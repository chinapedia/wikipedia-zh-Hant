<div style="float:left;border:solid #99B3FF 1px;margin:1px">

|                                |                                                    |
| ------------------------------ | -------------------------------------------------- |
| <span lang="en">**sgn**</span> | 這些用戶使用**[手語](../Category/sgn_使用者.md "wikilink")**。 |

</div>


使用手語的用戶可以利用以下四種模板：

  - **User sgn** 供手語為**母語**用戶使用
  - **User sgn-1** 供**基本**程度的手語用戶使用
  - **User sgn-2** 供**一般**程度的手語用戶使用
  - **User sgn-3** 供運用**熟練**手語的用戶使用

使用手語模板的語法相當簡單。 在你的用戶頁，輸入 `{{User sgn-X|Y}}` 即可！其中 X 代表你的手語水平，而 Y
則是你使用的手語名稱。

如果你想在[巴別模板中使用](../Page/Wikipedia:巴別.md "wikilink") sgn
模板，由於需要填寫語言參數，你需要多一個步驟。首先用
[subst](../Page/Help:Subst.md "wikilink")
關鍵字去替代模板中的源碼，如：`{{Subst:Babel-3|hi|en-3|sgn-2}}`。保存該頁後，再點擊「編輯本頁」，然後在
"User sgn" 模板中填上語言參數。例如，把 `{{User sgn-2}}` 改成 `{{User sgn-2|法國手語}}`。

[Sgn](../Category/用户语言.md "wikilink")