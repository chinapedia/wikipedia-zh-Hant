本分類收錄與**[日本](../Page/日本.md "wikilink")**相關的[小作品](../Page/Wikipedia:小作品.md "wikilink")。若要將條目加入本分類下，請加入****，或者****模板。

若條目有特定主題，請改用以下較為精確的小作品分類模板：

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><strong>人物</strong>
<ul>
<li><a href="../Category/日本政治人物.md" title="wikilink">政治人物</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Category/日本軍事人物.md" title="wikilink">軍事人物</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Category/日本企业家.md" title="wikilink">商業人物</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Category/日本藝人.md" title="wikilink">娛樂人物</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Category/日本藝術家.md" title="wikilink">藝術家</a>：<strong></strong> 或 <strong></strong>
<ul>
<li><a href="../Category/日本漫畫家.md" title="wikilink">漫畫家</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Category/日本作家.md" title="wikilink">作家</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
<li><a href="../Category/日本科學家.md" title="wikilink">科學家</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/學者.md" title="wikilink">學者</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Category/日本運動員.md" title="wikilink">體育人物</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
<li><strong>地理</strong>
<ul>
<li><a href="../Page/日本地理.md" title="wikilink">地理</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
</ul></td>
<td><ul>
<li><strong>歷史</strong>
<ul>
<li><a href="../Page/日本歷史.md" title="wikilink">歷史</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
<li><strong>建築</strong>
<ul>
<li><a href="../Category/日本建築.md" title="wikilink">建築</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
<li><strong>教育</strong>
<ul>
<li><a href="../Page/日本教育.md" title="wikilink">教育</a>：<strong></strong></li>
</ul></li>
<li><strong>娛樂</strong>
<ul>
<li><a href="../Page/日本電影.md" title="wikilink">電影</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Category/日本電視.md" title="wikilink">電視</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
<li><strong>交通</strong>
<ul>
<li><a href="../Page/日本鐵路.md" title="wikilink">鐵路</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
<li><strong>其它</strong>
<ul>
<li><a href="../Category/日本組織.md" title="wikilink">組織</a>：<strong></strong> 或 <strong></strong>
<ul>
<li>公司：<strong></strong> 或 <strong></strong></li>
</ul></li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

## 參見

  - [Wikipedia:小作品](../Page/Wikipedia:小作品.md "wikilink")
  - [Wikipedia:日本專題](../Page/Wikipedia:日本專題.md "wikilink")
  - [:Category:日本小作品模板](../Category/日本小作品模板.md "wikilink")

[\*](../Category/日本.md "wikilink") [J](../Category/亞洲小作品.md "wikilink")