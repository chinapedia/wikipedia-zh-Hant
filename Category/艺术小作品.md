本分類收錄關於**[藝術](../Page/藝術.md "wikilink")**的[小作品](../Page/Wikipedia:小作品.md "wikilink")。若要將條目加入本分類下，請加入****，或者****模板。

若條目有特定主題，請改用以下較為精確的小作品分類模板：

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><strong>文學</strong>
<ul>
<li><a href="../Page/文學.md" title="wikilink">文學</a>：<strong></strong> 或 <strong></strong>
<ul>
<li><a href="../Category/中文文學.md" title="wikilink">華語文學</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/史書.md" title="wikilink">史書</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/書法.md" title="wikilink">書法</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/小說.md" title="wikilink">小說</a>：<strong></strong> 或 <strong></strong>
<ul>
<li><a href="../Page/科幻.md" title="wikilink">科幻</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/托爾金.md" title="wikilink">托爾金</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/金庸.md" title="wikilink">金庸</a>：<strong></strong></li>
</ul></li>
</ul></li>
</ul></li>
<li><strong>音樂</strong>
<ul>
<li><a href="../Category/音樂類型.md" title="wikilink">音樂類型</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/樂器.md" title="wikilink">樂器</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/音樂專輯.md" title="wikilink">音樂專輯</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/歌曲.md" title="wikilink">歌曲</a>：<strong></strong> 或 <strong></strong>
<ul>
<li><a href="../Page/國歌.md" title="wikilink">國歌</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/嘻哈.md" title="wikilink">嘻哈歌曲</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/單曲.md" title="wikilink">單曲</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
</ul></li>
<li><strong>繪畫</strong>
<ul>
<li><a href="../Page/繪畫.md" title="wikilink">繪畫</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
<li><strong>攝影</strong>
<ul>
<li><a href="../Page/攝影.md" title="wikilink">攝影</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
</ul></td>
<td><ul>
<li><strong>電影</strong>
<ul>
<li><a href="../Page/電影.md" title="wikilink">電影</a>：<strong></strong> 或 <strong></strong>
<ul>
<li><a href="../Page/電影技術.md" title="wikilink">電影技術</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/美國電影.md" title="wikilink">美國電影</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/中國電影.md" title="wikilink">中國電影</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/法國電影.md" title="wikilink">法國電影</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/香港電影.md" title="wikilink">香港電影</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/日本電影.md" title="wikilink">日本電影</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/韓國電影.md" title="wikilink">韓國電影</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/新加坡電影.md" title="wikilink">新加坡電影</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/台灣電影.md" title="wikilink">台灣電影</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
</ul></li>
<li><strong>戲劇</strong>
<ul>
<li><a href="../Page/戲劇.md" title="wikilink">戲劇</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
<li><strong>戲曲</strong>
<ul>
<li><a href="../Page/戲曲.md" title="wikilink">戲曲</a>：<strong></strong></li>
</ul></li>
<li><strong>舞蹈</strong>
<ul>
<li><a href="../Page/舞蹈.md" title="wikilink">舞蹈</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
<li><strong>說唱藝術</strong>
<ul>
<li><a href="../Page/曲藝.md" title="wikilink">曲藝</a>：<strong></strong></li>
</ul></li>
<li><strong>博物館</strong>
<ul>
<li><a href="../Page/博物館.md" title="wikilink">博物館</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

[\*](../Category/藝術.md "wikilink") [A](../Category/文化小作品.md "wikilink")