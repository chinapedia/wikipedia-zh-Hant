本分類收錄與**[音樂](../Page/音樂.md "wikilink")**相關的**[小作品](../Page/Wikipedia:小作品.md "wikilink")**。若要將條目加入本分類下，請加入****，或者****模板。

若條目有特定主題，請改用以下較為精確的小作品分類模板：

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><strong>人物</strong>
<ul>
<li><a href="../Page/音樂家.md" title="wikilink">音樂家</a>：<strong></strong> 或 <strong></strong>
<ul>
<li><a href="../Page/古典音樂作曲家列表.md" title="wikilink">古典音樂作曲家</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
<li><a href="../Page/歌手.md" title="wikilink">歌手</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/樂團.md" title="wikilink">樂團</a>：<strong></strong> 或 <strong></strong>
<ul>
<li><a href="../Page/搖滾樂隊.md" title="wikilink">搖滾樂隊</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
</ul></li>
<li><strong>音樂類型</strong>
<ul>
<li><a href="../Page/音樂類型.md" title="wikilink">音樂類型</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
</ul></td>
<td><ul>
<li><strong>音樂作品</strong>
<ul>
<li><a href="../Page/專輯.md" title="wikilink">專輯</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/歌曲.md" title="wikilink">歌曲</a>：<strong></strong> 或 <strong></strong>
<ul>
<li><a href="../Page/國歌.md" title="wikilink">國歌</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/單曲.md" title="wikilink">單曲</a>：<strong></strong> 或 <strong></strong></li>
<li><a href="../Page/嘻哈.md" title="wikilink">嘻哈歌曲</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
</ul></li>
<li><strong>其它</strong>
<ul>
<li><a href="../Page/樂器.md" title="wikilink">樂器</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

-----

[Σ](../Category/音乐.md "wikilink") [M](../Category/艺术小作品.md "wikilink")