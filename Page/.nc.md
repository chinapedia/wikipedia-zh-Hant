**.nc**為[法國](../Page/法国.md "wikilink")[海外屬地](../Page/海外屬地.md "wikilink")[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")[國家及地區頂級域](../Page/國家及地區頂級域.md "wikilink")（ccTLD）的[域名](../Page/域名.md "wikilink")。

只有注册在新喀里多尼亞的公司才被容许使用.nc域名\[1\]\[2\]\[3\]。

## 参考

## 外部連結

  - [IANA .nc whois information](http://www.iana.org/root-whois/nc.htm)
  - [.nc domain registration website](http://www.cctld.nc/)

[sv:Toppdomän\#N](../Page/sv:Toppdomän#N.md "wikilink")

[nc](../Category/國家及地區頂級域.md "wikilink")
[Category:新喀里多尼亞](../Category/新喀里多尼亞.md "wikilink")

1.
2.
3.