**.org**是[互聯網的](../Page/互联网.md "wikilink")[通用頂級域之一](../Page/通用頂級域.md "wikilink")，「org」是英文「organization（組織）」的縮寫，在創立時主要供不屬於當時其他5個通用頂級域類別（如[.gov和](../Page/.gov.md "wikilink")[.edu](../Page/.edu.md "wikilink")）的組織使用，包括[非營利機構](../Page/非營利機構.md "wikilink")、[國際組織等](../Page/國際組織.md "wikilink")。現在已經沒有任何限制，它和頂級域[.com一樣可以用於任何場合](../Page/.com.md "wikilink")，包括盈利和非營利組織、機構、團體。[維基百科及其他維基屬下姊妹計劃與網站所使用的域名通用頂級域就是](../Page/維基百科.md "wikilink").org（wikipedia.org）。

## 外部連結

  - [IANA .org whois資料](http://www.iana.org/root-whois/org.htm)
  - [InterNIC FAQs on the .org
    Transition](http://www.internic.net/faqs/org-transition.html)
  - [List of .org accredited
    registrars](http://www.pir.org/GetAOrg/RegistrarList.aspx)

[org](../Category/頂級域.md "wikilink")