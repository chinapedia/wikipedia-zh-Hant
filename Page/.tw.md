**.tw**（或**.-{台灣}-**）是[臺灣的](../Page/臺灣.md "wikilink")[國家和地區頂級域名](../Page/國家和地區頂級域名.md "wikilink")，由[臺灣網路資訊中心維護管理](../Page/臺灣網路資訊中心.md "wikilink")。註冊網域需透過臺灣網路資訊中心所授權的網域代理商進行，全球目前有11家公司負責。

## 第二層網域

網域可註冊於第二層或數個第二層網域下的第三層：

  - **edu.tw**：教育與學術單位使用（由[教育部負責管理](../Page/中華民國教育部.md "wikilink")）
  - **gov.tw**：政府機關使用（由[國家發展委員會負責管理](../Page/國家發展委員會.md "wikilink")（2014年1月22日前為[行政院研考會](../Page/行政院研究發展考核委員會.md "wikilink")））
  - **mil.tw**：[國防部使用](../Page/中華民國國防部.md "wikilink")
  - **com.tw**：登記於[中華民國法律之下或是國外的商業公司使用](../Page/中華民國法律.md "wikilink")
  - **net.tw**：持有第一類及第二類電信事業許可執照的使用者
  - **org.tw**：根據中華民國法律創設或國外的非營利組織使用
  - **idv.tw**：個人使用，不限制（但需要經由[電子郵件認證](../Page/电子邮件.md "wikilink")）
  - **game.tw**：不限制（但申請者需要提供電子郵件以作認證，由[SEEDNet負責管理](../Page/SEEDNet.md "wikilink")）
  - **ebiz.tw**：不限制（但申請者需要提供電子郵件以作認證）
  - **club.tw**：不限制（但申請者需要提供電子郵件以作認證）
  - **.tw**：不限制
  - **.-{台灣}-**：不限制

## 中文網域

中文網域名稱可註冊於第二層。同時，透過台灣網域註冊機構註冊英文網域的註冊人可自動獲得一個 -{網路}-.tw、-{組織}-.tw及
-{商業}-.tw第二層網域下的第三層中文網域。這些第二層網域分別對映至**.net.tw**、**.org.tw**及**.com.tw**。

2010年6月25日，[ICANN宣布了以下两种中文域名](../Page/互联网名称与数字地址分配机构.md "wikilink")\[1\]:

  - \-{.台灣}- -
    “.taiwan”的[正體中文](../Page/正體中文.md "wikilink")（[IDNA版本](../Page/IDNA.md "wikilink")：.xn--kpry57d）
  - \-{.台湾}- -
    “.taiwan”的[简体中文](../Page/简体中文.md "wikilink")（[IDNA版本](../Page/IDNA.md "wikilink")：.xn--kprw13d）

2010年10月14日，臺灣網路資訊中心宣佈開始接受申請以上兩種網域，進入實用階段。

## 参考文献

## 外部連結

  - [臺灣網路資訊中心WHOIS](http://whois.twnic.net.tw/)
  - [IANA — .tw Domain Delegation
    Data](http://www.iana.org/root-whois/tw.htm)
  - [台灣網路資訊中心 TWNIC
    受理註冊機構](https://www.twnic.net.tw/dnservice_company_intro.php)

{{-}}

[sv:Toppdomän\#T](../Page/sv:Toppdomän#T.md "wikilink")

[tw](../Category/國家及地區頂級域.md "wikilink")
[Category:台灣網際網路](../Category/台灣網際網路.md "wikilink")

1.