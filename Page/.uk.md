[NominetUKLogo.gif](https://zh.wikipedia.org/wiki/File:NominetUKLogo.gif "fig:NominetUKLogo.gif")
**.uk**為[英國](../Page/英國.md "wikilink")[國家及地區頂級域](../Page/國家及地區頂級域.md "wikilink")（ccTLD）的[域名](../Page/域名.md "wikilink")。截至2012年3月，是全世界第四大最流行的[顶级域名](../Page/顶级域名.md "wikilink")，位于[.com](../Page/.com.md "wikilink")、[.de及](../Page/.de.md "wikilink")[.net之后](../Page/.net.md "wikilink")，有超过一千万的注册量\[1\]\[2\]。目前已可直接注册.uk域名，但是通常仍流行注册.uk的[二级域名](../Page/二级域名.md "wikilink")。

## 二級域名

  - **.ac.uk**
  - **.co.uk**
  - **.gov.uk**
  - **.judiciary.uk**
  - **.ltd.uk**
  - **.me.uk**
  - **.mod.uk**
  - **.net.uk**
  - **.nhs.uk**
  - **.nic.uk**
  - **.org.uk**
  - **.parliament.uk**
  - **.plc.uk**
  - **.police.uk**
  - **.sch.uk**

## 參考資料

[uk](../Category/國家及地區頂級域.md "wikilink")
[Category:英國網際網路](../Category/英國網際網路.md "wikilink")

1.  [BBC News - Landmark 10 millionth .uk site registered with
    Nominet](http://www.bbc.co.uk/news/technology-17393008)
2.  [Domain Name Wire - .Uk domain hits 10 million
    milestone](http://domainnamewire.com/2012/03/16/uk-domain-hits-10-million-milestone/)