**1**（**一**／
*壹*）是[0与](../Page/0.md "wikilink")[2之间的](../Page/2.md "wikilink")[自然数](../Page/自然数.md "wikilink")，是最小的正[奇數](../Page/奇數.md "wikilink")。

## 数学性质

  - 1是最小的[正整數](../Page/正整數.md "wikilink")。
  - 在[数论中](../Page/数论.md "wikilink")，1是最小的[自然数](../Page/自然数.md "wikilink")。
  - 1是唯一一個既不是[質数也不是](../Page/質数.md "wikilink")[合数的自然數](../Page/合数.md "wikilink")\[1\]

<!-- end list -->

  - 第1個[幸運數](../Page/幸運數.md "wikilink")
  - 第1個[快樂數](../Page/快樂數.md "wikilink")
  - [偶素數的](../Page/偶素數.md "wikilink")[數量](../Page/數量.md "wikilink")
  - 第1個[三角形数](../Page/三角形数.md "wikilink")
  - [斐波那契数列的第](../Page/斐波那契数列.md "wikilink")1项和第2项。
  - 1不能作為[進位制的](../Page/进位制.md "wikilink")[底](../Page/底.md "wikilink")。
  - 1不能做[对数的](../Page/对数.md "wikilink")[底](../Page/底数_\(对数\).md "wikilink")。
  - 1的[倒数是](../Page/倒数.md "wikilink")1。
  - 在[階乘](../Page/階乘.md "wikilink")，0\!=1\!=1
  - [相反數是](../Page/相反數.md "wikilink")-1
  - 在[概率論中](../Page/概率论.md "wikilink")，任一[样本空間中必然发生的](../Page/样本空間.md "wikilink")[隨机事件之](../Page/隨机事件.md "wikilink")[概率定义为](../Page/概率.md "wikilink")1。
  - 1是[正数](../Page/正数.md "wikilink")、[整数](../Page/整数.md "wikilink")、[有理数](../Page/有理数.md "wikilink")、[代数数](../Page/代數數.md "wikilink")、[實数](../Page/實数.md "wikilink")、复数。
  - 在[几何学中](../Page/几何学.md "wikilink")，[单位圆和](../Page/单位圆.md "wikilink")[單位球的](../Page/單位球.md "wikilink")[半徑都是](../Page/半径.md "wikilink")1。
  - [歐拉恆等式](../Page/歐拉恆等式.md "wikilink")，**\(e\,^{i \pi} + 1 = 0\;\)**，把[数学上五个重要的](../Page/数学.md "wikilink")[常数以简约的方式連繫起來](../Page/常数.md "wikilink")。公式中包含**1**、**[0](../Page/0.md "wikilink")**、[自然对数的底](../Page/自然对数.md "wikilink")<big>[{{Serif](../Page/e_\(数学常数\).md "wikilink")</big>、[圓周率](../Page/圓周率.md "wikilink")[***π***及](../Page/圓周率.md "wikilink")[複数的](../Page/複数.md "wikilink")[虛數單位](../Page/虛數.md "wikilink")[***i***](../Page/虛數單位.md "wikilink")。
  - 1在[十進制下是第](../Page/十進制.md "wikilink")1個[卡布列克數](../Page/卡布列克數.md "wikilink")
  - 任何[底数為自然數的](../Page/底數_\(進位記數法\).md "wikilink")[進位制裡的](../Page/進位制.md "wikilink")1都寫作1，即======
  - 1是任何自然數的[因數](../Page/因數.md "wikilink")
  - [0.999…](../Page/0.999….md "wikilink")=1
  - [巴都萬數列的第](../Page/巴都萬數列.md "wikilink")1項、第2項和第3項

## 在科學中

  - 在[計算機科學中](../Page/计算机科学.md "wikilink")，1經常用在[布尔逻辑的真值表中](../Page/布尔逻辑.md "wikilink")，表示「真」值。
  - 在[几何光学中](../Page/几何光学.md "wikilink")，[真空的](../Page/真空.md "wikilink")[折射率是](../Page/折射率.md "wikilink")1。
  - 在[天文学中](../Page/天文学.md "wikilink")，[太陽与](../Page/太阳.md "wikilink")[地球间之平均距離为](../Page/地球.md "wikilink")1个[天文单位](../Page/天文單位.md "wikilink")。
  - 在[化學中](../Page/化学.md "wikilink")，[氫的](../Page/氢.md "wikilink")[原子序數是](../Page/原子序数.md "wikilink")1。\[2\]
  - 在[ASCII中](../Page/ASCII.md "wikilink")，1為「Start of Heading」。

## 時間與曆法

  - 現代國際通用的[西曆](../Page/公历.md "wikilink")，將1[年分成](../Page/年.md "wikilink")12个[月](../Page/月.md "wikilink")。12个月每月長度不一，但都有1日，分別為[1月1日](../Page/1月1日.md "wikilink")、[2月1日](../Page/2月1日.md "wikilink")、[3月1日](../Page/3月1日.md "wikilink")、[4月1日](../Page/4月1日.md "wikilink")、[5月1日](../Page/5月1日.md "wikilink")、[6月1日](../Page/6月1日.md "wikilink")、[7月1日](../Page/7月1日.md "wikilink")、[8月1日](../Page/8月1日.md "wikilink")、[9月1日](../Page/9月1日.md "wikilink")、[10月1日](../Page/10月1日.md "wikilink")、[11月1日和](../Page/11月1日.md "wikilink")[12月1日](../Page/12月1日.md "wikilink")。
  - 此外，在公曆紀年方面，人類對公元[前1年](../Page/前1年.md "wikilink")、公元[1年](../Page/1年.md "wikilink")，公元[前1世纪及公元](../Page/前1世纪.md "wikilink")[1世纪均有記載](../Page/1世纪.md "wikilink")。

## 在电子-{A|zh-tw:訊號;zh-cn:信号;}-与-{zh-tw:資訊;zh-cn:信息;}-系统中

  - 在[-{zh-tw:數位;zh-hk:數碼;zh-cn:数字;}-電路中](../Page/数字电路.md "wikilink")，不使用[精确的](../Page/精确.md "wikilink")[电压值来代表](../Page/电压.md "wikilink")[信号的值](../Page/信号.md "wikilink")，只使用0和1两个值。1表示高于预先规定的[阈值电压](../Page/阈值.md "wikilink")，被称为[高电平或者](../Page/高电平.md "wikilink")[逻辑1](../Page/布尔逻辑#.E7.9C.9F.E5.80.BC.E8.A1.A8.md "wikilink")。与之对应，[0表示低于预先规定的阈值](../Page/0.md "wikilink")[电压](../Page/电压.md "wikilink")，被称为[低电平或者](../Page/低电平.md "wikilink")[逻辑0](../Page/布尔逻辑#.E7.9C.9F.E5.80.BC.E8.A1.A8.md "wikilink")。
  - 於[JavaScript裡](../Page/JavaScript.md "wikilink")，1代表[布林值](../Page/布林值.md "wikilink")`true`。
  - 在[雙音多頻信號的電話系統中](../Page/雙音多頻.md "wikilink")，按鍵1是由1209[赫茲](../Page/赫茲.md "wikilink")([高頻](../Page/高頻.md "wikilink"))和697赫茲([低頻](../Page/低頻.md "wikilink"))的[正弦信號所組成](../Page/正弦信號.md "wikilink")。

## 在人類文化中

  - A（小寫為**a**）是[拉丁字母的第](../Page/拉丁字母.md "wikilink")**1**個[字母](../Page/字母.md "wikilink")。
  - 在以[部首檢字法為主的](../Page/部首.md "wikilink")[中文](../Page/中文.md "wikilink")[字典中](../Page/字典.md "wikilink")，“一”往往是第一個[部首和第一個字](../Page/部首.md "wikilink")；口頭上“一”還被讀作“”。
  - 用於單位。如一瓶、一罐、一箱等。
  - 在[人类](../Page/人.md "wikilink")[文化中](../Page/文化.md "wikilink")，“一”别赋予了万物之始的意义：“惟初[太極](../Page/太極.md "wikilink")，[道立於一](../Page/道.md "wikilink")，造分天地，化成萬物，凡一之屬皆从一”（《[說文解字](../Page/说文解字.md "wikilink")》）。
  - [英文中也以](../Page/英语.md "wikilink")“The Great
    One”（偉大的一，太一）指代[聖經中的](../Page/聖經.md "wikilink")[上帝](../Page/上帝.md "wikilink")[耶和華](../Page/耶和華.md "wikilink")。
  - [貨幣中的基本面額](../Page/貨幣.md "wikilink")，如1[美元](../Page/美元.md "wikilink")、1[欧元](../Page/欧元.md "wikilink")、1[人民幣](../Page/人民幣.md "wikilink")、1[新臺幣](../Page/新臺幣.md "wikilink")。
  - 在[樂理中](../Page/音樂理論.md "wikilink")，[简谱上的do音用](../Page/简谱.md "wikilink")1表示；[器乐演奏时的谱子用类似](../Page/器乐演奏.md "wikilink")“1=C”的符号来[定调](../Page/定调.md "wikilink")。
  - 在[塑膠分類標誌中](../Page/塑膠分類標誌.md "wikilink")，代表[聚對苯二甲酸乙二酯](../Page/聚對苯二甲酸乙二酯.md "wikilink")。
  - 在[香港電影分級制度中](../Page/香港電影分級制度.md "wikilink")，第1級（常寫作「I級」）的電影適合任何年齡人士觀看。
  - 在[百家姓中](../Page/百家姓.md "wikilink")，排第1位的姓氏是[趙](../Page/趙姓.md "wikilink")。
  - 在[儒略曆中](../Page/儒略曆.md "wikilink")，[1月](../Page/1月.md "wikilink")
    Januarius
    名字来自古[罗马神话的神](../Page/罗马神话.md "wikilink")[雅努斯](../Page/雅努斯.md "wikilink")。
  - 在[男同志文化中](../Page/男同志.md "wikilink")，[1號表示性行為中的插入方](../Page/1和0_\(性\).md "wikilink")。
  - [世界第一](../Page/世界之最列表.md "wikilink")
  - 第一名，冠军、最佳。

## 在軍事政治中

  - [一黨制](../Page/一黨制.md "wikilink")
  - [單一制](../Page/單一制.md "wikilink")
  - [一院制](../Page/一院制.md "wikilink")
  - [一帶一路](../Page/一帶一路.md "wikilink")
  - [第一次世界大戰](../Page/第一次世界大戰.md "wikilink")

## 在體育中

  - 在[棒球中](../Page/棒球.md "wikilink")，1是[投手的代號](../Page/投手.md "wikilink")

## 注释

## 参见

  -
  - [1年](../Page/1年.md "wikilink")

  - [前1年](../Page/前1年.md "wikilink")

[Category:數字](../Category/數字.md "wikilink")
[Category:東亞傳統數學](../Category/東亞傳統數學.md "wikilink")

1.  1最初是被考虑为[質數的](../Page/質數.md "wikilink")：[質數最初的定义为之被](../Page/質數.md "wikilink")1和它自己整除的数。但为了[因式分解理论的一致性](../Page/因式分解.md "wikilink")，尤其是[算术基本定理](../Page/算术基本定理.md "wikilink")，后来[質數被定义只有两个正](../Page/質數.md "wikilink")[因子](../Page/因子.md "wikilink")（1和自己）的[自然数](../Page/自然数.md "wikilink")。最后一个把1包括在[質數里的](../Page/質數.md "wikilink")[数学家是](../Page/数学家.md "wikilink")[昂利·勒贝格](../Page/昂利·勒贝格.md "wikilink")（于[1899年](../Page/1899年.md "wikilink")）
2.  [Royal Society of Chemistry - Visual Element Periodic
    Table](http://www.rsc.org/periodic-table)