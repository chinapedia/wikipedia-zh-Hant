《**101拘捕令**》系列為[亞洲電視於](../Page/亞洲電視.md "wikilink")1983年至1984年之間播放共三輯的警匪連續劇，此劇故事圍繞警察破案的故事，另外，「101拘捕令」指「市民拘捕權」。

## 《101拘捕令》

**《101拘捕令》**（101 Citizen
Arrest），[亞洲電視](../Page/亞洲電視.md "wikilink")1983年的劇集，警匪片，共20集，監製[劉家豪](../Page/劉家豪.md "wikilink")
。

### 演員表

  - [羅樂林](../Page/羅樂林.md "wikilink") 飾 徐大華
  - [尹志強](../Page/尹志強.md "wikilink") 飾 何子維
  - [曾偉權](../Page/曾偉權.md "wikilink") 飾 李長仔
  - [葉玉萍](../Page/葉玉萍.md "wikilink")
  - [溫碧霞](../Page/溫碧霞.md "wikilink")
  - [李麗麗](../Page/李麗麗.md "wikilink")
  - [曹查理](../Page/曹查理.md "wikilink")

## 《101拘捕令第二輯之熱線999》

**《101拘捕令第二輯之熱線999》**（101 Citizen Arrest
II）是繼《101拘捕令》之後開拍的續集，1984年首播，共20集，監製[梅小青](../Page/梅小青.md "wikilink")。

### 演員表

#### 主要演員

  - [羅樂林](../Page/羅樂林.md "wikilink") 飾
    許偉國（[警長](../Page/警長.md "wikilink")）
  - [尹志強](../Page/尹志強.md "wikilink") 飾
    陳達偉（[探員](../Page/探員.md "wikilink")）
  - [馬敏兒](../Page/馬敏兒.md "wikilink") 飾 汪麗萍（探員）
  - [蔡倩兒](../Page/蔡倩兒.md "wikilink") 飾 Mi Mi（探員）
  - [黃秋生](../Page/黃秋生.md "wikilink") 飾 阿曾（探員）
  - [劉緯民](../Page/劉緯民.md "wikilink") 飾
    梁彼得（Peter）（[高級督察](../Page/高級督察.md "wikilink")）
  - [李顯明](../Page/李顯明.md "wikilink") 飾 金公子（探員）
  - [陳鳳芝](../Page/陳鳳芝.md "wikilink") 飾
    Candy（女[督察](../Page/督察.md "wikilink")）
  - [曹達華](../Page/曹達華.md "wikilink") 飾 曹華（退役督察）
  - [紀保羅](../Page/紀保羅.md "wikilink") 飾 [警司](../Page/警司.md "wikilink")
  - [金燕玲](../Page/金燕玲.md "wikilink") 飾 阿鈴（許偉國妻子）
  - [蔡國慶](../Page/蔡國慶.md "wikilink")
  - [陳植槐](../Page/陳植槐.md "wikilink")
  - [金興賢](../Page/金興賢.md "wikilink")
  - [華忠男](../Page/華忠男.md "wikilink")

#### 其他演員

  - [曹達華](../Page/曹達華.md "wikilink") 飾 陳達偉父
  - [施　明](../Page/施明.md "wikilink")
  - [范永華](../Page/范永華.md "wikilink")
  - [陳麗雲](../Page/陳麗雲.md "wikilink") 飾 尹夫人
  - [周　沛](../Page/周沛.md "wikilink") 飾 盲曹
  - [陳植槐](../Page/陳植槐.md "wikilink") 飾 亞溫
  - [陳　亮](../Page/陳亮.md "wikilink") 飾 亞譚
  - [吳耀良](../Page/吳耀良.md "wikilink")

## 《101拘捕令第三輯之勇敢新世界》

**《101拘捕令第三輯之勇敢新世界》**（101 Citizen Arrest
III）是《101拘捕令》系列的第三輯，1984年首播，共30集，監製[梅小青](../Page/梅小青.md "wikilink")。

### 演員表

#### 主要演員

  - [羅樂林](../Page/羅樂林.md "wikilink") 飾 蕭啟輝
  - [尹志強](../Page/尹志強.md "wikilink") 飾 查繼藩
  - [李　影](../Page/李影_\(香港演員\).md "wikilink") 飾 韋雪茵
  - [文雪兒](../Page/文雪兒.md "wikilink") 飾 韋雪英（韋雪茵妹）
  - [湯鎮宗](../Page/湯鎮宗.md "wikilink") 飾 蕭啟明（蕭啟輝弟）

#### 其他演員

  - [王愛華](../Page/王愛華.md "wikilink") 飾 張嘉麗
  - [徐寶鳳](../Page/徐寶鳳.md "wikilink")
  - [周比利](../Page/周比利.md "wikilink") 飾 薜冠雄
  - [弗　烈](../Page/弗烈.md "wikilink") 飾 王榮光
  - [羅敏玲](../Page/羅敏玲.md "wikilink")
  - [鄭恕峰](../Page/鄭恕峰.md "wikilink") 飾 酋長
  - [吳玉壽](../Page/吳玉壽.md "wikilink") 飾 暴強
  - [鄧麗盈](../Page/鄧麗盈.md "wikilink") 飾 周韻玲
  - [金　童](../Page/金童.md "wikilink") 飾 呂達財
  - [司馬華龍](../Page/司馬華龍.md "wikilink") 飾 薜文忠
  - [鄭文霞](../Page/鄭文霞.md "wikilink") 飾 薜文忠妻
  - [歐陽莎菲](../Page/歐陽莎菲.md "wikilink") 飾 呂達財母
  - [曹達華](../Page/曹達華.md "wikilink") 飾 爺爺
  - [黃秋生](../Page/黃秋生.md "wikilink") 飾 John
  - [葉碧雲](../Page/葉碧雲.md "wikilink") 飾 April
  - [許瑩英](../Page/許瑩英.md "wikilink") 飾 April母
  - 劉少雄（Vincent ）飾周良

## 主題曲

### 第二輯

提供：[風行唱有限公司](../Page/風行唱有限公司.md "wikilink")
作曲：[關聖佑](../Page/關聖佑.md "wikilink")
作詞：[潘偉源](../Page/潘偉源.md "wikilink")、[黃志華](../Page/黃志華.md "wikilink")
主唱：[陳鳳芝](../Page/陳鳳芝.md "wikilink")
\=== 第三輯 === 歌名：黑色太陽
提供：[EMI唱片有限公司](../Page/EMI唱片有限公司.md "wikilink")
作曲：[關聖佑](../Page/關聖佑.md "wikilink")
作詞：[盧國沾](../Page/盧國沾.md "wikilink")
主唱：[陳漢德](../Page/陳漢德.md "wikilink")

## 參考文獻

## 外部連結

  - [亞洲電視 -
    動作片集](https://archive.is/20130426123529/http://app.hkatv.com/info/atve/distribution/view.php?type=2)

[Category:1983年亞視電視劇集](../Category/1983年亞視電視劇集.md "wikilink")
[Category:1984年亞視電視劇集](../Category/1984年亞視電視劇集.md "wikilink")