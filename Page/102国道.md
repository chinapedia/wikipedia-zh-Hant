**102国道**（或**国道102线**、**G102线**）是在[中華人民共和國的一条](../Page/中華人民共和國.md "wikilink")[国道](../Page/中国国道.md "wikilink")，起点为[北京](../Page/北京市.md "wikilink")，终点为[黑龙江](../Page/黑龙江省.md "wikilink")[哈尔滨](../Page/哈尔滨市.md "wikilink")，全程1297千米。

这条国道经过[北京](../Page/北京市.md "wikilink")、[河北](../Page/河北省.md "wikilink")、[天津](../Page/天津市.md "wikilink")、[辽宁](../Page/辽宁省.md "wikilink")、[吉林和](../Page/吉林省.md "wikilink")[黑龙江](../Page/黑龙江省.md "wikilink")6个省份。

## 里程碑

<center>

| 城市名                                                                                                  | 距起点距离 |
| ---------------------------------------------------------------------------------------------------- | ----- |
| [北京市](../Page/北京市.md "wikilink")                                                                     | 0     |
| [河北省](../Page/河北省.md "wikilink")[三河市](../Page/三河市.md "wikilink")                                     | 64    |
| [河北省](../Page/河北省.md "wikilink")[玉田县](../Page/玉田县.md "wikilink")                                     | 124   |
| [河北省](../Page/河北省.md "wikilink")[唐山市](../Page/唐山市.md "wikilink")[丰润区](../Page/丰润区.md "wikilink")     | 157   |
| [河北省](../Page/河北省.md "wikilink")[卢龙县](../Page/卢龙县.md "wikilink")                                     | 224   |
| [河北省](../Page/河北省.md "wikilink")[秦皇岛市](../Page/秦皇岛市.md "wikilink")[抚宁区](../Page/抚宁区.md "wikilink")   | 257   |
| [河北省](../Page/河北省.md "wikilink")[秦皇岛市](../Page/秦皇岛市.md "wikilink")[山海关区](../Page/山海关区.md "wikilink") | 306   |
| [辽宁省](../Page/辽宁省.md "wikilink")[绥中县](../Page/绥中县.md "wikilink")                                     | 371   |
| [辽宁省](../Page/辽宁省.md "wikilink")[兴城市](../Page/兴城市.md "wikilink")                                     | 418   |
| [辽宁省](../Page/辽宁省.md "wikilink")[葫芦岛市](../Page/葫芦岛市.md "wikilink")[连山区](../Page/连山区.md "wikilink")   | 442   |
| [辽宁省](../Page/辽宁省.md "wikilink")[锦州市](../Page/锦州市.md "wikilink")                                     | 502   |
| [辽宁省](../Page/辽宁省.md "wikilink")[凌海市](../Page/凌海市.md "wikilink")                                     | 526   |
| [辽宁省](../Page/辽宁省.md "wikilink")[北镇市](../Page/北镇市.md "wikilink")                                     | 604   |
| [辽宁省](../Page/辽宁省.md "wikilink")[黑山县](../Page/黑山县.md "wikilink")                                     | 635   |
| [辽宁省](../Page/辽宁省.md "wikilink")[新民市](../Page/新民市.md "wikilink")                                     | 709   |
| [辽宁省](../Page/辽宁省.md "wikilink")[沈阳市](../Page/沈阳市.md "wikilink")                                     | 777   |
| [辽宁省](../Page/辽宁省.md "wikilink")[沈阳市](../Page/沈阳市.md "wikilink")[沈北新区](../Page/沈北新区.md "wikilink")   | 806   |
| [辽宁省](../Page/辽宁省.md "wikilink")[铁岭市](../Page/铁岭市.md "wikilink")                                     | 853   |
| [辽宁省](../Page/辽宁省.md "wikilink")[开原市](../Page/开原市.md "wikilink")                                     | 885   |
| [辽宁省](../Page/辽宁省.md "wikilink")[昌图县](../Page/昌图县.md "wikilink")                                     | 918   |
| [吉林省](../Page/吉林省.md "wikilink")[四平市](../Page/四平市.md "wikilink")                                     | 985   |
| [吉林省](../Page/吉林省.md "wikilink")[公主岭市](../Page/公主岭市.md "wikilink")                                   | 1032  |
| [吉林省](../Page/吉林省.md "wikilink")[长春市](../Page/长春市.md "wikilink")                                     | 1091  |
| [吉林省](../Page/吉林省.md "wikilink")[德惠市](../Page/德惠市.md "wikilink")                                     | 1149  |
| [黑龙江省](../Page/黑龙江省.md "wikilink")[哈尔滨市](../Page/哈尔滨市.md "wikilink")[双城区](../Page/双城区.md "wikilink") | 1297  |

## 參考資料

[Category:中国国道](../Category/中国国道.md "wikilink")
[Category:北京市公路](../Category/北京市公路.md "wikilink")
[Category:河北省公路](../Category/河北省公路.md "wikilink")
[Category:辽宁省公路](../Category/辽宁省公路.md "wikilink")
[Category:吉林省公路](../Category/吉林省公路.md "wikilink")
[Category:黑龙江省公路](../Category/黑龙江省公路.md "wikilink")