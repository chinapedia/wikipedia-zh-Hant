**10月13日**是[阳历一年中的第](../Page/阳历.md "wikilink")286天（[闰年第](../Page/闰年.md "wikilink")287天），离全年的结束还有79天。

## 大事记

### 1世紀

  - [54年](../Page/54年.md "wikilink")：[羅馬帝國](../Page/羅馬帝國.md "wikilink")[皇帝](../Page/羅馬皇帝.md "wikilink")[克勞狄一世被妻子](../Page/克勞狄一世.md "wikilink")[阿格里皮娜毒杀](../Page/小阿格里皮娜.md "wikilink")，其妻与前夫所生的儿子[尼禄继承皇位](../Page/尼禄.md "wikilink")。

### 10世紀

  - [982年](../Page/982年.md "wikilink")：[辽国](../Page/辽朝.md "wikilink")[萧太后掌握政权](../Page/萧绰.md "wikilink")。

### 14世紀

  - [1307年](../Page/1307年.md "wikilink")：[法国国王](../Page/法国.md "wikilink")[腓力四世下令逮捕各地的](../Page/腓力四世_\(法兰西\).md "wikilink")[圣殿骑士团](../Page/圣殿骑士团.md "wikilink")。

### 18世紀

  - [1773年](../Page/1773年.md "wikilink")：[法国天文学家](../Page/法国.md "wikilink")[梅西叶在](../Page/夏尔·梅西耶.md "wikilink")[猎犬座发现首个](../Page/猎犬座.md "wikilink")[螺旋星系](../Page/螺旋星系.md "wikilink")[M51](../Page/M51.md "wikilink")。

### 19世紀

  - [1815年](../Page/1815年.md "wikilink")：[英国占领](../Page/英国.md "wikilink")[大西洋上的](../Page/大西洋.md "wikilink")[阿森松岛](../Page/阿森松岛.md "wikilink")。
  - [1815年](../Page/1815年.md "wikilink")：[那不勒斯国王](../Page/那不勒斯.md "wikilink")[若阿尚·缪拉被](../Page/若阿尚·缪拉.md "wikilink")[奥地利军事法庭审判后处决](../Page/奥地利.md "wikilink")。
  - [1841年](../Page/1841年.md "wikilink")：[第一次鸦片战争](../Page/第一次鸦片战争.md "wikilink")：英军占领[宁波](../Page/宁波市.md "wikilink")。
  - [1860年](../Page/1860年.md "wikilink")：[第二次鸦片战争](../Page/第二次鸦片战争.md "wikilink")：英国和[法国军队占领](../Page/法国.md "wikilink")[北京](../Page/北京市.md "wikilink")，大掠[圆明园](../Page/圆明园.md "wikilink")
  - [1884年](../Page/1884年.md "wikilink")：[格林尼治时间正式被采用为](../Page/格林尼治標準時間.md "wikilink")[国际标准时间](../Page/协调世界时.md "wikilink")。
  - [1885年](../Page/1885年.md "wikilink")：[中国](../Page/中国.md "wikilink")[清朝设立](../Page/清朝.md "wikilink")[总理海军事务衙门](../Page/总理海军事务衙门.md "wikilink")，中国海军成立。

### 20世紀

  - [1928年](../Page/1928年.md "wikilink")：[安阳](../Page/安阳.md "wikilink")[殷墟开始发掘](../Page/殷墟.md "wikilink")。
  - [1937年](../Page/1937年.md "wikilink")：[新四军建军](../Page/新四军.md "wikilink")。
  - [1943年](../Page/1943年.md "wikilink")：[第二次世界大战](../Page/第二次世界大战.md "wikilink")：[意大利宣布加入](../Page/意大利.md "wikilink")[軸心國](../Page/軸心國.md "wikilink")，并向[同盟国宣战](../Page/同盟国.md "wikilink")。
  - [1944年](../Page/1944年.md "wikilink")：[第二次世界大战](../Page/第二次世界大战.md "wikilink")：盟军攻占[雅典](../Page/雅典.md "wikilink")，完成对[希腊的占领](../Page/希腊.md "wikilink")。
  - [1949年](../Page/1949年.md "wikilink")：[中国少年先锋队建队纪念日](../Page/中国少年先锋队.md "wikilink")。
  - [1961年](../Page/1961年.md "wikilink")：中国和[缅甸签署边界协定](../Page/缅甸.md "wikilink")。
  - [1964年](../Page/1964年.md "wikilink")：[蘇聯三位](../Page/蘇聯.md "wikilink")[太空人安全返回](../Page/太空人.md "wikilink")[地球](../Page/地球.md "wikilink")。
  - [1970年](../Page/1970年.md "wikilink")：[中華人民共和國和](../Page/中華人民共和國.md "wikilink")[加拿大政府簽訂聯合聲明](../Page/加拿大.md "wikilink")，兩國建立邦交關係。
  - [1972年](../Page/1972年.md "wikilink")：载有[橄榄球队成员的](../Page/橄榄球.md "wikilink")[乌拉圭空军571号班机在飞越](../Page/烏拉圭空軍571號班機空難.md "wikilink")[安第斯山脉时失事坠落](../Page/安第斯山脉.md "wikilink")，72天获救后剩16人生还。
  - [1980年](../Page/1980年.md "wikilink")：[香港地鐵](../Page/香港地鐵.md "wikilink")[荃灣綫](../Page/荃灣綫.md "wikilink")[長沙灣道近](../Page/長沙灣道.md "wikilink")[楓樹街地盤發生大火](../Page/楓樹街.md "wikilink")，一個助理消防區長殉職。
  - [1982年](../Page/1982年.md "wikilink")：[波兰](../Page/波兰.md "wikilink")[格但斯克造船工人在](../Page/格但斯克.md "wikilink")[团结工会率领下罢工](../Page/團結工聯.md "wikilink")。
  - [1991年](../Page/1991年.md "wikilink")：[台灣](../Page/台灣.md "wikilink")[民主進步黨於全國黨員代表大會通過](../Page/民主進步黨.md "wikilink")《[台獨黨綱](../Page/台獨黨綱.md "wikilink")》。
  - [1994年](../Page/1994年.md "wikilink")：[日本](../Page/日本.md "wikilink")[乒乓球手](../Page/乒乓球.md "wikilink")[小山智丽](../Page/小山智丽.md "wikilink")（即原中国球手何智丽）击败中国选手[鄧亞萍获得](../Page/鄧亞萍.md "wikilink")[廣島亚运会乒乓球女单金牌](../Page/第十二屆亞洲運動會.md "wikilink")，小山表示以代表日本为荣，引发争议。

### 21世紀

  - [2001年](../Page/2001年.md "wikilink")：[中国高性能计算机芯片](../Page/中国.md "wikilink")“[龙芯](../Page/龙芯.md "wikilink")”问世。
  - [2004年](../Page/2004年.md "wikilink")：[颱風蠍虎在](../Page/颱風蠍虎_\(2004年\).md "wikilink")[馬里亞納群島近海生成](../Page/馬里亞納群島.md "wikilink")，並在[宮古島東南發展為](../Page/宮古島.md "wikilink")[超大型颱風](../Page/超大型颱風.md "wikilink")。
  - [2010年](../Page/2010年.md "wikilink")：在[智利](../Page/智利.md "wikilink")[2010年科皮亞波礦難中被困](../Page/2010年科皮亞波礦難.md "wikilink")69日的33名科皮亚波矿工最终获救，过程更被电视直播，观众超过1亿人。
  - [2015年](../Page/2015年.md "wikilink")：[Minecraft故事模式發行](../Page/Minecraft.md "wikilink")。
  - [2016年](../Page/2016年.md "wikilink")：[拉瑪九世于泰国曼谷](../Page/拉瑪九世.md "wikilink")[诗里拉吉医院逝世](../Page/诗里拉吉医院.md "wikilink")，終年88歲。
  - [2017年](../Page/2017年.md "wikilink")：[拉瑪九世逝世](../Page/拉瑪九世.md "wikilink")1周年，眾多泰國人在[西里拉醫院和](../Page/西里拉醫院.md "wikilink")[曼谷大皇宮叩拜](../Page/曼谷大皇宮.md "wikilink")[拉瑪九世](../Page/拉瑪九世.md "wikilink")。

## 出生

  - [1696年](../Page/1696年.md "wikilink")：[約翰·赫維](../Page/約翰·赫維，第二代赫維男爵.md "wikilink")，[英國廷臣](../Page/英國.md "wikilink")、小冊子作者、回憶錄作家，第二代赫維男爵（[1743年去世](../Page/1743年.md "wikilink")）
  - [1786年](../Page/1786年.md "wikilink")：[律勞卑](../Page/律勞卑.md "wikilink")，首任[英國](../Page/英國.md "wikilink")[駐華商務總監](../Page/英國駐華商務總監.md "wikilink")（[1834年去世](../Page/1834年.md "wikilink")）
  - [1821年](../Page/1821年.md "wikilink")：[鲁道夫·菲尔绍](../Page/鲁道夫·菲尔绍.md "wikilink")，[德國醫學家](../Page/德國.md "wikilink")、人類學家、公共衛生學家、病理學家、古生物學家和政治家（[1902年去世](../Page/1902年.md "wikilink")）
  - [1870年](../Page/1870年.md "wikilink")：[艾尔伯特·杰伊·诺克](../Page/艾尔伯特·杰伊·诺克.md "wikilink")，[美國自由意志主義作家](../Page/美國.md "wikilink")、教育理論家、社會評論家（[1945年去世](../Page/1945年.md "wikilink")）
  - [1892年](../Page/1892年.md "wikilink")：[楊肇嘉](../Page/楊肇嘉.md "wikilink")，[臺灣日治時期政治家](../Page/臺灣日治時期.md "wikilink")（[1976年去世](../Page/1976年.md "wikilink")）
  - [1909年](../Page/1909年.md "wikilink")：[阿特·塔图姆](../Page/阿特·塔图姆.md "wikilink")，[美國爵士樂鋼琴家](../Page/美國.md "wikilink")（[1956年去世](../Page/1956年.md "wikilink")）
  - [1921年](../Page/1921年.md "wikilink")：[-{zh-tw:尤·蒙頓; zh-cn:伊夫·蒙当;
    }-](../Page/伊夫·蒙当.md "wikilink")，出生於[義大利的](../Page/義大利.md "wikilink")[法國演員及歌手](../Page/法國.md "wikilink")（[1991年去世](../Page/1991年.md "wikilink")）
  - [1925年](../Page/1925年.md "wikilink")：[-{zh-hans:撒切尔夫人;zh-hk:戴卓爾夫人;zh-tw:柴契爾夫人;}-](../Page/玛格利特·撒切尔.md "wikilink")，英國政治家，前[保守黨領袖](../Page/保守黨.md "wikilink")、[前首相](../Page/英国首相.md "wikilink")（2013年去世）
  - [1928年](../Page/1928年.md "wikilink")：[丁雄泉](../Page/丁雄泉.md "wikilink")，旅美華裔畫家，常以「採花大盜」落款（[2010年去世](../Page/2010年.md "wikilink")）
  - [1931年](../Page/1931年.md "wikilink")：[劉兆銘](../Page/劉兆銘.md "wikilink")，[香港男演員](../Page/香港.md "wikilink")、香港舞蹈開拓者
  - [1934年](../Page/1934年.md "wikilink")：[娜娜·穆斯库莉](../Page/娜娜·穆斯库莉.md "wikilink")，[希臘女歌手](../Page/希臘.md "wikilink")，被譽為“希臘國寶”「雅典的白玫瑰」。
  - [1941年](../Page/1941年.md "wikilink")：[-{zh-hans:保罗·西蒙;
    zh-tw:保羅·賽門;}-](../Page/保罗·西蒙.md "wikilink")，[美国民谣歌手](../Page/美国.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：[德西·鲍特瑟](../Page/德西·鲍特瑟.md "wikilink")，第九任[蘇利南總統](../Page/蘇利南.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：[姜根鐸](../Page/姜根鐸.md "wikilink")，[韓國外交官](../Page/韓國.md "wikilink")，前[駐香港總領事](../Page/大韓民國駐香港總領事館.md "wikilink")、、駐[斐濟大使](../Page/斐濟.md "wikilink")。
  - [1946年](../Page/1946年.md "wikilink")：[李光復](../Page/李光復.md "wikilink")，[中國內地男演員](../Page/中國.md "wikilink")
  - [1952年](../Page/1952年.md "wikilink")：[尊龍](../Page/尊龍.md "wikilink")，[美国華人演員](../Page/美国.md "wikilink")
  - [1952年](../Page/1952年.md "wikilink")：[貝弗莉·約翰遜](../Page/貝弗莉·約翰遜.md "wikilink")，[美國超模](../Page/美國.md "wikilink")，《時尚》封面史上第一位黑人模特兒及第一位非裔美國人
  - [1956年](../Page/1956年.md "wikilink")：[克里斯·卡特](../Page/克里斯·卡特.md "wikilink")，[美國電視劇劇作家](../Page/美國.md "wikilink")、製片家，以《[X檔案](../Page/X檔案.md "wikilink")》知名
  - [1961年](../Page/1961年.md "wikilink")：[道格·里弗斯](../Page/道格·里弗斯.md "wikilink")，[美國職業籃球教練](../Page/美國.md "wikilink")
  - [1961年](../Page/1961年.md "wikilink")：[徐錦江](../Page/徐錦江.md "wikilink")，[香港男演員](../Page/香港.md "wikilink")
  - [1962年](../Page/1962年.md "wikilink")：[凯莉·普雷斯顿](../Page/凯莉·普雷斯顿.md "wikilink")，[美國女演員](../Page/美國.md "wikilink")、模特
  - [1963年](../Page/1963年.md "wikilink")：[河承武](../Page/河承武.md "wikilink")，[韓國男诗人](../Page/韓國.md "wikilink")
  - [1964年](../Page/1964年.md "wikilink")：[小野坂昌也](../Page/小野坂昌也.md "wikilink")，[日本](../Page/日本.md "wikilink")[聲優](../Page/日本配音員.md "wikilink")
  - [1964年](../Page/1964年.md "wikilink")：[聶海勝](../Page/聶海勝.md "wikilink")，[中國宇航員](../Page/中國.md "wikilink")
  - [1967年](../Page/1967年.md "wikilink")：[崔佛·霍夫曼](../Page/崔佛·霍夫曼.md "wikilink")，[美國職棒大聯盟球員](../Page/美國職棒大聯盟.md "wikilink")，[美國職棒生涯總救援成功數紀錄保持者](../Page/美國.md "wikilink")
  - [1967年](../Page/1967年.md "wikilink")：[哈维尔·索托马约尔](../Page/哈维尔·索托马约尔.md "wikilink")，[古巴運動員](../Page/古巴.md "wikilink")，男子跳高世界紀錄2.45米和室內世界紀錄2.43米保持者
  - [1969年](../Page/1969年.md "wikilink")：[南茜·克里根](../Page/南茜·克里根.md "wikilink")，[美國花式溜冰運動員](../Page/美國.md "wikilink")，史上第九位獲得兩塊奧運會獎牌的女子單人溜冰運動員
  - [1970年](../Page/1970年.md "wikilink")：[傅明憲](../Page/傅明憲.md "wikilink")，[香港女演員](../Page/香港.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink")：[保羅·波茨](../Page/保羅·波茨.md "wikilink")，[英國手機銷售員](../Page/英國.md "wikilink")，2007年6月9日參加《[英國達人](../Page/英國達人.md "wikilink")》比賽節目獻唱歌劇《[公主徹夜未眠](../Page/公主徹夜未眠.md "wikilink")》崛起成為業餘歌劇唱家
  - [1970年](../Page/1970年.md "wikilink")：[高昌錫](../Page/高昌錫.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")
  - [1971年](../Page/1971年.md "wikilink")：[-{zh-hans:沙查·巴隆·科恩;zh-hk:沙格·畢朗·高漢;zh-tw:沙夏·拜倫·科恩;}-](../Page/沙查·巴隆·科恩.md "wikilink")，猶太裔英國喜劇演員
  - [1973年](../Page/1973年.md "wikilink")：[松嶋菜菜子](../Page/松嶋菜菜子.md "wikilink")，[日本女演员](../Page/日本.md "wikilink")，有「日劇女王」美名
  - [1974年](../Page/1974年.md "wikilink")：[劉愷威](../Page/劉愷威.md "wikilink")，[香港男演員](../Page/香港.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[保羅·皮雅斯](../Page/保罗·皮尔斯.md "wikilink")，[美國職業籃球運動員](../Page/美國.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[榊原由依](../Page/榊原由依.md "wikilink")，[日本女性聲優](../Page/日本.md "wikilink")、歌手。
  - [1982年](../Page/1982年.md "wikilink")：[科比](../Page/伊恩·詹姆斯·索普.md "wikilink")，[澳大利亚游泳运动员](../Page/澳大利亚.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[趙胤熙](../Page/趙胤熙.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[肘井美佳](../Page/肘井美佳.md "wikilink")，[日本女演員](../Page/日本.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[近江知永](../Page/近江知永.md "wikilink")，[日本女性聲優](../Page/日本.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[伊恩·索普](../Page/伊恩·索普.md "wikilink")，[澳洲游泳運動員](../Page/澳洲.md "wikilink")，綽號魚雷，共獲得5枚奧運金牌、3枚銀牌與1枚銅牌
  - [1983年](../Page/1983年.md "wikilink")：[唐振剛](../Page/唐振剛.md "wikilink")，[台灣男歌手](../Page/台灣.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[陳倩揚](../Page/陳倩揚.md "wikilink")，[香港女藝人](../Page/香港.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[Misono](../Page/Misono.md "wikilink")，[日本女歌手](../Page/日本.md "wikilink")，[倖田來未親妹妹](../Page/倖田來未.md "wikilink")
  - [1985年](../Page/1985年.md "wikilink") :
    [基藍馬](../Page/基藍馬.md "wikilink")，香港足球運動員
  - [1986年](../Page/1986年.md "wikilink")：[艾邦拉荷](../Page/加布里埃尔·阿格邦拉霍.md "wikilink")，[英格蘭職業足球運動員](../Page/英格蘭.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[全烋星](../Page/全烋星.md "wikilink")，[韓國](../Page/韓國.md "wikilink")[secret成員](../Page/secret.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[普嘉·海迪](../Page/普嘉·海迪.md "wikilink")，[印度女演員](../Page/印度.md "wikilink")
  - [1995年](../Page/1995年.md "wikilink")：[朴智旻](../Page/朴智旻.md "wikilink")，[韓國男子組合](../Page/韓國.md "wikilink")[防彈少年團成員](../Page/防彈少年團.md "wikilink")
  - [1996年](../Page/1996年.md "wikilink")：[黃之鋒](../Page/黃之鋒.md "wikilink")，[香港社運人士](../Page/香港.md "wikilink")

## 逝世

  - [54年](../Page/54年.md "wikilink")：[克劳狄一世](../Page/克劳狄一世.md "wikilink")，罗马皇帝（生于[前10年](../Page/前10年.md "wikilink")）
  - [982年](../Page/982年.md "wikilink")：[辽景宗耶律贤](../Page/辽景宗.md "wikilink")，[辽朝皇帝](../Page/辽朝.md "wikilink")（生于[948年](../Page/948年.md "wikilink")）
  - [1815年](../Page/1815年.md "wikilink")：[若阿尚·缪拉](../Page/若阿尚·缪拉.md "wikilink")，法国军事家（生于[1767年](../Page/1767年.md "wikilink")）
  - [1941年](../Page/1941年.md "wikilink")：[李叔同](../Page/李叔同.md "wikilink")，法號弘一大师，[中国现代画家](../Page/中国.md "wikilink")、书法家、音乐家、戏剧家（生于[1880年](../Page/1880年.md "wikilink")）
  - [2006年](../Page/2006年.md "wikilink")：[王光美](../Page/王光美.md "wikilink")，[中國前](../Page/中國.md "wikilink")[國家主席](../Page/中華人民共和國主席.md "wikilink")[劉少奇夫人](../Page/劉少奇.md "wikilink")（生于[1921年](../Page/1921年.md "wikilink")）
  - [2009年](../Page/2009年.md "wikilink")：[吕正操](../Page/吕正操.md "wikilink")，中國軍事家、[中國人民解放軍上將](../Page/中國人民解放軍上將.md "wikilink")（生于[1904年](../Page/1904年.md "wikilink")）
  - [2012年](../Page/2012年.md "wikilink")：[司徒錦源](../Page/司徒錦源.md "wikilink")，[香港電影及電視編劇](../Page/香港.md "wikilink")（生于[1964年](../Page/1964年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[柳瀨嵩](../Page/柳瀨嵩.md "wikilink")，[日本](../Page/日本.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")、《[麵包超人](../Page/麵包超人.md "wikilink")》作者\[1\]（生于[1919年](../Page/1919年.md "wikilink")）
  - [2016年](../Page/2016年.md "wikilink")：[拉瑪九世](../Page/拉瑪九世.md "wikilink")，[泰國國王](../Page/泰國.md "wikilink")，有確切記錄[在位時間第二長的獨立主權君主](../Page/在位時間最長的君主列表.md "wikilink")（生於[1927年](../Page/1927年.md "wikilink")）。

## 节假日和习俗

  - [世界保健日](../Page/世界保健日.md "wikilink")
  - [中国少年先锋队建队纪念日](../Page/中国少年先锋队.md "wikilink")
  - 拉瑪九世逝世紀念日

## 參考資料

1.