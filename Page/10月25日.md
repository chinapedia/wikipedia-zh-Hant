**10月25日**是[阳历一年中的第](../Page/阳历.md "wikilink")298天（[闰年第](../Page/闰年.md "wikilink")299天），离全年的结束还有67天。

## 大事记

### 15世紀

  - [1415年](../Page/1415年.md "wikilink")：[英格兰国王](../Page/英格兰国王.md "wikilink")[亨利五世率领的军队在](../Page/亨利五世_\(英格兰\).md "wikilink")[阿金库尔战役中打败](../Page/阿金库尔战役.md "wikilink")[法国军队](../Page/法国.md "wikilink")。

### 16世紀

  - [1555年](../Page/1555年.md "wikilink")：[查理五世逊](../Page/查理五世.md "wikilink")[西班牙王位](../Page/西班牙.md "wikilink")。
  - [1586年](../Page/1586年.md "wikilink")：[蘇格蘭女王](../Page/蘇格蘭.md "wikilink")[玛丽被](../Page/玛丽一世_\(苏格兰\).md "wikilink")[英格兰法庭判处死刑](../Page/英格兰.md "wikilink")。

### 19世紀

  - [1860年](../Page/1860年.md "wikilink")：[清朝恭親王](../Page/清朝.md "wikilink")[奕訢和法國駐華公使](../Page/愛新覺羅·奕訢.md "wikilink")[葛罗签订](../Page/葛罗.md "wikilink")《[中法北京条约](../Page/北京条约.md "wikilink")》。
  - [1864年](../Page/1864年.md "wikilink")：[太平天国幼天王](../Page/太平天国.md "wikilink")，[洪秀全长子](../Page/洪秀全.md "wikilink")[洪天贵福于](../Page/洪天贵福.md "wikilink")[江西石城荒山被清军所俘](../Page/江西省.md "wikilink")。
  - [1875年](../Page/1875年.md "wikilink")：[德国钢琴家](../Page/德国.md "wikilink")[汉斯·冯·彪罗在](../Page/汉斯·冯·彪罗.md "wikilink")[美国](../Page/美国.md "wikilink")[波士顿首演](../Page/波士顿.md "wikilink")[柴可夫斯基的](../Page/柴可夫斯基.md "wikilink")[第1钢琴协奏曲](../Page/第1钢琴协奏曲_\(柴可夫斯基\).md "wikilink")。

### 20世紀

  - [1915年](../Page/1915年.md "wikilink")：[孫中山和](../Page/孫中山.md "wikilink")[宋慶齡在](../Page/宋慶齡.md "wikilink")[日本](../Page/日本.md "wikilink")[东京登记结婚](../Page/东京.md "wikilink")。
  - [1931年](../Page/1931年.md "wikilink")：[美國](../Page/美國.md "wikilink")[纽约](../Page/纽约.md "wikilink")[乔治·华盛顿大桥通车](../Page/乔治·华盛顿大桥.md "wikilink")。
  - [1936年](../Page/1936年.md "wikilink")：[德国和](../Page/德国.md "wikilink")[意大利的代表在](../Page/意大利.md "wikilink")[柏林签订同盟](../Page/柏林.md "wikilink")[条约](../Page/条约.md "wikilink")，[柏林-罗马轴心建立](../Page/柏林-罗马轴心.md "wikilink")。
  - [1938年](../Page/1938年.md "wikilink")：[武汉沦陷](../Page/武汉.md "wikilink")，[中國抗日戰爭中规模最大的局部战争](../Page/中國抗日戰爭.md "wikilink")[武汉会战因此结束](../Page/武汉会战.md "wikilink")。
  - [1950年](../Page/1950年.md "wikilink")：[中国人民志愿军出国作战參加](../Page/中国人民志愿军.md "wikilink")[韓戰](../Page/韓戰.md "wikilink")，并同[朝鲜人民军于同日发动](../Page/朝鲜人民军.md "wikilink")[抗美援朝第一次戰役](../Page/抗美援朝第一次戰役.md "wikilink")。
  - [1951年](../Page/1951年.md "wikilink")：[英国大选](../Page/英国.md "wikilink")，[保守党击败](../Page/保守党.md "wikilink")[工党](../Page/工党.md "wikilink")，[邱吉尔再任首相](../Page/温斯顿·丘吉尔.md "wikilink")。
  - [1958年](../Page/1958年.md "wikilink")：[宁夏回族自治区成立](../Page/宁夏回族自治区.md "wikilink")。
  - [1961年](../Page/1961年.md "wikilink")：[東西德邊界](../Page/東西德邊界.md "wikilink")
    美蘇坦克在查理檢查哨互相對峙，此後東西德正式對立。
  - [1971年](../Page/1971年.md "wikilink")：[聯合國大會第](../Page/聯合國大會.md "wikilink")26屆會議通过[2758號決議](../Page/聯合國大會2758號決議.md "wikilink")，以[中华人民共和国代替](../Page/中华人民共和国.md "wikilink")[中華民國行使在联合国的一切合法权利](../Page/中華民國.md "wikilink")。
  - [1972年](../Page/1972年.md "wikilink")：[香港](../Page/香港.md "wikilink")[十年建屋計劃](../Page/十年建屋計劃.md "wikilink")。
  - [1980年](../Page/1980年.md "wikilink")：西摩两国计划连接[直布罗陀海峡](../Page/直布罗陀海峡.md "wikilink")。
  - [1987年](../Page/1987年.md "wikilink")：[中國共產黨第十三次全國代表大會在](../Page/中國共產黨第十三次全國代表大會.md "wikilink")[北京召开](../Page/北京.md "wikilink")。
  - [1995年](../Page/1995年.md "wikilink")：[巴勒斯坦在](../Page/巴勒斯坦.md "wikilink")[约旦河西岸全面自治](../Page/约旦河.md "wikilink")。
  - [1996年](../Page/1996年.md "wikilink")：最后一届[港京拉力赛结束](../Page/港京拉力赛.md "wikilink")。

### 21世紀

  - [2001年](../Page/2001年.md "wikilink")：美国[微软公司开发的](../Page/微软公司.md "wikilink")[视窗操作系统](../Page/视窗操作系统.md "wikilink")[Windows
    XP在](../Page/Windows_XP.md "wikilink")[纽约向全球正式发布](../Page/纽约.md "wikilink")。
  - [2006年](../Page/2006年.md "wikilink")：[La
    new熊在](../Page/La_new熊.md "wikilink")[中華職棒17年總冠軍賽擊敗](../Page/2006年中華職棒總冠軍賽.md "wikilink")[統一獅](../Page/統一獅.md "wikilink")，獲得年度總冠軍。
  - [2007年](../Page/2007年.md "wikilink")：[空中客车A380首次商業飛行](../Page/空中客车A380.md "wikilink")，[新加坡航空公司從](../Page/新加坡航空公司.md "wikilink")[新加坡](../Page/新加坡.md "wikilink")[樟宜機場飛抵](../Page/樟宜機場.md "wikilink")[澳洲](../Page/澳洲.md "wikilink")[雪梨](../Page/雪梨_\(城市\).md "wikilink")[京斯福特·史密斯機場](../Page/京斯福特·史密斯機場.md "wikilink")。
  - [2009年](../Page/2009年.md "wikilink")：[統一7-ELEVEn獅在](../Page/統一7-ELEVEn獅.md "wikilink")[中華職棒20年總冠軍賽擊敗](../Page/2009年中華職棒總冠軍賽.md "wikilink")[兄弟象](../Page/兄弟象.md "wikilink")，獲得年度總冠軍。
  - [2012年](../Page/2012年.md "wikilink")：微软在纽约发布[Windows
    8](../Page/Windows_8.md "wikilink")。

## 出生

  - [1642年](../Page/1642年.md "wikilink")：[鄭經](../Page/鄭經.md "wikilink")，[鄭成功之子](../Page/鄭成功.md "wikilink")，[台灣統治者](../Page/台灣.md "wikilink")（逝于[1681年](../Page/1681年.md "wikilink")）
  - [1682年](../Page/1682年.md "wikilink")：[蔡溫](../Page/蔡溫.md "wikilink")，[琉球國](../Page/琉球國.md "wikilink")[第二尚氏王朝時期著名的](../Page/第二尚氏王朝.md "wikilink")[政治家和](../Page/政治家.md "wikilink")[儒者](../Page/儒者.md "wikilink")（逝于[1762年](../Page/1762年.md "wikilink")）
  - [1811年](../Page/1811年.md "wikilink")：[埃瓦里斯特·伽罗瓦](../Page/埃瓦里斯特·伽罗瓦.md "wikilink")，数学家，[抽象代数的奠基人](../Page/抽象代数.md "wikilink")。（逝于[1832年](../Page/1832年.md "wikilink")）
  - [1825年](../Page/1825年.md "wikilink")：[小约翰·斯特劳斯](../Page/小约翰·施特劳斯.md "wikilink")，[奥地利音乐家](../Page/奥地利.md "wikilink")（逝于[1899年](../Page/1899年.md "wikilink")）
  - [1838年](../Page/1838年.md "wikilink")：[比才](../Page/乔治·比才.md "wikilink")，[法国作曲家](../Page/法国.md "wikilink")（逝于[1875年](../Page/1875年.md "wikilink")）
  - [1874年](../Page/1874年.md "wikilink")：[黄兴](../Page/黄兴.md "wikilink")，[辛亥革命先驱](../Page/辛亥革命.md "wikilink")（逝于[1916年](../Page/1916年.md "wikilink")）
  - [1881年](../Page/1881年.md "wikilink")：[毕加索](../Page/巴伯羅·畢卡索.md "wikilink")，近代著名[西班牙画家](../Page/西班牙.md "wikilink")、[法国现代画派主要代表](../Page/法国.md "wikilink")（逝于[1973年](../Page/1973年.md "wikilink")）
  - [1929年](../Page/1929年.md "wikilink")：[陳美玉](../Page/陳美玉.md "wikilink")，中華民國[排球前輩](../Page/排球.md "wikilink")、[台灣](../Page/台灣.md "wikilink")[童軍運動先驅](../Page/童军.md "wikilink")（逝于[2005年](../Page/2005年.md "wikilink")）
  - [1936年](../Page/1936年.md "wikilink")：[野澤雅子](../Page/野澤雅子.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1944年](../Page/1944年.md "wikilink")：[阿兹占阿都拉萨](../Page/阿兹占阿都拉萨.md "wikilink")，[马来西亚](../Page/马来西亚.md "wikilink")[吉打州第](../Page/吉打州.md "wikilink")10任[州务大臣](../Page/州务大臣.md "wikilink")（逝于[2013年](../Page/2013年.md "wikilink")）
  - [1949年](../Page/1949年.md "wikilink")：[卓勝利](../Page/卓勝利.md "wikilink")，[台灣藝人](../Page/台灣.md "wikilink")
    （逝於2012年）
  - [1952年](../Page/1952年.md "wikilink")：[關山月](../Page/關山月.md "wikilink")，中國[嶺南畫派](../Page/嶺南畫派.md "wikilink")[畫家](../Page/畫家.md "wikilink")（逝於[2000年](../Page/2000年.md "wikilink")）
  - [1964年](../Page/1964年.md "wikilink")：[恩田陸](../Page/恩田陸.md "wikilink")，[日本](../Page/日本.md "wikilink")[作家](../Page/作家.md "wikilink")
  - [1967年](../Page/1967年.md "wikilink")：[松本大洋](../Page/松本大洋.md "wikilink")，[日本](../Page/日本.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")
  - [1971年](../Page/1971年.md "wikilink")：[朱茵](../Page/朱茵.md "wikilink")，[香港女演员](../Page/香港.md "wikilink")
  - [1976年](../Page/1976年.md "wikilink")：[盧比度·羅沙達](../Page/盧比度·羅沙達.md "wikilink")，[香港](../Page/香港.md "wikilink")[西班牙籍足球員](../Page/西班牙.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[伊姆蘭·阿巴斯](../Page/伊姆蘭·阿巴斯.md "wikilink")，[巴基斯坦男演員](../Page/巴基斯坦.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[凱蒂·佩芮](../Page/凱蒂·佩芮.md "wikilink")，[美國歌手](../Page/美國.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[华天](../Page/华天.md "wikilink")，中国马术选手
  - 1990年：[米蘭娜·拉錫](../Page/米蘭娜·拉錫.md "wikilink")，[塞爾維亞女子排球運動員](../Page/塞爾維亞.md "wikilink")

## 逝世

  - [348年](../Page/348年.md "wikilink")：[慕容皝](../Page/慕容皝.md "wikilink")，字元真，小字万年，[鲜卑族人](../Page/鲜卑族.md "wikilink")。中國[五胡十六國時代](../Page/五胡十六國.md "wikilink")[前燕的開國君主](../Page/前燕.md "wikilink")（生于[297年](../Page/297年.md "wikilink")）
  - [1400年](../Page/1400年.md "wikilink")：[喬叟](../Page/喬叟.md "wikilink")，[英国](../Page/英国.md "wikilink")[中世纪著名](../Page/中世纪.md "wikilink")[作家](../Page/作家.md "wikilink")
  - [1760年](../Page/1760年.md "wikilink")：[喬治二世](../Page/喬治二世_\(英國\).md "wikilink")，漢諾瓦選帝侯、[英國國王](../Page/英國國王.md "wikilink")（[1683年出生](../Page/1683年.md "wikilink")）
  - [1955年](../Page/1955年.md "wikilink")：[佐佐木禎子](../Page/佐佐木禎子.md "wikilink")，[日本](../Page/日本.md "wikilink")[廣島市原爆](../Page/廣島市.md "wikilink")[被爆者](../Page/被爆者.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[陳百強](../Page/陳百強.md "wikilink")，[香港歌手](../Page/香港.md "wikilink")
  - [2010年](../Page/2010年.md "wikilink")：[章魚保羅](../Page/章魚保羅.md "wikilink")，足球“神算”。（[2008年出生](../Page/2008年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[比爾·夏曼](../Page/比爾·夏曼.md "wikilink")，前美國NBA籃球運動員（[1926年出生](../Page/1926年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[哈爾·尼達姆](../Page/哈爾·尼達姆.md "wikilink")，美國男演員（[1931年出生](../Page/1931年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[奧古斯都·奧登](../Page/奧古斯都·奧登.md "wikilink")，[義大利經濟學家](../Page/義大利.md "wikilink")（[1933年出生](../Page/1933年.md "wikilink")）
  - [2018年](../Page/2018年.md "wikilink")：[李咏](../Page/李咏.md "wikilink")，中国中央电视台著名节目主持人（生於[1968年](../Page/1968年.md "wikilink")）

## 节假日和习俗

  - [中华人民共和国](../Page/中华人民共和国.md "wikilink")：[中国人民志愿军出国抗美援朝纪念日](../Page/朝鮮戰爭.md "wikilink")
  - [中華民國](../Page/中華民國.md "wikilink")：[台灣光復節](../Page/台灣光復節.md "wikilink")
  - [人类天花绝迹日](../Page/人类天花绝迹日.md "wikilink")