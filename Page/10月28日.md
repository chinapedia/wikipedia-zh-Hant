**10月28日**是[阳历一年中的第](../Page/阳历.md "wikilink")301天（[闰年第](../Page/闰年.md "wikilink")302天），离全年的结束还有64天。

## 大事记

### 4世紀

  - [306年](../Page/306年.md "wikilink")：[馬克森提烏斯成为](../Page/馬克森提烏斯.md "wikilink")[羅馬皇帝](../Page/羅馬皇帝.md "wikilink")。
  - [312年](../Page/312年.md "wikilink")：[君士坦丁一世击败马克森提](../Page/君士坦丁一世_\(罗马帝国\).md "wikilink")，统一[羅馬帝國](../Page/羅馬帝國.md "wikilink")。

### 13世紀

  - [1277年](../Page/1277年.md "wikilink")：大都（今[北京](../Page/北京.md "wikilink")）日全食。

### 15世紀

  - [1492年](../Page/1492年.md "wikilink")：[克里斯托弗·哥伦布在](../Page/克里斯托弗·哥伦布.md "wikilink")[古巴靠岸](../Page/古巴.md "wikilink")。

### 16世紀

  - [1538年](../Page/1538年.md "wikilink")：首間[新大陸大學成立](../Page/新大陸.md "wikilink")。

### 18世紀

  - [1707年](../Page/1707年.md "wikilink")：日本發生[強烈有感地震](../Page/1707年寶永地震.md "wikilink")，至少5000人死亡。

### 19世紀

  - [1868年](../Page/1868年.md "wikilink")：[爱迪生申请生平第一项专利](../Page/托马斯·爱迪生.md "wikilink")，电子投票记录仪。
  - [1886年](../Page/1886年.md "wikilink")：[美国总统](../Page/美国.md "wikilink")[格罗弗·克利夫兰在](../Page/格罗弗·克利夫兰.md "wikilink")[紐約港主持](../Page/紐約港.md "wikilink")[法国赠送的](../Page/法国.md "wikilink")[自由女神像的揭幕仪式](../Page/自由女神像.md "wikilink")。
  - [1893年](../Page/1893年.md "wikilink")：[俄羅斯](../Page/俄羅斯.md "wikilink")[浪漫樂派作曲家](../Page/浪漫樂派.md "wikilink")[柴可夫斯基在](../Page/柴可夫斯基.md "wikilink")[聖彼得堡指揮首演第六交響曲](../Page/聖彼得堡.md "wikilink")《悲愴》，並於九天後死於家中。

### 20世紀

  - [1911年](../Page/1911年.md "wikilink")：[九广铁路](../Page/九广铁路.md "wikilink")[英段与](../Page/东铁线.md "wikilink")[华段接轨](../Page/广深铁路.md "wikilink")，铁路全线通车。
  - [1914年](../Page/1914年.md "wikilink")：行刺[斐迪南凶手普林西普入狱](../Page/斐迪南.md "wikilink")。
  - [1918年](../Page/1918年.md "wikilink")：从[奥匈帝国独立的](../Page/奥匈帝国.md "wikilink")[捷克与](../Page/捷克.md "wikilink")[斯洛伐克合併](../Page/斯洛伐克.md "wikilink")，[捷克斯洛伐克共和国成立](../Page/捷克斯洛伐克共和国.md "wikilink")。
  - [1922年](../Page/1922年.md "wikilink")：[意大利国家法西斯党领导人](../Page/意大利国家法西斯党.md "wikilink")[贝尼托·墨索里尼率领支持者发动](../Page/贝尼托·墨索里尼.md "wikilink")“[向罗马进军](../Page/向罗马进军.md "wikilink")”的行动，以夺取意大利政权。
  - [1928年](../Page/1928年.md "wikilink")：。
  - [1929年](../Page/1929年.md "wikilink")：[世界經濟遭遇大蕭條](../Page/世界經濟.md "wikilink")。
  - [1940年](../Page/1940年.md "wikilink")：[意大利要求占领](../Page/意大利.md "wikilink")[希腊领土的](../Page/希腊.md "wikilink")[最后通牒被拒绝](../Page/最后通牒.md "wikilink")，[意大利军队入侵](../Page/意大利军队.md "wikilink")[希腊](../Page/希腊.md "wikilink")，[希腊意大利战争爆发](../Page/希腊意大利战争.md "wikilink")。
  - [1942年](../Page/1942年.md "wikilink")：[奧許維茲集中營有](../Page/奧許維茲集中營.md "wikilink")240名[波蘭年輕人遭到集體槍決](../Page/波蘭.md "wikilink")，大部分來自[盧布林](../Page/盧布林.md "wikilink")，後稱「盧布林鎮壓」。
  - [1942年](../Page/1942年.md "wikilink")：[克拉科夫-普拉佐集中營啟用](../Page/克拉科夫-普拉佐集中營.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：
  - [1950年](../Page/1950年.md "wikilink")：[中华人民共和国与](../Page/中华人民共和国.md "wikilink")[芬兰共和国建交](../Page/芬兰.md "wikilink")。
  - [1962年](../Page/1962年.md "wikilink")：[古巴导弹危机](../Page/古巴导弹危机.md "wikilink")：[蘇聯領袖](../Page/蘇聯.md "wikilink")[赫鲁晓夫宣佈撤走](../Page/赫鲁晓夫.md "wikilink")[古巴導彈](../Page/古巴.md "wikilink")。
  - 1962年：[日本歹徒砍伤](../Page/日本.md "wikilink")[中國访日女乒乓选手](../Page/中國.md "wikilink")。
  - [1970年](../Page/1970年.md "wikilink")：[意大利恐怖组织](../Page/意大利.md "wikilink")[红色旅成立](../Page/红色旅.md "wikilink")。
  - [1977年](../Page/1977年.md "wikilink")：[香港](../Page/香港.md "wikilink")[警廉衝突](../Page/警廉衝突.md "wikilink")。
  - [1979年](../Page/1979年.md "wikilink")：日本[御岳山火山突然爆发](../Page/御岳山.md "wikilink")。
  - 1979年：。
  - [1986年](../Page/1986年.md "wikilink")：。
  - [1988年](../Page/1988年.md "wikilink")：[法国发射第一颗电视直播卫星](../Page/法国.md "wikilink")\[1\]。
  - [1990年](../Page/1990年.md "wikilink")：香港[維多利亞公園一萬人集會保衛](../Page/維多利亞公園.md "wikilink")[釣魚台](../Page/釣魚台.md "wikilink")。
  - [1992年](../Page/1992年.md "wikilink")：
  - 1992年：[埃及赛德港因油轮被撞进入紧急状态](../Page/埃及.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：。
  - [1995年](../Page/1995年.md "wikilink")：科克伦走钢丝跨越夔门天堑。
  - 1995年：阿塞拜疆巴库地铁失火死伤惨重。
  - [1996年](../Page/1996年.md "wikilink")：[拉贝日记发表](../Page/拉贝日记.md "wikilink")。
  - [1997年](../Page/1997年.md "wikilink")：黄河[小浪底工程胜利截流](../Page/小浪底工程.md "wikilink")。
  - [1998年](../Page/1998年.md "wikilink")：一架[中国国际航空公司客机被](../Page/中国国际航空公司.md "wikilink")[劫持到](../Page/中國國際航空905號班機劫機事件.md "wikilink")[台灣](../Page/台灣.md "wikilink")。
  - [1998年](../Page/1998年.md "wikilink")：[義大利電影](../Page/義大利.md "wikilink")[海上鋼琴師上映](../Page/海上鋼琴師.md "wikilink")。
  - [2000年](../Page/2000年.md "wikilink")：[江泽民怒斥香港记者张宝华](../Page/江泽民怒斥香港女记者.md "wikilink")

### 21世紀

  - [2002年](../Page/2002年.md "wikilink")：[意大利](../Page/意大利.md "wikilink")[西西里岛上的](../Page/西西里岛.md "wikilink")[活火山](../Page/活火山.md "wikilink")[埃特纳火山出现大规模的喷发](../Page/埃特纳火山.md "wikilink")。
  - 2002年：新修订的《[中华人民共和国文物保护法](../Page/中华人民共和国文物保护法.md "wikilink")》正式实施。
  - [2007年](../Page/2007年.md "wikilink")：[克里斯蒂娜·費爾南德斯當選成為首位](../Page/克里斯蒂娜·費爾南德斯.md "wikilink")[阿根廷經](../Page/阿根廷.md "wikilink")[民主選舉產生的女總統](../Page/民主選舉.md "wikilink")。
  - 2007年：[統一獅在](../Page/統一獅.md "wikilink")[中華職棒18年總冠軍賽擊敗](../Page/2007年中華職棒總冠軍賽.md "wikilink")[La
    new熊](../Page/La_new熊.md "wikilink")，獲得年度總冠軍。
  - [2013年](../Page/2013年.md "wikilink")：[北京](../Page/北京.md "wikilink")[天安門廣場發生](../Page/天安門廣場.md "wikilink")[恐怖襲擊](../Page/天安门撞桥事件.md "wikilink")，3名恐怖分子駕駛一輛吉普車，衝向行人路，撞倒路上的途人，然後撞向金水橋，實施自爆。
  - [2016年](../Page/2016年.md "wikilink")：韓國外交官[楊昌洙經](../Page/楊昌洙.md "wikilink")[青瓦台委任為](../Page/青瓦台.md "wikilink")[駐台北-{}-韓國代表部新任代表](../Page/駐台北韓國代表部.md "wikilink")\[2\]。

## 出生

  - [1818年](../Page/1818年.md "wikilink")：[伊凡·屠格涅夫](../Page/伊万·谢尔盖耶维奇·屠格涅夫.md "wikilink")，俄国现实主义小说家、诗人、剧作家（逝於[1883年](../Page/1883年.md "wikilink")）
  - [1860年](../Page/1860年.md "wikilink") :
    [嘉納治五郎](../Page/嘉納治五郎.md "wikilink")，[日本](../Page/日本.md "wikilink")[柔道的](../Page/柔道.md "wikilink")[創始人](../Page/創始人.md "wikilink")（逝於[1938年](../Page/1938年.md "wikilink")）
  - [1885年](../Page/1885年.md "wikilink")：，俄羅斯詩人。（1922年逝世）
  - [1894年](../Page/1894年.md "wikilink")：[叶圣陶](../Page/叶圣陶.md "wikilink")，中国现代作家、教育家及出版人。（1988年逝世）
  - [1914年](../Page/1914年.md "wikilink")：[喬納斯·索爾克](../Page/喬納斯·索爾克.md "wikilink")，又譯為約納斯·沙克，小兒麻痺疫苗發明者。（1995年逝世）
  - [1941年](../Page/1941年.md "wikilink")：，[日本](../Page/日本.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [1947年](../Page/1947年.md "wikilink")：[馬海倫](../Page/馬海倫.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [1951年](../Page/1951年.md "wikilink")：[張銘杰](../Page/張銘杰.md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")
  - [1953年](../Page/1953年.md "wikilink")：[朴英奎](../Page/朴英奎.md "wikilink")，[韓國演員](../Page/韓國.md "wikilink")
  - [1955年](../Page/1955年.md "wikilink")：[比爾蓋茲](../Page/比爾蓋茲.md "wikilink")，是一名美國著名企業家、軟體工程師、慈善家以及[微軟公司的董事長](../Page/微軟.md "wikilink")
  - [1959年](../Page/1959年.md "wikilink")：[增田俊郎](../Page/增田俊郎.md "wikilink")，[日本](../Page/日本.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")
  - [1960年](../Page/1960年.md "wikilink")：[福田己津央](../Page/福田己津央.md "wikilink")，[日本動畫](../Page/日本動畫.md "wikilink")[導演](../Page/導演.md "wikilink")
  - [1963年](../Page/1963年.md "wikilink")：[郭耀明](../Page/郭耀明.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [1964年](../Page/1964年.md "wikilink")：[尹天照](../Page/尹天照.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [1967年](../Page/1967年.md "wikilink")：[茱莉亞·羅勃茲](../Page/茱莉亞·羅勃茲.md "wikilink")，美國好萊塢演員
  - [1966年](../Page/1966年.md "wikilink")：[陳法蓉](../Page/陳法蓉.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [1968年](../Page/1968年.md "wikilink")：[張子健](../Page/張子健.md "wikilink")，中國男演員
  - 1968年：[史蒂芬·杭特](../Page/史蒂芬·杭特.md "wikilink")，[紐西蘭男演員](../Page/紐西蘭.md "wikilink")\[3\]
  - [1971年](../Page/1971年.md "wikilink")：[陳建寧](../Page/陳建寧.md "wikilink")，[華納唱片音樂總監](../Page/華納唱片.md "wikilink")、台灣著名音樂人、[F.I.R.團長](../Page/F.I.R..md "wikilink")
  - [1973年](../Page/1973年.md "wikilink")：[瑪麗安·納瓦茲·謝里夫](../Page/瑪麗安·納瓦茲·謝里夫.md "wikilink")，[巴基斯坦女政治人物](../Page/巴基斯坦.md "wikilink")
  - [1974年](../Page/1974年.md "wikilink")：[祖昆·馮力士](../Page/祖昆·馮力士.md "wikilink")，美國電影演員、音樂人
  - [1976年](../Page/1976年.md "wikilink")：[金瑞馨](../Page/金瑞馨.md "wikilink")，韓國女演員
  - [1977年](../Page/1977年.md "wikilink")：[陳金鋒](../Page/陳金鋒.md "wikilink")，是第一位在美國職棒大聯盟比賽中登場的台灣選手
  - [1980年](../Page/1980年.md "wikilink")：[阿倫·史密夫](../Page/阿倫·史密夫.md "wikilink")，[英国](../Page/英国.md "wikilink")[足球运动员](../Page/足球.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[巴路士](../Page/米兰·巴罗什.md "wikilink")，[捷克足球運動員](../Page/捷克.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[倉木麻衣](../Page/倉木麻衣.md "wikilink")，[日本女歌手](../Page/日本.md "wikilink")
  - 1982年：[福井裕佳梨](../Page/福井裕佳梨.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[NEEKO](../Page/NEEKO.md "wikilink")，[日本模特兒及聲優](../Page/日本.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[袁嘉敏](../Page/袁嘉敏.md "wikilink")，[香港女](../Page/香港.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [1985年](../Page/1985年.md "wikilink")：[崔宇革](../Page/崔宇革.md "wikilink")，[韓國男](../Page/韓國.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[翁滋蔓](../Page/翁滋蔓.md "wikilink")，[台灣女演員和歌手](../Page/台灣.md "wikilink")
  - 1986年：[豐崎愛生](../Page/豐崎愛生.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - 1986年：[艾迪蒂·拉·哈亞里](../Page/艾迪蒂·拉·哈亞里.md "wikilink")，[印度女演員](../Page/印度.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[杜海濤](../Page/杜海濤.md "wikilink")，[中國節目主持人](../Page/中國.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[黃浩銘](../Page/黃浩銘.md "wikilink")，[香港政界人物](../Page/香港.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[何浩文](../Page/何浩文.md "wikilink")，[香港男歌手](../Page/香港.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[卡蜜爾·莫法特](../Page/卡蜜爾·莫法特.md "wikilink")，[法國女子游泳運動員](../Page/法國.md "wikilink")（2015年逝世）
  - [1990年](../Page/1990年.md "wikilink")：[張天愛](../Page/張天愛.md "wikilink")，[中國女演員](../Page/中國.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[承俊](../Page/承俊.md "wikilink")，[韓國男子偶像團體](../Page/韓國.md "wikilink")[KNK前成員](../Page/KNK.md "wikilink")
  - [1994年](../Page/1994年.md "wikilink")：[张宇皎](../Page/张宇皎.md "wikilink")，[中國女子體操隊隊员](../Page/中國.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink")：[昀昀](../Page/昀昀.md "wikilink")，[韓國男子偶像團體](../Page/韓國.md "wikilink")[NCT](../Page/NCT.md "wikilink")[中國籍成員](../Page/中國.md "wikilink")

## 逝世

  - [1704年](../Page/1704年.md "wikilink")：[约翰·洛克](../Page/约翰·洛克.md "wikilink")，[英国哲学家](../Page/英国.md "wikilink")、经验主义的开创人（[1632年出生](../Page/1632年.md "wikilink")）
  - [1929年](../Page/1929年.md "wikilink")：[伯恩哈德·冯·比洛](../Page/伯恩哈德·冯·比洛.md "wikilink")，[德國首相](../Page/德國.md "wikilink")（[1849年出生](../Page/1849年.md "wikilink")）
  - [1937年](../Page/1937年.md "wikilink")：[官惠民](../Page/官惠民.md "wikilink")，[中华民国](../Page/中华民国.md "wikilink")[国民革命军将领](../Page/国民革命军.md "wikilink")
  - [1937年](../Page/1937年.md "wikilink")：[刘眉生](../Page/刘眉生.md "wikilink")，中华民国国民革命军将领
  - [1973年](../Page/1973年.md "wikilink")：[塔哈·侯賽因](../Page/塔哈·侯賽因.md "wikilink")，[埃及作家](../Page/埃及.md "wikilink")（[1889年出生](../Page/1889年.md "wikilink")）
  - [1980年](../Page/1980年.md "wikilink")：[何非凡](../Page/何非凡.md "wikilink")，[香港粵劇演員](../Page/香港.md "wikilink")（[1919年出生](../Page/1919年.md "wikilink")）
  - [1998年](../Page/1998年.md "wikilink")：[泰德·休斯](../Page/泰德·休斯.md "wikilink")，英國[桂冠詩人](../Page/桂冠詩人.md "wikilink")（[1930年出生](../Page/1930年.md "wikilink")）
  - [2004年](../Page/2004年.md "wikilink")：[羅莎琳德·希克斯](../Page/羅莎琳德·希克斯.md "wikilink")，作家[-{zh-hans:阿加莎·克里斯蒂;zh-hant:阿嘉莎·克莉絲蒂}-的女兒及文學遺產管理者](../Page/阿加莎·克里斯蒂.md "wikilink")（1919年出生）\[4\]
  - [2006年](../Page/2006年.md "wikilink")：[霍英東](../Page/霍英東.md "wikilink")，香港商人，[中國人民政治協商會議第八](../Page/中國人民政治協商會議.md "wikilink")、九、十屆全國委員會副主席（[1923年出生](../Page/1923年.md "wikilink")）
  - [2008年](../Page/2008年.md "wikilink")：[孔德成](../Page/孔德成.md "wikilink")，[孔子七十七代嫡長孫](../Page/孔子.md "wikilink")，[衍聖公](../Page/衍聖公.md "wikilink")，[大成至聖先師奉祀官](../Page/大成至聖先師奉祀官.md "wikilink")（[1920年出生](../Page/1920年.md "wikilink")）
  - [2010年](../Page/2010年.md "wikilink")：[梁从诫](../Page/梁从诫.md "wikilink")，知名环保人士、环保组织[自然之友会长](../Page/自然之友.md "wikilink")。著名建筑学家[梁思成与](../Page/梁思成.md "wikilink")[林徽因之子](../Page/林徽因.md "wikilink")，著名革命家及戊戌變法領袖，学者梁啟超之孫。（[1932年出生](../Page/1932年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[塔德烏什·馬佐維耶茨基](../Page/塔德烏什·馬佐維耶茨基.md "wikilink")，[二戰後](../Page/二戰.md "wikilink")[中歐和](../Page/中歐.md "wikilink")[東歐第一個非](../Page/東歐.md "wikilink")[共產主義](../Page/共產主義.md "wikilink")[總理](../Page/波蘭總理.md "wikilink")<ref>

</ref>（[1927年出生](../Page/1927年.md "wikilink")）。

  - 2013年：[川上哲治](../Page/川上哲治.md "wikilink")，日本棒球運動員。（[1920年出生](../Page/1920年.md "wikilink")）

## 节假日和习俗

  - 联合国卫生组织的[男性健康日](../Page/男性健康日.md "wikilink")
  - [世界動畫日](../Page/世界動畫日.md "wikilink")（World Animation
    Day）──[加拿大政府於](../Page/加拿大政府.md "wikilink")2007年所訂定。

## 參考資料

1.
2.
3.
4.