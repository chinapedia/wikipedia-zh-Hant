**11**（十一）是[10与](../Page/10.md "wikilink")[12之间的](../Page/12.md "wikilink")[自然数](../Page/自然数.md "wikilink")。

## 数学性质

  - [外觀數列的第](../Page/外觀數列.md "wikilink")2項
  - 在以10以上的數為底的[进位制中](../Page/进位制.md "wikilink")，11表示為B，例：。
  - 因為99=11×9，1/11=0.090909...循環節為兩位。
  - \-11是[Heegner數](../Page/Heegner數.md "wikilink")。
  - [五角星數](../Page/五角星數.md "wikilink")
  - [中心十邊形數](../Page/中心多邊形數.md "wikilink")

## 在科学中

  - 是[鈉的](../Page/鈉.md "wikilink")[原子序數](../Page/原子序數.md "wikilink")\[1\]。

## 時間与曆法

現代國際通用的[西曆](../Page/公历.md "wikilink")，則將1[年分成](../Page/年.md "wikilink")12个[月](../Page/月.md "wikilink")。12个月每月長度不一，但都有11日，分別為[1月11日](../Page/1月11日.md "wikilink")、[2月11日](../Page/2月11日.md "wikilink")、[3月11日](../Page/3月11日.md "wikilink")、[4月11日](../Page/4月11日.md "wikilink")、[5月11日](../Page/5月11日.md "wikilink")、[6月11日](../Page/6月11日.md "wikilink")、[7月11日](../Page/7月11日.md "wikilink")、[8月11日](../Page/8月11日.md "wikilink")、[9月11日](../Page/9月11日.md "wikilink")、[10月11日](../Page/10月11日.md "wikilink")、[11月11日和](../Page/11月11日.md "wikilink")[12月11日](../Page/12月11日.md "wikilink")。

此外，在公曆紀年方面，人類對公元[前11年](../Page/前11年.md "wikilink")、公元[11年](../Page/11年.md "wikilink")，公元[前11世纪及公元](../Page/前11世纪.md "wikilink")[11世纪均有記載](../Page/11世纪.md "wikilink")。

## 在人类文化中

  - 「11」號公車：比喻用[雙腳步行](../Page/腳.md "wikilink")
  - 在香港，不少人稱「與未成年少女發生性行為」的罪行為「衰十一」（因為該罪行的名字長達11個字）。
  - [11月11日](../Page/11月11日.md "wikilink")，因为有四个1，被戏称为[光棍节](../Page/光棍节.md "wikilink")，11也就代表光棍
  - 11個人：組成球隊的一個數字
  - [7-Eleven是全球大型的連鎖式](../Page/7-Eleven.md "wikilink")[便利商店](../Page/便利商店.md "wikilink")

## 在其它领域中

## 参考文献

[Category:整數素數](../Category/整數素數.md "wikilink")

1.  [Royal Society of Chemistry - Visual Element Periodic
    Table](http://www.rsc.org/periodic-table)