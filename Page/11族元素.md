**11族元素**是[元素周期表的第](../Page/元素周期表.md "wikilink")11族元素（IB
族），位于[10族元素和](../Page/10族元素.md "wikilink")[12族元素之间](../Page/12族元素.md "wikilink")，包括的元素有：

  - [铜](../Page/铜.md "wikilink")（Cu）
  - [银](../Page/银.md "wikilink")（Ag）
  - [金](../Page/金.md "wikilink")（Au）
  - [錀](../Page/錀.md "wikilink")（Rg）

這四個元素都是[电的良](../Page/电.md "wikilink")[导体](../Page/导体.md "wikilink")，都是[密度较大](../Page/密度.md "wikilink")、[熔点](../Page/熔点.md "wikilink")、[沸点较高](../Page/沸点.md "wikilink")，[延展性较好的](../Page/延展性.md "wikilink")[金属](../Page/金属.md "wikilink")，其中錀是具[放射性的](../Page/放射性.md "wikilink")[人造元素](../Page/人造元素.md "wikilink")。11族元素也稱為**銅族元素**。又由于铜、银、金在近代以前普遍用于铸造[货币](../Page/货币.md "wikilink")，所以该族元素又称为“**货币金属**”。

**铜族元素主要性质比较**

| 元素名称                         | [元素符号](../Page/元素符号.md "wikilink") | [原子](../Page/原子.md "wikilink")[半径](../Page/半径.md "wikilink")（[nm](../Page/纳米.md "wikilink")） | 主要化合价          | [状态](../Page/状态.md "wikilink")（[标况](../Page/标准状况.md "wikilink")） | 单质[熔点](../Page/熔点.md "wikilink")（[℃](../Page/摄氏度.md "wikilink")） | 单质[沸点](../Page/沸点.md "wikilink")（℃） |
| ---------------------------- | ---------------------------------- | -------------------------------------------------------------------------------------------- | -------------- | ---------------------------------------------------------------- | ---------------------------------------------------------------- | ----------------------------------- |
| [铜](../Page/铜.md "wikilink") | Cu                                 | 0.117                                                                                        | 0，+1，+2        | [固体](../Page/固体.md "wikilink")                                   | 1083                                                             | 2582                                |
| [银](../Page/银.md "wikilink") | Ag                                 | 0.134                                                                                        | 0，+1           | [固体](../Page/固体.md "wikilink")                                   | 960.5                                                            | 2177                                |
| [金](../Page/金.md "wikilink") | Au                                 | 0.134                                                                                        | 0，+1，+3        | [固体](../Page/固体.md "wikilink")                                   | 1063                                                             | 2707                                |
| [錀](../Page/錀.md "wikilink") | Rg                                 | 0.114                                                                                        | \-1，0，+1，+3，+5 | [固体](../Page/固体.md "wikilink")（預測）                               | 不詳                                                               | 不詳                                  |

<center>

<table>
<tbody>
<tr class="odd">
<td><p>左方一族：</p></td>
<td><p><strong>11族元素<br />
第11族</strong></p></td>
<td><p>右方一族：</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/10族元素.md" title="wikilink">10族元素</a></strong></p></td>
<td><p><strong><a href="../Page/12族元素.md" title="wikilink">12族元素</a></strong></p></td>
<td></td>
</tr>
</tbody>
</table>

</Center>

[族](../Category/元素周期表.md "wikilink")