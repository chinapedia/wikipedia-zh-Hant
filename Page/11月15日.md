**11月15日**是[阳历一年中的第](../Page/阳历.md "wikilink")319天（[闰年第](../Page/闰年.md "wikilink")320天），离全年的结束还有46天。

## 大事记

### 18世紀

  - [1777年](../Page/1777年.md "wikilink")：[北美殖民地](../Page/北美殖民地.md "wikilink")13個創始[州共同承認並遵守的](../Page/州.md "wikilink")[美利堅合眾國第一](../Page/美利堅合眾國.md "wikilink")[憲法](../Page/憲法.md "wikilink")《[邦聯條例](../Page/邦聯條例.md "wikilink")》制定。

### 19世紀

  - [1864年](../Page/1864年.md "wikilink")：[美国南北战争](../Page/美国南北战争.md "wikilink")：北军将领[威廉·谢尔曼焚毁](../Page/威廉·谢尔曼.md "wikilink")[亚特兰大](../Page/亚特兰大.md "wikilink")，发起[向大海进军](../Page/向大海进军.md "wikilink")。
  - [1889年](../Page/1889年.md "wikilink")：[巴西陆军](../Page/巴西.md "wikilink")[元帅](../Page/元帅.md "wikilink")[德奧多羅·達·豐塞卡发起政变](../Page/德奧多羅·達·豐塞卡.md "wikilink")，推翻皇帝[佩德罗二世的统治](../Page/佩德罗二世_\(巴西\).md "wikilink")，建立巴西第一共和国。

### 20世紀

  - [1920年](../Page/1920年.md "wikilink")：[國際聯盟第一次会员大会在](../Page/國際聯盟.md "wikilink")[瑞士](../Page/瑞士.md "wikilink")[日内瓦举行](../Page/日内瓦.md "wikilink")。
  - [1946年](../Page/1946年.md "wikilink")：[中华民国](../Page/中华民国.md "wikilink")[制宪国民大会在](../Page/制宪国民大会.md "wikilink")[南京召开](../Page/南京.md "wikilink")。
  - [1955年](../Page/1955年.md "wikilink")：[日本](../Page/日本.md "wikilink")[自由黨和](../Page/自由黨_\(日本1950年\).md "wikilink")[日本民主黨合併成立](../Page/日本民主黨.md "wikilink")[自由民主黨](../Page/自由民主黨_\(日本\).md "wikilink")，成為日本最大的右翼保守派政黨及長期執政黨。
  - [1966年](../Page/1966年.md "wikilink")：[澳門](../Page/澳門.md "wikilink")[一二·三事件事情起因开始发生](../Page/一二·三事件.md "wikilink")。
  - [1971年](../Page/1971年.md "wikilink")：[美國](../Page/美國.md "wikilink")[英特尔公司发布了全球第一款民用](../Page/英特尔.md "wikilink")[微處理器](../Page/微處理器.md "wikilink")[4004](../Page/Intel_4004.md "wikilink")。
  - 1971年：[中华人民共和国第一次派代表出席](../Page/中华人民共和国.md "wikilink")[联合国大会](../Page/联合国大会.md "wikilink")。
  - [1976年](../Page/1976年.md "wikilink")：《[選擇月刊](../Page/選擇月刊.md "wikilink")》正式創刊，每本定價1[港元](../Page/港元.md "wikilink")。
  - [1983年](../Page/1983年.md "wikilink")：[北賽普勒斯土耳其共和國成立](../Page/北賽普勒斯土耳其共和國.md "wikilink")。
  - [1988年](../Page/1988年.md "wikilink")：[巴勒斯坦全國委員會第](../Page/巴勒斯坦全國委員會.md "wikilink")19次會議在[阿爾及利亞](../Page/阿爾及利亞.md "wikilink")[阿爾及爾闭幕](../Page/阿爾及爾.md "wikilink")，会议宣布建立[巴勒斯坦国](../Page/巴勒斯坦国.md "wikilink")。
  - 1988年：[苏联首架](../Page/苏联.md "wikilink")[航天飞机](../Page/航天飞机.md "wikilink")[暴风雪号从](../Page/暴风雪号航天飞机.md "wikilink")[哈萨克斯坦](../Page/哈萨克斯坦.md "wikilink")[拜科努尔航天中心首次发射升空](../Page/拜科努尔航天中心.md "wikilink")。
  - [1999年](../Page/1999年.md "wikilink")：中国和美國签署关于中国加入[世界贸易组织的双边协议](../Page/世界贸易组织.md "wikilink")。

### 21世紀

  - [2002年](../Page/2002年.md "wikilink")：[中共十六届一中全会选出新一届](../Page/中共十六届一中全会.md "wikilink")[中共中央政治局常委](../Page/中共中央政治局常委.md "wikilink")，[胡锦涛当选](../Page/胡锦涛.md "wikilink")[中国共产党中央委员会总书记](../Page/中国共产党中央委员会总书记.md "wikilink")。
  - [2005年](../Page/2005年.md "wikilink")：[日皇](../Page/日本天皇.md "wikilink")[明仁長女](../Page/明仁.md "wikilink")[紀宮清子出嫁](../Page/黑田清子.md "wikilink")，放棄皇族身分。
  - [2006年](../Page/2006年.md "wikilink")：[日本](../Page/日本.md "wikilink")[北海道以北的俄屬](../Page/北海道.md "wikilink")[千島群島海域發生芮氏規模](../Page/千島群島.md "wikilink")8.1[強震](../Page/2006年千島群島地震.md "wikilink")，日本對周邊區域發佈[海嘯警報及注意報](../Page/海啸.md "wikilink")。
  - [2008年](../Page/2008年.md "wikilink")：[中国](../Page/中国.md "wikilink")[杭州](../Page/杭州.md "wikilink")[地铁1号线](../Page/杭州地铁1号线.md "wikilink")[湘湖站建设工地发生](../Page/湘湖站.md "wikilink")[大规模坍塌事故](../Page/杭州地铁工地坍塌事故.md "wikilink"),造成21人死亡。
  - [2010年](../Page/2010年.md "wikilink")：[中国](../Page/中国.md "wikilink")[上海](../Page/上海.md "wikilink")[胶州路](../Page/胶州路_\(上海\).md "wikilink")718号胶州教师公寓发生[特大火灾](../Page/上海“11·15”特别重大火灾.md "wikilink")，造成58人遇难。
  - [2011年](../Page/2011年.md "wikilink")：[英国商人](../Page/英国.md "wikilink")[尼尔·海伍德在重庆南山丽景度假酒店](../Page/尼尔·海伍德.md "wikilink")[离奇死亡](../Page/海伍德死亡案.md "wikilink")，成为[薄熙来案的导火索](../Page/薄熙来案.md "wikilink")。
  - [2012年](../Page/2012年.md "wikilink")：[中共十八届一中全会选出新一届](../Page/中共十八届一中全会.md "wikilink")[中共中央政治局常委](../Page/中共中央政治局常委.md "wikilink")，[习近平当选](../Page/习近平.md "wikilink")[中共中央总书记和](../Page/中共中央总书记.md "wikilink")[中共中央军委主席](../Page/中共中央军委主席.md "wikilink")。
  - [2016年](../Page/2016年.md "wikilink")：因在[宣誓就职时加入誓词规定外的内容](../Page/香港立法會宣誓風波.md "wikilink")，[梁頌恆和](../Page/梁頌恆.md "wikilink")[游蕙禎被取消](../Page/游蕙禎.md "wikilink")[香港立法會議員資格](../Page/香港立法會議員.md "wikilink")。

## 出生

  - [1316年](../Page/1316年.md "wikilink")：[约翰一世](../Page/约翰一世_\(法兰西\).md "wikilink")，名義上的[法國和](../Page/法國.md "wikilink")[納瓦拉國王](../Page/納瓦拉.md "wikilink")（逝於[1316年](../Page/1316年.md "wikilink")）
  - [1397年](../Page/1397年.md "wikilink")：[尼各老五世](../Page/尼各老五世.md "wikilink")，[羅馬主教教宗](../Page/羅馬主教.md "wikilink")（逝於[1455年](../Page/1455年.md "wikilink")）
  - [1559年](../Page/1559年.md "wikilink")：[阿尔布雷希特七世](../Page/阿尔布雷希特七世.md "wikilink")，[哈布斯堡王朝](../Page/哈布斯堡王朝.md "wikilink")[奧地利首席大公](../Page/奧地利.md "wikilink")、[布拉班特公爵](../Page/布拉班特公爵.md "wikilink")、[低地國家總督和至高無上的統治者](../Page/低地國家.md "wikilink")（逝於[1621年](../Page/1621年.md "wikilink")）
  - [1708年](../Page/1708年.md "wikilink")：[威廉·皮特](../Page/威廉·皮特，第一代查塔姆伯爵.md "wikilink")，[英國首相](../Page/英國.md "wikilink")（逝於[1778年](../Page/1778年.md "wikilink")）
  - [1738年](../Page/1738年.md "wikilink")：[威廉·赫歇爾](../Page/威廉·赫歇爾.md "wikilink")，德裔英國天文學家及音樂家，被譽為「恆星天文學之父」（逝於[1822年](../Page/1822年.md "wikilink")）
  - [1862年](../Page/1862年.md "wikilink")：[格哈特·霍普特曼](../Page/格哈特·霍普特曼.md "wikilink")，[德國劇作家和詩人](../Page/德國.md "wikilink")，1912年獲[諾貝爾文學獎](../Page/諾貝爾文學獎.md "wikilink")（逝於[1946年](../Page/1946年.md "wikilink")）
  - [1874年](../Page/1874年.md "wikilink")：[奧古斯特·克羅](../Page/奧古斯特·克羅.md "wikilink")，[丹麥](../Page/丹麥.md "wikilink")[哥本哈根大學動物生理學系教授](../Page/哥本哈根大學.md "wikilink")，1920年獲[諾貝爾生理學或醫學獎](../Page/諾貝爾生理學或醫學獎.md "wikilink")（逝於[1949年](../Page/1949年.md "wikilink")）
  - [1882年](../Page/1882年.md "wikilink")：[费利克斯·弗兰克福特](../Page/费利克斯·弗兰克福特.md "wikilink")，[奧地利猶太裔](../Page/奧地利.md "wikilink")[美國最高法院大法官](../Page/美國.md "wikilink")（逝於[1965年](../Page/1965年.md "wikilink")）
  - [1886年](../Page/1886年.md "wikilink")：[雷內·格農](../Page/雷內·格農.md "wikilink")，[法國哲學家和隱微論者](../Page/法國.md "wikilink")（逝於[1951年](../Page/1951年.md "wikilink")）
  - [1887年](../Page/1887年.md "wikilink")：[喬治亞·歐姬芙](../Page/喬治亞·歐姬芙.md "wikilink")，[美國女畫家](../Page/美國.md "wikilink")（逝於[1986年](../Page/1986年.md "wikilink")）
  - [1891年](../Page/1891年.md "wikilink")：[埃尔温·隆美尔](../Page/埃尔温·隆美尔.md "wikilink")，[德国陸軍元帥](../Page/德国.md "wikilink")，號稱「沙漠之狐」（逝於[1944年](../Page/1944年.md "wikilink")）
  - 1891年：[W·埃夫里尔·哈里曼](../Page/W·埃夫里尔·哈里曼.md "wikilink")，美國第48任[紐約州州長](../Page/紐約州州長.md "wikilink")（逝於[1986年](../Page/1986年.md "wikilink")）
  - [1895年](../Page/1895年.md "wikilink")：[歐嘉·尼古拉耶芙娜](../Page/歐嘉·尼古拉耶芙娜.md "wikilink")，[俄羅斯帝國末代公主](../Page/俄羅斯.md "wikilink")（逝於[1918年](../Page/1918年.md "wikilink")）
  - [1905年](../Page/1905年.md "wikilink")：[阿农齐奥·保罗·曼托瓦尼](../Page/阿农齐奥·保罗·曼托瓦尼.md "wikilink")，[義大利裔英國輕音樂家](../Page/義大利.md "wikilink")（逝於[1980年](../Page/1980年.md "wikilink")）
  - [1906年](../Page/1906年.md "wikilink")：[柯蒂斯·勒邁](../Page/柯蒂斯·勒邁.md "wikilink")，美國空軍四星上將、參謀長（逝於[1990年](../Page/1990年.md "wikilink")）
  - [1907年](../Page/1907年.md "wikilink")：[克劳斯·冯·施道芬堡](../Page/克劳斯·冯·施道芬堡.md "wikilink")，[納粹德國陸軍上校](../Page/納粹德國.md "wikilink")，暗杀[阿道夫·希特勒行動主要執行人之一](../Page/阿道夫·希特勒.md "wikilink")（逝於[1944年](../Page/1944年.md "wikilink")）
  - [1915年](../Page/1915年.md "wikilink")：[郑周永](../Page/郑周永.md "wikilink")，[韩国民族企业家](../Page/大韩民国.md "wikilink")，政治人物。（逝於[2001年](../Page/2001年.md "wikilink")）
  - [1929年](../Page/1929年.md "wikilink")：[爱德华·阿斯纳](../Page/爱德华·阿斯纳.md "wikilink")，美國影視演員工會前主席
  - [1930年](../Page/1930年.md "wikilink")：[J·G·巴拉德](../Page/J·G·巴拉德.md "wikilink")，[英國小說家](../Page/英國.md "wikilink")
    （逝於[2009年](../Page/2009年.md "wikilink")）
  - [1931年](../Page/1931年.md "wikilink")：[姆瓦伊·齐贝吉](../Page/姆瓦伊·齐贝吉.md "wikilink")，[肯亞總統](../Page/肯亞.md "wikilink")
  - [1932年](../Page/1932年.md "wikilink")：[阿尔文·普兰丁格](../Page/阿尔文·普兰丁格.md "wikilink")，美國基督教哲學家
  - [1937年](../Page/1937年.md "wikilink")：[耶佛·哥圖](../Page/耶佛·哥圖.md "wikilink")，美國演員
  - [1942年](../Page/1942年.md "wikilink")：[丹尼爾·巴倫博伊姆](../Page/丹尼爾·巴倫博伊姆.md "wikilink")，[阿根廷](../Page/阿根廷.md "wikilink")─[以色列](../Page/以色列.md "wikilink")─[西班牙三重國籍鋼琴家](../Page/西班牙.md "wikilink")、指揮家
  - [1946年](../Page/1946年.md "wikilink")：[陳婉嫻](../Page/陳婉嫻.md "wikilink")，[香港政界人物](../Page/香港.md "wikilink")
  - [1947年](../Page/1947年.md "wikilink")：[比尔·理查森](../Page/比尔·理查森.md "wikilink")，美國第30任[新墨西哥州州長](../Page/新墨西哥州.md "wikilink")
  - 1947年：[朴勝泰](../Page/朴勝泰.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1954年](../Page/1954年.md "wikilink")：[趙雅芝](../Page/趙雅芝.md "wikilink")，香港女演員
  - 1954年：[亞歷山大·克瓦希涅夫斯基](../Page/亞歷山大·克瓦希涅夫斯基.md "wikilink")，[波蘭共和國總統](../Page/波蘭共和國.md "wikilink")
  - [1958年](../Page/1958年.md "wikilink")：[姜育恆](../Page/姜育恆.md "wikilink")，[台灣歌手](../Page/台灣歌手.md "wikilink")
  - 1958年：[谷開來](../Page/谷開來.md "wikilink")，[中國北京開來律師事務所](../Page/中國.md "wikilink")、昂道律師事務所前主任
  - [1960年](../Page/1960年.md "wikilink")：[苏珊娜·罗莎](../Page/苏珊娜·罗莎.md "wikilink")，德國演員（逝於[2012年](../Page/2012年.md "wikilink")）
  - [1967年](../Page/1967年.md "wikilink")：[楊羚](../Page/楊羚.md "wikilink")，香港演員
  - 1967年：[何寶生](../Page/何寶生.md "wikilink")，香港演員
  - 1967年：[法蘭索瓦·奧桑](../Page/法蘭索瓦·奧桑.md "wikilink")，法國導演及編劇
  - 1967年：[古斯塔沃·普耶](../Page/古斯塔沃·普耶.md "wikilink")，前[烏拉圭足球運動員](../Page/烏拉圭.md "wikilink")
  - [1969年](../Page/1969年.md "wikilink")：[張崇基](../Page/張崇基.md "wikilink")，[香港歌手](../Page/香港歌手.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink")：[柏特歷·麥保馬](../Page/柏特歷·麥保馬.md "wikilink")，[喀麥隆足球運動員](../Page/喀麥隆.md "wikilink")
  - [1971年](../Page/1971年.md "wikilink")：[杰·哈林顿](../Page/杰·哈林顿.md "wikilink")，美國男演員
  - [1972年](../Page/1972年.md "wikilink")：[吳寶珠](../Page/吳寶珠.md "wikilink")，[越南數學家](../Page/越南.md "wikilink")，2010年[菲爾茲獎得主](../Page/菲爾茲獎.md "wikilink")
  - 1972年：[約翰·李·米勒](../Page/約翰·李·米勒.md "wikilink")，英國演員
  - [1976年](../Page/1976年.md "wikilink")：[張潔蓮](../Page/張潔蓮.md "wikilink")，[香港女演員](../Page/香港.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[彼得·菲利浦斯](../Page/彼得·菲利浦斯.md "wikilink")，香港[蘇格蘭皇家銀行經理](../Page/蘇格蘭皇家銀行.md "wikilink")，[伊利莎伯二世皇孫](../Page/伊利莎伯二世.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[洪天照](../Page/洪天照.md "wikilink")，台灣武打演員
  - 1979年：[趙虹喬](../Page/趙虹喬.md "wikilink")，台灣女演員、歌手
  - 1979年：[荷西美](../Page/荷西美.md "wikilink")，西班牙足球運動員
  - 1979年：[布雷特·兰开斯特](../Page/布雷特·兰开斯特.md "wikilink")，[澳洲自行車運動員](../Page/澳洲.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[王秀琳](../Page/王秀琳.md "wikilink")，[香港女歌手](../Page/香港.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[蘿瑞納·歐秋雅](../Page/蘿瑞納·歐秋雅.md "wikilink")，[墨西哥女子高爾夫球運動員](../Page/墨西哥.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[韓志旼](../Page/韓志旼.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[萨沙·帕夫洛维奇](../Page/萨沙·帕夫洛维奇.md "wikilink")，[蒙特內哥羅職業籃球運動員](../Page/蒙特內哥羅.md "wikilink")
  - 1983年：[費爾南多·貝爾達斯科](../Page/費爾南多·貝爾達斯科.md "wikilink")，西班牙職業網球運動員
  - [1984年](../Page/1984年.md "wikilink")：[杰玛·阿特金森](../Page/杰玛·阿特金森.md "wikilink")，英國模特兒、足球寶貝、肥皂劇女演員
  - [1986年](../Page/1986年.md "wikilink")：[萨尼娅·米尔扎](../Page/萨尼娅·米尔扎.md "wikilink")，[印度職業網球運動員](../Page/印度.md "wikilink")
  - 1986年：[蔡明芳](../Page/蔡明芳.md "wikilink")（Jessica），香港女子組合[Super
    Girls成員](../Page/Super_Girls#.E7.8F.BE.E4.BB.BB.E6.88.90.E5.93.A1.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[蔡黃汝](../Page/蔡黃汝.md "wikilink")，台灣歌手
  - 1987年：[伊塞亞·奧斯邦尼](../Page/伊塞亞·奧斯邦尼.md "wikilink")，英國職業足球員
  - [1988年](../Page/1988年.md "wikilink")：[B.o.B](../Page/B.o.B.md "wikilink")，美國音樂家及音樂製作人
  - [1989年](../Page/1989年.md "wikilink")：[横山美雪](../Page/横山美雪.md "wikilink")，[日本](../Page/日本.md "wikilink")[AV女优](../Page/AV女优.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[本鄉奏多](../Page/本鄉奏多.md "wikilink")，日本男演員
  - 1990年：[羅莉克·斯洛積斯](../Page/羅莉克·斯洛積斯.md "wikilink")，[荷蘭女子排球運動員](../Page/荷蘭.md "wikilink")
  - [1992年](../Page/1992年.md "wikilink")：[峯岸南](../Page/峯岸南.md "wikilink")，日本女子偶像團體[AKB48成員](../Page/AKB48.md "wikilink")、女演員
  - [1993年](../Page/1993年.md "wikilink")：[紗綾](../Page/紗綾.md "wikilink")，日本女寫真偶像
  - 1993年：[保羅·戴巴拿](../Page/保羅·戴巴拿.md "wikilink")，阿根廷職業足球員
  - 1993年：[張真詠](../Page/張真詠.md "wikilink")，[韓國女子偶像團體](../Page/韓國.md "wikilink")[Ela8te成員](../Page/Ela8te.md "wikilink")

## 逝世

  - [1630年](../Page/1630年.md "wikilink")：[约翰内斯·开普勒](../Page/约翰内斯·开普勒.md "wikilink")，德国天文学家（[1571年出生](../Page/1571年.md "wikilink")）
  - [1908年](../Page/1908年.md "wikilink")：[慈禧太后](../Page/慈禧太后.md "wikilink")，[清文宗](../Page/清文宗.md "wikilink")[咸丰皇帝妃子](../Page/咸丰皇帝.md "wikilink")，[清穆宗](../Page/清穆宗.md "wikilink")[同治皇帝生母](../Page/同治皇帝.md "wikilink")（[1835年出生](../Page/1835年.md "wikilink")）
  - [2002年](../Page/2002年.md "wikilink")：[孫基禎](../Page/孫基禎.md "wikilink")，[1936年柏林奧運會男子](../Page/1936年夏季奥林匹克运动会.md "wikilink")[馬拉松金牌得主](../Page/马拉松.md "wikilink")（[1912年出生](../Page/1912年.md "wikilink")）
  - [2003年](../Page/2003年.md "wikilink")：[林同炎](../Page/林同炎.md "wikilink")，中國桥梁大师（[1912年出生](../Page/1912年.md "wikilink")）
  - [2006年](../Page/2006年.md "wikilink")：[荣高棠](../Page/荣高棠.md "wikilink")，[中國體育界元老](../Page/中华人民共和国.md "wikilink")（[1912年出生](../Page/1912年.md "wikilink")）
  - 2007年：[梁洪](../Page/梁洪.md "wikilink")：[中国人民武装警察部队副司令员](../Page/中国人民武装警察部队.md "wikilink")（[1946年出生](../Page/1946年.md "wikilink")）
  - [2011年](../Page/2011年.md "wikilink")：[王偉](../Page/王偉_\(香港\).md "wikilink")，[香港演員](../Page/香港.md "wikilink")（[1939年出生](../Page/1939年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[格拉夫科斯·克萊里季斯](../Page/格拉夫科斯·克莱里季斯.md "wikilink")，賽普勒斯共和國第4任總統（[1919年出生](../Page/1919年.md "wikilink")）

## 节假日和习俗

：[七五三節](../Page/七五三節.md "wikilink")，也常被視作日本兒童節。

## 參考資料