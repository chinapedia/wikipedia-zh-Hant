**11月29日**是[公历一年中的第](../Page/公历.md "wikilink")333天（[闰年第](../Page/闰年.md "wikilink")334天），离全年结束还有32天。

## 大事记

### 11世紀

  - [1069年](../Page/1069年.md "wikilink")：[宋神宗颁布](../Page/宋神宗.md "wikilink")[农田水利法](../Page/农田水利法.md "wikilink")。

### 14世紀

  - [1394年](../Page/1394年.md "wikilink")：[李成桂將](../Page/李成桂.md "wikilink")[都城從](../Page/都城.md "wikilink")[開京遷移到了](../Page/開京.md "wikilink")[漢陽](../Page/漢陽.md "wikilink")，並正式命名為[漢城](../Page/漢城.md "wikilink")。

### 16世紀

  - [1580年](../Page/1580年.md "wikilink")：探险家[法蘭西斯·德瑞克爵士完成环球航行回到](../Page/法蘭西斯·德瑞克.md "wikilink")[英国](../Page/英国.md "wikilink")。

### 18世紀

  - [1716年](../Page/1716年.md "wikilink")：《[古今图书汇编](../Page/古今图书集成.md "wikilink")》初稿完成。该书由[陈梦雷等主编](../Page/陳夢雷.md "wikilink")，[雍正年间再一次修订](../Page/雍正帝.md "wikilink")，改名为《[古今图书集成](../Page/古今图书集成.md "wikilink")》。
  - [1777年](../Page/1777年.md "wikilink"):
    [西班牙美洲殖民地在](../Page/西班牙美洲殖民地.md "wikilink")[墨西哥成立上](../Page/墨西哥.md "wikilink")[加利福尼亞省第一座農業城鎮](../Page/加利福尼亞省.md "wikilink")[聖荷西](../Page/聖荷西.md "wikilink")。

### 19世紀

  - [1845年](../Page/1845年.md "wikilink")：[英国驻](../Page/英国.md "wikilink")[上海领事](../Page/上海.md "wikilink")[巴富尔与上海道台](../Page/巴富尔.md "wikilink")[宫慕久议定](../Page/宫慕久.md "wikilink")《[上海租地章程](../Page/上海租地章程.md "wikilink")》公布，设立外国在华的第一个[租界](../Page/租界.md "wikilink")。
  - [1847年](../Page/1847年.md "wikilink")：[共产主义者同盟第二次代表大会召开](../Page/共产主义者同盟.md "wikilink")。
  - [1877年](../Page/1877年.md "wikilink")：[美国发明家](../Page/美国.md "wikilink")[托马斯·爱迪生首次展示他发明的世界首台](../Page/托马斯·爱迪生.md "wikilink")[留声机](../Page/留声机.md "wikilink")。
  - [1880年](../Page/1880年.md "wikilink")：[日本首届内阁会议召开](../Page/日本.md "wikilink")。
  - [1890年](../Page/1890年.md "wikilink")：[日本首部](../Page/日本.md "wikilink")[宪法](../Page/宪法.md "wikilink")《[大日本帝国宪法](../Page/大日本帝国宪法.md "wikilink")》正式实施，採取[兩院制的最高](../Page/兩院制.md "wikilink")[立法機構](../Page/立法機構.md "wikilink")[帝國議會召開首次會議](../Page/帝國議會.md "wikilink")。。
  - [1893年](../Page/1893年.md "wikilink")：[清朝](../Page/清朝.md "wikilink")[湖广总督](../Page/湖广总督.md "wikilink")[张之洞奏请](../Page/张之洞.md "wikilink")[光绪皇帝创立](../Page/光绪皇帝.md "wikilink")[武汉大学的前身](../Page/武汉大学.md "wikilink")[自强学堂](../Page/自强学堂.md "wikilink")。
  - [1899年](../Page/1899年.md "wikilink")：[瑞士](../Page/瑞士.md "wikilink")[企業家](../Page/企業家.md "wikilink")[胡安·甘伯在](../Page/胡安·甘伯.md "wikilink")[西班牙](../Page/西班牙.md "wikilink")[加泰隆尼亞](../Page/加泰隆尼亞.md "wikilink")[巴塞隆納成立](../Page/巴塞隆納.md "wikilink")[-{zh-hans:巴塞罗那足球俱乐部;
    zh-tw:巴塞隆納足球俱樂部; zh-hk:巴塞隆拿足球會;
    zh-mo:巴塞羅那足球會;}-](../Page/巴塞罗那足球俱乐部.md "wikilink")，為[歐洲乃至於全球最成功和受歡迎的體育俱樂部之一](../Page/歐洲.md "wikilink")。

### 20世紀

  - [1909年](../Page/1909年.md "wikilink")：俄国小说家[高尔基以](../Page/马克西姆·高尔基.md "wikilink")“[小资产阶级享乐主义](../Page/小资.md "wikilink")”的罪名被革命党开除党籍。
  - [1911年](../Page/1911年.md "wikilink"):[外蒙古](../Page/外蒙古.md "wikilink")[博克多格根宣布](../Page/博克多格根.md "wikilink")[外蒙古獨立](../Page/外蒙古獨立.md "wikilink")。
  - [1929年](../Page/1929年.md "wikilink")：美国海军中校[理查德·伯德](../Page/理查德·伯德.md "wikilink")（Richard
    E. Byrd）首次飞越[南极](../Page/南极.md "wikilink")。
  - [1940年](../Page/1940年.md "wikilink")：[第二次世界大战中](../Page/第二次世界大战.md "wikilink")，[納粹德國飞机开始轰炸](../Page/納粹德國.md "wikilink")[伦敦](../Page/伦敦.md "wikilink")。
  - [1942年](../Page/1942年.md "wikilink")：[波士顿夜总会大火](../Page/波士顿.md "wikilink")300人丧生。
  - [1944年](../Page/1944年.md "wikilink")：[阿尔巴尼亚全国解放](../Page/阿尔巴尼亚.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：[南斯拉夫联邦人民共和国宣告成立](../Page/南斯拉夫联邦人民共和国.md "wikilink")。
  - [1947年](../Page/1947年.md "wikilink")：[联合国大会通过](../Page/联合国大会.md "wikilink")[第181号决议](../Page/联合国大会181号决议.md "wikilink")，将[巴勒斯坦地区分为一个](../Page/巴勒斯坦.md "wikilink")[犹太人国家和一个](../Page/犹太人.md "wikilink")[阿拉伯人国家](../Page/阿拉伯人.md "wikilink")。
  - [1948年](../Page/1948年.md "wikilink")：[中國人民解放軍部隊包圍由](../Page/中國人民解放軍.md "wikilink")[中華民國國軍駐防的](../Page/中華民國國軍.md "wikilink")[河北省](../Page/河北省.md "wikilink")[張家口](../Page/張家口.md "wikilink")，[平津戰役爆發](../Page/平津戰役.md "wikilink")。
  - [1951年](../Page/1951年.md "wikilink")：[屏·春哈旺发动](../Page/屏·春哈旺.md "wikilink")“平静的自我政变”解散原[銮披汶·颂堪政府](../Page/銮披汶·颂堪.md "wikilink")，中止1949年宪法，恢复1932年宪法，銮披汶·颂堪再次被推举为[泰國总理](../Page/泰國总理.md "wikilink")，維持軍事獨裁統治
  - [1951年](../Page/1951年.md "wikilink")：美国进行了第一次地下[原子弹爆破试验](../Page/原子弹.md "wikilink")。
  - [1953年](../Page/1953年.md "wikilink")：[法国伞兵部队在](../Page/法国.md "wikilink")[老挝边境着陆](../Page/老挝.md "wikilink")。
  - [1960年](../Page/1960年.md "wikilink")：德国科学家[马乐丁·斯特雷和美国科学家](../Page/马乐丁·斯特雷.md "wikilink")[罗伯特·伯恩斯·伍德沃德成功合成](../Page/罗伯特·伯恩斯·伍德沃德.md "wikilink")[叶绿素](../Page/叶绿素.md "wikilink")。
  - [1961年](../Page/1961年.md "wikilink")：[中国](../Page/中华人民共和国.md "wikilink")、[北越](../Page/北越.md "wikilink")、[朝鲜因](../Page/朝鲜民主主义人民共和国.md "wikilink")[阿尔巴尼亚问题与](../Page/阿尔巴尼亚问题.md "wikilink")[苏联公开决裂](../Page/苏联.md "wikilink")。
  - [1963年](../Page/1963年.md "wikilink")：[林登·约翰逊总统下令成立](../Page/林登·约翰逊.md "wikilink")[沃伦委员会调查](../Page/厄尔·沃伦.md "wikilink")[肯尼迪遇刺事件](../Page/约翰·肯尼迪.md "wikilink")。
  - [1963年](../Page/1963年.md "wikilink")：[环加拿大航空831号班机空难](../Page/环加拿大航空831号班机空难.md "wikilink")。
  - [1967年](../Page/1967年.md "wikilink")：[南也门人民共和国成立](../Page/也门民主人民共和国.md "wikilink")。
  - [1972年](../Page/1972年.md "wikilink"):
    [美國](../Page/美國.md "wikilink")[電腦公司](../Page/電腦公司.md "wikilink")[雅達利發表](../Page/雅達利.md "wikilink")[街機遊戲](../Page/街機遊戲.md "wikilink")《[乓](../Page/乓.md "wikilink")》，為首個在[電子遊樂器市場獲得成功的](../Page/電子遊樂器.md "wikilink")[遊戲作品](../Page/遊戲.md "wikilink")。
  - [1973年](../Page/1973年.md "wikilink")：[日本](../Page/日本.md "wikilink")[熊本百货公司发生大火](../Page/熊本百货公司.md "wikilink")，100多人丧生。
  - 1973年：中国[甘肃发现完整](../Page/甘肃.md "wikilink")[剑齿象](../Page/剑齿象.md "wikilink")[化石](../Page/化石.md "wikilink")。
  - [1975年](../Page/1975年.md "wikilink")：[比尔·盖茨与](../Page/比尔·盖茨.md "wikilink")[保罗·艾伦将他们共同创立的公司命名为](../Page/保罗·艾伦.md "wikilink")“[微软](../Page/微软.md "wikilink")”。
  - 1975年：中国第一次[回收卫星成功](../Page/回收卫星.md "wikilink")，这颗卫星是同年[11月26日发射的](../Page/11月26日.md "wikilink")。\[1\]
  - 1975年：[九廣鐵路舊](../Page/九廣鐵路.md "wikilink")[尖沙咀九龍車站結束歷史使命後關閉](../Page/尖沙咀站_\(九廣鐵路英段\).md "wikilink")，然後于1978年拆卸。舊址現建成[香港太空館](../Page/香港太空館.md "wikilink")、[香港文化中心](../Page/香港文化中心.md "wikilink")、[香港藝術館等現代建築](../Page/香港藝術館.md "wikilink")。而火車站旁的[鐘樓就屹立至今](../Page/尖沙咀鐘樓.md "wikilink")。
  - [1977年](../Page/1977年.md "wikilink")：聯合國將每年的11月29日定為"[世界聲援巴勒斯坦日](../Page/世界聲援巴勒斯坦日.md "wikilink")"
  - [1979年](../Page/1979年.md "wikilink")：[台湾](../Page/台湾.md "wikilink")《[美丽岛杂志](../Page/美丽岛杂志.md "wikilink")》[高雄服务处遭人砸毁](../Page/高雄市.md "wikilink")。
  - [1986年](../Page/1986年.md "wikilink")：中国[湖北为一对共肝连体女婴分离手术成功](../Page/湖北.md "wikilink")。
  - 1986年：中国第一座低温核供热模式堆在北京开始动工。
  - [1987年](../Page/1987年.md "wikilink")：[大韩航空](../Page/大韩航空.md "wikilink")[858号班机因被两名](../Page/大韩航空858号班机空难.md "wikilink")[朝鲜](../Page/朝鲜.md "wikilink")[特工安放了](../Page/特工.md "wikilink")[炸弹而在](../Page/炸弹.md "wikilink")[安達曼海的上空发生](../Page/安達曼海.md "wikilink")[爆炸](../Page/爆炸.md "wikilink")，机上115名人全部遇难。
  - [1989年](../Page/1989年.md "wikilink")：[羅馬尼亞](../Page/羅馬尼亞.md "wikilink")[奧運會](../Page/奧運會.md "wikilink")[體操金牌名將](../Page/競技體操.md "wikilink")[納迪婭·柯曼妮奇投奔](../Page/納迪婭·柯曼妮奇.md "wikilink")[匈牙利](../Page/匈牙利.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：[武汉大学隆重举行百年校庆](../Page/武汉大学.md "wikilink")，是中国第一所庆祝建校100周年的高等院校。
  - 1993年：[香港首位華人](../Page/香港.md "wikilink")[布政司](../Page/布政司.md "wikilink")[陳方安生正式履新](../Page/陳方安生.md "wikilink")。
  - [1998年](../Page/1998年.md "wikilink")：[瑞士公民投票](../Page/瑞士.md "wikilink")，反对[海洛因等](../Page/海洛因.md "wikilink")[毒品合法化](../Page/毒品.md "wikilink")。
  - [1999年](../Page/1999年.md "wikilink")：[世界貿易組織第三次部長級會議在](../Page/世界貿易組織第三次部長級會議.md "wikilink")[西雅圖舉行](../Page/西雅圖.md "wikilink")，众多[非政府組織及](../Page/非政府组织.md "wikilink")[反全球化人士在場外發動多次示威遊行](../Page/反全球化.md "wikilink")，造成嚴重衝突。

### 21世紀

  - [2002年](../Page/2002年.md "wikilink")：[香港公立醫院](../Page/香港公立醫院.md "wikilink")[急症室開始收費](../Page/急症室.md "wikilink")。
  - [2012年](../Page/2012年.md "wikilink")：[聯合國大會投票通過](../Page/聯合國大會.md "wikilink")，將[巴勒斯坦由](../Page/巴勒斯坦.md "wikilink")[觀察員實體提升為觀察員國](../Page/聯合國大會觀察員列表.md "wikilink")。\[2\]\[3\]
  - [2014年](../Page/2014年.md "wikilink")：[臺灣地方選舉](../Page/2014年中華民國地方公職人員選舉.md "wikilink")
    (九合一選舉)，[中國國民黨慘敗](../Page/中國國民黨.md "wikilink")\[4\]。國民黨僅贏得6個縣市長席次，但維持對17個縣市議會的控制權。
  - [2016年](../Page/2016年.md "wikilink")：[巴西甲組足球聯賽球會](../Page/巴西甲組足球聯賽.md "wikilink")[查比高恩斯在](../Page/查比高恩斯.md "wikilink")[玻利維亞乘坐客機前往](../Page/玻利維亞.md "wikilink")[哥倫比亞備戰](../Page/哥倫比亞.md "wikilink")2016年[南美球會盃決賽首回合途中發生空難](../Page/南美球會盃.md "wikilink")，造成最少71人死亡、6人受傷。

## 出生

[Cdoppler.jpg](https://zh.wikipedia.org/wiki/File:Cdoppler.jpg "fig:Cdoppler.jpg")
[John_Ambrose_Fleming_1890.png](https://zh.wikipedia.org/wiki/File:John_Ambrose_Fleming_1890.png "fig:John_Ambrose_Fleming_1890.png")

  - [1427年](../Page/1427年.md "wikilink")：[明英宗](../Page/明英宗.md "wikilink")，明朝第六代皇帝（逝於1464年）
  - [1488年](../Page/1488年.md "wikilink")：[朝鮮中宗](../Page/朝鮮中宗.md "wikilink")，朝鮮王朝第11代國王（逝於1544年）
  - [1627年](../Page/1627年.md "wikilink")：[约翰·雷](../Page/约翰·雷.md "wikilink")，英國博物學之父（逝於1705年）
  - [1797年](../Page/1797年.md "wikilink")：[-{zh-hans:葛塔诺·多尼采蒂;
    zh-hant:葛塔諾·董尼采第;}-](../Page/葛塔諾·多尼采蒂.md "wikilink")，意大利歌劇作曲家（逝於1848年）
  - [1803年](../Page/1803年.md "wikilink")：[-{zh-hans:克里斯琴·多普勒;
    zh-hant:克里斯蒂安·都卜勒;}-](../Page/克里斯琴·多普勒.md "wikilink")，奥地利數學家、物理學家（逝於1853年）
  - [1816年](../Page/1816年.md "wikilink")：[莫里森·韦特](../Page/莫里森·韦特.md "wikilink")，美國最高法院首席大法官（逝於1888年）
  - [1825年](../Page/1825年.md "wikilink")：[让-马丁·沙可](../Page/让-马丁·沙可.md "wikilink")，法國神經學家、解剖病理學教授（逝於1893年）
  - [1832年](../Page/1832年.md "wikilink")：[露依莎·奧爾柯特](../Page/露依莎·奧爾柯特.md "wikilink")，美國女作家，《小婦人》作者（逝於1888年）
  - [1849年](../Page/1849年.md "wikilink")：[约翰·弗莱明](../Page/约翰·弗莱明.md "wikilink")，英國電力工程師和物理學家（逝於1945年）
  - [1856年](../Page/1856年.md "wikilink")：[特奥巴登·冯·贝特曼-霍尔维格](../Page/特奥巴登·冯·贝特曼-霍尔维格.md "wikilink")，德意志帝國總理（逝於1921年）
  - [1874年](../Page/1874年.md "wikilink")：[安东尼奥·埃加斯·莫尼斯](../Page/安东尼奥·埃加斯·莫尼斯.md "wikilink")，葡萄牙神经学家，1949年诺贝尔生理学或医学奖得主（逝於1955年）
  - [1882年](../Page/1882年.md "wikilink")：[亨利·法布尔](../Page/亨利·法布尔.md "wikilink")，法國飛行員、史上第一架水上飛機發明者（逝於1984年）
  - [1898年](../Page/1898年.md "wikilink")：[C·S·路易斯](../Page/C·S·路易斯.md "wikilink")，威爾斯裔英國作家及護教家（逝於1963年）
  - [1899年](../Page/1899年.md "wikilink")：[艾瑪·莫拉諾](../Page/艾瑪·莫拉諾.md "wikilink")，義大利超級人瑞暨前任在世最長壽者（逝於2017年）
  - [1917年](../Page/1917年.md "wikilink")：[侯宝林](../Page/侯宝林.md "wikilink")，中国著名相声大师（逝於1993年）
  - [1918年](../Page/1918年.md "wikilink"):
    [馬德琳·恩格爾](../Page/馬德琳·恩格爾.md "wikilink"),
    美國作家, 《時間的皺紋》作者（逝於2007年）
  - [1931年](../Page/1931年.md "wikilink")：[勝新太郎](../Page/勝新太郎.md "wikilink")，日本演員、歌手（逝於1997年）
  - [1932年](../Page/1932年.md "wikilink")：[-{zh-hans:雅克·希拉克;zh-hk:雅克·希拉克;zh-tw:雅各·席哈克;}-](../Page/雅克·希拉克.md "wikilink")，第22任法国总统
  - [1953年](../Page/1953年.md "wikilink")：[亚历斯·格列](../Page/亚历斯·格列.md "wikilink")，美國新時代靈幻藝術家
  - [1954年](../Page/1954年.md "wikilink")：[邢峰](../Page/邢峰.md "wikilink")，台灣秀場主持人
  - [1954年](../Page/1954年.md "wikilink")：[喬伊·柯恩](../Page/科恩兄弟.md "wikilink")，美國電影導演、編劇、製片
  - [1956年](../Page/1956年.md "wikilink")：[徐亨](../Page/徐亨.md "wikilink")，台灣演員、歌手兼主持人
  - [1957年](../Page/1957年.md "wikilink")：[珍妮特·納波利塔諾](../Page/珍妮特·納波利塔諾.md "wikilink")，第3任美國國土安全部部長
  - [1959年](../Page/1959年.md "wikilink")：[拉姆·伊曼纽尔](../Page/拉姆·伊曼纽尔.md "wikilink")，第55任美國芝加哥市長
  - [1962年](../Page/1962年.md "wikilink")：[安德魯·麥卡錫](../Page/安德魯·麥卡錫.md "wikilink")，[美國男演員](../Page/美國.md "wikilink")
  - [1965年](../Page/1965年.md "wikilink")：[尾崎豐](../Page/尾崎豐.md "wikilink")，[日本歌手](../Page/日本.md "wikilink")、作詞家、作曲家（逝於[1992年](../Page/1992年.md "wikilink")）
  - [1969年](../Page/1969年.md "wikilink")：[馬里安諾·李維拉](../Page/馬里安諾·李維拉.md "wikilink")，[美國職棒紐約洋基終結者](../Page/美國.md "wikilink")
  - [1969年](../Page/1969年.md "wikilink")：[皮埃尔·范霍伊东克](../Page/皮埃尔·范霍伊东克.md "wikilink")，[荷蘭足球運動員](../Page/荷蘭.md "wikilink")
  - [1971年](../Page/1971年.md "wikilink")：[吉娜·李·诺琳](../Page/吉娜·李·诺琳.md "wikilink")，[美國女演員](../Page/美國.md "wikilink")、模特兒
  - [1973年](../Page/1973年.md "wikilink")：[-{zh-hans:瑞恩·吉格斯;zh-hk:賴恩·傑斯;zh-tw:瑞恩·吉格斯;}-](../Page/瑞恩·吉格斯.md "wikilink")，威爾斯足球員
  - [1974年](../Page/1974年.md "wikilink")：[林志玲](../Page/林志玲.md "wikilink")，[台灣女模特兒](../Page/台灣.md "wikilink")、主持人、演員
  - [1975年](../Page/1975年.md "wikilink")：[鄭立鍵](../Page/鄭立鍵.md "wikilink")，[台灣創業家](../Page/台灣.md "wikilink")，《金鑛咖啡連鎖》創辦人
  - [1976年](../Page/1976年.md "wikilink")：[安娜·法瑞絲](../Page/安娜·法瑞絲.md "wikilink")，美國女演員，《驚聲尖笑》女主角
  - [1977年](../Page/1977年.md "wikilink")：[余皚磊](../Page/余皚磊.md "wikilink")，[中國內地男演員](../Page/中國.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[申東美](../Page/申東美.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1978年](../Page/1978年.md "wikilink")：[劳伦·日尔曼](../Page/劳伦·日尔曼.md "wikilink")，[美國女演員](../Page/美國.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[Game](../Page/Game_\(歌手\).md "wikilink")，[美國說唱歌手](../Page/美國.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[千正明](../Page/千正明.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[張棟樑](../Page/張棟樑.md "wikilink")，[马来西亚男歌手](../Page/马来西亚.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[法瓦·阿夫扎·罕](../Page/法瓦·阿夫扎·罕.md "wikilink")，[巴基斯坦男藝人](../Page/巴基斯坦.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[徐子淇](../Page/徐子淇.md "wikilink")，[香港女模特兒](../Page/香港.md "wikilink")、電影演員
  - [1982年](../Page/1982年.md "wikilink")：[克莉絲朵·史迪爾](../Page/克莉絲朵·史迪爾.md "wikilink")，[美國女色情演員](../Page/美國.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[智鉉寓](../Page/智鉉寓.md "wikilink")（池賢宇），[韓國男演員](../Page/韓國.md "wikilink")、歌手
  - [1985年](../Page/1985年.md "wikilink")：[田口淳之介](../Page/田口淳之介.md "wikilink")，[日本男歌手](../Page/日本.md "wikilink")，KAT-TUN成員
  - [1985年](../Page/1985年.md "wikilink")：[-{zh-hans:香农·布朗;zh-hant:沙朗·布朗}-](../Page/香农·布朗.md "wikilink")，[美國職業籃球運動員](../Page/美國.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[吳綺珊](../Page/吳綺珊.md "wikilink")，[香港女演員](../Page/香港.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[史提芬·奧哈洛蘭](../Page/史提芬·奧哈洛蘭.md "wikilink")，[愛爾蘭足球運動員](../Page/愛爾蘭.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[李旼赫](../Page/李旼赫.md "wikilink")，[韓國男子團體](../Page/韓國.md "wikilink")[BTOB](../Page/BTOB.md "wikilink")，歌手
  - [1991年](../Page/1991年.md "wikilink")：[高柳明音](../Page/高柳明音.md "wikilink")，[日本女子偶像團體](../Page/日本.md "wikilink")，SKE48
    TEAM KII成員
  - [1995年](../Page/1995年.md "wikilink")：[阿部瑪利亞](../Page/阿部マリア.md "wikilink")，[日本女子偶像團體](../Page/日本.md "wikilink")，AKB48
    TEAM K成員

## 逝世

  - [741年](../Page/741年.md "wikilink")：[額我略三世](../Page/額我略三世.md "wikilink")，教宗（就任於731年）

  - [1314年](../Page/1314年.md "wikilink")：[腓力四世](../Page/腓力四世_\(法蘭西\).md "wikilink")，法国国王（生於1268年）

  -
  - [1780年](../Page/1780年.md "wikilink")：[玛丽娅·特蕾西娅](../Page/玛丽娅·特蕾西娅.md "wikilink")，神圣罗马帝国皇后（生於1717年）

  -
  - [1924年](../Page/1924年.md "wikilink")：[贾科莫·普契尼](../Page/贾科莫·普契尼.md "wikilink")，意大利歌剧作曲家（生於1858年）

  - [1927年](../Page/1927年.md "wikilink")：[吳昌碩](../Page/吳昌碩.md "wikilink")，中国近代书画家（生於1844年）

  - [1931年](../Page/1931年.md "wikilink")：[邓演达](../Page/邓演达.md "wikilink")，中国前國民革命軍總司令部政治部主任（生於1895年）

  - [1941年](../Page/1941年.md "wikilink")：[卓娅](../Page/卓娅.md "wikilink")，苏联共青团团员（生於1923年）

  - [1950年](../Page/1950年.md "wikilink")：[杨根思](../Page/杨根思.md "wikilink")，中国人民解放军戰鬥英雄、中國人民志願軍烈士（生於1922年）

  - 1950年：[马占山](../Page/马占山.md "wikilink")，中国著名抗日將領（生於1885年）

  - [1971年](../Page/1971年.md "wikilink")：[郭育栽](../Page/郭育栽.md "wikilink")，越南反共中文报《成功日报》总裁（生於1922年）

  - [1974年](../Page/1974年.md "wikilink")：[彭德怀](../Page/彭德怀.md "wikilink")，中国著名军事家（生於1898年）

  - [1981年](../Page/1981年.md "wikilink")

      - [娜妲麗華](../Page/娜妲麗華.md "wikilink")，美国著名女影星，加州海邊溺斃（生於1938年）
      - [威廉·韋爾](../Page/威廉·韋爾.md "wikilink")，[英國](../Page/英國.md "wikilink")[王牌飛行員](../Page/王牌飛行員.md "wikilink")（生於1914年）

  - [1986年](../Page/1986年.md "wikilink")：[加里·格兰特](../Page/加里·格兰特.md "wikilink")，美国影星（生於1904年）

  - [1989年](../Page/1989年.md "wikilink")：[任劍輝](../Page/任劍輝.md "wikilink")，香港著名粵劇演員（生於1913年）

  - [1994年](../Page/1994年.md "wikilink")：[孔繁森](../Page/孔繁森.md "wikilink")，前西藏自治區拉薩市副市長、阿里地區黨委書記（生於1944年）

  - [2000年](../Page/2000年.md "wikilink")：[李汶靜](../Page/李汶靜.md "wikilink")，香港新聞從業員（生於1957年）

  - [2001年](../Page/2001年.md "wikilink")：[乔治哈里森](../Page/乔治哈里森.md "wikilink")，披頭四樂隊成員（生於1943年）

  - [2002年](../Page/2002年.md "wikilink")：[家永三郎](../Page/家永三郎.md "wikilink")，日本著名的历史学家，對日本進行的侵略戰爭進行批判（生於1913年）

  - [2009年](../Page/2009年.md "wikilink")：[卡爾·佩格勞](../Page/卡爾·佩格勞.md "wikilink")，[德國](../Page/德國.md "wikilink")[交通心理學家及工程師](../Page/交通心理學.md "wikilink")、[交通號誌小人創造者](../Page/交通號誌小人.md "wikilink")（生於1927年）

  - [2011年](../Page/2011年.md "wikilink")：[大介](../Page/大介.md "wikilink")，日本著名的狗明星，因胃倒轉手術後而死（生於2005年）

## 节日、风俗习惯

  - [国际团结巴勒斯坦人民日](../Page/国际团结巴勒斯坦人民日.md "wikilink")
  - 阿尔巴尼亚国庆日

## 參考文獻

1.
2.  [General Assembly grants Palestine non-member observer State status
    at
    UN](http://www.un.org/apps/news/story.asp?NewsID=43640&Cr=palestin&Cr1=#.ULf2CPtj2Ak)
3.  [聯合國投票通過巴勒斯坦為觀察員國 - BBC中文網 -
    國際新聞](http://www.bbc.co.uk/zhongwen/trad/world/2012/11/121129_un_palestine_vote.shtml)
4.