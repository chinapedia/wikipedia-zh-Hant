**125街車站**（），曾被名為**曼哈頓街**（），是[紐約地鐵](../Page/紐約地鐵.md "wikilink")[IRT百老匯-第七大道線一個慢車](../Page/IRT百老匯-第七大道線.md "wikilink")[地鐵站](../Page/地鐵站.md "wikilink")，位於[曼哈頓](../Page/曼哈頓.md "wikilink")[曼哈頓維爾](../Page/曼哈頓維爾.md "wikilink")（[晨邊高地與](../Page/晨邊高地.md "wikilink")[哈萊姆區附近](../Page/哈萊姆區.md "wikilink")）及[百老匯交界](../Page/百老匯.md "wikilink")，設有[1號線](../Page/紐約地鐵1號線.md "wikilink")（任何時候停站）列車服務。

## 車站結構

<table>
<tbody>
<tr class="odd">
<td><p><strong>P<br />
月台層</strong></p></td>
<td><p><small><a href="../Page/側式月台.md" title="wikilink">側式月台</a>，右側開門</small></p></td>
</tr>
<tr class="even">
<td><p>&lt;span style=color:#&gt;<strong>北行</strong></span></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-1.svg" title="fig:纽约地铁1号线">纽约地铁1号线</a> 往<a href="../Page/范科特蘭公園-242街車站_(IRT百老匯-第七大道線).md" title="wikilink">范科特蘭公園-242街</a><small>（<a href="../Page/137街-市立學院車站_(IRT百老匯-第七大道線).md" title="wikilink">137街</a>）</small></p></td>
</tr>
<tr class="odd">
<td><p><span style="color:#{{NYCS color|black}}"><strong><em>尖峰快車</em></strong></span></p></td>
<td><p>無定期服務</p></td>
</tr>
<tr class="even">
<td><p>&lt;span style=color:#&gt;<strong>南行</strong></span></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-1.svg" title="fig:纽约地铁1号线">纽约地铁1号线</a> 往<a href="../Page/南碼頭-白廳街車站.md" title="wikilink">南碼頭</a><small>（<a href="../Page/116街-哥倫比亞大學車站_(IRT百老匯-第七大道線).md" title="wikilink">116街</a>）</small></p></td>
</tr>
<tr class="odd">
<td><p><small><a href="../Page/側式月台.md" title="wikilink">側式月台</a>，右側開門</small></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>M</strong></p></td>
<td><p>夾層</p></td>
</tr>
<tr class="odd">
<td><p><strong>G</strong></p></td>
<td><p>街道</p></td>
</tr>
</tbody>
</table>

此站是曼哈頓峽谷高架橋唯一的車站，由122街起跨過[曼哈頓維爾到](../Page/曼哈頓維爾.md "wikilink")135街並自1983年起成為[國家史蹟名錄](../Page/國家史蹟名錄.md "wikilink")\[1\]。此高架橋容許列車以相對較平坦避免陡削的坡度通過峽谷。總長2174英呎，跨過125街的鋼拱橋全長168.5英呎\[2\]。

此站作為原初地鐵的一部分，擁有兩個[側式月台和三條軌道](../Page/側式月台.md "wikilink")。中央軌道不營運。

## 參考資料

## 外部連結

  -
  - nycsubway.org – [River to River Artwork by Wopo Holup
    (1991)](http://www.nycsubway.org/perl/artwork_show?70)

  - Station Reporter – [1
    Train](https://web.archive.org/web/20141010102251/http://www.stationreporter.net/1train.htm)

  - Forgotten NY – [Original 28 - NYC's First 28 Subway
    Stations](http://forgotten-ny.com/2006/01/the-original-28-part-2-a-look-at-the-artwork-from-the-nycs-first-28-stations-opened-october-27-1904/)

  - The Subway Nut – [125th Street
    Pictures](http://www.subwaynut.com/irt/125n1/index.php)

  - [125th Street entrance from Google Maps Street
    View](https://maps.google.com/maps?ie=UTF8&ll=40.815734,-73.958459&spn=0.003816,0.013433&z=17&layer=c&cbll=40.815833,-73.958397&panoid=tYYlX-UzbB5hwpH7eOdo9Q&cbp=12,231.96,,0,-2.62)

  - [Tiemann Place entrance from Google Maps Street
    View](https://maps.google.com/maps?ie=UTF8&ll=40.815766,-73.958437&spn=0.003816,0.013433&z=17&layer=c&cbll=40.815181,-73.958861&panoid=0mFtpL1bTgv8_Lifn3JXVg&cbp=12,354.74,,0,5.65)

  - [Platforms from Google Maps Street
    View](https://maps.google.com/maps?hl=en&ll=40.815084,-73.958824&spn=0.002972,0.009645&t=m&z=17&layer=c&cbll=40.81508,-73.958827&panoid=CGy3YJIifJUAAAAAAABkNA&cbp=12,53.75,,0,-6.61)

[Category:IRT百老匯-第七大道線車站](../Category/IRT百老匯-第七大道線車站.md "wikilink")
[Category:1904年启用的铁路车站](../Category/1904年启用的铁路车站.md "wikilink")
[Category:曼哈頓紐約地鐵車站](../Category/曼哈頓紐約地鐵車站.md "wikilink")
[Category:1904年紐約州建立](../Category/1904年紐約州建立.md "wikilink")
[Category:晨邊高地](../Category/晨邊高地.md "wikilink")

1.
2.