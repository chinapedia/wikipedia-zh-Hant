<table>
<thead>
<tr class="header">
<th><p><a href="../Page/族_(化学).md" title="wikilink">族</a></p></th>
<th><p><strong>12</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/元素周期.md" title="wikilink">周期</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第4周期元素.md" title="wikilink"><strong>4</strong></a></p></td>
<td><p>30<br />
 <a href="../Page/锌.md" title="wikilink">Zn</a> </p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第5周期元素.md" title="wikilink"><strong>5</strong></a></p></td>
<td><p>48<br />
 <a href="../Page/镉.md" title="wikilink">Cd</a> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第6周期元素.md" title="wikilink"><strong>6</strong></a></p></td>
<td><p>80<br />
 <a href="../Page/汞.md" title="wikilink">Hg</a> </p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第7周期元素.md" title="wikilink"><strong>7</strong></a></p></td>
<td><p>112<br />
 <a href="../Page/鎶.md" title="wikilink">Cn</a> </p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

**12族元素**是在[元素周期表中第](../Page/元素周期表.md "wikilink")12[族的一系列元素](../Page/族_\(化学\).md "wikilink")，它包括[锌](../Page/锌.md "wikilink")、[镉](../Page/镉.md "wikilink")、[汞和](../Page/汞.md "wikilink")[鎶四个](../Page/鎶.md "wikilink")[过渡金属](../Page/过渡金属.md "wikilink")，位[铜族元素和](../Page/铜族元素.md "wikilink")[硼族元素之间](../Page/硼族元素.md "wikilink")。与其它族的过渡金属相比12族的元素的[熔点和](../Page/熔点.md "wikilink")[沸点比较低](../Page/沸点.md "wikilink")，而且在族内原子序数越高，其熔点和沸点越低。比如汞在[室温下是液态的](../Page/室温.md "wikilink")。

|                                                                |                                    |                                            |                                           |                                                 |                    |                                                 |
| -------------------------------------------------------------- | ---------------------------------- | ------------------------------------------ | ----------------------------------------- | ----------------------------------------------- | ------------------ | ----------------------------------------------- |
| style="vertical-align: top;" colspan="1" rowspan="3"\>|对右表的注释： | [过渡金属](../Page/过渡金属.md "wikilink") | <font color="black">黑色</font>的原子序数表示元素是固态的 | <font color="blue">蓝色</font>的原子序数表示元素是液态的 | <font color="green">綠色</font>的原子序数表示元素是气态的\[1\] | 实线边框表示元素在地球形成前就已存在 | 虚线边框表示元素是[人工合成元素](../Page/人工合成元素.md "wikilink") |

这个族的元素的低熔点（尤其是汞）在于其[电子排布及](../Page/电子排布.md "wikilink")[相对论效应](../Page/相对论.md "wikilink")\[2\]。汞的电子排布是\[Kr\]
4d<sup>10</sup> 4f<sup>14</sup> 5s<sup>2</sup> 5p<sup>6</sup>
5d<sup>10</sup>
6s<sup>2</sup>。最外的球状的[電子層](../Page/電子層.md "wikilink")6s已经满了，而且由于相对论的效应这个层离[原子核的距离比较近](../Page/原子核.md "wikilink")。其原因在于汞本身的原子序数已经比较高了，因此其原子核的正电荷比较高，这使得汞的电子层中的电子的运动非常快。快到在计算其运动时必须顾及到狭义相对论的现象，其质量增高，导致[s轨道的大小和能量降低](../Page/s轨道.md "wikilink")。

这两个效应的结果是汞的外电子层被束缚得比较紧，因此汞原子间无法形成非常强的[金属键](../Page/金属键.md "wikilink")。其结果是一种在室温下液态的金属。由于汞的外层电子的惰性汞蒸汽具有[惰性气体的特征](../Page/稀有气体.md "wikilink")。

## 性质的比较

| 元素名称 | [元素符号](../Page/元素符号.md "wikilink") | [原子](../Page/原子.md "wikilink")[半径](../Page/半径.md "wikilink")（[nm](../Page/纳米.md "wikilink")） | 主要化合价   | [状态](../Page/状态.md "wikilink")（[标况](../Page/标准状况.md "wikilink")） | 单质[熔点](../Page/熔点.md "wikilink")（[℃](../Page/摄氏度.md "wikilink")） | 单质[沸点](../Page/沸点.md "wikilink")（℃） |
| ---- | ---------------------------------- | -------------------------------------------------------------------------------------------- | ------- | ---------------------------------------------------------------- | ---------------------------------------------------------------- | ----------------------------------- |
| 锌    | Zn                                 | 0.125                                                                                        | 0，+2    | [固体](../Page/固体.md "wikilink")                                   | 419.4                                                            | 907                                 |
| 镉    | Cd                                 | 0.148                                                                                        | 0，+2    | [固体](../Page/固体.md "wikilink")                                   | 320.9                                                            | 763.3                               |
| 汞    | Hg                                 | 0.126                                                                                        | 0，+1，+2 | [液体](../Page/液体.md "wikilink")                                   | \-38.89                                                          | 357                                 |
| 鎶    | Cn                                 | 0.147                                                                                        | 不詳      | 不詳                                                               | 不詳                                                               | 不詳                                  |

<center>

<table>
<tbody>
<tr class="odd">
<td><p>左方一族：</p></td>
<td><p><strong>12族元素<br />
第12族</strong></p></td>
<td><p>右方一族：</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/11族元素.md" title="wikilink">11族元素</a></strong></p></td>
<td><p><strong><a href="../Page/硼族元素.md" title="wikilink">硼族元素</a></strong></p></td>
<td></td>
</tr>
</tbody>
</table>

</center>

## 参考资料

[族](../Category/元素周期表.md "wikilink")

1.  由[元素週期律推知](../Page/元素週期律.md "wikilink")：由於鎶的沸點只有攝氏80度左右，162號元素常溫下極可能為氣體，並且是唯一的氣態金屬元素
2.  *World Records in Chemistry* Hans-Jürgen Quadbeck-Seeger (Editor),
    Rüdiger Faust, Günter Knaus, Ulrich Siemeling **1999** ISBN
    3-527-29574-7