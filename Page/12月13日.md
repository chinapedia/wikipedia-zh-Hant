**12月13日**是[公历一年中的第](../Page/公历.md "wikilink")347天（[闰年第](../Page/闰年.md "wikilink")348天），离全年结束还有18天。

## 大事記

### 13世紀

  - [1294年](../Page/1294年.md "wikilink")：[教宗](../Page/教宗.md "wikilink")[塞莱斯廷五世宣布退位](../Page/雷定五世.md "wikilink")。

### 16世紀

  - [1545年](../Page/1545年.md "wikilink")：[教宗](../Page/教宗.md "wikilink")[保罗三世召开](../Page/保罗三世.md "wikilink")[特兰托会议](../Page/特兰托会议.md "wikilink")，以应付[新教和](../Page/新教.md "wikilink")[宗教改革运动](../Page/宗教改革运动.md "wikilink")。
  - [1577年](../Page/1577年.md "wikilink")：[英格兰海盗](../Page/英格兰.md "wikilink")[弗朗西斯·德雷克开始其环球航行](../Page/法蘭西斯·德瑞克.md "wikilink")。

### 18世紀

  - [1769年](../Page/1769年.md "wikilink")：[达特茅斯学院建校](../Page/达特茅斯学院.md "wikilink")。

### 19世紀

  - [1862年](../Page/1862年.md "wikilink")：在[美国](../Page/美国.md "wikilink")[南北战争期间](../Page/南北战争.md "wikilink")，[联邦军队在](../Page/美利坚合众国.md "wikilink")[弗雷德里克斯堡战役中遭到](../Page/弗雷德里克斯堡战役.md "wikilink")[邦联守军的顽强抵抗](../Page/美利堅聯盟國.md "wikilink")，伤亡惨重。

### 20世紀

  - [1937年](../Page/1937年.md "wikilink")：[中国抗日战争](../Page/中国抗日战争.md "wikilink")：[日军攻陷](../Page/日军.md "wikilink")[中華民國首都](../Page/中華民國.md "wikilink")[南京](../Page/南京.md "wikilink")，對当地軍民展開长达六周的[大屠杀](../Page/南京大屠杀.md "wikilink")，史称“[南京大屠杀](../Page/南京大屠殺.md "wikilink")”，超過30万人被殺害。2014年2月27日[中国](../Page/中国.md "wikilink")[全国人大常委会经表决将](../Page/全国人大常委会.md "wikilink")12月13日确定为南京大屠杀死难者国家公祭日。
  - [1939年](../Page/1939年.md "wikilink")：[第二次世界大战](../Page/第二次世界大战.md "wikilink")：[拉普拉塔河口海戰](../Page/拉普拉塔河口海戰.md "wikilink")，[德國戰艦](../Page/德國.md "wikilink")「[施佩伯爵海軍上將號重巡洋艦](../Page/施佩伯爵海軍上將號重巡洋艦.md "wikilink")」在[拉普拉塔河河口遭遇英国舰队](../Page/拉普拉塔河.md "wikilink")，最终在[蒙得维的亚自行爆炸沉沒](../Page/蒙得维的亚.md "wikilink")。
  - [1941年](../Page/1941年.md "wikilink")：第二次世界大戰：[匈牙利对](../Page/匈牙利王國.md "wikilink")[美國宣战](../Page/美國.md "wikilink")。
  - [1949年](../Page/1949年.md "wikilink")：[以色列议会决定将首都迁往](../Page/以色列.md "wikilink")[耶路撒冷](../Page/耶路撒冷.md "wikilink")，但未得到大多数国家公认。
  - [1959年](../Page/1959年.md "wikilink")：[马卡里奥斯大主教当选为](../Page/马卡里奥斯大主教.md "wikilink")[塞浦路斯总统](../Page/賽普勒斯.md "wikilink")。
  - [1964年](../Page/1964年.md "wikilink")：[刘少奇发布](../Page/刘少奇.md "wikilink")[中华人民共和国主席特赦令](../Page/中华人民共和国主席.md "wikilink")，对于确实改恶从善的[满洲国和](../Page/满洲国.md "wikilink")[蒙疆自治政府的战争罪犯实行特赦](../Page/蒙疆聯合自治政府.md "wikilink")。同时对所谓的“[国民党](../Page/中國國民黨.md "wikilink")[蒋介石集团](../Page/蒋介石.md "wikilink")”实行特赦。
  - [1974年](../Page/1974年.md "wikilink")：[马耳他宣布成为](../Page/马耳他.md "wikilink")[共和国](../Page/共和制.md "wikilink")。
  - [1981年](../Page/1981年.md "wikilink")：[波兰总理](../Page/波兰总理.md "wikilink")[沃依切赫·雅鲁泽尔斯基颁布](../Page/沃依切赫·雅鲁泽尔斯基.md "wikilink")[戒严令](../Page/戒严令.md "wikilink")，宣布取缔[团结工会](../Page/团结工会.md "wikilink")，并逮捕其领导人。
  - [1996年](../Page/1996年.md "wikilink")：[科菲·安南当选为](../Page/科菲·安南.md "wikilink")[联合国秘书长](../Page/联合国秘书长.md "wikilink")。
  - [2000年](../Page/2000年.md "wikilink")：[阿尔·戈尔最终宣布竞选失败](../Page/阿尔·戈尔.md "wikilink")，[2000年美国总统选举结束](../Page/2000年美国总统选举.md "wikilink")。

### 21世紀

  - [2003年](../Page/2003年.md "wikilink")：在[伊拉克战争中](../Page/伊拉克战争.md "wikilink")，[美军寻获藏身在](../Page/美军.md "wikilink")[提克里特的](../Page/提克里特.md "wikilink")[伊拉克前總統](../Page/伊拉克总统.md "wikilink")[萨达姆·侯赛因](../Page/萨达姆·侯赛因.md "wikilink")。
  - [2004年](../Page/2004年.md "wikilink")：[智利前独裁者](../Page/智利.md "wikilink")[奥古斯托·皮诺切特遭到软禁](../Page/奥古斯托·皮诺切特.md "wikilink")。
  - [2005年](../Page/2005年.md "wikilink")：[世界貿易組織第六次部長級會議在](../Page/世界貿易組織第六次部長級會議.md "wikilink")[香港會議展覽中心開幕](../Page/香港會議展覽中心.md "wikilink")。
  - [2006年](../Page/2006年.md "wikilink")：[长江淡水豚类考察结束](../Page/长江淡水豚类考察.md "wikilink")，宣布[白暨豚](../Page/白鱀豚.md "wikilink")“功能性灭绝”。
  - [2007年](../Page/2007年.md "wikilink")：[歐盟](../Page/欧洲联盟.md "wikilink")27個成員國的領導人正式簽署了《[里斯本條約](../Page/里斯本條約.md "wikilink")》以取代舊有的《[欧盟宪法](../Page/欧盟宪法.md "wikilink")》。
  - [2009年](../Page/2009年.md "wikilink")：[巴基斯坦](../Page/巴基斯坦.md "wikilink")[卡拉奇](../Page/卡拉奇.md "wikilink")[联合银行發生](../Page/联合银行.md "wikilink")[搶劫案](../Page/2009年卡拉奇銀行搶劫案.md "wikilink")。
  - [2018年](../Page/2018年.md "wikilink")：[土耳其一列高速火車出軌](../Page/土耳其.md "wikilink")，導致9死47傷。\[1\]

## 出生

  - [1520年](../Page/1520年.md "wikilink")：[-{zh-hans:西克斯圖斯五世;zh-hk:思道五世;zh-tw:思道五世;}-](../Page/西斯都五世.md "wikilink")，[羅馬主教](../Page/羅馬主教.md "wikilink")[教宗](../Page/教宗.md "wikilink")（逝於[1590年](../Page/1590年.md "wikilink")）
  - [1533年](../Page/1533年.md "wikilink")：[埃里克十四世](../Page/埃里克十四世.md "wikilink")，[瓦薩王朝第二位](../Page/瓦薩王朝.md "wikilink")[瑞典國王](../Page/瑞典.md "wikilink")（逝於[1577年](../Page/1577年.md "wikilink")）
  - [1553年](../Page/1553年.md "wikilink")：[亨利四世](../Page/亨利四世_\(法兰西\).md "wikilink")，[法國國王](../Page/法國國王.md "wikilink")，[波旁王朝創建者](../Page/波旁王朝.md "wikilink")（逝於[1610年](../Page/1610年.md "wikilink")）
  - [1678年](../Page/1678年.md "wikilink")：[愛新覺羅胤禛](../Page/雍正帝.md "wikilink")，[清朝雍正皇帝](../Page/清朝.md "wikilink")（逝於[1735年](../Page/1735年.md "wikilink")）
  - [1720年](../Page/1720年.md "wikilink")：[卡洛·戈齊](../Page/卡洛·戈齊.md "wikilink")，[義大利劇作家](../Page/義大利.md "wikilink")（逝於[1806年](../Page/1806年.md "wikilink")）
  - [1780年](../Page/1780年.md "wikilink")：[約翰·沃爾夫岡·德貝萊納](../Page/約翰·沃爾夫岡·德貝萊納.md "wikilink")，[德國化學家](../Page/德國.md "wikilink")（逝於[1849年](../Page/1849年.md "wikilink")）
  - [1797年](../Page/1797年.md "wikilink")：[海因里希·海涅](../Page/海因里希·海涅.md "wikilink")，德国[浪漫主义詩人](../Page/浪漫主义.md "wikilink")、記者（逝於[1856年](../Page/1856年.md "wikilink")）
  - [1816年](../Page/1816年.md "wikilink")：[维尔纳·冯·西门子](../Page/维尔纳·冯·西门子.md "wikilink")，德国發明家，[西門子公司創始人](../Page/西門子公司.md "wikilink")（逝於[1892年](../Page/1892年.md "wikilink")）
  - [1818年](../Page/1818年.md "wikilink")：[玛丽·托德](../Page/玛丽·托德.md "wikilink")，[美國第十六任總統](../Page/美國.md "wikilink")[亞伯拉罕·林肯的夫人](../Page/亞伯拉罕·林肯.md "wikilink")（逝於[1882年](../Page/1882年.md "wikilink")）
  - [1867年](../Page/1867年.md "wikilink")：[克里斯蒂安·伯克兰](../Page/克里斯蒂安·伯克兰.md "wikilink")，[挪威極光科學家](../Page/挪威.md "wikilink")，線圈砲、從空氣中固氮法發明人（逝於[1917年](../Page/1917年.md "wikilink")）
  - [1881年](../Page/1881年.md "wikilink")：[林獻堂](../Page/林獻堂.md "wikilink")，[台灣日治時期文學家](../Page/台灣日治時期.md "wikilink")、非暴力反日人士中右派代表人物，被譽為「台灣自治運動的領袖與文化的褓母」（逝於[1956年](../Page/1956年.md "wikilink")）
  - [1887年](../Page/1887年.md "wikilink")：[喬治·波利亞](../Page/喬治·波利亞.md "wikilink")，[匈牙利猶太裔美國數學家](../Page/匈牙利.md "wikilink")、數學教育家（逝於[1985年](../Page/1985年.md "wikilink")）
  - [1887年](../Page/1887年.md "wikilink")：[艾文·約克](../Page/艾文·約克.md "wikilink")，美國一次大戰戰爭英雄；1941年電影「約克軍曹」真實主角（逝於[1964年](../Page/1964年.md "wikilink")）
  - [1902年](../Page/1902年.md "wikilink")：[塔尔科特·帕森斯](../Page/塔尔科特·帕森斯.md "wikilink")，美國[社会学家](../Page/社会学.md "wikilink")（逝於[1979年](../Page/1979年.md "wikilink")）
  - [1906年](../Page/1906年.md "wikilink")：[馬里納郡主](../Page/馬里納郡主_\(根德公爵夫人\).md "wikilink")，[希腊公主](../Page/希腊.md "wikilink")，英國王室成員（逝於[1968年](../Page/1968年.md "wikilink")）
  - [1911年](../Page/1911年.md "wikilink")：[特里夫·哈維默](../Page/特里夫·哈維默.md "wikilink")，挪威經濟學家，1989年獲[諾貝爾經濟學獎](../Page/諾貝爾經濟學獎.md "wikilink")（逝於[1999年](../Page/1999年.md "wikilink")）
  - [1913年](../Page/1913年.md "wikilink")：[阿諾·布朗](../Page/阿諾·布朗.md "wikilink")，[加拿大](../Page/加拿大.md "wikilink")[救世軍大將](../Page/救世軍大將.md "wikilink")（逝於[2002年](../Page/2002年.md "wikilink")）
  - [1915年](../Page/1915年.md "wikilink")：[羅斯·麥唐諾](../Page/羅斯·麥唐諾.md "wikilink")，美國冷硬派[犯罪小說作家](../Page/犯罪小說.md "wikilink")（逝於[1983年](../Page/1983年.md "wikilink")）
  - [1919年](../Page/1919年.md "wikilink")：[漢斯-約阿希·馬西里](../Page/漢斯-約阿希·馬西里.md "wikilink")，二次大戰[德國空軍](../Page/德國空軍.md "wikilink")[王牌飛行員](../Page/王牌飛行員.md "wikilink")（逝於[1942年](../Page/1942年.md "wikilink")）
  - [1920年](../Page/1920年.md "wikilink")：[乔治·普拉特·舒尔茨](../Page/乔治·普拉特·舒尔茨.md "wikilink")，[美國國務卿](../Page/美國國務卿.md "wikilink")
  - [1923年](../Page/1923年.md "wikilink")：[菲利普·安德森](../Page/菲利普·安德森.md "wikilink")，美國物理學家，[1977年](../Page/1977年.md "wikilink")[诺贝尔物理学奖得主](../Page/诺贝尔物理学奖.md "wikilink")
  - [1929年](../Page/1929年.md "wikilink")：[克里斯托弗·普卢默](../Page/克里斯托弗·普卢默.md "wikilink")，加拿大演員，[奧斯卡金像獎及](../Page/奧斯卡金像獎.md "wikilink")[東尼獎](../Page/東尼獎.md "wikilink")、[艾美獎得主](../Page/艾美獎.md "wikilink")
  - [1934年](../Page/1934年.md "wikilink")：[理查·賽納克](../Page/理查·賽納克.md "wikilink")，美國電影監製，1989年以《[溫馨接送情](../Page/溫馨接送情.md "wikilink")》獲[奧斯卡最佳影片獎](../Page/奧斯卡最佳影片獎.md "wikilink")（逝於[2012年](../Page/2012年.md "wikilink")）
  - [1936年](../Page/1936年.md "wikilink")：[阿迦汗四世](../Page/阿迦汗四世.md "wikilink")，[伊斯兰教](../Page/伊斯兰教.md "wikilink")[什叶派的](../Page/什叶派.md "wikilink")[伊斯玛仪派的最高精神领袖](../Page/伊斯玛仪派.md "wikilink")
  - [1945年](../Page/1945年.md "wikilink")：[赫尔曼·凯恩](../Page/赫尔曼·凯恩.md "wikilink")，美國作家、電台主持、茶黨運動者，堪薩斯城聯邦儲備銀行主席
  - [1950年](../Page/1950年.md "wikilink")：[汤姆·维尔萨克](../Page/汤姆·维尔萨克.md "wikilink")，第30任美國農業部長
  - [1953年](../Page/1953年.md "wikilink")：[本·伯南克](../Page/本·伯南克.md "wikilink")，第14任[美國聯邦準備理事會主席](../Page/美國聯邦準備理事會.md "wikilink")
  - [1957年](../Page/1957年.md "wikilink")：[許純美](../Page/許純美.md "wikilink")，[台灣話題人物](../Page/台灣.md "wikilink")
  - [1959年](../Page/1959年.md "wikilink")：[莊靜而](../Page/莊靜而.md "wikilink")，前[香港女演員](../Page/香港.md "wikilink")
  - [1961年](../Page/1961年.md "wikilink")：[王維基](../Page/王維基.md "wikilink")，香港[城市電訊有限公司及](../Page/城市電訊.md "wikilink")[香港寬頻聯合創辦人兼董事局主席](../Page/香港寬頻.md "wikilink")
  - [1961年](../Page/1961年.md "wikilink")：[哈利·葛瑞森-威廉斯](../Page/哈利·葛瑞森-威廉斯.md "wikilink")，英國作曲家
  - [1964年](../Page/1964年.md "wikilink")：[hide](../Page/hide.md "wikilink")，日本[吉他手](../Page/吉他.md "wikilink")，[視覺系先驅者](../Page/視覺系.md "wikilink")（逝於[1998年](../Page/1998年.md "wikilink")）
  - [1967年](../Page/1967年.md "wikilink")：[織田裕二](../Page/織田裕二.md "wikilink")，日本演員、歌手
  - [1967年](../Page/1967年.md "wikilink")：[傑米·福克斯](../Page/傑米·福克斯.md "wikilink")，美國演員、歌手、鋼琴家
  - [1968年](../Page/1968年.md "wikilink")：[楊麗菁](../Page/楊麗菁.md "wikilink")，台灣演員
  - [1969年](../Page/1969年.md "wikilink")：[石川英郎](../Page/石川英郎.md "wikilink")，[日本聲優](../Page/日本配音員.md "wikilink")
  - [1969年](../Page/1969年.md "wikilink")：[托尼·库兰](../Page/托尼·库兰.md "wikilink")，[蘇格蘭演員](../Page/蘇格蘭.md "wikilink")
  - [1969年](../Page/1969年.md "wikilink")：[木拉提·纳斯洛夫](../Page/木拉提·纳斯洛夫.md "wikilink")，俄羅斯[維吾爾族流行歌手](../Page/維吾爾族.md "wikilink")（逝於[2007年](../Page/2007年.md "wikilink")）
  - [1970年](../Page/1970年.md "wikilink")：-{[于正昇](../Page/于正昇.md "wikilink")}-，[台灣](../Page/台灣.md "wikilink")[配音員](../Page/配音員.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink")：[格琳德·卡尔滕布鲁纳](../Page/格琳德·卡尔滕布鲁纳.md "wikilink")，[奧地利登山者](../Page/奧地利.md "wikilink")
  - [1973年](../Page/1973年.md "wikilink")：[邱彥翔](../Page/邱彥翔.md "wikilink")，台灣藝人
  - [1980年](../Page/1980年.md "wikilink")：[黃宗澤](../Page/黃宗澤.md "wikilink")，香港演員、歌手
  - [1980年](../Page/1980年.md "wikilink")：[妻夫木聰](../Page/妻夫木聰.md "wikilink")，日本演員
  - [1980年](../Page/1980年.md "wikilink")：[蘇麗文](../Page/蘇麗文.md "wikilink")，台灣[跆拳道選手](../Page/跆拳道.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[楚宣](../Page/楚宣.md "wikilink")，台灣女演員
  - [1981年](../Page/1981年.md "wikilink")：[張勝祖](../Page/張勝祖.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[-{zh-hans:艾米·李;zh-hk:艾米·李;zh-tw:艾米·李;}-](../Page/埃米·李.md "wikilink")，美國歌手、鋼琴家和詞曲創作人，搖滾樂團[伊凡塞斯創始人和主唱](../Page/伊凡塞斯.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[利亚姆·劳伦斯](../Page/利亚姆·劳伦斯.md "wikilink")，[愛爾蘭足球運動員](../Page/愛爾蘭.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[瑛太](../Page/瑛太.md "wikilink")，日本演員
  - [1982年](../Page/1982年.md "wikilink")：[安東尼·凱利](../Page/安東尼·凱利.md "wikilink")，[澳洲歌手](../Page/澳洲.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[木下亞由美](../Page/木下亞由美.md "wikilink")，日本演員
  - [1984年](../Page/1984年.md "wikilink")：[-{zh-hans:桑蒂·卡索拉;zh-hk:辛迪·卡蘇拿;zh-tw:桑蒂·卡索拉;}-](../Page/辛蒂·卡索拉.md "wikilink")，[西班牙足球運動員](../Page/西班牙.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[陳翔](../Page/陳翔.md "wikilink")，中國內地創作歌手、演員；
  - [1989年](../Page/1989年.md "wikilink")：[-{zh-cn:泰勒·斯威夫特; zh-tw:泰勒絲;
    zh-hk:泰勒·斯威夫特;}-](../Page/泰勒·斯威夫特.md "wikilink")（Taylor
    Swift），美國天后級創作歌手、唱片製作人、慈善家、演員
  - [1989年](../Page/1989年.md "wikilink")：[古川愛李](../Page/古川愛李.md "wikilink")，日本女子偶像團體[SKE48成員](../Page/SKE48.md "wikilink")
  - [1994年](../Page/1994年.md "wikilink")：[家入莉奥](../Page/家入里歐.md "wikilink")，日本歌手

## 逝世

  - [784年](../Page/784年.md "wikilink")：[尚可孤](../Page/尚可孤.md "wikilink")，[唐朝将领](../Page/唐朝.md "wikilink")
  - [1395年](../Page/1395年.md "wikilink")：[赵谦](../Page/赵谦.md "wikilink")，[明朝](../Page/明朝.md "wikilink")[儒学大师](../Page/儒学.md "wikilink")\[2\]
  - [1521年](../Page/1521年.md "wikilink")：[葡萄牙國王](../Page/葡萄牙.md "wikilink")[曼努埃尔一世](../Page/曼努埃尔一世_\(葡萄牙\).md "wikilink")（[1469年出生](../Page/1469年.md "wikilink")）
  - [1937年](../Page/1937年.md "wikilink")：[司徒非](../Page/司徒非.md "wikilink")，[国民革命军第六十六军一六O师少将参谋长](../Page/国民革命军.md "wikilink")（[1893年出生](../Page/1893年.md "wikilink")）
  - 1937年：[刘国用](../Page/刘国用.md "wikilink")，国民革命军第七十四军五十八师一七四旅少将副旅长（[1898年出生](../Page/1898年.md "wikilink")）
  - 1937年：[蔡如柏](../Page/蔡如柏.md "wikilink")，国民革命军第六十六军一六O师九五六团上校团长（[1898年出生](../Page/1898年.md "wikilink")）
  - 1937年：[谢承瑞](../Page/谢承瑞.md "wikilink")，国民革命军教导总队第一旅二团上校团长（[1905年出生](../Page/1905年.md "wikilink")）
  - [1944年](../Page/1944年.md "wikilink")：[康丁斯基](../Page/瓦西里·康定斯基.md "wikilink")，出生於[俄罗斯的](../Page/俄罗斯.md "wikilink")[抽象派](../Page/抽象表現主義.md "wikilink")[畫家和美術理論家](../Page/畫家.md "wikilink")（[1866年出生](../Page/1866年.md "wikilink")）
  - [1996年](../Page/1996年.md "wikilink")：[曹禺](../Page/曹禺.md "wikilink")，[中国現代](../Page/中国.md "wikilink")[剧作家及戏剧教育家](../Page/剧作家.md "wikilink")（[1910年出生](../Page/1910年.md "wikilink")）
  - [2016年](../Page/2016年.md "wikilink")：[托马斯·克罗姆比·谢林](../Page/托马斯·克罗姆比·谢林.md "wikilink")，美国[经济学家](../Page/经济学家.md "wikilink")，2005年[诺贝尔经济学奖得主](../Page/诺贝尔经济学奖.md "wikilink")（[1921年出生](../Page/1921年.md "wikilink")）
  - [2016年](../Page/2016年.md "wikilink")：[艾伦·锡克](../Page/艾伦·锡克.md "wikilink")，美国演员（[1947年出生](../Page/1947年.md "wikilink")）

## 節日與紀念日

：[南京大屠杀死难者国家公祭日](../Page/南京大屠杀死难者国家公祭日.md "wikilink")

## 參考資料

1.
2.