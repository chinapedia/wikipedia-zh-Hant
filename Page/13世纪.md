[1201年](../Page/1201年.md "wikilink")[1月1日至](../Page/1月1日.md "wikilink")[1300年](../Page/1300年.md "wikilink")[12月31日的这一段期间被称为](../Page/12月31日.md "wikilink")**13世纪**。

在[中国](../Page/中国.md "wikilink")13世纪是[南宋](../Page/南宋.md "wikilink")、[金](../Page/金朝.md "wikilink")、[西夏](../Page/西夏.md "wikilink")、[大理](../Page/大理國.md "wikilink")、[西辽](../Page/西辽.md "wikilink")、[蒙古](../Page/蒙古.md "wikilink")、[吐蕃等政权并存的时期](../Page/吐蕃.md "wikilink")，其中[成吉思汗統一](../Page/成吉思汗.md "wikilink")[蒙古](../Page/蒙古.md "wikilink")，建立[大蒙古國](../Page/大蒙古國.md "wikilink")，經過[蒙古三次西征](../Page/蒙古三次西征.md "wikilink")，東征西討，建立當時領土最遼闊國家。最後由孫兒[忽必烈建立的](../Page/忽必烈.md "wikilink")[元朝统一全中国](../Page/元朝.md "wikilink")。

在[日本](../Page/日本.md "wikilink")，[镰仓幕府逐渐衰弱](../Page/鎌倉幕府.md "wikilink")。[朝鲜成为](../Page/朝鲜民主主义人民共和国.md "wikilink")[元朝统治下的半独立国家](../Page/元朝.md "wikilink")。

在[中东](../Page/中东地区.md "wikilink")，传统的[伊斯兰国家遭到](../Page/伊斯兰教.md "wikilink")[蒙古的进攻](../Page/蒙古.md "wikilink")，[阿拉伯帝国灭亡](../Page/阿拉伯帝国.md "wikilink")，[奥斯曼帝国立国](../Page/奥斯曼帝国.md "wikilink")。

在[西欧](../Page/西欧.md "wikilink")13世纪是[中世纪中期](../Page/中世纪.md "wikilink")。[十字军屡次东侵](../Page/十字軍東征.md "wikilink")。[天主教会在与世俗贵族之间的权利斗争中赢得上手](../Page/羅馬天主教會.md "wikilink")。

在[东欧](../Page/东欧.md "wikilink")，[斯拉夫人受到](../Page/斯拉夫人.md "wikilink")[蒙古的压制](../Page/蒙古.md "wikilink")，成为[金帐汗国的臣民](../Page/金帳汗國.md "wikilink")。[拜占庭帝国遭到](../Page/拜占庭帝国.md "wikilink")[十字军攻击](../Page/十字軍東征.md "wikilink")，分裂且衰弱。

在[非洲](../Page/非洲.md "wikilink")，东北部的[埃及](../Page/埃及.md "wikilink")[阿尤布王朝鼎盛](../Page/阿尤布王朝.md "wikilink")，屡次击败十字军。西非[马里帝国兴起](../Page/马里帝国.md "wikilink")。东非[斯瓦西里文明进入全盛时期](../Page/斯瓦西里.md "wikilink")。

在[中美洲](../Page/中美洲.md "wikilink")[玛雅](../Page/玛雅.md "wikilink")[奇琴伊察达到了鼎盛时期](../Page/奇琴伊察.md "wikilink")。

## 重要事件、发展与成就

  - **科学技术**

在欧洲13世纪是一个思想很活跃的时代：[大阿尔伯特对大自然做了许多观察和研究](../Page/大阿尔伯特.md "wikilink")，他将[亚里士多德的思想引入欧洲](../Page/亚里士多德.md "wikilink")。他的学生[托马斯·阿奎那将](../Page/托马斯·阿奎那.md "wikilink")[巴黎大学变成了当时欧洲的思想中心](../Page/巴黎大学.md "wikilink")。

  - **战争与政治**
      - [十字军第四次至第八次东侵](../Page/十字军东征.md "wikilink")。
      - [成吉思汗建国](../Page/成吉思汗.md "wikilink")，[西征至东欧](../Page/蒙古军征战.md "wikilink")。
      - [瑞士建国](../Page/瑞士.md "wikilink")。
      - [1275年](../Page/1275年.md "wikilink")——[马可波罗抵达](../Page/马可波罗.md "wikilink")[元](../Page/元.md "wikilink")[大都](../Page/大都.md "wikilink")。
      - [1279年](../Page/1279年.md "wikilink")——[忽必烈灭](../Page/忽必烈.md "wikilink")[南宋](../Page/南宋.md "wikilink")。

<!-- end list -->

  - **13世纪的天灾人祸**

<!-- end list -->

  - **文化娱乐**
      - [尼伯龙根之歌写成](../Page/尼伯龍根之歌.md "wikilink")

<!-- end list -->

  - **社會與經濟**

<!-- end list -->

  - **疾病与医学**

<!-- end list -->

  - **环境与自然资源**

<!-- end list -->

  - **宗教與哲學**

## 重要人物

  - [成吉思汗](../Page/成吉思汗.md "wikilink")，[蒙古帝國创始人](../Page/蒙古帝國.md "wikilink")
  - [忽必烈](../Page/忽必烈.md "wikilink")，灭[南宋](../Page/南宋.md "wikilink")
  - [聖方濟各](../Page/聖方濟各_\(亞西西\).md "wikilink")（Francis of
    Assisi），[方济各会典基人](../Page/方济各会.md "wikilink")
  - [大阿尔伯特](../Page/大阿尔伯特.md "wikilink")（Albertus Magnus），学者
  - [托马斯·阿奎那](../Page/托马斯·阿奎纳.md "wikilink")（Thomas Aquinas），学者
  - [罗吉尔·培根](../Page/罗吉尔·培根.md "wikilink")（Roger Bacon），哲学家
  - [马可波罗](../Page/马可·波罗.md "wikilink")
  - [但丁](../Page/但丁.md "wikilink")
  - [喬托](../Page/喬托.md "wikilink")
  - [日莲](../Page/日莲.md "wikilink")

## 世界领导人

### [非洲](../Page/非洲.md "wikilink")、[美洲](../Page/美洲.md "wikilink")

### [歐洲](../Page/歐洲.md "wikilink")

### [亚洲](../Page/亚洲.md "wikilink")

#### [中國](../Page/中國.md "wikilink")

##### [南宋](../Page/南宋.md "wikilink")

  - [宋寧宗](../Page/宋寧宗.md "wikilink")[趙擴](../Page/趙擴.md "wikilink")
  - [宋理宗](../Page/宋理宗.md "wikilink")[趙昀](../Page/趙昀.md "wikilink")
  - [宋度宗](../Page/宋度宗.md "wikilink")[趙禥](../Page/趙禥.md "wikilink")
  - [宋恭帝](../Page/宋恭帝.md "wikilink")[趙㬎](../Page/趙㬎.md "wikilink")
  - [宋端宗](../Page/宋端宗.md "wikilink")[趙昰](../Page/趙昰.md "wikilink")
  - [宋少帝](../Page/宋少帝.md "wikilink")[趙昺](../Page/趙昺.md "wikilink")

##### [金](../Page/金朝.md "wikilink")

  - [金章宗](../Page/金章宗.md "wikilink")[完顏璟](../Page/完顏璟.md "wikilink")
  - [金衛紹王](../Page/金衛紹王.md "wikilink")[完顏永濟](../Page/完顏永濟.md "wikilink")
  - [金宣宗](../Page/金宣宗.md "wikilink")[完顏珣](../Page/完顏珣.md "wikilink")
  - [金哀宗](../Page/金哀宗.md "wikilink")[完顏守緒](../Page/完顏守緒.md "wikilink")

##### [西夏](../Page/西夏.md "wikilink")

##### [大理](../Page/大理.md "wikilink")

##### [王氏高麗](../Page/王氏高麗.md "wikilink")

##### [蒙古帝国](../Page/蒙古帝国.md "wikilink")

  - [元太祖](../Page/元太祖.md "wikilink")[鐵木真](../Page/鐵木真.md "wikilink")
  - [元睿宗](../Page/元睿宗.md "wikilink")[拖雷](../Page/拖雷.md "wikilink")(監國)
  - [元太宗](../Page/元太宗.md "wikilink")[窩闊台](../Page/窩闊台.md "wikilink")
  - [元定宗](../Page/元定宗.md "wikilink")[貴由](../Page/貴由.md "wikilink")
  - [元憲宗](../Page/元憲宗.md "wikilink")[蒙哥](../Page/蒙哥.md "wikilink")

###### [元](../Page/元.md "wikilink")

  - [元世祖](../Page/元世祖.md "wikilink")[忽必烈](../Page/忽必烈.md "wikilink")
  - [元成宗](../Page/元成宗.md "wikilink")[鐵穆耳](../Page/鐵穆耳.md "wikilink")

#### [越南](../Page/越南.md "wikilink")

##### [李朝](../Page/李朝_\(越南\).md "wikilink")

##### [陳朝](../Page/陳朝_\(越南\).md "wikilink")

## 13世纪年历

[13世纪](../Category/13世纪.md "wikilink")
[Category:2千纪](../Category/2千纪.md "wikilink")
[+13](../Category/世纪.md "wikilink")