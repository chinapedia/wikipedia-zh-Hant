## 大事记

  - [齐格蒙特三世把](../Page/齐格蒙特三世.md "wikilink")[波兰首都从](../Page/波兰.md "wikilink")[克拉科夫迁到](../Page/克拉科夫.md "wikilink")[华沙](../Page/华沙.md "wikilink")
  - [格罗宁根大学创办](../Page/格罗宁根大学.md "wikilink")
  - [剑桥大学悉尼·苏塞克斯学院创办](../Page/剑桥大学悉尼·苏塞克斯学院.md "wikilink")
  - [荷兰舰只首次到达](../Page/荷兰.md "wikilink")[蘇門答臘和](../Page/蘇門答臘.md "wikilink")[爪哇岛](../Page/爪哇岛.md "wikilink")
  - [李时珍著](../Page/李时珍.md "wikilink")《[本草纲目](../Page/本草纲目.md "wikilink")》刊行。
  - [許浚奉](../Page/許浚.md "wikilink")[朝鮮宣宗之命編撰並刊行](../Page/朝鮮宣宗.md "wikilink")《東醫寶鑒》一書。
  - [4月9日](../Page/4月9日.md "wikilink")——[西班牙军队占领](../Page/西班牙.md "wikilink")[加来](../Page/加来.md "wikilink")
  - [5月18日](../Page/5月18日.md "wikilink")——[威廉·巴伦支开始他的第三次北极探险](../Page/威廉·巴伦支.md "wikilink")
  - [6月17日](../Page/6月17日.md "wikilink")——巴伦支发现[斯匹次卑尔根岛](../Page/斯匹次卑尔根岛.md "wikilink")
  - [7月5日](../Page/7月5日.md "wikilink")——英国舰队攻占[加的斯](../Page/加的斯.md "wikilink")
  - [8月13日](../Page/8月13日.md "wikilink")——[大卫·法巴雷克斯发现](../Page/大卫·法巴雷克斯.md "wikilink")[鯨魚座ο的亮度会变化](../Page/蒭藁增二.md "wikilink")
  - [9月17日](../Page/9月17日.md "wikilink")——西班牙占领[亞眠](../Page/亞眠.md "wikilink")
  - [9月20日](../Page/9月20日.md "wikilink")——[蒙特雷建城](../Page/蒙特雷.md "wikilink")

## 出生

  - [卢卡斯·霍尔斯特尼乌斯](../Page/卢卡斯·霍尔斯特尼乌斯.md "wikilink")，德国人文主义者（逝世于[1661年](../Page/1661年.md "wikilink")）
  - [约格·耶纳施](../Page/约格·耶纳施.md "wikilink")，瑞士政治领导人（逝世于[1639年](../Page/1639年.md "wikilink")）
  - [理查德·马瑟](../Page/理查德·马瑟.md "wikilink")，美洲神学家（逝世于[1669年](../Page/1669年.md "wikilink")）
  - [爱德华·温斯洛](../Page/爱德华·温斯洛.md "wikilink")，英国殖民者（逝世于[1655年](../Page/1655年.md "wikilink")）
  - [1月13日](../Page/1月13日.md "wikilink")——[扬·凡·戈因](../Page/扬·凡·戈因.md "wikilink")，荷兰画家（逝世于[1656年](../Page/1656年.md "wikilink")）
  - [2月2日](../Page/2月2日.md "wikilink")——[雅各布·凡·坎彭](../Page/雅各布·凡·坎彭.md "wikilink")，荷兰建筑师（逝世于[1657年](../Page/1657年.md "wikilink")）
  - [3月16日](../Page/3月16日.md "wikilink")——[埃巴·布拉厄](../Page/埃巴·布拉厄.md "wikilink")，瑞典贵妇人（逝世于[1674年](../Page/1674年.md "wikilink")）
  - [3月31日](../Page/3月31日.md "wikilink")——[勒奈·笛卡爾法國](../Page/勒奈·笛卡爾.md "wikilink")|哲學家、數學家、物理學家（逝世于[1650年](../Page/1650年.md "wikilink")）
  - [6月29日](../Page/6月29日.md "wikilink")——[後水尾天皇](../Page/後水尾天皇.md "wikilink")，
    日本天皇
  - [7月3日](../Page/7月3日.md "wikilink")——[约翰·班纳尔](../Page/约翰·班纳尔.md "wikilink")，瑞典元帅（逝世于[1641年](../Page/1641年.md "wikilink")）
  - [7月12日](../Page/7月12日.md "wikilink")——[米哈伊尔·罗曼诺夫](../Page/米哈伊尔·费奥多罗维奇·罗曼诺夫.md "wikilink")，[1613年](../Page/1613年.md "wikilink")－[1645年间的俄罗斯沙皇](../Page/1645年.md "wikilink")（逝世于[1645年](../Page/1645年.md "wikilink")）
  - [8月19日](../Page/8月19日.md "wikilink")——[伊丽莎白·斯图亚特](../Page/伊丽莎白·斯图亚特.md "wikilink")，英格兰公主，波西米亚王后（逝世于[1662年](../Page/1662年.md "wikilink")）
  - [8月26日](../Page/8月26日.md "wikilink")——[腓特烈五世](../Page/腓特烈五世_\(普法尔茨\).md "wikilink")，普法尔茨选侯（逝世于[1632年](../Page/1632年.md "wikilink")）
  - [9月3日](../Page/9月3日.md "wikilink")——[尼古拉·阿马蒂](../Page/尼古拉·阿马蒂.md "wikilink")，意大利小提琴制造家（逝世于[1684年](../Page/1684年.md "wikilink")）
  - 9月——[詹姆斯·雪利](../Page/詹姆斯·雪利.md "wikilink")，英国剧作家（逝世于[1666年](../Page/1666年.md "wikilink")）
  - [9月4日](../Page/9月4日.md "wikilink")——[康斯坦丁·惠更斯](../Page/康斯坦丁·惠更斯.md "wikilink")，荷兰诗人（逝世于[1687年](../Page/1687年.md "wikilink")）
  - [11月1日](../Page/11月1日.md "wikilink")——[皮得罗·达·科尔托纳](../Page/皮得罗·达·科尔托纳.md "wikilink")，意大利雕塑家、建筑家和画家（逝世于[1669年](../Page/1669年.md "wikilink")）
  - [12月7日](../Page/12月7日.md "wikilink")——[约翰·卡西米尔](../Page/约翰·卡西米尔_\(安哈尔特-德绍\).md "wikilink")，安哈尔特-德绍亲王（逝世于[1660年](../Page/1660年.md "wikilink")）
  - [12月21日](../Page/12月21日.md "wikilink")——[彼得·莫奇拉](../Page/彼得·莫奇拉.md "wikilink")，基辅大主教（逝世于[1646年](../Page/1646年.md "wikilink")）

## 逝世

  - [1月28日](../Page/1月28日.md "wikilink")——[法蘭西斯·德瑞克](../Page/法蘭西斯·德瑞克.md "wikilink")，第二位在[麥哲倫之後完成環球航海的英國探險家](../Page/斐迪南·麥哲倫.md "wikilink")（出生于[1540年](../Page/1540年.md "wikilink")）
  - [2月7日](../Page/2月7日.md "wikilink")——[格奥尔格一世](../Page/格奥尔格一世_\(黑森-达姆施塔特\).md "wikilink")，黑森-达姆施塔特伯爵（出生于[1547年](../Page/1547年.md "wikilink")）
  - [7月23日](../Page/7月23日.md "wikilink")——[亨利·凯里](../Page/亨利·凯里，第1代亨斯顿男爵.md "wikilink")，第一代亨斯顿男爵（出生于[1526年](../Page/1526年.md "wikilink")）
  - [11月1日](../Page/11月1日.md "wikilink")——[皮埃尔·匹陶](../Page/皮埃尔·匹陶.md "wikilink")，法国律师（出生于[1539年](../Page/1539年.md "wikilink")）
  - [11月10日](../Page/11月10日.md "wikilink")——[彼得·温特沃斯](../Page/彼得·温特沃斯.md "wikilink")，英国议会成员（出生于[1530年](../Page/1530年.md "wikilink")）
  - [11月29日](../Page/11月29日.md "wikilink")——[威廉·吉布森](../Page/威廉·吉布森_\(殉教者\).md "wikilink")，英国神学家（出生于[1548年](../Page/1548年.md "wikilink")）
  - 11月29日——[威廉·耐特](../Page/威廉·耐特.md "wikilink")，英国天主教殉教者（出生于[1572年](../Page/1572年.md "wikilink")）

[\*](../Category/1596年.md "wikilink")
[6年](../Category/1590年代.md "wikilink")
[9](../Category/16世纪各年.md "wikilink")