**1694年康熙大地震**，[清](../Page/台灣清治時期.md "wikilink")[康熙卅三年發生於](../Page/康熙.md "wikilink")[台北的大](../Page/台北.md "wikilink")[地震](../Page/地震.md "wikilink")，造成[台北盆地地層嚴重下陷](../Page/台北盆地.md "wikilink")，可能因此形成「[康熙台北湖](../Page/康熙台北湖.md "wikilink")」。

## 歷史

依照學者估計，1694年康熙大地震之震央位於[北緯](../Page/北緯.md "wikilink")25.0度、[東經](../Page/東經.md "wikilink")121.5度，規模高達[芮氏規模](../Page/黎克特制地震震級.md "wikilink")7.0，\[1\]其震度約為6級，發生地點則是在[新店或](../Page/新店區.md "wikilink")[金山斷層](../Page/金山斷層.md "wikilink")。該地震發生的確切時間為1694年4月24日開始，之後大小[餘震不斷](../Page/餘震.md "wikilink")，將近一個月。

該地震讓[台北盆地多處土地瞬間](../Page/台北盆地.md "wikilink")[液化](../Page/土壤液化.md "wikilink")，而地層陷落使得[淡水河水侵入盆地之中](../Page/淡水河.md "wikilink")，產生了深達三至四公尺、面積超過30平方公里以上的「[台北大湖](../Page/康熙台北湖.md "wikilink")」。湖水淹沒區域包含現今[基隆河下游及其北側河道](../Page/基隆河.md "wikilink")、[社子島](../Page/社子島.md "wikilink")、[關渡平原的一部份](../Page/關渡.md "wikilink")\[2\]，亦包括現今[五股](../Page/五股區.md "wikilink")、[三重](../Page/三重區.md "wikilink")、[蘆洲](../Page/蘆洲區.md "wikilink")、[社子島](../Page/社子島.md "wikilink")、[士林](../Page/士林區.md "wikilink")、[北投等地區](../Page/北投區.md "wikilink")，而台北湖的中心大約是現在的三重、蘆洲地區，淹沒時間長達約一百多年。直到清[嘉慶年才因潮汐影響](../Page/嘉慶.md "wikilink")、沈積物增多、降雨量減少和人為的拓墾等原因開始淤塞，形成沙洲及河道，康熙台北湖才慢慢消失。\[3\]不過，今[社子島仍受大湖影響](../Page/社子島.md "wikilink")。

## 裨海紀遊的描述

[郁永河所著](../Page/郁永河.md "wikilink")《[裨海記遊](../Page/裨海紀遊.md "wikilink")》描繪該湖景色為：\[4\]而關於康熙台北湖的成因，郁永河轉述淡水社社長[張大的描述](../Page/張大.md "wikilink")：\[5\]
<small>註釋：所謂的「麻少翁等三社」，即指麻少翁、大浪泵與唭里岸三社。</small>

## 參照

  - [台灣地震列表](../Page/台灣地震列表.md "wikilink")

## 註釋

[Category:台北市歷史](../Category/台北市歷史.md "wikilink")
[Category:台灣清治時期](../Category/台灣清治時期.md "wikilink")
[Category:台灣地震](../Category/台灣地震.md "wikilink")
[Category:1694年](../Category/1694年.md "wikilink")
[Category:1694年地震](../Category/1694年地震.md "wikilink")
[Category:1694年台灣](../Category/1694年台灣.md "wikilink")
[Category:1694年4月](../Category/1694年4月.md "wikilink")

1.  鄭世楠、葉永田，1989，西元1604年至1988年台灣地區地震目錄，中央研究院地球科學研究所，IES-R-661，255頁。

2.  謝英宗，2000，康熙台北湖古地理環境之探討，地理學報，第27期，85-95。

3.

4.  郁永河，《裨海紀遊》，卷中（南投：臺灣省文獻委員會，1994），頁23。

5.