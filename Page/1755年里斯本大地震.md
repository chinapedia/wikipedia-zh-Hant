[1755_Lisbon_earthquake.jpg](https://zh.wikipedia.org/wiki/File:1755_Lisbon_earthquake.jpg "fig:1755_Lisbon_earthquake.jpg")所發生的火災及被[海嘯摧毀的船隻](../Page/海啸.md "wikilink")\]\]

**1755年-{里}-斯本大地震**，又名**里斯本大地震**，發生於1755年11月1日早上9時40分。這場天災造成的破壞和死傷人數在人類史上約排在第11位\[1\]，死亡人數高達約六萬至十萬人。大地震後隨之而來的火災和[海嘯幾乎將整個](../Page/海啸.md "wikilink")[里斯本付之一炬](../Page/里斯本.md "wikilink")，同時也令[葡萄牙的國力嚴重下降](../Page/葡萄牙殖民帝國.md "wikilink")，殖民帝國從此衰落。

现在的[地震学家估计这次地震的](../Page/地震学.md "wikilink")[矩震级达到](../Page/矩震级.md "wikilink")9级，震中位于[圣维森特角之西南偏西方约](../Page/圣维森特角.md "wikilink")200千米的[大西洋中](../Page/大西洋.md "wikilink")。它造成的影响首次被大范围地进行科学化的研究，标志着现代[地震学的诞生](../Page/地震学.md "wikilink")。这次事件也被[启蒙运动的](../Page/啟蒙時代.md "wikilink")[哲学家广泛讨论](../Page/哲学.md "wikilink")，启发了[神义论崇高哲学的发展](../Page/神义论.md "wikilink")。

## 地质背景

1755年-{}-里斯本大地震发生于[亚速尔-直布罗陀转换断层](../Page/亚速尔-直布罗陀转换断层.md "wikilink")，这是欧亚板块与非洲板块的边界，存在部分[俯冲带](../Page/俯冲带.md "wikilink")，因此地震频发。

## 地震經過

[Convento_do_Carmo_ruins_in_Lisbon.jpg](https://zh.wikipedia.org/wiki/File:Convento_do_Carmo_ruins_in_Lisbon.jpg "fig:Convento_do_Carmo_ruins_in_Lisbon.jpg")地震發生生11月1日[諸聖日的早上](../Page/諸聖日.md "wikilink")，現代的研究指出該次地震約持續了三分半鐘至六分鐘，令市中心出現了一條約5米（16英呎）闊的巨大裂縫。當時[芬蘭和北非地區均有震感](../Page/芬蘭.md "wikilink")，有些歷史資料宣稱甚至在[格林蘭](../Page/格林蘭.md "wikilink")\[2\]和[加勒比](../Page/加勒比.md "wikilink")\[3\]地區也有震感。人們紛紛逃至如碼頭等的空旷地方，此時岸邊的海水慢慢退去，水位低得露出海床上已沉沒的船隻和貨物，約四十分鐘後，一場海嘯席捲里斯本，摧毀了碼頭和市中心，海浪甚至衝到[特茹河](../Page/塔霍河.md "wikilink")。而未受海嘯影響的地區也難逃火災的一劫，大火足足燃燒了五天才被撲滅。不過這場大災難破壞的不只是里斯本，在葡萄牙的南部，特別是在[阿爾加威](../Page/阿爾加威.md "wikilink")，災難的破壞也是前所未見的。

[1755_Lisbon_Earthquake_Location.png](https://zh.wikipedia.org/wiki/File:1755_Lisbon_Earthquake_Location.png "fig:1755_Lisbon_Earthquake_Location.png")
整場地震和海嘯在里斯本奪去了9萬條生命（當時里斯本人口約27萬），85%的建築物被毀，當中包括一些著名景點、教堂、圖書館和很多16世紀葡萄牙的特色建築物，如剛建成的鳳凰歌劇院（Phoenix
Opera）、、[里斯本主教座堂和](../Page/里斯本主教座堂.md "wikilink")[卡爾莫修道院等](../Page/卡爾莫修道院.md "wikilink")，而即使在地震中沒有即時倒塌的建築物最終也捱不過火災而被摧毀。另外火災也燒毀了很多珍貴的資料，包括著名航海家[瓦斯科·達·伽馬的詳細航海記錄](../Page/瓦斯科·達·伽馬.md "wikilink")。現在卡爾莫修道院的遺址仍被保留在里斯本內，以讓世人認識這場歷史大災難。

現代很多人認為動物能夠預測地震，在地震之前就會逃到高處，里斯本大地震前就有人記錄了這個現象，這也是歐洲首個對此現象的記錄。

## 地震後的情況

[Lisbon1755hanging.jpg](https://zh.wikipedia.org/wiki/File:Lisbon1755hanging.jpg "fig:Lisbon1755hanging.jpg")。\]\]

當時的王室人員幸運地逃過一劫，國王[若澤一世和及其大臣在早上彌撒後就暫時離開了里斯本](../Page/若澤一世_\(葡萄牙\).md "wikilink")。若澤一世在地震後不敢住在石牆之下，他的大臣則在市郊的阿諸打山（Hills
of Ajuda）上搭建了大型的帳篷供他居住。國家顧問 Marquês de Pombal（或 Conde de Oeiras
Sebastião José de Carvalho e
Melo）也在地震中幸存，且立即提出重建計劃，他派人到城內滅火和移走屍體以防止疫症爆發，他把屍體放進船隻之中並於[特茹河口進行](../Page/塔霍河.md "wikilink")[海葬](../Page/海葬.md "wikilink")，而這種方法並不符合當時教會的習慣。另外，為了維持城內的秩序和防止有人趁火打劫，他也在城市附近的高處設置了很多絞刑架以作警示，但也有至少34人因此而被吊死。而軍隊也禁止人們離開市郊的範圍，並挑選強健的人協助清理瓦礫。

國王和首相也聘請了很多建築物和工程師進行重建，更藉此機會重新規劃城市，他們興建了新的市中心、大量的廣場和擴闊道路，不足一年時間，里斯本已漸恢復規模。

## 地震學的誕生

首相除了進行重建外，還對各個[堂區因地震而影響的情況進行了咨詢](../Page/堂區.md "wikilink")，問題包括：

  - 地震持續了多久？
  - 地震後出現了多少次餘震？
  - 地震如何產生破壞？
  - 動物的表現有否不正常？
  - 水井內有甚麼現象發生？

當時對這些問題的答案現在還存放於[葡萄牙國家檔案館](../Page/葡萄牙國家檔案館.md "wikilink")。通過這些數據，現代科學家就能對這次地震進行重組。假如當時馬盧沒有進行咨詢，人們就不能了解這次地震的經過。因為龐巴爾侯爵是第一個對地震的經過和結果進行客觀科學描述的人，他也被認為是現代[地震學的先驅](../Page/地震學.md "wikilink")。

而這次地震的成因，現代的很多科學家還在爭議之中，但經過與其他涉及[隱沒帶和](../Page/隱沒帶.md "wikilink")[矩震級高於](../Page/矩震級.md "wikilink")9的地震比對後，專家也認為里斯本大地震是和[大西洋的](../Page/大西洋.md "wikilink")[隱沒帶有關的](../Page/隱沒帶.md "wikilink")。

## 對哲學的影響

這次地震令里斯本這個[天主教國家的首都幾乎毀於一旦](../Page/天主教.md "wikilink")，也摧毀了城內很多重要的教堂，整個18世紀，[神學家和](../Page/神學.md "wikilink")[哲學家還未能解釋這是否激怒神的結果](../Page/哲学.md "wikilink")。而地震也影響了很多歐洲[啟蒙運動哲學家和思想家如](../Page/啟蒙時代.md "wikilink")[盧梭和](../Page/让-雅克·卢梭.md "wikilink")[康德](../Page/伊曼努尔·康德.md "wikilink")，而康德更發表了三篇文章試圖解釋地震的成因。

## 流行文化

此地震在遊戲《[刺客教條：叛變](../Page/刺客教條：叛變.md "wikilink")》劇情中佔有重要地位。劇情中主角因奉命取走聖物而引發這場地震。主角亦因此事而對刺客組織產生質疑，成為後來劇情發展的重要轉折點。

## 參看

  - [地震列表](../Page/地震列表.md "wikilink")
  - [里斯本](../Page/里斯本.md "wikilink")

## 参考文献

  - [瓦爾特·本雅明](../Page/瓦爾特·本雅明.md "wikilink")。"The Lisbon
    Earthquake."，出自《Selected Writings》：vol. 2。Belknap
    Press，1999年。ISBN 0-674-94586-7。

## 外部連結

  - [里斯本大地震文字敘述](https://web.archive.org/web/20031204233731/http://nisee.berkeley.edu/lisbon/)

  - [里斯本大地震的破壞](http://www.lisbon-and-portugal.com/travel/1755-lisbon-earthquake.html)

  - [里斯本大地震的圖片](http://nisee.berkeley.edu/elibrary/browse/kozak?eq=5234)


[Category:1755年](../Category/1755年.md "wikilink")
[Category:葡萄牙灾难](../Category/葡萄牙灾难.md "wikilink")
[Category:歐洲地震](../Category/歐洲地震.md "wikilink")
[Category:18世纪地震](../Category/18世纪地震.md "wikilink")
[Category:海嘯](../Category/海嘯.md "wikilink")

1.  [历史上死亡人数最多的十次大地震
    中国三次上榜(图)_新闻频道_中华网](http://news.china.com/history/all/11025807/20170809/31060385_all.html)
2.  Brockhaus' Konversations-Lexikon. 14th ed., Leipzig, Berlin and
    Vienna 1894; Vol. 6, p. 248
3.  Lyell, Charles. *Principles of Geology*. 1830. Vol. 1, chapter 25,
    p. 439 [Online electronic
    edition](http://www.esp.org/books/lyell/principles/facsimile/contents/lyell-v1-ch25.pdf).
    Accessed 2009-05-19.  2009-05-21.