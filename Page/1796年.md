## 大事记

  - [乾隆帝讓位於其十五子](../Page/乾隆帝.md "wikilink")[嘉慶帝](../Page/嘉庆帝.md "wikilink")，以[太上皇身份繼續執掌朝政](../Page/太上皇.md "wikilink")，直至[1799年逝世](../Page/1799年.md "wikilink")。
  - [川楚教亂](../Page/川楚教亂.md "wikilink")，[白莲教在湖北宜都](../Page/白蓮教.md "wikilink")、枝江起义。
  - [高斯得到了一个数学史上极重要的结果](../Page/卡爾·弗里德里希·高斯.md "wikilink")，即《[正十七边形尺规作图之理论与方法](../Page/正十七边形尺规作图.md "wikilink")》。
  - [英國東印度公司從](../Page/英國東印度公司.md "wikilink")[荷蘭手中奪得](../Page/荷蘭.md "wikilink")[錫蘭島的沿海地區](../Page/錫蘭島.md "wikilink")
  - [卡扎爾王朝首領](../Page/卡扎爾王朝.md "wikilink")[阿迦·穆罕默德·汗加冕為](../Page/阿迦·穆罕默德·汗.md "wikilink")[伊朗國王](../Page/伊朗.md "wikilink")，建都[德黑蘭](../Page/德黑蘭.md "wikilink")，同年征服[呼羅珊](../Page/大呼罗珊.md "wikilink")。
  - [2月1日](../Page/2月1日.md "wikilink")——[上加拿大的首府由](../Page/上加拿大.md "wikilink")[紐瓦克鎮移至](../Page/紐瓦克鎮.md "wikilink")[約克鎮](../Page/約克鎮.md "wikilink")，以遠離[美國的威脅](../Page/美國.md "wikilink")。
  - [3月9日](../Page/3月9日.md "wikilink")——[拿破崙·波拿巴與約瑟菲娜](../Page/拿破崙·波拿巴.md "wikilink")·德博阿爾內結婚，後者成為其第一位夫人。
  - [4月5日](../Page/4月5日.md "wikilink")——[彼得三世與](../Page/彼得三世_\(俄羅斯\).md "wikilink")[葉卡捷琳娜二世之子](../Page/葉卡捷琳娜二世.md "wikilink")[保羅一世繼位成為](../Page/保羅一世_\(俄羅斯\).md "wikilink")[俄羅斯沙皇](../Page/俄羅斯沙皇.md "wikilink")。
  - [4月21日](../Page/4月21日.md "wikilink")——[蒙多維戰役](../Page/蒙多維戰役.md "wikilink")，[拿破崙與](../Page/拿破崙.md "wikilink")[薩丁尼亞王國會戰於](../Page/薩丁尼亞王國.md "wikilink")[皮埃蒙特並取得勝利](../Page/皮埃蒙特.md "wikilink")。
  - [5月](../Page/5月.md "wikilink")——[巴貝夫](../Page/巴貝夫.md "wikilink")(Babeuf)組織一場一萬七千人的起義，推翻大資產階級領導的督政府，由於事蹟不密被毀於萌芽之初，[巴貝夫本人被推上斷頭臺](../Page/巴貝夫.md "wikilink")。
  - [5月10日](../Page/5月10日.md "wikilink")——[洛迪戰役](../Page/洛迪戰役.md "wikilink")，[拿破崙於](../Page/拿破崙.md "wikilink")[洛迪](../Page/洛迪.md "wikilink")(位於[義大利](../Page/義大利.md "wikilink"))擊敗[奧地利](../Page/奧地利.md "wikilink")。
  - [8月6日](../Page/8月6日.md "wikilink")——[卡斯奇里恩戰役](../Page/卡斯奇里恩戰役.md "wikilink")，[拿破崙率領的法軍在卡斯奇里恩](../Page/拿破崙.md "wikilink")(位於[義大利](../Page/義大利.md "wikilink"))擊敗[奧地利](../Page/奧地利.md "wikilink")。

## 出生

[Sadi_Carnot.jpeg](https://zh.wikipedia.org/wiki/File:Sadi_Carnot.jpeg "fig:Sadi_Carnot.jpeg")

  - [尼古拉·卡诺](../Page/尼古拉·卡诺.md "wikilink") - Nicolas
    Carnot，（1796年－1832年），[法国物理学家](../Page/法国.md "wikilink")、工程师。在[热学方面有一定贡献](../Page/热学.md "wikilink")。
  - [7月26日](../Page/7月26日.md "wikilink")——[讓-巴蒂斯·卡米耶·柯洛](../Page/讓-巴蒂斯·卡米耶·柯洛.md "wikilink")，法國著名的[巴比松派](../Page/巴比松派.md "wikilink")[畫家](../Page/畫家.md "wikilink")。

## 逝世

  - [5月27日](../Page/5月27日.md "wikilink")——[巴貝夫](../Page/巴貝夫.md "wikilink")，法國大革命時期的政治活動者和記者
  - [11月17日](../Page/11月17日.md "wikilink")——[葉卡捷琳娜二世](../Page/葉卡捷琳娜二世.md "wikilink")

[\*](../Category/1796年.md "wikilink")
[6年](../Category/1790年代.md "wikilink")
[9](../Category/18世纪各年.md "wikilink")