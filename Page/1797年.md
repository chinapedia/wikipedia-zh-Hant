## 大事记

  - [麥卡琵](../Page/麥卡琵.md "wikilink")（J.
    Macarthur）由[好望角將](../Page/好望角.md "wikilink")[美麗諾羊](../Page/美麗諾羊.md "wikilink")（Merino
    sheep）引進[澳洲](../Page/澳大利亚.md "wikilink")。
  - [法蘭西第一共和國立法團選舉中](../Page/法蘭西第一共和國.md "wikilink")，保王黨人大獲全勝，勢力直接威脅督政府。
  - [伊朗](../Page/伊朗.md "wikilink")[卡扎爾王朝](../Page/卡扎爾王朝.md "wikilink")[阿迦·穆罕默德·汗被宮中侍衛殺害](../Page/阿迦·穆罕默德·汗.md "wikilink")，姪子[法特赫-阿里沙·卡扎爾繼位](../Page/法特赫-阿里沙·卡扎爾.md "wikilink")。
  - [1月7日](../Page/1月7日.md "wikilink")——[奇斯帕達納共和國的議會決定使用紅](../Page/奇斯帕達納共和國.md "wikilink")、白、綠[三色旗作為](../Page/三色旗.md "wikilink")[國旗](../Page/國旗.md "wikilink")，後來演變成[義大利國旗](../Page/義大利國旗.md "wikilink")。
  - [1月15日](../Page/1月15日.md "wikilink")——[里沃利會戰](../Page/里沃利會戰.md "wikilink")，[拿破崙在里沃利](../Page/拿破崙.md "wikilink")(位於[義大利](../Page/義大利.md "wikilink"))擊敗[奧地利](../Page/奧地利.md "wikilink")，瓦解[第一次反法同盟](../Page/第一次反法同盟.md "wikilink")。
  - [2月14日](../Page/2月14日.md "wikilink")——[聖文生角戰役
    (1797年)](../Page/聖文生角戰役_\(1797年\).md "wikilink")，[英國](../Page/英國.md "wikilink")[皇家海軍於](../Page/皇家海軍.md "wikilink")[葡萄牙](../Page/葡萄牙.md "wikilink")[聖文生角擊敗](../Page/聖文生角.md "wikilink")[西班牙海軍](../Page/西班牙.md "wikilink")。
  - [3月4日](../Page/3月4日.md "wikilink")——第一任[美國總統](../Page/美國總統.md "wikilink")[喬治·華盛頓](../Page/喬治·華盛頓.md "wikilink")[退休](../Page/退休.md "wikilink")，[约翰·亚当斯接替他成為](../Page/约翰·亚当斯.md "wikilink")[美國第](../Page/美國.md "wikilink")2任[總統](../Page/總統.md "wikilink")。
  - [9月4日](../Page/9月4日.md "wikilink")——在[法國督政府的授意下](../Page/法國.md "wikilink")，[拿破崙發動政變](../Page/拿破崙.md "wikilink")，史稱[果月政變](../Page/果月政變.md "wikilink")。
  - [10月17日](../Page/10月17日.md "wikilink")——[法國與](../Page/法國.md "wikilink")[奧地利簽署](../Page/奧地利.md "wikilink")[坎波福爾米奧條約](../Page/坎波福爾米奧條約.md "wikilink")，象徵著[第一次反法同盟的崩潰](../Page/第一次反法同盟.md "wikilink")、[拿破崙於](../Page/拿破崙.md "wikilink")[義大利戰場的最終勝利和第一波的](../Page/義大利.md "wikilink")[法國大革命戰爭終結](../Page/法國大革命戰爭.md "wikilink")。
  - [10月22日](../Page/10月22日.md "wikilink")——[法国青年](../Page/法国.md "wikilink")[安德烈-雅克·加纳林在](../Page/安德烈-雅克·加纳林.md "wikilink")[巴黎借助热气球升至](../Page/巴黎.md "wikilink")100米的高空，完成人类史上首次成功的跳伞。

## 出生

  - [1月31日](../Page/1月31日.md "wikilink")——[舒伯特](../Page/弗朗茨·舒伯特.md "wikilink")，[奧地利作曲家](../Page/奥地利.md "wikilink")、有＂歌曲之王＂的稱號。（[1828年逝世](../Page/1828年.md "wikilink")）
  - [8月30日](../Page/8月30日.md "wikilink")——[瑪麗·雪莱](../Page/瑪麗·雪莱.md "wikilink")，英国作家（[1851年逝世](../Page/1851年.md "wikilink")）
  - [12月13日](../Page/12月13日.md "wikilink")——[海因里希·海涅](../Page/海因里希·海涅.md "wikilink")，[德国革命民族主義詩人](../Page/德国.md "wikilink")（[1856年逝世](../Page/1856年.md "wikilink")）
  - [歌川広重](../Page/歌川広重.md "wikilink")（Utagawa
    Hiroshige）──[日本](../Page/日本.md "wikilink")[畫家](../Page/畫家.md "wikilink")

## 逝世

  - [阿迦·穆罕默德·汗](../Page/阿迦·穆罕默德·汗.md "wikilink")，[伊朗](../Page/伊朗.md "wikilink")[卡扎爾王朝的建立者](../Page/卡扎爾王朝.md "wikilink")。

[\*](../Category/1797年.md "wikilink")
[7年](../Category/1790年代.md "wikilink")
[9](../Category/18世纪各年.md "wikilink")