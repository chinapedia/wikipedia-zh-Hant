## 大事记

  - **[中國](../Page/中國.md "wikilink")**
      - [三月](../Page/三月.md "wikilink")，[北宮伯玉等寇](../Page/北宫伯玉.md "wikilink")[三輔](../Page/三輔.md "wikilink")，以左[車騎將軍](../Page/車騎將軍.md "wikilink")[皇甫嵩鎮](../Page/皇甫嵩.md "wikilink")[長安討之](../Page/長安.md "wikilink")。
      - [十一月](../Page/十一月.md "wikilink")，[董卓大破](../Page/董卓.md "wikilink")[邊章](../Page/边章.md "wikilink")、[韓遂軍](../Page/韓遂.md "wikilink")。
      - [丹陽郡新設](../Page/丹陽郡.md "wikilink")[安吉](../Page/安吉县.md "wikilink")、[原鄉二縣](../Page/原乡县.md "wikilink")。
      - [月氏](../Page/月氏.md "wikilink")[僧](../Page/僧侣.md "wikilink")[支曜抵](../Page/支曜.md "wikilink")[洛阳](../Page/洛阳市.md "wikilink")，先后译出《[成具光明定意经](../Page/成具光明定意经.md "wikilink")》等大小乘经十部十一卷。
  - **[羅馬帝國](../Page/羅馬帝國.md "wikilink")**
      - [不列顛貴族決定使](../Page/不列顛尼亞_\(羅馬行省\).md "wikilink")[皇帝](../Page/羅馬皇帝.md "wikilink")[康茂德收回](../Page/康茂德.md "wikilink")[禁衛軍長官](../Page/禁衛軍長官.md "wikilink")[佩瑞尼斯](../Page/佩瑞尼斯.md "wikilink")（Tigidius
        Perennis）的所有權力。
      - 寢宮侍從長[克里安德](../Page/克里安德.md "wikilink")（Marcus Aurelius
        Cleander）觊觎权位，进言康茂德，以谋反罪处决佩瑞尼斯及其家人後，取代佩瑞尼斯，成為康茂德的決策者。
      - [佩蒂奈克斯](../Page/佩蒂纳克斯.md "wikilink")（Publius Helvius
        Pertinax）成為不列顛尼亞[總督](../Page/羅馬總督.md "wikilink")，并平息了不列顛[軍團的嘩變](../Page/羅馬軍團.md "wikilink")，嘩變士兵擁立長官[普里斯克庫斯為他們新皇帝](../Page/普里斯克庫斯.md "wikilink")。
      - 皇帝康茂德耗盡庫銀用於[角鬥士表演](../Page/角鬥士.md "wikilink")，并時常于[馬克西穆斯競技場親自下場與角鬥士或野獸搏鬥](../Page/馬克西穆斯競技場.md "wikilink")。
  - 其他
      - [十月](../Page/十月.md "wikilink")，[半人馬座附近的](../Page/半人馬座.md "wikilink")[超新星](../Page/超新星.md "wikilink")[南门客星爆發](../Page/SN_185.md "wikilink")，為《[後漢書](../Page/後漢書.md "wikilink")·卷十二·天文下》\[1\]所載，是人類記錄到的第一顆超新星。
      - [希臘](../Page/古希臘.md "wikilink")[天文學家](../Page/天文學家.md "wikilink")[克萊門德](../Page/克萊門德.md "wikilink")(Cleomedes)發現[地球大氣的](../Page/地球大氣層.md "wikilink")[折射現象](../Page/折射.md "wikilink")，參見[月球錯覺](../Page/月球錯覺.md "wikilink")。
      - [神學家](../Page/神學家.md "wikilink")[愛任紐提出](../Page/爱任纽.md "wikilink")[神所啟示的僅有四部](../Page/神.md "wikilink")[福音書](../Page/福音书.md "wikilink")。（大致年代）

## 出生

  - [奧利金](../Page/奧利金.md "wikilink")（約185年－[251年](../Page/251年.md "wikilink")），[基督教著名的神學家和](../Page/基督教.md "wikilink")[哲學家](../Page/哲學家.md "wikilink")，66岁。
  - [王祥](../Page/王祥.md "wikilink")（185年－269年），[魏](../Page/曹魏.md "wikilink")[司空](../Page/司空.md "wikilink")、[太尉](../Page/太尉.md "wikilink")，[晉](../Page/西晉.md "wikilink")[太保](../Page/太保.md "wikilink")。《[二十四孝](../Page/二十四孝.md "wikilink")》臥冰求鯉的主角，84岁。

## 逝世

  - [佩瑞尼斯](../Page/佩瑞尼斯.md "wikilink")（Tigidius
    Perennis），[禁衛軍長官](../Page/禁衛軍長官.md "wikilink")。
  - [聖達圖斯](../Page/聖達圖斯.md "wikilink")（S.
    Datus），[拉文納](../Page/拉文納.md "wikilink")[主教](../Page/主教.md "wikilink")，[殉道者](../Page/殉道者.md "wikilink")。

## 參考資料

[\*](../Category/185年.md "wikilink")
[5年](../Category/180年代.md "wikilink")
[8](../Category/2世纪各年.md "wikilink")

1.  《[后汉书](../Page/后汉书.md "wikilink")·卷十二·天文下》：“中平二年十月癸亥，客星出南门中，大如半筵，五色喜怒稍小，至后年六月消。占曰：‘为兵。’至六年，司隶校尉袁绍诛灭中官，大将军部曲将吴匡攻杀车骑将军何苗，死者数千人。”