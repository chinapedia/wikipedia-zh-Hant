## 大事记

  - [阿尔弗雷德·诺贝尔发明](../Page/阿尔弗雷德·诺贝尔.md "wikilink")[硝化甘油炸药](../Page/硝化甘油炸药.md "wikilink")。
  - [列支敦斯登独立](../Page/列支敦斯登.md "wikilink")。
  - [黄崖山惨案](../Page/黄崖山惨案.md "wikilink")。
  - [维尔纳·冯·西门子提出了发电机的工作原理](../Page/维尔纳·冯·西门子.md "wikilink")，西门子公司的工程师完成了人类第一台[交流发电机](../Page/交流发电机.md "wikilink")。
  - [舍門將軍號事件](../Page/舍門將軍號事件.md "wikilink")
  - 2月——[太平军](../Page/太平天国.md "wikilink")[李世贤](../Page/李世贤.md "wikilink")、[汪海洋部被消灭](../Page/汪海洋.md "wikilink")。
  - [3月7日](../Page/3月7日.md "wikilink")——[薩長同盟](../Page/薩長同盟.md "wikilink")，在[薩摩藩的](../Page/薩摩藩.md "wikilink")[西鄉隆盛](../Page/西鄉隆盛.md "wikilink")、[小松帶刀和](../Page/小松帶刀.md "wikilink")[長州藩的](../Page/長州藩.md "wikilink")[木戶孝允在](../Page/木戶孝允.md "wikilink")[京都組成倒幕秘密軍事同盟](../Page/京都.md "wikilink")，[土佐藩的脫藩浪人](../Page/土佐藩.md "wikilink")[坂本龍馬和](../Page/坂本龍馬.md "wikilink")[中岡慎太郎居中撮合](../Page/中岡慎太郎.md "wikilink")。
  - 6月——港元开始被使用。
  - [6月14日](../Page/6月14日.md "wikilink")——[普奥战争爆发](../Page/普奥战争.md "wikilink")。
  - [7月3日](../Page/7月3日.md "wikilink")——普奥战争：[克尼格雷茨战役](../Page/克尼格雷茨战役.md "wikilink")，[普鲁士战败](../Page/普魯士.md "wikilink")[奥地利](../Page/奥地利.md "wikilink")。
  - [7月27日](../Page/7月27日.md "wikilink")——第一次跨[大西洋](../Page/大西洋.md "wikilink")[电报通讯](../Page/电报.md "wikilink")。
  - [8月19日](../Page/8月19日.md "wikilink")——[左宗棠在福州馬尾設立福州船政局](../Page/左宗棠.md "wikilink")。福州船政局設有鐵廠、馬尾造船廠和船政學堂，是中國近代第一個新式造船廠。
  - [8月23日](../Page/8月23日.md "wikilink")——[布拉格条约](../Page/布拉格条约.md "wikilink")，普奥战争结束，[德意志邦聯宣布解散](../Page/德意志邦聯.md "wikilink")。
  - 8月，[日本第二次](../Page/日本.md "wikilink")[长州征讨終結](../Page/长州征讨.md "wikilink")，幕府大敗。
  - [12月7日](../Page/12月7日.md "wikilink")——[清朝任命](../Page/清朝.md "wikilink")[李鸿章为钦差大臣对](../Page/李鴻章.md "wikilink")[捻军作战](../Page/捻军.md "wikilink")。
  - [12月23日](../Page/12月23日.md "wikilink")——[船政學堂開始招生](../Page/船政學堂.md "wikilink")。

## 出生

  - [1月15日](../Page/1月15日.md "wikilink")——[纳坦·瑟德布卢姆](../Page/纳坦·瑟德布卢姆.md "wikilink")，瑞典神学家（逝世于[1931年](../Page/1931年.md "wikilink")）
  - [1月29日](../Page/1月29日.md "wikilink")——[罗曼·罗兰](../Page/罗曼·罗兰.md "wikilink")，法国作家（逝世于[1944年](../Page/1944年.md "wikilink")）
  - [5月17日](../Page/5月17日.md "wikilink")——[艾瑞克·薩提](../Page/艾瑞克·薩提.md "wikilink")，法國作曲家（逝世于[1925年](../Page/1925年.md "wikilink")）
  - [5月29日](../Page/5月29日.md "wikilink")──[吳趼人](../Page/吳沃堯.md "wikilink")，中國晚清四大小說家之一（逝世於[1910年](../Page/1910年.md "wikilink")）
  - [9月21日](../Page/9月21日.md "wikilink")——[赫伯特·乔治·威尔斯](../Page/赫伯特·乔治·威尔斯.md "wikilink")，英国作家（逝世于[1946年](../Page/1946年.md "wikilink")）
  - [9月21日](../Page/9月21日.md "wikilink")——[查爾斯·尼柯爾](../Page/查爾斯·尼柯爾.md "wikilink")，[法国细菌学家](../Page/法国.md "wikilink")
  - [11月12日](../Page/11月12日.md "wikilink")——[孙中山](../Page/孫中山.md "wikilink")，，中華民國創立者，中国政治家、民主革命家（逝世于[1925年](../Page/1925年.md "wikilink")，1940年被尊稱為[中華民國國父](../Page/中華民國國父.md "wikilink")）
  - [12月16日](../Page/12月16日.md "wikilink")（[儒略曆](../Page/儒略曆.md "wikilink")[12月4日](../Page/12月4日.md "wikilink")）——[瓦西里·康定斯基](../Page/瓦西里·康定斯基.md "wikilink")，俄罗斯画家（逝世于[1944年](../Page/1944年.md "wikilink")）
  - [喬治·傑生](../Page/喬治·傑生.md "wikilink")——[丹麥銀器藝術家](../Page/丹麦.md "wikilink")。

## 逝世

  - [7月20日](../Page/7月20日.md "wikilink")——[波恩哈德·黎曼](../Page/波恩哈德·黎曼.md "wikilink")，德国数学家（出生于[1826年](../Page/1826年.md "wikilink")）

[\*](../Category/1866年.md "wikilink")
[6年](../Category/1860年代.md "wikilink")
[6](../Category/19世纪各年.md "wikilink")