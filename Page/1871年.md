## 大事记

  - [同治](../Page/同治.md "wikilink")10年（1871年），近代《[星子县志](../Page/星子县志.md "wikilink")》成书，由（清）蓝煦、（清）徐鸣皋修、（清）曹征甲纂。
  - 1月6日——寧夏金積堡回軍首領[馬化龍向清軍投降](../Page/馬化龍.md "wikilink")。\[1\]
  - [1月18日](../Page/1月18日.md "wikilink")——[普魯士國王](../Page/普魯士.md "wikilink")[威廉一世在](../Page/威廉一世_\(德国\).md "wikilink")[法國](../Page/法國.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")[凡爾賽宮登基為皇帝](../Page/凡爾賽宮.md "wikilink")，[德意志帝国建立](../Page/德意志帝国.md "wikilink")，[德国统一](../Page/德国.md "wikilink")。
  - [1月25日](../Page/1月25日.md "wikilink")——[法屬印度獲准選舉成立自己的普通議會及地方議會](../Page/法屬印度.md "wikilink")。
  - [3月18日](../Page/3月18日.md "wikilink")——[巴黎工人階級武裝起義爆發](../Page/巴黎工人.md "wikilink")。
  - [3月28日](../Page/3月28日.md "wikilink")——[法國](../Page/法國.md "wikilink")[巴黎公社成立](../Page/巴黎公社.md "wikilink")。
  - [5月10日](../Page/5月10日.md "wikilink")——[普法戰爭結束](../Page/普法戰爭.md "wikilink")，根據[法蘭克福條約](../Page/法蘭克福條約.md "wikilink")，[法國归还](../Page/法國.md "wikilink")[阿爾薩斯和](../Page/阿爾薩斯.md "wikilink")[洛林给德国](../Page/洛林.md "wikilink")。
  - [5月15日](../Page/5月15日.md "wikilink")——[德國頒佈](../Page/德國.md "wikilink")[第175條](../Page/第175條.md "wikilink")，開始了德國迫害[同性戀者的歷史](../Page/同性戀者.md "wikilink")。直至[1994年條例廢除之前](../Page/1994年.md "wikilink")，因此條例而受害或被殺的同性戀者數以萬計。
  - [8月29日](../Page/8月29日.md "wikilink")——[日本實行](../Page/日本.md "wikilink")[廢藩置縣](../Page/廢藩置縣.md "wikilink")。
  - [8月31日](../Page/8月31日.md "wikilink")——[法國議會授予](../Page/法國.md "wikilink")[梯也爾](../Page/梯也爾.md "wikilink")「共和國總統」稱號，成為[法蘭西第三共和國首任總統](../Page/法蘭西第三共和國.md "wikilink")。
  - [9月13日](../Page/9月13日.md "wikilink")——《[日清修好條規](../Page/中日通商章程.md "wikilink")》由[李鴻章與](../Page/李鴻章.md "wikilink")[伊達宗城在](../Page/伊達宗城.md "wikilink")[天津签订](../Page/天津.md "wikilink")，共有18條，主要內容為雙方交換使節、互相承認有限制領事裁判權，通商事宜仿效歐美待遇，互相承認\[2\]。
  - [12月24日](../Page/12月24日.md "wikilink")——[朱塞佩·威尔第的著名歌劇](../Page/朱塞佩·威尔第.md "wikilink")《[阿依達](../Page/阿依達.md "wikilink")》在[埃及](../Page/埃及.md "wikilink")[開羅的歌劇院](../Page/開羅.md "wikilink")（[Khedivial
    Opera House](../Page/w:en:Khedivial_Opera_House.md "wikilink")）首演。

## 出生

  - [7月10日](../Page/7月10日.md "wikilink")——[普鲁斯特](../Page/普鲁斯特.md "wikilink")，法国文学家
  - [7月23日](../Page/7月23日.md "wikilink")——（日本舊農曆6月6日是錯誤），創價學會第一任會長(逝世[1944年](../Page/1944年.md "wikilink"))
  - [8月14日](../Page/8月14日.md "wikilink")——[愛新覺羅載湉](../Page/愛新覺羅載湉.md "wikilink")，即[光緒帝](../Page/光緒帝.md "wikilink")（逝世[1908年](../Page/1908年.md "wikilink")）
  - [8月19日](../Page/8月19日.md "wikilink")——[奥维尔·莱特](../Page/奥维尔·莱特.md "wikilink")，美国飞机设计师和发明家（逝世[1948年](../Page/1948年.md "wikilink")）
  - [8月30日](../Page/8月30日.md "wikilink")──[恩那斯特·盧瑟福](../Page/恩那斯特·盧瑟福.md "wikilink")（Ernest
    Rutherford），物理科學家，生於[新西蘭](../Page/新西蘭.md "wikilink")

## 逝世

  -
## 參考文獻

[\*](../Category/1871年.md "wikilink")
[1年](../Category/1870年代.md "wikilink")
[7](../Category/19世纪各年.md "wikilink")

1.  [刘锦棠与清军收复新疆之战](http://www.xjass.com/ls/content/2010-09/05/content_163330.htm)
    ，新疆哲学社会科学网，2010年9月5日。
2.