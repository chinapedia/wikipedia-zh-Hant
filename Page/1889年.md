## 大事记

  - [光绪皇帝舉行大婚](../Page/光緒.md "wikilink")，大婚後亲政。
  - [1月1日](../Page/1月1日.md "wikilink")——[美国](../Page/美國.md "wikilink")[纽约州引入](../Page/纽约州.md "wikilink")[电刑](../Page/电刑.md "wikilink")。
  - [1月8日](../Page/1月8日.md "wikilink")——[赫尔曼·霍勒瑞斯为他发明的](../Page/赫尔曼·霍勒瑞斯.md "wikilink")[穿孔卡系统获得](../Page/穿孔卡系统.md "wikilink")[专利](../Page/专利.md "wikilink")。
  - [1月15日](../Page/1月15日.md "wikilink")——[聯邦政府農業司改制為](../Page/聯邦政府.md "wikilink")[美國農業部](../Page/美國農業部.md "wikilink")。
  - [2月9日](../Page/2月9日.md "wikilink")——[普雷斯顿贏得首屆](../Page/普雷斯顿足球俱乐部.md "wikilink")[英格蘭足球聯賽冠軍](../Page/英格兰足球联赛.md "wikilink")，是世界上首支奪得職業足球聯賽冠軍的球隊。
  - [2月11日](../Page/2月11日.md "wikilink")——[日本公佈](../Page/日本.md "wikilink")[大日本帝國憲法](../Page/大日本帝國憲法.md "wikilink")。
  - [3月4日](../Page/3月4日.md "wikilink")——[本杰明·哈里森继](../Page/本杰明·哈里森.md "wikilink")[格罗弗·克利夫兰擔任美国第](../Page/格罗弗·克利夫兰.md "wikilink")24任总统。
  - [3月9日](../Page/3月9日.md "wikilink")——[孟尼利克二世继位为](../Page/孟尼利克二世.md "wikilink")[埃塞俄比亚皇帝](../Page/埃塞俄比亚.md "wikilink")。
  - [3月31日](../Page/3月31日.md "wikilink")——[埃菲尔铁塔落成](../Page/艾菲爾鐵塔.md "wikilink")。
  - [5月15日](../Page/5月15日.md "wikilink")——埃菲尔铁塔正式开放。
  - [7月8日](../Page/7月8日.md "wikilink")——[华尔街日报首刊](../Page/华尔街日报.md "wikilink")。
  - [7月14日](../Page/7月14日.md "wikilink")——在[恩格斯指导下](../Page/弗里得里希·恩格斯.md "wikilink")[国际社会主义者代表大会在](../Page/国际社会主义者代表大会.md "wikilink")[巴黎召开](../Page/巴黎.md "wikilink")，[第二国际宣告成立](../Page/第二国际.md "wikilink")，并决议每年[5月1日为](../Page/5月1日.md "wikilink")[国际劳动节](../Page/国际劳动节.md "wikilink")。
  - [11月2日](../Page/11月2日.md "wikilink")——[南达科他州和](../Page/南達科他州.md "wikilink")[北达科他州分別成为](../Page/北达科他州.md "wikilink")[美国的第](../Page/美國.md "wikilink")39和40個州。
  - [11月8日](../Page/11月8日.md "wikilink")——[蒙大拿州成为美国的第](../Page/蒙大拿州.md "wikilink")41州。
  - [11月11日](../Page/11月11日.md "wikilink")——[华盛顿州成为美国的第](../Page/华盛顿州.md "wikilink")42州。
  - [11月15日](../Page/11月15日.md "wikilink")——[巴西宣布成为](../Page/巴西.md "wikilink")[共和国](../Page/共和制.md "wikilink")。
  - 1889年 第一届国际计量大会批准用铂铱合金制作的最接近国际千克原器作为质量的标准
  - 德国莱茵金属公司成立。
  - 哥伦比亚唱片公司成立

## 科学

  - 德国开始出版第一部《数学百科全书》。
  - 高尔顿(Galton
    F)在[统计学中引入相关与回归等概念](../Page/统计学.md "wikilink")，开创了[生物统计学](../Page/生物统计学.md "wikilink")。

## 出生

  - [2月23日](../Page/2月23日.md "wikilink")——[维克托·弗莱明](../Page/维克托·弗莱明.md "wikilink")，美国导演（逝世于[1949年](../Page/1949年.md "wikilink")）
  - [4月16日](../Page/4月16日.md "wikilink")——[查理·卓别林](../Page/查理·卓别林.md "wikilink")，英国喜劇电影[演员和](../Page/演員.md "wikilink")[导演](../Page/导演.md "wikilink")（逝世于[1977年](../Page/1977年.md "wikilink")）
  - [4月20日](../Page/4月20日.md "wikilink")——[阿道夫·希特勒](../Page/阿道夫·希特勒.md "wikilink")，[德意志第三帝国总理](../Page/德意志第三帝国.md "wikilink")、[国家社会主义德国工人党领袖](../Page/国家社会主义德国工人党.md "wikilink")（逝世于[1945年](../Page/1945年.md "wikilink")）
  - [4月26日](../Page/4月26日.md "wikilink")——[路德维奇·维特根斯坦](../Page/路德维希·维特根斯坦.md "wikilink")，奥地利[哲学家](../Page/哲学家.md "wikilink")（逝世于[1951年](../Page/1951年.md "wikilink")）
  - [7月18日](../Page/7月18日.md "wikilink")——[木戶幸一](../Page/木戶幸一.md "wikilink")，日本政治家。（逝世於1977年）
  - [8月7日](../Page/8月7日.md "wikilink")——[里昂·布里於因](../Page/里昂·布里於因.md "wikilink")，法國[物理學家](../Page/物理学家.md "wikilink")（逝世于[1969年](../Page/1969年.md "wikilink")）
  - [9月26日](../Page/9月26日.md "wikilink")——[马丁·海德格](../Page/马丁·海德格.md "wikilink")，德国[哲学家](../Page/哲学家.md "wikilink")（逝世于[1976年](../Page/1976年.md "wikilink")）
  - [10月3日](../Page/10月3日.md "wikilink")——[卡尔·冯·奥西茨基](../Page/卡尔·冯·奥西茨基.md "wikilink")，德国[作家](../Page/作家.md "wikilink")，[诺贝尔和平奖获得者](../Page/阿尔弗雷德·诺贝尔.md "wikilink")（逝世于[1938年](../Page/1938年.md "wikilink")）
  - [10月26日](../Page/10月26日.md "wikilink")——[李四光](../Page/李四光.md "wikilink")，中国[地质学家](../Page/地质学家.md "wikilink")（逝世于[1971年](../Page/1971年.md "wikilink")）
  - [11月14日](../Page/11月14日.md "wikilink")——[贾瓦哈拉尔·尼赫鲁](../Page/贾瓦哈拉尔·尼赫鲁.md "wikilink")，[印度政治家](../Page/印度.md "wikilink")（逝世于[1964年](../Page/1964年.md "wikilink")）
  - [11月20日](../Page/11月20日.md "wikilink")——[埃德温·哈勃](../Page/愛德文·哈勃.md "wikilink")，美国[天文学家](../Page/天文学家.md "wikilink")（逝世于[1953年](../Page/1953年.md "wikilink")）

## 逝世

  - [1月30日](../Page/1月30日.md "wikilink")——[魯道夫
    (奧匈帝國皇太子)與女友一同殉情自殺](../Page/魯道夫_\(奧匈帝國皇太子\).md "wikilink")。
  - [6月28日](../Page/6月28日.md "wikilink")——[玛丽亚·米切尔](../Page/玛丽亚·米切尔.md "wikilink")，美国[天文学家](../Page/天文学家.md "wikilink")（出生于[1824年](../Page/1824年.md "wikilink")）
  - [10月11日](../Page/10月11日.md "wikilink")——[詹姆斯·焦耳](../Page/詹姆斯·焦耳.md "wikilink")，英国[物理学家](../Page/物理学家.md "wikilink")
  - [10月29日](../Page/10月29日.md "wikilink")——[车尔尼雪夫斯基](../Page/尼古拉·加夫里诺维奇·车尔尼雪夫斯基.md "wikilink")，[俄国革命民主主义者](../Page/俄国.md "wikilink")，[唯物主义](../Page/唯物主义.md "wikilink")[哲学家](../Page/哲学家.md "wikilink")、[文学家](../Page/文学家.md "wikilink")、[作家](../Page/作家.md "wikilink")。（出生于[1828年](../Page/1828年.md "wikilink")）
  - [12月6日](../Page/12月6日.md "wikilink")——[傑佛遜·戴維斯](../Page/傑佛遜·戴維斯.md "wikilink")，[美利堅聯盟國](../Page/美利堅聯盟國.md "wikilink")[總統](../Page/總統.md "wikilink")（出生于[1808年](../Page/1808年.md "wikilink")）

[\*](../Category/1889年.md "wikilink")
[9年](../Category/1880年代.md "wikilink")
[8](../Category/19世纪各年.md "wikilink")