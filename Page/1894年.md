## 大事记

  - 第一次里夫战争余波：摩洛哥苏丹与西班牙签订非斯协定，向西战争赔偿2千万比赛塔，并需对里夫人采取惩罚行动。西班牙可以在梅利利亚继续设防，摩洛哥则需在梅利利亚与摩洛哥之间设立军事缓冲区。
  - [德雷福斯事件](../Page/德雷福斯事件.md "wikilink")。
  - [1月11日](../Page/1月11日.md "wikilink")——[朝鮮國](../Page/朝鮮國.md "wikilink")[全羅道爆發](../Page/全羅道.md "wikilink")[東學黨之亂](../Page/東學黨之亂.md "wikilink")。
  - [3月12日](../Page/3月12日.md "wikilink")——瓶裝[可口可樂開始發售](../Page/可口可樂.md "wikilink")。
  - [6月11日](../Page/6月11日.md "wikilink")──[清军指挥官](../Page/清朝.md "wikilink")[叶志超不战而走](../Page/叶志超.md "wikilink")，致使[日本军队占领](../Page/日本.md "wikilink")[平壤](../Page/平壤.md "wikilink")。
  - [6月23日](../Page/6月23日.md "wikilink")──[国际奥林匹克委员会成立](../Page/国际奥林匹克委员会.md "wikilink")
  - [7月22日](../Page/7月22日.md "wikilink")──第一汽车赛，从[巴黎到](../Page/巴黎.md "wikilink")[法国](../Page/法国.md "wikilink")[鲁昂](../Page/鲁昂.md "wikilink")。
  - [7月25日](../Page/7月25日.md "wikilink")──[豐島海戰爆发](../Page/豐島海戰.md "wikilink")，是[中日甲午战争的開始](../Page/甲午戰爭.md "wikilink")。
  - [7月30日](../Page/7月30日.md "wikilink")——[朝鮮宣佈廢除](../Page/朝鮮王朝.md "wikilink")[清朝](../Page/清朝.md "wikilink")[光緒年號](../Page/光緒.md "wikilink")，改用[開國紀年](../Page/開國_\(朝鮮高宗\).md "wikilink")。
  - [8月1日](../Page/8月1日.md "wikilink")──[甲午戰爭爆發](../Page/甲午戰爭.md "wikilink")。
  - [9月17日](../Page/9月17日.md "wikilink")──[黄海海战爆发](../Page/黄海海战.md "wikilink")，北洋舰队与[日本联合舰队在](../Page/日本.md "wikilink")[鸭绿江口的大东沟海面交战近五个小时](../Page/鸭绿江.md "wikilink")，最终，[日本海军夺取了](../Page/日本.md "wikilink")[黄海与](../Page/黄海.md "wikilink")[渤海的制海权](../Page/渤海.md "wikilink")。
  - [10月1日](../Page/10月1日.md "wikilink")──[马达加斯加成为](../Page/马达加斯加.md "wikilink")[法国殖民地](../Page/法国.md "wikilink")。
  - [11月1日](../Page/11月1日.md "wikilink")──[俄國](../Page/俄罗斯帝国.md "wikilink")[沙皇](../Page/沙皇.md "wikilink")[亞歷山大三世駕崩](../Page/亞歷山大三世_\(俄國\).md "wikilink")，[尼古拉二世繼位](../Page/尼古拉二世_\(俄罗斯\).md "wikilink")。
  - [11月21日](../Page/11月21日.md "wikilink")──甲午战争期间，日军攻占“东亚第一堡垒”旅顺。
  - [11月24日](../Page/11月24日.md "wikilink")──[孫中山在](../Page/孫中山.md "wikilink")[夏威夷](../Page/夏威夷州.md "wikilink")[檀香山建立了](../Page/檀香山.md "wikilink")[中國第一個革命](../Page/中国.md "wikilink")[政黨](../Page/政党.md "wikilink")--[興中會](../Page/兴中会.md "wikilink")（即今日[中國國民黨之前身](../Page/中國國民黨.md "wikilink")）
  - [12月21日](../Page/12月21日.md "wikilink")──[麥肯齊·鮑威爾成為](../Page/麥肯齊·鮑威爾.md "wikilink")[加拿大第五任總理](../Page/加拿大.md "wikilink")。

## 出生

  - [2月3日](../Page/2月3日.md "wikilink")——[諾曼·洛克威爾](../Page/諾曼·洛克威爾.md "wikilink")，[美國在](../Page/美國.md "wikilink")20世紀早期的重要[畫家](../Page/畫家.md "wikilink")
  - [2月10日](../Page/2月10日.md "wikilink")——[麥美倫](../Page/麥美倫.md "wikilink")，前[英國首相](../Page/英國首相.md "wikilink")。（[1986年逝世](../Page/1986年.md "wikilink")）
  - [4月17日](../Page/4月17日.md "wikilink")——[尼基塔·謝爾蓋耶維奇·赫魯雪夫](../Page/赫鲁晓夫.md "wikilink")，[前蘇聯領導人](../Page/苏联.md "wikilink")、政治家（死于[1971年](../Page/1971年.md "wikilink")[9月11日](../Page/9月11日.md "wikilink")）
  - [4月26日](../Page/4月26日.md "wikilink")——[魯道夫·赫斯](../Page/魯道夫·赫斯.md "wikilink")，[希特勒副手](../Page/阿道夫·希特勒.md "wikilink")
  - [4月27日](../Page/4月27日.md "wikilink")——[林漢河](../Page/林漢河.md "wikilink")，[新加坡](../Page/新加坡.md "wikilink")[醫生及政治家](../Page/醫生.md "wikilink")。（[1983年逝世](../Page/1983年.md "wikilink")）
  - [5月11日](../Page/5月11日.md "wikilink")——[瑪莎·葛蘭姆](../Page/瑪莎·葛蘭姆.md "wikilink")，美國編舞家，現代舞蹈史上最早的創始人之一。
  - [7月9日](../Page/7月9日.md "wikilink")——[彼得·卡皮查](../Page/彼得·卡皮查.md "wikilink")，苏联著名物理学家，[超流的发现者之一](../Page/超流.md "wikilink")
  - [7月10日](../Page/7月10日.md "wikilink")——[金亨稷](../Page/金亨稷.md "wikilink")
    金日成的父亲，朝鲜革命家和爱国者
  - [7月17日](../Page/7月17日.md "wikilink")——[乔治·勒梅特](../Page/乔治·勒梅特.md "wikilink")，比利时牧师、宇宙学家
  - [10月22日](../Page/10月22日.md "wikilink")——[梅兰芳](../Page/梅兰芳.md "wikilink")，中國[京剧大師](../Page/京剧.md "wikilink")（死于[1961年](../Page/1961年.md "wikilink")[8月8日](../Page/8月8日.md "wikilink")）
  - [11月16日](../Page/11月16日.md "wikilink")——[理查德·尼古拉斯·冯·康登霍维-凯勒奇](../Page/理查德·尼古拉斯·冯·康登霍维-凯勒奇.md "wikilink")，[奥地利日本政治活动家](../Page/奥地利.md "wikilink")、「[欧洲联盟之父](../Page/歐盟之父.md "wikilink")」（死于[1972年](../Page/1972年.md "wikilink")[7月27日](../Page/7月27日.md "wikilink")）
  - [12月20日](../Page/12月20日.md "wikilink")——[羅伯特·孟席斯](../Page/羅伯特·孟席斯.md "wikilink")，前[澳洲總理](../Page/澳洲總理.md "wikilink")。（[1978年逝世](../Page/1978年.md "wikilink")）

## 逝世

  - [2月4日](../Page/2月4日.md "wikilink")——[阿道夫·薩克斯](../Page/阿道夫·薩克斯.md "wikilink")，[薩克斯風發明者](../Page/薩克斯風.md "wikilink")。
  - [3月2日](../Page/3月2日.md "wikilink")——[具伯·爾利](../Page/具伯·爾利.md "wikilink")，[南北戰爭期間的](../Page/南北戰爭.md "wikilink")[邦聯重要將領之一](../Page/邦聯.md "wikilink")。
  - [9月13日](../Page/9月13日.md "wikilink")——[夏布里耶](../Page/夏布里耶.md "wikilink")，[法國](../Page/法国.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")

[\*](../Category/1894年.md "wikilink")
[4年](../Category/1890年代.md "wikilink")
[9](../Category/19世纪各年.md "wikilink")