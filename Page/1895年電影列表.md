  - [冠詞](../Page/冠詞.md "wikilink")（a、an、das、the、l'、le、la、les、der、die、das、il、gli、el等）開頭的電影名稱將會被忽略。
  - 漢語電影依漢語拼音，**所有非漢語電影**依其在**網際網路電影資料庫**中的英文名稱排列。

__NOTOC__

[0-9](../Page/#0-9.md "wikilink") [A](../Page/#A.md "wikilink")
[B](../Page/#B.md "wikilink") [C](../Page/#C.md "wikilink")
[D](../Page/#D.md "wikilink") [E](../Page/#E.md "wikilink")
[F](../Page/#F.md "wikilink") [G](../Page/#G.md "wikilink")
[H](../Page/#H.md "wikilink") [I](../Page/#I.md "wikilink")
[J](../Page/#J.md "wikilink") [K](../Page/#K.md "wikilink")
[L](../Page/#L.md "wikilink") [M](../Page/#M.md "wikilink")
[N](../Page/#N.md "wikilink") [O](../Page/#O.md "wikilink")
[P](../Page/#P.md "wikilink") [Q](../Page/#Q.md "wikilink")
[R](../Page/#R.md "wikilink") [S](../Page/#S.md "wikilink")
[T](../Page/#T.md "wikilink") [U](../Page/#U.md "wikilink")
[V](../Page/#V.md "wikilink") [W](../Page/#W.md "wikilink")
[X](../Page/#X.md "wikilink") [Y](../Page/#Y.md "wikilink")
[Z](../Page/#Z.md "wikilink")

## 0-9

## A

| 片名                                                                             | 原名                               | 語言                                 | 國家／地區                              | 年份    |
| ------------------------------------------------------------------------------ | -------------------------------- | ---------------------------------- | ---------------------------------- | ----- |
| [Akrobatisches Potpourri](../Page/Akrobatisches_Potpourri.md "wikilink")       | Akrobatisches Potpourri          | [無聲](../Page/無聲電影列表.md "wikilink") | [德國](../Page/德國電影列表.md "wikilink") | 1895年 |
| [Annabelle Serpentine Dance](../Page/Annabelle_Serpentine_Dance.md "wikilink") | Annabelle Serpentine Dance       | [無聲](../Page/無聲電影列表.md "wikilink") | [美國](../Page/美國電影列表.md "wikilink") | 1895年 |
| [Apotheose](../Page/Apotheose.md "wikilink")                                   | Apotheose                        | [無聲](../Page/無聲電影列表.md "wikilink") | [德國](../Page/德國電影列表.md "wikilink") | 1895年 |
| [Archiv Skladanowsky](../Page/Archiv_Skladanowsky.md "wikilink")               | Archiv Skladanowsky              | [無聲](../Page/無聲電影列表.md "wikilink") | [德國](../Page/德國電影列表.md "wikilink") | 1895年 |
| [The Arrest of a Pickpocket](../Page/The_Arrest_of_a_Pickpocket.md "wikilink") | The Arrest of a Pickpocket       | [無聲](../Page/無聲電影列表.md "wikilink") | [英國](../Page/英國電影列表.md "wikilink") | 1895年 |
| [火車進站](../Page/火車進站.md "wikilink")                                             | L'Arrivée d'un train à La Ciotat | [無聲](../Page/無聲電影列表.md "wikilink") | [法國](../Page/法國電影列表.md "wikilink") | 1895年 |
| [水澆園丁](../Page/水澆園丁.md "wikilink")                                             | L'Arroseur arrosé                | [無聲](../Page/無聲電影列表.md "wikilink") | [法國](../Page/法國電影列表.md "wikilink") | 1895年 |
| [Assiettes tournantes](../Page/Assiettes_tournantes.md "wikilink")             | Assiettes tournantes             | [無聲](../Page/無聲電影列表.md "wikilink") | [法國](../Page/法國電影列表.md "wikilink") | 1895年 |
| [Ateliers de La Ciotat](../Page/Ateliers_de_La_Ciotat.md "wikilink")           | Ateliers de La Ciotat            | [無聲](../Page/無聲電影列表.md "wikilink") | [法國](../Page/法國電影列表.md "wikilink") | 1895年 |
| [Autour d'une cabine](../Page/Autour_d'une_cabine.md "wikilink")               | Autour d'une cabine              | [無聲](../Page/無聲電影列表.md "wikilink") | [法國](../Page/法國電影列表.md "wikilink") | 1895年 |

## B

| 片名                                                                                   | 原名                            | 語言                                 | 國家／地區                              | 年份    |
| ------------------------------------------------------------------------------------ | ----------------------------- | ---------------------------------- | ---------------------------------- | ----- |
| [Baignade en mer](../Page/Baignade_en_mer.md "wikilink")                             | Baignade en mer               | [無聲](../Page/無聲電影列表.md "wikilink") | [法國](../Page/法國電影列表.md "wikilink") | 1895年 |
| [出港的船](../Page/出港的船.md "wikilink")                                                   | Barque sortant du port        | [無聲](../Page/無聲電影列表.md "wikilink") | [法國](../Page/法國電影列表.md "wikilink") | 1895年 |
| [Bauerntanz zweier Kinder](../Page/Bauerntanz_zweier_Kinder.md "wikilink")           | Bauerntanz zweier Kinder      | [無聲](../Page/無聲電影列表.md "wikilink") | [德國](../Page/德國電影列表.md "wikilink") | 1895年 |
| [Billy Edwards and the Unknown](../Page/Billy_Edwards_and_the_Unknown.md "wikilink") | Billy Edwards and the Unknown | [無聲](../Page/無聲電影列表.md "wikilink") | [美國](../Page/美國電影列表.md "wikilink") | 1895年 |
| [Bocal aux poissons-rouges](../Page/Bocal_aux_poissons-rouges.md "wikilink")         | Bocal aux poissons-rouges     | [無聲](../Page/無聲電影列表.md "wikilink") | [法國](../Page/法國電影列表.md "wikilink") | 1895年 |
| [Das Boxende Känguruh](../Page/Das_Boxende_Känguruh.md "wikilink")                   | Das Boxende Känguruh          | [無聲](../Page/無聲電影列表.md "wikilink") | [德國](../Page/德國電影列表.md "wikilink") | 1895年 |

## C

| 片名                                                                             | 原名                         | 語言                                 | 國家／地區                              | 年份    |
| ------------------------------------------------------------------------------ | -------------------------- | ---------------------------------- | ---------------------------------- | ----- |
| [Chapeaux à transformations](../Page/Chapeaux_à_transformations.md "wikilink") | Chapeaux à transformations | [無聲](../Page/無聲電影列表.md "wikilink") | [法國](../Page/法國電影列表.md "wikilink") | 1895年 |
| [机器肉店](../Page/机器肉店.md "wikilink")                                             | La Charcuterie mécanique   | [無聲](../Page/無聲電影列表.md "wikilink") | [法國](../Page/法國電影列表.md "wikilink") | 1895年 |
| [Charge of the Uhlans 1895](../Page/Charge_of_the_Uhlans_1895.md "wikilink")   | Charge of the Uhlans 1895  | [無聲](../Page/無聲電影列表.md "wikilink") | [英國](../Page/英國電影列表.md "wikilink") | 1895年 |
| [Chinese Laundry Scene](../Page/Chinese_Laundry_Scene.md "wikilink")           | Chinese Laundry Scene      | [無聲](../Page/無聲電影列表.md "wikilink") | [美國](../Page/美國電影列表.md "wikilink") | 1895年 |
| [The Clown Barber](../Page/The_Clown_Barber.md "wikilink")                     | The Clown Barber           | [無聲](../Page/無聲電影列表.md "wikilink") | [英國](../Page/英國電影列表.md "wikilink") | 1895年 |
| [Course en sac](../Page/Course_en_sac.md "wikilink")                           | Course en sac              | [無聲](../Page/無聲電影列表.md "wikilink") | [法國](../Page/法國電影列表.md "wikilink") | 1895年 |

## D

| 片名                                                                                                                                     | 原名                                                     | 語言                                 | 國家／地區                              | 年份    |
| -------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------ | ---------------------------------- | ---------------------------------- | ----- |
| [Dance of Rejoicing](../Page/Dance_of_Rejoicing.md "wikilink")                                                                         | Dance of Rejoicing                                     | [無聲](../Page/無聲電影列表.md "wikilink") | [美國](../Page/美國電影列表.md "wikilink") | 1895年 |
| [The Derby](../Page/The_Derby.md "wikilink")                                                                                           | The Derby                                              | [無聲](../Page/無聲電影列表.md "wikilink") | [英國](../Page/英國電影列表.md "wikilink") | 1895年 |
| [Discussion de Monsieur Janssen et de Monsieur Lagrange](../Page/Discussion_de_Monsieur_Janssen_et_de_Monsieur_Lagrange.md "wikilink") | Discussion de Monsieur Janssen et de Monsieur Lagrange | [無聲](../Page/無聲電影列表.md "wikilink") | [法國](../Page/法國電影列表.md "wikilink") | 1895年 |
| [Le Déjeuner du chat](../Page/Le_Déjeuner_du_chat.md "wikilink")                                                                       | Le Déjeuner du chat                                    | [無聲](../Page/無聲電影列表.md "wikilink") | [法國](../Page/法國電影列表.md "wikilink") | 1895年 |
| [Départ en voiture](../Page/Départ_en_voiture.md "wikilink")                                                                           | Départ en voiture                                      | [無聲](../Page/無聲電影列表.md "wikilink") | [法國](../Page/法國電影列表.md "wikilink") | 1895年 |

## E

| 片名                                                                                 | 原名                           | 語言                                 | 國家／地區                              | 年份    |
| ---------------------------------------------------------------------------------- | ---------------------------- | ---------------------------------- | ---------------------------------- | ----- |
| [Elsie Jones](../Page/Elsie_Jones.md "wikilink")                                   | Elsie Jones                  | [無聲](../Page/無聲電影列表.md "wikilink") | [美國](../Page/美國電影列表.md "wikilink") | 1895年 |
| [Elsie Jones, No. 2](../Page/Elsie_Jones,_No._2.md "wikilink")                     | Elsie Jones, No. 2           | [無聲](../Page/無聲電影列表.md "wikilink") | [美國](../Page/美國電影列表.md "wikilink") | 1895年 |
| [Enfants aux jouets](../Page/Enfants_aux_jouets.md "wikilink")                     | Enfants aux jouets           | [無聲](../Page/無聲電影列表.md "wikilink") | [美國](../Page/美國電影列表.md "wikilink") | 1895年 |
| [The Execution of Mary Stuart](../Page/The_Execution_of_Mary_Stuart.md "wikilink") | The Execution of Mary Stuart | [無聲](../Page/無聲電影列表.md "wikilink") | [美國](../Page/美國電影列表.md "wikilink") | 1895年 |

## F

<table>
<thead>
<tr class="header">
<th><p>片名</p></th>
<th><p>原名</p></th>
<th><p>語言</p></th>
<th><p>國家／地區</p></th>
<th><p>年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/铁匠.md" title="wikilink">铁匠</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/法國電影列表.md" title="wikilink">法國</a></p></td>
<td><p>1895年</p></td>
</tr>
</tbody>
</table>

## G

<table>
<thead>
<tr class="header">
<th><p>片名</p></th>
<th><p>原名</p></th>
<th><p>語言</p></th>
<th><p>國家／地區</p></th>
<th><p>年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/The_Gay_Brothers.md" title="wikilink">The Gay Brothers</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/美國電影列表.md" title="wikilink">美國</a></p></td>
<td><p>1895年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/The_German_Emperor_Reviewing_His_Troops.md" title="wikilink">The German Emperor Reviewing His Troops</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/德國電影列表.md" title="wikilink">德國</a></p></td>
<td><p>1895年</p></td>
</tr>
</tbody>
</table>

## H

## I

## J

<table>
<thead>
<tr class="header">
<th><p>片名</p></th>
<th><p>原名</p></th>
<th><p>語言</p></th>
<th><p>國家／地區</p></th>
<th><p>年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/The_Gay_Brothers.md" title="wikilink">The Gay Brothers</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/美國電影列表.md" title="wikilink">美國</a></p></td>
<td><p>1895年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/聖女貞德_(電影).md" title="wikilink">聖女貞德</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/美國電影列表.md" title="wikilink">美國</a></p></td>
<td><p>1895年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/John_W._Wilson_and_Bertha_Waring.md" title="wikilink">John W. Wilson and Bertha Waring</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/美國電影列表.md" title="wikilink">美國</a></p></td>
<td><p>1895年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Der_Jongleur.md" title="wikilink">Der Jongleur</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/德國電影列表.md" title="wikilink">德國</a></p></td>
<td><p>1895年</p></td>
</tr>
</tbody>
</table>

## K

<table>
<thead>
<tr class="header">
<th><p>片名</p></th>
<th><p>原名</p></th>
<th><p>語言</p></th>
<th><p>國家／地區</p></th>
<th><p>年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/Kamarinskaja.md" title="wikilink">Kamarinskaja</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/德國電影列表.md" title="wikilink">德國</a></p></td>
<td><p>1895年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Komisches_Reck.md" title="wikilink">Komisches Reck</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/德國電影列表.md" title="wikilink">德國</a></p></td>
<td><p>1895年</p></td>
</tr>
</tbody>
</table>

## L

<table>
<thead>
<tr class="header">
<th><p>片名</p></th>
<th><p>原名</p></th>
<th><p>語言</p></th>
<th><p>國家／地區</p></th>
<th><p>年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/Lancement_d&#39;un_navire_à_la_Ciotat.md" title="wikilink">Lancement d'un navire à la Ciotat</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/法國電影列表.md" title="wikilink">法國</a></p></td>
<td><p>1895年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Lion,_London_Zoological_Gardens.md" title="wikilink">Lion, London Zoological Gardens</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/法國電影列表.md" title="wikilink">法國</a></p></td>
<td><p>1895年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Lutte.md" title="wikilink">Lutte</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/德國電影列表.md" title="wikilink">德國</a></p></td>
<td><p>1895年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Lyon,_place_Bellecour.md" title="wikilink">Lyon, place Bellecour</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/法國電影列表.md" title="wikilink">法國</a></p></td>
<td><p>1895年</p></td>
</tr>
</tbody>
</table>

## M

<table>
<thead>
<tr class="header">
<th><p>片名</p></th>
<th><p>原名</p></th>
<th><p>語言</p></th>
<th><p>國家／地區</p></th>
<th><p>年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/M._Lumière_Receiving_Guests.md" title="wikilink">M. Lumière Receiving Guests</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/法國電影列表.md" title="wikilink">法國</a></p></td>
<td><p>1895年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Le_Maréchal-ferrant.md" title="wikilink">Le Maréchal-ferrant</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/法國電影列表.md" title="wikilink">法國</a></p></td>
<td><p>1895年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/La_Mer.md" title="wikilink">La Mer</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/法國電影列表.md" title="wikilink">法國</a></p></td>
<td><p>1895年</p></td>
</tr>
</tbody>
</table>

## N

<table>
<thead>
<tr class="header">
<th><p>片名</p></th>
<th><p>原名</p></th>
<th><p>語言</p></th>
<th><p>國家／地區</p></th>
<th><p>年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/代表们的登陆.md" title="wikilink">代表们的登陆</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/法國電影列表.md" title="wikilink">法國</a></p></td>
<td><p>1895年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/New_Bar_Room.md" title="wikilink">New Bar Room</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/美國電影列表.md" title="wikilink">美國</a></p></td>
<td><p>1895年</p></td>
</tr>
</tbody>
</table>

## O

<table>
<thead>
<tr class="header">
<th><p>片名</p></th>
<th><p>原名</p></th>
<th><p>語言</p></th>
<th><p>國家／地區</p></th>
<th><p>年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/Opening_of_the_Kiel_Canal.md" title="wikilink">Opening of the Kiel Canal</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/英國電影列表.md" title="wikilink">英國</a></p></td>
<td><p>1895年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/The_Oxford_and_Cambridge_University_Boat_Race.md" title="wikilink">The Oxford and Cambridge University Boat Race</a></p></td>
<td></td>
<td><p><a href="../Page/無聲電影列表.md" title="wikilink">無聲</a></p></td>
<td><p><a href="../Page/英國電影列表.md" title="wikilink">英國</a></p></td>
<td><p>1895年</p></td>
</tr>
</tbody>
</table>

## P

## Q

## R

## S

## T

## U

## V

## W

## X

## Y

## Z

## 外部連結

  - [網際網路電影資料庫-－1895年電影](http://us.imdb.com/List?year=1895&&tv=on&&nav=/Sections/Years/1895/include-titles&&heading=7;All%20titles;1895)
  - [互聯影庫-－1895年電影](https://archive.is/20130427070728/http://allmov.mtime.com/query/year/1895.html)

[Category:各年电影列表](../Category/各年电影列表.md "wikilink")
[Category:電影年表](../Category/電影年表.md "wikilink")