[1701年](../Page/1701年.md "wikilink")[1月1日至](../Page/1月1日.md "wikilink")[1800年](../Page/1800年.md "wikilink")[12月31日的这一段期间被称为](../Page/12月31日.md "wikilink")**18世纪**。這個世紀注重的是“穩定”與“和諧”，卻也是人們對自然探索的萌芽期。民主思潮逐渐燃起，美国独立战争和法国大革命影响深远。

政治上，歐洲各國開始與[中國](../Page/中國.md "wikilink")、[印度和](../Page/印度.md "wikilink")[土耳其進行小規模的通商貿易](../Page/土耳其.md "wikilink")，並持續在[東南亞與](../Page/東南亞.md "wikilink")[大洋州建立殖民據點](../Page/大洋州.md "wikilink")。此時多數的[君主制](../Page/君主制.md "wikilink")[國家](../Page/國家.md "wikilink")（如[大清帝国](../Page/大清帝国.md "wikilink")、[蒙兀兒帝國](../Page/蒙兀兒帝國.md "wikilink")、[法蘭西帝國](../Page/法蘭西帝國.md "wikilink")、[奥斯曼帝国](../Page/奥斯曼帝国.md "wikilink")、[奧地利帝國](../Page/奧地利帝國.md "wikilink")、[俄羅斯帝國](../Page/俄罗斯帝国.md "wikilink")）正處於全盛時期，但民主思潮卻逐漸燃起，並以[美國獨立戰爭和](../Page/美國獨立戰爭.md "wikilink")[法國大革命影響最深](../Page/法國大革命.md "wikilink")。

學術上，在西歐興起的[啟蒙運動開始挑戰](../Page/啟蒙時代.md "wikilink")[基督教](../Page/基督教.md "wikilink")[教會的思想體系](../Page/教會.md "wikilink")，使[科學的成果感染到社會的各個層面](../Page/科學.md "wikilink")，而歐洲以外的地區也透過[傳教與](../Page/傳教.md "wikilink")[貿易的方式接觸這思潮](../Page/貿易.md "wikilink")，進而產生小規模的學術復興運動。

另外，由於商業上的需要，部分技術孕育而生，成為[工業革命之濫觴](../Page/工業革命.md "wikilink")。而在技術外，生產與管理方式在西歐逐漸發生改變：傳統世襲的學徒制逐漸被破壞，分工與工廠生產方式開始抬頭。

藝術與文化上，追尋[希臘與](../Page/希腊.md "wikilink")[古羅馬風格的](../Page/古羅馬.md "wikilink")[新古典主義盛行西方世界](../Page/新古典主義.md "wikilink")，並影響印度與中國的宮廷藝術。但同樣的，中國和[大洋洲的文化物品流入歐洲](../Page/大洋洲.md "wikilink")，使西方世界的[上流社會吹起十分表面的異國風](../Page/上流社會.md "wikilink")。

## 重要事件、发展与成就

### 战争与政治

  - [西班牙王位繼承戰爭](../Page/西班牙王位繼承戰爭.md "wikilink")：[法国](../Page/法国.md "wikilink")[波旁王朝的](../Page/波旁王朝.md "wikilink")[腓力五世继承](../Page/腓力五世_\(西班牙\).md "wikilink")[西班牙王位](../Page/西班牙.md "wikilink")，但是被[英国夺走很多海外](../Page/英国.md "wikilink")[殖民地](../Page/殖民地.md "wikilink")。
  - [大北方战争](../Page/大北方战争.md "wikilink")：[俄国战胜](../Page/俄国.md "wikilink")[瑞典](../Page/瑞典.md "wikilink")，获得[波罗的海出海口](../Page/波罗的海.md "wikilink")。
  - [波兰王位继承战争](../Page/波兰王位继承战争.md "wikilink")。
  - [奥地利王位继承战争](../Page/奥地利王位继承战争.md "wikilink")。
  - [七年战争](../Page/七年战争.md "wikilink")：英国夺取法国[北美洲殖民地](../Page/北美洲.md "wikilink")[加拿大和](../Page/加拿大.md "wikilink")[印度殖民地](../Page/印度.md "wikilink")。
  - [美國獨立戰爭](../Page/美國獨立戰爭.md "wikilink")：英国北美殖民地独立成為[美國](../Page/美國.md "wikilink")。
  - [法國大革命](../Page/法國大革命.md "wikilink")：[拿破仑帶領法国重新崛起](../Page/拿破仑.md "wikilink")。
  - [瓜分波兰](../Page/瓜分波兰.md "wikilink")：[波兰的土地被俄罗斯](../Page/波兰.md "wikilink")、[奥地利](../Page/奥地利.md "wikilink")、[普鲁士瓜分](../Page/普鲁士.md "wikilink")。
  - [清朝平定](../Page/清朝.md "wikilink")[准噶尔叛乱](../Page/准噶尔部.md "wikilink")，控制[新疆](../Page/新疆.md "wikilink")。

### 社會與經濟

  - 英国推行[贸易保护主义政策](../Page/贸易保护主义.md "wikilink")，[工業革命開始](../Page/工業革命.md "wikilink")。
  - [清朝采取限制出海](../Page/清朝.md "wikilink")，限制开矿的[闭关政策](../Page/闭关政策.md "wikilink")。中国工商业严重倒退。
  - [清朝康熙末年](../Page/清朝.md "wikilink")，推行[摊丁入亩政策](../Page/摊丁入亩.md "wikilink")，废除[人头税](../Page/人头税.md "wikilink")，导致中国人口迅速增加。

### 科学技术

  - [蒸汽機技術被改良及廣泛應用](../Page/蒸汽机.md "wikilink")。

### 宗教與哲學

  - [启蒙运动](../Page/启蒙运动.md "wikilink")。
  - 清[雍正](../Page/雍正.md "wikilink")、[乾隆朝大兴](../Page/乾隆.md "wikilink")[文字狱](../Page/文字狱.md "wikilink")。

### 文化娱乐

18世紀在西洋音樂史經歷了[巴洛克時期與](../Page/巴洛克時期.md "wikilink")[古典主義時期](../Page/古典主义音乐.md "wikilink")，其中巴洛克的代表為[巴赫](../Page/巴赫.md "wikilink")，古典主義的代表為[莫扎特](../Page/莫扎特.md "wikilink")，[貝多芬等](../Page/貝多芬.md "wikilink")

### 疾病与医學

  - 1735年，[卡尔·林奈发表了对自然世界的基本分类方法](../Page/卡尔·林奈.md "wikilink")，而在1750年代为他发现的所有物种都使用了[双名法](../Page/双名法.md "wikilink")

### 环境与自然资源

## 重要人物

### 世界領導人

#### [非洲](../Page/非洲.md "wikilink")

#### [美洲](../Page/美洲.md "wikilink")

##### [美國](../Page/美國.md "wikilink")([總統](../Page/美國總統.md "wikilink"))

  - [华盛顿](../Page/乔治·华盛顿.md "wikilink")
  - [亞當斯](../Page/约翰·亚当斯.md "wikilink")

#### [亚洲](../Page/亚洲.md "wikilink")

[Emperor_Kangxi.PNG](https://zh.wikipedia.org/wiki/File:Emperor_Kangxi.PNG "fig:Emperor_Kangxi.PNG")[康熙皇帝](../Page/康熙皇帝.md "wikilink")\]\]

##### 中國([清朝](../Page/大清帝國.md "wikilink"))

  - [清聖祖](../Page/清聖祖.md "wikilink")[愛新覺羅玄燁](../Page/愛新覺羅玄燁.md "wikilink")，[1662年](../Page/1662年.md "wikilink")-[1722年](../Page/1722年.md "wikilink")
  - [清世宗](../Page/清世宗.md "wikilink")[愛新覺羅胤禛](../Page/愛新覺羅胤禛.md "wikilink")，[1723年](../Page/1723年.md "wikilink")-[1735年](../Page/1735年.md "wikilink")
  - [清高宗](../Page/清高宗.md "wikilink")[愛新覺羅弘曆](../Page/愛新覺羅弘曆.md "wikilink")，[1736年](../Page/1736年.md "wikilink")-[1795年](../Page/1795年.md "wikilink")
  - [清仁宗](../Page/清仁宗.md "wikilink")[愛新覺羅顒琰](../Page/愛新覺羅顒琰.md "wikilink")，[1796年](../Page/1796年.md "wikilink")-[1820年](../Page/1820年.md "wikilink")

##### [日本](../Page/日本.md "wikilink")([天皇](../Page/天皇.md "wikilink"))

  - [東山天皇](../Page/東山天皇.md "wikilink")
  - [中御門天皇](../Page/中御門天皇.md "wikilink")
  - [櫻町天皇](../Page/櫻町天皇.md "wikilink")
  - [桃園天皇](../Page/桃園天皇.md "wikilink")
  - [後櫻町天皇](../Page/後櫻町天皇.md "wikilink")(至今最後一位女天皇)

刘泽沛

  - [德川家宣](../Page/德川家宣.md "wikilink")
  - [德川家繼](../Page/德川家繼.md "wikilink")
  - [德川吉宗](../Page/德川吉宗.md "wikilink")
  - [德川家重](../Page/德川家重.md "wikilink")
  - [德川家治](../Page/德川家治.md "wikilink")
  - [德川家齊](../Page/德川家齊.md "wikilink")

##### [朝鮮](../Page/李氏朝鮮.md "wikilink")

  - [朝鮮肅宗](../Page/朝鮮肅宗.md "wikilink")
  - [朝鮮景宗](../Page/朝鮮景宗.md "wikilink")
  - [朝鮮英祖](../Page/朝鮮英祖.md "wikilink")
  - [朝鮮正祖](../Page/朝鮮正祖.md "wikilink")

##### [越南](../Page/越南.md "wikilink")

###### [廣南國](../Page/廣南國.md "wikilink")

  - [阮福淍](../Page/阮福淍.md "wikilink")
  - [阮福澍](../Page/阮福澍.md "wikilink")
  - [阮福濶](../Page/阮福濶.md "wikilink")
  - [阮福淳](../Page/阮福淳.md "wikilink")
  - [阮福暘](../Page/阮福暘.md "wikilink")

###### [鄭主](../Page/鄭主.md "wikilink")

  - [鄭根](../Page/鄭根.md "wikilink")
  - [鄭棡](../Page/鄭棡.md "wikilink")
  - [鄭杠](../Page/鄭杠.md "wikilink")
  - [鄭楹](../Page/鄭楹.md "wikilink")
  - [鄭森](../Page/鄭森.md "wikilink")
  - [鄭檊](../Page/鄭檊.md "wikilink")
  - [鄭棕](../Page/鄭棕.md "wikilink")
  - [鄭槰](../Page/鄭槰.md "wikilink")

###### [後黎朝](../Page/後黎朝.md "wikilink")

  - [黎維祫](../Page/黎維祫.md "wikilink")
  - [黎維禟](../Page/黎維禟.md "wikilink")
  - [黎維祊](../Page/黎維祊.md "wikilink")
  - [黎維祥](../Page/黎維祥.md "wikilink")
  - [黎維祳](../Page/黎維祳.md "wikilink")
  - [黎維祧](../Page/黎維祧.md "wikilink")
  - [黎維祁](../Page/黎維祁.md "wikilink")

###### [西山朝](../Page/西山朝.md "wikilink")

  - [阮文岳](../Page/阮文岳.md "wikilink")
  - [阮文寶](../Page/阮文寶.md "wikilink")
  - [阮光平](../Page/阮光平.md "wikilink")
  - [阮光纘](../Page/阮光纘.md "wikilink")

#### [欧洲](../Page/欧洲.md "wikilink")

##### [法國](../Page/法国.md "wikilink")

  - [路易十四](../Page/路易十四.md "wikilink")
  - [路易十五](../Page/路易十五.md "wikilink")
  - [路易十六](../Page/路易十六.md "wikilink")
  - [羅伯斯庇爾](../Page/羅伯斯庇爾.md "wikilink")
  - [拿破崙](../Page/拿破仑·波拿巴.md "wikilink")

##### [英國](../Page/英国.md "wikilink")

  - [喬治一世](../Page/喬治一世_\(英國\).md "wikilink")
  - [喬治二世](../Page/喬治二世_\(英國\).md "wikilink")
  - [喬治三世](../Page/喬治三世_\(英國\).md "wikilink")

##### [俄国](../Page/俄国.md "wikilink")

  - [彼得一世](../Page/彼得大帝.md "wikilink")
  - [凱薩琳一世](../Page/叶卡捷琳娜一世.md "wikilink")
  - [伊麗莎白·彼得羅芙娜](../Page/伊麗莎白·彼得羅芙娜.md "wikilink")
  - [彼得三世](../Page/彼得三世_\(俄国\).md "wikilink")
  - [凱薩琳二世](../Page/叶卡捷琳娜大帝.md "wikilink")

##### [普魯士](../Page/普魯士.md "wikilink")

  - [腓特烈二世](../Page/腓特烈二世_\(普魯士\).md "wikilink")
  - [腓特烈·威廉二世](../Page/腓特烈·威廉二世.md "wikilink")

##### [奧地利](../Page/奧地利.md "wikilink")

  - [瑪麗亞·特蕾莎](../Page/瑪麗亞·特蕾莎.md "wikilink")
  - [約瑟夫二世](../Page/約瑟夫二世_\(神圣罗马帝国\).md "wikilink")

#### [中东](../Page/中东地区.md "wikilink")

##### [鄂圖曼土耳其](../Page/鄂圖曼土耳其.md "wikilink")

  - [奥斯曼三世](../Page/奥斯曼三世.md "wikilink")
  - [穆斯塔法三世](../Page/穆斯塔法三世.md "wikilink")
  - [阿卜杜勒·哈米德一世](../Page/阿卜杜勒·哈米德一世.md "wikilink")

<div style="height: 250px; overflow: auto; padding: 3px">

### 科學家

  - [瓦特](../Page/詹姆斯·瓦特.md "wikilink")，蘇格蘭著名的發明家和機械工程師。
  - [瑪麗亞·加埃塔納·阿涅西](../Page/瑪麗亞·加埃塔納·阿涅西.md "wikilink") ，義大利數學家，著有《分析講義》。
  - [達朗貝爾](../Page/讓·勒朗·達朗貝爾.md "wikilink")，法國物理學家。
  - [約瑟夫·班克斯](../Page/約瑟夫·班克斯.md "wikilink")，英國探險家和博物學家，曾長期擔任皇家學會會長。
  - [蘿拉·巴斯](../Page/蘿拉·巴斯.md "wikilink")，義大利科學家，為歐洲第一位女教授。
  - [白努利](../Page/丹尼尔·伯努利.md "wikilink")，著名數學家，流體力學中的[白努利定律即由他所提出](../Page/白努利定律.md "wikilink")。
  - [魯傑羅·朱塞佩·博斯科維奇](../Page/魯傑羅·朱塞佩·博斯科維奇.md "wikilink")，物理、天文學家。
  - [布豐](../Page/布豐.md "wikilink")，法國博物學家、數學家、生物學家、啟蒙時代著名作家。布豐的思想影響了之後兩代的博物學家，包括[達爾文和](../Page/達爾文.md "wikilink")[拉馬克](../Page/拉馬克.md "wikilink")。。
  - [安德斯·攝爾修斯](../Page/安德斯·攝爾修斯.md "wikilink")，瑞典天文學家。
  - [亞歷克西斯·克勞德·克萊羅](../Page/亞歷克西斯·克勞德·克萊羅.md "wikilink")，法國數學家、天文學家。
  - [詹姆斯·庫克](../Page/詹姆斯·庫克.md "wikilink")，英國皇家海軍軍官、航海家、探險家。
  - [尤拉](../Page/李昂哈德·尤拉.md "wikilink")，瑞士數學家和物理學家，近代數學先驅之一。
  - [丹尼爾·加布里埃爾·華倫海特](../Page/丹尼爾·加布里埃爾·華倫海特.md "wikilink")，德國物理學家，[華氏溫標的創立者](../Page/華氏溫標.md "wikilink")。
  - [喬治•福代斯](../Page/喬治•福代斯.md "wikilink")，英國物理學家。
  - [高斯](../Page/卡爾·弗里德里希·高斯.md "wikilink")，德國著名數學家、物理學家、天文學家。
  - [愛德華·詹納](../Page/愛德華·詹納.md "wikilink")，英國醫生，以研究及推廣[牛痘疫苗](../Page/牛痘.md "wikilink")，防止[天花而聞名](../Page/天花.md "wikilink")，被稱為[免疫學之父](../Page/免疫學.md "wikilink")。
  - [約瑟夫·拉格朗日](../Page/約瑟夫·拉格朗日.md "wikilink")，法國籍義大利裔數學家和天文學家。
  - [拉馬克](../Page/讓-巴蒂斯特·拉馬克.md "wikilink")，法國生物學家。
  - [拉普拉斯](../Page/皮耶爾-西蒙·拉普拉斯.md "wikilink")，法國著名的天文學家和數學家。
  - [拉瓦節](../Page/安東萬-羅倫·德·拉瓦節.md "wikilink")，法國化學家、生物學家，後世尊稱拉瓦節為近代化學之父。
  - [勒讓德](../Page/阿德里安-馬里·勒讓德.md "wikilink")，法國數學家。
  - [林奈](../Page/卡爾·林奈.md "wikilink")，瑞典植物學家、動物學家和醫生，他奠定了現代生物學命名法[二名法的基礎](../Page/二名法.md "wikilink")，是現代[生物分類學之父](../Page/生物分類學.md "wikilink")，也被認為是現代[生態學之父之](../Page/生態學.md "wikilink")。
  - [米哈伊爾·瓦西里耶維奇·羅蒙諾索夫](../Page/米哈伊爾·瓦西里耶維奇·羅蒙諾索夫.md "wikilink")，俄國化學家、哲學家。
  - [皮埃爾·莫佩爾蒂](../Page/皮埃爾·莫佩爾蒂.md "wikilink")，法國數學家、物理學家、哲學家。
  - [彼得·西蒙·帕拉斯](../Page/彼得·西蒙·帕拉斯.md "wikilink")，德國博物學家。
  - [約瑟夫·普利斯特里](../Page/約瑟夫·普利斯特里.md "wikilink")，英國化學家。
  - [瑞尼·瑞歐莫](../Page/瑞尼·瑞歐莫.md "wikilink")，法國科學家。
  - [安東尼奧·烏略亞](../Page/安東尼奧·烏略亞.md "wikilink")，西班牙將軍、探險家、作家、天文學家，因發現鉑元素而聞名。
  - [約翰·懷賀斯](../Page/約翰·懷賀斯.md "wikilink")，英國著名製鐘專家、科學家、工程學家、地理學家，為[月光社重要成員](../Page/月光社.md "wikilink")。
  - [戴震](../Page/戴震.md "wikilink")，清代著名學者。
  - [卡爾·威廉·舍勒](../Page/卡爾·威廉·舍勒.md "wikilink")，瑞典屬波美拉尼亞藥劑師及化學家。
  - [卡文迪西](../Page/亨利·卡文迪什.md "wikilink")，英國物理學家、化學家。
  - [約瑟夫·布拉克](../Page/約瑟夫·布拉克.md "wikilink")，英國籍的醫生和化學家。

### 哲學家

  - [伏爾泰](../Page/伏爾泰.md "wikilink")
  - [孟德斯鸠](../Page/孟德斯鸠.md "wikilink")
  - [德尼·狄德罗](../Page/德尼·狄德罗.md "wikilink")
  - [盧梭](../Page/讓-雅克·盧梭.md "wikilink")
  - [杰里米·边沁](../Page/杰里米·边沁.md "wikilink")

### 軍事領袖

  - [拿破仑](../Page/拿破仑.md "wikilink") 法国
  - [年羹尧](../Page/年羹尧.md "wikilink") 清

### 藝術家

  - [巴赫](../Page/約翰·塞巴斯蒂安·巴赫.md "wikilink")
  - [韓德爾](../Page/韓德爾.md "wikilink")
  - [海頓](../Page/法蘭茲·約瑟夫·海頓.md "wikilink")
  - [莫札特](../Page/沃尔夫冈·阿马多伊斯·莫扎特.md "wikilink")
  - [貝多芬](../Page/路德维希·凡·贝多芬.md "wikilink")
  - [鄭燮](../Page/郑燮.md "wikilink")

</div>

[18世纪](../Category/18世纪.md "wikilink")
[Category:2千纪](../Category/2千纪.md "wikilink")
[+18](../Category/世纪.md "wikilink")