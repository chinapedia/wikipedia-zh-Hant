## 大事记

  - [英國與](../Page/英國.md "wikilink")[美國教會於](../Page/美國.md "wikilink")[中国](../Page/中国.md "wikilink")[北京合作興辦協和醫學堂](../Page/北京.md "wikilink")，是[協和醫院的前身](../Page/北京協和醫院.md "wikilink")。
  - [2月13日](../Page/2月13日.md "wikilink")——[慈幼會來華](../Page/慈幼會.md "wikilink")。
  - [3月15日](../Page/3月15日.md "wikilink")——勞斯萊斯公司成立。
  - [3月17日](../Page/3月17日.md "wikilink")——[台灣](../Page/台灣.md "wikilink")[嘉義的](../Page/嘉義縣.md "wikilink")[梅山地區發生了規模](../Page/梅山鄉_\(臺灣\).md "wikilink")7.1的[大地震](../Page/1906年梅山大地震.md "wikilink")，震央在三美莊至開元后之間。
  - [2月15日](../Page/2月15日.md "wikilink")——[英国](../Page/英国.md "wikilink")[工党建党](../Page/英国工党.md "wikilink")。
  - [4月7日](../Page/4月7日.md "wikilink")——[维苏威火山爆发](../Page/維蘇威火山.md "wikilink")。
  - [4月18日](../Page/4月18日.md "wikilink")——[旧金山大地震](../Page/旧金山.md "wikilink")。
  - [4月27日](../Page/4月27日.md "wikilink")——中国和英国签署《[中英續訂藏印條約](../Page/中英續訂藏印條約.md "wikilink")》。
  - [5月10日](../Page/5月10日.md "wikilink")——[俄國第一屆國家](../Page/俄国.md "wikilink")[杜馬會議召開](../Page/杜馬.md "wikilink")。
  - [5月22日](../Page/5月22日.md "wikilink")——[朝鮮王朝](../Page/朝鮮王朝.md "wikilink")[高宗的](../Page/朝鮮高宗.md "wikilink")[嚴妃創建的](../Page/純獻皇貴妃.md "wikilink")[明心女校](../Page/明心女校.md "wikilink")（現在的[淑明女子高中](../Page/淑明女子高中.md "wikilink")）開學。
  - [9月1日](../Page/9月1日.md "wikilink")——清廷發佈《宣示預備立憲先行釐定官制諭》，標誌著預備立憲開始。
  - [10月12日](../Page/10月12日.md "wikilink")——农历八月二十五日，[清朝政府裁撤](../Page/清朝政府.md "wikilink")[打牲乌拉总管](../Page/打牲乌拉总管.md "wikilink")，[打牲乌拉总管衙门由此改称打牲乌拉翼领署](../Page/打牲乌拉总管衙门.md "wikilink")。
  - [11月3日](../Page/11月3日.md "wikilink")——於德國柏林召開的國際[無線電通信大會中](../Page/无线电.md "wikilink")，決定以「[SOS](../Page/SOS.md "wikilink")」為國際求救信號（1908年7月1日起正式使用）。
  - [11月16日](../Page/11月16日.md "wikilink")——[暨南大学前身暨南学堂在](../Page/暨南大学.md "wikilink")[南京成立](../Page/南京.md "wikilink")。
  - [12月24日](../Page/12月24日.md "wikilink")——[雷吉纳德·菲森登启用世界上第一台无线电台](../Page/雷吉纳德·菲森登.md "wikilink")。

## 出生

  - [1月11日](../Page/1月11日.md "wikilink")——[艾伯特·霍夫曼](../Page/艾伯特·霍夫曼.md "wikilink")，[瑞士](../Page/瑞士.md "wikilink")[化學家](../Page/化學家.md "wikilink")（[2008年去世](../Page/2008年.md "wikilink")）
  - [1月13日](../Page/1月13日.md "wikilink")——[周有光](../Page/周有光.md "wikilink")，[中國語言學家](../Page/中國.md "wikilink")（[2017年去世](../Page/2017年.md "wikilink")）
  - [1月15日](../Page/1月15日.md "wikilink")——[亚里士多德·奥纳西斯](../Page/亚里士多德·奥纳西斯.md "wikilink")，[希腊船王](../Page/希腊.md "wikilink")（[1975年去世](../Page/1975年.md "wikilink")）
  - [2月7日](../Page/2月7日.md "wikilink")——[爱新觉罗溥仪](../Page/爱新觉罗·溥仪.md "wikilink")，[中国末代皇帝](../Page/中国.md "wikilink")（[1967年去世](../Page/1967年.md "wikilink")）
  - [3月19日](../Page/3月19日.md "wikilink")——[阿道夫·艾希曼](../Page/阿道夫·艾希曼.md "wikilink")，[纳粹](../Page/纳粹主义.md "wikilink")[集中营长官](../Page/集中营.md "wikilink")（[1962年去世](../Page/1962年.md "wikilink")，絞刑）
  - [3月25日](../Page/3月25日.md "wikilink")——[A·J·P·泰勒](../Page/A·J·P·泰勒.md "wikilink")，[英國](../Page/英国.md "wikilink")[歷史學家](../Page/历史学家.md "wikilink")。（[1990年去世](../Page/1990年.md "wikilink")）
  - [4月28日](../Page/4月28日.md "wikilink")——[庫爾特·哥德爾](../Page/库尔特·哥德尔.md "wikilink")，[奥地利](../Page/奥地利.md "wikilink")[数学家](../Page/数学家.md "wikilink")（[1978年去世](../Page/1978年.md "wikilink")）
  - [5月4日](../Page/5月4日.md "wikilink")——[布達勞](../Page/布達勞.md "wikilink")，[香港](../Page/香港.md "wikilink")[土生葡裔](../Page/土生葡人.md "wikilink")[律師](../Page/律師.md "wikilink")、[公務員和](../Page/香港公務員.md "wikilink")[軍人](../Page/軍人.md "wikilink")（[1999年去世](../Page/1999年.md "wikilink")）
  - [5月24日](../Page/5月24日.md "wikilink")——[川島芳子](../Page/川島芳子.md "wikilink")，清朝皇族，日軍侵華間諜。（[1948年去世](../Page/1948年.md "wikilink")）
  - [5月31日](../Page/5月31日.md "wikilink")——[罗瑞卿](../Page/罗瑞卿.md "wikilink")，[中国人民解放军大將](../Page/中国人民解放军.md "wikilink")（[1978年去世](../Page/1978年.md "wikilink")）
  - [6月3日](../Page/6月3日.md "wikilink")——[柏立基](../Page/柏立基.md "wikilink")，第23任[香港總督](../Page/香港總督.md "wikilink")（[1999年去世](../Page/1999年.md "wikilink")）
  - [6月22日](../Page/6月22日.md "wikilink")——[比利·懷爾德](../Page/比利·懷爾德.md "wikilink")，[美国导演](../Page/美國.md "wikilink")（[2002年去世](../Page/2002年.md "wikilink")）
  - [7月21日](../Page/7月21日.md "wikilink")——[鄧雨賢](../Page/鄧雨賢.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[音樂家](../Page/音乐家.md "wikilink")（[1944年去世](../Page/1944年.md "wikilink")）
  - [8月15日](../Page/8月15日.md "wikilink")——[王稼祥](../Page/王稼祥.md "wikilink")，[中華人民共和國駐](../Page/中华人民共和国.md "wikilink")[蘇聯第一任大使](../Page/苏联.md "wikilink")（[1974年去世](../Page/1974年.md "wikilink")）
  - [8月27日](../Page/8月27日.md "wikilink")——[艾德·蓋恩](../Page/艾德·蓋恩.md "wikilink")，美國連環殺手（[1984年去世](../Page/1984年.md "wikilink")）
  - [9月25日](../Page/9月25日.md "wikilink")（即[儒略历](../Page/儒略曆.md "wikilink")[9月12日](../Page/9月12日.md "wikilink")）——[德米特里·肖斯塔科维奇](../Page/季米特里·肖斯塔科维奇.md "wikilink")，[俄罗斯作曲家](../Page/俄罗斯.md "wikilink")（[1975年去世](../Page/1975年.md "wikilink")）
  - [10月6日](../Page/10月6日.md "wikilink")——[徐敬直](../Page/徐敬直.md "wikilink")，中國和香港建築師（[1983年去世](../Page/1983年.md "wikilink")）
  - [10月7日](../Page/10月7日.md "wikilink")——[詹姆斯·韦伯](../Page/詹姆斯·韦伯.md "wikilink")，[美国国家航空航天局第二任局长](../Page/美国国家航空航天局.md "wikilink")（[1992年去世](../Page/1992年.md "wikilink")）
  - [10月10日](../Page/10月10日.md "wikilink")——[朱光](../Page/朱光_\(官员\).md "wikilink")，[广州市](../Page/广州市.md "wikilink")（[中华人民共和国时代](../Page/中华人民共和国.md "wikilink")）第一任[市委副书记](../Page/广州市委.md "wikilink")、市长。（[1969年去世](../Page/1969年.md "wikilink")）
  - [10月15日](../Page/10月15日.md "wikilink")──[譚富英](../Page/譚富英.md "wikilink")，中國[京劇大師](../Page/京剧.md "wikilink")（[1977年去世](../Page/1977年.md "wikilink")）
  - [10月20日](../Page/10月20日.md "wikilink")——[坂口安吾](../Page/坂口安吾.md "wikilink")，[日本](../Page/日本.md "wikilink")[小說家](../Page/小說家_\(職業\).md "wikilink")（[1955年去世](../Page/1955年.md "wikilink")）
  - [10月27日](../Page/10月27日.md "wikilink")──[大野一雄](../Page/大野一雄.md "wikilink")，日本舞踏家（[2010年去世](../Page/2010年.md "wikilink")）
  - [12月13日](../Page/12月13日.md "wikilink")——[根德公爵夫人馬里納郡主](../Page/馬里納郡主_\(根德公爵夫人\).md "wikilink")，[英國皇室成員](../Page/英國.md "wikilink")。（[1968年去世](../Page/1968年.md "wikilink")）
  - [12月19日](../Page/12月19日.md "wikilink")——[勃列日涅夫](../Page/列昂尼德·伊里奇·勃列日涅夫.md "wikilink")，[苏联第四代领导人](../Page/苏联.md "wikilink")（[1982年去世](../Page/1982年.md "wikilink")）
  - [12月23日](../Page/12月23日.md "wikilink")——[乌兰夫](../Page/乌兰夫.md "wikilink")，[中华人民共和国政治家](../Page/中华人民共和国.md "wikilink")，前國家副主席（[1988年去世](../Page/1988年.md "wikilink")）

## 逝世

  - [1月29日](../Page/1月29日.md "wikilink")——[克里斯蒂安九世](../Page/克里斯蒂安九世.md "wikilink")，[丹麥國王](../Page/丹麥.md "wikilink")。（[1818年出生](../Page/1818年.md "wikilink")）
  - [2月27日](../Page/2月27日.md "wikilink")——[塞缪尔·兰利](../Page/塞缪尔·兰利.md "wikilink")，美国航空先驱（出生[1834年](../Page/1834年.md "wikilink")）
  - [4月19日](../Page/4月19日.md "wikilink")——[皮埃爾·居里](../Page/皮埃爾·居里.md "wikilink")（Pierre
    Curie），[法国物理学家](../Page/法国.md "wikilink")（出生[1859年](../Page/1859年.md "wikilink")）
  - [5月23日](../Page/5月23日.md "wikilink")——[亨利·易卜生](../Page/亨利·易卜生.md "wikilink")（Henrik
    Ibsen），[挪威作家](../Page/挪威.md "wikilink")（出生[1828年](../Page/1828年.md "wikilink")）
  - [9月5日](../Page/9月5日.md "wikilink")——[路德维希·玻尔兹曼](../Page/路德维希·玻尔兹曼.md "wikilink")（Ludwig
    Boltzmann），奥地利物理学家（出生[1844年](../Page/1844年.md "wikilink")）
  - [10月22日](../Page/10月22日.md "wikilink")——[保罗·塞尚](../Page/保罗·塞尚.md "wikilink")，[法国](../Page/法国.md "wikilink")[后印象派画家](../Page/后印象派.md "wikilink")（出生[1839年](../Page/1839年.md "wikilink")）

## 诺贝尔奖

  - [物理](../Page/诺贝尔物理学奖.md "wikilink")：[约瑟夫·汤姆孙](../Page/约瑟夫·汤姆孙.md "wikilink")（Joseph
    John Thomson）
  - [化学](../Page/诺贝尔化学奖.md "wikilink")：[亨利·莫瓦桑](../Page/亨利·莫瓦桑.md "wikilink")（Henri
    Moissan）
  - [生理和医学](../Page/诺贝尔生理学或医学奖.md "wikilink")：[卡米洛·高尔基](../Page/卡米洛·高尔基.md "wikilink")（Camillo
    Golgi）和[聖地亞哥·拉蒙-卡哈爾](../Page/聖地亞哥·拉蒙-卡哈爾.md "wikilink")（Santiago
    Ramón y Cajal）
  - [文学](../Page/诺贝尔文学奖.md "wikilink")：[乔苏埃·卡尔杜齐](../Page/焦苏埃·卡尔杜奇.md "wikilink")（Giosuè
    Carducci）
  - [和平](../Page/诺贝尔和平奖.md "wikilink")：[西奥多·罗斯福](../Page/西奥多·罗斯福.md "wikilink")（Theodore
    Roosevelt）

## 參考來源

[\*](../Category/1906年.md "wikilink")
[6年](../Category/1900年代.md "wikilink")
[0](../Category/20世纪各年.md "wikilink")