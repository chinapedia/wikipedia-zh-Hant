**1918年南澳大地震**又稱為**汕頭大地震**，係發生於1918年2月13日下午2時7分之南澳地震。其震央位於[廣東省東部](../Page/廣東省.md "wikilink")[南澳島東北約](../Page/南澳縣.md "wikilink")10公里的海底，即北緯23.5度，東經117.2度，震級為7.2級，震中烈度为X。

這次[地震不但摧毁了南澳的舊縣城深澳](../Page/地震.md "wikilink")，附近縣市多受嚴重破壞，幾乎整個華南也都感受到震動，有感範圍包括廣東、[福建](../Page/福建省.md "wikilink")、[上海](../Page/上海市.md "wikilink")、[浙江](../Page/浙江省.md "wikilink")、[江蘇](../Page/江苏省.md "wikilink")、[安徽](../Page/安徽省.md "wikilink")、[湖南](../Page/湖南省.md "wikilink")、[湖北](../Page/湖北省.md "wikilink")，以至[廣西](../Page/廣西.md "wikilink")，[蘇州](../Page/蘇州.md "wikilink")、[廣州等省市皆受到破壞](../Page/廣州.md "wikilink")。這次地震是有历史记载以来廣東最严重的一次地震，造成死傷極眾。

南澳大地震受灾至烈，华人城镇各屋宇几至尽行倒塌，驻汕各国领事和洋商同罹巨劫，[法国领事馆](../Page/法国.md "wikilink")、[荷兰领事馆倒塌](../Page/荷兰.md "wikilink")，[英国领事馆墙壁破裂](../Page/英国.md "wikilink")，[日本和](../Page/日本.md "wikilink")[美国领事馆也倾塌一角](../Page/美国.md "wikilink")。而另据记载：[三国时](../Page/三国.md "wikilink")[吴国在](../Page/吴国.md "wikilink")[苏州城北](../Page/苏州.md "wikilink")[保恩寺寺前建造的九级塔相](../Page/保恩寺.md "wikilink")，是苏州城唯一的高塔。地震时塔尖倒下半截。[南京](../Page/南京.md "wikilink")[明孝陵的石像被地震倾倒](../Page/明孝陵.md "wikilink")。而整个[广州城房屋倒坍甚多](../Page/广州.md "wikilink")。

這次地震也是[香港開埠後唯一一次造成明顯破壞的地震](../Page/香港.md "wikilink")。當時時值春節，據記載地震發生時「正在慶賀新年的香港居民，見公共建築物、電話線搖動，多奔入街中。」香港不少建築物出現裂縫，例如[香港大學的一些建築](../Page/香港大學.md "wikilink")，就因此而需要拆卸。

福州因为地处长乐—诏安断裂带北端，虽然距离震中较远，但是也震感强烈，受灾严重。

这个地震也引发了中国20世纪唯一的一次地震海啸\[1\]。

## 相关条目

  - [1600年南澳大地震](../Page/1600年南澳大地震.md "wikilink")，1600年在此次地震震中附近发生的另一次大地震。

## 参考资料

  - 《近十五年中国重要地震记》 [翁文灏](../Page/翁文灏.md "wikilink")

## 外部連結

  - [廣東歷史地震](https://web.archive.org/web/20050217040647/http://www.chenghai.net.cn/e/kp/kpe-05.htm)

  - [南澳島地震](http://www.huaxia.com/ly/shls/dao/00181298.html)

  -
[Category:1918年中国灾难](../Category/1918年中国灾难.md "wikilink")
[Category:1918年香港](../Category/1918年香港.md "wikilink")
[Category:中国地震](../Category/中国地震.md "wikilink")
[Category:广东灾难](../Category/广东灾难.md "wikilink")
[Category:香港地震](../Category/香港地震.md "wikilink")
[Category:汕頭历史](../Category/汕頭历史.md "wikilink")
[Category:南澳县](../Category/南澳县.md "wikilink")
[Category:1918年2月](../Category/1918年2月.md "wikilink")

1.