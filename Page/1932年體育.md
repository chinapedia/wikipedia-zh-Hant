<div style="float:right; border:1px; border-style:solid; padding:2px">

**请参看：**

  - [1932年](../Page/1932年.md "wikilink")
  - [1932年电影](../Page/1932年电影.md "wikilink")
  - [1932年文学](../Page/1932年文学.md "wikilink")
  - [1932年音乐](../Page/1932年音乐.md "wikilink")
  - [1932年台灣](../Page/1932年台灣.md "wikilink")

</div>

**[1932年重要國際體育賽事包括](../Page/1932年.md "wikilink")**：

  - 第十屆[夏季奧林匹克運動會](../Page/1932年夏季奥林匹克运动会.md "wikilink")；[7月30日至](../Page/7月30日.md "wikilink")[8月14日於](../Page/8月14日.md "wikilink")[美國](../Page/美國.md "wikilink")[洛杉磯舉行](../Page/洛杉矶.md "wikilink")

## 大事記

### 1月至3月

### 4月至6月

### 7月至9月

  - [7月30日](../Page/7月30日.md "wikilink")－第十屆[夏季奧林匹克運動會於](../Page/1932年夏季奥林匹克运动会.md "wikilink")[美國](../Page/美國.md "wikilink")[洛杉磯揭幕](../Page/洛杉矶.md "wikilink")。

### 9月至12月

## 綜合運動會

  - [7月30日至](../Page/7月30日.md "wikilink")[8月14日](../Page/8月14日.md "wikilink")：**[夏季奧林匹克運動會](../Page/1932年夏季奥林匹克运动会.md "wikilink")**

獎牌榜（只列出頭5名最佳成績隊伍）

| 名次 | 國家                                                                                                                                                                          | \!align="center" bgcolor="gold"|**金牌：** | \!align="center" bgcolor="silver"|**银牌：** | \!align="center" bgcolor="CC9966"|**铜牌：** | 總數  |
| -- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------- | ----------------------------------------- | ----------------------------------------- | --- |
| 1  | [US_flag_48_stars.svg](https://zh.wikipedia.org/wiki/File:US_flag_48_stars.svg "fig:US_flag_48_stars.svg") [美國](../Page/美國.md "wikilink")                                | 41                                      | 32                                        | 30                                        | 103 |
| 2  | [Flag_of_Italy_(1861-1946).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Italy_\(1861-1946\).svg "fig:Flag_of_Italy_(1861-1946).svg") [意大利](../Page/意大利.md "wikilink") | 12                                      | 12                                        | 12                                        | 36  |
| 3  | [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg") [法國](../Page/法国.md "wikilink")                                       | 10                                      | 5                                         | 4                                         | 19  |
| 4  | [Flag_of_Sweden.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Sweden.svg "fig:Flag_of_Sweden.svg") [瑞典](../Page/瑞典.md "wikilink")                                       | 9                                       | 5                                         | 9                                         | 23  |
| 5  | [Flag_of_Japan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Japan.svg "fig:Flag_of_Japan.svg") [日本](../Page/日本.md "wikilink")                                          | 7                                       | 7                                         | 4                                         | 18  |

## [籃球](../Page/篮球.md "wikilink")

## [足球](../Page/足球.md "wikilink")

  -
    <small>主條目：[1932年足球](../Page/1932年足球.md "wikilink")</small>

### 國際足球賽事

## 逝世

[Category:1930年代體育](../Category/1930年代體育.md "wikilink")
[\*](../Category/1932年體育.md "wikilink")