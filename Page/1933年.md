## 大事记

  - [日屬台灣板橋至臺北萬華的](../Page/日屬台灣.md "wikilink")[昭和橋](../Page/昭和橋.md "wikilink")(今[光復橋](../Page/光復橋.md "wikilink"))闢建。
  - 世界经济[大萧条最严重的一年](../Page/大萧条.md "wikilink")。
  - [中國](../Page/中國.md "wikilink")[河北省发生巨大](../Page/河北省.md "wikilink")[蝗灾](../Page/蝗灾.md "wikilink")。

### [1月](../Page/1月.md "wikilink")

  - [1月3日](../Page/1月3日.md "wikilink")——[日军攻入中國](../Page/日军.md "wikilink")[山海关](../Page/山海关.md "wikilink")。
  - [1月5日](../Page/1月5日.md "wikilink")——[金门大桥开建](../Page/金门大桥.md "wikilink")。
  - [1月15日](../Page/1月15日.md "wikilink")——美國通告各國不承認[滿州國](../Page/滿州國.md "wikilink")。
  - [1月17日](../Page/1月17日.md "wikilink")——[中華蘇維埃共和國政府領導人毛澤東](../Page/中華蘇維埃共和國.md "wikilink")、朱德發表《為反對日本帝國主義侵入華北，願在三個條件下與全國各軍隊共同抗日宣言》
  - [1月30日](../Page/1月30日.md "wikilink")——[阿道夫·希特勒就任](../Page/阿道夫·希特勒.md "wikilink")[德国總理](../Page/德国.md "wikilink")，公開建立法西斯專政\[1\]。

### [2月](../Page/2月.md "wikilink")

  - [2月17日](../Page/2月17日.md "wikilink")——[美国](../Page/美国.md "wikilink")《[新闻周刊](../Page/新闻周刊.md "wikilink")》(《Newsweek》)杂志创刊。
  - [2月17日](../Page/2月17日.md "wikilink")——美国通過[布萊恩法案](../Page/布萊恩法案.md "wikilink")(Blaine
    Act)，容許3.2%酒精含量的飲料。
  - [2月27日](../Page/2月27日.md "wikilink")——德国[国会纵火案](../Page/国会纵火案.md "wikilink")。

### [3月](../Page/3月.md "wikilink")

  - [3月3日](../Page/3月3日.md "wikilink")——[清雲科技大學建校](../Page/清雲科技大學.md "wikilink")；[恩斯特·台尔曼被捕](../Page/恩斯特·台尔曼.md "wikilink")；恆生銀號（即[恆生銀行前身](../Page/恆生銀行.md "wikilink")）於[香港成立開業](../Page/香港.md "wikilink")。
  - [3月4日](../Page/3月4日.md "wikilink")——[富兰克林·德拉诺·罗斯福擔任](../Page/富兰克林·德拉诺·罗斯福.md "wikilink")[美国总统](../Page/美国总统.md "wikilink")，並著手推行[新政](../Page/新政.md "wikilink")。
  - [3月20日](../Page/3月20日.md "wikilink")——[纳粹德国在](../Page/纳粹德国.md "wikilink")[达豪设置第一个](../Page/達豪集中營.md "wikilink")[集中营](../Page/集中营.md "wikilink")。
  - [3月23日](../Page/3月23日.md "wikilink")——[德国国会通过](../Page/德国国会.md "wikilink")[授权法案](../Page/1933年德國授權法案.md "wikilink")，让[阿道夫·希特勒和](../Page/阿道夫·希特勒.md "wikilink")[纳粹党可以通过任何法例](../Page/纳粹党.md "wikilink")，而不需要议会同意。
  - [3月27日](../Page/3月27日.md "wikilink")——[日本樞密院通過退出國聯通告書](../Page/樞密院_\(日本\).md "wikilink")，經日本天皇批准，由外相[內田電達國聯秘書長](../Page/內田康哉.md "wikilink")，[日本首相](../Page/日本首相.md "wikilink")[齋藤發表退出國聯聲明書](../Page/齋藤實.md "wikilink")\[2\]。
  - [3月31日](../Page/3月31日.md "wikilink")——日本在[長春設置駐](../Page/長春.md "wikilink")「滿洲國」海軍部\[3\]。

### [4月](../Page/4月.md "wikilink")

  - [4月5日](../Page/4月5日.md "wikilink")——[國民政府令行政](../Page/國民政府.md "wikilink")、立法、監察院自4月6日起所有公私款項之收付及一切交易須一律改用[銀幣](../Page/銀幣.md "wikilink")，仍用[銀兩者在法律上無效](../Page/銀兩.md "wikilink")，凡本日以前原訂以銀兩收付者，應以規定銀七錢一分五厘折合銀幣一元為標準，概以銀幣收付，如發生爭執，各司法機關應將以銀兩收付之請求駁斥\[4\]。
  - [4月13日](../Page/4月13日.md "wikilink")——香港九龍汽車（一九三三）有限公司（即現時[九龍巴士（1933）有限公司](../Page/九龍巴士.md "wikilink")）正式成立。
  - [4月17日](../Page/4月17日.md "wikilink")——日本駐華使館代辦中山訪晤美使[詹森](../Page/詹森.md "wikilink")，提出能否由外國駐華武官安排中日停戰談判，並示意日軍可退至長城線，詹森即將此事電告[美國國務院](../Page/美國國務院.md "wikilink")\[5\]。

### [5月](../Page/5月.md "wikilink")

  - [5月8日](../Page/5月8日.md "wikilink")——[莫罕达斯·甘地开始](../Page/圣雄甘地.md "wikilink")21天禁食抗议[英国占据](../Page/英国.md "wikilink")[印度](../Page/印度.md "wikilink")。
  - [5月10日](../Page/5月10日.md "wikilink")——[纳粹](../Page/纳粹主义.md "wikilink")[德國學生](../Page/德國.md "wikilink")[焚书](../Page/纳粹党焚书.md "wikilink")。
  - [5月26日](../Page/5月26日.md "wikilink")——[冯玉祥](../Page/冯玉祥.md "wikilink")、[吉鸿昌](../Page/吉鸿昌.md "wikilink")、[方振武](../Page/方振武.md "wikilink")、[佟麟阁在](../Page/佟麟阁.md "wikilink")[察哈尔](../Page/察哈尔.md "wikilink")[张家口建立](../Page/张家口市.md "wikilink")[察哈尔民众抗日同盟军](../Page/察哈尔民众抗日同盟军.md "wikilink")。
  - [5月31日](../Page/5月31日.md "wikilink")——[中国与](../Page/中国.md "wikilink")[日本签订](../Page/日本.md "wikilink")《[塘沽协定](../Page/塘沽协定.md "wikilink")》。

### [6月](../Page/6月.md "wikilink")

### [7月](../Page/7月.md "wikilink")

  - [7月14日](../Page/7月14日.md "wikilink")——德国禁止除[纳粹党外的一切](../Page/纳粹党.md "wikilink")[政党](../Page/政党.md "wikilink")。
  - [7月22日](../Page/7月22日.md "wikilink")——[美國飞机师威利](../Page/美國.md "wikilink")·波斯特完成首次单机[环球飞行](../Page/环球飞行.md "wikilink")。

### [8月](../Page/8月.md "wikilink")

  - [8月25日](../Page/8月25日.md "wikilink")——[四川](../Page/四川.md "wikilink")[茂县发生規模](../Page/茂县.md "wikilink")7.5[地震](../Page/疊溪大地震.md "wikilink")，[叠溪古城被](../Page/叠溪古城.md "wikilink")[岷江淹没形成](../Page/岷江.md "wikilink")[堰塞湖](../Page/堰塞湖.md "wikilink")，估计逾9,000人丧生。

### [9月](../Page/9月.md "wikilink")

  - [9月2日](../Page/9月2日.md "wikilink")——[義大利與](../Page/義大利.md "wikilink")[蘇聯簽署互不侵犯友好條約](../Page/蘇聯.md "wikilink")。
  - [9月12日](../Page/9月12日.md "wikilink")——[利奧·西拉德提出了](../Page/利奧·西拉德.md "wikilink")[核连锁反应的理论](../Page/核连锁反应.md "wikilink")。
  - [9月15日](../Page/9月15日.md "wikilink")——[希臘與](../Page/希臘.md "wikilink")[土耳其簽署互不侵犯條約](../Page/土耳其.md "wikilink")。

### [10月](../Page/10月.md "wikilink")

  - [10月29日](../Page/10月29日.md "wikilink")——[西班牙](../Page/西班牙.md "wikilink")[法西斯组织](../Page/法西斯主义.md "wikilink")[长枪党成立](../Page/长枪党.md "wikilink")。

### [11月](../Page/11月.md "wikilink")

  - [11月16日](../Page/11月16日.md "wikilink")——美国和[苏联建交](../Page/苏联.md "wikilink")。
  - [11月20日](../Page/11月20日.md "wikilink")——[福建事变](../Page/閩變.md "wikilink")。

### [12月](../Page/12月.md "wikilink")

  - [12月26日](../Page/12月26日.md "wikilink")——[日产公司在日本](../Page/日產汽車.md "wikilink")[东京成立](../Page/东京.md "wikilink")。
  - [12月26日](../Page/12月26日.md "wikilink")——[调频](../Page/调频.md "wikilink")[收音机被报](../Page/收音机.md "wikilink")[专利](../Page/专利.md "wikilink")。
  - [12月27日](../Page/12月27日.md "wikilink")——[曹禺创作的话剧](../Page/曹禺.md "wikilink")《雷雨》上演。

## 出生

*參見：[:Category:1933年出生](../Category/1933年出生.md "wikilink")*

  - [1月2日](../Page/1月2日.md "wikilink")——[森村诚一](../Page/森村诚一.md "wikilink")，[日本推理小说作家](../Page/日本.md "wikilink")。
  - [1月16日](../Page/1月16日.md "wikilink")——[苏珊·桑塔格](../Page/苏珊·桑塔格.md "wikilink")，美国作家。
  - [1月25日](../Page/1月25日.md "wikilink")——[柯拉蓉·艾奎諾](../Page/柯拉蓉·艾奎諾.md "wikilink")，[菲律宾总统](../Page/菲律宾.md "wikilink")。（[2009年逝世](../Page/2009年.md "wikilink")）
  - [2月20日](../Page/2月20日.md "wikilink")——[高齐云](../Page/高齐云.md "wikilink")，中国哲学家。（[2005年逝世](../Page/2005年.md "wikilink")）
  - [2月21日](../Page/2月21日.md "wikilink")——[妮娜·西蒙](../Page/妮娜·西蒙.md "wikilink")，美國歌手、作曲家與[鋼琴表演家](../Page/钢琴.md "wikilink")。（[2003年逝世](../Page/2003年.md "wikilink")）
  - [3月6日](../Page/3月6日.md "wikilink")——[奧古斯都·奧登](../Page/奧古斯都·奧登.md "wikilink")，義大利經濟學家（[2013年逝世](../Page/2013年.md "wikilink")）
  - [3月21日](../Page/3月21日.md "wikilink")——[夏舜霆](../Page/邁克爾·夏舜霆.md "wikilink")，曾任[英國副首相](../Page/英國副首相.md "wikilink")。
  - [4月26日](../Page/4月26日.md "wikilink")——[阿诺·彭齐亚斯](../Page/阿诺·彭齐亚斯.md "wikilink")，美国射电天文学家。
  - [5月3日](../Page/5月3日.md "wikilink")——[詹姆士·布朗](../Page/詹姆士·布朗.md "wikilink")，美國[靈魂樂歌手](../Page/靈魂樂.md "wikilink")。（[2006年逝世](../Page/2006年.md "wikilink")）
  - [5月22日](../Page/5月22日.md "wikilink")——[陈景润](../Page/陈景润.md "wikilink")，数学家。（[1996年逝世](../Page/1996年.md "wikilink")）
  - [6月6日](../Page/6月6日.md "wikilink")——[李紹鴻](../Page/李紹鴻.md "wikilink")，[香港](../Page/香港.md "wikilink")[醫生](../Page/醫生.md "wikilink")，首任[衛生署署長](../Page/衛生署_\(香港\).md "wikilink")。（2014年逝世）
  - [8月18日](../Page/8月18日.md "wikilink")——[羅曼·波蘭斯基](../Page/羅曼·波蘭斯基.md "wikilink")，[波兰](../Page/波兰.md "wikilink")—[美国电影导演](../Page/美国电影.md "wikilink")。
  - [9月5日](../Page/9月5日.md "wikilink")——[林文月](../Page/林文月.md "wikilink")，[台灣女性](../Page/台灣.md "wikilink")[散文作家](../Page/散文.md "wikilink")。
  - [10月2日](../Page/10月2日.md "wikilink")——[約翰·格登](../Page/約翰·格登.md "wikilink")，[英國](../Page/英國.md "wikilink")[發育生物學家](../Page/發育生物學.md "wikilink")。他主要以在細胞核移植與[選殖方面的先驅性研究而知名](../Page/選殖.md "wikilink")，2012年獲得諾貝爾生理學或醫學獎。
  - [11月4日](../Page/11月4日.md "wikilink")——[高錕](../Page/高錕.md "wikilink")，[物理學家](../Page/物理學.md "wikilink")，[2009年獲得](../Page/2009年.md "wikilink")[諾貝爾物理學獎](../Page/諾貝爾物理學獎.md "wikilink")。
  - [11月12日](../Page/11月12日.md "wikilink")——[贾拉勒·塔拉巴尼](../Page/贾拉勒·塔拉巴尼.md "wikilink")，[伊拉克總統](../Page/伊拉克.md "wikilink")。
  - [12月1日](../Page/12月1日.md "wikilink")——[藤子.F.不二雄](../Page/藤子.F.不二雄.md "wikilink")，日本漫畫家。（1996年逝世）
  - [12月3日](../Page/12月3日.md "wikilink")——[桂詩勤](../Page/桂詩勤.md "wikilink")，香港前[路政署署長](../Page/路政署.md "wikilink")。
  - [12月23日](../Page/12月23日.md "wikilink")——[明仁天皇](../Page/明仁.md "wikilink")。
  - [12月25日](../Page/12月25日.md "wikilink")——[潘文凱](../Page/潘文凱.md "wikilink")，前[越南總理](../Page/越南.md "wikilink")。
  - [乔治·夏勒](../Page/乔治·夏勒.md "wikilink")，美国动物学家。

## 逝世

  - [1月5日](../Page/1月5日.md "wikilink")──[卡尔文·柯立芝](../Page/卡尔文·柯立芝.md "wikilink")，前[美国总统](../Page/美国总统.md "wikilink")（生於[1872年](../Page/1872年.md "wikilink")）
  - [4月22日](../Page/4月22日.md "wikilink")──[亨利·萊斯爵士](../Page/亨利·萊斯.md "wikilink")，[英國](../Page/英國.md "wikilink")[工程師](../Page/工程師.md "wikilink")，[勞斯萊斯創辦人之一](../Page/勞斯萊斯.md "wikilink")（生於[1863年](../Page/1863年.md "wikilink")）
  - [6月18日](../Page/6月18日.md "wikilink")——[杨杏佛](../Page/杨杏佛.md "wikilink")，中国民权保障同盟总干事（生於[1893年](../Page/1893年.md "wikilink")）
  - [6月20日](../Page/6月20日.md "wikilink")——[克拉拉·蔡特金](../Page/克拉拉·蔡特金.md "wikilink")，国际妇女运动领袖（生於[1857年](../Page/1857年.md "wikilink")）
  - [7月31日](../Page/7月31日.md "wikilink")——[清水紫琴](../Page/清水紫琴.md "wikilink")，日本[明治時代作家](../Page/明治時代.md "wikilink")、記者、女權運動家（生於[1868年](../Page/1868年.md "wikilink")）
  - [9月21日](../Page/9月21日.md "wikilink")──[鄧中夏](../Page/鄧中夏.md "wikilink")，[中國共產黨工人運動領袖](../Page/中國共產黨.md "wikilink")，[中華全國總工會前主席](../Page/中華全國總工會.md "wikilink")，遭到處死
  - [5月6日](../Page/5月6日.md "wikilink")──[李清雲](../Page/李清雲.md "wikilink")，是一名中草藥專家、氣功大師和戰術顧問。他曾聲稱生於1734年，但也有帶有爭議性的紀錄稱他生於1677年

## 诺贝尔奖

  - [物理](../Page/诺贝尔物理学奖.md "wikilink")——[保羅·狄拉克和](../Page/保羅·狄拉克.md "wikilink")[埃尔温·薛定谔](../Page/薛定谔.md "wikilink")
  - [化学](../Page/诺贝尔化学奖.md "wikilink")——從缺
  - [生理和医学](../Page/诺贝尔生理学或医学奖.md "wikilink")——[托马斯·亨特·摩尔根](../Page/托马斯·亨特·摩尔根.md "wikilink")
  - [文学](../Page/诺贝尔文学奖.md "wikilink")——[伊凡·亚历克塞维奇·蒲宁](../Page/伊凡·亚历克塞维奇·蒲宁.md "wikilink")
  - [和平](../Page/诺贝尔和平奖.md "wikilink")——[諾曼·安吉爾](../Page/諾曼·安吉爾.md "wikilink")

## [奥斯卡金像奖](../Page/奥斯卡金像奖.md "wikilink")

（第6届，[1934年颁发](../Page/1934年.md "wikilink")）

  - [奥斯卡最佳影片奖](../Page/奥斯卡最佳影片奖.md "wikilink")——《[乱世春秋](../Page/乱世春秋.md "wikilink")》（Cavalcade）
  - [奥斯卡最佳导演奖](../Page/奥斯卡最佳导演奖.md "wikilink")——[弗兰克·劳埃德](../Page/弗兰克·劳埃德.md "wikilink")（Frank
    Lloyd）《乱世春秋》
  - [奥斯卡最佳男主角奖](../Page/奥斯卡最佳男主角奖.md "wikilink")——[查尔斯·劳顿](../Page/查尔斯·劳顿.md "wikilink")（Charles
    Laughton）《[亨利八世的私生活](../Page/亨利八世的私生活.md "wikilink")》
  - [奥斯卡最佳女主角奖](../Page/奥斯卡最佳女主角奖.md "wikilink")——[凯瑟琳·赫本](../Page/凯瑟琳·赫本.md "wikilink")（Katharine
    Hepburn）《[惊才绝艳](../Page/惊才绝艳.md "wikilink")》
  - [奥斯卡最佳男配角奖](../Page/奥斯卡最佳男配角奖.md "wikilink")——未设此项奖
  - [奥斯卡最佳女配角奖](../Page/奥斯卡最佳女配角奖.md "wikilink")——未设此项奖

（其他奖项参见[奥斯卡金像奖获奖名单](../Page/奥斯卡金像奖.md "wikilink")）

## 參考來源

[\*](../Category/1933年.md "wikilink")
[3年](../Category/1930年代.md "wikilink")
[3](../Category/20世纪各年.md "wikilink")

1.

2.
3.
4.
5.