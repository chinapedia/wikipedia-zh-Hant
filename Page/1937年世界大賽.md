**1937年**的**[世界大賽](../Page/世界大賽_\(棒球\).md "wikilink")**是由衛冕隊[紐約洋基](../Page/紐約洋基.md "wikilink")[再度與](../Page/1936年世界大賽.md "wikilink")[紐約巨人交手](../Page/舊金山巨人.md "wikilink")，洋基隊以4勝2負的戰績完成二連霸，以及15年來第6座獎盃。

在這次的系列戰中，擊出了首發世界大賽的全壘打；而則是擊出了最後一發於世界大賽中擊出的全壘打。

**總教練**：（洋基）、（巨人）。

**裁判**：（美聯）、（國聯）、（美聯）、（國聯）。

## 總結

**[美聯](../Page/美國聯盟.md "wikilink")
[紐約洋基](../Page/紐約洋基.md "wikilink")（4）-
[國聯](../Page/國家聯盟.md "wikilink")
[紐約巨人](../Page/舊金山巨人.md "wikilink")（1）**

| |場次 | 比數            | 日期     | 地點                                                 | 觀眾     |
| --- | ------------- | ------ | -------------------------------------------------- | ------ |
|     |               |        |                                                    |        |
| 1   | 巨人 1，**洋基 8** | 10月6日  | [洋基体育场](../Page/洋基体育场.md "wikilink")               | 60,573 |
| 2   | 巨人 1，**洋基 8** | 10月7日  | [洋基体育场](../Page/洋基体育场.md "wikilink")               | 57,675 |
| 3   | **洋基 5**，巨人 1 | 10月8日  | [Polo Grounds](../Page/Polo_Grounds.md "wikilink") | 37,385 |
| 4   | 洋基 3，**巨人 7** | 10月9日  | [Polo Grounds](../Page/Polo_Grounds.md "wikilink") | 44,293 |
| 5   | **洋基 4**，巨人 2 | 10月10日 | [Polo Grounds](../Page/Polo_Grounds.md "wikilink") | 38,216 |

## 逐場結果

### 第一戰：10月6日

### 第二戰：10月7日

### 第三戰：10月8日

### 第四戰：10月9日

### 第五戰：10月10日

## 外部連結

  - [Baseball
    Almanac網站上關於1937年世界大賽的頁面](http://baseball-almanac.com/ws/yr1937ws.shtml)
  - [MLB官網裡的1937年世界大賽頁面](http://mlb.mlb.com/NASApp/mlb/mlb/history/postseason/mlb_ws_recaps.jsp?feature=1937)
  - [Baseball Reference
    網站上關於1937年世界大賽的頁面](http://www.baseball-reference.com/postseason/1937_WS.shtml)

[Category:世界大賽](../Category/世界大賽.md "wikilink")
[Category:1937年體育](../Category/1937年體育.md "wikilink")
[Category:紐約洋基](../Category/紐約洋基.md "wikilink") [Category:紐約巨人
(棒球)](../Category/紐約巨人_\(棒球\).md "wikilink")
[Category:1937年美国](../Category/1937年美国.md "wikilink")