[Wuhan_german_divs.jpg](https://zh.wikipedia.org/wiki/File:Wuhan_german_divs.jpg "fig:Wuhan_german_divs.jpg")\]\]

**1938年國軍戰鬥序列**編列發布於1938年1月，這裡的國軍是指[中華民國所屬](../Page/中華民國.md "wikilink")[國民革命軍](../Page/國民革命軍.md "wikilink")。該戰鬥序列是[中国抗日战争爆發後](../Page/中国抗日战争.md "wikilink")，[中華民國政府為了](../Page/中華民國政府.md "wikilink")「持久抗戰，鞏固武漢核心」，並且有效抗擊[日本的入侵](../Page/日本.md "wikilink")，所編列的中國全境國軍戰鬥序列，戰略目的則在於「東保津浦，西守道清」。另一方面，位於[武漢的](../Page/武汉市.md "wikilink")[國民政府以修正](../Page/國民政府.md "wikilink")《國民政府軍事委員會組織大綱》方式，將[國民政府軍事委員會定為戰時政府之實質統治機構](../Page/國民政府軍事委員會.md "wikilink")，也將統率國軍的國民政府軍事委員會[委員長](../Page/委員長.md "wikilink")[蒋中正取代](../Page/蒋中正.md "wikilink")[國民政府主席成為](../Page/國民政府主席.md "wikilink")[實際上的中国最高領導人](../Page/de_facto.md "wikilink")。

1938年11月中旬，中國軍隊於[武漢保衛戰失利](../Page/武漢保衛戰.md "wikilink")，加上南戰場重要據點[廣州亦極為危險](../Page/广州市.md "wikilink")，至此，中華民國政府再度[遷都至](../Page/遷都.md "wikilink")[重慶](../Page/重庆陪都时期.md "wikilink")。為了此戰略異動變數，加上兵士折損眾多，國軍戰鬥序列於同年年底至翌年年初予以大幅度更動。而跟隨戰鬥序列更動，抗日战争第一期之第二及三階段亦告正式結束，而中國也正式邁入以重慶為根據地的第二期抗日戰爭。

## 兵力與軍費

編成於1938年元月的中國國軍戰鬥序列，連整訓部隊及未經整編部隊，共有210個步兵師，35個步兵旅，11個騎兵師，6個騎兵旅，18個砲兵團，8個砲兵營。以1937年年中之兵力估算，陸軍現役兵員約達170餘萬人。其中不包含1937年實補之30萬，及1938年實補之1713780人。

在軍費方面，1938年國軍軍費支出約十四億六千兩百萬[法幣](../Page/法幣.md "wikilink")，包含普通軍務費（糧餉）四億兩千萬法幣，國防建設費（武器）三億一千萬法幣，其他戰務費用七億二千萬法幣。而該項支出佔同年中國國家總支出之二十二億一千法幣的六成一以上。

## 中日兵力对峙

1938年，與中國國軍列對峙的日軍軍隊為[寺內壽一率領的](../Page/寺內壽一.md "wikilink")[華北方面軍](../Page/華北方面軍.md "wikilink")。以日本[關東軍為主的該派遣軍](../Page/關東軍.md "wikilink")，本只預計使用15個師團攻佔中國全境，不過年初已擴增至26師團，據估算，1938年攻擊國軍之日軍平均已達45萬人之譜。

在這種兵力對峙下，1938年年間，中國國軍與日軍發生了[徐州会战與](../Page/徐州会战.md "wikilink")[武汉会战兩次大會戰及](../Page/武汉会战.md "wikilink")193次重要戰鬥及戰役。其中，國軍陣亡249213人，負傷485804人。而日軍方面也死亡88978人，負傷355912人。

## 戰鬥序列表

[國民政府整編了全國的軍隊](../Page/國民政府.md "wikilink")，並將全國劃分為若幹作戰區域。陸海空軍最高統帥軍事委員會委員長是[蔣中正](../Page/蔣介石.md "wikilink")，參謀总長是[何應欽](../Page/何應欽.md "wikilink")。

### [國民政府軍事委員會](../Page/國民政府軍事委員會.md "wikilink")

  - [陸海空軍最高統帥軍事委員會委員長](../Page/委員長.md "wikilink")[蔣中正](../Page/蔣中正.md "wikilink")
      - 參謀總長[何應欽](../Page/何應欽.md "wikilink")

### 第一戰區

  - 司令長官：[程潛](../Page/程潛.md "wikilink")
  - 作戰地區：[北平至](../Page/北平.md "wikilink")[武漢的](../Page/武汉市.md "wikilink")[華北](../Page/华北地区.md "wikilink")[華中一帶](../Page/华中.md "wikilink")（[平漢鐵路沿線](../Page/平漢鐵路.md "wikilink")）
      - 第20集團軍：[商震](../Page/商震.md "wikilink")
          - [國民革命軍第三十二軍商震](../Page/國民革命軍第三十二軍.md "wikilink")（兼）
          - 騎兵第14旅[張占魁](../Page/張占魁.md "wikilink")
      - 第1集團軍[宋哲元](../Page/宋哲元.md "wikilink")
          - [國民革命軍第五十三軍](../Page/國民革命軍第五十三軍.md "wikilink")[萬福麟](../Page/萬福麟.md "wikilink")
          - [國民革命軍第七十七軍](../Page/國民革命軍第七十七軍.md "wikilink")[馮治安](../Page/馮治安.md "wikilink")
          - 第181師[石友三](../Page/石友三.md "wikilink")
          - 第17師[趙壽山](../Page/赵寿山.md "wikilink")
          - 國民革命軍騎兵第3軍[鄭大章](../Page/鄭大章.md "wikilink")
      - [國民革命軍第六十八軍](../Page/國民革命軍第六十八軍.md "wikilink")[劉汝明](../Page/劉汝明.md "wikilink")（直屬）
      - [國民革命軍第九十二軍](../Page/國民革命軍第九十二軍.md "wikilink")[李仙洲](../Page/李仙洲.md "wikilink")（直屬）
      - 第106師[沈克](../Page/沈克.md "wikilink")（直屬）
      - 第118師[張硯田](../Page/張硯田.md "wikilink")（直屬）
      - 新編第8師[蔣在珍](../Page/蔣在珍.md "wikilink")（直屬）
      - 新編第35師[王勁哉](../Page/王勁哉.md "wikilink")（直屬）
      - 騎兵第4師[王奇峰](../Page/王奇峰.md "wikilink")（直屬）
  - 共轄25個步兵師，2步兵旅，2騎兵師；其他特種部隊未於序列內。

### 第二戰區

  - 司令長官：[閻錫山](../Page/閻錫山.md "wikilink")
  - 作戰地區：[山西方面](../Page/山西.md "wikilink")
  - 兵力：共轄27個步兵師，3個步兵旅，3個騎兵師
  - 南路前敵總司令[衛立煌](../Page/衛立煌.md "wikilink")，下轄：
      - [國民革命軍第三軍](../Page/國民革命軍第三軍.md "wikilink")，軍長[曾萬鐘](../Page/曾萬鐘.md "wikilink")
      - [國民革命軍第九軍](../Page/國民革命軍第九軍.md "wikilink")，軍長[郭寄嶠](../Page/郭寄嶠.md "wikilink")
      - [國民革命軍第十四軍](../Page/國民革命軍第十四軍.md "wikilink")，軍長[李默庵](../Page/李默庵.md "wikilink")
      - [國民革命軍第十五軍](../Page/國民革命軍第十五軍.md "wikilink")，軍長[劉茂恩](../Page/劉茂恩.md "wikilink")
      - [國民革命軍第十七軍](../Page/國民革命軍第十七軍.md "wikilink")，軍長[高桂滋](../Page/高桂滋.md "wikilink")
      - [國民革命軍第十九軍](../Page/國民革命軍第十九軍.md "wikilink")，軍長[王靖國](../Page/王靖國.md "wikilink")
      - [國民革命軍第四十七軍](../Page/國民革命軍第四十七軍.md "wikilink")，軍長[李家鈺](../Page/李家鈺.md "wikilink")
      - [國民革命軍第六十一軍](../Page/國民革命軍第六十一軍.md "wikilink")，軍長[陳長捷](../Page/陳長捷.md "wikilink")
      - [國民革命軍第九十三軍](../Page/國民革命軍第九十三軍.md "wikilink")，軍長[劉戡](../Page/劉戡.md "wikilink")
      - 第14軍團，軍團長[馮欽哉](../Page/馮欽哉.md "wikilink")
  - 北路前敵總司令[傅作義](../Page/傅作義.md "wikilink")，下轄：
      - [國民革命軍第三十五軍](../Page/國民革命軍第三十五軍.md "wikilink")，軍長傅作義（兼）
      - [國民革命軍騎兵第一軍](../Page/國民革命軍騎兵第一軍.md "wikilink")，軍長[趙承綬](../Page/趙承綬.md "wikilink")
      - [國民革命軍騎兵第二軍](../Page/國民革命軍騎兵第二軍.md "wikilink")，軍長[何柱國](../Page/何柱國.md "wikilink")
      - [國民革命軍第三十三軍](../Page/國民革命軍第三十三軍.md "wikilink")，軍長[孙楚](../Page/孙楚.md "wikilink")（直屬）
      - [國民革命軍第三十四軍](../Page/國民革命軍第三十四軍.md "wikilink")，軍長[楊澄源](../Page/楊澄源.md "wikilink")（直屬）
      - 新編第2師，師長[金憲章](../Page/金憲章.md "wikilink")
      - 第66師，師長[杜春沂](../Page/杜春沂.md "wikilink")（直屬）
      - 第71師，師長[郭宗汾](../Page/郭宗汾.md "wikilink")（直屬）

### 第三戰區

  - 司令長官：[顧祝同](../Page/顧祝同.md "wikilink")
  - 作戰地區：[蘇](../Page/江苏.md "wikilink")[浙方面](../Page/浙江.md "wikilink")
  - 兵力：共轄24個步兵師，6個步兵旅；不含特種部隊及游擊部隊。
      - 第10集團軍司令官[劉建緒](../Page/劉建緒.md "wikilink")，下轄：[國民革命軍第二十八軍軍長](../Page/國民革命軍第二十八軍.md "wikilink")[陶廣](../Page/陶廣.md "wikilink")，[國民革命軍第七十軍軍長](../Page/國民革命軍第七十軍.md "wikilink")[李覺](../Page/李覺.md "wikilink")，第79師師長[陳安寶](../Page/陳安寶.md "wikilink")，暫編第13旅旅長[楊永清](../Page/楊永清.md "wikilink")
      - 第19集團軍司令官[羅卓英](../Page/羅卓英.md "wikilink")，下轄：[國民革命軍第四軍軍長](../Page/國民革命軍第四軍.md "wikilink")[吳奇偉](../Page/吳奇偉.md "wikilink")，[國民革命軍第十八軍軍長羅卓英](../Page/國民革命軍第十八軍.md "wikilink")（兼），[國民革命軍第七十九軍軍長](../Page/國民革命軍第七十九軍.md "wikilink")[夏楚中](../Page/夏楚中.md "wikilink")，[國民革命軍第二十五軍軍長](../Page/國民革命軍第二十五軍.md "wikilink")[萬耀煌](../Page/萬耀煌.md "wikilink")，[國民革命軍第七十三軍軍長](../Page/國民革命軍第七十三軍.md "wikilink")[王東原](../Page/王東原.md "wikilink")
      - 第23集團軍司令官[唐式遵](../Page/唐式遵.md "wikilink")，下轄：[國民革命軍第二十一軍軍長唐式遵](../Page/國民革命軍第二十一軍.md "wikilink")（兼）
      - 第28集團軍司令官[潘文華](../Page/潘文華.md "wikilink")
      - [國民革命軍第二十三軍軍長潘文華](../Page/國民革命軍第二十三軍.md "wikilink")（兼）
      - [新編第4軍軍長](../Page/新四軍.md "wikilink")[葉挺](../Page/葉挺.md "wikilink")（直屬），獨立第6旅旅長[周誌群](../Page/周誌群.md "wikilink")（直屬）
      - 寧波守備司令[王皓南](../Page/王皓南.md "wikilink")，轄第194師師長[陳德法](../Page/陳德法.md "wikilink")
      - 溫臺守備司令[徐旨乾](../Page/徐旨乾.md "wikilink")，轄暫編第12旅旅長[李國鈞](../Page/李國鈞.md "wikilink")
      - 遊擊總司令[黃紹雄](../Page/黃紹雄.md "wikilink")

### 第四戰區

  - 司令長官：[何應欽](../Page/何應欽.md "wikilink")（參謀總長兼任）
  - 作戰地區：兩廣方面
  - 兵力：共轄9個步兵師，2個步兵旅
      - 第12集團軍司令官[余漢謀](../Page/余漢謀.md "wikilink")，下轄：[國民革命軍第六十二軍軍長](../Page/國民革命軍第六十二軍.md "wikilink")[張達](../Page/張達.md "wikilink")，[國民革命軍第六十三軍軍長](../Page/國民革命軍第六十三軍.md "wikilink")[張瑞貴](../Page/張瑞貴.md "wikilink")，[國民革命軍第六十四軍軍長](../Page/國民革命軍第六十四軍.md "wikilink")[李漢魂](../Page/李漢魂.md "wikilink")，[國民革命軍第六十五軍軍長](../Page/國民革命軍第六十五軍.md "wikilink")[李振球](../Page/李振球.md "wikilink")，獨立第9旅旅長[李振良](../Page/李振良.md "wikilink")，獨立第20旅旅長[陳勉吾](../Page/陳勉吾.md "wikilink")
      - 第8軍團軍團長[夏威](../Page/夏威.md "wikilink")，
      - 虎門要塞司令[陳策](../Page/陳策.md "wikilink")

### 第五戰區

  - 司令長官：[李宗仁](../Page/李宗仁.md "wikilink")
  - 作戰地區：[天津至](../Page/天津市.md "wikilink")[南京](../Page/南京市.md "wikilink")[浦口之華東一帶](../Page/浦口区.md "wikilink")（[津浦鐵路沿線](../Page/津浦鐵路.md "wikilink")）
  - 兵力：共轄27個步兵師，3個步兵旅，不含特種部隊。
      - 第3集團軍司令官[于學忠](../Page/于學忠.md "wikilink")，下轄：[國民革命軍第五十一軍軍長於學忠](../Page/國民革命軍第五十一軍.md "wikilink")（兼），[國民革命軍第十二軍軍長](../Page/國民革命軍第十二軍.md "wikilink")[孫桐萱](../Page/孫桐萱.md "wikilink")，[國民革命軍第五十五軍軍長](../Page/國民革命軍第五十五軍.md "wikilink")[曹福休](../Page/曹福休.md "wikilink")，[國民革命軍第五十六軍軍長](../Page/國民革命軍第五十六軍.md "wikilink")[谷良民](../Page/谷良民.md "wikilink")
      - 第11集團軍司令官[李品仙](../Page/李品仙.md "wikilink")，下轄：[國民革命軍第三十一軍軍長](../Page/國民革命軍第三十一軍.md "wikilink")[韋雲淞](../Page/韋雲淞.md "wikilink")
      - 第21集團軍司令官[廖磊](../Page/廖磊.md "wikilink")，下轄：[國民革命軍第七軍軍長](../Page/國民革命軍第七軍.md "wikilink")[周祖晃](../Page/周祖晃.md "wikilink")，[國民革命軍第四十八軍軍長廖磊](../Page/國民革命軍第四十八軍.md "wikilink")（兼）
      - 第22集團軍司令官[鄧錫侯](../Page/鄧錫侯.md "wikilink")，下轄：[國民革命軍第四十一軍軍長](../Page/國民革命軍第四十一軍.md "wikilink")[孫震](../Page/孫震.md "wikilink")，[國民革命軍第四十五軍軍長鄧錫侯](../Page/國民革命軍第四十五軍.md "wikilink")（兼）
      - 第24集團軍司令官[顧祝同](../Page/顧祝同.md "wikilink")（兼），下轄：[國民革命軍第五十七軍軍長](../Page/國民革命軍第五十七軍.md "wikilink")[繆溦流](../Page/繆溦流.md "wikilink")
      - 第27集團軍司令官[楊森](../Page/楊森.md "wikilink")
      - 第3軍團軍團長[龐炳勛](../Page/龐炳勛.md "wikilink")
      - [國民革命軍第五十九軍軍長](../Page/國民革命軍第五十九軍.md "wikilink")[張自忠](../Page/張自忠.md "wikilink")
      - [海軍陸戰隊](../Page/海軍陸戰隊.md "wikilink")

### 第八戰區

  - 司令長官：蔣中正（兼），副司令長官：[朱紹良](../Page/朱紹良.md "wikilink")
  - 作戰地區：[甘肅](../Page/甘肃省.md "wikilink")、[綏遠](../Page/綏遠.md "wikilink")、[寧夏及](../Page/宁夏回族自治区.md "wikilink")[青海一帶](../Page/青海省.md "wikilink")
  - 兵力：共轄5個步兵師，4個步兵旅，5個騎兵師，4個騎兵旅，不含特種部隊。
      - 第17集團軍司令官[馬鴻逵](../Page/馬鴻逵.md "wikilink")，下轄：
          - [國民革命軍第八十一軍軍長](../Page/國民革命軍第八十一軍.md "wikilink")[馬鴻賓](../Page/馬鴻賓.md "wikilink")
          - 第168師師長馬鴻逵（兼）
          - 騎兵第1旅旅長[馬光宗](../Page/馬光宗.md "wikilink")
          - 騎兵第10旅旅長[馬全忠](../Page/馬全忠.md "wikilink")
          - 寧夏警備第1旅旅長[馬寶琳](../Page/馬寶琳.md "wikilink")
          - 寧夏警備第2旅旅長[馬得貴](../Page/馬得貴.md "wikilink")
      - [國民革命軍第八十軍軍長](../Page/國民革命軍第八十軍.md "wikilink")[孔令恂](../Page/孔令恂.md "wikilink")（獨立）
      - [國民革命軍第八十二軍軍長](../Page/國民革命軍第八十二軍.md "wikilink")[馬步芳](../Page/馬步芳.md "wikilink")（獨立）
      - 國民革命軍騎兵第5軍軍長[馬步青](../Page/馬步青.md "wikilink")
      - 第191師師長[楊德亮](../Page/楊德亮.md "wikilink")
      - 挺進軍司令[馬占山](../Page/馬占山.md "wikilink")

### 武漢衛戍總司令部

  - 總司令：[陳誠](../Page/陳誠.md "wikilink")
  - 兵力：共轄14個步兵師，1個步兵旅
      - [國民革命軍第二軍](../Page/國民革命軍第二軍.md "wikilink")，軍長[李延年](../Page/李延年.md "wikilink")
      - [國民革命軍第四十九軍](../Page/國民革命軍第四十九軍.md "wikilink")，軍長[劉多荃](../Page/劉多荃.md "wikilink")
      - [國民革命軍第五十四軍](../Page/國民革命軍第五十四軍.md "wikilink")，軍長[霍揆章](../Page/霍揆章.md "wikilink")
      - [國民革命軍第六十軍](../Page/國民革命軍第六十軍.md "wikilink")，軍長[盧漢](../Page/盧漢.md "wikilink")
      - [國民革命軍第七十五軍](../Page/國民革命軍第七十五軍.md "wikilink")，軍長[周磊](../Page/周磊.md "wikilink")
      - 第13師，師長[吳良琛](../Page/吳良琛.md "wikilink")
      - 第57師，師長[施中誠](../Page/施中誠.md "wikilink")
      - 第77師，師長[彭位仁](../Page/彭位仁.md "wikilink")
      - 江防總司令[劉興](../Page/刘兴_\(民国\).md "wikilink")
      - 海軍陸戰隊

### 西安行營

  - 主任：[蔣鼎文](../Page/蔣鼎文.md "wikilink")
  - 兵力：共轄12個步兵師，4個步兵旅，3個騎兵師
      - 第11軍團軍團長[毛炳文](../Page/毛炳文.md "wikilink")，下轄：[國民革命軍第三十七軍軍長毛炳文](../Page/國民革命軍第三十七軍.md "wikilink")（兼），第43師師長[周祥初](../Page/周祥初.md "wikilink")
      - 第17軍團軍團長[胡宗南](../Page/胡宗南.md "wikilink")，下轄：[國民革命軍第一軍軍長胡宗南](../Page/國民革命軍第一軍.md "wikilink")（兼），[國民革命軍第八軍軍長](../Page/國民革命軍第八軍.md "wikilink")[黃-{杰}-](../Page/黃杰.md "wikilink")
      - 第21軍團軍團長[鄧寶珊](../Page/鄧寶珊.md "wikilink")
      - [國民革命軍第三十八軍軍長](../Page/國民革命軍第三十八軍.md "wikilink")[孫蔚如](../Page/孫蔚如.md "wikilink")
      - [國民革命軍第四十六軍軍長](../Page/國民革命軍第四十六軍.md "wikilink")[樊崧甫](../Page/樊崧甫.md "wikilink")
      - 第86師師長[高雙成](../Page/高雙成.md "wikilink")
      - 第165師師長[魯大昌](../Page/魯大昌.md "wikilink")
      - 暫編騎兵第1師（直屬）
      - 騎兵第6軍軍長[閻炳嶽](../Page/閻炳嶽.md "wikilink")（直屬）

### 閩綏靖公署

  - 主任：[陳儀](../Page/陳儀.md "wikilink")
  - 兵力：共轄2個步兵師，4個步兵旅
      - 第75師師長[宋天才](../Page/宋天才.md "wikilink")
      - 第80師師長[陳琪](../Page/陳琪.md "wikilink")
      - 福建保安第1旅旅長[陳佩玉](../Page/陳佩玉.md "wikilink")
      - 福建保安第2旅旅長[李樹棠](../Page/李樹棠.md "wikilink")
      - 福建保安第3旅旅長[趙琳](../Page/趙琳.md "wikilink")
      - 海軍陸戰隊第2旅

### 軍委會直轄兵團

  - 兵力：共轄17個步兵師
      - 第20軍團軍團長[湯恩伯](../Page/湯恩伯.md "wikilink")，下轄：[國民革命軍第十三軍軍長湯恩伯](../Page/國民革命軍第十三軍.md "wikilink")（兼），[國民革命軍第五十二軍軍長](../Page/國民革命軍第五十二軍.md "wikilink")[關麟征](../Page/關麟征.md "wikilink")，[國民革命軍第八十五軍軍長](../Page/國民革命軍第八十五軍.md "wikilink")[王仲廉](../Page/王仲廉.md "wikilink")
      - 第2集團軍司令官[孫連仲](../Page/孫連仲.md "wikilink")，下轄：[國民革命軍第三十軍軍長](../Page/國民革命軍第三十軍.md "wikilink")[田鎮南](../Page/田鎮南.md "wikilink")，[國民革命軍第四十二軍軍長](../Page/國民革命軍第四十二軍.md "wikilink")[馮安邦](../Page/馮安邦.md "wikilink")
      - 第8集團軍司令官[張發奎](../Page/張發奎.md "wikilink")，下轄：第36師師長[蔣伏生](../Page/蔣伏生.md "wikilink")，第50師師長[成光耀](../Page/成光耀.md "wikilink")，第92師師長[黃國棟](../Page/黃國棟.md "wikilink")，第93師師長[甘麗初](../Page/甘麗初.md "wikilink")，第167師師長[薛蔚英](../Page/薛蔚英.md "wikilink")
      - 第26集團軍司令官[徐源泉](../Page/徐源泉.md "wikilink")，下轄：[國民革命軍第十軍軍長徐源泉](../Page/國民革命軍第十軍.md "wikilink")（兼），[國民革命軍第八十七軍軍長](../Page/國民革命軍第八十七軍.md "wikilink")[劉膺古](../Page/劉膺古.md "wikilink")

## 空軍

  - 第一大隊（轟炸機），下轄2個中隊：[諾斯普羅-2E型](../Page/諾斯普羅-2E型.md "wikilink")18架、[弗力特-7型教練機](../Page/弗力特-7型教練機.md "wikilink")1架
  - 第二大隊（轟炸機），下轄3個中隊：諾斯普羅-2E型27架
  - 第三大隊（戰鬥機），下轄3個中隊：[霍克III型](../Page/霍克III型.md "wikilink")9架、[波音281型](../Page/波音281型.md "wikilink")10架、[布瑞達-27型](../Page/布瑞達-27型.md "wikilink")2架、[菲亞特-32型](../Page/菲亞特-32型.md "wikilink")3架
  - 第四大隊（戰鬥機），下轄3個中隊：霍克III型28架、[福克·華夫型教練機](../Page/福克·華夫型教練機.md "wikilink")1架
  - 第五大隊（戰鬥機），下轄3個中隊：霍克III型28架、福克·華夫型教練機1架
  - 第六大隊（戰鬥機、轟炸機），下轄4個中隊：[道格拉斯轟炸機](../Page/道格拉斯轟炸機.md "wikilink")27架、菲亞特-32型戰鬥機3架、[波羅尼-111型轟炸機](../Page/波羅尼-111型.md "wikilink")7架、[德·哈蘭-摩斯教練機](../Page/德·哈蘭-摩斯教練機.md "wikilink")2架
  - 第七大隊（偵察機），下轄4個中隊：[可塞型偵察機](../Page/可塞型偵察機.md "wikilink")27架
  - 第八大隊（轟炸機），下轄3個中隊：[薩伏亞S-72型](../Page/薩伏亞S-72型.md "wikilink")6架、道格拉斯轟炸機6架、[亨格爾111-A型](../Page/亨格爾111-A型.md "wikilink")6架、[馬丁型](../Page/馬丁型.md "wikilink")6架、[福克·華夫型教練機](../Page/福克·華夫型教練機.md "wikilink")1架
  - 第九大隊（攻擊機），下轄2個中隊：[雪萊克A-12型](../Page/雪萊克A-12型.md "wikilink")20架
  - 直轄大隊，下轄5個中隊：
      - 第十三中隊：道格拉斯轟炸機7架
      - 第十八中隊：道格拉斯轟炸機9架、可塞型偵察機3架
      - 第二十中隊：可塞型偵察機11架
      - 第二十九中隊：霍克III型9架、霍克II型3架
      - 第三十一中隊：道格拉斯轟炸機9架

## 海軍

  - 第一艦隊下轄：[海容](../Page/海容级巡洋舰.md "wikilink")、[海籌](../Page/海容级巡洋舰.md "wikilink")、寧海、逸仙、大同、自強、永健、永績、[中山](../Page/中山艦.md "wikilink")、建康、安定、克安，12艘17484噸
  - 第二艦隊下轄：楚有、楚泰、楚同、楚謙、楚觀、江元、江貞、永綏、民生、民權、鹹寧、德勝、江鯤、江犀、湖鵬、湖鷹、湖鶚、湖隼，19艘9359噸
  - 第三艦隊下轄：定海、永翔、楚豫、江利、鎮海、同安、海鷗、海鶴、海清、海燕、海駿、海逢、[海琛](../Page/海容级巡洋舰.md "wikilink")，14艘14717噸
  - 練習艦隊下轄：應瑞、通濟，2艘4360噸
  - 巡防艦隊下轄：順勝、義勝、勇勝、仁勝、海寧、江寧、撫寧、綏寧、肅寧、威寧、崇寧、義寧、正寧、長寧，14艘4270噸
  - 測量艦隊下轄：甘露、青天、誠勝、公勝、景星、廣雲、璈日，7艘3000噸
  - 直轄艦隊下轄：平海、普安、武勝、辰字號[魚雷艇](../Page/魚雷艇.md "wikilink")、宿字號魚雷艇，5艘5825噸

## 注释

## 參考文獻

  - [何應欽](../Page/何應欽.md "wikilink")：《日軍侵華八年抗戰史》.
    臺北：[黎明文化事業公司](../Page/黎明文化事業公司.md "wikilink")，1982年.
  - 張朋園、沈懷玉 合編：《國民政府職官年表》.
    臺北：[中央研究院近代史研究所](../Page/中央研究院近代史研究所.md "wikilink")，1987年.
  - [郭廷以](../Page/郭廷以.md "wikilink")：《[中華民國史事日誌](../Page/中華民國史事日誌.md "wikilink")》.
    臺北：中央研究院近代史研究所,1984年.

{{-}}

[Category:1938年中国军事](../Category/1938年中国军事.md "wikilink")
[Category:国民革命军](../Category/国民革命军.md "wikilink")
[Category:抗日战争战斗序列](../Category/抗日战争战斗序列.md "wikilink")