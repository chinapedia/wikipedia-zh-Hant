## 大事记

  - **[中國](../Page/中國.md "wikilink")**
      - [二月](../Page/二月.md "wikilink")，[徐州](../Page/徐州刺史部.md "wikilink")[牧](../Page/州牧.md "wikilink")[陶謙向](../Page/陶謙.md "wikilink")[劉備求救以拒](../Page/劉備.md "wikilink")[曹操](../Page/曹操.md "wikilink")，使屯[沛縣](../Page/沛县.md "wikilink")。
      - 同月，曹操根據地[兗州](../Page/兖州刺史部.md "wikilink")[陳留](../Page/陳留郡.md "wikilink")[太守](../Page/太守.md "wikilink")[張邈叛迎](../Page/張邈.md "wikilink")[呂布](../Page/呂布.md "wikilink")，曹操自徐州引軍還兗州，爆發[兗州之戰](../Page/兗州之戰.md "wikilink")。
      - [五月](../Page/五月.md "wikilink")，[河西四郡因離](../Page/河西四郡.md "wikilink")[州治](../Page/治所.md "wikilink")[漢陽](../Page/汉阳郡.md "wikilink")[冀縣過遠](../Page/冀县.md "wikilink")，其道路又為河寇所阻，上書求別置州。
      - [六月](../Page/六月.md "wikilink")，析[涼州之](../Page/涼州刺史部.md "wikilink")[武威](../Page/武威郡.md "wikilink")、[張掖](../Page/張掖郡.md "wikilink")、[酒泉](../Page/酒泉郡.md "wikilink")、[敦煌](../Page/敦煌郡.md "wikilink")、[西海五郡新置](../Page/西海郡.md "wikilink")[雍州](../Page/雍州刺史部.md "wikilink")，州治[姑臧](../Page/姑臧.md "wikilink")，以陳留[邯鄲商為雍州](../Page/邯鄲商.md "wikilink")[刺史](../Page/刺史.md "wikilink")。
      - [十二月](../Page/十二月.md "wikilink")，因天火燒城，[益州牧](../Page/益州刺史部.md "wikilink")[劉焉將益州治所由](../Page/劉焉_\(益州牧\).md "wikilink")[綿竹徙至](../Page/綿竹縣.md "wikilink")[成都](../Page/成都市.md "wikilink")。
      - 同月，劉焉病亡，其子[劉璋被舉為益州牧](../Page/劉璋.md "wikilink")。
      - 同月，陶謙病逝，劉備被推舉為徐州牧。
      - 析[右扶風](../Page/右扶風.md "wikilink")、[安定郡新設](../Page/安定郡.md "wikilink")[新平郡](../Page/新平郡.md "wikilink")，治[漆縣](../Page/漆县.md "wikilink")。

<!-- end list -->

  - **[羅馬帝國](../Page/羅馬帝國.md "wikilink")**
      - [統帥](../Page/Imperator.md "wikilink")[塞普蒂米烏斯·塞維魯及](../Page/塞普蒂米烏斯·塞維魯.md "wikilink")[克勞迪烏斯·阿爾拜努斯任](../Page/克勞迪烏斯·阿爾拜努斯.md "wikilink")[羅馬執政官](../Page/羅馬執政官.md "wikilink")。
      - [伊蘇斯戰役](../Page/伊蘇斯戰役.md "wikilink")：[皇帝塞维鲁帅第十二](../Page/羅馬皇帝.md "wikilink")[军团至](../Page/罗马军团.md "wikilink")[基利家](../Page/基利家.md "wikilink")，打败[叙利亚](../Page/叙利亚_\(罗马行省\).md "wikilink")[总督](../Page/羅馬總督.md "wikilink")[佩森尼爾斯·奈哲爾](../Page/佩森尼爾斯·奈哲爾.md "wikilink")（Pescennius
        Niger）,奈哲爾退至[安條克](../Page/安條克.md "wikilink")，為塞維魯的軍隊所處決。
      - 塞維魯包圍[拜占庭](../Page/拜占庭.md "wikilink")（194年 -
        [196年](../Page/196年.md "wikilink")），其[城牆嚴重受損](../Page/君士坦丁堡城牆.md "wikilink")。

<!-- end list -->

  - 其他
      - 塞爾維亞牧首[愛任紐宣佈](../Page/愛任紐.md "wikilink")[諾斯底主義為](../Page/諾斯底主義.md "wikilink")[異端](../Page/異端.md "wikilink")。

## 出生

  - [朱據](../Page/朱據.md "wikilink")（194年－[250年](../Page/250年.md "wikilink")），[吳將](../Page/孫吳_\(三國\).md "wikilink")。56岁

## 逝世

  - [刘焉](../Page/劉焉_\(益州牧\).md "wikilink")（[2世紀](../Page/2世紀.md "wikilink")?－194年）
  - [陶謙](../Page/陶謙.md "wikilink")（[132年](../Page/132年.md "wikilink")－194年）62岁
  - [马日磾](../Page/马日磾.md "wikilink")（? －194年）
  - [佩森尼爾斯·奈哲爾](../Page/佩森尼爾斯·奈哲爾.md "wikilink")（[140年](../Page/140年.md "wikilink")
    - 194年），羅馬[篡位者](../Page/篡位者.md "wikilink")。54岁

[\*](../Category/194年.md "wikilink")
[4年](../Category/190年代.md "wikilink")
[9](../Category/2世纪各年.md "wikilink")