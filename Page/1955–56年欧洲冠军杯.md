**1955–56年歐洲冠軍盃**是首屆舉行的[歐洲冠軍球會盃](../Page/歐洲冠軍聯賽.md "wikilink")，賽事共有 16
支隊伍參與。決賽階段於1956年6月13日在[巴黎王子球場進行](../Page/巴黎王子球場.md "wikilink")，最終來自[西班牙的](../Page/西班牙.md "wikilink")[皇家馬德里擊敗](../Page/皇家馬德里足球俱樂部.md "wikilink")[法國球會](../Page/法國.md "wikilink")[蘭斯獲得首屆冠軍](../Page/蘭斯足球俱樂部.md "wikilink")。

## 參賽球隊

按照歐洲足協原定邀請英格蘭的[車路士](../Page/車路士.md "wikilink")，但車路士以應付國內聯賽為由而拒絕，最後由波蘭球隊[華沙基華迪亞補上](../Page/華沙基華迪亞足球會.md "wikilink")。此外，[漢維特](../Page/布達佩斯捍衛者足球俱樂部.md "wikilink")、荷蘭體育會（Holland
Sport）及BK哥本哈根都拒絕代表匈牙利、荷蘭和丹麥出賽，改由[禾路斯](../Page/MTK布達佩斯足球俱樂部.md "wikilink")、[PSV燕豪芬及](../Page/PSV燕豪芬.md "wikilink")[阿曉斯參賽](../Page/奧胡斯體操協會俱樂部.md "wikilink")。該項賽事成為歐洲足協成立以來唯一一個邀請[薩爾代表參加的賽事](../Page/薩爾保護領.md "wikilink")，及後薩爾於1957年歸入西德領土。

<table>
<thead>
<tr class="header">
<th><p>中文名稱</p></th>
<th><p>英文名稱</p></th>
<th><p>參賽資格</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/維也納迅速足球會.md" title="wikilink">維也納迅速</a></p></td>
<td><p>SK Rapid Wien</p></td>
<td><p>|1954－55年奧地利錦標賽季軍</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/皇家安德列治體育會.md" title="wikilink">安德列治</a></p></td>
<td><p>R.S.C. Anderlecht</p></td>
<td><p>|1954–55年比利時甲組聯賽冠軍</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奧胡斯體操協會俱樂部.md" title="wikilink">阿曉斯</a></p></td>
<td><p>AGF Aarhus</p></td>
<td><p>'''1954–55年丹麥甲組聯賽冠軍</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蘭斯足球俱樂部.md" title="wikilink">蘭斯</a></p></td>
<td><p>Stade de Reims</p></td>
<td><p>'''1954–55年法國甲組聯賽冠軍</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/MTK布達佩斯足球俱樂部.md" title="wikilink">禾路斯</a></p></td>
<td><p>Vörös Lobogó</p></td>
<td><p>1954–55年匈牙利甲組聯賽亞軍（冠軍布達佩斯捍衛者拒絕參賽）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/AC米蘭.md" title="wikilink">AC米蘭</a></p></td>
<td><p>A.C. Milan</p></td>
<td><p>'''1954–55年意大利甲組聯賽冠軍</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/PSV燕豪芬.md" title="wikilink">PSV燕豪芬</a></p></td>
<td><p>PSV Eindhoven</p></td>
<td><p>1954–55年荷蘭錦標賽季軍（荷蘭體育會拒絕參賽）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/華沙基華迪亞體育會.md" title="wikilink">華沙基華迪亞</a></p></td>
<td><p>Gwardia Warszawa</p></td>
<td><p>1954年波蘭盃冠軍（英格蘭代表車路士拒絕參賽）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/葡萄牙士砵亭俱樂部.md" title="wikilink">士砵亭</a></p></td>
<td><p>Sporting CP</p></td>
<td><p>1954–55年葡萄牙國家錦標賽季軍</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/薩爾布呂肯足球俱樂部.md" title="wikilink">沙貝魯根</a></p></td>
<td><p>1. FC Saarbrücken</p></td>
<td><p>薩爾代表</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/希伯尼安足球俱樂部.md" title="wikilink">喜百年</a></p></td>
<td><p>Hibernian F.C.</p></td>
<td><p>1954–55年蘇格蘭甲組聯賽第五名</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/皇家馬德里.md" title="wikilink">皇家馬德里</a></p></td>
<td><p>Real Madrid C.F.</p></td>
<td><p>'''1954–55年西班牙甲組聯賽冠軍</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/佐加頓斯IF.md" title="wikilink">佐加頓斯</a></p></td>
<td><p>Djurgården</p></td>
<td><p>'''1954–55年瑞典甲組聯賽冠軍</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/塞爾維特足球俱樂部.md" title="wikilink">塞維特</a></p></td>
<td><p>Servette F.C.</p></td>
<td><p>1954–55年瑞士甲組聯賽第六名</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/紅白埃森足球俱樂部.md" title="wikilink">艾遜</a></p></td>
<td><p>Rot-Weiss Essen</p></td>
<td><p>'''1955年德國錦標賽冠軍</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/游擊隊足球俱樂部.md" title="wikilink">柏迪遜</a></p></td>
<td><p>F.K. Partizan</p></td>
<td><p>1954－55年南斯拉夫甲組聯賽第五名</p></td>
</tr>
</tbody>
</table>

## 賽事

### 第一圈

|}

### 半準決賽

|}

### 準決賽

|}

### 決賽

<table>
<thead>
<tr class="header">
<th><p>歐洲冠軍盃<br />
1955–56 冠軍</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><br />
<strong><a href="../Page/皇家馬德里足球俱樂部.md" title="wikilink">皇家馬德里</a></strong><br />
<strong>第 1 次奪冠</strong></p></td>
</tr>
</tbody>
</table>

## 外部連結

  - [歐洲足協官方網站有關資料](http://www.uefa.com/competitions/UCL/history/Season=1955/intro.html)
  - [RSSSF有關資料](http://www.rsssf.com/ec/ecomp.html)

[Category:1956年足球](../Category/1956年足球.md "wikilink")
[Category:歐洲冠軍盃](../Category/歐洲冠軍盃.md "wikilink")