**1957年**是联合国宣布的[国际地球观测年](../Page/国际地球观测年.md "wikilink")。

## 大事记

  - [中国](../Page/中国.md "wikilink")[哈尔滨工业大学研制成功中国第一台模拟式电子计算机](../Page/哈尔滨工业大学.md "wikilink")

### [1月](../Page/1月.md "wikilink")

  - [1月1日](../Page/1月1日.md "wikilink")－[萨尔重新回到](../Page/萨尔.md "wikilink")[德国](../Page/德国.md "wikilink")，成为[西德第](../Page/西德.md "wikilink")10个州。
  - [1月5日](../Page/1月5日.md "wikilink")－[艾森豪威尔主义发表](../Page/艾森豪威尔主义.md "wikilink")。
  - [1月10日](../Page/1月10日.md "wikilink")－[麦克米伦接替](../Page/麦克米伦.md "wikilink")[艾登的](../Page/艾登.md "wikilink")[英国首相职务](../Page/英国首相.md "wikilink")。
  - [1月23日](../Page/1月23日.md "wikilink")－世界上第一台[醉酒呼吸分析仪首次在](../Page/醉酒呼吸分析仪.md "wikilink")[瑞典投入使用](../Page/瑞典.md "wikilink")。

### [2月](../Page/2月.md "wikilink")

  - [2月5日](../Page/2月5日.md "wikilink")－[中国第一次公演西方歌剧](../Page/中国.md "wikilink")《[茶花女](../Page/茶花女.md "wikilink")》。
  - [2月7日](../Page/2月7日.md "wikilink")－[中国与](../Page/中国.md "wikilink")[斯里兰卡建交](../Page/斯里兰卡.md "wikilink")。
  - [2月26日](../Page/2月26日.md "wikilink")—[銮披汶·颂堪作弊获得大选胜利](../Page/銮披汶·颂堪.md "wikilink")。

### [3月](../Page/3月.md "wikilink")

  - [3月12日](../Page/3月12日.md "wikilink")——美國國務卿[杜勒斯提出美國對中共三原則](../Page/杜勒斯.md "wikilink")：不承認中共，繼續承認中華民國，反對中共加入聯合國\[1\]。
  - [3月25日](../Page/3月25日.md "wikilink")－《[建立欧洲经济共同体条约](../Page/羅馬條約.md "wikilink")》在[罗马签订](../Page/罗马.md "wikilink")，邁入[歐洲聯盟第一步](../Page/歐洲聯盟.md "wikilink")。

### [4月](../Page/4月.md "wikilink")

  - [4月12日](../Page/4月12日.md "wikilink")－[中国道教协会成立](../Page/中国道教协会.md "wikilink")。
  - [4月13日](../Page/4月13日.md "wikilink")－[黄河上的](../Page/黄河.md "wikilink")[三门峡水利枢纽工程正式开工](../Page/三门峡水利枢纽工程.md "wikilink")。
  - [4月27日](../Page/4月27日.md "wikilink")－[马寅初在北京大学大饭厅发表](../Page/马寅初.md "wikilink")[人口问题的演讲](../Page/人口问题.md "wikilink")。

### [5月](../Page/5月.md "wikilink")

  - [5月1日](../Page/5月1日.md "wikilink")－《[人民日报](../Page/人民日报.md "wikilink")》刊载了[中共中央在](../Page/中共中央.md "wikilink")[4月27日发出的](../Page/4月27日.md "wikilink")《[关于整风运动的指示](../Page/关于整风运动的指示.md "wikilink")》，决定在全党开展[整风运动](../Page/整風運動.md "wikilink")。
  - [5月15日](../Page/5月15日.md "wikilink")－[英国在](../Page/英国.md "wikilink")[太平洋](../Page/太平洋.md "wikilink")[圣诞岛上进行了该国首枚](../Page/圣诞岛.md "wikilink")[氢弹的试爆](../Page/氢弹.md "wikilink")，成为第三个拥有氢弹的国家。
  - [5月15日起](../Page/5月15日.md "wikilink") -
    中共中央先后3次发出关于[干部参加](../Page/干部.md "wikilink")[劳动的决定](../Page/劳动.md "wikilink")，全国有百万干部[下放到](../Page/下放.md "wikilink")[农村和](../Page/农村.md "wikilink")[工矿企业参加劳动](../Page/工矿企业.md "wikilink")。
  - [5月24日](../Page/5月24日.md "wikilink")－[台灣民眾因不滿](../Page/台灣.md "wikilink")[劉自然案的判決](../Page/劉自然.md "wikilink")，群聚攻擊[美國駐台北大使館](../Page/美國駐台北大使館.md "wikilink")、包圍警察局，是為[五二四事件](../Page/五二四事件.md "wikilink")。
  - [5月29日](../Page/5月29日.md "wikilink")－[香港第一間電視台](../Page/香港.md "wikilink")、全球首間華語電視台[麗的映聲](../Page/麗的映聲.md "wikilink")（即現時的[亞洲電視](../Page/亞洲電視.md "wikilink")）啟播。

### [6月](../Page/6月.md "wikilink")

  - [6月8日](../Page/6月8日.md "wikilink")－中共中央发出[毛泽东起草的](../Page/毛泽东.md "wikilink")《[关于组织力量准备反击右派分子进攻的指示](../Page/关于组织力量准备反击右派分子进攻的指示.md "wikilink")》，标志着[中国大陆大规模的](../Page/中国大陆.md "wikilink")[反右斗争的开始](../Page/反右斗争.md "wikilink")。

### [7月](../Page/7月.md "wikilink")

  - [7月1日](../Page/7月1日.md "wikilink")－[鄭州博物館成立](../Page/鄭州博物館.md "wikilink")，原名鄭州市文物陳列室。是[中國首批](../Page/中國.md "wikilink")[國家一級博物館](../Page/國家一級博物館.md "wikilink")。

### [8月](../Page/8月.md "wikilink")

  - [8月31日](../Page/8月31日.md "wikilink")－[马来西亚半岛及](../Page/马来西亚半岛.md "wikilink")[新加坡脱离](../Page/新加坡.md "wikilink")[英国殖民政府](../Page/英国.md "wikilink")，正式宣布独立。现为马来西亚国庆日。

### [9月](../Page/9月.md "wikilink")

  - [9月17日](../Page/9月17日.md "wikilink")—泰国政变小组内部崩溃，[銮披汶·颂堪被](../Page/銮披汶·颂堪.md "wikilink")[沙立·他那叻推翻](../Page/沙立·他那叻.md "wikilink")。
  - [9月29日](../Page/9月29日.md "wikilink")－[蘇聯](../Page/蘇聯.md "wikilink")[克什特姆發生](../Page/克什特姆.md "wikilink")[核廢料爆炸事故](../Page/核廢料.md "wikilink")。

### [10月](../Page/10月.md "wikilink")

  - [10月4日](../Page/10月4日.md "wikilink")－[苏联发射第一颗人造](../Page/苏联.md "wikilink")[地球](../Page/地球.md "wikilink")[卫星](../Page/卫星.md "wikilink")[斯普特尼克1號](../Page/斯普特尼克1號.md "wikilink")。

### [11月](../Page/11月.md "wikilink")

  - [11月13日](../Page/11月13日.md "wikilink")－中国《[人民日报](../Page/人民日报.md "wikilink")》发表社论《发动全民，讨论四十条纲要，掀起农业生产的新高潮》，最早提出了“[大跃进](../Page/大跃进.md "wikilink")”的口号。
  - [11月17日](../Page/11月17日.md "wikilink")－中国选手[郑凤荣打破女子](../Page/郑凤荣.md "wikilink")[跳高世界纪录](../Page/跳高.md "wikilink")。

### [12月](../Page/12月.md "wikilink")

  - [12月20日](../Page/12月20日.md "wikilink")－[波音707首次飛行](../Page/波音707.md "wikilink")，首款四引擎[噴氣客機](../Page/噴射客機列表.md "wikilink")，載客量為140至189人
    。
  - 在[台灣](../Page/台灣.md "wikilink")，[五月畫會的成立造成了畫壇走向現代藝術風格](../Page/五月畫會.md "wikilink")。
  - [北京古城墙被拆毁](../Page/北京.md "wikilink")。
  - 丹麥科學家[延斯·克里斯蒂安·斯科發現了](../Page/延斯·克里斯蒂安·斯科.md "wikilink")[鈉鉀泵](../Page/鈉鉀泵.md "wikilink")。

## 出生

  - [伊凡.格拉森博格](../Page/伊凡.格拉森博格.md "wikilink")，[嘉能可國際股份有限公司首席執行官](../Page/嘉能可國際股份有限公司.md "wikilink")
  - [池莉](../Page/池莉.md "wikilink")，[中國當代](../Page/中國.md "wikilink")[女作家](../Page/女作家.md "wikilink")
  - [蔡明亮](../Page/蔡明亮.md "wikilink")，[馬來西亞籍](../Page/馬來西亞.md "wikilink")[台灣](../Page/台灣.md "wikilink")[電影](../Page/電影.md "wikilink")[導演](../Page/導演.md "wikilink")
  - [王湘琦](../Page/王湘琦.md "wikilink")，[台灣醫師小說家](../Page/台灣.md "wikilink")
  - [歐博哲](../Page/歐博哲.md "wikilink")，[德國](../Page/德國.md "wikilink")[外交官](../Page/外交官.md "wikilink")
  - [1月12日](../Page/1月12日.md "wikilink")——[武居智久](../Page/武居智久.md "wikilink")，[日本](../Page/日本.md "wikilink")[海上自衛隊將領](../Page/海上自衛隊.md "wikilink")、第32任海上幕僚長（海軍參謀長）
  - [1月18日](../Page/1月18日.md "wikilink")——[毛孟靜](../Page/毛孟靜.md "wikilink")，[香港](../Page/香港.md "wikilink")[西九龍](../Page/九龍西選區.md "wikilink")[政治家](../Page/政治家.md "wikilink")
  - [2月16日](../Page/2月16日.md "wikilink")——[陽帆](../Page/陽帆.md "wikilink")，臺灣男藝人
  - [4月15日](../Page/4月15日.md "wikilink")——[阿吉仔](../Page/阿吉仔.md "wikilink")，台語歌手
  - [5月10日](../Page/5月10日.md "wikilink")——[席德·维瑟斯](../Page/席德·维瑟斯.md "wikilink")，[英国](../Page/英国.md "wikilink")[贝斯手](../Page/低音吉他.md "wikilink")（[1979年逝世](../Page/1979年.md "wikilink")）
  - [5月13日](../Page/5月13日.md "wikilink")——[林鄭月娥](../Page/林鄭月娥.md "wikilink")，香港特區第五屆政府行政長官。
  - [6月8日](../Page/6月8日.md "wikilink")——[譚香文](../Page/譚香文.md "wikilink")，[香港](../Page/香港.md "wikilink")[立法會前議員](../Page/立法會.md "wikilink")，[公民黨創會會員](../Page/公民黨.md "wikilink")（已退黨）
  - [6月10日](../Page/6月10日.md "wikilink")——[姐齿秀次](../Page/姐齿秀次.md "wikilink")，[日本](../Page/日本.md "wikilink")[建筑师](../Page/建筑师.md "wikilink")
  - [6月16日](../Page/6月16日.md "wikilink")——[雒發生](../Page/雒發生.md "wikilink")，[河南國龍礦業建設有限公司董事長及總經理](../Page/河南國龍礦業建設有限公司.md "wikilink")。
  - [7月1日](../Page/7月1日.md "wikilink")——[潘长江](../Page/潘长江.md "wikilink")，著名的电影演员、电视演员、小品演员、歌手和主持人
  - [7月26日](../Page/7月26日.md "wikilink")——[元彪](../Page/元彪.md "wikilink")，香港動作演員，七小福成員之一
  - [8月11日](../Page/8月11日.md "wikilink")——[孫正義](../Page/孫正義.md "wikilink")，日本企业家
  - [9月9日](../Page/9月9日.md "wikilink")——[鄭裕玲](../Page/鄭裕玲.md "wikilink")，香港資深女演員
  - [9月13日](../Page/9月13日.md "wikilink")──[冼灝英](../Page/冼灝英.md "wikilink")，香港演員
  - [9月26日](../Page/9月26日.md "wikilink")──[吳國昌](../Page/吳國昌.md "wikilink")，[澳門政治人物](../Page/澳門.md "wikilink")
  - [10月2日](../Page/10月2日.md "wikilink")——[赵本山](../Page/赵本山.md "wikilink")，著名小品、东北二人转演员
  - [10月10日](../Page/10月10日.md "wikilink")——[高桥留美子](../Page/高桥留美子.md "wikilink")，日本漫画家
  - [10月13日](../Page/10月13日.md "wikilink")——[余綺霞](../Page/余綺霞.md "wikilink")，已故藝人
  - [10月15日](../Page/10月15日.md "wikilink")——[米拉·奈兒](../Page/米拉·奈兒.md "wikilink")，印度導演
  - [12月6日](../Page/12月6日.md "wikilink")——[冯巩](../Page/冯巩.md "wikilink")，中国相声演员、电影演员

## 逝世

  - [2月8日](../Page/2月8日.md "wikilink")：[馮·諾伊曼](../Page/馮·諾伊曼.md "wikilink")，[匈牙利裔](../Page/匈牙利.md "wikilink")[美國數學家](../Page/美國.md "wikilink")（[1903年](../Page/1903年.md "wikilink")[12月28日出生](../Page/12月28日.md "wikilink")）
  - [5月2日](../Page/5月2日.md "wikilink")：[约瑟夫·麦卡锡](../Page/约瑟夫·麦卡锡.md "wikilink")，美国政治人物。
  - [9月16日](../Page/9月16日.md "wikilink")：[齐白石](../Page/齐白石.md "wikilink")，94歲，中國[國画大师](../Page/國画.md "wikilink")。（生於[1864年](../Page/1864年.md "wikilink"))
  - [9月20日](../Page/9月20日.md "wikilink")：[西贝柳斯](../Page/西贝柳斯.md "wikilink")，[芬蘭作曲家](../Page/芬蘭.md "wikilink")
  - [9月22日](../Page/9月22日.md "wikilink")：[周璇](../Page/周璇.md "wikilink")，38歲，[中國上海著名影星](../Page/中國.md "wikilink")、歌星，疑急性腦膜炎。（生於[1920年](../Page/1920年.md "wikilink"))
  - [11月15日](../Page/11月15日.md "wikilink")：[陳楨](../Page/陳楨.md "wikilink")
    (協三_Shisan C.
    Chen)，64歲，中華民國[中央研究院第一屆](../Page/中央研究院.md "wikilink")(生命科學組)院士（生於[1894年](../Page/1894年.md "wikilink"))
    [中央研究院](https://academicians.sinica.edu.tw/index.php?func=1-D)

## 诺贝尔奖

  - [物理](../Page/诺贝尔物理学奖.md "wikilink")：[楊振寧](../Page/楊振寧.md "wikilink")、[李政道](../Page/李政道.md "wikilink")
  - [化学](../Page/诺贝尔化学奖.md "wikilink")：[亞歷山大·R·托德，托德男爵](../Page/亞歷山大·R·托德，托德男爵.md "wikilink")
  - [生理和医学](../Page/诺贝尔医学奖.md "wikilink")：[達尼埃爾·博韋](../Page/達尼埃爾·博韋.md "wikilink")
  - [文学](../Page/诺贝尔文学奖.md "wikilink")：[阿爾貝·卡繆](../Page/阿爾貝·卡繆.md "wikilink")
  - [和平](../Page/诺贝尔和平奖.md "wikilink")：[萊斯特·皮爾遜](../Page/萊斯特·皮爾遜.md "wikilink")

## [奥斯卡金像奖](../Page/奥斯卡金像奖.md "wikilink")

（第30届，[1958年颁发](../Page/1958年.md "wikilink")）

  - [奥斯卡最佳影片奖](../Page/奥斯卡最佳影片奖.md "wikilink")——《[桂河大桥](../Page/桂河大桥.md "wikilink")》（The
    Bridge on the River Kwai）
  - [奥斯卡最佳导演奖](../Page/奥斯卡最佳导演奖.md "wikilink")——[戴维·利恩](../Page/戴维·利恩.md "wikilink")（David
    Lean）《桂河大桥》
  - [奥斯卡最佳男主角奖](../Page/奥斯卡最佳男主角奖.md "wikilink")——[亚历克·吉尼斯](../Page/亚历克·吉尼斯.md "wikilink")（Alec
    Guinness）《桂河大桥》
  - [奥斯卡最佳女主角奖](../Page/奥斯卡最佳女主角奖.md "wikilink")——[乔安妮·伍德沃特](../Page/乔安妮·伍德沃特.md "wikilink")（Joanne
    Woodward）《[三面夏娃](../Page/三面夏娃.md "wikilink")》
  - [奥斯卡最佳男配角奖](../Page/奥斯卡最佳男配角奖.md "wikilink")——[雷德·布顿斯](../Page/雷德·布顿斯.md "wikilink")（Red
    Buttons）《[樱花恋](../Page/樱花恋.md "wikilink")》
  - [奥斯卡最佳女配角奖](../Page/奥斯卡最佳女配角奖.md "wikilink")——[梅木美吉](../Page/梅木美吉.md "wikilink")（Miyoshi
    Umeki）《樱花恋》

（其他奖项参见[奥斯卡金像奖获奖名单](../Page/奥斯卡金像奖获奖名单.md "wikilink")）

## 參考文獻

[\*](../Category/1957年.md "wikilink")
[7年](../Category/1950年代.md "wikilink")
[5](../Category/20世纪各年.md "wikilink")

1.