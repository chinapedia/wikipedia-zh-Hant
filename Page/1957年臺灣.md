## 政府

  - 總統：蔣介石
  - 副總統：陳誠
  - 行政院院長：俞鴻鈞

## 大事記

### [1月](../Page/1月.md "wikilink")

  - 聯合國駐遠東軍統帥李尼茲訪台，謁見[蔣介石會談](../Page/蔣介石.md "wikilink")\[1\]。
  - [1月4日](../Page/1月4日.md "wikilink")——臺灣省政府發布「白喉防治辦法」\[2\]。

### [2月](../Page/2月.md "wikilink")

  - [2月6日](../Page/2月6日.md "wikilink")——國產[人造絲正式問世](../Page/人造絲.md "wikilink")\[3\]。

### [3月](../Page/3月.md "wikilink")

  - 蔣介石書面答覆日本記者，期望台日對付蘇共侵略，永久精誠合作\[4\]。
  - [3月2日](../Page/3月2日.md "wikilink")——中國煤礦開發公司成立\[5\]。
  - [3月22日](../Page/3月22日.md "wikilink")——蔣介石獲[希臘國王](../Page/希臘國王.md "wikilink")[保羅一世頒授](../Page/保羅一世_\(希臘\).md "wikilink")。

### [4月](../Page/4月.md "wikilink")

  - 「[中日合作策進委員會](../Page/中日合作策進委員會.md "wikilink")」在日本東京成立\[6\]。
  - [4月16日](../Page/4月16日.md "wikilink")——[國立臺灣藝術館成立](../Page/國立臺灣藝術館.md "wikilink")\[7\]。

### [5月](../Page/5月.md "wikilink")

  - [5月24日](../Page/5月24日.md "wikilink")——[劉自然案引發](../Page/劉自然.md "wikilink")[群眾抗議](../Page/五二四事件.md "wikilink")，[美使館被毁損](../Page/美國駐台北大使館.md "wikilink")，臺北衛戍司令[黃珍吾宣佈臺北市](../Page/黃珍吾.md "wikilink")、[陽明山區實施戒嚴](../Page/陽明山區.md "wikilink")\[8\]。

### [6月](../Page/6月.md "wikilink")

  - 在繼任三個月後，日本內閣總理大臣[岸信介訪問台灣](../Page/岸信介.md "wikilink")，與蔣介石伉儷商談台日有關問題\[9\]。並與蔣會談組織日華合作委員會。
  - [6月29日](../Page/6月29日.md "wikilink")——[原子能和平用途促進會成立](../Page/原子能和平用途促進會.md "wikilink")\[10\]。

### [7月](../Page/7月.md "wikilink")

  - 蔣介石接見日本議員，表示傳統革命精神，足以戰勝中共、蘇聯暴力\[11\]。

### [8月](../Page/8月.md "wikilink")

  - [8月1日](../Page/8月1日.md "wikilink")——[復興廣播電臺開播](../Page/復興廣播電臺.md "wikilink")\[12\]。

### [9月](../Page/9月.md "wikilink")

  - [9月17日](../Page/9月17日.md "wikilink")——總統[特使](../Page/特使.md "wikilink")[張群訪日](../Page/張群.md "wikilink")\[13\]。蔣中正派特使張群飛日本報聘\[14\]。

### [10月](../Page/10月.md "wikilink")

  - [10月11日](../Page/10月11日.md "wikilink")——教育部禁止採用[羅馬字](../Page/羅馬字.md "wikilink")[聖經](../Page/聖經.md "wikilink")\[15\]。
  - [10月31日](../Page/10月31日.md "wikilink")——自稱蔣介石總統特使的[王子惠以从事海外融資活动为藉口](../Page/王子惠.md "wikilink")，从兵庫县製鋼会社会计骗取7億日元，依詐欺罪受到检举。被逮捕的王子惠，此后去向不详。

### [11月](../Page/11月.md "wikilink")

  - [11月29日](../Page/11月29日.md "wikilink")——海軍成立「魚雷快艇隊」\[16\]。

### [12月](../Page/12月.md "wikilink")

  - [12月4日](../Page/12月4日.md "wikilink")——公布「自國外獲取贍養公約」\[17\]。
  - [12月9日](../Page/12月9日.md "wikilink")——縱貫線桃園─鶯歌段發生北上22次（高雄─基隆）普通列車出軌翻覆事故。事故起因為孩童於路線上嬉戲並推疊石塊所引起。共造成18人死亡(含司機員、司爐當場殉職)，116人輕重傷。事故列車本務機為[CT271蒸汽機車](../Page/台鐵CT270型蒸汽機車.md "wikilink")。此次事故也成為臺鐵日後宣導「不要在鐵路上堆疊石頭」的濫觴及案例\[18\]。

## 出生

  - [王湘琦](../Page/王湘琦.md "wikilink")，[台灣醫師小說家](../Page/台灣.md "wikilink")
  - [1月7日](../Page/1月7日.md "wikilink")——[葉匡時](../Page/葉匡時.md "wikilink")，臺灣政治人物、學者，曾任[交通部部長](../Page/中華民國交通部.md "wikilink")。
  - 2月16日——[陽帆](../Page/陽帆.md "wikilink")，臺灣男藝人
  - 4月15日——[阿吉仔](../Page/阿吉仔.md "wikilink")，本名林清吉，台語歌手。成名曲《命運的吉他》。
  - 4月26日－一[初安民](../Page/初安民.md "wikilink")，臺灣詩人、編輯人。
  - 5月1日——[楊麗環](../Page/楊麗環.md "wikilink")，政治人物。曾任[桃園縣議員](../Page/桃園縣議員.md "wikilink")、立法委員。
  - 9月2日——[蔣偉寧](../Page/蔣偉寧.md "wikilink")，政治人物、學者，曾任[教育部部長](../Page/中華民國教育部.md "wikilink")。
  - 9月28日——[陳學聖](../Page/陳學聖.md "wikilink")，政治人物。曾任記者，現任立法委員。
  - 10月27日——[蔡明亮](../Page/蔡明亮.md "wikilink")，[馬來西亞籍台灣](../Page/馬來西亞.md "wikilink")[電影](../Page/電影.md "wikilink")[導演](../Page/導演.md "wikilink")。作品有《愛情萬歲》、《[天邊一朵雲](../Page/天邊一朵雲.md "wikilink")》。
  - 10月31日——[夏曼·藍波安](../Page/夏曼·藍波安.md "wikilink")（Syaman
    Rapongan），[達悟族作家](../Page/達悟族.md "wikilink")。

## 逝世

[Category:1957年](../Category/1957年.md "wikilink")
[\*](../Category/1957年台灣.md "wikilink")
[Category:20世纪各年台湾](../Category/20世纪各年台湾.md "wikilink")

1.

2.

3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.