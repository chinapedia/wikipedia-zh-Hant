<div style="float:right; border:1px; border-style:solid; padding:2px">

**请参看：**

  - [1960年](../Page/1960年.md "wikilink")
  - [1960年电影](../Page/1960年电影.md "wikilink")
  - [1960年文学](../Page/1960年文学.md "wikilink")
  - [1960年音乐](../Page/1960年音乐.md "wikilink")
  - [1960年台灣](../Page/1960年台灣.md "wikilink")

</div>

**[1960年重要國際體育賽事包括](../Page/1960年.md "wikilink")**：

  - 首屆[欧洲足球锦标赛](../Page/1960年歐洲足球錦標賽.md "wikilink")；[7月6日至](../Page/7月6日.md "wikilink")[7月10日於](../Page/7月10日.md "wikilink")[法國舉行](../Page/法國.md "wikilink")
  - 第十七屆[夏季奧林匹克運動會](../Page/1960年夏季奧林匹克運動會.md "wikilink")；[8月25日至](../Page/8月25日.md "wikilink")[9月11日於](../Page/9月11日.md "wikilink")[意大利](../Page/意大利.md "wikilink")[羅馬舉行](../Page/羅馬.md "wikilink")
  - 首屆[夏季殘疾人奧林匹克運動會](../Page/1960年夏季残奥会.md "wikilink")；[9月18日至](../Page/9月18日.md "wikilink")[9月25日於](../Page/9月25日.md "wikilink")[意大利](../Page/意大利.md "wikilink")[羅馬舉行](../Page/羅馬.md "wikilink")

## 大事記

### 1月至3月

### 4月至6月

### 7月至9月

  - [7月6日](../Page/7月6日.md "wikilink")－首屆[欧洲足球锦标赛於](../Page/1960年欧洲足球锦标赛.md "wikilink")[法國揭幕](../Page/法國.md "wikilink")。
  - [7月10日](../Page/7月10日.md "wikilink")－[蘇聯在加時階段以二比一擊敗](../Page/蘇聯國家足球隊.md "wikilink")[南斯拉夫奪得首屆歐洲國家盃冠軍](../Page/南斯拉夫國家足球隊.md "wikilink")。
  - [8月25日](../Page/8月25日.md "wikilink")－第十七屆[夏季奧林匹克運動會於](../Page/1960年夏季奧林匹克運動會.md "wikilink")[意大利](../Page/意大利.md "wikilink")[羅馬揭幕](../Page/羅馬.md "wikilink")。
  - [9月18日](../Page/9月18日.md "wikilink")－首屆[夏季殘疾人奧林匹克運動會於](../Page/1960年夏季残奥会.md "wikilink")[意大利](../Page/意大利.md "wikilink")[羅馬揭幕](../Page/羅馬.md "wikilink")。

### 10月至12月

## 綜合運動會

  - [8月25日至](../Page/8月25日.md "wikilink")[9月11日](../Page/9月11日.md "wikilink")：**[夏季奧林匹克運動會](../Page/1960年夏季奧林匹克運動會.md "wikilink")**

獎牌榜（只列出頭5名最佳成績隊伍）

| 名次 | 國家                                                                                                                                                                                 | \!align="center" bgcolor="gold"|**金牌：** | \!align="center" bgcolor="silver"|**银牌：** | \!align="center" bgcolor="CC9966"|**铜牌：** | 總數  |
| -- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------- | ----------------------------------------- | ----------------------------------------- | --- |
| 1  | [Flag_of_the_Soviet_Union.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Soviet_Union.svg "fig:Flag_of_the_Soviet_Union.svg") [蘇聯](../Page/蘇聯.md "wikilink")              | 43                                      | 29                                        | 31                                        | 103 |
| 2  | [Flag_of_the_United_States.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_States.svg "fig:Flag_of_the_United_States.svg") [美國](../Page/美國.md "wikilink")           | 34                                      | 21                                        | 16                                        | 71  |
| 3  | [Flag_of_Italy.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg "fig:Flag_of_Italy.svg") [意大利](../Page/意大利.md "wikilink")                                               | 13                                      | 10                                        | 13                                        | 36  |
| 4  | [Flag_of_Germany-1960-Olympics.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Germany-1960-Olympics.svg "fig:Flag_of_Germany-1960-Olympics.svg") [德國](../Page/德國.md "wikilink") | 12                                      | 19                                        | 11                                        | 42  |
| 5  | [Flag_of_Australia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Australia.svg "fig:Flag_of_Australia.svg") [澳洲](../Page/澳洲.md "wikilink")                                     | 8                                       | 8                                         | 6                                         | 22  |

  - [9月18日至](../Page/9月18日.md "wikilink")[9月25日](../Page/9月25日.md "wikilink")：**[夏季殘疾人奧林匹克運動會](../Page/1960年夏季残奥会.md "wikilink")**

獎牌榜（只列出頭5名最佳成績隊伍）

| 名次 | 國家                                                                                                                                                                                     | \!align="center" bgcolor="gold"|**金牌：** | \!align="center" bgcolor="silver"|**银牌：** | \!align="center" bgcolor="CC9966"|**铜牌：** | 總數 |
| -- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------- | ----------------------------------------- | ----------------------------------------- | -- |
| 1  | [Flag_of_the_United_States.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_States.svg "fig:Flag_of_the_United_States.svg") [美國](../Page/美國.md "wikilink")               | 29                                      | 7                                         | 7                                         | 43 |
| 2  | [Flag_of_the_United_Kingdom.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_Kingdom.svg "fig:Flag_of_the_United_Kingdom.svg") [聯合王國](../Page/聯合王國.md "wikilink")        | 19                                      | 16                                        | 18                                        | 53 |
| 3  | [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg") [法國](../Page/法國.md "wikilink")                                                  | 15                                      | 1                                         | 3                                         | 19 |
| 4  | [Flag_of_Australia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Australia.svg "fig:Flag_of_Australia.svg") [澳洲](../Page/澳洲.md "wikilink")                                         | 11                                      | 4                                         | 1                                         | 16 |
| 5  | [Flag_of_Rhodesia_(1968–1979).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Rhodesia_\(1968–1979\).svg "fig:Flag_of_Rhodesia_(1968–1979).svg") [羅得西亞](../Page/羅得西亞.md "wikilink") | 10                                      | 1                                         | 2                                         | 13 |

## [籃球](../Page/籃球.md "wikilink")

## [足球](../Page/足球.md "wikilink")

  -
    <small>主條目：[1960年足球](../Page/1960年足球.md "wikilink")</small>

### 國際足球賽事

## 逝世

[\*](../Category/1960年體育.md "wikilink")