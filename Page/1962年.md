<div style="float:right; border:1px; border-style:solid; padding:2px">

**请参看：**

  - [1962年电影](../Page/1962年电影.md "wikilink")
  - [1962年文学](../Page/1962年文学.md "wikilink")
  - [1962年音乐](../Page/1962年音乐.md "wikilink")
  - [1962年体育](../Page/1962年体育.md "wikilink")
  - [1962年电视](../Page/1962年电视.md "wikilink")

</div>

## 大事记

  - [雷切尔·卡尔逊发表](../Page/雷切尔·卡尔逊.md "wikilink")《[寂静的春天](../Page/寂静的春天.md "wikilink")》，[环保运动的开始](../Page/环保运动.md "wikilink")。
  - 華語圈著名漫畫[老夫子開始連載](../Page/老夫子.md "wikilink")。

### [1月](../Page/1月.md "wikilink")

  - [1月1日](../Page/1月1日.md "wikilink")——[西萨摩亚独立](../Page/西萨摩亚.md "wikilink")。
  - [1月3日](../Page/1月3日.md "wikilink")——教皇[若望二十三世革除](../Page/若望二十三世.md "wikilink")[菲德尔·卡斯特罗的教籍](../Page/菲德尔·卡斯特罗.md "wikilink")。
  - [1月9日](../Page/1月9日.md "wikilink")——[古巴和](../Page/古巴.md "wikilink")[苏联签署贸易协议](../Page/苏联.md "wikilink")。
  - [1月22日](../Page/1月22日.md "wikilink")——[美洲国家组织革除古巴](../Page/美洲国家组织.md "wikilink")。

### [2月](../Page/2月.md "wikilink")

  - [2月7日](../Page/2月7日.md "wikilink")——[美国禁止对古巴的贸易](../Page/美国.md "wikilink")。
  - [2月9日](../Page/2月9日.md "wikilink")——[台灣證券交易所正式開業](../Page/台灣證券交易所.md "wikilink")。
  - [2月20日](../Page/2月20日.md "wikilink")——美国首次载人宇航飞行，[太空人](../Page/太空人.md "wikilink")[约翰·格伦](../Page/约翰·格伦.md "wikilink")。
  - [2月27日](../Page/2月27日.md "wikilink")——[日本首部](../Page/日本.md "wikilink")[粉紅色電影](../Page/粉紅色電影.md "wikilink")發行，正式上映。

### [3月](../Page/3月.md "wikilink")

  - [3月1日](../Page/3月1日.md "wikilink")——[乌干达自主](../Page/乌干达.md "wikilink")。
  - [3月2日](../Page/3月2日.md "wikilink")——在[缅甸发生军变](../Page/缅甸.md "wikilink")。
  - [3月5日](../Page/3月5日.md "wikilink")——[香港大會堂開幕](../Page/香港大會堂.md "wikilink")。
  - [3月18日](../Page/3月18日.md "wikilink")——[法国和](../Page/法国.md "wikilink")[阿尔及利亚在法国的](../Page/阿尔及利亚.md "wikilink")[埃维昂签订停火协议](../Page/埃维昂.md "wikilink")。
  - [3月21日](../Page/3月21日.md "wikilink")——[北欧理事会成立](../Page/北欧理事会.md "wikilink")。
  - [3月23日](../Page/3月23日.md "wikilink")——中国自行研制的[中近程火箭首次试验失败](../Page/中近程火箭.md "wikilink")。

### [5月](../Page/5月.md "wikilink")

  - [5月19日](../Page/5月19日.md "wikilink")——[玛丽莲·梦露在](../Page/玛丽莲·梦露.md "wikilink")[纽约](../Page/纽约.md "wikilink")[麦迪逊广场花园为](../Page/麦迪逊广场花园.md "wikilink")[肯尼迪总统高歌一曲](../Page/约翰·肯尼迪.md "wikilink")“生日快乐”。

### [6月](../Page/6月.md "wikilink")

  - [6月3日](../Page/6月3日.md "wikilink")——[法國航空007號班機在巴黎墜毀](../Page/法國航空007號班機.md "wikilink")，造成130人死亡。
  - [6月16日](../Page/6月16日.md "wikilink")——[彭德怀上](../Page/彭德怀.md "wikilink")[万言书](../Page/万言书.md "wikilink")。
  - [6月16日](../Page/6月16日.md "wikilink")——陸軍砲兵觀測官[施明德於小金門被捕](../Page/施明德.md "wikilink")。

### [7月](../Page/7月.md "wikilink")

  - [7月1日](../Page/7月1日.md "wikilink")——[卢旺达和](../Page/卢旺达.md "wikilink")[布隆迪独立](../Page/布隆迪.md "wikilink")。
  - [7月5日](../Page/7月5日.md "wikilink")——[阿尔及利亚独立](../Page/阿尔及利亚.md "wikilink")。
  - [7月10日](../Page/7月10日.md "wikilink")——世界上第一颗[通讯卫星](../Page/通讯卫星.md "wikilink")[電星1號进入轨道](../Page/電星1號.md "wikilink")。
  - [7月11日](../Page/7月11日.md "wikilink")——[人造衛星](../Page/人造衛星.md "wikilink")[電星1號首次向全球播放](../Page/電星1號.md "wikilink")[電視](../Page/電視.md "wikilink")。

### [8月](../Page/8月.md "wikilink")

  - [8月5日](../Page/8月5日.md "wikilink")——[南非政府將](../Page/南非.md "wikilink")[曼德拉逮捕入獄](../Page/曼德拉.md "wikilink")，曼德拉開始了他長達27年的牢獄生涯。
  - [8月6日](../Page/8月6日.md "wikilink")——[牙买加独立](../Page/牙买加.md "wikilink")。
  - [8月31日](../Page/8月31日.md "wikilink")——[特立尼达和多巴哥独立](../Page/特立尼达和多巴哥.md "wikilink")。

### [9月](../Page/9月.md "wikilink")

  - [9月1日](../Page/9月1日.md "wikilink")——[颱風溫黛正面吹襲](../Page/颱風溫黛_\(1962年\).md "wikilink")[香港](../Page/香港.md "wikilink")，造成廣泛破壞，183人死亡。
  - [9月2日](../Page/9月2日.md "wikilink")——苏联决定向古巴运送导弹。
  - [9月12日](../Page/9月12日.md "wikilink")——[约翰·F·肯尼迪宣布美国到](../Page/约翰·F·肯尼迪.md "wikilink")1960时代末将送人上[月球](../Page/月球.md "wikilink")。

### [10月](../Page/10月.md "wikilink")

  - [10月1日](../Page/10月1日.md "wikilink")——美国第一个黑人大学生入学。
  - [10月9日](../Page/10月9日.md "wikilink")——[乌干达独立](../Page/乌干达.md "wikilink")。
  - [10月10日](../Page/10月10日.md "wikilink")——由[蔣宋美齡按鈕](../Page/蔣宋美齡.md "wikilink")，[台灣電視公司正式開播](../Page/台灣電視公司.md "wikilink")，為[台灣第一家電視臺](../Page/台灣.md "wikilink")。
  - [10月11日](../Page/10月11日.md "wikilink")——[第二次梵蒂冈大公会议召开](../Page/第二次梵蒂冈大公会议.md "wikilink")。
  - [10月14日](../Page/10月14日.md "wikilink")——[古巴危机开始](../Page/古巴危机.md "wikilink")：美国发现在古巴的苏联导弹。
  - [10月20日](../Page/10月20日.md "wikilink")——[中国和](../Page/中国.md "wikilink")[印度间发生边境冲突](../Page/印度.md "wikilink")。
  - [10月22日](../Page/10月22日.md "wikilink")——[古巴导弹危机](../Page/古巴导弹危机.md "wikilink")。
  - [10月28日](../Page/10月28日.md "wikilink")——[尼基塔·谢尔盖耶维奇·赫鲁晓夫宣布撤回苏联在古巴的导弹](../Page/尼基塔·谢尔盖耶维奇·赫鲁晓夫.md "wikilink")，古巴危机结束。

### [11月](../Page/11月.md "wikilink")

  - [11月5日](../Page/11月5日.md "wikilink")——[沙特阿拉伯与](../Page/沙特阿拉伯.md "wikilink")[埃及断交](../Page/埃及.md "wikilink")。
  - [11月20日](../Page/11月20日.md "wikilink")——美国撤消对古巴的封锁。
  - [11月21日](../Page/11月21日.md "wikilink")——[中印边境冲突停火](../Page/中印边境冲突.md "wikilink")\[1\]。
  - [11月29日](../Page/11月29日.md "wikilink")——法国和[英国决定制造](../Page/英国.md "wikilink")[协和飞机](../Page/协和飞机.md "wikilink")。
  - [11月30日](../Page/11月30日.md "wikilink")——[吴丹当选为](../Page/吴丹.md "wikilink")[联合国秘书长](../Page/联合国秘书长.md "wikilink")。

## 出生

  - [1月5日](../Page/1月5日.md "wikilink")——[戚美珍](../Page/戚美珍.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [1月14日](../Page/1月14日.md "wikilink")——[賀恩德](../Page/賀恩德.md "wikilink")，[英國駐香港及澳門總領事](../Page/英國駐香港及澳門總領事.md "wikilink")
  - [1月17日](../Page/1月17日.md "wikilink")——[金·凱瑞](../Page/金·凱瑞.md "wikilink")，加拿大電影演員
  - [1月31日](../Page/1月31日.md "wikilink")——[李健仁](../Page/李健仁.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [2月5日](../Page/2月5日.md "wikilink")——[關禮傑](../Page/關禮傑.md "wikilink")，香港演員
  - [2月6日](../Page/2月6日.md "wikilink")——[埃克索爾·羅斯](../Page/埃克索爾·羅斯.md "wikilink")，美國搖滾歌手。
  - [2月8日](../Page/2月8日.md "wikilink")——[辛曉琪](../Page/辛曉琪.md "wikilink")，台灣歌手
  - [2月22日](../Page/2月22日.md "wikilink")——[史帝夫·厄文](../Page/史帝夫·厄文.md "wikilink")，[澳洲動物保育人士](../Page/澳洲.md "wikilink")（2006年逝世）
  - [3月1日](../Page/3月1日.md "wikilink")——[蔡康永](../Page/蔡康永.md "wikilink")，台灣節目主持人及作家
  - [3月2日](../Page/3月2日.md "wikilink")——[瓊·邦·喬飛](../Page/瓊·邦·喬飛.md "wikilink")，美國搖滾歌手。
  - [3月8日](../Page/3月8日.md "wikilink")——[葉童](../Page/葉童.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [3月10日](../Page/3月10日.md "wikilink")——[松田聖子](../Page/松田聖子.md "wikilink")，[日本歌手及演員](../Page/日本.md "wikilink")
  - [4月21日](../Page/4月21日.md "wikilink")——[狄鶯](../Page/狄鶯.md "wikilink")，台灣演員
  - [5月1日](../Page/5月1日.md "wikilink")——[林銀珠](../Page/林銀珠.md "wikilink")，台灣人
  - [5月14日](../Page/5月14日.md "wikilink")——[柴智屏](../Page/柴智屏.md "wikilink")
  - [5月21日](../Page/5月21日.md "wikilink")——[鄭子誠](../Page/鄭子誠.md "wikilink")，[香港男演員及唱片騎師](../Page/香港.md "wikilink")
  - [5月28日](../Page/5月28日.md "wikilink")——[盧覓雪](../Page/盧覓雪.md "wikilink")，[香港女節目主持人](../Page/香港.md "wikilink")、傳媒人
  - [6月10日](../Page/6月10日.md "wikilink")——[黃家駒](../Page/黃家駒.md "wikilink")，[香港歌手](../Page/香港.md "wikilink")（1993年6月30日逝世）
  - [6月18日](../Page/6月18日.md "wikilink")——[三澤光晴](../Page/三澤光晴.md "wikilink")，[日本男子摔角選手](../Page/日本.md "wikilink")（2009年6月13日逝世）
  - [6月22日](../Page/6月22日.md "wikilink")——[陳樂融](../Page/陳樂融.md "wikilink")，台灣音樂人、廣播主持人
  - [6月22日](../Page/6月22日.md "wikilink")——[周星馳](../Page/周星馳.md "wikilink")，[香港电影演员](../Page/香港.md "wikilink")
  - [6月27日](../Page/6月27日.md "wikilink")——[梁朝偉](../Page/梁朝偉.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [7月3日](../Page/7月3日.md "wikilink")——[湯姆·克鲁斯](../Page/湯姆·克鲁斯.md "wikilink")，美國電影演員
  - [7月19日](../Page/7月19日.md "wikilink")——[木藤亞也](../Page/木藤亞也.md "wikilink")，日本作家（1988年5月23日逝世）
  - [7月27日](../Page/7月27日.md "wikilink")——[史亞平](../Page/史亞平.md "wikilink")，台湾政治人物
  - [8月4日](../Page/8月4日.md "wikilink")——[羅傑·克萊門斯](../Page/羅傑·克萊門斯.md "wikilink")，美國棒球选手
  - [8月6日](../Page/8月6日.md "wikilink")
    ——[楊紫瓊](../Page/楊紫瓊.md "wikilink")，华人电影演员
  - [9月13日](../Page/9月13日.md "wikilink")——[麥景婷](../Page/麥景婷.md "wikilink")，香港演員
  - [9月24日](../Page/9月24日.md "wikilink")——[關之琳](../Page/關之琳.md "wikilink")，香港演員
  - [9月26日](../Page/9月26日.md "wikilink")——[吳宗憲](../Page/吳宗憲.md "wikilink")，台灣節目主持人及歌手
  - [10月11日](../Page/10月11日.md "wikilink")——[黄小娟](../Page/黄小娟.md "wikilink")，中国教师
  - [10月12日](../Page/10月12日.md "wikilink")——[陳歐珀](../Page/陳歐珀.md "wikilink")，台灣政治人物
  - [10月17日](../Page/10月17日.md "wikilink")——[游耀光](../Page/游耀光.md "wikilink")，台灣演員
  - [10月20日](../Page/10月20日.md "wikilink")——[王傑](../Page/王傑.md "wikilink")，台灣歌手
  - [11月19日](../Page/11月19日.md "wikilink")——[茱迪·科士打](../Page/茱迪·科士打.md "wikilink")，美國電影演員
  - [11月25日](../Page/11月25日.md "wikilink")——[坂口博信](../Page/坂口博信.md "wikilink")，日本遊戲制作人
  - [12月16日](../Page/12月16日.md "wikilink")——[羅嘉良](../Page/羅嘉良.md "wikilink")，[香港演員及歌手](../Page/香港.md "wikilink")
  - 生日不详——[簡偉斯](../Page/簡偉斯.md "wikilink")，臺灣影片工作者
  - 生日不详——[張棗](../Page/張棗.md "wikilink")，中國詩人

## 逝世

  - [2月9日](../Page/2月9日.md "wikilink")——[李克农](../Page/李克农.md "wikilink")，中国人民解放军情报高官（出生[1899年](../Page/1899年.md "wikilink")）
  - [2月10日](../Page/2月10日.md "wikilink")——[第一代伯基特勳爵](../Page/第一代伯基特男爵諾曼·伯基特.md "wikilink")，英國法官（出生[1883年](../Page/1883年.md "wikilink")）
  - [2月15日](../Page/2月15日.md "wikilink")——[莊長恭](../Page/莊長恭.md "wikilink")(丕可)，68歲，中華民國[中央研究院第一屆](../Page/中央研究院.md "wikilink")(數理科學組)院士。（生於[1894年](../Page/1894年.md "wikilink")）[中央研究院](https://academicians.sinica.edu.tw/index.php?func=1-D)
  - [2月24日](../Page/2月24日.md "wikilink")——[胡适](../Page/胡适.md "wikilink")
    (適之)，中国文学家，中華民國[中央研究院第一屆](../Page/中央研究院.md "wikilink")(人文及社會科學)院士。（出生[1891年](../Page/1891年.md "wikilink")）[中央研究院](https://academicians.sinica.edu.tw/index.php?func=1-D)
  - [5月19日](../Page/5月19日.md "wikilink")——[梅貽琦](../Page/梅貽琦.md "wikilink")（月涵），73歲，清華大學校長，中華民國[中央研究院第四屆](../Page/中央研究院.md "wikilink")(數理科學組)院士。（出生[1889年](../Page/1889年.md "wikilink")）[中央研究院](https://academicians.sinica.edu.tw/index.php?func=1-D)
  - [3月](../Page/3月.md "wikilink")——[李宗恩](../Page/李宗恩.md "wikilink")
    (C.U.
    Lee)，69歲，中華民國[中央研究院第一屆](../Page/中央研究院.md "wikilink")(生命科學組)院士（生於[1894年](../Page/1894年.md "wikilink"))
    [中央研究院](https://academicians.sinica.edu.tw/index.php?func=1-D)
  - [5月31日](../Page/5月31日.md "wikilink")——[阿道夫·艾希曼](../Page/阿道夫·艾希曼.md "wikilink")，纳粹军官，被处死（出生[1906年](../Page/1906年.md "wikilink")）
  - [6月27日](../Page/6月27日.md "wikilink")——[龙云](../Page/龙云.md "wikilink")，[滇军将领](../Page/滇军.md "wikilink")（出生[1884年](../Page/1884年.md "wikilink")）
  - [7月6日](../Page/7月6日.md "wikilink")——[福克納](../Page/福克納.md "wikilink")，美國小說家，曾獲「諾貝爾文學獎」（出生[1897年](../Page/1897年.md "wikilink")）[雪花新聞](https://www.xuehua.us/2018/07/06/%E7%BE%8E%E5%9B%BD%E5%B0%8F%E8%AF%B4%E5%AE%B6%E7%A6%8F%E5%85%8B%E7%BA%B3%E9%80%9D%E4%B8%96/zh-tw/)
  - [8月5日](../Page/8月5日.md "wikilink")——[玛丽莲·梦露](../Page/玛丽莲·梦露.md "wikilink")，美国电影演员（出生[1926年](../Page/1926年.md "wikilink")）
  - [8月9日](../Page/8月9日.md "wikilink")——[赫尔曼·黑塞](../Page/赫尔曼·黑塞.md "wikilink")，德国作家（出生[1877年](../Page/1877年.md "wikilink")）
  - [8月15日](../Page/8月15日.md "wikilink")——[雷锋](../Page/雷锋.md "wikilink")，中国军人（出生[1940年](../Page/1940年.md "wikilink")）
  - [9月21日](../Page/9月21日.md "wikilink")——[欧阳予倩](../Page/欧阳予倩.md "wikilink")，中国电影艺术家（出生[1889年](../Page/1889年.md "wikilink")）
  - [11月7日](../Page/11月7日.md "wikilink")——[埃莉诺·罗斯福](../Page/埃莉诺·罗斯福.md "wikilink")，美国第一夫人（出生[1884年](../Page/1884年.md "wikilink")）
  - [11月18日](../Page/11月18日.md "wikilink")——[尼尔斯·玻尔](../Page/尼尔斯·玻尔.md "wikilink")，丹麦物理学家（出生[1885年](../Page/1885年.md "wikilink")）

## 诺贝尔奖

  - [物理](../Page/诺贝尔物理学奖.md "wikilink")：[列夫·朗道](../Page/列夫·朗道.md "wikilink")
  - [化学](../Page/诺贝尔化学奖.md "wikilink")：[馬克斯·佩魯茨和](../Page/馬克斯·佩魯茨.md "wikilink")
    [約翰·肯德魯](../Page/約翰·肯德魯.md "wikilink")
  - [生理和医学](../Page/诺贝尔医学奖.md "wikilink")：[法兰西斯·哈里·坎普顿·克里克](../Page/法兰西斯·哈里·坎普顿·克里克.md "wikilink")、[詹姆斯·杜威·沃森和](../Page/詹姆斯·杜威·沃森.md "wikilink")[莫里斯·威爾金斯](../Page/莫里斯·威爾金斯.md "wikilink")
  - [文学](../Page/诺贝尔文学奖.md "wikilink")：[约翰·斯坦贝克](../Page/约翰·斯坦贝克.md "wikilink")
  - [和平](../Page/诺贝尔和平奖.md "wikilink")：[莱纳斯·鲍林](../Page/莱纳斯·鲍林.md "wikilink")

## [奥斯卡金像奖](../Page/奥斯卡金像奖.md "wikilink")

於4月8日颁发

  - [奥斯卡最佳影片奖](../Page/奥斯卡最佳影片奖.md "wikilink")——《[阿拉伯的劳伦斯](../Page/阿拉伯的劳伦斯_\(电影\).md "wikilink")》（Lawrence
    of Arabia）
  - [奥斯卡最佳导演奖](../Page/奥斯卡最佳导演奖.md "wikilink")——[大卫·里恩](../Page/大卫·里恩.md "wikilink")（David
    Lean）《[阿拉伯的劳伦斯](../Page/阿拉伯的劳伦斯_\(电影\).md "wikilink")》
  - [奥斯卡最佳男主角奖](../Page/奥斯卡最佳男主角奖.md "wikilink")——[格里高利·派克](../Page/格里高利·派克.md "wikilink")（Gregory
    Peck）《[怪屋疑雲](../Page/怪屋疑雲.md "wikilink")》（*To Kill a Mockingbird*）
  - [奥斯卡最佳女主角奖](../Page/奥斯卡最佳女主角奖.md "wikilink")——[安妮·班克劳夫特](../Page/安妮·班克劳夫特.md "wikilink")（Anne
    Bancroft）《[熱淚心聲](../Page/熱淚心聲.md "wikilink")》（*The Miracle Worker*）
  - [奥斯卡最佳男配角奖](../Page/奥斯卡最佳男配角奖.md "wikilink")——[埃德·贝格利](../Page/埃德·贝格利.md "wikilink")（Ed
    Begley）《[青春美女](../Page/青春美女.md "wikilink")》（Sweet Bird of Youth）
  - [奥斯卡最佳女配角奖](../Page/奥斯卡最佳女配角奖.md "wikilink")——[帕蒂·杜克](../Page/帕蒂·杜克.md "wikilink")（Patty
    Duke）《熱淚心聲》

## 參考文獻

[\*](../Category/1962年.md "wikilink")
[2年](../Category/1960年代.md "wikilink")
[6](../Category/20世纪各年.md "wikilink")

1.