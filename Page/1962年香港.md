## 大事記

  - 1月4日，[大角咀](../Page/大角咀.md "wikilink")[塘尾道唐樓發生恐怖大火](../Page/塘尾道.md "wikilink")，多名途人目擊2名小童攀窗呼救無果被活活燒死\[1\]，另有2人燒傷。
  - 3月，[香港大會堂竣工](../Page/香港大會堂.md "wikilink")。
  - [4月30日](../Page/4月30日.md "wikilink")，[鑽石山](../Page/鑽石山.md "wikilink")[下元嶺一間製鞋工場](../Page/下元嶺.md "wikilink")[發生大火](../Page/鑽石山鞋廠大火.md "wikilink")，唯一出口被大火堵塞，導致6死2傷。\[2\]\[3\]
  - 4月，[香港公共圖書館成立](../Page/香港公共圖書館.md "wikilink")。
  - 5月，香港發生偷渡潮。
  - [5月11日](../Page/5月11日.md "wikilink")，[小學升中試首次舉行](../Page/小學升中試.md "wikilink")，二萬六千名小學生參加。
  - [8月1日](../Page/8月1日.md "wikilink")，[長沙灣](../Page/長沙灣.md "wikilink")[元州街一幢混凝土新樓發生](../Page/元州街.md "wikilink")[重大火災](../Page/元州街唐樓大火_\(1962年\).md "wikilink")，釀成44死21傷。\[4\]\[5\]
  - [8月3日](../Page/8月3日.md "wikilink")，[后海灣蛇船與警方衝突](../Page/后海灣.md "wikilink")，2死1傷，近80名人蛇被扣留調查。
  - [8月7日](../Page/8月7日.md "wikilink")，[西貢](../Page/西貢.md "wikilink")[白沙灣發現麻包袋浮屍](../Page/白沙灣.md "wikilink")。\[6\]
  - [9月1日](../Page/9月1日.md "wikilink")，[颱風溫黛橫掃](../Page/颱風溫黛.md "wikilink")[香港](../Page/香港.md "wikilink")，[十號颶風信號歷](../Page/十號颶風信號.md "wikilink")8小時，[天文台總部錄得破紀錄的每小時](../Page/香港天文台總部.md "wikilink")133公里風速，[沙田遭受大型](../Page/沙田.md "wikilink")[風暴潮侵襲](../Page/風暴潮.md "wikilink")，漁村被毀，大型輪船沉沒，釀成183死388傷108[失蹤](../Page/失蹤.md "wikilink")。
  - [9月21日](../Page/9月21日.md "wikilink")，[馬鞍山礦埸發生二屍三命血案](../Page/馬鞍山礦埸.md "wikilink")。\[7\]
  - [11月28日](../Page/11月28日.md "wikilink")，[柴灣發生](../Page/柴灣.md "wikilink")[嚴重的蛇船海難](../Page/蛇船柴灣沉沒事故.md "wikilink")，釀成33人喪生。
  - 11月，[市政局轄下](../Page/康樂及文化事務署.md "wikilink")[香港藝術館在中環啟用](../Page/香港藝術館.md "wikilink")。
  - [12月17日](../Page/12月17日.md "wikilink")，[大嶼山發生](../Page/大嶼山.md "wikilink")[蛇船海難](../Page/蛇船大嶼山沉沒事故.md "wikilink")，造成16人死亡。

## 出生人物

  - 1月5日 [戚美珍](../Page/戚美珍.md "wikilink")
  - 1月7日 [陳秀雯](../Page/陳秀雯.md "wikilink")
  - 2月5日 [關禮傑](../Page/關禮傑.md "wikilink")
  - 2月10日 [杜德偉](../Page/杜德偉.md "wikilink")
  - 3月5日 [許長國](../Page/許長國.md "wikilink") (保齡球手)
  - 6月10日 [黃家駒](../Page/黃家駒.md "wikilink")
  - 6月16日 [黃耀明](../Page/黃耀明.md "wikilink")
  - 6月22日 [周星馳](../Page/周星馳.md "wikilink")
  - 6月27日 [梁朝偉](../Page/梁朝偉.md "wikilink")
  - 7月24日 [商天娥](../Page/商天娥.md "wikilink")
  - 8月29日 [顏福偉](../Page/顏福偉.md "wikilink")
  - 9月11日 [苑瓊丹](../Page/苑瓊丹.md "wikilink")
  - 9月24日 [關之琳](../Page/關之琳.md "wikilink")
  - 10月20日 [王傑](../Page/王傑.md "wikilink") (歌手)

<!-- end list -->

  - [陳家偉](../Page/陳家偉.md "wikilink") (區議員)
  - [陳可辛](../Page/陳可辛.md "wikilink")
  - [何超瓊](../Page/何超瓊.md "wikilink")
  - [許晉亨](../Page/許晉亨.md "wikilink")
  - [韋家輝](../Page/韋家輝.md "wikilink")
  - [胡志偉](../Page/胡志偉.md "wikilink") (區議員)
  - [蔣麗莉](../Page/蔣麗莉.md "wikilink") (企業家)
  - [顧紀筠](../Page/顧紀筠.md "wikilink")
  - [黃麗梅](../Page/黃麗梅.md "wikilink") (藝員)

## 逝世人物

  - 4月27日
    署理消防隊長[吳偉龍](../Page/吳偉龍.md "wikilink")，殉職於[第二街石灰倉大火](../Page/第二街石灰倉大火.md "wikilink")。
  - 5月20日
    消防員[周華彩](../Page/周華彩.md "wikilink")，出勤搶救海底電線火燭時於[香港仔跌出消防車外殉職](../Page/香港仔.md "wikilink")。

## 參考文獻

[Category:1962年香港](../Category/1962年香港.md "wikilink")
[Category:1960年代香港](../Category/1960年代香港.md "wikilink")
[Category:20世紀各年香港](../Category/20世紀各年香港.md "wikilink")

1.
2.
3.
4.  「元洲街大火浩劫四十三屍四十五命 \*傷者二十二人
    有九名留院」，[星島日報第二十版](../Page/星島日報.md "wikilink")
    港聞，1962年8月2日
5.  「元洲街大火慘案 昨又一傷者斃命 死者共成了四十四人」，[星島日報第十八版](../Page/星島日報.md "wikilink")
    港聞，1962年8月6日
6.  工商晚報, 1962-08-08 第1頁
7.  香港工商日報, 1962-09-22 第6頁