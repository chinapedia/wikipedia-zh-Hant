## 大事記

### 1月

  - [1月26日](../Page/1月26日.md "wikilink")——[明德銀號擠提](../Page/明德銀號.md "wikilink")。
  - [1月27日](../Page/1月27日.md "wikilink")——香港政府银行业监理处接管明德银号。
  - [1月30日](../Page/1月30日.md "wikilink")——[明德銀號因對地產放款過度宣佈破產](../Page/明德銀號.md "wikilink")。

### 2月

  - [2月6日](../Page/2月6日.md "wikilink")——[沙頭角街渡翻沉](../Page/沙頭角.md "wikilink")，1死3失蹤。
  - [2月6日](../Page/2月6日.md "wikilink")——[廣東信託銀行擠提](../Page/廣東信託銀行.md "wikilink")
  - [2月8日](../Page/2月8日.md "wikilink")——[廣東信託銀行破產](../Page/廣東信託銀行.md "wikilink")
  - [2月](../Page/2月.md "wikilink")——[恒生銀行](../Page/恒生銀行.md "wikilink")、[遠東銀行](../Page/遠東銀行.md "wikilink")、[永隆銀行](../Page/永隆銀行.md "wikilink")、[嘉華銀行](../Page/嘉華銀行.md "wikilink")、[道亨銀行](../Page/道亨銀行.md "wikilink")、[廣安銀行擠提](../Page/廣安銀行.md "wikilink")

### 3月

  - [3月1日](../Page/3月1日.md "wikilink")——東江水正式供應到香港\[1\]

### 4月

  - [4月12日](../Page/4月12日.md "wikilink")——[滙豐銀行收購](../Page/滙豐銀行.md "wikilink")[恒生銀行](../Page/恒生銀行.md "wikilink")51%股權

### 7月

  - [7月9日](../Page/7月9日.md "wikilink")，[銅鑼灣](../Page/銅鑼灣.md "wikilink")[怡華大廈發生碎屍案](../Page/怡華大廈.md "wikilink")。\[2\]
  - [7月14日](../Page/7月14日.md "wikilink")
    -[7月15日](../Page/7月15日.md "wikilink")，[超強颱風法妮黛襲港](../Page/颱風法妮黛_\(1965年\).md "wikilink")，[天文台懸掛](../Page/香港天文台.md "wikilink")[八號風球](../Page/八號烈風或暴風信號.md "wikilink")\[3\]，2死16傷\[4\]。

### 8月

  - [8月4日](../Page/8月4日.md "wikilink")——[旺角](../Page/旺角.md "wikilink")[通菜街](../Page/通菜街.md "wikilink")[新華公寓裸婦被殺](../Page/新華公寓.md "wikilink")。\[5\]
  - [8月8日](../Page/8月8日.md "wikilink")——[福榮街電焊店三級火](../Page/福榮街電焊店三級火.md "wikilink")，7死1傷\[6\]。
  - [8月24日](../Page/8月24日.md "wikilink")——[美軍運輸機墜海事故](../Page/美軍運輸機墜海事故_\(香港\).md "wikilink")，59死12傷，成為至今香港[最嚴重空難](../Page/香港航空事故列表.md "wikilink")。

### 9月

  - [9月26日](../Page/9月26日.md "wikilink")－[9月28日](../Page/9月28日.md "wikilink")，[熱帶風暴愛娜斯襲港](../Page/熱帶風暴愛娜斯_\(1965年\).md "wikilink")，[天文台懸掛](../Page/香港天文台.md "wikilink")[三號風球](../Page/三號強風信號.md "wikilink")\[7\]，5死3傷\[8\]。

### 11月

  - [11月13日](../Page/11月13日.md "wikilink")——[尖沙咀碼頭對開海面](../Page/尖沙咀.md "wikilink")，出口貨輪撞沉躉船，2死2傷。
  - [11月25日](../Page/11月25日.md "wikilink")——[遠東銀行再次擠提](../Page/遠東銀行.md "wikilink")，[匯豐銀行宣佈對該行作出無條件支持](../Page/匯豐銀行.md "wikilink")。

## 出生人物

  - [2月8日](../Page/2月8日.md "wikilink")——[張衛健](../Page/張衛健.md "wikilink")，[香港演員及歌手](../Page/香港.md "wikilink")，為[Big
    Four成員之一](../Page/Big_Four.md "wikilink")。
  - [7月28日](../Page/7月28日.md "wikilink")——[陳慧嫻](../Page/陳慧嫻.md "wikilink")，[香港歌手](../Page/香港.md "wikilink")。
  - [8月2日](../Page/8月2日.md "wikilink")——[吳君如](../Page/吳君如.md "wikilink")，[香港知名女演員](../Page/香港.md "wikilink")。
  - [8月6日](../Page/8月6日.md "wikilink")——[錢嘉樂](../Page/錢嘉樂.md "wikilink")，[香港男演員](../Page/香港.md "wikilink")。
  - [10月26日](../Page/10月26日.md "wikilink")——[郭富城](../Page/郭富城.md "wikilink")，[香港著名歌手](../Page/香港.md "wikilink")。
  - 蔡一智
  - 陳智思
  - 李賽鳳
  - 邵美琪
  - 王敏德
  - 曾華倩
  - 林保怡
  - 梁佩玲
  - 曹永廉
  - 劉細良
  - 劉嘉玲
  - 沈婉玲

## 逝世人物

  - 2月7日 [李海泉](../Page/李海泉.md "wikilink")
    著名[粵劇丑生](../Page/粵劇.md "wikilink")
  - 消防員[魏渝泉](../Page/魏渝泉.md "wikilink")，殉職於[北角消防局演習意外](../Page/北角消防局演習意外.md "wikilink")。

## 參考文獻

[1965年香港](../Category/1965年香港.md "wikilink")
[Category:20世紀各年香港](../Category/20世紀各年香港.md "wikilink")

1.  華僑日報, 1965-03-01 P.5 東江水今天供港居民將加倍負担水費

2.  香港工商日報, 1965-07-10 第5頁

3.  [熱帶氣旋警告信號資料庫](http://www.hko.gov.hk/cgi-bin/hko/warndb_c1.pl?opt=1&sgnl=91&start_ym=196501&end_ym=196512&submit=%B7j%B4M)，[香港天文台](../Page/香港天文台.md "wikilink")

4.  [一九六零年以來熱帶氣旋在香港所造成的人命傷亡及破壞](http://www.hko.gov.hk/informtc/historical_tc/cdtcc.htm)，[香港天文台](../Page/香港天文台.md "wikilink")

5.  華僑日報, 1965-08-05 第5頁

6.

7.
8.