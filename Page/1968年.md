## 大事记

[1968年夏季奥林匹克运动会的水球比赛由](../Page/1968年夏季奥林匹克运动会.md "wikilink")1968年10月14日至10月25日进行，为期11天；只设男子项目，决出金、银、铜牌各一面。本届赛事原有16支队伍参与，但由于[澳大利亚因故弃权](../Page/澳大利亚.md "wikilink")，后只余下15队竞逐。最终，由[南斯拉夫夺得金牌](../Page/南斯拉夫.md "wikilink")。

  - [1月5日](../Page/1月5日.md "wikilink")——[杜布切克当选](../Page/杜布切克.md "wikilink")[捷克斯洛伐克共产党第一书记](../Page/捷克斯洛伐克共产党.md "wikilink")，[布拉格之春运动开始](../Page/布拉格之春.md "wikilink")。
  - [1月8日](../Page/1月8日.md "wikilink")——[中华人民共和国第一艘万吨巨轮](../Page/中华人民共和国.md "wikilink")“东风”号建成。
  - [1月18日](../Page/1月18日.md "wikilink")——[穆罕默得·阿里拒绝应征入伍参加](../Page/穆罕默得·阿里.md "wikilink")[越南战争的上诉被法院驳回](../Page/越南战争.md "wikilink")。
  - [1月18日](../Page/1月18日.md "wikilink")——[日本](../Page/日本.md "wikilink")[全学联组织](../Page/全学联.md "wikilink")4.7万示威者抗议美国[企业号航空母艦前往](../Page/CVN-65.md "wikilink")[越南](../Page/越南.md "wikilink")。
  - [1月22日](../Page/1月22日.md "wikilink")——美國情報蒐集船[普韋布洛號](../Page/普韋布洛號通用環境研究艦.md "wikilink")（USS
    Pueblo
    AGER-2）在[朝鮮民主主義人民共和國外海進行作業時遭北韓方面以武力拘捕](../Page/朝鮮民主主義人民共和國.md "wikilink")，造成兩國政治緊繃，史稱「普韋布洛事件」。
  - [1月30日](../Page/1月30日.md "wikilink")——[越南民族解放阵线](../Page/越南民族解放阵线.md "wikilink")（越共）开始[春季攻势](../Page/春季攻势.md "wikilink")，[1月31日攻入南越首都](../Page/1月31日.md "wikilink")[西贡](../Page/胡志明市.md "wikilink")。
  - [1月31日](../Page/1月31日.md "wikilink")——[美國駐西貢大使館遭到越共敢死隊夜襲](../Page/美國駐西貢大使館.md "wikilink")，18名敢死隊員、[美陸軍](../Page/美國陸軍.md "wikilink")[憲兵](../Page/憲兵.md "wikilink")4人及[守館海軍陸戰隊衛兵](../Page/美國海軍陸戰隊使館警衛大隊.md "wikilink")1人在激戰一夜中死亡。
  - [2月15日](../Page/2月15日.md "wikilink")——[巴黎电影明星开始上街游行](../Page/巴黎.md "wikilink")。
  - [2月20日](../Page/2月20日.md "wikilink")——中国空间技术研究院成立，专门负责研制各类人造卫星。
  - [4月1日](../Page/4月1日.md "wikilink")——中国航天医学工程研究所成立，开始进行载人航天医学工程研究。
  - [4月3日](../Page/4月3日.md "wikilink")——[2001太空漫遊在](../Page/2001太空漫遊_\(電影\).md "wikilink")[美國上映](../Page/美國.md "wikilink")。[人工智慧](../Page/人工智慧.md "wikilink")（AI）始祖，[超級電腦](../Page/超級電腦.md "wikilink")[HAL
    9000來控制整艘](../Page/HAL_9000.md "wikilink")[太空船](../Page/太空船.md "wikilink")。
  - [4月15日](../Page/4月15日.md "wikilink")——[中国根治](../Page/中国.md "wikilink")[淮北平原涝灾的大型水利工程](../Page/淮北平原.md "wikilink")，新汴河工程开工。
  - [5月3日](../Page/5月3日.md "wikilink")——[巴黎警察封锁学生聚集的](../Page/巴黎.md "wikilink")[索邦神学院](../Page/索邦神学院.md "wikilink")，并逮捕几十名学生。
  - [5月6日](../Page/5月6日.md "wikilink")——巴黎及法国全境爆发大规模游行示威，[五月风暴定格为](../Page/五月风暴.md "wikilink")1968年的代表形象。
  - [5月10日](../Page/5月10日.md "wikilink")——[法国](../Page/法国.md "wikilink")，[巴黎](../Page/巴黎.md "wikilink")
    [街垒之夜](../Page/街垒之夜.md "wikilink")。
  - 6月——在[美國](../Page/美國.md "wikilink")[猶他州的](../Page/猶他州.md "wikilink")[羚羊泉發現在](../Page/羚羊泉.md "wikilink")[三葉蟲](../Page/三葉蟲.md "wikilink")[化石上的](../Page/化石.md "wikilink")[足印](../Page/足印.md "wikilink")（踩死[三葉蟲的](../Page/三葉蟲.md "wikilink")[足印](../Page/足印.md "wikilink")）
  - [8月18日](../Page/8月18日.md "wikilink")——[日本](../Page/日本.md "wikilink")[岐阜縣發生](../Page/岐阜縣.md "wikilink")[飛驒川巴士墜落事故](../Page/飛驒川巴士墜落事故.md "wikilink")，造成104死、3傷。
  - [8月21日](../Page/8月21日.md "wikilink")——苏联坦克进入[布拉格](../Page/布拉格.md "wikilink")，是為[布拉格之春](../Page/布拉格之春.md "wikilink")。
  - [8月24日](../Page/8月24日.md "wikilink")——[法国在南](../Page/法国.md "wikilink")[太平洋试验场爆炸了](../Page/太平洋.md "wikilink")[氢弹](../Page/氢弹.md "wikilink")，成为世界上第五个擁有[热核武器的國家](../Page/热核武器.md "wikilink")。
  - [8月25日](../Page/8月25日.md "wikilink")——台灣台東縣[紅葉少棒隊以](../Page/紅葉少棒隊.md "wikilink")7：0的懸殊比數擊敗由關西地方選拔出來的日本少棒明星隊。
  - [9月1日](../Page/9月1日.md "wikilink")——台灣企業家[张荣发创立](../Page/张荣发.md "wikilink")[长荣海运公司](../Page/长荣海运.md "wikilink")。
  - [9月3日](../Page/9月3日.md "wikilink")——[中国研制成第一批液压传动内燃机车](../Page/中国.md "wikilink")。
  - [9月9日](../Page/9月9日.md "wikilink")——[九年國民教育實施](../Page/九年國民教育.md "wikilink")，指[中華民國](../Page/中華民國.md "wikilink")[義務教育由六年延長為九年的教育措施](../Page/義務教育.md "wikilink")。
  - [9月11日](../Page/9月11日.md "wikilink")——[国际船级社协会](../Page/国际船级社协会.md "wikilink")：在[德国](../Page/德国.md "wikilink")[汉堡市成立](../Page/汉堡.md "wikilink")。
  - [9月](../Page/9月.md "wikilink")——[中華民國教育部政務次長](../Page/中華民國教育部.md "wikilink")[鄧傳楷再次獲總統親授一等](../Page/鄧傳楷.md "wikilink")（特種大綬）[景星勳章](../Page/景星勳章.md "wikilink")（[1961年首次獲頒](../Page/1961年.md "wikilink")）
  - 9月——[新疆](../Page/新疆.md "wikilink")、[西藏兩個自治區](../Page/西藏.md "wikilink")[革命委員會成立](../Page/革命委員會.md "wikilink")，至此中華人民共和國29個[省](../Page/省.md "wikilink")、[自治區](../Page/自治區.md "wikilink")、[直轄市革命委員會均已成立](../Page/直轄市.md "wikilink")，形成所謂「全國山河一片紅」。
  - [10月11日](../Page/10月11日.md "wikilink")——[美国国家航空航天局的](../Page/美国国家航空航天局.md "wikilink")[阿波罗7号成功发射](../Page/阿波罗7号.md "wikilink")。
  - [10月12日](../Page/10月12日.md "wikilink")——[第十九屆奧林匹克運動會在](../Page/1968年夏季奧林匹克運動會.md "wikilink")[墨西哥首都](../Page/墨西哥.md "wikilink")[墨西哥城開幕](../Page/墨西哥城.md "wikilink")。
  - [10月16日](../Page/10月16日.md "wikilink")——墨西哥奥运会上获胜的美国黑人运动员行握拳礼，表示对美国黑人和墨西哥学生斗争的支持，而旁邊的白人選手雖然並沒舉手，但也配戴象徵正義公平的奧運勳章表示支持。
  - [10月23日](../Page/10月23日.md "wikilink")——[日本](../Page/日本.md "wikilink")[明治百周年](../Page/明治.md "wikilink")。
  - [10月26日](../Page/10月26日.md "wikilink")——[中華民國空軍首架自製](../Page/中華民國空軍.md "wikilink")[PL-1介壽號初級教練機試飛成功](../Page/PL-1介壽號初級教練機.md "wikilink")。
  - 10月——中共八屆十二中全會通過決議，永遠開除[劉少奇的黨籍](../Page/劉少奇.md "wikilink")，撤銷其黨內外一切職務。
  - [11月5日](../Page/11月5日.md "wikilink")——[美国总统选举](../Page/1968年美国总统选举.md "wikilink")；共和党[理查德·尼克松战胜民主党](../Page/理查德·尼克松.md "wikilink")[休伯特·汉弗莱](../Page/休伯特·汉弗莱.md "wikilink")。
  - [11月11日](../Page/11月11日.md "wikilink")——[马尔代夫共和国宣告成立](../Page/马尔代夫.md "wikilink")。
  - 11月11日——[波兰统一工人党第五次代表大会在](../Page/波兰统一工人党.md "wikilink")[华沙举行](../Page/华沙.md "wikilink")。[勃列日涅夫在会上提出](../Page/勃列日涅夫.md "wikilink")“[有限主权论](../Page/有限主权论.md "wikilink")”。
  - [11月20日](../Page/11月20日.md "wikilink")——中国万吨远洋巨轮“高阳”号下水。
  - [12月6日](../Page/12月6日.md "wikilink")——[中華民國](../Page/中華民國.md "wikilink")[國防部部長](../Page/國防部.md "wikilink")[蔣經國與](../Page/蔣經國.md "wikilink")[教育部部長](../Page/教育部.md "wikilink")[閻振興同意將](../Page/閻振興.md "wikilink")[教育電視廣播電台](../Page/教育電視廣播電台.md "wikilink")（教育電視台）擴建為[中華電視台](../Page/中華電視台.md "wikilink")。\[1\]
  - [12月22日](../Page/12月22日.md "wikilink")——《[人民日报](../Page/人民日报.md "wikilink")》文章引述了[毛泽东指示](../Page/毛泽东.md "wikilink")：“知识青年到农村去，接受贫下中农的再教育，很有必要。」（[上山下乡运动](../Page/上山下乡运动.md "wikilink")）
  - [12月24日](../Page/12月24日.md "wikilink")——[美国国家航空航天局发射的](../Page/美国国家航空航天局.md "wikilink")[阿波罗8号任务进入月球轨道并环绕月球](../Page/阿波罗8号.md "wikilink")10周，是载人航天器首次离开地球轨道。
  - [12月25日](../Page/12月25日.md "wikilink")——中国[富春江大型水电站建成发电](../Page/富春江.md "wikilink")。
  - [12月28日](../Page/12月28日.md "wikilink")——中国成功进行一次新的[氢弹试验](../Page/氢弹.md "wikilink")。
  - [12月30日](../Page/12月30日.md "wikilink")——橫跨[長江的](../Page/長江.md "wikilink")[公路及](../Page/公路.md "wikilink")[鐵路兩用橋](../Page/鐵路.md "wikilink")—[中國](../Page/中國.md "wikilink")[南京長江大橋建成通車](../Page/南京長江大橋.md "wikilink")。

## 出生

  - [安明進](../Page/安明進.md "wikilink")，前北韓間諜，逃亡聲稱親眼目睹幾名被北韓綁架的日本公民，是[朝鮮綁架日本人問題的重要人證](../Page/朝鮮綁架日本人問題.md "wikilink")。
  - [1月1日](../Page/1月1日.md "wikilink")——[達沃·蘇克](../Page/達沃·蘇克.md "wikilink")，[克羅埃西亞](../Page/克羅埃西亞.md "wikilink")[足球選手](../Page/足球.md "wikilink")。
  - [1月12日](../Page/1月12日.md "wikilink")——[崔武成](../Page/崔武成.md "wikilink")，[韓國演員](../Page/韓國.md "wikilink")
  - [1月14日](../Page/1月14日.md "wikilink")——[伍佰](../Page/伍佰.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")。
  - [1月16日](../Page/1月16日.md "wikilink")——[蘇玉華](../Page/蘇玉華.md "wikilink")，[香港演員](../Page/香港.md "wikilink")。
  - [1月29日](../Page/1月29日.md "wikilink")——[曾國城](../Page/曾國城.md "wikilink")，[台灣藝人](../Page/台灣.md "wikilink")。
  - [1月30日](../Page/1月30日.md "wikilink")——[費利佩六世](../Page/費利佩六世.md "wikilink")，[西班牙現任國王](../Page/西班牙.md "wikilink")
  - [2月3日](../Page/2月3日.md "wikilink")——[弗拉德·迪瓦茨](../Page/弗拉德·迪瓦茨.md "wikilink")（Vlade
    Divac），前塞爾維亞籍[NBA球員](../Page/NBA.md "wikilink")。
  - [2月7日](../Page/2月7日.md "wikilink")——[張敏](../Page/張敏_\(演員\).md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [2月13日](../Page/2月13日.md "wikilink")——[胡凱莉](../Page/胡凱莉.md "wikilink")（Kelly
    Ann Hu），[美國華裔女演員](../Page/美國.md "wikilink")(魔蝎大帝女演員)
  - [2月17日](../Page/2月17日.md "wikilink")——[吾爾開希](../Page/吾爾開希.md "wikilink")，[六四事件中的学生领袖之一](../Page/六四.md "wikilink")。
  - [2月18日](../Page/2月18日.md "wikilink")——[陳嘉君](../Page/陳嘉君.md "wikilink")，[野百合學運中的学生领袖之一](../Page/野百合學運.md "wikilink")，[施明德妻子](../Page/施明德.md "wikilink")，人權、歷史工作者。
  - [2月22日](../Page/2月22日.md "wikilink")——[佐佐木主浩](../Page/佐佐木主浩.md "wikilink")，[日本](../Page/日本.md "wikilink")[棒球選手](../Page/棒球.md "wikilink")。
  - [3月6日](../Page/3月6日.md "wikilink")——[盧慶輝](../Page/盧慶輝.md "wikilink")，[香港演員](../Page/香港.md "wikilink")。
  - [3月17日](../Page/3月17日.md "wikilink")——[劉美珊](../Page/劉美珊.md "wikilink")，[無綫電视艺员](../Page/無綫電视.md "wikilink")
  - [3月18日](../Page/3月18日.md "wikilink")——[胡军](../Page/胡军.md "wikilink")，中国著名演員。
  - [3月18日](../Page/3月18日.md "wikilink")——[張錫銘](../Page/張錫銘.md "wikilink")，[台灣重大](../Page/台灣.md "wikilink")[通緝犯](../Page/通緝犯.md "wikilink")。
  - [3月28日](../Page/3月28日.md "wikilink")——[张纯如](../Page/张纯如.md "wikilink")，美籍华人，《蚕丝》、[《南京大屠杀：被遗忘的大浩劫》和](../Page/《南京大屠杀：被遗忘的大浩劫》.md "wikilink")《美国华人》的作者。
  - [3月30日](../Page/3月30日.md "wikilink")——[席琳·狄翁](../Page/席琳·狄翁.md "wikilink")，[加拿大歌手](../Page/加拿大.md "wikilink")。
  - [4月25日](../Page/4月25日.md "wikilink")——[謝祖武](../Page/謝祖武.md "wikilink")，[台灣男演員](../Page/台灣.md "wikilink")
  - [5月2日](../Page/5月2日.md "wikilink")——[綠川光](../Page/綠川光.md "wikilink")，[日本聲優](../Page/日本.md "wikilink")。
  - [5月28日](../Page/5月28日.md "wikilink")——[凱莉·米洛](../Page/凱莉·米洛.md "wikilink")，[澳洲歌手](../Page/澳洲.md "wikilink")。
  - [6月4日](../Page/6月4日.md "wikilink")——[劉南奎](../Page/劉南奎.md "wikilink"),前[韓國桌球名將](../Page/韓國.md "wikilink")
  - [6月26日](../Page/6月26日.md "wikilink")——[保羅·馬爾蒂尼](../Page/保羅·馬爾蒂尼.md "wikilink")，[意大利](../Page/意大利.md "wikilink")[足球選手](../Page/足球.md "wikilink")。
  - [6月27日](../Page/6月27日.md "wikilink")——[馬德鐘](../Page/馬德鐘.md "wikilink")，[香港男演員](../Page/香港.md "wikilink")。
  - [7月23日](../Page/7月23日.md "wikilink")——[蓋瑞·裴頓](../Page/蓋瑞·裴頓.md "wikilink")（Gary
    Payton），前[NBA球員](../Page/NBA.md "wikilink")。
  - [7月30日](../Page/7月30日.md "wikilink")——[馬越嘉彥](../Page/馬越嘉彥.md "wikilink")
    （Yoshihiko Umakoshi），[日本動漫人物設計師](../Page/日本.md "wikilink")
  - [8月4日](../Page/8月4日.md "wikilink")——[鄒凱光](../Page/鄒凱光.md "wikilink")，[香港編劇](../Page/香港.md "wikilink")、導演和[商業二台節目主持人](../Page/商業二台.md "wikilink")。
  - [8月5日](../Page/8月5日.md "wikilink")——[马琳·勒庞](../Page/马琳·勒庞.md "wikilink")，[法国政治人物](../Page/法国.md "wikilink")
  - [9月17日](../Page/9月17日.md "wikilink")——[翁虹](../Page/翁虹.md "wikilink")，[香港女藝員](../Page/香港.md "wikilink")。
  - 9月17日——[蒂托·比拉諾瓦](../Page/蒂托·比拉諾瓦.md "wikilink")，前西班牙加泰羅尼亞前足球中場球員。
  - [9月25日](../Page/9月25日.md "wikilink")——[約安-弗里索王子](../Page/約安-弗里索王子.md "wikilink")，荷蘭國王[威廉
    - 亞歷山大的弟弟](../Page/威廉-亞歷山大_\(荷蘭\).md "wikilink")。
  - [10月12日](../Page/10月12日.md "wikilink")——[休·傑克曼](../Page/休·傑克曼.md "wikilink")，[美國演員](../Page/美國.md "wikilink")。
  - [10月17日](../Page/10月17日.md "wikilink")——[廖敏雄](../Page/廖敏雄.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[棒球選手](../Page/棒球.md "wikilink")。
  - [11月17日](../Page/11月17日.md "wikilink")——[李彦宏](../Page/李彦宏.md "wikilink")，[中國](../Page/中國.md "wikilink")[百度創始人兼CEO](../Page/百度.md "wikilink")。
  - [12月2日](../Page/12月2日.md "wikilink")——[劉玉玲](../Page/劉玉玲.md "wikilink")，[美國華裔演員](../Page/美國.md "wikilink")。

## 逝世

  - [3月3日](../Page/3月3日.md "wikilink")——[许广平](../Page/许广平.md "wikilink")，作家（[1898年出生](../Page/1898年.md "wikilink")）
  - [3月3日](../Page/3月3日.md "wikilink")——[尤里·加加林](../Page/尤里·加加林.md "wikilink")，[蘇聯軍人](../Page/蘇聯.md "wikilink")，是第一个进入[太空的](../Page/太空.md "wikilink")[地球人](../Page/地球.md "wikilink")
  - [4月4日](../Page/4月4日.md "wikilink")——[馬丁·路德·金](../Page/馬丁·路德·金.md "wikilink")，[美國黑人民權運動領袖](../Page/美國黑人民權運動.md "wikilink")，遭暗杀（[1929年出生](../Page/1929年.md "wikilink")）
  - [5月7日](../Page/5月7日.md "wikilink")——[趙連芳](../Page/趙連芳.md "wikilink")(蘭屏_Lien-Fang
    Chao)，75歲，中華民國[中央研究院第二屆](../Page/中央研究院.md "wikilink")(生命科學組)院士（生於[1894年](../Page/1894年.md "wikilink"))
    [中央研究院](https://academicians.sinica.edu.tw/index.php?func=1-D)
  - [5月7日](../Page/5月7日.md "wikilink")——[吳景超](../Page/吳景超.md "wikilink")，67歲，中國社會學家（[1901年出生](../Page/1901年.md "wikilink")）[雪花新聞](https://www.xuehua.us/2018/05/07/%E4%B8%AD%E5%9B%BD%E7%A4%BE%E4%BC%9A%E5%AD%A6%E5%AE%B6%E5%90%B4%E6%99%AF%E8%B6%85%E9%80%9D%E4%B8%96/zh-tw/)
  - [6月1日](../Page/6月1日.md "wikilink")——“光明天使”[海伦·凯勒](../Page/海伦·凯勒.md "wikilink")，[美国女](../Page/美国.md "wikilink")[作家和](../Page/作家.md "wikilink")[教育家](../Page/教育家.md "wikilink")（[1880年出生](../Page/1880年.md "wikilink")）
  - [6月6日](../Page/6月6日.md "wikilink")——[羅伯特·弗朗西斯·甘迺迪](../Page/羅伯特·弗朗西斯·甘迺迪.md "wikilink")，[美国參議員](../Page/美国參議員.md "wikilink")，原民主黨總統參選人，遭暗殺（[1925年出生](../Page/1925年.md "wikilink")）
  - [6月30日](../Page/6月30日.md "wikilink")——[汪敬熙](../Page/汪敬熙.md "wikilink")(緝齋_Ging-Hsi
    Wang)，76歲，中華民國[中央研究院第一屆](../Page/中央研究院.md "wikilink")(生命科學組)院士（生於[1893年](../Page/1893年.md "wikilink"))
    [中央研究院](https://academicians.sinica.edu.tw/index.php?func=1-D)
  - [7月16日](../Page/7月16日.md "wikilink")——[胡先驌](../Page/胡先驌.md "wikilink")(步曾_Hsen-Hsu
    Hu)，75歲，中華民國[中央研究院第一屆](../Page/中央研究院.md "wikilink")(生命科學組)院士（生於[1894年](../Page/1894年.md "wikilink"))
    [中央研究院](https://academicians.sinica.edu.tw/index.php?func=1-D)
  - [8月20日](../Page/8月20日.md "wikilink")——[伽莫夫](../Page/伽莫夫.md "wikilink")，美籍俄裔天文学家、物理学家
  - [8月27日](../Page/8月27日.md "wikilink")——[根德公爵夫人馬里納郡主](../Page/馬里納郡主_\(根德公爵夫人\).md "wikilink")，[英國皇室成員](../Page/英國.md "wikilink")。（[1906年出生](../Page/1906年.md "wikilink")）
  - [10月16日](../Page/10月16日.md "wikilink")——[饒毓泰](../Page/饒毓泰.md "wikilink")(樹人)，77歲，([中央研究院院士](../Page/中央研究院院士.md "wikilink"))。（生於[1891年](../Page/1891年.md "wikilink")）[中央研究院](https://academicians.sinica.edu.tw/index.php?func=1-D)
  - [11月1日](../Page/11月1日.md "wikilink")——[喜饶嘉措](../Page/喜饶嘉措.md "wikilink")，[藏族大](../Page/藏族.md "wikilink")[格西](../Page/格西.md "wikilink")（（[1884年出生](../Page/1884年.md "wikilink")）
  - [12月10日](../Page/12月10日.md "wikilink")——[田汉](../Page/田汉.md "wikilink")，戏剧活动家、剧作家、诗人（[1898年出生](../Page/1898年.md "wikilink")）
  - [12月18日](../Page/12月18日.md "wikilink")——[翦伯赞](../Page/翦伯赞.md "wikilink")，中国历史学家（[1898年出生](../Page/1898年.md "wikilink")）
  - [12月27日](../Page/12月27日.md "wikilink")——[樂蒂](../Page/樂蒂.md "wikilink")，[香港國語片演員](../Page/香港.md "wikilink")（[1937年出生](../Page/1937年.md "wikilink")）
  - [12月31日](../Page/12月31日.md "wikilink")——[特里格韋·賴伊](../Page/特里格韋·賴伊.md "wikilink")，第一任[聯合國秘書長](../Page/聯合國秘書長.md "wikilink")。
  - 中国乒坛“三英”[4月16日](../Page/4月16日.md "wikilink")[傅其芳](../Page/傅其芳.md "wikilink")，[5月16日](../Page/5月16日.md "wikilink")[姜永宁](../Page/姜永宁.md "wikilink")，[6月20日](../Page/6月20日.md "wikilink")[容国团](../Page/容国团.md "wikilink")。
  - [钱海岳](../Page/钱海岳.md "wikilink")，著有《[南明史](../Page/南明史.md "wikilink")》。

## 诺贝尔奖

  - [物理](../Page/诺贝尔物理学奖.md "wikilink")：[阿尔瓦雷斯](../Page/阿尔瓦雷斯.md "wikilink")（美国），发展氢气泡室技术和数据分析，发现大量[共振态](../Page/共振态.md "wikilink")
  - [化学](../Page/诺贝尔化学奖.md "wikilink")：[拉斯·昂薩格](../Page/拉斯·昂薩格.md "wikilink")
  - [生理和医学](../Page/诺贝尔医学奖.md "wikilink")：[馬歇爾·沃倫·尼倫伯格](../Page/馬歇爾·沃倫·尼倫伯格.md "wikilink"),
    [羅伯特·W·霍利](../Page/羅伯特·W·霍利.md "wikilink"),
    [哈爾·葛賓·科拉納](../Page/哈爾·葛賓·科拉納.md "wikilink")
  - [文学](../Page/诺贝尔文学奖.md "wikilink")：[川端康成](../Page/川端康成.md "wikilink")，[日本小说家](../Page/日本.md "wikilink")，《雪国·千只鹤·古都》
  - [和平](../Page/诺贝尔和平奖.md "wikilink")：[勒內·卡森](../Page/勒內·卡森.md "wikilink")（René
    Cassin）

## [奥斯卡金像奖](../Page/奥斯卡金像奖.md "wikilink")

（第41届，[1969年颁发](../Page/1969年.md "wikilink")）

  - [奥斯卡最佳影片奖](../Page/奥斯卡最佳影片奖.md "wikilink")——《[奥利弗！](../Page/孤雛淚_\(電影\).md "wikilink")》（Oliver\!）
  - [奥斯卡最佳导演奖](../Page/奥斯卡最佳导演奖.md "wikilink")——[卡罗尔·里德](../Page/卡罗尔·里德.md "wikilink")（Carol
    Reed）《奥利弗！》
  - [奥斯卡最佳男主角奖](../Page/奥斯卡最佳男主角奖.md "wikilink")——[克利夫·罗伯逊](../Page/克利夫·罗伯逊.md "wikilink")（Cliff
    Robertson）《[畸人查利](../Page/畸人查利.md "wikilink")》
  - [奥斯卡最佳女主角奖](../Page/奥斯卡最佳女主角奖.md "wikilink")——[凯瑟琳·赫本](../Page/凯瑟琳·赫本.md "wikilink")（Katharine
    Hepburn）《[冬天的狮子](../Page/冬天的狮子.md "wikilink")》\*
  - [奥斯卡最佳女主角奖](../Page/奥斯卡最佳女主角奖.md "wikilink")——[芭芭拉·史翠珊](../Page/芭芭拉·史翠珊.md "wikilink")（Barbra
    Streisand）《[滑稽姑娘](../Page/滑稽姑娘.md "wikilink")》\*
  - [奥斯卡最佳男配角奖](../Page/奥斯卡最佳男配角奖.md "wikilink")——[杰克·艾伯森](../Page/杰克·艾伯森.md "wikilink")（Jack
    Albertson）《[主题是玫瑰](../Page/主题是玫瑰.md "wikilink")》
  - [奥斯卡最佳女配角奖](../Page/奥斯卡最佳女配角奖.md "wikilink")——[鲁思·戈登](../Page/鲁思·戈登.md "wikilink")（Ruth
    Gordon）《[魔鬼怪婴](../Page/魔鬼怪婴.md "wikilink")》

（其他奖项参见[奥斯卡金像奖获奖名单](../Page/奥斯卡金像奖获奖名单.md "wikilink")）

（注：*本年度女主角奖由两人并列获得*）

## 參考文獻

[\*](../Category/1968年.md "wikilink")
[8年](../Category/1960年代.md "wikilink")
[6](../Category/20世纪各年.md "wikilink")

1.  《華視二十年》，第6頁。