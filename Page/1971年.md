**1971年**，這裡描述的是當年度發生的各項事件。

## 大事记

### 聯合國

  - [联合国公布](../Page/联合国.md "wikilink")《[精神药物公约](../Page/精神药物公约.md "wikilink")》。
  - [10月25日](../Page/10月25日.md "wikilink")——[联合国大会2758号决议通过](../Page/联合国大会2758号决议.md "wikilink")，承认[中华人民共和国为](../Page/中华人民共和国.md "wikilink")[中国代表](../Page/中国.md "wikilink")，取代[中華民國行使在](../Page/中華民國.md "wikilink")[聯合國的一切合法權利](../Page/聯合國.md "wikilink")。

### 美國

  - 美國哈佛大學學者[約翰·羅爾斯出版的](../Page/約翰·羅爾斯.md "wikilink")《[正義論](../Page/正義論.md "wikilink")》，被[哲學界公認為二十世紀最重要的](../Page/哲學.md "wikilink")[政治哲學著作](../Page/政治哲學.md "wikilink")。
  - [IBM公司的](../Page/IBM.md "wikilink")[阿兰·舒格特发明](../Page/阿兰·舒格特.md "wikilink")[软盘](../Page/软盘.md "wikilink")。
  - [5月11日](../Page/5月11日.md "wikilink")——在[美军的炮火中](../Page/美军.md "wikilink")，[柬埔寨著名的](../Page/柬埔寨.md "wikilink")[佛教遗迹](../Page/佛教.md "wikilink")[吴哥窟被摧毁](../Page/吴哥窟.md "wikilink")。
  - [7月9日至](../Page/7月9日.md "wikilink")[7月11日](../Page/7月11日.md "wikilink")——[基辛格秘密访問中國大陸](../Page/基辛格.md "wikilink")。
  - [8月15日](../Page/8月15日.md "wikilink")——[美國宣佈停止](../Page/美國.md "wikilink")[美元與](../Page/美元.md "wikilink")[黃金的兌換](../Page/黃金.md "wikilink")，並對進口商品一律徵收百分之十的[進口附加稅](../Page/進口.md "wikilink")。

### 中國大陸

  - 1月——[中国国家卫星气象中心成立](../Page/中国国家卫星气象中心.md "wikilink")。它是[中国气象局直属科研事业单位](../Page/中国气象局.md "wikilink")。
  - [1月2日](../Page/1月2日.md "wikilink")——中国大陸宣布[轻工业形成比较完整的体系](../Page/轻工业.md "wikilink")。
  - [3月3日](../Page/3月3日.md "wikilink")——中国大陸成功发射第一颗科学实验[人造卫星](../Page/人造卫星.md "wikilink")“实践一号”。“实践一号”在预定轨道上工作了八年。
  - [3月22日](../Page/3月22日.md "wikilink")——[林彪制定](../Page/林彪.md "wikilink")[571工程](../Page/571工程.md "wikilink")。
  - [6月27日](../Page/6月27日.md "wikilink")——中国大陸第一艘两万吨货轮“长风”号下水。
  - 7月——中国大陸转发关于做好计划生育的工作报告。
  - [7月9日](../Page/7月9日.md "wikilink")——中国总理[周恩来同美国总统](../Page/周恩来.md "wikilink")[尼克松的](../Page/尼克松.md "wikilink")[总统国家安全事务助理](../Page/总统国家安全事务助理.md "wikilink")[基辛格在](../Page/基辛格.md "wikilink")[北京举行秘密会谈](../Page/北京.md "wikilink")。
  - [9月10日](../Page/9月10日.md "wikilink")——中国[洲际导弹首次飞行试验基本成功](../Page/洲际导弹.md "wikilink")。
  - [9月13日](../Page/9月13日.md "wikilink")——[九·一三事件](../Page/九·一三事件.md "wikilink")：中共中央副主席[林彪乘飞机外逃](../Page/林彪.md "wikilink")，坠机死亡于[蒙古](../Page/蒙古.md "wikilink")[温都尔汗](../Page/温都尔汗.md "wikilink")。
  - [9月27日](../Page/9月27日.md "wikilink")——中国大陸无偿援助越南36.14亿元。外援金额70亿，建国来最多的一年。
  - [10月2日](../Page/10月2日.md "wikilink")——中国大陸决定追加基本建设投资51亿人民币。
  - [11月18日](../Page/11月18日.md "wikilink")——中国新华社报道，全国建成1800多座小水泥厂。
  - [11月18日](../Page/11月18日.md "wikilink")——中国西部地区进行了一次新的核试验。
  - [11月23日](../Page/11月23日.md "wikilink")——[中华人民共和国代表团首次出席](../Page/中华人民共和国.md "wikilink")[联合国](../Page/联合国.md "wikilink")[安全理事會](../Page/安全理事會.md "wikilink")。
  - [12月7日](../Page/12月7日.md "wikilink")——中国新华社报道，全国年度水利建设50亿立方米，增加农田3000万亩。

### 台湾

  - [3月26日](../Page/3月26日.md "wikilink")——[澎湖跨海大橋通車](../Page/澎湖跨海大橋.md "wikilink")。
  - [8月14日](../Page/8月14日.md "wikilink")——[台灣](../Page/台灣.md "wikilink")[中山高速公路動工](../Page/中山高速公路.md "wikilink")。
  - [8月28日](../Page/8月28日.md "wikilink")——[台湾掀起](../Page/台湾.md "wikilink")“[保钓运动](../Page/保钓运动.md "wikilink")”。
  - [10月31日](../Page/10月31日.md "wikilink")——[中華民國第三家](../Page/中華民國.md "wikilink")[無線電視台](../Page/地面電視.md "wikilink")[中華電視台正式開播](../Page/中華電視公司.md "wikilink")。

### 香港

  - [7月7日](../Page/7月7日.md "wikilink")——[香港專上學生聯會於](../Page/香港專上學生聯會.md "wikilink")[維多利亞公園因](../Page/維多利亞公園.md "wikilink")[釣魚台事件示威](../Page/釣魚台.md "wikilink")，示威中發生流血及拘人事件。由於當時的學聯未有註冊為合法團體，是次示威未獲警方批准，警方拘捕學生。
  - [11月19日](../Page/11月19日.md "wikilink")——[麥理浩出任](../Page/麥理浩.md "wikilink")[香港總督](../Page/香港總督.md "wikilink")，是[英國管治](../Page/英國.md "wikilink")[香港以來首位外交官出身的港督](../Page/香港.md "wikilink")。

### 其他

  - [绿色和平在](../Page/绿色和平.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")[溫哥華成立](../Page/溫哥華.md "wikilink")。
  - [2月2日](../Page/2月2日.md "wikilink")──[濕地公約於](../Page/濕地公約.md "wikilink")[伊朗](../Page/伊朗.md "wikilink")[拉姆薩爾簽署](../Page/拉姆薩爾.md "wikilink")。
  - [2月7日](../Page/2月7日.md "wikilink")——在[瑞士](../Page/瑞士.md "wikilink")[妇女获得](../Page/妇女.md "wikilink")[选举权](../Page/选举权.md "wikilink")。
  - [2月15日](../Page/2月15日.md "wikilink")——英國實行新的貨幣進位制，1英鎊兌換100新便士。
  - [3月12日](../Page/3月12日.md "wikilink")——[土耳其发生](../Page/土耳其.md "wikilink")[军事政变](../Page/军事政变.md "wikilink")。
  - [4月19日](../Page/4月19日.md "wikilink")——[塞拉利昂共和国成立](../Page/塞拉利昂共和国.md "wikilink")。
  - [4月19日](../Page/4月19日.md "wikilink")——[苏联发射人类第一个](../Page/苏联.md "wikilink")[太空站](../Page/太空站.md "wikilink")“[礼炮](../Page/禮炮計劃.md "wikilink")”一号。
  - [6月28日](../Page/6月28日.md "wikilink")——有毒物质列表（Toxic Substances
    List）发布。
  - [7月21日](../Page/7月21日.md "wikilink")——[世界版权公约制定](../Page/世界版权公约.md "wikilink")。
  - [8月9日](../Page/8月9日.md "wikilink")——[印度與](../Page/印度.md "wikilink")[蘇聯簽署一個為期二十年的友好合作條約](../Page/蘇聯.md "wikilink")。
  - [8月14日](../Page/8月14日.md "wikilink")——[巴林脱离](../Page/巴林.md "wikilink")[英国独立](../Page/英国.md "wikilink")。
  - [9月3日](../Page/9月3日.md "wikilink")——[卡塔尔国成立](../Page/卡塔尔.md "wikilink")。
  - [9月21日](../Page/9月21日.md "wikilink")——[巴基斯坦宣佈進入](../Page/巴基斯坦.md "wikilink")[緊急狀態](../Page/緊急狀態.md "wikilink")。
  - [10月27日](../Page/10月27日.md "wikilink")——[剛果民主共和國改名為薩伊](../Page/剛果民主共和國.md "wikilink")。
  - [10月28日](../Page/10月28日.md "wikilink")——[英國下議院以](../Page/英國下議院.md "wikilink")356票對244票通過加入[歐洲經濟共同體](../Page/歐洲共同體.md "wikilink")。
  - [11月11日](../Page/11月11日.md "wikilink")——[日本](../Page/日本.md "wikilink")[神奈川縣發生](../Page/神奈川縣.md "wikilink")[川崎壤土邊坡破壞實驗事故](../Page/川崎壤土邊坡破壞實驗事故.md "wikilink")，造成15人死亡、10人受傷。
  - [11月15日](../Page/11月15日.md "wikilink")——[英特尔公司](../Page/英特尔.md "wikilink")，决定在《》杂志上刊登一则广告，向全世界公布[4004](../Page/4004.md "wikilink")[微处理器](../Page/微处理器.md "wikilink")。这一天，也演变为[微处理器的](../Page/微处理器.md "wikilink")[诞生纪念日](../Page/生日.md "wikilink")。
  - 1971年印巴战争，也被称为[第三次印巴战争](../Page/第三次印巴战争.md "wikilink")，是1971年12月印度与巴基斯坦两国之间的战争。该战争与孟加拉国独立战争密切相关。
  - [12月2日](../Page/12月2日.md "wikilink")——[阿拉伯联合酋长国成立](../Page/阿拉伯联合酋长国.md "wikilink")。
  - [12月2日](../Page/12月2日.md "wikilink")——前苏联[火星-3号探测器首次在](../Page/火星-3号探测器.md "wikilink")[火星表面软着陆成功](../Page/火星.md "wikilink")。
  - [12月25日](../Page/12月25日.md "wikilink")——[韓國](../Page/韓國.md "wikilink")[首爾發生一起](../Page/首爾.md "wikilink")22層樓的[大然閣飯店火災事故](../Page/大然閣飯店火災事故.md "wikilink")
  - [12月29日](../Page/12月29日.md "wikilink")——[英國放棄在](../Page/英國.md "wikilink")[馬爾他的軍事基地](../Page/馬爾他.md "wikilink")。

## 出生

### 1月

  - [1月2日](../Page/1月2日.md "wikilink")——[竹野內豐](../Page/竹野內豐.md "wikilink")，日本藝人
  - [1月10日](../Page/1月10日.md "wikilink")——[郭碧琪](../Page/郭碧琪.md "wikilink")，香港攝影師及企業家
  - [1月20日](../Page/1月20日.md "wikilink")——[蓋瑞·巴洛](../Page/蓋瑞·巴洛.md "wikilink"),
    英國歌手, [接招合唱團的成員](../Page/接招.md "wikilink")
  - [1月20日](../Page/1月20日.md "wikilink")——[鄭雄仁](../Page/鄭雄仁.md "wikilink")，[韓國演員](../Page/韓國.md "wikilink")
  - [1月30日](../Page/1月30日.md "wikilink")——[李瑞鎮](../Page/李瑞鎮.md "wikilink")，[韓國演員](../Page/韓國.md "wikilink")

### 2月

  - [2月14日](../Page/2月14日.md "wikilink")——[胡兵](../Page/胡兵.md "wikilink")，[中國藝人](../Page/中國.md "wikilink")

### 3月

  - [3月10日](../Page/3月10日.md "wikilink")——[藤崎龍](../Page/藤崎龍.md "wikilink")，日本[漫畫家與](../Page/漫畫家.md "wikilink")[插畫家](../Page/插畫家.md "wikilink")
  - [3月21日](../Page/3月21日.md "wikilink")——[陳宣裕](../Page/陳宣裕.md "wikilink")，台灣[搞笑藝人](../Page/搞笑藝人.md "wikilink")

### 4月

  - [4月16日](../Page/4月16日.md "wikilink")——[陳豪](../Page/陳豪.md "wikilink")，香港演員

### 5月

  - [5月10日](../Page/5月10日.md "wikilink")——[金正男](../Page/金正男.md "wikilink")，[朝鲜民主主义人民共和国领导人](../Page/朝鲜民主主义人民共和国.md "wikilink")[金正日的長子](../Page/金正日.md "wikilink")（逝於[2017年](../Page/2017年.md "wikilink")）
  - [5月11日](../Page/5月11日.md "wikilink")——[杨钰莹](../Page/杨钰莹.md "wikilink")，[中国大陆女歌手](../Page/中国大陆.md "wikilink")
  - [5月14日](../Page/5月14日.md "wikilink")——[蘇見信](../Page/蘇見信.md "wikilink")，[台灣男藝人](../Page/台灣.md "wikilink")

### 6月

  - [6月4日](../Page/6月4日.md "wikilink")——[约瑟夫·卡比拉](../Page/约瑟夫·卡比拉.md "wikilink")，[剛果民主共和國](../Page/剛果民主共和國.md "wikilink")[總統](../Page/總統.md "wikilink")
  - [6月5日](../Page/6月5日.md "wikilink")——[馬克·華伯格](../Page/馬克·華伯格.md "wikilink")，[美國](../Page/美國.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [6月24日](../Page/6月24日.md "wikilink")——[池珍熙](../Page/池珍熙.md "wikilink")，[韓國藝人](../Page/韓國.md "wikilink")
  - [6月27日](../Page/6月27日.md "wikilink")——[狄潘德拉](../Page/狄潘德拉.md "wikilink")，[尼泊爾第](../Page/尼泊爾.md "wikilink")12任國王（逝於[2001年](../Page/2001年.md "wikilink")）
  - [6月28日](../Page/6月28日.md "wikilink")——[藤原紀香](../Page/藤原紀香.md "wikilink")，日本藝人
  - [6月29日](../Page/6月29日.md "wikilink")——[汪峰](../Page/汪峰.md "wikilink")，中国大陆艺人

### 7月

  - [7月3日](../Page/7月3日.md "wikilink")——[朱利安·阿桑奇](../Page/朱利安·阿桑奇.md "wikilink")，[維基解密創辦人兼發言人](../Page/維基解密.md "wikilink")。
  - [7月28日](../Page/7月28日.md "wikilink")——[阿布·贝克尔·巴格达迪](../Page/阿布·贝克尔·巴格达迪.md "wikilink")，[伊斯兰国](../Page/伊斯兰国.md "wikilink")[恐怖主义组织领袖](../Page/恐怖主义.md "wikilink")

### 8月

  - [8月12日](../Page/8月12日.md "wikilink")——[皮特·山普拉斯](../Page/皮特·山普拉斯.md "wikilink")，[美國網球運動員](../Page/美國.md "wikilink")。[鄧惠文](../Page/鄧惠文.md "wikilink")，[台灣心理醫師](../Page/台灣.md "wikilink")
  - [8月14日](../Page/8月14日.md "wikilink")——[歐偉倫](../Page/歐偉倫.md "wikilink")，[香港足球運動員](../Page/香港.md "wikilink")
  - [8月17日](../Page/8月17日.md "wikilink")——[嚴正花](../Page/嚴正花.md "wikilink")，[韓國歌手](../Page/韓國.md "wikilink")
  - [8月27日](../Page/8月27日.md "wikilink")——[張智霖](../Page/張智霖.md "wikilink")，[香港演員及歌手](../Page/香港.md "wikilink")

### 9月

  - [9月4日](../Page/9月4日.md "wikilink")——[袁詠儀](../Page/袁詠儀.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [9月8日](../Page/9月8日.md "wikilink")——[盛本真理子](../Page/盛本真理子.md "wikilink")，[日本女演員](../Page/日本.md "wikilink")
  - [9月26日](../Page/9月26日.md "wikilink")——[韩红](../Page/韩红.md "wikilink")，[中国大陆女歌手兼空军军官](../Page/中国大陆.md "wikilink")
  - [9月30日](../Page/9月30日.md "wikilink")——[張文慈](../Page/張文慈.md "wikilink")，[香港女演員](../Page/香港.md "wikilink")

### 10月

  - [10月1日](../Page/10月1日.md "wikilink")——[黎姿](../Page/黎姿.md "wikilink")，香港演員。[宋一國](../Page/宋一國.md "wikilink")，[韓國藝人](../Page/韓國.md "wikilink")
  - [10月2日](../Page/10月2日.md "wikilink")——[呂宇俊](../Page/呂宇俊.md "wikilink")，[香港十大傑出青年之一](../Page/香港十大傑出青年.md "wikilink")
  - [10月4日](../Page/10月4日.md "wikilink")——[湯寶如](../Page/湯寶如.md "wikilink")，[香港歌手](../Page/香港.md "wikilink")
  - [10月25日](../Page/10月25日.md "wikilink")——[朱茵](../Page/朱茵.md "wikilink")，[香港藝人](../Page/香港.md "wikilink")
  - [10月28日](../Page/10月28日.md "wikilink")——[陳建寧](../Page/陳建寧.md "wikilink")，[台灣知名唱片製作人](../Page/台灣.md "wikilink")，詞曲創作人

### 11月

  - [11月5日](../Page/11月5日.md "wikilink")——[梁漢文](../Page/梁漢文.md "wikilink")，[香港演員及歌手](../Page/香港.md "wikilink")，為[Big
    Four成員之一](../Page/Big_Four.md "wikilink")

### 12月

  - [12月6日](../Page/12月6日.md "wikilink")——[黄磊](../Page/黄磊.md "wikilink")，中国内地艺人
  - [12月27日](../Page/12月27日.md "wikilink")——[伍樂城](../Page/伍樂城.md "wikilink")，[香港音樂創作人](../Page/香港.md "wikilink")

## 逝世

  - [逝世公告](../Page/逝世公告.md "wikilink")
  - [1月29日](../Page/1月29日.md "wikilink")——[翁文灝](../Page/翁文灝.md "wikilink")（詠霓），82歲，([中央研究院院士](../Page/中央研究院院士.md "wikilink"))。（生於[1889年](../Page/1889年.md "wikilink")）[中央研究院](https://academicians.sinica.edu.tw/index.php?func=1-D)
  - [2月23日](../Page/2月23日.md "wikilink")——[杜高智](../Page/杜高智.md "wikilink")，42歲，[南越陸軍中將](../Page/南越陸軍.md "wikilink")、[越南戰爭戰場指揮官](../Page/越南戰爭.md "wikilink")。（生於[1929年](../Page/1929年.md "wikilink")）
  - [4月17日](../Page/4月17日.md "wikilink")——[林子豐](../Page/林子豐.md "wikilink")，79歲，[香港](../Page/香港.md "wikilink")[教育家及銀行家](../Page/教育家.md "wikilink")。（生於[1892年](../Page/1892年.md "wikilink")）
  - [4月20日](../Page/4月20日.md "wikilink")——[周鯁生](../Page/周鯁生.md "wikilink")
    (覽)，82歲，([中央研究院院士](../Page/中央研究院院士.md "wikilink"))。（生於[1889年](../Page/1889年.md "wikilink")）[中央研究院](https://academicians.sinica.edu.tw/index.php?func=1-D)
  - [4月29日](../Page/4月29日.md "wikilink")——[李四光](../Page/李四光.md "wikilink")(仲揆)，82歲，([中央研究院院士](../Page/中央研究院院士.md "wikilink"))，中國科學家。（生於[1889年](../Page/1889年.md "wikilink")）[中央研究院](https://academicians.sinica.edu.tw/index.php?func=1-D)
  - [6月21日](../Page/6月21日.md "wikilink")——[陳垣](../Page/陳垣.md "wikilink")
    (援庵)，91歲，([中央研究院院士](../Page/中央研究院院士.md "wikilink"))。（生於[1880年](../Page/1880年.md "wikilink")）[中央研究院](https://academicians.sinica.edu.tw/index.php?func=1-D)
  - [7月3日](../Page/7月3日.md "wikilink")——[吉姆
    莫里森](../Page/吉姆_莫里森.md "wikilink")，27歲，美國創作歌手和詩人。（生於[1943年](../Page/1943年.md "wikilink")）
  - [7月6日](../Page/7月6日.md "wikilink")——[路易斯·阿姆斯特朗](../Page/路易斯·阿姆斯特朗.md "wikilink")，70歲，美国爵士音乐家。（生於[1901年](../Page/1901年.md "wikilink")）
  - [9月11日](../Page/9月11日.md "wikilink")——[赫鲁晓夫](../Page/赫鲁晓夫.md "wikilink")，77歲，前[苏联共产党中央第一书记](../Page/苏联共产党.md "wikilink")、部长会议主席。（生於[1894年](../Page/1894年.md "wikilink")）
  - [9月13日](../Page/9月13日.md "wikilink")——[林彪](../Page/林彪.md "wikilink")，64歲，[中國人民解放軍元帥](../Page/中國人民解放軍.md "wikilink")，[中共中央副主席](../Page/中共中央.md "wikilink")，國防部長。（生於[1907年](../Page/1907年.md "wikilink")）
  - [9月13日](../Page/9月13日.md "wikilink")——[葉群](../Page/葉群.md "wikilink")，54歲，[林彪妻子](../Page/林彪.md "wikilink")，[中共中央政治局委員](../Page/中共中央政治局委員.md "wikilink")。（生於[1917年](../Page/1917年.md "wikilink")）
  - [9月13日](../Page/9月13日.md "wikilink")——[林立果](../Page/林立果.md "wikilink")，26歲，[林彪之子](../Page/林彪.md "wikilink")，空軍司令部辦公室副主任兼作戰部副部長。（生於[1945年](../Page/1945年.md "wikilink")）
  - [12月4日](../Page/12月4日.md "wikilink")——[賈乃錫](../Page/賈乃錫.md "wikilink")，83歲，英國陸軍將領（生於[1888年](../Page/1888年.md "wikilink")）

## 诺贝尔奖

  - [物理](../Page/诺贝尔物理学奖.md "wikilink")：[伽博·丹尼斯](../Page/伽博·丹尼斯.md "wikilink")（Gabor
    Dennis）
  - [化学](../Page/诺贝尔化学奖.md "wikilink")：[格哈德·赫茨貝格](../Page/格哈德·赫茨貝格.md "wikilink")
    (Gerhard Herzberg)
  - [生理和医学](../Page/诺贝尔医学奖.md "wikilink")：[厄爾·威爾伯·薩瑟蘭](../Page/厄爾·威爾伯·薩瑟蘭.md "wikilink")（Earl
    W. Sutherland, Jr.）
  - [文学](../Page/诺贝尔文学奖.md "wikilink")：[巴勃罗·聂鲁达](../Page/巴勃罗·聂鲁达.md "wikilink")（Pablo
    Neruda）
  - [和平](../Page/诺贝尔和平奖.md "wikilink")：[威利·勃兰特](../Page/威利·勃兰特.md "wikilink")
    (Willy Brandt)
  - [经济](../Page/诺贝尔经济学奖.md "wikilink")：[西蒙·庫茲涅茨](../Page/西蒙·庫茲涅茨.md "wikilink")（Simon
    Kuznets）

## [奥斯卡金像奖](../Page/奥斯卡金像奖.md "wikilink")

（第44届，[1972年颁发](../Page/1972年.md "wikilink")）

  - [奥斯卡最佳影片奖](../Page/奥斯卡最佳影片奖.md "wikilink")——《[法国贩毒网](../Page/法国贩毒网.md "wikilink")》（The
    French Connection）
  - [奥斯卡最佳导演奖](../Page/奥斯卡最佳导演奖.md "wikilink")——[威廉·弗里德金](../Page/威廉·弗里德金.md "wikilink")（William
    Friedkin）《法国贩毒网》
  - [奥斯卡最佳男主角奖](../Page/奥斯卡最佳男主角奖.md "wikilink")——[吉恩·海克曼](../Page/吉恩·海克曼.md "wikilink")（Gene
    Hackman）《法国贩毒网》
  - [奥斯卡最佳女主角奖](../Page/奥斯卡最佳女主角奖.md "wikilink")——[简·方达](../Page/简·方达.md "wikilink")（Jane
    Fonda）《[花街杀人案](../Page/花街杀人案.md "wikilink")》
  - [奥斯卡最佳男配角奖](../Page/奥斯卡最佳男配角奖.md "wikilink")——[本·约翰逊](../Page/本·约翰逊.md "wikilink")（Ben
    Johnson）《[最后一场电影](../Page/最后一场电影.md "wikilink")》
  - [奥斯卡最佳女配角奖](../Page/奥斯卡最佳女配角奖.md "wikilink")——[克洛里斯·李奇曼](../Page/克洛里斯·李奇曼.md "wikilink")（Cloris
    Leachman）《最后一场电影》

（其他奖项参见[奥斯卡金像奖获奖名单](../Page/奥斯卡金像奖获奖名单.md "wikilink")）

## 参考

[\*](../Category/1971年.md "wikilink")
[1年](../Category/1970年代.md "wikilink")
[7](../Category/20世纪各年.md "wikilink")