## 大事記

  - [1月5日](../Page/1月5日.md "wikilink")，[中文成為法定語文](../Page/中文.md "wikilink")，法律效力與地位與[英文相同](../Page/英文.md "wikilink")。\[1\]
  - [2月15日](../Page/2月15日.md "wikilink")，[廉政公署成立](../Page/廉政公署_\(香港\).md "wikilink")。
  - [3月3日](../Page/3月3日.md "wikilink")，一輛貨車在[清水灣道](../Page/清水灣道.md "wikilink")[德望學校附近](../Page/德望學校.md "wikilink")[失控撞山](../Page/清水灣道貨車撞山事故.md "wikilink")，25死2傷。\[2\]
  - [4月2日](../Page/4月2日.md "wikilink")，香港[消費者委員會成立](../Page/消費者委員會.md "wikilink")。
  - [5月24日](../Page/5月24日.md "wikilink")，獨行賊挾持[寶生銀行人質事件](../Page/寶生銀行.md "wikilink")。\[3\]
  - [5月30日](../Page/5月30日.md "wikilink")，香港撤銷[燈火管制](../Page/燈火管制.md "wikilink")。
  - [5月30日](../Page/5月30日.md "wikilink")，6名學生在[鳳凰山準備觀賞](../Page/鳳凰山_\(香港\).md "wikilink")[日出之際](../Page/日出.md "wikilink")[遭雷擊](../Page/學生鳳凰山遭雷擊事件.md "wikilink")，3死3傷。\[4\]
  - [6月18日](../Page/6月18日.md "wikilink")，[沙田](../Page/沙田.md "wikilink")[小瀝源建築中地下引水道遭](../Page/小瀝源.md "wikilink")[雷擊](../Page/雷擊.md "wikilink")，1死4傷。
  - [7月23日](../Page/7月23日.md "wikilink")，[牛頭角下邨第](../Page/牛頭角下邨.md "wikilink")9座1227室命案。\[5\]
  - [8月13日](../Page/8月13日.md "wikilink")，[灣仔道](../Page/灣仔道.md "wikilink")234至236號鴻業大廈雙屍兇殺案。\[6\]
  - [8月15日](../Page/8月15日.md "wikilink")，[旺角長城公寓發生謀殺案](../Page/旺角.md "wikilink")。\[7\]
  - [9月12日](../Page/9月12日.md "wikilink")，[新蒲崗大有街及爵祿街一帶發生騷亂](../Page/新蒲崗.md "wikilink")，警方拘捕包括[岑建勳在內的](../Page/岑建勳.md "wikilink")5人。
  - [9月13日](../Page/9月13日.md "wikilink")，[運輸署為免白牌車冒充的士](../Page/運輸署.md "wikilink")，規定市區的士車身顏色，上半部為銀色，下半部為紅色，12個月內車主須按照賛定更改車身顏色。\[8\]
  - [9月20日](../Page/9月20日.md "wikilink")，[上水](../Page/上水.md "wikilink")[宣道圍印度河畔男子被殺](../Page/宣道圍.md "wikilink")。\[9\]
  - [10月19日](../Page/10月19日.md "wikilink")，[颱風嘉曼襲港](../Page/颱風嘉曼.md "wikilink")，帶來連場暴雨，[天文台懸掛](../Page/香港天文台.md "wikilink")[九號風球](../Page/九號烈風或暴風增強信號.md "wikilink")，1人喪生。
  - [10月25日](../Page/10月25日.md "wikilink")，[紅磡](../Page/紅磡.md "wikilink")[曲街殺人縱火命案](../Page/曲街.md "wikilink")。\[10\]
  - [11月3日](../Page/11月3日.md "wikilink")，[樂富邨](../Page/樂富邨.md "wikilink")[水塘山公園劫殺案](../Page/水塘山公園.md "wikilink")。\[11\]
  - [12月17日](../Page/12月17日.md "wikilink")，[跑馬地紙盒藏屍案](../Page/跑馬地紙盒藏屍案.md "wikilink")。

## 出生人物

  - [1月18日](../Page/1月18日.md "wikilink")，[唐劍康](../Page/唐劍康.md "wikilink")，[香港](../Page/香港.md "wikilink")[唱片騎師](../Page/唱片騎師.md "wikilink")。
  - [2月3日](../Page/2月3日.md "wikilink")，[楊千嬅](../Page/楊千嬅.md "wikilink")，[香港女歌手及](../Page/香港.md "wikilink")[電影演員](../Page/電影.md "wikilink")。
  - 2月3日，[郭偉亮](../Page/郭偉亮.md "wikilink")，[香港男歌手](../Page/香港.md "wikilink")。
  - [2月13日](../Page/2月13日.md "wikilink")，[馬國明](../Page/馬國明.md "wikilink")，[香港演員](../Page/香港.md "wikilink")。
  - [5月6日](../Page/5月6日.md "wikilink")，[鄧健泓](../Page/鄧健泓.md "wikilink")，[香港演員及歌手](../Page/香港.md "wikilink")。
  - [5月20日](../Page/5月20日.md "wikilink")，[周汶錡](../Page/周汶錡.md "wikilink")，[香港模特兒](../Page/香港.md "wikilink")。
  - [5月26日](../Page/5月26日.md "wikilink")，[陳琪](../Page/陳琪.md "wikilink")，[香港女藝員](../Page/香港.md "wikilink")。
  - [6月1日](../Page/6月1日.md "wikilink")，[陳卓智](../Page/陳卓智.md "wikilink")，[香港男性配音員](../Page/香港.md "wikilink")。
  - [6月5日](../Page/6月5日.md "wikilink")，[葉文輝](../Page/葉文輝.md "wikilink")，[香港](../Page/香港.md "wikilink")[唱片騎師及男歌手](../Page/唱片騎師.md "wikilink")。
  - [7月1日](../Page/7月1日.md "wikilink")，[洪天明](../Page/洪天明.md "wikilink")，[香港演員](../Page/香港.md "wikilink")、電視節目主持。
  - [7月22日](../Page/7月22日.md "wikilink")，[郭羨妮](../Page/郭羨妮.md "wikilink")，[香港女藝員](../Page/香港.md "wikilink")。
  - [7月27日](../Page/7月27日.md "wikilink")，[陳奕迅](../Page/陳奕迅.md "wikilink")，[香港男歌手及演員](../Page/香港.md "wikilink")。
  - [8月7日](../Page/8月7日.md "wikilink")，[徐榮](../Page/徐榮_\(演員\).md "wikilink")，[香港演員](../Page/香港.md "wikilink")。
  - [8月9日](../Page/8月9日.md "wikilink")，[馮德倫](../Page/馮德倫.md "wikilink")，[香港導演及演員](../Page/香港.md "wikilink")。
  - [9月30日](../Page/9月30日.md "wikilink")，[吳彥祖](../Page/吳彥祖.md "wikilink")，[香港](../Page/香港.md "wikilink")[電影演員](../Page/電影.md "wikilink")。
  - [11月30日](../Page/11月30日.md "wikilink")，[鍾漢良](../Page/鍾漢良.md "wikilink")，[香港男歌手及演員](../Page/香港.md "wikilink")。
  - [12月28日](../Page/12月28日.md "wikilink")，[車婉婉](../Page/車婉婉.md "wikilink")，[香港演員及歌手](../Page/香港.md "wikilink")。

## 逝世人物

  - [7月24日](../Page/7月24日.md "wikilink")，[白小曼](../Page/白小曼.md "wikilink")，18歲，[邵氏女星](../Page/邵氏.md "wikilink")。

## 參考文獻

[Category:1974年香港](../Category/1974年香港.md "wikilink")
[Category:20世紀各年香港](../Category/20世紀各年香港.md "wikilink")
[Category:1974年](../Category/1974年.md "wikilink")

1.
2.
3.
4.
5.  香港工商日報, 1974-07-24 第8頁
6.  工商晚報, 1974-08-14 第8頁
7.  香港工商日報, 1974-08-16 第7頁
8.  香港工商日報, 1974-09-14
9.  香港工商日報, 1974-09-21 第8頁
10. 香港工商日報, 1974-10-26 第8頁
11. 香港工商日報, 1974-11-04 第8頁