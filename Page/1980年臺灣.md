## 政治

  - 總統：蔣經國
  - 副總統：謝東閔
  - 行政院院長：孫運璿

## 大事記

  - [黃朝湖主持的](../Page/黃朝湖.md "wikilink")[臺中縣第一畫廊開幕](../Page/臺中縣第一畫廊.md "wikilink")。
  - [1972年](../Page/1972年.md "wikilink")[5月創立的](../Page/5月.md "wikilink")[長流畫會解散](../Page/長流畫會.md "wikilink")。

### 1月至3月

<table>
<tbody>
<tr class="odd">
<td><div style="position: relative; font-size: smaller; font-weight: bold;">
<p>{{Image label|x=1|y=28|text=[[張俊宏| {{Image label|x=45|y=55|text=[[黃信介| {{Image label|x=152|y=40|text=[[陳菊| {{Image label|x=185|y=26|text=[[姚嘉文| {{Image label|x=267|y=22|text=[[施明德| {{Image label|x=342|y=32|text=[[呂秀蓮| {{Image label|x=430|y=29|text=[[林弘宣|</p>
</div></td>
</tr>
<tr class="even">
<td><center>
<p>美麗島大審的關係人物</p></td>
</tr>
</tbody>
</table>

  - [1月8日](../Page/1月8日.md "wikilink")——[美麗島事件總指揮](../Page/美麗島事件.md "wikilink")[施明德逃亡](../Page/施明德.md "wikilink")26日後，於[臺北市武昌街被捕](../Page/臺北市.md "wikilink")。
  - [2月1日](../Page/2月1日.md "wikilink")——[北迴鐵路全線通車](../Page/北迴鐵路.md "wikilink")。
  - [2月9日](../Page/2月9日.md "wikilink")——[哥倫比亞與](../Page/哥倫比亞.md "wikilink")[中華民國斷交](../Page/中華民國.md "wikilink")。\[1\]
  - [2月28日](../Page/2月28日.md "wikilink")——[林義雄家發生](../Page/林義雄.md "wikilink")[滅門血案](../Page/林宅血案.md "wikilink")，其母親及兩名雙胞胎女兒遇害身亡，大女兒身受重傷。
  - [3月18日](../Page/3月18日.md "wikilink")——展開為期9天的[美麗島大審](../Page/美麗島大審.md "wikilink")。
  - [3月27日](../Page/3月27日.md "wikilink")——[施明德於](../Page/施明德.md "wikilink")[美麗島事件](../Page/美麗島事件.md "wikilink")[軍法大審的辯論庭上](../Page/美麗島大審.md "wikilink")，公開主張「中華民國模式的臺灣獨立」。
  - [3月31日](../Page/3月31日.md "wikilink")——位於[臺北市](../Page/臺北市.md "wikilink")[中正區的](../Page/中正區_\(臺北市\).md "wikilink")[中正紀念堂完工](../Page/中正紀念堂.md "wikilink")，並於同年4月5日對外開放，為[臺北市著名地標之一](../Page/臺北市.md "wikilink")。

### 4月至6月

  - [4月18日](../Page/4月18日.md "wikilink")——[警備總部](../Page/警備總部.md "wikilink")[軍事法庭對](../Page/軍事法庭.md "wikilink")[美麗島事件判決](../Page/美麗島事件.md "wikilink")：[施明德處](../Page/施明德.md "wikilink")[無期徒刑](../Page/無期徒刑.md "wikilink")，[姚嘉文](../Page/姚嘉文.md "wikilink")、[張俊宏](../Page/張俊宏.md "wikilink")、[林弘宣](../Page/林弘宣.md "wikilink")、[呂秀蓮](../Page/呂秀蓮.md "wikilink")、[陳菊等各](../Page/陳菊.md "wikilink")12年[有期徒刑](../Page/有期徒刑.md "wikilink")。
  - [5月4日](../Page/5月4日.md "wikilink")——[中華民國與](../Page/中華民國.md "wikilink")[諾魯建交](../Page/諾魯.md "wikilink")。
  - [6月](../Page/6月.md "wikilink")——[南部藝術家聯盟成立](../Page/南部藝術家聯盟.md "wikilink")。
  - [6月2日](../Page/6月2日.md "wikilink")——[臺灣省政府召開](../Page/臺灣省政府.md "wikilink")[臺灣省縣市文化中心籌備委員會第二次會議](../Page/臺灣省縣市文化中心.md "wikilink")，決議於[臺中市興建一座](../Page/臺中市.md "wikilink")[美術館](../Page/美術館.md "wikilink")。
  - [6月30日](../Page/6月30日.md "wikilink")——中華民國政府正式收回[紅毛城](../Page/紅毛城.md "wikilink")。\[2\]

### 7月至9月

  - [7月](../Page/7月.md "wikilink")——[寶藏巖從水源保護地正式劃入](../Page/寶藏巖.md "wikilink")[臨水區的](../Page/臨水區.md "wikilink")[297號都市計畫公園](../Page/297號都市計畫公園.md "wikilink")，不過因為涉及[新店溪臨水禁建區](../Page/新店溪.md "wikilink")，除了寶藏巖[觀音亭古蹟外](../Page/觀音亭.md "wikilink")，當地的[臺北市政府正式展開拆遷該地所有](../Page/臺北市政府.md "wikilink")[違建的計劃](../Page/違建.md "wikilink")。
  - [7月1日](../Page/7月1日.md "wikilink")——[高雄](../Page/高雄.md "wikilink")[八大畫廊開幕](../Page/八大畫廊.md "wikilink")。
  - [7月1日](../Page/7月1日.md "wikilink")——[南迴鐵路動工](../Page/南迴鐵路.md "wikilink")。
  - [7月10日](../Page/7月10日.md "wikilink")——[聯合報系開始推動](../Page/聯合報系.md "wikilink")[藝術歸鄉運動](../Page/藝術歸鄉運動.md "wikilink")。
  - [7月16日](../Page/7月16日.md "wikilink")——[臺鐵利用一些擁有冷氣空調車廂與內裝較高級的舊](../Page/臺鐵.md "wikilink")[莒光號車廂](../Page/莒光號列車.md "wikilink")，混合編組於[臺北](../Page/臺北車站.md "wikilink")[花蓮間及臺北](../Page/花蓮車站.md "wikilink")[高雄間](../Page/高雄車站.md "wikilink")，各試開4列次「[莒興號](../Page/莒興號.md "wikilink")」（39次、44次、15次與6次）。此階段，臺鐵高級車種共有[自強號](../Page/自強號列車.md "wikilink")、莒光號與此種莒興號，而莒興號仍與其他鐵路車輛相同，為[電力機車或](../Page/電力機車.md "wikilink")[柴電機車牽引](../Page/柴電機車.md "wikilink")。
  - [8月24日](../Page/8月24日.md "wikilink")——[美和青少棒贏得第二十屆](../Page/美和青少棒.md "wikilink")[世界青少棒大賽世界冠軍](../Page/世界青少棒大賽.md "wikilink")。
  - [8月30日](../Page/8月30日.md "wikilink")——[榮工少棒贏得第三十四屆](../Page/榮工少棒.md "wikilink")[世界少年棒球錦標賽世界冠軍](../Page/世界少棒大賽.md "wikilink")。
  - [9月](../Page/9月.md "wikilink")——[朱邦復與](../Page/朱邦復.md "wikilink")[宏碁公司共同發表世上首部具有](../Page/宏碁公司.md "wikilink")「中文[操作系统](../Page/操作系统.md "wikilink")、中文[程式語言](../Page/程式語言.md "wikilink")、中文[套裝軟體](../Page/套裝軟體.md "wikilink")」之中文電腦—[天龍中文電腦](../Page/天龍中文電腦.md "wikilink")，售價75萬元[新臺幣](../Page/新臺幣.md "wikilink")。

### 10月至12月

  - [10月13日](../Page/10月13日.md "wikilink")——[南非總理](../Page/南非總理.md "wikilink")[彼得·威廉·波塔伉儷來台訪問](../Page/彼得·威廉·波塔.md "wikilink")，由[行政院長](../Page/行政院長.md "wikilink")[孫運璿接機](../Page/孫運璿.md "wikilink")。\[3\]
  - [10月15日](../Page/10月15日.md "wikilink")——波塔與[蔣經國總統會晤](../Page/蔣經國.md "wikilink")，蔣並代表中華民國政府頒贈特種大綬[卿雲勳章予波塔](../Page/卿雲勳章.md "wikilink")。\[4\]
  - [10月22日](../Page/10月22日.md "wikilink")——[國立藝術學院成立籌備處](../Page/國立台北藝術大學.md "wikilink")。
  - [11月3日](../Page/11月3日.md "wikilink")——[第17屆金馬獎頒獎典禮](../Page/第17屆金馬獎.md "wikilink")，於[臺北市](../Page/臺北市.md "wikilink")[國父紀念館舉行](../Page/國父紀念館.md "wikilink")，由《[早安台北](../Page/早安台北.md "wikilink")》獲得最佳劇情片獎。
  - [12月6日](../Page/12月6日.md "wikilink")——
    [第一屆立法院第三次增額立法委員選舉進行投開票](../Page/1980年中華民國立法委員選舉.md "wikilink")。
  - [12月15日](../Page/12月15日.md "wikilink")——[新竹科學工業園區正式成立](../Page/新竹科學工業園區.md "wikilink")。
  - [12月29日](../Page/12月29日.md "wikilink")——國內[棒球體壇重要推手](../Page/棒球.md "wikilink")[謝國城逝世](../Page/謝國城.md "wikilink")，由[林鳳麟擔任](../Page/林鳳麟.md "wikilink")[中華民國棒球協會暫代理事長](../Page/中華民國棒球協會.md "wikilink")。

## 出生

*参见：* [1980年出生](../Category/1980年出生.md "wikilink")

  - [1月11日](../Page/1月11日.md "wikilink")——[林帛亨](../Page/林帛亨.md "wikilink")，台灣賽車手。
  - [1月20日](../Page/1月20日.md "wikilink")——[张栩](../Page/张栩.md "wikilink")，台灣旅日[棋士](../Page/圍棋.md "wikilink")。
  - [2月23日](../Page/2月23日.md "wikilink")——[周俊勳](../Page/周俊勳.md "wikilink")，台灣職業[棋士](../Page/圍棋.md "wikilink")。
  - [2月26日](../Page/2月26日.md "wikilink")——[明道](../Page/明道.md "wikilink")，台灣歌手、主持、演員。
  - [3月9日](../Page/3月9日.md "wikilink")——[黃瑜嫻](../Page/黃瑜嫻.md "wikilink")，台灣主持人。
  - [3月21日](../Page/3月21日.md "wikilink")——[余賢明](../Page/余賢明.md "wikilink")，台灣職棒選手。
  - [3月29日](../Page/3月29日.md "wikilink")——[蔡建偉](../Page/蔡建偉.md "wikilink")，台灣職棒選手。
  - 3月31日——[王建民](../Page/王建民_\(棒球選手\).md "wikilink")，[旅美棒球選手](../Page/旅美棒球選手.md "wikilink")。曾效力於美國職棒[洋基隊](../Page/洋基隊.md "wikilink")。
  - 5月6日——[張誌家](../Page/張誌家.md "wikilink")，台灣[旅日棒球選手](../Page/旅日棒球選手.md "wikilink")。
  - 6月13日——[王怡仁](../Page/王怡仁.md "wikilink")，台灣主播、藝人。
  - 7月1日——[陳信安](../Page/陳信安.md "wikilink")，台灣[籃球選手](../Page/籃球.md "wikilink")。
  - 7月26日——[謝祐鐘](../Page/謝祐鐘.md "wikilink")，台灣跆拳道選手。
  - 7月27日——[張建銘](../Page/張建銘.md "wikilink")，台灣職棒選手。
  - 8月6日——[潘瑋柏](../Page/潘瑋柏.md "wikilink")，台灣歌手。
  - 8月28日——[王仁甫](../Page/王仁甫.md "wikilink")，[男歌手](../Page/男歌手.md "wikilink")、主持人、男演員。演唱團體[5566副團長](../Page/5566.md "wikilink")。
  - 9月15日——[蔡依林](../Page/蔡依林.md "wikilink")，台灣歌手。
  - [9月29日](../Page/9月29日.md "wikilink")——[安以軒](../Page/安以軒.md "wikilink")，台灣演員。
  - [10月10日](../Page/10月10日.md "wikilink")——[李國慶](../Page/李國慶.md "wikilink")，台灣職棒選手。
  - [10月16日](../Page/10月16日.md "wikilink")——[許國隆](../Page/許國隆.md "wikilink")，台灣職棒選手。
  - [10月22日](../Page/10月22日.md "wikilink")——[莊宏亮](../Page/莊宏亮.md "wikilink")，台灣職棒選手。
  - [11月14日](../Page/11月14日.md "wikilink")——[林至涵](../Page/林至涵.md "wikilink")，台灣職業[棋士](../Page/圍棋.md "wikilink")。

## 逝世

*參見*：[1980年逝世](../Category/1980年逝世.md "wikilink")

  - [3月12日](../Page/3月12日.md "wikilink")——[莊嚴](../Page/莊嚴.md "wikilink")，[故宮副院長](../Page/故宮.md "wikilink")。
  - 3月14日——[洪炎秋](../Page/洪炎秋.md "wikilink")，台灣作家、教師。曾任臺大中文系教授、立法委員。
  - 8月19日——[郭秋生](../Page/郭秋生.md "wikilink")，台灣作家，也是[台灣話文運動的支持者](../Page/台灣話文運動.md "wikilink")。
  - 12月29日——[謝國城](../Page/謝國城.md "wikilink")，[中華民國棒球協會理事長](../Page/中華民國棒球協會.md "wikilink")。

## 參考來源

[\*](../Category/1980年台灣.md "wikilink")
[Category:1980年](../Category/1980年.md "wikilink")
[Category:20世纪各年台湾](../Category/20世纪各年台湾.md "wikilink")

1.
2.  薛化元：《戰後臺灣歷史閱覽》，台北：五南圖書出版，2010年，ISBN 9789571159102

3.  [南非共和國總理波塔訪問中華民國](http://catalog.digitalarchives.tw/item/00/3a/1b/29.html)

4.