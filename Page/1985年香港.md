## 大事記

### 1月

  - [1月28日](../Page/1月28日.md "wikilink")，[香港銀行公會表示將最優惠利率減少半厘](../Page/香港銀行公會.md "wikilink")。\[1\]
  - [1月28日](../Page/1月28日.md "wikilink")，百多名[沙田第一城居民抗議發展商加建油站](../Page/沙田第一城.md "wikilink")。\[2\]

### 2月

  - [2月2日](../Page/2月2日.md "wikilink")：
      - 一日內發生三宗大車禍，其中[屯門公路及](../Page/屯門公路.md "wikilink")[屯門舊墟分別造成](../Page/屯門.md "wikilink")6人喪生及3死2傷。同日另一宗車禍發生於[沙田大涌橋](../Page/沙田.md "wikilink")，60人受傷。\[3\]
      - [大埔工業邨附近舉行的越野車大賽發生意外](../Page/大埔工業邨.md "wikilink")，2名車手失事墮海喪命。\[4\]
  - [2月5日](../Page/2月5日.md "wikilink")，[醫務衛生署公佈香港首宗](../Page/醫務衛生署.md "wikilink")[愛滋病例](../Page/愛滋病.md "wikilink")。\[5\]
  - [2月17日](../Page/2月17日.md "wikilink")，香港首名愛滋病人去世。\[6\]

### 3月

  - [3月7日](../Page/3月7日.md "wikilink")，香港首次[統一區議會選舉](../Page/1985年香港區議會選舉.md "wikilink")。
  - [3月22日](../Page/3月22日.md "wikilink")，[西環車禍](../Page/西環.md "wikilink")，一屍兩命。

### 4月

  - [4月13日](../Page/4月13日.md "wikilink")，[坪洲揭發三屍兇殺及自殺案](../Page/坪洲_\(香港\).md "wikilink")。
  - [4月20日](../Page/4月20日.md "wikilink")，就讀[香港島一所中學的一對](../Page/香港島.md "wikilink")[英國籍情侶在前往](../Page/英國.md "wikilink")\[北角\]\][賽西湖公園之後失蹤](../Page/賽西湖公園.md "wikilink")。翌日，有晨運客在當時仍是荒地的北角寶馬山配水庫附近發現兩人屍體，男死者雙手反綁，身上傷痕多達數百處，而女死者死狀則更恐怖。經驗屍後證實兩人是被人亂棍打死，而且女死者曾被強姦，男死者則重傷後多個小時因失救而死。（另見：[寶馬山雙屍案](../Page/寶馬山雙屍案.md "wikilink")）

### 5月

  - [5月1日](../Page/5月1日.md "wikilink")，[尖沙咀](../Page/尖沙咀.md "wikilink")[彌敦道發生](../Page/彌敦道.md "wikilink")[忠信銀行械劫案](../Page/忠信銀行械劫案.md "wikilink")。
  - [5月12日](../Page/5月12日.md "wikilink")，[黃埔船塢工人燒焊時引起爆炸](../Page/黃埔船塢.md "wikilink")，觸發四級大火，1死3傷。
  - [5月14日](../Page/5月14日.md "wikilink")，事業如日中天的著名電視藝員[翁美玲在](../Page/翁美玲.md "wikilink")[九龍塘家中開](../Page/九龍塘.md "wikilink")[煤氣自殺身亡](../Page/煤氣.md "wikilink")。
  - [5月15日](../Page/5月15日.md "wikilink")，[石籬邨第](../Page/石籬邨.md "wikilink")1座女童兇殺案。\[7\]
  - [5月19日](../Page/5月19日.md "wikilink")，香港足球代表隊在北京以2-1擊敗中國隊，取得世界盃外圍賽亞洲區出線權。
  - [5月31日](../Page/5月31日.md "wikilink")，[香港地鐵](../Page/香港地鐵.md "wikilink")[港島線首期由](../Page/港島線.md "wikilink")[柴灣至](../Page/柴灣.md "wikilink")[金鐘段通車](../Page/金鐘.md "wikilink")。

### 6月

  - [6月7日](../Page/6月7日.md "wikilink")，海外信託銀行因財政問題被政府接管。
  - [6月16日](../Page/6月16日.md "wikilink")，[深水灣幾乎同一時間發生兩宗泳客溺斃事件](../Page/深水灣.md "wikilink")。

### 7月

  - [7月14日](../Page/7月14日.md "wikilink")，[九廣輕鐵首期動工](../Page/九廣輕鐵.md "wikilink")。
  - [7月26日](../Page/7月26日.md "wikilink")，[東區走廊](../Page/東區走廊.md "wikilink")（[鰂魚涌至](../Page/鰂魚涌.md "wikilink")[筲箕灣段](../Page/筲箕灣.md "wikilink")）竣工。

### 8月

  - [8月7日](../Page/8月7日.md "wikilink")：
      - [屯門](../Page/屯門.md "wikilink")[美樂花園](../Page/美樂花園.md "wikilink")10座10樓E座發生倫常命案，一名[警長之妻自縛與兩女墮樓身亡](../Page/警長.md "wikilink")。\[8\]
      - [廣東](../Page/廣東.md "wikilink")[惠陽一輛](../Page/惠陽.md "wikilink")[巴士墮山](../Page/巴士.md "wikilink")，全車3死25傷，其中[香港人](../Page/香港人.md "wikilink")1死16傷。

### 9月

  - [9月6日](../Page/9月6日.md "wikilink")，[颱風戴絲掠過香港](../Page/颱風戴絲_\(1985年\).md "wikilink")，[天文台懸掛](../Page/香港天文台.md "wikilink")[八號風球](../Page/八號烈風或暴風信號.md "wikilink")，2死12傷。
  - [9月24日](../Page/9月24日.md "wikilink")，[特別任務連攻入](../Page/特別任務連.md "wikilink")[跑馬地](../Page/跑馬地.md "wikilink")[成和道一個單位](../Page/成和道.md "wikilink")，拘捕[忠信錶行械劫案劫匪](../Page/忠信錶行械劫案.md "wikilink")。
  - [9月25日](../Page/9月25日.md "wikilink")，[吐露港公路通車](../Page/吐露港公路.md "wikilink")。
  - [9月26日](../Page/9月26日.md "wikilink")，香港首次舉行[香港立法局的](../Page/香港立法局.md "wikilink")[間接選舉](../Page/1985年香港立法局選舉.md "wikilink")。

### 10月

  - [10月19日](../Page/10月19日.md "wikilink")，政務司[廖本懷率領香港建築師代表團訪問北京](../Page/廖本懷.md "wikilink")，會見[國務院港澳辦公室主任](../Page/國務院港澳辦公室.md "wikilink")[姬鵬飛](../Page/姬鵬飛.md "wikilink")。[姬鵬飛表示在過渡期內](../Page/姬鵬飛.md "wikilink")，香港政制須與[基本法銜接](../Page/基本法.md "wikilink")，並希望「能夠不變就不變」。
  - [10月30日](../Page/10月30日.md "wikilink")，[立法會大樓舉行開幕儀式](../Page/立法會大樓_\(香港\).md "wikilink")。
  - [10月31日](../Page/10月31日.md "wikilink")，[太古城發生兇殺及自殺案](../Page/太古城.md "wikilink")。

### 11月

  - [11月21日](../Page/11月21日.md "wikilink")，政府公佈有577座公屋出現問題，需要維修。另[葵芳邨第](../Page/葵芳邨.md "wikilink")8，9，10和11座以及另外22座分佈在[黃竹坑邨](../Page/黃竹坑邨.md "wikilink")、[白田邨](../Page/白田邨.md "wikilink")、[石排灣邨](../Page/石排灣邨.md "wikilink")、[葵興邨](../Page/葵興邨.md "wikilink")、[石籬邨](../Page/石籬邨.md "wikilink")、[葵盛東邨等公屋的結構遠遠低於安全標準](../Page/葵盛東邨.md "wikilink")，有即時倒塌危險，需要儘快拆卸重建。（另見：[26座問題公屋醜聞](../Page/26座問題公屋醜聞.md "wikilink")）
  - [11月27日](../Page/11月27日.md "wikilink")，少女頸纏鐵鍊伏屍觀塘政府大廈廠房，男友[觀塘](../Page/觀塘.md "wikilink")[翠坪邨跳樓亡](../Page/翠坪邨.md "wikilink")。\[9\]

### 12月

  - [九龍公園重建工程展開](../Page/九龍公園.md "wikilink")，交易廣塲建成。
  - [12月18日](../Page/12月18日.md "wikilink")，[香港特別行政區基本法諮詢委員會成立](../Page/香港特別行政區基本法諮詢委員會.md "wikilink")，由[國務院港澳辦公室主任](../Page/國務院港澳辦公室.md "wikilink")[姬鵬飛主持](../Page/姬鵬飛.md "wikilink")，共有180名委員，包括8名[立法局議員](../Page/立法局議員.md "wikilink")、3名[香港政府高級官員及](../Page/香港政府高級官員.md "wikilink")5名[市政局議員](../Page/市政局議員.md "wikilink")。

## 相關條目

<div class="references-small">

<references />

</div>

## 參考資料

《八五年香港大事回顧》，無線電視，1985年12月29日

[Category:1985年香港](../Category/1985年香港.md "wikilink")
[Category:20世紀各年香港](../Category/20世紀各年香港.md "wikilink")
[Category:1985年](../Category/1985年.md "wikilink")

1.  1985年01月28日 - 翡翠台新聞簡報

2.
3.

4.

5.

6.
7.  華僑日報, 1985-05-16 第1頁

8.  華僑日報, 1985-08-08第1頁

9.  華僑日報, 1985-11-28 第7頁