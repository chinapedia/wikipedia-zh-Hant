**[1992年重要國際體育賽事包括](../Page/1992年.md "wikilink")**：

  - 第二十五屆[夏季奧林匹克運動會](../Page/1992年夏季奧林匹克運動會.md "wikilink")：[7月25日至](../Page/7月25日.md "wikilink")[8月9日於](../Page/8月9日.md "wikilink")[西班牙](../Page/西班牙.md "wikilink")[巴塞羅拿舉行](../Page/巴塞羅拿.md "wikilink")

## 大事記

### 1月至3月

### 4月至6月

  - [9月17日](../Page/9月17日.md "wikilink")－第二十五屆[夏季奧林匹克運動會於](../Page/1992年夏季奧林匹克運動會.md "wikilink")[西班牙](../Page/西班牙.md "wikilink")[巴塞羅拿揭幕](../Page/巴塞羅拿.md "wikilink")。

### 7月至9月

  - [9月12日](../Page/9月12日.md "wikilink")，[味全龍](../Page/味全龍.md "wikilink")[艾勃](../Page/艾勃.md "wikilink")、[羅世幸](../Page/羅世幸.md "wikilink")、[陳大順面對](../Page/陳大順.md "wikilink")[兄弟象](../Page/兄弟象.md "wikilink")[陳義信](../Page/陳義信.md "wikilink")，連續擊出3支全壘打，締造[中華職棒的紀錄](../Page/中華職棒.md "wikilink")。

### 10月至12月

## 綜合運動會

  - [7月25日至](../Page/7月25日.md "wikilink")[8月9日](../Page/8月9日.md "wikilink")：**[夏季奧林匹克運動會](../Page/1992年夏季奧林匹克運動會.md "wikilink")**

獎牌榜（只列出頭5名最佳成績隊伍）

<table style="width:55%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 19%" />
<col style="width: 8%" />
<col style="width: 8%" />
<col style="width: 8%" />
<col style="width: 5%" />
</colgroup>
<thead>
<tr class="header">
<th><p>名次</p></th>
<th><p>國家</p></th>
<th><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></th>
<th><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></th>
<th><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></th>
<th><p>總數</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Olympic_rings.svg" title="fig:Olympic_rings.svg">Olympic_rings.svg</a> <a href="../Page/獨立國協.md" title="wikilink">獨立國協</a></p></td>
<td><p>45</p></td>
<td><p>38</p></td>
<td><p>29</p></td>
<td><p>112</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p>37</p></td>
<td><p>34</p></td>
<td><p>37</p></td>
<td><p>108</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td><p>33</p></td>
<td><p>21</p></td>
<td><p>28</p></td>
<td><p>82</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td><p>16</p></td>
<td><p>22</p></td>
<td><p>16</p></td>
<td><p>54</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td><p>14</p></td>
<td><p>6</p></td>
<td><p>11</p></td>
<td><p>31</p></td>
</tr>
</tbody>
</table>

[Category:1992年](../Category/1992年.md "wikilink")
[Category:1990年代體育](../Category/1990年代體育.md "wikilink")
[\*](../Category/1992年體育.md "wikilink")