**1996年[國家聯盟分區賽](../Page/國家聯盟分區賽.md "wikilink")**，是1996年[國聯季後賽的第一輪](../Page/國家聯盟.md "wikilink")，於10月1日星期二至10月5日星期六間舉行。三個分區的冠軍與[外卡隊伍](../Page/外卡.md "wikilink")，兩兩捉對廝殺，分別進行5戰3勝制的系列賽。參與的隊伍有：

  - （一）[聖地牙哥教士](../Page/聖地牙哥教士.md "wikilink")（[西區冠軍](../Page/國家聯盟西區.md "wikilink")，91勝71負）對（三）[聖路易紅雀](../Page/聖路易紅雀.md "wikilink")（[中區冠軍](../Page/國家聯盟中區.md "wikilink")，88勝74負），紅雀橫掃教士。
  - （二）[亞特蘭大勇士](../Page/亞特蘭大勇士.md "wikilink")（[東區冠軍](../Page/國家聯盟東區.md "wikilink")，96勝66負）對（四）[洛杉磯道奇](../Page/洛杉磯道奇.md "wikilink")（外卡，90勝72負），勇士橫掃道奇。

<small>順位較高的種子隊伍（括號內的數字）有主場優勢，主場優勢跟戰績無關，而是事先決定的。這項安排相當不受到歡迎，所以在1997年季後賽結束後就不再採行這種方式。此外，有主場優勢的隊伍得先在客場進行前兩戰，然後才回到主場進行第三戰及有可能的四、五戰，以減少旅行成本。教士的對手是紅雀而不是外卡道奇，因為他們在同一分區。</small>

紅雀與勇士在橫掃對手之後，於[冠軍賽中碰頭](../Page/1996年國聯冠軍賽.md "wikilink")。勇士火力大爆發，以4勝3負贏得國聯冠軍，但是卻在[世界大賽中敗給](../Page/1996年世界大賽.md "wikilink")[美聯冠軍](../Page/1996年美聯冠軍賽.md "wikilink")[紐約洋基](../Page/紐約洋基.md "wikilink")。

**總教練：**（勇士）、（紅雀）、（道奇）、（教士）。

**裁判：**、、、、、（教士紅雀戰，第一、二場；勇士道奇戰，第三場）；、、、、、（勇士道奇戰，第一、二場；（教士紅雀戰，第三場）。

**電視轉播：**

  - [ESPN](../Page/ESPN.md "wikilink")（、，教士紅雀戰第一、二場；勇士道奇戰，第一場）
  - [FOX](../Page/FOX.md "wikilink")（、、，勇士道奇戰，第二、三場）
  - [NBC](../Page/NBC.md "wikilink")（、、，教士紅雀戰第三場）

## 聖地牙哥教士對聖路易紅雀

**紅雀（3）- 教士（0）**

| |場次 | 比數            | 日期    | 地點                                                               | 觀眾     |
| --- | ------------- | ----- | ---------------------------------------------------------------- | ------ |
|     |               |       |                                                                  |        |
| 1   | 教士 1，**紅雀 3** | 10月1日 | [Busch Stadium](../Page/Busch_Stadium.md "wikilink")             | 54,193 |
| 2   | 教士 4，**紅雀 5** | 10月3日 | [Busch Stadium](../Page/Busch_Stadium.md "wikilink")             | 56,752 |
| 3   | **紅雀 7**，教士 5 | 10月5日 | [Jack Murphy Stadium](../Page/Jack_Murphy_Stadium.md "wikilink") | 53,899 |

### 第一戰：10月1日

### 第二戰：10月3日

### 第三戰：10月5日

## 亞特蘭大勇士對洛杉磯道奇

**勇士（3）- 道奇（0）**

| |場次 | 比數                 | 日期    | 地點                                                                                   | 觀眾     |
| --- | ------------------ | ----- | ------------------------------------------------------------------------------------ | ------ |
|     |                    |       |                                                                                      |        |
| 1   | **勇士 2**，道奇 1（10局） | 10月2日 | [道奇體育場](../Page/道奇體育場.md "wikilink")                                                 | 47,428 |
| 2   | **勇士 3**，道奇 2      | 10月3日 | [道奇體育場](../Page/道奇體育場.md "wikilink")                                                 | 51,916 |
| 3   | 道奇 2，**勇士 5**      | 10月5日 | [Atlanta-Fulton County Stadium](../Page/Atlanta-Fulton_County_Stadium.md "wikilink") | 52,529 |

### 第一戰：10月2日

### 第二戰：10月3日

### 第三戰：10月5日

## 名言

## 外部連結

  - [（）勇士對道奇](http://www.baseball-reference.com/postseason/1996_NLDS1.shtml)
  - [（）紅雀對教士](http://www.baseball-reference.com/postseason/1996_NLDS2.shtml)
  - [（）1996年季後賽記分板及文字賽況](http://retrosheet.org/boxesetc/1996/YPS_1996.htm)

[Category:國聯分區賽](../Category/國聯分區賽.md "wikilink")
[Category:1996年體育](../Category/1996年體育.md "wikilink")
[Category:1996年10月](../Category/1996年10月.md "wikilink")
[Category:亞特蘭大勇士](../Category/亞特蘭大勇士.md "wikilink")
[Category:聖路易紅雀](../Category/聖路易紅雀.md "wikilink")
[Category:洛杉磯道奇](../Category/洛杉磯道奇.md "wikilink")
[Category:聖地牙哥教士](../Category/聖地牙哥教士.md "wikilink")