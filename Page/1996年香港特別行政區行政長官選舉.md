**1996年香港特別行政區行政長官選舉**是[香港特別行政區的首次行政長官選舉](../Page/香港特別行政區.md "wikilink")，在[香港主權移交半年多前舉行](../Page/香港主權移交.md "wikilink")。選舉結果由[董建華以](../Page/董建華.md "wikilink")320票當選為第一任[香港特別行政區行政長官](../Page/香港特別行政區行政長官.md "wikilink")。

## 历史

1996年1月26日，[香港特别行政區籌備委員會在](../Page/香港特别行政區籌備委員會.md "wikilink")[北京成立](../Page/北京.md "wikilink")，統籌特區成立的準備工作。1996年10月，行政長官選舉開始接受報名，共有31名人報名。11月，籌委會第六次全體會議主任委員會議對報名人進行審查，並公佈候選人名單，共有8位，包括4名熱門候選人[李福善](../Page/李福善.md "wikilink")、[董建華](../Page/董建華.md "wikilink")、[楊鐵樑及](../Page/楊鐵樑.md "wikilink")[吳光正](../Page/吳光正.md "wikilink")，以及其餘四位候選人杜森、區玉麟、蔡正矩及余漢彪。[羅德丞和賈施雅亦曾表示有意參選](../Page/羅德丞.md "wikilink")，但未有報名。

## 推選委員會

[香港特別行政區第一屆政府推選委員會成立](../Page/香港特別行政區第一屆政府推選委員會.md "wikilink")，由400名[香港永久性居民組成](../Page/香港永久性居民.md "wikilink")，成員主要為[建制派政界及商界人士](../Page/建制派.md "wikilink")。

1996年11月15日，政府推選委員會在[香港會議展覽中心舉行第一次會議](../Page/香港會議展覽中心.md "wikilink")，並對首任行政長官選舉候選人進行提名。投票在下午3時20分開始，歷時1小時。結果選出3名獲得超過50名推委提名的候選人，包括董建華、楊鐵樑及吳光正。

| 候選人     | 提名票     |
| ------- | ------- |
| **董建華** | **206** |
| **楊鐵樑** | **82**  |
| **吳光正** | **54**  |
| 李福善     | 43      |

## 選舉

1996年12月11日，香港特區第一屆政府推選委員會第三次全體會議舉行，會議上進行行政長官選舉，由400位推選委員會委員選出首任行政長官。

選舉結果董建華以320票當選，楊鐵樑及吳光正分別取得42票和36票，12月16日被[中华人民共和國國務院任命為香港特別行政區第一任行政長官](../Page/中华人民共和國國務院.md "wikilink")。\[1\]

| 候選人     | 得票              | 當選                                                                                           |
| ------- | --------------- | -------------------------------------------------------------------------------------------- |
| **董建華** | **320（得票率80%）** | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |
| 楊鐵樑     | 42              |                                                                                              |
| 吳光正     | 36              |                                                                                              |

## 資料來源

  - [董建華當選第一屆特別行政區行政長官](http://www.rthk.org.hk/special/headliner/headliner.htm)
    《[頭條新聞](../Page/頭條新聞.md "wikilink")》[香港電台](../Page/香港電台.md "wikilink")
  - [董建華簡歷](http://news.xinhuanet.com/ziliao/2002-03/07/content_305338.htm)

[Category:1996年選舉](../Category/1996年選舉.md "wikilink")
[Category:1996年香港](../Category/1996年香港.md "wikilink")
[Category:香港行政長官選舉](../Category/香港行政長官選舉.md "wikilink")

1.  [南方都市報](http://www.southcn.com/news/hktwma/zhuanti/djh/djhzl/200503110395.htm)
    2005年3月11日