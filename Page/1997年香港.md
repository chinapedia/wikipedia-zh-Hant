## 大事記

### 1月

  - [1月3日](../Page/1月3日.md "wikilink")：[白石船民中心準備關閉](../Page/白石船民中心.md "wikilink")，千多名船民陸續遷往[萬宜船民中心](../Page/萬宜船民中心.md "wikilink")。
  - [1月7日](../Page/1月7日.md "wikilink")：[飛鵝山發生山火](../Page/飛鵝山.md "wikilink")，焚毀25萬平方米林木。
  - [1月15日](../Page/1月15日.md "wikilink")：政府公佈打擊[炒樓措施](../Page/炒樓.md "wikilink")。
  - [1月19日](../Page/1月19日.md "wikilink")：[油麻地果欄發生三級火](../Page/油麻地果欄.md "wikilink")，燒毀生果批發倉。
  - [1月25日](../Page/1月25日.md "wikilink")：[尖沙咀](../Page/尖沙咀.md "wikilink")[寶勒巷一間卡拉OK被縱火](../Page/寶勒巷.md "wikilink")，列為三級火\[1\]，釀成17死13傷。（詳見[尖沙咀卡拉OK縱火案](../Page/尖沙咀卡拉OK縱火案.md "wikilink")）\[2\]被中國大陸囚禁的香港記者[席揚當日獲提前假釋返回香港](../Page/席揚.md "wikilink")。[郵政署同日發行](../Page/郵政署.md "wikilink")1997年版[香港通用郵票](../Page/香港通用郵票.md "wikilink")，並停售舊款有英女皇頭像的郵票。
  - [廣管局宣判](../Page/廣管局.md "wikilink")[無綫電視罰款](../Page/無綫電視.md "wikilink")10萬元。

### 2月

  - [2月7日](../Page/2月7日.md "wikilink")：尖沙咀[農曆新年花車巡遊發生意外](../Page/農曆新年花車巡遊.md "wikilink")，由[香港明天更好基金贊助](../Page/香港明天更好基金.md "wikilink")，掛滿[特區區徽和區旗的花車疑因車內積存過量](../Page/香港特別行政區區徽.md "wikilink")[氮氣導致司機昏厥](../Page/氮氣.md "wikilink")，車輛失控撞向人群，一名英藉女遊客捲入車底死亡。\[3\]
  - [2月10日](../Page/2月10日.md "wikilink")：[青山灣避風塘發生大火](../Page/青山灣避風塘.md "wikilink")，兩艘船被焚毀。
  - [2月20日](../Page/2月20日.md "wikilink")：[西九龍快速公路及](../Page/西九龍快速公路.md "wikilink")[葵涌高架道路通車](../Page/葵涌高架道路.md "wikilink")。候任行政長官[董建華公佈特區政府主要官員名單](../Page/董建華.md "wikilink")。
  - [2月24日](../Page/2月24日.md "wikilink")：[黃大仙](../Page/黃大仙.md "wikilink")[龍翔道發生火燒車事件](../Page/龍翔道.md "wikilink")，
    焚毀160多輛電單車，另有3人受傷。
  - [2月25日](../Page/2月25日.md "wikilink")：[香港公開進修學院升格為](../Page/香港公開進修學院.md "wikilink")[香港公開大學](../Page/香港公開大學.md "wikilink")。
  - [2月26日](../Page/2月26日.md "wikilink")：[天水圍八間學校受不明氣體侵襲](../Page/天水圍.md "wikilink")，卌三人不適及逾萬人需要疏散，事後證實[懲教署和](../Page/懲教署.md "wikilink")[警方機動部隊於青山練靶場防暴演習發射催淚彈](../Page/警察機動部隊.md "wikilink")，氣體沿西南風吹向元朗。

### 3月

  - [3月2日](../Page/3月2日.md "wikilink")：警方於[沙田](../Page/沙田.md "wikilink")[城門河畔揭發一宗木箱藏屍案](../Page/城門河.md "wikilink")。
  - [3月5日](../Page/3月5日.md "wikilink")：總督[彭定康巡視](../Page/彭定康.md "wikilink")[南丫島](../Page/南丫島.md "wikilink")[榕樹灣及](../Page/榕樹灣.md "wikilink")[大嶼山](../Page/大嶼山.md "wikilink")[梅窩](../Page/梅窩.md "wikilink")。
  - [3月8日](../Page/3月8日.md "wikilink")：[港督府於主權移交前最後一次向公眾開放](../Page/港督府.md "wikilink")，2天的參觀人數近10萬。
  - [3月10日](../Page/3月10日.md "wikilink")：[衛生署在市面出售的](../Page/衛生署.md "wikilink")[牛肉中首度發現有](../Page/牛肉.md "wikilink")[O157](../Page/O157.md "wikilink")[大腸桿菌](../Page/大腸桿菌.md "wikilink")
    。罪犯[葉繼歡被判入獄](../Page/葉繼歡.md "wikilink")30年。
  - [3月12日](../Page/3月12日.md "wikilink")：[財政司](../Page/財政司.md "wikilink")[曾蔭權宣讀](../Page/曾蔭權.md "wikilink")[1997年至1998年度財政預算案](../Page/1997年至1998年度香港政府財政預算案.md "wikilink")。
  - [3月16日](../Page/3月16日.md "wikilink")：[廉政公署搗破一個涉嫌](../Page/廉政公署.md "wikilink")[造馬的犯罪集團](../Page/造馬.md "wikilink")，拘捕30多人。
  - [3月20日](../Page/3月20日.md "wikilink")：來往[落馬洲及](../Page/落馬洲.md "wikilink")[皇崗的](../Page/皇崗.md "wikilink")[皇巴士正式通車](../Page/皇巴士.md "wikilink")。
  - [3月25日](../Page/3月25日.md "wikilink")：政府拍賣[小西灣及](../Page/小西灣.md "wikilink")[何文田商住地皮](../Page/何文田.md "wikilink")，庫房進帳132.2億港元，其中前者成交金額達118.2億港元，為史上最高。

### 4月

  - [4月1日](../Page/4月1日.md "wikilink")：[荔園正式結業](../Page/荔園.md "wikilink")。
  - [4月3日](../Page/4月3日.md "wikilink")：[北大嶼山快速公路東涌段完工](../Page/北大嶼山快速公路.md "wikilink")。
  - [4月4日](../Page/4月4日.md "wikilink")：[葵芳有託兒所爆發食物中毒](../Page/葵芳.md "wikilink")，33名學童不適。
  - [4月8日](../Page/4月8日.md "wikilink")，[美孚新邨發生因住客吸煙而導致的三級大火](../Page/美孚新邨.md "wikilink")\[4\]，造成9死36傷。\[5\]
  - [4月11日](../Page/4月11日.md "wikilink")：[昂船洲海軍基地關閉](../Page/昂船洲海軍基地.md "wikilink")。
  - [4月15日](../Page/4月15日.md "wikilink")：[維多利亞港西部發生雙體船撞渡輪事件](../Page/維多利亞港.md "wikilink")，造成27人受傷。
  - [4月19日](../Page/4月19日.md "wikilink")：總督[彭定康在](../Page/彭定康.md "wikilink")[港督府主持最後一次授勳儀式](../Page/港督府.md "wikilink")。
  - [4月21日](../Page/4月21日.md "wikilink")：40名[駐港解放軍先遣部隊抵達香港](../Page/駐港解放軍.md "wikilink")。
  - [4月22日](../Page/4月22日.md "wikilink")：入境處聯同消防處、警方和社署共30人採取行動，將非法入境居留的8歲女童[鍾若琳](https://www.google.com.hk/search?client=opera&q=鍾若琳事件&sourceid=opera&ie=UTF-8&oe=UTF-8)及其過期居留的母親遣返內地。
  - [4月23日](../Page/4月23日.md "wikilink")：[聯交所理事會前副主席](../Page/聯交所.md "wikilink")[陳葆心涉嫌貪污受審](../Page/陳葆心.md "wikilink")。
  - [4月24日](../Page/4月24日.md "wikilink")：[筲箕灣有女精神病者](../Page/筲箕灣.md "wikilink")[縱火焚家](../Page/筲箕灣西灣河街10號a-12號2樓縱火案.md "wikilink")，釀成5死4傷。
  - [4月30日](../Page/4月30日.md "wikilink")，[西區海底隧道正式通車](../Page/西區海底隧道.md "wikilink")，是香港第三條過海隧道。

### 5月

  - [5月4日](../Page/5月4日.md "wikilink")：[民權黨及](../Page/民權黨.md "wikilink")[社會民主陣線分別成立](../Page/社會民主陣線.md "wikilink")。
  - [5月9日](../Page/5月9日.md "wikilink")：[德國總理](../Page/德國總理.md "wikilink")[科爾訪問香港](../Page/科爾.md "wikilink")。
  - [5月11日](../Page/5月11日.md "wikilink")：[青嶼幹線舉行](../Page/青嶼幹線.md "wikilink")[公益金](../Page/公益金.md "wikilink")[百萬行](../Page/百萬行.md "wikilink")，約八萬人參加。
  - [5月14日](../Page/5月14日.md "wikilink")，[秀茂坪發生](../Page/秀茂坪.md "wikilink")[童黨燒屍案](../Page/秀茂坪燒屍案.md "wikilink")，一名16歲的少年在[秀茂坪邨一個單位內被一群童黨不人道地對待](../Page/秀茂坪邨.md "wikilink")，事主當場死亡。（詳見[秀茂坪童黨燒屍案](../Page/秀茂坪童黨燒屍案.md "wikilink")）
  - [5月17日](../Page/5月17日.md "wikilink")：總督[彭定康最後一次返倫敦述職](../Page/彭定康.md "wikilink")。香港爆發[霍亂疫潮](../Page/霍亂.md "wikilink")，12人受感染，政府表示會採取措施嚴防蔓延。
  - [5月19日](../Page/5月19日.md "wikilink")：
      - 香港代表隊在[釜山東亞運動會共獲一金五銀四銅的獎牌](../Page/第二屆東亞運動會.md "wikilink")，為主權移交前最後一次參加綜合運動會。
      - [王家衛憑](../Page/王家衛.md "wikilink")《[春光乍洩](../Page/春光乍洩.md "wikilink")》在第50屆[康城電影節獲最佳導演獎](../Page/康城電影節.md "wikilink")。
      - 首班[京九直通車抵達香港](../Page/京九直通車.md "wikilink")，首班[滬九直通車亦於次日抵達](../Page/滬九直通車.md "wikilink")。
  - [5月20日](../Page/5月20日.md "wikilink")：[北京控股](../Page/北京控股.md "wikilink")（）上市，掀起認購熱潮。
  - [5月21日](../Page/5月21日.md "wikilink")：[竹園邨發生三級火警](../Page/竹園邨.md "wikilink")。
  - [5月22日](../Page/5月22日.md "wikilink")：[8號幹線首期竣工](../Page/香港8號幹線.md "wikilink")。[青嶼幹線亦正式通車](../Page/青嶼幹線.md "wikilink")。
  - [5月26日](../Page/5月26日.md "wikilink")：[港進聯與](../Page/港進聯.md "wikilink")[自民聯正式合併](../Page/自民聯.md "wikilink")。同日[釣魚台號首度由香港抵達](../Page/釣魚台號.md "wikilink")[釣魚台海域宣示主權](../Page/釣魚台.md "wikilink")，與日本軍艦發生輕微碰撞。

### 6月

  - [6月2日](../Page/6月2日.md "wikilink")：[昂船洲污水處理廠完工](../Page/昂船洲污水處理廠.md "wikilink")。
  - [6月4日](../Page/6月4日.md "wikilink")：香港從3日晚上至4日早上連場暴雨，天文台發出[紅色暴雨警告信號](../Page/紅色暴雨警告信號.md "wikilink")，但[教育署在信號發出後](../Page/教育署.md "wikilink")3小時多才宣佈停課，引起學生和家長強烈不滿。暴雨造成37宗山泥傾瀉，在[九華徑有木屋被山泥淹埋](../Page/九華徑.md "wikilink")，1人死亡。\[6\]
  - [6月7日](../Page/6月7日.md "wikilink")：[太古城兩名男子被狂漢砍斬](../Page/太古城.md "wikilink")，一死一重傷。
  - [6月11日](../Page/6月11日.md "wikilink")：[中華電力宣佈計劃裁員千多人](../Page/中華電力.md "wikilink")。首屆[香港國際影視展在會展舉辦](../Page/香港國際影視展.md "wikilink")，有七十多家公司參展。
  - [6月14日](../Page/6月14日.md "wikilink")：[香港會議展覽中心二期落成啟用](../Page/香港會議展覽中心.md "wikilink")。同日，[香港臨時立法會三讀通過](../Page/香港臨時立法會.md "wikilink")《[公安條例](../Page/公安條例_\(香港\).md "wikilink")》及《[社團條例](../Page/社團條例.md "wikilink")》之修訂草案。
  - [6月15日](../Page/6月15日.md "wikilink")：[英皇御准香港賽馬會舉行主權移交前最後一次賽馬](../Page/英皇御准香港賽馬會.md "wikilink")。
  - [6月16日](../Page/6月16日.md "wikilink")：[白石船民中心關閉](../Page/白石船民中心.md "wikilink")。
  - [6月21日](../Page/6月21日.md "wikilink")：一輛由[元朗往](../Page/元朗.md "wikilink")[屯門碼頭的](../Page/屯門碼頭.md "wikilink")[香港輕鐵](../Page/香港輕鐵.md "wikilink")615號和720號於[兆康站相撞](../Page/兆康站_\(輕鐵\).md "wikilink")，29人受傷。同日，總督[彭定康為](../Page/彭定康.md "wikilink")[東涌新市鎮主持開幕儀式](../Page/東涌新市鎮.md "wikilink")。
  - [6月23日](../Page/6月23日.md "wikilink")：[黃大仙](../Page/黃大仙.md "wikilink")[翠竹花園發現一名婦女陪伴亡夫屍體兩年多的案件](../Page/翠竹花園.md "wikilink")。同日英國皇家遊艇[不列顛尼亞號駛入香港](../Page/不列顛尼亞號.md "wikilink")。
  - [6月24日](../Page/6月24日.md "wikilink")：總督[彭定康主持最後一次](../Page/彭定康.md "wikilink")[行政局會議](../Page/香港行政局.md "wikilink")。
  - [6月28日](../Page/6月28日.md "wikilink")：[立法局結束最後一次會議](../Page/香港立法局.md "wikilink")。
  - [6月30日](../Page/6月30日.md "wikilink")：[金鐘](../Page/金鐘.md "wikilink")[添馬艦舉行英方告別儀式](../Page/添馬艦.md "wikilink")，[英國結束對香港](../Page/英國.md "wikilink")150多年[殖民地管治](../Page/香港殖民地時期.md "wikilink")。

### 7月

  - [7月1日](../Page/7月1日.md "wikilink")：[香港主權移交](../Page/香港回歸.md "wikilink")，[香港特別行政區成立](../Page/香港特別行政區.md "wikilink")。同日[香港特別行政區護照開始簽發](../Page/香港特別行政區護照.md "wikilink")。
  - [7月3日](../Page/7月3日.md "wikilink")：香港連場暴雨，天文台於凌晨零時發出紅色暴雨警告信號歷時9小時半，是1992年設立暴雨警告信號系統以來最長的一次。全港共發生220多宗山泥傾瀉和水浸。[青山公路](../Page/青山公路.md "wikilink")[汀九](../Page/汀九.md "wikilink")[麗都灣一段因山泥傾瀉而封閉](../Page/麗都灣.md "wikilink")；凌晨時份[屯門公路近](../Page/屯門公路.md "wikilink")[麗城花園一段東西行都被大量沙石和洪水淹浸](../Page/麗城花園.md "wikilink")，路面變成激流，交通嚴重受阻，巴士乘客要下車涉水逃生\[7\]。
  - [7月8日](../Page/7月8日.md "wikilink")：[行政會議落實](../Page/行政會議.md "wikilink")[1998年立法會選舉辦法](../Page/1998年香港立法會選舉.md "wikilink")，20個地區議席將採用[比例代表制產生](../Page/比例代表制.md "wikilink")。
  - [7月16日](../Page/7月16日.md "wikilink")：[西環一名男子因無力償還賭債](../Page/西環.md "wikilink")，擔心年邁父母乏人照顧，遂毒殺雙親後跳樓身亡。
  - [7月18日](../Page/7月18日.md "wikilink")：[北角發生塌簷蓬意外](../Page/北角.md "wikilink")。
  - [7月20日](../Page/7月20日.md "wikilink")：[九巴一部巴士於](../Page/九巴.md "wikilink")[九龍塘](../Page/九龍塘.md "wikilink")[窩打老道上斜時發生火警](../Page/窩打老道.md "wikilink")，無人受傷。
  - [7月27日](../Page/7月27日.md "wikilink")：《[新晚報](../Page/新晚報.md "wikilink")》停刊。
  - [7月28日](../Page/7月28日.md "wikilink")：為期三個多月的[麻疹疫苗加強劑注射運動正式展開](../Page/麻疹疫苗加強劑注射運動.md "wikilink")，衛生署在此期間為全香港年齡介乎1歲至19歲的兒童及青少年免費注射一劑[麻疹腮腺炎德國麻疹混合疫苗](../Page/麻疹腮腺炎德國麻疹混合疫苗.md "wikilink")。

### 8月

  - [8月2日](../Page/8月2日.md "wikilink")：[颱風維克托襲港](../Page/颱風維克托.md "wikilink")，風暴中心更橫過剛通車的[青馬大橋](../Page/青馬大橋.md "wikilink")，天文台懸掛十四年來首次[九號烈風或暴風增強信號](../Page/九號烈風或暴風增強信號.md "wikilink")，1死58傷。
  - [8月10日](../Page/8月10日.md "wikilink")：[瑪麗醫院發生醫療事故](../Page/瑪麗醫院.md "wikilink")，一名車禍O型血傷者被先後輸錯A型及B型血液死亡。
  - [8月15日](../Page/8月15日.md "wikilink")：[菲律賓](../Page/菲律賓.md "wikilink")[馬尼拉灣一艘觀光船翻沉](../Page/馬尼拉灣.md "wikilink")，造成七名香港遊客死亡。
  - [8月28日](../Page/8月28日.md "wikilink")：中環發生致命交通意外，一輛私家車失控由[雲咸街直衝](../Page/雲咸街.md "wikilink")[皇后大道中行人路上人群](../Page/皇后大道中.md "wikilink")，造成三死十傷。\[8\]

### 9月

  - [9月1日](../Page/9月1日.md "wikilink")，[八達通系統正式啟動](../Page/八達通.md "wikilink")。
  - [9月2日](../Page/9月2日.md "wikilink")，九巴一架203線單層巴士(AM97
    EV9984)疑因閃避另一輛102線巴士(3AV
    GN8326)於[旺角](../Page/旺角.md "wikilink")[彌敦道右轉旺角道時撞入糖果店](../Page/彌敦道.md "wikilink")，1死12傷。\[9\]
  - [9月20日](../Page/9月20日.md "wikilink")：總理[李鵬訪問香港](../Page/李鵬.md "wikilink")。
  - [9月23日](../Page/9月23日.md "wikilink")：香港舉行[世界銀行](../Page/世界銀行.md "wikilink")[國際貨幣基金組織年會](../Page/國際貨幣基金組織.md "wikilink")，出席人士包括副總理[朱鎔基](../Page/朱鎔基.md "wikilink")。
  - [9月25日](../Page/9月25日.md "wikilink")：[教育署宣佈由下個學年起推行](../Page/教育署.md "wikilink")[母語教學政策](../Page/母語教學.md "wikilink")。

### 10月

  - [10月1日](../Page/10月1日.md "wikilink")，[香港特別行政區慶祝回歸後首個](../Page/香港特別行政區.md "wikilink")[國慶日](../Page/國慶日.md "wikilink")。
  - [10月8日](../Page/10月8日.md "wikilink")：行政長官[董建華發表](../Page/董建華.md "wikilink")[首份施政報告](../Page/1997年度香港行政長官施政報告.md "wikilink")。
  - [10月15日](../Page/10月15日.md "wikilink")：[黃金寶於上海](../Page/黃金寶.md "wikilink")[第八屆全運會獲得男子](../Page/中华人民共和国第八届运动会.md "wikilink")180公里單車公路賽金牌。
  - [10月23日](../Page/10月23日.md "wikilink")：受[亞洲金融風暴影響](../Page/亞洲金融風暴.md "wikilink")，香港[恆生指數大跌](../Page/恆生指數.md "wikilink")1,211.47點。
  - [10月28日](../Page/10月28日.md "wikilink")：香港[恆生指數再度崩跌](../Page/恆生指數.md "wikilink")1,621.80點，跌破9
    000點大關。
  - [10月29日](../Page/10月29日.md "wikilink")：[挪威國王](../Page/挪威國王.md "wikilink")[哈拉爾五世與皇后訪問香港](../Page/哈拉爾五世.md "wikilink")。

### 11月

  - [11月1日](../Page/11月1日.md "wikilink")：香港仔警署內一名廿四歲男疑犯被開槍擊斃。肇事警員後被控謀殺。
  - [11月10日](../Page/11月10日.md "wikilink")：[港基國際銀行爆發擠提](../Page/港基國際銀行.md "wikilink")，被提走16億港元。
  - [11月20日](../Page/11月20日.md "wikilink")：[波蘭總統](../Page/波蘭總統.md "wikilink")[克瓦希涅夫斯基訪問香港](../Page/克瓦希涅夫斯基.md "wikilink")。
  - [11月21日](../Page/11月21日.md "wikilink")，[八佰伴分店全線結業](../Page/八佰伴.md "wikilink")。
  - [11月24日](../Page/11月24日.md "wikilink")：[聖安娜餅店爆發擠提事件](../Page/聖安娜餅店.md "wikilink")，大批餅券被兌現。
  - [11月25日](../Page/11月25日.md "wikilink")：長沙灣賽馬會診所發現部分供兒童接種疫苗後作退燒用的60毫升裝藥水混有漱口水，並分發給共146名病人；事後[衛生署成立專責小組檢討該診所藥房的配藥程序及做法](../Page/衛生署.md "wikilink")。

### 12月

  - [12月2日](../Page/12月2日.md "wikilink")：[中英聯合聯絡小組於北京進行主權移交後的首次會議](../Page/中英聯合聯絡小組.md "wikilink")，討論[越南船民問題等事宜](../Page/香港越南船民問題.md "wikilink")。
  - [12月4日](../Page/12月4日.md "wikilink")：[大埔道發生](../Page/大埔道.md "wikilink")[山埃從車上翻滾事故](../Page/氰化鈉.md "wikilink")。
  - [12月9日](../Page/12月9日.md "wikilink")：行政長官[董建華首次到北京述職](../Page/董建華.md "wikilink")。
  - [12月23日](../Page/12月23日.md "wikilink")：為防範[禽流感](../Page/禽流感.md "wikilink")，香港暫停輸入中國大陸之活雞。
  - [12月29日](../Page/12月29日.md "wikilink")：為防範[禽流感](../Page/禽流感.md "wikilink")，政府開始銷毀香港超過100萬隻活雞。
  - [12月30日](../Page/12月30日.md "wikilink")：政府公佈[香港授勳及嘉獎制度](../Page/香港授勳及嘉獎制度.md "wikilink")。

## 公眾假期

1.  1月1日（星期三）：元旦
2.  2月6日（星期四）：農曆年初一的前一天（2月9日（星期日）農曆年初三補假）
3.  2月7日（星期五）：農曆年初一
4.  2月8日（星期六）：農曆年初二
5.  2月9日（星期日）：農曆年初三（連續放4天）
6.  3月21日（星期五）：耶穌受難節
7.  3月22日（星期六）：耶穌受難節翌日
8.  3月24日（星期一）：復活節星期一（3月23日（星期日）復活節補假；連同星期日連續放4天）
9.  4月5日（星期六）：清明節
10. 6月9日（星期一）：端午節
11. 6月28日（星期六）：英女皇壽辰（第四個星期六）
12. 6月30日（星期一）：英女皇壽辰後第一個星期一
13. 7月1日（星期二）：香港特別行政區成立紀念日
14. 7月2日（星期三）：香港特別行政區成立紀念日翌日（唯一一年不是星期日補假（即自己本身是星期一）的香港特別行政區成立紀念日翌日；連同星期日連續放5天，成為罕見也是至今唯一一次6月-7月之間出現連續四天或以上的假期）
15. 8月18日（星期一）：抗日戰爭勝利紀念日
16. 9月17日（星期三）：中秋節翌日
17. 10月1日（星期三）：國慶日
18. 10月2日（星期四）：國慶日翌日
19. 12月25日（星期四）：聖誕節
20. 12月26日（星期五）：聖誕節後第一個週日

共有20個假期，也是至今最多假期的年份。

## 出生

## 逝世

  - 1月14日：[胡金銓](../Page/胡金銓.md "wikilink")，64歲，香港電影導演。
  - 3月30日：[廖瑤珠](../Page/廖瑤珠.md "wikilink")，62歲，香港律師，[全國人大港區代表](../Page/全國人大.md "wikilink")。
  - 4月21日：[新馬師曾](../Page/新馬師曾.md "wikilink")，80歲，香港粵劇及喜劇演員。
  - 6月4日：[黃秉槐](../Page/黃秉槐.md "wikilink")，60歲，[香港市政局議員](../Page/香港市政局.md "wikilink")、[香港立法局議員](../Page/香港立法局.md "wikilink")。

## 大型獎項

### 電視廣播有限公司獎項（1997年）

## 参考文献

## 外部連結

  - [九七年香港大事回顧](https://www.youtube.com/watch?v=my-BgbWRpck)，[無綫電視](../Page/無綫電視.md "wikilink")，1997年12月28日

[Category:1997年香港](../Category/1997年香港.md "wikilink")
[Category:20世紀各年香港](../Category/20世紀各年香港.md "wikilink")
[Category:1997年](../Category/1997年.md "wikilink")

1.

2.
3.
4.

5.
6.

7.

8.  [97香港大事回顧片頭及片尾](http://www.youtube.com/watch?v=9Q_AN4Jz6r4&feature=related)，[有線新聞](../Page/有線新聞.md "wikilink")，1997年12月

9.