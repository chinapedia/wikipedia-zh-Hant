**1998年美国大使馆爆炸案**是指1998年8月7日，[美国驻](../Page/美国.md "wikilink")[东非](../Page/东非.md "wikilink")[坦桑尼亚](../Page/坦桑尼亚.md "wikilink")[首都](../Page/首都.md "wikilink")[达累斯萨拉姆和](../Page/达累斯萨拉姆.md "wikilink")[肯尼亚首都](../Page/肯尼亚.md "wikilink")[内罗毕的大使馆几乎同时遭遇](../Page/奈洛比.md "wikilink")[汽车炸弹袭击的事件](../Page/汽车炸弹.md "wikilink")。这两起共造成224人不幸遇难，超过4500人受伤的恐怖袭击，被认为是由[奥萨玛·本·拉登领导的](../Page/奥萨玛·本·拉登.md "wikilink")[基地组织於当地成员所为](../Page/基地组织.md "wikilink")，同时，也第一次引起了世人对本·拉登和“基地”的注意。美国[联邦调查局很快便将本](../Page/联邦调查局.md "wikilink")·拉登置于[十大通缉要犯之列](../Page/美國聯邦調查局十大通緝要犯.md "wikilink")。

与此前发生在[纽约的](../Page/纽约.md "wikilink")[1993年世界貿易中心爆炸案和发生在](../Page/1993年世界貿易中心爆炸案.md "wikilink")[沙特阿拉伯的](../Page/沙特阿拉伯.md "wikilink")[1996年阔巴尔塔楼袭击](../Page/1996年阔巴尔塔楼袭击.md "wikilink")，以及之后2000年，发生在[也门的](../Page/也门.md "wikilink")[美国海军柯尔号爆炸案一样](../Page/美国海军柯尔号爆炸案.md "wikilink")，这次的大使馆爆炸案也是发生在“[九一一恐怖袭击](../Page/九一一恐怖袭击.md "wikilink")”之前的一起主要的反美[恐怖袭击](../Page/恐怖袭击.md "wikilink")。对于这次袭击，当时正卷入[莱温斯基丑闻的](../Page/莱温斯基.md "wikilink")[美国总统](../Page/美国总统.md "wikilink")[比尔·克林顿以](../Page/比尔·克林顿.md "wikilink")[无限延伸行动](../Page/无限延伸行动.md "wikilink")、以及[逮捕这次](../Page/逮捕.md "wikilink")[炸弹袭击的](../Page/自殺攻擊.md "wikilink")[嫌犯等做為反擊](../Page/嫌犯.md "wikilink")。这些举措被对手小布什认为在一定程度上鼓励了基地组织实施[九一一恐怖袭击](../Page/九一一恐怖袭击.md "wikilink")，同时在美国国内也引起了应该对恐怖主义采取军事还是法律行动的争论。

## 袭击和伤亡

放置在两个大使馆附近的[汽车炸弹在当地时间上午](../Page/汽车炸弹.md "wikilink")10:45（GMT+03:00）左右相继爆炸。在内罗毕的美国大使馆由于位于一个繁忙的市中心区域，因而造成了213人死亡，和可能多至4000人的受伤；在[达累斯萨拉姆](../Page/达累斯萨拉姆.md "wikilink")，由于离市中心较远，此处的爆炸造成了至少12人死亡和85人受伤。

两座大使馆的建筑破坏严重，必须重建。

恐怖分子的目标显然是使馆内的美国人，但是除了几名外交官外，几乎所有的遇难者都是当地的非洲平民。在内罗毕，有32名肯尼亚籍使馆工作人员和12名美国人遇难；在达累斯萨拉姆，8名坦桑尼亚使馆工作人员遇难。其余的遇难者都是访客、过路人或者在附近建筑物中的人。

伊斯兰恐怖组织“[基地](../Page/基地组织.md "wikilink")”宣布对两起爆炸案负责

## 各方反应

作为对这次袭击的反应，[美国总统](../Page/美国总统.md "wikilink")[比尔·克林顿立即下达了在](../Page/比尔·克林顿.md "wikilink")1998年8月20日对[苏丹和](../Page/苏丹共和国.md "wikilink")[阿富汗境内恐怖分子目标进行](../Page/阿富汗.md "wikilink")[巡航导弹袭击的命令](../Page/巡航导弹.md "wikilink")，并在一次黄金时段的电视讲话中，向公众宣布了这一代号为“[无限延伸](../Page/无限延伸行动.md "wikilink")”的行动。

爆炸案的调查工作则是由[FBI与肯尼亚和坦桑尼亚当局负责的](../Page/FBI.md "wikilink")。一份嫌疑人名单被列了出来，并且有数人被控涉嫌参与了这次炸弹袭击。

在[伊拉克](../Page/伊拉克.md "wikilink")，[萨达姆·侯赛因的官方通讯社赞扬](../Page/萨达姆·侯赛因.md "wikilink")[本·拉丹为](../Page/本·拉丹.md "wikilink")“[阿拉伯和](../Page/阿拉伯.md "wikilink")[伊斯兰的英雄](../Page/伊斯兰.md "wikilink")”。稍后，克林顿政府的最高反恐官员[理查德·克拉克则宣称萨达姆在爆炸案后为本](../Page/理查德·克拉克.md "wikilink")·拉丹提供了庇护。

在[塔利班控制下的](../Page/塔利班.md "wikilink")[阿富汗](../Page/阿富汗.md "wikilink")，一个法庭于1998年11月20日宣布本·拉丹在爆炸案中没有犯下罪行。

## 恐怖分子的命运

[Kenya_bombing_2.jpg](https://zh.wikipedia.org/wiki/File:Kenya_bombing_2.jpg "fig:Kenya_bombing_2.jpg")

在[纽约举行的](../Page/纽约.md "wikilink")，并于2002年6月结束的一次联邦审判中，默罕默德·拉希德·达乌德·阿尔·奥哈里（Mohamed
Rashed Daoud Al-Owhali），默罕默德·奥德赫（Mohammed Odeh），瓦迪赫·埃尔·哈吉（Wadih el
Hage）和卡尔凡·卡米斯·默罕默德（Khalfan Khamis
Mohamed）四人被判在内罗毕爆炸案中有罪，并被判处终身监禁，不得假释。

1999年，[倫敦警察廳应美方的要求](../Page/倫敦警察廳.md "wikilink")，在[伦敦逮捕了两名](../Page/伦敦.md "wikilink")[埃及公民](../Page/埃及.md "wikilink")，易卜拉欣·侯赛因·阿卜代尔·哈迪·艾达罗斯（Ibrahim
Hussein Abdel Hadi Eidarous）和阿戴尔·莫罕奈德·阿布杜尔·阿尔马吉德·巴里（Adel Mohanned Abdul
Almagid
Bary）。因为在“[基地](../Page/基地组织.md "wikilink")”声称对爆炸案负责的信件上，发现了两人的[指纹](../Page/指纹.md "wikilink")。此二人已被引渡并被关押。

其他受到警方监控的嫌犯还包括卡利德·阿尔·法瓦兹（Khalid al
Fawwaz）。这名[沙烏地阿拉伯籍持不同政见者自](../Page/沙烏地阿拉伯.md "wikilink")1994年以来一直居住在伦敦。阿尔·法瓦兹被美国指控帮助奥萨马·本·拉丹协调了各次袭击，并且被美方要求引渡。他否认了这些指控，并在伦敦等待上诉结果。

阿纳斯·阿尔-利比（Anas
Al-Liby）于2002年[美军在](../Page/美军.md "wikilink")[阿富汗的行动中被捕](../Page/阿富汗.md "wikilink")，而阿赫迈德·卡尔凡·盖拉尼（Ahmed
Khalfan
Ghailani）则被认为已于2004年在[巴基斯坦被捕](../Page/巴基斯坦.md "wikilink")。阿布德·阿尔-拉希姆·阿尔-纳希里（Abd
al-Rahim
al-Nashiri），他帮助他作[人体炸弹的兄弟取得了一本](../Page/人体炸弹.md "wikilink")[也门护照](../Page/也门.md "wikilink")，现在已被美国人关押在了一个秘密地点。

默罕默德·阿提夫（Mohammed
Atef）在1998年11月4日被指在这次袭击事件中起了指挥的作用，被报告已死于2001年美军入侵阿富汗的轰炸中。

赛义夫·阿尔-阿代尔（Saif al-Adel）则被[伊朗政府监禁](../Page/伊朗.md "wikilink")。

### 继续在逃

  - [阿伊曼·阿尔-扎瓦希里](../Page/扎瓦希里.md "wikilink")（Ayman al-Zawahiri）
  - 阿卜杜拉·阿赫迈德·阿卜杜拉（Abdullah Ahmed Abdullah）
  - 穆赫欣·穆萨·阿塔瓦里·阿塔瓦（Muhsin Musa Atwalli Atwa）
  - 阿赫迈德·穆罕默德·哈迈德·阿里（Ahmed Mohammed Hamed Ali）
  - 法祖尔·阿卜杜拉·穆罕默德（Fazul Abdullah Mohammed）
  - 穆斯塔法·穆罕默德·法德西尔（Mustafa Mohamed Fadhil）
  - 谢克·阿赫迈德·萨利姆·斯威丹（Sheik Ahmed Salim Swedan）
  - 法希德·穆罕默德·阿里·穆萨拉姆（Fahid Mohammed Ally Msalam）

## 参见

  - [基地组织](../Page/基地组织.md "wikilink")
  - [无限延伸行动](../Page/无限延伸行动.md "wikilink")
  - [九一一恐怖袭击](../Page/九一一恐怖袭击.md "wikilink")

[Category:基地组织发动的恐怖活动](../Category/基地组织发动的恐怖活动.md "wikilink")
[Category:袭击美国外交代表机构事件](../Category/袭击美国外交代表机构事件.md "wikilink")
[Category:比尔·克林顿总统任期](../Category/比尔·克林顿总统任期.md "wikilink")
[Category:肯亞歷史](../Category/肯亞歷史.md "wikilink")
[Category:坦尚尼亞歷史](../Category/坦尚尼亞歷史.md "wikilink")
[Category:1998年美国](../Category/1998年美国.md "wikilink")
[Category:1998年屠杀](../Category/1998年屠杀.md "wikilink")
[Category:1991年后的美国历史](../Category/1991年后的美国历史.md "wikilink")
[Category:1998年恐怖活动](../Category/1998年恐怖活动.md "wikilink")
[Category:20世纪美国海军陆战队](../Category/20世纪美国海军陆战队.md "wikilink")