## 大事記

### 1月

  - [1月9日](../Page/1月9日.md "wikilink")：
      - 政府取消「[第一收容港](../Page/香港越南船民問題.md "wikilink")」政策。
      - [房屋委員會公佈](../Page/房屋委員會.md "wikilink")[租者置其屋計劃](../Page/租者置其屋計劃.md "wikilink")。

[Site_of_Citybus_tilted_at_Tonnochy_Road_in_January_1998.jpg](https://zh.wikipedia.org/wiki/File:Site_of_Citybus_tilted_at_Tonnochy_Road_in_January_1998.jpg "fig:Site_of_Citybus_tilted_at_Tonnochy_Road_in_January_1998.jpg")釀成5死50餘傷的天橋轉彎位置，當時其中3名乘客被拋出天橋底（右）\[1\]，更導致其中一人當場頭顱爆裂死亡\[2\]\]\]

  - [1月30日](../Page/1月30日.md "wikilink")，港島[灣仔](../Page/灣仔.md "wikilink")[杜老誌道發生](../Page/杜老誌道.md "wikilink")[致命車禍](../Page/1998年灣仔北城巴翻側事故.md "wikilink")。一架[118線](../Page/過海隧道巴士118線.md "wikilink")[城巴在杜老誌道天橋轉彎時](../Page/城巴.md "wikilink")，因為超速而翻側，造成5死58傷\[3\]。

### 2月

  - [2月17日](../Page/2月17日.md "wikilink")，港府宣佈不再延續[中巴專營權](../Page/中華汽車有限公司.md "wikilink")。\[4\]

### 3月

  - [3月17日](../Page/3月17日.md "wikilink")，[廉政公署拘捕](../Page/廉政公署.md "wikilink")[星島集團有限公司](../Page/星島集團有限公司.md "wikilink")3名現任或前任行政人員，指控他們誇大《[英文虎報](../Page/英文虎報.md "wikilink")》和《星期日英文虎報》的發行量，串謀詐騙廣告客戶。

### 4月

  - [藍田發生最後一宗中巴交通意外](../Page/藍田.md "wikilink")。
  - [4月1日](../Page/4月1日.md "wikilink")：[廉政公署通緝獲多利銀行前行政總裁](../Page/廉政公署_\(香港\).md "wikilink")[袁朗達](../Page/袁朗達.md "wikilink")。

### 5月

  - [5月6日](../Page/5月6日.md "wikilink")：[汀九橋通車](../Page/汀九橋.md "wikilink")。
  - [5月24日](../Page/5月24日.md "wikilink")
      - [香港特別行政區成立後舉行](../Page/香港特別行政區.md "wikilink")[首屆立法會選舉](../Page/1998年香港立法會選舉.md "wikilink")。\[5\]
      - [紅色暴雨警告訊號生效](../Page/香港暴雨警告信號#現行暴雨警告系統.md "wikilink")，深圳水庫排洪引致新界北水浸。\[6\]
  - [5月25日](../Page/5月25日.md "wikilink")，[大欖隧道通車](../Page/大欖隧道.md "wikilink")，至此[三號幹線全面啟用](../Page/三號幹線.md "wikilink")。
  - [5月26日](../Page/5月26日.md "wikilink")：新界東難民營舉行結役禮。

### 6月

  - [6月9日](../Page/6月9日.md "wikilink")：天文台發出[黑色暴雨警告](../Page/黑色暴雨警告.md "wikilink")，期間[太古站附近區域發生山泥傾瀉](../Page/太古站.md "wikilink")。
  - [6月13日](../Page/6月13日.md "wikilink")：
      - 地鐵[觀塘站上午](../Page/觀塘站.md "wikilink")7時許發生小火，燒毀一條電纜，鰂魚涌至彩虹列車服務暫停，4萬乘客受影響。\[7\]
      - 12名市民進食街市購買的深海魚後懷疑中[雪卡毒需留醫](../Page/雪卡毒.md "wikilink")。\[8\]
  - [6月22日](../Page/6月22日.md "wikilink")：[香港地鐵](../Page/香港地鐵.md "wikilink")[東涌綫啟用](../Page/東涌綫.md "wikilink")。
  - [6月25日](../Page/6月25日.md "wikilink")，[銅鑼灣歷史悠久之](../Page/銅鑼灣.md "wikilink")[大丸百貨宣佈在](../Page/大丸百貨.md "wikilink")1999年元旦前結業。

### 7月

  - [7月1日](../Page/7月1日.md "wikilink")，[第一屆香港立法會會期開始](../Page/第一屆香港立法會.md "wikilink")。
  - [7月2日](../Page/7月2日.md "wikilink")，[美國總統](../Page/美國總統.md "wikilink")[克林頓訪港兩日](../Page/比尔·克林顿.md "wikilink")，成為首位訪港的在任美國總統，他的[空軍一號專機在當時還有幾天才啟用的赤鱲角機場降落](../Page/空軍一號.md "wikilink")。同日，國家主席[江澤民在](../Page/江澤民.md "wikilink")[董建華及時任](../Page/董建華.md "wikilink")[機管局主席](../Page/機管局.md "wikilink")[黃保欣陪同下](../Page/黃保欣.md "wikilink")，為赤鱲角機場舉行開幕儀式\[9\]。
  - [7月6日](../Page/7月6日.md "wikilink")，[赤鱲角機場正式啟用](../Page/赤鱲角機場.md "wikilink")，[啟德機場關閉](../Page/啟德機場.md "wikilink")。但赤鱲角機場首天運作即因行李處理系統及電腦故障而遇上大混亂\[10\]。
  - [7月23日](../Page/7月23日.md "wikilink")，[德福花園發生轟動一時的](../Page/德福花園.md "wikilink")[五屍命案](../Page/德福花園五屍命案.md "wikilink")。\[11\]

### 8月

  - [8月31日](../Page/8月31日.md "wikilink")，[中巴結束專營權](../Page/中華汽車有限公司.md "wikilink")。

### 9月

  - [9月1日](../Page/9月1日.md "wikilink")，[新世界第一巴士接辦原有](../Page/新世界第一巴士.md "wikilink")[中巴](../Page/中華汽車有限公司.md "wikilink")88條路線。
  - [9月4日](../Page/9月4日.md "wikilink")：香港婦女團體到兩台請願，反對兩台賣弄[女性肉體](../Page/女性.md "wikilink")。

### 10月

  - [10月1日](../Page/10月1日.md "wikilink")：[駕易通及](../Page/駕易通.md "wikilink")[易通咭合併為](../Page/易通咭.md "wikilink")[快易通](../Page/快易通.md "wikilink")。

晚上7時10分，一輛[259D線沿](../Page/九龍巴士259D線.md "wikilink")[觀塘道往](../Page/觀塘道.md "wikilink")[旺角方向](../Page/旺角.md "wikilink")，駛至[啟業邨附近](../Page/啟業邨.md "wikilink")，因閃避一架切線私家車收掣，尾隨一輛[74A線巴士收掣不及](../Page/九龍巴士74A線.md "wikilink")，與前車相撞，事件中18名乘客及1名司機受傷。

  - [10月23日](../Page/10月23日.md "wikilink")：[無綫及](../Page/無綫.md "wikilink")[亞視兩家電視台因播映](../Page/亞洲電視.md "wikilink")「[陳健康事件](../Page/陳健康事件.md "wikilink")」而被[廣播事務管理局譴責](../Page/廣播事務管理局.md "wikilink")，分別於翌月判處罰款10萬和5萬元。

### 11月

  - [11月1日](../Page/11月1日.md "wikilink")：香港芭蕾舞團宣佈在2002元旦起全面轉以商業原則運作。

## 節慶

下列節慶，如無註明，均是香港的公眾假期，同時亦是法定假日（俗稱勞工假期）。有 \#
號者，不是公眾假期或法定假日（除非適逢星期日或其它假期），但在商業炒作下，市面上有一定節慶氣氛，[傳媒亦對其活動有所報導](../Page/香港傳媒.md "wikilink")。詳情可參看[香港節日與公眾假期](../Page/香港節日與公眾假期.md "wikilink")。

  - 1月1日（星期四）：[元旦](../Page/元旦.md "wikilink")
  - 1月28日（星期三）：[農曆年初一](../Page/農曆.md "wikilink")
  - 1月29日（星期四）：農曆年初二
  - 1月30日（星期五）：農曆年初三
  - 4月4日（星期六）：兒童節 \#
  - 4月6日（星期一）：[清明節翌日](../Page/清明節.md "wikilink")
    （原假期為4月5日清明節，唯本年該日適逢星期日，故於翌日補假）
  - 4月10日（星期五）：[耶穌受難節](../Page/耶穌受難日.md "wikilink")（是公眾假期，但不是法定假日）
  - 4月11日（星期六）：耶穌受難節翌日（是公眾假期，但不是法定假日）
  - 4月13日（星期一）：[復活節星期一](../Page/復活節.md "wikilink")（是公眾假期，但不是法定假日）
  - 5月30日（星期六）：[端午節](../Page/端午節.md "wikilink")
  - 7月1日（星期三）：[香港特別行政區成立紀念日](../Page/香港特別行政區成立紀念日.md "wikilink")
  - 8月17日（星期一）：[抗日戰爭勝利紀念日](../Page/第二次世界大战对日战争胜利纪念日.md "wikilink")
    （是公眾假期，但不是法定假日）
  - 10月1日（星期四）：[中華人民共和國國慶日](../Page/中華人民共和國國慶日.md "wikilink")
  - 10月2日（星期五）：[中華人民共和國國慶日翌日](../Page/中華人民共和國國慶日.md "wikilink")
    （是公眾假期，但不是法定假日）
  - 10月6日（星期二）：[中秋節翌日](../Page/中秋節.md "wikilink")
  - 10月28日（星期三）：[重陽節](../Page/重陽節.md "wikilink")
  - 10月31日（星期六）：[萬聖夜](../Page/萬聖夜.md "wikilink") \#
  - 12月22日（星期二）：[冬至](../Page/冬至.md "wikilink")（是法定假日，但不是公眾假期，根據法定假日放假的機構，或會聖誕節放假，冬至不放假。部份機構或會提早下班）
  - 12月24日（星期四）：[平安夜](../Page/平安夜.md "wikilink")\#（不是公眾假期或法定假日，但部份機構或會提早下班或放假一天）
  - 12月25日（星期五）：[聖誕節](../Page/聖誕節.md "wikilink")（根據法定假日放假的機構，或會冬至放假，聖誕節不放假）
  - 12月26日（星期六）：[聖誕節後第一個周日](../Page/節禮日.md "wikilink")（是公眾假期，但不是法定假日）
  - 12月31日（星期四）：[公曆除夕](../Page/跨年.md "wikilink")\#
    （不是公眾假期或法定假日，但部份機構或會提早下班或放假一天）

## 大型獎項

### 電視廣播有限公司獎項（1998年）

[香港小姐](../Page/香港小姐.md "wikilink")

  - 冠軍：[向海嵐](../Page/向海嵐.md "wikilink")
  - 亞軍：[趙翠儀](../Page/趙翠儀.md "wikilink")
  - 季軍：[吳文忻](../Page/吳文忻.md "wikilink")
  - 最上鏡小姐：[向海嵐](../Page/向海嵐.md "wikilink")
  - 國際親善小姐：[殷莉](../Page/殷莉.md "wikilink")
  - 最佳男主角：[羅嘉良](../Page/羅嘉良.md "wikilink")
  - 最佳女主角：[蔡少芬](../Page/蔡少芬.md "wikilink")
  - 全球華人新秀歌唱大賽香港區選拔賽金咪大獎（冠軍）：吳啟釗
  - 最受歡迎男歌手：[郭富城](../Page/郭富城.md "wikilink")
  - 最受歡迎女歌手：[王菲](../Page/王菲.md "wikilink")
  - 金曲金獎：《我這樣愛你》（歌手：[黎明](../Page/黎明.md "wikilink")）

## 参考文献

<div class="references-small">

<references />

</div>

[Category:1998年香港](../Category/1998年香港.md "wikilink")
[Category:20世紀各年香港](../Category/20世紀各年香港.md "wikilink")
[Category:1998年](../Category/1998年.md "wikilink")

1.

2.
3.  1998年1月30日[六點半新聞報道](../Page/六點半新聞報道.md "wikilink")

4.  1998年2月17日[六點鐘新聞](../Page/六點鐘新聞.md "wikilink")

5.  1998年5月24日[六點半新聞報道](../Page/六點半新聞報道.md "wikilink")

6.
7.
8.  1998年6月13日[六點鐘新聞](../Page/六點鐘新聞.md "wikilink")

9.

10.

11. 1998年7月23日晚間新聞