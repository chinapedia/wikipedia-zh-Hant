《**-{19歲的純情}-**》（）是[韓國](../Page/大韓民國.md "wikilink")[KBS
1TV於](../Page/韓國放送公社.md "wikilink")2006年5月22日起，每週一至週五晚上8：25播放的[日日連續劇](../Page/KBS_1TV日日連續劇.md "wikilink")，KBS於2006年收視第二高的劇集，僅次於《[傳聞中的七公主](../Page/傳聞中的七公主.md "wikilink")》。由新人[具惠善](../Page/具惠善.md "wikilink")、[徐芝釋](../Page/徐芝釋.md "wikilink")、[李珉宇](../Page/李珉宇.md "wikilink")、[李允智及](../Page/李允智.md "wikilink")[秋素英等人出演](../Page/秋素英.md "wikilink")。
[台灣](../Page/台灣.md "wikilink")[八大戲劇台於](../Page/八大戲劇台.md "wikilink")2009年4月15日起，每週一至週五早上9：00有重播。[韓國](../Page/大韓民國.md "wikilink")[KBS
World於](../Page/KBS_World.md "wikilink")2011年11月22日起，每週一至週五早上7：10亦有重播。

## 劇情介紹

19歲的([朝鮮族](../Page/朝鮮族.md "wikilink"))梁菊花因為家貧所以從[中國](../Page/中國.md "wikilink")[延邊嫁到](../Page/延邊.md "wikilink")[韓國](../Page/韓國.md "wikilink")。可是，菊花來到韓國前，未婚夫洪舜邱就因交通意外身亡。善良的菊花不想回延邊加重家人的負擔，決定留在[首爾](../Page/首爾.md "wikilink")[打工寄錢回鄉](../Page/打工.md "wikilink")。在未婚夫的侄子洪羽慶其同事-姜晨馨的介紹下，菊花到他們工作的公司當會長兒子朴允侯的秘書。

允侯是公司的太子爺，但對工作非常嚴格，因此不停更換秘書。菊花因一次失誤而被辭退，轉當該公司的清潔工。允侯在公司與菊花的相處中，漸漸被菊花那樂觀開朗和堅毅不屈的性格所吸引，一段王子與灰姑娘的愛情故事亦由此而展開……

## 演員陣容

### 主要人物

  - [具惠善](../Page/具惠善.md "wikilink") 飾 梁菊花
  - [李珉宇](../Page/李珉宇.md "wikilink") 飾 洪羽慶
  - [徐芝釋](../Page/徐芝釋.md "wikilink") 飾 朴允侯
  - [李允智](../Page/李允智.md "wikilink") 飾 朴允貞
  - [秋素英](../Page/秋素英.md "wikilink") 飾 姜晨馨

### 羽慶家

  - [申久](../Page/申久.md "wikilink") 飾 洪榮甘
  - [姜南吉](../Page/姜南吉.md "wikilink") 飾 洪文邱
  - [金美京](../Page/金美京.md "wikilink") 飾 金玉錦
  - [趙貞麟](../Page/趙貞麟.md "wikilink") 飾 洪羽淑
  - [李惠淑](../Page/李惠淑.md "wikilink") 飾 崔惠淑
  - [李漢偉](../Page/李漢偉.md "wikilink") 飾 高先生
  - [康石雨](../Page/康石雨.md "wikilink") 飾 洪豐邱
  - [孫鍾範](../Page/孫鍾範.md "wikilink") 飾 洪舜邱

### 允侯家

  - [韓振熙](../Page/韓振熙.md "wikilink") 飾 朴東國
  - [尹汝貞](../Page/尹汝貞.md "wikilink") 飾 尹明惠
  - [尹宥善](../Page/尹宥善.md "wikilink") 飾 朴允智
  - [安政勳](../Page/安政勳.md "wikilink") 飾 高光萬

### 其他人物

  - [高恩美](../Page/高恩美.md "wikilink") 飾 夏秀晶
  - [崔元俊](../Page/崔元俊.md "wikilink") 飾 姜建榮
  - [趙美玲](../Page/趙美玲.md "wikilink") 飾 羅八子

## 其他搭配歌曲

  - 台灣[八大第一台版本](../Page/八大第一台.md "wikilink") 
      - 片尾曲：[卓義峰](../Page/卓義峰.md "wikilink")《愛情乞丐》

## 收視率

在播出後收視率不斷上升，第2週的平均收視升至超過20%，第14週的平均收視達到30.3%。同時段[MBC的](../Page/文化廣播_\(韓國\).md "wikilink")《[有多愛](../Page/有多愛.md "wikilink")》只維持個位數收視。結局週的平均收視更達40.0%，最高收視的一集於2006年12月25日[聖誕節當晚播出](../Page/聖誕節.md "wikilink")，達到42.5%\[1\]。

## 翻拍

  - 《[小菊的春天](../Page/小菊的春天.md "wikilink")》：由[柴智屏擔任製作人](../Page/柴智屏.md "wikilink")，[穎兒](../Page/穎兒.md "wikilink")、[王傳一](../Page/王傳一.md "wikilink")、[高亞麟](../Page/高亞麟.md "wikilink")、[江淑娜](../Page/江淑娜.md "wikilink")、[鄒廷威出演](../Page/鄒廷威.md "wikilink")。\[2\]

## 腳註

## 外部連結

  - [加油\!菊花
    戲劇完整版](https://www.youtube.com/channel/UCThPYSKoo46AeHWsvBrkPOw)
  - [加油\!菊花
    YouTube頻道](https://www.youtube.com/channel/UC1-q9k4hrfzGGtCuWW24b8Q)
  - [韓國KBS](http://www.kbs.co.kr/drama/soonjung/)
  - [臺灣GTV](https://web.archive.org/web/20100106203016/http://www.gtv.com.tw/Program/B051420070904U/)

[Category:2006年韓國電視劇集](../Category/2006年韓國電視劇集.md "wikilink")
[Category:八大電視外購韓劇](../Category/八大電視外購韓劇.md "wikilink")
[Category:韓國愛情劇](../Category/韓國愛情劇.md "wikilink")
[Category:職場戀情連續劇](../Category/職場戀情連續劇.md "wikilink")
[Category:異族戀題材電視劇](../Category/異族戀題材電視劇.md "wikilink")
[Category:貧窮題材電視劇](../Category/貧窮題材電視劇.md "wikilink")

1.  收視資料：[TNS Media Korea](http://www.tnsmk.co.kr)
2.  [深圳衛視自制開年戲
    柴智屏打造《小菊的春天》](http://ent.sina.com.cn/v/m/2010-05-28/16532971977.shtml)