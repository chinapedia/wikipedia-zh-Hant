**1月14日**是[公历年的第](../Page/公历.md "wikilink")14天，离一年的结束还有351天（[闰年是](../Page/闰年.md "wikilink")352天）。

## 大事记

### 16世紀

  - [1514年](../Page/1514年.md "wikilink")：[教宗](../Page/教宗.md "wikilink")[利奥十世发表声明](../Page/利奥十世.md "wikilink")，反对[奴隶制](../Page/奴隶制.md "wikilink")。
  - [1539年](../Page/1539年.md "wikilink")：[西班牙宣布占领](../Page/西班牙.md "wikilink")[古巴](../Page/古巴.md "wikilink")。

### 18世紀

  - [1724年](../Page/1724年.md "wikilink")：[西班牙波旁王朝首位國王](../Page/西班牙波旁王朝.md "wikilink")[費利佩五世爭議性地將王位讓給長子](../Page/費利佩五世.md "wikilink")[路易斯一世](../Page/路易斯一世\(西班牙\).md "wikilink")。
  - [1761年](../Page/1761年.md "wikilink")：[杜兰尼王朝君王](../Page/杜兰尼王朝.md "wikilink")[艾哈迈德沙·杜兰尼在](../Page/艾哈迈德沙·杜兰尼.md "wikilink")[第三次帕尼帕特战役击败法国所派遣的部队](../Page/第三次帕尼帕特战役.md "wikilink")。
  - [1784年](../Page/1784年.md "wikilink")：[美国独立战争正式结束](../Page/美国独立战争.md "wikilink")。

### 19世紀

  - [1814年](../Page/1814年.md "wikilink")：[丹麦作为](../Page/丹麦.md "wikilink")[拿破仑战争的失败者和](../Page/拿破仑战争.md "wikilink")[瑞典签订](../Page/瑞典.md "wikilink")《[基尔条约](../Page/基尔条约.md "wikilink")》，割让[挪威予瑞典](../Page/挪威.md "wikilink")。
  - [1895年](../Page/1895年.md "wikilink")：[日本通过内阁会议决定](../Page/日本.md "wikilink")，声称[钓鱼岛为](../Page/钓鱼岛.md "wikilink")“[无主地](../Page/无主地.md "wikilink")”，在钓鱼岛建立标国标，正式划入日本版图。
  - [1900年](../Page/1900年.md "wikilink")：[意大利作曲家](../Page/意大利.md "wikilink")[贾科莫·普契尼创作的](../Page/贾科莫·普契尼.md "wikilink")[歌剧](../Page/歌剧.md "wikilink")《[托斯卡](../Page/托斯卡.md "wikilink")》在[罗马首演](../Page/罗马.md "wikilink")。

### 20世紀

  - [1907年](../Page/1907年.md "wikilink")：[牙买加](../Page/牙买加.md "wikilink")[金斯敦发生大](../Page/金斯敦_\(牙买加\).md "wikilink")[地震](../Page/地震.md "wikilink")。
  - [1916年](../Page/1916年.md "wikilink")：[荷兰](../Page/荷兰.md "wikilink")[须德海水坝崩溃](../Page/须德海水坝.md "wikilink")，造成洪水泛滥\[1\]。
  - [1938年](../Page/1938年.md "wikilink")：[新疆军阀](../Page/新疆.md "wikilink")[盛世才清洗异已](../Page/盛世才.md "wikilink")，杀300多人。
  - [1939年](../Page/1939年.md "wikilink")：挪威对[毛德王后地提出主权要求](../Page/毛德王后地.md "wikilink")。
  - [1942年](../Page/1942年.md "wikilink")：[二战](../Page/二战.md "wikilink")：[美军在](../Page/美军.md "wikilink")[菲律宾](../Page/菲律宾.md "wikilink")[巴丹半岛抵抗](../Page/巴丹半岛.md "wikilink")[日军进攻](../Page/日军.md "wikilink")。
  - [1943年](../Page/1943年.md "wikilink")：二战：[卡萨布兰卡会议在](../Page/卡萨布兰卡会议.md "wikilink")[摩洛哥举行](../Page/摩洛哥.md "wikilink")。
  - [1949年](../Page/1949年.md "wikilink")：[国共内战](../Page/国共内战.md "wikilink")：[毛泽东发表](../Page/毛泽东.md "wikilink")《[关于时局的声明](../Page/关于时局的声明.md "wikilink")》以回应[蒋介石的](../Page/蒋介石.md "wikilink")《[元旦文告](../Page/元旦文告.md "wikilink")》，提出结束内战的八项条件。
  - [1950年](../Page/1950年.md "wikilink")：[苏联](../Page/苏联.md "wikilink")[米高扬设计局研制的](../Page/米高扬设计局.md "wikilink")[米格-17战斗机原型机进行首次飞行](../Page/米格-17战斗机.md "wikilink")。
  - [1953年](../Page/1953年.md "wikilink")：[铁托开始担任](../Page/铁托.md "wikilink")[南斯拉夫社会主义联邦共和国总统和武装部队最高统帅](../Page/南斯拉夫社会主义联邦共和国.md "wikilink")。
  - [1965年](../Page/1965年.md "wikilink")：[中共中央开展](../Page/中共中央.md "wikilink")[四清运动](../Page/四清运动.md "wikilink")。
  - [1972年](../Page/1972年.md "wikilink")：[丹麦国王](../Page/丹麦国王.md "wikilink")[弗雷德里克九世逝世](../Page/弗雷德里克九世.md "wikilink")，[玛格丽特二世继位](../Page/玛格丽特二世.md "wikilink")。
  - [1978年](../Page/1978年.md "wikilink")：[香港賽馬會](../Page/香港賽馬會.md "wikilink")[馬伕工潮](../Page/馬伕.md "wikilink")。
  - [1978年](../Page/1978年.md "wikilink")：[奧地利](../Page/奧地利.md "wikilink")[邏輯學家](../Page/邏輯學家.md "wikilink")[庫爾特·哥德爾由於害怕遭他人下](../Page/庫爾特·哥德爾.md "wikilink")[毒而餓死在](../Page/毒.md "wikilink")[普林斯頓的醫院中](../Page/普林斯頓.md "wikilink")。
  - [1983年](../Page/1983年.md "wikilink")：。
  - [1987年](../Page/1987年.md "wikilink")：[香港政府宣佈清拆](../Page/香港政府.md "wikilink")[九龍城寨](../Page/九龍城寨.md "wikilink")，改建為[公園](../Page/公園.md "wikilink")。
  - [1994年](../Page/1994年.md "wikilink")：中国政府正式向美方登记发行10亿美元全球债券。
  - 1994年：[美国](../Page/美国.md "wikilink")、[俄罗斯和](../Page/俄罗斯.md "wikilink")[乌克兰签署协议](../Page/乌克兰.md "wikilink")，决定全部销毁乌克兰境内的[核武器](../Page/核武器.md "wikilink")。
  - [1995年](../Page/1995年.md "wikilink")：中华人民共和国第八届全国冬季运动会在[吉林省](../Page/吉林省.md "wikilink")[吉林市举行](../Page/吉林市.md "wikilink")。
  - [2000年](../Page/2000年.md "wikilink")：人类成功[克隆猴](../Page/克隆.md "wikilink")。

### 21世紀

  - [2004年](../Page/2004年.md "wikilink")：[格鲁吉亚议会通过法案](../Page/格鲁吉亚.md "wikilink")，恢复使用“五个十字旗”作为[格鲁吉亚国旗](../Page/格鲁吉亚国旗.md "wikilink")。
  - [2005年](../Page/2005年.md "wikilink")：[欧洲空间局](../Page/欧洲空间局.md "wikilink")[惠更斯号探测器登陆](../Page/惠更斯号.md "wikilink")[土卫六](../Page/土卫六.md "wikilink")，创造了探测器登陆其它[星球最远距离的新记录](../Page/星球.md "wikilink")。
  - [2011年](../Page/2011年.md "wikilink")：統治突尼斯23年的總統[本·阿里因](../Page/本·阿里.md "wikilink")[茉莉花革命而下台](../Page/茉莉花革命.md "wikilink")，與家人出走沙特阿拉伯。
  - [2012年](../Page/2012年.md "wikilink")：中華民國第十三屆總統、副總統及第八屆立法委員選舉日。
  - [2016年](../Page/2016年.md "wikilink")：[伊斯兰国恐怖组织在](../Page/伊斯兰国.md "wikilink")[印尼首都](../Page/印尼.md "wikilink")[雅加達发动連環爆炸事件](../Page/雅加達.md "wikilink")，造成六人死亡。\[2\]

## 出生

  - [前83年](../Page/前83年.md "wikilink")：[馬克·安東尼](../Page/馬克·安東尼.md "wikilink")，古羅馬政治家、軍事家（[前30年去世](../Page/前30年.md "wikilink")）
  - [1702年](../Page/1702年.md "wikilink")：[中御門天皇](../Page/中御門天皇.md "wikilink")，[日本第](../Page/日本.md "wikilink")114代[天皇](../Page/天皇.md "wikilink")（[1737年去世](../Page/1737年.md "wikilink")）
  - [1741年](../Page/1741年.md "wikilink")：[贝内迪克特·阿诺德](../Page/贝内迪克特·阿诺德.md "wikilink")，[美國獨立戰爭時期將軍](../Page/美國.md "wikilink")（[1801年去世](../Page/1801年.md "wikilink")）
  - [1770年](../Page/1770年.md "wikilink")：[亞當·耶日·恰爾托雷斯基](../Page/亞當·耶日·恰爾托雷斯基.md "wikilink")，[波蘭親王](../Page/波蘭.md "wikilink")、政治家（[1861年逝世](../Page/1861年.md "wikilink")）
  - [1798年](../Page/1798年.md "wikilink")：[约翰·鲁道夫·托尔贝克](../Page/约翰·鲁道夫·托尔贝克.md "wikilink")，[荷蘭首相](../Page/荷蘭.md "wikilink")（[1872年逝世](../Page/1872年.md "wikilink")）
  - [1800年](../Page/1800年.md "wikilink")：[路德维希·里特尔·冯·克歇尔](../Page/路德维希·里特尔·冯·克歇尔.md "wikilink")，[奧地利音樂學家](../Page/奧地利.md "wikilink")、作家、作曲家、植物學家、出版商（[1877年逝世](../Page/1877年.md "wikilink")）
  - [1806年](../Page/1806年.md "wikilink")：[马修·莫里](../Page/马修·莫里.md "wikilink")，[美国现代海洋学之父](../Page/美国.md "wikilink")([1872年逝世](../Page/1872年.md "wikilink"))
  - [1841年](../Page/1841年.md "wikilink")：[貝爾特·莫里索](../Page/貝爾特·莫里索.md "wikilink")，[法國巴黎印象派女畫家](../Page/法國.md "wikilink")（[1895年逝世](../Page/1895年.md "wikilink")）
  - [1850年](../Page/1850年.md "wikilink")：[皮埃爾·洛蒂](../Page/皮埃爾·洛蒂.md "wikilink")，法國小說家、海軍軍官（[1923年去世](../Page/1923年.md "wikilink")）
  - [1861年](../Page/1861年.md "wikilink")：[穆罕默德六世](../Page/穆罕默德六世_\(奥斯曼帝国\).md "wikilink")，[奥斯曼帝国第三十六代](../Page/奥斯曼帝国.md "wikilink")（末代）[苏丹](../Page/苏丹.md "wikilink")（[1926年去世](../Page/1926年.md "wikilink")）
  - [1875年](../Page/1875年.md "wikilink")：[艾伯特·史懷哲](../Page/艾伯特·史懷哲.md "wikilink")，[德國](../Page/德國.md "wikilink")[神學家](../Page/神學家.md "wikilink")、[哲學家](../Page/哲學家.md "wikilink")，1952年[諾貝爾和平獎得主](../Page/諾貝爾和平獎.md "wikilink")（[1965年去世](../Page/1965年.md "wikilink")）
  - [1882年](../Page/1882年.md "wikilink")：[亨德里克·威廉·房龙](../Page/亨德里克·威廉·房龙.md "wikilink")，荷蘭裔[美国](../Page/美国.md "wikilink")[作家](../Page/作家.md "wikilink")、[历史学家](../Page/历史学家.md "wikilink")、插圖畫家（[1944年去世](../Page/1944年.md "wikilink")）
  - [1892年](../Page/1892年.md "wikilink")：[馬丁·尼莫拉](../Page/馬丁·尼莫拉.md "wikilink")，德國神學家、信義宗牧師（[1984年去世](../Page/1984年.md "wikilink")）
  - [1896年](../Page/1896年.md "wikilink")：[约翰·多斯·帕索斯](../Page/约翰·多斯·帕索斯.md "wikilink")，美國小說家、藝術家（[1970年去世](../Page/1970年.md "wikilink")）
  - [1897年](../Page/1897年.md "wikilink")：[哈索·冯·曼陀菲尔](../Page/哈索·冯·曼陀菲尔.md "wikilink")，德國軍事家、政治家（[1978年去世](../Page/1978年.md "wikilink")）
  - [1901年](../Page/1901年.md "wikilink")：[阿尔弗雷德·塔斯基](../Page/阿尔弗雷德·塔斯基.md "wikilink")，美國籍波蘭裔猶太邏輯學家和數學家（[1983年去世](../Page/1983年.md "wikilink")）
  - [1905年](../Page/1905年.md "wikilink")：[福田赳夫](../Page/福田赳夫.md "wikilink")，第67任[日本內閣總理大臣](../Page/日本內閣總理大臣.md "wikilink")（[1995年去世](../Page/1995年.md "wikilink")）
  - [1911年](../Page/1911年.md "wikilink")：[黃友棣](../Page/黃友棣.md "wikilink")，[臺灣](../Page/臺灣.md "wikilink")[音樂家](../Page/音樂家.md "wikilink")（[2010年去世](../Page/2010年.md "wikilink")）
  - [1919年](../Page/1919年.md "wikilink")：[朱利奥·安德烈奥蒂](../Page/朱利奥·安德烈奥蒂.md "wikilink")，[義大利政治人物](../Page/義大利.md "wikilink")、作家和記者
  - [1919年](../Page/1919年.md "wikilink")：[安迪·魯尼](../Page/安迪·魯尼.md "wikilink")，美國資深傳媒工作者、幽默大師、時事評論員（[2011年去世](../Page/2011年.md "wikilink")）
  - [1925年](../Page/1925年.md "wikilink")：[三岛由纪夫](../Page/三岛由纪夫.md "wikilink")，日本小說家、劇作家、記者、電影製作人、電影演員、右翼思想家（[1970年去世](../Page/1970年.md "wikilink")）
  - [1933年](../Page/1933年.md "wikilink")：[斯坦·布拉哈格](../Page/斯坦·布拉哈格.md "wikilink")，美國非敘事實驗電影導演（[2003年去世](../Page/2003年.md "wikilink")）
  - [1938年](../Page/1938年.md "wikilink")：[細川護熙](../Page/細川護熙.md "wikilink")，日本第79任[日本内阁总理大臣](../Page/日本内阁总理大臣.md "wikilink")
  - [1941年](../Page/1941年.md "wikilink")：[費·唐娜薇](../Page/費·唐娜薇.md "wikilink")，美國電影演員，1976年獲[奧斯卡最佳女主角獎](../Page/奧斯卡最佳女主角獎.md "wikilink")
  - [1941年](../Page/1941年.md "wikilink")：[米蘭·庫昌](../Page/米蘭·庫昌.md "wikilink")，第一任[斯洛文尼亚总统](../Page/斯洛文尼亚总统.md "wikilink")
  - [1943年](../Page/1943年.md "wikilink")：[珊农·露茜德](../Page/珊农·露茜德.md "wikilink")，美国[太空人](../Page/太空人.md "wikilink")
  - [1943年](../Page/1943年.md "wikilink")：[马里斯·扬颂斯](../Page/马里斯·扬颂斯.md "wikilink")，[拉脱维亚](../Page/拉脱维亚.md "wikilink")[指挥家](../Page/指挥家.md "wikilink")
  - [1944年](../Page/1944年.md "wikilink")：[田中真紀子](../Page/田中真紀子.md "wikilink")，日本[政治家](../Page/政治家.md "wikilink")
  - [1946年](../Page/1946年.md "wikilink")：[哈羅德·希普曼](../Page/哈羅德·希普曼.md "wikilink")，英國家庭醫生、連環殺手（[2004年去世](../Page/2004年.md "wikilink")）
  - [1952年](../Page/1952年.md "wikilink")：[克林·波佩斯库-特里恰努](../Page/克林·波佩斯库-特里恰努.md "wikilink")，第60任[罗马尼亚总理](../Page/罗马尼亚总理.md "wikilink")
  - [1954年](../Page/1954年.md "wikilink")：[塞西莉亞·莫雷爾](../Page/塞西莉亞·莫雷爾.md "wikilink")，[智利第一夫人](../Page/智利.md "wikilink")
  - [1957年](../Page/1957年.md "wikilink")：[闵安琪](../Page/闵安琪.md "wikilink")，[美國華裔女作家](../Page/美國.md "wikilink")
  - [1959年](../Page/1959年.md "wikilink")：[吉田鋼太郎](../Page/吉田鋼太郎.md "wikilink")，[日本演員](../Page/日本.md "wikilink")
  - [1962年](../Page/1962年.md "wikilink")：[賀恩德](../Page/賀恩德.md "wikilink")，[英國駐香港及澳門總領事](../Page/英國駐香港及澳門總領事.md "wikilink")
  - [1963年](../Page/1963年.md "wikilink")：[史蒂文·索德伯格](../Page/史蒂文·索德伯格.md "wikilink")，美国[导演](../Page/导演.md "wikilink")、編劇
  - [1965年](../Page/1965年.md "wikilink")：[沙米尔·巴萨耶夫](../Page/沙米尔·巴萨耶夫.md "wikilink")，[車臣陸軍將領](../Page/車臣.md "wikilink")，車臣獨立運動領導人（[2006年去世](../Page/2006年.md "wikilink")）
  - [1968年](../Page/1968年.md "wikilink")：[伍佰](../Page/伍佰.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[摇滚乐](../Page/摇滚乐.md "wikilink")[歌手](../Page/歌手.md "wikilink")
  - [1968年](../Page/1968年.md "wikilink")：[LL Cool
    J](../Page/LL_Cool_J.md "wikilink")，[美國](../Page/美國.md "wikilink")[饒舌歌手](../Page/饒舌歌手.md "wikilink")
  - [1969年](../Page/1969年.md "wikilink")：[詹森·貝特曼](../Page/詹森·貝特曼.md "wikilink")，[美國演員](../Page/美國.md "wikilink")
  - [1969年](../Page/1969年.md "wikilink")：[戴夫·格羅爾](../Page/戴夫·格羅爾.md "wikilink")，[美國搖滾音樂家](../Page/美國.md "wikilink")、歌手、作曲人
  - [1971年](../Page/1971年.md "wikilink")：[安托尼奥斯·尼科波利迪斯](../Page/安托尼奥斯·尼科波利迪斯.md "wikilink")，[希臘足球運動員](../Page/希臘.md "wikilink")
  - [1973年](../Page/1973年.md "wikilink")：[詹卡洛·费斯切拉](../Page/詹卡洛·费斯切拉.md "wikilink")，[意大利](../Page/意大利.md "wikilink")[一级方程式赛车车手](../Page/一级方程式赛车.md "wikilink")
  - [1974年](../Page/1974年.md "wikilink")：[凱文·杜蘭](../Page/凱文·杜蘭.md "wikilink")，[加拿大演員](../Page/加拿大.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[纳拉因·卡蒂凯扬](../Page/纳拉因·卡蒂凯扬.md "wikilink")，[印度一级方程式赛车车手](../Page/印度.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[陳展鵬](../Page/陳展鵬.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [1978年](../Page/1978年.md "wikilink")：[肖恩·克劳福德](../Page/肖恩·克劳福德.md "wikilink")，美国[短跑](../Page/短跑.md "wikilink")[运动员](../Page/运动员.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[傅又宣](../Page/傅又宣.md "wikilink")，[香港歌手](../Page/香港.md "wikilink")
  - 1979年：[安琪拉·琳德沃](../Page/安琪拉·琳德沃.md "wikilink")，美國[超級名模](../Page/超級名模.md "wikilink")
  - 1979年：[凱倫·艾臣](../Page/凱倫·艾臣.md "wikilink")，[英國](../Page/英國.md "wikilink")[超級名模](../Page/超級名模.md "wikilink")、歌手
  - [1980年](../Page/1980年.md "wikilink")：[甲斐田裕子](../Page/甲斐田裕子.md "wikilink")，日本女性[聲優](../Page/聲優.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[玉木宏](../Page/玉木宏.md "wikilink")，[日本演員](../Page/日本.md "wikilink")、歌手
  - [1981年](../Page/1981年.md "wikilink")：[朴胤載](../Page/朴胤載.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[域陀·華迪斯](../Page/域陀·華迪斯.md "wikilink")，[西班牙](../Page/西班牙.md "wikilink")[足球](../Page/足球.md "wikilink")[守門員](../Page/守門員.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[上原多香子](../Page/上原多香子.md "wikilink")，[日本女歌手](../Page/日本.md "wikilink")、演員
  - [1983年](../Page/1983年.md "wikilink")：[塞薩爾·波沃](../Page/塞薩爾·波沃.md "wikilink")，[義大利足球運動員](../Page/義大利.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[艾瑞克·艾巴](../Page/艾瑞克·艾巴.md "wikilink")，[美國職棒大聯盟球員](../Page/美國.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[约翰·卡巴耶](../Page/约翰·卡巴耶.md "wikilink")，[法國足球運動員](../Page/法國.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[橋本淳](../Page/橋本淳.md "wikilink")，[日本演員](../Page/日本.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[杏樹紗奈](../Page/杏樹紗奈.md "wikilink")，[日本](../Page/日本.md "wikilink")[AV女優](../Page/AV女優.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[亞當·卡里頓](../Page/亞當·卡里頓.md "wikilink")，[英格蘭足球運動員](../Page/英格蘭.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[葛蘭·高斯汀](../Page/葛蘭·高斯汀.md "wikilink")，美國電視劇演員
  - [1992年](../Page/1992年.md "wikilink")：[罗比·布拉迪](../Page/罗比·布拉迪.md "wikilink")，[愛爾蘭足球運動員](../Page/愛爾蘭.md "wikilink")
  - [1992年](../Page/1992年.md "wikilink")：[妮慕·斯密](../Page/妮慕·斯密.md "wikilink")，[荷蘭超級名模](../Page/荷蘭.md "wikilink")
  - [1994年](../Page/1994年.md "wikilink")：[金鍾仁](../Page/金鍾仁.md "wikilink")，[韓國男子偶像團體](../Page/韓國.md "wikilink")[EXO成員](../Page/EXO.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink")：[托爾瑪琪夫雙胞胎姐妹](../Page/托爾瑪琪夫雙胞胎姐妹.md "wikilink")，[俄羅斯雙胞胎歌手組合](../Page/俄羅斯.md "wikilink")
  - [1998年](../Page/1998年.md "wikilink")：[森高愛](../Page/森高愛.md "wikilink")，日本女演員

## 逝世

  - [1224年](../Page/1224年.md "wikilink")：[金宣宗完颜珣](../Page/金宣宗.md "wikilink")，[金朝第八位皇帝](../Page/金朝.md "wikilink")（[1163年出生](../Page/1163年.md "wikilink")）
  - [1301年](../Page/1301年.md "wikilink")：[安德鲁三世](../Page/安德鲁三世.md "wikilink")，[匈牙利国王](../Page/匈牙利国王.md "wikilink")（约[1265年出生](../Page/1265年.md "wikilink")）
  - [1331年](../Page/1331年.md "wikilink")：[鄂多立克](../Page/鄂多立克.md "wikilink")，[意大利旅行家](../Page/意大利.md "wikilink")（约[1286年出生](../Page/1286年.md "wikilink")）
  - [1701年](../Page/1701年.md "wikilink")：[德川光國](../Page/德川光國.md "wikilink")，[日本](../Page/日本.md "wikilink")[江戶時代的](../Page/江戶時代.md "wikilink")[大名](../Page/大名.md "wikilink")（[1628年出生](../Page/1628年.md "wikilink")）
  - [1742年](../Page/1742年.md "wikilink")：[爱德蒙·哈雷](../Page/爱德蒙·哈雷.md "wikilink")，[英国天文学家](../Page/英国.md "wikilink")（[1656年出生](../Page/1656年.md "wikilink")）
  - [1753年](../Page/1753年.md "wikilink")：[乔治·贝克莱](../Page/乔治·贝克莱.md "wikilink")，[爱尔兰](../Page/爱尔兰.md "wikilink")[哲學家](../Page/哲學家.md "wikilink")（[1685年出生](../Page/1685年.md "wikilink")）
  - [1867年](../Page/1867年.md "wikilink")：[让·奥古斯特·多米尼克·安格尔](../Page/让·奥古斯特·多米尼克·安格尔.md "wikilink")，[法国](../Page/法国.md "wikilink")[新古典主义画派](../Page/新古典主义画派.md "wikilink")[画家](../Page/画家.md "wikilink")（[1780年出生](../Page/1780年.md "wikilink")）
  - [1898年](../Page/1898年.md "wikilink")：[路易斯·卡羅](../Page/路易斯·卡羅.md "wikilink")，英國作家（[1832年出生](../Page/1832年.md "wikilink")）
  - [1901年](../Page/1901年.md "wikilink")：[夏爾·埃爾米特](../Page/夏爾·埃爾米特.md "wikilink")，法国[数学家](../Page/数学家.md "wikilink")（[1822年出生](../Page/1822年.md "wikilink")）
  - [1905年](../Page/1905年.md "wikilink")：[恩斯特·阿贝](../Page/恩斯特·阿贝.md "wikilink")，[德国](../Page/德国.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")（[1840年出生](../Page/1840年.md "wikilink")）
  - [1953年](../Page/1953年.md "wikilink")：[鳥居龍藏](../Page/鳥居龍藏.md "wikilink")，[日本人類學家](../Page/日本.md "wikilink")（[1870年出生](../Page/1870年.md "wikilink")）
  - [1957年](../Page/1957年.md "wikilink")：[亨弗莱·鲍嘉](../Page/亨弗莱·鲍嘉.md "wikilink")，[美国](../Page/美国.md "wikilink")[演员](../Page/演员.md "wikilink")（[1899年出生](../Page/1899年.md "wikilink")）
  - [1970年](../Page/1970年.md "wikilink")：[威廉·费勒](../Page/威廉·费勒.md "wikilink")，美国数学家（[1907年出生](../Page/1907年.md "wikilink")）
  - [1972年](../Page/1972年.md "wikilink")：[弗雷德里克九世](../Page/弗雷德里克九世.md "wikilink")，[丹麦国王](../Page/丹麦国王.md "wikilink")（[1899年出生](../Page/1899年.md "wikilink")）
  - [1977年](../Page/1977年.md "wikilink")：[安東尼·艾登](../Page/安東尼·艾登.md "wikilink")，英国[政治家](../Page/政治家.md "wikilink")，曾任[英國首相](../Page/英國首相.md "wikilink")（[1897年出生](../Page/1897年.md "wikilink")）
  - 1977年：[彼得·芬奇](../Page/彼得·芬奇.md "wikilink")，美国[演员](../Page/演员.md "wikilink")（[1916年出生](../Page/1916年.md "wikilink")）
  - [1978年](../Page/1978年.md "wikilink")：[库尔特·哥德尔](../Page/库尔特·哥德尔.md "wikilink")，[奥地利数学家](../Page/奥地利.md "wikilink")、逻辑学家（[1906年出生](../Page/1906年.md "wikilink")）
  - [1984年](../Page/1984年.md "wikilink")；[梅爾·羅賓斯](../Page/梅爾·羅賓斯.md "wikilink")，美国理发师、紙牌遊戲[UNO的發明者](../Page/UNO.md "wikilink")（[1912年出生](../Page/1912年.md "wikilink")）
  - [1988年](../Page/1988年.md "wikilink")：[格奥尔基·马克西米利安诺维奇·马林科夫](../Page/格奥尔基·马克西米利安诺维奇·马林科夫.md "wikilink")，[苏联政治家](../Page/苏联.md "wikilink")，曾任苏联总理（[1902年出生](../Page/1902年.md "wikilink")）
  - [1997年](../Page/1997年.md "wikilink")：[胡金銓](../Page/胡金銓.md "wikilink")，中国電影導演（[1932年出生](../Page/1932年.md "wikilink")）
  - 2010年：[羅海星](../Page/羅海星.md "wikilink")，[香港民主運動支持者](../Page/香港.md "wikilink")
  - [2011年](../Page/2011年.md "wikilink")：[刘华清](../Page/刘华清.md "wikilink")，第十四届[中共中央政治局常委](../Page/中共中央政治局常委.md "wikilink")、[中央军委副主席](../Page/中華人民共和國中央軍事委員會副主席.md "wikilink")、[中国人民解放军上将](../Page/中国人民解放军.md "wikilink")，[中国航母之父](../Page/中国.md "wikilink")（[1916年出生](../Page/1916年.md "wikilink")）
  - [2015年](../Page/2015年.md "wikilink")：[张万年](../Page/张万年.md "wikilink")，
    第十五届[中共中央政治局委员](../Page/中共中央政治局委员.md "wikilink")、[中央军委副主席](../Page/中華人民共和國中央軍事委員會副主席.md "wikilink")、[中国人民解放军上将](../Page/中国人民解放军.md "wikilink")（[1928年出生](../Page/1928年.md "wikilink")）
  - [2016年](../Page/2016年.md "wikilink")：[艾倫·瑞克曼](../Page/艾倫·瑞克曼.md "wikilink")，
    英國電視劇、電影演員、飾演《[哈利波特](../Page/哈利波特.md "wikilink")》系列中神祕陰沉的[賽佛勒斯·石內卜教授](../Page/賽佛勒斯·石內卜.md "wikilink")（[1946年出生](../Page/1946年.md "wikilink")）
  - [2016年](../Page/2016年.md "wikilink")：[吉岡昌仁](../Page/吉岡昌仁.md "wikilink")，[日本电视动画](../Page/日本.md "wikilink")《[名侦探柯南](../Page/名偵探柯南_\(動畫\).md "wikilink")》製作人（[1961年出生](../Page/1961年.md "wikilink")）

## 节假日和习俗

  - [儒略曆新年](../Page/儒略曆.md "wikilink")

  - ：[醫檢師節](../Page/醫檢師.md "wikilink")

  - ：[日記情人節](../Page/日記情人節.md "wikilink")

## 参考资料

1.  [1](http://www.deltawerken.com/Zuider-Zee-flood-\(1916\)/306.html)，Zuyderzee
    Flood (1916)。
2.