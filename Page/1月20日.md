**1月20日**是[阳历年的第](../Page/阳历.md "wikilink")20天，离一年的结束还有345天（[闰年是](../Page/闰年.md "wikilink")346天）。

## 大事记

### 3世紀

  - [250年](../Page/250年.md "wikilink")：[羅馬帝國皇帝](../Page/羅馬帝國皇帝.md "wikilink")[德西烏斯開始大規模迫害基督教徒](../Page/德西烏斯.md "wikilink")，[教宗法比盎因而殉教](../Page/教宗法比盎.md "wikilink")。

### 13世紀

  - [1265年](../Page/1265年.md "wikilink")：[英格兰贵族](../Page/英格兰.md "wikilink")[孟福爾召集第一届英格兰](../Page/西蒙·德·蒙福爾，第六世萊斯特伯爵.md "wikilink")[议会在](../Page/议会.md "wikilink")[威斯敏斯特宫召开第一次会议](../Page/威斯敏斯特宫.md "wikilink")。

### 14世紀

  - [1320年](../Page/1320年.md "wikilink")：[瓦迪斯瓦夫一世在](../Page/瓦迪斯瓦夫一世.md "wikilink")[克拉科夫加冕成为](../Page/克拉科夫.md "wikilink")[波兰国王](../Page/波兰国王.md "wikilink")，[波兰重新统一](../Page/波兰.md "wikilink")。
  - [1356年](../Page/1356年.md "wikilink")：蘇格蘭國王[愛德華·巴里奧](../Page/愛德華·巴里奧.md "wikilink")[遜位](../Page/遜位.md "wikilink")。

### 16世紀

  - [1523年](../Page/1523年.md "wikilink")：丹麥與挪威國王[克里斯蒂安二世被迫遜位](../Page/克里斯蒂安二世.md "wikilink")。

### 19世紀

  - [1841年](../Page/1841年.md "wikilink")：[香港島遭英軍控制](../Page/香港島.md "wikilink")。
  - [1865年](../Page/1865年.md "wikilink")：[美國发明家](../Page/美國.md "wikilink")成为第一个注册[雲霄飛車](../Page/雲霄飛車.md "wikilink")[专利技术的人](../Page/专利.md "wikilink")。
  - [1892年](../Page/1892年.md "wikilink")：美国[马萨诸塞州](../Page/马萨诸塞州.md "wikilink")[斯普林菲尔德](../Page/斯普林菲尔德_\(马萨诸塞州\).md "wikilink")[基督教青年会举办了世界首次正式的](../Page/基督教青年会.md "wikilink")[篮球比赛](../Page/篮球.md "wikilink")。
  - [1895年](../Page/1895年.md "wikilink")：[香港天文台總部錄得最高氣溫](../Page/香港天文台總部.md "wikilink")、日平均氣溫、最低氣溫分別為11.1℃、8.9℃、7.2℃，是有紀錄以來最寒冷的「大寒」。\[1\]

### 20世紀

  - [1924年](../Page/1924年.md "wikilink")：[中國國民黨第一次全國代表大會在](../Page/中國國民黨第一次全國代表大會.md "wikilink")[广州](../Page/广州.md "wikilink")[国立广东高等师范学校的礼堂上开幕](../Page/国立广东高等师范学校.md "wikilink")，會在通過[聯俄容共政策](../Page/聯俄容共.md "wikilink")。
  - [1934年](../Page/1934年.md "wikilink")：攝影及電器製作公司[富士軟片在](../Page/富士軟片.md "wikilink")[日本](../Page/日本.md "wikilink")[東京成立](../Page/東京.md "wikilink")。
  - [1936年](../Page/1936年.md "wikilink")：[愛德華八世繼位成為](../Page/愛德華八世_\(英國\).md "wikilink")[英國國王](../Page/英國國王.md "wikilink")。
  - [1937年](../Page/1937年.md "wikilink")：[小羅斯福二度宣誓就任](../Page/富蘭克林·德拉諾·羅斯福.md "wikilink")[美國總統](../Page/美國總統.md "wikilink")，確立美國總統就職日期，之前美國總統就職日期為[3月4日](../Page/3月4日.md "wikilink")。
  - [1937年](../Page/1937年.md "wikilink")：[中国人民抗日军政大学](../Page/抗日军政大学.md "wikilink")（简称抗大）在[延安成立](../Page/延安.md "wikilink")。
  - [1941年](../Page/1941年.md "wikilink")：[中国共产党重建](../Page/中国共产党.md "wikilink")[新四军军部](../Page/新四军.md "wikilink")。
  - [1942年](../Page/1942年.md "wikilink")：[納粹德國官员在](../Page/納粹德國.md "wikilink")[柏林召开讨论](../Page/柏林.md "wikilink")「猶太人問題最後解決方法」的[万湖会议](../Page/万湖会议.md "wikilink")，决定进行[猶太人大屠殺](../Page/猶太人大屠殺.md "wikilink")。
  - [1946年](../Page/1946年.md "wikilink")：在[法兰西第四共和国的行政机构与宪法内容获得通过后](../Page/法兰西第四共和国.md "wikilink")，[夏尔·戴高乐宣布辞去](../Page/夏尔·戴高乐.md "wikilink")[法兰西共和国临时政府主席一职](../Page/法兰西共和国临时政府.md "wikilink")。
  - [1949年](../Page/1949年.md "wikilink")：[美国总统](../Page/美国总统.md "wikilink")[杜鲁门在就职演说中](../Page/杜鲁门.md "wikilink")，提出美国全球战略的四点行动计划，就是[第四点计划](../Page/第四点计划.md "wikilink")。
  - 1949年：[傅斯年出任](../Page/傅斯年.md "wikilink")[臺灣大學校長](../Page/國立臺灣大學.md "wikilink")。
  - [1955年](../Page/1955年.md "wikilink")：[中华人民共和国与](../Page/中华人民共和国.md "wikilink")[阿富汗建立](../Page/阿富汗.md "wikilink")[外交关系](../Page/外交关系.md "wikilink")。
  - 1955年：中華人民共和國軍隊攻佔由国民党占领的[一江山島](../Page/一江山島.md "wikilink")，一江山守軍指揮官[王生明陣亡](../Page/王生明.md "wikilink")。
  - [1960年](../Page/1960年.md "wikilink")：[亨德里克·弗倫施·維沃爾德宣傳將舉辦](../Page/亨德里克·弗倫施·維沃爾德.md "wikilink")[公民投票決定](../Page/公民投票.md "wikilink")[南非是否要成為](../Page/南非.md "wikilink")[共和國](../Page/共和國.md "wikilink")。
  - [1962年](../Page/1962年.md "wikilink")：[中華民國外交部聲明和西非](../Page/中華民國外交部.md "wikilink")[達荷美共和國建交](../Page/達荷美共和國.md "wikilink")。
  - [1969年](../Page/1969年.md "wikilink")：[船灣淡水湖由](../Page/船灣淡水湖.md "wikilink")[港督](../Page/港督.md "wikilink")[戴麟趾揭幕](../Page/戴麟趾.md "wikilink")。
  - 1969年：[東巴基斯坦警察殺死學運份子](../Page/東巴基斯坦.md "wikilink")，為引起[孟加拉國解放戰爭的一部分原因](../Page/孟加拉國解放戰爭.md "wikilink")。
  - 1969年：[香港天文台總部錄得最高氣溫](../Page/香港天文台總部.md "wikilink")26.4℃，日平均氣溫23.2℃，是有紀錄以來最高溫的「大寒」。\[2\]
  - [1972年](../Page/1972年.md "wikilink")：[石油輸出國組織](../Page/石油輸出國組織.md "wikilink")（OPEC）與世界石油公司集團成立協定，石油原油依公示價格提高8.49%。
  - [1974年](../Page/1974年.md "wikilink")：[西沙之战](../Page/西沙之战.md "wikilink")，[中国人民解放军海军驱逐](../Page/中国人民解放军海军.md "wikilink")[南越势力](../Page/越南共和國.md "wikilink")，控制[西沙群岛](../Page/西沙群岛.md "wikilink")。
  - [1981年](../Page/1981年.md "wikilink")：[雷根宣誓就任](../Page/隆納·雷根.md "wikilink")[美國總統](../Page/美國總統.md "wikilink")，四年後又再次連任。
  - 1981年：[伊朗人質危機](../Page/伊朗人質危機.md "wikilink")：被[伊朗槍手綁架十四個月的](../Page/伊朗.md "wikilink")[美國人質獲釋](../Page/美國.md "wikilink")。
  - [1983年](../Page/1983年.md "wikilink")：[金石文化廣場在](../Page/金石堂書店.md "wikilink")[台北市開幕](../Page/台北市.md "wikilink")。
  - [1986年](../Page/1986年.md "wikilink")：[馬丁·路德·金紀念日首次成為美國法定假日](../Page/馬丁·路德·金紀念日.md "wikilink")。
  - [1990年](../Page/1990年.md "wikilink")：[蘇聯共產黨中央委員會總書記](../Page/蘇聯共產黨中央委員會總書記.md "wikilink")[戈巴契夫宣布派遣軍隊攻擊占領](../Page/戈巴契夫.md "wikilink")[亞塞拜然巴庫地區的群眾](../Page/亞塞拜然.md "wikilink")。
  - [1992年](../Page/1992年.md "wikilink")：[中華人民共和國與](../Page/中華人民共和國.md "wikilink")[白俄羅斯建交](../Page/白俄羅斯.md "wikilink")。
  - 1992年：[因特航空148號班機在](../Page/因特航空148號班機空難.md "wikilink")[法國](../Page/法國.md "wikilink")[史特拉斯堡附近墜機](../Page/史特拉斯堡.md "wikilink")，造成82名乘客及5名機組員身亡。
  - [1996年](../Page/1996年.md "wikilink")：[巴勒斯坦民族权力机构成立](../Page/巴勒斯坦民族权力机构.md "wikilink")。
  - 1996年：[中華民國](../Page/中華民國.md "wikilink")[台南科學園區於台南新市動土](../Page/南部科學工業園區.md "wikilink")，開發成本達320億，第一期規劃為半導體、微電子精密機械與農業生物技術3專業區。
  - 1996年：[中華民國外交部長](../Page/中華民國外交部.md "wikilink")[錢復訪問巴哈馬](../Page/錢復_\(民國\).md "wikilink")，過境美國時二度發表演講。
  - [1998年](../Page/1998年.md "wikilink")：第八世[夏茸尕布活佛转世灵童认定](../Page/夏茸尕布活佛.md "wikilink")。

### 21世紀

  - [2001年](../Page/2001年.md "wikilink")：[菲律賓總統](../Page/菲律賓總統.md "wikilink")[約瑟夫·埃斯特拉達在](../Page/約瑟夫·埃斯特拉達.md "wikilink")[第二次人民力量革命中辭職下台](../Page/人民力量革命_\(2001年\).md "wikilink")，由副總統[格洛丽亚·阿罗约繼任](../Page/格洛麗亞·馬卡帕加爾·阿羅約.md "wikilink")。
  - [2001年](../Page/2001年.md "wikilink")：[小布什就任第](../Page/小布什.md "wikilink")43任美国总统。
  - [2009年](../Page/2009年.md "wikilink")：第一个[非裔总统](../Page/非裔美国人.md "wikilink")[贝拉克·奥巴马](../Page/贝拉克·奥巴马.md "wikilink")[宣誓就任第](../Page/2009年美國總統就職典禮.md "wikilink")44任美国总统。
  - [2017年](../Page/2017年.md "wikilink")：[唐納·川普](../Page/唐納·川普.md "wikilink")[宣誓就任第](../Page/2017年美國總統就職典禮.md "wikilink")45任美国总统。
  - [2019年](../Page/2019年.md "wikilink")：[香港](../Page/香港.md "wikilink")[中環灣仔繞道首階段通車](../Page/中環灣仔繞道.md "wikilink")。
  - [2019年](../Page/2019年.md "wikilink")：[香港](../Page/香港.md "wikilink")[西九文化區](../Page/西九文化區.md "wikilink")[戲曲中心開幕](../Page/戲曲中心.md "wikilink")。
  - [2019年](../Page/2019年.md "wikilink")：[香港天文台總部錄得最高氣溫](../Page/香港天文台總部.md "wikilink")23.4度，成為繼1969年「大寒」（錄得26.4度）、半世紀以來最高溫「大寒」，也是香港天文台有記錄以來第四高溫的「大寒」。\<ref
    name"winterwarm"\>[50年來最熱「大寒」　天文台下午錄得23.4度　預測周二急跌至12度，2019-01-20，香港01](https://www.hk01.com/%E7%A4%BE%E6%9C%83%E6%96%B0%E8%81%9E/285221/50%E5%B9%B4%E4%BE%86%E6%9C%80%E7%86%B1-%E5%A4%A7%E5%AF%92-%E5%A4%A9%E6%96%87%E5%8F%B0%E4%B8%8B%E5%8D%88%E9%8C%84%E5%BE%9723-4%E5%BA%A6-%E9%A0%90%E6%B8%AC%E5%91%A8%E4%BA%8C%E6%80%A5%E8%B7%8C%E8%87%B312%E5%BA%A6)</ref>

## 出生

  - [225年](../Page/225年.md "wikilink")：[戈爾迪安三世](../Page/戈爾迪安三世.md "wikilink")，羅馬皇帝（逝於[244年](../Page/244年.md "wikilink")）
  - [772年](../Page/772年.md "wikilink")：[白居易](../Page/白居易.md "wikilink")，[唐朝](../Page/唐朝.md "wikilink")[詩人](../Page/詩人.md "wikilink")（逝於[846年](../Page/846年.md "wikilink")）
  - [1425年](../Page/1425年.md "wikilink")：[足利義政](../Page/足利義政.md "wikilink")，[室町幕府第](../Page/室町幕府.md "wikilink")8代[征夷大將軍](../Page/征夷大將軍.md "wikilink")（逝於[1490年](../Page/1490年.md "wikilink")）
  - [1554年](../Page/1554年.md "wikilink")：[塞巴斯蒂昂一世](../Page/塞巴斯蒂昂_\(葡萄牙\).md "wikilink")，[葡萄牙國王](../Page/葡萄牙.md "wikilink")（逝於[1578年](../Page/1578年.md "wikilink")）
  - [1920年](../Page/1920年.md "wikilink")：[费德里柯·费里尼](../Page/费德里柯·费里尼.md "wikilink")，[意大利電影導演](../Page/意大利.md "wikilink")（逝於[1993年](../Page/1993年.md "wikilink")）
  - [1931年](../Page/1931年.md "wikilink")：[有吉佐和子](../Page/有吉佐和子.md "wikilink")，[日本小說家](../Page/日本.md "wikilink")（逝於[1984年](../Page/1984年.md "wikilink")）
  - [1933年](../Page/1933年.md "wikilink")：[林家聲](../Page/林家聲.md "wikilink")，[香港](../Page/香港.md "wikilink")[粵劇演員](../Page/粵劇.md "wikilink")（逝於[2015年](../Page/2015年.md "wikilink")）
  - [1946年](../Page/1946年.md "wikilink")：[大卫·林奇](../Page/大卫·林奇.md "wikilink")，美國電影導演
  - [1949年](../Page/1949年.md "wikilink")：[梁家仁](../Page/梁家仁.md "wikilink")，[香港武打演員](../Page/香港.md "wikilink")
  - [1950年](../Page/1950年.md "wikilink")：[斯琴高娃](../Page/斯琴高娃.md "wikilink")，[中國演員](../Page/中國.md "wikilink")
  - [1960年](../Page/1960年.md "wikilink")：[威爾·萊特](../Page/威爾·萊特.md "wikilink")，[美國电子游戏设计师](../Page/美國.md "wikilink")
  - [1965年](../Page/1965年.md "wikilink")：[蘇菲·惠斯-鍾斯](../Page/蘇菲·惠斯-鍾斯_\(威塞克斯伯爵夫人\).md "wikilink")，[英國](../Page/英國.md "wikilink")[威塞克斯伯爵夫人](../Page/威塞克斯伯爵夫人蘇菲.md "wikilink")
  - [1971年](../Page/1971年.md "wikilink")：[蓋瑞·巴洛](../Page/蓋瑞·巴洛.md "wikilink")，[英國歌手](../Page/英國.md "wikilink")、乐队[接招合唱團成員](../Page/接招.md "wikilink")
  - [1973年](../Page/1973年.md "wikilink")：[瑪蒂爾德](../Page/瑪蒂爾德_\(比利時王后\).md "wikilink")，[比利时王妃](../Page/比利时.md "wikilink")
  - [1974年](../Page/1974年.md "wikilink")：[羅倫士](../Page/羅倫士.md "wikilink")，[香港足球運動員](../Page/香港.md "wikilink")
  - [1976年](../Page/1976年.md "wikilink")：[滕麗名](../Page/滕麗名.md "wikilink")，香港演员
  - [1976年](../Page/1976年.md "wikilink")：[袁彩雲](../Page/袁彩雲.md "wikilink")，香港艺人
  - [1977年](../Page/1977年.md "wikilink")：[杜浚斌](../Page/杜浚斌.md "wikilink")，香港電台DJ
  - [1980年](../Page/1980年.md "wikilink")：[金楨勳](../Page/金楨勳.md "wikilink")，[韓國](../Page/韓國.md "wikilink")[歌手](../Page/歌手.md "wikilink")，[演員](../Page/演員.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[張栩](../Page/張栩.md "wikilink")，台灣旅日棋士
  - [1980年](../Page/1980年.md "wikilink")：[謝怡芬](../Page/謝怡芬.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[主持人](../Page/主持人.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[朱世赫](../Page/朱世赫.md "wikilink")，韓國桌球選手
  - [1983年](../Page/1983年.md "wikilink")：[羅平](../Page/羅平_\(模特兒\).md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[矢口真里](../Page/矢口真里.md "wikilink")，日本歌手、演员
  - [1985年](../Page/1985年.md "wikilink")：[井上麻里奈](../Page/井上麻里奈.md "wikilink")，日本女性[聲優](../Page/聲優.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[卓文萱](../Page/卓文萱.md "wikilink")，[台灣女歌手](../Page/台灣.md "wikilink")、演員
  - [1986年](../Page/1986年.md "wikilink")：[竹內由惠](../Page/竹內由惠.md "wikilink")，日本主持人
  - [1994年](../Page/1994年.md "wikilink")：[盧卡斯·比亞桑](../Page/盧卡斯·皮亞松.md "wikilink")，[巴西](../Page/巴西.md "wikilink")[足球運動員](../Page/足球.md "wikilink")
  - [1995年](../Page/1995年.md "wikilink")：[金昭希](../Page/金昭希.md "wikilink")，[韓國著名的女子短道速滑運動員](../Page/韓國.md "wikilink")

## 逝世

  - [619年](../Page/619年.md "wikilink")：[李密在](../Page/李密_\(隋\).md "wikilink")[熊耳山中](../Page/熊耳山.md "wikilink")[盛彥師埋伏被殺](../Page/盛彥師.md "wikilink")。（生於[582年](../Page/582年.md "wikilink")）
  - [1189年](../Page/1189年.md "wikilink")：[金世宗完顏雍](../Page/金世宗.md "wikilink")，[金朝第五位皇帝](../Page/金朝.md "wikilink")（生於[1123年](../Page/1123年.md "wikilink")）
  - [1745年](../Page/1745年.md "wikilink")：[查理七世](../Page/查理七世_\(神圣罗马帝国\).md "wikilink")，神圣罗马帝国皇帝（生於[1697年](../Page/1697年.md "wikilink")）
  - [1875年](../Page/1875年.md "wikilink")：[讓-弗朗索瓦·米勒](../Page/讓-弗朗索瓦·米勒.md "wikilink")，法國[巴比松派畫家](../Page/巴比松派.md "wikilink")（生於[1814年](../Page/1814年.md "wikilink")）
  - [1891年](../Page/1891年.md "wikilink")：[卡拉卡瓦](../Page/卡拉卡瓦.md "wikilink")，夏威夷王國國王（生於[1836年](../Page/1836年.md "wikilink")）
  - [1936年](../Page/1936年.md "wikilink")：[乔治五世](../Page/乔治五世_\(英国\).md "wikilink")，[英國國王](../Page/英國國王.md "wikilink")（生於[1865年](../Page/1865年.md "wikilink")）
  - [1938年](../Page/1938年.md "wikilink")：[劉湘](../Page/劉湘.md "wikilink")，[川軍將領](../Page/川軍.md "wikilink")，因胃病在[漢口去世](../Page/漢口.md "wikilink")（生於[1890年](../Page/1890年.md "wikilink")）
  - [1955年](../Page/1955年.md "wikilink")：[王生明](../Page/王生明.md "wikilink")，[中華民國將軍](../Page/中華民國.md "wikilink")（生於[1910年](../Page/1910年.md "wikilink")）
  - [1983年](../Page/1983年.md "wikilink")：井上优，[日本的中国语學家](../Page/日本.md "wikilink")\[3\]
  - [1990年](../Page/1990年.md "wikilink")：[東久邇宮稔彦王](../Page/東久邇宮稔彦王.md "wikilink")，日本皇族，第43任[日本首相](../Page/日本首相.md "wikilink")（生於[1887年](../Page/1887年.md "wikilink")）
  - [1990年](../Page/1990年.md "wikilink")：[芭芭拉·斯坦威克](../Page/芭芭拉·斯坦威克.md "wikilink")，演员（生於[1907年](../Page/1907年.md "wikilink")）
  - [1993年](../Page/1993年.md "wikilink")：[奥黛丽·赫本](../Page/奥黛丽·赫本.md "wikilink")，演员（生於[1929年](../Page/1929年.md "wikilink")）
  - [1994年](../Page/1994年.md "wikilink")：[马特·巴斯比爵士](../Page/马特·巴斯比.md "wikilink")，[曼徹斯特聯传奇主帅](../Page/曼徹斯特聯.md "wikilink")（生於[1909年](../Page/1909年.md "wikilink")）
  - [2014年](../Page/2014年.md "wikilink")：[克劳迪奥·阿巴多](../Page/克劳迪奥·阿巴多.md "wikilink")，指挥家（生於[1933年](../Page/1933年.md "wikilink")）
  - [2016年](../Page/2016年.md "wikilink")：[張榮發](../Page/張榮發.md "wikilink")，臺灣企業家、[長榮集團總裁](../Page/長榮集團.md "wikilink")（生於[1927年](../Page/1927年.md "wikilink")）\[4\]

## 节假日和习俗

  - [总统就职典礼](../Page/美国总统就职典礼.md "wikilink")（自1937年於该日举行）

  - [國際啾啾日](../Page/國際啾啾日.md "wikilink")

## 參考文獻

<references/>

1.
2.
3.

4.