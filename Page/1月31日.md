**1月31日**是[阳历年的第](../Page/阳历.md "wikilink")31天，离一年的结束还有334天（[闰年是](../Page/闰年.md "wikilink")335天）。

## 大事记

### 17世紀

  - [1606年](../Page/1606年.md "wikilink")：参与[火药阴谋企图](../Page/火药阴谋.md "wikilink")[刺杀](../Page/刺杀.md "wikilink")[英格兰国王](../Page/英格兰国王.md "wikilink")[詹姆士一世的](../Page/詹姆士一世_\(英格兰\).md "wikilink")[天主教教徒](../Page/天主教.md "wikilink")[盖伊·福克斯被处死](../Page/盖伊·福克斯.md "wikilink")。

### 20世紀

  - [1917年](../Page/1917年.md "wikilink")：[德国宣布重新开始](../Page/德国.md "wikilink")[无限制潜艇战](../Page/无限制潜艇战.md "wikilink")，用[潜艇击沉驶进](../Page/潜艇.md "wikilink")[英国等地海域的船只](../Page/英国.md "wikilink")。
  - [1942年](../Page/1942年.md "wikilink")：[日军进入](../Page/日军.md "wikilink")[马尼拉](../Page/马尼拉.md "wikilink")。
  - [1943年](../Page/1943年.md "wikilink")：[德意志国防军在](../Page/德意志国防军.md "wikilink")[史達林格勒投降](../Page/史達林格勒.md "wikilink")。
  - [1946年](../Page/1946年.md "wikilink")：[南斯拉夫通过](../Page/南斯拉夫.md "wikilink")[宪法](../Page/宪法.md "wikilink")，确定由[塞尔维亚](../Page/塞尔维亚.md "wikilink")、[黑山](../Page/黑山.md "wikilink")、[斯洛文尼亞](../Page/斯洛文尼亞.md "wikilink")、[克羅地亞](../Page/克羅地亞.md "wikilink")、[波黑](../Page/波黑.md "wikilink")、[马其顿](../Page/马其顿.md "wikilink")6个[共和国组成的](../Page/共和国.md "wikilink")[联邦制](../Page/联邦制.md "wikilink")。
  - 1946年：中国[政治协商会议通过五项决议后闭幕](../Page/政治协商会议.md "wikilink")。
  - [1949年](../Page/1949年.md "wikilink")：在[中国国民党](../Page/中国国民党.md "wikilink")[北平守军将领](../Page/北平.md "wikilink")[傅作义投诚之后](../Page/傅作义.md "wikilink")，[中国人民解放军](../Page/中国人民解放军.md "wikilink")[和平进入北平城](../Page/北平和平解放.md "wikilink")，[平津战役结束](../Page/平津战役.md "wikilink")。
  - [1955年](../Page/1955年.md "wikilink")：[中国卫生部提出适当节制生育](../Page/中国卫生部.md "wikilink")。
  - [1956年](../Page/1956年.md "wikilink")：《[漢字簡化方案](../Page/漢字簡化方案.md "wikilink")》及[國務院推行簡化字決議在](../Page/中國國務院.md "wikilink")《[人民日報](../Page/人民日報.md "wikilink")》上公佈，翌日在全國推行第一批簡化字，總共分四批推行方案中絕大多數簡化字。
  - [1958年](../Page/1958年.md "wikilink")：[美國第一颗](../Page/美國.md "wikilink")[人造卫星](../Page/人造卫星.md "wikilink")[探险者一号在](../Page/探险者一号.md "wikilink")[佛罗里达州](../Page/佛罗里达州.md "wikilink")[卡纳维拉尔角发射升空](../Page/卡纳维拉尔角.md "wikilink")。
  - [1966年](../Page/1966年.md "wikilink")：[苏联第一颗在月球上成功实现软着陆的探测器](../Page/苏联.md "wikilink")[月球9号在](../Page/月球9号.md "wikilink")[拜科努尔发射场发射升空](../Page/拜科努尔发射场.md "wikilink")。
  - [1968年](../Page/1968年.md "wikilink")：[諾魯共和國独立](../Page/諾魯.md "wikilink")。
  - 1968年：[美國駐西貢大使館遭到](../Page/美國駐西貢大使館.md "wikilink")[越南民族解放陣線](../Page/越南民族解放陣線.md "wikilink")（越共）夜襲，18名越南敢死隊員及[美陸軍](../Page/美國陸軍.md "wikilink")[憲兵](../Page/憲兵.md "wikilink")4人、[守館海軍陸戰隊衛兵](../Page/美國海軍陸戰隊使館警衛大隊.md "wikilink")1人在激戰一夜中死亡。
  - [1972年](../Page/1972年.md "wikilink")：[中国与](../Page/中華人民共和國.md "wikilink")[馬爾他建交](../Page/馬爾他.md "wikilink")\[1\]。
  - [1974年](../Page/1974年.md "wikilink")：[日本赤軍發動](../Page/日本赤軍.md "wikilink")[拉裕事件](../Page/拉裕事件.md "wikilink")，在[新加坡劫持一艘](../Page/新加坡.md "wikilink")[渡輪](../Page/渡輪.md "wikilink")。
  - [1975年](../Page/1975年.md "wikilink")：[非洲國家](../Page/非洲.md "wikilink")[安哥拉脫離](../Page/安哥拉.md "wikilink")[葡萄牙殖民統治](../Page/葡萄牙.md "wikilink")，取得獨立。
  - [1981年](../Page/1981年.md "wikilink")：被[伊朗扣压的](../Page/伊朗.md "wikilink")[52名人质回到](../Page/伊朗人質事件.md "wikilink")[美国](../Page/美国.md "wikilink")。
  - [2000年](../Page/2000年.md "wikilink")：[阿拉斯加航空261號班機空難](../Page/阿拉斯加航空261號班機空難.md "wikilink")。

### 21世紀

  - [2001年](../Page/2001年.md "wikilink")：[2001年日本航空班機空中接近事故](../Page/2001年日本航空班機空中接近事故.md "wikilink")。

  -
  - [2013年](../Page/2013年.md "wikilink")：加拿大華裔女孩藍可兒（Elisa
    Lam）[離奇失蹤](../Page/藍可兒死亡事件.md "wikilink")。

## 出生

  - [1530年](../Page/1530年.md "wikilink")：[大友宗麟](../Page/大友宗麟.md "wikilink")，[日本戰國時代](../Page/戰國_\(日本\).md "wikilink")[九州的大名](../Page/九州.md "wikilink")。（[1587年逝世](../Page/1587年.md "wikilink")）
  - [1543年](../Page/1543年.md "wikilink")：[德川家康](../Page/德川家康.md "wikilink")，[日本](../Page/日本.md "wikilink")[江戶幕府第一任](../Page/江戶幕府.md "wikilink")[征夷大將軍](../Page/征夷大將軍.md "wikilink")。（[1616年逝世](../Page/1616年.md "wikilink")）
  - [1797年](../Page/1797年.md "wikilink")：[弗朗茨·舒伯特](../Page/弗朗茨·舒伯特.md "wikilink")，[奧地利](../Page/奧地利.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")。（[1828年逝世](../Page/1828年.md "wikilink")）
  - [1868年](../Page/1868年.md "wikilink")：[西奧多·威廉·理查茲](../Page/西奧多·威廉·理查茲.md "wikilink")，[美國](../Page/美國.md "wikilink")[化學家](../Page/化學家.md "wikilink")。（[1928年逝世](../Page/1928年.md "wikilink")）
  - [1919年](../Page/1919年.md "wikilink")：[傑基·羅賓森](../Page/傑基·羅賓森.md "wikilink")，[美國黑人](../Page/美國.md "wikilink")[棒球](../Page/棒球.md "wikilink")[球員](../Page/球員.md "wikilink")。（[1972年逝世](../Page/1972年.md "wikilink")）
  - [1929年](../Page/1929年.md "wikilink")：[鲁道夫·穆斯堡尔](../Page/鲁道夫·穆斯堡尔.md "wikilink")，[德国](../Page/德国.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")。（[2011年逝世](../Page/2011年.md "wikilink")）
  - [1935年](../Page/1935年.md "wikilink")：[大江健三郎](../Page/大江健三郎.md "wikilink")，日本[存在主義](../Page/存在主義.md "wikilink")[作家](../Page/作家.md "wikilink")
  - [1938年](../Page/1938年.md "wikilink")：[碧翠斯](../Page/碧翠斯_\(荷蘭\).md "wikilink")，[荷蘭女王](../Page/荷兰女王.md "wikilink")
  - [1942年](../Page/1942年.md "wikilink")：[榮智健](../Page/榮智健.md "wikilink")，[香港](../Page/香港.md "wikilink")[企業家](../Page/企業家.md "wikilink")
  - [1951年](../Page/1951年.md "wikilink")：[盧傑](../Page/盧傑.md "wikilink")，[香港](../Page/香港.md "wikilink")[配音員](../Page/配音員.md "wikilink")
  - [1962年](../Page/1962年.md "wikilink")：[李健仁](../Page/李健仁.md "wikilink")，[香港](../Page/香港.md "wikilink")[電影](../Page/電影.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [1966年](../Page/1966年.md "wikilink")：[石黑賢](../Page/石黑賢.md "wikilink")，[日本男演員](../Page/日本.md "wikilink")
  - [1967年](../Page/1967年.md "wikilink")：[王祖賢](../Page/王祖賢.md "wikilink")，[台灣女](../Page/台灣.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink")：[陈琳](../Page/陳琳_\(歌手\).md "wikilink")，[中国大陆女歌手](../Page/中国大陆.md "wikilink")（[2009年逝世](../Page/2009年.md "wikilink")）
  - [1971年](../Page/1971年.md "wikilink")：[李英愛](../Page/李英愛.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1975年](../Page/1975年.md "wikilink")：[普丽缇·泽塔](../Page/普丽缇·泽塔.md "wikilink")，印度演員
  - [1977年](../Page/1977年.md "wikilink")：[香取慎吾](../Page/香取慎吾.md "wikilink")，[日本](../Page/日本.md "wikilink")[藝人](../Page/藝人.md "wikilink")
  - [1978年](../Page/1978年.md "wikilink")：[李淑楨](../Page/李淑楨.md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[松井優征](../Page/松井優征.md "wikilink")，[日本](../Page/日本.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[沈宜英](../Page/沈宜英.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[賈斯汀·提姆布萊克](../Page/賈斯汀·提姆布萊克.md "wikilink")，美国[歌手](../Page/歌手.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[郭鑫](../Page/郭鑫.md "wikilink")，[台灣藝人](../Page/台灣.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[李學林](../Page/李學林.md "wikilink")，台灣[籃球](../Page/籃球.md "wikilink")[運動員](../Page/運動員.md "wikilink")
  - [1985年](../Page/1985年.md "wikilink")：[藍又時](../Page/藍又時.md "wikilink")，[台灣女歌手](../Page/台灣歌手.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[林家琪](../Page/林家琪.md "wikilink")，台灣電視[主播](../Page/主播.md "wikilink")、模特兒
  - [1990年](../Page/1990年.md "wikilink")：[藪宏太](../Page/藪宏太.md "wikilink")，[日本演員](../Page/日本.md "wikilink")、歌手
  - [1992年](../Page/1992年.md "wikilink")：[周冬雨](../Page/周冬雨.md "wikilink")，[中国大陆女](../Page/中国大陆.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [1995年](../Page/1995年.md "wikilink")：[久保田未夢](../Page/久保田未夢.md "wikilink")，[日本](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink") :
    [赵美延](../Page/赵美延.md "wikilink")
    ，[韓國女子組合](../Page/韓國.md "wikilink")[(G)I-DLE成員](../Page/\(G\)I-DLE.md "wikilink")

## 逝世

  - [1888年](../Page/1888年.md "wikilink")：[鮑思高](../Page/鮑思高.md "wikilink")，[意大利](../Page/意大利.md "wikilink")[神父](../Page/神父.md "wikilink")，[慈幼會的會祖](../Page/慈幼會.md "wikilink")（[1815年出生](../Page/1815年.md "wikilink")）
  - [1943年](../Page/1943年.md "wikilink")：[賴和](../Page/賴和.md "wikilink")，[台灣新文學之父](../Page/台灣新文學.md "wikilink")（[1894年出生](../Page/1894年.md "wikilink")）
  - [1966年](../Page/1966年.md "wikilink")：[白思華](../Page/白思華.md "wikilink")，英國陸軍將領（[1887年出生](../Page/1887年.md "wikilink")）
  - [2000年](../Page/2000年.md "wikilink")：[刘丹](../Page/劉丹_\(中國女演員\).md "wikilink")，中国女演员（[1975年出生](../Page/1975年.md "wikilink")）
  - [2015年](../Page/2015年.md "wikilink")：[張敏祥](../Page/張敏祥.md "wikilink")，台灣退休導演、電視節目製作人，曾參與[公共電視節目製作多年](../Page/公共電視節目.md "wikilink")（[1953年出生](../Page/1953年.md "wikilink")）

## 节假日和习俗

  - ：[国庆日](../Page/国庆日.md "wikilink")

## 參考文獻

1.