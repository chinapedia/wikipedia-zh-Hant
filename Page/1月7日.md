**1月7日**是[公历年的第](../Page/公历.md "wikilink")7天，离一年的结束还有358天（[闰年是](../Page/闰年.md "wikilink")359天）。

## 大事记

### 14世紀

  - [1325年](../Page/1325年.md "wikilink")：[西班牙国王](../Page/西班牙国王.md "wikilink")[迪尼什一世去世](../Page/迪尼什一世.md "wikilink")，其子[阿方索四世继承王位](../Page/阿方索四世_\(葡萄牙\).md "wikilink")。

### 16世紀

  - [1558年](../Page/1558年.md "wikilink")：[吉斯公爵](../Page/吉斯公爵.md "wikilink")[弗朗索瓦率领](../Page/吉斯公爵_\(第二\).md "wikilink")[法国军队夺回](../Page/法国.md "wikilink")[英国在](../Page/英国.md "wikilink")[欧洲大陆的最后一个据点](../Page/欧洲大陆.md "wikilink")[加来](../Page/加来.md "wikilink")。

### 17世紀

  - [1610年](../Page/1610年.md "wikilink")：[意大利天文学家](../Page/意大利.md "wikilink")[伽利略用](../Page/伽利略·伽利莱.md "wikilink")[望远镜](../Page/望远镜.md "wikilink")[首次发现三颗](../Page/伽利略卫星.md "wikilink")[木星的卫星](../Page/木星的卫星.md "wikilink")[木卫一](../Page/木卫一.md "wikilink")、[木卫二和](../Page/木卫二.md "wikilink")[木卫四](../Page/木卫四.md "wikilink")。

### 18世紀

  - [1785年](../Page/1785年.md "wikilink")：[法国发明家](../Page/法国.md "wikilink")[布朗夏尔和](../Page/讓-皮埃爾·布蘭查德.md "wikilink")[美国医生](../Page/美国.md "wikilink")[杰弗里斯乘坐](../Page/約翰·傑弗里斯.md "wikilink")[气球第一次飞越](../Page/气球.md "wikilink")[英吉利海峡](../Page/英吉利海峡.md "wikilink")。
  - [1797年](../Page/1797年.md "wikilink")：[奇斯帕达纳共和国的议会决定使用红](../Page/奇斯帕达纳共和国.md "wikilink")、白、绿[三色旗作为](../Page/三色旗.md "wikilink")[国旗](../Page/国旗.md "wikilink")，后来演变成[意大利国旗](../Page/意大利国旗.md "wikilink")。

### 20世紀

  - [1902年](../Page/1902年.md "wikilink")：因[义和团运动](../Page/义和团.md "wikilink")，招致[八国联军进北京而被迫逃往西安避难的](../Page/八国联军.md "wikilink")[慈禧太后带着](../Page/慈禧太后.md "wikilink")[光绪帝返回北京](../Page/光绪帝.md "wikilink")。
  - [1927年](../Page/1927年.md "wikilink")：[中国](../Page/中国.md "wikilink")[国民政府接管](../Page/国民政府.md "wikilink")[汉口](../Page/汉口.md "wikilink")、[九江的](../Page/九江.md "wikilink")[英国](../Page/英国.md "wikilink")[租界](../Page/租界.md "wikilink")。
  - [1932年](../Page/1932年.md "wikilink")：美國國務卿[亨利·劉易斯·史汀生宣佈](../Page/亨利·劉易斯·史汀生.md "wikilink")[史汀生主义](../Page/不承認主義.md "wikilink")。
  - [1964年](../Page/1964年.md "wikilink")：[巴哈马群岛实行内部自治](../Page/巴哈马群岛.md "wikilink")。
  - [1975年](../Page/1975年.md "wikilink")：[廉政公署將](../Page/香港廉政公署.md "wikilink")[警務處前總警司](../Page/香港警務處.md "wikilink")[葛柏引渡返回](../Page/葛柏.md "wikilink")[香港](../Page/香港.md "wikilink")。
  - [1979年](../Page/1979年.md "wikilink")：[越南军队占领](../Page/越南.md "wikilink")[民主柬埔寨首都](../Page/民主柬埔寨.md "wikilink")[金边](../Page/金边.md "wikilink")，由[波尔布特领导下的](../Page/波尔布特.md "wikilink")[红色高棉政府被推翻](../Page/红色高棉.md "wikilink")，[柬越战争宣告结束](../Page/柬越战争.md "wikilink")。
  - [1981年](../Page/1981年.md "wikilink")：[中国民航由北京经上海](../Page/中国民用航空局.md "wikilink")、旧金山至纽约的中美首条定期航线开航。\[1\]
  - [1984年](../Page/1984年.md "wikilink")：[汶萊加入](../Page/汶萊.md "wikilink")[東南亞國協](../Page/東南亞國協.md "wikilink")，成為第六個會員國。
  - [1989年](../Page/1989年.md "wikilink")：[日本](../Page/日本.md "wikilink")[皇太子](../Page/皇太子.md "wikilink")[明仁在其父親](../Page/明仁.md "wikilink")[昭和天皇因病逝世後](../Page/昭和天皇.md "wikilink")，正式即位為[天皇並更改年號為](../Page/天皇.md "wikilink")[平成](../Page/平成.md "wikilink")。

### 21世紀

  - [2003年](../Page/2003年.md "wikilink")：[中国](../Page/中国.md "wikilink")[安徽](../Page/安徽.md "wikilink")[合肥发生](../Page/合肥.md "wikilink")[一七事件](../Page/2003年合肥学生游行事件.md "wikilink")。
  - [2015年](../Page/2015年.md "wikilink")：法国巴黎《[查理周刊](../Page/查理周刊.md "wikilink")》总部发生[枪击案](../Page/查理周刊总部枪击案.md "wikilink")。

## 出生

  - [1355年](../Page/1355年.md "wikilink")：[伍德斯托克的托马斯](../Page/伍德斯托克的托马斯_\(格洛斯特公爵\).md "wikilink")，[英格蘭](../Page/英格蘭.md "wikilink")[格洛斯特公爵](../Page/格洛斯特公爵.md "wikilink")。（[1397年去世](../Page/1397年.md "wikilink")）
  - [1502年](../Page/1502年.md "wikilink")：[額我略十三世](../Page/額我略十三世.md "wikilink")，[教宗](../Page/教宗.md "wikilink")。（[1585年去世](../Page/1585年.md "wikilink")）
  - [1748年](../Page/1748年.md "wikilink")：[大衛·基利](../Page/大衛·基利.md "wikilink")，[普魯士時期德意志建築家](../Page/普魯士王國.md "wikilink")。（[1808年去世](../Page/1808年.md "wikilink")）
  - [1768年](../Page/1768年.md "wikilink")：[約瑟夫·波拿巴](../Page/約瑟夫·波拿巴.md "wikilink")，法國皇帝[拿破崙一世的哥哥](../Page/拿破崙一世.md "wikilink")。（[1844年去世](../Page/1844年.md "wikilink")）
  - [1800年](../Page/1800年.md "wikilink")：[米勒德·菲尔莫尔](../Page/米勒德·菲尔莫尔.md "wikilink")，[美国第](../Page/美国.md "wikilink")13任[总统](../Page/美国总统.md "wikilink")。（[1874年去世](../Page/1874年.md "wikilink")）
  - [1858年](../Page/1858年.md "wikilink")：[馮國璋](../Page/馮國璋.md "wikilink")，曾任[中華民國副總統](../Page/中華民國副總統.md "wikilink")、代理大總統。（[1919年去世](../Page/1919年.md "wikilink")）
  - [1871年](../Page/1871年.md "wikilink")：[埃米尔·博雷尔](../Page/埃米尔·博雷尔.md "wikilink")，[法國](../Page/法國.md "wikilink")[數學家和](../Page/數學家.md "wikilink")[政治家](../Page/政治家.md "wikilink")。（[1956年去世](../Page/1956年.md "wikilink")）
  - [1919年](../Page/1919年.md "wikilink")：[埃列娜·齐奥塞斯库](../Page/埃列娜·齐奥塞斯库.md "wikilink")，[罗马尼亚政治家](../Page/罗马尼亚.md "wikilink")。（[1989年去世](../Page/1989年.md "wikilink")）
  - [1935年](../Page/1935年.md "wikilink")：[厲聲教](../Page/厉声教.md "wikilink")，中國外交家、國際法學家。（[2017年去世](../Page/2017年.md "wikilink")）
  - [1948年](../Page/1948年.md "wikilink")：[水木一郎](../Page/水木一郎.md "wikilink")，[日本](../Page/日本.md "wikilink")[歌手](../Page/歌手.md "wikilink")、演員。
  - [1952年](../Page/1952年.md "wikilink")：[洪金寶](../Page/洪金寶.md "wikilink")，[香港男演員](../Page/香港.md "wikilink")。
  - [1956年](../Page/1956年.md "wikilink")：[大衛·卡羅素](../Page/大衛·卡羅素.md "wikilink")，[美国演员](../Page/美国.md "wikilink")。
  - [1957年](../Page/1957年.md "wikilink")：[凱蒂·庫瑞克](../Page/凱蒂·庫瑞克.md "wikilink")，[美國新聞主播](../Page/美國.md "wikilink")。
  - [1961年](../Page/1961年.md "wikilink")：[蘇普麗亞·帕塔克](../Page/蘇普麗亞·帕塔克.md "wikilink")，[印度演員](../Page/印度.md "wikilink")。
  - [1962年](../Page/1962年.md "wikilink")：[陳秀雯](../Page/陳秀雯.md "wikilink")，[香港女演員](../Page/香港.md "wikilink")、歌手。
  - [1964年](../Page/1964年.md "wikilink")：[尼古拉斯·凯奇](../Page/尼古拉斯·凯奇.md "wikilink")，美国演員、[導演](../Page/導演.md "wikilink")，[奧斯卡最佳男主角獎得主](../Page/奧斯卡最佳男主角獎.md "wikilink")。
  - [1971年](../Page/1971年.md "wikilink")：[傑瑞米·雷納](../Page/傑瑞米·雷納.md "wikilink")，[美国演員](../Page/美国.md "wikilink")。
  - [1974年](../Page/1974年.md "wikilink")：[楊恭如](../Page/楊恭如.md "wikilink")，[香港女演員](../Page/香港.md "wikilink")。
  - [1976年](../Page/1976年.md "wikilink")：[阿方索·索里亚诺](../Page/阿方索·索里亚诺.md "wikilink")，[美国棒球运动员](../Page/美国.md "wikilink")。
  - 1979年：[比帕莎·巴蘇](../Page/比帕莎·巴蘇.md "wikilink")，[印度演员](../Page/印度.md "wikilink")。
  - [1980年](../Page/1980年.md "wikilink")：[青木琴美](../Page/青木琴美.md "wikilink")，[日本漫畫家](../Page/日本.md "wikilink")。
  - [1982年](../Page/1982年.md "wikilink")：[法蘭西斯科·羅德里奎茲](../Page/法蘭西斯科·羅德里奎茲.md "wikilink")，[美国棒球运动员](../Page/美国.md "wikilink")。
  - 1982年：[酒井真由](../Page/酒井真由.md "wikilink")，[日本漫畫家](../Page/日本.md "wikilink")。
  - [1984年](../Page/1984年.md "wikilink")：[孔賢珠](../Page/孔賢珠.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")。
  - [1985年](../Page/1985年.md "wikilink")：[刘易斯·汉密尔顿](../Page/刘易斯·汉密尔顿.md "wikilink")，[英国](../Page/英国.md "wikilink")[一級方程式賽車车手](../Page/一級方程式賽車.md "wikilink")。
  - [1990年](../Page/1990年.md "wikilink")：[连姆·艾肯](../Page/连姆·艾肯.md "wikilink")，[美国演员](../Page/美国.md "wikilink")。
  - [1994年](../Page/1994年.md "wikilink")：[井口和朋](../Page/井口和朋.md "wikilink")，日本男子棒球運動員，2015年夏季世界大學運動會棒球冠軍
  - [1996年](../Page/1996年.md "wikilink")
    ：[傅園慧](../Page/傅園慧.md "wikilink")，[中國游泳運動員](../Page/中國.md "wikilink")

## 去世

  - [1325年](../Page/1325年.md "wikilink")：[迪尼什一世](../Page/迪尼什一世.md "wikilink")，西班牙國王
  - [1451年](../Page/1451年.md "wikilink")：[阿梅迪奥八世](../Page/阿梅迪奥八世.md "wikilink")，[義大利](../Page/義大利.md "wikilink")[薩伏依公爵](../Page/薩伏依.md "wikilink")，[敌对教皇](../Page/敌对教皇.md "wikilink")。（[1383年出生](../Page/1383年.md "wikilink")）
  - [1524年](../Page/1524年.md "wikilink")：[唐寅](../Page/唐寅.md "wikilink")，[明代画家](../Page/明代.md "wikilink")。（[1470年出生](../Page/1470年.md "wikilink")）
  - [1920年](../Page/1920年.md "wikilink")：[埃德蒙·巴顿](../Page/埃德蒙·巴顿.md "wikilink")，[澳大利亚第一任](../Page/澳大利亚.md "wikilink")[总理](../Page/澳大利亚总理.md "wikilink")。（[1849年出生](../Page/1849年.md "wikilink")）
  - [1932年](../Page/1932年.md "wikilink")：，[法国国务活动家](../Page/法国.md "wikilink")。（[1877年出生](../Page/1877年.md "wikilink")）
  - [1943年](../Page/1943年.md "wikilink")：[尼古拉·特斯拉](../Page/尼古拉·特斯拉.md "wikilink")，美国物理学家。（[1856年出生](../Page/1856年.md "wikilink")）
  - [1984年](../Page/1984年.md "wikilink")：[阿尔弗雷德·卡斯特勒](../Page/阿尔弗雷德·卡斯特勒.md "wikilink")，法国物理学家，[诺贝尔物理学奖获得者](../Page/诺贝尔物理学奖.md "wikilink")。（[1902年出生](../Page/1902年.md "wikilink")）
  - [1988年](../Page/1988年.md "wikilink")：[廖梦醒](../Page/廖梦醒.md "wikilink")，[中国社会活动家](../Page/中国.md "wikilink")。（[1904年出生](../Page/1904年.md "wikilink")）
  - [1989年](../Page/1989年.md "wikilink")：[昭和天皇](../Page/昭和天皇.md "wikilink")，[日本](../Page/日本.md "wikilink")[天皇](../Page/天皇_\(日本\).md "wikilink")。（[1901年出生](../Page/1901年.md "wikilink")）
  - [1995年](../Page/1995年.md "wikilink")：[穆瑞·羅斯巴德](../Page/穆瑞·羅斯巴德.md "wikilink")，美國[经济学家](../Page/经济学家.md "wikilink")。（[1926年出生](../Page/1926年.md "wikilink")）
  - [1998年](../Page/1998年.md "wikilink")：[弗拉迪米尔·普雷洛格](../Page/弗拉迪米尔·普雷洛格.md "wikilink")，[瑞士](../Page/瑞士.md "wikilink")[化学家](../Page/化学家.md "wikilink")，[诺贝尔化学奖获得者](../Page/诺贝尔化学奖.md "wikilink")。（1906年出生）
  - [2006年](../Page/2006年.md "wikilink")：[海因里希·哈勒](../Page/海因里希·哈勒.md "wikilink")，[奧地利](../Page/奧地利.md "wikilink")[登山](../Page/登山.md "wikilink")[运动员](../Page/运动员.md "wikilink")，《[西藏七年](../Page/西藏七年.md "wikilink")》作者。（[1912年出生](../Page/1912年.md "wikilink")）
  - [2012年](../Page/2012年.md "wikilink")：[龐武豪](../Page/龐武豪.md "wikilink")，1988年至2005年擔任[台視新聞](../Page/台視新聞.md "wikilink")[攝影記者](../Page/攝影記者.md "wikilink")。（[1961年出生](../Page/1961年.md "wikilink")）
  - [2014年](../Page/2014年.md "wikilink")：[邵逸夫](../Page/邵逸夫.md "wikilink")，[香港知名的電影製作人](../Page/香港.md "wikilink")、娛樂業大亨、[慈善家](../Page/慈善家.md "wikilink")。（[1907年出生](../Page/1907年.md "wikilink")）
  - [2014年](../Page/2014年.md "wikilink")：[何偉龍](../Page/何偉龍.md "wikilink")，香港知名的戲劇導師、資深演員。（[1956年出生](../Page/1956年.md "wikilink")）
  - [2017年](../Page/2017年.md "wikilink")：[馬里奧·蘇亞雷斯](../Page/馬里奧·蘇亞雷斯.md "wikilink")，[葡萄牙政治家](../Page/葡萄牙.md "wikilink")。([1924年出生](../Page/1924年.md "wikilink"))
  - [2017年](../Page/2017年.md "wikilink")：[巫明霞](../Page/巫明霞.md "wikilink")，台湾演员，藝人[施易男之母](../Page/施易男.md "wikilink")。([1941年出生](../Page/1941年.md "wikilink"))

## 节假日和习俗

  - [儒略历上的](../Page/儒略历.md "wikilink")[圣诞节](../Page/圣诞节.md "wikilink")。

  - ：[胜利纪念日](../Page/胜利纪念日.md "wikilink")。

  - ：[国旗日](../Page/国旗日.md "wikilink")。

## 參考資料

## 外部連結

1.