**2-磷酸甘油酸**（英語：**2-phosphoglycerate**）是生物界中常見的[化學](../Page/化學.md "wikilink")[分子](../Page/分子.md "wikilink")，一般出現在[糖解作用與](../Page/糖解作用.md "wikilink")[糖質新生作用的過程中](../Page/糖質新生.md "wikilink")。

在糖解作用中，2-磷酸甘油酸是[3-磷酸甘油酸在磷酸甘油酸變位酶](../Page/3-磷酸甘油酸.md "wikilink")（Phosphoglycerate）的催化之下產生，2-磷酸甘油酸第2個碳上所接的磷酸根，是來自變位酶。這個反應需要[鎂離子或其他](../Page/鎂.md "wikilink")2價陽離子的參與。

2-磷酸甘油酸也會在接下來的步驟中，受到[烯醇化酶](../Page/烯醇化酶.md "wikilink")（enolase）催化生成一種稱為[磷酸烯醇式丙酮酸的高能磷酸分子](../Page/磷酸烯醇式丙酮酸.md "wikilink")，並釋出[水分子](../Page/水.md "wikilink")，這是糖解作用的第9個步驟，也是倒數第2個步驟。

[Category:糖酵解](../Category/糖酵解.md "wikilink")
[Category:磷酸酯](../Category/磷酸酯.md "wikilink")