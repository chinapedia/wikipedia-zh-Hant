## [比利时](../Page/比利时国家足球队.md "wikilink")

主教练：[罗贝尔·瓦塞热](../Page/罗贝尔·瓦塞热.md "wikilink")（Robert Waseige）

| 編號 | 位置                               | 球員名稱                                                                                       | 出生日期／年齡 | 代表次數 | 效力球隊                                                                  |
| -- | -------------------------------- | ------------------------------------------------------------------------------------------ | ------- | ---- | --------------------------------------------------------------------- |
| 1  | [門將](../Page/足球位置.md "wikilink") | [-{zh-hans:热尔·德弗利格; zh-hant:迪維列拿;}-](../Page/热尔·德弗利格.md "wikilink")（Geert De Vlieger）      | 30      | 25   | [Willem II](../Page/Willem_II_Tilburg.md "wikilink") (Netherlands)    |
| 2  | [後衛](../Page/足球位置.md "wikilink") | [-{zh-hans:埃里克·德弗朗德; zh-hant:迪法蘭迪;}-](../Page/埃里克·德弗朗德.md "wikilink")（Eric Deflandre）      | 28      | 41   | \[里昂\] [Lyon](../Page/Olympique_Lyonnais.md "wikilink") (France)      |
| 3  | [後衛](../Page/足球位置.md "wikilink") | [-{zh-hans:格伦·德波克; zh-hant:迪保錫;}-](../Page/格伦·德波克.md "wikilink")（Glen De Boeck）            | 30      | 33   | [Anderlecht](../Page/R.S.C._Anderlecht.md "wikilink") (Belgium)       |
| 4  | [後衛](../Page/足球位置.md "wikilink") | [-{zh-hans:埃里克·范梅尔; zh-hant:雲美亞;}-](../Page/埃里克·范梅尔.md "wikilink")（Eric Van Meir）          | 34      | 32   | [Standard Liège](../Page/Standard_Liège.md "wikilink") (Belgium)      |
| 5  | [後衛](../Page/足球位置.md "wikilink") | [-{zh-hans:尼克·范凯克霍温; zh-hant:雲卡浩雲;}-](../Page/尼克·范凯克霍温.md "wikilink")（Nico Van Kerckhoven） | 31      | 40   | [Schalke 04](../Page/FC_Schalke_04.md "wikilink") (Germany)           |
| 6  | [中場](../Page/足球位置.md "wikilink") | [蒂米·西蒙斯](../Page/蒂米·西蒙斯.md "wikilink")（Timmy Simons）                                       | 25      | 12   | [Club Brugge](../Page/Club_Brugge.md "wikilink") (Belgium)            |
| 7  | [前鋒](../Page/足球位置.md "wikilink") | [马克·威尔莫茨](../Page/马克·威尔莫茨.md "wikilink")（Marc Wilmots）                                     | 33      | 66   | [Schalke 04](../Page/FC_Schalke_04.md "wikilink") (Germany)           |
| 8  | [中場](../Page/足球位置.md "wikilink") | [巴尔特·古尔](../Page/巴尔特·古尔.md "wikilink")（Bart Goor）                                          | 29      | 38   | [Hertha Berlin](../Page/Hertha_BSC_Berlin.md "wikilink") (Germany)    |
| 9  | [前鋒](../Page/足球位置.md "wikilink") | [威斯利·松克](../Page/威斯利·松克.md "wikilink")（Wesley Sonck）                                       | 23      | 12   | [Racing Genk](../Page/K.R.C._Genk.md "wikilink") (Belgium)            |
| 10 | [中場](../Page/足球位置.md "wikilink") | [约翰·瓦勒姆](../Page/约翰·瓦勒姆.md "wikilink")（Johan Walem）                                        | 30      | 33   | [Standard Liège](../Page/Standard_Liège.md "wikilink") (Belgium)      |
| 11 | [中場](../Page/足球位置.md "wikilink") | [赫尔特·韦尔海恩](../Page/赫尔特·韦尔海恩.md "wikilink")（Gert Verheyen）                                  | 29      | 46   | \[布盧日\] [Club Brugge](../Page/Club_Brugge.md "wikilink") (Belgium)    |
| 12 | [後衛](../Page/足球位置.md "wikilink") | [彼得·范德海登](../Page/彼得·范德海登.md "wikilink")（Peter van der Heyden）                             | 25      | 4    | \[布盧日\] [Club Brugge](../Page/Club_Brugge.md "wikilink") (Belgium)    |
| 13 | [門將](../Page/足球位置.md "wikilink") | [弗兰基·范登德里舍](../Page/弗兰基·范登德里舍.md "wikilink")（Franky Vandendriessche）                       | 31      | 0    | [Mouscron](../Page/R.E._Mouscron.md "wikilink") (Belgium)             |
| 14 | [前鋒](../Page/足球位置.md "wikilink") | [斯文·韦尔曼特](../Page/斯文·韦尔曼特.md "wikilink")（Sven Vermant）                                     | 29      | 12   | [Schalke 04](../Page/FC_Schalke_04.md "wikilink") (Germany)           |
| 15 | [後衛](../Page/足球位置.md "wikilink") | [雅基·皮特斯](../Page/雅基·皮特斯.md "wikilink")（Jacky Peeters）                                      | 33      | 13   | [Gent](../Page/K.A.A._Gent.md "wikilink") (Belgium)                   |
| 16 | [後衛](../Page/足球位置.md "wikilink") | [丹尼尔·范比滕](../Page/丹尼尔·范比滕.md "wikilink")（Daniel Van Buyten）                                | 24      | 7    | [Marseille](../Page/Olympique_de_Marseille.md "wikilink") (France)    |
| 17 | [中場](../Page/足球位置.md "wikilink") | [盖坦·昂格勒贝](../Page/盖坦·昂格勒贝.md "wikilink")（Gaetan Englebert）                                 | 26      | 4    | [Club Brugge](../Page/Club_Brugge.md "wikilink") (Belgium)            |
| 18 | [中場](../Page/足球位置.md "wikilink") | [伊夫·范德埃格](../Page/伊夫·范德埃格.md "wikilink")（Yves Vanderhaeghe）                                | 32      | 30   | [Anderlecht](../Page/R.S.C._Anderlecht.md "wikilink") (Belgium)       |
| 19 | [中場](../Page/足球位置.md "wikilink") | [贝恩德·泰斯](../Page/贝恩德·泰斯.md "wikilink")（Bernd Thijs）                                        | 24      | 2    | [Racing Genk](../Page/K.R.C._Genk.md "wikilink") (Belgium)            |
| 20 | [前鋒](../Page/足球位置.md "wikilink") | [布兰科·斯特鲁帕](../Page/布兰科·斯特鲁帕.md "wikilink")（Branko Strupar）                                 | 32      | 14   | [Derby County](../Page/Derby_County_F.C..md "wikilink") (England)     |
| 21 | [中場](../Page/足球位置.md "wikilink") | [丹尼·博凡](../Page/丹尼·博凡.md "wikilink")（Danny Boffin）                                         | 36      | 52   | [St. Truiden](../Page/K._Sint-Truidense_V.V..md "wikilink") (Belgium) |
| 22 | [前鋒](../Page/足球位置.md "wikilink") | [姆博·姆蓬萨](../Page/姆博·姆蓬萨.md "wikilink")（Mbo Mpenza）                                         | 25      | 26   | [Mouscron](../Page/R.E._Mouscron.md "wikilink") (Belgium)             |
| 23 | [門將](../Page/足球位置.md "wikilink") | [弗雷德里克·埃尔珀](../Page/弗雷德里克·埃尔珀.md "wikilink")（Frederic Herpoel）                             | 27      | 7    | [Gent](../Page/K.A.A._Gent.md "wikilink") (Belgium)                   |

## [日本](../Page/日本国家足球队.md "wikilink")

主教练：[-{zh-hans:菲利普·特鲁西埃;
zh-hant:杜斯亞;}-](../Page/菲利普·特鲁西埃.md "wikilink")（Philippe
Troussier）

| 編號 | 位置                               | 球員名稱                                                     | 出生日期／年齡 | 代表次數 | 效力球隊                                     |
| -- | -------------------------------- | -------------------------------------------------------- | ------- | ---- | ---------------------------------------- |
| 1  | [門將](../Page/足球位置.md "wikilink") | [川口能活](../Page/川口能活.md "wikilink")（Yoshikatsu Kawaguchi） | 26      | 43   | [樸次茅夫](../Page/樸次茅夫.md "wikilink")（英格蘭）  |
| 2  | [後衛](../Page/足球位置.md "wikilink") | [秋田丰](../Page/秋田丰.md "wikilink")（Yutaka Akita）           | 31      | 38   | [鹿島鹿角](../Page/鹿島鹿角.md "wikilink")（日本）   |
| 3  | [後衛](../Page/足球位置.md "wikilink") | [松田直树](../Page/松田直树.md "wikilink")（Naoki Matsuda）        | 23      | 24   | [橫濱水手](../Page/橫濱水手.md "wikilink")（日本）   |
| 4  | [後衛](../Page/足球位置.md "wikilink") | [森冈隆三](../Page/森冈隆三.md "wikilink")（Ryuzo Morioka）        | 26      | 32   | [清水心跳](../Page/清水心跳.md "wikilink")（日本）   |
| 5  | [中場](../Page/足球位置.md "wikilink") | [稻本润一](../Page/稻本润一.md "wikilink")（Junichi Inamoto）      | 22      | 22   | [阿仙奴](../Page/阿仙奴.md "wikilink")（英格蘭）    |
| 6  | [後衛](../Page/足球位置.md "wikilink") | [服部年宏](../Page/服部年宏.md "wikilink")（Toshihiro Hattori）    | 28      | 35   | [磐田山葉](../Page/磐田山葉.md "wikilink")（日本）   |
| 7  | [中場](../Page/足球位置.md "wikilink") | [中田英壽](../Page/中田英壽.md "wikilink")（Hidetoshi Nakata）     | 25      | 39   | [帕爾馬](../Page/帕爾馬.md "wikilink")（意大利）    |
| 8  | [中場](../Page/足球位置.md "wikilink") | [森岛宽晃](../Page/森岛宽晃.md "wikilink")（Hiroaki Morishima）    | 30      | 57   | [大阪櫻花](../Page/大阪櫻花.md "wikilink")（日本）   |
| 9  | [前鋒](../Page/足球位置.md "wikilink") | [西泽明训](../Page/西泽明训.md "wikilink")（Akinori Nishizawa）    | 25      | 24   | [大阪櫻花](../Page/大阪櫻花.md "wikilink")（日本）   |
| 10 | [前鋒](../Page/足球位置.md "wikilink") | [中山雅史](../Page/中山雅史.md "wikilink")（Masashi Nakayama）     | 34      | 47   | [磐田山葉](../Page/磐田山葉.md "wikilink")（日本）   |
| 11 | [前鋒](../Page/足球位置.md "wikilink") | [铃木隆行](../Page/铃木隆行.md "wikilink")（Takayuki Suzuki）      | 25      | 10   | [鹿島鹿角](../Page/鹿島鹿角.md "wikilink")（日本）   |
| 12 | [門將](../Page/足球位置.md "wikilink") | [酋崎正刚](../Page/酋崎正刚.md "wikilink")（Seigo Narazaki）       | 26      | 15   | [名古屋八鯨](../Page/名古屋八鯨.md "wikilink")（日本） |
| 13 | [前鋒](../Page/足球位置.md "wikilink") | [柳泽敦](../Page/柳泽敦.md "wikilink")（Atsushi Yanagisawa）     | 25      | 22   | [鹿島鹿角](../Page/鹿島鹿角.md "wikilink")（日本）   |
| 14 | [中場](../Page/足球位置.md "wikilink") | [三都主](../Page/三都主.md "wikilink")（Alex）                   | 24      | 0    | [清水心跳](../Page/清水心跳.md "wikilink")（日本）   |
| 15 | [中場](../Page/足球位置.md "wikilink") | [福西崇史](../Page/福西崇史.md "wikilink")（Takashi Fukunishi）    | 25      | 5    | [磐田山葉](../Page/磐田山葉.md "wikilink")（日本）   |
| 16 | [後衛](../Page/足球位置.md "wikilink") | [中田浩二](../Page/中田浩二.md "wikilink")（Koji Nakata）          | 22      | 20   | [鹿島鹿角](../Page/鹿島鹿角.md "wikilink")（日本）   |
| 17 | [後衛](../Page/足球位置.md "wikilink") | [宫本恒靖](../Page/宫本恒靖.md "wikilink")（Tsuneyasu Miyamoto）   | 25      | 5    | [大阪松下](../Page/大阪松下.md "wikilink")（日本）   |
| 18 | [中場](../Page/足球位置.md "wikilink") | [小野伸二](../Page/小野伸二.md "wikilink")（Shinji Ono）           | 22      | 21   | [飛燕諾](../Page/飛燕諾.md "wikilink")（荷蘭）     |
| 19 | [中場](../Page/足球位置.md "wikilink") | [小笠原满男](../Page/小笠原满男.md "wikilink")（Mitsuo Ogasawara）   | 23      | 0    | [鹿島鹿角](../Page/鹿島鹿角.md "wikilink")（日本）   |
| 20 | [中場](../Page/足球位置.md "wikilink") | [明神智和](../Page/明神智和.md "wikilink")（Tomokazu Myojin）      | 24      | 16   | [鹿島鹿角](../Page/鹿島鹿角.md "wikilink")（日本）   |
| 21 | [中場](../Page/足球位置.md "wikilink") | [户田和幸](../Page/户田和幸.md "wikilink")（Kazuyuki Toda）        | 24      | 10   | [清水心跳](../Page/清水心跳.md "wikilink")（日本）   |
| 22 | [中場](../Page/足球位置.md "wikilink") | [市川大佑](../Page/市川大佑.md "wikilink")（Daisuke Ichikawa）     | 22      | 1    | [清水心跳](../Page/清水心跳.md "wikilink")（日本）   |
| 23 | [門將](../Page/足球位置.md "wikilink") | [曾端准](../Page/曾端准.md "wikilink")（Hitoshi Sogahata）       | 22      | 1    | [鹿島鹿角](../Page/鹿島鹿角.md "wikilink")（日本）   |

## [俄罗斯](../Page/俄罗斯国家足球队.md "wikilink")

主教练：[奥列格·罗曼采夫](../Page/奥列格·罗曼采夫.md "wikilink")（Oleg Romantsev）

| 編號 | 位置                               | 球員名稱                                                                     | 出生日期／年齡 | 代表次數 | 效力球隊                                                                  |
| -- | -------------------------------- | ------------------------------------------------------------------------ | ------- | ---- | --------------------------------------------------------------------- |
| 1  | [門將](../Page/足球位置.md "wikilink") | [鲁斯兰·尼格马图林](../Page/鲁斯兰·尼格马图林.md "wikilink")（Ruslan Nigmatullin）         | 27      | 20   | [维罗纳](../Page/维罗纳足球俱乐部.md "wikilink")（Hellas Verona）（意大利）             |
| 2  | [後衛](../Page/足球位置.md "wikilink") | [尤里·科夫通](../Page/尤里·科夫通.md "wikilink")（Yury Kovtun）                      | 32      | 44   | [莫斯科斯巴达克](../Page/莫斯科斯巴达克.md "wikilink")（Spartak Moscow）（俄罗斯）         |
| 3  | [後衛](../Page/足球位置.md "wikilink") | [尤里·尼基福罗夫](../Page/尤里·尼基福罗夫.md "wikilink")（Yury Nikiforov）               | 31      | 56   | [埃因霍温](../Page/PSV埃因霍温.md "wikilink")（PSV Eindhoven）（荷兰）              |
| 4  | [中場](../Page/足球位置.md "wikilink") | [阿列克谢·斯梅尔金](../Page/阿列克谢·斯梅尔金.md "wikilink")（Alexey Smertin）             | 27      | 26   | [波尔多](../Page/波尔多足球俱乐部.md "wikilink")（Girondins Bordeaux）（法国）         |
| 5  | [後衛](../Page/足球位置.md "wikilink") | [安德烈·索洛马金](../Page/安德烈·索洛马金.md "wikilink")（Andrei Solomatin）             | 26      | 5    | [莫斯科中央陆军](../Page/莫斯科中央陆军.md "wikilink")（CSKA Moscow）（俄罗斯）            |
| 6  | [中場](../Page/足球位置.md "wikilink") | [伊戈尔·谢姆绍夫](../Page/伊戈尔·谢姆绍夫.md "wikilink")（Igor Semshov）                 | 24      | 2    | [莫斯科鱼雷](../Page/莫斯科鱼雷.md "wikilink")（Torpedo Moscow）（俄罗斯）             |
| 7  | [後衛](../Page/足球位置.md "wikilink") | [维克托·奥诺普科](../Page/维克托·奥诺普科.md "wikilink")（Viktor Onopko）                | 32      | 97   | [皇家奥维耶多](../Page/皇家奥维耶多.md "wikilink")（Real Oviedo）（西班牙）              |
| 8  | [中場](../Page/足球位置.md "wikilink") | [瓦列里·卡尔平](../Page/瓦列里·卡尔平.md "wikilink")（Valeri Karpin）                  | 33      | 69   | [维戈塞尔塔](../Page/维戈塞尔塔.md "wikilink")（Celta Vigo）（西班牙）                 |
| 9  | [中場](../Page/足球位置.md "wikilink") | [伊戈尔·季托夫](../Page/伊戈尔·季托夫.md "wikilink")（Egor Titov）                     | 26      | 33   | [莫斯科斯巴达克](../Page/莫斯科斯巴达克.md "wikilink")（Spartak Moscow）（俄罗斯）         |
| 10 | [中場](../Page/足球位置.md "wikilink") | [亚历山大·莫斯托沃伊](../Page/亚历山大·莫斯托沃伊.md "wikilink")（Alexander Mostovoi）       | 33      | 59   | [维戈塞尔塔](../Page/维戈塞尔塔.md "wikilink")（Celta Vigo）（西班牙）                 |
| 11 | [前鋒](../Page/足球位置.md "wikilink") | [弗拉基米尔·别斯查尼奇](../Page/弗拉基米尔·别斯查尼奇.md "wikilink")（Vladimir Beschastnykh）  | 28      | 64   | [莫斯科斯巴达克](../Page/莫斯科斯巴达克.md "wikilink")（Spartak Moscow）（俄罗斯）         |
| 12 | [門將](../Page/足球位置.md "wikilink") | [斯坦尼斯拉夫·切尔切索夫](../Page/斯坦尼斯拉夫·切尔切索夫.md "wikilink")（Stanislav Cherchesov） | 38      | 49   | [蒂罗尔](../Page/蒂罗尔足球俱乐部.md "wikilink")（Tirol）（奥地利）                     |
| 13 | [後衛](../Page/足球位置.md "wikilink") | [维亚切斯拉夫·达耶夫](../Page/维亚切斯拉夫·达耶夫.md "wikilink")（Vyacheslav Daev）          | 29      | 7    | [莫斯科中央陆军](../Page/莫斯科中央陆军.md "wikilink")（CSKA Moscow）（俄罗斯）            |
| 14 | [後衛](../Page/足球位置.md "wikilink") | [伊戈尔·丘盖诺夫](../Page/伊戈尔·丘盖诺夫.md "wikilink")（Igor Chugainov）               | 32      | 30   | [乌拉尔埃利斯塔](../Page/乌拉尔埃利斯塔.md "wikilink")（Uralan Elista）（俄罗斯）          |
| 15 | [中場](../Page/足球位置.md "wikilink") | [季米特里·阿列尼切夫](../Page/季米特里·阿列尼切夫.md "wikilink")（Dmitri Alenichev）         | 29      | 43   | [波尔图足球俱乐部](../Page/波尔图足球俱乐部.md "wikilink")（Porto）（葡萄牙）                |
| 16 | [前鋒](../Page/足球位置.md "wikilink") | [亚历山大·克尔扎科夫](../Page/亚历山大·克尔扎科夫.md "wikilink")（Alexander Kerzhakov）      | 19      | 3    | [圣彼得堡泽尼特](../Page/圣彼得堡泽尼特.md "wikilink")（Zenit Saint Petersburg）（俄罗斯） |
| 17 | [中場](../Page/足球位置.md "wikilink") | [谢尔盖·谢马克](../Page/谢尔盖·谢马克.md "wikilink")（Sergei Semak）                   | 26      | 30   | [莫斯科中央陆军](../Page/莫斯科中央陆军.md "wikilink")（CSKA Moscow）（俄罗斯）            |
| 18 | [後衛](../Page/足球位置.md "wikilink") | [季米特里·先尼科夫](../Page/季米特里·先尼科夫.md "wikilink")（Dmitri Sennikov）            | 26      | 4    | [莫斯科火车头](../Page/莫斯科火车头.md "wikilink")（Lokomotiv Moscow）（俄罗斯）         |
| 19 | [前鋒](../Page/足球位置.md "wikilink") | [鲁斯兰·皮缅诺夫](../Page/鲁斯兰·皮缅诺夫.md "wikilink")（Ruslan Pimenov）               | 20      | 1    | [莫斯科火车头](../Page/莫斯科火车头.md "wikilink")（Lokomotiv Moscow）（俄罗斯）         |
| 20 | [中場](../Page/足球位置.md "wikilink") | [马拉特·伊斯梅洛夫](../Page/马拉特·伊斯梅洛夫.md "wikilink")（Marat Izmailov）             | 19      | 8    | [莫斯科火车头](../Page/莫斯科火车头.md "wikilink")（Lokomotiv Moscow）（俄罗斯）         |
| 21 | [中場](../Page/足球位置.md "wikilink") | [季米特里·霍赫洛夫](../Page/季米特里·霍赫洛夫.md "wikilink")（Dmitri Khokhlov）            | 26      | 38   | [皇家社会](../Page/皇家社会.md "wikilink")（Real Sociedad）（西班牙）                |
| 22 | [前鋒](../Page/足球位置.md "wikilink") | [季米特里·瑟乔夫](../Page/季米特里·瑟乔夫.md "wikilink")（Dmitri Sychev）                | 18      | 3    | [莫斯科斯巴达克](../Page/莫斯科斯巴达克.md "wikilink")（Spartak Moscow）（俄罗斯）         |
| 23 | [門將](../Page/足球位置.md "wikilink") | [亚历山大·菲利莫诺夫](../Page/亚历山大·菲利莫诺夫.md "wikilink")（Alexander Filimonov）      | 28      | 16   | [乌拉尔埃利斯塔](../Page/乌拉尔埃利斯塔.md "wikilink")（Uralan Elista）（俄罗斯）          |

## [突尼斯](../Page/突尼斯国家足球队.md "wikilink")

主教练：[Khemaies Laabidi](../Page/Khemaies_Laabidi.md "wikilink")

| 編號 | 位置                                                       | 球員名稱 | 出生日期／年齡 | 代表次數 | 效力球隊                                                                 |
| -- | -------------------------------------------------------- | ---- | ------- | ---- | -------------------------------------------------------------------- |
| 1  | [Ali Boumnijel](../Page/Ali_Boumnijel.md "wikilink")     | GK   | 36      | 14   | [Bastia](../Page/SC_Bastia.md "wikilink") (France)                   |
| 2  | [Khaled Badra](../Page/Khaled_Badra.md "wikilink")       | D    | 29      | 72   | [Espérance](../Page/Espérance.md "wikilink") (Tunisia)               |
| 3  | [Zoubeir Beya](../Page/Zoubeir_Beya.md "wikilink")       | M    | 31      | 77   | [Beşiktaş](../Page/Besiktas_Jimnastik_Kulübü.md "wikilink") (Turkey) |
| 4  | [Mohamed Mkacher](../Page/Mohamed_Mkacher.md "wikilink") | D    | 27      | 15   | [ES Sahel](../Page/ES_Sahel.md "wikilink") (Tunisia)                 |
| 5  | [Ziad Jaziri](../Page/Ziad_Jaziri.md "wikilink")         | F    | 23      | 26   | [ES Sahel](../Page/ES_Sahel.md "wikilink") (Tunisia)                 |
| 6  | [Hatem Trabelsi](../Page/Hatem_Trabelsi.md "wikilink")   | D    | 25      | 27   | [Ajax](../Page/Ajax_Amsterdam.md "wikilink") (Netherlands)           |
| 7  | [Imed Mhedhebi](../Page/Imed_Mhedhebi.md "wikilink")     | M    | 26      | 30   | [Genoa](../Page/Genoa_1893.md "wikilink") (Italy)                    |
| 8  | [Hassen Gabsi](../Page/Hassen_Gabsi.md "wikilink")       | M    | 28      | 48   | [Genoa](../Page/Genoa_1893.md "wikilink") (Italy)                    |
| 9  | [Riadh Jelassi](../Page/Riadh_Jelassi.md "wikilink")     | F    | 30      | 20   | [Club Africain](../Page/Club_Africain.md "wikilink") (Tunisia)       |
| 10 | [Kaies Ghodhbane](../Page/Kaies_Ghodhbane.md "wikilink") | M    | 26      | 62   | [ES Sahel](../Page/ES_Sahel.md "wikilink") (Tunisia)                 |
| 11 | [Adel Sellimi](../Page/Adel_Sellimi.md "wikilink")       | F    | 29      | 64   | [Freiburg](../Page/SC_Freiburg.md "wikilink") (Germany)              |
| 12 | [Raouf Bouzaiene](../Page/Raouf_Bouzaiene.md "wikilink") | D    | 31      | 39   | [Genoa](../Page/Genoa_1893.md "wikilink") (Italy)                    |
| 13 | [Riadh Bouzaizi](../Page/Riadh_Bouzaizi.md "wikilink")   | M    | 29      | 47   | [Bursaspor](../Page/Bursaspor.md "wikilink") (Turkey)                |
| 14 | [Hamdi Marzouki](../Page/Hamdi_Marzouki.md "wikilink")   | D    | 25      | 7    | [Club Africain](../Page/Club_Africain.md "wikilink") (Tunisia)       |
| 15 | [Radhi Jaidi](../Page/Radhi_Jaidi.md "wikilink")         | D    | 26      | 40   | [Espérance](../Page/Espérance.md "wikilink") (Tunisia)               |
| 16 | [Hassen Bejaoui](../Page/Hassen_Bejaoui.md "wikilink")   | GK   | 26      | 2    | [CA Bizertin](../Page/CA_Bizertin.md "wikilink") (Tunisia)           |
| 17 | [Tarek Thabet](../Page/Tarek_Thabet.md "wikilink")       | D    | 30      | 70   | [Espérance](../Page/Espérance.md "wikilink") (Tunisia)               |
| 18 | [Slim Ben Achour](../Page/Slim_Ben_Achour.md "wikilink") | M    | 20      | 3    | [Martigues](../Page/FC_Martigues.md "wikilink") (France)             |
| 19 | [Emir Mkademi](../Page/Emir_Mkademi.md "wikilink")       | D    | 22      | 9    | [ES Sahel](../Page/ES_Sahel.md "wikilink") (Tunisia)                 |
| 20 | [Ali Zitouni](../Page/Ali_Zitouni.md "wikilink")         | F    | 21      | 23   | [Espérance](../Page/Espérance.md "wikilink") (Tunisia)               |
| 21 | [Mourad Melki](../Page/Mourad_Melki.md "wikilink")       | M    | 27      | 11   | [Espérance](../Page/Espérance.md "wikilink") (Tunisia)               |
| 22 | [Ahmed Jaoachi](../Page/Ahmed_Jaoachi.md "wikilink")     | GK   | 26      | 0    | [US Monastir](../Page/US_Monastir.md "wikilink") (Tunisia)           |
| 23 | [Jose Clayton](../Page/Jose_Clayton.md "wikilink")       | D    | 28      | 12   | [Espérance](../Page/Espérance.md "wikilink") (Tunisia)               |

[Category:2002年世界杯足球赛](../Category/2002年世界杯足球赛.md "wikilink")