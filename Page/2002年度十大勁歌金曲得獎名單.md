**2002年度十大勁歌金曲頒獎典禮**於2003年1月5日在[紅磡香港體育館舉行](../Page/紅磡香港體育館.md "wikilink")；主持是[曾志偉](../Page/曾志偉.md "wikilink")、[林曉峰和](../Page/林曉峰.md "wikilink")[陳松齡](../Page/陳松齡.md "wikilink")。

## 歌曲獎項

### 十大勁歌金曲獎

|     | 歌曲   | 歌手                               | 作曲                                           | 填詞                               | 編曲                                                                      | 監製                                                                                 |
| --- | ---- | -------------------------------- | -------------------------------------------- | -------------------------------- | ----------------------------------------------------------------------- | ---------------------------------------------------------------------------------- |
| 第一首 | 女人之苦 | [許志安](../Page/許志安.md "wikilink") | [陳輝陽](../Page/陳輝陽.md "wikilink")             | [林　夕](../Page/林夕.md "wikilink")  | [陳輝陽](../Page/陳輝陽.md "wikilink")                                        | [陳輝陽](../Page/陳輝陽.md "wikilink")                                                   |
| 第二首 | 傷逝   | [葉蒨文](../Page/葉蒨文.md "wikilink") | [Eric Kwok](../Page/Eric_Kwok.md "wikilink") | [林　夕](../Page/林夕.md "wikilink")  | [Eric Kwok](../Page/Eric_Kwok.md "wikilink")                            | [梁榮駿](../Page/梁榮駿.md "wikilink")                                                   |
| 第三首 | 有福氣  | [陳慧琳](../Page/陳慧琳.md "wikilink") | [雷頌德](../Page/雷頌德.md "wikilink")             | [周禮茂](../Page/周禮茂.md "wikilink") | [雷頌德](../Page/雷頌德.md "wikilink")                                        | [雷頌德](../Page/雷頌德.md "wikilink")                                                   |
| 第四首 | 愛不釋手 | [李克勤](../Page/李克勤.md "wikilink") | [陳輝陽](../Page/陳輝陽.md "wikilink")             | [林　夕](../Page/林夕.md "wikilink")  | [陳輝陽](../Page/陳輝陽.md "wikilink")                                        | [陳輝陽](../Page/陳輝陽.md "wikilink")                                                   |
| 第五首 | 明年今日 | [陳奕迅](../Page/陳奕迅.md "wikilink") | [陳小霞](../Page/陳小霞.md "wikilink")             | [林　夕](../Page/林夕.md "wikilink")  | [陳輝陽](../Page/陳輝陽.md "wikilink")                                        | [陳輝陽](../Page/陳輝陽.md "wikilink")                                                   |
| 第六首 | 天生天養 | [劉德華](../Page/劉德華.md "wikilink") | Justin Chen                                  | [林　夕](../Page/林夕.md "wikilink")  | Terry Chan                                                              | 陳德建                                                                                |
| 第七首 | 爭氣   | [容祖兒](../Page/容祖兒.md "wikilink") | [陳輝陽](../Page/陳輝陽.md "wikilink")             | [林　夕](../Page/林夕.md "wikilink")  | [陳輝陽](../Page/陳輝陽.md "wikilink")                                        | [陳輝陽](../Page/陳輝陽.md "wikilink")                                                   |
| 第八首 | 楊千嬅  | [楊千嬅](../Page/楊千嬅.md "wikilink") | [Eric Kwok](../Page/Eric_Kwok.md "wikilink") | [林　夕](../Page/林夕.md "wikilink")  | [Eric Kwok](../Page/Eric_Kwok.md "wikilink")                            | \-{[于逸堯](../Page/于逸堯.md "wikilink")}-、[Eric Kwok](../Page/Eric_Kwok.md "wikilink") |
| 第九首 | 好心分手 | [盧巧音](../Page/盧巧音.md "wikilink") | [雷頌德](../Page/雷頌德.md "wikilink")             | [黃偉文](../Page/黃偉文.md "wikilink") | [Ted Lo](../Page/Ted_lo.md "wikilink")、[雷頌德](../Page/雷頌德.md "wikilink") | [雷頌德](../Page/雷頌德.md "wikilink")                                                   |
| 第十首 | 高妹正傳 | [梁詠琪](../Page/梁詠琪.md "wikilink") | [梁詠琪](../Page/梁詠琪.md "wikilink")             | [黃偉文](../Page/黃偉文.md "wikilink") | 張人傑                                                                     | [趙增熹](../Page/趙增熹.md "wikilink")                                                   |

| **十大勁歌金曲獎候選名單** |
| :-------------: |
|     **編號**      |
|       01        |
|       02        |
|       03        |
|       04        |
|       05        |
|       06        |
|       07        |
|       08        |
|       09        |
|       10        |
|       11        |
|       12        |
|       13        |
|       14        |
|       15        |
|       16        |
|       17        |
|       18        |
|       19        |
|       20        |
|       21        |
|       22        |
|       23        |
|       24        |
|       25        |
|       26        |
|       27        |
|       28        |
|       29        |
|       30        |
|       31        |
|       32        |
|       33        |
|       34        |
|       35        |
|       36        |
|       37        |
|       38        |
|       39        |
|       40        |
|       41        |
|       42        |
|       43        |
|       44        |
|       45        |
|       46        |
|       47        |
|       48        |
|       49        |
|       50        |
|       51        |
|       52        |
|       53        |
|       54        |
|       55        |
|       56        |
|       57        |
|       58        |
|       59        |
|       60        |
|       61        |
|       62        |
|       63        |
|       64        |
|       65        |
|       66        |
|       67        |
|       68        |
|       69        |
|       70        |
|       71        |
|       72        |
|       73        |
|       74        |
|       75        |
|       76        |
|       77        |
|       78        |
|       79        |
|       80        |
|       81        |
|       82        |
|       83        |
|       84        |

### 最受歡迎改編歌曲演繹大獎

  - **愛上殺手 - [麥浚龍](../Page/麥浚龍.md "wikilink")**

| **最受歡迎改編歌曲演繹大獎候選名單** |
| :------------------: |
|        **編號**        |
|          01          |
|          02          |
|          03          |
|          04          |
|          05          |
|          06          |
|          07          |
|          08          |
|          09          |
|          10          |
|          11          |
|          12          |
|          13          |
|          14          |
|          15          |

### 最受歡迎廣告歌曲大獎

  - **金獎：風箏與風 - [Twins](../Page/Twins.md "wikilink")**
  - 銀獎：爭氣 - [容祖兒](../Page/容祖兒.md "wikilink")
  - 銅獎：時光中飛舞 -
    [陳文媛](../Page/陳文媛.md "wikilink")、[李蘢怡](../Page/李蘢怡.md "wikilink")

| **最受歡迎廣告歌曲大獎候選名單** |
| :----------------: |
|       **編號**       |
|         01         |
|         02         |
|         03         |
|         04         |
|         05         |
|         06         |
|         07         |
|         08         |
|         09         |
|         10         |
|         11         |
|         12         |
|         13         |
|         14         |
|         15         |
|         16         |
|         17         |
|         18         |
|         19         |
|         20         |
|         21         |
|         22         |
|         23         |
|         24         |
|         25         |
|         26         |
|         27         |

### 最受歡迎國語歌曲獎

  - **金獎：回到過去 - [周杰倫](../Page/周杰倫.md "wikilink")**
  - 銀獎：Ask For More - [F4](../Page/F4_\(男子團體\).md "wikilink")
  - 銅獎：亂了情人 - [關心妍](../Page/關心妍.md "wikilink")

| **最受歡迎國語歌曲獎候選名單** |
| :---------------: |
|      **編號**       |
|        01         |
|        02         |
|        03         |
|        04         |
|        05         |
|        06         |
|        07         |
|        08         |
|        09         |
|        10         |
|        11         |
|        12         |
|        13         |
|        14         |
|        15         |
|        16         |
|        17         |
|        18         |

### 最受歡迎合唱歌曲獎

  - **金獎：魅力移動 -
    [EO2](../Page/EO2.md "wikilink")、[E-kids](../Page/E-kids.md "wikilink")、[張詠詩](../Page/張詠詩.md "wikilink")**
  - 銀獎：心寒(合唱版) -
    [鄧健泓](../Page/鄧健泓.md "wikilink")、[鄭中基](../Page/鄭中基.md "wikilink")
  - 銅獎：方寸大亂 -
    [小　雪](../Page/小雪_\(香港\).md "wikilink")、[葉文輝](../Page/葉文輝.md "wikilink")

| **最受歡迎合唱歌曲獎候選名單** |
| :---------------: |
|      **編號**       |
|        01         |
|        02         |
|        03         |
|        04         |
|        05         |
|        06         |
|        07         |
|        08         |
|        09         |
|        10         |
|        11         |
|        12         |
|        13         |
|        14         |
|        15         |
|        16         |
|        17         |
|        18         |

### 最受歡迎網上金曲獎

  - **金獎：失戀王 - [陳小春](../Page/陳小春.md "wikilink")**
  - 銀獎：再見露絲瑪莉 - [何韻詩](../Page/何韻詩.md "wikilink")
  - 銅獎：I Never Told You - [陳冠希](../Page/陳冠希.md "wikilink")

| **最受歡迎網上金曲獎候選名單** |
| :---------------: |
|      **編號**       |
|        01         |
|        02         |
|        03         |
|        04         |
|        05         |
|        06         |
|        07         |
|        08         |
|        09         |
|        10         |
|        11         |
|        12         |
|        13         |
|        14         |
|        15         |
|        16         |
|        17         |
|        18         |
|        19         |
|        20         |
|        21         |
|        22         |
|        23         |
|        24         |
|        25         |
|        26         |
|        27         |
|        28         |
|        29         |
|        30         |
|        31         |
|        32         |
|        33         |
|        34         |
|        35         |
|        36         |
|        37         |
|        38         |
|        39         |
|        40         |
|        41         |
|        42         |
|        43         |
|        44         |
|        45         |
|        46         |
|        47         |
|        48         |
|        49         |
|        50         |
|        51         |
|        52         |
|        53         |
|        54         |
|        55         |
|        56         |
|        57         |
|        58         |
|        59         |

## 歌手獎項

### 最受歡迎唱作歌星

  - **金獎：[梁詠琪](../Page/梁詠琪.md "wikilink")**
      - <small> 演繹歌曲：我們的永遠</small>
  - 銀獎：[陳曉東](../Page/陳曉東_\(藝人\).md "wikilink")
  - 銅獎：[何韻詩](../Page/何韻詩.md "wikilink")

|            **最受歡迎唱作歌星候選名單**             |
| :-------------------------------------: |
|    [王力宏](../Page/王力宏.md "wikilink")     |
|    [胡彥斌](../Page/胡彥斌.md "wikilink")     |
| [陳曉東](../Page/陳曉東_\(藝人\).md "wikilink") |
|    [謝霆鋒](../Page/謝霆鋒.md "wikilink")     |

### 2002年度傑出表現獎

  - **金獎：[F4](../Page/F4_\(男子團體\).md "wikilink")**
      - <small> 演繹歌曲：煙火的季節</small>
  - 銀獎：[Twins](../Page/Twins.md "wikilink")
  - 銅獎：[馬浚偉](../Page/馬浚偉.md "wikilink")

|       **2002年度傑出表現獎候選名單**        |
| :------------------------------: |
| [梁詠琪](../Page/梁詠琪.md "wikilink") |
| [陳文媛](../Page/陳文媛.md "wikilink") |
| [陳小春](../Page/陳小春.md "wikilink") |
| [谷祖琳](../Page/谷祖琳.md "wikilink") |
| [EO2](../Page/EO2.md "wikilink") |
| [蕭正楠](../Page/蕭正楠.md "wikilink") |
| [張柏芝](../Page/張柏芝.md "wikilink") |
| [鄭希怡](../Page/鄭希怡.md "wikilink") |
| [葉文輝](../Page/葉文輝.md "wikilink") |

### 公益金最喜愛慈善組合大獎

  - **[F4](../Page/F4_\(男子團體\).md "wikilink")、[Twins](../Page/Twins.md "wikilink")**
      - <small> 演繹歌曲：愛在深秋</small>

## 新人獎項

### 新星試打金曲獎

  - **還你門匙 - [余文樂](../Page/余文樂.md "wikilink")**

| **新星試打金曲獎候選名單** |
| :-------------: |
|     **編號**      |
|       01        |
|       02        |
|       03        |
|       04        |

### 最受歡迎新人獎（男）

  - **金獎：[蕭正楠](../Page/蕭正楠.md "wikilink")**
      - <small> 演繹歌曲：接受我</small>
  - 銀獎：[麥浚龍](../Page/麥浚龍.md "wikilink")
  - 銅獎：[胡彥斌](../Page/胡彥斌.md "wikilink")

|           **最受歡迎新人獎（男）候選名單**            |
| :-------------------------------------: |
| [王嘉明](../Page/王嘉明_\(香港\).md "wikilink") |
|    [胡彥斌](../Page/胡彥斌.md "wikilink")     |
|    [羅金榮](../Page/羅金榮.md "wikilink")     |

### 最受歡迎新人獎（女）

  - **金獎：[鄭希怡](../Page/鄭希怡.md "wikilink")、[關心妍](../Page/關心妍.md "wikilink")**
      - <small> 演繹歌曲：相對濕度、你有心</small>
  - 銀獎：從缺
  - 銅獎：[趙頌茹](../Page/趙頌茹.md "wikilink")

|        **最受歡迎新人獎（女）候選名單**        |
| :------------------------------: |
| [谷祖琳](../Page/谷祖琳.md "wikilink") |
| [張鎧潼](../Page/張鎧潼.md "wikilink") |

### 最受歡迎新人組合獎

  - **金獎：[Shine](../Page/Shine.md "wikilink")**
      - <small> 演繹歌曲：祖與占</small>
  - 銀獎：[EO2](../Page/EO2.md "wikilink")
  - 銅獎：[Cookies](../Page/Cookies_\(組合\).md "wikilink")

|                **最受歡迎新人組合獎候選名單**                |
| :---------------------------------------------: |
| [Cookies](../Page/Cookies_\(組合\).md "wikilink") |

## 幕後獎項

### 最佳作曲

  - **好心分手 - [雷頌德](../Page/雷頌德.md "wikilink")**\[1\]'''
      - <small>主唱歌手：[盧巧音](../Page/盧巧音.md "wikilink")</small>

### 最佳填詞

  - **失戀王 - [黃偉文](../Page/黃偉文.md "wikilink")**
      - <small>主唱歌手：[陳小春](../Page/陳小春.md "wikilink")</small>

### 最佳編曲

  - **傷逝 - [Eric Kwok](../Page/Eric_Kwok.md "wikilink")**
      - <small>主唱歌手：[葉蒨文](../Page/葉蒨文.md "wikilink")</small>

### 最佳歌曲監製

  - **安靜 - [周杰倫](../Page/周杰倫.md "wikilink")**
      - <small>主唱歌手：[周杰倫](../Page/周杰倫.md "wikilink")</small>

## 四台聯頒音樂大獎

### 2002四台聯頒音樂大獎 - 歌曲獎

  - **得獎歌曲：傷逝**
  - 作曲：[Eric Kwok](../Page/Eric_Kwok.md "wikilink")
  - 填詞：[林　夕](../Page/林夕.md "wikilink")\[2\]
  - 編曲：[Eric Kwok](../Page/Eric_Kwok.md "wikilink")
  - 監製：[Alvin Leong](../Page/梁榮駿.md "wikilink")
  - 主唱：[葉蒨文](../Page/葉蒨文.md "wikilink")

## 五大年度獎項

### 亞太區最受歡迎香港男歌星

  - **[劉德華](../Page/劉德華.md "wikilink")**
      - <small> 演繹歌曲：肉麻情歌</small>

|          **亞太區最受歡迎香港男歌星候選名單**           |
| :-------------------------------------: |
|    [李克勤](../Page/李克勤.md "wikilink")     |
|    [陳小春](../Page/陳小春.md "wikilink")     |
| [陳曉東](../Page/陳曉東_\(藝人\).md "wikilink") |
|    [張學友](../Page/張學友.md "wikilink")     |
|    [鄧健泓](../Page/鄧健泓.md "wikilink")     |
|    [蘇永康](../Page/蘇永康.md "wikilink")     |

### 亞太區最受歡迎香港女歌星

  - **[陳慧琳](../Page/陳慧琳.md "wikilink")**
      - <small> 演繹歌曲：閃亮每一天</small>

|          **亞太區最受歡迎香港女歌星候選名單**          |
| :------------------------------------: |
| [小　雪](../Page/小雪_\(香港\).md "wikilink") |
|    [容祖兒](../Page/容祖兒.md "wikilink")    |
|    [陳慧琳](../Page/陳慧琳.md "wikilink")    |
|    [張燊悅](../Page/張燊悅.md "wikilink")    |
|    [楊千嬅](../Page/楊千嬅.md "wikilink")    |
|    [鄭秀文](../Page/鄭秀文.md "wikilink")    |

### 最受歡迎男歌星

  - **[李克勤](../Page/李克勤.md "wikilink")**
      - <small> 演繹歌曲：高妹</small>
      - <small>
        最後五強：[李克勤](../Page/李克勤.md "wikilink")、[周杰倫](../Page/周杰倫.md "wikilink")、[陳奕迅](../Page/陳奕迅.md "wikilink")、[許志安](../Page/許志安.md "wikilink")、[劉德華](../Page/劉德華.md "wikilink")</small>

|             **最受歡迎男歌星候選名單**             |
| :-------------------------------------: |
|    [李克勤](../Page/李克勤.md "wikilink")     |
|    [陳小春](../Page/陳小春.md "wikilink")     |
| [陳曉東](../Page/陳曉東_\(藝人\).md "wikilink") |
|    [張學友](../Page/張學友.md "wikilink")     |
|    [鄧健泓](../Page/鄧健泓.md "wikilink")     |
|    [譚詠麟](../Page/譚詠麟.md "wikilink")     |

### 最受歡迎女歌星

  - **[楊千嬅](../Page/楊千嬅.md "wikilink")**
      - <small> 演繹歌曲：可惜我是水瓶座</small>
      - <small>
        最後五強：[容祖兒](../Page/容祖兒.md "wikilink")、[陳慧琳](../Page/陳慧琳.md "wikilink")、[楊千嬅](../Page/楊千嬅.md "wikilink")、[鄭秀文](../Page/鄭秀文.md "wikilink")、[Twins](../Page/Twins.md "wikilink")</small>

|            **最受歡迎女歌星候選名單**             |
| :------------------------------------: |
| [小　雪](../Page/小雪_\(香港\).md "wikilink") |
|    [容祖兒](../Page/容祖兒.md "wikilink")    |
|    [陳慧琳](../Page/陳慧琳.md "wikilink")    |
|    [張燊悅](../Page/張燊悅.md "wikilink")    |
|    [楊千嬅](../Page/楊千嬅.md "wikilink")    |
|    [鄭秀文](../Page/鄭秀文.md "wikilink")    |

### 十大勁歌金曲金獎

  - **明年今日 - [陳奕迅](../Page/陳奕迅.md "wikilink")**

## 榮譽大獎

### 十大勁歌金曲二十週年榮譽大獎

  - **[羅　文](../Page/羅文.md "wikilink")**

## 相關連結

  - [2002年度香港四台冠軍歌曲列表](../Page/2002年度香港四台冠軍歌曲列表.md "wikilink")

## 外部連結

  - [《2002年度十大勁歌金曲頒獎典禮》官方網站](https://web.archive.org/web/20170211075942/http://tvcity.tvb.com/jsg/2002final/index.html)

## 注釋

<div class="references-small">

<references />

</div>

1.  雷頌德缺席，由盧巧音代領。
2.  林夕缺席，由葉蒨文代領。