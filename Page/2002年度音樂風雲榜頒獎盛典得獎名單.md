第二届音乐风云榜颁奖盛典在2002年3月23日于[深圳大剧院举行](../Page/深圳大剧院.md "wikilink")。

## 年度最佳女歌手

  - 内地：[陈琳](../Page/陈琳.md "wikilink")　香港：[陈慧琳](../Page/陈慧琳.md "wikilink")　台湾：[孙燕姿](../Page/孙燕姿.md "wikilink")

## 年度最佳男歌手

  - 内地：空缺　香港：[黎明](../Page/黎明.md "wikilink")　　台湾：[周杰伦](../Page/周杰伦.md "wikilink")

## 年度卓越表现奖

  - 内地：[韩红](../Page/韩红.md "wikilink")、[常宽](../Page/常宽.md "wikilink")　港台：[陶子](../Page/陶子.md "wikilink")、[周华健](../Page/周华健.md "wikilink")、[张宇](../Page/张宇.md "wikilink")

## 年度最佳组合奖

  - 内地：[羽泉](../Page/羽泉.md "wikilink")　港台：[动力火车](../Page/动力火车.md "wikilink")

## 年度最佳摇滚专辑

  - 内地：[郑钧](../Page/郑钧.md "wikilink")　港台：[伍佰](../Page/伍佰.md "wikilink")

## 年度最佳摇滚单曲

  - 《别让爱你的人流泪》·[轮回乐队](../Page/轮回乐队.md "wikilink")

## 年度最佳乐队　

  - [零点乐队](../Page/零点乐队.md "wikilink")

## 年度最佳对唱　

  - 内地：《笑傲江湖》·[刘欢](../Page/刘欢.md "wikilink")、[王菲](../Page/王菲.md "wikilink")　港台：《屋顶》·[周杰伦](../Page/周杰伦.md "wikilink")、[温岚](../Page/温岚.md "wikilink")

## 年度最佳流行舞曲

  - 内地：[梦幻想](../Page/梦幻想.md "wikilink")　港台：[陈慧琳](../Page/陈慧琳.md "wikilink")

## 年度最佳影视歌曲

  - 内地：《情深深雨濛濛》[赵薇](../Page/赵薇.md "wikilink")　港台：《花样年华》[梁朝伟](../Page/梁朝伟.md "wikilink")、[吴恩淇](../Page/吴恩淇.md "wikilink")

## 年度最佳录影带　

  - 内地：《当爱情经过的时候》·[周艳泓](../Page/周艳泓.md "wikilink")　港台：《别框我》·[黎明](../Page/黎明.md "wikilink")

## 年度最佳专辑　

  - 内地：《爱就爱了》·[陈琳](../Page/陈琳.md "wikilink")　香港：《流年》·[王菲](../Page/王菲.md "wikilink")　台湾：《范特西》·[周杰伦](../Page/周杰伦.md "wikilink")

## 年度最佳作词　

  - 内地：《一笑而过》·[那英](../Page/那英.md "wikilink")　港台：《流年》·[林夕](../Page/林夕.md "wikilink")

## 年度最佳作曲　

  - 内地：《爱就爱了》·[张亚东](../Page/张亚东.md "wikilink")　港台：《流年》·[陈晓娟](../Page/陈晓娟.md "wikilink")

## 年度最佳编曲　

  - 内地：《一生有你》·[李延亮](../Page/李延亮.md "wikilink")　港台：《那个傻瓜爱过你》·[小虫](../Page/小虫.md "wikilink")

## 年度最佳制作人　

  - 内地：《爱就爱了》·[张亚东](../Page/张亚东.md "wikilink")　《忘忧草》·[李宗盛](../Page/李宗盛.md "wikilink")

## 年度最佳创作女歌手　

  - 内地：[韩红](../Page/韩红.md "wikilink")　港台：[林忆莲](../Page/林忆莲.md "wikilink")

## 年度最佳创作男歌手　

  - 内地：[李泉](../Page/李泉.md "wikilink")　港台：[王力宏](../Page/王力宏.md "wikilink")

## 年度女歌手飞跃奖　

  - 内地：[张咪](../Page/张咪.md "wikilink")、[叶蓓](../Page/叶蓓.md "wikilink")　港台：[彭佳慧](../Page/彭佳慧.md "wikilink")　[范晓萱](../Page/范晓萱.md "wikilink")

## 年度男歌手飞跃奖　

  - 内地：[谷峰](../Page/谷峰.md "wikilink")、[林依轮](../Page/林依轮.md "wikilink")、[汪峰](../Page/汪峰.md "wikilink")　港台：[陈小春](../Page/陈小春.md "wikilink")、[古巨基](../Page/古巨基.md "wikilink")、[伍佰](../Page/伍佰.md "wikilink")

## 年度杰出新人奖

  - [辛欣](../Page/辛欣.md "wikilink")、[周杰伦](../Page/周杰伦.md "wikilink")、[水木年华](../Page/水木年华.md "wikilink")、[陈冠希](../Page/陈冠希.md "wikilink")、[黄征](../Page/黄征.md "wikilink")、[果味VC](../Page/果味VC.md "wikilink")

## 年度杰出贡献奖

  - 内地：[谷建芬](../Page/谷建芬.md "wikilink")　港台：[李宗盛](../Page/李宗盛.md "wikilink")

## 年度最受欢迎男歌手　

  - 内地：[孙楠](../Page/孙楠.md "wikilink")　香港：[刘德华](../Page/刘德华.md "wikilink")　台湾：[任贤齐](../Page/任贤齐.md "wikilink")

## 年度最受欢迎女歌手　

  - 内地：[那英](../Page/那英.md "wikilink")　香港：[王菲](../Page/王菲.md "wikilink")　台湾：[林忆莲](../Page/林忆莲.md "wikilink")

## 港台十大金曲

  - 《飞鸟》　　　　　　　　　　　[任贤齐](../Page/任贤齐.md "wikilink")
  - 《纸飞机》　　　　　　　　　　[林忆莲](../Page/林忆莲.md "wikilink")
  - 《风筝》　　　　　　　　　　　[孙燕姿](../Page/孙燕姿.md "wikilink")
  - 《自由港》　　　　　　　　　　[陈晓东](../Page/陈晓东.md "wikilink")
  - 《替身》　　　　　　　　　　　[张宇](../Page/张宇.md "wikilink")
  - 《简单爱》　　　　　　　　　　[周杰伦](../Page/周杰伦.md "wikilink")
  - 《唯一》　　　　　　　　　　　[王力宏](../Page/王力宏.md "wikilink")
  - 《有没有一首歌让你想起我》　　[周华健](../Page/周华健.md "wikilink")
  - 《那个傻瓜爱过你》　　　　　　[赵传](../Page/赵传.md "wikilink")
  - 《流年》　　　　　　　　　　　[王菲](../Page/王菲.md "wikilink")

## 内地十大金曲：

  - 《醒了》　　　　　　　　　　　[韩红](../Page/韩红.md "wikilink")
  - 《双鱼》　　　　　　　　　　　[叶蓓](../Page/叶蓓.md "wikilink")
  - 《深呼吸》　　　　　　　　　　[羽泉](../Page/羽泉.md "wikilink")
  - 《爱就爱了》　　　　　　　　　[陈琳](../Page/陈琳.md "wikilink")
  - 《向快乐出发》　　　　　　　　[林依轮](../Page/林依轮.md "wikilink")、[零点乐队](../Page/零点乐队.md "wikilink")
  - 《别让爱你的人流泪》　　　　　[轮回乐队](../Page/轮回乐队.md "wikilink")
  - 《1/3理想》　　　　　　　　　 [郑钧](../Page/郑钧.md "wikilink")
  - 《水姻缘》　　　　　　　　　　[田震](../Page/田震.md "wikilink")
  - 《梦的眼睛》　　　　　　　　　[孙楠](../Page/孙楠.md "wikilink")
  - 《我不是天使》　　　　　　　　[那英](../Page/那英.md "wikilink")

## 外部链接

  - <https://web.archive.org/web/20070519114449/http://www.netandtv.com/newspage/htm2002-3/2002324135850985975.htm>

[Category:音乐风云榜](../Category/音乐风云榜.md "wikilink")