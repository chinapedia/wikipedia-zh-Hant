[2003年](../Page/2003年.md "wikilink")[9月的新闻事件](../Page/9月.md "wikilink")：

**请参看：**

  - [第58届联合国大会](../Page/第58届联合国大会.md "wikilink")
  - [2004年美国总统选举](../Page/2004年美国总统选举.md "wikilink")
  - [中东和平路线图](../Page/中东和平路线图.md "wikilink")
  - [欧盟扩大](../Page/欧盟.md "wikilink")
  - [北朝鲜核危机](../Page/北朝鲜核危机.md "wikilink")
  - [恐怖主义](../Page/恐怖主义.md "wikilink")
  - [美军占领伊拉克](../Page/美军占领伊拉克.md "wikilink")
  - [钓鱼岛](../Page/钓鱼岛.md "wikilink")

## [2003年](../Page/2003年.md "wikilink")

### 9月27日

  - [欧洲空间局发射](../Page/欧洲空间局.md "wikilink")[SMART-1探月卫星](../Page/SMART-1.md "wikilink")。

### 9月26日

  - [日本](../Page/日本.md "wikilink")[北海道于这天早上发生](../Page/北海道.md "wikilink")[黎克特制](../Page/黎克特制.md "wikilink")8级地震
    ，震央在北海道襟裳岬冲。

### [9月20日](../Page/9月20日.md "wikilink")

  - [伽利略号太空探测器如期与](../Page/伽利略号太空探测器.md "wikilink")[木星相撞](../Page/木星.md "wikilink")，并结束了持续14年的使命。[1](http://tech.sina.com.cn/other/2003-09-22/0857236520.shtml)
  - 美东华人在日驻美使馆前抗议[日本占领](../Page/日本.md "wikilink")[钓鱼岛](../Page/钓鱼岛.md "wikilink")。

### [9月19日](../Page/9月19日.md "wikilink")

  - [中国卫生部从即日起恢复对中国内地](../Page/中国.md "wikilink")[传染性非典型肺炎疫情监测情况的每日通报](../Page/传染性非典型肺炎.md "wikilink")。[2](http://news.xinhuanet.com/newscenter/2003-09/19/content_1090542.htm)
  - 国务院任命[汤显明为](../Page/汤显明.md "wikilink")[香港特别行政区海关关长](../Page/香港特别行政区.md "wikilink")。[3](http://news.sina.com.cn/c/2003-09-19/1618783336s.shtml)

### [9月18日](../Page/9月18日.md "wikilink")

  - [新华网推出国内最大英文](../Page/新华网.md "wikilink")[网站](../Page/网站.md "wikilink")。[4](http://www.xinhuanet.com/newscenter/index.htm)
  - 考古人员在[贵州发现最古老鲨鱼](../Page/贵州.md "wikilink")[化石](../Page/化石.md "wikilink")，这是目前[中国发现的数目最多](../Page/中国.md "wikilink")、齿列最长、个体最大、年代最久远的[旋齿鲨化石](../Page/旋齿鲨.md "wikilink")，约2.8亿年。[5](http://news.xinhuanet.com/st/2003-09/18/content_1088239.htm)

### [9月16日](../Page/9月16日.md "wikilink")

  - [第58届联合国大会开幕](../Page/第58届联合国大会.md "wikilink")。[6](http://news.sina.com.cn/w/2003-09-17/0640764782s.shtml)
  - [中东和平路线图](../Page/中东和平路线图.md "wikilink")：[美国否决安理会关于反对驱逐](../Page/美国.md "wikilink")[阿拉法特的议案](../Page/阿拉法特.md "wikilink")。[7](http://www.xinhuanet.com/newscenter/byct/index.htm)
  - [巴西科学家破译](../Page/巴西.md "wikilink")[曼氏血吸虫基因](../Page/曼氏血吸虫.md "wikilink")，绘出其[基因图谱](../Page/基因图谱.md "wikilink")。[8](http://news.xinhuanet.com/st/2003-09/17/content_1085688.htm)

### [9月14日](../Page/9月14日.md "wikilink")

  - [瑞典全民公决](../Page/瑞典.md "wikilink")，拒绝采用[欧元货币](../Page/欧元.md "wikilink")。
  - [爱莎尼亚公决](../Page/爱莎尼亚.md "wikilink")，赞成加入[欧盟](../Page/欧盟.md "wikilink")。

### [9月13日](../Page/9月13日.md "wikilink")

  - 有598名[二战后被遗弃在](../Page/二战.md "wikilink")[中国的](../Page/中国.md "wikilink")[日本公民将在本月底向法院集体起诉日本政府](../Page/日本.md "wikilink")。[9](http://www.phoenixtv.com/home/news/world/200309/13/107702.html)
  - [联合国秘书长](../Page/联合国秘书长.md "wikilink")[安南任命](../Page/科菲·安南.md "wikilink")[挪威人扬](../Page/挪威.md "wikilink")-埃杰兰特为新任联合国驻[伊拉克特使](../Page/伊拉克.md "wikilink")。[10](http://www.phoenixtv.com/home/news/world/200309/13/107689.html)
  - [以色列表示将帮助](../Page/以色列.md "wikilink")[印度方面建造一艘先进的潜艇](../Page/印度.md "wikilink")。[11](http://www.phoenixtv.com/home/news/world/200309/13/107668.html)

### [9月12日](../Page/9月12日.md "wikilink")

  - [中东和平路线图](../Page/中东和平路线图.md "wikilink")：[联合国](../Page/联合国.md "wikilink")[安理会主席发表讲话](../Page/安理会.md "wikilink")，呼吁[以色列不要驱逐](../Page/以色列.md "wikilink")[巴勒斯坦领导人](../Page/巴勒斯坦.md "wikilink")[阿拉法特](../Page/阿拉法特.md "wikilink")。但是以色列方面拒绝撤消驱逐阿拉法特的决定。大陆方面表示驱逐无助于中东和平进程。[12](http://www.phoenixtv.com/home/news/world/200309/13/107725.html)[13](http://www.phoenixtv.com/home/news/world/200309/13/107722.html)[14](http://www.phoenixtv.com/home/news/world/200309/12/107577.html)
  - [联合国通过决议](../Page/联合国.md "wikilink")，取消对[利比亚长达](../Page/利比亚.md "wikilink")11年的制裁。
  - [西班牙警方正式逮捕叙利亚裔](../Page/西班牙.md "wikilink")[半岛电视台](../Page/半岛电视台.md "wikilink")[记者泰西尔](../Page/记者.md "wikilink")·阿洛尼。警方表示他与[基地组织有关系](../Page/基地组织.md "wikilink")。[15](http://www.phoenixtv.com/home/news/world/200309/12/107595.html)
  - [丹麦的著名标志](../Page/丹麦.md "wikilink")“美人鱼雕像”遭人破坏。[16](http://www.phoenixtv.com/home/news/world/200309/12/107613.html)
  - [意大利总理](../Page/意大利.md "wikilink")[贝鲁斯科尼为](../Page/贝鲁斯科尼.md "wikilink")[墨索里尼辩护](../Page/墨索里尼.md "wikilink")，遭到指责。[17](http://www.phoenixtv.com/home/news/world/200309/12/107507.html)
  - [颱風鳴蟬吹襲南韓](../Page/颱風鳴蟬.md "wikilink")，造成113人死亡，13人失蹤，經濟損失逾13億美元。

### [9月11日](../Page/9月11日.md "wikilink")

  - [中东和平路线图](../Page/中东和平路线图.md "wikilink")：[沙龙召集内阁会议决定驱逐](../Page/沙龙.md "wikilink")[阿拉法特](../Page/阿拉法特.md "wikilink")。[18](http://www.xinhuanet.com/newscenter/byct/index.htm)
  - 国际科研小组创造人类历史上最低温度纪录：他们在[实验室内达到了仅仅比](../Page/实验室.md "wikilink")[绝对零度高](../Page/绝对零度.md "wikilink")０．５纳[开尔文的温度](../Page/开尔文.md "wikilink")。[19](http://news.xinhuanet.com/st/2003-09/12/content_1078462.htm)
  - [恐怖主义](../Page/恐怖主义.md "wikilink")：美国务院向全球发出恐怖袭击警告。[20](http://www.phoenixtv.com/home/news/world/200309/12/107391.html)

### [9月10日](../Page/9月10日.md "wikilink")

  - 据美国天文学会发布的最新消息，[科学家对](../Page/科学家.md "wikilink")[太阳耀斑进行观测分析后发现](../Page/太阳耀斑.md "wikilink")，太阳耀斑在释放出大量能量的同时，产生了[反物质](../Page/反物质.md "wikilink")，而且这些反物质与正常物质相遇并湮灭的地区与预期的地区不一样。
  - [瑞典外长](../Page/瑞典.md "wikilink")[安娜·琳德](../Page/安娜·琳德.md "wikilink")（Anna
    Lindh）遭到刺杀，在次日伤重去世。

### [9月9日](../Page/9月9日.md "wikilink")

  - [新加坡卫生部证实](../Page/新加坡.md "wikilink")，８日在新加坡总医院发现的一名非典疑似患者确实感染了[SARS](../Page/SARS.md "wikilink")。[21](http://news.xinhuanet.com/world/2003-09/09/content_1072214.htm)
  - [中东和平路线图](../Page/中东和平路线图.md "wikilink")：以军基地附近发生自杀袭击8人丧生。

### [9月6日](../Page/9月6日.md "wikilink")

  - [巴勒斯坦总理](../Page/巴勒斯坦.md "wikilink")[阿巴斯向](../Page/阿巴斯.md "wikilink")[巴勒斯坦民族权力机构主席](../Page/巴勒斯坦民族权力机构.md "wikilink")[阿拉法特递交辞呈辞职](../Page/阿拉法特.md "wikilink")。[22](http://news.xinhuanet.com/world/2003-09/06/content_1066500.htm)
  - [美国总统](../Page/美国总统.md "wikilink")[乔治·沃克·布什今天就](../Page/乔治·沃克·布什.md "wikilink")[伊拉克及反恐问题发表全国讲话](../Page/伊拉克.md "wikilink")。[23](http://news.xinhuanet.com/world/2003-09/06/content_1066081.htm)
  - [美国和](../Page/美国.md "wikilink")[英国对](../Page/英国.md "wikilink")[香港政府撤回](../Page/香港.md "wikilink")《国安条例草案》表示欢迎，美国同时表示，美国将一如既往的与香港政府进行多边对话。[24](http://www.phoenixtv.com/home/news/hkmo/200309/06/104752.html)
  - [台湾今天进行在](../Page/台湾.md "wikilink")[台北举行](../Page/台北.md "wikilink")“正名运动”，[民进党有两](../Page/民进党.md "wikilink")、三万人参加。游行民众高呼“台湾中国，一边一国”的口号，台湾前总统[李登辉一家三代](../Page/李登辉_\(总统\).md "wikilink")7口也参加了正名运动。[25](http://www.phoenixtv.com/home/news/taiwan/200309/06/104737.html)
    [26](http://www.phoenixtv.com/home/news/taiwan/200309/06/104791.html)
  - [美军占领伊拉克](../Page/美军占领伊拉克.md "wikilink")：[美国国防部长](../Page/美国.md "wikilink")[拉姆斯菲尔德在电视访问中表示](../Page/拉姆斯菲尔德.md "wikilink")，盟军不会撤出[伊拉克](../Page/伊拉克.md "wikilink")。[27](http://www.phoenixtv.com/home/news/world/200309/06/104720.html)
  - [朝鲜核危机](../Page/朝鲜核危机.md "wikilink")：据[香港媒体报道](../Page/香港.md "wikilink")，[朝鲜最近将举行大规模集会](../Page/朝鲜.md "wikilink")，在朝鲜核开发问题上和[美国抗争](../Page/美国.md "wikilink")。[28](http://www.phoenixtv.com/home/news/world/200309/06/104708.html)
  - [美国和](../Page/美国.md "wikilink")[英国联合提出的旨在解决](../Page/英国.md "wikilink")[伊拉克问题的草案遭到](../Page/伊拉克.md "wikilink")[法国和](../Page/法国.md "wikilink")[德国的反对](../Page/德国.md "wikilink")，而[联合国的十五个成员国就草案也出现严重的分歧](../Page/联合国.md "wikilink")。[29](http://www.phoenixtv.com/home/news/world/200309/06/104713.html)

### [9月5日](../Page/9月5日.md "wikilink")

  - [以色列海军袭击](../Page/以色列.md "wikilink")[约旦河西岸城市](../Page/约旦河.md "wikilink")，1名以军士兵死亡，4名巴平民受伤。[30](http://news.xinhuanet.com/world/2003-09/06/content_1066165.htm)
  - [美国联邦调查局今天发表一份公告](../Page/美国联邦调查局.md "wikilink")，宣布在全球范围内逮捕与针对美国进行恐怖袭击威胁有关的4名男子。[31](http://www.phoenixtv.com/home/news/world/200309/06/104701.html)
  - [香港特区行政长官](../Page/香港.md "wikilink")[董建华宣布撤销](../Page/董建华.md "wikilink")《国家安全（立法条文）条例草案》。

### [9月3日](../Page/9月3日.md "wikilink")

  - [香港金管局宣布推出港元新钞票](../Page/香港.md "wikilink")。
  - [香港立法会议员](../Page/香港.md "wikilink")[刘慧卿在沙田隆亨村的办事处被人淋粪便](../Page/刘慧卿.md "wikilink")，警方已接受此案件。据信，刘慧卿事件是由于她上个月到[台湾出席了由台湾独立人士举行的会议](../Page/台湾.md "wikilink")。[32](http://www.phoenixtv.com/home/news/hkmo/200309/04/103708.html)

### [9月2日](../Page/9月2日.md "wikilink")

  - 欧洲议会已经决定推迟对一项颇有争议的[软件专利法案的表决](../Page/软件专利.md "wikilink")。原定于本周一进行的这次表决将被推迟到[9月22日](../Page/9月22日.md "wikilink")。