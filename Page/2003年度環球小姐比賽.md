**2003年度[環球小姐比賽](../Page/環球小姐.md "wikilink")**是第五十一届比賽。在[巴拿馬首都](../Page/巴拿馬.md "wikilink")[巴拿馬城舉行](../Page/巴拿馬城.md "wikilink")。七十一位來自世界各地的選手競逐，最終由[多明尼加共和國小姐](../Page/多明尼加共和國.md "wikilink")[阿梅莉婭．維加勝出](../Page/阿梅莉婭．維加.md "wikilink")。

## 有關資料

## 結果

<table>
<thead>
<tr class="header">
<th><p>比賽結果</p></th>
<th><p>得獎佳麗</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>2003年度環球小姐</strong></p></td>
<td><ul>
<li><strong></strong> - Amelia Vega（阿梅莉婭·維加）</li>
</ul></td>
</tr>
<tr class="even">
<td><p><strong>亞軍</strong></p></td>
<td><ul>
<li><strong></strong> - Mariángel Ruiz</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><strong>季軍</strong></p></td>
<td><ul>
<li><strong></strong> - Cindy Nell(莘蒂.尼爾)</li>
</ul></td>
</tr>
<tr class="even">
<td><p><strong>第四名</strong></p></td>
<td><ul>
<li><strong></strong> - Sanja Papić</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><strong>第五名</strong></p></td>
<td><ul>
<li><strong></strong> - Miyako Miyazaki（宮崎京）</li>
</ul></td>
</tr>
<tr class="even">
<td><p><strong>十強</strong></p></td>
<td><ul>
<li><strong></strong> - Gislaine Ferreira</li>
<li><strong></strong> - Ndapewa Alfons</li>
<li><strong></strong> - Leanne Marie Cecile</li>
<li><strong></strong> - Kateřina Smrzová</li>
<li><strong></strong> - Faye Alibocus</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><strong>十五強</strong></p></td>
<td><ul>
<li><strong></strong> - Claudia Ortiz</li>
<li><strong></strong> - Ana Sebastião</li>
<li><strong></strong> - Susie Castillo</li>
<li><strong></strong> - Stefanie de Roux</li>
<li><strong></strong> - Marietta Chrousala</li>
</ul></td>
</tr>
</tbody>
</table>

## 重要記錄

## 選手記錄

  - **** - [Denisa Kola](../Page/Denisa_Kola.md "wikilink")
  - **** - Ana Sebastião
  - **** - Kai Davis
  - **** - Laura Romero
  - **** - Malayka Rasmijn
  - **** - Ashlea Talbot
  - **** - Nadia Johnson
  - **** - Nadia Forte
  - **** - [Julie Taton](../Page/Julie_Taton.md "wikilink")
  - **** - Becky Bernard
  - **** - Irene Aguilera
  - **** - Gislaine Ferreira
  - **** - Elena Tihomirova
  - **** - Leanne Marie Cecile
  - **** - Nichelle Welcome
  - **** - Wu Wei(吳薇)
  - **** - Diana Mantilla
  - **** - Andrea Ovares
  - **** - Ivana Delic
  - **** - [Vanessa van
    Arendonk](../Page/Vanessa_van_Arendonk.md "wikilink")
  - **** - Ivi Lazarou
  - **** - Kateřina Smrzová
  - **** - [Amelia Vega](../Page/Amelia_Vega.md "wikilink")'''
  - **** - Andrea Jácome
  - **** - Nour El-Samary
  - **** - Diana Valdivieso
  - **** - Katrin Susi
  - **** - Anna Maria Strömberg
  - **** - Emmanuelle Chossat
  - **** - Alexsandra Vodjanikova
  - **** - [Marietta
    Chrousala](../Page/Marietta_Chrousala.md "wikilink")
  - **** - Florecita de Jesus
  - **** - Leanna Damond
  - **** - Viktoria Tomozi
  - **** - Nikita Anand
  - **** - Lesley Flood

<!-- end list -->

  - **** - [Sivan Klein](../Page/Sivan_Klein.md "wikilink")
  - **** - Silvia Ceccon
  - **** - Michelle Lecky
  - **** - [Miyako Miyazaki](../Page/Miyako_Miyazaki.md "wikilink")
  - **** - Geum Na-na
  - **** - Elaine Daly
  - **** - Marie-Aimee Bergicourt
  - **** - [Marisol González](../Page/Marisol_González.md "wikilink")
  - **** - Ndapewa Alfons
  - **** - Tessa Brix
  - **** - Sharee Adams
  - **** - Claudia Salmeron
  - **** - Celia Ohumotu
  - **** - Hanne-Karine Sørby
  - **** - [Stefanie de Roux](../Page/Stefanie_de_Roux.md "wikilink")
  - **** - [Claudia Ortiz](../Page/Claudia_Ortiz.md "wikilink")
  - **** - [Carla Balingit](../Page/Carla_Balingit.md "wikilink")
  - **** - Iwona Makuch
  - **** - Carla Tricoli
  - **** - Olesya Bondarenko
  - **** - [Sanja Papić](../Page/Sanja_Papić.md "wikilink")
  - **** - Bernice Wong
  - **** - Petra Mokrošová
  - **** - Polona Baš
  - **** - Cindy Nell
  - **** - [Eva González](../Page/Eva_González.md "wikilink")
  - **** - [Helena Stenbäck](../Page/Helena_Stenbäck.md "wikilink")
  - **** - Nadine Vinzens
  - **** - Szu-Yu Chen(陳思羽)
  - **** - [Yaowalak
    Traisurat](../Page/Yaowalak_Traisurat.md "wikilink")
  - **** - Faye Alibocus
  - **** - Ozge Ulusoy
  - **** - Lilja Kopytova
  - **** - [Susie Castillo](../Page/Susie_Castillo.md "wikilink")
  - **** - [Mariángel Ruiz](../Page/Mariángel_Ruiz.md "wikilink")

## 評判員

## 補充

由於中國的政治因素,環球小姐大會要求台灣佳麗更名為中華台北 經抗議後 大會同意陳思羽台下戴台灣彩帶 台上依然使用中華台北

## 外部链接

  - [環球小姐官方網站](http://www.missuniverse.com)

[Category:環球小姐](../Category/環球小姐.md "wikilink")