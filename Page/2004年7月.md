## [7月30日](../Page/7月30日.md "wikilink")

  - 在[2004年亚洲杯足球赛四分之一决赛比赛中](../Page/2004年亚洲杯足球赛.md "wikilink")，[中国队](../Page/中国.md "wikilink")3:0大胜并淘汰[伊拉克队](../Page/伊拉克.md "wikilink")，进军四强。\[1\]
  - 早上刚过八时，一架正在执行航拍任务的[军机在](../Page/飞机.md "wikilink")[河北省丰宁县四岔口乡李起龙村门土沟门自然村坠毁](../Page/河北省.md "wikilink")，机上5人全部遇难。\[2\]
  - [湖北省人民代表大会接受](../Page/湖北省.md "wikilink")[蒋超良辞去湖北省副省长职务的请求](../Page/蒋超良.md "wikilink")。\[3\]
  - [联合国安理会通过由美国提案的](../Page/联合国安理会.md "wikilink")[1556号决议](../Page/联合国安理会第1556号决议.md "wikilink")，要求[苏丹政府立即中止](../Page/苏丹共和国.md "wikilink")[达尔富尔地区的](../Page/达尔富尔.md "wikilink")[种族屠杀活动](../Page/种族屠杀.md "wikilink")。

## [7月28日](../Page/7月28日.md "wikilink")

  - [布达拉宫第一批维修项目竣工](../Page/布达拉宫.md "wikilink")。\[4\]
  - [中国北极黄河站建成](../Page/中国北极黄河站.md "wikilink")。\[5\]
  - [美国民主党全国代表大会在](../Page/美国民主党全国代表大会.md "wikilink")[波士顿正式提名](../Page/波士顿.md "wikilink")[约翰·克里为](../Page/约翰·克里.md "wikilink")[美国民主党在](../Page/美国民主党.md "wikilink")[2004年美国总统选举总统候选人](../Page/2004年美国总统选举.md "wikilink")。\[6\]

## [7月27日](../Page/7月27日.md "wikilink")

  - [荷兰足球运动员](../Page/荷兰.md "wikilink")[奥维马斯宣布正式退役](../Page/奥维马斯.md "wikilink")。\[7\]
  - [朝鲜致信](../Page/朝鲜.md "wikilink")[科菲·安南](../Page/科菲·安南.md "wikilink")，要求[联合国撤销驻](../Page/联合国.md "wikilink")[朝鲜半岛联军](../Page/朝鲜半岛.md "wikilink")。\[8\]

## [7月25日](../Page/7月25日.md "wikilink")

  - [墨西哥与](../Page/墨西哥.md "wikilink")[古巴正式恢复大使级外交关系](../Page/古巴.md "wikilink")。\[9\]
  - [中国发射](../Page/中国.md "wikilink")[探测二号卫星](../Page/探测二号卫星.md "wikilink")，实现[地球空间双星探测计划](../Page/地球空间双星探测计划.md "wikilink")。\[10\]
  - 第二届[无家可归者世界杯足球赛在](../Page/无家可归者世界杯足球赛.md "wikilink")[瑞典第二大城市](../Page/瑞典.md "wikilink")[哥德堡拉开帷幕](../Page/哥德堡.md "wikilink")。\[11\]
  - 第89届国际[世界语大会在](../Page/世界语.md "wikilink")[北京开幕](../Page/北京.md "wikilink")。\[12\]
  - 在[2004年亚洲杯足球赛比赛中](../Page/2004年亚洲杯足球赛.md "wikilink")，中国队1:0小胜[卡塔尔队](../Page/卡塔尔.md "wikilink")，在A组7分第一战绩出现。
    \[13\]

## [7月22日](../Page/7月22日.md "wikilink")

  - [美國由](../Page/美國.md "wikilink")[共和黨主導的](../Page/共和黨.md "wikilink")[眾議院表決通過](../Page/眾議院.md "wikilink")[布什政府支持的](../Page/乔治·沃克·布什.md "wikilink")”**[婚姻保護法案](../Page/婚姻保護法案.md "wikilink")**”（Marriage
    Protection
    Act），不准[聯邦法院強行各州承認在其他州禁止的](../Page/聯邦法院.md "wikilink")[同性婚姻](../Page/同性婚姻.md "wikilink")，不過本案尚待[參議院研議](../Page/參議院.md "wikilink")。\[14\]
  - [明十三陵](../Page/明十三陵.md "wikilink")[德陵抢修工程竣工](../Page/德陵.md "wikilink")。\[15\]
  - [哥伦比亚](../Page/哥伦比亚.md "wikilink")[加莱拉斯火山爆发](../Page/加莱拉斯火山.md "wikilink")。\[16\]
  - [美国公布](../Page/美国.md "wikilink")[九一一事件的最终调查报告](../Page/九一一事件.md "wikilink")。\[17\]
  - [台灣的執政黨](../Page/台灣.md "wikilink")（[民進黨](../Page/民進黨.md "wikilink")）黨主席[陳水扁透過該黨秘書長](../Page/陳水扁.md "wikilink")[張俊雄宣布三席指定中常委為](../Page/張俊雄.md "wikilink")[呂秀蓮](../Page/呂秀蓮.md "wikilink")、[游錫堃](../Page/游錫堃.md "wikilink")、[蘇貞昌](../Page/蘇貞昌.md "wikilink")。\[18\]

## [7月21日](../Page/7月21日.md "wikilink")

  - [牛津大學](../Page/牛津大學.md "wikilink")[物理學家](../Page/物理學.md "wikilink")[霍金推翻他](../Page/霍金.md "wikilink")[1975年所提出的](../Page/1975年.md "wikilink")[黑洞理論](../Page/黑洞理論.md "wikilink")，他宣稱由[黑洞並不會吞噬和消滅一切物質](../Page/黑洞.md "wikilink")，而會保留些微痕跡，最後以殘破形式吐出來。\[19\]
  - 两架[F-18大黄蜂战斗机在](../Page/F-18大黄蜂战斗机.md "wikilink")[美国](../Page/美国.md "wikilink")[俄勒冈上空相撞](../Page/俄勒冈.md "wikilink")，导致2名预备役士兵死亡，另有1人受伤。\[20\]
  - [日本首相](../Page/日本.md "wikilink")[小泉純一郎與](../Page/小泉純一郎.md "wikilink")[韓國總統](../Page/韓國.md "wikilink")[盧武鉉在](../Page/盧武鉉.md "wikilink")[濟州島舉行峰會](../Page/濟州島.md "wikilink")，兩人承諾將在今年內互訪。\[21\]
  - [韩国](../Page/韩国.md "wikilink")[汉城](../Page/汉城.md "wikilink")、[釜山](../Page/釜山.md "wikilink")、[大邱](../Page/大邱.md "wikilink")、[仁川](../Page/仁川.md "wikilink")4个城市的5个[地铁](../Page/地铁.md "wikilink")[工会举行罢工](../Page/工会.md "wikilink")。[中国日报](https://web.archive.org/web/20040726172333/http://www.chinadaily.com.cn/gb/doc/2004-07/21/content_350308.htm)
  - [蒋彦永医生在被军方关押](../Page/蒋彦永.md "wikilink")45天后获释。\[22\]
  - 在[2004年亚洲杯足球赛比赛中](../Page/2004年亚洲杯足球赛.md "wikilink")，[中国队](../Page/中国.md "wikilink")5:0大胜[印度尼西亚队](../Page/印度尼西亚.md "wikilink")，在A组领先。
    \[23\]
  - [联合国大会通过决议](../Page/联合国大会.md "wikilink")，要求以色列遵守国际法院所做出的要求以色列撤除西岸隔离墙的判决。

## [7月20日](../Page/7月20日.md "wikilink")

  - [聯合國大會以](../Page/聯合國.md "wikilink")150票對6票的壓倒性多數通過決議，要求[以色列執行](../Page/以色列.md "wikilink")[國際法庭判決](../Page/國際法庭.md "wikilink")－拆除隔離[巴勒斯坦人的西岸圍牆](../Page/巴勒斯坦.md "wikilink")。不過這個決議並無拘束力，以色列也表示不會遵守它。\[24\]
  - [美世人力资源咨询公司在](../Page/美世人力资源咨询公司.md "wikilink")[上海公布了](../Page/上海.md "wikilink")[2004年全球](../Page/2004年.md "wikilink")[城市生活成本调查报告](../Page/城市.md "wikilink")，[东京以](../Page/东京.md "wikilink")130.7分位列全球生活成本最高的城市。\[25\]
  - [中国北极科考队启程前往](../Page/中国.md "wikilink")[北极](../Page/北极.md "wikilink")，准备建立中国第一个北极考察站。\[26\]
  - [波罗的海国家及](../Page/波罗的海国家.md "wikilink")[保加利亚举行代号为](../Page/保加利亚.md "wikilink")[援救／医疗2004的大规模](../Page/援救／医疗2004.md "wikilink")[军事演习](../Page/军事演习.md "wikilink")。\[27\]
  - [微软将向来自全球](../Page/微软.md "wikilink")27个国家的“最具价值的开发团体（[MVP](../Page/MVP.md "wikilink")）”免费提供[Windows操作系统的源代码程序](../Page/Windows.md "wikilink")。\[28\]
  - [中国互联网络信息中心在](../Page/中国互联网络信息中心.md "wikilink")[北京发布了](../Page/北京.md "wikilink")《第十四次中国互联网发展状况统计报告》。\[29\]

## [7月19日](../Page/7月19日.md "wikilink")

  - [范堡罗-2004国际航空航天装备展在](../Page/范堡罗-2004.md "wikilink")[伦敦郊外的](../Page/伦敦.md "wikilink")[范堡罗正式开幕](../Page/范堡罗.md "wikilink")。[中国日报](https://web.archive.org/web/20040726172042/http://www.chinadaily.com.cn/gb/doc/2004-07/20/content_350017.htm)

## [7月18日](../Page/7月18日.md "wikilink")

  - [阿丽亚娜火箭将最重的](../Page/阿丽亚娜火箭.md "wikilink")[通信卫星送入太空](../Page/通信卫星.md "wikilink")[新华网](http://news.xinhuanet.com/st/2004-07/19/content_1613020.htm)
  - [南非前总统](../Page/南非.md "wikilink")[曼德拉庆祝](../Page/曼德拉.md "wikilink")86岁生日。
  - [巴勒斯坦武装分子对一些巴勒斯坦政府办公楼进行抢劫并纵火](../Page/巴勒斯坦.md "wikilink")，以抗议[巴勒斯坦民族权力机构主席](../Page/巴勒斯坦民族权力机构.md "wikilink")[阿拉法特任命堂弟](../Page/阿拉法特.md "wikilink")[穆萨·阿拉法特担任巴安全机构负责人](../Page/穆萨·阿拉法特.md "wikilink")。[新浪新闻](http://news.sina.com.cn/w/2004-07-19/06103122916s.shtml)。

## [7月17日](../Page/7月17日.md "wikilink")

  - [2004年亚洲杯足球赛在](../Page/2004年亚洲杯足球赛.md "wikilink")[北京开幕](../Page/北京.md "wikilink")。[新浪体育](http://sports.sina.com.cn/n/2004-07-17/19141004024.shtml)
  - [2004年亚洲杯足球赛](../Page/2004年亚洲杯足球赛.md "wikilink")，[中国队首战](../Page/中国.md "wikilink")**2：2**被[巴林队逼平](../Page/巴林.md "wikilink")。
  - [巴勒斯坦宣布](../Page/巴勒斯坦.md "wikilink")[加沙地带进入紧急状态](../Page/加沙地带.md "wikilink")，总理[库赖提出辞呈](../Page/库赖.md "wikilink")，[阿拉法特拒绝接受](../Page/阿拉法特.md "wikilink")。[新华网](http://news.xinhuanet.com/world/2004-07/17/content_1609519.htm)
  - [新加坡总理府发表声明](../Page/新加坡.md "wikilink")，新加坡现任总理[吴作栋将于](../Page/吴作栋.md "wikilink")[8月12日正式卸任](../Page/8月12日.md "wikilink")，现任副总理[李显龙将接替他并于同日宣誓就职](../Page/李显龙.md "wikilink")。[中国宁波网](http://inews.cnnb.com.cn:81/web/cnnb_asp/newscenter/gaojian.asp?id=63084)
  - 第15届[国际生物奥林匹克竞赛在](../Page/国际生物奥林匹克竞赛.md "wikilink")[澳大利亚东部城市](../Page/澳大利亚.md "wikilink")[布里斯班落下帷幕](../Page/布里斯班.md "wikilink")。[新华网](http://news.xinhuanet.com/world/2004-07/17/content_1610245.htm)

## [7月16日](../Page/7月16日.md "wikilink")

  - [第15届世界艾滋病大会在](../Page/第15届世界艾滋病大会.md "wikilink")[泰国首都](../Page/泰国.md "wikilink")[曼谷闭幕](../Page/曼谷.md "wikilink")，会议发表了《[曼谷领导人行动声明](../Page/曼谷领导人行动声明.md "wikilink")》。
    [国际在线](https://web.archive.org/web/20040911040645/http://gb.chinabroadcast.cn/3821/2004/07/16/502@234520.htm)
  - 美国家政女王[马莎.斯图尔特被判](../Page/马莎.斯图尔特.md "wikilink")5个月监禁。
  - 第9号[热带风暴](../Page/热带风暴.md "wikilink")“圆规”（KOMPASU）正面吹向[广东和](../Page/广东.md "wikilink")[香港](../Page/香港.md "wikilink")，于下午在[万宜水库登陆](../Page/万宜水库.md "wikilink")。[中新网](http://www.chinanews.com.cn/news/2004year/2004-07-16/26/460678.shtml)
  - [北京公映](../Page/北京.md "wikilink")《[十面埋伏](../Page/十面埋伏_\(电影\).md "wikilink")》。[新华网](http://news.xinhuanet.com/ent/2004-07/16/content_1604515.htm)

## [7月15日](../Page/7月15日.md "wikilink")

  - [挪威](../Page/挪威.md "wikilink")[奥斯陆地区法庭裁定](../Page/奥斯陆.md "wikilink")，命令挪威小网站google.no把[域名交给](../Page/域名.md "wikilink")[美国著名](../Page/美国.md "wikilink")[搜索引擎网站](../Page/搜索引擎.md "wikilink")[google](../Page/google.md "wikilink").com。[北京青年报](https://web.archive.org/web/20041108033515/http://bjyouth.ynet.com/article.jsp?oid=3514362)
  - 电影《[亚瑟王](../Page/亚瑟王.md "wikilink")》首映式[伦敦举行](../Page/伦敦.md "wikilink")。[新华网](http://news.xinhuanet.com/ent/2004-07/16/content_1604444.htm)
  - [雅虎在](../Page/雅虎.md "wikilink")[美国宣布](../Page/美国.md "wikilink")，旗下著名的即时通讯工具[雅虎通将与微软](../Page/雅虎通.md "wikilink")[MSN和](../Page/MSN.md "wikilink")[美国在线AIM联手](../Page/美国在线.md "wikilink")，首次实现不同提供商在企业即时通讯领域的互联互通。[TOM财经](https://web.archive.org/web/20041011151139/http://finance.tom.com/1001/1006/2004716-74394.html)
  - [柬埔寨议会重新任命](../Page/柬埔寨.md "wikilink")[洪森为首相](../Page/洪森.md "wikilink")
  - [内蒙古自治区](../Page/内蒙古自治区.md "wikilink")[扎兰屯市发生风灾](../Page/扎兰屯市.md "wikilink")。

## [7月14日](../Page/7月14日.md "wikilink")

  - 中美两国已就[集成电路](../Page/集成电路.md "wikilink")[增值税问题正式达成谅解](../Page/增值税.md "wikilink")。[中新网](http://www.chinanews.com.cn/news/2004year/2004-07-14/26/459981.shtml)
  - [中国地质博物馆经历](../Page/中国地质博物馆.md "wikilink")3年的整修后重新开馆。[北京晚报](https://web.archive.org/web/20041028032702/http://bj.ynet.com/view.jsp?oid=3498729)
  - [英国](../Page/英国.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")[史蒂芬·霍金承认其之前有关](../Page/史蒂芬·霍金.md "wikilink")[黑洞的理论有错误](../Page/黑洞.md "wikilink")。

## [7月13日](../Page/7月13日.md "wikilink")

  - [英国国防部决定](../Page/英国.md "wikilink")，由于使用[iPod播放器可能会造成军方机密的泄露](../Page/iPod.md "wikilink")，因此英国军方已经禁止使用这种播放器。[新华网](http://news.xinhuanet.com/it/2004-07/14/content_1598328.htm)
  - [基地组织的重要人物](../Page/基地组织.md "wikilink")[阿哈比](../Page/阿哈比.md "wikilink")[沙烏地阿拉伯自首](../Page/沙烏地阿拉伯.md "wikilink")。[新华网](http://news.xinhuanet.com/world/2004-07/14/content_1597756.htm)
  - [何厚铧宣布参选第二任](../Page/何厚铧.md "wikilink")[澳门特区行政长官](../Page/澳门.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2004-07/13/content_1597323.htm)
  - [法国通过法案禁止](../Page/法国.md "wikilink")[克隆人研究](../Page/克隆人.md "wikilink")。[新华网](http://news.xinhuanet.com/world/2004-07/13/content_1596928.htm)

## [7月12日](../Page/7月12日.md "wikilink")

  - 罕见[沙尘暴袭击](../Page/沙尘暴.md "wikilink")[甘肃](../Page/甘肃.md "wikilink")[酒泉](../Page/酒泉.md "wikilink")[嘉峪关等地](../Page/嘉峪关.md "wikilink")
    。[西藏在线](https://web.archive.org/web/20040722203605/http://www.tibetonline.net/news/detail.asp?id=22459)
  - [李宗盛](../Page/李宗盛.md "wikilink")、[林忆莲承认离婚](../Page/林忆莲.md "wikilink")。[新华网](http://news.xinhuanet.com/ent/2004-07/13/content_1595073.htm)
  - [日本](../Page/日本.md "wikilink")[自民党选举受挫](../Page/自由民主黨_\(日本\).md "wikilink")[小泉纯一郎拟九月重组内阁](../Page/小泉纯一郎.md "wikilink")
    [美国之音](http://www.voanews.com/Chinese/article.cfm?objectID=41B0D050-0779-4B9B-834559539EE9E116&title=%D7%D4%C3%F1%B5%B3%D1%A1%BE%D9%CA%DC%B4%EC%20%D0%A1%C8%AA%BE%C5%D4%C2%D6%D8%D7%E9%C4%DA%B8%F3%2804%C4%EA7%D4%C212%C8%D5%29)
  - [以色列不接受](../Page/以色列.md "wikilink")[海牙裁决继续建](../Page/海牙.md "wikilink")[隔离墙](../Page/隔离墙.md "wikilink")，以色列军坦克再入[加沙南部难民营](../Page/加沙.md "wikilink")
    [美国之音](http://www.voanews.com/Chinese/article.cfm?objectID=300E19EB-89EE-4DC7-80031B2B5A8B37A2&title=%D2%D4%C9%AB%C1%D0%B2%BB%BD%D3%CA%DC%BA%A3%D1%C0%B2%C3%BE%F6%D0%F8%BD%A8%B8%F4%C0%EB%C7%BD%2804%C4%EA7%D4%C211%C8%D5%29)
  - [法国外交部表示与](../Page/法国.md "wikilink")[伊拉克复交](../Page/伊拉克.md "wikilink")，从而结束了双方中断了13年的外交关系。
    [英国广播公司](http://news.bbc.co.uk/chinese/simp/hi/newsid_3880000/newsid_3886500/3886597.stm)
  - 为拯救被[伊拉克武装分子绑架的](../Page/伊拉克.md "wikilink")[菲律宾人质](../Page/菲律宾.md "wikilink")，菲律宾政府宣布将立即撤出全部驻伊部队。

## [7月11日](../Page/7月11日.md "wikilink")

  - 中国[北京](../Page/北京.md "wikilink")、[广西](../Page/广西.md "wikilink")、[贵州暴雨成灾](../Page/贵州.md "wikilink")，北京六处房屋倒塌，广西星期五晚到星期天连续降暴雨，受灾人口达五十七万。
    [RFA](http://www.rfa.org/service/index.html?service=man)
  - [第15届世界艾滋病大会在](../Page/第15届世界艾滋病大会.md "wikilink")[泰国首都](../Page/泰国.md "wikilink")[曼谷开幕](../Page/曼谷.md "wikilink")。[新华网](http://news.xinhuanet.com/world/2004-07/11/content_1591170.htm)
  - [以色列城市](../Page/以色列.md "wikilink")[特拉维夫发生恐怖爆炸](../Page/特拉维夫.md "wikilink")，
    一名妇女丧生，21人受伤[英国广播公司](http://news.bbc.co.uk/chinese/simp/hi/newsid_3880000/newsid_3883200/3883291.stm)
  - [新加坡副总理](../Page/新加坡.md "wikilink")[李显龙访问](../Page/李显龙.md "wikilink")[台湾](../Page/台湾.md "wikilink")
    [1](http://news.bbc.co.uk/chinese/simp/hi/newsid_3880000/newsid_3884000/3884051.stm)
  - [联合国秘书长](../Page/联合国秘书长.md "wikilink")[安南指出](../Page/科菲·安南.md "wikilink")[爱滋病将严重打击](../Page/爱滋病.md "wikilink")[亚洲经济](../Page/亚洲.md "wikilink")
    [2](http://news.bbc.co.uk/chinese/simp/hi/newsid_3880000/newsid_3884100/3884167.stm)

## [7月10日](../Page/7月10日.md "wikilink")

  - [中国尝试以](../Page/中国.md "wikilink")“利益导向”应对[性別比例失衡问题](../Page/性別比.md "wikilink")
    [新浪](http://news.sina.com.cn/c/2004-07-11/14063053135s.shtml)
  - [英国广播公司报道首相](../Page/英国.md "wikilink")[布莱尔在](../Page/布莱尔.md "wikilink")6月份曾考虑辞职。

## [7月9日](../Page/7月9日.md "wikilink")

  - [国际法院对](../Page/国际法院.md "wikilink")[联合国大会提出的有关](../Page/联合国大会.md "wikilink")[以色列在](../Page/以色列.md "wikilink")[约旦河西岸建造](../Page/约旦河西岸.md "wikilink")[隔离墙是否违法的询问做出咨询性答复](../Page/隔离墙.md "wikilink")，认为此举违反了[国际法](../Page/国际法.md "wikilink")，并要求联合国大会及[安理会立即采取补救措施](../Page/安理会.md "wikilink")。
  - [埃及奥贝德内阁总辞职](../Page/埃及.md "wikilink")
    [穆巴拉克任命纳齐夫为新总理](../Page/穆巴拉克.md "wikilink")
  - 在[南奥塞梯共和国被扣押的](../Page/南奥塞梯.md "wikilink")[格鲁吉亚军人获释](../Page/格鲁吉亚.md "wikilink")

## [7月8日](../Page/7月8日.md "wikilink")

  - [海因茨·菲舍尔宣誓就职](../Page/海因茨·菲舍尔.md "wikilink")[奥地利新总统](../Page/奥地利.md "wikilink")
    接替两天前逝世的[克莱斯蒂尔](../Page/克莱斯蒂尔.md "wikilink")
  - [瑞典上诉法院推翻对杀害前外交大臣](../Page/瑞典.md "wikilink")[林德的凶手终身监禁的判决](../Page/林德.md "wikilink")
  - 美国[中央情报局局长](../Page/中央情报局.md "wikilink")[特尼特正式离职](../Page/特尼特.md "wikilink")
  - 在[台湾的蒋氏后人决定在明年春将](../Page/台湾.md "wikilink")[蒋介石与](../Page/蒋介石.md "wikilink")[蒋经国的遗体在](../Page/蒋经国.md "wikilink")[台北下葬](../Page/台北.md "wikilink")。

## [7月7日](../Page/7月7日.md "wikilink")

  - 玩具 Googles from Goo 的母公司控告搜索引擎
    [Google](../Page/Google.md "wikilink")
    侵犯版权。[华盛顿时报（英文）](http://washingtontimes.com/business/20040707-100721-9065r.htm)
  - [电子游戏业内分级工作在中国启动](../Page/电子游戏.md "wikilink")。[京华时报](https://web.archive.org/web/20040826055345/http://www.bjt.net.cn/news.asp?newsid=69100)
  - [中国就](../Page/中国.md "wikilink")[日本在](../Page/日本.md "wikilink")[东海进行](../Page/东海.md "wikilink")[海洋资源调查提出严正交涉](../Page/海洋资源.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2004-07/08/content_1582064.htm)

## [7月6日](../Page/7月6日.md "wikilink")

  - [2004年美国总统选举](../Page/2004年美国总统选举.md "wikilink")：[美国民主党总统候选人](../Page/美国民主党.md "wikilink")[约翰·克里宣布了他的竞选搭档为](../Page/约翰·克里.md "wikilink")[北卡罗莱纳州民主党参议员](../Page/北卡罗莱纳州.md "wikilink")[约翰·爱德华兹](../Page/约翰·爱德华兹.md "wikilink")。[新华网](http://news.xinhuanet.com/world/2004-07/07/content_1580238.htm)
  - [奥地利总统](../Page/奥地利.md "wikilink")[克莱斯蒂尔病逝](../Page/克莱斯蒂尔.md "wikilink")
    享年71岁 离他结束两届总统任期仅36小时
  - [国际原子能机构总干事](../Page/国际原子能机构.md "wikilink")[巴拉迪开始访问](../Page/巴拉迪.md "wikilink")[以色列](../Page/以色列.md "wikilink")
    劝说[沙龙改变](../Page/沙龙.md "wikilink")[模糊政策](../Page/模糊政策.md "wikilink")
  - [伊拉克武装组织](../Page/伊拉克.md "wikilink")"拯救运动"威胁要追杀基地组织三号人物[扎卡维](../Page/扎卡维.md "wikilink")
  - [联合国发表](../Page/联合国.md "wikilink")《2004年度全球[爱滋病报告](../Page/爱滋病.md "wikilink")》：500万感染
    300万死亡
  - [英国已故王妃](../Page/英国.md "wikilink")[戴安娜纪念喷泉揭幕典礼在伦敦](../Page/戴安娜王妃.md "wikilink")[海德公园举行](../Page/海德公园.md "wikilink")
  - [联合国下属机构](../Page/联合国.md "wikilink")[国际电信联盟表示](../Page/国际电信联盟.md "wikilink")，将制定反[垃圾邮件法规](../Page/垃圾邮件.md "wikilink")，对泛滥全球[互联网的垃圾邮件进行控制](../Page/互联网.md "wikilink")，并有效打击非法[网络广告商](../Page/网络广告.md "wikilink")，力争在2年内控制住垃圾邮件的蔓延。[新浪网](http://tech.sina.com.cn/i/w/2004-07-07/1347384465.shtml)
  - [2004年美洲杯足球赛在](../Page/2004年美洲杯足球赛.md "wikilink")[秘鲁开幕](../Page/秘鲁.md "wikilink")。[新浪网](http://sports.sina.com.cn/s/2004-07-07/0507296228s.shtml)
  - 中国国家[禽流感参考实验室最终确诊发生在](../Page/禽流感.md "wikilink")[安徽省](../Page/安徽.md "wikilink")[巢湖市](../Page/巢湖市.md "wikilink")[居巢区的鸡只死亡为](../Page/居巢区.md "wikilink")[H5N1亚型高致病性禽流感](../Page/H5N1亚型高致病性禽流感.md "wikilink")。[中国医药网](https://web.archive.org/web/20040710012247/http://www.pharmnet.com.cn/news-s/01/2004-07-07/00074293.html)

## [7月5日](../Page/7月5日.md "wikilink")

  - 世界遗产委员会对"[凯恩斯决定](../Page/凯恩斯决定.md "wikilink")"做重要修改，从[2006年起](../Page/2006年.md "wikilink")，《[保护世界文化和自然遗产公约](../Page/保护世界文化和自然遗产公约.md "wikilink")》的缔约国每年可申报两项[世界遗产](../Page/世界遗产.md "wikilink")，其中至少要有一项是自然遗产。
    [3](https://web.archive.org/web/20050104050435/http://china.dayoo.com/gb/content/2004-07/06/content_1617944.htm)
  - [印度尼西亚举行历史上首次总统直选](../Page/印度尼西亚.md "wikilink")，[苏西洛·班邦·尤多约诺与现任总统](../Page/苏西洛·班邦·尤多约诺.md "wikilink")[梅加瓦蒂·苏加诺普翠得票率列前两位](../Page/梅加瓦蒂·苏加诺普翠.md "wikilink")，两人将进入第二轮的投票。

## [7月4日](../Page/7月4日.md "wikilink")

  - [希腊队在](../Page/希腊.md "wikilink")[2004年欧洲足球锦标赛的决赛中以](../Page/2004年欧洲足球锦标赛.md "wikilink")1：0击败东道主[葡萄牙队夺得冠军](../Page/葡萄牙.md "wikilink")。[4](https://web.archive.org/web/20040707043918/http://www.xinminweekly.com.cn/xwzx/tyxw/t20040705_118598.htm)
  - [纽约](../Page/纽约.md "wikilink")[自由塔奠基仪式在世贸遗址举行](../Page/自由塔.md "wikilink")。[5](http://news.xinhuanet.com/world/2004-07/05/content_1571129.htm)
  - [英國的](../Page/英國.md "wikilink")[溫布敦網球錦標賽男子单打决赛在头号种子](../Page/溫布敦網球錦標賽.md "wikilink")、卫冕冠军[瑞士人](../Page/瑞士.md "wikilink")[费德勒](../Page/费德勒.md "wikilink")（Roger
    FEDERER）与2号种子[美国人](../Page/美国.md "wikilink")[罗迪克](../Page/罗迪克.md "wikilink")（Andy
    RODDICK）之间展开，费德勒最终以4-6/7-5/7-6(3)/6-4胜出，成为继[2000年的](../Page/2000年.md "wikilink")[桑普拉斯之后](../Page/桑普拉斯.md "wikilink")，第一个成功卫冕男单冠军的选手。[6](http://sports.sina.com.cn/o/2004-07-05/0056977837.shtml)

## [7月3日](../Page/7月3日.md "wikilink")

  - [伊拉克临时政府总理](../Page/伊拉克.md "wikilink")[阿拉维表示](../Page/阿拉维.md "wikilink")，可能对那些参加反美武装运动的人进行“[大赦](../Page/大赦.md "wikilink")”，即便是曾经杀过美军的伊拉克公民也不例外。[新华社](http://news.xinhuanet.com/world/2004-07/04/content_1569117.htm)
  - [俄罗斯警方突击搜查了饱受官司困扰的](../Page/俄罗斯.md "wikilink")[石油业巨头](../Page/石油.md "wikilink")[尤科斯公司在](../Page/尤科斯公司.md "wikilink")[莫斯科的总部](../Page/莫斯科.md "wikilink")，没收了许多文件和电脑服务器。[中新社](http://www.chinanews.com.cn/news/2004year/2004-07-04/26/455577.shtml)
  - [俄罗斯共产党主席](../Page/俄罗斯共产党.md "wikilink")[久加诺夫在第十次全代会上再度当选主席](../Page/久加诺夫.md "wikilink")。之前曾有反久加诺夫的党员试图推举伊凡诺佛地区省长[齐赫诺夫为新党的党主席](../Page/齐赫诺夫.md "wikilink")。（[联合早报](http://www.zaobao.com/special/realtime/2004/07/040704_7.html)；较早前报导：[凤凰网](http://www.phoenixtv.com/home/news/world/200407/04/286396.html)[BBC](http://news.bbc.co.uk/chinese/simp/hi/newsid_3860000/newsid_3864000/3864025.stm)）
  - [英國的](../Page/英國.md "wikilink")[溫布敦網球錦標賽女子单打决赛结束](../Page/溫布敦網球錦標賽.md "wikilink")，大會13種子17岁的俄罗斯选手[莎拉波娃以](../Page/莎拉波娃.md "wikilink")6:1、6:4直落两局击败卫冕冠军、大會1種子[美国的](../Page/美国.md "wikilink")[塞雷娜·威廉姆斯](../Page/塞雷娜·威廉姆斯.md "wikilink")，夺得冠军，終結了威廉姆斯家族四連霸。[中新社](http://www.chinanews.com.cn/news/2004year/2004-07-04/26/455587.shtml)[联合报](http://udn.com/NEWS/SPORTS/SPO7/2111305.shtml)

## [7月2日](../Page/7月2日.md "wikilink")

  - [美国国务卿](../Page/美国.md "wikilink")[鲍威尔与](../Page/鲍威尔.md "wikilink")[朝鲜外长](../Page/朝鲜.md "wikilink")[白南舜在](../Page/白南舜.md "wikilink")[印尼举行了会谈](../Page/印尼.md "wikilink")，这是两国两年来最高层级的对话。[凤凰网](http://www.phoenixtv.com/home/news/world/200407/02/285618.html)
  - 中国外长[李肇星会见了](../Page/李肇星.md "wikilink")[美国国务卿鲍威尔](../Page/美国国务卿.md "wikilink")，重申中方反对美方以任何借口向[台湾出售武器](../Page/台湾.md "wikilink")。[中新社](http://www.chinanews.com.cn/news/2004year/2004-07-02/26/455279.shtml)
  - 美国四星级上将[乔治·凯西已正式接替去职的](../Page/乔治·凯西.md "wikilink")[桑切斯中将](../Page/桑切斯.md "wikilink")，出任驻伊联军最高指挥官。[中新社](http://www.chinanews.com.cn/news/2004year/2004-07-02/26/455270.shtml)
  - 台湾前[华侨商業银行行长](../Page/花旗\(台灣\)商業銀行.md "wikilink")、经济部长[戴立宁返台后表示](../Page/戴立宁.md "wikilink")，自己出任[中国证监会国际顾问团委员并未违反台湾法律](../Page/中国证监会.md "wikilink")，指[陆委会查办违反法律原则](../Page/陆委会.md "wikilink")。[联合报](http://udn.com/NEWS/NATIONAL/NAT1/2110451.shtml)
  - [2004年欧洲足球锦标赛第二场半决赛结束](../Page/2004年欧洲足球锦标赛.md "wikilink")，[希腊队凭借在加时赛上半场补时阶段的进球以](../Page/希腊.md "wikilink")1：0战胜[捷克队](../Page/捷克.md "wikilink")，历史性地杀入决赛。他们将在决赛中迎战[葡萄牙队](../Page/葡萄牙.md "wikilink")。
  - [环球嘉年华在](../Page/环球嘉年华.md "wikilink")[北京开园](../Page/北京.md "wikilink")。[7](http://news.sina.com.cn/c/2004-07-02/02093581166.shtml)
  - [台灣受敏督利颱風影響](../Page/台灣.md "wikilink")，造成[七二水災](../Page/七二水災.md "wikilink")，33人死亡12人失蹤，損失超過八十五億[新台幣](../Page/新台幣.md "wikilink")。

## [7月1日](../Page/7月1日.md "wikilink")

  - [伊拉克前总统](../Page/伊拉克.md "wikilink")[萨达姆出现在伊拉克临时政府的特别法庭上](../Page/萨达姆.md "wikilink")，听取法官对他宣读的包括[屠杀和入侵](../Page/屠杀.md "wikilink")[科威特在内的](../Page/科威特.md "wikilink")7项指控。[新华网](http://news.xinhuanet.com/world/2004-07/01/content_1562035.htm)[凤凰网](http://www.phoenixtv.com/home/news/world/200407/02/285399.html)
  - [卡西尼—惠更斯号进入环绕](../Page/卡西尼—惠更斯号.md "wikilink")[土星轨道](../Page/土星.md "wikilink")。[新华社](http://news.xinhuanet.com/world/2004-07/01/content_1560700.htm)[BBC](http://news.bbc.co.uk/chinese/simp/hi/newsid_3850000/newsid_3855400/3855457.stm)
  - [香港回归](../Page/香港.md "wikilink")7周年，30万市民上街游行，争取2007、2008年[特首与](../Page/香港特別行政區行政長官.md "wikilink")[立法会直选](../Page/香港立法會.md "wikilink")。同时刚刚结束[北京访问的](../Page/北京.md "wikilink")[香港大学学生会发表声明](../Page/香港大学.md "wikilink")，认为[普选并非解决一切问题的方法](../Page/普选.md "wikilink")，建议以更温和的方式解决政改纷争。游行：[中时](https://web.archive.org/web/20050410185546/http://news.chinatimes.com/Chinatimes/Moment/newfocus-index/0,3687,930701032+0+0+155415,00.html)[BBC](http://news.bbc.co.uk/chinese/simp/hi/newsid_3850000/newsid_3855500/3855531.stm)[苹果](https://web.archive.org/web/20050520030731/http://appledaily.atnext.com/template/apple/sec_main.cfm)[明报](https://web.archive.org/web/20050210103228/http://www.mpinews.com/content.cfm?newsid=200407011600gb41550a)；学生会声明：[成报](https://web.archive.org/web/20040701131120/http://singpao.com/20040701/local/572404.html)[文汇报](https://web.archive.org/web/20150620182635/http://www.wenweipo.com/news.phtml?cat=001YO&news_id=YO0407010004&PHPSESSID=a9d30e8ddf1bfba11bc36d5dd1a08432&PHPSESSID=2a4df65e2b57d64e05d6145e9fa4a4e2)
  - [中国](../Page/中国.md "wikilink")[广州成功获得](../Page/广州.md "wikilink")[2010年](../Page/2010年.md "wikilink")[亚运会主办权](../Page/亚运会.md "wikilink")。[8](http://sports.sina.com.cn/s/2004-07-01/2218291288s.shtml)
  - [中国在全国范围内开始实施相当于](../Page/中国.md "wikilink")[欧洲二号标准的](../Page/欧洲二号标准.md "wikilink")[国家机动车污染物排放标准第二阶段限值](../Page/国家机动车污染物排放标准第二阶段限值.md "wikilink")。[9](http://news.sohu.com/2004/07/02/52/news220815286.shtml)
  - [日本](../Page/日本.md "wikilink")[丰田汽车发布公告](../Page/丰田.md "wikilink")，将在全球范围召回18200辆[凌志LS430](../Page/凌志LS430.md "wikilink")，召回的原因是[自动变速箱个别零件有可能损坏](../Page/自动变速箱.md "wikilink")。[10](http://www.xinhuanet.com/newscenter/index.htm)
  - [西班牙东北部的](../Page/西班牙.md "wikilink")[萨拉戈萨市自](../Page/萨拉戈萨.md "wikilink")[6月15日首次发现](../Page/6月15日.md "wikilink")[军团病患者以来](../Page/军团病.md "wikilink")，至今已有23人患病住院，其中5人已死亡。这是今年第一次在西班牙发现军团病。[11](http://news.xinhuanet.com/world/2004-07/02/content_1562333.htm)
  - 第28届[世界遗产委员会会议批准中国](../Page/世界遗产.md "wikilink")[高句丽王城](../Page/高句丽.md "wikilink")、王陵及贵族墓葬项目列入世界遗产名录。[东北新闻网](../Page/东北新闻网.md "wikilink")
  - [2004年欧洲足球锦标赛](../Page/2004年欧洲足球锦标赛.md "wikilink")：第一场半决赛结束，东道主[葡萄牙以](../Page/葡萄牙.md "wikilink")2:1击败[荷兰](../Page/荷兰.md "wikilink")，成为自[1984年以来首支进入决赛的东道主球队](../Page/1984年.md "wikilink")。[新华社](http://news.xinhuanet.com/sports/2004-07/01/content_1558892.htm)
  - [黄静裸死案七人司法鉴定专家团会聚](../Page/黄静裸死案.md "wikilink")[湖南进行第五次司法鉴定](../Page/湖南.md "wikilink")，将再次尸检。[TOM新闻](https://web.archive.org/web/20040826042058/http://news.tom.com/1006/3877/200471-1051193.html)[专题](https://web.archive.org/web/20040829050601/http://news.tom.com/hot/huangjing/)
  - [河南省正式实施](../Page/河南.md "wikilink")《河南省监狱提请老病残罪犯减刑假释实施办法》和《河南省监狱老病残罪犯认定标准》，这是[中国大陆首次对老病残](../Page/中国大陆.md "wikilink")[罪犯进行](../Page/罪犯.md "wikilink")[减刑和](../Page/减刑.md "wikilink")[假释](../Page/假释.md "wikilink")。

## 參考資料

<references />

1.  [亚洲杯官方网站](http://sports.sohu.com/20040731/n221291142.shtml)
2.  [新华网](http://news.xinhuanet.com/newscenter/2004-07/31/content_1685596.htm)
3.  [新华网](http://news.xinhuanet.com/newscenter/2004-07/31/content_1686068.htm)
4.  [新华网](http://news.xinhuanet.com/st/2004-07/29/content_1670615.htm)
5.  [新华网](http://news.xinhuanet.com/st/2004-07/29/content_1667931.htm)
6.  [CNN](http://www.cnn.com/2004/ALLPOLITICS/07/29/dems.main/index.html)
7.  [新华网](http://news.xinhuanet.com/sports/2004-07/28/content_1659851.htm)
8.  [中国日报](http://news.163.com/2004w07/12626/2004w07_1090907166068.html)
9.  [新华社](http://www.jmrb.com.cn/c/2004/07/26/05/c_324397.shtml)
10. [新华网](http://news.xinhuanet.com/st/2004-07/26/content_1647925.htm)
11. [新华网](http://news.xinhuanet.com/sports/2004-07/26/content_1648143.htm)
12. [新华网](http://news.xinhuanet.com/newscenter/2004-07/25/content_1644500.htm)
13. [亚洲杯官方网站](http://www.asiancup2004.com)
14. [聯合新聞網](http://udn.com/NEWS/WORLD/WOR6/2146681.shtml)
15. [新华社](http://www.jwb.com.cn/gb/content/2004-07/23/content_250649.htm)

16. [新华社](http://www.jmrb.com.cn/c/2004/07/23/08/c_321479.shtml)
17. [新华网](http://news.xinhuanet.com/world/2004-03/24/content_1382030.htm)
18. [聯合新聞網](http://udn.com/NEWS/NATIONAL/NATS4/2145641.shtml)
19. [聯合新聞網](http://udn.com/NEWS/WORLD/WOR4/2145893.shtml)
20. [中国日报网站](http://news.xinhuanet.com/mil/2004-07/22/content_1628276.htm)
21. [聯合新聞網](http://udn.com/NEWS/WORLD/WOR3/2143763.shtml)
22. [路透社](http://www.reuters.co.uk/newsPackageArticle.jhtml?type=worldNews&storyID=550322&section=news)

23. [亚洲杯网站](http://sports.sohu.com/20040722/n221130682.shtml)
24. [聯合新聞網](http://udn.com/NEWS/WORLD/WOR3/2143773.shtml)
25. [人民网](http://www.people.com.cn/GB/shehui/1062/2655093.html)
26. [东方网-文汇报](http://news.xinhuanet.com/st/2004-07/21/content_1622686.htm)
27. [新华网](http://news.xinhuanet.com/world/2004-07/20/content_1619437.htm)
28. [新华网](http://news.xinhuanet.com/it/2004-07/20/content_1617835.htm)
29. [千龙新闻网](http://tech.qianlong.com/28/2004/07/20/71@2172650.htm)