**[2004年夏季奥林匹克运动会的](../Page/2004年夏季奥林匹克运动会.md "wikilink")[举重比赛](../Page/举重.md "wikilink")**从8月15日－8月26日在[尼卡亚奥林匹克举重馆举行](../Page/尼卡亚奥林匹克举重馆.md "wikilink")。共有250位运动员争夺15块金牌。分为[抓举和](../Page/抓举.md "wikilink")[挺举两项](../Page/挺举.md "wikilink")，共男女15个项目。

## 奖牌榜

[2000年悉尼夏季奥运会后](../Page/2000年夏季奥林匹克运动会.md "wikilink")，世界女子[举重发展很快](../Page/举重.md "wikilink")，原有的男子举重强国如[土耳其已经在](../Page/土耳其.md "wikilink")[悉尼](../Page/悉尼.md "wikilink")[奥运会之后的](../Page/奥林匹克运动会.md "wikilink")4年中利用自身优势加大了对女举的发展，女举不再是[中国举重队一枝独秀](../Page/2004年夏季奥运会中国举重队.md "wikilink")，本届[雅典](../Page/雅典.md "wikilink")[夏季奥运会](../Page/夏季奥林匹克运动会.md "wikilink")[土耳其](../Page/土耳其.md "wikilink")、[缅甸](../Page/缅甸.md "wikilink")、[泰国](../Page/泰国.md "wikilink")、[韩国](../Page/韩国.md "wikilink")、[俄罗斯等国家取得很大突破](../Page/俄罗斯.md "wikilink")，土耳其、泰国、俄罗斯等选手在[本届雅典奥运会纷纷打破](../Page/2004年夏季奥林匹克运动会.md "wikilink")[世界纪录](../Page/世界纪录.md "wikilink")。

<table>
<tbody>
<tr class="odd">
<td><p>'<strong>'排名</strong></p></td>
<td><p><strong>国家：</strong></p></td>
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
<td><p><strong>总计：</strong></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td></td>
<td><p>5</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>8</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>4</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>4</p></td>
<td><p>7</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
</tr>
</tbody>
</table>

### 男子

#### 男子56公斤级

|                                            |                                           |                                           |
| ------------------------------------------ | ----------------------------------------- | ----------------------------------------- |
| \!align="center" bgcolor="gold"|**金牌：**    | \!align="center" bgcolor="silver"|**银牌：** | \!align="center" bgcolor="CC9966"|**铜牌：** |
| [哈利勒·穆特魯](../Page/哈利勒·穆特魯.md "wikilink")（） | [吴美锦](../Page/吴美锦.md "wikilink")（）        | [阿图克](../Page/阿图克.md "wikilink")（）        |
|                                            |                                           |                                           |

#### 男子62公斤级

|                                         |                                           |                                                    |
| --------------------------------------- | ----------------------------------------- | -------------------------------------------------- |
| \!align="center" bgcolor="gold"|**金牌：** | \!align="center" bgcolor="silver"|**银牌：** | \!align="center" bgcolor="CC9966"|**铜牌：**          |
| [石智勇](../Page/石智勇.md "wikilink")（）      | [乐茂盛](../Page/乐茂盛.md "wikilink")（）        | [伊斯利尔·侯赛·鲁比欧](../Page/伊斯利尔·侯赛·鲁比欧.md "wikilink")（） |
|                                         |                                           |                                                    |

#### 男子69公斤级

|                                           |                                           |                                           |
| ----------------------------------------- | ----------------------------------------- | ----------------------------------------- |
| \!align="center" bgcolor="gold"|**金牌：**   | \!align="center" bgcolor="silver"|**银牌：** | \!align="center" bgcolor="CC9966"|**铜牌：** |
| [张国政](../Page/張國政_\(選手\).md "wikilink")（） | [李培永](../Page/李培永.md "wikilink")（）        | [佩查洛夫](../Page/佩查洛夫.md "wikilink")（）      |
|                                           |                                           |                                           |

#### 男子77公斤级

|                                         |                                           |                                           |
| --------------------------------------- | ----------------------------------------- | ----------------------------------------- |
| \!align="center" bgcolor="gold"|**金牌：** | \!align="center" bgcolor="silver"|**银牌：** | \!align="center" bgcolor="CC9966"|**铜牌：** |
| [萨吉尔](../Page/萨吉尔.md "wikilink")（）      | [费力莫诺夫](../Page/费力莫诺夫.md "wikilink")（）    | [佩里佩琴诺夫](../Page/佩里佩琴诺夫.md "wikilink")（）  |

#### 男子85公斤级

|                                         |                                           |                                           |
| --------------------------------------- | ----------------------------------------- | ----------------------------------------- |
| \!align="center" bgcolor="gold"|**金牌：** | \!align="center" bgcolor="silver"|**银牌：** | \!align="center" bgcolor="CC9966"|**铜牌：** |
| [阿桑尼德泽](../Page/阿桑尼德泽.md "wikilink")（）  | [里巴库](../Page/里巴库.md "wikilink")（）        | [迪马斯](../Page/迪马斯.md "wikilink")（）        |

原男子举重85公斤级铜牌获得者希腊的[桑帕尼斯兴奋剂检查呈阳性被取消资格](../Page/桑帕尼斯.md "wikilink")。

#### 男子94公斤级

|                                         |                                            |                                           |
| --------------------------------------- | ------------------------------------------ | ----------------------------------------- |
| \!align="center" bgcolor="gold"|**金牌：** | \!align="center" bgcolor="silver"|**银牌：**  | \!align="center" bgcolor="CC9966"|**铜牌：** |
| [多布列夫](../Page/多布列夫.md "wikilink")（）    | [阿卡耶夫](../Page/哈吉姆拉德·阿卡耶夫.md "wikilink")（） | [特尤金](../Page/特尤金.md "wikilink")（）        |
|                                         |                                            |                                           |

#### 男子105公斤级

|                                                  |                                           |                                           |
| ------------------------------------------------ | ----------------------------------------- | ----------------------------------------- |
| \!align="center" bgcolor="gold"|**金牌：**          | \!align="center" bgcolor="silver"|**银牌：** | \!align="center" bgcolor="CC9966"|**铜牌：** |
| [德米特里·别列斯托夫](../Page/德米特里·别列斯托夫.md "wikilink")（） | [久尔科维茨](../Page/久尔科维茨.md "wikilink")（）    | [拉佐罗诺夫](../Page/拉佐罗诺夫.md "wikilink")（）    |

#### 男子105公斤以上级

|                                              |                                           |                                           |
| -------------------------------------------- | ----------------------------------------- | ----------------------------------------- |
| \!align="center" bgcolor="gold"|**金牌：**      | \!align="center" bgcolor="silver"|**银牌：** | \!align="center" bgcolor="CC9966"|**铜牌：** |
| [侯赛因·拉扎扎德](../Page/侯赛因·拉扎扎德.md "wikilink")（） | [斯科巴蒂斯](../Page/斯科巴蒂斯.md "wikilink")（）    | [乔拉科夫](../Page/乔拉科夫.md "wikilink")）（）     |

在8月25日的比赛中，[伊朗选手拉扎扎德以](../Page/伊朗.md "wikilink")263.5[公斤打破了由他自己保持的挺举](../Page/公斤.md "wikilink")[奥运会和世界纪录](../Page/奥运会.md "wikilink")。

### 女子

#### 女子48公斤级

|                                            |                                           |                                            |
| ------------------------------------------ | ----------------------------------------- | ------------------------------------------ |
| \!align="center" bgcolor="gold"|**金牌：**    | \!align="center" bgcolor="silver"|**银牌：** | \!align="center" bgcolor="CC9966"|**铜牌：**  |
| [努尔詹·塔伊兰](../Page/努尔詹·塔伊兰.md "wikilink")（） | [李卓](../Page/李卓.md "wikilink")（）          | [维拉塔沃·阿里](../Page/维拉塔沃·阿里.md "wikilink")（） |
|                                            |                                           |                                            |

在8月14日进行的奥运会举重女子48公斤级比赛中，土耳其选手塔伊兰以97.5公斤打破了抓举世界纪录，并且同时以210公斤打破了总成绩世界纪录。

#### 女子53公斤级

|                                         |                                           |                                           |
| --------------------------------------- | ----------------------------------------- | ----------------------------------------- |
| \!align="center" bgcolor="gold"|**金牌：** | \!align="center" bgcolor="silver"|**银牌：** | \!align="center" bgcolor="CC9966"|**铜牌：** |
| [波尔萨克](../Page/波尔萨克.md "wikilink")（）    | [丽萨](../Page/丽萨.md "wikilink")（）          | [莫斯奎拉](../Page/莫斯奎拉.md "wikilink")（）      |
|                                         |                                           |                                           |

#### 女子58公斤级

|                                         |                                           |                                           |
| --------------------------------------- | ----------------------------------------- | ----------------------------------------- |
| \!align="center" bgcolor="gold"|**金牌：** | \!align="center" bgcolor="silver"|**银牌：** | \!align="center" bgcolor="CC9966"|**铜牌：** |
| [陈艳青](../Page/陈艳青.md "wikilink")（）      | [李成姬](../Page/李成姬.md "wikilink")（）        | [卡米亚姆](../Page/卡米亚姆.md "wikilink")（）      |
|                                         |                                           |                                           |

#### 女子63公斤级

|                                         |                                           |                                           |
| --------------------------------------- | ----------------------------------------- | ----------------------------------------- |
| \!align="center" bgcolor="gold"|**金牌：** | \!align="center" bgcolor="silver"|**银牌：** | \!align="center" bgcolor="CC9966"|**铜牌：** |
| [斯卡昆](../Page/斯卡昆.md "wikilink")（）      | [巴特休斯塔](../Page/巴特休斯塔.md "wikilink")（）    | [斯图卡拉娃](../Page/斯图卡拉娃.md "wikilink")（）    |
|                                         |                                           |                                           |

在8月18日的比赛中，白俄罗斯选手巴特休斯塔以115公斤的成绩创造了新的抓举世界记录。

#### 女子69公斤级

|                                         |                                           |                                           |
| --------------------------------------- | ----------------------------------------- | ----------------------------------------- |
| \!align="center" bgcolor="gold"|**金牌：** | \!align="center" bgcolor="silver"|**银牌：** | \!align="center" bgcolor="CC9966"|**铜牌：** |
| [刘春红](../Page/刘春红.md "wikilink")（）      | [克鲁特兹勒](../Page/克鲁特兹勒.md "wikilink")（）    | [卡萨耶娃](../Page/卡萨耶娃.md "wikilink")（）      |
|                                         |                                           |                                           |

在8月19日的比赛中，中国选手刘春红以总成绩275.0公斤以及抓举122.5公斤，挺举153公斤同时打破三项世界纪录。

#### 女子75公斤级

|                                            |                                                    |                                           |
| ------------------------------------------ | -------------------------------------------------- | ----------------------------------------- |
| \!align="center" bgcolor="gold"|**金牌：**    | \!align="center" bgcolor="silver"|**银牌：**          | \!align="center" bgcolor="CC9966"|**铜牌：** |
| [帕维那·童苏克](../Page/帕维那·童苏克.md "wikilink")（） | [娜塔丽娅·扎波洛特那娅](../Page/娜塔丽娅·扎波洛特那娅.md "wikilink")（） | [波波娃](../Page/波波娃.md "wikilink")（）        |

在8月20日的比赛中，泰国选手童苏克和俄罗斯的扎波洛特那娅以272.5公斤打破总成绩世界纪录270.0公斤。同时，俄罗斯的扎波洛特那娅以125.0公斤的抓举成绩打破了抓举世界纪录。

#### 女子75公斤以上级

|                                         |                                           |                                           |
| --------------------------------------- | ----------------------------------------- | ----------------------------------------- |
| \!align="center" bgcolor="gold"|**金牌：** | \!align="center" bgcolor="silver"|**银牌：** | \!align="center" bgcolor="CC9966"|**铜牌：** |
| [唐功红](../Page/唐功红.md "wikilink")（）      | [张美兰](../Page/张美兰.md "wikilink")（）        | [弗洛贝尔](../Page/弗洛贝尔.md "wikilink")（）      |

在8月21日的比赛上，中国选手唐功红以305.0公斤的总成绩打破世界纪录302.5公斤。

[Category:2004年夏季奧林匹克運動會比賽項目](../Category/2004年夏季奧林匹克運動會比賽項目.md "wikilink")
[Category:奥林匹克运动会举重比赛](../Category/奥林匹克运动会举重比赛.md "wikilink")