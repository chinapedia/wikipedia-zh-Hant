**[2004年夏季奥林匹克运动会的](../Page/2004年夏季奥林匹克运动会.md "wikilink")[垒球比赛](../Page/垒球.md "wikilink")**从8月14日－23日在[海林尼克奥林匹克垒球场举行](../Page/海林尼克奥林匹克垒球场.md "wikilink")。共有八支球队参加，比赛按快速垒球的规则进行。八支参赛队中除东道主球队自动入围外，其它七支球队则通过各地区的预选赛产生。

八支参赛队首先进行大循环赛，每队均要与其它七个对手相遇一次。循环赛中排名前四位的球队进入佩寄制半决赛，即由第一名球队对第二名球队，胜队直接进入决赛；负队则与第三名球队对第四名球队间的另外一场半决赛的胜队争夺另一决赛席位。

## 参赛队伍

  - [美国](../Page/美国.md "wikilink")
  - [澳大利亚](../Page/澳大利亚.md "wikilink")
  - [日本](../Page/日本.md "wikilink")
  - [中华人民共和国](../Page/中华人民共和国.md "wikilink")
  - [加拿大](../Page/加拿大.md "wikilink")
  - [中华台北](../Page/中华台北.md "wikilink")
  - [希腊](../Page/希腊.md "wikilink")
  - [意大利](../Page/意大利.md "wikilink")

## 奖牌榜

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_United_States.svg" title="fig:Flag_of_the_United_States.svg">Flag_of_the_United_States.svg</a><a href="../Page/美国.md" title="wikilink">美国</a><br />
<a href="../Page/Leah_Amico.md" title="wikilink">Leah Amico</a><br />
<a href="../Page/Laura_Berg.md" title="wikilink">Laura Berg</a><br />
<a href="../Page/Crystil_Bustos.md" title="wikilink">Crystil Bustos</a><br />
<a href="../Page/Lisa_Fernandez.md" title="wikilink">Lisa Fernandez</a><br />
<a href="../Page/Jennie_Finch.md" title="wikilink">Jennie Finch</a><br />
<a href="../Page/Tairia_Flowers.md" title="wikilink">Tairia Flowers</a><br />
<a href="../Page/Amanda_Freed.md" title="wikilink">Amanda Freed</a><br />
<a href="../Page/Lori_Hannigan.md" title="wikilink">Lori Hannigan</a><br />
<a href="../Page/Lovieanne_Jung.md" title="wikilink">Lovieanne Jung</a><br />
<a href="../Page/Kelly_Kretschman.md" title="wikilink">Kelly Kretschman</a><br />
<a href="../Page/Jessica_Mendoza.md" title="wikilink">Jessica Mendoza</a><br />
<a href="../Page/Stacey_Nuveman.md" title="wikilink">Stacey Nuveman</a><br />
<a href="../Page/Catherine_Osterman.md" title="wikilink">Catherine Osterman</a><br />
<a href="../Page/Jenny_Topping.md" title="wikilink">Jenny Topping</a><br />
<a href="../Page/Natasha_Watley.md" title="wikilink">Natasha Watley</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Australia.svg" title="fig:Flag_of_Australia.svg">Flag_of_Australia.svg</a><a href="../Page/澳大利亚.md" title="wikilink">澳大利亚</a><br />
<a href="../Page/Sandra_Allen.md" title="wikilink">Sandra Allen</a><br />
<a href="../Page/Marissa_Carpadios.md" title="wikilink">Marissa Carpadios</a><br />
<a href="../Page/Fiona_Crawford.md" title="wikilink">Fiona Crawford</a><br />
<a href="../Page/Amanda_Doman.md" title="wikilink">Amanda Doman</a><br />
<a href="../Page/Peta_Edebone.md" title="wikilink">Peta Edebone</a><br />
<a href="../Page/Tanya_Harding.md" title="wikilink">Tanya Harding</a><br />
<a href="../Page/Natalie_Hodgskin.md" title="wikilink">Natalie Hodgskin</a><br />
<a href="../Page/Simmone_Morrow.md" title="wikilink">Simmone Morrow</a><br />
<a href="../Page/Tracey_Mosley.md" title="wikilink">Tracey Mosley</a><br />
<a href="../Page/Stacey_Porter.md" title="wikilink">Stacey Porter</a><br />
<a href="../Page/Melanie_Roche.md" title="wikilink">Melanie Roche</a><br />
<a href="../Page/Natalie_Titcume.md" title="wikilink">Natalie Titcume</a><br />
<a href="../Page/Natalie_Ward.md" title="wikilink">Natalie Ward</a><br />
<a href="../Page/Brooke_Wilkins.md" title="wikilink">Brooke Wilkins</a><br />
<a href="../Page/Kerry_Wyborn.md" title="wikilink">Kerry Wyborn</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Japan.svg" title="fig:Flag_of_Japan.svg">Flag_of_Japan.svg</a> <a href="../Page/日本.md" title="wikilink">日本</a><br />
<a href="../Page/乾繪美.md" title="wikilink">乾繪美</a><br />
<a href="../Page/伊藤良惠.md" title="wikilink">伊藤良惠</a><br />
<a href="../Page/岩淵有美.md" title="wikilink">岩淵有美</a><br />
<a href="../Page/三科真澄.md" title="wikilink">三科真澄</a><br />
<a href="../Page/內藤惠美.md" title="wikilink">內藤惠美</a><br />
<a href="../Page/齋藤春香.md" title="wikilink">齋藤春香</a><br />
<a href="../Page/坂井寬子.md" title="wikilink">坂井寬子</a><br />
<a href="../Page/坂本直子.md" title="wikilink">坂本直子</a><br />
<a href="../Page/佐藤理惠.md" title="wikilink">佐藤理惠</a><br />
<a href="../Page/佐藤由希.md" title="wikilink">佐藤由希</a><br />
<a href="../Page/高山樹里.md" title="wikilink">高山樹里</a><br />
<a href="../Page/上野由岐子.md" title="wikilink">上野由岐子</a><br />
<a href="../Page/宇津木麗華.md" title="wikilink">宇津木麗華</a><br />
<a href="../Page/山田惠里.md" title="wikilink">山田惠里</a><br />
<a href="../Page/山路典子.md" title="wikilink">山路典子</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

美国队至此已经三连冠，是第一支三连冠的球队。

[Category:2004年夏季奧林匹克運動會比賽項目](../Category/2004年夏季奧林匹克運動會比賽項目.md "wikilink")
[Category:奥林匹克运动会垒球比赛](../Category/奥林匹克运动会垒球比赛.md "wikilink")