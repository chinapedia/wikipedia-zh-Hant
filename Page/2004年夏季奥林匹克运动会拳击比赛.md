**[2004年夏季奥林匹克运动会的](../Page/2004年夏季奥林匹克运动会.md "wikilink")[拳击比赛](../Page/拳击.md "wikilink")**从8月14日－8月29日举行，8月26日休息。拳击只有17-34岁的男子选手，共11枚金牌，分为11个不同的重量级别。每场比赛为四轮，每轮两分钟，中间有一分钟休息。每场比赛有五个裁判现场评分，如有三个裁判同意击中就可得分，最高分的选手为胜，比賽場地為[第二十八屆雅典奧運會奧林匹克拳擊館](../Page/第二十八屆雅典奧運會奧林匹克拳擊館.md "wikilink")。

## 赛事

## 奖牌榜

### 各国奖牌榜

<table>
<tbody>
<tr class="odd">
<td><p>'<strong>'排名</strong></p></td>
<td><p><strong>国家：</strong></p></td>
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
<td><p><strong>总计：</strong></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td></td>
<td><p>5</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>8</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>6</p></td>
</tr>
<tr class="even">
<td><p>3=</p></td>
<td></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p>3=</p></td>
<td></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>3</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
</tr>
<tr class="odd">
<td><p>8=</p></td>
<td></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
</tr>
<tr class="even">
<td><p>8=</p></td>
<td></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
</tr>
<tr class="odd">
<td><p>8=</p></td>
<td></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
</tr>
<tr class="even">
<td><p>8=</p></td>
<td></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
</tr>
<tr class="odd">
<td><p>12=</p></td>
<td></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p>12=</p></td>
<td></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p>12=</p></td>
<td></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p>12=</p></td>
<td></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p>16=</p></td>
<td></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p>16=</p></td>
<td></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p>16=</p></td>
<td></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p>16=</p></td>
<td></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p>16=</p></td>
<td></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 次蝇量级（48公斤）

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/巴特勒米.md" title="wikilink">巴特勒米</a>，</p></td>
<td><p><a href="../Page/雅尔辛卡亚.md" title="wikilink">雅尔辛卡亚</a>，</p></td>
<td><p><a href="../Page/邹市明.md" title="wikilink">邹市明</a>，<br />
<a href="../Page/卡扎科夫.md" title="wikilink">卡扎科夫</a>，</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 蝇量级（51公斤）

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/甘姆博阿.md" title="wikilink">甘姆博阿</a>，</p></td>
<td><p><a href="../Page/托马斯.md" title="wikilink">托马斯</a>，</p></td>
<td><p><a href="../Page/阿斯兰诺夫.md" title="wikilink">阿斯兰诺夫</a>，<br />
<a href="../Page/拉希莫夫.md" title="wikilink">拉希莫夫</a>，</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 雛量级（54公斤）

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雷贡杜克斯.md" title="wikilink">雷贡杜克斯</a>，</p></td>
<td><p><a href="../Page/佩特奇库姆.md" title="wikilink">佩特奇库姆</a>，</p></td>
<td><p><a href="../Page/马曼多夫.md" title="wikilink">马曼多夫</a>，<br />
<a href="../Page/苏尔托诺夫.md" title="wikilink">苏尔托诺夫</a>，</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 羽量级（57公斤）

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蒂切琴科.md" title="wikilink">蒂切琴科</a>，</p></td>
<td><p><a href="../Page/金荣国.md" title="wikilink">金荣国</a>，</p></td>
<td><p><a href="../Page/塔伊伯特.md" title="wikilink">塔伊伯特</a>，<br />
<a href="../Page/赵承焕.md" title="wikilink">赵承焕</a>，</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 轻量级（60公斤）

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金德兰·梅萨.md" title="wikilink">金德兰·梅萨</a>，</p></td>
<td><p><a href="../Page/阿米爾·汗_(拳擊手).md" title="wikilink">阿米爾·汗</a>，</p></td>
<td><p><a href="../Page/叶勒耶夫.md" title="wikilink">叶勒耶夫</a> <br />
<a href="../Page/卡拉切夫.md" title="wikilink">卡拉切夫</a>，</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 次沉量级（64公斤）

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/曼努斯.md" title="wikilink">曼努斯</a>，</p></td>
<td><p><a href="../Page/约翰森.md" title="wikilink">约翰森</a>，</p></td>
<td><p><a href="../Page/格奥尔基.md" title="wikilink">格奥尔基</a>，<br />
<a href="../Page/乔吉耶夫.md" title="wikilink">乔吉耶夫</a>，</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 沉量级（69公斤）

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿特亚耶夫.md" title="wikilink">阿特亚耶夫</a>，</p></td>
<td><p><a href="../Page/阿拉贡.md" title="wikilink">阿拉贡</a>，</p></td>
<td><p><a href="../Page/金贞柱.md" title="wikilink">金贞柱</a>，<br />
<a href="../Page/塞托夫.md" title="wikilink">塞托夫</a>，</p></td>
</tr>
</tbody>
</table>

### 中量级（75公斤）

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/盖达贝科夫.md" title="wikilink">盖达贝科夫</a>，</p></td>
<td><p><a href="../Page/根纳季·戈洛夫金.md" title="wikilink">戈洛夫金</a>，</p></td>
<td><p><a href="../Page/苏里亚.md" title="wikilink">苏里亚</a>，<br />
<a href="../Page/迪雷尔.md" title="wikilink">迪雷尔</a>，</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 次重量级（81公斤）

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/瓦德.md" title="wikilink">瓦德</a>，</p></td>
<td><p><a href="../Page/亚利普加德吉耶夫.md" title="wikilink">亚利普加德吉耶夫</a>，</p></td>
<td><p><a href="../Page/伊斯梅尔.md" title="wikilink">伊斯梅尔</a>，<br />
<a href="../Page/海达洛夫.md" title="wikilink">海达洛夫</a>，</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 重量级（91公斤）

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/索利斯.md" title="wikilink">索利斯</a>，</p></td>
<td><p><a href="../Page/祖耶夫.md" title="wikilink">祖耶夫</a>，</p></td>
<td><p><a href="../Page/埃尔赛义德.md" title="wikilink">埃尔赛义德</a>，<br />
<a href="../Page/阿尔沙米.md" title="wikilink">阿尔沙米</a>，</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 超重量级（91公斤以上）

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波维特金.md" title="wikilink">波维特金</a>，</p></td>
<td><p><a href="../Page/阿里.md" title="wikilink">阿里</a>，</p></td>
<td><p><a href="../Page/洛佩兹·努涅兹.md" title="wikilink">洛佩兹·努涅兹</a>，<br />
<a href="../Page/卡马雷莱.md" title="wikilink">卡马雷莱</a>，</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

[Category:2004年夏季奧林匹克運動會比賽項目](../Category/2004年夏季奧林匹克運動會比賽項目.md "wikilink")
[Category:奥林匹克运动会拳击比赛](../Category/奥林匹克运动会拳击比赛.md "wikilink")