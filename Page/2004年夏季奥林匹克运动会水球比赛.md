**[2004年夏季奥林匹克运动会的](../Page/2004年夏季奥林匹克运动会.md "wikilink")[水球比赛](../Page/水球.md "wikilink")**在雅典奥林匹克体育中心的水上中心举行。设男、女两个项目，共222名运动员参加比赛，男子12个队，每队13名运动员；女子8个队，每队11名运动员。

## 男子

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><br />
<a href="../Page/Tibor_Benedek.md" title="wikilink">Tibor Benedek</a><br />
<a href="../Page/Peter_Biros.md" title="wikilink">Peter Biros</a><br />
<a href="../Page/Rajmund_Fodor.md" title="wikilink">Rajmund Fodor</a><br />
<a href="../Page/Istvan_Gergely.md" title="wikilink">Istvan Gergely</a><br />
<a href="../Page/Tamas_Kasas.md" title="wikilink">Tamas Kasas</a><br />
<a href="../Page/Gergely_Kiss.md" title="wikilink">Gergely Kiss</a><br />
<a href="../Page/Norbert_Madaras.md" title="wikilink">Norbert Madaras</a><br />
<a href="../Page/Tamas_Molnar.md" title="wikilink">Tamas Molnar</a><br />
<a href="../Page/Adam_Steinmetz.md" title="wikilink">Adam Steinmetz</a><br />
<a href="../Page/Barnabas_Steinmetz.md" title="wikilink">Barnabas Steinmetz</a><br />
<a href="../Page/Zoltan_Szecsi.md" title="wikilink">Zoltan Szecsi</a><br />
<a href="../Page/Tamas_Varga.md" title="wikilink">Tamas Varga</a><br />
<a href="../Page/Attila_Vari.md" title="wikilink">Attila Vari</a></p></td>
<td><p><br />
<a href="../Page/Aleksandar_Ciric.md" title="wikilink">Aleksandar Ciric</a><br />
<a href="../Page/Vladimir_Gojkovic.md" title="wikilink">Vladimir Gojkovic</a><br />
<a href="../Page/Danilo_Ikodinovic.md" title="wikilink">Danilo Ikodinovic</a><br />
<a href="../Page/Viktor_Jelenic.md" title="wikilink">Viktor Jelenic</a><br />
<a href="../Page/Predrag_Jokic.md" title="wikilink">Predrag Jokic</a><br />
<a href="../Page/Nikola_Kuljaca.md" title="wikilink">Nikola Kuljaca</a><br />
<a href="../Page/Slobodan_Nikic.md" title="wikilink">Slobodan Nikic</a><br />
<a href="../Page/Aleksandar_Sapic.md" title="wikilink">Aleksandar Sapic</a><br />
<a href="../Page/Dejan_Savic.md" title="wikilink">Dejan Savic</a><br />
<a href="../Page/Denis_Sefik.md" title="wikilink">Denis Sefik</a><br />
<a href="../Page/Petar_Trbojevic.md" title="wikilink">Petar Trbojevic</a><br />
<a href="../Page/Vanja_Udovicic.md" title="wikilink">Vanja Udovicic</a><br />
<a href="../Page/Vladimir_Vujasinovic.md" title="wikilink">Vladimir Vujasinovic</a></p></td>
<td><p><br />
<a href="../Page/Roman_Balashov.md" title="wikilink">Roman Balashov</a><br />
<a href="../Page/Revaz_Chomakhidze.md" title="wikilink">Revaz Chomakhidze</a><br />
<a href="../Page/Alexander_Eryshov.md" title="wikilink">Alexander Eryshov</a><br />
<a href="../Page/Alexander_Fedorov.md" title="wikilink">Alexander Fedorov</a><br />
<a href="../Page/Sergey_Garbuzov.md" title="wikilink">Sergey Garbuzov</a><br />
<a href="../Page/Dmitry_Gorshkov.md" title="wikilink">Dmitry Gorshkov</a><br />
<a href="../Page/Nikolay_Kozlov.md" title="wikilink">Nikolay Kozlov</a><br />
<a href="../Page/Nikolay_Maksimov.md" title="wikilink">Nikolay Maksimov</a><br />
<a href="../Page/Andrey_Rekechinsky.md" title="wikilink">Andrey Rekechinsky</a><br />
<a href="../Page/Dmitry_Stratan.md" title="wikilink">Dmitry Stratan</a><br />
<a href="../Page/Vitaly_Yurchik.md" title="wikilink">Vitaly Yurchik</a><br />
<a href="../Page/Marat_Zakirov.md" title="wikilink">Marat Zakirov</a><br />
<a href="../Page/Irek_Zinnurov.md" title="wikilink">Irek Zinnurov</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 女子

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><br />
<a href="../Page/Francesca_Conti.md" title="wikilink">Francesca Conti</a><br />
<a href="../Page/Martina_Miceli.md" title="wikilink">Martina Miceli</a><br />
<a href="../Page/Carmela_Allucci.md" title="wikilink">Carmela Allucci</a><br />
<a href="../Page/Silvia_Bosurgi.md" title="wikilink">Silvia Bosurgi</a><br />
<a href="../Page/Elena_Gigli.md" title="wikilink">Elena Gigli</a><br />
<a href="../Page/Manuela_Zanchi.md" title="wikilink">Manuela Zanchi</a><br />
<a href="../Page/Tania_Di_Mario.md" title="wikilink">Tania Di Mario</a><br />
<a href="../Page/Cinzia_Ragusa.md" title="wikilink">Cinzia Ragusa</a><br />
<a href="../Page/Giusy_Malato.md" title="wikilink">Giusy Malato</a><br />
<a href="../Page/Alexandra_Araujo.md" title="wikilink">Alexandra Araujo</a><br />
<a href="../Page/Maddalena_Musumeci.md" title="wikilink">Maddalena Musumeci</a><br />
<a href="../Page/Melania_Grego.md" title="wikilink">Melania Grego</a><br />
<a href="../Page/Noemi_Toth.md" title="wikilink">Noemi Toth</a><br />
</p></td>
<td><p><br />
<a href="../Page/Georgia_Ellinaki.md" title="wikilink">Georgia Ellinaki</a><br />
<a href="../Page/Dimitra_Asilian.md" title="wikilink">Dimitra Asilian</a><br />
<a href="../Page/Antiopi_Melidoni.md" title="wikilink">Antiopi Melidoni</a><br />
<a href="../Page/Angeliki_Karapataki.md" title="wikilink">Angeliki Karapataki</a><br />
<a href="../Page/Kyriaki_Liosi.md" title="wikilink">Kyriaki Liosi</a><br />
<a href="../Page/Stavroula_Kozompoli.md" title="wikilink">Stavroula Kozompoli</a><br />
<a href="../Page/Aikaterini_Oikonomopoulou.md" title="wikilink">Aikaterini Oikonomopoulou</a><br />
<a href="../Page/Antigoni_Roumpesi.md" title="wikilink">Antigoni Roumpesi</a><br />
<a href="../Page/Evangelia_Moraitidou.md" title="wikilink">Evangelia Moraitidou</a><br />
<a href="../Page/Eftychia_Karagianni.md" title="wikilink">Eftychia Karagianni</a><br />
<a href="../Page/Georgia_Lara.md" title="wikilink">Georgia Lara</a><br />
<a href="../Page/Antonia_Moraiti.md" title="wikilink">Antonia Moraiti</a><br />
<a href="../Page/Anthoula_Mylonaki.md" title="wikilink">Anthoula Mylonaki</a><br />
</p></td>
<td><p><br />
<a href="../Page/Jacqueline_Frank.md" title="wikilink">Jacqueline Frank</a><br />
<a href="../Page/Heather_Petri.md" title="wikilink">Heather Petri</a><br />
<a href="../Page/Ericka_Lorenz.md" title="wikilink">Ericka Lorenz</a><br />
<a href="../Page/Brenda_Villa.md" title="wikilink">Brenda Villa</a><br />
<a href="../Page/Ellen_Estes.md" title="wikilink">Ellen Estes</a><br />
<a href="../Page/Natalie_Golda.md" title="wikilink">Natalie Golda</a><br />
<a href="../Page/Margaret_Dingeldein.md" title="wikilink">Margaret Dingeldein</a><br />
<a href="../Page/Kelly_Rulon.md" title="wikilink">Kelly Rulon</a><br />
<a href="../Page/Heather_Moody.md" title="wikilink">Heather Moody</a><br />
<a href="../Page/Robin_Beauregard.md" title="wikilink">Robin Beauregard</a><br />
<a href="../Page/Amber_Stachowski.md" title="wikilink">Amber Stachowski</a><br />
<a href="../Page/Nicolle_Payne.md" title="wikilink">Nicolle Payne</a><br />
<a href="../Page/Thalia_Munro.md" title="wikilink">Thalia Munro</a><br />
</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

[Category:2004年夏季奧林匹克運動會比賽項目](../Category/2004年夏季奧林匹克運動會比賽項目.md "wikilink")
[Category:奥林匹克运动会水球比赛](../Category/奥林匹克运动会水球比赛.md "wikilink")
[Category:2004年水球](../Category/2004年水球.md "wikilink")