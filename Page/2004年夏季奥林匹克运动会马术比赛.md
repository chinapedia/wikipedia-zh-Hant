**[2004年夏季奥林匹克运动会的](../Page/2004年夏季奥林匹克运动会.md "wikilink")[马术比赛](../Page/马术.md "wikilink")**从8月15日到8月28日在[马可波罗奥林匹克马术中心举行](../Page/马可波罗奥林匹克马术中心.md "wikilink")。分为盛装舞步赛、障碍赛和三日赛三项，每项均设团体和个人金牌，共产生6枚金牌。

## 奖牌榜

### 各国奖牌榜

<table>
<tbody>
<tr class="odd">
<td><p>'<strong>'排名</strong></p></td>
<td><p><strong>国家：</strong></p></td>
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
<td><p><strong>总计：</strong></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>5</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>4</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p>4=</p></td>
<td></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p>4=</p></td>
<td></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p>4=</p></td>
<td></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
</tr>
</tbody>
</table>

### 盛装舞步

#### 个人盛装舞步

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><br />
<a href="../Page/格伦斯文.md" title="wikilink">格伦斯文</a><br />
盐贩</p></td>
<td><p><br />
<a href="../Page/萨兹格伯.md" title="wikilink">萨兹格伯</a><br />
顽固</p></td>
<td><p><br />
<a href="../Page/萨拉特.md" title="wikilink">萨拉特</a><br />
美女</p></td>
</tr>
</tbody>
</table>

#### 团体盛装舞步

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><br />
<a href="../Page/Heike_Kemmer.md" title="wikilink">Heike Kemmer</a> Bonaparte<br />
<a href="../Page/Hubertus_Schmidt.md" title="wikilink">Hubertus Schmidt</a> Wansuela Suerte<br />
<a href="../Page/Martin_Schaudt.md" title="wikilink">Martin Schaudt</a> Weltall<br />
<a href="../Page/Ulla_Salzgerber.md" title="wikilink">Ulla Salzgerber</a> Rusty</p></td>
<td><p><br />
<a href="../Page/Beatriz_Ferrer-Salat.md" title="wikilink">Beatriz Ferrer-Salat</a> Beauvalais<br />
<a href="../Page/Juan_Antonio_Jimenez.md" title="wikilink">Juan Antonio Jimenez</a> Guizo<br />
<a href="../Page/Ignacio_Rambla.md" title="wikilink">Ignacio Rambla</a> Oleaja<br />
<a href="../Page/Rafael_Solto.md" title="wikilink">Rafael Solto</a> Invasor</p></td>
<td><p><br />
<a href="../Page/Lisa_Wilcox.md" title="wikilink">Lisa Wilcox</a> Relevant 5<br />
<a href="../Page/Günter_Seidel.md" title="wikilink">Günter Seidel</a> Aragon<br />
<a href="../Page/Deborah_McDonald.md" title="wikilink">Deborah McDonald</a> Brentina<br />
<a href="../Page/Robert_Dover.md" title="wikilink">Robert Dover</a> Kennedy</p></td>
</tr>
</tbody>
</table>

### 障碍赛

#### 个人障碍赛

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><br />
<a href="../Page/佩索阿.md" title="wikilink">佩索阿</a><br />
Baloubet de Rouet</p></td>
<td><p><br />
<a href="../Page/卡普勒.md" title="wikilink">卡普勒</a><br />
Royal Kaliber</p></td>
<td><p><br />
<a href="../Page/Marco_Kutscher.md" title="wikilink">Marco Kutscher</a><br />
Montender 2</p></td>
</tr>
</tbody>
</table>

**注：**[爱尔兰选手](../Page/爱尔兰.md "wikilink")[奇·奥康纳原本夺得金牌](../Page/奇·奥康纳.md "wikilink")，但2005年3月28日，[国际马术联合会取消了他的成绩](../Page/国际马术联合会.md "wikilink")，原因是他的赛马“沃特福德水晶”的尿样中“被检测出[氟菲那嗪等](../Page/氟菲那嗪.md "wikilink")[安定类的违禁药物](../Page/地西泮.md "wikilink")”。

#### 团体障碍赛

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><br />
<a href="../Page/Peter_Wylde.md" title="wikilink">Peter Wylde</a><br />
Fein Cera<br />
<a href="../Page/Ward_McLain.md" title="wikilink">Ward McLain</a><br />
Sapphire<br />
<a href="../Page/Beezie_Madden.md" title="wikilink">Beezie Madden</a><br />
Authentic<br />
<a href="../Page/Chris_Kappler.md" title="wikilink">Chris Kappler</a><br />
Royal Kaliber</p></td>
<td><p><br />
<a href="../Page/Rolf-Goran_Bengtsson.md" title="wikilink">Rolf-Goran Bengtsson</a><br />
Mac Kinley<br />
<a href="../Page/Malin_Baryard.md" title="wikilink">Malin Baryard</a><br />
Butterfly Flip<br />
<a href="../Page/Peter_Eriksson.md" title="wikilink">Peter Eriksson</a><br />
Cardento<br />
<a href="../Page/Peder_Fredericson.md" title="wikilink">Peder Fredericson</a><br />
Magic Bengtsson</p></td>
<td><p><br />
<a href="../Page/Otto_Bercker.md" title="wikilink">Otto Bercker</a><br />
Cento<br />
<a href="../Page/Ludger_Beerbaum.md" title="wikilink">Ludger Beerbaum</a><br />
Goldfever<br />
<a href="../Page/Christian_Ahlmann.md" title="wikilink">Christian Ahlmann</a><br />
Coster<br />
<a href="../Page/Marco_Kutscher.md" title="wikilink">Marco Kutscher</a><br />
Montender</p></td>
</tr>
</tbody>
</table>

### 三日赛

#### 个人三日赛

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/莱斯利·劳.md" title="wikilink">莱斯利·劳</a><br />
Shear L'Eau</p></td>
<td><p><a href="../Page/斯文森.md" title="wikilink">斯文森</a><br />
Winsome Adante</p></td>
<td><p><a href="../Page/Pippa_Funnell.md" title="wikilink">Pippa Funnell</a><br />
Primmore's Pride</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 团体三日赛

<table>
<tbody>
<tr class="odd">
<td><p>!align="center" bgcolor="gold"|<strong>金牌：</strong></p></td>
<td><p>!align="center" bgcolor="silver"|<strong>银牌：</strong></p></td>
<td><p>!align="center" bgcolor="CC9966"|<strong>铜牌：</strong></p></td>
</tr>
<tr class="even">
<td><p><br />
<a href="../Page/Arnaud_Boiteau.md" title="wikilink">Arnaud Boiteau</a><br />
Expo du Moulin<br />
<a href="../Page/Cédric_Lyard.md" title="wikilink">Cédric Lyard</a><br />
Fine Merveille<br />
<a href="../Page/Didier_Courrèges.md" title="wikilink">Didier Courrèges</a><br />
Débat D'Estruval<br />
<a href="../Page/Jean_Teulère.md" title="wikilink">Jean Teulère</a><br />
Espoir de la Mère<br />
<a href="../Page/Nicolas_Touzaint.md" title="wikilink">Nicolas Touzaint</a><br />
Galan de Sauvegère</p></td>
<td><p><br />
<a href="../Page/Jeanette_Brakewell.md" title="wikilink">Jeanette Brakewell</a><br />
Over To You<br />
<a href="../Page/Mary_King.md" title="wikilink">Mary King</a><br />
King Solomon III<br />
<a href="../Page/Leslie_Law.md" title="wikilink">Leslie Law</a><br />
Shear L'Eau<br />
<a href="../Page/Philippa_Funnell.md" title="wikilink">Philippa Funnell</a><br />
Primmore's Pride<br />
<a href="../Page/William_Fox-Pitt.md" title="wikilink">William Fox-Pitt</a><br />
Tamarillo</p></td>
<td><p><br />
<a href="../Page/Kimberly_Severson.md" title="wikilink">Kimberly Severson</a><br />
Winsome Adante<br />
<a href="../Page/Darren_Chiacchia.md" title="wikilink">Darren Chiacchia</a><br />
Windfall 2<br />
<a href="../Page/John_Williams_(equestrian_athlete).md" title="wikilink">John Williams</a><br />
Carrick<br />
<a href="../Page/Amy_Tryon.md" title="wikilink">Amy Tryon</a><br />
Poggio II<br />
<a href="../Page/Julie_Richards.md" title="wikilink">Julie Richards</a><br />
Jacob Two Two<br />
</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

在8月18日的比赛中，原本德国是第一名，但是1个小时以后德国队的金牌被取消，原因是德国队女选手[贝蒂娜·霍伊在比赛中被罚的](../Page/贝蒂娜·霍伊.md "wikilink")12分没有记入总成绩。德国队变成第三名。经过德国队上诉，罚掉12分又被从罚分中去掉，金牌又重新还给了德国人。组委会发表声明说：仲裁委员会认为在比赛开始前的倒计时阶段是不应该有罚分的（即使队员在这个时间内发生失误）。

美国、法国和英国随即向体育仲裁法庭提出了抗议，三名法官作出最终判决，推翻了马术协会的决定，令处罚再次生效。这一事件被德国媒体称为“这可能是德国奥运史上最黑暗的一刻。”

[Category:2004年夏季奧林匹克運動會比賽項目](../Category/2004年夏季奧林匹克運動會比賽項目.md "wikilink")