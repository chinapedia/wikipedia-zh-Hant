**2004年美國總統選舉**於2004年11月2日舉行，時任總統[喬治·沃克·布希成功連任](../Page/喬治·沃克·布希.md "wikilink")，這次他成功同時贏得普選票及[選舉人票](../Page/選舉人票.md "wikilink")。2005年1月6日選舉人投票結束，確定總統人選，他於2005年1月20日宣誓就職。

## 結果

| <font color="gray">总统候选人</font>                                                                                                                                                                                                                                                                                                      | <font color="gray">选举人票</font> | <font color="gray">普选票数</font> | <font color="gray">百分比</font> | <font color="gray">政党</font>            | <font color="gray">竞选伙伴（选举人票数）</font>    |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------------ | ------------------------------ | ----------------------------- | --------------------------------------- | ---------------------------------------- |
| [乔治·W·布什](../Page/乔治·W·布什.md "wikilink")（胜）                                                                                                                                                                                                                                                                                          | 286                            | 62,040,610                     | 50.7                          | [共和党](../Page/共和党_\(美国\).md "wikilink") | [理察·切尼](../Page/理察·切尼.md "wikilink")     |
| [约翰·克里](../Page/约翰·克里.md "wikilink")                                                                                                                                                                                                                                                                                                 | 251                            | 59,028,444                     | 48.3                          | [民主党](../Page/民主党_\(美国\).md "wikilink") | [约翰·爱德华兹](../Page/约翰·爱德华兹.md "wikilink") |
| [拉尔夫·纳德](../Page/拉尔夫·纳德.md "wikilink")                                                                                                                                                                                                                                                                                               | 0                              | 394,794                        | 0.34                          | 獨立                                      | Peter Miguel Camejo                      |
| Michael Badnarik                                                                                                                                                                                                                                                                                                                     | 0                              | 377,792                        | 0.33                          | [自由党](../Page/自由党_\(美国\).md "wikilink") | Richard Campagna                         |
| Michael Peroutka                                                                                                                                                                                                                                                                                                                     | 0                              | 129,337                        | 0.11                          | [宪法党](../Page/宪法党_\(美国\).md "wikilink") | Chuck Baldwin                            |
| [大衛·柯布](../Page/大衛·柯布.md "wikilink")                                                                                                                                                                                                                                                                                                 | 0                              | 104,917                        | 0.09                          | [绿党](../Page/绿党_\(美国\).md "wikilink")   | Patricia LaMarche                        |
| **總計**                                                                                                                                                                                                                                                                                                                               | **538**                        | \-                             | **100**                       |                                         |                                          |
| **其他年度的总统选举**：[1988年](../Page/1988年美国总统选举.md "wikilink")，[1992年](../Page/1992年美国总统选举.md "wikilink")，[1996年](../Page/1996年美国总统选举.md "wikilink")，[2000年](../Page/2000年美国总统选举.md "wikilink")，**2004年**，[2008年](../Page/2008年美国总统选举.md "wikilink")，[2012年](../Page/2012年美国总统选举.md "wikilink")，[2016年](../Page/2016年美国总统选举.md "wikilink") |                                |                                |                               |                                         |                                          |
| *[来源：U.S. Office of the Federal Register](http://www.archives.gov/federal_register/electoral_college/scores.html#2004)*                                                                                                                                                                                                              |                                |                                |                               |                                         |                                          |

## 候选人

### [共和党](../Page/共和党_\(美国\).md "wikilink")

候选人[乔治·W·布什](../Page/乔治·W·布什.md "wikilink")（时任美国总统）。

  - 登记：
      - [乔治·W·布什](../Page/乔治·W·布什.md "wikilink")（时任美国总统）
      - [Edie Bukewihge](../Page/Edie_Bukewihge.md "wikilink")

### [民主党](../Page/民主党_\(美国\).md "wikilink")

候选人[约翰·克里](../Page/约翰·克里.md "wikilink")（John
Kerry）是在2004年美国民主党预选中选出。副总统候選人参议员[约翰·爱德华兹](../Page/约翰·爱德华兹.md "wikilink")（John
Edwards）则由大会决定。

  - 登记：
      - [约翰·克里](../Page/约翰·克里.md "wikilink")（John Kerry），1985年起擔任聯邦參議員。
      - [丹尼斯·库钦奇](../Page/丹尼斯·库钦奇.md "wikilink")（Dennis Kucinich），联邦众议员。
      - [阿爾·夏普頓牧师](../Page/阿爾·夏普頓.md "wikilink")（Reverend Al Sharpton），
  - 退出选举：
      - 参议员[Bob Graham](../Page/Bob_Graham.md "wikilink")（2003年10月7日）
      - 前参议员[卡罗尔·莫斯利·布劳恩](../Page/卡罗尔·莫斯利·布劳恩.md "wikilink")（Carol
        Moseley
        Braun，2004年1月16日），转向支持[霍华德·迪安](../Page/霍华德·迪安.md "wikilink")（Howard
        Dean）
      - 众议员[里查德·格普哈特](../Page/里查德·格普哈特.md "wikilink")（Richard
        Gephardt，2004年1月19日）
      - 参议员[约瑟夫·利伯曼](../Page/约瑟夫·利伯曼.md "wikilink")（Joseph
        Lieberman，2004年2月3日）
      - [韦斯利·克拉克上将](../Page/韦斯利·克拉克.md "wikilink")（Wesley
        Clark，2004年2月11日），转向支持克里。
      - 前任[佛蒙特州州长](../Page/佛蒙特州.md "wikilink")[霍华德·迪安](../Page/霍华德·迪安.md "wikilink")（Howard
        Dean，2004年2月18日）。
      - [约翰·爱德华兹](../Page/约翰·爱德华兹.md "wikilink")（John Edwards,
        2004年3月3日），1999年起擔任聯邦参议员。
  - 表示不参加竞选：[阿尔·戈尔](../Page/阿尔·戈尔.md "wikilink")（Al
    Gore）、[希拉里·克林顿](../Page/希拉里·克林顿.md "wikilink")（Hillary
    Clinton），[喬·拜登](../Page/喬·拜登.md "wikilink")（Joe Biden），[Tom
    Daschle](../Page/Tom_Daschle.md "wikilink")，[克里斯多夫·杜德](../Page/克里斯多夫·杜德.md "wikilink")，[Gary
    Hart](../Page/Gary_Hart.md "wikilink")

### [自由党](../Page/自由党_\(美国\).md "wikilink")

2004年5月30日，於[乔治亚州](../Page/乔治亚州.md "wikilink")[阿特蘭大舉行的黨大會選出](../Page/阿特蘭大.md "wikilink")[迈克尔·班纳瑞克](../Page/迈克尔·班纳瑞克.md "wikilink")（Michael
Badnarik）為總統候選人。副總統候選人是Richard Campagna。在选举中得到0.32%的选票，略少于独立候选人Ralph
Nader的0.36%，但比其他第三党总统候选人的得票总和还多。

### [绿党](../Page/绿党_\(美国\).md "wikilink")

2004年6月25日美国绿党在黨大會的第二輪選舉中選出David Cobb為總統候選人。副總統候選人是Pat LaMarche。

### [宪法党](../Page/宪法党_\(美国\).md "wikilink")

2004年6月25日美国宪党提名Michael Peroutka參選總統和Chuck Baldwin參選副總統。

## 时间表

### 2002年

  - 12月15日，前副总统戈尔宣布不会参加2004年总统竞选。\[1\]

### 2003年

  - 1月2日：美国[北卡罗来纳州民主党参议员](../Page/北卡罗来纳州.md "wikilink")[约翰·爱德华兹表示他将参加](../Page/约翰·爱德华兹.md "wikilink")2004年美国总统选举。
  - 1月13日：前副总统戈尔的竞选伙伴[约瑟夫·利伯曼表示他将参加](../Page/约瑟夫·利伯曼.md "wikilink")2004年美国总统选举。
  - 2月18日：前[伊利諾伊州女参议员](../Page/伊利諾伊州.md "wikilink")、民主党人[卡罗尔·莫斯利·布劳恩表示她将参加](../Page/卡罗尔·莫斯利·布劳恩.md "wikilink")2004年美国总统选举。
  - 2月19日：民主党众议员[里查德·格普哈特表示他将参加](../Page/里查德·格普哈特.md "wikilink")2004年美国总统选举。
  - 5月6日：美国[佛罗里达州民主党参议员](../Page/佛罗里达州.md "wikilink")[格雷厄姆表示他将参加](../Page/格雷厄姆.md "wikilink")2004年美国总统选举。

### 2004年

  - 1月15日：美国总统大选唯一一名女性竞选人[卡罗尔·莫斯利·布劳恩表示退出总统选举](../Page/卡罗尔·莫斯利·布劳恩.md "wikilink")。
  - 1月19日：民主党2004年总统选举的首次政党基层会议将在[爱荷华州](../Page/爱荷华州.md "wikilink")99个县市的1993个选区同时举行，从而标志着美国大选初选的序幕正式拉开，它将和27日在[新罕布什尔州举行的首次初选一道](../Page/新罕布什尔州.md "wikilink")，成为影响今年总统选举重要的风向标。
  - 1月20日：民主党党内总统候选人提名在[爱荷华州的选举结束](../Page/爱荷华州.md "wikilink")，此前未被看好的[约翰·克里赢得该州选举的胜利](../Page/约翰·克里.md "wikilink")，支持率为38%，而热门人选[霍德华·迪安仅获](../Page/霍德华·迪安.md "wikilink")18%，名列第三。而获得第四名的候选人[里查德·格普哈特正式表示退出提名选举](../Page/里查德·格普哈特.md "wikilink")。
  - 1月21日，美国总统[小布什来到](../Page/乔治·W·布什.md "wikilink")[亚利桑纳州](../Page/亚利桑纳州.md "wikilink")[菲尼克斯市为总统竞选拉票](../Page/菲尼克斯市.md "wikilink")。
  - 1月27日，克里在[新罕布什尔州中以](../Page/新罕布什尔州.md "wikilink")38%的得票率赢得该州的初选胜利。\[2\]
  - 1月29日：美国前总统[比尔·克林顿在](../Page/比尔·克林顿.md "wikilink")[国会山会见民主党参议员后为克里辩护](../Page/国会山.md "wikilink")。
  - 2月2日：由美国有线新闻网、“今日美国报”和[盖洛普民意调查机构联合进行的一项最新民意调查显示](../Page/盖洛普民意测验.md "wikilink")，美国总统[布什的支持率第一次跌破了](../Page/乔治·W·布什.md "wikilink")50%。
  - 2月3日：民主党参议员[约翰·克里在](../Page/约翰·克里.md "wikilink")[密苏里州和](../Page/密苏里州.md "wikilink")[特拉华州的民主党总统候选人初选中胜出](../Page/特拉华州.md "wikilink")；[约翰·爱德华兹在其故乡](../Page/约翰·爱德华兹.md "wikilink")[南卡罗来纳州获得首场胜利](../Page/南卡罗来纳州.md "wikilink")。
  - 2月3日：民主党总统候选人[约瑟夫·利伯曼表示退出选举](../Page/约瑟夫·利伯曼.md "wikilink")。
  - 2月7日：民主党候选人[约翰·克里在](../Page/约翰·克里.md "wikilink")[华盛顿州和](../Page/华盛顿州.md "wikilink")[密歇根州的党内初选中胜出](../Page/密歇根州.md "wikilink")。
  - 2月9日：民主党候选人[约翰·克里在](../Page/约翰·克里.md "wikilink")[缅因州的党内初选中胜出](../Page/缅因州.md "wikilink")。
  - 2月10日：民主党候选人[约翰·克里在](../Page/约翰·克里.md "wikilink")[弗吉尼亚州和](../Page/弗吉尼亚州.md "wikilink")[田纳西州的党内初选中胜出](../Page/田纳西州.md "wikilink")。
  - 2月11日：民主党候选人克拉克表示将退出总统竞选。
  - 2月14日：民主党候选人[约翰·克里在](../Page/约翰·克里.md "wikilink")[内华达州和首都](../Page/内华达州.md "wikilink")[华盛顿的党内初选中胜出](../Page/华盛顿哥伦比亚特区.md "wikilink")。
  - 2月18日：民主党候选人前[佛蒙特州州长](../Page/佛蒙特州.md "wikilink")[迪安表示将退出总统竞选](../Page/迪安.md "wikilink")。
  - 2月19日：工会组织[劳联－产联表示将支持克里](../Page/劳联－产联.md "wikilink")。
  - 3月3日：民主党参议员[约翰·爱德华兹于](../Page/约翰·爱德华兹.md "wikilink")3月3日退出选举

## 2000年选举团改变情况

2000年的[人口普查後](../Page/人口普查.md "wikilink")，州份的眾議員人數會根據該州份的人口增減而調整，以確保符合人口比例。而州份派出的選舉人人數相等於該個州的眾議院議員及參議院議員人數總和，州份的選舉人人數因此而變動。由於[2000年美国总统选举結果異常接近](../Page/2000年美国总统选举.md "wikilink")，選舉人人數的變動有可能影響2004年的選情。

下面的表格显示了从[2000年以来选举团成员的改变情况](../Page/2000年美国总统选举.md "wikilink")。<font color="#CC2200">**红色**</font>（**+7**）表示2000年[布什获得胜利的州份](../Page/乔治·W·布什.md "wikilink")，<font color="darkblue">**蓝色**</font>（**-7**）表示[戈尔获胜的州份](../Page/阿尔·戈尔.md "wikilink")。

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><font color="#CC2200"><strong>亚利桑那州</strong></font> (<font color="#FF0000"><strong>+2</strong></font>)</li>
<li><font color="darkblue"><strong>加利福尼亚州</strong></font> (<font color="#A00000"><strong>+1</strong></font>)</li>
<li><font color="#CC2200"><strong>科罗拉多州</strong></font> (<font color="#A00000"><strong>+1</strong></font>)</li>
<li><font color="darkblue"><strong>康涅狄格州</strong></font> (<font color="#008000"><strong>-1</strong></font>)</li>
<li><font color="#CC2200"><strong>佛罗里达州</strong></font> (<font color="#FF0000"><strong>+2</strong></font>)</li>
<li><font color="#CC2200"><strong>乔治亚州</strong></font> (<font color="#FF0000"><strong>+2</strong></font>)</li>
<li><font color="darkblue"><strong>伊利诺斯州</strong></font> (<font color="#008000"><strong>-1</strong></font>)</li>
<li><font color="#CC2200"><strong>印第安纳州</strong></font> (<font color="#008000"><strong>-1</strong></font>)</li>
<li><font color="darkblue"><strong>密歇根州</strong></font> (<font color="#008000"><strong>-1</strong></font>)</li>
</ul></td>
<td><ul>
<li><font color="#CC2200"><strong>密西西比州</strong></font> (<font color="#008000"><strong>-1</strong></font>)</li>
<li><font color="#CC2200"><strong>内华达州</strong></font> (<font color="#A00000"><strong>+1</strong></font>)</li>
<li><font color="darkblue"><strong>纽约州</strong></font> (<font color="00FF00"><strong>-2</strong></font>)</li>
<li><font color="#CC2200"><strong>北卡罗来纳州</strong></font> (<font color="#A00000"><strong>+1</strong></font>)</li>
<li><font color="#CC2200"><strong>俄亥俄州</strong></font> (<font color="#008000"><strong>-1</strong></font>)</li>
<li><font color="#CC2200"><strong>俄克拉荷马州</strong></font> (<font color="#008000"><strong>-1</strong></font>)</li>
<li><font color="darkblue"><strong>宾西法尼亚州</strong></font> (<font color="#FF0000"><strong>-2</strong></font>)</li>
<li><font color="#CC2200"><strong>德克萨斯州</strong></font> (<font color="#FF0000"><strong>+2</strong></font>)</li>
<li><font color="darkblue"><strong>威斯康星州</strong></font>(<font color="#008000"><strong>-1</strong></font>)</li>
</ul></td>
</tr>
</tbody>
</table>

## 外部連結

<references />

### 2004总统选举网站链接

  - [E-Democracy.US Election 2004 Links](http://www.e-democracy.us)

### 2004总统选举环球辩论与投票

  - [The world votes](http://www.theworldvotes.org)

### 选举新闻

  - [Associated Press news
    wire](http://news.yahoo.com/news?tmpl=index2&cid=694)
  - [*Washington Post* Election 2004
    coverage](http://www.washingtonpost.com/wp-dyn/politics/elections/2004/)

### 新闻文章

  - [Green Party considers 2004
    strategy](http://www.msnbc.com/news/941276.asp?vts=072120031525&cp1=1#BODY)
    - MSNBC, July 2003
  - [新华社](http://www.xinhuanet.com/world/uselection/index.htm)

[Category:2004年美国](../Category/2004年美国.md "wikilink")
[Category:1991年后的美国历史](../Category/1991年后的美国历史.md "wikilink")
[Category:2004年美國總統選舉](../Category/2004年美國總統選舉.md "wikilink")

1.  [戈尔知难而退
    新华网](http://news.xinhuanet.com/zonghe/2002-12/18/content_662659.htm)
2.  [美国参议员克里在新罕布什尔州初选中获胜
    来源：新华网](http://news.xinhuanet.com/world/2004-01/28/content_1289486.htm)