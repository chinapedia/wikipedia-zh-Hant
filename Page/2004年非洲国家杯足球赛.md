**2004年非洲國家盃**於1月24日至2月14日在[突尼斯举行](../Page/突尼斯.md "wikilink")，在决赛中[突尼斯队以](../Page/突尼斯国家足球队.md "wikilink")
2–1 战胜[摩洛哥获得冠军](../Page/摩洛哥国家足球队.md "wikilink")。

## 參賽隊伍

[African_Cup_of_Nations_2004.png](https://zh.wikipedia.org/wiki/File:African_Cup_of_Nations_2004.png "fig:African_Cup_of_Nations_2004.png")
以下 16 队球队进入决赛阶段：

<table style="width:60%;">
<colgroup>
<col style="width: 30%" />
<col style="width: 30%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li></li>
<li></li>
<li></li>
<li><p>（上届冠军）</p></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li><p>（主办国）</p></li>
<li></li>
</ul></td>
</tr>
</tbody>
</table>

## 比赛地点

<center>

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/雷达斯港口.md" title="wikilink">雷达斯港口</a></p></th>
<th></th>
<th><p><a href="../Page/突尼斯.md" title="wikilink">突尼斯</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/Stade_7_Novembre.md" title="wikilink">Stade 7 Novembre</a></p></td>
<td><p><a href="../Page/Stade_El_Menzah.md" title="wikilink">Stade El Menzah</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Mozambique-Tunisia_match_2009.jpg" title="fig:Mozambique-Tunisia_match_2009.jpg">Mozambique-Tunisia_match_2009.jpg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Stade_Olympique_d&#39;El_Menzah.jpg" title="fig:Stade_Olympique_d&#39;El_Menzah.jpg">Stade_Olympique_d'El_Menzah.jpg</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>容纳人数： <strong>60,000</strong></p></td>
<td><p>容纳人数： <strong>45,000</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/苏塞.md" title="wikilink">苏塞</a></p></td>
<td><p><a href="../Page/莫纳斯提尔.md" title="wikilink">莫纳斯提尔</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Stade_Olympique_de_Sousse.md" title="wikilink">Stade Olympique de Sousse</a></p></td>
<td><p><a href="../Page/Stade_Mustapha_Ben_Jannet.md" title="wikilink">Stade Mustapha Ben Jannet</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Stade_olympique_de_Sousse,_13_avril_2016.jpg" title="fig:Stade_olympique_de_Sousse,_13_avril_2016.jpg">Stade_olympique_de_Sousse,_13_avril_2016.jpg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:View_of_Monastir_from_the_ribat_tower.jpg" title="fig:View_of_Monastir_from_the_ribat_tower.jpg">View_of_Monastir_from_the_ribat_tower.jpg</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>容纳人数： <strong>28,000</strong></p></td>
<td><p>容纳人数： <strong>22,000</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/斯法克斯.md" title="wikilink">斯法克斯</a></p></td>
<td><p><a href="../Page/比塞大.md" title="wikilink">比塞大</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Stade_Taïeb_El_Mhiri.md" title="wikilink">Stade Taïeb El Mhiri</a></p></td>
<td><p><a href="../Page/Stade_15_Octobre.md" title="wikilink">Stade 15 Octobre</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Stade_taieb_mehiri.JPG" title="fig:Stade_taieb_mehiri.JPG">Stade_taieb_mehiri.JPG</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:CityHallBizerte.JPG" title="fig:CityHallBizerte.JPG">CityHallBizerte.JPG</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>容纳人数： <strong>22,000</strong></p></td>
<td><p>容纳人数： <strong>20,000</strong></p></td>
<td></td>
</tr>
</tbody>
</table>

</center>

## 外部链接

  - [详细资料在RSSSF](http://www.rsssf.com/tables/04a.html)

[Category:2004年足球](../Category/2004年足球.md "wikilink")
[Category:非洲國家盃](../Category/非洲國家盃.md "wikilink")