## [9月30日](../Page/9月30日.md "wikilink")

  - [日本](../Page/日本.md "wikilink")[大阪高等法院裁定](../Page/大阪高等法院.md "wikilink")，日本首相[小泉纯一郎参拜](../Page/小泉纯一郎.md "wikilink")[靖国神社的做法违反](../Page/靖国神社.md "wikilink")[日本宪法](../Page/日本宪法.md "wikilink")。
    [BBC中文网](https://web.archive.org/web/20051022131337/http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4290000/newsid_4296300/4296322.stm)

## [9月29日](../Page/9月29日.md "wikilink")

  - [东突厥斯坦解放组织](../Page/东突厥斯坦解放组织.md "wikilink")（ETLO）透过总部设在[德国](../Page/德国.md "wikilink")[慕尼黑的](../Page/慕尼黑.md "wikilink")“[东突信息中心](../Page/东突信息中心.md "wikilink")”宣称，将开始用一切手段向[中国政府发动武装战争](../Page/中华人民共和国.md "wikilink")。这是该组织首次公开宣布采用武装斗争形式争取独立。
    [BBC中文网](https://web.archive.org/web/20051022131158/http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4290000/newsid_4295900/4295912.stm)
  - [Google公司宣布和](../Page/Google公司.md "wikilink")[NASA建立合作关系](../Page/NASA.md "wikilink")，共同开发[宇航项目新技术](../Page/宇航.md "wikilink")。[BBC中文网](http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4290000/newsid_4293900/4293906.stm)
  - [西班牙和](../Page/西班牙.md "wikilink")[摩洛哥接壤的两个](../Page/摩洛哥.md "wikilink")[飞地](../Page/飞地.md "wikilink")[休达和](../Page/休达.md "wikilink")[梅利利亚发生](../Page/梅利利亚.md "wikilink")[非法移民流血事件后](../Page/非法移民.md "wikilink")，两国增派军队确保当地的的安全。
    [BBC中文网](http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4290000/newsid_4295800/4295842.stm)
  - [约翰·罗伯茨已经宣誓就任](../Page/约翰·罗伯茨.md "wikilink")[美国最高法院首席大法官接替已故](../Page/美国最高法院首席大法官.md "wikilink")[威廉·伦奎斯特](../Page/威廉·伦奎斯特.md "wikilink")。
    [VOA](http://www.voanews.com/chinese/n2005-09-29-voa77.cfm)
  - [联合国教科文组织第](../Page/联合国教科文组织.md "wikilink")172届执行局会议决定，正式批准设立国际“[孔子教育奖](../Page/孔子教育奖.md "wikilink")”，奖励在[教育和](../Page/教育.md "wikilink")[文化等方面做出突出贡献的各国政要和专家](../Page/文化.md "wikilink")。
    [新华网](http://news.xinhuanet.com/world/2005-09/30/content_3569994.htm)

## [9月28日](../Page/9月28日.md "wikilink")

  - [台风达维在](../Page/台风达维.md "wikilink")[东亚和](../Page/东亚.md "wikilink")[东南亚几个](../Page/东南亚.md "wikilink")[国家造成](../Page/国家.md "wikilink")70多人丧生后在[老挝消散](../Page/老挝.md "wikilink")。
    [美国之音](http://www.voanews.com/chinese/n2005-09-28-voa82.cfm)
  - [世界经济论坛公布](../Page/世界经济论坛.md "wikilink")2005年全球竞争力排名，在被调查的117个国家和地区中，[芬兰连续第三年保持世界上最具竞争力的经济体领先地位](../Page/芬兰.md "wikilink")。[西部在线](http://westking.cn/xhNews/20050928/WestKing_8524_20050928215921.html)
  - 台湾[行政院长](../Page/行政院长.md "wikilink")[谢长廷宣布](../Page/谢长廷.md "wikilink")[金门](../Page/金门.md "wikilink")、[马祖地区的金融机构](../Page/马祖.md "wikilink")，可试办[新台币与](../Page/新台币.md "wikilink")[人民币兑换业务](../Page/人民币.md "wikilink")
    [BBC中文网](https://web.archive.org/web/20051022125452/http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4280000/newsid_4282300/4282378.stm)

## [9月27日](../Page/9月27日.md "wikilink")

  - [埃及总统](../Page/埃及.md "wikilink")[穆巴拉克正式宣誓就任新一届总统](../Page/穆巴拉克.md "wikilink")，任期六年。[国际在线](http://gb.chinabroadcast.cn/1321/2005/09/27/542@717857.htm)
  - 迄今为止[中国军队邀请观摩国家最多](../Page/中国.md "wikilink")、展示规模最大的“北剑-2005”军事演习在[北京军区进行](../Page/北京军区.md "wikilink")。[新华军事](http://news.xinhuanet.com/mil/2005-09/26/content_3546614.htm)[BBC中文网](http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4280000/newsid_4285100/4285160.stm)
  - [爱尔兰共和军已完全解除武装](../Page/爱尔兰共和军.md "wikilink")。[西部在线](http://westking.cn/xhNews/20050927/WestKing_9055_20050927004958.html)
  - [基地组织在](../Page/基地组织.md "wikilink")[伊拉克的二号人物](../Page/伊拉克.md "wikilink")[阿布·阿萨姆在一次](../Page/阿布·阿萨姆.md "wikilink")[美](../Page/美国.md "wikilink")[伊联合军事行动中被击毙](../Page/伊拉克.md "wikilink")。
    [BBC中文网](https://web.archive.org/web/20051210111806/http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4280000/newsid_4285700/4285766.stm)

## [9月26日](../Page/9月26日.md "wikilink")

  - [美国](../Page/美国.md "wikilink")“反战母亲”[辛迪·希恩在白宫游行时被警方拘留](../Page/辛迪·希恩.md "wikilink")。[新浪新闻](http://news.sina.com.cn/w/2005-09-27/03107041938s.shtml)[新华网](https://web.archive.org/web/20160304213113/http://news3.xinhuanet.com/photo/2005-09/27/content_3549406.htm)[凤凰网](https://web.archive.org/web/20051110074440/http://news.phoenixtv.com/phoenixtv/83880642571403264/20050927/647666.shtml)[美国之音](http://www.voanews.com/chinese/w2005-09-27-voa9.cfm)
  - [中原标准时间凌晨](../Page/中原标准时间.md "wikilink")4时，[台风达维登陆](../Page/台风达维.md "wikilink")[海南](../Page/海南.md "wikilink")。13时25分，[台风达维导致海南电网瓦解](../Page/台风达维.md "wikilink")。首次使用“[黑启动](../Page/黑启动.md "wikilink")”恢复供电。[新华网](http://news.xinhuanet.com/politics/2005-09/26/content_3543081.htm)
    [新华网](https://web.archive.org/web/20051120004346/http://news3.xinhuanet.com/politics/2005-09/26/content_3546471.htm)
  - [2005年澳門立法會選舉结果揭晓](../Page/2005年澳門立法會選舉.md "wikilink")。[三门峡日报](http://westking.cn/xhNews/20050926/WestKing_3927_20050926081957.html)
  - [印尼](../Page/印尼.md "wikilink")[禽流感死亡人数增至六人](../Page/禽流感.md "wikilink")
    [BBC中文网](https://web.archive.org/web/20051210111806/http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4280000/newsid_4285700/4285766.stm)

## [9月25日](../Page/9月25日.md "wikilink")

  - [国际货币基金组织在](../Page/国际货币基金组织.md "wikilink")[华盛顿年会上决定](../Page/华盛顿哥伦比亚特区.md "wikilink")，取消部分[最贫困国家的](../Page/最贫困国家.md "wikilink")[债务](../Page/债务.md "wikilink")。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4270000/newsid_4279600/4279680.stm)
  - [2005年澳門立法會選舉投票开始](../Page/2005年澳門立法會選舉.md "wikilink")。
    [2005年澳門立法會選舉官方网站](https://web.archive.org/web/20120717092703/http://www.el2005.gov.mo/cn/default.asp)
  - [2005年日本国际博览会闭幕](../Page/2005年日本国际博览会.md "wikilink")。总入场者达2200万人。[共同社](https://web.archive.org/web/20070929231456/http://china.kyodo.co.jp/modules/fsStory/index.php?sel_lang=schinese&storyid=20357)
  - [香港特區](../Page/香港特別行政區.md "wikilink")[行政長官](../Page/行政長官.md "wikilink")[曾蔭權與](../Page/曾蔭權.md "wikilink")[香港立法會全體議員歷史性首次出訪](../Page/香港立法會.md "wikilink")[珠江三角洲](../Page/珠江三角洲.md "wikilink")。[香港政府新聞網](https://web.archive.org/web/20051112114740/http://www.news.gov.hk/tc/category/administration/050923/html/050923tc01004.htm)

## [9月24日](../Page/9月24日.md "wikilink")

  - [国际原子能机构通过](../Page/国际原子能机构.md "wikilink")[欧盟提出的](../Page/欧盟.md "wikilink")[动议](../Page/动议.md "wikilink")，[伊朗核问题将被提交](../Page/伊朗核问题.md "wikilink")[联合国安全理事会](../Page/联合国安全理事会.md "wikilink")
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4270000/newsid_4279300/4279308.stm)
  - [丽塔飓风在](../Page/丽塔飓风.md "wikilink")[美国南部沿海地区登陆](../Page/美国.md "wikilink")。狂风暴雨开始降临[德克萨斯州和](../Page/德克萨斯州.md "wikilink")[路易斯安娜州](../Page/路易斯安娜州.md "wikilink")[海岸](../Page/海岸.md "wikilink")。德克萨斯州数十万户停电。[BBC中文网](https://web.archive.org/web/20051104141026/http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4270000/newsid_4277700/4277726.stm)
  - [复旦大学举行百年校庆](../Page/复旦大学.md "wikilink")。[新华网](http://www.sh.xinhuanet.com/zhuanti/fudan/index.htm)
  - [強烈熱帶風暴](../Page/強烈熱帶風暴.md "wikilink")[達維集結在](../Page/熱帶風暴達維.md "wikilink")[香港之南約](../Page/香港.md "wikilink")300公里，預料向西移動，時速約15公里，橫過[南中國海北部](../Page/南中國海.md "wikilink")。
    [Sina](http://gd.news.sina.com.cn/local/2005-09-24/1725715.html)

## [9月23日](../Page/9月23日.md "wikilink")

  - [北韩在请求国际粮食援助十年过后正式要求](../Page/北韩.md "wikilink")[联合国终止对该国的粮食援助](../Page/联合国.md "wikilink")。
    [BBC中文网](http://news.bbc.co.uk/go/rss/-/chinese/simp/hi/newsid_4270000/newsid_4273800/4273842.stm)
  - [飓风丽塔逼近](../Page/飓风丽塔.md "wikilink")[美国](../Page/美国.md "wikilink")，[德克萨斯州呼吁](../Page/德克萨斯州.md "wikilink")[美国联邦政府派士兵待命执行搜寻和救援任务](../Page/美国联邦政府.md "wikilink")。
    [BBC中文网](https://web.archive.org/web/20051104140647/http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4270000/newsid_4272800/4272886.stm)
  - [微软庆祝创办三十周年](../Page/微软.md "wikilink")
    [BBC中文网](https://web.archive.org/web/20051023064719/http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4270000/newsid_4273800/4273880.stm)
  - [中国人民银行允许非](../Page/中国人民银行.md "wikilink")[美元](../Page/美元.md "wikilink")[货币对](../Page/货币.md "wikilink")[人民币交易价浮动幅度扩至](../Page/人民币.md "wikilink")3%。[新华网](http://news.xinhuanet.com/fortune/2005-09/23/content_3533541.htm)

## [9月22日](../Page/9月22日.md "wikilink")

  - [飓风丽塔已经升级成为](../Page/飓风丽塔.md "wikilink")[五级](../Page/五级飓风.md "wikilink")，正在从[墨西哥湾向](../Page/墨西哥湾.md "wikilink")[美国](../Page/美国.md "wikilink")[德克萨斯州移动](../Page/德克萨斯州.md "wikilink")，美国墨西哥湾地区的一百多万民众紧急疏散。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4270000/newsid_4270100/4270102.stm)
  - [印度尼西亚](../Page/印度尼西亚.md "wikilink")[禽流感症状的病人人数继续上升](../Page/禽流感.md "wikilink")，有13人在[雅加达医院接受观察](../Page/雅加达.md "wikilink")。
    [BBC中文网](https://web.archive.org/web/20051104134307/http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4270000/newsid_4271400/4271428.stm)
  - [欧盟和](../Page/欧盟.md "wikilink")[俄罗斯在](../Page/俄罗斯.md "wikilink")[维也纳举行的有关](../Page/维也纳.md "wikilink")[伊朗核问题的会谈陷入僵局](../Page/伊朗核问题.md "wikilink")，俄罗斯反对欧盟第二次提出的草案。
    [美国之音](http://www.voanews.com/chinese/n2005-09-22-voa68.cfm)

## [9月21日](../Page/9月21日.md "wikilink")

  - [台灣名嘴](../Page/台灣.md "wikilink")[李敖在](../Page/李敖.md "wikilink")[北京大學發表演說](../Page/北京大學.md "wikilink")。[BBC中文網](https://web.archive.org/web/20051105185751/http://news8.thdo.bbc.co.uk/chinese/trad/hi/newsid_4260000/newsid_4267000/4267024.stm)
  - [印度尼西亚海军表示](../Page/印度尼西亚海军.md "wikilink")，一艘[印尼军舰向涉嫌非法捕鱼的](../Page/印尼.md "wikilink")[中国](../Page/中国.md "wikilink")[渔船开火](../Page/渔船.md "wikilink")，造成一名中国船员被打死，两人受伤。
    [BBC中文网](https://web.archive.org/web/20051104140049/http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4260000/newsid_4266600/4266638.stm)
    [凤凰网](https://web.archive.org/web/20051119040623/http://news.phoenixtv.com/phoenixtv/83889438664425472/20050921/642175.shtml)
  - [日本政府确认](../Page/日本.md "wikilink")[中国在](../Page/中国.md "wikilink")[东海两国争议区域开始生产](../Page/东海.md "wikilink")[天然气或](../Page/天然气.md "wikilink")[石油](../Page/石油.md "wikilink")。
    [BBC中文网](https://web.archive.org/web/20051105180901/http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4260000/newsid_4262700/4262772.stm)

## [9月20日](../Page/9月20日.md "wikilink")

  - [印尼政府宣布](../Page/印尼.md "wikilink")[禽流感暴发进入非常状态](../Page/禽流感.md "wikilink")。[南京晨报](https://web.archive.org/web/20051118153750/http://www.xhby.net/xhby/content/2005-09/21/content_949970.htm)
  - [北韩对](../Page/北韩.md "wikilink")[六方会谈提出新条件](../Page/六方会谈.md "wikilink")，除非能得到一座用于民用[发电的](../Page/发电.md "wikilink")[轻水核反应堆](../Page/轻水核反应堆.md "wikilink")，否则将不放弃核计划。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4260000/newsid_4262500/4262576.stm)
  - [飓风丽塔已增强为四级](../Page/飓风丽塔.md "wikilink")[飓风](../Page/飓风.md "wikilink")，正向[墨西哥湾沿岸移动](../Page/墨西哥湾.md "wikilink")，可能会威胁还在恢复中的[美国南部海岸地区](../Page/美国.md "wikilink")。数万人已经逃离家园。[美国之音](http://www.voanews.com/chinese/n2005-09-19-voa61.cfm)
    [BBC中文网](https://web.archive.org/web/20051104140024/http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4260000/newsid_4266400/4266434.stm)

## [9月19日](../Page/9月19日.md "wikilink")

  - [朝鲜核危机第四輪](../Page/朝鲜核危机.md "wikilink")[六方會談代表在](../Page/朝核六方会谈.md "wikilink")[中國](../Page/中國.md "wikilink")[北京通过了](../Page/北京.md "wikilink")[中方起草的共同声明](../Page/中华人民共和国.md "wikilink")。[朝鮮承諾放棄一切](../Page/朝鲜民主主义人民共和国.md "wikilink")[核武器及現有核計畫](../Page/核武器.md "wikilink")，早日重返[不擴散核武器條約](../Page/不擴散核武器條約.md "wikilink")，並回到[國際原子能署保障監督](../Page/國際原子能署.md "wikilink")。[美國確認在](../Page/美國.md "wikilink")[朝鮮半島沒有核武器](../Page/朝鮮半島.md "wikilink")，無意以核武器或常規武器攻擊或入侵朝鮮。聲明旨在以和平方式可核查地實現朝鮮半島無核化。參見[中新網-聲明全文（中文版）](http://estate.chinanews.com.cn/news/2005/2005-09-19/8/627838.shtml)
    [凤凰网](http://www.phoenixtv.com/phoenixtv/79658522215710720/20050919/639589.shtml)[共同社](https://web.archive.org/web/20070929124412/http://china.kyodo.co.jp/modules/fsStory/index.php?sel_lang=schinese&storyid=20098)[BBC中文网](https://web.archive.org/web/20051104132855/http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4250000/newsid_4259400/4259416.stm)[CCTV](http://www.cctv.com/news/world/20050919/101024.shtml)
  - [小罗纳尔多当选本年度由](../Page/小罗纳尔多.md "wikilink")[国际职业球员联合会](../Page/国际职业球员联合会.md "wikilink")（FIFPro）评选的世界足球先生。[新华网](http://news.xinhuanet.com/sports/2005-09/20/content_3514624.htm)

## [9月18日](../Page/9月18日.md "wikilink")

  - [德国大选投票结束](../Page/2005年德国联邦选举.md "wikilink")，[德国基督教民主联盟候选人](../Page/德国基督教民主联盟.md "wikilink")[梅克尔险胜执政党社会民主党](../Page/梅克尔.md "wikilink")。但联盟仍不足以组成多数政府。[BBC中文](https://web.archive.org/web/20051023063007/http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4250000/newsid_4258500/4258592.stm)
  - [阿富汗议会选举](../Page/2005年阿富汗议会选举.md "wikilink")30年来第一次开始投票。
    [BBC中文网](https://web.archive.org/web/20051104135639/http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4250000/newsid_4257000/4257004.stm)
  - [伊朗总统](../Page/伊朗.md "wikilink")[艾哈麦迪纳贾德宣布决心继续发展和平用](../Page/艾哈麦迪纳贾德.md "wikilink")[核子项目](../Page/核工业.md "wikilink")
    [Wikinews](http://en.wikinews.org/wiki/Iran_determined_to_continue_with_nuclear_program)

## [9月17日](../Page/9月17日.md "wikilink")

  - [2005年联合国首脑会议在纽约](../Page/2005年联合国首脑会议.md "wikilink")[联合国总部落下帷幕](../Page/联合国.md "wikilink")，大会审议通过了《[成果文件](../Page/成果文件.md "wikilink")》，在发展、安全、人权和[联合国改革等问题上做出了一系列重要决定和承诺](../Page/联合国改革.md "wikilink")。[人民网](http://world.people.com.cn/GB/1029/42408/3703658.html)
  - [伊拉克前总统](../Page/伊拉克.md "wikilink")[萨达姆半裸照官司败诉](../Page/萨达姆.md "wikilink")，其律师已经撤销诉状。[新浪网](http://news.sina.com.cn/w/2005-09-17/13446968759s.shtml)

## [9月16日](../Page/9月16日.md "wikilink")

  - [国际移民组织在最新报告中称](../Page/国际移民组织.md "wikilink")[欧洲东南部人口走私活动猖獗](../Page/欧洲.md "wikilink")。[BBC中文](https://web.archive.org/web/20051023062812/http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4250000/newsid_4252500/4252540.stm)
  - 曾有“上海首富”称号的前[上海房地产董事会主席](../Page/上海.md "wikilink")[周正毅正式被](../Page/周正毅.md "wikilink")[香港](../Page/香港.md "wikilink")[廉政公署通缉](../Page/廉政公署.md "wikilink")，据称其涉嫌诈骗香港[上市公司](../Page/上市公司.md "wikilink")。[新浪网](http://news.sina.com.cn/c/p/2005-09-16/09347783469.shtml)
  - [華懋集團董事局主席](../Page/華懋集團.md "wikilink")[龔如心和家翁](../Page/龔如心.md "wikilink")[王廷歆就](../Page/王廷歆.md "wikilink")[王德輝身家的爭產案](../Page/王德輝.md "wikilink")，[香港終審法院](../Page/香港終審法院.md "wikilink")5位法官一致裁定推翻下級法院的裁決，龔如心上訴得直。經過8年訴訟，龔如心成為王德輝[港幣](../Page/港幣.md "wikilink")400億遺產的唯一承繼人。
    [香港雅虎引用明報](https://web.archive.org/web/20050917105426/http://hk.news.yahoo.com/050916/12/1gpxu.html)
  - [美國財政部點名指](../Page/美國.md "wikilink")[澳門](../Page/澳門.md "wikilink")[滙業銀行等境外銀行](../Page/滙業銀行.md "wikilink")，協助北韓洗黑錢、偽鈔流通等非法活動，滙業銀行出現擠提。
    [香港雅虎引用明報](https://web.archive.org/web/20050920232817/http://hk.news.yahoo.com/050915/12/1goxv.html)

## [9月15日](../Page/9月15日.md "wikilink")

  - 苹果[iPod Mini突然停产为新产品](../Page/iPod_Mini.md "wikilink")[iPod
    nano让路](../Page/iPod_nano.md "wikilink")。[中国经济网](https://web.archive.org/web/20160304204628/http://www.ce.cn/cysc/it/home/smjj/200509/16/t20050916_4705103.shtml)
  - [聯合國安全理事會擴充案未獲通過](../Page/聯合國安全理事會.md "wikilink")，欲申請成為常任理事國的[日本](../Page/日本.md "wikilink")、[德國](../Page/德國.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[印度無法加入](../Page/印度.md "wikilink")。
    [聯合新聞網](http://udn.com/NEWS/WORLD/WOR3/2900188.shtml)
  - [飓风奥菲莉娅袭击](../Page/飓风奥菲莉娅.md "wikilink")[美国](../Page/美国.md "wikilink")[北卡罗莱纳州海岸](../Page/北卡罗莱纳州.md "wikilink")。[新浪网](http://news.sina.com.cn/w/2005-09-15/15457777404.shtml)
  - [以色列最高法院判决修建西岸隔离墙合法](../Page/以色列.md "wikilink")。[新浪网](http://news.sina.com.cn/w/2005-09-15/16276953928s.shtml)。
  - [印度尼西亚](../Page/印度尼西亚.md "wikilink")[自由亚齐运动武装人员开始向](../Page/自由亚齐运动.md "wikilink")[亚齐和平国际监督团上缴武器](../Page/亚齐和平国际监督团.md "wikilink")。[新浪网](http://news.sina.com.cn/w/2005-09-15/15416953542s.shtml)

## [9月14日](../Page/9月14日.md "wikilink")

  - [联合国首脑会议今天拉开帷幕](../Page/联合国.md "wikilink")。170个国家的[国家元首或](../Page/国家元首.md "wikilink")[政府首脑出席大会](../Page/政府首脑.md "wikilink")，共同庆祝联合国成立60周年。[新华网](http://news.xinhuanet.com/world/2005-09/14/content_3486923.htm)
  - [中国](../Page/中国.md "wikilink")[公安部开始加大力度遏制本国](../Page/公安部.md "wikilink")[公民出境参赌](../Page/公民.md "wikilink")。[新华网](http://news.xinhuanet.com/legal/2005-09/14/content_3488813.htm)
  - [巴格达爆炸上百人丧生](../Page/巴格达.md "wikilink")
    [扎卡维发出袭击威胁](../Page/扎卡维.md "wikilink")。[新华网](http://www.xinhuanet.com/world/zt050920/)

## [9月13日](../Page/9月13日.md "wikilink")

  - 最大的网络拍卖公司[eBay以](../Page/eBay.md "wikilink")26[亿](../Page/亿.md "wikilink")[美元价格收购了快速成长的网络通讯公司](../Page/美元.md "wikilink")[Skype](../Page/Skype.md "wikilink")。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4230000/newsid_4238000/4238066.stm)
  - 最后一批[以色列士兵已经离开](../Page/以色列.md "wikilink")[加沙](../Page/加沙.md "wikilink")，从而正式结束了将近四十年的军事占领。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4230000/newsid_4234800/4234864.stm)
  - 第四轮[朝鲜核问题](../Page/朝鲜.md "wikilink")[六方会谈在](../Page/六方会谈.md "wikilink")[中国](../Page/中国.md "wikilink")[北京](../Page/北京.md "wikilink")[钓鱼台国宾馆芳菲苑开始第二阶段会谈](../Page/钓鱼台国宾馆.md "wikilink")。[中华网](http://news.china.com/zh_cn/focus/chaoxianproblem/)
  - 各国驻[联合国代表终于就](../Page/联合国.md "wikilink")[联合国改革方案达成一份包括继续努力达成](../Page/联合国改革.md "wikilink")[千年目标等的协议草案](../Page/联合国千年目标.md "wikilink")。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4240000/newsid_4243600/4243626.stm)
  - 將參加[聯合國](../Page/聯合國.md "wikilink")60週年首腦會議的[美國總統](../Page/美國總統.md "wikilink")[布什與](../Page/乔治·沃克·布什.md "wikilink")[中華人民共和國主席](../Page/中華人民共和國.md "wikilink")[胡錦濤當地時間下午抵達](../Page/胡錦濤.md "wikilink")[紐約](../Page/紐約.md "wikilink")[甘迺迪機場並於胡錦濤下榻的](../Page/甘迺迪國際機場.md "wikilink")[華爾道夫酒店會晤](../Page/華爾道夫酒店.md "wikilink")，他們討論了中美[貿易](../Page/貿易.md "wikilink")、[台灣問題](../Page/台灣問題.md "wikilink")、[六方會談](../Page/六方會談.md "wikilink")、[人權等在內的](../Page/中華人民共和國人權狀況.md "wikilink")[中美關係及世界局勢](../Page/中美關係.md "wikilink")。（綜合[有線電視新聞網及](../Page/有線電視新聞網.md "wikilink")[紐約時報報導](../Page/紐約時報.md "wikilink")）

## [9月12日](../Page/9月12日.md "wikilink")

  - [以色列军队在占领](../Page/以色列军队.md "wikilink")[加沙地带](../Page/加沙地带.md "wikilink")38年后從當地撤軍。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4230000/newsid_4234800/4234864.stm)
  - [香港迪士尼樂園開幕](../Page/香港迪士尼樂園.md "wikilink")，[中華人民共和國副主席](../Page/中華人民共和國副主席.md "wikilink")[曾慶紅主持開幕禮](../Page/曾慶紅.md "wikilink")。[明報新聞網](https://web.archive.org/web/20051119181154/http://www.mpinews.com/content.cfm?newsid=200509120710gb10710a)
  - [中華人民共和國国家保密局宣布](../Page/中華人民共和國.md "wikilink")，自今年[2005年8月起](../Page/2005年8月.md "wikilink")，对全国及省、自治区、直辖市因[自然灾害导致死亡人员的总数及相关资料解密](../Page/自然灾害.md "wikilink")，原《民政工作中国家秘密及其密级具体范围的规定》中的相关内容予以废止。[新华网](http://news.xinhuanet.com/newscenter/2005-09/12/content_3477785.htm)

## [9月11日](../Page/9月11日.md "wikilink")

  - [台风卡努已于](../Page/台风卡努.md "wikilink")14点50分在[中国](../Page/中国.md "wikilink")[浙江省](../Page/浙江.md "wikilink")[台州市路桥区金清镇登陆](../Page/台州.md "wikilink")，官方已经转移100万名居民。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4230000/newsid_4234400/4234414.stm)
    [新浪网](http://news.sina.com.cn/c/2005-09-11/15577737454.shtml)
  - [日本第四十四屆眾議院議員選舉進行開票](../Page/2005年日本大选.md "wikilink")，執政的[自由民主黨獲得過半席次](../Page/日本自由民主黨.md "wikilink")，首相[小泉純一郎續任](../Page/小泉純一郎.md "wikilink")。[BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4230000/newsid_4235700/4235728.stm)
    [讀賣新聞](http://headlines.yahoo.co.jp/hl?a=20050911-00000014-yom-pol)
  - [美国在](../Page/美国.md "wikilink")[卡特里娜飓风阴影下纪念](../Page/卡特里娜飓风.md "wikilink")[九一一袭击事件四周年](../Page/九一一袭击事件.md "wikilink")
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4230000/newsid_4235000/4235080.stm)

## [9月10日](../Page/9月10日.md "wikilink")

  - [约旦首相](../Page/约旦.md "wikilink")[巴德兰对](../Page/巴德兰.md "wikilink")[伊拉克首都](../Page/伊拉克.md "wikilink")[巴格达进行历史性访问](../Page/巴格达.md "wikilink")
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4230000/newsid_4234200/4234254.stm)
  - [中国国家主席](../Page/中国.md "wikilink")[胡锦涛与](../Page/胡锦涛.md "wikilink")[加拿大总理](../Page/加拿大.md "wikilink")[马丁会晤后](../Page/马丁.md "wikilink")，双方一致同意把中加关系提升为[战略伙伴关系](../Page/战略伙伴关系.md "wikilink")。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4230000/newsid_4232200/4232234.stm)
  - [香港专家在](../Page/香港.md "wikilink")[蝙蝠身上发现类](../Page/蝙蝠.md "wikilink")[SARS病毒](../Page/SARS病毒.md "wikilink")
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4230000/newsid_4234000/4234028.stm)
  - [李安执导影片](../Page/李安.md "wikilink")《[断背山](../Page/断背山.md "wikilink")》获[第62届威尼斯国际电影节金狮奖](../Page/第62届威尼斯国际电影节.md "wikilink")
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4230000/newsid_4234000/4234070.stm)

## [9月9日](../Page/9月9日.md "wikilink")

  - [埃及选举委员会宣布](../Page/埃及.md "wikilink")，现任总统[穆巴拉克赢得了该国历史上第一次有竞争的总统](../Page/穆巴拉克.md "wikilink")[选举](../Page/选举.md "wikilink")。[BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4230000/newsid_4231300/4231352.stm)
  - 继[马祖弹药库发生爆炸后](../Page/马祖.md "wikilink")，[台湾](../Page/台湾.md "wikilink")[高雄一兵工厂也发生爆炸](../Page/高雄.md "wikilink")，三人死亡
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4220000/newsid_4228500/4228586.stm)
  - 62岁的“互联网之父”，[TCP/IP协议设计者之一](../Page/TCP/IP协议.md "wikilink")[温特·科夫加盟](../Page/温特·科夫.md "wikilink")[Google公司](../Page/Google公司.md "wikilink")
    [Google
    Blog](http://googleblog.blogspot.com/2005/09/cerfs-up-at-google.html)
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4220000/newsid_4229900/4229972.stm)

## [9月8日](../Page/9月8日.md "wikilink")

  - [乌克兰总统](../Page/乌克兰.md "wikilink")[尤先科解散政府](../Page/維克多·尤先科.md "wikilink")，同时委任乌克兰东部地区的地方首长[叶哈努罗夫为代总理](../Page/叶哈努罗夫.md "wikilink")。[BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4220000/newsid_4225500/4225594.stm)
  - [美国](../Page/美国.md "wikilink")[新奥尔良市已开始清理因](../Page/新奥尔良市.md "wikilink")[卡特琳娜飓风袭击而死亡的市民](../Page/卡特琳娜飓风.md "wikilink")[尸体](../Page/尸体.md "wikilink")，已将2万5千个空[尸体袋送达](../Page/尸体袋.md "wikilink")[路易斯安那州](../Page/路易斯安那州.md "wikilink")。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4220000/newsid_4225100/4225178.stm)
  - [欧盟通过](../Page/欧盟.md "wikilink")[中国纺织品配额新协议](../Page/中国.md "wikilink")
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4220000/newsid_4222400/4222446.stm)
  - [巴基斯坦](../Page/巴基斯坦.md "wikilink")[卡拉奇两家](../Page/卡拉奇.md "wikilink")[麦当劳和](../Page/麦当劳.md "wikilink")[肯德基店发生炸弹爆炸](../Page/肯德基.md "wikilink")，至少有三人被炸伤。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4220000/newsid_4228300/4228332.stm)
  - [台湾高铁宣布延后至](../Page/台湾高铁.md "wikilink")[2006年](../Page/2006年.md "wikilink")[10月31日通车](../Page/10月31日.md "wikilink")。[联合新闻网](https://web.archive.org/web/20051130152808/http://www.udn.com/2005/9/8/NEWS/NATIONAL/NAT5/2887950.shtml)

## [9月7日](../Page/9月7日.md "wikilink")

  - 联合国[秘书长](../Page/联合国秘书长.md "wikilink")[科菲·安南表示为](../Page/科菲·安南.md "wikilink")[联合国对](../Page/联合国.md "wikilink")[伊拉克实行的](../Page/伊拉克.md "wikilink")[石油换食品计划的失败承担责任](../Page/石油换食品计划.md "wikilink")。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4210000/newsid_4217700/4217760.stm)
  - [埃及开始总统](../Page/埃及.md "wikilink")[大选投票](../Page/大选.md "wikilink")。这是埃及第一次有多名候选人参加的[大选](../Page/大选.md "wikilink")。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4220000/newsid_4221300/4221354.stm)
  - [巴勒斯坦总统高级安全顾问](../Page/巴勒斯坦.md "wikilink")，安全部队指挥官[穆萨·阿拉法特在](../Page/穆萨·阿拉法特.md "wikilink")[加沙市被人开枪打死](../Page/加沙市.md "wikilink")。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4220000/newsid_4221300/4221384.stm)

## [9月6日](../Page/9月6日.md "wikilink")

  - [台湾](../Page/台湾.md "wikilink")[花蓮外](../Page/花蓮縣.md "wikilink")[海发生規模](../Page/海.md "wikilink")6.1级[地震](../Page/地震.md "wikilink")
    [BBC中文網](http://news.bbc.co.uk/chinese/simp/hi/newsid_4210000/newsid_4217800/4217892.stm)
  - 第14号4级强[台风](../Page/台风.md "wikilink")"[彩蝶](../Page/彩蝶台风.md "wikilink")"(Nabi)袭击[日本](../Page/日本.md "wikilink")，多人死亡，10万人疏散。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4210000/newsid_4215500/4215538.stm)
  - 据韩国媒体报道，[北韩向](../Page/北韩.md "wikilink")[中国提出](../Page/中国.md "wikilink")，[六方会谈可于](../Page/六方会谈.md "wikilink")9月13日在中国[北京重开](../Page/北京.md "wikilink")。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4210000/newsid_4217800/4217838.stm)
  - [颱風彩蝶袭击](../Page/颱風彩蝶.md "wikilink")[日本西南部](../Page/日本.md "wikilink")，至少10人死亡，[九州與](../Page/九州.md "wikilink")[四國地方有](../Page/四國.md "wikilink")11万人疏散，[北海道亦恐於週四受襲](../Page/北海道.md "wikilink")。

## [9月5日](../Page/9月5日.md "wikilink")

  - 第22届[世界法律大会在北京召开](../Page/世界法律大会.md "wikilink")。[深圳新闻网](http://www.sznews.com/n3/ca1846637.htm)
  - [泰利台风造成死亡](../Page/泰利台风.md "wikilink")95人，失踪30人，直接经济损失121.
    9亿元。[新华网](http://news.xinhuanet.com/newscenter/2005-09/05/content_3446990.htm)
  - 美国总统[布什提名](../Page/乔治·沃克·布什.md "wikilink")[上诉法院法官](../Page/上诉法院.md "wikilink")、保守派人士[约翰·罗伯茨为日前病逝的联邦](../Page/约翰·罗伯茨.md "wikilink")[最高法院首席大法官](../Page/美国最高法院首席大法官.md "wikilink")[伦奎斯特的继任者](../Page/伦奎斯特.md "wikilink")。[中国日报](https://web.archive.org/web/20160305082330/http://www.chinadaily.com.cn/gb/doc/2005-09/05/content_475272.htm)
  - [印尼](../Page/印尼.md "wikilink")[曼达拉航空公司一架](../Page/曼达拉航空公司.md "wikilink")[波音737客机在飞往](../Page/波音737.md "wikilink")[雅加达的途中](../Page/雅加达.md "wikilink")，在[苏门答腊岛东北部](../Page/苏门答腊岛.md "wikilink")[棉兰一人口密集住宅区坠毁](../Page/棉兰.md "wikilink")。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4210000/newsid_4214600/4214644.stm)[新華網](http://news.xinhuanet.com/world/2005-09/05/content_3447916.htm)
  - [俄罗斯总统](../Page/俄罗斯.md "wikilink")[普京任命](../Page/普京.md "wikilink")[弗拉吉米尔·马索林将军为新的海军总司令](../Page/弗拉吉米尔·马索林.md "wikilink")。[BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4210000/newsid_4214200/4214236.stm)
  - 第八次[中欧领导人峰会在](../Page/中欧领导人峰会.md "wikilink")[中国](../Page/中国.md "wikilink")[北京召开](../Page/北京.md "wikilink")，[BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4210000/newsid_4214400/4214400.stm)

## [9月4日](../Page/9月4日.md "wikilink")

  - [美国开始空运疏散数以万计](../Page/美国.md "wikilink")[卡特里娜飓风幸存者](../Page/卡特里娜飓风.md "wikilink")
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4210000/newsid_4212600/4212676.stm)
  - [尼泊尔暴发](../Page/尼泊尔.md "wikilink")"[日本脑炎](../Page/日本脑炎.md "wikilink")"夺去两百多人的生命，数百人在医院接受抢救。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4210000/newsid_4213100/4213118.stm)
  - [法国](../Page/法国.md "wikilink")[巴黎再度发生严重](../Page/巴黎.md "wikilink")[火灾事故](../Page/火灾.md "wikilink")，造成至少10人死亡
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4210000/newsid_4212500/4212588.stm)

## [9月3日](../Page/9月3日.md "wikilink")

  - [台风泰利重创](../Page/台风泰利.md "wikilink")[福建沿海](../Page/福建.md "wikilink")。[BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4210000/newsid_4212600/4212650.stm)
  - [中国大陆批准四家](../Page/中国大陆.md "wikilink")[台湾航空公司飞越领空](../Page/台湾.md "wikilink")。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4200000/newsid_4208400/4208444.stm)
  - [英国外交部官员表示](../Page/英国外交部.md "wikilink")，在[阿富汗被绑架的](../Page/阿富汗.md "wikilink")[英国货车司机可能已经死亡](../Page/英国.md "wikilink")。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4210000/newsid_4212000/4212002.stm)
  - [北京舉行儀式](../Page/北京.md "wikilink")，紀念[抗日戰爭勝利](../Page/抗日戰爭.md "wikilink")60周年。上午九時，[中國黨政軍領導人在](../Page/中國.md "wikilink")[天安門廣場向](../Page/天安門廣場.md "wikilink")[人民英雄紀念碑敬獻花籃](../Page/人民英雄紀念碑.md "wikilink")；其後，在[人民大會堂舉行抗戰勝利紀念大會](../Page/人民大會堂.md "wikilink")，中共中央總書記、國家主席、中央軍委主席[胡錦濤發表重要講話](../Page/胡錦濤.md "wikilink")。

## [9月2日](../Page/9月2日.md "wikilink")

  - [加拿大最高法院驳回](../Page/加拿大.md "wikilink")[赖昌星的难民申请](../Page/赖昌星.md "wikilink")
    [BBC](http://news.bbc.co.uk/2/hi/asia-pacific/4206846.stm)
  - [美国](../Page/美国.md "wikilink")[参议院就](../Page/美国参议院.md "wikilink")[总统](../Page/美国总统.md "wikilink")[布什加快救援](../Page/乔治·沃克·布什.md "wikilink")“[卡特里娜](../Page/卡特里娜飓风.md "wikilink")”飓风灾区的要求，批准105[亿](../Page/亿.md "wikilink")[美元的救灾款](../Page/美元.md "wikilink")。
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4200000/newsid_4206800/4206834.stm)
  - [阿拉伯半岛电视台播出](../Page/阿拉伯半岛电视台.md "wikilink")[伦敦自杀炸弹攻击攻击者的录像](../Page/伦敦连环爆炸案.md "wikilink")[BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4200000/newsid_4206900/4206970.stm)
  - [索馬利亞](../Page/索馬利亞.md "wikilink")[叛軍挾持三艘](../Page/叛軍.md "wikilink")[台灣漁船要求贖金並揚言殺害被挾持漁](../Page/台灣.md "wikilink")[東森新聞](http://www.ettoday.com/2005/09/02/124-1839455.htm)

## [9月1日](../Page/9月1日.md "wikilink")

[Navy-FloodedNewOrleans.jpg](https://zh.wikipedia.org/wiki/File:Navy-FloodedNewOrleans.jpg "fig:Navy-FloodedNewOrleans.jpg")吹襲後被湖水掩沒的新奧爾良。\]\]

  - [中国](../Page/中国.md "wikilink")[国家统计局副局长](../Page/中国国家统计局.md "wikilink")[朱向东在](../Page/朱向东.md "wikilink")[台湾](../Page/台湾.md "wikilink")[阳明山泡温泉时猝死](../Page/阳明山.md "wikilink")
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4200000/newsid_4207200/4207228.stm)[烟台日报社水母网](https://web.archive.org/web/20051112025619/http://www.shm.com.cn/shan-dong/2005-09/03/content_929105.htm)
  - 繼淡水魚後，[香港在北角一超級市場](../Page/香港.md "wikilink")、來自台灣的青斑海魚樣本中，驗出致癌物[孔雀石綠](../Page/孔雀石綠.md "wikilink")。同時，淡水魚陸續恢復限量供應，價格上升達兩成。[明報](https://web.archive.org/web/20050912045308/http://hk.news.yahoo.com/050902/12/1g7iv.html)
    [東方日報](https://web.archive.org/web/20050912045259/http://hk.news.yahoo.com/050901/10/1g71a.html)
  - 庆祝[西藏自治区成立四十周年盛大庆典在](../Page/西藏自治区.md "wikilink")[拉萨举行](../Page/拉萨.md "wikilink")。[中国新闻网](http://www.chinanews.com.cn/news/2005/2005-09-01/8/619824.shtml)
  - [中华人民共和国国务院发表](../Page/中华人民共和国国务院.md "wikilink")《「中国的军控、裁军与防扩散努力」白皮书》[新华网专题](http://www.xinhuanet.com/mil/zt050901/)[BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4200000/newsid_4203400/4203452.stm)
  - [德国之声主办的](../Page/德国之声.md "wikilink")「第二届国际博客大赛」正式开始。[The
    Bobs](https://web.archive.org/web/20051126163614/http://www.thebobs.com/thebobs05/bob.php?language=zh)
  - [颱風泰利登陸](../Page/颱風泰利.md "wikilink")[台灣及](../Page/台灣.md "wikilink")[福建](../Page/福建.md "wikilink")，造成超過100人死亡。
  - 在遭受[卡特里娜颶風重創後](../Page/卡特里娜颶風.md "wikilink")，[新奥尔良市長宣布](../Page/新奥尔良.md "wikilink")「棄城」，居民將全數撤出。