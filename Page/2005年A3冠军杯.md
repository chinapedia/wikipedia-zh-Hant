**2005年A3冠军杯**（**A3 Champions Cup
2005**）是第三屆[A3冠軍杯](../Page/A3冠軍杯.md "wikilink")，這屆賽事是[韓國首度辦舉](../Page/韓國.md "wikilink")，在2005年2月13日至2月19日於[濟州島舉行](../Page/濟州島.md "wikilink")，主場館是曾經主辦[2002年國際足協世界盃的](../Page/2002年國際足協世界盃.md "wikilink")[濟州世界盃競技場並最終由東道主球隊](../Page/濟州世界盃競技場.md "wikilink")[水原三星藍翼奪得冠軍](../Page/水原三星藍翼足球俱樂部.md "wikilink")。

## 參賽球隊

  - [水原三星藍翼](../Page/水原三星藍翼足球俱樂部.md "wikilink")（2004年[南韓職業足球聯賽冠軍](../Page/南韓職業足球聯賽.md "wikilink")）

  - [浦項制鐵](../Page/浦項制鐵足球俱樂部.md "wikilink")（2004年[韓國足總盃冠軍](../Page/韓國足總盃.md "wikilink")）

  - [深圳健力寶](../Page/深圳市足球俱樂部.md "wikilink")（2004年[中國足球超級聯賽冠軍](../Page/中國足球超級聯賽.md "wikilink")）

  - [橫濱水手](../Page/橫濱水手.md "wikilink")（2004年[日本職業足球聯賽冠軍](../Page/日本職業足球聯賽.md "wikilink")）

## 積分榜

<table>
<thead>
<tr class="header">
<th><p>排名</p></th>
<th><p>球隊</p></th>
<th><p>比賽</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>入球</p></th>
<th><p>失球</p></th>
<th><p>球差</p></th>
<th><p>積分</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/水原三星藍翼足球俱樂部.md" title="wikilink">水原三星藍翼</a></p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>8</p></td>
<td><p>4</p></td>
<td><p>+4</p></td>
<td><p><strong>7</strong></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/浦項制鐵足球俱樂部.md" title="wikilink">浦項制鐵</a></p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>5</p></td>
<td><p>3</p></td>
<td><p>+2</p></td>
<td><p><strong>5</strong></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/橫濱水手.md" title="wikilink">橫濱水手</a></p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p><strong>4</strong></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/深圳市足球俱樂部.md" title="wikilink">深圳健力寶</a></p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>7</p></td>
<td><p>−6</p></td>
<td><p><strong>0</strong></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 賽果

|           |         |        |         |
| --------- | ------- | ------ | ------- |
| **日期**    | **俱樂部** | **比數** | **俱樂部** |
| 13-2-2005 | 浦項制鐵    | 1–1    | 橫濱水手    |
| 13-2-2005 | 水原三星    | 3–1    | 深圳健力宝   |
| 16-2-2005 | 橫濱水手    | 2–0    | 深圳健力宝   |
| 16-2-2005 | 水原三星    | 2–2    | 浦項制鐵    |
| 19-2-2005 | 水原三星    | 3–1    | 橫濱水手    |
| 19-2-2005 | 浦項制鐵    | 2–0    | 深圳健力宝   |

## 每轮排名

| 球队                                   | 1 | 2 | 3 |
| ------------------------------------ | - | - | - |
| [水原三星](../Page/水原三星.md "wikilink")   | 1 | 1 | 1 |
| [浦项制铁](../Page/浦项制铁.md "wikilink")   | 2 | 3 | 2 |
| [横滨水手](../Page/横滨水手.md "wikilink")   | 2 | 2 | 3 |
| [深圳健力宝](../Page/深圳健力宝.md "wikilink") | 4 | 4 | 4 |
|                                      |   |   |   |


{| width=95% |- align=center |**2005年A3冠军杯冠军**:


**[水原三星](../Page/水原三星.md "wikilink")**
**历史上首个冠军**

|}

[Category:2005年足球](../Category/2005年足球.md "wikilink")
[Category:A3冠军杯](../Category/A3冠军杯.md "wikilink")