[2005KMT-hepingzhilv-S.png](https://zh.wikipedia.org/wiki/File:2005KMT-hepingzhilv-S.png "fig:2005KMT-hepingzhilv-S.png")

**2005年中國國民黨和平之旅**，指2005年時任[中國國民黨主席](../Page/中國國民黨主席.md "wikilink")[連戰到](../Page/連戰.md "wikilink")[中國大陸進行的私人參訪活動](../Page/中國大陸.md "wikilink")。他與時任[中共中央總書記](../Page/中共中央總書記.md "wikilink")[胡錦濤在](../Page/胡錦濤.md "wikilink")[北京人民大會堂進行會談](../Page/北京人民大會堂.md "wikilink")，並達成五點共識，同意在[九二共識上推動兩岸談判](../Page/九二共識.md "wikilink")。這是自[國共内戰以来](../Page/國共内戰.md "wikilink")，[中國國民黨和](../Page/中國國民黨.md "wikilink")[中國共產黨之間首次最高層次的會晤](../Page/中國共產黨.md "wikilink")，[連戰成為](../Page/連戰.md "wikilink")1949年后第一位踏上中國大陸的國民黨最高領導人。

## 背景

中国国民党在2005年初提出预备将在年内首先由一位副主席訪問中國大陸，以开启[海峡两岸的谈判大门](../Page/海峡两岸.md "wikilink")，而主席连战本人也很早就有意愿访问中國大陸（国民党原先规划在[2004年中华民国总统大选结束](../Page/2004年中华民国总统大选.md "wikilink")、連[宋当选后](../Page/宋楚瑜.md "wikilink")，将由連戰以[總統當選人身份在正式就职前访问中國大陸](../Page/中華民國總統.md "wikilink")\[1\]，但后因敗選未能成行）。

前[中国国民党副主席](../Page/中国国民党副主席.md "wikilink")[江丙坤于](../Page/江丙坤.md "wikilink")2005年3月28日率访问团抵达[广州](../Page/广州.md "wikilink")，开始被称为“破冰之旅”的中国大陸之行。3月30日晨，江丙坤一行在[南京拜谒了](../Page/南京.md "wikilink")[中山陵](../Page/中山陵.md "wikilink")，随后动身前往[北京](../Page/北京.md "wikilink")，同时任[中共中央政治局常委](../Page/中共中央政治局常委.md "wikilink")、[全国政协主席](../Page/全国政协主席.md "wikilink")[贾庆林会晤](../Page/贾庆林.md "wikilink")。会晤中贾庆林转达了中共中央总书记[胡锦涛对中国国民党主席连战造访中国大陸的邀请](../Page/胡锦涛.md "wikilink")。4月1日正在[日本参观](../Page/日本.md "wikilink")[爱知世博会的连战接受贾庆林代表胡锦涛提出的邀请](../Page/爱知世博会.md "wikilink")，准备前往中国大陸访问。

由于中共方面对江丙坤一行高规格的接待以及其高姿态地邀请连战访问，再加上[亲民党主席](../Page/亲民党.md "wikilink")[宋楚瑜预定](../Page/宋楚瑜.md "wikilink")5月初代表[中華民國總統](../Page/中華民國總統.md "wikilink")[陳水扁访问中国大陸](../Page/陳水扁.md "wikilink")，连战访问中國大陸的日期也从原定的5月中旬提前到4月底。对于連戰的訪問，中国大陸民众普遍表示欢迎，海外華人華僑团体亦有赞赏及反對兩派意見對立。在台湾，民众对连战的访问依然主要以统独意识形态明显划分，大多数[泛蓝群众表示支持](../Page/泛蓝.md "wikilink")，而大多数[泛绿选民则不满连战的访问](../Page/泛绿.md "wikilink")。

## 经过

### 中正國際機場

连战一行近70人于2005年4月26日由[台北启程](../Page/台北.md "wikilink")，展开为期八天的“**和平之旅**”。当日晨，逾千人聚集[中正國際機場](../Page/中正國際機場.md "wikilink")，反對者與支持者爆發嚴重的肢體衝突，多人流血受伤。中国大陸媒体在做报道的时候，将中正机场稱为桃园机场（2006年10月台湾才将该机场改名為桃園機場）。

當天早上泛藍支持者[竹聯幫](../Page/竹聯幫.md "wikilink")\[2\]\[3\]
西堂成員著黑衣首先於中山足球場北側破壞抗議人士準備搭乘前往機場的巴士\[4\]。稍後，連戰赴機場途中，遭到由[史明策劃的四輛計程車前後包抄](../Page/史明.md "wikilink")，貼車阻擋，後來仰賴隨扈駕車反擋，連戰才得脫困\[5\]\[6\]。

在第二航廈，六時左右，第一批人馬泛藍支持者抵達，他們穿國旗裝、持中華民國國旗，要為連戰送行；接著陸陸續續有人趕到，總數約八十多人，進入出境大廳內。近上午八時，泛藍人馬將近千人抵達機場，多數被阻擋在航站大廳外，同時王世堅帶領七、八百名綠營群眾及大批旗幟，趁停車場入口警力微薄之際，進入第二航廈內。十時卅分，竹聯幫前虎堂堂主朱家訓突然現身，身後跟隨著卅餘名染髮、黑衣，年約十八、九歲的青少年加入泛藍人馬夾道鼓掌、歡呼，朱家訓率領這批黑衣部隊自北側的安全門進入，門口的員警以為綠營都已散去，遂任由他們進入，結果造成多人被害。雙方爆發十五波以上的大小衝突，以事前準備的[關刀](../Page/關刀.md "wikilink")、彈弓、空藥酒玻璃瓶、石頭、鞭炮、甘蔗、雞蛋等物品互相攻擊，造成多人受傷，以及多家機場商店、航空公司櫃臺被迫關閉。事後藍綠雙方都有多人被移送法辦。\[7\]

帶領泛綠的[台聯立委](../Page/台灣團結聯盟.md "wikilink")[羅志明](../Page/羅志明.md "wikilink")、民進黨立委[王世堅](../Page/王世堅.md "wikilink")、[徐國勇](../Page/徐國勇.md "wikilink")、[林國慶](../Page/林國慶.md "wikilink")、[潘孟安](../Page/潘孟安.md "wikilink")、臺南市議員[王定宇](../Page/王定宇.md "wikilink")、政論節目主持人[汪笨湖](../Page/汪笨湖.md "wikilink")、[建國黨外圍團體](../Page/建國黨.md "wikilink")[台灣捍衛隊成員黃明財等人](../Page/台灣捍衛隊.md "wikilink")，則以妨害公務罪嫌移送法辦。\[8\]

泛藍支持群眾則由[愛國同心會](../Page/愛國同心會.md "wikilink")、[新黨主席](../Page/新黨.md "wikilink")-{[郁慕明](../Page/郁慕明.md "wikilink")}-、台北縣議員[金介壽等人帶領](../Page/金介壽.md "wikilink")，群眾中也有黑道背景的黑衣部隊\[9\]\[10\]，專挑落單的泛綠老年群眾痛毆，造成多人重傷送醫。而當天揚言「血債血償」並與警方發生衝撞的-{郁}-慕明、新黨秘書長等人帶領。[李勝峰](../Page/李勝峰.md "wikilink")、前[竹聯幫虎鳳隊隊長](../Page/竹聯幫.md "wikilink")[王蘭等人也依妨害公務罪嫌函送法辦](../Page/王蘭.md "wikilink")。

在機場外，由台獨大老[史明所發動的泛綠群眾也利用許多計程車在](../Page/史明.md "wikilink")[高速公路上](../Page/高速公路.md "wikilink")，試圖包夾連戰座車以阻礙登機，但隨後遭到警方驅離。史明等人之後又在機場大廳違法點燃[鞭炮](../Page/鞭炮.md "wikilink")，事後已移送法辦。

由於[航警局處理不當](../Page/航警局.md "wikilink")，放縱兩派抗議群眾進入機場大廳，警力調派不當、又未逮捕現行犯。局長[陳瑞添當晚隨即遭到撤換](../Page/陳瑞添.md "wikilink")，內政部部長[蘇嘉全口頭請辭](../Page/蘇嘉全.md "wikilink")、次長[林永堅辭職獲准](../Page/林永堅.md "wikilink")。

### 南京

[kuomintang_nanjing.jpg](https://zh.wikipedia.org/wiki/File:kuomintang_nanjing.jpg "fig:kuomintang_nanjing.jpg")
[kuomintang_nanjing2.jpg](https://zh.wikipedia.org/wiki/File:kuomintang_nanjing2.jpg "fig:kuomintang_nanjing2.jpg")
访问团上午在[香港转机](../Page/香港.md "wikilink")，受到[中聯辦主任等人在机场欢迎](../Page/中央人民政府駐香港特別行政區聯絡辦公室.md "wikilink")，随后搭乘[中国东方航空公司专机前往](../Page/中国东方航空公司.md "wikilink")[南京](../Page/南京.md "wikilink")。

连战同访问团于4月26日16时40分左右包机抵达[南京禄口国际机场](../Page/南京禄口国际机场.md "wikilink")，同行有中国国民党副主席[江丙坤](../Page/江丙坤.md "wikilink")、[吴伯雄](../Page/吴伯雄.md "wikilink")、[林澄枝](../Page/林澄枝.md "wikilink")、秘书长[林丰正等](../Page/林丰正.md "wikilink")。[國台办主任](../Page/国务院台湾事务办公室.md "wikilink")[陈云林等在机场迎接](../Page/陈云林.md "wikilink")。连战随后在[停机坪上发表简短](../Page/停机坪.md "wikilink")[演说](../Page/演说.md "wikilink")，连战称抵达后“有一种相见恨晚的感觉”。是夜，连战一行接受[江苏省领导宴请](../Page/江苏省.md "wikilink")，并下榻[金陵饭店](../Page/金陵饭店.md "wikilink")。当日，大批南京市民自发上街对连战来访夹道欢迎。

4月27日8时50分，连战一行人抵达[中山陵](../Page/中山陵.md "wikilink")，中山陵还特意为国民党-{}-参访团开启了中间的大门，但并没有限制其他游客的正常游览。在众多特意赶来的当地民众与国际媒体记者的簇拥下开始举行拜谒仪式。连战随后在现场发表讲话，表示自己是以“庄严虔敬”的心情前来向创党总理[孙中山先生致意](../Page/孙中山.md "wikilink")；他提到目前两岸关系严峻，但又以孙中山的遗言来勉励大家“不要忘记和平、奋斗，救中国”，“做一个扬眉吐气的中华民族”，并为中山陵题词“中山美陵”。连战是56年来首位拜谒中山陵的国民党主席。

同日，连战一行还参观了[明孝陵](../Page/明孝陵.md "wikilink")、[原国民政府总统府](../Page/南京总统府.md "wikilink")、[南京天妃宫和](../Page/南京天妃宫.md "wikilink")[夫子庙](../Page/夫子庙.md "wikilink")。

4月28日上午，连战一行离开南京，乘机前往[北京](../Page/北京.md "wikilink")。

### 北京

连战一行4月28日中午抵达[北京](../Page/北京.md "wikilink")，在[北京首都国际机场发表了演讲](../Page/北京首都国际机场.md "wikilink")，随后即与[國台办在](../Page/國台办.md "wikilink")[钓鱼台国宾馆举行了工作会谈](../Page/钓鱼台国宾馆.md "wikilink")。下午，连战一行来到[北京故宫博物院参观](../Page/故宫.md "wikilink")，据现场一位从事外事工作多年的工作人员介绍，类似当日清场的举措属故宫近年难见的高规格接待。[全国政协主席](../Page/全国政协主席.md "wikilink")[贾庆林在](../Page/贾庆林.md "wikilink")17时30分左右，于[人民大会堂会见了连战一行](../Page/人民大会堂.md "wikilink")。随后访问团来到[老舍茶馆欣赏](../Page/老舍茶馆.md "wikilink")[戏曲演出](../Page/戏曲曲艺.md "wikilink")。

4月29日10时左右，连战在[北京大学行政楼礼堂发表演说并回答师生提问](../Page/北京大学.md "wikilink")。然後連戰在此次演說中曾提到[胡適先生和前](../Page/胡適.md "wikilink")[臺灣大學校長](../Page/臺灣大學.md "wikilink")[傅斯年等人當年都是北大出身](../Page/傅斯年.md "wikilink")，再到臺大任職，傳播自由主義的思想，「所以，簡單地來講，自由的思想，北大、台大系出同源，可以說是一脈相傳，尤其在中国，可以說是歷史上的一個自由的堡壘，隔了一個海峽，相互輝映」。但是這樣的說法卻激起了臺灣大學反對學生的不滿，他們認為胡適及傅斯年當年皆堅決反對[共產主義](../Page/共產主義.md "wikilink")，維護[學術自由](../Page/學術自由.md "wikilink")，兩校並不能相提並論，並持續在校門口抗議。
[zggmdfwt.jpg](https://zh.wikipedia.org/wiki/File:zggmdfwt.jpg "fig:zggmdfwt.jpg")

連戰随后参观了学校，以及其母昔日就读[燕京大学的宿舍楼](../Page/燕京大学.md "wikilink")。15时，连战与[中国共产党总书记](../Page/中国共产党.md "wikilink")[胡锦涛在](../Page/胡锦涛.md "wikilink")[人民大会堂北大厅进行历史性会见](../Page/人民大会堂.md "wikilink")，随后至福建厅举行会晤。这是自1945年之后，国共两党最高领导人首次会晤。17时30分左右，在[北京饭店举行](../Page/北京饭店.md "wikilink")“中国国民党连战主席和平之旅记者会”，會後為規避法律責任，由[国民党文传会主任张荣恭以](../Page/中國國民黨中央委員會文化傳播委員會.md "wikilink")「新聞公報」的形式，指出两党达成的五点共识：

  -
    一、在认同“[九二共识](../Page/九二共识.md "wikilink")”的基础上促进恢复两岸谈判；
    二、促进终止敌对状态，达成和平协议；
    三、促进两岸在经贸交流和共同打击犯罪等方面建立合作机制，推进双向直航、三通和农业交流；
    四、促进扩大台湾国际空间的谈判；
    五、建立国共两党定期沟通平台。

之后连战接受了媒体记者采访。

当晚，连战临时改变计划，再度同胡锦涛进行会面，据称是胡锦涛邀请在京最后一夜的连战一叙，并未谈及政治话题。

### 西安

4月30日11时25分，访问团飞抵[西安咸阳国际机场](../Page/西安咸阳国际机场.md "wikilink")，开始了连战首次重返出生地[西安之行](../Page/西安.md "wikilink")。随同访问的有中共中央台办常务副主任[李炳才](../Page/李炳才.md "wikilink")、中共中央台办副局长[何建华](../Page/何建华.md "wikilink")、[李维一等](../Page/李维一.md "wikilink")。[陕西省委副书记](../Page/陕西.md "wikilink")[杨永茂](../Page/杨永茂.md "wikilink")，省委常委、秘书长[李希等到机场迎接](../Page/李希.md "wikilink")。到机场欢迎国民党访问团的还有在西安的20多名[台商](../Page/台商.md "wikilink")，以及为连战夫妇献花的[后宰门小学学生](../Page/后宰门小学_\(西安\).md "wikilink")。

下午3时，连战重返阔别60余年的小学母校[北新街小学](../Page/北新街小学.md "wikilink")（现名后宰门小学）参观并发表讲话、观看“小学长”的演出（[《陽光、校園、童年》](../Page/爺爺您回來了.md "wikilink")）。随后向该校捐赠《[台湾通史](../Page/台湾通史.md "wikilink")》等礼物并为校图书馆捐款。后宰门小学则向连战赠送了一幅由学生们制作的长卷画幅《长安自古多名士、江南春暮隐奇贤》。

连战结束对母校访问之后，一行人乘车前往位于[临潼的](../Page/临潼.md "wikilink")[秦始皇兵马俑博物馆参观](../Page/秦始皇陵兵马俑.md "wikilink")。博物馆安排了盛大的解說團隊，一切比照元首規格，特准連戰親手觸碰兵馬俑，還預先遞上擦手毛巾，但連戰並未伸手摸兵馬俑。博物馆向连战赠送了出土于[秦始皇帝陵兵马俑一号坑的席纹印迹土块](../Page/秦始皇帝陵兵马俑一号坑.md "wikilink")。连战则为博物馆题词「**遊秦塚而憫萬民，跨兩岸為創雙贏**」。

当晚，中共陕西省委书记[李建国在](../Page/李建国.md "wikilink")[大唐芙蓉园以仿唐御宴招待连战访问团](../Page/大唐芙蓉园.md "wikilink")，李建国向连战与访问团赠送了[碑林的](../Page/碑林.md "wikilink")[拓片集和](../Page/拓片.md "wikilink")[青铜器](../Page/青铜器.md "wikilink")“轩辕圣土簋”，连战亦回赠台湾[琉璃名家](../Page/琉璃.md "wikilink")[王侠军制作的工艺品](../Page/王侠军.md "wikilink")“生生不息”等。随后，连战和访问团在凤鸣九天剧院观看了再现[唐朝盛世气象的](../Page/唐朝.md "wikilink")“梦回大唐”大型歌舞表演。

5月1日上午连战及家人、中国国民党中國大陸访问团赴西安连战祖母的墓地祭扫，并在近旁清凉寺上香。

### 上海

5月1日17时，连战按预期准时飞抵[上海浦东国际机场](../Page/上海浦东国际机场.md "wikilink")。

5月2日上午，连战取消原定登临“[东方明珠广播电视塔](../Page/东方明珠广播电视塔.md "wikilink")”的行程在[上海香格里拉饭店召开](../Page/上海香格里拉饭店.md "wikilink")“中国国民党连战主席和平之旅记者会”。随后赴[锦江小礼堂拜会](../Page/锦江小礼堂.md "wikilink")[汪道涵](../Page/汪道涵.md "wikilink")。晚9时游览[新天地](../Page/新天地.md "wikilink")，并于其后乘上海风采号游船观赏浦江夜景。

5月3日下午1点，连战乘坐[上海磁浮示范运营线抵上海浦东国际机场](../Page/上海磁浮示范运营线.md "wikilink")，搭机经由香港返國。

5月4日上午10点，连战在台北举行記者会。

## 各方反应

### 對於連戰此次訪問的觀點

#### 中立

  - [白德華](../Page/白德華.md "wikilink")（中国时报评论员）

在台湾，即将走入历史的连战，在大陆，竟成为开创历史的人物。

  - [陈文茜](../Page/陈文茜.md "wikilink")（台湾政治评论家）：

历史有时走得很慢，有时走得很快。一晃眼，中国与台湾隔阂了一百多年；一晃眼，台独公投制宪才刚喊完一年。一切成空，徒留遗恨，只剩城墙……天安门广场，游客打探连战生平与家人，历史好像断了，从满清又接回了国民党时代……
[清朝过去了](../Page/清朝.md "wikilink")，[中国国民党过去了](../Page/中国国民党.md "wikilink")，[文化大革命也过去了](../Page/文化大革命.md "wikilink")。一段属于新的历史，正关键性地展开。

  - [石全瑜](../Page/石全瑜.md "wikilink")（1921年7月7日—2011年2月11日，[中华人民共和国爱国民主人士](../Page/中华人民共和国.md "wikilink")）

与[二二八事件前相若的是](../Page/二二八事件.md "wikilink")，熟悉的世界秩序崩溃了。二二八时的感觉是，好人根本不好；现在的危机是，坏人未必坏。两者都令人恐惧。作为台独对立面的中国，与台湾的区隔开始模糊。

#### 赞成

**[陈水扁](../Page/陈水扁.md "wikilink")（时[中华民国总统](../Page/中华民国总统.md "wikilink")）：**祝福连战，连战谨守际份，对话的大门始终敞开。5月3日，陈水扁总统在太平洋地区[邦交国访问时表示](../Page/邦交国.md "wikilink")：“如果两岸和平是一出戏的话，连戰出访大陸仅仅是序曲，主戏还没有上演。”，在連戰出訪大陸結束後，親民黨主席[宋楚瑜也有意出訪](../Page/宋楚瑜.md "wikilink")，於是宋楚瑜主動去拜訪陳水扁总统，並由陳水扁总统授權，代表政府出訪大陸。同日，他还正式表示“希望大陸领导人[胡锦涛能到](../Page/胡锦涛.md "wikilink")[台湾来走走看看](../Page/台湾.md "wikilink")”。\[11\]

## 相關事件

[Taiwan_fruits.jpg](https://zh.wikipedia.org/wiki/File:Taiwan_fruits.jpg "fig:Taiwan_fruits.jpg")

2005年4月30日，[重慶人士](../Page/重慶.md "wikilink")[許萬平遭中華人民共和國警察逮捕](../Page/許萬平.md "wikilink")。許萬平曾參與聯署致中國國民黨主席連戰公開信、呼籲連戰到中國大陸開拓民主政治，讓[三民主義重返中國大陸](../Page/三民主義.md "wikilink")，與中國共產黨民主競爭。除了許萬平外，也有其他涉及此事的相關人士被逮捕，或是不同程度地受到中華人民共和國公安機關警告。這些人以「[中國泛藍聯盟](../Page/中國泛藍聯盟.md "wikilink")」、「[中國國民黨精神黨員](../Page/中國國民黨精神黨員.md "wikilink")」的身份，推薦[連戰和](../Page/連戰.md "wikilink")[馬英九為中國國民黨參加聯邦中國正](../Page/馬英九.md "wikilink")、副總統候選人參與全中國總統大選。\[12\]

## 参考文献

## 外部链接

  - 两岸

<!-- end list -->

  - [凤凰网专题报道](http://www.phoenixtv.com/phoenixtv/72628446731173888/index.shtml)
  - [新浪网专题报道](http://news.sina.com.cn/z/lzfdl/index.shtml)
  - [新华网专题报道](http://www.xinhuanet.com/taiwan/zt050401/index.htm)[新华网图片集](http://www.xinhuanet.com/newscenter/lzdlx/tpbd.htm)
  - [中新网专题报道](http://www.chinanews.com/focus_site/lianzhan/index.shtml)

<!-- end list -->

  - 国际

<!-- end list -->

  - [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_4480000/newsid_4488100/4488195.stm)
  - [NYT - Taiwan Opposition Leader Arrives in China for High-Level
    Talks](http://www.nytimes.com/2005/04/26/international/asia/26cnd-chin.html)
  - [美国之音中文版（许万平被捕报道）](http://www.voanews.com/chinese/n2005-05-03-voa33.cfm)
  - [大纪元（许万平被捕报道）](http://dajiyuan.com/b5/5/5/5/n910894.htm)

## 参见

  - [2005年亲民党搭桥之旅](../Page/2005年亲民党搭桥之旅.md "wikilink")
  - [2005年台湾新党民族之旅](../Page/2005年台湾新党民族之旅.md "wikilink")
  - [国共合作](../Page/国共合作.md "wikilink")
  - [中国历史](../Page/中国历史.md "wikilink")
  - [台湾问题](../Page/台湾问题.md "wikilink")
  - [台湾政治](../Page/台湾政治.md "wikilink")
  - [海峽兩岸關係](../Page/海峽兩岸關係.md "wikilink")
  - [海峽兩岸相關主題列表](../Page/海峽兩岸相關主題列表.md "wikilink")
  - [中华民国](../Page/中华民国.md "wikilink")
  - [中华人民共和国](../Page/中华人民共和国.md "wikilink")
  - [爷爷您回来了](../Page/爷爷您回来了.md "wikilink")

{{-}}

[Category:臺灣海峽兩岸關係政治史](../Category/臺灣海峽兩岸關係政治史.md "wikilink")
[Category:国共会谈](../Category/国共会谈.md "wikilink")
[Category:2005年中国政治事件](../Category/2005年中国政治事件.md "wikilink")
[Category:2005年台湾政治事件](../Category/2005年台湾政治事件.md "wikilink")
[Category:2005年4月](../Category/2005年4月.md "wikilink")
[Category:2005年5月](../Category/2005年5月.md "wikilink")

1.  [Chi Wang, 2011, A Compelling Journey from Beijing to Washington:
    Building a New Life in
    America](https://books.google.de/books?id=JN1ZRfOUceEC&pg=PA180&lpg=PA180&dq=the+KMT+first+proposed+that+the+former+president+candidate+Lien+Chan+would+visit+mainland+China+as+the+elected+President+of+the+Republic+of+China&source=bl&ots=WHza_JbEeh&sig=WjfSw5V0MaESu1fAKdCrNqpBaPU&hl=en&sa=X&ved=0ahUKEwjUhOf4muTQAhULBcAKHX7_BTgQ6AEINzAF#v=onepage&q&f=false)

2.  大紀元時報2005/4/28：[大執法
    掃蕩暴力黑衣軍](http://www.epochtimes.com/b5/5/4/28/n903021.htm)

3.
4.  臺北市政府警察局中山分局發佈之警政資訊
    [破獲竹聯幫西堂「魁星會」不法黑道幫派](http://www.taipei.gov.tw/cgi-bin/Message/MM_msg_control?mode=viewnews&ts=46362286:6ca2&theme=3790000000/379130000C/379130200C/Msg)


5.  自由時報2005/04/27：[大戰十多回
    出手都很狠](http://www.libertytimes.com.tw/2005/new/apr/27/today-fo4.htm)


6.  東森新聞2005/04/27：[和平之旅／計程車國道攔連、鞭炮轟機場　全由史明主導](http://www.ettoday.com/2005/04/27/301-1782948.htm)

7.
8.
9.
10.
11. [复兴记 Fuxingji：The revival of
    China](https://books.google.com.tw/books?id=DjSSBQAAQBAJ&pg=PA649&lpg=PA649&dq=%E9%80%A3%E6%88%B0%E8%AC%B9%E5%AE%88%E9%9A%9B%E4%BB%BD&source=bl&ots=XoC5rmePvb&sig=Cny9zqxZMVYJgomxuGRZSn6bGbI&hl=zh-TW&sa=X&ei=O6RSVezYNeSgmwXig4CYDw&ved=0CB4Q6AEwAA#v=onepage&q=%E9%80%A3%E6%88%B0%E8%AC%B9%E5%AE%88%E9%9A%9B%E4%BB%BD&f=false)

12.