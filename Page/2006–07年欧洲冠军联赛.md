**2006–07年歐洲冠軍聯賽**（**UEFA Champions League**）為第 52
屆[歐洲冠軍聯賽](../Page/歐洲冠軍聯賽.md "wikilink")，來自 49
個歐洲國家所屬球會進行爭奪冠軍，決賽階段於2007年5月23日在[希臘](../Page/希臘.md "wikilink")[雅典](../Page/雅典.md "wikilink")[奧林匹克體育場舉行](../Page/雅典奧林匹克體育場.md "wikilink")，由意大利的[AC米蘭迎戰英格蘭的](../Page/AC米蘭.md "wikilink")[利物浦](../Page/利物浦足球俱樂部.md "wikilink")，這是兩隊繼[2004–05年歐聯決賽後再度對碰](../Page/2004年至2005年歐洲聯賽冠軍盃.md "wikilink")。最終[AC米蘭在常規時間內](../Page/AC米蘭.md "wikilink")
2–1 戰勝了[利物浦](../Page/利物浦足球俱樂部.md "wikilink")，歷史上第七次捧起冠軍獎盃。

## 早期事件

[UCL2007Final.jpg](https://zh.wikipedia.org/wiki/File:UCL2007Final.jpg "fig:UCL2007Final.jpg")決賽海報\]\]

### 意大利假球醜聞

2006年，意大利球壇爆出當時最轟動的假波案，應屆冠軍[祖雲達斯](../Page/祖雲達斯.md "wikilink")、[AC米蘭](../Page/AC米蘭.md "wikilink")、[佛罗伦萨以及](../Page/佛罗伦萨足球俱乐部.md "wikilink")[拉齐奥涉嫌安排球證操縱比賽結果](../Page/拉齐奥足球俱乐部.md "wikilink")。該四間球會被意大利法院及意大利足總作出判決，原定可以參加下屆[歐聯及](../Page/歐聯.md "wikilink")[歐洲足協盃的他們被判禁止參加來屆所有歐洲賽事](../Page/歐洲足協盃.md "wikilink")，但AC米蘭在7月25日進行的上訴裁決獲得減刑，並且得以參加來屆歐聯第三圈外圍賽。

經最後裁決後，歐洲賽參賽資格由以下球隊取代：

| **歐洲聯賽冠軍盃**                             |
| --------------------------------------- |
| 假波案判決前參賽名額                              |
| [祖雲達斯](../Page/尤文圖斯足球俱樂部.md "wikilink") |
| [AC米蘭](../Page/AC米蘭.md "wikilink")      |
| [國際米蘭](../Page/國際米蘭足球俱樂部.md "wikilink") |
| [費倫天拿](../Page/佛羅倫薩足球俱樂部.md "wikilink") |
| **歐洲足協盃**                               |
| [羅馬](../Page/羅馬體育俱樂部.md "wikilink")     |
| [基爾禾](../Page/切沃維羅納足球俱樂部.md "wikilink") |
| [拉素](../Page/拉齊奧足球俱樂部.md "wikilink")    |

### 對希臘足球總會的禁賽令

[國際足協基於希臘政府嚴重](../Page/國際足協.md "wikilink")-{干}-預足球組織運作，因此2006年7月3日開始禁止希臘參加所有國際足球賽事，不過希臘議會就事件批准修訂有關體育管理方面的條例後，國際足協於7月12日取消對希臘足協的禁賽令。

## 外圍賽

### 外圍賽第一圈

首回合於2006年7月11日至12日進行，次回合則於同年7月18日至19日進行。

|}

### 外圍賽第二圈

首回合於2006年7月25日至26日進行，次回合則於同年8月1日至2日進行。

|}

### 外圍賽第三圈

首回合於2006年8月8日至9日進行，次回合則於同年8月22日至23日進行。                  |}

  - <sup>1</sup>以[塞爾維亞和黑山球隊名義參賽](../Page/塞爾維亞和黑山.md "wikilink")，現在屬於[塞爾維亞球隊](../Page/塞爾維亞.md "wikilink")。
  - <sup>2</sup>基於[以巴衝突令以色列局勢不穩定的安全理由](../Page/以巴衝突.md "wikilink")，[歐洲足協將利物浦作客對海法馬卡比的賽事改於](../Page/歐洲足協.md "wikilink")[烏克蘭的](../Page/烏克蘭.md "wikilink")[基輔舉行](../Page/基輔.md "wikilink")。

## 分組賽

總數 32 球隊會被分成 8 組，每組 4
隊參加分組賽。每組首名及次名均晉級十六強淘汰賽，第三名則轉戰[歐洲足協盃三十二強淘汰賽繼續參與歐洲賽事](../Page/歐洲足協盃.md "wikilink")。

如在小組中兩隊得分相同則順次序根據以下準則定出勝方：

  - 雙方對賽成績較佳的一方
  - 雙方對賽中淨勝球較多的一方
  - 雙方對賽中獲得較多作客進球的一方
  - 總得失球較佳的一方
  - 總進球較佳的一方
  - 根據歐洲足協的系數排名

| 圖示            |
| ------------- |
| 分組賽出線，晉級歐聯十六強 |
| 轉戰歐洲足協盃三十二強   |

### A組

<table>
<thead>
<tr class="header">
<th><p>隊伍</p></th>
<th><p>賽</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>得球</p></th>
<th><p>失球</p></th>
<th><p>球差</p></th>
<th><p>分</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/切爾西足球俱樂部.md" title="wikilink">車路士</a></p></td>
<td><p>6</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>10</p></td>
<td><p>4</p></td>
<td><p>+6</p></td>
<td><p><strong>13</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/巴塞羅那足球俱樂部.md" title="wikilink">巴塞隆拿</a></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>12</p></td>
<td><p>4</p></td>
<td><p>+8</p></td>
<td><p><strong>11</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/雲達不萊梅體育會.md" title="wikilink">雲達不萊梅</a></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>7</p></td>
<td><p>5</p></td>
<td><p>+2</p></td>
<td><p><strong>10</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/索菲亞列夫斯基足球俱樂部.md" title="wikilink">索菲亞利夫斯基</a></p></td>
<td><p>6</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>6</p></td>
<td><p>1</p></td>
<td><p>17</p></td>
<td><p>-16</p></td>
<td><p><strong>0</strong></p></td>
</tr>
</tbody>
</table>

|            |      |           |         |  |
| ---------- | ---- | --------- | ------- |  |
| 2006年9月12日 | 巴塞隆拿 | **5 – 0** | 索非亞利夫斯基 |  |
|            | 車路士  | **2 – 0** | 雲達不萊梅   |  |

|            |         |           |      |  |
| ---------- | ------- | --------- | ---- |  |
| 2006年9月27日 | 雲達不萊梅   | **1 – 1** | 巴塞隆拿 |  |
|            | 索非亞利夫斯基 | **1 – 3** | 車路士  |  |

|             |       |           |         |  |
| ----------- | ----- | --------- | ------- |  |
| 2006年10月18日 | 車路士   | **1 – 0** | 巴塞隆拿    |  |
|             | 雲達不萊梅 | **2 – 0** | 索非亞利夫斯基 |  |

|             |         |           |       |  |
| ----------- | ------- | --------- | ----- |  |
| 2006年10月31日 | 巴塞隆拿    | **2 – 2** | 車路士   |  |
|             | 索非亞利夫斯基 | **0 – 3** | 雲達不萊梅 |  |

|             |         |           |      |  |
| ----------- | ------- | --------- | ---- |  |
| 2006年11月22日 | 雲達不萊梅   | **1 – 0** | 車路士  |  |
|             | 索非亞利夫斯基 | **0 – 2** | 巴塞隆拿 |  |

|            |      |           |         |  |
| ---------- | ---- | --------- | ------- |  |
| 2006年12月5日 | 巴塞隆拿 | **2 – 0** | 雲達不萊梅   |  |
|            | 車路士  | **2 – 0** | 索非亞利夫斯基 |  |

### B組

<table>
<thead>
<tr class="header">
<th><p>隊伍</p></th>
<th><p>賽</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>得球</p></th>
<th><p>失球</p></th>
<th><p>球差</p></th>
<th><p>分</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/拜仁慕尼黑足球俱樂部.md" title="wikilink">拜仁慕尼黑</a></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>10</p></td>
<td><p>3</p></td>
<td><p>+7</p></td>
<td><p><strong>12</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/國際米蘭足球俱樂部.md" title="wikilink">國際米蘭</a></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>5</p></td>
<td><p>5</p></td>
<td><p>0</p></td>
<td><p><strong>10</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/莫斯科斯巴達克足球俱樂部.md" title="wikilink">莫斯科斯巴達</a></p></td>
<td><p>6</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>7</p></td>
<td><p>11</p></td>
<td><p>-4</p></td>
<td><p><strong>5</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/葡萄牙士砵亭俱樂部.md" title="wikilink">士砵亭</a></p></td>
<td><p>6</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>3</p></td>
<td><p>6</p></td>
<td><p>-3</p></td>
<td><p><strong>5</strong></p></td>
</tr>
</tbody>
</table>

|            |       |           |        |  |
| ---------- | ----- | --------- | ------ |  |
| 2006年9月12日 | 拜仁慕尼黑 | **4 – 0** | 莫斯科斯巴達 |  |
|            | 士砵亭   | **1 – 0** | 國際米蘭   |  |

|            |        |           |       |  |
| ---------- | ------ | --------- | ----- |  |
| 2006年9月27日 | 國際米蘭   | **0 – 2** | 拜仁慕尼黑 |  |
|            | 莫斯科斯巴達 | **1 – 1** | 士砵亭   |  |

|             |      |           |        |  |
| ----------- | ---- | --------- | ------ |  |
| 2006年10月18日 | 國際米蘭 | **2 – 1** | 莫斯科斯巴達 |  |
|             | 士砵亭  | **0 – 1** | 拜仁慕尼黑  |  |

|             |        |           |      |  |
| ----------- | ------ | --------- | ---- |  |
| 2006年10月31日 | 拜仁慕尼黑  | **0 – 0** | 士砵亭  |  |
|             | 莫斯科斯巴達 | **0 – 1** | 國際米蘭 |  |

|             |        |           |       |  |
| ----------- | ------ | --------- | ----- |  |
| 2006年11月22日 | 莫斯科斯巴達 | **2 – 2** | 拜仁慕尼黑 |  |
|             | 國際米蘭   | **1 – 0** | 士砵亭   |  |

|            |       |           |        |  |
| ---------- | ----- | --------- | ------ |  |
| 2006年12月5日 | 拜仁慕尼黑 | **1 – 1** | 國際米蘭   |  |
|            | 士砵亭   | **1 – 3** | 莫斯科斯巴達 |  |

### C組

<table>
<thead>
<tr class="header">
<th><p>隊伍</p></th>
<th><p>賽</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>得球</p></th>
<th><p>失球</p></th>
<th><p>球差</p></th>
<th><p>分</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/利物浦足球俱樂部.md" title="wikilink">利物浦</a></p></td>
<td><p>6</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>11</p></td>
<td><p>5</p></td>
<td><p>+6</p></td>
<td><p><strong>13</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/PSV燕豪芬.md" title="wikilink">PSV燕豪芬</a></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>6</p></td>
<td><p>6</p></td>
<td><p>0</p></td>
<td><p><strong>10</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波爾多足球俱樂部.md" title="wikilink">波爾多</a></p></td>
<td><p>6</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>3</p></td>
<td><p>6</p></td>
<td><p>7</p></td>
<td><p>-1</p></td>
<td><p><strong>7</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/加拉塔沙雷體育會足球隊.md" title="wikilink">加拉塔沙雷</a></p></td>
<td><p>6</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
<td><p>7</p></td>
<td><p>12</p></td>
<td><p>-5</p></td>
<td><p><strong>4</strong></p></td>
</tr>
</tbody>
</table>

|            |        |           |     |  |
| ---------- | ------ | --------- | --- |  |
| 2006年9月12日 | 加拉塔沙雷  | **0 – 0** | 波爾多 |  |
|            | PSV燕豪芬 | **0 – 0** | 利物浦 |  |

|            |     |           |        |  |
| ---------- | --- | --------- | ------ |  |
| 2006年9月27日 | 波爾多 | **0 – 1** | PSV燕豪芬 |  |
|            | 利物浦 | **3 – 2** | 加拉塔沙雷  |  |

|             |       |           |        |  |
| ----------- | ----- | --------- | ------ |  |
| 2006年10月18日 | 加拉塔沙雷 | **1 – 2** | PSV燕豪芬 |  |
|             | 波爾多   | **0 – 1** | 利物浦    |  |

|             |        |           |       |  |
| ----------- | ------ | --------- | ----- |  |
| 2006年10月31日 | 利物浦    | **3 – 0** | 波爾多   |  |
|             | PSV燕豪芬 | **2 – 0** | 加拉塔沙雷 |  |

|             |     |           |        |  |
| ----------- | --- | --------- | ------ |  |
| 2006年11月22日 | 利物浦 | **2 – 0** | PSV燕豪芬 |  |
|             | 波爾多 | **3 – 1** | 加拉塔沙雷  |  |

|            |        |           |     |  |
| ---------- | ------ | --------- | --- |  |
| 2006年12月5日 | PSV燕豪芬 | **1 – 3** | 波爾多 |  |
|            | 加拉塔沙雷  | **3 – 2** | 利物浦 |  |

### D組

<table>
<thead>
<tr class="header">
<th><p>隊伍</p></th>
<th><p>賽</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>得球</p></th>
<th><p>失球</p></th>
<th><p>球差</p></th>
<th><p>分</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/巴倫西亞足球俱樂部.md" title="wikilink">華倫西亞</a></p></td>
<td><p>6</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>12</p></td>
<td><p>6</p></td>
<td><p>+6</p></td>
<td><p><strong>13</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅馬體育俱樂部.md" title="wikilink">羅馬</a></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>8</p></td>
<td><p>4</p></td>
<td><p>+4</p></td>
<td><p><strong>10</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/頓涅茨克礦工足球俱樂部.md" title="wikilink">薩克達</a></p></td>
<td><p>6</p></td>
<td><p>1</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>6</p></td>
<td><p>11</p></td>
<td><p>-5</p></td>
<td><p><strong>6</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奧林匹亞科斯足球俱樂部.md" title="wikilink">奧林比亞高斯</a></p></td>
<td><p>6</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>3</p></td>
<td><p>6</p></td>
<td><p>11</p></td>
<td><p>-5</p></td>
<td><p><strong>3</strong></p></td>
</tr>
</tbody>
</table>

|            |        |           |      |  |
| ---------- | ------ | --------- | ---- |  |
| 2006年9月12日 | 奧林比亞高斯 | **2 – 4** | 華倫西亞 |  |
|            | 羅馬     | **4 – 0** | 薩克達  |  |

|            |      |           |        |  |
| ---------- | ---- | --------- | ------ |  |
| 2006年9月27日 | 華倫西亞 | **2 – 1** | 羅馬     |  |
|            | 薩克達  | **2 – 2** | 奧林比亞高斯 |  |

|             |        |           |     |  |
| ----------- | ------ | --------- | --- |  |
| 2006年10月18日 | 華倫西亞   | **2 – 0** | 薩克達 |  |
|             | 奧林比亞高斯 | **0 – 1** | 羅馬  |  |

|             |     |           |        |  |
| ----------- | --- | --------- | ------ |  |
| 2006年10月31日 | 羅馬  | **1 – 1** | 奧林比亞高斯 |  |
|             | 薩克達 | **2 – 2** | 華倫西亞   |  |

|             |      |           |        |  |
| ----------- | ---- | --------- | ------ |  |
| 2006年11月22日 | 華倫西亞 | **2 – 0** | 奧林比亞高斯 |  |
|             | 薩克達  | **1 – 0** | 羅馬     |  |

|            |        |           |      |  |
| ---------- | ------ | --------- | ---- |  |
| 2006年12月5日 | 羅馬     | **1 – 0** | 華倫西亞 |  |
|            | 奧林比亞高斯 | **1 – 1** | 薩克達  |  |

### E組

<table>
<thead>
<tr class="header">
<th><p>隊伍</p></th>
<th><p>賽</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>得球</p></th>
<th><p>失球</p></th>
<th><p>球差</p></th>
<th><p>分</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/奧林匹克里昂.md" title="wikilink">里昂</a></p></td>
<td><p>6</p></td>
<td><p>4</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>12</p></td>
<td><p>3</p></td>
<td><p>+9</p></td>
<td><p><strong>14</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/皇家馬德里足球俱樂部.md" title="wikilink">皇家馬德里</a></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>14</p></td>
<td><p>8</p></td>
<td><p>+6</p></td>
<td><p><strong>11</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/布加勒斯特星足球俱樂部.md" title="wikilink">布加勒斯特星隊</a></p></td>
<td><p>6</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>7</p></td>
<td><p>11</p></td>
<td><p>-4</p></td>
<td><p><strong>5</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/基輔迪納摩足球俱樂部.md" title="wikilink">基輔戴拿模</a></p></td>
<td><p>6</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>4</p></td>
<td><p>5</p></td>
<td><p>16</p></td>
<td><p>-11</p></td>
<td><p><strong>2</strong></p></td>
</tr>
</tbody>
</table>

|             |         |           |         |  |
| ----------- | ------- | --------- | ------- |  |
| 2006年9月13日  | 基輔戴拿模   | **1 – 4** | 布加勒斯特星隊 |  |
|             | 里昂      | **2 – 0** | 皇家馬德里   |  |
| 2006年9月26日  | 皇家馬德里   | **5 – 1** | 基輔戴拿模   |  |
|             | 布加勒斯特星隊 | **0 – 3** | 里昂      |  |
| 2006年10月17日 | 基輔戴拿模   | **0 – 3** | 里昂      |  |
|             | 布加勒斯特星隊 | **1 – 4** | 皇家馬德里   |  |
| 2006年11月1日  | 里昂      | **1 – 0** | 基輔戴拿模   |  |
|             | 皇家馬德里   | **1 – 0** | 布加勒斯特星隊 |  |
| 2006年11月21日 | 布加勒斯特星隊 | **1 – 1** | 基輔戴拿模   |  |
|             | 皇家馬德里   | **2 – 2** | 里昂      |  |
| 2006年12月6日  | 基輔戴拿模   | **2 – 2** | 皇家馬德里   |  |
|             | 里昂      | **1 – 1** | 布加勒斯特星隊 |  |

### F組

<table>
<thead>
<tr class="header">
<th><p>隊伍</p></th>
<th><p>賽</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>得球</p></th>
<th><p>失球</p></th>
<th><p>球差</p></th>
<th><p>分</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/曼徹斯特聯足球俱樂部.md" title="wikilink">曼聯</a></p></td>
<td><p>6</p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>10</p></td>
<td><p>5</p></td>
<td><p>+5</p></td>
<td><p><strong>12</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/凱爾特人足球俱樂部.md" title="wikilink">些路迪</a></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>8</p></td>
<td><p>9</p></td>
<td><p>-1</p></td>
<td><p><strong>9</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/里斯本與本菲卡體育俱樂部.md" title="wikilink">賓菲加</a></p></td>
<td><p>6</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>3</p></td>
<td><p>7</p></td>
<td><p>8</p></td>
<td><p>-1</p></td>
<td><p><strong>7</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/哥本哈根足球會.md" title="wikilink">哥本哈根</a></p></td>
<td><p>6</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>3</p></td>
<td><p>5</p></td>
<td><p>8</p></td>
<td><p>-3</p></td>
<td><p><strong>7</strong></p></td>
</tr>
</tbody>
</table>

|             |      |           |      |  |
| ----------- | ---- | --------- | ---- |  |
| 2006年9月13日  | 曼聯   | **3 – 2** | 些路迪  |  |
|             | 哥本哈根 | **0 – 0** | 賓菲加  |  |
| 2006年9月26日  | 賓菲加  | **0 – 1** | 曼聯   |  |
|             | 些路迪  | **1 – 0** | 哥本哈根 |  |
| 2006年10月17日 | 些路迪  | **3 – 0** | 賓菲加  |  |
|             | 曼聯   | **3 – 0** | 哥本哈根 |  |
| 2006年11月1日  | 賓菲加  | **3 – 0** | 些路迪  |  |
|             | 哥本哈根 | **1 – 0** | 曼聯   |  |
| 2006年11月21日 | 賓菲加  | **3 – 1** | 哥本哈根 |  |
|             | 些路迪  | **1 – 0** | 曼聯   |  |
| 2006年12月6日  | 哥本哈根 | **3 – 1** | 些路迪  |  |
|             | 曼聯   | **3 – 1** | 賓菲加  |  |

### G組

<table>
<thead>
<tr class="header">
<th><p>隊伍</p></th>
<th><p>賽</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>得球</p></th>
<th><p>失球</p></th>
<th><p>球差</p></th>
<th><p>分</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/阿森納足球俱樂部.md" title="wikilink">阿仙奴</a></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>7</p></td>
<td><p>3</p></td>
<td><p>+4</p></td>
<td><p><strong>11</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波爾圖足球俱樂部.md" title="wikilink">波圖</a></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>9</p></td>
<td><p>4</p></td>
<td><p>+5</p></td>
<td><p><strong>11</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/莫斯科中央陸軍足球俱樂部.md" title="wikilink">莫斯科中央陸軍</a></p></td>
<td><p>6</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>4</p></td>
<td><p>5</p></td>
<td><p>-1</p></td>
<td><p><strong>8</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/漢堡體育俱樂部.md" title="wikilink">漢堡</a></p></td>
<td><p>6</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>5</p></td>
<td><p>7</p></td>
<td><p>15</p></td>
<td><p>-8</p></td>
<td><p><strong>3</strong></p></td>
</tr>
</tbody>
</table>

|             |         |           |         |  |
| ----------- | ------- | --------- | ------- |  |
| 2006年9月13日  | 漢堡      | **1 – 2** | 阿仙奴     |  |
|             | 波圖      | **0 – 0** | 莫斯科中央陸軍 |  |
| 2006年9月26日  | 阿仙奴     | **2 – 0** | 波圖      |  |
|             | 莫斯科中央陸軍 | **1 – 0** | 漢堡      |  |
| 2006年10月17日 | 莫斯科中央陸軍 | **1 – 0** | 阿仙奴     |  |
|             | 波圖      | **4 – 1** | 漢堡      |  |
| 2006年11月1日  | 阿仙奴     | **0 – 0** | 莫斯科中央陸軍 |  |
|             | 漢堡      | **1 – 3** | 波圖      |  |
| 2006年11月21日 | 阿仙奴     | **3 – 1** | 漢堡      |  |
|             | 莫斯科中央陸軍 | **0 – 2** | 波圖      |  |
| 2006年12月6日  | 波圖      | **0 – 0** | 阿仙奴     |  |
|             | 漢堡      | **3 – 2** | 莫斯科中央陸軍 |  |

### H組

<table>
<thead>
<tr class="header">
<th><p>隊伍</p></th>
<th><p>賽</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>得球</p></th>
<th><p>失球</p></th>
<th><p>球差</p></th>
<th><p>分</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/AC米蘭.md" title="wikilink">AC米蘭</a></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>8</p></td>
<td><p>4</p></td>
<td><p>+4</p></td>
<td><p><strong>10</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/里爾足球俱樂部.md" title="wikilink">里爾</a></p></td>
<td><p>6</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>8</p></td>
<td><p>5</p></td>
<td><p>+3</p></td>
<td><p><strong>9</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/AEK雅典足球會.md" title="wikilink">AEK雅典</a></p></td>
<td><p>6</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>6</p></td>
<td><p>9</p></td>
<td><p>-3</p></td>
<td><p><strong>8</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/皇家安德列治體育會.md" title="wikilink">安德列治</a></p></td>
<td><p>6</p></td>
<td><p>0</p></td>
<td><p>4</p></td>
<td><p>2</p></td>
<td><p>7</p></td>
<td><p>11</p></td>
<td><p>-4</p></td>
<td><p><strong>4</strong></p></td>
</tr>
</tbody>
</table>

|             |       |           |       |  |
| ----------- | ----- | --------- | ----- |  |
| 2006年9月13日  | AC米蘭  | **3 – 0** | AEK雅典 |  |
|             | 安德列治  | **1 – 1** | 里爾    |  |
| 2006年9月26日  | AEK雅典 | **1 – 1** | 安德列治  |  |
|             | 里爾    | **0 – 0** | AC米蘭  |  |
| 2006年10月17日 | 安德列治  | **0 – 1** | AC米蘭  |  |
|             | 里爾    | **3 – 1** | AEK雅典 |  |
| 2006年11月1日  | AEK雅典 | **1 – 0** | 里爾    |  |
|             | AC米蘭  | **4 – 1** | 安德列治  |  |
|             |       |           |       |  |
| 2006年11月21日 | 里爾    | **2 – 2** | 安德列治  |  |
|             | AEK雅典 | **1 – 0** | AC米蘭  |  |
|             |       |           |       |  |
| 2006年12月6日  | AC米蘭  | **0 – 2** | 里爾    |  |
|             | 安德列治  | **2 – 2** | AEK雅典 |  |
|             |       |           |       |  |

## 淘汰賽

除決賽外，所有淘汰賽均以主客兩回合形式進行，兩回合計勝出的球隊可獲晉級資格。當兩回合計賽和時，取得較多作客入球者可獲晉級資格；但若雙方作客入球數字相同時，就需要加時作賽。若在加時依然賽和，次回合作客一方若取得入球，可獲晉級資格。假如依然未能分出勝負，則以互射點球形式決定勝負。

### 十六強

本輪抽籤儀式於2006年12月15日在[希臘](../Page/希臘.md "wikilink")[雅典進行](../Page/雅典.md "wikilink")，首回合於2007年2月20日至21日進行，次回合則於同年3月6日至7日進行，根據賽制，十六支出線球隊如屬同一個國家或分組賽階段處於同組則不會碰頭。

|}

### 半準決賽

本輪抽籤儀式於2007年3月9日在[希臘](../Page/希臘.md "wikilink")[雅典進行](../Page/雅典.md "wikilink")，首回合於2007年4月3日至4日進行，次回合則於2007年4月10日至11日進行。八支出線球隊沒有抽籤限制，因此同一個國家或分組賽階段處於同組的球隊將有機會碰頭。最後的抽籤結果顯示半準決賽中，英格蘭球隊和意大利球隊沒有抽到同一國家的球隊，但[利物浦則會對他們在分組賽中的對手](../Page/利物浦足球俱樂部.md "wikilink")[PSV燕豪芬](../Page/PSV燕豪芬.md "wikilink")。

|}

### 準決賽

本輪抽籤儀式於2007年3月9日在[希臘](../Page/希臘.md "wikilink")[雅典進行](../Page/雅典.md "wikilink")，首回合於2007年4月24日至25日進行，次回合則於2007年5月1日至2日進行。

|}

### 決賽

決賽於2007年5月23日在[希臘](../Page/希臘.md "wikilink")[雅典](../Page/雅典.md "wikilink")[奧林匹克體育場舉行](../Page/雅典奧林匹克體育場.md "wikilink")。AC米兰在常规时间内2比1战胜了利物浦队。

<table>
<thead>
<tr class="header">
<th><p>歐洲聯賽冠軍盃<br />
2006–07 冠軍</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><br />
<strong><a href="../Page/AC米蘭.md" title="wikilink">AC米蘭</a></strong><br />
<strong>第 7 次奪冠</strong></p></td>
</tr>
</tbody>
</table>

## 神射手

<small>只列出射入4球或以上的球員：</small>

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>球員名稱</p></th>
<th><p>效力球隊</p></th>
<th><p>入球</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/卡卡.md" title="wikilink">卡卡</a></p></td>
<td><p><a href="../Page/AC米蘭.md" title="wikilink">AC米蘭</a></p></td>
<td><p>10球</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/彼得·克劳奇.md" title="wikilink">克劳奇</a></p></td>
<td><p><a href="../Page/利物浦足球會.md" title="wikilink">利物浦</a></p></td>
<td><p>6球</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/杜奧巴.md" title="wikilink">德罗巴</a></p></td>
<td><p><a href="../Page/車路士.md" title="wikilink">切尔西</a></p></td>
<td><p>6球</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/摩連迪斯.md" title="wikilink">莫伦特斯</a></p></td>
<td><p><a href="../Page/華倫西亞足球會.md" title="wikilink">巴伦西亚</a></p></td>
<td><p>6球</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/雲尼斯杜萊.md" title="wikilink">范尼斯特鲁伊</a></p></td>
<td><p><a href="../Page/皇家馬德里.md" title="wikilink">皇家馬德里</a></p></td>
<td><p>6球</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/魯爾.md" title="wikilink">劳尔</a></p></td>
<td><p><a href="../Page/皇家馬德里.md" title="wikilink">皇家馬德里</a></p></td>
<td><p>5球</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="../Page/尼古萊·迪卡.md" title="wikilink">迪卡</a></p></td>
<td><p><a href="../Page/布加勒斯特星隊.md" title="wikilink">布加勒斯特星隊</a></p></td>
<td><p>4球</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/恩沙基.md" title="wikilink">恩沙基</a></p></td>
<td><p><a href="../Page/AC米蘭.md" title="wikilink">AC米蘭</a></p></td>
<td><p>4球</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/克勞迪奧·皮薩羅.md" title="wikilink">皮萨罗</a></p></td>
<td><p><a href="../Page/拜仁慕尼黑.md" title="wikilink">拜仁慕尼黑</a></p></td>
<td><p>4球</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/朗尼.md" title="wikilink">鲁尼</a></p></td>
<td><p><a href="../Page/曼聯.md" title="wikilink">曼聯</a></p></td>
<td><p>4球</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/沙夏.md" title="wikilink">沙夏</a></p></td>
<td><p><a href="../Page/曼聯.md" title="wikilink">曼聯</a></p></td>
<td><p>4球</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/托迪.md" title="wikilink">托蒂</a></p></td>
<td><p><a href="../Page/羅馬足球會.md" title="wikilink">羅馬</a></p></td>
<td><p>4球</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大衛韋拿.md" title="wikilink">大卫比利亚</a></p></td>
<td><p><a href="../Page/華倫西亞足球會.md" title="wikilink">華倫西亞</a></p></td>
<td><p>4球</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參見

  - [2006年至2007年歐洲足協盃](../Page/2006年至2007年歐洲足協盃.md "wikilink")
  - [2006年圖圖杯](../Page/2006年圖圖杯.md "wikilink")

[Category:2006年足球](../Category/2006年足球.md "wikilink")
[Category:2007年足球](../Category/2007年足球.md "wikilink")
[Category:欧洲冠军联赛](../Category/欧洲冠军联赛.md "wikilink")