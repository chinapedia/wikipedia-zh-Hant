## [6月30日](../Page/6月30日.md "wikilink")

  - 前[香港](../Page/香港.md "wikilink")[政務司司長](../Page/政務司.md "wikilink")[陳方安生再度呼籲爭取民主普選的市民參加](../Page/陳方安生.md "wikilink")7月1日的大遊行。[中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110505+112006070100071,00.html)
  - 中国举行“庆祝[中国共产党成立](../Page/中国共产党.md "wikilink")85周年暨总结[保持共产党员先进性教育活动大会](../Page/保持共产党员先进性教育活动.md "wikilink")”，中共中央政治局9常委全部出席大会，[胡锦涛发表讲话](../Page/胡锦涛.md "wikilink")。[新浪网](http://news.sina.com.cn/c/2006-06-30/12039339412s.shtml)

## [6月29日](../Page/6月29日.md "wikilink")

  - [以色列軍扣留包括](../Page/以色列.md "wikilink")[哈馬斯內閣部長](../Page/哈馬斯.md "wikilink")、議員及軍事官員共87名人質，迫使[巴勒斯坦武裝份子交出被俘的以色列士兵](../Page/巴勒斯坦.md "wikilink")。[Yahoo\!新聞](https://archive.is/20130106070842/http://hk.news.yahoo.com/060629/12/1pgap.html)[自由電子報](https://web.archive.org/web/20060720195041/http://www.libertytimes.com.tw/2006/new/jun/30/today-int1.htm)[中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110504+112006063000684,00.html)
  - [美國聯邦最高法院以五比三的票數](../Page/美國聯邦最高法院.md "wikilink")，判決[布什為審判囚禁在](../Page/乔治·沃克·布什.md "wikilink")[關達納摩灣美軍監獄的](../Page/關達納摩灣.md "wikilink")「[-{zh-hans:基地组织;
    zh-hant:開打組織;}-](../Page/基地组织.md "wikilink")」恐怖嫌犯而成立的特別軍事法庭，超越其權限且違反[日內瓦公約](../Page/日內瓦公約.md "wikilink")。[自由電子報](https://web.archive.org/web/20070519053856/http://www.libertytimes.com.tw/2006/new/jun/30/today-int4.htm)[中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110504+112006063000689,00.html)
  - 即將在9月卸任的[日本](../Page/日本.md "wikilink")[首相](../Page/首相.md "wikilink")[小泉純一郎抵達](../Page/小泉純一郎.md "wikilink")[華府訪問](../Page/華府.md "wikilink")，[美國總統](../Page/美國.md "wikilink")[布什在](../Page/乔治·沃克·布什.md "wikilink")[白宮草坪以元首級的規格接待小泉](../Page/白宮.md "wikilink")。[TVBS新聞網](http://www.tvbs.com.tw/NEWS/NEWS_LIST.asp?no=arieslu20060629222228)[中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110504+112006062900073,00.html)
  - [科威特女性首次參與議會選舉](../Page/科威特.md "wikilink")。[亞洲時報在線](http://www.atchinese.com/index.php?option=com_content&task=view&id=18602&Itemid=38)

## [6月28日](../Page/6月28日.md "wikilink")

  - [蒙特內哥羅經](../Page/蒙特內哥羅.md "wikilink")[聯合國各成員國代表表決後](../Page/聯合國.md "wikilink")，成為聯合國第192個成員國
    [聯合國新聞中心網站](http://www.un.org/apps/news/story.asp?NewsID=19028&Cr=montenegro&Cr1=)
  - [2006年以加冲突](../Page/2006年以加冲突.md "wikilink")——[以色列军队为营救被](../Page/以色列.md "wikilink")[巴勒斯坦武装组织绑架的一名士兵而在](../Page/巴勒斯坦.md "wikilink")[-{zh-hans:加沙地带;
    zh-hant:加薩走廊;}-展开名为](../Page/加沙地带.md "wikilink")“[夏雨行动](../Page/夏雨行动.md "wikilink")”的大规模军事行动。[美國之音](http://www.voanews.com/chinese/n2006-06-28-voa8.cfm)
  - [美國眾議院表決通過一項撥款法案](../Page/美國眾議院.md "wikilink")，要求[美國國務院等部門](../Page/美國國務院.md "wikilink")，解除限制美台互動的六項禁令。[自由電子報](https://web.archive.org/web/20070929134026/http://www.libertytimes.com.tw/2006/new/jun/30/today-fo2.htm)
  - 由於水患嚴重，甚至宣布進入緊急狀態的[美國東北部數州當局要求災區居民撤離家園](../Page/美國.md "wikilink")。[自由電子報](https://web.archive.org/web/20070519043322/http://www.libertytimes.com.tw/2006/new/jun/30/today-int2.htm)

## [6月27日](../Page/6月27日.md "wikilink")

  - 在[越南第十一届国会第九次会议上](../Page/越南.md "wikilink")，[阮明哲当选为越南国家主席](../Page/阮明哲.md "wikilink")、[阮晋勇为总理](../Page/阮晋勇.md "wikilink")。[新华网](http://news.xinhuanet.com/world/2006-06/27/content_4756586.htm)
  - [中華民國總統罷免案於](../Page/2006年中華民國總統罷免案.md "wikilink")[立法院進行表決](../Page/立法院.md "wikilink")，[民進黨立委全部缺席](../Page/民進黨.md "wikilink")，共133位立法委員投票，贊成票數119票，14張廢票，未超過148票的門檻，罷免案無法進入第二階段的公民表決。[TVBS新聞網](http://www.tvbs.com.tw/NEWS/NEWS_LIST.asp?no=sharan20060627105503)

## [6月26日](../Page/6月26日.md "wikilink")

  - [美国投资家](../Page/美国.md "wikilink")[沃伦·巴菲特正式决定向](../Page/沃伦·巴菲特.md "wikilink")[比尔与梅林达·盖茨基金会捐献善款约](../Page/比尔与梅林达·盖茨基金会.md "wikilink")300亿美元。[新华网](http://news.xinhuanet.com/newscenter/2006-06/27/content_4756804.htm)
  - 在連續數週要求下台的強大壓力下，[東帝汶總理](../Page/東帝汶.md "wikilink")[阿卡提里宣布辭職](../Page/阿卡提里.md "wikilink")。[中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110104+112006062600998+20060626151304,00.html)
  - [越南國會選出](../Page/越南.md "wikilink")[河內市黨委書記](../Page/河內.md "wikilink")[阮富仲成為新的國會主席](../Page/阮富仲.md "wikilink")。[自由電子報](https://web.archive.org/web/20060701200315/http://www.libertytimes.com.tw/2006/new/jun/27/today-int2.htm)

## [6月25日](../Page/6月25日.md "wikilink")

  - 针对[陈水扁日前在](../Page/陈水扁.md "wikilink")“[向人民报告](../Page/向人民报告.md "wikilink")”中否认其[吴淑珍见过](../Page/吴淑珍.md "wikilink")[陈由豪一说](../Page/陈由豪.md "wikilink")，陈由豪在美国召开记者会，指控说陈水扁夫妇公然说谎。[台灣](../Page/台灣.md "wikilink")[行政院發言人](../Page/行政院.md "wikilink")[鄭文燦則呼籲陳由豪](../Page/鄭文燦.md "wikilink")，「海外爆料不如回台受審」。[新浪网](http://news.sina.com.cn/c/2006-06-25/114010248266.shtml)[自由電子報](https://web.archive.org/web/20060712052139/http://www.libertytimes.com.tw/2006/new/jun/26/today-fo9.htm)
  - [日本自卫队开始正式撤离](../Page/日本自卫队.md "wikilink")[伊拉克](../Page/伊拉克.md "wikilink")。[中国日报](http://www.chinadaily.com.cn/hqkx/2006-06/25/content_625317.htm)
  - [温家宝结束对](../Page/温家宝.md "wikilink")[非洲七国的访问回到北京](../Page/非洲.md "wikilink")。[新华网](http://news.xinhuanet.com/politics/2006-06/25/content_4745695.htm)
  - 世界首座圆顶透光火车站[上海火車南站试运营](../Page/上海火車南站.md "wikilink")。[新华网](http://news.xinhuanet.com/photo/2006-06/25/content_4746315.htm)

## [6月24日](../Page/6月24日.md "wikilink")

  - [世界卫生组织证实在](../Page/世界卫生组织.md "wikilink")[印度尼西亚已经出现了由人传染的](../Page/印度尼西亚.md "wikilink")[禽流感病例](../Page/禽流感.md "wikilink")，这是全球第一例由一个人传染给另一个人的禽流感病例。[网易新闻](http://news.163.com/06/0624/18/2KDC7VRB0001121M.html)
    [中时电子报](https://web.archive.org/web/20060627051716/http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110504+112006062500060,00.html)
  - [越南国会于下午以投票的方式通过国家主席](../Page/越南.md "wikilink")[陈德良](../Page/陈德良.md "wikilink")、政府总理[潘文凯和国会主席](../Page/潘文凯.md "wikilink")[阮文安递交的辞呈](../Page/阮文安.md "wikilink")，并选举新一任国家主席、政府总理和国会主席。[新浪网](http://news.sina.com.cn/w/2006-06-24/15589288286s.shtml)
  - [中國中央軍委主席](../Page/中國.md "wikilink")[胡錦濤晉升十名](../Page/胡錦濤.md "wikilink")[解放軍將領為上將](../Page/解放軍.md "wikilink")。[中時電子報](http://news.chinatimes.com/Chinatimes/Moment/newfocus-index/0,3687,9506250131+95062513+0+134550+0,00.html)[新浪网](http://news.sina.com.cn/c/2006-06-24/15549288270s.shtml)

## [6月23日](../Page/6月23日.md "wikilink")

  - [日本最高法院駁回反戰團體以及](../Page/日本.md "wikilink")[二次大戰日](../Page/二次大戰.md "wikilink")、韓陣亡家屬控告日本[首相](../Page/首相.md "wikilink")[小泉純一郎參拜](../Page/小泉純一郎.md "wikilink")[靖國神社的上訴案](../Page/靖國神社.md "wikilink")。[東森新聞報](http://www.ettoday.com/2006/06/23/334-1957680.htm)
  - [中国人民银行发布的](../Page/中国人民银行.md "wikilink")2005年国际金融市场报告称，截至2005年末，[中国持有的](../Page/中国.md "wikilink")[美国](../Page/美国.md "wikilink")[国债达到](../Page/国债.md "wikilink")3109亿美元，仅次于[日本](../Page/日本.md "wikilink")，居世界第二位。[新华网](http://news.xinhuanet.com/newscenter/2006-06/23/content_4739867.htm)

## [6月22日](../Page/6月22日.md "wikilink")

  - [加拿大总理](../Page/加拿大.md "wikilink")[史蒂芬·哈珀在国会举行仪式](../Page/史蒂芬·哈珀.md "wikilink")，就“[人头税](../Page/人头税.md "wikilink")”问题及之后实施的《[排华法案](../Page/1923年華人移民法案.md "wikilink")》向华人正式道歉，并宣布加拿大政府的赔偿方案。[加拿大广播公司](http://www.cbc.ca/canada/story/2006/06/22/chinese-apology.html)、[新华网](http://news.xinhuanet.com/newscenter/2006-06/23/content_4738738.htm)
  - [美國](../Page/美國.md "wikilink")「勇敢之盾」演習進行海、空軍種配合演習，[KC-10](../Page/KC-10.md "wikilink")[空中加油機空中定點盤旋](../Page/空中加油機.md "wikilink")，分別替21架演習戰鬥機群加油，圓滿完成任務。[自由電子報](https://web.archive.org/web/20060701180459/http://www.libertytimes.com.tw/2006/new/jun/23/today-p3.htm)
  - 一艘[美軍軍艦在海基型飛彈防禦系統的測試中](../Page/美軍.md "wikilink")，成功的在[太平洋上空擊落一枚標靶飛彈](../Page/太平洋.md "wikilink")。[自由電子報](https://web.archive.org/web/20060714204549/http://www.libertytimes.com.tw/2006/new/jun/24/today-int3.htm)
  - [美國](../Page/美國.md "wikilink")[聯邦調查局幹員突擊](../Page/聯邦調查局.md "wikilink")[佛羅里達州](../Page/佛羅里達州.md "wikilink")[邁阿密的一座倉庫](../Page/邁阿密.md "wikilink")，逮捕七名意圖策劃攻擊[芝加哥的](../Page/芝加哥.md "wikilink")[席爾斯大樓與佛州邁阿密](../Page/席爾斯大樓.md "wikilink")[FBI辦公室等建築的嫌犯](../Page/FBI.md "wikilink")。[自由電子報](https://web.archive.org/web/20060701195720/http://www.libertytimes.com.tw/2006/new/jun/24/today-int1.htm)

## [6月21日](../Page/6月21日.md "wikilink")

  - [Google出售其持有的](../Page/Google.md "wikilink")[百度股份](../Page/百度.md "wikilink")。[Reuters
    路透社](http://today.reuters.com/stocks/QuoteCompanyNewsArticle.aspx?view=CN&storyID=2006-06-22T173136Z_01_N22398559_RTRIDST_0_TECH-GOOGLE-BAIDU-URGENT.XML&rpc=66)
  - [邁阿密熱火在NBA總決賽第六回合以](../Page/邁阿密熱火.md "wikilink")95比92擊敗[達拉斯小牛首度奪得](../Page/達拉斯小牛隊.md "wikilink")[NBA總冠軍](../Page/NBA.md "wikilink")，熱火隊憑[德懷恩·韋德個人獨取](../Page/德懷恩·韋德.md "wikilink")36分而得到[NBA總決賽最有價值球員](../Page/NBA總決賽最有價值球員.md "wikilink")。[東森新聞報](http://www.ettoday.com/2006/06/21/341-1956692.htm)
  - 2006年度[邵逸夫奖揭晓](../Page/邵逸夫奖.md "wikilink")，来自[美国](../Page/美国.md "wikilink")、[澳大利亚](../Page/澳大利亚.md "wikilink")、[中国的](../Page/中国.md "wikilink")6位科学家分别获得邵逸夫奖[天文学奖](../Page/天文学.md "wikilink")、生命科学与医学奖、数学科学奖，每个奖项奖金为100万美元。[中国广播网](http://www.cnradio.com.cn/2004news/internal/200606/t20060621_504227843.html)
  - [華盛頓法院決定對](../Page/華盛頓.md "wikilink")[法輪功學員](../Page/法輪功.md "wikilink")[王文怡緩起訴一年](../Page/王文怡.md "wikilink")。[中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110504+112006062300065,00.html)
  - [美國重申不願與](../Page/美國.md "wikilink")[北韓直接會談](../Page/北韓.md "wikilink")，並呼籲[平壤當局遵守凍結飛彈試射的協議](../Page/平壤.md "wikilink")。[自由電子報](https://web.archive.org/web/20060715201626/http://www.libertytimes.com.tw/2006/new/jun/23/today-int5.htm)

## [6月20日](../Page/6月20日.md "wikilink")

  - [日本](../Page/日本.md "wikilink")[首相](../Page/首相.md "wikilink")[小泉純一郎正式宣佈撤出日本駐](../Page/小泉純一郎.md "wikilink")[伊拉克](../Page/伊拉克.md "wikilink")[穆薩納省](../Page/穆薩納省.md "wikilink")[薩馬瓦市的陸上](../Page/薩馬瓦市.md "wikilink")[自衛隊](../Page/自衛隊.md "wikilink")。[自由電子報](https://web.archive.org/web/20060714204511/http://www.libertytimes.com.tw/2006/new/jun/21/today-int2.htm)[中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110504+112006062100067,00.html)
  - [美國](../Page/美國.md "wikilink")[華盛頓時報報導](../Page/華盛頓時報.md "wikilink")，為因應[北韓試射飛彈的威脅](../Page/北韓.md "wikilink")，美國國防部已啟動美國本土的新型陸基型飛彈防禦系統，並考慮發射攔截飛彈來擊落北韓飛彈。[自由電子報](https://web.archive.org/web/20060714204444/http://www.libertytimes.com.tw/2006/new/jun/21/today-int1.htm)
  - [中华民国軍方高層表示](../Page/中华民国.md "wikilink")，[美國未曾施壓及逼迫台灣軍方放棄飛彈發展計畫](../Page/美國.md "wikilink")。[中時電子報](https://web.archive.org/web/20060623042327/http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110505+112006062100075,00.html)
  - [中华民国總統](../Page/中华民国.md "wikilink")[陳水扁透過電視直播](../Page/陳水扁.md "wikilink")「向人民報告」，就國、親十大[罷免理由一一反駁及反擊](../Page/罷免.md "wikilink")。[自由電子報](https://web.archive.org/web/20060829044027/http://www.libertytimes.com.tw/2006/new/jun/21/today-t1.htm)
  - [IBM與](../Page/IBM.md "wikilink")[-{zh-hans:佐治亚;
    zh-hant:喬治亞;}-理工學院宣佈](../Page/喬治亞.md "wikilink")，他們已打破[半導體矽](../Page/半導體.md "wikilink")[晶片的速度紀錄](../Page/晶片.md "wikilink")，研發出比今日常用晶片速度快上250倍的新晶片。[自由電子報](https://web.archive.org/web/20060714204524/http://www.libertytimes.com.tw/2006/new/jun/21/today-int4.htm)[東森新聞報](http://www.ettoday.com/2006/06/20/334-1956237.htm)

## [6月19日](../Page/6月19日.md "wikilink")

  - [霍金教授参加在](../Page/霍金.md "wikilink")[人民大会堂举行的](../Page/人民大会堂.md "wikilink")2006年[国际弦理论大会开幕式](../Page/国际弦理论大会.md "wikilink")，并作“宇宙的起源”主题讲座。[新浪网](http://tech.sina.com.cn/d/2006-06-19/2155997544.shtml)
  - [美軍開始在](../Page/美軍.md "wikilink")[關島舉行五天的](../Page/關島.md "wikilink")「勇敢之盾」軍事演習。[自由電子報](https://web.archive.org/web/20060714203029/http://www.libertytimes.com.tw/2006/new/jun/20/today-p9.htm)
  - [美國副](../Page/美國.md "wikilink")[國務卿](../Page/國務卿.md "wikilink")[佐立克宣佈辭職](../Page/佐立克.md "wikilink")，決定轉向私人公司任職。[東森新聞報](http://www.ettoday.com/2006/06/19/334-1955966.htm)
  - 針對[伊拉克前總統](../Page/伊拉克.md "wikilink")[薩達姆·海珊的世紀大審判中](../Page/薩達姆·海珊.md "wikilink")，海珊和另外七名官員被檢方求處死刑。面對檢察官的指控，海珊處之泰然，甚至還露出微笑。[東森新聞報](http://www.ettoday.com/2006/06/19/334-1955968.htm)
  - [挪威一個稱為](../Page/挪威.md "wikilink")「[斯瓦爾巴全球種子庫](../Page/斯瓦爾巴全球種子庫.md "wikilink")」的工程動工。[中時電子報](https://web.archive.org/web/20060622064138/http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110504+112006062000062,00.html)
  - [紐約時報引述](../Page/紐約時報.md "wikilink")[美國官員透露的消息](../Page/美國.md "wikilink")，指稱[北韓似已完成](../Page/北韓.md "wikilink")[大浦洞二號的燃料裝填作業](../Page/大浦洞二號.md "wikilink")，試射已如箭在弦上。[自由電子報](https://web.archive.org/web/20060714200533/http://www.libertytimes.com.tw/2006/new/jun/20/today-int6.htm)[東森新聞報](http://www.ettoday.com/2006/06/20/334-1956052.htm)
  - [美國國防新聞週刊報導](../Page/美國.md "wikilink")，[台灣正在進行的地對地飛彈發展計畫引起美國政府關切](../Page/台灣.md "wikilink")。[東森新聞報](http://www.ettoday.com/2006/06/20/91-1956239.htm)

## [6月18日](../Page/6月18日.md "wikilink")

  - 日本[產經新聞引述官員稱](../Page/產經新聞.md "wikilink")，[北韓政府下令民眾在當地時間下午二時升起國旗](../Page/北韓.md "wikilink")，以及收看國家電視台直播。[美國政府和情報當局獲得的情報認為](../Page/美國.md "wikilink")，導彈已注入燃料，相信是試射[大浦洞二型導彈](../Page/大浦洞-2.md "wikilink")。結果證明是一場烏龍。
    [中國廣播公司](http://www.bcc.com.tw/news/newsview.asp?cde=242421)
    [香港電台](https://web.archive.org/web/20070922010120/http://www.rthk.org.hk/rthk/news/expressnews/20060618/20060618_55_318636.html)
    [新城電台](http://www.metroradio.com.hk/news/news/20060618112210.htm)[中時電子報](https://web.archive.org/web/20060621075531/http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110504+112006061900069,00.html)
  - [美国](../Page/美国.md "wikilink")、[俄国](../Page/俄国.md "wikilink")、[欧盟和](../Page/欧盟.md "wikilink")[联合国决定同意欧盟短暂恢复对巴勒斯坦提供援助](../Page/联合国.md "wikilink")。[BBC
    中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_5080000/newsid_5088800/5088846.stm)

## [6月17日](../Page/6月17日.md "wikilink")

  - [亚洲相互协作与信任措施会议](../Page/亚洲相互协作与信任措施会议.md "wikilink")（亚信会议）第二次领导人会议在[阿拉木图召开](../Page/阿拉木图.md "wikilink")，17个成员国领导人到会，[韩国被接纳为新成员](../Page/韩国.md "wikilink")。[CCTV](http://news.cctv.com/world/20060617/101701.shtml)
  - [西班牙北部](../Page/西班牙.md "wikilink")[加泰隆尼亞地區舉行自治權擴張](../Page/加泰隆尼亞.md "wikilink")[公投](../Page/公投.md "wikilink")，結果民眾以壓倒性的票數支持該項決定。[自由電子報](https://web.archive.org/web/20060714204432/http://www.libertytimes.com.tw/2006/new/jun/20/today-int9.htm)

## [6月16日](../Page/6月16日.md "wikilink")

  - 世界最長雙孔公路隧道－[臺灣](../Page/台灣.md "wikilink")[雪山隧道啟用](../Page/雪山隧道.md "wikilink")。[聯合新聞網](http://udn.com/NEWS/NATIONAL/NAT5/3359139.shtml)
  - [北京法院開始審理](../Page/北京.md "wikilink")「[紐約時報](../Page/紐約時報.md "wikilink")」北京分社新聞助理「[趙岩](../Page/趙岩.md "wikilink")」。[中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,130505+132006061601061,00.html)
  - [安徽省](../Page/安徽省.md "wikilink")[马鞍山市](../Page/马鞍山市.md "wikilink")[当涂县化工厂发生爆炸](../Page/当涂县.md "wikilink")，造成14人死亡，2人失踪，24人不同程度受伤。[新华网](http://news.xinhuanet.com/newscenter/2006-06/17/content_4710728.htm)

## [6月15日](../Page/6月15日.md "wikilink")

  - [中國](../Page/中國.md "wikilink")[河南省的](../Page/河南省.md "wikilink")[鄭州大學爆發大規模學生抗議示威](../Page/鄭州大學.md "wikilink")。[自由電子報](https://web.archive.org/web/20060714204420/http://www.libertytimes.com.tw/2006/new/jun/20/today-int8.htm)
  - [中華民國前](../Page/中華民國.md "wikilink")[行政院長](../Page/行政院長.md "wikilink")、[民進黨籍的](../Page/民進黨.md "wikilink")[謝長廷下午宣布參選](../Page/謝長廷.md "wikilink")[台北市長](../Page/台北市長.md "wikilink")。[明報即時新聞](https://web.archive.org/web/20060702183134/http://www.mpinews.com/htm/INews/20060615/ca41628a.htm)
  - [美國國務院官員對](../Page/美國國務院.md "wikilink")[台海兩岸同時宣佈達成專案包機共識](../Page/台海.md "wikilink")，表示歡迎，認為這是促進兩岸互動的重要進展。[自由電子報](https://web.archive.org/web/20060714203727/http://www.libertytimes.com.tw/2006/new/jun/16/today-p14.htm)
  - [上海合作组织峰会在](../Page/上海合作组织.md "wikilink")[上海举行](../Page/上海.md "wikilink")。六个成员国元首，观察员国[蒙古](../Page/蒙古.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[伊朗总统](../Page/伊朗.md "wikilink")，[印度石油天然气部长](../Page/印度.md "wikilink")，特邀客人[阿富汗总统参加](../Page/阿富汗.md "wikilink")，[联合国秘书长](../Page/联合国.md "wikilink")[科菲·安南发电祝贺](../Page/科菲·安南.md "wikilink")。[中国新闻网](http://www.chinanews.com.cn/special/2006-06-12/609.shtml)
    [中国新闻网](http://www.chinanews.com.cn//news/2006/2006-06-16/8/745053.shtml)
    [中国新闻网](http://www.chinanews.com.cn//news/2006/2006-06-15/8/744487.shtml)
  - [日銀總裁](../Page/日銀.md "wikilink")[福井俊彥公開向](../Page/福井俊彥.md "wikilink")[日本大眾道歉](../Page/日本.md "wikilink")，但未談及「下台」的問題。[自由電子報](https://web.archive.org/web/20060714204408/http://www.libertytimes.com.tw/2006/new/jun/16/today-int2.htm)
  - [比爾·蓋茲宣佈將在未來兩年內淡出公司的日常營運事務](../Page/比爾·蓋茲.md "wikilink")，全心投入慈善事業，並立即辭去[微軟首席軟體設計師](../Page/微軟.md "wikilink")。[東森新聞報](http://www.ettoday.com/2006/06/16/334-1954704.htm)

## [6月14日](../Page/6月14日.md "wikilink")

  - [美國](../Page/美國.md "wikilink")「國家審計總署」公布調查報告，許多假稱是[-{zh-hans:卡特里娜飓风;
    zh-hant:卡崔娜颶風;}-災民者所犯下的詐欺項目](../Page/卡崔娜颶風.md "wikilink")，讓美國[聯邦政府平白付出了多達十四億美元的救災款](../Page/聯邦政府.md "wikilink")。[中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110504+112006061600058,00.html)
  - [台湾](../Page/台湾.md "wikilink")[中央研究院因為研究](../Page/中央研究院.md "wikilink")[SARS而發現一種新型](../Page/SARS.md "wikilink")[肺部](../Page/肺部.md "wikilink")[幹細胞](../Page/幹細胞.md "wikilink")，可能可使肺病患者肺部重現生機。[中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110503+112006061400046,00.html)
  - [高雄市](../Page/高雄市.md "wikilink")[民進黨籍](../Page/民進黨.md "wikilink")[立委](../Page/立委.md "wikilink")[黃昭輝發起對同屬南區](../Page/黃昭輝.md "wikilink")[國民黨籍立委](../Page/國民黨.md "wikilink")[邱毅的](../Page/邱毅.md "wikilink")[罷免連署](../Page/罷免.md "wikilink")。[自由電子報](https://web.archive.org/web/20070702090850/http://www.libertytimes.com.tw/2006/new/jun/15/today-fo14.htm)[TVBS新聞網](http://www.tvbs.com.tw/NEWS/NEWS_LIST.asp?no=lili20060614120551)
  - 涉及2002年10月[印尼](../Page/印尼.md "wikilink")[-{zh-hans:巴厘岛;
    zh-hant:峇里島;}-爆炸案而入獄的激進派](../Page/峇里島.md "wikilink")[伊斯兰教教士](../Page/伊斯兰教.md "wikilink")[巴錫爾獲釋出獄](../Page/巴錫爾.md "wikilink")，[美國及](../Page/美國.md "wikilink")[澳大利亚均對此表示失望](../Page/澳大利亚.md "wikilink")。[自由電子報](https://web.archive.org/web/20070702015232/http://www.libertytimes.com.tw/2006/new/jun/15/today-int2.htm)

## [6月13日](../Page/6月13日.md "wikilink")

  - [布什突访](../Page/乔治·沃克·布什.md "wikilink")[巴格达](../Page/巴格达.md "wikilink")，这是他在[伊拉克战争以来再次访问伊拉克](../Page/伊拉克战争.md "wikilink")。[BBC
    中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_5070000/newsid_5076100/5076160.stm)[中時電子報](https://web.archive.org/web/20060616082144/http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110504+112006061400064,00.html)
  - [章启月之后中国外交部新任女发言人](../Page/章启月.md "wikilink")[姜瑜亮相](../Page/姜瑜.md "wikilink")。
    [中国新闻网](http://www.chinanews.com.cn/news/2006/2006-06-13/8/743319.shtml)
  - [AskMen.com在网上选出](../Page/AskMen.com.md "wikilink")“九十九个最令人向往的女性”，[章子怡排名](../Page/章子怡.md "wikilink")79
    。[网易国际新闻](http://news.163.com/06/0613/09/2JG4JJBM0001121M.html)
  - [日經指數狂跌](../Page/日經指數.md "wikilink")614點，創下今年單日最大跌幅，也創2001年[911攻擊以來最深紀錄](../Page/911攻擊.md "wikilink")，[日本央行總裁](../Page/日本.md "wikilink")[福井俊彥證實持有內線交易嫌犯](../Page/福井俊彥.md "wikilink")[村上世彰的私募基金](../Page/村上世彰.md "wikilink")，更為日股帶來致命一擊。
    [東森新聞報](http://www.ettoday.com/2006/06/13/334-1953358.htm)[中時電子報](https://web.archive.org/web/20060616083148/http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110507+112006061400245,00.html)
  - [日本](../Page/日本.md "wikilink")[東京都發生電車相撞事故](../Page/東京都.md "wikilink")，27人受傷。[香港雅虎新聞](https://archive.is/20130105230112/http://hk.news.yahoo.com/060613/12/1oq21.html)

## [6月12日](../Page/6月12日.md "wikilink")

  - [-{zh-hans:基地组织;
    zh-hant:開打組織;}-宣布](../Page/開打組織.md "wikilink")[-{zh-hans:扎卡维;
    zh-hant:札格維;}-的繼任人選](../Page/札格維.md "wikilink")，化名[穆哈傑](../Page/穆哈傑.md "wikilink")，具備[聖戰經驗與豐富知識](../Page/聖戰.md "wikilink")。[自由電子報](https://web.archive.org/web/20060714204356/http://www.libertytimes.com.tw/2006/new/jun/14/today-int1.htm)
  - 美国科学家在[南卡罗来纳州海岸附近发现了](../Page/南卡罗来纳州.md "wikilink")[双髻鲨家族的新种](../Page/双髻鲨.md "wikilink")，目前还没有正式的[学名](../Page/学名.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2006-06/13/content_4689756.htm)
  - [泰国国王](../Page/泰国.md "wikilink")[普密蓬·阿杜德登基](../Page/普密蓬·阿杜德.md "wikilink")60周年，多国祝贺。[新闻晚报](http://news.xinhuanet.com/world/2006-06/13/content_4689267.htm)
  - [英國](../Page/英國.md "wikilink")[物理學家](../Page/物理學家.md "wikilink")[霍金抵](../Page/霍金.md "wikilink")[港](../Page/香港.md "wikilink")。
    [雅虎新聞／明報](https://web.archive.org/web/20060615105627/http://hk.news.yahoo.com/060612/12/1oore.html)
  - [中華民國](../Page/中華民國.md "wikilink")[立法院決定召開臨時會](../Page/立法院.md "wikilink")，在野黨提出的[罷免總統案闖關成功](../Page/罷免.md "wikilink")，將在臨時會上處理此議案。[中時電子報](https://web.archive.org/web/20060613200906/http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,130501+132006061200728,00.html)
  - [欧盟同意启动](../Page/欧盟.md "wikilink")[土耳其入盟谈判](../Page/土耳其.md "wikilink")。[BBC
    中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_5070000/newsid_5072000/5072018.stm)

## [6月11日](../Page/6月11日.md "wikilink")

  - [中國透過](../Page/中國.md "wikilink")[新華社宣布](../Page/新華社.md "wikilink")，[北京市](../Page/北京市.md "wikilink")[人大常委會免除北京市副市長](../Page/人大常委會.md "wikilink")[劉志華的職務](../Page/劉志華.md "wikilink")。[中時電子報](https://web.archive.org/web/20060614025740/http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110505+112006061200063,00.html)[中時電子報](https://web.archive.org/web/20060613202137/http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,130505+132006061200818,00.html)

## [6月10日](../Page/6月10日.md "wikilink")

  - [古巴](../Page/古巴.md "wikilink")[-{zh-hans:关塔那摩;
    zh-hant:關達納摩灣;}-美軍監獄](../Page/關達納摩灣.md "wikilink")，發生三名囚犯以床單打成繩結上吊身亡事件。[自由電子報](https://web.archive.org/web/20060615021259/http://www.libertytimes.com.tw/2006/new/jun/12/today-int1.htm)

## [6月9日](../Page/6月9日.md "wikilink")

  - 第十八届[世界杯足球赛开幕式在](../Page/2006年世界盃足球賽.md "wikilink")[德国](../Page/德国.md "wikilink")[慕尼黑的世界杯体育场举行](../Page/慕尼黑.md "wikilink")。[新浪网](http://2006.sina.com.cn/others/2006-06-09/230322563.shtml)
  - [日本外相](../Page/日本.md "wikilink")[麻生太郎在](../Page/麻生太郎.md "wikilink")[東京的一項演講公開表明將角逐黨總裁兼](../Page/東京.md "wikilink")[首相選舉](../Page/首相.md "wikilink")。[大紀元電子報](http://www.epochtimes.com/b5/6/6/9/n1345143.htm)
  - [日本內閣會議決定對國會提出將](../Page/日本.md "wikilink")「[防衛廳](../Page/防衛廳.md "wikilink")」升格為「[防衛省](../Page/防衛省.md "wikilink")」的相關法案。[自由電子報](https://web.archive.org/web/20060614081509/http://www.libertytimes.com.tw/2006/new/jun/10/today-int4.htm)
  - [美國在台協會主席](../Page/美國在台協會.md "wikilink")[薄瑞光表示](../Page/薄瑞光.md "wikilink")，[美國對](../Page/美國.md "wikilink")[台灣總統](../Page/台灣.md "wikilink")[陳水扁](../Page/陳水扁.md "wikilink")2月間有關[國家統一委員會和](../Page/國家統一委員會.md "wikilink")[國家統一綱領的聲明感到滿意](../Page/國家統一綱領.md "wikilink")，因此「終統」一事已經「結束」。[東森新聞報](http://www.ettoday.com/2006/06/09/162-1952071.htm)[自由電子報](https://web.archive.org/web/20060614081123/http://www.libertytimes.com.tw/2006/new/jun/10/today-fo2.htm)
  - [美國國務院證實](../Page/美國國務院.md "wikilink")，長期被[緬甸軍事執政當局軟禁的緬甸反對陣營領袖](../Page/緬甸.md "wikilink")[翁山蘇姬](../Page/翁山蘇姬.md "wikilink")，已送醫住院。[中時電子報](https://web.archive.org/web/20060614025609/http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110504+112006061000075,00.html)

## [6月8日](../Page/6月8日.md "wikilink")

  - [中國科學院第](../Page/中國科學院.md "wikilink")13次院士大會和[中國工程院第](../Page/中國工程院.md "wikilink")8次院士大會完成大會各項議程後結束，於會中表決通過中國科學院院士章程修改稿，修改[院士和外籍院士標準與選舉](../Page/院士.md "wikilink")-{程序}-，並成立產生了第一個[科普和出版工作委員會](../Page/中國科學院學部科普和出版工作委員會.md "wikilink")。[新華網](http://news.xinhuanet.com/politics/2006-06/09/content_4667292.htm)、[中国教育和科研计算机网](http://www.edu.cn/20060608/3194143.shtml)
  - 全球第一個[子宮頸癌](../Page/子宮頸癌.md "wikilink")[疫苗](../Page/疫苗.md "wikilink")「[嘉喜](../Page/嘉喜.md "wikilink")」（Gardasil）取得[美國食品藥物管理局](../Page/美國食品藥物管理局.md "wikilink")（[FDA](../Page/FDA.md "wikilink")）的核准。[自由電子報](https://web.archive.org/web/20060614081525/http://www.libertytimes.com.tw/2006/new/jun/10/today-int6.htm)
  - [美國網路](../Page/美國.md "wikilink")[搜尋引擎](../Page/搜尋引擎.md "wikilink")[google表示](../Page/google.md "wikilink")，他們正在調查有關[中國網友無法進入google](../Page/中國.md "wikilink").com網站的報導。[中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,130509+132006060900982,00.html)[中國經濟網](https://web.archive.org/web/20060615130010/http://big5.ce.cn/cysc/ceit/ityj/200606/09/t20060609_7272511.shtml)[中時電子報](https://web.archive.org/web/20060614025701/http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110505+112006060900072,00.html)
  - [美國國務院發表聲明](../Page/美國國務院.md "wikilink")，對於[台灣總統](../Page/台灣.md "wikilink")[陳水扁在兩岸議題上](../Page/陳水扁.md "wikilink")，再一次對美保證「[四不](../Page/四不.md "wikilink")」原則在任期內不會改變，感到欣慰。[TVBS新聞網](http://www.tvbs.com.tw/NEWS/NEWS_LIST.asp?no=lili20060609083357)[大公網](https://web.archive.org/web/20060614113538/http://www.takungpao.com/news/06/06/09/TM-577348.htm)[TVBS新聞網](http://www.tvbs.com.tw/news/news_list.asp?no=lili20060609124402)[自由電子報](https://web.archive.org/web/20060614081107/http://www.libertytimes.com.tw/2006/new/jun/10/today-fo1.htm)
  - [中華人民共和國文化部宣佈揭曉](../Page/中華人民共和國文化部.md "wikilink")“中國[非物質文化遺產](../Page/非物質文化遺產.md "wikilink")”標識。[中華人民共和國文化部網站](https://web.archive.org/web/20060712230633/http://www.ccnt.gov.cn/xwzx/whbzhxw/t20060609_27357.htm)

## [6月7日](../Page/6月7日.md "wikilink")

  - [美國在台協會主席](../Page/美國在台協會.md "wikilink")[薄瑞光晚間八點鐘飛抵](../Page/薄瑞光.md "wikilink")[台灣](../Page/台灣.md "wikilink")。[中時電子報](https://web.archive.org/web/20060614025454/http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110101+112006060701337,00.html)[東森新聞報](http://www.ettoday.com/2006/06/07/301-1951083.htm)
  - 由[瑞士參議員](../Page/瑞士.md "wikilink")[馬提率領的調查行動](../Page/馬提.md "wikilink")，完成調查後向[歐洲議會提出報告](../Page/歐洲議會.md "wikilink")。報告內容說，[歐洲有](../Page/歐洲.md "wikilink")14個國家協助[CIA運送恐怖嫌犯到黑牢審訊](../Page/CIA.md "wikilink")，並有證據顯示，[羅馬尼亞和](../Page/羅馬尼亞.md "wikilink")[波蘭是可能的CIA黑牢地點](../Page/波蘭.md "wikilink")。[東森新聞報](http://www.ettoday.com/2006/06/07/334-1951021.htm)
  - [美军对](../Page/美军.md "wikilink")[伊拉克](../Page/伊拉克.md "wikilink")[巴格达附近的一处房屋实施](../Page/巴格达.md "wikilink")[空袭](../Page/空袭.md "wikilink")，伊拉克[基地组织的首领](../Page/基地组织.md "wikilink")[扎卡维被击毙](../Page/扎卡维.md "wikilink")。[中国新闻网](http://auto.chinanews.com.cn/news/2006/2006-06-08/8/741159.shtml)[环球外汇网](http://www.cnforex.com/news/%CD%E2%BB%E3%D0%C2%CE%C5/SS,2006060815,00021448.html)[新华网](http://news.xinhuanet.com/world/2006-06/08/content_4664835.htm)
  - [中國官方對官方傳媒下達一份內部通知指出](../Page/中國.md "wikilink")，《[-{zh-hans:达芬奇密码;
    zh-hant:達文西密碼;}-](../Page/達文西密碼.md "wikilink")》將於9日全面禁演。[中時電子報](https://web.archive.org/web/20060614025720/http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110505+112006061000078,00.html)

## [6月6日](../Page/6月6日.md "wikilink")

  - [三峡工程三期围堰成功实施爆破](../Page/三峡工程.md "wikilink")。[新华网](http://news.xinhuanet.com/politics/2006-06/06/content_4654399.htm)
  - [中共中央](../Page/中共中央.md "wikilink")、[國務院](../Page/中華人民共和國國務院.md "wikilink")、[中央軍委發出慰問電](../Page/中央軍委.md "wikilink")，慰問參加撲滅[內蒙古](../Page/內蒙古.md "wikilink")、[黑龍江](../Page/黑龍江.md "wikilink")[大興安嶺林區森林火災人員](../Page/大興安嶺.md "wikilink")。[人民網](http://politics.people.com.cn/GB/1026/4442307.html)
  - [美國在台協會](../Page/美國在台協會.md "wikilink")[台北辦事處代理發言人](../Page/台北.md "wikilink")[閔大衛表示](../Page/閔大衛.md "wikilink")，新任AIT主席[薄瑞光將首度來台](../Page/薄瑞光.md "wikilink")，進行例行訪問，在台期間「打算廣泛和各界人士交換意見」。[東森新聞報](http://www.ettoday.com/2006/06/06/301-1950402.htm)[自由電子報](https://web.archive.org/web/20060614081042/http://www.libertytimes.com.tw/2006/new/jun/6/today-fo6.htm)[中時電子報](https://web.archive.org/web/20060614025838/http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,130502+132006060601005,00.html)
  - [加拿大檢方公布日前於](../Page/加拿大.md "wikilink")[多倫多地區所破獲的一個恐怖組織之起訴書](../Page/多倫多.md "wikilink")，這批[穆斯林嫌犯策劃炸彈襲擊](../Page/穆斯林.md "wikilink")[渥太華國會山莊](../Page/渥太華.md "wikilink")、劫持國會議員及將總理[哈伯斬首](../Page/哈伯.md "wikilink")。[自由電子報](https://web.archive.org/web/20060715182748/http://www.libertytimes.com.tw/2006/new/jun/8/today-int1.htm)
  - [澳門一家新的](../Page/澳門.md "wikilink")[中文](../Page/中文.md "wikilink")[報紙](../Page/報紙.md "wikilink")[澳門商報宣佈正式創刊](../Page/澳門商報.md "wikilink")。[香港文匯報](https://web.archive.org/web/20140907034553/http://www.wenweipo.com/news.phtml?news_id=IN0606060099&cat=000IN)、[新華網](http://news.xinhuanet.com/tai_gang_ao/2006-06/07/content_4656321.htm)

## [6月5日](../Page/6月5日.md "wikilink")

  - 一个激进穆斯林民兵组织[伊斯兰法庭联盟宣称](../Page/伊斯兰法庭联盟.md "wikilink")，经过数周战争之后，他们已经击败由[美国支持的军阀](../Page/美国.md "wikilink")，控制了[索马里首都](../Page/索马里.md "wikilink")[摩加迪沙](../Page/摩加迪沙.md "wikilink")。[扬子晚报](https://web.archive.org/web/20060614150740/http://www.yangtse.com/pub/yzweb/dzbpd/gjxw/t20060606_92757.htm)
  - 第59届[世界报业大会和第](../Page/世界报业大会.md "wikilink")13届[世界编辑论坛在](../Page/世界编辑论坛.md "wikilink")[莫斯科举行](../Page/莫斯科.md "wikilink")。[第一财经日报](http://news.xinhuanet.com/newmedia/2006-06/06/content_4654093.htm)
  - 包括一直患病的[中國國務院副總理](../Page/中國國務院.md "wikilink")[黃菊在内的](../Page/黃菊.md "wikilink")[中共中央九位政治局常委全部出席](../Page/中共中央.md "wikilink")[中國科學院第](../Page/中國科學院.md "wikilink")13次院士大會、[中國工程院第](../Page/中國工程院.md "wikilink")8次院士大會的開幕式。[中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110505+112006060600085,00.html)
  - [塞爾維亞議會特別會議宣布](../Page/塞爾維亞.md "wikilink")，塞爾維亞共和國成為主权国家，並是原[塞爾維亞與黑山聯邦的合法繼承人](../Page/塞爾維亞與黑山.md "wikilink")。[有線電視新聞網](http://edition.cnn.com/2006/WORLD/europe/06/05/serbia.independence.ap/index.html)

## [6月4日](../Page/6月4日.md "wikilink")

  - [香港](../Page/香港.md "wikilink")[維多利亞公園有紀念八九年](../Page/維多利亞公園.md "wikilink")[六四事件集會](../Page/六四事件.md "wikilink")。舉辦者估計有四萬四千人參加。[雅虎新聞／明報](https://web.archive.org/web/20060615105308/http://hk.news.yahoo.com/060604/12/1ofth.html)
  - [秘魯前總統](../Page/秘魯.md "wikilink")[賈西亞擊敗](../Page/賈西亞.md "wikilink")[委內瑞拉總統](../Page/委內瑞拉.md "wikilink")[查韦斯所力挺的對手](../Page/查韦斯.md "wikilink")[烏馬拉](../Page/烏馬拉.md "wikilink")，贏得秘魯總統大選。[自由電子報](https://web.archive.org/web/20060614081344/http://www.libertytimes.com.tw/2006/new/jun/6/today-int4.htm)

## [6月3日](../Page/6月3日.md "wikilink")

  - [南韓最高法院對前總統](../Page/南韓.md "wikilink")[金泳三的次子](../Page/金泳三.md "wikilink")[金賢哲判處有期徒刑一年](../Page/金賢哲.md "wikilink")，緩期兩年執行，並追罰五億[韓元](../Page/韓元.md "wikilink")。[中時電子報](https://web.archive.org/web/20060614025558/http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110504+112006060400059,00.html)
  - 数学家[朱熹平和](../Page/朱熹平.md "wikilink")[曹怀东给出了著名数学难题](../Page/曹怀东.md "wikilink")[庞加莱猜想的完整证明](../Page/庞加莱猜想.md "wikilink")。[BBC
    中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_5040000/newsid_5045100/5045120.stm)[新华网](http://news.xinhuanet.com/newscenter/2006-06/03/content_4641469.htm)
  - 在国会的特别会议裡，[-{zh-hans:黑山共和国;zh-hk:黑山共和國;zh-tw:蒙特內哥羅共和國;}-正式宣布独立](../Page/黑山共和国.md "wikilink")。[（AP）](http://www.chron.com/disp/story.mpl/ap/world/3924784.html)[中時電子報](https://web.archive.org/web/20060614025507/http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110104+112006060400327,00.html)
  - [中國](../Page/中國.md "wikilink")[空軍一架軍用運輸機在](../Page/空軍.md "wikilink")[安徽某地失事](../Page/安徽.md "wikilink")，機上40人全部遇難。[人民網](http://military.people.com.cn/GB/4432336.html)[新華網](http://news.xinhuanet.com/newscenter/2006-06/04/content_4645035.htm)[中時電子報](https://web.archive.org/web/20060614025438/http://news.chinatimes.com/Chinatimes/Moment/newfocus-index/0,3687,9506050491+0+0+213519+0,00.html)[中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110505+112006060600083,00.html)

## [6月2日](../Page/6月2日.md "wikilink")

  - [東帝汶動亂持續延燒](../Page/東帝汶.md "wikilink")，近千名民眾洗劫政府倉庫，[澳洲士兵趕到現場卻無法制止暴民掠奪](../Page/澳洲.md "wikilink")。[東森新聞報](http://www.ettoday.com/2006/06/02/334-1949028.htm)
  - [Ubuntu](../Page/Ubuntu.md "wikilink") 6.06 LTS
    版发布。[维基新闻](../Page/n:Ubuntu_6.06_LTS_版发布.md "wikilink")
  - [美國國務院發言人](../Page/美國國務院.md "wikilink")[麥考馬克表示](../Page/麥考馬克.md "wikilink")，美方高度關切[新疆首富](../Page/新疆.md "wikilink")、女流亡異議人士[熱比婭的子女遭](../Page/熱比婭.md "wikilink")[中國警方囚禁並毆打一事](../Page/中國.md "wikilink")。[中時電子報](https://web.archive.org/web/20060614025633/http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110505+112006060300085,00.html)[自由電子報](https://web.archive.org/web/20060614081315/http://www.libertytimes.com.tw/2006/new/jun/3/today-int2.htm)
  - 針對[台灣總統](../Page/台灣.md "wikilink")[陳水扁宣布下放權力一事](../Page/陳水扁.md "wikilink")，引起[美國政府的高度關注](../Page/美國.md "wikilink")，[美國國務院首度對此發表公開聲明](../Page/美國國務院.md "wikilink")，美方最關切的就是不希望陳水扁政府的政治危機影響到[台海的穩定](../Page/台海.md "wikilink")。[東森新聞報](http://www.ettoday.com/2006/06/03/334-1949246.htm)[自由電子報](https://web.archive.org/web/20060614081025/http://www.libertytimes.com.tw/2006/new/jun/3/today-fo6.htm)[中時電子報](https://web.archive.org/web/20060614025549/http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110501+112006060300027,00.html)
  - [中國](../Page/中華人民共和國.md "wikilink")[國務院公布第六批全國重點文物保護單位](../Page/中華人民共和國國務院.md "wikilink")（共計1080處），以及與現有全國重點文物保護單位合併的項目共計106處。

## [6月1日](../Page/6月1日.md "wikilink")

  - 在驻[伊拉克美军深陷](../Page/伊拉克.md "wikilink")“[哈迪塞村事件](../Page/哈迪塞村事件.md "wikilink")”丑闻之际，[英国广播公司又曝光了一起美军在伊首都](../Page/英国广播公司.md "wikilink")[巴格达以北的](../Page/巴格达.md "wikilink")[伊斯哈吉滥杀无辜的事件](../Page/伊斯哈吉.md "wikilink")。伊拉克警察指责美军于今年3月在那里围捕并蓄意枪杀了11名平民。[新华网](http://news.xinhuanet.com/world/2006-06/03/content_4639712.htm)
  - [南韓五三一地方選舉結果](../Page/南韓.md "wikilink")，執政黨「[開放的我們黨](../Page/開放的我們黨.md "wikilink")」僅在[全羅道獲勝](../Page/全羅道.md "wikilink")，堪稱韓國選舉史上前所未見的慘敗，黨魁[鄭東泳為敗選下台負責](../Page/鄭東泳.md "wikilink")。[自由電子報](https://web.archive.org/web/20060614081302/http://www.libertytimes.com.tw/2006/new/jun/2/today-int5.htm)[自由電子報](https://web.archive.org/web/20060614081249/http://www.libertytimes.com.tw/2006/new/jun/2/today-int4.htm)
  - [土耳其驻华大使](../Page/土耳其.md "wikilink")[厄聚耶在](../Page/厄聚耶.md "wikilink")[北京签署了](../Page/北京.md "wikilink")《[亚太空间合作组织公约](../Page/亚太空间合作组织公约.md "wikilink")》。至此，总部计划设在[北京的](../Page/北京.md "wikilink")[亚太空间合作组织已经有](../Page/亚太空间合作组织.md "wikilink")9个国家签署加入。[新华网](http://news.xinhuanet.com/newscenter/2006-06/01/content_4632990.htm)
  - [伊朗核问题六国外长会议在](../Page/伊朗核问题.md "wikilink")[维也纳举行](../Page/维也纳.md "wikilink")。[中山日报](http://www.zsnews.cn/News/showcontent.asp?id=568486)
  - [廈門](../Page/廈門.md "wikilink")[遠華集團走私案涉嫌主犯](../Page/遠華走私案.md "wikilink")[賴昌星](../Page/賴昌星.md "wikilink")，在[加拿大成功申請暫緩遣返](../Page/加拿大.md "wikilink")，並呼籲[中國政府重審該案](../Page/中國.md "wikilink")。[明報即時新聞](https://web.archive.org/web/20060614105626/http://www.mpinews.com/htm/INews/20060602/ca50706w.htm)[自由電子報](https://web.archive.org/web/20060614081334/http://www.libertytimes.com.tw/2006/new/jun/3/today-int3.htm)
  - [中国银行在](../Page/中国银行.md "wikilink")[香港首日上市](../Page/香港.md "wikilink")，股价上升逾一成。[维基新闻](../Page/n:中國銀行今日起香港公開招股.md "wikilink")[BBC
    中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_5030000/newsid_5035800/5035882.stm)