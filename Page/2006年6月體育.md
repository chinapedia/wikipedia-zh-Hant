__NOTOC__ **參見**: [2006年體育](../Page/2006年體育.md "wikilink")

## [2006年](../Page/2006年.md "wikilink")[6月30日](../Page/6月30日.md "wikilink")（周五）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - 8強淘汰賽： 3–0
      - 8強淘汰賽： 1–1 ，德國在十二碼階段以四比二擊敗阿根廷出線四強

## [2006年](../Page/2006年.md "wikilink")[6月27日](../Page/6月27日.md "wikilink")（周二）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - 16強淘汰賽： 3–0
      - 16強淘汰賽： 1–3

## [2006年](../Page/2006年.md "wikilink")[6月26日](../Page/6月26日.md "wikilink")（周一）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - 16強淘汰賽： 1–0
      - 16強淘汰賽： (0)0–0(3)

## [2006年](../Page/2006年.md "wikilink")[6月25日](../Page/6月25日.md "wikilink")（周日）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - 16強淘汰賽： 1–0
      - 16強淘汰賽： 1–0

## [2006年](../Page/2006年.md "wikilink")[6月24日](../Page/6月24日.md "wikilink")（周六）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - 16強淘汰賽： 2–0
      - 16強淘汰賽： 2–1

## [2006年](../Page/2006年.md "wikilink")[6月23日](../Page/6月23日.md "wikilink")（周五）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - G組： 0–2
      - G組： 2–0
      - H組： 0–1
      - H組： 1–0

## [2006年](../Page/2006年.md "wikilink")[6月22日](../Page/6月22日.md "wikilink")（周四）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - E組： 0–2
      - E組： 2–1
      - F組： 1–4
      - F組： 2–2

## [2006年](../Page/2006年.md "wikilink")[6月21日](../Page/6月21日.md "wikilink")（周三）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - C組： 0–0
      - C組： 3–2
      - D組： 2–1
      - D組： 1–1
  - [籃球](../Page/籃球.md "wikilink")：[NBA總決賽](../Page/NBA總決賽.md "wikilink")：
      - [邁阿密熱火在NBA總決賽第六回合以](../Page/邁阿密熱火隊.md "wikilink")95比92擊敗[達拉斯小牛首度奪得](../Page/達拉斯小牛隊.md "wikilink")[NBA總冠軍](../Page/NBA.md "wikilink")，熱火隊憑[德懷恩·韋德個人獨取](../Page/德懷恩·韋德.md "wikilink")36分而成為[NBA總決賽最有價值球員](../Page/NBA總決賽最有價值球員.md "wikilink")。

## [2006年](../Page/2006年.md "wikilink")[6月20日](../Page/6月20日.md "wikilink")（周二）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - A組： 0–3
      - A組： 1–2
      - B組： 2–2
      - B組： 2–0

## [2006年](../Page/2006年.md "wikilink")[6月19日](../Page/6月19日.md "wikilink")（周一）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - G組： 0 – 2
      - H組： 0–4
      - H組： 3–1

## [2006年](../Page/2006年.md "wikilink")[6月18日](../Page/6月18日.md "wikilink")（周日）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - F組： 0 – 0
      - F組： 2 – 0
      - G組： 1 – 1

## [2006年](../Page/2006年.md "wikilink")[6月17日](../Page/6月17日.md "wikilink")（周六）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - D組： 2 – 0
      - E組： 0 – 2
      - E組： 1 – 1

## [2006年](../Page/2006年.md "wikilink")[6月16日](../Page/6月16日.md "wikilink")（周五）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - C組： 6 – 0
      - C組： 2 – 1
      - D組： 0 – 0

## [2006年](../Page/2006年.md "wikilink")[6月15日](../Page/6月15日.md "wikilink")（周四）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - A組： 3 – 0
      - B組： 2 – 0
      - B組： 1 – 0

## [2006年](../Page/2006年.md "wikilink")[6月14日](../Page/6月14日.md "wikilink")（周三）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - H組： 4 – 0
      - H組： 2 – 2
      - A組： 1 – 0

## [2006年](../Page/2006年.md "wikilink")[6月13日](../Page/6月13日.md "wikilink")（周二）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - F組： 1 – 0
      - G組： 2 – 1
      - G組： 0 – 0

## [2006年](../Page/2006年.md "wikilink")[6月12日](../Page/6月12日.md "wikilink")（周一）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - E組： 0 – 3
      - E組： 2 – 0
      - F組： 3 – 1

## [2006年](../Page/2006年.md "wikilink")[6月11日](../Page/6月11日.md "wikilink")（周日）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - C組： 0 – 1
      - D組： 3 – 1
      - D組： 0 – 1

## [2006年](../Page/2006年.md "wikilink")[6月10日](../Page/6月10日.md "wikilink")（周六）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - B組： 1 – 0
      - B組： 0 – 0
      - C組： 2 – 1

## [2006年](../Page/2006年.md "wikilink")[6月9日](../Page/6月9日.md "wikilink")（周五）

  - [足球](../Page/足球.md "wikilink")：[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")：
      - A組： 4 – 2
      - A組： 2 – 0

[Category:2006年体育](../Category/2006年体育.md "wikilink")