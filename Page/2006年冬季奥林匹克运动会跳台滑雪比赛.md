**[跳台滑雪](../Page/跳台滑雪.md "wikilink")**於[都靈冬季奧運中只有](../Page/2006年冬季奧林匹克運動會.md "wikilink")3個小項，分別為男子個人90米、男子個人120米以及團體賽120米。跳台滑雪的賽事由2月11日正式開始，至2月20日完成所有比賽。

## 男子

### 男子個人90米

<table>
<tbody>
<tr class="odd">
<td><p><strong>獎牌</strong></p></td>
<td><p><strong>運動員</strong></p></td>
<td><p><strong>得分</strong></p></td>
</tr>
<tr class="even">
<td><p>金牌</p></td>
<td><p>贝斯托尔</p></td>
<td><p>266.5分</p></td>
</tr>
<tr class="odd">
<td><p>銀牌</p></td>
<td><p>霍塔迈基</p></td>
<td><p>265.5分</p></td>
</tr>
<tr class="even">
<td><p>銅牌</p></td>
<td><p>里约克索伊获</p></td>
<td><p>264.5分</p></td>
</tr>
</tbody>
</table>

### 男子個人120米

<table>
<tbody>
<tr class="odd">
<td><p><strong>獎牌</strong></p></td>
<td><p><strong>運動員</strong></p></td>
<td><p><strong>得分</strong></p></td>
</tr>
<tr class="even">
<td><p>金牌</p></td>
<td><p>摩根斯坦</p></td>
<td><p>276.9分</p></td>
</tr>
<tr class="odd">
<td><p>銀牌</p></td>
<td><p>考夫勒</p></td>
<td><p>276.8分</p></td>
</tr>
<tr class="even">
<td><p>銅牌</p></td>
<td><p>比斯托尔</p></td>
<td><p>250.7分</p></td>
</tr>
</tbody>
</table>

## 團體

### 男子團體賽

在2月20日結束的男子團體賽中，奧地利成功奪金。男子團體賽為跳台滑雪最後的一個小項。

<table>
<tbody>
<tr class="odd">
<td><p><strong>獎牌</strong></p></td>
<td><p><strong>隊伍（運動員）</strong></p></td>
<td><p><strong>得分</strong></p></td>
</tr>
<tr class="even">
<td><p>金牌</p></td>
<td></td>
<td><p>984.0分</p></td>
</tr>
<tr class="odd">
<td><p>銀牌</p></td>
<td></td>
<td><p>976.6分</p></td>
</tr>
<tr class="even">
<td><p>銅牌</p></td>
<td></td>
<td><p>950.1分</p></td>
</tr>
</tbody>
</table>

## 獎牌榜

<table>
<thead>
<tr class="header">
<th><p>名次</p></th>
<th><p>國家或地區</p></th>
<th><p>金牌</p></th>
<th><p>銀牌</p></th>
<th><p>銅牌</p></th>
<th><p>總數</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

[Category:2006年冬季奧林匹克運動會比賽項目](../Category/2006年冬季奧林匹克運動會比賽項目.md "wikilink")
[Category:奥林匹克运动会跳台滑雪比赛](../Category/奥林匹克运动会跳台滑雪比赛.md "wikilink")