**2006年泛美棒球錦標賽**（**Pan-American Baseball Championship
2006**）是指2006年8月25日至9月7日在[古巴](../Page/古巴.md "wikilink")[哈瓦那所舉行的](../Page/哈瓦那.md "wikilink")[泛美棒球錦標賽](../Page/泛美棒球錦標賽.md "wikilink")（Pan-American
Baseball
Championship），也是[2008年夏季奧林匹克運動會棒球比賽的美洲區資格賽](../Page/2008年夏季奧林匹克運動會棒球比賽.md "wikilink")（**Americas
Olympic
Qualifier**）。本屆賽事前兩名可獲得參加[2008年夏季奧林匹克運動會棒球比賽的資格](../Page/2008年夏季奧林匹克運動會棒球比賽.md "wikilink")，第三名與第四名則可以獲得參加[2008年夏季奧運棒球最終資格排名賽的資格](../Page/2008年夏季奧運棒球最終資格排名賽.md "wikilink")。本屆賽事前六名可獲得參加[2007年世界盃棒球賽的資格](../Page/2007年世界盃棒球賽.md "wikilink")，本屆賽事前七名可以獲得[2007年泛美運動會棒球比賽的參賽資格](../Page/2007年泛美運動會棒球比賽.md "wikilink")。

本屆賽事共有12個國家參賽，除了主辦國[古巴之外](../Page/古巴.md "wikilink")，其餘的11個國家皆由分區進行的資格賽中選出，分別為[美國](../Page/美國.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[尼加拉瓜](../Page/尼加拉瓜.md "wikilink")、[巴拿馬](../Page/巴拿馬.md "wikilink")、[墨西哥](../Page/墨西哥.md "wikilink")、[多明尼加](../Page/多明尼加.md "wikilink")、[波多黎各](../Page/波多黎各.md "wikilink")、[委內瑞拉](../Page/委內瑞拉.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[厄瓜多與](../Page/厄瓜多.md "wikilink")[哥倫比亞](../Page/哥倫比亞.md "wikilink")。

12個參賽國家在預賽先分成兩組進行[單循環賽](../Page/循環賽.md "wikilink")，各組前四名可以晉級複賽，複賽亦是[單循環賽](../Page/循環賽.md "wikilink")，以戰績排名。

[美國獲得此屆賽事的冠軍](../Page/美國.md "wikilink")，與亞軍[古巴皆獲得參加](../Page/古巴.md "wikilink")[2008年夏季奧林匹克運動會棒球比賽的資格](../Page/2008年夏季奧林匹克運動會棒球比賽.md "wikilink")。季軍[墨西哥與殿軍](../Page/墨西哥.md "wikilink")[加拿大則取得參加](../Page/加拿大.md "wikilink")[2008年夏季奧運棒球最終資格排名賽的資格](../Page/2008年夏季奧運棒球最終資格排名賽.md "wikilink")。以上四隊與[委內瑞拉和](../Page/委內瑞拉.md "wikilink")[巴拿馬皆獲得參加](../Page/巴拿馬.md "wikilink")[2007年世界盃棒球賽的資格](../Page/2007年世界盃棒球賽.md "wikilink")。另外加上[尼加拉瓜一起共同獲得](../Page/尼加拉瓜.md "wikilink")[2007年泛美運動會棒球比賽](../Page/2007年泛美運動會棒球比賽.md "wikilink")
的參賽資格

## 資格賽

**2006年泛美棒球錦標賽資格賽**於2005年分成三個地區比賽，分別是北美中美洲、加勒比海地區與南美洲。從參賽的18個國家中選出11個國家參加2006年泛美棒球錦標賽。

加勒比海地區的資格賽中，[牙買加](../Page/牙買加.md "wikilink")、[美屬維京群島與](../Page/美屬維京群島.md "wikilink")[庫拉索棄權](../Page/庫拉索.md "wikilink")。

### 北美洲與中美洲

<table>
<thead>
<tr class="header">
<th><p>參賽國家</p></th>
<th><p>比賽</p></th>
<th><p>勝</p></th>
<th><p>敗</p></th>
<th><p>勝率</p></th>
<th><p>得分</p></th>
<th><p>失分</p></th>
<th><p>得失分差</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/美國棒球代表隊.md" title="wikilink">美國</a></p></td>
<td><p>5</p></td>
<td><p>5</p></td>
<td><p>0</p></td>
<td><p>1.000</p></td>
<td><p>49</p></td>
<td><p>13</p></td>
<td><p>+36</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/加拿大棒球代表隊.md" title="wikilink">加拿大</a></p></td>
<td><p>5</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>0.600</p></td>
<td><p>34</p></td>
<td><p>17</p></td>
<td><p>+17</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/尼加拉瓜棒球代表隊.md" title="wikilink">尼加拉瓜</a></p></td>
<td><p>5</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>0.600</p></td>
<td><p>20</p></td>
<td><p>14</p></td>
<td><p>+6</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/巴拿馬棒球代表隊.md" title="wikilink">巴拿馬</a></p></td>
<td><p>5</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>0.400</p></td>
<td><p>38</p></td>
<td><p>35</p></td>
<td><p>+3</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/墨西哥棒球代表隊.md" title="wikilink">墨西哥</a></p></td>
<td><p>5</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>0.400</p></td>
<td><p>24</p></td>
<td><p>28</p></td>
<td><p>-4</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/瓜地馬拉棒球代表隊.md" title="wikilink">瓜地馬拉</a></p></td>
<td><p>5</p></td>
<td><p>0</p></td>
<td><p>5</p></td>
<td><p>0.000</p></td>
<td><p>4</p></td>
<td><p>58</p></td>
<td><p>-54</p></td>
</tr>
</tbody>
</table>

#### 詳細賽程

<table>
<thead>
<tr class="header">
<th><p>主隊</p></th>
<th><p>比賽結果</p></th>
<th><p>客隊</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2005年11月15日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/尼加拉瓜棒球代表隊.md" title="wikilink">尼加拉瓜</a></p></td>
<td><p>1-3</p></td>
<td><p><strong><a href="../Page/加拿大棒球代表隊.md" title="wikilink">加拿大</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/美國棒球代表隊.md" title="wikilink">美國</a></strong></p></td>
<td><p>23-0（6局提前結束）</p></td>
<td><p><a href="../Page/瓜地馬拉棒球代表隊.md" title="wikilink">瓜地馬拉</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/巴拿馬棒球代表隊.md" title="wikilink">巴拿馬</a></strong></p></td>
<td><p>17-4（7局提前結束）</p></td>
<td><p><a href="../Page/墨西哥棒球代表隊.md" title="wikilink">墨西哥</a></p></td>
</tr>
<tr class="odd">
<td><p>2005年11月16日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/加拿大棒球代表隊.md" title="wikilink">加拿大</a></strong></p></td>
<td><p>12-0（7局提前結束）</p></td>
<td><p><a href="../Page/瓜地馬拉棒球代表隊.md" title="wikilink">瓜地馬拉</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/尼加拉瓜棒球代表隊.md" title="wikilink">尼加拉瓜</a></strong></p></td>
<td><p>6-1</p></td>
<td><p><a href="../Page/巴拿馬棒球代表隊.md" title="wikilink">巴拿馬</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/美國棒球代表隊.md" title="wikilink">美國</a></strong></p></td>
<td><p>5-4</p></td>
<td><p><a href="../Page/墨西哥棒球代表隊.md" title="wikilink">墨西哥</a></p></td>
</tr>
<tr class="odd">
<td><p>2005年11月17日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/加拿大棒球代表隊.md" title="wikilink">加拿大</a></strong></p></td>
<td><p>15-5</p></td>
<td><p><a href="../Page/巴拿馬棒球代表隊.md" title="wikilink">巴拿馬</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/美國棒球代表隊.md" title="wikilink">美國</a></strong></p></td>
<td><p>7-4</p></td>
<td><p><a href="../Page/尼加拉瓜棒球代表隊.md" title="wikilink">尼加拉瓜</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/墨西哥棒球代表隊.md" title="wikilink">墨西哥</a></strong></p></td>
<td><p>5-2</p></td>
<td><p><a href="../Page/瓜地馬拉棒球代表隊.md" title="wikilink">瓜地馬拉</a></p></td>
</tr>
<tr class="odd">
<td><p>2005年11月18日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/墨西哥棒球代表隊.md" title="wikilink">墨西哥</a></strong></p></td>
<td><p>7-2</p></td>
<td><p><a href="../Page/加拿大棒球代表隊.md" title="wikilink">加拿大</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/尼加拉瓜棒球代表隊.md" title="wikilink">尼加拉瓜</a></strong></p></td>
<td><p>6-1</p></td>
<td><p><a href="../Page/瓜地馬拉棒球代表隊.md" title="wikilink">瓜地馬拉</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/美國棒球代表隊.md" title="wikilink">美國</a></strong></p></td>
<td><p>9-3</p></td>
<td><p><a href="../Page/巴拿馬棒球代表隊.md" title="wikilink">巴拿馬</a></p></td>
</tr>
<tr class="odd">
<td><p>2005年11月19日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/美國棒球代表隊.md" title="wikilink">美國</a></strong></p></td>
<td><p>5-2</p></td>
<td><p><a href="../Page/加拿大棒球代表隊.md" title="wikilink">加拿大</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/尼加拉瓜棒球代表隊.md" title="wikilink">尼加拉瓜</a></strong></p></td>
<td><p>4-2</p></td>
<td><p><a href="../Page/墨西哥棒球代表隊.md" title="wikilink">墨西哥</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/瓜地馬拉棒球代表隊.md" title="wikilink">瓜地馬拉</a></p></td>
<td><p>1-12</p></td>
<td><p><strong><a href="../Page/巴拿馬棒球代表隊.md" title="wikilink">巴拿馬</a></strong></p></td>
</tr>
</tbody>
</table>

### 加勒比海地區

<table>
<thead>
<tr class="header">
<th><p>參賽國家</p></th>
<th><p>比賽</p></th>
<th><p>勝</p></th>
<th><p>敗</p></th>
<th><p>勝率</p></th>
<th><p>得分</p></th>
<th><p>失分</p></th>
<th><p>得失分差</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/多明尼加棒球代表隊.md" title="wikilink">多明尼加</a></p></td>
<td><p>3</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>1.000</p></td>
<td><p>32</p></td>
<td><p>0</p></td>
<td><p>+32</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波多黎各棒球代表隊.md" title="wikilink">波多黎各</a></p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>0.667</p></td>
<td><p>32</p></td>
<td><p>5</p></td>
<td><p>+27</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿魯巴棒球代表隊.md" title="wikilink">阿魯巴</a></p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>0.333</p></td>
<td><p>11</p></td>
<td><p>17</p></td>
<td><p>-6</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/巴哈馬棒球代表隊.md" title="wikilink">巴哈馬</a></p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>0.000</p></td>
<td><p>0</p></td>
<td><p>53</p></td>
<td><p>-53</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/牙買加棒球代表隊.md" title="wikilink">牙買加</a></p></td>
<td><p>棄權</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/美屬維京群島棒球代表隊.md" title="wikilink">美屬維京群島</a></p></td>
<td><p>棄權</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/庫拉索棒球代表隊.md" title="wikilink">庫拉索</a></p></td>
<td><p>棄權</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 詳細賽程

### 南美洲

<table>
<thead>
<tr class="header">
<th><p>參賽國家</p></th>
<th><p>比賽</p></th>
<th><p>勝</p></th>
<th><p>敗</p></th>
<th><p>勝率</p></th>
<th><p>得分</p></th>
<th><p>失分</p></th>
<th><p>得失分差</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/委內瑞拉棒球代表隊.md" title="wikilink">委內瑞拉</a></p></td>
<td><p>4</p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p>1.000</p></td>
<td><p>24</p></td>
<td><p>12</p></td>
<td><p>+12</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/巴西棒球代表隊.md" title="wikilink">巴西</a></p></td>
<td><p>4</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>0.750</p></td>
<td><p>26</p></td>
<td><p>8</p></td>
<td><p>+18</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/厄瓜多棒球代表隊.md" title="wikilink">厄瓜多</a></p></td>
<td><p>4</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>0.500</p></td>
<td><p>14</p></td>
<td><p>15</p></td>
<td><p>-1</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/哥倫比亞棒球代表隊.md" title="wikilink">哥倫比亞</a></p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>3</p></td>
<td><p>0.250</p></td>
<td><p>14</p></td>
<td><p>19</p></td>
<td><p>-5</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿根廷棒球代表隊.md" title="wikilink">阿根廷</a></p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p>4</p></td>
<td><p>0.000</p></td>
<td><p>12</p></td>
<td><p>36</p></td>
<td><p>-24</p></td>
</tr>
</tbody>
</table>

#### 詳細賽程

## 預賽

### A組

<table>
<thead>
<tr class="header">
<th><p>參賽國家</p></th>
<th><p>比賽</p></th>
<th><p>勝</p></th>
<th><p>敗</p></th>
<th><p>勝率</p></th>
<th><p>得分</p></th>
<th><p>失分</p></th>
<th><p>得失分差</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/古巴棒球代表隊.md" title="wikilink">古巴</a></p></td>
<td><p>5</p></td>
<td><p>5</p></td>
<td><p>0</p></td>
<td><p>1.000</p></td>
<td><p>64</p></td>
<td><p>10</p></td>
<td><p>+54</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/巴拿馬棒球代表隊.md" title="wikilink">巴拿馬</a></p></td>
<td><p>5</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>0.800</p></td>
<td><p>38</p></td>
<td><p>17</p></td>
<td><p>+21</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/尼加拉瓜棒球代表隊.md" title="wikilink">尼加拉瓜</a></p></td>
<td><p>5</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>0.600</p></td>
<td><p>24</p></td>
<td><p>18</p></td>
<td><p>+6</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/多明尼加棒球代表隊.md" title="wikilink">多明尼加</a></p></td>
<td><p>5</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>0.400</p></td>
<td><p>31</p></td>
<td><p>31</p></td>
<td><p>0</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/哥倫比亞棒球代表隊.md" title="wikilink">哥倫比亞</a></p></td>
<td><p>5</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
<td><p>0.200</p></td>
<td><p>15</p></td>
<td><p>41</p></td>
<td><p>-26</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/厄瓜多棒球代表隊.md" title="wikilink">厄瓜多</a></p></td>
<td><p>5</p></td>
<td><p>0</p></td>
<td><p>5</p></td>
<td><p>0.000</p></td>
<td><p>11</p></td>
<td><p>63</p></td>
<td><p>-52</p></td>
</tr>
</tbody>
</table>

### B組

<table>
<thead>
<tr class="header">
<th><p>參賽國家</p></th>
<th><p>比賽</p></th>
<th><p>勝</p></th>
<th><p>敗</p></th>
<th><p>勝率</p></th>
<th><p>得分</p></th>
<th><p>失分</p></th>
<th><p>得失分差</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/美國棒球代表隊.md" title="wikilink">美國</a></p></td>
<td><p>5</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>0.800</p></td>
<td><p>46</p></td>
<td><p>27</p></td>
<td><p>+19</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/墨西哥棒球代表隊.md" title="wikilink">墨西哥</a></p></td>
<td><p>5</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>0.800</p></td>
<td><p>23</p></td>
<td><p>19</p></td>
<td><p>+4</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/加拿大棒球代表隊.md" title="wikilink">加拿大</a></p></td>
<td><p>5</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>0.600</p></td>
<td><p>24</p></td>
<td><p>18</p></td>
<td><p>+6</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/委內瑞拉棒球代表隊.md" title="wikilink">委內瑞拉</a></p></td>
<td><p>5</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>0.400</p></td>
<td><p>27</p></td>
<td><p>39</p></td>
<td><p>-12</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/巴西棒球代表隊.md" title="wikilink">巴西</a></p></td>
<td><p>5</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
<td><p>0.200</p></td>
<td><p>27</p></td>
<td><p>32</p></td>
<td><p>-5</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波多黎各棒球代表隊.md" title="wikilink">波多黎各</a></p></td>
<td><p>5</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
<td><p>0.200</p></td>
<td><p>23</p></td>
<td><p>34</p></td>
<td><p>-11</p></td>
</tr>
</tbody>
</table>

### 詳細賽程

<table>
<thead>
<tr class="header">
<th><p>主隊</p></th>
<th></th>
<th><p>客隊</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2006年8月25日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/古巴棒球代表隊.md" title="wikilink">古巴</a></strong></p></td>
<td></td>
<td><p><a href="../Page/哥倫比亞棒球代表隊.md" title="wikilink">哥倫比亞</a></p></td>
</tr>
<tr class="odd">
<td><p>2006年8月26日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/美國棒球代表隊.md" title="wikilink">美國</a></strong></p></td>
<td><p>9-3</p></td>
<td><p><a href="../Page/加拿大棒球代表隊.md" title="wikilink">加拿大</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/古巴棒球代表隊.md" title="wikilink">古巴</a></strong></p></td>
<td><p>11-0</p></td>
<td><p><a href="../Page/厄瓜多棒球代表隊.md" title="wikilink">厄瓜多</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/墨西哥棒球代表隊.md" title="wikilink">墨西哥</a></strong></p></td>
<td><p>2-1</p></td>
<td><p><a href="../Page/巴西棒球代表隊.md" title="wikilink">巴西</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/巴拿馬棒球代表隊.md" title="wikilink">巴拿馬</a></strong></p></td>
<td><p>3-2</p></td>
<td><p><a href="../Page/多明尼加棒球代表隊.md" title="wikilink">多明尼加</a></p></td>
</tr>
<tr class="even">
<td><p>2006年8月27日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/加拿大棒球代表隊.md" title="wikilink">加拿大</a></strong></p></td>
<td><p>6-5</p></td>
<td><p><a href="../Page/波多黎各棒球代表隊.md" title="wikilink">波多黎各</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/美國棒球代表隊.md" title="wikilink">美國</a></strong></p></td>
<td><p>8-7</p></td>
<td><p><a href="../Page/巴西棒球代表隊.md" title="wikilink">巴西</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/古巴棒球代表隊.md" title="wikilink">古巴</a></strong></p></td>
<td><p>7-6</p></td>
<td><p><a href="../Page/巴拿馬棒球代表隊.md" title="wikilink">巴拿馬</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/墨西哥棒球代表隊.md" title="wikilink">墨西哥</a></strong></p></td>
<td><p>5-4</p></td>
<td><p><a href="../Page/委內瑞拉棒球代表隊.md" title="wikilink">委內瑞拉</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/尼加拉瓜棒球代表隊.md" title="wikilink">尼加拉瓜</a></strong></p></td>
<td><p>4-0</p></td>
<td><p><a href="../Page/哥倫比亞棒球代表隊.md" title="wikilink">哥倫比亞</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/多明尼加棒球代表隊.md" title="wikilink">多明尼加</a></strong></p></td>
<td><p>13-1</p></td>
<td><p><a href="../Page/厄瓜多棒球代表隊.md" title="wikilink">厄瓜多</a></p></td>
</tr>
<tr class="odd">
<td><p>2006年8月28日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/墨西哥棒球代表隊.md" title="wikilink">墨西哥</a></p></td>
<td><p>3-15</p></td>
<td><p><strong><a href="../Page/美國棒球代表隊.md" title="wikilink">美國</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/委內瑞拉棒球代表隊.md" title="wikilink">委內瑞拉</a></p></td>
<td><p>5-7</p></td>
<td><p><strong><a href="../Page/加拿大棒球代表隊.md" title="wikilink">加拿大</a></strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/波多黎各棒球代表隊.md" title="wikilink">波多黎各</a></strong></p></td>
<td><p>10-6</p></td>
<td><p><a href="../Page/巴西棒球代表隊.md" title="wikilink">巴西</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/古巴棒球代表隊.md" title="wikilink">古巴</a></strong></p></td>
<td><p>17-3</p></td>
<td><p><a href="../Page/多明尼加棒球代表隊.md" title="wikilink">多明尼加</a></p></td>
</tr>
<tr class="even">
<td><p>2006年8月29日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波多黎各棒球代表隊.md" title="wikilink">波多黎各</a></p></td>
<td><p>2-5</p></td>
<td><p><strong><a href="../Page/美國棒球代表隊.md" title="wikilink">美國</a></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/加拿大棒球代表隊.md" title="wikilink">加拿大</a></p></td>
<td><p>1-2</p></td>
<td><p><strong><a href="../Page/墨西哥棒球代表隊.md" title="wikilink">墨西哥</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/多明尼加棒球代表隊.md" title="wikilink">多明尼加</a></strong></p></td>
<td><p>8-3</p></td>
<td><p><a href="../Page/哥倫比亞棒球代表隊.md" title="wikilink">哥倫比亞</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/古巴棒球代表隊.md" title="wikilink">古巴</a></strong></p></td>
<td><p>11-0</p></td>
<td><p><a href="../Page/尼加拉瓜棒球代表隊.md" title="wikilink">尼加拉瓜</a></p></td>
</tr>
<tr class="odd">
<td><p>2006年8月30日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/美國棒球代表隊.md" title="wikilink">美國</a></p></td>
<td><p>9-12</p></td>
<td><p><strong><a href="../Page/委內瑞拉棒球代表隊.md" title="wikilink">委內瑞拉</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/加拿大棒球代表隊.md" title="wikilink">加拿大</a></strong></p></td>
<td><p>7-3</p></td>
<td><p><a href="../Page/巴西棒球代表隊.md" title="wikilink">巴西</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/古巴棒球代表隊.md" title="wikilink">古巴</a></strong></p></td>
<td><p>15-1</p></td>
<td><p><a href="../Page/厄瓜多棒球代表隊.md" title="wikilink">厄瓜多</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/尼加拉瓜棒球代表隊.md" title="wikilink">尼加拉瓜</a></strong></p></td>
<td><p>6-4</p></td>
<td><p><a href="../Page/多明尼加棒球代表隊.md" title="wikilink">多明尼加</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/墨西哥棒球代表隊.md" title="wikilink">墨西哥</a></strong></p></td>
<td><p>8-7</p></td>
<td><p><a href="../Page/波多黎各棒球代表隊.md" title="wikilink">波多黎各</a></p></td>
</tr>
</tbody>
</table>

## 複賽

<table>
<thead>
<tr class="header">
<th><p>參賽國家</p></th>
<th><p>比賽</p></th>
<th><p>勝</p></th>
<th><p>敗</p></th>
<th><p>勝率</p></th>
<th><p>得分</p></th>
<th><p>失分</p></th>
<th><p>得失分差</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/美國棒球代表隊.md" title="wikilink">美國</a></p></td>
<td><p>7</p></td>
<td><p>6</p></td>
<td><p>1</p></td>
<td><p>0.857</p></td>
<td><p>69</p></td>
<td><p>32</p></td>
<td><p>+37</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/古巴棒球代表隊.md" title="wikilink">古巴</a></p></td>
<td><p>7</p></td>
<td><p>6</p></td>
<td><p>1</p></td>
<td><p>0.857</p></td>
<td><p>63</p></td>
<td><p>18</p></td>
<td><p>+45</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/墨西哥棒球代表隊.md" title="wikilink">墨西哥</a></p></td>
<td><p>7</p></td>
<td><p>5</p></td>
<td><p>2</p></td>
<td><p>0.714</p></td>
<td><p>38</p></td>
<td><p>24</p></td>
<td><p>+14</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/加拿大棒球代表隊.md" title="wikilink">加拿大</a></p></td>
<td><p>7</p></td>
<td><p>4</p></td>
<td><p>3</p></td>
<td><p>0.571</p></td>
<td><p>43</p></td>
<td><p>38</p></td>
<td><p>+5</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/委內瑞拉棒球代表隊.md" title="wikilink">委內瑞拉</a></p></td>
<td><p>7</p></td>
<td><p>3</p></td>
<td><p>4</p></td>
<td><p>0.428</p></td>
<td><p>41</p></td>
<td><p>41</p></td>
<td><p>0</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/巴拿馬棒球代表隊.md" title="wikilink">巴拿馬</a></p></td>
<td><p>7</p></td>
<td><p>2</p></td>
<td><p>5</p></td>
<td><p>0.286</p></td>
<td><p>39</p></td>
<td><p>51</p></td>
<td><p>-12</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/尼加拉瓜棒球代表隊.md" title="wikilink">尼加拉瓜</a></p></td>
<td><p>7</p></td>
<td><p>2</p></td>
<td><p>5</p></td>
<td><p>0.286</p></td>
<td><p>16</p></td>
<td><p>33</p></td>
<td><p>-17</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/多明尼加棒球代表隊.md" title="wikilink">多明尼加</a></p></td>
<td><p>7</p></td>
<td><p>0</p></td>
<td><p>7</p></td>
<td><p>0.000</p></td>
<td><p>21</p></td>
<td><p>61</p></td>
<td><p>-40</p></td>
</tr>
</tbody>
</table>

### 詳細賽程

## 參考文獻與注釋

## 參照

  - [泛美棒球錦標賽](../Page/泛美棒球錦標賽.md "wikilink")
  - [2007年泛美運動會棒球比賽](../Page/2007年泛美運動會棒球比賽.md "wikilink")
  - [2007年世界盃棒球賽](../Page/2007年世界盃棒球賽.md "wikilink")
  - [2008年夏季奧運棒球最終資格排名賽](../Page/2008年夏季奧運棒球最終資格排名賽.md "wikilink")
  - [2008年夏季奧林匹克運動會棒球比賽](../Page/2008年夏季奧林匹克運動會棒球比賽.md "wikilink")

## 外部連結

[en:Baseball at the 2008 Summer Olympics – Qualification\#American
Qualifying
Tournament](../Page/en:Baseball_at_the_2008_Summer_Olympics_–_Qualification#American_Qualifying_Tournament.md "wikilink")

[Category:2006年棒球](../Category/2006年棒球.md "wikilink")
[Category:2006年古巴](../Category/2006年古巴.md "wikilink")
[Category:棒球競賽](../Category/棒球競賽.md "wikilink")
[Category:泛美棒球錦標賽](../Category/泛美棒球錦標賽.md "wikilink")