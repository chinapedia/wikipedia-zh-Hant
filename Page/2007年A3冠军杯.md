**2007年A3冠军杯**（**A3 Champions Cup
2007**）是第五屆[A3冠軍杯](../Page/A3冠軍杯.md "wikilink")，在2007年6月7日至13日於[中國](../Page/中华人民共和国.md "wikilink")[山東省](../Page/山東省.md "wikilink")[濟南市舉行](../Page/濟南市.md "wikilink")，主場館是[山东省体育中心](../Page/山东省体育中心体育场.md "wikilink")，在揭幕战[上海申花](../Page/上海申花.md "wikilink")
3–0
完胜[城南一和的比分同时也创造了中国俱乐部球队在亚洲赛事对](../Page/城南一和.md "wikilink")[韩国球队的最悬殊比分纪录](../Page/大韩民国.md "wikilink")，而[浦和红钻VS](../Page/浦和红钻.md "wikilink")[山東魯能泰山的比賽成為](../Page/山東魯能泰山.md "wikilink")[A3冠軍杯其中一場進球最多的比賽](../Page/A3冠軍杯.md "wikilink")。最终以联赛亚军身份参赛的上海申花以净胜球的优势，成为历史上首支夺得该项赛事的中国俱乐部，同時結束了在此之前韩国球隊連續三屆奪得冠军的紀錄。

## 參賽球隊

  - [山東魯能泰山](../Page/山東魯能泰山足球俱樂部.md "wikilink")（[2006年中國足球超級聯賽冠軍](../Page/2006年中國足球超級聯賽.md "wikilink")）

  - [浦和紅鑽](../Page/浦和紅鑽.md "wikilink")（2006年[日本職業足球聯賽冠軍](../Page/日本職業足球聯賽.md "wikilink")）

  - [城南一和天馬](../Page/城南足球俱樂部.md "wikilink")（2006年[南韓職業足球聯賽冠軍](../Page/南韓職業足球聯賽.md "wikilink")）

  - [上海申花](../Page/上海申花足球俱樂部.md "wikilink")（[2006年中國足球超級聯賽亞軍](../Page/2006年中國足球超級聯賽.md "wikilink")）

## 積分榜

<table>
<thead>
<tr class="header">
<th><p>排名</p></th>
<th><p>球隊</p></th>
<th><p>賽</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>入球</p></th>
<th><p>失球</p></th>
<th><p>球差</p></th>
<th><p>積分</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/上海申花足球俱樂部.md" title="wikilink">上海申花</a></p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>7</p></td>
<td><p>3</p></td>
<td><p>+4</p></td>
<td><p><strong>6</strong></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/山東魯能泰山足球俱樂部.md" title="wikilink">山東魯能泰山</a></p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>7</p></td>
<td><p>6</p></td>
<td><p>+1</p></td>
<td><p><strong>6</strong></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/浦和紅鑽.md" title="wikilink">浦和紅鑽</a></p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>5</p></td>
<td><p>7</p></td>
<td><p>−2</p></td>
<td><p><strong>3</strong></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/城南足球俱樂部.md" title="wikilink">城南一和天馬</a></p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>5</p></td>
<td><p>−3</p></td>
<td><p><strong>3</strong></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 賽果

**所有時間為[UTC+8](../Page/UTC+8.md "wikilink")**

**第一輪**

-----

**第二輪**

-----

**第三輪**


{| width=95% |- align=center |**2007年A3冠军杯冠军**:

[ShanghaiShenhuaSVG.gif](https://zh.wikipedia.org/wiki/File:ShanghaiShenhuaSVG.gif "fig:ShanghaiShenhuaSVG.gif")
**[上海申花](../Page/上海申花.md "wikilink")**
**历史上首个冠军**

|}
{| width=95% |- align=center |**赛事MVP**:
**[李钢](../Page/李钢.md "wikilink")**
([ShanghaiShenhuaSVG.gif](https://zh.wikipedia.org/wiki/File:ShanghaiShenhuaSVG.gif "fig:ShanghaiShenhuaSVG.gif")
**上海申花**) |}

## 每轮排名

| 球队                                     | 1 | 2 | 3 |
| -------------------------------------- | - | - | - |
| [上海申花](../Page/上海申花.md "wikilink")     | 1 | 2 | 1 |
| [山东鲁能泰山](../Page/山东鲁能泰山.md "wikilink") | 2 | 1 | 2 |
| [浦和红钻](../Page/浦和红钻.md "wikilink")     | 3 | 3 | 3 |
| [城南一和](../Page/城南一和.md "wikilink")     | 4 | 4 | 4 |
|                                        |   |   |   |

## 射手榜

| <font color="white"> **排名** | <font color="white"> **進球數目** | <font color="white"> **球員**                | <font color="white"> **效力球會**          |
| --------------------------- | ----------------------------- | ------------------------------------------ | -------------------------------------- |
| 1                           | **3球**                        | [华盛顿](../Page/华盛顿·斯泰卡奈罗·塞凯拉.md "wikilink") | [浦和红钻](../Page/浦和红钻.md "wikilink")     |
| 2                           | **2球**                        | [李钢](../Page/李钢.md "wikilink")             | [上海申花](../Page/上海申花.md "wikilink")     |
| 2                           | **2球**                        | [里卡多](../Page/汉密尔顿·里卡多.md "wikilink")      | [上海申花](../Page/上海申花.md "wikilink")     |
| 2                           | **2球**                        | [阿隆索](../Page/迭戈·阿隆索.md "wikilink")        | [上海申花](../Page/上海申花.md "wikilink")     |
| 2                           | **2球**                        | [鄭智](../Page/鄭智.md "wikilink")             | [山東魯能泰山](../Page/山東魯能泰山.md "wikilink") |
| 2                           | **2球**                        | [日夫科维奇](../Page/亚历山大·日夫科维奇.md "wikilink")  | [山東魯能泰山](../Page/山東魯能泰山.md "wikilink") |
| 7                           | **1球**                        | [長谷部誠](../Page/長谷部誠.md "wikilink")         | [浦和红钻](../Page/浦和红钻.md "wikilink")     |
| 7                           | **1球**                        | [田中斗莉王](../Page/田中斗莉王.md "wikilink")       | [浦和红钻](../Page/浦和红钻.md "wikilink")     |
| 7                           | **1球**                        | [周海濱](../Page/周海濱.md "wikilink")           | [山東魯能泰山](../Page/山東魯能泰山.md "wikilink") |
| 7                           | **1球**                        | [舒暢](../Page/舒畅_\(足球运动员\).md "wikilink")   | [山東魯能泰山](../Page/山東魯能泰山.md "wikilink") |
| 7                           | **1球**                        | [王曉龍](../Page/王曉龍.md "wikilink")           | [山東魯能泰山](../Page/山東魯能泰山.md "wikilink") |
| 7                           | **1球**                        | [常琳](../Page/常琳.md "wikilink")             | [上海申花](../Page/上海申花.md "wikilink")     |
| 7                           | **1球**                        | [金相植](../Page/金相植.md "wikilink")           | [城南一和](../Page/城南一和.md "wikilink")     |
| 7                           | **1球**                        | [崔成國](../Page/崔成國.md "wikilink")           | [城南一和](../Page/城南一和.md "wikilink")     |

## 外部連結

[新浪体育](http://sports.sina.com.cn/j/A32007/)

[Category:2007年足球](../Category/2007年足球.md "wikilink")
[Category:A3冠军杯](../Category/A3冠军杯.md "wikilink")