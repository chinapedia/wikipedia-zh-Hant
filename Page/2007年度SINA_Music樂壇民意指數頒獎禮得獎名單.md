**SINA
Music樂壇民意指數頒獎禮2007**於2008年1月15日假[香港國際展貿中心](../Page/香港國際展貿中心.md "wikilink")[Star
Hall舉行](../Page/EMax#匯星.md "wikilink")，主題為「Stop on Music
Power」，主持為[崔建邦](../Page/崔建邦.md "wikilink")、[楊愛瑾](../Page/楊愛瑾.md "wikilink")，當晚共頒發20個項目，其中14項「我最喜愛」獎項為由網民選舉得出，別具意義，總共35個獎項，以下為當晚的得獎名單：

## 得獎名單

  - SINA Music全年最高收聽率
      - 《All About Love》 ──[楊千嬅](../Page/楊千嬅.md "wikilink")

<!-- end list -->

  - SINA Music最高收聽率十大歌曲
      - 《男人KTV》 ──[側田](../Page/側田.md "wikilink")
      - 《鍾無豔》 ──[謝安琪](../Page/謝安琪.md "wikilink")
      - 《大女仔》 ──[鄭融](../Page/鄭融.md "wikilink")
      - 《電燈膽》 ──[鄧麗欣](../Page/鄧麗欣.md "wikilink")
      - 《思前戀後》 ──[孫耀威](../Page/孫耀威.md "wikilink")
      - 《逼得太緊》 ──[吳雨霏](../Page/吳雨霏.md "wikilink")
      - 《Crying In The Party》 ──[陳奕迅](../Page/陳奕迅.md "wikilink")
      - 《愛回家》 ──[古巨基](../Page/古巨基.md "wikilink")
      - 《酷愛》 ──[張敬軒](../Page/張敬軒.md "wikilink")
      - 《All About Love》 ──[楊千嬅](../Page/楊千嬅.md "wikilink")

<!-- end list -->

  - SINA Music合唱歌曲大獎
      - 《山歌》
        ──[側田](../Page/側田.md "wikilink")、[吳雨霏](../Page/吳雨霏.md "wikilink")

<!-- end list -->

  - SINA Music最高收視MV大獎
      - 《3/8》 ──[謝安琪](../Page/謝安琪.md "wikilink")

<!-- end list -->

  - SINA Music新浪直播室 —最高收視大獎
      - [農夫](../Page/農夫.md "wikilink")

<!-- end list -->

  - SINA Music全碟試聽 —最高收聽率大碟
      - [孫耀威](../Page/孫耀威.md "wikilink")

<!-- end list -->

  - SINA Music LIVE 現場演繹大獎
      - [關楚耀](../Page/關楚耀.md "wikilink")

<!-- end list -->

  - 我最喜愛至尊國語金曲
      - 《中國話》 ──[S.H.E](../Page/S.H.E.md "wikilink")

<!-- end list -->

  - 我最喜愛唱作人
      - [側田](../Page/側田.md "wikilink")

<!-- end list -->

  - 我最喜愛演唱會
      - 《Moving On Stage 1 演唱會》 ──[陳奕迅](../Page/陳奕迅.md "wikilink")

<!-- end list -->

  - 我最喜愛男新人
      - 金獎：[周柏豪](../Page/周柏豪.md "wikilink")
      - 銀獎：[陳柏宇](../Page/陳柏宇.md "wikilink")
      - 銅獎：[小肥](../Page/小肥.md "wikilink")

<!-- end list -->

  - 我最喜愛女新人
      - 金獎：[江若琳](../Page/江若琳.md "wikilink")
      - 銀獎：[李卓庭](../Page/李卓庭.md "wikilink")
      - 銅獎：[萱寧](../Page/萱寧.md "wikilink")

<!-- end list -->

  - 我最喜愛新組合
      - 金獎：[HotCha](../Page/HotCha.md "wikilink")
      - 銀獎：[棒棒堂](../Page/棒棒堂.md "wikilink")
      - 銅獎：[星光幫](../Page/星光幫.md "wikilink")

<!-- end list -->

  - 我最喜愛至尊大碟
      - 《Moments》 ──[古巨基](../Page/古巨基.md "wikilink")

<!-- end list -->

  - 我最喜愛男歌手（香港）
      - [古巨基](../Page/古巨基.md "wikilink")

<!-- end list -->

  - 我最喜愛女歌手（香港）
      - [楊千嬅](../Page/楊千嬅.md "wikilink")

<!-- end list -->

  - 我最喜愛組合（香港）
      - [農夫](../Page/農夫.md "wikilink")

<!-- end list -->

  - 我最喜愛男歌手（全國）
      - [王力宏](../Page/王力宏.md "wikilink")

<!-- end list -->

  - 我最喜愛女歌手（全國）
      - [張靚穎](../Page/張靚穎.md "wikilink")

<!-- end list -->

  - 我最喜愛組合（全國）
      - [S.H.E](../Page/S.H.E.md "wikilink")

<!-- end list -->

  - 我最喜愛至尊金曲
      - 《酷愛》 ──[張敬軒](../Page/張敬軒.md "wikilink")

## 媒體播放

於[香港](../Page/香港.md "wikilink")、[中國](../Page/中國.md "wikilink")、[台灣網上直播及重溫](../Page/台灣.md "wikilink")。另外，本年度之賽果並不計算入[四台聯頒音樂大獎內](../Page/四台聯頒音樂大獎.md "wikilink")。

## 外部链接

  - [SINA
    Music樂壇民意指數頒獎禮2007](https://web.archive.org/web/20080103055501/http://events.sina.com.hk/event07/musicawards/index.html)

## 參看

  - [SINA Music樂壇民意指數頒獎禮](../Page/SINA_Music樂壇民意指數頒獎禮.md "wikilink")

[Category:SINA
Music樂壇民意指數頒獎禮](../Category/SINA_Music樂壇民意指數頒獎禮.md "wikilink")
[Category:2007年](../Category/2007年.md "wikilink")