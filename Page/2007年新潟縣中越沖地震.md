**2007年新潟縣中越沖地震**發生於2007年7月16日[當地時間](../Page/日本時間.md "wikilink")10時13分，[日本](../Page/日本.md "wikilink")[新潟縣上中越沖發生的一次](../Page/新潟縣.md "wikilink")[地震](../Page/地震.md "wikilink")。[日本氣象廳正式命名為](../Page/日本氣象廳.md "wikilink")[平成](../Page/平成.md "wikilink")19年（2007年）新潟縣中越沖地震。

## 強度

地震震級達到[芮氏](../Page/芮氏.md "wikilink")6.8，為中越地方自[2004年新潟縣中越地震以來最強的一次地震](../Page/2004年新潟縣中越地震.md "wikilink")，還引發了輕微[海嘯](../Page/海嘯.md "wikilink")。

## 被害情況

  - [新潟縣](../Page/新潟縣.md "wikilink")（統計至2007年12月14日）\[1\]

:\* 死者：15名

:\* 輕重傷者：2,316名

:\* 建物全毀：1,319棟

:\* 建物大規模半毀：857棟

:\* 建物半毀：4,764棟

:\* 建物部分損壞：34,659棟

:\* 非住家損壞：31,041棟

  - [長野縣](../Page/長野縣.md "wikilink")（2007年・平成19年8月2日 9時30分現在）\[2\]

:\* 輕重傷者：29名

:\* 建物部分損壞：318棟

  - [富山縣](../Page/富山縣.md "wikilink")

:\* 軽傷者：1名

## 財物損害

  - 16日10時25分，[東京電力](../Page/東京電力.md "wikilink")[柏崎刈羽核电站三号機变压器发生火警](../Page/柏崎刈羽核电站.md "wikilink")；12時10分火被扑灭，但并未确认是否有放射性物质泄漏\[3\]。
  - 16日14時，[柏崎市約](../Page/柏崎市.md "wikilink")4万2600户居民断水，新潟、長野两县合超过了五万户居民以上断水。
  - 柏崎市、[上越市](../Page/上越市.md "wikilink")、[刈羽村](../Page/刈羽村.md "wikilink")、[長岡市](../Page/長岡市.md "wikilink")、[三条市](../Page/三条市.md "wikilink")、[燕市](../Page/燕市.md "wikilink")、[加茂市](../Page/加茂市.md "wikilink")、[新潟市在地震发生时](../Page/新潟市.md "wikilink")，共有3,5344户居民停电\[4\]。[長野市北部部分地区地震后](../Page/長野市.md "wikilink")（10時14分）有约2,1200户居民停電\[5\]。
  - 長野县[飯綱町地区的三水庁舎付近的道路隆起](../Page/飯綱町.md "wikilink")，水管破裂。
  - [柏崎刈羽核电站六号機外泄出含有微量放射性物質的水](../Page/柏崎刈羽核电站.md "wikilink")，并确认水已经排水管流入大海\[6\]。
  - 生產[汽車用](../Page/汽車.md "wikilink")[變速箱及](../Page/變速箱.md "wikilink")[引擎零件的](../Page/引擎.md "wikilink")[理研公司](../Page/理研公司.md "wikilink")（）位於柏崎市之工廠，受地震影響導致廠內部份生產機器受損而停工，間接影響到[豐田汽車](../Page/豐田汽車.md "wikilink")、[富士重工等日本車廠的生產](../Page/富士重工.md "wikilink")\[7\]。

## 资料來源

## 關連項目

  - [能登半岛地震](../Page/能登半岛地震.md "wikilink")
  - [2004年新潟县中越地震](../Page/2004年新潟县中越地震.md "wikilink")
  - [阪神大地震](../Page/阪神大地震.md "wikilink")

## 外部連結

  - [日本氣象廳](http://www.jma.go.jp/jma/index.html)
  - [新潟县](https://web.archive.org/web/20070622222442/http://www.pref.niigata.jp/)
      - [平成19年（2007年）新潟县中越沖地震关连情報](https://web.archive.org/web/20070808061605/http://bosai.pref.niigata.jp/bosaiportal/0716jishin/)
  - [長野县](http://www.pref.nagano.jp/)

{{-}}

[Category:2007年日本](../Category/2007年日本.md "wikilink")
[Category:2007年地震](../Category/2007年地震.md "wikilink")
[Category:日本地震](../Category/日本地震.md "wikilink")
[Category:新潟縣歷史](../Category/新潟縣歷史.md "wikilink")
[Category:中越地方](../Category/中越地方.md "wikilink")

1.  [平成19年7月16日に発生した新潟県中越沖地震よる被害状況について（第212報）](http://www.pref.niigata.lg.jp/HTML_Simple/higai1912141500.pdf)、新潟県災害対策本部、2007年12月14日
2.  [新潟県中越沖地震による県内の影響](http://www.pref.nagano.jp/kikikan/higai/h19/0716/higai_0802_0930.pdf)。長野県危機管理局、2007年8月2日
3.  [TEPCO：プレスリリース |
    新潟県上中越沖で発生した地震の影響について（午後1時現在）](http://www.tepco.co.jp/cc/press/07071601-j.html)。[東京電力](../Page/東京電力.md "wikilink")、2007年7月16日。
4.  [新潟県における地震発生による影響について（11時現在）|
    東北電力](http://www.tohoku-epco.co.jp/emergency/1/1175764_1493.html)
    。[東北電力](../Page/東北電力.md "wikilink")、2007年7月16日。
5.  [中部電力 | 緊急情報
    地震発生による影響について-2007年07月16日13時30分](http://www.chuden.co.jp/info/emergency/index.html)
    。[中部電力](../Page/中部電力.md "wikilink")、2007年7月16日閲覧。
6.  [TEPCO：プレスリリース |
    柏崎刈羽原子力発電所6号機の放射性物質の漏えいについて](http://www.tepco.co.jp/cc/press/07071604-j.html)。[東京電力](../Page/東京電力.md "wikilink")、2007年7月16日。
7.  <http://udn.com/NEWS/FINANCE/FIN5/3936001.shtml>