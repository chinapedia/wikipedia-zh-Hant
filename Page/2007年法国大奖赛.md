**2007年-{zh-hans:法国大奖赛;
zh-hant:法國站格蘭拔治大賽;}-**是2007年世界一级方程式锦标赛中的第8站。于6月29日至7月1日在[马尼库尔进行](../Page/马尼库尔.md "wikilink")。\[1\]

## 賽果

### 排位賽

<table>
<thead>
<tr class="header">
<th><p>位置</p></th>
<th><p>車手</p></th>
<th><p>車隊／引擎</p></th>
<th><p>第一次</p></th>
<th><p>第二次</p></th>
<th><p>第三次</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/費利·馬沙.md" title="wikilink">費利·馬沙</a></p></td>
<td><p><a href="../Page/法拉利車隊.md" title="wikilink">法拉利車隊</a></p></td>
<td><p>1:15.303</p></td>
<td><p>1:14.822</p></td>
<td><p><strong>1:15.034</strong></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/劉易斯·咸美頓.md" title="wikilink">劉易斯·咸美頓</a></p></td>
<td><p><a href="../Page/麥拿倫車隊.md" title="wikilink">麥拿倫車隊</a>-<a href="../Page/梅塞德斯.md" title="wikilink">梅塞德斯</a></p></td>
<td><p><strong>1:14.805</strong></p></td>
<td><p><strong>1:14.795</strong></p></td>
<td><p>1:15.104</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/奇米·拉高倫.md" title="wikilink">奇米·拉高倫</a></p></td>
<td><p><a href="../Page/法拉利車隊.md" title="wikilink">法拉利車隊</a></p></td>
<td><p>1:14.872</p></td>
<td><p>1:14.828</p></td>
<td><p>1:15.257</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/羅伯特·古碧沙.md" title="wikilink">羅伯特·古碧沙</a></p></td>
<td><p><a href="../Page/寶馬沙巴車隊.md" title="wikilink">寶馬沙巴車隊</a></p></td>
<td><p>1:15.778</p></td>
<td><p>1:15.066</p></td>
<td><p>1:15.493</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/尚卡羅·費希切拉.md" title="wikilink">尚卡羅·費希切拉</a></p></td>
<td><p><a href="../Page/雷諾車隊.md" title="wikilink">雷諾車隊</a></p></td>
<td><p>1:16.047</p></td>
<td><p>1:15.227</p></td>
<td><p>1:15.674</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/海基·科瓦莱宁.md" title="wikilink">海基·科瓦莱宁</a></p></td>
<td><p><a href="../Page/雷諾車隊.md" title="wikilink">雷諾車隊</a></p></td>
<td><p>1:15.524</p></td>
<td><p>1:15.272</p></td>
<td><p>1:15.826</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="../Page/尼克·夏菲特.md" title="wikilink">尼克·夏菲特</a></p></td>
<td><p><a href="../Page/寶馬沙巴車隊.md" title="wikilink">寶馬沙巴車隊</a></p></td>
<td><p>1:15.783</p></td>
<td><p>1:15.149</p></td>
<td><p>1:15.900</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><a href="../Page/雅诺·特鲁利.md" title="wikilink">雅诺·特鲁利</a></p></td>
<td><p><a href="../Page/豐田車隊.md" title="wikilink">豐田車隊</a></p></td>
<td><p>1:16.118</p></td>
<td><p>1:15.379</p></td>
<td><p>1:15.935</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p><a href="../Page/尼高·羅斯堡.md" title="wikilink">尼高·羅斯堡</a></p></td>
<td><p><a href="../Page/威廉士車隊.md" title="wikilink">威廉士車隊</a>-<a href="../Page/豐田車隊.md" title="wikilink">豐田</a></p></td>
<td><p>1:16.092</p></td>
<td><p>1:15.331</p></td>
<td><p>1:16.328</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p><a href="../Page/費蘭度·阿朗素.md" title="wikilink">費蘭度·阿朗素</a></p></td>
<td><p><a href="../Page/麥拿倫車隊.md" title="wikilink">麥拿倫車隊</a>-<a href="../Page/梅塞德斯.md" title="wikilink">梅塞德斯</a></p></td>
<td><p>1:15.322</p></td>
<td><p>1:15.084</p></td>
<td><p>‡</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p><a href="../Page/拉夫·舒密加.md" title="wikilink">拉夫·舒密加</a></p></td>
<td><p><a href="../Page/豐田車隊.md" title="wikilink">豐田車隊</a></p></td>
<td><p>1:15.760</p></td>
<td><p>1:15.534</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p><a href="../Page/詹森·畢頓.md" title="wikilink">詹森·畢頓</a></p></td>
<td><p><a href="../Page/本田車隊.md" title="wikilink">本田車隊</a></p></td>
<td><p>1:16.113</p></td>
<td><p>1:15.584</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p><a href="../Page/鲁宾斯·巴里切罗.md" title="wikilink">鲁宾斯·巴里切罗</a></p></td>
<td><p><a href="../Page/本田車隊.md" title="wikilink">本田車隊</a></p></td>
<td><p>1:16.140</p></td>
<td><p>1:15.761</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p><a href="../Page/馬克·韋伯.md" title="wikilink">馬克·韋伯</a></p></td>
<td><p><a href="../Page/紅牛車隊.md" title="wikilink">紅牛車隊</a>-<a href="../Page/雷諾車隊.md" title="wikilink">雷諾</a></p></td>
<td><p>1:15.746</p></td>
<td><p>1:15.806</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p><a href="../Page/史葛·史必.md" title="wikilink">史葛·史必</a></p></td>
<td><p><a href="../Page/紅牛二隊.md" title="wikilink">紅牛二隊</a>-<a href="../Page/法拉利車隊.md" title="wikilink">法拉利</a></p></td>
<td><p>1:15.980</p></td>
<td><p>1:16.049</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p><a href="../Page/大衛·古達.md" title="wikilink">大衛·古達</a></p></td>
<td><p><a href="../Page/紅牛車隊.md" title="wikilink">紅牛車隊</a>-<a href="../Page/雷諾車隊.md" title="wikilink">雷諾</a></p></td>
<td><p>1:15.915</p></td>
<td><p>†</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p><a href="../Page/維塔托尼奧·路爾茲.md" title="wikilink">維塔托尼奧·路爾茲</a></p></td>
<td><p><a href="../Page/紅牛二隊.md" title="wikilink">紅牛二隊</a>-<a href="../Page/法拉利車隊.md" title="wikilink">法拉利</a></p></td>
<td><p>1:16.142</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p><a href="../Page/亞歷山大·伍爾茲.md" title="wikilink">亞歷山大·伍爾茲</a></p></td>
<td><p><a href="../Page/威廉士車隊.md" title="wikilink">威廉士車隊</a>-<a href="../Page/豐田車隊.md" title="wikilink">豐田</a></p></td>
<td><p>1:16.241</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p><a href="../Page/安東尼·戴維森.md" title="wikilink">安東尼·戴維森</a></p></td>
<td><p><a href="../Page/超級亞久里車隊.md" title="wikilink">超級亞久里車隊</a>-<a href="../Page/本田車隊.md" title="wikilink">本田</a></p></td>
<td><p>1:16.366</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p><a href="../Page/克里斯蒂·艾爾拔.md" title="wikilink">克里斯蒂·艾爾拔</a></p></td>
<td><p><a href="../Page/世爵車隊.md" title="wikilink">世爵車隊</a>-<a href="../Page/法拉利車隊.md" title="wikilink">法拉利</a></p></td>
<td><p>1:17.826</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p><a href="../Page/艾德里安·苏蒂尔.md" title="wikilink">艾德里安·苏蒂尔</a></p></td>
<td><p><a href="../Page/世爵車隊.md" title="wikilink">世爵車隊</a>-<a href="../Page/法拉利車隊.md" title="wikilink">法拉利</a></p></td>
<td><p>1:17.915</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p><a href="../Page/佐藤琢磨.md" title="wikilink">佐藤琢磨</a></p></td>
<td><p><a href="../Page/超級亞久里車隊.md" title="wikilink">超級亞久里車隊</a>-<a href="../Page/本田車隊.md" title="wikilink">本田</a></p></td>
<td><p>1:16.244@</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

  -
    †[大衛·古達因波箱問題](../Page/大衛·古達.md "wikilink")，在第二輪排位賽中沒有參與。
    ‡[費蘭度·阿朗素因波箱問題](../Page/費蘭度·阿朗素.md "wikilink")，在三輪排位賽中沒有參與。
    @[佐藤琢磨在美國比賽中犯規](../Page/佐藤琢磨.md "wikilink")，被賽會下令排位退後十位。

### 正式比賽

<table>
<thead>
<tr class="header">
<th><p>位置</p></th>
<th><p>車號</p></th>
<th><p>車手</p></th>
<th><p>廾隊</p></th>
<th><p>圈數</p></th>
<th><p>時間</p></th>
<th><p>排位</p></th>
<th><p>分數</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>6</p></td>
<td><p><strong><a href="../Page/奇米·拉高倫.md" title="wikilink">奇米·拉高倫</a></strong></p></td>
<td><p><strong><a href="../Page/法拉利車隊.md" title="wikilink">法拉利車隊</a></strong></p></td>
<td><p>70</p></td>
<td><p>1:30:54.200</p></td>
<td><p>3</p></td>
<td><p><strong>10</strong></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>5</p></td>
<td><p><strong><a href="../Page/費利·馬沙.md" title="wikilink">費利·馬沙</a></strong></p></td>
<td><p><strong><a href="../Page/法拉利車隊.md" title="wikilink">法拉利車隊</a></strong></p></td>
<td><p>70</p></td>
<td><p>+2.4秒</p></td>
<td><p>1</p></td>
<td><p><strong>8</strong></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>2</p></td>
<td><p><strong><a href="../Page/劉易斯·咸美頓.md" title="wikilink">劉易斯·咸美頓</a></strong></p></td>
<td><p><strong><a href="../Page/麥拿倫車隊.md" title="wikilink">麥拿倫車隊</a>-<a href="../Page/梅塞德斯.md" title="wikilink">梅塞德斯</a></strong></p></td>
<td><p>70</p></td>
<td><p>+32.1秒</p></td>
<td><p>2</p></td>
<td><p><strong>6</strong></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>10</p></td>
<td><p><strong><a href="../Page/羅伯特·古碧沙.md" title="wikilink">羅伯特·古碧沙</a></strong></p></td>
<td><p><strong><a href="../Page/寶馬沙巴車隊.md" title="wikilink">寶馬沙巴車隊</a></strong></p></td>
<td><p>70</p></td>
<td><p>+41.7秒</p></td>
<td><p>4</p></td>
<td><p><strong>5</strong></p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>9</p></td>
<td><p><strong><a href="../Page/尼克·夏菲特.md" title="wikilink">尼克·夏菲特</a></strong></p></td>
<td><p><strong><a href="../Page/寶馬沙巴車隊.md" title="wikilink">寶馬沙巴車隊</a></strong></p></td>
<td><p>70</p></td>
<td><p>+48.8秒</p></td>
<td><p>7</p></td>
<td><p><strong>4</strong></p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>3</p></td>
<td><p><strong><a href="../Page/尚卡羅·費希切拉.md" title="wikilink">尚卡羅·費希切拉</a></strong></p></td>
<td><p><strong><a href="../Page/雷諾車隊.md" title="wikilink">雷諾車隊</a></strong></p></td>
<td><p>70</p></td>
<td><p>+52.2秒</p></td>
<td><p>5</p></td>
<td><p><strong>3</strong></p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>1</p></td>
<td><p><strong><a href="../Page/費蘭度·阿朗素.md" title="wikilink">費蘭度·阿朗素</a></strong></p></td>
<td><p><strong><a href="../Page/麥拿倫車隊.md" title="wikilink">麥拿倫車隊</a>-<a href="../Page/梅塞德斯.md" title="wikilink">梅塞德斯</a></strong></p></td>
<td><p>70</p></td>
<td><p>+56.5秒</p></td>
<td><p>10</p></td>
<td><p><strong>2</strong></p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>7</p></td>
<td><p><strong><a href="../Page/詹森·畢頓.md" title="wikilink">詹森·畢頓</a></strong></p></td>
<td><p><strong><a href="../Page/本田車隊.md" title="wikilink">本田車隊</a></strong></p></td>
<td><p>70</p></td>
<td><p>+58.8秒</p></td>
<td><p>12</p></td>
<td><p><strong>1</strong></p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>16</p></td>
<td><p><a href="../Page/尼高·羅斯堡.md" title="wikilink">尼高·羅斯堡</a></p></td>
<td><p><a href="../Page/威廉士車隊.md" title="wikilink">威廉士車隊</a>-<a href="../Page/豐田車隊.md" title="wikilink">豐田</a></p></td>
<td><p>70</p></td>
<td><p>+68.5秒</p></td>
<td><p>9</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>11</p></td>
<td><p><a href="../Page/拉夫·舒密加.md" title="wikilink">拉夫·舒密加</a></p></td>
<td><p><a href="../Page/豐田車隊.md" title="wikilink">豐田車隊</a></p></td>
<td><p>69</p></td>
<td><p>+1圈</p></td>
<td><p>11</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>8</p></td>
<td><p><a href="../Page/鲁宾斯·巴里切罗.md" title="wikilink">鲁宾斯·巴里切罗</a></p></td>
<td><p><a href="../Page/本田車隊.md" title="wikilink">本田車隊</a></p></td>
<td><p>69</p></td>
<td><p>+1圈</p></td>
<td><p>13</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>15</p></td>
<td><p><a href="../Page/馬克·韋伯.md" title="wikilink">馬克·韋伯</a></p></td>
<td><p><a href="../Page/紅牛車隊.md" title="wikilink">紅牛車隊</a>-<a href="../Page/雷諾車隊.md" title="wikilink">雷諾</a></p></td>
<td><p>69</p></td>
<td><p>+1圈</p></td>
<td><p>14</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>14</p></td>
<td><p><a href="../Page/大衛·古達.md" title="wikilink">大衛·古達</a></p></td>
<td><p><a href="../Page/紅牛車隊.md" title="wikilink">紅牛車隊</a>-<a href="../Page/雷諾車隊.md" title="wikilink">雷諾</a></p></td>
<td><p>69</p></td>
<td><p>+1圈</p></td>
<td><p>16</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p>17</p></td>
<td><p><a href="../Page/亞歷山大·禾斯.md" title="wikilink">亞歷山大·禾斯</a></p></td>
<td><p><a href="../Page/威廉士車隊.md" title="wikilink">威廉士車隊</a>-<a href="../Page/豐田車隊.md" title="wikilink">豐田</a></p></td>
<td><p>69</p></td>
<td><p>+1圈</p></td>
<td><p>18</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>4</p></td>
<td><p><a href="../Page/海基·科瓦莱宁.md" title="wikilink">海基·科瓦莱宁</a></p></td>
<td><p><a href="../Page/雷諾車隊.md" title="wikilink">雷諾車隊</a></p></td>
<td><p>69</p></td>
<td><p>+1圈</p></td>
<td><p>6</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p>22</p></td>
<td><p><a href="../Page/佐藤琢磨.md" title="wikilink">佐藤琢磨</a></p></td>
<td><p><a href="../Page/超級亞久里車隊.md" title="wikilink">超級亞久里車隊</a>-<a href="../Page/本田車隊.md" title="wikilink">本田</a></p></td>
<td><p>68</p></td>
<td><p>+2圈</p></td>
<td><p>22</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p>20</p></td>
<td><p><a href="../Page/艾德里安·苏蒂尔.md" title="wikilink">艾德里安·苏蒂尔</a></p></td>
<td><p><a href="../Page/世爵車隊.md" title="wikilink">世爵車隊</a>-<a href="../Page/法拉利車隊.md" title="wikilink">法拉利</a></p></td>
<td><p>68</p></td>
<td><p>+2圈</p></td>
<td><p>21</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Ret</p></td>
<td><p>19</p></td>
<td><p><a href="../Page/史葛·史必.md" title="wikilink">史葛·史必</a></p></td>
<td><p><a href="../Page/紅牛二隊.md" title="wikilink">紅牛二隊</a>-<a href="../Page/法拉利車隊.md" title="wikilink">法拉利</a></p></td>
<td><p>55</p></td>
<td><p>波箱</p></td>
<td><p>15</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Ret</p></td>
<td><p>21</p></td>
<td><p><a href="../Page/克里斯蒂·艾爾拔.md" title="wikilink">克里斯蒂·艾爾拔</a></p></td>
<td><p><a href="../Page/世爵車隊.md" title="wikilink">世爵車隊</a>-<a href="../Page/法拉利車隊.md" title="wikilink">法拉利</a></p></td>
<td><p>28</p></td>
<td><p>燃料意外</p></td>
<td><p>20</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Ret</p></td>
<td><p>23</p></td>
<td><p><a href="../Page/安東尼·戴維森.md" title="wikilink">安東尼·戴維森</a></p></td>
<td><p><a href="../Page/超級亞久里車隊.md" title="wikilink">超級亞久里車隊</a>-<a href="../Page/本田車隊.md" title="wikilink">本田</a></p></td>
<td><p>1</p></td>
<td><p>意外傷害</p></td>
<td><p>19</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Ret</p></td>
<td><p>12</p></td>
<td><p><a href="../Page/雅诺·特鲁利.md" title="wikilink">雅诺·特鲁利</a></p></td>
<td><p><a href="../Page/豐田車隊.md" title="wikilink">豐田車隊</a></p></td>
<td><p>1</p></td>
<td><p>意外傷害</p></td>
<td><p>7</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Ret</p></td>
<td><p>18</p></td>
<td><p><a href="../Page/維塔托尼奧·路爾茲.md" title="wikilink">維塔托尼奧·路爾茲</a></p></td>
<td><p><a href="../Page/紅牛二隊.md" title="wikilink">紅牛二隊</a>-<a href="../Page/法拉利車隊.md" title="wikilink">法拉利</a></p></td>
<td><p>0</p></td>
<td><p>意外傷害</p></td>
<td><p>17</p></td>
<td></td>
</tr>
</tbody>
</table>

#### 附註

  - 最快圈速：費利·馬沙1:16.099（第42圈）

## 註解

\<div class="references-small" {{\#if: | style="-moz-column-width:;
column-width:;" | {{\#if: | style="-moz-column-count:; column-count:
}};" |}}\>

<references />

</div>

## 外部链接

  - [Detailed French Grand Prix
    results](https://web.archive.org/web/20070705200210/http://www.manipef1.com/results/2007/france.php)

[Category:2007年一級方程式賽果](../Category/2007年一級方程式賽果.md "wikilink")
[Category:法国大奖赛](../Category/法国大奖赛.md "wikilink")

1.