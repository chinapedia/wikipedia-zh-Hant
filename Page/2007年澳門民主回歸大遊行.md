[20071220MacauParadeAd.jpg](https://zh.wikipedia.org/wiki/File:20071220MacauParadeAd.jpg "fig:20071220MacauParadeAd.jpg")
**2007年澳門民主回歸大遊行**發生於2007年12月20日下午，由[新澳門學社](../Page/新澳門學社.md "wikilink")、[職工民心協進會](../Page/職工民心協進會.md "wikilink")、[博彩建業聯合自由工會聯合發起](../Page/博彩建業聯合自由工會.md "wikilink")，以「反貪腐、爭民主、保民生，重建澳門的價值，共創我們這一代的集體記憶」為口號，就[澳門回歸八年以來所產生的社會問題表達意見](../Page/澳門回歸.md "wikilink")。是次被稱為是「歷來首次在回歸日發起民主遊行」，也是首次有遊行要求[雙普選](../Page/雙普選.md "wikilink")。

## 背景

[澳門回歸後](../Page/澳門回歸.md "wikilink")，[治安](../Page/治安.md "wikilink")、[經濟等方面雖然有所改善](../Page/澳門經濟.md "wikilink")，但社會上的[貧富懸殊問題日趨嚴重](../Page/貧富懸殊.md "wikilink")，[澳門特區政府又未能解決外勞](../Page/澳門特別行政區政府.md "wikilink")、[交通](../Page/交通.md "wikilink")、住屋問題等基本民生問題，令到草根階層的市民生活艱苦。另外2005年起，澳門政府被指在土地分配、[投資移民的問題上偏幫發展商](../Page/投資移民.md "wikilink")，與及停建[社會房屋及](../Page/社會房屋.md "wikilink")[經濟房屋](../Page/經濟房屋.md "wikilink")，以及[歐文龍貪污案被揭發等都令到澳門居民對澳門特區政府的管治威信失去信心](../Page/歐文龍貪污案.md "wikilink")，是回歸後的新低\[1\]。

## 遊行前

### 參與團體

由[新澳門學社主辦](../Page/新澳門學社.md "wikilink")，[職工民心協進會](../Page/職工民心協進會.md "wikilink")、[博彩建業聯合自由工會協辦並率領遊行者](../Page/博彩建業聯合自由工會.md "wikilink")。而[清潔員職工會](../Page/清潔員職工會.md "wikilink")、[護塔連線](../Page/護塔連線.md "wikilink")、[澳博職工則派代表參加其中](../Page/澳門博彩股份有限公司.md "wikilink")。另曾一度傳出會有團體發動警察參與遊行，但[澳門警察協會在](../Page/澳門警察協會.md "wikilink")12月17日表示不會參與「任何政治性質的活動」\[2\]；[澳門民主起動副主席](../Page/澳門民主起動.md "wikilink")[利建潤則以個人身份參與](../Page/利建潤.md "wikilink")。

### 遊行訴求

是次遊行訴求多元混雜，當中主要有：\[3\]

  - 爭取在2019年落實[雙普選](../Page/雙普選.md "wikilink")，並反對[《澳門基本法》第廿三條立法](../Page/澳門基本法第二十三條.md "wikilink")。
  - 要求政府以更嚴厲的力度打擊貪污與官商勾結、利益輸送，並公開賣地資料；賤價批地予以大財團用作商業項目，官員從中污取利，強調[歐文龍事件只屬冰山一角](../Page/歐文龍.md "wikilink")，並以此要求[中國國務院罷免](../Page/中華人民共和國國務院.md "wikilink")[何厚鏵的行政長官職位及其他司局級官員如打擊黑工不力的](../Page/何厚鏵.md "wikilink")[勞工暨就業局局長](../Page/勞工暨就業局.md "wikilink")[孫家雄等](../Page/孫家雄.md "wikilink")。
  - 要求澳門特區政府儘快出台政策，解決僱主只聘請黑工與輸入外勞政策偏重商家，變相排斥本地工人的問題，改善澳門裝修與建築工人失業與工資較澳門入息中位數偏低情況，另外增加對外輸入外勞的公眾知情透明度。
  - 要求政府重視在職貧窮、澳門本土工人開工不足。
  - 不滿炒風熾熱，樓價飇升，小市民無法置業安居；另外住屋租金亦隨之飇升之同時，居屋政策卻無助市民，要求增建公共房屋、社會房屋。
  - 不滿[終審法院法官漠視](../Page/澳門終審法院.md "wikilink")《[勞工法](../Page/勞工法_\(澳門\).md "wikilink")》，使[澳門旅遊娛樂有限公司前員工](../Page/澳門旅遊娛樂有限公司.md "wikilink")（現時已過檔至[澳門博彩股份有限公司](../Page/澳門博彩股份有限公司.md "wikilink")）在[澳娛茶資事件上無法獲得賠償](../Page/澳娛茶資事件.md "wikilink")。\[4\]
  - 不滿在[東望洋山及周邊地區興建超高樓宇](../Page/東望洋山.md "wikilink")，影響世遺景觀。

### 遊行路線

主辦組織在12月9日舉行記者招待會，當時預計遊行當天下午一時半會在[祐漢公園集合](../Page/祐漢公園.md "wikilink")，舉行一場名為「民主講場」的演講會。然後在下午三時出發，經[美副將大馬路](../Page/美副將大馬路.md "wikilink")、[水坑尾街](../Page/水坑尾街.md "wikilink")，遊行到[澳門政府總部](../Page/澳門政府總部.md "wikilink")，並在[南灣湖水上活動中心主入口處舉行民主遊行歷史印證活動](../Page/南灣湖水上活動中心.md "wikilink")。並表示整項活動已知會[民政總署](../Page/民政總署.md "wikilink")。\[5\]

[MacauLaoHonPark1.jpg](https://zh.wikipedia.org/wiki/File:MacauLaoHonPark1.jpg "fig:MacauLaoHonPark1.jpg")
12月10日，[澳門北區社團活動慶祝籌備委員會派代表到](../Page/澳門北區社團活動慶祝籌備委員會.md "wikilink")[民政總署要求澄清場地借用問題](../Page/民政總署.md "wikilink")，事因該社團在11月初已獲民署批准在12月20日借出[祐漢公園舉行名為](../Page/祐漢公園.md "wikilink")「萬衆歡騰慶回歸八周年」的慶祝活動。[民政總署管理委員會主席](../Page/民政總署管理委員會.md "wikilink")[譚偉文表示](../Page/譚偉文.md "wikilink")，申請借用場地採用[先到先得的原則](../Page/先到先得.md "wikilink")，由於該社團早已獲得批准，故12月20日當日在祐漢公園的使用權屬於該社團，並保證該慶祝活動能順利進行。\[6\]

遊行負責人[吳國昌則表示已在舉行前一個月去信知會民政總署](../Page/吳國昌.md "wikilink")，按法例只需知會當局而無需得到批准，強調會依法集會。但民政總署在12月10日晚上才通知該學社，表示12月20日不能借出[祐漢公園和](../Page/祐漢公園.md "wikilink")[南灣湖水上活動中心](../Page/南灣湖水上活動中心.md "wikilink")（因[體育發展局在當日有活動](../Page/體育發展局.md "wikilink")）。[吳國昌表示會爭取按原定計劃遊行](../Page/吳國昌.md "wikilink")，並就借用問題理性地斡旋，希望能分時段共用場地。\[7\]

[澳門北區社團活動慶祝籌備委員會表示不可能一同舉行活動](../Page/澳門北區社團活動慶祝籌備委員會.md "wikilink")，因為兩個活動理念和宗旨根本不同，在同一地點和時間舉行是「妄想」；而每年慶祝活動結束後都會立即申請下一年度在相同地方舉行活動。\[8\]

[Macau20071220green.jpg](https://zh.wikipedia.org/wiki/File:Macau20071220green.jpg "fig:Macau20071220green.jpg")最後在12月17日，遊行團體與北區社團和警方磋商後，遊行團體決定把出發地點改為[三角花園集合](../Page/三角花園.md "wikilink")，並依原有路線，而維持在[南灣湖水上活動中心作為終點](../Page/南灣湖水上活動中心.md "wikilink")，同時亦準備了一萬條綠絲帶向在場人士派發，但[吳國昌則表示並不是估計會有一萬人參與](../Page/吳國昌.md "wikilink")。\[9\]

### 12月16日遊行

12月11日，[澳門民生協進會及](../Page/澳門民生協進會.md "wikilink")[澳門民主起動公佈會於](../Page/澳門民主起動.md "wikilink")12月16日發起另一個集會遊行。負責人表示，由於過往多次遊行未能引起政府對訴求的回應，故再度發起遊行。而遊行訴求主要有：

  - 要求政府維護本地工人的權益。
  - 包括迅速削減外勞及嚴厲打擊黑工。
  - 提議用徵收外勞費方式保障本地工人權益及就業機會。
  - 打擊貪污腐敗及官商勾結。
  - 儘快落實將發放養老金年限下調至六十歲。

[澳門民主起動則指出是要延續](../Page/澳門民主起動.md "wikilink")9月30日的[電單車慢駛遊行](../Page/道路交通法_\(澳門\)#反法例遊行.md "wikilink")，以表達駕駛者對10月1日實施的《[道路交通法](../Page/道路交通法_\(澳門\).md "wikilink")》的不滿，希望當局清晰回應新舊罰單處理問題。但強調並非要與民生協進會聯合，因為兩者各有不同的訴求。

而團體預計12月16日下午二時正在[三角花園出發](../Page/三角花園.md "wikilink")，經[提督馬路](../Page/提督馬路.md "wikilink")、[沙梨頭海邊街](../Page/沙梨頭海邊街.md "wikilink")、[巴素打爾古街](../Page/巴素打爾古街.md "wikilink")、[新馬路](../Page/新馬路.md "wikilink")、[南灣大馬路](../Page/南灣大馬路.md "wikilink")，最後到[政府總部遞信](../Page/澳門政府總部.md "wikilink")。\[10\]

但在12月14日，[治安警察局以遊行所經的地點對交通以及民生均會構成重大的影響為由](../Page/治安警察局.md "wikilink")，根據《基本法》及按《集會權及示威權》（第2/93/M號法律）中第八條第二、三款規定，更改[澳門民生協進會的遊行路線為](../Page/澳門民生協進會.md "wikilink")：從[三角花園出發](../Page/三角花園.md "wikilink")，經[拱形馬路](../Page/拱形馬路.md "wikilink")、[提督馬路](../Page/提督馬路.md "wikilink")、[美副將大馬路](../Page/美副將大馬路.md "wikilink")、[俾利喇街](../Page/俾利喇街.md "wikilink")、[高士德大馬路](../Page/高士德大馬路.md "wikilink")、[士多紐拜斯大馬路](../Page/士多紐拜斯大馬路.md "wikilink")、[東望洋街](../Page/東望洋街.md "wikilink")、[水坑尾街](../Page/水坑尾街.md "wikilink")、[約翰四世大馬路](../Page/約翰四世大馬路.md "wikilink")、[蘇亞利斯博士大馬路](../Page/蘇亞利斯博士大馬路.md "wikilink")、[區華利前地](../Page/區華利前地.md "wikilink")、[南灣湖景大馬路](../Page/南灣湖景大馬路.md "wikilink")、政府總部\[11\]。而對[澳門民主起動的遊行路線則改為在](../Page/澳門民主起動.md "wikilink")[黑沙環公園出發](../Page/黑沙環公園.md "wikilink")，經[勞動節街](../Page/勞動節街.md "wikilink")、[馬場海邊馬路再到三角花園與](../Page/馬場海邊馬路.md "wikilink")[澳門民生協進會會合](../Page/澳門民生協進會.md "wikilink")\[12\]。

12月16日，兩個團體在下午二時半在三角花園出發\[13\]，至[提督馬路和](../Page/提督馬路.md "wikilink")[美副將大馬路交界](../Page/美副將大馬路.md "wikilink")（即[2007年澳門勞動節遊行時發生開槍事件的地點](../Page/2007年澳門勞動節遊行.md "wikilink")）時曾一度與警方對峙約十分鐘，後來仍按照警方修改的路線，經[美副將大馬路](../Page/美副將大馬路.md "wikilink")、[高士德大馬路](../Page/高士德大馬路.md "wikilink")、[士多紐拜斯大馬路等街道遊行至](../Page/士多紐拜斯大馬路.md "wikilink")[水坑尾街](../Page/水坑尾街.md "wikilink")，但期間警員不准居民加入遊行隊伍，引起遊行團體不滿，並對警員多次投訴，且一度要求與警方的現場指揮官對話，更有人高呼「[李小平無恥](../Page/李小平.md "wikilink")」等口號。最後遊行團體在[水坑尾](../Page/水坑尾.md "wikilink")[公共行政大樓前當眾燒燬請願信](../Page/公共行政大樓.md "wikilink")，遊行隨即提前終止，全部遊行人士及[電單車均自行和平散去](../Page/電單車.md "wikilink")。

據[治安警察局消息](../Page/治安警察局.md "wikilink")，這次遊行約有150人及30輛電單車參與。而局方共派出100名警員到場維持秩序及200名警員戒備，而秩序大致良好。\[14\]\[15\]

[利建潤表示](../Page/利建潤.md "wikilink")，燒燬信件提前結束遊行是不滿警方阻撓市民加入遊行，更認為「特區政府不想聽取市民的聲音」。\[16\]

## 遊行當日

[Macau20071220truck.jpg](https://zh.wikipedia.org/wiki/File:Macau20071220truck.jpg "fig:Macau20071220truck.jpg")\]\]
2007年12月20日（[澳門回歸八週年紀念當日](../Page/澳門回歸.md "wikilink")）下午一時半遊行團體在[三角花園集合](../Page/三角花園.md "wikilink")，期間舉行一場名為「民主講場」的演講會，同時亦準備了一萬條綠絲帶向在場人士派發。至下午三時正式出發，途經[拱形馬路](../Page/拱形馬路.md "wikilink")、[提督馬路](../Page/提督馬路.md "wikilink")、[美副將大馬路](../Page/美副將大馬路.md "wikilink")、[俾利喇街](../Page/俾利喇街.md "wikilink")、[高士德大馬路](../Page/高士德大馬路.md "wikilink")、[士多紐拜斯大馬路](../Page/士多紐拜斯大馬路.md "wikilink")，沿途高喊主題口號，並揮舞綠絲帶，至遊行人士途經[塔石廣場時](../Page/塔石廣場.md "wikilink")，恰巧遇上在此舉行的「慶祝回歸八週年馬拉松演唱會」，遊行人士即對其高呼喝倒-{采}-聲。

[Macau20071220collection.jpg](https://zh.wikipedia.org/wiki/File:Macau20071220collection.jpg "fig:Macau20071220collection.jpg")
其後隊伍再沿[東望洋街](../Page/東望洋街.md "wikilink")、[水坑尾街](../Page/水坑尾街.md "wikilink")、[約翰四世大馬路](../Page/約翰四世大馬路.md "wikilink")，至[友誼廣場警方曾以讓路人過馬路為由截龍](../Page/友誼廣場.md "wikilink")，令遊行人士一度鼓躁；後來隊伍再經[蘇亞利斯博士大馬路](../Page/蘇亞利斯博士大馬路.md "wikilink")、[區華利前地](../Page/區華利前地.md "wikilink")、[南灣湖景大馬路](../Page/南灣湖景大馬路.md "wikilink")，到政府總部遞交請願信。之後到達[南灣湖水上活動中心主入口處舉行民主遊行歷史印證活動](../Page/南灣湖水上活動中心.md "wikilink")，遊行人士紛紛在活動中心欄杆上繫上綠絲帶，並簽名表達訴求，同時進行「歷史印記物品收集」，收集在遊行期間用過的物品和橫額；至六時遊行人士已基本散去。\[17\]\[18\]

在是次遊行中，參與人士亦有青少年、教師、學生、社工、甚至有家庭，訴求原因主要都是擔心人才大量流向[博彩業和物價持續上漲等問題](../Page/澳門博彩業.md "wikilink")\[19\]。現場人士目測約有二千人參與是次遊行，[吳國昌聲稱約有七千人](../Page/吳國昌.md "wikilink")，而警方則估計有約1500人。現場遊行人士表示，參與遊行主要是表達民生訴求，而民主訴求則次之\[20\]。是次亦只有一輛[-{zh-hans:摩托车;zh-hk:電單車;zh-tw:摩托車;}-參與](../Page/電單車.md "wikilink")。

## 各方回應

對於首次有遊行要求[雙普選](../Page/雙普選.md "wikilink")，[何鴻燊表示](../Page/何鴻燊.md "wikilink")[澳門已有足夠](../Page/澳門.md "wikilink")[民主](../Page/民主.md "wikilink")，認為澳門特區政府已依照《[澳門基本法](../Page/澳門基本法.md "wikilink")》施政，無需要跟[香港一樣搞民主普選遊行](../Page/香港.md "wikilink")。並表示不支持是次民主遊行活動。\[21\]

[中聯辦主任](../Page/中聯辦.md "wikilink")[白志健表示](../Page/白志健.md "wikilink")，居民是有表達訴求的自由，選擇以遊行方式表達訴求是正常方法。[外交部駐澳特派員](../Page/中華人民共和國外交部.md "wikilink")[萬永祥則認為澳門居民應珍惜現有的大好形勢](../Page/萬永祥.md "wikilink")，並把握機會繼續發展。\[22\]

[全國人大常委會澳門基本法委員會委員](../Page/全國人大常委會.md "wikilink")[王振民在](../Page/王振民.md "wikilink")12月14日的[《澳門基本法》專題研究講座指出](../Page/《澳門基本法》專題研究講座.md "wikilink")，澳門沒有[雙普選是中央充分考慮澳門實際情況](../Page/雙普選.md "wikilink")，而澳門的政治體制已進入常態，原則上是應保持不變的；但如果需要變動，也只可以作出微調\[23\]；他又表示「澳門的迫切問題顯然不是缺乏民主，而是民主的質量需要大大提高」\[24\]。而[國務院](../Page/中華人民共和國國務院.md "wikilink")[港澳辦前副主任](../Page/港澳辦.md "wikilink")[陳滋英則認為沒有必要把雙普選編入](../Page/陳滋英.md "wikilink")《澳門基本法》\[25\]。

而早在11月13日特區行政長官[何厚鏵則在](../Page/何厚鏵.md "wikilink")08年度施政報告的記者會表示過，各界要深刻研究港澳兩地的基本法寫上政制發展「循序漸進最終達至全面普選」論述的原意；而現行三部《[選民登記法](../Page/選民登記法.md "wikilink")》、《[立法會選舉法](../Page/立法會選舉法.md "wikilink")》和《[行政長官選舉法](../Page/行政長官選舉法.md "wikilink")》法律文件的檢討方案，會於2007年底前對公眾諮詢（但在施政報吿內未有提及三份法案的檢討）；並認為社會有許多意見，部分也是值得參考的。但他重申在2009年澳門沒有條件落實[雙普選](../Page/雙普選.md "wikilink")，而2009年後的民主進程，社會討論始終都要根據《[澳門基本法](../Page/澳門基本法.md "wikilink")》，而《澳門基本法》沒有列明「循序漸進最終達至全面普選」是考慮到澳門整體社會發展的需要，並不是「漏寫」\[26\]。另外，[何厚鏵表示](../Page/何厚鏵.md "wikilink")，會在2009年任期屆滿前完成基本法第二十三條的立法\[27\]。

## 後續跟進

12月23日，[新澳門學社就是次遊行發表報告書](../Page/新澳門學社.md "wikilink")，指出遊行已基本表達民主訴求，亦體現了社會走向多元開放的前景，同時感謝參與遊行的人士，並表示在遊行的組織處理均有需要改善的地方。\[28\]\[29\]

1月2日下午，新澳門學社成員包括[吳國昌](../Page/吳國昌.md "wikilink")、[區錦新](../Page/區錦新.md "wikilink")、[陳偉智等人到](../Page/陳偉智.md "wikilink")[政府總部遞信](../Page/澳門政府總部.md "wikilink")，要求政府回應遊行訴求，並立即開展民主政制發展諮詢和要求將意見轉達至[中華人民共和國中央人民政府](../Page/中華人民共和國中央人民政府.md "wikilink")，以及要求約見各司司長。\[30\]

## 參考資料及註釋

## 相關條目

  - [2007年澳門勞動節遊行](../Page/2007年澳門勞動節遊行.md "wikilink")
  - [2007年澳門十一遊行](../Page/2007年澳門十一遊行.md "wikilink")
  - [香港七一遊行](../Page/香港七一遊行.md "wikilink")
  - [雙普選](../Page/雙普選.md "wikilink")
  - [新澳門學社](../Page/新澳門學社.md "wikilink")
  - [道路交通法 (澳門)](../Page/道路交通法_\(澳門\).md "wikilink")

## 外部連結

  - 《新澳門》第35期──「澳門的民主路」，《[澳門回歸民主大遊行
    (07)](http://newmacau.org/newmacauweb/35_p4.jpg)》
  - [民主昌在此 -
    民主回歸遊行芻議](http://blog.qooza.hk/newmacau?eid=7716520)，2007年12月3日
  - 遊行人士自拍片段：[1](http://hk.youtube.com/watch?v=WS8_ucStEbQ)、[2](http://hk.youtube.com/watch?v=ytBB7wFlot4)

[Category:澳門政治](../Category/澳門政治.md "wikilink")
[Category:澳門遊行](../Category/澳門遊行.md "wikilink")
[Category:2007年澳門](../Category/2007年澳門.md "wikilink")

1.  [明報](../Page/明報.md "wikilink") 2007年11月9日，《歐文龍案催化12月20日爭民主盼除官商勾結》

2.  [澳門日報](../Page/澳門日報.md "wikilink")
    2007年12月18日，《[警協表態不會上街遊行](http://www.macau.ctm.net/modailylog/20071218/big/gb-m16.htm)
    》

3.  [澳門日報](../Page/澳門日報.md "wikilink")
    2007年12月21日，《[千五人參與民主回歸遊行](http://www.macau.ctm.net/modailylog/20071221/big/gb-m15.htm)》

4.  [澳門日報](../Page/澳門日報.md "wikilink")
    2007年12月21日，《[澳娛茶資事件索賠者喊冤](http://www.macau.ctm.net/modailylog/20071221/big/gb-m42.htm)》

5.  [澳門日報](../Page/澳門日報.md "wikilink")
    2007年12月10日，《[三團體回歸日遊行](http://www.macau.ctm.net/modailylog/20071210/big/gb-m19.htm)》

6.  [澳門日報](../Page/澳門日報.md "wikilink")
    2007年12月11日，《[北區社團早已借用祐漢公園](http://www.macau.ctm.net/modailylog/20071211/big/gb-m14.htm)》

7.  [澳門日報](../Page/澳門日報.md "wikilink")
    2007年12月12日，《[團體爭取按原計劃遊行](http://www.macau.ctm.net/modailylog/20071212/big/gb-m53.htm)》

8.  [澳門電視台](../Page/澳門電視台.md "wikilink") 2007年12月10日 19:00
    澳視新聞，《兩社團稱有祐漢公園回歸日使用權》

9.  [澳門日報](../Page/澳門日報.md "wikilink")
    2007年12月18日，《[回歸日遊行三角花園出發](http://www.macau.ctm.net/modailylog/20071218/big/gb-m50.htm)
    》

10. [澳門日報](../Page/澳門日報.md "wikilink")
    2007年12月12日，《[兩團體周日遊行](http://www.macau.ctm.net/modailylog/20071212/big/gb-m51.htm)》

11. [澳門日報](../Page/澳門日報.md "wikilink")
    2007年12月15日，《[團體明遊行不經新馬路](http://www.macau.ctm.net/modailylog/20071215/big/gb-m68.htm)》

12. [澳門日報](../Page/澳門日報.md "wikilink")
    2007年12月16日，《[團體今慢駛遊行更改路線](http://www.macau.ctm.net/modailylog/20071216/big/gb-m51.htm)》

13. 註：[澳門民主起動並沒有依照修改路線在](../Page/澳門民主起動.md "wikilink")[黑沙環公園作為起點](../Page/黑沙環公園.md "wikilink")。

14. [澳門日報](../Page/澳門日報.md "wikilink")
    2007年12月17日，《[兩團體百人遊行提前終止](http://www.macau.ctm.net/modailylog/20071217/big/gb-m14.htm)》

15. [澳門電視台](../Page/澳門電視台.md "wikilink") 2007年12月16日 19:00 澳視新聞

16.
17.
18. [澳門電視台](../Page/澳門電視台.md "wikilink") 2007年12月20日 19:00 澳視新聞

19. [澳門日報](../Page/澳門日報.md "wikilink")
    2007年12月21日，《[「遊行新人類」提出不同訴求](http://www.macau.ctm.net/modailylog/20071221/big/gb-m41.htm)》

20.
21. [澳門日報](../Page/澳門日報.md "wikilink")
    2007年12月20日，《[何鴻燊：澳無須學港搞民主遊行](http://www.macau.ctm.net/modailylog/20071220/big/gb-m76.htm)》

22. [澳門日報](../Page/澳門日報.md "wikilink")
    2007年12月20日，《[白志健：澳門人有表達訴求自由](http://www.macau.ctm.net/modailylog/20071220/big/gb-m77.htm)》

23. [澳門日報](../Page/澳門日報.md "wikilink")
    2007年12月20日，《[王振民：澳政制發展入常態](http://www.macau.ctm.net/modailylog/20071215/big/gb-m14.htm)》

24. [澳門日報](../Page/澳門日報.md "wikilink")
    2007年12月20日，《[澳不缺民主惟須提高素質](http://www.macau.ctm.net/modailylog/20071215/big/gb-m64.htm)》

25. [澳門日報](../Page/澳門日報.md "wikilink")
    2007年12月20日，《[澳無雙普選利穩定發展](http://www.macau.ctm.net/modailylog/20071215/big/gb-m11.htm)》

26. [澳門日報](../Page/澳門日報.md "wikilink") 2007年11月14日，《特首：政制發展港澳不同》

27. [澳門日報](../Page/澳門日報.md "wikilink") 2007年11月14日，《特首：任滿前完成廿三條立法》

28. [澳門日報](../Page/澳門日報.md "wikilink")
    2007年12月24日，《[新澳門學社指遊行基本反映訴求](http://www.macau.ctm.net/modailylog/20071224/big/gb-m45.htm)》

29. [反貪腐、爭民主、保民生，民主回歸大遊行 -
    跟進工作告市民書](http://blog.qooza.hk/newmacau?eid=7896617&bpage=2)

30. [華僑報](../Page/華僑報.md "wikilink")
    2008年1月3日，《[新澳門學社遞信要求約見各司長深化官民互動](http://www.vakiodaily.com/index.php?tn=viewer&ncid=1&nid=127405&dt=20080103&lang=tw)》