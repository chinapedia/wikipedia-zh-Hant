## 1月31日

  - [非洲联盟第十届首脑会议在](../Page/非洲联盟.md "wikilink")[埃塞俄比亚首都](../Page/埃塞俄比亚.md "wikilink")[亚的斯亚贝巴开幕](../Page/亚的斯亚贝巴.md "wikilink")，本届会议的主题是“非洲工业发展”，与会者将重点讨论非洲[工业化和](../Page/工业.md "wikilink")[经济合作问题](../Page/经济.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/31/content_7535167.htm)
  - [英国运动服装生产商](../Page/英国.md "wikilink")[Umbro公司宣布](../Page/Umbro.md "wikilink")，其[股东已经同意](../Page/股东.md "wikilink")[耐克公司提出的](../Page/耐克.md "wikilink")2.85亿[英镑的全](../Page/英镑.md "wikilink")[现金收购要约](../Page/现金.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-02/01/content_7540928.htm)
  - [乍得总统](../Page/乍得.md "wikilink")[代比亲率](../Page/伊德里斯·代比.md "wikilink")[军队在首都](../Page/军队.md "wikilink")[恩贾梅纳周边完成部署](../Page/恩贾梅纳.md "wikilink")，准备阻挡反政府武装向首都推进。[新华网](http://news.xinhuanet.com/mil/2008-02/01/content_7541744.htm)
  - 据[中国民政部透露](../Page/中华人民共和国民政部.md "wikilink")，自1月10日至31日的[雪灾已使](../Page/2008年中国雪灾.md "wikilink")[湖南](../Page/湖南.md "wikilink")、[湖北](../Page/湖北.md "wikilink")、[贵州等](../Page/贵州.md "wikilink")19个[省级行政区受灾](../Page/省级行政区.md "wikilink")，60人死亡，2人失踪，175.9万人紧急转移，直接经济损失537.9亿元[人民币](../Page/人民币.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-02/01/content_7545782.htm)

## 1月30日

  - [中國](../Page/中華人民共和國.md "wikilink")[民政部透露](../Page/中华人民共和国民政部.md "wikilink")，自10日以来的[華中華東雪災已使](../Page/2008年中国雪灾.md "wikilink")[湖南](../Page/湖南.md "wikilink")、[湖北](../Page/湖北.md "wikilink")、[贵州](../Page/贵州.md "wikilink")、[广西](../Page/广西.md "wikilink")、[江西](../Page/江西.md "wikilink")、[安徽等](../Page/安徽.md "wikilink")17個[省级行政区及](../Page/省级行政区.md "wikilink")[新疆建設兵團受災](../Page/新疆建設兵團.md "wikilink")，38人死亡，161.7[萬人緊急轉移](../Page/萬.md "wikilink")，直接经济损失326.7[亿](../Page/亿.md "wikilink")[元](../Page/人民币.md "wikilink")。[廣州地區滯留旅客近](../Page/廣州.md "wikilink")80萬人，[滬](../Page/上海.md "wikilink")[寧](../Page/南京.md "wikilink")[杭地區滯留旅客逾](../Page/杭州.md "wikilink")10萬人。[新華網](http://news.xinhuanet.com/newscenter/2008-01/30/content_7528124.htm)
    [新華網](http://news.xinhuanet.com/newscenter/2008-01/30/content_7528098.htm)
  - [日本各媒體報導](../Page/日本.md "wikilink")，至少有10名日本民眾在吃下[中國生產的同一批冷凍](../Page/中國.md "wikilink")[煎餃後](../Page/煎餃.md "wikilink")，因[食物中毒症狀送醫急救](../Page/食物中毒.md "wikilink")。[自由電子報](https://web.archive.org/web/20080203115008/http://www.libertytimes.com.tw/2008/new/jan/31/today-t1.htm)[中時電子報](http://news.chinatimes.com/2007Cti/2007Cti-Focus/2007Cti-Focus-Content/0,4518,9701310151+0+0+165009+0,00.html)[東森新聞報](http://www.ettoday.com/2008/01/31/162-2225818.htm)[自由電子報](https://web.archive.org/web/20080203141527/http://www.libertytimes.com.tw/2008/new/feb/1/today-fo6.htm)[中時電子報](https://web.archive.org/web/20080203140347/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110501+112008020100103,00.html)
  - [2008年美国总统选举](../Page/2008年美国总统选举.md "wikilink")：[佛罗里达州初选后](../Page/佛罗里达州.md "wikilink")，共和党的[鲁迪·朱利亚尼和民主党的](../Page/鲁迪·朱利亚尼.md "wikilink")[约翰·爱德华兹宣布退选](../Page/约翰·爱德华兹.md "wikilink")。前者转为支持[约翰·麦凯恩](../Page/约翰·麦凯恩.md "wikilink")。[CNN](http://www.cnn.com/video/#/video/politics/2008/01/30/sot.giuliani.briefing.cnn)[CNN](http://www.cnn.com/2008/POLITICS/01/30/edwards/index.html)
  - [瑞士医生联合会发表研究公报称](../Page/瑞士.md "wikilink")，感染[HIV病毒并得到](../Page/HIV病毒.md "wikilink")[抗逆转录病毒药物有效治疗者](../Page/抗逆转录病毒药物.md "wikilink")，即使不采取[保护措施](../Page/安全性行為.md "wikilink")，也不会通过通常的性传播途径[传染病毒](../Page/传染.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/31/content_7533859.htm)
  - [美国联邦储备委员会再度降息](../Page/美国联邦储备委员会.md "wikilink")50个基点，[联邦基金利率降至](../Page/联邦基金利率.md "wikilink")3%。美联储在9天之内两度降息，幅度达125个基点。[新华网](http://news.xinhuanet.com/newscenter/2008-01/31/content_7532998.htm)
  - [地中海两条](../Page/地中海.md "wikilink")[海底电缆受损](../Page/海底电缆.md "wikilink")，造成[南亚和](../Page/南亚.md "wikilink")[中东地区的网络通信几乎瘫痪](../Page/中东.md "wikilink")。[BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_7220000/newsid_7220200/7220257.stm)
    [联合新闻网](http://gb.udn.com/gb/udn.com/NEWS/WORLD/WOR3/4205261.shtml)

## 1月29日

  - [人民力量党主席](../Page/人民力量党.md "wikilink")[沙马·顺达卫在](../Page/沙马·顺达卫.md "wikilink")[曼谷接受](../Page/曼谷.md "wikilink")[泰国国王](../Page/泰国国王.md "wikilink")[普密蓬·阿杜德签署的御令](../Page/普密蓬·阿杜德.md "wikilink")，正式成为[泰国总理](../Page/泰国总理.md "wikilink")。[新华网](http://news.xinhuanet.com/photo/2008-01/29/content_7522009.htm)　[新华网](http://news.xinhuanet.com/newscenter/2008-01/29/content_7516799.htm)
  - [澳門發生歷來最大規模的](../Page/澳門.md "wikilink")[天線訊號中斷事件](../Page/2008年澳門公共天線訊號中斷事件.md "wikilink")。[澳门电信管理局](https://web.archive.org/web/20080827201751/http://www.dsrt.gov.mo/chi/News/special/PressReleaseServiceSuspension.html)

## 1月28日

  - [中華民國在](../Page/中華民國.md "wikilink")[南沙群岛的](../Page/南沙群岛.md "wikilink")[太平岛已经完成了一个长](../Page/太平岛.md "wikilink")1,150米的飞机跑道。[BBC](http://newsvote.bbc.co.uk/chinese/simp/hi/newsid_7210000/newsid_7213300/7213307.stm)
  - [中國](../Page/中國.md "wikilink")[長江流域持續出現](../Page/長江.md "wikilink")[特大風雪](../Page/2008年中国雪灾.md "wikilink")，[安徽](../Page/安徽.md "wikilink")、[江西](../Page/江西.md "wikilink")、[河南](../Page/河南.md "wikilink")、[湖南](../Page/湖南.md "wikilink")、[湖北](../Page/湖北.md "wikilink")、[貴州](../Page/貴州.md "wikilink")、[陝西等](../Page/陝西.md "wikilink")14個省同時受災，是[1954年後以來最嚴重的雪災](../Page/1954年.md "wikilink")。据中国民政部提供的数据，共有7786.2万人受灾，因灾死亡24人，因灾直接经济损失220.9亿元。第三輪風雪導致各地高速公路、機場路面結冰及大量積雪，陸空交通癱瘓。15萬旅客滯留在[廣州火車站](../Page/廣州火車站.md "wikilink")，[白雲機場有大約一百班飛機取消](../Page/白雲機場.md "wikilink")。由於京廣及京九鐵路的供電網受到風雪摧毀，加上煤的運輸受阻，鐵路電力供應中斷，大量旅客被困在湖南鐵路上。由於糧食運輸停頓，各地包括[香港的豬肉蔬菜價格飆升](../Page/香港.md "wikilink")。[星島日報](https://web.archive.org/web/20080129160538/http://hk.news.yahoo.com/080126/60/2nwg5.html)[星島日報](https://web.archive.org/web/20080130102338/http://hk.news.yahoo.com/080127/60/2nx2s.html)[明報](https://web.archive.org/web/20080131153917/http://hk.news.yahoo.com/080128/12/2nyuu.html)[明報](https://web.archive.org/web/20080130093919/http://hk.news.yahoo.com/080126/12/2nwc5.html)[明報](https://web.archive.org/web/20080129084144/http://hk.news.yahoo.com/080127/12/2nwnb.html)[明報](https://web.archive.org/web/20080130141313/http://hk.news.yahoo.com/080127/12/2nwst.html)[新华网](http://news.xinhuanet.com/politics/2008-01/29/content_7513734.htm)
  - [泰国](../Page/泰国.md "wikilink")[国会](../Page/国会.md "wikilink")[下议院举行](../Page/下议院.md "wikilink")[首相人选表决会议](../Page/泰国首相.md "wikilink")，[人民力量党主席](../Page/人民力量党.md "wikilink")[沙马·顺达卫当选泰国新首相](../Page/沙马·顺达卫.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/28/content_7511324.htm)
  - [韩国当选](../Page/韩国.md "wikilink")[总统](../Page/韩国总统.md "wikilink")[李明博提名](../Page/李明博.md "wikilink")[联合国秘书长](../Page/联合国秘书长.md "wikilink")[气候变化问题特使](../Page/全球变暖.md "wikilink")[韩升洙为下届政府](../Page/韩升洙.md "wikilink")[总理](../Page/韩国总理.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/28/content_7510944.htm)
  - [诺基亚宣布以](../Page/诺基亚.md "wikilink")8.43亿[挪威克朗](../Page/挪威克朗.md "wikilink")（约合1.53亿美元）现金收购挪威的上市软件公司[Trolltech](../Page/Trolltech.md "wikilink")，[Qt的开发商](../Page/Qt.md "wikilink")。[网易](http://tech.163.com/08/0128/17/43AEIPDA000915BE.html)
  - [2008年北京奥运会主要场馆之一的国家游泳中心](../Page/2008年北京奥运会.md "wikilink")“[水立方](../Page/水立方.md "wikilink")”竣工并交付使用。[新浪网](http://2008.sina.com.cn/jz/other/p/2008-01-28/205146643.shtml)

## 1月27日

  - 2008年[世界经济论坛年會在](../Page/世界经济论坛.md "wikilink")[瑞士](../Page/瑞士.md "wikilink")[达沃斯閉幕](../Page/达沃斯.md "wikilink")，与会者对[美国以及全球](../Page/美国.md "wikilink")[经济前景感到担忧](../Page/经济.md "wikilink")，[工商](../Page/工商.md "wikilink")、[政府](../Page/政府.md "wikilink")、[社會各界領袖呼籲合作創新](../Page/社會.md "wikilink")，應對包括[貧困](../Page/貧困.md "wikilink")、[全球變暖](../Page/全球變暖.md "wikilink")、[巴以衝突等在內的各項](../Page/巴以衝突.md "wikilink")[全球化挑戰](../Page/全球化.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/27/content_7507785.htm)
    [世界經濟論壇](https://web.archive.org/web/20080130050713/http://www.weforum.org/en/media/Latest%20Press%20Releases/PR_closing_270108)
  - [以色列](../Page/以色列.md "wikilink")[总理](../Page/以色列总理.md "wikilink")[艾胡德·奧爾默特和](../Page/艾胡德·奧爾默特.md "wikilink")[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")[民族权力机构主席](../Page/巴勒斯坦民族权力机构主席.md "wikilink")[阿巴斯在](../Page/阿巴斯.md "wikilink")[耶路撒冷就](../Page/耶路撒冷.md "wikilink")[加沙地带与](../Page/加沙地带.md "wikilink")[埃及的边境局势举行会谈](../Page/埃及.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/27/content_7508004.htm)
  - [中国](../Page/中国.md "wikilink")[中央气象台发布](../Page/中央气象台.md "wikilink")[暴雪红色警报](../Page/雪.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/27/content_7508258.htm)
  - [塞爾維亞选手](../Page/塞爾維亞.md "wikilink")[德约科维奇获得](../Page/諾瓦克·德约科维奇.md "wikilink")[2008年澳洲網球公開賽男单冠军](../Page/2008年澳洲網球公開賽.md "wikilink")。[新华网](http://news.xinhuanet.com/sports/2008-01/28/content_7508705.htm)

## 1月25日

  - [世界经济论坛在](../Page/世界经济论坛.md "wikilink")[瑞士](../Page/瑞士.md "wikilink")[达沃斯将](../Page/达沃斯.md "wikilink")2008年度[水晶奖授予](../Page/水晶奖.md "wikilink")[英国](../Page/英国.md "wikilink")[电影](../Page/电影.md "wikilink")[演员](../Page/演员.md "wikilink")[埃玛·汤姆森和](../Page/埃玛·汤姆森.md "wikilink")[美国](../Page/美国.md "wikilink")[华裔](../Page/华裔.md "wikilink")[大提琴演奏家](../Page/大提琴.md "wikilink")[马友友](../Page/马友友.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/27/content_7500876.htm)
  - [俄罗斯选手](../Page/俄罗斯.md "wikilink")[莎拉波娃获得](../Page/玛丽亚·莎拉波娃.md "wikilink")[2008年澳洲網球公開賽女单冠军](../Page/2008年澳洲網球公開賽.md "wikilink")。[东方网](http://photo.eastday.com/m/20080127/u1a3376313.html)

## 1月24日

  - [英國就業及退休保障大臣](../Page/英國.md "wikilink")-{zh:[韓培德](../Page/韓培德.md "wikilink");
    zh-hans:\[\[韓培德|zh-hk:[韓培德](../Page/韓培德.md "wikilink");zh-tw:[彼得·海恩](../Page/韓培德.md "wikilink");}-因政治捐獻醜聞辭職，成為英揆-{zh:[白高敦](../Page/白高敦.md "wikilink");
    zh-hans:\[\[白高敦|zh-hk:[白高敦](../Page/白高敦.md "wikilink");zh-tw:[布朗](../Page/白高敦.md "wikilink");}-自[2007年](../Page/2007年.md "wikilink")[6月上任以來首位辭職的內閣大臣](../Page/6月.md "wikilink")。[有線電視新聞](http://inews.i-cable.com/webapps/news_detail.php?id=259746&category=3)、[大紀元](http://www.epochtimes.com/b5/8/1/24/n1989608.htm)
  - [联合国人权理事会针对](../Page/联合国人权理事会.md "wikilink")[巴勒斯坦局势召开特别会议](../Page/巴勒斯坦.md "wikilink")，通过决议要求[以色列立即解除对](../Page/以色列.md "wikilink")[加沙的封锁](../Page/加沙.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/24/content_7490679.htm)
  - [意大利](../Page/意大利.md "wikilink")[总理](../Page/意大利总理.md "wikilink")[普罗迪由于其领导的政府未能赢得在](../Page/罗马诺·普罗迪.md "wikilink")[参议院的信任投票](../Page/参议院.md "wikilink")，而向[总统](../Page/意大利总统.md "wikilink")[纳波利塔诺递交辞呈](../Page/乔治·纳波利塔诺.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/25/content_7491077.htm)
  - [法国兴业银行宣布](../Page/法国兴业银行.md "wikilink")，该银行一名[期货交易员在过去近一年时间里未经授权利用银行资金进行非法交易](../Page/期货.md "wikilink")，给银行造成了49亿[欧元的损失](../Page/欧元.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/25/content_7494020.htm)
  - 在前[联合国秘书长](../Page/联合国秘书长.md "wikilink")[安南的斡旋下](../Page/科菲·安南.md "wikilink")，[肯尼亚](../Page/肯尼亚.md "wikilink")[总统](../Page/总统.md "wikilink")[齐贝吉和](../Page/姆瓦伊·齐贝吉.md "wikilink")[反对党领导人](../Page/反对党.md "wikilink")[奥廷加在总统府进行了自](../Page/拉伊拉·奥廷加.md "wikilink")[肯尼亚骚乱以来的首次会谈](../Page/肯亞騷亂_\(2007年－2008年\).md "wikilink")。[德国之声中文网](http://www.dw-world.de/dw/article/0,2144,3088598,00.html)
  - [美国](../Page/美国.md "wikilink")[克雷格·文特尔研究所的一项研究成果在](../Page/克雷格·文特尔.md "wikilink")《[科学](../Page/科学_\(期刊\).md "wikilink")》杂志上发表，他们已成功制造出了人体生殖[支原体的完整](../Page/支原体.md "wikilink")[基因组](../Page/基因组.md "wikilink")，成为[合成生物学上的一项重大突破](../Page/合成生物学.md "wikilink")。[科技日报](https://web.archive.org/web/20080129095651/http://www.stdaily.com/gb/stdaily/2008-01/26/content_770591.htm)[联合新闻网](http://gb.udn.com/gb/udn.com/NEWS/WORLD/WOR3/4196733.shtml)

## 1月23日

  - 2008年[世界经济论坛年会在](../Page/世界经济论坛.md "wikilink")[瑞士](../Page/瑞士.md "wikilink")[达沃斯开幕](../Page/达沃斯.md "wikilink")，来自88个国家的2500多名与会者将通过235场专题研讨会。本届论坛年会的主题为“合作创新的力量”。[新华网](http://news.xinhuanet.com/world/2008-01/23/content_7475954.htm)
  - [刚果民主共和国政府和](../Page/刚果民主共和国.md "wikilink")[恩孔达率领的](../Page/劳伦特·恩孔达.md "wikilink")[图西族反政府武装在东部城市](../Page/图西族.md "wikilink")[戈马签署和平协议](../Page/戈马.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/24/content_7483026.htm)
  - [希腊总理](../Page/希腊.md "wikilink")[卡拉曼利斯开始对](../Page/卡拉曼利斯.md "wikilink")[土耳其进行为期](../Page/土耳其.md "wikilink")3天的正式访问，这是希腊总理近50年来首次正式访问土耳其。[新华网](http://news.xinhuanet.com/newscenter/2008-01/24/content_7486233.htm)

## 1月22日

  - [万维网联盟正式对外发布](../Page/万维网联盟.md "wikilink")[互联网](../Page/互联网.md "wikilink")[网页](../Page/网页.md "wikilink")[代码新标准](../Page/代码.md "wikilink")[HTML
    5的首个工作草案](../Page/HTML_5.md "wikilink")。[w3.org](http://www.w3.org/2008/02/html5-pressrelease)[计算机世界网](https://web.archive.org/web/20080131173110/http://news.ccw.com.cn/internet/htm2008/20080123_373300.shtml)
  - [伊拉克](../Page/伊拉克.md "wikilink")[议会通过一项有效期一年的修改](../Page/议会.md "wikilink")[国旗的临时法案](../Page/国旗.md "wikilink")，将原[伊拉克国旗上由前](../Page/伊拉克国旗.md "wikilink")[总统](../Page/伊拉克总统.md "wikilink")[萨达姆手书的](../Page/萨达姆·侯赛因.md "wikilink")“[真主至大](../Page/真主至大.md "wikilink")”字样换成印刷体，并去掉旗上代表[阿拉伯复兴社会党](../Page/阿拉伯复兴社会党.md "wikilink")“[统一](../Page/统一.md "wikilink")、[自由](../Page/自由.md "wikilink")、[社会主义](../Page/社会主义.md "wikilink")”口号的3颗绿色五角星。
    [新华网](http://news.xinhuanet.com/newscenter/2008-01/22/content_7474791.htm)
  - 面对经济衰退和[股市跌潮](../Page/2008年環球股災.md "wikilink")，[美国联邦储备局意外地一次降息](../Page/美国联邦储备局.md "wikilink")0.75%。[BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_7200000/newsid_7202600/7202683.stm)
  - 美、俄、中、英、法、德六国外长在德国外交部就[伊朗核问题举行会谈](../Page/伊朗核问题.md "wikilink")，并就伊核问题的安理会新草案要点达成一致。[新华网](http://news.xinhuanet.com/newscenter/2008-01/23/content_7475211.htm)
  - [中國国家文物局在](../Page/中國国家文物局.md "wikilink")[北京召開記者會宣布](../Page/北京.md "wikilink")：在[河南](../Page/河南.md "wikilink")[许昌灵井旧时器时代遗址](../Page/许昌.md "wikilink")[第四纪](../Page/第四纪.md "wikilink")[晚更新世早期地层中發現距今](../Page/晚更新世.md "wikilink")8万～10万年间的较完整的古人类[許昌人](../Page/許昌人.md "wikilink")[头盖骨化石](../Page/头盖骨.md "wikilink")。填补了中国现代人类起源中重要一环。[新華網](http://www.xinhuanet.com/chinanews/2008-01/23/content_12294724.htm)[新華網](https://web.archive.org/web/20140727041654/http://www.ha.xinhuanet.com/zfwq/2008-01/25/content_12319808.htm)

## 1月21日

  - 全球主要[股市出现](../Page/股市.md "wikilink")[暴跌](../Page/2008年環球股災.md "wikilink")，[伦敦股市](../Page/伦敦证券交易所.md "wikilink")[金融时报100指数](../Page/伦敦金融时报100指数.md "wikilink")、[巴黎股市](../Page/泛歐股票交易所.md "wikilink")[CAC40指数](../Page/巴黎CAC40指数.md "wikilink")、[法兰克福股市](../Page/法兰克福证券交易所.md "wikilink")[DAX指数](../Page/德国DAX指数.md "wikilink")、[新加坡股市](../Page/新加坡交易所.md "wikilink")[海峽時報指數](../Page/新加坡海峽時報指數.md "wikilink")、[香港股市](../Page/香港交易所.md "wikilink")[恒生指数](../Page/恒生指数.md "wikilink")、[上海股市](../Page/上海证券交易所.md "wikilink")[综合股价指数等](../Page/上海证券交易所综合股价指数.md "wikilink")[股市指数跌幅超过](../Page/股市指数.md "wikilink")5%。[新华网](http://news.xinhuanet.com/newscenter/2008-01/22/content_7472967.htm)[人民网](http://finance.people.com.cn/GB/6806239.html)

## 1月20日

  - 再次当选[格鲁吉亚总统的](../Page/格鲁吉亚总统.md "wikilink")[米哈伊爾·薩卡什維利在首都](../Page/米哈伊爾·薩卡什維利.md "wikilink")[第比利斯宣誓就职](../Page/第比利斯.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/20/content_7459160.htm)

<!-- end list -->

  - [塞尔维亚国家选举委员会公布的初步统计结果显示](../Page/塞尔维亚.md "wikilink")，[塞尔维亚激进党总统候选人](../Page/塞尔维亚激进党.md "wikilink")[尼科利奇在当天进行的总统选举中领先](../Page/托米斯拉夫·尼科利奇.md "wikilink")，将和得票率第二位的[民主党总统候选人](../Page/民主党_\(塞尔维亚\).md "wikilink")、现任总统[塔迪奇进入第二轮选举](../Page/鲍里斯·塔迪奇.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/21/content_7463075.htm)

## 1月19日

  - 以[人民力量党为首的](../Page/人民力量党.md "wikilink")6个[泰国](../Page/泰国.md "wikilink")[政党在](../Page/政党.md "wikilink")[曼谷正式宣布联合](../Page/曼谷.md "wikilink")[组阁](../Page/组阁.md "wikilink")，而新一届[国会](../Page/国会.md "wikilink")[下议院第二大党](../Page/下议院.md "wikilink")[民主党将成为下议院唯一的](../Page/民主党_\(泰国\).md "wikilink")[反对党](../Page/反对党.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/19/content_7452532.htm)
  - 反[全球化示威者在](../Page/全球化.md "wikilink")[瑞士首都](../Page/瑞士.md "wikilink")[伯恩与警方发生激烈冲突](../Page/伯恩.md "wikilink")，200多人被捕，这场示威主要针对23日将在[达沃斯举行的](../Page/达沃斯.md "wikilink")[世界经济论坛年会](../Page/世界经济论坛.md "wikilink")。[联合早报](https://archive.is/20130428171405/http://realtime.zaobao.com/2008/01/080120_10.shtml)

## 1月17日

  - 一架载有136名乘客及16名机组人员的英国航空038号班机\]\]在[英国](../Page/英国.md "wikilink")[希思罗机场降落时发生事故](../Page/希思罗机场.md "wikilink")。机上人员全部安全撤离，18人受轻伤被送往医院救治。[BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_7190000/newsid_7194100/7194137.stm)
  - 第五次[中美战略对话在](../Page/中美战略对话.md "wikilink")[贵阳开始](../Page/贵阳.md "wikilink")，由中国外交部副部长[戴秉国及美国副国务卿](../Page/戴秉国.md "wikilink")[内格罗蓬特主持](../Page/约翰·内格罗蓬特.md "wikilink")。中国军方首次参加对话。[BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_7190000/newsid_7193200/7193245.stm)
  - [瑞典皇家科学院宣布](../Page/瑞典皇家科学院.md "wikilink")，2008年[克拉福德奖授予](../Page/克拉福德奖.md "wikilink")[法国](../Page/法国.md "wikilink")[数学物理学家](../Page/数学物理学.md "wikilink")[马克西姆·孔采维奇](../Page/马克西姆·孔采维奇.md "wikilink")，美国数学物理学家[爱德华·威滕和](../Page/爱德华·威滕.md "wikilink")[俄罗斯](../Page/俄罗斯.md "wikilink")[天体物理学家](../Page/天体物理学.md "wikilink")[拉希德·辛亚耶夫三位科学家](../Page/拉希德·辛亚耶夫.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/17/content_7441211.htm)
  - [日本](../Page/日本.md "wikilink")[国际科学技术财团宣布](../Page/国际科学技术财团.md "wikilink")，2008年[日本国际奖授予美国](../Page/日本国际奖.md "wikilink")[计算机科学家](../Page/计算机科学家.md "wikilink")[文特·瑟夫和](../Page/文特·瑟夫.md "wikilink")[罗伯特·卡恩](../Page/罗伯特·卡恩.md "wikilink")，以及[遗传学家](../Page/遗传学家.md "wikilink")[维克托·麦库西克](../Page/维克托·麦库西克.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/17/content_7439669.htm)

## 1月16日

  - [美国](../Page/美国.md "wikilink")[Sun公司宣布](../Page/Sun公司.md "wikilink")，已经与瑞典[MySQL公司达成最终协议](../Page/MySQL.md "wikilink")，将以10亿美元收购这家[开放源代码的](../Page/开放源代码.md "wikilink")[数据库管理系统厂商](../Page/数据库管理系统.md "wikilink")。[新浪网](http://tech.sina.com.cn/it/2008-01-16/23341978404.shtml)
  - 十五日下午有兩名「海洋守護協會」抗議成員遭[日本](../Page/日本.md "wikilink")[捕鯨船](../Page/日本捕鯨業.md "wikilink")「勇新丸二號」扣留，[澳洲政府於同日依國內法勒令日方停止捕鯨活動](../Page/澳洲.md "wikilink")。[日本官房長官町村信孝在十六日記者會上表示願釋放兩人](../Page/日本官房長官.md "wikilink")，但譴責環保團體反捕鯨活動並指責澳洲干涉。[自由時報](http://tw.news.yahoo.com/article/url/d/a/080117/78/rzs4.html)
  - 美国[甲骨文公司宣布](../Page/甲骨文公司.md "wikilink")，已和[BEA公司达成协议](../Page/BEA.md "wikilink")，将以85亿美元收购这家[Java](../Page/Java.md "wikilink")[中间件软件公司](../Page/中间件.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/17/content_7438342.htm)

## 1月15日

  - 利用[細弱螺旋體](../Page/細弱螺旋體.md "wikilink")（*Treponema
    pertenue*）所進行的[遺傳學研究](../Page/遺傳學.md "wikilink")，發現了支持[梅毒是由](../Page/梅毒.md "wikilink")[哥倫布及其船員由](../Page/哥倫布.md "wikilink")[美洲帶往](../Page/美洲.md "wikilink")[歐洲之理論的新證據](../Page/歐洲.md "wikilink")。[新科學家](http://www.newscientist.com/channel/health/dn13186-columbus-blamed-for-spread-of-syphilis-.html?feedId=online-news_rss20)

## 1月14日

  - [非洲国家](../Page/非洲.md "wikilink")[-{zh-hans:马拉维;
    zh-hant:馬拉威;}-公开宣布斷绝與](../Page/马拉维.md "wikilink")[中華民國长达](../Page/中華民國.md "wikilink")41年的[外交关系](../Page/中華民國外交.md "wikilink")，并称在去年[12月28日就已经与](../Page/12月28日.md "wikilink")[中華人民共和國建立](../Page/中華人民共和國.md "wikilink")[外交关系](../Page/中华人民共和国与世界各国建交简表.md "wikilink")。[路透社](https://web.archive.org/web/20081222152054/http://africa.reuters.com/wire/news/usnL14131964.html)[美国之音中文网](http://www.voanews.com/chinese/w2008-01-14-voa41.cfm)
  - [美国国家航空航天局](../Page/美国国家航空航天局.md "wikilink")[信使号探测器掠过](../Page/信使号.md "wikilink")[水星](../Page/水星.md "wikilink")，其[轨道距离水星表面最近处只有约](../Page/轨道.md "wikilink")200公里。[新华网](http://news.xinhuanet.com/newscenter/2008-01/15/content_7424368.htm)

## 1月13日

  - [印度总理](../Page/印度总理.md "wikilink")[辛格乘专机抵达](../Page/曼莫汉·辛格.md "wikilink")[北京首都国际机场](../Page/北京首都国际机场.md "wikilink")，开始对[中国进行为期三天的国事访问](../Page/中华人民共和国.md "wikilink")。[新浪网](http://news.sina.com.cn/c/2008-01-13/102014731854.shtml)
  - 第65届[金球奖在](../Page/金球獎_\(影視獎項\).md "wikilink")[美国](../Page/美国.md "wikilink")[洛杉矶以](../Page/洛杉矶.md "wikilink")[新闻发布会方式揭晓](../Page/新闻发布会.md "wikilink")，[英国电影](../Page/英国电影.md "wikilink")《[赎罪](../Page/赎罪.md "wikilink")》获得剧情类最佳影片奖。[中国新闻网](http://www.chinanews.com.cn/yl/dyzx/news/2008/01-14/1133328.shtml)

## 1月12日

  - [中華民國第七屆立法委員選舉完成投開票](../Page/中華民國第七屆立法委員選舉.md "wikilink")，113席中[泛藍陣營獲得](../Page/泛藍陣營.md "wikilink")85席（四分之三多數）、[民主進步黨獲得](../Page/民主進步黨.md "wikilink")27席。民進黨主席[陳水扁晚上宣佈辭去黨主席職務](../Page/陳水扁.md "wikilink")。[中國時報1](https://web.archive.org/web/20090610013757/http://news.chinatimes.com/2007Cti/2007Cti-Focus/2007Cti-Focus-Content/0%2C4518%2C9701120221%2097011208%200%20000158%200%2C00.html)[中國時報2](https://web.archive.org/web/20080115094641/http://news.chinatimes.com/2007Cti/2007Cti-Focus/2007Cti-Focus-Content/0,4518,9701120301+97011210+0+000158+0,00.html)
  - [湖北省](../Page/湖北省.md "wikilink")[天門市有人因用手機拍下](../Page/天門市.md "wikilink")[城市管理執法局五十多名執法人員為填埋垃圾與農民發生衝突之事](../Page/城市管理執法局.md "wikilink")，而遭城管人員暴力毆打致死。[中國時報](https://web.archive.org/web/20080115091743/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110505+112008011200066,00.html)
  - [西班牙國歌](../Page/西班牙.md "wikilink")《[皇家進行曲](../Page/皇家進行曲.md "wikilink")》歌詞填詞比賽結果公布。在國會批准後，將結束30年沒有歌詞的歷史。[每日電訊報](http://www.telegraph.co.uk/news/main.jhtml?xml=/news/2008/01/12/wspain112.xml)

## 1月11日

  - [日本參議院否決了旨在恢復](../Page/日本參議院.md "wikilink")[日本海上自衛隊在](../Page/日本海上自衛隊.md "wikilink")[印度洋向美英等國艦船提供燃料等後勤保障的新反恐特別措施法案](../Page/印度洋.md "wikilink")。[法新社](https://web.archive.org/web/20080118124905/http://afp.google.com/article/ALeqM5jJ5zOm2ScXsyRbwQjmP8POYR5LYw)
  - [国际原子能机构总干事](../Page/国际原子能机构.md "wikilink")[穆罕默德·巴拉迪抵达德黑兰](../Page/穆罕默德·巴拉迪.md "wikilink")，将和[伊朗有关方面讨论](../Page/伊朗.md "wikilink")[伊朗核计划问题](../Page/伊朗核问题.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/11/content_7404118.htm)
  - 一架从[澳大利亚](../Page/澳大利亚.md "wikilink")[霍巴特起飞的](../Page/霍巴特.md "wikilink")[空中客车](../Page/空中客车.md "wikilink")[A319](../Page/A319.md "wikilink")[客机降落在](../Page/客机.md "wikilink")[南极澳大利亚](../Page/南极.md "wikilink")[凯西站附近的冰制](../Page/凯西站.md "wikilink")[跑道上](../Page/跑道.md "wikilink")，成为首部到达[南极洲的商用航班](../Page/南极洲.md "wikilink")。[中国新闻网](http://www.chinanews.com.cn/gj/kong/news/2008/01-11/1131472.shtml)[联合新闻网](http://gb.udn.com/gb/udn.com/NEWS/WORLD/WOR4/4177413.shtml)

## 1月10日

  - [巴基斯坦东部城市](../Page/巴基斯坦.md "wikilink")[拉合尔高等法院外发生自杀式爆炸](../Page/拉合尔.md "wikilink")，造成至少22名[警察和](../Page/警察.md "wikilink")4名[平民丧生](../Page/平民.md "wikilink")，同时造成包括过路平民在内的58人受伤。[新浪](http://news.sina.com.cn/w/2008-01-10/225213235714s.shtml)
  - [哥伦比亚反政府武装](../Page/哥伦比亚.md "wikilink")[哥伦比亚革命武装力量将扣押多年的两位女](../Page/哥伦比亚革命武装力量.md "wikilink")[人质](../Page/人质.md "wikilink")，前哥伦比亚[国会](../Page/国会.md "wikilink")[议员](../Page/议员.md "wikilink")[冈萨雷斯和](../Page/孔苏埃洛·冈萨雷斯.md "wikilink")[政治家](../Page/政治家.md "wikilink")[罗哈斯移交给](../Page/克拉拉·罗哈斯.md "wikilink")[国际红十字会和](../Page/国际红十字会.md "wikilink")[委内瑞拉政府的代表](../Page/委内瑞拉.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/11/content_7401827.htm)[新华社](https://archive.is/20130101133412/http://www.jmnews.com.cn/c/2008/01/11/01/c_5675506.shtml)
  - [中国](../Page/中国.md "wikilink")[華中](../Page/華中.md "wikilink")、[華南的](../Page/華南.md "wikilink")[湖南](../Page/湖南.md "wikilink")、[江蘇](../Page/江蘇.md "wikilink")、[江西](../Page/江西.md "wikilink")、[湖北等](../Page/湖北.md "wikilink")[省級行政區开始遭遇持续大規模](../Page/省級行政區.md "wikilink")[雪災](../Page/2008年中國雪災.md "wikilink")，造成重大损失。[南方报网](http://www.nfdaily.cn/special/chunyun/weather/content/2008-02/04/content_4324895.htm)

## 1月9日

  - [2008年美國總統選舉](../Page/2008年美國總統選舉.md "wikilink")：民主共和兩黨在[新罕布夏州舉行](../Page/新罕布夏州.md "wikilink")[初選](../Page/初選.md "wikilink")。民主黨代表[紐約州的](../Page/紐約州.md "wikilink")[聯邦參議員](../Page/美國參議院.md "wikilink")[希拉里·克林頓險勝](../Page/希拉里·克林頓.md "wikilink")[伊利諾州聯邦參議員](../Page/伊利諾州.md "wikilink")[巴拉克·奧巴馬](../Page/巴拉克·奧巴馬.md "wikilink")。共和黨方面代表[亞利桑那州的參議員](../Page/亞利桑那州.md "wikilink")[約翰·麥凱恩勝出](../Page/約翰·麥凱恩.md "wikilink")。[CNN](http://www.cnn.com/2008/POLITICS/01/08/nh.main/index.html)
  - [世界卫生组织发布一份和](../Page/世界卫生组织.md "wikilink")[伊拉克政府共同完成的调查报告](../Page/伊拉克.md "wikilink")，指出自2003年[美国入侵伊拉克至前年六月](../Page/伊拉克战争.md "wikilink")，共有15.1万名伊拉克人死于暴力事件。[联合新闻网](http://gb.udn.com/gb/udn.com/NEWS/WORLD/WOR3/4174494.shtml)

## 1月8日

  - [巴西警方表示](../Page/巴西.md "wikilink")，已经找到上月在[圣保罗艺术博物馆失窃的兩幅名畫](../Page/圣保罗艺术博物馆.md "wikilink")，兩名嫌犯被捕。这两幅名画其中一幅是[畢加索](../Page/畢加索.md "wikilink")[1904年的作品](../Page/1904年.md "wikilink")《[蘇珊寶殊的肖像](../Page/蘇珊寶殊的肖像.md "wikilink")》，另一幅是巴西畫家[坎迪多·波爾蒂納里的](../Page/坎迪多·波爾蒂納里.md "wikilink")《咖啡工人》。[美聯社](https://web.archive.org/web/20080201100147/http://ap.google.com/article/ALeqM5i9Mq-oRe_1aFaJKnPZ9b728qgK5AD8U21D700)[新华网](http://news.xinhuanet.com/shuhua/2008-01/09/content_7391444.htm)
  - [馬爾代夫總統](../Page/馬爾代夫.md "wikilink")[穆蒙·阿卜杜勒·加堯姆避過了一次暗殺](../Page/穆蒙·阿卜杜勒·加堯姆.md "wikilink")。[美聯社](https://web.archive.org/web/20080113044641/http://ap.google.com/article/ALeqM5h2IuVgHukJ_GWOTg73MBEIA4tiOQD8U1QIK00)
  - [黃金價格升至](../Page/黃金.md "wikilink")884美元，是歷來新高。[美聯社](https://web.archive.org/web/20080112211525/http://ap.google.com/article/ALeqM5jND4r3B-VBZu2Ogg2_yzjYnPIP8gD8U1V27O0)
  - [美國國防部公開日前](../Page/美國國防部.md "wikilink")[伊朗快艇在美軍艦艇附近游弋的片段](../Page/伊朗.md "wikilink")。[半島電視台](http://english.aljazeera.net/NR/exeres/C29FCA59-5D40-4B32-9018-2184C49AEEA4.htm)
  - [美国](../Page/美国.md "wikilink")[总统](../Page/美国总统.md "wikilink")[乔治·布什乘坐](../Page/乔治·沃克·布什.md "wikilink")[空军一号专机启程对](../Page/空军一号.md "wikilink")[以色列](../Page/以色列.md "wikilink")、[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")、[科威特](../Page/科威特.md "wikilink")、[巴林](../Page/巴林.md "wikilink")、[阿拉伯联合酋长国](../Page/阿拉伯联合酋长国.md "wikilink")、[沙特阿拉伯和](../Page/沙特阿拉伯.md "wikilink")[埃及等国进行为期](../Page/埃及.md "wikilink")8天的访问。[新华网](http://news.xinhuanet.com/newscenter/2008-01/09/content_7390819.htm)
  - [意大利](../Page/意大利.md "wikilink")[总理](../Page/意大利总理.md "wikilink")[普罗迪宣布](../Page/罗马诺·普罗迪.md "wikilink")[政府准备动用军队来彻底解决](../Page/政府.md "wikilink")[那不勒斯及其周边地区的](../Page/那不勒斯.md "wikilink")[垃圾处理危机](../Page/垃圾.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-01/09/content_7388390.htm)

## 1月7日

  - [美国](../Page/美国.md "wikilink")[好莱坞外国记者协会宣布](../Page/好莱坞外国记者协会.md "wikilink")，由于[好莱坞](../Page/好莱坞.md "wikilink")[编剧](../Page/编剧.md "wikilink")[罢工得到](../Page/罢工.md "wikilink")[美国电影演员协会的声援](../Page/美国电影演员协会.md "wikilink")，原订于13日举行的第65届[金球奖颁奖典礼将改成以](../Page/金球奖.md "wikilink")[新闻发布会发布得奖名单](../Page/新闻发布会.md "wikilink")。[彭博](http://www.bloomberg.com/apps/news?pid=20601088&sid=a1L6bZcQQVJA&refer=home)[新华网](http://news.xinhuanet.com/newscenter/2008-01/09/content_7387746.htm)

## 1月6日

  - 根據猶太人人口政策計劃機構的報告顯示，到去年底，全球[猶太人口達](../Page/猶太人.md "wikilink")1320萬，居住在[以色列的佔](../Page/以色列.md "wikilink")41%。[國土報](http://www.haaretz.com/hasen/spages/942009.html)
  - 美籍[蓋達分子](../Page/蓋達.md "wikilink")[阿當·加達恩昨在網站發表錄影講話](../Page/阿當·加達恩.md "wikilink")，呼籲伊斯蘭武裝分子「用炸彈及陷阱」歡迎星期三訪問中東的美國總統[小布殊](../Page/小布殊.md "wikilink")。[法新社](https://web.archive.org/web/20080108045505/http://afp.google.com/article/ALeqM5i7KiMCgWHQkjkry3itedneB14NUg)
  - [格鲁吉亚中央选举委员会公布部分投票站的初步计票结果](../Page/格鲁吉亚.md "wikilink")，前[总统](../Page/格鲁吉亚总统.md "wikilink")[萨卡什维利在](../Page/米哈伊爾·薩卡什維利.md "wikilink")5日的总统选举中获得了超过50%的选票，萨卡什维利宣布自己赢得大选。[中国新闻网](http://www.chinanews.com.cn/gj/ywdd/news/2008/01-07/1124925.shtml)[新华网](http://news.xinhuanet.com/newscenter/2008-01/07/content_7375314.htm)
  - 3艘[美国军舰和](../Page/美国.md "wikilink")5艘[伊朗快艇在](../Page/伊朗.md "wikilink")[波斯湾](../Page/波斯湾.md "wikilink")[霍尔木兹海峡发生对峙事件](../Page/霍尔木兹海峡.md "wikilink")，双方在数百米距离内险些开火。[新华网](http://news.xinhuanet.com/newscenter/2008-01/09/content_7384293.htm)

## 1月5日

  - [肯尼亚总统](../Page/肯尼亚.md "wikilink")[齐贝吉表示愿意组建](../Page/姆瓦伊·齐贝吉.md "wikilink")[联合政府](../Page/联合政府.md "wikilink")，以结束因总统选举引发的[暴力事件](../Page/肯亞騷亂.md "wikilink")，反对派领导人[奥廷加则拒绝接受](../Page/拉伊拉·奥廷加.md "wikilink")，坚持要求总统先辞职。
    [新华网](http://news.xinhuanet.com/newscenter/2008-01/07/content_7371754.htm)[美国之音中文网](http://www.voanews.com/chinese/n2008-01-06-voa10.cfm)

## 1月4日

  - [达喀尔拉力赛组委会宣布](../Page/达喀尔拉力赛.md "wikilink")，由于[毛里塔尼亚境内安全无法得到保障](../Page/毛里塔尼亚.md "wikilink")，2008年达喀尔拉力赛赛事全部取消。[新浪](http://sports.sina.com.cn/f1/2008dakar/)
  - 南韓航空宇宙研究院表示，南韓第一枚多功能實用衛星[阿里郎一號](../Page/阿里郎一號.md "wikilink")，於觀測朝鮮半島八年後的上個月三十日，突然在太空中消失。[法新社](https://web.archive.org/web/20080109062358/http://afp.google.com/article/ALeqM5hZk3CmXv9iwOA8idoYh9rDihr_dw)

## 1月3日

  - [2008年美國總統選舉](../Page/2008年美國總統選舉.md "wikilink")：代表[伊利諾伊州的](../Page/伊利諾伊州.md "wikilink")[參議員](../Page/美國參議院.md "wikilink")[巴拉克·奧巴馬和前](../Page/巴拉克·奧巴馬.md "wikilink")[阿肯色州](../Page/阿肯色州.md "wikilink")[州長](../Page/阿肯色州州長.md "wikilink")[麥克·赫卡比分別贏得民主共和兩黨在](../Page/麥克·赫卡比.md "wikilink")[愛阿華州的黨團會議](../Page/愛阿華州.md "wikilink")。[BBC](http://news.bbc.co.uk/2/hi/americas/7170954.stm)

## 1月2日

  - [巴基斯坦選舉委員會宣布](../Page/巴基斯坦.md "wikilink")，因前總理[貝娜齊爾·布托遇刺身亡](../Page/貝娜齊爾·布托.md "wikilink")，[國會選舉將由](../Page/2008年巴基斯坦國會選舉.md "wikilink")[1月8日押後至](../Page/1月8日.md "wikilink")[2月18日](../Page/2月18日.md "wikilink")。[紐約時報](http://www.nytimes.com/2008/01/03/world/asia/03pakistan.html?hp)
  - [智利](../Page/智利.md "wikilink")[亞伊馬火山](../Page/亞伊馬火山.md "wikilink")（Llaima）自去年5月以後再次爆發。[美聯社](https://web.archive.org/web/20080107012135/http://ap.google.com/article/ALeqM5h6XCGT4X37OvMVvC6CA6UaDMeriAD8TTG8100)
  - [無國界記者指出](../Page/無國界記者.md "wikilink")，去年有86名記者被殺，20名與媒體工作有關人士被殺害；887名記者被逮捕；1,511名記者遭受身體攻擊或威脅；67名記者被綁架。[路透社](https://archive.is/20130427205633/http://africa.reuters.com/world/news/usnL0277993.html)
  - [美国](../Page/美国.md "wikilink")[纽约商品交易所](../Page/纽约商品交易所.md "wikilink")2月份交货的[原油](../Page/原油.md "wikilink")[期货价格在历史上首次突破每桶](../Page/期货.md "wikilink")100[美元的关口](../Page/美元.md "wikilink")。[衛報](http://www.guardian.co.uk/business/2008/jan/02/oil)[人民网](http://auto.people.com.cn/GB/1049/6727718.html)
  - 2008年全国铁路[春运方案出台](../Page/春运.md "wikilink")。[新华网](http://www.xinhuanet.com/chinanews/2008-01/03/content_12117442.htm)
  - [斯里兰卡政府宣布](../Page/斯里兰卡.md "wikilink")，斯内阁当晚决定退出斯政府2002年与反政府武装[泰米尔伊拉姆猛虎解放组织](../Page/泰米尔伊拉姆猛虎解放组织.md "wikilink")（猛虎组织）签署的停火协议。[新华网](http://news.xinhuanet.com/newscenter/2008-01/03/content_7355744.htm)

## 1月1日

  - 由[海灣阿拉伯國家合作委員會](../Page/海灣阿拉伯國家合作委員會.md "wikilink")（海合會）6個成員國組成的[海灣共同市場今日正式啟動](../Page/海灣共同市場.md "wikilink")。[Arab
    News](http://www.arabnews.com/?page=1&section=0&article=105173&d=1&m=1&y=2008&pix=kingdom.jpg&category=Kingdom)
  - [馬爾他和](../Page/馬爾他.md "wikilink")[塞浦路斯](../Page/塞浦路斯.md "wikilink")（[北塞浦路斯除外](../Page/北賽普勒斯土耳其共和國.md "wikilink")）两个[地中海的](../Page/地中海.md "wikilink")[欧盟成员国正式採用](../Page/欧盟.md "wikilink")[歐元](../Page/歐元.md "wikilink")，使[歐元區的成員增至](../Page/歐元區.md "wikilink")15国。[美聯社](http://ap.google.com/article/ALeqM5h0JknXpqDPYAizeSKIl_Mu04rjJgD8TSPNVG0)[中国新闻网](http://www.chinanews.com.cn/gj/oz/news/2008/01-01/1120228.shtml)
  - [肯亞](../Page/肯亞.md "wikilink")[埃爾多雷特一家](../Page/埃爾多雷特.md "wikilink")[神召會教堂被縱火](../Page/神召會.md "wikilink")，在那裡躲避攻擊的50人被燒死。死者多數為現任總統[姆瓦伊·齊貝吉名支持者](../Page/姆瓦伊·齊貝吉.md "wikilink")。[波士頓論壇報](http://www.bostonherald.com/news/international/africa/view.bg?articleid=1064031)
  - [美國](../Page/美國.md "wikilink")[新罕布夏州承認](../Page/新罕布夏州.md "wikilink")[公民結合的合法地位](../Page/公民結合.md "wikilink")。[波士頓環球報](http://www.boston.com/news/local/articles/2008/01/02/gay_nh_couples_celebrate_gain_status_in_civil_unions/)
  - [委內瑞拉為抗擊](../Page/委內瑞拉.md "wikilink")[通貨膨脹](../Page/通貨膨脹.md "wikilink")，發行[強勢玻利瓦爾](../Page/強勢玻利瓦爾.md "wikilink")，面值為舊幣的一千倍。[聯合報](http://udn.com/NEWS/FINANCE/FIN5/4162842.shtml)
  - [台灣](../Page/台灣.md "wikilink")[臺北縣舉行萬人擊鼓活動](../Page/新北市.md "wikilink")，以10162人同時擊鼓，打破2007年香港10046人同時擊鼓之金氏世界紀錄。[金羊网](https://web.archive.org/web/20080130183812/http://www.ycwb.com/xkb/2008-01/03/content_1741514.htm)
  - [中華民國國軍義務役役期減為](../Page/中華民國國軍.md "wikilink")1年。

[Category:2008年](../Category/2008年.md "wikilink")
[Category:1月](../Category/1月.md "wikilink")