## 3月31日

  - [大湄公河次区域经济合作第三次领导人会议举行](../Page/大湄公河次区域经济合作.md "wikilink")，六国领导人在[老挝首都](../Page/老挝.md "wikilink")[万象签署了](../Page/万象.md "wikilink")《领导人宣言》。[新华网](http://news.xinhuanet.com/newscenter/2008-03/31/content_7893324.htm)
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_7320000/newsid_7322600/7322653.stm)
  - 根據[教廷公布最新資料](../Page/教廷.md "wikilink")，信仰[伊斯蘭教的人數已超越](../Page/伊斯蘭教.md "wikilink")[天主教](../Page/天主教.md "wikilink")，但[基督宗教仍然是全球第一大宗教](../Page/基督宗教.md "wikilink")。[泰晤士報](http://www.timesonline.co.uk/tol/news/world/article3653800.ece)
  - 凌晨，九巴在天慈邨旁的露天停車場，有10輛雙層巴士起火，火勢猛烈，多次傳出爆炸聲，消防員出動5條喉將火救熄，認為起火原因無可疑，相信是其中1輛巴士（ATE180／LN5481）電線短路引致機件過熱引起。事件中無人受傷，傳聞事故發生時火場附近有多輛巴士，由廠務人員駛離火場，其中其中兩部肇事巴士（KH3743及JZ5727）因太接近火場無法駛走，不過損毀輕微。除KH3743及JZ5727外，其餘8輛嚴重損毀巴士因不獲維修，於2008年7月3日正式除牌。

## 3月30日

  - [辛巴威大選結束](../Page/辛巴威.md "wikilink")。反對現任[羅伯特·穆加貝的政黨自行宣佈勝利](../Page/羅伯特·穆加貝.md "wikilink")。[CNN](http://edition.cnn.com/2008/WORLD/africa/03/30/zimbabwe.election/)
  - [希臘當局在首都](../Page/希臘.md "wikilink")[雅典](../Page/雅典.md "wikilink")，把[聖火移交](../Page/奧運聖火.md "wikilink")[北京奧組委](../Page/北京奧運.md "wikilink")，但仍然有人示威。[路透社](http://in.reuters.com/article/worldOfSport/idINIndia-32750420080330)
  - [美国与](../Page/美国.md "wikilink")[欧盟领导人于去年](../Page/欧盟.md "wikilink")4月在[华盛顿签署的](../Page/华盛顿.md "wikilink")（EU-US
    Open Skies
    Agreement）正式生效，双方将进一步向对方开放[航空市场](../Page/航空.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-03/30/content_7884679.htm)
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_7320000/newsid_7321200/7321242.stm)
  - [伊拉克](../Page/伊拉克.md "wikilink")[什叶派领导人](../Page/什叶派.md "wikilink")[穆克塔达·萨德尔发表声明](../Page/穆克塔达·萨德尔.md "wikilink")，命令其民兵武装[迈赫迪军停止与伊拉克安全部队进行对抗](../Page/迈赫迪军.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-03/30/content_7886426.htm)
  - [普利兹克奖评选委员会宣布](../Page/普利兹克奖.md "wikilink")，[法国建筑师](../Page/法国.md "wikilink")[让·努维尔获得本年度普利兹克奖](../Page/让·努维尔.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-04/01/content_7894420.htm)

## 3月29日

  - 第20次[阿拉伯国家联盟首脑会议在](../Page/阿拉伯国家联盟.md "wikilink")[叙利亚首都](../Page/叙利亚.md "wikilink")[大马士革开幕](../Page/大马士革.md "wikilink")，会议将讨论包括[中东和平进程在内的一系列议题](../Page/巴以冲突.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-03/29/content_7881678.htm)
  - [國務院總理](../Page/國務院總理.md "wikilink")[溫家寶到達](../Page/溫家寶.md "wikilink")[寮國作](../Page/寮國.md "wikilink")3日外訪，出席[大湄公河次區域經濟合作第三次峰會](../Page/湄公河.md "wikilink")。[新華社](http://news.xinhuanet.com/english/2008-03/29/content_7881357.htm)
  - [2008年西藏騷亂](../Page/2008年西藏騷亂.md "wikilink")：在外國外交官離開後，拉薩再度爆發示威。[美聯社](http://ap.google.com/article/ALeqM5h5Z6bJwtN_roGSIUQiQnfbf2NkhgD8VN6KA80)
  - [德國總理](../Page/德國.md "wikilink")[默克爾繼](../Page/默克爾.md "wikilink")[捷克](../Page/捷克.md "wikilink")、[波蘭](../Page/波蘭.md "wikilink")、[愛沙尼亞領導人之後宣佈不出席](../Page/愛沙尼亞.md "wikilink")[北京奧運開幕式](../Page/北京奧運.md "wikilink")。[福克斯](https://web.archive.org/web/20080404044149/http://www.foxnews.com/story/0,2933,343169,00.html)
  - 由[世界自然基金会发起的](../Page/世界自然基金会.md "wikilink")“[地球时间](../Page/地球时间.md "wikilink")”环保活动在全球40多个国家和地区的380多个城镇接力举行，这项活动旨在引起人们对[气候变化的警惕](../Page/全球变暖.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-03/30/content_7884162.htm)
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_7320000/newsid_7320800/7320865.stm)

## 3月28日

  - [中国邀请来自](../Page/中国.md "wikilink")[英国](../Page/英国.md "wikilink")、[美国等至少](../Page/美国.md "wikilink")15个国家的驻华使领馆官员前往[西藏](../Page/西藏.md "wikilink")，考察[西藏骚乱过后的情况](../Page/2008年西藏骚乱.md "wikilink")。[BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_7310000/newsid_7317800/7317864.stm)
  - [美國](../Page/美國.md "wikilink")[華盛頓時報報導](../Page/華盛頓時報.md "wikilink")，根據美國情報官員透露，[中國瞄準](../Page/中國.md "wikilink")[台灣的飛彈高達](../Page/台灣.md "wikilink")1400枚。[東森電子報](http://www.ettoday.com/2008/03/29/162-2252876.htm)[The
    Washington
    Times](http://www.washingtontimes.com/article/20080328/FOREIGN/914383504/1003)
  - [古巴共产党](../Page/古巴共产党.md "wikilink")[机关报](../Page/机关报.md "wikilink")《[格拉玛](../Page/格拉玛_\(报纸\).md "wikilink")》刊登[古巴政府的新政令](../Page/古巴.md "wikilink")，宣布将允许普通古巴人合法购买和使用[手机](../Page/手机.md "wikilink")。[新浪网](http://news.sina.com.cn/w/2008-03-30/011313655032s.shtml)[新华网](http://news.xinhuanet.com/newscenter/2008-03/30/content_7880905.htm)
  - [北韓在驅逐](../Page/北韓.md "wikilink")11名在[開城工業區的](../Page/開城工業區.md "wikilink")[南韓人員後](../Page/南韓.md "wikilink")，今日又試射導彈。[路透社](http://www.washingtonpost.com/wp-dyn/content/article/2008/03/27/AR2008032704075.html?hpid=topnews)

## 3月27日

  - 一段在[1860年](../Page/1860年.md "wikilink")[4月9日製作的錄音片段播出](../Page/4月9日.md "wikilink")，是目前已知最早的。[紐約時報](http://www.nytimes.com/2008/03/27/arts/27soun.html)
  - [荷蘭國會議員](../Page/荷蘭.md "wikilink")[基爾特·威爾德斯在互聯網發布了反伊斯蘭的影片](../Page/基爾特·威爾德斯.md "wikilink")《[鬥爭](../Page/鬥爭.md "wikilink")》。[BBC](http://news.bbc.co.uk/2/hi/uk_news/england/london/7317506.stm)

## 3月26日

  - [摩托罗拉宣布](../Page/摩托罗拉.md "wikilink")，计划于明年把公司分拆为两家独立上市的企业，一家专注于[手机业务](../Page/手机.md "wikilink")，另一家则主营[网络设备](../Page/网络.md "wikilink")、电视[机顶盒等业务](../Page/机顶盒.md "wikilink")。[腾讯](http://tech.qq.com/a/20080326/000600.htm)
  - [印度](../Page/印度.md "wikilink")[塔塔汽车公司和](../Page/塔塔汽车.md "wikilink")[美国](../Page/美国.md "wikilink")[福特汽车公司发表联合声明](../Page/福特汽车.md "wikilink")，塔塔将以23亿美元的价格收购福特旗下的“[捷豹](../Page/捷豹.md "wikilink")”和“[路虎](../Page/路虎.md "wikilink")”两大汽车品牌。[新华网](http://news.xinhuanet.com/world/2008-03/28/content_7872518.htm)
  - [美国](../Page/美国.md "wikilink")[奋进号](../Page/奋进号航天飞机.md "wikilink")[航天飞机在](../Page/航天飞机.md "wikilink")[佛罗里达州](../Page/佛罗里达州.md "wikilink")[肯尼迪航天中心着陆](../Page/肯尼迪航天中心.md "wikilink")，这次太空飞行共持续16天，期间与[国际空间站对接](../Page/国际空间站.md "wikilink")12天，[宇航员完成了](../Page/宇航员.md "wikilink")5次[太空行走任务](../Page/太空行走.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-03/28/content_7870319.htm)

## 3月25日

  - [科摩罗政府军和](../Page/科摩罗.md "wikilink")[非盟部队对](../Page/非洲联盟.md "wikilink")2007年起被叛军占领的[昂儒昂岛发动进攻](../Page/昂儒昂岛.md "wikilink")，科摩罗当局称已攻占该岛首府[穆察穆杜](../Page/穆察穆杜.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-03/25/content_7856821.htm)
  - [美國國防部證實在](../Page/美國國防部.md "wikilink")06年誤將[洲際彈道導彈的](../Page/洲際彈道導彈.md "wikilink")[引爆器當成](../Page/引爆器.md "wikilink")[直昇機](../Page/直昇機.md "wikilink")[電池售予](../Page/電池.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")。[中國時報](http://news.chinatimes.com/2007Cti/2007Cti-Rtn/2007Cti-Rtn-Content/0,4526,110109+112008032600666,00.html)[自由電子報](https://web.archive.org/web/20080329211708/http://www.libertytimes.com.tw/2008/new/mar/26/today-fo1.htm)
  - [美国科学家借助](../Page/美国.md "wikilink")[卫星图像证实](../Page/卫星图像.md "wikilink")，[南極洲](../Page/南極洲.md "wikilink")[南极半岛的一块巨大](../Page/南极半岛.md "wikilink")[冰川在](../Page/冰川.md "wikilink")[威尔金斯冰架边缘断裂入海](../Page/威尔金斯冰架.md "wikilink")，面積大約是415[平方公里](../Page/平方公里.md "wikilink")。[CNN新聞](http://edition.cnn.com/2008/TECH/science/03/25/antarctic.ice/?imw=Y&iref=mpstoryemail)[美聯社](https://web.archive.org/web/20080330110923/http://afp.google.com/article/ALeqM5hvgbwRoBCsttrkhOqhvZFlFuT_uw)[Bloomberg](http://www.bloomberg.com/apps/news?pid=20601086&sid=amx1sGjrgtlM&refer=latin_america)[新华网](http://news.xinhuanet.com/newscenter/2008-03/26/content_7862674.htm)

## 3月24日

  - [不丹选举委员会的初步统计结果显示](../Page/不丹.md "wikilink")，[不丹繁荣进步党在当天举行的](../Page/不丹繁荣进步党.md "wikilink")[国民议会选举中获胜](../Page/国民议会.md "wikilink")，至少赢得47个议席当中的40个席位。[法新社](http://afp.google.com/article/ALeqM5g0Cd2upY8hktIZGEX82jQaBdus9A)[新华网](http://news.xinhuanet.com/newscenter/2008-03/25/content_7851682.htm)
  - [伊拉克戰爭](../Page/伊拉克戰爭.md "wikilink")：美軍死亡人數達到4,000人。[BBC](http://news.bbc.co.uk/2/hi/middle_east/7310924.stm)
  - [中国](../Page/中国.md "wikilink")[黑龙江省](../Page/黑龙江省.md "wikilink")[佳木斯市法庭以](../Page/佳木斯市.md "wikilink")[煽动颠覆国家政权罪判处维权人士](../Page/煽动颠覆国家政权罪.md "wikilink")[杨春林有期徒刑五年](../Page/杨春林.md "wikilink")。[BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_7310000/newsid_7311100/7311194.stm)
  - [巴基斯坦](../Page/巴基斯坦.md "wikilink")[国民议会选举](../Page/国民议会.md "wikilink")[人民党候选人](../Page/巴基斯坦人民党.md "wikilink")[优素福·拉扎·吉拉尼为新一任](../Page/优素福·拉扎·吉拉尼.md "wikilink")[巴基斯坦总理](../Page/巴基斯坦总理.md "wikilink")。[中国新闻网](http://www.chinanews.com.cn/gj/kong/news/2008/03-24/1200983.shtml)
  - [2008年北京夏季奥运会](../Page/2008年夏季奥林匹克运动会.md "wikilink")[聖火的取火儀式在](../Page/奧運聖火.md "wikilink")[古代奧林匹克運動會的發源地](../Page/古代奧林匹克運動會.md "wikilink")、[希臘的](../Page/希臘.md "wikilink")[奧林匹亞舉行](../Page/奧林匹亞.md "wikilink")，[北京奥运会圣火传递活动开始](../Page/2008年夏季奧林匹克運動會聖火傳遞.md "wikilink")。在取火仪式中，三名[無國界記者和一名](../Page/無國界記者.md "wikilink")[藏人抗議者因干擾儀式進行而被捕](../Page/藏族.md "wikilink")。[美聯社](https://web.archive.org/web/20080419145310/http://ap.google.com/article/ALeqM5ghZ9ci28WTTMmDPvccRLc0MGpERQD8VJSVBO0)[新華社](http://news.xinhuanet.com/english/2008-03/24/content_7850206.htm)

## 3月23日

  - [巴勒斯坦解放组织和](../Page/巴勒斯坦解放组织.md "wikilink")[哈马斯的代表在](../Page/哈马斯.md "wikilink")[也门首都](../Page/也门.md "wikilink")[萨那签署宣言](../Page/萨那.md "wikilink")，同意在[也门总统](../Page/也门总统.md "wikilink")[萨利赫提出的](../Page/阿里·阿卜杜拉·萨利赫.md "wikilink")“也门倡议”框架下恢复直接对话。[新华网](http://news.xinhuanet.com/newscenter/2008-03/23/content_7844594.htm)

## 3月22日

  - 在[中华民国第12任总统、副总统大选暨第5、6号全国公民投票中](../Page/2008年中华民国总统选举.md "wikilink")，[国民党候选人](../Page/中国国民党.md "wikilink")[马英九以大约](../Page/马英九.md "wikilink")17个百分点的优势击败[民进党候选人](../Page/民主进步党.md "wikilink")[谢长廷](../Page/谢长廷.md "wikilink")，成为中华民国第12任总统（直接民選第四任）。[BBC
    News](http://news.bbc.co.uk/2/hi/asia-pacific/7309113.stm)[明報](https://web.archive.org/web/20080405000132/http://www.mpinews.com/htm/INews/20080322/ca61826a.htm)

## 3月20日

  - [比利时](../Page/比利时.md "wikilink")[国王](../Page/国王.md "wikilink")[阿尔贝二世任命](../Page/阿尔贝二世_\(比利时\).md "wikilink")[荷语基督教民主党领导人](../Page/荷语基督教民主党.md "wikilink")[伊夫·莱特姆为](../Page/伊夫·莱特姆.md "wikilink")[比利时首相](../Page/比利时首相.md "wikilink")，并授权他组建政府。[新浪网](http://news.sina.com.cn/w/2008-03-20/200815190457.shtml)
  - [美国国家航空航天局宣布](../Page/美国国家航空航天局.md "wikilink")，科学家借助[卡西尼号探测器的探测发现](../Page/卡西尼号.md "wikilink")，[土星的](../Page/土星.md "wikilink")[卫星](../Page/卫星.md "wikilink")[土卫六地表下可能潜藏着](../Page/土卫六.md "wikilink")[水和](../Page/水.md "wikilink")[氨构成的](../Page/氨.md "wikilink")[海洋](../Page/海洋.md "wikilink")。[新华网](http://news.xinhuanet.com/world/2008-03/21/content_7833497.htm)

## 3月19日

  - [塞爾維亞的三個鄰邦](../Page/塞爾維亞.md "wikilink")[保加利亞](../Page/保加利亞.md "wikilink")、[匈牙利和](../Page/匈牙利.md "wikilink")[克羅地亞承認](../Page/克羅地亞.md "wikilink")[科索沃](../Page/科索沃.md "wikilink")。訖今已有32個國家承認這前塞爾維亞自治省。[路透社](http://www.reuters.com/article/latestCrisis/idUSL19891210)
  - [美國](../Page/美國.md "wikilink")[總統](../Page/美國總統.md "wikilink")[小布什在](../Page/小布什.md "wikilink")[伊拉克戰爭五周年前夕堅持攻伊正確](../Page/伊拉克戰爭.md "wikilink")。[衛報](http://www.guardian.co.uk/world/feedarticle/7395833)
  - [基地組織領袖](../Page/基地組織.md "wikilink")[奧薩馬·本·拉登發表錄音講話](../Page/奧薩馬·本·拉登.md "wikilink")，指要向再次刊登[褻瀆穆罕默德漫畫的歐洲國家進行襲擊](../Page/日德蘭郵報穆罕默德漫畫事件.md "wikilink")。[美聯社](https://web.archive.org/web/20080325115546/http://ap.google.com/article/ALeqM5h0arauyjLz9xhnBdnw6pEEOpKErwD8VGP99G1)

## 3月18日

  - 为防止[次贷危机继续恶化及美国经济陷入衰退](../Page/次贷危机.md "wikilink")，[美国联邦储备委员会决定再次大幅下调](../Page/美国联邦储备委员会.md "wikilink")[联邦基金利率](../Page/联邦基金利率.md "wikilink")，即商业银行间隔夜拆借利率由原来的3.0％降至2.25％。[新华网](http://news.xinhuanet.com/fortune/2008-03/18/content_7817413.htm)
  - [德國](../Page/德國.md "wikilink")[總理](../Page/德國總理.md "wikilink")[默克爾在](../Page/默克爾.md "wikilink")[以色列](../Page/以色列.md "wikilink")[國會演講](../Page/以色列國會.md "wikilink")，這是首次有德國總理和非國家元首這樣做。[金融時報](http://www.ft.com/cms/s/0/f4a9f686-f556-11dc-a21b-000077b07658.html)
    [中国新闻网](http://www.chinanews.com.cn/gj/oz/news/2008/03-19/1195836.shtml)

## 3月16日

  - [中华人民共和国](../Page/中华人民共和国.md "wikilink")[第十一届全国人民代表大会投票表决通过](../Page/第十一届全国人民代表大会.md "wikilink")[温家宝为](../Page/温家宝.md "wikilink")[中华人民共和国国务院总理](../Page/中华人民共和国国务院总理.md "wikilink")。[新华网](http://news.xinhuanet.com/misc/2008-03/16/content_7799705.htm)
  - [美国](../Page/美国.md "wikilink")[摩根大通公司表示](../Page/摩根大通.md "wikilink")，将用换股的方式以总价2.63亿[美元的价格收购](../Page/美元.md "wikilink")[贝尔斯登公司](../Page/贝尔斯登.md "wikilink")，此价格较14日收盘时贝尔斯登的[股票](../Page/股票.md "wikilink")[市值缩水](../Page/市值.md "wikilink")93%。[新华网](http://news.xinhuanet.com/newscenter/2008-03/17/content_7804190.htm)

## 3月15日

  - [中华人民共和国](../Page/中华人民共和国.md "wikilink")[第十一届全国人民代表大会选举](../Page/第十一届全国人民代表大会.md "wikilink")[胡锦涛为](../Page/胡锦涛.md "wikilink")[中华人民共和国主席](../Page/中华人民共和国主席.md "wikilink")、[中央军事委员会主席](../Page/中华人民共和国中央军事委员会主席.md "wikilink")，[习近平为](../Page/习近平.md "wikilink")[中华人民共和国副主席](../Page/中华人民共和国副主席.md "wikilink")，[吴邦国为第十一届](../Page/吴邦国.md "wikilink")[全国人民代表大会常务委员会委员长](../Page/全国人民代表大会常务委员会委员长.md "wikilink")。[新华网](http://www.xinhuanet.com/2008lh/zb/0315a/)
  - “中日青少年友好交流年”在[北京开幕](../Page/北京.md "wikilink")。[胡锦涛](../Page/胡锦涛.md "wikilink")、[福田康夫出席开幕式](../Page/福田康夫.md "wikilink")。

## 3月14日

  - [中国](../Page/中华人民共和国.md "wikilink")[西藏首府](../Page/西藏自治区.md "wikilink")[拉萨自](../Page/拉萨市.md "wikilink")10日爆发的藏人抗议事件演變為[骚乱事件](../Page/2008年西藏骚乱.md "wikilink")，至少10人在冲突中丧生。[明報](https://web.archive.org/web/20080318025211/http://hk.news.yahoo.com/080314/12/2qmsp.html)[法新社](https://web.archive.org/web/20080318042627/http://hk.news.yahoo.com/080314/167/2qmt2.html)[BBC](http://news.bbc.co.uk/chinese/trad/hi/newsid_7290000/newsid_7295800/7295898.stm)
    [RFA](https://archive.is/20080327234025/http://www.rfa.org/mandarin/shenrubaodao/2008/03/14/llhasa)
    [RFA](http://www.rfa.org/mandarin/shenrubaodao/2008/03/14/xizang)[BBC中文网](http://news.bbc.co.uk/chinese/trad/hi/newsid_7290000/newsid_7297800/7297893.stm)
  - [美国](../Page/美国.md "wikilink")[纽约商品交易所的](../Page/纽约商品交易所.md "wikilink")[黄金](../Page/黄金.md "wikilink")[期货价格首次突破每](../Page/期货.md "wikilink")[盎司](../Page/盎司.md "wikilink")1000[美元大关](../Page/美元.md "wikilink")，同时美元兑日元汇率跌至1995年以来的最低点。[BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_7290000/newsid_7295300/7295388.stm)
    [新华网](http://news.xinhuanet.com/world/2008-03/14/content_7789780.htm)
  - [美國第五大](../Page/美國.md "wikilink")[投資銀行](../Page/投資銀行.md "wikilink")[貝爾斯登出現資金流動性的問題](../Page/貝爾斯登.md "wikilink")，導致[紐約聯邦銀行及](../Page/紐約聯邦銀行.md "wikilink")[摩根大通銀行提供援助](../Page/摩根大通.md "wikilink")，但股價仍然下跌五成。消息馬上讓市場失去信心。[雅虎新聞](https://web.archive.org/web/20080318043438/http://hk.biz.yahoo.com/080314/241/2qmvu.html)
  - [美國食品暨藥物管理局表示](../Page/美國食品暨藥物管理局.md "wikilink")，已獲得[美國國務院批准](../Page/美國國務院.md "wikilink")，在美國位於[中國的外交駐點設立美國食品暨藥物管理局常駐海外辦公室](../Page/中國.md "wikilink")，以確保輸美產品安全。[自由電子報](https://web.archive.org/web/20080319010146/http://www.libertytimes.com.tw/2008/new/mar/16/today-int1-2.htm)
  - [2008年夏季奧運棒球最終資格排名賽結果揭曉](../Page/2008年夏季奧運棒球最終資格排名賽.md "wikilink")，由[加拿大](../Page/加拿大棒球代表隊.md "wikilink")、[南韓](../Page/韓國棒球代表隊.md "wikilink")、[中華台北取得最後三個](../Page/中華成棒隊.md "wikilink")[北京奧運棒球項目參賽資格](../Page/2008年夏季奧林匹克運動會棒球比賽.md "wikilink")。[搜狐](http://news.sohu.com/20080315/n255717777.shtml)

## 3月13日

  - [印度方面扣留了一批流亡當地的](../Page/印度.md "wikilink")[西藏人](../Page/西藏.md "wikilink")，他們試圖步行到西藏抗議[北京奧運](../Page/北京奧運.md "wikilink")。[美聯社](http://www.pr-inside.com/indian-police-detain-tibetan-exiles-marching-r482419.htm)
  - [2008年美國總統選舉候选人](../Page/2008年美國總統選舉.md "wikilink")[希拉里·克林頓競選辦公室財務委員會成員](../Page/希拉里·克林頓.md "wikilink")[傑羅丁·費拉羅因針對](../Page/傑羅丁·費拉羅.md "wikilink")[巴拉克·奧巴馬的言論而辭職](../Page/巴拉克·奧巴馬.md "wikilink")。[紐約時報](http://www.nytimes.com/2008/03/13/us/politics/13ferraro.html?ref=us)
  - 由于[塞尔维亚](../Page/塞尔维亚.md "wikilink")[议会内部在](../Page/议会.md "wikilink")[科索沃独立和与](../Page/科索沃独立.md "wikilink")[欧盟的关系问题上存在分歧](../Page/欧盟.md "wikilink")，在塞尔维亚政府的请求下，[总统](../Page/鲍里斯·塔迪奇.md "wikilink")[塔迪奇宣布解散议会](../Page/塔迪奇.md "wikilink")。[新华网](http://news.xinhuanet.com/world/2008-03/13/content_7784149.htm)
  - 第十一次伊斯兰国家首脑会议在[塞内加尔首都](../Page/塞内加尔.md "wikilink")[达喀尔开幕](../Page/达喀尔.md "wikilink")，[伊斯兰会议组织](../Page/伊斯兰会议组织.md "wikilink")57个成员国派代表出席。[新华网](http://news.xinhuanet.com/newscenter/2008-03/13/content_7784183.htm)
  - [中国人民政治协商会议第十一届全国委员会第一次会议在](../Page/中国人民政治协商会议.md "wikilink")[北京](../Page/北京.md "wikilink")[人民大会堂举行第四次全体会议](../Page/人民大会堂.md "wikilink")，选举[贾庆林为](../Page/贾庆林.md "wikilink")[全国委员会主席](../Page/中国人民政治协商会议全国委员会主席.md "wikilink")。[新华网](http://news.xinhuanet.com/misc/2008-03/13/content_7783070.htm)
  - [欧盟峰会在位于](../Page/欧盟峰会.md "wikilink")[比利时首都](../Page/比利时.md "wikilink")[布鲁塞尔的欧盟总部拉开帷幕](../Page/布鲁塞尔.md "wikilink")。在首日会议上，欧盟成员国领导人勾勒出了未来三年的经济改革蓝图，还就建立[地中海联盟达成原则一致](../Page/地中海联盟.md "wikilink")，但在温室气体减排任务的分摊上仍存分歧。[新华网](http://news.xinhuanet.com/newscenter/2008-03/14/content_7789787.htm)

## 3月12日

  - [美國](../Page/美國.md "wikilink")[紐約州州長](../Page/紐約州.md "wikilink")[艾略特·斯皮策因](../Page/艾略特·斯皮策.md "wikilink")[嫖妓宣佈辭職](../Page/賣淫.md "wikilink")，由副州長[大衛·帕特森繼任](../Page/大衛·帕特森.md "wikilink")。後者將會是紐約州首任黑人州長和美國首任[失明州長](../Page/失明.md "wikilink")。[紐約時報](http://cityroom.blogs.nytimes.com/2008/03/12/full-text-of-spitzer-resignation/)
  - [泰国前](../Page/泰国.md "wikilink")[总理](../Page/泰国总理.md "wikilink")[他信·西那瓦作为](../Page/他信·西那瓦.md "wikilink")[被告出席](../Page/被告.md "wikilink")[泰国大理院就](../Page/泰国大理院.md "wikilink")2003年购地案举行的首次听证会。[新华网](http://news.xinhuanet.com/newscenter/2008-03/13/content_7775381.htm)

## 3月11日

  - [美国](../Page/美国.md "wikilink")[奋进号](../Page/奋进号航天飞机.md "wikilink")[航天飞机在](../Page/航天飞机.md "wikilink")[佛罗里达州](../Page/佛罗里达州.md "wikilink")[肯尼迪航天中心发射升空](../Page/肯尼迪航天中心.md "wikilink")，将运送[日本](../Page/日本.md "wikilink")[希望号实验舱的保管室组件和](../Page/希望号实验舱.md "wikilink")[加拿大的遥控](../Page/加拿大.md "wikilink")[机器人至](../Page/机器人.md "wikilink")[国际空间站](../Page/国际空间站.md "wikilink")。[新华网](http://news.xinhuanet.com/world/2008-03/11/content_7766908.htm)
  - [2008年美國總統選舉](../Page/2008年美國總統選舉.md "wikilink")：[民主黨聯邦參議員](../Page/民主黨_\(美國\).md "wikilink")[巴拉克·奧巴馬在](../Page/巴拉克·奧巴馬.md "wikilink")[密西西比州的](../Page/密西西比州.md "wikilink")[初選和](../Page/初選.md "wikilink")[德克薩斯州的](../Page/德克薩斯州.md "wikilink")[黨團會議勝出](../Page/黨團會議.md "wikilink")。[CNN](http://edition.cnn.com/2008/POLITICS/03/11/miss.primary/index.html)
  - 連接[阿拉伯聯合大公國首都](../Page/阿拉伯聯合大公國.md "wikilink")[阿布扎比和](../Page/阿布扎比.md "wikilink")[杜拜的公路發生](../Page/杜拜.md "wikilink")150輛汽車相撞的事故，造成4人死亡，250人受傷。[洛杉磯時報](http://latimesblogs.latimes.com/babylonbeyond/2008/03/united-arab-e-2.html)

## 3月10日

  - 天主教會公佈新的[大罪名單](../Page/大罪.md "wikilink")，包括爭取過度的財富、導致社會不公、污染環境、基因改造和吸毒等。[AHN](https://web.archive.org/web/20080314014233/http://www.allheadlinenews.com/articles/7010282589)
  - 根据[西班牙议会选举的初步计票结果显示](../Page/西班牙.md "wikilink")，现任[首相](../Page/西班牙首相.md "wikilink")[萨帕特罗所领导的](../Page/何塞·路易斯·罗德里格斯·萨帕特罗.md "wikilink")[工人社会党获胜](../Page/西班牙工人社會黨.md "wikilink")，萨帕特罗将连任首相。[彭博](http://www.bloomberg.com/apps/news?pid=20601085&sid=av27I.FGfe58&refer=europe)[新华网](http://news.xinhuanet.com/newscenter/2008-03/10/content_7755714.htm)
  - [馬爾他](../Page/馬爾他.md "wikilink")[大選](../Page/2008年馬爾他大選.md "wikilink")，執政[國民黨擊敗](../Page/國民黨.md "wikilink")[工黨連任](../Page/工黨.md "wikilink")。[今日美國報](http://www.usatoday.com/news/world/2008-03-09-malta-elections_N.htm)
  - 再次当选的[马来西亚](../Page/马来西亚.md "wikilink")[总理](../Page/马来西亚总理.md "wikilink")[巴达维在位于首都](../Page/阿卜杜拉·艾哈迈德·巴达维.md "wikilink")[吉隆坡的](../Page/吉隆坡.md "wikilink")[国家王宫宣誓就职](../Page/国家皇宫_\(吉隆坡\).md "wikilink")。[新华社](http://news.xinhuanet.com/newscenter/2008-03/10/content_7757929.htm)

## 3月9日

  - [欧洲首艘](../Page/欧洲.md "wikilink")[自动货运飞船在](../Page/自动货运飞船.md "wikilink")[法属圭亚那](../Page/法属圭亚那.md "wikilink")[库鲁航天中心搭乘](../Page/库鲁航天中心.md "wikilink")[阿丽亚娜-5ES型火箭升空](../Page/亞利安五號運載火箭.md "wikilink")，将运送货物到[国际空间站](../Page/国际空间站.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-03/09/content_7750511.htm)
  - [马来西亚选举委员会的初步计票结果显示](../Page/马来西亚.md "wikilink")，[执政党联盟](../Page/执政党.md "wikilink")[国民阵线在](../Page/國民陣線_\(馬來西亞\).md "wikilink")[国会选举中获得超过半数的议席](../Page/2008年馬來西亞大選.md "wikilink")，获得组建新政府的资格，但失去了三分之二多數，同時在[檳城](../Page/檳城.md "wikilink")、[雪蘭莪](../Page/雪蘭莪.md "wikilink")、[霹靂](../Page/霹靂州.md "wikilink")、[吉打和](../Page/吉打.md "wikilink")[吉蘭丹失去執政地位](../Page/吉蘭丹.md "wikilink")。顯示出華裔及印度裔族群對政府不滿。[BBC](http://news.bbc.co.uk/2/hi/asia-pacific/7284682.stm)[新华网](http://news.xinhuanet.com/world/2008-03/09/content_7748459.htm)
  - [美國總統](../Page/美國.md "wikilink")[小布什否決國會參眾兩院通過的法案](../Page/小布什.md "wikilink")，堅持用水刑等酷刑手段，向嫌疑恐怖分子逼供。[國際先驅論壇報](http://www.iht.com/articles/2008/03/09/america/09policy.php)
  - [臺灣第二條捷運系統](../Page/臺灣.md "wikilink")[高雄捷運的第一條主線](../Page/高雄捷運.md "wikilink")[紅線通車](../Page/高雄捷運紅線.md "wikilink")

## 3月8日

  - [塞尔维亚](../Page/塞尔维亚.md "wikilink")[总理](../Page/总理.md "wikilink")[科什图尼察因所领导的](../Page/沃伊斯拉夫·科什图尼察.md "wikilink")[联合政府内部在](../Page/联合政府.md "wikilink")[科索沃独立以及与](../Page/科索沃独立.md "wikilink")[欧盟的关系问题上有分歧而辞职](../Page/欧盟.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-03/09/content_7748316.htm)

## 3月7日

  - [俄罗斯中央选举委员会公布](../Page/俄罗斯.md "wikilink")[总统选举正式结果](../Page/2008年俄罗斯总统选举.md "wikilink")，[梅德韦杰夫当选新一任](../Page/德米特里·梅德韦杰夫.md "wikilink")[总统](../Page/俄罗斯总统.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-03/08/content_7741209.htm)
  - 获得连任的[捷克](../Page/捷克.md "wikilink")[总统](../Page/捷克总统.md "wikilink")[瓦茨拉夫·克劳斯在](../Page/瓦茨拉夫·克劳斯.md "wikilink")[布拉格总统府正式宣誓就职](../Page/布拉格.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-03/07/content_7741119.htm)
  - 第20届[里约集团首脑会议在](../Page/里约集团.md "wikilink")[多米尼加首都](../Page/多米尼加.md "wikilink")[圣多明各举行](../Page/圣多明各.md "wikilink")，会议通过了《圣多明各宣言》，[哥伦比亚在宣言中就](../Page/哥伦比亚.md "wikilink")3月1日的越境行动向[厄瓜多尔道歉](../Page/厄瓜多尔.md "wikilink")。随后，哥伦比亚总统[乌里韦与](../Page/阿尔瓦罗·乌里韦·贝莱斯.md "wikilink")[委内瑞拉总统](../Page/委内瑞拉.md "wikilink")[查韦斯](../Page/烏戈·查維茲.md "wikilink")、[尼加拉瓜总统](../Page/尼加拉瓜.md "wikilink")[奥尔特加](../Page/丹尼尔·奥尔特加.md "wikilink")、厄瓜多尔总统[科雷亚握手言和](../Page/拉斐尔·科雷亚.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-03/08/content_7745865.htm)
    [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_7280000/newsid_7284700/7284795.stm)[新华网](http://news.xinhuanet.com/newscenter/2008-03/08/content_7746943.htm)

## 3月6日

  - 一名[巴勒斯坦槍手闖入](../Page/巴勒斯坦.md "wikilink")[耶路撒冷一所](../Page/耶路撒冷.md "wikilink")[猶太教](../Page/猶太教.md "wikilink")[神學院圖書館](../Page/神學院.md "wikilink")，對館內學生掃射，造成八死九傷，槍手當場遭該校校友擊斃，[哈瑪斯已坦承不諱](../Page/哈瑪斯.md "wikilink")。[自由時報](https://web.archive.org/web/20080311012253/http://www.libertytimes.com.tw/2008/new/mar/8/today-int1.htm)
  - [紐約期油收市報](../Page/紐約期油.md "wikilink")104.52美元一桶，創歷史新高。[環球郵報](http://www.theglobeandmail.com/servlet/story/RTGAM.20080305.r-oil06/BNStory/energy/home)
  - 据[菲律宾](../Page/菲律宾.md "wikilink")[卫生部的数据](../Page/卫生部.md "wikilink")，[卡兰巴市自](../Page/卡兰巴市.md "wikilink")2月16日爆发[伤寒疫情至今](../Page/伤寒.md "wikilink")，已有1477人出现伤寒症状，37人证实患上伤寒。[法新社](http://afp.google.com/article/ALeqM5jtaWJm1gKeIYCcQmH1wyFKjNINSA)
    [中国新闻网](https://web.archive.org/web/20080929024305/http://estate.chinanews.com.cn/gj/yt/news/2008/03-06/1184273.shtml)
  - [联合国教科文组织举行](../Page/联合国教科文组织.md "wikilink")2008年度[世界杰出女科学家奖颁奖仪式](../Page/世界杰出女科学家奖.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-03/07/content_7737386.htm)

## 3月5日

  - [格魯吉亞的](../Page/格魯吉亞.md "wikilink")[南奧塞梯宣佈獨立](../Page/南奧塞梯.md "wikilink")，正要求[歐盟](../Page/歐盟.md "wikilink")、[俄羅斯及](../Page/俄羅斯.md "wikilink")[聯合國承認其獨立地位](../Page/聯合國.md "wikilink")。
    [明報](https://web.archive.org/web/20080308232331/http://hk.news.yahoo.com/080305/12/2q0tl.html)
  - 《[福布斯](../Page/福布斯.md "wikilink")》[雜誌公布](../Page/雜誌.md "wikilink")[2008年度全球富豪榜](../Page/2008年億萬富翁列表.md "wikilink")，[美国](../Page/美国.md "wikilink")[投资家](../Page/投资家.md "wikilink")[沃倫·巴菲特成為世界首富](../Page/沃倫·巴菲特.md "wikilink")，[墨西哥电信巨头](../Page/墨西哥.md "wikilink")[卡洛斯·斯利姆和](../Page/卡洛斯·斯利姆·埃卢.md "wikilink")[微软公司董事长](../Page/微软公司.md "wikilink")[比尔·盖茨分列第二](../Page/比尔·盖茨.md "wikilink")、三位。[路透社](http://www.reuters.com/article/businessNews/idUSN0564885820080305)[新华社](http://news.xinhuanet.com/newscenter/2008-03/06/content_7729012.htm)
  - [第十一届全国人民代表大会第一次会议在](../Page/第十一届全国人民代表大会.md "wikilink")[北京人民大会堂开幕](../Page/北京人民大会堂.md "wikilink")。[新华网](http://news.xinhuanet.com/misc/2008-03/05/content_7721864.htm)

## 3月4日

  - [2008年美國總統選舉](../Page/2008年美國總統選舉.md "wikilink")：[約翰·麥凱恩肯定能夠成為代表](../Page/約翰·麥凱恩.md "wikilink")[共和黨的候選人](../Page/共和黨_\(美國\).md "wikilink")，而[民主黨方面](../Page/民主黨_\(美國\).md "wikilink")，[希拉里·克林頓在](../Page/希拉里·克林頓.md "wikilink")[羅德島](../Page/羅德島州.md "wikilink")、[俄亥俄和](../Page/俄亥俄州.md "wikilink")[德克薩斯州取勝](../Page/德克薩斯州.md "wikilink")，重燃入主白宮的希望。[CNN](http://www.cnn.com/2008/POLITICS/03/04/march.4.contests/index.html)[纽约时报](http://www.nytimes.com/2008/03/05/us/politics/05primary.html?_r=1&hp&oref=slogin)

## 3月3日

  - 针对[哥伦比亚](../Page/哥伦比亚.md "wikilink")1日越境在[厄瓜多尔境内打死](../Page/厄瓜多尔.md "wikilink")[哥伦比亚革命武装力量二号人物](../Page/哥伦比亚革命武装力量.md "wikilink")[劳尔·雷耶斯的军事行动](../Page/劳尔·雷耶斯.md "wikilink")，厄瓜多尔宣布与哥伦比亚断绝外交关系，[委内瑞拉驱逐哥伦比亚所派驻的全部外交官](../Page/委内瑞拉.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2008-03/04/content_7713554.htm)[新华网](http://news.xinhuanet.com/newscenter/2008-03/04/content_7714097.htm)
  - 根据[俄罗斯中央选举委员会对](../Page/俄罗斯.md "wikilink")[总统选举](../Page/2008年俄罗斯总统选举.md "wikilink")99.45%选票的初步统计，[统一俄罗斯党总统候选人](../Page/统一俄罗斯党.md "wikilink")、现任第一副总理[梅德韦杰夫以](../Page/德米特里·梅德韦杰夫.md "wikilink")70.23%的得票率获胜。[新华网](http://news.xinhuanet.com/newscenter/2008-03/03/content_7709354.htm)
  - [中国人民政治协商会议第十一届全国委员会第一次会议在](../Page/中国人民政治协商会议.md "wikilink")[北京](../Page/北京.md "wikilink")[人民大会堂开幕](../Page/人民大会堂.md "wikilink")。[新华网](http://news.xinhuanet.com/misc/2008-03/03/content_7710056.htm)

## 3月2日

  - [委內瑞拉總統](../Page/委內瑞拉.md "wikilink")[烏戈·查韋斯抗議](../Page/烏戈·查韋斯.md "wikilink")[哥倫比亞對藏身](../Page/哥倫比亞.md "wikilink")[厄瓜多爾的哥國左翼游擊隊越境攻擊](../Page/厄瓜多爾.md "wikilink")，決定關閉駐哥國大使館並派出坦克到委哥邊境。[彭博](http://www.bloomberg.com/apps/news?pid=20601086&sid=a4PNTPV2Ccmw&refer=latin_america)

## 3月1日

  - 第十一屆[中國](../Page/中華人民共和國.md "wikilink")[全國人民代表大會代表名單公佈](../Page/全國人民代表大會.md "wikilink")，包含[胡錦濤等共](../Page/胡錦濤.md "wikilink")2987名。[TOM新聞-新華網](https://web.archive.org/web/20080304134535/http://news.tom.com/2008-03-01/OK3Q/84801672.html)[中國人大網](http://www.npc.gov.cn/npc/xinwen/dbgz/wj/2008-02/29/content_1401314.htm)
  - [伊朗](../Page/伊朗.md "wikilink")[總統](../Page/伊朗总统.md "wikilink")[馬哈茂德·艾哈邁迪內賈德抵达](../Page/馬哈茂德·艾哈邁迪內賈德.md "wikilink")[巴格达](../Page/巴格达.md "wikilink")，对[伊拉克进行访问](../Page/伊拉克.md "wikilink")，这是1979年[伊朗伊斯蘭革命以来伊朗](../Page/伊朗伊斯蘭革命.md "wikilink")[国家元首首次出访伊拉克](../Page/国家元首.md "wikilink")。[REUTERS](http://www.reuters.com/article/newsOne/idUSCOL13118420080301)[新华网](http://news.xinhuanet.com/newscenter/2008-03/02/content_7702121.htm)
  - 英国[哈里王子在](../Page/哈里王子.md "wikilink")[阿富汗赫尔曼德省南部服役](../Page/阿富汗.md "wikilink")10个星期后回到英国。[新华网](http://news.xinhuanet.com/newscenter/2008-03/02/content_7701823.htm)
  - [亚美尼亚](../Page/亚美尼亚.md "wikilink")[总统](../Page/亚美尼亚总统.md "wikilink")[科恰良宣布](../Page/罗伯特·科恰良.md "wikilink")，首都[埃里温开始进入为期](../Page/埃里温.md "wikilink")20天的[紧急状态](../Page/紧急状态.md "wikilink")，以防止[亚美尼亚总统选举结束后反对派的](../Page/2008年亚美尼亚总统选举.md "wikilink")[示威活动所造成的社会动荡](../Page/示威.md "wikilink")。[新华网](http://news.xinhuanet.com/world/2008-03/02/content_7702362.htm)

[Category:2008年](../Category/2008年.md "wikilink")
[Category:3月](../Category/3月.md "wikilink")