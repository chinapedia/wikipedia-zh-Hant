**2008年NBA总决赛**是[2007-08
NBA賽季的最後一個系列賽](../Page/2007-08_NBA賽季.md "wikilink")，由[東部聯盟冠軍](../Page/東部聯盟_\(NBA\).md "wikilink")[波士頓塞爾特人與](../Page/波士頓塞爾特人.md "wikilink")[西部聯盟冠軍](../Page/西部聯盟_\(NBA\).md "wikilink")[洛杉磯湖人對決](../Page/洛杉磯湖人.md "wikilink")。比賽采用七場四勝制2-3-2的形式，[常規賽成績較優的](../Page/NBA常规赛.md "wikilink")[波士頓凱爾特人](../Page/波士頓凱爾特人.md "wikilink")（66–16）將獲得多一個主場的優勢，系列賽的第一、二、六（如果需要），七（如果需要）場比賽在他們主場進行，而本年亦是自1997年以來，[東部聯盟冠軍首次擁有](../Page/東部聯盟_\(NBA\).md "wikilink")[主場優勢](../Page/主場優勢.md "wikilink")。

本屆總決賽戲碼是繼1987年以後，再一次由凱爾特人及湖人對決，亦是歷史上2支奪得總冠軍次數最多的球隊對疊，被人譽為「夢幻對決」。最終，塞爾特人以4比2的成績擊敗湖人，贏得22年來首個NBA總冠軍，[保罗·皮尔斯則獲得本年總決賽的最有價值球員獎](../Page/保罗·皮尔斯.md "wikilink")。

在至關重要的第四場，波士頓在一度落後24分的情況下成功反超，最終取勝。這是NBA總決賽歷史上迄今為止的單場比賽的最大逆轉。

## 總決賽之路

<table style="width:8%;">
<colgroup>
<col style="width: 4%" />
<col style="width: 4%" />
</colgroup>
<thead>
<tr class="header">
<th><p>洛杉磯湖人</p></th>
<th><p>波士頓塞爾特人</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>57-25 (.695)<br />
太平洋賽區第一名，西部第一名，全聯盟第三名</p></td>
<td><p><a href="../Page/2007-08_NBA賽季.md" title="wikilink">常規賽</a></p></td>
</tr>
<tr class="even">
<td><p>4-0擊敗<a href="../Page/丹佛掘金.md" title="wikilink">-{zh-hans:丹佛掘金;zh-hk:丹佛金塊;zh-tw:丹佛金塊;}-</a></p></td>
<td><p>第一輪</p></td>
</tr>
<tr class="odd">
<td><p>4-2擊敗<a href="../Page/犹他爵士.md" title="wikilink">犹他爵士</a></p></td>
<td><p>分區半決賽</p></td>
</tr>
<tr class="even">
<td><p>4-1擊敗<a href="../Page/圣安东尼奥马刺.md" title="wikilink">圣安东尼奥马刺</a></p></td>
<td><p>分區決賽</p></td>
</tr>
</tbody>
</table>

### 常規賽對陣

[波士頓塞爾特人在兩隊之間的常規賽對陣中全部勝出](../Page/波士頓塞爾特人.md "wikilink")。

### 晉級

  - [波士頓塞爾特人以](../Page/波士頓塞爾特人.md "wikilink")4-2总比分击败[底特律活塞赢得](../Page/底特律活塞.md "wikilink")2008年的东部分区决赛。
  - [洛杉磯湖人以](../Page/洛杉磯湖人.md "wikilink")4-1总比分击败[聖安東尼奧馬刺赢得](../Page/聖安東尼奧馬刺.md "wikilink")2008年的西部分区决赛。

## 形式

这次[七场四胜制的系列賽于](../Page/七场四胜制.md "wikilink")2008年6月6日开始。而七場四勝再不是以[2-2-1-1-1排列](../Page/2-2-1-1-1.md "wikilink")，而是[2-3-2排列](../Page/2-3-2.md "wikilink")。降低優先球隊的主場優勢。

## 電視轉播

除了[ABC](../Page/NBA_on_ABC.md "wikilink")（美國）和[TSN](../Page/The_Sports_Network.md "wikilink")（加拿大）為本年NBA總決賽進行電視轉播外，全球各地電視轉播如下：\[1\]

  - : [Canal 7](../Page/Canal_7_Argentina.md "wikilink")

  - : [ESPN](../Page/ESPN_Australia.md "wikilink")

  - : [Be 1](../Page/Be_1.md "wikilink") and [Prime Sport
    1](../Page/Prime_Sport_1.md "wikilink")

  - : [ESPN
    Brasil](../Page/ESPN_Brasil.md "wikilink")，[Globosporte.com](../Page/Globosporte.com.md "wikilink")，[TV
    Esporte Interativo](../Page/TV_Esporte_Interativo.md "wikilink")

  - : [CCTV-5以及多个地方电视台體育頻道](../Page/CCTV-5.md "wikilink")

  - : [DK4 Sport](../Page/DK4_Sport.md "wikilink")

  - : [Antena Latina](../Page/Antena_Latina.md "wikilink")

  - : [Urheilu TV](../Page/Urheilu_TV.md "wikilink")

  - : [Sport+](../Page/Canal+.md "wikilink")

  - : [Premiere Sport](../Page/Premiere_Sport.md "wikilink")

  - : [Sport+](../Page/Sport+.md "wikilink")

  - :
    [ESPN](../Page/ESPN.md "wikilink")，[香港有線電視體育台](../Page/香港有線電視體育平台#香港有線電視體育台.md "wikilink")，[衛視體育台](../Page/衛視體育台.md "wikilink")，[無綫電視](../Page/無綫電視.md "wikilink")[明珠台和](../Page/明珠台.md "wikilink")[高清翡翠台](../Page/高清翡翠台.md "wikilink")

  - : [Star Sports](../Page/Star_Sports.md "wikilink")

  - : [ESPN](../Page/ESPN.md "wikilink")，[Jak
    TV](../Page/Jak_TV.md "wikilink")，[Star
    Sports](../Page/Star_Sports.md "wikilink")

<!-- end list -->

  - : [Sport 5](../Page/Sport_5.md "wikilink")

  - : [J Sports 1](../Page/J_Sports_1.md "wikilink")，[NHK
    BS-1](../Page/NHK_BS-1.md "wikilink")，[SkyPerfecTV](../Page/SkyPerfecTV.md "wikilink")

  - : [SBS Sports](../Page/SBS_Sports.md "wikilink")，[Star
    Sports](../Page/Star_Sports.md "wikilink")

  - : [TVC](../Page/TVC_Mexico.md "wikilink")

  - [中东地区](../Page/中东地区.md "wikilink")：[ART Sport
    3](../Page/ART_Sport_3.md "wikilink")

  - : [Sport1](../Page/Sport1.md "wikilink")

  - : [C/S Sports on
    RPN](../Page/Radio_Philippines_Network.md "wikilink")，[Basketball
    TV](../Page/Basketball_TV.md "wikilink")

  - : [Canal+Sport1](../Page/Canal+_Poland.md "wikilink")

  - : [Sport TV 1](../Page/Sport_TV_1.md "wikilink")

  - : [NBA TV](../Page/NITV_Plus.md "wikilink")

  - : [Canal
    Plus](../Page/Canal+_Spain.md "wikilink")，[Cuatro](../Page/Cuatro_\(channel\).md "wikilink")

  - : [TV4 AB](../Page/TV4_AB.md "wikilink")

  - : [緯來電視網](../Page/緯來電視網.md "wikilink")，[Star
    Sports](../Page/Star_Sports.md "wikilink")

  - : [NTV](../Page/NTV_Turkey.md "wikilink")

  - : [Five](../Page/Five_\(channel\).md "wikilink")

  - : [Sport Plus](../Page/Sport_Plus.md "wikilink")

## 球員名單

### [波士頓塞爾特人](../Page/波士頓塞爾特人.md "wikilink")

### [洛杉磯湖人](../Page/洛杉磯湖人.md "wikilink")

<table>
<tbody>
<tr class="odd">
<td><p><big><font color="#ffcd35"><strong>洛杉磯湖人<br />
2008年NBA總決賽球員名單</strong></font></big></p></td>
</tr>
<tr class="even">
<td><p>主教练：<a href="../Page/菲尔·杰克逊.md" title="wikilink">菲尔·杰克逊</a>（Phil Jackson）</p></td>
</tr>
<tr class="odd">
<td><p><font color="#ffcd35">位置</font></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>SF</p></td>
</tr>
<tr class="even">
<td><p>SG</p></td>
</tr>
<tr class="odd">
<td><p>C</p></td>
</tr>
<tr class="even">
<td><p>PG</p></td>
</tr>
<tr class="odd">
<td><p>PG</p></td>
</tr>
<tr class="even">
<td><p>PF/C</p></td>
</tr>
<tr class="odd">
<td><p>PG/SG</p></td>
</tr>
<tr class="even">
<td><p>C</p></td>
</tr>
<tr class="odd">
<td><p>C</p></td>
</tr>
<tr class="even">
<td><p>SF</p></td>
</tr>
<tr class="odd">
<td><p>PF</p></td>
</tr>
<tr class="even">
<td><p>SF/PF</p></td>
</tr>
<tr class="odd">
<td><p>PF</p></td>
</tr>
<tr class="even">
<td><p>SG</p></td>
</tr>
<tr class="odd">
<td><p>SF</p></td>
</tr>
</tbody>
</table>

## 比賽摘要

### (E1)[波士顿凯尔特人](../Page/波士顿凯尔特人.md "wikilink") vs. (W1)[洛杉磯湖人](../Page/洛杉磯湖人.md "wikilink")

所有時間為北京时间。

### 第一場

### 第二場

### 第三場

### 第四場

### 第五場

### 第六場

[2008FinalsGm6End.JPG](https://zh.wikipedia.org/wiki/File:2008FinalsGm6End.JPG "fig:2008FinalsGm6End.JPG")支持者、球員、教練團成員和工作人員慶祝球隊歷來第17個NBA總冠軍\]\]

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [2008年NBA總決賽官方網站](http://www.nba.com/finals2008/)

[it:NBA Playoffs 2008\#NBA Finals: Boston Celtics - Los Angeles
Lakers](../Page/it:NBA_Playoffs_2008#NBA_Finals:_Boston_Celtics_-_Los_Angeles_Lakers.md "wikilink")

[Category:NBA總決賽](../Category/NBA總決賽.md "wikilink")
[總決賽](../Category/2007-08_NBA賽季.md "wikilink")
[N](../Category/2008年体育.md "wikilink")
[Category:2008年加利福尼亚州](../Category/2008年加利福尼亚州.md "wikilink")
[Category:21世纪洛杉矶](../Category/21世纪洛杉矶.md "wikilink")

1.  [Global NBA
    Programming](http://www.nba.com/schedules/international_nba_tv_schedule.html)，NBA.com