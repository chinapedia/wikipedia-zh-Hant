## 公開水域游泳

### 參賽資格

  - 男女項目各有25人參賽，各個國家及地區奧委會只限男女各2人參加，除了主辦國挑選的選手外，所有選手必須透過世界公開水域游泳錦標賽及奧運資格賽取得參賽名額。

### 男子公開水域游泳

以下為取得男子公開水域游泳參賽資格的選手：

<table style="width:88%;">
<colgroup>
<col style="width: 23%" />
<col style="width: 23%" />
<col style="width: 9%" />
<col style="width: 17%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th><p>選手</p></th>
<th><p>出線資格</p></th>
<th><p>參賽名額</p></th>
<th><p>資格賽舉行日期</p></th>
<th><p>資格賽舉行地點</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>主辦國</p></td>
<td><p>1</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Vladimir Dyatchin<br />
 David Davies<br />
 Thomas Lurz<br />
 Maarten van der Weijden<br />
 Evgeny Drattsev<br />
 Ky Hurst<br />
 Mark Warkentin<br />
 Valerio Cleri<br />
 Gianniotis Spridon<br />
 Brian Ryckeman</p></td>
<td><p><a href="https://web.archive.org/web/20080528140737/http://www.sevillaaguasabiertas.org/includes_general/10_km_Men_Results.pdf">FINA世界公開水域游泳錦標賽</a><br />
最佳成績首10名</p></td>
<td><p>10</p></td>
<td><p>2008年5月3日至5月4日</p></td>
<td><p><a href="../Page/西維爾.md" title="wikilink">西維爾</a></p></td>
</tr>
<tr class="odd">
<td><p>Gilles Rondy<br />
 Luis Escobar<br />
 Mohamed El Zanaty<br />
 Saleh Mohammad</p></td>
<td><p>FINA世界公開水域游泳錦標賽<br />
洲際代表<a href="../Page/#fn_1.md" title="wikilink"><sup>1</sup></a></p></td>
<td><p>5<a href="../Page/#fn_2.md" title="wikilink"><sup>2</sup></a></p></td>
<td><p>2008年5月3日至5月4日</p></td>
<td><p><a href="../Page/西維爾.md" title="wikilink">西維爾</a></p></td>
</tr>
<tr class="even">
<td><p>Petar Stoychev<br />
 Csaba Gercsak<br />
 Rostislav Vitek<br />
 Chad Ho<br />
 Erwin Maldonado<br />
 Allan Do Carmo<br />
 Damian Blaum<br />
 Jose Francisco Hervas<br />
 Arseniy Lavrentyev <a href="../Page/#fn_2.md" title="wikilink"><sup>2</sup></a><br />
</p></td>
<td><p><a href="https://web.archive.org/web/20090212124412/http://www.swimmarathon2008.org.cn/en/index.html">FINA公開水域游泳奧運資格賽</a>出線選手<a href="../Page/#fn_3.md" title="wikilink"><sup>3</sup></a></p></td>
<td><p>9</p></td>
<td><p>2008年5月31日至6月1日</p></td>
<td><p><a href="../Page/北京.md" title="wikilink">北京</a></p></td>
</tr>
<tr class="odd">
<td><p>總數</p></td>
<td></td>
<td><p>25</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

  - <span style="font-size:85%">

  - 在世錦賽中5大洲各1名尚未獲得資格的、而10名之外成績最佳的選手可以獲得參賽資格。

  - 由於是賽事中唯一的[大洋洲的國家代表](../Page/大洋洲.md "wikilink")，而澳洲代表已經以賽事最佳成績首10名獲取參賽資格，因此另一個參賽名額改由FINA公開水域游泳奧運資格賽最佳成績者。

  - 未獲得參賽資格的國家及地區奧委會可以在每個項目可派2名運動員參賽，所有剩餘資格席位在該賽事按成績分配。

### 女子公開水域游泳

以下為取得女子公開水域游泳參賽資格的選手：

<table style="width:88%;">
<colgroup>
<col style="width: 23%" />
<col style="width: 23%" />
<col style="width: 9%" />
<col style="width: 17%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th><p>選手</p></th>
<th><p>出線資格</p></th>
<th><p>參賽名額</p></th>
<th><p>資格賽舉行日期</p></th>
<th><p>資格賽舉行地點</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>主辦國</p></td>
<td><p>1</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Larisa Ilchenko<br />
 Cassandra Patten<br />
 Yurema Requena<br />
 Natalie Du Toit<br />
 Jana Pechanova<br />
 Poliana Okimoto<br />
 Angela Maurer<br />
 Keri Anne Payne<br />
 Aurelie Muller<br />
 Ana Marcela Cunha</p></td>
<td><p><a href="https://web.archive.org/web/20090105200736/http://www.sevillaaguasabiertas.org/includes_general/10_km_Women_Results.pdf">FINA世界公開水域游泳錦標賽</a><br />
最佳成績首10名</p></td>
<td><p>10<a href="../Page/#fn_1.md" title="wikilink"><sup>1</sup></a></p></td>
<td><p>2008年5月3日至5月4日</p></td>
<td><p><a href="../Page/西維爾.md" title="wikilink">西維爾</a></p></td>
</tr>
<tr class="odd">
<td><p>Edith van Dijk<br />
 <a href="../Page/方晏喬.md" title="wikilink">方晏喬</a><br />
 Andreina Pinto<br />
 Melissa Gorman</p></td>
<td><p>FINA世界公開水域游泳錦標賽<br />
洲際代表<a href="../Page/#fn_2.md" title="wikilink"><sup>2</sup></a></p></td>
<td><p>5<a href="../Page/#fn_2.md" title="wikilink"><sup>2</sup></a></p></td>
<td><p>2008年5月3日至5月4日</p></td>
<td><p><a href="../Page/西維爾.md" title="wikilink">西維爾</a></p></td>
</tr>
<tr class="even">
<td><p>Chloe Sutton<br />
 Martina Grimaldi<br />
 Marianna Lymperta<br />
 Nataliya Samorodina<br />
 Kristel Kobrich<br />
 Imelda Martinez<br />
 Eva Berglund<br />
 Teja Zupan<br />
 Antonella Bogarin**<br />
 Daniela Inacio***</p></td>
<td><p><a href="https://web.archive.org/web/20090212124412/http://www.swimmarathon2008.org.cn/en/index.html">FINA公開水域游泳奧運資格賽</a><br />
出線選手</p></td>
<td><p>9</p></td>
<td><p>2008年5月31日至6月1日</p></td>
<td><p><a href="../Page/北京.md" title="wikilink">北京</a></p></td>
</tr>
<tr class="odd">
<td><p>總數</p></td>
<td></td>
<td><p>25</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 接力項目

### 參賽資格

  - 每個接力項目最多16支隊伍參賽，各奧委會在每個接力項目上最多只能報1隊，獲得個人賽資格的選手可以報名接力項目。獲得一個
    接力項目參賽資格的奧委會還可以額外報兩人，兩個接力項目可多報4人，3個接力項目可多報6人，4個接力項目可多報10人，5個接力項目可多
    報12人，6個接力項目可多報16人，但這些選手必須達B標。
  - [2007年世界游泳錦標賽接力項目成為本屆賽事的預賽](../Page/2007年世界游泳錦標賽.md "wikilink")，每個小項的初12名完成者均會獲得參賽資格，其餘4支隊伍會由國際泳聯在奧運會舉行前15個月內，在國際泳聯認可國際比賽中4支成績最好的隊伍獲得。

以下為取得各項游泳接力項目參賽資格的國家，方格內的@是代表該國家取得該項目的參賽資格：

<table style="width:88%;">
<colgroup>
<col style="width: 14%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p><a href="../Page/2008年夏季奧林匹克運動會游泳比賽_-_男子4_x_100米自由泳接力.md" title="wikilink">男子4 x 100米<br />
自由泳接力</a>[1]</p></th>
<th><p><a href="../Page/2008年夏季奧林匹克運動會游泳比賽_-_男子4_x_200米自由泳接力.md" title="wikilink">男子4 x 200米<br />
自由泳接力</a>[2]</p></th>
<th><p><a href="../Page/2008年夏季奧林匹克運動會游泳比賽_-_男子4_x_100米混合泳接力.md" title="wikilink">男子4 x 100米<br />
混合泳接力</a>[3]</p></th>
<th><p><a href="../Page/2008年夏季奧林匹克運動會游泳比賽_-_女子4_x_100米自由泳接力.md" title="wikilink">女子4 x 100米<br />
自由泳接力</a>[4]</p></th>
<th><p><a href="../Page/2008年夏季奧林匹克運動會游泳比賽_-_女子4_x_200米自由泳接力.md" title="wikilink">女子4 x 200米<br />
自由泳接力</a>[5]</p></th>
<th><p><a href="../Page/2008年夏季奧林匹克運動會游泳比賽_-_女子4_x_100米混合泳接力.md" title="wikilink">女子4 x 100米<br />
混合泳接力</a>[6]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>@</p></td>
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td></td>
<td><p>@</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td></td>
<td></td>
<td><p>@</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>@</p></td>
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>@</p></td>
<td></td>
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td></td>
<td><p>@</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td><p>@</p></td>
<td></td>
<td></td>
<td><p>@</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td><p>@</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>@</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>@</p></td>
<td></td>
<td></td>
<td><p>@</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>@</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>@</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>@</p></td>
<td></td>
<td></td>
<td><p>@</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td><p>@</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>@</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>@</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>@</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>共计：28個國家</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部链接

  - [2008年北京奧運](https://web.archive.org/web/20060331010211/http://www.beijing2008.com/)
  - [國際泳聯](http://www.fina.org)

[Category:2008年夏季奧林匹克運動會游泳比賽](../Category/2008年夏季奧林匹克運動會游泳比賽.md "wikilink")

1.
2.
3.
4.
5.
6.