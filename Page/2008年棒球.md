## 賽事分類

### 職業棒球

  - [2008年美國職棒大聯盟](../Page/2008年.md "wikilink")
  - 2008年美國職棒小聯盟
  - 2008年[日本職棒](../Page/日本職棒.md "wikilink")
  - 2008年[韓國職棒](../Page/韓國職棒.md "wikilink")
  - 2008年[中華職棒](../Page/中華職棒.md "wikilink")
  - [7月15日](../Page/7月15日.md "wikilink")，2008年美國職棒大聯盟明星賽（2008 Major
    League Baseball All-Star
    Game）於[美國](../Page/美國.md "wikilink")[紐約](../Page/紐約.md "wikilink")[洋基體育場舉行](../Page/洋基體育場.md "wikilink")。
  - [7月20日](../Page/7月20日.md "wikilink")，[2008年中華職棒明星賽於](../Page/2008年中華職棒明星賽.md "wikilink")[臺中洲際棒球場舉行](../Page/臺中洲際棒球場.md "wikilink")
  - [10月22日起](../Page/10月22日.md "wikilink")，[2008年世界大賽](../Page/2008年世界大賽.md "wikilink")（2008
    World Series）
  - 2008年台灣大賽
  - 2008年日本大賽
  - 2008年韓國大賽
  - [11月13日至](../Page/11月13日.md "wikilink")[11月16日](../Page/11月16日.md "wikilink")，[2008年亞洲職棒大賽於](../Page/2008年亞洲職棒大賽.md "wikilink")[日本](../Page/日本.md "wikilink")[東京巨蛋舉行](../Page/東京巨蛋.md "wikilink")。

### 非職業聯賽

  - 2008年中國棒球聯賽

### 國際錦標賽

  - [7月17日至](../Page/7月17日.md "wikilink")[7月27日](../Page/7月27日.md "wikilink")，[2008年世界大學棒球錦標賽於](../Page/2008年世界大學棒球錦標賽.md "wikilink")[捷克的](../Page/捷克.md "wikilink")[布爾諾](../Page/布爾諾.md "wikilink")、[俄斯特拉發](../Page/俄斯特拉發.md "wikilink")、[布蘭斯科](../Page/布蘭斯科.md "wikilink")（Blansko）、丘曾（Choceň）與[奧洛穆克舉行](../Page/奧洛穆克.md "wikilink")。
  - [7月25日至](../Page/7月25日.md "wikilink")[8月3日](../Page/8月3日.md "wikilink")，[2008年世界青棒錦標賽於](../Page/2008年世界青棒錦標賽.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")[亞伯達](../Page/亞伯達.md "wikilink")[艾德蒙頓舉行](../Page/艾德蒙頓.md "wikilink")。
  - [8月13日至](../Page/8月13日.md "wikilink")[8月23日](../Page/8月23日.md "wikilink")，[2008年夏季奧林匹克運動會棒球比賽於](../Page/2008年夏季奧林匹克運動會棒球比賽.md "wikilink")[中國](../Page/中國.md "wikilink")[北京五棵松棒球場舉行](../Page/北京五棵松棒球場.md "wikilink")。
  - [8月24日至](../Page/8月24日.md "wikilink")[8月29日](../Page/8月29日.md "wikilink")，[2008年世界盃女子棒球錦標賽於](../Page/2008年世界盃女子棒球錦標賽.md "wikilink")[日本](../Page/日本.md "wikilink")[松山市舉行](../Page/松山市.md "wikilink")。

### 國際邀請賽

  - [7月4日至](../Page/7月4日.md "wikilink")[7月13日](../Page/7月13日.md "wikilink")，2008年哈連盃國際棒球邀請賽於[荷蘭](../Page/荷蘭.md "wikilink")[哈連的平姆](../Page/哈勒姆.md "wikilink")·穆利爾棒球場（Pim
    Mulierstadion）舉行。

### 其他賽事

  - [7月30日至](../Page/7月30日.md "wikilink")[8月4日](../Page/8月4日.md "wikilink")，2008年世界少年野球大會於[日本](../Page/日本.md "wikilink")[愛知舉行](../Page/愛知.md "wikilink")。
  - [8月2日至](../Page/8月2日.md "wikilink")[8月18日](../Page/8月18日.md "wikilink")，第90屆全國高等學校野球選手權大會於[日本](../Page/日本.md "wikilink")[阪神甲子園球場舉行](../Page/阪神甲子園球場.md "wikilink")。
  - [8月29日至](../Page/8月29日.md "wikilink")[9月9日](../Page/9月9日.md "wikilink")，第79屆都市對抗野球大會於[日本](../Page/日本.md "wikilink")[東京巨蛋舉行](../Page/東京巨蛋.md "wikilink")。

## 大事記

### 2月

  - [2月14日至](../Page/2月14日.md "wikilink")[2月17日](../Page/2月17日.md "wikilink")，[2008年鳳凰盃香港國際女子棒球邀請賽於](../Page/2008年鳳凰盃香港國際女子棒球邀請賽.md "wikilink")[香港](../Page/香港.md "wikilink")[藍田](../Page/藍田_\(香港\).md "wikilink")[晒草灣棒球場舉行](../Page/曬草灣遊樂場.md "wikilink")，是[香港首度舉辦國際女子棒球比賽](../Page/香港.md "wikilink")。

### 3月

  - [3月7日至](../Page/3月7日.md "wikilink")[3月14日](../Page/3月14日.md "wikilink")，2008年夏季奧運棒球最終資格排名賽於[台灣](../Page/台灣.md "wikilink")[臺中洲際棒球場與](../Page/臺中洲際棒球場.md "wikilink")[斗六棒球場舉行](../Page/斗六棒球場.md "wikilink")。[加拿大](../Page/加拿大.md "wikilink")、[韓國與](../Page/韓國.md "wikilink")[中華台北爭取到參加](../Page/中華台北.md "wikilink")[2008年夏季奧林匹克運動會棒球比賽的資格](../Page/2008年夏季奧林匹克運動會棒球比賽.md "wikilink")。
  - [3月15日至](../Page/3月15日.md "wikilink")[3月16日](../Page/3月16日.md "wikilink")，美國職棒大聯盟中國賽於[中國](../Page/中國.md "wikilink")[北京五棵松棒球場舉行](../Page/北京五棵松棒球場.md "wikilink")，是[美國職棒大聯盟首度於](../Page/美國職棒大聯盟.md "wikilink")[中國比賽](../Page/中國.md "wikilink")。
  - [3月22日至](../Page/3月22日.md "wikilink")[4月4日](../Page/4月4日.md "wikilink")，第80屆選拔高等學校野球大會於[日本](../Page/日本.md "wikilink")[阪神甲子園球場舉行](../Page/阪神甲子園球場.md "wikilink")，沖縄尚学高等学校獲得此次賽事的優勝。

## 外部連結

[\*](../Category/2008年棒球.md "wikilink")