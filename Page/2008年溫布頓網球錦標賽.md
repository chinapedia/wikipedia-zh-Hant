**2008年溫布頓網球錦標賽**是第132屆[溫布頓網球錦標賽](../Page/溫布頓網球錦標賽.md "wikilink")，也是本年度第三個[大滿貫賽](../Page/網球大滿貫.md "wikilink")。於2008年6月23日至7月6日在[英國](../Page/英國.md "wikilink")[倫敦的](../Page/倫敦.md "wikilink")[全英草地网球和门球俱乐部舉行](../Page/全英草地网球和门球俱乐部.md "wikilink")。

## 成年組

### 男子單打

**[拉斐爾·納達爾](../Page/拉斐爾·納達爾.md "wikilink")** 擊敗
[羅傑·費德勒](../Page/羅傑·費德勒.md "wikilink")，6–4、6–4、6–7(5)、6–7(8)、9–7

  - 納達爾是1980年以來第一位在同一年獲得法國網球公開賽冠軍與溫布頓網球錦標賽冠軍的選手。

### 女子單打

**[大威廉絲](../Page/大威廉絲.md "wikilink")** 擊敗
[小威廉絲](../Page/小威廉絲.md "wikilink")，7–5、6–4

### 男子雙打

**[丹尼爾·內斯特](../Page/丹尼爾·內斯特.md "wikilink")** /
**[內納德·齊莫尼奇](../Page/內納德·齊莫尼奇.md "wikilink")** 擊敗
[約納斯·比約克曼](../Page/約納斯·比約克曼.md "wikilink")／
[凱文·烏利耶特](../Page/凱文·烏利耶特.md "wikilink")，7–6(12)、6–7(3)、6–3、6–3

### 女子雙打

**[大威廉絲](../Page/大威廉絲.md "wikilink")** /
**[小威廉絲](../Page/小威廉絲.md "wikilink")** 擊敗
[麗莎·雷蒙德](../Page/麗莎·雷蒙德.md "wikilink")／
[薩曼莎·斯托瑟](../Page/薩曼莎·斯托瑟.md "wikilink")，6–2、6–2

### 混合雙打

**[鮑勃·布賴恩](../Page/鮑勃·布賴恩.md "wikilink")** /
**[薩曼莎·斯托瑟](../Page/薩曼莎·斯托瑟.md "wikilink")**
擊敗  [邁克·布賴恩](../Page/邁克·布賴恩.md "wikilink")／
[卡塔琳娜·斯雷博特尼克](../Page/卡塔琳娜·斯雷博特尼克.md "wikilink")，7–5、6–4

## 少年組

### 男子單打

### 女子單打

### 男子雙打

### 女子雙打

## 外部連結

  - [官方網站](http://www.wimbledon.org/)
  - [2012温网](https://web.archive.org/web/20120530090459/http://www.taimo.cn/2012WO)



[Category:2008年網球](../Category/2008年網球.md "wikilink")
[Category:温布尔登网球锦标赛](../Category/温布尔登网球锦标赛.md "wikilink")
[Category:2008年溫布頓網球錦標賽](../Category/2008年溫布頓網球錦標賽.md "wikilink")
[Category:2008年英国体育](../Category/2008年英国体育.md "wikilink")