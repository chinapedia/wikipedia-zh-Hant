[Heidfeld_and_Rosberg_-_2008_Melb_GP.jpg](https://zh.wikipedia.org/wiki/File:Heidfeld_and_Rosberg_-_2008_Melb_GP.jpg "fig:Heidfeld_and_Rosberg_-_2008_Melb_GP.jpg")
**2008年澳大利亚大奖赛**是[2008年世界一级方程式锦标赛第一個分站比賽](../Page/2008年世界一级方程式锦标赛.md "wikilink")。於3月14日至3月16日在[墨爾本賽道舉行](../Page/墨爾本賽道.md "wikilink")。

## 賽事詳細

### 星期五練習賽

### 排位賽

### 賽事日

比賽發生多宗嚴重意外，三度使用安全車。更只有7位車手完成整個賽程（由于[鲁本斯·巴里切罗赛后被取消成绩](../Page/鲁本斯·巴里切罗.md "wikilink")，事实上只有6辆赛车完成比赛）。

### 排位賽結果

<table>
<thead>
<tr class="header">
<th><p>位置</p></th>
<th><p>車手</p></th>
<th><p>車隊／引擎</p></th>
<th><p>第一次</p></th>
<th><p>第二次</p></th>
<th><p>第三次</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/劉易斯·咸美頓.md" title="wikilink">劉易斯·咸美頓</a></p></td>
<td><p><a href="../Page/麥拿倫車隊.md" title="wikilink">麥拿倫車隊</a>-<a href="../Page/梅塞德斯.md" title="wikilink">梅塞德斯</a></p></td>
<td><p>1:26.572</p></td>
<td><p><strong>1:25.187</strong></p></td>
<td><p><strong>1:26.714</strong></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/羅伯特·古碧沙.md" title="wikilink">羅伯特·古碧沙</a></p></td>
<td><p><a href="../Page/寶馬沙巴車隊.md" title="wikilink">寶馬沙巴車隊</a></p></td>
<td><p>1:26.103</p></td>
<td><p>1:25.315</p></td>
<td><p>1:26.869</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/海基·科瓦萊寧.md" title="wikilink">海基·科瓦萊寧</a></p></td>
<td><p><a href="../Page/麥拿倫車隊.md" title="wikilink">麥拿倫車隊</a>-<a href="../Page/梅塞德斯.md" title="wikilink">梅塞德斯</a></p></td>
<td><p><strong>1:25.664</strong></p></td>
<td><p>1:25.452</p></td>
<td><p>1:27.079</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/費利·馬沙.md" title="wikilink">費利·馬沙</a></p></td>
<td><p><a href="../Page/法拉利車隊.md" title="wikilink">法拉利車隊</a></p></td>
<td><p>1:25.994</p></td>
<td><p>1:25.691</p></td>
<td><p>1:27.178</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/尼克·夏菲特.md" title="wikilink">尼克·夏菲特</a></p></td>
<td><p><a href="../Page/寶馬沙巴車隊.md" title="wikilink">寶馬沙巴車隊</a></p></td>
<td><p>1:25.960</p></td>
<td><p>1:25.518</p></td>
<td><p>1:27.236</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/亞諾·杜連.md" title="wikilink">亞諾·杜連</a></p></td>
<td><p><a href="../Page/豐田車隊.md" title="wikilink">豐田車隊</a></p></td>
<td><p>1:26.427</p></td>
<td><p>1:26.101</p></td>
<td><p>1:28.527</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="../Page/尼高·羅斯堡.md" title="wikilink">尼高·羅斯堡</a></p></td>
<td><p><a href="../Page/威廉士車隊.md" title="wikilink">威廉士車隊</a>-<a href="../Page/豐田車隊.md" title="wikilink">豐田</a></p></td>
<td><p>1:26.295</p></td>
<td><p>1:26.059</p></td>
<td><p>1:28.687</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><a href="../Page/大衛·古達.md" title="wikilink">大衛·古達</a></p></td>
<td><p><a href="../Page/紅牛車隊.md" title="wikilink">紅牛車隊</a>-<a href="../Page/雷諾車隊.md" title="wikilink">雷諾</a></p></td>
<td><p>1:26.381</p></td>
<td><p>1:26.063</p></td>
<td><p>1:29.041</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p><a href="../Page/賽巴斯蒂安·華特爾.md" title="wikilink">賽巴斯蒂安·華特爾</a></p></td>
<td><p><a href="../Page/紅牛二隊.md" title="wikilink">紅牛二隊</a>-<a href="../Page/法拉利車隊.md" title="wikilink">法拉利</a></p></td>
<td><p>1:26.702</p></td>
<td><p>1:25.842</p></td>
<td><p>沒有時間<a href="../Page/#fn_3.md" title="wikilink"><sup>3</sup></a></p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p><a href="../Page/魯賓斯·巴利加奴.md" title="wikilink">魯賓斯·巴利加奴</a></p></td>
<td><p><a href="../Page/本田車隊.md" title="wikilink">本田車隊</a></p></td>
<td><p>1:26.369</p></td>
<td><p>1:26.173</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p><a href="../Page/費蘭度·阿朗素.md" title="wikilink">費蘭度·阿朗素</a></p></td>
<td><p><a href="../Page/雷諾車隊.md" title="wikilink">雷諾車隊</a></p></td>
<td><p>1:26.907</p></td>
<td><p>1:26.188</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p><a href="../Page/詹森·畢頓.md" title="wikilink">詹森·畢頓</a></p></td>
<td><p><a href="../Page/本田車隊.md" title="wikilink">本田車隊</a></p></td>
<td><p>1:26.712</p></td>
<td><p>1:26.259</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p><a href="../Page/中嶋一貴.md" title="wikilink">中嶋一貴</a></p></td>
<td><p><a href="../Page/威廉士車隊.md" title="wikilink">威廉士車隊</a>-<a href="../Page/豐田車隊.md" title="wikilink">豐田</a></p></td>
<td><p>1:26.891</p></td>
<td><p>1:26.413</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p><a href="../Page/馬克·韋伯.md" title="wikilink">馬克·韋伯</a></p></td>
<td><p><a href="../Page/紅牛車隊.md" title="wikilink">紅牛車隊</a>-<a href="../Page/雷諾車隊.md" title="wikilink">雷諾</a></p></td>
<td><p>1:26.914</p></td>
<td><p>沒有時間<a href="../Page/#fn_2.md" title="wikilink"><sup>2</sup></a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p><a href="../Page/奇米·拉高倫.md" title="wikilink">奇米·拉高倫</a></p></td>
<td><p><a href="../Page/法拉利車隊.md" title="wikilink">法拉利車隊</a></p></td>
<td><p>1:26.140</p></td>
<td><p>沒有時間<a href="../Page/#fn_1.md" title="wikilink"><sup>1</sup></a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p><a href="../Page/尚卡羅·費希切拉.md" title="wikilink">尚卡羅·費希切拉</a></p></td>
<td><p><a href="../Page/印度力量車隊.md" title="wikilink">印度力量車隊</a>-<a href="../Page/法拉利車隊.md" title="wikilink">法拉利</a></p></td>
<td><p>1:27.207</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p><a href="../Page/塞巴斯蒂安·布尔代.md" title="wikilink">塞巴斯蒂安·布尔代</a></p></td>
<td><p><a href="../Page/紅牛二隊.md" title="wikilink">紅牛二隊</a>-<a href="../Page/法拉利車隊.md" title="wikilink">法拉利</a></p></td>
<td><p>1:27.446</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p><a href="../Page/阿達·舒杜.md" title="wikilink">阿達·舒杜</a></p></td>
<td><p><a href="../Page/印度力量車隊.md" title="wikilink">印度力量車隊</a>-<a href="../Page/法拉利車隊.md" title="wikilink">法拉利</a></p></td>
<td><p>1:27.859</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p><a href="../Page/添姆·葛諾.md" title="wikilink">添姆·葛諾</a></p></td>
<td><p><a href="../Page/豐田車隊.md" title="wikilink">豐田車隊</a></p></td>
<td><p>1:26.919</p></td>
<td><p>1:26.164</p></td>
<td><p>1:29.593<a href="../Page/#fn_4.md" title="wikilink"><sup>4</sup></a></p></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p><a href="../Page/佐藤琢磨.md" title="wikilink">佐藤琢磨</a></p></td>
<td><p><a href="../Page/超級亞久里車隊.md" title="wikilink">超級亞久里車隊</a>-<a href="../Page/本田車隊.md" title="wikilink">本田</a></p></td>
<td><p>1:28.208</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p><a href="../Page/小尼爾森·皮奎.md" title="wikilink">小尼爾森·皮奎</a></p></td>
<td><p><a href="../Page/雷諾車隊.md" title="wikilink">雷諾車隊</a></p></td>
<td><p>1:28.330</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p><a href="../Page/安東尼·戴維森.md" title="wikilink">安東尼·戴維森</a></p></td>
<td><p><a href="../Page/超級亞久里車隊.md" title="wikilink">超級亞久里車隊</a>-<a href="../Page/本田車隊.md" title="wikilink">本田</a></p></td>
<td><p>1:29.059</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

  - 奇米·拉高倫的座駕在第一部份因燃燒問題提出退出，其餘時間留在維修站中。\[1\]

  - 馬克·韋伯的座駕因右前輪脫離車子而無法完成餘下排位。\[2\]

  - 賽巴斯蒂安·華特爾因油泵問題而沒有在第三次排位賽出車。\[3\]

  - 添姆·葛諾因為更換波箱而退後五名起步，而且阻礙了韋伯排位因為退後到第十九名起步。\[4\]

### 比賽結果

<table>
<thead>
<tr class="header">
<th><p>位置</p></th>
<th><p>車跑</p></th>
<th><p>車手</p></th>
<th><p>車隊</p></th>
<th><p>圈數</p></th>
<th><p>時間／退賽</p></th>
<th><p>排位</p></th>
<th><p>分數</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>22</p></td>
<td><p><strong><a href="../Page/劉易斯·咸美頓.md" title="wikilink">劉易斯·咸美頓</a></strong></p></td>
<td><p><strong><a href="../Page/麥拿倫車隊.md" title="wikilink">麥拿倫車隊</a>-<a href="../Page/梅塞德斯.md" title="wikilink">梅塞德斯</a></strong></p></td>
<td><p>58</p></td>
<td><p>1:34:50.616</p></td>
<td><p>1</p></td>
<td><p><strong>10</strong></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>3</p></td>
<td><p><strong><a href="../Page/尼克·夏菲特.md" title="wikilink">尼克·夏菲特</a></strong></p></td>
<td><p><strong><a href="../Page/寶馬沙巴車隊.md" title="wikilink">寶馬沙巴車隊</a></strong></p></td>
<td><p>58</p></td>
<td><p>+5.478</p></td>
<td><p>5</p></td>
<td><p><strong>8</strong></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>7</p></td>
<td><p><strong><a href="../Page/尼高·羅斯堡.md" title="wikilink">尼高·羅斯堡</a></strong></p></td>
<td><p><strong><a href="../Page/威廉士車隊.md" title="wikilink">威廉士車隊</a>-<a href="../Page/豐田車隊.md" title="wikilink">豐田</a></strong></p></td>
<td><p>58</p></td>
<td><p>+8.163</p></td>
<td><p>7</p></td>
<td><p><strong>6</strong></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>5</p></td>
<td><p><strong><a href="../Page/費蘭度·阿朗素.md" title="wikilink">費蘭度·阿朗素</a></strong></p></td>
<td><p><strong><a href="../Page/雷諾車隊.md" title="wikilink">雷諾車隊</a></strong></p></td>
<td><p>58</p></td>
<td><p>+17.181</p></td>
<td><p>11</p></td>
<td><p><strong>5</strong></p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>23</p></td>
<td><p><strong><a href="../Page/海基·科瓦萊寧.md" title="wikilink">海基·科瓦萊寧</a></strong></p></td>
<td><p><strong><a href="../Page/麥拿倫車隊.md" title="wikilink">麥拿倫車隊</a>-<a href="../Page/梅塞德斯.md" title="wikilink">梅塞德斯</a></strong></p></td>
<td><p>58</p></td>
<td><p>+18.014</p></td>
<td><p>3</p></td>
<td><p><strong>4</strong></p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>8</p></td>
<td><p><strong><a href="../Page/中嶋一貴.md" title="wikilink">中嶋一貴</a></strong></p></td>
<td><p><strong><a href="../Page/威廉士車隊.md" title="wikilink">威廉士車隊</a>-<a href="../Page/豐田車隊.md" title="wikilink">豐田</a></strong></p></td>
<td><p>57</p></td>
<td><p>+1圈</p></td>
<td><p>13</p></td>
<td><p><strong>3</strong></p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>14</p></td>
<td><p><strong><a href="../Page/塞巴斯蒂安·布尔代.md" title="wikilink">塞巴斯蒂安·布尔代</a></strong></p></td>
<td><p><strong><a href="../Page/紅牛二隊.md" title="wikilink">紅牛二隊</a>-<a href="../Page/法拉利車隊.md" title="wikilink">法拉利</a></strong></p></td>
<td><p>55</p></td>
<td><p>引擎</p></td>
<td><p>17</p></td>
<td><p><strong>2</strong></p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>1</p></td>
<td><p><strong><a href="../Page/奇米·拉高倫.md" title="wikilink">奇米·拉高倫</a></strong></p></td>
<td><p><strong><a href="../Page/法拉利車隊.md" title="wikilink">法拉利車隊</a></strong></p></td>
<td><p>54</p></td>
<td><p>引擎</p></td>
<td><p>15</p></td>
<td><p><strong>1</strong></p></td>
</tr>
<tr class="odd">
<td><p>Ret</p></td>
<td><p>4</p></td>
<td><p><a href="../Page/羅伯特·古碧沙.md" title="wikilink">羅伯特·古碧沙</a></p></td>
<td><p><a href="../Page/寶馬沙巴車隊.md" title="wikilink">寶馬沙巴車隊</a></p></td>
<td><p>47</p></td>
<td><p>相撞</p></td>
<td><p>2</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Ret</p></td>
<td><p>12</p></td>
<td><p><a href="../Page/添姆·葛諾.md" title="wikilink">添姆·葛諾</a></p></td>
<td><p><a href="../Page/豐田車隊.md" title="wikilink">豐田車隊</a></p></td>
<td><p>43</p></td>
<td><p>意外</p></td>
<td><p>18</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Ret</p></td>
<td><p>18</p></td>
<td><p><a href="../Page/佐藤琢磨.md" title="wikilink">佐藤琢磨</a></p></td>
<td><p><a href="../Page/超級亞久里車隊.md" title="wikilink">超級亞久里車隊</a>-<a href="../Page/本田車隊.md" title="wikilink">本田</a></p></td>
<td><p>32</p></td>
<td><p>變速器</p></td>
<td><p>19</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Ret</p></td>
<td><p>6</p></td>
<td><p><a href="../Page/小尼爾遜·畢奇.md" title="wikilink">小尼爾遜·畢奇</a></p></td>
<td><p><a href="../Page/雷諾車隊.md" title="wikilink">雷諾車隊</a></p></td>
<td><p>30</p></td>
<td><p>離合器</p></td>
<td><p>20</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Ret</p></td>
<td><p>2</p></td>
<td><p><a href="../Page/費利·馬沙.md" title="wikilink">費利·馬沙</a></p></td>
<td><p><a href="../Page/法拉利車隊.md" title="wikilink">法拉利車隊</a></p></td>
<td><p>29</p></td>
<td><p>傷害相撞</p></td>
<td><p>4</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Ret</p></td>
<td><p>9</p></td>
<td><p><a href="../Page/大衛·古達.md" title="wikilink">大衛·古達</a></p></td>
<td><p><a href="../Page/紅牛車隊.md" title="wikilink">紅牛車隊</a>-<a href="../Page/雷諾車隊.md" title="wikilink">雷諾</a></p></td>
<td><p>25</p></td>
<td><p>相撞</p></td>
<td><p>8</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Ret</p></td>
<td><p>11</p></td>
<td><p><a href="../Page/亞諾·杜連.md" title="wikilink">亞諾·杜連</a></p></td>
<td><p><a href="../Page/豐田車隊.md" title="wikilink">豐田車隊</a></p></td>
<td><p>19</p></td>
<td><p>零件</p></td>
<td><p>6</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Ret</p></td>
<td><p>20</p></td>
<td><p><a href="../Page/阿達·舒杜.md" title="wikilink">阿達·舒杜</a></p></td>
<td><p><a href="../Page/印度力量車隊.md" title="wikilink">印度力量車隊</a>-<a href="../Page/法拉利車隊.md" title="wikilink">法拉利</a></p></td>
<td><p>8</p></td>
<td><p>水箱</p></td>
<td><p>22</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Ret</p></td>
<td><p>10</p></td>
<td><p><a href="../Page/馬克·韋伯.md" title="wikilink">馬克·韋伯</a></p></td>
<td><p><a href="../Page/紅牛車隊.md" title="wikilink">紅牛車隊</a>-<a href="../Page/雷諾車隊.md" title="wikilink">雷諾</a></p></td>
<td><p>0</p></td>
<td><p>相撞</p></td>
<td><p>14</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Ret</p></td>
<td><p>16</p></td>
<td><p><a href="../Page/詹森·畢頓.md" title="wikilink">詹森·畢頓</a></p></td>
<td><p><a href="../Page/本田車隊.md" title="wikilink">本田車隊</a></p></td>
<td><p>0</p></td>
<td><p>相撞</p></td>
<td><p>12</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Ret</p></td>
<td><p>19</p></td>
<td><p><a href="../Page/安東尼·戴維森.md" title="wikilink">安東尼·戴維森</a></p></td>
<td><p><a href="../Page/超級亞久里車隊.md" title="wikilink">超級亞久里車隊</a>-<a href="../Page/本田車隊.md" title="wikilink">本田</a></p></td>
<td><p>0</p></td>
<td><p>相撞</p></td>
<td><p>21</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Ret</p></td>
<td><p>15</p></td>
<td><p><a href="../Page/賽巴斯蒂安·華特爾.md" title="wikilink">賽巴斯蒂安·華特爾</a></p></td>
<td><p><a href="../Page/紅牛二隊.md" title="wikilink">紅牛二隊</a>-<a href="../Page/法拉利車隊.md" title="wikilink">法拉利</a></p></td>
<td><p>0</p></td>
<td><p>相撞</p></td>
<td><p>9</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Ret</p></td>
<td><p>21</p></td>
<td><p><a href="../Page/尚卡羅·費希切拉.md" title="wikilink">尚卡羅·費希切拉</a></p></td>
<td><p><a href="../Page/印度力量車隊.md" title="wikilink">印度力量車隊</a>-<a href="../Page/法拉利車隊.md" title="wikilink">法拉利</a></p></td>
<td><p>0</p></td>
<td><p>相撞</p></td>
<td><p>16</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>DSQ</p></td>
<td><p>17</p></td>
<td><p><a href="../Page/魯賓斯·巴利加奴.md" title="wikilink">魯賓斯·巴利加奴</a></p></td>
<td><p><a href="../Page/本田車隊.md" title="wikilink">本田車隊</a></p></td>
<td><p>58</p></td>
<td><p>取消資格</p></td>
<td><p>10</p></td>
<td></td>
</tr>
</tbody>
</table>

  - 魯賓斯·巴利加奴在紅燈時離開維修站被取消資格。

## 参考資料

<div class="references-small">

<references />

</div>

[Category:2008年一級方程式賽果](../Category/2008年一級方程式賽果.md "wikilink")
[Category:澳大利亚大奖赛](../Category/澳大利亚大奖赛.md "wikilink")
[Category:2008年澳大利亚](../Category/2008年澳大利亚.md "wikilink")
[Category:2000年代澳大利亚体育](../Category/2000年代澳大利亚体育.md "wikilink")
[Category:墨尔本体育史](../Category/墨尔本体育史.md "wikilink")
[Category:2008年3月](../Category/2008年3月.md "wikilink")

1.
2.
3.
4.