**[2008年台灣返聯公民投票](../Page/2008年台灣返聯公民投票.md "wikilink")**，正式稱為**「推動我國以務實、有彈性的策略重返聯合國及加入其它國際組織」全國性公民投票案**（簡稱**返聯公投案**，選票上的標示為**務實返聯公投**），即**[中華民國](../Page/中華民國.md "wikilink")[全國性公民投票案第](../Page/中華民國全國性公民投票.md "wikilink")6案**，為[中華民國於](../Page/中華民國.md "wikilink")[臺灣](../Page/臺灣.md "wikilink")、[澎湖](../Page/澎湖.md "wikilink")、[金門](../Page/金門.md "wikilink")、[馬祖實際管轄區域的諮詢性公民投票案](../Page/馬祖.md "wikilink")。該公投案提案人為[2008年中華民國總統大選中](../Page/2008年中華民國總統大選.md "wikilink")[中國國民黨](../Page/中國國民黨.md "wikilink")[副總統候選人](../Page/副總統.md "wikilink")[蕭萬長於](../Page/蕭萬長.md "wikilink")2007年8月1日所提，並隨即獲得該黨支持。其中，「我國」指的是實際管轄領土位於台灣（[台澎金馬](../Page/台澎金馬.md "wikilink")）的中華民國。而有彈性策略，是指考量使用的返聯名稱可以是「**中華民國**」、「**台灣**」、「**中華台北**」（奧會模式）等，以返回聯合國為首要目標。馬英九否認用「中國臺北」名稱返聯。

2008年3月22日，該公投案因未達50%投票率而宣告失敗。

## 投票結果

## 參見

  - [2008年臺灣入聯公民投票](../Page/2008年臺灣入聯公民投票.md "wikilink")
  - [中華民國全國性公民投票](../Page/中華民國全國性公民投票.md "wikilink")
  - [公民投票法](../Page/公民投票法.md "wikilink")
  - [公民投票](../Page/公民投票.md "wikilink")
  - [自決](../Page/自決.md "wikilink")
  - [臺灣外交](../Page/臺灣外交.md "wikilink")
  - [中國與聯合國關係](../Page/中國與聯合國關係.md "wikilink")
  - [聯合國大會2758號決議](../Page/聯合國大會2758號決議.md "wikilink")
  - [臺海現狀](../Page/臺海現狀.md "wikilink")

## 參考資料

1.  <small>李佳霏：《國民黨：返聯公投未含臺獨動機》。《中央社》報導，2007年8月20日。</small>

[2008](../Category/中華民國公民投票.md "wikilink")
[Category:中国与联合国关系史](../Category/中国与联合国关系史.md "wikilink")
[Category:2000年代台湾外交事件](../Category/2000年代台湾外交事件.md "wikilink")
[Category:臺灣海峽兩岸關係政治史](../Category/臺灣海峽兩岸關係政治史.md "wikilink")
[Category:2008年台湾政治事件](../Category/2008年台湾政治事件.md "wikilink")