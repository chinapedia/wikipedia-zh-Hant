**2008年鳳凰盃香港國際女子棒球邀請賽**（**Oregon Scientific Phoenix Cup Hong Kong
International Women's Baseball
Tournament**）於2008年2月14日至2月17日於[香港](../Page/香港.md "wikilink")[藍田](../Page/藍田_\(香港\).md "wikilink")[晒草灣棒球場](../Page/曬草灣遊樂場.md "wikilink")\[1\]
舉行。本屆賽事為[香港棒球總會與](../Page/香港棒球總會.md "wikilink")[國際棒球總會合辦](../Page/國際棒球總會.md "wikilink")，是為了慶祝[香港棒球總會十五週年而舉辦](../Page/香港棒球總會.md "wikilink")，是[香港首度舉辦國際女子棒球比賽](../Page/香港.md "wikilink")。本屆賽事由[歐西亞](../Page/歐西亞.md "wikilink")（Oregon
Scientific）贊助，故於賽事名稱上冠上「Oregon Scientific」之名。

本屆賽事邀請六個隊伍參賽，分別是[日本的](../Page/日本.md "wikilink")[遠東燈籠褲隊](../Page/遠東燈籠褲隊.md "wikilink")（Far
East
Bloomers，ファー・イースト・ブルーマーズ）、[北美的](../Page/北美.md "wikilink")[女子棒球聯盟明星隊](../Page/女子棒球聯盟明星隊.md "wikilink")（Women's
Baseball League North Stars，WBL North
Stars）、[澳洲的](../Page/澳洲.md "wikilink")[澳洲之心隊](../Page/澳洲之心隊.md "wikilink")（Aussie
Hearts）、[香港的](../Page/香港.md "wikilink")[Allies隊](../Page/Allies隊.md "wikilink")、[台灣的](../Page/台灣.md "wikilink")[台北先鋒女子棒球隊與](../Page/台北先鋒女子棒球隊.md "wikilink")[韓國的](../Page/韓國.md "wikilink")[日出隊](../Page/日出隊.md "wikilink")（Sunrise）。

本屆賽事初賽為分組循環賽，分為兩組，一組三隊打[單循環賽](../Page/循環賽.md "wikilink")，各組取戰績前兩名晉級複賽。最後由[日本的](../Page/日本.md "wikilink")[遠東燈籠褲隊](../Page/遠東燈籠褲隊.md "wikilink")（Far
East Bloomers）獲得冠軍\[2\]。

## 預賽

### A組

| 預賽排行  | 隊伍                                                                     | 勝 | 負 |
| ----- | ---------------------------------------------------------------------- | - | - |
| **1** | [香港](../Page/香港.md "wikilink")[Allies隊](../Page/Allies隊.md "wikilink") | 2 | 0 |
| **2** | [澳洲](../Page/澳洲.md "wikilink")[澳洲之心隊](../Page/澳洲之心隊.md "wikilink")     | 1 | 1 |
| **3** | [韓國](../Page/韓國.md "wikilink")[日出隊](../Page/日出隊.md "wikilink")         | 0 | 2 |
|       |                                                                        |   |   |

### B組

| 預賽排行  | 隊伍                                                                         | 勝 | 負 |
| ----- | -------------------------------------------------------------------------- | - | - |
| **1** | [日本](../Page/日本.md "wikilink")[遠東燈籠褲隊](../Page/遠東燈籠褲隊.md "wikilink")       | 2 | 0 |
| **2** | [北美](../Page/北美.md "wikilink")[女子棒球聯盟明星隊](../Page/女子棒球聯盟明星隊.md "wikilink") | 1 | 1 |
| **3** | [台灣](../Page/台灣.md "wikilink")[台北先鋒女子棒球隊](../Page/台北先鋒女子棒球隊.md "wikilink") | 0 | 2 |
|       |                                                                            |   |   |

## 複賽

### 第五到第六名賽

| 主隊                                                                         |       | 客隊                                                             | 時間    |
| -------------------------------------------------------------------------- | ----- | -------------------------------------------------------------- | ----- |
| 2月16日                                                                      |       |                                                                |       |
| [台灣](../Page/台灣.md "wikilink")[台北先鋒女子棒球隊](../Page/台北先鋒女子棒球隊.md "wikilink") | 22:20 | [韓國](../Page/韓國.md "wikilink")[日出隊](../Page/日出隊.md "wikilink") | 08:30 |
|                                                                            |       |                                                                |       |

## 逐場比賽結果

| 主隊                                                                         |       | 客隊                                                                         | 時間    |
| -------------------------------------------------------------------------- | ----- | -------------------------------------------------------------------------- | ----- |
| 2月14日                                                                      |       |                                                                            |       |
| [韓國](../Page/韓國.md "wikilink")[日出隊](../Page/日出隊.md "wikilink")             | 6:16  | [澳洲](../Page/澳洲.md "wikilink")[澳洲之心隊](../Page/澳洲之心隊.md "wikilink")         | 08:30 |
| [台灣](../Page/台灣.md "wikilink")[台北先鋒女子棒球隊](../Page/台北先鋒女子棒球隊.md "wikilink") | 0:10  | [日本](../Page/日本.md "wikilink")[遠東燈籠褲隊](../Page/遠東燈籠褲隊.md "wikilink")       | 12:30 |
| [香港](../Page/香港.md "wikilink")[Allies隊](../Page/Allies隊.md "wikilink")     | 8:3   | [韓國](../Page/韓國.md "wikilink")[日出隊](../Page/日出隊.md "wikilink")             | 15:30 |
| 2月15日                                                                      |       |                                                                            |       |
| [日本](../Page/日本.md "wikilink")[遠東燈籠褲隊](../Page/遠東燈籠褲隊.md "wikilink")       | 13:0  | [北美](../Page/北美.md "wikilink")[女子棒球聯盟明星隊](../Page/女子棒球聯盟明星隊.md "wikilink") | 08:30 |
| [北美](../Page/北美.md "wikilink")[女子棒球聯盟明星隊](../Page/女子棒球聯盟明星隊.md "wikilink") | 12:7  | [台灣](../Page/台灣.md "wikilink")[台北先鋒女子棒球隊](../Page/台北先鋒女子棒球隊.md "wikilink") | 12:00 |
| [澳洲](../Page/澳洲.md "wikilink")[澳洲之心隊](../Page/澳洲之心隊.md "wikilink")         | 4:10  | [香港](../Page/香港.md "wikilink")[Allies隊](../Page/Allies隊.md "wikilink")     | 15:30 |
| 2月16日                                                                      |       |                                                                            |       |
| [台灣](../Page/台灣.md "wikilink")[台北先鋒女子棒球隊](../Page/台北先鋒女子棒球隊.md "wikilink") | 22:20 | [韓國](../Page/韓國.md "wikilink")[日出隊](../Page/日出隊.md "wikilink")             | 08:30 |
| [香港](../Page/香港.md "wikilink")[Allies隊](../Page/Allies隊.md "wikilink")     | 0:7   | [北美](../Page/北美.md "wikilink")[女子棒球聯盟明星隊](../Page/女子棒球聯盟明星隊.md "wikilink") | 12:00 |
| [日本](../Page/日本.md "wikilink")[遠東燈籠褲隊](../Page/遠東燈籠褲隊.md "wikilink")       | 6:2   | [澳洲](../Page/澳洲.md "wikilink")[澳洲之心隊](../Page/澳洲之心隊.md "wikilink")         | 15:30 |
| 2月17日                                                                      |       |                                                                            |       |
| [澳洲](../Page/澳洲.md "wikilink")[澳洲之心隊](../Page/澳洲之心隊.md "wikilink")         | 18:8  | [香港](../Page/香港.md "wikilink")[Allies隊](../Page/Allies隊.md "wikilink")     | 08:30 |
| [日本](../Page/日本.md "wikilink")[遠東燈籠褲隊](../Page/遠東燈籠褲隊.md "wikilink")       | 14:1  | [北美](../Page/北美.md "wikilink")[女子棒球聯盟明星隊](../Page/女子棒球聯盟明星隊.md "wikilink") | 12:00 |
|                                                                            |       |                                                                            |       |

## 獎項

### 團隊名次

1.  [日本](../Page/日本.md "wikilink")[遠東燈籠褲隊](../Page/遠東燈籠褲隊.md "wikilink")
2.  [北美](../Page/北美.md "wikilink")[女子棒球聯盟明星隊](../Page/女子棒球聯盟明星隊.md "wikilink")
3.  [澳洲](../Page/澳洲.md "wikilink")[澳洲之心隊](../Page/澳洲之心隊.md "wikilink")
4.  [香港](../Page/香港.md "wikilink")[Allies隊](../Page/Allies隊.md "wikilink")
5.  [台灣](../Page/台灣.md "wikilink")[台北先鋒女子棒球隊](../Page/台北先鋒女子棒球隊.md "wikilink")
6.  [韓國](../Page/韓國.md "wikilink")[日出隊](../Page/日出隊.md "wikilink")

### 個人獎項

  - 最多[全壘打獎](../Page/全壘打.md "wikilink")：從缺
  - 最多[安打獎](../Page/安打.md "wikilink")：[Julia
    Watson](../Page/Julia_Watson.md "wikilink")（[澳洲](../Page/澳洲.md "wikilink")[澳洲之心隊](../Page/澳洲之心隊.md "wikilink")，8支）
  - 優秀[投手獎](../Page/投手.md "wikilink")：[小久保志乃](../Page/小久保志乃.md "wikilink")（[日本](../Page/日本.md "wikilink")[遠東燈籠褲隊](../Page/遠東燈籠褲隊.md "wikilink")）
  - 優秀[教練獎](../Page/教練.md "wikilink")：[有村友志](../Page/有村友志.md "wikilink")（[日本](../Page/日本.md "wikilink")[遠東燈籠褲隊](../Page/遠東燈籠褲隊.md "wikilink")）
  - [最有價值球員獎](../Page/最有價值球員.md "wikilink")：[小久保志乃](../Page/小久保志乃.md "wikilink")（[日本](../Page/日本.md "wikilink")[遠東燈籠褲隊](../Page/遠東燈籠褲隊.md "wikilink")）、[Anna
    Kimbrell](../Page/Anna_Kimbrell.md "wikilink")（[北美](../Page/北美.md "wikilink")[女子棒球聯盟明星隊](../Page/女子棒球聯盟明星隊.md "wikilink")）、[Julia
    Watson](../Page/Julia_Watson.md "wikilink")（[澳洲](../Page/澳洲.md "wikilink")[澳洲之心隊](../Page/澳洲之心隊.md "wikilink")）、[卓莞爾](../Page/卓莞爾.md "wikilink")（[香港](../Page/香港.md "wikilink")[Allies隊](../Page/Allies隊.md "wikilink")）、[廖立欣](../Page/廖立欣.md "wikilink")（[台灣](../Page/台灣.md "wikilink")[台北先鋒女子棒球隊](../Page/台北先鋒女子棒球隊.md "wikilink")）與[李姃恩](../Page/李姃恩.md "wikilink")（이정은，[韓國](../Page/韓國.md "wikilink")[日出隊](../Page/日出隊.md "wikilink")）

## 注釋與參考文獻

## 外部連結

  - [2008年鳳凰盃香港國際女子棒球邀請賽官方網站](http://ospc2008.tripleplaycom.net/)

[Category:2008年棒球](../Category/2008年棒球.md "wikilink")
[Category:2008年香港體育](../Category/2008年香港體育.md "wikilink")
[Category:棒球競賽](../Category/棒球競賽.md "wikilink")
[Category:香港主辦的國際體育賽事](../Category/香港主辦的國際體育賽事.md "wikilink")

1.  該場地為香港第一個標準棒球場。
2.  <http://www.ibaf.tv/index.php?option=com_content&task=view&id=131&Itemid=72>