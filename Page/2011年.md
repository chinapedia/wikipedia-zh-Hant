  - [聯合國宣布該年為](../Page/聯合國.md "wikilink")[國際化學年和](../Page/國際化學年.md "wikilink")[國際森林年](../Page/國際森林年.md "wikilink")
  - [世界人口突破](../Page/世界人口.md "wikilink")70億

## 大事记

## 逝世

## [诺贝尔獎](../Page/诺贝尔獎.md "wikilink")

  - [物理学](../Page/诺贝尔物理学奖.md "wikilink")：[索尔·珀尔马特](../Page/索尔·珀尔马特.md "wikilink")、[亚当·里斯及](../Page/亚当·里斯.md "wikilink")[布莱恩·施密特](../Page/布莱恩·施密特.md "wikilink")
  - [化学](../Page/诺贝尔化学奖.md "wikilink")：[丹·舍特曼](../Page/丹·舍特曼.md "wikilink")
  - [医学](../Page/诺贝尔医学奖.md "wikilink")：[布魯斯·比尤特勒](../Page/布魯斯·比尤特勒.md "wikilink")、[朱尔斯·霍夫曼及](../Page/朱尔斯·霍夫曼.md "wikilink")[拉尔夫·斯坦曼](../Page/拉尔夫·斯坦曼.md "wikilink")\[1\]
  - [文学](../Page/诺贝尔文学奖.md "wikilink")：[托马斯·特兰斯特罗默](../Page/托马斯·特兰斯特罗默.md "wikilink")
  - [和平](../Page/诺贝尔和平奖.md "wikilink")：[埃伦·约翰逊-瑟利夫](../Page/埃伦·约翰逊-瑟利夫.md "wikilink")、[莱伊曼·古博薇及](../Page/莱伊曼·古博薇.md "wikilink")[塔瓦库·卡曼](../Page/塔瓦库·卡曼.md "wikilink")
  - [经济学](../Page/诺贝尔经济学奖.md "wikilink")：[克里斯托弗·西姆斯及](../Page/克里斯托弗·西姆斯.md "wikilink")[托瑪斯·薩金特](../Page/托瑪斯·薩金特.md "wikilink")

## [奧斯卡金像奖](../Page/奧斯卡金像奖.md "wikilink")

  - 最佳電影：[皇上無話兒](../Page/皇上無話兒.md "wikilink")
  - 最佳男主角：[柯林·佛斯](../Page/柯林·佛斯.md "wikilink")－《[皇上無話兒](../Page/皇上無話兒.md "wikilink")》
  - 最佳女主角：[娜塔莉·波曼](../Page/娜塔莉·波曼.md "wikilink")－《[黑天鵝](../Page/黑天鵝.md "wikilink")》
  - 最佳原創劇本：大衛·塞德勒－《[皇上無話兒](../Page/皇上無話兒.md "wikilink")》

## 參考資料

## 外部連結

  - [2011年世界拍賣總收入記錄前15名的世界知名畫家名單](https://web.archive.org/web/20120525204824/http://tw.knowledge.yahoo.com/question/article?qid=1712040607099)
  - [2011年世界拍賣總收入記錄前30名的中國畫家名單](https://web.archive.org/web/20120525205204/http://tw.knowledge.yahoo.com/question/article?qid=1712040606153)

[\*](../Category/2011年.md "wikilink")
[1](../Category/2010年代.md "wikilink")
[1](../Category/21世纪各年.md "wikilink")

1.  [2011年诺贝尔生理学或医学奖得主揭晓](http://world.people.com.cn/GB/15807588.html)