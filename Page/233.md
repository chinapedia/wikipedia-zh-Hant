**233**在十进制中，是[232与](../Page/232.md "wikilink")[234之间的](../Page/234.md "wikilink")[自然数](../Page/自然数.md "wikilink")。

## 数学性质

  - [斐波那契数列中的第](../Page/斐波那契数列.md "wikilink")14項
  - [馬爾可夫方程的一個解](../Page/馬爾可夫方程.md "wikilink")
  - [23](../Page/23.md "wikilink")、233、2333、23333都是[質數](../Page/質數.md "wikilink")，但是233333不是質數
  - 11個連續[質數和](../Page/質數.md "wikilink")（5+7+11+13+17+19+23+29+31+37+41）

## 地理

  - [台灣](../Page/台灣.md "wikilink")[新北市](../Page/新北市.md "wikilink")[烏來區的](../Page/烏來區.md "wikilink")[郵遞區號為](../Page/郵遞區號.md "wikilink")233

## 中國网络用语

  - 在中國网络上表示“捶地大笑”，起源自[猫扑](../Page/猫扑.md "wikilink")[论坛的第](../Page/网络论坛.md "wikilink")233号[表情符号](../Page/表情符号_\(图片\).md "wikilink")\[1\]。等价日本网络上的连续“w”\[2\]，多用于[弹幕视频网站和](../Page/弹幕视频网站.md "wikilink")[网络论坛中](../Page/网络论坛.md "wikilink")。

## 纪年

  - 西元[233年和](../Page/233年.md "wikilink")[前233年](../Page/前233年.md "wikilink")

## 参考资料

[Category:整數素數](../Category/整數素數.md "wikilink")
[Category:网络流行语](../Category/网络流行语.md "wikilink")

1.
2.  来自日文汉字“笑（わらう）”读音（Warau）的第一个字母