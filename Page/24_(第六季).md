《**24**》第六季，美国[霍士电视台于](../Page/福克斯网络.md "wikilink")2007年1月14日开播。

2007年1月6日，一个名为AsiaTeam的组织通过[BitTorrent和其他P](../Page/BitTorrent.md "wikilink")2P网络，泄漏了第一至第四集的全集，以及第五集的预告\[1\]，这些剧集来源于福克斯网络电视网原定于1月16日发行的第六季预演DVD\[2\]。

## 演员

***这是第六季的主要演员列表***:

  - [基夫修打兰](../Page/基夫修打兰.md "wikilink") 饰演
    [包智傑](../Page/包智傑.md "wikilink")（Jack Bauer）
  - [瑪麗·萊恩·萊傑斯庫](../Page/瑪麗·萊恩·萊傑斯庫.md "wikilink") 饰演
    [岳高蓮](../Page/岳高蓮.md "wikilink")（Chloe O'Brian）
  - [D.B. Woodside](../Page/D.B._Woodside.md "wikilink") 饰演
    [柏偉賢總統](../Page/柏偉賢總統.md "wikilink")（President Wayne
    Palmer）
  - [James Morrison](../Page/James_Morrison.md "wikilink") 饰演
    [布秉耀](../Page/布秉耀.md "wikilink")（Bill Buchanan）
  - [Peter MacNicol](../Page/Peter_MacNicol.md "wikilink") 饰演
    [龍杜文](../Page/龍杜文.md "wikilink")（Thomas Lennox）
  - [Jayne Atkinson](../Page/Jayne_Atkinson.md "wikilink") 饰演
    [郗嘉倫](../Page/郗嘉倫.md "wikilink")（Karen Hayes）
  - [Carlo Rota](../Page/Carlo_Rota.md "wikilink") 饰演
    [岳務凌](../Page/岳務凌.md "wikilink")（Morris O'Brian）
  - [Eric Balfour](../Page/Eric_Balfour.md "wikilink") 饰演
    [彭麥羅](../Page/彭麥羅.md "wikilink")（Milo Pressman）
  - [Marisol Nichols](../Page/Marisol_Nichols.md "wikilink") 饰演
    [尤立亭](../Page/尤立亭.md "wikilink")（Nadia Yasir）
  - [Regina King](../Page/Regina_King.md "wikilink") 饰演 [Sandra
    Palmer](../Page/Sandra_Palmer.md "wikilink")

客串：

  - [Roger Cross](../Page/Roger_Cross.md "wikilink") 饰演
    [文國定](../Page/文國定.md "wikilink")（Curtis Manning）
  - [Paul McCrane](../Page/Paul_McCrane.md "wikilink") 饰演
    [包君年](../Page/包君年.md "wikilink")（Graem Bauer）
  - [Adoni Maropis](../Page/Adoni_Maropis.md "wikilink") 饰演
    [樊安浦](../Page/樊安浦.md "wikilink")（Abu Fayed）
  - [Harry Lennix](../Page/Harry_Lennix.md "wikilink") 饰演 [Walid
    Al-Rezani](../Page/Walid_Al-Rezani.md "wikilink")
  - [Alexander Siddig](../Page/Alexander_Siddig.md "wikilink") 饰演 [Hamri
    Al-Assad](../Page/Hamri_Al-Assad.md "wikilink")
  - [凱爾·潘](../Page/凱爾·潘.md "wikilink")（Kal Penn） 饰演 [Ahmed
    Amar](../Page/Ahmed_Amar.md "wikilink")
  - [David Hunt](../Page/David_Hunt.md "wikilink") 饰演 [Darren
    McCarthy](../Page/Darren_McCarthy.md "wikilink")
  - [馬志](../Page/馬志.md "wikilink") 饰演 [鄭志](../Page/鄭志.md "wikilink")
  - [Shaun Majumder](../Page/Shaun_Majumder.md "wikilink") 饰演 [Hasan
    Numair](../Page/Hasan_Numair.md "wikilink")
  - [Raphael Sbarge](../Page/Raphael_Sbarge.md "wikilink") 饰演 [Ray
    Wallace](../Page/Ray_Wallace.md "wikilink")
  - [Megan Gallagher](../Page/Megan_Gallagher.md "wikilink") 饰演 [Jillian
    Wallace](../Page/Jillian_Wallace.md "wikilink")
  - [Michael Angarano](../Page/Michael_Angarano.md "wikilink") 饰演 [Scott
    Wallace](../Page/Scott_Wallace.md "wikilink")
  - [Rena Sofer](../Page/Rena_Sofer.md "wikilink") 饰演
    [包慕蓮](../Page/包慕蓮.md "wikilink")（Marilyn Bauer）
  - [Evan Ellingson](../Page/Evan_Ellingson.md "wikilink") 饰演
    [包佐思](../Page/包佐思.md "wikilink")（Josh Bauer）
  - [James Cromwell](../Page/James_Cromwell.md "wikilink") 饰演
    [包輝力](../Page/包輝力.md "wikilink")（Phillip Bauer）
  - [Chad Lowe](../Page/Chad_Lowe.md "wikilink") 饰演 [Reed
    Pollock](../Page/Reed_Pollock.md "wikilink")
  - [Powers Boothe](../Page/Powers_Boothe.md "wikilink") 饰演
    前副總統[鄧樂翱](../Page/鄧樂翱.md "wikilink")（U.S. Vice
    President Noah Daniels）
  - [Gregory Itzin](../Page/Gregory_Itzin.md "wikilink") 饰演
    前总统[勞卓思](../Page/查爾斯·洛根.md "wikilink")（Charles Logan）
  - [Jean Smart](../Page/Jean_Smart.md "wikilink") 饰演
    [勞瑪花](../Page/勞瑪花.md "wikilink")（Martha Logan）
  - [Glenn Morshower](../Page/Glenn_Morshower.md "wikilink") 饰演
    [皮亞朗](../Page/皮亞朗.md "wikilink")（Aaron Pierce）
  - [Kim Raver](../Page/Kim_Raver.md "wikilink") 饰演
    [榮安慈](../Page/榮安慈.md "wikilink")（Audrey Raines）
  - [William Devane](../Page/William_Devane.md "wikilink") 饰演
    [賀正時](../Page/賀正時.md "wikilink")（James Heller）
  - [Rade Šerbedžija](../Page/Rade_Šerbedžija.md "wikilink") 饰演 [General
    Dmitri Gredenko](../Page/General_Dmitri_Gredenko.md "wikilink")
  - [Kari Matchett](../Page/Kari_Matchett.md "wikilink") 饰演 丽莎，副总统秘書
    [Lisa Miller](../Page/Lisa_Miller.md "wikilink")

## 影集列表

| \# | 美國播放日期     | 產品號碼   | 時間                      | 編劇                                   | 導演                                             | 畫面                                      |
| -- | ---------- | ------ | ----------------------- | ------------------------------------ | ---------------------------------------------- | --------------------------------------- |
| 1  | 2007年1月14日 | 6AFF01 | 07:00 a.m. - 08:00 a.m. | Howard Gordon                        | [Jon Cassar](../Page/Jon_Cassar.md "wikilink") | |- style="background-color: \#FFFFFF" | |
| 2  | 2007年1月14日 | 6AFF02 | 08:00 a.m. - 09:00 a.m. | Manny Coto                           | Jon Cassar                                     | |- style="background-color: \#EFF8F7" | |
| 3  | 2007年1月15日 | 6AFF03 | 09:00 a.m. - 10:00 a.m. | David Fury,Evan Katz                 | Brad Turner                                    | |- style="background-color: \#FFFFFF" | |
| 4  | 2007年1月15日 | 6AFF04 | 10:00 a.m. - 11:00 a.m. | Robert Cochran                       | Brad Turner                                    | |- style="background-color: \#EFF8F7" | |
| 5  | 2007年1月22日 | 6AFF05 | 11:00 a.m. - 12:00 p.m. | Joel Surnow,Michael Loceff           | Milan Cheylov                                  | |- style="background-color: \#FFFFFF" | |
| 6  | 2007年1月29日 | 6AFF06 | 12:00 p.m. - 01:00 p.m. | Michael Loceff,Joel Surnow           | Milan Cheylov                                  | |- style="background-color: \#EFF8F7" | |
| 7  | 2007年2月5日  | 6AFF07 | 01:00 p.m. - 02:00 p.m. | Manny Coto,Howard Gordon             | Jon Cassar                                     | |- style="background-color: \#FFFFFF" | |
| 8  | 2007年2月12日 | 6AFF08 | 02:00 p.m. - 03:00 p.m. | Evan Katz & David Fury               | Jon Cassar                                     | |- style="background-color: \#EFF8F7" | |
| 9  | 2007年2月12日 | 6AFF09 | 03:00 p.m. - 04:00 p.m. | Adam E. Fierro                       | Brad Turner                                    | |- style="background-color: \#FFFFFF" | |
| 10 | 2007年2月19日 | 6AFF10 | 04:00 p.m. - 05:00 p.m. | Howard Gordon,Evan Katz              | Brad Turner                                    | |- style="background-color: \#EFF8F7" | |
| 11 | 2007年2月26日 | 6AFF11 | 05:00 p.m. - 06:00 p.m. | Manny Coto                           | Tim Iacofano                                   | |- style="background-color: \#FFFFFF" | |
| 12 | 2007年3月5日  | 6AFF12 | 06:00 p.m. - 07:00 p.m. | Evan Katz,David Fury                 | Tim Iacofano                                   | |- style="background-color: \#EFF8F7" | |
| 13 | 2007年3月12日 | 6AFF13 | 07:00 p.m. - 08:00 p.m. | Michael Loceff,Joel Surnow           | Jon Cassar                                     | |- style="background-color: \#FFFFFF" | |
| 14 | 2007年3月19日 | 6AFF14 | 08:00 p.m. - 09:00 p.m. | Evan Katz,Howard Gordon              | Jon Cassar                                     | |- style="background-color: \#EFF8F7" | |
| 15 | 2007年3月26日 | 6AFF15 | 09:00 p.m. - 10:00 p.m. | Manny Coto,Howard Gordon             | Brad Turner                                    | |- style="background-color: \#FFFFFF" | |
| 16 | 2007年4月2日  | 6AFF16 | 10:00 p.m. - 11:00 p.m. | Evan Katz,Robert Cochran             | Bryan Spicer                                   | |- style="background-color: \#EFF8F7" | |
| 17 | 2007年4月9日  | 6AFF17 | 11:00 p.m. - 12:00 a.m. | David Fury                           | Bryan Spicer                                   | |- style="background-color: \#FFFFFF" | |
| 18 | 2007年4月16日 | 6AFF18 | 12:00 a.m. - 01:00 a.m. | Matt Michnovetz,Nicole Ranadive      | Bryan Spicer                                   |                                         |
|    |            |        |                         |                                      |                                                |                                         |
| 19 | 2007年4月23日 | 6AFF19 | 01:00 a.m. - 02:00 a.m. | Joel Surnow,Michael Loceff           | Brad Turner                                    | |- style="background-color: \#FFFFFF" | |
| 20 | 2007年4月30日 | 6AFF20 | 02:00 a.m. - 03:00 a.m. | Howard Gordon,Evan Katz              | Brad Turner                                    | |- style="background-color: \#EFF8F7" | |
| 21 | 2007年5月7日  | 6AFF21 | 03:00 a.m. - 04:00 a.m. | Manny Coto                           | Bryan Spicer                                   | |- style="background-color: \#FFFFFF" | |
| 22 | 2007年5月14日 | 6AFF22 | 04:00 a.m. - 05:00 a.m. | Evan Katz,Howard Gordon              | Bryan Spicer                                   | |- style="background-color: \#EFF8F7" | |
| 23 | 2007年5月21日 | 6AFF23 | 05:00 a.m. - 06:00 a.m. | Joel Surnow,Michael Loceff           | Brad Turner                                    | |- style="background-color: \#FFFFFF" | |
| 24 | 2007年5月21日 | 6AFF24 | 06:00 a.m. - 07:00 a.m. | Robert Cochran,Manny Coto,David Fury | Brad Turner                                    | |- style="background-color: \#EFF8F7" | |

## 参考资料

[Category:24 (電視劇)](../Category/24_\(電視劇\).md "wikilink")
[Category:電視劇各集列表](../Category/電視劇各集列表.md "wikilink")
[Category:2007年美国电视季度](../Category/2007年美国电视季度.md "wikilink")
[Category:美國電視劇各集列表](../Category/美國電視劇各集列表.md "wikilink")

1.
2.