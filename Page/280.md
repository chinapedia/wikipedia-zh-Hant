**280**是介于[279和](../Page/279.md "wikilink")[281之间的](../Page/281.md "wikilink")[自然数](../Page/自然数.md "wikilink")。

## 在数学上

  - [中心八边形数](../Page/中心八边形数.md "wikilink")。
  - [快樂數](../Page/快樂數.md "wikilink")。
  - 6個連續[質數和](../Page/質數.md "wikilink")（37+41+43+47+53+59）
  - [半完全數](../Page/半完全數.md "wikilink")：280=1+8+35+40+56+140

## 在人類文化中

  - [Skyline第](../Page/日產Skyline.md "wikilink")8代馬力為280匹，動畫片《[頭文字D](../Page/頭文字D.md "wikilink")》更使其名聲大噪
  - [i-280.svg](https://zh.wikipedia.org/wiki/File:i-280.svg "fig:i-280.svg")是[美國](../Page/美國.md "wikilink")[80號州際公路的補助線](../Page/80號州際公路.md "wikilink")
  - [斯堪尼亞K280UD](../Page/斯堪尼亞K280UD.md "wikilink")，瑞典斯堪尼亞製造的超低地台雙層巴士型號。
  - [Ikarus
    280](../Page/伊凱洛斯280.md "wikilink")，[匈牙利](../Page/匈牙利.md "wikilink")[Ikarus生產的鉸接式中置引擎巴士車款](../Page/伊凱洛斯巴士.md "wikilink")，主要由東歐國家引進

## 在科學中

  - 到目前為止發現的[鐽的](../Page/鐽.md "wikilink")[同位素共有](../Page/同位素.md "wikilink")10種，其中鐽-280的半衰期最長，為7.4秒
  - [黃麴毒素加熱至](../Page/黃麴毒素.md "wikilink")280[℃以上才開始分解](../Page/℃.md "wikilink")
  - 短波[紫外線C光](../Page/紫外線.md "wikilink")，波長介於200\~280[奈米](../Page/奈米.md "wikilink")

## 在地理中

  - [查德湖海拔](../Page/查德湖.md "wikilink")280[公尺](../Page/公尺.md "wikilink")，預計此湖在不久之後將會完全消失
  - [綠島的高度為](../Page/綠島.md "wikilink")280[公尺](../Page/公尺.md "wikilink")

## 在天文中

  - [NGC 280](../Page/NGC_280.md "wikilink")
    是[仙女座的一個](../Page/仙女座.md "wikilink")[星系](../Page/星系.md "wikilink")
  - [靈神星的大小為](../Page/靈神星.md "wikilink") 280×230×190
    [km](../Page/km.md "wikilink")
  - [白塞爾年是一種](../Page/白塞爾年.md "wikilink")[回歸年](../Page/回歸年.md "wikilink")，以假想的太陽從[黃經](../Page/黃經.md "wikilink")280°再回到黃經280°所經歷的時間為準
  - [土衛七的大小為](../Page/土衛七.md "wikilink") 360×280×225 km

## 在其他領域中

  - 西元[280年和](../Page/280年.md "wikilink")[前280年](../Page/前280年.md "wikilink")
  - [GeForce GTX
    280於](../Page/GeForce_200.md "wikilink")[2008年](../Page/2008年.md "wikilink")[6月16日推出](../Page/6月16日.md "wikilink")，是一款高端產品
  - [SACD將所有訊號以每秒](../Page/SACD.md "wikilink")280萬次，直接把類比音樂訊號波形轉變為數位訊號
  - [乳牛的](../Page/乳牛.md "wikilink")[懷孕期為](../Page/懷孕期.md "wikilink")280天

[Category:整数](../Category/整数.md "wikilink")