**2K Games**是[美國遊戲發行商](../Page/美國.md "wikilink")[Take-Two
Interactive旗下的遊戲品牌](../Page/Take-Two_Interactive.md "wikilink")，2005年設立，總部位於[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[诺瓦托](../Page/诺瓦托_\(加利福尼亚州\).md "wikilink")。Take-Two为了让自有品牌发行的游戏与[Rockstar
Games所做区别以及在体育竞技游戏市场上与](../Page/Rockstar_Games.md "wikilink")[美国艺电旗下的](../Page/美国艺电.md "wikilink")[EA
Sports体育部门一决高下](../Page/EA_Sports.md "wikilink")，由Take-Two收购日本[世嘉公司旗下的工作室](../Page/世嘉.md "wikilink")[Visual
Concepts工作室以及其完全所有的子公司](../Page/Visual_Concepts.md "wikilink")[Kush
Games引入旗下](../Page/Kush_Games.md "wikilink")。此外它亦以[2K
Sports名義發行體育竞技遊戲](../Page/2K_Sports.md "wikilink")，以及[2K
Play名義發行休閒益智遊戲](../Page/2K_Play.md "wikilink")。

## 發行遊戲

  - 以2K Games为品牌发行
      - [生化奇兵系列](../Page/生化奇兵.md "wikilink")
      - [邊緣禁地系列](../Page/邊緣禁地.md "wikilink")
      - [文明城市：罗马](../Page/文明城市：罗马.md "wikilink")
      - [毁灭公爵系列](../Page/毁灭公爵.md "wikilink")
      - [四海兄弟系列](../Page/黑手党II.md "wikilink")
      - [猎魂](../Page/猎魂.md "wikilink")
      - [英雄萨姆2](../Page/英雄萨姆.md "wikilink")
      - [席德·梅尔的文明帝國系列](../Page/文明帝國系列.md "wikilink")
      - [席德·梅尔的铁路](../Page/席德·梅尔的铁路.md "wikilink")
      - [席德·梅尔的海盗](../Page/席德·梅尔的海盗.md "wikilink")
      - [特别行动系列](../Page/特别行动系列.md "wikilink")
      - [要塞系列](../Page/要塞_\(2001年游戏\).md "wikilink")
      - [丛林之狐系列](../Page/丛林之狐.md "wikilink")
      - [XCOM系列](../Page/X-COM.md "wikilink")
  - 以[2K Play为品牌发行](../Page/2K_Play.md "wikilink")
      - [温特伯顿先生的不幸旅程](../Page/温特伯顿先生的不幸旅程.md "wikilink")
      - [Axel & Pixel](../Page/Axel_&_Pixel.md "wikilink")
      - [花园小子](../Page/花园小子.md "wikilink")
      - [生日大聚会](../Page/生日大聚会.md "wikilink")
      - [Carnival Games](../Page/Carnival_Games.md "wikilink")
      - [探险家多拉系列](../Page/探险家多拉.md "wikilink")
      - [奇迹宠物](../Page/奇迹宠物.md "wikilink")
      - [Ni Hao, Kai-lan系列](../Page/Ni_Hao,_Kai-lan.md "wikilink")
      - [林林兄弟与巴纳姆及贝利马戏团：戏王之王](../Page/林林兄弟与巴纳姆及贝利马戏团：戏王之王.md "wikilink")
  - 以[2K Sports为品牌发行](../Page/2K_Sports.md "wikilink")
      - [MLB 2K系列](../Page/MLB_2K.md "wikilink")
      - [NBA 2K系列](../Page/NBA_2K.md "wikilink")
      - [NFL 2K系列](../Page/NFL_2K.md "wikilink")
      - [NHL 2K系列](../Page/NHL_2K.md "wikilink")
      - [上旋高手系列](../Page/上旋高手.md "wikilink")
      - [WWE 2K系列](../Page/WWE_2K.md "wikilink")

## 主要開發商

### 现有工作室

  - 2K Games
      - 2K澳大利亚（[2K Australia](../Page/2K_Australia.md "wikilink")）
      - [2K中国](../Page/2K中国.md "wikilink")（2K China）
      - 2K捷克（[2K Czech](../Page/2K_Czech.md "wikilink")）
      - 2K馬林（[2K Marin](../Page/2K_Marin.md "wikilink")）
      - 2K维加斯（2K Vegas）
      - [Firaxis Games](../Page/Firaxis_Games.md "wikilink")
      - [Hangar 13](../Page/Hangar_13.md "wikilink")
  - [2K Play](../Page/2K_Play.md "wikilink")
      - [Cat Daddy Games](../Page/Cat_Daddy_Games.md "wikilink")
  - [2K Sports](../Page/2K_Sports.md "wikilink")
      - [Visual Concepts](../Page/Visual_Concepts.md "wikilink")
          - Visual Concepts中国（Visual Concepts China）
          - Visual Concepts韩国（Visual Concepts Korea）

### 已解散工作室

  - [Frog City Software](../Page/Frog_City_Software.md "wikilink") -
    2006年与Firaxis Games合并
  - [Indie Built](../Page/Indie_Built.md "wikilink") - 2006年解散
  - [Irrational Games](../Page/Irrational_Games.md "wikilink") - 2014年解散
  - [PopTop Software](../Page/PopTop_Software.md "wikilink") -
    2006年与Firaxis Games合并
  - [Venom Games](../Page/Venom_Games.md "wikilink") - 2008年解散

## 外部連結

  - [2K品牌官方网站](http://www.2k.com)
      - [2K Games](http://www.2kgames.com/)
      - [2K Play](http://www.2kplay.com)
      - [2K
        Sports](https://web.archive.org/web/20090713061602/http://www.2ksports.com/)

[Category:2005年開業電子遊戲公司](../Category/2005年開業電子遊戲公司.md "wikilink")
[Category:美國電子遊戲公司](../Category/美國電子遊戲公司.md "wikilink")
[Category:Take-Two
Interactive的部门与子公司](../Category/Take-Two_Interactive的部门与子公司.md "wikilink")