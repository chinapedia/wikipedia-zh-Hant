**2月17日**是[阳历一年中的第](../Page/阳历.md "wikilink")48天，离全年的结束还有317天（[闰年则还有](../Page/闰年.md "wikilink")318天）。

## 大事记

### 16世紀

  - [1600年](../Page/1600年.md "wikilink")：[意大利哲学家](../Page/意大利.md "wikilink")[乔尔丹诺·布鲁诺因宣扬](../Page/乔尔丹诺·布鲁诺.md "wikilink")“太阳是神”而在[罗马被](../Page/罗马.md "wikilink")[教廷以](../Page/教廷.md "wikilink")[火刑處死](../Page/火刑.md "wikilink")。

### 17世紀

  - [1616年](../Page/1616年.md "wikilink")：[努尔哈赤自称天命汗](../Page/努尔哈赤.md "wikilink")，建立大金国，史称[后金](../Page/后金.md "wikilink")。

### 19世紀

  - [1801年](../Page/1801年.md "wikilink")：[美国选举人团选举](../Page/美国选举人团.md "wikilink")[总统出现的平局之后](../Page/美國總統.md "wikilink")，[众议院选出](../Page/美国众议院.md "wikilink")[托马斯·杰斐逊为](../Page/托马斯·杰斐逊.md "wikilink")[美国总统](../Page/美国总统.md "wikilink")。
  - [1854年](../Page/1854年.md "wikilink")：[英国承认](../Page/英国.md "wikilink")[奧蘭治自由邦独立](../Page/奧蘭治自由邦.md "wikilink")。
  - [1864年](../Page/1864年.md "wikilink")：隸屬於[南方邦聯的](../Page/南方邦聯.md "wikilink")[漢利號潛艇在](../Page/漢利號潛艇.md "wikilink")[查爾斯頓灣擊沉北方軍武裝蒸汽帆船](../Page/查爾斯頓灣.md "wikilink")，是史上第一次有潛艇在作戰中擊沉敵艦的紀錄。
  - [1895年](../Page/1895年.md "wikilink")：[北洋水师全军覆没](../Page/北洋水师.md "wikilink")，[中日甲午战争以](../Page/中日甲午战争.md "wikilink")[清朝彻底失败而告终](../Page/清朝.md "wikilink")。

### 20世紀

  - [1905年](../Page/1905年.md "wikilink")：[俄国社会革命党人杀死](../Page/俄国社会革命党.md "wikilink")[大公](../Page/大公.md "wikilink")。
  - [1916年](../Page/1916年.md "wikilink")：[英](../Page/英軍.md "wikilink")、[法军队在一次大战中占领德国殖民地](../Page/法軍.md "wikilink")[喀麦隆](../Page/喀麦隆.md "wikilink")。
  - [1927年](../Page/1927年.md "wikilink")：[台灣文化協會分裂後](../Page/台灣文化協會.md "wikilink")[蔣渭水提議組織台灣自治會](../Page/蔣渭水.md "wikilink")，並積極籌備。但因主張台灣自治以「凡標榜民族主義的任何政治結社，皆不准設立。」被禁止進行結社。
  - [1943年](../Page/1943年.md "wikilink")：為了美國更支持和同情中國抗戰
    ，[宋美齡作為](../Page/宋美齡.md "wikilink")[蔣介石之特使訪問美國](../Page/蔣介石.md "wikilink")，期間完成對美國募款任務，並於2月18日在國會發表演說為中國贏得美國同情。
  - [1944年](../Page/1944年.md "wikilink")：[第二次世界大战](../Page/第二次世界大战.md "wikilink")：[美国海军发动代号](../Page/美国海军.md "wikilink")“[冰雹](../Page/冰雹行动.md "wikilink")”的大规模海空联合[袭击行动](../Page/袭击.md "wikilink")，重创[日本在中太平洋的军事要港](../Page/大日本帝国.md "wikilink")[特鲁克](../Page/特鲁克岛.md "wikilink")。
  - [1951年](../Page/1951年.md "wikilink")：[中华人民共和国](../Page/中华人民共和国.md "wikilink")[镇压反革命运动](../Page/镇压反革命运动.md "wikilink")：北京市在[罗瑞卿的指挥下](../Page/罗瑞卿.md "wikilink")，一夜之间逮捕反革命675人，次日即公开处决了58人。
  - [1952年](../Page/1952年.md "wikilink")：中華民國工商協進會在台北成立。
  - [1959年](../Page/1959年.md "wikilink")：世界上第一颗[气象卫星](../Page/气象卫星.md "wikilink")，[美國](../Page/美國.md "wikilink")[先锋2号](../Page/先锋2号.md "wikilink")[人造卫星发射升空](../Page/人造卫星.md "wikilink")。
  - [1960年](../Page/1960年.md "wikilink")：美國民权领袖[马丁·路德·金被捕](../Page/马丁·路德·金.md "wikilink")。
  - [1979年](../Page/1979年.md "wikilink")：[中国人民解放军越过](../Page/中国人民解放军.md "wikilink")[广西](../Page/广西.md "wikilink")、[云南边境](../Page/云南.md "wikilink")，对[越南发动进攻](../Page/越南.md "wikilink")，[中越战争爆发](../Page/中越战争.md "wikilink")。
  - [1992年](../Page/1992年.md "wikilink")：[中華民國國防部首度公開第一份](../Page/中華民國國防部.md "wikilink")[國防報告書](../Page/國防報告書.md "wikilink")。

### 21世紀

  - [2007年](../Page/2007年.md "wikilink")：臺灣導演周美伶執導的電影《[刺青](../Page/刺青_\(電影\).md "wikilink")》勇奪[第57屆-{zh-hans:柏林国际电影节;zh-hk:柏林國際電影節;zh-tw:柏林影展;}-](../Page/第57屆柏林國際電影節.md "wikilink")[泰迪熊獎最佳影片](../Page/泰迪熊獎.md "wikilink")。
  - [2008年](../Page/2008年.md "wikilink")：[科索沃](../Page/科索沃.md "wikilink")[議會全票表决通过总理](../Page/議會.md "wikilink")[哈辛·塔奇提交的独立宣言](../Page/哈辛·塔奇.md "wikilink")，正式宣布脱离[塞尔维亚而](../Page/塞尔维亚.md "wikilink")[独立](../Page/科索沃独立.md "wikilink")。
  - [2010年](../Page/2010年.md "wikilink")：[北約部隊和](../Page/北約.md "wikilink")[阿富汗政府軍共](../Page/阿富汗.md "wikilink")1.5萬人在[赫爾曼德省向](../Page/赫爾曼德省.md "wikilink")[塔利班發動自](../Page/塔利班.md "wikilink")[2001年以來最大規模的攻擊](../Page/2001年.md "wikilink")。
  - [2013年](../Page/2013年.md "wikilink")：[曾雅妮在](../Page/曾雅妮.md "wikilink")[LPGA開幕戰澳洲公開賽名列亞軍](../Page/LPGA.md "wikilink")。

## 出生

  - [624年](../Page/624年.md "wikilink")：[武则天](../Page/武则天.md "wikilink")，[中国](../Page/中国.md "wikilink")[历史上唯一的女皇帝](../Page/历史.md "wikilink")（逝於[705年](../Page/705年.md "wikilink")）
  - [1878年](../Page/1878年.md "wikilink")：[連橫](../Page/連橫_\(歷史學家\).md "wikilink")，[台灣通史作者](../Page/台灣通史.md "wikilink")（逝於[1936年](../Page/1936年.md "wikilink")）
  - [1898年](../Page/1898年.md "wikilink")：[柯蒂斯·汉弗莱](../Page/柯蒂斯·汉弗莱.md "wikilink")，[美國物理学家](../Page/美國.md "wikilink")（逝於[1986年](../Page/1986年.md "wikilink")）
  - [1908年](../Page/1908年.md "wikilink")：[薄一波](../Page/薄一波.md "wikilink")，[中国政治家](../Page/中国.md "wikilink")（逝於[2007年](../Page/2007年.md "wikilink")）
  - [1924年](../Page/1924年.md "wikilink")：[瑪格麗特·杜魯門](../Page/瑪格麗特·杜魯門.md "wikilink")，[美國總統](../Page/美國總統.md "wikilink")[杜魯門的女兒](../Page/杜魯門.md "wikilink")，傳記及[推理小說](../Page/推理小說.md "wikilink")[作家](../Page/作家.md "wikilink")（逝於[2008年](../Page/2008年.md "wikilink")）
  - [1930年](../Page/1930年.md "wikilink")：[露絲·倫德爾](../Page/露絲·倫德爾.md "wikilink")，[英國](../Page/英國.md "wikilink")[驚悚和](../Page/驚悚.md "wikilink")[犯罪小說](../Page/犯罪小說.md "wikilink")[作家](../Page/作家.md "wikilink")。(逝於[2015年](../Page/2015年.md "wikilink")）
  - [1957年](../Page/1957年.md "wikilink")：[崔政宇](../Page/崔政宇.md "wikilink")，[韓國男](../Page/韓國.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [1963年](../Page/1963年.md "wikilink")：[迈克尔·乔丹](../Page/迈克尔·乔丹.md "wikilink")，美国[篮球运动员](../Page/篮球.md "wikilink")
  - [1963年](../Page/1963年.md "wikilink")：[黃仁勳](../Page/黃仁勳.md "wikilink")，[美籍](../Page/美國.md "wikilink")[台灣人](../Page/台灣.md "wikilink")，繪圖晶片公司[NVIDIA的創辦人](../Page/NVIDIA.md "wikilink")
  - [1965年](../Page/1965年.md "wikilink")：[麥可·貝](../Page/麥可·貝.md "wikilink")，[美國電影導演](../Page/美國.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink")：[沙村廣明](../Page/沙村廣明.md "wikilink")，[日本漫畫家](../Page/日本.md "wikilink")
  - [1972年](../Page/1972年.md "wikilink")：[比利·喬·阿姆斯壯](../Page/比利·喬·阿姆斯壯.md "wikilink")，美國樂團[年輕歲月主唱](../Page/年輕歲月.md "wikilink")
  - [1975年](../Page/1975年.md "wikilink")：[河莉秀](../Page/河莉秀.md "wikilink")，[韓國藝人](../Page/韓國.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[江明娟](../Page/江明娟.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[喬瑟夫·高登-李維](../Page/喬瑟夫·高登-李維.md "wikilink")，[美國](../Page/美國.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[阿德里亞諾](../Page/阿德里亞諾.md "wikilink")，[巴西](../Page/巴西.md "wikilink")[足球運動員](../Page/足球.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[邦妮·赖特](../Page/邦妮·赖特.md "wikilink")，[英國女演員](../Page/英國.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink") :
    [紅髮艾德](../Page/紅髮艾德.md "wikilink")，[英國男歌手](../Page/英國.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[佐佐木睦美](../Page/佐佐木睦美_\(插畫家\).md "wikilink")，[日本人物設計師](../Page/日本.md "wikilink")、遊戲插畫家
  - [1993年](../Page/1993年.md "wikilink")：[馬克·馬克爾斯](../Page/馬克·馬爾克斯.md "wikilink")，西班牙[世界摩托車錦標賽職業賽車手](../Page/世界摩托车锦标赛.md "wikilink")

## 逝世

  - [765年](../Page/765年.md "wikilink")：[高适](../Page/高适.md "wikilink")，唐朝邊塞诗人。（[706年出生](../Page/706年.md "wikilink")）
  - [1600年](../Page/1600年.md "wikilink")：[焦爾達諾·布魯諾](../Page/焦爾達諾·布魯諾.md "wikilink")，[意大利](../Page/意大利.md "wikilink")[數學家](../Page/數學家.md "wikilink")、[天文學家](../Page/天文學家.md "wikilink")、[哲學家](../Page/哲學家.md "wikilink")（[1548年出生](../Page/1548年.md "wikilink")）
  - [1673年](../Page/1673年.md "wikilink")：[莫裡哀](../Page/莫裡哀.md "wikilink")，[法國喜劇作家](../Page/法國.md "wikilink")、戲劇活動家（[1622年出生](../Page/1622年.md "wikilink")）
  - [1856年](../Page/1856年.md "wikilink")：[海涅](../Page/海涅.md "wikilink")，[德國詩人](../Page/德國.md "wikilink")（[1797年出生](../Page/1797年.md "wikilink")）
  - [1875年](../Page/1875年.md "wikilink")：[弗里德里希·阿格兰德](../Page/弗里德里希·阿格兰德.md "wikilink")，普鲁士天文学家，编辑出版了《[波恩星表](../Page/波恩星表.md "wikilink")》（[1799年出生](../Page/1799年.md "wikilink")）
  - [1881年](../Page/1881年.md "wikilink")：[路易斯·亨利·摩爾根](../Page/路易斯·亨利·摩爾根.md "wikilink")，[美國社會科學家](../Page/美國.md "wikilink")、民族學先驅（[1818年出生](../Page/1818年.md "wikilink")）
  - [1966年](../Page/1966年.md "wikilink")：[陳叔通](../Page/陳叔通.md "wikilink")，[全國人大常委會副委員長](../Page/全國人大常委會.md "wikilink")、[政協全國委員會副主席](../Page/政協.md "wikilink")、[中華全國工商業聯合會主任委員](../Page/中華全國工商業聯合會.md "wikilink")（[1876年出生](../Page/1876年.md "wikilink")）
  - [2014年](../Page/2014年.md "wikilink")：[高凌風](../Page/高凌風.md "wikilink")，本名葛元誠，台灣藝人。（[1950年出生](../Page/1950年.md "wikilink")）

## 节假日和习俗

  - ：[獨立日](../Page/獨立日.md "wikilink")

## 參考資料