**2月25日**是[公历一年中的第](../Page/公历.md "wikilink")56天，离全年结束还有309天（[闰年则还有](../Page/闰年.md "wikilink")310天）。

## 大事記

### 7世紀

  - [645年](../Page/645年.md "wikilink")：[中国](../Page/中国.md "wikilink")[唐朝](../Page/唐朝.md "wikilink")[佛教](../Page/佛教.md "wikilink")[僧侣](../Page/僧侣.md "wikilink")[玄奘自](../Page/玄奘.md "wikilink")[印度学成归国](../Page/印度.md "wikilink")，回到京城[长安](../Page/长安.md "wikilink")。

### 16世紀

  - [1570年](../Page/1570年.md "wikilink")：[教宗](../Page/教宗.md "wikilink")[庇护五世宣布教宗训令](../Page/庇护五世.md "wikilink")，革除[英格兰女王](../Page/英格兰.md "wikilink")[伊丽莎白一世的](../Page/伊丽莎白一世_\(英格兰\).md "wikilink")[天主教教籍](../Page/天主教.md "wikilink")。

### 19世紀

  - [1836年](../Page/1836年.md "wikilink")：[美国发明家](../Page/美国.md "wikilink")[柯尔特获得一个](../Page/萨谬尔·柯尔特.md "wikilink")[左轮手枪的](../Page/左轮手枪.md "wikilink")[专利](../Page/专利.md "wikilink")。

### 20世紀

  - [1901年](../Page/1901年.md "wikilink")：[J·P·摩根创办](../Page/J·P·摩根.md "wikilink")[美国钢铁公司](../Page/美国钢铁公司.md "wikilink")。
  - [1906年](../Page/1906年.md "wikilink")：[南昌教案发生](../Page/南昌教案.md "wikilink")。
  - [1921年](../Page/1921年.md "wikilink")：[苏联红军推翻](../Page/苏联红军.md "wikilink")[格鲁吉亚民主共和国政权](../Page/格鲁吉亚民主共和国.md "wikilink")，建立[格鲁吉亚苏维埃社会主义共和国](../Page/格鲁吉亚苏维埃社会主义共和国.md "wikilink")。
  - [1942年](../Page/1942年.md "wikilink")：[二战](../Page/二战.md "wikilink")：中国远征军进入[缅甸](../Page/缅甸.md "wikilink")。
  - [1946年](../Page/1946年.md "wikilink")：国共达成[整军协定将红军整编为国军](../Page/整军协定.md "wikilink")。
  - [1947年](../Page/1947年.md "wikilink")：[聯合國管理委員會頒布的第](../Page/聯合國.md "wikilink")47條法令，宣佈以[普魯士為名的國家正式滅亡並不獲承認](../Page/普魯士.md "wikilink")。
  - [1949年](../Page/1949年.md "wikilink")：[中华民国海军](../Page/中华民国海军.md "wikilink")[巡洋舰](../Page/巡洋舰.md "wikilink")[重庆号宣布加入](../Page/重庆号.md "wikilink")[中国人民解放军](../Page/中国人民解放军.md "wikilink")。
  - [1950年](../Page/1950年.md "wikilink")：[中华民国代总统](../Page/中华民国.md "wikilink")[李宗仁遭到弹劾](../Page/李宗仁.md "wikilink")。
  - [1951年](../Page/1951年.md "wikilink")：首届[泛美运动会在](../Page/泛美运动会.md "wikilink")[阿根廷首都](../Page/阿根廷.md "wikilink")[布宜诺斯艾利斯举行](../Page/布宜诺斯艾利斯.md "wikilink")。
  - [1954年](../Page/1954年.md "wikilink")：[纳赛尔就任](../Page/纳赛尔.md "wikilink")[埃及首相](../Page/埃及.md "wikilink")。
  - [1956年](../Page/1956年.md "wikilink")：[苏联领袖](../Page/苏联.md "wikilink")[赫鲁晓夫在](../Page/赫鲁晓夫.md "wikilink")[苏共二十大上突然进行](../Page/苏共二十大.md "wikilink")[秘密报告](../Page/关于个人崇拜及其后果.md "wikilink")，彻底批判[斯大林](../Page/斯大林.md "wikilink")。
  - [1957年](../Page/1957年.md "wikilink")：[岸信介就任](../Page/岸信介.md "wikilink")[日本首相](../Page/日本首相.md "wikilink")。
  - [1977年](../Page/1977年.md "wikilink")：[九巴一輛](../Page/九巴.md "wikilink")[51號線巴士](../Page/九龍巴士51號線.md "wikilink")，由[大角咀碼頭駛往](../Page/大角咀碼頭.md "wikilink")[元朗途中](../Page/元朗.md "wikilink")，於[石崗撞死一名騎](../Page/石崗.md "wikilink")[單車男子後](../Page/單車.md "wikilink")，衝入一間[超級市場後全車焚毀](../Page/超級市場.md "wikilink")，事故造成六死二十二傷。
  - [1986年](../Page/1986年.md "wikilink")：[菲律賓](../Page/菲律賓.md "wikilink")[人民力量革命勝利](../Page/人民力量革命_\(1986年\).md "wikilink")，[科拉松·阿基诺正式就任](../Page/科拉松·阿基诺.md "wikilink")[菲律宾总统](../Page/菲律宾总统.md "wikilink")，曾軍事獨裁統治長達21年的前总统[费迪南德·马科斯逃亡到](../Page/费迪南德·马科斯.md "wikilink")[夏威夷](../Page/夏威夷.md "wikilink")。
  - [1990年](../Page/1990年.md "wikilink")：[奧萊塔·查莫羅当选](../Page/奧萊塔·查莫羅.md "wikilink")[尼加拉瓜第一位女总统](../Page/尼加拉瓜.md "wikilink")。
  - [1991年](../Page/1991年.md "wikilink")：华约政治协商委员会特别会议宣布[华沙条约组织所有军事机构将全部解散](../Page/华沙条约组织.md "wikilink")。
  - [1992年](../Page/1992年.md "wikilink")：[纳戈尔诺－卡拉巴赫战争期间](../Page/纳戈尔诺－卡拉巴赫战争.md "wikilink")[亚美尼亚军队于](../Page/亚美尼亚.md "wikilink")[阿塞拜疆](../Page/阿塞拜疆.md "wikilink")[纳哥诺卡拉巴克地区进行](../Page/纳哥诺卡拉巴克.md "wikilink")[种族屠杀](../Page/霍贾利灭绝种族.md "wikilink")，最终造成当地613位阿塞拜疆族族人丧生。
  - [1994年](../Page/1994年.md "wikilink")：[约旦发生](../Page/约旦.md "wikilink")[易卜拉欣大惨案](../Page/易卜拉欣大惨案.md "wikilink")。

### 21世紀

  - [2008年](../Page/2008年.md "wikilink")：[李明博就任韓國第](../Page/李明博.md "wikilink")17任總統。
  - 2008年：[勞爾·卡斯特羅接替兄長](../Page/勞爾·卡斯特羅.md "wikilink")[菲德爾·卡斯特羅出任](../Page/菲德爾·卡斯特羅.md "wikilink")[古巴國家元首](../Page/古巴共和國.md "wikilink")。
  - [2009年](../Page/2009年.md "wikilink")：[孟加拉國準軍事組織步槍隊的成員在](../Page/孟加拉國.md "wikilink")[首都](../Page/首都.md "wikilink")[達卡的](../Page/達卡.md "wikilink")[步槍隊總部](../Page/孟加拉國步槍隊.md "wikilink")[發動叛亂](../Page/孟加拉國步槍隊叛亂.md "wikilink")，並和政府軍警發生槍戰，造成約50人死亡。
  - 2009年：東北季候風偏弱，[香港天氣異常溫暖](../Page/香港.md "wikilink")，[香港天文台於當日錄得最高氣溫](../Page/香港天文台.md "wikilink")28.3°C，是自1884年有記錄以來2月份的最高氣溫。\[1\]
  - [2011年](../Page/2011年.md "wikilink")：[美國海軍](../Page/美國海軍.md "wikilink")[福特級核動力航空母艦二号舰](../Page/福特級核動力航空母艦.md "wikilink")[CVN-79在](../Page/CVN-79.md "wikilink")[紐波特紐斯造船廠切割第一块钢板](../Page/紐波特紐斯造船及船塢公司.md "wikilink")。
  - [2013年](../Page/2013年.md "wikilink")：[朴槿惠就任](../Page/朴槿惠.md "wikilink")[韩国第](../Page/韩国.md "wikilink")18任总统，是[韩国历史上第一位女总统](../Page/韩国.md "wikilink")。
  - [2018年](../Page/2018年.md "wikilink")：[中国共产党中央委员会提议从宪法中删除中华人民共和国主席和副主席](../Page/中国共产党中央委员会.md "wikilink")“连续任期不得超过两届”的表述。\[2\]

## 出生

  - [1643年](../Page/1643年.md "wikilink")：[艾哈迈德二世](../Page/艾哈迈德二世.md "wikilink")，[奥斯曼帝国苏丹](../Page/奥斯曼帝国.md "wikilink")（逝於[1695年](../Page/1695年.md "wikilink")）
  - [1778年](../Page/1778年.md "wikilink")：[何塞·德·圣马丁](../Page/何塞·德·圣马丁.md "wikilink")，[南美洲独立运动领袖](../Page/南美洲.md "wikilink")（逝於[1850年](../Page/1850年.md "wikilink")）
  - [1841年](../Page/1841年.md "wikilink")：[皮耶-奧古斯特·雷諾瓦](../Page/皮耶-奧古斯特·雷諾瓦.md "wikilink")，[法国](../Page/法国.md "wikilink")[印象派画家](../Page/印象派.md "wikilink")（逝於[1919年](../Page/1919年.md "wikilink")）
  - [1873年](../Page/1873年.md "wikilink")：[恩里科·卡鲁索](../Page/恩里科·卡鲁索.md "wikilink")，[意大利](../Page/意大利.md "wikilink")[男高音](../Page/男高音.md "wikilink")（逝於[1921年](../Page/1921年.md "wikilink")）
  - [1888年](../Page/1888年.md "wikilink")：[约翰·福斯特·杜勒斯](../Page/约翰·福斯特·杜勒斯.md "wikilink")，[美国国务卿](../Page/美国国务卿.md "wikilink")（逝於[1959年](../Page/1959年.md "wikilink")）
  - [1910年](../Page/1910年.md "wikilink")：[白嘉時](../Page/白嘉時.md "wikilink")，[香港政治人物](../Page/香港.md "wikilink")（逝於[1998年](../Page/1998年.md "wikilink")）
  - [1911年](../Page/1911年.md "wikilink")：[谭其骧](../Page/谭其骧.md "wikilink")，[中国](../Page/中国.md "wikilink")[历史学家](../Page/历史学家.md "wikilink")（逝于[1992年](../Page/1992年.md "wikilink")）
  - [1917年](../Page/1917年.md "wikilink")：[安东尼·伯吉斯](../Page/安东尼·伯吉斯.md "wikilink")，[英国小说家](../Page/英国.md "wikilink")（逝於[1993年](../Page/1993年.md "wikilink")）
  - [1920年](../Page/1920年.md "wikilink")：[文鲜明](../Page/文鲜明.md "wikilink")，韩国[统一教创办人](../Page/统一教.md "wikilink")、教主（逝於[2012年](../Page/2012年.md "wikilink")）
  - [1924年](../Page/1924年.md "wikilink")：，[美國女演員](../Page/美國.md "wikilink")（逝於[2001年](../Page/2001年.md "wikilink")）
  - [1928年](../Page/1928年.md "wikilink")：[許文龍](../Page/許文龍.md "wikilink")，[台灣企業家](../Page/台灣.md "wikilink")
  - [1940年](../Page/1940年.md "wikilink")：[呂運計](../Page/呂運計.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")（逝於[2009年](../Page/2009年.md "wikilink")）
  - [1943年](../Page/1943年.md "wikilink")：[乔治·哈里森](../Page/乔治·哈里森.md "wikilink")，英國流行樂手，披頭士成員之一
    （逝世於[2001年](../Page/2001年.md "wikilink")）
  - [1950年](../Page/1950年.md "wikilink")：[内斯托尔·基什内尔](../Page/内斯托尔·基什内尔.md "wikilink")，[阿根廷政治家](../Page/阿根廷.md "wikilink")（逝於[2010年](../Page/2010年.md "wikilink")）
  - [1958年](../Page/1958年.md "wikilink")：[王芷蕾](../Page/王芷蕾.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")
  - [1962年](../Page/1962年.md "wikilink")：[許晉亨](../Page/許晉亨.md "wikilink")，香港中建企業董事，藝人[李嘉欣的丈夫](../Page/李嘉欣.md "wikilink")
  - [1965年](../Page/1965年.md "wikilink")：[楊繡惠](../Page/楊繡惠.md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")
  - [1967年](../Page/1967年.md "wikilink")：[王書麒](../Page/王書麒.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [1968年](../Page/1968年.md "wikilink")：[劉玉婷](../Page/劉玉婷_\(香港\).md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink")：[李綺虹](../Page/李綺虹.md "wikilink")，[香港電視](../Page/香港.md "wikilink")、電影演員
  - [1971年](../Page/1971年.md "wikilink")：[丹尼爾·帕德](../Page/丹尼爾·帕德.md "wikilink")，[加拿大歌手](../Page/加拿大.md "wikilink")、鋼琴家
  - [1974年](../Page/1974年.md "wikilink")：[森久保祥太郎](../Page/森久保祥太郎.md "wikilink")，[日本](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1975年](../Page/1975年.md "wikilink")：[千葉千惠巳](../Page/千葉千惠巳.md "wikilink")，日本女性聲優
  - [1981年](../Page/1981年.md "wikilink")：-{[朴智星](../Page/朴智星.md "wikilink")}-，[南韓](../Page/南韓.md "wikilink")[足球運動員](../Page/足球.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[韓宜邦](../Page/韓宜邦.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[沙希德·卡普爾](../Page/沙希德·卡普爾.md "wikilink")，[印度男演員](../Page/印度.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[黃啟昌](../Page/黃啟昌.md "wikilink")，香港配音員
  - [1982年](../Page/1982年.md "wikilink")：[克里斯·拜尔德](../Page/克里斯·拜尔德.md "wikilink")，[北愛爾蘭足球運動員](../Page/北愛爾蘭.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[黃婉曼](../Page/黃婉曼.md "wikilink")，[香港主持人](../Page/香港.md "wikilink")、前新聞主播
  - [1984年](../Page/1984年.md "wikilink")：[松本若菜](../Page/松本若菜.md "wikilink")，[日本女演員](../Page/日本.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[Superfly](../Page/Superfly.md "wikilink")（越智志帆），[日本女歌手](../Page/日本.md "wikilink")
  - [1985年](../Page/1985年.md "wikilink")：[喬金·諾亞](../Page/喬金·諾亞.md "wikilink")，[美國職業籃球員](../Page/美國.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[手束真知子](../Page/手束真知子.md "wikilink")，[日本寫真女星](../Page/日本.md "wikilink")、前[SKE48及](../Page/SKE48.md "wikilink")[SDN48成員](../Page/SDN48.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[朴政民](../Page/朴正民.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[都想友](../Page/都想友.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[鄒凱](../Page/鄒凱.md "wikilink")，中國[體操運動員](../Page/競技體操.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[花澤香菜](../Page/花澤香菜.md "wikilink")，[日本女性聲優](../Page/日本.md "wikilink")、演員
  - [1989年](../Page/1989年.md "wikilink")：[李相花](../Page/李相花.md "wikilink")，[韓國速滑運動員](../Page/韓國.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[黄雪辰](../Page/黄雪辰.md "wikilink")，[中國女子](../Page/中國.md "wikilink")[花樣游泳運動員](../Page/花樣游泳.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[何建曦](../Page/何建曦.md "wikilink")，香港樂團[C
    AllStar成員](../Page/C_AllStar.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[凯莉·帕尔默](../Page/凯莉·帕尔默.md "wikilink")，[澳大利亚女子](../Page/澳大利亚.md "wikilink")[游泳运动员](../Page/游泳.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[可晴](../Page/可晴.md "wikilink")，[馬來西亞](../Page/馬來西亞.md "wikilink")[女歌手](../Page/女歌手.md "wikilink")、[女演員](../Page/女演員.md "wikilink")
  - [1994年](../Page/1994年.md "wikilink")：[尤金妮·保查特](../Page/尤金妮·保查特.md "wikilink")，[加拿大女子網球運動員](../Page/加拿大.md "wikilink")
  - [1995年](../Page/1995年.md "wikilink")：[金永宰](../Page/金永宰.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink")：[伊莎貝拉·傅爾曼](../Page/伊莎贝拉·弗尔曼.md "wikilink")，[美國女演員](../Page/美國.md "wikilink")、童星
  - [1999年](../Page/1999年.md "wikilink")：[朴敏赫](../Page/朴敏赫.md "wikilink")，[韓國男子偶像團體ASTRO成員](../Page/韓國.md "wikilink")

## 逝世

  - [1553年](../Page/1553年.md "wikilink")：[平手政秀](../Page/平手政秀.md "wikilink")，[日本政治家](../Page/日本.md "wikilink")，[织田信长的傅役兼大老](../Page/织田信长.md "wikilink")（[1492年出生](../Page/1492年.md "wikilink")）
  - [1577年](../Page/1577年.md "wikilink")：[埃里克十四世](../Page/埃里克十四世.md "wikilink")，[瑞典国王](../Page/瑞典国王.md "wikilink")（[1533年出生](../Page/1533年.md "wikilink")）
  - [1713年](../Page/1713年.md "wikilink")：[腓特烈一世](../Page/腓特烈一世_\(普魯士\).md "wikilink")，[普鲁士国王](../Page/普鲁士国王.md "wikilink")（[1657年出生](../Page/1657年.md "wikilink")）
  - [1715年](../Page/1715年.md "wikilink")：[蒲松龄](../Page/蒲松龄.md "wikilink")，中国小说家（[1640年出生](../Page/1640年.md "wikilink")）
  - [1850年](../Page/1850年.md "wikilink")：[道光皇帝](../Page/道光皇帝.md "wikilink")，中国[清朝皇帝](../Page/清朝.md "wikilink")（[1782年出生](../Page/1782年.md "wikilink")）
  - [1852年](../Page/1852年.md "wikilink")：，[爱尔兰诗人](../Page/爱尔兰.md "wikilink")（[1779年出生](../Page/1779年.md "wikilink")）
  - [1912年](../Page/1912年.md "wikilink")：[威廉四世](../Page/威廉四世_\(盧森堡大公\).md "wikilink")，[盧森堡大公](../Page/盧森堡.md "wikilink")（[1852年出生](../Page/1852年.md "wikilink")）
  - [1947年](../Page/1947年.md "wikilink")：[弗裡德里希·帕邢](../Page/弗裡德里希·帕邢.md "wikilink")，德國物理學家（[1865年出生](../Page/1865年.md "wikilink")）
  - [1971年](../Page/1971年.md "wikilink")：[特奥多尔·斯韦德贝里](../Page/特奥多尔·斯韦德贝里.md "wikilink")，[瑞典化学家](../Page/瑞典.md "wikilink")（[1884年出生](../Page/1884年.md "wikilink")）
  - [1975年](../Page/1975年.md "wikilink")：[向前](../Page/向前.md "wikilink")，[中華民國大陸時期軍事人物](../Page/中華民國.md "wikilink")，[香港社會活動家](../Page/香港.md "wikilink")（[1907年出生](../Page/1907年.md "wikilink")）
  - [1982年](../Page/1982年.md "wikilink")：[趙元任](../Page/趙元任.md "wikilink")，中國語言學家（[1892年出生](../Page/1892年.md "wikilink")）
  - [1983年](../Page/1983年.md "wikilink")：[田纳西·威廉斯](../Page/田纳西·威廉斯.md "wikilink")，[美国剧作家](../Page/美国.md "wikilink")（[1911年出生](../Page/1911年.md "wikilink")）
  - [1986年](../Page/1986年.md "wikilink")：[杜聰明](../Page/杜聰明.md "wikilink")，[台灣第一位](../Page/台灣.md "wikilink")[醫學博士](../Page/醫學.md "wikilink")，創立[高雄醫學院](../Page/高雄醫學大學.md "wikilink")（[1893年出生](../Page/1893年.md "wikilink")）
  - [1987年](../Page/1987年.md "wikilink")：，美国演员（[1930年出生](../Page/1930年.md "wikilink")）
  - [1996年](../Page/1996年.md "wikilink")：[吳漢潤](../Page/吳漢潤.md "wikilink")，[柬埔寨裔美国演员](../Page/柬埔寨.md "wikilink")（[1940年出生](../Page/1940年.md "wikilink")）
  - [1999年](../Page/1999年.md "wikilink")：[格伦·西奥多·西博格](../Page/格伦·西奥多·西博格.md "wikilink")，美国化学家（[1912年出生](../Page/1912年.md "wikilink")）
  - [2005年](../Page/2005年.md "wikilink")：[彼得·本南森](../Page/彼得·本南森.md "wikilink")，[英国律师](../Page/英国.md "wikilink")，[國際特赦組織](../Page/國際特赦組織.md "wikilink")（大赦国际，Amnesty
    International）的创办人（[1921年出生](../Page/1921年.md "wikilink")）
  - [2006年](../Page/2006年.md "wikilink")：[梁靈光](../Page/梁靈光.md "wikilink")，前[廣東省委書記](../Page/廣東省.md "wikilink")、省長；[珠江三角洲概念提倡者](../Page/珠江三角洲.md "wikilink")（[1916年出生](../Page/1916年.md "wikilink")）
  - 2006年：，美國演員（[1922年出生](../Page/1922年.md "wikilink")）
  - [2012年](../Page/2012年.md "wikilink")：[陳之藩](../Page/陳之藩.md "wikilink")，[臺灣](../Page/臺灣.md "wikilink")[電機工程學者](../Page/電機工程.md "wikilink")、作家\[3\]（[1925年出生](../Page/1925年.md "wikilink")）

## 节日、风俗习惯

  - ：国庆日

## 參考資料

1.  [香港天文台
    異常溫暖的二月](http://www.weather.gov.hk/wxinfo/news/2009/pre0301c.htm)
2.  [CPC proposes change on Chinese president's term in
    Constitution](http://www.xinhuanet.com/english/2018-02/25/c_136998770.htm)
3.