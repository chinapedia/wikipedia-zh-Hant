**2月2日**是[阳历年的第](../Page/阳历.md "wikilink")33天，离一年的结束还有332天（[闰年是](../Page/闰年.md "wikilink")333天）。

## 大事记

### 6世紀

  - [506年](../Page/506年.md "wikilink")：[西哥特第八任国王](../Page/西哥特王国.md "wikilink")[亚拉里克二世公布](../Page/亚拉里克二世.md "wikilink")《亚拉里克法律要略》。

### 10世紀

  - [962年](../Page/962年.md "wikilink")：[奥托一世在](../Page/奥托一世_\(神圣罗马帝国\).md "wikilink")[罗马由](../Page/罗马.md "wikilink")[教宗若望十二世加冕成为](../Page/教宗若望十二世.md "wikilink")[神圣罗马帝国皇帝](../Page/神圣罗马帝国皇帝.md "wikilink")。

### 11世紀

  - [1032年](../Page/1032年.md "wikilink")：勃艮第王国成为[康拉德二世统治下的](../Page/康拉德二世_\(神圣罗马帝国\).md "wikilink")[神圣罗马帝国的一部分](../Page/神圣罗马帝国.md "wikilink")。

### 15世紀

  - [1421年](../Page/1421年.md "wikilink")：[明朝正式迁都](../Page/明朝.md "wikilink")[北京](../Page/北京.md "wikilink")。

### 16世紀

  - [1535年](../Page/1535年.md "wikilink")：[阿根廷城市](../Page/阿根廷.md "wikilink")[布宜诺斯艾利斯建成](../Page/布宜诺斯艾利斯.md "wikilink")。

### 19世紀

  - [1848年](../Page/1848年.md "wikilink")：[美墨战争结束](../Page/美墨战争.md "wikilink")。
  - [1895年](../Page/1895年.md "wikilink")：[日军占领](../Page/日军.md "wikilink")[威海卫](../Page/威海卫.md "wikilink")。

### 20世紀

  - [1913年](../Page/1913年.md "wikilink")：[纽约中央火车站启用](../Page/纽约中央火车站.md "wikilink")。

  - [1914年](../Page/1914年.md "wikilink")：[北洋政府决定开辟](../Page/北洋政府.md "wikilink")[山东](../Page/山东.md "wikilink")[龙口为对外贸易商埠](../Page/龙口.md "wikilink")。

  - [1920年](../Page/1920年.md "wikilink")：[爱沙尼亚同](../Page/爱沙尼亚.md "wikilink")[苏联签订和约并宣布独立](../Page/苏联.md "wikilink")。

  - 1920年：[胡适启用新式](../Page/胡适.md "wikilink")[标点符号](../Page/标点符号.md "wikilink")。

  - [1922年](../Page/1922年.md "wikilink")：[爱尔兰](../Page/爱尔兰.md "wikilink")[意识流文学作家](../Page/意识流文学.md "wikilink")[詹姆斯·乔伊斯创作的长篇小说](../Page/詹姆斯·乔伊斯.md "wikilink")《[尤利西斯](../Page/尤利西斯.md "wikilink")》正式出版。

  - [1923年](../Page/1923年.md "wikilink")：[英國從愛爾蘭撤軍](../Page/英國.md "wikilink")。

  -
  - [1943年](../Page/1943年.md "wikilink")：[斯大林格勒城裡的](../Page/斯大林格勒.md "wikilink")[德军向](../Page/德军.md "wikilink")[苏军投降](../Page/苏军.md "wikilink")，九万多名[納粹德國士兵成为](../Page/納粹德國.md "wikilink")[战俘](../Page/战俘.md "wikilink")，[斯大林格勒战役结束](../Page/斯大林格勒战役.md "wikilink")。

  - [1948年](../Page/1948年.md "wikilink")：年关将近，[中国](../Page/中華民國.md "wikilink")[上海军警驱赶占据申新第九棉纺织厂的罢工工人](../Page/上海.md "wikilink")，造成“[申九惨案](../Page/申九惨案.md "wikilink")”，3名女工死亡。

  - [1960年](../Page/1960年.md "wikilink")：[中国](../Page/中国.md "wikilink")[山西发生严重](../Page/山西.md "wikilink")[食物中毒事件](../Page/食物中毒.md "wikilink")，61位农民工食物中毒。曾入选中学课本的通讯《为了六十一个阶级弟兄》即为此故事所写。

  - [1967年](../Page/1967年.md "wikilink")：[美国篮球协会](../Page/美国篮球协会.md "wikilink")（[ABA](../Page/ABA.md "wikilink")）宣布成立。

  - [1971年](../Page/1971年.md "wikilink")：《[濕地公約](../Page/濕地公約.md "wikilink")》（又稱《[拉姆薩爾公約](../Page/拉姆薩爾公約.md "wikilink")》）在[伊朗](../Page/伊朗.md "wikilink")[拉姆薩爾簽署](../Page/拉姆薩爾.md "wikilink")。

  - [1974年](../Page/1974年.md "wikilink")：[美国](../Page/美国.md "wikilink")[通用动力公司设计的](../Page/通用动力公司.md "wikilink")[F-16战隼战斗机原型机进行首次试飞](../Page/F-16战隼战斗机.md "wikilink")。

  - [1981年](../Page/1981年.md "wikilink")：[韓國教育放送公社創立](../Page/韓國教育放送公社.md "wikilink")。

  - [1990年](../Page/1990年.md "wikilink")：[南非总统](../Page/南非总统.md "wikilink")[戴克拉克宣布废除对](../Page/弗雷德里克·威廉·戴克拉克.md "wikilink")[南非非洲人国民大会等反](../Page/南非非洲人国民大会.md "wikilink")[种族隔离](../Page/南非种族隔离制度.md "wikilink")[政党的禁令](../Page/政党.md "wikilink")，同日，[蘇聯取消一黨制](../Page/蘇聯.md "wikilink")。

### 21世紀

  - [2004年](../Page/2004年.md "wikilink")：[瑞士](../Page/瑞士.md "wikilink")[網球選手](../Page/網球.md "wikilink")[羅傑·費德勒成為](../Page/羅傑·費德勒.md "wikilink")[ATP世界排名第一男子單打選手](../Page/ATP世界排名第一男子單打選手列表.md "wikilink")，之後以連續237周的保持成績打破了[過往紀錄](../Page/ATP世界巡迴賽紀錄.md "wikilink")。
  - [2008年](../Page/2008年.md "wikilink")：法国总统[萨科齐和著名模特](../Page/萨科齐.md "wikilink")、歌手[卡拉·布吕尼闪电结婚](../Page/卡拉·布吕尼.md "wikilink")。
  - [2009年](../Page/2009年.md "wikilink")：[辛巴威儲備銀行第三度對](../Page/辛巴威儲備銀行.md "wikilink")[辛巴威元給予](../Page/辛巴威元.md "wikilink")[貨幣貶值](../Page/貨幣貶值.md "wikilink")，使得新制度的1辛巴威元相當於過去1兆辛巴威元。
  - [2017年](../Page/2017年.md "wikilink")：一尊[观音像于](../Page/塘埔观音像.md "wikilink")[广东省](../Page/广东省.md "wikilink")[揭阳市](../Page/揭阳市.md "wikilink")[渔湖镇](../Page/渔湖镇.md "wikilink")[榕江边发现](../Page/榕江_\(广东\).md "wikilink")。

## 出生

  - [1882年](../Page/1882年.md "wikilink")：[詹姆斯·喬伊斯](../Page/詹姆斯·喬伊斯.md "wikilink")，[愛爾蘭作家和詩人](../Page/愛爾蘭.md "wikilink")。（[1941年逝世](../Page/1941年.md "wikilink")）
  - [1895年](../Page/1895年.md "wikilink")：[陳澄波](../Page/陳澄波.md "wikilink")，[台灣畫家](../Page/台灣.md "wikilink")，[二二八事件受難者](../Page/二二八事件.md "wikilink")。（[1947年逝世](../Page/1947年.md "wikilink")）
  - [1901年](../Page/1901年.md "wikilink")：[雅沙·海飛茲](../Page/雅沙·海飛茲.md "wikilink")，[立陶宛](../Page/立陶宛.md "wikilink")[小提琴家](../Page/小提琴家.md "wikilink")。（[1987年逝世](../Page/1987年.md "wikilink")）
  - [1905年](../Page/1905年.md "wikilink")：[艾茵·蘭德](../Page/艾茵·蘭德.md "wikilink")，俄裔美國哲學家和小說家。（[1982年逝世](../Page/1982年.md "wikilink")）
  - [1929年](../Page/1929年.md "wikilink")：[維拉·希蒂洛娃](../Page/維拉·希蒂洛娃.md "wikilink")，[捷克電影導演](../Page/捷克.md "wikilink")（[2014年逝世](../Page/2014年.md "wikilink")）
  - [1934年](../Page/1934年.md "wikilink")：[曾憲梓](../Page/曾憲梓.md "wikilink")，[香港政界人士](../Page/香港.md "wikilink")
  - [1944年](../Page/1944年.md "wikilink")：[釋心定](../Page/釋心定.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[僧人](../Page/僧人.md "wikilink")
  - [1944年](../Page/1944年.md "wikilink")：[阿基勒·阿基洛夫](../Page/阿基勒·阿基洛夫.md "wikilink")，[塔吉克斯坦](../Page/塔吉克斯坦.md "wikilink")[總理](../Page/塔吉克斯坦總理.md "wikilink")。
  - [1948年](../Page/1948年.md "wikilink")：[蔡志忠](../Page/蔡志忠.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")。
  - [1952年](../Page/1952年.md "wikilink")：[朴槿惠](../Page/朴槿惠.md "wikilink")，第18任[大韓民國總統](../Page/大韓民國總統.md "wikilink")
  - [1954年](../Page/1954年.md "wikilink")：[梁能仁](../Page/梁能仁.md "wikilink")，[香港足球運動員](../Page/香港.md "wikilink")
  - [1958年](../Page/1958年.md "wikilink")：[尹揚明](../Page/尹揚明.md "wikilink")，[香港](../Page/香港.md "wikilink")[演員](../Page/演員.md "wikilink")。
  - [1960年](../Page/1960年.md "wikilink")：[惠英紅](../Page/惠英紅.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [1966年](../Page/1966年.md "wikilink")：[鶴卷和哉](../Page/鶴卷和哉.md "wikilink")，[日本](../Page/日本.md "wikilink")[動畫監督](../Page/動畫.md "wikilink")。
  - [1969年](../Page/1969年.md "wikilink")：[彭羚](../Page/彭羚.md "wikilink")，香港女歌手。
  - [1972年](../Page/1972年.md "wikilink")：[HISASHI](../Page/HISASHI.md "wikilink")，[日本乐队](../Page/日本.md "wikilink")[GLAY成员](../Page/GLAY.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[夏奇拉](../Page/夏奇拉.md "wikilink")，[哥伦比亚女歌手](../Page/哥伦比亚.md "wikilink")。
  - [1978年](../Page/1978年.md "wikilink")：[李智雅](../Page/李智雅.md "wikilink")，[韓國演員](../Page/韓國.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[北川弘美](../Page/北川弘美.md "wikilink")，[日本女演員](../Page/日本.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[許哲珮](../Page/許哲珮.md "wikilink")，台灣女歌手
  - [1981年](../Page/1981年.md "wikilink")：[草莓牛奶](../Page/草莓牛奶_\(AV女優\).md "wikilink")，[日本](../Page/日本.md "wikilink")[AV女优](../Page/AV女优.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[簡美妍](../Page/簡美妍.md "wikilink")，[韓國演員](../Page/韓國.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[胡金龍](../Page/胡金龍.md "wikilink")，台灣[棒球選手](../Page/棒球.md "wikilink")。
  - [1985年](../Page/1985年.md "wikilink")：[Michael
    Rivas](../Page/Michael_Rivas.md "wikilink")，[日本歌手](../Page/日本.md "wikilink")、[聲優](../Page/聲優.md "wikilink")。
  - [1985年](../Page/1985年.md "wikilink")：[桐山漣](../Page/桐山漣.md "wikilink")，日本演員
  - [1985年](../Page/1985年.md "wikilink")：[陳鈺芸](../Page/陳鈺芸.md "wikilink")，[香港歌手](../Page/香港歌手.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[劉祿存](../Page/劉祿存.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")、演員
  - [1987年](../Page/1987年.md "wikilink")：[宋茜](../Page/宋茜.md "wikilink")，[中國籍歌手](../Page/中國.md "wikilink")、韩国组合[F(x)隊長](../Page/F\(x\)_\(組合\).md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[鄭河娜](../Page/鄭河娜.md "wikilink")，[韓國歌手](../Page/韓國.md "wikilink")、舞者
  - [1993年](../Page/1993年.md "wikilink")：[P.O](../Page/P.O.md "wikilink")，[韓國歌手](../Page/韓國.md "wikilink")、组合[Block
    B成員](../Page/Block_B.md "wikilink")

## 逝世

  - [1907年](../Page/1907年.md "wikilink")：[德米特里·伊萬諾維奇·-{zh-tw: 門得列夫;
    zh-cn:门捷列夫;
    zh-hk:門捷列夫;}-](../Page/德米特里·伊萬諾維奇·門捷列夫.md "wikilink")，[俄国化学家](../Page/俄国.md "wikilink")。（生于[1834年](../Page/1834年.md "wikilink")）
  - [1925年](../Page/1925年.md "wikilink")：[米哈伊尔·伏龙芝](../Page/米哈伊尔·伏龙芝.md "wikilink")，[苏联统帅](../Page/苏联.md "wikilink")、军事理论家。（生於[1885年](../Page/1885年.md "wikilink")）
  - [1944年](../Page/1944年.md "wikilink")：[朱生豪](../Page/朱生豪.md "wikilink")，[中國翻译家](../Page/中國.md "wikilink")。（生於[1912年](../Page/1912年.md "wikilink")）
  - [1969年](../Page/1969年.md "wikilink")：[熊庆来](../Page/熊庆来.md "wikilink")，[中国数学家](../Page/中国.md "wikilink")。（生于[1893年](../Page/1893年.md "wikilink")）
  - [1970年](../Page/1970年.md "wikilink")：[伯特蘭·羅素](../Page/伯特蘭·羅素.md "wikilink")，思想家。（生于[1872年](../Page/1872年.md "wikilink")）
  - [1978年](../Page/1978年.md "wikilink")：[唐君毅](../Page/唐君毅.md "wikilink")，[新儒家](../Page/新儒家.md "wikilink")，哲學家，教育家，思想家。（生于[1909年](../Page/1909年.md "wikilink")）
  - [1979年](../Page/1979年.md "wikilink")：[席德·维瑟斯](../Page/席德·维瑟斯.md "wikilink")，[英国](../Page/英国.md "wikilink")[贝斯手](../Page/低音吉他.md "wikilink")（[性手枪樂團](../Page/性手枪.md "wikilink")）。（生于[1957年](../Page/1957年.md "wikilink")）
  - [1997年](../Page/1997年.md "wikilink")：[秦基偉](../Page/秦基偉.md "wikilink")，原[中國共產黨中央政治局委員](../Page/中國共產黨中央政治局.md "wikilink")、第八屆[全國人民代表大會常務委員會副委員長](../Page/全國人民代表大會常務委員會.md "wikilink")、[中央軍事委員會前委員](../Page/中央軍事委員會.md "wikilink")、[國務委員兼](../Page/國務委員.md "wikilink")[中華人民共和國國防部部長](../Page/中華人民共和國國防部.md "wikilink")、[北京軍區原司令員](../Page/北京軍區.md "wikilink")。（生於[1914年](../Page/1914年.md "wikilink"))
  - [2008年](../Page/2008年.md "wikilink")：[林之助](../Page/林之助.md "wikilink")，台灣[膠彩畫之父](../Page/膠彩畫.md "wikilink")。（生於[1917年](../Page/1917年.md "wikilink")）
  - 2008年：[姚卓然](../Page/姚卓然.md "wikilink")，[香港足球運動員](../Page/香港.md "wikilink")。（生於[1929年](../Page/1929年.md "wikilink")）
  - [2010年](../Page/2010年.md "wikilink")：[黃廷方](../Page/黃廷方.md "wikilink")，[新加坡及](../Page/新加坡.md "wikilink")[香港企業家](../Page/香港.md "wikilink")。（生於[1929年](../Page/1929年.md "wikilink")）
  - [2014年](../Page/2014年.md "wikilink")：[菲利浦·西摩·霍夫曼](../Page/菲利浦·西摩·霍夫曼.md "wikilink")，[美國第](../Page/美國.md "wikilink")78屆奧斯卡最佳男主角獎。（生於[1967年](../Page/1967年.md "wikilink")）

## 节假日和习俗

  - [土撥鼠日](../Page/土撥鼠日.md "wikilink")（[北美](../Page/北美.md "wikilink")）
  - [雙馬尾日](../Page/雙馬尾日.md "wikilink")（）
  - [可麗餅日](../Page/可麗餅日.md "wikilink")（）
  - [聖燭節](../Page/聖燭節.md "wikilink")（）

## 世界日及國際日

  - [世界濕地日](../Page/世界濕地日.md "wikilink")