**2月6日**是[阳历年的第](../Page/阳历.md "wikilink")37天，离一年的结束还有328天（[闰年是](../Page/闰年.md "wikilink")329天）。

## 大事记

### 17世紀

  - [1685年](../Page/1685年.md "wikilink")：英王[查理二世駕崩](../Page/查理二世_\(英國\).md "wikilink")，其弟[詹姆斯二世繼承王位](../Page/詹姆斯二世_\(英國\).md "wikilink")。

### 18世紀

  - [1778年](../Page/1778年.md "wikilink")：[法国與](../Page/法国.md "wikilink")[美国在](../Page/美国.md "wikilink")[巴黎簽訂](../Page/巴黎.md "wikilink")《法美同盟條約》，協議中兩國同意當某方遭受到英國的攻擊，則另一方需派兵支援，而且禁止雙方與英國議和，直到英國承認美國獨立為止。
  - [1788年](../Page/1788年.md "wikilink")：[麻薩諸塞州成為第六個批准美國憲法的州](../Page/麻薩諸塞州.md "wikilink")。

### 19世紀

  - [1819年](../Page/1819年.md "wikilink")：[英国殖民者](../Page/英国.md "wikilink")[斯坦福·莱佛士宣布](../Page/斯坦福·莱佛士.md "wikilink")[天猛公为](../Page/天猛公.md "wikilink")[柔佛新](../Page/柔佛.md "wikilink")[苏丹](../Page/苏丹.md "wikilink")，并与其订立协议，使[新加坡成为英国殖民地](../Page/新加坡.md "wikilink")。
  - [1833年](../Page/1833年.md "wikilink")：巴伐利亞王子[奧托一世被選為第一位近代](../Page/奥托一世_\(希腊\).md "wikilink")[希臘王國國王](../Page/希腊君主列表.md "wikilink")。
  - [1840年](../Page/1840年.md "wikilink")：[毛利人和英国政府签订](../Page/毛利人.md "wikilink")《[怀唐伊条约](../Page/怀唐伊条约.md "wikilink")》，[新西兰成为英国殖民地](../Page/新西兰.md "wikilink")。

### 20世紀

  - [1908年](../Page/1908年.md "wikilink")：[中國](../Page/中國.md "wikilink")[上海首次试行](../Page/上海.md "wikilink")[有轨电车](../Page/有轨电车.md "wikilink")\[1\]。
  - [1919年](../Page/1919年.md "wikilink")：德国[-{zh-hant:威瑪共和國;
    zh-hans:魏玛共和国;}-建立](../Page/威瑪共和國.md "wikilink")。
  - [1922年](../Page/1922年.md "wikilink")：[美国](../Page/美国.md "wikilink")、[英国](../Page/英国.md "wikilink")、[法国](../Page/法国.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[日本五国在](../Page/日本.md "wikilink")[华盛顿签订关于限制](../Page/华盛顿哥伦比亚特区.md "wikilink")[海军军备的](../Page/海军.md "wikilink")《[华盛顿海军条约](../Page/华盛顿海军条约.md "wikilink")》。
  - [1928年](../Page/1928年.md "wikilink")：[中国共產黨](../Page/中国共產黨.md "wikilink")[黨員](../Page/黨員.md "wikilink")[周文雍和](../Page/周文雍.md "wikilink")[陈铁军在](../Page/陈铁军.md "wikilink")[广州被](../Page/广州.md "wikilink")[中华民国国民政府处以](../Page/中华民国国民政府.md "wikilink")[死刑](../Page/死刑.md "wikilink")，临刑前两人在[刑场举行](../Page/刑场.md "wikilink")[婚礼](../Page/婚礼.md "wikilink")。
  - [1934年](../Page/1934年.md "wikilink")：[法國](../Page/法國.md "wikilink")[巴黎爆發大暴亂](../Page/巴黎.md "wikilink")。
  - [1938年](../Page/1938年.md "wikilink")：[八路军改为第十八集团军](../Page/八路军.md "wikilink")。
  - [1952年](../Page/1952年.md "wikilink")：[英國](../Page/英國.md "wikilink")[女王](../Page/英國君主.md "wikilink")[伊麗莎白二世登基](../Page/伊麗莎白二世.md "wikilink")。至2014年1月底止為世界上在位時間第二長的國家元首，僅次於[1946年即位的](../Page/1946年.md "wikilink")[泰國](../Page/泰國.md "wikilink")[國王](../Page/泰國君主列表.md "wikilink")[蒲美蓬·阿杜德](../Page/蒲美蓬·阿杜德.md "wikilink")。
  - [1956年](../Page/1956年.md "wikilink")：[中華人民共和國國務院發布關於推廣](../Page/中華人民共和國國務院.md "wikilink")[普通話的指示](../Page/普通話.md "wikilink")。
  - [1958年](../Page/1958年.md "wikilink")：载着[曼联足球俱乐部球员的](../Page/曼联足球俱乐部.md "wikilink")[英國航空客機在](../Page/英國航空.md "wikilink")[德國](../Page/德國.md "wikilink")[慕尼黑发生](../Page/慕尼黑.md "wikilink")[空难](../Page/慕尼黑空難.md "wikilink")，包括8名曼聯球員在内的23人遇难。
  - [1959年](../Page/1959年.md "wikilink")：[美国工程師](../Page/美国.md "wikilink")[傑克·基爾比为他发明的第一个](../Page/傑克·基爾比.md "wikilink")[-{zh-tw:積體電路;
    zh-cn:集成电路;
    zh-hk:集成電路;}-申请](../Page/積體電路.md "wikilink")[专利](../Page/专利.md "wikilink")。
  - [1987年](../Page/1987年.md "wikilink")：[谢育新加盟曾经在当时仍属于](../Page/谢育新.md "wikilink")[荷甲的](../Page/荷甲.md "wikilink")[茨瓦鲁队](../Page/茨瓦鲁队.md "wikilink")，成为中国足球运动员转会国外球队的第一人。
  - [1993年](../Page/1993年.md "wikilink")：[比利時決定由中央集權國家改變為聯邦政體國家](../Page/比利時.md "wikilink")。
  - [1998年](../Page/1998年.md "wikilink")：[華盛頓國家機場更名為](../Page/華盛頓國家機場.md "wikilink")[隆納·雷根華盛頓國家機場](../Page/雷根華盛頓國家機場.md "wikilink")。

### 21世紀

  - [2002年](../Page/2002年.md "wikilink")：[香港再次出現](../Page/香港.md "wikilink")[禽流感疫情](../Page/禽流感.md "wikilink")。
  - [2004年](../Page/2004年.md "wikilink")：在[莫斯科發生](../Page/莫斯科.md "wikilink")[地鐵](../Page/莫斯科地鐵.md "wikilink")[自殺爆炸事件](../Page/2004年2月莫斯科地鐵爆炸案.md "wikilink")，41人在此事件喪生，超過100人受傷。[車臣分離主義團體遭受到俄羅斯總統](../Page/車臣共和國.md "wikilink")[普京強烈的譴責](../Page/普京.md "wikilink")，車臣叛軍領導人則否認參與。
  - [2012年](../Page/2012年.md "wikilink")：原[重庆市副市长](../Page/重庆市.md "wikilink")[王立军突然进入](../Page/王立军.md "wikilink")[美国驻成都总领馆](../Page/美国驻成都总领馆.md "wikilink")，只留一天后“自行离开”，被称为[王立军事件](../Page/王立军事件.md "wikilink")。
  - [2016年](../Page/2016年.md "wikilink")：[台灣](../Page/台灣.md "wikilink")[高雄市](../Page/高雄市.md "wikilink")[美濃區當地時間凌晨](../Page/美濃區.md "wikilink")3點57分26.1秒發生芮氏規模6.6的[地震](../Page/2016年高雄美濃地震.md "wikilink")，重創[台南](../Page/台南.md "wikilink")，造成[維冠大樓倒塌](../Page/維冠大樓.md "wikilink")，這場地震總共造成117人死亡、551人受傷及多處建築物損毀。
  - [2018年](../Page/2018年.md "wikilink")：因美股暴瀉引發亞太區股市骨牌式暴跌，[香港](../Page/恒生指數.md "wikilink")、[台灣](../Page/加權指數.md "wikilink")、[日本股市收市急跌約](../Page/日經平均指數.md "wikilink")5%；[滬](../Page/上證綜合指數.md "wikilink")[深股市收市亦急跌約](../Page/深圳證券交易所成份股價指數.md "wikilink")3至4%。
  - [2018年](../Page/2018年.md "wikilink")：[台灣](../Page/台灣.md "wikilink")[花蓮縣近海當地時間](../Page/花蓮縣.md "wikilink")23點50分42.6秒發生芮氏規模6.0的[地震](../Page/2018年花蓮地震.md "wikilink")，造成17人死亡、291人受傷。

## 出生

  - [885年](../Page/885年.md "wikilink")：[醍醐天皇](../Page/醍醐天皇.md "wikilink")，[日本第](../Page/日本.md "wikilink")60代[天皇](../Page/天皇.md "wikilink")（[930年去世](../Page/930年.md "wikilink")）
  - [1465年](../Page/1465年.md "wikilink")：[希皮奧內·德爾·費羅](../Page/希皮奧內·德爾·費羅.md "wikilink")，[義大利](../Page/義大利.md "wikilink")[數學家](../Page/數學家.md "wikilink")（[1526年去世](../Page/1526年.md "wikilink")）
  - [1577年](../Page/1577年.md "wikilink")：[貝亞特麗切·倩契](../Page/貝亞特麗切·倩契.md "wikilink")，[義大利弒父女子](../Page/義大利.md "wikilink")（[1599年去世](../Page/1599年.md "wikilink")）
  - [1611年](../Page/1611年.md "wikilink")：[朱由檢](../Page/朱由檢.md "wikilink")，[明](../Page/明朝.md "wikilink")[崇禎帝](../Page/崇禎帝.md "wikilink")（[1644年去世](../Page/1644年.md "wikilink")）
  - [1664年](../Page/1664年.md "wikilink")：[穆斯塔法二世](../Page/穆斯塔法二世.md "wikilink")，[奧斯曼帝國](../Page/奧斯曼帝國.md "wikilink")[蘇丹](../Page/蘇丹.md "wikilink")（[1703年去世](../Page/1703年.md "wikilink")）
  - [1665年](../Page/1665年.md "wikilink")：[安妮女王](../Page/安妮女王.md "wikilink")，[大不列顛王國女王](../Page/大不列顛王國.md "wikilink")（[1714年逝世](../Page/1714年.md "wikilink")）
  - [1802年](../Page/1802年.md "wikilink")：[查爾斯·惠斯通](../Page/查爾斯·惠斯通.md "wikilink")，[英國](../Page/英国.md "wikilink")[維多利亞時代的科學家](../Page/維多利亞時代.md "wikilink")、發明家（[1875年逝世](../Page/1875年.md "wikilink")）
  - [1898年](../Page/1898年.md "wikilink")：[貝比·魯斯](../Page/貝比·魯斯.md "wikilink")，[美國棒球選手](../Page/美國.md "wikilink")（[1948年逝世](../Page/1948年.md "wikilink")）
  - [1911年](../Page/1911年.md "wikilink")：[-{zh-hans:罗纳德·里根;zh-hk:朗奴·列根;zh-tw:隆納·雷根;}-](../Page/罗纳德·里根.md "wikilink")，美國第40任[總統](../Page/美国總統.md "wikilink")（[2004年逝世](../Page/2004年.md "wikilink")）
  - [1945年](../Page/1945年.md "wikilink")：[巴布·馬利](../Page/巴布·馬利.md "wikilink")，[牙买加](../Page/牙买加.md "wikilink")[雷鬼樂歌手](../Page/雷鬼樂.md "wikilink")、作曲家（[1981年逝世](../Page/1981年.md "wikilink")）
  - [1946年](../Page/1946年.md "wikilink")：[张成泽](../Page/张成泽.md "wikilink")，原[朝鲜劳动党中央政治局委员](../Page/朝鲜劳动党.md "wikilink")、朝鲜劳动党中央行政部部长、[朝鲜国防委员会副委员长](../Page/朝鲜国防委员会.md "wikilink")、国家体育指导委员会委员长，拥有[朝鮮人民军大将军衔](../Page/朝鮮人民军.md "wikilink")（[2013年逝世](../Page/2013年.md "wikilink")）
  - [1962年](../Page/1962年.md "wikilink")：[加加美高浩](../Page/加加美高浩.md "wikilink")，[日本](../Page/日本.md "wikilink")[動畫人物設計師](../Page/人物設計師.md "wikilink")
  - [1962年](../Page/1962年.md "wikilink")：[王繪春](../Page/王繪春.md "wikilink")，[中國內地男演員](../Page/中國.md "wikilink")
  - [1964年](../Page/1964年.md "wikilink")：[陳為民](../Page/陳為民.md "wikilink")，[台灣男藝人](../Page/台灣.md "wikilink")
  - [1967年](../Page/1967年.md "wikilink")：[坂井泉水](../Page/坂井泉水.md "wikilink")，[日本女歌手](../Page/日本.md "wikilink")（[2007年逝世](../Page/2007年.md "wikilink")）
  - [1967年](../Page/1967年.md "wikilink")：[蔡一傑](../Page/蔡一傑.md "wikilink")，[香港男歌手](../Page/香港.md "wikilink")
  - [1969年](../Page/1969年.md "wikilink")：[福山雅治](../Page/福山雅治.md "wikilink")，[日本男歌手](../Page/日本.md "wikilink")、演員
  - [1969年](../Page/1969年.md "wikilink")：[喬靖夫](../Page/喬靖夫.md "wikilink")，[香港小說家](../Page/香港.md "wikilink")、填詞人
  - [1970年](../Page/1970年.md "wikilink")：[林姍](../Page/林姍.md "wikilink")，[台灣女歌手](../Page/台灣.md "wikilink")
  - [1974年](../Page/1974年.md "wikilink")：[吉野裕行](../Page/吉野裕行.md "wikilink")，[日本聲優](../Page/日本.md "wikilink")
  - [1975年](../Page/1975年.md "wikilink")：[川瀨智子](../Page/川瀨智子.md "wikilink")，[日本歌手](../Page/日本.md "wikilink")
  - [1976年](../Page/1976年.md "wikilink")：[瑪麗·卡瓦利耶](../Page/瑪麗·卡瓦利耶.md "wikilink")，[丹麥王妃](../Page/丹麥.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[陳忠義](../Page/陳忠義.md "wikilink")，[台灣男歌手](../Page/台灣.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[能登麻美子](../Page/能登麻美子.md "wikilink")，[日本聲優](../Page/日本.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[Tank](../Page/呂建中.md "wikilink")，[台灣男歌手](../Page/台灣.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[梅露絲·畢卡史達夫](../Page/梅露絲·畢卡史達夫.md "wikilink")，[美國時裝設計師](../Page/美國.md "wikilink")、模特兒
  - [1983年](../Page/1983年.md "wikilink")：[胡杨林](../Page/胡楊林.md "wikilink")，[中國女歌手](../Page/中國.md "wikilink")
  - [1985年](../Page/1985年.md "wikilink")：[竹井詩織里](../Page/竹井詩織里.md "wikilink")，[日本女歌手](../Page/日本.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[鄭允浩](../Page/鄭允浩.md "wikilink")，[韓國男歌手](../Page/韓國.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[蔡思貝](../Page/蔡思貝.md "wikilink")，[香港女演員](../Page/香港.md "wikilink")

## 逝世

  - [1181年](../Page/1181年.md "wikilink")：[高倉天皇](../Page/高倉天皇.md "wikilink")，[日本](../Page/日本.md "wikilink")[天皇](../Page/天皇.md "wikilink")（[1161年出生](../Page/1161年.md "wikilink")）
  - [1685年](../Page/1685年.md "wikilink")：[查理二世](../Page/查理二世_\(英國\).md "wikilink")，[蘇格蘭及](../Page/蘇格蘭.md "wikilink")[英格蘭國王](../Page/英格蘭.md "wikilink")（[1630年出生](../Page/1630年.md "wikilink")）
  - [1928年](../Page/1928年.md "wikilink")：[周文雍](../Page/周文雍.md "wikilink")，[中国共產黨](../Page/中国共產黨.md "wikilink")[黨員](../Page/黨員.md "wikilink")，中国革命家
  - 1928年：[陳鐵軍](../Page/陳鐵軍.md "wikilink")，中国共產黨黨員，中国革命家
  - [1952年](../Page/1952年.md "wikilink")：[喬治六世](../Page/喬治六世.md "wikilink")，英國國王（[1895年出生](../Page/1895年.md "wikilink")）
  - [1981年](../Page/1981年.md "wikilink")：[費德莉卡](../Page/費德莉卡_\(希臘王后\).md "wikilink")，[希臘王后](../Page/希臘.md "wikilink")（[1917年出生](../Page/1917年.md "wikilink")）
  - [1983年](../Page/1983年.md "wikilink")：[李梅樹](../Page/李梅樹.md "wikilink")，[台灣著名藝術家](../Page/台灣.md "wikilink")（[1902年出生](../Page/1902年.md "wikilink")）
  - [1992年](../Page/1992年.md "wikilink")：[靚次伯](../Page/靚次伯.md "wikilink")，[省港澳粵劇著名老倌](../Page/省港澳.md "wikilink")（[1905年出生](../Page/1905年.md "wikilink")）
  - [1995年](../Page/1995年.md "wikilink")：[夏衍](../Page/夏衍.md "wikilink")，[中國著名電影藝術家](../Page/中國.md "wikilink")、劇作家、作家、文藝活動家（[1900年出生](../Page/1900年.md "wikilink")）
  - [2014年](../Page/2014年.md "wikilink")：[森徹](../Page/森徹.md "wikilink")，日本的棒球選手（[1935年出生](../Page/1935年.md "wikilink")）
  - [2016年](../Page/2016年.md "wikilink")：[孟庭麗](../Page/孟庭麗.md "wikilink")，[台灣女演員](../Page/台灣.md "wikilink")（[1965年出生](../Page/1965年.md "wikilink")）
  - [2017年](../Page/2017年.md "wikilink")：[陳鑑泉](../Page/陳鑑泉.md "wikilink")，香港工會領袖及前立法局議員（[1925年出生](../Page/1925年.md "wikilink")）
  - [2018年](../Page/2018年.md "wikilink")：[饒宗頤](../Page/饒宗頤.md "wikilink")，[中國](../Page/中國.md "wikilink")[國學大師](../Page/國學.md "wikilink")（[1917年出生](../Page/1917年.md "wikilink")）

## 节假日和习俗

  - ：[国庆日](../Page/国庆日.md "wikilink")（[怀唐伊日](../Page/怀唐伊日.md "wikilink")）

## 參考資料

## 參考資料

1.  [1](http://www.thepaper.cn/baidu.jsp?contid=1335759)，有軌電車：它們並沒有停留在歷史裡。