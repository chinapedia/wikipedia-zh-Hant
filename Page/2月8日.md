**2月8日**是[公历一年中的第](../Page/公历.md "wikilink")39天，离全年结束还有326天（[闰年则还有](../Page/闰年.md "wikilink")327天）。

## 大事记

### 16世紀

  - [1587年](../Page/1587年.md "wikilink")：[苏格兰女王](../Page/苏格兰.md "wikilink")[玛丽一世因被誣陷卷入企图刺杀](../Page/玛丽一世_\(苏格兰\).md "wikilink")[英格兰女王](../Page/英格兰.md "wikilink")[伊丽莎白一世的阴谋中而被](../Page/伊丽莎白一世_\(英格兰\).md "wikilink")[斩首处死](../Page/斩首.md "wikilink")。

### 20世紀

  - [1902年](../Page/1902年.md "wikilink")：[梁启超在](../Page/梁启超.md "wikilink")[日本创办](../Page/日本.md "wikilink")《[新民丛报](../Page/新民丛报.md "wikilink")》。
  - [1904年](../Page/1904年.md "wikilink")：[日本舰队偷袭驻](../Page/大日本帝國.md "wikilink")[中国](../Page/中国.md "wikilink")[旅顺口的](../Page/旅顺口.md "wikilink")[俄国舰队](../Page/俄羅斯帝國.md "wikilink")，[日俄战争爆发](../Page/日俄战争.md "wikilink")。
  - [1915年](../Page/1915年.md "wikilink")：[美國导演](../Page/美國.md "wikilink")[格里菲思执导的电影](../Page/戴维·格里菲思.md "wikilink")《[一个国家的诞生](../Page/一个国家的诞生.md "wikilink")》在[洛杉矶首演](../Page/洛杉矶.md "wikilink")。
  - [1948年](../Page/1948年.md "wikilink")：[朝鮮人民軍成立](../Page/朝鮮人民軍.md "wikilink")。
  - [1970年](../Page/1970年.md "wikilink")：[江炳興](../Page/江炳興.md "wikilink")、[陳良](../Page/陳良.md "wikilink")、[鄭金河](../Page/鄭金河.md "wikilink")、[詹天增和](../Page/詹天增.md "wikilink")[謝東榮等囚犯於](../Page/謝東榮.md "wikilink")[台東泰源監獄起事](../Page/臺東縣.md "wikilink")，是謂[泰源事件](../Page/泰源事件.md "wikilink")。
  - [1974年](../Page/1974年.md "wikilink")：美國[太空實驗室再入地球](../Page/天空實驗室計劃.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[中华人民共和国和](../Page/中华人民共和国.md "wikilink")[葡萄牙正式建立邦交](../Page/葡萄牙.md "wikilink")，並確定[澳門的主權歸還問題](../Page/澳門.md "wikilink")。
  - 1979年：[德尼·萨苏-恩格索正式就任](../Page/德尼·萨苏-恩格索.md "wikilink")[刚果共和国总统](../Page/刚果共和国总统.md "wikilink")。
  - [1986年](../Page/1986年.md "wikilink")：[加拿大](../Page/加拿大.md "wikilink")[艾伯塔发生](../Page/艾伯塔.md "wikilink")[欣頓列車相撞事故](../Page/欣頓列車相撞事故.md "wikilink")，造成23人死亡，71人受伤。

### 21世紀

  - [2002年](../Page/2002年.md "wikilink")：[澳門博彩業經營權由](../Page/澳門博彩業.md "wikilink")[澳門博彩股份有限公司](../Page/澳門博彩股份有限公司.md "wikilink")、[永利及](../Page/永利澳門有限公司.md "wikilink")[銀河奪得](../Page/銀河娛樂場股份有限公司.md "wikilink")，結束了以[何鴻燊為首的單一經營權的局面](../Page/何鴻燊.md "wikilink")
  - [2016年](../Page/2016年.md "wikilink")：[香港因小販管理問題引發一次](../Page/香港.md "wikilink")[嚴重警民衝突](../Page/2016年農曆新年旺角騷亂.md "wikilink")，是日為農曆新年的[年初一](../Page/年初一.md "wikilink")。
  - [2018年](../Page/2018年.md "wikilink")：[美國](../Page/美國.md "wikilink")[道琼斯工业平均指数一周內第二次急挫過千點](../Page/道琼斯工业平均指数.md "wikilink")，收市跌1032點。

## 出生

  - [1700年](../Page/1700年.md "wikilink")：[丹尼尔·伯努利](../Page/丹尼尔·伯努利.md "wikilink")，[瑞士数学家](../Page/瑞士.md "wikilink")（逝於[1782年](../Page/1782年.md "wikilink")）
  - [1805年](../Page/1805年.md "wikilink")：[路易·奥古斯特·布朗基](../Page/路易·奥古斯特·布朗基.md "wikilink")，[法国早期工人运动杰出领袖](../Page/法国.md "wikilink")（逝於[1881年](../Page/1881年.md "wikilink")）
  - [1807年](../Page/1807年.md "wikilink")：[便雅憫·瓦特豪斯·郝金斯](../Page/便雅憫·瓦特豪斯·郝金斯.md "wikilink")，英國藝術家（逝於1894年）
  - [1819年](../Page/1819年.md "wikilink")：[約翰·拉斯金](../Page/約翰·拉斯金.md "wikilink")，[英國作家與藝術評論家](../Page/英國.md "wikilink")（逝於[1900年](../Page/1900年.md "wikilink")）
  - [1828年](../Page/1828年.md "wikilink")：[儒勒·凡尔纳](../Page/儒勒·凡尔纳.md "wikilink")，[法国](../Page/法国.md "wikilink")[科幻小說家](../Page/科幻小說.md "wikilink")。（逝於[1905年](../Page/1905年.md "wikilink")）
  - [1834年](../Page/1834年.md "wikilink")：[季米特里·门捷列夫](../Page/季米特里·门捷列夫.md "wikilink")，[俄國化學家](../Page/俄國.md "wikilink")，[元素週期表的發現者](../Page/元素週期表.md "wikilink")（逝於[1907年](../Page/1907年.md "wikilink")）
  - [1876年](../Page/1876年.md "wikilink")：[保拉·莫德索恩-贝克尔](../Page/保拉·莫德索恩-贝克尔.md "wikilink")，[德国画家](../Page/德国.md "wikilink")，早期表现主义的重要代表人物之一（逝於[1907年](../Page/1907年.md "wikilink")）
  - [1902年](../Page/1902年.md "wikilink")：[德王](../Page/德王.md "wikilink")，[內蒙古王公](../Page/內蒙古.md "wikilink")（逝於[1966年](../Page/1966年.md "wikilink")）
  - [1903年](../Page/1903年.md "wikilink")：[東姑阿都拉曼](../Page/東姑阿都拉曼.md "wikilink")，[马来西亚的国父](../Page/马来西亚.md "wikilink")（逝於[1990年](../Page/1990年.md "wikilink")[12月6日](../Page/12月6日.md "wikilink")）
  - [1921年](../Page/1921年.md "wikilink")：[拉娜·特纳](../Page/拉娜·特纳.md "wikilink")，美國電影演員（逝於[1995年](../Page/1995年.md "wikilink")）
  - [1924年](../Page/1924年.md "wikilink")：[坎代·西潘敦](../Page/坎代·西潘敦.md "wikilink")，[寮國政治家](../Page/寮國.md "wikilink")，曾任[老撾國家主席](../Page/老撾國家主席.md "wikilink")
  - [1931年](../Page/1931年.md "wikilink")：[詹姆斯·狄恩](../Page/占士·甸.md "wikilink")，美國電影演員（逝於[1955年](../Page/1955年.md "wikilink")）
  - [1932年](../Page/1932年.md "wikilink")：[約翰·威廉斯](../Page/約翰·威廉斯_\(作曲家\).md "wikilink")，[美國電影](../Page/美國.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")、[指揮家](../Page/指揮家.md "wikilink")
  - [1940年](../Page/1940年.md "wikilink")：[盛竹如](../Page/盛竹如.md "wikilink")，[台灣電視主持人](../Page/台灣.md "wikilink")，经理人
  - [1948年](../Page/1948年.md "wikilink")：[許仕仁](../Page/許仕仁.md "wikilink")，香港政界人物、前[政務司司長](../Page/政務司司長.md "wikilink")
  - [1952年](../Page/1952年.md "wikilink")：[苗可秀](../Page/苗可秀.md "wikilink")，香港女演员
  - [1960年](../Page/1960年.md "wikilink")：[貝尼格諾·艾奎諾](../Page/阿基諾三世.md "wikilink")，[菲律賓總統](../Page/菲律賓.md "wikilink")
  - [1962年](../Page/1962年.md "wikilink")：[辛曉琪](../Page/辛曉琪.md "wikilink")，台灣[歌手](../Page/歌手.md "wikilink")
  - [1965年](../Page/1965年.md "wikilink")：[張衛健](../Page/張衛健.md "wikilink")，[香港男藝人](../Page/香港.md "wikilink")
  - [1969年](../Page/1969年.md "wikilink")：[五十嵐寒月](../Page/CLAMP.md "wikilink")，[日本漫畫家](../Page/日本.md "wikilink")
  - [1973年](../Page/1973年.md "wikilink")：[馬念先](../Page/馬念先.md "wikilink")，台灣演員
  - [1976年](../Page/1976年.md "wikilink")：[劉嘉鴻](../Page/劉嘉鴻.md "wikilink")，香港政治人物
  - [1979年](../Page/1979年.md "wikilink")：[邓超](../Page/邓超.md "wikilink")，[中國演员](../Page/中华人民共和国.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[楊威](../Page/楊威.md "wikilink")，中國[體操運動員](../Page/競技體操.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[洪巧玲](../Page/洪巧玲.md "wikilink")，[菲律賓選美皇后](../Page/菲律賓.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[池周娟](../Page/池周娟.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[文太裕](../Page/文太裕.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")
  - [1985年](../Page/1985年.md "wikilink")：[王宇佐](../Page/王宇佐.md "wikilink")，台灣[網球運動員](../Page/網球.md "wikilink")
  - 1985年：[張永政](../Page/張永政.md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[曾治豪](../Page/曾治豪.md "wikilink")，[台灣男歌手](../Page/台灣.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[佐佐木希](../Page/佐佐木希.md "wikilink")，[日本](../Page/日本.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[克雷·湯普森](../Page/克雷·湯普森.md "wikilink")，[美國](../Page/美國.md "wikilink")[NBA籃球員](../Page/NBA.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[楊駿文](../Page/楊駿文.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[白珍熙](../Page/白珍熙.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[南優鉉](../Page/南優鉉.md "wikilink")，[韓國男子偶像團體](../Page/韓國.md "wikilink")[INFINITE成員](../Page/INFINITE.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[戴萌](../Page/戴萌.md "wikilink")，[中國女子偶像團體](../Page/中國.md "wikilink")[SNH48成員](../Page/SNH48.md "wikilink")
  - [1994年](../Page/1994年.md "wikilink")：[高山一實](../Page/高山一實.md "wikilink")，[日本女子偶像團體](../Page/日本.md "wikilink")[乃木坂46成員](../Page/乃木坂46.md "wikilink")
  - [1995年](../Page/1995年.md "wikilink")：[宋允亨](../Page/宋允亨.md "wikilink")，[韓國男子偶像團體](../Page/韓國.md "wikilink")[iKON成員](../Page/iKON.md "wikilink")

## 逝世

  - [1265年](../Page/1265年.md "wikilink")：[旭烈兀](../Page/旭烈兀.md "wikilink")，[蒙古的西南亚征服者](../Page/蒙古.md "wikilink")，[伊儿汗国的建立者](../Page/伊儿汗国.md "wikilink")（[1217年出生](../Page/1217年.md "wikilink")）
  - [1302年](../Page/1302年.md "wikilink")：[甘麻剌](../Page/甘麻剌.md "wikilink")，[元朝](../Page/元朝.md "wikilink")[晋王](../Page/晋王.md "wikilink")，[元世祖之孙](../Page/元世祖.md "wikilink")，[真金太子之子](../Page/真金.md "wikilink")，[1324年](../Page/1324年.md "wikilink")1月，[元泰定帝追尊甘麻剌为](../Page/元泰定帝.md "wikilink")[元显宗](../Page/元显宗.md "wikilink")。（[1263年出生](../Page/1263年.md "wikilink")）
  - [1587年](../Page/1587年.md "wikilink")：[瑪麗一世](../Page/瑪麗一世_\(蘇格蘭\).md "wikilink")，[蘇格蘭女王](../Page/蘇格蘭.md "wikilink")（[1542年出生](../Page/1542年.md "wikilink")）
  - [1725年](../Page/1725年.md "wikilink")：[彼得大帝](../Page/彼得大帝.md "wikilink")，[俄羅斯沙皇](../Page/俄羅斯沙皇.md "wikilink")（[1672年出生](../Page/1672年.md "wikilink")）
  - [1921年](../Page/1921年.md "wikilink")：[克魯泡特金](../Page/克魯泡特金.md "wikilink")，[蘇聯無政府主義理論家](../Page/蘇聯.md "wikilink")（[1842年出生](../Page/1842年.md "wikilink")）
  - [1922年](../Page/1922年.md "wikilink")：[樺山資紀](../Page/樺山資紀.md "wikilink")，[台灣日治時期第一任](../Page/台灣日治時期.md "wikilink")[總督](../Page/台灣總督.md "wikilink")（[1837年出生](../Page/1837年.md "wikilink")）
  - [1957年](../Page/1957年.md "wikilink")：[馮·諾伊曼](../Page/馮·諾伊曼.md "wikilink")，[匈牙利裔](../Page/匈牙利.md "wikilink")[美國數學家](../Page/美國.md "wikilink")，現代[電腦創始人之一](../Page/電腦.md "wikilink")。（[1903年出生](../Page/1903年.md "wikilink")）
  - [1974年](../Page/1974年.md "wikilink")：[弗里茨·兹威基](../Page/弗里茨·兹威基.md "wikilink")，瑞士天文学家（[1898年出生](../Page/1898年.md "wikilink")）
  - [1998年](../Page/1998年.md "wikilink")：[埃諾奇·鮑威爾](../Page/埃諾奇·鮑威爾.md "wikilink")，英國政治家（[1912年出生](../Page/1912年.md "wikilink")）
  - [2002年](../Page/2002年.md "wikilink")：[王鼎昌](../Page/王鼎昌.md "wikilink")，[新加坡首任民選](../Page/新加坡.md "wikilink")[總統](../Page/新加坡總統.md "wikilink")（[1936年出生](../Page/1936年.md "wikilink")）
  - [2003年](../Page/2003年.md "wikilink")：[杏林子](../Page/杏林子.md "wikilink")，原名劉俠，[台灣殘障作家](../Page/台灣.md "wikilink")（[1942年出生](../Page/1942年.md "wikilink")）
  - [2006年](../Page/2006年.md "wikilink")：[朱匯森](../Page/朱匯森.md "wikilink")，[中華民國教育人士](../Page/中華民國.md "wikilink")（[1911年出生](../Page/1911年.md "wikilink")）
  - [2007年](../Page/2007年.md "wikilink")：[楊啟彥](../Page/楊啟彥.md "wikilink")，[香港政府前高官](../Page/港英政府.md "wikilink")，[九廣鐵路公司前總裁](../Page/九廣鐵路公司.md "wikilink")（[1941年出生](../Page/1941年.md "wikilink")）
  - [2014年](../Page/2014年.md "wikilink")：[麦孔·佩雷拉·德·奥利维拉](../Page/麦孔·佩雷拉·德·奥利维拉.md "wikilink")，[巴西足球員](../Page/巴西.md "wikilink")（[1988年出生](../Page/1988年.md "wikilink")）
  - [2017年](../Page/2017年.md "wikilink")：[松野莉奈](../Page/松野莉奈.md "wikilink")，[日本女子偶像團體](../Page/日本.md "wikilink")「[私立惠比壽中學](../Page/私立惠比壽中學.md "wikilink")」成員（[1998年出生](../Page/1998年.md "wikilink")）

## 节日

[建军节](../Page/建军节.md "wikilink")（）：朝鲜人民军于1948年2月8日组建成立。[朝鲜劳动党中央委员会政治局在](../Page/朝鲜劳动党中央委员会政治局.md "wikilink")2018年宣布将2月8日定为建军节。\[1\]

## 參考資料

1.