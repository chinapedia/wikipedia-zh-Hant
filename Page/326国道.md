**326国道**（或“国道326线”、“G326线”）是在[中国的一条](../Page/中国.md "wikilink")[国道](../Page/中国国道.md "wikilink")，起点为[重庆](../Page/重庆.md "wikilink")[秀山](../Page/秀山.md "wikilink")，终点为[云南](../Page/云南.md "wikilink")[河口的国道](../Page/河口.md "wikilink")，全程1562千米。

这条国道经过[重庆](../Page/重庆.md "wikilink")、[贵州和](../Page/贵州.md "wikilink")[云南](../Page/云南.md "wikilink")3个省份。

<table>
<thead>
<tr class="header">
<th><p>城市名</p></th>
<th><p>距起点距离<br />
（<a href="../Page/公里.md" title="wikilink">公里</a>）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/重庆市.md" title="wikilink">重庆市</a><a href="../Page/秀山土家族苗族自治县.md" title="wikilink">秀山土家族苗族自治县</a></p></td>
<td><p>0</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/贵州省.md" title="wikilink">贵州省</a><a href="../Page/沿河土家族自治县.md" title="wikilink">沿河土家族自治县</a></p></td>
<td><p>97</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/贵州省.md" title="wikilink">贵州省</a><a href="../Page/德江县.md" title="wikilink">德江县</a></p></td>
<td><p>186</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/贵州省.md" title="wikilink">贵州省</a><a href="../Page/凤冈县.md" title="wikilink">凤冈县</a></p></td>
<td><p>254</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/贵州省.md" title="wikilink">贵州省</a><a href="../Page/湄潭县.md" title="wikilink">湄潭县</a></p></td>
<td><p>294</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/贵州省.md" title="wikilink">贵州省</a><a href="../Page/遵义市.md" title="wikilink">遵义市</a></p></td>
<td><p>373</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/贵州省.md" title="wikilink">贵州省</a><a href="../Page/金沙县.md" title="wikilink">金沙县</a></p></td>
<td><p>463</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/贵州省.md" title="wikilink">贵州省</a><a href="../Page/大方县.md" title="wikilink">大方县</a></p></td>
<td><p>572</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/贵州省.md" title="wikilink">贵州省</a><a href="../Page/毕节市.md" title="wikilink">毕节市</a></p></td>
<td><p>622</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/贵州省.md" title="wikilink">贵州省</a><a href="../Page/赫章县.md" title="wikilink">赫章县</a></p></td>
<td><p>716</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/贵州省.md" title="wikilink">贵州省</a><a href="../Page/威宁彝族回族苗族自治县.md" title="wikilink">威宁彝族回族苗族自治县</a></p></td>
<td><p>791</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/云南省.md" title="wikilink">云南省</a><a href="../Page/宣威市.md" title="wikilink">宣威市</a></p></td>
<td><p>963</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/云南省.md" title="wikilink">云南省</a><a href="../Page/沾益县.md" title="wikilink">沾益县</a></p></td>
<td><p>1053</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/云南省.md" title="wikilink">云南省</a><a href="../Page/曲靖市.md" title="wikilink">曲靖市</a></p></td>
<td><p>1066</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/云南省.md" title="wikilink">云南省</a><a href="../Page/陆良县.md" title="wikilink">陆良县</a></p></td>
<td><p>1132</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/云南省.md" title="wikilink">云南省</a><a href="../Page/石林彝族自治县.md" title="wikilink">石林彝族自治县</a></p></td>
<td><p>1192</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/云南省.md" title="wikilink">云南省</a><a href="../Page/弥勒县.md" title="wikilink">弥勒县</a></p></td>
<td><p>1248</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/云南省.md" title="wikilink">云南省</a><a href="../Page/开远市.md" title="wikilink">开远市</a></p></td>
<td><p>1337</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/云南省.md" title="wikilink">云南省</a><a href="../Page/蒙自县.md" title="wikilink">蒙自县</a></p></td>
<td><p>1394</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/云南省.md" title="wikilink">云南省</a><a href="../Page/河口瑶族自治县.md" title="wikilink">河口瑶族自治县</a></p></td>
<td><p>1562</p></td>
</tr>
</tbody>
</table>

[Category:中国国道](../Category/中国国道.md "wikilink")
[Category:重庆市公路](../Category/重庆市公路.md "wikilink")
[Category:贵州省公路](../Category/贵州省公路.md "wikilink")
[Category:云南省公路](../Category/云南省公路.md "wikilink")