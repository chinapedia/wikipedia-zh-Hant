**34街-賓州車站**（）是[紐約地鐵](../Page/紐約地鐵.md "wikilink")[IRT百老匯-第七大道線的一個快車](../Page/IRT百老匯-第七大道線.md "wikilink")[地鐵站](../Page/地鐵站.md "wikilink")，位於[曼哈頓](../Page/曼哈頓.md "wikilink")[34街及](../Page/34街_\(曼哈頓\).md "wikilink")[第七大道](../Page/第七大道_\(曼哈頓\).md "wikilink")，設有[1號線](../Page/紐約地鐵1號線.md "wikilink")（任何時候停站）、[2號線](../Page/紐約地鐵2號線.md "wikilink")（任何時候停站）與[3號線](../Page/紐約地鐵3號線.md "wikilink")（任何時候停站（深夜除外））列車服務。可經[賓夕法尼亞車站轉乘](../Page/賓夕法尼亞車站_\(紐約市\).md "wikilink")[長島鐵路](../Page/長島鐵路.md "wikilink")、[新澤西公共交通公司及](../Page/新澤西公共交通公司.md "wikilink")[美鐵](../Page/美鐵.md "wikilink")。

## 車站結構

<table>
<tbody>
<tr class="odd">
<td><p><strong>G</strong></p></td>
<td><p>街道</p></td>
<td><p>出入口<br />
</p></td>
</tr>
<tr class="even">
<td><p><strong>P<br />
月台層</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><small><a href="../Page/側式月台.md" title="wikilink">側式月台</a>，右側開門 </small></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><span style="color:#{{NYCS color|red}}"><strong>北行</strong></span></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-1.svg" title="fig:纽约地铁1号线">纽约地铁1号线</a> 往<a href="../Page/范科特蘭公園-242街車站_(IRT百老匯-第七大道線).md" title="wikilink">范科特蘭公園-242街</a>（深夜時<a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-2.svg" title="fig:纽约地铁2号线">纽约地铁2号线</a> 往<a href="../Page/威克菲爾德-241街車站_(IRT白原路線).md" title="wikilink">241街</a>）<small>（<a href="../Page/時報廣場-42街車站_(IRT百老匯-第七大道線).md" title="wikilink">時報廣場-42街</a>）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><span style="color:#{{NYCS color|red}}"><strong>北行快速</strong></span></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-2.svg" title="fig:纽约地铁2号线">纽约地铁2号线</a> 往<a href="../Page/威克菲爾德-241街車站_(IRT白原路線).md" title="wikilink">威克菲爾德-241街</a><small>（<a href="../Page/96街車站_(IRT百老匯-第七大道線).md" title="wikilink">96街</a>）</small><br />
 <a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-3.svg" title="fig:纽约地铁3号线">纽约地铁3号线</a> 往<a href="../Page/哈萊姆-148街車站_(IRT萊諾克斯大道線).md" title="wikilink">哈萊姆-148街</a><small>（時報廣場-42街）</small></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><small><a href="../Page/島式月台.md" title="wikilink">島式月台</a>，左側開門 </small></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><span style="color:#{{NYCS color|red}}"><strong>南行快速</strong></span></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-2.svg" title="fig:纽约地铁2号线">纽约地铁2号线</a> 往<a href="../Page/夫拉特布殊大道-布魯克林學院車站_(IRT諾斯特蘭大道線).md" title="wikilink">夫拉特布殊大道-布魯克林學院</a><small>（<a href="../Page/時報廣場-42街車站_(IRT百老匯-第七大道線).md" title="wikilink">時報廣場-42街</a>）</small><br />
 <a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-3.svg" title="fig:纽约地铁3号线">纽约地铁3号线</a> 往<a href="../Page/新地段大道車站_(IRT新地段線).md" title="wikilink">新地段大道</a><small>（<a href="../Page/14街車站_(IRT百老匯-第七大道線).md" title="wikilink">14街</a>）</small></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><span style="color:#{{NYCS color|red}}"><strong>南行</strong></span></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-1.svg" title="fig:纽约地铁1号线">纽约地铁1号线</a> 往<a href="../Page/南碼頭車站_(IRT百老匯-第七大道線).md" title="wikilink">南碼頭</a>（深夜時<a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-2.svg" title="fig:纽约地铁2号线">纽约地铁2号线</a> 往<a href="../Page/夫拉特布殊大道-布魯克林學院車站_(IRT諾斯特蘭大道線).md" title="wikilink">夫拉特布殊大道-布魯克林學院</a>）<small>（<a href="../Page/28街車站_(IRT百老匯-第七大道線).md" title="wikilink">28街</a>）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><small><a href="../Page/側式月台.md" title="wikilink">側式月台</a>，右側開門 </small></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>M</strong></p></td>
<td><p>夾層</p></td>
<td><p>月台連接層</p></td>
</tr>
</tbody>
</table>

與[IND第八大道線](../Page/IND第八大道線.md "wikilink")[34街-賓州車站和](../Page/34街-賓州車站_\(IND第八大道線\).md "wikilink")[IRT東公園道線](../Page/IRT東公園道線.md "wikilink")[大西洋大道-巴克萊中心車站相同](../Page/大西洋大道-巴克萊中心車站.md "wikilink")，此站設有兩個服務慢車的[側式月台和快車的](../Page/側式月台.md "wikilink")[島式月台](../Page/島式月台.md "wikilink")。這是因為預計乘客量的增長和鼓勵乘客在北行下一站[時報廣場-42街車站轉乘](../Page/時報廣場-42街車站_\(IRT百老匯-第七大道線\).md "wikilink")，其設計為一般為[跨月台轉乘的一般島式月台](../Page/跨月台轉乘.md "wikilink")\[1\]。

此站與位於IND第八大道線的同名車站沒有免費轉乘，雖然兩者皆連接至賓州車站。最近可轉乘的位置是時報廣場-42街車站，可免費轉乘至[42街-航港局客運總站車站](../Page/42街-航港局客運總站車站_\(IND第八大道線\).md "wikilink")\[2\]。

## 參考資料

## 外部連結

  -
  - nycsubway.org – [When the Animals Speak Artwork by Elizabeth
    Grajales (1998)](http://www.nycsubway.org/perl/artwork_show?42)

  - nycsubway.org – [A Bird's Life Artwork by Elizabeth Grajales
    (1997)](http://www.nycsubway.org/perl/artwork_show?188)

  - Station Reporter – [1
    Train](https://web.archive.org/web/20060924173239/http://www.stationreporter.net/1train.htm)

  - [34th Street entrance from Google Maps Street
    View](https://maps.google.com/maps?ie=UTF8&ll=40.751004,-73.990613&spn=0.00382,0.013433&z=17&layer=c&cbll=40.751016,-73.99064&panoid=Y2X89P924U4rav9Pw2REtA&cbp=12,87.27,,0,2.24)

  - [33rd Street entrance from Google Maps Street
    View](https://maps.google.com/maps?ie=UTF8&ll=40.749947,-73.991225&spn=0.00382,0.013433&z=17&layer=c&cbll=40.750321,-73.991181&panoid=V7zzTJlGaklNrDnjFNfb9A&cbp=12,343.53,,0,1.05)

  - [Platforms from Google Maps Street
    View](http://www.google.com/maps/@40.7511675,-73.99028,3a,75y,116.83h,91.8t/data=!3m8!1e1!3m6!1s-GDXHSRNR9Bc%2FV42i3zQ5jKI%2FAAAAAAAAKyU%2FQh7PN7yu1s043j9j8uS7pNXe6N4fekVOQCLIB!2e4!3e11!6s%2F%2Flh4.googleusercontent.com%2F-GDXHSRNR9Bc%2FV42i3zQ5jKI%2FAAAAAAAAKyU%2FQh7PN7yu1s043j9j8uS7pNXe6N4fekVOQCLIB%2Fw203-h100-p-k-no%2F!7i9728!8i4864!4m3!8m2!3m1!1e1!6m1!1e1)

[Category:IRT百老匯-第七大道線車站](../Category/IRT百老匯-第七大道線車站.md "wikilink")
[Category:1917年啟用的鐵路車站](../Category/1917年啟用的鐵路車站.md "wikilink")
[Category:曼哈頓紐約地鐵車站](../Category/曼哈頓紐約地鐵車站.md "wikilink")
[Category:1917年紐約州建立](../Category/1917年紐約州建立.md "wikilink")

1.

2.