**3DO公司**（）（前[NASDAQ](../Page/NASDAQ.md "wikilink")：THDO），由[美商藝電](../Page/美商藝電.md "wikilink")（Electronic
Arts）的创建者[特里普·霍金斯](../Page/特里普·霍金斯.md "wikilink")（Trip
Hawkins）于1991年以**SMSG, Inc.**（San Mateo Software
Games的缩写）的名称建立。一年后，SMSG改名为3DO，并推出了新一代游戏平台——[3DO游戏机](../Page/3DO游戏机.md "wikilink")，可惜未能坚持下来，最终不得不从硬件业全线撤退，转而成为一家纯粹的软件开发商和发行商，为此付出了惨痛的代价。3DO公司这几年一直致力于电脑游戏业，其代表作包括[第一人称](../Page/第一人称.md "wikilink")[角色扮演类游戏](../Page/角色扮演类.md "wikilink")[魔法门系列](../Page/魔法门.md "wikilink")、[第三人称](../Page/第三人称.md "wikilink")[回合制策略类游戏](../Page/回合制策略类.md "wikilink")[魔法门之英雄无敌系列和](../Page/魔法门之英雄无敌.md "wikilink")[玩具军人](../Page/玩具军人.md "wikilink")（）系列。

2003年5月，3DO公司正式[破产](../Page/破产.md "wikilink")。魔法门及英雄无敌系列游戏的版权被卖给了[育碧公司](../Page/育碧公司.md "wikilink")，其他一些版权卖给了[微软](../Page/微软.md "wikilink")、[Namco等公司](../Page/Namco.md "wikilink")。

[de:3DO\#The 3DO Company](../Page/de:3DO#The_3DO_Company.md "wikilink")

[Category:美國電子遊戲公司](../Category/美國電子遊戲公司.md "wikilink")
[Category:1991年開業電子遊戲公司](../Category/1991年開業電子遊戲公司.md "wikilink")
[Category:2002年結業電子遊戲公司](../Category/2002年結業電子遊戲公司.md "wikilink")
[Category:软件公司](../Category/软件公司.md "wikilink")