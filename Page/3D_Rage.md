**3D
Rage**是一个顯示卡系列，由[ATI推出](../Page/ATI.md "wikilink")，在1996年1月正式发布。第一款Rage系列显卡是3D
Rage (I)，但兼容度差，所以一般人都認為3D Rage
II+DVD才是ATI第一款真正的3D图形处理器。它支持硬件[Z-缓冲](../Page/Z-缓冲.md "wikilink")、纹理压缩、线性过滤和纹理混合，像素填充率是10M
Pixels/s。与之前的3D Rage和3D Rage II不同，3D Rage
II+DVD支援硬件动态补偿。硬件动态补偿可以協助CPU播放DVD。

## 產品列表

### 3D Rage (I)

3D Rage
(I)是ATI第一款3D显示核心，它於1996年1月推出，用於ATI的*Xpression*影像卡（前一代影像卡用上Mach64晶片）。3D
Rage (I)的核心是建基於Mach64
2D核心，並加上3D功能，以0.5微米的製程技術生產。它擁有1条像素流水線和1个顶点着色单元，能处理光源，支援[MPEG-1硬體加速](../Page/MPEG-1.md "wikilink")。但它的兼容度比較差，销情並不太好。

### 3D Rage II (IIC, II+, II+DVD)

[3drageii+dvd.jpg](https://zh.wikipedia.org/wiki/File:3drageii+dvd.jpg "fig:3drageii+dvd.jpg")
[ATIXpressionRageII.png](https://zh.wikipedia.org/wiki/File:ATIXpressionRageII.png "fig:ATIXpressionRageII.png")
[RageIIC.png](https://zh.wikipedia.org/wiki/File:RageIIC.png "fig:RageIIC.png")

第二代的Rage解決了兼容度問題，並帶來了兩倍的3D性能。該繪圖處理器是建基於重新設計的Mach64
GUI引擎，優化了2D性能，採用了單循環[EDO記憶體和高速的](../Page/DRAM#Extended_Data_Out_.28EDO.29_DRAM.md "wikilink")[SGRAM](../Page/SGRAM.md "wikilink")。3D
RAGE II晶片是一個增強，是3D
RAGE的座腳相容加速版。而第二代的PCI總線令它的2D效能增加了20%，亦新增了[MPEG-2](../Page/MPEG-2.md "wikilink")
(DVD)播放功能。這款晶片有支援[微軟](../Page/微軟.md "wikilink")[Direct3D](../Page/Direct3D.md "wikilink")、[Reality
Lab](../Page/Reality_Lab.md "wikilink")、[QuickDraw](../Page/QuickDraw.md "wikilink")
3D Rave、Criterion
[RenderWare和Argonaut](../Page/RenderWare.md "wikilink")
[BRender的驅動程式](../Page/BRender.md "wikilink")。專業的3D和[CAD用家可得到](../Page/CAD.md "wikilink")[OpenGL驅動程式](../Page/OpenGL.md "wikilink")，[AutoCAD用家可得到Heidi驅動程式](../Page/AutoCAD.md "wikilink")。驅動程式亦支援數個作業系統，包括[Windows
95](../Page/Windows_95.md "wikilink")、[Windows
NT](../Page/Windows_NT.md "wikilink")、[Mac
OS和](../Page/Mac_OS.md "wikilink")[OS/2](../Page/OS/2.md "wikilink")。ATI亦裝運了RAGE
II的*ImpacTV*輔助晶片。這是一顆電視編碼晶片。

[蘋果電腦的](../Page/蘋果電腦.md "wikilink")[Macintosh
G3和Power](../Page/Power_Macintosh_G3.md "wikilink") Mac 6500電腦都採用了Rage
II顯示晶片。一些個人電腦的主機版亦整合了該晶片。ATI的*3D Xpression+*、*3D Pro
Turbo*和原裝的*All-in-Wonder*顯示卡都採用Rage II晶片。

### 3D Rage Pro

[3dragepro.jpg](https://zh.wikipedia.org/wiki/File:3dragepro.jpg "fig:3dragepro.jpg")
ATI為這款晶片新加了[三角形設定引擎](../Page/三角形設定引擎.md "wikilink")；亦改善了[透視法修正](../Page/透視法修正.md "wikilink")，[距離模糊和透明化效果](../Page/距離模糊.md "wikilink")。新增了[反射高光技術和增強了影像播放功能](../Page/反射高光.md "wikilink")，尤其是[DVD播放](../Page/MPEG2.md "wikilink")。3D
Rage
Pro晶片是原生支援Intel的[AGP接口](../Page/AGP.md "wikilink")，有利於材質執行模式、指揮流水線、邊帶尋址和完全2x-模式協定。3D
Rage
Pro支援最高8MB[SGRAM或](../Page/SGRAM.md "wikilink")16MB[WRAM](../Page/WRAM.md "wikilink")，取決於針對的市場。

Rage Pro的效能與[NVIDIA的](../Page/NVIDIA.md "wikilink")[RIVA
128和](../Page/RIVA_128.md "wikilink")[3DFX的](../Page/3DFX.md "wikilink")[Voodoo加速器相若](../Page/Voodoo.md "wikilink")，但就不能勝過它們。原因就是缺乏對[OpenGL支援](../Page/OpenGL.md "wikilink")，難以成為一個堅固的遊戲解決方案。1998年2月，ATI企圖以新瓶舊酒的方式，重新發佈Rage
Pro顯示卡。他們將Rage Pro易名為"Rage Pro
Turbo"，並配合新的驅動程式（4.10.2312版本），希望效能有40%的增長。實際上，這個驅動程式只對[標準檢查程式有效](../Page/標準檢查程式.md "wikilink")，例如[Ziff-Davis的](../Page/Ziff-Davis.md "wikilink")[3D
Winbench](../Page/3D_Winbench.md "wikilink") 98和[Final
Reality](../Page/Final_Reality.md "wikilink")。到真正的遊戲程式時，效能實際上減少了。

3D Rage Pro主要售賣成Xpert@Work或Xpert@Play，他們的主要分別是Xpert@Play版本擁有TV-out接口。

[Category:顯示卡](../Category/顯示卡.md "wikilink")
[Category:冶天科技](../Category/冶天科技.md "wikilink")