**第三代合作伙伴计划**（，即）是一个成立于1998年12月的标准化机构。目前其成员包括[欧洲的](../Page/欧洲.md "wikilink")[ETSI](../Page/欧洲电信标准协会.md "wikilink")、[日本的](../Page/日本.md "wikilink")[ARIB和](../Page/电波产业会.md "wikilink")[TTC](../Page/情报通信技术委员会.md "wikilink")、[中国的](../Page/中国.md "wikilink")[CCSA](../Page/中国通信标准化协会.md "wikilink")、[韩国的](../Page/韩国.md "wikilink")[TTA](../Page/韩国情报通信技术协会.md "wikilink")、[北美洲的](../Page/北美洲.md "wikilink")[ATIS和](../Page/电信工业解决方案联盟.md "wikilink")[印度的](../Page/印度.md "wikilink")[电信标准开发协会](../Page/TSDSI.md "wikilink")。

3GPP的目标是在[国际电信联盟的](../Page/国际电信联盟.md "wikilink")[IMT-2000计划范围内制订和实现全球性的](../Page/3G.md "wikilink")（[第三代](../Page/第三代移动通信系统.md "wikilink")）[移动电话系统规范](../Page/移动电话.md "wikilink")。它致力于[GSM到](../Page/全球移动通信系统.md "wikilink")[UMTS](../Page/UMTS.md "wikilink")（[W-CDMA](../Page/W-CDMA.md "wikilink")）的演化，虽然GSM到W-CDMA[空中介面差别很大](../Page/空中介面.md "wikilink")，但是其核心网采用了[GPRS的框架](../Page/通用分组无线服务.md "wikilink")，因此仍然保持一定的延续性。

3GPP和[3GPP2两者实际上存在一定竞争关系](../Page/3GPP2.md "wikilink")，3GPP2致力于以[IS-95](../Page/CdmaOne.md "wikilink")（在北美和韩国应用广泛的CDMA标准，[中国电信CDMA与之兼容](../Page/中国电信.md "wikilink")）向[IS-2000过渡](../Page/CDMA2000.md "wikilink")，和高通公司关系更加紧密。

## 标准

3GPP的标准是由诸多“”构成的，因此3GPP的讨论频繁地涉及各个Release的功能。

| 版本\[1\]          | 发行时间\[2\]   | 信息                                                                                                                                                                                                                                                                                                                       |
| ---------------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| **Phase 1**      | 1992        | GSM特征                                                                                                                                                                                                                                                                                                                    |
| **Phase 2**      | 1995        | GSM特征、EFR解码器                                                                                                                                                                                                                                                                                                             |
| **Release 96**   | 1997 Q1     | GSM特征、14.4 kbit/s用户数据速率                                                                                                                                                                                                                                                                                                  |
| **Release 97**   | 1998 Q1     | GSM特征、GPRS                                                                                                                                                                                                                                                                                                               |
| **Release 98**   | 1999 Q1     | GSM特征、AMR、EDGE、PCS1900用GPRS                                                                                                                                                                                                                                                                                              |
| **Release 99**   | 2000 Q1     | 指定了第一个[UMTS](../Page/UMTS.md "wikilink") [3G网络](../Page/3G.md "wikilink")，集成了[CDMA](../Page/码分多址.md "wikilink")[空中接口](../Page/空中介面.md "wikilink")\[3\]                                                                                                                                                                   |
| '''Release 4 ''' | 2001 Q2     | 原称**Release 2000**——新增[all-IP核心网络](../Page/下一代网络.md "wikilink")\[4\]                                                                                                                                                                                                                                                     |
| '''Release 5 ''' | 2002 Q1     | 提出了[IMS与](../Page/IP多媒体子系统.md "wikilink")[HSDPA](../Page/HSDPA.md "wikilink")\[5\]                                                                                                                                                                                                                                       |
| '''Release 6 ''' | 2004 Q4     | 与[无线局域网集成](../Page/无线局域网.md "wikilink")，并增加了[HSUPA](../Page/HSUPA.md "wikilink")、[MBMS](../Page/多媒体广播多播服务.md "wikilink")、对[IMS的增强如](../Page/IP多媒体子系统.md "wikilink")[手機對講服務](../Page/手機對講服務.md "wikilink")（PoC）、[GAN](../Page/通用接入网络.md "wikilink")\[6\]                                                                  |
| '''Release 7 ''' | 2007 Q4     | 侧重于降低延迟，对[服务质量与实时应用的改善如](../Page/QoS.md "wikilink")[VoIP](../Page/IP电话.md "wikilink")。\[7\]规范同时侧重于[HSPA+](../Page/HSPA+.md "wikilink")、[SIM卡高速协议与非接触前段接口](../Page/SIM卡.md "wikilink")（允许运营商提供非接触式服务的[近场通讯如](../Page/近场通讯.md "wikilink")[移动支付](../Page/移动支付.md "wikilink")）、[EDGE Evolution](../Page/EDGE.md "wikilink")。 |
| '''Release 8 ''' | 2008 Q4     | 首次发行**[LTE](../Page/长期演进技术.md "wikilink")**。All-IP网络（SAE）。新增[OFDMA](../Page/OFDMA.md "wikilink")、[FDE与基于](../Page/SC-FDMA.md "wikilink")[MIMO的无线接口](../Page/MIMO.md "wikilink")，不向下兼容以前的CDMA接口。[DC-HSDPA](../Page/DC-HSDPA.md "wikilink")。                                                                               |
| '''Release 9 ''' | 2009 Q4     | 对SAES的增强、[WiMAX与LTE](../Page/WiMAX.md "wikilink")/UMTS的互操作性。含[MIMO的](../Page/MIMO.md "wikilink")[DC-HSDPA](../Page/DC-HSDPA.md "wikilink")、[DC-HSUPA](../Page/DC-HSUPA.md "wikilink")。                                                                                                                                   |
| **Release 10**   | 2011 Q1     | 实现了[IMT Advanced](../Page/IMT_Advanced.md "wikilink") [4G要求的](../Page/4G.md "wikilink")**[LTE Advanced](../Page/LTE_Advanced.md "wikilink")**。向下兼容Release 8（LTE）。[MC-HSDPA](../Page/MC-HSDPA.md "wikilink")（四载波）。                                                                                                        |
| **Release 11**   | 2012 Q3     | 高级IP[互连服务](../Page/互连.md "wikilink")。国家运营商以及第三方应用提供商之间的[服务层互连](../Page/服务层.md "wikilink")。                                                                                                                                                                                                                               |
| **Release 12**   | 2015 Q1     | 加強小蜂窝（small cell）及[載波聚合](../Page/載波聚合.md "wikilink")（CA, Carrier Aggregation）等                                                                                                                                                                                                                                           |
| **Release 13**   | 2016 Q1     | [LTE-U](../Page/LTE-U.md "wikilink")（LTE in unlicensed spectrum）、[LTE-A Pro](../Page/LTE-A_Pro.md "wikilink")（LTE Advanced Pro）等                                                                                                                                                                                         |
| **Release 14**   | 2017 Q2     | 内容待定。                                                                                                                                                                                                                                                                                                                    |
| **Release 15**   | 2018 Q2     | 内容待定。                                                                                                                                                                                                                                                                                                                    |
| **Release 16**   | 預定於2019年12月 | 内容待定。                                                                                                                                                                                                                                                                                                                    |

每一次发布包含数百单独的标准文件，每一份文件可能经过了多次改版。目前的3GPP标准包含[GSM标准的最新改版](../Page/全球移动通信系统.md "wikilink")。

这些文档可免费从3GPP的网站获得。虽然3GPP标准对新手来说可能令人眼花缭乱，但是它们非常完整详细，并提供了对手机行业如何运作的深刻见解。它们不但涵盖了无线电的部分（“[空中接口](../Page/空中介面.md "wikilink")”）及核心网络，而且还有收费信息以及深入源代码级别的[语音编码](../Page/语音编码.md "wikilink")。[加密方面也有详细的规定](../Page/密码学.md "wikilink")。[3GPP2也提供关于它的系统的类似信息](../Page/3GPP2.md "wikilink")。

## 参见

  - [3G](../Page/3G.md "wikilink")
  - [3GP](../Page/3GP.md "wikilink")
  - [3GPP2](../Page/3GPP2.md "wikilink")
  - [EDGE](../Page/EDGE.md "wikilink")
  - [FOMA](../Page/FOMA.md "wikilink")
  - [LTE](../Page/长期演进技术.md "wikilink")
  - [OFDMA](../Page/OFDMA.md "wikilink")
  - [SDMA](../Page/SDMA.md "wikilink")

## 参考文献

<references/>

## 外部链接

  - [3GPP Site](http://www.3gpp.org)
  - [3GP.com - 3GP Players and converters](http://www.3gp.com/)
  - [3G-FAQ](http://www.eurotechnology.com/3G/index.html)
  - [Novatics - Conduit3G Technology](http://www.novatics.com/)

[Category:無線通信](../Category/無線通信.md "wikilink")
[Category:移动通信标准](../Category/移动通信标准.md "wikilink")
[Category:3GPP標準](../Category/3GPP標準.md "wikilink")

1.  [3GPP - Releases](http://www.3gpp.org/Releases)

2.
3.  Overview of 3GPP Release 99, Summary of all Release 99 Features.
    ETSI Mobile Competence Centre, Version xx/07/04

4.  Overview of 3GPP Release 4, Summary of all Release 4 Features,
    v.1.1.0 (draft) ETSI Mobile Competence Centre 2004

5.  Summary of all Release 5 Features, ETSI Mobile Competence Centre,
    Version 9th September 2003

6.  Overview of 3GPP Release 6, Summary of all Release 6 Features,
    Version TSG \#33, ETSI Mobile Competence Centre 2006

7.  Review of the Work Plan at Plenaries \#31, 3GPP, SP-060232 3GPP TSG
    SA\#31 Sanya, March 13–16, 2006