**3月10日**是[公历一年中的第](../Page/公历.md "wikilink")69天（[闰年第](../Page/闰年.md "wikilink")70天），离全年的结束还有296天。

## 大事记

### 前3世紀

  - [前241年](../Page/前241年.md "wikilink")：[罗马舰队在](../Page/罗马.md "wikilink")[埃加迪群島附近大败](../Page/埃加迪群島.md "wikilink")[迦太基舰队](../Page/迦太基.md "wikilink")，[第一次布匿战争结束](../Page/第一次布匿战争.md "wikilink")。

### 17世紀

  - [1629年](../Page/1629年.md "wikilink")：[英格兰国王](../Page/英格兰国王.md "wikilink")[查理一世解散](../Page/查理一世.md "wikilink")[议会](../Page/英国议会.md "wikilink")。

### 18世紀

  - [1735年](../Page/1735年.md "wikilink")：[波斯国王](../Page/波斯.md "wikilink")[纳迪尔·沙阿与](../Page/纳迪尔·沙阿.md "wikilink")[俄国沙皇](../Page/俄国沙皇.md "wikilink")[保罗一世签订和约](../Page/保罗一世_\(俄罗斯\).md "wikilink")，[俄罗斯撤离](../Page/俄罗斯.md "wikilink")[巴库](../Page/巴库.md "wikilink")。

### 19世紀

  - [1831年](../Page/1831年.md "wikilink")：[法国国王](../Page/法国国王.md "wikilink")[路易·菲利普为了支持他在](../Page/路易·菲利普.md "wikilink")[阿尔及利亚发动的战争](../Page/阿尔及利亚.md "wikilink")，组建了[法国外籍兵团](../Page/法国外籍兵团.md "wikilink")。
  - [1848年](../Page/1848年.md "wikilink")：[美墨战争结束](../Page/美墨战争.md "wikilink")。
  - [1864年](../Page/1864年.md "wikilink")：[南北战争](../Page/南北战争.md "wikilink")：开始。
  - [1876年](../Page/1876年.md "wikilink")：[美國发明家](../Page/美國.md "wikilink")[亚历山大·格拉汉姆·贝尔用他发明的世界上第一台](../Page/亚历山大·格拉汉姆·贝尔.md "wikilink")[电话和助手进行了首次通话](../Page/电话.md "wikilink")，saying
    "Mr. Watson, come here, I want to see you.."。
  - [1893年](../Page/1893年.md "wikilink")：[科特迪瓦成为](../Page/科特迪瓦.md "wikilink")[法国的](../Page/法国.md "wikilink")[殖民地](../Page/殖民地.md "wikilink")。

### 20世紀

  - [1905年](../Page/1905年.md "wikilink")：[車路士足球會成立](../Page/車路士足球會.md "wikilink")
  - [1912年](../Page/1912年.md "wikilink")：[袁世凯在](../Page/袁世凯.md "wikilink")[北京宣誓就任](../Page/北京.md "wikilink")[中华民国](../Page/中华民国.md "wikilink")[临时大总统](../Page/中華民國大總統.md "wikilink")。
  - [1918年](../Page/1918年.md "wikilink")：[苏俄首都由](../Page/苏俄.md "wikilink")[彼得格勒迁至](../Page/彼得格勒.md "wikilink")[莫斯科](../Page/莫斯科.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：[二战](../Page/二战.md "wikilink")：[美軍](../Page/美軍.md "wikilink")[B-29轟炸機對](../Page/B-29轟炸機.md "wikilink")[日本](../Page/日本.md "wikilink")[東京進行](../Page/東京.md "wikilink")[大轟炸](../Page/東京大轟炸.md "wikilink")。
  - [1952年](../Page/1952年.md "wikilink")：[古巴前](../Page/古巴.md "wikilink")[总统](../Page/古巴总统.md "wikilink")[富尔亨西奥·巴蒂斯塔发动军事](../Page/富尔亨西奥·巴蒂斯塔.md "wikilink")[政变](../Page/政变.md "wikilink")，重新上台执政。
  - [1956年](../Page/1956年.md "wikilink")：。
  - [1959年](../Page/1959年.md "wikilink")：[中华人民共和国](../Page/中华人民共和国.md "wikilink")[西藏地方发生](../Page/西藏地方.md "wikilink")[1959年藏区骚乱](../Page/1959年藏区骚乱.md "wikilink")。事后[中华人民共和国中央人民政府接管西藏](../Page/中华人民共和国中央人民政府.md "wikilink")，成立[西藏自治区人民委员会](../Page/西藏自治区人民政府.md "wikilink")。以[第十四世達賴喇嘛為首的西藏噶厦政府随后流亡](../Page/第十四世達賴喇嘛.md "wikilink")[印度](../Page/印度.md "wikilink")。
  - [1973年](../Page/1973年.md "wikilink")：[文化大革命](../Page/文化大革命.md "wikilink")：[中共中央在](../Page/中共中央.md "wikilink")[毛泽东的指示下决定恢复](../Page/毛泽东.md "wikilink")[邓小平的](../Page/邓小平.md "wikilink")[中华人民共和国国务院副总理职务](../Page/中华人民共和国国务院副总理.md "wikilink")。
  - [1988年](../Page/1988年.md "wikilink")：[中國第一名試管嬰兒在](../Page/中國.md "wikilink")[北京醫科大學第三醫院出生](../Page/北京醫科大學第三醫院.md "wikilink")，是一名女嬰，重3.9公斤。
  - [1992年](../Page/1992年.md "wikilink")：[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[深水埗發生械劫案](../Page/深水埗.md "wikilink")。

### 21世紀

  - [2004年](../Page/2004年.md "wikilink")：[利比亚在](../Page/利比亚.md "wikilink")[维也纳与](../Page/维也纳.md "wikilink")[国际原子能机构正式签署了](../Page/国际原子能机构.md "wikilink")《[不扩散核武器条约](../Page/不扩散核武器条约.md "wikilink")》的附加议定书，允许该机构的核查人员对其境内的所有核设施进行突击式检查。
  - [2005年](../Page/2005年.md "wikilink")：[香港特首](../Page/香港特首.md "wikilink")[董建華宣佈辭職](../Page/董建華.md "wikilink")。
  - [2011年](../Page/2011年.md "wikilink")：[中国](../Page/中国.md "wikilink")[云南省](../Page/云南省.md "wikilink")[德宏傣族景颇族自治州](../Page/德宏傣族景颇族自治州.md "wikilink")[盈江县当地时间](../Page/盈江县.md "wikilink")12时58分（UTC+8），发生里氏5.8级[地震](../Page/地震.md "wikilink")，震中位于北纬24.7度，东经97.9度，震源深度约10公里。
  - [2017年](../Page/2017年.md "wikilink")：韩国总统[朴槿惠](../Page/朴槿惠.md "wikilink")[遭弹劾下台](../Page/朴槿惠弹劾案.md "wikilink")。

## 出生

  - [1415年](../Page/1415年.md "wikilink")：[瓦西里二世](../Page/瓦西里二世.md "wikilink")，[莫斯科大公](../Page/莫斯科.md "wikilink")（逝於1462年）
  - [1845年](../Page/1845年.md "wikilink")：[亞歷山大三世](../Page/亚历山大三世_\(俄国\).md "wikilink")，[俄羅斯帝國皇帝](../Page/俄羅斯帝國.md "wikilink")（逝於1894年）
  - [1924年](../Page/1924年.md "wikilink")：[金庸](../Page/金庸.md "wikilink")，香港作家（逝于[2018年](../Page/2018年.md "wikilink")）
  - [1934年](../Page/1934年.md "wikilink")：[傅聪](../Page/傅聪.md "wikilink")，中國鋼琴家
  - [1934年](../Page/1934年.md "wikilink")：[藤子不二雄](../Page/藤子不二雄A.md "wikilink")，日本漫畫家
  - [1937年](../Page/1937年.md "wikilink")：，[韓國男演员](../Page/韓國.md "wikilink")
  - [1947年](../Page/1947年.md "wikilink")：[金·坎貝爾](../Page/金·坎貝爾.md "wikilink")，[加拿大首位女總理](../Page/加拿大.md "wikilink")
  - [1948年](../Page/1948年.md "wikilink")：[劉天賜](../Page/劉天賜.md "wikilink")，[香港主持人](../Page/香港.md "wikilink")
  - [1957年](../Page/1957年.md "wikilink")：[奥萨玛·本·拉登](../Page/奥萨玛·本·拉登.md "wikilink")，基地組織首領（2011年逝世）
  - [1958年](../Page/1958年.md "wikilink")：[莎朗·史東](../Page/莎朗·史東.md "wikilink")，[美國女演员](../Page/美國.md "wikilink")
  - [1962年](../Page/1962年.md "wikilink")：[松田聖子](../Page/松田聖子.md "wikilink")，[日本女歌手](../Page/日本.md "wikilink")、演員
  - [1963年](../Page/1963年.md "wikilink")：[李宁](../Page/李宁.md "wikilink")，中国体操运动员，退役后从商
  - [1971年](../Page/1971年.md "wikilink")：[藤崎龍](../Page/藤崎龍.md "wikilink")，[日本漫畫家](../Page/日本.md "wikilink")
  - [1973年](../Page/1973年.md "wikilink")：[伊娃·赫茲高娃](../Page/伊娃·赫茲高娃.md "wikilink")，[捷克超級名模](../Page/捷克.md "wikilink")
  - [1976年](../Page/1976年.md "wikilink")：[陳浩然](../Page/陳浩然_\(音樂製作人\).md "wikilink")，[香港音樂製作人](../Page/香港.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[-{zh-hans:艾托奥;zh-hk:伊度奧;zh-tw:艾托奧;}-](../Page/萨缪埃尔·埃托奥.md "wikilink")，喀麥隆足球運動員
  - [1982年](../Page/1982年.md "wikilink")：[星崎未來](../Page/星崎未來.md "wikilink")，[日本AV女优](../Page/日本.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[楊佩婷](../Page/楊佩婷.md "wikilink")，[台灣女演員](../Page/台灣.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[林顏](../Page/香奈兒_\(歌手\).md "wikilink")，[澳大利亞女歌手](../Page/澳大利亞.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[柳賢慶](../Page/柳賢慶.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[李宇春](../Page/李宇春.md "wikilink")，[中国女歌手](../Page/中国.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[奧利維亞·魏爾德](../Page/奧利維亞·魏爾德.md "wikilink")，[美國女演員](../Page/美國.md "wikilink")
  - [1985年](../Page/1985年.md "wikilink")：[張度練](../Page/張度練.md "wikilink")，[韓國女諧星](../Page/韓國.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[刘诗诗](../Page/刘诗诗.md "wikilink")，[中国內地女演员](../Page/中国.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[陳僖儀](../Page/陳僖儀.md "wikilink")，[香港女歌手](../Page/香港.md "wikilink")（2013年逝世）
  - [1987年](../Page/1987年.md "wikilink")：[李天縱](../Page/李天縱.md "wikilink")，[香港女藝人](../Page/香港.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[金珠賢](../Page/金珠賢.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[鳳小岳](../Page/鳳小岳.md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[孫儀凌](../Page/孫儀凌.md "wikilink")，[香港模特兒及演員](../Page/香港.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[韋漢娜](../Page/韋漢娜.md "wikilink")，[香港游泳運動員](../Page/香港.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[鄧玉欣](../Page/鄧玉欣.md "wikilink")，[越南模特兒](../Page/越南.md "wikilink")、2010年越南小姐選美比賽冠軍
  - [1989年](../Page/1989年.md "wikilink")：[Riko](../Page/Riko.md "wikilink")，[韓國女子偶像團體](../Page/韓國.md "wikilink")[BP
    RANIA前成員](../Page/BP_RANIA.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[米津玄師](../Page/米津玄師.md "wikilink")，[日本男歌手](../Page/日本.md "wikilink")
  - [1994年](../Page/1994年.md "wikilink")：[田庾理](../Page/田庾理.md "wikilink")，[韓國女子偶像團體](../Page/韓國.md "wikilink")[Stellar前成員](../Page/Stellar.md "wikilink")
  - [1996年](../Page/1996年.md "wikilink")：[趙廷恩](../Page/趙廷恩.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1998年](../Page/1998年.md "wikilink")：[星☆美優](../Page/星☆美優.md "wikilink")，[日本童星](../Page/日本.md "wikilink")

## 逝世

  - [1792年](../Page/1792年.md "wikilink")：[約翰·斯圖爾特，第三代標得伯爵](../Page/約翰·斯圖爾特，第三代標得伯爵.md "wikilink")，乔治三世时的英国首相（1713年出生）
  - [1872年](../Page/1872年.md "wikilink")：[马志尼](../Page/马志尼.md "wikilink")，意大利革命家（1805年出生）
  - [1985年](../Page/1985年.md "wikilink")：[契尔年科](../Page/契尔年科.md "wikilink")，前苏共中央总书记、苏联最高苏维埃主席团主席（1911年出生）
  - [2016年](../Page/2016年.md "wikilink")：[杜潘芳格](../Page/杜潘芳格.md "wikilink")，台灣客家女詩人。（1927年出生）
  - [2018年](../Page/2018年.md "wikilink")：[唐翔千](../Page/唐翔千.md "wikilink")，前[香港](../Page/香港.md "wikilink")[政务司司长](../Page/政务司司长.md "wikilink")[唐英年父亲](../Page/唐英年.md "wikilink")，商人（1923年出生）
  - [2018年](../Page/2018年.md "wikilink")：[于貝爾·德·紀梵希](../Page/于貝爾·德·紀梵希.md "wikilink")，法国时尚设计师。（1927年出生）

## 节日、风俗习惯

  - [西藏抗暴紀念日](../Page/西藏抗暴紀念日.md "wikilink")