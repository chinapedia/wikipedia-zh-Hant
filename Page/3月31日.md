**3月31日**是[公历一年中的第](../Page/公历.md "wikilink")90天（[闰年第](../Page/闰年.md "wikilink")91天），离全年的结束还有275天。

## 大事記

### 12世紀

  - [1146年](../Page/1146年.md "wikilink")：法国修道院院长[圣伯尔纳铎于弗泽莱当地发表演讲](../Page/圣伯尔纳铎.md "wikilink")，并且向出席的国王[路易七世等人宣扬发动](../Page/路易七世_\(法兰西\).md "wikilink")[第二次十字军东征的必要性](../Page/第二次十字军东征.md "wikilink")。

### 15世紀

  - [1492年](../Page/1492年.md "wikilink")：[卡斯蒂利亞女王](../Page/卡斯蒂利亞.md "wikilink")[伊莎貝拉一世頒布](../Page/伊莎貝拉一世.md "wikilink")，驅逐境內的[猶太人和](../Page/猶太人.md "wikilink")[穆斯林](../Page/穆斯林.md "wikilink")。

### 19世紀

  - [1814年](../Page/1814年.md "wikilink")：[第六次反法同盟占領](../Page/第六次反法同盟.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")。
  - [1854年](../Page/1854年.md "wikilink")：[美國海軍將領](../Page/美國海軍.md "wikilink")[馬休·佩裡與](../Page/馬休·佩裡.md "wikilink")[日本簽署](../Page/日本.md "wikilink")《[神奈川條約](../Page/神奈川條約.md "wikilink")》，迫使日本開放[港口給美國商人](../Page/港口.md "wikilink")。
  - [1885年](../Page/1885年.md "wikilink")：[法國海軍中將](../Page/法国.md "wikilink")[孤拔攻佔](../Page/孤拔.md "wikilink")[台灣的](../Page/臺灣.md "wikilink")[澎湖列島](../Page/澎湖列島.md "wikilink")，[澎湖之役結束](../Page/澎湖之役_\(1885年\).md "wikilink")。
  - [1889年](../Page/1889年.md "wikilink")：由[法國工程師](../Page/法國.md "wikilink")[居斯塔夫·埃菲爾設計的](../Page/居斯塔夫·埃菲爾.md "wikilink")[埃菲爾鐵塔在](../Page/埃菲爾鐵塔.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")[戰神廣場建成](../Page/戰神廣場_\(巴黎\).md "wikilink")。

### 20世紀

  - [1904年](../Page/1904年.md "wikilink")：[日俄戰爭](../Page/日俄戰爭.md "wikilink")：[俄羅斯遠東艦隊在](../Page/俄羅斯.md "wikilink")[海參崴全軍覆滅](../Page/海參崴.md "wikilink")。
  - [1917年](../Page/1917年.md "wikilink")：[美國以](../Page/美國.md "wikilink")2500萬[美元的價格從](../Page/美元.md "wikilink")[丹麥手中購得位於](../Page/丹麥.md "wikilink")[加勒比海的](../Page/加勒比海.md "wikilink")[美屬維爾京群島](../Page/美屬維爾京群島.md "wikilink")。
  - [1920年](../Page/1920年.md "wikilink")：[李大釗發起](../Page/李大釗.md "wikilink")[北京大學](../Page/北京大學.md "wikilink")[馬克思學說研究會](../Page/馬克思學說研究會.md "wikilink")。
  - [1927年](../Page/1927年.md "wikilink")：重庆发生[三·三一惨案](../Page/三·三一惨案.md "wikilink")。
  - [1937年](../Page/1937年.md "wikilink")：[中共中央對](../Page/中共中央.md "wikilink")[張國燾的錯誤做出決定](../Page/張國燾.md "wikilink")。
  - [1942年](../Page/1942年.md "wikilink")：[中華民國和](../Page/中華民國.md "wikilink")[美國簽訂](../Page/美國.md "wikilink")《[五億美元借款協定](../Page/五億美元借款協定.md "wikilink")》。
  - [1953年](../Page/1953年.md "wikilink")：[瑞典人](../Page/瑞典.md "wikilink")[道格·哈馬紹當選](../Page/道格·哈馬紹.md "wikilink")[聯合國秘書長](../Page/聯合國秘書長.md "wikilink")。
  - [1954年](../Page/1954年.md "wikilink")：[越南游擊隊將法軍主力包圍在](../Page/越南.md "wikilink")[奠邊府](../Page/奠边府市.md "wikilink")。
  - [1964年](../Page/1964年.md "wikilink")：[巴西](../Page/巴西.md "wikilink")，軍人統治時期開始。
  - 1964年：[香港各界人士在](../Page/香港.md "wikilink")[政府大球場歡送](../Page/香港大球場.md "wikilink")[總督](../Page/香港總督.md "wikilink")[柏立基](../Page/柏立基.md "wikilink")。
  - [1971年](../Page/1971年.md "wikilink")：[越戰中](../Page/越戰.md "wikilink")[美萊大屠殺主犯](../Page/美萊大屠殺.md "wikilink")，美軍中尉被美國[佐治亞州軍事法庭以殺害平民的罪名判處終身勞役](../Page/佐治亞州.md "wikilink")。
  - [1979年](../Page/1979年.md "wikilink")：[埃及總統](../Page/埃及.md "wikilink")[薩達特和](../Page/薩達特.md "wikilink")[以色列總理](../Page/以色列.md "wikilink")[貝京簽署](../Page/貝京.md "wikilink")[大衛營協議](../Page/大衛營協議.md "wikilink")，結束戰爭狀態，同時，[阿拉伯世界紛紛與埃及斷交](../Page/阿拉伯世界.md "wikilink")。
  - [1984年](../Page/1984年.md "wikilink")：[香港島](../Page/香港島.md "wikilink")[銅鑼灣](../Page/銅鑼灣.md "wikilink")[伊利沙伯大廈揭發花槽藏屍案](../Page/伊利沙伯大廈.md "wikilink")。
  - [1986年](../Page/1986年.md "wikilink")：[墨西哥航空940号班机在从墨西哥城起飞后不久失控坠毁](../Page/墨西哥航空940号班机空难.md "wikilink")，导致机上167人全部遇难。
  - [1992年](../Page/1992年.md "wikilink")：[美国海军最后一艘](../Page/美国海军.md "wikilink")[战列舰](../Page/战列舰.md "wikilink")[密苏里号战列舰在](../Page/密苏里号.md "wikilink")[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[长滩宣告除役](../Page/長灘_\(加利福尼亞州\).md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：中國第八屆全國人民代表大會第一次會議通過《中華人民共和國[澳門特別行政區基本法](../Page/澳門特別行政區基本法.md "wikilink")》。
  - [1994年](../Page/1994年.md "wikilink")：[千島湖事件](../Page/千島湖事件.md "wikilink")：[中國](../Page/中國.md "wikilink")[浙江](../Page/浙江省.md "wikilink")[千島湖上的一艘遊輪遭到](../Page/千島湖.md "wikilink")3名歹徒搶劫並燒船，造成包括24名[台灣遊客在內的](../Page/台灣.md "wikilink")32人死亡。
  - [1996年](../Page/1996年.md "wikilink")：[鳳凰衛視創立](../Page/鳳凰衛視.md "wikilink")。
  - [1998年](../Page/1998年.md "wikilink")：[偉哥](../Page/偉哥.md "wikilink")（威而鋼）投放市場。
  - [2000年](../Page/2000年.md "wikilink")：[香港警務處](../Page/香港警務處.md "wikilink")[邊界警區](../Page/邊界警區.md "wikilink")[半軍事化的](../Page/半軍事化.md "wikilink")[快速應變部隊成立](../Page/快速應變部隊.md "wikilink")，成為[專業](../Page/專業.md "wikilink")[部門接替](../Page/香港警務處架構.md "wikilink")[香港回歸前由](../Page/香港回歸.md "wikilink")[駐港英軍負責的](../Page/駐港英軍.md "wikilink")[香港邊境禁區](../Page/香港邊境禁區.md "wikilink")[巡邏等任務](../Page/巡邏.md "wikilink")。

### 21世紀

  - [2001年](../Page/2001年.md "wikilink")：[世嘉停止了](../Page/世嘉.md "wikilink")[Dreamcast的业务](../Page/Dreamcast.md "wikilink")，退出了游戏主机市场，并改组为纯第三方游戏发行商。
  - [2002年](../Page/2002年.md "wikilink")：[台灣北部地區發生大](../Page/台灣.md "wikilink")[地震](../Page/331大地震.md "wikilink")，造成當時建造中的[台北101的吊塔倒塌](../Page/台北101.md "wikilink")，多人傷亡。
  - [2008年](../Page/2008年.md "wikilink")：香港[九巴在天慈邨旁的露天停車場有](../Page/九巴.md "wikilink")10輛雙層巴士起火。
  - [2012年](../Page/2012年.md "wikilink")：位于[中国](../Page/中国.md "wikilink")[湖南省](../Page/湖南省.md "wikilink")[吉首市](../Page/吉首市.md "wikilink")，[渝湘高速公路上的桥梁](../Page/渝湘高速公路.md "wikilink")[矮寨大桥正式建成通车](../Page/矮寨大桥.md "wikilink")，成为世界上最长的跨峡谷[悬索桥](../Page/悬索桥.md "wikilink")。
  - [2016年](../Page/2016年.md "wikilink")：法國發生了[不眠之夜](../Page/不眠之夜.md "wikilink")[佔領運動](../Page/佔領.md "wikilink")，且在數日內擴散到[比利時](../Page/比利時.md "wikilink")、[德國及](../Page/德國.md "wikilink")[西班牙](../Page/西班牙.md "wikilink")。

## 出生

  - [1519年](../Page/1519年.md "wikilink")：[亨利二世](../Page/亨利二世_\(法兰西\).md "wikilink")，[法國](../Page/法國.md "wikilink")[瓦盧瓦王朝國王](../Page/瓦盧瓦王朝.md "wikilink")。（[1559年逝世](../Page/1559年.md "wikilink")）
  - [1596年](../Page/1596年.md "wikilink")：[笛卡尔](../Page/笛卡尔.md "wikilink")，[法国](../Page/法国.md "wikilink")[数学家](../Page/数学家.md "wikilink")，[哲学家](../Page/哲學家.md "wikilink")（[1650年逝世](../Page/1650年.md "wikilink")）。
  - [1732年](../Page/1732年.md "wikilink")：[约瑟夫·海顿](../Page/约瑟夫·海顿.md "wikilink")，[奥地利](../Page/奥地利.md "wikilink")[音乐家](../Page/音乐家.md "wikilink")（[1809年逝世](../Page/1809年.md "wikilink")）。
  - [1811年](../Page/1811年.md "wikilink")：[罗伯特·威廉·本生](../Page/罗伯特·威廉·本生.md "wikilink")，[德国](../Page/德国.md "wikilink")[化学家](../Page/化学家.md "wikilink")。（[1899年逝世](../Page/1899年.md "wikilink")）
  - [1890年](../Page/1890年.md "wikilink")：[威廉·勞倫斯·布拉格](../Page/威廉·勞倫斯·布拉格.md "wikilink")，[英國](../Page/英國.md "wikilink")[物理學家](../Page/物理學家.md "wikilink")。（[1971年去世](../Page/1971年.md "wikilink")）
  - [1943年](../Page/1943年.md "wikilink")：[克里斯多夫·華肯](../Page/克里斯多夫·華肯.md "wikilink")，[美国演員](../Page/美国.md "wikilink")。
  - [1946年](../Page/1946年.md "wikilink")：[尹清楓](../Page/尹清楓.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[海軍](../Page/海軍.md "wikilink")[上校](../Page/上校.md "wikilink")（[1993年逝世](../Page/1993年.md "wikilink")）。
  - [1948年](../Page/1948年.md "wikilink")：[艾爾·高爾](../Page/艾爾·高爾.md "wikilink")，[美国副总统](../Page/美国.md "wikilink")。
  - [1957年](../Page/1957年.md "wikilink")：[潘維剛](../Page/潘維剛.md "wikilink")，[台灣政治人物](../Page/台灣.md "wikilink")，前[立法委員](../Page/立法委員.md "wikilink")。
  - 1957年：[鍾樹根](../Page/鍾樹根.md "wikilink")，[香港政界人物](../Page/香港.md "wikilink")
  - [1961年](../Page/1961年.md "wikilink")：[鄭怡](../Page/鄭怡.md "wikilink")，[台灣女歌手](../Page/台灣.md "wikilink")、主持人
  - [1964年](../Page/1964年.md "wikilink")：[黃貫中](../Page/黃貫中.md "wikilink")，[香港搖滾樂隊](../Page/香港.md "wikilink")[Beyond結他手](../Page/Beyond.md "wikilink")。
  - 1964年：[葉歡](../Page/葉歡.md "wikilink")，台灣[演員](../Page/演員.md "wikilink")
  - [1968年](../Page/1968年.md "wikilink")：[謝婉雯](../Page/謝婉雯.md "wikilink")，感染[SARS逝世的](../Page/SARS.md "wikilink")[香港女醫生](../Page/香港.md "wikilink")。（[2003年逝世](../Page/2003年.md "wikilink")）
  - [1971年](../Page/1971年.md "wikilink")：[伊万·迈克格雷戈](../Page/伊万·迈克格雷戈.md "wikilink")，[蘇格蘭演员](../Page/蘇格蘭.md "wikilink")、歌手與冒險家。
  - [1972年](../Page/1972年.md "wikilink")：[趙學而](../Page/趙學而.md "wikilink")，[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [1978年](../Page/1978年.md "wikilink")：[彭康育](../Page/彭康育.md "wikilink")，台湾演员、歌手、组合[5566前成員](../Page/5566.md "wikilink")。
  - [1979年](../Page/1979年.md "wikilink")：[盧弘喆](../Page/盧弘喆.md "wikilink")，[韓國節目主持人](../Page/韓國.md "wikilink")。
  - [1980年](../Page/1980年.md "wikilink")：[王建民](../Page/王建民_\(棒球選手\).md "wikilink")，[台灣](../Page/台灣.md "wikilink")[棒球選手](../Page/棒球.md "wikilink")。
  - 1980年：[坂本真綾](../Page/坂本真綾.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")。
  - [1981年](../Page/1981年.md "wikilink")：[新谷良子](../Page/新谷良子.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")。
  - [1982年](../Page/1982年.md "wikilink")：[林晶恩](../Page/林晶恩.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")。
  - [1983年](../Page/1983年.md "wikilink")：[傑夫·馬席斯](../Page/傑夫·馬席斯.md "wikilink")，[美國](../Page/美國.md "wikilink")[棒球選手](../Page/棒球.md "wikilink")。
  - [1984年](../Page/1984年.md "wikilink")：[房思瑜](../Page/房思瑜.md "wikilink")，[台灣女演員](../Page/台灣.md "wikilink")。
  - [1985年](../Page/1985年.md "wikilink")：[張睿家](../Page/張睿家.md "wikilink")，台灣男演員。
  - 1985年：[黃若薇](../Page/黃若薇.md "wikilink")，[台灣新聞主播](../Page/台灣.md "wikilink")。
  - 1985年：[謝茜嘉·蘇佐](../Page/謝茜嘉·蘇佐.md "wikilink")，[美國模特兒](../Page/美國.md "wikilink")、演員。
  - [1986年](../Page/1986年.md "wikilink")：[鄭禮騫](../Page/鄭禮騫.md "wikilink")，[香港](../Page/香港.md "wikilink")[足球選手](../Page/足球.md "wikilink")。
  - 1986年：[安宰弘](../Page/安宰弘.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")。
  - [1987年](../Page/1987年.md "wikilink")：[尹鈞相](../Page/尹鈞相.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")。
  - [1989年](../Page/1989年.md "wikilink")：[黃梓育](../Page/黃梓育.md "wikilink")，台灣[棒球選手](../Page/棒球.md "wikilink")。
  - [1990年](../Page/1990年.md "wikilink")：[方容國](../Page/方容國.md "wikilink")，韓國男子偶像團體[B.A.P前隊長](../Page/B.A.P.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：[李玉璽](../Page/李玉璽.md "wikilink")，[台灣男演員](../Page/台灣.md "wikilink")、歌手。
  - [1997年](../Page/1997年.md "wikilink")：[具俊會](../Page/具俊會.md "wikilink")，韓國男子偶像團體[iKON成員](../Page/iKON.md "wikilink")

## 逝世

  - [528年](../Page/528年.md "wikilink")：[元诩](../Page/元诩.md "wikilink")，[中国](../Page/中国.md "wikilink")[北魏肃宗孝明](../Page/北魏.md "wikilink")[皇帝](../Page/中国皇帝.md "wikilink")。（[510年出生](../Page/510年.md "wikilink")）
  - [1341年](../Page/1341年.md "wikilink")：[伊凡一世](../Page/伊凡一世.md "wikilink")，[莫斯科大公](../Page/莫斯科.md "wikilink")。（[1288年出生](../Page/1288年.md "wikilink")）
  - [1547年](../Page/1547年.md "wikilink")：[弗朗索瓦一世](../Page/弗朗索瓦一世_\(法兰西\).md "wikilink")，法國國王。（[1494年出生](../Page/1494年.md "wikilink")）
  - [1727年](../Page/1727年.md "wikilink")：[艾萨克·牛顿](../Page/艾萨克·牛顿.md "wikilink")，英国物理学家。([1643年出生](../Page/1643年.md "wikilink"))
  - [1816年](../Page/1816年.md "wikilink")：[法蘭西斯·亞斯理](../Page/法蘭西斯·亞斯理.md "wikilink")，[循道衛理宗在](../Page/循道衛理宗.md "wikilink")[美國設立的教會的兩位首任會督之一](../Page/美國.md "wikilink")。（[1745年出生](../Page/1745年.md "wikilink")）
  - [1855年](../Page/1855年.md "wikilink")：[夏綠蒂·勃朗特](../Page/夏綠蒂·勃朗特.md "wikilink")，[英国小说家](../Page/英国.md "wikilink")。（[1816年出生](../Page/1816年.md "wikilink")）
  - [1869年](../Page/1869年.md "wikilink")：[卡甸](../Page/卡甸.md "wikilink")，[通靈術的發表者](../Page/通靈術.md "wikilink")。（[1804年出生](../Page/1804年.md "wikilink")）
  - [1913年](../Page/1913年.md "wikilink")：[J·P·摩根](../Page/J·P·摩根.md "wikilink")，金融家。（[1837年出生](../Page/1837年.md "wikilink")）
  - [1927年](../Page/1927年.md "wikilink")：[康有为](../Page/康有为.md "wikilink")，中國政治家、思想家。（[1858年出生](../Page/1858年.md "wikilink")）
  - [1935年](../Page/1935年.md "wikilink")：[钱壮飞](../Page/钱壮飞.md "wikilink")，[中國共產黨著名](../Page/中國共產黨.md "wikilink")[間諜](../Page/間諜.md "wikilink")、[中國電影明星](../Page/中國電影.md "wikilink")[黎莉莉之父](../Page/黎莉莉.md "wikilink")。（[1895年出生](../Page/1895年.md "wikilink")）
  - [1972年](../Page/1972年.md "wikilink")：[米娜·古馬利](../Page/米娜·古馬利.md "wikilink")，[印度女演員和女詩人](../Page/印度.md "wikilink")（[1933年出生](../Page/1933年.md "wikilink")）
  - [2005年](../Page/2005年.md "wikilink")：[特麗·夏沃](../Page/特麗·夏沃.md "wikilink")，[美國植物人](../Page/美國.md "wikilink")。（[1963年出生](../Page/1963年.md "wikilink")）
  - 2010年：[狄娜](../Page/狄娜.md "wikilink")，[香港女演員](../Page/香港.md "wikilink")，企業家。（[1945年出生](../Page/1945年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[里坡](../Page/里坡.md "wikilink")，中国影视演员、配音演员。（[1928年出生](../Page/1928年.md "wikilink")）
  - [2014年](../Page/2014年.md "wikilink")：[艾琳·費南德斯](../Page/艾琳·費南德斯.md "wikilink")，馬來西亞社會工作者。（[1946年出生](../Page/1946年.md "wikilink")）
  - [2015年](../Page/2015年.md "wikilink")：[藤原可可亞](../Page/藤原可可亞.md "wikilink")，日本漫畫家，代表作《[妖狐×僕SS](../Page/妖狐×僕SS.md "wikilink")》。（[1983年出生](../Page/1983年.md "wikilink")）

## 节日、风俗习惯

  - ：[自由日](../Page/自由日_\(馬爾他\).md "wikilink")

  - [國際跨性別現身日](../Page/國際跨性別現身日.md "wikilink")

  - [世界备份日](../Page/世界备份日.md "wikilink")

  - [三月三十一日峰](../Page/三月三十一日峰.md "wikilink")

## 參考資料