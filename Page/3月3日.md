**3月3日**是[陽曆一年中的第](../Page/陽曆.md "wikilink")62天（[閏年第](../Page/閏年.md "wikilink")63天），离全年的结束还有303天。

## 大事记

### 6世紀

  - [581年](../Page/581年.md "wikilink")：[杨坚称帝](../Page/杨坚.md "wikilink")，[北周亡](../Page/北周.md "wikilink")。

### 7世紀

  - [636年](../Page/636年.md "wikilink")：[唐朝五部史书](../Page/唐朝.md "wikilink")（《[梁书](../Page/梁书.md "wikilink")》、《[陈书](../Page/陈书.md "wikilink")》、《[北齐书](../Page/北齐书.md "wikilink")》、《[北周书](../Page/北周书.md "wikilink")》、《[隋书](../Page/隋书.md "wikilink")》）修撰完成。

### 8世紀

  - [705年](../Page/705年.md "wikilink")：[唐中宗恢复](../Page/唐中宗.md "wikilink")[唐朝](../Page/唐朝.md "wikilink")[国号](../Page/国号.md "wikilink")，将国号由“周”改回“唐”。

### 19世紀

  - [1861年](../Page/1861年.md "wikilink")：[俄国](../Page/俄国.md "wikilink")[沙皇](../Page/沙皇.md "wikilink")[亚历山大二世正式批准了废除](../Page/亚历山大二世_\(俄国\).md "wikilink")[农奴制的法令](../Page/农奴制.md "wikilink")，并签署了关于废除农奴制的特别宣言。
  - [1865年](../Page/1865年.md "wikilink")：[香港上海匯豐銀行開業](../Page/香港上海匯豐銀行.md "wikilink")。
  - [1878年](../Page/1878年.md "wikilink")：[俄罗斯帝国与](../Page/俄罗斯帝国.md "wikilink")[奥斯曼帝国签署](../Page/奥斯曼帝国.md "wikilink")《[圣斯特凡诺条约](../Page/圣斯特凡诺条约.md "wikilink")》，确认[保加利亚脱离奥斯曼而独立](../Page/保加利亚.md "wikilink")。

### 20世紀

  - [1912年](../Page/1912年.md "wikilink")：[中国同盟会在](../Page/中国同盟会.md "wikilink")[南京正式转型为公开](../Page/南京.md "wikilink")[政党](../Page/政党.md "wikilink")。
  - [1915年](../Page/1915年.md "wikilink")：負責[航空科學研究的](../Page/航空科學.md "wikilink")[美國國家航空諮詢委員會正式成立](../Page/美國國家航空諮詢委員會.md "wikilink")，同時也是[美國國家航空暨太空總署的前身](../Page/美國國家航空暨太空總署.md "wikilink")
  - [1918年](../Page/1918年.md "wikilink")：[苏俄与](../Page/苏俄.md "wikilink")[同盟国签订](../Page/同盟國_\(第一次世界大戰\).md "wikilink")《[布列斯特-立陶夫斯克條約](../Page/布列斯特-立陶夫斯克條約.md "wikilink")》，以割地赔款的方式退出[第一次世界大战](../Page/第一次世界大战.md "wikilink")。
  - [1923年](../Page/1923年.md "wikilink")：[美國](../Page/美國.md "wikilink")《[时代](../Page/時代_\(雜誌\).md "wikilink")》[杂志第一期正式出版](../Page/杂志.md "wikilink")。
  - [1924年](../Page/1924年.md "wikilink")：[土耳其废除](../Page/土耳其.md "wikilink")[哈里发职位](../Page/哈里发.md "wikilink")。
  - [1931年](../Page/1931年.md "wikilink")：[美国国会正式將作家](../Page/美国国会.md "wikilink")[弗朗西斯·斯科特·基為](../Page/弗朗西斯·斯科特·基.md "wikilink")[1812年戰爭](../Page/1812年戰爭.md "wikilink")[巴爾的摩之戰作詞的](../Page/巴爾的摩之戰.md "wikilink")《[星條旗](../Page/星條旗.md "wikilink")》定為[美國國歌](../Page/美國國歌.md "wikilink")。
  - [1933年](../Page/1933年.md "wikilink")：[清雲科技大學建校](../Page/清雲科技大學.md "wikilink")。
  - [1938年](../Page/1938年.md "wikilink")：[沙特阿拉伯首次发现了](../Page/沙特阿拉伯.md "wikilink")[石油](../Page/石油.md "wikilink")。
  - [1948年](../Page/1948年.md "wikilink")：[国共内战](../Page/国共内战.md "wikilink")：[瓦子街战役](../Page/瓦子街战役.md "wikilink")，[彭德怀率军歼灭](../Page/彭德怀.md "wikilink")[国民革命军整编二十九军](../Page/国民革命军.md "wikilink")，军长[刘戡自杀](../Page/刘戡.md "wikilink")。
  - [1955年](../Page/1955年.md "wikilink")：[中共中央发出](../Page/中共中央.md "wikilink")《关于迅速布置粮食购销工作安定农民生产情绪的紧急指示》，强调对粮食必须采取“定产、定购、定销”的措施。
  - [1958年](../Page/1958年.md "wikilink")：[努里·賽義德第十四次出任](../Page/努里·賽義德.md "wikilink")[伊拉克總理一職](../Page/伊拉克總理.md "wikilink")，這也是其最後一次就任[總理](../Page/總理.md "wikilink")。
  - [1960年](../Page/1960年.md "wikilink")：[摩洛哥国王](../Page/摩洛哥.md "wikilink")[哈桑二世在首都](../Page/哈桑二世_\(摩洛哥\).md "wikilink")[拉巴特登基](../Page/拉巴特.md "wikilink")。
  - [1965年](../Page/1965年.md "wikilink")：[南部非洲的贝专纳](../Page/南部非洲.md "wikilink")（即现在的[博茨瓦纳共和国](../Page/博茨瓦纳共和国.md "wikilink")）实行内部自治。
  - [1983年](../Page/1983年.md "wikilink")：轟動[香港的](../Page/香港.md "wikilink")「[林過雲案](../Page/林過雲.md "wikilink")」在[最高法院開審](../Page/香港立法會大樓.md "wikilink")。
  - [1986年](../Page/1986年.md "wikilink")：“[863计划](../Page/863计划.md "wikilink")”草拟完成。
  - [1993年](../Page/1993年.md "wikilink")：[加拿大短跑运动员](../Page/加拿大.md "wikilink")[-{zh-hans:本·约翰逊;zh-hk:賓·莊遜;zh-tw:班·強生;}-被终身禁赛](../Page/班·強生_\(田徑運動員\).md "wikilink")。
  - [1995年](../Page/1995年.md "wikilink")：[聯合國維持和平部隊宣佈結束在](../Page/聯合國維持和平部隊.md "wikilink")[索馬里的維和任務](../Page/索馬里.md "wikilink")。
  - [1997年](../Page/1997年.md "wikilink")：位于[新西兰](../Page/新西兰.md "wikilink")[奥克兰的](../Page/奧克蘭_\(紐西蘭\).md "wikilink")[天空塔正式开放](../Page/天空塔.md "wikilink")，高达328米，成为[南半球最高的](../Page/南半球.md "wikilink")[建筑](../Page/建筑.md "wikilink")。

### 21世紀

  - [2001年](../Page/2001年.md "wikilink")：香港首次舉行[教師語文能力評核](../Page/教師語文能力評核.md "wikilink")，所有本港在職[英語及](../Page/英語.md "wikilink")[普通話](../Page/普通話.md "wikilink")[教師](../Page/教師.md "wikilink")，必須考試及格後，方可繼續任教有關科目。
  - [2005年](../Page/2005年.md "wikilink")：美國冒險家詹姆斯·斯蒂芬·福塞特駕駛試驗性嘅「環球飛行者」號噴氣式飛機完成世界上第一次一個人揸飛機完成無間斷環球飛行。
  - [2006年](../Page/2006年.md "wikilink")：首屆[世界棒球經典賽A組預賽在](../Page/世界棒球經典賽.md "wikilink")[日本](../Page/日本.md "wikilink")[東京巨蛋開打](../Page/東京巨蛋.md "wikilink")。
  - [2009年](../Page/2009年.md "wikilink")：[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")在[巴基斯坦](../Page/巴基斯坦.md "wikilink")[旁遮普省首府](../Page/旁遮普省.md "wikilink")[拉合爾遭遇](../Page/拉合爾.md "wikilink")[恐怖襲擊](../Page/斯里蘭卡板球隊遇襲事件.md "wikilink")，造成護送球隊的6名[警察和](../Page/警察.md "wikilink")2名平民死亡，6名球員受傷。
  - [2011年](../Page/2011年.md "wikilink")：[苹果公司在](../Page/苹果公司.md "wikilink")[旧金山芳草地艺术中心发布](../Page/旧金山.md "wikilink")[iPad
    2和](../Page/iPad_2.md "wikilink")[iOS](../Page/iOS.md "wikilink")
    4.3。
  - [2013年](../Page/2013年.md "wikilink")：「[長者及合資格殘疾人士公共交通票價優惠計劃](../Page/長者及合資格殘疾人士公共交通票價優惠計劃.md "wikilink")」第三階段實施，涵蓋範圍除[港鐵](../Page/港鐵.md "wikilink")、[九龍巴士](../Page/九龍巴士.md "wikilink")、[龍運巴士](../Page/龍運巴士.md "wikilink")、[城巴及](../Page/城巴.md "wikilink")[新世界第一巴士外](../Page/新世界第一巴士.md "wikilink")，亦加入[渡輪服務及](../Page/香港渡輪.md "wikilink")[新大嶼山巴士專營路線](../Page/新大嶼山巴士.md "wikilink")。

## 出生

  - [1455年](../Page/1455年.md "wikilink")：[若昂二世](../Page/若昂二世.md "wikilink")，[葡萄牙和阿爾加爾維國王](../Page/葡萄牙.md "wikilink")（逝於[1495年](../Page/1495年.md "wikilink")）
  - [1847年](../Page/1847年.md "wikilink")：[亞歷山大·格拉漢姆·貝爾](../Page/亞歷山大·格拉漢姆·貝爾.md "wikilink")，[加拿大](../Page/加拿大.md "wikilink")[发明家和企业家](../Page/发明家.md "wikilink")（逝於[1922年](../Page/1922年.md "wikilink")）
  - [1901年](../Page/1901年.md "wikilink")：[克勞德·喬勒斯](../Page/克勞德·喬勒斯.md "wikilink")，英国军人、[第一次世界大戰老兵](../Page/第一次世界大戰.md "wikilink")（逝於[2011年](../Page/2011年.md "wikilink")）
  - [1911年](../Page/1911年.md "wikilink")：[珍·哈露](../Page/珍·哈露.md "wikilink")，[美國著名女演員及性感女神](../Page/美國.md "wikilink")（逝於[1937年](../Page/1937年.md "wikilink")）
  - [1916年](../Page/1916年.md "wikilink")：[保罗·哈尔莫斯](../Page/保罗·哈尔莫斯.md "wikilink")，[匈牙利出身](../Page/匈牙利.md "wikilink")[美國数学家](../Page/美國.md "wikilink")（逝於[2006年](../Page/2006年.md "wikilink")）
  - [1924年](../Page/1924年.md "wikilink")：[村山富市](../Page/村山富市.md "wikilink")，[日本第](../Page/日本.md "wikilink")81任[首相](../Page/日本首相.md "wikilink")
  - [1933年](../Page/1933年.md "wikilink")：[卡羅琳·李·拉齊維爾](../Page/卡羅琳·李·拉齊維爾.md "wikilink")，美國社交名媛、公關及室內設計師（逝於[2019年](../Page/2019年.md "wikilink")）
  - [1940年](../Page/1940年.md "wikilink")：[金容琳](../Page/金容琳.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1955年](../Page/1955年.md "wikilink")：[陳偉業](../Page/陳偉業.md "wikilink")，[香港政界人物](../Page/香港.md "wikilink")
  - [1963年](../Page/1963年.md "wikilink")：[黃麗梅](../Page/黃麗梅.md "wikilink")，香港主持人
  - [1964年](../Page/1964年.md "wikilink")：[江嘉良](../Page/江嘉良.md "wikilink")，[中國乒乓球運動員](../Page/中國.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink")：[朱莉·鲍温](../Page/朱莉·鲍温.md "wikilink")，美國女演員
  - [1978年](../Page/1978年.md "wikilink")：[徐懷鈺](../Page/徐懷鈺.md "wikilink")，[台灣女歌手](../Page/台灣.md "wikilink")、演員
  - [1978年](../Page/1978年.md "wikilink")：[金勤](../Page/金勤.md "wikilink")，台湾男演员，代表作之一：《[孽子](../Page/孽子.md "wikilink")》（饰演小玉）
  - [1978年](../Page/1978年.md "wikilink")：[小澤圓](../Page/小澤圓.md "wikilink")，[日本](../Page/日本.md "wikilink")[AV女優](../Page/AV女優.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[柯雅馨](../Page/柯雅馨.md "wikilink")，台灣女演員
  - 1980年：[曾威豪](../Page/曾威豪.md "wikilink")，台灣歌手
  - 1980年：[凱薩琳·華特斯頓](../Page/凱薩琳·華特斯頓.md "wikilink")，[美国女演員](../Page/美国.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[成宥利](../Page/成宥利.md "wikilink")，[韩国女演员](../Page/韩国.md "wikilink")、歌手
  - [1981年](../Page/1981年.md "wikilink")：[柳真](../Page/柳真.md "wikilink")，[韩国女演员](../Page/韩国.md "wikilink")、歌手
  - [1982年](../Page/1982年.md "wikilink")：[謝茜嘉·比爾](../Page/謝茜嘉·比爾.md "wikilink")，[美國女演員](../Page/美國.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[川上優](../Page/川上優.md "wikilink")，[日本AV女優](../Page/日本.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[吉澤明步](../Page/吉澤明步.md "wikilink")，[日本AV女优](../Page/日本.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[櫻木凜](../Page/櫻木凜.md "wikilink")，[日本AV女优](../Page/日本.md "wikilink")
  - 1989年：[並木優](../Page/並木優.md "wikilink")，[日本AV女优](../Page/日本.md "wikilink")
  - 1989年：[李受美](../Page/李受美.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1991年：](../Page/1991年.md "wikilink")[朴初瓏](../Page/朴初瓏.md "wikilink")，韓國女子團體[Apink成員](../Page/Apink.md "wikilink")
  - [1992年](../Page/1992年.md "wikilink")：[施拉達·卡普爾](../Page/施拉達·卡普爾.md "wikilink")，[印度女演員](../Page/印度.md "wikilink")
  - [1994年](../Page/1994年.md "wikilink")：，[韓國女演員](../Page/韓國.md "wikilink")
  - [1994年](../Page/1994年.md "wikilink")：[池受玟](../Page/池受玟.md "wikilink")，韩国女子組合[SONAMOO隊長](../Page/SONAMOO.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink")：[李長埈](../Page/李長埈.md "wikilink")，[韓國男子偶像團體](../Page/韓國.md "wikilink")[Golden
    Child成員](../Page/Golden_Child.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink")：[卡蜜拉·卡貝羅](../Page/卡蜜拉·卡貝羅.md "wikilink")，[美國女子組合](../Page/美國.md "wikilink")[五佳人成員](../Page/五佳人.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink")：[蒂雅娜·寶斯高域](../Page/蒂雅娜·寶斯高域.md "wikilink")，[塞爾維亞女子排球運動員](../Page/塞爾維亞.md "wikilink")
  - [1998年](../Page/1998年.md "wikilink")：[傑森·塔圖姆](../Page/傑森·塔圖姆.md "wikilink")，NBA運動員

## 逝世

  - [1706年](../Page/1706年.md "wikilink")：[約翰·帕海貝爾](../Page/約翰·帕海貝爾.md "wikilink")，[德國著名作曲家](../Page/德國.md "wikilink")（[1653年出生](../Page/1653年.md "wikilink")）
  - [1707年](../Page/1707年.md "wikilink")：[奥朗则布](../Page/奥朗则布.md "wikilink")，[印度](../Page/印度.md "wikilink")[莫卧儿王朝大帝](../Page/莫卧儿王朝.md "wikilink")（[1618年出生](../Page/1618年.md "wikilink")）
  - [1968年](../Page/1968年.md "wikilink")：[许广平](../Page/许广平.md "wikilink")，作家（[1898年出生](../Page/1898年.md "wikilink")）
  - [1993年](../Page/1993年.md "wikilink")：[高元钧](../Page/高元钧.md "wikilink")，[山东快书一代宗师](../Page/山东快书.md "wikilink")（[1916年出生](../Page/1916年.md "wikilink")）

## 节日、风俗习惯

  - ：[解放日](../Page/解放日.md "wikilink")（[1878年](../Page/1878年.md "wikilink")）

  - ：[全国爱耳日](../Page/全国爱耳日.md "wikilink")

  - ：[女兒节](../Page/女兒节.md "wikilink")

## 出生（虛構人物）

  - [1943年](../Page/1943年.md "wikilink")：[兩津勘吉](../Page/兩津勘吉.md "wikilink")（[烏龍派出所](../Page/烏龍派出所.md "wikilink")）