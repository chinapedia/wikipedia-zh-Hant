**3月8日**是[公历一年中的第](../Page/公历.md "wikilink")67天（[闰年第](../Page/闰年.md "wikilink")68天），离全年的结束还有298天。

## 大事记

### 18世紀

  - [1702年](../Page/1702年.md "wikilink")：[英格兰](../Page/英格兰.md "wikilink")、[苏格兰和](../Page/苏格兰.md "wikilink")[爱尔兰国王](../Page/爱尔兰.md "wikilink")[威廉去世](../Page/威廉三世_\(英格兰\).md "wikilink")，其妻子的妹妹继承王位成为[安妮女王](../Page/安妮女王.md "wikilink")。
  - [1722年](../Page/1722年.md "wikilink")：[伊朗](../Page/伊朗.md "wikilink")[萨菲王朝在古尔纳巴德战役中被](../Page/萨非王朝.md "wikilink")[阿富汗军队击败](../Page/阿富汗.md "wikilink")，推动[伊朗陷入](../Page/伊朗.md "wikilink")[无政府状态](../Page/无政府状态.md "wikilink")。
  - [1736年](../Page/1736年.md "wikilink")：[阿夫沙尔王朝的创始人](../Page/阿夫沙尔王朝.md "wikilink")[纳迪尔沙加冕](../Page/纳迪尔沙.md "wikilink")[伊朗的](../Page/伊朗.md "wikilink")[沙阿](../Page/沙阿.md "wikilink")。
  - [1775年](../Page/1775年.md "wikilink")：一位匿名作家(一些人认为是[托马斯·潘恩](../Page/托马斯·潘恩.md "wikilink"))发表了“美国的非洲奴隶制”，这是美国殖民地的第一篇要求解放奴隶和废除奴隶制的文章。
  - [1777年](../Page/1777年.md "wikilink")：来自安斯巴赫和拜罗伊特的军团，在[美国独立战争中被派去支持英国](../Page/美国独立战争.md "wikilink")，在[奥森富尔特叛变](../Page/奥森富尔特.md "wikilink")。
  - [1782年](../Page/1782年.md "wikilink")：格纳等须藤屠杀：在[俄亥俄州](../Page/俄亥俄州.md "wikilink")，96名改信基督教的[美国原住民被宾夕法尼亚民兵杀害](../Page/美国原住民.md "wikilink")，以报复其他印第安部落进行的袭击。

### 19世紀

  - [1817年](../Page/1817年.md "wikilink")：[纽约证券交易所成立](../Page/纽约证券交易所.md "wikilink")。
  - [1844年](../Page/1844年.md "wikilink")：[瑞典和](../Page/瑞典国王.md "wikilink")[挪威国王](../Page/挪威国王.md "wikilink")[卡尔十四世·约翰去世](../Page/卡尔十四世·约翰.md "wikilink")，其子[奥斯卡一世继承王位](../Page/奥斯卡一世.md "wikilink")。

### 20世紀

  - [1909年](../Page/1909年.md "wikilink")：[美国](../Page/美国.md "wikilink")[芝加哥妇女大游行](../Page/芝加哥.md "wikilink")。
  - [1917年](../Page/1917年.md "wikilink")：[俄國](../Page/俄國.md "wikilink")[彼得格勒的工人举行](../Page/彼得格勒.md "wikilink")[罢工和](../Page/罢工.md "wikilink")[游行](../Page/游行.md "wikilink")，[二月革命爆發](../Page/俄國二月革命.md "wikilink")。
  - [1918年](../Page/1918年.md "wikilink")：[安福俱乐部成立](../Page/安福俱乐部.md "wikilink")，[安福系开始控制](../Page/安福系.md "wikilink")[中华民国政权](../Page/中华民国.md "wikilink")。
  - [1939年](../Page/1939年.md "wikilink")：[抗战](../Page/抗日战争_\(中国\).md "wikilink")：[日军占领](../Page/日军.md "wikilink")[南昌](../Page/南昌.md "wikilink")。
  - [1942年](../Page/1942年.md "wikilink")：[太平洋戰爭](../Page/太平洋戰爭.md "wikilink")：[爪哇島](../Page/爪哇島.md "wikilink")[荷蘭軍隊向日軍投降](../Page/荷蘭.md "wikilink")，同日日軍佔領[緬甸首都](../Page/緬甸.md "wikilink")[仰光](../Page/仰光.md "wikilink")。
  - [1947年](../Page/1947年.md "wikilink")：[蔣介石在](../Page/蔣介石.md "wikilink")[陳儀的暗中要求下](../Page/陳儀.md "wikilink")，增派陸軍整編第21師與憲兵第4團等軍隊共一萬三千人自中國大陸抵達[臺灣](../Page/臺灣.md "wikilink")，展開[二二八事件的鎮壓與](../Page/二二八事件.md "wikilink")[清鄉](../Page/清鄉.md "wikilink")。
  - [1950年](../Page/1950年.md "wikilink")：[苏联](../Page/苏联.md "wikilink")[塔斯社发布消息](../Page/塔斯社.md "wikilink")，苏联第一枚[原子弹试爆成功](../Page/原子弹.md "wikilink")。
  - [1954年](../Page/1954年.md "wikilink")：《[美日共同防御协定](../Page/美日共同防御协定.md "wikilink")》签署。
  - [1965年](../Page/1965年.md "wikilink")：[越南战争](../Page/越南战争.md "wikilink")：[美国海军陆战队登陆](../Page/美国海军陆战队.md "wikilink")[南越](../Page/南越.md "wikilink")[峴港](../Page/峴港市.md "wikilink")，标志着美军开始直接参战。
  - [1966年](../Page/1966年.md "wikilink")：[中国](../Page/中华人民共和国.md "wikilink")[河北](../Page/河北.md "wikilink")[邢台发生](../Page/邢台.md "wikilink")[地震](../Page/邢台地震.md "wikilink")。
  - [1974年](../Page/1974年.md "wikilink")：[法國](../Page/法國.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")[夏爾·戴高樂國際機場正式啟用](../Page/夏爾·戴高樂國際機場.md "wikilink")。
  - [1976年](../Page/1976年.md "wikilink")：中國[吉林省降落罕见](../Page/吉林省.md "wikilink")[陨石雨](../Page/陨石雨.md "wikilink")。
  - [1983年](../Page/1983年.md "wikilink")：[美国总统](../Page/美国总统.md "wikilink")[里根在](../Page/罗纳德·里根.md "wikilink")[佛罗里达州](../Page/佛罗里达州.md "wikilink")[奥兰多的一场演讲中称](../Page/奥兰多_\(佛罗里达州\).md "wikilink")[苏联为](../Page/苏联.md "wikilink")[邪恶帝国](../Page/邪恶帝国.md "wikilink")。
  - [1985年](../Page/1985年.md "wikilink")：[非洲前线国家首脑会议在](../Page/非洲前线国家首脑会议.md "wikilink")[卢萨卡举行](../Page/卢萨卡.md "wikilink")。
  - [1996年](../Page/1996年.md "wikilink")：中國大陆於[台灣海峡進行第一轮](../Page/台灣海峡.md "wikilink")[飛彈演習](../Page/1996年台湾海峡导弹危机.md "wikilink")。
  - [2000年](../Page/2000年.md "wikilink")：[日本](../Page/日本.md "wikilink")[東京都發生](../Page/東京都.md "wikilink")[營團地下鐵日比谷線事故](../Page/營團地下鐵日比谷線事故.md "wikilink")。

### 21世紀

  - [2008年](../Page/2008年.md "wikilink")：[2008年馬來西亞第十二屆全國大選](../Page/2008年马来西亚大选.md "wikilink")，執政联盟[國陣再度執政](../Page/國陣.md "wikilink")[馬來西亞下议院](../Page/馬來西亞下议院.md "wikilink")，但是却在本届大选中失去五个州政权和三分之二席位的优势。
  - [2013年](../Page/2013年.md "wikilink")：[朝鮮祖國和平統一委員會宣佈將全面廢除](../Page/朝鮮祖國和平統一委員會.md "wikilink")[朝](../Page/朝鮮.md "wikilink")[韩之間簽訂的互不侵犯協議](../Page/南韓.md "wikilink")，切斷南北韓熱線電話等板門店聯絡渠道。
  - [2014年](../Page/2014年.md "wikilink")：[马来西亚航空370号班机在执行由](../Page/马来西亚航空370号班机.md "wikilink")[吉隆坡国际机场飞往](../Page/吉隆坡国际机场.md "wikilink")[北京首都国际机场航线时于马来西亚当地时间凌晨](../Page/北京首都国际机场.md "wikilink")1时21分在[馬來西亞與](../Page/馬來西亞.md "wikilink")[越南的航空管制區內失去與塔台的聯繫](../Page/越南.md "wikilink")，之后該機折返回馬來西亞，并在馬來西亞[霹靂島上空失去衛星聯絡訊號](../Page/霹靂島.md "wikilink")，至今下落不明。

## 出生

  - [1514年](../Page/1514年.md "wikilink")：[尼子晴久](../Page/尼子晴久.md "wikilink")，[日本戰國時代的](../Page/日本戰國時代.md "wikilink")[大名](../Page/大名.md "wikilink")（逝於[1561年](../Page/1561年.md "wikilink")）
  - [1859年](../Page/1859年.md "wikilink")：[肯尼思·格拉姆](../Page/肯尼思·格拉姆.md "wikilink"),
    是一位[英國](../Page/英國.md "wikilink")[作家](../Page/作家.md "wikilink")，以經典兒童文學[柳林中的風聲](../Page/柳林中的風聲.md "wikilink")（The
    Wind in the Willows,
    1908）而聞名於世。(逝於[1932年](../Page/1932年.md "wikilink")）
  - [1881年](../Page/1881年.md "wikilink")：[張作相](../Page/張作相.md "wikilink"),[中華民國軍事將領](../Page/中華民國.md "wikilink")，前[奉系要人](../Page/奉系.md "wikilink")。(卒於[1949年](../Page/1949年.md "wikilink"))
  - [1907年](../Page/1907年.md "wikilink")：[康斯坦丁·卡拉曼利斯](../Page/康斯坦丁·卡拉曼利斯.md "wikilink")，前[希臘首相](../Page/希臘.md "wikilink")、总理和总统（逝於[1998年](../Page/1998年.md "wikilink")）
  - [1914年](../Page/1914年.md "wikilink")：[雅可夫·泽尔多维奇](../Page/雅可夫·泽尔多维奇.md "wikilink")，前苏联的理论天体物理学家（逝於[1987年](../Page/1987年.md "wikilink")）
  - [1921年](../Page/1921年.md "wikilink")：[劉殿爵](../Page/劉殿爵.md "wikilink")，[香港學者](../Page/香港.md "wikilink")、教授（逝於[2010年](../Page/2010年.md "wikilink")）
  - [1922年](../Page/1922年.md "wikilink")：[水木茂](../Page/水木茂.md "wikilink")，[日本漫畫家](../Page/日本.md "wikilink")、漫画《[鬼太郎](../Page/鬼太郎.md "wikilink")》作者（逝於[2015年](../Page/2015年.md "wikilink")）
  - [1930年](../Page/1930年.md "wikilink")：[韓達德](../Page/韓達德.md "wikilink")，前[英國](../Page/英國.md "wikilink")[外務大臣](../Page/外務及英聯邦事務大臣.md "wikilink")
  - [1937年](../Page/1937年.md "wikilink")：[朱韋納爾·哈比亞利馬納](../Page/朱韋納爾·哈比亞利馬納.md "wikilink")，前[盧旺達總統](../Page/盧旺達總統.md "wikilink")（逝於[1994年](../Page/1994年.md "wikilink")）
  - [1957年](../Page/1957年.md "wikilink")：[堀江美都子](../Page/堀江美都子.md "wikilink")，[日本歌手](../Page/日本.md "wikilink")、聲優
  - [1958年](../Page/1958年.md "wikilink")：[重野秀一](../Page/重野秀一.md "wikilink")，日本漫畫家
  - [1959年](../Page/1959年.md "wikilink")：[胡競英](../Page/胡競英.md "wikilink")，[香港女演員](../Page/香港.md "wikilink")、商人
  - [1959年](../Page/1959年.md "wikilink")：[張瓊姿](../Page/張瓊姿.md "wikilink")，[台灣藝人](../Page/台灣.md "wikilink")
  - [1960年](../Page/1960年.md "wikilink")：[王惠五](../Page/王惠五.md "wikilink")，[台灣男演員](../Page/台灣.md "wikilink")
  - [1961年](../Page/1961年.md "wikilink")：[江川達也](../Page/江川達也.md "wikilink")，[日本漫畫家](../Page/日本.md "wikilink")
  - [1962年](../Page/1962年.md "wikilink")：[葉童](../Page/葉童.md "wikilink")，[香港藝人](../Page/香港.md "wikilink")
  - [1965年](../Page/1965年.md "wikilink")：[越田哲弘](../Page/越田哲弘.md "wikilink")，[日本漫畫家](../Page/日本.md "wikilink")
  - [1969年](../Page/1969年.md "wikilink")：[林智賢](../Page/林智賢.md "wikilink")，[台灣藝人](../Page/台灣.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink")：[笠原留美](../Page/笠原留美.md "wikilink")，[日本聲優](../Page/日本.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink")：[櫻井和壽](../Page/櫻井和壽.md "wikilink")，[日本音樂家](../Page/日本.md "wikilink")
  - [1971年](../Page/1971年.md "wikilink")：[錢薇娟](../Page/錢薇娟.md "wikilink")，[臺灣籃球运动員](../Page/臺灣.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[尼諾·馬查伊澤](../Page/尼諾·馬查伊澤.md "wikilink")，[喬治亞女高音歌唱家](../Page/喬治亞.md "wikilink")、歌劇演員
  - [1984年](../Page/1984年.md "wikilink")：[萨沙·武贾西奇](../Page/萨沙·武贾西奇.md "wikilink")，[美國職籃](../Page/美國職籃.md "wikilink")[NBA球員](../Page/NBA.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[朱芯儀](../Page/朱芯儀_\(演藝人員\).md "wikilink")，台灣女演員
  - [1987年](../Page/1987年.md "wikilink")：[李佳航](../Page/李佳航.md "wikilink")，[中國大陸男演員](../Page/中國.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[天海翼](../Page/天海翼.md "wikilink")，[日本AV女優](../Page/日本.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[佩特拉·克維托娃](../Page/佩特拉·克維托娃.md "wikilink")，[捷克職業網球女運動員](../Page/捷克.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[童菲](../Page/童菲.md "wikilink")，[中國大陸女演員](../Page/中國.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[尹智聖](../Page/尹智聖.md "wikilink")，[韓國男子偶像團體](../Page/韓國.md "wikilink")[Wanna
    One](../Page/Wanna_One.md "wikilink") 隊長
  - [1994年](../Page/1994年.md "wikilink")：[高地優吾](../Page/高地優吾.md "wikilink")，日本艺人、[小傑尼斯成员](../Page/小傑尼斯.md "wikilink")
  - [1995年](../Page/1995年.md "wikilink")：[小嶋菜月](../Page/小嶋菜月.md "wikilink")，[日本女子偶像團體](../Page/日本.md "wikilink")[AKB48成員](../Page/AKB48.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink")：[松井珠理奈](../Page/松井珠理奈.md "wikilink")，[日本女子偶像團體](../Page/日本.md "wikilink")[SKE48成員](../Page/SKE48.md "wikilink")
  - [1998年](../Page/1998年.md "wikilink")：[小松由佳](../Page/小松由佳_\(声优\).md "wikilink")，[日本配音演员](../Page/日本.md "wikilink")、舞台演員

## 逝世

  - [1641年](../Page/1641年.md "wikilink")：[徐霞客](../Page/徐霞客.md "wikilink")，[明朝地理學家](../Page/明朝.md "wikilink")（生於[1587年](../Page/1587年.md "wikilink")）
  - [1702年](../Page/1702年.md "wikilink")：[威廉三世](../Page/威廉三世_\(英格蘭\).md "wikilink")，[奧蘭治親王](../Page/奧蘭治親王.md "wikilink")、[荷蘭執政和](../Page/荷蘭.md "wikilink")[英國國王](../Page/英國國王.md "wikilink")（生於[1650年](../Page/1650年.md "wikilink")）
  - [1869年](../Page/1869年.md "wikilink")：[白遼士](../Page/白遼士.md "wikilink")，[法國作曲家](../Page/法國.md "wikilink")（生於[1803年](../Page/1803年.md "wikilink")）
  - [1874年](../Page/1874年.md "wikilink")：[米勒德·菲爾莫爾](../Page/米勒德·菲爾莫爾.md "wikilink")，第13任[美國總統](../Page/美國總統.md "wikilink")（生於[1800年](../Page/1800年.md "wikilink")）
  - [1930年](../Page/1930年.md "wikilink")：[威廉·霍華德·塔夫脫](../Page/威廉·霍華德·塔夫脫.md "wikilink")，第27任美國總統（生於[1857年](../Page/1857年.md "wikilink")）
  - [1935年](../Page/1935年.md "wikilink")：[阮玲玉](../Page/阮玲玉.md "wikilink")，[中國電影明星](../Page/中國電影.md "wikilink")（生於[1910年](../Page/1910年.md "wikilink")）
  - [1975年](../Page/1975年.md "wikilink")：[周信芳](../Page/周信芳.md "wikilink")，京劇表演藝術家（生於[1895年](../Page/1895年.md "wikilink")）
  - [2017年](../Page/2017年.md "wikilink")：[李元簇](../Page/李元簇.md "wikilink")，[中華民國第八任](../Page/中華民國.md "wikilink")[副總統](../Page/副總統.md "wikilink")（生於[1923年](../Page/1923年.md "wikilink")）

## 节日、风俗习惯

  - [国际妇女节](../Page/国际妇女节.md "wikilink")

## 參考資料