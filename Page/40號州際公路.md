**40號州際公路**（****，簡稱**I-40**）是[美國](../Page/美國.md "wikilink")[州際公路系統的一部份](../Page/州際公路系統.md "wikilink")。西起[加利福尼亞州](../Page/加利福尼亞州.md "wikilink")[巴斯托](../Page/巴斯托_\(加利福尼亞州\).md "wikilink")（與[15號州際公路交匯](../Page/15號州際公路.md "wikilink")），東在[北卡羅萊納州](../Page/北卡羅萊納州.md "wikilink")[威爾明頓與](../Page/威爾明頓_\(北卡羅萊納州\).md "wikilink")[美國國道117和](../Page/美國國道117.md "wikilink")[北卡羅萊納州公路132交匯](../Page/北卡羅萊納州公路132.md "wikilink")。全長。

## 里程及經過主要城市

<table>
<thead>
<tr class="header">
<th><p>所經州份</p></th>
<th><p>里程數[1]</p></th>
<th><p>所經主要都市<br />
<small>粗體化的城鎮為使用在交通標誌上的正式指定定點城市</small></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>英里</p></td>
<td><p>公里</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/加利福尼亞州.md" title="wikilink">加利福尼亞州</a></p></td>
<td><p>154.61</p></td>
<td><p>248.82</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/亞歷桑那州.md" title="wikilink">亞歷桑那州</a></p></td>
<td><p>359.48</p></td>
<td><p>578.53</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/新墨西哥州.md" title="wikilink">新墨西哥州</a></p></td>
<td><p>373.51</p></td>
<td><p>601.11</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/德克薩斯州.md" title="wikilink">德克薩斯州</a></p></td>
<td><p>177.10</p></td>
<td><p>285.11</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奧克拉荷馬州.md" title="wikilink">奧克拉荷馬州</a></p></td>
<td><p>331.03</p></td>
<td><p>532.74</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿肯色州.md" title="wikilink">阿肯色州</a></p></td>
<td><p>284.69</p></td>
<td><p>458.16</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/田納西州.md" title="wikilink">田納西州</a></p></td>
<td><p>455.28</p></td>
<td><p>732.70</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/北卡羅萊納州.md" title="wikilink">北卡羅萊納州</a></p></td>
<td><p>419.40</p></td>
<td><p>674.96</p></td>
</tr>
<tr class="even">
<td><p>全長</p></td>
<td><p>2,555.10</p></td>
<td><p>4,112.03</p></td>
</tr>
</tbody>
</table>

## 參考資料

<div class="references-small">

<references />

</div>

[Category:40号州际公路](../Category/40号州际公路.md "wikilink")
[Category:州際公路系統](../Category/州際公路系統.md "wikilink")
[Category:66号美国国道](../Category/66号美国国道.md "wikilink")

1.