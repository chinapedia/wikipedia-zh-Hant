**4月1日**是公历年中的第91天（[闰年的第](../Page/闰年.md "wikilink")92天），离全年结束还有274天。

## 大事记

### 6世紀

  - [528年](../Page/528年.md "wikilink")：已故[北魏孝明帝](../Page/北魏.md "wikilink")[元诩之女](../Page/魏孝明帝.md "wikilink")[元氏被祖母胡太后](../Page/元氏_\(魏孝明帝女\).md "wikilink")（[宣武靈皇后](../Page/宣武靈皇后.md "wikilink")）以先帝[皇子身份拥立为](../Page/皇子.md "wikilink")[皇帝](../Page/中国皇帝.md "wikilink")，是为[中国历史上第一位称帝的](../Page/中国历史.md "wikilink")[女性](../Page/女性.md "wikilink")，但不被后世普遍承认，当日即被废黜，次日由[元钊即位](../Page/元钊.md "wikilink")。

### 12世紀

  - [1130年](../Page/1130年.md "wikilink")：南宋初期，[锺相乘着地方官逃遁的机会发动起义](../Page/锺相.md "wikilink")。

### 14世紀

  - [1356年](../Page/1356年.md "wikilink")：[朱元璋攻克集庆](../Page/朱元璋.md "wikilink")（今[南京市](../Page/南京市.md "wikilink")）。
  - [1357年](../Page/1357年.md "wikilink")：[元朝末年](../Page/元朝.md "wikilink")[紅巾軍](../Page/紅巾軍.md "wikilink")[刘福通部将领](../Page/刘福通.md "wikilink")[毛贵攻下](../Page/毛貴_\(元朝\).md "wikilink")[莱州](../Page/莱州_\(古代\).md "wikilink")。

### 16世紀

  - [1572年](../Page/1572年.md "wikilink")：[荷兰独立战争开始](../Page/八十年戰爭.md "wikilink")。

### 17世紀

  - [1605年](../Page/1605年.md "wikilink")：[良十一世成為天主教教皇](../Page/良十一世.md "wikilink")。

### 19世紀

  - [1826年](../Page/1826年.md "wikilink")：[美国发明家](../Page/美国.md "wikilink")获得[内燃机的](../Page/内燃机.md "wikilink")[专利权](../Page/专利.md "wikilink")。
  - [1867年](../Page/1867年.md "wikilink")：[新加坡成为](../Page/新加坡.md "wikilink")[英国的](../Page/英国.md "wikilink")[直轄殖民地](../Page/直轄殖民地.md "wikilink")。
  - [1873年](../Page/1873年.md "wikilink")：[英国班轮](../Page/英国.md "wikilink")在[加拿大東部](../Page/加拿大.md "wikilink")[新斯科舍半岛外沉没](../Page/新斯科舍半岛.md "wikilink")，547人遇难。

### 20世紀

  - [1905年](../Page/1905年.md "wikilink")：。

  - [1906年](../Page/1906年.md "wikilink")：[中國](../Page/中國.md "wikilink")[京汉铁路全线通车](../Page/京广铁路.md "wikilink")。

  - [1906年](../Page/1906年.md "wikilink")：[南满洲铁道株式会社](../Page/南滿洲鐵道.md "wikilink")（简称满铁）正式营业。满铁是日本在中国东北的侵略工具。

  - [1912年](../Page/1912年.md "wikilink")：[孫中山卸任](../Page/孫中山.md "wikilink")[临时大总统职](../Page/中華民國大總統.md "wikilink")。

  - [1913年](../Page/1913年.md "wikilink")：[福特公司引入](../Page/福特汽车.md "wikilink")[流水生产线大规模生产](../Page/大量生產.md "wikilink")[福特T型车](../Page/福特T型车.md "wikilink")。

  - [1923年](../Page/1923年.md "wikilink")：中国[大连的电话全面自动化](../Page/大连.md "wikilink")。

  - [1924年](../Page/1924年.md "wikilink")：[阿道夫·希特勒因](../Page/阿道夫·希特勒.md "wikilink")[啤酒館政變被判五年囚刑](../Page/啤酒館政變.md "wikilink")。

  - [1927年](../Page/1927年.md "wikilink")：[蒋介石与](../Page/蒋介石.md "wikilink")[汪精卫密谈反共](../Page/汪精卫.md "wikilink")[清党](../Page/中国国民党清党.md "wikilink")。

  - [1930年](../Page/1930年.md "wikilink")：[阎锡山](../Page/阎锡山.md "wikilink")、[冯玉祥](../Page/冯玉祥.md "wikilink")、[李宗仁宣誓讨蒋](../Page/李宗仁.md "wikilink")，[中原大戰爆发在即](../Page/中原大戰.md "wikilink")。

  - [1931年](../Page/1931年.md "wikilink")：[国民党开始第二次](../Page/中國國民黨.md "wikilink")[围剿](../Page/中央苏区反围剿战爭.md "wikilink")[中央苏区](../Page/中央革命根据地.md "wikilink")。

  - [1932年](../Page/1932年.md "wikilink")：[马占山再度抗日](../Page/马占山.md "wikilink")。

  - [1933年](../Page/1933年.md "wikilink")：[纳粹组织了抵制犹太人商店的活动](../Page/纳粹党.md "wikilink")，标志着[納粹德國开始针对](../Page/納粹德國.md "wikilink")[犹太人的迫害](../Page/犹太人.md "wikilink")。

  - [1937年](../Page/1937年.md "wikilink")：[亞丁成为英国直辖殖民地](../Page/亞丁_\(也門\).md "wikilink")。

  - [1938年](../Page/1938年.md "wikilink")：[美国](../Page/美国.md "wikilink")[通用电气公司发明](../Page/通用电气.md "wikilink")[螢光燈](../Page/螢光燈.md "wikilink")。

  - 1938年：[雀巢公司推出](../Page/雀巢.md "wikilink")[雀巢咖啡](../Page/雀巢咖啡.md "wikilink")。

  - [1939年](../Page/1939年.md "wikilink")：[弗朗西斯科·佛朗哥宣布](../Page/弗朗西斯科·佛朗哥.md "wikilink")[西班牙内战结束](../Page/西班牙内战.md "wikilink")。

  - 1939年：[英国和](../Page/英国.md "wikilink")[波兰签订互助条约](../Page/波兰.md "wikilink")。

  - 1940年：中華民國國民政府通令全國，尊稱孫中山為為中華民國 國父

  - [1940年](../Page/1940年.md "wikilink")：晋西北[八路军反](../Page/八路军.md "wikilink")[扫荡获胜利](../Page/掃蕩.md "wikilink")。

  - [1943年](../Page/1943年.md "wikilink")：[中国远征军昆明训练中心开学](../Page/中国远征军.md "wikilink")。

  - [1945年](../Page/1945年.md "wikilink")：[二戰](../Page/第二次世界大战.md "wikilink")[沖繩島戰役](../Page/沖繩島戰役.md "wikilink")：[美军登陸](../Page/美军.md "wikilink")[日本](../Page/日本.md "wikilink")[沖繩島](../Page/沖繩島.md "wikilink")。

  - [1946年](../Page/1946年.md "wikilink")：[阿留申群島发生里氏](../Page/阿留申群島.md "wikilink")8.1级[地震並引起](../Page/1946年阿留申群島地震.md "wikilink")[海啸](../Page/海啸.md "wikilink")，在[夏威夷群島造成](../Page/夏威夷群島.md "wikilink")159人死亡\[1\]。

  - 1946年：[马来亚联邦成立](../Page/马来亚联邦.md "wikilink")。

  - [1948年](../Page/1948年.md "wikilink")：[柏林危机](../Page/柏林危机.md "wikilink")：[東德政府开始封锁](../Page/東德.md "wikilink")[西柏林](../Page/西柏林.md "wikilink")。

  - 1948年：[法罗群岛从](../Page/法罗群岛.md "wikilink")[丹麦获得自治权](../Page/丹麦.md "wikilink")。

  - 1948年：[毛泽东提出](../Page/毛泽东.md "wikilink")[新民主主义革命总路线和总政策](../Page/新民主主义.md "wikilink")。

  -
  - 1949年：[纽芬兰加入](../Page/紐芬蘭與拉布拉多.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")。

  - 1949年：[爱尔兰共和国成立](../Page/爱尔兰共和国.md "wikilink")。

  - [1950年](../Page/1950年.md "wikilink")：[中央美术学院成立](../Page/中央美术学院.md "wikilink")。

  - 1950年：中国与[印度建交](../Page/印度.md "wikilink")。

  - [1955年](../Page/1955年.md "wikilink")：[賽普勒斯爆发反对英国统治的起义](../Page/賽普勒斯.md "wikilink")，要求独立。

  - [1957年](../Page/1957年.md "wikilink")：[西德第一次征兵](../Page/西德.md "wikilink")，成立联邦德国军队。

  - [1958年](../Page/1958年.md "wikilink")：中国第一座[新石器时代文化遗址](../Page/新石器时代.md "wikilink")[博物馆开放](../Page/西安半坡博物館.md "wikilink")，此博物馆建立在[陕西](../Page/陕西省.md "wikilink")[西安附近的](../Page/西安市.md "wikilink")[半坡村](../Page/半坡村.md "wikilink")。

  - [1959年](../Page/1959年.md "wikilink")：[北京](../Page/北京市.md "wikilink")—[平壤](../Page/平壤.md "wikilink")[国际航线正式通航](../Page/国际航线.md "wikilink")。

  - [1960年](../Page/1960年.md "wikilink")：[美国发射第一颗](../Page/美国.md "wikilink")[氣象衛星](../Page/氣象衛星.md "wikilink")[TIROS-1](../Page/TIROS-1.md "wikilink")。

  - [1964年](../Page/1964年.md "wikilink")：[日本解除](../Page/日本.md "wikilink")[外汇限制](../Page/外汇限制.md "wikilink")。

  - [1969年](../Page/1969年.md "wikilink")：[中国共产党第九次全国代表大会举行](../Page/中国共产党第九次全国代表大会.md "wikilink")。

  - [1976年](../Page/1976年.md "wikilink")：[-{zh-tw:賈伯斯; zh-hans:乔布斯;
    zh-hk:喬布斯;}-和](../Page/史蒂夫·乔布斯.md "wikilink")[-{zh-hant:沃茲尼克;
    zh-hans:沃兹尼亚克;}-在美国](../Page/斯蒂夫·沃兹尼亚克.md "wikilink")[加利福尼亚州成立](../Page/加利福尼亚州.md "wikilink")[蘋果公司](../Page/蘋果公司.md "wikilink")。

  - [1978年](../Page/1978年.md "wikilink")：[國際貨幣基金組織](../Page/國際貨幣基金組織.md "wikilink")《[牙买加协定](../Page/牙买加协定.md "wikilink")》正式生效。

  - 1978年：中国[国务院批准恢复和增设五十五所高校](../Page/中华人民共和国国务院.md "wikilink")。

  - [1979年](../Page/1979年.md "wikilink")：[伊朗举行全民公决](../Page/伊朗.md "wikilink")，[通过在伊朗建立](../Page/伊朗伊斯蘭革命.md "wikilink")[伊斯兰](../Page/伊斯兰教.md "wikilink")[共和制政体的国家](../Page/共和制.md "wikilink")。

  - [1980年](../Page/1980年.md "wikilink")：中国开始发行[外汇兑换券](../Page/中国银行外汇兑换券.md "wikilink")。

  - [1983年](../Page/1983年.md "wikilink")：[北京市政府决定每年](../Page/北京.md "wikilink")4月1日至7日为“[爱鸟週](../Page/爱鸟週.md "wikilink")”。

  - [1985年](../Page/1985年.md "wikilink")：《[中华人民共和国专利法](../Page/中华人民共和国专利法.md "wikilink")》实施。

  - [1986年](../Page/1986年.md "wikilink")：中国作家[王蒙走马上任](../Page/王蒙_\(作家\).md "wikilink")[中华人民共和国文化部部长](../Page/中华人民共和国文化部.md "wikilink")。

  - [1987年](../Page/1987年.md "wikilink")：[日本國有鐵道](../Page/日本國有鐵道.md "wikilink")（JNR）[分割為為七間公司](../Page/國鐵分割民營化.md "wikilink")（合稱[JR](../Page/JR.md "wikilink")），將原本國有的經營權移轉為民營。

  - [1988年](../Page/1988年.md "wikilink")：[臺灣](../Page/臺灣.md "wikilink")“[中國小姐](../Page/中國小姐.md "wikilink")”选拔恢复。

  - [1992年](../Page/1992年.md "wikilink")：中国第一部《[进出境动植物检疫站](../Page/进出境动植物检疫站.md "wikilink")》施行。

  - [1995年](../Page/1995年.md "wikilink")：中国首批全国“[青年文明号](../Page/青年文明号.md "wikilink")”命名授牌大会在[北京举行](../Page/北京.md "wikilink")。

  - [1997年](../Page/1997年.md "wikilink")：[中国实施历史上](../Page/中国.md "wikilink")[第一次铁路大提速](../Page/中国铁路大提速.md "wikilink")。

  - 1997年：[-{zh-cn:口袋妖怪; zh-tw:神奇寶貝;
    zh-hk:寵物小精靈;}-系列动画片开始在](../Page/精灵宝可梦系列.md "wikilink")[東京電視台播出](../Page/東京電視台.md "wikilink")。

### 21世紀

  - [2001年](../Page/2001年.md "wikilink")：[中美撞机事件](../Page/中美撞机事件.md "wikilink")：一架被怀疑在进行间谍侦查任务的[美国](../Page/美国.md "wikilink")[EP-3E侦察机在](../Page/EP-3E侦察机.md "wikilink")[中国领空附近与中国一架战斗机相撞](../Page/中华人民共和国.md "wikilink")，中方飞机失事，美方飞机迫降[海南岛](../Page/海南岛.md "wikilink")，机上24名美军被拘留，[中美关系陷入低潮](../Page/中美关系.md "wikilink")。
  - 2001年：前[南斯拉夫联盟](../Page/南斯拉夫联盟共和国.md "wikilink")[总统](../Page/南斯拉夫統治者列表.md "wikilink")[-{zh-hans:米洛舍维奇;zh-hk:米洛舍維奇;zh-tw:米洛塞維奇;}-因涉嫌滥用职权等罪被](../Page/斯洛博丹·米洛舍维奇.md "wikilink")[塞尔维亚警方逮捕](../Page/塞尔维亚.md "wikilink")。
  - 2001年：[同性婚姻在](../Page/荷兰同性婚姻.md "wikilink")[荷兰獲得法律認同](../Page/荷兰.md "wikilink")，成为世界上第一个承认同性婚姻合法的国家。
  - [2002年](../Page/2002年.md "wikilink")：[荷兰](../Page/荷兰.md "wikilink")[安乐死法律正式生效](../Page/安乐死.md "wikilink")，成为世界上第一个承认安乐死合法的国家。
  - [2003年](../Page/2003年.md "wikilink")：[張國榮在](../Page/張國榮.md "wikilink")[香港文華東方酒店](../Page/香港文華東方酒店.md "wikilink")24楼跳楼自杀。
  - [2004年](../Page/2004年.md "wikilink")：[Google对公众启动](../Page/Google.md "wikilink")[Gmail服务](../Page/Gmail.md "wikilink")。
  - [2007年](../Page/2007年.md "wikilink")：日本[新潟县](../Page/新潟县.md "wikilink")[新潟市](../Page/新潟市.md "wikilink")、[靜岡縣](../Page/靜岡縣.md "wikilink")[滨松市升格为](../Page/滨松市.md "wikilink")[政令指定都市](../Page/政令指定都市.md "wikilink")。
  - [2009年](../Page/2009年.md "wikilink")：[克罗地亚及](../Page/克罗地亚.md "wikilink")[阿尔巴尼亚加入](../Page/阿尔巴尼亚.md "wikilink")[北大西洋公约组织](../Page/北大西洋公约组织.md "wikilink")。
  - [2011年](../Page/2011年.md "wikilink")：[東京都青少年健全育成條例自主部份實施](../Page/東京都青少年健全育成條例.md "wikilink")。
  - [2015年](../Page/2015年.md "wikilink")：兩架隸屬於的[F-18戰鬥機因機械故障因素而](../Page/F/A-18黃蜂式戰鬥攻擊機.md "wikilink")[降落在台灣台南空軍基地](../Page/2015年美戰機降落臺南事件.md "wikilink")，並進行兩天的維修。此為美國與[中華民國斷交後](../Page/中華民國.md "wikilink")，首次有美軍戰機飛抵台灣。
  - [2015年](../Page/2015年.md "wikilink")：[亞洲電視不獲](../Page/亞洲電視.md "wikilink")[行政長官會同行政會議續牌](../Page/行政長官會同行政會議.md "wikilink")，成為[香港電視廣播史上首家未能續牌的電視台](../Page/香港電視廣播.md "wikilink")。
  - [2015年](../Page/2015年.md "wikilink")：[马来西亚政府正式落实](../Page/马来西亚政府.md "wikilink")[消费税制度](../Page/消费税.md "wikilink")，其税率为百分之六。
  - [2016年](../Page/2016年.md "wikilink")：[亞洲電視在](../Page/亞洲電視.md "wikilink")[香港本地](../Page/香港.md "wikilink")[免費電視節目服務牌照](../Page/香港電視廣播.md "wikilink")[有效期屆滿](../Page/亞洲電視欠薪及不獲續牌事件.md "wikilink")。
  - [2017年](../Page/2017年.md "wikilink")：[中国共产党中央委员会](../Page/中共中央.md "wikilink")、[中华人民共和国国务院决定设立](../Page/国务院.md "wikilink")[雄安新区](../Page/雄安新区.md "wikilink")。
  - [2019年](../Page/2019年.md "wikilink")：日本公布即将于当年5月1日启用的新年号为[令和](../Page/令和.md "wikilink")。\[2\]

## 出生

  - [1578年](../Page/1578年.md "wikilink")：[威廉·哈维](../Page/威廉·哈维.md "wikilink")，[英国生理学家](../Page/英国.md "wikilink")。（[1657年逝世](../Page/1657年.md "wikilink")）
  - [1776年](../Page/1776年.md "wikilink")：[索菲·熱爾曼](../Page/索菲·熱爾曼.md "wikilink")，[法國](../Page/法國.md "wikilink")[數學家](../Page/數學.md "wikilink")。（[1831年逝世](../Page/1831年.md "wikilink")）
  - [1809年](../Page/1809年.md "wikilink")：[果戈理](../Page/果戈理.md "wikilink")，[俄国](../Page/俄国.md "wikilink")[作家](../Page/作家.md "wikilink")。（[1852年逝世](../Page/1852年.md "wikilink")）
  - [1815年](../Page/1815年.md "wikilink")：[奥托·冯·俾斯麦](../Page/奥托·冯·俾斯麦.md "wikilink")，[普鲁士王国首相](../Page/普鲁士王国.md "wikilink")，[德意志帝国总理](../Page/德意志帝国.md "wikilink")（[1898年逝世](../Page/1898年.md "wikilink")）。
  - [1861年](../Page/1861年.md "wikilink")：[加藤友三郎](../Page/加藤友三郎.md "wikilink")，[日本首相](../Page/日本內閣總理大臣.md "wikilink")、[日本海軍元帥](../Page/日本海軍.md "wikilink")。（[1923年逝世](../Page/1923年.md "wikilink")）
  - [1873年](../Page/1873年.md "wikilink")（[格里曆](../Page/格里曆.md "wikilink")）：[-{zh-hant:拉赫曼尼諾夫;
    zh-cn:拉赫玛尼诺夫;}-](../Page/谢尔盖·瓦西里耶维奇·拉赫玛尼诺夫.md "wikilink")，[俄國](../Page/俄罗斯.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")、[指揮家及](../Page/指揮家.md "wikilink")[鋼琴演奏家](../Page/鋼琴.md "wikilink")（[1943年逝世](../Page/1943年.md "wikilink")）。
  - [1908年](../Page/1908年.md "wikilink")：[亚伯拉罕·马斯洛](../Page/亚伯拉罕·马斯洛.md "wikilink")，[美国心理學家](../Page/美国.md "wikilink")（[1970年逝世](../Page/1970年.md "wikilink")）。
  - [1920年](../Page/1920年.md "wikilink")：[三船敏郎](../Page/三船敏郎.md "wikilink")，[日本演員](../Page/日本.md "wikilink")。（[1997年病逝](../Page/1997年.md "wikilink")）。
  - [1924年](../Page/1924年.md "wikilink")：[邵恩新](../Page/邵恩新.md "wikilink")，前[台北縣長](../Page/台北縣長.md "wikilink")。（[2014年病逝](../Page/2014年.md "wikilink")）。
  - [1929年](../Page/1929年.md "wikilink")：[米兰·昆德拉](../Page/米兰·昆德拉.md "wikilink")，[捷克裔](../Page/捷克.md "wikilink")[法國作家](../Page/法国.md "wikilink")。
  - [1932年](../Page/1932年.md "wikilink")：[德琵·雷諾](../Page/德琵·雷諾.md "wikilink")，[美國女演員](../Page/美國.md "wikilink")（[2016年逝世](../Page/2016年.md "wikilink")）。
  - [1940年](../Page/1940年.md "wikilink")：[馬如龍](../Page/馬如龍_\(演員\).md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")。
  - [1948年](../Page/1948年.md "wikilink")：[柯安龍](../Page/柯安龍.md "wikilink")，[英國](../Page/英国.md "wikilink")[外交官](../Page/外交官.md "wikilink")，前[駐台貿易文化辦事處處長](../Page/英國在台辦事處.md "wikilink")、駐[菲律賓大使](../Page/菲律宾.md "wikilink")、駐[新加坡高級專員](../Page/新加坡.md "wikilink")、駐[紐約總領事](../Page/纽约.md "wikilink")。
  - [1950年](../Page/1950年.md "wikilink")：[塞繆爾·阿利托](../Page/塞繆爾·阿利托.md "wikilink")，現任[美國最高法院大法官](../Page/美國最高法院大法官.md "wikilink")。
  - [1976年](../Page/1976年.md "wikilink")：[大衛·奧伊羅](../Page/大衛·奧伊羅.md "wikilink")，[英國男演員](../Page/英國.md "wikilink")。
  - [1977年](../Page/1977年.md "wikilink")：[毛孩](../Page/毛孩_\(演員\).md "wikilink")，[中国大陆演員](../Page/中国大陆.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[黃玉榮](../Page/黃玉榮.md "wikilink")，台湾艺人。
  - [1980年](../Page/1980年.md "wikilink")：[紀俊麟](../Page/紀俊麟.md "wikilink")，[台灣棒球選手](../Page/台灣.md "wikilink")。
  - [1980年](../Page/1980年.md "wikilink")：[竹內結子](../Page/竹內結子.md "wikilink")，[日本女](../Page/日本.md "wikilink")[演員](../Page/演員.md "wikilink")。
  - [1981年](../Page/1981年.md "wikilink")：[陳茵媺](../Page/陳茵媺.md "wikilink")，[香港演员](../Page/香港.md "wikilink")。
  - [1981年](../Page/1981年.md "wikilink")：[朴藝珍](../Page/朴藝珍.md "wikilink")，[韓國女](../Page/大韩民国.md "wikilink")[演員](../Page/演員.md "wikilink")。
  - [1981年](../Page/1981年.md "wikilink")：[KEI](../Page/KEI.md "wikilink")，[日本](../Page/日本.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")、[人物設計師](../Page/人物設計師.md "wikilink")。
  - [1981年](../Page/1981年.md "wikilink")：[劉心悠](../Page/劉心悠.md "wikilink")，[香港演員](../Page/香港.md "wikilink")。
  - [1982年](../Page/1982年.md "wikilink")：[張小平](../Page/張小平.md "wikilink")，[中國男子](../Page/中國.md "wikilink")[拳擊運動員](../Page/拳擊.md "wikilink")。
  - [1982年](../Page/1982年.md "wikilink")：[山姆·亨亭頓](../Page/山姆·亨亭頓.md "wikilink")，[美國男演員](../Page/美國.md "wikilink")。
  - [1983年](../Page/1983年.md "wikilink")：[馬特·蘭特爾](../Page/馬特·蘭特爾.md "wikilink")，[美國男演員](../Page/美國.md "wikilink")。
  - [1984年](../Page/1984年.md "wikilink")：[王勝偉](../Page/王勝偉.md "wikilink")，台灣棒球選手。
  - [1986年](../Page/1986年.md "wikilink")：[宮本駿一](../Page/宮本駿一.md "wikilink")，[日本](../Page/日本.md "wikilink")[歌手](../Page/歌手.md "wikilink")、動畫聲優。
  - [1987年](../Page/1987年.md "wikilink")：[李婷](../Page/李婷_\(跳水運動員\).md "wikilink")，[中國](../Page/中國.md "wikilink")[跳水運動員](../Page/跳水.md "wikilink")。
  - [1988年](../Page/1988年.md "wikilink")：[馬琇荃](../Page/馬琇荃.md "wikilink")，[香港電視主播](../Page/香港.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[丁海寅](../Page/丁海寅.md "wikilink")，[韓國男](../Page/大韩民国.md "wikilink")[演員](../Page/演員.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：[岡本圭人](../Page/岡本圭人.md "wikilink")，[日本演員](../Page/日本.md "wikilink")、歌手。
  - [1993年](../Page/1993年.md "wikilink")：[柴田阿彌](../Page/柴田阿弥.md "wikilink")，日本女子偶像团体[SKE48成員](../Page/SKE48.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[吳采臻](../Page/吳采臻.md "wikilink")，[台灣女藝人](../Page/臺灣.md "wikilink")。

## 逝世

  - [1085年](../Page/1085年.md "wikilink")：[宋神宗赵顼](../Page/宋神宗.md "wikilink")，[北宋皇帝](../Page/北宋.md "wikilink")。（[1048年出生](../Page/1048年.md "wikilink")）
  - [1282年](../Page/1282年.md "wikilink")：[阿八哈](../Page/阿八哈.md "wikilink")，[蒙古帝国](../Page/蒙古帝国.md "wikilink")[伊兒汗國君主](../Page/伊兒汗國.md "wikilink")。（[1234年出生](../Page/1234年.md "wikilink")）
  - [1939年](../Page/1939年.md "wikilink")：[安东·马卡连柯](../Page/安东·马卡连柯.md "wikilink")，[苏联](../Page/苏联.md "wikilink")[教育学家](../Page/教育.md "wikilink")。（[1888年出生](../Page/1888年.md "wikilink")）
  - [1955年](../Page/1955年.md "wikilink")：[林徽因](../Page/林徽因.md "wikilink")，[中國著名](../Page/中國.md "wikilink")[建筑师](../Page/建筑师.md "wikilink")、[诗人](../Page/诗人.md "wikilink")。[中华人民共和国](../Page/中华人民共和国.md "wikilink")[国徽和](../Page/国徽.md "wikilink")[人民英雄纪念碑的设计者之一](../Page/人民英雄纪念碑.md "wikilink")。（[1904年出生](../Page/1904年.md "wikilink")）
  - [1960年](../Page/1960年.md "wikilink")：[端姑阿都·拉曼](../Page/端姑阿都·拉曼.md "wikilink")，[马来亚](../Page/马来亚联合邦.md "wikilink")（现今的[马来西亚](../Page/马来西亚.md "wikilink")）第一任[最高元首](../Page/马来西亚最高元首.md "wikilink")。（[1895年出生](../Page/1895年.md "wikilink")）
  - [1968年](../Page/1968年.md "wikilink")：[列夫·-{zh-tw:蘭道;zh-cn:朗道}-](../Page/朗道.md "wikilink")，苏联[物理学家](../Page/物理学家.md "wikilink")。（[1908年出生](../Page/1908年.md "wikilink")）
  - [1991年](../Page/1991年.md "wikilink")：[瑪莎·葛蘭姆](../Page/瑪莎·葛蘭姆.md "wikilink")，[美國編舞家](../Page/美國.md "wikilink")，現代[舞蹈史上最早的創始人之一](../Page/舞蹈.md "wikilink")。（[1894年出生](../Page/1894年.md "wikilink")）
  - [2001年](../Page/2001年.md "wikilink")：[王伟](../Page/王偉_\(飛行員\).md "wikilink")，中国飞行员。美侦察机在南海上空撞毁中国军用飞机身亡。（[1968年出生](../Page/1968年.md "wikilink")）
  - [2001年](../Page/2001年.md "wikilink")：[鄭公山](../Page/鄭公山.md "wikilink")，[越南著名](../Page/越南.md "wikilink")[现代音乐](../Page/现代音乐.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")。（[1939年出生](../Page/1939年.md "wikilink")）
  - [2003年](../Page/2003年.md "wikilink")：[張國榮](../Page/張國榮.md "wikilink")，[香港藝人](../Page/香港.md "wikilink")、[歌手](../Page/歌手.md "wikilink")。（[1956年出生](../Page/1956年.md "wikilink")）
  - [2012年](../Page/2012年.md "wikilink")：[張美瑤](../Page/張美瑤.md "wikilink")，[台灣女演員](../Page/台灣.md "wikilink")。（[1941年出生](../Page/1941年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[陳招娣](../Page/陳招娣.md "wikilink")，[中國著名女](../Page/中國.md "wikilink")[排球运动員](../Page/排球.md "wikilink")。（[1955年出生](../Page/1955年.md "wikilink")）
  - 2016年：[梅可望](../Page/梅可望.md "wikilink")，台灣教育家，曾任[中央警官學校](../Page/中央警察大學.md "wikilink")、[東海大學校長](../Page/東海大學_\(台灣\).md "wikilink")。（[1918年出生](../Page/1918年.md "wikilink")）
  - [2017年](../Page/2017年.md "wikilink")：[叶夫根尼·叶夫图申科](../Page/叶夫根尼·叶夫图申科.md "wikilink")，[苏联和俄罗斯诗人](../Page/苏联.md "wikilink")（[1933年出生](../Page/1933年.md "wikilink")）

## 节日、风俗习惯

  - 在很多国家，4月1日是[愚人节](../Page/愚人节.md "wikilink")（[天主教国家的愚人节在](../Page/天主教.md "wikilink")12月28日）。

  - ：[主計節](../Page/主計節.md "wikilink")。

  - [羅馬帝國](../Page/羅馬帝國.md "wikilink")：[维纳斯节](../Page/维纳斯.md "wikilink")（Veneralia）。

  - ：开学、开工日。

  - ：南部[布里勒城庆祝战胜](../Page/布里勒.md "wikilink")[西班牙军](../Page/西班牙.md "wikilink")。

  - ：大议会选举产生两名执政官。

## 參考資料

1.  [Historic
    Earthquakes](http://earthquake.usgs.gov/earthquakes/states/events/1946_04_01.php)

2.