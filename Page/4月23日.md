**4月23日**在一年当中是第113天（[闰年则是](../Page/闰年.md "wikilink")114天），距离一年的结束还有252天。

## 大事记

### 4世紀

  - [303年](../Page/303年.md "wikilink")：[圣乔治日](../Page/圣乔治日.md "wikilink")，相传當天[罗马](../Page/罗马.md "wikilink")[骑兵军官](../Page/骑兵.md "wikilink")[圣乔治因试图阻止](../Page/圣乔治.md "wikilink")[罗马皇帝](../Page/罗马皇帝.md "wikilink")[戴克里先对](../Page/戴克里先.md "wikilink")[基督徒的迫害](../Page/基督徒.md "wikilink")，而被[砍头处死](../Page/砍头.md "wikilink")。

### 14世紀

  - [1348年](../Page/1348年.md "wikilink")：[英国国王](../Page/英国国王.md "wikilink")[爱德华三世在](../Page/爱德华三世.md "wikilink")[圣乔治日宣布正式成立](../Page/圣乔治日.md "wikilink")[嘉德骑士团](../Page/嘉德骑士团.md "wikilink")。

### 17世紀

  - [1661年](../Page/1661年.md "wikilink")：[查理二世在](../Page/查理二世_\(英国\).md "wikilink")[威斯敏斯特教堂加冕成为](../Page/威斯敏斯特教堂.md "wikilink")[英格兰](../Page/英格兰.md "wikilink")、[苏格兰和](../Page/苏格兰.md "wikilink")[爱尔兰国王](../Page/爱尔兰.md "wikilink")。

### 18世紀

  - [1732年](../Page/1732年.md "wikilink")：[台灣發生以](../Page/台灣.md "wikilink")[吳福生為首的](../Page/吳福生.md "wikilink")[民變](../Page/民變.md "wikilink")，即[吳福生事件](../Page/吳福生事件.md "wikilink")。

### 19世紀

  - [1827年](../Page/1827年.md "wikilink")：[爱尔兰数学家](../Page/爱尔兰.md "wikilink")、物理学家[哈密顿发表论文](../Page/威廉·卢云·哈密顿.md "wikilink")《光线系统理论》，建立了[光学的数学理论](../Page/光学.md "wikilink")。
  - [1853年](../Page/1853年.md "wikilink")：[俄国占领](../Page/俄国.md "wikilink")[库页岛](../Page/库页岛.md "wikilink")。
  - [1873年](../Page/1873年.md "wikilink")：[荷兰向](../Page/荷兰.md "wikilink")[亚齐开战](../Page/亚齐.md "wikilink")。
  - [1875年](../Page/1875年.md "wikilink")：清廷任命[左宗棠为](../Page/左宗棠.md "wikilink")[钦差大臣](../Page/钦差大臣.md "wikilink")，督办[新疆军务](../Page/新疆.md "wikilink")。
  - [1895年](../Page/1895年.md "wikilink")：[三国干涉还辽](../Page/三国干涉还辽.md "wikilink")：在[俄罗斯](../Page/俄罗斯.md "wikilink")、[德国和](../Page/德国.md "wikilink")[法国的干涉下](../Page/法国.md "wikilink")，[日本将](../Page/日本.md "wikilink")《马关条约》中清廷割让的辽东半岛“归还”中国。

### 20世紀

  - [1910年](../Page/1910年.md "wikilink")：[汪精卫等谋刺](../Page/汪精卫.md "wikilink")[清朝摄政王](../Page/清朝.md "wikilink")[载沣未遂被捕](../Page/载沣.md "wikilink")。
  - [1920年](../Page/1920年.md "wikilink")：首届[土耳其大国民议会在](../Page/土耳其大国民议会.md "wikilink")[安卡拉召开](../Page/安卡拉.md "wikilink")，以[凯末尔为首的临时政府成立](../Page/穆斯塔法·凯末尔·阿塔土克.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：[中国共产党第七次全国代表大会在](../Page/中国共产党第七次全国代表大会.md "wikilink")[陕西](../Page/陕西.md "wikilink")[延安召开](../Page/延安.md "wikilink")。
  - [1949年](../Page/1949年.md "wikilink")：[第二次国共内战](../Page/第二次国共内战.md "wikilink")：[中国人民解放军在](../Page/中国人民解放军.md "wikilink")[渡江战役中占领](../Page/渡江战役.md "wikilink")[中華民國首都](../Page/中華民國.md "wikilink")[南京](../Page/南京.md "wikilink")。
  - 1949年：[中国人民解放军海军在](../Page/中国人民解放军海军.md "wikilink")[江苏](../Page/江苏.md "wikilink")[泰州](../Page/泰州.md "wikilink")[白馬廟成立](../Page/白馬廟.md "wikilink")。
  - [1975年](../Page/1975年.md "wikilink")：[锡金被并入](../Page/锡金.md "wikilink")[印度](../Page/印度.md "wikilink")。
  - 1975年：[越南战争](../Page/越南战争.md "wikilink")：[越南共和国政府总辞职](../Page/越南共和国.md "wikilink")，[美国承认战争已失败](../Page/美国.md "wikilink")。一星期後[越共攻陷](../Page/越南共產黨.md "wikilink")[西貢](../Page/胡志明市.md "wikilink")，南越滅亡。
  - [1982年](../Page/1982年.md "wikilink")：[美国](../Page/美国.md "wikilink")[基韦斯特市市长宣布](../Page/基韋斯特_\(佛羅里達州\).md "wikilink")[佛羅里達群島从](../Page/佛羅里達群島.md "wikilink")[美国独立](../Page/美国.md "wikilink")，建立[海螺共和国](../Page/海螺共和国.md "wikilink")，并在一分钟后向[美国海军投降](../Page/美国海军.md "wikilink")。
  - [1990年](../Page/1990年.md "wikilink")：[尼泊爾警察騷亂](../Page/尼泊爾.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：[俄羅斯](../Page/俄羅斯.md "wikilink")[總統](../Page/俄羅斯總統.md "wikilink")[葉利欽公布新憲法草案](../Page/葉利欽.md "wikilink")。

### 21世紀

  - [2001年](../Page/2001年.md "wikilink")：一批支持[車臣獨立的槍手攻入](../Page/車臣.md "wikilink")[土耳其](../Page/土耳其.md "wikilink")[伊斯坦布爾一間](../Page/伊斯坦布爾.md "wikilink")[酒店脅持](../Page/酒店.md "wikilink")[人質](../Page/人質.md "wikilink")，最終被鎮壓。
  - [2003年](../Page/2003年.md "wikilink")：[北京由于](../Page/北京.md "wikilink")[SARS疫情而关闭所有学校两周](../Page/SARS疫情.md "wikilink")。
  - 2003年：[香港警務處](../Page/香港警務處.md "wikilink")[水警總區的](../Page/水警總區.md "wikilink")[小艇隊與](../Page/小艇隊.md "wikilink")[反走私特遣隊合併成為](../Page/反走私特遣隊.md "wikilink")[小艇分區](../Page/小艇分區.md "wikilink")。
  - [2005年](../Page/2005年.md "wikilink")：第一部[YouTube](../Page/YouTube.md "wikilink")[影片上傳](../Page/影片.md "wikilink")，標題為「[我在動物園](../Page/我在動物園.md "wikilink")」（Me
    at the zoo）。
  - [2008年](../Page/2008年.md "wikilink")：[賽德克族於](../Page/賽德克族.md "wikilink")2008年4月23日正式成為第十四個台灣原住民族。
  - [2009年](../Page/2009年.md "wikilink")：美国[雨燕卫星探测到](../Page/雨燕卫星.md "wikilink")[GRB
    090423所发射的](../Page/GRB_090423.md "wikilink")[伽玛射线暴](../Page/伽玛射线暴.md "wikilink")，这是当时太空探测历史上所测得最为遥远的伽玛射线纪录。
  - [2018年](../Page/2018年.md "wikilink")：加拿大多倫多客貨車撞向人群，增至10人死亡，15人受傷。
  - 2018年：英國[劍橋公爵夫人凱薩琳](../Page/劍橋公爵夫人凱薩琳.md "wikilink")（Kate
    Middleton）周一誕下小王子。\[1\]

## 出生

  - [1564年](../Page/1564年.md "wikilink")：[莎士比亚](../Page/莎士比亚.md "wikilink")，[英国](../Page/英国.md "wikilink")[劇作家](../Page/剧作家.md "wikilink")。（[1616年逝世](../Page/1616年.md "wikilink")）
  - [1775年](../Page/1775年.md "wikilink")：[約瑟夫·瑪羅德·威廉·特納](../Page/約瑟夫·瑪羅德·威廉·特納.md "wikilink")，英國[畫家](../Page/畫家.md "wikilink")。
  - [1791年](../Page/1791年.md "wikilink")：[詹姆斯·布坎南](../Page/詹姆斯·布坎南.md "wikilink")，[美國第](../Page/美國.md "wikilink")15任[總統](../Page/美國總統.md "wikilink")。（[1868年逝世](../Page/1868年.md "wikilink")）
  - [1794年](../Page/1794年.md "wikilink")：[魏源](../Page/魏源_\(清朝\).md "wikilink")，[中国](../Page/中国.md "wikilink")[思想家](../Page/思想家.md "wikilink")、[文学家](../Page/文学家.md "wikilink")。（[1856年逝世](../Page/1856年.md "wikilink")）
  - [1823年](../Page/1823年.md "wikilink")：[阿卜杜勒-邁吉德一世](../Page/阿卜杜勒-邁吉德一世.md "wikilink")，[奧斯曼帝國蘇丹](../Page/奧斯曼帝國.md "wikilink")。（[1861年逝世](../Page/1861年.md "wikilink")）
  - [1858年](../Page/1858年.md "wikilink")：[普朗克](../Page/普朗克.md "wikilink")，[德国](../Page/德国.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")。（[1947年逝世](../Page/1947年.md "wikilink")）
  - [1875年](../Page/1875年.md "wikilink")：[上村松園](../Page/上村松園.md "wikilink")，[日本](../Page/日本.md "wikilink")[膠彩畫女画家](../Page/膠彩畫.md "wikilink")。（[1949年逝世](../Page/1949年.md "wikilink")）
  - [1877年](../Page/1877年.md "wikilink")：[廖仲恺](../Page/廖仲恺.md "wikilink")，[中国政治家](../Page/中國.md "wikilink")。（[1925年逝世](../Page/1925年.md "wikilink")）
  - [1891年](../Page/1891年.md "wikilink")：[普罗柯菲耶夫](../Page/普罗柯菲耶夫.md "wikilink")，[苏联](../Page/苏联.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")。（[1953年逝世](../Page/1953年.md "wikilink")）
  - [1899年](../Page/1899年.md "wikilink")：[弗拉基米尔·纳博科夫](../Page/弗拉基米尔·纳博科夫.md "wikilink")，俄国作家。（[1977年逝世](../Page/1977年.md "wikilink")）
  - [1902年](../Page/1902年.md "wikilink")：[哈尔多尔·拉克斯内斯](../Page/哈尔多尔·拉克斯内斯.md "wikilink")，[冰岛作家](../Page/冰岛.md "wikilink")，[1955年](../Page/1955年.md "wikilink")[诺贝尔文学奖得主](../Page/诺贝尔文学奖.md "wikilink")。（[1998年逝世](../Page/1998年.md "wikilink")）
  - [1928年](../Page/1928年.md "wikilink")：[秀兰·邓波儿](../Page/秀兰·邓波儿.md "wikilink")，[美国电影明星](../Page/美国.md "wikilink")，一款無酒精[雞尾酒以她的名字命名](../Page/鸡尾酒.md "wikilink")。（[2014年逝世](../Page/2014年.md "wikilink")）
  - [1939年](../Page/1939年.md "wikilink")：[梁智鴻](../Page/梁智鴻.md "wikilink")，[香港医学界](../Page/香港.md "wikilink")、政界人物。
  - [1945年](../Page/1945年.md "wikilink")：[李焯芬](../Page/李焯芬.md "wikilink")，香港大學教授、學者。
  - [1954年](../Page/1954年.md "wikilink")：[麥可·摩爾](../Page/麥可·摩爾.md "wikilink")，美國[紀錄片](../Page/紀錄片.md "wikilink")[導演](../Page/導演.md "wikilink")。
  - [1957年](../Page/1957年.md "wikilink")：[川井憲次](../Page/川井憲次.md "wikilink")，[日本作曲家](../Page/日本.md "wikilink")。
  - [1960年](../Page/1960年.md "wikilink")：[凡萊麗·柏帝內](../Page/凡萊麗·柏帝內.md "wikilink")，[美國演員](../Page/美国.md "wikilink")
  - [1961年](../Page/1961年.md "wikilink")：[周正毅](../Page/周正毅.md "wikilink")，香港企业家。
  - [1962年](../Page/1962年.md "wikilink")：[約翰·漢納](../Page/約翰·漢納.md "wikilink")，[蘇格蘭演員](../Page/蘇格蘭.md "wikilink")
  - [1963年](../Page/1963年.md "wikilink")：[梁緻盈](../Page/梁緻盈.md "wikilink")，香港配音员（[2001年逝世](../Page/2001年.md "wikilink")）
  - [1968年](../Page/1968年.md "wikilink")：[阿伊莎·賓特·侯賽因](../Page/阿伊莎·賓特·侯賽因公主.md "wikilink")，[約旦公主](../Page/約旦.md "wikilink")。
  - [1969年](../Page/1969年.md "wikilink")：[曼努吉·巴帕依](../Page/曼努吉·巴帕依.md "wikilink")，[印度演員](../Page/印度.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink")：[喬寶寶](../Page/喬寶寶.md "wikilink")，香港外藉藝員。
  - [1970年](../Page/1970年.md "wikilink")：[阿部サダヲ](../Page/阿部サダヲ.md "wikilink")，日本[演員](../Page/演員.md "wikilink")。
  - [1976年](../Page/1976年.md "wikilink")：[森山直太朗](../Page/森山直太朗.md "wikilink")，日本歌手。
  - [1977年](../Page/1977年.md "wikilink")：[約翰·希南](../Page/約翰·希南.md "wikilink")，美國演員、[Hip-Hop歌手](../Page/Hip-hop.md "wikilink")、[職業摔角選手](../Page/職業摔角.md "wikilink")。
  - [1977年](../Page/1977年.md "wikilink")：[约翰·奥利弗](../Page/约翰·奥利弗.md "wikilink")，美國名嘴
  - [1977年](../Page/1977年.md "wikilink")：[凱爾朋·蘇雷什·默迪](../Page/凱爾朋·蘇雷什·默迪.md "wikilink")，[美國演員](../Page/美國.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[周蘇紅](../Page/周蘇紅.md "wikilink")，中國女排队员
  - [1979年](../Page/1979年.md "wikilink")：[潔米·金](../Page/潔米·金.md "wikilink")，美國演員
  - [1982年](../Page/1982年.md "wikilink")：[Hiroyuki](../Page/ヒロユキ.md "wikilink")，日本[漫畫家](../Page/漫畫家.md "wikilink")。
  - [1984年](../Page/1984年.md "wikilink")：[傑西·李·索弗](../Page/傑西·李·索弗.md "wikilink")，美國演員
  - [1986年](../Page/1986年.md "wikilink")：[謝茜嘉·史譚](../Page/謝茜嘉·史譚.md "wikilink")，[加拿大女性模特兒](../Page/加拿大.md "wikilink")。
  - 1986年：[張芯瑜](../Page/張芯瑜.md "wikilink")，[台灣女歌手](../Page/台灣.md "wikilink")。
  - [1988年](../Page/1988年.md "wikilink")：[嚴爵](../Page/嚴爵.md "wikilink")，台灣創作歌手。
  - [1989年](../Page/1989年.md "wikilink")：[瓦伊迪索娃](../Page/瓦伊迪索娃.md "wikilink")，[捷克](../Page/捷克.md "wikilink")[网球运动员](../Page/网球.md "wikilink")。
  - [1995年](../Page/1995年.md "wikilink")：[吉吉·哈蒂德](../Page/吉吉·哈蒂德.md "wikilink")，美國女性模特兒。
  - 1995年：[尹芮珠](../Page/尹芮珠.md "wikilink")，韓國女[演員](../Page/演員.md "wikilink")。
  - [1999年](../Page/1999年.md "wikilink")：[孫彩瑛](../Page/彩瑛.md "wikilink")，[韓國女子偶像團體](../Page/大韩民国.md "wikilink")[TWICE成員](../Page/TWICE.md "wikilink")。
  - [2000年](../Page/2000年.md "wikilink")：[Jeno](../Page/Jeno.md "wikilink")，[韓國男子偶像團體](../Page/韓國.md "wikilink")[NCT成員](../Page/NCT.md "wikilink")。
  - [2018年](../Page/2018年.md "wikilink")：[路易王子](../Page/路易王子_\(劍橋\).md "wikilink")，[英國王子](../Page/英國王室.md "wikilink")。

## 逝世

  - [1200年](../Page/1200年.md "wikilink")：[朱熹](../Page/朱熹.md "wikilink")，[中国思想家](../Page/中国.md "wikilink")、文学家（生於[1130年](../Page/1130年.md "wikilink")）
  - [1616年](../Page/1616年.md "wikilink")：[印卡·加西拉索·德拉維加](../Page/印卡·加西拉索·德拉維加.md "wikilink")，[麥士蒂索人作家](../Page/麥士蒂索人.md "wikilink")（生于[1539年](../Page/1539年.md "wikilink")）
  - [1616年](../Page/1616年.md "wikilink")：[莎士比亚](../Page/莎士比亚.md "wikilink")，英国劇作家。（[儒略历](../Page/儒略历.md "wikilink")，生於[1564年](../Page/1564年.md "wikilink")）
  - 1616年：[塞万提斯](../Page/塞万提斯.md "wikilink")，[西班牙](../Page/西班牙.md "wikilink")[小说家](../Page/小说家.md "wikilink")、[剧作家](../Page/剧作家.md "wikilink")、[诗人](../Page/诗人.md "wikilink")（生於[1547年](../Page/1547年.md "wikilink")）
  - [1850年](../Page/1850年.md "wikilink")：[威廉·華茲華斯](../Page/威廉·華茲華斯.md "wikilink")，英國[浪漫主義詩人](../Page/浪漫主義.md "wikilink")（生於[1770年](../Page/1770年.md "wikilink")）
  - [1896年](../Page/1896年.md "wikilink")：[武训](../Page/武训.md "wikilink")，乞讨财物兴办义学的奇人（生於[1838年](../Page/1838年.md "wikilink")）
  - [1946年](../Page/1946年.md "wikilink")：[夏丏尊](../Page/夏丏尊.md "wikilink")，中國作家，教育家。（生於[1886年](../Page/1886年.md "wikilink")）
  - [1969年](../Page/1969年.md "wikilink")：[郑君里](../Page/郑君里.md "wikilink")，中国电影、话剧艺术家。（生於[1911年](../Page/1911年.md "wikilink")）
  - [1989年](../Page/1989年.md "wikilink")：[胡蝶](../Page/胡蝶.md "wikilink")，中国早期电影明星。（生於[1907年](../Page/1907年.md "wikilink")）
  - [1996年](../Page/1996年.md "wikilink")：[P·L·卓華斯](../Page/P·L·卓華斯.md "wikilink")，[英國](../Page/英國.md "wikilink")[作家](../Page/作家.md "wikilink")，《[瑪麗·包萍](../Page/瑪麗·包萍.md "wikilink")》作者。（生於[1899年](../Page/1899年.md "wikilink")）
  - [1997年](../Page/1997年.md "wikilink")：[黄胄](../Page/黄胄.md "wikilink")，中国国画家。（生於[1925年](../Page/1925年.md "wikilink")）
  - [2005年](../Page/2005年.md "wikilink")：[約翰·米爾斯爵士](../Page/約翰·米爾斯.md "wikilink")，[英國電影演員](../Page/英國電影.md "wikilink")。（生於[1908年](../Page/1908年.md "wikilink")）
  - [2007年](../Page/2007年.md "wikilink")：[黃秉乾](../Page/黃秉乾_\(律師\).md "wikilink")，香港事務律師。（生於[1928年](../Page/1928年.md "wikilink")）
  - 2007年：[葉利欽](../Page/葉利欽.md "wikilink")，[俄羅斯前](../Page/俄羅斯.md "wikilink")[總統](../Page/俄羅斯總統.md "wikilink")。（生於[1931年](../Page/1931年.md "wikilink")）
  - [2009年](../Page/2009年.md "wikilink")：[林尚義](../Page/林尚義.md "wikilink")，前[香港足球員及香港](../Page/香港足球.md "wikilink")[電視](../Page/電視.md "wikilink")[足球評述員](../Page/足球評述員.md "wikilink")。（生於[1934年](../Page/1934年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[穆罕默德·奧馬爾](../Page/穆罕默德·奧馬爾.md "wikilink")，[阿富汗](../Page/阿富汗.md "wikilink")[伊斯蘭教組織](../Page/伊斯蘭教.md "wikilink")[塔利班的領導人](../Page/塔利班.md "wikilink")。（生於[1960年](../Page/1960年.md "wikilink")）
  - [2017年](../Page/2017年.md "wikilink")：[曾守明](../Page/曾守明.md "wikilink")，[香港](../Page/香港.md "wikilink")[無綫電視演員](../Page/無綫電視.md "wikilink")、[特技人](../Page/特技人.md "wikilink")、[皇家香港軍團](../Page/皇家香港軍團.md "wikilink")（義勇軍）軍人。(生於[1958年](../Page/1958年.md "wikilink"))

## 节假日和习俗

  - [世界图书与版权日](../Page/世界图书与版权日.md "wikilink")／[世界閱讀日](../Page/世界閱讀日.md "wikilink")

  - [海螺共和國](../Page/海螺共和國.md "wikilink")：[獨立日](../Page/獨立日.md "wikilink")（[佛羅里達州基韋斯特](../Page/基韋斯特_\(佛羅里達州\).md "wikilink")）

  - 和 ：[國家主權及兒童日](../Page/國家主權及兒童日.md "wikilink")

  - ：[國慶日](../Page/國慶日.md "wikilink")（[圣乔治日](../Page/圣乔治日.md "wikilink")）

## 參考資料

1.  <http://std.stheadline.com/instant/articles/detail/698461-星島日報-國際新聞>