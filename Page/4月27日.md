**4月27日**是[公历一年中的第](../Page/公历.md "wikilink")117天（[闰年第](../Page/闰年.md "wikilink")118天），离全年的结束还有248天。

## 大事记

### 16世紀

  - [1521年](../Page/1521年.md "wikilink")：率领[西班牙船队进行环球航行的探险家](../Page/西班牙.md "wikilink")[麦哲伦在](../Page/斐迪南·麥哲倫.md "wikilink")[菲律宾因介入部落冲突而被当地土著杀死](../Page/菲律宾.md "wikilink")。
  - [1522年](../Page/1522年.md "wikilink")：[比克卡会战](../Page/比克卡会战.md "wikilink")，[神圣罗马帝国军队重创以瑞士雇佣兵为主的](../Page/神圣罗马帝国.md "wikilink")[法国军队](../Page/法国.md "wikilink")。
  - [1526年](../Page/1526年.md "wikilink")：[莫卧儿帝国诞生](../Page/莫卧儿帝国.md "wikilink")。
  - [1565年](../Page/1565年.md "wikilink")：[西班牙殖民者](../Page/西班牙.md "wikilink")率领的军队在[宿霧登陆](../Page/宿务市.md "wikilink")，建立在[菲律宾的首个](../Page/菲律宾.md "wikilink")[殖民地](../Page/殖民地.md "wikilink")。

### 17世紀

  - [1667年](../Page/1667年.md "wikilink")：英国诗人[约翰·弥尔顿将作品](../Page/约翰·弥尔顿.md "wikilink")《[失乐园](../Page/失乐园.md "wikilink")》的[著作权以](../Page/著作权.md "wikilink")10英镑的价格卖给出版商。

### 18世紀

  - [1749年](../Page/1749年.md "wikilink")：[英國](../Page/英國.md "wikilink")[德裔音樂家](../Page/德意志裔人.md "wikilink")[韓德爾的作品](../Page/韓德爾.md "wikilink")《[皇家煙火](../Page/皇家煙火.md "wikilink")》在[倫敦的煙火典禮上首次正式公演](../Page/倫敦.md "wikilink")。

### 19世紀

  - [1810年](../Page/1810年.md "wikilink")：德国作曲家[路德维希·范·贝多芬创作出](../Page/路德维希·范·贝多芬.md "wikilink")[A小调](../Page/A小调.md "wikilink")[回旋曲](../Page/回旋曲.md "wikilink")《[给爱丽丝](../Page/给爱丽丝.md "wikilink")》，之后这首没有公开发表的钢琴曲成为其著名的音乐作品之一。
  - [1813年](../Page/1813年.md "wikilink")：[美国攻陷](../Page/美国.md "wikilink")[安大略首府](../Page/安大略.md "wikilink")[多倫多](../Page/多倫多.md "wikilink")。

### 20世紀

  - [1908年](../Page/1908年.md "wikilink")：[1908年夏季奥林匹克运动会在](../Page/1908年夏季奥林匹克运动会.md "wikilink")[伦敦开幕](../Page/伦敦.md "wikilink")。
  - [1911年](../Page/1911年.md "wikilink")：[中国同盟会在](../Page/中国同盟会.md "wikilink")[黄兴的率领下于](../Page/黄兴.md "wikilink")[广州发动](../Page/广州.md "wikilink")[黄花岗起义](../Page/黄花岗起义.md "wikilink")，攻打[清朝政府](../Page/清朝.md "wikilink")[两广总督衙门](../Page/两广总督.md "wikilink")。
  - [1951年](../Page/1951年.md "wikilink")：[镇压反革命运动](../Page/镇压反革命运动.md "wikilink")：上海一举逮捕8359人，此后每隔数日即枪决一批，少则20-30人，多则近300人，到11月初，半年时间就已处决到将近2000人。
  - [1960年](../Page/1960年.md "wikilink")：[西非國家](../Page/西非.md "wikilink")[多哥獨立](../Page/多哥.md "wikilink")。
  - [1961年](../Page/1961年.md "wikilink")：西非国家[塞拉利昂独立](../Page/塞拉利昂.md "wikilink")。
  - [1978年](../Page/1978年.md "wikilink")：[努尔·穆罕默德·塔拉基发动军事政变](../Page/努尔·穆罕默德·塔拉基.md "wikilink")（[四月革命](../Page/四月革命_\(阿富汗\).md "wikilink")），击毙[阿富汗共和国总统](../Page/阿富汗共和国.md "wikilink")[穆罕默德·達烏德汗](../Page/穆罕默德·達烏德汗.md "wikilink")，夺取政权。
  - [1989年](../Page/1989年.md "wikilink")：北京大学生举行抗议[四二六社论的徒步环北京城游行](../Page/四二六社论.md "wikilink")，又称427大游行。
  - [1992年](../Page/1992年.md "wikilink")：由[塞尔维亚和](../Page/塞尔维亚.md "wikilink")[黑山组成的](../Page/黑山共和國.md "wikilink")[南斯拉夫联邦共和国宣告成立](../Page/南斯拉夫联邦共和国.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：[辜汪會談在](../Page/辜汪會談.md "wikilink")[新加坡举行](../Page/新加坡.md "wikilink")。
  - [1994年](../Page/1994年.md "wikilink")：[南非举行首次](../Page/南非.md "wikilink")[不分种族的全国大选](../Page/南非种族隔离.md "wikilink")，[南非非洲人国民大会赢得大选](../Page/南非非洲人国民大会.md "wikilink")。
  - [1997年](../Page/1997年.md "wikilink")：橫跨[香港](../Page/香港.md "wikilink")[青衣島和](../Page/青衣島.md "wikilink")[馬灣](../Page/馬灣.md "wikilink")，連接[大嶼山](../Page/大嶼山.md "wikilink")[赤鱲角](../Page/赤鱲角.md "wikilink")，全球最長的[公路](../Page/公路.md "wikilink")[鐵路雙用](../Page/鐵路.md "wikilink")[懸索吊橋](../Page/懸索吊橋.md "wikilink")—[青馬大橋落成](../Page/青馬大橋.md "wikilink")。

### 21世紀

  - [2005年](../Page/2005年.md "wikilink")：全球載客量最高的客機[空中巴士A380首航](../Page/空中巴士A380.md "wikilink")
  - [2009年](../Page/2009年.md "wikilink")：美国艾奥瓦州成为全美第三个将[同性婚姻合法化的州份](../Page/同性婚姻.md "wikilink")。
  - 2009年：[三星电子发布](../Page/三星电子.md "wikilink")[三星Galaxy系列的第一代手机](../Page/三星Galaxy系列.md "wikilink")[三星Galaxy
    i7500](../Page/三星Galaxy_i7500.md "wikilink")。
  - 2018年：南北韓領導人在板門店[和平之家舉行](../Page/和平之家.md "wikilink")[2018年南北韓高峰會](../Page/2018年南北韓高峰會.md "wikilink")。

## 出生

  - [1791年](../Page/1791年.md "wikilink")：[萨缪尔·摩尔斯](../Page/萨缪尔·摩尔斯.md "wikilink")，美国发明家。（[1872年逝世](../Page/1872年.md "wikilink")）
  - [1822年](../Page/1822年.md "wikilink")：[尤利西斯·辛普森·格蘭特](../Page/尤利西斯·辛普森·格蘭特.md "wikilink")，美國第十八任總統。（[1885年逝世](../Page/1885年.md "wikilink")）
  - [1856年](../Page/1856年.md "wikilink")：[清穆宗](../Page/清朝.md "wikilink")（[同治帝](../Page/同治帝.md "wikilink")）[爱新觉罗载淳](../Page/爱新觉罗载淳.md "wikilink")。（[1874年逝世](../Page/1874年.md "wikilink")）
  - [1894年](../Page/1894年.md "wikilink")：[林漢河](../Page/林漢河.md "wikilink")，[新加坡](../Page/新加坡.md "wikilink")[醫生及政治家](../Page/醫生.md "wikilink")。（[1983年逝世](../Page/1983年.md "wikilink")）
  - [1910年](../Page/1910年.md "wikilink")：[蒋经国](../Page/蒋经国.md "wikilink")，[中華民國第六、七任總統](../Page/中華民國總統.md "wikilink")，[中國國民黨](../Page/中國國民黨.md "wikilink")[主席](../Page/中國國民黨主席.md "wikilink")，生於[中國](../Page/中國.md "wikilink")[浙江](../Page/浙江.md "wikilink")[奉化](../Page/奉化.md "wikilink")。（[1988年逝世](../Page/1988年.md "wikilink")）
  - [1927年](../Page/1927年.md "wikilink")：[科丽塔·斯科特·金](../Page/科丽塔·斯科特·金.md "wikilink")，[美国民权活动家](../Page/美国.md "wikilink")。（[2006年逝世](../Page/2006年.md "wikilink")）
  - [1931年](../Page/1931年.md "wikilink")：[矢田稔](../Page/矢田稔.md "wikilink")，[日本](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")、[演員](../Page/演員.md "wikilink")。
  - [1947年](../Page/1947年.md "wikilink")：[劉健儀](../Page/劉健儀.md "wikilink")，[香港政界人物](../Page/香港.md "wikilink")
  - [1935年](../Page/1935年.md "wikilink")：[泰奧·安哲羅普洛斯](../Page/泰奧·安哲羅普洛斯.md "wikilink")，[希臘](../Page/希臘.md "wikilink")[電影導演](../Page/電影導演.md "wikilink")。（2012年逝世）
  - [1963年](../Page/1963年.md "wikilink")：[藍潔瑛](../Page/藍潔瑛.md "wikilink")，香港演員（[2018年逝世](../Page/2018年.md "wikilink")）
  - [1966年](../Page/1966年.md "wikilink")：[冨樫義博](../Page/冨樫義博.md "wikilink")，日本[漫畫家](../Page/漫画家.md "wikilink")。
  - [1967年](../Page/1967年.md "wikilink")：[威廉-亞歷山大](../Page/威廉-亞歷山大_\(荷蘭\).md "wikilink")，[荷兰國王](../Page/荷兰国王.md "wikilink")。
  - [1972年](../Page/1972年.md "wikilink")：[寧靜](../Page/寧靜.md "wikilink")，[中国女演員](../Page/中国.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[潘文俊](../Page/潘文俊.md "wikilink")，[香港足球運動員](../Page/香港.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[派崔克·史坦普](../Page/派崔克·史坦普.md "wikilink")，[美國樂手](../Page/美国.md "wikilink")。
  - [1987年](../Page/1987年.md "wikilink")：[鈴木杏](../Page/鈴木杏.md "wikilink")，[日本演員](../Page/日本.md "wikilink")。
  - [1987年](../Page/1987年.md "wikilink")：[王霏霏](../Page/王霏霏.md "wikilink")，[韓國女子團體](../Page/韓國.md "wikilink")[miss
    A成員](../Page/miss_A.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[瑪莎·亨特](../Page/瑪莎·亨特.md "wikilink")，美國女性模特兒。
  - [1995年](../Page/1995年.md "wikilink")：[尼克·基爾喬斯](../Page/尼克·基爾喬斯.md "wikilink")，[澳大利亚](../Page/澳大利亚.md "wikilink")[網球選手](../Page/網球.md "wikilink")。
  - 1995年：[妮琳·吉爾](../Page/妮琳·吉爾.md "wikilink")，英国女性模特兒。

## 逝世

  - [1521年](../Page/1521年.md "wikilink")：[麥哲倫](../Page/麥哲倫.md "wikilink")，[葡萄牙探險家](../Page/葡萄牙.md "wikilink")，為[西班牙政府效力探險](../Page/西班牙.md "wikilink")。（[1480年出生](../Page/1480年.md "wikilink")）
  - [1605年](../Page/1605年.md "wikilink")：[良十一世](../Page/良十一世.md "wikilink")，天主教教皇。（[1535年出生](../Page/1535年.md "wikilink")）
  - [1915年](../Page/1915年.md "wikilink")：[亚历山大·尼古拉耶维奇·斯克里亚宾](../Page/亚历山大·尼古拉耶维奇·斯克里亚宾.md "wikilink")，俄国作曲家、钢琴家。（[1872年出生](../Page/1872年.md "wikilink")）
  - [2009年](../Page/2009年.md "wikilink")：[于承妍](../Page/于承妍.md "wikilink")，韓國女藝人。（[1985年出生](../Page/1985年.md "wikilink")）
  - [2017年](../Page/2017年.md "wikilink")：[林奕含](../Page/林奕含.md "wikilink")，臺灣女作家。（[1991年出生](../Page/1991年.md "wikilink")）
  - [2017年](../Page/2017年.md "wikilink"):[佐田之山晉松](../Page/佐田之山晉松.md "wikilink")，日本[相撲](../Page/相撲.md "wikilink")[橫綱](../Page/橫綱.md "wikilink")

## 节日、风俗习惯

  - ：[國王日](../Page/國王日.md "wikilink")（公共假日）

  - ：[自由日](../Page/自由日_\(南非\).md "wikilink")（公共假日）

## 參考資料