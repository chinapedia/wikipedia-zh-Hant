**4月9日**是[公历一年中的第](../Page/公历.md "wikilink")99天（[闰年第](../Page/闰年.md "wikilink")100天），离全年的结束还有266天。

## 大事记

### 13世紀

  - [1241年](../Page/1241年.md "wikilink")：[蒙古帝国的军队在](../Page/蒙古帝国.md "wikilink")[列格尼卡战役中战胜](../Page/列格尼卡战役.md "wikilink")[波兰和](../Page/波兰.md "wikilink")[军事修士会的联军](../Page/军事修士会.md "wikilink")。

### 15世紀

  - [1413年](../Page/1413年.md "wikilink")：[亨利五世加冕](../Page/亨利五世_\(英格兰\).md "wikilink")[英格兰国王](../Page/英国君主列表.md "wikilink")。

### 19世紀

  - [1865年](../Page/1865年.md "wikilink")：[美利坚联盟国軍队總司令](../Page/美利坚联盟国.md "wikilink")[罗伯特·李向](../Page/罗伯特·李.md "wikilink")[美利坚合众国军队总司令](../Page/美利坚合众国.md "wikilink")[格兰特投降](../Page/尤里西斯·格兰特.md "wikilink")，[南北戰爭结束](../Page/南北戰爭.md "wikilink")。

### 20世紀

  - [1917年](../Page/1917年.md "wikilink")：[加拿大军队在](../Page/加拿大.md "wikilink")[法国维米岭向](../Page/法国.md "wikilink")[德国军队发动全面反击](../Page/德国.md "wikilink")，[维米岭战役爆发](../Page/维米岭战役.md "wikilink")。
  - [1924年](../Page/1924年.md "wikilink")：美國提出[道威斯計劃](../Page/道威斯計劃.md "wikilink")，试图解决[德国](../Page/德国.md "wikilink")[一战赔款问题](../Page/一战.md "wikilink")。
  - [1931年](../Page/1931年.md "wikilink")：[紐約](../Page/紐約市.md "wikilink")[帝國大廈落成](../Page/帝國大廈.md "wikilink")。
  - [1936年](../Page/1936年.md "wikilink")：[周恩來](../Page/周恩來.md "wikilink")、[李克農与](../Page/李克農.md "wikilink")[張學良在](../Page/張學良.md "wikilink")[延安舉行會談](../Page/延安.md "wikilink")，同意一致抗[日](../Page/日本.md "wikilink")。
  - [1940年](../Page/1940年.md "wikilink")：[納粹德國入侵](../Page/納粹德國.md "wikilink")[丹麥和](../Page/丹麥.md "wikilink")[挪威](../Page/挪威.md "wikilink")。
  - [1948年](../Page/1948年.md "wikilink")：全副武裝的[猶太人闖入](../Page/猶太人.md "wikilink")[耶路撒冷](../Page/耶路撒冷.md "wikilink")，對[阿拉伯人聚集區的阿拉伯人進行](../Page/阿拉伯人.md "wikilink")[大屠殺](../Page/代爾亞辛村大屠殺.md "wikilink")。
  - [1959年](../Page/1959年.md "wikilink")：[美国国家航空航天局宣布选中首批](../Page/美国国家航空航天局.md "wikilink")[七名宇航员参与](../Page/水星计划7人.md "wikilink")[水星计划](../Page/水星计划.md "wikilink")。
  - [1961年](../Page/1961年.md "wikilink")：[中华人民共和国](../Page/中华人民共和国.md "wikilink")[乒乓球運動員在](../Page/乒乓球.md "wikilink")[北京舉行的第](../Page/北京.md "wikilink")26届[世界乒乓球锦标赛中獲得男子團體冠軍](../Page/世界乒乓球锦标赛.md "wikilink")，这是中华人民共和国成立以来获得的首个团体体育锦标。
  - [1967年](../Page/1967年.md "wikilink")：[美国](../Page/美国.md "wikilink")[波音公司研发的](../Page/波音公司.md "wikilink")[波音737](../Page/波音737.md "wikilink")[客机进行首次试飞](../Page/客机.md "wikilink")。
  - [1981年](../Page/1981年.md "wikilink")：援助非洲難民國際會議在[日内瓦召開](../Page/日内瓦.md "wikilink")。
  - [1982年](../Page/1982年.md "wikilink")：。
  - [1987年](../Page/1987年.md "wikilink")：第27任[香港總督](../Page/香港總督.md "wikilink")[衛奕信抵港履新](../Page/衛奕信.md "wikilink")。
  - [1991年](../Page/1991年.md "wikilink")：[格魯吉亞獨立](../Page/格魯吉亞.md "wikilink")。
  - 1991年：。
  - [1998年](../Page/1998年.md "wikilink")：[麥加](../Page/麥加.md "wikilink")[朝聖發生人踩人慘劇](../Page/朝覲_\(伊斯蘭教\).md "wikilink")，造成118人死亡。\[1\]
  - [1999年](../Page/1999年.md "wikilink")：[尼日尔发生](../Page/尼日尔.md "wikilink")[政变](../Page/1999年尼日政變.md "wikilink")，总统[易卜拉欣·巴雷·麦纳萨拉被枪杀](../Page/易卜拉欣·巴雷·麦纳萨拉.md "wikilink")。\[2\]

### 21世紀

  - [2003年](../Page/2003年.md "wikilink")：[美伊战争联军占领](../Page/伊拉克战争.md "wikilink")[巴格达](../Page/巴格达.md "wikilink")，[萨达姆·侯赛因的政权解体](../Page/萨达姆·侯赛因.md "wikilink")。
  - [2005年](../Page/2005年.md "wikilink")：[中國](../Page/中國.md "wikilink")[北京爆發大規模](../Page/北京.md "wikilink")[反日遊行](../Page/2005年反日遊行.md "wikilink")。
  - [2006年](../Page/2006年.md "wikilink")：[意大利國會選舉舉行](../Page/2006年意大利國會選舉.md "wikilink")。
  - [2008年](../Page/2008年.md "wikilink")：[香港](../Page/香港.md "wikilink")[廉價航空公司](../Page/廉價航空公司.md "wikilink")[甘泉航空宣佈結業](../Page/甘泉航空.md "wikilink")。
  - [2011年](../Page/2011年.md "wikilink")：第一届全球流行音乐金榜颁奖典礼在[台湾](../Page/台湾.md "wikilink")[台北市的](../Page/台北市.md "wikilink")[小巨蛋举行](../Page/台北小巨蛋.md "wikilink")。
  - [2013年](../Page/2013年.md "wikilink")：[伊朗南部](../Page/伊朗.md "wikilink")[布什爾省發生](../Page/布什爾省.md "wikilink")[芮氏規模](../Page/芮氏地震規模.md "wikilink")6.3地震，造成37人死亡，850人受傷。
  - [2019年](../Page/2019年.md "wikilink")：[日本](../Page/日本.md "wikilink")[航空自衛隊一架由](../Page/航空自衛隊.md "wikilink")[三菱重工生產的](../Page/三菱重工.md "wikilink")[F-35A初號機在](../Page/F-35.md "wikilink")[太平洋夜間航行](../Page/太平洋.md "wikilink")，直接墜毀，是F-35A首度失事\[3\]。

## 出生

  - [1285年](../Page/1285年.md "wikilink")：[元仁宗爱育黎拔力八达](../Page/元仁宗.md "wikilink")，[元朝皇帝](../Page/元朝.md "wikilink")。（[1320年逝世](../Page/1320年.md "wikilink")）
  - [1802年](../Page/1802年.md "wikilink")：[艾里阿斯·隆洛特](../Page/艾里阿斯·隆洛特.md "wikilink")，[芬蘭](../Page/芬蘭.md "wikilink")—[瑞典](../Page/瑞典.md "wikilink")[語言學家](../Page/語言學家.md "wikilink")。（[1884年逝世](../Page/1884年.md "wikilink")）
  - [1821年](../Page/1821年.md "wikilink")：[夏尔·波德莱尔](../Page/夏尔·波德莱尔.md "wikilink")，[法国著名](../Page/法国.md "wikilink")[诗人](../Page/诗人.md "wikilink")，[现代派](../Page/现代派.md "wikilink")[诗歌的先驱](../Page/诗歌.md "wikilink")，象征主义文学的鼻祖。（[1867年逝世](../Page/1867年.md "wikilink")）
  - [1865年](../Page/1865年.md "wikilink")：[埃里希·魯登道夫](../Page/埃里希·魯登道夫.md "wikilink")，[德國](../Page/德國.md "wikilink")[軍事家](../Page/軍事家.md "wikilink")。（[1937年逝世](../Page/1937年.md "wikilink")）
  - [1869年](../Page/1869年.md "wikilink")：[埃利·嘉当](../Page/埃利·嘉当.md "wikilink")，法国[数学家](../Page/数学家.md "wikilink")。（[1951年逝世](../Page/1951年.md "wikilink")）
  - [1928年](../Page/1928年.md "wikilink")：[林家駿](../Page/林家駿.md "wikilink")，[澳門榮休](../Page/澳門.md "wikilink")[主教](../Page/主教.md "wikilink")。（[2009年逝世](../Page/2009年.md "wikilink")）
  - [1930年](../Page/1930年.md "wikilink")：[雷納托·魯傑羅](../Page/雷納托·魯傑羅.md "wikilink")，[義大利政治家](../Page/意大利.md "wikilink")。（[2013年逝世](../Page/2013年.md "wikilink")）
  - [1938年](../Page/1938年.md "wikilink")：[切爾諾梅爾金](../Page/切爾諾梅爾金.md "wikilink")，[俄羅斯政治家](../Page/俄羅斯.md "wikilink")，曾任俄羅斯總理。（[2010年逝世](../Page/2010年.md "wikilink")）
  - [1948年](../Page/1948年.md "wikilink")：[贾雅·巴克罕](../Page/贾雅·巴克罕.md "wikilink")，[印度女演員及政治家](../Page/印度.md "wikilink")。
  - [1950年](../Page/1950年.md "wikilink")：[梁中昀](../Page/梁中昀.md "wikilink")，香港學聯旅遊董事局主席，香港家長協進會主席。（[2006年逝世](../Page/2006年.md "wikilink")）
  - [1954年](../Page/1954年.md "wikilink")：[丹尼斯·奎德](../Page/丹尼斯·奎德.md "wikilink")，[美國男](../Page/美國.md "wikilink")[演員](../Page/演員.md "wikilink")。
  - [1957年](../Page/1957年.md "wikilink")：[塞維·巴列斯特羅斯](../Page/塞維·巴列斯特羅斯.md "wikilink")，[西班牙高爾夫球選手](../Page/西班牙.md "wikilink")。首位贏得奧古塔斯名人賽的歐洲選手。
  - [1960年](../Page/1960年.md "wikilink")：[沈雁](../Page/沈雁.md "wikilink")，[台灣歌手](../Page/台灣歌手.md "wikilink")
  - [1963年](../Page/1963年.md "wikilink")：[馬克·雅各布斯](../Page/馬克·雅各布斯.md "wikilink")，美國時尚設計師。
  - 1963年：[林志美](../Page/林志美.md "wikilink")，香港歌手。
  - [1965年](../Page/1965年.md "wikilink")：[馬克·佩雷格里諾](../Page/馬克·佩雷格里諾.md "wikilink")，美國男演員。
  - [1966年](../Page/1966年.md "wikilink")：[仙菲亞·歷遜](../Page/仙菲亞·歷遜.md "wikilink")，[美國女演員](../Page/美國.md "wikilink")。
  - [1977年](../Page/1977年.md "wikilink")：[李致和](../Page/李致和.md "wikilink")，[香港](../Page/香港.md "wikilink")[三項鐵人運動員](../Page/三項鐵人.md "wikilink")。
  - [1976年](../Page/1976年.md "wikilink")：[陳超明](../Page/陳超明.md "wikilink")，香港電台DJ。
  - [1979年](../Page/1979年.md "wikilink")：[格雷姆·布朗](../Page/格雷姆·布朗.md "wikilink")，[澳大利亞](../Page/澳大利亞.md "wikilink")[自行車](../Page/自行車.md "wikilink")[運動員](../Page/運動員.md "wikilink")。
  - [1980年](../Page/1980年.md "wikilink")：[柔愛娜·侯斯](../Page/柔愛娜·侯斯.md "wikilink")，美國[模特兒](../Page/模特兒.md "wikilink")、《[全美超級模特兒新秀大賽第二季](../Page/全美超級模特兒新秀大賽第二季.md "wikilink")》[冠軍](../Page/冠軍.md "wikilink")。
  - 1980年：[楊睿智](../Page/楊睿智.md "wikilink")，台湾棒球运动员
  - 1980年：[李瑤媛](../Page/李瑤媛.md "wikilink")，[南韓](../Page/南韓.md "wikilink")[演員](../Page/演員.md "wikilink")。
  - [1982年](../Page/1982年.md "wikilink")：[積·巴朗祖](../Page/積·巴朗祖.md "wikilink")，[加拿大男演員](../Page/加拿大.md "wikilink")。
  - [1984年](../Page/1984年.md "wikilink")：[鍾嘉欣](../Page/鍾嘉欣.md "wikilink")，[香港演員](../Page/香港.md "wikilink")、[歌手](../Page/歌手.md "wikilink")。
  - 1984年：[何傲芝](../Page/何傲芝.md "wikilink")，香港主持人
  - [1985年](../Page/1985年.md "wikilink")：[山下智久](../Page/山下智久.md "wikilink")，[日本](../Page/日本.md "wikilink")[藝人](../Page/藝人.md "wikilink")。
  - 1985年：[李懿](../Page/李懿.md "wikilink")，[台灣女藝人](../Page/台灣.md "wikilink")。
  - [1986年](../Page/1986年.md "wikilink")：[麗頓·麥斯達](../Page/麗頓·麥斯達.md "wikilink")，美國女演員。
  - [1987年](../Page/1987年.md "wikilink")：[陳零九](../Page/陳零九.md "wikilink")，臺灣男[歌手](../Page/歌手.md "wikilink")。
  - 1987年：[傑西·麥卡尼](../Page/傑西·麥卡尼.md "wikilink")，美國男[歌星](../Page/歌星.md "wikilink")。
  - 1987年：[薩拉](../Page/薩拉_\(汶萊王儲妃\).md "wikilink")，[汶萊王儲妃](../Page/汶萊.md "wikilink")。
  - [1988年](../Page/1988年.md "wikilink")：[U-ie](../Page/U-ie.md "wikilink")，南韓歌手、演員，女子團體[After
    School成員](../Page/After_School.md "wikilink")
  - 1988年：[絲瓦拉·巴斯卡](../Page/絲瓦拉·巴斯卡.md "wikilink")，[印度演員](../Page/印度.md "wikilink")。
  - [1989年](../Page/1989年.md "wikilink")：[張含韻](../Page/張含韻.md "wikilink")，[中國歌手](../Page/中國.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[克莉絲汀·史都華](../Page/克莉絲汀·史都華.md "wikilink")，美國女[演員](../Page/演員.md "wikilink")。
  - [1992年](../Page/1992年.md "wikilink")：[金有權](../Page/金有權.md "wikilink")，南韓歌手，組合[BLOCK
    B成員](../Page/BLOCK_B.md "wikilink")。
  - [1997年](../Page/1997年.md "wikilink")：[吳澋滔](../Page/吳澋滔.md "wikilink")，香港[童星](../Page/童星.md "wikilink")。
  - [1998年](../Page/1998年.md "wikilink")：[艾兒·芬妮](../Page/艾兒·芬妮.md "wikilink")，美國女[演員](../Page/演員.md "wikilink")。
  - [2003年](../Page/2003年.md "wikilink")：[梁世奇](../Page/梁世奇.md "wikilink")，美籍華裔[国际象棋神童](../Page/國際象棋.md "wikilink")

## 逝世

  - [1483年](../Page/1483年.md "wikilink")：[愛德華四世](../Page/愛德華四世.md "wikilink")，英國國王。（[1442年出生](../Page/1442年.md "wikilink")）
  - [1804年](../Page/1804年.md "wikilink")：[雅克·內克](../Page/雅克·內克.md "wikilink")，[法國](../Page/法國.md "wikilink")[路易十六的財政總監](../Page/路易十六.md "wikilink")（1777～1781、1788～1789、1789～1790）與銀行家。（[1732年出生](../Page/1732年.md "wikilink")）
  - [1997年](../Page/1997年.md "wikilink")：[吳作人](../Page/吳作人.md "wikilink")，中國藝術家和美術教育家。（[1908年出生](../Page/1908年.md "wikilink")）
  - [1999年](../Page/1999年.md "wikilink")：[易卜拉欣·巴雷·邁納薩拉](../Page/易卜拉欣·巴雷·邁納薩拉.md "wikilink")，[尼日爾總統](../Page/尼日爾.md "wikilink")。（[1948年出生](../Page/1948年.md "wikilink")）
  - [2003年](../Page/2003年.md "wikilink")：[吳祖光](../Page/吳祖光.md "wikilink")，中國劇作家。（[1917年出生](../Page/1917年.md "wikilink")）
  - [2006年](../Page/2006年.md "wikilink")：[王啟民](../Page/王啟民.md "wikilink")，中國著名電影藝術家，原長春電影製片廠一級攝影師。（[1921年出生](../Page/1921年.md "wikilink")）
  - [2012年](../Page/2012年.md "wikilink")：[青野武](../Page/青野武.md "wikilink")，[日本](../Page/日本.md "wikilink")[聲優及](../Page/聲優.md "wikilink")[舞台](../Page/舞台劇.md "wikilink")[演員](../Page/演員.md "wikilink")，代表作品包括《[宇宙戰艦大和號](../Page/宇宙戰艦大和號.md "wikilink")》、[櫻桃小丸子友藏爺爺](../Page/櫻桃小丸子.md "wikilink")。（[1936年出生](../Page/1936年.md "wikilink")）

## 節日、風俗習惯

  - ：[國家核技術日](../Page/國家核技術日.md "wikilink")

  - ：[巴丹日](../Page/巴丹日.md "wikilink")

## 參考資料

1.
2.
3.