**5**（五）是[4与](../Page/4.md "wikilink")[6之间的](../Page/6.md "wikilink")[自然数](../Page/自然数.md "wikilink")，是第3個[質數](../Page/質數.md "wikilink")。

## 数学性质

  - [巴都萬數列的第](../Page/巴都萬數列.md "wikilink")8項
  - 是2+3
  - 5和6組成了一對[魯斯·阿倫數對](../Page/魯斯·阿倫數對.md "wikilink")
  - 5出現在兩個[勾股數組之中](../Page/勾股数.md "wikilink")：\(3^2+4^2=5^2\)及\(5^2+12^2=13^2\)
  - 第4個[卡塔蘭數](../Page/卡塔蘭數.md "wikilink")
  - [速算为乘](../Page/速算.md "wikilink")[10](../Page/10.md "wikilink")，再[除以2](../Page/除以二.md "wikilink")（[乘法](../Page/乘法.md "wikilink")，[除法正好相反](../Page/除法.md "wikilink")）

### 基本运算

| [乘法](../Page/乘法.md "wikilink") | 1     | 2                              | 3                              | 4                              | 5                              | 6                              | 7                              | 8                              | 9                              | 10                             |  | 11                             | 12                             | 13                             | 14                             | 15                             | 16                             | 17                             | 18                             | 19                             | 20                               |  | 21                               | 22                               | 23                               | 24                               | 25                               |  | 50                               | 100                              | 1000                               |
| ------------------------------ | ----- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |  | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | -------------------------------- |  | -------------------------------- | -------------------------------- | -------------------------------- | -------------------------------- | -------------------------------- |  | -------------------------------- | -------------------------------- | ---------------------------------- |
| \(5 \times x\)                 | **5** | [10](../Page/10.md "wikilink") | [15](../Page/15.md "wikilink") | [20](../Page/20.md "wikilink") | [25](../Page/25.md "wikilink") | [30](../Page/30.md "wikilink") | [35](../Page/35.md "wikilink") | [40](../Page/40.md "wikilink") | [45](../Page/45.md "wikilink") | [50](../Page/50.md "wikilink") |  | [55](../Page/55.md "wikilink") | [60](../Page/60.md "wikilink") | [65](../Page/65.md "wikilink") | [70](../Page/70.md "wikilink") | [75](../Page/75.md "wikilink") | [80](../Page/80.md "wikilink") | [85](../Page/85.md "wikilink") | [90](../Page/90.md "wikilink") | [95](../Page/95.md "wikilink") | [100](../Page/100.md "wikilink") |  | [105](../Page/105.md "wikilink") | [110](../Page/110.md "wikilink") | [115](../Page/115.md "wikilink") | [120](../Page/120.md "wikilink") | [125](../Page/125.md "wikilink") |  | [250](../Page/250.md "wikilink") | [500](../Page/500.md "wikilink") | [5000](../Page/5000.md "wikilink") |

| [除法](../Page/除法.md "wikilink") | 1     | 2   | 3                  | 4                               | 5 | 6                   | 7                                  | 8     | 9                  | 10  |  | 11                             | 12                   | 13                                 | 14                                  | 15                 |
| ------------------------------ | ----- | --- | ------------------ | ------------------------------- | - | ------------------- | ---------------------------------- | ----- | ------------------ | --- |  | ------------------------------ | -------------------- | ---------------------------------- | ----------------------------------- | ------------------ |
| \(5 \div x\)                   | **5** | 2.5 | \(1.\overline{6}\) | [1](../Page/1.md "wikilink").25 | 1 | \(0.8\overline{3}\) | \(0.\overline{7}1428\overline{5}\) | 0.625 | \(0.\overline{5}\) | 0.5 |  | \(0.\overline{4}\overline{5}\) | \(0.41\overline{6}\) | \(0.\overline{3}8461\overline{5}\) | \(0.3\overline{5}7142\overline{8}\) | \(0.\overline{3}\) |
| \(x \div 5\)                   | 0.2   | 0.4 | 0.6                | 0\.[8](../Page/8.md "wikilink") | 1 | 1.2                 | 1.4                                | 1.6   | 1.8                | 2   |  | 2.2                            | 2.4                  | 2.6                                | 2.8                                 | 3                  |

| [乘方](../Page/乘方.md "wikilink") | 1     | 2                              | 3                                | 4    | 5    | 6     | 7     | 8      | 9       | 10                                     |  | 11       | 12        | 13         |
| ------------------------------ | ----- | ------------------------------ | -------------------------------- | ---- | ---- | ----- | ----- | ------ | ------- | -------------------------------------- |  | -------- | --------- | ---------- |
| \(5 ^ x\,\)                    | **5** | 25                             | 125                              | 625  | 3125 | 15625 | 78125 | 390625 | 1953125 | 9765625                                |  | 48828125 | 244140625 | 1220703125 |
| \(x ^ 5\,\)                    | 1     | [32](../Page/32.md "wikilink") | [243](../Page/243.md "wikilink") | 1024 | 3125 | 7776  | 16807 | 32768  | 59049   | [100000](../Page/100000.md "wikilink") |  | 161051   | 248832    | 371293     |

### 其他相關

  - [正五邊形](../Page/正五邊形.md "wikilink")
  - [柏拉圖立體有](../Page/柏拉圖立體.md "wikilink")5個。
  - [五進制](../Page/五進制.md "wikilink")
  - [五阶魔方](../Page/五阶魔方.md "wikilink")
  - [五子棋](../Page/五子棋.md "wikilink")
  - [五角柱體](../Page/五角柱.md "wikilink")、[五角錐體](../Page/五角錐.md "wikilink")
  - [三角形的五心](../Page/三角形.md "wikilink")：[形心](../Page/形心.md "wikilink")、[外心](../Page/外心.md "wikilink")、[內心](../Page/内切圆.md "wikilink")、[垂心](../Page/垂心.md "wikilink")、[旁心](../Page/旁心.md "wikilink")。

## 在時間與曆法上

  - 在公曆紀年方面，人類對公元[前5年](../Page/前5年.md "wikilink")、公元[5年](../Page/5年.md "wikilink")，公元[前5世纪及公元](../Page/前5世纪.md "wikilink")[5世纪均有記載](../Page/5世纪.md "wikilink")。
  - 現代國際通用的[西曆](../Page/公历.md "wikilink")，則將1[年分成](../Page/年.md "wikilink")12个[月](../Page/月.md "wikilink")。12个月每月長度不一，但都有5日，分別為[1月5日](../Page/1月5日.md "wikilink")、[2月5日](../Page/2月5日.md "wikilink")、[3月5日](../Page/3月5日.md "wikilink")、[4月5日](../Page/4月5日.md "wikilink")、[5月5日](../Page/5月5日.md "wikilink")、[6月5日](../Page/6月5日.md "wikilink")、[7月5日](../Page/7月5日.md "wikilink")、[8月5日](../Page/8月5日.md "wikilink")、[9月5日](../Page/9月5日.md "wikilink")、[10月5日](../Page/10月5日.md "wikilink")、[11月5日和](../Page/11月5日.md "wikilink")[12月5日](../Page/12月5日.md "wikilink")。

## 在科学中

  - [硼的](../Page/硼.md "wikilink")[原子序數為](../Page/原子序數.md "wikilink")5。\[1\]
  - 人有5種[感官](../Page/感官.md "wikilink")：[視覺](../Page/視覺.md "wikilink")、[聽覺](../Page/聽覺.md "wikilink")、[嗅覺](../Page/嗅覺.md "wikilink")、[味覺](../Page/味覺.md "wikilink")、[觸覺](../Page/觸覺.md "wikilink")。
  - 在[幽浮学上把](../Page/幽浮学.md "wikilink")[人与外星生物接触分为五类接触](../Page/人与外星生物接触.md "wikilink")。

## 在自然界中

  - [哺乳類](../Page/哺乳類.md "wikilink")、大多數[爬虫類](../Page/爬虫類.md "wikilink")、[鳥類的手腳有](../Page/鳥類.md "wikilink")5指。
  - [灵长类以四肢都有](../Page/灵长类.md "wikilink")5个趾头作为划分。

## 在歷史中

  - [三皇五帝](../Page/三皇五帝.md "wikilink")
  - [五胡亂華](../Page/五胡亂華.md "wikilink")、[五胡十六國時期](../Page/五胡十六國.md "wikilink")：五胡指[匈奴](../Page/匈奴.md "wikilink")、[鮮卑](../Page/鮮卑.md "wikilink")、[羯](../Page/羯.md "wikilink")、[羌](../Page/羌.md "wikilink")、[氐五個胡人遊牧民族](../Page/氐.md "wikilink")。
  - [五代十國](../Page/五代十國.md "wikilink")：五代指[後梁](../Page/後梁.md "wikilink")、[後唐](../Page/後唐.md "wikilink")、[後晉](../Page/後晉.md "wikilink")、[後漢](../Page/後漢.md "wikilink")、[後周五個國家](../Page/後周.md "wikilink")。

## 在人类文化中

  - [五行思想認為](../Page/五行.md "wikilink")[金](../Page/金.md "wikilink")、[木](../Page/木.md "wikilink")、[水](../Page/水.md "wikilink")、[火](../Page/火.md "wikilink")、[土這](../Page/土.md "wikilink")5種事物是萬物的根本，而[亚里士多德有类似的](../Page/亚里士多德.md "wikilink")“五元素说”，认为世界由水、土、火、[空气](../Page/空气.md "wikilink")、[以太五种元素组成](../Page/以太.md "wikilink")。
  - 五[大洋為](../Page/大洋.md "wikilink")[太平洋](../Page/太平洋.md "wikilink")、[大西洋](../Page/大西洋.md "wikilink")、[印度洋](../Page/印度洋.md "wikilink")、[南冰洋](../Page/南冰洋.md "wikilink")、[北冰洋](../Page/北冰洋.md "wikilink")。
  - [貨幣常用面額](../Page/貨幣.md "wikilink")，如5[歐元](../Page/歐元.md "wikilink")。
  - [儒家的](../Page/儒家.md "wikilink")[五經是](../Page/五經.md "wikilink")《[詩經](../Page/詩經.md "wikilink")》、《[尚书](../Page/尚书_\(经\).md "wikilink")》、《[易经](../Page/易经.md "wikilink")》、《[礼记](../Page/礼记.md "wikilink")》和《[春秋](../Page/春秋_\(史书\).md "wikilink")》。
  - [基督教的](../Page/基督教.md "wikilink")[摩西五經是](../Page/摩西五經.md "wikilink")《[創世記](../Page/創世記.md "wikilink")》、《[出埃及記](../Page/出埃及記.md "wikilink")》、《[利未記](../Page/利未記.md "wikilink")》、《[民數記](../Page/民數記.md "wikilink")》、《[申命記](../Page/申命記.md "wikilink")》。
  - [宗教改革运动时期](../Page/宗教改革.md "wikilink")[路德宗基本神学信仰](../Page/路德宗.md "wikilink")[五个唯独为](../Page/五个唯独.md "wikilink")[唯独恩典](../Page/唯独恩典.md "wikilink")、[唯独信心](../Page/唯独信心.md "wikilink")、[唯独圣经](../Page/唯独圣经.md "wikilink")、[唯独基督](../Page/唯独基督.md "wikilink")、[唯独荣耀上帝](../Page/唯独荣耀上帝.md "wikilink")。
  - 在[樂理中](../Page/樂理.md "wikilink")，[簡譜上的sol音用](../Page/簡譜.md "wikilink")5表示；在[音樂中](../Page/音樂.md "wikilink")，有中國五色[音階和日本五色音階](../Page/音階.md "wikilink")，中國古代之[五音為](../Page/五音.md "wikilink")[宮](../Page/宮.md "wikilink")、[商](../Page/商.md "wikilink")、[角](../Page/角.md "wikilink")、-{[徵](../Page/徵.md "wikilink")}-、[羽](../Page/羽.md "wikilink")。
  - 在[塑膠分類標誌中](../Page/塑膠分類標誌.md "wikilink")，代表[聚丙烯](../Page/聚丙烯.md "wikilink")。
  - [五言律詩](../Page/五言律詩.md "wikilink")、五言絕詩。
  - [13號碰上星期五的日子](../Page/13.md "wikilink")[13號星期五](../Page/13號星期五.md "wikilink")，又稱為[黑色星期五](../Page/黑色星期五.md "wikilink")。
  - [五权宪法中的](../Page/五权宪法.md "wikilink")“五权”指[行政](../Page/行政.md "wikilink")、[立法](../Page/立法.md "wikilink")、[司法](../Page/司法.md "wikilink")、[考試](../Page/考試.md "wikilink")、[監察一共五个政府权力](../Page/監察.md "wikilink")；《[逸周书](../Page/逸周书.md "wikilink")》中的“五权”指[地](../Page/地.md "wikilink")、[物](../Page/物.md "wikilink")、鄙、[刑](../Page/刑.md "wikilink")、[食一共五个统治者治军治国应权衡的事](../Page/食.md "wikilink")；《[汉书](../Page/汉书.md "wikilink")》中的“五权”指[铢](../Page/铢.md "wikilink")、[两](../Page/两.md "wikilink")、[斤](../Page/斤.md "wikilink")、[钧](../Page/钧.md "wikilink")、[石一共五个重量单位](../Page/石.md "wikilink")。
  - [五族共和中的](../Page/五族共和.md "wikilink")“五族”指[汉族](../Page/汉族.md "wikilink")、[满族](../Page/满族.md "wikilink")、[蒙族](../Page/蒙族.md "wikilink")、[回族](../Page/回族.md "wikilink")、[藏族](../Page/藏族.md "wikilink")。
  - [五帝指](../Page/五帝.md "wikilink")[黄帝](../Page/黄帝.md "wikilink")、[颛顼](../Page/颛顼.md "wikilink")、[帝喾](../Page/帝喾.md "wikilink")、[尧](../Page/尧.md "wikilink")、[舜](../Page/舜.md "wikilink")。（《史记·五帝本纪》、《礼记》和《春秋国语》说法）
  - [五常指](../Page/五常.md "wikilink")[仁](../Page/仁.md "wikilink")、[義](../Page/義.md "wikilink")、[禮](../Page/禮.md "wikilink")、[智](../Page/智.md "wikilink")、[信](../Page/信.md "wikilink")；[五育指](../Page/五育.md "wikilink")[德](../Page/德.md "wikilink")、[智](../Page/智.md "wikilink")、[體](../Page/體.md "wikilink")、[群](../Page/群.md "wikilink")、[美](../Page/美.md "wikilink")；[五福指](../Page/五福.md "wikilink")[壽](../Page/壽.md "wikilink")、[富](../Page/富.md "wikilink")、[康寧](../Page/康寧.md "wikilink")、[攸好德](../Page/攸好德.md "wikilink")、[考終命](../Page/考終命.md "wikilink")；[五倫指](../Page/五倫.md "wikilink")[君臣](../Page/君臣.md "wikilink")、[父子](../Page/父子.md "wikilink")、[夫婦](../Page/夫婦.md "wikilink")、[兄弟](../Page/兄弟.md "wikilink")、[朋友](../Page/朋友.md "wikilink")；[五義指](../Page/五義.md "wikilink")[父義](../Page/父義.md "wikilink")、[母慈](../Page/母慈.md "wikilink")、[兄友](../Page/兄友.md "wikilink")、[弟恭](../Page/弟恭.md "wikilink")、[子孝](../Page/子孝.md "wikilink")。
  - [五岳指北岳](../Page/五岳.md "wikilink")[恒山](../Page/恒山.md "wikilink")、南岳[衡山](../Page/衡山.md "wikilink")、西岳[华山](../Page/华山.md "wikilink")、东岳[泰山](../Page/泰山.md "wikilink")、中岳[嵩山](../Page/嵩山.md "wikilink")。
  - [五牲指](../Page/五牲.md "wikilink")[牛](../Page/牛.md "wikilink")、[羊](../Page/羊.md "wikilink")、[豕](../Page/豕.md "wikilink")、[雞](../Page/雞.md "wikilink")、[犬](../Page/犬.md "wikilink")；[五穀是指](../Page/五穀.md "wikilink")[米](../Page/稻.md "wikilink")、[麦](../Page/麦.md "wikilink")、[粟](../Page/粟.md "wikilink")、[黍](../Page/黍.md "wikilink")、[豆](../Page/豆.md "wikilink")；[五味指](../Page/五味.md "wikilink")[酸](../Page/酸.md "wikilink")、[甜](../Page/甜.md "wikilink")、[苦](../Page/苦.md "wikilink")、[辣](../Page/辣.md "wikilink")、[咸](../Page/咸.md "wikilink")；[五虫指毛虫](../Page/五虫.md "wikilink")（哺乳类）、羽虫（鸟类）、鳞虫（鱼类、昆虫类、爬行类）、介虫（甲壳类、两栖类）、倮虫（人类）。
  - [清教徒搭乘](../Page/清教徒.md "wikilink")[五月花號前往](../Page/五月花號.md "wikilink")[美洲大陸](../Page/美洲大陸.md "wikilink")。
  - [奧運会的旗帜图案是由](../Page/奧運会.md "wikilink")**5**個環組合在一起的[奥林匹克五环](../Page/奥林匹克五环.md "wikilink")。（[日本](../Page/日本.md "wikilink")[奧運稱為](../Page/奧運.md "wikilink")[五輪](../Page/五輪.md "wikilink")）
  - [五的大寫](../Page/五.md "wikilink")[伍也是](../Page/伍.md "wikilink")[姓氏](../Page/姓氏.md "wikilink")。
  - 现代经济学领域把[产业分为五级](../Page/产业.md "wikilink")，[第一产业包括一切直接从地球开采资源的行业](../Page/第一产业.md "wikilink")，[第二产业包括所有进行加工的行业](../Page/第二产业.md "wikilink")，[第三产业指一切提供服务的行业](../Page/第三产业.md "wikilink")，[第四产业指为全社会进行学术研究探索和维持秩序的行业](../Page/第四产业.md "wikilink")，[第五产业指文化和创意的行业](../Page/第五产业.md "wikilink")。
  - 英文五個母音為[a](../Page/a.md "wikilink")、[e](../Page/e.md "wikilink")、[i](../Page/i.md "wikilink")、[o](../Page/o.md "wikilink")、[u](../Page/u.md "wikilink")。
  - [日本人認為](../Page/日本.md "wikilink")「5」是吉祥的數字，因為和幸運的日文發音相近 。
  - 中国大陆政治术语中的“[黑五类](../Page/黑五类.md "wikilink")”、“[红五类](../Page/红五类.md "wikilink")”。
  - [香港立法會地方直選分為](../Page/香港立法會.md "wikilink")5大分區，另衍生出[五區公投運動](../Page/五區公投.md "wikilink")。
  - 五恥：居其位，無其言。有其言，無其行。旣得之而又失之。地有餘而民不足。眾寡均而倍焉。

### 中醫學上

  - [五體](../Page/五體.md "wikilink")：[骨](../Page/骨.md "wikilink")、[筋](../Page/筋.md "wikilink")、[脉](../Page/脉.md "wikilink")、[肉](../Page/肉.md "wikilink")、[皮](../Page/皮.md "wikilink")
    [五官](../Page/五官.md "wikilink")：[眼](../Page/眼.md "wikilink")、[耳](../Page/耳.md "wikilink")、[口](../Page/口.md "wikilink")、[鼻](../Page/鼻.md "wikilink")、[舌](../Page/舌.md "wikilink")
    [五臟](../Page/五臟.md "wikilink")：[肝](../Page/肝_\(臟腑\).md "wikilink")、[心](../Page/心_\(臟腑\).md "wikilink")、[脾](../Page/脾_\(臟腑\).md "wikilink")、[肺](../Page/肺_\(臟腑\).md "wikilink")、[腎](../Page/腎_\(臟腑\).md "wikilink")
    [五色](../Page/五色.md "wikilink")：[青](../Page/青.md "wikilink")、[黄](../Page/黄.md "wikilink")、[赤](../Page/红色.md "wikilink")、[白](../Page/白.md "wikilink")、[黑](../Page/黑.md "wikilink")

## 在其它领域中

  - 美國文化中，[High
    five代表了為了成功而擊掌的手勢](../Page/High_five.md "wikilink")，有時也做Give
    me five（直譯為：給我5）
  - [5](../Page/5_\(遊戲\).md "wikilink")，[RAM推出的](../Page/RAM_\(公司\).md "wikilink")[美少女遊戲](../Page/美少女遊戲.md "wikilink")。
  - [5也是一種](../Page/5_Gum.md "wikilink")[口香糖的品牌](../Page/口香糖.md "wikilink")。
  - 根据[英美协定](../Page/英美协定.md "wikilink")，[美国](../Page/美国.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[英国](../Page/英国.md "wikilink")、[澳大利亚](../Page/澳大利亚.md "wikilink")、[新西兰组成](../Page/新西兰.md "wikilink")[五眼联盟共享各项军事与机密情报](../Page/五眼联盟.md "wikilink")。

## 另見

  -
  -
## 参考文献

[Category:整數素數](../Category/整數素數.md "wikilink")

1.  [Royal Society of Chemistry - Visual Element Periodic
    Table](http://www.rsc.org/periodic-table)