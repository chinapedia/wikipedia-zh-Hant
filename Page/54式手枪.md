**五四式手槍**（，又稱**黑星手槍**），是由[中華人民共和國在](../Page/中華人民共和國.md "wikilink")1954年後開始生產的一種軍用[手槍](../Page/手槍.md "wikilink")。

## 設計

在1953年7月[韓戰結束後](../Page/韓戰.md "wikilink")，中華人民共和國和[蘇聯的專家改良了在韓戰中被大量使用的中華人民共和國製造的](../Page/蘇聯.md "wikilink")[51式手槍](../Page/51式手槍.md "wikilink")。1954年，改良後的手槍面世，取名為54式手槍，開始投入工業生產，作為了51式手槍的改進型\[1\]。

## 結構

54式手槍的[槍機運作方式採用](../Page/槍機.md "wikilink")[槍管短行程](../Page/槍管.md "wikilink")[後座作用式](../Page/後座作用.md "wikilink")，閉鎖方式採用槍管擺動式，並設有空槍掛機，而後期型54-1式手槍設有保險。

## 衍生型

  - [51式手槍](../Page/51式手槍.md "wikilink")：[蘇聯製](../Page/蘇聯.md "wikilink")[TT手槍的](../Page/TT手槍.md "wikilink")[仿製品](../Page/仿製品.md "wikilink")。
  - 54-1式手槍：追加了手動保險掣的改良型，有遠較原型高的安全性，猶對一些喜歡**固鎖待發**的射手方便，但因為[中國人民解放軍已經採用](../Page/中國人民解放軍.md "wikilink")[92式手槍](../Page/92式手槍.md "wikilink")，本槍主要供[外銷](../Page/外銷.md "wikilink")。
  - 北方工業213型：外銷版本，使用14發彈匣、具有安全裝置和較薄的握把。此型號除了有[7.62×25mm托卡列夫手槍彈版本外](../Page/7.62×25mm托卡列夫手槍彈.md "wikilink")，還有[9×19毫米魯格版本](../Page/9×19毫米魯格.md "wikilink")。
  - 北方工業M20：沒有工廠標記的54式手槍，[越南人民軍使用的版本](../Page/越南人民軍.md "wikilink")。
  - 北方工業TU-90：[埃及製Tokagypt](../Page/埃及.md "wikilink")
    58手槍的[仿製品](../Page/仿製品.md "wikilink")。

### 优点

  - 所用的子彈貫穿力強大
  - 射程遠
  - 重量輕
  - 成本低廉
  - 握持及攜帶方便
  - 易於裝配和拆卸
  - 可靠性優異
  - 具有威懾力

### 缺点

  - 子彈數少
  - 會出現卡彈現象
  - 性能已經開始比不上現代的新型手槍
  - 人机工效较差

## 採用

54式手槍是中華人民共和國生產和裝備量最大的手槍，除了軍用，也被[警察](../Page/警察.md "wikilink")、現金押運公司及私人軍訓院校廣泛使用。隨著[中國人民解放軍實行裝備現代化計劃](../Page/中國人民解放軍.md "wikilink")，中國人民解放軍陸續換裝新款的[92式手槍](../Page/92式手槍.md "wikilink")，以取代54式手槍。但54式手槍在中華人民共和國[中國人民武裝警察部隊中](../Page/中國人民武裝警察部隊.md "wikilink")，以至[柬埔寨和](../Page/柬埔寨.md "wikilink")[孟加拉等國家仍有使用](../Page/孟加拉.md "wikilink")。

[中國北方工業公司在](../Page/中國北方工業公司.md "wikilink")1992年至1994年間曾經把54-1式手槍出口到[美國](../Page/美國.md "wikilink")。為了更加適應[北美地區市場](../Page/北美地區.md "wikilink")，原來軍用的7.62×25mm托卡列夫手槍彈槍管換成了更加流行的[9×19毫米魯格槍管](../Page/9毫米魯格彈.md "wikilink")。同時為了滿足美國的槍械安全要求，在槍身加上了保險裝置。這款槍械推出的時候價格大約在一百多[美元](../Page/美元.md "wikilink")，由於價廉物美而廣受美國用家歡迎。不到一年半，中華人民共和國生產槍械的[市場佔有率已經達到了全美國總進口量的](../Page/市場.md "wikilink")60%以上。這趨勢簡直震撼了當時的美國民用軍火市場[供應商](../Page/供應商.md "wikilink")。間接導致了1994年[美國總統](../Page/美國總統.md "wikilink")[克林頓為了要遊說國會批准中華人民共和國](../Page/克林頓.md "wikilink")[最惠國待遇](../Page/最惠國待遇.md "wikilink")，而以防止武器擴散為理由，發出了[總統](../Page/總統.md "wikilink")[行政命令](../Page/行政命令_\(美國\).md "wikilink")，禁止中華人民共和國攻擊性武器彈藥進口。

90年代，54式手槍由於容易透過地下[軍火](../Page/軍火.md "wikilink")[市場中取得](../Page/市場.md "wikilink")，成為不少[東亞和](../Page/東亞.md "wikilink")[東南亞地區的匪徒和](../Page/東南亞.md "wikilink")[黑社會用作犯案的槍械](../Page/黑社會.md "wikilink")。每枝54式手槍的[黑市價為數千至萬多元](../Page/黑市.md "wikilink")[人民幣不等](../Page/人民幣.md "wikilink")。在[香港和](../Page/香港.md "wikilink")[台灣警方繳獲的](../Page/台灣.md "wikilink")[非法](../Page/非法.md "wikilink")[槍械中](../Page/槍械.md "wikilink")，黑星手槍佔有一定比例。\[2\]

## 流行文化

  - 54式手槍在不少[TVB劇集和](../Page/TVB.md "wikilink")[80年代](../Page/80年代.md "wikilink")—[90年代的](../Page/90年代.md "wikilink")[香港電影中出現](../Page/香港電影.md "wikilink")，並幾乎跟片中出現的[黑社會和](../Page/黑社會.md "wikilink")[組織犯罪劃上等號](../Page/組織犯罪.md "wikilink")。在《[潛行狙擊](../Page/潛行狙擊.md "wikilink")》中，罪犯羅勝、馬榮森和臥底探員梁笑棠都是使用[54式手槍作案](../Page/54式手槍.md "wikilink")，而在《[法證先鋒III](../Page/法證先鋒III.md "wikilink")》中也有出現並命名為「黑星54」。
  - 由於在槍柄上有一顆黑色的五角星形圖案，54式手槍因此又稱為「黑星手槍」。
  - [日本](../Page/日本.md "wikilink")[輕小說作品](../Page/輕小說.md "wikilink")《[刀劍神域](../Page/刀劍神域.md "wikilink")》中的角色死槍所使用的[手槍就是](../Page/手槍.md "wikilink")54式手槍。
  - 由於54式手槍（及[TT手槍](../Page/TT手槍.md "wikilink")）在[日本收繳的非法槍枝當中佔有一席之地](../Page/日本.md "wikilink")，許多當地以暴力犯罪或黑社會為題材的文藝作品都會出現該槍的影子，並也曾在不少[日本動畫中出現過](../Page/日本動畫.md "wikilink")。

## 使用國

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 參考

  - [TT-33](../Page/TT-33.md "wikilink")
  - [M57手槍](../Page/M57手槍.md "wikilink")

## 參考資料

<div class="references-small">

<references/>

</div>

## 外部連結

  - [中國制手枪竞三杰：54式、64式、84式](https://web.archive.org/web/20070817190915/http://www.pladaily.com.cn/item2/jsbw/jssc/jssc005.htm)

  - [巨龙之爪-54式军用警用手枪](http://sidalin.blog.hexun.com/5430380_d.html)

[Category:半自動手槍](../Category/半自動手槍.md "wikilink")
[Category:中华人民共和国半自动手枪](../Category/中华人民共和国半自动手枪.md "wikilink")
[Category:7.62×25毫米托卡列夫手槍彈槍械](../Category/7.62×25毫米托卡列夫手槍彈槍械.md "wikilink")
[Category:越戰武器](../Category/越戰武器.md "wikilink")

1.  [7.62 mm Soviet Automatic 1933 Tokarev (TT33) (Chinese Pistol
    Type 51)](http://www.rt66.com/~korteng/SmallArms/tt_33.html)
2.  [「非法槍械增
    多為『黑星』」](http://www.hkheadline.com/news/headline_news_detail.asp?id=23266&section_name=wnn)《[頭條日報](../Page/頭條日報.md "wikilink")》，2007年3月29日