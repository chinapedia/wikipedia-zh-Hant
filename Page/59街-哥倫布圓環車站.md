**59街-哥倫布圓環車站**（）是[紐約地鐵一個](../Page/紐約地鐵.md "wikilink")[地鐵站](../Page/地鐵站.md "wikilink")，由[IRT百老匯-第七大道線和](../Page/IRT百老匯-第七大道線.md "wikilink")[IND第八大道線共用](../Page/IND第八大道線.md "wikilink")，是整個系統內第八繁忙的車站，位於[曼哈頓中城](../Page/曼哈頓中城.md "wikilink")[哥倫布圓環](../Page/哥倫布圓環.md "wikilink")59街、[百老匯和](../Page/百老匯.md "wikilink")[第八大道交界](../Page/第八大道_\(曼哈頓\).md "wikilink")，設有以下列車服務：

  - [1號線](../Page/紐約地鐵1號線.md "wikilink")、[A線](../Page/紐約地鐵A線.md "wikilink")、[D線列車](../Page/紐約地鐵D線.md "wikilink")（任何時候停站）
  - [C線列車](../Page/紐約地鐵C線.md "wikilink")（任何時候停站（深夜除外））
  - [B線列車](../Page/紐約地鐵B線.md "wikilink")（僅平日停站）
  - [2號線列車](../Page/紐約地鐵2號線.md "wikilink")（僅深夜停站）

## 車站結構

<table>
<tbody>
<tr class="odd">
<td><p><strong>G</strong></p></td>
<td><p>街道</p></td>
<td><p>出入口</p></td>
</tr>
<tr class="even">
<td><p><strong>B1</strong></p></td>
<td><p>夾層</p></td>
<td><p>閘機，車站詢問處，MetroCard售票機</p>
<hr>
<p>商店，往出口<br />
</p></td>
</tr>
<tr class="odd">
<td><p><small><a href="../Page/側式月台.md" title="wikilink">側式月台</a>，右側開門 </small></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><span style="color:#{{NYCS color|red}}"><strong>北行</strong></span></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-1.svg" title="fig:纽约地铁1号线">纽约地铁1号线</a> 往<a href="../Page/范科特蘭公園-242街車站_(IRT百老匯-第七大道線).md" title="wikilink">范科特蘭公園-242街</a>（深夜時<a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-2.svg" title="fig:纽约地铁2号线">纽约地铁2号线</a> 往<a href="../Page/威克菲爾德-241街車站_(IRT白原路線).md" title="wikilink">241街</a>）<small>（<a href="../Page/66街-林肯中心車站_(IRT百老匯-第七大道線).md" title="wikilink">66街-林肯中心</a>）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><span style="color:#{{NYCS color|red}}"><strong>北行快速</strong></span></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-2.svg" title="fig:纽约地铁2号线">纽约地铁2号线</a> <a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-3.svg" title="fig:纽约地铁3号线">纽约地铁3号线</a> 不停靠</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><span style="color:#{{NYCS color|red}}"><strong>南行快速</strong></span></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-2.svg" title="fig:纽约地铁2号线">纽约地铁2号线</a> <a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-3.svg" title="fig:纽约地铁3号线">纽约地铁3号线</a> 不停靠</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><span style="color:#{{NYCS color|red}}"><strong>南行</strong></span></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-1.svg" title="fig:纽约地铁1号线">纽约地铁1号线</a> 往<a href="../Page/南碼頭車站_(IRT百老匯-第七大道線).md" title="wikilink">南碼頭</a>（深夜時<a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-2.svg" title="fig:纽约地铁2号线">纽约地铁2号线</a> 往<a href="../Page/夫拉特布殊大道-布魯克林學院車站_(IRT諾斯特蘭大道線).md" title="wikilink">布魯克林學院</a>）<small>（<a href="../Page/50街車站_(IRT百老匯-第七大道線).md" title="wikilink">50街</a>）</small></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><small><a href="../Page/側式月台.md" title="wikilink">側式月台</a>，右側開門 </small></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>B2</strong></p></td>
<td><p>IND夾層</p></td>
<td><p>路線和月台之間的轉乘</p></td>
</tr>
<tr class="even">
<td><p><strong>B3</strong></p></td>
<td><p><strong>北行</strong></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-B.svg" title="fig:纽约地铁B线">纽约地铁B线</a> 尖峰時段往<a href="../Page/貝德福德公園林蔭路車站_(IND匯集線).md" title="wikilink">貝德福德公園林蔭路</a>，其餘時段<a href="../Page/145街車站_(IND第八大道線).md" title="wikilink">145街</a><small>（<a href="../Page/72街車站_(IND第八大道線).md" title="wikilink">72街</a>）</small><br />
 <a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-C.svg" title="fig:纽约地铁C线">纽约地铁C线</a> 往<a href="../Page/168街車站_(IND第八大道線).md" title="wikilink">168街</a>（<a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-A.svg" title="fig:纽约地铁A线">纽约地铁A线</a> 深夜時往<a href="../Page/英伍德-207街車站_(IND第八大道線).md" title="wikilink">英伍德-207街</a>）<small>（72街）</small></p></td>
</tr>
<tr class="odd">
<td><p><small><a href="../Page/島式月台.md" title="wikilink">島式月台</a>，普通列車左側開門，快速列車右側開門 </small></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>北行快速</strong></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-A.svg" title="fig:纽约地铁A线">纽约地铁A线</a> 往<a href="../Page/英伍德-207街車站_(IND第八大道線).md" title="wikilink">207街</a><small>（<a href="../Page/125街車站_(IND第八大道線).md" title="wikilink">125街</a>）</small><br />
 <a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-D.svg" title="fig:纽约地铁D线">纽约地铁D线</a> 往<a href="../Page/諾伍德-205街車站_(IND匯集線).md" title="wikilink">諾伍德-205街</a><small>（125街）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><small><a href="../Page/島式月台.md" title="wikilink">島式月台</a>，不使用，作為連接IRT百老匯-第七大道線月台的行人通道</small></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>南行快速</strong></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-A.svg" title="fig:纽约地铁A线">纽约地铁A线</a> 往<a href="../Page/臭氧公園-勒弗茲林蔭路車站_(IND福爾頓街線).md" title="wikilink">勒弗茲林蔭路</a>/<a href="../Page/遠洛克威-莫特大道車站_(IND洛克威線).md" title="wikilink">遠洛克威-莫特大道</a>（黃昏尖峰時段往<a href="../Page/洛克威公園-海灘116街車站_(IND洛克威線).md" title="wikilink">洛克威公園</a>）<small>（<a href="../Page/時報廣場-42街/航港局客運總站車站.md" title="wikilink">42街-航港局客運總站</a>）</small><br />
 <a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-D.svg" title="fig:纽约地铁D线">纽约地铁D线</a> 往<a href="../Page/康尼島-斯提威爾大道車站.md" title="wikilink">康尼島-斯提威爾大道</a><small>（<a href="../Page/第七大道車站_(IND皇后大道線).md" title="wikilink">第七大道</a>）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><small><a href="../Page/島式月台.md" title="wikilink">島式月台</a>，普通列車左側開門，快速列車右側開門 </small></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>南行</strong></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-B.svg" title="fig:纽约地铁B线">纽约地铁B线</a> 往<a href="../Page/布萊頓海灘車站_(BMT布萊頓線).md" title="wikilink">布萊頓海灘</a><small>（第七大道）</small><br />
 <a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-C.svg" title="fig:纽约地铁C线">纽约地铁C线</a> 往<a href="../Page/尤克利德大道車站_(IND福爾頓街線).md" title="wikilink">尤克利德大道</a>（<a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-A.svg" title="fig:纽约地铁A线">纽约地铁A线</a> 深夜時往<a href="../Page/遠洛克威-莫特大道車站_(IND洛克威線).md" title="wikilink">遠洛克威-莫特大道</a>）<small>（<a href="../Page/50街車站_(IND第八大道線).md" title="wikilink">50街</a>）</small></p></td>
<td></td>
</tr>
</tbody>
</table>

{{-}}

## IRT百老匯-第七大道線月台

**59街-哥倫布圓環車站**位於[IRT百老匯-第七大道線](../Page/IRT百老匯-第七大道線.md "wikilink")，於1904年10月27日啟用，設有兩個[島式月台和四條軌道](../Page/島式月台.md "wikilink")。

雖然位於與IND第八大道線的主要轉乘站，但興建時車站只是慢車站，並無考慮1932年興建的IND。[紐約市公共運輸局一度考慮將此站改為快車停靠站](../Page/紐約市公共運輸局.md "wikilink")，並將慢車軌道移動到月台外側\[1\]。然而這樣會導致[72街車站降級為慢車站](../Page/72街車站_\(IRT百老匯-第七大道線\).md "wikilink")，並將島式月台的快車一側以圍欄或牆壁封閉，將此站轉為快車站為興建中的紐約體育場服務\[2\]。

車站啟用是，上下行月台曾設有地下道，1970年代關閉，樓梯入口亦被圍封。現時乘客使用[IND夾層和月台轉乘](../Page/獨立地鐵系統.md "wikilink")，兩個月台於同一層設有兩個閘機，其中一個連接夾層並前往IND月台。
{{-}}

## IND第八大道線月台

**59街-哥倫布圓環車站**位於[IND第八大道線](../Page/IND第八大道線.md "wikilink")，1932年9月10日啟用，是一個大型快車站，設有三個島式月台和四條軌道，當中外側兩條正在營運。

車站以南，列車可繼續前往第八大道或東轉經[IND第六大道線前往](../Page/IND第六大道線.md "wikilink")[第七大道車站](../Page/第七大道車站_\(IND第六大道線\).md "wikilink")。車站以北設有兩個方向的剪式橫渡線，北行軌道跨過南行軌道形成[103街車站的雙層配置](../Page/103街車站_\(IND第八大道線\).md "wikilink")。

北行的下一個快車站[125街車站距離此站](../Page/125街車站_\(IND第八大道線\).md "wikilink")3.35英哩（5.391公里），跳過了七個慢車站，是紐約地鐵當中兩個快車站之間最長的距離。

中央月台於1959年首度作為客運使用，但原初與其他月台一同興建，作為[西班牙式月臺佈局](../Page/西班牙式月臺佈局.md "wikilink")，容許乘客於兩側離開車廂，與快車相同。新式地鐵列車的車門控制令兩側同時開門更為困難，該月台於1973年11月8日關閉。2007－2010年間，其成為IRT側式月台的地下道，並立起了大型金屬圍欄防止乘客走近邊緣。

在各使用中的月台中央設有兩條樓梯和一部升降機，與[IRT百老匯-第七大道線北行月台連接](../Page/IRT百老匯-第七大道線.md "wikilink")。此外各月台北端亦設有一條樓梯前往同一個月台。南端一條樓梯則連接IRT南行月台。此站設有兩個閱報架，分別位於兩個月台的正中央。

## 參考資料

## 延伸閱讀

  - Lee Stokey. *Subway Ceramics : A History and Iconography*. 1994.

## 外部連結

  -
  -
  - nycsubway.org – [Whirls and Twirls Artwork by Sol Lewitt
    (2007)](http://www.nycsubway.org/perl/artwork_show?200)

  - nycsubway.org – [Hello Columbus Artwork by the NYC Artists & Public
    School Students
    (1992)](http://www.nycsubway.org/perl/artwork_show?161)

  - Station Reporter – [Columbus Circle/59th Street
    Complex](https://web.archive.org/web/20070315000207/http://www.stationreporter.net/columbus.htm)

  - TheSubwayNut – [59th Street IND
    station](http://subwaynut.com/ct/59a/index.php)

  - TheSubwayNut – [59th Street IRT
    station](http://subwaynut.com/ct/59n1_downtown/index.php)

  - Forgotten NY – [Original 28 - NYC's First 28 Subway
    Stations](http://forgotten-ny.com/2006/01/the-original-28-part-2-a-look-at-the-artwork-from-the-nycs-first-28-stations-opened-october-27-1904/)

  - MTA's Arts For Transit – [59th Street–Columbus
    Circle](https://web.archive.org/web/20120927014743/http://mta.info/mta/aft/permanentart/permart.html?agency=nyct&line=1&station=10&xdev=982)

  - Abandoned Stations - [59 St
    platform](http://www.columbia.edu/~brennan/abandoned/59st.html)

  - [Entrance to Trump International Hotel & Tower at 60th Street from
    Google Maps Street
    View](https://maps.google.com/maps?f=q&source=s_q&hl=en&q=W+59th+St,+New+York,+10116&sll=37.0625,-95.677068&sspn=34.450489,76.552734&ie=UTF8&cd=1&geocode=FUsLbgIdvDCX-w&split=0&hq=&hnear=W+59th+St,+New+York&ll=40.767997,-73.981032&spn=0.004022,0.013433&z=17&layer=c&cbll=40.768714,-73.981953&panoid=wF3zeDxPsUOauNkm8kuKMw&cbp=12,71.69,,1,2.92)

  - [Columbus Circle & Central Park South entrance to Broadway-Seventh
    Avenue Line from Google Maps Street
    View](https://maps.google.com/maps?q=Central+Park+South,+New+York,+NY,+United+States&hl=en&ll=40.768549,-73.981462&spn=0.000033,0.019248&sll=40.767802,-73.981429&sspn=0.0052,0.009624&vpsrc=6&t=m&z=16&layer=c&cbll=40.768656,-73.981397&panoid=BHa5KZnOpiIvmUkVnQjomQ&cbp=12,98.75,,0,1.2)

  - [Eighth Avenue & 58th Street entrance to Eighth Avenue Line from
    Google Maps Street
    View](https://maps.google.com/maps?q=W+59th+St,+New+York,+10116&hl=en&ll=40.767298,-73.982137&spn=0.005859,0.01133&sll=37.0625,-95.677068&sspn=34.450489,76.552734&geocode=FUsLbgIdvDCX-w&hnear=W+59th+St,+New+York,+10019&layer=c&cbll=40.767336,-73.982236&panoid=1Lw4FcVruf5uuPzQ8gmCVw&cbp=12,63.28,,0,9.38&t=m&z=174)

  - [Time Warner Center entrance from Google Maps Street
    View](https://maps.google.com/maps?q=W+59th+St,+New+York,+10116&hl=en&ll=40.767574,-73.982266&spn=0.000016,0.009624&sll=37.0625,-95.677068&sspn=34.450489,76.552734&geocode=FUsLbgIdvDCX-w&layer=c&cbll=40.767674,-73.9822&panoid=5WPVxAG2p_FYRg8jXReMAQ&cbp=12,263.67,,1,0.61&t=m&z=17&vpsrc=0)

  - [Eighth Avenue & 57th Street entrance to Eighth Avenue Line from
    Google Maps Street
    View](https://maps.google.com/maps?f=q&source=s_q&hl=en&q=W+59th+St,+New+York,+10116&sll=37.0625,-95.677068&sspn=34.450489,76.552734&ie=UTF8&cd=1&geocode=FUsLbgIdvDCX-w&split=0&hq=&hnear=W+59th+St,+New+York&ll=40.766508,-73.982722&spn=0.000963,0.003358&z=19&layer=c&cbll=40.766695,-73.982884&panoid=8W8szVhd2lFU1-daMYHP9g&cbp=12,163.32,,2,-1.85)

  - [Broadway & 60th Street entrance to Broadway-Seventh Avenue Line
    from Google Maps Street
    View](https://maps.google.com/maps?f=q&source=s_q&hl=en&q=W+59th+St,+New+York,+10116&sll=37.0625,-95.677068&sspn=34.450489,76.552734&ie=UTF8&cd=1&geocode=FUsLbgIdvDCX-w&split=0&hq=&hnear=W+59th+St,+New+York&ll=40.768947,-73.98218&spn=0.003851,0.013433&z=17&layer=c&cbll=40.768851,-73.982176&panoid=rgbM0JY2t414L-Gzhoaecg&cbp=12,125.84,,0,7.16)

  - [Elevator entrance at Columbus Circle and Eighth Avenue from Google
    Maps Street
    View](https://maps.google.com/maps?ll=40.767757,-73.982154&spn=0.002929,0.005665&t=m&z=18&layer=c&cbll=40.767837,-73.982264&panoid=cutH2_VUUvExWRGv9GDAwA&cbp=12,219.74,,2,0.26)

  - [IRT platforms from Google Maps Street
    View](http://www.google.com/maps/@40.7687247,-73.9817207,3a,75y,41.06h,90.52t/data=!3m8!1e1!3m6!1s-TCS_2xSIBac%2FV6uWQ7me-fI%2FAAAAAAAAPiI%2F6v-tPrtOjAgJs4xuIDmHOWps3ZeHcWQOACLIB!2e4!3e11!6s%2F%2Flh4.googleusercontent.com%2F-TCS_2xSIBac%2FV6uWQ7me-fI%2FAAAAAAAAPiI%2F6v-tPrtOjAgJs4xuIDmHOWps3ZeHcWQOACLIB%2Fw203-h100-p-k-no%2F!7i9728!8i4864!4m3!8m2!3m1!1e1!6m1!1e1)

  - [IND platforms from Google Maps Street
    View](http://www.google.com/maps/@40.7687313,-73.9817187,3a,75y,4.93h,84.16t/data=!3m7!1e1!3m5!1s-aFAB2eC0xiw%2FV6uV6APIHII%2FAAAAAAAAPok%2F7vIulzJcM9EShsAI-_oGpGUinnwmv41owCLIB!2e4!6s%2F%2Flh5.googleusercontent.com%2F-aFAB2eC0xiw%2FV6uV6APIHII%2FAAAAAAAAPok%2F7vIulzJcM9EShsAI-_oGpGUinnwmv41owCLIB%2Fw203-h100-p-k-no%2F!7i9728!8i4864!4m3!8m2!3m1!1e1!6m1!1e1)

  - [Turnstyle shop from Google Maps Street
    View](http://www.google.com/maps/@40.7669106,-73.9827728,3a,75y,84.25h,86.18t/data=!3m8!1e1!3m6!1s-7I-DrLIUr4g%2FV696XLmLwPI%2FAAAAAAAAQJQ%2FicMcAhEgujojzYCUPmKzcCcX-6oh9uhowCLIB!2e4!3e11!6s%2F%2Flh4.googleusercontent.com%2F-7I-DrLIUr4g%2FV696XLmLwPI%2FAAAAAAAAQJQ%2FicMcAhEgujojzYCUPmKzcCcX-6oh9uhowCLIB%2Fw203-h100-p-k-no%2F!7i9728!8i4864!4m3!8m2!3m1!1e1!6m1!1e1)

[Category:IRT百老匯-第七大道線車站](../Category/IRT百老匯-第七大道線車站.md "wikilink")
[Category:IND第八大道線車站](../Category/IND第八大道線車站.md "wikilink")
[Category:1904年启用的铁路车站](../Category/1904年启用的铁路车站.md "wikilink")
[Category:1932年啟用的鐵路車站](../Category/1932年啟用的鐵路車站.md "wikilink")
[Category:曼哈頓紐約地鐵車站](../Category/曼哈頓紐約地鐵車站.md "wikilink")
[Category:1904年紐約州建立](../Category/1904年紐約州建立.md "wikilink")

1.

2.