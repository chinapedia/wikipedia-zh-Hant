**5月9日**是阳历年的第129天（闰年是130天），离一年的结束还有236天。

## 大事记

### 4世紀

  - [328年](../Page/328年.md "wikilink")：[東方正統教會教父](../Page/東方正統教會.md "wikilink")[亚他那修成为](../Page/亚他那修.md "wikilink")[埃及](../Page/埃及.md "wikilink")[亞歷山大港牧首](../Page/亞歷山大港牧首.md "wikilink")。

### 16世紀

  - [1502年](../Page/1502年.md "wikilink")：[克里斯托弗·哥伦布最后一次启航出发](../Page/克里斯托弗·哥伦布.md "wikilink")，探索他认为的[印度](../Page/印度.md "wikilink")（另一说为[5月11日](../Page/5月11日.md "wikilink")）

### 20世紀

  - [1901年](../Page/1901年.md "wikilink")：[澳大利亚第一届](../Page/澳大利亚.md "wikilink")[议会在](../Page/澳大利亚议会.md "wikilink")[墨尔本](../Page/墨尔本.md "wikilink")[皇家展览馆召开](../Page/皇家展览馆.md "wikilink")。
  - [1911年](../Page/1911年.md "wikilink")：[清朝政府頒佈](../Page/清朝.md "wikilink")[鐵路](../Page/鐵路.md "wikilink")[國有化詔令](../Page/國有化.md "wikilink")。
  - [1915年](../Page/1915年.md "wikilink")：[中華民國總統](../Page/中華民國總統.md "wikilink")[袁世凯迫于](../Page/袁世凯.md "wikilink")[日本军事压力](../Page/日本.md "wikilink")，接受其提出的《[二十一条](../Page/二十一条.md "wikilink")》，史称[五九國恥](../Page/五九國恥.md "wikilink")。
  - [1926年](../Page/1926年.md "wikilink")：和宣稱成功乘[飛機飛越](../Page/固定翼飛機.md "wikilink")[北極](../Page/北极点.md "wikilink")，但依伯德之後的日記這項成就存有爭議。
  - [1930年](../Page/1930年.md "wikilink")：[貝璐就任](../Page/貝璐.md "wikilink")[香港總督](../Page/香港總督.md "wikilink")。
  - [1936年](../Page/1936年.md "wikilink")：[意大利自](../Page/意大利.md "wikilink")[5月5日占领首都](../Page/5月5日.md "wikilink")[亚的斯亚贝巴后](../Page/亚的斯亚贝巴.md "wikilink")，宣布正式吞并[埃塞俄比亚](../Page/埃塞俄比亚.md "wikilink")。
  - [1944年](../Page/1944年.md "wikilink")：任[薩爾瓦多臨時總統](../Page/萨尔瓦多总统.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：[納粹德國的代表在](../Page/納粹德國.md "wikilink")[柏林和](../Page/柏林.md "wikilink")[苏军签署](../Page/苏联红军.md "wikilink")[德军无条件](../Page/德意志國防軍.md "wikilink")[投降书于本日零时生效](../Page/第二次世界大戰歐洲戰場的結束.md "wikilink")，投降书的生效标志着[第二次世界大战](../Page/第二次世界大战.md "wikilink")[苏联](../Page/苏联.md "wikilink")[卫国战争结束](../Page/苏德战争.md "wikilink")。
  - [1946年](../Page/1946年.md "wikilink")：[尼古拉·米哈伊洛维奇·什维尔尼克任](../Page/尼古拉·米哈伊洛维奇·什维尔尼克.md "wikilink")[苏联](../Page/苏联.md "wikilink")[最高苏维埃主席团主席](../Page/苏联最高苏维埃主席团.md "wikilink")，参见[苏联国家元首列表](../Page/苏联国家元首列表.md "wikilink")。
  - 1946年：[意大利国王](../Page/意大利统治者列表.md "wikilink")[维托里奥·埃马努埃莱三世退位](../Page/维托里奥·埃马努埃莱三世.md "wikilink")，其子[翁貝托二世继承王位](../Page/翁貝托二世.md "wikilink")。
  - [1950年](../Page/1950年.md "wikilink")：[法国外交部长](../Page/法国.md "wikilink")[罗贝尔·舒曼公布](../Page/罗贝尔·舒曼.md "wikilink")[舒曼计划](../Page/舒曼计划.md "wikilink")，提议设立机构管理法国、[德国等](../Page/德国.md "wikilink")[欧洲国家的](../Page/欧洲.md "wikilink")[歐洲煤钢共同體](../Page/歐洲煤钢共同體.md "wikilink")。
  - [1960年](../Page/1960年.md "wikilink")：[美国批准出售](../Page/美国.md "wikilink")[避孕药](../Page/避孕药.md "wikilink")。
  - [1977年](../Page/1977年.md "wikilink")：[香港商品期貨交易所正式開業](../Page/香港商品期貨交易所.md "wikilink")，成為遠東國際性[棉花期貨市場](../Page/棉花.md "wikilink")。繼棉花之後，[原糖及](../Page/原糖.md "wikilink")[黃豆期貨市場亦相繼於](../Page/大豆.md "wikilink")1977年和1979年底開業。
  - [1978年](../Page/1978年.md "wikilink")：一年一度[香港](../Page/香港.md "wikilink")[長洲太平清醮盛會發生包山倒塌意外](../Page/長洲太平清醮.md "wikilink")。「[搶包山](../Page/搶包山.md "wikilink")」於當日零時開始，數百人爭相攀上包山，其中一座大包山突然倒塌，壓向旁邊的一座包山，然後一起倒下，24人受傷。當局為安全起見，決定以後取消「搶包山」活動。不過停辦二十幾年之後，2005年恢復舉辦，但是加強安全措施。
  - [1991年](../Page/1991年.md "wikilink")：[韩国全國幾十個城市爆發大規模示威](../Page/大韩民国.md "wikilink")，抗議[漢城一個大學生在參加集會之後](../Page/首爾.md "wikilink")，企圖爬牆進入校園時，被警察用鐵棒打死。
  - [1993年](../Page/1993年.md "wikilink")：第一屆[東亞運動會在中國](../Page/東亞運動會.md "wikilink")[上海開幕](../Page/上海市.md "wikilink")，共有九個國家及地區的運動員參加本屆運動會。
  - [1994年](../Page/1994年.md "wikilink")：[纳尔逊·曼德拉当选](../Page/纳尔逊·曼德拉.md "wikilink")[南非共和国历史上首位](../Page/南非.md "wikilink")[黑人](../Page/黑人.md "wikilink")[总统](../Page/南非总统.md "wikilink")。

### 21世紀

  - [2004年](../Page/2004年.md "wikilink")：[俄罗斯联邦](../Page/俄罗斯联邦.md "wikilink")[车臣共和国总统](../Page/车臣共和国.md "wikilink")[艾哈迈德·卡德罗夫在](../Page/艾哈迈德·卡德罗夫.md "wikilink")[格罗兹尼参加纪念](../Page/格罗兹尼.md "wikilink")[二战胜利的庆祝活动时被](../Page/第二次世界大战.md "wikilink")[地雷炸死](../Page/地雷.md "wikilink")。
  - [2013年](../Page/2013年.md "wikilink")：[廣大興28號事件](../Page/廣大興28號事件.md "wikilink")：菲律賓公務船於[臺灣與](../Page/臺灣.md "wikilink")[菲律宾重疊](../Page/菲律宾.md "wikilink")[专属经济区武力攻擊進行海上作業的台灣漁船廣大興](../Page/专属经济区.md "wikilink")28號，造成漁民洪石成死亡，進而造成台灣與菲律賓外交惡化，在台灣也欣起反菲的浪潮。
  - [2018年](../Page/2018年.md "wikilink")：[馬來西亞進行第十四屆全國大選](../Page/2018年马来西亚大选.md "wikilink")。反對阵线[希望聯盟以简单多数席位勝選](../Page/希望聯盟.md "wikilink")，終結[國民陣線執政聯盟自](../Page/國民陣線.md "wikilink")[馬來西亞獨立以來近](../Page/馬來西亞.md "wikilink")61年的執政，達成馬來西亞首次中央政府[政黨輪替紀錄](../Page/政黨輪替.md "wikilink")，也讓將二度擔任首相的[馬哈迪成為目前最高齡的國家領導人](../Page/馬哈迪.md "wikilink")。

## 出生

  - [1147年](../Page/1147年.md "wikilink")：[源赖朝](../Page/源赖朝.md "wikilink")，[日本](../Page/日本.md "wikilink")[镰仓幕府首任](../Page/镰仓幕府.md "wikilink")[征夷大将军](../Page/征夷大將軍.md "wikilink")（[1199年逝世](../Page/1199年.md "wikilink")）
  - [1439年](../Page/1439年.md "wikilink")：[庇护三世](../Page/庇護三世.md "wikilink")，[教宗](../Page/教宗.md "wikilink")（[1503年逝世](../Page/1503年.md "wikilink")）
  - [1733年](../Page/1733年.md "wikilink")：[愛新覺羅](../Page/爱新觉罗氏.md "wikilink")·[弘曕](../Page/弘曕.md "wikilink")，[清朝](../Page/清朝.md "wikilink")[雍正帝第六子](../Page/雍正帝.md "wikilink")，[乾隆帝把他過繼給叔父](../Page/乾隆帝.md "wikilink")[果親王](../Page/果親王.md "wikilink")[允禮](../Page/允禮.md "wikilink")，後襲封果親王（[1765年逝世](../Page/1765年.md "wikilink")）
  - [1912年](../Page/1912年.md "wikilink")：[赵萝蕤](../Page/赵萝蕤.md "wikilink")，中国[翻译家](../Page/翻译家.md "wikilink")（[1998年逝世](../Page/1998年.md "wikilink")）
  - [1921年](../Page/1921年.md "wikilink")：[蘇菲·蕭爾](../Page/蘇菲·蕭爾.md "wikilink")，[德国反纳粹英雄](../Page/德国.md "wikilink")，1940年代[慕尼黑](../Page/慕尼黑.md "wikilink")「[白玫瑰](../Page/白玫瑰.md "wikilink")」組織的主要成員（[1943年逝世](../Page/1943年.md "wikilink")）
  - [1936年](../Page/1936年.md "wikilink")：[亞伯特·芬尼](../Page/亞伯特·芬尼.md "wikilink")，[英国](../Page/英国.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [1946年](../Page/1946年.md "wikilink")：[坎娣絲·伯根](../Page/坎娣絲·伯根.md "wikilink")，[美国女演員](../Page/美国.md "wikilink")
  - [1947年](../Page/1947年.md "wikilink")：[邁可·列維特](../Page/邁可·列維特.md "wikilink")，美國[史丹佛大學](../Page/史丹佛大學.md "wikilink")[結構生物學教授](../Page/结构生物学.md "wikilink")，2013年因「為複雜化學系統創造了多尺度模型」而獲得[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")
  - [1950年](../Page/1950年.md "wikilink")：[邱義仁](../Page/邱義仁.md "wikilink")，[臺灣](../Page/臺灣.md "wikilink")[民進黨政治人物](../Page/民進黨.md "wikilink")
  - [1954年](../Page/1954年.md "wikilink")：[李國麟](../Page/李國麟_\(演員\).md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [1957年](../Page/1957年.md "wikilink")：[魚戶修](../Page/魚戶修.md "wikilink")，日本漫畫家
  - [1958年](../Page/1958年.md "wikilink")：[金惠鈺](../Page/金惠鈺.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1967年](../Page/1967年.md "wikilink")：[岡野剛](../Page/岡野剛.md "wikilink")，[日本漫畫家](../Page/日本.md "wikilink")
  - [1972年](../Page/1972年.md "wikilink")：[若木民喜](../Page/若木民喜.md "wikilink")，日本漫畫家
  - [1973年](../Page/1973年.md "wikilink")：[秋相美](../Page/秋相美.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1975年](../Page/1975年.md "wikilink")：[翁立友](../Page/翁立友.md "wikilink")，臺灣歌手
  - [1978年](../Page/1978年.md "wikilink")：[陳楚河](../Page/陳楚河.md "wikilink")，[臺灣演員](../Page/臺灣.md "wikilink")
  - 1978年：[李富玉](../Page/李富玉.md "wikilink")，[中國](../Page/中华人民共和国.md "wikilink")[自行車](../Page/自行車.md "wikilink")[運動員](../Page/運動員.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[横山裕](../Page/横山裕.md "wikilink")，日本演員、歌手
  - [1983年](../Page/1983年.md "wikilink")：[許嘉凌](../Page/許嘉凌.md "wikilink")，台灣藝人
  - [1984年](../Page/1984年.md "wikilink")：[平原綾香](../Page/平原綾香.md "wikilink")，日本女歌手
  - [1988年](../Page/1988年.md "wikilink")：[李代沫](../Page/李代沫.md "wikilink")，中国男歌手
  - [1991年](../Page/1991年.md "wikilink")：[何超蓮](../Page/何超蓮.md "wikilink")、[何猷啟](../Page/何猷啟.md "wikilink")，[澳門賭王](../Page/澳門.md "wikilink")[何鴻燊與其第三位太太](../Page/何鴻燊.md "wikilink")[陳婉珍所生的一對子女](../Page/陳婉珍.md "wikilink")
      - [許淑淨台灣前舉重運動員](../Page/許淑淨.md "wikilink")，兩屆奧運會金牌得主
  - [1993年](../Page/1993年.md "wikilink")：[山田涼介](../Page/山田涼介.md "wikilink")，[日本演員](../Page/日本.md "wikilink")、歌手
  - [1994年](../Page/1994年.md "wikilink")：[呂會鉉](../Page/呂會鉉.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")

## 逝世

  - [1805年](../Page/1805年.md "wikilink")：[弗里德里希·席勒](../Page/弗里德里希·席勒.md "wikilink")，[德国](../Page/德国.md "wikilink")[诗人](../Page/诗人.md "wikilink")、[剧作家](../Page/剧作家.md "wikilink")。（[1759年出生](../Page/1759年.md "wikilink")）
  - [1958年](../Page/1958年.md "wikilink")：[及川古志郎](../Page/及川古志郎.md "wikilink")，[日本海軍大將](../Page/大日本帝國海軍.md "wikilink")。（[1883年出生](../Page/1883年.md "wikilink")）
  - [1931年](../Page/1931年.md "wikilink")：[阿尔伯特·迈克耳孙](../Page/阿尔伯特·迈克耳孙.md "wikilink")，[美国物理學家](../Page/美国.md "wikilink")，1907年[诺贝尔物理学奖得獎者](../Page/诺贝尔物理学奖.md "wikilink")。（[1852年出生](../Page/1852年.md "wikilink")）
  - [1978年](../Page/1978年.md "wikilink")：[阿尔多·莫罗](../Page/阿尔多·莫罗.md "wikilink")，[意大利](../Page/意大利.md "wikilink")[政治家](../Page/政治家.md "wikilink")。（[1916年出生](../Page/1916年.md "wikilink")）
  - [2006年](../Page/2006年.md "wikilink")：[严恺](../Page/严恺.md "wikilink")，[中国科学院院士](../Page/中国科学院.md "wikilink")、[中国工程院院士](../Page/中国工程院.md "wikilink")、水利科學家。（[1912年出生](../Page/1912年.md "wikilink")）
  - [2012年](../Page/2012年.md "wikilink")：[維達·沙宣](../Page/維達·沙宣.md "wikilink")，[倫敦沙宣美髮學院創辦人](../Page/倫敦沙宣美髮學院.md "wikilink")。（[1928年出生](../Page/1928年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[乔治·迈克尔·利德](../Page/乔治·迈克尔·利德.md "wikilink")，[美国政治人物](../Page/美国.md "wikilink")。（[1918年出生](../Page/1918年.md "wikilink")）
  - 2013年：[安德鲁·辛普森](../Page/安德鲁·辛普森.md "wikilink")，[英国](../Page/英国.md "wikilink")[帆船運動員](../Page/帆船.md "wikilink")。（[1977年出生](../Page/1977年.md "wikilink")）
  - 2013年：[洪石成](../Page/廣大興28號事件.md "wikilink")，[臺灣](../Page/臺灣.md "wikilink")[漁民](../Page/漁民.md "wikilink")，在[廣大興28號事件中被](../Page/廣大興28號事件.md "wikilink")[菲律宾](../Page/菲律宾.md "wikilink")[海巡署人員槍殺](../Page/菲律賓海巡署.md "wikilink")。
  - 2017年：[钱其琛](../Page/钱其琛.md "wikilink")，原[中共中央政治局委员](../Page/中国共产党中央政治局.md "wikilink")，[国务院副总理](../Page/中华人民共和国国务院副总理.md "wikilink")，[外交部部长](../Page/中华人民共和国外交部.md "wikilink")（[1928年出生](../Page/1928年.md "wikilink")）

## 节假日和习俗

  - [聯合國](../Page/聯合國.md "wikilink")：[纪念与和解日](../Page/纪念与和解日.md "wikilink")

  - ：[五九國恥日](../Page/五九國恥.md "wikilink")

  - [卫国战争胜利日](../Page/苏德战争.md "wikilink")（大多數前[蘇聯共和國和](../Page/蘇聯共和國.md "wikilink")[東歐國家](../Page/東歐.md "wikilink")）

## 參考資料