《**60分钟**》（）為[美國的一個](../Page/美國.md "wikilink")[新聞雜誌節目](../Page/新聞雜誌.md "wikilink")，由[哥倫比亞廣播公司](../Page/哥倫比亞廣播公司.md "wikilink")（CBS）制作并播出，自1968年開始播出，迄今已播出逾40年。

該節目制作品質精良，口碑上佳，是美國知名的電視節目，也多次獲得獎項。節目開播的靈感來自[加拿大國營頻道同類節目](../Page/加拿大國營頻道.md "wikilink")《》。

此外，在英国、澳大利亞、紐西蘭、葡萄牙、法國和臺灣均制作過同名節目。

在香港，[亞視国际台曾经在](../Page/亞洲電視國際台.md "wikilink")2009年初和2011年年尾播出本节目；2012年9月6日至2013年9月19日，[無綫](../Page/電視廣播有限公司.md "wikilink")[明珠台逢星期四晚上播出](../Page/明珠台.md "wikilink")《-{六十分鐘時事雜誌}-》，其後於2013年9月28日至2016年6月18日、2016年10月1日至2017年6月10日及2017年9月30日起改為逢星期六傍晚播出，並於[myTV
SUPER免費區提供節目重溫](../Page/myTV_SUPER.md "wikilink")（集數上傳後7日後會刪除）。

## 主持及記者

### 现任

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>职务</p></th>
<th><p>工作年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>史提夫·克羅夫特<br />
</p></td>
<td><p>主持、主编</p></td>
<td><p>1989-今</p></td>
</tr>
<tr class="even">
<td><p>萊絲莉·斯塔爾<br />
</p></td>
<td><p>主持、主编</p></td>
<td><p>1991-今</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/斯科特·佩利.md" title="wikilink">斯科特·佩利</a><br />
Scott Pelley</p></td>
<td><p>主持</p></td>
<td><p>2003-今</p></td>
</tr>
<tr class="even">
<td><p>拉娜·洛根<br />
</p></td>
<td><p>兼职记者</p></td>
<td><p>2005-2012</p></td>
</tr>
<tr class="odd">
<td><p>主持</p></td>
<td><p>2012-今</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>比尔·惠特克<br />
</p></td>
<td><p>主持</p></td>
<td><p>2014-今</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/安德森·库珀.md" title="wikilink">安德森·库珀</a><br />
Anderson Cooper</p></td>
<td><p>兼职记者</p></td>
<td><p>2006-今</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>兼职记者</p></td>
<td><p>2015-今</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>兼职记者</p></td>
<td><p>2015-今</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奥普拉·温弗里.md" title="wikilink">奥普拉·温弗里</a><br />
Oprah Winfrey</p></td>
<td><p>兼职记者</p></td>
<td><p>2017-今</p></td>
</tr>
</tbody>
</table>

### 前任

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>职务</p></th>
<th><p>工作年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>†</p></td>
<td><p>主持</p></td>
<td><p>1968-1970<br />
1978-1991</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/麦克·华莱士.md" title="wikilink">麦克·华莱士</a> †<br />
Mike Wallace †</p></td>
<td><p>主持</p></td>
<td><p>1968-2006</p></td>
</tr>
<tr class="odd">
<td><p>名誉记者</p></td>
<td><p>2006-2008</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/莫利·塞弗.md" title="wikilink">莫利·塞弗</a> †<br />
Morley Safer †</p></td>
<td><p>兼职记者</p></td>
<td><p>1968-1970</p></td>
</tr>
<tr class="odd">
<td><p>主持</p></td>
<td><p>1970-2016</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/丹·拉瑟.md" title="wikilink">丹·拉瑟</a><br />
Dan Rather</p></td>
<td><p>兼职记者</p></td>
<td><p>1968-1975</p></td>
</tr>
<tr class="odd">
<td><p>主持</p></td>
<td><p>1975-1981<br />
2005-2006</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>†</p></td>
<td><p>兼职记者</p></td>
<td><p>1976-1981</p></td>
</tr>
<tr class="odd">
<td><p>主持</p></td>
<td><p>1981-2006</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黛安·索耶.md" title="wikilink">黛安·索耶</a><br />
Diane Sawyer</p></td>
<td><p>兼职记者</p></td>
<td><p>1981-1984</p></td>
</tr>
<tr class="odd">
<td><p>主持</p></td>
<td><p>1984-1989</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>兼职记者</p></td>
<td><p>1982-1985<br />
1991-1993</p></td>
</tr>
<tr class="odd">
<td><p>主持</p></td>
<td><p>1990-1991</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鲍勃·西蒙.md" title="wikilink">鲍勃·西蒙</a> †<br />
Bob Simon †</p></td>
<td><p>主持</p></td>
<td><p>1996-2015</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/克里斯汀·阿曼普.md" title="wikilink">克里斯汀·阿曼普</a><br />
Christiane Amanpour</p></td>
<td><p>兼职记者</p></td>
<td><p>1996-2000</p></td>
</tr>
<tr class="even">
<td><p>主持</p></td>
<td><p>2000-2005</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/沃尔特·克朗凯特.md" title="wikilink">沃尔特·克朗凯特</a> †<br />
Walter Cronkite †</p></td>
<td><p>兼职记者</p></td>
<td><p>1968-1991</p></td>
</tr>
<tr class="even">
<td><p>†</p></td>
<td><p>兼职记者</p></td>
<td><p>1968-1979</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>兼职记者</p></td>
<td><p>1968-1980</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>兼职记者</p></td>
<td><p>1968-1995</p></td>
</tr>
<tr class="odd">
<td><p>†</p></td>
<td><p>兼职记者</p></td>
<td><p>1968-1969</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>兼职记者</p></td>
<td><p>1969-1975</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>兼职记者</p></td>
<td><p>1973-1996</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>兼职记者</p></td>
<td><p>1975-1979</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>兼职记者</p></td>
<td><p>1978-1987</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>兼职记者</p></td>
<td><p>1981-1994</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>兼职记者</p></td>
<td><p>1985-1987</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宗毓华.md" title="wikilink">宗毓华</a><br />
Connie Chung</p></td>
<td><p>兼职记者</p></td>
<td><p>1990-1993</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>兼职记者</p></td>
<td><p>1990-1999</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>兼职记者</p></td>
<td><p>1992-2005</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>兼职记者</p></td>
<td><p>1995-1998</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>兼职记者</p></td>
<td><p>1997-2002</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>兼职记者</p></td>
<td><p>1998-2002</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/凯蒂·库瑞克.md" title="wikilink">凯蒂·库瑞克</a><br />
Katie Couric</p></td>
<td><p>兼职记者</p></td>
<td><p>2006-2011</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/查理·罗斯.md" title="wikilink">查理·罗斯</a><br />
Charlie Rose</p></td>
<td><p>兼职记者</p></td>
<td><p>2008-2017</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>兼职记者</p></td>
<td><p>2009-2013</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/桑贾伊·古普塔.md" title="wikilink">桑贾伊·古普塔</a><br />
Bryant Gumbel</p></td>
<td><p>兼职记者</p></td>
<td><p>2011-2014</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>兼职记者</p></td>
<td><p>2012</p></td>
</tr>
</tbody>
</table>

注：标 † 为已离世

## 腳註

## 外部連結

  - [無綫官方页面](http://programme.tvb.com/news/60minutes)

[Category:美國電視新聞節目](../Category/美國電視新聞節目.md "wikilink")
[Category:CBS電視節目](../Category/CBS電視節目.md "wikilink")
[Category:皮博迪獎得主](../Category/皮博迪獎得主.md "wikilink")
[Category:亞洲電視外購節目](../Category/亞洲電視外購節目.md "wikilink")
[Category:無綫電視外購節目](../Category/無綫電視外購節目.md "wikilink")
[Category:英语电视节目](../Category/英语电视节目.md "wikilink")
[Category:金球獎最佳戲劇類影集](../Category/金球獎最佳戲劇類影集.md "wikilink")
[Category:超级碗后续节目](../Category/超级碗后续节目.md "wikilink")