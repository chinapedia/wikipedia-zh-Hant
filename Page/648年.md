## 大事记

  - [唐太宗派](../Page/唐太宗.md "wikilink")[阿史那社爾](../Page/阿史那社爾.md "wikilink")、[契苾何力](../Page/契苾何力.md "wikilink")、[郭孝恪率軍征服](../Page/郭孝恪.md "wikilink")[龜茲](../Page/龜茲.md "wikilink")（今新疆[阿克蘇](../Page/阿克蘇.md "wikilink")），俘獲國王[白訶黎布失畢](../Page/白訶黎布失畢.md "wikilink")，也徵用了[鐵勒](../Page/鐵勒.md "wikilink")、[東突厥](../Page/東突厥.md "wikilink")、[吐蕃和](../Page/吐蕃.md "wikilink")[吐谷渾的士兵](../Page/吐谷渾.md "wikilink")，[白訶黎布失畢逃跑後](../Page/白訶黎布失畢.md "wikilink")，[阿史那社爾改立](../Page/阿史那社爾.md "wikilink")[白訶黎布失畢弟弟](../Page/白訶黎布失畢.md "wikilink")[白葉護為新國王](../Page/白葉護.md "wikilink")，在[西突厥](../Page/西突厥.md "wikilink")、[于闐和](../Page/于闐.md "wikilink")[安國向唐朝進貢後撤軍](../Page/安國.md "wikilink")，並將[高昌的](../Page/高昌.md "wikilink")[安西都護府遷於](../Page/安西都護府.md "wikilink")[龜茲](../Page/龜茲.md "wikilink")，守備[天山山脈南側的](../Page/天山山脈.md "wikilink")[絲綢之路](../Page/絲綢之路.md "wikilink")（天山南路），防備[突厥](../Page/突厥.md "wikilink")、[吐蕃等勢力](../Page/吐蕃.md "wikilink")。
  - [阿史那社爾再攻](../Page/阿史那社爾.md "wikilink")[焉耆](../Page/焉耆.md "wikilink")，國王[龍薛婆阿那支逃到](../Page/龍薛婆阿那支.md "wikilink")[龜茲東部](../Page/龜茲.md "wikilink")，被[阿史那社爾擒殺](../Page/阿史那社爾.md "wikilink")，[阿史那社爾擁立](../Page/阿史那社爾.md "wikilink")[龍薛婆阿那支的堂弟](../Page/龍薛婆阿那支.md "wikilink")[龍先那準為焉耆國王](../Page/龍先那準.md "wikilink")。
  - [王玄策生擒](../Page/王玄策.md "wikilink")[阿羅那順及王妃](../Page/阿羅那順.md "wikilink")、子等，虜男女12000人、牛馬20000餘被獻到[長安](../Page/長安.md "wikilink")[唐太宗面前](../Page/唐太宗.md "wikilink")。
  - [奚](../Page/奚.md "wikilink")、[契丹歸順](../Page/契丹.md "wikilink")[唐朝](../Page/唐朝.md "wikilink")，建立了兩個羈縻州[饒樂都督府](../Page/饒樂都督府.md "wikilink")、[松漠都督府](../Page/松漠都督府.md "wikilink")。
  - [唐太宗派](../Page/唐太宗.md "wikilink")[薛萬徹和](../Page/薛萬徹.md "wikilink")[裴行方率軍從萊州渡海攻打](../Page/裴行方.md "wikilink")[鴨綠江口](../Page/鴨綠江.md "wikilink")，但隨著[唐太宗于](../Page/唐太宗.md "wikilink")[649年去世](../Page/649年.md "wikilink")，使唐再次攻[高句麗的戰役擱淺](../Page/高句麗.md "wikilink")。
  - 《[晋书](../Page/晋书.md "wikilink")》编成。
  - [西安](../Page/西安.md "wikilink")[大慈恩寺建成](../Page/大慈恩寺.md "wikilink")。
  - [西域](../Page/西域.md "wikilink")[結骨國遣使入朝](../Page/結骨.md "wikilink")，人皆「赤髮綠睛」。
  - [教皇](../Page/教皇.md "wikilink")[西奥多一世革除](../Page/西奥多一世.md "wikilink")[君士坦丁堡教主](../Page/君士坦丁堡.md "wikilink")[保罗二世的教籍](../Page/保罗二世.md "wikilink")。

## 出生

  - [弘文天皇](../Page/弘文天皇.md "wikilink")，日本天皇（[672年去世](../Page/672年.md "wikilink")，48岁）

## 逝世

  - [李百药](../Page/李百药.md "wikilink")，著《[北齐书](../Page/北齐书.md "wikilink")》（[565年出生](../Page/565年.md "wikilink")，83岁）
  - [孔颖达](../Page/孔颖达.md "wikilink")，著《[礼记注疏](../Page/礼记注疏.md "wikilink")》（[574年出生](../Page/574年.md "wikilink")，74岁）
  - [房玄龄](../Page/房玄龄.md "wikilink")，唐朝大臣（[579年出生](../Page/579年.md "wikilink")，69岁）
  - [龍薛婆阿那支](../Page/龍薛婆阿那支.md "wikilink")，[西域](../Page/西域.md "wikilink")[焉耆國王](../Page/焉耆.md "wikilink")。

[\*](../Category/648年.md "wikilink")
[8年](../Category/640年代.md "wikilink")
[4](../Category/7世纪各年.md "wikilink")