**6月16日**在一年当中是第167天（[闰年则是](../Page/闰年.md "wikilink")168天），距离一年的结束还有198天。

## 大事记

### 10世紀

  - [907年](../Page/907年.md "wikilink")：[後梁封浙江杭州臨安人](../Page/後梁.md "wikilink")[钱镠為吳越王](../Page/钱镠.md "wikilink")，[吳越國建立](../Page/吳越國.md "wikilink")。

### 15世紀

  - [1487年](../Page/1487年.md "wikilink")：[玫瑰戰爭](../Page/玫瑰戰爭.md "wikilink")：[博斯沃思原野战役](../Page/博斯沃思原野战役.md "wikilink")。

### 16世紀

  - [1522年](../Page/1522年.md "wikilink")：[神聖羅馬帝國](../Page/神聖羅馬帝國.md "wikilink")[皇帝](../Page/神聖羅馬帝國皇帝列表.md "wikilink")[查理五世與](../Page/查理五世_\(神聖羅馬帝國\).md "wikilink")[英王](../Page/英格蘭君主列表.md "wikilink")[亨利八世簽署](../Page/亨利八世.md "wikilink")，號召進攻[法國](../Page/法國.md "wikilink")。

### 17世紀

  - [1636年](../Page/1636年.md "wikilink")：[清太宗皇太極始设](../Page/清太宗.md "wikilink")[都察院](../Page/都察院.md "wikilink")。
  - [1643年](../Page/1643年.md "wikilink")：[张献忠攻陷汉阳](../Page/张献忠.md "wikilink")，[武昌大震](../Page/武昌.md "wikilink")。

### 18世紀

  - [1779年](../Page/1779年.md "wikilink")：[西班牙对](../Page/西班牙.md "wikilink")[英国宣战](../Page/英国.md "wikilink")，并开始围攻[直布罗陀](../Page/直布罗陀.md "wikilink")。

### 19世紀

  - [1842年](../Page/1842年.md "wikilink")：英军攻陷[吴淞炮台](../Page/吴淞.md "wikilink")，江南[提督](../Page/提督.md "wikilink")[陈化成率众英勇作战](../Page/陈化成.md "wikilink")，力竭牺牲。
  - [1865年](../Page/1865年.md "wikilink")：英國長老教會將基督教的福音傳入台灣
  - [1888年](../Page/1888年.md "wikilink")：《[国际歌](../Page/国际歌.md "wikilink")》诞生。
  - [1900年](../Page/1900年.md "wikilink")：[义和团进入](../Page/义和团.md "wikilink")[北京后](../Page/北京.md "wikilink")，[清政府命](../Page/清政府.md "wikilink")[荣禄派](../Page/荣禄.md "wikilink")“武衞中军”前往[东交民巷保护各国](../Page/东交民巷.md "wikilink")[使馆](../Page/使馆.md "wikilink")。西方列强[假照会激怒](../Page/假照会.md "wikilink")[慈禧太后](../Page/慈禧太后.md "wikilink")。

### 20世紀

  - [1903年](../Page/1903年.md "wikilink")：[亨利·福特在](../Page/亨利·福特.md "wikilink")[美国](../Page/美国.md "wikilink")[密歇根州](../Page/密歇根州.md "wikilink")[迪尔伯恩创立](../Page/迪尔伯恩.md "wikilink")[福特汽车公司](../Page/福特汽车.md "wikilink")。
  - [1908年](../Page/1908年.md "wikilink")：全中国掀起[立宪](../Page/立宪.md "wikilink")[请愿高潮](../Page/请愿.md "wikilink")。
  - [1916年](../Page/1916年.md "wikilink")：[全国学生联合会在](../Page/全国学生联合会.md "wikilink")[上海成立](../Page/上海.md "wikilink")。
  - [1917年](../Page/1917年.md "wikilink")：全俄[苏维埃第一次代表大会召开](../Page/苏维埃.md "wikilink")。
  - [1919年](../Page/1919年.md "wikilink")：。
  - [1921年](../Page/1921年.md "wikilink")：[胡适提出](../Page/胡适.md "wikilink")“打孔店”。
  - [1922年](../Page/1922年.md "wikilink")：[六·一六事變](../Page/六·一六事變.md "wikilink")：[陈炯明炮击广州](../Page/陈炯明.md "wikilink")，以武力驱赶孙文为首的广东中华民国政府。
  - [1924年](../Page/1924年.md "wikilink")：[黄埔陸軍軍官學校正式开学](../Page/黄埔陸軍軍官學校.md "wikilink")，当天举行开学典礼，象徵[國民革命軍](../Page/國民革命軍.md "wikilink")（今[中華民國國軍](../Page/中華民國國軍.md "wikilink")）建軍。
  - [1932年](../Page/1932年.md "wikilink")：[洛桑会议决定停止](../Page/洛桑会议.md "wikilink")[德国赔款](../Page/德国.md "wikilink")。
  - [1933年](../Page/1933年.md "wikilink")：[罗斯福签署](../Page/富兰克林·德拉诺·罗斯福.md "wikilink")[美国国家经济复兴法案](../Page/美国国家经济复兴法案.md "wikilink")。
  - [1935年](../Page/1935年.md "wikilink")：。
  - [1939年](../Page/1939年.md "wikilink")：《[中蘇通商條約](../Page/中蘇通商條約.md "wikilink")》簽定\[1\]。
  - [1940年](../Page/1940年.md "wikilink")：[法国](../Page/法国.md "wikilink")[总理](../Page/法国总理.md "wikilink")[雷诺辞职](../Page/保罗·雷诺.md "wikilink")，[元帅](../Page/法国元帅.md "wikilink")[贝当在](../Page/菲利普·贝当.md "wikilink")[波尔多出任新总理](../Page/波尔多.md "wikilink")。
  - [1944年](../Page/1944年.md "wikilink")：[美国](../Page/美国.md "wikilink")[B-29轰炸机空袭](../Page/B-29轰炸机.md "wikilink")[日本本土](../Page/日本.md "wikilink")[九州岛](../Page/九州岛.md "wikilink")，从[成都出发开始了对日本城市的毁灭性进攻](../Page/成都.md "wikilink")，直至日本投降方才结束。
  - [1949年](../Page/1949年.md "wikilink")：《[光明日报](../Page/光明日报.md "wikilink")》创刊。
  - [1953年](../Page/1953年.md "wikilink")：[民主德国政府宣布取消对抗议行为的禁令](../Page/民主德国.md "wikilink")，[东柏林工人为获得此权利而欢呼](../Page/东柏林.md "wikilink")。
  - [1958年](../Page/1958年.md "wikilink")：曾推动[匈牙利实行](../Page/匈牙利.md "wikilink")[多党制和退出](../Page/多党制.md "wikilink")[华沙条约组织的前总理](../Page/华沙条约组织.md "wikilink")[伊姆雷被秘密审判后以](../Page/纳吉·伊姆雷.md "wikilink")[叛国罪名处决](../Page/叛国.md "wikilink")。
  - [1960年](../Page/1960年.md "wikilink")：英国导演[阿尔弗雷德·希区柯克执导的惊悚恐怖片](../Page/阿尔弗雷德·希区柯克.md "wikilink")《[惊魂记](../Page/惊魂记.md "wikilink")》在美国首演。
  - [1961年](../Page/1961年.md "wikilink")：
      - [美国同意在南越派出的](../Page/美国.md "wikilink")685人军事顾问团，来扶植[越南共和國](../Page/越南共和國.md "wikilink")。
      - [苏联舞蹈家](../Page/苏联.md "wikilink")[魯道夫·紐瑞耶夫逃往西方](../Page/魯道夫·紐瑞耶夫.md "wikilink")。
      - [法国](../Page/法国.md "wikilink")[布列塔尼农民同镇压示威的警察发生冲突](../Page/布列塔尼.md "wikilink")。
  - [1962年](../Page/1962年.md "wikilink")：[彭德怀上万言书申辩](../Page/彭德怀.md "wikilink")。
  - [1963年](../Page/1963年.md "wikilink")：[苏联宇航员](../Page/苏联.md "wikilink")[捷列什科娃乘坐](../Page/瓦莲京娜·捷列什科娃.md "wikilink")[东方六号](../Page/东方六号.md "wikilink")[宇宙飞船进入地球轨道](../Page/宇宙飞船.md "wikilink")，成为首个进入[太空的女宇航员](../Page/太空.md "wikilink")。
  - [1964年](../Page/1964年.md "wikilink")：[77国集团成立](../Page/77国集团.md "wikilink")。
  - [1974年](../Page/1974年.md "wikilink")：[中華民國總統](../Page/中華民國總統.md "wikilink")[蔣中正在](../Page/蔣中正.md "wikilink")[中華民國陸軍軍官學校](../Page/中華民國陸軍軍官學校.md "wikilink")50週年校慶發表訓詞《黃埔精神與革命大業的再推進》，批判[批林批孔運動](../Page/批林批孔運動.md "wikilink")。
  - [1976年](../Page/1976年.md "wikilink")：
      - [索維托起義](../Page/索維托起義.md "wikilink")：[南非](../Page/南非.md "wikilink")[索維托小鎮黑人學生示威](../Page/索維托.md "wikilink")，遭白人警察開槍鎮壓；隨後事件蔓延至其他小鎮，並延至1977年才告平息。
      - [香港政府派人清拆](../Page/香港政府.md "wikilink")[沙田墟](../Page/沙田墟.md "wikilink")，以擴闊[大埔公路](../Page/大埔公路.md "wikilink")。
  - [1979年](../Page/1979年.md "wikilink")：[美国駐](../Page/美国.md "wikilink")[黎巴嫩大使](../Page/黎巴嫩.md "wikilink")[弗郎西斯·梅洛伊和](../Page/弗郎西斯·梅洛伊.md "wikilink")[参赞](../Page/参赞.md "wikilink")[理查德·沃林在](../Page/理查德·沃林.md "wikilink")[贝鲁特遭绑驾并被杀害](../Page/贝鲁特.md "wikilink")。
  - [1982年](../Page/1982年.md "wikilink")：中华人民共和国首批[博士产生](../Page/博士.md "wikilink")。
  - [1983年](../Page/1983年.md "wikilink")：11个[西欧国家研制的](../Page/西欧.md "wikilink")[-{zh-hans:阿丽亚娜一号运载火箭;zh-hk:亞里安一號運載火箭;zh-tw:亞利安一號運載火箭;}-发射成功](../Page/亞利安一號運載火箭.md "wikilink")，两颗[通讯卫星进入](../Page/通讯卫星.md "wikilink")[同步轨道](../Page/同步轨道.md "wikilink")。
  - [1988年](../Page/1988年.md "wikilink")：香港政府開始對抵港的[越南船民實施](../Page/越南船民.md "wikilink")「甄別政策」，该政策亦开始[通过广播播发](../Page/北漏洞拉.md "wikilink")。
  - [1991年](../Page/1991年.md "wikilink")：
      - [美国制成人血代用品](../Page/美国.md "wikilink")。
      - [菲律賓](../Page/菲律賓.md "wikilink")[皮納圖博火山爆發](../Page/皮納圖博火山.md "wikilink")。
  - [1992年](../Page/1992年.md "wikilink")：[中共中央](../Page/中共中央.md "wikilink")、[国务院决定加快发展](../Page/国务院.md "wikilink")[第三产业](../Page/第三产业.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：前[香港交易及結算所主席](../Page/香港交易及結算所.md "wikilink")[李福兆在服刑兩年多後獲提早出獄](../Page/李福兆.md "wikilink")。
  - [1994年](../Page/1994年.md "wikilink")：《中国知识产权保护状况》白皮书发表。
  - [1998年](../Page/1998年.md "wikilink")：。

### 21世紀

  - [2002年](../Page/2002年.md "wikilink")：[北京藍極速](../Page/北京.md "wikilink")[-{zh-tw:網咖;zh-hans:网吧;zh-hant:網咖;zh-cn:网吧;zh-hk:網吧;zh-mo:網吧;}-](../Page/網咖.md "wikilink")[發生大火](../Page/藍極速網吧縱火案.md "wikilink")，導致25人死亡、數十人受傷。
  - [2004年](../Page/2004年.md "wikilink")：[腾讯控股正式在香港交易所挂牌上市](../Page/腾讯控股.md "wikilink")，股票代码为0700。随后成为并保持多年中国市值最高互联网企业的地位。
  - [2006年](../Page/2006年.md "wikilink")：[台灣最長的](../Page/台灣.md "wikilink")[公路](../Page/公路.md "wikilink")[隧道](../Page/隧道.md "wikilink")－[雪山隧道正式完工通車](../Page/雪山隧道.md "wikilink")。
  - [2011年](../Page/2011年.md "wikilink")：[蓋達組織宣布](../Page/蓋達組織.md "wikilink")[扎瓦希里接替](../Page/扎瓦希里.md "wikilink")[賓·拉登成為領袖](../Page/賓·拉登.md "wikilink")。
  - [2012年](../Page/2012年.md "wikilink")：中国[神舟九号飞船在甘肃省](../Page/神舟九号.md "wikilink")[酒泉卫星发射中心由](../Page/酒泉卫星发射中心.md "wikilink")[长征二号F遥九运载火箭搭载升空](../Page/长征二号F遥九运载火箭.md "wikilink")，将与[天宫一号目标飞行器对接](../Page/天宫一号.md "wikilink")。
  - [2014年](../Page/2014年.md "wikilink")：[阿里巴巴正式向美国证监会提交F](../Page/阿里巴巴.md "wikilink")1/A招股书。阿里巴巴是中国首家全球互联网公司，此次交易极有可能成为美国资本市场史上最大IPO。
  - [2016年](../Page/2016年.md "wikilink")：[上海迪士尼樂園在](../Page/上海迪士尼樂園.md "wikilink")[上海](../Page/上海.md "wikilink")[浦東新區正式營運](../Page/浦東新區.md "wikilink")，擁有全球最高、最大的[迪士尼公主](../Page/迪士尼公主.md "wikilink")[城堡](../Page/城堡.md "wikilink")。

## 出生

  - [1139年](../Page/1139年.md "wikilink")：[近衛天皇](../Page/近衛天皇.md "wikilink")，[日本第](../Page/日本.md "wikilink")76代天皇。
  - [1888年](../Page/1888年.md "wikilink")：[亚历山大·弗里德曼](../Page/亚历山大·弗里德曼.md "wikilink")，苏联数学家、宇宙學家
  - [1902年](../Page/1902年.md "wikilink")：[巴巴拉·麦克林托克](../Page/巴巴拉·麦克林托克.md "wikilink")，[美国生物學家](../Page/美国.md "wikilink")。
  - [1908年](../Page/1908年.md "wikilink")：[沙立·他那叻](../Page/沙立·他那叻.md "wikilink")，[泰国军事强人](../Page/泰国.md "wikilink")。
  - [1947年](../Page/1947年.md "wikilink")：[李錫錕](../Page/李錫錕.md "wikilink")，[台灣政治學學者](../Page/台灣.md "wikilink")。
  - [1962年](../Page/1962年.md "wikilink")：[黃耀明](../Page/黃耀明.md "wikilink")，[香港男歌手](../Page/香港.md "wikilink")。
  - [1972年](../Page/1972年.md "wikilink")：[約翰·趙](../Page/約翰·趙.md "wikilink")，韓裔美國電影演員。
  - [1978年](../Page/1978年.md "wikilink")：[梁靜茹](../Page/梁靜茹.md "wikilink")，[馬來西亞知名女歌手](../Page/馬來西亞.md "wikilink")。
  - [1980年](../Page/1980年.md "wikilink")：[容祖兒](../Page/容祖兒.md "wikilink")，[香港女歌手](../Page/香港.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[程予希](../Page/程予希.md "wikilink")，[台灣女演員](../Page/台灣.md "wikilink")。
  - [1991年](../Page/1991年.md "wikilink")：[顏慧萍](../Page/顏慧萍.md "wikilink")，[馬來西亞女歌手](../Page/馬來西亞.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：[朴寶劍](../Page/朴寶劍.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")。
  - [1995年](../Page/1995年.md "wikilink")：[王子聰](../Page/王子聰.md "wikilink")，[香港](../Page/香港.md "wikilink")[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")。
  - 1995年：[奇喜賢](../Page/奇喜賢.md "wikilink")，[韓國女子偶像團體](../Page/韓國.md "wikilink")[DIA隊長](../Page/DIA.md "wikilink")。
  - 1995年：[朱多英](../Page/朱多英.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")。
  - [2000年](../Page/2000年.md "wikilink")：[范丞丞](../Page/范丞丞.md "wikilink")，[中國男子偶像團體](../Page/中國.md "wikilink")[NINE
    PERCENT成員](../Page/NINE_PERCENT.md "wikilink")。

## 逝世

  - [1216年](../Page/1216年.md "wikilink")：[英諾森三世](../Page/英諾森三世.md "wikilink")，[教宗](../Page/教宗.md "wikilink")。
  - [1842年](../Page/1842年.md "wikilink")：[陳化成](../Page/陳化成.md "wikilink")，[中國](../Page/中國.md "wikilink")[清朝江南](../Page/清朝.md "wikilink")[提督](../Page/提督.md "wikilink")。
  - [1969年](../Page/1969年.md "wikilink")：[哈罗德·亚历山大](../Page/哈罗德·亚历山大.md "wikilink")，[英国军官](../Page/英国.md "wikilink")。
  - [2000年](../Page/2000年.md "wikilink")：[香淳皇后](../Page/香淳皇后.md "wikilink")，[日本](../Page/日本.md "wikilink")[昭和天皇皇后](../Page/昭和天皇.md "wikilink")，[今上天皇](../Page/今上天皇.md "wikilink")[明仁之母](../Page/明仁.md "wikilink")。
  - [2012年](../Page/2012年.md "wikilink")：[纳伊夫·本·阿卜杜勒-阿齐兹](../Page/纳伊夫·本·阿卜杜勒-阿齐兹.md "wikilink")，沙特阿拉伯王储（生于[1933年](../Page/1933年.md "wikilink")）
  - 2012年：[陳廷驊](../Page/陳廷驊.md "wikilink")，[香港企業家](../Page/香港.md "wikilink")（生于[1923年](../Page/1923年.md "wikilink")）

## 參考資料

<references/>

1.  [（中華民國）全國法規資料庫入口網站](http://law.moj.gov.tw/LawClass/LawAll.aspx?PCode=Y0070129)