**6月22日**是[公历一年中的第](../Page/公历.md "wikilink")173天（[闰年第](../Page/闰年.md "wikilink")174天），离全年结束还有192天。

## 大事记

### 前3世紀

  - [前217年](../Page/前217年.md "wikilink")：在[第四次叙利亚战争中](../Page/叙利亚战争.md "wikilink")，埃及[托勒密王朝国王](../Page/托勒密王朝.md "wikilink")[托勒密四世率领的军队在](../Page/托勒密四世.md "wikilink")[拉菲亞战役中战胜马其顿](../Page/拉菲亞战役.md "wikilink")[塞琉古王朝国王](../Page/塞琉古帝国.md "wikilink")[安条克三世率领的军队](../Page/安条克三世.md "wikilink")。

### 前2世紀

  - [前168年](../Page/前168年.md "wikilink")：在[第三次马其顿战争中](../Page/第三次马其顿战争.md "wikilink")，[马其顿国王](../Page/马其顿王国.md "wikilink")[珀尔修斯率领的军队在](../Page/珀尔修斯_\(马其顿\).md "wikilink")[彼得那战役被](../Page/彼得那战役.md "wikilink")[罗马军队击败](../Page/罗马共和国.md "wikilink")。

### 4世紀

  - [318年](../Page/318年.md "wikilink")：[晋元帝大舆元年五月](../Page/晋元帝.md "wikilink")
    ，[段匹磾杀](../Page/段匹磾.md "wikilink")[刘琨](../Page/刘琨.md "wikilink")。

### 16世紀

  - [1543年](../Page/1543年.md "wikilink")：[英格兰国王](../Page/英国君主列表.md "wikilink")[亨利八世向](../Page/亨利八世.md "wikilink")[法国发出最后通牒](../Page/法国.md "wikilink")，宣布开战。

### 17世紀

  - [1633年](../Page/1633年.md "wikilink")：羅馬教廷強迫[伽利略撤銷](../Page/伽利略·伽利萊.md "wikilink")“太陽是宇宙的中心，地球非宇宙的中心”的觀點。

### 19世紀

  - [1815年](../Page/1815年.md "wikilink")：[法兰西帝国皇帝](../Page/法兰西第一帝国.md "wikilink")[拿破仑在](../Page/拿破仑·波拿巴.md "wikilink")[滑铁卢战役战败后宣布退位](../Page/滑铁卢战役.md "wikilink")，其子[拿破仑二世继承皇位](../Page/拿破仑二世.md "wikilink")。
  - [1826年](../Page/1826年.md "wikilink")：[西蒙·玻利瓦尔成功组织的](../Page/西蒙·玻利瓦尔.md "wikilink")[泛美會議在](../Page/泛美會議.md "wikilink")[巴拿马举行](../Page/巴拿马.md "wikilink")。
  - [1840年](../Page/1840年.md "wikilink")：[英军总司令](../Page/英军.md "wikilink")，全权代表[乔治·义律宣布自](../Page/乔治·义律.md "wikilink")[6月28日起封锁](../Page/6月28日.md "wikilink")[珠江口](../Page/珠江口.md "wikilink")，[第一次鸦片战争正式爆发](../Page/第一次鸦片战争.md "wikilink")。
  - [1895年](../Page/1895年.md "wikilink")：[臺灣爆发](../Page/臺灣.md "wikilink")[新竹保衛戰](../Page/乙未戰爭#戰爭過程#戰爭第二階段.md "wikilink")，新竹城軍民反抗[日本佔領](../Page/台灣日治時期.md "wikilink")。

### 20世紀

  - [1908年](../Page/1908年.md "wikilink")：[日本](../Page/日本.md "wikilink")[东京的](../Page/东京.md "wikilink")[社会主义者举行欢迎出狱](../Page/社会主义.md "wikilink")[同志大会](../Page/同志.md "wikilink")，会后举着红旗唱着革命歌曲走上街头，遭警察镇压，[大杉荣等十五人被捕并判处](../Page/大杉荣.md "wikilink")[死刑](../Page/死刑.md "wikilink")，史称“[赤旗事件](../Page/赤旗事件.md "wikilink")”。
  - [1911年](../Page/1911年.md "wikilink")：[喬治五世加冕為英國國王](../Page/喬治五世_\(英國\).md "wikilink")。
  - [1940年](../Page/1940年.md "wikilink")：[德国和](../Page/德国.md "wikilink")[法国代表在康边森林的火车上召开停战会议](../Page/法国.md "wikilink")，法代表洪特辛格尔将军和德方代表季特尔将军分别代表本国政府于下午正式签订停戰協定。
  - [1941年](../Page/1941年.md "wikilink")：[纳粹德国发动](../Page/纳粹德国.md "wikilink")[巴巴罗萨作战](../Page/巴巴罗萨作战.md "wikilink")，與其盟國大规模入侵[蘇聯](../Page/蘇聯.md "wikilink")，[苏德战争爆发](../Page/苏德战争.md "wikilink")。
  - [1942年](../Page/1942年.md "wikilink")：驻[基辅的](../Page/基辅.md "wikilink")[德国占领军邀请](../Page/德国.md "wikilink")[蘇聯基辅当地的狄纳莫足球队进行足球比赛](../Page/蘇聯.md "wikilink")，蘇聯队获胜后德国[法西斯在球赛结束后就杀害了五名队员](../Page/法西斯.md "wikilink")。
  - [1944年](../Page/1944年.md "wikilink")：[蘇聯軍隊發動](../Page/蘇聯.md "wikilink")[巴格拉基昂行動](../Page/巴格拉基昂行動.md "wikilink")，開始對當時佔領[白俄羅斯的](../Page/白俄羅斯.md "wikilink")[納粹德國軍隊](../Page/納粹德國.md "wikilink")[德國中央集團軍大規模的進攻](../Page/德國中央集團軍.md "wikilink")。
  - [1956年](../Page/1956年.md "wikilink")：[阿尔及尔卡斯巴教堂发生大爆炸](../Page/阿尔及尔.md "wikilink")：3座楼房遭到破坏，在碎砖瓦砾中发现了370具尸体。
  - 1956年：[纳塞尔被选为](../Page/纳塞尔.md "wikilink")[埃及总统](../Page/埃及总统.md "wikilink")。
  - [1965年](../Page/1965年.md "wikilink")：[日本和](../Page/日本.md "wikilink")[南韩建交](../Page/南韩.md "wikilink")。
  - [1973年](../Page/1973年.md "wikilink")：[宇航员在](../Page/宇航员.md "wikilink")[美国第一座](../Page/美国.md "wikilink")[地球轨道](../Page/地球轨道.md "wikilink")[空间站](../Page/空间站.md "wikilink")[天空實驗室2號飞行](../Page/天空實驗室2號.md "wikilink")28天后安全返回[地球](../Page/地球.md "wikilink")。
  - 1973年：苏联领导人[列昂尼德·伊里奇·勃列日涅夫在](../Page/列昂尼德·伊里奇·勃列日涅夫.md "wikilink")[华盛顿与](../Page/华盛顿哥伦比亚特区.md "wikilink")[美国总统](../Page/美国总统.md "wikilink")[理查德·尼克松签署](../Page/理查德·尼克松.md "wikilink")《[苏美防止核战争协定](../Page/苏美防止核战争协定.md "wikilink")》。
  - [1976年](../Page/1976年.md "wikilink")：[加拿大國會下議院批准废除](../Page/加拿大國會下議院.md "wikilink")[死刑](../Page/死刑.md "wikilink")。
  - [1978年](../Page/1978年.md "wikilink")：[美国天文学家](../Page/美国.md "wikilink")[詹姆斯·克里斯蒂在检查](../Page/詹姆斯·克里斯蒂.md "wikilink")[冥王星的照片时](../Page/冥王星.md "wikilink")，发现其[卫星](../Page/卫星.md "wikilink")[冥卫一](../Page/冥卫一.md "wikilink")。
  - [1979年](../Page/1979年.md "wikilink")：[中华人民共和国与](../Page/中华人民共和国.md "wikilink")[爱尔兰建交](../Page/爱尔兰.md "wikilink")。
  - [1982年](../Page/1982年.md "wikilink")：[民主柬埔寨联合政府成立](../Page/柬埔寨.md "wikilink")。
  - [1984年](../Page/1984年.md "wikilink")：[邓小平分别会见](../Page/邓小平.md "wikilink")[香港工商界访京团和香港知名人士](../Page/香港.md "wikilink")[鍾士元发表谈话](../Page/鍾士元.md "wikilink")：香港实行[一国两制五十年不变](../Page/一国两制.md "wikilink")。
  - [1986年](../Page/1986年.md "wikilink")：在[墨西哥世界杯足球赛](../Page/1986年世界杯足球赛.md "wikilink")[阿根廷对](../Page/阿根廷國家足球隊.md "wikilink")[英格兰的比赛中](../Page/英格蘭足球代表隊.md "wikilink")，[马拉多纳攻入](../Page/迭戈·马拉多纳.md "wikilink")[上帝之手和](../Page/上帝之手.md "wikilink")[世纪最佳进球两个入球](../Page/世纪最佳进球.md "wikilink")。
  - [1990年](../Page/1990年.md "wikilink")：[兩德統一的大门正式开启](../Page/兩德統一.md "wikilink")：[民主德国人民议院和](../Page/民主德国人民议院.md "wikilink")[联邦德国联邦议院分别以压倒多数](../Page/联邦德国联邦议院.md "wikilink")，正式批准1990年5月双方签订的关于两国建立[经济](../Page/经济.md "wikilink")、[货币和社会联盟的](../Page/货币.md "wikilink")[国家条约](../Page/国家条约.md "wikilink")。
  - [1991年](../Page/1991年.md "wikilink")：[陕甘宁盆地发现大型](../Page/陕甘宁盆地.md "wikilink")[天然气田](../Page/天然气.md "wikilink")，即[陕甘宁气田](../Page/陕甘宁气田.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：[中国](../Page/中国.md "wikilink")[银河全数字仿真II计算机研制成功](../Page/银河计算机.md "wikilink")。
  - [1994年](../Page/1994年.md "wikilink")：[日本首次承认](../Page/日本.md "wikilink")[二战时](../Page/第二次世界大战.md "wikilink")[强掳中国人当劳工](../Page/日本强掳中国劳工问题.md "wikilink")。
  - [1998年](../Page/1998年.md "wikilink")：[香港特區政府宣佈暫停賣地至](../Page/香港特區政府.md "wikilink")[次年](../Page/1999年.md "wikilink")3月。
  - 1998年：[香港地鐵](../Page/香港地鐵.md "wikilink")[東涌綫正式通車](../Page/東涌綫.md "wikilink")。
  - [2000年](../Page/2000年.md "wikilink")：[武漢航空343號班機在武漢遭雷擊墜毀](../Page/武漢航空343號班機空難.md "wikilink")，造成機上42人及地面7人遇難。

### 21世紀

  - [2002年](../Page/2002年.md "wikilink")：[伊朗北部發生了](../Page/伊朗.md "wikilink")[里氏規模](../Page/里氏.md "wikilink")6.1的[地震](../Page/地震.md "wikilink")，造成了261人喪生。
  - [2006年](../Page/2006年.md "wikilink")︰[加拿大總理](../Page/加拿大總理.md "wikilink")[哈珀於](../Page/史蒂芬·哈珀.md "wikilink")[國會下議院就](../Page/加拿大國會.md "wikilink")[人頭稅正式向加拿大華人道歉](../Page/人頭稅.md "wikilink")。
  - [2012年](../Page/2012年.md "wikilink")：[巴拉圭总统](../Page/巴拉圭总统.md "wikilink")[费尔南多·卢戈遭参议院弹劾下台](../Page/费尔南多·卢戈.md "wikilink")，副总统[费德里科·弗朗哥接任总统一职](../Page/费德里科·弗朗哥.md "wikilink")。
  - [2014年](../Page/2014年.md "wikilink")：[让爱与和平占领中环舉行](../Page/让爱与和平占领中环.md "wikilink")[6.22民间全民投票讓香港市民選擇普選方案](../Page/6.22民间全民投票.md "wikilink")。

## 出生

  - [662年](../Page/662年.md "wikilink")：[唐睿宗李旦](../Page/唐睿宗.md "wikilink")，[唐朝皇帝](../Page/唐朝.md "wikilink")（[716年去世](../Page/716年.md "wikilink")）
  - [1704年](../Page/1704年.md "wikilink")：[約翰·泰勒](../Page/約翰·泰勒.md "wikilink")，[美國第十任總統](../Page/美國.md "wikilink")
  - [1767年](../Page/1767年.md "wikilink")：[威廉·馮·洪堡](../Page/威廉·馮·洪堡.md "wikilink")，[德國哲學家和政治家](../Page/德國.md "wikilink")
  - [1805年](../Page/1805年.md "wikilink")：[朱塞佩·馬志尼](../Page/朱塞佩·馬志尼.md "wikilink")，[義大利革命家](../Page/義大利.md "wikilink")
  - [1864年](../Page/1864年.md "wikilink")：[赫爾曼·閔可夫斯基](../Page/赫爾曼·閔可夫斯基.md "wikilink")，德國數學家，曾經是物理學家[愛因斯坦的老師](../Page/愛因斯坦.md "wikilink")
  - [1887年](../Page/1887年.md "wikilink")：[朱利安·赫胥黎](../Page/朱利安·赫胥黎.md "wikilink")，英國生物學家
  - [1910年](../Page/1910年.md "wikilink")：[康拉德·楚澤](../Page/康拉德·楚澤.md "wikilink")，德國工程師
  - [1932年](../Page/1932年.md "wikilink")：[索拉雅·伊凡迪亞利-巴克提亞利](../Page/索拉雅·伊凡迪亞利-巴克提亞利.md "wikilink")，[伊朗前皇后](../Page/伊朗.md "wikilink")（[2001年去世](../Page/2001年.md "wikilink")）
  - [1933年](../Page/1933年.md "wikilink")：[黛安·費恩斯坦](../Page/黛安·費恩斯坦.md "wikilink")，[美國政治人物](../Page/美國.md "wikilink")，1992年起代表[加利福尼亞州的美國聯邦](../Page/加利福尼亞州.md "wikilink")[參議員](../Page/美國參議院.md "wikilink")，也是[舊金山首任以及至今唯一的女市長](../Page/舊金山.md "wikilink")
  - [1937年](../Page/1937年.md "wikilink")：[陳履安](../Page/陳履安.md "wikilink")，[中華民國政治人物](../Page/中華民國.md "wikilink")
  - [1947年](../Page/1947年.md "wikilink")：[皮特·馬拉維奇](../Page/皮特·馬拉維奇.md "wikilink")，美國籃球運動員
  - [1949年](../Page/1949年.md "wikilink")：[梅麗·史翠普](../Page/梅麗·史翠普.md "wikilink")，美國[影星](../Page/电影明星.md "wikilink")
  - [1954年](../Page/1954年.md "wikilink")：[鄭漢溶](../Page/鄭漢溶.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")
  - [1956年](../Page/1956年.md "wikilink")：[沙阿·迈赫穆德·库雷希](../Page/沙阿·迈赫穆德·库雷希.md "wikilink")，[巴基斯坦政治家](../Page/巴基斯坦.md "wikilink")
  - [1957年](../Page/1957年.md "wikilink")：[劉江華](../Page/劉江華.md "wikilink")，[香港政治人物](../Page/香港.md "wikilink")
  - [1958年](../Page/1958年.md "wikilink")：[布魯斯·坎貝爾](../Page/布魯斯·坎貝爾.md "wikilink")，[美國電影演員](../Page/美國電影.md "wikilink")
  - [1962年](../Page/1962年.md "wikilink")：[周星馳](../Page/周星馳.md "wikilink")，[香港電影導演](../Page/香港電影.md "wikilink")、演員
  - 1962年：[-{zh-hans:克莱德·德雷克斯勒;zh-hk:克萊德·德士拿;zh-tw:克萊德·崔斯勒;}-](../Page/克萊德·崔斯勒.md "wikilink")，美國篮球运动员
  - [1964年](../Page/1964年.md "wikilink")：[丹·布朗](../Page/丹·布朗.md "wikilink")，美國小說家
  - 1964年：[阿部寬](../Page/阿部寬.md "wikilink")，[日本](../Page/日本.md "wikilink")[演員](../Page/演員.md "wikilink")
  - 1966年：[鈕承澤](../Page/鈕承澤.md "wikilink")，台灣導演、演員
  - [1968年](../Page/1968年.md "wikilink")：柳美里，[日本](../Page/日本.md "wikilink")[小說家和](../Page/小說家.md "wikilink")[劇作家](../Page/劇作家.md "wikilink")
  - [1971年](../Page/1971年.md "wikilink")：[刀郎](../Page/刀郎.md "wikilink")，[中國大陸](../Page/中國大陸.md "wikilink")[歌手](../Page/歌手.md "wikilink")、詞曲創作人、音樂製作人
  - [1972年](../Page/1972年.md "wikilink")：[土門仁](../Page/土門仁.md "wikilink")，日本[聲優](../Page/聲優.md "wikilink")
  - [1974年](../Page/1974年.md "wikilink")：[李斌](../Page/李斌.md "wikilink")，中國[企業家](../Page/企業家.md "wikilink")
  - [1974年](../Page/1974年.md "wikilink")：[內藤玲](../Page/內藤玲.md "wikilink")，日本聲優
  - [1977年](../Page/1977年.md "wikilink")：[小野涼子](../Page/小野涼子.md "wikilink")，日本女性聲優
  - [1978年](../Page/1978年.md "wikilink")：[戴愛玲](../Page/戴愛玲.md "wikilink")，[台灣歌手](../Page/台灣歌手.md "wikilink")
  - 1978年：[丹·威爾頓](../Page/丹·威爾頓.md "wikilink")，英國賽車手
  - [1981年](../Page/1981年.md "wikilink")：[天野洋一](../Page/天野洋一.md "wikilink")，日本[漫畫家](../Page/漫畫家.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[伊恩·金斯勒](../Page/伊恩·金斯勒.md "wikilink")，美國棒球選手
  - [1984年](../Page/1984年.md "wikilink")：[達斯汀·約翰遜](../Page/達斯汀·約翰遜.md "wikilink")，美國高爾夫球手
  - 1984年：[揚科·蒂普薩雷維奇](../Page/揚科·蒂普薩雷維奇.md "wikilink")，塞爾維亞網球選手
  - 1984年：[周孝安](../Page/周孝安.md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")
  - 1984年：[郭文珺](../Page/郭文珺.md "wikilink")，中國女子[射擊運動員](../Page/射擊.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[小野友樹](../Page/小野友樹.md "wikilink")，日本[聲優](../Page/聲優.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[加藤羅莎](../Page/加藤羅莎.md "wikilink")，日本女演員、模特
  - 1987年：[張嘉倪](../Page/張嘉倪.md "wikilink")，中國電視演員
  - 1987年：[李敏鎬](../Page/李敏鎬.md "wikilink")，[韓國演員](../Page/韓國.md "wikilink")
  - 1987年：[吳漣序](../Page/吳漣序.md "wikilink")，[韓國演員](../Page/韓國.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[江晨希](../Page/江晨希.md "wikilink")，[台灣女演員](../Page/台灣.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[鄭容和](../Page/鄭容和.md "wikilink")，[韓國男歌手](../Page/韓國.md "wikilink")、演員、乐队[CNBLUE成员](../Page/CNBLUE.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[伊野尾慧](../Page/伊野尾慧.md "wikilink")，日本演員、歌手、团体[Hey\!
    Say\! JUMP成员](../Page/Hey!_Say!_JUMP.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[戴莉亞·迪米崔耶娃](../Page/戴莉亞·迪米崔耶娃.md "wikilink")，[俄羅斯](../Page/俄羅斯.md "wikilink")[韻律體操選手](../Page/韻律體操.md "wikilink")
  - [1995年](../Page/1995年.md "wikilink")，[周興哲](../Page/周興哲.md "wikilink")，[台灣男歌手](../Page/台灣.md "wikilink")
  - [龍小菌](../Page/龍小菌.md "wikilink")，[香港網絡唱作歌手](../Page/香港.md "wikilink")

## 逝世

  - [1865年](../Page/1865年.md "wikilink")：[王茂荫](../Page/王茂荫.md "wikilink")（[1798年出生](../Page/1798年.md "wikilink")）道光[进士](../Page/进士.md "wikilink")，主张发行[纸币](../Page/纸币.md "wikilink")，[卡尔·马克思曾在](../Page/卡尔·马克思.md "wikilink")《[资本论](../Page/资本论.md "wikilink")》中认为他是个有远见的人物）
  - [1951年](../Page/1951年.md "wikilink")：[高剑父](../Page/高剑父.md "wikilink")，岭南画派第一代画家（[1879年出生](../Page/1879年.md "wikilink")）
  - [1967年](../Page/1967年.md "wikilink")：[李立三](../Page/李立三.md "wikilink")，中共前领导人（[1899年出生](../Page/1899年.md "wikilink")）
  - [1977年](../Page/1977年.md "wikilink")：[胡璉](../Page/胡璉.md "wikilink")，在[金門砲戰](../Page/金門砲戰.md "wikilink")（八二三砲戰）中成功防衛[金門的](../Page/金門.md "wikilink")[上將](../Page/上將.md "wikilink")（[1907年出生](../Page/1907年.md "wikilink")）
  - [1988年](../Page/1988年.md "wikilink")：[萧军](../Page/萧军.md "wikilink")，[小说家](../Page/小说家.md "wikilink")（[1907年出生](../Page/1907年.md "wikilink")）
  - [1993年](../Page/1993年.md "wikilink")：[帕特·尼克森](../Page/帕特·尼克森.md "wikilink")，第37任美國總統[理查·尼克森妻子](../Page/理查·尼克森.md "wikilink")，前第一夫人（[1912年出生](../Page/1912年.md "wikilink")）
  - [2000年](../Page/2000年.md "wikilink")：[赵一荻](../Page/赵一荻.md "wikilink")，[张学良第三任](../Page/张学良.md "wikilink")（最后一任）夫人（[1912年出生](../Page/1912年.md "wikilink")）
  - [2002年](../Page/2002年.md "wikilink")：[張徹](../Page/張徹.md "wikilink")，香港武俠電影導演（[1923年出生](../Page/1923年.md "wikilink")）
  - [2015年](../Page/2015年.md "wikilink")：[小玉](../Page/小玉_\(貓\).md "wikilink")（），[日本](../Page/日本.md "wikilink")[和歌山電鐵](../Page/和歌山電鐵.md "wikilink")[貴志川線](../Page/貴志川線.md "wikilink")[貴志站的雌性](../Page/貴志站.md "wikilink")[三色貓站長](../Page/三色貓.md "wikilink")\[1\]（[1999年出生](../Page/1999年.md "wikilink")）

## 节日、风俗习惯

## 參考資料

1.