**6月2日**是[公历一年中的第](../Page/公历.md "wikilink")153天（[闰年第](../Page/闰年.md "wikilink")154天），离全年结束还有212天。

## 大事记

### 3世紀

  - [260年](../Page/260年.md "wikilink")：[魏帝](../Page/曹魏.md "wikilink")[曹髦讨伐](../Page/曹髦.md "wikilink")[司马昭](../Page/司马昭.md "wikilink")，兵敗被杀。

### 5世紀

  - [455年](../Page/455年.md "wikilink")：[汪达尔人进入](../Page/汪达尔人.md "wikilink")[罗马城](../Page/罗马市.md "wikilink")，开始进行长达两个星期的洗劫。

### 18世紀

  - [1793年](../Page/1793年.md "wikilink")：[巴黎新的起义](../Page/巴黎.md "wikilink")－[法国革命](../Page/法国革命.md "wikilink")，[法国](../Page/法国.md "wikilink")[雅各宾派执政](../Page/雅各賓俱樂部.md "wikilink")。

### 19世紀

  - [1815年](../Page/1815年.md "wikilink")：[拿破仑一世颁布](../Page/拿破仑一世.md "wikilink")[法国](../Page/法国.md "wikilink")[自由宪法](../Page/法国自由宪法.md "wikilink")。
  - [1847年](../Page/1847年.md "wikilink")：[共产主义者同盟第一次代表大会在](../Page/共产主义者同盟.md "wikilink")[比利时召开](../Page/比利时.md "wikilink")。
  - [1886年](../Page/1886年.md "wikilink")：[美国总统](../Page/美国总统.md "wikilink")[格罗弗·克利夫兰於](../Page/格罗弗·克利夫兰.md "wikilink")[白宮舉行婚禮](../Page/白宮.md "wikilink")，是史上唯一一位在任期內結婚的美國總統。
  - [1895年](../Page/1895年.md "wikilink")：[李经方与](../Page/李经方.md "wikilink")[桦山资纪为](../Page/桦山资纪.md "wikilink")[马关条约于基隆海上办交接仪式](../Page/马关条约.md "wikilink")。
  - [1896年](../Page/1896年.md "wikilink")：[意大利发明家](../Page/意大利.md "wikilink")[古列尔莫·马可尼在](../Page/古列尔莫·马可尼.md "wikilink")[英国获得他发明的](../Page/英国.md "wikilink")[无线电技术的](../Page/无线电.md "wikilink")[专利](../Page/专利.md "wikilink")。

### 20世紀

  - [1907年](../Page/1907年.md "wikilink")：[广东](../Page/廣東省_\(清\).md "wikilink")[惠州七女湖起义](../Page/惠州七女湖起义.md "wikilink")。
  - [1939年](../Page/1939年.md "wikilink")：[古巴拒绝](../Page/古巴.md "wikilink")[犹太难民船入境](../Page/犹太人.md "wikilink")。
  - [1942年](../Page/1942年.md "wikilink")：[中華民國和](../Page/中華民國.md "wikilink")[美国](../Page/美国.md "wikilink")《[中美抵抗侵略互助协定](../Page/中美抵抗侵略互助协定.md "wikilink")》在[华盛顿签订](../Page/华盛顿哥伦比亚特区.md "wikilink")。
  - [1946年](../Page/1946年.md "wikilink")：[意大利举行决定](../Page/意大利.md "wikilink")[国家政体的](../Page/国家政体.md "wikilink")[公民投票](../Page/公民投票.md "wikilink")，[意大利王国被废除](../Page/意大利王國_\(1861年–1946年\).md "wikilink")，意大利共和国成立。
  - [1949年](../Page/1949年.md "wikilink")：[青即战役结束](../Page/青即战役.md "wikilink")，[中国人民解放军攻占](../Page/中国人民解放军.md "wikilink")[青岛](../Page/青岛市.md "wikilink")，[中国共产党接管政权](../Page/中国共产党.md "wikilink")，改青岛[院辖市为](../Page/院辖市.md "wikilink")[山东省](../Page/山东省.md "wikilink")[省辖市](../Page/省辖市.md "wikilink")。
  - [1953年](../Page/1953年.md "wikilink")：[英国女王](../Page/英国女王.md "wikilink")[伊丽莎白二世在](../Page/伊丽莎白二世.md "wikilink")[西敏寺举行加冕典礼](../Page/西敏寺.md "wikilink")。
  - [1962年](../Page/1962年.md "wikilink")：廣州火車東站（[大沙頭站](../Page/大沙头站.md "wikilink")）聚集大量擁港民眾鼓譟，淩晨1點，當局宣佈戒嚴，警方出動民眾散去，市公安局組織警力抓捕民眾，據計有16人被逮捕，被[勞動教養有](../Page/劳动教养.md "wikilink")22人，被[行政拘留有](../Page/行政拘留.md "wikilink")34人。
  - [1964年](../Page/1964年.md "wikilink")：中国[山东省最长的](../Page/山东省.md "wikilink")[公交](../Page/公共交通.md "wikilink")[無軌電車线路暨青岛市第二条无轨电车线路](../Page/無軌電車.md "wikilink")：5路（[火车站至](../Page/青岛站.md "wikilink")[四方北岭段](../Page/四方站.md "wikilink")）建成投产。
  - [1966年](../Page/1966年.md "wikilink")：在[美國探測](../Page/美國.md "wikilink")[月球的](../Page/月球.md "wikilink")[測量員計畫中](../Page/測量員計畫.md "wikilink")，[測量員1號登陸](../Page/測量員1號.md "wikilink")[月球](../Page/月球.md "wikilink")。
  - [1983年](../Page/1983年.md "wikilink")：[加拿大航空797號班機發生大火](../Page/加拿大航空797號班機空難.md "wikilink")，23人死亡。
  - [1989年](../Page/1989年.md "wikilink")：[中國國民黨第十三屆中央委員會第二次全體會議及中央評議委員第二次會議在](../Page/中國國民黨第十三屆中央委員會.md "wikilink")[陽明山](../Page/陽明山國家公園.md "wikilink")[中山樓召開](../Page/中山樓_\(陽明山\).md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：[香港發生](../Page/香港.md "wikilink")[北角地盤升降機墜下事故](../Page/北角地盤升降機墜下事故.md "wikilink")，造成12人死亡。
  - [1994年](../Page/1994年.md "wikilink")：[中国工程院产生首批](../Page/中国工程院.md "wikilink")[院士](../Page/中国工程院院士.md "wikilink")。
  - [1998年](../Page/1998年.md "wikilink")：[欧洲中央银行正式成立](../Page/欧洲中央银行.md "wikilink")。

### 21世紀

  - [2003年](../Page/2003年.md "wikilink")：[欧洲空间局首个](../Page/欧洲空间局.md "wikilink")[火星](../Page/火星.md "wikilink")[探测器](../Page/探测器.md "wikilink")[火星快車號搭乘](../Page/火星快車號.md "wikilink")[俄罗斯](../Page/俄罗斯.md "wikilink")[联盟号运载火箭发射升空](../Page/聯合運載火箭.md "wikilink")。
  - [2004年](../Page/2004年.md "wikilink")：[上海](../Page/上海市.md "wikilink")[F1赛道全面建成](../Page/一级方程式赛车.md "wikilink")。
  - 2010年：[日本第](../Page/日本.md "wikilink")93任[日本首相](../Page/日本內閣總理大臣.md "wikilink")[鳩山友紀夫辭任首相一職](../Page/鳩山友紀夫.md "wikilink")。
  - [2012年](../Page/2012年.md "wikilink")：[埃及](../Page/埃及.md "wikilink")[前总统](../Page/埃及總統列表.md "wikilink")[穆罕默德·胡斯尼·穆巴拉克因参与杀害](../Page/穆罕默德·胡斯尼·穆巴拉克.md "wikilink")[2011年埃及革命示威者被判处](../Page/2011年埃及革命.md "wikilink")[终身监禁](../Page/無期徒刑.md "wikilink")。
  - [2013年](../Page/2013年.md "wikilink")：[台灣發生規模最強](../Page/台灣.md "wikilink")6級的[602地震](../Page/2013年6月南投地震.md "wikilink")，有感地區多為西半部。
  - [2014年](../Page/2014年.md "wikilink")：[西班牙國王](../Page/西班牙.md "wikilink")[胡安·卡洛斯一世宣布退位](../Page/胡安·卡洛斯一世.md "wikilink")。

## 出生

  - [1535年](../Page/1535年.md "wikilink")：[良十一世](../Page/良十一世.md "wikilink")，[教宗](../Page/教宗.md "wikilink")（[1605年逝世](../Page/1605年.md "wikilink")）
  - [1835年](../Page/1835年.md "wikilink")：[庇护十世](../Page/庇护十世.md "wikilink")，[教宗](../Page/教宗.md "wikilink")（[1914年逝世](../Page/1914年.md "wikilink")）
  - [1840年](../Page/1840年.md "wikilink")：[托马斯·哈代](../Page/托马斯·哈代.md "wikilink")，[英国著名](../Page/英国.md "wikilink")[诗人及](../Page/诗人.md "wikilink")[小说家](../Page/小说家.md "wikilink")（[1928年逝世](../Page/1928年.md "wikilink")）
  - [1857年](../Page/1857年.md "wikilink")：[爱德华·埃尔加](../Page/爱德华·埃尔加.md "wikilink")，英国浪漫主義時期[作曲家](../Page/作曲家.md "wikilink")（[1934年逝世](../Page/1934年.md "wikilink")）
  - [1899年](../Page/1899年.md "wikilink"):
    [洛特·赖尼格](../Page/洛特·赖尼格.md "wikilink"),
    德國剪影動畫師及電影導演。([1981年逝世](../Page/1981年.md "wikilink")）
  - 1900年：[王蘧常](../Page/王蘧常.md "wikilink")，[中國历史学家](../Page/中國.md "wikilink")、著名书法家、诗人（[1989年逝世](../Page/1989年.md "wikilink")）
  - [1903年](../Page/1903年.md "wikilink")：[冯雪峰](../Page/冯雪峰.md "wikilink")，[中國作家](../Page/中國.md "wikilink")（[1976年逝世](../Page/1976年.md "wikilink")）
  - [1906年](../Page/1906年.md "wikilink")：[蔡儀](../Page/蔡儀.md "wikilink")，中國美術學家（[1992年逝世](../Page/1992年.md "wikilink")）
  - [1913年](../Page/1913年.md "wikilink")：[杜葉錫恩](../Page/杜葉錫恩.md "wikilink")，[香港政界人物](../Page/香港.md "wikilink")（[2015年逝世](../Page/2015年.md "wikilink")）
  - [1915年](../Page/1915年.md "wikilink")：[戴麟趾](../Page/戴麟趾.md "wikilink")，[英國資深](../Page/英国.md "wikilink")[香港](../Page/香港.md "wikilink")[殖民地官員](../Page/殖民地.md "wikilink")、第24任[香港總督](../Page/香港總督.md "wikilink")（[1988年逝世](../Page/1988年.md "wikilink")）
  - [1944年](../Page/1944年.md "wikilink")：[马文·哈姆利奇](../Page/马文·哈姆利奇.md "wikilink")，美国作曲家和指挥家（[2012年逝世](../Page/2012年.md "wikilink")）
  - [1944年](../Page/1944年.md "wikilink")：[平泉成](../Page/平泉成.md "wikilink")，日本演員
  - [1946年](../Page/1946年.md "wikilink")：[西村知道](../Page/西村知道.md "wikilink")，[日本](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1956年](../Page/1956年.md "wikilink")：[陈瑞仁](../Page/陈瑞仁.md "wikilink")，[中華民國檢察官](../Page/中華民國.md "wikilink")
  - [1965年](../Page/1965年.md "wikilink")：[今野緒雪](../Page/今野緒雪.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[小說家](../Page/小說家.md "wikilink")
  - 1965年：，[澳大利亚](../Page/澳大利亚.md "wikilink")[板球隊前隊長](../Page/板球.md "wikilink")；
  - 1965年：，[澳大利亚](../Page/澳大利亚.md "wikilink")[板球隊前副隊長](../Page/板球.md "wikilink")；史提夫的孿生弟弟
  - [1967年](../Page/1967年.md "wikilink")：[橋口隆志](../Page/橋口隆志.md "wikilink")，[日本](../Page/日本.md "wikilink")[漫畫家](../Page/漫画家.md "wikilink")
  - [1968年](../Page/1968年.md "wikilink")：[方岑](../Page/方岑.md "wikilink")，台灣演員
  - [1970年](../Page/1970年.md "wikilink")：[莫文蔚](../Page/莫文蔚.md "wikilink")，[香港](../Page/香港.md "wikilink")[歌手和](../Page/歌手.md "wikilink")[演員](../Page/演員.md "wikilink")
  - 1970年：[吉野弘幸](../Page/吉野弘幸.md "wikilink")，[日本](../Page/日本.md "wikilink")[編劇](../Page/編劇.md "wikilink")、小說家
  - [1972年](../Page/1972年.md "wikilink")：[何耀珊](../Page/何耀珊.md "wikilink")，[新加坡](../Page/新加坡.md "wikilink")[歌手](../Page/歌手.md "wikilink")
  - 1972年：[溫特沃思·米勒](../Page/溫特沃思·米勒.md "wikilink")，[美國](../Page/美國.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[黃文星](../Page/黃文星.md "wikilink")，[台灣歌手](../Page/臺灣.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[秋菜里子](../Page/秋菜里子.md "wikilink")，[日本](../Page/日本.md "wikilink")[AV女優](../Page/AV女優.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[曹錦輝](../Page/曹錦輝.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[旅美棒球選手](../Page/旅美棒球選手.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[姚家豪](../Page/姚家豪.md "wikilink")（阿Bu），[香港](../Page/香港.md "wikilink")[商業電台](../Page/商業電台.md "wikilink")[唱片騎師](../Page/唱片騎師.md "wikilink")
  - 1982年：[吳昶錫](../Page/吳昶錫.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[楊博壬](../Page/楊博壬.md "wikilink")，台灣棒球選手
  - [1985年](../Page/1985年.md "wikilink")：[澤城美雪](../Page/澤城美雪.md "wikilink")，[日本](../Page/日本.md "wikilink")[動畫](../Page/動畫.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[索納絲·辛哈](../Page/索納絲·辛哈.md "wikilink")，[印度女演員](../Page/印度.md "wikilink")
  - 1987年：，[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")[板球隊現任隊長](../Page/板球.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[齋藤彩夏](../Page/齋藤彩夏.md "wikilink")，日本動畫聲優
  - 1988年：[阿古路](../Page/阿古路.md "wikilink")，[阿根廷足球員](../Page/阿根廷.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[艾度](../Page/艾度.md "wikilink")，[美国足球員](../Page/美国.md "wikilink")
  - 1989年：，[澳洲](../Page/澳大利亚.md "wikilink")[板球隊現任隊長](../Page/板球.md "wikilink")
  - 1989年：[奧卡菲娜](../Page/奧卡菲娜.md "wikilink")，[美国饒舌歌手及女演員](../Page/美国.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[宇宙](../Page/宇宙_\(A'N'D\).md "wikilink")，[臺灣團體](../Page/臺灣.md "wikilink")（[A'N'D](../Page/A'N'D.md "wikilink")）成員
  - [2003年](../Page/2003年.md "wikilink")：，[香港童星](../Page/香港.md "wikilink")

## 逝世

  - [260年](../Page/260年.md "wikilink")：[魏帝](../Page/曹魏.md "wikilink")[曹髦](../Page/曹髦.md "wikilink")（－[241年](../Page/241年.md "wikilink")）
  - [396年](../Page/396年.md "wikilink")：[慕容垂](../Page/慕容垂.md "wikilink")，燕成武帝。[鮮卑族人](../Page/鮮卑族.md "wikilink")。十六國[後燕開國君主](../Page/後燕.md "wikilink")。前燕文明帝[慕容皝的第五子](../Page/慕容皝.md "wikilink")。（生于[326年](../Page/326年.md "wikilink")）
  - [1625年](../Page/1625年.md "wikilink")：[毛利輝元](../Page/毛利輝元.md "wikilink")，[日本](../Page/日本.md "wikilink")[長州藩主](../Page/長州藩.md "wikilink")（－[1553年](../Page/1553年.md "wikilink")）
  - [1714年](../Page/1714年.md "wikilink")：[噶禮](../Page/噶禮.md "wikilink")，[中國](../Page/中國.md "wikilink")[清朝官員](../Page/清朝.md "wikilink")（－[1782年](../Page/1782年.md "wikilink")）
  - [1852年](../Page/1852年.md "wikilink")：[弗里德里希·福禄贝尔](../Page/弗里德里希·福禄贝尔.md "wikilink")，[-{zh-hant:幼稚園;
    zh-cn:幼儿园; zh-sg:幼稚园;}-的创始人](../Page/幼稚園.md "wikilink")、德国教育家
  - [1882年](../Page/1882年.md "wikilink")：[朱塞佩·加里波底](../Page/朱塞佩·加里波底.md "wikilink")，[義大利民族英雄](../Page/意大利.md "wikilink")（－[1807年](../Page/1807年.md "wikilink")）
  - [1888年](../Page/1888年.md "wikilink")：[麻恭](../Page/麻恭.md "wikilink")，首任[香港輔政司](../Page/香港輔政司.md "wikilink")（－[1810年](../Page/1810年.md "wikilink")）
  - [1901年](../Page/1901年.md "wikilink")：[馬偕](../Page/馬偕.md "wikilink")，[加拿大傳教士](../Page/加拿大.md "wikilink")，台灣女子教育奠基者（－[1844年](../Page/1844年.md "wikilink")）
  - [1927年](../Page/1927年.md "wikilink")：[王国维](../Page/王国维.md "wikilink")，国学大师，自沉于[北京市](../Page/北京市.md "wikilink")[颐和园](../Page/颐和园.md "wikilink")[昆明湖](../Page/昆明湖.md "wikilink")（－[1877年](../Page/1877年.md "wikilink")）
  - [1932年](../Page/1932年.md "wikilink")：[約翰·華特·古格里](../Page/約翰·華特·古格里.md "wikilink")，[蘇格蘭](../Page/蘇格蘭.md "wikilink")[地質學家](../Page/地质学.md "wikilink")（－[1864年](../Page/1864年.md "wikilink")）
  - [1970年](../Page/1970年.md "wikilink")：[布魯斯·麥克拉倫](../Page/布鲁斯·麦克拉伦.md "wikilink")，[一級方程式](../Page/一級方程式.md "wikilink")[麥克拉倫車隊創立者](../Page/邁凱倫車隊.md "wikilink")，車手，工程師（生於[1937年](../Page/1937年.md "wikilink")）
  - [2007年](../Page/2007年.md "wikilink")：[黄菊](../Page/黄菊.md "wikilink")，[中共中央政治局常委](../Page/中国共产党中央政治局常务委员会.md "wikilink")、[中华人民共和国国务院副总理](../Page/中华人民共和国国务院副总理.md "wikilink")（－[1938年](../Page/1938年.md "wikilink")）
  - 2007年：[莊世平](../Page/莊世平.md "wikilink")，[香港銀行家](../Page/香港.md "wikilink")、[中华全国归国华侨联合会副主席](../Page/中华全国归国华侨联合会.md "wikilink")、[大紫荊勳章得主](../Page/大紫荊勳章.md "wikilink")（－[1911年](../Page/1911年.md "wikilink")）

## 节日、风俗习惯

  - [義大利共和國日](../Page/義大利共和國日.md "wikilink")