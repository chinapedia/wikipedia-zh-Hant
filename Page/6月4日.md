**6月4日**是[公历一年中的第](../Page/公历.md "wikilink")155天（[闰年第](../Page/闰年.md "wikilink")156天），离全年结束还有210天。

## 大事记

### 10世紀

  - [979年](../Page/979年.md "wikilink")：[北宋灭](../Page/北宋.md "wikilink")[北汉](../Page/北汉.md "wikilink")，[五代十国结束](../Page/五代十国.md "wikilink")。

### 11世紀

  - [1039年](../Page/1039年.md "wikilink")：[神圣罗马帝国皇帝](../Page/神圣罗马帝国.md "wikilink")[康拉德二世去世](../Page/康拉德二世_\(神圣罗马帝国\).md "wikilink")，[亨利三世继承皇位](../Page/亨利三世_\(神圣罗马帝国\).md "wikilink")。

### 17世紀

  - [1615年](../Page/1615年.md "wikilink")：[日本](../Page/日本.md "wikilink")[大御所](../Page/大御所.md "wikilink")[德川家康率领军队](../Page/德川家康.md "wikilink")[攻下大坂城](../Page/大坂之戰.md "wikilink")，城主[豐臣秀賴自杀](../Page/豐臣秀賴.md "wikilink")。
  - [1644年](../Page/1644年.md "wikilink")：[李自成率农民军在](../Page/李自成.md "wikilink")[吴三桂和满清](../Page/吴三桂.md "wikilink")[八旗军追击下](../Page/八旗.md "wikilink")，被迫退出[北京](../Page/北京.md "wikilink")。

### 19世紀

  - [1844年](../Page/1844年.md "wikilink")：[普魯士](../Page/普魯士.md "wikilink")[西里西亚纺织工人起义](../Page/西里西亚.md "wikilink")。
  - [1859年](../Page/1859年.md "wikilink")：[马詹塔战役](../Page/马詹塔战役.md "wikilink")，[法国和](../Page/法国.md "wikilink")[撒丁王国联军击败](../Page/撒丁王国.md "wikilink")[奥地利军队](../Page/奥地利.md "wikilink")。

### 20世紀

  - [1911年](../Page/1911年.md "wikilink")：[孙洪伊等在](../Page/孙洪伊.md "wikilink")[北京组织成立](../Page/北京.md "wikilink")[宪友会](../Page/宪友会.md "wikilink")，鼓吹[君主立宪](../Page/君主立宪.md "wikilink")。
  - [1913年](../Page/1913年.md "wikilink")：爭取[女性投票權的代表人](../Page/女性參政權.md "wikilink")[埃米莉·戴維森被英王](../Page/埃米莉·戴維森.md "wikilink")[喬治五世的](../Page/喬治五世.md "wikilink")[馬踏重傷](../Page/馬.md "wikilink")，四日後身亡，引起英國女教師極大迴響。
  - [1920年](../Page/1920年.md "wikilink")：[匈牙利簽署](../Page/匈牙利.md "wikilink")[特里阿農條約](../Page/特里阿農條約.md "wikilink")。
  - [1925年](../Page/1925年.md "wikilink")：[中国共产党创办的第一张日报](../Page/中国共产党.md "wikilink")《[热血日报](../Page/热血日报.md "wikilink")》正式出版。
  - [1928年](../Page/1928年.md "wikilink")：[皇姑屯事件](../Page/皇姑屯事件.md "wikilink")：[中華民國安國陸海軍大元帥](../Page/中華民國大總統.md "wikilink")[张作霖在](../Page/张作霖.md "wikilink")[沈阳的皇姑屯火车站被日本](../Page/沈阳.md "wikilink")[关东军炸死](../Page/关东军.md "wikilink")。
  - [1937年](../Page/1937年.md "wikilink")：中国[东北抗日联军越过](../Page/东北抗日联军.md "wikilink")[满洲国与](../Page/满洲国.md "wikilink")[日治朝鲜的边境](../Page/日治朝鲜.md "wikilink")，袭击[朝鲜境内日本统治下的据点](../Page/朝鲜.md "wikilink")[普天堡](../Page/普天堡.md "wikilink")，史称[普天堡战役](../Page/普天堡战役.md "wikilink")。
  - [1942年](../Page/1942年.md "wikilink")：[南云忠一率领的](../Page/南云忠一.md "wikilink")[日本海军向](../Page/日本海军.md "wikilink")[中途岛的](../Page/中途岛.md "wikilink")[美军发动进攻](../Page/美军.md "wikilink")，[中途岛海战爆发](../Page/中途岛海战.md "wikilink")。
  - [1944年](../Page/1944年.md "wikilink")：英美聯軍進佔[意大利首都](../Page/意大利.md "wikilink")[羅馬](../Page/羅馬.md "wikilink")。
  - [1953年](../Page/1953年.md "wikilink")：[中华人民共和国政府与](../Page/中华人民共和国政府.md "wikilink")[苏联政府签订](../Page/苏联政府.md "wikilink")《关于海军交货和关于在建造军舰方面给与中国以技术援助的协定》。
  - [1960年](../Page/1960年.md "wikilink")：[中華人民共和國教育部](../Page/中華人民共和國教育部.md "wikilink")、[文化部](../Page/中華人民共和國文化部.md "wikilink")、[中國文字改革委員會發出通知徵集全國各地的新簡化字](../Page/中國文字改革委員會.md "wikilink")，準備[再次簡化漢字](../Page/二簡字.md "wikilink")。
  - [1970年](../Page/1970年.md "wikilink")：[太平洋島國](../Page/太平洋.md "wikilink")[湯加獨立](../Page/湯加.md "wikilink")。
  - [1982年](../Page/1982年.md "wikilink")：[以色列轰炸](../Page/以色列.md "wikilink")[黎巴嫩首都](../Page/黎巴嫩.md "wikilink")[贝鲁特](../Page/贝鲁特.md "wikilink")，造成惨重平民伤亡。
  - [1985年](../Page/1985年.md "wikilink")：中央军委主席[邓小平在](../Page/邓小平.md "wikilink")[中央军委扩大会议上宣布](../Page/中央军委.md "wikilink")[中国人民解放军进行](../Page/中国人民解放军.md "wikilink")[百万大裁军](../Page/百万大裁军.md "wikilink")。
  - [1989年](../Page/1989年.md "wikilink")：[六四事件](../Page/六四事件.md "wikilink")：中國政府出動軍隊鎮壓[天安門廣場內的示威民眾](../Page/天安門廣場.md "wikilink")。
  - 1989年：伊朗总统[哈梅内伊获推选为伊朗最高领袖](../Page/哈梅内伊.md "wikilink")。
  - 1989年：[波兰舉行第一次民主大选](../Page/波兰.md "wikilink")，[團結工会徹底擊敗](../Page/團結工会.md "wikilink")[波兰统一工人党](../Page/波蘭統一工人黨.md "wikilink")，後者在参众兩院全軍覆沒後下台。
  - [1991年](../Page/1991年.md "wikilink")：[新華社公布](../Page/新華社.md "wikilink")「[四人幫](../Page/四人幫.md "wikilink")」主犯[江青死訊](../Page/江青.md "wikilink")。
  - [1999年](../Page/1999年.md "wikilink")：[中華民國](../Page/中華民國.md "wikilink")[立法院三讀通過](../Page/立法院.md "wikilink")「菸酒管理法」，開放民間可以公開方式參與菸酒產銷，宣告[台灣實施近五十年的菸酒專賣制度將走入歷史](../Page/台灣.md "wikilink")。

### 21世紀

  - [2002年](../Page/2002年.md "wikilink")：[中國男子足球隊首次出戰](../Page/中國國家足球隊.md "wikilink")[世界盃決賽週](../Page/2002年世界盃足球賽.md "wikilink")，它們的首場對手是[哥斯達黎加](../Page/哥斯達黎加國家足球隊.md "wikilink")，最終三場分組賽全敗零得球，成為「三零部隊出局」。
  - 2010年：[菅直人当选](../Page/菅直人.md "wikilink")[日本民主党党代表](../Page/日本民主党.md "wikilink")，并在国会全体会议上当选为[首相](../Page/日本首相.md "wikilink")，接替辞职的[鸠山由纪夫](../Page/鸠山由纪夫.md "wikilink")。
  - [2011年](../Page/2011年.md "wikilink")：中国女子[网球选手](../Page/网球.md "wikilink")[李娜在](../Page/李娜_\(网球运动员\).md "wikilink")[法国网球公开赛女單决赛中](../Page/法国网球公开赛.md "wikilink")2-0击败意大利选手[弗朗西斯卡·斯齐亚沃尼](../Page/弗朗西斯卡·斯齐亚沃尼.md "wikilink")，获得冠军。成为亚洲第一个女单大满贯冠军。

## 出生

  - [1715年](../Page/1715年.md "wikilink")：[曹雪芹](../Page/曹雪芹.md "wikilink")，《红楼梦》作者
  - [1738年](../Page/1738年.md "wikilink")：[喬治三世](../Page/喬治三世_\(英國\).md "wikilink")，[大不列顛國王及](../Page/大不列顛.md "wikilink")[愛爾蘭國王](../Page/愛爾蘭.md "wikilink")（[1820年逝世](../Page/1820年.md "wikilink")）
  - [1855年](../Page/1855年.md "wikilink")：[犬養毅](../Page/犬養毅.md "wikilink")，[日本第](../Page/日本.md "wikilink")29任[首相](../Page/日本首相.md "wikilink")（[1932年逝世](../Page/1932年.md "wikilink")）
  - [1916年](../Page/1916年.md "wikilink")：[羅伯·佛契哥特](../Page/羅伯·佛契哥特.md "wikilink")，[美國生物化學家](../Page/美國.md "wikilink")，[1998年](../Page/1998年.md "wikilink")[諾貝爾生理學或醫學獎獲獎者](../Page/諾貝爾生理學或醫學獎.md "wikilink")（[2009年逝世](../Page/2009年.md "wikilink")）
  - [1928年](../Page/1928年.md "wikilink")：[田豐](../Page/田豐_\(演員\).md "wikilink")，[香港](../Page/香港.md "wikilink")[演員](../Page/演員.md "wikilink")（[2015年逝世](../Page/2015年.md "wikilink")）
  - [1936年](../Page/1936年.md "wikilink")：[努坦](../Page/努坦.md "wikilink")，[印度女演員](../Page/印度.md "wikilink")（[1991年逝世](../Page/1991年.md "wikilink")）
  - [1959年](../Page/1959年.md "wikilink")：[胡瓜](../Page/胡瓜.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[主持人](../Page/主持人.md "wikilink")
  - 1959年：[安尼尔·安巴尼](../Page/安尼尔·安巴尼.md "wikilink")，印度富豪
  - [1960年](../Page/1960年.md "wikilink")：[葛平](../Page/葛平.md "wikilink")，[中国](../Page/中国.md "wikilink")[动画片](../Page/动画.md "wikilink")[配音演员](../Page/配音演员.md "wikilink")
  - [1962年](../Page/1962年.md "wikilink")：[何君堯](../Page/何君堯.md "wikilink")，[香港律師](../Page/香港.md "wikilink")、政治人物
  - [1971年](../Page/1971年.md "wikilink")：[諾亞·懷利](../Page/諾亞·懷利.md "wikilink")，美國演員
  - [1975年](../Page/1975年.md "wikilink")：[安潔莉娜·裘莉](../Page/安潔莉娜·裘莉.md "wikilink")，美國女演員，社會活動家
  - [1976年](../Page/1976年.md "wikilink")：[林昌勇](../Page/林昌勇.md "wikilink")，[韓國](../Page/韓國.md "wikilink")[棒球選手](../Page/棒球.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[高原直泰](../Page/高原直泰.md "wikilink")，日本足球選手
  - [1981年](../Page/1981年.md "wikilink")：[林家緯](../Page/林家緯.md "wikilink")，台灣[棒球選手](../Page/棒球.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[蘇銘倫](../Page/蘇銘倫.md "wikilink")，[比利時籍騎師](../Page/比利時.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[歐陽靖](../Page/歐陽靖_\(香港\).md "wikilink")，[香港](../Page/香港.md "wikilink")[饒舌歌手](../Page/饒舌歌手.md "wikilink")，[演員](../Page/演員.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[戴嬌倩](../Page/戴娇倩.md "wikilink")，[中國](../Page/中國.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[楊丞琳](../Page/楊丞琳.md "wikilink")，[台灣歌手](../Page/台灣歌手.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[陳勢安](../Page/陈势安.md "wikilink")，台灣男歌手
  - [1985年](../Page/1985年.md "wikilink")：[林星辰](../Page/林星辰.md "wikilink")，台灣棒球選手
  - [1985年](../Page/1985年.md "wikilink")：[卢卡斯·波多尔斯基](../Page/卢卡斯·波多尔斯基.md "wikilink")，德国足球选手
  - [1986年](../Page/1986年.md "wikilink")：[朴有天](../Page/朴有天.md "wikilink")，韓國歌手，演員
  - [1989年](../Page/1989年.md "wikilink")：[毕赣](../Page/畢贛.md "wikilink")，中国导演
  - [1990年](../Page/1990年.md "wikilink")：[吉增·佩玛](../Page/吉增·佩玛.md "wikilink")，[不丹王后](../Page/不丹.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[林志堅](../Page/林志堅_\(足球運動員\).md "wikilink")，[香港足球運動員](../Page/香港.md "wikilink")
  - [1995年](../Page/1995年.md "wikilink")：[玉井詩織](../Page/玉井詩織.md "wikilink")，[日本女子團體](../Page/日本.md "wikilink")[桃色幸運草Z成員](../Page/桃色幸運草Z.md "wikilink")
  - [1998年](../Page/1998年.md "wikilink")：[鄭叡仁](../Page/鄭叡仁.md "wikilink")，韓國女子團體[Lovelyz成員](../Page/Lovelyz.md "wikilink")
  - [1999年](../Page/1999年.md "wikilink")：[金所炫](../Page/金所炫.md "wikilink")，韓國女演員

## 逝世

  - [1039年](../Page/1039年.md "wikilink")：[康拉德二世](../Page/康拉德二世_\(神圣罗马帝国\).md "wikilink")，[德意志國王和](../Page/德意志.md "wikilink")[神聖羅馬帝國皇帝](../Page/神聖羅馬帝國.md "wikilink")
  - [1135年](../Page/1135年.md "wikilink")：[宋徽宗赵佶](../Page/宋徽宗.md "wikilink")，[北宋皇帝](../Page/北宋.md "wikilink")（[1082年出生](../Page/1082年.md "wikilink")）
  - [1571年](../Page/1571年.md "wikilink")：[氏家直元](../Page/氏家直元.md "wikilink")，[日本戰國時代的武將](../Page/日本戰國時代.md "wikilink")
  - [1615年](../Page/1615年.md "wikilink")：[豐臣秀賴](../Page/豐臣秀賴.md "wikilink")，日本戰國時代、[江戶時代初期的](../Page/江戶時代.md "wikilink")[大名](../Page/大名.md "wikilink")，[豐臣氏第](../Page/豐臣氏.md "wikilink")2任當主，豐臣政權領袖，[大坂城城主](../Page/大坂城.md "wikilink")（[1593年出生](../Page/1593年.md "wikilink")）
  - [1615年](../Page/1615年.md "wikilink")：[毛利勝永](../Page/毛利勝永.md "wikilink")，[日本戰國時代](../Page/日本.md "wikilink")、[江戶時代初期武將](../Page/江戶時代.md "wikilink")（[1577年出生](../Page/1577年.md "wikilink")）
  - [1615年](../Page/1615年.md "wikilink")：[真田幸昌](../Page/真田幸昌.md "wikilink")，日本[江戶時代初期的少年武將](../Page/江戶時代.md "wikilink")（[1601年出生](../Page/1601年.md "wikilink")）
  - [1615年](../Page/1615年.md "wikilink")：[氏家行廣](../Page/氏家行廣.md "wikilink")，日本戰國時代的人物（[1546年出生](../Page/1546年.md "wikilink")）
  - [1680年](../Page/1680年.md "wikilink")：[德川家綱](../Page/德川家綱.md "wikilink")，日本[江戶幕府](../Page/江戶幕府.md "wikilink")[將軍](../Page/幕府將軍.md "wikilink")（[1641年出生](../Page/1641年.md "wikilink")）
  - [1862年](../Page/1862年.md "wikilink")：[陈玉成](../Page/陈玉成.md "wikilink")，[太平天国英王](../Page/太平天国.md "wikilink")（[1837年出生](../Page/1837年.md "wikilink")）
  - [1928年](../Page/1928年.md "wikilink")：[张作霖](../Page/张作霖.md "wikilink")，當時的中華民國[北洋政府國家元首](../Page/北洋政府.md "wikilink")，[奉系军阀首领](../Page/奉系军阀.md "wikilink")（[1875年出生](../Page/1875年.md "wikilink")）
  - [1928年](../Page/1928年.md "wikilink")：[吳俊升](../Page/吳俊升_\(民國軍政人物\).md "wikilink")，中華民國北洋時期軍政人物（[1863年出生](../Page/1863年.md "wikilink")）
  - [1942年](../Page/1942年.md "wikilink")：[海德里希](../Page/海德里希.md "wikilink")，[納粹德國波希米亞攝政](../Page/納粹德國.md "wikilink")（[1904年出生](../Page/1904年.md "wikilink")）
  - [1943年](../Page/1943年.md "wikilink")：[埃德温·泰勒·波洛克](../Page/埃德温·泰勒·波洛克.md "wikilink")，美国海军职业军官（[1870年出生](../Page/1870年.md "wikilink")）
  - [2001年](../Page/2001年.md "wikilink")：[迪潘德拉](../Page/迪潘德拉.md "wikilink")，[尼泊尔国王](../Page/尼泊尔国王.md "wikilink")（[1971年出生](../Page/1971年.md "wikilink")）
  - [2001年](../Page/2001年.md "wikilink")：[卢嘉锡](../Page/卢嘉锡.md "wikilink")，[中國科學院前院長](../Page/中國科學院.md "wikilink")、[中國農工民主黨中央委員會前主席](../Page/中國農工民主黨.md "wikilink")（[1915年出生](../Page/1915年.md "wikilink")）
  - [2014年](../Page/2014年.md "wikilink")：[何佐芝](../Page/何佐芝.md "wikilink")，[香港商業電台創辦人](../Page/香港商業電台.md "wikilink")（[1918年出生](../Page/1918年.md "wikilink")）
  - [2015年](../Page/2015年.md "wikilink")：[安妮·沃伯頓](../Page/安妮·沃伯頓.md "wikilink")，英國外交官（[1927年出生](../Page/1927年.md "wikilink")）

## 節假日和習俗

  - [受侵略戕害的无辜儿童國際日](../Page/受侵略戕害的无辜儿童國際日.md "wikilink")
  - [芬蘭](../Page/芬蘭.md "wikilink")：[卡尔·古斯塔夫·埃米尔·曼纳海姆壽辰和](../Page/卡尔·古斯塔夫·埃米尔·曼纳海姆.md "wikilink")[芬蘭國防軍的](../Page/芬蘭國防軍.md "wikilink")[國旗日](../Page/國旗日.md "wikilink")
  - [東加](../Page/東加.md "wikilink")：[奴隸解放日或](../Page/奴隸解放日.md "wikilink")[獨立日](../Page/獨立日.md "wikilink")，紀念1862年[國王](../Page/汤加君主.md "wikilink")[乔治·图普一世廢除農奴制](../Page/乔治·图普一世.md "wikilink")，和1970年從[英國殖民保護獨立](../Page/保护国.md "wikilink")。
  - [愛沙尼亞](../Page/愛沙尼亞.md "wikilink")：[國旗日](../Page/爱沙尼亚国旗.md "wikilink")
  - [匈牙利](../Page/匈牙利.md "wikilink")：[民族統一日](../Page/民族統一日.md "wikilink")