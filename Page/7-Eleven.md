**7-Eleven**是一間[跨國](../Page/跨國公司.md "wikilink")[連鎖](../Page/連鎖店.md "wikilink")[便利店集團](../Page/便利店.md "wikilink")，源於[美國企業](../Page/美國.md "wikilink")[南方公司從冰品商店特許經營起家](../Page/南方公司.md "wikilink")，後來被[日本](../Page/日本.md "wikilink")[7\&I控股公司收購](../Page/7&I控股.md "wikilink")，目前為其旗下全資[子公司](../Page/子公司.md "wikilink")。公司總部設於日本[东京的](../Page/东京都.md "wikilink")[千代田区](../Page/千代田区.md "wikilink")[二番町](../Page/二番町_\(千代田區\).md "wikilink")8-8號，現任會長是[鈴木敏文](../Page/鈴木敏文.md "wikilink")。\[1\]。

截至2018年8月，7-Eleven全球門店總數達66,581家\[2\]，分佈於全球17個國家和地區，包括日本、美國、[加拿大](../Page/加拿大.md "wikilink")、[墨西哥](../Page/墨西哥.md "wikilink")、[香港](../Page/香港.md "wikilink")、[中国大陸](../Page/中国大陸.md "wikilink")、[澳大利亞](../Page/澳大利亞.md "wikilink")、[澳門](../Page/澳門.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[韓國](../Page/韓國.md "wikilink")、[挪威](../Page/挪威.md "wikilink")、[瑞典](../Page/瑞典.md "wikilink")、[丹麥](../Page/丹麥.md "wikilink")、[越南與](../Page/越南.md "wikilink")[阿聯酋](../Page/阿拉伯联合酋长国.md "wikilink")。與[FamilyMart](../Page/FamilyMart.md "wikilink")、[Lawson](../Page/Lawson.md "wikilink")、[Circle
K等便利店互相競爭](../Page/Circle_K.md "wikilink")。

## 歷史

[7_Eleven_Fukushima_Shinchi_Town_Shop.jpg](https://zh.wikipedia.org/wiki/File:7_Eleven_Fukushima_Shinchi_Town_Shop.jpg "fig:7_Eleven_Fukushima_Shinchi_Town_Shop.jpg")新地町店\]\]
[7-11_in_Hong_Kong_International_Airport_Terminal_2_2017.jpg](https://zh.wikipedia.org/wiki/File:7-11_in_Hong_Kong_International_Airport_Terminal_2_2017.jpg "fig:7-11_in_Hong_Kong_International_Airport_Terminal_2_2017.jpg")）\]\]
[Nørreport_Station_at_night_04.jpg](https://zh.wikipedia.org/wiki/File:Nørreport_Station_at_night_04.jpg "fig:Nørreport_Station_at_night_04.jpg")的7-Eleven\]\]

  - 1927年，南方公司(The Southland
    Corporation)於在美國[德克薩斯州](../Page/德克薩斯州.md "wikilink")[達拉斯成立了販售冰品](../Page/達拉斯.md "wikilink")、[牛奶](../Page/牛奶.md "wikilink")、[雞蛋為主的圖騰商店](../Page/雞蛋.md "wikilink")（Tote'm
    stores）。
  - 1946年，由於營業時間延長為上午7時至晚上11時，圖騰商店改名為7-Eleven。
  - 1952年，南方公司成立了第100家7-Eleven。
  - 1962年，南方公司首次在德州[奥斯汀實驗](../Page/奥斯汀.md "wikilink")24小時經營。
  - 1969年，与加拿大成立第一间跨国便利店.
  - 1971年，南方公司於墨西哥開拓7-Eleven市場。
  - 1974年，日本[伊藤洋華堂公司獲授權在日本成立](../Page/伊藤洋華堂.md "wikilink")7-Eleven。
  - 1979年，臺灣[統一企業公司獲授權在臺灣成立](../Page/統一企業.md "wikilink")7-Eleven。
  - 1981年，香港[牛奶公司獲授權在香港成立](../Page/牛奶公司.md "wikilink")7-Eleven。
  - 1989年，韓國開設第一家7-Eleven。
  - 1991年，伊藤洋華堂取得南方公司過半股權，南方公司因此成為日本企業。
  - 1992年，香港牛奶公司獲授權在[深圳成立](../Page/深圳.md "wikilink")7-Eleven。
  - 1996年，香港牛奶公司獲授權在[廣州成立](../Page/廣州.md "wikilink")7-Eleven。
  - 1999年，南方公司名稱改為7-Eleven公司（7-Eleven Inc.）。
  - 2000年7月7日，7-Eleven
    Inc.從[納斯達克](../Page/納斯達克.md "wikilink")（NASDAQ）交易市場晉升到[紐約證券交易所](../Page/紐約證券交易所.md "wikilink")。
  - 2005年，香港牛奶公司獲授權在澳門成立7-Eleven。
  - 2005年9月1日，伊藤洋華堂成立新控股公司「[7\&I控股](../Page/7&I控股.md "wikilink")」，統一管理伊藤洋華堂、7-Eleven
    Inc.及在日本的7-Eleven，並於2005年11月9日正式完成收購7-Eleven
    Inc.的全部股權\[3\]，正式的將這家美國公司完全的子公司化，同時也正式自證券市場下市。
  - 2017年5月19日柒一拾壹（中國）投資有限公司（SEVEN ELEVEN
    CHINA，簡稱SEC）將正式把[浙江省的](../Page/浙江省.md "wikilink")7-ELEVEN經營授權給[統一超商](../Page/統一超商.md "wikilink")（浙江）便利店有限公司
  - 2017年7月4日由[印尼Honoris何春霖家族經營的控股上市公司現代國](../Page/印尼.md "wikilink")（Modern
    Internasional，IDX:MDRN），旗下的零售子公司Modern Sevel
    Indonesia日前宣布，自6月30日起，將關閉在印尼剩下的120家7-Eleven店面。

## 全球分佈及店數

[7eleven_map.svg](https://zh.wikipedia.org/wiki/File:7eleven_map.svg "fig:7eleven_map.svg")
[7-Eleven長虹門市.jpg](https://zh.wikipedia.org/wiki/File:7-Eleven長虹門市.jpg "fig:7-Eleven長虹門市.jpg")
至2017年12月末的統計，7-Eleven的全球分店總數為64,319余家，其經營市場分別由下列地區所組成\[4\]：

  - 7-Eleven
    INC（全球7-Eleven總部，[7\&I控股子公司](../Page/7&I控股.md "wikilink")）所屬：

      - 2009年的營業額171億美元

          - 8,378家

          - 504家

      - 區域授權經營：413家

          - Garb-Ko,Inc.（[密歇根州部份地區](../Page/密歇根州.md "wikilink")、[印第安納州](../Page/印第安納州.md "wikilink")（[印第安納波利斯](../Page/印第安納波利斯.md "wikilink")）及[俄亥俄州](../Page/俄亥俄州.md "wikilink")（[哥倫布](../Page/哥倫布_\(俄亥俄州\).md "wikilink")））89家
          - Handee Marts,
            Inc.（[賓夕法尼亞州及俄亥俄州部份地區](../Page/賓夕法尼亞州.md "wikilink")）62家
          - Resort Retailers（[猶他州](../Page/猶他州.md "wikilink")）9家
          - Prima
            Marketing（[西維珍尼亞州](../Page/西維珍尼亞州.md "wikilink")（中部及西部）及東俄亥俄州部份地區）86家
          - Southwest Convenience
            Stores,Inc.（[德克薩斯州西部](../Page/德克薩斯州.md "wikilink")、[新墨西哥州](../Page/新墨西哥州.md "wikilink")）167家
          - [夏威夷](../Page/夏威夷.md "wikilink")56家

  - [日本](../Page/日本.md "wikilink")7-Eleven（[7\&I控股子公司](../Page/7&I控股.md "wikilink")）所屬：

      - 20,337家 经营总面积2,439,550平方米

  - 7-Eleven（[卜蜂集團](../Page/卜蜂集團.md "wikilink")）10,268家

  - 7-Eleven（[樂天集團](../Page/樂天_\(Lotte\).md "wikilink")）9,231家

  - （含、）2,599家

      - 7-Eleven（[牛奶公司](../Page/牛奶國際有限公司.md "wikilink")）933家

      - 7-Eleven（[牛奶公司](../Page/牛奶國際有限公司.md "wikilink")）47家

  - 7-Eleven（[成功零售有限公司](../Page/成功零售有限公司.md "wikilink")（Berjaya Retail
    Bhd）持有57%股份7-11大马控股）2,225家

  - 1,835家

  - 183家

  - 7-Eleven（[牛奶公司](../Page/牛奶國際有限公司.md "wikilink")）393家

  - 7-Eleven（[統一超商](../Page/統一超商股份有限公司.md "wikilink")）5,221家\[5\]

  - 7-Eleven（[統一超商持股](../Page/統一超商股份有限公司.md "wikilink")51.56％）2,561家

  - 675家

  - 187家

  - 153家

  - （[7\&I控股](../Page/7&I控股.md "wikilink")）11家

  - 13家

## 經營特色

### 代表商品

[Double_Gulp_with_stapler_for_comparison.jpg](https://zh.wikipedia.org/wiki/File:Double_Gulp_with_stapler_for_comparison.jpg "fig:Double_Gulp_with_stapler_for_comparison.jpg")（Big
Gulp）\]\]
[711IcyDrinkCup.jpg](https://zh.wikipedia.org/wiki/File:711IcyDrinkCup.jpg "fig:711IcyDrinkCup.jpg")
目前7-Eleven有幾項商品於全[世界](../Page/世界.md "wikilink")[銷售](../Page/銷售.md "wikilink")，包含：

  - [重量杯](../Page/重量杯.md "wikilink")（Big
    Gulp）：[中國大陸](../Page/中國大陸.md "wikilink")[廣東及](../Page/廣東.md "wikilink")[港](../Page/香港.md "wikilink")[澳称](../Page/澳門.md "wikilink")[自由斟](../Page/自由斟.md "wikilink")。[台灣曾有一段](../Page/台灣.md "wikilink")[時期](../Page/時期.md "wikilink")[流行過](../Page/流行.md "wikilink")，但現在只有少數門市有販售。分為[大](../Page/大.md "wikilink")、[中](../Page/中.md "wikilink")、-{zh-cn:[小](../Page/小.md "wikilink");
    zh-tw:[小](../Page/小.md "wikilink");
    zh-hk:[細](../Page/小.md "wikilink");}-[杯](../Page/杯.md "wikilink")。
  - [大亨堡](../Page/大亨堡.md "wikilink")（Big Bite）
  - Coffee-to-go（與臺灣的「[City
    Cafe](../Page/City_Cafe.md "wikilink")」及香港的「[Daily
    Cafe](../Page/Daily_Cafe.md "wikilink")」略有不同）
  - [思樂冰](../Page/思樂冰.md "wikilink")（Slurpee）：一種[冷凍為](../Page/冷凍.md "wikilink")[冰沙狀的](../Page/冰沙.md "wikilink")[碳酸飲料](../Page/碳酸飲料.md "wikilink")，7-Eleven獨家販售，[美國為主要銷售區域](../Page/美國.md "wikilink")，[中國](../Page/中華人民共和國.md "wikilink")[廣東](../Page/廣東.md "wikilink")、[上海](../Page/上海.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門](../Page/澳門.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、马来西亚、[台灣也有販售](../Page/台灣.md "wikilink")。

### 國際製販同盟

由於7-Eleven在許多[國家都有展店](../Page/國家.md "wikilink")，因此可以通過[國際共同採購或是相互介紹廠商的方式](../Page/國際.md "wikilink")，降低[商品採購](../Page/商品.md "wikilink")[成本](../Page/成本.md "wikilink")，並創造有特色的商品，例如：[臺灣的統一超商透過](../Page/臺灣.md "wikilink")[菲律賓](../Page/菲律賓.md "wikilink")7-Eleven的介紹引進[菲律賓生產的](../Page/菲律賓.md "wikilink")[芒果乾](../Page/芒果.md "wikilink")，[香港也相繼引進](../Page/香港.md "wikilink")，7-Eleven店內提供的[購物袋也通過這樣的方式](../Page/購物袋.md "wikilink")，由許多[國家共同採購降低成本](../Page/國家.md "wikilink")。

### 7-11 Day

每年的7月11日為「7-11
Day」，據信此傳統為[臺灣](../Page/臺灣.md "wikilink")7-Eleven所創，最早的目的是希望後勤單位不要忘記第一線門市店作業的辛苦，因此選定每年的此日，所有臺灣7-Eleven後勤單位人員包含所有高級主管，都要到門市上班一天，因此又稱並肩工作日，後來其他[國家陸續仿效](../Page/國家.md "wikilink")，但其目的不一，[美國](../Page/美國.md "wikilink")7-11
Day定位在慶祝的角色，每年此日全美國7-Eleven均會提供免費的[思樂冰](../Page/思樂冰.md "wikilink")，[日本的](../Page/日本.md "wikilink")7-11
Day有兩天，除了7月11日外還有11月7日，在這天全[日本的](../Page/日本.md "wikilink")7-Eleven均會清理門市店的周邊巷道，這項工作目前也被[臺灣列入](../Page/臺灣.md "wikilink")7-11
Day的工作項目之一 。\[6\]

因澳洲的日期格式為日／月／年，因此其「7-11 Day」是在 11月 7日，也考慮到 11
月正是南半球的夏天。當日上午七點至晚上十一點，任何人只要到
7-Eleven 門市，對工作人員說「Happy 7-Eleven Day\!」便可免費獲贈一杯思樂冰，因此每年當天各門市往往人滿為患。

### 商標設計

1969年，7-11的正式商標表記方式為<span style="font-size:larger;">**<span style="font-size:smaller;">7-ELEVE</span>**<span style="font-size:larger;">n</span></span>，其中除了結尾的n為[小寫外](../Page/小寫字母.md "wikilink")，其餘英文字母為[大寫](../Page/大寫字母.md "wikilink")；此種設計的原因，在[華人](../Page/華人.md "wikilink")[地區](../Page/地區.md "wikilink")（尤其是[臺灣](../Page/臺灣.md "wikilink")）的民間說法是因為[風水的因素](../Page/風水.md "wikilink")（大寫N的最後一劃為往外，表示會將錢財散出去，而小寫n的結尾為往內，表示會吸引錢財進來），不過官方說法則表示此設計在南方公司時期就已經存在，而當初設計者只是因為美觀問題才創造出此商標。\[7\]\[8\]\[9\]

## 各地的7-Eleven

### 北美洲

[加拿大的第一家分店](../Page/加拿大.md "wikilink")1969年在[卡加利開幕](../Page/卡加利.md "wikilink")。目前加拿大分店為650家，市內便利店非7-Eleven獨大，郊區的7-Eleven則和加油站合營。\[10\]美國目前有8,421間分店，大部份為合營，7-Eleven可說是無處不在。\[11\]

### 日本

[7-Eleven_in_Osaka_2014.jpg](https://zh.wikipedia.org/wiki/File:7-Eleven_in_Osaka_2014.jpg "fig:7-Eleven_in_Osaka_2014.jpg")

[日本的](../Page/日本.md "wikilink")7-Eleven是在1973年由[伊藤洋華堂自美國引進](../Page/伊藤洋華堂.md "wikilink")，1974年於日本東京開設第一家店，在成立之初，伊藤洋華堂內部相當反對，[鈴木敏文](../Page/鈴木敏文.md "wikilink")（現任日本7-Eleven
CEO）以即使失敗也不影響母公司為條件，取得公司高層的首肯，而後，鈴木敏文長期領導發展，目前已經成為全世界最大的單一連鎖便利商店公司，店鋪數20,033家，佔全世界7-Eleven店數的三分之一，1991年甚至買下7-Eleven全球總部南方公司，2005年將日本7-Eleven、伊藤洋華堂、美國7-Eleven合併入[7\&I控股](../Page/7&I控股.md "wikilink")。由於在日本的成功發展，亞洲多數國家的7-Eleven多以日本為效法對象，臺灣、韓國、泰國等地的店鋪多有日本經營方式的影子。

#### 特色

[Lunches_of_Seven-Eleven.jpg](https://zh.wikipedia.org/wiki/File:Lunches_of_Seven-Eleven.jpg "fig:Lunches_of_Seven-Eleven.jpg")\]\]
[Typical_japanese_sushi_set.jpg](https://zh.wikipedia.org/wiki/File:Typical_japanese_sushi_set.jpg "fig:Typical_japanese_sushi_set.jpg")\]\]

  - 與[FamilyMart](../Page/FamilyMart.md "wikilink")、[Lawson並列為](../Page/羅森_\(便利店\).md "wikilink")，但尚未完成全日本展店，目前在[沖繩縣沒有分店](../Page/沖繩縣.md "wikilink")\[12\]。
  - 沒有販售杯裝汽水與[熱狗](../Page/熱狗.md "wikilink")。[思樂冰已在部分分店試賣中](../Page/思樂冰.md "wikilink")。
  - 超過30%的營業額來自店內販售的即食商品，如：[便當](../Page/便當.md "wikilink")、[飯糰](../Page/飯糰.md "wikilink")、[關東煮與其他需](../Page/關東煮.md "wikilink")[微波爐加熱的食品等](../Page/微波爐.md "wikilink")。
  - 超過50%的品項是獨家販賣或[自有品牌商品](../Page/自有品牌.md "wikilink")。
  - 擁有自己的銀行（），其主要業務為經營7-Eleven店內的[自動櫃員機](../Page/自動櫃員機.md "wikilink")\[13\]。
  - 透過子公司經營7dream.com網路購物網站（现已經更名为711net.jp），並與日本[Yahoo\!合作成立Seven](../Page/Yahoo!.md "wikilink")\&Y公司經營網路購物。
  - 於2007年發行預付式電子錢包「[nanaco](../Page/nanaco.md "wikilink")」。

### 香港

[Comparison_of_receipts_of_7-11_stores_in_Hong_Kong_and_Shanghai.jpg](https://zh.wikipedia.org/wiki/File:Comparison_of_receipts_of_7-11_stores_in_Hong_Kong_and_Shanghai.jpg "fig:Comparison_of_receipts_of_7-11_stores_in_Hong_Kong_and_Shanghai.jpg")的中英文字样\]\]
[201609_7-11_stores_at_Austin_Station.jpg](https://zh.wikipedia.org/wiki/File:201609_7-11_stores_at_Austin_Station.jpg "fig:201609_7-11_stores_at_Austin_Station.jpg")[柯士甸站内的](../Page/柯士甸站.md "wikilink")7-Eleven门店\]\]
[香港的](../Page/香港.md "wikilink")7-Eleven起源於1981年4月3日，由香港[牛奶公司取得授權](../Page/牛奶公司.md "wikilink")。7-Eleven在[港澳地區唸作](../Page/港澳地區.md "wikilink")「七十一」，但「七仔」、「些粉」（取Seven的[粵語化讀音](../Page/粵語.md "wikilink")）這些坊間俗稱也相當流行。第一間7-Eleven於[香港島](../Page/香港島.md "wikilink")[跑馬地開幕](../Page/跑馬地.md "wikilink")，其後以[加盟的形式迅速擴展分店網絡](../Page/加盟.md "wikilink")。2004年9月，牛奶公司成功以1.05億港元從[南華早報集團收購香港第三大便利商店](../Page/南華早報集團.md "wikilink")[地利店全線](../Page/地利店.md "wikilink")87間分店，加上早前收購了原擁有[港鐵](../Page/港鐵.md "wikilink")[將軍澳綫及](../Page/將軍澳綫.md "wikilink")[東鐵綫便利店專營權的](../Page/東鐵綫.md "wikilink")[世昌便利店](../Page/世昌便利店.md "wikilink")，使其分店總數一下子由510間（已包括原世昌的分店）大幅提升至597間，進一步鞏固其在香港便利商店市場的領導地位。香港7-Eleven採用高密度策略設置經營點，部分地段往往不足數分鐘步程已有另一家分店。直至2014年2月，7-Eleven已有912家分店，其中有386間由特許加盟商營運，其餘由7-Eleven總公司營運。7-Eleven分店數目之多是香港另一所市面常見的連鎖便利商店——[OK便利店](../Page/OK便利店.md "wikilink")312間的3倍多。

但近年被批評欺壓加盟商，包括發現特許經營商賺大錢，便會拒絕與加盟商續約，奪回經營權；又或在盈利高的店舖附近開分店，務求搶走特許經營商生意\[14\]。

#### 自家品牌

  - 熱賣點：提供各種熟食（如[魚蛋](../Page/魚蛋.md "wikilink")、[燒賣](../Page/燒賣.md "wikilink")、[司華力腸](../Page/司華力腸.md "wikilink")、[撈麵](../Page/拌麵.md "wikilink")、烏冬、[關東煮等小吃](../Page/關東煮.md "wikilink")），更會隨季節推出特式食品，如[蛋黄咸肉粽](../Page/蛋黄咸肉粽.md "wikilink")（端午節）、[心型朱古力包](../Page/心型朱古力包.md "wikilink")（情人節）；部分商業地區店舗更有「午市兩餸飯」、即製[三文治等食品](../Page/三文治.md "wikilink")。
  - Daily Cafe：售賣中高檔即磨咖啡産品，截至2015年6月，已有超過300間分店設有[Daily
    Cafe](../Page/Daily_Cafe.md "wikilink")，覆蓋全港18區，分店數目亦已超越[星巴克](../Page/星巴克.md "wikilink")。
  - Deli Fresh：出產各款新鮮果汁産品，亦有出產中式産品（如川貝杏仁茶、合桃花生露）。
  - 包新鮮：出產各款麵包、蛋糕産品，其産品均由[奇華餅家或](../Page/奇華餅家.md "wikilink")[美心西餅生産](../Page/美心西餅.md "wikilink")。
  - 滋味選：出產各款日韓及西式快餐飯盒，購買後必須以微波爐加熱才可食用。
  - [龍鳳茶樓](../Page/龍鳳茶樓.md "wikilink")：出產各款中式點心及[盅頭蒸飯快餐飯盒](../Page/盅頭蒸飯.md "wikilink")，購買後必須以微波爐加熱才可食用。
  - 7-Signature：出產各款小食、三文治、凍卷等快餐産品。
  - MOMOKO：出產各款膠樽裝茶類飲品。
  - 御之簡食：出產各款日式食品（飯團、溫泉玉子）其出産之飯團不含壽司醋，令其存放在4℃以下低溫數天，飯質亦不會變硬，提升口感。壽司飯以[鹽代替壽司醋調味](../Page/鹽.md "wikilink")。

#### 特色

  - 香港7-Eleven是全世界7-Eleven家族中平均單店來客數最高的區域。
  - 香港7-Eleven土地密度，以2009年12月共有963間分店計算，平均每1.14平方公里有一間分店；但仍不及於澳門7-Eleven不足1平方公里便有一分店；
  - 香港7-Eleven人口平均分店密度計測，平均每約8,750人便有一間分店，僅次於臺灣排名第二。
  - 部份繁忙地區的7-Eleven密度很高，分店的距離可能只相隔一個路口，甚至同一個[港鐵站的車站大堂也有](../Page/港鐵.md "wikilink")3間分店，而僅[落馬洲站範圍內更多達](../Page/落馬洲站.md "wikilink")6間分店。
  - 香港的7-Eleven分店數遠遠超越其競爭對手。2007年7月中，7-Eleven在香港擁有800間分店，其唯一主要競爭對手[OK便利店只得](../Page/OK便利店.md "wikilink")268間分店，而香港其他連鎖便利商店集團更只是小規模經營，除[華潤集團的](../Page/華潤.md "wikilink")[VanGO外](../Page/VanGO.md "wikilink")，其餘一般不會超過10間分店。
  - 香港人喜愛暱稱7-Eleven為「7仔」。由於其市場主導地位，「7仔」已成為不少香港人對便利商店的統稱。
  - 店內可以使用[八達通卡消費](../Page/八達通.md "wikilink")，也是香港首間接受八達通繳費的非[交通機構](../Page/香港交通.md "wikilink")。由2010年起，更接受visa
    paywave付款，而且在部份日子提供折扣于恆生enjoy卡，此為香港便利店中首次。
  - 香港的7-Eleven提供手提電話充電服務，使用者可以在店舖留下一塊3.6V規格的鋰離子[充電池](../Page/充電池.md "wikilink")，並繳付12港元，便會為其提供30分鐘快速充電（鐵路沿線分店除外）。
  - 香港的7-Eleven和其唯一主要競爭對手[香港OK便利店都是世界上少數全面設有](../Page/OK便利店#香港OK便利店.md "wikilink")[Wi-Fi網絡覆蓋的便利商店](../Page/Wi-Fi.md "wikilink")，而且都由[電訊盈科負責提供有關服務](../Page/電訊盈科.md "wikilink")。
  - 香港的7-Eleven曾長期售賣[新地（聖代）軟雪糕](../Page/圣代.md "wikilink")，但因衛生問題經常不能通過[食物環境衞生署的標準](../Page/食物環境衞生署.md "wikilink")。為保商譽，該商品已經於2007年全線停售。
  - 部份地區之7-Eleven分店設有[匯豐銀行或](../Page/匯豐銀行.md "wikilink")[渣打銀行的](../Page/渣打銀行.md "wikilink")[自動提款機](../Page/自動櫃員機.md "wikilink")（ATM），彌補銀行服務不足\[15\]。
  - 全線7-Eleven均接受[Visa](../Page/Visa.md "wikilink")、[銀聯](../Page/銀聯.md "wikilink")、[易辦事方式付款](../Page/易辦事.md "wikilink")，顧客購物時更可透過[EasyCash方式提款](../Page/EasyCash.md "wikilink")(每次交易可提取100、200、300、400或500港元)
  - 2013年3月18日起，申請人可在港澳兩地任何一間7-Eleven繳付美國簽證申請費。
  - 香港7-Eleven於2014年12月19日推出\[「7-Fans」手機程式，顧客購物時可透過於手機程式內的[二維碼儲分](../Page/二維碼.md "wikilink")、使用優惠券、換領禮品及使用[電子錢包付款](../Page/電子錢包.md "wikilink")。
  - 香港7-Eleven接受支付寶HK付款及增值服務，為支付寶在香港業務的先驅

### 澳門

2005年7-Eleven正式於[澳門開設分店](../Page/澳門.md "wikilink")，由[香港](../Page/香港.md "wikilink")[牛奶公司經營](../Page/牛奶公司.md "wikilink")。首間分店位於[新口岸北京街](../Page/新口岸.md "wikilink")[澳門維景酒店斜對面](../Page/澳門維景酒店.md "wikilink")，不過該店已於2008年遷至原店旁另一較大的舖面經營。在短短的5年間，澳門7-Eleven擴充至現在有44間分店，現時在[澳門半島](../Page/澳門半島.md "wikilink")、[氹仔及](../Page/氹仔.md "wikilink")[路氹城均有分店](../Page/路氹城.md "wikilink")，但[路環則未開設分店](../Page/路環.md "wikilink")。

早在1980年代，位於新口岸區的[舊港澳碼頭內開設了一間同名便利店](../Page/舊港澳碼頭_\(澳門\).md "wikilink")，但該店舖於舊碼頭停用後一同結業，而該便利店並沒有得到授權而經營，因此並非本條目所提及的7-Eleven，只是一家同名便利店。

目前澳門的7-Eleven一般面積較小，售賣的貨品種類與香港的7-Eleven基本相同，但也有部份從[葡萄牙進口的貨品是於香港的](../Page/葡萄牙.md "wikilink")7-Eleven找不到的，而香港7-Eleven提供的贈品換領，澳門7-Eleven亦同樣地提供。與香港7-Eleven情況一樣，其唯一主要競爭對手是[OK便利店](../Page/OK便利店.md "wikilink")。

澳門陸地面積只有30平方公里，但如以2010年9月共有45間分店計算，即不足0.7平方公里便有一分店，是全球7-Eleven土地密度最高的地區，比香港平均每1.14平方公里有一間分店還要高。

#### 特色

  - 澳門7-Eleven除路環外，各區均已設立分店；其中以[花地瑪堂區和](../Page/花地瑪堂區.md "wikilink")[大堂區擁有最多分店](../Page/大堂區.md "wikilink")，各有9間分店；而[風順堂區和](../Page/風順堂區.md "wikilink")[路氹城均只有一間分店](../Page/路氹城.md "wikilink")。
  - 與香港一樣，澳門人會把7-Eleven唸作「七十一」，也有「七仔」或「些粉」的坊間俗稱。
  - 澳門7-Eleven已經成為[澳門通的認可服務供應商](../Page/澳門通.md "wikilink")，接受澳門通卡購物消費、加值和新卡銷售等服務。
  - 澳門7-Eleven除提供[澳門本地的報紙和雜誌外](../Page/澳門報紙.md "wikilink")、亦會提供香港的報紙和雜誌。
  - 澳門7-Eleven提供的贈品換領基本與香港相同。
  - 部份產品在香港7-Eleven的優惠有時未能於澳門使用，由於部份產品可能由個別澳門地區代理進貨，來貨價比香港地區高或者低等原因。舉例由於[酒精類飲品在澳門的稅率較香港為低](../Page/酒精.md "wikilink")，因此使用優惠券的折扣價比澳門發售的原價還要高。
  - 7-Eleven是[澳門首間非政府機構代收水電費用](../Page/澳門.md "wikilink")。
  - 澳門的7-Eleven分店超越其競爭對手一倍之多。截至2010年9月，7-Eleven在澳門擁有45間分店，其唯一主要競爭對手[OK便利店只有](../Page/OK便利店.md "wikilink")21間分店。
  - 澳門的7-Eleven提供手提電話充電服務，使用者可以在店舖留下一塊3.6V規格的鋰離子[充電池](../Page/充電池.md "wikilink")，繳付[澳門幣](../Page/澳門幣.md "wikilink")10圓費用，便會為其提供30分鐘快速充電。
  - 2013年3月18日起，申請人可在港澳兩地任何一間7-Eleven繳付美國簽證申請費。

### 中国大陆

7-Eleven在[中国大陆的主要城市多有分布](../Page/中国大陆.md "wikilink")，但由于中国经济发展不均衡，其经营范围主要集中在经济发达的省份与城市，7-Eleven分布最广的地区当属[广东省](../Page/广东省.md "wikilink")，特别是[珠三角地区](../Page/珠三角地区.md "wikilink")。

#### 广东省

[201504_Two_shops_at_Dongshankou_Station.jpg](https://zh.wikipedia.org/wiki/File:201504_Two_shops_at_Dongshankou_Station.jpg "fig:201504_Two_shops_at_Dongshankou_Station.jpg")[东山口站内的](../Page/东山口站.md "wikilink")7-Eleven门店\]\]
[广东地区](../Page/广东.md "wikilink")7-Eleven由广东赛壹便利店有限公司（又称“南中国7-Eleven”，香港[牛奶国际控股](../Page/牛奶国际.md "wikilink")65%，广东信捷商务发展有限公司参股35%）授權经营，1992年最先在[深圳開幕](../Page/深圳.md "wikilink")5间分店，1996年进入[广州](../Page/广州.md "wikilink")（俗称“七仔”），店铺主要分布于[广州](../Page/广州市.md "wikilink")、[深圳](../Page/深圳市.md "wikilink")、[东莞](../Page/东莞市.md "wikilink")、[佛山](../Page/佛山市.md "wikilink")、[中山](../Page/中山市.md "wikilink")、[珠海](../Page/珠海市.md "wikilink")、[江门](../Page/江门市.md "wikilink")、[惠州](../Page/惠州市.md "wikilink")、[湛江](../Page/湛江市.md "wikilink")、[茂名](../Page/茂名市.md "wikilink")、[阳江等城市](../Page/阳江市.md "wikilink")。

2002年3月，南中国7-Eleven收购了在1998年进军广州并已有5间分店的[am/pm](../Page/am/pm.md "wikilink")，而当时[联华仍然认为便利店在广东还是空白](../Page/联华.md "wikilink")，声称会快速开设大量快客便利店，最终失败收场。\[16\]

南中国7-Eleven在2005年获得[商务部批准](../Page/中华人民共和国商务部.md "wikilink")，允许其在[华南地区开展特许经营业务](../Page/华南.md "wikilink")，成为国内首家获得批准的外资连锁零售企业。7-Eleven也与[广州地铁紧密合作](../Page/广州地铁.md "wikilink")，大部分车站均有7-Eleven分店。广州全线分店都可使用[羊城通消费及为其充值](../Page/羊城通.md "wikilink")。

因北方企业水土不服，与7-Eleven及OK便利店这种符合[广州人习惯的粤或港式本土服务相距甚远](../Page/广州人.md "wikilink")，2007年[联华快客在](../Page/联华快客.md "wikilink")[广州的](../Page/广州.md "wikilink")110间分店被南中国7-Eleven全部收购，退出广州。\[17\]上海[农工商超市集团旗下可的便利店在](../Page/农工商超市集团.md "wikilink")2006年退出，关闭[广州](../Page/广州.md "wikilink")30间分店，并将8家经营较好的自营店转让给7-Eleven。\[18\]

至2019年2月，南中國7-ELEVEn在廣東省各地展店數目：廣州536間，深圳284間，東莞31間，佛山155間，中山32間，珠海16間，江門8間，惠州10間，湛江4間，茂名2間，陽江4間。（共計1082店）\[19\]

#### 京津地区

[7-Eleven_Lianhuachi_East_Rd_Store_(20160323144319).jpg](https://zh.wikipedia.org/wiki/File:7-Eleven_Lianhuachi_East_Rd_Store_\(20160323144319\).jpg "fig:7-Eleven_Lianhuachi_East_Rd_Store_(20160323144319).jpg")南侧的7-Eleven便利店\]\]
[201606_A_7-11_store_in_798,_Beijing.jpg](https://zh.wikipedia.org/wiki/File:201606_A_7-11_store_in_798,_Beijing.jpg "fig:201606_A_7-11_store_in_798,_Beijing.jpg")内的7-Eleven便利店\]\]
[京津地区的](../Page/京津地区.md "wikilink")7-Eleven便利店由柒—拾壹（北京）有限公司经营，该公司由日商株式会社[伊藤洋华堂](../Page/伊藤洋华堂.md "wikilink")、[王府井百貨以及中国糖业酒类集团于](../Page/王府井百貨.md "wikilink")2004年1月合资设立，该公司目前拥有京津冀三个省份的店铺经营权，但尚未有在河北省开设直营或加盟店铺的考量。

[北京地区](../Page/北京.md "wikilink")7-ELEVEn首店（[東直門店](../Page/東直門.md "wikilink")）於2004年4月15日開幕，目前主要分布于北京主城四区及通州、丰台、昌平靠近市区的区域。天津地区首店（[小白楼店](../Page/小白楼.md "wikilink")）于2009年9月17日开幕，店面主要分佈於天津市[中心城区](../Page/天津市区.md "wikilink")，2018年起逐步向津南、西青、北辰与东丽等环城区域内大型居住区进驻。[滨海新区的](../Page/滨海新区.md "wikilink")4間店铺因城市发展的政策因素、物流运输成本过高及[2015年天津港危化品仓库爆炸事故的后续影响](../Page/2015年天津港危化品仓库爆炸事故.md "wikilink")，于2017年1月已悉数收店。

北京市政府曾在部分店面推广使用[市政公交一卡通消费](../Page/北京市政交通一卡通.md "wikilink")，后放弃。京津地区7-ELEVEn直至2017年中才先后普及支付宝及微信支付，采用与[卡购商城合作另设扫码机收单方式进行结算](../Page/卡购商城.md "wikilink")，对收银员结算及记账造成诸多不变，此后直至2018年春天才正式将该功能整合至收银机内。然因系统与银联的刷卡机整合不顺，截至2018年10月，银行卡消费及Apple
Pay功能在北京的7-ELEVEn店铺中一直无法正常使用。而天津地区店铺则一直使用旧有的手持式POS进行银行卡交易。

天津地區7-ELEVEn自2016年起即普及7-Coffee服務，並在2018年陸續增加了熱牛奶、可可、奶茶及抹茶拿鐵等更符合在地人消費習慣的飲品種類。而该服务在北京于2017年末才开始慢慢普及，饮品种类则只有美式咖啡及拿铁两种。北京地区店铺使用的商用咖啡机与[星巴克及](../Page/星巴克.md "wikilink")[瑞幸咖啡一致](../Page/瑞幸咖啡.md "wikilink")，而天津地区店铺使用的则与[肯德基](../Page/肯德基.md "wikilink")、[麦当劳等快餐店一致](../Page/麦当劳.md "wikilink")。

至2019年3月末，7-ELEVEn京津地區展店數目：北京272間\[20\]，天津153間\[21\]。（共計425店）

#### 上海市

[上海的](../Page/上海.md "wikilink")7-Eleven便利店于2009年4月29日开业，上海的7-Eleven便利店由[统一超商](../Page/统一超商.md "wikilink")（上海）便利有限公司为[台湾统一超商股份有限公司](../Page/台湾.md "wikilink")100%转投资的子公司经营。

上海7-Eleven在現打飲品方面不止銷售7-Coffee，還銷售台灣統一超商旗下的「城市現萃茶」。

至2019年1月初，7-Eleven上海展店共計130間。\[22\]

#### 成都市

[成都于](../Page/成都.md "wikilink")2011年3月17日在开出第一家店铺，由株式会社7\&i持有股份81%和株式会社[伊藤洋华堂日本持有股份](../Page/伊藤洋华堂.md "wikilink")19%，共同出资成立的柒—拾壹（成都）有限公司經營。

至2018年9月末，7-Eleven成都展店共計78間。\[23\]

#### 山東省

7-Eleven于2012年11月7日在山东[青岛开出第一家店铺](../Page/青岛.md "wikilink")，该地区店铺由山东众邸便利生活有限公司经营，其中[众地集团有限公司持有股份](../Page/众地集团.md "wikilink")65%，株式会社7\&i持有股份35%。于2012年7月9日经过柒一拾壹（中国）投资有限公司授予该公司山东省内的特许经营授权。

山东省7-Eleven曾在部分店铺销售思乐冰饮品，该机器由可口可乐方提供，但由于机器维修零件完全依赖进口，且山东本地无可维修该机器的师傅，因而总公司决定在2018年农历年后将该产品停售并取消。

至2019年2月末，7-Eleven山東展店數目：青島71間\[24\]。（共計71店）

#### 重庆市

[重庆的](../Page/重庆.md "wikilink")7-Eleven于2013年12月24日开业，由[日本](../Page/日本.md "wikilink")[7\&I控股集团旗下柒一拾壹](../Page/7&I控股.md "wikilink")（中国）投资有限公司、日本[三井物产株式会社和](../Page/三井物产.md "wikilink")[新希望集团共同出资成立的新玖商业发展有限公司經營](../Page/新希望集团.md "wikilink")。均可使用重庆[宜居畅通卡进行消费](../Page/宜居畅通卡.md "wikilink")。

至2018年8月末，7-Eleven重慶展店共計65間。\[25\]

#### 浙江省

[杭州与](../Page/杭州.md "wikilink")[宁波的](../Page/宁波.md "wikilink")7-Eleven首店分别于2017年6月29日和2017年7月6日开出，由统一超商（浙江）便利店有限公司独资经营。

至2018年10月末，7-Eleven浙江展店數目：杭州12間\[26\]，寧波14間\[27\]。（共計26店）

#### 江蘇省

柒—拾壹（中國）投資有限公司于2017年11月30日正式授予具台商背景的金鷹國際商貿集團（中國）有限公司簽約，由金鷹旗下的南京金鷹便利超市管理有限公司，取得7-Eleven[江蘇省](../Page/江蘇省.md "wikilink")20年經營權。

江蘇首間7-ELEVEn門市於2018年5月30日上午7點開業，單店日銷超過人民幣37萬，打破全球7-ELEVEn最高單店日銷紀錄。\[28\]

至2018年8月末，7-Eleven江蘇展店數目：南京8間。（共計8店）\[29\]

#### 湖北省

2018年7月11日晚间，[湖北东方美邻便利店有限公司与](../Page/湖北东方美邻便利店有限公司.md "wikilink")[柒—拾壹（中國）投資有限公司签订湖北地区特许经营合同](../Page/柒—拾壹（中國）投資有限公司.md "wikilink")，获得在[湖北省内开展](../Page/湖北省.md "wikilink")7-Eleven便利店业务的许可\[30\]。其中，“湖北美邻”为“无锡商业大厦大东方股份有限公司”的全资子公司。

根据「7-ELEVEn湖北」公众号的信息，全省首店将于2019年3月21日于武汉市汉街楚汉路开幕。\[31\]

至2019年3月末，7-Eleven湖北展店數目：武漢2間。（共計2店）\[32\]

#### 陕西省

2018年12月18日晚间，柒一拾壹（中国）投资有限公司与[陕西赛文提客便利连锁有限公司合作](../Page/陕西赛文提客便利连锁有限公司.md "wikilink")，并授权该公司在[陕西省内的](../Page/陕西省.md "wikilink")7-ELEVEn特许经营权\[33\]，此授权具有唯一性，该公司注册在西咸新区内。陕西赛文提客便利连锁有限公司是陕西提客商贸有限公司的全资子公司，由陕西省实业发展集团和陕西高川实业集团共同出资注册，从事便利店的连锁运营和管理业务，并将从[西安](../Page/西安.md "wikilink")、[咸阳两市开始进行全省便利店的布点](../Page/咸阳.md "wikilink")。

#### 福建省

2019年4月11日，福建榕宁便利店管理有限公司与柒一拾壹（中国）投资有限公司签订福建省特许经营合同\[34\]，获得[中国大陆](../Page/中华人民共和国.md "wikilink")[福建省境内的](../Page/福建省.md "wikilink")7-ELEVEn特许经营权。7-ELEVEn福建门店初期将立足[福州](../Page/福州市.md "wikilink")，并在未来向[厦门](../Page/厦门市.md "wikilink")、[泉州等城市扩张](../Page/泉州市.md "wikilink")。\[35\]。

### 新加坡和马来西亚

[新加坡的](../Page/新加坡.md "wikilink")7-Eleven是[牛奶国际的一员](../Page/牛奶国际.md "wikilink")，自1983年起由美国的7-Eleven总部授权经营。1983年6月，7-Eleven在[樟宜路上段设立了第一家分店](../Page/樟宜路.md "wikilink")。时至今日，7-Eleven在新加坡已经设立了455间店面。7-Eleven在新加坡除了零售外，还提供包括[NETS-FlashPay充值](../Page/NETS-FlashPay.md "wikilink")、[易通卡出售](../Page/易通卡.md "wikilink")，缴付帐单等服务。位於新加坡的7-Eleven多數都是設立與購物中心和百貨商店的一層，並與內部相連，成為一個進入內部的通路。另外一些7-Eleven設立與地鐵站中，但是會因為地鐵站營業時間的限制無法24小時經營。極小部分的7-Eleven是獨立在類似加油站地區的戶外。除此之外，根據新加坡7-Eleven的消費條例，在部分店面消費滿20[新加坡幣即可使用信用卡消費](../Page/新加坡幣.md "wikilink")\[36\]

#### 特色

  - 新加坡的7-Eleven出售的[NETS-FlashPay與新加坡](../Page/NETS-FlashPay.md "wikilink")[SMRT以及](../Page/SMRT.md "wikilink")[SBS
    Transit所出售的](../Page/SBS_Transit.md "wikilink")[易通卡所屬於不同的系統](../Page/易通卡.md "wikilink")，雖然都可以乘坐地鐵，但是不可以在相互的充值點充值。
  - 新加坡7-Eleven和其他地區一樣出售手機SIM卡，但是由於法律規定手機號碼實名制，顧客在購買時亦需要提交ID卡或護照。
  - 在新加坡，部分大型的7-Eleven中會放置低齡電動機供兒童娛樂，不過有需要監護人陪同的限制。
  - 大部分的熟食（如[三明治](../Page/三明治.md "wikilink")，[麵包](../Page/麵包.md "wikilink")）等貨物均為由馬來西亞運輸的商品。
  - 新加坡多數7-Eleven由兩位店員值班，一般情況下可以使用流利的[華語以及](../Page/華語.md "wikilink")[英語甚至是使用](../Page/英語.md "wikilink")[馬來語和](../Page/馬來語.md "wikilink")[泰米爾語與顧客溝通](../Page/泰米爾語.md "wikilink")。
  - 儘管大部分設立於地鐵站中的7-Eleven會由於地鐵的開放時間限制而無法24小時營業，但亦有小部分設立於地鐵站外卻在地下的7-Eleven實行24小時營業。
  - 作為政府的[講華語運動之一](../Page/講華語運動.md "wikilink")，新加坡7-Eleven與地鐵站一樣會在每日早上上班時期免費提供[TODAY和](../Page/TODAY.md "wikilink")[我报這兩種英語及華語的報紙](../Page/我报.md "wikilink")。

### 韓國

1988年[韓國SEVEN成立](../Page/韓國.md "wikilink")，1989年於首爾[松坡區奧運選手村商店街成立首家分店](../Page/松坡區.md "wikilink")。目前全國有9,231家分店，隸屬於[樂天
(Lotte)](../Page/樂天_\(Lotte\).md "wikilink")。

### 臺灣

[臺灣的](../Page/臺灣.md "wikilink")7-Eleven是起源於1978年4月由[統一企業集資](../Page/統一企業.md "wikilink")[新臺幣](../Page/新臺幣.md "wikilink")1億9千萬元，創辦「統一超級商店股份有限公司」，並於1979年引進7-Eleven。同年5月，14家「統一超級商店」於[臺北市](../Page/臺北市.md "wikilink")、[高雄市與](../Page/高雄市.md "wikilink")[臺南市同時開幕](../Page/臺南市.md "wikilink")。

臺灣7-Eleven早期因民眾消費習慣等因素，而出現連續7年的虧損窘境，1982年因虧損累累被合併入[統一企業](../Page/統一企業.md "wikilink")，而在之後經歷了一段時間的努力與摸索，並調整商品品項與經營方式，由原本完全移植自美國的風格逐漸本土化，終於在1986年轉虧為盈，並於1987年重新獨立為[統一超商股份有限公司](../Page/統一超商股份有限公司.md "wikilink")，其後逐漸在國內的通路競賽中嶄露頭角，最後終贏得臺灣[零售業第一的地位](../Page/零售業.md "wikilink")，並於2000年4月20日與美國7-Eleven簽訂永久的授權契約。1994年7月千成門市成立後首度突破1,000家，1999年突破2,000家。1995年進入[宜蘭](../Page/宜蘭縣.md "wikilink")，1996年進入[花東地區](../Page/東臺灣.md "wikilink")，完成臺灣本島縣市全部展店目標。1999年開始也跨海至離島展店，先後在[澎湖縣](../Page/澎湖縣.md "wikilink")、[金門縣本島](../Page/金門縣.md "wikilink")、[烈嶼](../Page/烈嶼鄉.md "wikilink")、[馬祖](../Page/馬祖.md "wikilink")[南竿](../Page/南竿鄉.md "wikilink")、[北竿](../Page/北竿鄉.md "wikilink")、[東引](../Page/東引鄉.md "wikilink")、[綠島](../Page/綠島鄉.md "wikilink")、[蘭嶼](../Page/蘭嶼.md "wikilink")、[琉球等島成立超過](../Page/琉球鄉.md "wikilink")41家門市。目前有5,221間分店。

#### 特色

[Taipei_Bridge_Station_7-ELEVEN.jpg](https://zh.wikipedia.org/wiki/File:Taipei_Bridge_Station_7-ELEVEN.jpg "fig:Taipei_Bridge_Station_7-ELEVEN.jpg")[台北橋站的](../Page/台北橋站.md "wikilink")7-Eleven\]\]
[Slurpee_Machine.jpg](https://zh.wikipedia.org/wiki/File:Slurpee_Machine.jpg "fig:Slurpee_Machine.jpg")

  - 臺灣在門市總數上僅次於美國、日本、韩国與泰國，但在土地平均分店密度方面，臺灣遙遙領先在美日之前，在人口平均分店密度方面，臺灣則以每4,900人一家分店的平均密度，世界排名第一。
  - 在臺灣，從[車站](../Page/車站.md "wikilink")、[街道](../Page/街道.md "wikilink")、[體育場](../Page/體育場.md "wikilink")、[學校](../Page/學校.md "wikilink")、[醫院](../Page/醫院.md "wikilink")、[百貨公司](../Page/百貨公司.md "wikilink")、[國道休息站到公司行號都有](../Page/中華民國國道服務區列表.md "wikilink")7-Eleven的蹤跡，店面大小差異也很大，相較日本近幾年才跨入特殊通路店鋪，臺灣7-Eleven店型變化之大可以堪稱全球之最。現今臺灣的7-Eleven也向有車流的道路旁展店，該型店面具有可容納汽機車的停車空間。
  - 2000年開始與[臺鐵合作](../Page/臺鐵.md "wikilink")，在火車站站內新開「Express」門市，後來有其他業者跟進；目前臺鐵[臺北車站站內擁有](../Page/臺北車站.md "wikilink")6間「Express」門市（其中兩間位於付費區內）和2間高鐵門市（也在付費區內），因應車站重新裝修，各店鋪也將輪流整修\[37\]。
  - 是臺灣第一個完成臺灣本島各縣市全面展店與第一個跨入離島展店的便利商店業者，但仍有部份較偏遠鄉鎮尚未展店。
  - 店內提供的代收業務，可以讓消費者直接在店內繳交水電等多達上百種費用。\[38\]
  - [自動提款機](../Page/自動櫃員機.md "wikilink")（ATM）與[臺灣中國信託商業銀行獨家簽約](../Page/中國信託商業銀行.md "wikilink")，機臺數超過3,000台以上，並於2007年4月起雙方合作發行兼具[電子錢包與](../Page/電子錢包.md "wikilink")[信用卡功能的](../Page/信用卡.md "wikilink")「icashwave」卡\[39\]。
  - 店內設置有多媒體終端機[ibon](../Page/ibon.md "wikilink")，可提供售票、繳費與信用卡紅利兌換等服務。
  - 本身沒有直接經營網路購物業務，是透過子公司介入經營，並與許多網路購物業者簽約提供到店取貨或付款的服務。
  - 2005年複製香港7-Eleven的經驗，以購物滿額送贈品的方式，在臺灣掀起全店整合行銷戰爭，業績大幅成長。
  - 店內可以使用自行開發[iCash卡](../Page/iCash.md "wikilink")（屬預付儲值卡）消費，2007年再增加由臺灣的中國信託發行的[Visa
    payWave信用卡付款](../Page/Visa_payWave.md "wikilink")（必須有加註icashwave字樣才可使用）。另自行發售禮券，使用範圍不限7-Eleven，多數統一流通次集團店面皆可使用。
  - 透過日本廣告公司設計專屬代言人「OPEN小將」，並設計有一系列商品。
  - 成立7-mobile（後更名為ibon
    Mobile）於各7-Eleven門市皆可申辦手機電信服務，此服務通信訊號由[遠傳電信提供](../Page/遠傳電信.md "wikilink")。
  - 2008年，統一集團有鑑於「7-Eleven」（[統一超商](../Page/統一超商.md "wikilink")）為臺灣最知名之統一集團關係企業，因而將「7-ELEVEn」加入至集團旗下的棒球隊名當中，成為中華職棒首次有企業品牌名稱加入隊名的球隊。
  - 2008年9月於[桃園縣設立第一家附設自助加油站的門市](../Page/桃園市.md "wikilink")。
  - 2010年4月，開始接受[悠遊卡小額消費](../Page/悠遊卡.md "wikilink")。
  - 2015年1月16日，開放使用「[一卡通票證公司](../Page/一卡通票證公司.md "wikilink")」[一卡通小額消費](../Page/一卡通_\(台灣\).md "wikilink")。
  - 2018年1月29日，全球首家7-Eleven無人便利店「X-STORE」今天在在台北市東興路統一超商總部1樓開業。

## 各地分店圖片

<File:HK-711-711.JPG>|[香港的第](../Page/香港.md "wikilink")711家分店，於2006年7月11日開幕
<File:7-ELEVEn> Guangzhou.JPG|中国[广州市的分店](../Page/广州市.md "wikilink")
[File:7-11_Qianmen_shop.jpg|中国](File:7-11_Qianmen_shop.jpg%7C中国)[北京市的分店](../Page/北京市.md "wikilink")
[File:天津小白楼7-11便利店.jpg|中国](File:天津小白楼7-11便利店.jpg%7C中国)[天津市](../Page/天津市.md "wikilink")[小白樓的分店](../Page/小白樓.md "wikilink")
7-Eleven_Copenhagen.jpg|[丹麥](../Page/丹麥.md "wikilink")[哥本哈根的分店](../Page/哥本哈根.md "wikilink")
<File:Yan> On Estate 2013
part4.JPG|香港[馬鞍山](../Page/馬鞍山_\(香港市鎮\).md "wikilink")[欣安邨的分店](../Page/欣安邨.md "wikilink")
<File:7-Eleven> Dongchi Store 20080803
night.jpg|[台灣](../Page/台灣.md "wikilink")[台東縣](../Page/台東縣.md "wikilink")[池上鄉的東馳門市](../Page/池上鄉.md "wikilink")
<File:7-Eleven_Lund.jpg>|[瑞典](../Page/瑞典.md "wikilink")[隆德的分店](../Page/隆德.md "wikilink")
<File:7_Eleven_Singapore.jpg>|[新加坡的分店](../Page/新加坡.md "wikilink")
<File:SEJ-haJi.JPG>|[東京](../Page/東京.md "wikilink")[原宿竹下通的分店](../Page/原宿竹下通.md "wikilink")
<File:7-Eleven>
Ibaraki-Funaki.JPG|[大阪](../Page/大阪.md "wikilink")[茨木市的分店](../Page/茨木市.md "wikilink")
<File:Front> Of Oklahoma 7-Eleven With Icy Drink
Ad.jpg|[美國](../Page/美國.md "wikilink")[奧克拉荷馬州的分店](../Page/奧克拉荷馬州.md "wikilink")，店面門口有Icy
Drink飲品廣告橫額 <File:201701> 7-Eleven store in Soi Mai Khao
5.jpg|[泰国](../Page/泰国.md "wikilink")[普吉岛的一家门店](../Page/普吉岛.md "wikilink")
<File:E6高速公路> 法尔肯贝里 Shell 7-Eleven 商店内部(inside) -
panoramio.jpg|[瑞典E](../Page/瑞典.md "wikilink")6高速公路法尔肯贝里的[Shell](../Page/壳牌石油.md "wikilink")
7-Eleven门店内部

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - 鈴木敏文：《7-Eleven經商之道》，商周文化事業（2005年1月），ISBN 986-124-279-1.
  - 楊瑪利：《臺灣7-ELEVEN創新行銷學》，天下雜誌（2005年1月20日），ISBN 9877561597.
  - 李仁芳：《7-ELEVEN統一超商縱橫臺灣》，遠流出版事業（1995年7月1日），ISBN 957-32-2590-5.
  - 胜见明：《巷口商学院》，中国城市出版社（2009年4月1日），ISBN 978-7-5074-2084-5.

## 外部連結

  -
  -
  -
  -
  -
  -
  -
  - [臺灣7-ELEVEN](http://line.naver.jp/ti/p/%407-eleven_tw)的[LINE官方帳號](../Page/LINE_\(應用程式\).md "wikilink")

  -
  - [7BLOG](http://blog.7-11.com.tw/)

## 參見

  - [統一超商](../Page/統一超商.md "wikilink")
  - [牛奶國際](../Page/牛奶國際.md "wikilink")
  - [伊藤洋華堂](../Page/伊藤洋華堂.md "wikilink")
  - [南方公司](../Page/南方公司.md "wikilink")

{{-}}

[Category:1927年成立的公司](../Category/1927年成立的公司.md "wikilink")
[Category:便利商店](../Category/便利商店.md "wikilink")
[Category:零售商](../Category/零售商.md "wikilink")
[Category:總部在日本的跨國公司](../Category/總部在日本的跨國公司.md "wikilink")
[Category:日本零售商](../Category/日本零售商.md "wikilink")
[Category:美國零售商](../Category/美國零售商.md "wikilink")
[Category:澳門零售業](../Category/澳門零售業.md "wikilink")
[Category:香港零售商](../Category/香港零售商.md "wikilink")
[Category:日本便利商店](../Category/日本便利商店.md "wikilink")
[Category:台灣便利商店](../Category/台灣便利商店.md "wikilink")
[Category:美國連鎖速食店](../Category/美國連鎖速食店.md "wikilink")
[Category:1927年加利福尼亞州建立](../Category/1927年加利福尼亞州建立.md "wikilink")

1.  "[7-Eleven, Inc. Announces Aggressive Growth Plans Throughout
    SoCal](http://www.cbre.com/NR/rdonlyres/1C0F25B8-CDBC-4839-AE4B-77AFEF021249/0/SoCalCBRE724logo.pdf)
    ." 7-Eleven. Retrieved on November 15, 2009.
2.  [7–11 Corporate
    website](http://www.sej.co.jp/company/en/g_stores.html)
3.  2005年11月9日，[Seven-ELEVEN Japan Completes Cash Tender Offer
    for 7-ELEVEN,
    Inc.](http://www.7-ELEVEN.com/newsroom/articles.asp?p=2371)
    ，7-ELEVEN News Room；2007年6月11日驗證引用。
4.  [セブン-イレブン・ジャパン
    世界のセブン-イレブン](http://www.sej.co.jp/company/tenpo.html)，日本7-Eleven官方網站
5.  [台灣 7-11全台電子地圖系統](http://emap.pcsc.com.tw/)
6.  [7月11日（セブン−イレブンデー）に全国のセブン−イレブンで一斉清掃活動を実施](http://www.sej.co.jp/news/h11izen/070102.html)，[日本](../Page/日本.md "wikilink")7-Eleven[新聞稿](../Page/新聞稿.md "wikilink")；2007年6月11日驗證引用。
7.  [小「n」謎團猜猜看](http://blog.7-11.com.tw/diary.asp?blogid=27)
8.  [小「n」謎團揭曉](http://blog.7-11.com.tw/diary.asp?blogid=28)
9.  [常見問題－學生篇Q4：為什麼7-ELEVEN的商標LOGO，最後一個n是小寫呢？有無特別的意義呢？](http://www.7-11.com.tw/faq/q4.asp)（臺灣7-11）
10. [International
    Licensing](http://corp.7-eleven.com/AboutUs/InternationalLicensing/tabid/115/Default.aspx)
11. [About Us](http://corp.7-eleven.com/AboutUs/tabid/73/Default.aspx)
12. [セブン-イレブン・ジャパン
    国内都道府県別店舗数](http://www.sej.co.jp/corp/company/tenpo01.html)，日本7-Eleven
13. [セブン銀行官方網站](http://www.sevenbank.co.jp/)
14.
15. [匯豐銀行香港：7-Eleven自動櫃員機服務](http://www.hsbc.com.hk/1/2/chinese/hk/personal/customer-care/improve/detail#atm711)

16.
17.
18.
19.
20.
21.
22.
23.
24.
25.
26.
27.
28.
29.
30.
31.
32.
33.
34.
35.
36. [7-Eleven:Company
    Info](http://www.7-eleven.com.sg/company-info.html)
37. [台灣7-Eleven Blog](http://blog.7-11.com.tw/diary.asp?blogid=20)
38. 臺灣 7-Eleven 官方網站服務一覽，2007年6月11日
39. [臺灣 7-Eleven
    新聞稿](http://www.7-11.com.tw/pcsc/news/news_detail.asp?typeid=01&SerialID=0120070117001&SearchType=&SearchCriteria=)，2007年6月11日