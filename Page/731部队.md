**731部队**是[旧日本帝国陆军](../Page/旧日本帝国陆军.md "wikilink")[關東軍](../Page/關東軍.md "wikilink")本部的[通稱號](../Page/通稱號.md "wikilink")。該單位由[石井四郎所領導](../Page/石井四郎.md "wikilink")，因此也稱之為「石井部隊」。“731部队”同时也可以是指在[抗日战争和](../Page/抗日战争.md "wikilink")[第二次世界大战期间](../Page/第二次世界大战.md "wikilink")，旧日本帝国陆军於[日本以外領土从事](../Page/日本.md "wikilink")[生物战](../Page/生物战.md "wikilink")、[细菌战和](../Page/细菌战.md "wikilink")[人體試驗相关研究的所有秘密](../Page/人體試驗.md "wikilink")[军事](../Page/军事.md "wikilink")[医疗部队](../Page/医疗.md "wikilink")，也代指大日本帝国陆军在占领[满洲期间所做的生物战和人体试验研究](../Page/满洲.md "wikilink")。
[缩略图](https://zh.wikipedia.org/wiki/File:Building_on_the_site_of_the_Harbin_bioweapon_facility_of_Unit_731_関東軍防疫給水部本部731部隊（石井部隊）日軍第731部隊旧址_PB121201.JPG "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Unit_731_-_Complex.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Unit_731_victim.jpg "fig:缩略图")
731部队的正式編號是[關東軍满洲第](../Page/關東軍.md "wikilink")691部隊（關東軍防疫給水部）下之满洲第731部队（防疫給水部本部），研究內容对外宣传主要以研究防治疾病與飲水淨化為主，但其实該部隊使用活體[中国人](../Page/中國人.md "wikilink")、[苏联人和](../Page/苏联人.md "wikilink")[朝鲜人進行](../Page/朝鲜人.md "wikilink")[生物武器與](../Page/生物武器.md "wikilink")[化學武器的效果實驗](../Page/化學武器.md "wikilink")。731部队把基地建在[中国东北](../Page/中国东北.md "wikilink")[哈尔滨](../Page/哈尔滨.md "wikilink")[平房区](../Page/平房区_\(哈尔滨市\).md "wikilink")，这一区域当时是日本[傀儡政權](../Page/傀儡政權.md "wikilink")[满洲国的一部分](../Page/满洲国.md "wikilink")。一些研究者认为至少10,000名中国人、苏联人、[朝鲜人和](../Page/朝鲜人.md "wikilink")[同盟国](../Page/同盟國_\(第二次世界大戰\).md "wikilink")[战俘在](../Page/战俘.md "wikilink")731部队的试验中被害，据日本作家[森村诚一在](../Page/森村诚一.md "wikilink")《》中称，通过“特别输送”进入到731部队的“马路大”需要进行编号，而从1939年以后，进行了两轮编号，每一轮编号极限为1,500，于是在抗战结束时，共计有3,000人死于此\[1\]\[2\]。

## 日本生化部队概况

日本在戰時主要的衛生研究部門分别於日本、中国、新加坡。而位于中国大陆的研究单位主要集中于满洲国境内，除了研究单位以外另外有63个支队分布在各战场。

1.  **陆军军医学校防疫研究室**，驻地日本东京新宿，实为细菌武器研究室
2.  **关东军防疫给水部**（[通称号](../Page/通稱號.md "wikilink")：满洲第[691部队](../Page/691部队.md "wikilink")）驻地哈尔滨
    1.  **关东军防疫给水部本部**（满洲第**731部队**）驻地哈尔滨[平房区](../Page/平房区_\(哈尔滨市\).md "wikilink")
    2.  **关东军军马防疫厂**（满洲第），驻地长春，下设2630部队等。负责人[高桥隆笃兽医中将和](../Page/高桥隆笃.md "wikilink")[松有次郎兽医少将](../Page/松有次郎.md "wikilink")，进行过大量牲畜试验
3.  **[北支那方面军防疫给水部](../Page/华北方面军.md "wikilink")**（甲第[1855部队](../Page/1855部队.md "wikilink")），驻地北京[神乐署](../Page/神乐署.md "wikilink")，原国民党中央防疫处所在地，后称**第151兵站医院**，部长初为黑江，后为[菊池齐](../Page/菊池齐.md "wikilink")，1939年[西村英二继任也被称为西村部队](../Page/西村英二.md "wikilink")。下设3个课：
    1.  第1课，设于[协和医学院](../Page/协和医学院.md "wikilink")，从事细菌（生物）战剂的研究
    2.  第2课，设于[天坛公园西门南侧](../Page/天坛公园.md "wikilink")，从事细菌生产
    3.  第3课，设于[北海旁](../Page/北海公园.md "wikilink")[北京图书馆西原](../Page/北京图书馆.md "wikilink")[北平静生生物调查所和](../Page/北平静生生物调查所.md "wikilink")[北平社会调查所](../Page/北平社会调查所.md "wikilink")，为细菌武器研究所
    4.  在济南、天津、太原、青岛、郑州、开封、郾城等地派驻多个支队
4.  **[中支那方面军防疫给水部](../Page/华中方面军.md "wikilink")**（荣第），驻地南京原南京陆军中央医院，又称“多摩部队”。部队长为[桔田武夫中佐](../Page/桔田武夫.md "wikilink")，副部队长兼研究课长为[小林贤二少佐](../Page/小林贤二.md "wikilink")。下设7个课，在上海、南京、岳阳、荆门、宜昌等地派驻12个支队
5.  **[南支那方面军防疫给水部](../Page/华南方面军.md "wikilink")**（波第），驻地广州原百子路中山大学医学院，部队长先后为[田中严大佐](../Page/田中严.md "wikilink")、、、[龟泽鹿郎](../Page/龟泽鹿郎.md "wikilink")。下设6个课：
    1.  总务课，课长[熊仓少佐](../Page/熊仓.md "wikilink")
    2.  细菌研究课，课长[沟口少佐](../Page/沟口.md "wikilink")
    3.  防疫给水课，课长[江口少佐](../Page/江口.md "wikilink")
    4.  传染病治疗课，课长少佐
    5.  鼠疫培养和病体解剖课，课长[渡边少佐](../Page/渡边.md "wikilink")
    6.  器材供应课
6.  **[南方军防疫给水部](../Page/南方軍_\(日本陸軍\).md "wikilink")**（冈第），驻地新加坡

## 关东军防疫给水部

本部设于哈尔滨平房区，即731部队。

### 形成

[Shiro-ishii.jpg](https://zh.wikipedia.org/wiki/File:Shiro-ishii.jpg "fig:Shiro-ishii.jpg")

731部队的前身，是[石井四郎于](../Page/石井四郎.md "wikilink")1932年在中国东北[哈尔滨市郊的](../Page/哈尔滨.md "wikilink")[背阴河设立的](../Page/背阴河.md "wikilink")[东乡部队](../Page/东乡部队.md "wikilink")。

1932年，[石井四郎率部队在哈尔滨市郊的监狱修建](../Page/石井四郎.md "wikilink")[中马城](../Page/中马城.md "wikilink")，但1935年的一次监狱暴动迫使石井关闭中马城。

其后石井到离哈尔滨更近的平房区重新设立一个新細菌一工廠，於1939年建成，佔地四平方公里。

其中731部隊總部四方樓「監獄」佔地就達四萬平方米。

工廠內安置了500具孵育器和6座能容納兩噸製造培養液之鍋爐等設備。\[3\]

673部队在黑河孙吴县建立细菌实验基地，包括动物饲养、制菌室等300间建筑。

### 主要成员

  - 陆军中将[石井四郎](../Page/石井四郎.md "wikilink")。
  - 陆军中佐（中校）。
  - 医生[北野政次](../Page/北野政次.md "wikilink")。
  - 柄泽班[柄澤十三夫](../Page/柄澤十三夫.md "wikilink")。

### 組織架構

731部队進行分为8个部和4个支队：

  - 總務部（部长[中留金藏中佐](../Page/中留金藏.md "wikilink")（中校），后由[太田澄军医大佐](../Page/太田澄.md "wikilink")（上校）兼任）
    副官室

    調査課

      -
        翻譯班
        印刷班
        寫真班
        兵要地誌班
        調査班
        圖書班

    人事課

    庶務課

      -
        勞務班
        庶務室
        食堂
        酒保
        學校

    企劃課

    經理課

    管理課

      -
        建設班
        工務班
        動力班
        運輸班
        電話班

    軍需課

  - 第一部（在活受试验者身上研究[淋巴腺鼠疫](../Page/淋巴腺鼠疫.md "wikilink")、[霍乱](../Page/霍乱.md "wikilink")、[炭疽病](../Page/炭疽病.md "wikilink")、[伤寒](../Page/伤寒.md "wikilink")、[肺结核](../Page/肺结核.md "wikilink")。为此目的建造了一个容纳300人左右的监狱。部长[菊地齐军医少将](../Page/菊地齐.md "wikilink")。）
    第一課（伤寒）

      -
        田部班（研究伤寒。班长[田部井和军医](../Page/田部井和.md "wikilink")[中校](../Page/中校.md "wikilink")）

    第二課（霍乱）

      -
        湊班（班长[湊政雄陆军技师](../Page/湊政雄.md "wikilink")。研究霍乱）

    第三課（俘虜管理）

      -
        吉田班（健康診断）
        宮川班（Ｘ光）
        在田班（研究Ｘ光等其他放射线）
        栗秋班（藥理）
        草味班（研究药理、毒剂的化学结构。班长[草味正夫药剂少佐](../Page/草味正夫.md "wikilink")（少校））
        石井班（俘虜出入管理）
        蓬田班（俘虜出入管理）
        志村班
        特別班（负责特别秘密监狱的管理和实验动物的培养，负责人[石井刚男和](../Page/石井刚男.md "wikilink")[石井三男](../Page/石井三男.md "wikilink")。）

    第四課（痢疾）

      -
        江島班（研究赤痢及血清学。班长[江岛真平陆军技师](../Page/江岛真平.md "wikilink")。）

    第五課（鼠疫）

      -
        高橋班（研究鼠疫。班长[高桥正彦军医少佐](../Page/高桥正彦.md "wikilink")）

    第六課（病理）

      -
        石川班（研究病理及制作人体组织标本。班长陆军技师）
        岡本班（研究病理、活体及死体解剖。班长[冈本耕造陆军技师](../Page/冈本耕造.md "wikilink")）

    第七課

    第八課（斑疹伤寒）

      -
        野口班（班長野口圭一，研究斑疹伤寒)

    第九課(水棲昆蟲)

      -
        田中班（研究昆虫。班长军医少佐（少校））

    第十課（血清）

      -
        内海班（研究血清、开发疫苗及实行细菌战时当友军感染後对症疗法。）
        小滝班（結核菌素注射液）

    第十一課（結核）

      -
        肥之藤班（炭疽；班长[肥之藤信三](../Page/肥之藤信三.md "wikilink")。）
        太田班（研究炭疽，班长[太田澄軍医大佐](../Page/太田澄.md "wikilink")）
        樋渡班
        降旗班（[腺鼠疫](../Page/腺鼠疫.md "wikilink")）
        金澤班
        貴貴院班（天花）
        二木班（结核菌。班长陆军技师）

    所属課不詳

      -
        笠原班（研究滤过性病毒及当地风土病。班长[笠原四郎军医大佐](../Page/笠原四郎.md "wikilink")）
        吉村班（研究治疗冻伤的有效方法及航空医学。班长陆军技师）
        碇班（炭疽）：研究炭疽；班长[碇常重](../Page/碇常重.md "wikilink")。

  - 第二部（研究生物武器的在战场上的使用，特别是以空中传播细菌和寄生虫的设备的研究。部长[太田澄大佐](../Page/太田澄.md "wikilink")。）

      -
        八木泽班（植物菌）：研究植物菌。班长[八木泽行正陆军技师](../Page/八木泽行正.md "wikilink")。
        燒成班（製造炸彈）
        氣象班
        航空班
        無線班
        田中班（昆蟲）
        篠田班（昆蟲）
        安達實驗場（位於鞠家窑）

  - 第三部（防疫給水部隊，另負責生產生化戰用炮彈，驻扎在哈尔滨。部长江口中佐，下设两个工厂，主要是陶瓷弹壳制造厂，用于生产“石井式”陶瓷细菌彈）
    庶務課

      -
        第一課（檢索）
        第二課（毒物檢查）
        第三課
          -
            濾水班
            給水班
            運輸班
            工作班（濾水機）
            濾水機・彈筒製造窯

  - 第四部（部长[川岛清少将](../Page/川岛清.md "wikilink")，生产各种生物战剂）
    第一課（细菌制造班长[柄泽十三夫军医少佐](../Page/柄泽十三夫.md "wikilink")（少校））

      -
        野口班（班长[野口圭一](../Page/野口圭一.md "wikilink")。生产鼠疫菌和炭疽菌）

    第二課（研究乾燥菌和疫苗。负责人[三谷幸雄](../Page/三谷幸雄.md "wikilink")。）

    第三課（研究乾燥菌和疫苗）

    第四課（疫苗）

      -
        有田班（生产斑疹伤寒及疫苗。班长[有田正义军医少佐](../Page/有田正义.md "wikilink")（少校）。）
        植村班（生产瓦斯坏疽菌和炭疽菌。班长植村肇。）

    所属課不詳

      -
        朝比奈班（班长陆军技师。製造）

  - 教育部（部长[园田太郎大佐](../Page/园田太郎.md "wikilink")（上校），后由[西俊英军医中佐接任](../Page/西俊英.md "wikilink")。负责培训部隊相關人才）
    庶務課

    教育課

    衛生兵

    炊事班

    診療所

    錬成隊

    少年隊

  - 資材部（部长少将。 负责器材设备的供应）
    庶務課

    第一課（藥物合成）

      -
        山口班（細菌彈）
        堀口班（）

    第二課（購買補給）

    第三課（濾水機）

    第四課（倉庫）

    第五課（兵器保管）

    第六課（動物飼育）

  - 診療部（附屬醫院，部长[大佐](../Page/上校.md "wikilink")。负责细菌感染的预防和日本人的医疗）
    傳染病病房

    診療室

    家族診療所

    憲兵室

    保機隊

石井的衛生部队（659部队）除了平房本部（731部队）外，还有4个支队：

  - 162部队（林口支队）：驻地[林口县](../Page/林口县.md "wikilink")，支队长[西俊英军医中佐](../Page/西俊英.md "wikilink")，后由[榊原秀夫军医少佐](../Page/榊原秀夫.md "wikilink")（少校）接任。
  - 643部队（牡丹江或海林支队）：驻地[海林镇](../Page/海林镇.md "wikilink")，支队长军医少佐（少校）。
  - 673部队（孙吴支队）：驻地[黑河市](../Page/黑河市.md "wikilink")[孙吴县](../Page/孙吴县.md "wikilink")，支队长[西俊英军医中佐](../Page/西俊英.md "wikilink")（中校）。
  - 543部队（海拉尔支队）：驻地[海拉尔市](../Page/海拉尔市.md "wikilink")，支队长[加藤恒则军医少佐](../Page/加藤恒则.md "wikilink")（少校）。
  - 319部队（大连细菌研究所）：驻地[大连市](../Page/大连市.md "wikilink")，前身为[南满洲铁道株式会社卫生研究所](../Page/南满洲铁道株式会社.md "wikilink")，1939年划归731部队。

### 设施

[Building_on_the_site_of_the_Harbin_bioweapon_facility_of_Unit_731.jpg](https://zh.wikipedia.org/wiki/File:Building_on_the_site_of_the_Harbin_bioweapon_facility_of_Unit_731.jpg "fig:Building_on_the_site_of_the_Harbin_bioweapon_facility_of_Unit_731.jpg")
[Building_on_the_site_of_the_Harbin_bioweapon_facility_of_Unit_731_関東軍防疫給水部本部731部隊（石井部隊）日軍第731部隊旧址_PB121177.JPG](https://zh.wikipedia.org/wiki/File:Building_on_the_site_of_the_Harbin_bioweapon_facility_of_Unit_731_関東軍防疫給水部本部731部隊（石井部隊）日軍第731部隊旧址_PB121177.JPG "fig:Building_on_the_site_of_the_Harbin_bioweapon_facility_of_Unit_731_関東軍防疫給水部本部731部隊（石井部隊）日軍第731部隊旧址_PB121177.JPG")
[Building_on_the_site_of_the_Harbin_bioweapon_facility_of_Unit_731_関東軍防疫給水部本部731部隊（石井部隊）日軍第731部隊旧址_PB121168.JPG](https://zh.wikipedia.org/wiki/File:Building_on_the_site_of_the_Harbin_bioweapon_facility_of_Unit_731_関東軍防疫給水部本部731部隊（石井部隊）日軍第731部隊旧址_PB121168.JPG "fig:Building_on_the_site_of_the_Harbin_bioweapon_facility_of_Unit_731_関東軍防疫給水部本部731部隊（石井部隊）日軍第731部隊旧址_PB121168.JPG")
[Building_on_the_site_of_the_Harbin_bioweapon_facility_of_Unit_731_関東軍防疫給水部本部731部隊（石井部隊）日軍第731部隊旧址_PB121179ボイラー楝跡.JPG](https://zh.wikipedia.org/wiki/File:Building_on_the_site_of_the_Harbin_bioweapon_facility_of_Unit_731_関東軍防疫給水部本部731部隊（石井部隊）日軍第731部隊旧址_PB121179ボイラー楝跡.JPG "fig:Building_on_the_site_of_the_Harbin_bioweapon_facility_of_Unit_731_関東軍防疫給水部本部731部隊（石井部隊）日軍第731部隊旧址_PB121179ボイラー楝跡.JPG")
731部队的建筑群占地6平方公里，由150余座建筑组成。设施设计用以抵御空袭，防止设施摧毁。建筑群包括各式不同的工厂，还有大約4500个用于饲养[跳蚤的容器](../Page/跳蚤.md "wikilink")，6个用于制造化学制剂的大锅炉以及約1800个用于制造生物制剂的容器，几天内可製出约30千克的[腺鼠疫细菌](../Page/腺鼠疫.md "wikilink")。

如今731部队的一些周边设施正由多家中國工业企业所使用，一部分则作为[731部队罪证遗址供游客参观](../Page/731部队罪证遗址.md "wikilink")。

### 非人道实验

**冻伤实验**：

地点：野外冻伤试验场

强迫受害者的双手裸露在零下35度的户外，并往上淋冰水，人会因为极度寒冷而昏迷。十小时后，手已完全坏死，呈黑紫色，此时将人送入冷冻实验室，受害者苏醒，又将其双手浸入15度的温水中进行解冻，然后将手上坏死的组织撕下，双手便只剩白骨。

**低温实验**：

地点：冷冻实验室

将受害者的双手放入零下196度的速冻柜中进行速冻，随后双手便坚硬如石，呈灰白色，表面凝结了一层厚厚的白霜。此时用玻璃棒猛击受害者的双手，已经被冻硬的手指就会被一根根地打掉，最后手掌也会被击碎。

**灭压实验**：

地点：灭压舱

将人关进灭压舱后进行减压，此时人会有全身即将爆裂的疼痛感，当指数显示为-4后，人会逐渐死亡，身体会变得浮肿。当压力再减时人的眼球会从眼眶里突出，大小肠、排泄物和未消化的食物会从肛门喷出。灭压舱上有专供部队人士观察的玻璃窗。

**毒气实验**：

地点：毒气实验室

将成人、儿童和鸟类同时关进全透明毒气室，随即释放不同浓度，不同种类的毒气，以此观察成人，儿童和鸟类对于毒气的不同反应，受害者会在慢性毒气中逐渐被毒死，抑或是窒息，会有口吐白沫，眼睛流泪甚至内出血，耳鼻口眼出血的情况。

**低温陶瓷细菌弹爆破**：

地点：安达野外试验场

将少则十几名，多则数十名的不同国籍人种的受害者绑于十字木桩上，随后引爆周围的陶瓷细菌弹，受害者会出现全身皮肤肌肉腐烂，组织坏死，甚至肢体断裂等情况，最终因细菌感染和肌肉组织坏死而死亡。

**活体解剖**：

地点：解剖室

经常作为其他实验的辅助工作，在进行细菌注射后，如果人没有出现任何发病的现象，就会被活体解剖以观察器官活动情况。或是在进行人畜杂交实验后，将疑似已经怀孕的妇女进行活体解剖以观察有无胎儿。如果不是为了取器官做标本，其他原因的解剖都不会进行麻醉，因为部队人士认为用麻醉后的人作观察研究结果是不准确的。

#### 东京

二战期间，731部队在[东京](../Page/东京.md "wikilink")[新宿区运营着一家医学院校和其下属的研究设施](../Page/新宿區.md "wikilink")。2006年，曾于战期在该院校工作过的护士石井东洋（音译）透露，1945年[日本投降后不久](../Page/日本投降.md "wikilink")，她在院校的庭院参与掩埋尸体与残肢的援助。\[4\]作为回应，日本[厚生劳动省于](../Page/厚生劳动省.md "wikilink")2011年2月开始了该遗址的挖掘工作。
中国政府要求获取从遗址中挖掘出的遗骸的DNA样本。而由于日本政府从未正式承认731部队的存在，所以拒绝了这一要求。

#### 广州

驻扎在[广州的隶属于](../Page/广州.md "wikilink")[南支那方面军的](../Page/南支那方面军.md "wikilink")[8604部队是](../Page/8604部队.md "wikilink")731部队的关联单位。部队在基地中进行禁食、脱水以及水传[斑疹伤寒的人体试验](../Page/斑疹伤寒.md "wikilink")。战后证词表明，该设施曾作为鼠类养殖场，向部队的医疗分队提供[腺鼠疫的实验带菌媒介](../Page/腺鼠疫.md "wikilink")\[5\]，並大量生產鼠疫桿菌，進行細菌戰。\[6\]

#### 奉天战俘营

服役于英国皇家陆军军械总队的罗伯特·皮蒂少校曾是距哈尔滨平房区350英里的奉天战俘营的英国高级官员。根据他的证词，731部队的医生对战俘施行传染病病菌的常规性注射，并伪装成无害的牛痘疫苗，最终导致186位英国战俘死亡。

## 倖存者

历史上有且仅有4名幸存者成功越狱，他们分别是：国民党吉林省党务第三督导区108支部书记长李广德和党员何家训，110支部书记长张人天，以及中共党员、抗联侦察员李遇迟。1945年8月12日夜，由于苏联红军逼近牡丹江，日军对这里的在押人员进行了大屠杀。日军射击两轮后迅速撤离，并未进入牢房检查，也未找到预先准备的汽油焚毁建筑。天亮后四人纷纷越狱。当时和何家训同室的赵连青也活着，但是赵连青大门都没走出去就突然死去。其余四人于13日逃出。\[7\]

## 731部队与战后政治

由於沒有判罪的緣故，许多前731部队的成员都加入了日本医疗组织。[北野政次领导了日本最大的](../Page/北野政次.md "wikilink")[制药公司](../Page/制药.md "wikilink")：[绿十字](../Page/绿十字.md "wikilink")。其他成员或进入医学院校的领导层，或为日本[厚生省工作](../Page/厚生省.md "wikilink")。\[8\]
\[9\]

1997年，180名中国人（731部队的死者或其家属），对日本政府提出诉讼，要求全面披露731部队事实，道歉并赔偿。

2002年8月，[东京地方法院承认](../Page/东京地方法院.md "wikilink")731部队的存在以及所进行的生物战的行为。

2017年8月13日，[日本放送協會](../Page/日本放送協會.md "wikilink")（NHK）播放纪录片《731部队的真相——精英医者与人体实验》，首次公布了731部隊認罪的錄音資料，并指责多名日本医学界权威专家涉入了731部队人体实验\[10\]\[11\]。

## 相關出版物

### 紀錄片

  - 1988年12月1日，香港上映影片《[黑太阳731](../Page/黑太阳731.md "wikilink")》是一部表现日本在中国開战時731部隊的重要影片，亦是香港電影史上首部[三級電影](../Page/三級片.md "wikilink")。
  - 1991年，韓國電視劇《[黎明的眼睛](../Page/黎明的眼睛.md "wikilink")》則描述731部隊的一小部分。
  - 1992年7月10日，香港再上映《[黑太陽731續集](../Page/黑太陽731續集.md "wikilink")》，但[票房大不如前](../Page/票房.md "wikilink")，故較少人認識。
  - 1994年10月23日，香港再上映《[黑太陽731完結篇](../Page/黑太陽731完結篇.md "wikilink")》。
  - 1995年首播的[華視](../Page/中華電視公司.md "wikilink")[紀錄片](../Page/紀錄片.md "wikilink")《[一寸山河一寸血](../Page/一寸山河一寸血.md "wikilink")》，第三十集
    死亡工廠731。
  - 2017年8月13日，[日本放送協會](../Page/日本放送協會.md "wikilink")（NHK）播放纪录片《731部队的真相——精英医者与人体实验》引发关注\[12\]\[13\]\[14\]。

### 小說與文字出版品

  - 1981年，日本作者[森村诚一與](../Page/森村诚一.md "wikilink")共著了《恶魔的盛宴》（）一书，后于1983年出版了《恶魔的盛宴－结局》。它们是第一个广泛揭露731部队历史的小说出版物。日本歷史學家[秦郁彦称此书为将小说和非幻想作品混同的例子](../Page/秦郁彦.md "wikilink")，此书存在如下的问题：\[15\]
      - 相关人士全是匿名的使得证言无法查证。
      - 书中写到将人关入大型离心器，从而榨干血液，是不可能的。
      - 用针管吸取人的体液，使人木乃伊化是不可能的。
      - 书中讲到将人关入真空室，使得内脏从口、肛、眼等处挤出的情形。但是太空试验已证明这种情况也不可能发生。
      - 所用照片存在大量伪照。
      - 世界日报说森村诚一有编造报道和歪曲美国解密文件的行为\[16\]，泽地久枝也说此书存在编造。\[17\]1983年[角川书店将](../Page/角川书店.md "wikilink")《恶魔的盛宴》删去大部分不可靠内容后再版。
  - 1991年8月，日本、合編《[十五年戰爭極秘資料集 第29集
    七三一部隊作成資料](../Page/十五年戰爭極秘資料集_第29集_七三一部隊作成資料.md "wikilink")》一書。
  - 1994年4月，松村高夫再編著《[＜論爭＞731部隊](../Page/＜論爭＞731部隊.md "wikilink")》一書。
  - 1997年8月，松村高夫再著《[戰爭和疫病――七三一部隊帶來的東西](../Page/戰爭和疫病――七三一部隊帶來的東西.md "wikilink")》一書。
  - 2002年，日本教授[常石敬一再出版](../Page/常石敬一.md "wikilink")《[謀略的十字路口―帝銀事件捜査和731部隊](../Page/謀略的十字路口―帝銀事件捜査和731部隊.md "wikilink")》一書。
  - 2003年，出版《[Unit 731 -
    Testimony](../Page/Unit_731_-_Testimony.md "wikilink")》一書，其後再由[濱田徹翻譯成日文版的](../Page/濱田徹.md "wikilink")《[証言・731部隊的真相―人体実験的全貌和戦後謀略的軌跡](../Page/証言・731部隊的真相―人体実験的全貌和戦後謀略的軌跡.md "wikilink")》。
  - 2005年8月，日本[記者及](../Page/記者.md "wikilink")[報告文學作家](../Page/報告文學.md "wikilink")出版《》一書。她曾於2003年發現731部隊創設者石井四郎的「終戰memo」。
  - 2007年3月，[松村高夫與](../Page/松村高夫.md "wikilink")共同編著《》一書。
  - 2014， Asako Serizawa 撰写小说 Train to Harbin\[18\]

## 相關條目

  - [南石頭大屠殺](../Page/南石頭大屠殺.md "wikilink")
  - [夜櫻花行動](../Page/夜櫻花行動.md "wikilink")：该部队策划的一起针对美国南加州市民的生化武器攻击，最终因日本投降而被取消
  - [美國非道德人體實驗](../Page/美國非道德人體實驗.md "wikilink")
      - 美國政府的[危地馬拉梅毒試驗](../Page/危地馬拉梅毒試驗.md "wikilink")
      - 美國政府對黑人的[塔斯基吉梅毒試驗](../Page/塔斯基吉梅毒試驗.md "wikilink")

## 注释

## 参考资料

## 外部链接

  - [ the truth of Harbin Unit 731(English
    Narration)](http://www.youtube.com/view_play_list?p=83FA5D949DC8B0CA&playnext=1&playnext_from=PL)

  - [The Asian Auschwitz of
    Unit 731](http://www.theage.com.au/articles/2002/08/28/1030508070534.html)—来自[时代报](../Page/时代报.md "wikilink")。2002年8月29日。

  - [Alliance for Preserving the Truth of Sino-Japanese
    War](https://web.archive.org/web/20080113004217/http://www.sjwar.org/)—抗日战争史实维护会（APTSJW）。

  - [Japan's sins of the
    past](http://www.guardian.co.uk/elsewhere/journalist/story/0,7792,1338296,00.html)—来自[卫报](../Page/卫报.md "wikilink")。

  - [中日网
    细菌战](https://web.archive.org/web/20100310191551/http://www.sjhistory.net/site/newxh/yjzt3.htm)

  - [湖南文理学院“细菌战罪行研究所”](https://web.archive.org/web/20160304120711/http://jhsdaj.jinhua.gov.cn/xxdt/xydt/846df0bf_789a_40a9_a550_c4e3806c3089.html)

  - [央視-桑家的70年與731](http://news.cntv.cn/2015/09/05/VIDE1441463935451834.shtml)

## 参考文献

  - Gold, Hal. *Unit 731 Testimony*, Charles E Tuttle Co., 1996. ISBN
    4-900737-39-9
  - Williams, Peter. *Unit 731: Japan's Secret Biological Warfare in
    World War II*, Free Press, 1989. ISBN 0-02-935301-7
  - Endicott, Stephen and Edward Hagerman. *The United States and
    Biological Warfare: Secrets from the Early Cold War and Korea*,
    Indiana University Press, 1999. ISBN 0-253-33472-1
  - Handelman, Stephen and Ken Alibek. *Biohazard: The Chilling True
    Story of the Largest Covert Biological Weapons Program in the
    World--Told from Inside by the Man Who Ran It*, Random House, 1999.
    ISBN 0-375-50231-9 ISBN 0-385-33496-6
  - Harris, Robert and Jeremy Paxman. *A Higher Form of Killing : The
    Secret History of Chemical and Biological Warfare*, Random House,
    2002. ISBN 0-8129-6653-8
  - Barnaby, Wendy. *The Plague Makers: The Secret World of Biological
    Warfare*, Frog Ltd, 1999. ISBN 1-883319-85-4 ISBN 0-7567-5698-7 ISBN
    0-8264-1258-0 ISBN 0-8264-1415-X
  - 《侵華日軍在粵進行細菌戰之概況》 沙東迅

[Category:關東軍](../Category/關東軍.md "wikilink")
[Category:日军在第二次中日战争中的战争罪行](../Category/日军在第二次中日战争中的战争罪行.md "wikilink")
[Category:中国人体试验](../Category/中国人体试验.md "wikilink")

1.  [Book on Japan's germ warfare crimes
    published](http://www.chinadaily.com.cn/en/doc/2003-10/17/content_273165.htm)

2.  [Biological Weapons
    Program](http://www.fas.org/nuke/guide/japan/bw/) --
    [美國科學家聯盟](../Page/美國科學家聯盟.md "wikilink")（FAS）2000年4月16日。

3.  辛華，《[明報](../Page/明報.md "wikilink")》特稿，香港，1995年5月13日

4.  [1](http://www.guardian.co.uk/world/2011/feb/21/japan-excavates-site-human-experiments)

5.  Gold, Hal. *Unit 731: Testimony*. Tuttle Publishing, 2006, p. 50

6.  [日軍曾在廣州拿活人做實驗](http://news.wenweipo.com/2014/04/27/IN1404270022.htm)

7.  “逃出七三一”萨苏，《尊严不是无代价的：从日本史料揭秘中国抗战》，2009.2，山东画报出版社

8.  [An Ethical Blank
    Check](http://www.commondreams.org/views05/0510-24.htm)

9.

10.

11.

12.
13.

14. 林永富，[731部隊殘酷史
    NHK罕見揭露](http://www.chinatimes.com/newspapers/20170816000726-260309)，中時電子報，2017-08-16

15. 秦郁彦 『昭和史の謎を追う（上）』 文藝春秋〈文春文庫〉、1999年、538頁。

16. 文艺春秋1983年二月号

17. 滄海よ眠れ: ミッドウェー 海戦の生と死 澤地久枝 每日新聞社, 1984 P14

18.