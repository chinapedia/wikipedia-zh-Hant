**798艺术区**，又称**大山子艺术区**，是一个位于[北京市](../Page/北京市.md "wikilink")[朝阳区](../Page/朝阳区_\(北京市\).md "wikilink")[大山子地区的一个](../Page/大山子.md "wikilink")[艺术园区](../Page/藝術村.md "wikilink")。艺术区的名字是由北京国营电子工业老厂区的名称沿用而来，如今，798除了数字本来含义以外，一般专指北京798艺术区，在798艺术区的发展过程中形成了798共识，因此798也指这一艺术区引申出的一种文化概念，以及LOFT这种时尚的居住与工作方式，亦称798方式。

## 概况

**798艺术区**所在地，原为《社會主義統一計劃》（Socialist Unification
Plan）的補充，1950年代由苏联援建中國第一個[五年計劃期間建造了](../Page/五年計劃.md "wikilink")156個軍工廠但缺乏相關電子元件、北京當局尋求蘇聯軍工體系的重要電子元件供應方[德意志民主共和國幫助](../Page/德意志民主共和國.md "wikilink")。
故由东德负责设计建造、总面积达110万平方米的重点工业顶目798联合厂，即北京第三无线电器材厂，雖說是蘇聯所援，建筑多为[东德的](../Page/东德.md "wikilink")[包豪斯风格](../Page/包豪斯.md "wikilink")，並沒有太強社會主義建物的氣息。1980年代到1990年代798厂逐渐衰落，从2002年开始，由于租金低廉，来自北京周边和北京以外的诸多艺术家工作室和当代艺术机构开始聚集于此，逐渐形成了一个艺术群落，因当代艺术和798生活方式闻名于世。

一些著名的艺术人如[洪晃](../Page/洪晃.md "wikilink")、[李宗盛等亦先后进驻](../Page/李宗盛.md "wikilink")。并有大量外国人参观访问。自2004年以来已经进行了两届“[北京大山子国际艺术节](../Page/北京大山子国际艺术节.md "wikilink")”。成为北京的文化地标之一。也有新的北京三大景点“[长城](../Page/长城.md "wikilink")，[故宫](../Page/故宫.md "wikilink")，798”的说法。

## 国际影响

798艺术区吸引了众多政界要人、影视明星、社会名流参观。2004年以来，瑞典首相、瑞士首相、德国总理（[施罗德](../Page/施罗德.md "wikilink")）、奥地利总理、欧盟主席（[巴罗佐](../Page/巴罗佐.md "wikilink")）、比利时王妃、[科菲·安南及夫人](../Page/科菲·安南.md "wikilink")、法国总统[希拉克夫人](../Page/希拉克.md "wikilink")、挪威总理夫人、比利时王储、法国总统（[萨科齐](../Page/萨科齐.md "wikilink")）、国际奥委会主席[罗格等都先后参观访问过](../Page/罗格.md "wikilink")798艺术区。

## 对798的批评

### 过度商业化

北京[奥运会以来](../Page/奥运会.md "wikilink")，798艺术区的名气越来越大。越来越多的中外资本涌入这里，不少人期望短时间内淘到破记录的拍卖作品，“一夜暴富”的投机气氛浓厚，但实际上并无多少艺术家能够达到很高的水准。相反，由于租金上涨，许多安心创作的艺术家被迫离开798。

## 画廊

<File:798> Art Zone.jpg|798大门
[File:798创意广场.jpg|right|thumb|200px|798创意广场](File:798创意广场.jpg%7Cright%7Cthumb%7C200px%7C798创意广场)
[File:798工厂展厅.jpg|right|thumb|200px|798著名的车间展厅](File:798工厂展厅.jpg%7Cright%7Cthumb%7C200px%7C798著名的车间展厅)
[File:798artzone2.jpg|合作工厂](File:798artzone2.jpg%7C合作工厂)
<File:798machine> art.jpg|2005年12月当代艺术展览的旧机器
[File:Dior在798的休息区.jpg|Dior在798的休息区](File:Dior在798的休息区.jpg%7CDior在798的休息区)
[File:798ArtZone2009.jpg|2009年的798艺术区](File:798ArtZone2009.jpg%7C2009年的798艺术区)
<File:798废弃铁路> 20150607 131459.jpg|798废弃铁路

## 参见

  - [北京环行铁路](../Page/北京环行铁路.md "wikilink")

## 外部链接

  - [798艺术区](https://archive.is/20130223044234/http://www.798art.org/)

[Category:北京市文化场所](../Category/北京市文化场所.md "wikilink")
[Category:北京市朝阳区](../Category/北京市朝阳区.md "wikilink")
[Category:艺术区](../Category/艺术区.md "wikilink")