## 非洲

  - **[库施](../Page/库施.md "wikilink")**
      - 国王：Natakamani（前1年－20年）
  - **[毛里塔尼亚](../Page/毛里塔尼亚.md "wikilink")**
      - 国王：Juba II（前25年－23年）

## 亚洲

  - **[中国](../Page/中国.md "wikilink")**
      - [汉朝](../Page/汉朝.md "wikilink")
          - 皇帝：[孺子婴](../Page/孺子婴.md "wikilink")（6年－8年）
  - [匈奴](../Page/匈奴.md "wikilink")
      - 单于：[乌珠留若鞮单于](../Page/乌珠留若鞮单于.md "wikilink")（前8年－13年）
  - **[朝鲜](../Page/朝鲜.md "wikilink")**
      - [百济](../Page/百济.md "wikilink")：
          - 国王：[温祚王](../Page/温祚王.md "wikilink")（前18年－28年）
      - [东扶余](../Page/东扶余.md "wikilink")：
          - 国王：[带素](../Page/带素.md "wikilink")（前7年－22年）
      - [高句丽](../Page/高句丽.md "wikilink")：
          - 国王：[琉璃王](../Page/琉璃王.md "wikilink")（前19年－18年）
      - [新罗](../Page/新罗.md "wikilink")：
          - 国王：[南解次次雄](../Page/南解次次雄.md "wikilink")（4年－24年）
  - **[贵霜帝国](../Page/贵霜帝国.md "wikilink")**
      - 翕侯：[赫拉欧斯](../Page/赫拉欧斯.md "wikilink")（1年－30年）

## 欧洲

  - **[阿特雷巴特](../Page/阿特雷巴特.md "wikilink")**
      - 国王：
        1.  Tincomarus（前20年－7年）
        2.  Eppillus（7年－15年）
  - **[卡图维勒尼](../Page/卡图维勒尼.md "wikilink")**
      - 国王：Tasciovanus（前20年－9年）
  - **[高加索伊比里亚](../Page/高加索伊比里亚.md "wikilink")**
      - 国王：Aderk（前2年－30年）
  - **[马科曼尼](../Page/马科曼尼.md "wikilink")**
      - 国王：Marbod（前9年－19年）
  - **[罗马帝国](../Page/罗马帝国.md "wikilink")**
      - 皇帝：[奥古斯都](../Page/奥古斯都.md "wikilink")（前27年－14年）

## 中东

  - **[卡帕多细亚](../Page/卡帕多细亚.md "wikilink")**
      - 国王：Archelaus（前36年－17日）
  - **[科马吉尼](../Page/科马吉尼.md "wikilink")**
      - 国王：Antiochus III（前12年－17年）
  - **[奈巴提亚](../Page/奈巴提亚.md "wikilink")**
      - 国王：Aretas IV Philopatris（前9年－40年）
  - **[奥斯若恩](../Page/奥斯若恩.md "wikilink")**
      - 国王：
        1.  Abgar V（前4年－7年）
        2.  Ma'nu IV bar Ma'nu（7年－13年）
  - **[安息](../Page/安息_\(国家\).md "wikilink")**
      - 国王：空缺（6年－8年）
  - **[本都](../Page/本都.md "wikilink")** -
    [皮托多里斯](../Page/皮托多里斯.md "wikilink")，[本都女王](../Page/本都统治者列表.md "wikilink")（西元前8年－西元23年）

[Category:7年](../Category/7年.md "wikilink")
[Category:历年国家领导人列表](../Category/历年国家领导人列表.md "wikilink")