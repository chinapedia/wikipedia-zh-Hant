**80号州际公路**（****，简称**I-80**）是[美国](../Page/美国.md "wikilink")[州际公路系统的一部份](../Page/州际公路系统.md "wikilink")。西起[加州](../Page/加州.md "wikilink")[旧金山](../Page/旧金山.md "wikilink")，东连[纽泽西州](../Page/纽泽西州.md "wikilink")[提內克](../Page/提內克.md "wikilink")。全长\[1\]，是整个系统第二长的。与[林肯公路](../Page/林肯公路\(美国\).md "wikilink")，以及历史上的[奥勒冈小径](../Page/奥勒冈小径.md "wikilink")、[加利福尼亚小路和](../Page/加利福尼亚小路.md "wikilink")[联合太平洋铁路](../Page/联合太平洋铁路.md "wikilink")[第一条横贯大陆铁路大致平行](../Page/第一条横贯大陆铁路.md "wikilink")。该高速公路连接[纽约](../Page/纽约.md "wikilink")，[芝加哥](../Page/芝加哥.md "wikilink")，[盐湖城](../Page/盐湖城.md "wikilink")，[旧金山等美国重要城市](../Page/旧金山.md "wikilink")，是美国最重要的州际公路之一。

## 加利福尼亚州段

[I-80_(CA).svg](https://zh.wikipedia.org/wiki/File:I-80_\(CA\).svg "fig:I-80_(CA).svg")
80号州际公路在加利福尼亚州连接北部最重要的数个城市：[旧金山](../Page/旧金山.md "wikilink")、[奥克兰和](../Page/奥克兰_\(加州\).md "wikilink")[萨克拉门多](../Page/萨克拉门多.md "wikilink")。旧金山为I-80的西端点，往东经[海湾大桥跨过](../Page/海湾大桥.md "wikilink")[旧金山湾进入奥克兰转向北经](../Page/旧金山湾.md "wikilink")[柏克莱至](../Page/柏克莱.md "wikilink")[列治文](../Page/列治文_\(加利福尼亚州\).md "wikilink")，再转向东北经[卡齐尼兹大桥离开](../Page/卡齐尼兹大桥.md "wikilink")[湾区](../Page/旧金山湾区.md "wikilink")。公路通过沙加缅度后继续往东北进入[内华达山区](../Page/内华达山脉_\(美国\).md "wikilink")，从[太浩湖北岸进入](../Page/太浩湖.md "wikilink")[内华达州往](../Page/内华达州.md "wikilink")[雷诺](../Page/雷诺_\(内华达州\).md "wikilink")。

### 旧金山湾区

[Interstate80westernend.jpg](https://zh.wikipedia.org/wiki/File:Interstate80westernend.jpg "fig:Interstate80westernend.jpg")
[I-80_Eastshore_Fwy.jpg](https://zh.wikipedia.org/wiki/File:I-80_Eastshore_Fwy.jpg "fig:I-80_Eastshore_Fwy.jpg")附近的東灣岸高速公路\]\]
80号州际公路的西端点是和[101号美国国道交会的交流道](../Page/101号美国国道加利福尼亚州段.md "wikilink")，往东接上[海湾大桥往](../Page/海湾大桥.md "wikilink")[奥克兰](../Page/奥克兰_\(加州\).md "wikilink")，过桥之后，I-580在[麦克阿瑟迷宫并入I](../Page/麦克阿瑟迷宫.md "wikilink")-80主线，合称**东湾岸高速公路**，比较特别的是，I-80东（西）向与I-580西（东）向共构，但实际上东湾岸高速公路是南北向。I-580在[奥柏尼离开主线跨过](../Page/奥柏尼_\(加利福尼亚州\).md "wikilink")[列治文─圣拉斐尔大桥](../Page/列治文─圣拉斐尔大桥.md "wikilink")，往北湾的[圣拉斐尔](../Page/圣拉菲尔_\(加利福尼亚州\).md "wikilink")。
I-80主线则继续往北，经[卡齐尼兹桥](../Page/卡齐尼兹桥.md "wikilink")，进入[瓦列霍转向东北](../Page/瓦列霍_\(加利福尼亚州\).md "wikilink")，[680号州际公路在](../Page/680号州际公路_\(加利福尼亚州\).md "wikilink")[柯蒂利亚汇入](../Page/柯蒂利亚_\(加利福尼亚州\).md "wikilink")，离开湾区继续通往[沙加缅度](../Page/沙加缅度.md "wikilink")。

80号州际公路在[旧金山湾区有七条辅助线](../Page/旧金山湾区.md "wikilink")，组成湾区的高速公路网。其中I-280和I-680被认为是湾区的外环线，在湾区南端的[圣荷西合流](../Page/圣荷西_\(加利福尼亚州\).md "wikilink")。
I-580则是湾区通往[5号州际公路的最重要通道](../Page/5号州际公路.md "wikilink")（尤其是南-{向}-往[洛杉矶](../Page/洛杉矶.md "wikilink")）。
I-880连接圣荷西与奥克兰。另外三条公路均为短程，作为往重要地点（如[旧金山国际机场](../Page/旧金山国际机场.md "wikilink")）的联络道。这些州际公路通过的桥梁均为单向收费。

### 沙加缅度

80号州际公路离开湾区之后，进入[沙加缅度谷地](../Page/沙加缅度谷地.md "wikilink")。
[505号州际公路在](../Page/505号州际公路.md "wikilink")[瓦卡维尔附近汇入](../Page/瓦卡维尔\(加利福尼亚州\).md "wikilink")，续往东北行经[戴维斯进入](../Page/戴维斯\(加利福尼亚州\).md "wikilink")[沙加缅度市区](../Page/沙加缅度.md "wikilink")。在市区西侧分出商业线往市中心，公路主线则绕行市区北缘，称为**外环高速公路**（**Beltline
Freeway**）。商业线则称为**首府高速公路**（**Capital City
Freeway**）。二条高速公路与[5号州际公路交会后](../Page/5号州际公路.md "wikilink")，在市区东北角合流继续往东北进入[内华达山脉](../Page/内华达山脉\(美国\).md "wikilink")。

[Business_Loop_80.svg](https://zh.wikipedia.org/wiki/File:Business_Loop_80.svg "fig:Business_Loop_80.svg")
外环高速公路在1958年核准兴建（原始编号为[242号加州州道](../Page/242号加州州道.md "wikilink")），1964年改编为[880号州际公路](../Page/880号州际公路.md "wikilink")，1972年完工。
80号州际公路原本使用首府高速公路穿过沙加缅度，但首府高速公路东半段不符合州际公路标准，1978年，市区内连接首府高速公路和外环高速公路的[160号加州州道升级计划被取消](../Page/160号加州州道.md "wikilink")，联邦即将80号州际公路改道行经外环高速公路，首府高速公路则改编为商业线，外环高速公路的880编号亦撤销。

### 内华达山区

## 中西部段

### 內布拉斯加州

### 爱荷华州

自内布拉斯加州进入爱荷华州，途径[得梅因](../Page/得梅因.md "wikilink")，与纵向35号高速公路交叉，后经过[爱荷华城](../Page/爱荷华城.md "wikilink")，在284号出口附近，有世界最大的高速公路卡车休息中心，再往东经过[阔德城](../Page/阔德城.md "wikilink")，跨越[密西西比河进入伊利诺州](../Page/密西西比河.md "wikilink")。
[I-80_Bridge.jpg](https://zh.wikipedia.org/wiki/File:I-80_Bridge.jpg "fig:I-80_Bridge.jpg")

### 伊利诺州

80号州际公路在[爱荷华州和](../Page/爱荷华州.md "wikilink")[伊利诺州交界的](../Page/伊利诺州.md "wikilink")[阔德城分出](../Page/阔德城.md "wikilink")[88号州际公路与](../Page/88号州际公路\(伊利诺州\).md "wikilink")[74号州际公路后](../Page/74号州际公路.md "wikilink")，往东穿过伊利诺州北部抵达[芝加哥近郊与](../Page/芝加哥.md "wikilink")[294号州际公路](../Page/294号州际公路.md "wikilink")（三州收费公路）共构，过了I-294的南端点，公路继续与[94号州际公路共构往](../Page/94号州际公路.md "wikilink")[印第安纳州](../Page/印第安纳州.md "wikilink")[加里](../Page/加里\(印第安纳州\).md "wikilink")，此段称为**金格利快速道路**（**Kingery
Expressway**）。

本段公路穿过伊利诺州北部平原地带农业区，在进入芝加哥之前仅有零星小镇分布，大致与[6号美国国道平行](../Page/6号美国国道.md "wikilink")。其中在[普林斯顿附近向南分出](../Page/普林斯顿_\(伊利诺州\).md "wikilink")[180号州际公路往](../Page/180号州际公路_\(伊利诺州\).md "wikilink")[亨内平](../Page/亨内平_\(伊利诺州\).md "wikilink")，至[拉萨尔与](../Page/拉萨尔_\(伊利诺州\).md "wikilink")[39号州际公路交叉](../Page/39号州际公路.md "wikilink")，继续往东至芝加哥南郊[乔利埃特与](../Page/乔利埃特_\(伊利诺州\).md "wikilink")[55号州际公路交会后](../Page/55号州际公路.md "wikilink")，[355号州际公路亦在附近汇入](../Page/355号州际公路_\(伊利诺州\).md "wikilink")，再往东行数哩后与[57号州际公路交会后](../Page/57号州际公路.md "wikilink")，并入三州收费公路。

### 印第安纳州

80号州际公路在[印第安纳州分别与](../Page/印第安纳州.md "wikilink")[94号州际公路与](../Page/94号州际公路.md "wikilink")[90号州际公路共构](../Page/90号州际公路.md "wikilink")，其中与94号州际公路共构段名为''
'波尔曼快速道路**（**Borman
Expressway'''）与90号州际公路共构段则是[印州收费公路的一部份](../Page/印州收费公路.md "wikilink")。I-80与I-94共构线由[芝加哥南边的](../Page/芝加哥.md "wikilink")[伊利诺州州界进入印第安纳州](../Page/伊利诺州.md "wikilink")[加里](../Page/加里_\(印第安纳州\).md "wikilink")，与I-90交会后（[湖站](../Page/湖站_\(印第安纳州\).md "wikilink")）换与I-90共构，进入印州收费公路。I-80沿印第安纳州北缘经[南弯横贯全州进入](../Page/南弯_\(印第安纳州\).md "wikilink")[俄亥俄州接续](../Page/俄亥俄州.md "wikilink")[俄亥俄州收费公路往](../Page/俄亥俄州收费公路.md "wikilink")[托莱多](../Page/托莱多_\(俄亥俄州\).md "wikilink")，其中在印州东北角与[69号州际公路交会](../Page/69号州际公路.md "wikilink")。印第安纳州段全长151.56哩（243.91公里）。

### 俄亥俄州

[OhioTurnpike.svg](https://zh.wikipedia.org/wiki/File:OhioTurnpike.svg "fig:OhioTurnpike.svg")

## 美东段

### 宾夕法尼亚州

### 纽泽西州

## 里程与所经主要都市

<table>
<thead>
<tr class="header">
<th><p>所经州份</p></th>
<th><p>哩程数</p></th>
<th><p>所经主要都市<br />
（粗体化的城镇为使用在交通标志上的正式指定定点城市）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>英里[2]</p></td>
<td><p>公里</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/80号州际公路加利福尼亚州段.md" title="wikilink">加利福尼亚州段</a></p></td>
<td></td>
<td><p><strong><a href="../Page/旧金山.md" title="wikilink">旧金山</a></strong>、<strong><a href="../Page/奥克兰_(加州).md" title="wikilink">奥克兰</a></strong>、<a href="../Page/柏克莱.md" title="wikilink">柏克莱</a>、<strong><a href="../Page/沙加缅度.md" title="wikilink">沙加缅度</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/80号州际公路内华达州段.md" title="wikilink">内华达州段</a></p></td>
<td></td>
<td><p><strong><a href="../Page/雷诺_(内华达州).md" title="wikilink">雷诺</a></strong>、<a href="../Page/战斗山_(内华达州).md" title="wikilink">战斗山</a>、<strong><a href="../Page/埃尔科_(内华达州).md" title="wikilink">埃尔科</a></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/80号州际公路犹他州段.md" title="wikilink">犹他州段</a></p></td>
<td></td>
<td><p><strong><a href="../Page/盐湖城.md" title="wikilink">盐湖城</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/80号州际公路怀俄明州段.md" title="wikilink">怀俄明州段</a></p></td>
<td></td>
<td><p><strong><a href="../Page/埃文斯顿_(怀俄明州).md" title="wikilink">埃文斯顿</a></strong>、<strong><a href="../Page/岩石泉_(怀俄明州).md" title="wikilink">岩石泉</a></strong>、<a href="../Page/拉勒米_(怀俄明州).md" title="wikilink">拉勒米</a>、<strong><a href="../Page/夏延_(怀俄明州).md" title="wikilink">夏延</a></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/80号州际公路内布拉斯加州段.md" title="wikilink">内布拉斯加州段</a></p></td>
<td></td>
<td><p><strong><a href="../Page/锡尼_(内布拉斯加州).md" title="wikilink">锡尼</a></strong>、<strong><a href="../Page/北普拉特_(内布拉斯加州).md" title="wikilink">北普拉特</a></strong> 、<strong><a href="../Page/吉尔尼_(内布拉斯加州).md" title="wikilink">吉尔尼</a></strong>、<strong><a href="../Page/大岛市_(内布拉斯加州).md" title="wikilink">大岛市</a></strong>、<em>' <a href="../Page/林肯_(内布拉斯加州).md" title="wikilink">林肯</a><strong>、</strong><a href="../Page/奥马哈_(内布拉斯加州).md" title="wikilink">奥马哈</a></em>'</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/80号州际公路爱荷华州段.md" title="wikilink">爱荷华州段</a></p></td>
<td></td>
<td><p><strong><a href="../Page/康瑟尔布拉夫斯_(艾奥瓦州).md" title="wikilink">康瑟尔布拉夫斯</a></strong>、<strong><a href="../Page/德梅因.md" title="wikilink">德梅因</a></strong>、<strong><a href="../Page/艾奥瓦城_(艾奥瓦州).md" title="wikilink">艾奥瓦城</a></strong>、<strong><a href="../Page/达文波特_(艾奧瓦州).md" title="wikilink">达文波特</a></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/80号州际公路伊利诺伊州段.md" title="wikilink">伊利诺伊州段</a></p></td>
<td></td>
<td><p><strong><a href="../Page/莫林_(伊利诺伊州).md" title="wikilink">莫林</a>─<a href="../Page/岩石岛_(伊利諾州).md" title="wikilink">岩石岛</a></strong>、<a href="../Page/乔利埃特_(伊利诺州).md" title="wikilink">乔利埃特</a>、<strong><a href="../Page/芝加哥.md" title="wikilink">芝加哥</a></strong>（外环线，由<a href="../Page/I-55.md" title="wikilink">I-55</a>、<a href="../Page/I-57.md" title="wikilink">I-57</a>、<a href="../Page/I-90.md" title="wikilink">I-90</a>、<a href="../Page/I-94.md" title="wikilink">I-94进入市区</a>）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/80号州际公路印第安纳州段.md" title="wikilink">印第安纳州段</a></p></td>
<td></td>
<td><p><strong><a href="../Page/南弯_(印第安纳州).md" title="wikilink">南弯</a></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/80号州际公路俄亥俄州段.md" title="wikilink">俄亥俄州段</a></p></td>
<td></td>
<td><p><strong><a href="../Page/托萊多_(俄亥俄州).md" title="wikilink">托萊多</a></strong>、<strong><a href="../Page/克里夫兰_(俄亥俄州).md" title="wikilink">克里夫兰</a></strong>（外环线，由<a href="../Page/I-90.md" title="wikilink">I-90进入市区</a>）、<strong><a href="../Page/扬斯敦_(俄亥俄州).md" title="wikilink">扬斯敦</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/80号州际公路宾夕法尼亚州段.md" title="wikilink">宾夕法尼亚州段</a></p></td>
<td></td>
<td><p><strong><a href="../Page/夏隆_(宾夕法尼亚州).md" title="wikilink">夏隆</a></strong>、<strong><a href="../Page/克拉里恩_(宾夕法尼亚州).md" title="wikilink">克拉里恩</a></strong>、<strong><a href="../Page/贝勒冯_(宾夕法尼亚州).md" title="wikilink">贝勒冯</a></strong>、<strong><a href="../Page/威廉波特_(宾夕法尼亚州).md" title="wikilink">威廉波特</a></strong>（经<a href="../Page/180号州际公路_(宾夕法尼亚州).md" title="wikilink">I-180</a>）、<strong><a href="../Page/布伦斯堡_(宾夕法尼亚州).md" title="wikilink">布伦斯堡</a></strong>、<strong><a href="../Page/黑泽尔顿_(宾夕法尼亚州).md" title="wikilink">黑泽尔顿</a></strong>、<strong><a href="../Page/史特拉斯堡_(宾夕法尼亚州).md" title="wikilink">史特拉斯堡</a></strong>、<strong><a href="../Page/德拉瓦水峡.md" title="wikilink">德拉瓦水峡</a></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/80号州际公路纽泽西州段.md" title="wikilink">纽泽西州段</a></p></td>
<td></td>
<td><p><em>'<a href="../Page/内特孔_(纽泽西州).md" title="wikilink">内特孔</a> <strong>、</strong><a href="../Page/帕特森_(纽泽西州).md" title="wikilink">帕特森</a><strong>、</strong><a href="../Page/纽约市.md" title="wikilink">纽约市</a></em>'（经<a href="../Page/I-95.md" title="wikilink">I-95</a>）</p></td>
</tr>
<tr class="odd">
<td><p>全长</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参考资料

<div class="references-small">

<references />

</div>

[Category:州际公路系统](../Category/州际公路系统.md "wikilink")
[80号州际公路](../Category/80号州际公路.md "wikilink")

1.
2.