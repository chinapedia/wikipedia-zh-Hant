**88號州際公路**（**Interstate
88**，簡稱**I-88**），又稱或，是[美國](../Page/美國.md "wikilink")[州際公路系統的一部份](../Page/州際公路系統.md "wikilink")，全路段位於[紐約州內](../Page/紐約州.md "wikilink")。西南始於[賓漢頓](../Page/賓漢頓.md "wikilink")（與[81號州際公路相連](../Page/81號州際公路.md "wikilink")），東北止於[斯克內克塔迪](../Page/斯克內克塔迪_\(紐約州\).md "wikilink")（與[90號州際公路相連](../Page/90號州際公路.md "wikilink")），全長189.5公里。與[7號紐約州州道平行](../Page/7號紐約州州道.md "wikilink")。

1968年12月13日被納入系統中，\[1\]，1989年完工。\[2\]
目前由[紐約州運輸部負責維護](../Page/紐約州運輸部.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p><big>途經主要城鎮</big><br />
<small>粗體字為使用在交通標誌上的正式指定定點城市</small></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li><strong><a href="../Page/賓漢頓.md" title="wikilink">賓漢頓</a></strong>（經<a href="../Page/I-81.md" title="wikilink">I-81</a>)</li>
<li><a href="../Page/歐尼安塔_(紐約州).md" title="wikilink">歐尼安塔</a></li>
<li><a href="../Page/斯克內克塔迪_(紐約州).md" title="wikilink">斯克內克塔迪</a></li>
<li><strong><a href="../Page/奧爾巴尼.md" title="wikilink">奧爾巴尼</a></strong>（經<a href="../Page/I-90.md" title="wikilink">I-90</a>）</li>
</ul></td>
</tr>
</tbody>
</table>

## 参考文献

<div class="references-small">

<references />

</div>

[Category:紐約州交通](../Category/紐約州交通.md "wikilink")
[Category:州際公路系統](../Category/州際公路系統.md "wikilink")

1.
2.  [賓夕法尼亞州運輸部](../Page/賓夕法尼亞州運輸部.md "wikilink")，[Pennsylvania Official
    Transportation
    Map](ftp://ftp.dot.state.pa.us/public/pdf/BPR_pdf_files/Maps/Statewide/Historic_OTMs/1989fr.pdf)
    ，1989年，2007年10月30日查閱。