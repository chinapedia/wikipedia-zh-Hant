**8start Launcher**是[Microsoft
Windows系統下的一款免費桌面軟件](../Page/Microsoft_Windows.md "wikilink")，主要功能包括簡潔桌面、快速啟動軟件等。

## 系統需求

軟件適用於[Windows 2000](../Page/Windows_2000.md "wikilink")，[Windows Server
2003](../Page/Windows_Server_2003.md "wikilink")，[Windows
XP](../Page/Windows_XP.md "wikilink")，[Windows
Vista](../Page/Windows_Vista.md "wikilink")，[Windows
7](../Page/Windows_7.md "wikilink")。其中[Windows
XP和](../Page/Windows_XP.md "wikilink")[Windows
7已進行全面測試](../Page/Windows_7.md "wikilink")。\[1\]

## 參考資料

## 外部連結

  - [8start Launcher 主頁](http://www.8start.com)

[Category:綠色軟件](../Category/綠色軟件.md "wikilink")
[Category:免費軟件](../Category/免費軟件.md "wikilink")

1.  [8start Launcher Features](http://www.8start.com/features.htm)