[World_in_750_CE.png](https://zh.wikipedia.org/wiki/File:World_in_750_CE.png "fig:World_in_750_CE.png")的世界\]\]

公元[701年](../Page/701年.md "wikilink")[1月1日至](../Page/1月1日.md "wikilink")[800年](../Page/800年.md "wikilink")[12月31日的这一段期间被称为](../Page/12月31日.md "wikilink")**8世纪**。[武则天死后](../Page/武则天.md "wikilink")，李氏再度执掌[大唐帝国](../Page/大唐帝国.md "wikilink")，[唐玄宗时期出现了](../Page/唐玄宗.md "wikilink")[开元盛世](../Page/开元盛世.md "wikilink")。[安史之乱是唐由盛转衰的转折点](../Page/安史之乱.md "wikilink")，决定了8世纪后期军阀割据混战的局面。

本世纪[朝鲜半岛上](../Page/朝鲜半岛.md "wikilink")[新罗王朝居于主导地位](../Page/新罗.md "wikilink")，大多数时间[日本处于](../Page/日本.md "wikilink")[奈良时代](../Page/奈良时代.md "wikilink")。在急剧扩张后，[阿拔斯王朝取代](../Page/阿拔斯王朝.md "wikilink")[倭马亚王朝成为中东地区的统治者](../Page/倭马亚王朝.md "wikilink")，[阿拉伯帝国在本世纪中期版图扩张到顶峰](../Page/阿拉伯帝国.md "wikilink")。北欧的[维京人至迟在本世纪末期开始侵扰欧洲大陆沿岸地区](../Page/维京人.md "wikilink")。

## 重要事件、发展与成就

  - **科学技术**
      - [贾比尔](../Page/贾比尔.md "wikilink")（721年－815年）：波斯煉金術士，被称为「现代化学之父」。首先引用碱、锑等化学术语；并且记载过硝酸、王水、硝酸银、氯化铵、升汞的制法，金属的冶煉方法以及染色方法等；

<!-- end list -->

  - **战争与政治**
      - [馬雅古文明開始衰弱](../Page/馬雅.md "wikilink")。
      - 732年[鐵鎚查理在](../Page/鐵鎚查理.md "wikilink")[伊比利亞半島抗擊](../Page/伊比利亞半島.md "wikilink")[阿拉伯人的入侵](../Page/阿拉伯人.md "wikilink")。
      - 755年[唐朝](../Page/唐朝.md "wikilink")[安史之亂](../Page/安史之亂.md "wikilink")。
      - 772年─804年[查理曼入侵現今的](../Page/查理曼.md "wikilink")[德國西北部](../Page/德國.md "wikilink")，與[撒克遜人作戰三十多年](../Page/撒克遜人.md "wikilink")，最終擊潰他們的叛亂，將[薩克森納入](../Page/薩克森.md "wikilink")[法蘭克帝國和](../Page/法蘭克帝國.md "wikilink")[基督教世界](../Page/基督教.md "wikilink")。
      - 774年[查理曼征伐](../Page/查理曼.md "wikilink")[倫巴底](../Page/倫巴底.md "wikilink")。
  - **公元7世纪的天灾人祸**

<!-- end list -->

  - **文化娱乐**

<!-- end list -->

  - **社會與經濟**

<!-- end list -->

  - **疾病与医学**

<!-- end list -->

  - **环境与自然资源**

<!-- end list -->

  - **宗教與哲學**

## 重要人物

### 世界领导人

  - [非洲](../Page/非洲.md "wikilink")

<!-- end list -->

  - [美洲](../Page/美洲.md "wikilink")

<!-- end list -->

  - [亚洲](../Page/亚洲.md "wikilink")
      - [武则天](../Page/武则天.md "wikilink")
      - [唐玄宗](../Page/唐玄宗.md "wikilink")

<!-- end list -->

  - [欧洲](../Page/欧洲.md "wikilink")
      - [丕平三世](../Page/丕平三世.md "wikilink")
      - [利奥三世](../Page/利奥三世_\(拜占庭\).md "wikilink")
      - [伊琳娜女皇](../Page/伊琳娜女皇.md "wikilink")
      - [查里曼](../Page/查里曼.md "wikilink")
  - [中东](../Page/中东.md "wikilink")
      - [阿布·阿拔斯](../Page/阿布·阿拔斯.md "wikilink")
      - [哈伦·拉希德](../Page/哈伦·拉希德.md "wikilink")

### 科学家

  - [一行](../Page/一行.md "wikilink")

### 军事领袖

  - [安禄山](../Page/安禄山.md "wikilink")
  - [郭子仪](../Page/郭子仪.md "wikilink")
  - [高仙芝](../Page/高仙芝.md "wikilink")

### 艺术家

  - [李白](../Page/李白.md "wikilink")
  - [颜真卿](../Page/颜真卿.md "wikilink")
  - [吴道子](../Page/吴道子.md "wikilink")

## 8世纪年历

[8世纪](../Category/8世纪.md "wikilink")
[Category:1千纪](../Category/1千纪.md "wikilink")
[+08](../Category/世纪.md "wikilink")