**8月28日**是[阳历年的第](../Page/阳历.md "wikilink")240天（[闰年是](../Page/闰年.md "wikilink")241天），离一年的结束还有125天。

## 大事记

### 前3世紀

  - [前210年](../Page/前210年.md "wikilink")：[秦始皇出巡](../Page/秦始皇.md "wikilink")，且死於今日。

### 16世紀

  - [1511年](../Page/1511年.md "wikilink")：[葡萄牙人征服](../Page/葡萄牙.md "wikilink")[馬六甲](../Page/馬六甲.md "wikilink")。
  - [1521年](../Page/1521年.md "wikilink")：[奧斯曼帝國佔領](../Page/奧斯曼帝國.md "wikilink")[塞爾維亞王國的](../Page/塞爾維亞王國.md "wikilink")[貝爾格勒](../Page/貝爾格勒.md "wikilink")。

### 17世紀

  - [1619年](../Page/1619年.md "wikilink")：[斐迪南二世就任](../Page/斐迪南二世_\(神圣罗马帝国\).md "wikilink")[神聖羅馬帝國君主](../Page/神聖羅馬帝國.md "wikilink")。

### 18世紀

  - [1789年](../Page/1789年.md "wikilink")：[英國](../Page/英國.md "wikilink")[天文學家](../Page/天文學.md "wikilink")[威廉·赫歇尔發現新的](../Page/威廉·赫歇尔.md "wikilink")[土星的卫星](../Page/土星的卫星.md "wikilink")，后来将其命名为[土卫二](../Page/土卫二.md "wikilink")。

### 19世紀

  - [1833年](../Page/1833年.md "wikilink")：[英国议会禁止在整个大英帝国内实行](../Page/英国议会.md "wikilink")[奴隶制](../Page/奴隶制.md "wikilink")。
  - [1845年](../Page/1845年.md "wikilink")：[美国科普](../Page/美国.md "wikilink")[杂志](../Page/杂志.md "wikilink")《[科学美国人](../Page/科学美国人.md "wikilink")》在[纽约创刊](../Page/纽约.md "wikilink")。
  - [1850年](../Page/1850年.md "wikilink")：[德国作曲家](../Page/德国.md "wikilink")[瓦格纳创作的](../Page/理查德·瓦格纳.md "wikilink")[歌剧](../Page/歌剧.md "wikilink")《[罗恩格林](../Page/罗恩格林.md "wikilink")》在[匈牙利作曲家](../Page/匈牙利.md "wikilink")[李斯特的指挥下](../Page/弗兰兹·李斯特.md "wikilink")，于[魏玛首演](../Page/魏玛.md "wikilink")。
  - [1867年](../Page/1867年.md "wikilink")：[美國佔領](../Page/美國.md "wikilink")[中途島](../Page/中途島.md "wikilink")。
  - [1879年](../Page/1879年.md "wikilink")：在[祖鲁战争中战败逃亡的](../Page/祖鲁战争.md "wikilink")[祖鲁国王](../Page/祖鲁.md "wikilink")[塞奇瓦约被](../Page/塞奇瓦约·卡姆潘德.md "wikilink")[英国军队俘获](../Page/英国.md "wikilink")。
  - [1885年](../Page/1885年.md "wikilink")：[羅馬教廷从江西北境代牧区分设江西东境代牧区](../Page/羅馬教廷.md "wikilink")，此为[天主教余江教区的前身](../Page/天主教余江教区.md "wikilink")。
  - [1895年](../Page/1895年.md "wikilink")：日軍包圍彰化八卦山，發生了日本接收台灣過程中戰況最為慘烈的[八卦山戰役](../Page/八卦山戰役.md "wikilink")，反抗軍領袖[吳湯興戰死](../Page/吳湯興.md "wikilink")。
  - [1900年](../Page/1900年.md "wikilink")：[庚子事变](../Page/庚子事变.md "wikilink")：[沙俄軍隊攻陷](../Page/沙俄.md "wikilink")[齐齐哈尔](../Page/齐齐哈尔.md "wikilink")，黑龙江将军[袁寿山自杀](../Page/袁寿山.md "wikilink")。

### 20世紀

  - [1909年](../Page/1909年.md "wikilink")：[云南陆军讲武堂成立](../Page/云南陆军讲武堂.md "wikilink")。
  - [1910年](../Page/1910年.md "wikilink")：[德蘭修女在這一天於家鄉](../Page/真福加爾各答的德肋撒.md "wikilink")[馬其頓](../Page/馬其頓.md "wikilink")[受浸](../Page/受浸.md "wikilink")。
  - [1914年](../Page/1914年.md "wikilink")：[第一次世界大戰](../Page/第一次世界大戰.md "wikilink")：[德軍佔領](../Page/德軍.md "wikilink")[比利時的](../Page/比利時.md "wikilink")[那慕爾](../Page/那慕爾.md "wikilink")。
  - [1916年](../Page/1916年.md "wikilink")：[第一次世界大戰](../Page/第一次世界大戰.md "wikilink")：[德國向](../Page/德國.md "wikilink")[羅馬尼亞宣戰](../Page/羅馬尼亞.md "wikilink")。
  - 1916年：[第一次世界大戰](../Page/第一次世界大戰.md "wikilink")：[意大利向](../Page/意大利.md "wikilink")[德國宣戰](../Page/德國.md "wikilink")。
  - [1928年](../Page/1928年.md "wikilink")：[凯洛格—白里安公约在](../Page/凯洛格—白里安公约.md "wikilink")[巴黎签定](../Page/巴黎.md "wikilink")，禁止主动性战争。
  - [1931年](../Page/1931年.md "wikilink")：[法國與](../Page/法國.md "wikilink")[蘇聯簽署互不侵犯協議](../Page/蘇聯.md "wikilink")。
  - [1937年](../Page/1937年.md "wikilink")：[日本](../Page/日本.md "wikilink")[豐田汽車公司成立](../Page/豐田汽車公司.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：[中国共产党的代表](../Page/中国共产党.md "wikilink")[毛泽东](../Page/毛泽东.md "wikilink")、[周恩来](../Page/周恩来.md "wikilink")、[王若飞从](../Page/王若飞.md "wikilink")[延安飞抵](../Page/延安.md "wikilink")[重庆](../Page/重庆.md "wikilink")，准备与[国民政府展开](../Page/国民政府.md "wikilink")[重庆谈判](../Page/重庆谈判.md "wikilink")。
  - [1946年](../Page/1946年.md "wikilink")：中国共产党政权平息了[八二八暴动](../Page/八二八暴动.md "wikilink")，打破国民政府收复[哈尔滨的企图](../Page/哈尔滨.md "wikilink")。
  - [1952年](../Page/1952年.md "wikilink")：[日本](../Page/日本.md "wikilink")[眾議院解散](../Page/眾議院.md "wikilink")。
  - [1953年](../Page/1953年.md "wikilink")：[日本首間電視台](../Page/日本.md "wikilink")[日本電視台首次播放電視節目](../Page/日本電視台.md "wikilink")。
  - [1955年](../Page/1955年.md "wikilink")：[香港](../Page/香港.md "wikilink")[大埔](../Page/大埔_\(香港\).md "wikilink")[松仔園一群師生於旅行途中遇山洪暴發](../Page/松仔園.md "wikilink")，部份人被沖走，總共造成28人死亡。
  - [1963年](../Page/1963年.md "wikilink")：[美國黑人民權運動领袖](../Page/美國黑人民權運動.md "wikilink")[-{zh:马丁·路德·金;zh-hans:马丁·路德·金;zh-hk:馬丁·路德·金;zh-tw:馬丁·路德·金恩;}-在](../Page/马丁·路德·金.md "wikilink")[华盛顿](../Page/华盛顿哥伦比亚特区.md "wikilink")[林肯纪念堂发表](../Page/林肯纪念堂.md "wikilink")《[我有一个梦](../Page/我有一个梦.md "wikilink")》的演讲。
  - [1971年](../Page/1971年.md "wikilink")：[台湾掀起](../Page/台湾.md "wikilink")“[保钓运动](../Page/保钓运动.md "wikilink")”。
  - [1972年](../Page/1972年.md "wikilink")：[联合国殖民主义问题委员会承认](../Page/联合国殖民主义问题委员会.md "wikilink")[波多黎各实行](../Page/波多黎各.md "wikilink")“自决和独立权”。
  - [1973年](../Page/1973年.md "wikilink")：。
  - [1988年](../Page/1988年.md "wikilink")：[拉姆斯泰因航展事故](../Page/拉姆斯泰因航展事故.md "wikilink")：[西德](../Page/西德.md "wikilink")[空軍飛行表演时發生嚴重事故](../Page/空軍.md "wikilink")，三架飞机相撞后坠落，死亡75人。
  - [1992年](../Page/1992年.md "wikilink")：[中国首次拍卖破产合资企业](../Page/中国.md "wikilink")[天津辛普森家用电器有限公司](../Page/天津辛普森家用电器有限公司.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：[王军霞获](../Page/王军霞.md "wikilink")[第4届世界田径锦标赛女子一萬米金牌](../Page/世界田徑錦標賽.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：[王鼎昌當選新加坡首任民選總統](../Page/王鼎昌.md "wikilink")。
  - [1995年](../Page/1995年.md "wikilink")：[马拉多纳药检第](../Page/马拉多纳.md "wikilink")3次出现问题。
  - [1996年](../Page/1996年.md "wikilink")：[英國](../Page/英國.md "wikilink")[王儲](../Page/王儲.md "wikilink")[查爾斯和](../Page/威爾斯親王查爾斯.md "wikilink")[王妃](../Page/王妃.md "wikilink")[黛安娜正式離婚](../Page/威爾斯王妃戴安娜.md "wikilink")。
  - [1999年](../Page/1999年.md "wikilink")：俄罗斯停用[和平号空间站](../Page/和平号空间站.md "wikilink")，结束载人飞行。

### 21世紀

  - [2001年](../Page/2001年.md "wikilink")：反种族主义大会非政府组织论坛在[南非举行](../Page/南非.md "wikilink")。
  - [2003年](../Page/2003年.md "wikilink")：英国发生。

## 出生

  - [1025年](../Page/1025年.md "wikilink")：[後冷泉天皇](../Page/後冷泉天皇.md "wikilink")，[日本第](../Page/日本.md "wikilink")70代[天皇](../Page/日本天皇.md "wikilink")（[1068年逝世](../Page/1068年.md "wikilink")）
  - [1582年](../Page/1582年.md "wikilink")（[儒略曆](../Page/儒略曆.md "wikilink")）：朱常洛，[明光宗](../Page/明光宗.md "wikilink")（泰昌帝，[1620年逝世](../Page/1620年.md "wikilink")）
  - [1592年](../Page/1592年.md "wikilink")：[乔治·维利尔斯](../Page/乔治·维利尔斯，第一代白金汉公爵.md "wikilink")，英格蘭政治家，第一世[白金漢公爵](../Page/白金漢公爵.md "wikilink")（[1628年逝世](../Page/1628年.md "wikilink")）
  - [1749年](../Page/1749年.md "wikilink")：[约翰·沃尔夫冈·冯·歌德](../Page/约翰·沃尔夫冈·冯·歌德.md "wikilink")，[德国诗人](../Page/德国.md "wikilink")（[1832年逝世](../Page/1832年.md "wikilink")）
  - [1801年](../Page/1801年.md "wikilink")：[安托万·奥古斯丁·库尔诺](../Page/安托万·奥古斯丁·库尔诺.md "wikilink")，[法國數學家及經濟學家](../Page/法國.md "wikilink")，提出[古諾模型](../Page/古諾模型.md "wikilink")（[1877年逝世](../Page/1877年.md "wikilink")）
  - [1814年](../Page/1814年.md "wikilink")：[喬瑟夫·雪利登·拉·芬努](../Page/喬瑟夫·雪利登·拉·芬努.md "wikilink")，[愛爾蘭恐怖小說作家](../Page/愛爾蘭.md "wikilink")（[1873年逝世](../Page/1873年.md "wikilink")）
  - [1814年](../Page/1814年.md "wikilink")：[翁贝托·焦尔达诺](../Page/翁贝托·焦尔达诺.md "wikilink")，[義大利](../Page/義大利.md "wikilink")[歌劇作曲家](../Page/歌劇.md "wikilink")（[1948年逝世](../Page/1948年.md "wikilink")）
  - [1828年](../Page/1828年.md "wikilink")（[儒略曆](../Page/儒略曆.md "wikilink")）：[列夫·托尔斯泰](../Page/列夫·托尔斯泰.md "wikilink")，[俄国作家](../Page/俄国.md "wikilink")（[1910年逝世](../Page/1910年.md "wikilink")）
  - [1878年](../Page/1878年.md "wikilink")：[喬治·惠普爾](../Page/喬治·惠普爾.md "wikilink")，[美国病理学家](../Page/美国.md "wikilink")，[1934年](../Page/1934年.md "wikilink")[诺贝尔生理学或医学奖](../Page/诺贝尔生理学或医学奖.md "wikilink")（[1976年逝世](../Page/1976年.md "wikilink")）
  - [1894年](../Page/1894年.md "wikilink")：[卡爾·貝姆](../Page/卡爾·貝姆.md "wikilink"),
    [奧地利指揮家](../Page/奧地利.md "wikilink")，被認為是德奧樂派管弦樂和歌劇最權威詮釋者（[1981年逝世](../Page/1981年.md "wikilink")）
  - [1902年](../Page/1902年.md "wikilink")：[周培源](../Page/周培源.md "wikilink")，[中國理論物理學家](../Page/中國.md "wikilink")、流體力學家（[1993年逝世](../Page/1993年.md "wikilink")）
  - [1908年](../Page/1908年.md "wikilink")：[罗杰·托瑞·彼得森](../Page/罗杰·托瑞·彼得森.md "wikilink")，[美國博物學家](../Page/美國.md "wikilink")、鳥類學家、藝術家和教育家，被認為是20世紀環保運動的奠基者之一（[1996年逝世](../Page/1996年.md "wikilink")）
  - [1910年](../Page/1910年.md "wikilink")：[特亚林·科普曼斯](../Page/特亚林·科普曼斯.md "wikilink")，原[荷蘭經濟學家](../Page/荷蘭.md "wikilink")，1946年入籍[美國](../Page/美國.md "wikilink")，1975獲諾貝爾經濟學獎（[1985年逝世](../Page/1985年.md "wikilink")）
  - [1915年](../Page/1915年.md "wikilink")：[塔莎·杜朵](../Page/塔莎·杜朵.md "wikilink")，[美國插畫家和兒童文學作家](../Page/美國.md "wikilink")（[2008年逝世](../Page/2008年.md "wikilink")）
  - [1916年](../Page/1916年.md "wikilink")：[杰克·万斯](../Page/杰克·万斯.md "wikilink")，美國作家，曾先後獲得雨果獎、星雲獎、科幻協會大師獎……等（[2013年逝世](../Page/2013年.md "wikilink")）
  - [1917年](../Page/1917年.md "wikilink")：[杰克·科比](../Page/杰克·科比.md "wikilink")，美國漫畫家、編輯和編劇。被稱為「美國漫畫藝術大師」（[1994年逝世](../Page/1994年.md "wikilink")）
  - [1919年](../Page/1919年.md "wikilink")：[高弗雷·豪斯费尔德](../Page/高弗雷·豪斯费尔德.md "wikilink")，[英國電機工程師](../Page/英國.md "wikilink")，獲1979年諾貝爾生理學或醫學獎（[2004年逝世](../Page/2004年.md "wikilink")）
  - [1929年](../Page/1929年.md "wikilink")：[伊斯特凡·克尔特斯](../Page/伊斯特凡·克尔特斯.md "wikilink")，[匈牙利指揮家](../Page/匈牙利.md "wikilink")（[1973年逝世](../Page/1973年.md "wikilink")）
  - [1938年](../Page/1938年.md "wikilink")：[保罗·马丁](../Page/保罗·马丁.md "wikilink")，[加拿大第](../Page/加拿大.md "wikilink")26任總理
  - [1943年](../Page/1943年.md "wikilink")：[大卫·索尔](../Page/大卫·索尔.md "wikilink")，美國演員、歌手
  - [1943年](../Page/1943年.md "wikilink")：[素拉育·朱拉暖](../Page/素拉育·朱拉暖.md "wikilink")，[泰國第](../Page/泰國.md "wikilink")32任首相
  - [1947年](../Page/1947年.md "wikilink")：[汪明荃](../Page/汪明荃.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [1950年](../Page/1950年.md "wikilink")：[湯家驊](../Page/湯家驊.md "wikilink")，[香港政界人物](../Page/香港.md "wikilink")
  - [1950年](../Page/1950年.md "wikilink")：[羅恩·古德瑞](../Page/羅恩·古德瑞.md "wikilink")，美國棒球選手、教練
  - [1957年](../Page/1957年.md "wikilink")：[維克托·鮑里索維奇·赫裡斯堅科](../Page/維克托·鮑里索維奇·赫裡斯堅科.md "wikilink")，[俄羅斯政治家](../Page/俄羅斯.md "wikilink")
  - [1957年](../Page/1957年.md "wikilink")：[伊沃·约西波维奇](../Page/伊沃·约西波维奇.md "wikilink")，[克羅埃西亞政治家](../Page/克羅埃西亞.md "wikilink")、法學家、作曲家、大學教授，第3任[克羅埃西亞總統](../Page/克罗地亚总统.md "wikilink")
  - [1958年](../Page/1958年.md "wikilink")：[斯科特·汉密尔顿](../Page/斯科特·汉密尔顿.md "wikilink")，美國花式溜冰運動員、體育評論員
  - [1959年](../Page/1959年.md "wikilink")：[美樹本晴彥](../Page/美樹本晴彥.md "wikilink")，[日本插畫家](../Page/日本.md "wikilink")、[人物設計師](../Page/人物設計.md "wikilink")
  - [1960年](../Page/1960年.md "wikilink")：[曾偉忠](../Page/曾偉忠.md "wikilink")，[香港主持人](../Page/香港.md "wikilink")、[香港足球代表隊主教練](../Page/香港足球代表隊.md "wikilink")
  - [1961年](../Page/1961年.md "wikilink")：[詹妮佛·库里奇](../Page/詹妮佛·库里奇.md "wikilink")，美國女演員
  - [1962年](../Page/1962年.md "wikilink")：[顧紀筠](../Page/顧紀筠.md "wikilink")，前[香港節目主持人](../Page/香港.md "wikilink")
  - [1962年](../Page/1962年.md "wikilink")：[吳麗珠](../Page/吳麗珠.md "wikilink")，前香港女藝人
  - [1962年](../Page/1962年.md "wikilink")：[大卫·芬奇](../Page/大卫·芬奇.md "wikilink")，美國电影導演
  - [1965年](../Page/1965年.md "wikilink")：[田尻智](../Page/田尻智.md "wikilink")，日本[電子遊戲製作人](../Page/電子遊戲.md "wikilink")
  - [1965年](../Page/1965年.md "wikilink")：[仙妮亞·唐恩](../Page/仙妮亞·唐恩.md "wikilink")，[加拿大女歌手](../Page/加拿大.md "wikilink")、作曲家
  - [1966年](../Page/1966年.md "wikilink")：[高橋洋子](../Page/高橋洋子.md "wikilink")，日本歌手
  - [1967年](../Page/1967年.md "wikilink")：[遠藤正明](../Page/遠藤正明.md "wikilink")，日本歌手
  - [1968年](../Page/1968年.md "wikilink")：[-{zh-cn:比利·博伊德;zh-hk:比利·包依德;zh-tw:比利·包依德}-](../Page/比利·包依德.md "wikilink")，[蘇格蘭演員](../Page/蘇格蘭.md "wikilink")
  - [1969年](../Page/1969年.md "wikilink")：[傑克·布萊克](../Page/傑克·布萊克.md "wikilink")，[美國演員](../Page/美國.md "wikilink")
  - [1974年](../Page/1974年.md "wikilink")：[水橋香織](../Page/水橋香織.md "wikilink")，[日本動畫](../Page/日本動畫.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1974年](../Page/1974年.md "wikilink")：[卡斯滕·扬克尔](../Page/卡斯滕·扬克尔.md "wikilink")，[德國足球運動員](../Page/德國.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[吳佩賢](../Page/吳佩賢.md "wikilink")，香港電台[DJ](../Page/DJ.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[王仁甫](../Page/王仁甫.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")
  - 1980年：[塔蒂安娜](../Page/塔蒂安娜_\(希臘王妃\).md "wikilink")，[希臘皇室成員](../Page/希臘.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[結城愛良](../Page/結城愛良.md "wikilink")，日本歌手
  - [1982年](../Page/1982年.md "wikilink")：[王敬之](../Page/王敬之.md "wikilink")，[中國男子](../Page/中國.md "wikilink")[擊劍運動員](../Page/擊劍.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[黎安·萊姆絲](../Page/黎安·萊姆絲.md "wikilink")，美國歌手
  - [1982年](../Page/1982年.md "wikilink")：[蒂亞戈·莫塔](../Page/蒂亞戈·莫塔.md "wikilink")，[義大利足球運動員](../Page/義大利.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[羅仲謙](../Page/羅仲謙.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[傑夫·格林](../Page/傑夫·格林.md "wikilink")，[美國職業](../Page/美國.md "wikilink")[籃球運動員](../Page/籃球.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[吉拉德·沙利特](../Page/吉拉德·沙利特.md "wikilink")，[以色列士兵](../Page/以色列.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[芙蘿倫絲·威爾希](../Page/芙蘿倫絲·威爾希.md "wikilink")，英国乐队[芙蘿倫絲機進份子主唱](../Page/芙蘿倫絲機進份子.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[雷·瓊斯](../Page/雷·瓊斯.md "wikilink")，[英格蘭職業](../Page/英格蘭.md "wikilink")[足球運動員](../Page/足球.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[趙權](../Page/趙權.md "wikilink")，[韓國男子組合](../Page/韓國.md "wikilink")[2AM成員](../Page/2AM.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[卡莎蒂·普](../Page/卡莎蒂·普.md "wikilink")，美國[流行朋克](../Page/流行朋克.md "wikilink")[創作歌手](../Page/創作歌手.md "wikilink")，搖滾樂隊Hey
    Monday主唱
  - [1989年](../Page/1989年.md "wikilink")：[塞萨尔·阿斯皮利奎塔](../Page/塞萨尔·阿斯皮利奎塔.md "wikilink")，[西班牙足球运动员](../Page/西班牙.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[維爾特利·鮑達斯](../Page/維爾特利·鮑達斯.md "wikilink")，[芬蘭](../Page/芬蘭.md "wikilink")[一級方程式賽車車手](../Page/一級方程式賽車.md "wikilink")。
  - [1990年](../Page/1990年.md "wikilink")：[珍·雷達爾](../Page/珍·雷達爾.md "wikilink")，[美國模特兒](../Page/美國.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[博揚·柯爾基奇](../Page/博揚·柯爾基奇.md "wikilink")，[西班牙足球運動員](../Page/西班牙.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[安德烈·佩伊奇](../Page/安德烈·佩伊奇.md "wikilink")，[澳洲模特](../Page/澳洲.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[李源一](../Page/李源一.md "wikilink")，[中国足球运动员](../Page/中国.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[Hui](../Page/李會澤.md "wikilink")，[韓國男子團體](../Page/韓國.md "wikilink")[Pentagon成員](../Page/Pentagon_\(男子組合\).md "wikilink")
  - [1996年](../Page/1996年.md "wikilink")：[世正](../Page/金世正.md "wikilink")，[韓國女子團體](../Page/韓國.md "wikilink")[I.O.I](../Page/I.O.I.md "wikilink")、[gugudan成員](../Page/gugudan.md "wikilink")
  - [1999年](../Page/1999年.md "wikilink")：[尼古拉·威廉·亚历山大·弗雷德里克](../Page/尼古拉王子_\(丹麦\).md "wikilink")，丹麥王子

## 逝世

  - [430年](../Page/430年.md "wikilink")：[希波的奥古斯丁](../Page/希波的奥古斯丁.md "wikilink")，[罗马帝国基督教思想家](../Page/罗马帝国.md "wikilink")。（生于约[354年](../Page/354年.md "wikilink")）
  - [476年](../Page/476年.md "wikilink")：[欧瑞斯特](../Page/欧瑞斯特.md "wikilink")，羅馬將軍和政治家。
  - [683年](../Page/683年.md "wikilink")：[巴加爾二世](../Page/巴加爾二世.md "wikilink")，[瑪雅文明古典時期城邦](../Page/瑪雅文明.md "wikilink")[帕倫克國王](../Page/帕倫克.md "wikilink")。（生于[603年](../Page/603年.md "wikilink")）
  - [1055年](../Page/1055年.md "wikilink")：[辽兴宗耶律宗真](../Page/辽兴宗.md "wikilink")，[契丹皇帝](../Page/契丹.md "wikilink")。（生于[1016年](../Page/1016年.md "wikilink")）
  - [1645年](../Page/1645年.md "wikilink")：[格老秀斯](../Page/格老秀斯.md "wikilink")，[荷蘭法學家](../Page/荷蘭.md "wikilink")，[國際法的創造者](../Page/國際法.md "wikilink")。（生於[1583年](../Page/1583年.md "wikilink")）
  - [1822年](../Page/1822年.md "wikilink")：[明亮](../Page/明亮_\(清朝\).md "wikilink")，清朝政治人物及軍事家。（生于[1736年](../Page/1736年.md "wikilink")）
  - [1895年](../Page/1895年.md "wikilink")：[吳湯興](../Page/吳湯興.md "wikilink")，反日領袖。
  - [1900年](../Page/1900年.md "wikilink")：[袁壽山](../Page/袁壽山.md "wikilink")，[清朝黑龍江將軍](../Page/清朝.md "wikilink")。（生于[1860年](../Page/1860年.md "wikilink")）
  - [1976年](../Page/1976年.md "wikilink")：[林鳳](../Page/林鳳_\(香港藝人\).md "wikilink")，[香港女藝人](../Page/香港.md "wikilink")。（生于[1940年](../Page/1940年.md "wikilink")）
  - [1992年](../Page/1992年.md "wikilink")：[谭其骧](../Page/谭其骧.md "wikilink")，[中国著名](../Page/中国.md "wikilink")[历史学家](../Page/历史学家.md "wikilink")。（生于[1911年](../Page/1911年.md "wikilink")）
  - [2003年](../Page/2003年.md "wikilink")：[李承仙](../Page/李承仙.md "wikilink")，[中国画家](../Page/中国.md "wikilink")，敦煌艺术研究者。[常书鸿的夫人](../Page/常书鸿.md "wikilink")。（生于[1924年](../Page/1924年.md "wikilink")）
  - [2007年](../Page/2007年.md "wikilink")：[佩亞達](../Page/佩亞達.md "wikilink")，[西班牙職業](../Page/西班牙.md "wikilink")[足球運動員](../Page/足球.md "wikilink")、[西維爾足球員](../Page/西維爾足球會.md "wikilink")（生於[1984年](../Page/1984年.md "wikilink")）

## 节假日和习俗

## 參考資料