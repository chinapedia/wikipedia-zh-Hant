**8月9日**是[阳历年的第](../Page/阳历.md "wikilink")221天（[闰年是](../Page/闰年.md "wikilink")222天），离一年的结束还有144天。

## 大事记

### 前1世紀

  - [前48年](../Page/前48年.md "wikilink")：[格涅乌斯·庞贝和](../Page/格涅乌斯·庞贝.md "wikilink")[凯撒之间著名的](../Page/凯撒.md "wikilink")[法薩盧斯戰役开始](../Page/法薩盧斯戰役.md "wikilink")。

### 4世紀

  - [378年](../Page/378年.md "wikilink")：[罗马帝国军队在](../Page/罗马帝国.md "wikilink")[阿德里安堡战役中被反叛的](../Page/阿德里安堡战役.md "wikilink")[西哥特人击败](../Page/西哥特人.md "wikilink")，其东部皇帝[瓦伦斯战死](../Page/瓦伦斯.md "wikilink")。

### 7世紀

  - [681年](../Page/681年.md "wikilink")：[保加利亚建国](../Page/保加利亚.md "wikilink")。

### 12世紀

  - [1173年](../Page/1173年.md "wikilink")：[意大利](../Page/意大利.md "wikilink")[比萨大教堂的钟楼开始建造](../Page/比萨.md "wikilink")，后来成为[比萨斜塔](../Page/比萨斜塔.md "wikilink")。

### 15世紀

  - [1483年](../Page/1483年.md "wikilink")：[西斯廷小堂落成](../Page/西斯廷小堂.md "wikilink")。

### 17世紀

  - [1615年](../Page/1615年.md "wikilink")：。

### 19世紀

  - [1896年](../Page/1896年.md "wikilink")：[清朝维新派在上海创办](../Page/清朝.md "wikilink")《[时务报](../Page/时务报.md "wikilink")》。
  - [1897年](../Page/1897年.md "wikilink")：[国际数学家大会在](../Page/国际数学家大会.md "wikilink")[瑞士](../Page/瑞士.md "wikilink")[苏黎世召开](../Page/苏黎世.md "wikilink")。

### 20世紀

  - [1930年](../Page/1930年.md "wikilink")：[中国农工民主党的前身](../Page/中国农工民主党.md "wikilink")[中国国民党临时行动委员会在上海成立](../Page/中国国民党临时行动委员会.md "wikilink")。
  - [1937年](../Page/1937年.md "wikilink")：日军上海海军特别陆战队中尉大山勇夫和斋藤要藏两人驾车冲向军用的虹桥机场，被中国机场卫兵击毙。此即“[虹桥机场事件](../Page/虹桥机场事件.md "wikilink")”，被视为淞沪会战的导火线之一。\[1\]
  - [1942年](../Page/1942年.md "wikilink")：[第二次世界大战期间](../Page/第二次世界大战.md "wikilink")，前往支援[瓜达康纳尔岛战役的大日本帝国海军在](../Page/瓜达康纳尔岛战役.md "wikilink")[萨沃岛海战中获得胜利](../Page/萨沃岛海战.md "wikilink")，迫使美国海军舰队从[所罗门群岛撤离](../Page/所罗门群岛.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：[毛澤東發表](../Page/毛澤東.md "wikilink")《對日寇的最後一戰》的聲明。命令[華北](../Page/華北.md "wikilink")、[華中和](../Page/華中.md "wikilink")[華南的共軍收繳敵人武器](../Page/華南.md "wikilink")，接受投降，且在冀熱遼邊抗日區的共軍迅速深入東北。
  - [1945年](../Page/1945年.md "wikilink")：[美国陆军航空队在](../Page/美国陆军航空队.md "wikilink")[日本](../Page/日本.md "wikilink")[长崎投下](../Page/长崎.md "wikilink")[胖子原子彈](../Page/胖子原子彈.md "wikilink")，引发的[剧烈爆炸造成约](../Page/長崎市原子彈爆炸.md "wikilink")7万人死亡。
  - [1955年](../Page/1955年.md "wikilink")：[香港考古人員在](../Page/香港.md "wikilink")[深水埗發現](../Page/深水埗.md "wikilink")[東漢](../Page/東漢.md "wikilink")[李鄭屋古墓](../Page/李鄭屋古墓.md "wikilink")。
  - [1965年](../Page/1965年.md "wikilink")：受到[人民行動黨和](../Page/人民行動黨.md "wikilink")[馬來民族統一機構](../Page/馬來民族統一機構.md "wikilink")（[巫统](../Page/巫统.md "wikilink")）衝突加劇影響，[新加坡被逐出](../Page/新加坡.md "wikilink")[馬來西亞联邦而被迫宣布](../Page/馬來西亞联邦.md "wikilink")[獨立](../Page/新加坡历史.md "wikilink")。
  - [1974年](../Page/1974年.md "wikilink")：[美国总统](../Page/美国总统.md "wikilink")[理查德·尼克松因](../Page/理查德·尼克松.md "wikilink")[水门事件正式辞职](../Page/水门事件.md "wikilink")，副总统[杰拉尔德·福特接任总统一职](../Page/杰拉尔德·福特.md "wikilink")。
  - [1978年](../Page/1978年.md "wikilink")：[中国与](../Page/中華人民共和國.md "wikilink")[卡扎菲领导的独裁政权](../Page/穆阿迈尔·卡扎菲.md "wikilink")[大阿拉伯利比亚人民社会主义民众国建交](../Page/大阿拉伯利比亚人民社会主义民众国.md "wikilink")。
  - [1989年](../Page/1989年.md "wikilink")：[香港地鐵](../Page/香港地鐵.md "wikilink")（即今港鐵）[藍田站正式啟用](../Page/藍田站.md "wikilink")，惟同日下午車站月台路軌附近爆水喉，令兩邊路軌浸水，需再安排同年的[10月1日為新通車日](../Page/10月1日.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：[日本自民黨垮台](../Page/日本自民黨.md "wikilink")，結束1955年以來38年執政黨地位，改由[細川護熙組成](../Page/細川護熙.md "wikilink")7黨聯合內閣。
  - [1995年](../Page/1995年.md "wikilink")：[網景首次公開募股獲得巨大成功](../Page/網景.md "wikilink")。原本股票價值為每股14美元，但股價因一個臨時決定倍增至每股28美元。第一天收市，股價升至每股75美元，幾乎是當時創記錄的「首日獲利」。
  - [1996年](../Page/1996年.md "wikilink")：[叶利钦宣誓就职](../Page/鲍里斯·尼古拉耶维奇·叶利钦.md "wikilink")[俄罗斯总统](../Page/俄罗斯.md "wikilink")，开始了他第二个总统任期。
  - [1999年](../Page/1999年.md "wikilink")：中国设立[国家最高科学技术奖](../Page/中华人民共和国国家最高科学技术奖.md "wikilink")。
  - [1999年](../Page/1999年.md "wikilink")：叶利钦任命[普京为政府第一副总理和政府代总理](../Page/弗拉基米尔·弗拉基米罗维奇·普京.md "wikilink")。叶利钦在当天发表的电视讲话中表示，希望普京在下一次总统大选中成为俄罗斯新的国家元首。

### 21世紀

  - [2001年](../Page/2001年.md "wikilink")：[中国人民解放军海军总医院首次成功利用](../Page/中国人民解放军海军总医院.md "wikilink")[机器人进行远程](../Page/机器人.md "wikilink")[脑外科手术](../Page/脑外科.md "wikilink")。\[2\]
  - [2007年](../Page/2007年.md "wikilink")：[2007年－2008年环球金融危机的开始](../Page/2007年－2008年环球金融危机.md "wikilink")。

## 出生

  - [1890年](../Page/1890年.md "wikilink")：[侯德榜](../Page/侯德榜.md "wikilink")，[中国](../Page/中国.md "wikilink")[化工专家](../Page/化工专家.md "wikilink")，[中国](../Page/中国.md "wikilink")[化学工业的奠基人](../Page/化学工业.md "wikilink")（[1974年去世](../Page/1974年.md "wikilink")）
  - [1896年](../Page/1896年.md "wikilink")：[讓·皮亞傑](../Page/讓·皮亞傑.md "wikilink")，著名[兒童心理發展學家](../Page/兒童心理發展學家.md "wikilink")（[1980年去世](../Page/1980年.md "wikilink")）
  - [1899年](../Page/1899年.md "wikilink")：[P·L·卓華斯](../Page/P·L·卓華斯.md "wikilink")，[英國](../Page/英國.md "wikilink")[作家](../Page/作家.md "wikilink")，《[瑪麗·包萍](../Page/瑪麗·包萍.md "wikilink")》作者（[1996年去世](../Page/1996年.md "wikilink")）
  - [1908年](../Page/1908年.md "wikilink")：[周立波](../Page/周立波_\(当代作家\).md "wikilink")，[作家](../Page/作家.md "wikilink")（[1979年去世](../Page/1979年.md "wikilink")）
  - [1917年](../Page/1917年.md "wikilink")：[饒宗頤](../Page/饒宗頤.md "wikilink")，[香港](../Page/香港.md "wikilink")[歷史學家](../Page/歷史學家.md "wikilink")（[2018年去世](../Page/2018年.md "wikilink")）
  - [1927年](../Page/1927年.md "wikilink")：[馬文·閔斯基](../Page/馬文·閔斯基.md "wikilink")，美國[人工智慧領域的](../Page/人工智慧.md "wikilink")[科學家](../Page/科學家.md "wikilink")，[麻省理工學院人工智慧實驗室的創始人之一](../Page/麻省理工學院.md "wikilink")（[2016年去世](../Page/2016年.md "wikilink")）
  - 1927年：[丹尼爾·凱斯](../Page/丹尼爾·凱斯.md "wikilink")，美國作家。（[2014年去世](../Page/2014年.md "wikilink")）
  - [1936年](../Page/1936年.md "wikilink")：[謝賢](../Page/謝賢.md "wikilink")，香港演員
  - [1938年](../Page/1938年.md "wikilink")：[羅德·拉沃](../Page/羅德·拉沃.md "wikilink")，前[澳洲網球選手](../Page/澳洲.md "wikilink")
  - [1940年](../Page/1940年.md "wikilink")：[张振富](../Page/张振富.md "wikilink")，[中国](../Page/中国.md "wikilink")[歌唱家](../Page/歌唱家.md "wikilink")（[2000年去世](../Page/2000年.md "wikilink")）
  - [1951年](../Page/1951年.md "wikilink")：，[日本演員](../Page/日本.md "wikilink")
  - [1959年](../Page/1959年.md "wikilink")：[陳炳安](../Page/陳炳安.md "wikilink")，[香港](../Page/香港.md "wikilink")[足球運動員](../Page/足球運動員.md "wikilink")、[評述員](../Page/評述員.md "wikilink")
  - [1963年](../Page/1963年.md "wikilink")：[惠妮·休斯頓](../Page/惠妮·休斯頓.md "wikilink")，[美國](../Page/美國.md "wikilink")[歌手](../Page/歌手.md "wikilink")（[2012年去世](../Page/2012年.md "wikilink")）
  - [1965年](../Page/1965年.md "wikilink")：[會川昇](../Page/會川昇.md "wikilink")，[日本動畫](../Page/日本動畫.md "wikilink")[腳本家](../Page/腳本家.md "wikilink")
  - [1968年](../Page/1968年.md "wikilink")：[姬蓮·安德森](../Page/姬蓮·安德森.md "wikilink")，[好萊塢](../Page/好萊塢.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [1972年](../Page/1972年.md "wikilink")：[張惠妹](../Page/張惠妹.md "wikilink")，[台灣原住民](../Page/台灣原住民.md "wikilink")（[卑南族](../Page/卑南族.md "wikilink")）女歌手
  - [1974年](../Page/1974年.md "wikilink")：[馮德倫](../Page/馮德倫.md "wikilink")，[香港男](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[伊东杂音](../Page/伊東雜音_\(1999年出道\).md "wikilink")，[日本](../Page/日本.md "wikilink")[插畫家](../Page/插畫家.md "wikilink")、[人物設計師](../Page/人物設計.md "wikilink")
  - [1978年](../Page/1978年.md "wikilink")：[張心宇](../Page/張心宇.md "wikilink")，[壹電視新聞台](../Page/壹電視.md "wikilink")[主播](../Page/主播.md "wikilink")
  - [1978年](../Page/1978年.md "wikilink")：[奧黛莉·朵杜](../Page/奧黛莉·朵杜.md "wikilink")，[法國女](../Page/法國.md "wikilink")[電影演員](../Page/電影演員.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[余采恩](../Page/余采恩.md "wikilink")，[台灣女演員](../Page/台灣.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[李佳薇](../Page/李佳薇_\(乒乓球运动员\).md "wikilink")，[新加坡](../Page/新加坡.md "wikilink")[乒乓球手](../Page/乒乓球.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[陳淑萍](../Page/陳淑萍.md "wikilink")，[台灣女歌手](../Page/台灣.md "wikilink")
  - [1985年](../Page/1985年.md "wikilink")：[宋熙年](../Page/宋熙年.md "wikilink")，香港藝員，[2007年](../Page/2007年.md "wikilink")[國際中華小姐](../Page/國際中華小姐.md "wikilink")[冠軍](../Page/冠軍.md "wikilink")
  - [1985年](../Page/1985年.md "wikilink")：[洪真英](../Page/洪真英.md "wikilink")，[韓國女歌手](../Page/韓國.md "wikilink")
  - [1985年](../Page/1985年.md "wikilink")：[安娜·坎卓克](../Page/安娜·坎卓克.md "wikilink")，[美國女歌手](../Page/美國.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[阿黛莱德·凯恩](../Page/阿黛莱德·凯恩.md "wikilink")，[澳大利亞女演員](../Page/澳大利亞.md "wikilink")
  - [1995年](../Page/1995年.md "wikilink")：[黃旼炫](../Page/黃旼炫.md "wikilink")，[韓國男子組合](../Page/韓國.md "wikilink")[NU'EST成員](../Page/NU'EST.md "wikilink")

## 逝世

  - [117年](../Page/117年.md "wikilink")：[图拉真](../Page/图拉真.md "wikilink")，[罗马帝国](../Page/罗马帝国.md "wikilink")[皇帝](../Page/羅馬皇帝列表.md "wikilink")（[53年出生](../Page/53年.md "wikilink")）
  - [378年](../Page/378年.md "wikilink")：[瓦伦斯](../Page/瓦伦斯.md "wikilink")，罗马帝国皇帝（[328年出生](../Page/328年.md "wikilink")）
  - [803年](../Page/803年.md "wikilink")：[伊琳娜女皇](../Page/伊琳娜女皇.md "wikilink")，[东罗马帝国](../Page/东罗马帝国.md "wikilink")[皇帝](../Page/拜占庭皇帝列表.md "wikilink")（[752年出生](../Page/752年.md "wikilink")）
  - [1601年](../Page/1601年.md "wikilink")：[勇敢者米哈伊](../Page/勇敢者米哈伊.md "wikilink")，[罗马尼亚](../Page/罗马尼亚.md "wikilink")[民族英雄](../Page/民族英雄.md "wikilink")（[1558年出生](../Page/1558年.md "wikilink")）
  - [1899年](../Page/1899年.md "wikilink")：（Великий Князь Георгий
    Александрович
    Романов），[俄羅斯帝國](../Page/俄羅斯帝國.md "wikilink")[沙皇](../Page/沙皇.md "wikilink")[亞歷山大三世之子](../Page/亚历山大三世_\(俄国\).md "wikilink")（[1871年出生](../Page/1871年.md "wikilink")）
  - [1919年](../Page/1919年.md "wikilink")：[恩斯特·海克爾](../Page/恩斯特·海克爾.md "wikilink")，[德國](../Page/德國.md "wikilink")[動物學家和](../Page/動物學.md "wikilink")[哲學家](../Page/哲學.md "wikilink")（[1834年出生](../Page/1834年.md "wikilink")）
  - [1962年](../Page/1962年.md "wikilink")：[赫尔曼·黑塞](../Page/赫尔曼·黑塞.md "wikilink")，德国[作家](../Page/作家.md "wikilink")，[1946年获](../Page/1946年.md "wikilink")[诺贝尔文学奖](../Page/诺贝尔文学奖.md "wikilink")（[1877年出生](../Page/1877年.md "wikilink")）
  - [1975年](../Page/1975年.md "wikilink")：[德米特里·肖斯塔科维奇](../Page/德米特里·肖斯塔科维奇.md "wikilink")，[苏联](../Page/苏联.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")（[1906年出生](../Page/1906年.md "wikilink")）
  - [2004年](../Page/2004年.md "wikilink")：[大衛·拉克辛](../Page/大衛·拉克辛.md "wikilink")，[美国电影导演](../Page/美国电影.md "wikilink")（[1912年出生](../Page/1912年.md "wikilink")）
  - [2008年](../Page/2008年.md "wikilink")：[吳舜文](../Page/吳舜文.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[女](../Page/女.md "wikilink")[實業家](../Page/實業家.md "wikilink")，與其夫[嚴慶齡同為](../Page/嚴慶齡.md "wikilink")[裕隆集團創辦人](../Page/裕隆汽車.md "wikilink")（[1913年出生](../Page/1913年.md "wikilink")）
  - 2008年：[馬哈茂德·達爾維什](../Page/馬哈茂德·達爾維什.md "wikilink")，[巴勒斯坦民族](../Page/巴勒斯坦.md "wikilink")[詩人](../Page/詩人.md "wikilink")（[1941年出生](../Page/1941年.md "wikilink")）
  - [2016年](../Page/2016年.md "wikilink")：[王拓](../Page/王拓.md "wikilink")，原名王紘久，臺灣[鄉土文學作家](../Page/鄉土文學.md "wikilink")、政治人物（[1944年出生](../Page/1944年.md "wikilink")）\[3\]。
  - [2016年](../Page/2016年.md "wikilink")：[杰拉尔德·格罗夫纳，第六代威斯敏斯特公爵](../Page/杰拉尔德·格罗夫纳，第六代威斯敏斯特公爵.md "wikilink")，[英國貴族](../Page/英國.md "wikilink")、億萬富翁和地產商（1951年出生）

## 节假日和习俗

  - ：[集郵節](../Page/集郵節.md "wikilink")

  - ：[8+9節](../Page/8+9節.md "wikilink")

  - [新加坡国庆日](../Page/新加坡国庆日.md "wikilink")

  -
  - 南非妇女节：为纪念[南非女性大游行而设立的节日](../Page/南非女性大游行.md "wikilink")

## 参考资料

1.
2.
3.