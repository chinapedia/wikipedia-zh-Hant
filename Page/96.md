**96**是[95与](../Page/95.md "wikilink")[97之间的](../Page/97.md "wikilink")[自然数](../Page/自然数.md "wikilink")。

## 在数学中

  - [欧拉函数的前](../Page/欧拉函数.md "wikilink")17个自然数的和为96。
  - 96写出来呈180°[轴向对称](../Page/轴向对称.md "wikilink")。
  - 1/96 = 0.010 416 666 666 ...

## 在科学中

  - 96是[锔的](../Page/锔.md "wikilink")[原子序数](../Page/原子序数.md "wikilink")\[1\]。

## 在[天文学中](../Page/天文学.md "wikilink")

  - [梅西耶天體](../Page/梅西耶天體.md "wikilink")[M96是](../Page/M96.md "wikilink")[狮子座中的一个亮度为](../Page/狮子座.md "wikilink")10.5等[星等的](../Page/星等.md "wikilink")[螺旋星系](../Page/螺旋星系.md "wikilink")。
  - [星云和星团新总表中的第](../Page/星云和星团新总表.md "wikilink")96个天体[NGC
    96是](../Page/NGC_96.md "wikilink")[仙女座中的一个螺旋星系](../Page/仙女座.md "wikilink")。
  - [日食的](../Page/日食.md "wikilink")[沙罗周期始于](../Page/沙罗周期.md "wikilink")[94年](../Page/94年.md "wikilink")[7月1日](../Page/7月1日.md "wikilink")，终于[1374年](../Page/1374年.md "wikilink")[8月8日](../Page/8月8日.md "wikilink")，其第96系列的周期为1280.1年，包含72个日食。

## 在其它领域中

  - 公元[前96年](../Page/前96年.md "wikilink")、[96年或](../Page/96年.md "wikilink")[1996年](../Page/1996年.md "wikilink")
  - 运行的兼容计算机的荧光屏的标准分辨率为96
  - [古兰经中第](../Page/古兰经.md "wikilink")96章的名字为《[血块](../Page/血块.md "wikilink")》（）
  - [Candy Crush
    Saga在第](../Page/Candy_Crush_Saga.md "wikilink")96關首次出現[炸彈](../Page/炸彈.md "wikilink")
  - 在[希尔斯堡惨剧中有](../Page/希尔斯堡惨剧.md "wikilink")96人丧生
  - [STS-96是](../Page/STS-96.md "wikilink")[發現號太空梭的第](../Page/發現號太空梭.md "wikilink")26次太空飞行，于[1999年](../Page/1999年.md "wikilink")[5月27日起飞](../Page/5月27日.md "wikilink")

## 参考文献

1.  [Royal Society of Chemistry - Visual Element Periodic
    Table](http://www.rsc.org/periodic-table)