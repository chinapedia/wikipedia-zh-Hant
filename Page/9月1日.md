**9月1日**是[阳历年的第](../Page/阳历.md "wikilink")244天（[闰年是](../Page/闰年.md "wikilink")245天），离一年的结束还有121天。

## 大事记

### 13世紀

  - [1271年](../Page/1271年.md "wikilink")：[格列戈里十世当选为](../Page/格列戈里十世.md "wikilink")[教皇](../Page/教皇.md "wikilink")。

### 15世紀

  - [1449年](../Page/1449年.md "wikilink")：[土木之变](../Page/土木之变.md "wikilink")：[明英宗被](../Page/明英宗.md "wikilink")[王振挟持以五十万大军亲征](../Page/王振.md "wikilink")[蒙古](../Page/蒙古.md "wikilink")[瓦剌部](../Page/瓦剌.md "wikilink")，在土木堡（今[河北](../Page/河北省.md "wikilink")[怀来县西南](../Page/怀来县.md "wikilink")）被瓦刺军包围，英宗被俘。

### 19世紀

  - [1804年](../Page/1804年.md "wikilink")：[德国天文学家](../Page/德国.md "wikilink")[卡尔·哈丁通过](../Page/卡爾·路德維希·哈丁.md "wikilink")[望远镜在](../Page/望远镜.md "wikilink")[小行星带中发现](../Page/小行星带.md "wikilink")[婚神星](../Page/婚神星.md "wikilink")。
  - [1870年](../Page/1870年.md "wikilink")：[普法戰爭](../Page/普法戰爭.md "wikilink")：[普魯士在](../Page/普魯士.md "wikilink")[色當會戰打敗](../Page/色當會戰.md "wikilink")[法國](../Page/法蘭西第二帝國.md "wikilink")。

### 20世紀

  - [1902年](../Page/1902年.md "wikilink")：第一齣[科幻電影](../Page/科幻電影.md "wikilink")《[月球旅行記](../Page/月球旅行記.md "wikilink")》在法國上映。
  - [1914年](../Page/1914年.md "wikilink")：[旅鸽灭绝](../Page/旅鸽.md "wikilink")。
  - [1917年](../Page/1917年.md "wikilink")：[護法之役](../Page/護法之役.md "wikilink")：[孙中山被非常國會选为軍政府大元帅](../Page/孙中山.md "wikilink")，号召北伐。
  - [1918年](../Page/1918年.md "wikilink")：[徐世昌当选](../Page/徐世昌.md "wikilink")[中華民國總統](../Page/中華民國總統.md "wikilink")。
  - [1919年](../Page/1919年.md "wikilink")：[俄国](../Page/俄国.md "wikilink")[圣彼得堡更名彼得格勒](../Page/圣彼得堡.md "wikilink")。
  - [1923年](../Page/1923年.md "wikilink")：[日本](../Page/日本.md "wikilink")[關東平原发生](../Page/關東平原.md "wikilink")[里氏](../Page/里氏.md "wikilink")7.8级的[强烈地震](../Page/關東大地震.md "wikilink")，約14萬人死亡。
  - [1939年](../Page/1939年.md "wikilink")：[第二次世界大战](../Page/第二次世界大战.md "wikilink")：[納粹德國军队发动](../Page/納粹德國.md "wikilink")[波蘭戰役入侵](../Page/波蘭戰役.md "wikilink")[波兰](../Page/波兰.md "wikilink")，[第二次世界大战在](../Page/第二次世界大战.md "wikilink")[欧洲爆发](../Page/欧洲.md "wikilink")。
  - [1939年](../Page/1939年.md "wikilink")：[馬歇爾成為](../Page/馬歇爾.md "wikilink")[美國陸軍參謀長](../Page/美國陸軍參謀長.md "wikilink")。
  - [1951年](../Page/1951年.md "wikilink")：[美國與](../Page/美國.md "wikilink")[澳洲](../Page/澳洲.md "wikilink")、[紐西蘭簽署](../Page/紐西蘭.md "wikilink")[太平洋安全保障條約](../Page/太平洋安全保障條約.md "wikilink")。
  - [1953年](../Page/1953年.md "wikilink")：[中国人民解放军军事工程学院成立于](../Page/中国人民解放军军事工程学院.md "wikilink")[哈尔滨](../Page/哈尔滨.md "wikilink")。
  - [1955年](../Page/1955年.md "wikilink")：[武汉长江大桥开工建设](../Page/武汉长江大桥.md "wikilink")。
  - [1958年](../Page/1958年.md "wikilink")：[鱈魚戰爭](../Page/鱈魚戰爭.md "wikilink")：[冰島立法擴展捕魚區](../Page/冰島.md "wikilink")，[英國動員海軍前往冰島水域](../Page/英國.md "wikilink")。
  - [1961年](../Page/1961年.md "wikilink")：[厄立特里亞軍隊向](../Page/厄立特里亞.md "wikilink")[埃塞俄比亞軍隊開火](../Page/埃塞俄比亞.md "wikilink")，為期30年的[厄立特里亞獨立戰爭爆發](../Page/厄立特里亞獨立戰爭.md "wikilink")。
  - [1962年](../Page/1962年.md "wikilink")：[颱風-{zh-tw:萬達; zh-hk:溫黛;
    zh-cn:温黛;}-吹襲](../Page/颱風溫黛_\(1962年\).md "wikilink")[香港](../Page/香港.md "wikilink")，導致130人死亡，53人受傷及36隻遇事越洋[船舶](../Page/船舶.md "wikilink")，造成嚴重破壞。
  - [1965年](../Page/1965年.md "wikilink")：[西藏自治区正式成立](../Page/西藏自治区.md "wikilink")。
  - [1968年](../Page/1968年.md "wikilink")：[九年國民教育指](../Page/九年國民教育.md "wikilink")[中華民國義務教育由六年延長為九年的教育措施](../Page/中華民國.md "wikilink")。
  - [1968年](../Page/1968年.md "wikilink")：[张荣发创立](../Page/张荣发.md "wikilink")[长荣海运公司](../Page/长荣海运.md "wikilink")。
  - [1969年](../Page/1969年.md "wikilink")：[利比亞國王](../Page/利比亞王國.md "wikilink")[伊德里斯一世在政變中被推翻](../Page/伊德里斯一世_\(利比亞\).md "wikilink")，[-{zh-cn:卡扎菲;
    zh-tw:格達費; zh-hk:卡達菲;
    zh-sg:格达费;}-開始掌權](../Page/卡達菲.md "wikilink")[利比亞長達](../Page/利比亞.md "wikilink")42年之久。
  - [1971年](../Page/1971年.md "wikilink")：[卡塔尔国成立](../Page/卡塔尔.md "wikilink")。
  - [1982年](../Page/1982年.md "wikilink")：[美國空軍太空司令部成立](../Page/美國空軍太空司令部.md "wikilink")。
  - [1983年](../Page/1983年.md "wikilink")：[冷戰](../Page/冷戰.md "wikilink")：[大韓航空007號班機在](../Page/大韓航空007號班機.md "wikilink")[库页岛附近被](../Page/库页岛.md "wikilink")[苏联空军击落](../Page/苏联.md "wikilink")，269人罹難，包括一名[美國](../Page/美國.md "wikilink")[聯邦眾議員](../Page/聯邦眾議員.md "wikilink")。
  - [1985年](../Page/1985年.md "wikilink")：[美國和](../Page/美國.md "wikilink")[法國聯合搜索隊發現](../Page/法國.md "wikilink")[鐵達尼號殘骸](../Page/鐵達尼號.md "wikilink")。
  - [1991年](../Page/1991年.md "wikilink")：[蘇聯解體](../Page/蘇聯解體.md "wikilink")：[烏茲別克宣佈脫離](../Page/烏茲別克.md "wikilink")[蘇聯獨立](../Page/蘇聯.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：俄羅斯總統[葉利欽解除副總統](../Page/葉利欽.md "wikilink")[魯茨科伊和第一副總理舒梅科的職務](../Page/魯茨科伊.md "wikilink")。
  - 1993年：香港[城巴有限公司正式接管](../Page/城巴有限公司.md "wikilink")[中華巴士](../Page/中華巴士.md "wikilink")26條港島區專利巴士路線。
  - [1995年](../Page/1995年.md "wikilink")：[香港的](../Page/香港.md "wikilink")[城巴再接管原由](../Page/城巴.md "wikilink")[中巴營運的另外](../Page/中華巴士.md "wikilink")14條路線。
  - [1998年](../Page/1998年.md "wikilink")：[香港](../Page/香港.md "wikilink")[中華巴士專營權結束](../Page/中華汽車有限公司#專利巴士.md "wikilink")，88條路線交由[新世界第一巴士接辦](../Page/新世界第一巴士.md "wikilink")。

### 21世紀

  - [2003年](../Page/2003年.md "wikilink")：[中華民國護照開始於封面加註](../Page/中華民國護照.md "wikilink")「TAIWAN」字樣。
  - [2004年](../Page/2004年.md "wikilink")：[车臣分离主义武装在](../Page/车臣.md "wikilink")[俄罗斯](../Page/俄罗斯.md "wikilink")[北奥塞梯共和国](../Page/北奥塞梯共和国.md "wikilink")[别斯兰一所中学劫持](../Page/别斯兰.md "wikilink")1200多名人质，[别斯兰人质危机发生](../Page/别斯兰人质危机.md "wikilink")。
  - [2005年](../Page/2005年.md "wikilink")：[德国之声主办的第二届](../Page/德国之声.md "wikilink")[国际博客大赛正式开始](../Page/德國之聲國際博客大賽.md "wikilink")。
  - [2008年](../Page/2008年.md "wikilink")：第91任[日本首相](../Page/日本首相.md "wikilink")[福田康夫因支持率低迷不振](../Page/福田康夫.md "wikilink")，宣布辭職。
  - [2009年](../Page/2009年.md "wikilink")：全球開始發行120 GB輕薄版[PLAYSTATION
    3](../Page/PLAYSTATION_3.md "wikilink")。
  - [2009年](../Page/2009年.md "wikilink")：美国佛蒙特州正式成为全美第四个将[同性婚姻合法化的州份](../Page/同性婚姻.md "wikilink")。
  - [2012年](../Page/2012年.md "wikilink")：台灣民間舉行[反媒體壟斷大遊行](../Page/反媒體壟斷大遊行.md "wikilink")，訴求為反對[旺中集團壟斷新聞媒體](../Page/旺中集團.md "wikilink")。
  - [2014年](../Page/2014年.md "wikilink")：[十二年國民教育將由施行前的](../Page/十二年國民教育.md "wikilink")[九年國教延長至](../Page/九年國教.md "wikilink")12年，且後3年採強迫性入學、免學費、公私立並行及免試為主。

## 出生

  - [948年](../Page/948年.md "wikilink")：[辽景宗耶律贤](../Page/辽景宗.md "wikilink")，[辽朝皇帝](../Page/辽朝.md "wikilink")（[982年去世](../Page/982年.md "wikilink")）
  - [1593年](../Page/1593年.md "wikilink")：[姬蔓·芭奴](../Page/姬蔓·芭奴.md "wikilink")，[莫卧儿帝国皇后](../Page/莫卧儿帝国.md "wikilink")（[1631年去世](../Page/1631年.md "wikilink")）
  - [1653年](../Page/1653年.md "wikilink")：[约翰·帕赫贝尔](../Page/约翰·帕赫贝尔.md "wikilink")，[德國](../Page/德國.md "wikilink")[巴洛克時期作曲家](../Page/巴洛克時期.md "wikilink")、管風琴大師，《[D大調卡農與吉格](../Page/D大調卡農.md "wikilink")》作者（[1706年去世](../Page/1706年.md "wikilink")）
  - [1835年](../Page/1835年.md "wikilink")：[威廉·斯坦利·傑文斯](../Page/威廉·斯坦利·傑文斯.md "wikilink")，[英国著名的经济学家和逻辑学家](../Page/英国.md "wikilink")（[1882年去世](../Page/1882年.md "wikilink")）
  - [1854年](../Page/1854年.md "wikilink")：[恩格尔贝特·洪佩尔丁克](../Page/恩格尔贝特·洪佩尔丁克.md "wikilink")，德國後浪漫主義作曲家（[1921年去世](../Page/1921年.md "wikilink")）
  - [1856年](../Page/1856年.md "wikilink")：[谢尔盖·尼古拉耶维奇·维诺格拉茨基](../Page/谢尔盖·尼古拉耶维奇·维诺格拉茨基.md "wikilink")，俄國微生物學家，土壤微生物學創立者之一（[1953年去世](../Page/1953年.md "wikilink")）
  - [1868年](../Page/1868年.md "wikilink")：[武藤信义](../Page/武藤信义.md "wikilink")，日本陆军元帅（[1933年去世](../Page/1933年.md "wikilink")）
  - [1875年](../Page/1875年.md "wikilink")：[埃德加·赖斯·巴勒斯](../Page/埃德加·赖斯·巴勒斯.md "wikilink")，美國科幻小說家，「泰山」（Tarzan）系列創作者（[1950年去世](../Page/1950年.md "wikilink")）
  - [1877年](../Page/1877年.md "wikilink")：[弗朗西斯·阿斯頓](../Page/弗朗西斯·阿斯頓.md "wikilink")，英國化學家，[諾貝爾化學獎得主](../Page/諾貝爾化學獎.md "wikilink")（[1945年去世](../Page/1945年.md "wikilink")）
  - [1878年](../Page/1878年.md "wikilink")：[約翰·弗雷德里克·查爾斯·富勒](../Page/約翰·弗雷德里克·查爾斯·富勒.md "wikilink")，英國軍事理論家、軍事歷史學家、探照燈發明者（[1966年去世](../Page/1966年.md "wikilink")）
  - [1886年](../Page/1886年.md "wikilink")：[奥特马·舍克](../Page/奥特马·舍克.md "wikilink")，[瑞士作曲家](../Page/瑞士.md "wikilink")、指揮家，擅聲樂寫作（[1957年去世](../Page/1957年.md "wikilink")）
  - [1906年](../Page/1906年.md "wikilink")：[华金·巴拉格尔](../Page/华金·巴拉格尔.md "wikilink")，[多明尼加總統](../Page/多明尼加.md "wikilink")(1960-1962、1966-1978、1986-1996)（[2002年去世](../Page/2002年.md "wikilink")）
  - [1923年](../Page/1923年.md "wikilink")：[洛基·馬西安諾](../Page/洛基·馬西安諾.md "wikilink")，[義大利裔美國職業拳擊手](../Page/義大利.md "wikilink")，拳擊史上唯一生涯全勝的世界重量級拳王（[1969年去世](../Page/1969年.md "wikilink")）
  - [1935年](../Page/1935年.md "wikilink")：[小泽征尔](../Page/小泽征尔.md "wikilink")，[日本](../Page/日本.md "wikilink")[指挥家](../Page/指挥家.md "wikilink")
  - [1935年](../Page/1935年.md "wikilink")：[周亦卿](../Page/周亦卿.md "wikilink")，香港企業家（[2018年去世](../Page/2018年.md "wikilink")）
  - [1939年](../Page/1939年.md "wikilink")：[莉莉·湯姆琳](../Page/莉莉·湯姆琳.md "wikilink")，美國女演員、作家及劇場製作人
  - [1946年](../Page/1946年.md "wikilink")：[盧武鉉](../Page/盧武鉉.md "wikilink")，[韓國第](../Page/韓國.md "wikilink")16任[總統](../Page/韓國總統.md "wikilink")（[2009年去世](../Page/2009年.md "wikilink")）
  - [1946年](../Page/1946年.md "wikilink")：[巴里·吉布](../Page/巴里·吉布.md "wikilink")，英國[比吉斯樂團節奏吉他手](../Page/比吉斯.md "wikilink")、歌手、作曲家與製作人
  - [1948年](../Page/1948年.md "wikilink")：[詹姆斯·瑞布霍恩](../Page/詹姆斯·瑞布霍恩.md "wikilink")，美國演員
  - [1950年](../Page/1950年.md "wikilink")：[黎新祥](../Page/黎新祥.md "wikilink")，[香港足球員及足球教練](../Page/香港.md "wikilink")（2010年去世）
  - [1950年](../Page/1950年.md "wikilink")：[米哈伊爾·葉菲莫維奇·弗拉德科夫](../Page/米哈伊爾·弗拉德科夫.md "wikilink")，[俄羅斯政治家](../Page/俄羅斯.md "wikilink")，[俄羅斯總理](../Page/俄羅斯總理.md "wikilink")
  - [1954年](../Page/1954年.md "wikilink")：[丁守中](../Page/丁守中.md "wikilink")，[台灣政治人物](../Page/台灣.md "wikilink")
  - [1956年](../Page/1956年.md "wikilink")：[張豐毅](../Page/張豐毅.md "wikilink")，[中國內地男演員](../Page/中國.md "wikilink")
  - [1957年](../Page/1957年.md "wikilink")：[格洛丽亚·埃斯特凡](../Page/格洛丽亚·埃斯特凡.md "wikilink")，[古巴裔美國歌手](../Page/古巴.md "wikilink")、演員，曾獲七座[葛萊美獎](../Page/葛萊美獎.md "wikilink")
  - [1961年](../Page/1961年.md "wikilink")：[江蕙](../Page/江蕙.md "wikilink")，台灣女歌手
  - [1962年](../Page/1962年.md "wikilink")：[路德·古利特](../Page/路德·古利特.md "wikilink")，荷蘭足球員及足球領隊
  - [1962年](../Page/1962年.md "wikilink")：[托尼·卡斯卡里诺](../Page/托尼·卡斯卡里诺.md "wikilink")，前英國足球運動員、體育播音員
  - [1966年](../Page/1966年.md "wikilink")：[羅美薇](../Page/羅美薇.md "wikilink")，香港演員
  - [1968年](../Page/1968年.md "wikilink")：[穆罕默德·阿塔](../Page/穆罕默德·阿塔.md "wikilink")，[埃及恐怖分子](../Page/埃及.md "wikilink")，[基地組織成員](../Page/基地組織.md "wikilink")，[九一一事件劫機者領袖](../Page/九一一事件.md "wikilink")（[2001年去世](../Page/2001年.md "wikilink")）
  - [1969年](../Page/1969年.md "wikilink")：[潘儀君](../Page/潘儀君.md "wikilink")，台灣演員
  - [1969年](../Page/1969年.md "wikilink")：[亨宁·伯格](../Page/亨宁·伯格.md "wikilink")，[挪威退役職業足球運動員和現役主教練](../Page/挪威.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink")：[湯屋敦子](../Page/湯屋敦子.md "wikilink")，日本女性[聲優](../Page/聲優.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink")：[黄正民](../Page/黄正民.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")
  - [1974年](../Page/1974年.md "wikilink")：[山本寬](../Page/山本寬.md "wikilink")，[日本動畫演出家](../Page/日本動畫.md "wikilink")
  - [1976年](../Page/1976年.md "wikilink")：[福西崇史](../Page/福西崇史.md "wikilink")，日本足球运动员
  - [1978年](../Page/1978年.md "wikilink")：[中島沙樹](../Page/中島沙樹.md "wikilink")，日本動畫聲優
  - [1984年](../Page/1984年.md "wikilink")：[喬·特洛曼](../Page/喬·特洛曼.md "wikilink")，美國樂手，[打倒男孩樂團成員](../Page/打倒男孩.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[劉盈秀](../Page/劉盈秀.md "wikilink")，台灣電視[主播](../Page/主播.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[王炳忠](../Page/王炳忠.md "wikilink")，台灣政治人物
  - [1989年](../Page/1989年.md "wikilink")：，德國樂手，[東京飯店酷兒樂團主唱](../Page/東京飯店酷兒.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：，德國樂手，東京飯店酷兒樂團吉他手
  - [1992年](../Page/1992年.md "wikilink")：[禹惠林](../Page/惠林.md "wikilink")，韓國女子偶像團體[Wonder
    Girls前成員](../Page/Wonder_Girls.md "wikilink")
  - [1994年](../Page/1994年.md "wikilink")：[小卡洛斯·塞恩斯](../Page/小卡洛斯·塞恩斯.md "wikilink")，[西班牙](../Page/西班牙.md "wikilink")[一級方程式賽車車手](../Page/一級方程式賽車.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink")：[田柾國](../Page/田柾國.md "wikilink")，[韓國男子偶像團體](../Page/韓國.md "wikilink")[防彈少年團成員](../Page/防彈少年團.md "wikilink")
  - [2003年](../Page/2003年.md "wikilink")：[安宥真](../Page/安宥真.md "wikilink")，[韓國女子偶像團體](../Page/韓國.md "wikilink")[IZ\*ONE成員](../Page/IZ*ONE.md "wikilink")

## 逝世

  - [1648年](../Page/1648年.md "wikilink")：[马兰·梅森](../Page/马兰·梅森.md "wikilink")，法国神学家、数学家、音乐理论家（[1588年出生](../Page/1588年.md "wikilink")）
  - [1715年](../Page/1715年.md "wikilink")：[路易十四](../Page/路易十四.md "wikilink")，法国国王（[1638年出生](../Page/1638年.md "wikilink")）
  - [1948年](../Page/1948年.md "wikilink")：[冯玉祥](../Page/冯玉祥.md "wikilink")，國民革命軍將領（[1882年出生](../Page/1882年.md "wikilink")）
  - [1972年](../Page/1972年.md "wikilink")：[何香凝](../Page/何香凝.md "wikilink")，中國革命黨人、國畫家（[1878年出生](../Page/1878年.md "wikilink")）
  - [1988年](../Page/1988年.md "wikilink")：[路易斯·沃爾特·阿爾瓦雷茨](../Page/路易斯·沃爾特·阿爾瓦雷茨.md "wikilink")，西班牙裔[美國物理學家](../Page/美國.md "wikilink")（[1911年出生](../Page/1911年.md "wikilink")）
  - [1989年](../Page/1989年.md "wikilink")：[鍾保羅](../Page/鍾保羅.md "wikilink")，[香港影視藝員](../Page/香港.md "wikilink")（[1959年出生](../Page/1959年.md "wikilink")）
  - [2005年](../Page/2005年.md "wikilink")：[朱向东](../Page/朱向东.md "wikilink")，[中华人民共和国国家统计局副局长](../Page/中华人民共和国国家统计局.md "wikilink")（[1955年出生](../Page/1955年.md "wikilink")）
  - [2006年](../Page/2006年.md "wikilink")：[羅鷹石](../Page/羅鷹石.md "wikilink")，[香港企業家](../Page/香港.md "wikilink")（[1913年出生](../Page/1913年.md "wikilink")）
  - [2009年](../Page/2009年.md "wikilink")：[張真英](../Page/張真英.md "wikilink")，[韓國影視藝員](../Page/韓國.md "wikilink")（[1974年出生](../Page/1974年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[郭東修](../Page/郭東修.md "wikilink")，[台灣知名殯葬業者](../Page/台灣.md "wikilink")（[1962年出生](../Page/1962年.md "wikilink")）
  - 2013年：[策纳伊·帕尔](../Page/策纳伊·帕尔.md "wikilink")，匈牙利前足球运动员及教练（[1932年出生](../Page/1932年.md "wikilink")）

## 节假日和习俗

  - [中華民國](../Page/中華民國.md "wikilink")：[記者節](../Page/記者節.md "wikilink")（始於[1933年](../Page/1933年.md "wikilink")）
  - [兄弟象隊隊慶](../Page/兄弟象隊.md "wikilink")
  - 日本政府在1960年宣布9月1日為一年一度的「防災日」（當天也是關東大地震37週年），提醒人們準備預防地震災害的重要性；在每年的這一天，日本各地學校會默哀地震所造成的傷亡，並進行防災避難演練。
  - [中国大陆](../Page/中国大陆.md "wikilink")、[英國](../Page/英國.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門等的](../Page/澳門.md "wikilink")[中](../Page/中學.md "wikilink")[小學普遍於每年的](../Page/小學.md "wikilink")9月1日作為新學年的[開學日](../Page/開學日.md "wikilink")。如遇上9月1日為假日或是星期六、[星期日](../Page/星期日.md "wikilink")，則可能順延至第一個[平日](../Page/平日.md "wikilink")，最遲為[9月3日](../Page/9月3日.md "wikilink")。