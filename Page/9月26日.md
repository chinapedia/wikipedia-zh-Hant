**9月26日**是[阳历一年中的第](../Page/阳历.md "wikilink")269天（[闰年第](../Page/闰年.md "wikilink")270天），离全年结束还有96天。

## 大事记

### 16世紀

  - [1580年](../Page/1580年.md "wikilink")：[英國探險家](../Page/英國.md "wikilink")[法蘭西斯·德瑞克完成環球航海旅行](../Page/法蘭西斯·德瑞克.md "wikilink")，回到英国[普利茅斯](../Page/普利茅斯.md "wikilink")。

### 17世紀

  - [1679年](../Page/1679年.md "wikilink")：[丹麦和](../Page/丹麦.md "wikilink")[瑞典签订](../Page/瑞典.md "wikilink")[隆德条约](../Page/隆德条约.md "wikilink")。\[1\]
  - [1687年](../Page/1687年.md "wikilink")：[威尼斯共和国的军队炮轰](../Page/威尼斯共和国.md "wikilink")[奥斯曼帝国设在](../Page/奥斯曼帝国.md "wikilink")[雅典](../Page/雅典.md "wikilink")[帕台农神庙的军火库](../Page/帕台农神庙.md "wikilink")，致使神庙被炸毁。

### 18世紀

  - [1754年](../Page/1754年.md "wikilink")：[尹继善第四次出任](../Page/尹继善.md "wikilink")[两江总督](../Page/两江总督.md "wikilink")。
  - [1777年](../Page/1777年.md "wikilink")：在[美国独立战争期间](../Page/美国独立战争.md "wikilink")，[英国军队占领](../Page/英国.md "wikilink")[美国独立运动中心](../Page/美国.md "wikilink")[费城](../Page/费城.md "wikilink")。
  - [1789年](../Page/1789年.md "wikilink")：[托马斯·杰斐逊被任命为美国第一任国务卿](../Page/托马斯·杰斐逊.md "wikilink")，[約翰·傑伊被任命为美国第一任](../Page/約翰·傑伊.md "wikilink")[最高法院](../Page/最高法院.md "wikilink")[首席大法官](../Page/首席大法官.md "wikilink")，[塞繆爾·奧斯古德被任命为美国第一任邮电部长](../Page/塞繆爾·奧斯古德.md "wikilink")，[埃德蒙·倫道夫被任命为第一任](../Page/埃德蒙·倫道夫.md "wikilink")[美國司法部長](../Page/美國司法部長.md "wikilink")。
  - [1792年](../Page/1792年.md "wikilink")：[马戛尔尼代表英国出使清朝](../Page/乔治·马戛尔尼.md "wikilink")。

### 19世紀

  - [1815年](../Page/1815年.md "wikilink")：俄、普、奥三国在[巴黎发表缔结](../Page/巴黎.md "wikilink")“[神圣同盟](../Page/神圣同盟.md "wikilink")”。
  - [1886年](../Page/1886年.md "wikilink")：[威廉·亨利·芬利](../Page/威廉·亨利·芬利.md "wikilink")（William
    Henry Finlay）发现[15P/芬利彗星](../Page/15P/芬利彗星.md "wikilink")。
  - [1900年](../Page/1900年.md "wikilink")：[陶模继](../Page/陶模.md "wikilink")[鹿传霖任](../Page/鹿传霖.md "wikilink")[两广总督](../Page/两广总督.md "wikilink")。

### 20世紀

  - [1907年](../Page/1907年.md "wikilink")：[新西兰和](../Page/新西兰.md "wikilink")[纽芬兰获得自治](../Page/纽芬兰.md "wikilink")，成为[大英帝国的](../Page/大英帝国.md "wikilink")[自治领](../Page/自治领.md "wikilink")。
  - [1914年](../Page/1914年.md "wikilink")：美国[联邦贸易委员会](../Page/联邦贸易委员会.md "wikilink")（FTC）根据美国[联邦贸易委员会法案成立](../Page/联邦贸易委员会法案.md "wikilink")。
  - [1914年](../Page/1914年.md "wikilink")：[一战](../Page/一战.md "wikilink")：[日军控制](../Page/日本军.md "wikilink")[胶济铁路](../Page/胶济铁路.md "wikilink")，包围驻[青岛的](../Page/青岛.md "wikilink")[德军](../Page/德军.md "wikilink")。德军唯一可作战飞机同日军9架飞机周旋，是为[亚洲首次](../Page/亚洲.md "wikilink")[空战](../Page/空战.md "wikilink")。
  - [1946年](../Page/1946年.md "wikilink")：[國泰航空有限公司成立](../Page/國泰航空.md "wikilink")。
  - [1950年](../Page/1950年.md "wikilink")：[联合国军从](../Page/联合国.md "wikilink")[北朝鲜手中夺回](../Page/北朝鲜.md "wikilink")[首尔](../Page/首尔.md "wikilink")。
  - [1950年](../Page/1950年.md "wikilink")：[印度尼西亚被接纳为](../Page/印度尼西亚.md "wikilink")[联合国成员国](../Page/联合国.md "wikilink")。
  - [1957年](../Page/1957年.md "wikilink")：由[莎士比亚的](../Page/威廉·莎士比亚.md "wikilink")[戏剧](../Page/戏剧.md "wikilink")《[罗密欧与朱丽叶](../Page/罗密欧与朱丽叶.md "wikilink")》改编而成的[音乐剧](../Page/音乐剧.md "wikilink")《[夢斷城西](../Page/夢斷城西.md "wikilink")》在[美国](../Page/美国.md "wikilink")[纽约](../Page/纽约.md "wikilink")[百老汇首演](../Page/百老汇.md "wikilink")。
  - [1958年](../Page/1958年.md "wikilink")：中国撤销[内蒙古](../Page/内蒙古.md "wikilink")[察哈尔盟](../Page/察哈尔盟.md "wikilink")，辖域并入[锡林郭勒盟](../Page/锡林郭勒盟.md "wikilink")。
  - [1959年](../Page/1959年.md "wikilink")：中国在[黑龍江省发现](../Page/黑龍江省.md "wikilink")[大庆油田](../Page/大庆油田.md "wikilink")。
  - [1960年](../Page/1960年.md "wikilink")：[约翰·肯尼迪和](../Page/约翰·肯尼迪.md "wikilink")[理查德·尼克松进行](../Page/理查德·尼克松.md "wikilink")[美国历史上首次总统候选人全国](../Page/美国.md "wikilink")[电视辩论](../Page/电视辩论.md "wikilink")。
  - [1961年](../Page/1961年.md "wikilink")：[鲍勃·迪伦首次公开演出](../Page/鲍勃·迪伦.md "wikilink")。
  - [1963年](../Page/1963年.md "wikilink")：[新加坡政府派军警进入](../Page/新加坡.md "wikilink")[南洋大学校园](../Page/南洋大学_\(新加坡\).md "wikilink")。
  - [1967年](../Page/1967年.md "wikilink")：[中国与](../Page/中国.md "wikilink")[突尼斯中止](../Page/突尼斯.md "wikilink")[外交关系](../Page/外交关系.md "wikilink")，参见[中华人民共和国与世界各国建交简表](../Page/中华人民共和国与世界各国建交简表.md "wikilink")。
  - [1968年](../Page/1968年.md "wikilink")：[香港警察成立](../Page/香港警察.md "wikilink")[机动部队](../Page/香港警察机动部队.md "wikilink")，俗称蓝帽子。
  - [1969年](../Page/1969年.md "wikilink")：[披头士乐队的专辑](../Page/披头士.md "wikilink")[Abbey
    Road在英国发行](../Page/Abbey_Road.md "wikilink")。
  - [1973年](../Page/1973年.md "wikilink")：[毛里塔尼亚加入](../Page/毛里塔尼亚.md "wikilink")[阿拉伯国家联盟](../Page/阿拉伯国家联盟.md "wikilink")。
  - [1975年](../Page/1975年.md "wikilink")：[堪察加半岛上著名的](../Page/堪察加半岛.md "wikilink")[克留切夫火山群中的](../Page/克留切夫火山群.md "wikilink")[托尔巴希克火山爆发](../Page/托尔巴希克火山.md "wikilink")。
  - [1980年](../Page/1980年.md "wikilink")：中国自行设计制造的第一架现代化喷气式大客机——“[运十](../Page/运十.md "wikilink")”首次试飞成功。
  - [1980年](../Page/1980年.md "wikilink")：[国际宇航联合会](../Page/国际宇航联合会.md "wikilink")（International
    Astronautical Federation）接纳中国为会员国。
  - [1982年](../Page/1982年.md "wikilink")：[日本首相](../Page/日本首相.md "wikilink")[铃木善幸访问](../Page/铃木善幸.md "wikilink")[中国](../Page/中国.md "wikilink")，见[中日关系](../Page/中日关系.md "wikilink")。
  - [1984年](../Page/1984年.md "wikilink")：[中](../Page/中華人民共和國.md "wikilink")[英兩國政府在](../Page/英國.md "wikilink")[北京草簽](../Page/北京.md "wikilink")[香港前途問題聯合聲明](../Page/香港前途問題.md "wikilink")。
  - [1985年](../Page/1985年.md "wikilink")：[香港立法局首次舉行間接選舉](../Page/香港立法局.md "wikilink")，詳情請參閱[1985年香港立法局選舉](../Page/1985年香港立法局選舉.md "wikilink")。
  - [1988年](../Page/1988年.md "wikilink")：[本·约翰逊因为](../Page/本·约翰逊.md "wikilink")[药检不过关而被剥夺了](../Page/药检.md "wikilink")[汉城奥运会中](../Page/1988年夏季奥运会.md "wikilink")[男子100米短跑比赛中获得的](../Page/男子100米短跑.md "wikilink")[金牌](../Page/金牌.md "wikilink")。
  - [1991年](../Page/1991年.md "wikilink")：[美国](../Page/美国.md "wikilink")“[生物圈2号](../Page/生物圈2号.md "wikilink")”实验室开始进行第一次实验任务。
  - [1995年](../Page/1995年.md "wikilink")：[塔利班占领了](../Page/塔利班.md "wikilink")[阿富汗电台](../Page/阿富汗.md "wikilink")、电视台和总统府。
  - [1997年](../Page/1997年.md "wikilink")：[印尼发生](../Page/印尼.md "wikilink")[加鲁达航空152号班机空难](../Page/加鲁达航空152号班机空难.md "wikilink")。

### 21世紀

  - [2001年](../Page/2001年.md "wikilink")：[水木清华BBS开始试行仲裁制度](../Page/水木清华BBS.md "wikilink")。
  - [2002年](../Page/2002年.md "wikilink")：[塞內加爾載有](../Page/塞內加爾.md "wikilink")1034名乘客的「喬拉號」客輪在[岡比亞海域失事](../Page/岡比亞.md "wikilink")，只有64人生還。
  - [2003年](../Page/2003年.md "wikilink")：[日本](../Page/日本.md "wikilink")[北海道发生](../Page/北海道.md "wikilink")[里氏地震规模](../Page/里氏地震规模.md "wikilink")8级地震。
  - [2003年](../Page/2003年.md "wikilink")：中国出现“[乙肝歧视第一案](../Page/乙肝歧视.md "wikilink")”。
  - [2005年](../Page/2005年.md "wikilink")：中国决定将开发下一代基于AVS视频编码的[增强型通用光盘](../Page/增强型通用光盘.md "wikilink")（EVD）标准。
  - [2006年](../Page/2006年.md "wikilink")：[安倍晋三就任第](../Page/安倍晋三.md "wikilink")90届[日本首相](../Page/日本首相.md "wikilink")。
  - [2006年](../Page/2006年.md "wikilink")：[美林集团与](../Page/美林集团.md "wikilink")[贝莱德合并](../Page/贝莱德.md "wikilink")。

## 出生

  - [1791年](../Page/1791年.md "wikilink")：[傑利柯](../Page/傑利柯.md "wikilink")，[法國浪漫主義畫派先驅](../Page/法國.md "wikilink")（[1824年逝世](../Page/1824年.md "wikilink")）
  - [1849年](../Page/1849年.md "wikilink")：[巴甫洛夫](../Page/巴甫洛夫.md "wikilink")，[俄羅斯生理學家](../Page/俄羅斯.md "wikilink")、心理學家、醫師，1904年獲[諾貝爾生理學或醫學獎](../Page/諾貝爾生理學或醫學獎.md "wikilink")（1936年去世）
  - [1870年](../Page/1870年.md "wikilink")：[克里斯蒂安十世](../Page/克里斯蒂安十世.md "wikilink")，[丹麦国王](../Page/丹麦.md "wikilink")（[1947年逝世](../Page/1947年.md "wikilink")）
  - [1874年](../Page/1874年.md "wikilink")：[路易斯·海因](../Page/路易斯·海因.md "wikilink")，[美國攝影師](../Page/美國.md "wikilink")（[1940年逝世](../Page/1940年.md "wikilink")）
  - [1877年](../Page/1877年.md "wikilink")：[阿尔弗雷德·科尔托](../Page/阿尔弗雷德·科尔托.md "wikilink")，法國鋼琴家、指揮家（[1962年逝世](../Page/1962年.md "wikilink")）
  - [1886年](../Page/1886年.md "wikilink")：[阿奇博尔德·希尔](../Page/阿奇博尔德·希尔.md "wikilink")，[英国生理学家](../Page/英国.md "wikilink")，[1922年获](../Page/1922年.md "wikilink")[诺贝尔生理学及医学奖金](../Page/诺贝尔生理学及医学奖.md "wikilink")（[1977年逝世](../Page/1977年.md "wikilink")）
  - [1888年](../Page/1888年.md "wikilink")：[T·S·艾略特](../Page/T·S·艾略特.md "wikilink")，美裔[英國诗人](../Page/英國.md "wikilink")、文學評論家，1948年獲[諾貝爾文學獎](../Page/諾貝爾文學獎.md "wikilink")（[1965年逝世](../Page/1965年.md "wikilink")）
  - [1889年](../Page/1889年.md "wikilink")：[马丁·海德格尔](../Page/马丁·海德格尔.md "wikilink")，[德国哲學家](../Page/德国.md "wikilink")（[1976年逝世](../Page/1976年.md "wikilink")）
  - [1897年](../Page/1897年.md "wikilink")：[保禄六世](../Page/保禄六世.md "wikilink")，[羅馬天主教](../Page/羅馬天主教.md "wikilink")[教宗](../Page/教宗.md "wikilink")（[1978年逝世](../Page/1978年.md "wikilink")）
  - [1898年](../Page/1898年.md "wikilink")：[-{zh-cn:乔治·格什温;zh-hk:喬治·哥舒詠;zh-tw:喬治·蓋希文;}-](../Page/乔治·格什温.md "wikilink")，[美国作曲家](../Page/美国.md "wikilink")（[1937年逝世](../Page/1937年.md "wikilink")）
  - [1914年](../Page/1914年.md "wikilink")：[杰克·拉兰内](../Page/杰克·拉兰内.md "wikilink")，美國健身大師（[2011年逝世](../Page/2011年.md "wikilink")）
  - [1916年](../Page/1916年.md "wikilink")：[斯凯荣内](../Page/佛朗哥·斯凯荣内.md "wikilink")，意大利汽车设计师（[1993年逝世](../Page/1993年.md "wikilink")）
  - 1916年：[李福和](../Page/李福和.md "wikilink")，[香港](../Page/香港.md "wikilink")[銀行家](../Page/銀行家.md "wikilink")，[東亞銀行主席](../Page/東亞銀行.md "wikilink")（[2014年逝世](../Page/2014年.md "wikilink")）
  - [1917年](../Page/1917年.md "wikilink")：[陈德滔](../Page/陈德滔.md "wikilink")，越南哲學家（[1993年逝世](../Page/1993年.md "wikilink")）
  - [1921年](../Page/1921年.md "wikilink")：[王光美](../Page/王光美.md "wikilink")，中国国家主席[刘少奇的夫人](../Page/刘少奇.md "wikilink")，中国政治人物（[2006年逝世](../Page/2006年.md "wikilink")）
  - [1926年](../Page/1926年.md "wikilink")：[小柴昌俊](../Page/小柴昌俊.md "wikilink")，[日本](../Page/日本.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")，[诺贝尔奖获得者](../Page/诺贝尔奖.md "wikilink")
  - 1926年：[茱莉伦敦](../Page/茱莉伦敦.md "wikilink")，美國歌手、演員（[2000年逝世](../Page/2000年.md "wikilink")）
  - [1927年](../Page/1927年.md "wikilink")：[恩佐·贝阿尔佐特](../Page/恩佐·贝阿尔佐特.md "wikilink")，義大利足球運動員及教練（[2010年逝世](../Page/2010年.md "wikilink")）
  - [1932年](../Page/1932年.md "wikilink")：[曼莫汉·辛格](../Page/曼莫汉·辛格.md "wikilink")，[印度总理](../Page/印度总理.md "wikilink")
  - [1941年](../Page/1941年.md "wikilink")：[萨尔瓦托雷·阿卡多](../Page/萨尔瓦托雷·阿卡多.md "wikilink")，義大利小提琴家、作曲家
  - [1948年](../Page/1948年.md "wikilink")：[-{zh-cn:奥莉维亚·纽顿-约翰;
    zh-hk:奧莉花·紐頓-莊;
    zh-tw:奧莉薇亞·紐頓-強}-](../Page/奧莉維亞·紐頓-約翰.md "wikilink")，英國裔澳洲流行樂手，1974年獲第16屆[-{zh-hans:葛萊美奖;zh-hant:葛萊美獎;zh-cn:格莱美奖;zh-tw:葛萊美獎;zh-hk:格林美獎;zh-mo:格林美獎;}-](../Page/葛萊美獎.md "wikilink")
  - [1949年](../Page/1949年.md "wikilink")：[安德里亞·德沃金](../Page/安德里亞·德沃金.md "wikilink")，美國女性主義者（[2005年逝世](../Page/2005年.md "wikilink")）
  - [1950年](../Page/1950年.md "wikilink")：[李司棋](../Page/李司棋.md "wikilink")，[香港电视演员](../Page/香港.md "wikilink")
  - [1956年](../Page/1956年.md "wikilink")：[琳達·漢密爾頓](../Page/琳達·漢密爾頓.md "wikilink")，美國女演員，以《[魔鬼終結者](../Page/魔鬼終結者.md "wikilink")》莎拉·康納一角聞名
  - [1961年](../Page/1961年.md "wikilink")：[威爾·塞爾夫](../Page/威爾·塞爾夫.md "wikilink")，英國小說家和專欄作家
  - [1962年](../Page/1962年.md "wikilink")：[吳宗憲](../Page/吳宗憲.md "wikilink")，[台灣男](../Page/台灣.md "wikilink")[歌手](../Page/歌手.md "wikilink")、[主持人](../Page/主持人.md "wikilink")
  - [1967年](../Page/1967年.md "wikilink")：[郭藹明](../Page/郭藹明.md "wikilink")，香港女[演員](../Page/演員.md "wikilink")，1991年[香港小姐競選冠軍](../Page/香港小姐競選.md "wikilink")
  - [1971年](../Page/1971年.md "wikilink")：[韩红](../Page/韩红.md "wikilink")，[中国歌手](../Page/中华人民共和国歌手列表.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[丸藤正道](../Page/丸藤正道.md "wikilink")，日本職業摔角選手
  - [1980年](../Page/1980年.md "wikilink")：[帕特里克·弗里萨赫](../Page/帕特里克·弗里萨赫.md "wikilink")，[奧地利賽車手](../Page/奧地利.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[克里斯蒂娜·米蓮](../Page/克里斯蒂娜·米蓮.md "wikilink")，美國R\&B與Pop歌手、舞者、演員、流行音樂作曲者及模特兒
  - [1981年](../Page/1981年.md "wikilink")：[姚贝娜中国青年歌唱家](../Page/姚贝娜.md "wikilink")，逝世於2015年1月16日
  - [1988年](../Page/1988年.md "wikilink")：[-{zh-hans:基拉·科尔皮;
    zh-tw:綺拉·蔻爾普;zh-hk:琪拉·科爾皮;}-](../Page/基拉·科尔皮.md "wikilink")，芬蘭女子花式滑冰運動員

## 逝世

  - [1573年](../Page/1573年.md "wikilink")：[浅井长政](../Page/浅井长政.md "wikilink")，[日本战国时代的](../Page/日本战国时代.md "wikilink")[大名](../Page/大名_\(称谓\).md "wikilink")（[1545年出生](../Page/1545年.md "wikilink")）
  - [1620年](../Page/1620年.md "wikilink")：朱常洛，[明光宗](../Page/明光宗.md "wikilink")（泰昌帝，[1582年出生](../Page/1582年.md "wikilink")）
  - [1841年](../Page/1841年.md "wikilink")：[龚自珍](../Page/龚自珍.md "wikilink")，[清代](../Page/清代.md "wikilink")[思想家](../Page/思想家.md "wikilink")、[文学家](../Page/文学家.md "wikilink")（[1792年出生](../Page/1792年.md "wikilink")）
  - [1868年](../Page/1868年.md "wikilink")：[莫比乌斯](../Page/莫比乌斯.md "wikilink")，[德国](../Page/德国.md "wikilink")[数学家](../Page/数学家.md "wikilink")、[天文学家](../Page/天文学家.md "wikilink")（[1790年出生](../Page/1790年.md "wikilink")）
  - [1932年](../Page/1932年.md "wikilink")：[狄蓋特](../Page/狄蓋特.md "wikilink")，《[國際歌](../Page/國際歌.md "wikilink")》作曲者（[1848年出生](../Page/1848年.md "wikilink")）
  - [1945年](../Page/1945年.md "wikilink")：[巴尔托克](../Page/巴尔托克.md "wikilink")，[匈牙利](../Page/匈牙利.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")（[1881年出生](../Page/1881年.md "wikilink")）
  - [1953年](../Page/1953年.md "wikilink")：[徐悲鸿](../Page/徐悲鸿.md "wikilink")，[中国](../Page/中国.md "wikilink")[画家](../Page/画家.md "wikilink")、美术教育家（[1895年出生](../Page/1895年.md "wikilink")）
  - [1961年](../Page/1961年.md "wikilink")：[艾克尔伯格](../Page/罗伯特·艾克尔伯格.md "wikilink")，[美国将军](../Page/美国.md "wikilink")（[1886年出生](../Page/1886年.md "wikilink")）
  - [1990年](../Page/1990年.md "wikilink")：[阿尔贝托·莫拉维亚](../Page/阿尔贝托·莫拉维亚.md "wikilink")，[意大利](../Page/意大利.md "wikilink")[作家](../Page/作家.md "wikilink")（[1907年出生](../Page/1907年.md "wikilink")）
  - [1996年](../Page/1996年.md "wikilink")：[陳毓祥](../Page/陳毓祥.md "wikilink")，[香港保釣人士](../Page/香港.md "wikilink")，於宣示[釣魚台主權時遇溺逝世](../Page/釣魚台.md "wikilink")（[1950年出生](../Page/1950年.md "wikilink")）
  - [1999年](../Page/1999年.md "wikilink")：[布達勞](../Page/布達勞.md "wikilink")，[香港](../Page/香港.md "wikilink")[土生葡裔](../Page/土生葡人.md "wikilink")[律師](../Page/律師.md "wikilink")、[公務員和](../Page/香港公務員.md "wikilink")[軍人](../Page/軍人.md "wikilink")（[1906年出生](../Page/1906年.md "wikilink")）
  - [2008年](../Page/2008年.md "wikilink")：[林世榮](../Page/林世榮_\(商人\).md "wikilink")，[香港企業家](../Page/香港.md "wikilink")（[1955年出生](../Page/1955年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[于光远](../Page/于光远.md "wikilink")，[中国社科院原副院长](../Page/中国社科院.md "wikilink")、原[中顾委委员](../Page/中顾委.md "wikilink")、经济学家（[1915年出生](../Page/1915年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[阿兹占阿都拉萨](../Page/阿兹占阿都拉萨.md "wikilink")，[马来西亚](../Page/马来西亚.md "wikilink")[吉打州第](../Page/吉打州.md "wikilink")10任[州务大臣](../Page/州务大臣.md "wikilink")（[1944年出生](../Page/1944年.md "wikilink")）

## 节假日和习俗

  - 阿拉伯[也门共和国](../Page/也门.md "wikilink")：革命节
  - [新西兰](../Page/新西兰.md "wikilink")：独立日

## 參考資料

1.  [1](https://books.google.com.tw/books?id=h8GnNQLGDRQC&pg=PA6&lpg=PA6&dq=Denmark+and+Sweden+signed+a+Lund+treaty&source=bl&ots=ZnEsho2j_n&sig=Cn8ir_LueusM5JFNfTJ7RYyZeUk&hl=zh-TW&sa=X&ved=0CDMQ6AEwA2oVChMI0L_-8qG5xwIVSJyUCh34MApk#v=onepage&q=Denmark%20and%20Sweden%20signed%20a%20Lund%20treaty&f=false)，Multilateral
    Treaty Calendar(多邊的條約日歷)。