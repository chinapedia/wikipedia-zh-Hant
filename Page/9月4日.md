**9月4日**是[阳历年的第](../Page/阳历.md "wikilink")247天（[闰年是](../Page/闰年.md "wikilink")248天），离一年的结束还有118天。

在[英國與其屬地和](../Page/英國.md "wikilink")[北美十三州](../Page/北美十三州.md "wikilink")，因[1752年將曆法從](../Page/1752年.md "wikilink")[儒略曆轉換至](../Page/儒略曆.md "wikilink")[格里曆](../Page/格里曆.md "wikilink")，故該年沒有9月4日。

## 大事記

### 5世紀

  - [476年](../Page/476年.md "wikilink")：[日耳曼人领袖](../Page/日耳曼人.md "wikilink")[奥多亚克占领](../Page/奥多亚克.md "wikilink")[拉韦纳](../Page/拉韦纳.md "wikilink")，并废黜[西罗马帝国皇帝](../Page/西罗马帝国.md "wikilink")[罗慕路斯·奥古斯都](../Page/罗慕路斯·奥古斯都.md "wikilink")，西罗马帝国灭亡。

### 7世紀

  - [626年](../Page/626年.md "wikilink")：[唐太宗李世民正式登基成為](../Page/唐太宗.md "wikilink")[唐朝皇帝](../Page/唐朝君主列表.md "wikilink")。

### 18世紀

  - [1774年](../Page/1774年.md "wikilink")：[英国探险家](../Page/英国.md "wikilink")[詹姆斯·库克发现](../Page/詹姆斯·库克.md "wikilink")[新喀里多尼亚岛屿](../Page/新喀里多尼亚.md "wikilink")。
  - [1781年](../Page/1781年.md "wikilink")：[洛杉矶建城](../Page/洛杉矶.md "wikilink")。

### 19世紀

  - [1812年](../Page/1812年.md "wikilink")：[1812年戰爭](../Page/1812年戰爭.md "wikilink")：[哈里森堡壘戰役爆發](../Page/哈里森堡壘戰役.md "wikilink")。
  - [1870年](../Page/1870年.md "wikilink")：[巴黎民众爆发革命](../Page/巴黎.md "wikilink")，[法国皇帝](../Page/法国皇帝.md "wikilink")[拿破仑三世被廢黜](../Page/拿破仑三世.md "wikilink")，[法兰西第三共和国成立](../Page/法兰西第三共和国.md "wikilink")。
  - [1888年](../Page/1888年.md "wikilink")：[美国发明家](../Page/美国.md "wikilink")[乔治·伊士曼在获得胶卷式](../Page/乔治·伊士曼.md "wikilink")[照相机的](../Page/照相机.md "wikilink")[专利后注册了](../Page/专利.md "wikilink")[柯达的商标](../Page/柯达.md "wikilink")。

### 20世紀

  - [1913年](../Page/1913年.md "wikilink")：[德國發生](../Page/德國.md "wikilink")[華格納槍擊案](../Page/華格納槍擊案.md "wikilink")。
  - [1939年](../Page/1939年.md "wikilink")：[二战](../Page/二战.md "wikilink")：[日本宣布在](../Page/日本.md "wikilink")[欧洲战争中保持](../Page/欧洲.md "wikilink")[中立](../Page/中立.md "wikilink")。
  - [1944年](../Page/1944年.md "wikilink")：二战：[英國第十一裝甲師解放](../Page/英國第十一裝甲師.md "wikilink")[比利時的](../Page/比利時.md "wikilink")[安特衛普](../Page/安特衛普.md "wikilink")。
  - [1948年](../Page/1948年.md "wikilink")：[荷蘭女王](../Page/荷兰国王.md "wikilink")[威廉明娜宣布因健康原因退位](../Page/威廉明娜女王.md "wikilink")，並由女兒[朱麗安娜繼位](../Page/朱麗安娜_\(荷蘭\).md "wikilink")。
  - [1957年](../Page/1957年.md "wikilink")：[美国民权运动](../Page/美国民权运动.md "wikilink")：[小石城事件](../Page/小石城事件.md "wikilink")。
  - [1958年](../Page/1958年.md "wikilink")：[中國政府公佈關於](../Page/中华人民共和国政府.md "wikilink")[領海的聲明](../Page/領海.md "wikilink")。
  - [1963年](../Page/1963年.md "wikilink")：[瑞士航空306號班機空難](../Page/瑞士航空306號班機空難.md "wikilink")，機上80人悉數罹難。
  - [1967年](../Page/1967年.md "wikilink")：[美國海軍陸戰隊進攻](../Page/美國海軍陸戰隊.md "wikilink")[北越的](../Page/北越.md "wikilink")[溪生](../Page/溪生.md "wikilink")。
  - [1970年](../Page/1970年.md "wikilink")：[萨尔瓦多·阿连德当选](../Page/萨尔瓦多·阿连德.md "wikilink")[智利](../Page/智利.md "wikilink")[总统](../Page/智利總統.md "wikilink")。
  - [1971年](../Page/1971年.md "wikilink")：由[波音727擔任的](../Page/波音727.md "wikilink")[阿拉斯加航空](../Page/阿拉斯加航空.md "wikilink")於[阿拉斯加州](../Page/阿拉斯加州.md "wikilink")[朱諾附近墜毀](../Page/朱諾.md "wikilink")，機上111人全部罹難。
  - [1972年](../Page/1972年.md "wikilink")：美国[游泳运动员](../Page/游泳.md "wikilink")[马克·斯皮茨在](../Page/马克·斯皮茨.md "wikilink")[慕尼黑奥运会上夺得第七枚](../Page/慕尼黑奥运会.md "wikilink")[金牌](../Page/金牌.md "wikilink")，成为当时在一届[奥运会中获得最多金牌的运动员](../Page/奥运会.md "wikilink")。此紀錄一直維持到2008年，由其同胞且同樣擅長游泳的[麥可·菲爾普斯於](../Page/麥可·菲爾普斯.md "wikilink")[北京奧運獨得八枚金牌為止](../Page/北京奧運.md "wikilink")。
  - [1975年](../Page/1975年.md "wikilink")：[埃及與](../Page/埃及.md "wikilink")[以色列签订](../Page/以色列.md "wikilink")，埃及收复[西奈半岛](../Page/西奈半岛.md "wikilink")。
  - [1990年](../Page/1990年.md "wikilink")：[朝鲜总理](../Page/朝鲜总理.md "wikilink")[延亨默越過](../Page/延亨默.md "wikilink")[三八线到達](../Page/三八线.md "wikilink")[漢城](../Page/漢城.md "wikilink")，與[韩国总理](../Page/韩国总理.md "wikilink")[姜英勳举行](../Page/姜英勳.md "wikilink")[朝鲜半岛自分裂以来双方的首次总理會談](../Page/朝鲜半岛.md "wikilink")。
  - [1995年](../Page/1995年.md "wikilink")：拍賣網站[ebay](../Page/ebay.md "wikilink")（前身為Actionweb）正式成立。
  - [1995年](../Page/1995年.md "wikilink")：於[北京開幕](../Page/北京.md "wikilink")，有4750名分別來自181個國家的人士出席。
  - [1998年](../Page/1998年.md "wikilink")：[網際網路](../Page/網際網路.md "wikilink")[搜尋引擎](../Page/搜尋引擎.md "wikilink")[Google成立](../Page/Google.md "wikilink")。

### 21世紀

  - [2001年](../Page/2001年.md "wikilink")：[東京迪士尼海洋開幕](../Page/東京迪士尼海洋.md "wikilink")。
  - [2007年](../Page/2007年.md "wikilink")：[香港年齡最小的大學生](../Page/香港.md "wikilink")[沈詩鈞到](../Page/沈詩鈞.md "wikilink")[香港浸會大學上學](../Page/香港浸會大學.md "wikilink")。
  - [2010年](../Page/2010年.md "wikilink")：[新西兰](../Page/新西兰.md "wikilink")[南岛发生了](../Page/南島_\(新西蘭\).md "wikilink")[里氏规模](../Page/里氏地震规模.md "wikilink")7.1级的[大地震](../Page/2010年新西兰南岛地震.md "wikilink")，造成高达3.5亿新西兰元的经济损失。
  - [2013年](../Page/2013年.md "wikilink")：[美國總統](../Page/美國總統.md "wikilink")[歐巴馬訪問](../Page/歐巴馬.md "wikilink")[瑞典](../Page/瑞典.md "wikilink")，強化[兩國關係](../Page/瑞典－美國關係.md "wikilink")、創下美國總統訪瑞先例。
  - [2016年](../Page/2016年.md "wikilink")：[香港舉行](../Page/香港.md "wikilink")[第六屆立法會選舉](../Page/2016年香港立法會選舉.md "wikilink")。當天的地區選舉投票率達58.28%，而[功能界別選舉的投票率則達](../Page/功能界別.md "wikilink")57.09%。

## 出生

  - [1241年](../Page/1241年.md "wikilink")：[亞歷山大三世](../Page/亞歷山大三世_\(蘇格蘭\).md "wikilink")，[蘇格蘭](../Page/蘇格蘭.md "wikilink")[國王](../Page/蘇格蘭君主列表.md "wikilink")（[1286年去世](../Page/1286年.md "wikilink")）
  - [1359年](../Page/1359年.md "wikilink")：[足利氏滿](../Page/足利氏滿.md "wikilink")，[室町幕府第](../Page/室町幕府.md "wikilink")2代[鎌倉公方](../Page/鎌倉公方.md "wikilink")（[1398年去世](../Page/1398年.md "wikilink")）
  - [1563年](../Page/1563年.md "wikilink")：[明神宗朱翊鈞](../Page/明神宗.md "wikilink")，[明朝](../Page/明朝.md "wikilink")[皇帝](../Page/明朝君主列表.md "wikilink")（[1620年去世](../Page/1620年.md "wikilink")）
  - [1652年](../Page/1652年.md "wikilink")：[德川綱誠](../Page/德川綱誠.md "wikilink")，第3代[尾張藩主](../Page/尾張藩.md "wikilink")（[1699年去世](../Page/1699年.md "wikilink")）
  - [1787年](../Page/1787年.md "wikilink")：[二宮尊德](../Page/二宮尊德.md "wikilink")（二宮金次郎），思想家（[1856年去世](../Page/1856年.md "wikilink")）
  - [1824年](../Page/1824年.md "wikilink")：[安东·布鲁克纳](../Page/安东·布鲁克纳.md "wikilink")，[奥地利作曲家](../Page/奥地利.md "wikilink")（[1896年去世](../Page/1896年.md "wikilink")）
  - [1825年](../Page/1825年.md "wikilink")：[納奧羅吉](../Page/納奧羅吉.md "wikilink")，印度教育家（1917年去世）
  - [1850年](../Page/1850年.md "wikilink")：[路易吉·卡多尔纳](../Page/路易吉·卡多尔纳.md "wikilink")，義大利軍人（1928年去世）
  - [1892年](../Page/1892年.md "wikilink")：[达律斯·米约](../Page/达律斯·米约.md "wikilink")，猶太裔[法國作曲家](../Page/法國.md "wikilink")，[六人團成員之一](../Page/六人團.md "wikilink")；20世紀產量最大的作曲家之一（[1974年去世](../Page/1974年.md "wikilink")）
  - [1895年](../Page/1895年.md "wikilink")：[向警予](../Page/向警予.md "wikilink")，中国妇女运动领袖（[1928年去世](../Page/1928年.md "wikilink")）
  - [1896年](../Page/1896年.md "wikilink")：[安托南·阿尔托](../Page/安托南·阿尔托.md "wikilink")，法國詩人（1948年去世）
  - [1905年](../Page/1905年.md "wikilink")：[瑪莉·雷諾特](../Page/瑪莉·雷諾特.md "wikilink")，英國作家（1983年去世）
  - [1906年](../Page/1906年.md "wikilink")：[馬克斯·德爾布呂克](../Page/馬克斯·德爾布呂克.md "wikilink")，德裔美籍生物[物理學家](../Page/物理學家.md "wikilink")（[1981年去世](../Page/1981年.md "wikilink")）
  - [1908年](../Page/1908年.md "wikilink")：[理查德·赖特](../Page/理查德·赖特_\(作家\).md "wikilink")，[美国作家](../Page/美国.md "wikilink")（[1960年去世](../Page/1960年.md "wikilink")）
  - [1913年](../Page/1913年.md "wikilink")：[丹下健三](../Page/丹下健三.md "wikilink")，[日本建築師](../Page/日本.md "wikilink")（[2005年去世](../Page/2005年.md "wikilink")）
  - 1913年：[斯坦福·摩爾](../Page/斯坦福·摩爾.md "wikilink")，美國生物化學家（[1982年去世](../Page/1982年.md "wikilink")）
  - 1913年：[米奇·柯罕](../Page/米奇·柯罕.md "wikilink")，美國黑手黨首腦（1976年去世）
  - [1924年](../Page/1924年.md "wikilink")：[瓊·艾肯](../Page/瓊·艾肯.md "wikilink")，[英國小說家](../Page/英國.md "wikilink")，以兒童文學、歷史小說而聞名。（[2004年去世](../Page/2004年.md "wikilink")）
  - [1925年](../Page/1925年.md "wikilink")：[艾薩·卡特](../Page/艾薩·卡特.md "wikilink")，美國種族隔離主義
  - [1926年](../Page/1926年.md "wikilink")：[伊凡·伊利奇](../Page/伊凡·伊利奇.md "wikilink")，美國哲學家（[2002年去世](../Page/2002年.md "wikilink")）
  - [1927年](../Page/1927年.md "wikilink")：[约翰·麦卡锡](../Page/约翰·麦卡锡.md "wikilink")，美國[計算機科學家](../Page/計算機科學家.md "wikilink")，於1955年[达特矛斯会议上提出](../Page/达特矛斯会议.md "wikilink")「人工智慧」概念、[LISP發明人](../Page/LISP.md "wikilink")（[2011年去世](../Page/2011年.md "wikilink")）
  - [1932年](../Page/1932年.md "wikilink")：[卡洛斯·罗梅罗·巴塞洛](../Page/卡洛斯·罗梅罗·巴塞洛.md "wikilink")，波多黎各總督
  - [1934年](../Page/1934年.md "wikilink")：[克莱夫·格兰杰](../Page/克莱夫·格兰杰.md "wikilink")，[威尔士裔經濟學家](../Page/威尔士.md "wikilink")，獲2003年[諾貝爾經濟學獎](../Page/諾貝爾經濟學獎.md "wikilink")（[2009年去世](../Page/2009年.md "wikilink")）
  - [1936年](../Page/1936年.md "wikilink")：[梶原一騎](../Page/梶原一騎.md "wikilink")，日本漫畫編劇（[1987年去世](../Page/1987年.md "wikilink")）
  - [1937年](../Page/1937年.md "wikilink")：[達恩·福瑞澤](../Page/達恩·福瑞澤.md "wikilink")，澳洲游泳運動員
  - [1944年](../Page/1944年.md "wikilink")：[东尼·阿特金森](../Page/东尼·阿特金森.md "wikilink")，英國經濟學家
  - [1951年](../Page/1951年.md "wikilink")：[小林薰](../Page/小林薰.md "wikilink")，日本演員
  - [1952年](../Page/1952年.md "wikilink")：[里希·卡普尔](../Page/里希·卡普尔.md "wikilink")，印度演員
  - [1955年](../Page/1955年.md "wikilink")：[布萊恩·施威澤](../Page/布萊恩·施威澤.md "wikilink")，美國政治人物
  - [1961年](../Page/1961年.md "wikilink")：[黃日華](../Page/黃日華.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [1964年](../Page/1964年.md "wikilink")：[王玉玲](../Page/王玉玲.md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")（[1993年去世](../Page/1993年.md "wikilink")）
  - [1965年](../Page/1965年.md "wikilink")：[林保怡](../Page/林保怡.md "wikilink")，香港演員
  - [1966年](../Page/1966年.md "wikilink")：[周子寒](../Page/周子寒.md "wikilink")，台灣歌手
  - [1968年](../Page/1968年.md "wikilink")：[麥克·皮耶薩](../Page/麥克·皮耶薩.md "wikilink")，美國棒球球員
  - [1971年](../Page/1971年.md "wikilink")：[袁詠儀](../Page/袁詠儀.md "wikilink")，香港女演員
  - [1972年](../Page/1972年.md "wikilink")：[葉芳華](../Page/葉芳華.md "wikilink")，[加拿大籍華人女演員](../Page/加拿大.md "wikilink")
  - [1973年](../Page/1973年.md "wikilink")：[伍秀霞](../Page/伍秀霞.md "wikilink")，香港配音員
  - [1975年](../Page/1975年.md "wikilink")：[馬克·朗森](../Page/馬克·朗森.md "wikilink")，[英國](../Page/英國.md "wikilink")[歌手](../Page/歌手.md "wikilink")、[音樂家](../Page/音樂家.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[廖-{于}-誠](../Page/廖于誠.md "wikilink")，台灣[棒球](../Page/棒球.md "wikilink")[選手](../Page/選手.md "wikilink")
  - 1980年：[碧昂絲·諾利斯](../Page/碧昂絲·諾利斯.md "wikilink")，美國歌手
  - 1980年：[岛谷瞳](../Page/岛谷瞳.md "wikilink")，日本歌手
  - 1980年：[朝河蘭](../Page/朝河蘭.md "wikilink")，AV女優
  - [1982年](../Page/1982年.md "wikilink")：[張鈞甯](../Page/張鈞甯.md "wikilink")，台灣演員
  - [1983年](../Page/1983年.md "wikilink")：[中丸雄一](../Page/中丸雄一.md "wikilink")，日本艺人
  - 1983年：[蔡曉慧](../Page/蔡曉慧.md "wikilink")，香港游泳選手
  - 1983年：[游詩璟](../Page/游詩璟.md "wikilink")，台灣女演員
  - [1990年](../Page/1990年.md "wikilink")：[斯特凡尼娅·费尔南德斯](../Page/斯特凡尼娅·费尔南德斯.md "wikilink")，2008年度[委內瑞拉小姐和](../Page/委內瑞拉小姐.md "wikilink")[2009年度環球小姐比賽冠軍](../Page/2009年度環球小姐比賽.md "wikilink")
  - 1990年：[貢米 (演員)](../Page/貢米_\(演員\).md "wikilink")，中國女演員
  - [1991年](../Page/1991年.md "wikilink")：[李迪恩](../Page/李迪恩.md "wikilink")，台灣團體[4ever成員](../Page/4ever.md "wikilink")
  - [1992年](../Page/1992年.md "wikilink")：[朴恩玭](../Page/朴恩玭.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[段宜恩](../Page/段宜恩.md "wikilink")，韓國組合[GOT7成員](../Page/GOT7.md "wikilink")

## 逝世

  - [422年](../Page/422年.md "wikilink")：[教宗波尼法爵一世](../Page/教宗波尼法爵一世.md "wikilink")
  - [799年](../Page/799年.md "wikilink")：[穆萨·卡齐姆](../Page/穆萨·卡齐姆.md "wikilink")，伊斯蘭教什葉派伊瑪目
  - [1037年](../Page/1037年.md "wikilink")：[貝爾穆多三世](../Page/贝尔穆多三世_\(莱昂\).md "wikilink")，雷昂國王
  - [1063年](../Page/1063年.md "wikilink")：[图赫里勒·贝格](../Page/图赫里勒·贝格.md "wikilink")，塞爾柱帝國蘇丹
  - [1323年](../Page/1323年.md "wikilink")：[元英宗硕德八剌](../Page/元英宗.md "wikilink")，[元朝](../Page/元朝.md "wikilink")[皇帝](../Page/元朝君主列表.md "wikilink")（生于[1303年](../Page/1303年.md "wikilink")）
  - [1342年](../Page/1342年.md "wikilink")：[安娜·阿纳卓特鲁](../Page/安娜·阿纳卓特鲁.md "wikilink")，[特拉比松帝國](../Page/特拉比松帝國.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")
  - [1571年](../Page/1571年.md "wikilink")：[第四代伦诺克斯伯爵马修·斯图亚特](../Page/第四代伦诺克斯伯爵马修·斯图亚特.md "wikilink")
  - [1811年](../Page/1811年.md "wikilink")：[松村吳春](../Page/松村吳春.md "wikilink")，日本畫家（生於[1752年](../Page/1752年.md "wikilink")）
  - [1907年](../Page/1907年.md "wikilink")：[爱德华·格里格](../Page/爱德华·格里格.md "wikilink")，[挪威作曲家](../Page/挪威.md "wikilink")（生于[1843年](../Page/1843年.md "wikilink")）
  - [1916年](../Page/1916年.md "wikilink")：[何塞·埃切加賴](../Page/何塞·埃切加賴.md "wikilink")，[諾貝爾文學獎得主](../Page/諾貝爾文學獎.md "wikilink")，[西班牙劇作家](../Page/西班牙.md "wikilink")（生于[1832年](../Page/1832年.md "wikilink")）
  - [1940年](../Page/1940年.md "wikilink")：[北白川宮永久王](../Page/北白川宮永久王.md "wikilink")，日本[皇族](../Page/皇族.md "wikilink")（生於[1910年](../Page/1910年.md "wikilink")）
  - [1965年](../Page/1965年.md "wikilink")：[史懷哲](../Page/史懷哲.md "wikilink")，[諾貝爾和平獎得主](../Page/諾貝爾和平獎.md "wikilink")、致力於[非洲人民健康的醫生](../Page/非洲.md "wikilink")（生於[1875年](../Page/1875年.md "wikilink")）
  - [1989年](../Page/1989年.md "wikilink")：[喬治·西默農](../Page/喬治·西默農.md "wikilink")，比利時法語作家（生於1903年）
  - [2006年](../Page/2006年.md "wikilink")：[史蒂夫·厄文](../Page/史蒂夫·厄文.md "wikilink")，[動物星球頻道主持人](../Page/動物星球頻道.md "wikilink")（生于[1962年](../Page/1962年.md "wikilink")）
  - 2006年：[吉亞琴托·法切蒂](../Page/吉亞琴托·法切蒂.md "wikilink")，[意大利](../Page/意大利.md "wikilink")[足球運動員](../Page/足球.md "wikilink")（生於[1942年](../Page/1942年.md "wikilink")）
  - [2014年](../Page/2014年.md "wikilink")：[瓊·瑞佛斯](../Page/瓊·瑞佛斯.md "wikilink")，美國演員

## 节假日和习俗

  - [德蕾莎修女封聖日](../Page/德蕾莎修女.md "wikilink")\[1\]

## 參考資料

1.