**9月9日**是[阳历年的第](../Page/阳历.md "wikilink")252天（[闰年是](../Page/闰年.md "wikilink")253天），离一年的结束还有113天。

在[英國和](../Page/英國.md "wikilink")[北美十三州](../Page/北美十三州.md "wikilink")，因[1752年將曆法從](../Page/1752年.md "wikilink")[儒略曆轉換至](../Page/儒略曆.md "wikilink")[格里曆](../Page/格里曆.md "wikilink")，故該年沒有9月9日。

## 大事记

### 1世紀

  - [9年](../Page/9年.md "wikilink")：[日耳曼人部队在](../Page/日耳曼人.md "wikilink")[阿尔米尼乌斯的领导下于](../Page/阿尔米尼乌斯.md "wikilink")[条顿堡森林战役中击败](../Page/条顿堡森林战役.md "wikilink")[罗马帝国部队](../Page/罗马帝国.md "wikilink")，并且在之后几天成功歼灭三个罗马军团。

### 4世紀

  - [337年](../Page/337年.md "wikilink")：[君士坦提乌斯二世在](../Page/君士坦提乌斯二世.md "wikilink")[拜占庭登基为](../Page/拜占庭.md "wikilink")[罗马帝国东部皇帝](../Page/罗马帝国.md "wikilink")。

### 16世紀

  - [1513年](../Page/1513年.md "wikilink")：[苏格兰国王](../Page/苏格兰.md "wikilink")[詹姆斯四世在抗击](../Page/詹姆斯四世_\(苏格兰\).md "wikilink")[英格兰入侵的](../Page/英格兰.md "wikilink")[弗洛登战役中阵亡](../Page/弗洛登战役.md "wikilink")。

### 18世紀

  - [1776年](../Page/1776年.md "wikilink")：[大陆会议将北美各州的邦联正式命名为](../Page/大陆会议.md "wikilink")[美利坚合众国](../Page/美利坚合众国.md "wikilink")。
  - [1791年](../Page/1791年.md "wikilink")：美国将其新都命名为[华盛顿哥伦比亚特区](../Page/华盛顿哥伦比亚特区.md "wikilink")，以纪念其首位总统[乔治·华盛顿](../Page/乔治·华盛顿.md "wikilink")。

### 19世紀

  - [1850年](../Page/1850年.md "wikilink")：[加利福尼亚州加入](../Page/加利福尼亚州.md "wikilink")[美国](../Page/美国.md "wikilink")。
  - [1892年](../Page/1892年.md "wikilink")：[木卫五被发现](../Page/木卫五.md "wikilink")。
  - [1895年](../Page/1895年.md "wikilink")：。

### 20世紀

  - [1922年](../Page/1922年.md "wikilink")：[粤汉铁路工人罢工](../Page/粤汉铁路.md "wikilink")。
  - 1922年：[希土战争以](../Page/希土戰爭_\(1919年-1922年\).md "wikilink")[土耳其获胜告终](../Page/土耳其.md "wikilink")。
  - [1927年](../Page/1927年.md "wikilink")：[第一次國共內戰](../Page/第一次國共內戰.md "wikilink")：[中国共产党领导](../Page/中国共产党.md "wikilink")[湖南](../Page/湖南.md "wikilink")、[江西兩省邊界的工人](../Page/江西.md "wikilink")、农民发动[秋收起义](../Page/秋收起义.md "wikilink")。
  - [1930年](../Page/1930年.md "wikilink")：第一次國共內戰：中共决定成立[中华苏维埃共和国](../Page/中华苏维埃共和国.md "wikilink")。
  - 1930年：[阎锡山为首的](../Page/阎锡山.md "wikilink")[北平国民政府成立](../Page/北平国民政府.md "wikilink")。
  - [1942年](../Page/1942年.md "wikilink")：[第二次世界大戰](../Page/第二次世界大戰.md "wikilink")：[德国海军袖珍](../Page/德国海军.md "wikilink")[潛艇开始襲擊在](../Page/潛艇.md "wikilink")[大西洋航行的](../Page/大西洋.md "wikilink")[英](../Page/英國.md "wikilink")[美補給艦](../Page/美國.md "wikilink")。
  - [1944年](../Page/1944年.md "wikilink")：第二次世界大戰：[盟军占领](../Page/盟军.md "wikilink")[比利时全境](../Page/比利时.md "wikilink")，同日[保加利亞王國向盟軍投降](../Page/保加利亞王國.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：[中國抗日戰爭](../Page/中國抗日戰爭.md "wikilink")：[日本中国派遣军总司令](../Page/日本.md "wikilink")[冈村宁次在](../Page/冈村宁次.md "wikilink")[南京向](../Page/南京.md "wikilink")[中华民国陆军总司令](../Page/中华民国.md "wikilink")[何应钦递交投降书](../Page/何应钦.md "wikilink")，中国抗日战争结束。
  - [1948年](../Page/1948年.md "wikilink")：[朝鲜民主主义人民共和国在](../Page/朝鲜民主主义人民共和国.md "wikilink")[朝鲜半岛北部成立](../Page/朝鲜半岛.md "wikilink")，[金日成出任首任](../Page/金日成.md "wikilink")[总理](../Page/朝鲜总理.md "wikilink")。
  - [1948年](../Page/1948年.md "wikilink")：[香港工会联合会成立](../Page/香港工会联合会.md "wikilink")。
  - [1968年](../Page/1968年.md "wikilink")：[亚瑟·阿什赢得](../Page/亚瑟·阿什.md "wikilink")[美国网球公开赛男子单打冠军](../Page/美国网球公开赛.md "wikilink")，这是[黑人首次赢得男子网球大赛桂冠](../Page/黑人.md "wikilink")。
  - [1971年](../Page/1971年.md "wikilink")：英國歌手[約翰·藍儂的第二张专辑](../Page/約翰·藍儂.md "wikilink")－[想像](../Page/想像_\(專輯\).md "wikilink")（Imagine）在美國發行。
  - [1976年](../Page/1976年.md "wikilink")：[毛泽东先生逝世](../Page/毛泽东.md "wikilink")。
  - [1983年](../Page/1983年.md "wikilink")：[任天堂在](../Page/任天堂.md "wikilink")[紅白機上推出](../Page/紅白機.md "wikilink")《[超級瑪利歐兄弟](../Page/超級瑪利歐兄弟.md "wikilink")》。
  - [1986年](../Page/1986年.md "wikilink")：[中国国务院颁布](../Page/中国国务院.md "wikilink")[改革劳动制度的](../Page/改革劳动制度.md "wikilink")4项规定。
  - [1990年](../Page/1990年.md "wikilink")：美苏首脑在[赫尔辛基会晤](../Page/赫尔辛基.md "wikilink")。
  - [1991年](../Page/1991年.md "wikilink")：[蘇聯解體](../Page/蘇聯解體.md "wikilink")：[塔吉克斯坦宣布从](../Page/塔吉克斯坦.md "wikilink")[苏联独立](../Page/苏联.md "wikilink")。
  - [1992年](../Page/1992年.md "wikilink")：[香港](../Page/香港.md "wikilink")[青衣島一油庫發生輕微爆炸](../Page/青衣島.md "wikilink")，幸無人傷亡，但引起當地居民關注要求將油庫遠離民居。
  - [1995年](../Page/1995年.md "wikilink")：[第一代PlayStation在](../Page/PlayStation_\(遊戲機\).md "wikilink")[北美開始發售](../Page/北美.md "wikilink")。

### 21世紀

  - [2001年](../Page/2001年.md "wikilink")：[阿富汗](../Page/阿富汗.md "wikilink")[北方联盟领导人](../Page/北方聯盟_\(阿富汗\).md "wikilink")[马苏德在](../Page/艾哈迈德·沙阿·马苏德.md "wikilink")[塔哈尔省接受采访时遭到](../Page/塔哈尔省.md "wikilink")[自杀式炸弹袭击](../Page/自杀式恐怖袭击.md "wikilink")，后不治身亡。
  - [2006年](../Page/2006年.md "wikilink")：。
  - [2006年](../Page/2006年.md "wikilink")：[百萬人民倒扁運動在](../Page/百萬人民倒扁運動.md "wikilink")[台北市](../Page/台北市.md "wikilink")[凱達格蘭大道開始靜坐](../Page/凱達格蘭大道.md "wikilink")。
  - [2012年](../Page/2012年.md "wikilink")：[香港特別行政區立法會選舉](../Page/2012年香港立法會選舉.md "wikilink")。
  - [2016年](../Page/2016年.md "wikilink")：研究人員發現[長頸鹿至少應該區分為](../Page/長頸鹿.md "wikilink")[網紋長頸鹿等](../Page/網紋長頸鹿.md "wikilink")4個[物種](../Page/物種.md "wikilink")，彼此間的[基因差距極大](../Page/基因.md "wikilink")。

## 出生

  - [214年](../Page/214年.md "wikilink")：[奥勒良](../Page/奥勒良.md "wikilink")，[羅馬帝國皇帝](../Page/羅馬帝國.md "wikilink")（270年－275年）
  - [384年](../Page/384年.md "wikilink")：[弗拉維烏斯·奧古斯都·霍諾留](../Page/弗拉維烏斯·奧古斯都·霍諾留.md "wikilink")，[西羅馬帝國首任皇帝](../Page/西羅馬帝國.md "wikilink")（393年-423年）
  - [1349年](../Page/1349年.md "wikilink")：[阿爾布雷希特三世](../Page/阿爾布雷希特三世_\(奧地利\).md "wikilink")，[哈布斯堡王朝](../Page/哈布斯堡王朝.md "wikilink")[奧地利大公](../Page/奧地利.md "wikilink")（1365年－1395年）
  - [1466年](../Page/1466年.md "wikilink")：[足利義稙](../Page/足利義稙.md "wikilink")，日本[室町幕府第](../Page/室町幕府.md "wikilink")10代征夷大將軍（[1523年去世](../Page/1523年.md "wikilink")）
  - [1567年](../Page/1567年.md "wikilink")：[真田信繁](../Page/真田信繁.md "wikilink")，[日本](../Page/日本.md "wikilink")[戰國時代末期武將](../Page/戰國時代.md "wikilink")（[1615年去世](../Page/1615年.md "wikilink")）
  - [1585年](../Page/1585年.md "wikilink")：[黎塞留](../Page/黎塞留.md "wikilink")，[法国政治家](../Page/法国.md "wikilink")、樞機主教（[1642年去世](../Page/1642年.md "wikilink")）
  - [1737年](../Page/1737年.md "wikilink")：[路易吉·伽伐尼](../Page/路易吉·伽伐尼.md "wikilink")，[義大利醫生](../Page/義大利.md "wikilink")、物理學家、現代產科學先驅（[1798年去世](../Page/1798年.md "wikilink")）
  - [1789年](../Page/1789年.md "wikilink")：[威廉·邦德](../Page/威廉·邦德.md "wikilink")，[美国天文学家](../Page/美国.md "wikilink")，[哈佛大学天文台的首任台长](../Page/哈佛大学天文台.md "wikilink")（1859年去世）
  - [1828年](../Page/1828年.md "wikilink")：[列夫·托爾斯泰](../Page/列夫·托爾斯泰.md "wikilink")，[俄国小說家](../Page/俄国.md "wikilink")、評論家、劇作家和哲學家、非暴力基督教無政府主義者和教育改革家（[1910年去世](../Page/1910年.md "wikilink")）
  - [1887年](../Page/1887年.md "wikilink")：[阿尔夫·兰登](../Page/阿尔夫·兰登.md "wikilink")，美國銀行家、政治家，堪薩斯州州長（1933年-1937年）（[1987年去世](../Page/1987年.md "wikilink")）
  - [1890年](../Page/1890年.md "wikilink")：[哈兰德·桑德斯](../Page/哈兰德·桑德斯.md "wikilink")，[肯德基](../Page/肯德基.md "wikilink")（KFC）創始人（[1980年去世](../Page/1980年.md "wikilink")）
  - [1911年](../Page/1911年.md "wikilink")：[朱匯森](../Page/朱匯森.md "wikilink")，[中華民國教育人士](../Page/中華民國.md "wikilink")（2006年去世）
  - [1915年](../Page/1915年.md "wikilink")：[鹽田剛三](../Page/鹽田剛三.md "wikilink")，[日本知名的](../Page/日本.md "wikilink")[合氣道家](../Page/合氣道.md "wikilink")，也是養神館合氣道的創始人（[1994年去世](../Page/1994年.md "wikilink")）
  - [1918年](../Page/1918年.md "wikilink")：[奥斯卡·路易吉·斯卡尔法罗](../Page/奥斯卡·路易吉·斯卡尔法罗.md "wikilink")，[義大利戰後第](../Page/義大利.md "wikilink")9任總統（1992年至1999年）
  - [1920年](../Page/1920年.md "wikilink")：[冯康](../Page/冯康.md "wikilink")，[中國数学家及科學家](../Page/中國.md "wikilink")（1993年去世）
  - [1922年](../Page/1922年.md "wikilink")：[漢斯·德默爾特](../Page/漢斯·德默爾特.md "wikilink")，德國裔物理學家，諾貝爾物理學獎得獎者
  - [1922年](../Page/1922年.md "wikilink")：[沃里克·埃斯特万·克尔](../Page/沃里克·埃斯特万·克尔.md "wikilink")，[巴西農業工程師](../Page/巴西.md "wikilink")、遺傳學家、昆蟲學家
  - [1922年](../Page/1922年.md "wikilink")：[波琳·拜恩斯](../Page/波琳·拜恩斯.md "wikilink")，英國插畫家（2008年去世）\[1\]\[2\]
  - [1923年](../Page/1923年.md "wikilink")：[丹尼尔·卡尔顿·盖杜谢克](../Page/丹尼尔·卡尔顿·盖杜谢克.md "wikilink")，美国病毒学家（2008年去世）
  - [1925年](../Page/1925年.md "wikilink")：[張悅楷](../Page/張悅楷.md "wikilink")（楷叔），廣州粵語[話劇](../Page/話劇.md "wikilink")、[相聲演員和](../Page/相聲.md "wikilink")[說書藝人](../Page/說書.md "wikilink")（1997年去世）
  - [1934年](../Page/1934年.md "wikilink")：[王德輝](../Page/王德輝.md "wikilink")，[香港](../Page/香港.md "wikilink")[企業家](../Page/企業家.md "wikilink")（1990年失蹤至今）
  - [1941年](../Page/1941年.md "wikilink")：[丹尼斯·里奇](../Page/丹尼斯·里奇.md "wikilink")，美國電腦科學家，C語言和Unix作業系統開發者之一（[2011年去世](../Page/2011年.md "wikilink")）
  - [1941年](../Page/1941年.md "wikilink")：[奧蒂斯·雷丁](../Page/奧蒂斯·雷丁.md "wikilink")，美國靈魂樂歌手（[1967年去世](../Page/1967年.md "wikilink")）
  - [1947年](../Page/1947年.md "wikilink")：[任志剛](../Page/任志剛.md "wikilink")，前[香港金融管理局專員](../Page/香港金融管理局.md "wikilink")
  - [1947年](../Page/1947年.md "wikilink")：[弘兼憲史](../Page/弘兼憲史.md "wikilink")，日本漫畫家
  - [1949年](../Page/1949年.md "wikilink")：[苏西洛·班邦·尤多约诺](../Page/苏西洛·班邦·尤多约诺.md "wikilink")，[印度尼西亞第六任](../Page/印度尼西亞.md "wikilink")[總統](../Page/印度尼西亞總統.md "wikilink")（2004年10月—）
  - [1950年](../Page/1950年.md "wikilink")：[周松崗](../Page/周松崗.md "wikilink")，[香港交易及結算所有限公司主席](../Page/香港交易及結算所有限公司.md "wikilink")
  - [1951年](../Page/1951年.md "wikilink")：[亞歷山大·唐納](../Page/亞歷山大·唐納.md "wikilink")，[澳大利亞史上任期最長外交部長](../Page/澳大利亞.md "wikilink")
  - [1951年](../Page/1951年.md "wikilink")：[关菁](../Page/关菁.md "wikilink")，[香港甘草男演员](../Page/香港.md "wikilink")（2014年去世）
  - [1953年](../Page/1953年.md "wikilink")：[周玉蔻](../Page/周玉蔻.md "wikilink")，[台灣著名](../Page/台灣.md "wikilink")[媒體人](../Page/媒體人.md "wikilink")
  - [1957年](../Page/1957年.md "wikilink")：[鄭裕玲](../Page/鄭裕玲.md "wikilink")，香港資深女藝人
  - [1958年](../Page/1958年.md "wikilink")：[鄧藹霖](../Page/鄧藹霖.md "wikilink")，[香港主持人](../Page/香港.md "wikilink")
  - [1959年](../Page/1959年.md "wikilink")：[艾瑞克·塞拉](../Page/艾瑞克·塞拉.md "wikilink")，法國作曲家，[盧·貝松](../Page/盧·貝松.md "wikilink")「御用電影配樂師」
  - [1960年](../Page/1960年.md "wikilink")：[休·格兰特](../Page/休·格兰特.md "wikilink")，美國男演員
  - [1963年](../Page/1963年.md "wikilink")：[黃造時](../Page/黃造時.md "wikilink")，前香港女演员
  - [1963年](../Page/1963年.md "wikilink")：[羅伯托·多納多尼](../Page/羅伯托·多納多尼.md "wikilink")，意大利足球員，1990年代開拓美國足球先驅之一
  - [1966年](../Page/1966年.md "wikilink")：[亞當·山德勒](../Page/亞當·山德勒.md "wikilink")，美國演員、電影製作人、音樂家
  - [1967年](../Page/1967年.md "wikilink")：[阿克夏·庫馬](../Page/阿克夏·庫馬.md "wikilink")，印度演員，兩度獲印度電影觀眾獎
  - [1970年](../Page/1970年.md "wikilink")：[賴亞文](../Page/賴亞文.md "wikilink")，[中國女排運動員](../Page/中國女排.md "wikilink")
  - [1971年](../Page/1971年.md "wikilink")：[艾力克·斯通斯特里特](../Page/艾力克·斯通斯特里特.md "wikilink")，美國演員
  - [1972年](../Page/1972年.md "wikilink")：[夏利奧和](../Page/夏利奧.md "wikilink")[夏健龍兄弟](../Page/夏健龍.md "wikilink")，[Soler樂隊成員](../Page/Soler.md "wikilink")
  - [1975年](../Page/1975年.md "wikilink")：[陳少霞](../Page/陳少霞.md "wikilink")，香港女演員
  - [1975年](../Page/1975年.md "wikilink")：[飛勇](../Page/飛勇.md "wikilink")，美國籍[棒球運動員](../Page/棒球.md "wikilink")
  - [1975年](../Page/1975年.md "wikilink")：[麥可·布雷](../Page/麥可·布雷.md "wikilink")，[加拿大流行爵士樂歌手](../Page/加拿大.md "wikilink")、影視演員
  - [1976年](../Page/1976年.md "wikilink")：[松風雅也](../Page/松風雅也.md "wikilink")，[日本聲優](../Page/日本.md "wikilink")
  - [1976年](../Page/1976年.md "wikilink")：[洪書勤](../Page/洪書勤.md "wikilink")，[中華民國詩人](../Page/中華民國.md "wikilink")
  - [1976年](../Page/1976年.md "wikilink")：[阿基·里希拉赫蒂](../Page/阿基·里希拉赫蒂.md "wikilink")，[芬兰国家足球队員](../Page/芬兰国家足球队.md "wikilink")。
  - [1977年](../Page/1977年.md "wikilink")：[蔡貞安](../Page/蔡貞安.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1978年](../Page/1978年.md "wikilink")：[肖恩·巴蒂尔](../Page/肖恩·巴蒂尔.md "wikilink")，美國[籃球運動員](../Page/籃球.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[酒井若菜](../Page/酒井若菜.md "wikilink")，[日本女演員](../Page/日本.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[蜜雪兒·威廉絲](../Page/蜜雪兒·威廉絲.md "wikilink")，[美國演員](../Page/美國.md "wikilink")，2005年參演《[斷背山](../Page/斷背山.md "wikilink")》獲[奧斯卡最佳女配角獎提名](../Page/奧斯卡最佳女配角獎.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[徐智碩](../Page/徐智碩.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[胡定欣](../Page/胡定欣.md "wikilink")，[香港無綫電視演員](../Page/香港.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[朱莉·冈萨洛](../Page/朱莉·冈萨洛.md "wikilink")，美籍拉丁裔女演員
  - [1982年](../Page/1982年.md "wikilink")：[大塚爱](../Page/大塚爱.md "wikilink")，[日本女性創作歌手](../Page/日本.md "wikilink")，與[濱崎步](../Page/濱崎步.md "wikilink")、[倖田來未齊名](../Page/倖田來未.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[金晶和](../Page/金晶和.md "wikilink")，[韓國女藝人](../Page/韓國.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[利利亚纳·纳西尔](../Page/利利亚纳·纳西尔.md "wikilink")，[印尼女子](../Page/印尼.md "wikilink")[羽毛球运动员](../Page/羽毛球.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[丁一宇](../Page/丁一宇.md "wikilink")，[韓國演員](../Page/韓國.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[艾尚真](../Page/艾尚真.md "wikilink")，2008年[國際中華小姐競選季軍](../Page/國際中華小姐競選.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[劉佩玥](../Page/劉佩玥.md "wikilink")，香港女演員
  - [1991年](../Page/1991年.md "wikilink")；[亨特·海耶斯](../Page/亨特·海耶斯.md "wikilink")，美國[鄉村音樂](../Page/鄉村音樂.md "wikilink")[歌手和詞曲作者](../Page/歌手.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")；[王柏融](../Page/王柏融.md "wikilink")，台灣中華職棒桃猿隊選手
  - [2000年](../Page/2000年.md "wikilink")：[維多莉亞·德·馬利查勒](../Page/維多莉亞·德·馬利查勒.md "wikilink")，
    [西班牙貴族](../Page/西班牙.md "wikilink")
  - [2003年](../Page/2003年.md "wikilink")：[陳沛妍](../Page/陳沛妍.md "wikilink")，[香港童星](../Page/香港.md "wikilink")

## 逝世

  - [1087年](../Page/1087年.md "wikilink")：[威廉一世](../Page/威廉一世_\(英格蘭\).md "wikilink")，征服者威廉，[英格蘭國王](../Page/英格蘭.md "wikilink")、諾曼第公爵（[1028年出生](../Page/1028年.md "wikilink")）
  - [1487年](../Page/1487年.md "wikilink")：明宪宗[朱见深](../Page/朱见深.md "wikilink")，中國[大明第九代皇帝](../Page/明朝.md "wikilink")（1447年出生）
  - [1513年](../Page/1513年.md "wikilink")：[詹姆斯四世](../Page/詹姆斯四世_\(蘇格蘭\).md "wikilink")，[苏格兰国王](../Page/苏格兰.md "wikilink")（1473年出生）
  - [1732年](../Page/1732年.md "wikilink")：[蒋廷锡](../Page/蒋廷锡.md "wikilink")，中國[清代畫家](../Page/清代.md "wikilink")（1669年出生）
  - [1875年](../Page/1875年.md "wikilink")：[查理·義律](../Page/查理·義律.md "wikilink")，[英國](../Page/英國.md "wikilink")[駐華商務總監](../Page/駐華商務總監.md "wikilink")（[1801年出生](../Page/1801年.md "wikilink")）
  - [1901年](../Page/1901年.md "wikilink")：[亨利·德·土魯斯-羅特列克](../Page/亨利·德·土魯斯-羅特列克.md "wikilink")，巴黎画家（[1864年出生](../Page/1864年.md "wikilink")）
  - [1929年](../Page/1929年.md "wikilink")：[田中义一](../Page/田中义一.md "wikilink")，[日本](../Page/日本.md "wikilink")[內閣總理大臣](../Page/日本內閣總理大臣.md "wikilink")、陆军大将（1864年出生）
  - [1976年](../Page/1976年.md "wikilink")：[毛泽东](../Page/毛泽东.md "wikilink")，[中國共產黨領導人](../Page/中國共產黨.md "wikilink")、[中國人民解放軍領導人](../Page/中國人民解放軍.md "wikilink")，[中華人民共和國領導人](../Page/中華人民共和國.md "wikilink")（[1893年出生](../Page/1893年.md "wikilink")，参见：[毛泽东之死](../Page/毛泽东之死.md "wikilink")）
  - [2001年](../Page/2001年.md "wikilink")：[艾哈迈德·沙阿·马苏德](../Page/艾哈迈德·沙阿·马苏德.md "wikilink")，[阿富汗北方联盟主要军事领导人](../Page/阿富汗.md "wikilink")（[1953年出生](../Page/1953年.md "wikilink")）
  - [2007年](../Page/2007年.md "wikilink")：[徐四民](../Page/徐四民.md "wikilink")，前[全國政協常委](../Page/全國政協.md "wikilink")，香港《[鏡報](../Page/鏡報月刊.md "wikilink")》創辦人（1914年出生）
  - 2007年：[韓鼎祥](../Page/韓鼎祥.md "wikilink")，[河北省](../Page/河北省.md "wikilink")[天主教永年教區第二任主教](../Page/天主教永年教區.md "wikilink")（[1937年出生](../Page/1937年.md "wikilink")）

## 节假日和习俗

  - [朝鲜](../Page/朝鲜.md "wikilink")：国庆节
  - [中華民國](../Page/中華民國.md "wikilink")：體育節、律師節、狗狗節
  - [塔吉克](../Page/塔吉克斯坦.md "wikilink")：獨立日

## 參考資料

1.
2.