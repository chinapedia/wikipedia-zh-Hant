A列車（遊戲）}}

**A-train**（）是專指[日立製作所以](../Page/日立製作所.md "wikilink")[鋁合金大型](../Page/鋁合金.md "wikilink")[擠出加工型材而成的車輛系統](../Page/擠出加工.md "wikilink")，並以此來製造[鐵道車輛的商標](../Page/鐵道車輛.md "wikilink")，當中的「A」是指Advanced、Amenity、Ability、Aluminum。
[JR_West_683-0_Series_Kinkito_W31.jpg](https://zh.wikipedia.org/wiki/File:JR_West_683-0_Series_Kinkito_W31.jpg "fig:JR_West_683-0_Series_Kinkito_W31.jpg")

## 特徴

1.  採用[鋁](../Page/鋁.md "wikilink")[合金擠出加工大型材所成的](../Page/合金.md "wikilink")[雙皮層構造](../Page/雙皮層構造.md "wikilink")（Double
    skin structure）
      -
        最初鋁合金構體的做法是利用鋁合金薄板（外板）與框架互相熔接而成，基本上與鋼構體的構造相同。隨著補強材料與內部一體化後的大型材出現後，即以此開發成[單皮層構造](../Page/單皮層構造.md "wikilink")（Single
        skin structure），其好處是熔接成本可大幅減低，亦可令車身輕量化。
        其後有中空箱型斷面的鋁合金擠出加工大型材的出現，就出現了雙皮層構造（Double skin
        structure）。由於側面與天井面有二層構造的關係，車箱內的隔音效果已被提升，亦由於箱形內部有大量三角構架結構作支撐，車身強度在輕量化的同時得而保持。另外，由於型材的長度可達車卡的總長度關係，原材的數目可以減少之餘，溶接的成本更進一步下降。
2.  採用[摩擦攪拌接合工法](../Page/摩擦攪拌接合工法.md "wikilink")
      -
        由於使用摩擦攪拌接合（FSW）工法關係，與傳統高溫熔接工法如MIG法比較下，FSW溶接在材料接合部份因高熱引致的歪曲現象與接合強度上均優勝於傳統工法。即使構體不採用塗料也可以保持美觀。如[九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")（JR九州）的[815系](../Page/JR九州815系電力動車組.md "wikilink")、[817系](../Page/JR九州817系電力動車組.md "wikilink")，[東武鐵道的](../Page/東武鐵道.md "wikilink")[50000系](../Page/東武50000系電力動車組.md "wikilink")，[首都圈新都市鐵道的](../Page/首都圈新都市鐵道.md "wikilink")[TX-1000系](../Page/首都圈新都市鐵道TX-1000系電力動車組.md "wikilink")、[TX-2000系等都是不採用塗裝的](../Page/首都圈新都市鐵道TX-2000系電力動車組.md "wikilink")。\[1\]
        [JR-K_885-01.jpg](https://zh.wikipedia.org/wiki/File:JR-K_885-01.jpg "fig:JR-K_885-01.jpg")\]\]
3.  採用自立式内裝構造
      -
        傳統鐵路車輛的內裝是從完成的構體的內側以[螺絲釘裝上鋁面板或強化纖維面板](../Page/螺絲.md "wikilink")（FRP），由於零部件數量極多，在正常維修時需用很多的時間；另外，由於必須要等待車輛構體完成後才可開始進行內裝工程，在將車輛建造的總時間縮短的前提下，內裝工程便成了一個限制關卡。
        對此，A-train採用了可自立的內裝，以獨立模組（module）方式製作，完成後於構體的端部推入車體內安裝。由於車身構體內設有支持導軌的關係，在安裝時只需將組件放入導軌推入至指定位置即成。由於在固定模組所需的部件得以大幅減少，整體上安裝成本可以因此而削減。而由於內裝可以獨立地於不同工廠內製作的關係，車輛組合的工時亦可大幅縮短。
        最後，由於使用了[模組化的方式](../Page/模組化.md "wikilink")，不論是操縱台、中長距離列車所需的[廁所](../Page/廁所.md "wikilink")、[餐廳等設施亦可以兼顧](../Page/餐廳.md "wikilink")。列車內部設計的自由度亦增加不少。
        簡單來說，就是以「[樂高積木](../Page/樂高積木.md "wikilink")」的方式將所需的部分以標準化的大小製作，然後裝上指定的地方。

## 現役中的A-train

[SentosaExpress_Purple.JPG](https://zh.wikipedia.org/wiki/File:SentosaExpress_Purple.JPG "fig:SentosaExpress_Purple.JPG")的单轨列车\]\]

  - [JR九州815系電力動車組](../Page/JR九州815系電力動車組.md "wikilink")
  - [JR九州817系電力動車組](../Page/JR九州817系電力動車組.md "wikilink")
  - [JR九州885系電力動車組](../Page/JR九州885系電力動車組.md "wikilink")
  - [JR西日本683系電力動車組](../Page/JR西日本683系電力動車組.md "wikilink")
  - [JR東日本E257系電聯車](../Page/JR東日本E257系電聯車.md "wikilink")
  - [東武50000系電力動車組](../Page/東武50000系電力動車組.md "wikilink")（包含50050系）
  - [西武20000系電力動車組](../Page/西武20000系電力動車組.md "wikilink")
  - [西武30000系電力動車組](../Page/西武30000系電力動車組.md "wikilink")
  - [首都圈新都市鐵道TX-1000系電力動車組](../Page/首都圈新都市鐵道TX-1000系電力動車組.md "wikilink")
  - [首都圈新都市鐵道TX-2000系電力動車組](../Page/首都圈新都市鐵道TX-2000系電力動車組.md "wikilink")

[Unit_395008_at_Ebbsfleet_International.JPG](https://zh.wikipedia.org/wiki/File:Unit_395008_at_Ebbsfleet_International.JPG "fig:Unit_395008_at_Ebbsfleet_International.JPG")\]\]

  - [東葉高速鐵道2000系電力動車組](../Page/東葉高速鐵道2000系電力動車組.md "wikilink")
  - [東京地下鐵05系電力動車組](../Page/東京地下鐵05系電力動車組.md "wikilink")（第40編成以後）
  - [東京地下鐵10000系電力動車組](../Page/東京地下鐵10000系電力動車組.md "wikilink")
  - [阪急9300系電力動車組](../Page/阪急9300系電力動車組.md "wikilink")
  - [阪急9000系電力動車組](../Page/阪急9000系電力動車組.md "wikilink")
  - [福岡市交通局3000系電力動車組](../Page/福岡市交通局3000系電力動車組.md "wikilink")
  - [台鐵TEMU1000型電聯車](../Page/台鐵TEMU1000型電聯車.md "wikilink")（太鲁阁号,[885系的後代](../Page/JR九州885系電力動車組.md "wikilink")）

[TRA_TED1002_at_Hualien_Station_20080523.jpg](https://zh.wikipedia.org/wiki/File:TRA_TED1002_at_Hualien_Station_20080523.jpg "fig:TRA_TED1002_at_Hualien_Station_20080523.jpg")（太鲁阁号）\]\]

  - 英國CTRL（Channel Tunnel Rail
    Link）段所用的[英铁395型电力动车组列車](../Page/英铁395型电力动车组.md "wikilink")

<!-- end list -->

  - 新加坡[圣淘沙捷运单轨列车](../Page/圣淘沙捷运.md "wikilink")

<!-- end list -->

  - 中国[重庆轨道交通单轨列车](../Page/重庆轨道交通.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [HITACHI 日立評論
    最近の鉄道車両“A-train”](https://web.archive.org/web/20051226111708/http://www.hitachi.co.jp/Sp/TJ/2003/hrnaug03/hrn08a02.html)

[Category:鋁合金](../Category/鋁合金.md "wikilink")
[Category:日本鐵路車輛](../Category/日本鐵路車輛.md "wikilink")
[Category:鐵路車輛工學](../Category/鐵路車輛工學.md "wikilink")

1.  熔接指將金屬加熱至超過沸點，溶接則只是加熱至熔點。