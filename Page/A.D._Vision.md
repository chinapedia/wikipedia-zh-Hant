**A.D. Vision**（簡稱
**ADV**）是一家曾经存在的[美国國際性多媒體娛樂公司](../Page/美国.md "wikilink")，總部設於[美國](../Page/美國.md "wikilink")[德克薩斯州的](../Page/德克薩斯州.md "wikilink")[休士頓](../Page/休士頓.md "wikilink")。主要業務包括：[家庭影片製作及發行](../Page/家庭影片.md "wikilink")、[電視廣播](../Page/電視廣播.md "wikilink")、[電影發行](../Page/電影發行.md "wikilink")、[營銷](../Page/營銷.md "wikilink")、[創作](../Page/創作.md "wikilink")、[漫畫出版](../Page/漫畫.md "wikilink")、及[雜誌出版](../Page/雜誌.md "wikilink")。該公司曾经是[北美洲最大家的](../Page/北美洲.md "wikilink")[日本動畫代理公司](../Page/日本動畫.md "wikilink")。

公司創立於1992年，創辦人是日本動畫迷[John
Ledford和](../Page/John_Ledford.md "wikilink")[Matt
Greenfield](../Page/Matt_Greenfield.md "wikilink")。公司自創立以來，規模越做越大，開設有多間分公司，旗下擁有不少物業、創作產權與及發行權。其中最出名的發行品，可能是自[GAINAX手上買回來](../Page/GAINAX.md "wikilink")，除澳洲及亞洲外，全球所有地方的《[新世紀福音戰士](../Page/新世紀福音戰士.md "wikilink")》動畫發行權。到結業為止，公司已在[北美](../Page/北美洲.md "wikilink")、[歐洲及](../Page/歐洲.md "wikilink")[亞洲均設有支部辦公室](../Page/亞洲.md "wikilink")。

公司聲稱，在1990年代中期，公司名字裡的「A.D.」的意思是「[機密](../Page/機密.md "wikilink")」。不過實際上，「A.D.」只是「Animation
Dubbing（動畫譯制）」的簡寫而已，可見於他們早期的發行品定位。他們最新的買回來的發行權是來自[Sunrise的](../Page/Sunrise.md "wikilink")《[Keroro軍曹](../Page/Keroro軍曹.md "wikilink")》，而該片在英文世界發行時的正式名稱是《SGT.
FROG》（青蛙[中士](../Page/中士.md "wikilink")）。

由於自2004年起日本動畫市場萎縮，該公司在2007年就已傳出陷入經營危機，於2009年9月1日宣佈出售資產、結束營業，旗下大部分作品发行权于2008年转手[Funimation
Entertainment](../Page/Funimation_Entertainment.md "wikilink")。

## 子公司

### ADV Films

**ADV Films**是**A.D.
Vision**創始的骨幹部份，主要業務是發行家庭影片，特別是[日本動畫及](../Page/日本動畫.md "wikilink")[特攝片](../Page/特攝片.md "wikilink")。

在最初，他們發行的日本動畫，只做到[日語原聲](../Page/日語.md "wikilink")，配以英文[字幕](../Page/字幕.md "wikilink")。發展幾年之後，他們開始建立自己的[配音班底](../Page/配音.md "wikilink")，為日本動畫配音。由於那時A.D.
Vision沒有自己的[錄音場地](../Page/錄音.md "wikilink")，他們只能租用附近的[錄音室](../Page/錄音室.md "wikilink")。這造成不少限制，比方說，由於錄音室是時租的付費，要是[配音員遲到或缺席](../Page/配音員.md "wikilink")，便得有人頂上，以免白白花錢了——這解釋了為甚麼早期的配音裡頭，公司老闆[Matt
Greenfield經常充當一些次要角色的配音員](../Page/Matt_Greenfield.md "wikilink")。事實上，英文版的《[魔物獵人妖子](../Page/魔物獵人妖子.md "wikilink")》（Devil
Hunter Yohko）、《[無限地帶](../Page/無限地帶.md "wikilink")》（Megazone
23）、《[潮與虎](../Page/潮與虎.md "wikilink")》（Ushio and
Tora）、《[帝國母艦](../Page/帝國母艦.md "wikilink")》（Ellcia）、《[妖世紀水滸傳](../Page/妖世紀水滸傳.md "wikilink")》（Suikoden:
Demon Century）、《[種子特務](../Page/種子特務.md "wikilink")》（Blue
Seed）、《[泡泡糖危機](../Page/泡泡糖危機.md "wikilink")》（Bubble Gum
Crisis）、《[青空少女隊](../Page/青空少女隊.md "wikilink")》（801 T.T.S.
Airbats）、《[美少女槍神](../Page/美少女槍神.md "wikilink")》（Gun Smith
Cats）、《[聖戰武士](../Page/聖戰武士.md "wikilink")》（Legend of
Crystania）、《[魔法獵人](../Page/魔法獵人.md "wikilink")》（Sorcerer
Hunters）、《[古怪獵人](../Page/古怪獵人.md "wikilink")》（Those Who Hunt
Elves）、《[科幻特攻](../Page/科幻特攻.md "wikilink")》（Burn Up
W）、《[魔術師歐菲](../Page/魔術師歐菲.md "wikilink")》（Sorcerous
Stabber Orphen）、《[女棒甲子園](../Page/女棒甲子園.md "wikilink")》（Princess
Nine）、《[鋼鐵天使](../Page/鋼鐵天使.md "wikilink")》（Steel Angel
Kurumi）、《[女惡魔人](../Page/女惡魔人.md "wikilink")》（The Devil
Lady）、《[娜姬卡電擊大作戰](../Page/娜姬卡電擊大作戰.md "wikilink")》（Najica
Blitz Tactics）、《[辣妹掌門人](../Page/辣妹掌門人.md "wikilink")》（Super
GALS\!）、《[疾風境界](../Page/疾風境界.md "wikilink")》（Final Fantasy:
Unlimited）、《[白雪戰士](../Page/白雪戰士.md "wikilink")》（Pretear）、《[家有MISS](../Page/家有MISS.md "wikilink")》（Happy
Lesson）、《[變種危機](../Page/變種危機.md "wikilink")》（I Wish You Were
Here）、《[重力王](../Page/重力王.md "wikilink")》（Gravion）、《[愛的魔法](../Page/愛的魔法.md "wikilink")》（Maburaho）、甚至《[新世紀福音戰士](../Page/新世紀福音戰士.md "wikilink")》的部份角色都由[Greenfield配音](../Page/Matt_Greenfield.md "wikilink")——雖然有時，他負責的配音工作其實是他自己喜好。[Greenfield配音的角色](../Page/Matt_Greenfield.md "wikilink")，會以假名[Brian
Granveldt為名](../Page/Matt_Greenfield.md "wikilink")。

### Soft Cel Pictures

### Newtype USA

日本月刊《[Newtype](../Page/月刊Newtype.md "wikilink")》英文版的出版公司，在2008年2月號後廢刊。

### Anime Network

### ADV Manga

### ADV Music

### ADV Pro

### ADV Toys

## Section23 Films

**Section23 Films**公司的前身即是**A.D. Vision**公司。

ADV原本名為Gamtronix，專作進口日本電玩的生意，在休士頓的一家小店門口販售。後來在因緣際會下，買了改編自日本電玩遊戲的動畫《魔物獵人妖子》(英文:Devil
Hunter
Yohko；日語:魔物ハンター妖子)的動畫代理權，日本於1990年推出第1集動畫OVA，陸續製作五集。ADV拿下OVA代理權加上後製配音等工作，花費五萬五千美元，卻在發售後短短九十天內賺回這些錢。

嘗到甜頭的ADV從此不作電玩進口，只專心代理日本動畫。很聰明地，為了降低成本，他們一開始鎖定日本動畫小眾向的客戶，也就是當時已經默默存在的少數日本動漫迷，以低價代理日本作品，並請業餘聲優配音成英文版，還不用處理行銷廣告費用。如此一來，不用賣太多就可以賺錢。ADV以這個獲利公式，逐步擴展它的事業版圖，2005年的時候，ADV已經擁有自己的電視台和行銷通路，總營業額達到五千萬美元，其中90%獲利來自動畫DVD的銷售量。專門代理日本動畫的ADV公司其年的發行，在2004年就已經超過時代華納公司及派拉蒙影視公司，美國前兩名影劇發行公司的總和。

A.D.
Vision公司在全盛時期，將業務擴展至澳洲、歐洲、亞洲地區，曾經擁有數百部日本動畫作品和電影在歐、美、澳地區的DVD代理版權，且擁有自家的英文配音工作室，曾經是日本動畫在西方市場最大的代理商，也是日本御宅族文化在歐美澳最有力的幕後推手。

[世界金融危機加上](../Page/2007年－2011年環球金融危機.md "wikilink")2004年後網路資源日漸犯濫，公司2007年開始傳出財務危機，2009年9月1日正式宣佈倒閉，旗下股份和影視版權部分交由其他代理商收購，例如北美動畫代理商Funimation在此時，拿下了許多日本動畫版權。A.D.
Vision公司倒閉後，隨後設立**Section23 Films**新公司。

Section23 Films公司由「Sentai Filmworks」和「AEsir Holdings」兩間公司控股。Section23
Films和A.D. Vision公司的總辦事處皆是設立在美國德克薩斯州的休士頓。2009年後，Section23 Films和Sentai
Filmworks乃維持合作夥伴的關係，繼續發行娛樂影音產品。

2012年，動畫代理商Sentai Filmworks迅速成為在北美知名授權以及發行日本動畫的獲益最大的公司實體。Sentai
Filmworks
在2011年向北美零售業市場輸出了許多的最新的日本動畫(不包括重製版動畫)。這個公司積極主動、迎合日本季度動畫的受權行為已經收穫了許多動畫的版權。

目前，西方市場最大的動畫代理商為Funimation公司，動畫代理方面的主要招牌是龍珠系列，以及其他日本超人氣動畫和人氣漫畫改編的動畫。一線版權費較吃重，回收時間長，Funimation常以低價折扣策略，旗下DVD/BD在亞馬遜銷售換取現金流。Sentai
Filmworks旗下動畫DVD還是鎖定小眾向消費者，不廣告不行銷，粉絲之間口耳相傳，DVD/BD在亞馬遜的售價策略比Funimation高上許多。

### 發行動畫

  - 1988: MOV - [螢火蟲之墓](../Page/螢火蟲之墓.md "wikilink")（）
  - 1990: TVA - [冒險少女娜汀亞](../Page/冒險少女娜汀亞.md "wikilink")(又譯:海底兩萬里)
  - 1993: TVA - [GS美神](../Page/GS美神.md "wikilink")（）
  - 1995: TVA - [新世紀福音戰士](../Page/新世紀福音戰士.md "wikilink")（）
  - 1998: TVA - [魔術士歐菲](../Page/魔術士歐菲.md "wikilink")（）
  - 1999: OVA - [恐怖寵物店](../Page/恐怖寵物店.md "wikilink")（）
  - 2000: TVA - [櫻花大戰](../Page/櫻花大戰.md "wikilink")
  - 2001: TVA - [黑色天使/黑街二人組/Noir](../Page/Noir.md "wikilink")（）
  - 2001: TVA - [逮捕令SS](../Page/逮捕令_\(動漫\).md "wikilink")（）
  - 2002: TVA - [笑園漫畫大王](../Page/笑園漫畫大王.md "wikilink")（）
  - 2002: TVA - [閃靈二人組](../Page/閃靈二人組.md "wikilink")（）
  - 2003: TVA - [聖槍修女/摩登大法師/Chrono
    Crusade](../Page/聖槍修女.md "wikilink")（）
  - 2003: MOV - [星之聲](../Page/星之聲.md "wikilink")
  - 2003: TVA - [月姬](../Page/月姬.md "wikilink")
  - 2004: MOV - [蘋果核戰/APPLESEED](../Page/蘋果核戰.md "wikilink")（）
  - 2004: TVA - [異域天使](../Page/異域天使.md "wikilink")（）
  - 2004: OVA - [柯塞特的肖像](../Page/柯塞特的肖像.md "wikilink")（）
  - 2004: TVA - [薔薇少女](../Page/薔薇少女.md "wikilink")
  - 2005: TVA - [光速蒙面俠21](../Page/光速蒙面俠21.md "wikilink")（）
  - 2005: TVA - [玻璃假面](../Page/玻璃假面.md "wikilink")（）
  - 2006: TVA - [銀魂](../Page/銀魂_\(動畫\).md "wikilink")
  - 2006: TVA - [地獄少女 二籠](../Page/地獄少女.md "wikilink")
  - 2007: TVA - [怪物王女](../Page/怪物王女.md "wikilink")（）
  - 2007: TVA - [逮捕令3 全速前進](../Page/逮捕令_\(動漫\).md "wikilink")（）
  - 2007: TVA - [BLUE DROP/天使們的戲曲](../Page/BLUE_DROP.md "wikilink")（）
  - 2007: TVA - [神靈狩/GHOST
    HOUND](../Page/神靈狩/GHOST_HOUND.md "wikilink")（）
  - 2007: TVA - [向陽素描](../Page/向陽素描.md "wikilink")（）
  - 2007: TVA - [鬼面騎士](../Page/鬼面騎士.md "wikilink")（）
  - 2008: TVA - [地獄少女 三鼎](../Page/地獄少女.md "wikilink")
  - 2008: TVA - [骷髏13/Golgo 13](../Page/骷髏13.md "wikilink")（）
  - 2008: TVA - [死後文](../Page/死後文.md "wikilink")（）
  - 2008: TVA - [北鬥神拳 天之霸王](../Page/北鬥神拳.md "wikilink")（）
  - 2008: TVA - [向陽素描×365](../Page/向陽素描×365.md "wikilink")（）
  - 2008: TVA - [泰坦尼亞](../Page/泰坦尼亞.md "wikilink")（）
  - 2008: ONA - [亡念之扎姆德](../Page/亡念之扎姆德.md "wikilink")（）
  - 2008: TVA - [艾莉森與莉莉亞](../Page/艾莉森與莉莉亞.md "wikilink")
  - 2009: TVA - [迦南/CANAAN](../Page/CANAAN.md "wikilink")（）
  - 2009: TVA - [東京地震8.0](../Page/東京地震8.0.md "wikilink")
  - 2009: TVA - [花冠之淚/Tears to Tiara](../Page/花冠之淚.md "wikilink")（）
  - 2009: TVA - [貓願三角戀](../Page/貓願三角戀.md "wikilink")（）
  - 2009: TVA - [大正野球娘](../Page/大正野球娘.md "wikilink")
  - 2009: TVA - [豹頭王傳說/Guin
    Saga](../Page/豹頭王傳說/Guin_Saga.md "wikilink")（）
  - 2009: TVA - [NEEDLESS/超能力大戰](../Page/NEEDLESS.md "wikilink")
  - 2009: TVA - [瑪利亞狂熱](../Page/瑪利亞狂熱.md "wikilink")（）
  - 2009: MOV - [The Asylum
    Session](../Page/The_Asylum_Session.md "wikilink")（）
  - 2009: TVA - [戰鬥司書系列](../Page/戰鬥司書系列.md "wikilink")（）
  - 2010: TVA - [Angel Beats\!](../Page/Angel_Beats!.md "wikilink")（）
  - 2011: TVA - [閃光的夜襲](../Page/閃光的夜襲.md "wikilink")（）
  - 2011: TVA - [未來都市NO.6](../Page/未來都市NO.6.md "wikilink")（）
  - 2011: MOV - [永久之久遠](../Page/永久之久遠.md "wikilink")
  - 2011: MOV - [破刃之劍](../Page/破刃之劍.md "wikilink")（）
  - 2011: TVA - [UN-GO](../Page/UN-GO.md "wikilink")
  - 2011: TVA - [女神異聞錄4](../Page/女神異聞錄4.md "wikilink")（）
  - 2011: MOV - [追逐繁星的孩子](../Page/追逐繁星的孩子.md "wikilink")
  - 2012: TVA - [Another](../Page/Another.md "wikilink")
  - 2012: MOV - [PLANZET](../Page/PLANZET.md "wikilink")
  - 2012: MOV - [Fate/Stay Night: Unlimited Blade
    Works](../Page/Fate/stay_night.md "wikilink")
  - 2012: TVA - [K-ON！輕音少女](../Page/K-ON！輕音部.md "wikilink")（第二季）
  - 2012: OVA - [女王之刃](../Page/女王之刃.md "wikilink")（女王之刃叛亂系列OVA）
  - 2012: TVA - [釣球](../Page/釣球.md "wikilink")（）
  - 2012: TVA - [Tari Tari](../Page/Tari_Tari.md "wikilink")
  - 2012: TVA - [人類衰退之後](../Page/人類衰退之後.md "wikilink")（）
  - 2012: TVA - [宇宙兄弟](../Page/宇宙兄弟.md "wikilink")
  - 2013: MOV - [言葉之庭](../Page/言葉之庭.md "wikilink")

<!-- end list -->

  -
    ※部分承接ADV時代作品

## 相關連結

  - [ADV Films
    官方網頁](https://web.archive.org/web/20140101054539/http://www.advfilms.com/)
  - [Sentai Filmworks 官方網頁](http://www.sentai-filmworks.com/)

[Category:動畫產業公司](../Category/動畫產業公司.md "wikilink")
[Category:美國已結業公司](../Category/美國已結業公司.md "wikilink")
[Category:1992年成立的公司](../Category/1992年成立的公司.md "wikilink")
[Category:2009年結業公司](../Category/2009年結業公司.md "wikilink")