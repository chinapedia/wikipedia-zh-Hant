**A/a**
是[拉丁字母的首](../Page/拉丁字母.md "wikilink")[字母](../Page/字母.md "wikilink")，来源于[拉丁语中有](../Page/拉丁语.md "wikilink")[低元音](../Page/開元音.md "wikilink")和音值的[希腊字母](../Page/希腊字母.md "wikilink")。其可从[伊特鲁里亚语和希腊语α追溯到](../Page/伊特鲁里亚语.md "wikilink")[闪含语的](../Page/闪含语.md "wikilink")'âlep（用于[声门塞音](../Page/声门塞音.md "wikilink")）。其希腊名源自闪含语，而闪含语是因[希伯来语而闻名的](../Page/希伯来语.md "wikilink")。[古英语的](../Page/古英语.md "wikilink")和音变为现代的和，前者原由a和e连写的æ表示。在其他语言（例如[意大利语](../Page/意大利语.md "wikilink")、[西班牙语和](../Page/西班牙语.md "wikilink")[德语](../Page/德语.md "wikilink")）中，A的音也与拉丁语相同。

可以确定A最早源于[腓尼基字母表的第一个字母](../Page/腓尼基字母.md "wikilink")[Aleph](../Page/Aleph.md "wikilink")\[1\]（腓尼基字母表完全由辅音组成，是[辅音音素文字](../Page/辅音音素文字.md "wikilink")）。而Aleph的来源则可能是表示牛头的[埃及圣书体](../Page/埃及圣书体.md "wikilink")。

| 埃及语                                                                                                                      | 腓尼基文 *[aleph](../Page/aleph.md "wikilink")*                                                      | 希腊语 *[Alpha](../Page/Α.md "wikilink")*                                                        | 伊特鲁里亚语 A                                                                        | 罗马语/闪族语 A                                                                 |
| ------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------- | ------------------------------------------------------------------------- |
| [Egyptian hieroglyphic ox head](https://zh.wikipedia.org/wiki/File:EgyptianA-01.svg "fig:Egyptian hieroglyphic ox head") | [Phoenician aleph](https://zh.wikipedia.org/wiki/File:PhoenicianA-01.svg "fig:Phoenician aleph") | [Alpha_uc_lc.svg](https://zh.wikipedia.org/wiki/File:Alpha_uc_lc.svg "fig:Alpha_uc_lc.svg") | [Etruscan A](https://zh.wikipedia.org/wiki/File:EtruscanA.svg "fig:Etruscan A") | [Roman A](https://zh.wikipedia.org/wiki/File:RomanA-01.svg "fig:Roman A") |

在电脑使用的[ASCII中](../Page/ASCII.md "wikilink")，大写用编码65，小写用编码97。

在[演绎法中](../Page/演绎推理.md "wikilink")，字母A表作形如“甲均是乙”的全肯定命题。字母I、E和O表形如“一些甲是乙”的部分肯定，以及一些形如“一些甲非乙”的部分否定命题。以上可能来自拉丁动词affirmo（“我断言”）与nego（“我否认”）。这种用法可溯至13世纪，虽然一些学者认为来源是[希腊](../Page/希腊.md "wikilink")[逻辑学家](../Page/逻辑学家.md "wikilink")。

上述用法也有应用在[数学和](../Page/数学.md "wikilink")[逻辑学中](../Page/逻辑.md "wikilink")，符号“∀”（反转的字母A），表示“（对）任意……”。例如：\(\forall x, x+x=2x\)。

在[十二小时制](../Page/十二小时制.md "wikilink")，a可代替午前（ante），用于表示[上午的时间段](../Page/上午.md "wikilink")。
同時Ａ也是英文的第一個字母。

## 字符编码

| 字符 编码                              | [ASCII](../Page/ASCII.md "wikilink") | [Unicode](../Page/Unicode.md "wikilink") | [EBCDIC](../Page/EBCDIC.md "wikilink") | [摩斯电码](../Page/摩尔斯电码.md "wikilink") |
| ---------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- | ----------------------------------- |
| [大写](../Page/大寫字母.md "wikilink") A | 65                                   | 0041                                     | 193                                    | `·-`                                |
| [小写](../Page/小寫字母.md "wikilink") a | 97                                   | 0061                                     | 129                                    |                                     |

## 其他表示法

## 参看

### a的变体

  - 字母

  - [開後不圓唇元音](../Page/開後不圓唇元音.md "wikilink")

  - [開後圓唇元音](../Page/開後圓唇元音.md "wikilink")

  - [次開央元音](../Page/次開央元音.md "wikilink")

  - [∀](../Page/∀.md "wikilink")（数学上“对任意的”意思）

  - [@](../Page/@.md "wikilink")（at符號）

### 其他字母中的相近字母

  - （[希腊字母](../Page/希腊字母.md "wikilink") Alpha）

  - （[西里尔字母](../Page/西里尔字母.md "wikilink") A）

  - a for shuxing

  - b is also for shuxing

## 另見

  -
## 参考文献

{{-}}

[Category:拉丁字母](../Category/拉丁字母.md "wikilink")

1.  "A", "Encyclopædia Britannica", Volume 1, 1962. p.1.