**A3冠軍盃**（一般名稱：**A3聯賽**、**A3俱樂部冠軍盃**，[英文](../Page/英文.md "wikilink")：**A3
Champions Cup**）亦可稱為**東亞俱樂部冠軍盃**（**East Asian Champions
Cup**），是一項由[東亞足球協会於](../Page/東亞足球協会.md "wikilink")2003年開始一年一度舉辦的盃賽，並由[中國](../Page/中國.md "wikilink")，[日本及](../Page/日本.md "wikilink")[韓國三國輪流主辦](../Page/韓國.md "wikilink")，是項盃賽的舉辦主要目的是加強交流，促進三國足球水平的發展。這三个東亞國家的所屬聯賽冠軍及盃賽冠軍參與的小型足球錦標賽，合共有四支球隊參加。現時以韓國球会成績最佳共三次奪得冠軍、日本球会亦一次奪得冠軍，而中國球会的最佳成績則是[上海申花於](../Page/上海申花.md "wikilink")2007年奪得该项赛事的冠军。由2007年開始還增加了U-12、U-14和U-17三个級別的比赛，以培养三国的青少年對[足球的興趣](../Page/足球.md "wikilink")。2008年9月23日，[A3联盟在](../Page/A3联盟.md "wikilink")[北京宣布](../Page/北京.md "wikilink")，由于赞助承包商韩国现代体育公司破产，此项赛事将无限期中断。\[1\]

## 命名

大会名称是以**Asia三国**的含义下，命名为 A3；确定真正的俱乐部冠军为宗旨，命名为**A3冠军盃**。

## 參賽資格

參賽球会必須是[中國](../Page/中國.md "wikilink")、[日本及](../Page/日本.md "wikilink")[韓國的頂級聯賽](../Page/韓國.md "wikilink")（[中國足球超級聯賽](../Page/中國足球超級聯賽.md "wikilink")、[日本職業足球聯賽及](../Page/日本職業足球聯賽.md "wikilink")[韓國職業足球聯賽](../Page/韓國職業足球聯賽.md "wikilink")）的應屆冠軍及主办国的盃賽（[中國足協盃](../Page/中國足協盃.md "wikilink")、[日本聯賽盃](../Page/日本聯賽盃.md "wikilink")、[韓國足总盃](../Page/韓國足总盃.md "wikilink")）應屆冠軍。如果盃賽及頂級聯賽冠軍由同一支球会奪得，則由頂級聯賽的亞軍頂上。

## 賽制

每屆主辦國都拥有兩個席位（聯賽冠軍和盃賽冠軍）加上其餘兩個國家的聯賽冠軍，比賽以[單循環賽賽制進行](../Page/循環賽.md "wikilink")，每場勝方者可得三分，和局者則得一分，敗方者則得零分，完成所有比賽後總積分最高者可以成為冠軍，如果兩隊出現同樣分數，便按照以下的順序排定排名：淨勝球和總進球數。

## 獎金

  - 冠軍可獲得40萬[美元](../Page/美元.md "wikilink")
  - 亞軍可獲得20萬[美元](../Page/美元.md "wikilink")
  - 季軍可獲得15萬[美元](../Page/美元.md "wikilink")
  - 殿軍（即是第四名）可獲得10萬[美元](../Page/美元.md "wikilink")

## 歷屆賽事

<table style="width:21%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 5%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>主辦國</p></th>
<th></th>
<th><p>冠軍</p></th>
<th><p>亞軍</p></th>
<th><p>季軍</p></th>
<th><p>殿軍</p></th>
<th></th>
<th><p>參賽隊數</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2003<br />
<em><a href="../Page/2003年A3冠軍盃.md" title="wikilink">詳細</a></em></p></td>
<td><p><a href="../Page/東京.md" title="wikilink">東京</a></p></td>
<td><p><br />
<strong><a href="../Page/鹿島鹿角.md" title="wikilink">鹿島鹿角</a></strong></p></td>
<td><p><br />
<a href="../Page/大連實德足球俱樂部.md" title="wikilink">大連實德</a></p></td>
<td><p><br />
<a href="../Page/城南足球俱樂部.md" title="wikilink">城南一和天馬</a></p></td>
<td><p><br />
<a href="../Page/磐田山葉.md" title="wikilink">磐田山葉</a></p></td>
<td><p>4</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004<br />
<em><a href="../Page/2004年A3冠軍盃.md" title="wikilink">詳細</a></em></p></td>
<td><p><a href="../Page/上海.md" title="wikilink">上海</a></p></td>
<td><p><br />
<strong><a href="../Page/城南足球俱樂部.md" title="wikilink">城南一和天馬</a></strong></p></td>
<td><p><br />
<a href="../Page/橫濱水手.md" title="wikilink">橫濱水手</a></p></td>
<td><p><br />
<a href="../Page/上海綠地申花足球俱樂部.md" title="wikilink">上海申花</a></p></td>
<td><p><br />
<a href="../Page/北京人和足球俱樂部.md" title="wikilink">上海國際</a></p></td>
<td><p>4</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005<br />
<em><a href="../Page/2005年A3冠軍盃.md" title="wikilink">詳細</a></em></p></td>
<td><p><a href="../Page/西歸浦.md" title="wikilink">西歸浦</a></p></td>
<td><p><br />
<strong><a href="../Page/水原三星藍翼足球俱樂部.md" title="wikilink">水原三星藍翼</a></strong></p></td>
<td><p><br />
<a href="../Page/浦項制鐵足球俱樂部.md" title="wikilink">浦項制鐵</a></p></td>
<td><p><br />
<a href="../Page/橫濱水手.md" title="wikilink">橫濱水手</a></p></td>
<td><p><br />
<a href="../Page/深圳市足球俱樂部.md" title="wikilink">深圳健力寶</a></p></td>
<td><p>4</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006<br />
<em><a href="../Page/2006年A3冠軍盃.md" title="wikilink">詳細</a></em></p></td>
<td><p><a href="../Page/東京.md" title="wikilink">東京</a></p></td>
<td><p><br />
<strong><a href="../Page/蔚山現代足球俱樂部.md" title="wikilink">蔚山現代</a></strong></p></td>
<td><p><br />
<a href="../Page/大阪飛腳.md" title="wikilink">大阪飛腳</a></p></td>
<td><p><br />
<a href="../Page/千葉市原.md" title="wikilink">千葉市原</a></p></td>
<td><p><br />
<a href="../Page/大連實德足球俱樂部.md" title="wikilink">大連實德</a></p></td>
<td><p>4</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007<br />
<em><a href="../Page/2007年A3冠軍盃.md" title="wikilink">詳細</a></em></p></td>
<td><p><a href="../Page/濟南.md" title="wikilink">濟南</a></p></td>
<td><p><br />
<strong><a href="../Page/上海綠地申花足球俱樂部.md" title="wikilink">上海申花</a></strong></p></td>
<td><p><br />
<a href="../Page/山東魯能泰山足球俱樂部.md" title="wikilink">山東魯能泰山</a></p></td>
<td><p><br />
<a href="../Page/浦和紅鑽.md" title="wikilink">浦和紅鑽</a></p></td>
<td><p><br />
<a href="../Page/城南足球俱樂部.md" title="wikilink">城南一和天馬</a></p></td>
<td><p>4</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 統計

  - 最佳射手：**[纳德森·罗德里格斯·德·索萨](../Page/纳德森·罗德里格斯·德·索萨.md "wikilink")** -
    6球（2005年）、**[李天秀](../Page/李天秀.md "wikilink")** - 6球（2006年）
  - 單場最大比分：[大阪飛腳](../Page/大阪飛腳.md "wikilink")
    **0–6**[蔚山現代](../Page/蔚山現代.md "wikilink")（2006年）
  - 單場進球最多比賽：[浦和紅鑽](../Page/浦和紅鑽.md "wikilink") '''3–4
    '''[山東魯能泰山](../Page/山東魯能泰山.md "wikilink")（2007年）

## 參賽次數

<table>
<thead>
<tr class="header">
<th><p>排名</p></th>
<th><p>球會</p></th>
<th><p>所屬國家</p></th>
<th><p>參賽次數</p></th>
<th><p>參賽年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/城南足球俱樂部.md" title="wikilink">城南一和天馬</a></p></td>
<td></td>
<td><p>3 次</p></td>
<td><p>2003, 2004, 2007</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/橫濱水手.md" title="wikilink">橫濱水手</a></p></td>
<td></td>
<td><p>2 次</p></td>
<td><p>2004, 2005</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大連實德足球俱樂部.md" title="wikilink">大連實德</a></p></td>
<td></td>
<td><p>2 次</p></td>
<td><p>2003, 2006</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/上海綠地申花足球俱樂部.md" title="wikilink">上海申花</a></p></td>
<td></td>
<td><p>2 次</p></td>
<td><p>2004, 2007</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/鹿島鹿角.md" title="wikilink">鹿島鹿角</a></p></td>
<td></td>
<td><p>1 次</p></td>
<td><p>2003</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/磐田喜悦.md" title="wikilink">磐田喜悦</a></p></td>
<td></td>
<td><p>1 次</p></td>
<td><p>2003</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/北京人和足球俱樂部.md" title="wikilink">上海國際</a></p></td>
<td></td>
<td><p>1 次</p></td>
<td><p>2004</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/深圳市足球俱樂部.md" title="wikilink">深圳健力寶</a></p></td>
<td></td>
<td><p>1 次</p></td>
<td><p>2005</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/水原三星藍翼足球俱樂部.md" title="wikilink">水原三星藍翼</a></p></td>
<td></td>
<td><p>1 次</p></td>
<td><p>2005</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/浦項制鐵足球俱樂部.md" title="wikilink">浦項制鐵</a></p></td>
<td></td>
<td><p>1 次</p></td>
<td><p>2005</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大阪飛腳.md" title="wikilink">大阪飛腳</a></p></td>
<td></td>
<td><p>1 次</p></td>
<td><p>2006</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蔚山現代足球俱樂部.md" title="wikilink">蔚山現代</a></p></td>
<td></td>
<td><p>1 次</p></td>
<td><p>2006</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/千葉市原.md" title="wikilink">千葉市原</a></p></td>
<td></td>
<td><p>1 次</p></td>
<td><p>2006</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/山東魯能泰山足球俱樂部.md" title="wikilink">山東魯能泰山</a></p></td>
<td></td>
<td><p>1 次</p></td>
<td><p>2007</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/浦和紅鑽.md" title="wikilink">浦和紅鑽</a></p></td>
<td></td>
<td><p>1 次</p></td>
<td><p>2007</p></td>
<td></td>
</tr>
</tbody>
</table>

## 参考资料

## 外部連結

  - [官方網站](https://web.archive.org/web/20061206021258/http://www.a3cup.com/)

[\*](../Category/A3冠军盃.md "wikilink")

1.