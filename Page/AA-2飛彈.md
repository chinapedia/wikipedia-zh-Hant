**AA-2**[飛彈是](../Page/飛彈.md "wikilink")[蘇聯第一種](../Page/蘇聯.md "wikilink")[紅外線導引](../Page/紅外線導引.md "wikilink")[空對空飛彈](../Page/空對空飛彈.md "wikilink")。**AA-2**是[北約賦予的代號](../Page/北約.md "wikilink")，名稱是**環礁**（Atoll），蘇聯軍方給予的代號包括**K
13**、**R-3**或者是**R-13**，是蘇聯早期外銷最廣，實戰經驗最多的空對空飛彈。

## 歷史

AA-2是蘇聯根據[中國提供的](../Page/中國.md "wikilink")[AIM-9響尾蛇飛彈仿造而來](../Page/AIM-9響尾蛇飛彈.md "wikilink")，外型也與AIM-9B非常接近。早期傳說蘇聯取得的AIM-9是[中華民國空軍](../Page/中華民國空軍.md "wikilink")[F-86於空戰中發射之後插在中國一架](../Page/F-86.md "wikilink")[米格-17戰鬥機上的未爆彈而來](../Page/米格-17.md "wikilink")。

1958年蘇聯取的中國提供的樣本之後開始積極發展，除了來自中國的樣本外，瑞典空軍上校斯泰格·溫納斯特羅姆提供的完整設計圖與發射裝置在蘇聯仿製該飛彈上有極大幫助，第一枚以[逆向工程仿造的R](../Page/逆向工程.md "wikilink")-3飛彈在改裝的[米格-19戰鬥機上進行飛行試驗](../Page/米格-19戰鬥機.md "wikilink")，稍後也在[米格-21的原型機](../Page/米格-21.md "wikilink")**Ye-6T**上進行測試。第一種量產型編號**R-3S**（S代表俄文中的**量產**），成為米格-21F-13戰鬥機的標準配備。

## 生產次型

[AA-2_seeker.jpg](https://zh.wikipedia.org/wiki/File:AA-2_seeker.jpg "fig:AA-2_seeker.jpg")
AA-2主要有三種生產次型。

### AA-2

AA-2也就是**R-3S**或是**K-13**是第一種量產型，外型與AIM-9B非常相似，諸多AIM-9B初期的設計缺陷导致无法与AA2相比，尋標頭同樣採用沒有冷卻的設計，只有在目標尾部很小的範圍能才能鎖定，但總體性能上較AIM-9B稍高。

### AA-2C

AA-2C的蘇聯編號是**R-3R**或**K-13R**。除了火箭發動機、控制面與彈頭以外，整顆飛彈採用完全不同的設計。導引方式改採[半主動雷達導引](../Page/半主動雷達導引.md "wikilink")。可攻擊的角度也擴大為全向位，不受到只能在尾部的限制。

### AA-2D

AA-2D的蘇聯編號**R-13M**或**K-13M**，是AA-2系列的最後一種次型。尋標器改用新設計與液態[氮冷卻](../Page/氮.md "wikilink")，效果比AA-2要好，不過還是必須在目標的尾部才能夠發揮作用。雖然彈體長度比AA-2要短，可是火箭發動機提供幾乎是AA-2兩倍的推力。火箭燃燒時間為3.3到5.4秒之間，可控制的飛行時間為55秒。

AA-2D的另外一個特點是能夠對付發出高熱量的小型地面目標。

## 作戰紀錄

AA-2第一次的實戰紀錄是在[越戰時期](../Page/越戰.md "wikilink")，雖然1966年[北越空軍已經攜帶飛彈與美國戰鬥機遭遇](../Page/北越.md "wikilink")，不過直到該年10月，負責掩護[EB-66的兩架](../Page/EB-66.md "wikilink")[F-4C成為最早的犧牲品](../Page/F-4.md "wikilink")。除了大量出現在越戰以外，埃及在1970年到1973年這一段時間之中也有使用。

## 服役國

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 參見

  - [AIM-9響尾蛇飛彈](../Page/AIM-9響尾蛇飛彈.md "wikilink")
  - [霹靂-2飛彈](../Page/霹靂-2飛彈.md "wikilink")
  - [霹靂-1飛彈](../Page/霹靂-1飛彈.md "wikilink")
  - [紅外線導引](../Page/紅外線導引.md "wikilink")

## 參考文獻

  -
  - Duncan Lennox， Jane's Air-Launched Weapons ，Jane's Information
    Group， ISBN 0-7106-0866-7

## 外部連結

  - [Chinese PL-2 air-to-air
    missile](http://www.airforceworld.com/pla/j-6-mig-19-fighter-china-pl-2-missile.htm)

[Category:空對空飛彈](../Category/空對空飛彈.md "wikilink")
[Category:蘇聯飛彈](../Category/蘇聯飛彈.md "wikilink")