**AFAS體育場**是[荷蘭](../Page/荷蘭.md "wikilink")[阿爾克馬爾市的足球場](../Page/阿爾克馬爾.md "wikilink")，在A9高速公路旁，是[荷蘭足球甲級聯賽](../Page/荷蘭足球甲級聯賽.md "wikilink")[AZ阿爾克馬爾隊的主場](../Page/AZ阿爾克馬爾.md "wikilink")。

體育場在2006年8月4日，在大約一年半的建造後開放。容量爲17023個座位。總造價爲3800万[歐元](../Page/歐元.md "wikilink")。設計于Zwarts
& Jansma architecten建築事務所，該所已經在荷蘭設計過多個新體育場。體育場的草皮由捲鋪和人造草皮混合而成。

體育場落成後的首場比賽是對[阿森納的友誼賽](../Page/阿森纳足球俱乐部.md "wikilink")，阿爾克馬爾0:3落敗。

有計劃加建觀眾席至3万個座位。

## 参考资料

## 外部链接

  - [體育場官方網頁（荷蘭文）](https://web.archive.org/web/20071011193102/http://az.nl/index.php?buttonID=174)

[Category:荷蘭足球場](../Category/荷蘭足球場.md "wikilink")