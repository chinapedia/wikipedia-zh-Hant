2006年，[美国电影学会從](../Page/美国电影学会.md "wikilink")300片提名電影中，評選出100片百年來最偉大的勵志片名單，作為美国电影学院的“百年百大”系列的一个重要部分——“**AFI百年百大勵志電影**”（**AFI's
100 Years... 100 Cheers**）。

## 百大勵志片名單及三百大提名名單

  - 在300片提名名單中，[亨利·方達](../Page/亨利·方達.md "wikilink")、[湯姆·漢克各出現](../Page/湯姆·漢克.md "wikilink")8次最多。[賈利·古柏](../Page/賈利·古柏.md "wikilink")、[西德尼·波蒂埃各出現](../Page/西德尼·波蒂埃.md "wikilink")7次居次。女演員以[珍·亞瑟出現](../Page/珍·亞瑟.md "wikilink")4次最多。
  - 在100片勝出名單中，[詹姆斯·史都華在前五名百年最偉大勵志片中](../Page/詹姆斯·史都華.md "wikilink")，主演第一名影片《[風雲人物](../Page/風雲人物.md "wikilink")》、以及第五名影片《[史密斯遊美京](../Page/史密斯遊美京.md "wikilink")》，他也主演第69名影片《壯志凌雲（[林白征空記](../Page/林白征空記.md "wikilink")）》。
  - 在300片提名名單中，[法蘭克·卡普拉](../Page/法蘭克·卡普拉.md "wikilink")、[史蒂芬·史匹伯各以](../Page/史蒂芬·史匹伯.md "wikilink")6部作品，並列出現最多的導演。
  - 在300片提名名單中，超過1/3的影片本質上是屬於傳記電影。
  - 1939年出品了12片提名電影、以及5片勝出電影，超過其他任何一個年份。

## AFI百年百大勵志電影

1.  It's a Wonderful Life (1946) [風雲人物](../Page/風雲人物.md "wikilink")

2.  To Kill a Mockingbird (1962)
    [杀死一只知更鸟](../Page/杀死一只知更鸟.md "wikilink")

3.  Schindler's List (1993) [辛德勒的名單](../Page/辛德勒的名單.md "wikilink")

4.  Rocky (1976) [洛基](../Page/洛基_\(電影\).md "wikilink")

5.  Mr. Smith Goes to Washington (1939)
    [史密斯遊美京](../Page/史密斯遊美京.md "wikilink")

6.  E.T. the Extra-Terrestrial (1982)
    [E.T.外星人](../Page/E.T.外星人.md "wikilink")

7.  The Grapes of Wrath (1940)
    [怒火之花](../Page/怒火之花.md "wikilink")、[憤怒的葡萄](../Page/憤怒的葡萄.md "wikilink")

8.  Breaking Away (1979) [突破](../Page/突破_\(電影\).md "wikilink")

9.  Miracle on 34th Street (1947)
    [第三十四街的奇蹟](../Page/第三十四街的奇蹟.md "wikilink")

10. Saving Private Ryan (1998) [搶救雷恩大兵](../Page/搶救雷恩大兵.md "wikilink")

11. The Best Years of Our Lives (1946)
    [黃金時代](../Page/黃金時代.md "wikilink")

12. Apollo 13 (1995) [阿波羅13号](../Page/阿波罗13号_\(电影\).md "wikilink")

13. Hoosiers (1986) [火爆教頭草地兵](../Page/火爆教頭草地兵.md "wikilink")

14. The Bridge on the River Kwai (1957)
    [桂河大橋](../Page/桂河大橋.md "wikilink")

15. The Miracle Worker (1962) [熱淚心聲](../Page/熱淚心聲.md "wikilink")

16. Norma Rae (1979) [諾瑪蕊](../Page/諾瑪蕊.md "wikilink")

17. One Flew Over the Cuckoo's Nest (1975)
    [飛越杜鵑窩](../Page/飛越杜鵑窩.md "wikilink")

18. The Diary of Anne Frank (1959)
    [安妮少女的日記](../Page/安妮少女的日記.md "wikilink")

19. The Right Stuff (1983) [太空先鋒](../Page/太空先鋒.md "wikilink")

20. Philadelphia (1993) [费城故事](../Page/费城故事_\(1993年\).md "wikilink")、費城

21. In the Heat of the Night (1967) [惡夜追緝令](../Page/惡夜追緝令.md "wikilink")

22. The Pride of the Yankees (1942)
    [洋基隊的驕傲](../Page/洋基隊的驕傲.md "wikilink")

23. The Shawshank Redemption (1994)
    [肖申克的救赎](../Page/肖申克的救赎.md "wikilink")

24. National Velvet (1944) [玉女神駒](../Page/玉女神駒.md "wikilink")

25. Sullivan's Travels (1941) [蘇利文的旅行](../Page/蘇利文的旅行.md "wikilink")

26. The Wizard of Oz (1939)
    [綠野仙蹤](../Page/綠野仙蹤_\(1939年電影\).md "wikilink")

27. High Noon (1952) [日正當中](../Page/日正當中.md "wikilink")

28. Field of Dreams (1989) [夢幻成真](../Page/夢幻成真.md "wikilink")

29. Gandhi (1982) [甘地傳](../Page/甘地傳.md "wikilink")

30. Lawrence of Arabia (1962)
    [阿拉伯的勞倫斯](../Page/阿拉伯的劳伦斯_\(电影\).md "wikilink")

31. Glory (1989) [光榮戰役](../Page/光榮戰役.md "wikilink")

32. Casablanca (1942) [北非諜影](../Page/北非諜影.md "wikilink")、卡薩布蘭卡

33. City Lights (1931) [城市之光](../Page/城市之光.md "wikilink")

34. All the President's Men (1976) [大陰謀](../Page/大陰謀.md "wikilink")

35. Guess Who's Coming to Dinner (1967)
    [誰來晚餐](../Page/誰來晚餐.md "wikilink")

36. On the Waterfront (1954) [岸上風雲](../Page/岸上風雲.md "wikilink")

37. Forrest Gump (1994) [阿甘正傳](../Page/阿甘正傳.md "wikilink")

38. Pinocchio (1940) [木偶奇遇記](../Page/木偶奇遇記.md "wikilink")

39. Star Wars (1977) [星際大戰](../Page/星際大戰.md "wikilink")

40. Mrs. Miniver (1942) [忠勇之家](../Page/忠勇之家.md "wikilink")

41. The Sound of Music (1965) [真善美](../Page/音乐之声_\(电影\).md "wikilink")

42. 12 Angry Men (1957) [十二怒漢](../Page/十二怒漢.md "wikilink")

43. Gone with the Wind (1939) [亂世佳人](../Page/亂世佳人.md "wikilink")

44. Spartacus (1960) [萬夫莫敵](../Page/萬夫莫敵.md "wikilink")

45. On Golden Pond (1981) [金池塘](../Page/金池塘.md "wikilink")

46. Lilies of the Field (1963) [流浪漢](../Page/流浪漢.md "wikilink")

47. 2001: A Space Odyssey (1968)
    [2001太空漫遊](../Page/2001太空漫遊_\(電影\).md "wikilink")

48. (1951) [非洲皇后](../Page/非洲皇后.md "wikilink")

49. Meet John Doe (1941) [群眾](../Page/群眾_\(電影\).md "wikilink")

50. Seabiscuit (2003) [奔騰年代](../Page/奔騰年代.md "wikilink")

51. The Color Purple (1985) [紫色姊妹花](../Page/紫色姐妹花_\(電影\).md "wikilink")

52. Dead Poets Society (1989) [死亡诗社](../Page/死亡诗社.md "wikilink")

53. Shane (1953) [原野奇俠](../Page/原野奇俠.md "wikilink")

54. Rudy (1993) [追夢赤子心](../Page/追夢赤子心.md "wikilink")

55. The Defiant Ones (1958) [逃獄驚魂](../Page/逃獄驚魂.md "wikilink")

56. Ben-Hur (1959) [賓漢](../Page/賓漢.md "wikilink")

57. Sergeant York (1941) [約克軍曹](../Page/約克軍曹.md "wikilink")

58. Close Encounters of the Third Kind (1977)
    [第三類接觸](../Page/第三類接觸.md "wikilink")

59. Dances with Wolves (1990) [與狼共舞](../Page/與狼共舞.md "wikilink")

60. The Killing Fields (1984) [殺戮地带](../Page/殺戮地带.md "wikilink")

61. Sounder (1972) [兒子離家時](../Page/兒子離家時.md "wikilink")

62. Braveheart (1995) [勇敢的心](../Page/勇敢的心.md "wikilink")

63. Rain Man (1988) [雨人](../Page/雨人.md "wikilink")

64. The Black Stallion (1979) [黑神駒](../Page/黑神駒.md "wikilink")

65. A Raisin in the Sun (1961) [烈日當頭](../Page/烈日當頭.md "wikilink")

66. Silkwood (1983) [絲克伍事件](../Page/絲克伍事件.md "wikilink")

67. The Day the Earth Stood Still (1951)
    [-{zh-hans:地球停转之日;zh-hk:地球末日記;zh-tw:地球末日記;}-](../Page/地球末日記.md "wikilink")

68. An Officer and a Gentleman (1982)
    [軍官與紳士](../Page/軍官與紳士.md "wikilink")

69. The Spirit of St. Louis (1957)
    [林白征空記](../Page/林白征空記.md "wikilink")、壯志凌雲

70. Coal Miner's Daughter (1980) [礦工的女兒](../Page/礦工的女兒.md "wikilink")

71. Cool Hand Luke (1967) [鐵窗喋血](../Page/鐵窗喋血.md "wikilink")

72. Dark Victory (1939) [卿何薄命](../Page/卿何薄命.md "wikilink")

73. Erin Brockovich (2000) [永不妥協](../Page/永不妥協.md "wikilink")

74. Gunga Din (1939) [古廟戰笳聲](../Page/古廟戰笳聲.md "wikilink")

75. The Verdict (1982) [大審判](../Page/大審判.md "wikilink")

76. Birdman of Alcatraz (1962) [阿卡翠斯的鳥人](../Page/阿卡翠斯的鳥人.md "wikilink")

77. Driving Miss Daisy (1989) [溫馨接送情](../Page/溫馨接送情.md "wikilink")

78. Thelma and Louise (1991) [末路狂花](../Page/末路狂花.md "wikilink")

79. The Ten Commandments (1956) [十誡](../Page/十誡.md "wikilink")

80. Babe (1995) [我不笨，所以我有話要說](../Page/小猪宝贝.md "wikilink")

81. Boys Town (1938) [孤兒樂園](../Page/孤兒樂園.md "wikilink")

82. Fiddler on the Roof (1971)
    [屋上的提琴手](../Page/屋上的提琴手_\(電影\).md "wikilink")

83. Mr. Deeds Goes to Town (1936) [富貴浮雲](../Page/富貴浮雲.md "wikilink")

84. Serpico (1973) [衝突](../Page/衝突_\(電影\).md "wikilink")

85. What's Love Got to Do with It? (1993)
    [與愛何干](../Page/與愛何干.md "wikilink")

86. Stand and Deliver (1988) [為人師表](../Page/為人師表.md "wikilink")

87. Working Girl (1988) [打工女郎](../Page/打工女郎.md "wikilink")

88. Yankee Doodle Dandy (1942) [勝利之歌](../Page/勝利之歌.md "wikilink")

89. Harold and Maude (1972) [哈洛與茂德](../Page/哈洛與茂德.md "wikilink")

90. Hotel Rwanda (2004) [盧安達飯店](../Page/盧安達飯店.md "wikilink")

91. The Paper Chase (1973) [平步青雲](../Page/平步青雲.md "wikilink")

92. Fame (1980) [名揚四海](../Page/名揚四海.md "wikilink")

93. A Beautiful Mind (2001) [美麗境界](../Page/美麗境界.md "wikilink")

94. Captains Courageous (1937) [怒海餘生](../Page/怒海餘生.md "wikilink")

95. Places in the Heart (1984) [心田深處](../Page/心田深處.md "wikilink")

96. Searching For Bobby Fischer (1993)
    [天生小棋王](../Page/天生小棋王.md "wikilink")

97. Madame Curie (1943) [居里夫人](../Page/居里夫人.md "wikilink")

98. The Karate Kid (1984) [小子難纏](../Page/小子難纏.md "wikilink")、龍威小子

99. Ray (2004) [雷之心靈傳奇](../Page/雷之心靈傳奇.md "wikilink")

100. Chariots of Fire (1981) [火戰車](../Page/火戰車.md "wikilink")、烈火戰車

## 外部連結

  - [Complete list](http://www.afi.com/docs/tvevents/pdf/cheers100.pdf)
    (pdf)
  - [Press release announcing the
    show](http://www.afi.com/Docs/about/press/2005/100cheers.pdf) (pdf)
  - [List of the 300 nominated
    films](http://www.afi.com/Docs/tvevents/pdf/cheers300.pdf) (pdf)

[A](../Category/美國電影學院百年百大系列.md "wikilink")
[A](../Category/英語電影.md "wikilink")
[A](../Category/劇情片.md "wikilink")
[A](../Category/家庭片.md "wikilink")
[A](../Category/傳記片.md "wikilink")