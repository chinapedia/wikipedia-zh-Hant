1998年开始，[美国电影学会](../Page/美国电影学会.md "wikilink")（AFI）为了庆祝[电影诞生](../Page/电影.md "wikilink")100周年，每年都会评选一个美国电影“**百年百大……**”（**AFI
100 Years... series**）。

## 百年百大系列：

  - 1998年：[AFI百年百大電影](../Page/AFI百年百大電影.md "wikilink")（）
  - 1999年：[AFI百年百大明星](../Page/AFI百年百大明星.md "wikilink")（）
  - 2000年：[AFI百年百大喜劇電影](../Page/AFI百年百大喜劇電影.md "wikilink")（）
  - 2001年：[AFI百年百大驚悚電影](../Page/AFI百年百大驚悚電影.md "wikilink")（）
  - 2002年：[AFI百年百大愛情電影](../Page/AFI百年百大愛情電影.md "wikilink")（）
  - 2003年：[AFI百年百大英雄與反派](../Page/AFI百年百大英雄與反派.md "wikilink")（）
  - 2004年：[AFI百年百大电影歌曲](../Page/AFI百年百大电影歌曲.md "wikilink")（）
  - 2005年：[AFI百年百大电影台词](../Page/AFI百年百大电影台词.md "wikilink")（）
  - 2005年：[AFI百年百大电影配乐](../Page/AFI百年百大电影配乐.md "wikilink")（）
  - 2006年：[AFI百年百大勵志電影](../Page/AFI百年百大勵志電影.md "wikilink")（）
  - 2006年：[AFI百年百大歌舞電影](../Page/AFI百年百大歌舞電影.md "wikilink")（）
  - 2007年：[AFI百年百大電影（十周年版）](../Page/AFI百年百大电影_\(2007年版\).md "wikilink")（）
  - 2008年：[AFI百年各類型電影十大佳片](../Page/AFI百年各類型電影十大佳片.md "wikilink")（）

## 外部链接

  - [美国电影学院百年百大系列](http://www.afi.com/tvevents/100years/100yearslist.aspx)

[Category:美國電影學會](../Category/美國電影學會.md "wikilink")
[Category:美国电影学院百年百大系列](../Category/美国电影学院百年百大系列.md "wikilink")