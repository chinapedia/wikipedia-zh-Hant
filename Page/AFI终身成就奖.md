**美国电影学会终身成就奖**，简称**AFI终身成就奖**（****）是由[美国电影学会理事会颁发的奖项](../Page/美国电影学会.md "wikilink")，用以表彰通过电影或电视，终身致力于美国文化发展的个人。该奖项从1973年开始颁发，第一位获奖人为[约翰·福特](../Page/约翰·福特.md "wikilink")。\[1\]

## 获奖名单

[Lillian_Gish-edit1.jpg](https://zh.wikipedia.org/wiki/File:Lillian_Gish-edit1.jpg "fig:Lillian_Gish-edit1.jpg")是唯一[默片時代的獲獎者](../Page/默片.md "wikilink")\]\]
下文列出自1973年以来历届获奖人：\[2\]

  - 2017年：[黛安·基頓](../Page/黛安·基頓.md "wikilink")
  - 2016年：[约翰·威廉姆斯](../Page/约翰·威廉姆斯.md "wikilink")
  - 2015年：[史提夫·馬丁](../Page/史提夫·馬丁.md "wikilink")
  - 2014年：[珍·芳達](../Page/珍·芳達.md "wikilink")
  - 2013年：[梅尔·布鲁克斯](../Page/梅尔·布鲁克斯.md "wikilink")
  - 2012年：[莎莉·麥克琳](../Page/莎莉·麥克琳.md "wikilink")
  - 2011年：[摩根·費里曼](../Page/摩根·費里曼.md "wikilink")
  - 2010年：[麥克·尼可斯](../Page/麥克·尼可斯.md "wikilink")
  - 2009年：[迈克尔·道格拉斯](../Page/迈克尔·道格拉斯.md "wikilink")
  - 2008年：[沃伦·比蒂](../Page/沃伦·比蒂.md "wikilink")
  - 2007年：[艾尔·帕西诺](../Page/艾尔·帕西诺.md "wikilink")
  - 2006年：[肖恩·康纳利](../Page/肖恩·康纳利.md "wikilink")
  - 2005年：[乔治·卢卡斯](../Page/乔治·卢卡斯.md "wikilink")
  - 2004年：[梅丽·史翠普](../Page/梅丽·史翠普.md "wikilink")
  - 2004年：[汤姆·克鲁斯](../Page/汤姆·克鲁斯.md "wikilink")
  - 2003年：[罗伯特·德尼罗](../Page/罗伯特·德尼罗.md "wikilink")
  - 2002年：[汤姆·汉克斯](../Page/汤姆·汉克斯.md "wikilink")
  - 2001年：[芭芭拉·史翠珊](../Page/芭芭拉·史翠珊.md "wikilink")
  - 2000年：[哈里森·福特](../Page/哈里森·福特.md "wikilink")
  - 1999年：[达斯汀·霍夫曼](../Page/达斯汀·霍夫曼.md "wikilink")
  - 1998年：[罗伯特·怀斯](../Page/罗伯特·怀斯.md "wikilink")
  - 1997年：[马丁·斯科塞斯](../Page/马丁·斯科塞斯.md "wikilink")
  - 1996年：[克林特·伊斯特伍德](../Page/克林特·伊斯特伍德.md "wikilink")
  - 1995年：[斯蒂芬·斯皮尔伯格](../Page/斯蒂芬·斯皮尔伯格.md "wikilink")
  - 1994年：[杰克·尼科尔森](../Page/杰克·尼科尔森.md "wikilink")
  - 1993年：[伊丽莎白·泰勒](../Page/伊丽莎白·泰勒.md "wikilink")
  - 1992年：[西德尼·波蒂埃](../Page/西德尼·波蒂埃.md "wikilink")
  - 1991年：[柯克·道格拉斯](../Page/柯克·道格拉斯.md "wikilink")
  - 1990年：[大卫·利恩](../Page/大卫·利恩.md "wikilink")
  - 1989年：[格里高利·派克](../Page/格里高利·派克.md "wikilink")
  - 1988年：[杰克·李蒙](../Page/杰克·李蒙.md "wikilink")
  - 1987年：[芭芭拉·史坦維克](../Page/芭芭拉·史坦維克.md "wikilink")
  - 1986年：[比利·怀德](../Page/比利·怀德.md "wikilink")
  - 1985年：[金·凯利](../Page/金·凯利.md "wikilink")
  - 1984年：[莉莲·吉许](../Page/莉莲·吉许.md "wikilink")
  - 1983年：[约翰·休斯顿](../Page/约翰·休斯顿.md "wikilink")
  - 1982年：[法兰克·卡普拉](../Page/法兰克·卡普拉.md "wikilink")
  - 1981年：[弗雷德·阿斯泰尔](../Page/弗雷德·阿斯泰尔.md "wikilink")
  - 1980年：[詹姆斯·史都華](../Page/詹姆斯·史都華.md "wikilink")
  - 1979年：[阿尔弗雷德·希区柯克](../Page/阿尔弗雷德·希区柯克.md "wikilink")
  - 1978年：[亨利·方达](../Page/亨利·方达.md "wikilink")
  - 1977年：[贝蒂·戴维斯](../Page/贝蒂·戴维斯.md "wikilink")
  - 1976年：[威廉·惠勒](../Page/威廉·惠勒.md "wikilink")
  - 1975年：[奥森·威尔士](../Page/奥森·威尔士.md "wikilink")
  - 1974年：[詹姆斯·卡格尼](../Page/詹姆斯·卡格尼.md "wikilink")
  - 1973年：[约翰·福特](../Page/约翰·福特.md "wikilink")

## 参考资料

[Category:美国电影奖项](../Category/美国电影奖项.md "wikilink")
[Category:美國電影學會](../Category/美國電影學會.md "wikilink")
[Category:终身成就奖](../Category/终身成就奖.md "wikilink")

1.  [美国电影学会官方网站关于AFI终身成就奖介绍及历届获奖人名单](http://www.afi.com/tvevents/laa/laalist.aspx)

2.