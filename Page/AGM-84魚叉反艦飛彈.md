**AGM-84魚叉**（Harpoon）[反艦飛彈是由](../Page/反艦飛彈.md "wikilink")[美国](../Page/美国.md "wikilink")[麥克唐納-道格拉斯公司开发的反舰导弹](../Page/麥克唐納-道格拉斯公司.md "wikilink")，在1979年装备部队使用，也是美國海空軍現役最主要的反艦武器，可以自[飛機](../Page/飛機.md "wikilink")，各類水面[軍艦以及](../Page/軍艦.md "wikilink")[潛艇上發射](../Page/潛艇.md "wikilink")。此外，[美國海軍利用魚叉飛彈開發出遠程陸上攻擊型](../Page/美國海軍.md "wikilink")（SLAM：Stand-off
Land Attack
Missile）。在美國三軍通用編號當中，AGM-84為空射型，RGM-84為艦射型，UGM-84則是水下潛艇發射型，但是他們的基本結構都是相同的。

## 發展沿革

在鱼叉飛彈開發前，美國海軍已經有可空射的[AGM-12飛彈](../Page/AGM-12飛彈.md "wikilink")，1965年由推動研發的魚叉飛彈是一種空射、主要設定目標是[潛艦](../Page/潛艦.md "wikilink")，計畫射程45公里；美國海軍在1960年代初尚無認真地思考艦載反艦飛彈的需求，當時[核子武器可以將大部分的威脅給解決掉](../Page/核子武器.md "wikilink")。直到1967年[第三次中东战争中](../Page/第三次中东战争.md "wikilink")，[埃及用俄製](../Page/埃及.md "wikilink")[冥河飛彈击沉了](../Page/SS-N-2.md "wikilink")[以色列的](../Page/以色列.md "wikilink")[艾拉特号驱逐舰](../Page/艾拉特.md "wikilink")，美國海軍才意識到反艦飛彈對水面艦的威脅，並成立團隊評估艦載反艦飛彈的需求。在美國海軍作戰部長上將推動下，鱼叉飛彈在1968年開始方案論證，1970年11月确定開發計劃，1971年1月进行招标，同年6月从参予競争的5家公司中选定[麦道公司为主承包商](../Page/麦道公司.md "wikilink")，進入工程发展階段。

魚叉飛彈发展计划分为武器系统的设计、研制和使用鉴定试验3个阶段。计划中共用样本飞弹102枚，其中设计用的有32枚，研制用的有40枚，用于鉴定试验有30枚；其中80枚带制导及控制系统，10枚裝有戰鬥段的全功能實彈。

首批原型彈1972年10月17日試射，原型彈是由固態火箭發動機提供動力，不過在後續技術需求上射程加倍至90公里，麥道因此放棄火箭推進，決定以[渦輪噴射發動機作為飛彈動力](../Page/渦輪噴射發動機.md "wikilink")。魚叉飛彈的動力測試直至1977年3月才试验结束，共发射样本飞弹40枚；至1974年起魚叉飛彈的主動尋標器開始進行測試。1975年7月投入首批生产，同年12月完成研制，在1977年7月开始进入美国海军水面艦艇服役，空射型魚叉飛彈在1979年服役，配備至P-3巡邏機上；潛射型魚叉飛彈則到1981年才完成研發服役。

魚叉飛彈研制费382.3百万[美元](../Page/美元.md "wikilink")，采购费为3909.6百万美元，总计4291.9百万美元，制造样本飞弹84枚，首批生产总数4013枚，每月生产率40枚，单价97.4万美元。

艦射型魚叉飛彈絕大部分都裝設在上，但某些缺少放置發射架空間的艦種也可以使用[Mk
13型飛彈發射器或](../Page/Mk_13型飛彈發射器.md "wikilink")[MK-16反潛火箭發射器投射](../Page/阿斯洛克反潛飛彈.md "wikilink")。至今仍不相容[Mark
41垂直發射系統](../Page/Mark_41垂直發射系統.md "wikilink")。

[中华民国空军的](../Page/中华民国空军.md "wikilink")[F-16 Block
20是全世界F](../Page/F-16_Block_20.md "wikilink")-16族系中第一款整合魚叉飛彈做為主力反艦武器的型號\[1\]。

## 性能

魚叉飛彈設計主要分成四個結構：尋標器、導航電腦、彈頭、引擎。

初期型魚叉飛彈主動尋標器型號為AN／DSQ-28，它為一款操作頻率的二維雷達，可在飛行方向45度角區域內進行搜索，由於魚叉飛彈原始設計為攻擊上浮的潛艦，對小型目標的尋獲鎖定功能不差，對現代驅逐艦搜索距離最遠可達40公里，一般可搜索距離也有18公里。導航電腦代號AN／DSQ-44，DSQ-44並無使用[慣性導航系統](../Page/慣性導航系統.md "wikilink")，而是運用可測定三軸空間的姿態參照系統（Attitude
Reference
Assembly，**ATA**），ATA導航準確度雖差於慣性導航，但是對配備主動尋標器的魚叉飛彈來說是一個可以容忍的設計權衡。至2000年代後，魚叉Block
II之後的新型號導航段引入[聯合直接攻擊彈藥的導航設備](../Page/聯合直接攻擊彈藥.md "wikilink")，具有所謂「沿岸目標壓制模式」（Coastal
Target Suppression Mode），亦即對沿岸、港灣内，甚至陸上目標的攻擊能力，在準確度與可調整的導航點都大幅精進。

魚叉飛彈彈頭制式編號WDU-18／B，長90公分，重211公斤，為半穿甲高爆彈設計；陸攻型的彈頭更換為WDU-40／B，重360公斤。

AGM-84魚叉导弹有各種衍生型號，其性能如下\[2\][AGM-84“捕鲸叉/斯拉姆”AGM-84反舰/空地导弹 - 中国空军网 -
中国空军-见证中国空军成长历程，关注中国空军建设.](http://www.plaaf.net/html/92/n-92.html)
</ref>：

### 導引設計

  - ATA導航，末端主動雷達導引（AGM-84A/B/C/G）
  - 惯性导航、[GPS中制导和红外成像末導引](../Page/GPS.md "wikilink")（AGM-84E/H）
  - 惯性导航、GPS、地形匹配中制导和红外成像末導引（Grand SLAM）

### 結構

  - 引信：近炸引信／触发引信
  - 弹重；540公斤（AGM-84A/B/C/G），627公斤（AGM-84E），725公斤（AGM-84H）
  - 弹长：3.85[米](../Page/米.md "wikilink")（AGM-84A/B/C/G），4.50米（AGM-84E），4.37米（AGM-84H）
  - 弹径：343毫米
  - 翼展：914毫米（AGM-84A/B/C/E/G），2430毫米（AGM-84H)

## 特點

因為美軍認為只用一兩枚魚叉導彈直接擊沉大型船艦並非易事，而且美國海軍具有明顯空優，魚叉導彈在美國海軍的角色是癱瘓敵艦，再由航母的艦載機空襲擊沉，所以在設計魚叉導彈時，預定魚叉導彈攻擊中大型船艦時，將採取俯衝攻擊及低空引爆模式，在飛行末端迅速爬升，再立即俯衝到目標上方引爆彈頭，利用衝擊波及碎片破壞敵艦裝設在艦橋及桅杆的雷達設備，令其失去目標偵測及射擊控制能力，然後再派艦載機擊沉已失去作戰能力的敵艦。此種攻擊方式不但可減少錯過目標的機會，敵艦也難以迴避，但因為此模式不適用於攻擊小型艦艇，所以後來可選用穿甲引爆模式。

## 發射過程

AGM-84魚叉导弹发射前，由载机上的探测系统提供目标数据，然后输入导弹的-{zh-cn:[计算机](../Page/计算机.md "wikilink");zh-tw:[電腦](../Page/電腦.md "wikilink");}-内。导弹发射后，迅速下降至60米左右的[巡航高度](../Page/巡航.md "wikilink")，以0.75[马赫的速度飞行](../Page/马赫.md "wikilink")。在离目标一定距离时，导引头根据所选定的方式，开始搜索前方的区域。於捕获到目标后，AGM-84魚叉导弹进一步下降高度，並贴海飞行。當接近敌舰时，导弹會突然跃升，然后向目标俯冲，穿入艦橋内部爆炸，以提高摧毁敵艦指管通情能量效果。\[3\]

## 實戰紀錄

魚叉飛彈於1981年與1982年總計發生了兩次的誤射事件。其中一次發生於美國海軍，另一次則發生在丹麥海軍的巡防艦上。

魚叉飛彈首次實戰紀錄發生於兩伊戰爭期間。1980年11月，伊朗在*珍珠行動*之中以飛彈快艇襲擊並擊沉了兩艘伊拉克所屬的“黄蜂”級(OSA-class)飛彈快艇。其中伊朗所使用的武裝即包括了魚叉飛彈。

1986年，美國海軍在[錫德拉灣以魚叉飛彈擊沉了兩艘](../Page/錫德拉灣.md "wikilink")[利比亞所屬的巡邏艇](../Page/利比亞.md "wikilink")。其中兩枚飛彈係由CG-48約克頓號所發射的，但可能沒有命中目標；[A-6入侵者式攻擊機另外發射了數枚魚叉飛彈](../Page/A-6入侵者式攻擊機.md "wikilink")，並據信皆命中其鎖定之標的\[4\]\[5\]。雖然攻擊後的初步報告顯示由約克頓號所發射的飛彈亦擊中所瞄準的巡邏艇，但後續報告顯示這些魚叉飛彈所命中的可能僅僅是假目標，並非原先所認為的巡邏艇\[6\]。

1988年，美國以魚叉飛彈擊沉了伊朗海軍所屬的砂罕號(Sahand)巡防艦。伊朗方面亦向美國CG-28溫賴特號飛彈巡洋艦發射了一枚魚叉飛彈，但遭到美軍之電子干擾而沒有命中目標。\[7\]

同年12月，一枚由美國海軍[CV-64所屬的](../Page/CV-64.md "wikilink")[F/A-18戰機所發射的魚叉飛彈擊中了一艘印度籍的貨船](../Page/F/A-18.md "wikilink")*Jagvivek*號。據信其發生原因為該貨船於離港時因沒有接到海上演習通知而誤入演習海域。因此安裝演習彈頭的魚叉飛彈誤以為該船為其鎖定目標，並造成一人死亡。\[8\]

## 衍生型

AGM-84E/F/G/H/J/K/L/M SLAM飛彈，即AGM-84的遠程空射攻擊型

RGM-84E/F/G/H/J/K/L/M SLAM飛彈，即RGM-84的遠程艦射攻擊型

UGM-84G/L
SLAM飛彈，即UGM-84的遠程潛射攻擊型，它们合起来称为[战区外攻陆导弹](../Page/战区外.md "wikilink")
除了以上的近代衍生型，還有近年來被廣為所知的[卜派飛彈和](../Page/卜派飛彈.md "wikilink")[巡弋飛彈的鼻祖](../Page/巡弋飛彈.md "wikilink")，尤其是[戰斧巡弋飛彈](../Page/戰斧巡弋飛彈.md "wikilink")。

## 使用國

[Harpoon_operators.png](https://zh.wikipedia.org/wiki/File:Harpoon_operators.png "fig:Harpoon_operators.png")

  -



























## 参考文献

{{-}}

[Category:反艦飛彈](../Category/反艦飛彈.md "wikilink")
[Category:中華民國飛彈](../Category/中華民國飛彈.md "wikilink")
[Category:美国导弹](../Category/美国导弹.md "wikilink")
[Category:面对面导弹](../Category/面对面导弹.md "wikilink")

1.  [Lockheed Martin F-16A/B Fighting
    Falcon](http://www.taiwanairpower.org/af/f16.html)
    Taiwanairpower.org

2.
3.  [AGM-84“鱼叉”导弹
    -兵器装备网|世界军事|中国军事|中国海军|武器装备](http://www.wpeu.net/article/Article.asp?Articleid=138#)

4.  [Time (magazine)](../Page/Time_\(magazine\).md "wikilink").
    [High-Tech
    Firepower](http://www.time.com/time/magazine/article/0,9171,961035,00.html).
    April 7, 1986.

5.  Ronald Reagan. [Letter to the Speaker of the House of
    Representatives and the President Pro Tempore of the Senate on the
    Gulf of Sidra
    Incident](http://www.reagan.utexas.edu/archives/speeches/1986/32686h.htm).
    March 26, 1986.

6.  [The New York Times](../Page/The_New_York_Times.md "wikilink").
    PENTAGON REVISES LIBYAN SHIP TOLL. March 27, 1986.

7.  [The New York Times](../Page/The_New_York_Times.md "wikilink").
    [U.S. STRIKES 2 IRANIAN OIL RIGS AND HITS 6 WARSHIPS IN BATTLES OVER
    MINING SEA LANES IN
    GULF](http://query.nytimes.com/gst/fullpage.html?res=940DE0DC1038F93AA25757C0A96E948260).
    April 19, 1988.

8.  The New York Times / AP. [U.S. Rocket Hits Indian Ship Accidentally,
    Killing
    Crewman](http://query.nytimes.com/gst/fullpage.html?res=940DEEDA1F3AF930A25751C1A96E948260).
    December 13, 1988.