[Tel-pictogram.png](https://zh.wikipedia.org/wiki/File:Tel-pictogram.png "fig:Tel-pictogram.png")
[Toilet-pictogram.png](https://zh.wikipedia.org/wiki/File:Toilet-pictogram.png "fig:Toilet-pictogram.png")

**AIGA符号**是[美国交通部](../Page/美国交通部.md "wikilink")（DoT, Department of
Transportation）和[美国设计师学会](../Page/美国设计师学会.md "wikilink")（AIGA, American
Institute of Graphic
Arts）共同制定的一种不需要口头语言便能表达有用信息的符号。举例来说：现在广为人知的[厕所和](../Page/厕所.md "wikilink")[电话的标志](../Page/电话.md "wikilink")。自从在1974年推出以来，它们不止被广泛应用于美国的[飞机场](../Page/飞机场.md "wikilink")、[火车站](../Page/火车站.md "wikilink")、[酒店和其它公众场所](../Page/酒店.md "wikilink")，也被世界上很多地方所采用。因为其普遍被人们接受，有些人把它们称为象形符号中的“Helvetica字体”，在某些地方则被称为“人形Helvetica”。

## 历史

1974年，美国交通部发现使用在[美国州际高速公路系统上的](../Page/美国州际公路.md "wikilink")[象形符号有缺陷](../Page/象形符号.md "wikilink")，便委托美国设计师学会设计一套完整的象形符号集。在与[库克和商诺斯基联合公司](../Page/库克和商诺斯基联合公司.md "wikilink")（Cook
and Shanosky
Associates）的合作下，设计师对在世界上已经使用的象形符号进行了彻底的调查，其中包括[1972年慕尼黑奥运会](../Page/1972年夏季奥林匹克运动会.md "wikilink")。设计师们把象形符号按易读性、国际辨认度和用户抵制程度几项标准评估。在确定哪项功能最成功和合适后，设计师们按美国交通部的需求绘制了一套34个不同意思的象形符号。

到了1979年，又添加了16个符号，使符号总数达到了50个。

## 参考文献

  - 《Design Writing Research: Writing About Graphic Design》；Ellen
    Lupton和J. Abbot Miller；New York: Kiosk, 1996年
  - 《Symbol signs, 2nd ed》；American Institute of Graphic Arts for the
    U.S. Department of Transportation；New York: American Institute of
    Graphic Arts, 1993年

## 参见

  - [象形符号](../Page/象形符号.md "wikilink")

## 外部链接

  - [AIGA设计的标志符号](http://www.aiga.org/symbol-signs/)：英文

[Category:符號](../Category/符號.md "wikilink")
[Category:信息图形](../Category/信息图形.md "wikilink")