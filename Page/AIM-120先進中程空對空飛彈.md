<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p>AIM-120 AMRAAM<br />
（AIM-120先進中程空對空飛彈）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:_AIM-120A_AMRAAM_scheme.svg" title="fig:_AIM-120A_AMRAAM_scheme.svg">_AIM-120A_AMRAAM_scheme.svg</a><br />
<font size="-2">AIM-120 AMRAAM </font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>基本資料</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>名稱</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>種類</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>生產商</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>造價</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>開始服役</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>性能諸元</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>引擎</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>發射重量</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>長度</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>直徑</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>翼展</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>速度</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>有效射程</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>彈頭</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>導引</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>發射平台</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

**AIM-120先進中程空對空飛彈**（AIM-120 **A**dvanced **M**edium-**R**ange
**A**ir-to-**A**ir
**M**issile，縮寫為**AMRAAM**），通常被暱稱為**Slammer**（監獄），是[美國現役的](../Page/美國.md "wikilink")[主動雷達導引](../Page/主動雷達導引.md "wikilink")[空對空飛彈](../Page/空對空飛彈.md "wikilink")（AAM）。

## 起源

美國海軍於1950年代開發[AIM-7麻雀中程飛彈](../Page/AIM-7麻雀飛彈.md "wikilink")，AIM-7擁有約19[公里](../Page/公里.md "wikilink")（12[英哩](../Page/英哩.md "wikilink")）的有效射程，使用[半主動雷達導引](../Page/半主動雷達導引.md "wikilink")，可以攻擊視距內至[視距外的空中目標](../Page/超視距作戰.md "wikilink")。早期沒有配備機炮的[F-4幽靈與後來服役的其他](../Page/F-4.md "wikilink")[戰鬥機在機腹下特殊設計的位置可以攜帶最多](../Page/戰鬥機.md "wikilink")4枚飛彈，當飛彈有效作用時，能夠在纏鬥時發揮很大的威力。AIM-7與紅外線導引的[AIM-9響尾蛇一起取代當時服役中的雷達與紅外線導引版](../Page/AIM-9響尾蛇飛彈.md "wikilink")[AIM-4飛彈的位置](../Page/AIM-4飛彈.md "wikilink")。它的缺點是同時間只能攻擊一個目標，而且戰機發射它後必須持續指向敵機的方向，帶給提供射控資料的戰機很大麻煩和危險。

接著，美國海軍發展出[AIM-54鳳凰空對空飛彈作為艦隊防空之用](../Page/AIM-54.md "wikilink")。鳳凰飛彈重量高達454[公斤](../Page/公斤.md "wikilink")（1000磅），鳳凰飛彈能以5馬赫的速度攔截其設計時的假想敵-巡弋飛彈和發射它們的轟炸機。最早的方案是在直線機翼設計的[F6D戰鬥機上攜帶](../Page/F6D戰鬥機.md "wikilink")6枚，稍後則改到[F-111B上面](../Page/F-111戰鬥機.md "wikilink")。當[格魯曼設計新的](../Page/格魯曼公司.md "wikilink")[F-14時](../Page/F-14.md "wikilink")，他們預留足夠的載重量與空間來攜帶這個龐然大物。鳳凰飛彈是美國第一種具有[射後不理能力的主動雷達導引飛彈](../Page/射後不理.md "wikilink")：飛彈利用鼻端的雷達系統來導引，而不需發射載具的協助。理論上，雄貓能搭載6枚鳳凰飛彈，同時攔截6個在160公里（100英哩）外的目標。在當時，這是難以想像的能力。

由於只有F-14能夠攜帶鳳凰飛彈，使得雄貓成為美國當時唯一配備視距外[射後不理空對空飛彈的機種](../Page/射後不理.md "wikilink")。但高達6,000磅的飛彈重量已經遠超過越戰時執行傳統轟炸任務的載重。雄貓隻能最多掛着二到四枚飛彈返回航空母艦、餘下導彈則即使沒發射過也必須白白丢進海裏，否則損失的就是整架飛機（甚至是飛行員）了。儘管有很高的評價，但是鳳凰飛彈無法在近距離下使用。直到2005年在美國海軍除役之前，也少有實戰經驗。

到了1990年代，麻雀飛彈的可靠性已經遠超過其在越戰的表現。在[沙漠風暴中](../Page/海灣戰爭.md "wikilink")，麻雀飛彈包辦了最多的擊落數目，並能有效的對抗極速達三馬赫的[MiG-25狐蝠](../Page/米格-25戰鬥機.md "wikilink")。儘管[美國空軍沒有採用鳳凰飛彈](../Page/美國空軍.md "wikilink")，而持續改進[AIM-47](../Page/AIM-47.md "wikilink")／[YF-12組合的纏鬥性能](../Page/YF-12戰鬥機.md "wikilink")，他們還是期盼能夠像是海軍一般配備射後不理的飛彈。美國空軍需要一種新的飛彈，能裝在小如[F-16輕型戰機上](../Page/F-16戰隼戰鬥機.md "wikilink")，也能使用原先在[F-4幽靈上掛載麻雀飛彈的空間](../Page/F-4幽靈II戰鬥機.md "wikilink")。它必須讓[F-22猛禽如同舊式](../Page/F-22猛禽戰鬥機.md "wikilink")[F-106三角鏢一樣使用內載彈艙](../Page/F-106三角鏢.md "wikilink")，來減少[雷達反射面積](../Page/雷達反射面積.md "wikilink")（Radar
Cross
Section，RCS）。海軍也渴望爲即將取代F-14雄貓戰鬥機的[F/A-18E/F超級大黃蜂上加入這種能力](../Page/F/A-18E/F超級大黃蜂式打擊戰鬥機.md "wikilink")。

## 發展

[AMRAAM是](../Page/AMRAAM.md "wikilink")[美國政府跟幾個](../Page/美國.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")[北約成員國關於發展空對空導彈及分享相關生產技術的協議的產物](../Page/北約.md "wikilink")，但是這個協議目前已經失效。根據該協定，美國負責開發下一代中距離空對空導彈，也就是AMRAAM，北約歐洲成員國將負責開發下一代短程空對空導彈，也就是[AIM-132先進短程空對空飛彈](../Page/AIM-132先進短程空對空飛彈.md "wikilink")。該協議的終止導緻歐洲發展一種跟AMRAAM競爭的導彈[MBDA](../Page/MBDA.md "wikilink")（綽號：[流星飛彈](../Page/流星飛彈.md "wikilink")），美國則繼續升級[AIM-9響尾蛇飛彈](../Page/AIM-9響尾蛇飛彈.md "wikilink")。經過持續開發，AMRAAM在1991年9月開始部署。

俄國陣營對應的飛彈型號是[R-77](../Page/Vympel_R77.md "wikilink")（AA-12
Adder，西方通常稱為俄國的「AMRAAMski」）。

## 運作方式

[CATM_120C_AMRAAM_p1230119.jpg](https://zh.wikipedia.org/wiki/File:CATM_120C_AMRAAM_p1230119.jpg "fig:CATM_120C_AMRAAM_p1230119.jpg")
[FA-18_Hornet_VX-4_with_10_AMRAAM.jpg](https://zh.wikipedia.org/wiki/File:FA-18_Hornet_VX-4_with_10_AMRAAM.jpg "fig:FA-18_Hornet_VX-4_with_10_AMRAAM.jpg")
[AIM-120A_061215-F-0000O-101.jpg](https://zh.wikipedia.org/wiki/File:AIM-120A_061215-F-0000O-101.jpg "fig:AIM-120A_061215-F-0000O-101.jpg")
[AIM-9_AIM-120_and_AGM-88_on_F-16C.jpg](https://zh.wikipedia.org/wiki/File:AIM-9_AIM-120_and_AGM-88_on_F-16C.jpg "fig:AIM-9_AIM-120_and_AGM-88_on_F-16C.jpg")上的[AIM-9](../Page/AIM-9響尾蛇飛彈.md "wikilink")、**AIM-120**、[AGM-88](../Page/AGM-88飛彈.md "wikilink")
\]\]
[F-15C_AIM-9_AIM-120_m02006120700063.jpg](https://zh.wikipedia.org/wiki/File:F-15C_AIM-9_AIM-120_m02006120700063.jpg "fig:F-15C_AIM-9_AIM-120_m02006120700063.jpg")掛載四枚AIM-120的機腹\]\]
[Aircraft_Combat_Archer_(2565196807).jpg](https://zh.wikipedia.org/wiki/File:Aircraft_Combat_Archer_\(2565196807\).jpg "fig:Aircraft_Combat_Archer_(2565196807).jpg")發射AIM-120
\]\]
AMRAAM具有[全天候](../Page/全天候.md "wikilink")、[超視距作戰](../Page/超視距作戰.md "wikilink")（[BVR](../Page/BVR.md "wikilink")）的能力，它增進美國和其盟友未來在空戰中的優勢。AMRAAM將取代AIM-7麻雀飛彈成為新一代的空對空飛彈，它比以往的飛彈飛得更快、更小、更輕，也更能有效地對付低空目標。內部整合的主動雷達、慣性基準元件和微電腦設備也減少AMRAAM對載具火控系統的依賴性。

一旦飛彈接近目標，AMRAAM將會啟動本身的主動[雷達來攔截目標](../Page/雷達.md "wikilink")。這種稱為[射後不理的功能](../Page/射後不理.md "wikilink")，讓駕駛員不需持續地以雷達照明鎖定敵機，也讓駕駛員能同時攻擊數個目標，並在飛彈鎖定敵人後進行迴避動作。

## 導航系統

### 中途導引

攔截遠距離目標時，AMRAAM使用兩段式[導引](../Page/飛彈導引.md "wikilink")。發射時，會將目標的動態和飛彈發射的位置輸入到AMRAAM中。AMRAAM利用飛彈內的[慣性導航系統](../Page/慣性導航系統.md "wikilink")（INS）和這些資訊來攔截目標。機載[雷達](../Page/雷達.md "wikilink")、[紅外線搜索追蹤裝置](../Page/紅外線搜索追蹤裝置.md "wikilink")（[IRST](../Page/IRST.md "wikilink")）、[聯合戰術情報發布系統](../Page/聯合戰術情報發布系統.md "wikilink")（[JTIDS](../Page/JTIDS.md "wikilink")）或是[空中預警管制機](../Page/空中預警管制機.md "wikilink")（[AWACS](../Page/AWACS.md "wikilink")）都能提供目標的動態。

如果持續追蹤目標，飛彈內目標的資訊也會同時更新。AMRAAM會根據目標速率、方向的改變，來修正攔截路線，讓目標能成功的進入飛彈主動雷達的偵測距離，進行自我歸向導引。

不是所有AMRAAM用戶都決定購買中段升級方案，這限制了AMRAAM的有效性。[英國](../Page/英國.md "wikilink")[皇家空軍決定不購買中段升級方案去強化他們的](../Page/皇家空軍.md "wikilink")[龍捲風F3](../Page/旋風戰鬥機.md "wikilink")，因而使得實際測試時，未配備中段升級方案的AMRAAM，表現比不上配備[半主動雷達導引的](../Page/半主動雷達導引.md "wikilink")[天閃空對空飛彈](../Page/天閃空對空飛彈.md "wikilink")--AMRAAM本身的雷達的有效距離必然不及戰機的雷達。

### 終端導引

一旦飛彈接近目標並進入自我歸向導引時，AMRAAM會啟動主動雷達去尋找目標。如果目標出現在估計的位置或其附近，AMRAAM會將自己導引至目標。當在近距離空戰時（通常指視距範圍內，最遠10海裏），無需雷達鎖定可直接發射，AMRAAM發射後會立即啟動主動雷達，讓飛彈成為真正的[射後不理](../Page/射後不理.md "wikilink")，飛行員稱爲「Mad
Dog」，意爲「放瘋狗咬人」，這時AMRAAM會做幅度很小、類似桶滾的動作，以增大AMRAAM內雷達的搜索範圍，增加補獲目标的機率，但MADDOG下的AMRAAM有可能打着自己人，因爲任何空空導彈自身都不具備敵我識别能力，發射前的敵我識别靠的飛機自身體的IFF（敵我識别系統），而不是導彈。[北大西洋公約組織在](../Page/北大西洋公約組織.md "wikilink")[無線電中使用代號](../Page/無線電.md "wikilink")"PITBULL"來表示飛彈進入自我歸向導引模式，如同發射時使用的"Fox
Three"（意指發射主動雷達導引飛彈）。

## 擊落機率與戰術

### 標準情況

在進入終端模式之後，飛彈先進的[電子反反制](../Page/電子反反制.md "wikilink")（ECCM）能力與優秀的機動性，代表它在對付有閃避機動能量的目標時，直接命中或近距引爆殺傷的機率還是很高（大約在90%上下）。

[擊殺率](../Page/擊殺率.md "wikilink")（Probablity of
Kill，PK）是由包括與目標相對夾角（位於目標前方、側方或是後方）、高度、飛彈與目標空速與目標迴轉極限等因素所決定。

通常飛彈在終端歸向階段擁有足夠的能量時（代表發射飛彈的飛機與目標距離不遠，同時飛行高度與速度皆足），擊中敵機的機率就非常的高。如果飛彈自遠距離發射，在接近目標時速度已經過低，或者是目標運動迫使飛彈必須跟隨，並且消耗許多速度而無法繼續追蹤時，擊中的機率就會大幅降低。

### 性能較差的目標

面對性能較差的目標有兩種接戰型態。如果目標機組無武裝或者是沒有攜帶任何中或長距離[射後不理的武器](../Page/射後不理.md "wikilink")，發射AMRAAM的飛機隻需要依據是正對或者是尾追目標，以及飛彈具有合理的命中機率來決定發射的距離。尤其在對付運動能力低的目標時，因為錯失的機會低而可以自遠距離外發射。假設目標和發射飛機處於接近的狀態，特別是在高接近率時，飛行距離持續快速縮短使得飛彈也可以自遠距離外發射。在這種狀況下，即便目標進行迴轉，也很難有機會在飛彈追上前加速並且拉開足夠的距離（只要飛彈並未過早發射）。此外他們也不太可能在這種高接近率下有足夠的能力閃避開來。在尾追的情況下，發射的飛機可能需要與目標拉近至一半到四分之一的最大射程以內發射（對速度更高的目標，需要更近的距離）才能夠讓飛彈追上。

如果目標有攜帶飛彈，AMRAAM具備的[射後不理的性能就更佳珍貴](../Page/射後不理.md "wikilink")，在發射之後飛機就可以轉向離開。即使目標配備遠程[半主動雷達導引](../Page/半主動雷達導引.md "wikilink")（Semi-Active
Radar
Homing，SARH）飛彈，他們也得要保持追擊才得以讓飛彈繼續追蹤，很容易會讓他們進入AMRAAM的射程範圍之內，要是他們發射半主動雷達導引飛彈之後就轉向，這些飛彈將不可能擊中目標。當然，假設目標機組配備遠程飛彈，就算不具備[射後不理能力](../Page/射後不理.md "wikilink")，迫使發射AMRAAM的飛機轉向離開就足以降低飛彈命中的機率，因為在欠缺中途更新目標資料的情況下很可能無法在最後階段找到敵機。即使受到這些因素影響，飛彈還是有挺高的機會命中目標，而且發射的飛機也能夠避開威脅，這就讓配備AMRAAM的飛機有先天上的優勢。若是發射出去的飛彈都沒有命中，飛機可以掉頭再度進行接戰，只是這樣一來在迴轉時損失的速度會讓他們比追擊的敵機較為不利，同時也得要避免被半主動導引飛彈鎖定。

### 同級的武裝目標

另外一種接戰的型態是對方配備像是[R-77這一類](../Page/R-77飛彈.md "wikilink")[射後不理的飛彈](../Page/射後不理.md "wikilink")
-
譬如[MiG-29](../Page/MiG-29戰鬥機.md "wikilink")、[Su-27或者是同類的戰機](../Page/Su-27.md "wikilink")。這時候團隊合作成為重要因素，或者說演變成「誰先怯場的競賽」。雙方都可以在[視距外就發射飛彈](../Page/視距外.md "wikilink")，然而接下來要面對的問題就是假如要持續追蹤目標以提供中途導引更新需要的資料，雙方都會進入對方的飛彈射程範圍之內，此刻特別突顯團隊支援的重要性，同時先進的飛彈與導引系統加上手不離桿的設計得以大幅減輕這方面的問題。另外一種主要的戰術是偷溜至敵機的後方發射飛彈，然後讓發射的飛機有足夠的時間脫離危險區域。即便敵機發現而且轉向攔截時，在這個過程當中損失速度，也許同時降低高度，會讓他們的飛彈處於能量劣勢而在發射後被對方成功閃躲。要達到這些目的需要優異的[地面管制攔截](../Page/地面管制攔截.md "wikilink")（GCI）或是[空中預警管制機的協助](../Page/空中預警管制機.md "wikilink")。

## 各種版本

### 空對空飛彈

現在AMRAAM有三個衍生型，全部都有在[美國空軍和](../Page/美國空軍.md "wikilink")[美國海軍服役](../Page/美國海軍.md "wikilink")。現在AIM-120A已不再生產，它與正在生產的後繼者AIM-120B共用較大的翼面。AIM-120C為了能被放進[F-22的內部彈倉](../Page/F-22.md "wikilink")，它的翼面被縮小了。AIM-120B於1994年開始交付，AIM-120C於1996年開始交付。

AIM-120C自從推出後便一直逐步升級。AIM-120C-6比它的前輩多一條改良過的信管（目標偵查設備）。1998年AIM-120C-7開始發展，改善了導向系統和更大航程（實際改良沒有公佈）。2003年它成功完成測試和投入服務（2005年初）。它幫助[美國海軍以](../Page/美國海軍.md "wikilink")[F-18替換即將退役的](../Page/F-18.md "wikilink")[F-14](../Page/F-14.md "wikilink")，AMRAAM可以部分抵銷因放棄[AIM-54的遠射導彈造成的問題](../Page/AIM-54.md "wikilink")，但要注意的是AMRAAM的射程不及AIM-54遠。

#### AIM-120A

  - 換裝WDU-33/B高爆破片彈頭

#### AIM-120B

主要更新如下：

  - 換裝新型WGU-41/B尋標器
  - 換裝新型數位處理器
  - 可程式化ROM
  - 五組電子元件升級

#### AIM-120C

主要更新如下：

  - 換裝縮小型彈翼
  - 可程式化電子反反制
  - 飛彈軟體升級

##### AIM-120C-4

主要更新如下：

  - 換裝改良型WDU-41/B彈頭以增進殺傷力

##### AIM-120C-5

主要更新如下：

  - 使用新型火箭發動機
  - 使用新型WPU-16/B推進段
  - 使用新型WCU-28/B控制段
  - 使用新型WDU-41/B高爆破片彈頭
  - 增強反電子幹擾能力
  - 新型縮小彈翼

#### AIM-120C-6

主要更新如下：

  - 為攔截巡弋飛彈，主要升級尋標頭。

##### AIM-120C-7\[1\]

主要更新如下：

  - 使用商用處理器
  - 更新飛彈軟體
  - 更新資料鏈
  - 增強電子反反制能力（ECCM）
  - 縮小尋標器
  - 換裝大推力火箭發動機

#### AIM-120C-8

#### AIM-120C-9

#### AIM-120P3-IP3

主要更新如下：

  - 使用新型雙脈衝火箭

#### AIM-120P3-IP4

主要更新如下：

  - 使用膠化燃料
  - 向量推進系統

#### AIM-120D

AIM-120D是AMRAAM的一個計畫升級的版本，它的性能比以前的版本強化了不少

主要更新如下：

  - 使用雙向資料鏈
  - 加裝[GPS導航](../Page/GPS.md "wikilink")，提升導航精度
  - 使用適形前端天線
  - 擴展無逃脫獵殺區範圍
  - 強化大角度離軸攻擊能力
  - 有效射程提高50%
  - 強化電子反反制性能
  - 選效幹擾反欺模組（Selective Availability Anti-Spoofing Module）

[雷神公司計畫開發](../Page/雷神公司.md "wikilink")[衝壓發動機推進的AMRAAM](../Page/衝壓發動機.md "wikilink")，即未來中程空對空導彈FMRAAM。自從設想目標客戶之一的英國國防部放棄FMRAAM而選擇[流星飛彈作為](../Page/流星飛彈.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")[颱風戰鬥機](../Page/颱風戰鬥機.md "wikilink")（Eurofighter
Typhoon）的視距外導彈後，FMRAAM是否生產尚未得而知。

### 地對空飛彈

[雷神公司成功自](../Page/雷神公司.md "wikilink")[悍馬車上安裝的](../Page/悍馬車.md "wikilink")5發飛彈架發射AMRAAM成功，飛彈是接收另外一具雷達傳遞過來的初始導引訊號（可能來自於一具[MPQ-64](../Page/MPQ-64.md "wikilink")[雷達或者是](../Page/雷達.md "wikilink")[愛國者飛彈陣地的雷達](../Page/愛國者飛彈.md "wikilink")），攔截低空近距離目標，而由愛國者飛彈負責高空遠程的目標。飛彈自地面發射時的射程會短於空射，這是因為發射時載具沒有速度與高度的緣故。這套系統被稱作「SLAMRAAM」（意指「地面發射的AMRAAM」）。

## NASAMS

(National Advanced Surface-to-Air Missile
System，NASAMS）是由[康斯伯格防衛與航太](../Page/康斯伯格防衛與航太.md "wikilink")（Kongsberg
Defence &
Aerospace）與雷神公司共同開發，是第一款實用化的陸基AMRAAM應用。整套系統包括可由地面車輛拖曳的發射器（每具發射器有6枚飛彈）、雷達和控制中心。目前總共有7個國家使用NASAMS，包括美國首都區域防空系統(National
Capital Region's air defense system)
、挪威、芬蘭、西班牙、丹麥....等等)。NASAMS也是目前北約國家中最廣泛部署的中短程防空飛彈系統。

## AMRAAM-ER

AMRAAM-ER是AMRAAM的增程形版本，2016年10月4號，雷神公司在挪威首次成功試射了最新版的AMRAAM-ER飛彈。這次試射是結合了AMRAAM-ER飛彈、NASAMS的發設器、Sentinel
Radar、Fire Distribution
Center(FDC)，作為NASAMS和新的飛彈的相容性以及飛彈本體的飛行測試。這次的測試中裝有彈頭的AMRAMM-ER型飛彈
成功擊落了作為目標的靶機。AMRAAM-ER的特色是在原先的AMRAAM飛彈的導引和裝藥部的基礎上，換裝了[ESSM，Evolved Sea
Sparrow
Missile先進海麻雀飛彈的火箭推進器來增加射程及射高以強化NASANS的性能](../Page/ESSM，Evolved_Sea_Sparrow_Missile.md "wikilink")，同時又必須符合經濟效益。這次測試中也證明了NASAMS中更新過的FDC可以有效的和新的AMRAAM-ER飛彈相容。

## 使用國家、單位

  - ：[美國空軍](../Page/美國空軍.md "wikilink")，[美國海軍與](../Page/美國海軍.md "wikilink")[美國海軍陸戰隊](../Page/美國海軍陸戰隊.md "wikilink")

  - ：[皇家澳洲空軍](../Page/皇家澳洲空軍.md "wikilink")

  - ：[皇家巴林空軍](../Page/皇家巴林空軍.md "wikilink")

  - ：[德國空軍](../Page/德國空軍.md "wikilink")

  - ：[芬蘭空軍](../Page/芬蘭空軍.md "wikilink")

  - ：[大韓民國空軍](../Page/大韓民國空軍.md "wikilink")

  - ：[皇家荷蘭空軍](../Page/皇家荷蘭空軍.md "wikilink")

  - ：[皇家挪威空軍](../Page/皇家挪威空軍.md "wikilink")

  - ：[中華民國空軍](../Page/中華民國空軍.md "wikilink")

  - ：[新加坡共和國空軍](../Page/新加坡共和國空軍.md "wikilink")

  - : [馬來西亞皇家空軍](../Page/馬來西亞皇家空軍.md "wikilink")

  - ：[瑞典空軍](../Page/瑞典空軍.md "wikilink")

  - ：[皇家空軍與](../Page/皇家空軍.md "wikilink")[皇家海軍](../Page/皇家海軍.md "wikilink")

  - ：[葡萄牙空軍](../Page/葡萄牙空軍.md "wikilink")

  - ：[以色列空軍](../Page/以色列空軍.md "wikilink")

  - ：[西班牙空軍](../Page/西班牙空軍.md "wikilink")，[西班牙海軍與](../Page/西班牙海軍.md "wikilink")[西班牙陸軍](../Page/西班牙陸軍.md "wikilink")

  - ：[土耳其空軍](../Page/土耳其空軍.md "wikilink")

  - ：[加拿大空軍](../Page/加拿大空軍.md "wikilink")

  - ：[巴基斯坦空軍](../Page/巴基斯坦空軍.md "wikilink")

  - ：[智利空軍](../Page/智利空軍.md "wikilink")

  - ：[航空自衛隊](../Page/航空自衛隊.md "wikilink")

## 搭載平台

  - ：[美國空軍](../Page/美國空軍.md "wikilink")，[美國海軍與](../Page/美國海軍.md "wikilink")[美國海軍陸戰隊](../Page/美國海軍陸戰隊.md "wikilink")

      - [F-14雄貓式戰鬥機](../Page/F-14雄貓式戰鬥機.md "wikilink")（退役）
      - [F-15鷹式戰鬥機](../Page/F-15鷹式戰鬥機.md "wikilink")（各機型）
      - [F-16戰隼式戰鬥機](../Page/F-16戰隼式戰鬥機.md "wikilink")（僅限於F-16A/B以後之機型）
      - [F/A-18黃蜂式戰鬥攻擊機](../Page/F/A-18黃蜂式戰鬥攻擊機.md "wikilink")（各機型）
      - [F-22猛禽戰鬥機](../Page/F-22猛禽戰鬥機.md "wikilink")
      - [F-35閃電II戰鬥機](../Page/F-35閃電II戰鬥機.md "wikilink")

  - ：[中華民國空軍](../Page/中華民國空軍.md "wikilink")

      - [F-16戰隼式戰鬥機](../Page/F-16戰隼式戰鬥機.md "wikilink")([F-16 A/B Block
        20](../Page/F-16_Block_20.md "wikilink"))（F-16V）

## 相關條目

  - [空對空飛彈](../Page/空對空飛彈.md "wikilink")
  - [視距外](../Page/視距外.md "wikilink")
  - [主動雷達導引](../Page/主動雷達導引.md "wikilink")
  - [半主動雷達導引](../Page/半主動雷達導引.md "wikilink")

### 其他現役空對空飛彈

  - [霹靂-12導彈](../Page/霹靂-12導彈.md "wikilink")
  - [天劍二型飛彈](../Page/天劍二型飛彈.md "wikilink")
  - [AIM-7麻雀飛彈](../Page/AIM-7麻雀飛彈.md "wikilink")
  - [AA-12](../Page/AA-12.md "wikilink")
  - [MICA](../Page/MICA.md "wikilink")
  - [AA-10飛彈](../Page/AA-10飛彈.md "wikilink")
  - [AAM-4飛彈](../Page/AAM-4飛彈.md "wikilink")

### 發展中

  - [Astra missile](../Page/Astra_missile.md "wikilink")
  - [流星飛彈](../Page/流星飛彈.md "wikilink")

## 參考資料

## 外部連結

  - [Federation of American Scientists
    page](http://www.fas.org/man/dod-101/sys/missile/aim-120.htm)
  - [GlobalSecurity.org
    page](http://www.globalsecurity.org/military/systems/munitions/aim-120.htm)
  - [GlobalSecurity.org HUMRAAM
    page](http://www.globalsecurity.org/military/systems/munitions/claws.htm#CLAWS)
  - [Designation-Systems
    page](http://www.designation-systems.net/dusrm/m-120.html)
  - [FMRAAM at
    Global-Defence.com](https://web.archive.org/web/20061016072611/http://www.global-defence.com/2000/pages/fraam.html)
  - [Meteor vs. FMRAAM at
    Global-Defence.com](https://web.archive.org/web/20061016072611/http://www.global-defence.com/2000/pages/fraam.html)
  - [NATO brevity
    words](http://www.kangaldogs.net/training/docs/nato.htm)
  - [Raytheon: AIM-120
    AMRAAM](https://web.archive.org/web/20060927190201/http://www.raytheon.com/products/stellent/groups/rms/documents/content/cms01_054563.pdf)
  - [More HUMRAAM
    information](https://web.archive.org/web/20060404103548/http://www.deagel.com/pandora/?p=pm00003001)
  - [NASAMS (Kongsberg Defence & Aerospace official
    information)](https://web.archive.org/web/20060311084334/http://www.kongsberg.com/eng/kda/products/Aircraft/NASAMS/)
  - [NASAMS (third-party
    information)](http://www.gbad.org/gbad/amd_nasams.html)
  - [F-16.net網站對AIM-120的介紹（英文）](http://www.f-16.net/f-16_armament_article3.html)

[Category:空對空飛彈](../Category/空對空飛彈.md "wikilink")
[Category:中華民國空軍飛彈](../Category/中華民國空軍飛彈.md "wikilink")

1.  [空軍計畫採購最新先進空對空飛彈](http://udn.com/NEWS/BREAKINGNEWS/BREAKINGNEWS1/6851379.shtml#ixzz1jgpwIaZN)