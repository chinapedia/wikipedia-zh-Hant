**AIM**（**美國在線即時通訊**、**AOL即時通**）是一個由[AOL出版以廣告收入來支持的個人](../Page/美國線上.md "wikilink")[即時通訊軟件](../Page/即時通訊軟件.md "wikilink")。由AOL發布於1997年5月，使用OSCAR立即傳訊協定和TOC協定。
於2017年12月15日停止服務。\[1\]

## 參考資料

## 外部連結

  - [官方AIM網站](http://www.aim.com/)
  - [AIM Express](http://aimexpress.aol.com)
  - [AIM
    Lite](https://web.archive.org/web/20100527184250/http://x.aim.com/laim/about.php)

[Category:即时通讯软件](../Category/即时通讯软件.md "wikilink")
[Category:美國線上公司](../Category/美國線上公司.md "wikilink")
[Category:Mac OS即時通訊客戶端](../Category/Mac_OS即時通訊客戶端.md "wikilink")
[Category:Mac OS X即時通訊](../Category/Mac_OS_X即時通訊.md "wikilink")
[Category:Windows即時通訊客戶端](../Category/Windows即時通訊客戶端.md "wikilink")

1.  [1](https://help.aol.com/articles/aim-discontinued)