**AJ·史都華**（，本名，）是《[全美超級模特兒新秀大賽](../Page/全美超級模特兒新秀大賽.md "wikilink")》[第七季的參賽者](../Page/全美超級模特兒新秀大賽第七季.md "wikilink")，來自[加州的沙加緬度市](../Page/加州.md "wikilink")。她是比賽中第五個被淘汰的參賽者。\[1\]

## 早年生平

出生於九月八日。

A.J.在St. Francis High
School高中畢業，一間在[加州](../Page/加州.md "wikilink")[沙加緬度市的天主教老式中學](../Page/沙加緬度.md "wikilink")。高中畢業後，她搬到[洛杉磯繼續學業](../Page/洛杉磯.md "wikilink")。她比賽前沒有模特兒經驗，以及未看過任何一集《[全美超級模特兒新秀大賽](../Page/全美超級模特兒新秀大賽.md "wikilink")》。

A.J.曾患子宮頸癌，現已痊癒。

## 超模大賽

從比賽一開始，泰雅叫她做未經打磨的鑽石。之後順利打入十三強，而且一開始拍下高水準的相片，受到評判的稱讚。她也勝出了走步的挑戰，而且和兩個朋友（Megg
& CariDee）一起分享獎品，她們可以到德州進行時裝展。

她在第六集被淘汰，因為沒有爭勝心和熱誠去繼續比賽。另外，她也曾當上兩次《Cover Girl of the Week》。

## 比賽過後

她在2006年10月25日的《[泰雅賓絲脫口秀](../Page/泰雅·賓絲#泰雅賓絲脫口秀.md "wikilink")》中出現。\[2\]她說會繼續做模特兒。

A.J.在2007年冬季到亞洲發展，包括[韓國和](../Page/韓國.md "wikilink")[香港](../Page/香港.md "wikilink")，拍攝當地時裝雜誌的內頁，還登上了封面，在香港登上時裝雜誌《Jessica》，並成為手袋品牌《Cervo》的代言模特兒，發展相當不俗。

## 注釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [AJ的Fanpage](http://myspace.com/ajsfanpage)
  - [AJ 支持者的Myspace Group](http://groups.myspace.com/AJStewart)
  - [AJ's fan livejournal
    community](http://community.livejournal.com/team_aj/)
  - [AJ的ANTM資料](http://www.cwtv.com/page/topmodel_photosView_AJ.html)
  - [E\!的訪問](http://www.eonline.com/gossip/kristin/blog/index.jsp?uuid=e593475c-3dbe-4f30-80bb-f644b3c62fe8)
  - [AJ的Myspace](https://web.archive.org/web/20070310234033/http://myspace.com/topmodelaj)

[Category:全美超級模特兒新秀大賽](../Category/全美超級模特兒新秀大賽.md "wikilink")
[Category:美國女性模特兒](../Category/美國女性模特兒.md "wikilink")
[Category:加州人](../Category/加州人.md "wikilink")

1.  <http://www.cwtv.com/page/topmodel_episodedetail5.html>
2.  <http://tyrashow.warnerbros.com/newsletter/102306/index.html>