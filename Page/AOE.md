**AOE**或**AoE**可以指：

  - （《[-{zh-cn:帝国时代; zh-tw:世紀帝國;}-](../Page/世紀帝國.md "wikilink")》），一種電腦遊戲

  - （[入境航空站](../Page/入境航空站.md "wikilink")），一種提供海關和移民業務的[機場](../Page/機場.md "wikilink")

  - （[艾伯塔卓越獎](../Page/艾伯塔卓越獎.md "wikilink")），[加拿大](../Page/加拿大.md "wikilink")[艾伯塔省最高的公眾獎項](../Page/艾伯塔.md "wikilink")

  - ，20世纪八十年代[美國的一種軍隊組織結構](../Page/美國.md "wikilink")

  - ，一種儲存網路化的技術及協定

  - （[AOE網](../Page/AOE網.md "wikilink")）（常作Activity On Edge
    Network），一種常用來表示工程的帶權有向圖

  - ，一種通過以太網傳輸數字音頻的技術

  - （[邪惡軸心](../Page/邪惡軸心.md "wikilink")），來源於[美国总统](../Page/美国总统.md "wikilink")[乔治·沃克·布什于](../Page/乔治·沃克·布什.md "wikilink")2002年1月29日發表的[國會演講](../Page/國會演講.md "wikilink")

  - "AOE"，美國海軍船體分級符號，代表著快速作戰支援艦（）

  - ，一种游戏术语，指范围性作用技能