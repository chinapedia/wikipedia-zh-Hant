**APS**可以指：

## 技术

  - [先進規劃排程系統](../Page/先進規劃排程系統.md "wikilink")（）

  - [先進攝影系統](../Page/先進攝影系統.md "wikilink")（）

  -
  - ，一种[DVD的防复制技术](../Page/DVD.md "wikilink")

  - [IBM](../Page/IBM.md "wikilink")
    [Thinkpad](../Page/Thinkpad.md "wikilink")
    笔记本电脑的硬盘[主动保护系统](../Page/主动保护系统.md "wikilink")（）

## 军事

  - [主動防衛系統](../Page/主動防衛系統.md "wikilink")（），通过主动干扰敌方武器来保护自身的防御系统
  - [斯捷奇金自動手槍](../Page/斯捷奇金自動手槍.md "wikilink")（）
  - [APS水下突擊步槍](../Page/APS水下突擊步槍.md "wikilink")（）

## 化学物质

  - [過硫酸銨](../Page/過硫酸銨.md "wikilink")（），在聚丙烯酰胺凝膠電泳（PAGE）中用作[自由基產生劑](../Page/自由基.md "wikilink")

  - [腺苷酰硫酸](../Page/腺苷酰硫酸.md "wikilink")（），微生物硫酸還原過程的中間產物

  -
## 组织

  - [留德人員審核部](../Page/留德人員審核部.md "wikilink")（），[德國駐華大使館審核留學德國人員的機構](../Page/德國駐華大使館.md "wikilink")

  - [美国哲学会](../Page/美国哲学会.md "wikilink")（American Philosophical Society）

  - [美国物理学会](../Page/美国物理学会.md "wikilink")（American Physical Society）

  -
  -