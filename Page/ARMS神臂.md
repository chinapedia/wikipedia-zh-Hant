《**ARMS神臂**》，原名《**ARMS**》，是[日本漫畫](../Page/日本漫畫.md "wikilink")，作者為[皆川亮二](../Page/皆川亮二.md "wikilink")；動畫版名稱為《**PROJECT
ARMS**》。各ARMS的命名.

## 故事內容

  -
    巨大組織「艾格里」的外星生命「阿札桀爾」的專職研究機構，目的是研發新武器，為此目的「艾格里」培養了一群基因改造的小孩，小女孩「愛麗絲」正是這群小孩之一。但在「愛麗絲」及同伴企圖逃離「艾格里」時慘遭殺害，在「愛麗絲」的絕望之中，最初的「阿札桀爾」便和「愛麗絲」的精神結合，成為「愛麗絲」的化身，並誕生了「魔獸」、「騎士」、「白兔」、「心之女王」等4個ARMS的原核。

## 登場人物

### 主角與其同伴

  - 高槻涼（CV：[神奈延年](../Page/神奈延年.md "wikilink")）
    主角，高中二年級男生。（初登場時）
    ARMS持有：魔獸

<!-- end list -->

  - 新宮隼人（CV：[三木真一郎](../Page/三木真一郎.md "wikilink")）
    轉學生。
    ARMS持有：騎士

<!-- end list -->

  - 巴武士（CV：[上田祐司](../Page/上田祐司.md "wikilink")）
    ARMS持有：白兔

<!-- end list -->

  - 久留間恵（CV：[高山南](../Page/高山南.md "wikilink")）
    ARMS持有：心之女王

<!-- end list -->

  - 赤木勝美（CV：[三浦智子](../Page/三浦智子.md "wikilink")）
    高槻涼的青梅竹馬。
    ARMS持有：銀獠

### 基斯系列（複製人）

由科學家基斯·白利用其細胞複製所創造出來的複製人。 港譯：基使系列

  - 基斯·白（WHITE）港譯：基使·偉德
    一切的元兇，也是「基斯系列」的創造者。
    ARMS持有：神之卵

<!-- end list -->

  - 基斯．黑（BLACK）港譯：基使·布勒克
    現任「艾格里」的最高領導，也是基斯系列的兄長。
    ARMS持有：神之卵

<!-- end list -->

  - 基斯．藍（BLUE）港譯：基使·保路
    「藍人」組織的領導人。也是ARMS不適應者。
    ARMS持有：睡鼠

<!-- end list -->

  - 基斯．磊德（紅）（RED）台譯：基斯·磊德 港譯：基使·利頓

（CV：[速水獎](../Page/速水獎.md "wikilink")）

  -
    ARMS持有：神獸格利芬

<!-- end list -->

  - 基斯．銀（SILVER）港譯：基使·施爾巴

（CV：[川津泰彦](../Page/川津泰彦.md "wikilink")）

  -
    「艾格里」的最高幹部之一。
    ARMS持有：賣帽人

<!-- end list -->

  - 基斯．綠（GREEN）港譯：基使·古連

（CV：[私市淳](../Page/私市淳.md "wikilink")）

  -
    「艾格里」的最高幹部之一。基斯系列中最年少的一位。
    ARMS持有：魔貓

<!-- end list -->

  - 基斯．紫（VIOLET）港譯：基使·巴爾奧雷特

（CV：[桑島法子](../Page/桑島法子.md "wikilink")）

  -
    「艾格里」的最高幹部之一。也是基斯系列中唯一的女性。
    ARMS持有：三月兔
    名言：

<center>

    讓人停下腳步的不是「絕望」，而是「自我放棄」……
    促使人繼續前進的，不是「希望」，而是「意志」。

</center>

### 友方

  - 阿爾·伯恩（CV：[緒方惠美](../Page/緒方惠美.md "wikilink")）
    「艾格里」的「教堂之子」計畫所實驗出來的天才兒童。其雙胞胎弟弟傑夫，被「基斯·磊德」所殺。隨後貝高槻帶回家照顧，之後就一直跟著高槻一起行動，常提供很多線索幫助高槻一行人，但常因說得太深奧而被隼人敲頭最後甚至連久留間恵也會敲他的頭。

<!-- end list -->

  - 基爾博特·雨戈（CV：）
    世界最強的心電感應能力者，綽號「天使雨戈」。

<!-- end list -->

  - 高櫬巖（CV：[有本欽隆](../Page/有本欽隆.md "wikilink")）
    高槻涼的養父。世界最強的傭兵之一。忍者。綽號「風」，「寂靜之狼」。

<!-- end list -->

  - 高櫬美沙（CV：）
    高槻涼的養母。世界最強的傭兵之一。綽號「微笑母豹」。劇紅帽部隊隊長所稱她已經死了13次了。即便已退出傭兵這一行，但面對艾格里的改造人和紅帽部隊仍應付裕如，還在家裡藏有M60機槍，這一點連高槻都不知道。

<!-- end list -->

  - 兜光一（CV：[中村大樹](../Page/中村大樹.md "wikilink")）
    藍空警察署的刑警。在藍空市集結警察對反抗紅帽部隊，事件結束後辭去刑警一職，和高槻一行人到美國。

### 敵方組織

## ARMS

  - 阿爾札克
    自外太空而來的矽生命體。
    1.  最初的「阿爾札克」成為「艾格里」的超級電腦「愛麗絲」，並透過網路上的幼教網站與全世界的兒童直接對話。
    2.  第2顆「阿爾札克」，它為了救「教堂之子」基地的所有人，而分解自己的身體，把毒氣分解掉。
    3.  第3顆「阿爾札克」是漫畫版的最後希望。

<!-- end list -->

  - 愛麗絲
    愛麗絲善良及負面的情緒所凝聚的精神體，負面的精神體「黑愛麗絲」創造出魔獸毀滅世界，向「艾格里」復仇，善良的精神體「白愛麗絲」則持續對抗「黑愛麗絲」，創造出騎士、白兔以及心之女王牽制魔獸。

  - 魔獸

<!-- end list -->

  - 騎士

<!-- end list -->

  - 白兔

<!-- end list -->

  - 心之女王

<!-- end list -->

  - 神之卵

<!-- end list -->

  - 三月兔

<!-- end list -->

  - 魔貓

<!-- end list -->

  - 賣帽人

<!-- end list -->

  - 神獸格利芬

<!-- end list -->

  - 睡鼠

<!-- end list -->

  - 銀獠

## 相關產品

### 漫畫

### TV版動畫

#### 主題曲

  - OP

<!-- end list -->

  - 『Free Bird』

<!-- end list -->

  -
    （1 - 13話）
    作詞：舩木基有／作曲：岩井勇一郎／編曲、歌：[New Cinema
    蜥蜴](../Page/New_Cinema_蜥蜴.md "wikilink")

<!-- end list -->

  - 『Breathe on me』

<!-- end list -->

  -
    作詞：舩木基有／作曲：岩井勇一郎／編曲、歌：New Cinema 蜥蜴
    （14 - 26話）

<!-- end list -->

  - 『TIME WAITS FOR NO ONE』

<!-- end list -->

  -
    作詞：WAG、徳永暁人／作曲：[徳永暁人](../Page/徳永暁人.md "wikilink")／編曲、歌：[WAG](../Page/WAG.md "wikilink")
    （27 - 52話）

<!-- end list -->

  - ED

<!-- end list -->

  - 『Just wanna be』

<!-- end list -->

  -
    作詞：WAG／作曲、編曲：[徳永暁人](../Page/徳永暁人.md "wikilink")／歌：[WAG](../Page/WAG.md "wikilink")
    （1 - 13話）

<!-- end list -->

  - 『[call my name](../Page/call_my_name.md "wikilink")』

<!-- end list -->

  -
    作詞：[AZUKI七](../Page/AZUKI七.md "wikilink")／作曲：[中村由利](../Page/中村由利_\(歌手\).md "wikilink")／編曲：[古井弘人](../Page/古井弘人.md "wikilink")／歌：[GARNET
    CROW](../Page/GARNET_CROW.md "wikilink")
    （14 - 26話）

<!-- end list -->

  - 『[Timeless Sleep](../Page/Timeless_Sleep.md "wikilink")』

<!-- end list -->

  -
    作詞：AZUKI七／作曲：中村由利／編曲：古井弘人／歌：GARNET CROW
    （27 - 40話）

<!-- end list -->

  - 『』

<!-- end list -->

  -
    作詞、作曲：PROJECT ARMS／編曲：project-B／歌：PROJECT ARMS
    （41 - 52話）

#### 每集標題

<div class="references-small" style="-moz-column-count:4; column-count:4;">

</div>

### PS2遊戲

  - 『PROJECT ARMS』
    2002年3月28日，由日本[萬代發售](../Page/萬代.md "wikilink")[PlayStation
    2用的遊戲](../Page/PlayStation_2.md "wikilink")。\[1\]

## 外部連結

  - [ARMS
    COMICTHEATER](https://web.archive.org/web/20080509011603/http://www.shogakukan.co.jp/arms/top.htm)（[小學館的此漫畫官網](../Page/小學館.md "wikilink")）

  - [](http://www.tms-e.com/library/on_air_back/arms/)（TV版動畫官網）

  -
  - [Amazon.co.jp:
    ARMS：本](http://www.amazon.co.jp/s/ref=nb_ss_b?__mk_ja_JP=%83J%83%5E%83J%83i&url=search-alias%3Dstripbooks&field-keywords=ARMS)

  - [博客來網路書店 -
    目前您搜尋的關鍵字為：ARMS神臂](http://search.books.com.tw/exep/prod_search.php?cat=all&key=+%AF%AB%C1u+%AC%D2%A4t%ABG%A4G)

## 参考文献

[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:科幻漫畫](../Category/科幻漫畫.md "wikilink")
[Category:PlayStation 2游戏](../Category/PlayStation_2游戏.md "wikilink")
[Category:2001年日本電視動畫](../Category/2001年日本電視動畫.md "wikilink")
[Category:TMS
Entertainment](../Category/TMS_Entertainment.md "wikilink")
[Category:週刊少年Sunday](../Category/週刊少年Sunday.md "wikilink")
[Category:愛麗絲夢遊仙境題材作品](../Category/愛麗絲夢遊仙境題材作品.md "wikilink")
[Category:2002年电子游戏](../Category/2002年电子游戏.md "wikilink")
[Category:万代游戏](../Category/万代游戏.md "wikilink")

1.  [『PROJECT
    ARMS』遊戲的外盒封面](http://media.gamestats.com/gg/image/ProjectArms_PS2JPNBOXboxart_160w.jpg)