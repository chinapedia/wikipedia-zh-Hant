**ASIAN KUNG-FU GENERATION presents NANO-MUGEN COMPILATION**是[ASIAN
KUNG-FU
GENERATION主辦的搖滾音樂祭](../Page/ASIAN_KUNG-FU_GENERATION.md "wikilink")「NANO-MUGEN
FES.2006」中演出的所有藝人的合輯。在2006年7月5日由開始發售。初回限定盤特別以貼紙封好包裝。

## 收錄曲

1.  「十二進法の夕景」[ASIAN KUNG-FU
    GENERATION](../Page/ASIAN_KUNG-FU_GENERATION.md "wikilink")
      - 與「ブラックアウト（Black Out）」不同，並沒有收錄在他們的正式專輯裡。
2.  「ANOTHER TIME / ANOTHER STORY」BEAT CRUSADERS
3.  「恋愛スピリッツ」CHATMONCHY
4.  「Change」DREAM STATE
5.  「Stereoman」ELLEGARDEN
6.  「ダーティーな世界（Put your head）」髭（HiGE）
7.  「Have you ever seen the stars?(shooting star ver.)」MO'SOME
    TONEBENDER
8.  「Getting By」
9.  「Bubblegum」
10. 「The Nowarist」STRAIGHTENER
11. 「I Am For You」
12. 「Wake Up, Make Up, Bring It Up, Shake Up」

## 相關條目

  - [NANO-MUGEN
    FES.](../Page/ASIAN_KUNG-FU_GENERATION_presents_NANO-MUGEN_FES..md "wikilink")
  - [NANO-MUGEN
    COMPILATION](../Page/ASIAN_KUNG-FU_GENERATION_presents_NANO-MUGEN_COMPILATION.md "wikilink")
  - [NANO-MUGEN
    2008](../Page/ASIAN_KUNG-FU_GENERATION_presents_NANO-MUGEN_COMPILATION_2008.md "wikilink")

[Category:ASIAN KUNG-FU
GENERATION的專輯](../Category/ASIAN_KUNG-FU_GENERATION的專輯.md "wikilink")
[Category:2006年音樂專輯](../Category/2006年音樂專輯.md "wikilink")
[Category:合輯](../Category/合輯.md "wikilink")