**ASM太平洋**（、）是一家在[香港交易所上市的工業公司](../Page/香港交易所.md "wikilink")。主要業務是設計、製造及銷售[半導體工業所用之器材](../Page/半導體.md "wikilink")、工具及物料。ASM太平洋在[開曼群島註冊](../Page/開曼群島.md "wikilink")。

2010年，ASM太平洋收購[西門子旗下的Siemens](../Page/西門子.md "wikilink") Electronics
Assembly
Systems（簡稱：SEAS），拓展電子裝嵌系統市場。收購價為1[歐元](../Page/歐元.md "wikilink")，ASM並承諾會向SEAS增資2.02億元，並在3年內再提供2.02億元貸款融資。\[1\]

## 參考

[Category:香港製造公司](../Category/香港製造公司.md "wikilink")
[Category:香港電子公司](../Category/香港電子公司.md "wikilink")
[Category:香港交易所上市公司](../Category/香港交易所上市公司.md "wikilink")
[Category:香港上市資訊科技公司](../Category/香港上市資訊科技公司.md "wikilink")
[Category:美国半导体公司](../Category/美国半导体公司.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")
[Category:开曼群岛离岸公司](../Category/开曼群岛离岸公司.md "wikilink")

1.  [ASM上季賺6.8億破紀錄
    買西門子業務 30年來首收購](https://hk.finance.appledaily.com/finance/daily/article/20100730/14293212)
    蘋果日報，2010年7月30日。