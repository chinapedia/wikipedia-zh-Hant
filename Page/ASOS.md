**ASOS**（全寫：**Adult Sisters of
Shu**；中文名：**徐氏姊妹**）是一組[臺灣](../Page/臺灣.md "wikilink")[女子團體](../Page/女子團體.md "wikilink")，由[徐熙媛](../Page/徐熙媛.md "wikilink")（大S）和[徐熙娣](../Page/徐熙娣.md "wikilink")（小S）組成

ASOS舊名為**S.O.S**（全寫：**Sisters of
Shu**），但因合約糾紛而更換為現今的ASOS。且出道之初亦以**嘟比嘟哇**作為組合名稱（取自專輯《佔領年輕》中的歌曲《do
bi di wa》

## 簡介

ASOS的成員[徐熙媛和](../Page/徐熙媛.md "wikilink")[徐熙娣是一對姐妹](../Page/徐熙娣.md "wikilink")。她們在就讀[華岡藝校之後發行第一張專輯](../Page/華岡藝校.md "wikilink")《[佔領年輕](../Page/佔領年輕.md "wikilink")》並獲得非常大的迴響。高中畢業後，ASOS因為「S.O.S」在換經紀公司時發生合約問題被禁止出版有聲品、被迫更名為ASOS。

## 作品

以下僅列以SOS（ASOS）名義發表的作品，兩人的個別作品請參見[徐熙媛和](../Page/徐熙媛.md "wikilink")[徐熙娣之條目](../Page/徐熙娣.md "wikilink")。

### 音樂專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>《佔領年輕》</p></td>
<td style="text-align: left;"><p>1994年8月台灣首發，1995年8月日本首發。</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>《Best of SOS》</p></td>
<td style="text-align: left;"><p>1995年12月發行，日本限定精選。</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>《天天寄出的信》</p></td>
<td style="text-align: left;"><p>1995年1月發行。</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>《姐妹情深》</p></td>
<td style="text-align: left;"><p>1995年8月發行。</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>《我是女菩薩》</p></td>
<td style="text-align: left;"><p>1996年2月發行。</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>《顛覆歌》</p></td>
<td style="text-align: left;"><p>1996年12月發行。</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>《貝殼》</p></td>
<td style="text-align: left;"><p>1997年4月發行。</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>《變態少女》</p></td>
<td style="text-align: left;"><p>2001年12月發行。是ASOS離開波麗佳音、演藝事業主力轉向主持界後，簽約擎天娛樂發行的唯一專輯。</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 書籍

|           |            |                                                         |
| --------- | ---------- | ------------------------------------------------------- |
| **年份**    | **書名**     | **出版**                                                  |
| 1997年1月1日 | SOS超猛青春    | [皇冠出版社](../Page/皇冠出版社.md "wikilink")，ISBN 957-33-1356-1 |
| 1998年7月1日 | SOS東京拚裝大地圖 | 平裝本出版社，ISBN 957-803-189-0                               |
|           |            |                                                         |

### 節目主持

|                 |                                      |                                          |
| --------------- | ------------------------------------ | ---------------------------------------- |
| **年份**          | **頻道**                               | **節目**                                   |
| 1998年-2006年     | [八大綜合台](../Page/八大綜合台.md "wikilink") | [娛樂百分百](../Page/娛樂百分百.md "wikilink")     |
| 2001年           | [台視](../Page/台視.md "wikilink")       | [週末三寶FUN](../Page/週末三寶FUN.md "wikilink") |
| 2007年6月－2008年5月 | 八大綜合台                                | [大小愛吃](../Page/大小愛吃.md "wikilink")       |
|                 |                                      |                                          |

## 廣告代言

  - 1994年：

TOYOTA[汽車](../Page/汽車.md "wikilink")
台鈴SUZUKI莎比亞50[機車](../Page/機車.md "wikilink")

## 註釋或參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  -
[A](../Category/臺灣女子演唱團體.md "wikilink")
[Category:華語流行音樂團體](../Category/華語流行音樂團體.md "wikilink")