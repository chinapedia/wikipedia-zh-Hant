**美國電話電報公司**（），AT\&T 原為 American Telephone & Telegraph
的縮寫，是一間为[商業](../Page/商業.md "wikilink")、[客戶及政府機構](../Page/客戶.md "wikilink")，提供語音、視頻、數據和[互聯網專業服務的美國](../Page/互聯網.md "wikilink")[電訊公司](../Page/電訊.md "wikilink")。在悠久的历史中，美国电话电报公司曾经垄断了世界上电话及有限电视的运营。今天美國電話電報公司是
[AT\&T](../Page/AT&T.md "wikilink") 公司的旗下公司，並在之下提供橫跨長途電話服務。

在2005年美國電話電報公司被之一的 SBC 電訊以 160 億美元收購（現值約178億美元），並繼以 **AT\&T** 為名。

雖然美國電話電報公司作為AT\&T公司的子公司存在，但名字只會偶爾出現在AT\&T公司的新聞稿中\[1\]。

## 公司組建和擴張

[Lines_and_Metallic_Circuit_Connections,_American_Telephone_and_Telegraph_Co,_March_1,_1891_edit.jpg](https://zh.wikipedia.org/wiki/File:Lines_and_Metallic_Circuit_Connections,_American_Telephone_and_Telegraph_Co,_March_1,_1891_edit.jpg "fig:Lines_and_Metallic_Circuit_Connections,_American_Telephone_and_Telegraph_Co,_March_1,_1891_edit.jpg")

美國電話電報公司源於由[亞歷山大·格拉漢姆·貝爾及他的兩位從事金融的朋友](../Page/亞歷山大·格拉漢姆·貝爾.md "wikilink")[赫巴德及](../Page/加德納·格林·赫巴德.md "wikilink")成立的[貝爾電話公司](../Page/貝爾電話公司.md "wikilink")。
1879年3月更名為國家貝爾電話公司，第二年3月再更名為美國貝爾電話公司。
1881年美國貝爾電話公司從[西聯公司收購了](../Page/西聯匯款.md "wikilink")，三年前西聯公司拒絕了赫巴德報價10萬元出售的電話尃利權（現值225萬美元）。

1880年美國貝爾公司啟動“建立第一個商業上可行的國內長途電話網絡”的計畫，該計畫後，美國貝爾公司於1885年3月3日在紐約州成立美國電話電報公司。
1892年網絡已從[紐約敷設至](../Page/紐約.md "wikilink")[芝加哥](../Page/芝加哥.md "wikilink")。

貝爾出售的電話專利在1894年到期，但因公司擁有龐大的客戶群，加上設備仍較其他公司優良，所以公司業務仍大幅增加。

1899年12月30日美國電話電報公司收購美國貝爾。這是因為麻省的公司法限制公司資本額上限為一千萬美元，因而限制了美國電話電報公司的發展\[2\]。資產轉讓後，美國電話電報公司成為母公司\[3\]。

美國國內長途電話於1915已敷設至[三藩市](../Page/三藩市.md "wikilink")，從1927年起，使用雙向無線電提供跨大西洋服務，直至1956年全球第一條橫渡大西洋的同軸電纜投入使用為止。
[Att_new_logo.png](https://zh.wikipedia.org/wiki/File:Att_new_logo.png "fig:Att_new_logo.png")
1984年，[美國司法部依據](../Page/美國司法部.md "wikilink")《[反托拉斯法](../Page/反托拉斯法.md "wikilink")》拆分AT\&T，分拆出一個繼承了母公司名稱的新AT\&T公司（專營長途電話業務）和七個本地電話公司（即“貝爾七兄弟”），美國電信業從此進入了競爭時代。
1995年，又從公司中分離出了從事設備開發製造的[朗訊科技和](../Page/朗訊科技.md "wikilink")[NCR公司](../Page/NCR公司.md "wikilink")，只保留了通信服務業務。
2000年後，AT\&T又先後出售了[無線通信](../Page/無線通信.md "wikilink")，[有線電視和](../Page/有線電視.md "wikilink")[寬帶通信部門](../Page/寬帶通信.md "wikilink")。
2005年，原“小貝爾”之一的[西南貝爾](../Page/西南貝爾.md "wikilink")（SBC）對AT\&T兼併，合併後的企業繼承了AT\&T的名稱。
2007年，AT\&T開始在其提供固網電話的地區拓展[光纖電視服務](../Page/光纖電視.md "wikilink")，與[衛星電視和近年進入](../Page/衛星電視.md "wikilink")[寬帶電話市場的有線電視公司競爭](../Page/寬帶電話.md "wikilink")。
2014年5月，AT\&T宣布即將收購美國最大衛星電視運營商[DirecTV](../Page/DirecTV.md "wikilink")，以現金加股票方式進行交易，總額達485億美元(約合新台幣1.39兆元)\[4\]。

## 參考文獻

[At\&tPhone.JPG](https://zh.wikipedia.org/wiki/File:At&tPhone.JPG "fig:At&tPhone.JPG")
[Texasdd.JPG](https://zh.wikipedia.org/wiki/File:Texasdd.JPG "fig:Texasdd.JPG")（原“SBC中心”）\]\]

[A](../Category/道瓊斯工業平均指數成份股.md "wikilink")
[Category:AT\&T](../Category/AT&T.md "wikilink")
[Category:1885年成立的公司](../Category/1885年成立的公司.md "wikilink")

1.  [AT\&T Corp restructured its Asia Pacific Operations - AT\&T Press
    Release,
    Oct 31, 2007](http://www.att.com/gen/press-room?pid=4800&cdvn=news&newsarticleid=24633)
2.  Brooks 1976, pg107
3.  Brooks 1976, pg107
4.  [finance.sina.com/gb/tech/sinacn/20140519/10201036250.html
    AT\&T宣布485億美元收購美最大衛星電視運營商, May 19, 2014](http://)