**ATOK**（一般發音為，但一些人慣於發音為）是一款由日本軟體公司製作的[日語](../Page/日語.md "wikilink")[輸入法](../Page/輸入法.md "wikilink")，在1982年推出首個[商用版本](../Page/商業軟體.md "wikilink")，因其轉換的精確性而擁有極好的名聲。[昇陽選擇了ATOK作為](../Page/昇陽.md "wikilink")[Solaris
10或以上版本在日本](../Page/Solaris.md "wikilink")[區域設置運行下的輸入法](../Page/區域設置.md "wikilink")，加強了昇陽原已非常強大的日語支援。

JustSystems為嵌入式系統開發了+ATOK，一些日本的[手提電話](../Page/手提電話.md "wikilink")、[PDA](../Page/個人數碼助理.md "wikilink")、[遊戲機](../Page/遊戲機.md "wikilink")（[PSP](../Page/PlayStation_Portable.md "wikilink")、[PS3與](../Page/PS3.md "wikilink")[Wii](../Page/Wii.md "wikilink")）、[數位錄影機](../Page/數位錄影機.md "wikilink")、[汽車導航系統隨機供應](../Page/GPS.md "wikilink")+ATOK。

ATOK曾意為**A**utomatic **T**ransfer **O**f
[**K**ana](../Page/假名.md "wikilink")-[kanji](../Page/漢字.md "wikilink")（假名漢字自動轉換），現時意為**A**dvanced
**T**echnology **O**f **K**ana-kanji
Transfer（假名轉漢字高階技術）。一些人曾以為其意思為[**A**wa](../Page/阿波國.md "wikilink")-[**TOK**ushima](../Page/德島縣.md "wikilink")（阿波國-德島縣）或[**A**SCII](../Page/ASCII.md "wikilink")
**TO** [**K**anji](../Page/漢字.md "wikilink")（ASCII轉漢字）。

## 功能

不同的平台可能会有不同的功能，且下述的功能列表并不完整。

  - 通过发音检索[汉字](../Page/日語漢字.md "wikilink")（支援[罗马字及](../Page/日语罗马字.md "wikilink")[假名输入](../Page/日語假名.md "wikilink")）
  - 通过[部首检索汉字](../Page/部首.md "wikilink")
  - 手写识别
  - 检索汉字的读音（音读及训读）
  - 提供常用语、地址及[颜文字的](../Page/颜文字.md "wikilink")[模板](../Page/模板.md "wikilink")
  - 针对日文及英文单词的实时自动补全（例如，输入英文“fun”，即可得到以“fun”开头的一系列英文单词“function”、“fun”、“fund”、“funk”……）
  - 字符信息，[JIS及](../Page/JIS.md "wikilink")[Unicode字符](../Page/Unicode.md "wikilink")，读音，相关字符
  - 日式日期格式
  - 特殊字符面板，可以输入[特殊符号](../Page/特殊符号.md "wikilink")、注音罗马字、俄文字符等
  - 自定义键盘快捷键

截止至2010年2月，最新的[Windows版本为ATOK](../Page/Windows.md "wikilink")
2010，[Macintosh版本为ATOK](../Page/Macintosh.md "wikilink")
2009，[Linux版本为ATOK](../Page/Linux.md "wikilink") X3。

## 参见

  - [一太郎](../Page/一太郎.md "wikilink")：一款由Justsystems製作的日語[文字處理器](../Page/文字處理器.md "wikilink")，當中有綑綁銷售ATOK。

## 外部連結

  - [ATOK.com](http://www.atok.com/)

[Category:日文輸入法](../Category/日文輸入法.md "wikilink")
[Category:日語書寫系統](../Category/日語書寫系統.md "wikilink")
[Category:好設計獎](../Category/好設計獎.md "wikilink")