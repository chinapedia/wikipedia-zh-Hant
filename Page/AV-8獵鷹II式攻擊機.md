**AV-8「獵鷹II式」**（[英語](../Page/英語.md "wikilink")：AV-8 Harrier
II）是美國引自[英國航太的](../Page/英國航太.md "wikilink")[獵鷹式戰鬥攻擊機版本](../Page/海鹞战斗攻击机.md "wikilink")。AV-8是[美國海軍陸戰隊的現役](../Page/美國海軍陸戰隊.md "wikilink")[攻擊機](../Page/攻擊機.md "wikilink")，能夠短距起飛／[垂直起降](../Page/垂直起降.md "wikilink")（STOVL）。該攻擊機由[英國航太公司設計](../Page/英國航太.md "wikilink")，[麥道公司製造](../Page/麥道.md "wikilink")。目前服役的型號為**AV-8B**和**AV-8B+**，兩者間的不同為後者經過改良，使用由[F/A-18退換下來的](../Page/F/A-18.md "wikilink")[AN/APG-65雷達](../Page/AN/APG-65雷達.md "wikilink")。

## 歷史

AV-8並非[美國自行研發的機種](../Page/美國.md "wikilink")，而是現役中非常少數從外國取得生產權的[武器系統](../Page/武器.md "wikilink")。AV-8的原始設計來自[英國的](../Page/英國.md "wikilink")[霍克西德利鷂式戰鬥機](../Page/霍克西德利鷂式戰鬥機.md "wikilink")（BAe
Harrier），在美國生產的編號為**AV-8A**，用作[近距空中支援和](../Page/近距空中支援.md "wikilink")[偵察](../Page/偵察.md "wikilink")。

有鑑於AV-8A的性能不完全滿足[美國海軍陸戰隊的需要](../Page/美國海軍陸戰隊.md "wikilink")，尤其是在載彈量上面。因此，負責生產的[麥道飛機公司加以改良](../Page/麥克唐納-道格拉斯公司.md "wikilink")，將6架AV-8A改成為AV-8B。主要的改動包括艙蓋改為水滴形、更新電子系統及[外掛掛架改為](../Page/外掛掛架.md "wikilink")7個，可掛載[AIM-9響尾蛇飛彈](../Page/AIM-9響尾蛇飛彈.md "wikilink")、[AGM-65小牛](../Page/AGM-65小牛飛彈.md "wikilink")[反坦克導彈等](../Page/反坦克導彈.md "wikilink")。而改良的技術與採用的新系統也交給英國使用在他們的獵鷹II式與海獵鷹II式（海軍版）上，並投入量產，B型初始造價為2370萬[美元](../Page/美元.md "wikilink")。\[1\]

## 發展型

### 原始版本

#### AV-8A

美國海軍陸戰隊共購買了102架AV-8A，第一架AV-8A於1971年交付美國海軍陸戰隊，直至1977年才被全部交付。由始至終，美國並沒有將AV-8A用作實戰。
AV-8A的一大缺點是酬弹量與航程不足，霍克西德利公司和麥道公司都很了解這一點，因此，在推出AV-8A，兩家公司就分頭開始尋求功能比AV-8A更先進的改良型。霍克西德利所走的方向是所謂的P.1184型；而在大西洋彼岸的麥道則有AV-8C的構想。

美國海軍陸戰隊剛開始訓練AV-8A的飛行員時，學員必須先接受10.5小時的A-4攻擊機飛行訓練和4.5小時的CH-46直升機飛行訓練，然後才轉飛AV-8A。在課程中加入CH-46的訓練，本意是要讓學員熟悉垂直起降的技巧。只是AV-8A服役初期的失事率相當高，後來海軍陸戰隊了解到AV-8A和直升機的垂直起降是截然不同的經驗，才亡羊補牢地訂購8架雙座的獵鷹Mk54型，訓練課程和遴選飛行員的方式也因此修改。這批獵鷹Mk54型的美軍編號為TAV-8A，於1975年7月16日作了第一次試飛，並全部運交第203攻擊訓練中隊使用，而AV-8A的失事率也自1978年起明顯降低。

### 改良版本

#### AV-8B

[YAV-8B_Harrier_testing_a_ski_jump.jpg](https://zh.wikipedia.org/wiki/File:YAV-8B_Harrier_testing_a_ski_jump.jpg "fig:YAV-8B_Harrier_testing_a_ski_jump.jpg")
[BAE-McDonell-Douglas_AV8B-01.jpg](https://zh.wikipedia.org/wiki/File:BAE-McDonell-Douglas_AV8B-01.jpg "fig:BAE-McDonell-Douglas_AV8B-01.jpg")
[AV-8B_Harrier.KC-10.drogue.jpg](https://zh.wikipedia.org/wiki/File:AV-8B_Harrier.KC-10.drogue.jpg "fig:AV-8B_Harrier.KC-10.drogue.jpg")
AV-8B由美國麥道公司和英國宇航公司聯合研製，於1983年開始服役。AV-8B製造工作的60％由麥克唐納·道格拉斯公司承擔，其餘40％由英國航宇公司承擔。對於西班牙、意大利等其他國家的訂單而言，麥克唐納·道格拉斯公司將負責飛機生產工作量的75％，英國航宇公司負責25％，各自負責機身內的系統安裝。\[2\]

AV-8B首次參戰為美國的[沙漠風暴行動](../Page/沙漠風暴.md "wikilink")，包括了VMA-311、VMA-513、VMA-231、VMA-223、VMA-331和VMA-542六個中隊，總數為86架。VMA-311、VMA-513、VMA-231、VMA-542中隊部署在距離[科威特邊境](../Page/科威特.md "wikilink")183[公里的](../Page/公里.md "wikilink")[沙特阿吉斯海軍基地的](../Page/沙特阿吉斯海軍基地.md "wikilink")[機場](../Page/機場.md "wikilink")，而VMA-223和VMA-331中隊則分別部署於[塔拉瓦號](../Page/塔拉瓦號.md "wikilink")（LHA-1）和[拿撒爾號](../Page/拿撒爾號.md "wikilink")（LHA-4）[兩棲突擊艦上](../Page/兩棲突擊艦.md "wikilink")。在整場[海灣戰爭中](../Page/海灣戰爭.md "wikilink")，AV-8B共出動了3,342架次，飛行小時為4317小時，且投放了2700噸以上的彈藥。在該戰爭中，VMA-542中隊損失2架AV-8B，而VMA-311、VMA-231、VMA-331中隊則分別損失1架，總擊落為5架，全數損失的AV-8B都是被地面防空火力擊落的。

AV-8B在減重上下了很大的工夫，其中，採用複合材料主翼是主要改善項目之一，據估計，以複合材料製造的主翼要比金屬做的同樣主翼輕了150公斤。AV-8B的機身前段也使用了大量的複合材料，估計減掉了大約68公斤的重量。其他採用複合材料的部分包括升力提升裝置、水平尾翼、尾舵，只有垂直尾翼、主翼與水平尾翼的前緣及翼端、機身中段及後段等處使用金屬質材。機身中後段使用金屬製造，乃是為了抗熱。
AV-8B的超臨界主翼比AV-8A的主翼厚，同時翼展增加20%，後掠角減少10%，面積增加14.5%，每邊也各增加一個掛架，導致AV-8B的飛行速度遜於AV-8A，但是在升力上的表現卻比AV-8A優異。主翼加厚的好處之一是可以提燃油的內容量，相當於增加了飛行30分鐘的油料。除了主翼內部AV-8B在機身前段與中段的兩側、機身後段的上半部均設有油箱，總燃油內容量為4,319公升，另外，機翼內側和中間的掛架都可以掛載副油箱，不過AV-8B加掛副油箱的情況並不多見。\[3\]

#### AV-8B+

AV-8B+於1996年服役使用了[F/A-18所用的](../Page/F/A-18.md "wikilink")[AN/APG-65雷達攻擊雷達](../Page/AN/APG-65雷達.md "wikilink")，機頭的光學傳感器改為一個整體的機頭雷達整流罩，強化全天候作戰能力。AV-8B+採用[英國](../Page/英國.md "wikilink")[勞斯萊斯公司的F](../Page/勞斯萊斯股份有限公司.md "wikilink")402-RR-408引擎。\[4\]

#### TAV-8B

TAV-8B是[美國海軍陸戰隊的雙座獵鷹II式攻擊機](../Page/美國海軍陸戰隊.md "wikilink")，用於作戰訓練。

## 使用國家

[Italian_TAV-8B_Harrier_II.jpg](https://zh.wikipedia.org/wiki/File:Italian_TAV-8B_Harrier_II.jpg "fig:Italian_TAV-8B_Harrier_II.jpg")。\]\]

  - [意大利海軍](../Page/意大利海軍.md "wikilink")（以[加里波底號航空母艦為基地](../Page/加里波底號航空母艦.md "wikilink")）

<!-- end list -->

  - [西班牙海軍](../Page/西班牙海軍.md "wikilink")（以[阿斯圖里亞斯親王號航空母艦為基地](../Page/阿斯圖里亞斯親王號航空母艦.md "wikilink")）

<!-- end list -->

  - [泰國皇家海軍](../Page/泰國皇家海軍.md "wikilink")（以[查克里·納呂貝特航空母艦為基地](../Page/查克里·納呂貝特航空母艦.md "wikilink")，自西班牙轉售）

<!-- end list -->

  - [英國皇家空軍](../Page/英國皇家空軍.md "wikilink")
      - [第一中隊](../Page/第一中隊.md "wikilink")
      - [第IV中隊](../Page/第IV中隊.md "wikilink")
      - [第20中隊](../Page/第20中隊.md "wikilink")
      - [RAF SAOEU打擊效用評估單位](../Page/RAF_SAOEU.md "wikilink")
  - [英國皇家海軍](../Page/英國皇家海軍.md "wikilink")
      - [800海軍航空中隊](../Page/800海軍航空中隊.md "wikilink")
      - [801海軍航空中隊](../Page/801海軍航空中隊.md "wikilink")

<!-- end list -->

  - [美國海軍陸戰隊](../Page/美國海軍陸戰隊.md "wikilink")
      - VMA-211
      - VMA-214
      - VMA-223
      - VMA-231
      - VMA-311
      - VMA-513
      - VMA-542
      - VMAT-203

## 規格

[McDONNELL_DOUGLAS,_BAe_AV-8B_HARRIER_II.png](https://zh.wikipedia.org/wiki/File:McDONNELL_DOUGLAS,_BAe_AV-8B_HARRIER_II.png "fig:McDONNELL_DOUGLAS,_BAe_AV-8B_HARRIER_II.png")

## 類似機種

  - [Yak-38](../Page/Yak-38.md "wikilink")
  - [Yak-141](../Page/Yak-141.md "wikilink")
  - [XFV-12](../Page/XFV-12.md "wikilink")
  - [JAS-39](../Page/JAS-39.md "wikilink")
  - [F-35](../Page/F-35閃電II戰鬥機.md "wikilink")

## 參考文獻

## 外部連結

  - [AV-8B Plus product
    page](http://www.boeing.com/boeing/history/mdc/harrier.page) at
    Boeing.com
  - [AV-8B Harrier II fact
    sheet](https://web.archive.org/web/20111204032951/http://www.navair.navy.mil/index.cfm?fuseaction=home.display&key=40EAA7E2-1C25-4857-A429-E2D7D16ED62B)
    and [AV-8B Harrier II history
    page](https://web.archive.org/web/20070401062508/http://www.history.navy.mil/planes/av8.htm)
    at Navy.mil
  - [AV-8B Harrier
    page](http://www.globalsecurity.org/military/systems/aircraft/av-8.htm)
    at Globalsecurity.org
  - [McDonnell Douglas/British Aerospace AV-8B Harrier II Attack Fighter
    page](http://www.aerospaceweb.org/aircraft/attack/av8/) on
    Aerospaceweb.org
  - [RTP-TV AeroSpace Show: Video of Harrier
    Hovering](http://www.rtptv.homestead.com/rtpharrier.html)
  - [3D view of Harrier
    AV-8B](http://www.virtualusmcmuseum.com/3-D_Models2.asp?pc_strURL=Tower_3&strModel=VR_Harrier)
    at the National Museum of the Marines Corps site

[Category:美國攻擊機](../Category/美國攻擊機.md "wikilink")
[Category:垂直起降機](../Category/垂直起降機.md "wikilink")
[Category:艦載機](../Category/艦載機.md "wikilink")
[Category:美國海軍陸戰隊裝備](../Category/美國海軍陸戰隊裝備.md "wikilink")

1.  [美陸戰隊的空中拳頭——AV-8B（獵鷹式）垂直起降攻擊機：:空軍世界](http://www.airforceworld.com/attacker/av8.htm)

2.  [美陸戰隊的空中拳頭——AV-8B（鷂）垂直起降攻擊機：:空軍世界](http://www.airforceworld.com/attacker/av8.htm)

3.  [1](http://tw.myblog.yahoo.com/jw!C43OWtqUAkasdQJtp8k-/article?mid=452)

4.  [美陸戰隊的空中拳頭——AV-8B（鷂）垂直起降攻擊機：:空軍世界](http://www.airforceworld.com/attacker/av8.htm)