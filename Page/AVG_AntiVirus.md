**AVG AntiVirus**, **AVG
Anti-Virus**是由[捷克公司](../Page/捷克.md "wikilink")所開發的[防毒軟體](../Page/防毒軟體.md "wikilink")。該公司原名「Grisoft」，自1991年成立。2000年，Grisoft首次榮獲
[VB100](../Page/VB100.md "wikilink") 獎。2008年，Grisoft正式更名為「AVG
Technologies CZ,
s.r.o.」。而在2008年，AVG推出的8.0版本則顛覆防毒專長的Anti-Virus版，另闢[網路安全的新途](../Page/網路安全.md "wikilink")，這一舉動也正趕上了2008年網路混合威脅浪潮。2016年9月，捷克的另一知名防毒軟體公司併購了AVG
Technologies\[1\]。

## 中文版

  - 2008年2月，AVG Technologies許可「哲想方案」代理銷售AVG
    Anti-Virus繁體中文版。而在2009年7月，提供繁體中文個人免費版下載。
  - 2009年7月\[2\]，AVG
    Anti-Virus提供简体中文版免费版下载，并一改原来的“免费版低调”态度，仿照[360殺毒宣传的](../Page/360殺毒.md "wikilink")“永久免费”等商业标语吸引用户。

## AVG 特性

  - AVG 版本區分：

<!-- end list -->

1.  免費版的部份僅提供：個人、[家庭和](../Page/家庭.md "wikilink")[非營利組織使用](../Page/非營利組織.md "wikilink")。
2.  商業版的部份則是提供：專業用戶、企業用戶之[商業性使用](../Page/商業.md "wikilink")。

<!-- end list -->

  - AVG -{zh:防毒; zh-cn:杀毒; zh-tw:防毒;}-特性：
    硬體的等級需求度並不高，所消耗的硬體資源低。
    軟體的[病毒定義檔更新快速](../Page/病毒.md "wikilink")，每天更新定義檔。
    加強以往的[作業系統的背景掃瞄](../Page/作業系統.md "wikilink")，有效提高系統防護能力。
    增加[瀏覽器對網頁連結掃瞄功能](../Page/瀏覽器.md "wikilink")，有效提高網頁防護能力。

### AVG Anti-Virus Free Edition

  - 軟體特性：

<!-- end list -->

1.  自由允許家庭和非營利組織使用者[免費使用與更新](../Page/免費.md "wikilink")。
2.  只供個人、[家庭和非營利組織使用](../Page/家庭.md "wikilink")，如作[商業性使用則屬非法行為](../Page/商業.md "wikilink")。

<!-- end list -->

  - 功能介紹：
    病毒防護（Anti-Virus）– 病毒、蠕蟲、木馬的防禦。
    間諜軟體防護（Anti-Spyware）–
    [間諜程式](../Page/間諜程式.md "wikilink")、[廣告軟體](../Page/廣告軟體.md "wikilink")、身份盜用（Identity
    Theft）的防禦。
    垃圾信防堵（Anti-Spam）–
    可以過濾[垃圾信件](../Page/垃圾信件.md "wikilink")、防止網路詐騙（[網路釣魚](../Page/網路釣魚.md "wikilink")）。
    網頁防護＆連結掃瞄（Web Shield & LinkScanner）– 讓使用者遠離惡意網站的危害。

### AVG Anti-Virus Professional

  - 軟體特性：

<!-- end list -->

1.  這是一款相當專業的商用防毒軟體。
2.  專用伺服器快速更新，在 Anti-Virus Free Edition 基礎上增加的功能。

<!-- end list -->

  - 功能介紹：
    病毒防護（Anti-Virus）– 病毒、蠕蟲、木馬的防禦。
    間諜軟體防護（Anti-Spyware）– 間諜程式、廣告軟體、身份盜用（Identity Theft）的防禦。
    垃圾信防堵（Anti-Spam）– 可以過濾垃圾信件、防止網路詐騙（網路釣魚）。
    網頁防護＆連結掃瞄（Web Shield & LinkScanner）– 讓使用者遠離惡意網站的危害。
    惡意軟體防護（Anti-Rootkit）– 威脅性惡意軟體（Rootkit）的防禦。

### AVG Anti-Virus plus Firewall

  - 軟體特性：

<!-- end list -->

1.  這是一款相當專業的商用且含有防火牆之防毒軟體。
2.  專用伺服器快速更新，在 Anti-Virus Professional 基礎上增加的功能。

<!-- end list -->

  - 功能介紹：
    病毒防護（Anti-Virus）– 病毒、蠕蟲、木馬的防禦。
    間諜軟體防護（Anti-Spyware）– 間諜程式、廣告軟體、身份盜用（Identity Theft）的防禦。
    垃圾信防堵（Anti-Spam）– 可以過濾垃圾信件、防止網路詐騙（網路釣魚）。
    網頁防護＆連結掃瞄（Web Shield & LinkScanner）– 讓使用者遠離惡意網站的危害。
    惡意軟體防護（Anti-Rootkit）– 威脅性惡意軟體（Rootkit）的防禦。
    [防火牆](../Page/防火牆.md "wikilink")（Firewall）– 協助使用者對抗駭客的攻擊。

### AVG Internet Security Suite

  - 軟體特性：

<!-- end list -->

1.  這是一款相當專業的商用且是全方位之防毒軟體。
2.  專用伺服器快速更新，在 Anti-Virus plus Firewall 基礎上增加的功能。

<!-- end list -->

  - 功能介紹：
    病毒防護（Anti-Virus）– 病毒、蠕蟲、木馬的防禦。
    間諜軟體防護（Anti-Spyware）– 間諜程式、廣告軟體、身份盜用（Identity Theft）的防禦。
    垃圾信防堵（Anti-Spam）– 可以過濾垃圾信件、防止網路詐騙（網路釣魚）。
    網頁防護＆連結掃瞄（Web Shield & LinkScanner）– 讓使用者遠離惡意網站的危害。
    惡意軟體防護（Anti-Rootkit）– 威脅性惡意軟體（Rootkit）的防禦。
    防火牆（Firewall）– 協助使用者對抗駭客的攻擊。
    系統工具（System Tools）– 協助使用者處理簡單的系統管理。

### 停止支援版本

  - AVG Anti-Virus 7.5 和 AVG Anti-Spyware 7.5 ，因這兩款軟體在 2008 年併為 AVG
    Anti-Virus 8.0。
  - AVG Anti-Spyware
    是款著名的防[木馬軟體](../Page/特洛伊木馬_\(電腦\).md "wikilink")，前身為[德國](../Page/德國.md "wikilink")
    Ewido Networks 公司所開發的軟體 Ewido Anti-Spyware。Ewido Network被 Grisoft
    收購後，Ewido Anti-Spyware 被正式改名為 AVG Anti-Spyware。

## 參見

  - [防毒軟體列表](../Page/防毒軟體列表.md "wikilink")

## 外部連結

  - [商業版官方網站](http://www.avg.com/)
  - [台灣官方網站](http://www.avgtaiwan.com)
  - [台灣免費版官方網站](http://free.avgtaiwan.com/)
  - [香港軟體代理商](http://grandsoft.com.hk/zh/products/avg.html)
  - [AntiVirus FREE：免費反病毒軟體- Google Play Android
    應用程式](https://play.google.com/store/apps/details?id=com.antivirus&hl=zh_TW)
  - [AVG Technologies - Google+](https://plus.google.com/+AVG/posts)

## 參考資料

[Category:殺毒軟體](../Category/殺毒軟體.md "wikilink")

1.
2.  强势出击 AVG简体中文版来袭  华军软件园测评中心，2009年7月7日