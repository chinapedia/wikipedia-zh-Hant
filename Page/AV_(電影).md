《**AV**》，又名《**青春夢工場**》，由[彭浩翔於](../Page/彭浩翔.md "wikilink")2005年執導的[香港電影](../Page/香港電影.md "wikilink")。該片獲得[金紫荆獎年度十大華語電影](../Page/金紫荆獎.md "wikilink")\[1\]，並獲選[法國](../Page/法国.md "wikilink")[多维尔亚洲电影节](../Page/多维尔亚洲电影节.md "wikilink")（Festival
du film asiatique de
Deauville）競賽單元之一\[2\]。本電影於2006年第十一屆香港電影金紫荊獎中獲得十大華語片獎。

根據《[電影檢查條例](../Page/電影檢查條例.md "wikilink")》，本片列為青少年及兒童不宜（IIB）。

## 劇情

故事講述一群青年學生為實現自己的夢想，向政府籌錢，請來日本女優拍攝[AV](../Page/AV.md "wikilink")。

## 作品特色

此套作品的副標題為「**在世界的中心呼喚性愛**」，揶揄同時期的一部日本電影《[在世界中心呼喚愛](../Page/在世界中心呼喚愛.md "wikilink")》。

## 演員

### 主演

  - [黃又南](../Page/黃又南.md "wikilink")：陳浩銘
  - [周俊偉](../Page/周俊偉.md "wikilink")：梁志安
  - [曾國祥](../Page/曾國祥.md "wikilink")：何寶華
  - [徐天佑](../Page/徐天佑.md "wikilink")：黃家樂
  - [周振輝](../Page/周振輝.md "wikilink")：麥啟光
  - [天宮真奈美](../Page/天宮真奈美.md "wikilink")：天宮真奈美
  - [國分康仁](../Page/國分康仁.md "wikilink")：暉峻創三，天宮真奈美經理人

### 客串

  - [錢嘉樂](../Page/錢嘉樂.md "wikilink")：動作指導
  - [張達明](../Page/張達明.md "wikilink")：護衛
  - [劉偉恆](../Page/劉偉恆.md "wikilink")：淫強
  - [鍾景輝](../Page/鍾景輝.md "wikilink")：公司老闆
  - [詹瑞文](../Page/詹瑞文.md "wikilink")：狼狗，色情光碟店老闆，安的舅父
  - [吳日言](../Page/吳日言.md "wikilink")：Cally，華女友
  - [葛民輝](../Page/葛民輝.md "wikilink")：貸款主任
  - [許紹雄](../Page/許紹雄.md "wikilink")：大學訓導主任
  - [黃文慧](../Page/黃文慧.md "wikilink")：賣偉哥的藥房老闆
  - [陳自瑤](../Page/陳自瑤.md "wikilink")：阿芝，阿肥的前女友
  - [董敏莉](../Page/董敏莉.md "wikilink")：Belinda
  - [張詠妍](../Page/張詠妍.md "wikilink")：Yoyo

## 参考資料

## 外部連結

  - {{@movies|fahk20452931|AV}}

  -
  -
  -
  -
  -
  -
[5](../Category/2000年代香港電影作品.md "wikilink")
[Category:香港劇情片](../Category/香港劇情片.md "wikilink")
[Category:美亞電影](../Category/美亞電影.md "wikilink")
[A](../Category/彭浩翔電影.md "wikilink")
[Category:電影題材電影](../Category/電影題材電影.md "wikilink")
[Category:色情題材作品](../Category/色情題材作品.md "wikilink")

1.  [歷屆得獎名單 -
    第十二屆香港電影金紫荊獎頒獎典禮](http://hk.movies.yahoo.com/gbaward/history.html)

2.  [2006 - Festival du Film Asiatique de
    Deauville](http://www.deauvilleasia.com/historique_2006.htm)