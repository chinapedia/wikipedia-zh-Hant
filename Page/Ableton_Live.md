***Ableton Live***是一套以[loop為基礎的](../Page/Music_loop.md "wikilink")[Music
Sequencer](../Page/Music_Sequencer.md "wikilink")，可在Macintosh及Windows上執行。相比於其他的[Software
Sequencer](../Page/Music_Sequencer.md "wikilink")，Live設計出來多數是用來作實時[樂器演奏多於作為一個作曲及編曲的工具](../Page/音樂樂器.md "wikilink")。從它的名字就可得知。

## 歷史

Live原本是在[Max/MSP中設計的](../Page/Max.md "wikilink")。由於想把它定位在商業的用途上，Ableton在1999年由[Gerhard
Behles](../Page/Gerhard_Behles.md "wikilink")，[Monolake的](../Page/Monolake.md "wikilink")[Robert
Henke及Bernd](../Page/Robert_Henke.md "wikilink")
Roggendorf所創立。Live被重新在[C++中編寫成一套商業的軟體](../Page/C++.md "wikilink")，第一版是在2001年推出的。截至2006年9月，Live已推出到6.01版。Ableton也同時開發了另一套軟體，[Operator](../Page/Ableton_Operator.md "wikilink")，一套在Live中用作音頻合成樂器的產品。現時Ableton的行政總裁是Behles，而技術總監是Henke，他也專注於Live's當中內置的效果器及樂器。Ableton's的總部位於[柏林中部](../Page/柏林.md "wikilink")。

## 功能

大部份Live's的獨特介面都是設計成用於實時表演的。它的介面比實際的sequencers更加小巧及清晰，以便在一個螢幕中使用。更完美的是它不會彈出訊息框及對話框。有部份的介面是可隨著鼠標的控制作出彈性隱藏及顯示
{如：隱藏樂器／效果器列表或顯示／隱藏幫助框）。另外，因為Live's是設計來用在表演用途，所以所有它的音效處理都是實時的，與其他普通需要預先演算的sequencers及sample編輯器不同。

## 參見

  - [多軌錄音軟體比較](../Page/多軌錄音軟體比較.md "wikilink")
  - [Pro Tools](../Page/Pro_Tools.md "wikilink")

## 外部連結

  - [Ableton官方网站](http://www.ableton.com)

  - [Ableton Live Yahoo User
    Group](http://groups.yahoo.com/group/ableton_live/)

  - [Unofficial Ableton Live DJ Forum & Ableton User's
    Website](http://www.abletonlivedj.com/)

  - [Unofficial Ableton Live
    Wiki](https://web.archive.org/web/20060717172313/http://www.teragon.org/wiki/index.php?title=Main_Page)

  - [Ableton 艺术品](http://www.abletonliveartwork.blogspot.com/)

[Category:音樂科技](../Category/音樂科技.md "wikilink")
[Category:应用软件](../Category/应用软件.md "wikilink")
[Category:數位音訊工作站軟體](../Category/數位音訊工作站軟體.md "wikilink")