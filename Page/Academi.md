**Academi**，\[1\]\[2\] 原稱：**黑水国际**（Blackwater Worldwide）、**Xe Services
LLC**、**美國黑水**（Blackwater
USA），是一家私人軍事、安全顧問公司。由前[海豹部隊軍官](../Page/海豹部隊.md "wikilink")[艾瑞克·普林斯與Al](../Page/艾瑞克·普林斯.md "wikilink")
Clark於1997年創立，公司總部位於美國的[維珍尼亞州的](../Page/維珍尼亞州.md "wikilink")[里斯頓](../Page/里斯頓.md "wikilink")。\[3\]
公司成立時只有6個人，[911事件後](../Page/911事件.md "wikilink")，業務突飛猛進，從2002年到2005年，營業額增長了600倍。該公司在推廣市場時自稱是「全球最全面的專業軍事、執法、安全、維持和平與穩定行動的公司」。它是美國在伊拉克與阿富汗的主要軍事任務承包商（Military
Contractor），負責建立與訓練伊拉克新陸軍、警察。該公司在2007年變得惡名昭彰，原因是當時[公司雇員在巴格達的尼蘇爾廣場殺害了14名伊拉克平民而被定罪](../Page/黑水公司巴格達槍擊案.md "wikilink")，其中有4名護衛在美國法院被判有罪。\[4\]\[5\]

美國黑水公司在[虐囚門事件中所扮演的幕後角色曝光後](../Page/虐囚門事件.md "wikilink")，它的業務與營運方式就成為輿論爭議與指責的焦點，黑水国际因一連串違規事件，在2009年1月被停止在伊拉克運作的牌照\[6\]，之前的合約執行至2009年9月為止\[7\]。

## 公司結構

Academi由下列子公司與獨立部門組成如下：

### 黑水訓練中心

對軍隊、政府和執法機構提供戰術和武器訓練。黑水訓練中心每年也會不定期舉辦些對外開放的短期訓練課程，例如徒手戰鬥課程、[狙擊手訓練課程等](../Page/狙擊手.md "wikilink")。在這裡受過訓練的有5萬多人。新進招募人員在經過嚴格的[背景調查](../Page/背景調查.md "wikilink")、犯罪紀錄調查以及通過體能測驗與[臨床心理學測驗後可進入黑水學院](../Page/臨床心理學.md "wikilink")（Blackwater
Academy）接受訓練，並於結訓後進入公司服務。

### 黑水標靶系統

此部門提供與維修靶場標靶與「shoothouse」系統。\[8\]

### 黑水安全顧問

黑水安全顧問（BSC）成立於2002年。它是伊拉克戰爭中受雇負責保護政府官員，建立與訓練伊拉克新陸軍、警察的60多家私營保安公司之一。\[9\]

黑水安全顧問有非常好的裝備，目前已知的有：

  - 直昇機，配屬於[快速反應小組](../Page/快速反應小組.md "wikilink")。

  - 直昇機。

  - 直昇機，使用於伊拉克。

  - 人員運輸裝甲車，主要使用於伊拉克[巴格達機場與](../Page/巴格達.md "wikilink")[綠區間的](../Page/綠區_\(巴格達\).md "wikilink")。

  - 的裝甲車。\[10\]

### 黑水飛艇公司

黑水飛艇公司成立於2006年1月，主要業務是建造及提供遙控飛艇（RPAV）監控服務。

### 黑水載具

黑水最近宣佈自製的人員運輸裝甲車。\[11\]

### 黑水海事

黑水海事提供海事保安單位的戰術訓練。

### 渡鴉開發群

為設計與建造Academi於北卡羅來納州的訓練中心於1997年成立。

### 環球航空服務

飛機維修與策略性運輸，擁有「Presidential Airways」與 「STI Aviation」兩家航空公司。Presidential
Airways聲稱獲得[美國國防部的機密設施使用許可](../Page/美國國防部.md "wikilink")。\[12\]

### 黑水傘兵小組（BWPT）

### 灰石有限

灰石有限註冊於[加勒比海與](../Page/加勒比海.md "wikilink")[大西洋邊界上的](../Page/大西洋.md "wikilink")[巴貝多](../Page/巴貝多.md "wikilink")，負責招募非美國籍的工作人員以執行美國境外的安全服務業務。

## 人事

黑水的創建人Erik Prince\[13\]、總裁Gary Jackson、執行副總裁Bill
Mathews和各個部門的負責人都是退役[海豹部隊隊員](../Page/海豹部隊.md "wikilink")。

## 設施

黑水在北卡羅來納州的設施佔地超過7,000 [英畝](../Page/英畝.md "wikilink")（28
km²），其中有數個室內、室外靶場、城市摸擬環境靶場。黑水在公司的簡介上說「全國最大的私營武器訓練設施」，事實上是全世界最大的武器訓練設施。

2006年11月，美國黑水宣佈取得位於芝加哥西邊的一處佔地80[英畝的設施](../Page/英畝.md "wikilink")。該設施目前以「北方黑水」的名稱營運。

黑水同時也試圖\[14\]於加州建立軍事武器訓練中心。\[15\]\[16\]\[17\]

## 主件

1.  2003年4月[虐囚門事件](../Page/虐囚門事件.md "wikilink")。
2.  2004年3月31日，4名美國黑水的雇傭兵在伊拉克[費盧傑遭到伏擊全部死亡](../Page/費盧傑.md "wikilink")，之後他們的屍體被伊拉克武裝分子和當地人焚燒，還拖著燒焦的屍體遊街。\[18\]\[19\]
3.  2005年4月一架美國黑水的[Mi-8直升機在伊拉克被擊落](../Page/Mi-8直升機.md "wikilink")，機上6名美籍傭兵、3名[保加利亞籍機師以及](../Page/保加利亞.md "wikilink")2名[斐濟槍手全部死亡](../Page/斐濟.md "wikilink")，乘員都是美國黑水的雇員\[20\]。
4.  2007年1月23日，5名美國黑水的雇傭兵，所搭乘的[H-6直升機在伊拉克被擊落](../Page/H-6直升機.md "wikilink")，全部死亡。\[21\]\[22\]
5.  2007年9月17日，伊拉克內政部宣布，由於黑水的警衛在16日保護美國國務院的一隊車隊時，涉嫌開槍殺死八名平民，另擊傷十三人，其在伊拉克的經營牌照一度被吊銷\[23\]\[24\]。
6.  2007年9月22日，兩名前黑水僱員於[北卡羅來納州法院承認](../Page/北卡羅來納州.md "wikilink")，走私軍火到伊拉克黑市，並表示會合作以換取減刑。不過黑水公司隨即發表聲明，否認公司有走私軍火到伊拉克，亦不清楚其員工是否走私軍火的行為。之前，有美國媒體報導，黑水涉嫌走私軍火到伊拉克，給一個同時被美國、歐盟、北約列為恐怖份子的庫爾德人武裝組織。土耳其亦聲稱從[庫爾德工人黨手中繳獲了一批美國製造的武器](../Page/庫爾德工人黨.md "wikilink")。
7.  2007年9月30日，美國的「新聞週刊」報導，伊拉克警方掌握的證據，包括多位目擊證人的證詞，以及一捲錄影帶顯示美國黑水的僱員，於9月16日在巴格達的槍擊事件中，在沒有受到挑釁的情形下開槍濫射。
8.  2008年11月9日，黑水因未經授權向伊拉克和約旦運輸軍火，可能面臨商務部的刑事指控，和數百萬美元的罰款。\[25\]

## 参见

  - [雇佣兵](../Page/雇佣兵.md "wikilink")
  - [私人軍事服務公司](../Page/私人軍事服務公司.md "wikilink")

## 参考文献

## 外部連結

  - [Official website](http://academi.com/)
  - [U.S. Training Center's official
    website](https://web.archive.org/web/20090802030954/http://www.ustraining.com/)
  - [Greystone Limited's website (international
    division)](http://www.greystone-ltd.com/)

[Category:美國公司](../Category/美國公司.md "wikilink")
[Category:1997年成立的公司](../Category/1997年成立的公司.md "wikilink")
[Category:國防承包商](../Category/國防承包商.md "wikilink")
[Category:美国雇佣兵](../Category/美国雇佣兵.md "wikilink")
[Category:1997年美國建立](../Category/1997年美國建立.md "wikilink")

1.
2.
3.  [Academi - About Us - Contact
    Us](http://academi.com/pages/about-us/contact-us)
4.
5.
6.
7.
8.
9.  [伊拉克/2004–04–01-security-usat_x.htm Role of security companies
    likely to become more visible](http://www.usatoday.com/news/world/)
10.
11. [Blackwater Product Page](http://www.blackwaterusa.com/armored/)
12. <http://baltimorechronicle.com/2007/032607Cherbonnier.html>
13.
14. <http://www.10news.com/news/13308753/detail.html>
15. <http://www.thespywhobilledme.com/the_spy_who_billed_me/2007/04/blackwater_usa_.html>
16. <http://www.sdreader.com/php/cityshow.php?id=1566>
17.
18. [The High Risk Contracting
    Business](http://www.pbs.org/wgbh/pages/frontline/shows/warriors/contractors/highrisk.html)
19. ['Residents hang slain Americans' bodies from
    bridge'—CNN.com](http://www.cnn.com/2004/WORLD/meast/03/31/iraq.main/)
20.
21. [U.S. crew of downed helicopter shot at close
    range—CNN.com](http://www.cnn.com/2007/WORLD/meast/01/24/iraq.helicopter.crash.ap/index.html)

22. [4 Americans in Iraq Crash Shot in
    Head—WTOP.com](http://www.wtop.com/?nid=500&sid=577999)
23. <http://news.xinhuanet.com/mil/2007-09/17/content_6743628.htm>
24. <http://news.eastday.com/w/20070918/u1a3113344.html>
25. <http://www.pubrecord.org/component/content/479.html?task=view>