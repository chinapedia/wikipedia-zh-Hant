**AdLib**，是一款FM音源的[ISA界面的音效卡](../Page/ISA.md "wikilink")，1987年時問世，使用[Yamaha
YM3812的音效處理器晶片](../Page/Yamaha_YM3812.md "wikilink")。台灣由[智冠生產與銷售相容的商品](../Page/智冠.md "wikilink")「魔奇音效卡」。1992年時，再發行AdLib
Gold（音效處理晶片改為[Yamaha YMF262](../Page/Yamaha_YMF262.md "wikilink")）。
[Adlib.jpg](https://zh.wikipedia.org/wiki/File:Adlib.jpg "fig:Adlib.jpg")

## 簡史

  - 1987年－AdLib Card問世，第一款平價的音效卡，使用FM音源的發聲方式（YM3812 chip by Yamaha）
  - 1988年－第一款支援AdLib的遊戲問世。
  - 1992年－AdLib Gold問世。
  - 1992年－破產於5月1日。
  - 1994年－授權給台灣的某公司。（可能是[智冠](../Page/智冠.md "wikilink")？）

## FM音源的發音原理

  - \(F_1 = I_1 \sin (w_1t) + I_2 \sin (w_2t)\)
  - \(F_2 = I_2 (w_1t + I_2 \sin (w_2t))\)

## 退出市場的原因

  - 因為只具有FM音源的輸出能力（不論是音效還是樂器聲，都是採FM音源所間接模擬的方式），所以很快的就被之後誕生的具有Wave處理能力[聲霸卡給取代](../Page/聲霸卡.md "wikilink")，成為至今的市場標準。

## 参考资料

  -
## 外部連結

  - [AdLib Music Archive](http://adlib.superfighter.com/)

  - [OPLx](http://www.oplx.com/)

  - [Adplug - AdLib sound player
    library](http://adplug.sourceforge.net/)

  - [SOUNDSHOCK, an English language discussion forum dedicated
    exclusively to FM Synthesis](http://www.soundshock.se/)

[Category:音效卡](../Category/音效卡.md "wikilink")