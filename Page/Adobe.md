**Adobe公司**（，發音：，旧称**Adobe系統公司**，股票代码：、），是[美國一家](../Page/美國.md "wikilink")[跨國](../Page/跨國公司.md "wikilink")[電腦](../Page/计算机.md "wikilink")[软件公司](../Page/软件.md "wikilink")，總部位於[加州的](../Page/加利福尼亚州.md "wikilink")[聖荷西](../Page/聖荷西_\(加利福尼亞州\).md "wikilink")，其官方[大中華部門內也常以](../Page/大中華.md "wikilink")[中文](../Page/中文.md "wikilink")「**奧多比**」\[1\]\[2\]自稱。主要从事[多媒体制作类软件的开发](../Page/多媒体.md "wikilink")，近年亦开始涉足[丰富互联网应用程序](../Page/丰富互联网应用程序.md "wikilink")、、[金融分析应用程序等](../Page/金融分析.md "wikilink")[软件开发](../Page/软件开发.md "wikilink")。

## 历史

[Adobe_Systems_logo_and_wordmark.svg](https://zh.wikipedia.org/wiki/File:Adobe_Systems_logo_and_wordmark.svg "fig:Adobe_Systems_logo_and_wordmark.svg")
Adobe由[約翰·沃諾克和](../Page/約翰·沃諾克.md "wikilink")[查理斯·格什克於](../Page/查理斯·格什克.md "wikilink")1982年12月創办\[3\]，两人先前都曾任職於[施樂公司的](../Page/施樂公司.md "wikilink")[帕洛阿爾托研究中心](../Page/帕洛阿爾托研究中心.md "wikilink")，離開後組建了Adobe，使[PostScript](../Page/PostScript.md "wikilink")[頁描述語言得到商業化應用](../Page/頁描述語言.md "wikilink")。公司名稱「Adobe」來自於加州[洛思阿圖斯的](../Page/洛思阿圖斯_\(加利福尼亞州\).md "wikilink")，這條河在公司原位於加州[山景城的辦公室不遠處](../Page/山景城_\(加利福尼亞州\).md "wikilink")\[4\]。

1985年，Adobe公司在由[蘋果公司](../Page/蘋果公司.md "wikilink")[LaserWriter](../Page/LaserWriter.md "wikilink")[印表機帶領下的PostScript](../Page/印表機.md "wikilink")[桌面出版革命中扮演了重要的角色](../Page/桌面出版.md "wikilink")。2005年4月18日，Adobe以34億[美元的價格收購了原先最大的競爭對手](../Page/美元.md "wikilink")[Macromedia公司](../Page/Macromedia.md "wikilink")，這一收購極大豐富了Adobe的產品線，提高了其在[多媒體和](../Page/多媒體.md "wikilink")[網絡出版業的能力](../Page/網絡.md "wikilink")，這宗交易在2005年12月3日完成。2006年12月，Adobe宣佈全線產品採用新圖示，以彩色的背景配搭該程式的簡寫，例如：紅色配搭Fl是[Flash](../Page/Adobe_Flash.md "wikilink")，藍色配搭Ps是[Photoshop](../Page/Photoshop.md "wikilink")。

截至2012年，Adobe共有員工11,144。另外在美國[華盛頓州的](../Page/華盛頓州.md "wikilink")[西雅圖](../Page/西雅圖.md "wikilink")、[明尼蘇達州的](../Page/明尼蘇達州.md "wikilink")[明尼亞波利](../Page/明尼亞波利.md "wikilink")、[麻省的](../Page/麻省.md "wikilink")[牛頓](../Page/牛頓_\(馬薩諸塞州\).md "wikilink")、[加州的](../Page/加州.md "wikilink")[聖路易斯-奧比斯波](../Page/聖路易斯-奧比斯波縣_\(加利福尼亞州\).md "wikilink")，以及[印度的](../Page/印度.md "wikilink")[諾伊達及](../Page/諾伊達.md "wikilink")[班加羅爾](../Page/班加羅爾.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")[渥太華](../Page/渥太華.md "wikilink")、[德國](../Page/德國.md "wikilink")[漢堡及](../Page/漢堡市.md "wikilink")[中國大陸](../Page/中國大陸.md "wikilink")[北京都有辦事處](../Page/北京.md "wikilink")。

2014年9月24日，Adobe宣布将关闭中国大陆研发分公司，仅保留销售部门。据透露，原本在中国大陆的绝大部分研发和技术工作将由Adobe印度分公司承继。

2018年5月23日，Adobe Systems斥資16.8億美元收購CMS Magento。

2018年9月24日，Adobe Systems斥資47.5億美元向Vista Equity收購營銷軟件公司Marketo
Inc，以加強公司以雲計算為基本的數碼營銷業務表現

2019年1月23日，Adobe宣布收购法国软件公司Allegorithmic，旗下产品包括为电子游戏开发流程中使用的贴图软件Substance
Painter和Substance Designer。

### 关闭中国研发公司

据《[法制晚报](../Page/法制晚报.md "wikilink")》报道
2014年9月24日，Adobe宣布关闭在中国的研发分公司，仅保留销售部门。

Adobe中国官方微博25日发布消息称，Adobe在中国今后以市场拓展业务为主，因此决定撤销并关闭在中国的研发分公司。研发分公司在12月底停止运营。Adobe保留在上海、北京、广州、深圳、香港的销售业务。

据媒体报道，Adobe中国共有开发、测试、设计和销售人员400人左右。分公司确认关闭后，这些员工被分为五类，分别将拿到不同颜色的信封。拿到绿色信封的员工将是最早离开的一批，辞退时间为10月底；红色信封则代表12月底离开公司。据已经拿到赔偿方案的员工透露，员工离职赔偿或采取N+5的方案(N代表在该公司工作的年限)。

## 批評

對奧多比系統公司的批評主要在軟體產品售價、軟體安全性以及用戶資料保全的問題上。在2013年[Reddit.com的一個調查中](../Page/Reddit.md "wikilink")，奧多比公司在「網際網路上9大最令人討厭的公司」中名列第五。\[5\]奧多比旗下的[Adobe
Reader和](../Page/Adobe_Reader.md "wikilink")[Adobe
Flash被](../Page/Adobe_Flash.md "wikilink")[TechRadar.com列入](../Page/TechRadar.md "wikilink")「有史以來最令人討厭的10大軟體」之中。\[6\]

### 軟體定價過高

Adobe被批評得最多的當屬旗下產品的定價策略\[7\]\[8\]，最明顯的例子是同一軟體套件的零售價在非美國地區的售價是[美國本土的兩倍還多](../Page/美國本土.md "wikilink")。\[9\]有[澳洲的Adobe產品買家反映](../Page/澳洲.md "wikilink")，前往美國購買一套Adobe的軟體套裝加上往返機票的花費，比在澳洲本地購買一套同樣的軟體套裝的花費還要低。\[10\]\[11\]也有人指出Adobe產品在開發中國家市場的營收不佳也是這種失敗的定價策略導致的。

2007年Adobe發表的[Creative Suite 3 Master
Collection](../Page/Creative_Suite.md "wikilink")，在[英國區的售價比](../Page/英國.md "wikilink")[歐洲大陸地區的高出](../Page/歐洲大陸.md "wikilink")1,000[英鎊](../Page/英鎊.md "wikilink")，\[12\]因此英國超過10,000名舊版本CS的使用者聯名訴狀奧多比的「不公平定價」行為。\[13\]但是在2009年6月，奧多比為應對英鎊兌換美元的疲軟[匯率](../Page/匯率.md "wikilink")，還將英國區的奧多比軟體產品的售價提高10%，\[14\]並且奧多比還禁止英國的使用者從美國的商店中購買其軟體，並嚴查其它跨區購買的行為。\[15\]奧多比公司的這種行為也被認為一定程度上無助於其正版軟體在開發中國家的普及，反而導致有已開發國家的使用者不堪忍受高價而去購買盜版，或是改用其它同類型軟體。

由於Adobe所收購的產品很多都是市場上的領導者，所以在財務上一直為公司帶來巨大盈利，當中主要是在學校及大機構的軟件使用版權費用。

### 軟體安全性問題

奧多比旗下的軟體產品經常能獲得黑客們的青睞——他們經常能從奧多比的軟體中發現漏洞，常見的有Adobe Reader、 Adobe
Animate等，用於取得未經正常授權的電腦存取權限。\[16\]Adobe
Flash除了安全性問題嚴重以外，還被批評在穩定性、運行效能、記憶體使用等方面飽受批評（詳見Flash
Player中對Flash Player的批評一節），現時主流的移動裝置，像是Android平台、iOS平台甚至Windows 10
Mobile平台等，均全數拒絕支援Adobe
Flash。2011年[卡巴斯基實驗室還發表了一份安全研究報告](../Page/卡巴斯基實驗室.md "wikilink")，點名批評Adobe的旗下的軟體產品中有齊了當年的10大安全漏洞。\[17\]

2007年有安全觀察報告指出Adobe有監視使用者的行為，有軟體開發人員發現Adobe在Creative Suite
3中安置了[間諜軟體](../Page/間諜軟體.md "wikilink")，會未經用戶同意在背景上載使用者資料至一個名為的機構（該機構在2009年被Adobe收購）。\[18\]面對安全觀察報告、使用者的質疑，Adobe承認了這些行為，並解釋是為了使用者的Adobe帳號的安全。\[19\]後來Photoshop
CS5被爆出一個長期存在的高危安全漏洞後，Adobe被使用者們千夫所指，認為Adobe故意不修復安全漏洞並迫使使用者們掏腰包升級新版本。\[20\]後來在使用者們準備對Adobe提起法律訴訟前，Adobe同意為使用者提供安全修復程式。\[21\]

Adobe也被批評會推送使用者不想要的第三方瀏覽器插件以及病毒掃描器，常見於Adobe
Flash的升級進程中，需要使用者仔細瀏覽升級頁面取消核取方塊方能避免此問題\[22\]還推送一些第三方的恐嚇軟體，設計用以恐嚇使用者們，令他們購買不需要的「系統修復」程式。\[23\]

## 參考文獻

## 外部連結

  - [Adobe Systems, Inc.](http://www.adobe.com/)

  - [Adobe Systems, Inc.（中國）](http://www.adobe.com/cn/)

  - [Adobe Systems, Inc.（台灣）](http://www.adobe.com/tw/)

  - [Adobe Systems, Inc.（香港）](http://www.adobe.com/hk_zh/)

  - [Adobe: The
    Story](https://web.archive.org/web/20070106181904/http://www.adobe.com/macromedia/story/)

[Adobe_Systems](../Category/Adobe_Systems.md "wikilink")
[Category:納斯達克上市公司](../Category/納斯達克上市公司.md "wikilink")
[Category:倫敦證券交易所上市公司](../Category/倫敦證券交易所上市公司.md "wikilink")
[Category:跨國軟件公司](../Category/跨國軟件公司.md "wikilink")
[A](../Category/聖荷西公司.md "wikilink")

1.  \[

2.

3.

4.
5.

6.

7.

8.

9.

10.

11.

12.

13.

14.

15.

16.

17.

18.

19.

20.

21.

22.

23.