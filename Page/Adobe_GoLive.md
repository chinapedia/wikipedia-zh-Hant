**Adobe
GoLive**是[Adobe系统公司出品的网页设计及编辑软件](../Page/Adobe系统.md "wikilink")。它的推出代替了[Adobe
PageMill](../Page/Adobe_PageMill.md "wikilink")，其最新版本为9，且不再是[Adobe
Creative
Suite软件包的套件之一](../Page/Adobe_Creative_Suite.md "wikilink")。GoLive是一個早期的网页编辑工具，屬於[所見即所得軟體](../Page/所見即所得.md "wikilink")。開發公司是[CyberStudio](../Page/CyberStudio.md "wikilink")，而GoLive就是其旗艦產品。於1999年，Adobe系统收購了GoLive，並更名為Adobe
GoLive。

Adobe
GoLive以前主要的竞争对手是[Dreamweaver](../Page/Dreamweaver.md "wikilink")，后来Adobe公司收购[Macromedia](../Page/Macromedia.md "wikilink")，两个软件被纳入同一家公司。现在，在[Creative
Suite](../Page/Creative_Suite.md "wikilink")
3中，Adobe已經剔除了GoLive，而用Dreamweaver代替，且Adobe鼓励客户们轉移至Dreamweaver。Dreamweaver可以支援[AJAX和](../Page/AJAX.md "wikilink")[CSS](../Page/CSS.md "wikilink")，而GoLive則不支援。所以GoLive被Dreamweaver代替是一個趨勢。

2008年4月，Adobe宣佈GoLive停止開發，只會提供相關的技术支持。舊GoLive用戶可以199美元升級到Dreamweaver。

## 版本歷史

| 版本                     | 代號          | 支援平台                                        | 日期       |
| ---------------------- | ----------- | ------------------------------------------- | -------- |
| Gonet GoLive Pro 1.0   |             | Classic Mac OS                              | 1996年8月  |
| GoLive CyberStudio 1.0 |             | Classic Mac OS                              | 1997年4月  |
| GoLive CyberStudio 2.0 |             | Classic Mac OS                              | 1997年9月  |
| GoLive CyberStudio 3.0 |             | Classic Mac OS                              | 1998年4月  |
| Adobe GoLive 4.0       |             | Classic Mac OS                              | 1999年1月  |
| Adobe GoLive 4.0       |             | Microsoft Windows                           | 1999年5月  |
| Adobe GoLive 5.0       |             | Classic Mac OS, Microsoft Windows           | 2000年8月  |
| Adobe GoLive 6.0       | The 6th Day | Classic Mac OS, Mac OS X, Microsoft Windows | 2002年2月  |
| Adobe GoLive CS        | Se7en       | Mac OS X, Microsoft Windows                 | 2003年10月 |
| Adobe GoLive CS2       | Reloaded    | Mac OS X, Microsoft Windows                 | 2005年4月  |
| Adobe GoLive CS3 (9.0) | Vicious     | Universal Mac OS X, Microsoft Windows       | 2007年6月  |

## 參見

  - [Adobe Dreamweaver](../Page/Adobe_Dreamweaver.md "wikilink")

## 外部連結

  - [瞭解有關改用Adobe
    Dreamweaver的資訊（英文）](http://www.adobe.com/products/dreamweaver/switch/)

[Category:C++软件](../Category/C++软件.md "wikilink")
[Category:已停止開發的Adobe軟體](../Category/已停止開發的Adobe軟體.md "wikilink")
[Category:1996年软件](../Category/1996年软件.md "wikilink")