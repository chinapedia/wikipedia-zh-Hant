[Adobe产品列表](../Page/Adobe_Systems.md "wikilink")。

## 目前

  - [Acrobat](../Page/Adobe_Acrobat.md "wikilink") 家族
      - [Acrobat Capture](../Page/Adobe_Acrobat_Capture.md "wikilink")
      - [Acrobat Connect](../Page/Adobe_Acrobat_Connect.md "wikilink")
        *(前為 Macromedia Breeze)*
          - [Presenter](../Page/Adobe_Presenter.md "wikilink")
      - [Acrobat
        Distiller](../Page/Adobe_Acrobat_Distiller.md "wikilink")
      - [Reader](../Page/Adobe_Reader.md "wikilink")
  - [Audition](../Page/Adobe_Audition.md "wikilink") *(前為 Cool Edit
    Pro)*
  - [Bridge](../Page/Adobe_Bridge.md "wikilink")
  - [Captivate](../Page/Adobe_Captivate.md "wikilink") *(前為 RoboDemo)*
  - [Central Output
    Server](../Page/Adobe_Central_Output_Server.md "wikilink")
  - [ColdFusion](../Page/Adobe_ColdFusion.md "wikilink")
  - [Content Server](../Page/Adobe_Content_Server.md "wikilink")
  - [Creative Suite](../Page/Adobe_Creative_Suite.md "wikilink")
      - [Animate](../Page/Adobe_Animate.md "wikilink")
      - [Acrobat Connect](../Page/Adobe_Acrobat_Connect.md "wikilink")
      - [After Effects](../Page/Adobe_After_Effects.md "wikilink")
      - [Bridge](../Page/Adobe_Bridge.md "wikilink")
      - [Contribute](../Page/Adobe_Contribute.md "wikilink")
      - [Device Central](../Page/Adobe_Device_Central.md "wikilink")
      - [Dreamweaver](../Page/Adobe_Dreamweaver.md "wikilink")
      - [Dynamic Link](../Page/Adobe_Dynamic_Link.md "wikilink")
      - [Encore](../Page/Adobe_Encore.md "wikilink")
      - [Experience
        Design](../Page/Adobe_Experience_Design.md "wikilink")
      - [Fireworks](../Page/Adobe_Fireworks.md "wikilink")
      - [Illustrator](../Page/Adobe_Illustrator.md "wikilink")
      - [InDesign](../Page/Adobe_InDesign.md "wikilink")
      - [OnLocation](../Page/Adobe_OnLocation.md "wikilink") *(前為
        Serious Magic DV Rack)*
      - [Photoshop](../Page/Adobe_Photoshop.md "wikilink")
      - [Premiere Pro](../Page/Adobe_Premiere_Pro.md "wikilink")
      - [Version Cue](../Page/Adobe_Version_Cue.md "wikilink")
      - [Audition](../Page/Adobe_Audition.md "wikilink")
  - [Digital Editions](../Page/Adobe_Digital_Editions.md "wikilink")
  - [Digital
    Negative](../Page/Digital_Negative_\(file_format\).md "wikilink")
  - [Director](../Page/Adobe_Director.md "wikilink")
  - [DNG Converter](../Page/Adobe_DNG_Converter.md "wikilink")
  - [Document Server](../Page/Adobe_Document_Server.md "wikilink")
  - [Document Policy
    Server](../Page/Adobe_Document_Policy_Server.md "wikilink")
  - [eBook Reader](../Page/Adobe_eBook_Reader.md "wikilink")
  - [Engagement
    Platform](../Page/Adobe_Engagement_Platform.md "wikilink")
  - [eLearning Suite](../Page/Adobe_eLearning_Suite.md "wikilink")
      - [Acrobat](../Page/Adobe_Acrobat.md "wikilink")
      - [Bridge](../Page/Adobe_Bridge.md "wikilink")
      - [Captivate](../Page/Adobe_Captivate.md "wikilink")
      - [Device Central](../Page/Adobe_Device_Central.md "wikilink")
      - [Dreamweaver](../Page/Adobe_Dreamweaver.md "wikilink")
      - [Flash](../Page/Adobe_Flash.md "wikilink")
      - [Photoshop](../Page/Adobe_Photoshop.md "wikilink")
      - [Presenter](../Page/Adobe_Presenter.md "wikilink")
      - [FDK](../Page/Adobe_FDK.md "wikilink")
  - [Flash](../Page/Adobe_Flash.md "wikilink") 家族
      - [Flash Cast](../Page/Adobe_Flash_Cast.md "wikilink")
      - [Flash Catalyst](../Page/Adobe_Flash_Catalyst.md "wikilink")
      - [Flash Lite](../Page/Adobe_Flash_Lite.md "wikilink")
      - [Flash Media
        Server](../Page/Adobe_Flash_Media_Server.md "wikilink")
      - [Flash Player](../Page/Adobe_Flash_Player.md "wikilink")
  - [Flex](../Page/Adobe_Flex.md "wikilink")
  - [Font Folio](../Page/Adobe_Font_Folio.md "wikilink")
  - [InCopy](../Page/Adobe_InCopy.md "wikilink")
  - [Integrated Runtime](../Page/Adobe_Integrated_Runtime.md "wikilink")
    *(或簡稱 **AIR**)*
  - [Kuler](../Page/Adobe_Kuler.md "wikilink")
  - [LiveCycle](../Page/Adobe_LiveCycle.md "wikilink") 家族
      - Assembler
      - Barcoded Forms
      - Designer
      - Document Security
      - Forms
      - Form Manager
      - PDF Generator
      - Policy Server
      - Reader Extensions
      - Security Server
  - [Media Player](../Page/Adobe_Media_Player.md "wikilink")
  - [Output Designer](../Page/Adobe_Output_Designer.md "wikilink")
  - [PDF JobReady](../Page/Adobe_PDF_JobReady.md "wikilink")
  - [Photoshop](../Page/Adobe_Photoshop.md "wikilink") 家族
      - [Photoshop
        Elements](../Page/Adobe_Photoshop_Elements.md "wikilink")
      - [Photoshop
        Express](../Page/Adobe_Photoshop_Express.md "wikilink")
      - [Photoshop
        Lightroom](../Page/Adobe_Photoshop_Lightroom.md "wikilink")
  - [Premiere Elements](../Page/Adobe_Premiere_Elements.md "wikilink")
  - [Shockwave](../Page/Adobe_Shockwave.md "wikilink")
  - [Source Libraries](../Page/Adobe_Source_Libraries.md "wikilink")
  - [Visual
    Communicator](../Page/Adobe_Visual_Communicator.md "wikilink")
  - [Technical Communication
    Suite](../Page/Adobe_Technical_Communication_Suite.md "wikilink")
      - [Acrobat](../Page/Adobe_Acrobat.md "wikilink")
      - [Captivate](../Page/Adobe_Captivate.md "wikilink")
      - [FrameMaker](../Page/Adobe_FrameMaker.md "wikilink")
      - [RoboHelp](../Page/Adobe_RoboHelp.md "wikilink")
  - [Type](../Page/Adobe_Type.md "wikilink")
  - [Web Output Pack](../Page/Adobe_Web_Output_Pack.md "wikilink")
  - [Serious Magic Ovation](../Page/Serious_Magic_Ovation.md "wikilink")
  - [Serious Magic Vlog
    It\!](../Page/Serious_Magic_Vlog_It!.md "wikilink")

## 中止

### 销售和开发

  - [Adobe ActiveShare](../Page/Adobe_ActiveShare.md "wikilink")
  - [Adobe Atmosphere](../Page/Adobe_Atmosphere.md "wikilink")
  - [Adobe Dimensions](../Page/Adobe_Dimensions.md "wikilink")
  - [Adobe GoLive](../Page/Adobe_GoLive.md "wikilink")
  - [Adobe FreeHand](../Page/Adobe_FreeHand.md "wikilink")
  - [Adobe ImageReady](../Page/Adobe_ImageReady.md "wikilink")
  - [Adobe ImageStyler](../Page/Adobe_ImageStyler.md "wikilink")
  - [Adobe InProduction](../Page/Adobe_InProduction.md "wikilink")
  - [Adobe License Manager](../Page/Adobe_License_Manager.md "wikilink")
  - [Adobe LiveMotion](../Page/Adobe_LiveMotion.md "wikilink")
  - [Adobe PageMaker](../Page/Adobe_PageMaker.md "wikilink")
  - [Adobe PageMill](../Page/Adobe_PageMill.md "wikilink")
  - [Adobe Persuasion](../Page/Adobe_Persuasion.md "wikilink")
  - [Adobe PhotoDeluxe](../Page/Adobe_PhotoDeluxe.md "wikilink")
  - [Adobe Photoshop Album](../Page/Adobe_Photoshop_Album.md "wikilink")
  - [Adobe PressReady](../Page/Adobe_PressReady.md "wikilink")
  - [Adobe PressWise](../Page/Adobe_PressWise.md "wikilink")
  - [Adobe ScreenReady](../Page/Adobe_ScreenReady.md "wikilink")
  - [Adobe SiteMill](../Page/Adobe_SiteMill.md "wikilink")
  - [Adobe Stock Photos](../Page/Adobe_Stock_Photos.md "wikilink")
  - [Adobe Streamline](../Page/Adobe_Streamline.md "wikilink")
  - [Adobe Texture Maker](../Page/Adobe_Texture_Maker.md "wikilink")
  - [Adobe Transcript](../Page/Adobe_Transcript.md "wikilink")
  - [Adobe TrapWise](../Page/Adobe_TrapWise.md "wikilink")
  - [Adobe Type Manager](../Page/Adobe_Type_Manager.md "wikilink")
  - [Adobe TypeAlign](../Page/Adobe_TypeAlign.md "wikilink")
  - [Macromedia Central](../Page/Macromedia_Central.md "wikilink")
  - [Adobe Soundboth](../Page/Adobe_Soundboth.md "wikilink")
  - [Experience Cloud](../Page/Experience_Cloud.md "wikilink")
      - [Adobe Analytics](../Page/Adobe_Analytics.md "wikilink")
      - [Adobe Audience
        Manager](../Page/Adobe_Audience_Manager.md "wikilink")
      - [Adobe Campaign](../Page/Adobe_Campaign.md "wikilink")
      - [Adobe Experience
        Manage](../Page/Adobe_Experience_Manage.md "wikilink")
      - [Adobe Media
        Optimizer](../Page/Adobe_Media_Optimizer.md "wikilink")
      - [Adobe Primetime](../Page/Adobe_Primetime.md "wikilink")
      - [Adobe Social](../Page/Adobe_Social.md "wikilink")
      - [Adobe Target](../Page/Adobe_Target.md "wikilink")
  - [Adobe Document Cloud](../Page/Adobe_Document_Cloud.md "wikilink")
      - [Adobe Acrobat Reader
        DC](../Page/Adobe_Acrobat_Reader_DC.md "wikilink")
      - [Acrobat Pro DC](../Page/Acrobat_Pro_DC.md "wikilink")
      - [Adobe Sign](../Page/Adobe_Sign.md "wikilink")
  - [Adobe Fireworks](../Page/Adobe_Fireworks.md "wikilink")
  - [Adobe Captivate](../Page/Adobe_Captivate.md "wikilink")
  - [Adobe Captivate Prime
    LMS](../Page/Adobe_Captivate_Prime_LMS.md "wikilink")
  - [Adobe Presenter Video
    Express](../Page/Adobe_Presenter_Video_Express.md "wikilink")

### 仅开发

  - [Authorware](../Page/Authorware.md "wikilink")
  - [Adobe SVG Viewer](../Page/Adobe_SVG_Viewer.md "wikilink")
  - [Macromedia HomeSite](../Page/Macromedia_HomeSite.md "wikilink")

[Adobe软件](../Category/Adobe软件.md "wikilink")
[Adobe软件](../Category/软件列表.md "wikilink")