{{ Otheruses|subject=蘋果電腦之無線網路系統|other=Airport中文解釋| 機場 }}
**AirPort**是[蘋果電腦以](../Page/蘋果電腦.md "wikilink")[IEEE
802.11](../Page/IEEE_802.11.md "wikilink")（又稱[Wi-Fi](../Page/Wi-Fi.md "wikilink")）標準為基礎的[無線網路系統](../Page/無線網路.md "wikilink")，也通過認證能夠與其他的802.11裝置相容。最新的產品家族中，是以[IEEE
802.11ac規格為基礎](../Page/IEEE_802.11ac.md "wikilink")，提供最高每秒1.3Gbps的速度，并且能夠與舊的產品相容與共用。

有些容易混淆的是，AirPort和**AirPort Extreme**也會被用來當作協定（個別使用 802.11b 和
802.11g），擴充卡以及基地台。

在日本市場，這個系統也被稱為 **AirMac**。

2018年,蘋果公司正式宣布AirPort產品線停產。

## 概觀

AirPort 在 1999年7月21日 在 [紐約](../Page/紐約.md "wikilink") 的  中首次展出。一開始提供包含了給
[蘋果公司](../Page/蘋果公司.md "wikilink") 新的
[iBook](../Page/iBook.md "wikilink") 產品線所可選購的擴充卡，加上一個 AirPort 基地台。

原本的 AirPort 系統最高能夠達到每秒 11Mbit 的傳輸率。天線整合在
[iBook](../Page/iBook.md "wikilink") 顯示器的四周，因此接收能力非常的好。蘋果電腦也是第一個擁抱
[802.11b](../Page/802.11b.md "wikilink") 無線網路的製造商。AirPort
擴充卡後來都被加入為幾乎所有蘋果產品線的選購配備，包含了
[PowerBook](../Page/PowerBook.md "wikilink")、[eMac](../Page/eMac.md "wikilink")、[iMac](../Page/iMac.md "wikilink")、和
[PowerMac](../Page/PowerMac.md "wikilink")。只有
[Xserve](../Page/Xserve.md "wikilink") 沒有 AirPort 的選購配備。

於 2003年1月7日，[蘋果電腦推出了以](../Page/蘋果電腦.md "wikilink")
[802.11g](../Page/802.11g.md "wikilink") 規格為基礎的 [AirPort
Extreme](../Page/AirPort_Extreme.md "wikilink")。AirPort Extreme 最大可以達到每秒
54 Mbit 的資料傳輸率，而且能夠向前相容（不像競爭對手 [802.11a](../Page/802.11a.md "wikilink")
無法向前相容）於現有在咖啡廳、零售店、辦公室、家裡的 [802.11b](../Page/802.11b.md "wikilink")
基地台。所有的蘋果電腦現在的電腦型號，除了 [Xserve](../Page/Xserve.md "wikilink")
例外，都有一個插槽可以插入 AirPort Extreme 擴充卡，以及所有的
[PowerBook](../Page/PowerBook.md "wikilink") 和
[iBook](../Page/iBook.md "wikilink") 型號現在都將無線網卡列為標準配備一起出貨。AirPort 和
AirPort Extreme 在實體上並不相容：AirPort Extreme 擴充卡無法安裝到舊的 Mac 機器上，而 AirPort
擴充卡則無法安裝在較新的 Mac 機器上。然而，AirPort Extreme 裝置也可以跟其他以 802.11g 為基礎的裝置或是
802.11b 的 AirPort 擴充卡互相溝通。

原先的 AirPort 擴充卡是使用 [朗訊電訊](../Page/朗訊電訊.md "wikilink") (Lucent) 的晶片，但是不像
Lucent WaveLan Silver Card（相等的 40 位元擴充卡），蘋果在 2001年 春天釋出的
[韌體](../Page/韌體.md "wikilink") 更新，提升了加密等級到 128 位元（有效地提供了 Lucent
WaveLan Gold Card 免費升級）。到了 2004年，原本的 AirPort 就停止生產。

## 基地台

[Apple_graphite_airport_base_station_front.jpg](https://zh.wikipedia.org/wiki/File:Apple_graphite_airport_base_station_front.jpg "fig:Apple_graphite_airport_base_station_front.jpg")

AirPort基地台是用來連接AirPort的電腦到[網際網路](../Page/網際網路.md "wikilink")、有線的[區域網路或是其他的裝置](../Page/區域網路.md "wikilink")。

### AirPort

原先的版本（**石墨灰**色）提供[數據機和](../Page/數據機.md "wikilink")[乙太網路部的特色](../Page/乙太網路.md "wikilink")，是以[朗訊科技WaveLan](../Page/朗訊科技.md "wikilink")
PC-Card為基礎，使用嵌入式的[英特爾](../Page/英特爾.md "wikilink")[80486處理器](../Page/80486.md "wikilink")。

第二代的型號（*雙乙太網路*或者*雪白*色）於2001年11月13日推出，擁有兩個[乙太網路部的特色](../Page/乙太網路.md "wikilink")，一個是給[區域網路](../Page/區域網路.md "wikilink")，另外一個是給[廣域網路使用](../Page/廣域網路.md "wikilink")。也增加了與
[AOL](../Page/AOL.md "wikilink")
撥接服務的相容性，讓AirPort成為能夠作這些功能的唯一無線解決方案。這個型號是以[Motorola的](../Page/Motorola.md "wikilink")[PowerPC](../Page/PowerPC.md "wikilink")860處理器為基礎。

### AirPort Extreme

[ME918.jpeg](https://zh.wikipedia.org/wiki/File:ME918.jpeg "fig:ME918.jpeg")

**Airport Extreme** 是 [蘋果電腦](../Page/蘋果電腦.md "wikilink") 在
[Macintosh](../Page/Macintosh.md "wikilink") 平台上的無線解決方案。Airport Extreme
簡單的說就是
[無線網路](../Page/無線網路.md "wikilink")，然而蘋果把它定義得跟其他一般無线網路解決方案有稍微不同。在蘋果電腦的
macintosh 電腦和[路由器的無線網路基地台](../Page/路由器.md "wikilink")，Airport Extreme 使用
Airport Extreme 擴充卡（無線網卡）。

在 2003年1月7日，AirPort 基地台被 AirPort Extreme 基地台取代。除了也提供了 AirPort Extreme
更快的傳輸速度，它增加了一個外部的天線埠，和一個 USB 埠可以連接到
[印表機](../Page/印表機.md "wikilink")。網路使用者可以利用 [Bonjour
協定](../Page/Bonjour_協定.md "wikilink") 和
[IPP](../Page/Internet_Printing_Protocol.md "wikilink")
來使用印表機。也有第二個比較便宜的版本，缺少了數據機和外部天線埠，但是在
2004年 中期的 AirPort Express 推出後就終止了。2004年4月19日，第三版推出，支援 [Power over
Ethernet](../Page/Power_over_Ethernet.md "wikilink") 以及遵循
[UL](../Page/Underwriters_Laboratories.md "wikilink") 的
[2043](../Page/UL_2043.md "wikilink") 防火管理規定。

所有的 AirPort Extreme 型號都是使用[AMD](../Page/AMD.md "wikilink") Alchemy
Au1500，是以[MIPS架構為基礎的處理器](../Page/MIPS架構.md "wikilink")。

### AirPort Express

[Apple_airport_express.jpg](https://zh.wikipedia.org/wiki/File:Apple_airport_express.jpg "fig:Apple_airport_express.jpg")

AirPort Express 是簡化和精簡版的 AirPort Extreme 基地台，擁有新的特色稱為 AirTunes。他並沒有取代
AirPort Extreme 基地台。蘋果電腦於 2004年6月7日 推出，並且包含了類比跟數位聲音輸出；和一個 USB
埠給遠端列印；以及一個
[乙太網路埠](../Page/乙太網路.md "wikilink")。聲音的輸出是給
[AirTunes](../Page/AirTunes.md "wikilink") 這個功能使用，可以讓
[iTunes](../Page/iTunes.md "wikilink")
音樂透過[無線網路的串流來播放](../Page/無線網路.md "wikilink")。AirPort
Express 也能夠輕易地透過[WDS橋接來擴充無線涵蓋率](../Page/WDS.md "wikilink")。

AirPort Express 的主要處理器是 [Broadcom](../Page/Broadcom.md "wikilink")
BCM4712KFB 無線網路晶片。他是內建 200MHz [MIPS](../Page/MIPS.md "wikilink")
處理器。聲音的部份是由 [德州仪器](../Page/德州仪器.md "wikilink")（Texas
Instruments）的PCM2705數位到類比轉換晶片來處理。

AirPort Express 的 USB 埠也可以用來接上  的 USB 紅外線遙控器來控制 AirTunes。

### AirPort Time Capsule

AirPort Time Capsule 是配備[硬碟的AirPort](../Page/硬碟.md "wikilink") Extreme
基地台，可同時作為[路由器及](../Page/路由器.md "wikilink")[網絡附加儲存](../Page/網絡附加儲存.md "wikilink")。於2008年推出第一代Time
Capsule 並無配置無線基地台，現時已推出至第五代。

## 安全性

在無線射頻為基礎的網路下，安全性是很重要的一環，因為系統有可能從遠端地方被存取。就像大部分的
[WLAN](../Page/WLAN.md "wikilink") 系統，無線射頻網路安全是以
[WEP](../Page/WEP.md "wikilink")
標準為基礎，給一些固有的限制來作為安全防護，但在很多安全公司已經認為這種防護已經被破解。最新的基地台，也就是
AirPort Extreme，也提供了 [WPA](../Page/WPA.md "wikilink") 安全性。在管理上以及預設的
AirPort
組態設定出現了更多重要的問題。在重設組態後，基地台會進入全部功能的狀態，但預設不提供加密功能。系統提供遠端管理，但是會使用公開且大家都知道的預設<sup>[1](../Page/#References.md "wikilink")</sup>密碼。

即使管理界面的密碼已經安全地更新到一個安全值，但也被發現管理系統所使用的密碼，竟然沒有加密<sup>[2](../Page/#References.md "wikilink")</sup>地在網路上傳輸。為了回應這樣的發現，[蘋果電腦](../Page/蘋果電腦.md "wikilink")
發表聲明說 AirPort 的管理應該是由有線連線或是經由 WEP 加密的網路上來操作。然而，如果因為使用者意外或是忽略，在不安全的網路上作
AirPort 的遠端管理，不幸的是系統軟體並不會給使用者任何的警告。

AirPort Extreme 基地台所附的安裝手冊幾乎沒有包含任何安全性的指引，也就是預設密碼的資訊，以及安全插槽的位置（使用 cable
lock 可以用來作為實體安全的功能）。在 2004年，AirPort 的網頁上也缺乏 AirPort 安全性之明顯的指引資訊，然而在可以下載的
*Designing Airport Networks*
手冊上，有一章致力於安全性<sup>[3](../Page/#References.md "wikilink")</sup>。另外一個由支援網頁可以得到的手冊
*Managing AirPort Extreme
Networks*，也包含了一個選擇加密技術<sup>[4](../Page/#References.md "wikilink")</sup>的章節，以及包含了不同技術的比較。這個章節提供了基本的安全性建議，但並沒有完整地包含某些風險；舉例來說，在封閉網路的那段說使用者"必須知道網路的名稱"，但無提到被猜中的機率。

在 IP 網路方面，AirPort 預設組態提供了網路位址轉換 [NAT](../Page/NAT.md "wikilink")
[閘道](../Page/閘道.md "wikilink")，是一種 [stateful
firewall](../Page/stateful_firewall.md "wikilink")
的基本功能。儘管這個功能沒有提供全部完整的應用等級過濾，但是也意味著透過
AirPort 基地台連接到 [網際網路](../Page/網際網路.md "wikilink")
的電腦，將會有比直接連線到網際網路有更好的保護。

如果是被有好的安全知識之有經驗管理者所使用，那麼 AirPort
基地台就可已變成解決方案的一部份，在大部分的應用上，提供一個可以接受的安全等級。如果情況是讓沒有經驗，且只有少數的
WLAN 安全知識的使用者來設定基地台，AirPort 系統就會很輕易地允許不安全無線網路上的簡易設定，且無提供任何的警告給使用者。

## 參考資料

1.  AirPort Extreme Base Station Setup Guide, Apple Computer Inc. Taiwan
    1999 Page 20
2.  [Apple AirPort Administrative Password Obfuscation Security
    Advisory](https://web.archive.org/web/20050406230114/http://www.atstake.com/research/advisories/2003/a051203-1.txt)，Jeremy
    Rauch and Dave G. @stake, Inc, retrived 2003/12/05 from
    <https://web.archive.org/web/20050406230114/http://www.atstake.com/research/advisories/2003/a051203-1.txt>
    2004/12/29.
3.  [Designing AirPort
    Networks](https://web.archive.org/web/20050408150347/http://manuals.info.apple.com/en/airport/DesigningAirPortNetworks0190271.pdf)，Apple
    Computing, 2004, retrieved 2004/12/29 from
    <http://www.apple.com/support/airport/>
4.  [Managing AirPort Extreme
    Networks](http://download.info.apple.com/Apple_Support_Area/Manuals/hardware/Managing_AirPort_Extreme_Networks_v3.4.pdf)，Apple
    Computing, 2004, retrieved 2004/12/29 from
    <http://www.apple.com/support/airport/>

## 外部連結

  - [Apple: AirPort](http://www.apple.com/airport/)
  - [Airport Extreme at
    Apple.com](https://web.archive.org/web/20040608045215/http://www.apple.com/airportextreme/)
  - [AirPort Base Station Experiences](http://www.vonwentzel.net/ABS/)
  - [AirPort Support](http://www.info.apple.com/usen/airport/)

[Category:蘋果公司周邊設備](../Category/蘋果公司周邊設備.md "wikilink")
[Category:Wi-Fi](../Category/Wi-Fi.md "wikilink")
[Category:ITunes](../Category/ITunes.md "wikilink")