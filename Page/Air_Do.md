**Air
Do**（），原名「**北海道國際航空**」（），是一家[日本](../Page/日本.md "wikilink")[航空公司](../Page/航空公司.md "wikilink")，總部設於[北海道](../Page/北海道.md "wikilink")[札幌市](../Page/札幌市.md "wikilink")[中央區](../Page/中央區_\(札幌市\).md "wikilink")，以[東京國際機場為](../Page/東京國際機場.md "wikilink")[樞紐](../Page/樞紐機場.md "wikilink")，主要營運來往[東京至北海道多座城市之間的航班](../Page/東京.md "wikilink")。

## 歷史

Air
Do在1996年成立，1998年投入服務。創立的觀念來自北海道一個成功的雞農，當時希望未來能發展[樺太](../Page/樺太.md "wikilink")（即[庫頁島](../Page/庫頁島.md "wikilink")）等北海道周圍地區的「北方圈」國際航線，因而將公司命名為「北海道國際航空株式會社」，並在開航前的1998年7月透過公開募集的方式決定使用「Air
Do」的暱稱。其中的「Do」除了代表北海道的「道」之外，還有[英語動詞](../Page/英語.md "wikilink")「做」（do）的含意。至2012年10月1日，正式以暱稱將公司改名為「株式會社Air
Do」。

和[天馬航空一樣](../Page/天馬航空.md "wikilink")，北海道航空誕生於日本國內航空市場開放后的1996年，當時濱田召集29个投資人組建了这家低成本航空公司，以和[日本航空](../Page/日本航空.md "wikilink")、[全日空等日本國內航空業巨头競爭](../Page/全日空.md "wikilink")。由於開航後收入不斷萎縮并面臨著嚴重的財政危機，2000年時北海道政府為該公司注入救濟金，但2002年6月北海道航空還是进入重組程序。在重組過程中Air
Do透過獲得一個有期限的企業再生[基金挹注資金](../Page/投資信託.md "wikilink")，而全日空實際上是該組合的關鍵投資人，除此之外還有[石屋製菓](../Page/石屋製菓.md "wikilink")、等北海道的在地企業也參與其中。此后，两家航空公司開始[代码共享](../Page/代码共享.md "wikilink")、航空维修、地勤、咨询服务和机队租赁等合作。该限期基金于2008年9月期滿後解散，日本政策投資銀行以42%的持股比成為Air
Do的主要股東，而全日空也成為Air Do的直接股东。在日本政策投資銀行逐漸降低持股比例之後，Air
Do在實務上已成為全日空集團所屬的航空公司之一。\[1\]

## 航點

AIR DO飛往以下航點：

<table>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p>城市</p></th>
<th><p>機場</p></th>
<th><p>營運期間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><strong><a href="../Page/札幌市.md" title="wikilink">札幌</a></strong></p></td>
<td><p><a href="../Page/新千歲機場.md" title="wikilink"><strong>新千歲機場</strong></a></p></td>
<td><p><strong>1998/12/20-</strong></p></td>
<td><p><strong>主要樞紐</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/東京都.md" title="wikilink"><strong>東京</strong></a></p></td>
<td><p><a href="../Page/東京國際機場.md" title="wikilink"><strong>東京國際機場</strong></a></p></td>
<td><p><strong>1998/12/20-</strong></p></td>
<td><p><strong>次要樞紐</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/旭川市.md" title="wikilink">旭川</a></p></td>
<td><p><a href="../Page/旭川機場.md" title="wikilink">旭川機場</a></p></td>
<td><p>2003/09/01-</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/網走市.md" title="wikilink">網走</a></p></td>
<td><p><a href="../Page/女滿別機場.md" title="wikilink">女滿別機場</a></p></td>
<td><p>2006/02-</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/釧路市.md" title="wikilink">釧路</a></p></td>
<td><p><a href="../Page/釧路機場.md" title="wikilink">釧路機場</a></p></td>
<td><p>2013/03/31-</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/帶廣市.md" title="wikilink">帶廣</a></p></td>
<td><p><a href="../Page/帶廣機場.md" title="wikilink">帶廣機場</a></p></td>
<td><p>2011/03/27-</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/函館市.md" title="wikilink">函館</a></p></td>
<td><p><a href="../Page/函館機場.md" title="wikilink">函館機場</a></p></td>
<td><p>2005/03/18-</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/仙台市.md" title="wikilink">仙台</a></p></td>
<td><p><a href="../Page/仙台機場.md" title="wikilink">仙台機場</a></p></td>
<td><p>2008/11-</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/名古屋市.md" title="wikilink">名古屋</a></p></td>
<td><p><a href="../Page/中部國際機場.md" title="wikilink">中部國際機場</a></p></td>
<td><p>2015/10-</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/神户市.md" title="wikilink">神戶</a></p></td>
<td><p><a href="../Page/神戶機場.md" title="wikilink">神戶機場</a></p></td>
<td><p>2013/06-</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 機隊

### 現役機種

截至2016年9月，AIRDO的機隊平均機齡13.3年，擁有以下飛機\[2\]\[3\]：

<center>

| 機型                                                | 數量 | 已訂購 | 載客量 | 备注                                     |
| ------------------------------------------------- | -- | --- | --- | -------------------------------------- |
| [波音737-781](../Page/波音737-700.md "wikilink")      | 9  | 0   | 144 | 全部装有[翼尖小翼](../Page/翼尖小翼.md "wikilink") |
| [波音767-381](../Page/波音767-300.md "wikilink")      | 2  | 0   | 288 | 其中JA602A为"BearDo北海道JET"特殊涂装            |
| [波音767-33A/ER](../Page/波音767-300ER.md "wikilink") | 2  | 0   | 286 |                                        |
| 總數                                                | 13 | 0   |     | 所有飞机为全经济舱布局                            |

**Air Do機隊**

</center>

### 過去曾使用機型

  - [波音767-200](../Page/波音767.md "wikilink")
  - [波音737-400](../Page/波音737-400.md "wikilink")
  - [波音737-500](../Page/波音737-500.md "wikilink")

<File:Image-Air> DO B762 in
Shin-ChitoseAP.jpg|AIRDO波音767-200客機。2004年時攝於[新千歲機場停機坪](../Page/新千歲機場.md "wikilink")。
<File:Boeing> 737-500 (Hokkaido International Airlines)
8595.jpg|AIRDO波音737-500客機

## 參考資料

## 外部連結

  - [Air Do](https://www.airdo.jp/)

[分類:1996年成立的航空公司](../Page/分類:1996年成立的航空公司.md "wikilink")

[Category:低成本航空公司](../Category/低成本航空公司.md "wikilink")
[Category:日本航空公司](../Category/日本航空公司.md "wikilink")
[Category:札幌市公司](../Category/札幌市公司.md "wikilink")

1.  有価証券報告書 株式会社ＡＩＲＤＯ 平成29年6月29日 日本金融庁
2.  <http://www.airfleets.net/flottecie/Hokkaido%20Int%20Airlines%20-%20Air%20Do.htm>
3.