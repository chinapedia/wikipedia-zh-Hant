**Ak 4**（）是[瑞典軍隊在](../Page/瑞典.md "wikilink")1965年至1985年的制式步槍，在1985年被[Ak
5突擊步槍取代](../Page/Ak_5突擊步槍.md "wikilink")。

## 历史

Ak 4與[黑克勒-科赫](../Page/黑克勒-科赫.md "wikilink")[HK
G3相同](../Page/HK_G3自動步槍.md "wikilink")，由[瑞典本土工廠特許生產並裝備](../Page/瑞典.md "wikilink")[瑞典國防軍](../Page/瑞典國防軍.md "wikilink")，取代常當時的[m/45衝鋒槍](../Page/卡爾·古斯塔夫M/45衝鋒槍.md "wikilink")、[AG
m/42B半自動步槍及](../Page/Ag_M/42半自動步槍.md "wikilink")[m/37自動步槍](../Page/白朗寧自動步槍#Kulsprutegevär_m/21_及_m/37.md "wikilink")，瑞典軍方把他們的[HK
G3命名為](../Page/HK_G3自動步槍.md "wikilink")（簡稱Ak 4），中文解為「4號自動步槍」。

1960年代，瑞典為了替換舊式步槍，舉行了新一代制式武器評選，參與的包括[比利時](../Page/比利時.md "wikilink")[Fabrique
Nationale的](../Page/Fabrique_Nationale.md "wikilink")[FN
FAL](../Page/FN_FAL自動步槍.md "wikilink")、[瑞士](../Page/瑞士.md "wikilink")[SIG
SG 510](../Page/SIG_SG_510自動步槍.md "wikilink")、瑞典卡爾古斯塔夫的[GRAM
63](../Page/GRAM_63.md "wikilink")、[美國](../Page/美國.md "wikilink")[春田兵工廠的](../Page/春田兵工廠.md "wikilink")[M14及](../Page/M14自動步槍.md "wikilink")[德國的](../Page/德國.md "wikilink")[黑克勒-科赫](../Page/黑克勒-科赫.md "wikilink")[HK
G3](../Page/HK_G3自動步槍.md "wikilink")，經過多次不同種類的測試後，最終挑選HK
G3作制式步槍。在1965年至1970年Ak
4由生產，1970年後改為位於埃斯基通納（Eskilstuna）的[卡爾古斯塔夫（Carl
Gustafs）生產](../Page/卡爾古斯塔夫.md "wikilink")，直至1985年被小口徑的[Ak
5](../Page/Ak_5突擊步槍.md "wikilink")（[FN
FNC](../Page/FN_FNC突擊步槍.md "wikilink")）取代，但Ak
4至今仍然被瑞典國土警備隊、[立陶宛及](../Page/立陶宛.md "wikilink")[愛沙尼亞的軍警所採用](../Page/愛沙尼亞.md "wikilink")。

## 型號

瑞典國防軍的Ak 4有三種型號，包括：

  - **Ak 4**：標準型，與G3A3相同（固定[槍托](../Page/槍托.md "wikilink")）。
  - **Ak 4 B**：裝有紅點瞄準鏡、移除機械照門、提高射速、加強全槍表面處理以提高耐用牲。
  - **Ak 4 OR**：標準型Ak 4加裝Hensoldt 4x24瞄準鏡（重0.65公斤）\[1\]。

所有型號的Ak
4皆可裝上柯爾特[M203及](../Page/M203榴彈發射器.md "wikilink")[HK79](../Page/HK79附加型榴彈發射器.md "wikilink")[榴彈發射器](../Page/榴彈發射器.md "wikilink")。

## 使用國

[Latvian_Soldier_G3A3.jpg](https://zh.wikipedia.org/wiki/File:Latvian_Soldier_G3A3.jpg "fig:Latvian_Soldier_G3A3.jpg")巡邏的[拉脫維亞士兵](../Page/拉脫維亞.md "wikilink")，攝於2006年\]\]

  -
  -
  -
  -
  -
## 参考文献

<div class="references-small">

<references />

</div>

## 參見

  - [CETME自動步槍](../Page/CETME自動步槍.md "wikilink")
  - [HK G3自動步槍](../Page/HK_G3自動步槍.md "wikilink")
  - [FN FAL自動步槍](../Page/FN_FAL自動步槍.md "wikilink")
  - [SIG SG 510自動步槍](../Page/SIG_SG_510自動步槍.md "wikilink")（Sturmgewehr
    57）
  - [M14自动步枪](../Page/M14自动步枪.md "wikilink")
  - [Ak 5突擊步槍](../Page/Ak_5突擊步槍.md "wikilink")

## 資料來源

  - \-[Den svenska automatkarbinen -
    Ak4](http://www.gotavapen.se/gota/ak/ak4_5/ak4.htm)

  - \-[Automatkarbin 4](http://www.soldf.com/ak4.html)

  - \-[hem1.passagen.se-Ak 4](https://web.archive.org/web/20070625070330/http://hem1.passagen.se/pgroen/ak4.htm)

  - \-[Guns.ru-HK G3](http://world.guns.ru/assault/as12-e.htm)

[en:Heckler & Koch
G3\#Variants](../Page/en:Heckler_&_Koch_G3#Variants.md "wikilink")

[Category:自动步枪](../Category/自动步枪.md "wikilink")
[Category:戰鬥步槍](../Category/戰鬥步槍.md "wikilink")
[Category:7.62×51毫米槍械](../Category/7.62×51毫米槍械.md "wikilink")
[Category:瑞典槍械](../Category/瑞典槍械.md "wikilink") [Category:HK
G3衍生槍](../Category/HK_G3衍生槍.md "wikilink")

1.  [Heckler & Koch G3](http://www.bellum.nu/armoury/HKG3.html)