**Akeboshi**日本歌手，原名明星嘉男，[神奈川縣](../Page/神奈川縣.md "wikilink")[橫濱市出身](../Page/橫濱市.md "wikilink")，唱片公司為[日本Epic
Records](../Page/日本Epic_Records.md "wikilink")，主要作品為《[Wind](../Page/STONED_TOWN.md "wikilink")》，《Yellow
Moon》。

## 經歷

3歲開始學習鋼琴，13歲開始在樂隊活動。 1999年高中畢業後去了英國利物浦藝術大學LIPA留學，2002年獨立製作發行第一張Mini
Album『STONED TOWN』，2003年發行第二張Mini Album『White
reply』，同年大學畢業後回到日本，參加了日本[富士音樂祭](../Page/富士音樂祭.md "wikilink")（FUJI
ROCK FESTIVAL '03）。

《[火影忍者](../Page/火影忍者.md "wikilink")》動畫ed採用了他的曲“wind”配以歌詞重新演繹。他的音樂以愛爾蘭音樂為基礎融合了實況音樂、爵士樂等，充滿了獨特的魅力。2004年發行第三張獨立Mini
Album『Faerie Punks』，並且在日本全國5個地方舉行了個人演唱會，票全部一售而空，同年夏天參加了「RISING SUN ROCK
FESTIVAL 2004 in EZO」。

2005年正式簽約EPIC RECORDS JAPAN後將以前的三張Mini
Album裏面的東西篩選後發行了第一張正式的專輯『Akeboshi』之後又發行了Maxi
Single「Rusty lance」。『Yellow Moon』是個人的第二張正式專輯。主打歌Yellow
Moon被選作了動畫片火影忍者（Naruto）的第十三支片尾單曲。

## 外部連結及參考

[官方網站](http://www.sonymusic.co.jp/Music/Info/Akeboshi/)

[Category:日本男歌手](../Category/日本男歌手.md "wikilink")
[Category:神奈川縣出身人物](../Category/神奈川縣出身人物.md "wikilink")