[Aldus_Manutius.jpg](https://zh.wikipedia.org/wiki/File:Aldus_Manutius.jpg "fig:Aldus_Manutius.jpg")
**Aldus
Corporation**（中文有稱為“阿圖斯”）是軟件[PageMaker的發明者](../Page/PageMaker.md "wikilink")；創辦人及公司主席[Paul
Brained於創業前任職編輯](../Page/Paul_Brained.md "wikilink")，深明利用電腦科技可以大大改善印前排版的效率和使用。在他的帶領下，PageMaker於1985年7月問世。Aldus名字源於[阿尔杜斯·马努提乌斯](../Page/阿尔杜斯·马努提乌斯.md "wikilink")，他15世紀[意大利](../Page/意大利.md "wikilink")[威尼斯的著名](../Page/威尼斯.md "wikilink")[學者及印刷商](../Page/學者.md "wikilink")，他發明了[袖珍書刊及](../Page/袖珍書.md "wikilink")[斜體字](../Page/斜體字.md "wikilink")。

憑著蘋果[麥金塔電腦的圖像操作界面](../Page/麥金塔.md "wikilink")（[GUI](../Page/GUI.md "wikilink")）、蘋果雷射印表機（[Apple
LaserWriter](../Page/Apple_LaserWriter.md "wikilink")）以及[Adobe的](../Page/Adobe.md "wikilink")[PostScript頁面描述語言](../Page/PostScript.md "wikilink")（一種處理電腦文件以供雷射印表機使用的翻譯語言）；PageMaker幾乎一夜間取代了傳統排版的工業，啟動了所謂的[桌面出版](../Page/桌面出版.md "wikilink")（Desktop
Publishing，簡稱[DTP](../Page/DTP.md "wikilink")）革命。

及後Aldus從[Altsys公司手中購得另一著名產品](../Page/Altsys.md "wikilink")—[FreeHand](../Page/FreeHand.md "wikilink")；自此。在DTP領域中，以PageMaker對決[QuarkXpress](../Page/QuarkXpress.md "wikilink")，FreeHand對[Adobe
Illustrator](../Page/Adobe_Illustrator.md "wikilink")，展開了激烈的競爭。

Aldus在後期又陸續開發或從其他公司購入了如[PhotoStyler](../Page/PhotoStyler.md "wikilink")，[IntelliDraw](../Page/IntelliDraw.md "wikilink")，[Persuasion](../Page/Persuasion.md "wikilink")（當時[PowerPoint的主要對手](../Page/PowerPoint.md "wikilink")）等軟件。又參與開發[TIFF及](../Page/TIFF.md "wikilink")[OPI的業界標準](../Page/OPI.md "wikilink")。

1994年9月，軟件巨人[Adobe併購了Aldus](../Page/Adobe.md "wikilink")，[PageMaker持續發展至](../Page/PageMaker.md "wikilink")[Adobe
InDesign的出現](../Page/Adobe_InDesign.md "wikilink")；FreeHand則由[Macromedia購得](../Page/Macromedia.md "wikilink")，開發至第11版。2005年，Adobe亦併購了Macromedia。

## 外部链接

  -
  - [The Vintage Mac Museum: Aldus
    FreeHand](https://web.archive.org/web/20070717233355/http://www.d4.dion.ne.jp/~motohiko/freehand.htm)

  - [Logo of Aldus
    Corporation](http://www.selfpublishingreview.com/2009/12/deconstructing-bembo-typographic-beauty-and-bloody-murder/).
    An article on typography briefly discussing the origin of the Aldus
    logo.

[Category:美國歇業軟體公司](../Category/美國歇業軟體公司.md "wikilink")
[Category:1984年成立的公司](../Category/1984年成立的公司.md "wikilink")
[Category:Adobe软件](../Category/Adobe软件.md "wikilink")