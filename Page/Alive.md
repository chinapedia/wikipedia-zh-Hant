**Alive**是一隊[香港演藝組合](../Page/香港.md "wikilink")，由[吳彥祖](../Page/吳彥祖.md "wikilink")、[尹子維](../Page/尹子維.md "wikilink")、[陳子聰](../Page/陳子聰.md "wikilink")、[連凱四人組成](../Page/連凱.md "wikilink")\[1\]。

## 过程

Alive在2005年8月成立，曾公開演唱，並自資六萬港幣灌錄大碟《[阿當的抉擇](../Page/阿當的抉擇.md "wikilink")》。2006年Alive曾傳出不和，並把互罵片段放上其網站。當時Alive正拍攝由[吳彥祖執導](../Page/吳彥祖.md "wikilink")、以[紀錄片形式拍攝的電影](../Page/紀錄片.md "wikilink")《[四大天王](../Page/四大天王（電影）.md "wikilink")》，當《四大天王》上映，傳媒發現所謂不和，實際是由吳彥祖精心佈局，以達到宣傳及愚弄目的\[2\]\[3\]。包括该电影与社团都遭到了舆论的广泛质疑\[4\]。

## 参考

## 外部連結

  - [Alive官方網址](http://www.alivenotdead.com/?alive)

[Category:香港男子演唱團體](../Category/香港男子演唱團體.md "wikilink")
[Category:已解散的男子演唱團體](../Category/已解散的男子演唱團體.md "wikilink")

1.
2.
3.
4.