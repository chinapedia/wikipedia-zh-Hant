**Allmovie**（原名**All Movie
Guide**）是一個包含關於[電影明星](../Page/電影明星.md "wikilink")、[電影與](../Page/電影.md "wikilink")[電視劇資訊的商業資料庫](../Page/電視劇.md "wikilink")\[1\]\[2\]。它由電影資料庫維護者Michael
Erlewine與擁有數學[哲學博士學位的Vladimir](../Page/哲學博士.md "wikilink")
Bogdanov共同創辦，兩人亦創辦了[Allmusic與](../Page/Allmusic.md "wikilink")[Allgame](../Page/Allgame.md "wikilink")。

## 历史

該資料庫已授權給成千上萬個發行商與[銷售時點情報系統](../Page/銷售時點情報系統.md "wikilink")、網站與報攤使用。Allmovie資料庫非常豐富與廣泛，當中包含了基本的產品資訊、演員與製作人員名單、情節概要、傳記、專家評論與相關鏈接等。

Allmovie上的資料可透過網路於allmovie.com網站存取，亦可經由可自動識別DVD的媒體識別服務[AMG
LASSO存取](../Page/AMG_LASSO.md "wikilink")。

該網站是[All Media
Guide的產品](../Page/All_Media_Guide.md "wikilink")，其他還有[Allmusic與](../Page/Allmusic.md "wikilink")[Allgame](../Page/Allgame.md "wikilink")。

2011年，Allmovie与Allmusic被整合至[Rovi公司创办的](../Page/Rovi公司.md "wikilink")**Allrovi**。

## 參考資料

## 外部連結

  - [Allmovie](http://www.allmovie.com)

  -
  -
[Category:2011年建立的网站](../Category/2011年建立的网站.md "wikilink")
[Category:电影主题网站](../Category/电影主题网站.md "wikilink")
[Category:线上电影数据库](../Category/线上电影数据库.md "wikilink")
[Category:被防火长城封锁的网站](../Category/被防火长城封锁的网站.md "wikilink")

1.
2.