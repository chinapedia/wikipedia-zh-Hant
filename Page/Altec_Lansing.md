**Altec Lansing Technologies, Inc.**是一家電腦與家居音頻裝置裝造商。其中有名的產品線"Voice of
the Theatre"於1947年製作，以及"in
Motion"，一條為[蘋果電腦](../Page/蘋果電腦.md "wikilink")[iPod而設計的便攜揚聲器產品線](../Page/iPod.md "wikilink")。

它為很多電腦製造廠商如[Dell](../Page/戴爾電腦.md "wikilink")、[Compaq](../Page/康柏.md "wikilink")、[Hewlett-Packard和](../Page/惠普.md "wikilink")[Gateway提供產品](../Page/Gateway_Computers.md "wikilink")，在最近逾十年來一直保持個人電腦專用揚聲器市場的最高佔有率。主要競爭對手包括[Harman
Kardon與](../Page/Harman_Kardon.md "wikilink")[Bose](../Page/Bose_Corporation.md "wikilink")。

## 歷史

[AltecLansing.png](https://zh.wikipedia.org/wiki/File:AltecLansing.png "fig:AltecLansing.png")

  - 1928年，[AT\&T所屬的](../Page/AT&T.md "wikilink")[Western
    Electric成立了一個電子產品部門稱作](../Page/Western_Electric.md "wikilink")"**ERPI**"
    (Electric Research Products,
    Inc.)，專門替工作室、戲院與電影放映上的目的而製造、服務和安裝揚聲器及相關電子產品，為當時興起（那時是*[The
    Jazz
    Singer](../Page/The_Jazz_Singer.md "wikilink")*興起的年份）的電影工業提供音頻設備的領導者。

<!-- end list -->

  - 1936年，ERPI從Western Electric的電子產品部門中獨立出來成為一家公司，並更名為"**All Technical
    Services Company**"。

<!-- end list -->

  - 1941年5月1日，All Technical Services Company收購了Lansing工業公司（以主要人物[James
    B.
    Lansing的名字來命名](../Page/James_Bullough_Lansing.md "wikilink")），並合併更名為"**Altec
    Lansing Corporation**"（即將兩字"All Tech"和 "Lansing"混合成"Altec Lansing
    Corporation"）。第一個Altec Lansing製造的揚聲器，Model 142B於同一年面世。

<!-- end list -->

  - 1986年，開始生產在汽車和家庭用途方面的揚聲器。

<!-- end list -->

  - 1990年，導入[個人電腦專用揚聲器系統的製造](../Page/個人電腦.md "wikilink")。

<!-- end list -->

  - 1995年，其所製造的個人電腦專用揚聲器系統被[戴爾電腦的](../Page/戴爾電腦.md "wikilink")[多媒體電腦指定採用](../Page/多媒體電腦.md "wikilink")，及替[惠普生產其電腦專用的揚聲器系統](../Page/惠普.md "wikilink")，並推出世界第一個有內建[杜比電路的電腦揚聲器環繞音響系統](../Page/杜比.md "wikilink")。自1986年起總共獲得41項獎項，公司更名為"**Altec
    Lansing Technologies, Inc**"。

<!-- end list -->

  - 1996年，在[中國](../Page/中國.md "wikilink")[廣東省](../Page/廣東省.md "wikilink")[東莞市設廠](../Page/東莞市.md "wikilink")。(喇叭單元由台資企業“東莞嘉茂電子科技有限公司”代工生產)

<!-- end list -->

  - 2003年，為[蘋果電腦](../Page/蘋果電腦.md "wikilink")[iPod推出第一款可攜式的揚聲器系統](../Page/iPod.md "wikilink")。

<!-- end list -->

  - 2005年，替[Palm
    PDA和](../Page/Palm_PDA.md "wikilink")[智慧型手機推出市場上第一個揚聲器系統](../Page/智慧型手機.md "wikilink")。同年7月11日Altec
    Lansing宣佈被[Plantronics以將近](../Page/Plantronics.md "wikilink")1億6,600萬美金所收購。\[1\]

<!-- end list -->

  - 2008年，設計出適用於蘋果電腦[iPhone的揚聲器](../Page/iPhone.md "wikilink")，及全球第一款內建兩個下射式重低音喇叭單體的2.2個人電腦揚聲器系統。

<!-- end list -->

  - 2009年，公司被私募基金以2500万美金从Pantranics买走，公司资产大幅缩水。

<!-- end list -->

  - 2011年，公司业务团队出走，开始大规模裁员。因为业绩不佳，2011年3月公司总裁Vicki Marion黯然去职。

<!-- end list -->

  - 2017年，公司在台灣已結束代理，倒閉

## 參考

<references/>

## 外部連結

  - [官方網站](http://www.alteclansing.com/)
  - [Altec Lansing's (unofficial)
    Homepage\!](http://alteclansingunofficial.nlenet.net/)
  - [Altec Lansing - 台灣代理商 思維寶藍科技有限公司
    官方網站](http://www.thinkinggroup.com.tw)
  - [Altec Lansing - 台灣代理商 力孚國際
    官方網站](https://web.archive.org/web/20101110114453/http://www.altecmm.com.tw/web/index)
  - [Altec Lansing - 香港地區總代理 Action Group Limited
    網站](https://web.archive.org/web/20120213061210/http://www.actiongroup.hk/)

[Category:賓夕法尼亞州](../Category/賓夕法尼亞州.md "wikilink")
[Category:揚聲器廠商](../Category/揚聲器廠商.md "wikilink")
[Category:揚聲器](../Category/揚聲器.md "wikilink")

1.  [Plantronics to Acquire Altec
    Lansing](http://www.alteclansing.com/press_news.asp?id=99) , press
    release