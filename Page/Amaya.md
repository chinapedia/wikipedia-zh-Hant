**Amaya**是一套由[W3C及](../Page/World_Wide_Web_Consortium.md "wikilink")[INRIA所製作的](../Page/法國國家信息與自動化研究所.md "wikilink")[自由及開放原始碼](../Page/自由及開放原始碼軟體.md "wikilink")[網頁瀏覽器及網頁製作軟體](../Page/網頁瀏覽器.md "wikilink")\[1\]。它是Grif這套所見即所得的[SGML編輯器及Symposia](../Page/SGML.md "wikilink")[HTML編輯器的後代](../Page/HTML編輯器.md "wikilink")\[2\]。

Amaya最初的設計成一套[HTML及](../Page/HTML.md "wikilink")[CSS編輯器](../Page/Cascading_Style_Sheets.md "wikilink")，但它後來擴展成支持[XML相容規格的編輯器](../Page/XML.md "wikilink")，如[XHTML](../Page/XHTML.md "wikilink")\[3\]，[MathML](../Page/MathML.md "wikilink")\[4\]及[SVG](../Page/Scalable_Vector_Graphics.md "wikilink")\[5\]。它可以顯示開放圖像格式如[PNG](../Page/PNG.md "wikilink")，SVG及SVG動畫。它被廣泛用於新網頁技術試驗台，以測試那些未能於主流瀏覽器上運行的技術\[6\]\[7\]。

## 參考文獻

## 參見

  - [網頁瀏覽器列表](../Page/網頁瀏覽器列表.md "wikilink")
  - [網頁瀏覽器比較](../Page/網頁瀏覽器比較.md "wikilink")

## 外部連結

  -
  -
[Category:自由HTML編輯器](../Category/自由HTML編輯器.md "wikilink")
[Category:用C編程的自由軟體](../Category/用C編程的自由軟體.md "wikilink")
[Category:自由網頁瀏覽器](../Category/自由網頁瀏覽器.md "wikilink")
[Category:1996年軟體](../Category/1996年軟體.md "wikilink")
[Category:全球資訊網聯盟](../Category/全球資訊網聯盟.md "wikilink")

1.

2.

3.
4.
5.
6.

7.