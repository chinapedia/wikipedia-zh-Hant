**Animax**（）為[索尼集團旗下專門播放](../Page/索尼.md "wikilink")[動畫的](../Page/動畫.md "wikilink")[電視頻道](../Page/電視頻道.md "wikilink")。[日本Animax是由](../Page/日本.md "wikilink")[索尼影業與](../Page/索尼影業.md "wikilink")[東映動畫](../Page/東映動畫.md "wikilink")、[日昇動畫](../Page/日昇動畫.md "wikilink")、[TMS
Entertainment及](../Page/TMS_Entertainment.md "wikilink")[NAS](../Page/日本AD_SYSTEMS.md "wikilink")（Nihon
Animation Development Systems）合資成立，公司全名「」（Animax Broadcast Japan
Inc.），1998年7月1日於[SKY
PerfecTV\!啟播](../Page/SKY_PerfecTV!.md "wikilink")。

另外，Animax也是日本以外地區第一個全日24小時放送的動畫專屬頻道（日本地區首個動畫專屬頻道為[Kids
Station](../Page/Kids_Station.md "wikilink")），海外經營由[美國索尼影業所屬](../Page/美國.md "wikilink")[索尼影视电视](../Page/索尼影视电视.md "wikilink")（SPT）統籌，實質上與日本Animax並無直接關聯。

2017年2月23日，日本索尼影業與[三井物產共同宣布於同年](../Page/三井物產.md "wikilink")3月31日設立、簡稱AKH，前者持有過半股份，將各自的Animax、Kids
Station納入該[控股公司旗下管理](../Page/控股公司.md "wikilink")，由索尼影業也是Animax社長瀧山正夫兼任AKH及Kids
Station社長<ref>AKH相關資訊：

  -
  -
</ref>。

## 公司設立

[東映動畫跟](../Page/東映動畫.md "wikilink")[富士電視台在](../Page/富士電視台.md "wikilink")1997年有意設立衛星動畫放送局，並聯絡[旭通廣告](../Page/旭通廣告.md "wikilink")。富士跟[索尼皆出資參與](../Page/索尼.md "wikilink")[JSkyB株式會社取得](../Page/Sky_PerfecTV!.md "wikilink")[衛星電視](../Page/衛星電視.md "wikilink")[平台](../Page/平台.md "wikilink")，當時旭通旗下製作人也是[NAS社員](../Page/日本AD_SYSTEMS.md "wikilink")[片岡義朗奔走撰寫意向書](../Page/片岡義朗.md "wikilink")，[日昇動畫](../Page/日昇動畫.md "wikilink")、[TMS娛樂亦加入](../Page/TMS娛樂.md "wikilink")，富士方面牽線找來索尼投資持股66%其餘四家動畫公司則是34%，解決相關動畫業者所擔心的資金問題\[1\]。公司於1998年5月20日成立，總部設在東京都港區，由瀧山正夫擔任總裁。

## 國際擴展

### 亞太地區

台灣[超級電視台曾在索尼影業入主時代引進帶狀動畫節目](../Page/超級電視台.md "wikilink")《Animax》，亦用Animax[標識](../Page/標識.md "wikilink")<ref>[超視官網](../Page/超視.md "wikilink")：

  -
  -
  - </ref>。

2004年1月，索尼影業與Animax共同宣佈，在總部位於[新加坡之SPE](../Page/新加坡.md "wikilink")
Networks-Asia Pte. Ltd.（亦為[AXN](../Page/AXN.md "wikilink")
Asia之營運所在）成立Animax Asia頻道，並針對語言與市場差異設立兩分台：Animax
Taiwan（部份動畫提供國語配音）與Animax Hong
Kong（部份動畫提供粵語配音），為[台灣](../Page/台灣.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門以及](../Page/澳門.md "wikilink")[東南亞等地約九千四百萬動畫迷提供全日動畫廣播](../Page/東南亞.md "wikilink")。2004年7月，Animax
Asia再成立 Animax South Asia（後改名為Animax
India），為南亞地區包括[印度](../Page/印度.md "wikilink")、[巴基斯坦等地提供二十四小時動畫放送](../Page/巴基斯坦.md "wikilink")。Animax
Asia與旗下各區域頻道在[亞洲的收視戶大約有一千七百萬個收視戶](../Page/亞洲.md "wikilink")。

2006年4月29日啟播Animax[韓國頻道](../Page/韓國.md "wikilink")，為韓國的動畫迷提供全日亞洲動畫廣播服務；2006年8月31日在[馬來西亞提供服務](../Page/馬來西亞.md "wikilink")。Animax也播放日本動畫以外、亞洲各國製作的高素質動畫，例如[神鵰俠侶](../Page/神鵰俠侶_\(動畫\).md "wikilink")（第二輯）、[時空冒險記等](../Page/時空冒險記.md "wikilink")。

在2007年6月，Animax Mobile正式於澳洲[3
Mobile推出](../Page/3_\(電訊\).md "wikilink")3G行動電話手機頻道。

2007年8月1日，香港[無綫收費電視因業務理由停止播放Animax](../Page/無綫收費電視.md "wikilink")，使香港播放Animax的有線電視系統商數目減至三個。Animax於[無綫收費電視停播前所使用的頻道為第](../Page/無綫收費電視.md "wikilink")50頻道。隨後於2009年4月1日，Animax連同[AXN](../Page/AXN.md "wikilink")、[beTV及](../Page/beTV.md "wikilink")[索尼台等由索尼娛樂亞洲經營的頻道重新於](../Page/Sony_Channel_\(亞洲\).md "wikilink")[無綫收費電視播放](../Page/無綫收費電視.md "wikilink")，但使用的頻道則改至第24/224頻道。2013年頻道重編之後改為第33頻道，直至2016年7月1日，加盟TVB
OTT平台[myTV SUPER](../Page/myTV_SUPER.md "wikilink"),編號為第504頻道。

2014年4月1日，[香港寬頻bbTV停止播放Animax](../Page/香港寬頻bbTV.md "wikilink")，使香港播放Animax的有線電視系統商數目減至三個，後因[myTV
SUPER啟播變](../Page/myTV_SUPER.md "wikilink") 回4個。

2015年1月20日，台灣[中華電信MOD開台](../Page/中華電信MOD.md "wikilink")[AnimaxHD](http://mod.cht.com.tw/channel/channelinfo.php?chid=132)高畫質頻道，採用1080i視訊格式播放，為[Animax
Asia於亞太地區第一個高畫質頻道](../Page/Animax_Asia.md "wikilink")。

### 拉丁美洲

2005年7月，Animax在[墨西哥](../Page/墨西哥.md "wikilink")、[阿根廷等地提供日本動畫廣播服務](../Page/阿根廷.md "wikilink")，取代了原在拉丁美洲、由索尼所收購的[Locomotion頻道](../Page/Locomotion.md "wikilink")。2011年5月1日改為「Sony
Spin」，現已不再播放動畫節目。

### 歐洲

2007年4月，SPTI宣佈Animax正式揮軍[歐洲](../Page/歐洲.md "wikilink")，先由於中歐之[捷克](../Page/捷克.md "wikilink")、[匈牙利](../Page/匈牙利.md "wikilink")、[羅馬尼亞](../Page/羅馬尼亞.md "wikilink")、[斯洛伐克等國家](../Page/斯洛伐克.md "wikilink")，由先前合併之A+動畫電視網路正式改名Animax，並於2007年6月5日於[德國開播](../Page/德國.md "wikilink")，索尼計畫繼續在[英國](../Page/英國.md "wikilink")、[法國](../Page/法國.md "wikilink")、[義大利](../Page/義大利.md "wikilink")、[西班牙等地正式開始播出](../Page/西班牙.md "wikilink")，此舉為Animax正式大規模進入歐洲。

2007年10月，Animax作為AXN的部份頻道時段於西班牙、[葡萄牙登場](../Page/葡萄牙.md "wikilink")。2008年4月12日，Animax正式於這兩個國家成立新頻道，透過付費寬頻數位電視的方式，提供全天候的頻道節目廣播。

另外，Animax原本預定2008年1月於[波蘭開播](../Page/波蘭.md "wikilink")，同其姊妹頻道AXN由[HBO](../Page/HBO.md "wikilink")
Poland代理，不過一再延宕而胎死腹中。2013年10月25日，以[隨選視訊方式在](../Page/隨選視訊.md "wikilink")[英國播放](../Page/英國.md "wikilink")。

，僅存德國和英國的Animax仍在運作。捷克、匈牙利、羅馬尼亞、斯洛伐克則已改納入AXN旗下。

### 非洲

2007年10月，Animax正式透過DStv直播衛星服務於[南非](../Page/南非.md "wikilink")、[納米比亞](../Page/納米比亞.md "wikilink")、[辛巴威](../Page/辛巴威.md "wikilink")、[波札那](../Page/波札那.md "wikilink")、[尚比亞](../Page/尚比亞.md "wikilink")、[莫三比克與](../Page/莫三比克.md "wikilink")[賴索托提供廣播服務](../Page/賴索托.md "wikilink")。現已被「Sony
Max」取代。

### 北美

迄今Animax已贊助北美洲許多的動漫活動。2007年7月，Animax於加拿大Bell Mobility推出「Animax
Mobile」行動電話專屬頻道。索尼影業旗下美國[視訊網站](../Page/視訊網站.md "wikilink")曾於2010年3月30日宣佈以「ANIMAX
Collection」名義播放；2012年1月17日再次宣佈提供[平板電腦](../Page/平板電腦.md "wikilink")、行動上網、OTT（Over-the-top）收看的服務，回復第二代Logo再出發、瞄準北美市場；但約2013年底又將Animax移除。

## 電視之外

除現時利用電視機收看Animax外，Animax正計劃增加播放媒體。2007年2月，Animax日本官方宣布會率先推出Animax[行動電話頻道](../Page/手提電話.md "wikilink")（俗稱**Animax
Mobile**），日本3G流動電話用戶從同年4月開始只須訂閱[MOBAHO\!便可隨時隨地欣賞Animax日本頻道節目](../Page/MOBAHO!.md "wikilink")。

Animax 亦在2007年6月於澳洲之3 Mobile及在同年7月於加拿大之Bell Mobility推出「Animax
Mobile」行動專屬頻道。現時，提供Animax Mobile的地區有日本、澳洲、加拿大、香港與台灣。

## 宣傳活動

為了令播放當地的收視戶更能認識Animax頻道，頻道不定期舉辦不同類型的活動。

### 日本

  - 從2002年開始設立Animax動畫大獎活動。
  - 2009年5月9日，於東京舉辦「ANIMAX MUSIC LINK TO THE WORLD」的動畫演唱會。

### 亞洲地區

## A+

**A+**為Animax匈牙利頻道的前身，於2004年12月4日在[匈牙利](../Page/匈牙利.md "wikilink")，[捷克](../Page/捷克.md "wikilink")，[羅馬尼亞及](../Page/羅馬尼亞.md "wikilink")[斯洛文尼亞播放動畫節目](../Page/斯洛文尼亞.md "wikilink")，時間為當地時間每日晚上八時至凌晨兩時。2006年10月被索尼影業收購，並於2007年4月11日轉型為Animax歐洲頻道。

由於A+的頻道與另外的頻道分享時段，故此Animax歐洲頻道成立時，仍依舊使用A+的時段。

## 節目型態

頻道以廿四小時全日播放日本動畫及動畫資訊為主，更於2007年更播放動畫以外的節目，以開拓更多觀眾收看Animax。例如在2006年年尾播放世界電子遊戲競技大賽意大利總決賽特輯。

2007年3月，香港與東南亞地區更引入了由[田森及](../Page/田森.md "wikilink")[堂真理子主持的](../Page/堂真理子.md "wikilink")[朝日電視台長壽音樂節目](../Page/朝日電視台.md "wikilink")[Music
Station等](../Page/Music_Station.md "wikilink")。

### 節目區塊

以下**所有區塊只適用**於**現時的日本頻道**。

  - **六時半藏**（日語：****）

<!-- end list -->

  -
    於逢星期一至五晚六時三十分至七時正，播放適合兒童收看的動畫。

<!-- end list -->

  - **動畫大世界**（日語：**MEGA ZONE OOO**；OOO為MON、TUE、WED、THU及FRI的縮寫）

<!-- end list -->

  -
    於逢星期一至五晚十時至十時三十分播放[週刊少年Jump及](../Page/週刊少年Jump.md "wikilink")[週刊少年Sunday連載漫畫的動畫](../Page/週刊少年Sunday.md "wikilink")，及於晚上十時三十分至十一時三十分播放Animax原創，或深夜動畫。

<!-- end list -->

  - **歡樂星期日**（日語：****）

<!-- end list -->

  -
    於星期日早上七時至九時三十分播放親子動畫的時段。

<!-- end list -->

  - **大星期日**（日語：****）

<!-- end list -->

  -
    於星期日晚九時至十一時播放電影動畫或OVA的時段。此時段曾經命名為**星期日精選**（），於中午十二時至下午二時播映。

**以下所有區塊**於2006年6月1日開始**已全不適用**，**此部份所列出**的為[日本及](../Page/日本.md "wikilink")[亞洲頻道](../Page/亞洲.md "wikilink")**於2006年5月31日前**所**使用的節目區塊**，此外[台灣由於有](../Page/台灣.md "wikilink")[限制級](../Page/限制級.md "wikilink")[鎖碼政策](../Page/鎖碼.md "wikilink")，**某些節目區塊不適用**。

  - **可愛動畫**（英語：**Kids Hour**，日語：****）

<!-- end list -->

  -
    此節目區塊的[動畫專為](../Page/動畫.md "wikilink")7-14歲[兒童而設](../Page/兒童.md "wikilink")。故事以有趣且富教育意義為題材。在2004年11月，此節目區塊更名為**小學堂**（英語：**Fun
    Fun Hour**）。

<!-- end list -->

  - **少女動畫特區**（英語：**Sweet Tea**，日語：****）

<!-- end list -->

  -
    **[日本Animax](../Page/日本.md "wikilink")**節目區塊，以播放少女動畫為主的時段。

<!-- end list -->

  - **少年動畫特區**（英語：**Youth Hour**）

<!-- end list -->

  -
    此節目區塊的[動畫專為](../Page/動畫.md "wikilink")15歲以上的[青少年而設](../Page/青少年.md "wikilink")，故事以[科幻動畫](../Page/科幻動畫.md "wikilink")、[體育動畫以及其他人氣動畫為主](../Page/體育動畫.md "wikilink")。此節目區已於2004年11月廢除。[日本Animax並無此區塊](../Page/日本.md "wikilink")。

<!-- end list -->

  - **動畫大世界**（英語：**Megazone**，日語：****）

<!-- end list -->

  -
    此節目區塊以播放超人氣[動畫為主](../Page/動畫.md "wikilink")。除[日本Animax外](../Page/日本.md "wikilink")，更會播放於其他[電視台從未播放過的動畫](../Page/電視台.md "wikilink")。

<!-- end list -->

  - **超能動畫**（又稱**超飆動畫**，英語：**Super Maniax**，日語：****）

<!-- end list -->

  -
    [動畫通常列為](../Page/動畫.md "wikilink")[家長指引類別](../Page/香港電視節目分類制度.md "wikilink")。以播放[日本](../Page/日本.md "wikilink")[深夜動畫為主的時段](../Page/深夜動畫.md "wikilink")。[南亞與](../Page/南亞.md "wikilink")[台灣Animax並沒有此節目區塊](../Page/台灣.md "wikilink")。

<!-- end list -->

  - **假日動畫劇場**（英語：**Weekend Anime**）

<!-- end list -->

  -
    於週末及週日播放電視動畫及[OVA為主的節目區塊](../Page/OVA.md "wikilink")，[日本Animax並沒有此節目區塊](../Page/日本.md "wikilink")。除[南亞Animax外](../Page/南亞.md "wikilink")，此節目區塊已於2004年11月廢除。

<!-- end list -->

  - **深夜俱樂部**（英語：**Midnight Club**）

<!-- end list -->

  -
    以播放[成人動畫為主的節目區塊](../Page/成人動畫.md "wikilink")，動畫通常被列為**成人節目**類別。此節目區塊於2004年11月成立。[南亞Animax並沒有此節目區塊](../Page/南亞.md "wikilink")。

<!-- end list -->

  - **歡樂星期日**（英語：**HappySundays**，日語：****）

<!-- end list -->

  -
    **[日本Animax](../Page/日本.md "wikilink")**節目區塊，播放適合兒童觀看的[OVA及](../Page/OVA.md "wikilink")[劇場版](../Page/劇場版.md "wikilink")[動畫](../Page/動畫.md "wikilink")。

<!-- end list -->

  - **大星期日**（英語：**Big Sundays**，日語：****）

<!-- end list -->

  -
    **[日本Animax](../Page/日本.md "wikilink")**節目區塊，播放適合成人觀看的[OVA及](../Page/OVA.md "wikilink")[劇場版](../Page/劇場版.md "wikilink")[動畫](../Page/動畫.md "wikilink")。自2005年8月開始，本區塊於[香港及亞洲頻道推出](../Page/香港.md "wikilink")，而於該兩個頻道播放的節目則為日本最新及超人氣的動畫為主。

<!-- end list -->

  - **動畫劇場**（香港及亞洲）/ **動漫大劇場**（台灣）（英語：**Anime Theater**）

<!-- end list -->

  -
    於2005年8月重新推出的節目區塊，為[亞洲](../Page/亞洲.md "wikilink")、[香港](../Page/香港.md "wikilink")、[南亞及](../Page/南亞.md "wikilink")[台灣播放](../Page/台灣.md "wikilink")[OVA及](../Page/OVA.md "wikilink")[劇場版](../Page/劇場版.md "wikilink")[動畫](../Page/動畫.md "wikilink")。此區塊曾於2004年時試行。

<!-- end list -->

  - **少女力量**（英語：**Girl Power**）

<!-- end list -->

  -
    2005年9月Animax亞洲新推出的節目區塊，播放以少女為主角的動畫。

<!-- end list -->

  - **少年行**（英語：**Shonen Walker**（香港、亞洲）/ **Teen-edge**（南亞））

<!-- end list -->

  -
    2005年9月Animax亞洲新推出的節目區塊，播放少年最喜愛的動畫題材，如科幻、體育等動畫。

<!-- end list -->

  - **阿尼萬利亞**（英語：**Animania**）

<!-- end list -->

  -
    原屬於[AXN亞洲頻道的節目區塊](../Page/AXN.md "wikilink")，播放最暢銷漫畫改編的動畫、較冷門的動畫題材和重播於AXN播放過的動畫。

<!-- end list -->

  - **絕對爆笑**（英語：**Absolute Laughter**）

<!-- end list -->

  -
    2005年9月Animax[亞洲](../Page/亞洲.md "wikilink")（但不包括[香港及](../Page/香港.md "wikilink")[台灣](../Page/台灣.md "wikilink")）新推出的節目區塊，播放逗笑動畫為主的節目區塊。

### 香港節目區塊

  - **週一科幻夜**

<!-- end list -->

  -
    於星期一晚上10時播映的動畫，曾播映[玻璃艦隊](../Page/玻璃艦隊.md "wikilink")、[命運守護夜](../Page/命運守護夜.md "wikilink")、[曙光少女及](../Page/曙光少女.md "wikilink")[奔向地球](../Page/奔向地球.md "wikilink")。

<!-- end list -->

  - **週二大激鬥**

<!-- end list -->

  -
    於星期二晚上10時播映的動畫，曾播映[少年陰陽師](../Page/少年陰陽師.md "wikilink")、[鬼眼狂刀](../Page/鬼眼狂刀.md "wikilink")
    及[黑之契約者](../Page/黑之契約者.md "wikilink")。

<!-- end list -->

  - **週三浪漫夜**

<!-- end list -->

  -
    於星期三晚上10時播映的動畫，曾播映[金色琴弦](../Page/金色琴弦.md "wikilink")、[REC及](../Page/REC.md "wikilink")[天堂之吻](../Page/天堂之吻.md "wikilink")。

<!-- end list -->

  - **週四動起來**

<!-- end list -->

  -
    於星期四晚上10時播映的動畫，曾播映[王牌投手
    振臂高揮](../Page/王牌投手_振臂高揮.md "wikilink")、[冰上萬花筒及](../Page/冰上萬花筒.md "wikilink")[飛輪少年](../Page/飛輪少年.md "wikilink")。

<!-- end list -->

  - **週五怪誕夜**

<!-- end list -->

  -
    於星期五晚上10時播映的動畫，曾播映[×××HOLiC及](../Page/×××HOLiC.md "wikilink")[地獄少女](../Page/地獄少女.md "wikilink")。

上述節目區塊於2009年1月5日起廢除。

## 海外形象

海外Animax於2006年6月1日改版，除更改台徽、版面風格外，亦引入更多宣揚創意工業的節目等。該台的觀眾對象為15至24歲的青少年和年青向的成年觀眾。除播放動畫外，全新的Animax亦大力宣傳知識產權的工作。

### 想像王國

短片想像王國（Imagine-Nation）是當時宣揚創意工業以及保護[著作權的訪問節目](../Page/著作權.md "wikilink")。節目中會邀請名人、[CEO等人士如動畫家Nockson](../Page/CEO.md "wikilink")
Fong、[STICKFAS創始人Bany](../Page/STICKFAS.md "wikilink")
J、[新加坡歌手](../Page/新加坡.md "wikilink")[林俊傑](../Page/林俊傑.md "wikilink")（J.
J.
Lin）及[印度首位](../Page/印度.md "wikilink")[一級方程式車手卡菲基因](../Page/一級方程式.md "wikilink")（Narain
Karthikeyan）等講述他們著作權的己見。節目於廣告時段內播放。

### 形象角色

海外Animax當時亦引入形象角色來配合改版行動，計有BATTLE GIRL及SPY KID。Animax
Asia於2014則推出由台灣動畫公司設計的吉祥物狸貓「OO-Kun」\[2\]。

## 動畫作品

Animax的節目製作部在近年自行或協助製作了多套動畫作品，以下列舉為Animax自行製作及參與、協助製作的作品，包括美國加工的美國版動畫。

### 參與製作作品

  - [空戰88區](../Page/空戰88區.md "wikilink")
  - [戀愛小魔女](../Page/戀愛小魔女.md "wikilink")
  - [愛你寶貝](../Page/愛你寶貝.md "wikilink") 又譯 型仔湊女記
  - [野驁射手](../Page/野驁射手.md "wikilink") 又譯 踢出我天地、飆悍射將
  - [神樣家族](../Page/神樣家族.md "wikilink")
  - [哨聲響起](../Page/哨聲響起.md "wikilink") 又譯 足球夢

### 協助製作作品

  - [原子小金剛](../Page/原子小金剛.md "wikilink") 又譯 鐵臂阿童木 （2003年版本）
  - [地獄少女](../Page/地獄少女.md "wikilink")
  - [吟遊默示錄](../Page/吟遊默示錄.md "wikilink")
  - [銀牙傳說WEED](../Page/銀牙傳說WEED.md "wikilink")

### 自行製作作品

  - [禁錮天使](../Page/禁錮天使.md "wikilink") 又譯 LaMB無聲天使

### Animax動畫大獎

**Animax
Awards**，中文官方譯名為**Animax動畫大賞**，於2002年開始成立，成立目的是為了招募更多動畫創作人才，提供動畫迷一個創作靈感的機會。每一年得獎的動畫作品將會由Animax夥拍日本知名的動畫公司共同製作一集約30分鐘的動畫節目，並安排於Animax播放。自2007年開始，Animax動畫大獎更推廣至亞洲地區，亞洲地區的動畫迷可從此活動參與製作動畫的工作。

#### 歷年得獎動畫

  -
    *由於首五屆的參賽作品及主題沒有其官方中文名稱，有關內容會以英文列出，敬請留意。*

<!-- end list -->

  - 2002年：Super Bear（谷 大將）

<!-- end list -->

  -
    製作單位：東映動畫、Animax
    主題：Action Hero

<!-- end list -->

  - 2003年：Azusa Will Help\!（川邊 優子）

<!-- end list -->

  -
    製作單位：TMS、Animax
    主題：Sports Spirit in the 21st Century

<!-- end list -->

  - 2004年：[I Only Want
    Happiness](../Page/I_Only_Want_Happiness.md "wikilink")（宮崎 麻耶）

<!-- end list -->

  -
    製作單位：Sunrise、Animax
    主題：Robots

<!-- end list -->

  - 2005年：Lily and Frog and (Little Brother)（吉成 郁子）

<!-- end list -->

  -
    製作單位：東映動畫、Animax
    主題：Adventure Action

<!-- end list -->

  - 2006年：Yumedamaya Kidan（）

<!-- end list -->

  -
    製作單位：Production IG、Animax
    主題：A Story you Wish to be Animated

<!-- end list -->

  - 2007年：高嶺的單車（

<!-- end list -->

  -
    製作單位：A1 Pictures、Animax
    主題：A Story I Wish to be Animated

<!-- end list -->

  - 2008年：書家（

<!-- end list -->

  -
    製作單位：Production IG、Animax
    主題：Action

## 爭議事件

## 註釋

## 參見

  - [Animax Asia](../Page/Animax_Asia.md "wikilink")
  - [Animax播映節目列表](../Page/Animax播映節目列表.md "wikilink")（臺灣、香港、日本）
  - [日本動畫](../Page/日本動畫.md "wikilink")
  - [電視台](../Page/電視台.md "wikilink")

## 外部連結

  - [Animax全球網站](http://www.animaxtv.com)

  - [Animax日本](http://www.animax.co.jp)

  - [Animax亞洲](https://web.archive.org/web/20070217172845/http://www.animax-asia.com/)


  - [Animax台灣](http://www.animax-taiwan.com)

  - [Animax HD MOD節目表](http://mod.cht.com.tw/tv/channel.php?id=97)

  - [Animax HD官方網站](http://www.animaxhd-tw.com/)

  - [Animax韓國](http://www.animaxtv.co.kr/)

  -

  -

  -

  -

  -

[Category:日本電視台](../Category/日本電視台.md "wikilink")
[Category:動畫關係企業](../Category/動畫關係企業.md "wikilink")
[Category:国际媒体](../Category/国际媒体.md "wikilink")
[Category:索尼子公司](../Category/索尼子公司.md "wikilink")
[A](../Category/索尼影視電視公司.md "wikilink")
[Category:1998年成立的公司](../Category/1998年成立的公司.md "wikilink")

1.
2.