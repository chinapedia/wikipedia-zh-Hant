**Animelo Summer Live**（，直譯為「動畫旋律的夏季演唱會」，簡稱為（Ani
Summer））是由開設動畫歌曲專門的手機來電答鈴網站「Animelomix」的[DWANGO公司](../Page/DWANGO.md "wikilink")，以及[文化放送電台主辦的日本國內最大的動畫歌曲演唱會](../Page/文化放送.md "wikilink")，此活動從2005年開始舉辦。出席的歌手雖然限為[動畫](../Page/動畫.md "wikilink")[歌手](../Page/歌手.md "wikilink")，但是演唱會的歌手不只唱動畫的主題曲，電玩、[特攝的主題曲](../Page/特攝.md "wikilink")，以及原創歌曲也有包含在內。每年在演唱會舉辦前夕，官方都會分批宣佈參加演出歌手名單，並會邀請一些神秘或特別嘉賓參與演出。

自2013年起，[NHK BS
Premium每年都会播出Animelo](../Page/NHK_BS_Premium.md "wikilink")
Summer Live的精选影像。

## 2005年

  - 名稱：**Animelo Summer Live 2005 -THE BRIDGE-**
  - 舉辦日：2005年7月10日
  - 會場：[國立代代木競技場第一體育館](../Page/國立代代木競技場.md "wikilink")

### 演出者

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [奧井雅美](../Page/奧井雅美.md "wikilink")
  - [影山浩宣](../Page/影山浩宣.md "wikilink")
  - [JAM Project](../Page/JAM_Project.md "wikilink")
  - [水樹奈奈](../Page/水樹奈奈.md "wikilink")
  - [高橋直純](../Page/高橋直純.md "wikilink")
  - [栗林美奈實](../Page/栗林美奈實.md "wikilink")
  - [米倉千尋](../Page/米倉千尋.md "wikilink")
  - [石田燿子](../Page/石田燿子.md "wikilink")
  - [can/goo](../Page/can/goo.md "wikilink")
  - [下川美娜](../Page/下川美娜.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [unicorn table](../Page/unicorn_table.md "wikilink")
  - [鈴木達央](../Page/鈴木達央.md "wikilink")
  - [近江知永](../Page/近江知永.md "wikilink")
  - [愛內里菜](../Page/愛內里菜.md "wikilink")

</div>

### 主題曲

  - ONENESS（作詞、作曲：奧井雅美）

### 演唱歌曲一覽

1.  TRANSMIGRATION ／
    水樹奈奈、奧井雅美（[ESアワーラジヲのおじかん](../Page/ESアワーラジヲのおじかん.md "wikilink")
    印象歌曲）
2.  輪舞-revolution ／ 奧井雅美（[少女革命歐蒂娜](../Page/少女革命.md "wikilink") 主題曲）
3.  A confession of TOKIO ／ 奧井雅美（Animelomix CM曲）
4.  TRUST ／ 奧井雅美（[我的主人愛作怪](../Page/我的主人愛作怪.md "wikilink") 主題曲）
5.  OPEN YOUR MIND～小さな羽根ひろげて ／ 石田燿子（[幸運女神](../Page/幸運女神.md "wikilink")
    電視版系列（第一期）主題曲）
6.  情熱の女神 ／ 石田燿子（[Anime TV](../Page/Anime_TV.md "wikilink") 片尾曲）
7.  [残酷な天使のテーゼ](../Page/残酷な天使のテーゼ.md "wikilink") ／
    石田燿子、奧井雅美（[新世紀福音戰士](../Page/新世紀福音戰士.md "wikilink")
    主題曲）
8.  ウィーアー\! ／
    [北谷洋](../Page/北谷洋.md "wikilink")、[遠藤正明](../Page/遠藤正明.md "wikilink")、高橋直純（[ONE
    PIECE](../Page/ONE_PIECE.md "wikilink") 主題曲）
9.  KUJIKENAIKARA\! ／ 栗林美奈實、下川美娜（[秀逗魔導士無印](../Page/秀逗魔導士.md "wikilink")
    片尾曲）
10. CHA-LA HEAD CHA-LA ／ 影山浩宣（[七龍珠Z](../Page/七龍珠.md "wikilink") 主題曲）
11. 夢光年 ／ 影山浩宣（[宇宙船サジタリウス](../Page/宇宙船サジタリウス.md "wikilink") 片尾曲）
12. Just a Survivor ／ 鈴木達央（[喜歡所以喜歡](../Page/喜歡所以喜歡.md "wikilink") 主題曲）
13. tomorrow ／ 下川美娜（[驚爆危機](../Page/驚爆危機.md "wikilink") 主題曲）
14. 南風 ／ 下川美娜（[驚爆危機 The Second Raid](../Page/驚爆危機.md "wikilink") 主題曲）
15. 遙か、君のもとへ… ／ 高橋直純（[遙久時空](../Page/遙久時空.md "wikilink") 主題曲）
16. glorydays ／ 高橋直純（S.S.D.S.～刹那の英雄～ 印象曲）
17. 君に会えてよかった（iromelo-mix）／ 高橋直純（RADIO Animelomix 主題曲）
18. FLY AWAY ／ unicorn
    table（[Jinki:EXTEND](../Page/Jinki:EXTEND.md "wikilink") 主題曲）
19. 嵐の中で輝いて ／ 米倉千尋（[機動戰士鋼彈 第08MS小隊](../Page/機動戰士鋼彈.md "wikilink") 主題曲）
20. 永遠の扉／ 米倉千尋（[機動戰士鋼彈 第08MS小隊劇場版 Miller's
    Report](../Page/機動戰士鋼彈.md "wikilink") 主題曲）
21. Ani Summer Special Medley ／ 米倉千尋
    1.  WILL（[仙界傳 封神演義](../Page/仙界傳_封神演義.md "wikilink") 主題曲）
    2.  Little Soldier（遊戲[實況野球8](../Page/實況野球.md "wikilink") 主題曲）
    3.  僕のスピードで（[我們的仙境](../Page/我們的仙境.md "wikilink") 片尾曲）
22. ウタカタ ／ 近江知永（電視節目[Dream Factory](../Page/Dream_Factory.md "wikilink")
    片尾曲）
23. まぼろし ／ can/goo（[妹妹公主Repure](../Page/妹妹公主.md "wikilink") 主題曲）
24. 教えてあげる ／ can/goo（[老師的時間](../Page/老師的時間.md "wikilink") 主題曲）
25. Precious Memories ／
    栗林美奈實（電視動畫[你所期望的永遠](../Page/你所期望的永遠.md "wikilink")
    主題曲）
26. Blue Treasure ／ 栗林美奈實（[Tide-Line
    Blue](../Page/Tide-Line_Blue.md "wikilink") 主題曲）
27. マブラヴ ／ 栗林美奈實（[Muv-Luv](../Page/Muv-Luv.md "wikilink") 主題曲）
28. SKILL ／ JAM Project（[第2次超級機器人大戰α](../Page/第2次超級機器人大戰α.md "wikilink")
    主題曲）
29. 限界バトル ／ JAM Project（[遊戲王GX](../Page/遊戲王GX.md "wikilink") 片尾曲）
30. 迷宮のプリズナー ／ JAM Project（[超級機器人大戰ORIGINAL GENERATION THE
    ANIMATION](../Page/超級機器人大戰ORIGINAL_GENERATION_THE_ANIMATION.md "wikilink")
    主題曲）
31. VICTORY ／ JAM
    Project、石田燿子、米倉千尋（[超級機器人大戰MX](../Page/超級機器人大戰MX.md "wikilink")
    主題曲）
32. I can't stop my love for you♥／
    愛內里菜（[名偵探柯南](../Page/名偵探柯南.md "wikilink")
    主題曲）
33. 恋はスリル、ショック、サスペンス ／ 愛內里菜（[名偵探柯南](../Page/名偵探柯南.md "wikilink") 主題曲）
34. Still in the groove ／ 水樹奈奈（いろメロミックス CM曲）
35. Take a shot ／ 水樹奈奈（[魔法少女奈葉](../Page/魔法少女奈葉.md "wikilink") 插入曲）
36. WILD EYES ／
    水樹奈奈（[BASILISK甲賀忍法帖](../Page/BASILISK甲賀忍法帖.md "wikilink")
    片尾曲）
37. POWER GATE ／ 女性出演者（タイアップ無し）
38. ONENESS ／ Ani Summer Friends
      -
        **-安可曲-**
39. ACCESS\! ／ 高橋直純、石田燿子、遠藤正明、奧井雅美、栗林美奈實、下川美娜、鈴木達央、水樹奈奈、米倉千尋（Radio
    Animemix 主題曲）
40. ONENESS ／ 所有表演歌手

## 2006年

  - 名稱：**Animelo Summer Live 2006 -OUTRIDE-**
  - 會場：2006年7月8日
  - 會場：[日本武道館](../Page/日本武道館.md "wikilink")

### 演出者

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [奧井雅美](../Page/奧井雅美.md "wikilink")
  - [JAM Project](../Page/JAM_Project.md "wikilink")
  - [水樹奈奈](../Page/水樹奈奈.md "wikilink")
  - [高橋直純](../Page/高橋直純.md "wikilink")
  - [栗林美奈實](../Page/栗林美奈實.md "wikilink")
  - [米倉千尋](../Page/米倉千尋.md "wikilink")
  - [石田燿子](../Page/石田燿子.md "wikilink")
  - [愛內里菜](../Page/愛內里菜.md "wikilink")
  - [ALI PROJECT](../Page/ALI_PROJECT.md "wikilink")
  - [三枝夕夏IN db](../Page/三枝夕夏IN_db.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [石川智晶](../Page/石川智晶.md "wikilink")
  - [savage genius](../Page/savage_genius.md "wikilink")
  - [KENN with The NaB's](../Page/KENN.md "wikilink")
  - [嘉陽愛子](../Page/嘉陽愛子.md "wikilink")
  - 特別來賓（[涼宮春日的憂鬱](../Page/涼宮春日的憂鬱.md "wikilink")）
      - [平野綾](../Page/平野綾.md "wikilink")
      - [茅原實里](../Page/茅原實里.md "wikilink")
      - [後藤邑子](../Page/後藤邑子.md "wikilink")

</div>

### 演唱歌曲一覽

1.  MASK ／ 奧井雅美、栗林美奈實（[魔幻獵人](../Page/魔幻獵人.md "wikilink") 片尾曲）
2.  Rumbling hearts ／ 栗林美奈實（遊戲[你所期望的永遠](../Page/你所期望的永遠.md "wikilink")
    主題曲）
3.  Crystal Energy ／ 栗林美奈實（[舞-乙HiME](../Page/舞-乙HiME.md "wikilink") 主題曲）
4.  幸せのいろ ／ 石田燿子（[幸運女神 それぞれの翼](../Page/幸運女神.md "wikilink") 主題曲）
5.  紅の静寂 ／ 石田燿子（[灼眼的夏娜](../Page/灼眼的夏娜.md "wikilink") 片尾曲）
6.  電光石火の恋 ／ 高橋直純（[遙久時空3](../Page/遙久時空3.md "wikilink") 印象歌曲）
7.  無敵なsmile ／ 高橋直純（[無敵看板娘](../Page/無敵看板娘.md "wikilink") 片尾曲）
8.  OK\! ／ 高橋直純（タイアップ無し）
9.  女子アニソンメドレー
    1.  嵐の中で輝いて ／ 米倉千尋（[機動戰士鋼彈 第08MS小隊](../Page/機動戰士鋼彈.md "wikilink")
        主題曲）
    2.  乙女のポリシー ／ 石田燿子（[美少女戰士R](../Page/美少女戰士.md "wikilink") 主題曲）
    3.  Shining☆Days ／ 栗林美奈實（[舞-HiME](../Page/舞-HiME.md "wikilink") 主題曲）
    4.  [めざせポケモンマスター](../Page/めざせポケモンマスター.md "wikilink") ／
        松本梨香（[神奇寶貝](../Page/神奇寶貝.md "wikilink") 主題曲）
10. [ハレ晴レユカイ](../Page/ハレ晴レユカイ.md "wikilink") ／
    平野綾、茅原實里、後藤邑子（[涼宮春日的憂鬱](../Page/涼宮春日的憂鬱.md "wikilink")
    片尾曲）
11. Wake Up Your Heart ／ KENN with The
    NaB's（[遊戲王GX](../Page/遊戲王GX.md "wikilink") 片尾曲）
12. Forever... ／ savage genius（[武器種族傳說](../Page/武器種族傳說.md "wikilink")
    主題曲）
13. 祈りの詩 ／ savage genius（[SIMOUN](../Page/SIMOUN.md "wikilink") 片尾曲）
14. 愛してね♥もっと ／ 嘉陽愛子（[天上天下](../Page/天上天下.md "wikilink") 片尾曲）
15. 瞳の中の迷宮 ／ 嘉陽愛子（[闇與帽子與書的旅人](../Page/闇與帽子與書的旅人.md "wikilink") 主題曲）
16. WILL ／ 米倉千尋（[封神演義](../Page/封神演義.md "wikilink") 主題曲）
17. 青空とキミへ ／ 米倉千尋（タイアップ無し）
18. 男子アニソンメドレー
    1.  CHA-LA HEAD CHA-LA ／ 影山浩宣（[七龍珠Z](../Page/七龍珠Z.md "wikilink")
        主題曲）
    2.  PLANET DANCE ／ 福山芳樹（[超時空要塞7](../Page/超時空要塞.md "wikilink") 插入曲）
    3.  ウィーアー！ ／ 北谷洋（[ONE PIECE](../Page/ONE_PIECE.md "wikilink") 主题曲）
    4.  勇者王誕生！ ／ 遠藤正明（[勇者王](../Page/勇者王.md "wikilink") 主題曲）
    5.  SOLDIER DREAM～聖闘士神話～ ／
        影山浩宣、福山芳樹、北谷洋、遠藤正明（[聖闘士星矢](../Page/聖闘士星矢.md "wikilink")
        主題曲）
19. Candy Lie ／
    [r.o.r/s](../Page/r.o.r/s.md "wikilink")（奧井雅美、米倉千尋）（タイアップ無し）
20. SECOND IMPACT ／ 奧井雅美（Animelomix CM曲）
21. WILD SPICE ／ 奧井雅美（[無敵看板娘](../Page/無敵看板娘.md "wikilink") 主題曲）
22. zero-G- ／ 奧井雅美（[RAY THE
    ANIMATION](../Page/RAY_THE_ANIMATION.md "wikilink") 主題曲）
23. 君と約束した優しいあの場所まで ／ 三枝夕夏 IN db（[名偵探柯南](../Page/名偵探柯南.md "wikilink")
    主題曲）
24. Everybody Jump ／ 三枝夕夏 IN db（[格闘美神 武龍
    REBIRTH](../Page/格闘美神_武龍_REBIRTH.md "wikilink") 主題曲）
25. 100もの扉／[愛內里菜&三枝夕夏](../Page/愛內里菜&三枝夕夏.md "wikilink")（コーラス：[スパークリング☆ポイント](../Page/スパークリング☆ポイント.md "wikilink")）（[名偵探柯南](../Page/名偵探柯南.md "wikilink")
    主題曲）
26. GLORIOUS ／ 愛內里菜（[Another Century's Episode
    2](../Page/Another_Century's_Episode#Another_Century's_Episode_2.md "wikilink")
    印象歌曲）
27. MIRACLE -Allegro. vivacemix- ／ 愛內里菜（[MÄR
    -メルヘヴン-](../Page/MÄR.md "wikilink") 片尾曲）
28. [聖少女領域](../Page/聖少女領域.md "wikilink") ／ ALI
    PROJECT（[薔薇少女第2期](../Page/薔薇少女.md "wikilink") 主題曲）
29. [亡國覚醒カタルシス](../Page/亡國覚醒カタルシス.md "wikilink") ／ ALI
    PROJECT（[.hack//Roots](../Page/.hack/Roots.md "wikilink") 片尾曲）
30. GONG（Album version）／ JAM Project（[第3次超級機器人大戰α
    終焉的銀河](../Page/第3次超級機器人大戰α_終焉的銀河.md "wikilink")
    主題曲）
31. 牙狼～SAVIOR IN THE DARK～ ／ JAM
    Project（[GARO-牙狼-](../Page/GARO-牙狼-.md "wikilink") 主題曲）
32. SKILL ／ JAM
    Project、米倉千尋、栗林美奈實、石田燿子（[第2次超級機器人大戰α](../Page/第2次超級機器人大戰α.md "wikilink")
    主題曲）
33. 美しければそれでいい ／ 石川智晶（[SIMOUN](../Page/SIMOUN.md "wikilink") 主題曲）
34. あんなに一緒だったのに ／ 石川智晶（[機動戰士GUNDAM
    SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink") 片尾曲）
35. SUPER GENERATION ／ 水樹奈奈（[やぐちひとり](../Page/やぐちひとり.md "wikilink") 片尾曲）
36. innocent starter ／ 水樹奈奈（[魔法少女奈葉](../Page/魔法少女奈葉.md "wikilink") 主題曲）
37. ヒメムラサキ ／ 水樹奈奈（[BASILISK甲賀忍法帖](../Page/BASILISK甲賀忍法帖.md "wikilink")
    片尾曲）
38. ETERNAL BLAZE ／ 水樹奈奈（[魔法少女奈葉A's](../Page/魔法少女奈葉A's.md "wikilink")
    主題曲）
39. OUTRIDE ／ Ani Summer Friends（2006年主題曲）
      -
        **-安可曲-**
40. ONENESS ／ Ani Summer Friends（2005年主題曲）
41. OUTRIDE ／ 出演者全員（2006年主題曲）

### 廣播

  -
    2006 年4月8日より9月30日まで、『[A\&G 超RADIO
    SHOW～アニスパ\!～](../Page/A&G_超RADIO_SHOW～アニスパ!～.md "wikilink")』（文化放送）內にて、Animelo
    Summer Live 2006と連動したミニ番組『アニサマSTATION』が放送されていた。パーソナリティは奧井雅美と栗林美奈實。

### CD

  - OUTRIDE（作詞：[奧井雅美](../Page/奧井雅美.md "wikilink")／作曲：[影山浩宣](../Page/影山浩宣.md "wikilink")／編曲：[河野陽吾](../Page/河野陽吾.md "wikilink")）

<!-- end list -->

  -
    Animelo Summer 2006-OUTRIDE-主題曲
    2006年6月9日發售／[Geneon
    Entertainment](../Page/Geneon_Entertainment.md "wikilink")

### DVD

  - Animelo Summer Live 2006 -OUTRIDE- I

<!-- end list -->

  -
    2006年12月21日發售／[Kingrecords](../Page/Kingrecords.md "wikilink")

<!-- end list -->

  - Animelo Summer Live 2006 -OUTRIDE- II

<!-- end list -->

  -
    2006年12月21日發售／[Victor音樂](../Page/Victor音樂.md "wikilink")

※註：若購買DVD 以上表演曲目並沒有完全收錄其中 請參考官方網站商品項目查詢 考慮是否購買。

## 2007年

  - 名稱：**Animelo Summer Live 2007 -Generation-A-**
  - 舉辦日：2007年7月7日
  - 會場：日本武道館

### 演出者

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [奧井雅美](../Page/奧井雅美.md "wikilink")
  - [JAM Project](../Page/JAM_Project.md "wikilink")
  - [水樹奈奈](../Page/水樹奈奈.md "wikilink")
  - [高橋直純](../Page/高橋直純.md "wikilink")
  - [栗林美奈實](../Page/栗林美奈實.md "wikilink")
  - [ALI PROJECT](../Page/ALI_PROJECT.md "wikilink")
  - [桃井晴子](../Page/桃井晴子.md "wikilink")
  - [樹海](../Page/樹海_\(樂團\).md "wikilink")
  - [m.o.v.e](../Page/m.o.v.e.md "wikilink")
  - [近江知永](../Page/近江知永.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [PSYCHIC LOVER](../Page/PSYCHIC_LOVER.md "wikilink")
  - [Suara](../Page/Suara.md "wikilink")
  - [Cy-Rim rev.](../Page/Cy-Rim_rev..md "wikilink")
  - [茅原實里](../Page/茅原實里.md "wikilink")
  - 特別來賓
      -
      - [平野綾](../Page/平野綾.md "wikilink")

</div>

### 演唱歌曲一覽

1.  　輪舞-revolution／奥井雅美、水樹奈奈（[少女革命片頭曲](../Page/少女革命.md "wikilink")）
2.  　-w-／奥井雅美
3.  　空にかける橋／奥井雅美（[Tales Of Eternia](../Page/永恆傳奇.md "wikilink") THE
    ANIMATION 主題曲）
4.  　Romantic summer／桃井晴子（[瀨戶的新娘](../Page/瀨戶的花嫁_\(漫畫\).md "wikilink")
    片頭曲）
5.  　WONDER MOMO-i／桃井晴子（[太鼓の達人系列](../Page/太鼓の達人.md "wikilink")）
6.  　純白サンクチュアリィ／茅原實里（キディ、グレイド2 パイロットDVD テーマソング）
7.  　君がくれたあの日／茅原實里
8.  　恋のDice☆教えて／Cy-Rim rev.（[CircusLand
    I](../Page/CircusLand_I.md "wikilink") 主題歌）
9.  　聖少女領域／宝野アリカ、水樹奈奈（[薔薇少女](../Page/薔薇少女.md "wikilink") 主題曲）
10. 　てのひら／高橋直純
11. 　君に会えてよかった／高橋直純（RADIO AnimeloMix 片尾曲）
12. 　Yell！／栗林美奈實（超級機器人大戰OG 片尾曲）
13. 　翼はPleasure Line／栗林美奈實（[聖槍修女](../Page/聖槍修女.md "wikilink") 片頭曲）
14. 　夢想歌／Suara（[傳頌之物](../Page/傳頌之物.md "wikilink") 片頭曲）
15. 　キミガタメ／Suara（[傳頌之物](../Page/傳頌之物.md "wikilink") 片尾曲）
16. 　Float～空の彼方で～／近江知永（SoltyRei 片尾曲）
17. 　ロックリバーへ／近江知永（あらいぐまラスカル OPカバー）
18. 　うしろゆびさされ組／栗林美奈實、桃井晴子（ハイスクール\!奇面組 OPカバー）
19. 　Systematic Fantasy／m.o.v.e（激走\!GT 片尾曲）
20. 　Gamble Rumble／m.o.v.e（[頭文字D](../Page/頭文字D.md "wikilink") 主題曲）
21. 　レッツゴー！陰陽師／矢部野彦麿＆琴姫 With 坊主ダンサーズ
22. 　特捜戦隊デカレンジャー／サイキックラバー（特捜戦隊デカレンジャー 主題曲）
23. 　XTC／サイキックラバー（ウィッチブレイド 主題曲）
24. 　あなたがいた森／樹海（[Fate/Stay Night](../Page/Fate/Stay_Night.md "wikilink")
    片尾曲）
25. 　咲かせてはいけない花／樹海
26. 　勇侠青春謳／ALI PROJECT（[Code Geass
    反叛的魯路修](../Page/Code_Geass_反叛的魯路修.md "wikilink")
    片尾曲）
27. 　暗黒天国／ALI PROJECT（[花鈴的魔法戒](../Page/花鈴的魔法戒.md "wikilink") 片頭曲）
28. 　跪いて足をお嘗め／ALI PROJECT（怪物王女 ED）
29. 　魂のルフラン、残酷な天使のテーゼ／高橋洋子（[新世紀福音戰士](../Page/新世紀福音戰士.md "wikilink")）
30. 　冒険でしょでしょ？／平野綾（[涼宮春日的憂鬱](../Page/涼宮春日的憂鬱.md "wikilink") 主題曲）
31. 　Break Out／JAM Project（[超級機器人大戰OG](../Page/超級機器人大戰OG.md "wikilink")
    主題曲）
32. 　VICTORY／JAM Project（[超級機器人大戰MX](../Page/超級機器人大戰MX.md "wikilink")
    主題曲）
33. 　SKILL／JAM
    Project（[第二次超級機器人大戰alpha](../Page/第二次超級機器人大戰alpha.md "wikilink")
    片頭曲）
34. 　Justice to Believe／水樹奈奈（[狂野歷險 the Vth
    Vanguard](../Page/狂野歷險_the_Vth_Vanguard.md "wikilink") 主題曲）
35. 　SECRET
    AMBITION／水樹奈奈（[魔法少女奈葉StrikerS](../Page/魔法少女奈葉StrikerS.md "wikilink")
    片頭曲）
36. 　Heart-shaped chant／水樹奈奈（[光明之風](../Page/光明之風.md "wikilink") OP）
37. 　Generation-A／Ani Summer Friends（2007年主題曲）
      -
        **-安可曲-**
38. 　OUTRIDE／Ani Summer Friends（2006年主題曲）
39. 　Generation-A／出演アーティスト＋バンドメンバー（2007年主題曲）

### CD/DVD產品

  - Generation-A（作詞：奧井雅美／作曲：奧井雅美／編曲：MACARONI☆、Monta）

<!-- end list -->

  -
    Animelo Summer Live 2007 -Generation-A- 主題曲
    2007年6月20日發售／[DWANGO AG
    Entertainment](../Page/DWANGO_AG_Entertainment.md "wikilink")

<!-- end list -->

  - Animelo Summer Live 2007 -GENERATION A-

<!-- end list -->

  -
    2007年11月28日發售／Lantis
    ※共三片DVD。含彩排過程畫面。

※註：若購買DVD 以上表演曲目並沒有完全收錄其中 請參考官方網站商品項目查詢 考慮是否購買。

### 電視

  -
    2007
    年4月19日，在奧井雅美主持的音樂節目『[@Tunes.](../Page/@Tunes..md "wikilink")』（[神奈川電視台](../Page/神奈川電視台.md "wikilink")）之中，每週約播放10分鐘左右的活動及之後的歌曲排
    行榜單元『Ani Summer STATION』。主持人同樣與2006年「Ani Summer STATION」為奧井雅美與栗林美奈實。
    另外在官方網站中也有播放網路串流的節目。

### CM

2007年4月2日－2007年7月7日間在以下的節目中以廣告播放。

#### 電視

  - 音樂節目「@Tunes.」（神奈川電視台、毎週四 25:15～）
  - 電視動畫「[魔法少女奈葉StrikerS](../Page/魔法少女奈葉.md "wikilink")」（神奈川電視台及其他6電視台、毎週三
    27:15～）
  - 電視動畫「[英雄時代](../Page/英雄時代.md "wikilink")」（[東京電視台系其他](../Page/東京電視台.md "wikilink")5電視台、毎週日
    25:30～）

#### 廣播

  - [文化放送各個](../Page/文化放送.md "wikilink")[A\&G
    Zone節目中](../Page/A&G_Zone.md "wikilink")，有各個藝人的說明播放及告知

## 2008年

  - 名稱：**Animelo Summer Live 2008 -CHALLENGE-**
  - 舉辦日：2008年8月30日、8月31日
  - 會場：[埼玉超級競技場](../Page/埼玉超級競技場.md "wikilink")

### 演出者

#### 8月30日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [AKINO from bless4](../Page/AKINO_from_bless4.md "wikilink")
  - [ALI PROJECT](../Page/ALI_PROJECT.md "wikilink")
  - [石田燿子](../Page/石田燿子.md "wikilink")
  - [奥井雅美](../Page/奥井雅美.md "wikilink")
  - [可憐Girl's](../Page/可憐Girl's.md "wikilink")
  - [GRANRODEO](../Page/GRANRODEO.md "wikilink")
  - [CooRie](../Page/CooRie.md "wikilink")
  - [栗林美奈實](../Page/栗林美奈實.md "wikilink")
  - [Suara](../Page/Suara.md "wikilink")
  - [savage genius](../Page/savage_genius.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [田村由香里](../Page/田村由香里.md "wikilink")
  - [茅原實里](../Page/茅原實里.md "wikilink")
  - [AAA](../Page/AAA.md "wikilink")
  - [水樹奈奈](../Page/水樹奈奈.md "wikilink")
  - [m.o.v.e](../Page/m.o.v.e.md "wikilink")
  - [yozuca\*](../Page/yozuca*.md "wikilink")

</div>

#### 8月31日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [ave;new](../Page/ave;new.md "wikilink")
    feat.[佐倉紗織](../Page/佐倉紗織.md "wikilink")

  - [石川智晶](../Page/石川智晶.md "wikilink")

  - [ELISA](../Page/ELISA.md "wikilink")

  - [黑薔薇保存會](../Page/黑薔薇保存會.md "wikilink")

  - [PSYCHIC LOVER](../Page/PSYCHIC_LOVER.md "wikilink")

  - Sound Horizon

  - [JAM Project](../Page/JAM_Project.md "wikilink")

  -
  - [中村繪里子](../Page/中村繪里子.md "wikilink")、[今井麻美](../Page/今井麻美.md "wikilink")、[高橋智秋](../Page/高橋智秋.md "wikilink")、[下田麻美](../Page/下田麻美.md "wikilink")
    from [THE IDOLM@STER](../Page/THE_IDOLM@STER.md "wikilink")

  - [平野綾](../Page/平野綾.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [福山芳樹](../Page/福山芳樹.md "wikilink")
  - [miko](../Page/miko.md "wikilink")
  - [美郷あき](../Page/美郷あき.md "wikilink")
  - [May'n](../Page/May'n.md "wikilink")
  - [桃井晴子](../Page/桃井晴子.md "wikilink")
  - [MOSAIC.WAV](../Page/MOSAIC.WAV.md "wikilink")
  - [米倉千尋](../Page/米倉千尋.md "wikilink")
  - [Lia](../Page/Lia.md "wikilink")
  - [妖精帝國](../Page/妖精帝國.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

</div>

### 主題曲

  - 「Yells 〜It's a beautiful life〜」
    作詞：奧井雅美，作曲：影山浩宣，編曲：鈴木Daichi秀行

### 演唱歌曲一覽

#### 8月30日

1.  「恋せよ女の子」～「アノネ～まみむめ☆もがちょ」／水樹奈奈＋田村由香里
    （[極上生徒會](../Page/極上生徒會.md "wikilink") 片頭曲、
    片頭曲）

2.  [童話迷宮](../Page/童話迷宮.md "wikilink")／田村由香里
    （[童話槍手小紅帽](../Page/童話槍手小紅帽.md "wikilink")
    前期片頭曲）

3.  [バンビーノ・バンビーナ](../Page/バンビーノ・バンビーナ.md "wikilink")／田村由香里

4.  ／田村由香里

5.  STRIKE WITCHES～わたしにできること～／石田燿子（[Strike
    Witches](../Page/Strike_Witches.md "wikilink") 片頭曲）

6.  永遠の花/石田燿子 （[青出於藍](../Page/青出於藍_\(動畫\).md "wikilink")（12話）片尾曲）

7.  サクラサクミライコイユメ／yozuca\* （[初音島](../Page/初音島.md "wikilink") 片頭曲）

8.  Morning - sugar rays／yozuca\* （同名专辑）

9.  センチメンタル／CooRie （[美鳥伴身邊](../Page/美鳥伴身邊.md "wikilink") 片頭曲）

10. 存在／CooRie （初音島 片尾曲）

11. DIVE INTO STREAM／m.o.v.e （PlayStation 3"頭文字D Extreme Stage"使用曲）

12. Gamble Rumble／m.o.v.e（[頭文字D](../Page/頭文字D.md "wikilink")）

13. ZERØ／AAA （[毀滅世界的六人](../Page/毀滅世界的六人.md "wikilink") 片頭曲）

14. Climax Jump／AAA（[假面騎士電王](../Page/假面騎士電王.md "wikilink") 片頭曲）

15. Over The Future／可憐Gir'ls
    （[楚楚可憐超能少女組](../Page/楚楚可憐超能少女組.md "wikilink")
    片頭曲）

16. nowhere／savage genius/茅原実里/yozuca\*
    （[異域天使](../Page/異域天使.md "wikilink") 插曲）

17. JUST TUNE／savage genius （[夜櫻四重奏](../Page/夜櫻四重奏.md "wikilink") 片頭曲）

18. 想いを奏でて／savage genius （[詩片](../Page/詩片.md "wikilink") 片頭曲）

19. 創聖のアクエリオン／AKINO from
    bless4（[創聖的亞庫艾里翁](../Page/創聖的亞庫艾里翁.md "wikilink")
    片頭曲）

20. Go Tight！／AKINO from bless4（創聖的亞庫艾里翁 片頭曲）

21. 慟哭ノ雨／GRANRODEO（恋する天使アンジェリーク～かがやきの明日～ 片頭曲）

22. ケンゼンな本能／GRANRODEO

23. Love Jump／栗林美奈實（[紅](../Page/紅_\(小說\).md "wikilink") 片頭曲）

24. Next Season／栗林美奈實（[你所期待的永遠～Next
    Season～](../Page/你所期望的永遠.md "wikilink") 片頭曲）

25. Shining☆Days／栗林美奈實（[舞-HiME](../Page/舞-HiME.md "wikilink") OP）

26. haunting Melody／Suara（PS3ソフト「ティアーズ、トゥ、ティアラ」）

27. 星座／Suara （鎖 -クサリ- 片尾曲）

28. Contact / 茅原実里（Contact IN）

29. 诗人之旅 / 茅原実里（Contact IN）

30. 雨上がりの花よ咲け / 茅原実里（Contact IN）

31. 雪、無音、窓辺にて。／茅原実里（[涼宮春日的憂鬱](../Page/涼宮春日的憂鬱.md "wikilink") 角色歌曲）

32. 輪舞-revolution／奥井雅美+茅原実里（[少女革命](../Page/少女革命.md "wikilink") 片頭曲）

33. INSANITY／奥井雅美（[Muv-Luv
    Alternative](../Page/Muv-Luv_Alternative.md "wikilink") 片頭曲）

34. コトダマ／ALI PROJECT（[死後文](../Page/死後文.md "wikilink") OP）

35. 愛と誠／ALI PROJECT

36. わが臈たし悪の華／ALI PROJECT（[Code Geass
    反叛的魯路修R2](../Page/Code_Geass_反叛的魯路修.md "wikilink")
    片尾曲）

37. 残光のガイア／水樹奈奈（セレクションX 片尾曲）

38. Dancing in the velvet
    moon／水樹奈奈（[十字架與吸血鬼](../Page/十字架與吸血鬼.md "wikilink")
    片尾曲）

39. Pray／水樹奈奈（[魔法少女奈葉StrikerS](../Page/魔法少女奈葉StrikerS.md "wikilink") 插曲）

40. ETERNAL
    BLAZE／水樹奈奈＋寶野亞莉華（[魔法少女奈葉A's](../Page/魔法少女奈葉A's.md "wikilink")
    片頭曲）

41. Yells ～It's a beautiful life～ （2008年主題曲）

      -
        **-安可曲-**

42. EN-1 Generation-A （2007年主題曲）

43. EN-2 Yells ～It's a beautiful life～ （2008年主題曲）

44. 終演後BGM　ONENESS

#### 8月31日

1.  思いではおっくせんまん\!／JAM Project+美郷あき
2.  BLOOD QUEEN／美郷あき （[怪物王女](../Page/怪物王女.md "wikilink") OP）
3.  君が空だった／美郷あき （[舞-HiME](../Page/舞-HiME.md "wikilink") ED）
4.  euphoric field／ELISA （[ef-a tale of
    memories](../Page/ef_-_a_fairy_tale_of_the_two..md "wikilink") OP）
5.  HIKARI／ELISA （[隱王](../Page/隱王.md "wikilink") ED）
6.  ハッピー☆マテリアル\[ドメラバstyle\]／ドメスティック、ラヴバンド
    （[魔法老師](../Page/魔法老師_\(動畫\).md "wikilink") OP）
7.  Shangri-La\[ドメラバstyle\]／ドメスティック、ラヴバンド
8.  朝と夜の物語／Sound Horizon
9.  奴隷市場／Sound Horizon
10. 聖戦のイベリアメドレー／Sound Horizon
11. 「花火」～「満天プラネタリウム」（メドレー）／黑薔薇保存會
12. ヒカリ（黒薔薇ver.）／黑薔薇保存會 （[犬神！](../Page/犬神！.md "wikilink") OP）
13. Feel so Easy\!／桃井晴子 （[Mission-E](../Page/Mission-E.md "wikilink")
    ED）
14. LOVE.EXE／桃井晴子
15. 天罰！エンジェルラビィ／UNDER17（桃井晴子+小池雅也）+MOSAIC.WAV
16. 最強○×計画／MOSAIC.WAV （[地上最強新娘](../Page/地上最強新娘.md "wikilink") OP）
17. ガチャガチャきゅ～と、ふぃぎゅ@メイト／MOSAIC.WAV
18. 「true my heart」～「Iris」～「ラブリー☆えんじぇる！！」（メドレー）／ave;new feat.佐倉紗織
19. 魔理沙は大変なものを盗んでいきました／miko（IOSYS）
20. 鳥の詩／Lia （[AIR](../Page/AIR.md "wikilink") OP）
21. IDOLM@STERメドレー／中村繪里子、今井麻美、高橋智秋、下田麻美 from THE IDOLM@STER
    1.  「GO MY WAY\!\!」
    2.  「キラメキラリ」
    3.  ｢relations｣
    4.  「エージェント夜を往く」
    5.  「Do-Dai」
    6.  「蒼い鳥」
    7.  「GO MY WAY\!\!」
22. THE IDOLM@STER／中村繪里子、今井麻美、高橋智秋、下田麻美 from THE IDOLM@STER
23. 真赤な誓い／福山芳樹 （[武裝鍊金](../Page/武裝鍊金.md "wikilink") OP）
24. ノーザンクロス／May'n （[超時空要塞
    Frontier](../Page/超時空要塞_Frontier.md "wikilink") ED）
25. 射手座☆午後九時Don't be late／May'n （超時空要塞 Frontier IN）
26. Precious Time,Glory Days／サイキックラバー
    （[遊戲王GX](../Page/遊戲王GX.md "wikilink") OP）
27. 鼓動～get closer～／サイキックラバー （[魔女之刃](../Page/魔女之刃_\(動畫\).md "wikilink")
    ED）
28. LOVE★GUN／平野綾
29. Unnamed world／平野綾 （[二十面相少女](../Page/二十面相少女.md "wikilink") OP）
30. アンインストール／石川智晶 （[地球防衛少年](../Page/地球防衛少年.md "wikilink") OP）
31. Prototype／石川智晶 （[機動戰士鋼彈 00 第二季](../Page/機動戰士鋼彈_00.md "wikilink") ED）
32. 「あんなに一緒だったのに」～「嵐の中で輝いて」／米倉千尋＋石川智晶
    （[機動戰士鋼彈SEED](../Page/機動戰士鋼彈SEED.md "wikilink")
    ED ～ 機動戦士ガンダム第08小隊OP）
33. 永遠の扉／米倉千尋（[機動戦士ガンダム第08MS小隊
    ミラーズ・リポート](../Page/機動戦士ガンダム第08MS小隊.md "wikilink")
    THEME）
34. FRIENDS／米倉千尋 （[封神演義](../Page/封神演義_\(漫畫\).md "wikilink") ED）
35. No Border／JAM Project
36. Rocks／JAM Project
37. SKILL／JAM Project （[第二次超級機器人大戰α](../Page/超級機器人大戰.md "wikilink") OP）
38. Yells ～It's a beautiful life～ （2008年主題曲）
      -
        **-安可曲-**
39. EN-1 OUTRIDE （2006年主題曲）
40. EN-2 Yells ～It's a beautiful life～ （2008年主題曲）
41. 終演後BGM　ONENESS

## 2009年

  - 名稱：**Animelo Summer Live 2009 -RE:BRIDGE-**
  - 舉辦日：2009年8月22日、8月23日
  - 會場：[埼玉超級競技場](../Page/埼玉超級競技場.md "wikilink")

### 演出者

  - 8月22日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [彩音](../Page/彩音.md "wikilink")
  - [ALI PROJECT](../Page/ALI_PROJECT.md "wikilink")
  - [angela](../Page/angela.md "wikilink")
  - [石川智晶](../Page/石川智晶.md "wikilink")
  - [伊藤香奈子](../Page/伊藤香奈子.md "wikilink")
  - [ELISA](../Page/ELISA_\(歌手\).md "wikilink")
  - [栗林美奈實](../Page/栗林美奈實.md "wikilink")
  - [下田麻美](../Page/下田麻美.md "wikilink")
  - [GRANRODEO](../Page/GRANRODEO.md "wikilink")
  - [JAM Project](../Page/JAM_Project.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [茅原實里](../Page/茅原實里.md "wikilink")
  - [中川翔子](../Page/中川翔子.md "wikilink")（特別出演）
  - [初音未來](../Page/初音未來.md "wikilink")
  - [ビートまりお(COOL\&CREATE)](../Page/COOL&CREATE.md "wikilink")
  - [堀江由衣](../Page/堀江由衣.md "wikilink")
  - [manzo](../Page/manzo.md "wikilink")
  - [宮野真守](../Page/宮野真守.md "wikilink")
  - [May'n](../Page/May'n.md "wikilink")
  - [桃井晴子](../Page/桃井晴子.md "wikilink")

</div>

  - 8月23日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [近江知永](../Page/近江知永.md "wikilink")
  - [大槻ケンヂ與絕望少女們](../Page/大槻ケンヂ.md "wikilink")（[野中藍](../Page/野中藍.md "wikilink")、[新谷良子](../Page/新谷良子.md "wikilink")、[小林優](../Page/小林優.md "wikilink")、[澤城美雪](../Page/澤城美雪.md "wikilink")）
  - [奧井雅美](../Page/奧井雅美.md "wikilink")
  - [GACKT](../Page/Gackt.md "wikilink")
  - [影山浩宣](../Page/影山浩宣.md "wikilink")
  - [PSYCHIC LOVER](../Page/PSYCHIC_LOVER.md "wikilink")
  - [榊原由依](../Page/榊原由依.md "wikilink")
  - [savage genius](../Page/savage_genius.md "wikilink")
  - [Suara](../Page/Suara.md "wikilink")
  - [田村由香里](../Page/田村由香里.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [中村繪里子](../Page/中村繪里子.md "wikilink")、[今井麻美](../Page/今井麻美.md "wikilink")、[仁後真耶子](../Page/仁後真耶子.md "wikilink")
    from THE IDOLM@STER
  - [平野綾](../Page/平野綾.md "wikilink")
  - [FictionJunction](../Page/FictionJunction.md "wikilink")
  - [飛蘭](../Page/飛蘭.md "wikilink")
  - [水樹奈奈](../Page/水樹奈奈.md "wikilink")
  - [m.o.v.e](../Page/m.o.v.e.md "wikilink")
  - [妖精帝國](../Page/妖精帝國.md "wikilink")
  - [米倉千尋](../Page/米倉千尋.md "wikilink")

</div>

### 主題曲

  - RE:BRIDGE〜Return to oneself〜
    作詞：[奧井雅美](../Page/奧井雅美.md "wikilink")，作曲：[栗林美奈實](../Page/栗林美奈實.md "wikilink")，編曲：

### 演唱歌曲一覽

:\* 以下未有特別注明之作品，發佈媒體均為動畫。

  - 8月22日

<!-- end list -->

1.  [残酷な天使のテーゼ](../Page/残酷な天使のテーゼ.md "wikilink") /
    [angela](../Page/angela.md "wikilink") +
    [石川智晶](../Page/石川智晶.md "wikilink")
    （[新世紀福音戰士](../Page/新世紀福音戰士.md "wikilink") 主題曲）
2.  Spiral / [angela](../Page/angela.md "wikilink")
    （[機巧魔神](../Page/機巧魔神.md "wikilink") 主題曲）
3.  Shangri-La / [angela](../Page/angela.md "wikilink")
    （[蒼穹之戰神](../Page/蒼穹之戰神.md "wikilink") 主題曲）
4.  F.D.D. / [伊藤香奈子](../Page/伊藤香奈子.md "wikilink")
    （[CHAOS;HEAD](../Page/CHAOS;HEAD.md "wikilink") 主題曲）
5.  追想のディスペア / [伊藤香奈子](../Page/伊藤香奈子.md "wikilink")
    （[暮蟬悲鳴時絆](../Page/暮蟬悲鳴時.md "wikilink") 第一卷、祟
    主題曲）
6.  愛のメディスン～アニサマバージョン～ / [桃井晴子](../Page/桃井晴子.md "wikilink")
    （[魔法護士小麥](../Page/魔法護士小麥.md "wikilink") 主題曲）
7.  WONDER MOMO-i ～World tour version～ /
    [桃井晴子](../Page/桃井晴子.md "wikilink")
    （[太鼓之達人系列](../Page/太鼓之達人.md "wikilink")）
8.  マイペース大王 / [桃井晴子](../Page/桃井晴子.md "wikilink") +
    [manzo](../Page/manzo.md "wikilink")
    （[現視研](../Page/現視研.md "wikilink") 主題曲）
9.  溝ノ口太陽族 / [manzo](../Page/manzo.md "wikilink")
    （[天體戰士](../Page/天體戰士.md "wikilink") 第一季主題曲）
10. 続・溝ノ口太陽族 / [manzo](../Page/manzo.md "wikilink")
    （[天體戰士](../Page/天體戰士.md "wikilink") 第二季主題曲）
11. Endless Tears... / [彩音](../Page/彩音.md "wikilink")
    （[11eyes](../Page/11eyes.md "wikilink") Xbox360版主題曲）
12. コンプレックス・イマージュ / [彩音](../Page/彩音.md "wikilink") （[暮蟬悲鳴時祭
    澪盡篇](../Page/暮蟬悲鳴時祭_澪盡篇.md "wikilink") 主題曲）
13. Wonder Wind / [ELISA](../Page/ELISA_\(歌手\).md "wikilink")
    （[旋風管家](../Page/旋風管家.md "wikilink") 第二期 主題曲）
14. ebullient future-アニサマVer- /
    [ELISA](../Page/ELISA_\(歌手\).md "wikilink") （[ef - a tale of
    melodies](../Page/ef_-_a_fairy_tale_of_the_two..md "wikilink") 主題曲）
15. ココロ / [下田麻美](../Page/下田麻美.md "wikilink")
16. みくみくにしてあげる♪【してやんよ】 / [初音未來](../Page/初音未來.md "wikilink")
17. ブラック★ロックシューター / [初音未來](../Page/初音未來.md "wikilink")
18. Help me,ERINNNNNN\!\! /
    [ビートまりお(COOL\&CREATE)](../Page/COOL&CREATE.md "wikilink")
19. tRANCE / [GRANRODEO](../Page/GRANRODEO.md "wikilink")
    （[黑神](../Page/黑神.md "wikilink") 主題曲）
20. modern strange cowboy / [GRANRODEO](../Page/GRANRODEO.md "wikilink")
    （[超能力大戰](../Page/超能力大戰.md "wikilink") 主題曲）
21. メドレー／[堀江由衣](../Page/堀江由衣.md "wikilink")
    1.  JET\!\!
    2.  バニラソルト（[TIGER×DRAGON！](../Page/TIGER×DRAGON！.md "wikilink") 片尾曲）
22. YAHHO\!\! / [堀江由衣](../Page/堀江由衣.md "wikilink")
    （[加奈日記](../Page/加奈日記.md "wikilink") 片尾曲）
23. ハッピー☆マテリアル / [堀江由衣](../Page/堀江由衣.md "wikilink") +
    [茅原實里](../Page/茅原實里.md "wikilink")
    （[魔法老師](../Page/魔法老師.md "wikilink") 主題曲）
24. First Pain / [石川智晶](../Page/石川智晶.md "wikilink")
    （[元素猎人](../Page/元素猎人.md "wikilink") 主題曲）
25. Prototype / [石川智晶](../Page/石川智晶.md "wikilink")
    （[機動戰士鋼彈00](../Page/機動戰士鋼彈00.md "wikilink") 片尾曲）
26. J☆S / [宮野真守](../Page/宮野真守.md "wikilink")
27. Discovery / [宮野真守](../Page/宮野真守.md "wikilink") （[夢幻遊戲
    朱雀異聞](../Page/夢幻遊戲.md "wikilink") 主題曲）
28. キミシニタモウコトナカレ / [May'n](../Page/May'n.md "wikilink")
    （[香格里拉](../Page/香格里拉.md "wikilink") 主題曲）
29. ダイヤモンドクレバス / [May'n](../Page/May'n.md "wikilink") （[超时空要塞
    Frontier](../Page/超时空要塞_Frontier.md "wikilink") 片尾曲）
30. ミラクル・アッパーWL / [May'n](../Page/May'n.md "wikilink") +
    [奥井雅美](../Page/奥井雅美.md "wikilink") （ONTAMA\! 主題曲）
31. 空色デイズ / [中川翔子](../Page/中川翔子.md "wikilink")
    （[天元突破紅蓮螺岩](../Page/天元突破.md "wikilink")
    主題曲）
32. 涙の種、笑顔の花 / [中川翔子](../Page/中川翔子.md "wikilink") （劇場版
    [天元突破紅蓮螺岩](../Page/天元突破.md "wikilink") 紅蓮篇 主題曲）
33. What's Up Guys? / [栗林美奈實](../Page/栗林美奈實.md "wikilink") +
    [谷山紀章](../Page/谷山紀章.md "wikilink") （爆走獵人 主題曲）
34. 鬼帝の剣 / [ALI PROJECT](../Page/ALI_PROJECT.md "wikilink")
    （[武裝機甲](../Page/武裝機甲.md "wikilink") 主題曲）
35. 戦慄の子供たち / [ALI PROJECT](../Page/ALI_PROJECT.md "wikilink") （[Phantom
    ～Requiem for the
    Phantom～](../Page/Phantom_～Requiem_for_the_Phantom～.md "wikilink")
    主題曲）
36. 地獄の門 / [ALI PROJECT](../Page/ALI_PROJECT.md "wikilink") （[Phantom
    ～Requiem for the
    Phantom～](../Page/Phantom_～Requiem_for_the_Phantom～.md "wikilink")
    片尾曲）
37. Voyager train / [茅原實里](../Page/茅原實里.md "wikilink")
38. Tomorrow's chance / [茅原實里](../Page/茅原實里.md "wikilink")
39. Paradise Lost/ [茅原實里](../Page/茅原實里.md "wikilink")
    （[食靈](../Page/食靈.md "wikilink") 主題曲）
40. Precious Memories / [栗林美奈實](../Page/栗林美奈實.md "wikilink")
    （[你所期望的永遠](../Page/你所期望的永遠.md "wikilink") 主題曲）
41. 涙の理由 / [栗林美奈實](../Page/栗林美奈實.md "wikilink") （[School
    Days](../Page/School_Days.md "wikilink") 主題曲）
42. sympathizer / [栗林美奈實](../Page/栗林美奈實.md "wikilink")
    （[黑神](../Page/黑神.md "wikilink") 主題曲）
43. Crest of "Z's" / [JAM Project](../Page/JAM_Project.md "wikilink")
    （[超級機器人大戰Z](../Page/超級機器人大戰Z.md "wikilink") 主題曲）
44. 守護神-The guardian / [JAM Project](../Page/JAM_Project.md "wikilink")
    （[真無敵鐵金剛 衝擊！Z篇](../Page/真無敵鐵金剛_衝擊！Z篇.md "wikilink") TV 主題曲）
45. レスキューファイアー / [JAM Project](../Page/JAM_Project.md "wikilink")
    （[Tomica Hero: Rescue
    Fire](../Page/Tomica_Hero:_Rescue_Fire.md "wikilink") 主題曲）
46. SKILL / [JAM Project](../Page/JAM_Project.md "wikilink")
    （[第二次超級機器人大戰α](../Page/超級機器人大戰.md "wikilink")
    主題曲）
47. RE:BRIDGE～Return to oneself～ （2009年主題曲）
      -
        **-安可曲-**
48. EN-1 OUTRIDE （2006年主題曲）
49. EN-2 RE:BRIDGE～Return to oneself～ （2009年主題曲）
50. 終演後BGM　Yells ～It's a beautiful life～ （2008年主題曲）

<!-- end list -->

  - 8月23日

<!-- end list -->

1.  メドレー /
    [水樹奈奈](../Page/水樹奈奈.md "wikilink")+[平野綾](../Page/平野綾.md "wikilink")
    1.  DISCOTHEQUE （[十字架与吸血鬼 CAPU2](../Page/十字架与吸血鬼.md "wikilink") 片头曲）
    2.  MonStAR
2.  Super Driver / [平野綾](../Page/平野綾.md "wikilink")
    （[凉宫春日的忧郁](../Page/凉宫春日的忧郁.md "wikilink")09年版
    片头曲）
3.  Set me free / [平野綾](../Page/平野綾.md "wikilink")
4.  約束の場所へ / [米倉千尋](../Page/米倉千尋.md "wikilink")
    （[百變之星](../Page/百變之星.md "wikilink") 主題曲）
5.  10 YEARS AFTER / [米倉千尋](../Page/米倉千尋.md "wikilink")
    （[機動戰士GUNDAM第08MS小隊](../Page/機動戰士GUNDAM第08MS小隊.md "wikilink")
    片尾曲）
6.  そして僕は… / [榊原由依](../Page/榊原由依.md "wikilink") （[PRISM
    ARK](../Page/PRISM_ARK.md "wikilink") 片头曲）
7.  恋の炎 / [榊原由依](../Page/榊原由依.md "wikilink")
    （[我的狐仙女友](../Page/我的狐仙女友.md "wikilink")
    片尾曲）
8.  霊喰い / [妖精帝國](../Page/妖精帝國.md "wikilink")
    （[喰霊-零-](../Page/喰霊.md "wikilink") 印象曲）
9.  last Moment / [妖精帝國](../Page/妖精帝國.md "wikilink") （[舞-HiME
    運命の系統樹](../Page/舞-HiME.md "wikilink") 插入曲）
10. 人として軸がぶれている /
    [大槻ケンヂ](../Page/大槻ケンヂ.md "wikilink")+絶望少女達（[野中藍](../Page/野中藍.md "wikilink")、[新谷良子](../Page/新谷良子.md "wikilink")、[小林優](../Page/小林優.md "wikilink")、[澤城美雪](../Page/澤城美雪.md "wikilink")）
    （[絕望先生](../Page/絕望先生.md "wikilink") 片头曲）
11. 空想ルンバ /
    [大槻ケンヂ](../Page/大槻ケンヂ.md "wikilink")+絶望少女達（[野中藍](../Page/野中藍.md "wikilink")、[新谷良子](../Page/新谷良子.md "wikilink")、[小林優](../Page/小林優.md "wikilink")、[澤城美雪](../Page/澤城美雪.md "wikilink")）
    （[俗·绝望先生](../Page/絕望先生.md "wikilink") 片头曲）
12. 林檎もぎれビーム\! /
    [大槻ケンヂ](../Page/大槻ケンヂ.md "wikilink")+絶望少女達（[野中藍](../Page/野中藍.md "wikilink")、[新谷良子](../Page/新谷良子.md "wikilink")、[小林優](../Page/小林優.md "wikilink")、[澤城美雪](../Page/澤城美雪.md "wikilink")）
    （[忏·绝望先生](../Page/絕望先生.md "wikilink") 片头曲）
13. CHA-LA HEAD-CHA-LA / [影山浩宣](../Page/影山浩宣.md "wikilink")
    （[龍珠Z](../Page/龍珠Z.md "wikilink") 片头曲）
14. 聖闘士神話-SOLDIER DREAM- / [影山浩宣](../Page/影山浩宣.md "wikilink")
    （[聖鬥士星矢](../Page/聖鬥士星矢.md "wikilink") 片头曲）
15. Parallel Hearts /
    [FictionJunction](../Page/FictionJunction.md "wikilink")
    （[潘朵拉之心](../Page/潘朵拉之心.md "wikilink") 片头曲）
16. 暁の車 / [FictionJunction](../Page/FictionJunction.md "wikilink")
    （[機動戰士GUNDAM SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")
    插入曲）
17. nowhere / [FictionJunction](../Page/FictionJunction.md "wikilink")
    （[MADLAX](../Page/MADLAX.md "wikilink") 插入曲）
18. Return to Love / [近江知永](../Page/近江知永.md "wikilink")
    （[SoltyRei](../Page/SoltyRei.md "wikilink") 片尾曲）
19. Maze / [savage genius](../Page/savage_genius.md "wikilink")
    feat.[近江知永](../Page/近江知永.md "wikilink")
    （[潘朵拉之心](../Page/潘朵拉之心.md "wikilink") 片尾曲）
20. 私をみつけて。 / [savage genius](../Page/savage_genius.md "wikilink")
    （[潘朵拉之心](../Page/潘朵拉之心.md "wikilink") 片尾曲）
21. Dark Side of the Light / [飛蘭](../Page/飛蘭.md "wikilink")
    （[喰霊-零-](../Page/喰霊.md "wikilink") 插入曲）
22. mind as Judgment / [飛蘭](../Page/飛蘭.md "wikilink")
    （[CANAAN](../Page/CANAAN.md "wikilink") 片头曲）
23. 魂のルフラン /
    [飛蘭](../Page/飛蘭.md "wikilink")+[奥井雅美](../Page/奥井雅美.md "wikilink")
    （[新世紀福音戰士劇場版：死與新生](../Page/新世紀福音戰士劇場版：死與新生.md "wikilink") 主题曲）
24. LOVE SHIELD / [奥井雅美](../Page/奥井雅美.md "wikilink") （[戀愛少女與守護之盾〜The
    shield of AIGIS〜(PS2)](../Page/戀愛少女與守護之盾.md "wikilink") 主题曲）
25. 舞い落ちる雪のように / [Suara](../Page/Suara.md "wikilink") （[WHITE
    ALBUM](../Page/WHITE_ALBUM.md "wikilink") 片尾曲）
26. Free and Dream / [Suara](../Page/Suara.md "wikilink")
    （[花冠之淚](../Page/花冠之淚.md "wikilink") 片头曲）
27. DANZEN\!ふたりはプリキュア （ver.Max Heart） /
    [田村由香里](../Page/田村由香里.md "wikilink")+[新谷良子](../Page/新谷良子.md "wikilink")
    （[光之美少女 Max Heart](../Page/光之美少女_Max_Heart.md "wikilink") 片头曲）
28. LOST IN SPACE / [PSYCHIC LOVER](../Page/PSYCHIC_LOVER.md "wikilink")
    （[泰坦尼亞](../Page/泰坦尼亞.md "wikilink") 片尾曲）
29. THE IDOLM@STER /
    [中村繪里子](../Page/中村繪里子.md "wikilink")・[今井麻美](../Page/今井麻美.md "wikilink")・[仁後真耶子](../Page/仁後真耶子.md "wikilink")
    from THE IDOLM@STER
30. キラメキラリ /
    [中村繪里子](../Page/中村繪里子.md "wikilink")・[今井麻美](../Page/今井麻美.md "wikilink")・[仁後真耶子](../Page/仁後真耶子.md "wikilink")
    from THE IDOLM@STER
31. my song /
    [中村繪里子](../Page/中村繪里子.md "wikilink")・[今井麻美](../Page/今井麻美.md "wikilink")・[仁後真耶子](../Page/仁後真耶子.md "wikilink")
    from THE IDOLM@STER
32. メドレー / [m.o.v.e](../Page/m.o.v.e.md "wikilink")
    1.  DOGFIGHT（[頭文字D](../Page/頭文字D.md "wikilink") Fourth Stage OP）
    2.  Blazin' Beat（[頭文字D](../Page/頭文字D.md "wikilink") Second Stage OP）
33. Gravity / [m.o.v.e](../Page/m.o.v.e.md "wikilink")
    （[幸運☆星](../Page/幸運☆星.md "wikilink") 插入曲）
34. チェルシーガール / [田村由香里](../Page/田村由香里.md "wikilink")
35. Tomorrow / [田村由香里](../Page/田村由香里.md "wikilink")
36. Little Wish ～first step～ / [田村由香里](../Page/田村由香里.md "wikilink")
    （[魔法少女奈葉](../Page/魔法少女奈葉.md "wikilink") 片尾曲）
37. 哀 戦士 / [GACKT](../Page/GACKT.md "wikilink") （机动战士高达 高达VS.高达NEXT 主题曲）
38. The Next Decade / [GACKT](../Page/GACKT.md "wikilink") （[劇場版
    假面騎士Decade 全騎士 VS
    大修卡](../Page/劇場版_假面騎士Decade_全騎士_VS_大修卡.md "wikilink")
    主题曲）
39. REDEMPTION / [GACKT](../Page/GACKT.md "wikilink")
    （[地狱犬的挽歌：最终幻想VII](../Page/地狱犬的挽歌：最终幻想VII.md "wikilink")
    主题曲）
40. Gimmick Game /
    [水樹奈奈](../Page/水樹奈奈.md "wikilink")+motsu（[m.o.v.e](../Page/m.o.v.e.md "wikilink")）
41. 深愛 /
    [水樹奈奈](../Page/水樹奈奈.md "wikilink")+[Suara](../Page/Suara.md "wikilink")
    （[WHITE ALBUM](../Page/WHITE_ALBUM.md "wikilink") 片头曲）
42. 悦楽カメリア / [水樹奈奈](../Page/水樹奈奈.md "wikilink")
43. Orchestral Fantasia / [水樹奈奈](../Page/水樹奈奈.md "wikilink")
44. RE:BRIDGE～Return to oneself～（2009年主題曲）
      -
        **-安可曲-**
45. EN-1 ONENESS （2005年主題曲）
46. EN-2 RE:BRIDGE～Return to oneself～ （2009年主題曲）
47. 終演後BGM　Yells ～It's a beautiful life～ （2008年主題曲）

## 2010年

  - 名稱：**Animelo Summer Live 2010 -evolution-**
  - 舉辦日：2010年8月28日、8月29日
  - 會場：[埼玉超級競技場](../Page/埼玉超級競技場.md "wikilink")

### 演出者

  - 8月28日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [angela](../Page/angela.md "wikilink")
  - [石川智晶](../Page/石川智晶.md "wikilink")
  - [伊藤香奈子](../Page/伊藤香奈子.md "wikilink")
  - [ELISA](../Page/ELISA.md "wikilink")
  - [Girls Dead
    Monster](../Page/Angel_Beats!角色列表#Girls_Dead_Monster.md "wikilink")（[LiSA](../Page/LiSA.md "wikilink")、[marina](../Page/marina.md "wikilink")）
  - [栗林美奈實](../Page/栗林美奈實.md "wikilink")
  - [GRANRODEO](../Page/GRANRODEO.md "wikilink")
  - [THE GOMBAND](../Page/THE_GOMBAND.md "wikilink")
  - [JAM Project](../Page/JAM_Project.md "wikilink")
  - [sphere](../Page/sphere_\(聲優團體\).md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [高橋直純](../Page/高橋直純.md "wikilink")
  - [南里侑香](../Page/南里侑香.md "wikilink")
  - [nomico](../Page/nomico.md "wikilink") +
  - [fripSide](../Page/fripSide.md "wikilink")
  - [米倉千尋](../Page/米倉千尋.md "wikilink")
  - [Lia](../Page/Lia.md "wikilink")

</div>

  - 8月29日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [彩音](../Page/彩音.md "wikilink")

  - [ALI PROJECT](../Page/ALI_PROJECT.md "wikilink")

  - [遠藤正明](../Page/遠藤正明.md "wikilink")

  - [奧井雅美](../Page/奧井雅美.md "wikilink")

  -
  - [PSYCHIC LOVER](../Page/PSYCHIC_LOVER.md "wikilink")

  - [田村由香里](../Page/田村由香里.md "wikilink")

  - [茅原實里](../Page/茅原實里.md "wikilink")

  - [水樹奈奈](../Page/水樹奈奈.md "wikilink")

  - [飛蘭](../Page/飛蘭.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [Milky Holmes](../Page/偵探歌劇_少女福爾摩斯.md "wikilink")
  - [milktub](../Page/milktub.md "wikilink")
  - [May'n](../Page/May'n.md "wikilink")
  - [桃井晴子](../Page/桃井晴子.md "wikilink")

</div>

### 主題曲

  - 「evolution ～for beloved one～」
    作詞：[奧井雅美](../Page/奧井雅美.md "wikilink")，作曲：[栗林美奈實](../Page/栗林美奈實.md "wikilink")，編曲：[飯塚昌明](../Page/飯塚昌明.md "wikilink")

### 演唱歌曲一覽

:\* 以下未有特別注明之作品，發佈媒體均為動畫。

  - 8月28日

<!-- end list -->

1.  『レスキューファイアー』/[JAM
    Project](../Page/JAM_Project.md "wikilink")×[栗林美奈实](../Page/栗林美奈实.md "wikilink")
2.  『only my
    railgun』/[fripSide](../Page/fripSide.md "wikilink")（[科學超電磁砲](../Page/科學超電磁砲.md "wikilink")）主題曲
3.  『LEVEL5-judgelight-』/fripSide（[科學超電磁砲](../Page/科學超電磁砲.md "wikilink")）主題曲2
4.  『オルタナティヴ』/angela（[魔神相剋者主題曲](../Page/魔神相剋者.md "wikilink")）
5.  『Separation』/[angela](../Page/angela.md "wikilink")（[蒼穹之戰神片尾曲](../Page/蒼穹之戰神.md "wikilink")）
6.  『蒼い春』/angela（[妄想學生會片尾曲](../Page/妄想學生會.md "wikilink")）
7.  『遠くまで～infinity～』/[高橋直純](../Page/高橋直純.md "wikilink")
8.  『クローバー』/高橋直純
9.  『月導-Tsukishirube-』/[南里侑香](../Page/南里侑香.md "wikilink")（[神隱之狼片尾曲](../Page/神隱之狼.md "wikilink")）
10. 『雫』/南里侑香（[.hack//Quantum主題曲](../Page/.hack/Quantum.md "wikilink")）
11. 『Dear My
    Friend-まだ見ぬ未来へ-』/[ELISA](../Page/ELISA.md "wikilink")（[科學超電磁砲](../Page/科學超電磁砲.md "wikilink")）片尾曲
12. 『Real Force』/ELISA（[科學超電磁砲](../Page/科學超電磁砲.md "wikilink")）片尾曲2
13. 『逆光』/[石川智晶](../Page/石川智晶.md "wikilink")（[戰國BASARA3片尾曲](../Page/戰國BASARA3.md "wikilink")）
14. 『涙腺』/石川智晶（[戰國BASARA貳插入曲](../Page/戰國BASARA.md "wikilink")）
15. 『Find the
    blue』/[伊藤香奈子](../Page/伊藤香奈子.md "wikilink")＋志倉千代丸（[CHAOS;HEAD主題曲](../Page/CHAOS;HEAD.md "wikilink")）
16. 『スカイクラッドの観測者』/伊藤香奈子＋志倉千代丸（[Steins;Gate主題曲](../Page/Steins;Gate.md "wikilink")）
17. 『Bad Apple\!\!』/[nomico](../Page/nomico.md "wikilink")+
18. 『Braveheart』/[THE
    GOMBAND](../Page/THE_GOMBAND.md "wikilink")（[BLACK★ROCK
    SHOOTER片尾曲](../Page/BLACK★ROCK_SHOOTER.md "wikilink")）
19. 『ブラック★ロックシューター』/THE GOMBAND（[BLACK★ROCK
    SHOOTER插入曲](../Page/BLACK★ROCK_SHOOTER.md "wikilink")）
20. 『[鸟之诗](../Page/鸟之诗.md "wikilink")』/[Lia](../Page/Lia.md "wikilink")（[AIR主題曲](../Page/AIR.md "wikilink")）
21. 『My Soul,Your Beats\!』/Lia（[Angel
    Beats\!主題曲](../Page/Angel_Beats!.md "wikilink")）
22. 『Alchemy【marina】』/[Girls Dead
    Monster](../Page/Girls_Dead_Monster.md "wikilink")([LiSA](../Page/LiSA.md "wikilink"),[marina](../Page/marina.md "wikilink"))（[Angel
    Beats\!插入曲](../Page/Angel_Beats!.md "wikilink")）
23. 『Crow Song【Lisa】』/Girls Dead Monster(LiSA,marina)（[Angel
    Beats\!插入曲](../Page/Angel_Beats!.md "wikilink")）
24. 『Brave Song』/Lia×Girls Dead Monster(LiSA,marina)（[Angel
    Beats\!片尾曲](../Page/Angel_Beats!.md "wikilink")）
25. 『Butterfly
    Kiss』/[米倉千尋](../Page/米倉千尋.md "wikilink")（[聖石小子主題曲](../Page/聖石小子.md "wikilink")）
26. 『WILL』/米倉千尋（[封神演義主題曲](../Page/封神演義.md "wikilink")）
27. 『ゆずれない願い』/米倉千尋×[田村直美](../Page/田村直美.md "wikilink")（[魔法騎士主題曲](../Page/魔法騎士.md "wikilink")）
28. 『Crystal Energy』/栗林美奈实（[舞-乙HiME主題曲](../Page/舞-乙HiME.md "wikilink")2）
29. 『冥夜花伝廊』/栗林美奈实（[刀語片頭曲](../Page/刀語.md "wikilink")）
30. 『あんりある♥パラダイス』/栗林美奈实（[肯普法主題曲](../Page/肯普法.md "wikilink")）
31. 『Now
    loading...SKY\!\!』/[Sphere](../Page/Sphere.md "wikilink")-スフィア-（[玩伴貓耳娘主題曲](../Page/玩伴貓耳娘.md "wikilink")）
32. 『Super Noisy
    Nova』/Sphere-スフィア-（[星空情緣主題曲](../Page/星空情緣.md "wikilink")）
33. 『Future
    Stream』/Sphere-スフィア-（[初戀限定。主題曲](../Page/初戀限定。.md "wikilink")）
34. 『欲望∞』/[GRANRODEO](../Page/GRANRODEO.md "wikilink")（[大和彼氏主題曲](../Page/大和彼氏.md "wikilink")）
35. 『ROSEHIP‐BULLET』/GRANRODEO（[咎狗之血主題曲](../Page/咎狗之血.md "wikilink")）
36. 『Once＆Foever』/GRANRODEO（[Muv-Luv
    Alternative主題曲](../Page/Muv-Luv_Alternative.md "wikilink")）
37. 『MAXIMIZER』/JAM Project
38. 『TRANSFORMERS EVO. 』/JAM
    Project（[變形金剛進化版主題曲](../Page/變形金剛進化版.md "wikilink")）
39. 『HERO』/JAM Project
40. 『GONG～SKILL』JAM Project
      -
        *'-安可曲-*
41. EN-1 『RE:BRIDGE～Return to oneself～』/アニサマ2009主题曲
42. EN-2 『evolution ～for beloved one～』/アニサマ2010主题曲

<!-- end list -->

  - 8月29日

<!-- end list -->

1.  『創聖のアクエリオン』/[茅原实里](../Page/茅原实里.md "wikilink")×[May'n](../Page/May'n.md "wikilink")
2.  『Paradise Lost』/茅原实里（[食靈主題曲](../Page/食靈.md "wikilink")）
3.  『優しい忘却』/茅原实里（[涼宮春日的消失主題曲](../Page/涼宮春日的消失.md "wikilink")）
4.  『Freedom Dreamer』/茅原实里
5.  『バカ・ゴー・ホーム』/[milktub](../Page/milktub.md "wikilink")（[笨蛋，測驗，召喚獸片尾曲](../Page/笨蛋，測驗，召喚獸.md "wikilink")）
6.  『Happy Go\!\!』/milktub
7.  『Get Wild』/[PSYCHIC
    LOVER](../Page/PSYCHIC_LOVER.md "wikilink")×milktub（[城市獵人主題曲](../Page/城市獵人.md "wikilink")）
8.  『超\!最強\!ウォーリアーズ』/PSYCHIC LOVER（[爆丸主題曲](../Page/爆丸.md "wikilink")）
9.  『侍戦隊シンケンジャー』/PSYCHIC
    LOVER（[侍戰隊真劍者主題曲](../Page/侍戰隊真劍者.md "wikilink")）
10. 『Astro Rider』/
11. 『Communication Breakdown』/Crush
    Tears（[爆丸片尾曲](../Page/爆丸.md "wikilink")）
12. 『SERIOUS-AGE』/[飛蘭](../Page/飛蘭.md "wikilink")（劇場版[破刃之劍片尾曲](../Page/破刃之劍.md "wikilink")）
13. 『戦場に咲いた一輪の花』/飛蘭（PS3遊戲[白騎士物語
    光與闇的覺醒主題曲](../Page/白騎士物語_光與闇的覺醒.md "wikilink")）
14. 『Errand』/飛蘭（[聖痕鍊金士片頭曲](../Page/聖痕鍊金士.md "wikilink")）
15. 『雨上がりのミライ』/[Milky Holmes](../Page/偵探歌劇_少女福爾摩斯.md "wikilink") ([偵探歌劇
    少女福爾摩斯PSP片頭曲](../Page/偵探歌劇_少女福爾摩斯.md "wikilink"))
16. 『恋華大乱』/[奥井雅美](../Page/奥井雅美.md "wikilink")（[戀姬†無雙第三季](../Page/戀姬†無雙.md "wikilink")[真・戀姬†無雙〜少女大亂〜片頭曲](../Page/真・戀姬†無雙〜少女大亂〜.md "wikilink")）
17. 『Flower』/奥井雅美
18. 『Arrival of Tears』/[彩音](../Page/彩音.md "wikilink")（[11eyes
    -罪與罰與贖的少女-主題曲](../Page/11eyes_-罪與罰與贖的少女-.md "wikilink")）
19. 『Angelic bright』/彩音
20. 『Northern lights』/彩音×飛蘭(通靈王的主題曲)
21. 『トンドルベイビー』/[桃井晴子](../Page/桃井晴子.md "wikilink")
22. 『21世紀』/桃井晴子
23. 『勝利の女ネ申』/桃井晴子
24. 『BELIEVE IN NEXUS』/[遠藤正明](../Page/遠藤正明.md "wikilink")
25. 『Carry On』/遠藤正明
26. 『薔薇獄乙女』/[ALI
    PROJECT](../Page/ALI_PROJECT.md "wikilink")（[薔薇少女](../Page/薔薇少女.md "wikilink")
    ～序曲～片頭曲）
27. 『刀と鞘』/ALI PROJECT（[刀語片頭曲](../Page/刀語.md "wikilink")2）
28. 『亂世エロイカ』/ALI PROJECT
29. 『ユニバーサル・バニー』/May'n（劇中曲）
30. 『愛は降る星のごとく』/May'n（[最强武将传・三国演義片尾曲](../Page/三国演义_\(动画\).md "wikilink")）
31. 『Ready Go\!』/May'n（[大神與七位夥伴片頭曲](../Page/大神與七位夥伴.md "wikilink")）
32. 『教えてA to
    Z』/[田村由香里](../Page/田村由香里.md "wikilink")（[B型H系片頭曲](../Page/B型H系.md "wikilink")）
33. 『Tiny Rainbow』/田村由香里〈魔法少女奈葉A'S PORTABLE -THE BATTLE OF ACES- ED〉
34. 『fancy baby doll』/田村由香里
35. 『You ＆ Me feat.motsu』/田村由香里 feat. motsu
36. 『Shooting
    Star』/[KOTOKO](../Page/KOTOKO.md "wikilink")（[拜托了老师片头曲](../Page/拜托了老师.md "wikilink")）
37. 『Loop-the-Loop』/KOTOKO（[出包王女 第二季片头曲](../Page/出包王女.md "wikilink")）
38. 『Re-sublimity』/KOTOKO（[神无月的巫女片头曲](../Page/神无月的巫女.md "wikilink")）
39. 『Don't be long.』/[水樹奈奈](../Page/水樹奈奈.md "wikilink")（[魔法少女奈葉 The
    MOVIE 1st劇場版插入曲](../Page/魔法少女奈葉_The_MOVIE_1st.md "wikilink")）
40. 『NEXT ARCADIA』/水樹奈奈
41. 『PHANTOM MINDS』/水樹奈奈（[魔法少女奈葉 The MOVIE
    1st劇場版片頭曲](../Page/魔法少女奈葉_The_MOVIE_1st.md "wikilink")）
42. 『UNCHAIN∞WORLD』/水樹奈奈×奥井雅美（無限邊界:機戰OG傳說 超越NDS版片頭曲）

\#: *'-安可曲-*

1.  EN-1 『Generation-A』/アニサマ2007主题曲
2.  EN-2 『evolution ～for beloved one～』/アニサマ2010主题曲

## 2011年

### Anisama in Shanghai -Only One-

  - 舉辦日：2011年2月19日
  - 會場：[上海大舞台](../Page/上海大舞台.md "wikilink")

#### 演出者

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [ALI PROJECT](../Page/ALI_PROJECT.md "wikilink")

  - [石田耀子](../Page/石田耀子.md "wikilink")

  - [遠藤正明](../Page/遠藤正明.md "wikilink")

  - （[岩田光央](../Page/岩田光央.md "wikilink")・[小野大輔](../Page/小野大輔.md "wikilink")・[鈴村健一](../Page/鈴村健一.md "wikilink")・[森久保祥太郎](../Page/森久保祥太郎.md "wikilink")）

  - [影山浩宣](../Page/影山浩宣.md "wikilink")

  - [北谷洋](../Page/北谷洋.md "wikilink")

  -
  - [栗林美奈实](../Page/栗林美奈实.md "wikilink")

  - [JAM Project](../Page/JAM_Project.md "wikilink")

  - [初音未来](../Page/初音未来.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [飛蘭](../Page/飛蘭.md "wikilink")
  - [福山芳樹](../Page/福山芳樹.md "wikilink")
  - [May'n](../Page/May'n.md "wikilink")

</div>

#### 演唱曲目一覽

1.  VICTORY／[JAM
    Project](../Page/JAM_Project.md "wikilink")（[超級機器人大戰MX](../Page/超級機器人大戰MX.md "wikilink")
    主題曲）
2.  聖闘士星矢～ソルジャー・ドリーム～／[影山浩宣](../Page/影山浩宣.md "wikilink")
3.  Errand／[飛蘭](../Page/飛蘭.md "wikilink")（[聖痕鍊金士片頭曲](../Page/聖痕鍊金士.md "wikilink")）
4.  mind as judgment／飛蘭（[CANAAN](../Page/CANAAN.md "wikilink") 片頭曲）
5.  乙女のポリシー／[石田燿子](../Page/石田燿子.md "wikilink")（[美少女戰士R](../Page/美少女戰士R.md "wikilink")
    主題曲）
6.  STRIKE WITCHES 2～笑顔の魔法～／石田燿子
7.  ヴィーアー！／[北谷洋](../Page/北谷洋.md "wikilink")（[ONE
    PIECE](../Page/ONE_PIECE.md "wikilink") 主題曲）
8.  真赤な誓い／[福山芳樹](../Page/福山芳樹.md "wikilink")（[武裝鍊金](../Page/武裝鍊金.md "wikilink")
    OP）
9.  Make You Free／
10. You got game？／Kimeru
11. みくみくにしてあげる【してやんよ】／[初音未来](../Page/初音未来.md "wikilink")
12. おれパラップ／
13. Stand down／[森久保祥太郎](../Page/森久保祥太郎.md "wikilink")
14. mirror／森久保祥太郎
15. 熱烈ANSWER／[小野大輔](../Page/小野大輔.md "wikilink")
16. だいすき／小野大輔
17. in my space／[鈴村健一](../Page/鈴村健一.md "wikilink")
18. 月とストーブ／鈴村健一
19. セルフ／[岩田光央](../Page/岩田光央.md "wikilink")
20. フルーツマン／岩田光央
21. 眠るものたちへ／
22. わが﨟たし悪の華／[ALI PROJECT](../Page/ALI_PROJECT.md "wikilink")
23. 亡国覚醒カタルシス／ALI PROJECT
24. 聖少女領域／ALI PROJECT（[薔薇少女第](../Page/薔薇少女.md "wikilink")2期 主題曲）
25. Precious
    Memories／[栗林美奈实](../Page/栗林美奈实.md "wikilink")（[你所期望的永遠](../Page/你所期望的永遠.md "wikilink")
    主題曲）
26. Shining☆Days／栗林美奈实（[舞-HiME](../Page/舞-HiME.md "wikilink") 主題曲）
27. 翼はPleasure
    Line／栗林美奈实＆[奥井雅美](../Page/奥井雅美.md "wikilink")（[聖槍修女](../Page/聖槍修女.md "wikilink")
    片頭曲）
28. [残酷天使的行动纲领](../Page/残酷天使的行动纲领.md "wikilink")／石田燿子＆奥井雅美＆栗林美奈实＆飛蘭（[新世紀福音戰士](../Page/新世紀福音戰士.md "wikilink")
    主題曲）
29. 輪舞-revolution／奥井雅美（[少女革命](../Page/少女革命.md "wikilink") 主題曲）
30. 勇者王誕生／[遠藤正明](../Page/遠藤正明.md "wikilink")（[勇者王](../Page/勇者王.md "wikilink")
    主題曲）
31. CHA-LA HEAD-CHA-LA／影山浩宣（[龍珠Z](../Page/龍珠Z.md "wikilink") 片頭曲）
32. 突撃ラブハート／[May'n](../Page/May'n.md "wikilink")＆[福山芳樹](../Page/福山芳樹.md "wikilink")([超時空要塞
    7](../Page/超時空要塞_7.md "wikilink") IN)
33. ダイヤモンドクレバス／May'n（[超時空要塞
    Frontier](../Page/超時空要塞_Frontier.md "wikilink") 片尾曲）
34. ReadyGo！／May'n
35. 射手座☆午後九時Don't be late／May'n（[超時空要塞
    Frontier](../Page/超時空要塞_Frontier.md "wikilink") IN）
36. TRANSFORMERS EVO.／[JAM
    Project](../Page/JAM_Project.md "wikilink")（[變形金剛進化版主題曲](../Page/變形金剛進化版.md "wikilink")）
37. レスキューファイヤー／JAM Project（ 主題曲）
38. Only One（中国語ヴァージョン）／JAM Project
39. GONG～SKILL／JAM Project
      -
        *'-安可曲-*
40. Only One（日本語ヴァージョン）／全員

### Animelo Summer Live 2011 -rainbow-

  - 舉辦日：2011年8月27日、8月28日
  - 會場：[埼玉超级体育馆](../Page/埼玉超级体育馆.md "wikilink")

#### 主題曲

  - rainbow
    作詞、作曲：[志倉千代丸](../Page/志倉千代丸.md "wikilink")，編曲：真下正樹

#### 出演者

  - 8月27日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [偶像大師](../Page/偶像大師.md "wikilink")（[中村繪里子](../Page/中村繪里子.md "wikilink")、[今井麻美](../Page/今井麻美.md "wikilink")、[長谷川明子](../Page/長谷川明子.md "wikilink")、[原由實](../Page/原由實.md "wikilink")）

  - [麻生夏子](../Page/麻生夏子.md "wikilink")

  -
  - [ELISA](../Page/ELISA.md "wikilink")

  - [佐咲紗花](../Page/佐咲紗花.md "wikilink")

  - [JAM Project](../Page/JAM_Project.md "wikilink")

  - [田村由香里](../Page/田村由香里.md "wikilink")

  - [茅原實里](../Page/茅原實里.md "wikilink")

  - [七森中☆娛樂部](../Page/輕鬆百合.md "wikilink")

  - [Hyadin](../Page/前山田健一.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [飛蘭](../Page/飛蘭.md "wikilink")
  - [fripSide](../Page/fripSide.md "wikilink")
  - [BREAKERZ](../Page/BREAKERZ.md "wikilink")
  - [May'n](../Page/May'n.md "wikilink")
  - [桃色幸運草Z](../Page/桃色幸運草Z.md "wikilink")

</div>

  - 8月28日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [石川智晶](../Page/石川智晶.md "wikilink")
  - [伊藤香奈子](../Page/伊藤香奈子.md "wikilink")＋[志倉千代丸](../Page/志倉千代丸.md "wikilink")
  - [艾莉歐をかまってちゃん](../Page/電波女&青春男.md "wikilink")
  - [Kalafina](../Page/Kalafina.md "wikilink")（特別出演）
  - [栗林美奈實](../Page/栗林美奈實.md "wikilink")
  - [黒崎真音](../Page/黒崎真音.md "wikilink")
  - [GRANRODEO](../Page/GRANRODEO.md "wikilink")
  - [JAM Project](../Page/JAM_Project.md "wikilink")
  - [Phantasm](../Page/Phantasm.md "wikilink")（FES
    cv.[榊原由依](../Page/榊原由依.md "wikilink")）
  - [堀江由衣](../Page/堀江由衣.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [水樹奈奈](../Page/水樹奈奈.md "wikilink")
  - [宮野真守](../Page/宮野真守.md "wikilink")
  - [Milky Holmes](../Page/偵探歌劇_少女福爾摩斯.md "wikilink")
  - [RO-KYU-BU\!](../Page/蘿球社！.md "wikilink")

</div>

#### 演唱歌曲一覽

:\* 以下未有特別注明之作品，發佈媒體均為動畫。

  - 8月27日

<!-- end list -->

1.  [混合曲](../Page/混合曲.md "wikilink")：「Freedom Dreamer」 ～ 「惑星のランデブー」 /
    [茅原实里](../Page/茅原实里.md "wikilink")＆[田村由香里](../Page/田村由香里.md "wikilink")
2.  禁断のエリクシア / [May'n](../Page/May'n.md "wikilink")([劇場版 Macross F
    戀離飛翼插曲](../Page/超时空要塞Frontier.md "wikilink"))
3.  もしも君が願うのなら / May'n (PSP 遊戲 戦場女武神3 主題曲)
4.  Scarlet Ballet / May'n([緋彈的亞莉亞主題曲](../Page/緋彈的亞莉亞.md "wikilink"))
5.  侵略ノススメ☆ / ([侵略！花枝娘主題曲](../Page/侵略！花枝娘.md "wikilink"))
6.  ゆりゆららららゆるゆり大事件 /
    [七森中☆娱乐部](../Page/摇曳百合#娱乐社.md "wikilink")([輕鬆百合主題曲](../Page/輕鬆百合.md "wikilink"))
7.  GO MY WAY\!\! / [THE
    IDOLM@STER](../Page/THE_IDOLM@STER.md "wikilink")([偶像大師Xbox360版登場曲](../Page/偶像大師.md "wikilink"))
8.  READY\!\! / THE IDOLM@STER([偶像大師主題曲](../Page/偶像大師.md "wikilink"))
9.  エウレカベイビー / [麻生夏子](../Page/麻生夏子.md "wikilink")(
    [笨蛋，測驗，召喚獸第二季](../Page/笨蛋，測驗，召喚獸.md "wikilink")
    片尾曲)
10. [混合曲](../Page/混合曲.md "wikilink")：「ダイヤモンドスター☆」 ～ 「More-more
    LOVERS\!\!」 ～ 「Perfect-area complete\!」 /
    [麻生夏子](../Page/麻生夏子.md "wikilink")
11. 混合曲：「ヒャダインのカカカタ☆カタオモイ-C」 ～
    「ヒャダインのじょーじょーゆーじょー」([日常片头曲](../Page/日常.md "wikilink")1&2)
    / [Hyadin](../Page/前山田健一.md "wikilink")
    feat.[佐咲紗花](../Page/佐咲紗花.md "wikilink")
12. Zzz /
    [佐咲紗花](../Page/佐咲紗花.md "wikilink")([日常片尾曲](../Page/日常.md "wikilink"))
13. Last vision for last / [飛蘭](../Page/飛蘭.md "wikilink")([百花繚亂 SAMURAI
    GIRLS主題曲](../Page/百花繚亂_SAMURAI_GIRLS.md "wikilink"))
14. 螺旋、或いは聖なる欲望。 / 飛蘭([聖痕鍊金士II主題曲](../Page/聖痕鍊金士.md "wikilink"))
15. God only knows～集積回路の夢旅人 / [ELISA](../Page/ELISA.md "wikilink") (動畫
    [只有神知道的世界第一季](../Page/只有神知道的世界.md "wikilink") 最終回片尾曲)
16. 熱帯夜Girls / [奥井雅美](../Page/奥井雅美.md "wikilink")＆飛蘭＆麻生夏子([アニサマGirls
    Night主題曲](../Page/アニサマGirls_Night.md "wikilink"))
17. CLIMBER×CLIMBER / [BREAKERZ](../Page/BREAKERZ.md "wikilink")
18. Everlasting Luv / BREAKERZ([名偵探柯南主題曲](../Page/名偵探柯南.md "wikilink"))
19. メドレー：「LEVEL5 judgelight」 ～ 「only my railgun」 /
    [fripSide](../Page/fripSide.md "wikilink")
20. Heaven is a Place on Earth /
    [fripSide](../Page/fripSide.md "wikilink")([劇場版 爆笑管家工作日誌 HEAVEN IS A
    PLACE ON
    EARTH主題曲](../Page/劇場版_爆笑管家工作日誌_HEAVEN_IS_A_PLACE_ON_EARTH.md "wikilink"))
21. 宇宙戦艦ヤマト /
    [佐佐木功](../Page/佐佐木功.md "wikilink")([宇宙戰艦大和號主題曲](../Page/宇宙戰艦大和號.md "wikilink"))
22. 銀河鉄道999 / 佐佐木功([银河铁道999主題曲](../Page/银河铁道999.md "wikilink"))
23. マジンガーZ /
    [水木一郎](../Page/水木一郎.md "wikilink")([鐵甲萬能俠主題曲](../Page/鐵甲萬能俠.md "wikilink"))
24. コン・バトラーVのテーマ / 水木一郎([超力電磁俠主題曲](../Page/超力電磁俠.md "wikilink"))
25. Z伝説 ～終わりなき革命～ / [桃色幸运草Z](../Page/桃色幸运草Z.md "wikilink")
26. [混合曲](../Page/混合曲.md "wikilink")：「ミライボウル」 ～ 「ピンキージョーンズ」 / 桃色幸运草Z
27. Butter-Fly /
    [影山浩宣](../Page/影山浩宣.md "wikilink")＆[May'n](../Page/May'n.md "wikilink")　
28. Defection / [茅原实里](../Page/茅原实里.md "wikilink")
29. Planet Patrol / 茅原实里
30. TERMINATED / 茅原实里([境界線上的地平線主題曲](../Page/境界線上的地平線.md "wikilink"))
31. Beautiful Amulet /
    [田村由香里](../Page/田村由香里.md "wikilink")([魔法少女奈葉StrikerS片尾曲](../Page/魔法少女奈葉StrikerS.md "wikilink"))
32. Endless Story / 田村由香里([C3
    -魔幻三次方-主題曲](../Page/C3_-魔幻三次方-.md "wikilink"))
33. LOVE ME NOW\! / 田村由香里
34. MAXON / [JAM
    Project](../Page/JAM_Project.md "wikilink")([超級機械人大戰OG-The
    Inspector-主題曲](../Page/超級機械人大戰OG-The_Inspector-.md "wikilink"))
35. VICTORY / JAM
    Project([超級機械人大戰MX主題曲](../Page/超級機械人大戰MX.md "wikilink"))
36. Rocks / JAM Project
37. [混合曲](../Page/混合曲.md "wikilink")：「GONG」 ～ 「SKILL」 / JAM
    Project＆[水樹奈奈](../Page/水樹奈奈.md "wikilink")
      -
        *'-安可曲-*
38. EN 「rainbow」 / アニサマ2011主题曲

<!-- end list -->

  - 8月28日

<!-- end list -->

1.  [混合曲](../Page/混合曲.md "wikilink")：「ヒカリ」 ～ 「COSMIC LOVE」 /
    [堀江由衣](../Page/堀江由衣.md "wikilink")＆[水樹奈奈](../Page/水樹奈奈.md "wikilink")
2.  Rumbling hearts / [栗林美奈实](../Page/栗林美奈实.md "wikilink")
3.  時すでに始まりを刻む / 栗林美奈实
4.  STRAIGHT JET / 栗林美奈实([IS〈Infinite
    Stratos〉主題曲](../Page/IS〈Infinite_Stratos〉.md "wikilink")）
5.  翼はPleasure Line / 栗林美奈实＆[黒崎真音](../Page/黒崎真音.md "wikilink")
    ([聖槍修女主題曲](../Page/聖槍修女.md "wikilink"))
6.  Magic∞world / 黒崎真音
7.  メモリーズ・ラスト / 黒崎真音
8.  オルフェ / [宮野真守](../Page/宮野真守.md "wikilink")
9.  BODY ROCK / 宮野真守
10. SHOOT\! / [RO-KYU-BU\!](../Page/萝球社！.md "wikilink")
11. Party Love～おっきくなりたい～ / RO-KYU-BU\!
12. 正解はひとつ\!じゃない\!\! / [Milky
    Holmes](../Page/侦探歌剧_少女福尔摩斯#Milky_Holmes.md "wikilink")
13. 雨上がりのミライ / Milky Holmes
14. Hacking to the Gate /
    [伊藤香奈子](../Page/伊藤香奈子.md "wikilink")＋[志倉千代丸](../Page/志倉千代丸.md "wikilink")
15. 刻司ル十二ノ盟約 / [Phantasm](../Page/Phantasm.md "wikilink")(FES
    cv.[榊原由依](../Page/榊原由依.md "wikilink"))
16. 裏切りの夕焼け /
    [谷山紀章](../Page/谷山紀章.md "wikilink")＆[宮野真守](../Page/宮野真守.md "wikilink")
17. インモラリスト / [堀江由衣](../Page/堀江由衣.md "wikilink")
18. [混合曲](../Page/混合曲.md "wikilink")：「PRESENTER」 ～ 「YAHHO\!\!」 / 堀江由衣
19. もう何も怖くない、怖くはない / [石川智晶](../Page/石川智晶.md "wikilink")
20. 不完全燃焼 / 石川智晶 (神樣DOLLS 片头曲)
21. NOAH / [JAM Project](../Page/JAM_Project.md "wikilink")
22. Vanguard / JAM Project([卡片战斗先导者一期](../Page/卡片战斗先导者.md "wikilink")
    片头曲1)
23. レスキューファイアー / JAM Project
24. Os-宇宙人 / [Os-外星人](../Page/Os-外星人.md "wikilink")
25. コタツから眺める世界地図 / Os-外星人
26. SUPERNOVA / [GRANRODEO](../Page/GRANRODEO.md "wikilink")
27. アウトサイダー / GRANRODEO
28. Go For It\! / GRANRODEO
29. Magia / [Kalafina](../Page/Kalafina.md "wikilink")
30. sprinter / Kalafina
31. I'll believe / [ALTIMA](../Page/ALTIMA.md "wikilink")
32. FLAGS / [T.M.Revolution](../Page/T.M.Revolution.md "wikilink")
33. SWORD SUMMIT / T.M.Revolution
34. ignited-イグナイテッド- / T.M.Revolution
35. ETERNAL BLAZE /
    [水樹奈奈](../Page/水樹奈奈.md "wikilink")＆[遠藤正明](../Page/遠藤正明.md "wikilink")
36. UNBREAKABLE / 水樹奈奈
37. 純潔パラドックス / 水樹奈奈
38. SCARLET KNIGHT / 水樹奈奈
      -
        *'-安可曲-*
39. EN 「rainbow」 / アニサマ2011主题曲

## 2012年

### SUPER GAMESONG LIVE 2012 -NEW GAME-

  - 标题：**SUPER GAMESONG LIVE 2012 -NEW GAME-**
  - 开始日期：2012年5月26日
  - 会場：[横濱國際平和會議場](../Page/横濱國際平和會議場.md "wikilink")
  - 主催：[MAGES.](../Page/MAGES..md "wikilink")、[文化放送](../Page/文化放送.md "wikilink")

#### 出演者

  - [AiRI](../Page/AiRI.md "wikilink")
  - [Afilia Saga](../Page/Afilia_Saga.md "wikilink")
  - [彩音](../Page/彩音.md "wikilink")
  - [伊藤香奈子](../Page/伊藤香奈子.md "wikilink")
  - [今井麻美](../Page/今井麻美.md "wikilink")
  - [CooRie](../Page/CooRie.md "wikilink")
  - [栗林美奈实](../Page/栗林美奈实.md "wikilink")
  - [榊原由依](../Page/榊原由依.md "wikilink")
  - [佐藤裕美](../Page/佐藤裕美.md "wikilink")
  - [Ceui](../Page/Ceui.md "wikilink")
  - [NanosizeMir](../Page/NanosizeMir.md "wikilink")
  - [飛蘭](../Page/飛蘭.md "wikilink")
  - [yozuca\*](../Page/yozuca*.md "wikilink")
  - [Rita](../Page/Rita.md "wikilink")

### Animelo Summer Live 2012 -infinity∞-

  - 舉辦日期：2012年8月25日、8月26日
  - 会場、協力：[埼玉超级竞技场](../Page/埼玉超级竞技场.md "wikilink")
  - 主辦：MAGES.、[文化放送](../Page/文化放送.md "wikilink")
  - 協力賛助：鐵人化計劃、[Good Smile
    Company](../Page/Good_Smile_Company.md "wikilink")、ブシロード
  - 後援：[King Records](../Page/King_Records.md "wikilink")、Geneon
    Universal Entertainment Japan、[Flying
    DOG](../Page/Flying_DOG.md "wikilink")、[Lantis](../Page/Lantis.md "wikilink")
  - 企画：AniSummer Project實行委員会

#### 主題曲

  - （INFINITY～1000年的夢～）
    主唱：AKINO from
    bless4、川田麻美、KISHOW（GRANRODEO）、喜多村英梨、栗林美奈實、田村由香里、茅原實里、May'n
    作詞：[影山浩宣](../Page/影山浩宣.md "wikilink")、[奥井雅美](../Page/奥井雅美.md "wikilink")；作曲：[織田哲郎](../Page/織田哲郎.md "wikilink")

#### 出演者

  - 8月25日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [AKINO with bless4](../Page/AKINO.md "wikilink")
  - [川田真美](../Page/川田真美.md "wikilink")
  - [田村由香里](../Page/田村由香里.md "wikilink")
  - [May'n](../Page/May'n.md "wikilink")
  - [ALI PROJECT](../Page/ALI_PROJECT.md "wikilink")
  - [野水伊織](../Page/野水伊織.md "wikilink")
  - [唯夏織](../Page/唯夏織.md "wikilink")
  - [StylipS](../Page/StylipS.md "wikilink")
  - [PERSONA4 MUSIC BAND](../Page/女神異聞錄4.md "wikilink")
  - [南里侑香](../Page/南里侑香.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [ALTIMA](../Page/ALTIMA.md "wikilink")

  - [Afilia Saga](../Page/Afilia_Saga.md "wikilink")

  - [伊藤香奈子](../Page/伊藤香奈子.md "wikilink")＋[志倉千代丸](../Page/志倉千代丸.md "wikilink")（科學歷險隊）

  -
  - [三澤紗千香](../Page/三澤紗千香.md "wikilink")

  - [铃木木乃美](../Page/铃木木乃美.md "wikilink")

  - [森口博子](../Page/森口博子.md "wikilink")（特別嘉賓）

<!-- end list -->

  - 以下特別演出並不收錄於日後之影碟中。

<!-- end list -->

  - [蓝井艾露](../Page/蓝井艾露.md "wikilink")
  - [LiSA](../Page/LiSA.md "wikilink")
  - [春奈露娜](../Page/春奈露娜.md "wikilink")

</div>

  - 8月26日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [喜多村英梨](../Page/喜多村英梨.md "wikilink")
  - [GRANRODEO](../Page/GRANRODEO.md "wikilink")
  - [栗林美奈實](../Page/栗林美奈實.md "wikilink")
  - [茅原實里](../Page/茅原實里.md "wikilink")
  - [小野大輔](../Page/小野大輔.md "wikilink")
  - [Milky Holmes](../Page/偵探歌劇_少女福爾摩斯.md "wikilink")
  - [七森中☆娛樂部](../Page/輕鬆百合.md "wikilink")
  - [从后匍匐而来队G](../Page/襲來！美少女邪神.md "wikilink")
  - [堀江由衣](../Page/堀江由衣.md "wikilink")
  - [小松未可子](../Page/小松未可子.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [Ray](../Page/Ray_\(歌手\).md "wikilink")
  - [ST☆RISH](../Page/歌之王子殿下.md "wikilink")
  - [宮野真守](../Page/宮野真守.md "wikilink")
  - [鈴村健一](../Page/鈴村健一.md "wikilink")
  - [石川智晶](../Page/石川智晶.md "wikilink")
  - [黑崎真音](../Page/黑崎真音.md "wikilink")
  - [μ's](../Page/μ's.md "wikilink")
  - [初音未來](../Page/初音未來.md "wikilink")

</div>

#### 演唱歌曲一覽

:\* 以下未有特別注明之作品，發佈媒體均為動畫。

  - 8月25日

<!-- end list -->

1.  [混合曲](../Page/混合曲.md "wikilink")： A to Z＋Ready
    Go\!（《[B型H系](../Page/B型H系.md "wikilink")》片頭曲＋《[野狼大神與七位夥伴](../Page/野狼大神.md "wikilink")》片頭曲）／[May'n](../Page/May'n.md "wikilink")×[田村由香里](../Page/田村由香里.md "wikilink")

2.  [JOINT](../Page/JOINT.md "wikilink")（《[灼眼的夏娜Ⅱ](../Page/灼眼的夏娜.md "wikilink")》片頭曲）／[川田真美](../Page/川田真美.md "wikilink")

3.  Boderland（《[軍火女王](../Page/軍火女王.md "wikilink")》片頭曲）／川田真美

4.  [No
    buts\!](../Page/No_buts!.md "wikilink")（《[魔法禁書目錄Ⅱ](../Page/魔法禁書目錄.md "wikilink")》片頭曲1）／川田真美

5.  （遊戲版《[命運石之門](../Page/命運石之門.md "wikilink")》片頭曲）／[伊藤香奈子](../Page/伊藤香奈子.md "wikilink")＋[志倉千代丸](../Page/志倉千代丸.md "wikilink")

6.  Hacking to the
    Gate（動畫版《[命運石之門](../Page/命運石之門.md "wikilink")》片頭曲）／伊藤香奈子＋志倉千代丸

7.  （遊戲版《[ROBOTICS;NOTES](../Page/ROBOTICS;NOTES.md "wikilink")》片頭曲）／Zwei

8.  La\*La\*La（遊戲《[命運石之門
    比翼戀理的摯愛](../Page/命運石之門.md "wikilink")》片頭曲）／[Afilia
    Saga](../Page/Afilia_Saga.md "wikilink")

9.  [混合曲](../Page/混合曲.md "wikilink")：PUPPY LOVE＋[Our Steady
    Boy](../Page/Our_Steady_Boy.md "wikilink")
    （NICONICO直播《電波研究社》》2011年9月片尾曲、《[Kiss×sis](../Page/Kiss×sis.md "wikilink")》片尾曲）／[唯夏織](../Page/唯夏織.md "wikilink")

10. 混合曲：Choose
    me♡＋[STUDY×STUDY](../Page/STUDY×STUDY.md "wikilink")＋[MIRACLE
    RUSH](../Page/MIRACLE_RUSH.md "wikilink")
    （《[其中1個是妹妹！](../Page/其中1個是妹妹！.md "wikilink")》片頭曲、《[惡魔高校D×D](../Page/惡魔高校D×D.md "wikilink")》片尾曲、《[咲-Saki-
    阿知賀篇 episode of
    side-A](../Page/咲-Saki-.md "wikilink")》片頭曲）／[StylipS](../Page/StylipS.md "wikilink")

11. Tonight（《[這樣算是殭屍嗎？](../Page/這樣算是殭屍嗎？.md "wikilink")》片頭曲）／[野水伊織](../Page/野水伊織.md "wikilink")

12. \*\*\*（《[這樣算是殭屍嗎？ OF THE
    DEAD](../Page/這樣算是殭屍嗎？.md "wikilink")》片頭曲）／野水伊織

13. \-kiseki-（《[Sacred
    Seven](../Page/Sacred_Seven.md "wikilink")》主題曲）／[南里侑香](../Page/南里侑香.md "wikilink")

14. LIVE ON\!（遊戲《ARK FRONTIER -時空漂流-》主題曲）／南里侑香

15. （《[加速世界](../Page/加速世界.md "wikilink")》片尾曲2）／[三澤紗千香](../Page/三澤紗千香.md "wikilink")

16. MEMORIA（《[Fate/Zero](../Page/Fate/Zero.md "wikilink")》第一季片尾曲）／[蓝井艾露](../Page/蓝井艾露.md "wikilink")

17. （《[Fate/Zero](../Page/Fate/Zero.md "wikilink")》第二季片尾曲）／[春奈露娜](../Page/春奈露娜.md "wikilink")

18. key plus
    words（《[女神異聞錄4](../Page/女神異聞錄4.md "wikilink")》後期主要片頭曲）／[PERSONA4
    MUSIC BAND](../Page/女神異聞錄4.md "wikilink")

19. Beauty of Destiny（《女神異聞錄4》前期主要片尾曲）／PERSONA4 MUSIC BAND

20. CHOIR
    JAIL（《[黃昏少女×遺失記憶](../Page/黃昏少女×遺失記憶.md "wikilink")》片頭曲）／[铃木木乃美](../Page/铃木木乃美.md "wikilink")

21. （《[冒險少女娜汀亞](../Page/冒險少女娜汀亞.md "wikilink")》片頭曲）／鈴木木乃美＋[三澤紗千香](../Page/三澤紗千香.md "wikilink")＋[野水伊織](../Page/野水伊織.md "wikilink")

22. （《[機動戰士Z
    GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")》片頭曲2）／[森口博子](../Page/森口博子.md "wikilink")

23. （《機動戰士Z GUNDAM》插曲）／森口博子

24. ETERNAL WIND（《[機動戰士GUNDAM
    F91](../Page/機動戰士GUNDAM_F91.md "wikilink")》主題曲） ／森口博子

25. oath
    sign（《[Fate/Zero](../Page/Fate/Zero.md "wikilink")》第一季片頭曲）／[LiSA](../Page/LiSA.md "wikilink")

26. [crossing
    field](../Page/crossing_field.md "wikilink")（《[刀劍神域](../Page/刀劍神域.md "wikilink")》片頭曲1）／LiSA

27. Burst The
    Gravity（《[加速世界](../Page/加速世界.md "wikilink")》片頭曲2）／[ALTIMA](../Page/ALTIMA.md "wikilink")

28. I'll believe（《[灼眼的夏娜Ⅲ](../Page/灼眼的夏娜.md "wikilink")》片尾曲1）／ALTIMA

29. ONE（《灼眼的夏娜Ⅲ》片尾曲2）／ALTIMA

30. （《[Another](../Page/Another.md "wikilink")》片頭曲）／[ALI
    PROJECT](../Page/ALI_PROJECT.md "wikilink")

31. （《AVENGER》片頭曲）／ALI PROJECT

32. （《[CLAMP學園偵探團](../Page/CLAMP學園偵探團.md "wikilink")》片頭曲）／ALI PROJECT

33. （《[創聖大天使EVOL](../Page/創聖大天使EVOL.md "wikilink")》片頭曲2）／[AKINO with
    bless4](../Page/AKINO.md "wikilink")

34. （《創聖大天使EVOL》片頭曲1）／AKINO with bless4

35. （《[創聖大天使](../Page/創聖大天使.md "wikilink")》片頭曲1）／AKINO with
    bless4＋[伊藤香奈子](../Page/伊藤香奈子.md "wikilink")

36. Brain
    Diver（《[天才黃金腦～神之謎](../Page/天才黃金腦～神之謎.md "wikilink")》第一季片頭曲）／[May'n](../Page/May'n.md "wikilink")

37. Mr. Super Future Star（遊戲《E.X.TROOPERS》主題曲）／May'n

38. （《[超時空要塞Frontier](../Page/超時空要塞Frontier.md "wikilink")》片尾曲）／May'n

39. Chase the world（《[加速世界](../Page/加速世界.md "wikilink")》片頭曲1）／May'n

40. （《[魔法少女奈葉 The MOVIE 2nd
    A's](../Page/魔法少女奈葉_The_MOVIE_2nd_A's.md "wikilink")》片尾曲）／[田村由香里](../Page/田村由香里.md "wikilink")

41. （遊戲《[魔法少女奈葉A's PORTABLE -THE GEARS OF
    DESTINY-](../Page/魔法少女奈葉A's.md "wikilink")》片尾曲）／田村由香里

42. Endless Story（《[C³
    -魔幻三次方-](../Page/C³_-魔幻三次方-.md "wikilink")》片頭曲1）／田村由香里

43. You & me（TBS節目《》片頭曲）／田村由香里＋motsu

<!-- end list -->

  - 8月26日

<!-- end list -->

1.  LOVE1000％（《[歌之王子殿下](../Page/歌之王子殿下.md "wikilink")》片尾曲）／[ST☆RISH](../Page/歌之王子殿下.md "wikilink")

2.  （《歌之王子殿下》插曲）／ST☆RISH

3.  YAHHO\!\!（《[加奈日記](../Page/加奈日記.md "wikilink")》片尾曲）／[堀江由衣](../Page/堀江由衣.md "wikilink")

4.  （《[DOG DAYS'](../Page/DOG_DAYS.md "wikilink")》片尾曲）／堀江由衣

5.  CHILDSH♡LOVE♡WORLD／堀江由衣

6.  Happiness\!\!（《[偵探歌劇 少女福爾摩斯
    第二幕](../Page/偵探歌劇_少女福爾摩斯.md "wikilink")》片頭曲）／[Milky
    Holmes](../Page/偵探歌劇_少女福爾摩斯.md "wikilink")

7.  （《[偵探歌劇 少女福爾摩斯](../Page/偵探歌劇_少女福爾摩斯.md "wikilink")》片頭曲）／Milky Holmes

8.  （《[輕鬆百合](../Page/輕鬆百合.md "wikilink")》片尾曲）／[七森中☆娛樂部](../Page/輕鬆百合.md "wikilink")

9.  （《[輕鬆百合♪♪](../Page/輕鬆百合.md "wikilink")》片頭曲）／七森中☆娛樂部

10. [混合曲](../Page/混合曲.md "wikilink")：＋（《[輕鬆百合](../Page/輕鬆百合.md "wikilink")》片頭曲＋遊戲《[偵探歌劇
    少女福爾摩斯](../Page/偵探歌劇_少女福爾摩斯.md "wikilink")》片頭曲）／Milky Holmes＋七森中☆娛樂部

11. Happy
    Girl（《[要聽爸爸的話！](../Page/要聽爸爸的話！.md "wikilink")》片頭曲）／[喜多村英梨](../Page/喜多村英梨.md "wikilink")

12. re;story／[喜多村英梨](../Page/喜多村英梨.md "wikilink")

13. DELIGHT／[小野大輔](../Page/小野大輔.md "wikilink")

14. ANSWER（《[Battle Spirits
    Brave](../Page/Battle_Spirits_Brave.md "wikilink")》片尾曲）／[小野大輔](../Page/小野大輔.md "wikilink")

15. （《[神的記事本](../Page/神的記事本.md "wikilink")》片尾曲）／[鈴村健一](../Page/鈴村健一.md "wikilink")

16. messenger／[鈴村健一](../Page/鈴村健一.md "wikilink")

17. （《[地球防衛少年](../Page/地球防衛少年.md "wikilink")》片頭曲）／[石川智晶](../Page/石川智晶.md "wikilink")&[茅原實里](../Page/茅原實里.md "wikilink")

18. signs～～（《[Muv-Luv Alternative Total
    Eclipse](../Page/Muv-Luv_Alternative.md "wikilink")》片尾曲）／[栗林美奈實](../Page/栗林美奈實.md "wikilink")

19. （《[機動戰士鋼彈AGE](../Page/機動戰士鋼彈AGE.md "wikilink")》片尾曲1）／栗林美奈實

20. HAPPY CRAZY
    BOX（《[最強學生會長](../Page/最強學生會長.md "wikilink")》第一季片頭曲）／栗林美奈實

21. Sign（《[在盛夏等待](../Page/在盛夏等待.md "wikilink")》片頭曲）／[Ray](../Page/Ray_\(歌手\).md "wikilink")

22. Black
    Holy（《[迷你裙宇宙海賊](../Page/迷你裙宇宙海賊.md "wikilink")》插曲）／[小松未可子](../Page/小松未可子.md "wikilink")

23. DAN DAN （《[七龙珠
    最强之道](../Page/七龙珠_最强之道.md "wikilink")》片頭曲）／[織田哲郎](../Page/織田哲郎.md "wikilink")＋[黑崎真音](../Page/黑崎真音.md "wikilink")＋Ray＋小松未可子

24. （《[裝甲騎兵](../Page/裝甲騎兵.md "wikilink")》片頭曲）／[TETSU](../Page/織田哲郎.md "wikilink")

25. （《[灌籃高手](../Page/灌籃高手.md "wikilink")》片尾曲2）／[織田哲郎](../Page/織田哲郎.md "wikilink")＋上杉昇

26. ／[初音未來](../Page/初音未來.md "wikilink")

27. Tell Your World／初音未來

28. （《[襲來！美少女邪神](../Page/襲來！美少女邪神.md "wikilink")》片頭曲）／[从后匍匐而来队G](../Page/襲來！美少女邪神.md "wikilink")

29. 1,2,Jump\!／μ's from [Love Live\!](../Page/Love_Live!.md "wikilink")

30. YOU GET TO
    BURNING（《[機動戰艦](../Page/機動戰艦.md "wikilink")》片頭曲）／[栗林美奈實](../Page/栗林美奈實.md "wikilink")＆[喜多村英梨](../Page/喜多村英梨.md "wikilink")

31. \-reimei-（《[薄櫻鬼
    黎明錄](../Page/薄櫻鬼_～新選組奇譚～.md "wikilink")》片頭曲）／[黑崎真音](../Page/黑崎真音.md "wikilink")

32. Magic ∞ World（《[魔法禁書目錄Ⅱ](../Page/魔法禁書目錄.md "wikilink")》片尾曲1）／黑崎真音

33. Dream
    Fighter（《[奥特曼列傳](../Page/奥特曼列傳.md "wikilink")》片頭曲）／[宮野真守](../Page/宮野真守.md "wikilink")

34. （《[歌之王子殿下](../Page/歌之王子殿下.md "wikilink")》片頭曲）／宮野真守

35. The Giving
    Tree（遊戲《[幻想水滸傳](../Page/幻想水滸傳.md "wikilink")》主題曲）／[石川智晶](../Page/石川智晶.md "wikilink")

36. （《[機動戰士GUNDAM
    SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")》片尾曲1）／石川智晶

37. Can
    Do（《[黑子的籃球](../Page/黑子的籃球.md "wikilink")》片頭曲1）／[GRANRODEO](../Page/GRANRODEO.md "wikilink")

38. RIMFIRE（《黑子的籃球》片頭曲2）／GRANRODEO

39. ／GRANRODEO

40. ZONE//ALONE（《[境界線上的地平線Ⅱ](../Page/境界線上的地平線.md "wikilink")》片頭曲）／[茅原實里](../Page/茅原實里.md "wikilink")

41. TERMINATED（《[境界線上的地平線](../Page/境界線上的地平線.md "wikilink")》片頭曲）／茅原實里

42. SELF
    PRODUCER（《[就算是哥哥，有愛就沒問題了，對吧](../Page/就算是哥哥，有愛就沒問題了，對吧.md "wikilink")》片頭曲）／茅原實里

43. Paradise Lost（《[食靈-零-](../Page/食靈.md "wikilink")》片頭曲）／茅原實里

#### 其他

  - 2012年3月23日发表记者见面会。会议主持是鷲崎健和[石田絵里奈](../Page/石田絵里奈.md "wikilink")（文化放送アナウンサー）。

### Anisama in Shanghai 2012 \~ Next Stage \~

因中日關係事宜而宣佈取消。

  - 原定日期：2012年10月27日
  - 原定会場：上海奔驰文化中心

#### 原定出演者

  - [JAM
    Project](../Page/JAM_Project.md "wikilink")([影山浩宣](../Page/影山浩宣.md "wikilink")、[遠藤正明](../Page/遠藤正明.md "wikilink")、[北谷洋](../Page/北谷洋.md "wikilink")、[奥井雅美](../Page/奥井雅美.md "wikilink")、[福山芳樹](../Page/福山芳樹.md "wikilink"))
  - [水樹奈奈](../Page/水樹奈奈.md "wikilink")
  - [√5](../Page/√5.md "wikilink")([蛇足](../Page/蛇足.md "wikilink")、[ぽこた](../Page/ぽこた.md "wikilink")、[みーちゃん](../Page/みーちゃん.md "wikilink")、[けったろ](../Page/けったろ.md "wikilink")、[koma'n](../Page/koma'n.md "wikilink"))
  - [岩田光央](../Page/岩田光央.md "wikilink")
  - [CONNECT](../Page/CONNECT.md "wikilink")([岩田光央](../Page/岩田光央.md "wikilink")、[鈴村健一](../Page/鈴村健一.md "wikilink"))
  - [鈴村健一](../Page/鈴村健一.md "wikilink")
  - [田村由香里](../Page/田村由香里.md "wikilink")
  - [茅原实里](../Page/茅原实里.md "wikilink")

## 2013年

### Animelo Summer Live 2013 -FLAG NINE-

本年度為大會首度將活動擴展至三天舉行。

  - 舉辦日期：2013年8月23日－8月25日
  - 會場：[埼玉超級體育館](../Page/埼玉超級體育館.md "wikilink")
  - 主辦：MAGES.、[文化放送](../Page/文化放送.md "wikilink")
  - 協力贊助：鐵人化計劃、[Good Smile
    Company](../Page/Good_Smile_Company.md "wikilink")、ブシロード
  - 後援：[Avex
    Entertainment](../Page/Avex_Entertainment.md "wikilink")、[King
    Records](../Page/King_Records.md "wikilink")、Geneon Universal
    Entertainment Japan、[Flying
    DOG](../Page/Flying_DOG.md "wikilink")、[Lantis](../Page/Lantis.md "wikilink")、Warner
    Home Video
  - 企劃：AniSummer Project實行委員會

#### 主題曲

  - The Galaxy Express 999
    作詞：奈良橋陽子、山川啓介；作曲：武川行秀；編曲：佐久間正英

#### 出演者

  - 8月23日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [ALI PROJECT](../Page/ALI_PROJECT.md "wikilink")
  - [ZAQ](../Page/ZAQ.md "wikilink")
  - [茅原實里](../Page/茅原實里.md "wikilink")
  - [ChouCho](../Page/ChouCho.md "wikilink")
  - [nano.RIPE](../Page/nano.RIPE.md "wikilink")
  - [May'n](../Page/May'n.md "wikilink")
  - [串田晃](../Page/串田晃.md "wikilink")
  - [桃色幸運草Z](../Page/桃色幸運草Z.md "wikilink")
  - [Ray](../Page/Ray_\(歌手\).md "wikilink")
  - [三澤紗千香](../Page/三澤紗千香.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [μ′s](../Page/LoveLive!.md "wikilink")
  - [THE IDOLM@STER CINDERELLA
    GIRLS](../Page/THE_IDOLM@STER_CINDERELLA_GIRLS.md "wikilink")
  - [藤田麻衣子](../Page/藤田麻衣子.md "wikilink")
  - [铃木木乃美](../Page/铃木木乃美.md "wikilink")
  - [Milky Holmes](../Page/偵探歌劇_少女福爾摩斯.md "wikilink")
  - [earthmind](../Page/earthmind.md "wikilink")
  - [FLOW](../Page/FLOW.md "wikilink")
  - [中川翔子](../Page/中川翔子.md "wikilink")
  - [NoB](../Page/NoB.md "wikilink")（特別嘉賓）

</div>

  - 8月24日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [angela](../Page/angela.md "wikilink")

  - [上坂菫](../Page/上坂菫.md "wikilink")

  - [栗林美奈實](../Page/栗林美奈實.md "wikilink")

  - [GRANRODEO](../Page/GRANRODEO.md "wikilink")

  - [T-Pistonz+KMC](../Page/T-Pistonz+KMC.md "wikilink")

  - [中島愛](../Page/中島愛.md "wikilink")

  - [OLDCODEX](../Page/OLDCODEX.md "wikilink")

  -
  - [fripSide](../Page/fripSide.md "wikilink")

  - [PSYCHIC LOVER](../Page/PSYCHIC_LOVER.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [七森中☆娛樂部](../Page/輕鬆百合.md "wikilink")

  - [THE IDOLM@STER MILLION STARS](../Page/PROJECT_IM@S.md "wikilink")

  - [Afilia Saga](../Page/Afilia_Saga.md "wikilink")

  - [藍井艾露](../Page/藍井艾露.md "wikilink")

  - [春奈露娜](../Page/春奈露娜.md "wikilink")

  - [Aiu♥Love](../Page/Aiura.md "wikilink")

  -
  - [LiSA](../Page/LiSA.md "wikilink")

  -

</div>

  - 8月25日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [黑崎真音](../Page/黑崎真音.md "wikilink")
  - [田村由香里](../Page/田村由香里.md "wikilink")
  - [富永TOMMY弘明](../Page/富永TOMMY弘明.md "wikilink")
  - [水樹奈奈](../Page/水樹奈奈.md "wikilink")
  - [宮野真守](../Page/宮野真守.md "wikilink")
  - [小倉唯](../Page/小倉唯.md "wikilink")
  - [sphere](../Page/sphere_\(聲優團體\).md "wikilink")
  - [唯夏織](../Page/唯夏織.md "wikilink")
  - [小松未可子](../Page/小松未可子.md "wikilink")
  - [鈴村健一](../Page/鈴村健一.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [ST☆RISH](../Page/歌之王子殿下.md "wikilink")(寺島拓篤+鈴村健一+谷山紀章+諏訪部順一+宮野真守+下野紘+鳥海浩輔)
  - [伊藤香奈子](../Page/伊藤香奈子.md "wikilink")
  - [野水伊織](../Page/野水伊織.md "wikilink")
  - [從後匍匐而來隊G](../Page/襲來！美少女邪神.md "wikilink")
  - [喜多村英梨](../Page/喜多村英梨.md "wikilink")
  - [竹達彩奈](../Page/竹達彩奈.md "wikilink")
  - [日笠陽子](../Page/日笠陽子.md "wikilink")
  - [petit milady](../Page/petit_milady.md "wikilink")
  - [i☆Ris](../Page/i☆Ris.md "wikilink")
  - [南里侑香](../Page/南里侑香.md "wikilink")

</div>

#### 演唱歌曲一覽

:\* 以下未有特別注明之作品，發佈媒體均為動畫。

:; 8月23日

:\# Celestial Diva（遊戲《Chaos Rings
Ⅱ》主題曲）／[茅原實里](../Page/茅原實里.md "wikilink")＆[寶野亞莉華](../Page/寶野亞莉華.md "wikilink")

:\#
（《[美少女戰士](../Page/美少女戰士_\(第一輯\).md "wikilink")》片頭曲）／[桃色幸運草Z](../Page/桃色幸運草Z.md "wikilink")

:\# （劇集《[惡夢小姐](../Page/惡夢小姐.md "wikilink")》主題曲）／桃色幸運草Z

:\# （《[猛烈宇宙海賊](../Page/迷你裙宇宙海賊.md "wikilink")》片頭曲）／桃色幸運草Z

:\# ／[THE IDOLM@STER CINDERELLA
GIRLS](../Page/THE_IDOLM@STER_CINDERELLA_GIRLS.md "wikilink")

:\# ／THE IDOLM@STER CINDERELLA GIRLS

:\#
Recall（《[失憶症](../Page/失憶症_\(遊戲\).md "wikilink")》片尾曲）／[Ray](../Page/Ray_\(歌手\).md "wikilink")

:\# （《[出包王女DARKNESS](../Page/出包王女.md "wikilink")》片頭曲）／Ray

:\#
DreamRiser（《[少女與戰車](../Page/少女與戰車.md "wikilink")》片頭曲）／[ChouCho](../Page/ChouCho.md "wikilink")

:\#
telepath～～（《[劇場版魔法禁書目錄：恩底彌翁的奇蹟](../Page/劇場版魔法禁書目錄：恩底彌翁的奇蹟.md "wikilink")》插曲）／[三澤紗千香](../Page/三澤紗千香.md "wikilink")

:\# （《[科學超電磁砲S](../Page/科學超電磁砲.md "wikilink")》片尾曲1）／三澤紗千香

:\# （《[惡魔高校D×D
New](../Page/惡魔高校D×D.md "wikilink")》片頭曲2）／[ZAQ](../Page/ZAQ.md "wikilink")

:\# Sparkling
Daydream（《[中二病也想談戀愛！](../Page/中二病也想談戀愛！.md "wikilink")》片頭曲）／ZAQ

:\# ENERGY（《[Vividred
Operation](../Page/Vividred_Operation.md "wikilink")》片頭曲）／[earthmind](../Page/earthmind.md "wikilink")

:\#
（《[天元突破紅蓮螺巖](../Page/天元突破紅蓮螺巖.md "wikilink")》片頭曲）／[中川翔子](../Page/中川翔子.md "wikilink")

:\#
（《[聖鬥士星矢Ω](../Page/聖鬥士星矢Ω.md "wikilink")》片頭曲1）／中川翔子＆[NoB](../Page/NoB.md "wikilink")

:\# （特攝片《[轟轟戰隊冒險者](../Page/轟轟戰隊冒險者.md "wikilink")》片頭曲）／NoB　

:\# [混合曲](../Page/混合曲.md "wikilink")： ＆
（《[美食獵人TORIKO](../Page/美食獵人TORIKO.md "wikilink")》片頭曲;1＆2）／[串田晃](../Page/串田晃.md "wikilink")

:\# （特攝片《[宇宙刑事Gavan](../Page/宇宙刑事Gavan.md "wikilink")》片頭曲）／串田晃

:\#  Go Fight\!（《[筋肉人](../Page/筋肉人.md "wikilink")》片頭曲1）／串田晃＆桃色幸運草Z

:\#
Days（《[交響詩篇](../Page/交響詩篇.md "wikilink")》片頭曲1）／[FLOW](../Page/FLOW.md "wikilink")

:\# CHA-LA
HEAD-CHA-LA（《[龍珠Z劇場版神與神片頭曲](../Page/龍珠Z劇場版神與神.md "wikilink")》）／FLOW

:\# GO\!\!\!（《[火影忍者](../Page/火影忍者.md "wikilink")》片頭曲4）／FLOW

:\# （《[Love
Live\!](../Page/Love_Live!.md "wikilink")》片頭曲）／[μ′s](../Page/LoveLive!.md "wikilink")

:\# START：DASH\!\!（《Love Live\!》插曲）／μ′s

:\# （《[劇場版 花開物語 HOME SWEET
HOME](../Page/花開物語.md "wikilink")》主題曲）／[nano.RIPE](../Page/nano.RIPE.md "wikilink")

:\# （《[人類衰退之後](../Page/人類衰退之後.md "wikilink")》片頭曲）／nano.RIPE

:\#
(《[緋色的碎片](../Page/緋色的碎片.md "wikilink")》片頭曲)／[藤田麻衣子](../Page/藤田麻衣子.md "wikilink")

:\# DAYS of
DASH（《[櫻花莊的寵物女孩](../Page/櫻花莊的寵物女孩.md "wikilink")》片尾曲1）／[铃木木乃美](../Page/铃木木乃美.md "wikilink")

:\#
（《[我不受歡迎，怎麼想都是你們的錯！](../Page/我不受歡迎，怎麼想都是你們的錯！.md "wikilink")》片頭曲）／鈴木木乃美
n' ＆[ZAQ](../Page/ZAQ.md "wikilink")

:\# （《[二人是少女福爾摩斯](../Page/偵探歌劇_少女福爾摩斯.md "wikilink")》片頭曲）／[Milky
Holmes](../Page/偵探歌劇_少女福爾摩斯.md "wikilink")

:\# （《[偵探歌劇 少女福爾摩斯](../Page/偵探歌劇_少女福爾摩斯.md "wikilink")》片頭曲）／Milky Holmes

:\# （《[薔薇少女](../Page/薔薇少女.md "wikilink")》片頭曲）／[ALI
PROJECT](../Page/ALI_PROJECT.md "wikilink")

:\# （《[薔薇少女 彷如夢境](../Page/薔薇少女.md "wikilink")》片頭曲）／ALI PROJECT

:\# （《[新薔薇少女](../Page/薔薇少女.md "wikilink")》片頭曲）／ALI PROJECT

:\# ViViD（《[BLOOD LAD
血意少年](../Page/BLOOD_LAD_血意少年.md "wikilink")》片頭曲）／[May'n](../Page/May'n.md "wikilink")

:\# （《[BTOOOM\!](../Page/BTOOOM!.md "wikilink")》片尾曲）／May'n

:\# Don't be
late（《[超時空要塞Frontier](../Page/超時空要塞Frontier.md "wikilink")》插曲）／May'n

:\#
（《[翠星上的加爾岡緹亞](../Page/翠星上的加爾岡緹亞.md "wikilink")》片頭曲）／[茅原實里](../Page/茅原實里.md "wikilink")

:\# CRADLE OVER（遊戲《 D2》片頭曲）／茅原實里

:\# （《[境界的彼方](../Page/境界的彼方.md "wikilink")》片頭曲）／茅原實里

:\# TERMINATED（《[境界線上的地平線](../Page/境界線上的地平線.md "wikilink")》片頭曲）／茅原實里

:; 8月24日

:\#
（《[地獄老師](../Page/靈異教師神眉.md "wikilink")》片頭曲）／[angela](../Page/angela.md "wikilink")＆

:\# sister's
noise（《[科學超電磁砲S](../Page/科學超電磁砲.md "wikilink")》片頭曲1）／[fripSide](../Page/fripSide.md "wikilink")

:\# [混合曲](../Page/混合曲.md "wikilink")：LEVEL5-judgelight- ＆ only my
railgun（《[科學超電磁砲](../Page/科學超電磁砲.md "wikilink")》片頭曲1＆2）／fripSide feat.

:\#
（《[人魚又上鉤](../Page/人魚又上鉤.md "wikilink")》片頭曲）／[上坂菫](../Page/上坂菫.md "wikilink")

:\#
（《[Aiura](../Page/Aiura.md "wikilink")》片頭曲）／[Aiu♥Love](../Page/Aiura.md "wikilink")

:\# （《[超次元戰記 戰機少女](../Page/超次元戰記_戰機少女.md "wikilink")》片尾曲）／[Afilia
Saga](../Page/Afilia_Saga.md "wikilink")

:\# BELOVED×SURVIVAL（《[BROTHERS
CONFLICT](../Page/BROTHERS_CONFLICT.md "wikilink")》片頭曲）／

:\# （《[ROBOTICS;NOTES](../Page/ROBOTICS;NOTES.md "wikilink")》片頭曲1）／Zwei

:\#
（《[琴浦小姐](../Page/琴浦小姐.md "wikilink")》片頭曲）／[中島愛](../Page/中島愛.md "wikilink")

:\# （《[玉響〜More Aggressive〜](../Page/幸福光暈.md "wikilink")》片尾曲）／中島愛

:\#
[Overfly](../Page/Overfly.md "wikilink")（《[刀劍神域](../Page/刀劍神域.md "wikilink")》片尾曲2）／[春奈露娜](../Page/春奈露娜.md "wikilink")

:\# （《[物語系列 第二季](../Page/物語系列_第二季.md "wikilink")》片尾曲1）／春奈露娜

:\#
（《[逆轉王](../Page/小雙俠.md "wikilink")》片頭曲）／＆[七森中☆娛樂部](../Page/輕鬆百合.md "wikilink")

:\#
（《[小雙俠](../Page/小雙俠_\(2008年動畫版\).md "wikilink")》片頭曲）／山本正之＆七森中☆娛樂部＆Afilia
Saga

:\# [混合曲](../Page/混合曲.md "wikilink")： ＆ （《[閃電十一人GO
第三季](../Page/閃電十一人GO.md "wikilink")》／《[閃電十一人](../Page/閃電十一人.md "wikilink")》片頭曲）／[T-Pistonz＋KMC](../Page/T-Pistonz＋KMC.md "wikilink")

:\#
（《[黑子的籃球](../Page/黑子的籃球.md "wikilink")》片尾曲2）／[OLDCODEX](../Page/OLDCODEX.md "wikilink")

:\# Rage on（《[Free\!](../Page/Free!.md "wikilink")》片頭曲）／OLDCODEX

:\# [混合曲](../Page/混合曲.md "wikilink")：THE IDOLM@STER ＆ READY\!\!（ -
／《[偶像大師](../Page/偶像大師_\(動畫\).md "wikilink")》片頭曲1）／[THE
IDOLM@STER MILLION STARS](../Page/PROJECT_IM@S.md "wikilink")

:\# Thank You\!／THE IDOLM@STER MILLION STARS

:\# STRAIGHT JET（《[IS〈Infinite
Stratos〉](../Page/IS〈Infinite_Stratos〉.md "wikilink")》片頭曲）／[栗林美奈實](../Page/栗林美奈實.md "wikilink")

:\# Doubt the World（《[Muv-Luv Alternative Total
Eclipse](../Page/Muv-Luv_Alternative_Total_Eclipse.md "wikilink")》片尾曲1）／栗林美奈實

:\# ZERO\!\!（《[打工吧！魔王大人](../Page/打工吧！魔王大人.md "wikilink")》片頭曲）／栗林美奈實

:\#
（《[革命機Valvrave](../Page/革命機Valvrave.md "wikilink")》片尾曲）／[angela](../Page/angela.md "wikilink")

:\# [混合曲](../Page/混合曲.md "wikilink")：明日へのbrilliant road ＆ Beautiful
fighter ＆ gravitation ＆  ＆
Shangri-La（《[宇宙星路](../Page/宇宙星路.md "wikilink")》片頭曲 &
《[尸姬](../Page/尸姬.md "wikilink")》片头曲 &
《[英雄時代](../Page/英雄時代_\(動畫\).md "wikilink")》片頭曲 &
《[妄想学生会](../Page/妄想学生会.md "wikilink")》片尾曲 ＆
《[苍穹之战神](../Page/苍穹之战神.md "wikilink")》片头曲）／angela

:\# KINGS（《[K](../Page/K_\(動畫\).md "wikilink")》片頭曲）／angela

:\# ／七森中☆娛樂部

:\# （《[彩虹小馬：友情就是魔法](../Page/彩虹小馬：友情就是魔法.md "wikilink")》片頭曲2）／七森中☆娛樂部

:\# Vanguard Fight（《[卡片戰鬥先導者
聯合王牌篇](../Page/卡片戰鬥先導者.md "wikilink")》片頭曲1）／[PSYCHIC
LOVER](../Page/PSYCHIC_LOVER.md "wikilink")

:\# （特攝片《[侍戰隊真劍者](../Page/侍戰隊真劍者.md "wikilink")》片頭曲）／PSYCHIC LOVER

:\# （特攝片《[特搜戰隊刑事連者](../Page/特搜戰隊刑事連者.md "wikilink")》片頭曲）／PSYCHIC LOVER &
T-Pistonz＋KMC

:\# AURORA（《[機動戰士GUNDAM
AGE](../Page/機動戰士GUNDAM_AGE.md "wikilink")》片頭曲4）／[藍井艾露](../Page/藍井艾露.md "wikilink")

:\# INNOCENCE（《[刀劍神域](../Page/刀劍神域.md "wikilink")》片頭曲2）／藍井艾露

:\#
rose（《[NANA](../Page/NANA.md "wikilink")》片頭曲1）／[土屋安娜](../Page/土屋安娜.md "wikilink")

:\# Switch
On\!（特攝片《[假面騎士Fourze](../Page/假面騎士Fourze.md "wikilink")》片頭曲）／土屋安娜

:\# crossing
field（《[刀劍神域](../Page/刀劍神域.md "wikilink")》片頭曲1）／[LiSA](../Page/LiSA.md "wikilink")

:\# träumerei（《[穿透幻影的太陽](../Page/穿透幻影的太陽.md "wikilink")》片頭曲）／LiSA

:\# Crow Song（《[Angel
Beats\!](../Page/Angel_Beats!.md "wikilink")》插曲）／LiSA

:\#
（《[黑色嘉年華](../Page/黑色嘉年華.md "wikilink")》片頭曲）／[GRANRODEO](../Page/GRANRODEO.md "wikilink")

:\# DARK SHAME（《[CØDE:BREAKER
法外制裁者](../Page/CØDE:BREAKER_法外制裁者.md "wikilink")》片頭曲）／GRANRODEO

:\# Can Do（《[黑子的籃球](../Page/黑子的籃球.md "wikilink")》片頭曲1）／GRANRODEO

:\# Go For It\!（《[IGPX](../Page/IGPX.md "wikilink")》片頭曲）／GRANRODEO

:; 8月25日

:\# [混合曲](../Page/混合曲.md "wikilink")： ＆ SCARLET KNIGHT（《[歌之王子殿下
真爱1000%](../Page/歌之王子殿下.md "wikilink")》片頭曲／《[DOG
DAYS](../Page/DOG_DAYS.md "wikilink")》片頭曲）／[水樹奈奈](../Page/水樹奈奈.md "wikilink")
& [宮野真守](../Page/宮野真守.md "wikilink")

:\#
（《[襲來！美少女邪神](../Page/襲來！美少女邪神.md "wikilink")》片頭曲）／[從後匍匐而來隊G](../Page/襲來！美少女邪神.md "wikilink")

:\# （《[襲來！美少女邪神W](../Page/襲來！美少女邪神.md "wikilink")》片頭曲）／從後匍匐而來隊G

:\# ／[竹達彩奈](../Page/竹達彩奈.md "wikilink")

:\# SAVE THE
WORLD（《[約會大作戰](../Page/約會大作戰.md "wikilink")》片尾曲2）／[野水伊織](../Page/野水伊織.md "wikilink")

:\# Black †
White（《[問題兒童都來自異世界？](../Page/問題兒童都來自異世界？.md "wikilink")》片頭曲）／野水伊織

:\# Get along（《[秀逗魔導士](../Page/秀逗魔導士.md "wikilink")》片頭曲）／野水伊織 ＆
[黑崎真音](../Page/黑崎真音.md "wikilink")

:\# §Rainbow（《[星光少女 Rainbow
Live](../Page/星光少女_Rainbow_Live.md "wikilink")》片尾曲2）／[i☆Ris](../Page/i☆Ris.md "wikilink")

:\# [混合曲](../Page/混合曲.md "wikilink")：Baby Sweet Berry Love ＆
Raise（《[變態王子與不笑貓](../Page/變態王子與不笑貓.md "wikilink")》片尾曲／《[Campione
弒神者！](../Page/Campione_弒神者！.md "wikilink")》片尾曲）／[小倉唯](../Page/小倉唯.md "wikilink")

:\# 混合曲： ＆ Shiny Blue／[唯夏織](../Page/唯夏織.md "wikilink")

:\# （《[CØDE:BREAKER
法外制裁者](../Page/CØDE:BREAKER_法外制裁者.md "wikilink")》片尾曲）／[鈴村健一](../Page/鈴村健一.md "wikilink")

:\# （音樂節目《[Anison Plus](../Page/Anison_Plus.md "wikilink")》片尾曲38）／鈴村健一

:\#
（《[神不在的星期天](../Page/神不在的星期天.md "wikilink")》片尾曲）／[小松未可子](../Page/小松未可子.md "wikilink")

:\# ／小松未可子

:\# [混合曲](../Page/混合曲.md "wikilink")： ＆
（《[遊戲王ZEXALⅡ](../Page/遊戲王ZEXAL.md "wikilink")》片頭曲／文化放送《碧和彩奈的Peti-Mila廣播》片頭曲）／[petit
milady](../Page/petit_milady.md "wikilink")（[悠木碧](../Page/悠木碧.md "wikilink")＆[竹達彩奈](../Page/竹達彩奈.md "wikilink")）

:\#
（《[ROBOTICS;NOTES](../Page/ROBOTICS;NOTES.md "wikilink")》片尾曲2）／[伊藤香奈子](../Page/伊藤香奈子.md "wikilink")

:\# （《[劇場版 命運石之門 負荷領域的既視感](../Page/命運石之門.md "wikilink")》片頭曲）／伊藤香奈子

:\#
（《[進擊的巨人](../Page/進擊的巨人.md "wikilink")》片尾曲1）／[日笠陽子](../Page/日笠陽子.md "wikilink")

:\#
（《[JoJo的奇妙冒險](../Page/JoJo的奇妙冒險.md "wikilink")》片頭曲1）／[富永TOMMY弘明](../Page/富永TOMMY弘明.md "wikilink")

:\# BLOODY STREAM（《JoJo的奇妙冒險》片頭曲2）／Coda

:\# （《[歌之王子殿下
真爱2000%](../Page/歌之王子殿下.md "wikilink")》片尾曲）／[ST☆RISH](../Page/歌之王子殿下.md "wikilink")

:\# （《歌之王子殿下 真爱2000%》插曲）／ST☆RISH

:\#
Birth（《[神不在的星期天](../Page/神不在的星期天.md "wikilink")》片頭曲）／[喜多村英梨](../Page/喜多村英梨.md "wikilink")

:\# Sha-le-la（OVA動畫《》片頭曲）／喜多村英梨

:\# [混合曲](../Page/混合曲.md "wikilink")：Black Holy ＆ Happy
Girl（《[猛烈宇宙海賊](../Page/迷你裙宇宙海賊.md "wikilink")》片尾曲／《[要聽爸爸的話！](../Page/要聽爸爸的話！.md "wikilink")》片頭曲）／小松未可子
& 喜多村英梨

:\# UNDER/SHAFT（《[軍火女王 PERFECT
ORDER](../Page/軍火女王.md "wikilink")》片頭曲）／黑崎真音

:\# （《[學園默示錄](../Page/學園默示錄.md "wikilink")》片尾曲）／黑崎真音

:\# （《[機動戰士GUNDAM
SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")》插曲）／[南里侑香](../Page/南里侑香.md "wikilink")
& 伊藤香奈子

:\# Mother
land（《[革命機Valvrave](../Page/革命機Valvrave.md "wikilink")》插曲）／南里侑香

:\# BLOODY HOLIC（《[BLOOD LAD
血意少年](../Page/BLOOD_LAD_血意少年.md "wikilink")》片尾曲）／南里侑香

:\# （《[歌之王子殿下 真爱2000%](../Page/歌之王子殿下.md "wikilink")》片頭曲）／宮野真守

:\# ULTRA FLY（特攝片《[超人力霸王列傳](../Page/奥特曼列传.md "wikilink")》片頭曲7）／宮野真守

:\# GENESIS
ARIA（《[革神語](../Page/革神語.md "wikilink")》片頭曲）／[sphere](../Page/sphere_\(聲優團體\).md "wikilink")

:\# Pride on Everyday（《[爆漫王。3](../Page/爆漫王。.md "wikilink")》片尾曲）／sphere

:\# LET・ME・DO\!\!（日劇《》片頭曲）／sphere

:\#
（《[我的妹妹哪有這麼可愛！](../Page/我的妹妹哪有這麼可愛！_\(動畫\).md "wikilink")》插曲）／[田村由香里](../Page/田村由香里.md "wikilink")

:\# W：Wonder
tale（《[我女友與青梅竹馬的慘烈修羅場](../Page/我女友與青梅竹馬的慘烈修羅場.md "wikilink")》片尾曲）／田村由香里

:\# Fantastic
future（《[變態王子與不笑貓](../Page/變態王子與不笑貓.md "wikilink")》片頭曲）／田村由香里

:\# （《[宇宙戰艦大和號2199 OVA](../Page/宇宙戰艦大和號2199.md "wikilink")》片尾曲7）／水樹奈奈

:\# BRAVE PHOENIX（《[魔法少女奈葉A's](../Page/魔法少女奈葉A's.md "wikilink")》插曲）／水樹奈奈

:\# Vitalization（《[戰姬絕唱SYMPHOGEAR
G](../Page/戰姬絕唱SYMPHOGEAR.md "wikilink")》片頭曲）／水樹奈奈

:\#
Synchrogazer（《[戰姬絕唱SYMPHOGEAR](../Page/戰姬絕唱SYMPHOGEAR.md "wikilink")》片頭曲）／水樹奈奈

## 2014年

### Animelo Summer Live 2014 -ONENESS-

  - 舉辦日期：2014年8月29日－8月31日
  - 會場：[埼玉超級體育館](../Page/埼玉超級體育館.md "wikilink")
  - 主辦：MAGES.、[文化放送](../Page/文化放送.md "wikilink")
  - 協力贊助：[Good Smile
    Company](../Page/Good_Smile_Company.md "wikilink")、[武士道](../Page/武士道_\(公司\).md "wikilink")
  - 後援：[NBC環球娛樂](../Page/NBC環球娛樂.md "wikilink")、[King
    Records](../Page/King_Records.md "wikilink")、[日本索尼音樂娛樂](../Page/日本索尼音樂娛樂.md "wikilink")、[日本哥倫比亞](../Page/日本哥倫比亞.md "wikilink")、[Flying
    DOG](../Page/Flying_DOG.md "wikilink")、[波麗佳音](../Page/波麗佳音.md "wikilink")、[Media
    Factory](../Page/Media_Factory.md "wikilink")、[Lantis](../Page/Lantis.md "wikilink")、Warner
    Home Video
  - 企劃：AniSummer Project實行委員會
  - 協力：埼玉超級體育館、[7-Eleven](../Page/7-Eleven.md "wikilink")、[淘兒唱片](../Page/淘兒唱片.md "wikilink")
  - 製作協力：Grand Slam

#### 主題曲

  - ONENESS
    作詞、作曲：[奧井雅美](../Page/奧井雅美.md "wikilink")，編曲：服部隆之

#### 出演者

  - 8月29日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [ALTIMA](../Page/ALTIMA.md "wikilink")
  - [ZAQ](../Page/ZAQ.md "wikilink")
  - [JAM Project](../Page/JAM_Project.md "wikilink")
  - [sweet
    ARMS](../Page/sweet_ARMS.md "wikilink")<small>（[野水伊織](../Page/野水伊織.md "wikilink")、[富樫美鈴](../Page/富樫美鈴.md "wikilink")、[佐土原香織](../Page/佐土原香織.md "wikilink")、[味里](../Page/味里.md "wikilink")）</small>
  - [茅原實里](../Page/茅原實里.md "wikilink")
  - [fripSide](../Page/fripSide.md "wikilink")
  - [Wake Up, Girls\!](../Page/Wake_Up,_Girls!.md "wikilink")
  - [STAR☆ANIS](../Page/STAR☆ANIS.md "wikilink")
  - [藍井艾露](../Page/藍井艾露.md "wikilink")
  - [T.M.Revolution](../Page/T.M.Revolution.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [小野賢章](../Page/小野賢章.md "wikilink")
  - [艦隊收藏](../Page/艦隊收藏.md "wikilink")<small>（[藤田咲](../Page/藤田咲.md "wikilink")、[野水伊織](../Page/野水伊織.md "wikilink")、[東山奈央](../Page/東山奈央.md "wikilink")）</small>
  - [谷本貴義](../Page/谷本貴義.md "wikilink")
  - [南條愛乃](../Page/南條愛乃.md "wikilink")
  - [petit milady](../Page/petit_milady.md "wikilink")
  - [黑崎真音](../Page/黑崎真音.md "wikilink")
  - [9nine](../Page/9nine.md "wikilink")
  - [和田光司](../Page/和田光司.md "wikilink")
  - [流田Project](../Page/流田Project.md "wikilink")
  - [Project.R](../Page/Project.R.md "wikilink")<small>（[谷本貴義](../Page/谷本貴義.md "wikilink")、[PSYCHIC
    LOVER](../Page/YOFFY.md "wikilink")、[鎌田章吾](../Page/鎌田章吾.md "wikilink")）</small>

</div>

  - 8月30日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [angela](../Page/angela.md "wikilink")
  - [栗林美奈實](../Page/栗林美奈實.md "wikilink")
  - [GRANRODEO](../Page/GRANRODEO.md "wikilink")
  - [FLOW](../Page/FLOW.md "wikilink")
  - [三澤紗千香](../Page/三澤紗千香.md "wikilink")
  - [三森鈴子](../Page/三森鈴子.md "wikilink")
  - [桃色幸運草Z](../Page/桃色幸運草Z.md "wikilink")
  - [THE
    IDOLM@STER](../Page/THE_IDOLM@STER.md "wikilink")<small>（[中村繪里子](../Page/中村繪里子.md "wikilink")、[今井麻美](../Page/今井麻美.md "wikilink")、[下田麻美](../Page/下田麻美.md "wikilink")、[沼倉愛美](../Page/沼倉愛美.md "wikilink")）</small>
  - [THE IDOLM@STER CINDERELLA
    GIRLS](../Page/THE_IDOLM@STER_CINDERELLA_GIRLS.md "wikilink")
  - [THE IDOLM@STER MILLION STARS](../Page/PROJECT_IM@S.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [單色小姐](../Page/單色小姐_-The_Animation-.md "wikilink")
  - [堀江由衣](../Page/堀江由衣.md "wikilink")
  - [喜多村英梨](../Page/喜多村英梨.md "wikilink")
  - [地獄的沙汰全明星
    阿仁尊地獄篇](../Page/鬼燈的冷徹.md "wikilink")<small>（[安元洋貴](../Page/安元洋貴.md "wikilink")、[長嶝高士](../Page/長嶝高士.md "wikilink")、[青山桐子](../Page/青山桐子.md "wikilink")、[小林由美子](../Page/小林由美子.md "wikilink")、[種崎敦美](../Page/種崎敦美.md "wikilink")、喜多村英梨、[山田榮子](../Page/山田榮子.md "wikilink")、[島本須美](../Page/島本須美.md "wikilink")）</small>
  - [Kalafina](../Page/Kalafina.md "wikilink")
  - [碧琪·瑪姬](../Page/鬼燈的冷徹.md "wikilink")
  - [fhána](../Page/fhána.md "wikilink")
  - [堀江美都子](../Page/堀江美都子.md "wikilink")

</div>

  - 8月31日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [小倉唯](../Page/小倉唯.md "wikilink")
  - [铃木木乃美](../Page/铃木木乃美.md "wikilink")
  - [田村由香里](../Page/田村由香里.md "wikilink")
  - [水樹奈奈](../Page/水樹奈奈.md "wikilink")
  - [宮野真守](../Page/宮野真守.md "wikilink")
  - [May'n](../Page/May'n.md "wikilink")
  - [唯夏織](../Page/唯夏織.md "wikilink")
  - [LiSA](../Page/LiSA.md "wikilink")
  - [Afilia Saga](../Page/Afilia_Saga.md "wikilink")
  - [μ′s](../Page/LoveLive!.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [光之美少女夏日彩虹！](../Page/光之美少女系列.md "wikilink")<small>（[五條真由美](../Page/五條真由美.md "wikilink")、[工藤真由](../Page/工藤真由.md "wikilink")、[池田彩](../Page/池田彩.md "wikilink")、[吉田仁美](../Page/吉田仁美.md "wikilink")、[仲谷明香](../Page/仲谷明香.md "wikilink")）</small>
  - [伊藤香奈子](../Page/伊藤香奈子.md "wikilink")
  - [悠木碧](../Page/悠木碧.md "wikilink")
  - [OLDCODEX](../Page/OLDCODEX.md "wikilink")
  - [橋本仁](../Page/橋本仁.md "wikilink")

</div>

#### 演唱歌曲一覽

:; 8月29日

:\# SKILL（PS2遊戲《第2次超級機械人大戰α》片頭曲）／[JAM
Project](../Page/JAM_Project.md "wikilink")

:\# Wings of the
legend（PS2遊戲《[第2次超級機械人大戰OG](../Page/第2次超級機械人大戰OG.md "wikilink")》片頭曲）／JAM
Project

:\#
[混合曲](../Page/混合曲.md "wikilink")：Breakthrough～Rocks～（電視動畫《[愚者信長](../Page/愚者信長.md "wikilink")》片頭曲2、PS2遊戲《[超級機械人大戰OG
ORIGINAL
GENERATIONS](../Page/超級機械人大戰OG_ORIGINAL_GENERATIONS.md "wikilink")》片頭曲、特攝片《119特警隊救火英雄》片頭曲）／JAM
Project

:\# FOOL THE WORLD（電視動畫《愚者信長》片頭曲1）／[茅原實里](../Page/茅原實里.md "wikilink")

:\# （電視動畫《[境界的彼方](../Page/境界的彼方.md "wikilink")》片頭曲）／茅原實里

:\# （電視動畫《[RAIL WARS\!
-日本國有鐵道公安隊-](../Page/RAIL_WARS!_-日本國有鐵道公安隊-.md "wikilink")》片頭曲）／茅原實里

:\# 混合曲：OVERDRIVER～ZAQNESS（電視動畫《RAIL WARS\!
-日本國有鐵道公安隊-》片尾曲）／[ZAQ](../Page/ZAQ.md "wikilink")

:\#
Alteration（電視動畫《[鎖鎖美小姐@不好好努力](../Page/鎖鎖美小姐@不好好努力.md "wikilink")》片頭曲）／ZAQ

:\# 混合曲：Trust in
you～（電視動畫《[約會大作戰](../Page/約會大作戰.md "wikilink")》片頭曲、電視動畫《[約會大作戰Ⅱ](../Page/約會大作戰.md "wikilink")》片頭曲）／[sweet
ARMS](../Page/sweet_ARMS.md "wikilink")

:\# 7 Girls War（電視動畫《[Wake Up,
Girls\!](../Page/Wake_Up,_Girls!.md "wikilink")》片頭曲2）／[Wake Up,
Girls\!](../Page/Wake_Up,_Girls!.md "wikilink")

:\# （劇場版及電視動畫《Wake Up, Girls\!》主題曲）／Wake Up, Girls\!

:\#
混合曲：～Signalize\!～（電視動畫《[星夢學園](../Page/星夢學園.md "wikilink")》插曲、片頭曲1、片頭曲2）／[STAR☆ANIS](../Page/STAR☆ANIS.md "wikilink")

:\# SHINING LINE\* （電視動畫《星夢學園》片頭曲4）／STAR☆ANIS

:\# [With You/With
Me](../Page/With_You/With_Me.md "wikilink")（電視動畫《[魔奇少年
The kingdom of
magic](../Page/魔奇少年.md "wikilink")》片尾曲2）／[9nine](../Page/9nine.md "wikilink")

:\#
（電視動畫《[東京闇鴉](../Page/東京闇鴉.md "wikilink")》片尾曲1）／[南條愛乃](../Page/南條愛乃.md "wikilink")

:\# 混合曲：Dragon Soul～
Kuu-Zen-Zetsu-Go（電視動畫《[龍珠改](../Page/龍珠改.md "wikilink")》片頭曲、電視動畫《[龍珠改
魔人布歐篇](../Page/龍珠改.md "wikilink")》片頭曲）／[谷本貴義](../Page/谷本貴義.md "wikilink")

:\#
（特攝片《[烈車戰隊特急者](../Page/烈車戰隊特急者.md "wikilink")》片尾曲）／[Project.R](../Page/Project.R.md "wikilink")

:\# Fight 4
Real（電視動畫《[噬血狂襲](../Page/噬血狂襲.md "wikilink")》片頭曲2）／[ALTIMA](../Page/ALTIMA.md "wikilink")

:\# Burst The
Gravity（電視動畫《[加速世界](../Page/加速世界.md "wikilink")》片頭曲2）／ALTIMA

:\# CYBER CYBER／ALTIMA featuring Wake Up, Girls, STAR☆ANIS & 9nine

:\# （電視動畫《[聖鬥士星矢Ω](../Page/聖鬥士星矢Ω.md "wikilink")》片頭曲3）／流田Project

:\# （電視動畫《[機動戰士GUNDAM
ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")》片頭曲1）／流田Project
& Project.R

:\# （電視動畫《[三坪房間的侵略者！？](../Page/三坪房間的侵略者！？.md "wikilink")》片尾曲）／[petit
milady](../Page/petit_milady.md "wikilink")

:\# ♡／petit milady

:\# ／[艦隊收藏
第一航空戰隊](../Page/艦隊收藏.md "wikilink")（[野水伊織](../Page/野水伊織.md "wikilink")、[藤田咲](../Page/藤田咲.md "wikilink")）

:\# ／[艦隊收藏
金剛型高速戰艦](../Page/艦隊收藏.md "wikilink")（[東山奈央](../Page/東山奈央.md "wikilink")）

:\# FANTASTIC TUNE（電視動畫《[幻影籃球王
第2期](../Page/幻影籃球王.md "wikilink")》片尾曲2）／[小野賢章](../Page/小野賢章.md "wikilink")　

:\#
Butter-Fly（電視動畫《[數碼暴龍大冒險](../Page/數碼暴龍大冒險.md "wikilink")》片頭曲）／[和田光司](../Page/和田光司.md "wikilink")

:\#
IGNITE（電視動畫《[刀劍神域Ⅱ](../Page/刀劍神域.md "wikilink")》片頭曲）／[藍井艾露](../Page/藍井艾露.md "wikilink")

:\# （電視動畫《[KILL la KILL](../Page/KILL_la_KILL.md "wikilink")》片頭曲）／藍井艾露

:\# [Believe](../Page/Believe_\(玉置成實單曲\).md "wikilink")（電視動畫《[機動戰士GUNDAM
SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")》片頭曲3）／藍井艾露 & 黑崎真音

:\#
X-encounter（電視動畫《[東京闇鴉](../Page/東京闇鴉.md "wikilink")》片頭曲1）／[黑崎真音](../Page/黑崎真音.md "wikilink")

:\# [sister's
noise](../Page/sister's_noise.md "wikilink")（電視動畫《[科學超電磁砲S](../Page/科學超電磁砲.md "wikilink")》片頭曲1）／[fripSide](../Page/fripSide.md "wikilink")

:\# [black
bullet](../Page/black_bullet.md "wikilink")（電視動畫《[黑色子彈](../Page/黑色子彈.md "wikilink")》片頭曲）／fripSide

:\# [only my
railgun](../Page/only_my_railgun.md "wikilink")（電視動畫《[科學超電磁砲](../Page/科學超電磁砲.md "wikilink")》片頭曲1）／fripSide

:\# [WHITE
BREATH](../Page/WHITE_BREATH.md "wikilink")／[T.M.Revolution](../Page/T.M.Revolution.md "wikilink")

:\# [INVOKE](../Page/INVOKE.md "wikilink")（電視動畫《[機動戰士GUNDAM
SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")》片頭曲1）／T.M.Revolution

:\# ignited --（電視動畫《[機動戰士GUNDAM SEED
DESTINY](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")》片頭曲1）／T.M.Revolution

:\# [HEART OF SWORD
〜夜明け前〜](../Page/HEART_OF_SWORD_～夜明之前～.md "wikilink")（電視動畫《[浪客劍心](../Page/浪客劍心.md "wikilink")》片尾曲3）／T.M.Revolution

:\# EN-1 OUTRIDE（2006年主題曲）

:\# EN-2 ONENESS（2014年主題曲）

:; 8月30日

:\# GO\!\!\!（電視動畫《[火影忍者](../Page/火影忍者.md "wikilink")》片頭曲4）／FLOW &
GRANRODEO

:\# COLORS（電視動畫《[Code
Geass反叛的魯路修](../Page/Code_Geass反叛的魯路修.md "wikilink")》片頭曲1）／[FLOW](../Page/FLOW.md "wikilink")

:\# （電視動畫《[武士弗拉明戈](../Page/武士弗拉明戈.md "wikilink")》片頭曲2）／FLOW

:\# [MOON
PRIDE](../Page/MOON_PRIDE.md "wikilink")（電視動畫《[美少女戰士Crystal](../Page/美少女戰士Crystal.md "wikilink")》片頭曲）／[桃色幸運草Z](../Page/桃色幸運草Z.md "wikilink")

:\# 月虹（電視動畫《美少女戰士Crystal》片尾曲）／桃色幸運草Z

:\# （Emperor Style）（劇場版動畫《[猛烈宇宙海賊
亞空的深淵](../Page/迷你裙宇宙海賊.md "wikilink")》片頭曲）／桃色幸運草Z

:\# （電視動畫《[萌萌侵略者 OUTBREAK
COMPANY](../Page/萌萌侵略者_OUTBREAK_COMPANY.md "wikilink")》片頭曲）／[三森鈴子](../Page/三森鈴子.md "wikilink")

:\# （電視動畫《[修業魔女璐璐萌](../Page/修業魔女璐璐萌.md "wikilink")》片頭曲）／三森鈴子

:\# （電視動畫《[白銀的意志
ARGEVOLLEN](../Page/白銀的意志_ARGEVOLLEN.md "wikilink")》片尾曲）／[三澤纱千香](../Page/三澤纱千香.md "wikilink")

:\# （電視動畫《[加速世界](../Page/加速世界.md "wikilink")》片尾曲2）／三澤纱千香

:\#
（電視動畫《[我們大家的河合莊](../Page/我們大家的河合莊.md "wikilink")》片頭曲）／[fhána](../Page/fhána.md "wikilink")

:\# divine
intervention（電視動畫《[魔女的使命](../Page/魔女的使命.md "wikilink")》片頭曲）／fhána

:\# （電視動畫《[單色小姐 -The
Animation-](../Page/單色小姐_-The_Animation-.md "wikilink")》主題曲）／[單色小姐](../Page/單色小姐_-The_Animation-.md "wikilink")

:\# Stand Up！（手機遊戲《白貓計劃》主題曲）／[堀江由衣](../Page/堀江由衣.md "wikilink")

:\# 混合曲：Golden
Time～♡（電視動畫《[青春紀行](../Page/青春紀行.md "wikilink")》片頭曲1、片尾曲2）／堀江由衣

:\#
混合曲：～～～（電視動畫《[小甜甜](../Page/小甜甜.md "wikilink")》片頭曲、電視動畫《[小魔鏡](../Page/小魔鏡.md "wikilink")
第2作》片頭曲、電視動畫《[花仙子](../Page/花仙子.md "wikilink")》片頭曲、電視動畫《[海螺小姐](../Page/海螺小姐.md "wikilink")》片頭曲1）／[堀江美都子](../Page/堀江美都子.md "wikilink")

:\# （電視動畫《[噴嚏大魔王](../Page/噴嚏大魔王.md "wikilink")》片尾曲）／堀江美都子 featuring
桃色幸運草Z

:\# moving soul（電視動畫《[Fate/kaleid liner 魔法少女☆伊莉雅
2wei](../Page/Fate/kaleid_liner_魔法少女☆伊莉雅.md "wikilink")》片頭曲）／[栗林美奈實](../Page/栗林美奈實.md "wikilink")

:\# Shining☆Days（電視動畫《[舞-HiME](../Page/舞-HiME.md "wikilink")》片頭曲）／栗林美奈實

:\# （電視動畫《[機動戰士GUNDAM00 2nd
Season](../Page/機動戰士GUNDAM00.md "wikilink")》片頭曲1）／栗林美奈實 &
angela

:\# ／[地獄的沙汰全明星
阿仁尊地獄篇](../Page/鬼燈的冷徹.md "wikilink")（[安元洋貴](../Page/安元洋貴.md "wikilink")、[長嶝高士](../Page/長嶝高士.md "wikilink")、[青山桐子](../Page/青山桐子.md "wikilink")、[小林由美子](../Page/小林由美子.md "wikilink")、[種崎敦美](../Page/種崎敦美.md "wikilink")、[喜多村英梨](../Page/喜多村英梨.md "wikilink")）

:\#
（電視動畫《[鬼燈的冷徹](../Page/鬼燈的冷徹.md "wikilink")》片尾曲3）／[碧琪·瑪姬](../Page/鬼燈的冷徹.md "wikilink")

:\#  （電視動畫《鬼燈的冷徹》片頭曲）／地獄的沙汰全明星
阿仁尊地獄篇（安元洋貴、長嶝高士、青山桐子、小林由美子、種崎敦美、喜多村英梨、[山田榮子](../Page/山田榮子.md "wikilink")、[島本須美](../Page/島本須美.md "wikilink")）

:\# READY\!\!（電視動畫《[偶像大師](../Page/偶像大師_\(動畫\).md "wikilink")》片頭曲1）／[THE
IDOLM@STER](../Page/THE_IDOLM@STER.md "wikilink")

:\# ／[THE IDOLM@STER CINDERELLA
GIRLS](../Page/THE_IDOLM@STER_CINDERELLA_GIRLS.md "wikilink")

:\# Thank You\!／[THE IDOLM@STER MILLION
STARS](../Page/PROJECT_IM@S.md "wikilink")

:\# M@STERPIECE／THE IDOLM@STER THREE STARS\!\!\!

:\# [to the beginning](../Page/to_the_beginning.md "wikilink")
（電視動畫《[Fate/Zero
第二季](../Page/Fate/Zero.md "wikilink")》片頭曲）／[Kalafina](../Page/Kalafina.md "wikilink")

:\# [君の銀の庭](../Page/銀色庭園.md "wikilink") （劇場版動畫《\[\[劇場版_魔法少女小圓|魔法少女小圓
\[新編\] 叛逆的物語\]\]》片尾曲）／Kalafina

:\# [heavenly
blue](../Page/heavenly_blue.md "wikilink")（電視動畫《[ALDNOAH.ZERO](../Page/ALDNOAH.ZERO.md "wikilink")》片頭曲）／Kalafina

:\# 掌
-show-（電視動畫《[銀河騎士傳](../Page/銀河騎士傳.md "wikilink")》片尾曲）／[喜多村英梨](../Page/喜多村英梨.md "wikilink")

:\#
[Birth](../Page/Birth_\(喜多村英梨單曲\).md "wikilink")（電視動畫《[神不在的星期天](../Page/神不在的星期天.md "wikilink")》片頭曲）／喜多村英梨

:\# Different colors（劇場版動畫《[K MISSING
KINGS](../Page/K_\(動畫\).md "wikilink")》主題曲）／[angela](../Page/angela.md "wikilink")

:\# Shangri-La（電視動畫《[蒼穹之戰神](../Page/蒼穹之戰神.md "wikilink")》片頭曲）／angela

:\# （電視動畫《銀河騎士傳》片頭曲）／angela

:\# （電視動畫《[幻影籃球王
第2期](../Page/幻影籃球王.md "wikilink")》片頭曲2）／[GRANRODEO](../Page/GRANRODEO.md "wikilink")

:\# ／GRANRODEO

:\# （電視動畫《戀愛天使安琪莉可 輝煌的明日》片頭曲）／GRANRODEO

:\# Can Do（電視動畫《幻影籃球王》片頭曲1）／GRANRODEO

:\# EN-1 RE:BRIDGE～Return to oneself～（2009年主題曲）

:\# EN-2 ONENESS（2014年主題曲）

:; 8月31日

:\# [Preserved
Roses](../Page/Preserved_Roses.md "wikilink")（電視動畫《[革命機Valvrave](../Page/革命機Valvrave.md "wikilink")》片頭曲）／[T.M.Revolution](../Page/T.M.Revolution.md "wikilink")×水樹奈奈

:\# （電視動畫《[革命機Valvrave
第2期](../Page/革命機Valvrave.md "wikilink")》片頭曲）／水樹奈奈×T.M.Revolution

:\#
（電視動畫《[香格里拉](../Page/香格里拉_\(科幻小說\).md "wikilink")》片頭曲）／[May'n](../Page/May'n.md "wikilink")
featuring Daichi

:\#
Re:REMEMBER（電視動畫《[M3〜其為黑鋼〜](../Page/M3〜其為黑鋼〜.md "wikilink")》片頭曲1）／May'n

:\# （電視動畫《[狐仙的戀愛入門](../Page/狐仙的戀愛入門.md "wikilink")》片頭曲）／May'n

:\# （電視動畫《[世界征服
謀略之星](../Page/世界征服_謀略之星.md "wikilink")》片尾曲）／[悠木碧](../Page/悠木碧.md "wikilink")

:\# （電視動畫《[如果折斷她的旗](../Page/如果折斷她的旗.md "wikilink")》片頭曲）／悠木碧

:\# （電視動畫《[我的腦內戀礙選項](../Page/我的腦內戀礙選項.md "wikilink")》片頭曲）／[Afilia
Saga](../Page/Afilia_Saga.md "wikilink")

:\# ／Afilia Saga featuring

:\# 混合曲：Charming Do\! ～Tinkling
Smile（電視動畫《[最近，妹妹的樣子有點怪？](../Page/最近，妹妹的樣子有點怪？.md "wikilink")》片尾曲、電視動畫《[前進吧！登山少女
第二季](../Page/前進吧！登山少女.md "wikilink")》片尾曲）／[小倉唯](../Page/小倉唯.md "wikilink")

:\# 混合曲：PUPPY LOVE\!\! ～ Intro
Situation／[唯夏織](../Page/唯夏織.md "wikilink")

:\# （電視動畫《[T寶的悲慘日常](../Page/T寶的悲慘日常.md "wikilink")》片頭曲）／田村由香里 & 宮野真守

:\# Anisama 10th Special Medley（前半）：[This
game](../Page/This_game.md "wikilink")～CHOIR JAIL～（電視動畫《[NO GAME NO LIFE
遊戲人生](../Page/NO_GAME_NO_LIFE_遊戲人生.md "wikilink")》片頭曲、電視動畫《[黃昏乙女×失憶幽靈](../Page/黃昏乙女×失憶幽靈.md "wikilink")》片頭曲、電視動畫《[我不受歡迎，怎麼想都是你們的錯！](../Page/我不受歡迎，怎麼想都是你們的錯！.md "wikilink")》片頭曲）／[鈴木木乃美](../Page/鈴木木乃美.md "wikilink")

:\# Anisama 10th Special Medley（後半）：AVENGE WORLD～DAYS of DASH～This
game（電視動畫《[結界女王
震顫](../Page/結界女王.md "wikilink")》片頭曲、電視動畫《[櫻花莊的寵物女孩](../Page/櫻花莊的寵物女孩.md "wikilink")》片尾曲、電視動畫《NO
GAME NO LIFE 遊戲人生》片頭曲）／鈴木木乃美

:\# Another
Heaven（遊戲《[命運石之門](../Page/命運石之門.md "wikilink")》主題曲）／[伊藤香奈子](../Page/伊藤香奈子.md "wikilink")

:\# Hacking to the Gate（電視動畫《命運石之門》片頭曲）／伊藤香奈子 & 鈴木木乃美

:\# STAND PROUD（電視動畫《[JoJo的奇妙冒險
星塵鬥士](../Page/JoJo的奇妙冒險.md "wikilink")》片頭曲）／橋本仁

:\# [Rising
Hope](../Page/Rising_Hope.md "wikilink")（電視動畫《[魔法科高中的劣等生](../Page/魔法科高中的劣等生.md "wikilink")》片頭曲1）／[LiSA](../Page/LiSA.md "wikilink")

:\# （電視動畫《[目隱都市的演繹者](../Page/陽炎計劃.md "wikilink")》插曲）／LiSA

:\# [crossing
field](../Page/crossing_field.md "wikilink")（電視動畫《[刀劍神域](../Page/刀劍神域.md "wikilink")》片頭曲）／LiSA

:\# （電視動畫《[Happiness Charge
光之美少女！](../Page/Happiness_Charge_光之美少女！.md "wikilink")》片頭曲）／[仲谷明香](../Page/仲谷明香.md "wikilink")

:\# 混合曲：～（電視動畫《Happiness Charge
光之美少女！》片尾曲1、片尾曲2）／[吉田仁美](../Page/吉田仁美.md "wikilink")

:\# DANZEN\! （Ver. Max Heart）（電視動畫《[光之美少女Max
Heart](../Page/光之美少女.md "wikilink")》片頭曲）／[五條真由美](../Page/五條真由美.md "wikilink")＋工藤真由＋池田彩

:\# 混合曲：／[光之美少女夏日彩虹！](../Page/光之美少女系列.md "wikilink")　

:\# （電視動畫《[LoveLive\!
第2期](../Page/LoveLive!.md "wikilink")》片頭曲）／[μ's](../Page/LoveLive!.md "wikilink")

:\# Snow halation（電視動畫《LoveLive\! 第2期》插曲）／μ's

:\# No brand
girls（電視動畫《[LoveLive\!](../Page/LoveLive!.md "wikilink")》插曲）／μ's

:\# WALK（電視動畫《[幻影籃球王
第2期](../Page/幻影籃球王.md "wikilink")》片尾曲1）／[OLDCODEX](../Page/OLDCODEX.md "wikilink")

:\# Dried Up Youthful Fame（電視動畫《[Free\!-Eternal
Summer-](../Page/Free!.md "wikilink")》片頭曲）／OLDCODEX

:\# [Rage
on](../Page/Rage_on.md "wikilink")（電視動畫《[Free\!](../Page/Free!.md "wikilink")》片頭曲）／OLDCODEX

:\# NEW ORDER（電視動畫《[T寶的悲慘日常
覺醒篇](../Page/T寶的悲慘日常.md "wikilink")》片頭曲）／[宮野真守](../Page/宮野真守.md "wikilink")

:\# （電視動畫《[歌之王子殿下2000%](../Page/歌之王子殿下.md "wikilink")》片頭曲）／宮野真守

:\# You & Me／[田村由香里](../Page/田村由香里.md "wikilink") featuring motsu

:\# Fantastic
future（電視動畫《[變態王子與不笑貓](../Page/變態王子與不笑貓.md "wikilink")》片頭曲）／田村由香里

:\# （電視動畫《[農林](../Page/農林.md "wikilink")》片頭曲）／田村由香里

:\#  -type EXCITER-（PSP遊戲《[潛龍諜影
和平先驅](../Page/潛龍諜影_和平先驅.md "wikilink")》插曲）／[水樹奈奈](../Page/水樹奈奈.md "wikilink")

:\# METRO BAROQUE（劇場版動畫《[BLOOD-C The Last
Dark](../Page/BLOOD-C.md "wikilink")》主題曲）／水樹奈奈

:\# BRIGHT STREAM（劇場版動畫《[魔法少女奈葉 The MOVIE 2nd
A's](../Page/魔法少女奈葉_The_MOVIE_2nd_A's.md "wikilink")》片頭曲）／水樹奈奈

:\# TRANSMIGRATION／水樹奈奈 & [奥井雅美](../Page/奥井雅美.md "wikilink")

:\# EN-1 Generation-A（2007年主題曲）

:\# EN-2 ONENESS（2014年主題曲）

### ANISAMA WORLD 2014 in Saitama

  - 舉辦日：2014年10月12日
  - 會場：

#### 出演者

  - [奧井雅美](../Page/奧井雅美.md "wikilink")
  - [北谷洋](../Page/北谷洋.md "wikilink")
  - [彩音](../Page/彩音.md "wikilink")
  - [橋本仁](../Page/橋本仁.md "wikilink")
  - [Ray](../Page/Ray_\(歌手\).md "wikilink")
  - [春奈露娜](../Page/春奈露娜.md "wikilink")
  - [上坂堇](../Page/上坂堇.md "wikilink")
  - [鈴木木乃美](../Page/鈴木木乃美.md "wikilink")
  - [Afilia Saga](../Page/Afilia_Saga.md "wikilink")
  - [大橋彩香](../Page/大橋彩香.md "wikilink")

#### 演唱歌曲一覽

:\# Get
along（電視動畫《[秀逗魔導士](../Page/秀逗魔導士.md "wikilink")》片頭曲）／[奧井雅美](../Page/奧井雅美.md "wikilink")×[鈴木木乃美](../Page/鈴木木乃美.md "wikilink")

:\# ～for a
yours～（電視動畫《[滴骰孖妹聖誕歷奇](../Page/Di_Gi_Charat.md "wikilink")》片頭曲）／奧井雅美

:\# ／奧井雅美

:\# Arrival of Tears（電視動畫《[11eyes
-罪與罰與贖的少女-](../Page/11eyes_-罪與罰與贖的少女-.md "wikilink")》片頭曲）／[彩音](../Page/彩音.md "wikilink")

:\# （劇場版動畫《[命運石之門 負荷領域的既視感](../Page/命運石之門.md "wikilink")》片尾曲）／彩音

:\#
lull～～（電視動畫《[來自風平浪靜的明天](../Page/來自風平浪靜的明天.md "wikilink")》片頭曲）／[Ray](../Page/Ray_\(歌手\).md "wikilink")

:\# （電視動畫《[在那個夏天等待 特別編](../Page/在那個夏天等待.md "wikilink")》片頭曲）／Ray

:\# （遊戲《To LOVE Darkness 戰鬥絕頂》主題曲）／Ray

:\# S・M・L☆（電視動畫《[我的腦內戀礙選項](../Page/我的腦內戀礙選項.md "wikilink")》片頭曲）／[Afilia
Saga](../Page/Afilia_Saga.md "wikilink")

:\# ／Afilia Saga feat.

:\# ／Afilia Saga×奧井雅美

:\#
YES\!\!（電視動畫《[生存遊戲社](../Page/生存遊戲社.md "wikilink")》片頭曲）／[大橋彩香](../Page/大橋彩香.md "wikilink")

:\# （電視動畫《[物語系列
第二季](../Page/物語系列_第二季.md "wikilink")》片尾曲）／[春奈露娜](../Page/春奈露娜.md "wikilink")

:\#
[Startear](../Page/Startear.md "wikilink")（電視動畫《[刀劍神域II](../Page/刀劍神域.md "wikilink")》片尾曲）／春奈露娜

:\# [secret base ～君がくれたもの～（10 years after
Ver.）](../Page/secret_base_～你給我的東西～_\(10_years_after_Ver.\).md "wikilink")（電視動畫《[我們仍未知道那天所看見的花名。](../Page/我們仍未知道那天所看見的花名。.md "wikilink")》片尾曲）／春奈露娜×Ray×彩音

:\#
（電視動畫《[人魚又上鉤](../Page/人魚又上鉤.md "wikilink")》片頭曲）／[上坂堇](../Page/上坂堇.md "wikilink")

:\# （電視動畫《[鬼燈的冷徹](../Page/鬼燈的冷徹.md "wikilink")》片尾曲）／上坂堇

:\# （電視動畫《[幻想玩偶](../Page/幻想玩偶.md "wikilink")》片頭曲）／上坂堇×大橋彩香

:\# STAND PROUD（電視動畫《[JoJo的奇妙冒险
星塵鬥士](../Page/JoJo的奇妙冒险.md "wikilink")》片頭曲）／橋本仁

:\# （特攝劇集《[假面騎士空我](../Page/假面騎士空我.md "wikilink")》片尾曲）／橋本仁

:\# （電視動畫《[洛克人EXE](../Page/洛克人EXE_\(動畫\).md "wikilink")》片頭曲）／橋本仁

:\# This game ～ CHOIR JAIL ～ （電視動畫《[NO GAME NO LIFE
遊戲人生](../Page/NO_GAME_NO_LIFE_遊戲人生.md "wikilink")》片頭曲、《[黃昏少女×遺失記憶](../Page/黃昏少女×遺失記憶.md "wikilink")》片頭曲、《[我不受歡迎，怎麼想都是你們的錯](../Page/我不受歡迎，怎麼想都是你們的錯.md "wikilink")》片頭曲）／鈴木木乃美

:\# AVENGE WORLD ～ DAYS of DASH ～ This game（電視動畫《[結界女王
震顫](../Page/結界女王.md "wikilink")》片頭曲、《[櫻花莊的寵物女孩](../Page/櫻花莊的寵物女孩.md "wikilink")》片尾曲1、《NO
GAME NO LIFE 遊戲人生》片頭曲）／鈴木木乃美

:\#  （電視動畫《[魔彈之王與戰姬](../Page/魔彈之王與戰姬.md "wikilink")》片頭曲）／鈴木木乃美

:\# （電視動畫《[ONE PIECE](../Page/ONE_PIECE.md "wikilink")》片頭曲15）／

:\# （電視動畫《[JoJo的奇妙冒险](../Page/JoJo的奇妙冒险.md "wikilink")》片頭曲）／×橋本仁

:\# Divine love（電視動畫《Saint Beast～光陰敘事詩天使譚～》片頭曲）／×奧井雅美

:\# （電視動畫《ONE PIECE》片頭曲15）／

:\# Generation-A（Animelo Summer Live 2007 -Generation-A-主題曲）／All Cast

:\# ONENESS（Animelo Summer Live 2014 -ONENESS-主題曲）／All Cast

## 2015年

### Animelo Summer Live 2015 -THE GATE-

  - 舉辦日期：2015年8月28日－8月30日
  - 會場：[埼玉超級體育館](../Page/埼玉超級體育館.md "wikilink")
  - 主辦：MAGES.、[文化放送](../Page/文化放送.md "wikilink")
  - 協力贊助：LIVE DAM STADIUM、[Good Smile
    Company](../Page/Good_Smile_Company.md "wikilink")
  - 後援：[NBC環球娛樂](../Page/NBC環球娛樂.md "wikilink")、[KADOKAWA](../Page/KADOKAWA.md "wikilink")、[King
    Records](../Page/King_Records.md "wikilink")、[日本索尼音樂娛樂](../Page/日本索尼音樂娛樂.md "wikilink")、DIVE
    II
    entertainment、[日本哥倫比亞](../Page/日本哥倫比亞.md "wikilink")、FRAME、[武士道](../Page/武士道_\(公司\).md "wikilink")、[波麗佳音](../Page/波麗佳音.md "wikilink")、[Lantis](../Page/Lantis.md "wikilink")、Warner
    Home Video
  - 企劃：AniSummer Project實行委員會
  - 協力：埼玉超級體育館、[7-Eleven](../Page/7-Eleven.md "wikilink")、[淘兒唱片](../Page/淘兒唱片.md "wikilink")
  - 製作協力：Grand Slam

#### 主題曲

  - 「, THE GATE\!\!」
    作詞：[畑亜貴](../Page/畑亜貴.md "wikilink")，作曲、編曲：渡邊和紀

  - 主唱:
    [i☆Ris](../Page/i☆Ris.md "wikilink")、[angela](../Page/angela.md "wikilink")、[井口裕香](../Page/井口裕香.md "wikilink")、[今井麻美](../Page/今井麻美.md "wikilink")、[内田彩](../Page/内田彩.md "wikilink")、[内田真礼](../Page/内田真礼.md "wikilink")、[小野賢章](../Page/小野賢章.md "wikilink")、[GRANRODEO](../Page/GRANRODEO.md "wikilink")、[黒崎真音](../Page/黒崎真音.md "wikilink")、[昆夏美](../Page/昆夏美.md "wikilink")、[ZAQ](../Page/ZAQ.md "wikilink")、[鈴木このみ](../Page/鈴木このみ.md "wikilink")、[TRUSTRICK](../Page/TRUSTRICK.md "wikilink")、[西沢幸奏](../Page/西沢幸奏.md "wikilink")、[Pile](../Page/Pile.md "wikilink")、[春奈るな](../Page/春奈るな.md "wikilink")、[ミルキィホームズ](../Page/ミルキィホームズ.md "wikilink")、[ゆいかおり](../Page/ゆいかおり.md "wikilink")

#### 出演者

  - 8月28日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [THE
    IDOLM@STER](../Page/THE_IDOLM@STER.md "wikilink")<small>（[中村繪里子](../Page/中村繪里子.md "wikilink")、[今井麻美](../Page/今井麻美.md "wikilink")、[下田麻美](../Page/下田麻美.md "wikilink")、[淺倉杏美](../Page/淺倉杏美.md "wikilink")、[原由實](../Page/原由實.md "wikilink")、[沼倉愛美](../Page/沼倉愛美.md "wikilink")）</small>
  - [井口裕香](../Page/井口裕香.md "wikilink")
  - [King Cream
    Soda](../Page/King_Cream_Soda.md "wikilink")（**特別演出，不收錄於影碟內**）
  - [鈴木木乃美](../Page/鈴木木乃美.md "wikilink")
  - [春奈露娜](../Page/春奈露娜.md "wikilink")
  - [μ′s](../Page/LoveLive!.md "wikilink")
  - [桃色幸運草Z](../Page/桃色幸運草Z.md "wikilink")
  - [LiSA](../Page/LiSA.md "wikilink")
  - [Ray](../Page/Ray_\(歌手\).md "wikilink")
  - [AKINO](../Page/AKINO.md "wikilink") from bless4

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [オーイシマサヨシ](../Page/オーイシマサヨシ.md "wikilink")

  -
  - [野水伊織](../Page/野水伊織.md "wikilink")

  -
  - [黑崎真音](../Page/黑崎真音.md "wikilink")

  -
  -
  - [伊藤香奈子](../Page/伊藤香奈子.md "wikilink")

  - [西澤幸奏](../Page/西澤幸奏.md "wikilink")（特別嘉賓）

</div>

  - 8月29日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [THE IDOLM@STER CINDERELLA
    GIRLS](../Page/THE_IDOLM@STER_CINDERELLA_GIRLS.md "wikilink")
  - [i☆Ris](../Page/i☆Ris.md "wikilink")
  - [angela](../Page/angela.md "wikilink")
  - [川田真美](../Page/川田真美.md "wikilink")
  - [GARNiDELiA](../Page/GARNiDELiA.md "wikilink")
  - [SCREEN mode](../Page/SCREEN_mode.md "wikilink")
  - [sphere](../Page/sphere_\(聲優團體\).md "wikilink")
  - [宮野真守](../Page/宮野真守.md "wikilink")
  - [Milky Holmes](../Page/偵探歌劇_少女福爾摩斯.md "wikilink")
  - [分島花音](../Page/分島花音.md "wikilink")
  - [今井麻美](../Page/今井麻美.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [内田彩](../Page/内田彩.md "wikilink")
  - [Pile](../Page/Pile.md "wikilink")
  - [藍井艾露](../Page/藍井艾露.md "wikilink")（因病辭演）\[1\]
  - [蒼井翔太](../Page/蒼井翔太.md "wikilink")
  - [ChouCho](../Page/ChouCho.md "wikilink")
  - [七森中☆娛樂部](../Page/輕鬆百合.md "wikilink")
  - カスタマイZ
  - [3年E班](../Page/暗殺教室.md "wikilink")<small>［渚（[淵上舞](../Page/淵上舞.md "wikilink")）＆茅野（[洲崎綾](../Page/洲崎綾.md "wikilink")）＆業（[岡本信彥](../Page/岡本信彥.md "wikilink")）＆磯貝（[逢坂良太](../Page/逢坂良太.md "wikilink")）＆岡野（[田中美海](../Page/田中美海.md "wikilink")）＆杉野（[山谷祥生](../Page/山谷祥生.md "wikilink")）］</small>
  - [南條愛乃](../Page/南條愛乃.md "wikilink")
  - [fripSide](../Page/fripSide.md "wikilink")（特別嘉賓）

</div>

  - 8月30日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [綾野真白](../Page/綾野真白.md "wikilink")
  - [內田真禮](../Page/內田真禮.md "wikilink")
  - [小倉唯](../Page/小倉唯.md "wikilink")
  - [小野賢章](../Page/小野賢章.md "wikilink")
  - [GRANRODEO](../Page/GRANRODEO.md "wikilink")
  - [ZAQ](../Page/ZAQ.md "wikilink")
  - [JO☆STARS](../Page/JoJo的奇妙冒險.md "wikilink")
    〜TOMMY,Coda,JIN〜<small>（富永TOMMY弘明、Coda、橋本仁）</small>
  - [茅原實里](../Page/茅原實里.md "wikilink")
  - [fhána](../Page/fhána.md "wikilink")
  - [唯夏織](../Page/唯夏織.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [大橋彩香](../Page/大橋彩香.md "wikilink")

  - [新田恵海](../Page/新田恵海.md "wikilink")

  - [Kalafina](../Page/Kalafina.md "wikilink")

  - [てさプルん♪](../Page/摸索吧！部活劇.md "wikilink")

  - [Wake Up, Girls\!](../Page/Wake_Up,_Girls!.md "wikilink")

  - [Rhodanthe\*](../Page/黃金拼圖.md "wikilink")

  - [小松未可子](../Page/小松未可子.md "wikilink")

  - [TrySail](../Page/TrySail.md "wikilink")

  -

</div>

#### 演唱歌曲一覽

:; 8月28日

:\#
[混合曲](../Page/混合曲.md "wikilink")：READY\!\!～（電視動畫《[偶像大師](../Page/偶像大師_\(動畫\).md "wikilink")》片頭曲、電視動畫《[LoveLive\!](../Page/LoveLive!.md "wikilink")》片頭曲）／[THE
IDOLM@STER](../Page/THE_IDOLM@STER.md "wikilink")×[μ′s](../Page/LoveLive!.md "wikilink")

:\# 混合曲：Wonderful Rush～～No brand girls～GO MY
WAY\!\!（[μ's](../Page/LoveLive!.md "wikilink") 5th
Single、電視動畫《偶像大師》插曲、電視動畫《LoveLive\!》插曲、電視動畫《偶像大師》片尾曲）／THE
IDOLM@STER×μ′s

:\# （電視動畫《[妖怪手錶](../Page/妖怪手錶.md "wikilink")》片頭曲4）／[King Cream
Soda](../Page/King_Cream_Soda.md "wikilink")

:\# （電視動畫《妖怪手錶》片頭曲）／King Cream Soda

:\# （電視動畫《[GUNDAM創戰者](../Page/GUNDAM創戰者.md "wikilink")》片頭曲）／

:\# （電視動畫《[GUNDAM創戰者TRY](../Page/GUNDAM創戰者.md "wikilink")》片頭曲）／BACK-ON

:\# （電視動畫《[棺姬嘉依卡 AVENGING
BATTLE](../Page/棺姬嘉依卡.md "wikilink")》片頭曲）／[野水伊織](../Page/野水伊織.md "wikilink")

:\#
D.O.B.（電視動畫《[空戰魔導士培訓生的教官](../Page/空戰魔導士培訓生的教官.md "wikilink")》片頭曲）／野水伊織

:\# Hey
World（電視動畫《[在地下城尋求邂逅是否搞錯了什麼](../Page/在地下城尋求邂逅是否搞錯了什麼.md "wikilink")》片頭曲）／[井口裕香](../Page/井口裕香.md "wikilink")

:\# Grow Slowly（電視動畫《[科學超電磁砲S](../Page/科學超電磁砲.md "wikilink")》片尾曲）／井口裕香

:\# Rolling\!
Rolling\!（電視動畫《[蘿球社！SS](../Page/蘿球社！.md "wikilink")》片尾曲）／井口裕香×[Ray](../Page/Ray_\(歌手\).md "wikilink")

:\# ebb and
flow（電視動畫《[來自風平浪靜的明天](../Page/來自風平浪靜的明天.md "wikilink")》片頭曲2）／[Ray](../Page/Ray_\(歌手\).md "wikilink")

:\# secret
arms（電視動畫《[出包王女DARKNESS](../Page/出包王女.md "wikilink")》第2期片頭曲）／Ray

:\# （電視動畫《[一週的朋友](../Page/一週的朋友.md "wikilink")》片頭曲）／

:\# ISOtone（電視動畫《[Chaos Dragon
赤龍戰役](../Page/赤龍_\(小說\).md "wikilink")》片頭曲）／昆夏美

:\#
（電視動畫《[學園孤島](../Page/學園孤島.md "wikilink")》片尾曲）／[黑崎真音](../Page/黑崎真音.md "wikilink")

:\# （電視動畫《[灰色的樂園](../Page/灰色的樂園.md "wikilink")》片頭曲）／黑崎真音

:\#
（電視動畫《[戰姬絕唱SYMPHOGEAR](../Page/戰姬絕唱SYMPHOGEAR.md "wikilink")》插曲）／黑崎真音×

:\# FLYING FAFNIR（電視動畫《[銃皇無盡的法夫納](../Page/銃皇無盡的法夫納.md "wikilink")》片頭曲）／

:\# （電視動畫《偶像大師》插曲）／THE IDOLM@STER

:\# M@STERPIECE（劇場版《[偶像大師 劇場版
前往光輝的另一端！](../Page/偶像大師_劇場版_前往光輝的另一端！.md "wikilink")》主題曲）／THE
IDOLM@STER

:\# [MOON
PRIDE](../Page/MOON_PRIDE.md "wikilink")（電視動畫《[美少女戰士Crystal](../Page/美少女戰士Crystal.md "wikilink")》片頭曲）／[桃色幸運草Z](../Page/桃色幸運草Z.md "wikilink")

:\# （劇場版《[七龍珠Z 復活的F](../Page/七龍珠Z_復活的F.md "wikilink")》主題曲）／桃色幸運草Z

:\# CHA-LA
HEAD-CHA-LA（電視動畫《[七龍珠Z](../Page/七龍珠Z.md "wikilink")》片頭曲）／桃色幸運草Z

:\# Hacking to the
Gate（電視動畫《[命運石之門](../Page/命運石之門.md "wikilink")》片頭曲）／[伊藤香奈子](../Page/伊藤香奈子.md "wikilink")

:\# （遊戲《[STEINS;GATE 0](../Page/命運石之門.md "wikilink")》主題曲）／伊藤香奈子

:\#
（電視動畫《[月刊少女野崎同學](../Page/月刊少女野崎同學.md "wikilink")》片頭曲）／[オーイシマサヨシ](../Page/オーイシマサヨシ.md "wikilink")

:\#
（電視動畫《[不起眼女主角培育法](../Page/不起眼女主角培育法.md "wikilink")》片頭曲）／[春奈露娜](../Page/春奈露娜.md "wikilink")

:\# Startear（電視動畫《[刀劍神域Ⅱ](../Page/刀劍神域.md "wikilink")》片尾曲）／春奈露娜

:\#
（電視動畫《[魔法少女小圓](../Page/魔法少女小圓.md "wikilink")》片頭曲）／春奈露娜×[鈴木木乃美](../Page/鈴木木乃美.md "wikilink")

:\# ～CHOIR
JAIL（電視動畫《[魔彈之王與戰姬](../Page/魔彈之王與戰姬.md "wikilink")》片頭曲、電視動畫《[黃昏乙女×失憶幽靈](../Page/黃昏乙女×失憶幽靈.md "wikilink")》片頭曲）／[鈴木木乃美](../Page/鈴木木乃美.md "wikilink")

:\# Absolute Soul (THE GATE LIVE
ver.）（電視動畫《[絕對雙刃](../Page/絕對雙刃.md "wikilink")》片頭曲）／鈴木木乃美
feat. [OxT](../Page/OxT.md "wikilink")

:\# 1st Priority（電視動畫《STRATOS 4》片頭曲）／

:\# Agapē（電視動畫《[圓盤皇女](../Page/圓盤皇女.md "wikilink")》插曲）／Melocure

:\#
[海色](../Page/海色.md "wikilink")（電視動畫《[艦隊Collection](../Page/艦隊Collection.md "wikilink")》片頭曲）／[AKINO
from bless4](../Page/AKINO.md "wikilink")

:\#
[吹雪](../Page/吹雪.md "wikilink")（電視動畫《艦隊Collection》片尾曲）／[西澤幸奏](../Page/西澤幸奏.md "wikilink")

:\# （電視動畫《LoveLive\!》第2期片頭曲）／μ′s

:\# （電視動畫《LoveLive\!》第2期插曲）／μ′s

:\# [KiRa-KiRa
Sensation\!](../Page/KiRa-KiRa_Sensation!/Happy_maker!.md "wikilink")（電視動畫《LoveLive\!》第2期插曲）／μ′s

:\# Rally Go
Round（電視動畫《[偽戀：](../Page/偽戀.md "wikilink")》片頭曲）／[LiSA](../Page/LiSA.md "wikilink")

:\# No More Time Machine（電視動畫《刀劍神域Ⅱ》片尾曲2）／LiSA

:\# （電視動畫《刀劍神域Ⅱ》片尾曲3）／LiSA

:\# [Rising
Hope](../Page/Rising_Hope.md "wikilink")（電視動畫《[魔法科高中的劣等生](../Page/魔法科高中的劣等生.md "wikilink")》片頭曲）／LiSA

:\# EN「, THE GATE\!\!」（ASL2015主題曲）

:; 8月29日

:\#
（電視動畫《[魔法老師](../Page/魔法老師_\(動畫\).md "wikilink")》片頭曲）／[sphere](../Page/sphere_\(聲優團體\).md "wikilink")×[fripSide](../Page/fripSide.md "wikilink")

:\# Luminize（電視動畫《[未來卡片
戰鬥夥伴](../Page/未來卡片_戰鬥夥伴.md "wikilink")》第2期片頭曲）／[fripSide](../Page/fripSide.md "wikilink")

:\# [black
bullet](../Page/black_bullet.md "wikilink")（電視動畫《[黑色子彈](../Page/黑色子彈.md "wikilink")》片頭曲）／fripSide

:\#
（電視動畫《[星光樂園](../Page/星光樂園.md "wikilink")》第2期片頭曲）／[i☆Ris](../Page/i☆Ris.md "wikilink")

:\# 混合曲：Make it！～～Realize\!（電視動畫《星光樂園》片頭曲1、2、3）／i☆Ris

:\# （電視動畫《[偵探歌劇 少女福爾摩斯](../Page/偵探歌劇_少女福爾摩斯.md "wikilink")》片頭曲）／[Milky
Holmes](../Page/偵探歌劇_少女福爾摩斯.md "wikilink")×i☆Ris

:\#  A GO GO（電視動畫《偵探歌劇 少女福爾摩斯》第4期片頭曲）／Milky Holmes

:\# （電視動畫《[銀河騎士傳](../Page/銀河騎士傳.md "wikilink")》第2期片尾曲）／Z

:\# BLESS YoUr NAME（電視動畫《[惡魔高校D×D
BorN](../Page/惡魔高校D×D.md "wikilink")》片頭曲）／[ChouCho](../Page/ChouCho.md "wikilink")

:\# （電視動畫《[冰菓](../Page/古籍研究社系列.md "wikilink")》片頭曲）／ChouCho

:\# RIGHT LIGHT
RISE（電視動畫《[在地下城尋求邂逅是否搞錯了什麼](../Page/在地下城尋求邂逅是否搞錯了什麼.md "wikilink")》片尾曲）／[分島花音](../Page/分島花音.md "wikilink")

:\# killy killy JOKER（電視動畫《[selector infected
WIXOSS](../Page/selector_infected_WIXOSS.md "wikilink")》片頭曲）／分島花音

:\# ／[內田彩](../Page/內田彩.md "wikilink")

:\# KISEKI（電視動畫《Duel Masters VSR》片尾曲）／[Pile](../Page/Pile.md "wikilink")

:\#
（電視動畫《[灰色的樂園](../Page/灰色的樂園.md "wikilink")》片尾曲2／[南條愛乃](../Page/南條愛乃.md "wikilink")

:\# 極限Dreamer（電視動畫《[夜晚的小雙俠](../Page/夜晚的小雙俠.md "wikilink")》片頭曲）／[SCREEN
mode](../Page/SCREEN_mode.md "wikilink")

:\# （電視動畫《[黑子的籃球](../Page/黑子的籃球.md "wikilink")》第3期片尾曲）／SCREEN mode

:\# Ding\! Dong\! Ding\!
Dong\!／[sphere](../Page/sphere_\(聲優團體\).md "wikilink")

:\# 情熱CONTINUE（電視動畫《[夜晚的小雙俠](../Page/夜晚的小雙俠.md "wikilink")》片尾曲）／sphere

:\# vivid brilliant
door\!（電視動畫《[電波教師](../Page/電波教師.md "wikilink")》片頭曲2）／sphere

:\#
（電視動畫《[暗殺教室](../Page/暗殺教室.md "wikilink")》片頭曲）／[3年E班](../Page/暗殺教室.md "wikilink")（渚＆茅野＆業＆磯貝＆岡野＆杉野）

:\#
（電視動畫《[輕鬆百合](../Page/輕鬆百合.md "wikilink")》OVA片頭曲）／[七森中☆娛樂部](../Page/輕鬆百合.md "wikilink")

:\# （電視動畫《輕鬆百合》特別篇片頭曲）／七森中☆娛樂部

:\# （電視動畫《輕鬆百合》片頭曲）／七森中☆娛樂部 feat.
[馬蒂·弗里德曼](../Page/馬蒂·弗里德曼.md "wikilink")

:\# Ring of Fortune（電視動畫《[可塑性記憶](../Page/可塑性記憶.md "wikilink")》片頭曲）／佐佐木惠梨

:\# （電視動畫《可塑性記憶》片尾曲）／[今井麻美](../Page/今井麻美.md "wikilink")

:\# （電視動畫《[南家三姊妹](../Page/南家三姊妹.md "wikilink")》片頭曲）／今井麻美&内田彩＆Pile

:\# ambiguous（電視動畫《[KILL la
KILL](../Page/KILL_la_KILL.md "wikilink")》片頭曲2）／[GARNiDELiA](../Page/GARNiDELiA.md "wikilink")

:\# BLAZING（電視動畫《[高達G
Reconquista](../Page/GUNDAM_G之复国运动.md "wikilink")》片頭曲）／GARNiDELiA

:\# ／[蒼井翔太](../Page/蒼井翔太.md "wikilink")

:\#
Gardens（電視動畫《[出包王女DARKNESS](../Page/出包王女.md "wikilink")》第2期片尾曲）／[川田真美](../Page/川田真美.md "wikilink")

:\# 混合曲：～Borderland～[No
buts\!](../Page/No_buts!.md "wikilink")（電視動畫《[灼眼的夏娜](../Page/灼眼的夏娜_\(動畫\).md "wikilink")》片頭曲、電視動畫《[軍火女王](../Page/軍火女王.md "wikilink")》片頭曲、電視動畫《[魔法禁書目錄Ⅱ](../Page/魔法禁書目錄.md "wikilink")》片頭曲）／川田真美

:\#
[JOINT](../Page/JOINT.md "wikilink")（電視動畫《[灼眼的夏娜Ⅱ](../Page/灼眼的夏娜_\(動畫\).md "wikilink")》片頭曲）／川田真美

:\# （電視動畫《[歌之王子殿下
真愛革命](../Page/歌之王子殿下.md "wikilink")》片頭曲）／[宮野真守](../Page/宮野真守.md "wikilink")

:\# NEW ORDER（電視動畫《[T寶的悲慘日常
覺醒編](../Page/T寶的悲慘日常.md "wikilink")》片頭曲）／宮野真守

:\# BREAK IT\!（電視動畫《[卡片鬥爭\!\!
先導者G](../Page/卡片戰鬥先導者.md "wikilink")》片頭曲）／宮野真守

:\# （電視動畫《[偶像大師 灰姑娘女孩](../Page/偶像大師_灰姑娘女孩_\(動畫\).md "wikilink")》插曲）／[THE
IDOLM@STER CINDERELLA
GIRLS](../Page/THE_IDOLM@STER_CINDERELLA_GIRLS.md "wikilink")

:\# Star\!\!（電視動畫《偶像大師 灰姑娘女孩》片頭曲）／THE IDOLM@STER CINDERELLA GIRLS

:\# Shine\!\!（電視動畫《偶像大師 灰姑娘女孩》第2期片頭曲）／THE IDOLM@STER CINDERELLA GIRLS

:\# （電視動畫《[蒼穹之戰神
EXODUS](../Page/蒼穹之戰神.md "wikilink")》片頭曲）／[angela](../Page/angela.md "wikilink")

:\# brilliant road（電視動畫《[宇宙星路](../Page/宇宙星路.md "wikilink")》片頭曲）／angela

:\# （電視動畫《[銀河騎士傳](../Page/銀河騎士傳.md "wikilink")》片頭曲）／angela

:\# 騎士行進曲（電視動畫《銀河騎士傳》第2期片頭曲）／angela

:\# EN「, THE GATE\!\!」（ASL2015主題曲）

:; 8月30日

:\#
（電視動畫《[黑子的籃球](../Page/黑子的籃球.md "wikilink")》第2期片頭曲2）／[GRANRODEO](../Page/GRANRODEO.md "wikilink")×[小野賢章](../Page/小野賢章.md "wikilink")

:\# Stand
Up\!\!\!\!\!\!\!\!\!\!（電視動畫《[摸索吧！部活劇](../Page/摸索吧！部活劇.md "wikilink")》片頭曲）／[てさプルん♪](../Page/摸索吧！部活劇.md "wikilink")

:\# 色彩crossroad（電視動畫《摸索吧！部活劇》第3期片尾曲）／

:\# Youthful
Dreamer（電視動畫《[電波教師](../Page/電波教師.md "wikilink")》片頭曲）／[TrySail](../Page/TrySail.md "wikilink")

:\#
（電視動畫《[Classroom☆Crisis](../Page/Classroom☆Crisis.md "wikilink")》片頭曲）／TrySail

:\# Raise (album ver.)（電視動畫《[Campione
弒神者！](../Page/Campione_弒神者！.md "wikilink")》片尾曲）／[小倉唯](../Page/小倉唯.md "wikilink")

:\#
Honey♥Come\!\!（電視動畫《[城下町的蒲公英](../Page/城下町的蒲公英.md "wikilink")》片尾曲）／小倉唯

:\#
（網路動畫《[動畫心療系](../Page/漫畫心療系.md "wikilink")》片尾曲）／[內田真禮](../Page/內田真禮.md "wikilink")

:\# （電視動畫《[我，要成為雙馬尾](../Page/我，要成為雙馬尾.md "wikilink")》片頭曲）／內田真禮

:\# ／[新田恵海](../Page/新田恵海.md "wikilink")

:\# 探求Dreaming（電視動畫《[偵探歌劇 少女福爾摩斯
TD](../Page/偵探歌劇_少女福爾摩斯.md "wikilink")》片尾曲）／新田恵海

:\#
YES\!\!（電視動畫《[生存遊戲社](../Page/生存遊戲社.md "wikilink")》片頭曲）／[大橋彩香](../Page/大橋彩香.md "wikilink")

:\# ENERGY☆SMILE／大橋彩香

:\# ideal white（電視動畫《[Fate/stay night \[Unlimited Blade
Works](../Page/Fate/stay_night.md "wikilink")\]》片頭曲）／[綾野真白](../Page/綾野真白.md "wikilink")

:\#
（電視動畫《[天體運行式](../Page/天體運行式.md "wikilink")》片尾曲）／[fhána](../Page/fhána.md "wikilink")

:\# （電視動畫《[Fate/kaleid liner 魔法少女☆伊莉雅 2wei
Herz！](../Page/Fate/kaleid_liner_魔法少女☆伊莉雅.md "wikilink")》片頭曲）／fhána

:\# divine
intervention（電視動畫《[魔女的使命](../Page/魔女的使命.md "wikilink")》片頭曲）／fhána
feat. [ZAQ](../Page/ZAQ.md "wikilink")

:\#
（電視動畫《[小長門有希的消失](../Page/小長門有希的消失.md "wikilink")》片尾曲）／[茅原實里](../Page/茅原實里.md "wikilink")

:\# （電視動畫《[境界的彼方](../Page/境界的彼方.md "wikilink")》片頭曲）／茅原實里

:\# （劇場版《[境界的彼方 -I'LL BE HERE-
未來篇](../Page/境界的彼方.md "wikilink")》主題曲）／茅原實里

:\# [Our Steady
Boy](../Page/Our_Steady_Boy.md "wikilink")（電視動畫《[Kiss×sis](../Page/Kiss×sis.md "wikilink")》片尾曲）／[唯夏織](../Page/唯夏織.md "wikilink")

:\# Ring Ring Rainbow\!\!（電視動畫《城下町的蒲公英》片頭曲）／唯夏織

:\# 少女交響曲（劇場版《[Wake Up, Girls\!
青春之影](../Page/Wake_Up,_Girls!.md "wikilink")》主題曲）／[Wake Up,
Girls\!](../Page/Wake_Up,_Girls!.md "wikilink")

:\# （劇場版《[Wake Up, Girls\!
七位偶像](../Page/Wake_Up,_Girls!.md "wikilink")》主題曲）／Wake Up,
Girls\!

:\# （電視節目《Anisong Club-i》主題曲）／（新田恵海＆大橋彩香）

:\# 混合曲：Your
Voice～Jumping\!\!（電視動畫《[黃金拼圖](../Page/黃金拼圖.md "wikilink")》片尾曲、片頭曲）／[Rhodanthe\*](../Page/黃金拼圖.md "wikilink")

:\# （電視動畫《[你好！！黃金拼圖](../Page/黃金拼圖.md "wikilink")》片頭曲）／Rhodanthe\*

:\#
（電視動畫《[青春×機關槍](../Page/青春×機關槍.md "wikilink")》片尾曲）／[小松未可子](../Page/小松未可子.md "wikilink")

:\# （電視動畫《[浪客劍心](../Page/浪客劍心.md "wikilink")》片頭曲）／小松未可子×內田真禮 feat.

:\# ZERO（電視動畫《黑子的籃球》第3期片頭曲2）／[小野賢章](../Page/小野賢章.md "wikilink")

:\# Blue horizon／小野賢章

:\# Philosophy of Dear
World（電視動畫《[純潔的瑪利亞](../Page/純潔的瑪利亞.md "wikilink")》片頭曲）／[ZAQ](../Page/ZAQ.md "wikilink")

:\# Seven Doors（電視動畫《[TRINITY SEVEN
魔道書7使者](../Page/TRINITY_SEVEN_魔道書7使者.md "wikilink")》片頭曲）／ZAQ

:\# OVERDRIVER-ANISAMA Remix- feat.motsu／ZAQ feat.

:\# SURPRISE-DRIVE（特攝《[假面騎士Drive](../Page/假面騎士Drive.md "wikilink")》主題曲）／

:\# re-ray（特攝《[劇場版 假面騎士Drive SURPRISE
FUTURE](../Page/劇場版_假面騎士Drive_SURPRISE_FUTURE.md "wikilink")》主題曲）／Mitsuru
Matsuoka EARNEST DRIVE

:\# ～BLOODY STREAM～STAND
PROUD（電視動畫《[JoJo的奇妙冒險](../Page/JoJo的奇妙冒險.md "wikilink")》第1期片頭曲1、2、第2期片頭曲1）／[JO☆STARS](../Page/JoJo的奇妙冒險.md "wikilink")
～TOMMY, Coda, JIN～

:\#  ～end of THE WORLD～（電視動畫《JoJo的奇妙冒險》第2期片頭曲2）／JO☆STARS ～TOMMY, Coda,
JIN～

:\#
[believe](../Page/believe_\(Kalafina單曲\).md "wikilink")（電視動畫《[Fate/stay
night \[Unlimited Blade
Works](../Page/Fate/stay_night.md "wikilink")\]》片尾曲1）／[Kalafina](../Page/Kalafina.md "wikilink")

:\# [ring your
bell](../Page/ring_your_bell.md "wikilink")（電視動畫《Fate/stay
night \[Unlimited Blade Works\]》片尾曲2）／Kalafina

:\# [One
Light](../Page/One_Light.md "wikilink")（電視動畫《[亞爾斯蘭戰記](../Page/亞爾斯蘭戰記.md "wikilink")》片尾曲2）／Kalafina

:\# DAWN GATE／[GRANRODEO](../Page/GRANRODEO.md "wikilink")

:\# Punky Funky Love（電視動畫《黑子的籃球》第3期片頭曲1）／GRANRODEO

:\# サマーGT09／GRANRODEO

:\# （電視動畫《黑子的籃球》第3期片頭曲3）／GRANRODEO

:\# EN「, THE GATE\!\!」（ASL2015主題曲）

## 2016年

### Animelo Summer Live 2016 刻-TOKI-

  - 舉辦日期：2016年8月26日－8月28日
  - 會場：[埼玉超級體育館](../Page/埼玉超級體育館.md "wikilink")
  - 主辦：MAGES.、[文化放送](../Page/文化放送.md "wikilink")
  - 協力贊助：LIVE DAM STADIUM、[Good Smile
    Company](../Page/Good_Smile_Company.md "wikilink")
  - 後援：株式會社AT-X、[NBC環球娛樂](../Page/NBC環球娛樂.md "wikilink")、[KADOKAWA](../Page/KADOKAWA.md "wikilink")、[King
    Records](../Page/King_Records.md "wikilink")、[日本索尼音樂娛樂](../Page/日本索尼音樂娛樂.md "wikilink")、DIVE
    II entertainment、[日本哥倫比亞](../Page/日本哥倫比亞.md "wikilink")、F.I.X.
    RECORDS、[飛犬](../Page/飛犬.md "wikilink")、[波麗佳音](../Page/波麗佳音.md "wikilink")、[Lantis](../Page/Lantis.md "wikilink")、Warner
    Home Video
  - 企劃：AniSummer Project實行委員會
  - 協力：埼玉超級體育館、[7-Eleven](../Page/7-Eleven.md "wikilink")、[淘兒唱片](../Page/淘兒唱片.md "wikilink")、Pia
  - 製作協力：Grand Slam

#### 主題曲

  - 「PASSION RIDERS」
    作詞：[畑亜貴](../Page/畑亜貴.md "wikilink")，作曲、編曲：[Q-MHz](../Page/Q-MHz.md "wikilink")
    主唱：[相坂優歌](../Page/相坂優歌.md "wikilink")、[藍井艾露](../Page/藍井艾露.md "wikilink")、[蒼井翔太](../Page/蒼井翔太.md "wikilink")、[every♥ing！](../Page/Every♥ing!.md "wikilink")、[大橋彩香](../Page/大橋彩香.md "wikilink")、[黑崎真音](../Page/黑崎真音.md "wikilink")、[GRANRODEO](../Page/GRANRODEO.md "wikilink")、[SCREEN
    mode](../Page/SCREEN_mode.md "wikilink")、[鈴木木乃美](../Page/鈴木木乃美.md "wikilink")、[早見沙織](../Page/早見沙織.md "wikilink")、[三森鈴子](../Page/三森鈴子.md "wikilink")、[LiSA](../Page/LiSA.md "wikilink")

#### 出演者

  - 8月26日\[2\]

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [相坂優歌](../Page/相坂優歌.md "wikilink")
  - [i☆Ris](../Page/i☆Ris.md "wikilink")
  - [藍井艾露](../Page/藍井艾露.md "wikilink")（辭演）\[3\]
  - [AKINO](../Page/AKINO.md "wikilink") with
    [bless4](../Page/bless4.md "wikilink")
  - [every♥ing\!](../Page/every♥ing!.md "wikilink")
  - [GRANRODEO](../Page/GRANRODEO.md "wikilink")
  - [ZAQ](../Page/ZAQ.md "wikilink")
  - [Suara](../Page/Suara.md "wikilink")
  - [田所梓](../Page/田所梓.md "wikilink")
  - [村川梨衣](../Page/村川梨衣.md "wikilink")
  - [May'n](../Page/May'n.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [小暮閣下](../Page/小暮閣下.md "wikilink")

  - 山本陽介

  - [井口裕香](../Page/井口裕香.md "wikilink")

  - [玉置成實](../Page/玉置成實.md "wikilink")

  - [ALTIMA](../Page/ALTIMA.md "wikilink")

  - [IDOLM@STER CINDERELLA GIRLS](../Page/偶像大師_灰姑娘女孩.md "wikilink")

  -
  -
  - [KOTOKO](../Page/KOTOKO.md "wikilink")

  - [春奈露娜](../Page/春奈露娜.md "wikilink")\[4\]

</div>

  - 8月27日\[5\]

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [內田真禮](../Page/內田真禮.md "wikilink")

  -
  - [大橋彩香](../Page/大橋彩香.md "wikilink")

  - [SCREEN mode](../Page/SCREEN_mode.md "wikilink")

  - [鈴木木乃美](../Page/鈴木木乃美.md "wikilink")

  - [高垣彩陽](../Page/高垣彩陽.md "wikilink")

  - [竹達彩奈](../Page/竹達彩奈.md "wikilink")

  - [早見沙織](../Page/早見沙織.md "wikilink")

  - [fhána](../Page/fhána.md "wikilink")

  - [Plasmagica](../Page/SHOW_BY_ROCK!!.md "wikilink")

  - [LiSA](../Page/LiSA.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [北宇治四重奏](../Page/吹響吧！上低音號～歡迎加入北宇治高中管樂團～.md "wikilink")
  - [TRUE](../Page/唐澤美帆.md "wikilink")
  - [Earphones](../Page/那就是聲優！.md "wikilink")
  - [FLOW](../Page/FLOW_\(樂團\).md "wikilink")
  - [Poppin' Party from BanG
    Dream\!](../Page/Poppin'_Party_from_BanG_Dream!.md "wikilink")
  - [OxT](../Page/OxT.md "wikilink")
  - [MICHI](../Page/MICHI.md "wikilink")
  - [Lia](../Page/Lia.md "wikilink")
  - [B.B.QUEENS](../Page/B.B.QUEENS.md "wikilink")

</div>

  - 8月28日\[6\]

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [蒼井翔太](../Page/蒼井翔太.md "wikilink")
  - [ALI PROJECT](../Page/ALI_PROJECT.md "wikilink")
  - [angela](../Page/angela.md "wikilink")
  - [小倉唯](../Page/小倉唯.md "wikilink")
  - [小野賢章](../Page/小野賢章.md "wikilink")
  - [黑崎真音](../Page/黑崎真音.md "wikilink")
  - [戶松遙](../Page/戶松遙.md "wikilink")
  - [TrySail](../Page/TrySail.md "wikilink")
  - [Minami](../Page/Minami.md "wikilink")
  - [三森鈴子](../Page/三森鈴子.md "wikilink")
  - [唯夏織](../Page/唯夏織.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [Pile](../Page/Pile.md "wikilink")
  - [Petit Milady](../Page/Petit_Milady.md "wikilink")
  - [地球防衛部](../Page/美男高校地球防衛部LOVE！.md "wikilink")
  - [西澤幸奏](../Page/西澤幸奏.md "wikilink")
  - [飯田-{里}-穂](../Page/飯田里穂.md "wikilink")
  - THE DU
  - [nano](../Page/奈米_\(歌手\).md "wikilink")
  - [筋肉少女帶](../Page/筋肉少女帶.md "wikilink")
  - batta
  - [沼倉愛美](../Page/沼倉愛美.md "wikilink")

</div>

#### 演唱歌曲一覽

:; 8月26日\[7\]

:\#
（電視動畫《[北斗神拳](../Page/北斗神拳.md "wikilink")》片頭曲）／[小暮閣下](../Page/小暮閣下.md "wikilink")×[GRANRODEO](../Page/GRANRODEO.md "wikilink")

:\# TRASH CANDY（電視動畫《[文豪Stray
Dogs](../Page/文豪Stray_Dogs.md "wikilink")》片頭曲）／GRANRODEO

:\# （電視劇《[甜心戰士 THE
LIVE](../Page/甜心戰士_THE_LIVE.md "wikilink")》插曲）／GRANRODEO

:\# Ripple
Effect（電視動畫《[高校艦隊](../Page/高校艦隊.md "wikilink")》片尾曲）／[春奈露娜](../Page/春奈露娜.md "wikilink")

:\#
[Overfly](../Page/Overfly.md "wikilink")（電視動畫《[刀劍神域](../Page/刀劍神域.md "wikilink")》片尾曲2）／春奈露娜

:\# Shining Star-☆-LOVE
Letter（劇場版動畫《[魔法禁書目錄：恩底彌翁的奇蹟](../Page/劇場版魔法禁書目錄：恩底彌翁的奇蹟.md "wikilink")》印象曲）／[井口裕香](../Page/井口裕香.md "wikilink")

:\# Lostorage（電視動畫《[Lostorage incited
WIXOSS](../Page/Lostorage_incited_WIXOSS.md "wikilink")》片頭曲）／井口裕香

:\#
（電視動畫《[靈感少女](../Page/靈感少女.md "wikilink")》片頭曲）／[every♥ing\!](../Page/every♥ing!.md "wikilink")

:\# ／every♥ing\!

:\# （電視動畫《[Active Raid－機動強襲室第八係－
2nd](../Page/Active_Raid－機動強襲室第八係－.md "wikilink")》片頭曲）／[相坂優歌](../Page/相坂優歌.md "wikilink")

:\#
純真Always（電視動畫《[無彩限的幻影世界](../Page/無彩限的幻影世界.md "wikilink")》片尾曲）／[田所梓](../Page/田所梓.md "wikilink")

:\#
（電視動畫《[冒險少女娜汀亞](../Page/冒險少女娜汀亞.md "wikilink")》片頭曲）／田所梓×[i☆Ris](../Page/i☆Ris.md "wikilink")

:\# Ready Smile\!\!（電視動畫《[星光樂園](../Page/星光樂園.md "wikilink")》片頭曲7）／i☆Ris

:\# Re：Call（電視動畫《[雙星之陰陽師](../Page/雙星之陰陽師.md "wikilink")》片頭曲2）／i☆Ris

:\#
[混合曲](../Page/混合曲.md "wikilink")：～OK\!～～～（電視動畫《[寵物小精靈](../Page/寵物小精靈_\(1997-2002年動畫\).md "wikilink")》片頭曲1、片頭曲3、片頭曲2、片尾曲5、片頭曲1）／[松本梨香](../Page/松本梨香.md "wikilink")
with [bless4](../Page/bless4.md "wikilink") feat. i☆Ris

:\# innocent promise（電視動畫《[少年女僕](../Page/少年女僕.md "wikilink")》片頭曲）／

:\# Recall THE END（電視動畫《[槍彈辯駁3 －The End of 希望峰學園－
未來篇](../Page/槍彈辯駁3_－The_End_of_希望峰學園－.md "wikilink")》片尾曲）／TRUSTRICK

:\# DEAD OR LIE（電視動畫《槍彈辯駁3 －The End of 希望峰學園－
未來篇》片頭曲）／[黑崎真音](../Page/黑崎真音.md "wikilink")
feat. TRUSTRICK

:\# [Believe](../Page/Believe_\(玉置成實單曲\).md "wikilink")（電視動畫《[機動戰士GUNDAM
SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")》片頭曲3）／[玉置成實](../Page/玉置成實.md "wikilink")

:\# Belief（電視動畫《[TABOO
TATTOO－禁忌咒紋－](../Page/TABOO_TATTOO－禁忌咒紋－.md "wikilink")》片頭曲）／[May'n](../Page/May'n.md "wikilink")

:\# （電視動畫《[創聖大天使LOGOS](../Page/Aquarion_Logos.md "wikilink")》片頭曲）／May'n

:\#
混合曲：（電視動畫《[超時空要塞Frontier](../Page/超時空要塞Frontier.md "wikilink")》片尾曲、劇場版動畫《》插曲、電視動畫《超時空要塞Frontier》插曲、片頭曲）／May'n

:\# PLANET / THE
HELL（電視動畫《[火星異種](../Page/火星異種.md "wikilink")》第2期片頭曲2）／小暮閣下

:\# Red Zone（電視動畫《火星異種》第2期片尾曲）／

:\# The Beginning（電視動畫《[Concrete
Revolutio～超人幻想～](../Page/Concrete_Revolutio～超人幻想～.md "wikilink")》片尾曲）／山本陽介

:\# （電視動畫《Concrete Revolutio～超人幻想～》片頭曲）／[ZAQ](../Page/ZAQ.md "wikilink")
feat. 山本陽介

:\# （電視動畫《[Concrete Revolutio～超人幻想～The Last
Song](../Page/Concrete_Revolutio～超人幻想～.md "wikilink")》片頭曲）／ZAQ feat.
山本陽介

:\# hopeness（電視動畫《[紅殼的潘朵拉](../Page/紅殼的潘朵拉.md "wikilink")》片頭曲）／ZAQ

:\# ALL-WAYS（電視動畫《Concrete Revolutio～超人幻想～The Last Song》片尾曲2）／山本陽介 feat.
玉置成實

:\# Sweet
Sensation（電視動畫《[12歲。～小小胸口的怦然心動～](../Page/12歲。.md "wikilink")》片頭曲）／[村川梨衣](../Page/村川梨衣.md "wikilink")

:\# INSIDE
IDENTITY（電視動畫《[中二病也想談戀愛！](../Page/中二病也想談戀愛！.md "wikilink")》片尾曲）／村川梨衣×相坂優歌

:\# （電視動畫《[傳頌之物
虛偽的假面](../Page/傳頌之物_虛偽的假面.md "wikilink")》片頭曲）／[Suara](../Page/Suara.md "wikilink")

:\# （電視動畫《[傳頌之物](../Page/傳頌之物.md "wikilink")》片尾曲）／Suara

:\#
（電視動畫《[甘城輝煌樂園救世主](../Page/甘城輝煌樂園救世主.md "wikilink")》片頭曲）／[AKINO](../Page/AKINO.md "wikilink")
with bless4

:\# Golden Life（電視動畫《[Active
Raid－機動強襲室第八係－](../Page/Active_Raid－機動強襲室第八係－.md "wikilink")》片頭曲）／AKINO
with bless4

:\# 混合曲：Genesis of
Aquarion～[創聖大天使](../Page/創聖大天使_\(單曲\).md "wikilink")（電視動畫《[創聖大天使](../Page/創聖大天使.md "wikilink")》片頭曲）／AKINO
with bless4

:\#
→unfinished→（電視動畫《[加速世界](../Page/加速世界.md "wikilink")》片尾曲）／[KOTOKO](../Page/KOTOKO.md "wikilink")

:\# Plasmic Fire（劇場版動畫《[加速世界 INFINITE
BURST](../Page/加速世界.md "wikilink")》主題曲）／KOTOKO×[ALTIMA](../Page/ALTIMA.md "wikilink")

:\# Burst The Gravity（電視動畫《加速世界》片頭曲2）／ALTIMA

:\# CYBER CYBER／ALTIMA

:\# M@GIC☆（電視動畫《[偶像大師
灰姑娘女孩](../Page/偶像大師_灰姑娘女孩_\(動畫\).md "wikilink")》插曲）／[IDOLM@STER
CINDERELLA GIRLS](../Page/偶像大師_灰姑娘女孩.md "wikilink")

:\# 混合曲：～ØωØver\!\!～Tulip～Trancing Pulse～S(mile)ING\!（電視動畫《偶像大師
灰姑娘女孩》插曲）／IDOLM@STER CINDERELLA GIRLS

:\# GOIN'\!\!\!（電視動畫《偶像大師 灰姑娘女孩》插曲）／IDOLM@STER CINDERELLA GIRLS

:\# （電視動畫《偶像大師 灰姑娘女孩》插曲）／IDOLM@STER CINDERELLA GIRLS

:\# EN「PASSION RIDERS」（ASL2016主題曲）

:; 8月27日\[8\]

:\# [混合曲](../Page/混合曲.md "wikilink")：We Are
B.B.～[おどるポンポコリン](../Page/大家來跳舞.md "wikilink")／（－、電視動畫《[櫻桃小丸子](../Page/櫻桃小丸子.md "wikilink")》片头曲）／B.B.QUEENS
with

:\# （電視動畫《[小松先生](../Page/小松先生.md "wikilink")》片頭曲1）／A應P

:\# （電視動畫《小松先生》片頭曲2）／A應P

:\# Yes\! BanG_Dream\!／[Poppin' Party from BanG
Dream\!](../Page/Poppin'_Party_from_BanG_Dream!.md "wikilink")

:\# STARBEAT\!～～／|Poppin' Party from BanG Dream\!

:\# DREAM
SOLISTER（電視動畫《[吹響吧！上低音號](../Page/奏響吧！上低音號～歡迎加入北宇治高中管樂團～.md "wikilink")》片頭曲）／[TRUE](../Page/唐澤美帆.md "wikilink")

:\#
（電視動畫《吹響吧！上低音號》片尾曲）／[北宇治四重奏](../Page/吹響吧！上低音號～歡迎加入北宇治高中管樂團～.md "wikilink")

:\# Bravely
You（電視動畫《[Charlotte](../Page/Charlotte_\(動畫\).md "wikilink")》片頭曲）／[Lia](../Page/Lia.md "wikilink")

:\#
[鳥の詩](../Page/鳥之詩.md "wikilink")（電視動畫《[AIR](../Page/AIR_\(遊戲\).md "wikilink")》片頭曲）／Lia

:\#
[やさしい希望](../Page/溫柔的希望.md "wikilink")（電視動畫《[赤髮白雪姬](../Page/赤髮白雪姬.md "wikilink")》片頭曲）／[早見沙織](../Page/早見沙織.md "wikilink")

:\# ／早見沙織

:\#
Checkmate\!?（電視動畫《[粗點心戰爭](../Page/粗點心戰爭.md "wikilink")》片頭曲）／[MICHI](../Page/MICHI.md "wikilink")

:\# Hey\!Queen（電視動畫《粗點心戰爭》片尾曲）／[竹達彩奈](../Page/竹達彩奈.md "wikilink")

:\# ／竹達彩奈

:\# （電視動畫《[SHOW BY
ROCK\!\!](../Page/SHOW_BY_ROCK!!.md "wikilink")》插曲）／[Plasmagica](../Page/SHOW_BY_ROCK!!.md "wikilink")

:\# Non-Stop！（電視動畫《SHOW BY ROCK\!\!》片頭曲）／Plasmagica

:\#
（電視動畫《[那就是聲優！](../Page/那就是聲優！.md "wikilink")》片頭曲）／[Earphones](../Page/那就是聲優！.md "wikilink")

:\#
混合曲：～（電視動畫《那就是聲優！》片尾曲、電視動畫《[美少女戰士R](../Page/美少女戰士R.md "wikilink")》片尾曲）／Earphones
with [石田燿子](../Page/石田燿子.md "wikilink") feat.

:\# Steppin'out（電視動畫《[無頭騎士異聞錄
DuRaRaRa\!\!×2](../Page/無頭騎士異聞錄_DuRaRaRa!!.md "wikilink")》片頭曲3）／[FLOW](../Page/FLOW_\(樂團\).md "wikilink")

:\# BURN（遊戲《[緋夜傳奇](../Page/緋夜傳奇.md "wikilink")》主題曲）／FLOW

:\# （電視動畫《[熱情傳奇](../Page/熱情傳奇.md "wikilink")》片頭曲）／FLOW

:\# GO\!\!\!（電視動畫《[火影忍者](../Page/火影忍者.md "wikilink")》片頭曲4）／FLOW

:\#
（電視動畫《[春&夏事件簿](../Page/春&夏事件簿.md "wikilink")》片頭曲）／[fhána](../Page/fhána.md "wikilink")

:\# calling（電視動畫《熱情傳奇》片尾曲）／fhána

:\# [時を刻む唄](../Page/刻印時間之歌/TORCH.md "wikilink")（電視動畫《[CLANNAD ～AFTER
STORY～](../Page/CLANNAD.md "wikilink")》片頭曲）／Lia×fhána

:\#
（電視動畫《[彗星·路西法](../Page/彗星·路西法.md "wikilink")》片尾曲1）／[大橋彩香](../Page/大橋彩香.md "wikilink")

:\# （電視動畫《彗星·路西法》片尾曲3）／大橋彩香

:\# Rebirth-day（電視動畫《[戰姬絕唱SYMPHOGEAR
GX](../Page/戰姬絕唱SYMPHOGEAR.md "wikilink")》片尾曲）／[高垣彩陽](../Page/高垣彩陽.md "wikilink")

:\# Komm, süsser Tod〜（劇場版動畫《[新世紀福音戰士劇場版：THE END OF
EVANGELION](../Page/新世紀福音戰士劇場版：THE_END_OF_EVANGELION.md "wikilink")》主題曲）／高垣彩陽×早見沙織
feat. 佐藤純一（fhána）

:\# Naked
Dive（電視動畫《[無彩限的幻影世界](../Page/無彩限的幻影世界.md "wikilink")》片頭曲）／[SCREEN
mode](../Page/SCREEN_mode.md "wikilink")

:\# ROUGH DIAMONDS（電視動畫《[食戟之靈
貳之皿](../Page/食戟之靈.md "wikilink")》片頭曲）／SCREEN
mode

:\# Divine Spell（電視動畫《[Regalia
三聖星](../Page/Regalia_三聖星.md "wikilink")》片頭曲）／TRUE

:\#
Clattanoia（電視動畫《[Overlord](../Page/OVERLORD_\(小說\).md "wikilink")》片頭曲）／[OxT](../Page/OxT.md "wikilink")

:\# STRIDER'S HIGH（電視動畫《[疾走王子](../Page/疾走王子.md "wikilink")》片頭曲）／OxT

:\# 混合曲：～Resonant
Heart（電視動畫《[惡魔的謎語](../Page/惡魔的謎語.md "wikilink")》片頭曲、電視動畫《[聖戰刻耳柏洛斯](../Page/聖戰刻耳柏洛斯.md "wikilink")》片頭曲）／[內田真禮](../Page/內田真禮.md "wikilink")

:\# （電視動畫《[我，要成為雙馬尾](../Page/我，要成為雙馬尾.md "wikilink")》片頭曲）／內田真禮

:\# Beat your
Heart（電視動畫《[舞武器·舞亂伎](../Page/舞武器·舞亂伎.md "wikilink")》片頭曲）／[鈴木木乃美](../Page/鈴木KONOMI.md "wikilink")

:\# Love is MY RAIL（電視動畫《[聖潔天使](../Page/聖潔天使.md "wikilink")》片頭曲）／鈴木木乃美

:\#
Redo（電視動畫《[Re:從零開始的異世界生活](../Page/Re:從零開始的異世界生活.md "wikilink")》片頭曲）／鈴木木乃美

:\# [Crow Song](../Page/Crow_Song.md "wikilink")（電視動畫《[Angel
Beats\!](../Page/Angel_Beats!.md "wikilink")》插曲）／[Girls Dead
Monster](../Page/Girls_Dead_Monster.md "wikilink")

:\# Brave Freak Out（電視動畫《[Qualidea
Code](../Page/Qualidea_Code.md "wikilink")》片頭曲）／[LiSA](../Page/LiSA.md "wikilink")

:\# [oath
sign](../Page/oath_sign.md "wikilink")（電視動畫《[Fate/Zero](../Page/Fate/Zero.md "wikilink")》片頭曲）／LiSA

:\#
[シルシ](../Page/印記_\(LiSA單曲\).md "wikilink")（電視動畫《[刀劍神域Ⅱ](../Page/刀劍神域.md "wikilink")》片尾曲3）／LiSA

:\# EN「PASSION RIDERS」（ASL2016主題曲）

:; 8月28日\[9\]

:\#
（電視動畫《[潮與虎](../Page/潮與虎.md "wikilink")》片頭曲）／[筋肉少女帶](../Page/筋肉少女帶.md "wikilink")×[angela](../Page/angela.md "wikilink")

:\# （電視動畫《潮與虎》片頭曲2）／筋肉少女帶

:\# ☆Fallin'
LOVE☆（電視動畫《[美男高校地球防衛部LOVE！](../Page/美男高校地球防衛部LOVE！.md "wikilink")》片頭曲）／[地球防衛部](../Page/美男高校地球防衛部LOVE！.md "wikilink")

:\# ☆LOVE IS
POWER☆（電視動畫《[美男高校地球防衛部LOVE！LOVE！](../Page/美男高校地球防衛部LOVE！.md "wikilink")》片頭曲）／地球防衛部

:\#
Honey♥Come\!\!（電視動畫《[城下町的蒲公英](../Page/城下町的蒲公英.md "wikilink")》片尾曲）／[小倉唯](../Page/小倉唯.md "wikilink")

:\# （電視動畫《[卡片鬥爭\!\! 先導者G
超越之門篇](../Page/卡片鬥爭!!_先導者.md "wikilink")》片尾曲）／小倉唯

:\#
[courage](../Page/courage.md "wikilink")（電視動畫《[刀劍神域Ⅱ](../Page/刀劍神域.md "wikilink")》片頭曲2）／[戶松遙](../Page/戶松遙.md "wikilink")

:\# （電視動畫《[鄰座的怪同學](../Page/鄰座的怪同學.md "wikilink")》片頭曲）／戶松遙

:\# ／[飯田-{里}-穂](../Page/飯田里穂.md "wikilink")

:\#
Melody（電視動畫《[境界之輪迴](../Page/境界之輪迴.md "wikilink")》片頭曲3）／[Pile](../Page/Pile.md "wikilink")

:\#
[混合曲](../Page/混合曲.md "wikilink")：（電視動畫《[展開騎士](../Page/展開騎士.md "wikilink")》片尾曲、電視動畫《[境界觸發者](../Page/境界觸發者.md "wikilink")》主題曲3）／Pile

:\# Light for
Knight（電視動畫《[槍與面具](../Page/槍與面具.md "wikilink")》片頭曲）／[三森鈴子](../Page/三森鈴子.md "wikilink")

:\# ユニバーページ（電視動畫《[萌萌侵略者 OUTBREAK
COMPANY](../Page/萌萌侵略者_OUTBREAK_COMPANY.md "wikilink")》片頭曲）／三森鈴子

:\# （電視動畫《[麵包與和平！](../Page/麵包與和平！.md "wikilink")》片頭曲）／[petit
milady](../Page/petit_milady.md "wikilink")

:\# azurite（電視動畫《[獻給某飛行員的戀歌](../Page/獻給某飛行員的戀歌.md "wikilink")》片頭曲）／petit
milady

:\#
[もってけ\!セーラーふく](../Page/拿去吧！水手服.md "wikilink")（電視動畫《[幸運☆星](../Page/幸運☆星.md "wikilink")》片頭曲）／[唯夏織](../Page/唯夏織.md "wikilink")×petit
milady

:\#
X-encounter（電視動畫《[東京闇鴉](../Page/東京闇鴉.md "wikilink")》片頭曲）／[黑崎真音](../Page/黑崎真音.md "wikilink")

:\# （電視動畫《[灰色的果實](../Page/灰色的果實.md "wikilink")》片頭曲）／黑崎真音

:\# Z・（電視動畫《[機動戰士Z GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")》片頭曲）／

:\# Patria（電視動畫《[Regalia
三聖星](../Page/Regalia_三聖星.md "wikilink")》片尾曲）／[Minami](../Page/Minami.md "wikilink")

:\# （電視動畫《[刀語](../Page/刀語.md "wikilink")》片尾曲）／Minami

:\# chase（電視動畫《[JoJo的奇妙冒險
不滅鑽石](../Page/JoJo的奇妙冒險_\(電視動畫\).md "wikilink")》片頭曲3）／batta

:\# Crazy Noisy Bizarre Town（電視動畫《JoJo的奇妙冒險 不滅鑽石》片頭曲1）／THE DU

:\# Ring Ring Rainbow\!\!（電視動畫《城下町的蒲公英》片頭曲）／唯夏織

:\# Promise You\!\!（電視動畫《[卡片鬥爭\!\! 先導者G
GIRS危機篇](../Page/卡片鬥爭!!_先導者.md "wikilink")》片尾曲2）／唯夏織

:\#
whiz（網絡動畫《[曆物語](../Page/曆物語.md "wikilink")》片尾曲）／[TrySail](../Page/TrySail.md "wikilink")

:\# High Free
Spirits（電視動畫《[高校艦隊](../Page/高校艦隊.md "wikilink")》片頭曲）／TrySail

:\# Brand-new
World（電視動畫《[學戰都市Asterisk](../Page/學戰都市Asterisk.md "wikilink")》片頭曲）／[西澤幸奏](../Page/西澤幸奏.md "wikilink")

:\# The Asterisk War（電視動畫《學戰都市Asterisk》第二期片頭曲）／西澤幸奏

:\# Night Drivin'／[小野賢章](../Page/小野賢章.md "wikilink")

:\# STORY（電視動畫《[幸運邏輯](../Page/幸運邏輯.md "wikilink")》片頭曲）／小野賢章

:\#
（電視動畫《[魔法少女育成計劃](../Page/魔法少女育成計劃.md "wikilink")》片頭曲）／[沼倉愛美](../Page/沼倉愛美.md "wikilink")

:\# Believe（電視動畫《[ONE
PIECE](../Page/ONE_PIECE_\(動畫\).md "wikilink")》片頭曲2）／沼倉愛美×三森鈴子

:\# SAVIOR OF
SONG（電視動畫《[蒼藍鋼鐵戰艦](../Page/蒼藍鋼鐵戰艦.md "wikilink")》片頭曲）／[nano](../Page/奈米_\(歌手\).md "wikilink")

:\# Rock on.（劇場版動畫《[蒼藍鋼鐵戰艦 -ARS NOVA-
DC](../Page/蒼藍鋼鐵戰艦.md "wikilink")》主題曲）／nano

:\#
（電視動畫《[夢幻之星Online2](../Page/夢幻之星在線2.md "wikilink")》片頭曲）／[蒼井翔太](../Page/蒼井翔太.md "wikilink")

:\# （電視動畫《[初戀怪獸](../Page/初戀怪獸.md "wikilink")》片頭曲）／蒼井翔太

:\# （電視動畫《[落第騎士英雄譚](../Page/落第騎士英雄譚.md "wikilink")》片尾曲）／[ALI
PROJECT](../Page/ALI_PROJECT.md "wikilink")

:\# GEMINI（電視動畫《[聖劍使的禁咒詠唱](../Page/聖劍使的禁咒詠唱.md "wikilink")》片頭曲）／ALI
PROJECT feat. petit milady

:\# （電視動畫《》片尾曲）／ALI PROJECT

:\# DEAD OR ALIVE（電視動畫《[蒼穹之戰神
EXODUS](../Page/蒼穹之戰神.md "wikilink")》片頭曲2）／angela

:\# 混合曲：KINGS～～Peace of
mind～Shangri-La～騎士行進曲（電視動畫《[K](../Page/K_\(動畫\).md "wikilink")》片頭曲、電視動畫《[妄想學生會](../Page/妄想學生會.md "wikilink")》片尾曲、電視動畫《[蒼穹之戰神－Right
of
Left](../Page/蒼穹之戰神.md "wikilink")》片尾曲、電視動畫《[蒼穹之戰神](../Page/蒼穹之戰神.md "wikilink")》片頭曲、電視動畫《[銀河騎士傳
第九行星戰役](../Page/銀河騎士傳.md "wikilink")》片頭曲）／angela

:\# （電視動畫《[銀河騎士傳](../Page/銀河騎士傳.md "wikilink")》片頭曲）／angela

:\# EN「PASSION RIDERS」（ASL2016主題曲）

## 2017年

### Animelo Summer Live 2017 -THE CARD-

  - 舉辦日期：2017年8月25日－8月27日
  - 會場：[埼玉超級體育館](../Page/埼玉超級體育館.md "wikilink")
  - 主辦：MAGES.、[文化放送](../Page/文化放送.md "wikilink")
  - 協力贊助：ANiUTa、[Good Smile
    Company](../Page/Good_Smile_Company.md "wikilink")
  - 後援：[NBC環球娛樂](../Page/NBC環球娛樂.md "wikilink")、[KADOKAWA](../Page/KADOKAWA.md "wikilink")、[King
    Records](../Page/King_Records.md "wikilink")、[GloryHeaven](../Page/Lantis.md "wikilink")、[日本索尼音樂娛樂](../Page/日本索尼音樂娛樂.md "wikilink")、DIVE
    II entertainment、[日本哥倫比亞](../Page/日本哥倫比亞.md "wikilink")、[Flying
    DOG](../Page/Flying_DOG.md "wikilink")、[武士道](../Page/武士道_\(公司\).md "wikilink")、[波麗佳音](../Page/波麗佳音.md "wikilink")、[Lantis](../Page/Lantis.md "wikilink")、Rokkan
    Music
  - 企劃：AniSummer Project實行委員會
  - 協力：埼玉超級體育館、[7-Eleven](../Page/7-Eleven.md "wikilink")、[淘兒唱片](../Page/淘兒唱片.md "wikilink")、PIA
  - 製作協力：Grand Slam

#### 主題曲

  - 「Playing The World」
    作詞、作曲：[ZAQ](../Page/ZAQ.md "wikilink")，編曲：ZAQ、EFFY
    主唱：[KISHOW](../Page/GRANRODEO.md "wikilink")、[OxT](../Page/OxT.md "wikilink")、DRAMATIC
    STARS、[Minami](../Page/Minami.md "wikilink")、[三森鈴子](../Page/三森鈴子.md "wikilink")、[鈴木このみ](../Page/鈴木このみ.md "wikilink")、[春奈るな](../Page/春奈るな.md "wikilink")、[Machico](../Page/Machico.md "wikilink")、[TRUE](../Page/唐澤美帆.md "wikilink")、[茅原實里](../Page/茅原實里.md "wikilink")、[南條愛乃](../Page/南條愛乃.md "wikilink")、[中島愛](../Page/中島愛.md "wikilink")、[Pyxis](../Page/Pyxis.md "wikilink")、[羽多野渉](../Page/羽多野渉.md "wikilink")、[早見沙織](../Page/早見沙織.md "wikilink")

#### 出演者

  - 8月25日\[10\]

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [Aqours](../Page/Aqours.md "wikilink")
  - [SOS團](../Page/涼宮春日系列角色列表#SOS團.md "wikilink")
  - [大橋彩香](../Page/大橋彩香.md "wikilink")
  - [OxT](../Page/OxT.md "wikilink")
  - [ClariS](../Page/ClariS.md "wikilink")
  - [GRANRODEO](../Page/GRANRODEO.md "wikilink")
  - [动物朋友](../Page/动物朋友.md "wikilink")
  - [鈴木KONOMI](../Page/鈴木KONOMI.md "wikilink")
  - [田村由香里](../Page/田村由香里.md "wikilink")
  - [茅原實里](../Page/茅原實里.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [TRUE](../Page/唐澤美帆.md "wikilink")
  - [西澤幸奏](../Page/西澤幸奏.md "wikilink")
  - [早見沙織](../Page/早見沙織.md "wikilink")
  - [Pyxis](../Page/Pyxis.md "wikilink")
  - [FLOW](../Page/FLOW_\(樂團\).md "wikilink")
  - [Machico](../Page/Machico.md "wikilink")
  - [Minami](../Page/Minami.md "wikilink")
  - [motsu](../Page/motsu.md "wikilink")
  - [Roselia from BanG
    Dream\!](../Page/BanG_Dream!#Roselia.md "wikilink")

</div>

  - 8月26日\[11\]

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [IDOLM@STER MILLIONSTARS](../Page/偶像大師_百萬人演唱會！.md "wikilink")
  - [i☆Ris](../Page/i☆Ris.md "wikilink")
  - [蒼井翔太](../Page/蒼井翔太.md "wikilink")
  - [angela](../Page/angela.md "wikilink")
  - [KiraKira☆光之美少女 A La
    Mode](../Page/KiraKira☆光之美少女_A_La_Mode.md "wikilink")
    <small>（[駒形友梨](../Page/駒形友梨.md "wikilink")、[宮本佳那子](../Page/宮本佳那子.md "wikilink")、[美山加恋](../Page/美山加恋.md "wikilink")、[福原遥](../Page/福原遥.md "wikilink")、[村中知](../Page/村中知.md "wikilink")、[藤田咲](../Page/藤田咲.md "wikilink")、[森奈奈子](../Page/森奈奈子_\(聲優\).md "wikilink")、[水瀨祈](../Page/水瀨祈.md "wikilink")）</small>
  - [KING OF PRISM](../Page/星光少男_KING_OF_PRISM.md "wikilink")
  - [鈴村健一](../Page/鈴村健一.md "wikilink")
  - [sphere](../Page/sphere_\(聲優團體\).md "wikilink")
  - 茅原實里
  - DJ KOO

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [東山奈央](../Page/東山奈央.md "wikilink")
  - [中島愛](../Page/中島愛.md "wikilink")
  - [羽多野涉](../Page/羽多野涉.md "wikilink")
  - [冰川清志](../Page/冰川清志.md "wikilink")
  - [fhána](../Page/fhána.md "wikilink")
  - [fripSide](../Page/fripSide.md "wikilink")
  - [三森鈴子](../Page/三森鈴子.md "wikilink")
  - [Luce Twinkle Wink☆](../Page/Luce_Twinkle_Wink☆.md "wikilink")

</div>

  - 8月27日\[12\]

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [IDOLM@STER SideM](../Page/偶像大師SideM.md "wikilink")
  - [Wake Up, Girls\!](../Page/Wake_Up,_Girls!.md "wikilink")
  - [上坂堇](../Page/上坂堇.md "wikilink")
  - [内田真礼](../Page/内田真礼.md "wikilink")
  - [奧華子](../Page/奧華子.md "wikilink")
  - [小倉唯](../Page/小倉唯.md "wikilink")
  - [黑崎真音](../Page/黑崎真音.md "wikilink")
  - [ZAQ](../Page/ZAQ.md "wikilink")
  - [SCREEN mode](../Page/SCREEN_mode.md "wikilink")
  - [TrySail](../Page/TrySail.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [春奈露娜](../Page/春奈露娜.md "wikilink")
  - [B-PROJECT](../Page/B-PROJECT.md "wikilink")
  - [水樹奈奈](../Page/水樹奈奈.md "wikilink")
  - 水瀨祈
  - [Milky Holmes](../Page/Milky_Holmes.md "wikilink")
  - [May'n](../Page/May'n.md "wikilink")
  - [LiSA](../Page/LiSA.md "wikilink")

</div>

#### 演唱歌曲一覽

:; 8月25日\[13\]

:\#
[晴天愉快](../Page/晴天愉快.md "wikilink")（電視動畫《[涼宮春日的憂鬱](../Page/涼宮春日的憂鬱.md "wikilink")》片尾曲）／SOS團（[平野綾](../Page/平野綾.md "wikilink")、[茅原實里](../Page/茅原實里.md "wikilink")、[後藤邑子](../Page/後藤邑子.md "wikilink")）

:\#
[歡迎來到加帕利公園](../Page/歡迎來到加帕利公園.md "wikilink")（電視動畫《[動物朋友](../Page/動物朋友.md "wikilink")》片頭曲）／動物朋友
feat.[大石昌良](../Page/大石昌良.md "wikilink")

:\#
[Connect](../Page/Connect.md "wikilink")（電視動畫《[魔法少女小圓](../Page/魔法少女小圓.md "wikilink")》主題曲）／[ClariS](../Page/ClariS.md "wikilink")

:\# （電視動畫《[情色漫畫老師](../Page/情色漫畫老師.md "wikilink")》片頭曲）／ClariS

:\#
FLAWLESS（電視動畫《[決鬥大師VSRF](../Page/決鬥大師.md "wikilink")》片尾曲）／[Pyxis](../Page/Pyxis.md "wikilink")

:\# （動畫《[庫洛魔法使](../Page/庫洛魔法使.md "wikilink")》第3期片頭曲）／Pyxis

:\#
（電視動畫《[騎士&魔法](../Page/騎士&魔法.md "wikilink")》片尾曲）／[大橋彩香](../Page/大橋彩香.md "wikilink")

:\# （電視動畫《[政宗君的復仇](../Page/政宗君的復仇.md "wikilink")》片頭曲）／大橋彩香

:\# fantastic
dreamer（電視動畫《[為美好的世界獻上祝福！](../Page/為美好的世界獻上祝福！.md "wikilink")》片頭曲）／[Machico](../Page/Machico.md "wikilink")

:\# TOMORROW（電視動畫《為美好的世界獻上祝福！2》片頭曲）／Machico

:\# （電視動畫《[吹響吧！上低音號
2](../Page/吹響吧！上低音號.md "wikilink")》片頭曲）／[TRUE](../Page/唐澤美帆.md "wikilink")

:\# BUTTERFLY
EFFECTOR（電視動畫《[雛邏輯～來自幸運邏輯～](../Page/幸運邏輯.md "wikilink")》片頭曲）／TRUE

:\#
DreamRiser（電視動畫《[少女與戰車](../Page/少女與戰車.md "wikilink")》片頭曲）／[Minami](../Page/Minami.md "wikilink")×TRUE

:\# illuminate（電視動畫《[Tales of Zestiria the
X](../Page/熱情傳奇_the_X.md "wikilink")》第二季片頭曲）／Minami

:\# ZERO\!\!（電視動畫《[打工吧！魔王大人](../Page/打工吧！魔王大人.md "wikilink")》片頭曲）／Minami

:\# （電視動畫《[月刊少女野崎君](../Page/月刊少女野崎君.md "wikilink")》片頭曲）／OxT

:\# Go EXCEED\!\!（電視動畫《[鑽石王牌](../Page/鑽石王牌.md "wikilink")》片頭曲）／OxT

:\#
[巴拉萊卡琴](../Page/巴拉萊卡琴_\(歌曲\).md "wikilink")（電視動畫《[花漾明星](../Page/花漾明星.md "wikilink")》片頭曲2）／[田村由香里](../Page/田村由香里.md "wikilink")×OxT

:\#
（電視動畫《[CHAOS;CHILD](../Page/CHAOS;CHILD.md "wikilink")》片尾曲）／[鈴木KONOMI](../Page/鈴木KONOMI.md "wikilink")

:\# Blow
out（電視動畫《[不正經的魔術講師與禁忌教典](../Page/不正經的魔術講師與禁忌教典.md "wikilink")》片頭曲）／鈴木KONOMI

:\# This game（電視動畫《[NO GAME NO LIFE
遊戲人生](../Page/NO_GAME_NO_LIFE_遊戲人生.md "wikilink")》片頭曲）／鈴木KONOMI

:\# BLACK SHOUT／[Roselia from BanG
Dream\!](../Page/BanG_Dream!.md "wikilink")

:\# LOUDER／Roselia from BanG Dream\!

:\# Break your
fate（遊戲《[地下城與勇士](../Page/地下城與勇士.md "wikilink")》廣告歌曲）／[西澤幸奏](../Page/西澤幸奏.md "wikilink")

:\#
歸還（動畫電影《[艦隊Collection](../Page/艦隊Collection_\(動畫\).md "wikilink")》主題曲）／西澤幸奏

:\# （電視動畫《[天元突破
紅蓮螺巖](../Page/天元突破_紅蓮螺巖.md "wikilink")》片頭曲）／西澤幸奏×鈴木KONOMI

:\# Installation(Acoustic)／[早見沙織](../Page/早見沙織.md "wikilink")

:\#
（動畫電影《[窈窕淑女前編～紅緒，花樣的17歲～](../Page/窈窕淑女_\(漫畫\)_#劇場版.md "wikilink")》主題曲）／早見沙織

:\# （電視動畫《[農林](../Page/農林.md "wikilink")》片尾曲）／田村由香里

:\# You & Me／田村由香里 feat. motsu &

:\# [青空Jumping
Heart](../Page/青空Jumping_Heart.md "wikilink")（電視動畫《[LoveLive\!
Sunshine\!\!](../Page/LoveLive!_Sunshine!!.md "wikilink")》片頭曲）／[Aqours](../Page/Aqours.md "wikilink")

:\# [HAPPY PARTY TRAIN](../Page/HAPPY_PARTY_TRAIN.md "wikilink")／Aqours

:\# [想在AQUARIUM戀愛](../Page/想在AQUARIUM戀愛.md "wikilink")／Aqours

:\# [從訴說夢想到歌唱夢想](../Page/從訴說夢想到歌唱夢想.md "wikilink")（電視動畫《LoveLive\!
Sunshine\!\!》片尾曲）／Aqours

:\# ／茅原實里

:\# SELF
PRODUCER（電視動畫《[就算是哥哥，有愛就沒問題了，對吧](../Page/就算是哥哥，有愛就沒問題了，對吧.md "wikilink")》片頭曲）／茅原實里

:\# [Paradise
Lost](../Page/Paradise_Lost.md "wikilink")（電視動畫《[食靈-零-](../Page/食靈.md "wikilink")》片頭曲）／茅原實里

:\# INNOSENSE（電視動畫《Tales of Zestiria the
X》第二季片尾曲）／[FLOW](../Page/FLOW_\(樂團\).md "wikilink")

:\# WORLD END（電視動畫《[叛逆的魯魯修 R2](../Page/叛逆的魯魯修.md "wikilink")》片頭曲2）／FLOW

:\# Go\!\!\!（電視動畫《[火影忍者](../Page/火影忍者.md "wikilink")》片頭曲4）／FLOW

:\# 7
-seven-（電視動畫《[七大罪](../Page/七大罪_\(漫畫\).md "wikilink")》片尾曲）／FLOW×[GRANRODEO](../Page/GRANRODEO.md "wikilink")

:\# Glorious days（動畫電影《[黑子的籃球 LAST
GAME](../Page/黑子的籃球.md "wikilink")》主題曲／GRANRODEO

:\# move on\! （電視動畫《[最遊記RELOAD
BLAST](../Page/最遊記.md "wikilink")》片頭曲）／GRANRODEO

:\# The Other
self（電視動畫《[黑子的籃球](../Page/黑子的籃球.md "wikilink")》第二季片頭曲）／GRANRODEO

:\# Playing The World／ASL2017演出者（除平野綾、後藤邑子、ClariS）

:; 8月26日\[14\]

:\#
（電視動畫《[亞人](../Page/亞人_\(漫畫\).md "wikilink")》第二季片頭曲）／[angela](../Page/angela.md "wikilink")×[fripSide](../Page/fripSide.md "wikilink")

:\# （電視動畫《[蒼穹之戰神
EXODUS](../Page/蒼穹之戰神.md "wikilink")》片頭曲）／[angela](../Page/angela.md "wikilink")

:\# 全力☆Summer\!（電視動畫《[單蠢女孩](../Page/單蠢女孩.md "wikilink")》片頭曲）／angela

:\# （動畫電影《[KING OF PRISM by
PrettyRhythm](../Page/星光少男_KING_OF_PRISM.md "wikilink")》片尾曲）／[KING
OF PRISM](../Page/星光少男_KING_OF_PRISM.md "wikilink")

:\# 混合曲：[CRAZY GONNA CRAZY](../Page/CRAZY_GONNA_CRAZY.md "wikilink")～EZ
DO DANCE（動畫電影《[KING OF PRISM -PRIDE the
HERO-](../Page/星光少男_KING_OF_PRISM.md "wikilink")》插曲）／KING OF
PRISM feat. [DJ KOO](../Page/TRF.md "wikilink")

:\# go to
Romance\>\>\>\>\>（電視動畫《[烏菈菈迷路帖](../Page/烏菈菈迷路帖.md "wikilink")》片尾曲）／[Luce
Twinkle Wink☆](../Page/Luce_Twinkle_Wink☆.md "wikilink")

:\# Fight on\!（電視動畫《[Gamers
電玩咖！](../Page/Gamers_電玩咖！.md "wikilink")》片尾曲）／Luce
Twinkle Wink☆

:\# §Rainbow（電視動畫《[星光少女
彩虹舞台](../Page/星光少女_彩虹舞台.md "wikilink")》片尾曲2）／[i☆Ris](../Page/i☆Ris.md "wikilink")

:\# Shining Star（電視動畫《[星光樂園](../Page/星光樂園.md "wikilink")》第三季片頭曲3）／i☆Ris

:\# （電視動畫《[蟲奉行](../Page/蟲奉行.md "wikilink")》片尾曲）／i☆Ris

:\# You Only Live Once（電視動畫《[Yuri\!\!\! on
ICE](../Page/Yuri!!!_on_ICE.md "wikilink")》片尾曲）／[YURI\!\!\! on ICE feat.
w.hatano](../Page/羽多野渉.md "wikilink")

:\#
（電視動畫《[獨佔我的英雄](../Page/獨佔我的英雄.md "wikilink")》片頭曲）／[羽多野渉](../Page/羽多野渉.md "wikilink")

:\#
（電視動畫《[月色真美](../Page/月色真美.md "wikilink")》片頭曲）／[東山奈央](../Page/東山奈央.md "wikilink")

:\# （電視動畫《月色真美》片尾曲）／東山奈央

:\# TRY UNITE\! -extended
version-（電視動畫《[輪迴的拉格朗日](../Page/輪迴的拉格朗日.md "wikilink")》片頭曲）／[中島愛](../Page/中島愛.md "wikilink")

:\#
[星間飛行](../Page/星間飛行.md "wikilink")（電視動畫《[超時空要塞Frontier](../Page/超時空要塞Frontier.md "wikilink")》插曲）／中島愛

:\# ／[三森鈴子](../Page/三森鈴子.md "wikilink")

:\# （動畫電影《[結城友奈是勇者-鷲尾須美之章-](../Page/鷲尾須美是勇者.md "wikilink")》片頭曲）／三森鈴子

:\# 混合曲：My Only Place～[Super Noisy
Nova](../Page/Super_Noisy_Nova.md "wikilink")（電視動畫《[神裝少女小纏](../Page/神裝少女小纏.md "wikilink")》片尾曲、《[浪漫追星社](../Page/浪漫追星社.md "wikilink")》片頭曲）／[sphere](../Page/sphere_\(聲優團體\).md "wikilink")

:\# [MOON
SIGNAL](../Page/MOON_SIGNAL.md "wikilink")（電視動畫《[半妖少女綺麗譚](../Page/半妖少女綺麗譚.md "wikilink")》片頭曲）／sphere

:\# [HIGH
POWERED](../Page/HIGH_POWERED.md "wikilink")（電視動畫《[侵略！花枝娘](../Page/侵略！花枝娘.md "wikilink")》第二季片頭曲）／sphere

:\# （電視動畫《[KiraKira☆光之美少女 A La
Mode](../Page/KiraKira☆光之美少女_A_La_Mode.md "wikilink")》片頭曲）／[駒形友梨](../Page/駒形友梨.md "wikilink")

:\# （電視動畫《KiraKira☆光之美少女 A La
Mode》片尾曲2）／[宮本佳那子](../Page/宮本佳那子.md "wikilink")

:\# （電視動畫《KiraKira☆光之美少女 A La Mode》片尾曲）／KiraKira☆光之美少女 A La Mode Summer
Session

:\#
HIDE-AND-SEEK（電視動畫《[半田君傳說](../Page/元氣囝仔.md "wikilink")》片尾曲）／[鈴村健一](../Page/鈴村健一.md "wikilink")

:\# SHIPS／鈴村健一

:\# 英雄（特攝影集《[超人力斯](../Page/超人力斯.md "wikilink")》片頭曲）／鈴村健一×羽多野渉

:\# [Don't say
"lazy"](../Page/Don't_say_"lazy".md "wikilink")（電視動畫《[K-ON！輕音少女](../Page/K-ON！輕音少女.md "wikilink")》片尾曲）／Minorin×Mimorin（[茅原實里](../Page/茅原實里.md "wikilink")×三森鈴子）

:\#
（電視動畫《[初戀怪獸](../Page/初戀怪獸.md "wikilink")》片尾曲）／[蒼井翔太](../Page/蒼井翔太.md "wikilink")

:\# [DDD](../Page/DDD_\(蒼井翔太單曲\).md "wikilink")（電視動畫《[未來卡片
戰鬥夥伴DDD](../Page/未來卡片_戰鬥夥伴.md "wikilink")》片頭曲）／蒼井翔太

:\#
（電視動畫《[有頂天家族2](../Page/有頂天家族.md "wikilink")》片尾曲）／[fhána](../Page/fhána.md "wikilink")

:\# Hello\!My
World\!\!（電視動畫《[騎士&魔法](../Page/騎士&魔法.md "wikilink")》片頭曲）／fhána

:\# （電視動畫《[小林家的龍女僕](../Page/小林家的龍女僕.md "wikilink")》片頭曲）／fhána

:\# （電視動畫《[小魔女DoReMi](../Page/小魔女DoReMi.md "wikilink")》片頭曲）／sphere×i☆Ris

:\#
（電視動畫《[七龍珠超](../Page/七龍珠超.md "wikilink")》片頭曲2）／[冰川清志](../Page/冰川清志.md "wikilink")

:\# CHA-LA HEAD-CHA-LA（電視動畫《[七龍珠Z](../Page/七龍珠Z.md "wikilink")》片頭曲）／冰川清志

:\# Dreaming\!／[IDOLM@STER
MILLIONSTARS](../Page/偶像大師_百萬人演唱會！.md "wikilink")

:\# 混合曲：PRETTY DREAMER～～Up\!10sion♪Pleeeeeeeeease\!～Eternal
Harmony／IDOLM@STER MILLIONSTARS

:\# 混合曲：dear...～～～Shooting Stars／IDOLM@STER MILLIONSTARS

:\# Brand New Theater\!／IDOLM@STER MILLIONSTARS

:\# [black
bullet](../Page/black_bullet.md "wikilink")（電視動畫《[黑色子彈](../Page/黑色子彈.md "wikilink")》片頭曲）／[fripSide](../Page/fripSide.md "wikilink")

:\# The end of escape（電視動畫《亞人》第二季片頭曲2）／fripSide×angela

:\# clockwork
planet（電視動畫《[時鐘機關之星](../Page/時鐘機關之星.md "wikilink")》片頭曲）／fripSide

:\# [only my
railgun](../Page/only_my_railgun.md "wikilink")（電視動畫《[科學超電磁砲](../Page/科學超電磁砲.md "wikilink")》片頭曲）／fripSide

:\# Playing The World／ASL2017演出者

:; 8月27日\[15\]

:\# 混合曲：[crossing field](../Page/crossing_field.md "wikilink")～Chase the
world（電視動畫《[刀劍神域](../Page/刀劍神域.md "wikilink")》片頭曲、《[加速世界](../Page/加速世界.md "wikilink")》片頭曲）／[LiSA](../Page/LiSA.md "wikilink")×[May'n](../Page/May'n.md "wikilink")

:\# [Inner
Urge](../Page/Inner_Urge.md "wikilink")（電視動畫《[下流梗不存在的灰暗世界](../Page/下流梗不存在的灰暗世界.md "wikilink")》片尾曲）／[上坂堇](../Page/上坂堇.md "wikilink")

:\# （電視動畫《[單蠢女孩](../Page/單蠢女孩.md "wikilink")》片尾曲）／上坂堇

:\# Beyond The Dream／

:\# 混合曲：～HIGH JUMP NO LIMIT～Study Equal Magic\!／IDOLM@STER SideM

:\# DRIVE A LIVE／IDOLM@STER SideM

:\#
（電視動畫《[不起眼女主角培育法♭](../Page/不起眼女主角培育法.md "wikilink")》片頭曲）／[春奈露娜](../Page/春奈露娜.md "wikilink")

:\# 混合曲：～（電視動畫《[物語系列
第二季](../Page/物語系列_第二季.md "wikilink")》片尾曲、《[不起眼女主角培育法](../Page/不起眼女主角培育法.md "wikilink")》片頭曲）／春奈露娜

:\# [Starry Wish](../Page/Starry_Wish.md "wikilink")（電視動畫《[ViVid
Strike\!](../Page/ViVid_Strike!.md "wikilink")》片尾曲）／[水瀨祈](../Page/水瀨祈.md "wikilink")

:\#
[曖昧模糊](../Page/曖昧模糊.md "wikilink")（電視動畫《[徒然喜歡你](../Page/徒然喜歡你.md "wikilink")》片頭曲）／水瀨祈

:\#
（電視動畫《[清戀](../Page/清戀.md "wikilink")》片頭曲）／[奧華子](../Page/奧華子.md "wikilink")

:\# （動畫電影《[穿越時空的少女](../Page/跳躍吧！時空少女.md "wikilink")》片尾曲）／奧華子

:\# Future Strike（電視動畫《ViVid
Strike\!》片頭曲）／[小倉唯](../Page/小倉唯.md "wikilink")

:\# 混合曲：～Honey♥Come\!\!（電視動畫《[卡片鬥爭\!\! 先導者G
超越之門篇](../Page/卡片鬥爭!!_先導者.md "wikilink")》片尾曲、《[城下町的蒲公英](../Page/城下町的蒲公英.md "wikilink")》片尾曲）／小倉唯

:\#
[Magic∞world](../Page/Magic∞world.md "wikilink")（電視動畫《[魔法禁書目錄Ⅱ](../Page/魔法禁書目錄.md "wikilink")》片尾曲）／[黑崎真音](../Page/黑崎真音.md "wikilink")

:\# VERMILLION（電視動畫《[漂流武士](../Page/漂流武士.md "wikilink")》片尾曲）／黑崎真音

:\# 混合曲：～～～Pleasure Stride～（電視動畫《[未來卡片
戰鬥夥伴100](../Page/未來卡片_戰鬥夥伴.md "wikilink")》片尾曲2、《[卡片鬥爭\!\!
先導者](../Page/卡片鬥爭!!_先導者.md "wikilink")》片尾曲4、*不適用*、《[卡片鬥爭\!\! 先導者G
NEXT](../Page/卡片鬥爭!!_先導者.md "wikilink")》片尾曲3、《卡片鬥爭\!\! 先導者》片尾曲4）／[Milky
Holmes](../Page/Milky_Holmes.md "wikilink")

:\# （電視動畫《[偵探歌劇 少女福爾摩斯](../Page/偵探歌劇_少女福爾摩斯.md "wikilink")》片頭曲）／Milky
Holmes

:\# Last Proof（動畫電影《[TRINITY SEVEN
-悠久圖書館與鍊金術少女-](../Page/TRINITY_SEVEN_魔道書7使者.md "wikilink")》主題曲）／[ZAQ](../Page/ZAQ.md "wikilink")

:\#
Alteration（電視動畫《[鎖鎖美小姐@不好好努力](../Page/鎖鎖美小姐@不好好努力.md "wikilink")》片頭曲）／ZAQ

:\# Sparkling
Daydream（電視動畫《[中二病也想談戀愛！](../Page/中二病也想談戀愛！.md "wikilink")》片頭曲）／ZAQ
feat.
[內田真禮](../Page/內田真禮.md "wikilink")＆[上坂堇](../Page/上坂堇.md "wikilink")

:\# （遊戲《[B-PROJECT
無敵＊危險](../Page/B-PROJECT.md "wikilink")》主題曲）／[B-PROJECT](../Page/B-PROJECT.md "wikilink")

:\# 混合曲：Hungry Wolf～dreaming time／B-PROJECT

:\# ／B-PROJECT

:\# One In A Billion（電視動畫《[異世界食堂](../Page/異世界食堂.md "wikilink")》片頭曲）／Wake
Up, May'n（[Wake Up,
Girls\!](../Page/Wake_Up,_Girls!_\(聲優組合\).md "wikilink")×May'n）

:\# （電視動畫《[戀愛暴君](../Page/戀愛暴君_\(三星眼鏡的漫畫\).md "wikilink")》片頭曲）／Wake Up,
Girls\!

:\# Beyond the Bottom（動畫電影《[Wake Up, Girls！Beyond the
Bottom](../Page/Wake_Up,_Girls!.md "wikilink")》主題曲／Wake Up, Girls\!

:\# Reason Living（電視動畫《[文豪Stray
Dogs](../Page/文豪Stray_Dogs.md "wikilink")》片頭曲2）／[SCREEN
mode](../Page/SCREEN_mode.md "wikilink")

:\#
MYSTERIUM（電視動畫《[梵諦岡奇蹟調查官](../Page/梵諦岡奇蹟調查官.md "wikilink")》片頭曲）／SCREEN
mode

:\# （電視動畫《[大\~集合！小魔女DoReMi](../Page/小魔女DoReMi.md "wikilink")》片頭曲）／MILKY
mode（Milky Holmes×SCREEN mode）

:\# （電視動畫《[我，要成為雙馬尾](../Page/我，要成為雙馬尾.md "wikilink")》片頭曲）／內田真禮

:\# +INTERSECT+／內田真禮

:\#
（電視動畫《[亞人醬有話要說](../Page/亞人醬有話要說.md "wikilink")》片頭曲）／[TrySail](../Page/TrySail.md "wikilink")

:\#
adrenaline\!\!\!（電視動畫《[情色漫畫老師](../Page/情色漫畫老師.md "wikilink")》片尾曲）／TrySail

:\# [oath
sign](../Page/oath_sign.md "wikilink")（電視動畫《[Fate/Zero](../Page/Fate/Zero.md "wikilink")》片頭曲）／LiSA

:\# （電視動畫《[我的英雄學院](../Page/我的英雄學院.md "wikilink")》片尾曲3）／LiSA

:\# [Catch the
Moment](../Page/Catch_the_Moment.md "wikilink")（動畫電影《[刀劍神域劇場版
-序列爭戰-](../Page/刀劍神域劇場版_-序列爭戰-.md "wikilink")》主題曲）／LiSA

:\# [Rising
Hope](../Page/Rising_Hope.md "wikilink")（電視動畫《[魔法科高中的劣等生](../Page/魔法科高中的劣等生.md "wikilink")》片頭曲）／LiSA

:\# TESTAMENT -Aufwachen Form-（電視動畫《[戰姬絕唱SYMPHOGEAR
AXZ](../Page/戰姬絕唱SYMPHOGEAR.md "wikilink")》片頭曲）／[水樹奈奈](../Page/水樹奈奈.md "wikilink")

:\# Destiny's
Prelude（動畫電影《[魔法少女奈葉Reflection](../Page/魔法少女奈葉Reflection.md "wikilink")》主題曲）／水樹奈奈

:\# Invisible Heat（動畫電影《魔法少女奈葉Reflection》插曲）／水樹奈奈

:\# UNLIMITED BEAT（遊戲《[戰姬絕唱Symphogear XD
UNLIMITED](../Page/戰姬絕唱SYMPHOGEAR.md "wikilink")》主題曲）／水樹奈奈

:\# Playing The World／ASL2017演出者

## 2018年

### Animelo Summer Live 2018 OK\!

  - 舉辦日期：2018年8月24日－8月26日
  - 會場：[埼玉超級體育館](../Page/埼玉超級體育館.md "wikilink")
  - 主辦：MAGES.、[文化放送](../Page/文化放送.md "wikilink")、[BS富士](../Page/BS富士.md "wikilink")
  - 協力贊助：ANiUTa、[Good Smile
    Company](../Page/Good_Smile_Company.md "wikilink")、LIVE DAM STADIUM
  - 後援：[愛貝克思娛樂](../Page/愛貝克思娛樂.md "wikilink")、[NBC環球娛樂](../Page/NBC環球娛樂.md "wikilink")、[KADOKAWA](../Page/KADOKAWA.md "wikilink")、[King
    Records](../Page/King_Records.md "wikilink")、ZERO-A、[日本索尼音樂娛樂](../Page/日本索尼音樂娛樂.md "wikilink")、DIVE
    II
    entertainment、[東寶映像事業部](../Page/東寶.md "wikilink")、[日本古倫美亞](../Page/日本古倫美亞.md "wikilink")、[萬代南夢宮遊戲](../Page/萬代南夢宮遊戲.md "wikilink")、[5pb.](../Page/5pb..md "wikilink")、[Flying
    DOG](../Page/Flying_DOG.md "wikilink")、[武士道](../Page/武士道.md "wikilink")、[波麗佳音](../Page/波麗佳音.md "wikilink")、[Lantis](../Page/Lantis.md "wikilink")、[日本華納家庭娛樂](../Page/日本華納家庭娛樂.md "wikilink")
  - 企劃：AniSummer Project實行委員會
  - 協力：埼玉超級競技場、[7-Eleven](../Page/7-Eleven.md "wikilink")、[淘兒唱片](../Page/淘兒唱片.md "wikilink")、PIA
  - 製作協力：Grand Slam

#### 主題曲

  - 「Stand by...MUSIC\!\!\!」\[16\]
    作詞：[唐澤美帆](../Page/唐澤美帆.md "wikilink")，作曲、編曲：[神前曉](../Page/神前曉.md "wikilink")
    主唱：[偶像大師SideM](../Page/偶像大師SideM.md "wikilink")、[偶像大師
    百萬人演唱會！](../Page/偶像大師_百萬人演唱會！.md "wikilink")、[亞咲花](../Page/亞咲花.md "wikilink")、[伊藤美来](../Page/伊藤美来.md "wikilink")、[内田彩](../Page/内田彩.md "wikilink")、[內田真禮](../Page/內田真禮.md "wikilink")、[大石昌良](../Page/大石昌良.md "wikilink")、[GRANRODEO](../Page/GRANRODEO.md "wikilink")、[鈴木KONOMI](../Page/鈴木KONOMI.md "wikilink")、[鈴木實里](../Page/鈴木實里.md "wikilink")、[竹達彩奈](../Page/竹達彩奈.md "wikilink")、[TRUE](../Page/唐澤美帆.md "wikilink")、[fhána](../Page/fhána.md "wikilink")、[悠木碧](../Page/悠木碧.md "wikilink")

#### 出演者

  - 8月24日\[17\]

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [Aqours](../Page/Aqours.md "wikilink")
  - [亞咲花](../Page/亞咲花.md "wikilink")
  - [伊藤美来](../Page/伊藤美来.md "wikilink")
  - [內田彩](../Page/內田彩.md "wikilink")
  - [OLDCODEX](../Page/OLDCODEX.md "wikilink")
  - [GARNiDELiA](../Page/GARNiDELiA.md "wikilink")
  - [DearDream](../Page/夢幻慶典.md "wikilink")
  - [春奈露娜](../Page/春奈露娜.md "wikilink")
  - [Poppin'Party](../Page/BanG_Dream!.md "wikilink")
  - [MYTH & ROID](../Page/MYTH_&_ROID.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [三森鈴子](../Page/三森鈴子.md "wikilink")

  - [黑崎真音](../Page/黑崎真音.md "wikilink")

  - [大無限樂團](../Page/大無限樂團.md "wikilink")

  - [中島愛](../Page/中島愛.md "wikilink")

  - [伊藤香奈子](../Page/伊藤香奈子.md "wikilink")

  -
  - 蓮（[楠木灯](../Page/楠木灯.md "wikilink")）

  - [山崎惠理](../Page/山崎惠理.md "wikilink")

  - [Wake Up, Girls\!](../Page/Wake_Up,_Girls!_\(聲優組合\).md "wikilink")

  - [藍井艾露](../Page/藍井艾露.md "wikilink")

</div>

  - 8月25日\[18\]

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [偶像大師SideM](../Page/偶像大師SideM.md "wikilink")
  - [內田真禮](../Page/內田真禮.md "wikilink")
  - [大橋彩香](../Page/大橋彩香.md "wikilink")
  - [GRANRODEO](../Page/GRANRODEO.md "wikilink")
  - [竹達彩奈](../Page/竹達彩奈.md "wikilink")
  - [茅原實里](../Page/茅原實里.md "wikilink")
  - [TRUE](../Page/唐澤美帆.md "wikilink")
  - [TrySail](../Page/TrySail.md "wikilink")
  - [fhána](../Page/fhána.md "wikilink")
  - [水瀨祈](../Page/水瀨祈.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [悠木碧](../Page/悠木碧.md "wikilink")
  - [賽馬娘Pretty Derby](../Page/賽馬娘Pretty_Derby.md "wikilink")
  - [宮野真守](../Page/宮野真守.md "wikilink")
  - [Starlight九九組](../Page/少女☆歌劇Revue_Starlight.md "wikilink")
  - [Minami](../Page/Minami.md "wikilink")
  - [山崎遙](../Page/山崎遙.md "wikilink")
  - [petit milady](../Page/petit_milady.md "wikilink")

</div>

  - 8月26日\[19\]

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [偶像大師 百萬人演唱會！](../Page/偶像大師_百萬人演唱會！.md "wikilink")
  - [I☆Ris](../Page/I☆Ris.md "wikilink")
  - [蒼井翔太](../Page/蒼井翔太.md "wikilink")
  - [上坂堇](../Page/上坂堇.md "wikilink")
  - [大石昌良](../Page/大石昌良.md "wikilink")
  - [ORESAMA](../Page/ORESAMA.md "wikilink")
  - [ZAQ](../Page/ZAQ.md "wikilink")
  - [妹S](../Page/干物妹！小埋.md "wikilink")
  - [JAM Project](../Page/JAM_Project.md "wikilink")
  - [鈴木KONOMI](../Page/鈴木KONOMI.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [鈴木實里](../Page/鈴木實里.md "wikilink")

  - [早見沙織](../Page/早見沙織.md "wikilink")

  - [Milky Holmes](../Page/Milky_Holmes.md "wikilink")

  - [東山奈央](../Page/東山奈央.md "wikilink")

  - [小倉唯](../Page/小倉唯.md "wikilink")

  -
  - [OxT](../Page/OxT.md "wikilink")

  - [麻倉桃](../Page/麻倉桃.md "wikilink")

  - [雨宮天](../Page/雨宮天.md "wikilink")

  - [夏川椎菜](../Page/夏川椎菜.md "wikilink")

</div>

#### 演唱歌曲一覽

:; 8月24日\[20\]

:\# DISCOTHEQUE（電視動畫《[十字架與吸血鬼
CAPU2](../Page/十字架與吸血鬼.md "wikilink")》片頭曲）／[三森鈴子](../Page/三森鈴子.md "wikilink")×[内田彩](../Page/内田彩.md "wikilink")

:\# 流星（電視動畫《[刀劍神域外傳Gun Gale
Online](../Page/刀劍神域外傳Gun_Gale_Online.md "wikilink")》片頭曲）／[藍井艾露](../Page/藍井艾露.md "wikilink")

:\# （電視動畫《[亞爾斯蘭戰記](../Page/亞爾斯蘭戰記.md "wikilink")》片尾曲 ）／藍井艾露

:\# SPEED
STAR（劇場版《[魔法科高中的劣等生：呼喚繁星的少女](../Page/魔法科高中的劣等生.md "wikilink")》主題曲）／[GARNiDELiA](../Page/GARNiDELiA.md "wikilink")

:\# Error（電視動畫《[沒有心跳的少女
BEATLESS](../Page/沒有心跳的少女_BEATLESS.md "wikilink")》片頭曲）／GARNiDELiA

:\#
[你不知道的故事](../Page/你不知道的故事.md "wikilink")（電視動畫《[化物語](../Page/化物語.md "wikilink")》片尾曲）／GARNiDELiA×[中島愛](../Page/中島愛.md "wikilink")

:\# To see the future（電視動畫《刀劍神域外傳Gun Gale
Online》片尾曲）／蓮（[楠木灯](../Page/楠木灯.md "wikilink")）

:\# Shocking
Blue（電視動畫《[武裝少女Machiavellianism](../Page/武裝少女Machiavellianism.md "wikilink")》片頭曲）／[伊藤美來](../Page/伊藤美來.md "wikilink")

:\# （電視動畫《[龍王的工作！](../Page/龍王的工作！.md "wikilink")》片尾曲）／伊藤美來

:\# PLEASURE
FLAG（電視動畫《[夢幻慶典](../Page/夢幻慶典.md "wikilink")》片頭曲）／[DearDream](../Page/夢幻慶典.md "wikilink")

:\# （電視動畫《[夢幻慶典R](../Page/夢幻慶典.md "wikilink")》片頭曲）／DearDream

:\# Open your
eyes（電視動畫《[超自然9人組](../Page/超自然9人組.md "wikilink")》片尾曲）／[亞咲花](../Page/亞咲花.md "wikilink")

:\# SHINY DAYS（電視動畫《[搖曳露營△](../Page/搖曳露營△.md "wikilink")》片頭曲）／亞咲花

:\# Bright
way（電視動畫《[百鍊霸王與聖約女武神](../Page/百鍊霸王與聖約女武神.md "wikilink")》片頭曲）／内田彩

:\# So
Happy（電視動畫《[你還是不懂群馬](../Page/你還是不懂群馬.md "wikilink")》片尾曲）／内田彩、[群馬將](../Page/群馬將.md "wikilink")

:\#
Starlight（電視動畫《[昴宿七星](../Page/昴宿七星.md "wikilink")》片尾曲）／[山崎惠理](../Page/山崎惠理.md "wikilink")

:\# （電視動畫《[橘子醬男孩](../Page/橘子醬男孩.md "wikilink")》片頭曲）／伊藤美來×山崎惠理

:\# ／[Poppin'Party](../Page/BanG_Dream!.md "wikilink")

:\# （電視動畫《[BanG
Dream\!](../Page/BanG_Dream!.md "wikilink")》片尾曲）／Poppin'Party

:\# God
knows...（電視動畫《[涼宮春日的憂鬱](../Page/涼宮春日的憂鬱.md "wikilink")》插曲）／Poppin'Party

:\#
（電視動畫《[命運石之門0](../Page/命運石之門0.md "wikilink")》片頭曲）／[伊藤香奈子](../Page/伊藤香奈子.md "wikilink")

:\# LAST GAME（電視動畫《命運石之門0》片尾曲）／

:\# （電視動畫《[命運石之門](../Page/命運石之門.md "wikilink")》片尾曲）／伊藤香奈子×Zwei

:\# （劇場版《[Macross F 戀離飛翼](../Page/超時空要塞Frontier.md "wikilink")》主題曲）／中島愛

:\# （電視動畫《[網路勝利組](../Page/網路勝利組.md "wikilink")》片頭曲）／中島愛

:\#
（電視動畫《[搖曳莊的幽奈小姐](../Page/搖曳莊的幽奈小姐.md "wikilink")》片頭曲）／[春奈露娜](../Page/春奈露娜.md "wikilink")

:\#
[Startear](../Page/Startear.md "wikilink")（電視動畫《[刀劍神域Ⅱ](../Page/刀劍神域.md "wikilink")》片尾曲）／春奈露娜

:\#
[多愁善感的瞬間](../Page/多愁善感的瞬間.md "wikilink")（電視動畫《[鋼之鍊金術師FA](../Page/鋼之鍊金術師FA.md "wikilink")》片尾曲）／[黑崎真音](../Page/黑崎真音.md "wikilink")×春奈露娜
feat.
[大塚紗英](../Page/大塚紗英.md "wikilink")&[西本梨美](../Page/西本梨美.md "wikilink")
from Poppin'Party

:\# décadence --（電視動畫《[罪人與龍共舞](../Page/罪人與龍共舞.md "wikilink")》片尾曲）／黑崎真音

:\# Gravitation（電視動畫《[魔法禁書目錄Ⅲ](../Page/魔法禁書目錄.md "wikilink")》片頭曲）／黑崎真音

:\# （電視動畫《[Wake Up,
Girls\!](../Page/Wake_Up,_Girls!.md "wikilink")》插曲）／[Wake Up,
Girls\!](../Page/Wake_Up,_Girls!_\(聲優組合\).md "wikilink")

:\# Polaris（電視動畫《[Wake Up, Girls\!
新章](../Page/Wake_Up,_Girls!.md "wikilink")》插曲）／Wake Up, Girls\!

:\# （電視動畫《[萌萌侵略者 OUTBREAK
COMPANY](../Page/萌萌侵略者_OUTBREAK_COMPANY.md "wikilink")》片頭曲）／三森鈴子

:\# （[動作遊戲](../Page/動作遊戲.md "wikilink")《[無雙OROCHI 蛇魔
3](../Page/無雙OROCHI_蛇魔_3.md "wikilink")》主題曲）／三森鈴子

:\# VORACITY（電視動畫《[OVERLORD
Ⅲ](../Page/OVERLORD_\(小說\).md "wikilink")》片頭曲）／[MYTH &
ROID](../Page/MYTH_&_ROID.md "wikilink")

:\# HYDRA（電視動畫《[OVERLORD
Ⅱ](../Page/OVERLORD_\(小說\).md "wikilink")》片尾曲）／MYTH &
ROID

:\#
（電視動畫《[十二大戰](../Page/十二大戰.md "wikilink")》片尾曲）／[大無限樂團](../Page/大無限樂團.md "wikilink")

:\# （電視動畫《[犬夜叉](../Page/犬夜叉.md "wikilink")》片尾曲2）／大無限樂團

:\# （電視動畫《[犬夜叉 完結篇](../Page/犬夜叉.md "wikilink")》片頭曲）／大無限樂團

:\# [未來的我們早已知曉](../Page/未來的我們早已知曉.md "wikilink")（電視動畫《[LoveLive\!
Sunshine\!\!](../Page/LoveLive!_Sunshine!!.md "wikilink")》第二季片頭曲）／[Aqours](../Page/Aqours.md "wikilink")

:\# ／Aqours

:\# 混合曲：[WONDERFUL
STORIES](../Page/WATER_BLUE_NEW_WORLD/WONDERFUL_STORIES.md "wikilink")～[勇氣在哪？在你的內心！](../Page/勇氣在哪？在你的內心！.md "wikilink")（電視動畫《LoveLive\!
Sunshine\!\!》第二季插曲、片尾曲）／Aqours

:\# Growth
Arrow（電視動畫《[Butlers～千年百年物語～](../Page/Butlers～千年百年物語～.md "wikilink")》片頭曲）／[OLDCODEX](../Page/OLDCODEX.md "wikilink")

:\# One Side（劇場版《[吸血鬼僕人-Alice in the
Garden-](../Page/吸血鬼僕人.md "wikilink")》主題曲）／OLDCODEX

:\# Heading to Over（電視動畫《[Free\!-Dive to the
Future-](../Page/Free!.md "wikilink")》片頭曲）／OLDCODEX

:\# WALK（電視動畫《[黑子的籃球](../Page/黑子的籃球.md "wikilink")》第二季片尾曲）／OLDCODEX

:\# Stand by...MUSIC\!\!\!／ASL2018演出者

:; 8月25日\[21\]

:\#
混合曲：～[曖昧模糊](../Page/曖昧模糊.md "wikilink")（電視動畫《[我，要成為雙馬尾](../Page/我，要成為雙馬尾.md "wikilink")》片頭曲、電視動畫《[徒然喜歡你](../Page/徒然喜歡你.md "wikilink")》片頭曲）／[內田真禮](../Page/內田真禮.md "wikilink")×[水瀨祈](../Page/水瀨祈.md "wikilink")

:\# 混合曲：～Make Debut！（電視動畫《[賽馬娘 Pretty
Derby](../Page/賽馬娘_Pretty_Derby.md "wikilink")》片尾曲、片頭曲）／[賽馬娘
Pretty Derby](../Page/賽馬娘_Pretty_Derby.md "wikilink")

:\# （電視動畫《賽馬娘 Pretty Derby》片尾曲）／賽馬娘 Pretty Derby

:\#
（電視動畫《[粗點心戰爭2](../Page/粗點心戰爭.md "wikilink")》片頭曲）／[竹達彩奈](../Page/竹達彩奈.md "wikilink")

:\# （電視動畫《[粗點心戰爭](../Page/粗點心戰爭.md "wikilink")》片尾曲）／竹達彩奈

:\#
（電視動畫《[琴之森](../Page/琴之森.md "wikilink")》片尾曲）／[悠木碧](../Page/悠木碧.md "wikilink")

:\#
（電視動畫《[我的女友是個過度認真的處女bitch](../Page/我的女友是個過度認真的處女bitch.md "wikilink")》片頭曲）／悠木碧

:\#
（電視動畫《[魔法少女網站](../Page/魔法少女網站.md "wikilink")》片尾曲）／[山崎遙](../Page/山崎遙.md "wikilink")

:\# Star
Divine／[Starlight九九組](../Page/少女☆歌劇Revue_Starlight.md "wikilink")

:\# （電視動畫《[少女☆歌劇Revue
Starlight](../Page/少女☆歌劇Revue_Starlight.md "wikilink")》片頭曲）／Starlight九九組

:\# （電視動畫《[櫻花大戰](../Page/櫻花大戰_\(動畫\).md "wikilink")》片頭曲）／Starlight九九組

:\# Million Futures（遊戲《乖離性百萬亞瑟王》主題曲）／水瀨祈

:\# [Starry Wish](../Page/Starry_Wish.md "wikilink")（電視動畫《[ViVid
Strike\!](../Page/ViVid_Strike!.md "wikilink")》片尾曲）／水瀨祈

:\# NOISY LOVE POWER☆（電視動畫《[魔法少女
我](../Page/魔法少女_我.md "wikilink")》片頭曲）／[大橋彩香](../Page/大橋彩香.md "wikilink")

:\#
[YES\!\!](../Page/YES!!_\(大橋彩香單曲\).md "wikilink")（電視動畫《[生存遊戲社](../Page/生存遊戲社.md "wikilink")》片頭曲）／大橋彩香

:\#  ～My Uncompleted
Story～（電視動畫《[童話魔法使](../Page/童話魔法使.md "wikilink")》片頭曲）／[fhána](../Page/fhána.md "wikilink")

:\# [Que Sera,
Sera](../Page/Que_Sera,_Sera_\(fhána歌曲\).md "wikilink")（電視動畫《[有頂天家族](../Page/有頂天家族.md "wikilink")》片尾曲）／fhána

:\# 青空のラプソディ（電視動畫《[小林家的龍女僕](../Page/小林家的龍女僕.md "wikilink")》片頭曲）／fhána

:\# WANTED GIRL（電視動畫《[時間飛船24
惡黨反擊](../Page/時間飛船24.md "wikilink")》片頭曲）／[TrySail](../Page/TrySail.md "wikilink")

:\# Truth.（電視動畫《[沒有心跳的少女
BEATLESS](../Page/沒有心跳的少女_BEATLESS.md "wikilink")》片頭曲）／TrySail

:\#
adrenaline\!\!\!（電視動畫《[情色漫畫老師](../Page/情色漫畫老師.md "wikilink")》片尾曲）／TrySail

:\# Los\! Los\!
Los\!（電視動畫《[幼女戰記](../Page/幼女戰記.md "wikilink")》片尾曲）／悠木碧（[譚雅·提古雷查夫](../Page/幼女戰記.md "wikilink")）

:\# aventure bleu（電視動畫《[酒鬼妹子](../Page/酒鬼妹子.md "wikilink")》片頭曲）／內田真禮

:\# Smiling Spiral／內田真禮

:\# One
Unit（電視動畫《[行星與共](../Page/行星與共.md "wikilink")》片頭曲）／[Minami](../Page/Minami.md "wikilink")

:\# Precious
Memories（電視動畫《[你所期望的永遠](../Page/你所期望的永遠.md "wikilink")》片頭曲）／Minami

:\# Remained dream（電視動畫《[驚爆危機！Invisible
Victory](../Page/驚爆危機.md "wikilink")》片尾曲）／[茅原實里](../Page/茅原實里.md "wikilink")

:\# （電視動畫《[紫羅蘭永恆花園](../Page/紫羅蘭永恆花園.md "wikilink")》片尾曲）／茅原實里

:\# Sincerely（電視動畫《紫羅蘭永恆花園》片頭曲）／[TRUE](../Page/唐澤美帆.md "wikilink")

:\# UNISONIA（電視動畫《[BUDDY
COMPLEX](../Page/BUDDY_COMPLEX.md "wikilink")》片頭曲）／TRUE

:\# DREAM
SOLISTER（電視動畫《[吹響吧！上低音號](../Page/吹響吧！上低音號.md "wikilink")》片頭曲）／TRUE

:\# （電視動畫《[昴宿七星](../Page/昴宿七星.md "wikilink")》片頭曲）／[petit
milady](../Page/petit_milady.md "wikilink")

:\# （電視動畫《[百鍊霸王與聖約女武神](../Page/百鍊霸王與聖約女武神.md "wikilink")》片尾曲）／petit
milady

:\# （電視動畫《[POP TEAM EPIC](../Page/POP_TEAM_EPIC.md "wikilink")》插曲）／petit
milady

:\#
[JOINT](../Page/JOINT.md "wikilink")（電視動畫《[灼眼的夏娜Ⅱ](../Page/灼眼的夏娜_\(動畫\).md "wikilink")》片頭曲）／Mimorin×Minorin（三森鈴子×茅原實里）

:\#
Reason\!\!（電視動畫《[偶像大師SideM](../Page/偶像大師SideM.md "wikilink")》片頭曲）／[偶像大師SideM](../Page/偶像大師SideM.md "wikilink")

:\# GLORIOUS RO@D（電視動畫《偶像大師SideM》插曲）／偶像大師SideM

:\# DRIVE A LIVE（電視動畫《偶像大師SideM》片尾曲）／偶像大師SideM

:\# The Birth（劇場版《[亞人
-衝戟-](../Page/亞人_\(漫畫\).md "wikilink")》主題曲）／[宮野真守](../Page/宮野真守.md "wikilink")

:\# SHOUT\!（電視動畫《[卡片鬥爭\!\! 先導者G
超越之門篇](../Page/卡片鬥爭!!_先導者.md "wikilink")》片頭曲）／宮野真守

:\# （電視動畫《[歌之王子殿下 真愛革命](../Page/歌之王子殿下.md "wikilink")》片頭曲）／宮野真守

:\#
BEASTFUL（電視動畫《[刃牙](../Page/刃牙.md "wikilink")》片尾曲）／[GRANRODEO](../Page/GRANRODEO.md "wikilink")

:\# Deadly Drive（劇場版《[文豪Stray Dogs DEAD
APPLE](../Page/文豪Stray_Dogs.md "wikilink")》片頭曲）／GRANRODEO

:\# （電視動畫《[機動戰士高達
鐵血的孤兒](../Page/機動戰士高達_鐵血的孤兒.md "wikilink")》第二季片尾曲）／GRANRODEO

:\# Punky Funky
Love（電視動畫《[黑子的籃球](../Page/黑子的籃球.md "wikilink")》第三季片尾曲）／GRANRODEO

:\# Stand by...MUSIC\!\!\!／ASL2018演出者

:; 8月26日\[22\]

:\# ANISAM A GO GO／[Milky
Holmes](../Page/Milky_Holmes.md "wikilink")×[i☆Ris](../Page/i☆Ris.md "wikilink")×[上坂堇](../Page/上坂堇.md "wikilink")×[東山奈央](../Page/東山奈央.md "wikilink")

:\#
（電視動畫《[干物妹！小埋R](../Page/干物妹！小埋.md "wikilink")》片尾曲）／[妹S](../Page/干物妹！小埋.md "wikilink")

:\# （電視動畫《[干物妹！小埋](../Page/干物妹！小埋.md "wikilink")》片頭曲）／妹S

:\#
（電視動畫《[魔法律事務所](../Page/魔法律事務所.md "wikilink")》片尾曲）／[ORESAMA](../Page/ORESAMA.md "wikilink")

:\#
（電視動畫《[咕嚕咕嚕魔法陣](../Page/咕嚕咕嚕魔法陣_#2017年《咕嚕咕嚕魔法陣》（第3作）.md "wikilink")》片頭曲）／ORESAMA

:\# [Snow halation](../Page/Snow_halation.md "wikilink")／[THE
MONSTERS](../Page/JAM_Project.md "wikilink")

:\# Hell Yeah\!\!／[HellShake矢野](../Page/POP_TEAM_EPIC.md "wikilink")

:\# POP TEAM EPIC（電視動畫《[POP TEAM
EPIC](../Page/POP_TEAM_EPIC.md "wikilink")》片頭曲）／[上坂堇](../Page/上坂堇.md "wikilink")

:\# （電視動畫《[寶石之國](../Page/寶石之國.md "wikilink")》片頭曲）／

:\# 混合曲：Shiny Ray～MIND
CONDUCTOR（電視動畫《[小魔女學園](../Page/小魔女學園.md "wikilink")》片頭曲）／YURiKA

:\#
（電視動畫《[多田君不戀愛](../Page/多田君不戀愛.md "wikilink")》片頭曲）／[大石昌良](../Page/大石昌良.md "wikilink")

:\# Jewelry（電視動畫《[庫洛魔法使
透明牌篇](../Page/庫洛魔法使_透明牌篇.md "wikilink")》片尾曲）／[早見沙織](../Page/早見沙織.md "wikilink")

:\# （劇場版《[窈窕淑女 後篇
～花之東京大浪漫～](../Page/窈窕淑女_\(漫畫\).md "wikilink")》主題曲）／早見沙織

:\# True Destiny（電視動畫《[鎖鏈戰記
～赫克瑟塔斯之光～](../Page/鎖鏈戰記.md "wikilink")》片尾曲）／[東山奈央](../Page/東山奈央.md "wikilink")

:\# （電視動畫《[妖怪旅館營業中](../Page/妖怪旅館營業中.md "wikilink")》片頭曲）／東山奈央

:\# FEELING
AROUND（電視動畫《[愛吃拉麵的小泉同學](../Page/愛吃拉麵的小泉同學.md "wikilink")》片頭曲）／[鈴木實里](../Page/鈴木實里.md "wikilink")

:\# （電視動畫《[百變小櫻Clear咭](../Page/百變小櫻Clear咭.md "wikilink")》片尾曲）／鈴木實里

:\# Catch You Catch
Me（電視動畫《[百變小櫻Magic咭](../Page/百變小櫻Magic咭.md "wikilink")》片頭曲）／早見沙織×鈴木實里

:\# （遊戲《[Summer
Pockets](../Page/Summer_Pockets.md "wikilink")》主題曲）／[鈴木KONOMI](../Page/鈴木KONOMI.md "wikilink")

:\# （電視動畫《[LOST SONG](../Page/LOST_SONG.md "wikilink")》片頭曲）／鈴木KONOMI

:\#
（電視動畫《[我不受歡迎，怎麼想都是你們的錯！](../Page/我不受歡迎，怎麼想都是你們的錯！.md "wikilink")》片頭曲）／鈴木KONOMI

:\# （電視動畫《》片頭曲）／[麻倉桃](../Page/麻倉桃.md "wikilink")

:\# （電視動畫《[七大罪
戒律的復活](../Page/七大罪_\(漫畫\).md "wikilink")》片尾曲）／[雨宮天](../Page/雨宮天.md "wikilink")

:\# （電視動畫《水嫩小嘰\!\!》片尾曲）／[夏川椎菜](../Page/夏川椎菜.md "wikilink")

:\# Changing
point（電視動畫《[魔法少女網站](../Page/魔法少女網站.md "wikilink")》片頭曲）／[i☆Ris](../Page/i☆Ris.md "wikilink")

:\# 混合曲：Make it\!～～Realize\!～～～Goin’on～Ready Smile\!\!～Shining
Star～Memorial（電視動畫《[星光樂園](../Page/星光樂園.md "wikilink")》第一季片頭曲1—3、第二季片頭曲1—3、第三季片頭曲1及片頭曲3；電視動畫《[偶像時間星光樂園](../Page/偶像時間星光樂園.md "wikilink")》片頭曲3）／i☆Ris

:\# Baby Sweet Berry
Love（電視動畫《[變態王子與不笑貓](../Page/變態王子與不笑貓.md "wikilink")》片尾曲）／[小倉唯](../Page/小倉唯.md "wikilink")

:\# 永遠少年（電視動畫《[音樂少女](../Page/音樂少女.md "wikilink")》片頭曲）／小倉唯

:\# 零／[蒼井翔太](../Page/蒼井翔太.md "wikilink")

:\# Eclipse（電視動畫《[DEVILSLINE
惡魔戰線](../Page/DEVILSLINE_惡魔戰線.md "wikilink")》片頭曲）／蒼井翔太

:\#
（電視動畫《[歡迎來到實力至上主義的教室](../Page/歡迎來到實力至上主義的教室.md "wikilink")》片頭曲）／[ZAQ](../Page/ZAQ.md "wikilink")

:\# JOURNEY（劇場版《[中二病也想談戀愛！-Take On
Me-](../Page/中二病也想談戀愛！.md "wikilink")》片頭曲）／ZAQ

:\#
（電視動畫《[血界戰線](../Page/血界戰線.md "wikilink")》片尾曲）／ZAQ×[OxT](../Page/OxT.md "wikilink")

:\# GO CRY
GO（電視動畫《[OVERLORDⅡ](../Page/OVERLORD_\(小說\).md "wikilink")》片頭曲）／OxT

:\# Silent Solitude（電視動畫《OVERLORDⅢ》片尾曲）／OxT

:\# Brand New Theater\!（遊戲《[偶像大師
百萬人演唱會！劇場時光](../Page/偶像大師_百萬人演唱會！劇場時光.md "wikilink")》主題曲）／[偶像大師
百萬人演唱會！ MILLION STARS](../Page/偶像大師_百萬人演唱會！.md "wikilink")

:\# 混合曲：Princess Be Ambitious\!\!～Angelic Parade♪～FairyTale／偶像大師 百萬人演唱會！
MILLION STARS

:\# UNION\!\!／偶像大師 百萬人演唱會！ MILLION STARS

:\# （電視動畫《[偵探歌劇 少女福爾摩斯](../Page/偵探歌劇_少女福爾摩斯.md "wikilink")》片頭曲）／[Milky
Holmes](../Page/Milky_Holmes.md "wikilink")

:\# 混合曲：～（遊戲《偵探歌劇 少女福爾摩斯》片頭曲、片尾曲）／Milky Holmes

:\# Shining Storm ～～（遊戲《[超級機械人大戰OG THE MOON
DWELLERS](../Page/超級機械人大戰OG_THE_MOON_DWELLERS.md "wikilink")》主題曲）／[JAM
Project](../Page/JAM_Project.md "wikilink")

:\# THE HERO \!\! ～～（電視動畫《[一拳超人](../Page/一拳超人.md "wikilink")》片頭曲）／JAM
Project

:\#
混合曲：VICTORY～GONG～Warriors（遊戲《[超級機械人大戰MX](../Page/超級機械人大戰MX.md "wikilink")》、《[第3次超級機械人大戰α
終焉之銀河](../Page/第3次超級機械人大戰α_終焉之銀河.md "wikilink")》、《[超級機械人大戰X](../Page/超級機械人大戰X.md "wikilink")》主題曲）／JAM
Project

:\# SKILL（遊戲《[第2次超級機械人大戰α](../Page/第2次超級機械人大戰α.md "wikilink")》主題曲）

:\# Stand by...MUSIC\!\!\!／ASL2018演出者

## 2019年

### Animelo Summer Live 2019 -STORY-

  - 舉辦日期：2019年8月30日－9月1日
  - 會場：埼玉超級體育館

#### 主題曲

  - 「CROSSING STORIES」
    作詞：林英樹，作曲、編曲：佐藤純一（fhána）
    主唱：[亞咲花](../Page/亞咲花.md "wikilink")、[石原夏織](../Page/石原夏織.md "wikilink")、[オーイシマサヨシ](../Page/大石昌良.md "wikilink")、[ZAQ](../Page/ZAQ.md "wikilink")、[鈴木KONOMI](../Page/鈴木KONOMI.md "wikilink")、[sphere](../Page/sphere_\(聲優團體\).md "wikilink")、[TRUE](../Page/唐澤美帆.md "wikilink")、towana（[fhána](../Page/fhána.md "wikilink")）、幹葉（）、[三森鈴子](../Page/三森鈴子.md "wikilink")

#### 出演者

  - 8月30日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [ReoNa](../Page/ReoNa.md "wikilink")
  - [Roselia](../Page/BanG_Dream!_#Roselia.md "wikilink")
  - [i☆Ris](../Page/I☆Ris.md "wikilink")
  - [オーイシマサヨシ](../Page/大石昌良.md "wikilink")
  - [鈴木KONOMI](../Page/鈴木KONOMI.md "wikilink")
  - [三森鈴子](../Page/三森鈴子.md "wikilink")
  - [sphere](../Page/sphere_\(聲優團體\).md "wikilink")
  - [石原夏織](../Page/石原夏織.md "wikilink")
  - [fhána](../Page/fhána.md "wikilink")
  - [伊藤美來](../Page/伊藤美來.md "wikilink")

</div>

  - 8月31日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [亞咲花](../Page/亞咲花.md "wikilink")
  - [寺島拓篤](../Page/寺島拓篤.md "wikilink")
  - [偶像大師SideM](../Page/偶像大師SideM.md "wikilink")
  - [水瀨祈](../Page/水瀨祈.md "wikilink")
  - [TrySail](../Page/TrySail.md "wikilink")
  - [TRUE](../Page/唐澤美帆.md "wikilink")
  - [茅原實里](../Page/茅原實里.md "wikilink")
  - [鈴木實里](../Page/鈴木實里.md "wikilink")
  - [Minami](../Page/Minami.md "wikilink")
  - [Aqours](../Page/Aqours.md "wikilink")

</div>

  - 9月1日

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - buzz★Vibes

  - [内田雄馬](../Page/内田雄馬.md "wikilink")

  - [內田真禮](../Page/內田真禮.md "wikilink")

  - [蒼井翔太](../Page/蒼井翔太.md "wikilink")

  - [ZAQ](../Page/ZAQ.md "wikilink")

  - [JAM Project](../Page/JAM_Project.md "wikilink")

  -
  - [Poppin'Party](../Page/BanG_Dream!_#Poppin'Party.md "wikilink")

  - [藍井艾露](../Page/藍井艾露.md "wikilink")

  - [小倉唯](../Page/小倉唯.md "wikilink")

</div>

## 參考資料

## 外部連結

  - [Animelo Summer Live 最新一屆官方網站](http://anisama.tv/index.php)

<!-- end list -->

  -   - [Animelo Summer Live 2005 -THE BRIDGE-
        官方網站](http://anisama.tv/2005/)

      - [Animelo Summer Live 2006 -OUTRIDE-
        官方網站](http://anisama.tv/2006/)

      - [Animelo Summer Live 2007 Generation-A
        官方網站](http://anisama.tv/2007/)

      - [Animelo Summer Live 2008 -CHALLENGE-
        官方網站](http://anisama.tv/2008/)

      - [Animelo Summer Live 2009 -RE:BRIDGE-
        官方網站](http://anisama.tv/2009/)

      - [Animelo Summer Live 2010 -EVOLUTION-
        官方網站](http://anisama.tv/2010/)

      - [Anisama in Shanghai Live 2011 -Only One-
        官方網站](https://web.archive.org/web/20110423073244/http://pc.animelo.jp/shanghai/)

      - [Animelo Summer Live 2011 -rainbow-
        官方網站](http://anisama.tv/2011/)

      - [Animelo Summer Live 2012 -infinity∞-
        官方網站](http://anisama.tv/2012/)

      - [Animelo Summer Live 2013 -FLAG NINE-
        官方網站](http://anisama.tv/2013/)

      - [Animelo Summer Live 2014 -ONENESS-
        官方網站](http://anisama.tv/2014/)

      - [Animelo Summer Live 2015 -THE GATE-
        官方網站](http://anisama.tv/2015/)

      - [Animelo Summer Live 2016 刻-TOKI- 官方網站](http://anisama.tv/2016/)

      - [Animelo Summer Live 2017 THE CARD
        官方網站](http://anisama.tv/2017/)

      - [Animelo Summer Live 2018 OK\! 官方網站](https://anisama.tv/2018/)

      - [Animelo Summer Live 2019 -STORY-
        官方網站](https://anisama.tv/2019/)

[\*](../Category/動畫音樂會.md "wikilink")

1.

2.

3.

4.
5.
6.
7.

8.
9.
10.

11.
12.
13.

14.
15.
16.

17.

18.
19.
20.

21.
22.