**Aniplex株式会社**（）通常簡寫**Aniplex**或**ANX**\[1\]，是[日本索尼音樂旗下以](../Page/日本索尼音樂.md "wikilink")[動畫為主](../Page/動畫.md "wikilink")，從事[发行](../Page/发行.md "wikilink")、[企画](../Page/企画.md "wikilink")、[製作及](../Page/製作.md "wikilink")[販賣等業務的](../Page/販賣.md "wikilink")[公司](../Page/公司.md "wikilink")。

## 沿革

  - 1995年9月：前身「株式会社SPE\[2\] Music Publishing」成立。
  - 1997年1月：由[日本索尼音樂](../Page/日本索尼音樂.md "wikilink")（SMEJ）及[日本索尼影業](../Page/索尼影業.md "wikilink")（SPEJ）合資成立「株式会社SPE
    Visual Works」繼承業務。
  - 2001年1月：納入日本索尼音樂旗下，更名為「株式会社SME\[3\] Visual Works」。
  - 2003年4月：更名為「株式会社Aniplex」。
  - 2005年3月：成立美國分公司「Aniplex of America Inc.」；5月，子公司[A-1
    Pictures設立](../Page/A-1_Pictures.md "wikilink")。
  - 2006年4月：針對行動電話游戏业务的「株式会社番町製作所」設立。
  - 2017年4月：「番町製作所」更名为「Quatro A」\[4\]。

## 所属艺人

  - [井上麻里奈](../Page/井上麻里奈.md "wikilink")

  - [久木田薫](../Page/久木田薫.md "wikilink")

  -
  - [大竹英二](../Page/大竹英二.md "wikilink")

  - [ドルチェ・デ・ムジカ](../Page/ドルチェ・デ・ムジカ.md "wikilink")

  -
  -
  - [河野万里奈](../Page/河野万里奈.md "wikilink")

  -
  - [豊永利行](../Page/豊永利行.md "wikilink")

## 主要参与作品

### SPE Visual Works时期

包括更名后的SME Visual Works的关联作品。

  - [浪客剑心](../Page/浪客剑心.md "wikilink")

  - [晴天小猪](../Page/晴天小猪.md "wikilink")

  - [秀逗博士](../Page/秀逗博士.md "wikilink")

  - [福星大嘴鸟](../Page/福星大嘴鸟.md "wikilink")

  - [波波罗古洛伊斯物语](../Page/波波罗古洛伊斯物语.md "wikilink")

  - [麻辣教师GTO](../Page/麻辣教师GTO.md "wikilink")

  - [学校怪谈](../Page/学校怪谈_\(动画\).md "wikilink")

  -
  - [R.O.D -READ OR DIE-](../Page/R.O.D_-READ_OR_DIE-.md "wikilink")

  - [机动战士高达SEED](../Page/机动战士高达SEED.md "wikilink")

### Aniplex（2003年-）

#### TV动画

包含TV动画的剧场版（动画电影）

  - [阿貴的家族](../Page/阿貴的家族.md "wikilink")
  - [偶像大师](../Page/偶像大师_\(动画\).md "wikilink")
      - 偶像大师 剧场版 迈向闪耀的彼端！
  - [青之驱魔师](../Page/青之驱魔师.md "wikilink")
      - 青之驱魔师 剧场版
  - [我们仍未知道那天所看见的花的名字。](../Page/我们仍未知道那天所看见的花的名字。.md "wikilink")
      - 剧场版 我们仍未知道那天所看见的花的名字。
  - [R.O.D -THE TV-](../Page/R.O.D_-THE_TV-.md "wikilink")
  - [ALDNOAH.ZERO](../Page/ALDNOAH.ZERO.md "wikilink")
  - [韦驮天翔](../Page/IDATEN翔.md "wikilink")
  - [妖狐×僕SS](../Page/妖狐×僕SS.md "wikilink")
  - [吸血鬼骑士系列](../Page/吸血鬼骑士.md "wikilink")
  - [宇宙兄弟](../Page/宇宙兄弟.md "wikilink")
  - [宇宙战舰大和号2199](../Page/宇宙战舰大和号2199.md "wikilink")
  - [A频道](../Page/A频道.md "wikilink")
  - [天使心](../Page/天使心_\(北条司\).md "wikilink")
  - [Angel Beats\!](../Page/Angel_Beats!.md "wikilink")
  - [盗贼王JING](../Page/盗贼王JING.md "wikilink")
  - [王牌投手 振臂高挥系列](../Page/王牌投手_振臂高挥.md "wikilink")
  - [半妖少女绮丽谭](../Page/半妖少女绮丽谭.md "wikilink")
  - [我的妹妹哪有这么可爱系列](../Page/我的妹妹哪有这么可爱！_\(动画\).md "wikilink")
  - [我女友与青梅竹马的惨烈修罗场](../Page/我女友与青梅竹马的惨烈修罗场.md "wikilink")
  - [爱丽丝学园](../Page/爱丽丝学园.md "wikilink")
  - [革命机Valvrave](../Page/革命机Valvrave.md "wikilink")
  - [陰守忍者](../Page/陰守忍者.md "wikilink")
  - [刀語](../Page/刀語.md "wikilink")\[5\]\[6\]
  - [神樣中學生](../Page/神樣中學生.md "wikilink")（※主題歌制作は[ブロッコリーが担当](../Page/ブロッコリー_\(企業\).md "wikilink")）
  - [伽利略少女](../Page/伽利略少女.md "wikilink")
  - [GUNDAM系列](../Page/GUNDAM系列.md "wikilink")（※映像制作は萬代影視が担当）\[7\]
      - [機動戰士GUNDAM 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")\[8\]
      - [劇場版 機動戰士GUNDAM00 -A wakening of the
        Trailblazer-](../Page/劇場版_機動戰士GUNDAM00_-A_wakening_of_the_Trailblazer-.md "wikilink")
  - [神薙](../Page/神薙.md "wikilink")
  - [牙 -KIBA-](../Page/牙_\(動畫\).md "wikilink")
  - [少年同盟系列](../Page/少年同盟.md "wikilink")
  - [ギャラリーフェイク](../Page/ギャラリーフェイク.md "wikilink")
  - [罪惡王冠](../Page/罪惡王冠.md "wikilink")
  - [KILL la KILL](../Page/KILL_la_KILL.md "wikilink")
  - [金色琴弦 〜primo passo〜](../Page/金色琴弦.md "wikilink")
      - 金色琴弦 〜secondo passo〜
  - [銀魂系列](../Page/銀魂_\(動畫\).md "wikilink")
      - [銀魂劇場版 新譯紅櫻篇](../Page/銀魂劇場版_新譯紅櫻篇.md "wikilink")
      - [銀魂劇場版 完結篇 永遠的萬事屋](../Page/銀魂劇場版_完結篇_永遠的萬事屋.md "wikilink")
  - [银之匙 Silver Spoon](../Page/银之匙_Silver_Spoon.md "wikilink")
  - [冰上萬花筒](../Page/冰上萬花筒.md "wikilink")
  - [Code
    Geass系列](../Page/Code_Geass反叛的魯路修.md "wikilink")（※映像制作は萬代影視が担当。※音楽制作はキャラクターソング関係含め[ビクター/フライングドッグ主導](../Page/フライングドッグ.md "wikilink")\[9\]。劇場版である[亡国のアキトには不参加](../Page/コードギアス_亡国のアキト.md "wikilink")）
  - [グイン・サーガ](../Page/グイン・サーガ_\(テレビアニメ\).md "wikilink")
  - [黑執事系列](../Page/黑執事.md "wikilink")
  - [穿透幻影的太阳](../Page/穿透幻影的太阳.md "wikilink")
  - [戀愛與選舉與巧克力](../Page/戀愛與選舉與巧克力.md "wikilink")\[10\]
  - [GO\!GO\!575](../Page/project575.md "wikilink")（※製作協力）
  - [Servant x Service](../Page/Servant_x_Service.md "wikilink")
  - [不起眼女主角培育法](../Page/不起眼女主角培育法.md "wikilink")
  - [鎖鎖美小姐@不好好努力](../Page/鎖鎖美小姐@不好好努力.md "wikilink")\[11\]
  - [武士弗拉明戈](../Page/武士弗拉明戈.md "wikilink")
  - [東京殘響](../Page/東京殘響.md "wikilink")
  - [四月是你的謊言](../Page/四月是你的謊言.md "wikilink")
  - [屍鬼](../Page/屍鬼.md "wikilink")
  - [地獄少女系列](../Page/地獄少女.md "wikilink")
  - [人造人間キカイダー THE
    ANIMATION](../Page/人造人間キカイダー_THE_ANIMATION.md "wikilink")
  - [STAR DRIVER 閃亮的塔科特](../Page/STAR_DRIVER_閃亮的塔科特.md "wikilink")
      - [STAR DRIVER THE
        MOVIE](../Page/STAR_DRIVER_閃亮的塔科特#劇場版.md "wikilink")
  - [推理之絆](../Page/推理之絆.md "wikilink")
  - [世紀末超自然學院](../Page/世紀末超自然學院.md "wikilink")
  - [世界征服
    謀略之星](../Page/世界征服_謀略之星.md "wikilink")（※オープニング主題歌は[フライングドッグ](../Page/フライングドッグ.md "wikilink")）\[12\]
  - [鶺鴒女神シリーズ](../Page/鶺鴒女神.md "wikilink")\[13\]
  - [絕園的暴風雨](../Page/絕園的暴風雨.md "wikilink")
  - [戰姬絕唱SYMPHOGEAR](../Page/戰姬絕唱SYMPHOGEAR.md "wikilink")（※[製作委員会への出資のみ](../Page/製作委員会方式.md "wikilink")。映像製作・音楽制作・製作委の主幹事は[キングレコードが担当](../Page/キングレコード.md "wikilink")。第2期には不参加。）
  - [闪光的夜袭](../Page/闪光的夜袭.md "wikilink")
  - [戰國BASARA系列](../Page/戰國BASARA.md "wikilink")（※テレビ版の第2期にあたる「戦国BASARA弐」のみソニー・ミュージック名義として参加。第1期ならびに劇場版には不参加。）
  - [戰場女武神](../Page/戰場女武神.md "wikilink")
  - [ぜんまいざむらい](../Page/ぜんまいざむらい.md "wikilink")
  - [009-1](../Page/009-1.md "wikilink")
  - [空·之·音](../Page/空·之·音.md "wikilink")
  - [DARKER THAN BLACK系列](../Page/DARKER_THAN_BLACK.md "wikilink")
      - DARKER THAN BLACK -黑之契約者-
      - DARKER THAN BLACK -流星之雙子-
  - [超譯百人一首戀歌](../Page/超譯百人一首戀歌.md "wikilink")
  - [釣球](../Page/釣球.md "wikilink")
  - [驅魔少年](../Page/驅魔少年.md "wikilink")
  - [鉄腕バーディー DECODEシリーズ](../Page/鉄腕バーディー_DECODE.md "wikilink")
  - [奔向地球](../Page/奔向地球.md "wikilink")\[14\]
  - [拯救德尔托拉](../Page/拯救德尔托拉.md "wikilink")
  - [飛天小女警Z](../Page/飛天小女警Z.md "wikilink")
  - [無頭騎士異聞錄 DuRaRaRa\!\!](../Page/無頭騎士異聞錄_DuRaRaRa!!.md "wikilink")
  - [天元突破紅蓮螺巖](../Page/天元突破紅蓮螺巖.md "wikilink")
      - [劇場版 天元突破紅蓮螺巖 紅蓮篇](../Page/天元突破紅蓮螺巖#劇場版.md "wikilink")
      - [劇場版 天元突破紅蓮螺巖 螺巖篇](../Page/天元突破紅蓮螺巖#劇場版.md "wikilink")
  - [天保異聞 妖奇士](../Page/天保異聞_妖奇士.md "wikilink")
  - [咎狗之血](../Page/咎狗之血.md "wikilink")\[15\]
  - [DOG
    DAYS系列](../Page/DOG_DAYS.md "wikilink")（※主題歌制作はキングレコードが担当。\[16\]）
  - [鄰座的怪同學](../Page/鄰座的怪同學.md "wikilink")
  - [夏色奇蹟](../Page/夏色奇蹟.md "wikilink")\[17\]
  - [夏目友人帳系列](../Page/夏目友人帳.md "wikilink")
  - [火影忍者](../Page/火影忍者動畫集數列表.md "wikilink")
      - [火影忍者劇場版：大活劇！雪姬忍法帖！！](../Page/火影忍者劇場版：大活劇！雪姬忍法帖！！.md "wikilink")
      - [火影忍者劇場版：大激突！夢幻的地底遺跡](../Page/火影忍者劇場版：大激突！夢幻的地底遺跡.md "wikilink")
      - [火影忍者劇場版：大興奮！三日月島上的動物騷亂](../Page/火影忍者劇場版：大興奮！三日月島上的動物騷亂.md "wikilink")
  - [火影忍者疾風傳](../Page/火影忍者疾風傳.md "wikilink")
      - [火影忍者疾風傳劇場版](../Page/火影忍者疾風傳劇場版.md "wikilink")（※主題歌制作は[EMIミュージック・ジャパン](../Page/EMIミュージック・ジャパン.md "wikilink")（現[ユニバーサル
        ミュージック](../Page/ユニバーサルミュージック_\(日本\).md "wikilink")/EMIレコーズ・ジャパン）が担当）
      - [火影忍者疾風傳劇場版 牽絆](../Page/火影忍者疾風傳劇場版_牽絆.md "wikilink")
      - [火影忍者疾風傳劇場版 火意志的繼承者](../Page/火影忍者疾風傳劇場版_火意志的繼承者.md "wikilink")
      - [火影忍者疾風傳劇場版 失落之塔](../Page/火影忍者疾風傳劇場版_失落之塔.md "wikilink")
      - [火影忍者劇場版 血獄](../Page/火影忍者劇場版_血獄.md "wikilink")
      - [火影忍者劇場版 忍者之路](../Page/火影忍者劇場版_忍者之路.md "wikilink")
      - [火影忍者SD 李洛克的青春全力忍傳](../Page/李洛克的青春全力忍傳.md "wikilink")
  - [未來都市NO.6](../Page/未來都市NO.6.md "wikilink")
  - [偽戀](../Page/偽戀.md "wikilink")
  - [練馬大根ブラザーズ](../Page/練馬大根ブラザーズ.md "wikilink")
  - [鋼之鍊金術師](../Page/鋼之鍊金術師_\(動畫\).md "wikilink")
      - [劇場版 鋼之鍊金術師 森巴拉的征服者](../Page/劇場版_鋼之鍊金術師_森巴拉的征服者.md "wikilink")
  - [鋼之鍊金術師 FULLMETAL
    ALCHEMIST](../Page/鋼之鍊金術師_FULLMETAL_ALCHEMIST.md "wikilink")
      - [鋼之鍊金術師 嘆息之丘的聖星](../Page/鋼之鍊金術師_嘆息之丘的聖星.md "wikilink")
  - [Baccano\!](../Page/Baccano!.md "wikilink")
  - [天國之吻](../Page/天國之吻.md "wikilink")
  - [鬍子小雞](../Page/鬍子小雞.md "wikilink")
  - [向陽素描系列](../Page/向陽素描.md "wikilink")\[18\]
  - [Vividred Operation](../Page/Vividred_Operation.md "wikilink")
  - [乒乓 THE ANIMATION](../Page/乒乓_\(漫畫\).md "wikilink")
  - [Fate系列](../Page/Fate/stay_night.md "wikilink")（不含[Fate/stay
    night及其](../Page/Fate/stay_night#電視動畫（第1作）.md "wikilink")[劇場版](../Page/Fate/stay_night#劇場版.md "wikilink")）（※「[Fate/stay
    night](../Page/Fate/stay_night.md "wikilink")｣為當時旗下的製作。\[19\]）
      - [Fate/Zero](../Page/Fate/Zero.md "wikilink")
      - [Fate/stay night Unlimited Blade
        Works](../Page/Fate/stay_night#電視動畫（第2作）.md "wikilink")\[20\]
      - [劇場版 Fate/stay night Heaven's
        Feel](../Page/Fate/stay_night_Heaven's_Feel.md "wikilink")
  - [FLAG](../Page/FLAG.md "wikilink")
  - [BLACK★ROCK
    SHOOTER](../Page/BLACK★ROCK_SHOOTER.md "wikilink")（※[OVA版は](../Page/OVA.md "wikilink")[グッドスマイルカンパニーの製作](../Page/グッドスマイルカンパニー.md "wikilink")）
  - [BLOOD+](../Page/BLOOD+.md "wikilink")
  - [BLOOD-C](../Page/BLOOD-C.md "wikilink")（※エンディングテーマのみキングレコードからのリリース\[21\]）
      - [劇場版 BLOOD-C The Last
        Dark](../Page/BLOOD-C#劇場版.md "wikilink")（※主題歌制作はキングレコードが担当\[22\]）
  - [BLEACH](../Page/BLEACH_\(動畫\).md "wikilink")
      - [劇場版BLEACH 別處的記憶](../Page/BLEACH_漂靈：劇場版－別處的記憶.md "wikilink")
      - 劇場版BLEACH The DiamondDust Rebellion 另一把冰輪丸
      - 劇場版BLEACH Fade to Black 呼喚你的名字
      - 劇場版BLEACH 地獄篇
  - [女神异闻录 ～三位一体之魂～](../Page/女神异闻录_～三位一体之魂～.md "wikilink")
  - [女神異聞錄4 the
    ANIMATION](../Page/女神異聞錄4.md "wikilink")（原作担当の[インデックス](../Page/インデックス_\(1995年設立の企業\).md "wikilink")（[アトラス](../Page/アトラス_\(ゲーム会社\).md "wikilink")）が音楽制作に協力）\[23\]
      - 女神異聞錄4 the Golden ANIMATION
  - [亡念之扎姆德](../Page/亡念之扎姆德.md "wikilink")
  - [放浪男孩](../Page/放浪男孩.md "wikilink")
  - [炎之蜃氣樓](../Page/炎之蜃氣樓.md "wikilink")
  - [魔奇少年](../Page/魔奇少年.md "wikilink")
  - [魔法科高中的劣等生](../Page/魔法科高中的劣等生.md "wikilink")
  - [魔法少女小圓](../Page/魔法少女小圓.md "wikilink")\[24\]
      - [劇場版 魔法少女小圓 ［前篇］起始的物語](../Page/劇場版_魔法少女小圓.md "wikilink")（※配給を含む）
      - [劇場版 魔法少女小圓 ［後篇］永遠的物語](../Page/劇場版_魔法少女小圓.md "wikilink")（※配給を含む）
      - [劇場版 魔法少女小圓
        ［新篇］叛逆的物語](../Page/劇場版_魔法少女小圓.md "wikilink")（※同作品のみ[ワーナー・ブラザース映画が配給](../Page/ワーナー_エンターテイメント_ジャパン.md "wikilink")）
  - [超元氣3姊妹系列](../Page/超元氣3姊妹.md "wikilink")\[25\]
  - [奇蹟列車\~歡迎來到大江戶線\~](../Page/奇蹟列車.md "wikilink")\[26\]
  - [蟲師
    特別篇「蝕日之翳」](../Page/蟲師.md "wikilink")（※TV第1期は[マーベラスエンターテイメント](../Page/マーベラスAQL.md "wikilink")・[エイベックス・エンタテインメントの製作](../Page/エイベックス・エンタテインメント.md "wikilink")）
      - [蟲師 續章](../Page/蟲師.md "wikilink")
  - [目隱都市的演繹者](../Page/陽炎計劃.md "wikilink")
  - [物語系列](../Page/物語系列.md "wikilink")
      - [化物語](../Page/化物語.md "wikilink")
      - [偽物語](../Page/偽物語.md "wikilink")
      - [傷物語](../Page/傷物語.md "wikilink")
      - [貓物語（黒）](../Page/貓物語.md "wikilink")
      - [物語系列 第二季](../Page/物語系列_第二季.md "wikilink")
  - [農大菌物語
    Returns](../Page/農大菌物語.md "wikilink")（※第1期は[アスミック・エースの製作](../Page/アスミック・エース.md "wikilink")）
  - [日式麵包王](../Page/日式麵包王.md "wikilink")
  - [戀愛情結](../Page/戀愛情結.md "wikilink")（※主題歌制作は[ジャニーズ・エンタテイメントが担当](../Page/ジャニーズ・エンタテイメント.md "wikilink")）
  - [戀愛研究所](../Page/戀愛研究所.md "wikilink")
  - [龍孃七七七埋藏的寶藏](../Page/龍孃七七七埋藏的寶藏.md "wikilink")
  - [靈異E接觸](../Page/靈異E接觸.md "wikilink")
  - [ロビーとケロビー](../Page/ロビーとケロビー.md "wikilink")（[アニメロビー内](../Page/アニメロビー.md "wikilink")
    ※主題歌制作は[zetimaが担当](../Page/アップフロントワークス.md "wikilink")）
  - [ROBOTICS;NOTES](../Page/ROBOTICS;NOTES.md "wikilink")
  - [迷糊餐廳系列](../Page/迷糊餐廳.md "wikilink")
  - [ワンダーベビルくん](../Page/ワンダーベビルくん.md "wikilink")（[天才ビットくん内](../Page/天才ビットくん.md "wikilink")）
  - [魔术快斗1412](../Page/魔术快斗.md "wikilink")
  - [亞人醬有話要說](../Page/亞人醬有話要說.md "wikilink")
  - [Re:CREATORS](../Page/Re:CREATORS.md "wikilink")
  - [調教咖啡廳](../Page/調教咖啡廳.md "wikilink")
  - [DARLING in the FRANXX](../Page/DARLING_in_the_FRANXX.md "wikilink")
  - [卫宫家今天的餐桌风景](../Page/卫宫家今天的餐桌风景.md "wikilink")
  - [Banana Fish](../Page/Banana_Fish.md "wikilink")

#### 原创动画

  - アニメ文庫\[27\]
      - [百合星人奈緒子美眉](../Page/百合星人奈緒子美眉.md "wikilink")
      - [みのりスクランブル\!](../Page/みのりスクランブル!.md "wikilink")
      - [ギョ](../Page/ギョ.md "wikilink")
  - [アラタなるセカイ](../Page/アラタなるセカイ.md "wikilink") -
    [アスキー・メディアワークス創立](../Page/アスキー・メディアワークス.md "wikilink")20周年及び専属[作家](../Page/ライトノベル作家.md "wikilink")・[入間人間](../Page/入間人間.md "wikilink")
    作家デビュー10周年記念、[角川書店](../Page/角川書店.md "wikilink")/[角川グループパブリッシングとの](../Page/角川グループパブリッシング.md "wikilink")4社共同事業。
  - [A頻道+smile](../Page/A頻道.md "wikilink")
  - [盗贼王JING in Seventh Heaven](../Page/盗贼王JING.md "wikilink")
  - [機動戰士GUNDAM
    UC](../Page/機動戰士GUNDAM_UC.md "wikilink")（※映像制作は萬代影視が担当）\[28\]
  - [柯塞特的肖像](../Page/柯塞特的肖像.md "wikilink")
  - [深海潛艦707R](../Page/深海潛艦707R.md "wikilink")
  - [戰場女武神3
    為誰而負的槍傷](../Page/戰場女武神3.md "wikilink")（※ブラックパッケージの発売・販売およびブルーパッケージの発売は[セガ](../Page/セガ.md "wikilink")、音楽制作は[原作シリーズでも関与の](../Page/戦場のヴァルキュリア.md "wikilink")[ベイシスケイプとランティスがそれぞれ担当](../Page/ベイシスケイプ.md "wikilink")）

#### 剧场版动画

オリジナルアニメとして製作された作品（テレビアニメ版の派生は前掲）

  - [歡迎來到宇宙劇場](../Page/歡迎來到宇宙劇場.md "wikilink")（[製作委員会への出資](../Page/製作委員会方式.md "wikilink")）
  - [空之境界](../Page/空之境界.md "wikilink")（同上）
  - [五彩繽紛](../Page/五彩繽紛.md "wikilink")（同上）
  - [劇場版 遙遠時空
    舞一夜](../Page/遙遠時空#遙遠時空〜舞一夜〜.md "wikilink")（※製作委員会への出資。映像制作と製作委の主幹事は萬代影視が担当）
  - [聖☆哥傳](../Page/聖哥傳.md "wikilink")
  - [センコロール](../Page/センコロール.md "wikilink")
  - [惡童](../Page/惡童.md "wikilink")（製作委員会への出資）
  - [被狙击的学园](../Page/被狙击的学园.md "wikilink")（アニメ映画版、同上）
  - [女神異聞錄3 THE
    MOVIE](../Page/女神異聞錄3.md "wikilink")（原作担当のインデックス（アトラス）が音楽制作に協力）
      - 女神異聞錄3 THE MOVIE \#1 Spring of Birth
      - 女神異聞錄3 THE MOVIE \#2 Midsummer Knight's Dream
  - [螢火之森](../Page/螢火之森.md "wikilink")
  - [魔女っこ姉妹のヨヨとネネ](../Page/のろい屋しまい.md "wikilink")（製作委員会への出資）
  - [魔法少女奈葉 The MOVIE
    1st](../Page/魔法少女奈葉_The_MOVIE_1st.md "wikilink")（※製作委員会への出資、配給。音楽・映像制作と製作委の主幹事は従来のテレビアニメ版通り[キングレコードが担当](../Page/キングレコード.md "wikilink")）
  - [魔法少女奈葉 The MOVIE 2nd
    A's](../Page/魔法少女奈葉_The_MOVIE_2nd_A's.md "wikilink")（※同上）
  - [壳中少女](../Page/壳中少女.md "wikilink")（※同上。音楽・映像制作と製作委の主幹事はキングレコード（[スターチャイルド](../Page/スターチャイルド.md "wikilink")）が担当）
      - 壳中少女 壓縮
      - 壳中少女 燃燒
      - 壳中少女 排氣
  - [我想吃掉你的胰臟](../Page/我想吃掉你的胰臟.md "wikilink")

#### 手機遊戲

  - [Fate/Grand
    Order](../Page/Fate/Grand_Order.md "wikilink")（與[TYPE-MOON共同營運](../Page/TYPE-MOON.md "wikilink")
    開發商：DELiGHTWORKS，2015年8月上線）
  - [魔法紀錄
    魔法少女小圓外傳](../Page/魔法紀錄_魔法少女小圓外傳.md "wikilink")（開發商：f4samurai，2017年8月上線）
  - [閃耀幻想曲](../Page/閃耀幻想曲.md "wikilink")（開發商：，2017年12月上線）
  - 高校艦隊 指尖艦隊戰鬥（開發商：[Aniplex](../Page/Aniplex.md "wikilink")，2019年3月27日）

## 參見

  - [A-1 Pictures](../Page/A-1_Pictures.md "wikilink")

## 註腳

### 註釋

## 外部連結

  - [Aniplex](http://www.aniplex.co.jp)
  - [Aniplex USA](http://aniplexusa.com/)

[\*](../Category/Aniplex.md "wikilink")
[Category:日本媒體公司](../Category/日本媒體公司.md "wikilink")
[Category:日本索尼音樂娛樂](../Category/日本索尼音樂娛樂.md "wikilink")
[Category:動畫產業公司](../Category/動畫產業公司.md "wikilink")
[Category:千代田區公司](../Category/千代田區公司.md "wikilink")
[Category:音樂出版社](../Category/音樂出版社.md "wikilink")
[Category:电影发行公司](../Category/电影发行公司.md "wikilink")

1.  [Aniplex新聞稿](http://www.aniplex.co.jp/company/press100120.html)

2.  “SPE”為“Sony Pictures Entertainment”的縮寫。

3.  “SME”為“Sony Music Entertainment”的縮寫。

4.

5.  音楽制作はランティスが担当。

6.  [フジテレビ系](../Page/フジテレビジョン.md "wikilink")『[ノイタミナ](../Page/ノイタミナ.md "wikilink")』枠による再放映版ではソニーミュージック系による主題歌差し替えが行われる。

7.  リアル系では「[SEED
    DESTINY](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")」、SD系では「[SDガンダムフォース](../Page/SDガンダムフォース.md "wikilink")」以降の作品から。当作品以降は制作スタッフに同ジャンルの同業他社が加わっている事や他のスポンサーとの兼ね合いからソニーミュージック名義となっている。但し、[ガンダムビルドファイターズには不参加](../Page/ガンダムビルドファイターズ.md "wikilink")（こちらは[エイベックスが担当](../Page/エイベックス.md "wikilink")）。

8.  初版では[エピックソニーレコードから発売されていたが](../Page/エピックレコードジャパン.md "wikilink")、[ブルースペックCD化に伴うデジタルリマスター盤の発売から担当](../Page/ブルースペックCD.md "wikilink")（但し近年のガンダムシリーズにおける他のスポンサーとの兼ね合いから[GT
    music名義での販売](../Page/ソニー・ミュージックダイレクト.md "wikilink")）。

9.  アニプレックスは制作委員会出資のみ。主題歌制作はソニー・ミュージックエンタテインメント名義で協力。

10.
11.
12. [ノーツ](../Page/TYPE-MOON.md "wikilink")・[一迅社](../Page/一迅社.md "wikilink")・[MBSとの共同企画](../Page/毎日放送.md "wikilink")。[他系列局・ABCでも](../Page/朝日放送.md "wikilink")「[劇場版
    空の境界TV版](../Page/空の境界.md "wikilink")」放映中の提供スポットにてCM告知があったが別のネット局（[BS11でも放送](../Page/日本BS放送.md "wikilink")）では1クール前に放映終了して「[魔法少女まどか☆マギカ](../Page/魔法少女まどか☆マギカ.md "wikilink")（TV再放映版）」を放送している
    ため制作キー局としての権限は前者のMBSにある。

13. [都築真紀率いる](../Page/都築真紀.md "wikilink")[セブン・アークスグループがアニメ制作の作品において同社が音楽制作](../Page/セブン・アークス.md "wikilink")・主題歌制作共に手掛ける作品は現時点で同作品のみ（『[DOG
    DAYS](../Page/DOG_DAYS.md "wikilink")』シリーズでは映像制作および劇中音楽・挿入歌制作を担当）。

14. MBS制作枠のテレビアニメ。

15.
16. サウンドトラック制作は同社、キャラクターソング等も一部を除き同社が担当 。

17.
18.
19. [角川グループによる](../Page/角川ゲームス.md "wikilink")[リメイク版PSVゲームの音楽制作も直接親会社である](../Page/Fate/stay_night#Fate/stay_night_Réalta_Nua.md "wikilink")[ソニー・ミュージックエンタテインメントを通じて担当](../Page/ソニー・ミュージックエンタテインメント_\(日本\).md "wikilink")。

20. アニプレックス、[TYPE-MOON](../Page/TYPE-MOON.md "wikilink")、[ufotableの](../Page/ufotable.md "wikilink")3社共同によるリメイク作品

21. 主演がアーティスト声優・[水樹奈々のため](../Page/水樹奈々.md "wikilink")。当該各項を参照。

22.
23. 一部楽曲はゲーム版のものを流用。

24. 同社とニトロプラス・[芳文社](../Page/芳文社.md "wikilink")・シャフトとの共同企画。

25.
26.
27. アニプレックスと[アニメ制作会社の](../Page/アニメ制作会社.md "wikilink")[ufotableによるオリジナルレーベル](../Page/ユーフォーテーブル.md "wikilink")（[公式サイト](http://www.animebunko.com/)）。

28. アニプレックスが制作委員会に出資、音楽制作にも関与。但し、キャラクターソング・サウンドトラックのリリース分については前掲の都合上ミュージックレインが実質的に代行。