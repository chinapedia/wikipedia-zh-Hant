**Apache软件基金会**（Apache Software
Foundation，简称为ASF），是专门为支持[开源软件项目而办的一个非營利性组织](../Page/开源软件.md "wikilink")。在它所支持的Apache项目与子项目中，所发行的软件产品都遵循[Apache许可证](../Page/Apache许可证.md "wikilink")（Apache
License）。

## 发展历史

Apache软件基金会正式创建于1999年，它的创建者是一个自称为“Apache组织”的群体。这个“Apache组织”在1999年以前就已经存在很长时间了，这个组织的开发爱好者们聚集在一起，在美国伊利诺伊大学超级计算机应用程序国家中心（National
Center for Supercomputing
Applications，简称为[NCSA](../Page/NCSA.md "wikilink")）开发的NCSA
HTTPd服务器的基础上开发与维护了一个叫[Apache的](../Page/Apache.md "wikilink")[HTTP服务器](../Page/HTTP服务器.md "wikilink")。

最初NCSA HTTPd服务器是由Rob
McCool开发出来的，但是它的最初开发者们逐渐对这个软件失去了兴趣，并转移到了其他地方，造成了没有人来对这个[服务器软件提供更多的技术支持](../Page/服务器.md "wikilink")。因为这个[服务器的功能又如此强大](../Page/服务器.md "wikilink")，而代码可以自由下载修改与发布，当时这个[服务器软件的一些爱好者与用户开始自发起来](../Page/服务器.md "wikilink")，互相交流并分发自己修正后的软件版本，并不断改善其功能。为了更好进行沟通，Brian
Behlendorf自己建立了一个[邮件列表](../Page/邮件列表.md "wikilink")，把它作为这个群体（或者社区）交流技术、维护软件的一个媒介，把代码重写与维护的工作有效组织起来。这些开发者们逐渐地把他们这个群体称为“Apache组织”，把这个经过不断修正并改善的服务器软件命名为Apache服务器（Apache
Server）。

这个命名是根据[北美当地的一支](../Page/北美.md "wikilink")[印第安部落而来](../Page/印第安.md "wikilink")，这支部落以高超的军事素养和超人的忍耐力著称，19世纪后半期对侵占他们领土的入侵者进行了反抗。为了对这支[印第安部落表示敬仰之意](../Page/印第安.md "wikilink")，取该部落名称（Apache）作为服务器名。但一提到这个命名，这里还有流传着一段有意思的故事。因为这个服务器是在NCSA
HTTPd服务器的基础之上，通过众人努力，不断地修正、打补丁（Patchy）的产物，被戏称为“*A Patchy
Server*”（一个补丁服务器）。在这裡，因为“A
Patchy”与“Apache”是谐音，故最后正式命名为“[Apache
Server](../Page/Apache.md "wikilink")”。

后来由于商业需求的不断扩大，以Apache HTTP服务器为中心，启动了更多的与Apache项目并行的项目，比如[mod
perl](../Page/mod_perl.md "wikilink")、[PHP](../Page/PHP.md "wikilink")、Java
Apache等等。随着时间的推移、形势的变化，Apache软件基金会的项目列表也不断更新变化中——不断的有新项目启动，项目的中止以及项目的拆分与合并。比如一开始，[Jakarta就是为了发展](../Page/Jakarta项目.md "wikilink")[JAVA容器而启动的Java](../Page/JAVA.md "wikilink")
Apache项目，后来由于[昇陽電腦的建议](../Page/昇陽電腦.md "wikilink")，项目名称变为Jakarta。但当时该项目的管理者也没有想到Jakarta项目因为[JAVA的火爆而发展到如今一个囊括了众多基于](../Page/JAVA.md "wikilink")[JAVA语言](../Page/JAVA.md "wikilink")[开源软件子项目的项目](../Page/开源软件.md "wikilink")。以至后来，不得不把个别项目从Jakarta中独立出来，成为Apache软件基金会的顶级项目，[Struts项目就是其中之一](../Page/Struts.md "wikilink")。

最近，为了避免[SCO与](../Page/SCO_Group.md "wikilink")[UNIX](../Page/UNIX.md "wikilink")
[开源社区之间的发生](../Page/开源社区.md "wikilink")[纠纷降临在Apache软件基金会](../Page/SCO-Linux爭議.md "wikilink")（ASF）身上。Apache软件基金会（ASF）裡面开始采取一些措施，让众多的项目进行更多协调的、结构化管理，并保护自己的合法利益，避免一些潜在的合乎法律的侵犯（potential
legal attacks）。

## 组织构成

### 理事会

理事会（Board of
Directors）是管理与监督整个Apache软件基金会（ASF）的商务与日常事务，并让它们能符合章程的规定下正常地运作。

### 项目管理委员会

项目管理委员会（Project Management Committees，简称为PMC），主要负责保证一个或者多个开源社区的活动都能运转良好。

## 项目

  - [HTTP
    Server](../Page/Apache_HTTP_Server.md "wikilink")：可以在[UNIX](../Page/UNIX.md "wikilink")，[MS-Windows](../Page/Windows.md "wikilink")，[Macintosh和](../Page/Macintosh.md "wikilink")[Netware](../Page/Netware.md "wikilink")[操作系统下运行的](../Page/操作系统.md "wikilink")[HTTP服务器的项目](../Page/HTTP服务器.md "wikilink")。
  - [Ant](../Page/Apache_Ant.md "wikilink")：基于[Java语言的构建工具](../Page/Java.md "wikilink")，类似于[C语言的Make工具](../Page/C语言.md "wikilink")。
  - [AXIS2](../Page/AXIS2.md "wikilink")：Web服務（SOAP,
    WSDL）的處理器，基於AXIS1.X重新構建。
  - APR：（也就是：Apache Portable
    Runtime）[C语言实现的便携运行库的管理工具](../Page/C语言.md "wikilink")。
  - [Beehive](../Page/Apache_Beehive.md "wikilink")：为了简单构建[J2EE应用的](../Page/J2EE.md "wikilink")[对象模型](../Page/对象_\(计算机科学\).md "wikilink")。
  - [Apache Camel](../Page/Apache_Camel.md "wikilink")：一个开源的企业应用集成框架。
  - [Cocoon](../Page/Apache_Cocoon.md "wikilink")：一个基于组件技术和[XML和Web应用开发框架](../Page/XML.md "wikilink")。
  - [Cassandra](../Page/Cassandra.md "wikilink")：一個分散式，非關連型，[NoSQL的大型資料庫](../Page/NoSQL.md "wikilink")。
  - CloudStack：一个開源的雲計算IaaS管理平台。
  - Cordova：一個基於Javascrip html css 的混生App開發計劃。
  - DB：关于[資料庫管理系統的几个开源项目集合](../Page/資料庫管理系統.md "wikilink")。
  - [Apache::Deploy](../Page/Apache::Deploy.md "wikilink")：是一个(R)?ex的模块，用来快速发布网站到Apache和WAR文件到Tomcat.简化了多服务器应用的发布过程。支持基于Git的发布和rpm/deb发布，同时简化了构建rpm和deb包。
  - [Derby](../Page/Apache_Derby.md "wikilink")：一個純[JAVA的](../Page/JAVA.md "wikilink")[資料庫管理系統](../Page/資料庫管理系統.md "wikilink")。
  - Directory：基于[JAVA语言的](../Page/JAVA.md "wikilink")[目录服务器](../Page/目录服务器.md "wikilink")，支持[LDAP等目录访问协议](../Page/LDAP.md "wikilink")。
  - Excalibur：Apache Avalon项目的前身。
  - Forrest：一个发布系统框架的项目。
  - [Geronimo](../Page/Geronimo.md "wikilink")：[J2EE服务器](../Page/J2EE.md "wikilink")。
  - Gump：整合管理器。
  - [Hadoop](../Page/Hadoop.md "wikilink")：並行運算編程工具和分佈式文件系統。
  - [Harmony](../Page/Apache_Harmony.md "wikilink")：一个兼容[JAVA标准的](../Page/JAVA.md "wikilink")[JAVA语言的开源实现](../Page/JAVA.md "wikilink")。
  - [HiveMind](../Page/HiveMind.md "wikilink")：一个服务（Services）与配置（configuration）的微内核。
  - [iBATIS](../Page/iBATIS.md "wikilink")：一个基于[JAVA语言的](../Page/JAVA.md "wikilink")[数据持久化框架](../Page/数据持久化.md "wikilink")。
  - Incubator：为了帮助那些希望获取**Apache软件基金会**支持的计划进入**Apache软件基金会**的审核项目。
  - Jackrabbit：内容仓库API标准（Content Repository for Java Technology
    API，即[JSR](../Page/JSR.md "wikilink")-170）的一个开源实现项目。
  - [Jakarta](../Page/Jakarta项目.md "wikilink")：在ASF中，基于[Java语言的一组](../Page/Java.md "wikilink")[开源子项目的集合](../Page/开源.md "wikilink")，现在包含的子项目有：BCEL，BSF，Cactus，Commons，ECS，HttpComponents，JCS，JMeter，ORO，Regexp，Slide，Taglibs，Turbine，Velocity。
  - James：JAVA语言实现的[邮件](../Page/E-Mail.md "wikilink")[新闻服务器](../Page/新闻.md "wikilink")。
  - Labs：为基金会成员提供最新变更的思维的计划。
  - Lenya：[内容管理系统](../Page/内容管理系统.md "wikilink")。
  - [Logging](../Page/Logging.md "wikilink")：一个开发可以在[C++](../Page/C++.md "wikilink")、[Java](../Page/Java.md "wikilink")、[Perl](../Page/Perl.md "wikilink")、[PHP](../Page/PHP.md "wikilink")、[.NET](../Page/.NET.md "wikilink")
    [计算机语言下运行的通用](../Page/计算机语言.md "wikilink")[日志工具项目集合](../Page/日志.md "wikilink")。
  - [Lucene](../Page/Apache_Lucene.md "wikilink")：高性能的，基于[Java语言的](../Page/Java.md "wikilink")[全文检索项目](../Page/全文检索.md "wikilink")。
  - [Maven](../Page/Apache_Maven.md "wikilink")：项目集成构建工具。
  - [MyFaces](../Page/Apache_MyFaces.md "wikilink")：一个[JavaServer
    Faces](../Page/JavaServer_Faces.md "wikilink")（JSF）的实现框架。
  - mod_perl：为[Apache服务器提供](../Page/Apache.md "wikilink")[Perl语言整合的项目](../Page/Perl.md "wikilink")。
  - [POI](../Page/Apache_POI.md "wikilink")：提供API以供[JAVA程式對](../Page/JAVA.md "wikilink")[Microsoft
    Office格式檔案的讀](../Page/Microsoft_Office.md "wikilink")／寫。
  - [OpenOffice](../Page/Apache_OpenOffice.md "wikilink")：提供兼容MS.Office的文档、表格和演示稿的编辑和展示功能。
  - Portals：与[门户](../Page/门户.md "wikilink")（Portal）技术相关的几个项目集合。
  - Santuario：发展[XML安全性方面的项目](../Page/XML.md "wikilink")。
  - Shale：在[Struts之后](../Page/Struts.md "wikilink")，发展起来基于[JAVA语言Web应用框架](../Page/JAVA.md "wikilink")。
  - Shiro：权限管理
  - SpamAssassin：[垃圾邮件过滤器](../Page/垃圾邮件.md "wikilink")。
  - [Struts](../Page/Struts.md "wikilink")：一个基于[J2EE平台的](../Page/J2EE.md "wikilink")[MVC](../Page/MVC.md "wikilink")
    [设计模式的Web应用框架](../Page/设计模式.md "wikilink")。
  - [Subversion](../Page/Subversion.md "wikilink")：一個軟件版本管理系統。
  - [Tapestry](../Page/Tapestry.md "wikilink")：另一个[J2EE平台的](../Page/J2EE.md "wikilink")、能产生动态、高性能Web应用的框架。
  - TCL：为[Apache服务器提供](../Page/Apache.md "wikilink")[Tcl语言整合的项目](../Page/Tcl.md "wikilink")。
  - [Tomcat](../Page/Apache_Tomcat.md "wikilink")：一个运行[Java
    Servlet与](../Page/Servlet.md "wikilink")[JavaServer
    Pages](../Page/JSP.md "wikilink")（JSP）的容器。
  - [Web Services](../Page/Web_Services.md "wikilink")：与[Web
    Services技术相关的项目集合](../Page/Web_Services.md "wikilink")。
  - [Xalan](../Page/Apache_Xalan.md "wikilink")：[XML转换处理器](../Page/XML.md "wikilink")。
  - [Xerces](../Page/Apache_Xerces.md "wikilink")：一组可以在[Java](../Page/Java.md "wikilink")，[C++](../Page/C++.md "wikilink")，[Perl](../Page/Perl.md "wikilink")
    [计算机语言下使用的](../Page/计算机语言.md "wikilink")[XML解析器项目](../Page/XML.md "wikilink")。
  - [Apache
    XML](../Page/Apache_XML.md "wikilink")：[XML解决方案](../Page/XML.md "wikilink")。
  - XMLBeans：基于[JAVA语言XML对象绑定工具](../Page/JAVA.md "wikilink")。
  - XML Graphics：发展XML与图形进行转换的计划项目。

## 外部链接

  - [Apache官方站点](http://www.apache.org)

## 参见

  - [自由软件运动](../Page/自由软件运动.md "wikilink")
  - [自由软件](../Page/自由软件.md "wikilink")
  - [GNU](../Page/GNU.md "wikilink")

{{-}}

[Apache软件基金会](../Category/Apache软件基金会.md "wikilink")
[Category:自由軟體組織](../Category/自由軟體組織.md "wikilink")