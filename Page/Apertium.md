**Apertium**是一個[機器翻譯平台](../Page/機器翻譯.md "wikilink")，由[西班牙政府和](../Page/西班牙.md "wikilink")[加泰羅尼亞自治政府撥款支持](../Page/加泰羅尼亞.md "wikilink")[阿利坎特大學開發](../Page/阿利坎特.md "wikilink")，為一個以[GNU通用公共許可證條件發行的](../Page/GNU通用公共許可證.md "wikilink")[自由軟件](../Page/自由軟件.md "wikilink")。

Apertium原是[OpenTrad](http://www.opentrad.org/)計劃中其中一個機器翻譯引擎，原意為關係密切的語言之間互相翻譯。不過現在Apertium已經發展到可以為分野較大的語言之間翻譯。要創立一套新的語言翻譯系統，只需將有關語言的相關資料，即字典與文法規則，放入既定的[XML格式便可](../Page/XML.md "wikilink")。

[阿利坎特大學與](../Page/阿利坎特.md "wikilink")[維戈大學](../Page/維戈.md "wikilink")、[加泰羅尼亞理工大學](../Page/加泰羅尼亞.md "wikilink")、
Pompeu
Fabra大學合作，Apertium現已有下列語言的相關資料：[西班牙中的](../Page/西班牙.md "wikilink")[羅曼語族語言如](../Page/羅曼語族.md "wikilink")[西班牙語](../Page/西班牙語.md "wikilink")、[加泰羅尼亞語和](../Page/加泰羅尼亞語.md "wikilink")[加里西亞語](../Page/加里西亞語.md "wikilink")、[英語](../Page/英語.md "wikilink")、[葡萄牙語](../Page/葡萄牙語.md "wikilink")、[法語](../Page/法語.md "wikilink")、[奧克西唐語以及](../Page/奧克西唐語.md "wikilink")[羅馬尼亞語](../Page/羅馬尼亞語.md "wikilink")。

Apertium是一種轉化原則法的[機器翻譯系統](../Page/機器翻譯.md "wikilink")。字詞翻譯時會用[有限狀態置換器](../Page/有限狀態置換器.md "wikilink")，[詞性辨別時會用](../Page/詞性辨別.md "wikilink")[隐马尔可夫模型](../Page/隐马尔可夫模型.md "wikilink")。

## 歷史

Apertium起源於項目[OpenTrad中的其中一個機器翻譯引擎](../Page/OpenTrad.md "wikilink")，該項目由[西班牙政府資助](../Page/西班牙.md "wikilink")，由[阿拉貢大學的](../Page/阿拉貢大學.md "wikilink")[Transducens研究小組開發](../Page/Transducens.md "wikilink")。它最初設計用於在密切相關的語言之間翻譯，但最近已經擴展到處理更多的語言對。若要創建新的機器翻譯系統，你只需要以良好規定的[XML格式開發語言數據](../Page/XML.md "wikilink")（字典，規則）。

為此開發的語言數據目前支持（穩定版本）[阿拉伯語](../Page/阿拉伯語.md "wikilink")，[阿拉貢語](../Page/阿拉貢語.md "wikilink")，[阿斯圖里亞斯語](../Page/阿斯圖里亞斯語.md "wikilink")，[巴斯克語](../Page/巴斯克語.md "wikilink")，[布列塔尼語](../Page/布列塔尼語.md "wikilink")，[保加利亞語](../Page/保加利亞語.md "wikilink")，[加泰羅尼亞語](../Page/加泰羅尼亞語.md "wikilink")，[丹麥語](../Page/丹麥語.md "wikilink")，[英語](../Page/英語.md "wikilink")，[加泰羅尼亞語](../Page/加泰羅尼亞語.md "wikilink")，[世界語](../Page/世界語.md "wikilink")，[法語](../Page/法語.md "wikilink")，[加利西亞語](../Page/加利西亞語.md "wikilink")，[印地語](../Page/印地語.md "wikilink")，[冰島語](../Page/冰島語.md "wikilink")，[印尼語](../Page/印尼語.md "wikilink")，[意大利語](../Page/意大利語.md "wikilink")，[哈薩克語](../Page/哈薩克語.md "wikilink")，[馬其頓語](../Page/馬其頓語.md "wikilink")，[馬來西亞語](../Page/馬來西亞語.md "wikilink")，[馬耳他語](../Page/馬耳他語.md "wikilink")，[北薩米語](../Page/北薩米語.md "wikilink")，[挪威語](../Page/挪威語.md "wikilink")，[奧克西唐語](../Page/奧克西唐語.md "wikilink")，[葡萄牙語](../Page/葡萄牙語.md "wikilink")，[羅馬尼亞語](../Page/羅馬尼亞語.md "wikilink")，[塞爾維亞克羅地亞語](../Page/塞爾維亞克羅地亞語.md "wikilink")，[斯洛文尼亞語](../Page/斯洛文尼亞語.md "wikilink")，[烏爾都語和](../Page/烏爾都語.md "wikilink")[威爾士語](../Page/威爾士語.md "wikilink")。

項目參與了2009,\[1\] 2010,\[2\] 2011,\[3\] 2012,\[4\]
2013\[5\]和2014\[6\]年版的[Google Summer of
Code及](../Page/Google_Summer_of_Code.md "wikilink") 2010,\[7\]
2011,\[8\] 2012,\[9\] 2013,\[10\]
2014,\[11\]2015\[12\]及2016\[13\]年版的[Google
Code-In](../Page/Google_Code-In.md "wikilink")。

## 相關條目

  -
  - [機器翻譯](../Page/機器翻譯.md "wikilink")

  - Matxin

## 參考資料

  - Corbí-Bellot, M. et al. (2005) "An open-source shallow-transfer
    machine translation engine for the romance languages of Spain" in
    *Proceedings of the European Association for Machine Translation,
    10th Annual Conference, Budapest 2005*, pp. 79-86
  - Armentano-Oller, C. et al. (2006) "Open-source Portuguese-Spanish
    machine translation" *in Lecture Notes in Computer Science 3960
    \[Computational Processing of the Portuguese Language, Proceedings
    of the 7th International Workshop on Computational Processing of
    Written and Spoken Portuguese, PROPOR 2006\]*, p 50-59.

## 外部連結

  - [Sourceforge : Apertium](http://apertium.sourceforge.net/)
  - [Apertium web
    demos](https://web.archive.org/web/20080408050337/http://xixona.dlsi.ua.es/apertium/)
  - [Apertium
    Wiki](https://archive.is/20121209095606/http://xixona.dlsi.ua.es/wiki/)
  - [OpenTrad](http://www.opentrad.org/)

[Category:机器翻译](../Category/机器翻译.md "wikilink")
[Category:2009年面世的產品](../Category/2009年面世的產品.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.