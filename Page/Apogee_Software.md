[apogeelogo.png](https://zh.wikipedia.org/wiki/File:apogeelogo.png "fig:apogeelogo.png")
**Apogee
Software**是一家[电脑游戏和](../Page/电脑游戏.md "wikilink")[电视游戏开发商和出版商](../Page/电视游戏.md "wikilink")，成立于1987年。人们更熟悉它现在的名字[3D
Realms](../Page/3D_Realms.md "wikilink")。

## 知名游戏

**参与开发**

  - 1986年－[Beyond the Titanic](../Page/Beyond_the_Titanic.md "wikilink")
  - 1986年－[Diamond Digger](../Page/Diamond_Digger.md "wikilink")
  - 1986年－[Maze Runner](../Page/Maze_Runner.md "wikilink")
  - 1987年－[Kingdom of Kroz II](../Page/Kingdom_of_Kroz_II.md "wikilink")
  - 1987年－Supernova
  - 1990年－[Word Whiz](../Page/Word_Whiz.md "wikilink")
  - 1991年－[Arctic Adventure](../Page/Arctic_Adventure.md "wikilink")
  - 1991年－[Monuments of Mars](../Page/Monuments_of_Mars.md "wikilink")
  - 1991年－[Dark Ages](../Page/Dark_Ages_\(video_game\).md "wikilink")
  - 1991年－[Crystal Caves](../Page/Crystal_Caves.md "wikilink")
  - 1991年－[毁灭公爵](../Page/毁灭公爵.md "wikilink")
  - 1991年－[Paganitzu](../Page/Paganitzu.md "wikilink")
  - 1992年－Secret Agent
  - 1992年－[Word Rescue](../Page/Word_Rescue.md "wikilink")
  - 1993年－[Cosmo's Cosmic
    Adventure](../Page/Cosmo's_Cosmic_Adventure.md "wikilink")
  - 1993年－[Bio Menace](../Page/Bio_Menace.md "wikilink")
  - 1993年－[Major Stryker](../Page/Major_Stryker.md "wikilink")
  - 1993年－[毁灭公爵II](../Page/毁灭公爵II.md "wikilink")
  - 1993年－[Monster Bash](../Page/Monster_Bash.md "wikilink")
  - 1994年－[Hocus Pocus](../Page/Hocus_Pocus_\(game\).md "wikilink")
  - 1994年－[Rise of the Triad](../Page/Rise_of_the_Triad.md "wikilink")
  - 1995年－[Realms of
    Chaos](../Page/Realms_of_Chaos_\(1995_game\).md "wikilink")
  - 1996年－[Stargunner](../Page/Stargunner.md "wikilink")

**出版发行**

  - 1990年－[指挥官基恩](../Page/指挥官基恩.md "wikilink") - [id
    Software](../Page/id_Software.md "wikilink")
  - 1991年－[指挥官基恩](../Page/指挥官基恩.md "wikilink") 再见银河\! - [id
    Software](../Page/id_Software.md "wikilink")
  - 1992年－[德軍總部3D](../Page/德軍總部3D.md "wikilink") - [id
    Software](../Page/id_Software.md "wikilink")
  - 1993年－[Alien Carnage](../Page/Alien_Carnage.md "wikilink")（aka
    [Halloween Harry](../Page/Halloween_Harry.md "wikilink")）- [SubZero
    Software](../Page/SubZero_Software.md "wikilink")
  - 1993年－[Blake Stone: Aliens of
    Gold](../Page/Blake_Stone:_Aliens_of_Gold.md "wikilink") - JAM
  - 1994年－[Boppin](../Page/Boppin.md "wikilink") - Accursed Toys
  - 1994年－[Raptor: Call of the
    Shadows](../Page/Raptor:_Call_of_the_Shadows.md "wikilink") -
    [Mountain King Studios](../Page/Mountain_King_Studios.md "wikilink")
  - 1994年－[Wacky Wheels](../Page/Wacky_Wheels.md "wikilink") - Beavis
    Soft
  - 1996年－[Death Rally](../Page/Death_Rally.md "wikilink") - [Remedy
    Entertainment](../Page/Remedy_Entertainment.md "wikilink")

## 外部链接

  - [The Apogee FAQ](http://rinkworks.com/apogee/)
  - [Lon
    Matero的Apogee页](https://web.archive.org/web/20140803172116/http://www.apogeegames.com/)
  - [Apogee中心](http://www.apogeecentral.tk)

[Category:美國電腦公司](../Category/美國電腦公司.md "wikilink")
[A](../Category/美國電子遊戲公司.md "wikilink")
[Category:1987年開業電子遊戲公司](../Category/1987年開業電子遊戲公司.md "wikilink")