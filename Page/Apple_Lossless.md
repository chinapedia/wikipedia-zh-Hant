****（）為蘋果的無損音頻壓縮編碼格式，可將非壓縮音頻格式（[WAV](../Page/WAV.md "wikilink")、[AIFF](../Page/AIFF.md "wikilink")）壓縮至原先容量的40%至60%左右，編解碼速度很快。也因為是無損壓縮，聽起來與原檔案完全一樣，不會因解壓縮和壓縮而改變。

ALAC与MP3的主要分别在于编码过程中，MP3会取消小部分高频及低频部分的音频数据，而ALAC则会如实记录，不会删除音频中任何细节数据。由于资料无损，ALAC音频文件大小会比MP3大，通常每片音乐CD（约70至80分钟）经ALAC编码后，音频文件大小约300MB。

它在2004年4月28日公佈的[iTunes](../Page/iTunes.md "wikilink")4.5和[QuickTime](../Page/QuickTime.md "wikilink")6.5.1的其中一部份。

ALAC的编码器已于2011年10月26日以[Apache
License为协议公布源代码](../Page/Apache_License.md "wikilink")\[1\]。

## 相關條目

  - [iPod](../Page/iPod.md "wikilink")
  - [FLAC](../Page/FLAC.md "wikilink")
  - [Monkey's Audio](../Page/Monkey's_Audio.md "wikilink")
  - [MPEG-4 ALS](../Page/MPEG-4_ALS.md "wikilink")
  - [TTA](../Page/TTA.md "wikilink")
  - [WavPack](../Page/WavPack.md "wikilink")
  - [WMA Lossless](../Page/Windows_Media_Audio.md "wikilink")
  - [文件格式列表](../Page/文件格式列表.md "wikilink")

## 注释

## 外部連結

  - [iTunes - Apple](https://www.apple.com/itunes/)
  - [iTunes - Apple (中国)](https://www.apple.com/cn/itunes/)
  - [iTunes - Apple (台灣)](https://www.apple.com/tw/itunes/)
  - [ALAC Decoder
    (crazney.net)](https://web.archive.org/web/20120620124229/http://craz.net/programs/itunes/alac.html)
  - [Apple Lossless decoder
    (RareWares)](http://www.rarewares.org/lossless.html)

[Category:无损音频编解码器](../Category/无损音频编解码器.md "wikilink")
[Category:蘋果公司軟體](../Category/蘋果公司軟體.md "wikilink")
[Category:IPod](../Category/IPod.md "wikilink")
[Category:ITunes](../Category/ITunes.md "wikilink")
[Category:使用BSD许可证的软件](../Category/使用BSD许可证的软件.md "wikilink")

1.  <http://alac.macosforge.org/>