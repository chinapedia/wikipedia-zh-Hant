[Apple_interactive_television_box.jpg](https://zh.wikipedia.org/wiki/File:Apple_interactive_television_box.jpg "fig:Apple_interactive_television_box.jpg")
**蘋果機頂盒**（Apple Interactive Television
Box）是一台由蘋果電腦（即今[蘋果公司](../Page/蘋果公司.md "wikilink")）與包括[英國電信與](../Page/英國電信.md "wikilink")[比利時電信在內等多個](../Page/比利時電信.md "wikilink")[國家的](../Page/國家.md "wikilink")[電訊公司合作開發的](../Page/電訊公司.md "wikilink")[機頂盒](../Page/機頂盒.md "wikilink")，樣機於1994年至1995年期間在[美國與](../Page/美國.md "wikilink")[歐洲部份地區進行測試](../Page/歐洲.md "wikilink")，但該產品在隨後被取消，未曾進行過大量生產或上市。

該機頂盒設計作為[消費者與](../Page/消費者.md "wikilink")[互動電視服務之間的界面](../Page/互動電視.md "wikilink")，其[遙控器讓用戶選擇在連接的](../Page/遙控器.md "wikilink")[電視機上顯示的內容以及提供快轉與重播等功能](../Page/電視機.md "wikilink")，在這方面與現時的[衛星接收器或](../Page/衛星接收器.md "wikilink")[TiVo裝置相似](../Page/TiVo.md "wikilink")。但與TiVo不同，該機頂盒只會將用戶的選擇傳送到中央內容[伺服器](../Page/伺服器.md "wikilink")，而不會自行分發內容。此外，亦制訂了遊戲節目、[兒童用的教材與其他形式可利用裝置上互動能力內容的計劃](../Page/兒童.md "wikilink")。

現時該裝置已是蘋果收藏家之間的最愛，偶爾會有二手貨品出讓，範圍包括從非常早期的概念性樣機到有產品質素的機器。該些接近完成的裝置沒有早期機頂盒未完成的感覺：外殼接合良好，內部組件通常沒有樣機的指示器，一些裝置甚至有[FCC的批核貼紙](../Page/FCC.md "wikilink")（一般為一款產品推出市面前其中一個最後的附加物），此外連同一個[洩漏的使用說明書](https://web.archive.org/web/20070927194424/http://www.applefritter.com/prototypes/tvbox/0306983AppleTVBox.pdf)暗示該機頂盒計劃在取消前已非常接近完成。

由於該機器原意是作為數據訂閱服務的一部份，現存的裝置基本上無法運作，該機頂盒的[唯讀記憶體只包含了自外部](../Page/唯讀記憶體.md "wikilink")[硬盤或自](../Page/硬盤.md "wikilink")[以太網連接開機的必要訊息](../Page/以太網.md "wikilink")，更甚者許多樣機明顯甚至不會嘗試開機。這很可能是取決於唯讀記憶體的變更。

## 硬體詳情

[Apple_set_top_box_back.jpg](https://zh.wikipedia.org/wiki/File:Apple_set_top_box_back.jpg "fig:Apple_set_top_box_back.jpg")
蘋果機頂盒是以[Macintosh Quadra
605](../Page/Macintosh_Quadra_605.md "wikilink")/LC475為基礎。由於該機頂盒未曾推出市面，因此蘋果沒有發表官方的技術規格。以下是一個典型裝置的描述：

  - A/V插孔包括一個[合成視訊的](../Page/合成視訊.md "wikilink")[RCA插孔與兩個立體聲用的額外RCA插孔](../Page/RCA插孔.md "wikilink")，一個[S-端子插孔](../Page/S-端子.md "wikilink")，兩個射頻[同軸電纜插孔以及兩個](../Page/同軸電纜.md "wikilink")[SCART插孔](../Page/SCART.md "wikilink")
  - 類似麥金塔的端口，包括一個[麥金塔串行端口](../Page/麥金塔.md "wikilink")、一個[RJ-45](../Page/RJ-45.md "wikilink")[以太網端口與一個](../Page/以太網.md "wikilink")[SCSI端口](../Page/SCSI.md "wikilink")
  - 裝置前方擁有一個蘋果商標與一個[紅外線接收器](../Page/紅外線.md "wikilink")，明顯用來與[遙控器溝通](../Page/遙控器.md "wikilink")
  - 該裝置包含一個[68LC040](../Page/68LC040.md "wikilink")[處理器及內建](../Page/處理器.md "wikilink")4
    MB[記憶體](../Page/記憶體.md "wikilink")，但沒有[硬盤](../Page/硬盤.md "wikilink")
  - 一個未曾用在其他蘋果產品上的內置擴充插槽樣式，推測可能是以蘋果的[PDS擴充插槽為基礎](../Page/Processor_Direct_Slot.md "wikilink")

蘋果原意會隨該機頂盒提供一隻相配的黑色ADB滑鼠、鍵盤、蘋果300e光碟機、[StyleWriter印表機與多個款式的遙控器中的其中一款](../Page/StyleWriter.md "wikilink")。

## 外部連結

  - [蘋果機頂盒的用戶指南](https://web.archive.org/web/20070927194424/http://www.applefritter.com/prototypes/tvbox/0306983AppleTVBox.pdf)
  - [英國電信對該機頂盒的詳細描述](https://web.archive.org/web/20070927194131/http://www.applefritter.com/prototypes/tvbox/AppleSTB%231.pdf)
  - [蘋果機頂盒提交申請的專利](http://www.google.com/patents?vid=USPATD383456&id=XuInAAAAEBAJ&printsec=abstract&zoom=4)

## 請參閱

  - [Apple TV](../Page/Apple_TV.md "wikilink")
  - [Macintosh TV](../Page/Macintosh_TV.md "wikilink")
  - [Apple Pippin](../Page/Apple_Pippin.md "wikilink")
  - [IPTV](../Page/IPTV.md "wikilink") ([Internet
    Protocol](../Page/Internet_Protocol.md "wikilink") Television)

[Category:蘋果公司硬體](../Category/蘋果公司硬體.md "wikilink")
[Category:電視技術](../Category/電視技術.md "wikilink")