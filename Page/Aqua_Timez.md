**Aqua
timez**（暱稱「」）是日本[搖滾](../Page/搖滾.md "wikilink")[樂隊](../Page/樂隊.md "wikilink")，由五位成員組成：太志、大介、OKP-STAR、mayuko、TASSHI，為[EPIC
Records Japan
Inc旗下藝人](../Page/史诗唱片日本.md "wikilink")。他們是第一隊在地下時期和主流出道時期皆能獲得專輯週榜首位的樂隊。

## 成員介紹

**太志（）**

  -
    1980年5月10日出生（38歲），男性，出身於[岐阜縣](../Page/岐阜縣.md "wikilink")[岐阜市](../Page/岐阜市.md "wikilink")。
    擔任樂團主唱，負責大部分作詞與作曲。

**大介（）**

  -
    本名「長谷川大介」。
    1977年4月12日出生（41歲），男性，出身於[福島縣](../Page/福島縣.md "wikilink")[富岡町](../Page/富岡町.md "wikilink")。
    擔任樂團吉他手，負責吉他伴奏與Programming。

**OKP-STAR（）**

  -
    本名「岡田知久」。
    1977年3月25日出生（41歲），男性，出身於[福島縣](../Page/福島縣.md "wikilink")[磐城市](../Page/磐城市.md "wikilink")。
    擔任樂團貝斯手，負責低音與和音伴奏。

**mayuko（）**

  -
    1977年9月18日出生（41歲），女性，出身於[富山縣](../Page/富山縣.md "wikilink")[富山市](../Page/富山市.md "wikilink")。
    擔任樂團鍵盤手。

**TASSHI（）**

  -
    本名「田島智之」。
    1978年8月21日出生（40歲），男性，出身於[高知縣](../Page/高知縣.md "wikilink")[須崎市](../Page/須崎市.md "wikilink")。
    擔任樂團鼓手。
    在退出樂團後，OKP-STAR去電邀請TASSHI加入樂團。

### 退出成員

****

  -
    2月21日出生，男性，出身於[三重県](../Page/三重県.md "wikilink")[熊野市](../Page/熊野市.md "wikilink")。
    擔任樂團鼓手。
    於發行迷你專輯『（七彩的塗鴉）』後退出樂團。

## 樂團歷程

  - 2003年

<!-- end list -->

  - Aqua
    Times組成，由**主唱太志**、**吉他手大介**、**貝斯手OKP-STAR**、**鍵盤手Mayuko**及**鼓手**五人所組成的樂團，正式出道前主要以[東京為中心](../Page/東京.md "wikilink")，活躍於地下樂團活動。

<!-- end list -->

  - 2004年

<!-- end list -->

  - 起連續兩年獲得日本新星堂樂團大賽最優秀大獎，並開始在[吉祥寺](../Page/吉祥寺.md "wikilink")、[涉谷](../Page/涉谷.md "wikilink")、[大宮等地開始街頭演唱](../Page/大宮.md "wikilink")，累積了許多演唱經驗及固定的歌迷。

<!-- end list -->

  - 2005年

<!-- end list -->

  - 8月24日　發行首張地下樂團創作專輯『**奏響天際的祈禱**』，寫下日本[Oricon公信榜史上地下樂團中](../Page/Oricon.md "wikilink")，第五組首張專輯獲得公信流行榜專輯冠軍的團體，創下日本音樂史上地下專輯發行後花了最長時間（14週）才獲得冠軍的紀錄。

<!-- end list -->

  - 2006年

<!-- end list -->

  - 4月5日　推出迷你專輯『**七彩的塗鴉**』，正式主流出道。Aqua
    Timez以人的喜怒哀樂為主要創作題材，以朗朗上口的旋律、令人感同身受的歌詞，加上主唱太志給予人勇氣與溫暖的唱腔，及完美的演奏，根據調查，最受到10幾到20幾歲的年輕人的認同，正式出道後立即受到[日本年輕歌迷的喜愛](../Page/日本.md "wikilink")。另**鼓手**於本張專輯發行時離開樂團，**TASSHI**加入樂團、擔任鼓手位置。
  - 12月6日　在日本推出首張創作專輯『**追風之歌**』，專輯收錄之前地下樂團時期多首膾炙人口的創作歌曲，如電影【[勇者物語](../Page/勇者物語.md "wikilink")】的主題曲「**迎向朝陽**」、[SONY](../Page/SONY.md "wikilink")
    walkman廣告曲「**夜夜夜夜**」等多首歌曲，同年首度登上第57回[NHK](../Page/NHK.md "wikilink")[紅白歌合戰的舞台](../Page/紅白歌合戰.md "wikilink")，在2007年年初的ORICON公信榜前100名中，寫下共有2張單曲及2張專輯共計4張作品同時在榜內的紀錄。

<!-- end list -->

  - 2007年

<!-- end list -->

  - 推出第二張創作專輯『**街頭塗鴉**』空降日本ORICON公信榜專輯榜第二名，奠定Aqua
    Timez在日本樂壇的地位。收錄包括動畫【[BLEACH](../Page/BLEACH.md "wikilink")】片頭曲「**ALONES**」、日劇【女警狀況外】主題曲「**手掌心**」、碳酸飲料廣告曲「**Shiori**」。

<!-- end list -->

  - 2008年

<!-- end list -->

  - 推出單曲「**虹**」停留在公信榜第二名兩週，連續六週前十名，數位手機下載超過200萬次。
  - 12月31日　二度登上[紅白歌合戰的舞台](../Page/紅白歌合戰.md "wikilink")。

<!-- end list -->

  - 2009年

<!-- end list -->

  - 3月11日　推出第三張創作專輯『**無名花**』，收錄包括生產限定單曲「**STAY
    GOLD**」、動畫【[BLEACH](../Page/BLEACH.md "wikilink")】片頭曲「**薇蘿妮卡**」、電影【應援團少女】主題曲「**夏天的尾巴**」等。
  - 3月20日　舉行全日本小型巡迴演唱會「**still connected tour '09**」。
  - 10月14日　發行首張精選輯『**The BEST of Aqua Timez**』，並獲得公信榜第一名。

<!-- end list -->

  - 2010年

<!-- end list -->

  - 3月10日　開始舉行「**Music 4 Music tour 2010**」巡迴演唱會，共24場。
  - 8月25日　首次舉辦了6場Fan Club限定的演唱會「**The FANtastic Tour 2010**」。

<!-- end list -->

  - 2011年

<!-- end list -->

  - 2月16日　推出第四張創作專輯『**活在當下**』，收錄【[極道鮮師
    (電影)](../Page/極道鮮師_\(電影\).md "wikilink")】主題曲「**緬梔花～花之歌～**」、動畫【[火影忍者疾風傳](../Page/火影忍者疾風傳.md "wikilink")】片尾曲「**深夜的管弦樂團**」、動畫【[STAR
    DRIVER
    閃亮的塔科特](../Page/STAR_DRIVER_閃亮的塔科特.md "wikilink")】片頭曲「**超越地心引力**」等。
  - 6月4日　開始舉行巡迴演唱會「**Carpe diem tour 2011**」，共25場。8月24日巡迴的最終場，為Aqua
    Timez首次登上[日本武道館演出](../Page/日本武道館.md "wikilink")。

<!-- end list -->

  - 2012年

<!-- end list -->

  - 9月5日　第五張創作專輯『**because you are
    you**』發行，收錄動畫【[BLEACH](../Page/BLEACH.md "wikilink")】片尾曲「**MASK**」、日劇【[幽靈媽媽搜查線](../Page/幽靈媽媽搜查線.md "wikilink")】主題曲「**花苞**」等歌曲。
  - 10月25日　開始巡迴演唱會「**because we are we tour 2012-2013**」。

<!-- end list -->

  - 2018年

<!-- end list -->

  - 5月8日 宣布「**Present is a Present tour 2018**」巡演結束後樂團解散。
  - 11月18日 於[橫濱體育館舉辦最後一場演唱會](../Page/橫濱體育館.md "wikilink")『Aqua
    Timez　FINAL LIVE「last dance」』，於會後解散。

## 音樂作品

### 單曲

<table>
<thead>
<tr class="header">
<th><p>發行序</p></th>
<th><p>發售日</p></th>
<th><p>單曲名稱</p></th>
<th><p>收錄歌曲</p></th>
<th><p>規格</p></th>
<th><p>產品編號</p></th>
<th><p><a href="../Page/Oricon.md" title="wikilink">Oricon排行榜</a></p></th>
<th><p>附註</p></th>
<th><p>收錄專輯</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>自主製作</p></td>
<td><p>2004年9月19日</p></td>
<td><p><strong></strong><br />
（永遠在一起）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = </p></td>
<td><p>title2 = </p></td>
<td><p>title3 = （讓你感動的小小情歌）</p></td>
</tr>
<tr class="even">
<td><p>1st</p></td>
<td><p>2006年7月5日</p></td>
<td><p><strong></strong><br />
（迎向朝陽）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = </p></td>
<td><p>title2 = （奔跑）</p></td>
<td><p>title3 = </p></td>
</tr>
<tr class="odd">
<td><p>2nd</p></td>
<td><p>2006年11月22日</p></td>
<td><p><strong></strong><br />
（夜夜夜夜）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = </p></td>
<td><p>title2 = (Insrumental Mix) }}</p></td>
<td><p>CD+DVD<br />
CD+DVD<br />
CD</p></td>
</tr>
<tr class="even">
<td><p>3rd</p></td>
<td><p>2007年5月9日</p></td>
<td><p><strong></strong><br />
（Shiori）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = </p></td>
<td><p>title2 =  }}</p></td>
<td><p>CD</p></td>
</tr>
<tr class="odd">
<td><p>4th</p></td>
<td><p>2007年8月1日</p></td>
<td><p><strong></strong></p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = ALONES</p></td>
<td><p>title2 = </p></td>
<td><p>title3 = (DJ Mass'Skate Sonic* Remix)</p></td>
</tr>
<tr class="even">
<td><p>5th</p></td>
<td><p>2007年10月31日</p></td>
<td><p><strong></strong><br />
（手掌心）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = </p></td>
<td><p>title2 = (Instrumental Mix) }}</p></td>
<td><p>ESCL-3003</p></td>
</tr>
<tr class="odd">
<td><p>6th</p></td>
<td><p>2008年5月8日<br />
（台灣：2008年5月9日）</p></td>
<td><p><strong></strong></p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = </p></td>
<td><p>title2 = （溫柔的回憶~evalastingⅡ~）</p></td>
<td><p>title3 = （其實啊）</p></td>
</tr>
<tr class="even">
<td><p>7th</p></td>
<td><p>2008年10月1日<br />
（台灣：2008年10月3日）</p></td>
<td><p><strong></strong><br />
（夏天的尾巴）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = </p></td>
<td><p>title2 = （秋日時分）</p></td>
<td><p>title3 = on the run</p></td>
</tr>
<tr class="odd">
<td><p>8th</p></td>
<td><p>2009年1月14日<br />
（台灣：2009年1月16日）</p></td>
<td><p><strong>Velonica</strong><br />
（薇蘿妮卡）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = Velonica</p></td>
<td><p>title2 = （協奏）</p></td>
<td><p>title3 = </p></td>
</tr>
<tr class="even">
<td><p>9th</p></td>
<td><p>2009年3月4日</p></td>
<td><p><strong>STAY GOLD</strong></p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = STAY GOLD</p></td>
<td><p>title2 = STAY GOLD -Instrumental- }}</p></td>
<td><p>CD+DVD</p></td>
</tr>
<tr class="odd">
<td><p>10th</p></td>
<td><p>2009年7月29日<br />
（台灣：2009年7月31日）</p></td>
<td><p><strong></strong><br />
（緬梔花～花之歌～）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = </p></td>
<td><p>title2 = （漫漫長夜）</p></td>
<td><p>title3 = Perfect World -blue forest ver.-</p></td>
</tr>
<tr class="even">
<td><p>11th</p></td>
<td><p>2010年1月27日<br />
（台灣：2010年2月5日）</p></td>
<td><p><strong></strong><br />
（春意的信籤）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = </p></td>
<td><p>title2 = （流星之歌）</p></td>
<td><p>title3 = （鄰近天空的街道）</p></td>
</tr>
<tr class="odd">
<td><p>12th</p></td>
<td><p>2010年10月13日<br />
（台灣：2010年10月15日）</p></td>
<td><p><strong></strong><br />
（超越地心引力）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = GRAVITY Ø</p></td>
<td><p>title2 = （月之窗簾）</p></td>
<td><p>title3 = （口袋中的宇宙）</p></td>
</tr>
<tr class="even">
<td><p>13th</p></td>
<td><p>2011年1月26日<br />
（台灣：2011年1月28日）</p></td>
<td><p><strong></strong><br />
（深夜的管弦樂團）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = </p></td>
<td><p>title2 = （被風吹拂）</p></td>
<td><p>title3 = Full a Gain</p></td>
</tr>
<tr class="odd">
<td><p>14th</p></td>
<td><p>2012年2月22日<br />
（台灣：2012年2月24日）</p></td>
<td><p><strong>MASK</strong></p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = MASK</p></td>
<td><p>title2 = （通往天空之道）</p></td>
<td><p>title3 = 1980</p></td>
</tr>
<tr class="even">
<td><p>15th</p></td>
<td><p>2012年8月22日</p></td>
<td><p><strong></strong><br />
（花苞）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = </p></td>
<td><p>title2 = </p></td>
<td><p>title3 =  -kawasaki electro academy mix- (remixed by Takeshi Ueda (AA=) +Kei Kusama)（塵埃瞬間）</p></td>
</tr>
<tr class="odd">
<td><p>16th</p></td>
<td><p>2013年11月27日<br />
（台灣：2013年11月29日）</p></td>
<td><p><strong></strong><br />
（伊旬園）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = </p></td>
<td><p>title2 = （cranberry jam）</p></td>
<td><p>title3 = The FANtastic Journey</p></td>
</tr>
<tr class="even">
<td><p>17th</p></td>
<td><p>2014年12月3日</p></td>
<td><p><strong></strong><br />
（活下去）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = </p></td>
<td><p>title2 = MASK(Shoes and Stargazing version)</p></td>
<td><p>title3 = (Instrumental) }}</p></td>
</tr>
<tr class="odd">
<td><p>18th</p></td>
<td><p>2015年8月5日</p></td>
<td><p><strong></strong><br />
（最後一刻II）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = </p></td>
<td><p>title2 = </p></td>
<td><p>title3 = (Instrumental)</p></td>
</tr>
<tr class="even">
<td><p>19th</p></td>
<td><p>2016年9月28日</p></td>
<td><p><strong></strong><br />
（12月向日葵）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = no</p></td>
<td><p>headline =</p></td>
<td><p>title1 = </p></td>
<td><p>title2 = </p></td>
<td><p>title3 = (Instrumental)</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th><p>發行序</p></th>
<th><p>發售日</p></th>
<th><p>單曲名稱</p></th>
<th><p>收錄歌曲</p></th>
<th><p>收錄專輯</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>2006年11月1日</p></td>
<td><p><strong></strong></p></td>
<td><p><br />
<br />
（讓你感動的小小情歌）<br />
 -Instrumental-</p></td>
<td><p>（追風之歌）</p></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p>2008年2月9日</p></td>
<td><p><strong></strong></p></td>
<td><p><br />
(Instrumental)</p></td>
<td><p>（無名花）</p></td>
</tr>
<tr class="odd">
<td><p>3rd</p></td>
<td><p>2014年4月2日</p></td>
<td><p><strong></strong>（回信）</p></td>
<td></td>
<td><p>（精靈的眼淚）</p></td>
</tr>
<tr class="even">
<td><p>4th</p></td>
<td><p>2015年4月8日</p></td>
<td><p><strong></strong>（櫻花道）</p></td>
<td></td>
<td><p>10th Anniversary Best RED（十周年緋紅精選）</p></td>
</tr>
<tr class="odd">
<td><p>5th</p></td>
<td><p>2015年5月6日</p></td>
<td><p><strong></strong>（睡顏）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>6th</p></td>
<td><p>2015年6月3日</p></td>
<td><p><strong></strong>（Singalong）</p></td>
<td></td>
<td><p>10th Anniversary Best BLUE（十周年湛藍精選）</p></td>
</tr>
<tr class="odd">
<td><p>7th</p></td>
<td><p>2015年11月9日</p></td>
<td><p><strong></strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>8th</p></td>
<td><p>2016年6月1日</p></td>
<td><p><strong>We must</strong></p></td>
<td><p>We must</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 專輯

<table>
<thead>
<tr class="header">
<th><p>發行序</p></th>
<th><p>發售日</p></th>
<th><p>專輯名稱</p></th>
<th><p>收錄歌曲</p></th>
<th><p>規格</p></th>
<th><p>產品編號</p></th>
<th><p><a href="../Page/Oricon.md" title="wikilink">Oricon排行榜</a></p></th>
<th><p>附註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>2006年12月6日<br />
（台灣：2007年8月3日）</p></td>
<td><p><strong></strong><br />
（追風之歌）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = yes</p></td>
<td><p>headline = DISC</p></td>
<td><p>title1 = 1mm</p></td>
<td><p>title2 = （看不見星星的夜晚）</p></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p>2007年11月21日<br />
（台灣：2007年11月23日）</p></td>
<td><p><strong></strong><br />
（街頭塗鴉）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = yes</p></td>
<td><p>headline = DISC</p></td>
<td><p>title1 = （塵埃瞬間）</p></td>
<td><p>title2 = （世界上最小的海）</p></td>
</tr>
<tr class="odd">
<td><p>3rd</p></td>
<td><p>2009年3月11日<br />
（台灣：2009年3月13日）</p></td>
<td><p><strong></strong><br />
（無名花）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = yes</p></td>
<td><p>headline = DISC</p></td>
<td><p>title1 = BIRTH</p></td>
<td><p>title2 = Velonica（薇蘿妮卡）</p></td>
</tr>
<tr class="even">
<td><p>4th</p></td>
<td><p>2011年2月16日<br />
（台灣：2011年2月18日）</p></td>
<td><p><strong></strong><br />
（活在當下）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = yes</p></td>
<td><p>headline = DISC</p></td>
<td><p>title1 = （百年之樹）</p></td>
<td><p>title2 = （最後一刻）</p></td>
</tr>
<tr class="odd">
<td><p>5th</p></td>
<td><p>2012年9月5日<br />
（台灣：2012年9月14日）</p></td>
<td><p><strong>because you are you</strong></p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = yes</p></td>
<td><p>headline = DISC</p></td>
<td><p>title1 = （音速的風景）</p></td>
<td><p>title2 = （歐若拉降臨的夜晚）</p></td>
</tr>
<tr class="even">
<td><p>6th</p></td>
<td><p>2014年8月27日<br />
（台灣：2014年8月29日）</p></td>
<td><p><strong></strong><br />
（精靈的眼淚）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = yes</p></td>
<td><p>headline = DISC</p></td>
<td><p>title1 = （亞當的覺悟）</p></td>
<td><p>title2 = （夏娃的結論）</p></td>
</tr>
<tr class="odd">
<td><p>7th</p></td>
<td><p>2016年12月14日</p></td>
<td><p><strong></strong></p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = yes</p></td>
<td><p>headline = DISC</p></td>
<td><p>title1 = </p></td>
<td><p>title2 = （最後一刻II）</p></td>
</tr>
<tr class="even">
<td><p>8th</p></td>
<td><p>2018年4月25日</p></td>
<td><p><strong></strong></p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = yes</p></td>
<td><p>headline = DISC</p></td>
<td><p>title1 = </p></td>
<td><p>title2 = over and over</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th><p>發行序</p></th>
<th><p>發售日</p></th>
<th><p>專輯名稱</p></th>
<th><p>收錄歌曲</p></th>
<th><p>規格</p></th>
<th><p>產品編號</p></th>
<th><p><a href="../Page/Oricon.md" title="wikilink">Oricon排行榜</a></p></th>
<th><p>附註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>自主製作</p></td>
<td><p>2004年4月24日</p></td>
<td><p><strong></strong><br />
（在悲傷盡頭亮起光）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = yes</p></td>
<td><p>headline = DISC</p></td>
<td><p>title1 = （白色森林）</p></td>
<td><p>title2 = </p></td>
</tr>
<tr class="even">
<td><p>1st</p></td>
<td><p>2005年8月24日<br />
（獨立製作）</p></td>
<td><p><strong></strong><br />
（奏響天際的祈禱）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = yes</p></td>
<td><p>headline = DISC</p></td>
<td><p>title1 = （從希望的山丘上）</p></td>
<td><p>title2 = </p></td>
</tr>
<tr class="odd">
<td><p>2nd</p></td>
<td><p>2006年4月5日</p></td>
<td><p><strong></strong><br />
（七彩的塗鴉）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = yes</p></td>
<td><p>headline = DISC</p></td>
<td><p>title1 = （幻影的日子）</p></td>
<td><p>title2 = </p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th><p>發行序</p></th>
<th><p>發售日</p></th>
<th><p>專輯名稱</p></th>
<th><p>收錄歌曲</p></th>
<th><p>規格</p></th>
<th><p>產品編號</p></th>
<th><p><a href="../Page/Oricon.md" title="wikilink">Oricon排行榜</a></p></th>
<th><p>附註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>2009年10月14日<br />
（台灣：2009年10月30日）</p></td>
<td><p><strong>The BEST of Aqua Timez</strong><br />
（街頭收藏全精選）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = yes</p></td>
<td><p>headline = DISC 1</p></td>
<td><p>title1 = （夜夜夜夜）</p></td>
<td><p>title2 = </p></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p>2015年8月25日<br />
（台灣：2015年8月28日）</p></td>
<td><p><strong>10th Anniversary Best RED</strong><br />
（十周年緋紅精選）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = yes</p></td>
<td><p>headline = DISC 1</p></td>
<td><p>title1 = （讓你感動的小小情歌）</p></td>
<td><p>title2 = （迎向朝陽）</p></td>
</tr>
<tr class="odd">
<td><p><strong>10th Anniversary Best BLUE</strong><br />
（十周年湛藍精選）</p></td>
<td><p>{{Track listing</p></td>
<td><p>collapsed = yes</p></td>
<td><p>headline = DISC 1</p></td>
<td><p>title1 = （夜夜夜夜）</p></td>
<td><p>title2 = ALONES</p></td>
<td><p>title3 = Velonica（薇蘿妮卡）</p></td>
<td><p>title4 = STAY GOLD</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 演唱會

## 備註

<div class="references-small">

</div>

## 外部連結

  - [Aqua Timez 官網](http://www.aquatimez.com/)

  - [Aqua Timez 官方Twitter](https://twitter.com/AquaTimezStaff)

  - [Aqua Timez 臉書專頁](https://www.facebook.com/AquaTimezofficial)

  - [Aqua Timez 的
    Blog「流るる風の跡を」](https://web.archive.org/web/20080117102531/http://blog.excite.co.jp/aquatimez/)

[Category:日本另类摇滚乐团](../Category/日本另类摇滚乐团.md "wikilink")
[Category:2003年成立的音樂團體](../Category/2003年成立的音樂團體.md "wikilink")
[Category:SCHOOL OF LOCK\!](../Category/SCHOOL_OF_LOCK!.md "wikilink")
[Category:2018年解散的音樂團體](../Category/2018年解散的音樂團體.md "wikilink")
[Category:日本索尼音樂娛樂旗下藝人](../Category/日本索尼音樂娛樂旗下藝人.md "wikilink")
[Category:流行搖滾樂團](../Category/流行搖滾樂團.md "wikilink")