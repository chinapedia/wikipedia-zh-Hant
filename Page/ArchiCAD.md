**ArchiCAD**是一个理想的3D建筑设计软件，它同时具备了成熟的2D绘图与布图功能，由匈牙利公司Graphisoft开发。自ArchiCAD问世就以3D建模和设计为特色，并提出虚拟建筑的概念。现在ArchiCAD也同样宣传自己是BIM软件，业内也认为ArchiCAD是世界上最早的BIM软件，其扩展模块中也有MEP（水暖电）ECO（能耗分析）及Artlantis渲染插件等。ArchiCAD支持IFC标准和GDL技术。Graphisoft公司推出ArchiCAD系列支持10余种语言。

## 本土化過程

  - 在台灣已由官方網站龍庭資訊公開發表『ArchiCAD 建築專家系統 19』...等本土化參數智慧型圖庫，提供ArchiCAD 19
    中文版使用者使用，免去建築師自行撰寫圖庫之苦，將精力放在設計創作上。

## 简体中文本地化

一直以来在网络上都有人通过特殊途径散播简体中文版本直到官方简体中文版本发布。 重要版本：金钥匙版本（已超过授权期限），内含交互式设计教学案例。
教材：重庆大学出版社出版，《轻松学用ArchiCAD9》

## 相關網站

  - 在台灣有關討論ArchiCAD之論壇目前已有兩個網站，一個是**CADER設計論壇**，另一個則是**台灣省建築師公會台北市連絡處論壇**。

<!-- end list -->

1.  相關網站:
    [注册后可免费下载使用该软件](https://web.archive.org/web/20130522022531/http://archicadregistration.cn/)
2.  相關網站:
    [CADER設計論壇](https://web.archive.org/web/20080516120422/http://cader-tw.info/cgi-bin/ut/board_show.cgi?id=30)
3.  相關網站: [ArchiCAD 建築專家系統](http://sites.google.com/site/taigdlstudio)

## 中国大陆地区ArchiCAD相关网站

  - ArchiCAD官方：[ArchiCADChina](http://www.graphisoft.cn)
  - 中国BIM门户：[ArchiCAD专栏](https://web.archive.org/web/20130119190442/http://www.chinabim.com/archicad/)
  - 新探索发现论坛：[BIMStudio](http://www.bimst.com)

[Category:三维图像软件](../Category/三维图像软件.md "wikilink")
[Category:建築資訊模型](../Category/建築資訊模型.md "wikilink")
[Category:電腦輔助設計軟體](../Category/電腦輔助設計軟體.md "wikilink")
[Category:三維電腦圖形軟體](../Category/三維電腦圖形軟體.md "wikilink")
[Category:三維模型](../Category/三維模型.md "wikilink")
[Category:匈牙利发明](../Category/匈牙利发明.md "wikilink")