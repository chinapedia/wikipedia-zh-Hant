**Arial**是一套隨同多套[微軟應用軟件所分發的](../Page/微軟.md "wikilink")[無襯線體](../Page/無襯線體.md "wikilink")[TrueType](../Page/TrueType.md "wikilink")[字型](../Page/字型.md "wikilink")。[Monotype設計這套字型是要以其作為一個較便宜的替代品與](../Page/Monotype.md "wikilink")[Linotype的](../Page/Linotype.md "wikilink")[Helvetica競爭](../Page/Helvetica.md "wikilink")。由於Helvetica是支援[Adobe](../Page/Adobe.md "wikilink")
[PostScript語言的](../Page/PostScript.md "wikilink")[印表機所指定的其中一款字型](../Page/印表機.md "wikilink")，用戶選用它能確保文件可正確打印，所以是一套流行的字型。微軟購買了較便宜的Arial的使用許可，所以只包含了類似Helvetica字型如Arial及MS
Sans Serif在[視窗系統裏面](../Page/Microsoft_Windows.md "wikilink")。

雖然比例及[字重](../Page/字重.md "wikilink")（weight）和Helvetica極之相近，但Arial其實是Monotype
[Grotesque系列的變種](../Page/Grotesque.md "wikilink")。設計Arial時考慮到會在電腦上面使用，在字體及[字距上都作了一些細微的調整和變動](../Page/字距.md "wikilink")，以增加它在電腦屏幕上不同[解析度下的](../Page/解析度.md "wikilink")[可讀性](../Page/可讀性.md "wikilink")。

從微軟在[Windows
3.1上推出購自](../Page/Windows_3.1.md "wikilink")[蘋果電腦的](../Page/蘋果電腦.md "wikilink")[TrueType技術起Arial就一直跟隨視窗系統分發](../Page/TrueType.md "wikilink")。TrueType的推出就是打算取代[PostScript語言所使用的](../Page/PostScript.md "wikilink")[Type
1字型](../Page/Type_1.md "wikilink")。而微軟也曾嘗試以自行開發的[TrueImage印表機描述語言徹底取代PostScript](../Page/TrueImage.md "wikilink")。

最新版本的視窗系統都帶有Arial的[Unicode版本](../Page/Unicode.md "wikilink")：[Arial
Unicode
MS](../Page/Arial_Unicode_MS.md "wikilink")。這字型依據[Unicode標準包含多國語言文字在內](../Page/Unicode.md "wikilink")。雖說比如[Bitstream
Cyberbit和](../Page/Bitstream_Cyberbit.md "wikilink")[Code2000都比它收錄更多字元更完整](../Page/Code2000.md "wikilink")，不過以跟隨作業系統大規模分發的字型來說，Arial
Unicode MS就是最完整的一部了。

最新的PostScript語言除了原有的Helvetica也包括了Arial作為標準字型。使用PostScript第三級（level
3）語言的機器都應該支援。

## 讀法

Arial的英文發音有很多不同說法。雖然有人習慣讀作"air-ree-al"\[\]，但其實應該讀作"are-ree-al"\[\]。

## Arial變體

以下是著名的Arial變體：

  - **Arial**：有時稱為**Arial Regular**以便與Arial Narrow區別，其包括Arial、Arial
    Italic（[斜體](../Page/斜體.md "wikilink")）、Arial
    Bold（[粗體](../Page/粗體.md "wikilink")）、Arial Bold
    Italic（粗斜體）和Arial Unicode MS
  - **Arial Black**：此字體的特色在於其筆畫相當的粗，包含Arial Black、Arial Black Italic（斜體）
  - **Arial Narrow**：為Arial的細瘦版本，包含Arial Narrow Regular、Arial Narrow
    Bold（粗體）、Arial Narrow Italic（斜體）和Arial Narrow Bold Italic（粗斜體）
  - **Arial Rounded**：包含Arial Rounded
    Bold（粗體），此字型可在微軟[韓文字型](../Page/韓文.md "wikilink")[Gulim找到](../Page/Gulim.md "wikilink")

## 参考文献

## 外部連結

  - [Fonts.com:
    Arial](http://www.fonts.com/findfonts/detail.htm?pid=243200)
  - [The Scourge of Arial](http://www.ms-studio.com/articles.html) -
    Arial之灾
  - [怎樣分辨Arial、Helvetica和Grotesque](http://www.ms-studio.com/articlesarialsid.html)
  - [PostScript 3字型集](http://www.adobe.com/products/postscript/pdfs/ps3fonts.pdf)
  - [Arial字型資訊](http://www.microsoft.com/typography/fonts/font.aspx?FID=8&FNAME=Arial)
  - [Arial
    Black字型資訊](http://www.microsoft.com/typography/fonts/font.aspx?FID=13&FNAME=Arial%20Black)
  - [下載Arial](http://prdownloads.sourceforge.net/corefonts/arial32.exe?download)
  - [下載Arial
    Black](http://prdownloads.sourceforge.net/corefonts/arialb32.exe?download)

## 参见

  - [TrueType](../Page/TrueType.md "wikilink")
  - [網頁核心字型](../Page/網頁核心字型.md "wikilink")

{{-}}

[Category:无衬线字体](../Category/无衬线字体.md "wikilink")
[Category:蒙纳字体](../Category/蒙纳字体.md "wikilink")
[Category:網頁核心字型](../Category/網頁核心字型.md "wikilink")
[Category:Windows XP字體](../Category/Windows_XP字體.md "wikilink")