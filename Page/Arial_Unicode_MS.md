****是一种[无衬线的](../Page/无衬线.md "wikilink")[TrueType类型](../Page/TrueType.md "wikilink")[电脑字体](../Page/电脑字体.md "wikilink")。

## 概要

Arial Unicode
MS是[电脑字体](../Page/电脑字体.md "wikilink")[Arial的拓展版本之一](../Page/Arial.md "wikilink")。比起Arial，它的线高更大，省略了[字符间距](../Page/字符间距.md "wikilink")，添加了足够的字形来覆盖[Unicode](../Page/Unicode.md "wikilink")
2.1版本，因此它支持大多数[微软](../Page/微软.md "wikilink")[代码页](../Page/代码页.md "wikilink")，但也导致其庞大的容量（22
[MB](../Page/百万字节.md "wikilink")）。\[1\]它包括[表意符号排版表](../Page/表意符号.md "wikilink")，但和Arial不同的是，它在14–18[点时不需要抗锯齿平滑处理](../Page/点.md "wikilink")，而且它只有正体字符、没有斜体版本。Arial
Unicode MS通常随[Microsoft
Office发布](../Page/Microsoft_Office.md "wikilink")，但它也随[Mac OS X
v10.5捆绑发布](../Page/Mac_OS_X_v10.5.md "wikilink")。且微软许可Ascender公司以“Arial
Unicode”的名称进行单独出售。

由于使用同样的引擎进行描摹而没有根据不同尺寸进行调整，Arial和Arial Unicode MS中的字符看起来有些许宽和圆。Arial
Unicode MS横排文本看起来具有更宽的线间距，因为它拥有较大的字框（为显示一些拓展字符Arial Unicode
MS需要更大的空间）和描摹限制，而字符形状保持不变。Arial Unicode
MS本身缺乏间距对定义的性质也影响了一些描摹过程中的字符间距离。

## 历史和传播

Arial是由罗宾·尼古拉斯（Robin
Nicholas）和帕特里希亚·萨云德在1982年设计，并在1990年作为TrueType字体发布的。1993年到1999年间，由Monotype公司的技术人员拓展为Arial
Unicode MS并在1998年作为TrueType字体单独发布并卖给微软。

Monotype Imaging目前仍拥有Arial和Arial Unicode
MS商标，但微软持有字体的专有许可权。目前微软将字体许可转给Ascender
Corporation销售。而且事实表明它还授权给了[苹果公司](../Page/苹果公司.md "wikilink")，因为苹果在2007年10月16日宣布其新版[操作系统](../Page/操作系统.md "wikilink")[Mac
OS X v10.5](../Page/Mac_OS_X_v10.5.md "wikilink")（Leopard）将捆绑Arial
Unicode字体。有意思的是，Leopard也预定搭载其他[其他几个](https://web.archive.org/web/20081215210759/http://www.apple.com/macosx/features/300.html#fonts#fonts)原先微软独占的字体，包括[Microsoft
Sans
Serif](../Page/Microsoft_Sans_Serif.md "wikilink")，[Tahoma和](../Page/Tahoma.md "wikilink")[Wingdings](../Page/Wingdings.md "wikilink")。

[Microsoft Publisher](../Page/Microsoft_Publisher.md "wikilink") 2000
SR-1的许可用户在2001年中到2002年可以单独下载Arial Unicode MS。微软在Microsoft Publisher
2002自动捆绑之后就不能免费下载了。

### 版本

0.84版本通过Microsoft Office 2000及其除Publisher 2000
SR-1以外的单品软件发布，包含51180个字形（38911个字符），支持32代码页，并包括拉丁字符和[汉字](../Page/汉字.md "wikilink")[OpenType排版表信息](../Page/OpenType.md "wikilink")。支持的代码页有[1250](../Page/Code_page_1250.md "wikilink")（拉丁2:[东欧](../Page/东欧.md "wikilink")）,
[1251](../Page/Code_page_1251.md "wikilink")（[西里尔字母](../Page/西里尔字母.md "wikilink")）,
[1252](../Page/Code_page_1252.md "wikilink")（拉丁1）,
[1253](../Page/Code_page_1253.md "wikilink")（希腊字母）,
[1254](../Page/Code_page_1254.md "wikilink")（[土耳其语](../Page/土耳其语.md "wikilink")）,
[1255](../Page/Code_page_1255.md "wikilink")（希伯来文）,
[1256](../Page/Code_page_1256.md "wikilink")（[阿拉伯文](../Page/阿拉伯文.md "wikilink")）,
[1257](../Page/Code_page_1257.md "wikilink")（Windows Baltic）, [Code page
1258](../Page/Code_page_1258.md "wikilink")（Vietnamese）,
[437](../Page/Code_page_437.md "wikilink")（US）,
[708](../Page/Code_page_708.md "wikilink")（Arabic; ASMO 708）,
[737](../Page/Code_page_737.md "wikilink")（希腊文）,
[775](../Page/Code_page_775.md "wikilink")（MS-DOS波罗的语）,
[850](../Page/Code_page_850.md "wikilink")（WE／拉丁1）,
[852](../Page/Code_page_852.md "wikilink")（拉丁2）,
[855](../Page/Code_page_855.md "wikilink")（IBM西里尔语；基础俄语）,
[857](../Page/Code_page_857.md "wikilink")（MS-DOS IBM Turkish）,
[860](../Page/Code_page_860.md "wikilink")（MS-DOS
[葡萄牙文](../Page/葡萄牙文.md "wikilink")）,
[861](../Page/Code_page_861.md "wikilink")（MS-DOS I冰岛语）,
[862](../Page/Code_page_862.md "wikilink")（希伯来文）,
[863](../Page/Code_page_863.md "wikilink")（MS-DOS
[加拿大](../Page/加拿大.md "wikilink")[法语](../Page/法语.md "wikilink")）,
[864](../Page/Code_page_864.md "wikilink")（阿拉伯文）,
[865](../Page/Code_page_865.md "wikilink")（MS-DOS Nordic）,
[866](../Page/Code_page_866.md "wikilink")（MS-DOS
[俄语](../Page/俄.md "wikilink")）,
[869](../Page/Code_page_869.md "wikilink")（IBM希腊文）,
[874](../Page/Code_page_874.md "wikilink")（[泰语](../Page/泰语.md "wikilink")）,
[932](../Page/Code_page_932.md "wikilink")（JIS/[日语](../Page/日语.md "wikilink")）,
[936](../Page/Code_page_936.md "wikilink")（[简体中文](../Page/简体中文.md "wikilink")）,
[949](../Page/Code_page_949.md "wikilink")（[韩文Wansung](../Page/韩文.md "wikilink")）,
[950](../Page/Code_page_950.md "wikilink")（[繁体中文](../Page/繁体中文.md "wikilink")）,
"[Macintosh Character Set](../Page/MacRoman.md "wikilink")" (US Roman),
and "[Windows
OEM字符集](../Page/Code_page_437.md "wikilink")"。它覆盖了包括无控制字符的Unicode2.0所有码位。

0.86版覆盖范围和0.84版本一样。

1.00和1.01版通过Microsoft Office 2002 (Microsoft Office XP), Microsoft
Office
2003及其单品软件发布。它包含50,377个字形（38,917个字符），将合併发音符减少到72个，将复杂技术字符增加到123个，将私有使用区域字符增加到43个，将空格控制符减少到57个。添加了[1361](../Page/Code_page_1361.md "wikilink")（韩文Johab），并添加了[天城文](../Page/天城文.md "wikilink")，古吉拉特文，[假名](../Page/日语假名.md "wikilink")（[平假名和](../Page/平假名.md "wikilink")[片假名](../Page/片假名.md "wikilink")，Gurmukhi字母[卡纳达语和](../Page/卡纳达语.md "wikilink")[泰米尔语的排版表信息](../Page/泰米尔语.md "wikilink")。其中日韩统一表意符号开始支持竖排。\[2\]它覆盖了包括无控制字符的Unicode2.1所有码位。

## 错误

[Arial_Unicode_MS_bug.png](https://zh.wikipedia.org/wiki/File:Arial_Unicode_MS_bug.png "fig:Arial_Unicode_MS_bug.png")比较）
\]\]

Arial Unicode MS各种版本在处理全角连字字符时会出错，症状是连字弧线向左偏移一个字符的宽度。根据[Unicode
Standard 4.0.0,
section 7.7](http://www.unicode.org/versions/Unicode4.0.0/ch07.pdf)合并两个选中的字符进行连字。尽管如此，要使Arial
Unicode MS看起来正常，可以将全角连字符放在选中字符的后面。这意味着在Arial Unicode
MS必须使用和其他（设计正确的）Unicode字体不同的描摹来正常显示这些字符。

这个错误影响到[国际音标和](../Page/国际音标.md "wikilink")[ALA-LC
Romanization非拉丁文字转写的文本](../Page/American_Library_Association-Library_of_Congress_Romanization_Tables.md "wikilink")。

拉丁字连字fi, fl, ffi, ffl, long st, st中头尾，和末尾两个小写字母无法连接。

阿拉伯文中五星符（U+066D）错误的写成6点而不是5点。

## 参见

其他覆盖Unicode的字体有[Bitstream
Cyberbit](../Page/Bitstream_Cyberbit.md "wikilink")，[TITUS Cyberbit
Basic](../Page/TITUS_Cyberbit_Basic.md "wikilink")，[Code2000](../Page/Code2000.md "wikilink")，[Doulos
SIL](../Page/Doulos_SIL.md "wikilink")，[Lucida Sans
Unicode](../Page/Lucida_Sans_Unicode.md "wikilink")，[Free software
Unicode
typefaces](../Page/Free_software_Unicode_typefaces.md "wikilink")，和[Unicode
fonts等等](../Page/Unicode_typefaces.md "wikilink")。

## 注释

## 外部链接

  - Font catalog entries:
      - [Microsoft typography: Arial Unicode MS -
        Version 1.01](http://www.microsoft.com/typography/fonts/font.aspx?FID=24&FNAME=Arial+Unicode+MS&FVER=1.01)
      - [Microsoft typography: Arial Unicode MS -
        Version 0.84](http://www.microsoft.com/typography/fonts/font.aspx?FID=24&FNAME=Arial+Unicode+MS&FVER=0.84)
      - [Ascender Corporation: Arial Unicode
        Font](http://www.ascendercorp.com/msfonts/arial_unicode.html)
      - [Monotype Imaging: Arial
        Unicode](https://web.archive.org/web/20080305164726/http://www.monotypeimaging.com/ProductsServices/uni_arial.aspx)
  - [Agfa Monotype Unicode Font In
    Windows 2000](https://web.archive.org/web/20061115063318/http://www.monotypeimaging.com/aboutus/pr_display.aspx?year=1999&pr=38)

[Category:无衬线字体](../Category/无衬线字体.md "wikilink")

1.  .
2.