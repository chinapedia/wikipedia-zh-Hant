**Astro
AEC**是[馬來西亞](../Page/馬來西亞.md "wikilink")[Astro擁有的頻道](../Page/Astro.md "wikilink")，全天候24小时播出。該頻道主要播出來自[台灣](../Page/台灣.md "wikilink")、[新加坡的節目](../Page/新加坡.md "wikilink")，同時亦播出本地節目（亦稱**“Astro本地圈”**节目）该频道备有[中文](../Page/中文.md "wikilink")、[马来语及](../Page/马来语.md "wikilink")[英文字幕服务](../Page/英文.md "wikilink")。

## Astro AEC HD

[Astro_AEC_HD.png](https://zh.wikipedia.org/wiki/File:Astro_AEC_HD.png "fig:Astro_AEC_HD.png")
**Astro AEC
HD**是[Astro第五個](../Page/Astro.md "wikilink")[中文](../Page/中文.md "wikilink")[高清頻道](../Page/高清.md "wikilink")，於2014年11月16日正式啟播。该频道於其標清频道同步播出所有節目。

## Astro本地圈

**Astro AEC、Astro AEC
HD**為“Astro本地圈”的起點。在Astro剛啟動“本地圈”時，所有的自製節目都在此頻道播出。隨著本土節目的成功，“Astro本地圈”旗下的節目並不再局限於此頻道播出，而Astro也把旗下中文頻道所製作的本地節目都歸類為“Astro本地圈”节目。

## “Astro本地圈”历年口号

  - 2004年：本地圈 圈动你的心
  - 2007年：本地圈 在身邊
  - 2008年：一起本地圈
  - 2011年：本地圈 全心為您
  - 2013年：本地圈 在這裡 \[1\]
  - 2014年：十年本地圈 \[2\]

## 现时节目

| 日期            | 播出時间                                          | 现时节目                                 | 备注 |
| ------------- | --------------------------------------------- | ------------------------------------ | -- |
| 星期一至五         | 15:30 - 16:00                                 | [美食好简单](../Page/美食好简单.md "wikilink") |    |
| 16:00 - 17:00 | 五零高手                                          |                                      |    |
| 17:00 - 18:30 | [命運好好玩](../Page/命運好好玩_\(電視節目\).md "wikilink") |                                      |    |
| 18:30 - 19:00 | 带你去走走                                         | Astro优先看，与新加坡同日播出                    |    |
| 19:00 - 20:00 | 愛啓航                                           |                                      |    |
| 20:30 - 21:30 | [娛樂百分百](../Page/娛樂百分百.md "wikilink")          | 与台灣同日播出                              |    |
| 星期二           | 21:30 - 22:30                                 | 台湾第一等                                |    |
| 星期一、三、四及五     | [爱玩客](../Page/爱玩客.md "wikilink")              |                                      |    |
| 星期六           | 17:00 - 17:30                                 | 食在好源头 3 精华版                          |    |
| 17:30 - 18:00 | 小毛病 大问题S7                                     |                                      |    |
| 18:00 - 19:00 | 分界线                                           |                                      |    |
| 20:30 - 22:00 | [一千個晚安](../Page/一千個晚安.md "wikilink")          | 与台灣24小时内同日播出                         |    |
| 22:00 - 22:30 | [健康123](../Page/健康123.md "wikilink") S4       |                                      |    |
| 23:00 - 00:00 | [麻辣天后传](../Page/麻辣天后传.md "wikilink")          |                                      |    |
| 星期日           | 17:00 - 18:00                                 | 幸福乡味                                 |    |
| 18:00 - 19:00 | 两代美味关系                                        |                                      |    |
| 20:30 - 21:00 | 魅力东方                                          |                                      |    |
| 21:00 - 22:00 | 节庆飨宴                                          |                                      |    |
| 22:00 - 22:30 | 老友出走记                                         |                                      |    |
| 每晚            | 20:00 - 20:30                                 | 八点最热报                                | 直播 |
| 22:30 - 23:00 | 新闻报报看                                         |                                      |    |
|               |                                               |                                      |    |

## 新春节目

| 日期            | 播出時间                                         | 现时节目                                                      | 备注 |
| ------------- | -------------------------------------------- | --------------------------------------------------------- | -- |
| 2月4日（除夕）      | 17:00 - 17:30                                | [美食好简单](../Page/美食好简单.md "wikilink")2019新春特备              |    |
| 17:30 - 17:50 | 大地回春首映特备                                     |                                                           |    |
| 20:00 - 21:00 | [爱玩客](../Page/爱玩客.md "wikilink")2019新春特备     |                                                           |    |
| 21:00 - 21:30 | 欠你一个拥抱                                       |                                                           |    |
| 21:30 - 00:15 | 勇气棒嘟嘟迎春接福大庆典                                 |                                                           |    |
| 2月5日（年初一）     | 00:15 - 02:00                                | 经典成名曲音乐会                                                  |    |
| 14:30 - 17:00 | 新春电影 - [败者为王](../Page/败者为王.md "wikilink")    |                                                           |    |
| 17:00 - 17:30 | [美食好简单](../Page/美食好简单.md "wikilink")2019新春特备 |                                                           |    |
| 18:30 - 19:00 | 带你去走走（第71集）                                  |                                                           |    |
| 20:30 - 22:00 | 好经典2019桃李争春（一）                               |                                                           |    |
| 22:00 - 23:00 | [爱玩客](../Page/爱玩客.md "wikilink")2019新春特备     |                                                           |    |
| 2月6日（年初二）     | 07:00 - 13:00                                | 2018 - 2019[台北最High新年城](../Page/台北最High新年城.md "wikilink") |    |
| 14:30 - 17:00 | 新春电影 - [战狼2](../Page/战狼2.md "wikilink")      |                                                           |    |
| 17:00 - 17:30 | [美食好简单](../Page/美食好简单.md "wikilink")2019新春特备 |                                                           |    |
| 18:30 - 19:00 | 带你去走走（第72集）                                  |                                                           |    |
| 20:30 - 22:00 | 好经典2019桃李争春（二）                               |                                                           |    |
| 22:00 - 23:00 | [爱玩客](../Page/爱玩客.md "wikilink")2019新春特备     |                                                           |    |
| 23:00 - 00:00 | 谢谢你出现在我的行程里（第6集）                             |                                                           |    |
| 2月7日（年初三）     | 07:00 - 13:00                                | [2019超级巨星红白艺能大赏](../Page/2019超级巨星红白艺能大赏.md "wikilink")    |    |
| 14:30 - 17:30 | 新春电影 - [红海行动](../Page/红海行动.md "wikilink")    |                                                           |    |
| 17:30 - 18:00 | [美食好简单](../Page/美食好简单.md "wikilink")2019新春特备 |                                                           |    |
| 18:30 - 19:00 | 带你去走走（第73集）                                  |                                                           |    |
| 20:30 - 22:00 | 好经典2019桃李争春（三）                               |                                                           |    |
| 22:00 - 23:00 | [爱玩客](../Page/爱玩客.md "wikilink")2019新春特备     |                                                           |    |
| 23:00 - 00:00 | 谢谢你出现在我的行程里（第7集）                             |                                                           |    |
| 2月8日（年初四）     | 08:30 - 11:30                                | 经典名曲歌唱大赛2018决赛                                            | 重播 |
| 14:30 - 17:00 | 新春电影 - [猜猜我是谁](../Page/猜猜我是谁.md "wikilink")  |                                                           |    |
| 17:00 - 17:30 | [美食好简单](../Page/美食好简单.md "wikilink")2019新春特备 |                                                           |    |
| 17:30 - 18:30 | [爱玩客](../Page/爱玩客.md "wikilink")2019新春特备     |                                                           |    |
| 18:30 - 19:00 | 带你去走走（第74集）                                  |                                                           |    |
| 23:00 - 00:00 | 谢谢你出现在我的行程里（第8集）                             |                                                           |    |
| 2月9日（年初五）     | 14:30 - 17:00                                | 新春电影 - [西游记之孙悟空三打白骨精](../Page/西游记之孙悟空三打白骨精.md "wikilink") |    |
| 20:30 - 22:00 | [非诚勿扰](../Page/非诚勿扰.md "wikilink")           |                                                           |    |
| 22:00 - 22:30 | 徤康123 2019新春特备                               |                                                           |    |
| 2月10日（年初六）    | 19:00 - 20:00                                | 节庆享宴 2019新春特备                                             |    |

## 直播节目列表

## Astro自製及合拍節目/電視劇

### 正在播出電視節目

<table style="width:180%;">
<colgroup>
<col style="width: 17%" />
<col style="width: 18%" />
<col style="width: 70%" />
<col style="width: 35%" />
<col style="width: 40%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><strong>節目名稱</strong></p></td>
<td><p><strong>主持人/主播</strong></p></td>
<td><p><strong>首播日期</strong></p></td>
<td><p><strong>節目內容</strong></p></td>
<td><p><strong>網頁</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/新闻报报看.md" title="wikilink">新闻报报看</a><br />
Evening Edition</p></td>
<td><p>颜江瀚、陈韵传、庄文杰、梁宝仪、<br />
关萃汶、王钶媃、蔡心惠</p></td>
<td><p>2006年6月12日</p></td>
<td><p>每晚10点30分主播们播报观众最关心的国内外新闻重点。</p></td>
<td><p><a href="https://zh-cn.facebook.com/EveningEdition/">網頁</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/八点最热报.md" title="wikilink">八点最热报</a><br />
2000 Prime Talk</p></td>
<td><p>颜江瀚、<a href="../Page/萧慧敏.md" title="wikilink">萧慧敏</a>、陈韵传、庄文杰、<br />
梁宝仪、关萃汶</p></td>
<td><p>2014年12月1日</p></td>
<td><p>每晚准时8点提供给观众国内及国际最新新闻资讯。</p></td>
<td><p><a href="https://zh-cn.facebook.com/aecprimetalk/">網頁</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Astro经典名曲歌唱大赛.md" title="wikilink">好經典2018</a><br />
The Golden Melodies 2018</p></td>
<td></td>
<td><p>2018年11月10日</p></td>
<td><p>|馬來西亞經典曲獻唱節目。每星期六晚上8:30播出</p></td>
<td><p><a href="https://web.facebook.com/AstroCGM/">網頁</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 正在播出電視戲劇

|        |        |        |        |        |
| ------ | ------ | ------ | ------ | ------ |
| **劇名** | **演员** | **集數** | **備註** | **網頁** |
|        |        |        |        |        |

### 即將播出電視節目及戲劇

|          |            |          |          |        |
| -------- | ---------- | -------- | -------- | ------ |
| **節目名稱** | **主持人/演员** | **首播日期** | **節目內容** | **網頁** |
|          |            |          |          |        |

### 已經播畢的電視節目及戲劇

#### 2018年

<table style="width:225%;">
<colgroup>
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 13%" />
<col style="width: 70%" />
<col style="width: 35%" />
<col style="width: 40%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><strong>節目名稱</strong></p></td>
<td><p><strong>集数</strong></p></td>
<td><p><strong>主持人/演员</strong></p></td>
<td><p><strong>首播日期</strong></p></td>
<td><p><strong>節目內容</strong></p></td>
<td><p><strong>網頁</strong></p></td>
</tr>
<tr class="even">
<td><p>九5搭8<br />
Where Got Money?</p></td>
<td><p>26</p></td>
<td><p><a href="../Page/陈建宏.md" title="wikilink">陈建宏</a>、<a href="../Page/林建文.md" title="wikilink">林建文</a>、<a href="../Page/黄薏玲.md" title="wikilink">黄薏玲</a>、<a href="../Page/曾潔鈺.md" title="wikilink">曾潔鈺</a>、<br />
<a href="../Page/吕爱琼.md" title="wikilink">吕爱琼</a></p></td>
<td><p>2017年12月9日</p></td>
<td><p>Astro自制处境喜剧<br />
共播两季，一季共13集</p></td>
<td><p><a href="http://www.xuan.com.my/channels/aec/article/articleid/16219/95%E6%90%AD8">網頁</a></p></td>
</tr>
<tr class="odd">
<td><p>爱的交响曲</p></td>
<td><p>6</p></td>
<td><p>颜江瀚</p></td>
<td><p>1月19日</p></td>
<td><p>真人秀节目。</p></td>
<td><p><a href="http://www.xuan.com.my/lifestyle/article/articleid/15598/tabid/992/%E7%88%B1%E7%9A%84%E4%BA%A4%E5%93%8D%E6%9B%B2-Symphony-of-Love">网页</a></p></td>
</tr>
<tr class="even">
<td><p>新年FUN新吃</p></td>
<td><p>5</p></td>
<td><p>萧慧敏</p></td>
<td><p>年1月21日</p></td>
<td><p>新年特备烹饪节目。</p></td>
<td><p><a href="http://www.xuan.com.my/2018/cny/article/17505/%E6%96%B0%E5%B9%B4fun%E6%96%B0%E5%90%83">网页</a></p></td>
</tr>
<tr class="odd">
<td><p>新春暖心食谱</p></td>
<td><p>5</p></td>
<td></td>
<td><p>年2月15日</p></td>
<td><p>新年特备烹饪节目。</p></td>
<td><p><a href="http://www.xuan.com.my/2018/cny/article/17508/%E6%96%B0%E6%98%A5%E6%9A%96%E5%BF%83%E9%A3%9F%E8%B0%B1-Sr-2">网页</a></p></td>
</tr>
<tr class="even">
<td><p>麦玲玲 Whoopee 好风水</p></td>
<td><p>6</p></td>
<td><p><a href="../Page/麦玲玲.md" title="wikilink">麦玲玲</a>、<a href="../Page/萧慧敏.md" title="wikilink">萧慧敏</a></p></td>
<td><p>2月15日</p></td>
<td><p>新年特备节目。</p></td>
<td><p><a href="http://www.xuan.com.my/2018/cny/article/17607/%E9%BA%A6%E7%8E%B2%E7%8E%B2-Whoopee-%E5%A5%BD%E9%A3%8E%E6%B0%B4">网页</a></p></td>
</tr>
<tr class="odd">
<td><p>活出自己 快乐Whoopee 迎春接福大庆典</p></td>
<td><p>1</p></td>
<td><p>颜江瀚、林德荣、林震前、菲比、许佳麟、陈浩然、赖淞凤、颜慧萍、凯心、阿牛</p></td>
<td><p>2月15日</p></td>
<td><p>新年特备歌唱节目。</p></td>
<td><p><a href="http://www.xuan.com.my/2018/cny/article/17507/%E6%B4%BB%E5%87%BA%E8%87%AA%E5%B7%B1-%E5%BF%AB%E4%B9%90WHOOPEE-%E8%BF%8E%E6%98%A5%E6%8E%A5%E7%A6%8F%E5%A4%A7%E5%BA%86%E5%85%B8">网页</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Astro经典名曲歌唱大赛.md" title="wikilink">好经典新春大籍汇</a></p></td>
<td><p>1</p></td>
<td><p>谢玲玲、李燕萍、白琳、石鲡仪、黄一飞、陈建彬、山脚下男孩、杨克意、蔡宝珠</p></td>
<td><p>2月16日</p></td>
<td><p>新年特备歌唱节目。</p></td>
<td><p><a href="http://www.xuan.com.my/2018/cny/article/17511/%E5%A5%BD%E7%BB%8F%E5%85%B8%E6%96%B0%E6%98%A5%E5%A4%A7%E7%B1%8D%E6%B1%87">网页</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Astro经典名曲歌唱大赛.md" title="wikilink">好经典桃李争春</a></p></td>
<td><p>2</p></td>
<td><p>主持人：颜江瀚<br />
导师：风采姐妹，曾潍山<br />
评审团：黄一飞，陈建彬，郑国亮<br />
蓝宝石队员：杨克意，卢润江，陈时达，吴国豪，方爱凌，周蕊丽，张淑凤，曾曼文。<br />
金钥匙队员：Jazz白琳，黄雪云，石鲡仪，陈丽媚，庄盛，邓和利，程敏莉，张燕凌</p></td>
<td><p>2月17日<br />
2月18日</p></td>
<td><p>新年特备歌唱节目。</p></td>
<td><p><a href="https://web.facebook.com/AstroCGM/">网页</a></p></td>
</tr>
<tr class="even">
<td><p>舞蔬弄果(第二季)<br />
Fruit, Veggies &amp; Me<br />
(Season 2)</p></td>
<td><p>13</p></td>
<td></td>
<td><p>2月24日</p></td>
<td><p>分享给观众制作创意蔬果料理</p></td>
<td><p><a href="http://www.xuan.com.my/bdq/article/articleid/12290/episodeid/1505/%E8%88%9E%E8%94%AC%E5%BC%84%E6%9E%9C">網頁</a></p></td>
</tr>
<tr class="odd">
<td><p>暖心食谱(第二季)<br />
Comfort Food Recipes<br />
(Season 2)</p></td>
<td><p>13</p></td>
<td></td>
<td><p>2月25日</p></td>
<td><p>邀请各地各族的料理人分享私藏食谱</p></td>
<td><p><a href="http://www.xuan.com.my/bdq/article/articleid/12290/episodeid/1505/%E8%88%9E%E8%94%AC%E5%BC%84%E6%9E%9C">網頁</a></p></td>
</tr>
<tr class="even">
<td><p>房地产百万富翁就是你!<br />
Anyone Can Be A Property Millionaire</p></td>
<td><p>10</p></td>
<td></td>
<td><p>3月26日</p></td>
<td><p>电视真人秀节目</p></td>
<td><p><a href="http://www.xuan.com.my/channels/aec/article/18034/tabid/1031/%E6%88%BF%E5%9C%B0%E4%BA%A7%E7%99%BE%E4%B8%87%E5%AF%8C%E7%BF%81%E5%B0%B1%E6%98%AF%E4%BD%A0">網頁</a></p></td>
</tr>
<tr class="odd">
<td><p>生产者<br />
Food Producers</p></td>
<td><p>13</p></td>
<td></td>
<td><p>5月6日</p></td>
<td><p>为观众掀开食材产制的源头和过程，参考生产者推荐的烹饪方式，烹制成美味佳肴！</p></td>
<td><p><a href="https://web.facebook.com/astroaec">網頁</a></p></td>
</tr>
<tr class="even">
<td><p>选战最热报2018<br />
Polling Night 2018</p></td>
<td><p>1</p></td>
<td><p>颜江瀚、<a href="../Page/萧慧敏.md" title="wikilink">萧慧敏</a>、陈韵传、庄文杰、<br />
梁宝仪、关萃汶、王钶媃、蔡心惠</p></td>
<td><p>5月9日</p></td>
<td><p>《第十四届马来西亚大选》开票成绩特别节目，直播时间从晚上7点至凌晨4点结束。Astro AEC频道及Youtube同步转播。</p></td>
<td><p><a href="https://www.youtube.com/watch?v=-9gbLZYaYvg">網頁</a></p></td>
</tr>
<tr class="odd">
<td><p>问神<br />
Pray For?</p></td>
<td><p>13</p></td>
<td><p>苏诗僡</p></td>
<td><p>2018年5月12日</p></td>
<td></td>
<td><p><a href="https://web.facebook.com/astroaec">網頁</a></p></td>
</tr>
<tr class="even">
<td><p>健康密码（第二季）<br />
Health Code<br />
(Season 2)</p></td>
<td><p>13</p></td>
<td><p>欧阳林医生、梁宝仪</p></td>
<td><p>2018年5月26日</p></td>
<td><p>节目请来了多位专业的医生当嘉宾，还有明星医生作家－欧阳林医生与知性女主播－梁宝仪搭档主持，为你的健康把脉！</p></td>
<td><p><a href="https://web.facebook.com/astroaec">網頁</a></p></td>
</tr>
<tr class="odd">
<td><p>企业大联盟<br />
Business Talk</p></td>
<td><p>13</p></td>
<td></td>
<td><p>2018年6月4日</p></td>
<td><p>此节目是一个专为中小型企业而设的财经节目，企业家和专家将剖析时下热门商业话题和难关，在现场就必须做出快、狠、准的抉择，带你看见危机、寻找转机、开创商机。</p></td>
<td><p><a href="http://www.xuan.com.my/channels/aec/article/19635/tabid/1058/%E4%BC%81%E4%B8%9A%E5%A4%A7%E8%81%94%E7%9B%9F">網頁</a></p></td>
</tr>
<tr class="even">
<td><p>青色围墙<br />
Green Wall</p></td>
<td><p>6</p></td>
<td><p>Gabriel、蔡佩璇、温绍平、萧莉璇、周雪婷、梁瑜麟、周力文、朱健美</p></td>
<td><p>6月23日</p></td>
<td><p>Astro中文青春喜剧<br />
改编自本地作家许友彬的同名畅销小说--《青色围墙》<br />
2018年6月9日 - 制作特辑<br />
2018年6月16日 - 番外篇<br />
2018年6月23日 - 晚上11:00正式播出</p></td>
<td><p><a href="http://www.xuan.com.my/2018/greenwall/">網頁</a></p></td>
</tr>
<tr class="odd">
<td><p>百日论坛<br />
Hundred Days Forum</p></td>
<td><p>1</p></td>
<td><p>萧慧敏</p></td>
<td><p>2018年8月17日</p></td>
<td><p>马来西亚首度政党轮替，希盟政府执政第100日，Astro AEC特别制作政论直播节目，出席嘉宾有<strong>刘镇东、张念群、魏家祥、周美芬</strong>，节目直播日期及时间 8月17日(星期五) 9.30pm - 11.00pm</p></td>
<td><p><a href="http://www.hotspot.com.my/forum">網頁</a></p></td>
</tr>
<tr class="even">
<td><p>叫我男神（回顾）<br />
Call Me Handsome<br />
(Recap)</p></td>
<td><p>3</p></td>
<td><p>林德荣、何念兹、Jaspers</p></td>
<td><p>2018年8月11日</p></td>
<td><p>深夜综艺节目。</p></td>
<td><p><a href="https://web.facebook.com/AstroCallMeHandsome/">網頁</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Astro经典名曲歌唱大赛.md" title="wikilink">Astro经典名曲歌唱大赛2018</a><br />
Astro Classic Golden Melody</p></td>
<td><p>19</p></td>
<td><p>顏江瀚</p></td>
<td><p>2018年6月30日</p></td>
<td><p>马来西亚大型经典名曲歌唱比赛节目。</p></td>
<td><p><a href="https://web.facebook.com/AstroCGM/">網頁</a></p></td>
</tr>
<tr class="even">
<td><p>阿贤逛巴刹<br />
Jason's Market Trails</p></td>
<td></td>
<td><p>杨佳贤</p></td>
<td><p>2018年8月5日</p></td>
<td><p>阿贤全新饮食节目，节目将纪录全马各地巴刹的老好滋味。</p></td>
<td><p><a href="https://web.facebook.com/astroaec/">網頁</a></p></td>
</tr>
<tr class="odd">
<td><p>寻花探草<br />
Garden of Treasure<br />
(Season 5)</p></td>
<td><p>13</p></td>
<td><p>庄文杰</p></td>
<td><p>2018年8月25日</p></td>
<td><p>由Cloud Production制作的休闲资讯节目。跟着主持人庄文杰的脚步“翻山越岭”找寻有益花草。每星期六晚上10:00播出。</p></td>
<td><p><a href="https://web.facebook.com/AstroCallMeHandsome/">網頁</a></p></td>
</tr>
<tr class="even">
<td><p>叫我男神（第三季）<br />
Call Me Handsome<br />
(Season 3)</p></td>
<td><p>14</p></td>
<td><p>林德荣、何念兹、Jaspers</p></td>
<td><p>2018年9月1日</p></td>
<td><p>深夜综艺节目。<br />
节目中男神有张哲伟、潘胤杰、谢允、Hero祖雄、黄旭及宇炯龙。</p></td>
<td><p><a href="https://web.facebook.com/mycloudproduction/">網頁</a></p></td>
</tr>
<tr class="odd">
<td><p>全家私房钱（第二季）<br />
Money Money Home<br />
(Season 2)</p></td>
<td><p>13</p></td>
<td><p>劉元元、颜江瀚</p></td>
<td><p>2018年9月3日</p></td>
<td><p>节目中和观众分享各种理财秘诀。星期一晚上10:00播出</p></td>
<td><p><a href="https://web.facebook.com/moneymoneyhome.my/">網頁</a></p></td>
</tr>
<tr class="even">
<td><p>零距离<br />
Up Close</p></td>
<td><p>13</p></td>
<td><p>萧慧敏</p></td>
<td><p>2018年9月16日</p></td>
<td><p>主持人萧慧敏独家一对一专访马来西亚政治人物。每星期日晚上9:00播出</p></td>
<td><p><a href="https://web.facebook.com/astroaec/">網頁</a></p></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><p>剧集</p></td>
<td><p>资讯/娱乐</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 外部链接

  - [Astro AEC官方網站](http://www.xuan.com.my/channels/aec)

  -
  -
  - (舊頻道)

  - [Astro AEC节目表](http://www.quake.com.my)

## 註明

[Category:Astro](../Category/Astro.md "wikilink")
[Category:馬來西亞電視台](../Category/馬來西亞電視台.md "wikilink")

1.
2.  “十年本地圈”之新秀帮 - 本地圈 - Astro 中文视界
    <http://zhongwen.astro.com.my/Channels/HHD/ArticleDetails.aspx?articleid=3708>