**Atlassian**（）是一家[澳大利亞軟件企業公司](../Page/澳大利亞.md "wikilink")，主要為[軟件開發者及](../Page/軟件開發者.md "wikilink")[項目經理設計軟件](../Page/項目經理.md "wikilink")\[1\]\[2\]\[3\]。這公司的主力產品為專門用作追蹤應用程式問題的[JIRA](../Page/JIRA.md "wikilink")，還有用作協作團隊的產品[Confluence](../Page/Confluence.md "wikilink")\[4\]\[5\]。現時，Atlassian在全球的客戶有超過6萬家\[6\]\[7\]\[8\]\[9\]\[10\]。

Atlassian的總部設於[新南威爾士州的首府](../Page/新南威爾士州.md "wikilink")[悉尼](../Page/悉尼.md "wikilink")\[11\]，不過在[歐洲的](../Page/歐洲.md "wikilink")[阿姆斯特丹和](../Page/阿姆斯特丹.md "wikilink")[北美洲的](../Page/北美洲.md "wikilink")[三藩市亦有辦事處](../Page/三藩市.md "wikilink")。在2014年9月，公司有僱員1,148名，辦事處位於12個城市，超過四萬名客戶及數以百萬計的用戶\[12\]\[13\]。2014年2月14日，Atlassian的總裁Jay
Simons宣佈在[美國](../Page/美國.md "wikilink")[德薩斯州首府](../Page/德薩斯州.md "wikilink")[奧斯汀開設新的辦事處](../Page/奧斯汀.md "wikilink")，並會聘請600名員工。

## 歷史

Cenqua是一家[澳大利亞的](../Page/澳大利亞.md "wikilink")[軟件開發公司](../Page/軟件開發.md "wikilink")，專門開發為[Java編程而設計的軟件測試及支援工具](../Page/Java.md "wikilink")。Cenqua於1999年成立，當時的名稱叫作「Cortex
eBusiness」，正值[科網股泡沫出現的時候](../Page/科網股泡沫.md "wikilink")。當時，他們專門為這些dotcom公司開發應用戶要求的軟件。在科網股爆破之後，公司把原來用作內部測試及開發軟件的工具拿出來發售。於2007年8月1日被**Atlassian**併購。\[14\]\[15\]\[16\]

## 產品

Cenqua的產品有以下三種：

  - [FishEye](../Page/FishEye.md "wikilink")（[產品網頁](https://web.archive.org/web/20070531203816/http://www.cenqua.com/fisheye/)）：[CVS及版本控制軟件的](../Page/CVS.md "wikilink")[瀏覽器介面](../Page/瀏覽器.md "wikilink")。
  - [Clover](../Page/Clover.md "wikilink")（[產品網頁](https://web.archive.org/web/20070605083939/http://www.cenqua.com/clover/)）：[Java測試軟件](../Page/Java.md "wikilink")。
  - [Crucible](../Page/Crucible.md "wikilink")（[產品網頁](https://web.archive.org/web/20070605084338/http://www.cenqua.com/crucible/)）：容許源程序作[同行評審的軟件](../Page/同行評審.md "wikilink")。

## 參考資料

[Category:软件公司](../Category/软件公司.md "wikilink")
[Category:悉尼公司](../Category/悉尼公司.md "wikilink")
[Category:2002年成立的公司](../Category/2002年成立的公司.md "wikilink")

1.

2.

3.

4.
5.

6.

7.
8.
9.

10.

11.
12.
13.

14.

15.

16.