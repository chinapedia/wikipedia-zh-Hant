**亞利山大·詹姆斯·「A.
J.」·麥克林**（****，）\[1\]是一位[美國](../Page/美國.md "wikilink")[歌手](../Page/歌手.md "wikilink")，[新好男孩成員之一](../Page/新好男孩.md "wikilink")。

## 簡歷

### 早年生活

麥克林出生於[佛羅-{里}-達州的](../Page/佛羅里達.md "wikilink")[西棕榈滩](../Page/西棕榈滩_\(佛罗里达州\).md "wikilink")，他的父親是鮑伯·麥克林，母親是丹尼斯·費南德斯·麥克林。他的雙親在他四歲時離婚，\[2\]麥克林是由母親與外祖父母烏蘇拉·費南德斯、阿道夫·費南德斯撫養大的。1990年，麥克林與母親遷至[奧蘭多以便更好地發展演藝事業](../Page/奧蘭多.md "wikilink")。在為[迪士尼樂園演出數次後](../Page/迪士尼樂園.md "wikilink")，他加入了[新好男孩組合並一舉成名](../Page/新好男孩.md "wikilink")。

### 精神疾病

麥克林在加入樂隊後迅速成名，並成為了樂隊中的「壞孩子」。2001年，樂隊的其他成員表示對麥克林的一些舉動感到很失望，麥克林在這段時期情緒消沉，染上了[酒癮與](../Page/酒癮.md "wikilink")[可卡因](../Page/可卡因.md "wikilink")[毒癮](../Page/毒癮.md "wikilink")。後來他的母親丹尼斯·麥克林撰寫了一本名為《新好男孩的母親》（*Backstreet
Mom*）的書，詳細描述了麥克林在成為癮君子前後的生活。\[3\]

2003年12月，麥克林與母親參加了一次[脫口秀](../Page/脫口秀.md "wikilink")，他詳細介紹了他的[抑郁症](../Page/抑郁症.md "wikilink")、[酒癮以及](../Page/酒癮.md "wikilink")[毒癮的病史](../Page/毒癮.md "wikilink")。在這次脫口秀上，[奇雲·理查森](../Page/奇雲·理查森.md "wikilink")、[布萊恩·利特爾](../Page/布萊恩·利特爾.md "wikilink")、[豪兒·多羅夫與](../Page/豪兒·多羅夫.md "wikilink")[尼克·卡特突然出現](../Page/尼克·卡特.md "wikilink")，給了麥克林一個惊喜。他們討論了麥克林的疾病，並鼓舞麥克林重新站起來。這幾乎是新好男孩兩年來第一次在一起。

### 另一個自我

為了將自己內心深處的另一個自我得到釋放，在不用作為新好男孩的一員演出時，麥克林將自己化名為“強尼·無名氏”（）。\[4\]
兩個自我擁有相同之處，都是單親家庭長大，都在年輕時與外祖父母生活。然而兩者不同的是，強尼曾入獄，而麥克林沒有。\[5\]
麥克林經常以「強尼·無名氏」為名參加[紐約附近的](../Page/紐約.md "wikilink")[搖滾樂團](../Page/搖滾樂.md "wikilink")。他還成立了基金會，為糖尿病研究和校園音樂的發展籌集善款。麥克林還曾以無名約翰的身份做了九城巡演來支持VH1電視臺。

麥克林原本打算採用「強尼·蘇」（Johnny
Suede）作為自己的藝名，這個名字是[布拉德·皮特曾飾演的一角色名](../Page/布拉德·皮特.md "wikilink")。後來電影片方要起訴麥克林，他便將名字改作「強尼·無名氏」。

## 參考資料

## 相關連結

  - [A.J Mclean My Space](http://www.myspace.com/ajmclean)

  - [A.J Mclean 官方網站](http://www.aj-mclean.com/)

  -
[Category:新好男孩成員](../Category/新好男孩成員.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:蘇格蘭裔美國人](../Category/蘇格蘭裔美國人.md "wikilink")
[Category:英格蘭裔美國人](../Category/英格蘭裔美國人.md "wikilink")
[Category:爱尔兰裔美国人](../Category/爱尔兰裔美国人.md "wikilink")
[Category:古巴裔美國人](../Category/古巴裔美國人.md "wikilink")
[Category:美国男歌手](../Category/美国男歌手.md "wikilink")
[Category:美国流行音乐歌手](../Category/美国流行音乐歌手.md "wikilink")
[Category:21世紀美國歌手](../Category/21世紀美國歌手.md "wikilink")

1.

2.
3.  <http://www.backstreetmom.com>

4.  [A.J. McLean talks about "Johnny No
    Name"](http://www.canoe.ca/AllPop-BackstreetBoys/000329_johnny.html)

5.  [JNN Fountation - Founding
    Story](http://www.johnnyno-name.com/foundation/story.asp)