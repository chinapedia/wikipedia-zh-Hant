**B\&Q**（中文：百安居、特力屋）是一家[跨國的大型](../Page/跨國公司.md "wikilink")[DIY用家庭與園藝工具材料](../Page/DIY.md "wikilink")[連鎖店與](../Page/連鎖店.md "wikilink")[大賣場](../Page/大賣場.md "wikilink")，1969年成立於[英國](../Page/英國.md "wikilink")[南安普敦](../Page/南安普敦.md "wikilink")，目前屬於世界前500大企業之一的[英國](../Page/英國.md "wikilink")[翠豐集團](../Page/翠豐集團.md "wikilink")（Kingfisher
Plc）旗下，是[英國第一大的家庭與園藝用品連鎖店](../Page/英國.md "wikilink")，在全球則排名第三。B\&Q的公司创始人是理查·布洛克（Richard
Block）與大衛·奎勒（David
Quayle），B\&Q的公司名稱来自两位创始人姓氏布洛克（Block）与奎勒（Quayle）的英文首字母缩写。除了百安居，该集团还拥有Screwfix、Brico
Dépôt和Castorama家装DIY连锁店，以及土耳其Koctas公司50%的股权。

## B\&Q在台灣的發展

[臺灣臺北市特力屋.jpg](https://zh.wikipedia.org/wiki/File:臺灣臺北市特力屋.jpg "fig:臺灣臺北市特力屋.jpg")
翠豐集團在1995年時與[特力集團以各半的持股](../Page/特力集團.md "wikilink")，在[台灣成立B](../Page/台灣.md "wikilink")\&Q第一家海外[子公司](../Page/子公司.md "wikilink")——特力翠豐，並於1996年時以「**B\&Q特力屋**」品牌名稱於台灣[桃園縣](../Page/桃園市.md "wikilink")[南崁成立第一家海外分店](../Page/南崁.md "wikilink")。在翠豐集團眾多海外投資中，台灣的B\&Q特力屋是獲利最高的海外據點。截至2007年底為止，B\&Q特力屋在台灣至少擁有22家分店以及網路商店。2006年營收超過106億[新台幣](../Page/新台幣.md "wikilink")。在居家修繕產品方面，其主要對手為[普來利實業經營的台灣本土連鎖品牌](../Page/普來利實業.md "wikilink")[好博家](../Page/好博家.md "wikilink")（Homebox）以及以台灣中部為主要市場的連鎖五金賣場[振宇五金](../Page/振宇五金.md "wikilink")；而在家具銷售方面，主要競爭對手則為[宜家家居](../Page/宜家家居.md "wikilink")（IKEA）。

2007年12月18日，特力集團董事長何湯雄宣佈以34.6億新台幣收購原本由翠豐集團所持有的特力翠豐5成股權，將B\&Q特力屋完全納入特力集團旗下。根據協議，特力集團仍能繼續在台灣使用B\&Q的品牌，為期兩年；在期限之後，「B\&Q」英文的品牌將無法再使用，「[特力屋](../Page/特力屋.md "wikilink")」中文的品牌則延續使用，特力翠豐公司之後也更名為特力屋公司。\[1\]\[2\]B\&Q正式退出臺灣市場。

## B\&Q在的香港發展

[MegaBoxB\&Q_20070607.jpg](https://zh.wikipedia.org/wiki/File:MegaBoxB&Q_20070607.jpg "fig:MegaBoxB&Q_20070607.jpg")

英國翠豐集團在2005年10月正式進駐香港，當時[投資推廣署曾透過](../Page/投資推廣署.md "wikilink")[政府新聞處發出新聞稿恭賀](../Page/政府新聞處.md "wikilink")，表示對這間歐洲主要零售商願意在香港發展投下信心的一票。2007年6月7日，B\&Q首間分店在[九龍灣](../Page/九龍灣.md "wikilink")[MegaBox開幕](../Page/MegaBox.md "wikilink")，初期的推廣活動，投資推廣署高層亦多次到賀。\[3\]一年後於馬鞍山頌安邨商場開設首家家居設計室。

不過由於B\&Q主要銷售大型DIY用家庭與園藝工具材料，不適合香港家居環境-{zh-tw:狹小;zh-hk:細小;zh-cn:狭小;}-的市場。再加上2008年發生金融海嘯下，企業生意減少，但舖租卻未回落，B\&Q在2009年6月25日宣佈，分店將於2009年9月停止營業，原鋪位則會開設[宜家家居](../Page/宜家家居.md "wikilink")，顧客服務維持至2009年年底。\[4\]

## B\&Q在其他海外市場

  - 1998年收购[波兰的第一大DIY零售商NOMI](../Page/波兰.md "wikilink")，並合并[法国的DIY龍頭Castorama](../Page/法国.md "wikilink")。
  - 1999年又以**-{百安居}-**的命名進軍[中国市場](../Page/中国.md "wikilink")，首家分店位於[上海](../Page/上海.md "wikilink")，截至2008年底為止，百安居在中国有61家分店。
  - 2014年12月22日[翠豐集團](../Page/翠豐集團.md "wikilink")（kingfisher）宣布，已同意將旗下[百安居中國](../Page/百安居.md "wikilink")（b\&q
    china）業務的控股股權（70%）以1.4億英鎊（約合13.6億人民幣）出售給北京物美投資集團有限公司，翠豐集團在合資企業中將保留30%股權。

## 參考資料

## 外部链接

  - [英國-{百安居}-官方網站](http://www.diy.com)
  - [中國-{百安居}-官方網站](https://web.archive.org/web/20110315131545/http://www.bnq.com.cn/)

<!-- end list -->

  - [法國Castorama官方網站](http://www.castorama.fr/)
  - [法國PAvé Granit官方網站](http://www.desenvolmente.com/fr/)

[Category:英国百货公司](../Category/英国百货公司.md "wikilink")
[Category:跨國公司](../Category/跨國公司.md "wikilink")
[Category:傢俱製造商](../Category/傢俱製造商.md "wikilink")

1.  [聯合新聞網](http://210.244.31.140/NEWS/STOCK/STO4/4139285.shtml)
2.  [東森新聞](http://www.ettoday.com/2007/12/18/320-2203468.htm)
3.  [明報](http://property.mpfinance.com/cfm/pa3.cfm?File=20090626/paa01/laa2.txt)
4.  香港經濟日報：宜家15萬呎新店明年7月開業　百安居撤出香港MegaBox店舖舊換新，2009年6月26日