**B'z**（）是一個由[吉他手](../Page/吉他手.md "wikilink")[松本孝弘與](../Page/松本孝弘.md "wikilink")[主唱](../Page/主唱.md "wikilink")[稻葉浩志所組成的](../Page/稻葉浩志.md "wikilink")[日本](../Page/日本.md "wikilink")[搖滾](../Page/搖滾.md "wikilink")[組合](../Page/音樂組合.md "wikilink")
\[1\]\[2\]。所屬事務所為[VERMILLION](../Page/VERMILLION.md "wikilink")。所屬唱片公司是[VERMILLION
RECORDS](../Page/VERMILLION_RECORDS.md "wikilink")。官方粉絲俱樂部為「[B'z
Party](../Page/B'z_Party.md "wikilink")」。

## 概要

1988年以同時發售單曲「[だからその手を離して](../Page/だからその手を離して.md "wikilink")」及專輯『[B'z](../Page/B'z_\(1988年專輯\).md "wikilink")』出道。在1990年發售的5th單曲「[太陽のKomachi
Angel](../Page/太陽のKomachi_Angel.md "wikilink")」首次獲得[Oricon公信榜第](../Page/Oricon公信榜.md "wikilink")1位，在同年發售的4th專輯『[RISKY](../Page/RISKY.md "wikilink")』、1991年發售的10th單曲「[LADY
NAVIGATION](../Page/LADY_NAVIGATION.md "wikilink")」首次獲得百萬銷量紀錄等作品熱銷不斷\[3\]，迄今為止已有15張單曲、19張專輯獲得百萬銷量紀錄。其中，1998年發售的[精選輯](../Page/精選輯.md "wikilink")『[B'z
The Best
"Pleasure"](../Page/B'z_The_Best_"Pleasure".md "wikilink")』除了是日本國內音樂史上首次總銷量突破500萬張以外，與同年發售的『[B'z
The Best
"Treasure"](../Page/B'z_The_Best_"Treasure".md "wikilink")』2作品合計獲得約1,000萬張總銷量紀錄\[4\]。B'z合計唱片總銷量突破8,000萬張，為日本Oricon公信榜設立以來最高成績，於全球最暢銷歌手中排行第64位，在亞洲最暢銷歌手中排行第1位。2007年進入[名人堂](../Page/名人堂.md "wikilink")「[Hollywood's
Rock
Walk](../Page/吉他中心#好萊塢搖滾大道.md "wikilink")」，成為目前唯一受此殊榮的亞洲藝人\[5\]\[6\]。

自1989年以來冠名為「[LIVE-GYM](../Page/LIVE-GYM.md "wikilink")」的演唱會（根據公演内容及規模亦會分為「LIVE-GYM
Pleasure」或「SHOWCASE」），迄今為止除了在日本國内從[LIVE
HOUSE到體育場至海外各式各樣的會場舉行以外](../Page/展演空間.md "wikilink")，亦會在各種音樂活動上出演。

### 組合名

#### 組合名由來

關於「B'z」的名稱，成員在訪談等處表示「無深刻含義{{\#tag:ref||group="原文"}}」，但有著「具現代感似記號般的類型為佳{{\#tag:ref||group="原文"}}」，以猶如企業標誌般[設計化的簡潔名稱為要求](../Page/設計.md "wikilink")\[7\]。迄今為止，B'z兩人對於名稱由來的說明並無一貫性，曖昧地說詞繁多，在2012年的訪談中松本表示「以往會適當作答{{\#tag:ref||group="原文"}}」。
迄今為止「以Z結尾是最初就決定好的，經過與工作人員商量的結果成為了『B'z』{{\#tag:ref||group="原文"}}\[8\]」、「想使用『Z』是因為該字母細微差異地擁有男性化的印象，與『B』結合後得到的結果為『B'z』{{\#tag:ref||group="原文"}}」、1995年使用的B'z標誌由來為「蜜蜂成群（Bee的複數型）便是『B'z』{{\#tag:ref||group="原文"}}」、使用[英語](../Page/英語.md "wikilink")[拉丁字母的首字母與尾字母](../Page/拉丁字母.md "wikilink")『A
to
Z』，包含了『A到Z的一切皆含於內』的意義，例舉出『A'z』（）作為候補\[9\]，但由於有被讀成「A'z≒[愛滋(AIDS)](../Page/後天免疫缺乏症候群.md "wikilink")」的可能性而檢討其他方案，結果以「A其後即是B{{\#tag:ref||group="原文"}}」為由成為了「B'z」。
此外，「因諸多強勁樂團的名稱皆以「B」為首，亦有『B'z感覺好像很容易記住』等意見而定奪{{\#tag:ref||group="原文"}}\[10\]」，其他亦有「『A'z』方案也有以『吉他Ace』與『主唱Ace』聯合為由來，但因發音難唸而成為了『B'z』{{\#tag:ref||group="原文"}}」等，除此之外，在2012年的訪談中，松本亦做過這樣的發言「從前還回答過『是[The
Beatles的](../Page/The_Beatles.md "wikilink")"B"，與[Led
Zeppelin的](../Page/Led_Zeppelin.md "wikilink")"z"』{{\#tag:ref||group="原文"}}」。

#### 唸法

關於「B'z」的發音，雖然在日本世間是以無抑揚頓挫的平板唸法「**B'z**」成為主流\[11\]，但成員們及關係者皆為在「B」放置重音，讀作『**B**'z』（）\[12\]。關於平板的發音方式被普遍化的理由，[NHK放送文化研究所講述了以下見解](../Page/NHK放送文化研究所.md "wikilink")，以「關於NHK節目內的發音，是由負責人做決定{{\#tag:ref||group="原文"}}」為開場白，「（[日語中](../Page/日語.md "wikilink")[外來語的](../Page/外來語.md "wikilink")）吉他（）、連續劇（）、電影（）、導演（）。這類單詞，曾經是將字首發為高音，但如今卻是以平音唸法為主流對吧。由於是以日語習性來組織言語，因此對於陌生單詞腦中會有習慣的重音位置，並且隨著言語脫口而出的次數增加，會有逐漸向後推移，化穩定為主的情況發生。因此將「B'z」發為平音，便是順其自然的結果吧{{\#tag:ref||group="原文"}}」\[13\]。

## 成員

參照官方網站的「BIOGRAPHY」\[14\]

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>職務</p></th>
<th><p>出生年月日</p></th>
<th><p>出生地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><ruby><rb><strong><a href="../Page/松本孝弘.md" title="wikilink">松本孝弘</a></strong></rb><rp>（</rp><rt>まつもと たかひろ</rt><rp>）</rp></ruby><br />
<em>TAK MATSUMOTO</em>[15]</p></td>
<td><p><a href="../Page/吉他.md" title="wikilink">吉他</a>、<a href="../Page/作曲.md" title="wikilink">作曲</a>、<a href="../Page/製作人.md" title="wikilink">製作人</a></p></td>
<td><p><a href="../Page/3月27日.md" title="wikilink">3月27日</a></p></td>
<td><p><a href="../Page/大阪府.md" title="wikilink">大阪府</a></p></td>
</tr>
<tr class="even">
<td><p><ruby><rb><strong><a href="../Page/稻葉浩志.md" title="wikilink">稻葉浩志</a></strong></rb><rp>（</rp><rt>いなば こうし</rt><rp>）</rp></ruby><br />
<em>KOSHI INABA</em>[16]</p></td>
<td><p><a href="../Page/主唱.md" title="wikilink">主唱</a>、<a href="../Page/作詞.md" title="wikilink">作詞</a></p></td>
<td><p><a href="../Page/9月23日.md" title="wikilink">9月23日</a></p></td>
<td><p><a href="../Page/岡山縣.md" title="wikilink">岡山縣</a></p></td>
</tr>
</tbody>
</table>

### 成為2人組的經過

起初，松本以由少數人結成樂團為計畫尋找成員，但狀況卻持續陷入未曾遇見令他滿意的人選\[17\]。在這種情況下，遇到了稻葉，並結成了2人組的[音樂組合](../Page/音樂組合.md "wikilink")「B'z」，但松本表示「為了擴展人聲與吉他極其模擬樂器的可能性，若可以的話，便以電腦及各種新器材進行同時對比與共存{{\#tag:ref||group="原文"}}\[18\]」、「由我寫曲、他（稻葉）寫詞，邊歌唱邊彈吉他，便足已創作出音樂，因此未必需要再加入其他成員{{\#tag:ref||group="原文"}}\[19\]」。
當時的日本國內音樂面，是所謂「
」的正盛之時，因此B'z出道亦顯得別具異彩。對此，松本表示「由於作為伴奏音樂家來到極限，因此僅只是認為到了該組織團體的時機而有所行動，和樂團熱潮無關{{\#tag:ref||group="原文"}}」。另一方面，關於演唱會，松本則表示「由於在演唱會上光憑電腦無法良好呈現，因此欲選擇聘請優秀的支援樂手{{\#tag:ref||group="原文"}}\[20\]」。

## 音樂性

### 音樂類型

起初具有將結合強勁吉他的音樂風格。接近[TM
NETWORK的音色](../Page/TM_NETWORK.md "wikilink")。當時對此松本表示「B'z的初期，確實承襲了（自己作為支援樂手參加的）TM的流派{{\#tag:ref||group="原文"}}」、「我認為始於TM而來的東西，亦成為我如今風格的一部分{{\#tag:ref||group="原文"}}」、「B'z初期受到小哲（[小室哲哉](../Page/小室哲哉.md "wikilink")）很大的影響{{\#tag:ref||group="原文"}}」，亦是來自於製作人[長戶大幸](../Page/長戶大幸.md "wikilink")「通過組成姿容類似於TM成員的樂團，來創建一個新的團體{{\#tag:ref||group="原文"}}」這樣具有概念的組合企劃\[21\]。

此外，松本與稻葉在喜愛的吉他手或歌手、今後欲嘗試的事物上亦會不謀而合，殘留部分使人起勁的8拍子[搖滾](../Page/搖滾.md "wikilink")、電腦編曲堅持製作出具有人性化的韻律\[22\]。之後，因交疊著錄音及巡演，發表出帶有強烈搖滾色彩的作品，納入支援樂手後更彰顯出樂團氣息。
並且，會在以硬式搖滾為根基的同時，將[原音調](../Page/原音樂.md "wikilink")、[爵士調](../Page/爵士樂.md "wikilink")、[放克調](../Page/放克.md "wikilink")、[雷鬼調](../Page/雷鬼樂.md "wikilink")、[藍調](../Page/藍調.md "wikilink")、[拉丁調](../Page/拉丁美洲音樂.md "wikilink")、[進行曲調](../Page/進行曲.md "wikilink")、[GS調](../Page/Group_Sounds.md "wikilink")、[昭和歌謠調等各式各樣的音樂類型融入作品](../Page/日本歌謠曲.md "wikilink")。

在日本國內所謂的「[J-POP](../Page/J-POP.md "wikilink")」之中，據說活動於邊以硬式搖滾為根基，邊狙擊排行榜上位的除了B'z以外，已絕無僅有\[23\]。

### 樂曲製作

B'z的歌曲[作詞由](../Page/作詞.md "wikilink")[稻葉浩志擔任](../Page/稻葉浩志.md "wikilink")、[作曲由](../Page/作曲.md "wikilink")[松本孝弘擔任](../Page/松本孝弘.md "wikilink")。B'z在結成當初對於歌曲便有「我們自己想辦法創作{{\#tag:ref||group="原文"}}」的構想，自然而然地成為由負責[吉他的松本作曲](../Page/吉他.md "wikilink")、負責[主唱的稻葉作詞](../Page/主唱.md "wikilink")。不過，由於2人皆是自B'z起才開始接觸正式作詞・作曲的創作活動，因此1st專輯『[B'z](../Page/B'z_\(1988年專輯\).md "wikilink")』中所收錄的「Nothing
to Change」作詞是、「孤独にDance in
vain」作詞是[大槻啓之](../Page/大槻啓之.md "wikilink")，松本表示「當時那已然是極限了{{\#tag:ref||group="原文"}}」。此外，由於稻葉對於書寫[日語歌詞及用日語歌唱會產生抵觸](../Page/日語.md "wikilink")，因此首次作詞時度過了苦思焦慮的生活\[24\]。

基本上是從松本創作出旋律或吉他連復段等片斷產物開始，之後在書寫歌詞，便是以所謂的「曲先」製作，不過亦會有應用庫存歌詞或旋律來製作的時候。松本會使用錄音機或手機錄下浮現於腦海的點子，若身旁沒有吉他便會純粹錄下哼唱\[25\]。稻葉亦會將靈光乍現的想法寫成筆記或使用手機錄音\[26\]。此外，稻葉亦有表示關於B'z歌曲的作詞，許多是經由松本演奏的旋律及音色喚起言詞。關於編曲，當初是由[明石昌夫單獨執行](../Page/明石昌夫.md "wikilink")，但在1st迷你專輯『[BAD
COMMUNICATION](../Page/BAD_COMMUNICATION.md "wikilink")』時，執行手法為先由松本與明石製作出基础，再由稻葉書寫歌詞\[27\]，在6th專輯『[RUN](../Page/RUN.md "wikilink")』製作途中，是與巡演支援樂手一同錄音，因此增添了不少樂團氣息\[28\]。稻葉表示，自己是從7th專輯『[The
7th
Blues](../Page/The_7th_Blues.md "wikilink")』錄音時起，對錄音室作業具體地產生興趣，自8th專輯『[LOOSE](../Page/LOOSE_\(B'z專輯\).md "wikilink")』錄音起稻葉開始參加製作，在16th單曲「[ねがい](../Page/心願_\(B'z單曲\).md "wikilink")」稻葉首次並列編曲製作人員名單。此外，B'z兩人開始將支援樂手、導演等，對於編曲的意見、想到的主意具現化為音符，除此之外，亦有將[DEMO交給複數編曲家](../Page/DEMO.md "wikilink")，並採用優秀編曲的情況。

錄音是將松本的吉他獨奏或稻葉的歌聲進行個別錄製，松本在關於歌詞的部分，多為稻葉的歌聲加入後在首次聽聞。松本的吉他獨奏錄音，是從數種彈法中定奪樂句，亦有表示將幾種樂句混合彈奏的情況居多。錄音地點大多是在[洛杉磯與](../Page/洛杉磯.md "wikilink")[東京進行](../Page/東京.md "wikilink")，關於製作等錄音室作業，除了洛杉磯及東京以外，亦會在[紐約](../Page/紐約.md "wikilink")、[夏威夷](../Page/夏威夷.md "wikilink")、[沖繩](../Page/沖繩縣.md "wikilink")、[福岡](../Page/福岡市.md "wikilink")、[大阪等地進行](../Page/大阪.md "wikilink")\[29\]。此外，曾經亦有過為了學習美國的製作流程或作業系統而前往洛杉磯，迎接製作人Andy
Johns進行錄音，作為20th單曲「[Real Thing
Shakes](../Page/Real_Thing_Shakes.md "wikilink")」發售等，啟用外部製作人來製作歌曲的情況\[30\]。

### B+U+M

「B+U+M」是曾經的B'z的音樂製作集團，名稱由來為「B'z Unreal
Music」的首字母。雖然B'z是由[吉他手](../Page/吉他手.md "wikilink")[松本孝弘與](../Page/松本孝弘.md "wikilink")[主唱](../Page/主唱.md "wikilink")[稻葉浩志所構成](../Page/稻葉浩志.md "wikilink")，但為了「將光憑我們自己無法實現的[音樂實現出來](../Page/音樂.md "wikilink"){{\#tag:ref||group="原文"}}」為目的結成了「B+U+M」，主要以B'z歌曲的製作及演唱會支援樂手等進行活動。在1990年發售的3rd專輯『[BREAK
THROUGH](../Page/BREAK_THROUGH.md "wikilink")』中所收錄的「B.U.M」一曲，有使用「We're
B'z & Funky Crew」來暗示「B+U+M」的歌詞，在同年發售的4th單曲「[BE
THERE](../Page/BE_THERE.md "wikilink")」該集團首次列入製作人員名單。此外，在1990年11月16日作為B'z的經紀公司成立了「B.U.M」（2001年成立「[VERMILLION
RECORDS](../Page/VERMILLION_RECORDS.md "wikilink")」繼承了「B.U.M」）。之後，為了有效活用「B'z是2個人{{\#tag:ref||group="原文"}}」的優點，在1994年發售的15th單曲「[MOTEL](../Page/MOTEL.md "wikilink")」製作完成後，最終解散了「B+U+M」。

<table>
<caption>B+U+M成員</caption>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>職務</p></th>
<th><p>備考</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/松本孝弘.md" title="wikilink">松本孝弘</a></p></td>
<td><p><a href="../Page/吉他.md" title="wikilink">吉他</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/稻葉浩志.md" title="wikilink">稻葉浩志</a></p></td>
<td><p><a href="../Page/主唱.md" title="wikilink">主唱</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/明石昌夫.md" title="wikilink">明石昌夫</a></p></td>
<td><p>、<a href="../Page/貝斯.md" title="wikilink">貝斯</a></p></td>
<td><p>演奏貝斯是自1992年發售的11th單曲「<a href="../Page/ZERO_(B&#39;z單曲).md" title="wikilink">ZERO</a>」起[31][32]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/音訊工程師.md" title="wikilink">音訊工程師</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/寺島良一.md" title="wikilink">寺島良一</a></p></td>
<td><p><a href="../Page/Director.md" title="wikilink">音樂總監</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/爵士鼓.md" title="wikilink">爵士鼓</a></p></td>
<td><p>自1992年發售的11th單曲「<a href="../Page/ZERO_(B&#39;z單曲).md" title="wikilink">ZERO</a>」起至1993年發售的12th單曲「<a href="../Page/愛のままにわがままに_僕は君だけを傷つけない.md" title="wikilink">愛のままにわがままに 僕は君だけを傷つけない</a>」止</p></td>
</tr>
<tr class="odd">
<td><p>畠山勝紀</p></td>
<td><p>吉他技師</p></td>
<td><p>自1992年發售的11th單曲「ZERO」起</p></td>
</tr>
</tbody>
</table>

## 歷程

### 1988年（相遇至出道）

隸屬於音樂製作公司[Being](../Page/Being_\(公司\).md "wikilink")，作為、[TM
NETWORK等藝人的](../Page/TM_NETWORK.md "wikilink")或活動的[吉他手](../Page/吉他手.md "wikilink")[松本孝弘](../Page/松本孝弘.md "wikilink")，為了實現其構想「創建一個可以表現自身音樂的[樂團](../Page/樂團.md "wikilink")
{{\#tag:ref||group="原文"}}」，開始朝向結成樂團為目標進行活動。雖然亦歷經了將與某[歌手組成樂團公布在音樂雜誌上的時期](../Page/歌手.md "wikilink")，但最終並未與該歌手出道\[33\]。

1988年5月，尋找歌手的松本，由來自Being社長兼[音樂製作人的](../Page/音樂製作人.md "wikilink")[長戶大幸轉交了](../Page/長戶大幸.md "wikilink")1捲[DEMO](../Page/樣本唱片.md "wikilink")。該DEMO是隸屬於當時「Being音樂振興會」（後更名為[Being
Music
School](../Page/Being_Music_School.md "wikilink")）的[稻葉浩志](../Page/稻葉浩志.md "wikilink")，錄下自身翻唱的的「T-BORN
SHUFFLE」、[Led Zeppelin的](../Page/Led_Zeppelin.md "wikilink")「YOU SHOOK
ME」、[Billy
Joel的](../Page/Billy_Joel.md "wikilink")「Honesty」。當時長戶所抱心意為「稻葉想做點什麼，迫切早日出道{{\#tag:ref||group="原文"}}」\[34\]。聽了稻葉歌聲的松本便決定採用稻葉作為主唱，於是通過長戶，2人首次見面。日後松本回顧關於當時，表示「由於DEMO也聽了、照片也看了，因此（在與稻葉見面之前）自身心意已決／自身已下定決心了{{\#tag:ref||group="原文"}}」、「之後便拜託『好人』會見{{\#tag:ref||group="原文"}}」\[35\]。
隔日，在當時位於六本木的[錄音室](../Page/錄音室.md "wikilink")「SOUND JOKER」即興共演了[The
Beatles的](../Page/The_Beatles.md "wikilink")「[Let It
Be](../Page/Let_It_Be.md "wikilink")」、「[Oh\!
Darling](../Page/Oh!_Darling.md "wikilink")」\[36\]。然而，由於器材故障導致當日並未共演完2曲，但根據松本表示「大約10分鐘就決定好了{{\#tag:ref||group="原文"}}」。於是，作為組合名「B'z」在1988年9月21日以同時發售單曲「[だからその手を離して](../Page/だからその手を離して.md "wikilink")」與專輯『[B'z](../Page/B'z_\(1988年專輯\).md "wikilink")』出道。專輯[廣告標語為](../Page/廣告標語.md "wikilink")「自最頂端開始加速」（）。初次見面直至出道僅約短短4個月間，因此是在任憑尚未建立起信賴關係或友誼，便直接邁向出道活動，對於稻葉而言，表示「迄今為止，未曾被松本先生說過『一起來組樂團吧』{{\#tag:ref||group="原文"}}」。
對於松本而言，當時的唱片公司具有「發售到3張專輯前必須走紅{{\#tag:ref||group="原文"}}」的方針，B'z打從一開始就有「不暢銷便沒有意義{{\#tag:ref||group="原文"}}」的高度意識，思索著「如何得以暢銷？{{\#tag:ref||group="原文"}}」。稻葉表示，具有「以『ギターブック/Gb』（當時最暢銷的音樂雜誌）封面為目標{{\#tag:ref||group="原文"}}」、「要在3年內登上（週刊排行榜誌）『[Oricon](../Page/Oricon.md "wikilink")』（刊登第1位
-
第50位為止）左頁{{\#tag:ref||group="原文"}}」這樣的B'z「3年計畫」\[37\]。此外，初期製作人是[中島正雄](../Page/中島正雄.md "wikilink")，在松本成為總體製作人後，中島的信用名義改為了「[監察人](../Page/監察人.md "wikilink")」。

### 1988年（出道後） - 1993年

出道第1年之戰略為通過製作音源並發行來維繫話題，當時的松本有著此般願景「在拿出2張專輯前絕不辦巡演{{\#tag:ref||group="原文"}}」「不是辦在LIVE
HOUSE，起初就要從音樂廳開始發展{{\#tag:ref||group="原文"}}」。為此松本任職了[YAMAHA的評論員](../Page/山葉公司.md "wikilink")，舉辦了「吉他研討會」，在研討會後與同行的稻葉一同披露B'z的活動等，巡迴日本全國各地做宣傳活動。當時，由於松本仍剩有參加TM
NETWORK的行程，因此亦有在音樂會前後舉辦研討會的情況發生。

B'z首次在觀眾面前披露現場表演，是1989年在TM NETWORK及[FENCE OF
DEFENSE的音樂活動上作為開場表演出演時](../Page/FENCE_OF_DEFENSE.md "wikilink")，松本陳述首次邊彈吉他，邊在身旁-{注視}-著身處於演唱會上歌唱的稻葉時，便「感到『這絕對可行』{{\#tag:ref||group="原文"}}」。2nd專輯『[OFF
THE LOCK](../Page/OFF_THE_LOCK.md "wikilink")』發售後，舉辦了首次獨立演唱會『B'z
LIVE-GYM \#00 "OFF THE LOCK"』在東名阪的3個場地。3rd專輯發售前以實驗性質發表的1st迷你專輯『[BAD
COMMUNICATION](../Page/BAD_COMMUNICATION.md "wikilink")』長期暢銷保持中\[38\]，在1990年發售的3rd專輯『[BREAK
THROUGH](../Page/BREAK_THROUGH.md "wikilink")』獲得[Oricon公信榜初登場第](../Page/Oricon公信榜.md "wikilink")3位，實現進入TOP
10。

雖然迄今為止皆是專輯與單曲同時發售，但即便4th單曲「[BE
THERE](../Page/BE_THERE.md "wikilink")」是單獨發售單曲，亦實現進入Oricon公信榜TOP
10。再者，自本張單曲起，除了收錄曲開始以「1st beat」、「2nd
beat」作為表記以外，將音樂製作集團「B+U+M」列入了製作人員名單。接著5th單曲「[太陽のKomachi
Angel](../Page/太陽のKomachi_Angel.md "wikilink")」首次獲得Oricon公信榜第1位\[39\]、4th專輯『[RISKY](../Page/RISKY_\(專輯\).md "wikilink")』首次獲得專輯榜第1位，達成百萬銷量。這年發表了5張單曲、2張原創專輯、1張迷你專輯、1張影像作品，每個月奪得各家音樂雜誌篇幅，亦成功登上封面\[40\]。1991年發售的8th單曲「[LADY
NAVIGATION](../Page/LADY_NAVIGATION.md "wikilink")」是單曲首次達成百萬銷量（並且，自「LADY
NAVIGATION」起至1996年發售的20th單曲「[Real Thing
Shakes](../Page/Real_Thing_Shakes.md "wikilink")」止，是13張作品連續獲得百萬銷量）。此外，1991年是從迄今為止發表過的樂曲中，挑選出人氣曲等曲目演奏，舉辦了『B'z
LIVE-GYM "Pleasure '91"』的一年，此後Pleasure系列化。5th專輯『[IN THE
LIFE](../Page/IN_THE_LIFE.md "wikilink")』發售後，隔年舉辦了『B'z LIVE-GYM '91-'92
"IN THE LIFE"』。

1992年的『B'z LIVE-GYM Pleasure '92 "TIME"』是在競技場等級的會場，帶入名為「STAR
FISH」的可動式照明桁架，於觀眾席360度全方位設置\[41\]。這年發售了10th單曲「[BLOWIN'](../Page/BLOWIN'.md "wikilink")」與11th單曲「[ZERO](../Page/ZERO_\(B'z單曲\).md "wikilink")」、6th專輯『[RUN](../Page/RUN.md "wikilink")』與4th迷你專輯『[FRIENDS](../Page/FRIENDS_\(B'z專輯\).md "wikilink")』。1993年舉辦了『B'z
LIVE-GYM '93 "RUN"』，在巡演中發售了12th單曲「[愛のままにわがままに
僕は君だけを傷つけない](../Page/愛のままにわがままに_僕は君だけを傷つけない.md "wikilink")」與13th單曲「[裸足の女神](../Page/裸足の女神.md "wikilink")」。夏季舉辦了B'z的首次野外演唱會『B'z
LIVE-GYM Pleasure '93 "JAP THE RIPPER"』，於2日間在（静岡縣弁天島海濱公園）。

### 1994年 - 1998年

1994年，自14th單曲「[Don't Leave
Me](../Page/Don't_Leave_Me.md "wikilink")」發售日起，舉辦了『B'z
LIVE-GYM '94 "The 9th Blues"』，1整個年度舉行了87場演唱會。此外，在首次推出2張一組的7th專輯『[The 7th
Blues](../Page/The_7th_Blues.md "wikilink")』發售日，於日本全國31家報紙實施了音樂界首次共30面全彩2頁的廣告開發。15th單曲「[MOTEL](../Page/MOTEL.md "wikilink")」發售後，最終解體了製作團隊「B+U+M」。

1995年，發售16th單曲「[ねがい](../Page/心願_\(B'z單曲\).md "wikilink")」，自17th單曲「[love
me, I love you](../Page/love_me,_I_love_you.md "wikilink")」發售日起舉行的『B'z
LIVE-GYM Pleasure '95 "BUZZ\!\!"』，以體育場等級的會場為中心舉辦。在巡演上作為未發表曲披露的「[LOVE
PHANTOM](../Page/LOVE_PHANTOM.md "wikilink")」作為18th單曲發售。以「B'z是2個人{{\#tag:ref||group="原文"}}」返回原點製作的8th專輯『[LOOSE](../Page/LOOSE_\(B'z專輯\).md "wikilink")』總銷量高達300萬張。1996年19th單曲「[ミエナイチカラ
〜INVISIBLE
ONE〜/MOVE](../Page/看不見的力量_～INVISIBLE_ONE～/MOVE.md "wikilink")」發售後，舉辦『B'z
LIVE-GYM '96 "Spirit LOOSE"』。在巡演上披露的全篇英語歌詞的「[Real Thing
Shakes](../Page/Real_Thing_Shakes.md "wikilink")」作為20th單曲發售。

1997年，21st單曲「[FIREBALL](../Page/FIREBALL.md "wikilink")」發售後舉辦『B'z
LIVE-GYM Pleasure '97
"FIREBALL"』，成為了內含[名古屋巨蛋](../Page/名古屋巨蛋.md "wikilink")
\[42\]公演的首次巨蛋巡演。在巡演上作為未發表曲披露的「[Calling](../Page/Calling_\(B'z單曲\).md "wikilink")」經過大幅度編曲改編後作為22th單曲發售。9th專輯『[SURVIVE](../Page/SURVIVE.md "wikilink")』發售後，自1998年起舉辦了『B'z
LIVE-GYM '98
"SURVIVE"』。在出道10週年這年所發售的首張官方[精選輯](../Page/精選輯.md "wikilink")『[B'z
The Best
"Pleasure"](../Page/B'z_The_Best_"Pleasure".md "wikilink")』，更新了當時的首週總銷量獲得270萬張紀錄，並隨著總銷量的增長，創下了日本國內音樂史上首次總銷量突破500萬張。此外，以根據封入『Pleasure』的[明信片點播決定收錄曲的精選輯](../Page/明信片.md "wikilink")『[B'z
The Best
"Treasure"](../Page/B'z_The_Best_"Treasure".md "wikilink")』總銷量亦超過400萬張，2作品合計約創下1,000萬張總銷量紀錄\[43\]。

### 1999年 - 2003年

1999年，根據去年發售的精選輯銷售量等，獲得了『[日本金唱片大獎](../Page/日本金唱片大獎.md "wikilink")』以「年度最佳藝人」（*Artist
of the Year*）為首的各類獎項\[44\]、『[世界音樂獎](../Page/世界音樂獎.md "wikilink")』（*The
World Music Awards*）的「World Selling Japanese Artist of The
Year」。經過SOLO活動後發售26th單曲「[ギリギリchop](../Page/ギリギリchop.md "wikilink")」，在10th專輯『[Brotherhood](../Page/Brotherhood_\(B'z專輯\).md "wikilink")』收錄的「ギリギリchop（Version
51）」中參加了來自[Mr. Big的](../Page/Mr._Big.md "wikilink")[Billy
Sheehan與](../Page/Billy_Sheehan.md "wikilink")[Pat
Torpey](../Page/w:_Pat_Torpey.md "wikilink")\[45\]。此外，參加自從前便認識的[Steve
Vai的錄音](../Page/Steve_Vai.md "wikilink")，製作了『[The Ultra
Zone](../Page/w:_The_Ultra_Zone.md "wikilink")』中所收錄的「ASIAN SKY」。『B'z
LIVE-GYM '99
"Brotherhood"』的[横濱公演成為](../Page/横濱市.md "wikilink")[横濱國際綜合競技場首次使用於舉辦音樂會](../Page/横濱國際綜合競技場.md "wikilink")\[46\]。

2000年，27th單曲「[今夜月の見える丘に](../Page/今夜月の見える丘に.md "wikilink")」，作為Mast
Album（*マストアルバム*）『[B'z The
"Mixture"](../Page/B'z_The_"Mixture".md "wikilink")』發售。在[日本放送的廣播節目](../Page/日本放送.md "wikilink")『B'zの』出演時，網路直播的同時觀看人數高達14,600人，更新了當時的日本紀錄
\[47\]。29th單曲「[juice](../Page/juice.md "wikilink")」的[Music
Video是在](../Page/Music_Video.md "wikilink")[札幌以](../Page/札幌市.md "wikilink")「Guerrilla
Live」形式拍攝而成。『B'z LIVE-GYM Pleasure 2000
"juice"』8月9日在[千葉海洋球場的演唱會](../Page/千葉海洋球場.md "wikilink")，因暴雨與落雷，造成急遽將內容縮短後結束的事態\[48\]。同年發售了11th專輯『[ELEVEN](../Page/ELEVEN.md "wikilink")』。

2001年，31st單曲「[ultra soul](../Page/ultra_soul.md "wikilink")」發售後舉辦了『B'z
LIVE-GYM 2001
"ELEVEN"』，在首次舉辦音樂會的[札幌巨蛋拍攝了](../Page/札幌巨蛋.md "wikilink")32nd單曲「[GOLD](../Page/GOLD_\(B'z單曲\).md "wikilink")」的Music
Video。此外，這年在[臺北與](../Page/臺北市.md "wikilink")[香港舉辦了B](../Page/香港.md "wikilink")'z的首次海外公演。2002年，在伴隨著舉辦『[2002年國際足總世界盃](../Page/2002年國際足總世界盃.md "wikilink")』所發售的[合輯](../Page/合輯.md "wikilink")『[2002
FIFA World Cup Official
Album](../Page/w:_The_Official_Album_of_the_2002_FIFA_World_Cup.md "wikilink")』Songs
of
KOREA/JAPAN盤，以「DEVIL」參加，並在[東京體育場舉辦的國際足球聯盟](../Page/東京體育場.md "wikilink")（*FIFA*）首次出演官方音樂會『2002
FIFA World Cup KOREA/JAPAN Official Concert International
Day』。在世界盃與[Aerosmith一起披露了](../Page/史密斯飛船.md "wikilink")「Train
Kept A
Rollin」。此外，12th專輯『[GREEN](../Page/GREEN_\(B'z專輯\).md "wikilink")』發售後除了舉辦了『B'z
LIVE-GYM 2002 "GREEN
〜GO★FIGHT★WIN〜"』以外，在[聖地牙哥與](../Page/聖地牙哥.md "wikilink")[洛杉磯舉辦了首次](../Page/洛杉磯.md "wikilink")[美國公演](../Page/美國.md "wikilink")『B'z
LIVE-GYM 2002 "Rock n' California Roll"』。並且，這年的支援樂手成員，[Billy
Sheehan參加了](../Page/Billy_Sheehan.md "wikilink")[貝斯](../Page/貝斯.md "wikilink")。冬季發售了首張抒情精選輯『[The
Ballads 〜Love & B'z〜](../Page/The_Ballads_〜Love_&_B'z〜.md "wikilink")』。

2003年，在34th單曲「[IT'S
SHOWTIME\!\!](../Page/IT'S_SHOWTIME!!.md "wikilink")」發售的同時，將自「[BE
THERE](../Page/BE_THERE.md "wikilink")」起至「[裸足の女神](../Page/裸足の女神.md "wikilink")」為止的單曲10作品Maxi
Single化重新發售，在Oricon公信榜週間單曲榜TOP 10內上榜了9作品 。『B'z LIVE-GYM The Final
Pleasure "IT'S
SHOWTIME\!\!"』最終公演是睽違10年舉辦在（静岡縣弁天島海濱公園），迎來出道15週年。之後，繼去年的海外公演，在美國與[加拿大西岸](../Page/加拿大.md "wikilink")5座都市舉辦了『B'z
LIVE-GYM 2003 "BANZAI IN NORTH AMERICA"』，在日本國內則是舉辦了包含5大巨蛋的巡演『B'z
LIVE-GYM 2003 "BIG MACHINE"』。

### 2004年 - 2008年

2004年雖以SOLO活動為主，但亦發售了36th單曲「[BANZAI](../Page/BANZAI_\(B'z單曲\).md "wikilink")」、37th單曲「[ARIGATO](../Page/ARIGATO.md "wikilink")」。

2005年在38th單曲「[愛のバクダン](../Page/愛のバクダン.md "wikilink")」、14th專輯『[THE
CIRCLE](../Page/THE_CIRCLE.md "wikilink")』發售後舉辦了『B'z LIVE-GYM 2005
"CIRCLE OF
ROCK"』，成為B'z首次在競技場公演採用圓形舞台。39th單曲「[OCEAN](../Page/OCEAN_\(B'z單曲\).md "wikilink")」的Music
Video成為史上首次使用[海上保安廳的](../Page/海上保安廳.md "wikilink")（[瑞穗級](../Page/瑞穗級巡視船.md "wikilink")）拍攝\[49\]。根據蘋果電腦（現今的[蘋果公司](../Page/蘋果公司.md "wikilink")）在日本國內iTunes
Music Store（現今的[iTunes
Store](../Page/iTunes_Store.md "wikilink")）服務開始以來，伴隨自樂曲上架開始（部分樂曲除外）\[50\]，發行了限定數位上架的[Boxset](../Page/Boxset.md "wikilink")『』（現已停止上架）\[51\]。此外，同年發售的精選輯『[B'z
The Best "Pleasure
II"](../Page/B'z_The_Best_"Pleasure_II".md "wikilink")』，從[iTunes可下載到](../Page/iTunes.md "wikilink")1首作為購入特典首次公開的演唱會影像\[52\]。

2006年15th專輯『[MONSTER](../Page/MONSTER_\(B'z專輯\).md "wikilink")』發售後，舉辦了包含5大巨蛋的巡演『B'z
LIVE-GYM 2006 "MONSTER'S GARAGE"』。此外，在演唱會影像發佈企業的「Network
LIVE」（當時名稱），作為首位日本藝人參加\[53\]，為了收錄影像舉行了演唱會。

2007年作為B'z首次在演出，出演了『[SUMMER
SONIC](../Page/SUMMER_SONIC.md "wikilink")』。此外，在迎來出道第20年當下，成為首個進入名人堂「[Hollywood's
Rock
Walk](../Page/吉他中心#好萊塢搖滾大道.md "wikilink")」的亞洲圈音樂家，在[好萊塢](../Page/好萊塢.md "wikilink")[吉他中心舉行了紀念典禮](../Page/吉他中心.md "wikilink")。16th專輯『[ACTION](../Page/ACTION.md "wikilink")』發售，2008年舉辦了『B'z
LIVE-GYM 2008 "ACTION"』。此外，發售了精選輯『[B'z The Best "ULTRA
Pleasure"](../Page/B'z_The_Best_"ULTRA_Pleasure".md "wikilink")』與根據來自[網際網路點播決定收錄曲的精選輯](../Page/網際網路.md "wikilink")『[B'z
The Best "ULTRA
Treasure"](../Page/B'z_The_Best_"ULTRA_Treasure".md "wikilink")』\[54\]\[55\]，睽違5年舉辦了Pleasure系列『B'z
LIVE-GYM Pleasure 2008 -GLORY DAYS-』。

### 2009年 - 2013年

2009年，發售46th單曲「[イチブトゼンブ/DIVE](../Page/イチブトゼンブ/DIVE.md "wikilink")」。[Chad
Smith參加了](../Page/w:_Chad_Smith.md "wikilink")[爵士鼓](../Page/爵士鼓.md "wikilink")\[56\]\[57\]。在『B'z
SHOWCASE 2009 -B'z In Your Town-』舉行後出演了『SUMMER
SONIC』。在[端島](../Page/端島.md "wikilink")（通稱「軍艦島」）拍攝了47th單曲「[MY
LONELY TOWN](../Page/MY_LONELY_TOWN.md "wikilink")」的Music Video
（唱片封面照）\[58\]。17th專輯『[MAGIC](../Page/MAGIC_\(B'z專輯\).md "wikilink")』發售，隔年舉辦了『B'z
LIVE-GYM 2010 "Ain't No Magic"』。巡演後表面活動以SOLO為主。

2011年，被起用為「Pepsi
NEX」[TVCM出場人物](../Page/電視廣告.md "wikilink")，成為B'z首次出演[CM](../Page/廣告.md "wikilink")\[59\]。CM使用了「[さよなら傷だらけの日々よ](../Page/さよなら傷だらけの日々よ.md "wikilink")」、夏季使用18th專輯『[C'mon](../Page/C'mon.md "wikilink")』標題曲「C'mon」\[60\]、冬季使用「[いつかのメリークリスマス](../Page/いつかのメリークリスマス.md "wikilink")」\[61\]。伴隨著發生3月11日的「[東日本大震災](../Page/東日本大震災.md "wikilink")」，為了支援受災者，參加來自[Linkin
Park設立的](../Page/Linkin_Park.md "wikilink")「Music For
Relief」所[數位發行的合輯](../Page/數位發行.md "wikilink")『[Download
to Donate: Tsunami
Relief](../Page/w:_Download_to_Donate:_Tsunami_Relief.md "wikilink")』，提供了「Home」\[62\]。此外，睽違8年在加拿大與美國的3座都市舉辦海外公演『B'z
LIVE-GYM 2011 -long tome no see-』後，出演了「Music For
Relief」為了支援受災者在[洛杉磯所舉行的秘密演唱會](../Page/洛杉磯.md "wikilink")『Music
for Relief - Secret Show for Japan』\[63\]。『B'z LIVE-GYM 2011
-C'mon-』最初舉辦地點的宮城公演，收益全額捐贈於復興支援，所有公演開始販售慈善周邊\[64\]。

2012年除了接續出演「Pepsi NEX」的CM，CM使用了「[GO FOR IT, BABY
-キオクの山脈-](../Page/GO_FOR_IT,_BABY_-記憶的山脈-.md "wikilink")」\[65\]以外，夏季限定數位上架的專輯『[B'z](../Page/B'z_\(2012年專輯\).md "wikilink")』，使用了來自將38th單曲「[愛のバクダン](../Page/愛のバクダン.md "wikilink")」改為英語歌詞並重新錄製的「Love
Bomb」\[66\]。此外，除了將成為[卡普空遊戲軟體](../Page/卡普空.md "wikilink")『[Dragon's
Dogma](../Page/Dragon's_Dogma.md "wikilink")』主題歌的24th單曲「[さまよえる蒼い弾丸](../Page/さまよえる蒼い弾丸.md "wikilink")」改為英語歌詞並重新錄製的「Into
Free
-Dangan-」，作為限定數位上架單曲發行了以外\[67\]，[金賢重成為作為B](../Page/金賢重.md "wikilink")'z首次提供樂曲，全新創作了「[HEAT](../Page/HEAT_\(金賢重單曲\).md "wikilink")」\[68\]。在美國與加拿大的7座都市舉辦了『B'z
LIVE-GYM 2012 -Into Free-』，作為EXTRA公演亦有在日本國內4座都市舉辦。

在2013年的出道25週年，將收錄有迄今為止所發表的50張單曲歌曲的精選輯『[B'z The Best XXV
1988-1998](../Page/B'z_The_Best_XXV_1988-1998.md "wikilink")』與『[B'z The
Best XXV
1999-2012](../Page/B'z_The_Best_XXV_1999-2012.md "wikilink")』同時發售\[69\]，並舉辦了『B'z
LIVE-GYM Pleasure 2013 -ENDLESS
SUMMER-』\[70\]\[71\]。此外，在QVC海洋球場舉辦的『AEROSONIC』是睽違11年與[Aerosmith共演](../Page/史密斯飛船.md "wikilink")，一起披露了「Mama
Kin」\[72\]\[73\]。

### 2014年 - 2018年

2014年以SOLO活動為主，作為B'z是自年末起至2015年年初開始出演複數音樂節目\[74\]\[75\]\[76\]\[77\]，披露了51st單曲「[有頂天](../Page/有頂天.md "wikilink")」。19th專輯『[EPIC
DAY](../Page/EPIC_DAY.md "wikilink")』發售後舉辦了『B'z LIVE-GYM 2015 -EPIC
NIGHT-』，除了在日本全國的會場舉行了追加公演以外，並出演了大阪心斎橋的MUSIC
BAR「ROCKROCK」開幕20週年紀念演唱會活動，與[LOUDNESS共演](../Page/響度樂團.md "wikilink")\[78\]。此外，為了當時從[紐約洋基復歸老巢](../Page/紐約洋基.md "wikilink")[廣島東洋鯉魚的](../Page/廣島東洋鯉魚.md "wikilink")[黑田博樹](../Page/黑田博樹.md "wikilink")，全新創作了[MAZDA
Zoom-Zoom
廣島球場](../Page/馬自達Zoom-Zoom廣島球場.md "wikilink")（馬自達球場）登場曲「[RED](../Page/RED.md "wikilink")」\[79\]。

2016年雖以SOLO活動為主，但發行了限定數位上架單曲「[世界はあなたの色になる](../Page/世界はあなたの色になる.md "wikilink")」、「」。2017年，稻葉與[Stevie
Salas](../Page/w:_Stevie_Salas.md "wikilink")，松本與Daniel
Ho[合作](../Page/合作.md "wikilink")，分別發售專輯並舉辦了巡演。之後，作為B'z發售53rd單曲「[声明/Still
Alive](../Page/声明/Still_Alive.md "wikilink")」，舉辦了睽違8年的In Your Town系列『B'z
SHOWCASE 2017 -B'z In Your Town-』。此外，首次出演了『[ROCK IN JAPAN
FESTIVAL](../Page/ROCK_IN_JAPAN_FESTIVAL.md "wikilink")』與『』。作為實體發行，B'z首次發表發售Boxset『[B'z
COMPLETE SINGLE
BOX](../Page/B'z_COMPLETE_SINGLE_BOX.md "wikilink")』，與[7-Eleven合作販售了完全預約接單生産的](../Page/7-Eleven.md "wikilink")【Trailer
Edition】，以及展開了「B'z×7-Eleven商品博覽會」\[80\]。 作為出道30週年活動的一環，舉辦了『[B'z
Loud-Gym](../Page/B'z_Loud-Gym.md "wikilink")』在47[都道府縣的](../Page/日本行政區劃.md "wikilink")[LIVE
HOUSE](../Page/展演空間.md "wikilink")，每月1場至隔年9月為止\[81\]。自20th專輯『[DINOSAUR](../Page/DINOSAUR.md "wikilink")』發售後，隔年舉辦了『B'z
LIVE-GYM 2017-2018 "LIVE
DINOSAUR"』。此外，除了舉辦紀念出道30週年的首次大型[展覽會](../Page/展覽會.md "wikilink")，分別為『B'z
30th Year Exhibition "SCENES"
1988-2018』前期（1988-2002）與後期（2003-2018）\[82\]\[83\]，並將在會內上映的影像於日本全國電影院順次公開以外\[84\]，亦舉辦了睽違5年的Pleasure系列『[B'z
LIVE-GYM Pleasure 2018
-HINOTORI-](../Page/B'z_LIVE-GYM_Pleasure_2018_-HINOTORI-.md "wikilink")』\[85\]\[86\]。

## 評價

，以由於不是一位狂熱的聽眾，因此僅能給出印象論為開場白，分析出「也許是通過硬式搖滾的聲響與吶喊、稻葉好看的外表等，B'z以一種容易理解的方式，具體表現出日本一般人所估計的搖滾形象{{\#tag:ref||group="原文"}}\[87\]」。[音樂製作人](../Page/音樂製作人.md "wikilink")[龜田誠治](../Page/龜田誠治.md "wikilink")，將作為熱門樂曲必要條件的「藝人力」（）與「樂曲力」（）兩者兼備便會穩定持續暢銷的「穩定型獲勝組」（）例子例舉出B'z\[88\]，並陳述唱片行亦會積極出售，總銷量隨之增長\[89\]。[作詞家](../Page/作詞家.md "wikilink")在關於如何以日語接近西洋音樂的[節拍](../Page/拍_\(音樂\).md "wikilink")，這樣「日語搖滾」的反覆試驗中表示，「若讓稻葉君來唱，無論什麼都會變得搖滾。他的歌詞，即便加入生活在狹小房間中沒志氣的男人所說的話語，只要在B'z的旋律中讓他來唱，就會變得搖滾{{\#tag:ref||group="原文"}}」、「作為歌詞的世界觀，即便是吐露著懦弱、小氣的心情，只要配上松本君的吉他，置身於B'z的世界中聆聽，便一點都不覺狹隘。令我感嘆那即是搖滾吧{{\#tag:ref||group="原文"}}」。[吉他手](../Page/吉他手.md "wikilink")[Marty
Friedman陳述](../Page/Marty_Friedman.md "wikilink")，「B'z明明擁有那種程度的人氣，卻無人隨之效仿對吧。我認為是由於技藝精湛，並且2人都確立了僅只自己獨一無二的風格，即便想模仿也模仿不了{{\#tag:ref||group="原文"}}」\[90\]。音樂評論家陳述，「B'z迄今為止打破了不少[邦樂與洋樂之間的壁壘](../Page/邦樂.md "wikilink"){{\#tag:ref||group="原文"}}」、「聽了B'z的音樂後，會變得無論如何也想再聽那種音樂。這就是為何（邦樂與洋樂之間）類似境界線般的東西正在崩壞。現在回想起來便覺得這是一件了不起的事{{\#tag:ref||group="原文"}}」、「（在關於松本先生或稻葉先生所受影響的海外藝人或專輯的軼事話題中）我想也可以說是你們正在擴大B'z粉絲的音樂基礎吧{{\#tag:ref||group="原文"}}」\[91\]。

B'z的2人，在關於總銷量或演唱會動員數等話題上，松本陳述「銷售量完全沒有成為壓力{{\#tag:ref||group="原文"}}」、「比方說，以"數字"來表示是世上最容易理解的方式。例如賣出了幾萬張、賺了多少錢等。雖然那樣也沒什麼不好，但在我心中是對類似於品質方面的追求永無止盡{{\#tag:ref||group="原文"}}」，稻葉陳述「若他們是將其作為1個有趣的話題……我認為有人關心數字，並不是什麼壞事{{\#tag:ref||group="原文"}}」。此外，稻葉陳述「比起紀錄與目標，如今，已稍有不同…例如，想嘗試到未曾去舉辦過演唱會的地方表演，那種事會令我十分愉快{{\#tag:ref||group="原文"}}\[92\]」，松本亦陳述「看來，創作新專輯、經歷誕生出新的事物等、想去到未曾謀面的場所舉辦演唱會等，這些事是我目前最大的樂趣{{\#tag:ref||group="原文"}}\[93\]」。

### 進入名人堂「Hollywood's Rock Walk」

在[2007年](../Page/2007年.md "wikilink")[9月21日突入結成](../Page/9月21日.md "wikilink")20週年的B'z，進入名人堂「[Hollywood's
Rock Walk](../Page/吉他中心#好萊塢搖滾大道.md "wikilink")」。

好萊塢搖滾大道（*Hollywood's Rock
Walk*）是作為表彰搖滾藝術，再者對音樂發展做出極大貢獻的音樂家，[1985年](../Page/1985年.md "wikilink")[11月設立在位於](../Page/11月.md "wikilink")[好萊塢中央位置的](../Page/好萊塢.md "wikilink")[吉他中心商店](../Page/吉他中心.md "wikilink")。與[Elvis
Aaron Presley](../Page/Elvis_Aaron_Presley.md "wikilink")、[John
Lennon等人](../Page/John_Lennon.md "wikilink")，超過170組音樂家受邀進入殿堂，在館內展示音樂家的簽名金屬板及手印、吉他等。

B'z成為第176組獲獎者，作為亞洲圈音樂家則為首次壯舉。主要獲選理由為，長達20年的長時間活動、日本CD總銷量之最、美國巡迴演唱會成功實績，由過去的獲獎者[Steve
Vai所推舉](../Page/Steve_Vai.md "wikilink")。Vai祝福道「B'z非常有才華，搖滾大道上能有像他們這樣的非英語系藝人受到矚目，是一件非常美妙的事情。我由衷恭喜他們」\[94\]。11月19日正午（當地時間），好萊塢搖滾大道頒獎典禮實行對外開放，在此處刻下B'z兩人手印。

## 獎項・紀錄

### 獲獎歷

**[日本有線大獎](../Page/日本有線大獎.md "wikilink")**

  - 『第23屆日本有線大獎』最多點播歌手獎\[95\]
  - 『第24屆日本有線大獎』最多點播歌手獎\[96\]

**[日本唱片大獎](../Page/日本唱片大獎.md "wikilink")**

  - 『』【流行搖滾部門】優秀專輯獎『[WICKED
    BEAT](../Page/WICKED_BEAT.md "wikilink")』\[97\]
  - 『』【流行搖滾部門】優秀專輯獎『[RISKY](../Page/RISKY.md "wikilink")』\[98\]
  - 『』【流行搖滾部門】優秀專輯獎『[IN THE
    LIFE](../Page/IN_THE_LIFE.md "wikilink")』\[99\]

**[日本金唱片大獎](../Page/日本金唱片大獎.md "wikilink")**

  - 『第5屆日本金唱片大獎』5位最佳藝人獎（*Best 5 Artist Awards*）\[100\]
  - 『第6屆日本金唱片大獎』5位最佳藝人獎\[101\]、5張最佳單曲獎（*Best 5 Single Award*）「[LADY
    NAVIGATION](../Page/LADY_NAVIGATION.md "wikilink")」\[102\]、音樂影片獎（*Music
    Video Award*）『』\[103\]
  - 『第7屆日本金唱片大獎』5位最佳藝人獎\[104\]、5張最佳單曲獎「[BLOWIN'](../Page/BLOWIN'.md "wikilink")」\[105\]
  - 『第8屆日本金唱片大獎』5張最佳單曲獎「[愛のままにわがままに
    僕は君だけを傷つけない](../Page/愛のままにわがままに_僕は君だけを傷つけない.md "wikilink")」\[106\]、音樂影片獎『』\[107\]
  - 『第9屆日本金唱片大獎』5位最佳藝人獎\[108\]、5張最佳單曲獎「[Don't Leave
    Me](../Page/Don't_Leave_Me.md "wikilink")」\[109\]
  - 『第10屆日本金唱片大獎』5位最佳藝人獎\[110\]、5張最佳單曲獎「[LOVE
    PHANTOM](../Page/LOVE_PHANTOM.md "wikilink")」\[111\]、Grand
    Prix專輯獎（*Grand Prix Album
    Award*）『[LOOSE](../Page/LOOSE_\(B'z專輯\).md "wikilink")』\[112\]、專輯獎
    [民謠搖滾部門](../Page/民謠搖滾.md "wikilink")（男性）『LOOSE』\[113\]、音樂影片獎『』\[114\]
  - 『第12屆日本金唱片大獎』年度最佳搖滾專輯（*Rock Album of the
    year*）『[SURVIVE](../Page/SURVIVE.md "wikilink")』\[115\]
  - 『第13屆日本金唱片大獎』年度最佳藝人（*Artist of the Year*）\[116\]、年度最佳搖滾專輯『[B'z The
    Best
    "Pleasure"](../Page/B'z_The_Best_"Pleasure".md "wikilink")』『[B'z
    The Best
    "Treasure"](../Page/B'z_The_Best_"Treasure".md "wikilink")』\[117\]、年度最佳歌曲（*Song
    of the Year*）「[HOME](../Page/HOME_\(B'z單曲\).md "wikilink")」\[118\]
  - 『第14屆日本金唱片大獎』年度最佳搖滾專輯『[Brotherhood](../Page/Brotherhood_\(B'z專輯\).md "wikilink")』\[119\]
  - 『第15屆日本金唱片大獎』年度最佳歌曲「[今夜月の見える丘に](../Page/今夜月の見える丘に.md "wikilink")」\[120\]、年度最佳搖滾專輯『[B'z
    The
    "Mixture"](../Page/B'z_The_"Mixture".md "wikilink")』『[ELEVEN](../Page/ELEVEN.md "wikilink")』\[121\]
  - 『第17屆日本金唱片大獎』年度最佳搖滾＆流行專輯（*Rock & Pop Album of the
    Year*）『[GREEN](../Page/GREEN_\(B'z專輯\).md "wikilink")』『[The
    Ballads 〜Love &
    B'z〜](../Page/The_Ballads_〜Love_&_B'z〜.md "wikilink")』\[122\]、年度最佳音樂影片（長篇）『[a
    BEAUTIFUL
    REEL.](../Page/a_BEAUTIFUL_REEL._B'z_LIVE-GYM_2002_GREEN_〜GO★FIGHT★WIN〜.md "wikilink")』\[123\]
  - 『第18屆日本金唱片大獎』年度最佳歌曲（*Song of the Year*）「[IT'S
    SHOWTIME\!\!](../Page/IT'S_SHOWTIME!!.md "wikilink")」「[野性のENERGY](../Page/野性のENERGY.md "wikilink")」\[124\]、年度最佳搖滾＆流行專輯『[BIG
    MACHINE](../Page/BIG_MACHINE.md "wikilink")』\[125\]
  - 『第19屆日本金唱片大獎』年度最佳音樂影片（*Music Video of the Year*）『[Typhoon No.15 〜B'z
    LIVE-GYM The Final Pleasure "IT'S SHOWTIME\!\!" in
    渚園〜](../Page/Typhoon_No.15_〜B'z_LIVE-GYM_The_Final_Pleasure_"IT'S_SHOWTIME!!"_in_渚園〜.md "wikilink")』\[126\]
  - 『第20屆日本金唱片大獎』年度最佳歌曲「[OCEAN](../Page/OCEAN_\(B'z單曲\).md "wikilink")」\[127\]、年度最佳搖滾＆流行專輯『[THE
    CIRCLE](../Page/THE_CIRCLE.md "wikilink")』『[B'z The Best "Pleasure
    II"](../Page/B'z_The_Best_"Pleasure_II".md "wikilink")』\[128\]
  - 『第23屆日本金唱片大獎』10張最佳專輯（*The Best 10 Album*）『[B'z The Best "ULTRA
    Pleasure"](../Page/B'z_The_Best_"ULTRA_Pleasure".md "wikilink")』\[129\]
  - 『第24屆日本金唱片大獎』5張最佳單曲（*The Best 5
    Songs*）「[イチブトゼンブ](../Page/イチブトゼンブ.md "wikilink")」\[130\]
  - 『第28屆日本金唱片大獎』5張最佳專輯（*The Best 5 Album*）『[B'z The Best XXV
    1988-1998](../Page/B'z_The_Best_XXV_1988-1998.md "wikilink")』\[131\]

**[World Music Awards](../Page/世界音樂獎.md "wikilink")**

  - 『1999 The World Music Awards』World Selling Japanese Artist of The
    Year

**[ORICON](../Page/ORICON.md "wikilink")**

  - 『WE LOVE MUSIC AWARDS』\[132\]

****

  - 『Billboard JAPAN Music Awards 2009』Hot 100 of the Year
    2009「[イチブトゼンブ](../Page/イチブトゼンブ/DIVE.md "wikilink")」\[133\]

### ORICON紀錄

  - 藝人總銷量（CD總銷量）：8,262.4萬張（歴代第1位）\[134\]
      - 平成30年排行榜　藝人類銷售量（CD總銷量）：8,262.4萬張（第1位）\[135\]
  - 單曲總銷量：約3,596.9萬張（歴代第2位）\[136\]
  - 單曲首位獲得數：49作（歴代第1位）
  - 單曲連續首位獲得數：49作（歴代第1位・保持中）
  - 單曲連續初登場首位獲得數：49作（歴代第1位・保持中）
  - 單曲首位獲得週數：66週（歴代第1位）
  - 單曲連續首位獲得年數：20年（歴代第2位）
  - 單曲TOP10獲得數：50作（歴代第5位）
  - 單曲連續TOP10獲得數：50作（歴代第3位平手・保持中）
  - 單曲連續TOP10獲得年數：20年（歴代第3位平手）
  - 單曲總計百萬銷量獲得數：15作（歴代第2位）
  - 單曲連續百萬銷量獲得數：13作（歴代第2位）\[137\]
  - 單曲連續百萬銷量獲得年數：6年（歴代第1位）
  - 根據同一藝人週間單曲TOP10內同時上榜數：9作（歴代第1位）\[138\]

**專輯紀錄**

  - 專輯總銷量：約4,665.5萬張（歴代第1位）\[139\]
  - 專輯總計首位獲得數：28作（歴代第1位）
  - 專輯首位連続獲得年數：8年（截至1996年至2003年。歴代5位平手）
  - 專輯總計百萬銷量獲得數：19作（歴代第1位）\[140\]
  - 專輯連續百萬銷量獲得數：8作（歴代第2位，歴代第1位是[ZARD的](../Page/ZARD.md "wikilink")9作）
  - 專輯初動百萬銷量獲得數：10作（歴代第1位）

**影像作品記錄**

  - 根據音樂影像作品年間音樂影片榜首位獲得數：8作（[VHS](../Page/VHS.md "wikilink")：5作、[DVD](../Page/DVD.md "wikilink")：1作、[Blu-ray
    Disc](../Page/Blu-ray_Disc.md "wikilink")：2作，歴代第1位）
  - 根據音樂影像作品年間綜合影片榜首位獲得數：5作（歴代第1位）
  - 音樂VHS作品銷售張數：30.1萬張（歴代第3位）
  - 音樂DVD作品中的DVD綜合排行榜首位獲得作品數：8作（歴代第3位平手）
  - 音樂DVD作品中的DVD綜合排行榜首位連續獲得作品數：8作（歴代第1位）
  - 音樂Blu-ray Disc作品年間排行榜首位獲得數：2作（歴代第1位）\[141\]
  - 音樂Blu-ray Disc作品中的Blu-ray Disc綜合排行榜首位獲得作品數：3作（歴代第1位平手）

### 其他

  - 進入名人堂「[Hollywood's Rock Walk](../Page/吉他中心#好萊塢搖滾大道.md "wikilink")」
  - [金氏世界紀錄](../Page/金氏世界紀錄.md "wikilink")「日本專輯最暢銷藝人」（*日本でもっともアルバムを売り上げたアーティスト*）\[142\]\[143\]

## 作品

  - 單曲

<!-- end list -->

1.  [だからその手を離して](../Page/だからその手を離して.md "wikilink")（1988年）
2.  [君の中で踊りたい](../Page/君の中で踊りたい.md "wikilink")（1989年）
3.  [LADY-GO-ROUND](../Page/LADY-GO-ROUND.md "wikilink")（1990年）
4.  [BE THERE](../Page/BE_THERE.md "wikilink")（1990年）
5.  [太陽のKomachi Angel](../Page/太陽のKomachi_Angel.md "wikilink")（1990年）
6.  [Easy Come, Easy
    Go\!](../Page/Easy_Come,_Easy_Go!.md "wikilink")（1990年）
7.  [愛しい人よGood
    Night...](../Page/愛しい人よGood_Night....md "wikilink")（1990年）
8.  [LADY NAVIGATION](../Page/LADY_NAVIGATION.md "wikilink")（1991年）
9.  [ALONE](../Page/ALONE_\(B'z單曲\).md "wikilink")（1991年）
10. [BLOWIN'](../Page/BLOWIN'.md "wikilink")（1992年）
11. [ZERO](../Page/ZERO_\(B'z單曲\).md "wikilink")（1992年）
12. [愛のままにわがままに
    僕は君だけを傷つけない](../Page/愛のままにわがままに_僕は君だけを傷つけない.md "wikilink")（1993年）
13. [裸足の女神](../Page/裸足の女神.md "wikilink")（1993年）
14. [Don't Leave Me](../Page/Don't_Leave_Me.md "wikilink")（1994年）
15. [MOTEL](../Page/MOTEL.md "wikilink")（1994年）
16. [ねがい](../Page/心願_\(B'z單曲\).md "wikilink")（1995年）
17. [love me, I love
    you](../Page/love_me,_I_love_you.md "wikilink")（1995年）
18. [LOVE PHANTOM](../Page/LOVE_PHANTOM.md "wikilink")（1995年）
19. [ミエナイチカラ 〜INVISIBLE
    ONE〜/MOVE](../Page/看不見的力量_～INVISIBLE_ONE～/MOVE.md "wikilink")（1996年）
20. [Real Thing Shakes](../Page/Real_Thing_Shakes.md "wikilink")（1996年）
21. [FIREBALL](../Page/FIREBALL.md "wikilink")（1997年）
22. [Calling](../Page/Calling_\(B'z單曲\).md "wikilink")（1997年）
23. [Liar\! Liar\!](../Page/Liar!_Liar!.md "wikilink")（1997年）
24. [さまよえる蒼い弾丸](../Page/さまよえる蒼い弾丸.md "wikilink")（1998年）
25. [HOME](../Page/HOME_\(B'z單曲\).md "wikilink")（1998年）
26. [ギリギリchop](../Page/ギリギリchop.md "wikilink")（1999年）
27. [今夜月の見える丘に](../Page/今夜月の見える丘に.md "wikilink")（2000年）
28. [May](../Page/May_\(B'z單曲\).md "wikilink")（2000年）
29. [juice](../Page/juice.md "wikilink")（2000年）
30. [RING](../Page/RING_\(B'z單曲\).md "wikilink")（2000年）
31. [ultra soul](../Page/ultra_soul.md "wikilink")（2001年）
32. [GOLD](../Page/GOLD_\(B'z單曲\).md "wikilink")（2001年）
33. [熱き鼓動の果て](../Page/熱き鼓動の果て.md "wikilink")（2002年）
34. [IT'S SHOWTIME\!\!](../Page/IT'S_SHOWTIME!!.md "wikilink")（2003年）
35. [野性のENERGY](../Page/野性のENERGY.md "wikilink")（2003年）
36. [BANZAI](../Page/BANZAI_\(B'z單曲\).md "wikilink")（2004年）
37. [ARIGATO](../Page/ARIGATO.md "wikilink")（2004年）
38. [愛のバクダン](../Page/愛のバクダン.md "wikilink")（2005年）
39. [OCEAN](../Page/OCEAN_\(B'z單曲\).md "wikilink")（2005年）
40. [衝動](../Page/衝動_\(B'z單曲\).md "wikilink")（2006年）
41. [ゆるぎないものひとつ](../Page/ゆるぎないものひとつ.md "wikilink")（2006年）
42. [SPLASH\!](../Page/SPLASH!.md "wikilink")（2006年）
43. [永遠の翼](../Page/永遠の翼.md "wikilink")（2007年）
44. [SUPER LOVE SONG](../Page/SUPER_LOVE_SONG.md "wikilink")（2007年）
45. [BURN -フメツノフェイス-](../Page/BURN_-フメツノフェイス-.md "wikilink")（2008年）
46. [イチブトゼンブ/DIVE](../Page/イチブトゼンブ/DIVE.md "wikilink")（2009年）
47. [MY LONELY TOWN](../Page/MY_LONELY_TOWN.md "wikilink")（2009年）
48. [さよなら傷だらけの日々よ](../Page/さよなら傷だらけの日々よ.md "wikilink")（2011年）
49. [Don't Wanna Lie](../Page/Don't_Wanna_Lie.md "wikilink")（2011年）
50. [GO FOR IT, BABY
    -キオクの山脈-](../Page/GO_FOR_IT,_BABY_-記憶的山脈-.md "wikilink")（2012年）
51. [有頂天](../Page/有頂天.md "wikilink")（2015年）
52. [RED](../Page/RED.md "wikilink")（2015年）
53. [-{声}-明／Still Alive](../Page/声明/Still_Alive.md "wikilink")（2017年）

<!-- end list -->

  - 原創專輯

<!-- end list -->

1.  [B'z](../Page/B'z_\(1988年專輯\).md "wikilink")（1988年）
2.  [OFF THE LOCK](../Page/OFF_THE_LOCK.md "wikilink")（1989年）
3.  [BREAK THROUGH](../Page/BREAK_THROUGH.md "wikilink")（1990年）
4.  [RISKY](../Page/RISKY.md "wikilink")（1990年）
5.  [IN THE LIFE](../Page/IN_THE_LIFE.md "wikilink")（1991年）
6.  [RUN](../Page/RUN.md "wikilink")（1992年）
7.  [The 7th Blues](../Page/The_7th_Blues.md "wikilink")（1994年）
8.  [LOOSE](../Page/LOOSE_\(B'z專輯\).md "wikilink") （1995年）
9.  [SURVIVE](../Page/SURVIVE.md "wikilink")（1997年）
10. [Brotherhood](../Page/Brotherhood_\(B'z專輯\).md "wikilink")（1999年）
11. [ELEVEN](../Page/ELEVEN.md "wikilink")（2000年）
12. [GREEN](../Page/GREEN_\(B'z專輯\).md "wikilink")（2002年）
13. [BIG MACHINE](../Page/BIG_MACHINE.md "wikilink")（2003年）
14. [THE CIRCLE](../Page/THE_CIRCLE.md "wikilink")（2005年）
15. [MONSTER](../Page/MONSTER_\(B'z專輯\).md "wikilink")（2006年）
16. [ACTION](../Page/ACTION.md "wikilink")（2007年）
17. [MAGIC](../Page/MAGIC_\(B'z專輯\).md "wikilink")（2009年）
18. [C'mon](../Page/C'mon.md "wikilink")（2011年）
19. [EPIC DAY](../Page/EPIC_DAY.md "wikilink")（2015年）
20. [DINOSAUR](../Page/DINOSAUR.md "wikilink")（2017年）
21. [NEW LOVE](../Page/NEW_LOVE.md "wikilink")（2019年）

## 演唱會

LIVE-GYM\#B'z公演列表}}

### 支援樂手

2018年7月開始的巡迴演唱會「B'z LIVE-GYM Pleasure 2018 -HINOTORI-」時點的支援樂手。

<table>
<thead>
<tr class="header">
<th><p>人名</p></th>
<th><p>職務（演唱會）</p></th>
<th><p>職務（錄音）</p></th>
<th><p>在籍期間（僅演唱會）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/增田隆宣.md" title="wikilink">增田隆宣</a></p></td>
<td><p><a href="../Page/電子琴.md" title="wikilink">電子琴</a></p></td>
<td><p><a href="../Page/鋼琴.md" title="wikilink">鋼琴</a>、<a href="../Page/管風琴.md" title="wikilink">管風琴</a></p></td>
<td><p>1992年 - 1997年、1999年 - 2018年</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/爵士鼓.md" title="wikilink">爵士鼓</a></p></td>
<td><p>爵士鼓、<a href="../Page/打擊樂器.md" title="wikilink">打擊樂器</a></p></td>
<td><p>2002年 - 2018年</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/貝斯.md" title="wikilink">貝斯</a></p></td>
<td><p>貝斯</p></td>
<td><p>2003年、2008年 - 2018年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大賀好修.md" title="wikilink">大賀好修</a></p></td>
<td><p><a href="../Page/吉他.md" title="wikilink">吉他</a></p></td>
<td><p><a href="../Page/編曲.md" title="wikilink">編曲</a></p></td>
<td><p>2011年 - 2018年</p></td>
</tr>
</tbody>
</table>

**前任支援樂手**

<table>
<thead>
<tr class="header">
<th><p>人名</p></th>
<th><p>職務</p></th>
<th><p>在籍期間（僅演唱會）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/明石昌夫.md" title="wikilink">明石昌夫</a></p></td>
<td><p>、<a href="../Page/貝斯.md" title="wikilink">貝斯</a></p></td>
<td><p>1989年 - 1997年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/德永曉人.md" title="wikilink">德永曉人</a>（from <a href="../Page/doa_(樂團).md" title="wikilink">doa</a>）</p></td>
<td><p>貝斯、<a href="../Page/和聲歌手.md" title="wikilink">和聲</a></p></td>
<td><p>1998年、2003年 - 2007年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/滿園庄太郎.md" title="wikilink">滿園庄太郎</a></p></td>
<td><p>貝斯</p></td>
<td><p>1999年 - 2001年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Billy_Sheehan.md" title="wikilink">Billy Sheehan</a></p></td>
<td><p>2002年</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/爵士鼓.md" title="wikilink">爵士鼓</a></p></td>
<td><p>1989年 - 1990年</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1990年 - 1994年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Denny Fongheiser</p></td>
<td><p>1995年 - 1997年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黑瀨蛙一.md" title="wikilink">黑瀨蛙一</a></p></td>
<td><p>1998年 - 2001年</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/電子琴.md" title="wikilink">電子琴</a></p></td>
<td><p>1989年 - 1992年</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1998年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>高原裕枝</p></td>
<td><p><a href="../Page/和聲歌手.md" title="wikilink">和聲</a></p></td>
<td><p>1992年</p></td>
</tr>
<tr class="even">
<td><p>中村優子</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大田紳一郎.md" title="wikilink">大田紳一郎</a>（from doa）</p></td>
<td><p>和聲歌手、<a href="../Page/吉他.md" title="wikilink">吉他</a>、和聲</p></td>
<td><p>2003年 - 2008年</p></td>
</tr>
<tr class="even">
<td><p>澤野博敬</p></td>
<td><p><a href="../Page/小號.md" title="wikilink">小號</a></p></td>
<td><p>1993年</p></td>
</tr>
<tr class="odd">
<td><p>澤田秀浩</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/長號.md" title="wikilink">長號</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>吉田じゅんべい</p></td>
<td><p><a href="../Page/薩克斯風.md" title="wikilink">薩克斯風</a></p></td>
<td></td>
</tr>
</tbody>
</table>

### Z'b

Z'b是在『B'z LIVE-GYM '91-'92 "IN THE
LIFE"』開始前準備活動中玩心大發創造的樂團名，以[翻唱西洋樂曲為中心在日本國內](../Page/翻唱.md "wikilink")4間[LIVE
HOUSE舉行了](../Page/展演空間.md "wikilink")『Z'b LIVE HOUSE
TOUR』。之後Z'b成員亦登場過演唱會上的環節之一，對於來自粉絲的詢問「不會再做Z'b了嗎{{\#tag:ref||group="原文"}}」松本回答表示「大概不會再做了{{\#tag:ref||group="原文"}}」\[144\]。

<table>
<caption>Z'b成員</caption>
<thead>
<tr class="header">
<th><p>人名</p></th>
<th><p>職務</p></th>
<th><p>飾演者</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Amigo Komachi Angel Jr.（僅在名古屋為早乙女 順）</p></td>
<td><p><a href="../Page/主唱.md" title="wikilink">主唱</a></p></td>
<td><p><a href="../Page/稻葉浩志.md" title="wikilink">稻葉浩志</a></p></td>
</tr>
<tr class="even">
<td><p>綾乃小路 幹彦（僅在神戶為マリモ・ハラオ）</p></td>
<td><p><a href="../Page/吉他.md" title="wikilink">吉他</a></p></td>
<td><p><a href="../Page/松本孝弘.md" title="wikilink">松本孝弘</a></p></td>
</tr>
<tr class="odd">
<td><p>一貫全裸（はだか一貫）</p></td>
<td><p><a href="../Page/貝斯.md" title="wikilink">貝斯</a></p></td>
<td><p><a href="../Page/明石昌夫.md" title="wikilink">明石昌夫</a></p></td>
</tr>
<tr class="even">
<td><p>翻車魚・烏龜（マンボウ・カメ）（僅在名古屋為菊之門 雅（菊の門 雅））</p></td>
<td><p><a href="../Page/電子琴.md" title="wikilink">電子琴</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>恰吉木芥子（チャッキーこけし）</p></td>
<td><p><a href="../Page/爵士鼓.md" title="wikilink">爵士鼓</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 出演

**廣播電台**

  - 『[B'z WAVE-GYM](../Page/B'z_WAVE-GYM.md "wikilink")』（1990年1月4日 -
    1991年3月）

  - [日本放送](../Page/日本放送.md "wikilink")『B'zの』『B'zの』（1992年、1995年、2000年、2001年、2003年、2005年）

**動畫**

  - [讀賣電視台](../Page/讀賣電視台.md "wikilink")・[日本電視台系動畫](../Page/日本電視台.md "wikilink")『[名偵探柯南](../Page/名偵探柯南_\(動畫\).md "wikilink")』（片頭曲影像使用了一部分的「[ギリギリchop](../Page/ギリギリchop.md "wikilink")」（1999年5月3日
    - 11月8日）、「[Don't Wanna
    Lie](../Page/Don't_Wanna_Lie.md "wikilink")」（2011年4月30日 -
    7月30日）[音樂錄影帶](../Page/音樂錄影帶.md "wikilink")）

**電視節目**  **特別節目**

  - 『B'z LIVE-GYM 2002 "Rock n' California Roll"
    LIVE\&DOCUMENT』（2002年12月21日）

  - [朝日電視台](../Page/朝日電視台.md "wikilink")『B'z 15周年特別番組 「IT'S SHOWTIME\!\!
    〜とどけ！世界水泳バルセロナ2003〜」』（2003年6月27日）

  - BSデジタル5局 開局3周年共同特別番組『B'z LIVE-GYM 2003 "BANZAI IN NORTH AMERICA"
    LIVE\&DOCUMENT』（2003年12月23日）

  - [NHK綜合](../Page/NHK綜合頻道.md "wikilink")『NHKスペシャル 「メガヒットの秘密
    〜20年目のB'z〜」』（2008年10月6日\[145\]、2018年9月17日）

  - [NHK BS2](../Page/NHK衛星第2頻道.md "wikilink")『RUN
    〜B'z・20年の軌跡〜』（2009年1月1日）

  - [WOWOW](../Page/WOWOW.md "wikilink")『B'z 25th Anniversary Special
    「Only Two」』（2012年12月9日）

  - [niconico直播](../Page/ニコニコ生放送.md "wikilink")『B'z 25th Anniversary
    DIGEST 1988-2013』（2013年6月8日）\[146\]

  - B'z Official YouTube Channel『B'z 25th Anniversary YouTube Special
    Program』（2013年6月16日、2013年6月19日 -）\[147\]\[148\]

  - WOWOW『WOWOW×B'z スペシャル番組』（2018年4月11日、5月26日、6月30日、7月21日、8月18日）

  - WOWOW『B'z LIVE-GYM Pleasure 2018 -HINOTORI-』（2018年11月24日）\[149\]

**CM**

  - [百事可樂](../Page/百事可樂.md "wikilink")「Pepsi NEX」
      - エフェクト・荒野篇（2011年3月1日 - ）\[150\]
      - People篇（2011年7月16日 - ）\[151\]
      - Xmas Lover篇（2011年12月3日 - ）
      - Tshirts Live篇（2012年2月28日 - ）\[152\]
      - Summer Line篇（2012年7月14日 - ）

## 商業搭配一覽

<table>
<thead>
<tr class="header">
<th><p>年</p></th>
<th><p>曲</p></th>
<th><p>商業搭配</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1989年</p></td>
<td><p><a href="../Page/君の中で踊りたい.md" title="wikilink">君の中で踊りたい</a></p></td>
<td><p><a href="../Page/TBS電視台.md" title="wikilink">TBS系連續劇</a>『ハイミスで悪かったネ!』片尾曲</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BAD_COMMUNICATION.md" title="wikilink">BAD COMMUNICATION</a></p></td>
<td><p><a href="../Page/富士通.md" title="wikilink">富士通</a>「<a href="../Page/FM_Towns.md" title="wikilink">FM TOWNS</a>」<a href="../Page/電視廣告.md" title="wikilink">廣告曲</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1990年</p></td>
<td><p><a href="../Page/LADY-GO-ROUND.md" title="wikilink">LOVE &amp; CHAIN</a></p></td>
<td><p><a href="../Page/朝日電視台.md" title="wikilink">朝日電視台系</a>『<a href="../Page/NEWS_STATION.md" title="wikilink">NEWS STATION</a>』體育單元片頭曲</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BE_THERE.md" title="wikilink">BE THERE</a></p></td>
<td><p>朝日電視台系『』片尾曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/太陽のKomachi_Angel.md" title="wikilink">太陽のKomachi Angel</a></p></td>
<td><p>「Camellia Diamond」廣告曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Easy_Come,_Easy_Go!.md" title="wikilink">Easy Come, Easy Go!</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛しい人よGood_Night....md" title="wikilink">愛しい人よGood Night...</a></p></td>
<td><p>朝日電視台系連續劇『』片尾曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/WICKED_BEAT.md" title="wikilink">I Wanna Dance -Wicked Beat Style-</a></p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台系</a>『』片頭曲（僅前奏部分）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/RISKY.md" title="wikilink">HOT FASHION -流行過多-</a></p></td>
<td><p>富士電視台系『上岡龍太郎にはダマされないぞ!』片尾曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1991年</p></td>
<td><p><a href="../Page/LADY_NAVIGATION.md" title="wikilink">LADY NAVIGATION</a></p></td>
<td><p>'91夏日形象曲</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/ALONE_(B&#39;z單曲).md" title="wikilink">ALONE</a></p></td>
<td><p><a href="../Page/關西電視台.md" title="wikilink">關西電視台</a>・富士電視台系連續劇『』主題歌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1992年</p></td>
<td><p><a href="../Page/BLOWIN&#39;.md" title="wikilink">BLOWIN'</a></p></td>
<td><p><a href="../Page/卡樂比.md" title="wikilink">Calbee</a>「<a href="../Page/洋芋片.md" title="wikilink">-{zh-tw:洋芋片;zh-hk:薯片;zh-cn:马铃薯片;}-</a>」廣告曲</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BLOWIN&#39;.md" title="wikilink">TIME</a></p></td>
<td><p>朝日電視台系『』片頭曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1993年</p></td>
<td><p><a href="../Page/愛のままにわがままに_僕は君だけを傷つけない.md" title="wikilink">愛のままにわがままに 僕は君だけを傷つけない</a></p></td>
<td><p><a href="../Page/日本電視放送網.md" title="wikilink">日本電視台開局</a>40年記念連續劇『<a href="../Page/西遊記系列#西遊記（1993年）.md" title="wikilink">西遊記</a>』主題歌</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/裸足の女神.md" title="wikilink">裸足の女神</a></p></td>
<td><p><a href="../Page/豐田汽車.md" title="wikilink">豐田汽車</a>「」廣告曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1994年</p></td>
<td><p><a href="../Page/Don&#39;t_Leave_Me.md" title="wikilink">Don't Leave Me</a></p></td>
<td><p>朝日電視台系連續劇『』主題歌</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/MOTEL.md" title="wikilink">MOTEL</a></p></td>
<td><p>三貴「Boutique JOY」廣告曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1995年</p></td>
<td><p><a href="../Page/心願_(B&#39;z單曲).md" title="wikilink">ねがい</a></p></td>
<td><p>日本全國25局聯播網『』片尾曲</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/love_me,_I_love_you.md" title="wikilink">love me, I love you</a></p></td>
<td><p>朝日電視台系連續劇『』主題歌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/LOVE_PHANTOM.md" title="wikilink">LOVE PHANTOM</a></p></td>
<td><p>朝日電視台系連續劇『<a href="../Page/X檔案.md" title="wikilink">X-FILE</a>』主題歌</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>TBS系『<a href="../Page/COUNT_DOWN_TV.md" title="wikilink">COUNT DOWN TV</a>』片頭曲</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996年</p></td>
<td><p><a href="../Page/看不見的力量_～INVISIBLE_ONE～/MOVE.md" title="wikilink">ミエナイチカラ 〜INVISIBLE ONE〜</a></p></td>
<td><p>朝日電視台系動畫『<a href="../Page/靈異教師神眉.md" title="wikilink">-{zh-tw:靈異教師神眉;zh-cn:地狱老师;zh-hk:鳴～地獄老師}-</a>』片尾曲</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/看不見的力量_～INVISIBLE_ONE～/MOVE.md" title="wikilink">MOVE</a></p></td>
<td><p>「」廣告曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Real_Thing_Shakes.md" title="wikilink">Real Thing Shakes</a></p></td>
<td><p>日本電視台系連續劇『』主題歌</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>朝日電視台系『』主題曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997年</p></td>
<td><p><a href="../Page/FIREBALL.md" title="wikilink">FIREBALL</a></p></td>
<td><p><a href="../Page/資生堂.md" title="wikilink">資生堂</a>「」廣告曲</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/FIREBALL.md" title="wikilink">哀しきdreamer</a></p></td>
<td><p><a href="../Page/Takara_Tomy.md" title="wikilink">Tomy製</a><a href="../Page/遊戲軟體.md" title="wikilink">遊戲軟體</a>『<a href="../Page/印第安納波利斯500.md" title="wikilink">INDY500</a>』形象曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/いつかのメリークリスマス.md" title="wikilink">いつかのメリークリスマス</a></p></td>
<td><p><a href="../Page/東寶.md" title="wikilink">東寶系電影</a>『』插入曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Calling_(B&#39;z單曲).md" title="wikilink">Calling</a></p></td>
<td><p>朝日電視台系連續劇『<a href="../Page/千面女郎_(電視劇).md" title="wikilink">千面女郎</a>』主題歌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Liar!_Liar!.md" title="wikilink">Liar! Liar!</a></p></td>
<td><p><a href="../Page/SKY_PerfecTV!.md" title="wikilink">PerfecTV</a>『』廣告曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>『'97 <a href="../Page/國家冰球聯盟.md" title="wikilink">NHL</a> 日本官方開幕戰』官方主題曲</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Liar!_Liar!.md" title="wikilink">ビリビリ</a></p></td>
<td><p>『J-ROCK ARTIST BEST 50』片尾曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1998年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/さまよえる蒼い弾丸.md" title="wikilink">さまよえる蒼い弾丸</a></p></td>
<td><p><a href="../Page/大塚製藥.md" title="wikilink">大塚製藥</a>「<a href="../Page/寶礦力水得.md" title="wikilink">-{zh-tw:寶礦力水得;zh-cn:宝矿力水特;zh-hk:寶礦力水特}-</a>」廣告曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>『J-ROCK ARTIST BEST 50』片尾曲</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Calling</p></td>
<td><p>朝日電視台系連續劇『千面女郎』主題歌</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/SURVIVE.md" title="wikilink">ハピネス</a></p></td>
<td><p>朝日電視台系連續劇『<a href="../Page/千面女郎_(電視劇).md" title="wikilink">千面女郎</a>』片尾曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/さまよえる蒼い弾丸.md" title="wikilink">Hi</a></p></td>
<td><p>富士電視台系『Fomurla Nippon』主題曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/HOME_(B&#39;z單曲).md" title="wikilink">The Wild Wind</a></p></td>
<td><p><a href="../Page/東映.md" title="wikilink">東映</a>・電影『<a href="../Page/不夜城_(小說).md" title="wikilink">不夜城</a>』主題歌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/HOME_(B&#39;z單曲).md" title="wikilink">HOME</a></p></td>
<td><p><a href="../Page/角川書店.md" title="wikilink">角川文庫廣告曲</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/B&#39;z_The_Best_&quot;Treasure&quot;.md" title="wikilink">RUN -1998 Style-</a></p></td>
<td><p><a href="../Page/日產汽車.md" title="wikilink">日産</a>「」廣告曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/B&#39;z_The_Best_&quot;Treasure&quot;.md" title="wikilink">Pleasure'98 〜人生の快楽〜</a></p></td>
<td><p>『NHL GAME ONE 98 JAPAN』主題曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999年</p></td>
<td><p><a href="../Page/ギリギリchop.md" title="wikilink">ONE</a></p></td>
<td><p>東寶系電影『<a href="../Page/名偵探柯南：世紀末的魔術師.md" title="wikilink">名偵探柯南：世紀末的魔術師</a>』主題歌</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/ギリギリchop.md" title="wikilink">ギリギリchop</a></p></td>
<td><p><a href="../Page/讀賣電視台.md" title="wikilink">讀賣電視台</a>・日本電視台系動畫『<a href="../Page/名偵探柯南_(動畫).md" title="wikilink">名偵探柯南</a>』片頭曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Brotherhood_(B&#39;z專輯).md" title="wikilink">F・E・A・R</a></p></td>
<td><p>富士電視台系『1999』主題曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p><a href="../Page/今夜月の見える丘に.md" title="wikilink">今夜月の見える丘に</a></p></td>
<td><p>TBS系連續劇『<a href="../Page/美麗人生.md" title="wikilink">美麗人生</a>』主題歌</p></td>
</tr>
<tr class="odd">
<td><p>F・E・A・R</p></td>
<td><p>富士電視台系『F1グランプリ2000』主題曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/May_(B&#39;z單曲).md" title="wikilink">You pray,I stay</a></p></td>
<td><p><a href="../Page/三得利.md" title="wikilink">三得利</a>「SUPER CHU-HI」廣告曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/juice.md" title="wikilink">juice</a></p></td>
<td><p>朝日電視台系『』片尾曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/RING_(B&#39;z單曲).md" title="wikilink">RING</a></p></td>
<td><p>讀賣電視台・日本電視台系連續劇『』主題歌</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><a href="../Page/ultra_soul.md" title="wikilink">ultra soul</a></p></td>
<td><p>「<a href="../Page/2001年世界游泳錦標賽.md" title="wikilink">2001年福岡世界游泳錦標賽</a>」大會官方主題曲</p></td>
</tr>
<tr class="even">
<td><p>[[ultra_soul|スイマーよ2001</p></td>
<td><p>]]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/ultra_soul.md" title="wikilink">ROCK man</a></p></td>
<td><p>富士電視台系『』片尾曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/GOLD_(B&#39;z單曲).md" title="wikilink">GOLD</a></p></td>
<td><p>「2001年福岡世界游泳錦標賽」大會官方主題曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p><a href="../Page/GREEN_(B&#39;z專輯).md" title="wikilink">SIGNAL</a></p></td>
<td><p><a href="../Page/科樂美數位娛樂.md" title="wikilink">科樂美</a>『<a href="../Page/純愛手札_Girl&#39;s_Side.md" title="wikilink">-{zh-tw:純愛手札;;zh-cn:心跳回忆;zh-hk:心跳回憶;}- Girl's Side</a>』片頭曲[153]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/GREEN_(B&#39;z專輯).md" title="wikilink">美しき世界</a></p></td>
<td><p>科樂美『-{zh-tw:純愛手札;;zh-cn:心跳回忆;zh-hk:心跳回憶;}- Girl's Side』片尾曲[154]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/GREEN_(B&#39;z專輯).md" title="wikilink">Everlasting</a></p></td>
<td><p>東寶系電影『<a href="../Page/名偵探柯南：貝克街的亡靈.md" title="wikilink">名偵探柯南：貝克街的亡靈</a>』主題歌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/熱き鼓動の果て.md" title="wikilink">熱き鼓動の果て</a></p></td>
<td><p>『』主題曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>「<a href="../Page/泛太平洋游泳錦標賽.md" title="wikilink">2002年橫濱泛太平洋游泳錦標賽</a>」大會官方主題曲</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>いつかのメリークリスマス</p></td>
<td><p>富士電視台系『感動ファクトリー・すぽると!』片頭曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p>[[IT'S_SHOWTIME</p></td>
<td><p>|IT'S SHOWTIME</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BIG_MACHINE.md" title="wikilink">CHANGE THE FUTURE</a></p></td>
<td><p><a href="../Page/NHK衛星第2頻道.md" title="wikilink">NHK-BS2</a> 『<a href="../Page/時空冒險記.md" title="wikilink">時空冒險記</a>』主題歌</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BIG_MACHINE.md" title="wikilink">WAKE UP, RIGHT NOW</a></p></td>
<td><p><a href="../Page/朝日啤酒.md" title="wikilink">朝日啤酒</a>「」廣告曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/野性のENERGY.md" title="wikilink">野性のENERGY</a></p></td>
<td><p>『TV ASAHI NETWORK SPORTS 2003』主題曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BIG_MACHINE.md" title="wikilink">儚いダイヤモンド</a></p></td>
<td><p>『<a href="../Page/NBA.md" title="wikilink">NBA</a> JAPAN GAMES 2003』廣告曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BIG_MACHINE.md" title="wikilink">アラクレ</a></p></td>
<td><p>富士電視台系連續劇『』主題歌</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BIG_MACHINE.md" title="wikilink">Nightbird</a></p></td>
<td><p><a href="../Page/Lotte.md" title="wikilink">Lotte</a>「潤喉糖」廣告曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BIG_MACHINE.md" title="wikilink">ROOTS</a></p></td>
<td><p>讀賣電視台・日本電視台系動畫『<a href="../Page/怪醫黑傑克_(動畫).md" title="wikilink">怪醫黑傑克2小時特別篇 ～4個生命奇蹟～</a>』主題歌</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p><a href="../Page/BANZAI_(B&#39;z單曲).md" title="wikilink">BANZAI</a></p></td>
<td><p>朝日啤酒「SUPER DRY」廣告曲</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/ARIGATO.md" title="wikilink">ARIGATO</a></p></td>
<td><p>『TV ASAHI NETWORK SPORTS 2004』主題曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>朝日電視台系<a href="../Page/2004年夏季奧林匹克運動會.md" title="wikilink">2004年夏季奧林匹克運動會播放主題曲</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>いつかのメリークリスマス New ver.</p></td>
<td><p>TBS系『』主題曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/B&#39;z_The_Best_&quot;Pleasure_II&quot;.md" title="wikilink">いつかのメリークリスマス 〜「恋するハニカミ!」Version〜</a>[155]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p><a href="../Page/THE_CIRCLE.md" title="wikilink">パルス</a></p></td>
<td><p><a href="../Page/日本放送協會.md" title="wikilink">NHK連續劇</a>『』主題曲</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛のバクダン.md" title="wikilink">愛のバクダン</a></p></td>
<td><p><a href="../Page/東京電視台.md" title="wikilink">東京電視台系</a>『』片尾曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>「Zespri黃金奇異果」廣告曲</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/OCEAN_(B&#39;z單曲).md" title="wikilink">OCEAN</a></p></td>
<td><p>富士電視台系連續劇『<a href="../Page/海猿#《海猿_Evolution》（電視劇）.md" title="wikilink">海猿 UMIZARU EVOLUTION</a>』主題歌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/OCEAN_(B&#39;z單曲).md" title="wikilink">なりふりかまわず抱きしめて</a></p></td>
<td><p><a href="../Page/朝日新聞社.md" title="wikilink">朝日新聞社</a>「<a href="../Page/朝日新聞.md" title="wikilink">朝日新聞</a>」廣告曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/RUN.md" title="wikilink">RUN</a></p></td>
<td><p>『・晴れの国おかやま国体』形象曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>『・輝いて!おかやま大会』形象曲</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p><a href="../Page/衝動_(B&#39;z單曲).md" title="wikilink">衝動</a></p></td>
<td><p>讀賣電視台・日本電視台系動畫『名偵探柯南』片頭曲</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/衝動_(B&#39;z單曲).md" title="wikilink">結晶</a></p></td>
<td><p>日本電視台系連續劇|『<a href="../Page/大胃王神探.md" title="wikilink">-{zh-tw:大胃王神探;zh-cn:美食侦探王;zh-hk:為食神探;}-</a>』主題歌</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/ゆるぎないものひとつ.md" title="wikilink">ゆるぎないものひとつ</a></p></td>
<td><p>東寶系電影『<a href="../Page/名偵探柯南：偵探們的鎮魂歌.md" title="wikilink">名偵探柯南：偵探們的鎮魂歌</a>』主題歌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/ゆるぎないものひとつ.md" title="wikilink">ピエロ</a></p></td>
<td><p><a href="../Page/多玩國.md" title="wikilink">多玩國</a>「」廣告曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/SPLASH!.md" title="wikilink">SPLASH!</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p><a href="../Page/永遠の翼.md" title="wikilink">永遠の翼</a></p></td>
<td><p>東映系電影『』主題歌[156]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/永遠の翼.md" title="wikilink">ロンリースターズ</a></p></td>
<td><p>電影『<a href="../Page/北斗神拳.md" title="wikilink">真救世主傳說 北斗神拳 拉歐傳 激鬥之章</a>』主題歌[157]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>「ポケメロJOYSOUND」廣告配樂</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/SUPER_LOVE_SONG.md" title="wikilink">FRICTION</a></p></td>
<td><p><a href="../Page/美商藝電.md" title="wikilink">美商藝電</a>『<a href="../Page/橫衝直撞.md" title="wikilink">橫衝直撞</a>』提供曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>東京電視台系『JAPAN COUNTDOWN』2007年10月期片頭曲</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/ACTION.md" title="wikilink">オレとオマエの新しい季節</a></p></td>
<td><p>日本電視台系連續劇『<a href="../Page/The_O.C..md" title="wikilink">The O.C.</a>』片尾曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/ACTION.md" title="wikilink">ONE ON ONE</a></p></td>
<td><p>TBS系『』片尾曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/ACTION.md" title="wikilink">純情ACTION</a></p></td>
<td><p>「<a href="../Page/日本籃球超级聯賽.md" title="wikilink">日本籃球超级聯賽</a> 2007-2008」官方曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/ACTION.md" title="wikilink">パーフェクトライフ</a></p></td>
<td><p>「<a href="../Page/music.jp.md" title="wikilink">music.jp</a>」廣告曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p><a href="../Page/BURN_-フメツノフェイス-.md" title="wikilink">BURN -フメツノフェイス-</a></p></td>
<td><p><a href="../Page/高絲.md" title="wikilink">KOSÉ</a>「」廣告曲</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/B&#39;z_The_Best_&quot;ULTRA_Treasure&quot;.md" title="wikilink">いつかまたここで</a></p></td>
<td><p>朝日電視台系連續劇『』主題歌[158]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p><a href="../Page/ZERO_(B&#39;z單曲).md" title="wikilink">ZERO</a></p></td>
<td><p><a href="../Page/麒麟啤酒.md" title="wikilink">麒麟啤酒</a>「麒麟ZERO」廣告曲[159]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/一部分與全部/DIVE.md" title="wikilink">DIVE</a></p></td>
<td><p><a href="../Page/鈴木公司.md" title="wikilink">鈴木公司</a>「<a href="../Page/鈴木Swift.md" title="wikilink">SWIFT</a>」廣告曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/イチブトゼンブ/DIVE.md" title="wikilink">イチブトゼンブ</a></p></td>
<td><p>富士電視台系<a href="../Page/富士電視台週一晚間九點連續劇.md" title="wikilink">週一晚間9點連續劇</a>『<a href="../Page/零秒出手.md" title="wikilink">零秒出手</a>』主題歌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/MY_LONELY_TOWN.md" title="wikilink">イチブトゼンブ -Ballad Version-</a></p></td>
<td><p>富士電視台系週一晚間9點連續劇『零秒出手』插入曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/MAGIC_(B&#39;z專輯).md" title="wikilink">PRAY</a></p></td>
<td><p>東急系電影『』主題歌[160]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p><a href="../Page/MAGIC_(B&#39;z專輯).md" title="wikilink">long time no see</a></p></td>
<td><p>朝日電視台系連續劇『』主題歌[161]</p></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p><a href="../Page/さよなら傷だらけの日々よ.md" title="wikilink">さよなら傷だらけの日々よ</a></p></td>
<td><p><a href="../Page/百事可樂.md" title="wikilink">百事可樂</a>「Pepsi NEX」エフェクト・荒野篇 廣告曲</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Don&#39;t_Wanna_Lie.md" title="wikilink">Don't Wanna Lie</a></p></td>
<td><p>東寶系電影『<a href="../Page/名偵探柯南：沉默的15分鐘.md" title="wikilink">名偵探柯南：沉默的15分鐘</a>』主題歌[162]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>讀賣電視台・日本電視台系動畫『名偵探柯南』片頭曲[163]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Don&#39;t_Wanna_Lie.md" title="wikilink">Homebound</a></p></td>
<td><p>TBS系『』片尾曲[164]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/C&#39;mon.md" title="wikilink">ultra soul 2011</a></p></td>
<td><p>朝日電視台系<a href="../Page/2011年世界游泳錦標賽.md" title="wikilink">2011年上海世界游泳錦標賽播放主題曲</a>[165]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/C&#39;mon.md" title="wikilink">C'mon</a></p></td>
<td><p>百事可樂「Pepsi NEX」People篇 廣告曲[166]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/C&#39;mon.md" title="wikilink">ピルグリム</a></p></td>
<td><p>讀賣電視台・日本電視台系動畫『名偵探柯南』片尾曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>いつかのメリークリスマス</p></td>
<td><p>百事可樂「Pepsi NEX」Xmas Lover篇 廣告曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012年</p></td>
<td><p><a href="../Page/さまよえる蒼い弾丸.md" title="wikilink">Into Free -Dangan-</a></p></td>
<td><p><a href="../Page/卡普空.md" title="wikilink">卡普空製遊戲軟體</a>『<a href="../Page/龍族教義.md" title="wikilink">龍族教義</a>』主題歌[167]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/GO_FOR_IT,_BABY_-記憶的山脈-.md" title="wikilink">GO FOR IT, BABY -キオクの山脈-</a></p></td>
<td><p>百事可樂「Pepsi NEX」Tshirts Live篇 廣告曲[168]</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>百事可樂「Pepsi NEX」Summer Line篇 廣告曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p><a href="../Page/B&#39;z_The_Best_XXV_1988-1998.md" title="wikilink">核心</a></p></td>
<td><p>日本電視台系連續劇『<a href="../Page/白雲階梯_(日本電視劇).md" title="wikilink">白雲階梯</a>』主題歌[169]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/B&#39;z_The_Best_XXV_1999-2012.md" title="wikilink">Q&amp;A</a></p></td>
<td><p>讀賣電視台・日本電視台系動畫『名偵探柯南』片頭曲[170]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>ultra soul</p></td>
<td><p>朝日電視台系<a href="../Page/2013年世界游泳錦標賽.md" title="wikilink">2013年巴塞隆納世界游泳錦標賽播放主題曲</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/B&#39;z_The_Best_XXV_1999-2012.md" title="wikilink">ユートピア</a></p></td>
<td><p>朝日電視台系連續劇『<a href="../Page/最強名醫.md" title="wikilink">DOCTORS 2〜最強名醫〜</a>』主題歌[171]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014年</p></td>
<td><p>ultra soul</p></td>
<td><p>朝日電視台系<a href="../Page/泛太平洋游泳錦標賽.md" title="wikilink">2014年黃金海岸泛太平洋游泳錦標賽播放主題曲</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/EPIC_DAY.md" title="wikilink">Exit To The Sun</a></p></td>
<td><p>NHK連續劇『』主題歌[172]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/EPIC_DAY.md" title="wikilink">NO EXCUSE</a></p></td>
<td><p><a href="../Page/斯米諾伏特加.md" title="wikilink">Smirnoff</a>／麒麟啤酒「<a href="../Page/斯米诺伏特加.md" title="wikilink">斯米诺伏特加</a>」廣告曲[173]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p><a href="../Page/有頂天.md" title="wikilink">有頂天</a></p></td>
<td><p>日本電視台系連續劇『<a href="../Page/學校的階梯_(電視劇).md" title="wikilink">學校的階梯</a>』主題歌</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/EPIC_DAY.md" title="wikilink">アマリニモ</a></p></td>
<td><p>「花編」「ハウステンボス編」廣告曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/EPIC_DAY.md" title="wikilink">Las Vegas</a></p></td>
<td><p>『<a href="../Page/紅牛特技飛行世界錦標賽.md" title="wikilink">Red Bull Air Race Chiba 2015</a>』主題曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/EPIC_DAY.md" title="wikilink">君を気にしない日など</a></p></td>
<td><p><a href="../Page/豪斯登堡.md" title="wikilink">豪斯登堡</a>「111万本のバラ祭り」廣告曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/RED.md" title="wikilink">RED</a></p></td>
<td><p><a href="../Page/SKY_PerfecTV!.md" title="wikilink">SKY PerfecTV!</a> 「もっと、ドキドキな毎日『黒田篇』」廣告曲[174]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>日本電視台系『{{link-ja|SUKKIRI</p></td>
<td><p>|スッキリ</p></td>
<td><p>}}』6月主題曲</p></td>
</tr>
<tr class="odd">
<td><p>ultra soul</p></td>
<td><p>朝日電視台系<a href="../Page/2015年世界游泳錦標賽.md" title="wikilink">2015年俄羅斯喀山世界游泳錦標賽播放主題曲</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016年</p></td>
<td><p><a href="../Page/世界はあなたの色になる.md" title="wikilink">世界はあなたの色になる</a></p></td>
<td><p>東寶系電影『<a href="../Page/名偵探柯南：純黑的惡夢.md" title="wikilink">名偵探柯南：純黑的惡夢</a>』主題歌[175]</p></td>
</tr>
<tr class="odd">
<td><p>讀賣電視台・日本電視台系動畫『名偵探柯南』片頭曲</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/聲明/Still_Alive.md" title="wikilink">フキアレナサイ</a></p></td>
<td><p>東映系電影『<a href="../Page/疾風迴旋曲#電影.md" title="wikilink">疾風迴旋曲</a>』主題歌[176]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017年</p></td>
<td><p><a href="../Page/聲明/Still_Alive.md" title="wikilink">Still Alive</a></p></td>
<td><p>TBS系連續劇『<a href="../Page/A_LIFE～深愛的人～.md" title="wikilink">A LIFE〜深愛之人〜</a>』主題歌[177]</p></td>
</tr>
<tr class="even">
<td><p>TBS系『』6・7月度片尾曲<ref name="still">{{Cite web |url=<a href="https://bz-vermillion.com/news/170601.html">https://bz-vermillion.com/news/170601.html</a> |title=B'z「Still Alive」TBS各番組 6・7月度テーマソングに決定</p></td>
<td><p>|date=2017-06-01 |accessdate=2019-01-27}}</ref></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>TBS『イベントGO！』6月度片頭曲[178]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>TBS系『』6・7月度片頭曲[179]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/聲明/Still_Alive.md" title="wikilink">声明</a></p></td>
<td><p><a href="../Page/悠詩詩上島咖啡.md" title="wikilink">UCC</a>「UCC BLACK無糖黑咖啡」電視廣告曲[180]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>ultra soul</p></td>
<td><p>朝日電視台系<a href="../Page/2017年世界游泳錦標賽.md" title="wikilink">2017年布達佩斯世界游泳錦標賽播放主題曲</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/DINOSAUR.md" title="wikilink">CHAMP</a></p></td>
<td><p>B'z×<a href="../Page/7-Eleven.md" title="wikilink">7-Eleven商品博覽會</a> 電視廣告曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>「」電視廣告曲</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2018年</p></td>
<td><p><a href="../Page/DINOSAUR.md" title="wikilink">Dinosaur</a></p></td>
<td><p>電影『<a href="../Page/氣象戰_(電影).md" title="wikilink">氣象戰</a>』日語配音版主題歌[181]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/DINOSAUR.md" title="wikilink">King Of The Street</a></p></td>
<td><p><a href="../Page/光榮特庫摩遊戲.md" title="wikilink">KOEI TECMO GAMES</a>『<a href="../Page/真·三國無雙8.md" title="wikilink">真・三國無雙8</a>』主題曲[182]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>ultra soul</p></td>
<td><p>朝日電視系播放主題曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NEW_LOVE.md" title="wikilink">マジェスティック</a></p></td>
<td><p><a href="../Page/江崎固力果.md" title="wikilink">江崎固力果</a>『<a href="../Page/Pocky.md" title="wikilink">Pocky</a>』電視廣告主題歌[183][184]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NEW_LOVE.md" title="wikilink">WOLF</a></p></td>
<td><p>富士電視台系週一晚間9點連續劇『』主題歌[185]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>WOLF -Ballad Version-</p></td>
<td><p>富士電視台系週一晚間9點連續劇『SUITS/スーツ』插入曲[186]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NEW_LOVE.md" title="wikilink">兵、走る</a></p></td>
<td><p><a href="../Page/大正製藥.md" title="wikilink">大正製藥</a>「<a href="../Page/力保美達.md" title="wikilink">-{zh-tw:力保美達;zh-cn:心跳回忆;zh-hk:力保健}-</a>」<a href="../Page/日本國家橄欖球隊.md" title="wikilink">日本國家橄欖球隊應援歌</a>「最強の自分」篇 廣告曲[187]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2019年</p></td>
<td><p><a href="../Page/NEW_LOVE.md" title="wikilink">デウス</a></p></td>
<td><p><a href="../Page/鈴木_(公司).md" title="wikilink">鈴木</a>「<a href="../Page/鈴木Escudo.md" title="wikilink">-{zh-hk:Vitara;zh-tw:Escudo;}-</a>」妥協なきSUV篇 電視廣告曲</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 腳註

**注釋**  **出典**  **引用日語原文**

## 參考文獻

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 關連項目

  - [暢銷音樂藝人列表](../Page/暢銷音樂藝人列表.md "wikilink")
  - [B'z Loud-Gym](../Page/B'z_Loud-Gym.md "wikilink")

## 外部連結

  -
  -
  -
  -
  -
[\*](../Category/B'z.md "wikilink")
[Category:日本樂團](../Category/日本樂團.md "wikilink")
[Category:日本搖滾樂團](../Category/日本搖滾樂團.md "wikilink")
[Category:硬式搖滾樂團](../Category/硬式搖滾樂團.md "wikilink")
[Category:日本硬式搖滾樂團](../Category/日本硬式搖滾樂團.md "wikilink")
[Category:日本男子演唱團體](../Category/日本男子演唱團體.md "wikilink")
[Category:二人组](../Category/二人组.md "wikilink")
[Category:Being旗下藝人](../Category/Being旗下藝人.md "wikilink")
[Category:日本金唱片大獎邦樂部門獲獎者](../Category/日本金唱片大獎邦樂部門獲獎者.md "wikilink")
[Category:Oricon專輯年榜冠軍獲得者](../Category/Oricon專輯年榜冠軍獲得者.md "wikilink")
[Category:Oricon音樂相關影像作品年榜冠軍獲得者](../Category/Oricon音樂相關影像作品年榜冠軍獲得者.md "wikilink")
[Category:Japan Hot
100年榜冠軍獲得者](../Category/Japan_Hot_100年榜冠軍獲得者.md "wikilink")
[Category:曾舉行五大巨蛋巡迴演唱會的音樂人](../Category/曾舉行五大巨蛋巡迴演唱會的音樂人.md "wikilink")
[Category:曾在日產體育場舉行演唱會的音樂人](../Category/曾在日產體育場舉行演唱會的音樂人.md "wikilink")
[Category:日本金氏世界紀錄保持者](../Category/日本金氏世界紀錄保持者.md "wikilink")
[Category:1988年成立的音樂團體](../Category/1988年成立的音樂團體.md "wikilink")
[Category:1988年日本建立](../Category/1988年日本建立.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.
11.

12.
13.
14.

15. 雖然起初的英語表記為「*TAKAHIRO MATSUMOTO*」，但自1994年發售的14th單曲「[Don't Leave
    Me](../Page/Don't_Leave_Me.md "wikilink")」起變更。

16. 雖然起初的英語表記為「*KOHSHI
    INABA*」，但自1999年發售的26th單曲「[ギリギリchop](../Page/ギリギリchop.md "wikilink")」起變更。

17.

18.
19.
20.
21. 出自[講談社刊](../Page/講談社.md "wikilink")『小室哲哉　深層の美意識（講談社文庫版）』著187P-188P。

22.
23.

24.
25.

26.
27.
28.
29.
30.
31. 出自『Treasure : B'z Chronicle 1988～1998 10th anniversary special
    issue』P26

32. 不過開始被表記在製作人員名單是自1992年發售的4th迷你專輯『[FRIENDS](../Page/FRIENDS_\(B'z專輯\).md "wikilink")』起

33.

34.

35.

36.

37.
38.

39.
40.
41.
42. 「柿落」（）為[日本表演藝術用語](../Page/日本.md "wikilink")，意指一個[劇場落成後的首次公開演出](../Page/劇場_\(建築\).md "wikilink")。

43.

44.

45. 此外，[Billy
    Sheehan除了](../Page/Billy_Sheehan.md "wikilink")「ギリギリchop（Version
    51）」以外，亦參加了標題曲「Brotherhood」「流れゆく日々」「イカせておくれ\!」「SHINE」。

46.

47.

48.

49.

50.

51.
52.

53.

54.

55.

56. 亦參加了17th專輯『[MAGIC](../Page/MAGIC_\(B'z專輯\).md "wikilink")』中所收錄的「long
    time no see」。

57.

58.

59.

60.

61.

62.

63.

64.

65.

66.

67.

68.

69.

70.

71.

72.

73.

74.

75.

76.

77.

78.

79.

80.

81.

82.

83.

84.

85.

86.

87.
88.

89.
90.

91.

92.

93.
94. 「B'zが米ロック殿堂入り\!アジアのミュージシャンで初の快挙」 『』2007年9月21日

95.

96.

97.

98.

99.

100.

101.

102.
103.
104.

105.
106.

107.
108.

109.
110.

111.
112.
113.
114.
115.

116.
117.
118.
119.

120.

121.
122.

123.
124.

125.

126.

127.

128.

129.

130.

131.

132.

133.

134.

135.
136.
137.

138.
139.
140.

141.

142.

143.

144.

145.

146.

147.

148.

149.

150.

151.

152.

153. 雖在Being的行動網站內，B'z的[音樂唱片分類表記為](../Page/音樂唱片分類.md "wikilink")[南夢宮](../Page/南夢宮.md "wikilink")（[萬代南夢宮娛樂](../Page/萬代南夢宮娛樂.md "wikilink")），但該處為誤記。

154.
155. 商業搭配時的標題表記為「いつかのメリークリスマス アンプラグド ver.」。

156.

157.

158.

159.

160.

161.

162.

163.
164.

165.

166.

167.

168.

169.

170.

171.

172.

173.

174.

175.

176.

177.

178.
179.
180.

181.

182.

183.

184.

185.

186.

187.