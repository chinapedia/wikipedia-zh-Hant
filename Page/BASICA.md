**BASICA**是「**A**dvanced
**BASIC**」的縮寫，它是[微軟公司為](../Page/微軟.md "wikilink")[PC-DOS寫的一套單純以](../Page/PC-DOS.md "wikilink")[磁碟為基礎的](../Page/磁碟.md "wikilink")[BASIC](../Page/BASIC.md "wikilink")[直譯器](../Page/直譯器.md "wikilink")。

BASICA允許在有[ROM BASIC](../Page/ROM_BASIC.md "wikilink")（此ROM
BASIC能獨自運作，不用載入任何東西）的早期[IBM
PC機型上面使用](../Page/IBM_PC.md "wikilink")，當[DOS被載入後](../Page/DOS.md "wikilink")，它會增加一些存取磁碟上檔案的功能。自從後來的IBM
PC或相容機型，少了ROM BASIC之後，它就無用武之地了。

## 世系

BASICA的開發環境很像在Dartmouth Time Sharing System上面執行[Dartmouth
BASIC](../Page/Dartmouth_BASIC.md "wikilink")。這兩者都有一個[提示符](../Page/提示符.md "wikilink")（prompt），讓[使用者知道可以輸入指令了](../Page/使用者.md "wikilink")。

假如[指令的最前頭是一個](../Page/指令.md "wikilink")[行號](../Page/行號_\(程式語言\).md "wikilink")（此指令的序位，跟[標名](../Page/標名.md "wikilink")—Label的作用一樣），那它就會被插入目前的程式中。假如不是，它就被立刻執行。

BASICA的繼承者是微軟的[GW-BASIC](../Page/GW-BASIC.md "wikilink")，它們非常相似；但後者不需要任何ROM
BASIC上面的[常式](../Page/常式.md "wikilink")，可以在任何IBM PC相容機型上面執行。

## 參見

  -
  - [GW-BASIC](../Page/GW-BASIC.md "wikilink")

  - [QBASIC](../Page/QBASIC.md "wikilink")

  - [QuickBASIC](../Page/QuickBASIC.md "wikilink")

  - [Microsoft BASIC](../Page/Microsoft_BASIC.md "wikilink")

  - [BASIC](../Page/BASIC.md "wikilink")

  - [编程语言](../Page/编程语言.md "wikilink")

  - [程序设计](../Page/程序设计.md "wikilink")

<!-- end list -->

  - [IBM PC](../Page/IBM_PC.md "wikilink")

  - [PC-DOS](../Page/PC-DOS.md "wikilink")

  -
[Category:程序设计语言](../Category/程序设计语言.md "wikilink")
[Category:DOS軟體](../Category/DOS軟體.md "wikilink")