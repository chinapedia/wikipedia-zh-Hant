**電子佈告欄系統**（****，缩写****）是一種網站系統，是目前流行[網路論壇的前身](../Page/網路論壇.md "wikilink")，它允许用户使用终端程序通过[调制解调器拨接或者](../Page/调制解调器.md "wikilink")[因特网来进行连接](../Page/因特网.md "wikilink")，BBS站台提供佈告欄、分類討論區、新闻阅读、軟體下载與上传、遊戲、与其它用户線上對話等功能。

## BBS的歷史

BBS的歷史概分三個階段：

1.  [撥號BBS](../Page/BBS#撥號BBS.md "wikilink")
2.  [Telnet BBS](../Page/BBS#Telnet_BBS.md "wikilink")
3.  [網頁論壇](../Page/BBS#網頁論壇.md "wikilink")

BBS發展初期，BBS站台大部份是由站长（通常被称为SYSOP，SYStem
OPerator，原意为系统操作员）业余维护，其BBS的型態大多是使用者透過[调制解调器拨接或](../Page/调制解调器.md "wikilink")[telnet協議連接BBS站台](../Page/telnet.md "wikilink")，以單色或彩色之**純文字**畫面，在使用者的[終端機上呈現](../Page/終端機.md "wikilink")，中後期開始有一些BBS商業站台提供收费加值服务，隨著[HTTP網路協議及](../Page/HTTP.md "wikilink")[HTML等](../Page/HTML.md "wikilink")[網頁技術的發展](../Page/網頁.md "wikilink")，BBS的型態也轉變為[网络论坛或](../Page/网络论坛.md "wikilink")[網路社群等多元樣貌](../Page/網路社群.md "wikilink")，然而在[台灣](../Page/台灣.md "wikilink")，telnet方式的BBS系統仍然盛行，但由於[调制解调器撥接方式的BBS早已式微](../Page/调制解调器.md "wikilink")，故在台灣的「BBS」一詞，通常是指telnet
BBS系統。

### 撥號BBS

在80年代因特网發展初期，網際網路與寬頻尚未出現，此時是數據機活躍的年代（1980年代至1990年代中期），就开始出现基于调制解调器和电话线通信的[撥接式BBS及其相互连接而成的](../Page/拨号BBS.md "wikilink")[BBS网络](../Page/BBS网络.md "wikilink")。当时BBS站点间所使用的网络协议主要是BNU/[UUCP或是Fidonet通訊協議](../Page/UUCP.md "wikilink")，用戶端透過撥接軟體通过[调制解调器拨叫该BBS站台的电话号码来连接进站](../Page/调制解调器.md "wikilink")，站台內容也全都是文字或由文字所組成的圖形。

隨著撥接技術日趨成熟穩，BBS開始普便流行，大大小小的站台如雨後春筍般出現。在這個時期，此類撥接式BBS的站台主要服務多為：布告欄、檔案／共享軟體下載、討論區，但也僅限站台與使用者的單向服務，尚無法跨越站際，為達成站際交流因此開有一些知名的站際轉信網路組織出現，如最著名[FidoNet](../Page/FidoNet.md "wikilink")（惠多網）國際性業餘組織，BBS站台申請加入該組織，透過Fidonet通訊協議標準，可以達成彼此之間的信件交遞、討論區的共享，甚至達成橫跨洲際國際化網路，如同現今的網際網路。

台灣在1988年正式加入國際惠多網[FidoNet](../Page/FidoNet.md "wikilink")，區域碼為region
56，在撥接BBS發展最盛期約89至96年左右，也出現許多區域性BBS網站組織，例如：90網、AirNet等等，而一個BBS站台通常會加入一個或以上的BBS網站組織。

大陆地区最著名的是[中国惠多网](../Page/中国惠多网.md "wikilink")，曾长期由[金山软件公司](../Page/金山软件公司.md "wikilink")[求伯君的](../Page/求伯君.md "wikilink")“西点”BBS提供[北京](../Page/北京.md "wikilink")-[珠海南北网间长途转信](../Page/珠海.md "wikilink")。

撥接式BBS站台通常架設在DOS系統，而架站所需要的軟體分為

1.  前導程式，較為常見的前導程式有：Frontdoor、Binkleyterm。
2.  伺服端程式，較為常見的伺服端程式有：SuperBBS (SBBS)、ProBoard、Remote Access (RA)、、等。

而用戶端使用撥接程式如：、[Kermit](../Page/Kermit.md "wikilink")、[ProComm](../Page/ProComm.md "wikilink")，經由PC
RS232 port連接Modem
([調制解調器](../Page/調制解調器.md "wikilink"))再經由一般電話撥接到BBS站台，撥接進站之後使用該站台所提供的各類服務，但由於撥接式BBS是採用電話線路連線，長時間使用將使電話費用增高，因此便出現離線軟體，例如讨论區離線软件：蓝波快信（BlueWave）、銀版快信（Silver
Xpress）等，可以将文章按时间、分类、收件人等过滤并压缩打包再传输下載離線閱讀，以提高效率達到節省時間，除此以外还有类似[FTP的文件服务器以及文字](../Page/FTP.md "wikilink")[MUD游戏等软件](../Page/MUD.md "wikilink")。

多数免费BBS只有1台主机、1个调制解调器（modem）和1條电话線路，少数大型免费BBS会拥有2条或3条通信线路，收费（商业）BBS则可能拥有更多线路。由于1条通信线路在同一时间只能为一位用户服务，因此多数站点对用户每天上站的时间作限制，并且根据用户表现授予各种级别，分配不同时段。

### Telnet BBS

由撥接BBS是基於電話線路所形成網路系統，連線成本所費不貲，在1990年代中期[因特网开始大规模普之後](../Page/因特网.md "wikilink")，以[互聯網為基礎的](../Page/互聯網.md "wikilink")[Telnet协议純文字式的BBS](../Page/Telnet协议.md "wikilink")，開始取代掉傳統的撥接BBS。在服务器端，采用Maple
BBS或者FireBird
BBS系统。用户端通过Telnet软件如：[NetTerm](../Page/NetTerm.md "wikilink")、[CTerm](../Page/CTerm.md "wikilink")、[FTerm等来登陆服务器](../Page/FTerm.md "wikilink")，阅读发表文章、发送邮件，通过仿真的协议来上传下载数据文件。有些站点还提供[SSH登陆](../Page/SSH.md "wikilink")，确保连接的安全性，还有很多站点提供Web方式的界面，方便用户使用。

### 網頁論壇

在Telnet
BBS開始取代掉傳統撥接BBS的同時，一樣基於[因特网HTTP協議而發展出來的網頁](../Page/因特网.md "wikilink")[論壇](../Page/論壇.md "wikilink")（Forum）亦開始盛行，由於網頁論壇內容以多媒體網頁方式呈現，內容比傳統純文字式BBS更多豐富與多元，所以網頁論壇亦逐漸取代的Telnet
BBS，在此之後拨号BBS和Telnet
BBS所以形成的BBS网络已经日渐凋零所剩无几，因此在大多數國家或地區，BBS一詞所指稱的討論環境多半已非傳統的純文字式介面，字義已相同或近似於“論壇”。

### 臺灣BBS歷史

  - 1983年 - 在台外僑引進RBBS_PC系統，在台北設置TUGNET（Taiwan User
    Group），此為臺灣撥接式BBS站台的濫觴，由於是外僑的電腦使用者所建，這些BBS僅提供英文的資訊，且當時數據機價格昂貴，因此未能在臺灣引起廣大的迴響。\[1\]
  - 1987年 - 天威視訊首先半實驗性質的推出電子佈告欄系統，該系統程式使用dBASE
    III撰寫，為台灣人所創第一套中文BBS系統，架設於其辦公大樓的區域網路中。同年12月由呂陳蒼林先生建立國內第一座個架設的BBS站——魔教BBS，並由數名對BBS狂熱的用戶大力引進設站程式及相關技術，此時在台灣國內玩家定型的BBS如雨後春筍般紛紛出現。\[2\]
  - 1988年 - 4月台北地區的BBS網已經建立\[3\]，5月臺灣BBS網路正式成為國際業餘FidoNet的新網路區域（region
    56），即臺灣區FidoNet\[4\]，此時台灣約有10個BBS站使用者約1000多人。該年年底，台灣的中南部亦跟隨加入國際惠多網路（FidoNet），BBS站台數暴增至上百站，使用者超過一萬人。\[5\]
  - 1990年 - 撥接式BBS突破200站台，使用人口已超過3萬人\[6\]。
  - 1991年 -
    撥接式BBS已超過500站台，使用者超過十萬人以上，此時正式進入撥接式BBS顛峰期\[7\]，在BBS站際連網，大部份BBS站台大都會加入全球性的國際惠多網路（FidoNet），另也會加入區域性連網，如：90網、臺灣郵網、龍界網、國資網、破解網、旅遊網、柔情網等，除了一般使用者架設的BBS業餘站台，另外也開始出現商業性BBS站，如：白日夢、CADs、NEXT
    BBS等。
  - 1992年 - BBS的發展已不再侷限於個人業餘站台或商業性站台，許多公司、團體、政府機關甚至政黨開始加入BBS架設，如：
      - [玉山銀行](../Page/玉山銀行.md "wikilink")1992年2月率先設立BBS提供財金資訊\[8\]。
      - [中華民國教育部](../Page/中華民國教育部.md "wikilink")1992年7月啟用4線BBS系統站台，以各級學校教師及學生為服務對象，提供教學及休閒功能，並提供輔助教學軟體多達315套，含括各類學科、語文、電腦、繪圖等課程，免費下載學習使用\[9\]。
  - 1992年 -
    [國立中山大學陳年興教授](../Page/國立中山大學.md "wikilink")（openmind）立志推廣台灣的電腦網路，他們以Pirates
    BBS為基礎加以改良，支援Internet
    E-mail與bit-8，建構起全台灣第一個全[中文化的BBS](../Page/中文化.md "wikilink")——中山大學美麗之島站。後來他們將程式寄回給原來的作者，作者再整理發表出來，就成為所謂的Eagle
    BBS\[10\]。Eagle
    BBS採用[開放源始碼方式發行](../Page/開放源始碼.md "wikilink")，而後臺灣各大專院校根據Eagle
    BBS開放源始碼自行修改而衍生出許多BBS系統分支，如：Eagle BBS 3.0、Pivot BBS、NCTU CIS
    BBS、Phoenix BBS等。
  - 1994年 -
    臺灣正大力推行Internet網際網路，由台灣學術網路（TANet）、中華電信局HINet和資策會的SEEDNet構成台灣三大INTERNET網路並與世界接軌，在台灣學術網路（TANet）推行主導下，加上Telnet
    BBS中文化成熟，各大專院校紛紛開始架設Telnet BBS站台，在TANet架構之下的Telnet
    BBS站際之間是即時連線，只要在學校電腦中心便可連入各校際BBS站台，無需透過電話撥接，間接節省可觀的電話費，且Telnet
    BBS站台同時可接受數百人（後期甚至數萬人，如PTT）同時登入，在各大聊天室、討論區便是熱鬧非凡，盛況空前，此時撥接BBS與Telnet
    BBS並行年代，臺灣兩大BBS系統彼此開始嘗試討論區互遞，但由於轉換不穩定，一年之後便告取消。
  - 1996年 - 雖然撥接BBS與Telnet
    BBS所提供的服務並無太大差異，但在節省費用、即時、可登入數量形成群聚效應之下，撥接BBS開始邁入衰退期。
  - 1998年 - 依據Telnet
    BBS全台灣站台統計，大學共計有575站台，科技大學、學院共計有91站台，專校、職校、官校共計有89站台，中學、國中、小學共計有103站台，政府共計有27站台，民間單位計有117站台，全部合計1002站台，此時為Telnet
    BBS高峰期。
  - 2000年 - 隨著WWW盛行，Telnet
    BBS開始邁入衰退期，依2018年統計只剩85有效站台，而使用者幾乎集中至[PTT](../Page/PTT.md "wikilink")。

### 中國大陆BBS歷史

中國大陆使用的论坛，编写语言大致可分为以下几个年代：

  - 1998-2001以[Perl语言为代表性的](../Page/Perl.md "wikilink")[BBS3000](../Page/BBS3000.md "wikilink")、[雷傲](../Page/雷傲.md "wikilink")
  - 2001-2004以[ASP语言为代表性的](../Page/ASP.md "wikilink")[BBSxp](../Page/BBSxp.md "wikilink")、[动网](../Page/动网.md "wikilink")
  - 2004-2012以[PHP语言为代表性的](../Page/PHP.md "wikilink")[Discuz\!](../Page/Discuz!.md "wikilink")、[PHPWind](../Page/PHPWind.md "wikilink")

### 作用

<File:ANSI_art.jpg>|[ANSI](../Page/ANSI.md "wikilink")[ASCII文字藝術](../Page/ASCII.md "wikilink")
<File:Neon2.png>|[數據機撥接方式](../Page/调制解调器.md "wikilink")（Dialup）的BBS
[File:Koala_Country_BBS_Login_Screen.jpg|Koala](File:Koala_Country_BBS_Login_Screen.jpg%7CKoala)
Country BBS登入畫面
[File:Ptt3.jpg|臺灣](File:Ptt3.jpg%7C臺灣)[ptt](../Page/ptt.md "wikilink")
<File:Callisto_screenshot.png>|[网络论坛](../Page/网络论坛.md "wikilink")

## BBS软件與技術

BBS軟體依連線技術區分三大類：撥接式、Telnet、Web，目前BBS泛指Telnet形式。

### 撥接式

撥接式通常是以80年初期到90年DOS系統連接BBS必需使用的軟體，由於DOS對[網際協議](../Page/網際協議.md "wikilink")[TCP/IP支援不完整](../Page/TCP/IP協議.md "wikilink")，因此電腦與電腦之間的連線，是透過數據機（MODEM）連線傳遞訊號，而彼此之間通訊協議採用Fidonet所架構出來的網路技術，此技術分為：FidoNet技術標準（FTS，FidoNet
Technical Standards）與FidoNet標準委員會（FSC，FidoNet Standards Committee）兩大類。

#### 客户端軟體

##### 撥接軟體

  -
  -
  -
##### 離線讀信軟體

  -
  - 銀版快信（Silver Xpress）\[11\]

#### 架站軟體

##### 前導程式

  -
  - [Binkleyterm](../Page/Binkleyterm.md "wikilink")

##### 架站軟體

  -
  -
  -
  -
  -
  - [ProBoard](../Page/ProBoard.md "wikilink")

### Telnet

#### 客户端軟體

目前常见的telnet客户端軟體：

  - [NetTerm](../Page/NetTerm.md "wikilink")：由[InterSoft,
    International這家公司所開發的](../Page/InterSoft,_International.md "wikilink")[共享軟體](../Page/共享軟體.md "wikilink")。在視窗環境亦為知名的telnet程式，近年來，也配合SSH協定的發展，推出另一套名為[SecureNetTerm的程式](../Page/SecureNetTerm.md "wikilink")。
  - [FTerm](../Page/FTerm.md "wikilink")：支持SSH及代理连接。功能强大（最新版2.5稳定版2.4）。
  - [STerm](../Page/STerm.md "wikilink")：支持SSH及代理连接、脚本功能强大。
  - [CTerm](../Page/CTerm.md "wikilink")：（原Cterm2000版曾在大陆非常流行）有大量快捷键，最新版本支持SSH，最新版本是CTerm
    3.6.1。
  - [Qterm](../Page/Qterm.md "wikilink")：這是一個非常優異的客戶端程式，用Qt寫成的，是同類軟體中適用平台最廣的（Windows、Linux、Solaris和Mac
    OS X等）
  - [PCMan](../Page/PCMan.md "wikilink")：有繁体及简体版本，以档案体积小，不占资源闻名，是繼KKman后最通行的浏览器。另有PCManX版本可在非Microsoft
    Windows平台执行（包括Linux/FreeBSD和Mac OS X），還有可內嵌在[Mozilla
    Firefox瀏覽器當中執行的plug](../Page/Mozilla_Firefox.md "wikilink")-in（有Windows
    & Linux版本）。
  - [KKman](../Page/KKman.md "wikilink")：第一个结合telnet与http分页浏览的综合浏览器。另附有ANSI彩色编辑器、表情符號快捷鍵、防闲置等功能，并使用hyper-link，讓使用者可以用滑鼠操作telnet介面。支援SSH1。
  - [BBman](../Page/BBman.md "wikilink")：以跨平台为诉求的telnet浏览器，为目前非Windows平台最佳的telnet客户端程式之一。
  - [PuTTY](../Page/PuTTY.md "wikilink")：於Windows平台上的開放原始碼[Telnet](../Page/Telnet.md "wikilink")／[SSH](../Page/SSH.md "wikilink")／[rlogin客戶端軟體](../Page/rlogin.md "wikilink")。支援SSH
    Tunnel功能。有一衍生版本為[PieTTY](../Page/PieTTY.md "wikilink")。
  - [MultiTerm / MultiTerm
    Pro](../Page/MultiTerm.md "wikilink")：在Windows平台執行的軟體。
  - [AlienBBS](https://web.archive.org/web/20091111222105/http://iim.nctu.edu.tw/~toki/AlienBBS/)：支援[蘋果電腦](../Page/蘋果電腦.md "wikilink")[Mac
    OS X的telnet客戶端](../Page/Mac_OS_X.md "wikilink")，惟已停止開發。
  - Nally：支援[Mac OS X
    Leopard](../Page/Mac_OS_X_10.5.md "wikilink")、[Unicode補完計畫的開放原始碼telnet用戶端軟體](../Page/Unicode補完計畫.md "wikilink")。
  - [Welly](../Page/Welly.md "wikilink")：核心衍生自Nally，並加入許多特有功能如自動登入、全文下載、IP辯識等。
  - [ZTerm](http://zhouer.org/ZTerm/):以Java撰寫，可跨平台並支援SSH連線。亦有人改寫成Java
    Applet版本供網頁嵌入。
  - 在[UNIX環境下](../Page/UNIX.md "wikilink")，透過[telnet程式就可連接BBS](../Page/telnet.md "wikilink")，當然也可以使用Qterm，大部份都是透過[主控台](../Page/主控台.md "wikilink")（console）或[終端機](../Page/終端機.md "wikilink")（Terminal）程式，執行telnet程式連接BBS，透過終端機模擬和控制畫面。這類終端機包含：[iTerm](../Page/iTerm.md "wikilink")、[Terminal](../Page/Terminal_\(Apple\).md "wikilink")、[xterm](../Page/xterm.md "wikilink")、[rxvt](../Page/rxvt.md "wikilink")、[Konsole](../Page/Konsole.md "wikilink")、gnome-terminal。另外Firefox瀏覽器也有專門開啟BBS的擴充套件，如：BBSFox等

#### 服务器端软件

台灣BBS界最早期是以Pirates BBS為基礎加以改良成為Engle BBS為主，再加以修改衍生出BBS系統分支，如：\[12\]

  - PirateBBS 和 EagleBBS 這兩個版本是台灣學術網路BBS的始祖
  - Phoenix - 是由交大資工由 EagleBBS 所發展出來的程式
  - PalmBBS - 是台大計中的 BBS 所採用的系統，也是同樣由台大自行發展維護的
  - PowerBBS - 是目前台灣學術網路上使用 Client-Server 架構最成熟的一個BBS系統
  - PivotBBS - 是由中興大學計中所發展的BBS系統，也是由 EagleBBS 修改而來
  - SecretBBS - 是大同工學院資工系發展的BBS系統，是由 PhoenixBBS 3.0 演變而來
  - MapleBBS - 是清華大學資訊科學系所自SecretBBS發展過來的BBS系統，這個版本的BBS系統在記憶體的使用方面較為節省
  - NapoleonBBS - 是由交大工管系的同學自行修改的，這版結合了 PhoenixBBS 、 SecretBBS 、
    FirebirdBBS 各版的功能
  - FeelingBBS - 是中正大學計中所發展的BBS，這版承續了PivotBBS的原始程式繼續發展
  - [Firebird BBS](../Page/Firebird_BBS.md "wikilink") -
    是中正大學資工系所修改的，大致上還是架構在Phoenix4.0版上面
  - FormosaBBS - 是中山大學的BBS系統，和上列的各種版本有些許的差異
  - WindTopBBS - 是元智資工以MapleBBS修改而成的
  - 以Maple 3為基礎發展的
    [itoc系列](https://web.archive.org/web/20170702223850/http://processor.tfcis.org/~itoc/)
  - 以Maple 2.36為基礎發展的 [Ptt BBS](../Page/批踢踢.md "wikilink") 系列
  - 使用[Java語言所寫成的](../Page/Java.md "wikilink")
    [ColaBBS](../Page/ColaBBS.md "wikilink") （曾停止維護，現已由原作者吳英豪開放源碼，收集在
    [ColaBBS](https://sourceforge.net/projects/colabbs/) ）。

中国大陆BBS界在[Firebird BBS基础上还发展了](../Page/Firebird_BBS.md "wikilink")[Smth
BBS](../Page/水木清华BBS.md "wikilink")、[Ytht
BBS](../Page/一塌糊涂BBS.md "wikilink")、[Lily
BBS等](../Page/南大小百合BBS.md "wikilink")，提供非常丰富Web方式访问，如发文、[即时消息](../Page/即时消息.md "wikilink")、信件、[Blog](../Page/Blog.md "wikilink")，而部分管理操作则仍然限定为Telnet访问。而客户端的Telnet软件也发展了很多便于操作的功能，如，鼠标响应、URL识别、图片预览，文章自动下载、自定义脚本等。

在Web介面的论坛程序中，大陆地区开发的主要有基于[Perl语言的](../Page/Perl.md "wikilink")[BBS3000](../Page/BBS3000.md "wikilink")、[雷傲](../Page/雷傲.md "wikilink")、基于[ASP语言的](../Page/ASP.md "wikilink")[BBSxp](../Page/BBSxp.md "wikilink")、[动网](../Page/动网.md "wikilink")、基于语言[PHP的](../Page/PHP.md "wikilink")**[Discuz\!](../Page/Discuz!.md "wikilink")**、[PHPWind及](../Page/PHPWind.md "wikilink")[phpBB](../Page/phpBB.md "wikilink")、基于[Java语言的](../Page/Java.md "wikilink")[Jute
Forum](../Page/Jute_Forum.md "wikilink")。

### 其他軟體

由於BBS發展初期是經由電話撥接上線，如同一般市話通話，隨時上線時越久其收費越高昂，因此「壓縮軟體」的出現縮短了檔案傳輸時間，以達到節省電話費，早期常見的「壓縮軟體」如：

  - [ZIP](../Page/ZIP格式.md "wikilink")
  - [ARJ](../Page/ARJ.md "wikilink")
  - [RAR](../Page/RAR.md "wikilink")
  - [LZH](../Page/LHA_\(檔案格式\).md "wikilink")

## BBS人文文化

### 中国BBS“系统维护”现象

某些特殊时期，[中国的许多BBS会不约而同的出现大面积限制访问现象](../Page/中国.md "wikilink")，通常公布的理由为“系统维护”。例如，2005年3月，[中华人民共和国政府召开](../Page/中华人民共和国.md "wikilink")“两会”前夕，有以下BBS同时宣布进入“系统维护”状态：

  - 西安交大[兵马俑BBS站](../Page/兵马俑BBS.md "wikilink")：3月5日起限制校外访问
  - [白云黄鹤BBS站](../Page/白云黄鹤BBS.md "wikilink")：夜间只读及暂停校外IP注册
  - [北邮真情流露BBS站](../Page/北邮真情流露BBS.md "wikilink")：关闭[telnet的](../Page/telnet.md "wikilink")23端口以及[http服务](../Page/http.md "wikilink")
  - [饮水思源BBS站](../Page/饮水思源BBS.md "wikilink")：3月5日服务器升级
  - [水木清华BBS站](../Page/水木清华BBS.md "wikilink")：系统维护通告

### “资料洩漏”现象

BBS站的站长拥有很大的权力，因为是由纯文字组合而成，有权限的人甚至不用任何基础就能轻易查看他人资料\[13\]。

### BBS用語

## 参看

  - [網絡論壇](../Page/網絡論壇.md "wikilink")
  - [BBS鄉民的正義](../Page/BBS鄉民的正義.md "wikilink")
  - [批踢踢](../Page/批踢踢.md "wikilink")

## 參考文獻

## 外部链接

  - [已知現存中文 BBS
    站台](https://hackmd.io/s/r1pdt-59b#%E5%B7%B2%E7%9F%A5%E7%8F%BE%E5%AD%98%E4%B8%AD%E6%96%87-BBS-%E7%AB%99%E5%8F%B0)
    - 台灣、中國大陸、全球華文 BBS 列表。
  - [台灣目前常見的BBS列表](http://ptt-kkman-pcman.org/bbs.html) -
    包含常見的BBS網址與使用軟體。
  - [台灣過去BBS站台及討論區列表](https://hackmd.io/s/rk91dlzSm#)

[BBS](../Category/BBS.md "wikilink")
[Category:互联网](../Category/互联网.md "wikilink")

1.  Internet/Fidonet網路技術實務v1.00 PD version, released on Jan 20, 1995

2.  [BBS,E-Mail,MSN,IM,ICQ,Skype網路人際關係，南華出版所研究生，洪宗慶、蕭嘉農](http://mail.nhu.edu.tw/~society/e-j/46/46-03.htm)

3.  [傳說中的Honlin](http://honlin.com/2007/04/14/bbs-%E4%BB%80%E9%BA%BC%E6%98%AFbbs/)

4.
5.
6.

7.

8.

9.

10. [台灣BBS發展簡史](http://content.edu.tw/senior/computer/ks_ks/comsense/bbs.htm)

11. [銀版快信（Silver
    Xpress）](http://www.santronics.com/products/sxpress/index.php)

12.

13.