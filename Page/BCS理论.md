**BCS理论**是解释[常规](../Page/常规.md "wikilink")[超导体的](../Page/超导体.md "wikilink")[超导电性的微观理论](../Page/超导电性.md "wikilink")（所以也常意译为**超导的微观理论**）。该理论以其发明者[约翰·巴丁](../Page/约翰·巴丁.md "wikilink")、[利昂·库珀和](../Page/利昂·库珀.md "wikilink")[约翰·施里弗的名字首字母命名](../Page/约翰·施里弗.md "wikilink")。

## 理論

某些[金属在极低的](../Page/金属.md "wikilink")[温度下](../Page/温度.md "wikilink")，其[电阻会完全消失](../Page/电阻.md "wikilink")，[电流可以在其间无损耗的流动](../Page/电流.md "wikilink")，这种现象称为[超导](../Page/超导.md "wikilink")。超导现象于1911年发现，但直到1957年，巴丁、库珀和施里弗提出BCS理论，其微观机理才得到一个令人满意的解释。BCS理论把超导现象看作一种[宏观](../Page/宏观.md "wikilink")[量子效应](../Page/量子效应.md "wikilink")。它提出，金属中[自旋和](../Page/自旋.md "wikilink")[动量相反的](../Page/动量.md "wikilink")[电子可以配对形成所谓](../Page/电子.md "wikilink")“[库珀对](../Page/库珀对.md "wikilink")”，库珀对在晶格当中可以无损耗的运动，形成超导电流。在BCS理论提出的同时，[尼科莱·勃格留波夫](../Page/尼科莱·勃格留波夫.md "wikilink")（Nikolay
Bogolyubov）也独立的提出了超导电性的[量子力学解释](../Page/量子力学.md "wikilink")，他使用的（Bogoliubov
transformation）至今为人常用。

\(E=3.52k_BT_c\sqrt{1-(T/T_c)}\)

电子间的直接相互作用是相互排斥的[库伦力](../Page/库伦力.md "wikilink")。如果仅仅存在库伦力直接作用的话，电子不能形成配对。但电子间还存在以[晶格](../Page/晶格.md "wikilink")[振动](../Page/振动.md "wikilink")（[声子](../Page/声子.md "wikilink")）为媒介的间接相互作用：[電聲子交互作用](../Page/電聲子交互作用.md "wikilink")。电子间的这种相互作用在满足一定条件时，可以是相互吸引的，正是这种吸引作用导致了“库珀对”的产生。大致上，其机理如下：电子在晶格中移动时会吸引邻近格点上的[正电荷](../Page/正电荷.md "wikilink")，导致格点的局部畸变，形成一个局域的高正电荷区。这个局域的高正电荷区会吸引自旋相反的电子，和原来的电子以一定的[结合能相结合配对](../Page/结合能.md "wikilink")。在很低的温度下，这个结合能可能高于晶格[原子振动的](../Page/原子.md "wikilink")[能量](../Page/能量.md "wikilink")，这样，电子对将不会和晶格发生能量交换，也就没有电阻，形成所谓“超导”。

## 貢獻

巴丁、库珀、施里弗因此获得1972年的[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")。不過[BCS理論并無法成功的解釋所謂第二類超導](../Page/BCS理論.md "wikilink")，或[高溫超導的現象](../Page/高溫超導.md "wikilink")。

## 参见

  - [超导](../Page/超导.md "wikilink")

## 拓展阅读

  - 关于BCS理论的介绍，可以参见施里弗所著的面向研究生的优秀教材
  - ScienceDaily: [Physicist Discovers Exotic
    Superconductivity](http://www.sciencedaily.com/releases/2006/08/060817101658.htm)（[University
    of Arizona](../Page/University_of_Arizona.md "wikilink")）August 17,
    2006
  - [Hyperphysics page on
    BCS](http://hyperphysics.phy-astr.gsu.edu/hbase/solids/bcs.html)
  - [BCS
    History](http://ffden-2.phys.uaf.edu/212_fall2003.web.dir/T.J_Barry/bcstheory.html)
  - [Dance
    Analogy](http://www.aip.org/history/mod/superconductivity/03.html)
    of BCS theory as explained by Bob Schrieffer (audio recording)

[Category:超导](../Category/超导.md "wikilink")