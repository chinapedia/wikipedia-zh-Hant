**BPM短波授时台**是[中华人民共和国的国家短波](../Page/中华人民共和国.md "wikilink")[授时台](../Page/授时.md "wikilink")，位于[陕西省](../Page/陕西省.md "wikilink")[蒲城县](../Page/蒲城县.md "wikilink")[临潼区东北约](../Page/临潼区.md "wikilink")70千米处，隶属[中国科学院](../Page/中国科学院.md "wikilink")[陕西天文台](../Page/陕西天文台.md "wikilink")\[1\]。一并广播的还有100
kHz [BPL长波授时台](../Page/BPL长波授时台.md "wikilink")。

它于1987年7月1日开始广播，
BPM以与美國[WWV和](../Page/WWV_\(无线电台\).md "wikilink")[WWVH授时台相同的](../Page/WWVH.md "wikilink")2.5、5、10和15[兆赫频率广播](../Page/兆赫.md "wikilink")，时刻表如下：\[2\]

| MHz | [UTC](../Page/UTC.md "wikilink") | [CST](../Page/中国标准时间.md "wikilink") |
| --- | -------------------------------- | ----------------------------------- |
| 2.5 | 7:30-1:00                        | 15:30-9:00                          |
| 5   | 0:00-24:00                       | 0:00-24:00                          |
| 10  | 0:00-24:00                       | 0:00-24:00                          |
| 15  | 1:00-9:00                        | 9:00-17:00                          |

## 发播格式

BPM以半小时为周期发出不同的信号，以1kHz音频调制讯号来提供秒和分的滴答：

| 分钟 | 时长 | 内容 |
| -- | -- | -- |
| 00 | 30 | 10 |
| 10 | 40 | 5  |
| 15 | 45 | 10 |
| 25 | 55 | 4  |
| 29 | 59 | 1  |

BPM广播时间表\[3\]\[4\]

BPM的特点在于它于25分和29分及55分和59分之间发送[UT1时间信号](../Page/UT1.md "wikilink")，如果在更强的时间信号台下，如WWV，收听时就会听到奇怪的两声响。\[5\]

## 参考

## 外部链接

  - [国家授时中心](http://english.ntsc.cas.cn)
  - [短波授时台](https://web.archive.org/web/20150410130817/http://www.time.ac.cn/center/222.html)，中国科学院国家授时中心

[Category:中國時間](../Category/中國時間.md "wikilink")
[Category:时码发播台](../Category/时码发播台.md "wikilink")
[Category:蒲城县](../Category/蒲城县.md "wikilink")

1.

2.

3.
4.

5.