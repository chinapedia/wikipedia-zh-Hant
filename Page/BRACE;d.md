**BRACE;d**，是一隊[J-POP組合](../Page/J-POP.md "wikilink")，於2005年11月25日以動畫《[增血鬼果林](../Page/增血鬼果林.md "wikilink")》的片頭主題曲《scarlet》正式出道\[1\]。主唱[川瀨未希目前是就讀中的女子高中生](../Page/川瀨未希.md "wikilink")。

## 成員

  - [川瀨未希](../Page/川瀨未希.md "wikilink")（[主唱](../Page/主唱.md "wikilink")）
      -
        1989年4月13日出生，[東京都出身](../Page/東京都.md "wikilink")。在試聽選拔中被選中。
  - [a.k.a.dRESS](../Page/a.k.a.dRESS.md "wikilink")（[電子琴](../Page/電子琴.md "wikilink")、[作詞](../Page/作詞.md "wikilink")、[作曲](../Page/作曲.md "wikilink")、[編曲](../Page/編曲.md "wikilink")、[音聲制作](../Page/音聲制作.md "wikilink")）
      -
        「a.k.a」全寫是「as know as」。
        1977年3月10日出生。名稱簡作「dRESS」。[ave;new的音聲制作者](../Page/ave;new.md "wikilink")。
        是個很喜歡喝酒的人\[2\]。

## 音樂

### 單曲

  - scarlet
      -
        c/w veil
        2005年11月25日／FCCM-0092 / [Frontier
        Works](../Page/Frontier_Works.md "wikilink")
        [動畫](../Page/日本動畫.md "wikilink")「[增血鬼果林](../Page/增血鬼果林.md "wikilink")」片頭曲
  - Devotion
      -
        c/w “missing”
        2006年4月21日／FCCM-0120 / Frontier Works
        動畫「[甜蜜聲優](../Page/甜蜜聲優.md "wikilink")」片尾曲

### 合作

  - ave;new feat.BRACE;d「Allure」
      -
        ave;new 專輯「virtu」收錄
        2006年5月31日
  - ave;new feat.BRACE;d「Allure -HARD dRESS STYLE-」
      -
        ave;new 專輯「Regard」收錄
        2006年8月11日

## 註

<div class="references-small">

<references />

</div>

## 外部連結

  - [BRACE;d official
    site](https://web.archive.org/web/20070216074507/http://www.brace-d.com/)（久未更新）
  - [ave;new 上川瀨未希的個人資料（附照片）](http://www.avenew.jp/staff/miki.html)

[ja:Ave;new\#BRACE;d](../Page/ja:Ave;new#BRACE;d.md "wikilink")

[BRACE;d](../Category/日本演唱團體.md "wikilink")
[BRACE;d](../Category/動畫歌手.md "wikilink")

1.  [](http://bb.watch.impress.co.jp/cda/news/11528.html)
2.  [](http://www.dengekionline.com/g-net/strike/200510dress/gn20051014_strike_dress.htm)