[Bröderna_Ivarsson_i_Osby.jpg](https://zh.wikipedia.org/wiki/File:Bröderna_Ivarsson_i_Osby.jpg "fig:Bröderna_Ivarsson_i_Osby.jpg")
**BRIO**是一家成立於[瑞典的](../Page/瑞典.md "wikilink")[玩具公司](../Page/玩具.md "wikilink")。創立者[伊瓦·本特森](../Page/伊瓦·本特森.md "wikilink")（Ivar
Bengtsson）原先是從事籃子製造，後在瑞典南部的[奧斯比](../Page/奧斯比.md "wikilink")（Osby）轉做玩具製造。在1908年，伊瓦的三個兒子繼承其事業並成立
BRIO ，是 Bröderna Ivarsson \[at\] Osby
的縮寫，意即在奧斯比的伊瓦兄弟。時至今日，所生產的玩具仍以原木為材料，一世紀始終在奧斯比從事玩具的設計。

在1984年該公司創立了 BRIO Lekoseum ，一個以 BRIO
玩具為主軸的玩具博物館（同時有像[馬克林](../Page/馬克林.md "wikilink")（Märklin）或是[芭比娃娃等公司](../Page/芭比娃娃.md "wikilink")），位於BRIO在奧斯比的總部，孩童們可以自由地嘗試與把玩。

## 產品

[Brio_puujunarata.jpg](https://zh.wikipedia.org/wiki/File:Brio_puujunarata.jpg "fig:Brio_puujunarata.jpg")
一些BRIO著名的產品：

  - 自1958年開始於歐洲銷售的[木製玩具火車](../Page/木製玩具火車.md "wikilink")。它們適合年齡較小的孩童，當時並不使用電力。隨著時間推進，各種動力方式紛紛出現，如：電池、遙控、高智能的軌道。
  - BRIO擁有[湯姆仕火車](../Page/湯姆仕火車.md "wikilink")（Thomas the Tank
    Engine）在部分歐洲地區的銷售權。
  - 長期以來，薄木縫翼與均勻間隔洞是用一種五顏六色塑膠扣件（fasteners）連接在一起的。年幼的孩童也能建立穩固的、細密的鐵路系統。

許多競爭者，如[削徑鐵路公司](../Page/削徑鐵路.md "wikilink")（Whittle
Shortline）便生產合乎BRIO尺寸的軌道。而BRIO在瑞典製造且使用高品質木材使得BRIO的玩具比起市價來的較高。

## 相關網站

  - [BRIO
    US](https://web.archive.org/web/20071219163141/http://brio.knex.com/)
    – *BRIO美國官方網站*
  - [BRIO corporate homepage](http://www.brio.net/) – *各國家與語言的BRIO網站*
  - 在[www.brio.net](http://www.brio.net) 搜尋"Lekoseum"便能線上遊覽"Brio玩具博物館"。
  - [Wooden Train
    Manufacturers](http://magicref.tripod.com/trains/trainlnk.htm#toc) –
    ''其他與BRIO合作生產木火車的公司網站

[Category:玩具](../Category/玩具.md "wikilink")
[Category:瑞典公司](../Category/瑞典公司.md "wikilink")