[TBS_akasaka.jpg](https://zh.wikipedia.org/wiki/File:TBS_akasaka.jpg "fig:TBS_akasaka.jpg")

**BS-TBS**是隸屬於[TBS電視的](../Page/TBS電視.md "wikilink")[衛星電視委託放送事業者](../Page/衛星電視.md "wikilink")，為[日本新聞網集團的成員](../Page/日本新聞網.md "wikilink")。

## 沿革

  - 1998年（平成10年）11月 - 成立**株式会社 Japan Digital Communications**
  - 1999年（平成11年）1月 - Japan Digital
    Communications加入[日本民間放送聯盟](../Page/日本民間放送聯盟.md "wikilink")
  - 2000年（平成12年）6月 - Japan Digital Communications更改名稱為**株式会社BS-i**
  - 2000年（平成12年）12月1日11:00 - BS-i開播
  - 2004年（平成16年）7月 - BS-i總部由TBS放送会館遷移至TBS放送中心15樓
  - 2005年（平成17年）9月30日 - BS-i結束廣播節目
  - 2009年（平成21年）4月1日 - BS-i公司名稱更改為**株式会社BS-TBS**，頻道名改為BS-TBS。
  - 2018年（平成30年）12月1日 - BS-TBS 4K开播。

## 頻道細節

### 電視廣播

  -
    被分配到BS的161～163頻道。Remote Control Key ID是**6**。
    在開台以來、免費放送的BS數位電視台中、唯一一家將節目分成為三個頻道播送、於2005年2月11日結束。
    （（日本時間）週一～週五 14:00～14:55、161ch=Television
    Pink、162ch=「i-collection」、163ch=放送休止）
    按下遙控器的d鈕、能夠看節目資訊。（廣告中及連動資訊播送時段除外）
    登錄會員有提供雙向性服務，内容有新聞・氣象情報・J聯盟情報。
    字幕放送（文字多重放送）有2時間Suspense drama（「Mystery
    selection」是為了TBS在地面播送的「月曜Mystery劇場」而重播）。

#### 夜晚播出

**播放完畢**

  - [AIR](../Page/AIR.md "wikilink")
  - [這醜陋又美麗的世界](../Page/這醜陋又美麗的世界.md "wikilink")
  - [我的主人愛作怪](../Page/我的主人愛作怪.md "wikilink")
  - [真月譚月姬](../Page/真月譚月姬.md "wikilink")
  - [Popotan](../Page/Popotan.md "wikilink")
  - [魔力女管家–更美麗的事物](../Page/魔力女管家.md "wikilink")
  - [ゆめりあ](../Page/ゆめりあ.md "wikilink")
  - [009-1](../Page/009-1.md "wikilink")
  - [×××HOLiC](../Page/×××HOLiC.md "wikilink")
  - [黑貓](../Page/黑貓.md "wikilink")
  - [幸運女神](../Page/幸運女神.md "wikilink")
  - [幸運女神–各自的翅膀](../Page/幸運女神.md "wikilink")
  - [後天的方向](../Page/後天的方向.md "wikilink")
  - [備長炭](../Page/備長炭_\(動畫\).md "wikilink")
  - [REC](../Page/REC.md "wikilink")
  - [草莓棉花糖](../Page/草莓棉花糖.md "wikilink")
  - [冬季花園](../Page/冬季花園.md "wikilink")（特別篇）
  - [英國戀物語艾瑪](../Page/艾瑪_\(漫畫\).md "wikilink")（重播[UHF
    Anime已播的動畫](../Page/UHF_Anime.md "wikilink")）
  - [Chobits](../Page/Chobits.md "wikilink")（重播原在白天播出的時段）
  - [Heat Guy-J](../Page/Heat_Guy-J.md "wikilink")
  - [忘卻的旋律](../Page/忘卻的旋律.md "wikilink")
  - [薔薇少女](../Page/薔薇少女.md "wikilink")
  - [薔薇少女 ～夢見～](../Page/薔薇少女.md "wikilink")
  - [薔薇少女 ～序曲～](../Page/薔薇少女.md "wikilink")（特別篇）
  - [夜明前的琉璃色 Crescent Love](../Page/夜明前的琉璃色.md "wikilink")
  - [Venus Versus Virus](../Page/Venus_Versus_Virus.md "wikilink")
  - [向陽素描](../Page/向陽素描.md "wikilink")
  - [Kanon](../Page/Kanon.md "wikilink")※[京都動畫製作](../Page/京都動畫.md "wikilink")
  - [魔力女管家](../Page/魔力女管家.md "wikilink")
  - [怪物王女](../Page/怪物王女.md "wikilink")
  - [CLANNAD](../Page/CLANNAD.md "wikilink")
  - [K-ON\!](../Page/K-ON!.md "wikilink")

另外，BS-i只在深夜播放的動畫多與UHF Anime相似。

#### 白天播出

  - [機動戰士Gundam 0083:Stardust
    Memory](../Page/機動戰士Gundam_0083:Stardust_Memory.md "wikilink")（[OVA](../Page/OVA.md "wikilink")・2006年11月19日開始播放）

**播放完畢**

  - [MUSASHI GUN道](../Page/MUSASHI_GUN道.md "wikilink")
  - [RGBアドベンチャー](../Page/RGBアドベンチャー.md "wikilink")
  - [小小雪精靈](../Page/小小雪精靈.md "wikilink")
  - [秋葉原電腦組](../Page/秋葉原電腦組.md "wikilink")
  - [魔術師歐菲](../Page/魔術師歐菲.md "wikilink")
  - [機動戰士鋼彈0080:口袋裡的戰爭](../Page/機動戰士鋼彈0080:口袋裡的戰爭.md "wikilink")（OVA）

## 外部連結

  - [BS-TBS](http://www.bs-tbs.co.jp/)

[B](../Category/1998年建立.md "wikilink")
[B](../Category/東京放送控股.md "wikilink")
[B](../Category/日本衛星電視頻道.md "wikilink") [Category:港區公司
(東京都)](../Category/港區公司_\(東京都\).md "wikilink")
[Category:赤坂](../Category/赤坂.md "wikilink")