**株式會社BS富士**（登記的全名為****）為[富士電視台關係企業](../Page/富士電視台.md "wikilink")，主要業務為[BS數位放送](../Page/衛星電視.md "wikilink")，經營BS電視頻道（BS第181至183頻道）與BS電台頻道（BS第488至489頻道，皆已於2006年3月結束營業）。

## 沿革

  - 1998年12月15日－**株式會社BS富士**設立。
  - 2000年12月1日[日本時間午前](../Page/日本時間.md "wikilink")11時－BS富士正式開播。
  - 2006年1月－BS富士成為民營BS[電視台中第一個實現財務](../Page/電視台.md "wikilink")[盈余的電視台](../Page/盈余.md "wikilink")。

## 外部連結

  - [BS FUJI](http://www.bsfuji.tv/top/)

[Category:富士電視台](../Category/富士電視台.md "wikilink")
[Category:1998年建立](../Category/1998年建立.md "wikilink")
[Category:日本衛星電視頻道](../Category/日本衛星電視頻道.md "wikilink")
[Category:港區公司 (東京都)](../Category/港區公司_\(東京都\).md "wikilink")