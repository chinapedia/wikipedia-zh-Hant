**株式會社BS日本**（****）是[日本的一家](../Page/日本.md "wikilink")[廣播衛星](../Page/衛星電視.md "wikilink")（BS）電視公司，為[日本電視台](../Page/日本電視台.md "wikilink")、[NNN的關聯組織](../Page/NNN.md "wikilink")，頻道名稱為「**BS日視**」（****），成立於1998年12月2日，開播于2000年12月1日，總部位于[東京都](../Page/東京都.md "wikilink")[千代田區日本電視台麴町分室](../Page/千代田區.md "wikilink")（日本電視台總部舊址，現為BS日本與[CS日本總部所在地](../Page/CS日本.md "wikilink")），為日本最早的一家實行[24小時播出的](../Page/24小時.md "wikilink")[民放](../Page/民放.md "wikilink")[核心局](../Page/核心局.md "wikilink")。

## 歷史

[Nippon_Television_Network_(former_head_office).jpg](https://zh.wikipedia.org/wiki/File:Nippon_Television_Network_\(former_head_office\).jpg "fig:Nippon_Television_Network_(former_head_office).jpg")

  - 1998年12月2日，「株式会社」成立。
  - 2000年12月1日11時（[日本時間](../Page/日本時間.md "wikilink")24時制，以下皆同），BS日視開播。
  - 2000年12月至2005年11月，BS日視[口號為](../Page/口號.md "wikilink")「來看、來看、來玩。BS日視」（）。
  - 2004年6月23日，株式会社改名為「株式会社BS日本」。
  - 2005年9月30日，BS日視結束[電台服務](../Page/電台.md "wikilink")。
  - 2005年12月至2008年3月，BS日視口號為「本物空間　BS日視　4 you」（），其中「4」讀作「for」。
  - 2008年4月1日，BS日視商標部份改版：「BS」字體顏色改為橙色，「日視」（）字體改為與[日本電視控股](../Page/日本電視控股.md "wikilink")（）的「日視」金色字體相同，[吉祥物](../Page/吉祥物.md "wikilink")「Nāndarō」（）的線條顏色改為金色，BS日視商標在畫面上的顯示位置從左上角改為右上角。
  - 2008年4月至今，BS日視口號為「BS4　BS日視」（）。

## 外部連結

  - [BS日視](http://www.bs4.jp/)

[Category:1998年成立的公司](../Category/1998年成立的公司.md "wikilink")
[Category:日本電視台 (電視台)](../Category/日本電視台_\(電視台\).md "wikilink")
[Category:日本衛星電視頻道](../Category/日本衛星電視頻道.md "wikilink")