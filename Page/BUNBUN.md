**BUNBUN**（）是[日本男性](../Page/日本.md "wikilink")[插畫家](../Page/插畫家.md "wikilink")，身高177公分，出身於[京都府](../Page/京都府.md "wikilink")。因負責設計[輕小說](../Page/輕小說.md "wikilink")《[薔薇的瑪利亞](../Page/薔薇的瑪利亞.md "wikilink")》的角色人物而知名。他的姊姊[堀口悠紀子則是一位知名的](../Page/堀口悠紀子.md "wikilink")[原畫師](../Page/原畫師.md "wikilink")。

雖然本人在Twitter上表示他跟[abec並非同一個人](../Page/abec.md "wikilink")\[1\]，但是在台灣角川官網《刀劍神域》系列小說的介紹上則敘述abec也以BUNBUN名義繪製插畫\[2\]，而兩人的簽名會上出現的也都是同一人。在[刀劍神域編輯](../Page/刀劍神域.md "wikilink")[三木一馬的書中正式寫出了確實與](../Page/三木一馬.md "wikilink")[abec老師為同一人的事實](../Page/abec.md "wikilink")，原本[abec是用於私人部落格的暱稱](../Page/abec.md "wikilink")，後在聽了[三木一馬編輯](../Page/三木一馬.md "wikilink")：「我會讓[abec這個名字比](../Page/abec.md "wikilink")[BUNBUN還要出名](../Page/BUNBUN.md "wikilink")！」的建議下，以[abec的名義擔當](../Page/abec.md "wikilink")[刀劍神域的插畫一職](../Page/刀劍神域.md "wikilink")，讓[abec此名稱正式商業出道](../Page/abec.md "wikilink")。\[3\]

## 作品

### 小說插畫

  - 日本

<!-- end list -->

  - （著：上月司／[電擊文庫](../Page/電擊文庫.md "wikilink")）

  - [薔薇的瑪利亞](../Page/薔薇的瑪利亞.md "wikilink")（著：十文字青／[角川Sneaker文庫](../Page/角川Sneaker文庫.md "wikilink")）

  - （著：川上亮／[富士見Mystery文庫](../Page/富士見Mystery文庫.md "wikilink")）

  - [神曲奏界
    黑](../Page/神曲奏界.md "wikilink")（著：[大迫純一](../Page/大迫純一.md "wikilink")／[GA文庫](../Page/GA文庫.md "wikilink")）

  - （著：深山森／[電擊文庫](../Page/電擊文庫.md "wikilink")）

  - 妮娜與兔子與魔法戰車（著：兎月 竜之介／[集英社Super
    Dash文庫](../Page/Super_Dash文庫.md "wikilink")）

  - RINGADAWN（著：あやめゆう／C★NOVELS Fantasia）

  - [鷲尾須美是勇者](../Page/鷲尾須美是勇者.md "wikilink") **（2014）**（著：タカヒロ／[電擊G's
    magazine](../Page/電擊G's_magazine.md "wikilink")）

  - 神兵編年史（著：丈月城／[集英社](../Page/集英社.md "wikilink")）

  - [乃木若葉是勇者](../Page/乃木若葉是勇者.md "wikilink") **（2015）**（著：朱白あおい / [電撃G's
    magazine](../Page/電撃G's_magazine.md "wikilink")）

<!-- end list -->

  - 台灣

<!-- end list -->

  - 七重夜（著：關兮／浮文字）

### 電視動畫

  - [刀劍神域](../Page/刀劍神域.md "wikilink")（2012）
  - [結城友奈是勇者](../Page/結城友奈是勇者.md "wikilink") (2014) (Project2H)
  - [終末的伊澤塔](../Page/終末的伊澤塔.md "wikilink") (2016)
  - [SAKURA QUEST](../Page/SAKURA_QUEST.md "wikilink") (2017)

### 遊戲插畫

  - （[任天堂DS](../Page/任天堂DS.md "wikilink")）
  - （PC）[神曲奏界 THE BLACK](../Page/神曲奏界.md "wikilink")
  - （[SQUARE ENIX](../Page/SQUARE_ENIX.md "wikilink")）拡散性ミリオンアーサー -
    アーサー-魔法の派
  - （[SQUARE ENIX](../Page/SQUARE_ENIX.md "wikilink")）乖離性ミリオンアーサー-
    富豪アーサー-

## 相關條目

  - [堀口悠紀子](../Page/堀口悠紀子.md "wikilink")
  - [abec](../Page/abec.md "wikilink")

## 參考資料

## 外部連結

  - [個人網站「BUN+BUN」](http://bunplusbun.sakura.ne.jp/)

  - [Pixiv頁面](http://www.pixiv.net/member.php?id=8142)

  -
[Category:日本插畫家](../Category/日本插畫家.md "wikilink")
[Category:京都府出身人物](../Category/京都府出身人物.md "wikilink")
[Category:電子遊戲繪圖及原畫家](../Category/電子遊戲繪圖及原畫家.md "wikilink")

1.
2.
3.  [只要有趣就夠了！發行累計6000萬冊――編輯工作目錄](https://www.kadokawa.com.tw/p1-products_detail.php?id=4779i6yUpimK2f4vrkZgKnqjFQD5TLTeObry_MHOyiz6)ISBN
    9789864734603 [三木一馬著](../Page/三木一馬.md "wikilink")，出自第五章第一節"《Sword
    Art Online刀劍神域》在選擇插畫家時的秘辛"。