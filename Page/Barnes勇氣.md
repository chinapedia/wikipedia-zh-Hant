'''Barnes勇氣 '''（バーンズ勇気，伯恩斯勇氣，ばーんず ゆうき（Yuki Barnes）,
）是出身於日本[神奈川縣的](../Page/神奈川縣.md "wikilink")[藝人](../Page/藝人.md "wikilink")。屬於[Stardust
Promotion的一員](../Page/Stardust_Promotion.md "wikilink")。

血型是O型。身高在2006年4月當時是166公分。母親是日本人，父親是美國人的日美混血兒。特技是打鼓，倒立等等。喜歡的食物是拉麵。喜歡的台詞是「Come
on\!」。將來的夢想是成為舞者。

從2004年度開始在[天才兒童MAX中擔任](../Page/天才兒童MAX.md "wikilink")[TV戰士演出](../Page/TV戰士.md "wikilink")。另外，也在所屬事務所組成的「[Prizmax.Jr](../Page/Prizmax.Jr.md "wikilink")」這個團體中活躍。所屬事務所同時也是TV戰士[de・Lencquesaing望](../Page/de・Lencquesaing望.md "wikilink")・[藤本七海的所屬事務所](../Page/藤本七海.md "wikilink")。

## 天才兒童MAX

  - 從前曾出現把音階「**ファ**」回答成「**ワ**」，在提示「**動物之王**」時，回答的卻是「**大猩猩**」（正確答案是「獅子」）等場面。但是從2006年度開始成為最年長的TV
    戰士後，有了作為領導者的自覺，在很多場面發揮了領導力。
  - 曾參與許多『[MTK](../Page/MTK.md "wikilink")』。在2004年度中，是演唱『MTK』的新人戰士第1號。
    在2005年度的單元[片尾曲MTK裡](../Page/片尾曲MTK.md "wikilink")，和[前田公輝](../Page/前田公輝.md "wikilink")・-{[飯田里穗](../Page/飯田里穗.md "wikilink")}-・[木內江莉一起組成了](../Page/木內江莉.md "wikilink")[Our
    Treasures樂團而活躍](../Page/音樂TV君#Our_Treasures.md "wikilink")。在2006年度的『片尾曲MTK』中，包含可說是他自己主題曲的『**ユウキスイッチ**』在內，演唱了3首曲子（包括節目主題曲在內）。
  - 在[紙飛機達陣賽中](../Page/天才兒童MAX的單元.md "wikilink")，從2004年度開始獲得了許多良好成績。在2005年度中作為「蒸氣騎士團・サッソウ」的隊長，曾發明了「**Barnes接住**」這個在天才兒童MAX歷史上留名的有名台詞。這個台詞也作為笑料運用在紙飛機達陣賽以外的單元。2006年開始作為「ムサシ・ジョウネッツ」的一員參與競賽。附帶一提，他現在是紙飛機達陣賽總合比賽勝利數的世界紀錄保持人。
  - 在2006年度夏季公演『解救孩子星\!未來是屬於我們的』中擔任主角。由於他的台詞是最多的，為了背台詞甚至一度把身體累壞了，但在他的努力之下，正式演出時展現了堂堂的演技，並且成功的演唱MTK『ユウキスイッチ』，獲得了觀眾們的掌聲。
  - 在2006年度的『[這裡是「週刊優克迪爾」編輯部](../Page/天才兒童MAX的單元#這裡是「週刊優克迪爾」編輯部.md "wikilink")』中，藤田Ryan以「踊る大家族！！（跳舞的大家族）」為題，表示自己意外發現了勇氣家裡的人每當聽到「∞SAKAおばちゃんROCK」這首曲子時，就會一齊開始跳舞。原來勇氣的母親是[錦戸亮與](../Page/錦戸亮.md "wikilink")[關西傑尼斯8的愛好者](../Page/關西傑尼斯8.md "wikilink")，與朋友們每次聽到相關歌曲就會隨之起舞。

## 主要演出作品

  - NHK教育[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")（2004年－2006年）
  - 雜誌[なかよし](../Page/なかよし.md "wikilink")
  - 雜誌Sesame Jr.
  - 雜誌Myojo

## 外部連結

  - [官方個人簡介](http://www.stardust.co.jp/talent/86.html)
  - [スターダストプロモーション](http://www.stardust.co.jp/)

[Category:TV戰士](../Category/TV戰士.md "wikilink")