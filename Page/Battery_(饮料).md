[Battery_logo.png](https://zh.wikipedia.org/wiki/File:Battery_logo.png "fig:Battery_logo.png")
**Battery**是[芬兰Oy](../Page/芬兰.md "wikilink")
[Sinebrychoff](../Page/Sinebrychoff.md "wikilink")
Ab公司出品的一款[机能性饮料](../Page/机能性饮料.md "wikilink")。

## 配方

[软饮料包含了](../Page/软饮料.md "wikilink")[水](../Page/水.md "wikilink")、[糖](../Page/糖.md "wikilink")、[糊精](../Page/糊精.md "wikilink")、[牛磺酸](../Page/牛磺酸.md "wikilink")、[柠檬酸](../Page/柠檬酸.md "wikilink")、[咖啡因](../Page/咖啡因.md "wikilink")、[瓜拿納种子精华](../Page/瓜拿納.md "wikilink")、[维生素B](../Page/维生素B.md "wikilink")、香精、色素和[防腐剂](../Page/防腐剂.md "wikilink")。
在芬兰包含咖啡因产品较为常见，官方网站宣称Battery饮料“每100厘升中包含着320毫升”。这一咖啡因含量比例和一杯[浓缩咖啡相近](../Page/浓缩咖啡.md "wikilink")。

## 口味

2006年开始销售一种新口味的姜味机能性饮料\[1\]。Battery在2006年底推出了无糖型，称之为Stripped\[2\]
Battery。

## 参考资料

<div class="references-small">

<references/>

</div>

## 外部链接

  - [Battery website](http://www.batterydrink.com/)
  - [Koff Battery
    info](http://www.koff.fi/fi/tuotteet/energiajuomat/battery.html)
  - [Gingered Battery
    announcement](http://www.koff.fi/en/news/info/551.html)
  - [Gingered Battery can
    picture](http://tuotanto.heyday.fi/batterymedia/popup.php?id=5)
  - [Battery Ecuador
    website](https://web.archive.org/web/20080424081233/http://www.battery.ec/)

[Category:机能性饮料](../Category/机能性饮料.md "wikilink")
[Category:无酒精饮料](../Category/无酒精饮料.md "wikilink")
[Category:1997年面世的產品](../Category/1997年面世的產品.md "wikilink")

1.  [Battery Gingered: a new flavor to energize your
    spring](http://www.koff.fi/en/news/info/551.html), dated 23 March
    2006, accessed 14 January 2007
2.  *Oluen ystävät*, Oy Sinebrychoff Ab customer magazine 4/2006