[BeOS5Pro.png](https://zh.wikipedia.org/wiki/File:BeOS5Pro.png "fig:BeOS5Pro.png")
**BeOS**是一种由[Be公司针对](../Page/Be公司.md "wikilink")[多媒体使用所開發的](../Page/多媒体.md "wikilink")[作業系統](../Page/作業系統.md "wikilink")。

## 歷史

1991年，[让-路易·加西带领包括](../Page/让-路易·加西.md "wikilink")[AppleNewton开发员](../Page/Apple.md "wikilink")[Steve
Sakoman在内的一众Apple员工创建了Be公司](../Page/Steve_Sakoman.md "wikilink")。Be公司开发了一个全新的操作系统，从设计之初就针对多[CPU和](../Page/CPU.md "wikilink")[多线程的](../Page/多线程.md "wikilink")[应用程序](../Page/应用程序.md "wikilink")。与此同时，Apple已陷入不能推出其新操作系统[Copland的困境](../Page/Copland.md "wikilink")，正在寻找代替品。加西认为这是个黄金时机。1996年，加西要价4亿美金，允许Apple
Computer使用BeOS。然而Apple估算Be公司的总值为8000万美金，故此出价1.2亿，后来上升到2亿。最终未能成交，Apple转而购买[NeXTSTEP](../Page/NeXTSTEP.md "wikilink")，同时重新得到[史蒂夫·乔布斯](../Page/Steve_Jobs.md "wikilink")。

  - 1996年11月，BeOS發佈了第一個運行于[蘋果機上的版本](../Page/蘋果電腦.md "wikilink")。
  - 1998年，發佈了第一個運行于[Intel平臺的版本](../Page/x86.md "wikilink")。
  - 2000年發佈了5.0版本，包括了個人版（BeOS 5.0 Personal Edition）和專業版（BeOS 5.0
    Professional Edition），其中個人版是免費的。

官方最後發行的版本是[BeOS
R5版](../Page/BeOS_R5.md "wikilink")。後Be公司被[Palm公司于](../Page/Palm公司.md "wikilink")2001年8月以1100万收购，從此不再發佈官方版本。

### 版本歷史

| 版本                                                   | 發佈日期             | 平台                                                                                        |
| ---------------------------------------------------- | ---------------- | ----------------------------------------------------------------------------------------- |
| DR1–DR5                                              | October 1995     | [AT\&T Hobbit](../Page/AT&T_Hobbit.md "wikilink")                                         |
| DR6 (developer release)                              | January 1996     | [PowerPC](../Page/PowerPC.md "wikilink")                                                  |
| DR7                                                  | April 1996       |                                                                                           |
| DR8                                                  | September 1996   |                                                                                           |
| Advanced Access Preview Release                      | May 1997         |                                                                                           |
| PR1 (preview release)                                | June 1997        |                                                                                           |
| PR2                                                  | October 1997     |                                                                                           |
| R3                                                   | March 1998       | [PowerPC](../Page/PowerPC.md "wikilink") and [Intel x86](../Page/Intel_x86.md "wikilink") |
| R3.1                                                 | June 1998        |                                                                                           |
| R3.2                                                 | July 1998        |                                                                                           |
| R4                                                   | November 4, 1998 |                                                                                           |
| R4.5 ("Genki")                                       | June 1999        |                                                                                           |
| [R5 PE/Pro ("Maui")](../Page/BeOS_R5.md "wikilink")  | March 2000       |                                                                                           |
| [R5.1 ("Dano")](../Page/Dano_\(BeOS\).md "wikilink") | November 2001    | [Intel x86](../Page/Intel_x86.md "wikilink")                                              |

## 特征

BeOS的設計理念是專門用於多媒體處理的“多媒體作業系統”，採用先進的64位[BeFS](../Page/BeFS.md "wikilink")[文件系統](../Page/文件系統.md "wikilink")，支援多處理器，其多媒體性能異常優越。BeOS开始是运行在[BeBox硬件之上的](../Page/BeBox.md "wikilink")。与其他同期的操作系统不同，BeOS是为了充分利用现代硬件的优点而编写。针对数字媒体工作优化，BeOS能够充分利用多处理器系统通过模块化的I/O带宽，多线程，抢断式的多任务和被称为BFS的定制64位日志文件系统。BeOS的[GUI遵循清晰整洁的设计原理而开发](../Page/GUI.md "wikilink")。其[API是用](../Page/API.md "wikilink")[C++编写而成](../Page/C++.md "wikilink")，非常容易编程。虽非源于[Unix的操作系统](../Page/Unix.md "wikilink")，但其实现了[POSIX兼容](../Page/POSIX.md "wikilink")，并通过[Unix
Shell命令行界面来访问](../Page/Unix_Shell.md "wikilink")

### 特点

  - 优秀的多媒体性能
  - 易于安装配置
  - 清晰的GUI
  - 均衡的多处理技术
  - 原生的面向对象[接口](../Page/界面_\(程式设计\).md "wikilink")
  - 拥有32个工作空间（虚拟桌面）
  - 类似数据库的64位日志的文件系统
  - 記憶體保护
  - 遵循POSIX标准
  - 整洁的API

### 缺点

  - 有限的硬件支援
  - 有限的可选应用程式
  - 有限的办公文档相容性
  - 系统的维护勉强
  - 非标准的web浏览器

## 衍生版本及現況

由BeOS衍生出來的其它非官方版本仍然在繼續發展，如開放源代碼的[Haiku](../Page/Haiku.md "wikilink")（OpenBeOS）等。

以完全重新創建類BeOS系統為目標的衍生版本有：

  - [Haiku](../Page/Haiku.md "wikilink")
  - [Blue Eyed OS](../Page/Blue_Eyed_OS.md "wikilink")
  - [Cosmoe](../Page/Cosmoe.md "wikilink")

以繼續發展原有BeOS系統為目標的衍生版本有：

  - [Zeta](../Page/Magnussoft_ZETA.md "wikilink")

## 外部連結

  - [BeGroovy](http://www.begroovy.com)
  - [BeOS News](http://www.beosnews.com)
  - [Bebits（BeOS軟件大全）](http://www.bebits.com)
  - [Zeta OS](http://www.zeta-os.com/)
  - [Haiku Project](http://www.haiku-os.org/)
  - [BeOSChina](https://web.archive.org/web/20171019065321/http://www.beoschina.com/)

[Category:視窗系統](../Category/視窗系統.md "wikilink")
[Category:BeOS](../Category/BeOS.md "wikilink")