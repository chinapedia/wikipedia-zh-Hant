《**Be My
Valentine**》是[歌手](../Page/歌手.md "wikilink")[古巨基的流行](../Page/古巨基.md "wikilink")[音樂專輯](../Page/音樂.md "wikilink")，於1998年12月29日正式發行。

## 派台歌曲成績

| 歌曲              | 903   | RTHK  | 997   | TVB   | 備註 |
| --------------- | ----- | ----- | ----- | ----- | -- |
| 有你一天            | **1** | 2     | **1** | 3     |    |
| Be My Valentine | 5     | **1** | **1** | **1** |    |
| 「倒影」先生          | 4     | \-    | \-    | \-    |    |

## 參見

[古巨基](../Page/古巨基.md "wikilink")

[Category:古巨基音樂專輯](../Category/古巨基音樂專輯.md "wikilink")
[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:1998年音樂專輯](../Category/1998年音樂專輯.md "wikilink")
[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")