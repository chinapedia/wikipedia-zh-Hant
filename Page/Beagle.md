**Beagle**是[GNOME的一个](../Page/GNOME.md "wikilink")[搜尋工具](../Page/搜尋工具.md "wikilink")，透過索引的建立可以快速搜索个人信息空间中的多种文件类型。目前已不再開發。

## 搜尋的範圍

  - 文件系统
  - [Evolution邮件](../Page/Evolution.md "wikilink")，日历，地址簿
  - [Gaim即时通讯和IRC日志](../Page/Gaim.md "wikilink")
  - [Firefox和Epiphany网页](../Page/Firefox.md "wikilink")
  - Blam和Liferea的RSS feed
  - Tomboy笔记
  - [KDE](../Page/KDE.md "wikilink") KMail邮件
  - 办公文档
      - [OpenOffice.org](../Page/OpenOffice.org.md "wikilink")（sxw, sxc,
        sxi, odt, odp等）
      - [Microsoft Office](../Page/Microsoft_Office.md "wikilink")（doc,
        xls, ppt）
      - AbiWord（abw）
      - Rich Text Format (rtf)
      - [PDF](../Page/PDF.md "wikilink")
  - 文本文档
      - HTML (xhtml, html, htm)
      - Source code (C, C++, C\#, Fortran, Java, JavaScript, Lisp,
        Matlab, Pascal, Perl, PHP, Python, Scilab)
      - 純文本（txt等）
  - 帮助文档
      - Texinfo
      - Man pages
      - Docbook
      - Monodoc
      - Windows帮助文件 (chm)
  - 图像 (jpeg, png, svg)
  - 音频（mp3, ogg, flac）
  - 应用启动器

## 外部链接

  - [Beagle主页](https://web.archive.org/web/20051027002749/http://www.beaglewiki.org/Main_Page)

[Category:桌面搜索引擎](../Category/桌面搜索引擎.md "wikilink")
[Category:Linux軟件](../Category/Linux軟件.md "wikilink")
[Category:GNOME](../Category/GNOME.md "wikilink")