**because of
you**，[Stephanie的第二张单曲](../Page/Stephanie.md "wikilink")，商品番号SECL-528。

## 作品概要

包括单曲「because of
you」在内的全3曲收录。初回特典是电视动画[星界死者之书的特典卡片封入](../Page/星界死者之书.md "wikilink")。
在单曲「because of you」中，Stephanie感谢了自己出身20年以来，关心过自己的人，以及对未来的希望。
Oricon排名：最高排名第50位，销量：初期销量2358张，累计销量7898张。

## 收录曲

1.  because of you
      - 作詞：[STEPHANIE](../Page/STEPHANIE.md "wikilink") &
        [矢住夏菜](../Page/矢住夏菜.md "wikilink") 作曲：[Joe
        Rinoie](../Page/Joe_Rinoie.md "wikilink") 編曲：Joe Rinoie &
        [長岡成貢](../Page/長岡成貢.md "wikilink")
      - [东京电视台动画](../Page/东京电视台.md "wikilink")[星界死者之书第二期片尾曲](../Page/星界死者之书.md "wikilink")（ED2）
2.  To. Be. Me.
      - 作詞：STEPHANIE & 矢住夏菜 作曲・編曲：Joe Rinoie &
        [峰正典](../Page/峰正典.md "wikilink")
3.  Ben
      - 作詞・作曲：DON BLACK/WALTER SCHARF 編曲：Joe Rinoie

[Category:2007年单曲](../Category/2007年单曲.md "wikilink")
[Category:東京電視台動畫主題曲](../Category/東京電視台動畫主題曲.md "wikilink")
[Category:Stephanie歌曲](../Category/Stephanie歌曲.md "wikilink")