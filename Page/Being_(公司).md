**Being**（，）是一家在[日本](../Page/日本.md "wikilink")[東京都](../Page/東京都.md "wikilink")[港區成立於](../Page/港區_\(東京都\).md "wikilink")[1978年的音樂製作](../Page/1978年.md "wikilink")、唱片發行及藝人經紀管理公司。現在除了音樂事業之外，還涉足[不動產投資](../Page/不動產.md "wikilink")。公司的創始人是[長戶大幸](../Page/長戶大幸.md "wikilink")。株式會社Being旗下的子公司曾多達60餘家，包括：[GIZA
studio](../Page/GIZA_studio.md "wikilink")、[VERMILLION
RECORDS](../Page/VERMILLION_RECORDS.md "wikilink")、、、[ZAIN
ARTISTS等](../Page/ZAIN_ARTISTS.md "wikilink")；現經過公司改制之後，部分公司將以關聯公司的形式存在，而子公司的規模則縮小到40餘家。

## 公司簡介

## 發展變革

## 企業特征

## 內部試唱

## 公司現狀

## 相關公司

## 廠牌與所屬藝人

## White Dream所属藝人、模特與演員

  - [麻亜里](../Page/麻亜里.md "wikilink")
  - [本田恭子](../Page/本田恭子.md "wikilink")
  - [栗咲寛子](../Page/栗咲寛子.md "wikilink")
  - [中村祐美子](../Page/中村祐美子.md "wikilink")
  - [内田衣津子](../Page/内田衣津子.md "wikilink")
  - [松村龍之介](../Page/松村龍之介.md "wikilink")
  - [小野匠](../Page/小野匠.md "wikilink")
  - [三上翔平](../Page/三上翔平.md "wikilink")
  - [宮元英光](../Page/宮元英光.md "wikilink")

## 曾所屬歌手

## 曾所屬製作人

## 經紀管理

## 相關人士

## 節目製作

## 參與媒體

## 參考資料

## 相關詞條

  - [日本歌謠曲](../Page/日本歌謠曲.md "wikilink")·[日本流行音樂](../Page/日本流行音樂.md "wikilink")·[新音樂](../Page/日本流行音樂#.E6.96.B0.E9.9F.B3.E6.A8.82.EF.BC.881970.E5.B9.B4_-_1980.E5.B9.B4.EF.BC.89.md "wikilink")·[流行樂](../Page/流行_\(音樂類型\).md "wikilink")
  - [編曲](../Page/改編曲.md "wikilink")·[錄音室](../Page/錄音室.md "wikilink")·[音乐家](../Page/音乐家.md "wikilink")·[导演](../Page/导演.md "wikilink")·[唱片公司](../Page/唱片公司.md "wikilink")

## 外部連接

  -
  - [Musing](http://musing.jp/)（Being旗下藝人的音樂門戶站）

  -
[Category:1978年成立的公司](../Category/1978年成立的公司.md "wikilink")
[Category:港區公司 (東京都)](../Category/港區公司_\(東京都\).md "wikilink")
[Category:日本唱片公司](../Category/日本唱片公司.md "wikilink")
[Category:日本電影公司](../Category/日本電影公司.md "wikilink")
[Category:日本藝人經紀公司](../Category/日本藝人經紀公司.md "wikilink")