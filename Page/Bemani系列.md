[Bemani_series_logo.png](https://zh.wikipedia.org/wiki/File:Bemani_series_logo.png "fig:Bemani_series_logo.png")
**Bemani系列**是[科乐美](../Page/科乐美.md "wikilink")[音乐游戏系列统一的品牌名称](../Page/音乐游戏.md "wikilink")。如《[劲舞革命](../Page/劲舞革命.md "wikilink")》、《[狂热节拍IIDX](../Page/狂热节拍IIDX.md "wikilink")》、《[乐动魔方](../Page/乐动魔方.md "wikilink")》、《[舞蹈进化](../Page/舞蹈进化.md "wikilink")》等音乐游戏都属于Bemani系列。其名稱來自於該公司1997年至2002年間推出的狂热节拍（beatmania）系列。

這種遊戲機的主要概念是玩家在遊戲機上隨著音樂的節拍，在指定的拍點做出指定的動作，如腳踩或敲擊特定的機器部位、或是擺出特定的姿勢等等，而遊戲機依據感應器接收到的動作準確度予以評價，在樂曲或遊戲結束後評分。雖然遊戲機無法測知玩家的動作是否正確，不過對玩家練習反應、節奏感有很大的幫助。除了街機之外，[科樂美也發行街機遊戲的](../Page/科樂美.md "wikilink")[家用遊戲機或](../Page/家用遊戲機.md "wikilink")[掌上遊戲機版本](../Page/掌上遊戲機.md "wikilink")。

## 機種一覽

### 街机平台

#### 现行机种

  - [beatmania IIDX](../Page/beatmania_IIDX.md "wikilink")（狂热节拍IIDX）

  - [Pop'n Music](../Page/Pop'n_Music.md "wikilink")（流行音乐）

  - [DanceDanceRevolution](../Page/DanceDanceRevolution.md "wikilink")（劲舞革命）

  - GITADORA

      - [GuitarFreaks](../Page/GuitarFreaks.md "wikilink")（結他高手）
      - [DrumMania](../Page/DrumMania.md "wikilink")（狂热鼓手）

  - [jubeat](../Page/jubeat.md "wikilink")（乐动魔方）

  - [REFLEC BEAT](../Page/REFLEC_BEAT.md "wikilink")（动感弹珠）

  - [SOUND VOLTEX](../Page/SOUND_VOLTEX.md "wikilink")（音樂瘋）

  -
  -
  - [DANCE RUSH](../Page/DANCE_RUSH.md "wikilink") (熱舞燦漫)

#### 已完结

  - [beatmania](../Page/beatmania.md "wikilink")

      - beatmania III

  - GuitarFreaks V

      - GuitarFreaks XG

  - Drummania V

      - Drummania XG

  - DanceDanceRevolution Solo

  - Dancing Stage

  - KEYBOARDMANIA

  - ParaParaParadise

  - RAP FREAKS

  - Dance Maniax

  - MANBO A GO GO

  - TOY'S MARCH

  - DANCE 86.4 FUNKY RADIO STATION

  -
  - [Dance Evolution
    Arcade](../Page/Dance_Evolution_Arcade.md "wikilink")（家用版并无Bemani标志）

  -
### 家用游戏机平台

  - うたっち
  - pop'n stage
  - Beat'n Groovy
  - マリンバ天国

## 相關條目

  - [街機](../Page/街機.md "wikilink")
  - [電子遊戲](../Page/電子遊戲.md "wikilink")

## 外部連結

  - [KONAMI](http://www.konami.com)官方網站
  - [Bemanistyle.com](http://www.bemanistyle.com) Bemani玩家網站
  - [MusicGuild](https://web.archive.org/web/20161130124843/http://mg.zkiz.com/)
    純粹音樂遊戲討論區
  - [DDR Freak](http://www.ddrfreak.com) 《DDR》玩家網站
  - [GDAmania](https://web.archive.org/web/20040610194104/http://www.homei.cc/gdamania/)
    Drummania/Guitar Freaks 玩家網站

[Category:音樂遊戲](../Category/音樂遊戲.md "wikilink")
[Category:科樂美遊戲](../Category/科樂美遊戲.md "wikilink")
[BEMANI系列](../Category/BEMANI系列.md "wikilink")