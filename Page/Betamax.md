[Sony的](../Page/Sony.md "wikilink")**Betamax**，簡稱**Beta**，是一种年份较早的专业用
[U-matic](../Page/U-matic.md "wikilink")[录像带演进而来的](../Page/录像带.md "wikilink")

家用录像带[格式](../Page/格式.md "wikilink")。它于1975年4月16日发表，同年5月10日上市。如同1976年[JVC发表的](../Page/JVC.md "wikilink")[VHS制式](../Page/VHS.md "wikilink")，它没有[guard
band](../Page/guard_band.md "wikilink")，使用[水平录制以减少](../Page/水平录制.md "wikilink")[漏话](../Page/漏话.md "wikilink")。Betamax名字的由来有两种说法。一种是当磁带在机器中运转时，它环绕机件的样子像希腊字母"[Beta](../Page/Beta.md "wikilink")"；另一种说法是"Beta"在日文中是"品质"的意思，而"max"则用来形容"beta"。

[三洋電機](../Page/三洋電機.md "wikilink")（Sanyo）以*Betacord*的名字上市贩售，而这也偶然地一起被叫做Beta。除了Sony与Sanyo，[东芝](../Page/东芝.md "wikilink")、[先锋](../Page/先鋒公司.md "wikilink")、[爱华](../Page/爱华.md "wikilink")、[日本電氣与](../Page/日本電氣.md "wikilink")[大同公司亦有贩售Betamax录像机](../Page/大同公司.md "wikilink")。另外，[增你智与](../Page/增你智.md "wikilink")[WEGA亦曾联系与Sony签合约以加入生产线](../Page/WEGA.md "wikilink")。如同[RadioShack电器连锁店](../Page/RadioShack.md "wikilink")，在美国与加拿大的[Sears与德国的](../Page/Sears,_Roebuck_and_Company.md "wikilink")[Quelle百货也贩售他们的](../Page/Quelle百货.md "wikilink")[自有品牌Betamax录像机](../Page/自有品牌.md "wikilink")。

SONY在1975年发表了带有LV-1901
Trinitron/Betamax仪表板的家用录像系统。Betamax曾是1983年最畅销的录像带格式，在英国市场得到三分之一的[市占率](../Page/市占率.md "wikilink")，当时三洋電機的VTC5000是英国卖得最好的一款录像机。然而，到了1985年，整个市场开始转向VHS。

世界上第一款[手持摄像机是](../Page/手持摄像机.md "wikilink")1983年三洋電機推出的Betamax摄像机“Betamovie”。
[Sony_bmc100p.jpg](https://zh.wikipedia.org/wiki/File:Sony_bmc100p.jpg "fig:Sony_bmc100p.jpg")

## 格式概要

  - 存储方式：螺旋扫描
  - 磁头数：2
  - 磁鼓直径：约74mm
  - 磁鼓转速：约30Hz（约1800rpm）
  - 磁带宽：12.7mm
  - 磁带前进速度：βI・βIs-约40mm/s / βII-约20mm/s / βIII-约13mm/s
  - 信号轨宽：βI-约58μm / βIs-39μm（SONY机种）・33μm（NEC机种）/ βII-29μm / βIII-19.5μm
  - 信号存储方式：
      - 视频信号：[调频](../Page/调频.md "wikilink")（FM）Sink
        chip：3.6MHz／白峰值：4.8MHz：颜色信号：低域変換方式
      - 音频信号：2声道纵向纪录

## 录制格式

以下列举Betamax的各种格式与其对应的制造厂：

  - **普通（单声道）Beta模式**：SONY（所有机型）与参与Betamax生产的所有公司。（对应βII・βIII）
      - βI（最早的Betamax机种的基本模式。早期的机种支持录影功能，所有机种支持播放功能）
      - βII（Betamax实质上的标准格式。有βI的两倍时长）
      - βIII（Betamax的长时间模式。有βI的三倍、βII的1.5倍时长）
  - **Beta Hi-Fi模式**：SONY及所有参与Betamax生产的公司（ゼネラル除外）
      - βII（对应高音质录制。在单声道的Betamax机器上播放其录制的带子可能会有杂音）
      - βIII（对应高音质录制。在单声道的Betamax机器上播放其录制的带子可能会有杂音）
  - **Hi-Band(SuperBeta)**：SONY、NEC、先锋（对应βIs）、爱华、東芝（对应βII・βIII）、三洋（对应βII・βIII播放机）
      - βIs（基于Hi-Band βII提升影带前进速率的规格。与βI不兼容）
      - SHB-βIs（针对βIs模式High-Band优化。可以播放βIs模式录制的影带。搭载于SONY的中、高级机种。）
      - βII（基于Beta Hi-Fi格式的高画质模式。）
      - βIII（基于Beta Hi-Fi格式的高画质模式。）
  - **ED-Beta格式**：仅SONY
      - βII（ED-Beta可播放／录制）
      - βIII（ED-Beta可播放／录制）

## Betamax的历史

Betamax的失败已经成为一种经典的市场销售案例。有一个口语化的用语"to
Betamax"表示这种情形：一种具独占性的科技，在对手格式允许多家厂商合作的情况下遭遇压倒性的失败。Sony掌控工业标准的能力在[JVC与其母公司](../Page/JVC.md "wikilink")[松下做出战术性决策](../Page/松下.md "wikilink")－忽略SONY提供Betamax予JVC的VHS技术－时遭到了逆火。他们认为这最终将会重蹈[U-Matic的覆辙](../Page/U-Matic.md "wikilink")：Sony支配一切，让他们收烂摊子。在1984年，40家公司联合起来支持VHS格式。而同时，支持Betamax的-{只}-有12家。Sony最终在1988年承认失败并开始生产VHS录像机。然而，Sony在这事件中或许仍有些值得做为慰藉的：他们的[Video8](../Page/Video8.md "wikilink")（V8）格式实质上就是缩小版的Betamax。而Video-8与其競争对手[VHS-C共同支配了家用摄像机市场](../Page/VHS-C.md "wikilink")，直到两种格式皆逐渐被数码标准的[MiniDV取代](../Page/MiniDV.md "wikilink")。

真正使VHS成功的原因是[RCA](../Page/RCA.md "wikilink")。他们要求松下公司制造可以录制长达4小时的VHS录像机。RCA在Betamax正在开发时曾与索尼讨论过相同的问题，但是索尼的工程师认为将影带前进速度从每秒4公分降至2公分以及将影像轨变窄会使得画质损失过于严重。而松下公司则不顾JVC的反对，提供了正是RCA所要的长时间录制模式。相对地，RCA以995美元的建议售价贩售这种4小时录制的VHS机种。RCA的价格与市场策略在此起了决定性的作用。

Betamax在美国的上市发表引发了一个重要事件，即是SONY与[环球影城之间的官司](../Page/环球影城.md "wikilink")（*[Sony
Corp. v. Universal City
Studios](../Page/Sony_Corp._v._Universal_City_Studios.md "wikilink")*，1984年"Betamax事件"）。[美国最高法院最后判决家用录像带](../Page/美国最高法院.md "wikilink")、机在[美国是合法的](../Page/美国.md "wikilink")，因为录像机有实质上不违法的使用方式。这个先例后来在*[MGM
v. Grokster](../Page/MGM_v._Grokster.md "wikilink")*
(2005)的官司中被援引，使高等法院同意这个"实际上不违法的使用方式"也可以应用于[P2P软件](../Page/P2P.md "wikilink")，而它们的作者、提供者也应是无罪的。（特别注意：那些"积极引诱"违反版权法经由"蓄意、有罪的表示与行为"不在此合法范围。）

[Three_betamax_vcrs.jpg](https://zh.wikipedia.org/wiki/File:Three_betamax_vcrs.jpg "fig:Three_betamax_vcrs.jpg")

[Sony_SL-MV1_and_RMT-117_20051001.jpg](https://zh.wikipedia.org/wiki/File:Sony_SL-MV1_and_RMT-117_20051001.jpg "fig:Sony_SL-MV1_and_RMT-117_20051001.jpg")在日本上市的一种罕见的Betamax录像机与电视结合的机器-“SL-MV1”。\]\]

[SONY_BCT_30G_(brighter_cropped).jpg](https://zh.wikipedia.org/wiki/File:SONY_BCT_30G_\(brighter_cropped\).jpg "fig:SONY_BCT_30G_(brighter_cropped).jpg")

[Betacam_betamax_tapes.jpg](https://zh.wikipedia.org/wiki/File:Betacam_betamax_tapes.jpg "fig:Betacam_betamax_tapes.jpg")

在专业与广播影像中，Sony的[Betacam](../Page/Betacam.md "wikilink")－源自于Betamax并成为一专业用格式－成为一种标准格式。需要做专业影像处理的业者利用Betacam交换影像，因此Betacam系统成为在[ENG](../Page/Electronic_News_Gathering.md "wikilink")（电子新闻采集，Electronic
News Gathering）业中最被广泛使用的格式并取代了3/4"
U-matic格式。U-matic曾是一种对于广播电视来说实用且具经济效益的便携式影带格式－标示着16厘米影带的终结。而"film
at
eleven"这个片语在16厘米影带被开发出来前常在6点钟新闻播报里听到。而专业形式的VHS－[MII](../Page/MII_\(录像带\).md "wikilink")（又叫Recam）则在与Betacam的对抗中失败。当Betacam成为一种默认的广播业标准格式时，其在专业市场上的地位恰巧反映了VHS在家用录像系统的称霸。在技术层面上，Betacam与Betamax的相似处在于它们都使用相同的影带形式、同样的二氧化铬磁带与相同的[保磁力](../Page/保磁力.md "wikilink")，而且它们都在影带相同的位置上记录线性音频轨。然而在视频录制上，Betacam与Betamax完全不同－Betacam使用[色差端子编码系统](../Page/色差端子.md "wikilink")。Betacam的带子可以与Betamax交换使用，但录制模式不互通。Betacam的影带前进速率是12公分／秒，而Betamax则在4公分／秒到1.33公分／秒之间。

SONY亦提供了一些工业用的Betamax产品－只支持Beta
I录制模式的机器－针对工业及公共团体用户。基本上较U-Matic便宜且尺寸更小。后来Betacam系列的到位减少了工业用Betamax与U-Matic的需求。

当SONY发表了他们的[PCM](../Page/脉冲编码调制.md "wikilink")（脉冲码调幅。Pulse Code
Modulation）数码录制系统于一编码箱－连接于Betamax录像机的PCM转接器－时，Betamax也在音乐录制工业上扮演了重要的角色。Sony的PCM-F1转接器搭配Betamax录像机SL-2000做为便携式[数字音频录制系统](../Page/数字音频.md "wikilink")。在80－90年代，许多录音工程师使用此系统录制他们的第一个数字音源。

最初，Sony可以吹捧一些-{只}-有Betamax有的功能，例如BetaScan（可以在快进／快退时进行图像搜寻）以及BetaSkipScan（可以让操作者在按下快进／倒退键时知道现在带子的位置，而影带传送模式会切换成BetaScan模式，直到松开快进／快退键为止）。Sony相信VHS所采用的M型进带方式无法应用这些技术。BetaScan最早被称为Videola，直到制作Moviola的公司警告要采取法律行动为止。

Sony也同时贩售BetaPak－一种设计给照相机使用的小型机器。在考量过各部件及连接线的需求之后，他们设计出了整合照相、录像功能的机器－Camcorder。最后以Betamovie的名称上市。Betamovie使用一般尺寸的影带，但采用了不同的进带方式。磁带会以300度环绕在一个较小的（直径44.671mm）的磁鼓上。使用一个双方位磁头以写入视频轨。要播放时，它可以在一般的Betamax机器上播放。由于使用了不同的磁带录制技术与进带方式，使用手持摄像机播放是不可行的。SONY亦有贩售SuperBeta与工业用Betamovie摄像机。

Betamax以Beta
Hi-Fi的名称发布了录于影带上的高真实音频。对于[NTSC制式](../Page/NTSC.md "wikilink")，Beta
Hi-Fi透过放置一对FM载体于颜色（C）与光（Y）载体中间来实现，这个操作被称为音频调幅。每个头都有一对明确指定的载体，总共有4个独立声道。磁头A将其Hi-Fi载体记录于1.38(L)及1.68(R)MHz，而磁头B则在1.53与1.83
MHz这使它能提供80分贝动态范围音频，并将失真与抖动控制于0.005%以下。

在Beta Hi-Fi发布之前，Sony将Y载体调升400kHz以预留空间给Beta Hi-Fi需要的4
FM载体。所有的Betamax机器都应用了这个改变，并加上寻找低频前AFM
Y载体。Sony应用了"反寻找"回路以阻止机器去寻找不存在的Y载体。

某些Sony的NTSC机型被标示为"HiFi
Ready"（型号以SL-HFR做为前缀以与一般的SL或SL-HF区别）。这些Betamax机型的面板与其他一般机型相似，但在后方多了一个28针的接脚。如果有人希望购买Beta
Hi-Fi机型但没有足够的资金，他可以先购买一款"SL-HFRxx"机型，日后再购买外接的Hi-Fi处理器。Sony提供了两种外接式Hi-Fi处理器：HFP-100与HFP-200。除了HFP-200可以处理多声道、并有"stereocast"字样在"Betahifi"的logo后面之外，它们是相同的。如此做可行的原因在于NTSC制式的Betamax不需要像VHS一样多一对磁头。Hi-Fi处理器能产生会被附加主机录下的必要载体，而在播放时AFM载体会被传送至Hi-Fi处理器。机器后面板上还有一个微调钮以处理某些较难处理的带子。

然而对于[PAL制式](../Page/PAL.md "wikilink")，颜色与光载体之间的带宽不足以容下另外的FM载体。于是深多路技术被应用。如此音轨将使用与视频轨相同的方式录制。较低频的音轨先被音频磁头写入，接着视频轨由视频磁头复写在音频轨上。头盘上有另一组位在一般视频磁头之前的不同方向的音频磁头。

Sony自信VHS不能达到与Beta Hi-Fi相同的音频水平。然而，使SONY感到烦恼的是，JVC约在第一台Sony的Beta
Hi-Fi机种－SL-5200上市后一年基于深多路原理开发出了VHS Hi-Fi系统。尽管它们皆自吹可提供"CD音质"，Beta
Hi-Fi与VHS
Hi-Fi都深受"载体噪声"所害。高频信号会混入音频载体，造成瞬间的嗡嗡声与其他音频缺陷。两种系统亦都采用压缩降噪系统，而这可能在某些情况下产生"抽动"电波。两种格式也都有交换上的问题－在一台机器上录制好的影带并不总是可以在其他机器上正常播放。当发生这种情况时，用户将被迫恢复使用旧的线性音轨。

在1985年，Sony发表了一项新功能－High Band（或称作SuperBeta）。他们再次调整了Y载体800kHz。这增加了Y
sideband可用的带宽，给予普通Betamax录像带290\~300条水平扫描线于此模式。普通的录像机是240\~250条。磁头并减小到29微米以减少漏话。稍后，部份机种更针对Beta
I做更多改进－一个高波段的Beta I
(BI)录制模式。旧的Beta机器与SuperBeta有些地方不兼容，但大多数都可以在不产生严重问题的情况下播放高波段Beta
I影带。SuperBeta机种有个开关，可以让用户为了兼容性关闭SuperBeta模式。

JVC则推出VHS HQ（高品质，High
Quality）－一系列针对VHS格式的改进－以与SuperBeta竟争。最初的HQ机种包含了光杂讯消除回路、色杂讯消除回路、白片段伸展及改进过的视频锐化回路。它达到了使VHS录制分辨率明显提高的效果。主要的VHS
[OEM厂顾虑到成本而拒绝HQ](../Page/OEM.md "wikilink")，最后导致JVC减少HQ的需求到*白片段伸展与一项其他改进*，而"其他改进"则是从JVC或RCA提供的回路中选择。

JVC则在数年后发表了SuperVHS。它有400行水平扫描线。相较于原有的VHS设计，它的写入速度较慢。因此，SuperVHS需要一种特制的高级影带。不像SuperBeta只需要普通的Betamax影带。Sony亦在他们的工业用Betamax生产线上提供了SuperBeta。

[Sony_ED_Beta_and_TDK_Hi8_tape_20071005.jpg](https://zh.wikipedia.org/wiki/File:Sony_ED_Beta_and_TDK_Hi8_tape_20071005.jpg "fig:Sony_ED_Beta_and_TDK_Hi8_tape_20071005.jpg")带。外观有些相似之处\]\]
[ED-Beta-Recordclip.jpg](https://zh.wikipedia.org/wiki/File:ED-Beta-Recordclip.jpg "fig:ED-Beta-Recordclip.jpg")

Sony再一次地以ED（高清，Extended
Definition）Betamax来试探其极限。它可容纳500线分辨率，使用Betacam的金属带。他们更在进带方式上应用了一些改进以减少机械造成的图像偏移。SONY在1980年代末期发布了两款ED机种及一款手持影像机。最高级的ED机种有强大的编辑能力，可媲美U-Matic的准确度，但因为缺乏时间码与其他专业功能而未能有商业上的成功。

尽管在1980年代末期，Betamax录像机的销量锐减，并由于零件调度困难而最终导致SONY自2002年8月27日起停止生产新的Betamax录像机（录像带仍持续生产），这种格式仍被一群人使用着。这些人多半是收藏家或迷恋类似物件的人。新的录像带仍可在[在线商店上买到](../Page/在线商店.md "wikilink")。二手的录像机也常能在[跳蚤市场](../Page/跳蚤市场.md "wikilink")、便宜旧货商店或拍卖网站上找到。

## 与VHS相较

与VHS相比，Betamax有以下的特征及不同点：

  - 磁带盒尺寸较小（传言说是SONY公司职员的笔记本的大小）。
  - 磁带的前进速度较大（对画质较有利）。
  - 初期的机种支援特殊播放。

## 批评

[Betavhs2.jpg](https://zh.wikipedia.org/wiki/File:Betavhs2.jpg "fig:Betavhs2.jpg")
许多由专利性质造成的技术上的缺点使Betamax在与VHS的格式战中占了劣势，尽管Betamax在画质上优于競爭对手。其他[Sony的专利格式如](../Page/Sony.md "wikilink")[Memory
Stick](../Page/Memory_Stick.md "wikilink")、[UMD](../Page/Universal_Media_Disc.md "wikilink")、[Digital8以及](../Page/Digital8.md "wikilink")[ATRAC等亦被类似地批评](../Page/ATRAC.md "wikilink")。

早期，Betamax格式在美国市场碰到的最主要问题是录制时长。最早展示给松下的原型使用40公厘／秒的线性磁带。以当时的技术，由于采用了60微米磁头，必须要这种速度。SONY的工程师与经销部门认为，既然一小时的长度对于U-Matic的消费者来说可以接受，Betamax消费者应该也可以接受。于是Betamax格式有个较小的一小时长的卡带－K-60（后来更名为L-500）。这种卡带内含150公尺长的磁带（接近500英尺，而这就是L-500名称的由来）。

RCA一开始（约在1974年时）曾经计划开发一种叫做"SelectaVision
MagTape"的家用录像格式，但在听闻Sony的Betamax格式的传言之后取消了这个计划，并考虑让Sony做为RCA牌录像机的OEM制造厂。RCA曾与Sony讨论过，但RCA觉得录制时长实在太短了，并坚持他们需要至少4小时的录制时长（据闻这是因为一场电视转播的美式足球赛平均时长是4小时）。Sony的工程师知道制作适合的视频磁头的技术仍未到位，而减慢影带速度与视频轨的宽度是可行的，但是这会使画质严重减低，而SONY的工程师觉得这个牺牲并不值得。

不久之后，RCA与创造VHS（代表"视频螺旋扫描（Video Helical Scan）"，后改为"家用视频系统（Video Home
System）"）的JVC公司的领导会谈。但JVC亦拒绝在画质上妥协以提供4小时录制模式。讽刺的是，他们的母公司－松下－稍后与RCA会谈并同意制造可提供4小时录制模式的机器。这使得JVC相当愤怒。（JVC从未制作可提供4小时"LP"模式的机器。后来他们提供了6小时录制模式，并声称因视频磁头与电路的改进，它的表现比早期的4小时机种更好。）

Sony则发表了"X2"录制模式（20公厘／秒）以使Betamax达到2小时的录制时长。这造成市场上的混淆，因为某些机器只用X2模式录制，其他一些机器甚至不能播放X1的带子。稍后的机种则可以处理一种较薄的磁带。该种磁带在X1模式有90分钟的时长，但许多机器只用X2模式录制。

录影时长代表了一切。Betamax最终让超薄型L-830录像带在BIII（13.3公厘／秒）录制模式下达到5小时的录影时长。而VHS则能在使用T-120磁带时达到6小时、T-180达到9小时，后来还有更长的T-200磁带。较慢的磁带运转速度会使得画质降低，但消费者似乎不在乎。SONY在Betamax刚发表的时候一直无法突破一小时的录制时限。

值得注意的是，在使用[PAL制式的欧洲](../Page/PAL.md "wikilink")，录影时长从不是一个大问题。一卷L-750在PAL制式下提供3.25小时的时长，而相近的E180则-{只}-有3小时，但25分钟的差距不足以做为VHS较受欢迎的引证。

## 传言

近年来，随着一个新的格式战－[藍光光碟与](../Page/藍光光碟.md "wikilink")[HD
DVD](../Page/HD_DVD.md "wikilink")－的发起，一些关于Betamax与VHS格式战的传言开始浮现（尽管两个格式战有许多本质上的差异\[1\]）。许多主流媒体如[CNN经常陈述说成人影业](../Page/CNN.md "wikilink")（色情影片）是使VHS格式获胜的主要原因，因为VHS得到成人影业的支持，而Betamax则被排挤。然而，亦有些反驳此传言的说法：

首先，快速地在Betamax影带库中看过，可以发现以Betamax录制的成人影片能轻易被找到。举例来说，[花花公子在](../Page/花花公子.md "wikilink")1980年代时，同时以两种影带格式出版他们的影片。因此，Betamax的失败不能归咎于缺乏成人影业的支持。

其次，成人影业对于格式选择无法产生长久且强力的作用。依据Forbes.com的数据，影带收入约在5亿至1兆8000亿美元之间。相较于无线电视（1999年达到32兆3千亿）、有线电视（45兆5千亿）、报业（27兆5千亿）、好莱坞（31兆）甚至是专业及教育出版（14兆8千亿）的营收，这显得微不足道。成人影业在Betamax与VHS以及现在蓝光与HD
DVD的格式战中作用是相当小的。\[2\]这样的结果是决定于更重要的因素，例如低价位VHS兼容机的易得性与较长的录制时间（最长至9小时）。

## 流行文化

在[David
Cronenberg于](../Page/David_Cronenberg.md "wikilink")1983年上映的电影*[Videodrome](../Page/Videodrome.md "wikilink")*中，Betamax影带被用于一个邪恶用途－以录制在其中的视频信号做[心灵控制](../Page/心灵控制.md "wikilink")。而在[霹雳游侠第](../Page/霹雳游侠.md "wikilink")17集"Chariot
of
Gold"中，一群想要得到KITT的人观看了一卷录有KITT性能表现的Betamax录像带。然而，至1980年代末期，对Betamax的不普及所开的玩笑开始出现于流行文化中。在1989年播出的一集[情境喜剧](../Page/情境喜剧.md "wikilink")*[Married...
with
Children](../Page/Married..._with_Children.md "wikilink")*中，Bundys家族被描述成"地球上最后一个仍在使用Beta的家庭"。他们必须跨越州界以从"Bob's
Betas and
[Bell-bottoms](../Page/Bell-bottoms.md "wikilink")"店里租用Betamax影带，而且他们-{只}-能租*[Oh
Heavenly
Dog](../Page/Oh_Heavenly_Dog.md "wikilink")*这部片。\[3\]在1990年的小说*[好预兆](../Page/好预兆.md "wikilink")*中，一本17世纪的预言书里写着这么一个警告："不要买Betamacks(Do
Notte Buye
Betamacks)"。\[4\]在一集1992年播出的*[辛普森家族](../Page/辛普森家族.md "wikilink")*中，Snake偷了一台录像机，但在检查的时候他惊叫道"喔不！是Beta\!"<ref>'"The
Simpsons*, "Itchy and Scratchy, The Movie",
[1992-11-03](../Page/1992-11-03.md "wikilink").</ref>
在一集*[Futurama](../Page/Futurama.md "wikilink")''裡，[Mom说她不会永远在这裡](../Page/Mom_\(Futurama\).md "wikilink")，而一台会说话的Betamax播放机回答"Oh
shush"。\[5\]
在一集[星际牛仔卡通影集中](../Page/星际牛仔.md "wikilink")，两个主角必须从一个古老的历史博物馆里找出一台Betamax播放机以播放他们收到的一卷神秘的Betamax录像带。但是就在他们拿回播放机的路上，他们失望地发现那是台VHS录像机。

做为一种众所皆知，已被放弃的[存储媒体Betamax影带常被与已经](../Page/存储媒体.md "wikilink")（或即将）过时的东西相提并论。举例来说，在一集于2003年播出的*辛普森家族*裡，一个垃圾场里放着一堆Betamax录像带、一堆[LD](../Page/LD.md "wikilink")，与一个写着"预留给DVD"的空位。\[6\]类似地，在一集2006年播出的*[爱酷一族](../Page/爱酷一族.md "wikilink")*中描述[Fred
Flintstone是在这样一条时间线上的](../Page/Fred_Flintstone.md "wikilink")：[恐龙](../Page/恐龙.md "wikilink")、Betamax影带、[磁盘以及](../Page/磁盘.md "wikilink")[酸洗过的](../Page/酸洗.md "wikilink")"破裤"。\[7\]

在[攀岩活动中](../Page/攀岩.md "wikilink")，指引攀爬者如何攀爬特定路线被称为"Give
beta"。这是因为攀岩爱好者在Betamax格式仍在被使用时有时会以Betamax影带录下其他攀岩者攀爬该路线的情形，然后回放影带来评定攀岩者的技术以帮助他们将来攀爬同一路线。

## 其他

  - 作为播放用的规格，由于磁带外壳体积过大，导致SONY研发同一类型不同规格的Betacam，用以对抗松下电器的M系列格式，这属于小型机型，靠编辑系统的line
    up等技术占据市场，直到现在为止还和松下主导的M系统对抗，并且处于优势。
  - [神奈川电视台等播放过的](../Page/神奈川电视台.md "wikilink")「[SONY MUSIC
    TV](../Page/SONY_MUSIC_TV.md "wikilink")」在当初，播放时间等同于βII模式最长录像时间的200分钟，附带Betamax的宣传节目（VHS标准模式在当时最长录制时长是120分钟）。
  - 在[动画电影](../Page/动画电影.md "wikilink")「[银河铁道999](../Page/银河铁道999.md "wikilink")」等二部作品被录像软件录制的同时，以同样的理由有着VHS版的删减版（像西方影片一样录制成两盘卡带，总时长明显缩短而成本增加）。而爱好者则会为了追求无删减版而购买整套Betamax影带版。
  - 在日本，SuperBeta被称为Hi-Band。
  - βI与βIs的磁带前进速度一样，βI采用的是线性突出法、而βIs则是与βII/βIII类似，采用非线性突出法，βI模式下录制的卡带在βIs模式下播放会出现闪点等情形。-{只}-能播放βI的旧型VTR，在播放βIs模式录制的卡带时，Hi-Band规格录制下来的卡带会发生反转，突出方面也出现异常，不能播放正常的信号。另外βIs为了特殊播放用的播放磁头，与βI的轨带宽也不相同。
  - Beta
    hi-fi模式所录制下来的卡带，hi-fi音频部份与旧型Beta影像记录位置重叠，因此在旧型Beta机种播放的时候，播放时会有杂音。为使Hi-Band录制的卡带增加磁性，会有不与Hi-Band对应的机种在播放时引入黑尾噪音（回转噪音）的场合。与Hi-Band对应的机种除ED
    Beta外全部的规格，都存在使ED Beta播放全Beta格式的可能。
  - Beta
    hi-fi方式为了记录影像头，音频头装备了独立的VHS-HiFi方式，使得在原理上避免了影像与HiFi音频声画不同步的发生。有益于其它机器播放时也会正常地播放
  - ED
    Beta机器可以播放／录制βII和βIII模式的影带，而-{只}-能播放βI、βIs模式录制的影带。但是ED机种中的最高级机种－EDV-9000可以利用计时器的错误（倒退）来达成βIs录影。这在测试狂人之间是被广泛知道的。

## 附注与来源

## 相关条目

  - [录像带格式战](../Page/录像带格式战.md "wikilink")
  - [Peep search](../Page/Peep_search.md "wikilink") A picture search
    system pioneered with Betamax and available on most video formats
    since.

## 外部链接

  - [Running time of all lengths of Betamax tape, and how to copy the
    film to DVD/miniDV and
    AVI](https://web.archive.org/web/20071006121548/http://www.alivestudios-memories.co.uk/info3.html)
  - [The Ultimate Betamax Info Guide - covering the Betamax format in
    the North American market](http://www.betainfoguide.net)
  - [Mister Betamax - extensive Beta supply
    site](http://www.mrbetamax.com/)
  - [Betamax PALsite - over 350 pages of Betamax information, running
    since 1997](http://betamax.palsite.org)
  - [The 'Total Rewind' VCR museum, covering Betamax and other vintage
    formats](http://www.totalrewind.org)
  - [The Betamax format in the UK, including technical information on
    servicing Sanyo Beta machines](http://www.colin99.co.uk/beta.html)
  - ["Daily Giz Wiz" Podcast discussing the
    Betamax](https://web.archive.org/web/20060818014014/http://aolradio.podcast.aol.com/twit/DGW-030.mp3)
  - [蓝光（BD）与HD
    DVD之争不是第二个Betamax与VHS之争](http://tpemail2002.blog.163.com/blog/static/2510346920091139116930/edit/)
  - [下代DVD综述HD DVD/BD之争陷入僵局](http://dvd.zol.com.cn/new/30/307951.html)

[索尼](../Category/索尼.md "wikilink")
[Category:影像儲存](../Category/影像儲存.md "wikilink")

1.  <http://tpemail2002.blog.163.com/blog/static/2510346920091139116930/edit/>
2.  <http://www.forbes.com/2001/05/25/0524porn.html>
3.  *Married... with Children*第50集，"The Harder They
    Fall"，[1989-03-26](../Page/1989-03-26.md "wikilink")。
4.
5.  *[Futurama](../Page/Futurama.md "wikilink")*, "[Mother's
    Day](../Page/Mother's_Day_\(Futurama\).md "wikilink")",
    [2000-05-14](../Page/2000-05-14.md "wikilink").
6.  *The Simpsons*, "[The Fat and the
    Furriest](../Page/The_Fat_and_the_Furriest.md "wikilink")",
    [2003-11-30](../Page/2003-11-30.md "wikilink").
7.  *The Grim Adventures of Billy and Mandy*, "Modern Primitives",
    [2006-01-27](../Page/2006-01-27.md "wikilink").