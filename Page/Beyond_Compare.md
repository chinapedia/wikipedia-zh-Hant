**Beyond Compare**是一套由[Scooter
Software推出的](../Page/Scooter_Software.md "wikilink")[內容比較工具軟件](../Page/內容比較.md "wikilink")。除了可以作[檔案比較以外](../Page/檔案比較.md "wikilink")，還可以比對檔案目錄、FTP目錄及壓縮檔案的內容等\[1\]。因為這些功能，Beyond
Compare被應用於[版本控制及](../Page/版本控制.md "wikilink")[資料同步的工作上](../Page/資料同步.md "wikilink")。

## 外掛

透過另外下載的外掛，Beyond Compare可以達成以下的各種比較：

  - 機器碼比較
  - 圖片比較
  - [MP3比較](../Page/MP3.md "wikilink")

此外，透過額外安裝的比較規則，亦可作MS Word文本、MS Excel之間的比較。

Beyond
Compare的強項在於可以設定成為[git的difftool或mergetool的預設比較工具](../Page/git.md "wikilink")\[2\]。

## 開發平台

Beyond
Compare是一套以[Delphi及](../Page/Delphi.md "wikilink")[Kylix開發的軟件](../Page/Kylix.md "wikilink")\[3\].。

## 參考資料

## 外部連結

  - [Beyond Compare 主網頁](http://www.scootersoftware.com/index.php)

[Category:内容管理系统](../Category/内容管理系统.md "wikilink")
[Category:应用软件](../Category/应用软件.md "wikilink")
[Category:檔案比較工具](../Category/檔案比較工具.md "wikilink")

1.
2.
3.