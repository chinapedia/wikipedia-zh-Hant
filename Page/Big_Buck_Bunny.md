**大雄兔**（）是[Blender基金會第](../Page/Blender基金會.md "wikilink")2部[開放授權](../Page/內容開放.md "wikilink")、[創作共用的](../Page/創作共用.md "wikilink")[動畫電影](../Page/動畫電影.md "wikilink")，代号Peach。片長10分鐘，Big
Buck
Bunny全部使用[開放源碼軟件製作](../Page/開放源碼.md "wikilink")（如[Blender](../Page/Blender.md "wikilink")、[Linux](../Page/Linux.md "wikilink")），[渲染的](../Page/渲染.md "wikilink")[計算機集群使用](../Page/計算機集群.md "wikilink")[昇陽電腦公司的Sun](../Page/昇陽電腦公司.md "wikilink")
Grid亦是[開放源碼的](../Page/開放源碼.md "wikilink")（如：[OpenSolaris](../Page/OpenSolaris.md "wikilink")、[Sun
Grid Engine等](../Page/Sun_Grid_Engine.md "wikilink")）\[1\]
\[2\]製作技術和素材徹底公開。不同于上一个项目[Elephants
Dream](../Page/Elephants_Dream.md "wikilink")，本篇全程无语音。本片完成之后，其素材适用在Blender官方的游戏项目[Yo
Frankie\!之中](../Page/Yo_Frankie!.md "wikilink")，反派Frankie这次成为主角。

## 圖集

<File:Big.Buck.Bunny.-.Bunny.Portrait.png>|

<center>

主角: 賓妮[兔](../Page/兔.md "wikilink") (Bunny)

</center>

<File:Big.Buck.Bunny.-.Frank.Rinky.Gimera.png>
<File:Big.Buck.Bunny.-.Frank.Bunny.png>
<File:Big.Buck.Bunny.-.Landscape.png>

## 註釋

<references/>

## 其他

  - Apricot：衍生项目[Yo Frankie\!的代号](../Page/Yo_Frankie!.md "wikilink")
  - [Elephants
    Dream](../Page/Elephants_Dream.md "wikilink")：blender基金会的第一个开源电影项目

## 外部連結

  - [Big Buck Bunny官方網站](http://www.bigbuckbunny.org/)

  -
  -
[Category:美國動畫電影](../Category/美國動畫電影.md "wikilink")
[Category:荷蘭電影作品](../Category/荷蘭電影作品.md "wikilink")
[Category:2008年电影](../Category/2008年电影.md "wikilink")

1.
2.