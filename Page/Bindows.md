**Bindows**
是一個[軟體發展套件](../Page/軟體發展套件.md "wikilink")，它使用DHTML，JavaScript，[層疊樣式表和](../Page/層疊樣式表.md "wikilink")[可擴展標記語言的组合来生成可以与现代桌面程序相媲美的具有丰富界面的高交互性的網際網路應用程式](../Page/可擴展標記語言.md "wikilink")。

Bindows 框架基于动态[超文件標示語言](../Page/超文件標示語言.md "wikilink")，编程语言使用
[JavaScript](../Page/JavaScript.md "wikilink")。当 web
应用程序运行时，脚本将以一种被称为应用程序描述文件（ADF）的可擴展標記語言格式从服务器端下载，并在客户端执行。

框架效仿 Swing 编程和 [JavaScript
对象模型](../Page/文档对象模型.md "wikilink")。类名以“Bi”开始，例如
BiObject，BiRadioButton 等。Bindows 的名字来自
[商业智能](../Page/商业智能.md "wikilink") (BI) 和
[视窗](../Page/视窗.md "wikilink") 的组合，BI 是负责创建这个框架的 MB
科技有限公司的一个兴趣点。

Bindows 应用程序不依赖于：

服务器 —— 任何服务器（Java，.NET，PHP 等都可以与 Bindows 结合使用）

后端平台 —— 任何后端硬件和后端软件

操作系统 —— 客户端可以使用任何操作系统（只要它包含一个支持的浏览器）

语言 —— 所有的语言都支持。Bindows 支持 Unicode

浏览器 —— 大部分浏览器都支持（比如：Internet Explorer 5.5 及其更高版本，Mozilla 1.4
及其更高版本，Netscape 7.1 及其更高版本，Firefox，K-Meleon 和 Camino）

Bindows
支持下列窗体小部件：标签，按钮，复选框，单选按钮，文本框，菜单，工具栏，状态栏，工具提示，表格，树，滑块，微调器，进度条和量表。另外，他还支持绘制柱状图、棒状图和图表。

## 版本

  - 0.9  - 2003 年 8 月
  - 1.00 - 2004 年 2 月
  - 1.10 - 2004 年 5 月
  - 1.20 - 2004 年 8 月
  - 1.25 - 2004 年 10 月
  - 1.30 - 2005 年 3 月
  - 1.31 - 2005 年 5 月
  - 1.5b - 2005 年 6 月（测试版）
  - 1.5  - 2005 年 7 月（最新稳定版）

## 外部链接

  - <http://www.bindows.net>
  - <https://web.archive.org/web/20160111213242/http://www.bindows.net.cn/>

[Category:应用软件](../Category/应用软件.md "wikilink")