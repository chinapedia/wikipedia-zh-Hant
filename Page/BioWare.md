**BioWare**，（中国大陆译为“生软”）是一家位于[加拿大](../Page/加拿大.md "wikilink")[埃尔伯塔省省会](../Page/埃尔伯塔省.md "wikilink")[埃德蒙顿市的游戏开发公司](../Page/埃德蒙顿.md "wikilink")，该公司于1995年成立，由、和奧古斯汀·葉共同创立。Bioware开发了一些著名的[RPG游戏](../Page/RPG.md "wikilink")，比如《[博德之门](../Page/博德之门.md "wikilink")》系列、《[无冬之夜](../Page/无冬之夜.md "wikilink")》、《[星球大战：旧共和国武士](../Page/星球大战：旧共和国武士.md "wikilink")》、《[翡翠帝国](../Page/翡翠帝国.md "wikilink")》、《[质量效应系列](../Page/质量效应系列.md "wikilink")》以及《》系列等。正在开发的《质量效应系列》和《龙腾世纪》系列属于跨平台作品，而《[星球大战：旧共和国](../Page/星球大战：旧共和国.md "wikilink")》则是一款基于PC平台的大型多人在线RPG（MMORPG）。在2012年9月前，[Mythic
Entertainment与](../Page/Mythic_Entertainment.md "wikilink")[勝利遊戲为BioWare的一部分](../Page/勝利遊戲.md "wikilink")。\[1\]

## 公司历史

BioWare
于1995年2月由毕业于[阿尔伯塔大学医学院的雷](../Page/阿尔伯塔大学.md "wikilink")·穆吉卡、葛雷格·傑斯曲克以及奧古斯汀·葉共同创建\[2\]公司的第一款游戏诞生于其创建的第二年.
在其独立营运的10年当中，BioWare
开发了《》、《[柏德之門](../Page/柏德之門.md "wikilink")》系列、《[MDK2](../Page/MDK2.md "wikilink")》、《[絕冬城之夜](../Page/絕冬城之夜.md "wikilink")》、《[星球大战：旧共和国武士](../Page/星球大战：旧共和国武士.md "wikilink")》，还有《[翡翠帝国](../Page/翡翠帝国.md "wikilink")》。它和[Interplay](../Page/Interplay_Entertainment.md "wikilink")（通过[黑岛工作室](../Page/黑岛工作室.md "wikilink")）、[英寶格](../Page/英寶格.md "wikilink")／[雅達利](../Page/雅達利.md "wikilink")、[LucasArts及](../Page/LucasArts.md "wikilink")[微软都有过合作发行关系](../Page/微软.md "wikilink").

接下来的几年BioWare发生了一些重大转变.2005年11月，BioWare和[Pandemic
Studios](../Page/Pandemic_Studios.md "wikilink")（由前[动视员工建立](../Page/动视.md "wikilink")）宣布合并，这一举动依靠来自于[Elevation
Partners对其合作伙伴的私有股权投资](../Page/Elevation_Partners.md "wikilink")。然而到了2007年10月11日的时候，这个新的合作组织（整合为
VG Holding Corp）被[美商藝電](../Page/美商藝電.md "wikilink")（EA）收购。\[3\]BioWare
因此成为了EA的子公司，但是继续保留其品牌。（美商藝電以六亿两千万美元收购其母公司Elevation
Partners，同时额外向Elevation
Partners的特定员工提供价值两亿五百万美元的股票作为额外补偿，共耗资八亿两千五百万美元。）

2007年，BioWare发售了科幻RPG《[质量效应](../Page/质量效应.md "wikilink")》。2008年，BioWare发售了[任天堂DS平台的游戏](../Page/任天堂DS.md "wikilink")《[Sonic
Chronicles: The Dark
Brotherhood](../Page/Sonic_Chronicles:_The_Dark_Brotherhood.md "wikilink")》，这是他们的第一款[掌机游戏](../Page/掌机游戏.md "wikilink")。2009年底，BioWare发售了奇幻RPG《[闇龍紀元：序章](../Page/闇龍紀元：序章.md "wikilink")》。2010年1月《[质量效应2](../Page/质量效应2.md "wikilink")》发售。

近日，EA 宣布 BioWare 和其另外一家子公司[Mythic
Entertainment合并](../Page/Mythic_Entertainment.md "wikilink")，以此来把他们所有的RPG游戏整合到一个公司的旗下。

BioWare目前至少在进行四个游戏项目的开发。[MMORPG](../Page/MMORPG.md "wikilink")（大型多人在线角色扮演游戏）《[星球大战：旧共和国](../Page/星球大战：旧共和国.md "wikilink")》基于BioWare之前的《[星球大战](../Page/星球大战.md "wikilink")》游戏风格，\[4\]游戏于2008年10月21日对外公布，而在2007年10月BioWare就曾提起过和LucasArts有一个未公开的项目。\[5\]其他的项目分别是《龙腾世纪》的续作、《质量效应3》以及一个还没有公开细节的项目。\[6\]\[7\]\[8\]

由于EA RPG/MMO业务的增长，他们在2008年以后把三个工作室整合进了**BioWare
Group**。这些工作室都位于埃德蒙顿总部以外的地方。第一个是坐落于[得克萨斯州](../Page/得克萨斯州.md "wikilink")[奥斯丁的工作室](../Page/奥斯丁.md "wikilink")，由游戏行业的老手和[Richard
Vogel共同带领](../Page/Richard_Vogel.md "wikilink")，他们主要负责《星球大战：旧共和国》这个MMORPG项目的开发。2009年3月2日，BioWare在[魁北克省](../Page/魁北克省.md "wikilink")[蒙特利尔设立了一个新的工作室来协助开发已有的项目](../Page/蒙特利尔.md "wikilink")。\[9\]2009年，坐落于[弗吉尼亚州](../Page/弗吉尼亚州.md "wikilink")[費爾法克斯的Mythic](../Page/費爾法克斯_\(維吉尼亞州\).md "wikilink")
Entertainment，成为了EA RPG/MMO Group的一员，并在2010年初被命名为BioWare Mythic。

美商藝電在2009年6月24日宣布，他们要把旗下所有的RPG和MMO游戏整合到一个新的公司下面，这个新的公司包括Mythic
Entertainment还有BioWare。新成立的公司（现在称为BioWare
Group）由BioWare的合作创始人兼总经理雷·穆吉卡领导。因此，穆吉卡成为了这个新公司的总经理。BioWare的另一位合作创始人葛雷格·傑斯曲克成为新公司的创意总监。Rob
Denton则成为新公司的营运总监。BioWare原有的工作室保持不变并由穆吉卡全权负责。\[10\]

新公司同时宣布他们将于2011年在爱尔兰的[戈爾韋设立一家新的客户服务中心](../Page/戈爾韋.md "wikilink")。\[11\]

BioWare也是一个[艾伯塔游戏开发者发家的地方](../Page/艾伯塔.md "wikilink")，比如它曾雇佣HermitWorks
Entertainment的前员工。HermitWorks Entertainment制作了十分受欢迎的混合类型游戏《Space
Trader》，这款游戏的开发过程还获得了[加拿大政府给予的一项艺术授权](../Page/加拿大.md "wikilink")。

在《质量效应3》发售以后，大量的玩家抱怨游戏的结局没有兑现制作者先前对于该游戏作为三部曲的终篇所许下的承诺。为了回应这些批评，BioWare于4月5日宣布他们重新安排了游戏的后续开发计划，并将发布一款名为“扩展剪辑”的DLC，用于扩充原有的结局，主要对那些广受批评的关键点进行了充分的解释。\[12\]扩展剪辑补丁于2012年6月26日免费发布。\[13\]在这个广受争议的事件结束后，福布斯的Erik
Kain写道“这种做法对于游戏产业是健康而积极的，它改写了游戏玩家，制作者以及发行商之间的关系”，他认为BioWare和EA的举措是对粉丝们的抱怨最好的回应，并让玩家们意识到“他们受到了重视，这种做法对于那些有质量的游戏绝对不是一件坏事”。\[14\]

## 游戏

| 名称                                                                                            | 发行年份    | 游戏引擎                                                       | 游戏平台                                                                                                                                                                                                                                                         | 备注                                                    |
| --------------------------------------------------------------------------------------------- | ------- | ---------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------- |
| *[超钢战神](../Page/超钢战神.md "wikilink")（Shattered Steel）*                                         | 1996    |                                                            | [DOS](../Page/DOS.md "wikilink")，[Mac OS](../Page/Mac_OS.md "wikilink")                                                                                                                                                                                      | *一些游戏光盘有缺陷,游戏收藏者手中能正常工作的光盘很少.*                        |
| *[柏德之門](../Page/柏德之門.md "wikilink")（Baldur's Gate）*                                           | 1998    | [Infinity Engine](../Page/Infinity_Engine.md "wikilink")   | [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")，[Mac OS](../Page/Mac_OS.md "wikilink")                                                                                                                                                          |                                                       |
| *[柏德之門：剑湾传奇](../Page/柏德之門：剑湾传奇.md "wikilink")（Baldur's Gate: Tales of the Sword Coast）*       | 1999    | [Infinity Engine](../Page/Infinity_Engine.md "wikilink")   | [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")，[Mac OS](../Page/Mac_OS.md "wikilink")                                                                                                                                                          | *博得之门的资料片*                                            |
| *[MDK2](../Page/MDK2.md "wikilink")*                                                          | 2000    | Omen Engine                                                | [Dreamcast](../Page/Dreamcast.md "wikilink")，[Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")，[PlayStation 2](../Page/PlayStation_2.md "wikilink")，[WiiWare](../Page/WiiWare.md "wikilink")                                                      | *[MDK的续作](../Page/MDK.md "wikilink")*                 |
| *[柏德之門2：安姆疑雲](../Page/柏德之門2：安姆疑雲.md "wikilink")（Baldur's Gate II: Shadows of Amn）*            | 2000    | [Infinity Engine](../Page/Infinity_Engine.md "wikilink")   | [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")，[Mac OS](../Page/Mac_OS.md "wikilink")                                                                                                                                                          |                                                       |
| *[柏德之門2：邪神霸權](../Page/柏德之門2：邪神霸權.md "wikilink")（Baldur's Gate II: Throne of Bhaal）*           | 2001    | [Infinity Engine](../Page/Infinity_Engine.md "wikilink")   | [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")，[Mac OS](../Page/Mac_OS.md "wikilink")                                                                                                                                                          | *博得之门2的资料片*                                           |
| *[絕冬城之夜](../Page/絕冬城之夜.md "wikilink")（Neverwinter Nights）*                                    | 2002    | [Aurora Engine](../Page/Aurora_Engine.md "wikilink")       | [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")，[Linux](../Page/Linux.md "wikilink")，[Mac OS X](../Page/Mac_OS_X.md "wikilink")                                                                                                                 |                                                       |
| *[絕冬城之夜：古城阴影](../Page/絕冬城之夜：古城阴影.md "wikilink")（Neverwinter Nights: Shadows of Undrentide）*   | 2003    | [Aurora Engine](../Page/Aurora_Engine.md "wikilink")       | [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")，[Linux](../Page/Linux.md "wikilink")，[Mac OS X](../Page/Mac_OS_X.md "wikilink")                                                                                                                 | *无冬之夜的资料片*                                            |
| *[絕冬城之夜：幽城魔影](../Page/絕冬城之夜：幽城魔影.md "wikilink")（Neverwinter Nights: Hordes of the Underdark）* | 2003    | [Aurora Engine](../Page/Aurora_Engine.md "wikilink")       | [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")，[Linux](../Page/Linux.md "wikilink")，[Mac OS X](../Page/Mac_OS_X.md "wikilink")                                                                                                                 | *无冬之夜的资料片*                                            |
| *[星球大战：旧共和国武士](../Page/星球大战：旧共和国武士.md "wikilink")（Star Wars: Knights of the Old Republic）*    | 2003    | [Odyssey Engine](../Page/Odyssey_Engine.md "wikilink")     | [Xbox](../Page/Xbox.md "wikilink")，[Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")，[Mac OS X](../Page/Mac_OS_X.md "wikilink")                                                                                                                   |                                                       |
| *[翡翠帝国](../Page/翡翠帝国.md "wikilink")（Jade Empire）*                                             | 2005    | [Odyssey Engine](../Page/Odyssey_Engine.md "wikilink")     | [Xbox](../Page/Xbox.md "wikilink")，[Windows XP](../Page/Microsoft_Windows_XP.md "wikilink")，[Xbox 360](../Page/Xbox_360.md "wikilink")，[Mac OS X](../Page/Mac_OS_X.md "wikilink")                                                                            |                                                       |
| *[质量效应](../Page/质量效应.md "wikilink")（Mass Effect）*                                             | 2007    | [虛幻引擎3](../Page/虛幻引擎3.md "wikilink")                       | [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")，[Xbox 360](../Page/Xbox_360.md "wikilink")                                                                                                                                                      |                                                       |
| *[索尼克编年史：黑暗兄弟会](../Page/索尼克编年史：黑暗兄弟会.md "wikilink")（Sonic Chronicles: The Dark Brotherhood）*  | 2008    |                                                            | [Nintendo DS](../Page/Nintendo_DS.md "wikilink")                                                                                                                                                                                                             |                                                       |
| *[质量效应：银河](../Page/质量效应：银河.md "wikilink")（Mass Effect Galaxy）*                                | 2009    |                                                            | [iOS](../Page/iOS.md "wikilink")                                                                                                                                                                                                                             |                                                       |
| *[龙腾世纪：起源](../Page/龙腾世纪：起源.md "wikilink")（Dragon Age: Origins）*                               | 2009    | [Eclipse Engine](../Page/Eclipse_Engine.md "wikilink")     | [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")，[PlayStation 3](../Page/PlayStation_3.md "wikilink")，[Xbox 360](../Page/Xbox_360.md "wikilink")，[Mac OS X](../Page/Mac_OS_X.md "wikilink")                                                      | *被称为*[博得之门](../Page/博德之门系列.md "wikilink")'' 的精神继承者.'' |
| *[质量效应2](../Page/质量效应2.md "wikilink")（Mass Effect 2）*                                         | 2010    | [虛幻引擎3](../Page/虛幻引擎3.md "wikilink")                       | [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")，[Xbox 360](../Page/Xbox_360.md "wikilink")，[PlayStation 3](../Page/PlayStation_3.md "wikilink")                                                                                                 |                                                       |
| *[龙腾世纪：起源-觉醒](../Page/龙腾世纪：起源-觉醒.md "wikilink")（Dragon Age: Origins – Awakening）*             | 2010    | [Eclipse Engine](../Page/Eclipse_Engine.md "wikilink")     | [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")，[PlayStation 3](../Page/PlayStation_3.md "wikilink")，[Xbox 360](../Page/Xbox_360.md "wikilink")                                                                                                 | *龙腾世纪：起源*的资料片\[15\]                                   |
| *[龙腾世纪2](../Page/龙腾世纪2.md "wikilink")（Dragon Age II）*\[16\]                                   | 2011    | [Lycium Engine](../Page/Lycium_Engine.md "wikilink")\[17\] | [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")，[PlayStation 3](../Page/PlayStation_3.md "wikilink")，[Xbox 360](../Page/Xbox_360.md "wikilink")                                                                                                 | *已发售*\[18\]                                           |
| *[星球大战：旧共和国](../Page/星球大战：旧共和国.md "wikilink")（Star Wars: The Old Republic）*\[19\]             | Q4 2011 | [HeroEngine](../Page/HeroEngine.md "wikilink")             | [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")                                                                                                                                                                                                 | *已发售*                                                 |
| *[質量效應3](../Page/質量效應3.md "wikilink")（Mass Effect 3）*                                         | Q1 2012 | [虛幻引擎3](../Page/虛幻引擎3.md "wikilink")                       | [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")，[PlayStation 3](../Page/PlayStation_3.md "wikilink")，[Xbox 360](../Page/Xbox_360.md "wikilink")                                                                                                 | *已发售*\[20\]                                           |
| *[闇龍紀元：異端審判](../Page/闇龍紀元：異端審判.md "wikilink")（Dragon Age: Inquisition）*                       | 2014    | [寒霜3](../Page/寒霜3.md "wikilink")                           | [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")，[PlayStation 4](../Page/PlayStation_4.md "wikilink")，[PlayStation 3](../Page/PlayStation_3.md "wikilink")，[Xbox One](../Page/Xbox_One.md "wikilink")，[Xbox 360](../Page/Xbox_360.md "wikilink") | ''已发售                                                 |
| *[質量效應：仙女座](../Page/質量效應：仙女座.md "wikilink")（Mass Effect: Andromeda）*                          | 2017    | [寒霜3](../Page/寒霜3.md "wikilink")                           | [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")，[PlayStation 4](../Page/PlayStation_4.md "wikilink")，[Xbox One](../Page/Xbox_One.md "wikilink")                                                                                                 | ''已发售                                                 |
| *[冒險聖歌](../Page/冒險聖歌.md "wikilink")（Anthem）*                                                  | 2019    | [寒霜3](../Page/寒霜3.md "wikilink")                           | [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")，[PlayStation 4](../Page/PlayStation_4.md "wikilink")，[Xbox One](../Page/Xbox_One.md "wikilink")                                                                                                 | ''已发售                                                 |

## 游戏引擎

BioWare开发的[无限引擎](../Page/无限引擎.md "wikilink")，曾是开发基于[2D](../Page/二维计算机图形.md "wikilink")《[龙与地下城](../Page/龙与地下城.md "wikilink")》[角色扮演游戏的核心部件](../Page/電子角色扮演遊戲.md "wikilink")，例如《[柏德之门系列](../Page/柏德之门.md "wikilink")》以及《[冰風之谷系列](../Page/冰風之谷.md "wikilink")》。2002年，BioWare为其新作《[絕冬城之夜](../Page/絕冬城之夜.md "wikilink")》开发了[极光引擎](../Page/极光引擎.md "wikilink")（Aurora
Engine） ，它成为了后来许多很好的电脑（主机）3D游戏的引擎.

《絕冬城之夜》提供的编辑器使得玩家可以建立自己的单机和线上游戏内容。通过这个使用极光引擎的编辑器，不管是业余还是专业的玩家都可以把他们制作的游戏内容放到网络上与其他玩家分享，有一些则是作为附加内容出售。《无冬之夜》是最早的玩家可以把自建MOD通过[网络免费分享或者商业化營運的游戏之一](../Page/网络.md "wikilink")。

[奥德赛引擎](../Page/奥德赛引擎.md "wikilink")（Odyssey
Engine）为[PC](../Page/电脑.md "wikilink") &
[Xbox游戏](../Page/Xbox.md "wikilink")《[星球大战：旧共和国武士](../Page/星球大战：旧共和国武士.md "wikilink")》而开发，它基于极光引擎.
BioWare最新开发的日蚀引擎（Eclipse
Engine）是为《[龙腾世纪：起源](../Page/龙腾世纪：起源.md "wikilink")》而开发的次世代游戏引擎.

BioWare厌倦了多年的授权游戏开发，特别是D\&D游戏，他们开始制作自己原创的独立世界观、风格独特的游戏。《[翡翠帝国](../Page/翡翠帝国.md "wikilink")》是他们迈出的第一步，而他们现在的RPG系列游戏《[质量效应](../Page/质量效应_\(系列\).md "wikilink")》主要针对
[Xbox 360](../Page/Xbox_360.md "wikilink")
和[PC平台](../Page/Microsoft_Windows.md "wikilink")，则是使用了改版的第三代[虚幻引擎](../Page/虚幻引擎.md "wikilink")。

BioWare还授权并协助[黑曜石娱乐开发了](../Page/黑曜石娱乐.md "wikilink")《[絕冬城之夜2](../Page/絕冬城之夜2.md "wikilink")》（Neverwinter
Nights
2）以及《[星球大战：旧共和国武士2：西斯领主](../Page/星球大战：旧共和国武士2：西斯领主.md "wikilink")》（Star
Wars: Knights of the Old Republic II: The Sith Lords）两部作品。

### 极光引擎

极光引擎作为[3D引擎](../Page/三维计算机图形.md "wikilink")，是BioWare早期[2D游戏引擎](../Page/二维计算机图形.md "wikilink")[无限引擎的成功继承者](../Page/无限引擎.md "wikilink")。\[21\]这个引擎用于演算[即时光线和阴影](../Page/即时电脑图像.md "wikilink")，以及[环绕音效](../Page/环绕音效.md "wikilink")。\[22\]第一款使用极光引擎的游戏是《絕冬城之夜》，游戏附带的「[极光引擎编辑器](../Page/极光引擎编辑器.md "wikilink")」可以让玩家通过这个工具来建立自己的游戏内容。《絕冬城之夜》的续作《[絕冬城之夜2](../Page/絕冬城之夜2.md "wikilink")》由黑曜石娱乐开发，使用了极光引擎的升级版[电子引擎](../Page/电子引擎.md "wikilink")（Electron
engine）。《星球大战：旧共和国武士》和《星球大战：旧共和国武士2:
西斯领主》（黑曜石娱乐）则是使用极光引擎的升级版[奥德赛引擎](../Page/奥德赛引擎.md "wikilink")。波兰开发商[CD
Projekt
Red开发的游戏](../Page/CD_Projekt.md "wikilink")《[巫師](../Page/巫師_\(遊戲\).md "wikilink")》，使用的则是一个被全部改写的极光引擎.

### 使用BioWare游戏引擎的第三方游戏

无限引擎:

  - *[异域镇魂曲](../Page/异域镇魂曲.md "wikilink")* (1999)
  - *[冰風之谷](../Page/冰風之谷.md "wikilink")* (2000)
      - *[冰風之谷: 寒冬之心](../Page/冰風之谷:_寒冬之心.md "wikilink")* (2001)
      - *[冰風之谷: 寒冬之心 Trials of the
        Luremaster](../Page/冰風之谷:_寒冬之心_Trials_of_the_Luremaster.md "wikilink")*
        (2001)
  - *[冰風之谷2](../Page/冰風之谷2.md "wikilink")* (2002)

奥德赛引擎:

  - *[星球大战：旧共和国武士2：西斯领主](../Page/星球大战：旧共和国武士2：西斯领主.md "wikilink")*
    (2004)

极光引擎:

  - *[絕冬城之夜2](../Page/絕冬城之夜2.md "wikilink")* (2006)
      - *[絕冬城之夜2: 背叛者的面具](../Page/絕冬城之夜2:_背叛者的面具.md "wikilink")* (2007)
      - *[絕冬城之夜2: 泽希尔风暴](../Page/絕冬城之夜2:_泽希尔风暴.md "wikilink")* (2008)
      - *[絕冬城之夜2: 西门之谜](../Page/絕冬城之夜2:_西门之谜.md "wikilink")* (2009)
  - *[巫師](../Page/巫师_\(游戏\).md "wikilink")* (2007)

## 获奖

除了不计其数的游戏奖项，公司也获得了一系列商业相关的奖项:

  - 盈利 100 – 加拿大成长最快的公司 2005 (排名 81)
  - 2008年10月，BioWare获得Mediacorp Canada
    Inc评选的[阿尔伯特最佳雇主](../Page/阿尔伯特最佳雇主.md "wikilink").
    由[Calgary
    Herald](../Page/Calgary_Herald.md "wikilink")\[23\]和[Edmonton
    Journal共同颁发](../Page/Edmonton_Journal.md "wikilink")。\[24\]\[25\]

所有获奖信息可以到BioWare网站上查阅。\[26\]

  - Spike TV's 2010 Video Game Awards: Studio Of The Year (2010)
  - Hall of Fame induction (2010)\[27\]

## 其他

  - [Black Isle Studios](../Page/Black_Isle_Studios.md "wikilink")
  - [Obsidian
    Entertainment](../Page/Obsidian_Entertainment.md "wikilink")

## 参考资料

## 额外链接

  - [BioWare](http://www.bioware.com/)
  - [BioWare](http://www.mobygames.com/company/bioware-corporation) at
    [MobyGames](../Page/MobyGames.md "wikilink")

[Category:1995年開業電子遊戲公司](../Category/1995年開業電子遊戲公司.md "wikilink")
[BioWare](../Category/BioWare.md "wikilink") [Category:Academy of
Interactive Arts & Sciences
members](../Category/Academy_of_Interactive_Arts_&_Sciences_members.md "wikilink")
[Category:國際遊戲開發者協會成員](../Category/國際遊戲開發者協會成員.md "wikilink")
[Category:加拿大電子遊戲公司](../Category/加拿大電子遊戲公司.md "wikilink")
[Category:電子遊戲開發公司](../Category/電子遊戲開發公司.md "wikilink")
[Category:艺电的部门与子公司](../Category/艺电的部门与子公司.md "wikilink")

1.

2.

3.

4.

5.  [LucasArts.com | LucasArts and BioWare Corp. to Create
    Ground-Breaking Interactive Entertainment
    Product](http://www.lucasarts.com/company/release/news20071030.html)

6.  [ActionTrip Mass Effect
    Interview](http://www.actiontrip.com/previews/360/masseffect_i_2.phtml)


7.

8.

9.

10.

11.

12.

13.

14.

15.

16. [Mass Effect 2 DLC and Dragon
    Age 2](http://www.n4g.com/pc/News-390633.aspx)

17.

18.

19.

20.

21.

22.
23.

24.

25.

26. [BioWare: BioWare Corporate And Community
    Awards](http://www.bioware.com/bioware_info/awards/)

27. USA Today