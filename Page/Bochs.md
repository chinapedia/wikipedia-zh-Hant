**Bochs**（发音：box）是一個以[GNU宽通用公共许可证發放的](../Page/GNU宽通用公共许可证.md "wikilink")[开放源代码的](../Page/开放源代码.md "wikilink")[x86](../Page/x86.md "wikilink")、[x86-64](../Page/x86-64.md "wikilink")[IBM
PC兼容机](../Page/IBM_PC兼容机.md "wikilink")[模擬器和](../Page/模擬器.md "wikilink")[调试工具](../Page/调试工具.md "wikilink")。它支持处理器（包括[保护模式](../Page/保护模式.md "wikilink")），内存，硬盘，显示器，[以太网](../Page/以太网.md "wikilink")，[BIOS](../Page/BIOS.md "wikilink")，[IBM
PC兼容机的常见硬件外设的仿真](../Page/IBM_PC兼容机.md "wikilink")。

许多客户[操作系统能通过该仿真器运行](../Page/操作系统.md "wikilink")，包括[DOS](../Page/DOS.md "wikilink")，[Microsoft
Windows的一些版本](../Page/Microsoft_Windows.md "wikilink"), [AmigaOS
4](../Page/AmigaOS_4.md "wikilink"), [BSD](../Page/BSD.md "wikilink"),
[Linux](../Page/Linux.md "wikilink"),
[MorphOS](../Page/MorphOS.md "wikilink"),
[Xenix和](../Page/Xenix.md "wikilink")[Rhapsody](../Page/Rhapsody.md "wikilink")
(Mac OS X的前身).
Bochs能在许多主机操作系统运行，例如[Windows](../Page/Windows.md "wikilink"),
[Windows Mobile](../Page/Windows_Mobile.md "wikilink"),
[Linux](../Page/Linux.md "wikilink"), [Mac OS
X](../Page/Mac_OS_X.md "wikilink"), [iOS](../Page/iOS.md "wikilink"),
[PlayStation 2](../Page/PlayStation_2.md "wikilink").

Bochs主要用于操作系统开发（当一个模拟操作系统崩溃，它不崩溃主机操作系统，所以可以调试仿真操作系统）和在主机操作系统运行其他来宾操作系统。它也可以用来运行不兼容的旧的软件（如电脑游戏）。

它的優點在於能夠模擬跟主機不同的機種，例如在Sparc系統裡模擬x86，但缺點是它的速度卻慢得多。

## 參見

  - [虚拟机比较](../Page/虚拟机比较.md "wikilink")

## 相關連接

  - [Bochs官方網頁](http://bochs.sourceforge.net)

[Category:自由仿真软件](../Category/自由仿真软件.md "wikilink")
[Category:跨平台軟體](../Category/跨平台軟體.md "wikilink")