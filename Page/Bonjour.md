**Bonjour**，原名**Rendezvous**，是[苹果电脑公司在其开发的](../Page/苹果电脑公司.md "wikilink")[操作系统](../Page/操作系统.md "wikilink")[Mac
OS
X](../Page/Mac_OS_X.md "wikilink")10.2版本之后引入的[服务器搜索](../Page/服务器.md "wikilink")[协议所使用的一个](../Page/协议.md "wikilink")[商标名](../Page/商标.md "wikilink")。适用于[LAN](../Page/LAN.md "wikilink")，
Bonjour使用[多点传送](../Page/多点传送.md "wikilink")[域名系统服务记录来定位各种](../Page/域名系统.md "wikilink")[设备](../Page/设备.md "wikilink")，比如[打印机或者其他](../Page/打印机.md "wikilink")[电脑](../Page/电脑.md "wikilink")，以及另外设备上的服务。

「Bonjour」是[法语](../Page/法语.md "wikilink")，有「你好」或「早（午）安」之意，由「bon」（好）、「jour」（日）兩個辭彙所組成。

## 概要

Bonjour是在[LAN](../Page/LAN.md "wikilink")（局域网）中寻找服务的一个主要方法。这项技术广泛用于Mac
OS X，允许用户不用在任何设置下建立一个网络连接。目前用于在Mac OS
X和其他操作系统上寻找[打印机和文件共享服务器](../Page/打印机.md "wikilink")。还可以用于在[iTunes中寻找共享音乐](../Page/iTunes.md "wikilink")，在[iPhoto寻找共享照片](../Page/iPhoto.md "wikilink")，在[iChat](../Page/iChat.md "wikilink")，[Proteus](../Page/Proteus.md "wikilink")，[Adium](../Page/Adium.md "wikilink")，[Fire](../Page/Fire.md "wikilink")，[Skype和](../Page/Skype.md "wikilink")[Gizmo
Project中寻找本地网络中的其他用户](../Page/Gizmo_Project.md "wikilink")，在[TiVo桌面上寻找视频录制器](../Page/TiVo.md "wikilink")，在[SubEthaEdit和](../Page/SubEthaEdit.md "wikilink")[E中寻找文件协作](../Page/E.md "wikilink")，在[Contactizer中寻找](../Page/Contactizer.md "wikilink")、共享联系人、任务和活动消息。另外还可以用于在[Safari中寻找当地网络服务器和当地设备的设置页面](../Page/Safari.md "wikilink")，用[Asterisk来推广电话服务](../Page/Asterisk.md "wikilink")，设置参数VoIP电话和拨号。[Bonjour
Browser可以用来浏览所有设备和这些程序的服务](../Page/Bonjour_Browser.md "wikilink")。

没有特别的DNS[设置](../Page/设置.md "wikilink")，Bonjour只能在一个单一[子网上运行](../Page/子网.md "wikilink")。

有人对Bonjour有一些误解，认为它会将个人电脑的一些服务（如文件共享）等变成公开的[互联网服务](../Page/互联网.md "wikilink")，引发安全危机问题。而实际上Bonjour并不提供其他额外的接入服务，甚至在用一个局域网（LAN）上；它很少宣称（"推广"）他们的存在。比如，一个用户可以浏览附近电脑上共享文件的列表——这些电脑上的Bonjour已经告诉用户这个服务可以使用，但是他必须进一步提供密码才能接入那些机器上的保密文件。而且，Bonjour只在一个封闭的范围内工作，默认设置中，它的信息只能传给同样子网的其他用户。

## 命名

Bonjour在2002年8月作为[Mac OS X
v10.2一部分发布的时候](../Page/Mac_OS_X#Mac_OS_X_v10.2美洲虎\(Jaguar\).md "wikilink")，原来的名字是"Rendezvous"，在[法语中意思是](../Page/法语.md "wikilink")“约会”。在2003年8月27日，[TIBCO软件公司声称这个名字已经是他们的注册商标](../Page/TIBCO软件公司.md "wikilink")\[1\]
自1994年起TIBCO软件公司已经有一个[商用软件集成产品名叫](../Page/商用软件集成.md "wikilink")[TIBCO
Rendezvous面市](../Page/TIBCO_Rendezvous.md "wikilink")，该公司称试图和苹果公司商讨此事但无果而终。2004年7月，苹果公司和Tibco发布和解方案，\[2\]此前广有传闻称新名字可能是OpenTalk，但是最后没被采用（可能是由于类似[LocalTalk和](../Page/LocalTalk.md "wikilink")[PowerTalk](../Page/Apple_Open_Collaboration_Environment.md "wikilink")）。在2005年4月12日，苹果公司宣布Rendezvous改名为Bonjour,\[3\]，仍是一个[法语词汇](../Page/法语.md "wikilink")。

## 注释

## 外部链接

  - [Bonjour -
    簡化版網路連缐](https://web.archive.org/web/20061209232321/http://www.apple.com.tw/macosx/features/bonjour/)
    蘋果電腦 繁體中文
  - [Bonjour 开发者网页（英文）](http://developer.apple.com/networking/bonjour/)
  - [Zeroconf.org](http://zeroconf.org) - site with myriad useful links
    maintained by Stuart Cheshire
  - [Hour-long
    talk](http://video.google.com/videoplay?docid=-7398680103951126462)
    by Stuart Cheshire on [Google
    Video](../Page/Google_Video.md "wikilink") about Bonjour and
    Zeroconf
  - [Stuart Cheshire and Rendezvous mentioned on Daniel Smith's weblog
    on the O'Reilly
    Network](https://web.archive.org/web/20040624050813/http://www.oreillynet.com/pub/wlg/3137)
  - [Bonjour for
    Windows](http://www.apple.com/support/downloads/bonjourforwindows.html)
    - Bonjour services from Apple for Windows 2000/2003/XP
  - [Understanding Zeroconf and Multicast
    DNS](http://www.oreillynet.com/pub/a/wireless/2002/12/20/zeroconf.html)
    on the [O'Reilly Network](../Page/O'Reilly.md "wikilink") - An
    introduction to zero configuration networking, including a
    comparison between Bonjour/Zeroconf and Universal Plug 'n' Play

[Category:2002年软件](../Category/2002年软件.md "wikilink")
[Category:蘋果公司軟體](../Category/蘋果公司軟體.md "wikilink")
[Category:MacOS](../Category/MacOS.md "wikilink")
[Category:网络协议](../Category/网络协议.md "wikilink")

1.
2.
3.