**Buzy**是[日本的演唱团体](../Page/日本.md "wikilink")。Buzy意为
"**B**lend"，"**U**nique"，"**Z**ipping"，"**Y**ou just
wait"。2004年3月3日凭单曲 "鯨" 在日本流行乐界出道。所属唱片公司为Imperial
Records。2006年6月解散。

## 团员资料

**宮里真央**（*Mao Miyazato*，みやざと まお）

  - 出生日期：1983年4月19日，[大阪府人](../Page/大阪府.md "wikilink")
  - 血型：O型，身高：159.5cm

**當山奈央**（*Nao Toyama*，とうやま なお）：主唱

  - 出生日期：1984年2月22日，[大阪府人](../Page/大阪府.md "wikilink")
  - 血型：A型，身高：168cm

**朝間由利沙**（[日语](../Page/日语.md "wikilink")**朝間ユリサ**，*Yurisa Asama*，あさま
ゆりさ）

  - 出生日期：1984年7月27日，[京都府人](../Page/京都府.md "wikilink")
  - 血型：O型，身高：165.8cm

**丹羽麻由美**（*Mayumi Niwa*，にわ まゆみ）：演唱团的队长

  - 出生日期：1984年9月12日，[大阪府人](../Page/大阪府.md "wikilink")
  - 血型：O型，身高：168cm

**岩永幸子**（*Sachiko Iwanaga*，いわなが さちこ）

  - 出生日期：1986年5月19日，[大阪府人](../Page/大阪府.md "wikilink")
  - 血型：A型，身高：162cm

**竹田侑美**（*Yumi Takeda*，たけだ ゆみ）

  - 出生日期：1986年10月29日，[兵库县人](../Page/兵库县.md "wikilink")
  - 血型：A型，身高：153cm

## 历年作品

### 单曲

[Kujira.jpg](https://zh.wikipedia.org/wiki/File:Kujira.jpg "fig:Kujira.jpg")

1.  鯨 ＜Kujira＞（2004年3月3日）
2.  一人一途 ＜Hitori Ichizu＞（2004年7月7日）
3.  Be Somewhere（2005年1月26日）
4.  パシオン ＜*Pasion*＞（2005年11月9日）

### 专辑

1.  Buzy（2006年1月25日）

## 外部链接

  - [Buzy官方网站](https://web.archive.org/web/20060215055034/http://www.amuse.co.jp/buzy/)

  - [Imperial Records官方网站](http://www.teichiku.co.jp/artist/buzy/)

[Category:日本女子演唱團體](../Category/日本女子演唱團體.md "wikilink")