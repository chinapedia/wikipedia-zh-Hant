**雷利·班·-{zh-hans:金;zh-hk:京;zh-tw:金;}-**（，），藝名**B·B·金**（，來自於Blues
Boy的縮寫），生於[美國](../Page/美國.md "wikilink")[密西西比州](../Page/密西西比州.md "wikilink")，[藍調音樂家](../Page/藍調.md "wikilink")、[吉他手和歌曲作者](../Page/吉他手.md "wikilink")。他是有史以来最伟大的藍調[音乐家之一](../Page/音乐家.md "wikilink")，外号「藍調之王」（The
King of
Blues）。他創立與推展了[電吉他推弦](../Page/電吉他.md "wikilink")、揉弦的技法，2003年在[滾石杂志评选的](../Page/滾石杂志.md "wikilink")[滾石雜誌一百大吉他手](../Page/滾石雜誌一百大吉他手.md "wikilink")，B·B·金位列第3名\[1\]、2011年位列第6名\[2\]，1987年就已經被引入[搖滾名人堂](../Page/搖滾名人堂.md "wikilink")。

## 演艺生涯

### 辉煌岁月

1947年，B·B·金开始为位于[洛杉矶的RPM唱片录制歌曲](../Page/洛杉矶.md "wikilink")。金的很多早期作品都是由[山姆·菲利普斯](../Page/山姆·菲利普斯.md "wikilink")（Sam
Phillips）制作，后来他为太阳唱片工作。他还是[孟菲斯的一个唱片音乐节目主持人](../Page/孟菲斯.md "wikilink")，节目播出时称他为「比尔街布鲁斯男孩」（Beale
Street Blues Boy），后来直接称为「布鲁斯男孩」，缩写B.B.。\[3\]

1950年代，B·B·金成为[节奏布鲁斯音乐重要的一分子](../Page/节奏布鲁斯.md "wikilink")。他的作品包括《你知道我爱你》（*You
Know I Love You*）、《早晨醒来》（*Woke Up This Morning*）、《请爱我》（*Please Love
Me*）、《当我心如锤般跳动》（*When My Heart Beats like a Hammer*）、《你让我不安，宝贝》（*You
Upset Me Baby*）、《每天我都拥有布鲁斯》（*Every Day I Have the Blues*）、《逃走》（*Sneakin'
Around*）、《十年》（*Ten Long Years*）、，《坏运气》（*Bad Luck*）、《可爱的小安琪儿》（*Sweet
Little Angel*）和《请接受我的爱》（*Please Accept My Love*）等。

1962年，他签约ABC唱片公司。

1964年11月，B·B·金在[芝加哥雷格尔剧院录制了专辑](../Page/芝加哥.md "wikilink")《雷格尔现场音乐会》（）。
[BBKing07.JPG](https://zh.wikipedia.org/wiki/File:BBKing07.JPG "fig:BBKing07.JPG")
1969年，B·B·金为[罗伊·霍金斯](../Page/罗伊·霍金斯.md "wikilink")（）创作的《激情已逝》（）在流行和节奏布鲁斯两项排行榜都登上榜首，这也是他第一次在布鲁斯以外的领域获得成功。这首歌曲在滚石杂志评选的最佳500首摇滚歌曲榜排名183位\[4\]。

1970年代，他佳作不断，如《爱你才想了解你》（*To Know You Is to Love You*）和「我希望生命里有爱」（*I Like
to Live the Love*）。

从1951年到1985年，B·B·金在《[告示牌雜誌](../Page/告示牌雜誌.md "wikilink")》的节奏布鲁斯排行榜上出现了74次之多\[5\]。

### 后期作品

1980年代、1990年代和2000年代B·B·金录制的唱片越来越少，与之同时，在电视节目中出现得越来越多。他参加了多部电视节目的演出，包括《[天才老爹](../Page/天才老爹.md "wikilink")》、《[年轻与躁动](../Page/年轻与躁动.md "wikilink")》（）、《[新鲜王子妙事多](../Page/新鲜王子妙事多.md "wikilink")》、《[奉子成婚](../Page/奉子成婚.md "wikilink")》（）、《[桑福德和儿子](../Page/桑福德和儿子.md "wikilink")》（）等；以及电影《[蓝调兄弟2000](../Page/蓝调兄弟2000.md "wikilink")》（）等\[6\]。

1988年，他和[爱尔兰](../Page/爱尔兰.md "wikilink")[U2樂團联手推出的单曲](../Page/U2樂團.md "wikilink")《当爱降临》（*When
Love Comes To
Town*），受到歌迷欢迎。2000年，B·B·金和[埃里克·克莱普顿合作推出专辑](../Page/埃里克·克莱普顿.md "wikilink")《天王競飆》（*Riding
With the King*）。

2003年，B·B·金在[新泽西州和Phish乐队同台演出](../Page/新泽西州.md "wikilink")，他一共演奏了3首经典曲目，时间超过30分钟。

2004年，B·B·金参加了克莱普顿发起的十字路口中心吉他节，为中心义演筹款。在后期发行的DVD里收录了他和埃里克·克莱普顿、[巴迪·盖伊](../Page/巴迪·盖伊.md "wikilink")、[吉米·沃恩一起演奏的](../Page/吉米·沃恩.md "wikilink")《摇滚我吧，宝贝！》（*Rock
Me Baby*）。

### 告别演出

2005年3月29日，当时已经80岁的B·B·金在[谢菲尔德的哈兰姆体育场举办了告别演唱会](../Page/谢菲尔德.md "wikilink")，这是他英国和欧洲告别巡演的第一站。英国的最后一站是4月4日在[伦敦的](../Page/伦敦.md "wikilink")[温布萊体育场举行](../Page/温布萊体育场.md "wikilink")。

7月，B·B·金回到欧洲，这次演唱会在[瑞士举行](../Page/瑞士.md "wikilink")，连演两场。11月和12月，B·B·金在[巴西演出六场](../Page/巴西.md "wikilink")。

11月29日，[圣保罗的记者会上](../Page/聖保羅_\(巴西\).md "wikilink")，B·B·金被问及是否这是他最后的演出，他说：

2015年5月14日，B·B·金在美國[拉斯維加斯過世](../Page/拉斯維加斯.md "wikilink")，享年89歲\[7\]。

## 轶事

  - B·B·金拥有[飞行员的执照](../Page/飞行员.md "wikilink")，是一个[赌徒和](../Page/赌博.md "wikilink")[素食主義者](../Page/素食主義.md "wikilink")，他不吸烟也不喝酒\[8\]。[糖尿病困扰他多年](../Page/糖尿病.md "wikilink")，他经常出现在糖尿病相关产品的[广告中](../Page/广告.md "wikilink")。

<!-- end list -->

  - [拳击手](../Page/拳擊.md "wikilink")[索尼·里斯顿](../Page/索尼·里斯顿.md "wikilink")（Sonny
    Liston）是B·B·金的叔叔\[9\]。

<!-- end list -->

  - 他最喜欢的歌手是[法蘭克·辛纳屈](../Page/法蘭克·辛纳屈.md "wikilink")。在他的自传中他描述自己每天晚上都听辛纳屈的歌曲入睡。他认为辛纳屈为黑人音乐人打开了一道门，使他们有机会在白人统治舞台演出\[10\]。

## 荣誉和奖项

### 荣誉

  - 1990年，B·B·金荣获美国[国家艺术奖章](../Page/国家艺术奖章.md "wikilink")\[11\]。
  - 1995年，B·B·金荣获[肯尼迪中心荣誉奖](../Page/肯尼迪中心荣誉奖.md "wikilink")\[12\]。
  - 2004年，[密西西比大学授予B](../Page/密西西比大学.md "wikilink")·B·金荣誉博士学位，[瑞典皇家音乐学会由于B](../Page/瑞典皇家音乐学会.md "wikilink")·B·金在布鲁斯音乐的杰出贡献，授予他[保拉音乐奖](../Page/保拉音乐奖.md "wikilink")\[13\]。
  - 2006年12月15日，[乔治·沃克·布什](../Page/乔治·沃克·布什.md "wikilink")[总统授予B](../Page/总统.md "wikilink")·B·金[总统自由勋章](../Page/总统自由勋章.md "wikilink")\[14\]。

### 葛莱美奖

  - B·B·金迄今为止一共获得过9次葛莱美最佳传统布鲁斯专辑奖，分别为：
      - 1984年《布鲁斯和爵士》（*Blues 'N' Jazz*）
      - 1986年《我的吉他轻吟布鲁斯》（*My Guitar Sings the Blues*）
      - 1991年《圣昆丁现场音乐会》（*Live at San Quentin*）
      - 1992年《阿波罗现场音乐会》（**）
      - 1994年《布鲁斯之巅》（*Blues Summit*）
      - 2000年《海湾布鲁斯》（*Blues on the Bayou*）
      - 2001年《天王競飆》（**）
      - 2003年《一场为了希望的圣诞狂欢》（*A Christmas Celebration of Hope*）
      - 2006年《B·B·金和朋友们：80》（**）

<!-- end list -->

  - 1971年，他获得葛莱美最佳R\&B男歌手奖。
  - 1982年，他的专辑《肯定有更美好的世界》（）获得葛莱美最佳民族或传统乡村专辑奖，该奖项自1986年后停止颁发。
  - 1987年，他荣获[葛莱美终身成就奖](../Page/葛莱美终身成就奖.md "wikilink")。
  - 1997年，他获得葛莱美最佳搖滾器乐演奏奖。
  - 2001年，他获得葛莱美最佳流行合唱团奖。
  - 2003年，他获得葛莱美最佳器樂演奏奖\[15\]。

## 参考

<references/>

## 外部链接

  - [B·B·金官方网站](http://www.bbking.com/)

  - [B·B·金唱片目录](http://www.bluescritic.com/BBKing.htm)

  - [B·B·金的吉他](https://web.archive.org/web/20070927201531/http://www.gibson.com/products/gibson/lucille/Lucille.html)

  - [B·B·金英国粉丝网站](http://www.bluesboyking.com/)

  -
  -
  - [B·B·金巴黎演唱会](http://www.volubilis.net/concerts/bb_king_2006/concert_bb_king_18092006_01.php)



[Category:葛莱美奖获得者](../Category/葛莱美奖获得者.md "wikilink")
[Category:美国吉他手](../Category/美国吉他手.md "wikilink")
[Category:葛萊美終身成就獎獲得者](../Category/葛萊美終身成就獎獲得者.md "wikilink")
[Category:摇滚名人堂入选者](../Category/摇滚名人堂入选者.md "wikilink")
[Category:20世纪歌手](../Category/20世纪歌手.md "wikilink")
[Category:密西西比州人](../Category/密西西比州人.md "wikilink")
[Category:非洲裔美国歌手](../Category/非洲裔美国歌手.md "wikilink")
[Category:美國素食主義者](../Category/美國素食主義者.md "wikilink")
[Category:美國創作歌手](../Category/美國創作歌手.md "wikilink")
[Category:死于糖尿病的人](../Category/死于糖尿病的人.md "wikilink")

1.  [2003年滚石杂志：最伟大的一百名吉他手](http://www.rollingstone.com/news/story/5937559/the_100_greatest_guitarists_of_all_time)
2.  [2011年滾石雜誌：最偉大的一百名吉他手](http://www.rollingstone.com/music/lists/100-greatest-guitarists-20111123)
3.
4.  <http://www.rollingstone.com/news/story/11028260/the_rs_500_greatest_songs_of_all_time>
    滚石杂志最佳500首摇滚歌曲榜
5.  [布告牌杂志关于B.B.金介绍](http://www.billboard.com/bbcom/bio/index.jsp?pid=76520&aid=826972)
6.
7.
8.  [吉他世界关于B.B.金介绍](http://www.guitarworld.com/allaccess/interviews/bb-king.html)

9.  \["The Devil and Sonny Liston" by Nick Tosches, 2000, ISBN
    0-316-89775-2\]
10. Blue All Around Me, 1999, BB King and Daniel Ritz
11. [美国国家艺术奖章1990年获奖名单](http://www.nea.gov/honors/medals/medalists_year.html#90)

12. [肯尼迪中心荣誉终身成就奖网站关于B·B·金介绍](http://www.kennedy-center.org/calendar/index.cfm?fuseaction=showIndividual&entitY_id=3696&source_type=A)

13. [保拉音乐奖网站](http://www.polarmusicprize.se/)
14. [总统自由奖章获奖名单](http://www.senate.gov/pagelayout/reference/two_column_table/Presidential_Medal_of_Freedom_Recipients.htm)
15. [葛莱美官方网站B·B·金获奖列表](http://www.grammy.com/GRAMMY_Awards/Winners/Results.aspx?title=&winner=B.+B.+King&year=0&genreID=0&hp=1)