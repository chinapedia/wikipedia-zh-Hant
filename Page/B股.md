**B股**（**B
share**）在许多国家的[股市都存在](../Page/股市.md "wikilink")，可以是[普通股或](../Page/普通股.md "wikilink")[優先股](../Page/優先股.md "wikilink")。

## 欧美

## 香港

**B股**是[香港股票市場中一種](../Page/香港交易所.md "wikilink")[股票分類](../Page/股票.md "wikilink")，與其相對的是A股。兩者分別在於B股的面值多是A股的五分之一至十分之一，但兩者的投票權是相等的。所以於1970年代、1980年代很多公司皆曾發行，務求以最少的金錢控制公司，但這也不是無敵的，只要有股東持有足夠的A股，便可召開[股東大會](../Page/股東.md "wikilink")，取消B股上市地位。

### B股的出現及取消

[香港第一個想出發行B股來控制上市公司的是](../Page/香港.md "wikilink")[會德豐大班](../Page/會德豐.md "wikilink")[約翰·馬登於](../Page/約翰·馬登.md "wikilink")1960年代末至1970年代初，因收購[連卡佛及](../Page/連卡佛.md "wikilink")[聯邦地產](../Page/聯邦地產.md "wikilink")，使股權被攤薄及財困，遂用一種当时西方大部分股票市場已禁用的方法——發行B股來籌集資金。1972年[會德豐旗下上市公司皆於同一時期發行B股](../Page/會德豐.md "wikilink")，1973年[太古洋行亦跟隨做法](../Page/太古股份有限公司.md "wikilink")。直至1987年3月27日[怡和集團於發年度業績時宣佈除發](../Page/怡和集團.md "wikilink")[股息外](../Page/股息.md "wikilink")，還以[紅股方式送出B股](../Page/紅股.md "wikilink")，當時流傳著[怡和集團旗下公司被狙擊](../Page/怡和集團.md "wikilink")，使[長江實業及](../Page/長江實業.md "wikilink")[和記黃埔於](../Page/和記黃埔.md "wikilink")3月31日公佈除先前公佈的股息外，另加B股。[凱瑟克與](../Page/凱瑟克.md "wikilink")[李嘉誠家族在股票市場的舉動](../Page/李嘉誠.md "wikilink")，一時間使十多間上市公司擬仿效及民間爭議；最後隨著4月7日[長江實業](../Page/長江實業.md "wikilink")、[和記黃埔及](../Page/和記黃埔.md "wikilink")[怡和集團先後取消發行B股計劃](../Page/怡和集團.md "wikilink")，風波才得以平息。1989年12月[香港聯合交易所修例訂明除特殊情況外](../Page/香港聯合交易所.md "wikilink")，不再考慮B股上市。\[1\]

在小米集团上市之前，[太古股份有限公司的B股股份為碩果僅存的B股](../Page/太古股份有限公司.md "wikilink")，太古A和太古B的股票編號分別是00019.HK和00087.HK。A股與B股所佔權益及獲派發股息是1:5，但投票權相等。有人建議AB股應該合併，即使沒有B股，[施懷雅家族所持股份仍能控制太古公司](../Page/施懷雅家族.md "wikilink")，但太古公司表示相關法律程序的成本很高，沒有此打算。

2013年，[香港交易所提出不同投票權架構](../Page/香港交易所.md "wikilink")，並展開諮詢，但建議不獲證監會支持，更有人認為是把監管制度倒退回幾十年前。其後，[阿里巴巴因股权结构问题弃港赴美上市](../Page/阿里巴巴集团.md "wikilink")，引发香港各界对股权结构的讨论。\[2\]2015年，[香港證監會委員基於公眾利益一致反對](../Page/香港證監會.md "wikilink")「同股不同權」，香港交易所上市委員會擱置撰寫建議草案。

### B股的恢复

随着新加坡于2017年允许「同股不同權」，以及中国科网公司上市热潮，加上[林鄭月娥政府的推动](../Page/林鄭月娥政府.md "wikilink")，香港各界再次对股权结构进行讨论。香港政府倾向支持双重股权上市结构，目前倾向在主板引入「同股不同权」，获得了[香港交易所和香港證監會的支持](../Page/香港交易所.md "wikilink")。\[3\]雖然被不少市民甚至證劵商批評罔顧小股東利益，但措施始終在2018年4月30日開始生效，首間以此種方式上市的公司是[小米集團](../Page/小米集團.md "wikilink")。

### 「同股不同权」的公司

[太古股份有限公司](../Page/太古股份有限公司.md "wikilink")、[小米集团](../Page/小米集团.md "wikilink")
- W、[美團點評](../Page/美團點評.md "wikilink") - W

### 曾發B股的公司 \[4\]

[會德豐](../Page/會德豐.md "wikilink")、[連卡佛](../Page/連卡佛.md "wikilink")、[香港置業信託](../Page/香港置業信託.md "wikilink")、[太古](../Page/太古.md "wikilink")、[格蘭酒店集團](../Page/格蘭酒店集團.md "wikilink")、[聯邦地產](../Page/聯邦地產.md "wikilink")

## 中国大陆

**B股**也称为**[人民币特种股票](../Page/人民币特种股票.md "wikilink")**。是指那些在中国注册、在中国上市的特种股票。以[人民币标明面值](../Page/人民币.md "wikilink")，只能以外币认购和交易。上海B股以[美元結算](../Page/美元.md "wikilink")，深圳B股則以[港元結算](../Page/港元.md "wikilink")。1991年第一只B股[上海电真空B股发行](../Page/上海电真空B股.md "wikilink")。

B股不是实物股票，以[无纸化电子记帐](../Page/无纸化电子记帐.md "wikilink")，实行“[T+3](../Page/T+3.md "wikilink")”交割制度，有涨跌幅（10%）限制，参与投资者为香港、澳门和外国人，持有合法外汇存款的大陆居民也可投资。

## 相关条目

  - [A股](../Page/A股.md "wikilink")
  - [G股](../Page/G股.md "wikilink")
  - [H股](../Page/H股.md "wikilink")
  - [N股](../Page/N股.md "wikilink")
  - [S股](../Page/S股.md "wikilink")

## 參考資料

[Category:股市](../Category/股市.md "wikilink")

1.  [同股不同權 玩殘管治 - 金融講ED: 渾水 -
    am730](https://www.am730.com.hk/column/%E8%B2%A1%E7%B6%93/%E5%90%8C%E8%82%A1%E4%B8%8D%E5%90%8C%E6%AC%8A-%E7%8E%A9%E6%AE%98%E7%AE%A1%E6%B2%BB-73100)
2.  [阿里巴巴放弃在香港上市](http://www.bjnews.com.cn/finance/2013/10/11/286792.html)
3.  [香港政府倾向支持双重股权上市结构](http://finance.sina.com.cn/7x24/2017-10-23/doc-ifymzksi1206015.shtml)
4.  [以古論今：同股不同權 小米不是第一間？ │米羅](https://tokuhon.blog/?p=8449)