**Chips and Technologies**， 也稱**Chips &
Technologies**，簡稱**C\&T**，美國半導體業者。為[無廠半導體公司](../Page/無廠半導體公司.md "wikilink")（fabless
semiconductor company）營運模式獨立，IBM
兼容晶片與獨立[顯示晶片的開創者](../Page/顯示晶片.md "wikilink")。1997年被[英特爾購併](../Page/英特爾.md "wikilink")。

## x86系列產品

C\&T Super386 38600DX/38600SX 為自行開發的[386
中央處理器](../Page/Intel_80386.md "wikilink")。其 38605DX 具有 512KB
緩存，但由於封裝方式為 144-pin
PGA，與其它主要廠商如[Intel與](../Page/Intel.md "wikilink")[AMD不兼容](../Page/AMD.md "wikilink")，無法成為市場主流。

C\&T SuperMath J38700DX
為[80387DX兼容](../Page/Intel_80387.md "wikilink")[FPU](../Page/浮點運算.md "wikilink")[處理器](../Page/處理器.md "wikilink")。

## 顯示晶片產品

C\&T 為全球第一個提出 IBM
兼容的獨立顯示晶片（82C451）的廠商，啟動[顯示晶片與](../Page/顯示晶片.md "wikilink")[顯示卡市場](../Page/顯示卡.md "wikilink")。其後由於
3D 繪圖核心的瓶頸遲遲無法突破而衰退。

在被[Intel購併前](../Page/Intel.md "wikilink")，C\&T
的顯示晶片仍具有最好的平面顯示器點屏支持，能支援多樣化的各式顯示屏如
[EL](../Page/EL.md "wikilink")，[STN](../Page/STN.md "wikilink")，[DSTN](../Page/DSTN.md "wikilink")，[TFT](../Page/TFT.md "wikilink")，[等離子](../Page/等離子.md "wikilink")（plasma）等。能將顯示晶片做到最小尺寸的
17x17mm 並內建 4MB
[顯存](../Page/顯存.md "wikilink")，且並無一般顯示晶片的劇烈發熱。廣泛適用於[工業電腦與](../Page/工業電腦.md "wikilink")[嵌入式電腦等特殊電腦應用領域](../Page/嵌入式電腦.md "wikilink")。直到被[Intel購併後](../Page/Intel.md "wikilink")，後者只想取得其顯示技術，無心繼續發展與支持產品，以致無法提出[Windows
2000以後的驅動程式終告淡出市場](../Page/Windows_2000.md "wikilink")。

**C\&T 65 與 69 系列顯示晶片**

C\&T65545 16-bit ISA 或 32-bit VL 總線的顯示晶片，具 32-bit 繪圖核心，支持 1MB 外接式顯存。

C\&T65550 32-bit VL 或 32-bit PCI 總線的顯示晶片，具 64-bit 繪圖核心，支持 2MB 外接式顯存。

C\&T65554 與 C\&T65550 類似，BGA 封裝，支持 4MB 外接式顯存。

C\&T65555 與 C\&T65554 類似，支持 3.3V 低電壓運行。

C\&T69000 PCI 或 AGP 總線的顯示晶片，具 2MB 內建式顯存。有 27x27 或 17x17mm 兩種封裝方式。

C\&T69030 與 C\&T65554 類似，具 4MB 內建式顯存。有 27x27 或 17x17mm 兩種封裝方式。

## 外部連結

  - [Asiliant Technologies](http://www.asiliant.com)：Intel 授權生產與銷售 C\&T
    系列產品（西元2000年迄今）。

[Category:美國電腦公司](../Category/美國電腦公司.md "wikilink")