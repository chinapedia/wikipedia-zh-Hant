「**CAN'T BUY MY
LOVE**」是[日本唱作女歌手](../Page/日本.md "wikilink")[YUI](../Page/YUI.md "wikilink")，於2007年4月4日所推出的第二張大碟。由Sony
Music Records發行。

## 收錄歌曲

| 發行               | 排行榜       | 最高位     | 首周銷量    | 總銷量 |
| ---------------- | --------- | ------- | ------- | --- |
| 2007年4月4日        | Oricon 日榜 | 1       |         |     |
| Oricon 週榜        | 1         | 290,640 | 682,585 |     |
| Oricon 月榜        | 2         |         |         |     |
| Oricon 年榜 (2007) | 8         |         |         |     |
| Oricon 年榜 (2008) | 202       |         |         |     |

[Category:2006年音樂專輯](../Category/2006年音樂專輯.md "wikilink")
[Category:YUI音樂專輯](../Category/YUI音樂專輯.md "wikilink")
[Category:2007年Oricon專輯週榜冠軍作品](../Category/2007年Oricon專輯週榜冠軍作品.md "wikilink")