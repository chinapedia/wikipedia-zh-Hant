[EthernetCableGreen.jpg](https://zh.wikipedia.org/wiki/File:EthernetCableGreen.jpg "fig:EthernetCableGreen.jpg")
**6類雙絞線**（英文：Category 6
cable），一般稱為**CAT-6線**，是在[千兆乙太網以及其它](../Page/吉比特以太网.md "wikilink")[CAT-5](../Page/CAT-5.md "wikilink")／[CAT-3](../Page/CAT-3.md "wikilink")[向下兼容網路上所使用到的傳輸線材標準](../Page/向下兼容.md "wikilink")。相比起CAT-5纜線，CAT-6纜線加強了對抗[串擾及系統](../Page/串擾.md "wikilink")[雜訊的防護](../Page/雜訊.md "wikilink")。而在規格上，它的訊號傳輸[頻率高達](../Page/頻率.md "wikilink")250MHz，適合用於[10BASE-T](../Page/10BASE-T.md "wikilink")、[100BASE-TX及](../Page/100BASE-TX.md "wikilink")[1000BASE-T等各種](../Page/1000BASE-T.md "wikilink")[乙太網傳送標準](../Page/乙太網.md "wikilink")。在短距離內，CAT-6甚至可用作架構[萬兆乙太網](../Page/萬兆乙太網.md "wikilink")。

與部份[TIA標準的線材同是以四對雙絞線組成](../Page/TIA.md "wikilink")，而根據ANSI/TIA-568-B.2-1內的標準，CAT-6線的線徑可由22[AWG至](../Page/AWG.md "wikilink")24AWG，比CAT-5線的規格更有彈性。

## Category 6a

為增強雙絞線的傳輸能力，[TIA於](../Page/TIA.md "wikilink")2008年2月發表的ANSI/TIA/EIA-568-B.2-10中，列明**CAT-6a**（或稱**Augmented
Category 6**，而Augmented是指增強）的傳輸[頻率可高達](../Page/頻率.md "wikilink")500
MHz，是CAT-6線的兩倍。並且在100米內，提供10GBASE-T的萬兆乙太網。

## 連接插頭與其他資訊

而在接線上，CAT-6線與CAT-5線也是普遍用上8P8C的接法，而常用的插頭也是[RJ45](../Page/RJ45.md "wikilink")。

[Rj45plug-8p8c.png](https://zh.wikipedia.org/wiki/File:Rj45plug-8p8c.png "fig:Rj45plug-8p8c.png")插頭及針頭編號\]\]

| 接頭 | 雙絞線組 | 線 | 顏色                                                                                                                                       |
| -- | ---- | - | ---------------------------------------------------------------------------------------------------------------------------------------- |
| 1  | 3    | 1 | [Wire_white_green_stripe.svg](https://zh.wikipedia.org/wiki/File:Wire_white_green_stripe.svg "fig:Wire_white_green_stripe.svg") 白綠    |
| 2  | 3    | 2 | [Wire_green.svg](https://zh.wikipedia.org/wiki/File:Wire_green.svg "fig:Wire_green.svg") 綠                                              |
| 3  | 2    | 1 | [Wire_white_orange_stripe.svg](https://zh.wikipedia.org/wiki/File:Wire_white_orange_stripe.svg "fig:Wire_white_orange_stripe.svg") 白橙 |
| 4  | 1    | 2 | [Wire_blue.svg](https://zh.wikipedia.org/wiki/File:Wire_blue.svg "fig:Wire_blue.svg") 藍                                                 |
| 5  | 1    | 1 | [Wire_white_blue_stripe.svg](https://zh.wikipedia.org/wiki/File:Wire_white_blue_stripe.svg "fig:Wire_white_blue_stripe.svg") 白藍       |
| 6  | 2    | 2 | [Wire_orange.svg](https://zh.wikipedia.org/wiki/File:Wire_orange.svg "fig:Wire_orange.svg") 橙                                           |
| 7  | 4    | 1 | [Wire_white_brown_stripe.svg](https://zh.wikipedia.org/wiki/File:Wire_white_brown_stripe.svg "fig:Wire_white_brown_stripe.svg") 白棕    |
| 8  | 4    | 2 | [Wire_brown.svg](https://zh.wikipedia.org/wiki/File:Wire_brown.svg "fig:Wire_brown.svg") 棕                                              |

8P8C 接線（[TIA/EIA-568-A](../Page/TIA/EIA-568-A.md "wikilink") T568A）

| 接頭 | 雙絞線組 | 線 | 顏色                                                                                                                                       |
| -- | ---- | - | ---------------------------------------------------------------------------------------------------------------------------------------- |
| 1  | 2    | 1 | [Wire_white_orange_stripe.svg](https://zh.wikipedia.org/wiki/File:Wire_white_orange_stripe.svg "fig:Wire_white_orange_stripe.svg") 白橙 |
| 2  | 2    | 2 | [Wire_orange.svg](https://zh.wikipedia.org/wiki/File:Wire_orange.svg "fig:Wire_orange.svg") 橙                                           |
| 3  | 3    | 1 | [Wire_white_green_stripe.svg](https://zh.wikipedia.org/wiki/File:Wire_white_green_stripe.svg "fig:Wire_white_green_stripe.svg") 白綠    |
| 4  | 1    | 2 | [Wire_blue.svg](https://zh.wikipedia.org/wiki/File:Wire_blue.svg "fig:Wire_blue.svg") 藍                                                 |
| 5  | 1    | 1 | [Wire_white_blue_stripe.svg](https://zh.wikipedia.org/wiki/File:Wire_white_blue_stripe.svg "fig:Wire_white_blue_stripe.svg") 白藍       |
| 6  | 3    | 2 | [Wire_green.svg](https://zh.wikipedia.org/wiki/File:Wire_green.svg "fig:Wire_green.svg") 綠                                              |
| 7  | 4    | 1 | [Wire_white_brown_stripe.svg](https://zh.wikipedia.org/wiki/File:Wire_white_brown_stripe.svg "fig:Wire_white_brown_stripe.svg") 白棕    |
| 8  | 4    | 2 | [Wire_brown.svg](https://zh.wikipedia.org/wiki/File:Wire_brown.svg "fig:Wire_brown.svg") 棕                                              |

8P8C 接線（[TIA/EIA-568-B](../Page/TIA/EIA-568-B.md "wikilink") T568B）

{{-}}

### 影音

在2008年的[中國數字家庭產業高峰會上](../Page/中國數字家庭產業高峰會.md "wikilink")，[DIVA](../Page/数字高清互动接口.md "wikilink")（Digital
Interface for Video and Audio）推廣聯盟使用了一條CAT-6線進行影音傳輸。\[1\]
使CAT-6線的用途變得更廣。

## 參考

## 參見

[Category:網路技術](../Category/網路技術.md "wikilink")
[Category:訊號傳輸線](../Category/訊號傳輸線.md "wikilink")
[Category:網路硬體](../Category/網路硬體.md "wikilink")

1.  [DIVA推廣聯盟正式成立 利用CAT6線進行影音傳輸－電腦領域
    HKEPC](http://www.hkepc.com/?id=1206&fs=c1n)