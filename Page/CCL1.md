**CCL1**（）是一小分子的[细胞因子属于CC](../Page/细胞因子.md "wikilink")[趋化因子家族](../Page/趋化因子.md "wikilink")，是由活化的[T细胞分泌的](../Page/T细胞.md "wikilink")[糖蛋白](../Page/糖蛋白.md "wikilink")\[1\]。CCL1与细胞表面的趋化因子受体[CCR8结合](../Page/CCR8.md "wikilink")\[2\]。CCL1对[单核细胞](../Page/单核细胞.md "wikilink")，[自然杀伤细胞](../Page/自然杀伤细胞.md "wikilink")，未成熟的[B细胞和](../Page/B细胞.md "wikilink")[树突状细胞有细胞趋化作用](../Page/树突状细胞.md "wikilink")\[3\]。人类的CCL1[基因与许多CC趋化因子的基因相邻聚集在第](../Page/基因.md "wikilink")17染色体上\[4\].

## 参见

  - [趋化因子](../Page/趋化因子.md "wikilink")
  - [CCR8](../Page/CCR8.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [Charo IF, Ransohoff RM. The many roles of chemokines and chemokine
    receptors in inflammation. N Engl J Med. 2006
    Feb 9;354(6):610-21.](http://content.nejm.org/cgi/content/extract/354/6/610)

[Category:细胞因子](../Category/细胞因子.md "wikilink")
[Category:免疫学](../Category/免疫学.md "wikilink")

1.  Miller MD, Krangel MS. The human cytokine I-309 is a monocyte
    chemoattractant. Proc Natl Acad Sci U S A. 1992 Apr 1;89(7):2950-4.
2.  Roos RS, Loetscher M, Legler DF, Clark-Lewis I, Baggiolini M, Moser
    B. Identification of CCR8, the receptor for the human CC chemokine
    I-309. J Biol Chem. 1997 Jul 11;272(28):17251-4.
3.  Miller MD, Krangel MS. The human cytokine I-309 is a monocyte
    chemoattractant. Proc Natl Acad Sci U S A. 1992 Apr 1;89(7):2950-4.
4.  Miller MD, Wilson SD, Dorf ME, Seuanez HN, O'Brien SJ, Krangel MS.
    Sequence and chromosomal location of the I-309 gene. Relationship to
    genes encoding a family of inflammatory cytokines. J Immunol. 1990
    Oct 15;145(8):2737-44.