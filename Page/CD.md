[Interference-colors.jpg](https://zh.wikipedia.org/wiki/File:Interference-colors.jpg "fig:Interference-colors.jpg")
[Small_cdisk_ubt.jpeg](https://zh.wikipedia.org/wiki/File:Small_cdisk_ubt.jpeg "fig:Small_cdisk_ubt.jpeg")
**-{zh-hans:激光唱片;zh-hant:雷射唱片}-**，又稱**-{zh-hans:雷射唱片;zh-hant:激光唱片}-**（，縮寫：**CD**），是一種用以儲存數位資料的[-{zh-hans:光学盘片;
zh-hant:光學碟片;}-](../Page/光学盘片.md "wikilink")，原被開發用作儲存[數位音訊](../Page/數位音訊.md "wikilink")。CD在1982年面世，至今仍然是商業錄音的標準[儲存裝置](../Page/儲存裝置.md "wikilink")。

在CD尚未發明之前，音響系統都是屬於-{zh-hans:[模拟信号](../Page/模拟信号.md "wikilink");
zh-hk:[模擬訊號](../Page/模擬訊號.md "wikilink");
zh-tw:[類比訊號](../Page/類比訊號.md "wikilink")}-，音樂的來源大多是30公分直徑的[密紋唱片](../Page/密紋唱片.md "wikilink")、收音機以及錄音機等，CD發明之前就沒有數位音響。

## 歷史

CD光碟機起始於1974年，由[荷蘭的](../Page/荷蘭.md "wikilink")[飛利浦公司與](../Page/飛利浦.md "wikilink")[日本的](../Page/日本.md "wikilink")[Sony合作所發表的](../Page/索尼.md "wikilink")[音樂光碟](../Page/音樂光碟.md "wikilink")（Audio
CD），亦稱為[CD-DA](../Page/CDDA.md "wikilink")（Compact Disc-Digital
Audio）；從此之後，因其它儲存媒體市場的發展而連續推出一系列的光碟規格與產品。

CD原本僅是為了家電、唱片市場所設計，並沒有想到CD將來可以用於[電腦的用途](../Page/電腦.md "wikilink")。當時電腦的資料儲存還在5.25吋的[磁片階段](../Page/磁片.md "wikilink")，包括3.5吋的磁碟亦尚未被發明。

CD技術其後被用作儲存資料，稱為[CD-ROM](../Page/CD-ROM.md "wikilink")546。可錄式光碟隨後面世，包括只可錄寫一次的[CD-R及可重複錄寫的](../Page/CD-R.md "wikilink")[CD-RW](../Page/CD-RW.md "wikilink")，直至2007年為止，成為[個人電腦業界最為廣泛採用的儲存媒體之一](../Page/個人電腦.md "wikilink")。CD及其衍生格式取得極大的成功，2004年，全球Audio
CD、CD-ROM、CD-R、CD-RW等的合計總銷量達到300億張。

自CD出現在音響市場之後，30公分直徑的模擬制式LP唱片就開始慢慢隱退。以後所有有關CD的同類產品，包括[DVD與](../Page/DVD.md "wikilink")[藍光光碟](../Page/藍光光碟.md "wikilink")，均是由此衍生的。

## 物理分析

[CD_layers.svg](https://zh.wikipedia.org/wiki/File:CD_layers.svg "fig:CD_layers.svg")
CD的基本規格（根據1983年9月的Audio-CD 標準紅皮書）：

  - 掃瞄速度：1.2–1.4 m/s，固定線速度/[CLV](../Page/CLV.md "wikilink")（Constant
    Linear Velocity）- 約相等於碟內環每分鐘500轉，碟外環每分鐘200轉。
  - 軌距 (Track pitch)：1.65 微米
  - 碟直徑：124 毫米
  - 碟厚度：1.2 毫米
  - 內半徑面積：24 毫米
  - 外半徑面積：358 毫米
  - 讀寫速度：1X = 150 KiB/s = 153,600 Bytes/s
  - 容量：700MB

[Print_vs._bytes.jpg](https://zh.wikipedia.org/wiki/File:Print_vs._bytes.jpg "fig:Print_vs._bytes.jpg")

最初，[飛利浦和](../Page/飛利浦.md "wikilink")[索尼打算製造一種播放時間為](../Page/索尼.md "wikilink")60分鐘，直徑為100毫米（索尼）或115毫米（飛利浦）的數碼光碟，但時為索尼副社長的[大賀典雄堅持](../Page/大賀典雄.md "wikilink")「不能讓[歌劇在幕中中斷](../Page/歌劇.md "wikilink")，並且需得納入[貝多芬](../Page/貝多芬.md "wikilink")[第九交響曲](../Page/第9號交響曲_\(貝多芬\).md "wikilink")。若不是使用者能接受的媒體那就沒意義了。」\[1\]
由於已知最長演奏為[福特萬格勒](../Page/福特萬格勒.md "wikilink")1951年拜魯特音樂節的九號是市面上貝多芬九號中的74分鐘錄音版本\[2\]，最終雷射唱片定為能容納74分42秒音樂的120毫米光碟。

### 音频CD

音频CD（Audio-CD）包括一條或以上的[立體聲音軌](../Page/立體聲.md "wikilink")（Track），以16[bit](../Page/位元.md "wikilink")
[PCM編碼](../Page/脉冲编号调变.md "wikilink")，[採樣率為](../Page/採樣率.md "wikilink")44.1[kHz](../Page/kHz.md "wikilink")。標準CD的直徑為120毫米或80毫米，120毫米CD可儲存約80分鐘的音频。80毫米的鐳射唱片，有時被用作發行單曲鐳射唱片（CD
singles），則可儲存約20分鐘的聲音資料。

音樂CD采用“交叉交错的[里德-所罗门码](../Page/里德-所罗门码.md "wikilink")（）”与“[八比十四調變](../Page/八比十四調變.md "wikilink")（EFM编码）”保证所刻内容的质量。\[3\]

## 保養

無論日常使用還是在環境暴露的情況下，-{zh-tw:光碟;zh-cn:光盤}-都很容易受到損害。由於小坑更接近於碟片標籤的一面，所以在播放時，儘管在乾淨面上的缺口和污垢容易成為焦點而被當做數據錯誤地解析出來，但是划痕可以用折射率相近的塑料填充或通過精心打磨而去除。相對地，當CD在標籤面受到刮擦時受到的損害實際更大。

## 參看

[光碟片的發展歷史.jpg](https://zh.wikipedia.org/wiki/File:光碟片的發展歷史.jpg "fig:光碟片的發展歷史.jpg")，[DVD](../Page/DVD.md "wikilink")4.7G，[DVD](../Page/DVD.md "wikilink")8.5G，[VCD](../Page/VCD.md "wikilink")，[錄影帶](../Page/錄影帶.md "wikilink")，[LD](../Page/LD.md "wikilink")，[錄音帶](../Page/錄音帶.md "wikilink")\]\]

  - [DVD](../Page/DVD.md "wikilink")
  - [Blu-ray](../Page/Blu-ray.md "wikilink")
  - [HD-DVD](../Page/HD-DVD.md "wikilink")
  - [AVCD](../Page/AVCD.md "wikilink")
  - [SACD](../Page/SACD.md "wikilink")
  - [HQCD](../Page/HQCD.md "wikilink")
  - [K2HQCD](../Page/K2HQCD.md "wikilink")
  - [LPCD](../Page/LPCD.md "wikilink")
  - [DVD-Audio](../Page/DVD-Audio.md "wikilink")
  - [CD-ROM](../Page/CD-ROM.md "wikilink")
  - [CD-R](../Page/CD-R.md "wikilink")
  - [CD-RW](../Page/CD-RW.md "wikilink")
  - [CD Text](../Page/CD_Text.md "wikilink")
  - [CD Video](../Page/CD_Video.md "wikilink")
  - [Video Single Disc](../Page/Video_Single_Disc.md "wikilink")
  - [音频文件格式](../Page/音频文件格式.md "wikilink")
  - [聲音儲存](../Page/聲音儲存.md "wikilink")
  - [彩虹書](../Page/彩虹書.md "wikilink")
      - [紅皮書](../Page/紅皮書.md "wikilink")（音訊CD）
      - [黃皮書](../Page/黃皮書.md "wikilink")（CD-ROM）
  - [CD+G](../Page/CD-G.md "wikilink")
  - [ECD](../Page/Enhanced_CD.md "wikilink")
  - [Video CD](../Page/Video_CD.md "wikilink")
  - [SVCD](../Page/SVCD.md "wikilink")
  - [CD盒](../Page/CD盒.md "wikilink")（Jewel case）
  - [HDCD](../Page/HDCD.md "wikilink")
  - [XRCD](../Page/XRCD.md "wikilink")
  - [miniCD](../Page/miniCD.md "wikilink")

## 参考文献及注释

[光碟](../Category/光碟.md "wikilink")
[Category:1982年初次投入使用](../Category/1982年初次投入使用.md "wikilink")
[Category:聲音儲存](../Category/聲音儲存.md "wikilink")
[Category:數位音訊](../Category/數位音訊.md "wikilink")
[Category:高端音频](../Category/高端音频.md "wikilink")
[Category:影像儲存](../Category/影像儲存.md "wikilink")
[Category:荷兰发明](../Category/荷兰发明.md "wikilink")
[Category:日本发明](../Category/日本发明.md "wikilink")

1.  [Sony Japan | Sony
    History　第8章　「レコードに代わるものはこれだ」](http://www.sony.co.jp/SonyInfo/CorporateInfo/History/SonyHistory/2-08.html)
2.  [Optical Recording: Beethoven's Ninth Symphony of greater importance
    than technology
    (cache)](https://web.archive.org/web/20080129201342/http://www.research.philips.com/newscenter/dossier/optrec/beethoven.html),
    Philips
3.