**CD8受體**（）是[细胞毒性T细胞的](../Page/细胞毒性T细胞.md "wikilink")[膜上標記](../Page/膜上標記.md "wikilink")（surface
marker）之一。當病菌入侵人體，有一部分必定會被廣佈的抗原呈現細胞（此時主要指非B細胞的[巨噬細胞及](../Page/巨噬細胞.md "wikilink")[棘狀細胞](../Page/棘狀細胞.md "wikilink")）給吞噬，其餘病菌則順利潛入正常的體細胞中。病菌無論是進入正常體細胞或抗原呈現細胞，其構成[抗原的部分必會與細胞內的](../Page/抗原.md "wikilink")[MHC
complex結合而表現在細胞外](../Page/主要组织相容性复合体.md "wikilink")。

抗原呈現細胞與正常體細胞不同處，就在於抗原呈現細胞能「移動」至最近的[淋巴結](../Page/淋巴結.md "wikilink")。在淋巴結中有許多CD8序列相異的、「不」具毒殺能力的T細胞，當其中一種（可能或通常不只一種）CD8
T細胞對抗原「有反應」，該T細胞便開始分裂、並成熟為毒殺型T細胞。毒殺型T細胞在周遊人體時若「碰巧」（或是受發炎反應物質吸引）遇到一個受感染的正常細胞，其表面有如前所述的「抗原與MHC的結合體」，則該T細胞便會「毒殺」受感染的細胞。嚴格來說，毒殺型T細胞在「啟動」受感染的細胞的「[自殺程式](../Page/细胞凋亡.md "wikilink")」（apoptosis）後便會離開。毒殺型T細胞既不「殺」（如[自然杀伤细胞](../Page/自然杀伤细胞.md "wikilink")），也不「吞」（如巨噬細胞）。

## 參見

  - [T細胞](../Page/T細胞.md "wikilink")

## 参考文献

## 外部連結

  - [T-cell Group - Cardiff
    University](http://www.tcells.org/scientific/CD8/)
  - [Mouse CD Antigen
    Chart](http://www.ebioscience.com/ebioscience/whatsnew/mousecdchart.htm)
  - [Human CD Antigen
    Chart](http://www.ebioscience.com/ebioscience/whatsnew/humancdchart.htm)

[Category:免疫学](../Category/免疫学.md "wikilink")