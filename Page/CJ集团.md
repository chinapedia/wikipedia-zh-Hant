**CJ公司**（，又译**希杰公司**），是一家总部位于[韩国](../Page/韩国.md "wikilink")[首尔的大型跨国公司](../Page/首尔.md "wikilink")，主要业务包括食品餐饮、娱乐传媒、家庭购物与物流、生物工程。\[1\]CJ集团起源于韩国[三星集团的](../Page/三星集团.md "wikilink")“第一制糖”，其名也源自於第一製糖的羅馬拼音字首(**C**heil
**J**edang)。1990年代，三星集团进行了高强度的产业结构调整後，拆分出第一製糖，但經營者仍為三星集团家族成員。\[2\]

韩国知名娱乐公司[Mnet Media](../Page/Mnet_Media.md "wikilink")、韩国最大的电影分销商[CJ
E\&M](../Page/CJ_E&M.md "wikilink")\[3\]和韩国最大的连锁影院[CJ
CGV都是希杰集团旗下的子公司](../Page/CJ_CGV.md "wikilink")。

## 发展历史

CJ集团起源于三星集团的“第一制糖”，於1997年4月正式完成法律上的集團分離。

1980年代，CJ从食品事业拓展到[生物工程及](../Page/生物工程.md "wikilink")[制药事业](../Page/制药.md "wikilink")，1986年CJ以发酵合成技术为基础研发出[B性肝炎](../Page/乙型肝炎.md "wikilink")[疫苗](../Page/疫苗.md "wikilink")，1988年CJ在[印度尼西亚成立了法人](../Page/印度尼西亚.md "wikilink")，主要进行进行[MSG](../Page/MSG.md "wikilink")、[赖氨酸](../Page/赖氨酸.md "wikilink")、[核苷酸生产](../Page/核苷酸.md "wikilink")。印尼法人的成立，标志着CJ跨国经营成功的第一步。1991年，作为CJ主干事业的食品制造业在同行业内最先突破了1兆韩元销售额的纪录，走上快速发展壮大的道路。1994年，CJ继续进军餐饮服务事业。至此，作为CJ主干的食品事业派生出了生物工程及制药事业和餐饮事业。

1996年，国家顾客满意度最高的CGV（CJ Golden
Village）多元化电影院成立，成为Culture、Great、Vital的代言，为观众呈现“超越电影的感动”。1997年，CJ收购了韩国代表性的音乐广播电台Mnet
Media，将音乐娱乐节目出口到[日本](../Page/日本.md "wikilink")、[中国](../Page/中国.md "wikilink")、[新加坡等地](../Page/新加坡.md "wikilink")，促进了亚洲文化事业之间的交流和合作。

为了促进中韩两国政治、经济、文化间的交流，2004年CJ成立了[中華TV](../Page/中華TV.md "wikilink")，成为韩国最早也是唯一一个中文電視節目频道，也是韩国有线电视台中唯一一个播放[中國中央電視台新闻和](../Page/中國中央電視台.md "wikilink")[SMG财经节目的频道](../Page/SMG.md "wikilink")。2005年，《[人民日报](../Page/人民日报.md "wikilink")》评价中华电视台为“韩国最好的专门播放中国节目的中华电视台”。同时，中华电视台也通过开展同[人民日报社等主要媒体间的讨论会](../Page/人民日报社.md "wikilink")，积极促进了两国媒体之间的交流。

CJ
E\&M致力成为亚洲No.1传媒集团，旗下共拥有18个有线频道，节目内容包括娱乐、[电影](../Page/电影.md "wikilink")、[时尚](../Page/时尚.md "wikilink")、[音乐](../Page/音乐.md "wikilink")、[动画](../Page/动画.md "wikilink")、[游戏等](../Page/游戏.md "wikilink")，2010年打造了韩国史上最成功的国民选秀节目《Superstar
K》。1995年，CJ在韩国最先开展了从生产者到消费者的家庭购物服务，通过网络、目录、实体店等方式进行全方位销售。配合家庭购物的发展，1998年CJ
GLS成立，进军物流配送领域，事业链条再次延长。1999年，CJ在韩国本土也创建了Oliveyoung健康美容产品便利店。

1995年是CJ集团核心事业结构基本定型的一年，其后的15年是各个事业在韩国国内成长壮大并进军国际市场，实现全球化扩张的时期。[欧美地区以](../Page/欧美.md "wikilink")[美国](../Page/美国.md "wikilink")、[墨西哥为主](../Page/墨西哥.md "wikilink")，[亚洲以中国](../Page/亚洲.md "wikilink")、日本、印尼、新加坡、[越南](../Page/越南.md "wikilink")、[印度为主](../Page/印度.md "wikilink")，并在全世界范围内将中国作为了最核心、最重要的海外事业基地。自2003年開始，希杰集團的網上購物坊“CJMall”成立，並在各[電視及網上媒體廣泛宣傳](../Page/電視.md "wikilink")。2010年CJ集团整体销售额达到了1000亿人民币，2013年的目标为2200亿人民币，并实现中国事业占据整体的30%的目标。

在事业发展的同时，CJ集团从自身的事业范围出发，积极履行着社会责任，主要集中在[教育](../Page/教育.md "wikilink")、[文化两个领域](../Page/文化.md "wikilink")。1999年，CJ集团设置了专门的社会贡献部门，以“人间爱•自然爱•文化爱”为理念，展开了一系列的社会活动。
2005年7月设立了“CJ分享基金会”，主要通过“爱心奉献营”、“食物银行”、“社会志愿活动”等，2010年为止累计社会贡献资金达到6.8亿人民币。
为了赞助专业性文化艺术事业，2006年5月设立了
“CJ文化基金会”，展开了CJ中国电影节、青年表演艺术节、韩国“画音”室内乐团等活动，2009年设立CJ
Azit新一代艺术人才援助体系，累计赞助资金达2.2亿人民币。

2013年4月1日，在收購韓國國內第一物流公司大韓通運與原集團內物流公司CJ GLS合併，成立希杰大韓通運，

2016年，希杰和马来西亚[首要媒体合作](../Page/首要媒体.md "wikilink")，创立了新的电视购物节目《CJ Wow
Shop》。同样也开张了邮递产品的公司－MP CJ O Shopping。

2017年1月8日，首要媒体为CJ Wow
Shop设立中文版本，为马来西亚华裔提供另一个电视购物频道，但不提供部分马来商品，并在之后纳入以中文节目为主的八度空间里，其他电视台如TV3、NTV7、TV9和Tonton网站则继续播出马来文版本。

2018年，首要媒体和CJ WOW SHOP决定开全购物频道，并在数码电视Freeview和MY TV播出。

2018年12月30日，首要媒体在NTV7再次为CJ WOW SHOP设立中文版本，而NTV7马来文版本的联播正式取消。

## 關連企業

[Tous_Les_Jours.jpg](https://zh.wikipedia.org/wiki/File:Tous_Les_Jours.jpg "fig:Tous_Les_Jours.jpg")店\]\]

  -
  - [CJ家居購物](../Page/CJ家居購物.md "wikilink")

  - [CJ宅配](../Page/CJ宅配.md "wikilink")

  - [CJ大韓通運](../Page/CJ大韓通運.md "wikilink")([CJ Korea
    Express](../Page/CJ_Korea_Express.md "wikilink"))

  - [CJ娛樂](../Page/CJ娛樂.md "wikilink")

  - [CJ-CGV](../Page/CJ-CGV.md "wikilink")

  - [Mnet媒體](../Page/Mnet媒體.md "wikilink")

  - [Morning World](../Page/Morning_World.md "wikilink")

  - [第一東健產業](../Page/第一東健產業.md "wikilink")

  - [多乐之日](../Page/多乐之日.md "wikilink")

  - [必品阁](../Page/必品阁.md "wikilink")

  - [Lucy Entertainment](../Page/Lucy_Entertainment.md "wikilink")

  - [Big Hit娛樂](../Page/Big_Hit娛樂.md "wikilink")

  - [Netmarble Games](../Page/Netmarble_Games.md "wikilink")

## 參看

  - [財閥](../Page/財閥.md "wikilink")

## 参考资料

## 外部連結

  -

  -

  -

  -

  - 中国

<!-- end list -->

  - 马来西亚CJ Wow Shop

[Category:1953年成立的公司](../Category/1953年成立的公司.md "wikilink")
[Category:韩国公司](../Category/韩国公司.md "wikilink")
[Category:餐饮公司](../Category/餐饮公司.md "wikilink")
[Category:物流公司](../Category/物流公司.md "wikilink")

1.  [CJ中国 - 概况](http://www.cjchina.net/)
2.
3.