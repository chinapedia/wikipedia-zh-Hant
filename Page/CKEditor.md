**CKEditor**，舊稱**FCKeditor**，是一個專門使用在網頁上屬於[開放原始碼的](../Page/開放原始碼.md "wikilink")[所見即所得](../Page/所見即所得.md "wikilink")[文字編輯器](../Page/文字編輯器.md "wikilink")。它志於輕量化，不需要太複雜的安裝步驟即可使用。它可和[PHP](../Page/PHP.md "wikilink")、[JavaScript](../Page/JavaScript.md "wikilink")、[ASP](../Page/Active_Server_Pages.md "wikilink")、[ASP.NET](../Page/ASP.NET.md "wikilink")、[ColdFusion](../Page/ColdFusion.md "wikilink")、[Java](../Page/Java.md "wikilink")、以及[ABAP等不同的程式語言相結合](../Page/ABAP.md "wikilink")。「FCKeditor」名稱中的「FCK」
是這個編輯器的作者的名字Frederico Caldeira Knabben的縮寫。

FCKeditor 相容於絕大部分的網頁瀏覽器，像是：[Internet
Explorer](../Page/Internet_Explorer.md "wikilink") 5.5+
(Windows)、[Mozilla Firefox](../Page/Mozilla_Firefox.md "wikilink")
1.0+、[Mozilla](../Page/Mozilla.md "wikilink")
1.3+、[Opera](../Page/Opera.md "wikilink")9.50+和[Netscape](../Page/Netscape.md "wikilink")
7+。

FCKeditor大概已经存在超过10年了。自从2003年它建立了一个强大的用户群体从而成为市场中用户最多的编辑器，累计超过7,500,000个用户下载。在2009年FCKeditor更名为CKEditor,为了让它成为下一代轻量级的解决方案：CKEditor
3.0\[1\]

## 參考文獻

## 外部連結

  - 官方資源

<!-- end list -->

  - [Free Download FCKeditor](http://www.hangren.com/FCKeditor.zip)
  - [FCKeditor web site](http://www.fckeditor.net/)
  - [Official documentation](http://wiki.fckeditor.net) (Wiki)
  - [SourceForge
    Forums](https://web.archive.org/web/20081218015705/http://sourceforge.net/forum/?group_id=75348)
  - [SourceForge page](http://sourceforge.net/projects/fckeditor/)

<!-- end list -->

  - 非官方資源

<!-- end list -->

  - [Tutoriales sobre
    FCKeditor](http://www.baluart.net/categoria/fckeditor) (Español)

[Category:免费软件](../Category/免费软件.md "wikilink")
[Category:HTML编辑器](../Category/HTML编辑器.md "wikilink")
[Category:网页设计软件](../Category/网页设计软件.md "wikilink")
[Category:自由软件](../Category/自由软件.md "wikilink")

1.