星河**COCO
Park**，是中國大陸的大型购物[商场品牌](../Page/商场.md "wikilink")，由深圳市星河房地产经营有限公司管理，目前在[深圳有](../Page/深圳.md "wikilink")3家分店，及在[常州有](../Page/常州.md "wikilink")1家分店。

## 福田店

[COCO_Park_Inside2.jpg](https://zh.wikipedia.org/wiki/File:COCO_Park_Inside2.jpg "fig:COCO_Park_Inside2.jpg")
Coco
Park福田店邻近[深圳会展中心及](../Page/深圳会展中心.md "wikilink")[深圳地铁](../Page/深圳地铁.md "wikilink")[购物公园站](../Page/购物公园站.md "wikilink")，於2006年9月30日正式对外开放。商场楼高5层，面积达85,000平方米。商场设有[百老匯院線](../Page/百老匯院線.md "wikilink")，提供5个影院，合共774座位的戏院，整个项目总投资达6.8亿[人民币](../Page/人民币.md "wikilink")。

### 商场设计

据资料显示，COCO
Park为[中国国内首个以](../Page/中国.md "wikilink")「内街」概念设计的大型购物商场，其中四分之一的面积被设计为休闲空间。商场中央设有6000平方米的下沉式露天广场，可作不同类型推广活动之用。露天广场附近的100米空中天桥，让游人可以从该处欣赏广场的景色。此外，商场四面臨街，合共有9個入口，內部更設有5个大型中庭，每個中庭的布局色调也各有不同。

### 簡介

COCO
Park设有多种类型商店，集餐饮、购物、休闲、娱乐于一身。商场地库为1万多平方米的[吉之岛超级市场](../Page/吉之岛.md "wikilink")、个人护理店、礼品店及珠宝配饰店。商场一楼主要以名牌服装、珠宝、配饰店为主，还设有由中国移动通信所開設之展覽館
- 信息生活館（U-life gallery）。商场二楼主要以运动服饰、时尚服装、特色餐厅为主。商场三楼为高级食肆及酒吧为主。

### 主要商戶

  - B1（地庫，與地鐵[購物公園站連接](../Page/購物公園站.md "wikilink")）：[永輝Bravo及超級物種](../Page/永輝超市.md "wikilink")、[肯德基](../Page/肯德基.md "wikilink")、[仙踪林](../Page/仙踪林.md "wikilink")、[美心西餅](../Page/美心西餅.md "wikilink")、[華潤堂](../Page/華潤堂.md "wikilink")、[Skechers斯凯奇](../Page/Skechers斯凯奇.md "wikilink")、[Uniqlo](../Page/Uniqlo.md "wikilink")
  - L1：[周大福](../Page/周大福.md "wikilink")、[施華洛世奇](../Page/施華洛世奇.md "wikilink")、[匯豐銀行](../Page/匯豐銀行.md "wikilink")、[中國移動通信信息生活館](../Page/中國移動通信.md "wikilink")、[Starbucks](../Page/Starbucks.md "wikilink")、[面包新語](../Page/面包新語.md "wikilink")、[Uniqlo](../Page/Uniqlo.md "wikilink")、[西西弗書店](../Page/西西弗書店.md "wikilink")、[喜茶](../Page/喜茶.md "wikilink")
  - L2：[和民居食屋](../Page/和民居食屋.md "wikilink")、[百老匯戲院](../Page/百老匯戲院.md "wikilink")、[Nike](../Page/Nike.md "wikilink")、[Walker
    Shop](../Page/Walker_Shop.md "wikilink")、[李寧](../Page/李寧.md "wikilink")
  - L3：鑫泰泰國料理、超級牛扒Super Steak

<File:COCO> ParkOutside.jpg|COCO Park 露天廣場

<File:COCO> Park BroadwayCinema.jpg|百老匯影城

### 公共交通

COCO
Park商场位於[深圳地铁](../Page/深圳地铁.md "wikilink")[购物公园站上盖](../Page/购物公园站.md "wikilink")，因此商场下方设有地铁前往[罗湖区及](../Page/罗湖区.md "wikilink")[福田口岸](../Page/福田口岸.md "wikilink")，吸引不少[香港人前往该商场购物](../Page/香港人.md "wikilink")。附近有多条巴士路线途經COCO
Park商场。

  - [深圳地铁](../Page/深圳地铁.md "wikilink")

<!-- end list -->

  - [购物公园站](../Page/购物公园站.md "wikilink")

  - [购物公园站](../Page/购物公园站.md "wikilink")

<!-- end list -->

  - [大巴](../Page/大巴.md "wikilink")

<!-- end list -->

  - 50、73、109、121、371、375、379路[中海华庭站](../Page/中海华庭.md "wikilink")
  - 15、64、71、76、80、M223、235、374路益田中路站
  - E1、K359路、观光巴士④线[购物公园站](../Page/购物公园.md "wikilink")
  - 3、109、K318、373、398、B612、B686、B709路[购物公园地铁站站](../Page/购物公园.md "wikilink")

## 龙岗店

Coco
Park龙岗店位于[龙岗大道和](../Page/龙岗大道.md "wikilink")[黄阁南路交汇处](../Page/黄阁南路.md "wikilink")，临近深圳地铁[大运站和](../Page/大运站.md "wikilink")[爱联站](../Page/爱联站.md "wikilink")，于2012年9月1日开放，项目投资10亿人民币。内另有[山姆会员店](../Page/沃尔玛.md "wikilink")。

### 公共交通

  - [深圳地铁](../Page/深圳地铁.md "wikilink")

<!-- end list -->

  - [大运站](../Page/大运站.md "wikilink")、[爱联站](../Page/爱联站.md "wikilink")

<!-- end list -->

  - [巴士](../Page/巴士.md "wikilink")

<!-- end list -->

  - 深惠3A、深惠3B、M266、M268、309、M309、M310、M320、351、358、M359、366、380、651、812、833、905路龙岗Coco
    Park东站
  - M229路、923路龙格路口站
  - 深惠3A、深惠3B、M266、M268、M278、309、M309、M310、M319、M320、358、M359、366、380、651、K651、811、812、833、868路爱联地铁站站
  - 深惠3A路、深惠3B路、M229、M266、M268、M294、M303、309、M309、M310、M314、M316、M318、M320、M322、351、M357、358、M359、366、M367、380、651、B809、812、823、833、B852、868、923路大运地铁站站

## 銀湖谷店

[龍華新區](../Page/龍華新區.md "wikilink")[民治星河銀湖谷COCO](../Page/民治街道.md "wikilink")
Park已於2018年9月開業。商場鄰近興建中的[深圳地鐵](../Page/深圳地鐵.md "wikilink")[10號綫](../Page/深圳地鐵10號綫.md "wikilink")[雅寶站](../Page/雅寶站.md "wikilink")。

## 姊妹品牌

  - COCO
    City（在[龍華新區](../Page/龍華新區.md "wikilink")[民治街道開設的一間](../Page/民治街道.md "wikilink")[購物中心](../Page/商場.md "wikilink")）
  - 星河ICO（在[龍華新區](../Page/龍華新區.md "wikilink")[龍華街道及](../Page/龍華街道_\(深圳市\).md "wikilink")[龍崗區](../Page/龍崗區.md "wikilink")[龍城街道開設的](../Page/龍城街道.md "wikilink")[購物中心](../Page/商場.md "wikilink")）

[Category:深圳商场](../Category/深圳商场.md "wikilink")
[Category:福田區](../Category/福田區.md "wikilink")
[Category:龙岗区](../Category/龙岗区.md "wikilink")
[Category:广东建筑物](../Category/广东建筑物.md "wikilink")
[Category:深圳建筑物](../Category/深圳建筑物.md "wikilink")
[Category:深圳旅游景点](../Category/深圳旅游景点.md "wikilink")