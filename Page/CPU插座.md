[Socket_775.jpg](https://zh.wikipedia.org/wiki/File:Socket_775.jpg "fig:Socket_775.jpg")插座（圖中為[LGA775](../Page/LGA775.md "wikilink")）\]\]
[SocketA.jpg](https://zh.wikipedia.org/wiki/File:SocketA.jpg "fig:SocketA.jpg")插座（圖中為[Socket
462](../Page/Socket_462.md "wikilink")）\]\]

**CPU插座**，中央处理器插座（）是電腦裡[主機板上固定住](../Page/主機板.md "wikilink")[CPU並導通電氣訊號的一種](../Page/CPU.md "wikilink")[插座](../Page/插座.md "wikilink")。

## 簡介

在CPU插座尚未問世之前，CPU是直接焊在主機板上，因早期電腦不普及、CPU種類較單純，尚不致發生問題。後來因CPU種類開始變得多樣，且[DIY風氣興起](../Page/DIY.md "wikilink")，為便利拆裝更換，CPU插座應運而生。早期使用PGA（[Pin
Grid
Array](../Page/Pin_Grid_Array.md "wikilink")），即針腳全位於處理器上，安裝時要將處理器的針腳插到插座上，拔取時需使用工具方可拔起，但由於這種方式較易損壞CPU針腳且不好拆裝。後來發展出零插入施力（[ZIF](../Page/ZIF.md "wikilink"),
Zero Insertion
Force）的CPU插座設計以便安裝。由於CPU接腳數量越多，針腳必須越做越細且密度越高。因此再發展為[LGA](../Page/LGA.md "wikilink"),
（Land Grid
Array），即針腳改成彈性針腳位於CPU插座上，處理器上僅有接觸點。現在，[Intel的主要的個人電腦處理器是使用LGA](../Page/Intel.md "wikilink")，而[AMD仍沿用PGA](../Page/AMD.md "wikilink")，僅部份產品改用LGA。

## 處理器插座和插槽列表

<table>
<thead>
<tr class="header">
<th><p>插座名称</p></th>
<th><p>投入使用年份</p></th>
<th><p><a href="../Page/产品寿命结束.md" title="wikilink">退出使用年份</a></p></th>
<th><p>支持的<a href="../Page/CPU.md" title="wikilink">CPU</a></p></th>
<th><p>电脑型号</p></th>
<th><p>封装形式</p></th>
<th><p>针脚数</p></th>
<th><p>针脚大小</p></th>
<th><p>总线速度</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/雙列直插封裝.md" title="wikilink">DIP</a></p></td>
<td><p>1970s</p></td>
<td><p>仍然使用</p></td>
<td><p><a href="../Page/Intel.md" title="wikilink">Intel</a> <a href="../Page/8086.md" title="wikilink">8086</a><br />
<a href="../Page/Intel.md" title="wikilink">Intel</a> <a href="../Page/8088.md" title="wikilink">8088</a></p></td>
<td></td>
<td><p>DIP</p></td>
<td><p>40</p></td>
<td><p>2.54mm</p></td>
<td><p>5/10 MHz</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Plastic_leaded_chip_carrier.md" title="wikilink">PLCC</a></p></td>
<td><p>?</p></td>
<td><p>仍在使用</p></td>
<td><p><a href="../Page/Intel.md" title="wikilink">Intel</a> <a href="../Page/80186.md" title="wikilink">80186</a><br />
<a href="../Page/Intel.md" title="wikilink">Intel</a> <a href="../Page/80286.md" title="wikilink">80286</a><br />
<a href="../Page/Intel.md" title="wikilink">Intel</a> <a href="../Page/80386.md" title="wikilink">80386</a></p></td>
<td></td>
<td><p>PLCC</p></td>
<td><p>68, 132</p></td>
<td><p>1.27mm</p></td>
<td><p>6–40 MHz</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Socket_1.md" title="wikilink">Socket 1</a></p></td>
<td><p>1989</p></td>
<td><p>?</p></td>
<td><p><a href="../Page/Intel.md" title="wikilink">Intel</a> <a href="../Page/80486.md" title="wikilink">80486</a></p></td>
<td></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>169</p></td>
<td><p>2.54mm</p></td>
<td><p>16–50 MHz</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Socket_2.md" title="wikilink">Socket 2</a></p></td>
<td><p>?</p></td>
<td><p>?</p></td>
<td><p><a href="../Page/Intel.md" title="wikilink">Intel</a> <a href="../Page/80486.md" title="wikilink">80486</a></p></td>
<td></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>238</p></td>
<td><p>2.54mm</p></td>
<td><p>16–50 MHz</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Socket_3.md" title="wikilink">Socket 3</a></p></td>
<td><p>1991</p></td>
<td><p>?</p></td>
<td><p><a href="../Page/Intel.md" title="wikilink">Intel</a> <a href="../Page/80486.md" title="wikilink">80486</a></p></td>
<td></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>237</p></td>
<td><p>2.54mm</p></td>
<td><p>16–50 MHz</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Socket_4.md" title="wikilink">Socket 4</a></p></td>
<td><p>?</p></td>
<td><p>?</p></td>
<td><p><a href="../Page/Intel.md" title="wikilink">Intel</a> <a href="../Page/Intel_P5.md" title="wikilink">Pentium</a></p></td>
<td></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>273</p></td>
<td><p>?</p></td>
<td><p>60–66 MHz</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Socket_5.md" title="wikilink">Socket 5</a></p></td>
<td><p>?</p></td>
<td><p>?</p></td>
<td><p><a href="../Page/Intel.md" title="wikilink">Intel</a> <a href="../Page/Intel_P5.md" title="wikilink">Pentium</a><br />
<a href="../Page/超微半导体.md" title="wikilink">AMD</a> <a href="../Page/AMD_K5.md" title="wikilink">K5</a><br />
<a href="../Page/Integrated_Device_Technology.md" title="wikilink">IDT</a> <a href="../Page/WinChip.md" title="wikilink">WinChip</a> C6<br />
IDT WinChip 2</p></td>
<td></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>320</p></td>
<td><p>?</p></td>
<td><p>50–66 MHz</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Socket_6.md" title="wikilink">Socket 6</a></p></td>
<td><p>?</p></td>
<td><p>?</p></td>
<td><p><a href="../Page/Intel.md" title="wikilink">Intel</a> <a href="../Page/80486.md" title="wikilink">80486</a></p></td>
<td></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>235</p></td>
<td><p>?</p></td>
<td><p>?</p></td>
<td><p>未被投入使用</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Socket_7.md" title="wikilink">Socket 7</a></p></td>
<td><p>1994</p></td>
<td><p>?</p></td>
<td><p><a href="../Page/Intel.md" title="wikilink">Intel</a> <a href="../Page/Intel_P5.md" title="wikilink">Pentium</a><br />
Intel <a href="../Page/Pentium_MMX.md" title="wikilink">Pentium MMX</a><br />
<a href="../Page/超微半导体.md" title="wikilink">AMD</a> <a href="../Page/AMD_K6.md" title="wikilink">K6</a></p></td>
<td></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>321</p></td>
<td><p>?</p></td>
<td><p>50–66 MHz</p></td>
<td><p>It is possible to use Socket 7 processors in a Socket 5. An adapter is required, or if one is careful, a socket 7 can be pulled off its pins and put onto a socket 5 board, allowing the use of socket 7 processors.</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Super_Socket_7.md" title="wikilink">Super Socket 7</a></p></td>
<td><p>1998</p></td>
<td><p>?</p></td>
<td><p><a href="../Page/AMD_K6-2.md" title="wikilink">AMD K6-2</a><br />
<a href="../Page/AMD_K6-III.md" title="wikilink">AMD K6-III</a><br />
<a href="../Page/Rise_Technology.md" title="wikilink">Rise</a> <a href="../Page/mP6.md" title="wikilink">mP6</a><br />
Cyrix <a href="../Page/Cyrix_6x86.md" title="wikilink">MII</a></p></td>
<td></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>321</p></td>
<td><p>?</p></td>
<td><p>66–100 MHz</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Socket_8.md" title="wikilink">Socket 8</a></p></td>
<td><p>1995</p></td>
<td><p>?</p></td>
<td><p>Intel <a href="../Page/Pentium_Pro.md" title="wikilink">Pentium Pro</a></p></td>
<td></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>387</p></td>
<td><p>?</p></td>
<td><p>60–66 MHz</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Slot_1.md" title="wikilink">Slot 1</a></p></td>
<td><p>1997</p></td>
<td><p>?</p></td>
<td><p>Intel <a href="../Page/Pentium_II.md" title="wikilink">Pentium II</a><br />
Intel <a href="../Page/Pentium_III.md" title="wikilink">Pentium III</a></p></td>
<td></td>
<td><p>Slot</p></td>
<td><p>242</p></td>
<td><p>?</p></td>
<td><p>66–133 MHz</p></td>
<td><p>Celeron (Covington, Mendocino)<br />
Pentium II (Klamath, Deschutes)<br />
Pentium III (Katmai)- all versions<br />
Pentium III (coppermine)</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Slot_2.md" title="wikilink">Slot 2</a></p></td>
<td><p>1998</p></td>
<td><p>?</p></td>
<td><p>Intel <a href="../Page/Pentium_II_Xeon.md" title="wikilink">Pentium II Xeon</a></p></td>
<td></td>
<td><p>Slot</p></td>
<td><p>330</p></td>
<td><p>?</p></td>
<td><p>100–133 MHz</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Socket_463.md" title="wikilink">Socket 463</a>/<br />
<a href="../Page/Socket_NexGen.md" title="wikilink">Socket NexGen</a></p></td>
<td><p>?</p></td>
<td><p>?</p></td>
<td><p><a href="../Page/NexGen.md" title="wikilink">NexGen</a> <a href="../Page/Nx586.md" title="wikilink">Nx586</a></p></td>
<td></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>463</p></td>
<td><p>?</p></td>
<td><p>37.5–66MHz</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Socket_587.md" title="wikilink">Socket 587</a></p></td>
<td><p>?</p></td>
<td><p>?</p></td>
<td><p><a href="../Page/Alpha_21164A.md" title="wikilink">Alpha 21164A</a></p></td>
<td></td>
<td><p>Slot</p></td>
<td><p>587</p></td>
<td><p>?</p></td>
<td><p>?</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Slot_A.md" title="wikilink">Slot A</a></p></td>
<td><p>1999</p></td>
<td><p>?</p></td>
<td><p>AMD <a href="../Page/Athlon.md" title="wikilink">Athlon</a></p></td>
<td></td>
<td><p>Slot</p></td>
<td><p>242</p></td>
<td><p>?</p></td>
<td><p>100 MHz</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Slot_B.md" title="wikilink">Slot B</a></p></td>
<td><p>?</p></td>
<td><p>?</p></td>
<td><p><a href="../Page/Alpha_21264.md" title="wikilink">Alpha 21264</a></p></td>
<td></td>
<td><p>Slot</p></td>
<td><p>587</p></td>
<td><p>?</p></td>
<td><p>?</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Socket_370.md" title="wikilink">Socket 370</a></p></td>
<td><p>1999</p></td>
<td><p>?</p></td>
<td><p>Intel <a href="../Page/Pentium_III.md" title="wikilink">Pentium III</a><br />
Intel <a href="../Page/Celeron.md" title="wikilink">Celeron</a><br />
<a href="../Page/VIA_Technologies.md" title="wikilink">VIA</a> <a href="../Page/Cyrix_III.md" title="wikilink">Cyrix III</a><br />
<a href="../Page/VIA_Technologies.md" title="wikilink">VIA</a> <a href="../Page/VIA_C3.md" title="wikilink">C3</a></p></td>
<td></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>370</p></td>
<td><p>1.27mm[1]</p></td>
<td><p>66–133 MHz</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Socket_462.md" title="wikilink">Socket 462</a>/<br />
<a href="../Page/Socket_A.md" title="wikilink">Socket A</a></p></td>
<td><p>2000</p></td>
<td><p>?</p></td>
<td><p>AMD <a href="../Page/Athlon.md" title="wikilink">Athlon</a><br />
AMD <a href="../Page/Duron.md" title="wikilink">Duron</a><br />
AMD <a href="../Page/Athlon_XP.md" title="wikilink">Athlon XP</a><br />
AMD <a href="../Page/Athlon_XP-M.md" title="wikilink">Athlon XP-M</a><br />
AMD <a href="../Page/Athlon_MP.md" title="wikilink">Athlon MP</a><br />
AMD <a href="../Page/Sempron.md" title="wikilink">Sempron</a></p></td>
<td><p>Desktop</p></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>462</p></td>
<td><p>?</p></td>
<td><p>100–200 MHz This is a double data rate bus having a 400 MT/s (megatransfers/second) FSB in the later models</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Socket_423.md" title="wikilink">Socket 423</a></p></td>
<td><p>2000</p></td>
<td><p>?</p></td>
<td><p>Intel <a href="../Page/Pentium_4.md" title="wikilink">Pentium 4</a></p></td>
<td></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>423</p></td>
<td><p>1mm[2]</p></td>
<td><p>400 MT/s (100 MHz)</p></td>
<td><p>Willamette core only</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Socket_478.md" title="wikilink">Socket 478</a>/<br />
<a href="../Page/Socket_N.md" title="wikilink">Socket N</a></p></td>
<td><p>2000</p></td>
<td><p>~2007</p></td>
<td><p>Intel <a href="../Page/Pentium_4.md" title="wikilink">Pentium 4</a><br />
Intel <a href="../Page/Celeron.md" title="wikilink">Celeron</a><br />
Intel <a href="../Page/P4EE#Extreme_Edition.md" title="wikilink">Pentium 4 EE</a><br />
Intel <a href="../Page/List_of_Intel_Pentium_4_microprocessors#Mobile_processors.md" title="wikilink">Pentium 4 M</a></p></td>
<td><p>Desktop</p></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>478</p></td>
<td><p>1.27mm[3]</p></td>
<td><p>400–800 MT/s (100–200 MHz)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Socket_495.md" title="wikilink">Socket 495</a></p></td>
<td><p>2000</p></td>
<td><p>?</p></td>
<td><p>Intel <a href="../Page/Celeron.md" title="wikilink">Celeron</a><br />
Intel <a href="../Page/Pentium_III.md" title="wikilink">Pentium III</a></p></td>
<td></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>495</p></td>
<td><p>?</p></td>
<td><p>?</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/LGA_775.md" title="wikilink">LGA 775</a>/<a href="../Page/Socket_T.md" title="wikilink">Socket T</a></p></td>
<td><p>2004</p></td>
<td><p>?</p></td>
<td><p><a href="../Page/Intel_Pentium_4.md" title="wikilink">Intel Pentium 4</a><br />
<a href="../Page/Intel_Celeron_D(Prescott核心).md" title="wikilink">Intel Celeron D(Prescott核心)</a><br />
<a href="../Page/Intel_Pentium_D.md" title="wikilink">Intel Pentium D</a><br />
<a href="../Page/Intel_Core_2.md" title="wikilink">Intel Core 2</a><br />
<a href="../Page/Intel_Xeon_3000_Sequence.md" title="wikilink">Intel Xeon 3000 Sequence</a></p></td>
<td><p>Desktop</p></td>
<td><p><a href="../Page/LGA.md" title="wikilink">LGA</a></p></td>
<td><p>775</p></td>
<td><p>?</p></td>
<td><p>133MHz<a href="../Page/QDR.md" title="wikilink">QDR</a>,<a href="../Page/FSB.md" title="wikilink">FSB</a>533 - 400MHz<a href="../Page/QDR.md" title="wikilink">QDR</a>,<a href="../Page/FSB.md" title="wikilink">FSB</a>1600</p></td>
<td><p>替代<a href="../Page/Socket_478.md" title="wikilink">Socket 478</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Socket_S1.md" title="wikilink">Socket S1</a></p></td>
<td><p>2006</p></td>
<td><p>?</p></td>
<td><p>AMD <a href="../Page/Turion_64_X2.md" title="wikilink">Turion 64 X2</a></p></td>
<td></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>638</p></td>
<td><p>1.27mm[4]</p></td>
<td><p>200–800 MHz</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Socket_AM2.md" title="wikilink">Socket AM2</a></p></td>
<td><p>2006</p></td>
<td><p>?</p></td>
<td><p>AMD <a href="../Page/Athlon_64.md" title="wikilink">Athlon 64</a><br />
AMD <a href="../Page/Athlon_64_X2.md" title="wikilink">Athlon 64 X2</a></p></td>
<td><p>Desktop</p></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>940</p></td>
<td><p>1.27mm[5]</p></td>
<td><p>200–1000 MHz</p></td>
<td><p>替代 <a href="../Page/Socket_754.md" title="wikilink">Socket 754</a> 和 <a href="../Page/Socket_939.md" title="wikilink">Socket 939</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Socket_F.md" title="wikilink">Socket F</a>/Socket L (Socket 1207FX)</p></td>
<td><p>2006</p></td>
<td><p>?</p></td>
<td><p>AMD <a href="../Page/Athlon_64_FX.md" title="wikilink">Athlon 64 FX</a><br />
AMD <a href="../Page/Opteron.md" title="wikilink">Opteron</a>(Socket L 仅支持 Athlon 64 FX)</p></td>
<td><p>Server<br />
Desktop</p></td>
<td><p><a href="../Page/Land_grid_array.md" title="wikilink">LGA</a></p></td>
<td><p>1207</p></td>
<td><p>1.1mm[6]</p></td>
<td><p>? Socket L: 1000 MHz in Single CPU mode, 2000 MHz in Dual CPU mode</p></td>
<td><p>替代 <a href="../Page/Socket_940.md" title="wikilink">Socket 940</a><br />
Socket L 提供给在台式电脑上使用服务器电源的发烧友们。它仅仅是 Socket F 的一个新品牌，不需要特别的内存，并且只能适用于华硕 L1N64-SLI WS 主板。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Socket_AM2+.md" title="wikilink">Socket AM2+</a></p></td>
<td><p>2007</p></td>
<td><p>?</p></td>
<td><p>AMD <a href="../Page/Athlon_64.md" title="wikilink">Athlon 64</a><br />
AMD <a href="../Page/Athlon_X2.md" title="wikilink">Athlon X2</a><br />
AMD <a href="../Page/Phenom.md" title="wikilink">Phenom</a><br />
AMD <a href="../Page/Phenom_II.md" title="wikilink">Phenom II</a></p></td>
<td><p>Desktop</p></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>940</p></td>
<td><p>1.27mm[7]</p></td>
<td><p>200–2600 MHz</p></td>
<td><p>Separated power planes<br />
替代 <a href="../Page/Socket_AM2.md" title="wikilink">Socket AM2</a><br />
与 Socket AM2 相互兼容</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Socket_P.md" title="wikilink">Socket P</a></p></td>
<td><p>2007</p></td>
<td><p>?</p></td>
<td><p>Intel <a href="../Page/Core_2.md" title="wikilink">Core 2</a></p></td>
<td><p>Notebook</p></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>478</p></td>
<td></td>
<td><p>533–1066 MT/s</p></td>
<td><p>替代 <a href="../Page/Socket_M.md" title="wikilink">Socket M</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Socket_441.md" title="wikilink">Socket 441</a></p></td>
<td><p>2008</p></td>
<td><p>?</p></td>
<td><p><a href="../Page/Intel_Atom.md" title="wikilink">Intel Atom</a></p></td>
<td><p>Sub-notebook</p></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>441</p></td>
<td><p>?</p></td>
<td><p>400–667 MHz</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/LGA_1366.md" title="wikilink">LGA 1366</a>/<br />
Socket B</p></td>
<td><p>2008</p></td>
<td><p>?</p></td>
<td><p>Intel <a href="../Page/Core_i7.md" title="wikilink">Core i7</a> (900 系列)<br />
Intel <a href="../Page/Xeon.md" title="wikilink">Xeon</a> (35xx, 36xx, 55xx, 56xx 系列)</p></td>
<td><p>Server</p></td>
<td><p><a href="../Page/Land_grid_array.md" title="wikilink">LGA</a></p></td>
<td><p>1366</p></td>
<td></td>
<td><p>4.8–6.4 GT/s</p></td>
<td><p>替代入门级的 <a href="../Page/Socket_J.md" title="wikilink">Socket J</a> (LGA 771)</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Socket_G1.md" title="wikilink">Socket G1</a>/<br />
rPGA 988A</p></td>
<td><p>2008</p></td>
<td><p>?</p></td>
<td><p>Intel <a href="../Page/Core_i7.md" title="wikilink">Core i7</a> (600, 700, 800, 900 系列)<br />
Intel <a href="../Page/Core_i5.md" title="wikilink">Core i5</a> (400, 500 系列)<br />
Intel <a href="../Page/Core_i3.md" title="wikilink">Core i3</a> (300 系列)<br />
Intel <a href="../Page/Intel_P5.md" title="wikilink">Pentium</a> (P6000 系列)<br />
Intel <a href="../Page/Celeron.md" title="wikilink">Celeron</a> (P4000 系列)</p></td>
<td><p>Notebook</p></td>
<td><p><a href="../Page/Socket_G1.md" title="wikilink">rPGA</a></p></td>
<td><p>988</p></td>
<td><p>1mm</p></td>
<td><p>2.5GT/s, 4.8GT/s</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/LGA_1156.md" title="wikilink">LGA 1156</a>/<br />
Socket H/<br />
Socket H1</p></td>
<td><p>2010</p></td>
<td><p>?</p></td>
<td><p>Intel <a href="../Page/Core_i7.md" title="wikilink">Core i7</a> (800 系列)<br />
Intel <a href="../Page/Core_i5.md" title="wikilink">Core i5</a> (700, 600 系列)<br />
Intel <a href="../Page/Core_i3.md" title="wikilink">Core i3</a> (500, 400 系列)<br />
Intel <a href="../Page/Intel_P5.md" title="wikilink">Pentium</a> (G6000 系列)</p></td>
<td><p>Desktop</p></td>
<td><p><a href="../Page/Land_grid_array.md" title="wikilink">LGA</a></p></td>
<td><p>1156</p></td>
<td><p>?</p></td>
<td><p>?</p></td>
<td><p>替代 <a href="../Page/LGA_775.md" title="wikilink">LGA 775</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Socket_AM3.md" title="wikilink">Socket AM3</a></p></td>
<td><p>2009</p></td>
<td><p>?</p></td>
<td><p>AMD <a href="../Page/Phenom_II.md" title="wikilink">Phenom II</a><br />
AMD <a href="../Page/Athlon_II.md" title="wikilink">Athlon II</a><br />
AMD <a href="../Page/Sempron.md" title="wikilink">Sempron</a></p></td>
<td><p>Desktop</p></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>941</p></td>
<td><p>?</p></td>
<td><p>200–3200 MHz</p></td>
<td><p>替代 <a href="../Page/Socket_F.md" title="wikilink">Socket F</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Socket_C32.md" title="wikilink">Socket C32</a></p></td>
<td><p>2010</p></td>
<td><p>?</p></td>
<td><p>AMD <a href="../Page/Opteron.md" title="wikilink">Opteron</a> (4000 系列)</p></td>
<td><p>Server</p></td>
<td><p><a href="../Page/Land_grid_array.md" title="wikilink">LGA</a></p></td>
<td><p>1207</p></td>
<td><p>?</p></td>
<td><p>200–3200 MHz</p></td>
<td><p>替代 <a href="../Page/Socket_F.md" title="wikilink">Socket F</a>, <a href="../Page/Socket_AM3.md" title="wikilink">Socket AM3</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/LGA_1248.md" title="wikilink">LGA 1248</a></p></td>
<td><p>2010</p></td>
<td><p>?</p></td>
<td><p><a href="../Page/Itanium#Itanium_9300_(Tukwila)_:_2010.md" title="wikilink">Intel Itanium 9300 系列</a></p></td>
<td><p>Server</p></td>
<td><p><a href="../Page/Land_grid_array.md" title="wikilink">LGA</a></p></td>
<td><p>1248</p></td>
<td><p>?</p></td>
<td><p>4.8 GT/s</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/LGA_1567.md" title="wikilink">LGA 1567</a></p></td>
<td><p>2010</p></td>
<td><p>?</p></td>
<td><p><a href="../Page/Beckton_(microprocessor)#Beckton.md" title="wikilink">Intel Xeon 6500/7500 系列</a></p></td>
<td><p>Server</p></td>
<td><p><a href="../Page/Land_grid_array.md" title="wikilink">LGA</a></p></td>
<td><p>1567</p></td>
<td><p>?</p></td>
<td><p>4.8–6.4 GT/s</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/LGA_1155.md" title="wikilink">LGA 1155</a>/<br />
Socket H2</p></td>
<td><p>2011/Q1<br />
2011.01.09</p></td>
<td><p>?</p></td>
<td><p>Intel <a href="../Page/Sandy_Bridge.md" title="wikilink">Sandy Bridge</a><br />
Intel <a href="../Page/Ivy_Bridge.md" title="wikilink">Ivy Bridge</a></p></td>
<td><p>Desktop</p></td>
<td><p><a href="../Page/Land_grid_array.md" title="wikilink">LGA</a></p></td>
<td><p>1155</p></td>
<td><p>?</p></td>
<td><p>5.7, GT/s</p></td>
<td><p>Sandy Bridge 支持20条 <a href="../Page/PCI_Express.md" title="wikilink">PCIe</a> 2.0 总线。<br />
Ivy Bridge 支持40条 <a href="../Page/PCI_Express.md" title="wikilink">PCIe</a> 3.0 总线。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/LGA_2011.md" title="wikilink">LGA 2011-0</a>/<br />
Socket R</p></td>
<td><p>2011/Q3<br />
(2011.11.14)</p></td>
<td><p>?</p></td>
<td><p>Intel <a href="../Page/Sandy_Bridge.md" title="wikilink">Sandy Bridge-E</a><br />
Intel <a href="../Page/Ivy_Bridge.md" title="wikilink">Ivy Bridge-E</a><br />
Intel <a href="../Page/Xeon_E5.md" title="wikilink">Xeon E5</a> 2xxx/4xxx Intel <a href="../Page/Sandy_Bridge.md" title="wikilink">Sandy Bridge EP</a> (2/4S)<br />
Intel <a href="../Page/Xeon_E5.md" title="wikilink">Xeon E5</a>-2xxx/4xxx v2 Intel <a href="../Page/Ivy_Bridge.md" title="wikilink">Ivy Bridge EP</a> (2/4S)</p></td>
<td><p>Desktop<br />
Server</p></td>
<td><p><a href="../Page/Land_grid_array.md" title="wikilink">LGA</a></p></td>
<td><p>2011</p></td>
<td><p>?</p></td>
<td><p>4.8–6.4 GT/s</p></td>
<td><p>Sandy Bridge-E/EP 和 Ivy Bridge-E/EP 均支持40条 <a href="../Page/PCI_Express.md" title="wikilink">PCIe</a> 3.0 总线。<br />
使用 Socket 2011 的志强 CPU 也有四通道内存。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/rPGA_988B.md" title="wikilink">rPGA 988B</a>/<br />
<a href="../Page/Socket_G2.md" title="wikilink">Socket G2</a></p></td>
<td><p>2011</p></td>
<td><p>?</p></td>
<td><p>Intel <a href="../Page/Sandy_Bridge.md" title="wikilink">Sandy Bridge</a><br />
Intel <a href="../Page/Ivy_Bridge.md" title="wikilink">Ivy Bridge</a></p></td>
<td><p>Notebook</p></td>
<td><p><a href="../Page/Socket_G2.md" title="wikilink">rPGA</a></p></td>
<td><p>988</p></td>
<td><p>1mm</p></td>
<td><p>2.5GT/s, 4.8GT/s</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Socket_FM1.md" title="wikilink">Socket FM1</a></p></td>
<td><p>2011</p></td>
<td><p>?</p></td>
<td><p>AMD <a href="../Page/AMD_Fusion.md" title="wikilink">Llano Processors</a></p></td>
<td><p>Desktop</p></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>905</p></td>
<td><p>1.27mm</p></td>
<td></td>
<td><p>使用于第一代 APU</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Socket_AM3+.md" title="wikilink">Socket AM3+</a></p></td>
<td><p>2011</p></td>
<td><p>?</p></td>
<td><p><a href="../Page/Bulldozer_(processor)#2nd_Generation_Piledriver_core.md" title="wikilink">AMD FX Vishera</a><br />
<a href="../Page/Bulldozer_(microarchitecture).md" title="wikilink">AMD FX Zambezi</a><br />
AMD <a href="../Page/Phenom_II.md" title="wikilink">Phenom II</a><br />
AMD <a href="../Page/Athlon_II.md" title="wikilink">Athlon II</a><br />
AMD <a href="../Page/Sempron.md" title="wikilink">Sempron</a></p></td>
<td><p>Desktop</p></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>942 (CPU 71pin)</p></td>
<td><p>1.27mm</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Socket_FM2.md" title="wikilink">Socket FM2</a></p></td>
<td><p>2012</p></td>
<td><p>?</p></td>
<td><p>AMD <a href="../Page/AMD_Fusion#Trinity.md" title="wikilink">Trinity Processors</a><br />
AMD <a href="../Page/AMD_Fusion#Richland.md" title="wikilink">Richland Processors</a></p></td>
<td><p>Desktop</p></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>904</p></td>
<td><p>1.27mm</p></td>
<td></td>
<td><p>使用于第二代，第三代 APU</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/LGA_1150.md" title="wikilink">LGA 1150</a>/<br />
Socket H3</p></td>
<td><p>2013/Q2<br />
(2013.06.02)</p></td>
<td><p>?</p></td>
<td><p>Intel <a href="../Page/Haswell.md" title="wikilink">Haswell</a><br />
Intel <a href="../Page/Broadwell.md" title="wikilink">Broadwell</a></p></td>
<td><p>Desktop</p></td>
<td><p><a href="../Page/Land_grid_array.md" title="wikilink">LGA</a></p></td>
<td><p>1150</p></td>
<td><p>?</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Socket_G3.md" title="wikilink">Socket G3</a></p></td>
<td><p>2013/Q2</p></td>
<td><p>?</p></td>
<td><p>Intel <a href="../Page/Haswell.md" title="wikilink">Haswell</a><br />
Intel <a href="../Page/Broadwell.md" title="wikilink">Broadwell</a></p></td>
<td><p>Notebook</p></td>
<td><p><a href="../Page/Land_grid_array.md" title="wikilink">rPGA</a></p></td>
<td><p>946</p></td>
<td><p>?</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Socket_FM2+.md" title="wikilink">Socket FM2+</a></p></td>
<td><p>2014</p></td>
<td><p>?</p></td>
<td><p>AMD <a href="../Page/AMD_Fusion#Kaveri.md" title="wikilink">Kaveri Processors</a></p></td>
<td><p>Desktop</p></td>
<td><p><a href="../Page/Pin_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>906</p></td>
<td><p>1.27mm</p></td>
<td></td>
<td><p>使用于第四代 APU</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/LGA_2011.md" title="wikilink">LGA 2011-1</a></p></td>
<td><p>2014</p></td>
<td><p>?</p></td>
<td><p>Intel <a href="../Page/Xeon_E7.md" title="wikilink">Xeon E7</a> v2</p></td>
<td><p>Server</p></td>
<td><p><a href="../Page/Land_grid_array.md" title="wikilink">LGA</a></p></td>
<td><p>2011</p></td>
<td><p>?</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/LGA_2011.md" title="wikilink">LGA 2011-3</a></p></td>
<td><p>2014</p></td>
<td><p>?</p></td>
<td><p>Intel <a href="../Page/Haswell.md" title="wikilink">Haswell-EP</a></p></td>
<td><p>Desktop</p></td>
<td><p><a href="../Page/Land_grid_array.md" title="wikilink">LGA</a></p></td>
<td><p>2011</p></td>
<td><p>1.016</p></td>
<td></td>
<td><p>与 <a href="../Page/LGA_2011.md" title="wikilink">LGA 2011-0</a> 不兼容</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Socket_AM1.md" title="wikilink">Socket AM1</a></p></td>
<td><p>2014</p></td>
<td><p>?</p></td>
<td><p>AMD<a href="../Page/AMD加速處理器列表#Kabini.md" title="wikilink">Jagaur Processors</a></p></td>
<td><p>Desktop</p></td>
<td><p><a href="../Page/Land_grid_array.md" title="wikilink">PGA</a></p></td>
<td><p>722</p></td>
<td></td>
<td></td>
<td><p>第一代 AMD 低功耗 APU</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/LGA_1151.md" title="wikilink">LGA 1151</a></p></td>
<td><p>2015</p></td>
<td><p>?</p></td>
<td><p>Intel <a href="../Page/Skylake微架構.md" title="wikilink">Skylake微架構及</a><a href="../Page/Kaby_Lake.md" title="wikilink">Kaby Lake微架構</a></p></td>
<td><p>Desktop</p></td>
<td><p><a href="../Page/Land_grid_array.md" title="wikilink">LGA</a></p></td>
<td><p>1151</p></td>
<td><p>?</p></td>
<td><p>?</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>LGA 3647</p></td>
<td><p>2016</p></td>
<td><p>?</p></td>
<td><p>Intel <a href="../Page/Knights_Landing.md" title="wikilink">Knights Landing及</a><a href="../Page/Skylake-EX/EP.md" title="wikilink">Skylake-EX/EP微架構</a></p></td>
<td><p>Server</p></td>
<td><p><a href="../Page/LGA.md" title="wikilink">LGA</a></p></td>
<td><p>3647</p></td>
<td><p>?</p></td>
<td><p>?</p></td>
<td><p>替代 LGA 2011-3</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Socket_AM4.md" title="wikilink">Socket AM4</a></p></td>
<td><p>2017</p></td>
<td><p>?</p></td>
<td><p><a href="../Page/AMD_Ryzen.md" title="wikilink">AMD Ryzen</a><br />
以及 <a href="../Page/AMD_Athlon.md" title="wikilink">Athlon</a></p></td>
<td><p>Desktop</p></td>
<td><p><a href="../Page/µOPGA.md" title="wikilink">µOPGA</a></p></td>
<td><p>1331</p></td>
<td><p>?</p></td>
<td><p>?</p></td>
<td><p>这是AMD 新一代CPU(Ryzen/APU)的插槽</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Socket_R4.md" title="wikilink">Socket R4</a></p></td>
<td><p>2017</p></td>
<td><p>?</p></td>
<td><p><a href="../Page/Intel_Skylake-X.md" title="wikilink">Intel Skylake-X</a><br />
<a href="../Page/Intel_Kaby_Lake-X.md" title="wikilink">Intel Kaby Lake-X</a></p></td>
<td><p>Desktop</p></td>
<td><p><a href="../Page/LGA.md" title="wikilink">LGA</a></p></td>
<td><p>2066</p></td>
<td><p>?</p></td>
<td><p>?</p></td>
<td><p>替代 LGA 2011</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Socket_TR4.md" title="wikilink">Socket TR4</a></p></td>
<td><p>2017</p></td>
<td><p>?</p></td>
<td><p>AMD <a href="../Page/AMD_Ryzen.md" title="wikilink">Ryzen Threadripper</a></p></td>
<td><p>Desktop</p></td>
<td><p><a href="../Page/LGA.md" title="wikilink">LGA</a></p></td>
<td><p>4094</p></td>
<td><p>?</p></td>
<td><p>?</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>插座名称</p></td>
<td><p>投入使用年份</p></td>
<td><p><a href="../Page/产品寿命结束.md" title="wikilink">退出使用年份</a></p></td>
<td><p>支持的<a href="../Page/CPU.md" title="wikilink">CPU</a></p></td>
<td><p>电脑型号</p></td>
<td><p>封装形式</p></td>
<td><p>针脚数</p></td>
<td><p>针脚大小</p></td>
<td><p>总线速度</p></td>
<td><p>备注</p></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部連結

  - [CPU Sockets
    Chart](https://web.archive.org/web/20090322202532/http://users.erols.com/chare/sockets.htm)
  - [Socket ID Guide](http://www.cpushack.net/SocketID.html)
  - [CPU Sockets Chart](http://pclinks.xtreemhost.com/) - 一個詳盡的x86
    CPU插座及相關參數的頁面
  - [techPowerUp\! CPU Database](http://www.techpowerup.com/cpudb/)
  - [Processor sockets](http://www.cpu-world.com/Sockets/index.html)
  - [超能课堂(7)：闲聊CPU针脚，一年一换都怪AMD不给力？](http://www.expreview.com/42725.html)
  - [一针定乾坤！AMD全系CPU针脚接口解析](http://www.pcpop.com/doc/0/643/643760_all.shtml)

## 参见

  - [Intel 微處理器列表](../Page/Intel_微處理器列表.md "wikilink")
  - [AMD處理器列表](../Page/AMD處理器列表.md "wikilink")

{{-}}

[CPU插座](../Category/CPU插座.md "wikilink")

1.

2.

3.

4.

5.

6.

7.