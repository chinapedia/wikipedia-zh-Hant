《**CSI犯罪現場**》（*CSI: Crime Scene
Investigation*）是一套基於電視劇《[CSI犯罪現場](../Page/CSI犯罪現場.md "wikilink")》的[電腦遊戲](../Page/電腦遊戲.md "wikilink")。該遊戲由[369
Interactive開發](../Page/369_Interactive.md "wikilink")，[Ubisoft發行](../Page/Ubisoft.md "wikilink")，[PC平台](../Page/PC.md "wikilink")，2003年發表，並由[Aspyr於](../Page/Aspyr.md "wikilink")[Mac上發行](../Page/Mac.md "wikilink")。

这款遊戲與《[CSI犯罪現場：黑暗動機](../Page/CSI犯罪現場：黑暗動機.md "wikilink")》、《[CSI犯罪現場：謀殺的三維](../Page/CSI犯罪現場：謀殺的三維.md "wikilink")》及《[CSI犯罪現場：邁阿密](../Page/CSI犯罪現場：邁阿密\(遊戲\).md "wikilink")》一樣，跟隨五個佈局截然不同的案件，然後在第五個案件中連接之前的四個案件。

## 案件

### 案件1 - *Inn & Out*

一名遭謀殺的女性受害者發現她被丟到草丝里。這個任務的第一部份由一個外部的聲音作敍述你該怎樣做。後期你得到自行探索證物的自由並在[吉爾·葛瑞森旁邊工作](../Page/吉爾·葛瑞森.md "wikilink")。

### 案件2 - *Light My Fire*

一名生意人家中的辦公室發生火災。玩家與[莎拉·賽德爾於本案中一起工作](../Page/莎拉·賽德爾.md "wikilink")。標題參考自[門戶樂隊](../Page/門戶樂隊.md "wikilink")（The
Doors）的[同名歌曲](../Page/Light_My_Fire.md "wikilink")。

### 案件3 - *Garvey's Beat*

你必需調查一個警察的死亡。玩家與[尼克·史多克斯於本案中一起工作](../Page/尼克·史多克斯.md "wikilink")。

### 案件4 - *More Fun Than a Barrel of Corpses*

一個奇怪的電話打到*CSI*的接線總機。緊接着的調查中玩家偶然發現一具在桶中的屍體。玩家與[華瑞克．布朗於本案中一起工作](../Page/華瑞克．布朗.md "wikilink")。

### 案件5 - *Leda's Swan Song*

葛瑞森失蹤，而玩家的工作是找到他。這案件提供了與[凱薩琳·韋羅斯合夥重新檢查案件](../Page/凱薩琳·韋羅斯.md "wikilink")1與4犯案現場的機會。

## 外部連結

  - [GameSpot上](../Page/GameSpot.md "wikilink")[CSI犯罪現場的評論](http://www.gamespot.com/pc/adventure/csicrimesceneinvestigation/index.html?q=csi)

[Category:2003年电子游戏](../Category/2003年电子游戏.md "wikilink")
[Category:冒險遊戲](../Category/冒險遊戲.md "wikilink")
[Category:第一人称冒险游戏](../Category/第一人称冒险游戏.md "wikilink")
[Category:电视改编电子游戏](../Category/电视改编电子游戏.md "wikilink")
[Category:育碧游戏](../Category/育碧游戏.md "wikilink")
[Category:Windows遊戲](../Category/Windows遊戲.md "wikilink")
[Category:MacOS遊戲](../Category/MacOS遊戲.md "wikilink")
[Category:Xbox游戏](../Category/Xbox游戏.md "wikilink")
[Category:CSI犯罪現場](../Category/CSI犯罪現場.md "wikilink")
[Category:加拿大開發電子遊戲](../Category/加拿大開發電子遊戲.md "wikilink")
[Category:美國開發電子遊戲](../Category/美國開發電子遊戲.md "wikilink")