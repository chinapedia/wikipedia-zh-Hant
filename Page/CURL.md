**cURL**是一个利用URL语法在[命令行下工作的文件传输工具](../Page/命令行界面.md "wikilink")，1997年首次发行。它支持文件上传和下载，所以是综合传输工具，但按传统，习惯称cURL为下载工具。cURL还包含了用于程序开发的libcurl。

cURL支持的通訊協定有[FTP](../Page/FTP.md "wikilink")、[FTPS](../Page/FTPS.md "wikilink")、[HTTP](../Page/HTTP.md "wikilink")、[HTTPS](../Page/HTTPS.md "wikilink")、[TFTP](../Page/TFTP.md "wikilink")、[SFTP](../Page/SFTP.md "wikilink")、[Gopher](../Page/Gopher_\(网络协议\).md "wikilink")、[SCP](../Page/安全复制.md "wikilink")、[Telnet](../Page/Telnet.md "wikilink")、[DICT](../Page/DICT.md "wikilink")、[FILE](../Page/FILE.md "wikilink")、[LDAP](../Page/LDAP.md "wikilink")、LDAPS、[IMAP](../Page/IMAP.md "wikilink")、[POP3](../Page/POP3.md "wikilink")、[SMTP和](../Page/SMTP.md "wikilink")[RTSP](../Page/RTSP.md "wikilink")。

curl还支持SSL认证、HTTP POST、HTTP PUT、FTP上传, HTTP form based
upload、proxies、HTTP/2、cookies、用户名+密码认证(Basic, Plain, Digest,
CRAM-MD5, NTLM, Negotiate and Kerberos)、file transfer resume、proxy
tunneling。

## 参考资料

## 外部链接

  -
[Category:FTP客户端](../Category/FTP客户端.md "wikilink")
[Category:网络软件](../Category/网络软件.md "wikilink")
[Category:下載工具](../Category/下載工具.md "wikilink")
[Category:HTTP客户端](../Category/HTTP客户端.md "wikilink")