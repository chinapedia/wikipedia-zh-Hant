趋化因子**CXCL12**
又称**基质细胞衍生因子－1**(SDF-1)是小分子的[细胞因子](../Page/细胞因子.md "wikilink")，属于[趋化因子蛋白家族](../Page/趋化因子.md "wikilink")。它有两种形式，SDF-1α/CXCL12a和SDF-1β/CXCL12b\[1\]。趋化因子有四个保守的半胱氨酸残基形成两对[双硫键以构成趋化因子的特殊结构](../Page/双硫键.md "wikilink")。第一第二半胱氨酸残基之间隔着一个介入氨基酸残基。

趋化因子CXCL12对[淋巴细胞有强烈的趋化作用并在发育中起重要作用](../Page/淋巴细胞.md "wikilink")\[2\]。在胚胎發育中CXCL12引导[造血干细胞从胎儿](../Page/造血干细胞.md "wikilink")[肝脏到](../Page/肝脏.md "wikilink")[骨髓的迁徙](../Page/骨髓.md "wikilink")。CXCL12基因敲除的小鼠常常死于胎中或出生后1小时内\[3\]\[4\]。SDF-1α/CXCL12a还可以影响[神经元的电生理](../Page/神经元.md "wikilink")。CXCL12可以在许多组织（包括[脑](../Page/脑.md "wikilink")，[胸腺](../Page/胸腺.md "wikilink")，心，[肺](../Page/肺.md "wikilink")，[肝](../Page/肝.md "wikilink")，[肾](../Page/肾.md "wikilink")，骨髓，[脾脏](../Page/脾脏.md "wikilink")）中表达。

趋化因子CXCL12的受体是[CXCR4](../Page/CXCR4.md "wikilink")\[5\]。但是，最近有人认为CXCL12还可以与[CXCR7受体结合](../Page/CXCR7.md "wikilink")\[6\]。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:细胞因子](../Category/细胞因子.md "wikilink")
[Category:免疫学](../Category/免疫学.md "wikilink")

1.  De La Luz Sierra et al. Differential processing of stromal-derived
    factor-1alpha and beta explains functional diversity. Blood
    103:2452-2459, 2004.
2.  Bleul et al. A highly efficacious lymphocyte chemoattractant,
    stromal cell-derived factor 1 (SDF-1). J. Exp. Med. 184: 1101-1109,
    1996.
3.  Ara et al. Impaired colonization of the gonads by primordial germ
    cells in mice lacking a chemokine, stromal cell-derived factor-1
    (SDF-1). Proc. Nat. Acad. Sci. 100: 5319-5323, 2003.
4.  Ma et al. Impaired B-lymphopoiesis, myelopoiesis, and derailed
    cerebellar neuron migration in CXCR4- and SDF-1-deficient mice.
    Proc. Nat. Acad. Sci. 95: 9448-9453, 1998.
5.  Bleul et al. The lymphocyte chemoattractant SDF-1 is a ligand for
    LESTR/fusin and blocks HIV-1 entry. Nature 382: 829-833, 1996.
6.  Balabanian et al. The chemokine SDF-1/CXCl12 binds to and signals
    through the orphan receptor RDC1 in T lymphocytes. J Biol Chem
    280:35760-35766, 2005.