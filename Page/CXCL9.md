**CXCL9**（）是一小分子的[细胞因子属于CXC](../Page/细胞因子.md "wikilink")[趋化因子家族](../Page/趋化因子.md "wikilink")，又被称作“干扰素伽玛诱导的单核细胞因子”（Monokine
induced by gamma interferon, MIG）\[1\]
。CXCL9是[T细胞趋化因子](../Page/T细胞.md "wikilink")\[2\]
。[干扰素伽玛诱导CXCL](../Page/干扰素伽玛.md "wikilink")9的表达。人类的CXCL9与[CXCL10](../Page/CXCL10.md "wikilink")，[CXCL11的基因相邻聚集在第四染色体上](../Page/CXCL11.md "wikilink")\[3\]
。CXCL9，CXCL10,
CXCL11结合趋化因子受体[CXCR3而起其细胞趋化作用](../Page/CXCR3.md "wikilink")\[4\]\[5\]
。

## 参见

  - [趋化因子](../Page/趋化因子.md "wikilink")
  - [CXCR3](../Page/CXCR3.md "wikilink")
  - [CXCL10](../Page/CXCL10.md "wikilink")
  - [CXCL11](../Page/CXCL11.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [Charo IF, Ransohoff RM. The many roles of chemokines and chemokine
    receptors in inflammation. N Engl J Med. 2006
    Feb 9;354(6):610-21.](http://content.nejm.org/cgi/content/extract/354/6/610)

[Category:细胞因子](../Category/细胞因子.md "wikilink")
[Category:免疫学](../Category/免疫学.md "wikilink")

1.  Liao F, Rabin RL, Yannelli JR, Koniaris LG, Vanguri P, Farber JM.
    Human Mig chemokine: biochemical and functional characterization. J
    Exp Med. 1995 Nov 1;182(5):1301-14.
2.  Liao F, Rabin RL, Yannelli JR, Koniaris LG, Vanguri P, Farber JM.
    Human Mig chemokine: biochemical and functional characterization. J
    Exp Med. 1995 Nov 1;182(5):1301-14.
3.  Lee HH, Farber JM. Localization of the gene for the human MIG
    cytokine on chromosome 4q21 adjacent to INP10 reveals a chemokine
    "mini-cluster". Cytogenet Cell Genet. 1996;74(4):255-8.
4.  Loetscher M, Gerber B, Loetscher P, Jones SA, Piali L, Clark-Lewis
    I, Baggiolini M, Moser B. Chemokine receptor specific for IP10 and
    mig: structure, function, and expression in activated T-lymphocytes.
    J Exp Med. 1996 Sep 1;184(3):963-9.
5.  Weng Y, Siciliano SJ, Waldburger KE, Sirotina-Meisher A, Staruch MJ,
    Daugherty BL, Gould SL, Springer MS, DeMartino JA. Binding and
    functional properties of recombinant and endogenous CXCR3 chemokine
    receptors. J Biol Chem. 1998 Jul 17;273(29):18288-91.