**C shell**（**csh**）是[Unix
shell的一种](../Page/Unix_shell.md "wikilink")，由[比尔·乔伊在](../Page/比尔·乔伊.md "wikilink")[BSD系统上开发](../Page/BSD.md "wikilink")。C
shell脱胎于[Unix第六版的](../Page/Unix.md "wikilink")`/bin/sh`，也是[Bourne
shell的前身](../Page/Bourne_shell.md "wikilink")。这种shell的语法类似于[C语言](../Page/C语言.md "wikilink")，与[Bourne
shell相比](../Page/Bourne_shell.md "wikilink")，C
shell有不少特别的功能，比如`aliases`（别名）、command
history（命令的历史）。目前C shell已不再被广泛使用，后继者包括[Tenex C
shell](../Page/Tcsh.md "wikilink")（tcsh）、[Korn
shell](../Page/Korn_shell.md "wikilink")（ksh）、[GNU](../Page/GNU.md "wikilink")
[Bourne-Again shell](../Page/Bourne-Again_shell.md "wikilink")（bash）。

C shell在交互模式中引入许多开创性功能的同时，C
shell的脚本执行能力受到了不少批评。无论如何，由于在所有的[单一UNIX规范相容系统中都放了标准的Bourne](../Page/单一UNIX规范.md "wikilink")
shell，所以大部分人都建议使用`sh`来进行脚本编写。

## 参考文献

## 外部链接

  - Tom
    Christiansen，[Csh编程的有害处](http://www.faqs.org/faqs/unix-faq/shell/csh-whynot/)
  - Bruce Barnett，[不使用C
    shell的十大理由](http://www.grymoire.com/Unix/CshTop10.txt)

{{-}}

[Category:Unix shells](../Category/Unix_shells.md "wikilink")
[Category:专用编程语言](../Category/专用编程语言.md "wikilink")
[Category:基于文本的编程语言](../Category/基于文本的编程语言.md "wikilink")
[Category:脚本语言](../Category/脚本语言.md "wikilink")
[Category:1978年软件](../Category/1978年软件.md "wikilink")