**CalorieMate**（カロリーメイト）是一個由[日本](../Page/日本.md "wikilink")[大塚製藥生產的](../Page/大塚製藥.md "wikilink")[能量補充食品品牌](../Page/膳食補充劑.md "wikilink")。CalorieMate產品包裝上只標注為"Balanced
Food"，它們的包裝非常簡潔，可說是毫不起眼。

所有CalorieMate產品皆有以下描述：

  -
    *CALORIE MATE*（產品名）*是每日活動所需能量的營養補充來源。CALORIE
    MATE*（產品名）*自然適合在早餐、工作、運動、學習或任何忙碌時需要容易得到能量與營養的人。*

描述後是成份與營養資訊。

CalorieMate擁有數種形式，分別為Block、Jelly與Can。*CalorieMate
Block*（カロリーメイト　ブロック）是一種以二或四條包裝銷售的條狀[曲奇](../Page/曲奇.md "wikilink")；*CalorieMate
Jelly*（カロリーメイト　ゼリー）是一種包裝在擁有吸管袋裝裏的[明膠](../Page/明膠.md "wikilink")；*CalorieMate
Can*（カロリーメイト　缶）是一種罐裝飲品。

## 口味

### CalorieMate Block

  - [乳酪](../Page/乳酪.md "wikilink")
  - [巧克力](../Page/巧克力.md "wikilink")
  - [水果](../Page/水果.md "wikilink")
  - [蔬菜](../Page/蔬菜.md "wikilink")
  - [楓糖](../Page/楓糖.md "wikilink")

### CalorieMate Jelly

  - [蘋果](../Page/蘋果.md "wikilink")

### CalorieMate Can

  - [玉米](../Page/玉米.md "wikilink")[湯](../Page/湯.md "wikilink")
  - [咖啡牛奶](../Page/咖啡牛奶.md "wikilink")
  - [咖啡](../Page/咖啡.md "wikilink")
  - [可可豆](../Page/可可豆.md "wikilink")

## 在流行文化中出現

  - 在電子遊戲*[潛龍諜影3:食蛇者](../Page/潛龍諜影3:食蛇者.md "wikilink")*中有一個圖像為CalorieMate
    Block（巧克力味）名為CalorieMate的道具，使用後會完全回復主角Snake(蛇)的精力。

<!-- end list -->

  - CalorieMate通過與飾演[Fox電視台受歡迎電視劇](../Page/Fox.md "wikilink")*[24](../Page/24_\(電視劇\).md "wikilink")*主角杰克鮑爾的[基夫修打蘭合作拍攝多個廣告來宣傳他們的產品](../Page/基夫修打蘭.md "wikilink")。

<!-- end list -->

  - 電子遊戲[夢幻之星系列首作日文版中有一個明顯參考CalorieMate名為](../Page/夢幻之星.md "wikilink")*PelorieMate*（ペロリメイト）的道具，英文版中該道具被命名為*Cola*。

<!-- end list -->

  - 動漫畫[驚爆危機的惡搞版いきなり](../Page/驚爆危機.md "wikilink")\!フルメタル・パニック\!中的男主角相良仲介表明他吃一種名為CalorieMark的能量補充食品，明顯是模仿CalorieMate；他亦在驚爆危機校園篇其中一集中吃看起來像CalorieMate
    Block的棒狀物，儘管該項物品在動畫中沒有名字。在多本原著小說中他被描寫成在吃CalorieMates，當中亦有一些明確表示的插圖。

<!-- end list -->

  - 輕小說和動畫[NO GAME NO LIFE
    遊戲人生中男主角](../Page/NO_GAME_NO_LIFE_遊戲人生.md "wikilink")
    空 通宵玩遊戲所吃的補給品

## 參考

  - [CalorieMate官方網站](http://www.otsuka.co.jp/cmt)
  - [包含CalorieMate的大塚製藥購物網站](http://www.otsuka-plus1.com)（日語）
  - [與電視劇*[24](../Page/24_\(電視劇\).md "wikilink")*合作宣傳CalorieMate的網站](http://www.cmt24.net)

[Category:食品](../Category/食品.md "wikilink")
[Category:大塚製藥](../Category/大塚製藥.md "wikilink")