**Cambria**\[1\]是搭載於[微軟](../Page/微軟.md "wikilink")[Windows
Vista](../Page/Windows_Vista.md "wikilink")、[Microsoft Office
2007和](../Page/Microsoft_Office_2007.md "wikilink")[Microsoft Office
2008 for
Mac等軟體的一個](../Page/Microsoft_Office_2008_for_Mac.md "wikilink")[襯線](../Page/襯線體.md "wikilink")[字型](../Page/计算机字体.md "wikilink")，其字元間距和比例相當的平均，對角和垂直方向的筆畫和[襯線比較粗](../Page/襯線.md "wikilink")，而水平向的襯線比較細，強調筆畫的末筆。

當Cambria使用在大小超過20[點的標題時](../Page/點_\(印刷\).md "wikilink")，其字體間的間距會些微的縮小以求美觀，但這樣的設計並不適用於商業文件。

## Cambria Math

微软另外還發布了衍生字型**Cambria Math**，由Jelle Bosma和Ross
Mills開發，其含有更多專門的[數學符號字元](../Page/數學符號.md "wikilink")，以供科學出版之用。Cambria和Cambria
Math都是並存在一個[TrueType檔案裡](../Page/TrueType.md "wikilink")（TTC），安裝後，兩種字體會分別顯示在字型選項裡。

## 注釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [Microsoft Cleartype Font
    Collection](http://www.microsoft.com/typography/ClearTypeFonts.mspx)
    at Microsoft Typography

[Category:衬线字体](../Category/衬线字体.md "wikilink")
[Category:微软字体](../Category/微软字体.md "wikilink")
[Category:Windows Vista字體](../Category/Windows_Vista字體.md "wikilink")

1.  同樣以Cambria為名的還有另一套字型，那是1989年由Cambria Publishing公司的字型設計師Ian
    Koshnick所開發的，但和微軟的此套字型是完全無關的