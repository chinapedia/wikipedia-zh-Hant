**Canon**（音標：英－\['kænən\]；美－\['kænən\]）是[英文名词](../Page/英文.md "wikilink")，意思是「典型」或「真传经典」，也可表示：
1.正式〔倫理或思想行為上的〕規範、標準、準則，如行為(高尚趣味)的準則。
2.原著、真作、真本(書目)、正經、正史、正典；亦指其他聖典或著作家之作品的真本。

## [基督教](../Page/基督教.md "wikilink")

  - [正典](../Page/正典.md "wikilink")（Biblical
    canon），[聖經中教會所認可的真經部分](../Page/聖經.md "wikilink")。
  - [教会法](../Page/教会法.md "wikilink")（Canon
    law），即教規/[教會法規](../Page/教會.md "wikilink")。
  - [法政牧师](../Page/法政牧师.md "wikilink")（canon），在[大教堂中擔任職務的](../Page/大教堂.md "wikilink")[教士](../Page/教士.md "wikilink")/大教堂教士會（[參事會](../Page/座堂主任牧師.md "wikilink")）成員（頭銜為The
    Reverend Canon）。
  - 正式名單（如[聖徒者](../Page/聖徒.md "wikilink")）。

## 其它宗教

如是[基督教外的宗教下](../Page/基督教.md "wikilink")，**canon**可意指「真传的经典」。如：

  - 佛教的[三藏](../Page/三藏.md "wikilink")。
  - 道教的[道藏](../Page/道藏.md "wikilink")。
  - 中国文化的[典籍](../Page/典籍.md "wikilink")。

## 文学

  - 虛構作品正典/正史(英：Canon
    Fiction）：是指在**虛構作品**（如[文学](../Page/文学.md "wikilink")/[動漫](../Page/動漫.md "wikilink")/[影視劇](../Page/影視作品.md "wikilink")）自成體系的「**世界觀**」（[虛構世界](../Page/虛構世界.md "wikilink")，Fictional
    Universe）。其中由創作者本身所親自發想建立/開創或認可的一系列「官方」設定與信息構成元素，以便讓觀眾能瞭解並分辨何謂其正統，區別其作品本身的「多元宇宙」、「平行宇宙」，甚至是身為「非官方」的[同人作品](../Page/同人作品.md "wikilink")(英：Fan
    Fiction/Fanon）。
  - [西方正典](../Page/西方正典.md "wikilink")，一種西方世界對於其正統文化的觀念。
      - [西方正典](../Page/西方正典.md "wikilink")，[哈羅德·布魯姆於](../Page/哈羅德·布魯姆.md "wikilink")[1994年撰寫的書](../Page/1994年.md "wikilink")。

## 音乐

  - [卡农](../Page/卡农.md "wikilink")，一种[音乐创作技法或者作品类型](../Page/音乐.md "wikilink")。
      - 也可直接代指[卡農 (帕海貝爾)](../Page/卡農_\(帕海貝爾\).md "wikilink")。
  - Kanun，一种西亚弦乐器。

## 影視作品

電影經典（Film canon）

## 企業

  - [佳能](../Page/佳能.md "wikilink")（Canon），[日本佳能公司](../Page/日本.md "wikilink")，以經營[光學](../Page/光學.md "wikilink")、辦公自動化產品產品等相關業務為主。

## 其它

  - [坎揚城](https://web.archive.org/web/20170204170406/http://terms.naer.edu.tw/detail/2198736/)（Canon
    City），位於[富蘭克林郡東部及](../Page/富蘭克林縣_\(喬治亞州\).md "wikilink")[哈特郡西部間的一座城市](../Page/哈特縣_\(喬治亞州\).md "wikilink")。
  - 美學準則（Aesthetic canon）
  - [雅溫得加農炮足球俱樂部](../Page/雅溫得加農炮足球俱樂部.md "wikilink")（Canon Yaoundé）