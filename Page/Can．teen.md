[Can.teen_(Admiralty_Centre_branch).jpg](https://zh.wikipedia.org/wiki/File:Can.teen_\(Admiralty_Centre_branch\).jpg "fig:Can.teen_(Admiralty_Centre_branch).jpg")[海富中心分店](../Page/海富中心.md "wikilink")\]\]
[HK_Admiralty_Citic_Tower_interior_Can-Teen_Maxims_night_March-2013.JPG](https://zh.wikipedia.org/wiki/File:HK_Admiralty_Citic_Tower_interior_Can-Teen_Maxims_night_March-2013.JPG "fig:HK_Admiralty_Citic_Tower_interior_Can-Teen_Maxims_night_March-2013.JPG")[中信大廈分店](../Page/中信大廈.md "wikilink")\]\]
**can．teen**，是[香港一間自助式](../Page/香港.md "wikilink")[連鎖](../Page/連鎖店.md "wikilink")[快餐店](../Page/快餐.md "wikilink")，為[美心食品有限公司旗下品牌](../Page/美心食品有限公司.md "wikilink")，由[m.a.x.
concepts管理](../Page/m.a.x._concepts.md "wikilink")。

[m.a.x.
concepts快餐副線品牌有](../Page/m.a.x._concepts.md "wikilink")[中環](../Page/中環.md "wikilink")[怡和大廈](../Page/怡和大廈.md "wikilink")**Deli-O**，除一般中西快餐食品外，也有包裝壽司提供。

## 歷史

1998年起，美心將旗下多個品牌革新，率先成為**can．teen**的分店有[太子大廈](../Page/太子大廈.md "wikilink")、[海富中心及](../Page/海富中心.md "wikilink")[花旗銀行大廈](../Page/花旗銀行大廈.md "wikilink")、[尖沙咀](../Page/尖沙咀.md "wikilink")[文化中心Curtain](../Page/文化中心.md "wikilink")
Up，以及[中環](../Page/中環.md "wikilink")[怡和大廈](../Page/怡和大廈.md "wikilink")**Deli-O**，並歸入[m.a.x.
concepts管理](../Page/m.a.x._concepts.md "wikilink")。

2003年10月，佔地7,000平方呎的[國際金融中心商場旗艦店開業](../Page/國際金融中心商場.md "wikilink")，由[Hernan
Zanghellini設計](../Page/Hernan_Zanghellini.md "wikilink")，並為首間設有[酒吧的快餐](../Page/酒吧.md "wikilink")。店內八成的坐位都可以坐擁維港海景。開幕日午市時段派發免費飯盒及半價作招徠，引來1,500名市民排隊。後來作小型翻新時將在店最尾的位置[酒吧部份取消](../Page/酒吧.md "wikilink")，並加設彩色螢幕餐牌。

2006年8月，「Curtain Up」裝修後改為**Encore**，2010年10月結業。

2007年3月，[九龍灣](../Page/九龍灣.md "wikilink")[德福廣場翻新](../Page/德福廣場.md "wikilink")，can．teen第五分店開業；10月同場內開設[美心food²](../Page/美心food².md "wikilink")，故該分店已於2008年7月結業，成為[美心皇宮擴展部份](../Page/美心皇宮.md "wikilink")。

2007年12月，Deli-O裝修後重開。

2008年4月，[花旗銀行大廈](../Page/花旗銀行大廈.md "wikilink") can．teen改為[simplylife
FOODPLACE](../Page/simplylife_FOODPLACE.md "wikilink")；10月，[國際金融中心商場分店結業](../Page/國際金融中心商場.md "wikilink")，改為[simplylife
BREAD AND
WINE](../Page/simplylife_BREAD_AND_WINE.md "wikilink")，2009年1月開業。

2009年4月，[大埔](../Page/大埔_\(香港\).md "wikilink")[太和廣場分店開業](../Page/太和廣場.md "wikilink")，為首間不設於商業區的分店，提供食品與[美心MX無異](../Page/美心MX.md "wikilink")。

2010年5月，[中環](../Page/中環.md "wikilink")[太子大廈分店裝修後復業](../Page/太子大廈.md "wikilink")；8月，[中環中心](../Page/中環中心.md "wikilink")[美心MX改為can](../Page/美心MX.md "wikilink")．teen。

2011年11月[中環中心分店結業](../Page/中環中心.md "wikilink")，同月24日，[金鐘](../Page/金鐘.md "wikilink")[中信大廈分店開業](../Page/中信大廈.md "wikilink")。

2013年3月20日，[國際金融中心商場分店結業](../Page/國際金融中心商場.md "wikilink")，改為 Open
Kitchen並於同年6月重開。

2013年11月16日，[怡和大廈](../Page/怡和大廈.md "wikilink") Deli-O 結業，改為Deli and
Wine並於2014年2月7日重開。

## 分店

  - can．teen
      - [金鐘](../Page/金鐘.md "wikilink")
        [海富中心](../Page/海富中心.md "wikilink")
      - [中環](../Page/中環.md "wikilink")
        [太子大廈](../Page/太子大廈.md "wikilink")
      - [中環](../Page/中環.md "wikilink")
        [中環中心](../Page/中環中心.md "wikilink")\[已結業\]
      - [大埔](../Page/大埔_\(香港\).md "wikilink")
        [太和廣場](../Page/太和廣場.md "wikilink")\[已結業\]

<!-- end list -->

  - Deli-O
      - [中環](../Page/中環.md "wikilink")
        [怡和大廈](../Page/怡和大廈.md "wikilink")
      - [上環](../Page/上環.md "wikilink")
        [信德中心](../Page/信德中心.md "wikilink")
      - [太古康怡廣場北座地舖](../Page/太古.md "wikilink")

## 參見

  - [美心食品有限公司](../Page/美心食品有限公司.md "wikilink")

[Category:香港快餐店](../Category/香港快餐店.md "wikilink")
[Category:美心食品](../Category/美心食品.md "wikilink")
[Category:香港連鎖餐廳](../Category/香港連鎖餐廳.md "wikilink")