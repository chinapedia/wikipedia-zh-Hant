**Casey**是一款西文[無襯線體](../Page/無襯線體.md "wikilink")，是[九廣鐵路公司為其](../Page/九廣鐵路公司.md "wikilink")[鐵路系統](../Page/九廣鐵路.md "wikilink")[企業形象而設計的字體](../Page/企業形象.md "wikilink")。Casey字體家族細分下可分成Casey
Regular、Casey Medium和Casey Bold。

## 由來

據聞Casey這個名稱是來自九鐵英文簡稱KCR頭兩個字母的諧音（KC\>Casey）。於網上亦有人製作Casey
Replica字體以模仿Casey字體，都是由Myriad字體系列修改而成的。\[1\]其與Casey的分別是前者間隔較小。實際上，Casey字體和其他相似字體例如[Myriad和](../Page/Myriad.md "wikilink")[Segoe
UI的分別只在於其字體顯得較瘦](../Page/Segoe_UI.md "wikilink")，而數字則以[Formata
Cond加以修改](../Page/Formata_Cond.md "wikilink")。

## 應用

Casey是[九廣鐵路公司委託](../Page/九廣鐵路公司.md "wikilink")[Dalton
Maag製作](../Page/Dalton_Maag.md "wikilink")，於1996年開始率先使用在九鐵標誌英文"KCR"及柴油機車編號上，並於1997年5月號員工雜誌《策力》封面開始使用新字體及標誌。後來於[東鐵都城嘉慕電動列車翻新後](../Page/東鐵綫都城嘉慕電動列車.md "wikilink")，隨後來港服務的[SP1900型電動列車及輕鐵在](../Page/SP1900/1950型電動列車.md "wikilink")1998至2002年間翻新的第1、2期列車及分別在1997年購入的輕鐵第3期列車均於列車編號及車內告示均用上此字體。[九廣西鐵通車時](../Page/西鐵綫.md "wikilink")，更廣泛拓展到作為指示牌、車站名、宣傳單張的官方字型。但個別機構如[恆生](../Page/恆生.md "wikilink")[Cash
dollars的宣傳品](../Page/Cash_dollars.md "wikilink")、[領展](../Page/領展.md "wikilink")[商場的英文專用字型亦有做用類似Casey的字體](../Page/商場.md "wikilink")。

2008年起，[長虹電視](../Page/長虹.md "wikilink")[字體用上了Casey](../Page/字體.md "wikilink")。但显示的Casey字体数字是[等宽的](../Page/等宽.md "wikilink")，不是比例的。

康文署轄下公園及運動場的部分指示牌亦採用Casey字體。

[File:Kcr_exit_05.png|九鐵出口指示上，英文字體用上了Casey](File:Kcr_exit_05.png%7C九鐵出口指示上，英文字體用上了Casey)
<File:Kcr> garbage room plate.jpg|九鐵車站房間標示上的Casey字體（圖片使用了Casey Replica）
<File:Sp1900> throughdoor.jpg|最先使用Casey的地方是車內外的車廂編號（右上角）

## 注释

<div class="references-small">

<references />

</div>

[Category:九廣鐵路](../Category/九廣鐵路.md "wikilink")
[Category:無襯線字體](../Category/無襯線字體.md "wikilink")

1.  I-Circle：Casey
    Replica：\[[http://i-circle.net/downloads/font/casey-replica\]附件解壓縮後的](http://i-circle.net/downloads/font/casey-replica%5D附件解壓縮後的)[PDF檔案內有指出Casey](../Page/PDF.md "wikilink")
    Replica中有若干數量的字元是選用Myriad Pro中的字元作為代替。