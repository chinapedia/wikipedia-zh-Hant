**ChatZilla**是一个基于[Mozilla网络浏览器的](../Page/Mozilla.md "wikilink")[IRC客户端软件](../Page/IRC.md "wikilink")，它是由[XUL和](../Page/XUL.md "wikilink")[JavaScript语言编写的](../Page/JavaScript.md "wikilink")。ChatZilla程序本身十分轻巧，同时由于它是基于Mozilla网络浏览器，所以ChatZilla是[跨平台的](../Page/跨平台.md "wikilink")，可以支持多操作系统下的使用。ChatZilla支持大部IRC客户端软件的特性，如可以同时连接多个IRC服务器，支持[UTF-8等等](../Page/UTF-8.md "wikilink")。ChatZilla支持[JavaScript作为](../Page/JavaScript.md "wikilink")[脚本语言](../Page/脚本语言.md "wikilink")。ChatZilla可以作为[Firefox](../Page/Mozilla_Firefox.md "wikilink")，[Mozilla](../Page/Mozilla.md "wikilink")，[SeaMonkey网络浏览器的插件使用](../Page/SeaMonkey.md "wikilink")，但随着Firefox从第57版开始舍弃对一些老的插件的支持，该软件已不再支持57版及以上的Firefox。

## 参见

  - [IRC](../Page/IRC.md "wikilink")
  - [维基百科 IRC聊天频道](../Page/Wikipedia:IRC聊天频道.md "wikilink")

## 外部链接

  - [ChatZilla on Mozilla
    Add-ons](https://addons.mozilla.org/firefox/addon/chatzilla/)
  - [Mozilla.org ChatZilla
    page](http://www.mozilla.org/projects/rt-messaging/chatzilla/)

[Category:Firefox 附加组件](../Category/Firefox_附加组件.md "wikilink")
[Category:IRC客户端](../Category/IRC客户端.md "wikilink")
[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:使用XUL的軟體](../Category/使用XUL的軟體.md "wikilink")