《**CHEER**》是[台灣創作歌手](../Page/台灣.md "wikilink")[陳綺貞的](../Page/陳綺貞.md "wikilink")[精選輯](../Page/精選輯.md "wikilink")。在台灣於2005年3月29日由[滾石唱片推出](../Page/滾石唱片.md "wikilink")。分為限量的精裝版和普通版。精裝版的包裝採用與[精裝書類似的翻頁設計](../Page/精裝書.md "wikilink")，並且附贈收錄了[音樂錄影帶和演唱會影片的](../Page/音樂錄影帶.md "wikilink")[VCD](../Page/VCD.md "wikilink")。

## 曲目

1.  九份的咖啡店 - 4:50
2.  雨天的尾巴 (滬尾小情歌) - 3:33
3.  讓我想一想 - 3:44
4.  還是會寂寞 - 4:28
5.  躺在你的衣櫃-Guitar - 5:03
6.  吉他手 - 3:00
7.  告訴我 - 5:07
8.  會不會 - 4:17
9.  太聰明 - 4:19
10. 小步舞曲 - 4:17
11. 孩子 - 3:32
12. 慢歌3 - 4:14
13. 嫉妒 - 4:24
14. 溫室花朵 - 4:47
15. 就算全世界與我為敵 - 4:48

### 精裝版VCD

1.  吉他手演唱會精華 - 20:00
2.  九份的咖啡店 [MV](../Page/音樂錄影帶.md "wikilink") - 4:50
3.  雨天的尾巴（滬尾小情歌）MV -3:33
4.  躺在你的衣櫃 MV -5:03
5.  還是會寂寞 MV - 4:28

## 排行資訊

[Category:陳綺貞音樂專輯](../Category/陳綺貞音樂專輯.md "wikilink")
[Category:台灣流行音樂專輯](../Category/台灣流行音樂專輯.md "wikilink")
[Category:2005年音樂專輯](../Category/2005年音樂專輯.md "wikilink")
[Category:滚石唱片音乐专辑](../Category/滚石唱片音乐专辑.md "wikilink")