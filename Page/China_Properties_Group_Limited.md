**China Properties Group
Limited**（），前稱**中國地產集團**，\[1\]是[香港](../Page/香港.md "wikilink")[上市公司之一](../Page/上市公司.md "wikilink")。\[2\]\[3\]\[4\]
它的主要業務是[中國大陸](../Page/中國大陸.md "wikilink")[房地產](../Page/房地產.md "wikilink")[住宅及](../Page/住宅.md "wikilink")[商場](../Page/商場.md "wikilink")、[寫字樓物業項目的發展及銷售](../Page/寫字樓.md "wikilink")。公司註冊地點是[開曼群島](../Page/開曼群島.md "wikilink")。核數師是香港立信德豪會計師事務所有限公司。

## 相關

  - [宏觀調控](../Page/宏觀調控.md "wikilink")
  - [中國](../Page/中國.md "wikilink")[物業市場](../Page/物業市場.md "wikilink")
  - [上海物業市場](../Page/上海物業市場.md "wikilink")
  - [上海康城](../Page/上海康城.md "wikilink")：[上海](../Page/上海.md "wikilink")[閔行區的大型屋苑及](../Page/閔行區.md "wikilink")[零售社區](../Page/零售.md "wikilink")。
  - [協和城](../Page/協和城.md "wikilink")：是上海[步行街](../Page/步行街.md "wikilink")[南京路的大型綜合購物中心](../Page/南京路.md "wikilink")、商住項目。
  - [董事總經理](../Page/董事總經理.md "wikilink")[汪世忠](../Page/汪世忠.md "wikilink")：[加州大學工程](../Page/加州大學.md "wikilink")[博士](../Page/博士.md "wikilink")，[太平協和](../Page/太平協和.md "wikilink")[董事](../Page/董事.md "wikilink")，有多年[土木工程經驗](../Page/土木工程.md "wikilink")。
    \[5\]

## 連結

  - [cpg-group.com](http://www.cpg-group.com/)

## 参考文献

<div class="references-small">

<references />

</div>

[Category:香港上市地產公司](../Category/香港上市地產公司.md "wikilink")
[Category:中國房地產開發公司](../Category/中國房地產開發公司.md "wikilink")
[Category:中國民營企業](../Category/中國民營企業.md "wikilink")
[Category:香港交易所上市公司](../Category/香港交易所上市公司.md "wikilink")
[Category:1992年成立的公司](../Category/1992年成立的公司.md "wikilink")

1.  [China Properties Group
    Limited](http://www.hkexnews.hk/listedco/listconews/sehk/20090213/LTN20090213390_C.pdf)
2.  [香港IPO
    招股章程內容](http://main.ednews.hk/listedco/listconews/sehk/20070209/LTN20070209004_C.htm)

3.  [中國地產上半年溢利增長](http://www.takungpao.com/news/07/07/30/ZM-773058.htm)
4.  [中國地產集團資料](http://www.sanfull.com/pdf/2007_IPO/1838.pdf)
5.  [汪世忠簡介](http://main.ednews.hk/listedco/listconews/sehk/20070209/01838/CWP117_c.pdf)