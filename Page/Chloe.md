**克蘿伊**（Chloe，中國大陸常譯**科洛**，Chloë, Cloe, Chlöe,
[Chloé](../Page/Chloé.md "wikilink"), Clowy, Kloe, Khloe, Khloë,
Khloé, Kloé or Kloë）是女性名字，在英國尤其常見。法語形式為Chloé。

Chloe源自[希臘語](../Page/希臘語.md "wikilink")「（*khlóē*）」，為希臘女神[狄蜜特眾多名稱之一](../Page/狄蜜特.md "wikilink")，意思是年輕的、綠葉、植物的幼芽\[1\]。Chloe這個名字出現在《[新約聖經](../Page/新約聖經.md "wikilink")》，《[哥林多前書](../Page/哥林多前書.md "wikilink")》1:11中的"the
house of Chloe"\[2\]。

  - ，愛爾蘭歌手，女性合唱團體[天使女伶](../Page/天使女伶.md "wikilink")（Celtic Woman）的最年輕成員。

  - [克蘿伊·布莉姬](../Page/克蘿伊·布莉姬.md "wikilink")（Chloe Bridges），美國女演員、歌手。

  - [高儀](../Page/高儀_\(時裝設計師\).md "wikilink")（Chloe
    Dao），越南裔美國女性時裝設計師，真人實境秀《[決戰時裝伸展台](../Page/決戰時裝伸展台.md "wikilink")》第二季的優勝者。

  - （Chloe Jones），美國女性色情演員。

  - （Chloe Marshall），英國大尺碼模特兒，2008年薩里小姐冠軍，英國小姐亞軍。

  - [克蘿伊·摩蕾茲](../Page/科洛·莫瑞兹.md "wikilink")（Chloë Grace
    Moretz），美國童星，在奧斯卡獎得獎電影《雨果的冒險》（Hugo）中飾演女主角伊莎貝爾。

  - [科洛·塞维尼](../Page/科洛·塞维尼.md "wikilink")（Chloë Sevigny），美國女演員。

  - \[3\]（Chloe Sutton），美國競泳選手。

  - （Chloe Webb），美國女演員。

  - [汪可盈](../Page/汪可盈.md "wikilink")（Chloe Wang→Chloe
    Bennet），中美混血女演員及歌手。

  - [王樂妍](../Page/王樂妍.md "wikilink")（Chloe Wang），台灣藝人。

  - [童妮‧摩里森](../Page/托妮·莫里森.md "wikilink")（本名Chloe
    Wofford），美國小說家，1993年獲諾貝爾文學獎。

  - [克洛伊·卡黛珊](../Page/克洛伊·卡黛珊.md "wikilink")\[4\]（Khloé
    Kardashian），美國電視節目主持人。

  - [Daine Chloe de
    Charny](../Page/Daine_Chloe_de_Charny.md "wikilink")，韩团[1ELASTIN法国籍成员](../Page/1ELASTIN.md "wikilink")

## 虛構人物

  - [克洛依·欧布莱恩](../Page/克洛依·欧布莱恩.md "wikilink")（Chloe
    O'Brian），美國電視劇《24小時反恐任務》的角色。

## 電影

  - [色·誘](../Page/色·誘.md "wikilink")（Chloe），2009年情色驚悚片，改拍自2003年電影《[愛上娜塔莉](../Page/愛上娜塔莉.md "wikilink")》。

## 參見

  -
  -
## 參考文獻

[Category:希臘語女性名字](../Category/希臘語女性名字.md "wikilink")
[Category:英語女性名字](../Category/英語女性名字.md "wikilink")

1.
2.
3.
4.