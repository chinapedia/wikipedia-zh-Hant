**Cinemax**，又稱**Max**，是一個專門播放[電影的](../Page/電影.md "wikilink")[有線電視網絡](../Page/有線電視.md "wikilink")，客戶需訂購才可收看。Cinemax為[HBO集團](../Page/HBO.md "wikilink")（由[時代華納所擁有](../Page/時代華納.md "wikilink")）的max系列的其中一個頻道，從[美國](../Page/美國.md "wikilink")[加州](../Page/加州.md "wikilink")[三藩市傳送播放](../Page/三藩市.md "wikilink")。Cinemax在美國還會在深夜播放[色情節目](../Page/色情.md "wikilink")，所以也被人稱作"skin-a-max"（露出最多肌膚）。

## 歷史

**Cinemax**在1980年8月開始播放，此頻道是由"Robert
Kulp"提議播放的。"Kulp"介紹Cinemax為所有節目只會有關電影。在當時，同集團頻道[HBO除了播放電影](../Page/HBO.md "wikilink")，還播放紀錄片、兒童節目、體育賽事和娛樂精選；因此Cinemax只會播放電影，早期多播放美國經典電影（Amercian
Movie Classic,
AMC），為頻道最重要的節目時段，節目表上滿是1950年代至1970年代的電影。Cinemax的電影全未經剪接，也沒有商業廣告。

Cinemax在早期非常成功，原因是客戶只需在約三十個頻道上追加此頻道便可以了。電影是有線電視訂戶最想收看的頻道，而Cinemax播放沒有廣告、沒有剪接的經典電影也吸引[HBO訂戶再訂講Cinemax](../Page/HBO.md "wikilink")。事實上，有線電視供應商不會供非[HBO用戶訂購Cinemax](../Page/HBO.md "wikilink")，這兩條頻道以套餐形式銷售，若訂購兩條通常會有折扣。在1980年代初，HBO大約一個月美金13元，而Cinemax則需再加美金7-10元一個月。

由於在美國本土的電影頻道增加，Cinemax為求留住客戶，所以改變節目定位，最初決定播放更多暴力的電影（[HBO只在深夜播放此類電影](../Page/HBO.md "wikilink")），之後再加入成人電影。現在美國的Cinemax仍會播放[成人電影](../Page/成人電影.md "wikilink")，特別是為成人電視網絡提供的，也因此為Cinemax帶來很多別名，如"Skin-a-max"，
"Sinamax" 和 "Climax"等。

## Max系列頻道

Cinemax為Max系列的其中一個頻道，Max系列還包括以下頻道：

  - Cinemax：旗艦頻道，主要播放精選影集、首輪電影和受歡迎影片。
      - Cinemáx: Cinemax的西班牙語版、其前身為青少年頻道@max。
  - Moremax：附屬頻道，內容與Cinemax相似，還包括外地影片、獨立製片公司電影和藝術電影。
  - Actionmax：動作電影頻道，播放精選動作電影、西方片集、武術和戰爭影片。
  - Outermax：科幻電影頻道，同時播放驚慄電影和幻想曲。
  - Thrillermax：懸疑電影頻道，同時播放神秘、戰顫和恐怖電影。
  - Moviemax：以18-34歲觀眾為對象，其前身為女士頻道Wmax。
  - 5Starmax：精選電影頻道，播放現代名著、獲獎無數和受歡迎的古典電影。

## Cinemax（亞洲）

**Cinemax**是[HBO
Asia的其中一條頻道](../Page/HBO_Asia.md "wikilink")，專門播放熱門的動作、懸疑的刺激類電影。Cinemax和HBO
Asia的總部和[時代華納（亞洲）同樣在](../Page/時代華納.md "wikilink")[新加坡](../Page/新加坡.md "wikilink")。與[美國本土版不同的是](../Page/美國.md "wikilink")，Cinemax（亞洲）並沒有播放成人節目；Cinemax（亞洲）更於2003年重新定位為"Get
Into The Movies"。

### 歷史

在1996年正式引入[亞洲](../Page/亞洲.md "wikilink")，同年開始於[新加坡](../Page/新加坡.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[泰國和](../Page/泰國.md "wikilink")[菲律賓播放](../Page/菲律賓.md "wikilink")；1998年開始於[斯裏蘭卡播放](../Page/斯裏蘭卡.md "wikilink")；2000年開始於[香港有線電視播放](../Page/香港有線電視.md "wikilink")。2006年7月1日起和[HBO系列頻道一同在](../Page/HBO.md "wikilink")[香港](../Page/香港.md "wikilink")[Now寬頻電視獨家播放](../Page/Now寬頻電視.md "wikilink")。

中國大陸地區可通過亞太六號衛星上的中國境外衛星電視統一平台接收到Cinemax，該服務一般只提供三星級以上的[酒店和政府指定的居住區](../Page/酒店.md "wikilink")。

## 参见

  - [HBO](../Page/HBO.md "wikilink")
  - [HBO Signature](../Page/HBO_Signature.md "wikilink")

## 外部連結

  - [Cinemax 美國官方網站](http://www.cinemax.com/)
  - [Cinemax 亞洲官方網站](http://www.cinemaxasia.com/)
  - [Cinemax](http://www.youtube.com/user/Cinemax)於
    [YouTube](../Page/YouTube.md "wikilink")

[Category:台灣電視播放頻道](../Category/台灣電視播放頻道.md "wikilink")
[Category:美國電視台](../Category/美國電視台.md "wikilink")
[Category:國際媒體](../Category/國際媒體.md "wikilink")
[Category:HBO](../Category/HBO.md "wikilink")
[Category:無廣告電視網](../Category/無廣告電視網.md "wikilink")