**Circle**專輯，為[木村KAELA的個人第二張專輯](../Page/木村KAELA.md "wikilink")，於2006年3月8日由哥倫比亞音樂發行。

## 解說

  - 離前作同名專輯「KAELA」1年又3個月的第2張專輯。包含單曲「[Real Life Real
    Heart](../Page/Real_Life_Real_Heart.md "wikilink")」「[BEAT](../Page/BEAT_\(木村KAELA\).md "wikilink")」「[You](../Page/You_\(木村KAELA\).md "wikilink")」，以及其C/W曲「Twinkle」「PIONEER」也收錄。
  - 初回限定盤附DVD，收錄三支單曲的PV。
  - 台灣於2006年4月17日由風雲唱片代理發行台壓（CD+DVD版本）。

## 曲目

作詞全是Kaela本人（包含You也是）

1.  **[Real Life Real
    Heart](../Page/Real_Life_Real_Heart.md "wikilink")**
      - 作曲：曾田茂一
      - 為代言日本手機vodafone的廣告曲
2.  **tea cup**
      - 作曲：高桑圭
3.  **I ♥ hug**
      - 作曲：堀江博久
4.  **[BEAT](../Page/BEAT_\(木村KAELA\).md "wikilink") (Album ver.)**
      - 作曲：[奥田民生](../Page/奥田民生.md "wikilink")
5.  '''TRILL TRILL RECUR
      - 作曲：會田茂一
      - 電影「[令人討厭的松子的一生](../Page/令人討厭的松子的一生.md "wikilink")」的插曲。電影原聲帶「令人討厭的松子之歌」中也收錄。
6.  **Twinkle (NANA ver.)**
      - 作曲：會田茂一
      - 為NANA概念專輯「[LOVE for NANA～Only 1
        Tribute～](../Page/LOVE_for_NANA～Only_1_Tribute～.md "wikilink")」收錄曲。
7.  **[You](../Page/You_\(木村KAELA\).md "wikilink")**
      - 作詞、作曲：渡邊忍
      - 為代言雀巢Kit Kat巧克力的廣告曲
8.  **PIONEER**
      - 作曲：會田茂一
9.  **Deep Blue Sky**
      - 作曲：吉村秀樹（bloodthirsty butchers）
10. **Dancing now**
      - 作曲：岸田繁（團團轉）
11. **Circle**
      - 作曲：mito（可樂棒）
12. **蜂蜜**
      - 作曲：會田茂一
13. **C-hildren**
      - 作曲：田渕ひさ子

[Category:木村KAELA專輯](../Category/木村KAELA專輯.md "wikilink")
[Category:2006年音樂專輯](../Category/2006年音樂專輯.md "wikilink")