[ClearTypeLine.PNG](https://zh.wikipedia.org/wiki/File:ClearTypeLine.PNG "fig:ClearTypeLine.PNG")線；3和4分別為1和2的四倍放大圖；5是1實際顯示在液晶顯示器上的放大示意圖。如圖所示，ClearType充分利用LCD色條排列特性，顯示出更為完美的斜線。\]\]

**ClearType**，由[美國](../Page/美國.md "wikilink")[微軟公司在其](../Page/微軟公司.md "wikilink")[视窗](../Page/视窗.md "wikilink")[操作系统中提供的螢幕](../Page/操作系统.md "wikilink")[亚像素微调](../Page/亚像素微调.md "wikilink")[字體平滑工具](../Page/字體平滑.md "wikilink")，讓Windows字體更加漂亮。ClearType主要是針對LCD[液晶顯示器設計](../Page/液晶顯示器.md "wikilink")，可提高文字的清晰度。基本原理是，将显示器的R,
G, B各个次像素也发光，让其色调进行微妙调整，可以达到实际分辨率以上（横方向分辨率的三倍）的纤细文字的显示效果。

Windows上的[像素和显示器上的像素对应的](../Page/像素.md "wikilink")[液晶显示器上效果最为明显](../Page/液晶.md "wikilink")，使用阶调控制一般CRT显示器上也可以得到一些效果。

在[Windows XP平台上](../Page/Windows_XP.md "wikilink")，這項技術預設是關閉，到了[Windows
Vista才預設為開啟](../Page/Windows_Vista.md "wikilink")。而与ClearType几乎同样的技术在[苹果电脑的](../Page/苹果电脑.md "wikilink")[Mac
OS操作系统中](../Page/Mac_OS.md "wikilink")，早在1998年发布的[Mac OS
8.5就已经使用了](../Page/Mac_OS_8.5.md "wikilink")。

另外，依靠ClearType技术提高字体的可读性，相当程度上依赖于使用的字体，所以和原有的**标准抗锯齿**技术不能进行单纯比较。

## 顯示器需求

如果顯示器不具有適用於ClearType的像素組合特性，以ClearType顯示文字的實際效果會比使用前還要差。部分平面顯示器面板使用不常見的像素排列方式，例如可能有不同的顏色排列順序，或不同軸向的子像素位置（三條水平線等），所以需要為特定的顯示器手動調整ClearType的顯示效果。不具有固定排列組合的顯示器，如CRT，將難以閱讀ClearType所顯示的文字。

\==

## 中文ClearType

大多Windows默认的中文字型在显示小文字时使用[点阵来显示](../Page/位图.md "wikilink")，不使用ClearType。微软在[Windows
Vista裡](../Page/Windows_Vista.md "wikilink")，新發佈了两个ClearType中文字型：[微软雅黑和](../Page/微软雅黑.md "wikilink")[微軟正黑體](../Page/微軟正黑體.md "wikilink")。

[<File:ClearType-ZHCN.jpg>](https://zh.wikipedia.org/wiki/File:ClearType-ZHCN.jpg "fig:File:ClearType-ZHCN.jpg")

  -
    在Windows 7中使用ClearType字体放大6倍的效果。

## 参见

  -
[Category:數碼字體排印](../Category/數碼字體排印.md "wikilink")
[Category:Windows多媒体](../Category/Windows多媒体.md "wikilink")