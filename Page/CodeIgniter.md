**CodeIgniter**是一套给[PHP网站开发者使用的应用程序开发框架和工具包](../Page/PHP.md "wikilink")。它提供一套丰富的标准库以及简单的接口和逻辑结构，其目的是使开发人员更快速地进行项目开发。

## 特性

和[Ruby on
Rails类似](../Page/Ruby_on_Rails.md "wikilink")，CodeIgniter允许用户使用[Active
Record作为](../Page/Active_Record.md "wikilink")[数据库的接口](../Page/数据库.md "wikilink")，并鼓励使用[模型-视图-控制器的](../Page/模型-视图-控制器.md "wikilink")[架构模式](../Page/架构模式.md "wikilink")。

  - 基于[MVC体系](../Page/MVC.md "wikilink")
  - 超[轻量级](../Page/轻量级.md "wikilink")
  - 对多种[数据库平台的全特性支持的数据库类](../Page/数据库.md "wikilink")
  - [Active Record支持](../Page/Active_Record.md "wikilink")
  - 表单与数据验证
  - [安全性与](../Page/安全性.md "wikilink")[XSS过滤](../Page/XSS.md "wikilink")
  - [Session管理](../Page/Session.md "wikilink")
  - 邮件发送类，支持[附件](../Page/附件.md "wikilink")、[HTML或文本邮件](../Page/HTML.md "wikilink")，多种协议（[sendmail](../Page/sendmail.md "wikilink")、[SMTP和](../Page/SMTP.md "wikilink")[Mail](../Page/Mail.md "wikilink")）等等。
  - 图像处理类库（[剪裁](../Page/剪裁.md "wikilink")、[缩放](../Page/缩放.md "wikilink")、[旋转等](../Page/旋转.md "wikilink")）。支持[GD](../Page/GD.md "wikilink")、[ImageMagick和](../Page/ImageMagick.md "wikilink")[BetPBM](../Page/BetPBM.md "wikilink")
  - 文件上传类
  - [FTP类](../Page/FTP.md "wikilink")
  - [本地化](../Page/本地化.md "wikilink")
  - 分页
  - 数据加密
  - 基准测试
  - 全页面[缓存](../Page/缓存.md "wikilink")
  - 错误日志
  - 应用程序评测
  - 日历类
  - [User-Agent类](../Page/用户代理.md "wikilink")
  - Zip编码类
  - [模板引擎类](../Page/模板.md "wikilink")
  - [Trackback类](../Page/Trackback.md "wikilink")
  - [XML-RPC类库](../Page/XML-RPC.md "wikilink")
  - [单元测试类](../Page/单元测试.md "wikilink")
  - “搜索引擎友好”的URL
  - 灵活的[URI路由](../Page/URI.md "wikilink")
  - 支持[勾子](../Page/勾子.md "wikilink")，类扩展
  - 大量的[辅助函数](../Page/辅助函数.md "wikilink")

## CodeIgniter 项目的分支

在CodeIgniter项目中，存在一个名为[Kohana的分支项目](../Page/Kohana.md "wikilink")。

Kohana是一个使用了[MVC模式的应用程序框架](../Page/MVC.md "wikilink")。它是以安全，轻量，易用为目标进行开发的。

## 参见

  - [Web框架比较](../Page/Web框架比较.md "wikilink")

## 参考资料

## 外部链接

  -
  -
  - [CodeIgniter简体中文网站](http://codeigniter.org.cn)

  - [CodeIgniter简体中文使用手册(Wiki)](https://web.archive.org/web/20071020025013/http://codeigniter.org.cn/user_guide/toc.html)

  - [CodeIgniter繁體中文網站](http://codeigniter.org.tw)

  - [CodeIgniter繁體中文使用手冊](http://codeigniter.org.tw/user_guide/)

[Category:Web应用框架](../Category/Web应用框架.md "wikilink") [Category:Web
2.0](../Category/Web_2.0.md "wikilink")
[Category:PHP](../Category/PHP.md "wikilink")
[Category:电脑小作品](../Category/电脑小作品.md "wikilink")