**Code Geass**（）是一个日本ACG系列作品，包括：

## 第一作：反叛的鲁路修

  - **[Code Geass
    反叛的魯路修](../Page/Code_Geass_反叛的魯路修.md "wikilink")**（第一季：2006\~07年；R2：2007\~08年），TV动画，该系列的成名作品和主要作品
  - [娜娜莉梦游仙境](../Page/Code_Geass_反叛的鲁路修#OVA：娜娜莉梦游仙境.md "wikilink")（2012年），OVA动画
  - [Code Geass 娜娜莉的梦魇等漫画](../Page/Code_Geass_娜娜莉的梦魇.md "wikilink")
  - [叛逆的魯魯修：失去的色彩](../Page/叛逆的魯魯修：失去的色彩.md "wikilink")，游戏

## 第二作：亡國的AKITO

  - [Code Geass
    亡國的阿基德](../Page/Code_Geass_亡國的阿基德.md "wikilink")（2012\~16年），剧场版动画，主作品
  - [Code Geass 漆黑的蓮夜](../Page/Code_Geass_漆黑的蓮夜.md "wikilink")，漫画
  - [Code Geass 雙貌的OZ](../Page/Code_Geass_雙貌的OZ.md "wikilink")，小说&漫画

## 第三作：复活的鲁路修

  - [Code Geass
    反叛的鲁路修：兴道](../Page/Code_Geass_反叛的鲁路修：兴道.md "wikilink")（2017年），剧场版动画，总篇集
  - [Code Geass
    反叛的鲁路修：叛道](../Page/Code_Geass_反叛的鲁路修：叛道.md "wikilink")（2018年），剧场版动画，总篇集
  - [Code Geass
    反叛的鲁路修：皇道](../Page/Code_Geass_反叛的鲁路修：皇道.md "wikilink")（2018年），剧场版动画，总篇集
  - [Code Geass
    复活的鲁路修](../Page/Code_Geass_复活的鲁路修.md "wikilink")（2019年），剧场版动画，主作品

[\*](../Page/Cat:Code_Geass.md "wikilink")