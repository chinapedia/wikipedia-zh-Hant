[coleco-adam.jpg](https://zh.wikipedia.org/wiki/File:coleco-adam.jpg "fig:coleco-adam.jpg")
**Coleco Adam** 是[美国的玩具制造商](../Page/美国.md "wikilink")
[Coleco](../Page/Coleco.md "wikilink")
公司企图接续其[电视游乐器](../Page/电视游乐器.md "wikilink")
[ColecoVision](../Page/ColecoVision.md "wikilink")
于1980年代初的成功而推出的[家用电脑](../Page/家用电脑.md "wikilink")。取名为
Adam（[亚当](../Page/亚当.md "wikilink")）是希望它能「咬」一口[苹果公司的市场份额](../Page/苹果公司.md "wikilink")（参见[伊甸园](../Page/伊甸园.md "wikilink")），然而它并不成功。有一部份是由于早期的量产问题。

## 简介

Coleco于1983年6月的夏季[国际消费电子展中首次发表Adam](../Page/国际消费电子展.md "wikilink")，主管人员们并予估其将能在1983年圣诞节达到50万台的销量。从上市发表至正式出货的期间，建议售价从525美元上涨至725美元。

Adam于1983年发表时有个小插曲。为了炫耀它，Coleco决定展示从ColecoVision移植至Adam的[*大金刚*游戏](../Page/大金刚_\(游戏\).md "wikilink")。[任天堂此时正在与](../Page/任天堂.md "wikilink")[Atari商议授权将](../Page/Atari.md "wikilink")[FC游戏机销售至日本以外地区](../Page/FC游戏机.md "wikilink")，而最后签署预计要在国际消费电子展中完成。Atari对于*大金刚*游戏的家用电脑版本有独占权利（而Coleco则拥有电视游乐器版本的权利），当Atari看到Coleco在电脑上展示大金刚时，他们与任天堂计划好的交易被延迟了。Coleco被迫同意不能贩售大金刚的Adam版本，致使其最后被腰斩。但Atari与任天堂的交易亦未付诸实行，因为Atari的[CEO在一个月后被开除](../Page/CEO.md "wikilink")，而该计划草案亦不了了之。最终，任天堂决定贩售自己开发的系统。

## 技术资讯

受到厚爱的Adam一开始就有丰富的软件库。它是由ColecoVision转变而来，并兼容ColecoVision的软件和附件。当时很普遍的[CP/M](../Page/CP/M.md "wikilink")[操作系统也是一种选择](../Page/操作系统.md "wikilink")。消费者买到的是一套完整的系统：一台有64[KB](../Page/KB.md "wikilink")[内存的电脑](../Page/内存.md "wikilink")、录音机、信件品质的[打印机及软件](../Page/打印机.md "wikilink")（包含*[Buck
Rogers: Planet of
Zoom](../Page/Buck_Rogers:_Planet_of_Zoom.md "wikilink")*游戏）。[IBM
PCjr当时的售价是](../Page/IBM_PCjr.md "wikilink")669美元，但不付任何周边设备。至于[Commodore
64](../Page/Commodore_64.md "wikilink")，虽然主机售价仅约200美元，但在购买打印机、录音机或软驱及软件后，费用仍相当可观。

如同当时的许多其他电脑，Adam使用电视做为显示器。SmartWriter"电子打字机"在开机时会自动启动。在这个模式里，系统就如同打字机一般工作－能立刻打印出用户输入的文字。按下Escape/WP键则能使SmartWriter切换至文书处理模式，如同现代的文书处理器一般工作。

一个较便宜的Adam版本可以插在ColecoVision上，这被宣称是提供一种较便宜的方式让ColecoVision用户可以升级到Adam。

## 问题

Adam有以下幾個缺點：

  - Adam在开机时会产生强大的磁场，其足以破坏所有留在机内或主机旁之磁性媒体的数据。\[1\]更糟的是，某些Coleco的说明书指示用户在开机前放入录音带－据推测这些说明书可能是在此问题被发现前印的。\[2\]
  - 由于Coleco应用了相当奇怪的设计－从打印机供应所有系统的电源，若是打印机故障，所有的系统都将无法作动。\[3\]
  - 与当时其他电脑不一样的是，Adam没有内建于[ROM的BASIC编译器](../Page/ROM.md "wikilink")。它内建的是SmartWriter－"电子打字机"与文书处理器、EOS(Elementary
    Operating System)操作系统核心与8K OS-7
    [ColecoVision操作系统](../Page/ColecoVision.md "wikilink")。SmartBASIC编译器另附于其专有的Digital
    Data Pack录音带格式媒体中。
  - 若切换至文书处理模式，SmartWriter无法在不重开机的情况下切回打字机模式。
  - Adam的Digital Data
    Pack驱动器虽然比竟争对手的录音机更快，储存容量也更高，但较不可靠且仍无法与[软驱的速度相比](../Page/软驱.md "wikilink")。Coleco最终推出了160K
    [5英寸软驱给Adam使用](../Page/5英寸软驱.md "wikilink")。

## 反馈

Adam由于其高品质的键盘、打印机和具竟争力的音效、图像而收到了一些好评。它的[BASIC](../Page/BASIC.md "wikilink")[编译器](../Page/编译器.md "wikilink")－[SmartBASIC](../Page/SmartBASIC.md "wikilink")－与[Applesoft
BASIC的兼容非常好](../Page/Applesoft_BASIC.md "wikilink")，也就是说许多电脑书、杂志介绍的type-in程序不需修改或及需微小修改即可在Adam上运行。

然而，销售并不理想，尤其是在技术问题浮上台面后。至1984年末，Coleco由于退货涌入而赔掉了3500万美元。Coleco将其怪罪于"使用手册未能给予新用户足够的指导。"\[4\]
Coleco重新推出Adam，附带了新的使用手册、以较低的价格贩售。每台给孩子用的Adam还附带500美元的大学奖学金（孩子们考上大学时支付）。最后，Adam的销量未能突破10万，并于1985年终止，距上市发表未满两年。

## 后期

一家叫做Telegames的公司买下了Adam的存货与权利并透过邮购贩售。他们继续了一些游戏的开发并售出大幅重新设计过的产品－Personal
Arcade，直到该公司被龙卷风摧毁财产为止。\[5\]

Adam使Coleco赔掉的钱即使是以[Cabbage Patch
Kids赚来的钱也无法弥补](../Page/Cabbage_Patch_Kids.md "wikilink")。Coleco于1988年申请宣告[破产](../Page/破产.md "wikilink")。

如同当时许多其他电脑，Adam即使在离开市场后仍有一群死忠支持者。有一群狂热份子每年都会在[Adamcon聚会上聚集在一起](../Page/Adamcon.md "wikilink")。第19届聚会于2007年7月26日至7月29日于[加拿大](../Page/加拿大.md "wikilink")[安大略省](../Page/安大略.md "wikilink")[渥太华举办](../Page/渥太华.md "wikilink")。

## 配置

[Underside_of_a_Coleco_Adam.jpg](https://zh.wikipedia.org/wiki/File:Underside_of_a_Coleco_Adam.jpg "fig:Underside_of_a_Coleco_Adam.jpg")
[Coleco_Adam,_with_monitor,_keyboard,_printer,_and_software.jpg](https://zh.wikipedia.org/wiki/File:Coleco_Adam,_with_monitor,_keyboard,_printer,_and_software.jpg "fig:Coleco_Adam,_with_monitor,_keyboard,_printer,_and_software.jpg")
[Coleco_Adam,_with_monitor,_keyboard,_and_software.jpg](https://zh.wikipedia.org/wiki/File:Coleco_Adam,_with_monitor,_keyboard,_and_software.jpg "fig:Coleco_Adam,_with_monitor,_keyboard,_and_software.jpg")

  - CPU： [Zilog Z80](../Page/Zilog_Z80.md "wikilink") @ 3.58 MHz
  - 支持处理器： 三个[Motorola 6801](../Page/Motorola_6800.md "wikilink") @ 1
    MHz（内存 & I/O、录音带与键盘控制）
  - 内存：64KB，16KB视频内存，32KB ROM
  - 扩充：3个内部插槽、1个卡匣插槽与一62.5[kbit/s](../Page/kbit/s.md "wikilink")
    [half-duplex序列槽](../Page/duplex_\(telecommunications\).md "wikilink")－AdamNet。在右侧另有一个与ColecoVision相同的扩充插槽。
  - 第二存储媒体：Digital Data Pack录音带，256KB
  - 图像：[德州仪器](../Page/德州仪器.md "wikilink")
    TMS9928A（类似在[TI-99/4A内的](../Page/Texas_Instruments_TI-99/4A.md "wikilink")[TMS9918](../Page/Texas_Instruments_TMS9918.md "wikilink")）
      - 256 × 192 分辨率
      - 32 [叠图](../Page/叠图.md "wikilink")
  - 音效：德州仪器SN76489AN
      - 3声
      - [白噪音](../Page/白噪音.md "wikilink")

## 参考来源与外部链接

  - [Coleco Adam Technical Reference
    Manual](https://archive.is/20120524101918/http://drushel.cwru.edu/atm/atm.html)
    (Coleco Industries, Inc., 1984)
  - [Coleco ADAM
    Schematics](https://archive.is/20121210084211/http://drushel.cwru.edu/schematics/)
    (in PDF format; restored and unrestored)
  - [Video Game
    Nostalgia's](https://web.archive.org/web/20070927061937/http://www.techtite.com/Features/Adam.html)
    Adam page
  - [Expandable Computer News
    Archive](http://www.sacnews.net/adamcomputer/)
  - [ADAM article at The Dot
    Eaters](https://web.archive.org/web/20061120011835/http://www.thedoteaters.com/p3_stage4.php)，an
    extensive history of Coleco and the ADAM
  - [www.old-computers.com](http://www.old-computers.com/museum/computer.asp?st=1&c=57)
    Coleco Adam's page
  - [Adamcon](http://www.adamcon.org/) home page
  - [1](http://oldcomputers.net/adam.html) Oldcomputers.net's Adam page
    has some nice photos of components

[Category:早期电脑](../Category/早期电脑.md "wikilink")
[Category:个人电脑](../Category/个人电脑.md "wikilink")

1.
2.
3.
4.
5.