《****》是2004年2月6日由[戲畫發售的](../Page/戲畫_\(遊戲品牌\).md "wikilink")[十八禁](../Page/十八禁遊戲.md "wikilink")[戀愛冒險遊戲](../Page/戀愛冒險遊戲.md "wikilink")。[KOTOKO主唱](../Page/KOTOKO.md "wikilink")、作詞、[I've製作的主題曲](../Page/I've.md "wikilink")「」和Colorful系列前作的主題曲「」一樣，是網上「惹笑的[電波歌曲](../Page/電波歌曲.md "wikilink")」的代表之一。發售前曲名曾一度發表為「」，其後變為「」。2007年12月14日發售對應[Windows
Vista的](../Page/Windows_Vista.md "wikilink")「Memorial Edition（）」。

和前作《》在內容上並無關聯。

## 登場人物

  -










    （聲：[森野花梨](../Page/森野花梨.md "wikilink")）

## 參見

  - [戲畫](../Page/戲畫_\(遊戲品牌\).md "wikilink")

  - （Colorful系列第一作）

  - （Colorful系列第三作）

## 外部連結

（以下為年齡限制網站）

  - [戲畫](http://www.web-giga.com/top/top.html)

  -
  - [Colorful Kiss/Colorful Heart Memorial
    Edition](http://www.web-giga.com/colorful_me/colorfulme.htm)

[Category:戲畫](../Category/戲畫.md "wikilink")
[Category:美少女遊戲](../Category/美少女遊戲.md "wikilink")
[Category:戀愛冒險遊戲](../Category/戀愛冒險遊戲.md "wikilink")
[Category:2004年日本成人遊戲](../Category/2004年日本成人遊戲.md "wikilink")
[Category:高中題材電子遊戲](../Category/高中題材電子遊戲.md "wikilink")