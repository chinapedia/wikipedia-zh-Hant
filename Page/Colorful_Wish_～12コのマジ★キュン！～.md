《****》是於2008年4月25日由[戲畫發售的](../Page/戲畫_\(遊戲品牌\).md "wikilink")[十八禁](../Page/十八禁遊戲.md "wikilink")[戀愛冒險遊戲](../Page/戀愛冒險遊戲.md "wikilink")。和前兩作一樣，主題曲《》是由[KOTOKO主唱](../Page/KOTOKO.md "wikilink")、[I've製作](../Page/I've.md "wikilink")。

## 登場人物

  -












## 參見

  - [戲畫](../Page/戲畫_\(遊戲品牌\).md "wikilink")

  - （Colorful系列第一作）

  - （Colorful系列第二作）

## 參考資料

  -
## 外部連結

（以下為年齡限制網站）

  - [戲畫](http://www.web-giga.com/top/top.html)
  - [遊戲官方網站](https://web.archive.org/web/20100615085518/http://www.web-giga.com/colorfulwish/)

[Category:戲畫](../Category/戲畫.md "wikilink")
[Category:美少女遊戲](../Category/美少女遊戲.md "wikilink")
[Category:戀愛冒險遊戲](../Category/戀愛冒險遊戲.md "wikilink")
[Category:2008年日本成人遊戲](../Category/2008年日本成人遊戲.md "wikilink")
[Category:魔法學校背景作品](../Category/魔法學校背景作品.md "wikilink")
[Category:高中題材電子遊戲](../Category/高中題材電子遊戲.md "wikilink")