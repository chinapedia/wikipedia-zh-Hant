[CompactFlash.jpg](https://zh.wikipedia.org/wiki/File:CompactFlash.jpg "fig:CompactFlash.jpg")
**CompactFlash**（**CF卡**）最初是一种用于便携式电子设备的数据存储设备，于1994年首次由[SanDisk公司生产并制定了相关规范](../Page/SanDisk.md "wikilink")。它的物理格式曾被多种设备所采用。从外形上CF卡可以分为两种：CF
I型卡以及稍厚一些的CF II型卡。从速度上它可以分为CF卡、高速CF卡（CF+/CF
2.0规范）、CF3.0、CF4.0，更快速的CF4.1标准也在2007年被采用。CF
II型卡槽主要用于[微型硬盘等一些其他的设备](../Page/微型硬盘.md "wikilink")。

CF是与出现更早且尺寸更大的PCMCIA
I型内存卡竞争的第一批闪存标准之一，它最初是建立在[英特尔的](../Page/英特尔.md "wikilink")[或非型闪存的基础上](../Page/或非型闪存.md "wikilink")，之后改为使用[与非型闪存](../Page/与非型闪存.md "wikilink")。CF是最老也是最成功的标准之一，尤其在早期的专业数码相机市场。

CF卡可以通过转换器直接用于[PCMCIA卡插槽](../Page/PCMCIA卡.md "wikilink")，也可以通过[读卡器连接到多种常用的端口](../Page/读卡器.md "wikilink")，如[USB](../Page/USB.md "wikilink")、[Firewire等](../Page/Firewire.md "wikilink")。另外，由于它具有较大的尺寸（相对于较晚出现的小型存储卡而言），大多数其他格式的存储卡可以通过适配器在CF卡插槽上使用，其中包括[SD卡](../Page/SD卡.md "wikilink")／[MMC卡](../Page/MMC卡.md "wikilink")、[Memory
Stick
Duo](../Page/Memory_Stick_Duo.md "wikilink")、[XD卡以及](../Page/XD卡.md "wikilink")[SmartMedia卡等](../Page/SmartMedia.md "wikilink")。

[Compactflash.jpg](https://zh.wikipedia.org/wiki/File:Compactflash.jpg "fig:Compactflash.jpg")

## 概述

由於使用的NOR型[閃存存儲密度低於較新的NAND閘](../Page/閃存.md "wikilink")[閃存](../Page/閃存.md "wikilink")，CF卡的體積是90年代初期出現的三種存儲卡中最大的（另兩種是[Miniature
Card](../Page/Miniature_Card.md "wikilink")—MiniCard和[SmartMedia卡](../Page/SmartMedia.md "wikilink")）在之後，CF卡也改用了NAND型閃存，另外，IBM的微型硬盤使用CF
II型介面，但並不是[固態硬碟](../Page/固態硬碟.md "wikilink")。[日立和](../Page/日立.md "wikilink")[希捷也有製造微型硬碟](../Page/希捷.md "wikilink")。

CompactFlash的電氣特性與[PCMCIA](../Page/PCMCIA.md "wikilink")-ATA接口一致，但外形尺寸較小。

連接器為43毫米寬，外殼的深度是36毫米，厚度分3.3毫米（CF I型卡）和5毫米（CF II型卡）兩種

CF I型卡可以用於CF II型卡插槽，但CF II型卡由於厚度的關係無法插入CF I型卡的插槽中。CF閃存卡多數是CF I型卡。

CF卡比早期的PC卡（PCMCIA）I型更細小，然而厚度則和PC卡I及II型相同。CF卡是早期記憶卡規格之中最成功的，受歡迎程度比Miniature
Card、[SmartMedia卡及PC卡I型更勝一籌](../Page/SmartMedia.md "wikilink")。在應用在體積較小的器材時，SmartMedia卡曾經是CF卡的主要競爭對手，從市場滲透率而言甚至一度超越CF卡。不過，SmartMedia的優勢，受制於其最大128MB的容量，逐漸被其他新制式的記憶卡淘汰（大約於2002－2005年時）。

九十年代末至廿一世紀初出現的記憶卡制式（如[SD](../Page/Secure_Digital.md "wikilink")／[MMC](../Page/MultiMedia卡.md "wikilink")／[Secure
Digital High Capacity（SDHC）](../Page/SDHC.md "wikilink")，各種[Memory
Stick](../Page/Memory_Stick.md "wikilink")，[xD圖像卡等等](../Page/xD圖像卡.md "wikilink")）有著激烈競爭。新款記憶卡的體積比CF卡小數倍，某程度上與當時CF比PC
Card的相差還要大。新制式已主導掌上電腦，手提電話以及消費級數碼相機，特別是超迷你型號。

虽然如此，CF卡目前依然是一些顶级專業數碼相機的标准配置。在2007年出產的半專業級數碼相機（專業消費級之數碼相機、單鏡反光相機）中，有相當比例還是支援CF卡。過往，CF的特點主要是以最少的價錢換取最大的MB數，但如今相同容量的小型记忆卡已比CF卡便宜得多。現時高端SD卡的容量和性能已经不输CF卡，但平均来说CF卡仍往往有着比其他小型記憶卡更高的容量（自CF
1.0標準開始已支援137GB之理論上限，至2007年第三季，最大容量為64GB），更快的存取速度（Sandisk Extreme IV
:40MB/s，CF 4.0標準支援最高133MB/s），更開放的規格及使用條款，較佳的兼容性（自CF 1.0標準起極少出現重大的改動，CF
Type II能使用MicroDrive，以及透過轉接器使用多種較小記憶卡）。

閃存型存儲設備具有非易失性和固態，所以它比磁盤驅動器更穩固，耗電量僅相當於磁盤驅動器的5%，卻仍然具有較快的傳輸速率（SanDisk
Extreme
IV型CF卡的寫入速度和讀取速度可達40MB/s）。它們的工作電壓為3.3[伏特或](../Page/伏特.md "wikilink")5[伏特](../Page/伏特.md "wikilink")，可以在不同的系統間轉換。閃存型CF卡可以適應極端的溫度變化，工業標準的閃存卡可以在-45至85[攝氏度的範圍內工作](../Page/攝氏度.md "wikilink")。

到2018年，CF卡的容量規格從最小的8[MB到最大可達](../Page/Megabyte.md "wikilink")256[GB](../Page/Gigabyte.md "wikilink")。（這裡的1MB=1,000,000[byte](../Page/byte.md "wikilink")，1GB=1000MB）

## 微型硬盘

[MicroDrive1GB.jpg](https://zh.wikipedia.org/wiki/File:MicroDrive1GB.jpg "fig:MicroDrive1GB.jpg")
1GB微型硬盘\]\]

微型硬盘是一种符合CF-II型标准的微型[硬盘驱动器](../Page/硬盘驱动器.md "wikilink")（约1英寸宽）。在1999年[IBM首次发布了拥有](../Page/IBM.md "wikilink")340MB容量的微型硬盘，后于2002年将其部门连同Microdrive商标一起卖给了[日立](../Page/日立.md "wikilink")。目前，许多其他的厂商（如：[希捷](../Page/希捷.md "wikilink")、[索尼等](../Page/索尼.md "wikilink")）也在出售微型硬盘，截止到2005年中期，微型硬盘的容量已经达到了6GB并且还在高速成长着。

做为一种机械式存储设备，这些微型硬盘在运行的时候需要消耗比闪存更多的能源，所以在某些供能不足的设备上它们也许不能很好的运行。同时，作为机械设备，它们对物理震动和温度的变化要比闪存更加敏感，尽管在实际使用中并无大碍。

## CF规范

在CompactFlash規範第一次標準化的時候，即使是全尺寸的硬盤的容量也很少超過4GB的，因此ATA規範自身存在的限制被認為是可接受的。但是，在硬盤由於不斷增長的容量需求而對ATA規範作出大量改變的今天，閃存卡很快就超過了4GB的限制。

  - **CF+**（或CF2.0）包括了两个较大的变化：数据传输率提高到16MB/s，容量最大可达到137GB（根据CompactFlash协会（CFA）的资料）
  - CF3.0，支持UDMA mode 4，最高66MB/s
  - CF4.0，支持UDMA mode 5，最高100MB/s
  - CF4.1，支持UDMA mode 6，最高133MB/s Up to 137GB
  - CF5.0，支持UDMA mode 6，最高133MB/s Up to 144PB
  - CF6.0，支持UDMA mode 7，最高167MB/s Up to 144PB

## 符合CF标准的其他设备

CompactFlash规范还被应用于多种输入／输出以及接口设备。由于它的电气性能与[PCMCIA卡一致](../Page/PCMCIA卡.md "wikilink")，因此大多数[PCMCIA卡都有类似的CF版本](../Page/PCMCIA卡.md "wikilink")。如：

  - [以太网](../Page/以太网.md "wikilink")
  - [调制解调器](../Page/调制解调器.md "wikilink")
  - [无线局域网](../Page/无线局域网.md "wikilink")
  - [数码相机](../Page/数码相机.md "wikilink")
  - [GPS](../Page/GPS.md "wikilink")（全球衞星定位系統）
  - [条码扫描仪](../Page/条码扫描仪.md "wikilink")
  - [磁条读写器](../Page/磁条.md "wikilink")
  - [Super VGA显示适配器](../Page/Super_VGA.md "wikilink")
  - 多种其他[闪存卡的读卡器](../Page/闪存卡.md "wikilink")
  - [GBA电影播放器](../Page/GBA电影播放器.md "wikilink")

## 相关文章

  - [微硬盘](../Page/微硬盘.md "wikilink")
  - [XQD卡](../Page/XQD卡.md "wikilink")
  - [PCMCIA](../Page/PCMCIA.md "wikilink")
  - [多媒体卡](../Page/多媒体卡.md "wikilink")
  - [xD卡](../Page/xD卡.md "wikilink")
  - [SD卡](../Page/SD卡.md "wikilink")
  - [记忆棒](../Page/记忆棒.md "wikilink")
  - [SmartMedia卡](../Page/SmartMedia.md "wikilink")

## 外部链接

  - [CompactFlash协会](http://www.compactflash.org/)
  - [Rob Galbraith DPI: CF Performance
    Database](https://web.archive.org/web/20130503200620/http://www.robgalbraith.com/bins/multi_page.asp?cid=6007)
  - [SATA to
    CF轉接卡](http://www.minerva.com.tw/2.5%20SATA%20Adapter%20for%20CF%20Card.html)

[Category:記憶卡](../Category/記憶卡.md "wikilink")