**Los Angeles Complexity**，又稱**Complexity Gaming**，**Team
compLexity**，**compLexity**，**coL**。是一支美國[電子競技的隊伍](../Page/電子競技.md "wikilink")，創立於[美國](../Page/美國.md "wikilink")[加州](../Page/加州.md "wikilink")[洛杉磯](../Page/洛杉磯.md "wikilink")，創辦人為[傑森·雷克](../Page/傑森·雷克.md "wikilink")（代號**1**）於2004年成立。目前隊伍中包括有[絕對武力：次世代](../Page/絕對武力：次世代.md "wikilink")、生死格鬥4、世界街頭賽車3以及[FIFA
2007的電子遊戲隊伍](../Page/FIFA_2007.md "wikilink")。舊有的隊伍遊戲項目包括[絕對武力](../Page/絕對武力.md "wikilink")、[決勝時刻](../Page/決勝時刻.md "wikilink")、[DotA
Allstars](../Page/DotA_Allstars.md "wikilink")、[雷神之鎚4](../Page/雷神之鎚4.md "wikilink")、[決勝之日以及](../Page/勝利日.md "wikilink")[決勝之日：次世代](../Page/勝利日：起源.md "wikilink")。
目前Complexity也是美國G7隊伍中的一員。

## 現役隊員

<table>
<caption><strong><em><a href="../Page/絕對武力：次世代.md" title="wikilink">絕對武力：次世代</a></em></strong></caption>
<thead>
<tr class="header">
<th></th>
<th><p>名字</p></th>
<th><p>遊戲名稱</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>Matt Dickens</p></td>
<td><p>Warden</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Ronald Kim</p></td>
<td><p>Rambo</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Danny Montaner</p></td>
<td><p>fRoD</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Marcus Sundstrom</p></td>
<td><p>zet</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Tyler Wood</p></td>
<td><p>Storm</p></td>
</tr>
</tbody>
</table>

<table>
<caption><strong><em><a href="../Page/生死格鬥4.md" title="wikilink">生死格鬥4</a></em></strong></caption>
<thead>
<tr class="header">
<th></th>
<th><p>名字</p></th>
<th><p>遊戲名稱</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>Megan Ceder</p></td>
<td><p>beLLE</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Adande Thorne</p></td>
<td><p>sWooZIe</p></td>
</tr>
</tbody>
</table>

<table>
<caption><strong><em><a href="../Page/世界街頭賽車3.md" title="wikilink">世界街頭賽車3</a></em></strong></caption>
<thead>
<tr class="header">
<th></th>
<th><p>名字</p></th>
<th><p>遊戲名稱</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>Javier Gutierrez</p></td>
<td><p>Chavisan</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>C.J. Nelson</p></td>
<td><p>GeeTeeOhh</p></td>
</tr>
</tbody>
</table>

<table>
<caption><strong><em><a href="../Page/FIFA_2007.md" title="wikilink">FIFA 2007</a></em></strong></caption>
<thead>
<tr class="header">
<th></th>
<th><p>名字</p></th>
<th><p>遊戲名稱</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>Felipe Stoyne</p></td>
<td><p>KreeganBG</p></td>
</tr>
</tbody>
</table>

## 連結

  - [Official Website](http://www.complexitygaming.com)

[Category:美国电子竞技团队](../Category/美国电子竞技团队.md "wikilink")