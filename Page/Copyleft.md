[Copyleft.svg](https://zh.wikipedia.org/wiki/File:Copyleft.svg "fig:Copyleft.svg")

**Copyleft**是一由[自由軟體運動所發展的概念](../Page/自由軟體運動.md "wikilink")，是一種利用現有-{著}-作權體制來挑戰該體制的授權方式，在自由軟體授權方式中增加copyleft條款之後，該自由軟體除了允許使用者自由使用、散佈、改作之外，copyleft條款更要求使用者改作後的衍生作品必須要以同等的授權方式釋出以回饋社群。

Copyleft的中文译法包括「著作傳」\[1\]\[2\]、「著佐權」\[3\]、「反版權」、「版權屬左」、「版權左派」、「公共版權」、「版責」、「版可」等。\[4\]Copyleft授權方式雖然與常見的著作權授權模式不同：選擇Copyleft授權方式並不代表作者放棄著作權，反而是贯彻始终，强制被授权者使用同样授权发布衍生作品，copyleft授權條款不反對著作權的基本體制，卻是透過利用著作權法來進一步地促進創作自由。

## 授權方式

「版權」（[Copyright](../Page/Copyright.md "wikilink")）的概念是藉由賦予對著作的專有權利的方式提供作者從事創作之經濟動機，但相對的此種賦予作者專有權利的方式同時也限制了他人任意使用創作物的自由。Copyleft則是自由軟體運動為了保護這種自由而發展的概念：它**允許他人任意的修改散佈作品，惟其散佈及修改的行為和作法，亦限定以Copyleft的方式行之**。

Copyleft作品是有版權的，但它們加入了法律上的分發條款，保障任何人都擁有對該作品及其衍生品的使用、修改和重新發佈的權力，惟前提是這些發佈條款不能被改變。

Copyleft不同於傳統的[公有領域](../Page/公有領域.md "wikilink")（public
domain）。因為公共領域的作品，任何使用者雖然都可以使用，但可以不回饋變成已用并获取利益；而Copyleft作品的使用者若是向外界发布衍生作品，不按Copyleft的許可證要求保持同樣的授權條款，並將更改的版本公开，就是違反授权的侵權行為。

Copyleft是将一個程式变為自由軟體的通用方法，同時也使得這個程式的修改和擴充版本成為自由軟體。提出並使用Copyleft觀念的是[GNU計畫](../Page/GNU.md "wikilink")，具體的發佈條款包含在[GNU通用公共授權條款](../Page/GNU通用公共授權條款.md "wikilink")、[GNU較寬鬆公共許可證](../Page/GNU較寬鬆公共許可證.md "wikilink")、[GNU自由文件授權條款的條款裡](../Page/GNU自由文件授權條款.md "wikilink")。其他使用copyleft概念的還有[創作共用的](../Page/創作共用.md "wikilink")[姓名標示─相同方式分享](http://creativecommons.org/licenses/by-sa/3.0/tw/)。

## 標誌

Copyleft的標誌是一個反寫的C套上圓圈（即反轉的[版權標誌](../Page/版權.md "wikilink")©）。這個標誌沒有法律意義。\[5\]2016被提議\[6\]加入到Unicode並獲Unicode技術委員會許可。\[7\]其編碼收錄在Unicode
11。\[8\]\[9\]由於現時多數字型沒有實作，可以由或更常用的加上括號組成「(ɔ)」，或者在程式支援的情況下與組合：「ↄ⃝」。\[10\]

## 屬於Copyleft的授權方式

  - [GNU通用公眾授權條款](../Page/GNU通用公眾授權條款.md "wikilink")
  - [GNU較寬鬆公共許可證](../Page/GNU較寬鬆公共許可證.md "wikilink")
  - [GNU自由文件授權條款](../Page/GNU自由文件授權條款.md "wikilink")
  - [創作共用的](../Page/創作共用.md "wikilink")[姓名標示─相同方式分享](http://creativecommons.org/licenses/by-sa/3.0/tw/)條款
  - [自由艺术作品许可协议](../Page/自由艺术作品许可协议.md "wikilink")

## 参考文献

## 外部链接

  - [What is Copyleft?](http://www.gnu.org/copyleft/copyleft.html)（什麼是
    copyleft？）－[理查德·斯托曼撰寫](../Page/理查德·马修·斯托曼.md "wikilink")
  - [GNU 公告 第一卷 第四号](http://www.gnu.org/bulletins/bull4.html)——第一篇介紹「什么是
    copyleft？」的文章
  - [自由和力量](http://www.gnu.org/philosophy/freedom-or-power.html)，－理查德·斯托曼，Bradley
    Kuhn
  - [Copyleft: Pragmatic
    Idealism](http://www.gnu.org/philosophy/pragmatic.html)－理查德·斯托曼
  - [Linus
    Torvalds谈Linux软件商业趋势](http://seattletimes.nwsource.com/html/businesstechnology/2002059632_linus11.html)（2004年10月专访）
  - [Copyleft and
    Copyright](http://www.eyemagazine.com/opinion.php?id=117&oid=290)－《Eye》杂志

## 參見

  - [著作權](../Page/著作權.md "wikilink")

  - [All Rights Reversed](../Page/All_Rights_Reversed.md "wikilink")

  - [Copyleft作品的商业使用](../Page/Copyleft作品的商业使用.md "wikilink")

  -
  - [自由著作权](../Page/自由著作权.md "wikilink")

  - [創作共用](../Page/創作共用.md "wikilink")

  - [电子前沿基金会](../Page/电子前沿基金会.md "wikilink")

  - [技术法律术语](../Page/技术法律术语.md "wikilink")

  - [开放音乐](../Page/开放音乐.md "wikilink")

  - [平均分摊](../Page/平均分摊.md "wikilink")

  - [GNU通用公共许可协议](../Page/GNU通用公共许可协议.md "wikilink")

  - [反对版权](../Page/反对版权.md "wikilink")

  - [Copyleft艺术](../Page/Copyleft艺术.md "wikilink")

  - [創用CC公眾授權條款](../Page/創用CC公眾授權條款.md "wikilink")

  - [自由文化运动](../Page/自由文化运动.md "wikilink")

  - [自由软件基金会](../Page/自由软件基金会.md "wikilink")

  - [开放游戏许可协议](../Page/开放游戏许可协议.md "wikilink")

  - [公有领域](../Page/公有领域.md "wikilink")

  - [GNU自由文件授權條款](../Page/GNU自由文件授權條款.md "wikilink")

  - [藍絲帶](../Page/藍絲帶.md "wikilink")

  - [反著作權運動](../Page/反著作權運動.md "wikilink")

{{-}}

[Category:Copyleft](../Category/Copyleft.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.
9.

10.