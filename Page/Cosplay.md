[Cosplayer_of_Dark_Magician_Girl,_Yu-Gi-Oh\!_20161029.jpg](https://zh.wikipedia.org/wiki/File:Cosplayer_of_Dark_Magician_Girl,_Yu-Gi-Oh!_20161029.jpg "fig:Cosplayer_of_Dark_Magician_Girl,_Yu-Gi-Oh!_20161029.jpg")角色“黑魔導女孩”的cosplayer\]\]
**Cosplay**（，[和製英語](../Page/和製英語.md "wikilink")：，簡稱****（Cosplay）），中文一般翻譯為「**角色扮演**\[1\]」或「**扮裝**」，是指利用服裝、飾品、道具還有化妝等手段來扮演動漫、遊戲中人物角色的一種[表演藝術行為](../Page/表演藝術.md "wikilink")。

參與Cosplay行為的扮裝者在日文稱為「」（可簡稱「」），中文一般稱「**扮裝者**」、「**角色扮演者**」或「**角色扮演員**」，英文「cosplayer」或大陸則簡稱「coser」。

Cosplay的名稱起源於1978年[Comic
Market召集人米澤嘉博氏為場刊撰文時](../Page/Comic_Market.md "wikilink")，以「costume
Play」來指出裝扮為動漫角色人物的行為，並由日本動畫家於1984年在[美國](../Page/美國.md "wikilink")[洛杉磯舉行的](../Page/洛杉磯.md "wikilink")時確立以和製英語詞語「Cosplay」來表示。

而當代的Cosplay通常被視為一種[次文化活動](../Page/次文化.md "wikilink")，扮演的對象[角色一般來自](../Page/角色.md "wikilink")[動畫](../Page/動畫.md "wikilink")、[漫畫](../Page/漫畫.md "wikilink")、[電子遊戲](../Page/電子遊戲.md "wikilink")、[輕小說](../Page/輕小說.md "wikilink")、[電影](../Page/電影.md "wikilink")、影集、[特攝](../Page/特攝.md "wikilink")、[偶像團體](../Page/偶像.md "wikilink")、[職業](../Page/職業.md "wikilink")、[歷史](../Page/歷史.md "wikilink")[故事](../Page/故事.md "wikilink")、[社會故事](../Page/社會.md "wikilink")、現實世界中具有傳奇性的獨特事物（或其[擬人化形態](../Page/擬人化.md "wikilink")）、或是其他自創的有形[角色](../Page/角色.md "wikilink")。方法是特意穿戴相似的[服飾](../Page/服飾.md "wikilink")，加上[道具的配搭](../Page/道具.md "wikilink")，[化粧造型](../Page/化粧.md "wikilink")、[身體語言等等](../Page/身體語言.md "wikilink")[參數來模仿該等角色](../Page/参数.md "wikilink")。

## 词源

在1984年美國[洛杉磯舉行的](../Page/洛杉磯.md "wikilink")上，赴會的日本動畫家暨日本藝術工作室「」行政總裁把一種自力演繹角色的扮裝性質表演藝術行為正式以一條[和製英語詞語](../Page/和製英語.md "wikilink")「Cosplay」來表示，Cosplay因而得名。

之後到1990年代，日本ACG業界成功舉辦了大量的動漫畫展和遊戲展，此時的日本漫畫商和[電玩公司為了宣傳自身產品](../Page/電玩.md "wikilink")，在這些遊戲展和漫畫節中找來一些男男女女裝扮成ACG作品中的角色以吸引參展人群。這一招可以說是與當年和路迪士尼開辦迪士尼樂園的時引客招數如出一轍，由此可見當代Cosplay的顯露成形與蓬勃發展的關鍵是建立在本身ACG商業化的程度之上，可以說正是將Cosplay作為一種商業上的促銷手段，Cosplay其本身才可能得到長足的發展與認識。就因此，Cosplay文化在ACG界熱門化和發揚光大，同時藉著各種Cosplay活動、傳媒的介紹、[互聯網有關Cosplay的大量資訊傳播等](../Page/互聯網.md "wikilink")，使Cosplay的自由參與者激增，Cosplay才漸漸得到了真正的、獨立的發展。更甚者，專門為Cosplay行為而舉行的活動也漸漸出現，形式類似化粧舞會，遂民眾逐漸能在越來越多的場合中看到該批奇裝異服者，並了解到這集服飾、化粧、表演於一體的扮裝文化現象──Cosplay。
\[2\]

當代的Cosplay一般以[動畫](../Page/動畫.md "wikilink")、[漫畫](../Page/漫畫.md "wikilink")、[遊戲](../Page/遊戲.md "wikilink")、[電玩](../Page/電玩.md "wikilink")、[輕小說](../Page/輕小說.md "wikilink")、[電影](../Page/電影.md "wikilink")、影集、[特攝](../Page/特攝.md "wikilink")、[偶像團體](../Page/偶像.md "wikilink")、職業、歷史故事、社會故事或是其他自創的有形[角色為目標](../Page/角色.md "wikilink")，刻意穿著類似的[服飾](../Page/服飾.md "wikilink")，加上[道具的配搭](../Page/道具.md "wikilink")，[化粧造型](../Page/化粧.md "wikilink")、[身體語言等等](../Page/身體語言.md "wikilink")[參數](../Page/参数.md "wikilink")，以人力扮演成一個「活起來」的角色。另一種Cosplay主要是對非人類的動物、軍事武器、交通工具、土木基建、作業系統、網站等進行[擬人化](../Page/擬人化.md "wikilink")，灌以具智慧的靈魂，並以相應服飾、道具、化粧、身體語言等配套來呈現該等擬人化角色，其中一常見手法乃以[萌擬人化形態出場](../Page/萌擬人化.md "wikilink")。

## 歷史

### 角色扮裝

[En_garde_02.jpg](https://zh.wikipedia.org/wiki/File:En_garde_02.jpg "fig:En_garde_02.jpg")
扮裝最早的起源可能是來自於對[神話](../Page/神話.md "wikilink")[傳說](../Page/傳說.md "wikilink")、民間逸聞等的演繹，以及節日故事、文藝作品、[哲理學說](../Page/哲學.md "wikilink")、[祭祖情節](../Page/祭祖.md "wikilink")、振奮助興情節、側繹願望訴求、心靈幻想等，並以相應的[服飾](../Page/服飾.md "wikilink")、[道具和情節](../Page/道具.md "wikilink")，把要演繹的角色和內容活靈活現地呈現出來。這些活動通常屬於戲劇表演、民俗活動等。比如說有[古希臘](../Page/古希臘.md "wikilink")[祭司們的裝扮](../Page/古希腊宗教.md "wikilink")，繼而有兩部偉大希臘史詩《[伊利亞特](../Page/伊利亞特.md "wikilink")》和《[奧德賽](../Page/奧德賽.md "wikilink")》的那群活躍於前8世紀的[吟遊詩人們扮演著別人的角色](../Page/吟遊詩人.md "wikilink")。前者引變為後世的[先知](../Page/先知.md "wikilink")、先見，成功地演繹出神之[使徒的存在](../Page/使徒.md "wikilink")，而後者則如同是現今[話劇的鼻祖](../Page/話劇.md "wikilink")，出神入化地演繹出若干英雄事蹟。

歐洲的遊牧民族[吉普賽人也可以說是最早的一批扮裝表演者](../Page/吉普賽人.md "wikilink")。每當路經一地，為了生存，他們就透過演出神話傳說、民間逸聞、吟遊彈唱的方式來獲得麵包與水，這其中各種演出用的服飾與道具自然是必不可少的裝備。隨時隨地舉行的角色、[萬聖節遊行](../Page/萬聖節.md "wikilink")、[新年大遊行](../Page/新年.md "wikilink")、[國慶日遊行活動或特別盛典時中](../Page/國慶日.md "wikilink")，不少人裝扮成節日故事的人物或各類[吉祥物](../Page/吉祥物.md "wikilink")，濃厚的[扮裝文化得以體現](../Page/扮裝.md "wikilink")。

[中國古代的先民也有著歷史悠久的扮裝文化](../Page/中國.md "wikilink")。具有千年傳統的[舞龍儀式可以說是其中最具代表性的活動](../Page/舞龍.md "wikilink")。舞龍在當時往往有兩種寓意，一種是祈求上蒼降甘露給農田，另一種則有祈求五穀豐登、萬象吉祥之意。此活動進行前，首先要選出體格健壯、姿態威武的男性青年若干位，並讓他們穿上黃、紅色的代表喜慶的服飾（有時為貴族表演時衣服上甚至繡有花紋），按照事先的編舞他們將組成一支或多支舞龍隊伍表演出各種方陣圖案，並有[鼓](../Page/鼓.md "wikilink")[鑼聲作為伴奏](../Page/鑼.md "wikilink")。到17世紀左右（即[明末](../Page/明.md "wikilink")[清初](../Page/清.md "wikilink")），由舞龍中又繁衍出了舞獅、[鳳舞龍翔的活動](../Page/鳳舞龍翔.md "wikilink")，這些都與以服飾扮裝某些角色都有著緊密的聯繫。

[西藏民間神話英雄的中國](../Page/西藏.md "wikilink")[藏戲](../Page/藏戲.md "wikilink")、[超渡亡魂到](../Page/超渡.md "wikilink")[極樂凈土和](../Page/極樂凈土.md "wikilink")[印度](../Page/印度.md "wikilink")[佛教中的佛事](../Page/佛教.md "wikilink")、祭祀[山靈](../Page/山靈.md "wikilink")[神器与](../Page/神器.md "wikilink")[日本](../Page/日本.md "wikilink")[神道教](../Page/神道教.md "wikilink")[神社活動](../Page/神社.md "wikilink")，服飾、道具、表演都是這些活動的重要組成元素\[3\]。

### 迪士尼的推廣

1930年代末期[和路迪士尼](../Page/和路迪士尼.md "wikilink")[米奇老鼠出現](../Page/米奇老鼠.md "wikilink")，[美國的動畫風格有了一個明確的定義](../Page/美國.md "wikilink")，而史上真正的第一個以動畫人物為受扮者的Cosplay也正是出於此時期。和路迪士尼看準時機適時的在1955年創建了世界上首座[迪士尼樂園](../Page/迪士尼樂園.md "wikilink")，同時為了替產品自身作宣傳及為更好的吸引遊客，和路迪士尼還特別請來員工穿上米奇老鼠服飾以供遊客玩賞或是拍照留念。當初這群默默無聞的米奇老鼠裝扮者就是當代全世界Cosplay行為的真正始祖。

起初為當時那群在迪士尼樂園中裝扮成[米奇老鼠](../Page/米奇老鼠.md "wikilink")、[布魯托](../Page/布魯托.md "wikilink")、[高飛狗](../Page/高飛狗.md "wikilink")、[唐老鴨以及其他迪士尼人物製作Cosplay服飾的是和路迪士尼公司早先的道具部](../Page/唐老鴨.md "wikilink")。在迪士尼樂園正式成立後不久，和路迪士尼擴大了道具部的規模，不僅要為影視作品製作道具，更負責所有在迪士尼樂園工作所需的Cosplay服飾。早期用作Cosplay的服飾只是一個擁有固定外形的「大紙袋」，缺乏美感和舒適，成品相對也較粗糙，裝扮者穿上這種服飾後很容易發生呼吸不暢的現象。縱使如此，此時迪士尼的Cosplay服飾製作已算是擁有了一定的規模。

當代Cosplay最初成形的目的仍是出於一種商業上的形為而並非像現在這樣是一種流行品位上的消費。將美國或是更確切的一點說，將迪士尼作為當代Cosplay的真正發源其實還有一個很重要的依據，那就是當時迪士尼卡通人物裝扮者們身上所穿著Cosplay服飾的專業製作化。雖然以現在的Cosplay服飾而言，有許多是裝扮者們自己所縫製的。但是作為一個當代Cosplay的鼻祖，擁有一個規範並且體系化的服飾製作組織是必要的條件。

### 與日本動漫結合

日本的[ACG](../Page/ACG.md "wikilink")（指的是Animations動畫、Comics漫畫、Games遊戲）市場興起自1947年漫畫之神[手塚治蟲根據](../Page/手塚治蟲.md "wikilink")原作改編而成的[紅皮書](../Page/紅皮書.md "wikilink")[漫畫](../Page/漫畫.md "wikilink")《》，為日本[ACG的地位打下了堅實的基礎](../Page/ACG.md "wikilink")。恰好正在此時，迪士尼那種所為宣傳而作的Cosplay活動被傳入日本，有ACG界同好起而模仿，漸漸蔚為風潮，最終成了日本現在ACG界的常態活動。直到在1955年左右，日本的扮裝活動都僅僅只是小童間的玩意，但在服飾方面還是頗為講究。當時不少小童都裝扮《[月光假面](../Page/月光假面.md "wikilink")》與《[少年傑特](../Page/少年傑特.md "wikilink")》這兩部作品的主人公。當時的日本並沒有如迪士尼樂園般擁有專門的Cosplay服飾製作單位和行號，裝扮者如想要擁有與動畫中主人公相同服飾的話就必須先請畫家繪好服飾設計圖樣，然後再到百貨公司或裁縫店請師傳縫製。現今著名的遊戲製作人[廣井王子小時候Cosplay的服飾設計圖](../Page/廣井王子.md "wikilink")，便是請離他家附近很近的一條[藝妓街上的那些藝妓為他繪製的](../Page/藝妓.md "wikilink")。這種較為粗制的狀況一直維持了將近二十年的時間，直至1970年代末至1980年代初日本的ACG經歷了探索和成長期之後，此時日本的Cosplay活動在起初是作為[看版娘在](../Page/看版娘.md "wikilink")[同人誌即賣會而生](../Page/同人誌即賣會.md "wikilink")，為各同好會等場合上活躍氣氛的一种即興節目，後期引申為伴隨著動漫展覽、遊戲發佈會上頻繁出現。[One_Piece_cosplayers_in_Yokohama_20050703.jpg](https://zh.wikipedia.org/wiki/File:One_Piece_cosplayers_in_Yokohama_20050703.jpg "fig:One_Piece_cosplayers_in_Yokohama_20050703.jpg")的Cosplay\]\]

## 類型與呈現形式

總括人類歷史出現過的Cosplay範疇、類型、來源和性質，大致可定位如下，包括但不限於：

  - 有形像的[文藝作品](../Page/文藝.md "wikilink")
      - [ACG](../Page/ACG.md "wikilink")：[動畫](../Page/動畫.md "wikilink")、[漫畫](../Page/漫畫.md "wikilink")、[遊戲](../Page/遊戲.md "wikilink")、[電玩](../Page/電玩.md "wikilink")、[輕小說](../Page/輕小說.md "wikilink")、[浮文字](../Page/浮文字.md "wikilink")
      - [影視](../Page/影視.md "wikilink")：[電視劇](../Page/電視劇.md "wikilink")、[電影](../Page/電影.md "wikilink")、[廣告](../Page/廣告.md "wikilink")、[特攝等](../Page/特攝.md "wikilink")
      - [偶像團體](../Page/偶像.md "wikilink")：[藝人](../Page/藝人.md "wikilink")、[視覺系人物](../Page/視覺系.md "wikilink")、[節目](../Page/節目.md "wikilink")[吉祥物等](../Page/吉祥物.md "wikilink")
      - [布袋戲](../Page/布袋戲.md "wikilink")
      - ……
  - [名人](../Page/名人.md "wikilink")
  - [神話傳說](../Page/神話傳說.md "wikilink")
  - [民間逸聞](../Page/民間逸聞.md "wikilink")
  - [節日故事](../Page/節日故事.md "wikilink")
  - [哲理學說](../Page/哲理學說.md "wikilink")
  - [祭祖情節](../Page/祭祖情節.md "wikilink")
  - [振奮助興情節](../Page/振奮助興情節.md "wikilink")
  - [擬人化](../Page/擬人化.md "wikilink")（或稱「智慧體化」）
      - [萌擬人化](../Page/萌擬人化.md "wikilink")
  - [Fursuit](../Page/Fursuit.md "wikilink")（以穿著特定動物外型的全身式皮毛衣 以扮演演繹特定動物）
  - 基於現實世界、歷史故事、社會故事的人、事、物、職業人員、道理等穿的Cosplay
  - ……

（因應情況，一個角色可以涉及以上多個類型，各不相剋）

而以呈現形式而言，也可以分為：

  - [寫實的Cosplay](../Page/寫實.md "wikilink")，可比喻為類似[西方寫實畫](../Page/西方.md "wikilink")（[實用畫](../Page/實用畫.md "wikilink")），旨在儘量複製受扮者之外形與靈魂。
  - [精神形態上的Cosplay](../Page/精神形態.md "wikilink")，可比喻為類似古代[東方以描繪精神形態為旨的圖畫](../Page/東方.md "wikilink")（非實用畫），重點在於服飾與道具能反映受扮者之精神形態、靈魂與心靈訴求，而非完全複製外形，或以文字為基礙再按需要生成外形。當中可能會引用比如[二次創作的手法](../Page/二次創作.md "wikilink")。

因應情況，而一個角色可以同時涉及以上兩種性質
[Harajuku_bridge_02.jpg](https://zh.wikipedia.org/wiki/File:Harajuku_bridge_02.jpg "fig:Harajuku_bridge_02.jpg")的Cosplayer\]\]

## 爭議

### 審視與判斷的不同尺度

由於Cosplay發展形式多樣，各派各界人士解讀Cosplay的用途與立場，有著不同的差異，但在審視判斷方面的尺度大致上有分為兩種，也因此形成兩大派系：

  - 其中一種尺度以行為作判斷條件，只要有關行為與外觀（服飾、道具、化妝、身體語言的配搭）含有模仿／扮演某角色（或其變形原體）機能的成份和元素，即予以推斷其屬於Cosplay。
  - 另一種尺度則以行為、心態與目的並重作判斷條件，除須達到上述行為與外觀方面的條件外，負責扮演的人員還需要具備明確的思維與意識──「忠於原著與[角色](../Page/角色.md "wikilink")，力求在各方面正確地扮演某角色（服飾、道具、化妝、身體語言甚至髮型的配搭要完全合乎原著呈示標準，舉止、動作、神態等神韻也要完全合乎原著呈示標準，達至「形神俱似」的要求），並在藉此表達因出自自身對該角色之熱愛而扮演」，而且角色的名份、外形、性格、傳奇與情節資料等也需要有具體的直接[可靠來源支持並廣為人知以便大眾隨時對照參考](../Page/Wikipedia:可靠來源.md "wikilink")，當上述所有條件皆完全符合方予以推斷其屬於Cosplay。

後者為動畫、漫畫、遊戲、影視、獨特打扮歌手等相關產品的擁躉群體派系（以下簡稱「[偶像擁躉派](../Page/偶像.md "wikilink")」）把Cosplay品位為一種以「藉在有關感官上模仿角色表達忠於角色的訊息」作為達成慾望手段的興趣行為時所主要採用。

當個別情況尺度標準使用不當，甚至藉此吹毛求疵、指鹿為馬，可能會引發爭議，比如興趣性「偶像擁躉派」為力保勢力範圍情況「一統化」而堅持只使用其尺度與立場來達成淨空異己的目的，甚至否認個別類型的Cosplay行為以及一切純粹「興趣目的先決」以外的Cosplay行為（比如「商業宣傳目的先決」、「宗教儀式目的先決」、「政治目的先決」、「牟利目的先決」、「話劇目的先決」、「戲劇目的先決」、「舞臺劇目的先決」、「攝影目的先決」、「賣弄性感色情目的先決」、「為Cosplay而Cosplay」等Cosplay行為）。\[4\]
[Cosplay_-_Naruto_e_Kakashi.jpg](https://zh.wikipedia.org/wiki/File:Cosplay_-_Naruto_e_Kakashi.jpg "fig:Cosplay_-_Naruto_e_Kakashi.jpg")的Cosplay\]\]

### 其他爭議點

部份人士認為在人類歷史傳統上，Cosplay有被用作演繹神話傳說、民間逸聞、節日故事、文藝作品、哲理學說、[祭祖情節](../Page/祭祖.md "wikilink")、振奮助興情節、側繹願望訴求、心靈幻想等，並以相應的服飾、道具和情節，把要演繹的角色和內容活靈活現地呈現。而另一部份人士比如「偶像擁躉派」則認為當今常見的Cosplay的用途只能作為純綷抒發興趣──出於對於該角色的喜愛而作出的角色扮演行為，與古代角色扮演行為的用途與動機有所出入，而且Cosplay一詞乃於1984年方確立，所以認為遠至古代的角色扮演行為不應視為Cosplay的一種；而因著對Cosplay的含義、用途與動機的認受程度關係，他們僅視Cosplay起源最多只遠至1970年代末至1980年代初[看板娘在](../Page/看板娘.md "wikilink")[同人誌即賣會出現的歷史](../Page/同人誌即賣會.md "wikilink")，並否認類似用途與動機以外的角色扮演行為為Cosplay。

部份人士認為裝扮者可以按自己的感興、知識累積以及其他客觀與主觀情況，對角色的服飾或配件設計作適量策略性修改或變卦（比如引用[二次創作的手法](../Page/二次創作.md "wikilink")），以方便扮演或表達另類訊息，認為這有利Cosplay的理性與多元發展。而另一部份人士比如「偶像擁躉派」則以忠於原著與角色的精神，不鼓勵作此類跟原著呈示有任何修改的角色扮演行為（不論策略性修改或變卦如何），否則將造成他們作出「外觀造形不正確」的論點，而不能滿足他們觀點中一項Cosplay應到位的所有條件。

部份人士認為獨創性的「名份」與「情節」是作為角色以Cosplay形式演繹時能達至生動入勝和易觀易辨的條件，因此他們會對純以社會角色（含崗位之獨特著裝與打扮）、動物外觀與脾性為藍本之Cosplay行為的Cosplay名銜有所保留。

另外Cosplay界亦習慣將動漫角色以美型化的方式呈現，特別是Cosplay界以女性較為活躍，因此體現在反串男性角色上更為明顯，然而原作並非都走美型畫風，因此呈現的效果有時會與一般人對原作角色的認知有所差異，不過這已成為Cosplay界甚至是動漫界的主流。另一些由官方製作的真人劇或音樂劇，亦會為了拉攏女性觀眾或提升其視覺效果而有類似的情形，甚至音樂劇有多由女性演員反串男性角色的案例（此情況亦也成為該次文化的主流）。

## 誤區

在[ACG相關活動中](../Page/ACG.md "wikilink")，時常會見到數項類似的盛裝文化現象同時出現，比如Cosplay扮裝文化、
[蘿莉塔](../Page/蘿莉塔.md "wikilink")（Lolita，日文中為Rorita）服裝服飾風格文化、[龐克](../Page/龐克.md "wikilink")（Punk）服裝服飾風格文化等。

Cosplay扮演者所扮演的目標是以歷史或圖像性文獻中的特定角色（或其變形原體）為主，這跟其他[盛裝現象的法則有所不同](../Page/盛裝.md "wikilink")。禮儀上，Cosplay行為不是借服裝服飾爭妍鬥麗的手段，亦不是任攝影師任意妄為的場合。倘民眾不了解箇中差異，則會易生混淆。\[5\]因此，如純粹穿著指定款式的特定服裝，比如[蘿莉塔](../Page/蘿莉塔.md "wikilink")（Lolita）、[龐克](../Page/龐克.md "wikilink")（Punk）、[女僕服裝](../Page/女僕.md "wikilink")、[校服](../Page/校服.md "wikilink")（如[水手服](../Page/水手服.md "wikilink")、[英倫校服等](../Page/英格兰校服.md "wikilink")）、[巫女服](../Page/巫女.md "wikilink")、[和服浴衣](../Page/和服.md "wikilink")、[SD娃娃服](../Page/SD娃娃.md "wikilink")、英式連衣裙等等，又或者單單穿著一些於圖像性文獻中出現過的服飾，卻沒有扮演某特定角色或複製角色參數到演員身上的意思，則不算是Cosplay。為比如嬰兒或動物穿上某角色的服裝，但如被穿著者卻對有關服裝、角色性格與行為等在腦海中不明所以（即沒有複製角色參數到演員身上的意思），就與Cosplay在技術上的「自力」條件不符，因此也不算是Cosplay。

## 副產品

隨著Cosplay的發展日趨完善，Cosplay自身亦衍生出不少“副產品”，如[女僕咖啡店](../Page/女僕咖啡店.md "wikilink")。

## 亞洲地區

Cosplay在[亞洲地區發展擴張與應用](../Page/亞洲.md "wikilink")，一般以[日本的模式為主導](../Page/日本.md "wikilink")，其中又以東亞文化圈為盛。亞洲地區因地緣關係，Cosplay文化通常較歐美為成熟和盛行。

### 日本

#### 發源

日本當代的Cosplay，起源自1970年代後半的[Comic
Market](../Page/Comic_Market.md "wikilink")（又稱Comiket、Comike或CM，日本最大的同人展會）活動中，參加者打扮成當時流行的動畫角色的模樣前來會場。而這行為雖然也被指為，是在仿效美國所舉辦的[日本科幻大會中](../Page/日本科幻大會.md "wikilink")，一部分參加者扮裝成《[星艦迷航記](../Page/星艦迷航記.md "wikilink")》等作品中登場人物前來參展的舉動，但詳細情況則依舊不明。

在這之後，Comic
Market以外的[同人誌販售會以及日本國內的](../Page/同人誌.md "wikilink")[日本科幻大會也相繼出現了仿效的人](../Page/日本科幻大會.md "wikilink")，日本的Cosplay文化也因此漸次拓展。

日本媒體初次刊載Cosplay的報導是在Rapport出版社的《》雜誌1980年8月號（創刊號），模仿當時席捲[原宿的](../Page/原宿.md "wikilink")「[竹筍族](../Page/竹之子族.md "wikilink")」（）而被稱之為「富野族」（），「富野」之名是來自[機動戰士鋼彈的](../Page/機動戰士鋼彈.md "wikilink")[富野由悠季監督](../Page/富野由悠季.md "wikilink")，在報導中刊登著富野穿著機動戰士鋼彈的扮裝跳著舞的片。

[Cosplayer_of_Keroro,_Sgt._Frog_20050624.jpg](https://zh.wikipedia.org/wiki/File:Cosplayer_of_Keroro,_Sgt._Frog_20050624.jpg "fig:Cosplayer_of_Keroro,_Sgt._Frog_20050624.jpg")的Cosplayer。背景裡可見Cosplay的活動情形。\]\]

#### 20世纪末的發展

在日本，Cosplay這個用語廣為人知是在1990年代中期左右。這個時期由於動畫《[新世紀福音戰士](../Page/新世紀福音戰士.md "wikilink")》的大受好評等原因，至今為止不大有機會被刊載的[次文化成了眾人注目的焦點](../Page/次文化.md "wikilink")，而Cosplay這樣的用語與行為也跟著普及了。

從1990年代左右開始，商業資本亦開始著眼在Cosplay上。一向只有私人製作的Cosplay用服飾，也出現了將其視為成衣來製作及販賣的業者。相對於仰賴製作者的手藝而左右了服飾水準的私人製品，業界製品由於能夠維持一定程度以上的品質而深獲青睞。

即使是在活動時，Cosplay向來都只是在Comic Market為首的同人誌販售會，或是Wonder
Festival、日本科幻大會等活動中，順便被從事的行為，然而單獨只有Cosplay的活動也開始舉辦。基本形的以下兩種，或是這兩種的混合形活動開始被舉辦：

  - 各隨己意進行角色扮演（Cosplay），配合著舞曲或動畫歌曲起舞的舞會形式。
  - 集合於會場，促進角色扮演同好者（Cosplayer）們的交流，互相拍照，讓業餘攝影師（俗稱拍照狂）拍照等的活動形式。

活動會場以及各種展示會場雖然經常用來進行Cosplay活動，但是舞會形式的則是[迪斯可](../Page/迪斯可.md "wikilink")（Disco）或[俱樂部](../Page/俱樂部.md "wikilink")（Club）較常使用。另外，藉由[後樂園](../Page/後樂園.md "wikilink")[遊樂園](../Page/遊樂園.md "wikilink")（現在的[東京巨蛋City](../Page/東京巨蛋.md "wikilink")
Attractions遊樂場）率先舉辦[萬聖節扮裝活動的契機](../Page/萬聖節.md "wikilink")，各種遊樂園、[主題樂園也都跟著舉辦活動](../Page/主題樂園.md "wikilink")。而其他像是「[東京電玩展](../Page/東京電玩展.md "wikilink")」或是「Chara-Fes」之類，允許Cosplay的展示會等活動也開始舉辦。

然而因為風紀問題等，近來會場或主辦單位對於肌膚暴露的服飾或女裝有諸多禁止，此外Comic
Market等單位基於防犯、安全上的理由，禁止攜帶武器狀物品（模型刀槍等）或是棍棒等長形物體，諸如此類的小道具入場。至於攝影拍照方面，也為了防止像是偷拍以及對Cosplayer的[肖像權侵犯](../Page/肖像權.md "wikilink")（例如：未經許可逕向照片投稿雜誌的投稿、未經許可擅自在各種網頁的公開）之類的問題產生，除了經常藉由須在開場當天或者事前申請的形式，附帶了參加者登錄的義務之外，視主辦單位而定，也時常會對使用的相機、鏡頭、攝影器材設有限制。

此外，頻繁的被電視或雜誌報導刊載的現象，也可說從1990年代開始的。甚至到了動畫、遊戲雜誌也會收錄Cosplay相關內容的程度，而一般雜誌與新聞情報節目、綜藝節目等則變的也會收錄一些特集記事。更有甚者，連Cosplay的專門期刊、雜誌也都開始發行（2005年目前有《》（Inforest·英知出版）《[電擊Layers](../Page/電擊Layers.md "wikilink")》（Media
Works）等雜誌）。

#### 展現出擴張性的Cosplay

<File:Cosplay> Kurisumasu 2009, Costa
Rica.jpg|[VOCALOID的cosplayer](../Page/VOCALOID.md "wikilink")
<File:The> Cosplayers of Comiket 69.jpg|位于东京Comic Market的Cosplayer
<File:Cosplayer> of Madoka Kaname in Akihabara
20111225.jpg|扮演成《[魔法少女小圆](../Page/魔法少女小圆.md "wikilink")》的主角鹿目圆的Cosplayer
<File:Japan> Expo 2010 - Cosplay -
Dimanche.jpg|神奇女郎的Cosplayer，摄于2010年日本博览会

從1990年代末期以後，Cosplay作為商業/職業手段的應用開始擴張，更比其原始形態走得更前衛。此時開始有店員穿著Cosplay用服飾並提供服務的飲食店（角色扮演系飲食店/Cosplay系飲食店）以及風俗店等陸續登場，此外像是女演員穿著動畫角色服飾登場的[成人影帶也被大量販售](../Page/色情片.md "wikilink")。

從1990年代至2000年以後，由於[互聯網的普及](../Page/互聯網.md "wikilink")，Cosplayer們各自製作網頁，這更包含了[網路偶像的要素](../Page/網路名人.md "wikilink")。甚至出現了自行製作同人角色扮演[寫真集與](../Page/寫真集.md "wikilink")[CD-ROM寫真集](../Page/CD-ROM.md "wikilink")，在Comic
Market等活動直接販賣，或者透過[同人誌專門店發行的角色扮演者與攝影師](../Page/同人誌.md "wikilink")。

攝影的地點也不再限於各種活動會場，由Cosplayer或攝影師自行舉辦的形式，或者由活動籌設單位、模特兒事務所與角色扮演者訂立契約的形式，攝影大會也經常被舉辦。

另一方面，任用知名Cosplayer擔任「官方角色扮演者」/「官方Cosplayer」來作為動畫及遊戲宣傳人員的例子也很常見。由[世嘉公司首創的](../Page/世嘉.md "wikilink")「MMB（、、）」可說是其先驅。近年來，即使默默無名者也能藉由擔任官方Cosplayer，而朝知名Cosplayer的目標更進一步。

*註：MMB為世嘉旗下三名官方角色扮演者的合稱。*

此外，知名Cosplayer當中也有人加入藝能事務所，正式從事像是藝人、活動接待員、模特兒、配音員等活動，如、（）、、、（）、等人）。另一方面偶像藝人、配音員等在活動展場與促銷活動中，或在印刷品上從事Cosplay的例子也不少（如[深田恭子](../Page/深田恭子.md "wikilink")、[小倉優子](../Page/小倉優子.md "wikilink")、[中川翔子](../Page/中川翔子.md "wikilink")、[加藤夏希](../Page/加藤夏希.md "wikilink")、[桃井晴子](../Page/桃井晴子.md "wikilink")、[栗林美奈實](../Page/栗林美奈實.md "wikilink")、[井上麻里奈等人](../Page/井上麻里奈.md "wikilink")）。

2003年開始，由[愛知電視台主辦](../Page/愛知電視台.md "wikilink")，以[名古屋市內為會場](../Page/名古屋市.md "wikilink")，舉辦了招攬世界各地著名Cosplayer前來日本的「[世界角色扮演高峰會](../Page/世界角色扮演高峰會.md "wikilink")（世界コスプレサミット）」。角色扮演高峰會2005年不只在名古屋市內，也訂定將在[愛知萬國博覽會上舉行](../Page/2005年日本世界博覽會.md "wikilink")，在網路關聯事務上得到了業者[活力門](../Page/活力門.md "wikilink")（Livedoor，其旗下擁有Cosplay
Community Site「Cure」網站）的協助，而擴大了規模。

### 台湾

#### 1990年代

[台灣早期Cosplay可追溯至](../Page/台灣.md "wikilink")1990年左右，當時參與人數不多，多數附屬於同人會場之下，大部分是由[同人誌作家](../Page/同人誌.md "wikilink")、社團所扮演的，很少有專門進行Cosplay的玩家。因坊間欠缺相關資料，時間也無從追溯。此時期的Cosplay活動算是極少數人士、社團私下進行的。少數[同人作家開始自己製作相關的服飾](../Page/同人.md "wikilink")、道具，但專門Cosplay的Coser還是少數，當時的Cosplay環境可說是非常的艱辛，也因為經歷過此困難期，讓Coser們都擁有自己製作服飾與道具的能力，觀念與思想也都是從這段時期開始建立，此階段大部是學習日本佔為多數，但[網路資訊剛起步](../Page/全球資訊網.md "wikilink")，資訊還不是很流通，所以Cosplay原創性與品質都還有很大的進步空間。

之後台灣Cosplay環境開始成長，進行Cosplay的人數全台大概約幾百人左右，在1994年開始有私下交流以外的正式主辦單位誕生，
1997年兩年一度的同人活動Comic World
TW誕生，開始了[同人販售會的歷史](../Page/同人誌即賣會.md "wikilink")，Cosplay此時仍附屬於同人誌之下，屬於少數族群。由[衛視中文台節目](../Page/衛視中文台.md "wikilink")《[電玩大觀園](../Page/電玩大觀園.md "wikilink")》報導開始。Cosplay人數快速增加，也開始獨立。各種問題也開始衍生。會場規定及自我約束規定成立。這段時間也是主辦單位互相競爭的開端。週邊產業開始發展，幫忙同好製作Cosplay服與道具的工作室開張。
[Sailor_Moon_cosplayers_at_FanimeCon_2010-05-30_5.JPG](https://zh.wikipedia.org/wiki/File:Sailor_Moon_cosplayers_at_FanimeCon_2010-05-30_5.JPG "fig:Sailor_Moon_cosplayers_at_FanimeCon_2010-05-30_5.JPG")的Cosplay\]\]

#### 2000年代

參與Cosplay人數激增。南部活動社團減少、外拍越來越風行、參與Cosplay者之年齡層越來越低。

[台灣漫畫國度同人會](../Page/台灣漫畫國度同人會.md "wikilink")（Taiwan
ComicKingdom）在2002年首度舉辦場次，部分人士排四個多小時才順利入場。[暑假場](../Page/暑假.md "wikilink")[PCHOME的比賽擠爆](../Page/PCHOME.md "wikilink")[Comic
World
TW台大場](../Page/Comic_World_TW.md "wikilink")。介紹Cosplay與同人誌的專業雜誌《DREAM
創夢
同人資訊情報誌》和《[COSmania](../Page/COSmania.md "wikilink")》分別在2005和2006年開始在台灣發行。《COSmania》第五期結束後原製作小組與「文藝復興」聯手發行Cosplay雜誌《[COSMORE](../Page/COSMORE.md "wikilink")》。[天空部落格](../Page/天空部落格.md "wikilink")、[無名部落格兩支帶有Cosplay召集力成份的](../Page/無名部落格.md "wikilink")[網誌](../Page/網誌.md "wikilink")（[部落格](../Page/部落格.md "wikilink")）崛起。[華視新聞雜誌介紹Cosplay](../Page/華視.md "wikilink")。[台灣論壇Cosplay版正式運作](../Page/台灣論壇.md "wikilink")。《[Beyou](../Page/Beyou.md "wikilink")》雜誌發行，採用一般流行雜誌風格製作。《[變身天使寇詩兒](../Page/變身天使寇詩兒.md "wikilink")》新書發行。緯來綜合台的節目「我的馬吉情人」，利用Cosplay作為節目效果。有Cosplay同好參加[李明依的](../Page/李明依.md "wikilink")《[天天啃蘋果](../Page/天天啃蘋果.md "wikilink")》節目。華視播出《[至尊玻璃鞋](../Page/至尊玻璃鞋.md "wikilink")》[偶像劇](../Page/偶像劇.md "wikilink")，劇中有Cosplay情結。由於仍屬推廣中的小眾活動，不少在媒體中出現的不正確的描繪，都引發愛好者批評。

Cosplay觀念和規則開始受到挑戰。例如成衣[蘿莉與成衣](../Page/蘿莉.md "wikilink")[視覺的討論](../Page/視覺系.md "wikilink")、自創角色與半自創角色的討論。[巴哈姆特電玩資訊站版友的熱烈討論偶像化問題](../Page/巴哈姆特電玩資訊站.md "wikilink")。[龐克](../Page/龐克.md "wikilink")（Punk）服裝服飾文化與
[蘿莉塔](../Page/蘿莉塔.md "wikilink")（Lolita）服裝服飾文化的爭論。守舊派一辭出現，新派與舊派的觀念的爭執持續進行。台灣論壇、巴哈姆特電玩資訊站與蒼穹社群發表聯合聲明，針對Cosplay行為作規範。
[Getxo_cosplay_coroneles.jpg](https://zh.wikipedia.org/wiki/File:Getxo_cosplay_coroneles.jpg "fig:Getxo_cosplay_coroneles.jpg")的Cosplay|左\]\]

Cosplay的興起也引發社會議論，如[TVBS和](../Page/TVBS.md "wikilink")[東森兩家新聞媒體分別以](../Page/東森.md "wikilink")「春光乍現」和「春色無邊」字眼醜化[開拓動漫祭第](../Page/開拓動漫祭.md "wikilink")8屆的場面。台大心理系教授黃光國以當代流行動漫畫未能向人民灌輸任何道德價值為重點，抨擊進行Cosplay的行為就像是吸大麻、荼毒人民心靈\[6\]。

#### 2010年代

[链接=<https://zh.wikipedia.org/wiki/File:Cosplayer_of_Rem,_Re-Zero_20161210a.jpg>](https://zh.wikipedia.org/wiki/File:Cosplayer_of_Rem,_Re-Zero_20161210a.jpg "fig:链接=https://zh.wikipedia.org/wiki/File:Cosplayer_of_Rem,_Re-Zero_20161210a.jpg")
Cosplay漸漸進入主流。參與者增加也表示較容易發生爭端，如2011年於台大體育館舉辦的同人活動發生Cosplay玩家使用色彩塗料塗抹會場地面並沒有清理、由台灣競舞娛樂公司舉辦的[英雄聯盟Cosplay競賽結果遭質疑背離Cosplay精神](../Page/英雄聯盟.md "wikilink")、生存遊戲玩家以氣槍蓄意射擊Cosplay玩家。

[台北國際書展動漫館於](../Page/台北國際書展.md "wikilink")2013年後，因台北展演二館的拆遷而取消，Cosplay競賽一度在信義商圈成為絕響，但[台北國際電玩展在](../Page/台北國際電玩展.md "wikilink")2014年起開辦Cosplay競賽，使得[台北世界貿易中心商圈逢展覽時的Cosplay競賽舉辦紀錄並未中斷](../Page/台北世界貿易中心.md "wikilink")。

除動漫圈以外，和沛科技總經理[翟本喬以裝扮成](../Page/翟本喬.md "wikilink")[犬夜叉人物的形式](../Page/犬夜叉.md "wikilink")，到知名媒體業者總部前發雞排，因長相神似香港明星[鄭伊健而引起網友熱議](../Page/鄭伊健.md "wikilink")。

### 香港

#### 歷史

[香港的Cosplay最初出現於](../Page/香港.md "wikilink")1993年[沙田藝墟中](../Page/沙田藝墟.md "wikilink")。香港同人誌團體[四百尺租用檔攤販賣其團員的漫畫](../Page/四百尺.md "wikilink")，所有男團員則打扮成《[銀河英雄傳說](../Page/銀河英雄傳說.md "wikilink")》的人物以及鮮紅的特撮英雄服，收引人注意之效。

1994年1月23日下午的[沙田藝墟中](../Page/沙田藝墟.md "wikilink")，[四百尺繼續Cosplay演出](../Page/四百尺.md "wikilink")，並帶領新生的小組U.R.V.Z以日本巫女(日本神道教陰陽師﹕俗稱巫女)打扮出現，而特撮英雄亦加了一位死對頭。1995年特撮英雄一分為三，成了紅、藍、黃戰士。其後從1996年起團員淡出Cosplay活動，然而後繼有人，薪火相傳不斷。1996年至1997年，一群同人誌組織包括「草民小堅」、「漫樂館」、「UVRZ」和「Comicbabie's」在[旺角](../Page/旺角.md "wikilink")[麥花臣室內場館主辦了](../Page/麥花臣室內場館.md "wikilink")[同人祭](../Page/同人祭.md "wikilink")，「[火狗工房](../Page/火狗工房.md "wikilink")」在1997年也加入負責主辦。

自1997年7月30日至8月3日[香港漫畫協會主辦](../Page/香港漫畫協會.md "wikilink")「[漫人墟](../Page/漫人墟.md "wikilink")」後，香港的同人誌及Cosplay愛好者便每年多了一個活動的機會，參與「漫人墟」的人數每年上升。後來，由1998年開始舉辦的漫人墟及由1999年開始舉辦的[Comic
World
HK先後面世](../Page/Comic_World_HK.md "wikilink")，逐漸催生了更多商業同人動漫與Cosplay活動。

1999年後，Cosplay文化開始受到媒體注意，在同一時期也開始與同人活動慢慢分道揚鑣，呈現獨立發展的傾向。同年，香港各間大學也開始舉辦Cosplay活動。在香港境內每年關於Cosplay的大型活動有十多個之多，而參與的Cosplayer則多達數百甚至更多。
[Wikipe-tan_full_length.svg](https://zh.wikipedia.org/wiki/File:Wikipe-tan_full_length.svg "fig:Wikipe-tan_full_length.svg")，由[日本](../Page/日本.md "wikilink")[維基人Kasuga所繪](../Page/維基人.md "wikilink")，是[英文維基百科的特色圖片之一](../Page/英文維基百科.md "wikilink")。\]\]
同一時間，香港的Cosplay方向也開始呈現一些脫離原始化現象，除了日本的漫畫人物、偶像歌星、電影及電視劇角色外，本地角色也成為扮演的對象。

除了大家熟悉的動畫、漫畫作品外，近年在香港出現的Cosplay角色更層出不窮，出現了不少的[擬人化角色](../Page/擬人化.md "wikilink")。
在Cosplay的範疇分類和手法上主要以品牌服務設施的[萌擬人化角色實體化Cosplay為主](../Page/萌擬人化.md "wikilink")，在當時的日本和美國已有一定的發展並被歸納為一種獨立的範疇分類和手法，甚至有以相關名義販售有關服飾：
\[7\]\[8\]。但在當時而言，在香港境內則屬於比較新鮮的範疇分類和手法（或是一直以Cosplay以外的名義發展著），謹香港境內有關的實踐歷史缺乏前車可鑑、甚至近乎空白。

此外，由於傳媒於大型Cosplay相關活動採訪漸多，以及藉由互聯網方便Cosplay資訊傳播，參與Cosplay的人數因此不斷增加，而Cosplay文化也因此普及起來。但一些規格較謹守的Cosplay參與者則認為，部份新進的Cosplay參與者的行演標準與表現良莠不齊，會影響到Cosplay的整體整齊性和大眾評價，因此對其出口抨擊；甚至有人產生一些帶[偏見性的空泛詞語](../Page/偏見.md "wikilink")（如「偽Cos」──即「偽Cosplay」（指裝扮者外形與原著呈示角色外形有所偏差）、「衣架人」（指空有外表，不了解甚至不認識他所扮演的角色的行演者）。目前，有一部份規格較謹守的Cosplay參與者正在竭力糾正此歪風，同時推廣Cosplay活動的攝影禮儀。

#### 活動

##### 大型活動

<table>
<tbody>
<tr class="odd">
<td><p><strong>首度舉辦</strong></p></td>
<td><p><strong>活動名稱</strong></p></td>
<td><p><strong>舉辦週期</strong></p></td>
<td><p><strong>主辦單位</strong></p></td>
<td><p><strong>舉辦地點</strong></p></td>
<td><p><strong>最近舉辦</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td><p><a href="../Page/城大秋祭.md" title="wikilink">城大秋祭</a></p></td>
<td><p>一年一度</p></td>
<td><p>香港城市大學學生會動漫畫同人誌</p></td>
<td><p><a href="../Page/香港城市大學.md" title="wikilink">香港城市大學校園</a></p></td>
<td><p>2012年10月1日至10月2日</p></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p><a href="../Page/科大之約.md" title="wikilink">科大之約</a><br />
Dating at UST</p></td>
<td><p>一年一度</p></td>
<td><p>香港科技大學學生會動畫與漫畫學會</p></td>
<td><p><a href="../Page/香港科技大學.md" title="wikilink">香港科技大學校園</a></p></td>
<td><p>2010年7月3日至7月4日</p></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><a href="../Page/藝墟.md" title="wikilink">藝墟</a></p></td>
<td><p>每年一月</p></td>
<td><p>香港理工大學學生會動畫及漫畫學會</p></td>
<td><p><a href="../Page/香港理工大學.md" title="wikilink">香港理工大學校園</a></p></td>
<td><p>2012年1月26日</p></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td><p><a href="../Page/角色扮演大會.md" title="wikilink">角色扮演大會</a><br />
HKU Cosplay Party</p></td>
<td><p>每年11月</p></td>
<td><p>香港大學學生會動漫聯盟</p></td>
<td><p><a href="../Page/香港大學.md" title="wikilink">香港大學校園</a></p></td>
<td><p>2012年11月18日</p></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p><a href="../Page/未完之約.md" title="wikilink">未完之約</a></p></td>
<td><p>一年一度</p></td>
<td><p>香港中文大學動漫畫研究社</p></td>
<td><p><a href="../Page/香港中文大學.md" title="wikilink">香港中文大學崇基學院</a></p></td>
<td><p>2011年6月25日</p></td>
</tr>
<tr class="even">
<td><p>2004年</p></td>
<td><p><a href="../Page/冬之宴會.md" title="wikilink">冬之宴會</a></p></td>
<td><p>已停辦</p></td>
<td><p>香港專業教育學院(觀塘)動漫學會</p></td>
<td><p><a href="../Page/香港專業教育學院觀塘分校.md" title="wikilink">香港專業教育學院(觀塘)禮堂</a></p></td>
<td><p>2004年11月4日</p></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p><a href="../Page/螢之祭.md" title="wikilink">螢之祭</a><br />
Firefly</p></td>
<td><p>已復辦</p></td>
<td><p>螢之祭籌備委員會</p></td>
<td><p><a href="../Page/香港專業教育學院觀塘分校.md" title="wikilink">香港專業教育學院(觀塘)</a></p></td>
<td><p>2011年9月17日至9月18日</p></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><a href="../Page/初之夏.md" title="wikilink">初之夏</a></p></td>
<td><p>已停辦</p></td>
<td><p>職業訓練局工商資訊學院動漫畫學會<br />
協辦：香港專業教育學院(黃克競)ACG學會</p></td>
<td><p><a href="../Page/職業訓練局工商資訊學院.md" title="wikilink">職業訓練局工商資訊學院平台</a></p></td>
<td><p>2006年6月24日至6月25日</p></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p>不詳</p></td>
<td><p>已停辦</p></td>
<td><p>香港專業教育學院(柴灣)洛漫聯盟</p></td>
<td><p><a href="../Page/香港專業教育學院柴灣分校.md" title="wikilink">香港專業教育學院(柴灣)</a></p></td>
<td><p>2007年6月23日至6月24日</p></td>
</tr>
<tr class="even">
<td><p>2008年</p></td>
<td><p>不詳</p></td>
<td><p>已停辦</p></td>
<td><p>香港專業教育學院(沙田)現代動漫文化研究學會</p></td>
<td><p><a href="../Page/香港專業教育學院沙田分校.md" title="wikilink">香港專業教育學院(沙田)</a></p></td>
<td><p>2008年4月29日、4月30日及5月2日</p></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p><a href="../Page/仲夏祭.md" title="wikilink">仲夏祭</a></p></td>
<td><p>已停辦</p></td>
<td><p>香港專業教育學院(屯門)學生會<br />
協辦：香港專業教育學院(屯門)系會及ACGN學會</p></td>
<td><p><a href="../Page/香港專業教育學院屯門分校.md" title="wikilink">香港專業教育學院(屯門)</a></p></td>
<td><p>2009年7月25日</p></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p><a href="../Page/動漫☆浸大.md" title="wikilink">動漫☆浸大</a></p></td>
<td><p>每年5月</p></td>
<td><p>香港浸會大學學生會動漫藝術研究學會</p></td>
<td><p><a href="../Page/香港浸會大學.md" title="wikilink">香港浸會大學善衡校園</a></p></td>
<td><p>2012年5月27日</p></td>
</tr>
</tbody>
</table>

##### 舉辦者質量變動

在2007年2月25日舉行的[Comic World
HK第](../Page/Comic_World_HK.md "wikilink")23屆由於場地安排和設施有弊病，引起部份參與者發出不滿意見，或是身體不適。

在2007年7月27日至7月31日舉行的[香港動漫節第](../Page/香港動漫節.md "wikilink")9屆，主辦商一度收窄Cosplay規定，例如禁止[制服服飾的Cosplay和攜帶尖銳物品入場](../Page/制服.md "wikilink")，一度引起計劃與會角色扮演者之意慾、士氣受打擊。到後來事件問清，他們心中的恐懼才得以消除。

在2007年8月26日舉行的Comic World
HK第24屆由於入場機制問題（售票速度較慢，而且售票口很少），發生特長排隊人潮，一度令市民汗流浹背地排隊長達四小時。

##### 私人摄影活动

所有在非正式活動日子期間在各會場附近Cosplay之私人Cosplay活動，通稱為「私影」，一般是以私人形式邀約少至數位、多至十數位朋友或同好（多數是演繹同一作品內諸等角色之Cosplayer們）一塊兒到某個地方Cosplay，聚會行動主要以拍攝照片、影片為主。

近年，隨著同好們對Cosplay的品味和視點不斷提高，私影的場景要求也越來越高，甚至會為了配合自己的角色而四處到不同地方去進行拍攝等等。
[Wikipe-tan_(Cosplay).jpg](https://zh.wikipedia.org/wiki/File:Wikipe-tan_\(Cosplay\).jpg "fig:Wikipe-tan_(Cosplay).jpg")的Cosplayer\]\]

#### 舞臺劇應用

部份人士認為某程度上舞臺劇上一些角色扮演行為也可以Cosplay稱之。例如由香港[劇場組合聯合藝術總監](../Page/劇場組合.md "wikilink")[詹瑞文推出的](../Page/詹瑞文.md "wikilink")[獨腳戲](../Page/獨腳戲.md "wikilink")[舞臺劇](../Page/舞臺劇.md "wikilink")《[男人之虎](../Page/男人之虎.md "wikilink")》，由詹瑞文一人分飾30多角。其中在2007年5月第7度的公演「伸凸手版」，以[香港特別行政區特首選舉為題](../Page/2007年香港特别行政区行政长官选举.md "wikilink")，加入「煲呔曾」及「袋巾梁」兩位角色，分別借代兩位候選人[曾蔭權和](../Page/曾蔭權.md "wikilink")[梁家傑](../Page/梁家傑.md "wikilink")，並由詹瑞文一人同時扮演兩位角色；到了2008年6月第8度公演「超級防火版」，以[北京奧運為題](../Page/2008年夏季奧林匹克運動會.md "wikilink")，由詹瑞文扮演「煲呔曾」，借代當時[香港特別行政區行政長官曾蔭權](../Page/香港特別行政區行政長官.md "wikilink")。而狹義觀者認為因著認為此乃「舞臺劇目的先決」而非純粹「興趣目的先決」，因此不贊同舞臺劇上的角色扮演行為為Cosplay。

### 澳门

[澳門由於地方較小](../Page/澳門.md "wikilink")，人口較少，澳門本土的Cosplay文化啟動較其他地方遲，發展總體而言不算十分高調，澳門境內的Cosplayer們也相對較團結與和諧。但由於澳門跟香港與中國大陸的溝通日益頻繁，因此在澳門境內舉辦的Cosplay相關活動也陸續吸引到不少來自澳門、香港、中國大陸的Cosplayer和Cosplay愛好者的青睞。

澳門境內第一個含有當代Cosplay文化成份的Cosplay活動，是在1999年12月於澳門[荷蘭園廣場舉行的](../Page/荷蘭園.md "wikilink")「澳門回歸漫畫繽紛SHOW」活動。當時此活動已設有Cosplay
[Catwalk](../Page/Catwalk.md "wikilink")、才藝表演等環節，帶領日後澳門境內日後的Cosplay活動於其他活動中出現或獨立出現。到了2006年6月，由[社會工作局與民間機構合辦](../Page/社會工作局_\(澳門\).md "wikilink")[國際禁毒日系列活動中的](../Page/國際禁毒日.md "wikilink")「禁毒巡遊響全城之電單車創意比賽」環節，就有一群參賽者以Cosplay姿態參賽，更獲得當場「最具代表性大獎」獎項。\[9\]\[10\]

而在2008年4月27日由[澳門大學學生會動漫研究會和](../Page/澳門大學.md "wikilink")[澳門科技大學學生會美術學會合辦的](../Page/澳門科技大學.md "wikilink")
「[Illusion
Feather](../Page/Illusion_Feather.md "wikilink")[同人祭](../Page/同人祭.md "wikilink")」（「[澳門青少年動漫文化祭](../Page/澳門青少年動漫文化祭.md "wikilink")」）活動，更蒙澳門特別行政區[教育暨青年局贊助](../Page/教育暨青年局.md "wikilink")\[11\]，這在一定程度也許會有促進澳門Cosplay文化健康發展的作用。

### 中国大陆

#### 廣東省廣州市

2003年，[YACA動漫協會創立](../Page/YACA.md "wikilink")，孕育中國華南地區第一個Cosplay舞台，每年均有在[廣州市舉行動漫和Cosplay聚會活動](../Page/廣州市.md "wikilink")。\[12\]

2007年，[香港動漫節的主辦商也踏足中國大陸](../Page/香港動漫節.md "wikilink")，在2007年10月3日至2007年10月7日於廣州市舉行首屆《[穗港澳动漫游戏展](../Page/穗港澳动漫游戏展.md "wikilink")》\[13\]。

#### 湖南省长沙市

2005年，[湖南省](../Page/湖南省.md "wikilink")[长沙市在](../Page/长沙市.md "wikilink")1月1日元旦举办了《[2005年长沙新春动漫游戏嘉年华](../Page/2005年长沙新春动漫游戏嘉年华.md "wikilink")》Cosplay活动\[14\]，这是湖南省长沙市第一次举行的大型Cosplay活动，在湖南省开创了先河。

#### 黑龙江省哈尔滨市

2011年，[黑龙江省](../Page/黑龙江省.md "wikilink")[哈尔滨市开始在每年](../Page/哈尔滨市.md "wikilink")1月举办中国冰雪动漫节\[15\]。这也是中国首次把冰雪与动漫文化相结合的Cosplay活动。
自2012年开始，中国冰雪动漫节举办地变为[冰雪大世界](../Page/冰雪大世界.md "wikilink")\[16\]\[17\]。

#### China Joy

2004年1月16日，中国国内首届[ChinaJoy](../Page/ChinaJoy.md "wikilink")（全称：首届中国国际数码互动娱乐产品及技术应用展览会）在[北京展览馆开幕](../Page/北京展览馆.md "wikilink")。这是中国第一个由官方主办的动漫游戏类产业的国际盛会。

ChinaJoy
Cosplay嘉年华是中国大陆最大规模的Cosplay联赛活动，有北京、上海、重庆、武汉、山东、长沙、昆明、南京、南宁、长春、沈阳、哈尔滨、广东等遍及全国的多个分赛区，每个分赛区经过舞台活动选拔若干代表队，参与现每年7月于上海新国际会展中心举办的ChinaJoy中国国际数码互动娱乐站W3展馆的Cosplay嘉年华全国总决赛。
此活动极大地促进了各地区间Cosplay爱好者的交流与共同发展。

ChinaJoy Cover
Coser是ChinaJoy每年于1到5月举行的网上平面Cosplay赛事，是中国大陆最大规模的Cosplay摄影作品比赛交流活动。除大陆及港澳台选手外有来自其他部分国家的Cosplay爱好者投稿参加。

除此以外还有配套活动如CosPlay内容的DV比赛等。

C3时尚动漫网·中国Cosplay首席社区是为ChinaJoy系列cosplay活动建立的参与者交流用在线社区，主站用以报导各地cosplay活动。

### 东南亚

Cosplay在2000年代初期藉著附在動畫、漫畫、遊戲或科幻相關的展覽中舉行的Cosplay節目，迅速踏入[菲律賓主流文化](../Page/菲律賓.md "wikilink")。多半，這些大會和事件被主辦，並且辯論發怒了法官的透視是否由一個cosplay事件的組織者影響。
並且，菲律賓cosplay規則俯視並且允許專家充分地被委任的服裝參加競爭。

[印尼首個Cosplay活動始於](../Page/印尼.md "wikilink")2000年初，當時「[印度尼西亞大學日本文化節](../Page/印度尼西亞大學.md "wikilink")」（Gelar
Jepang Universitas
Indonesia）新增Cosplay作為其中一項主要節目，開始吸引到一些自製服飾披身於會場展示的年青人注意。

印尼的角色扮演者們結伴成群，並以他們的[服飾進行](../Page/服飾.md "wikilink")[卡巴萊](../Page/卡巴萊.md "wikilink")（Cabaret）表演，而此類節目一般由大學或當地雜誌社所舉辦。在[雅加達](../Page/雅加達.md "wikilink")、[萬隆和](../Page/萬隆.md "wikilink")[茂物也有Cosplay節目頻繁舉行](../Page/茂物.md "wikilink")。

## 歐美地區

西方的角色扮裝（Costuming）起自迪士尼公司帶起的扮裝風潮，許多開發[英雄漫畫的製作公司也紛紛推出了真人扮演](../Page/英雄漫畫.md "wikilink")[超人](../Page/超人.md "wikilink")、[蜘蛛俠](../Page/蜘蛛俠.md "wikilink")、[夜魔俠](../Page/夜魔俠.md "wikilink")、[蝙蝠俠的扮演秀活動](../Page/蝙蝠俠.md "wikilink")，並在美國各地舉行了以愛好者群體為主的類似活動。基於各地固有文化的差異，在美國、加拿大和英國，又或是在亞洲地區的歐美裔或南亞人族群中，Cosplay的發展、選材和應用跟日本的一套有著一些差別──有關《[星艦奇航記](../Page/星艦奇航記.md "wikilink")》、《[星際大戰](../Page/星際大戰.md "wikilink")》、其他[科幻領域](../Page/科幻.md "wikilink")、以及歷史事件（比如[美國內戰](../Page/美國內戰.md "wikilink")）範疇的Cosplay，尤其是科幻故事範疇的Cosplay，遠比在日本多。相反，一些在歐美地區被視為令大眾不悅的角色（比如在一些漫畫和遊戲中含[納粹相關成份服飾的角色等](../Page/納粹.md "wikilink")）則可能會在日本的Cosplay活動中亮相。

西方世界在接近半世紀以來，服飾愛好者團隊一直跟服裝業者同步協調與廣泛發展；從首屆開始，動畫角色服飾現身會場漸多，Cosplay一詞逐漸被用作形容特定類型的角色扮演行為（尤其是來自日本的媒體中的角色）。此外，美國與日本在[第二次世界大戰後建立起來的特殊友好關係](../Page/第二次世界大戰.md "wikilink")，也使得兩國之間的服飾扮裝文化經常互有通融，再加上動漫、遊戲（[ACG](../Page/ACG.md "wikilink")）載體在兩國間高密度的發展，因此美日兩國的Cosplay越來越顯示出同一性、商業性的共同文化特徵\[18\]。日式的Cosplay傳入並影響西方的扮裝方式後，加速了Costuming被改稱為Cosplay；有些人仍將Cosplay一詞限制在扮裝成日本AGC角色的情況。

[Superman_and_Batman.jpg](https://zh.wikipedia.org/wiki/File:Superman_and_Batman.jpg "fig:Superman_and_Batman.jpg")和[蝙蝠侠的Cosplayer](../Page/蝙蝠侠.md "wikilink")\]\]

### 北美

[New_York_Comic_Con_2015_-_Yang_(21878006029).jpg](https://zh.wikipedia.org/wiki/File:New_York_Comic_Con_2015_-_Yang_\(21878006029\).jpg "fig:New_York_Comic_Con_2015_-_Yang_(21878006029).jpg")的Cosplayer\]\]
從2000年代開始，在美國和[加拿大的動畫展覽活動規模漸大](../Page/加拿大.md "wikilink")，也越來越流行。藉著大眾對一些從日本入口的流行動畫系列（比如《[火影忍者](../Page/火影忍者.md "wikilink")》、《[鋼之鍊金術師](../Page/鋼之鍊金術師.md "wikilink")》、《[死亡筆記](../Page/死亡筆記.md "wikilink")》、《[犬夜叉](../Page/犬夜叉.md "wikilink")》和《[BLEACH](../Page/BLEACH.md "wikilink")》等）更添注意，角色扮演者與動畫世界一直在主流流行文化中嶄露頭角，或最少以相對較小的規模亮相。而越來越多展覽赴會者扮演成他們喜愛的動畫角色，可見Cosplay文化與動畫文化對更多動畫展覽相繼設立有著足夠影響力以容納不斷增多的角色扮演者。

北美的展覽一般同時設有Cosplay和服飾的競賽。\[19\]Cosplay，或（Masquerade
ball），是一種身穿Cosplay服飾進行的短劇比賽。服飾競賽通常考驗參賽者所發揮的技能、設計、以及觀眾反應。競賽者在舞台上表演（配合觀眾拍掌與否視乎會場禮節而定），賽果乃事先評審或即場評審。日漸普及的展覽扮裝風氣引發了數個較新、以Cosplay為基礎的節目誕生，並結合傳統化粧舞會和會廳盛裝（Hall
costume）競賽一同舉行。而在同類的節目比如「Anime Dating Game」（動畫約會遊戲）和「Cosplay Human
Chess」（Cosplay真人戰棋），參與的角色扮演者更能在遊戲中相應地扮演出他們的角色。

此類比賽引發了許多Cosplay團隊衍生發展出來──他們往往在展覽前數個月便開始籌備登場細節。至於非比賽性的Cosplay則通常在[科幻片與](../Page/科幻片.md "wikilink")[幻想片的](../Page/幻想片.md "wikilink")[開幕夜可見](../Page/開幕夜.md "wikilink")，尤其以下所述一些已確認的情況：

  - 即使只是在一些小鎮，一些角色扮演者在長篇電影（比如《[星際大戰](../Page/星際大戰.md "wikilink")》、《[哈利波特](../Page/哈利波特.md "wikilink")》和《[魔戒](../Page/魔戒.md "wikilink")》開映數小時前就已駐足排隊。
  - 一些賣座的電影，即使只是[邪典電影](../Page/邪典電影.md "wikilink")（比如《[衝出寧靜號](../Page/衝出寧靜號.md "wikilink")》）也會設有開幕夜Cosplay。

而在[英國](../Page/英國.md "wikilink")、美國和其他各地，邪典電影《[洛基恐怖秀](../Page/洛基恐怖秀.md "wikilink")》（The
Rocky Horror Picture
Show）的擁躉都會身披該影片中的角色的服飾出席該影片的放映。此傳統始於1975年《洛基恐怖秀》上畫後。

[哥倫比亞廣播公司賣座作品](../Page/哥倫比亞廣播公司.md "wikilink")《[CSI犯罪現場：紐約](../Page/CSI犯罪現場：紐約.md "wikilink")》中，其中一位在該作品前傳裡《》被發現死亡的受害人正在Cosplay她的化身──受害人的兇手。

### 墨西哥

在[墨西哥](../Page/墨西哥.md "wikilink")，Cosplay常見於以電玩、科幻或動畫為主題的展覽中，角色扮演者們時常結伴成群合作去拍攝Cosplay照片。

而Cosplay在墨西哥憑著一些信譽卓著的表演者，有著健康的競爭情況。此現象也可見於其他[拉丁美洲國家](../Page/拉丁美洲.md "wikilink")，比如[巴西](../Page/巴西.md "wikilink")、[阿根廷和](../Page/阿根廷.md "wikilink")[智利](../Page/智利.md "wikilink")。

### 澳大利亚

在[澳大利亚](../Page/澳大利亚.md "wikilink")，这种趋势反映了不仅仅源于日本动漫的这一类美式服装。来源是提供了鲜明的人物形象和服装的任何东西，包括美国漫画、电脑游戏、科幻/奇幻小说、电影和电视秀、动画短片、戏剧、小说。通常来说，Cosplay并非完全展现了历史、游戏的准确性。总的来说，澳大利亚的Cosplay常常出现在有很多人的地方，例如首都和城市中心。因为这些庞大的人口支撑着边缘文化的多样性。尽管对于优秀的Cosplayer而言，在整个澳大利亚的大型赛事中进行表演是十分寻常的，但Cosplay展示不因此而局限于大型赛事。除了一些大型表演活动外，许多地方的小团体也会举行他们自己的Cosplay赛事。\[20\]

### 法国

在[法國](../Page/法國.md "wikilink")，Cosplay是一種在動漫展覽中普遍的節目，大型展覽如「[日本博覽會](../Page/日本博覽會.md "wikilink")」（Japan
Expo）甚至能吸引多達500位角色扮演者赴會Cosplay。而在展覽中法國的角色扮演者除了有多數從動畫、漫畫取靈感的外，更有裝扮成電影角色、著名歌手甚至電視劇演員──即使此等範疇並非與展覽主題有直接關係，但也可見Cosplay在法國發展的獨立性。

跟日本的角色扮演者不同的是，法國的角色扮演者使用幾乎純手製的服飾且通常只用一次，購買或再用服飾會被視為不公平競爭（在某些比賽中不能作出競爭）。

法国cosplayers主要集中在角色扮演比赛，这在几乎所有的漫画，科幻小说，幻想或角色扮演游戏公约。他们不是真正具有竞争力，他们更多的机会来展示服装，似乎不如可能不是（例如场景，灯光，音乐等）。代理和唱歌技能竞赛高度重视，一些团体重演战斗或音乐喜剧场面也。举例来说，能够做翻特技是在服装的一部分，日本世博会的传统，其中最有价值的数字竞赛。

## 註釋

## 参考文獻

## 相關纪录片

  - 戀妝物語（公共電視記錄觀點節目，2004年度觀點短片展短片，導演[江金霖](../Page/江金霖.md "wikilink")）
  - 蔻絲普蕾的魔咒（公共電視記錄觀點節目，2004年度觀點短片展短片，導演[溫知儀](../Page/溫知儀.md "wikilink")）

[Cosplay](../Category/Cosplay.md "wikilink")
[Category:特定用途或場合所穿服裝](../Category/特定用途或場合所穿服裝.md "wikilink")
[Category:日本文化](../Category/日本文化.md "wikilink")
[Category:次文化](../Category/次文化.md "wikilink")
[Category:同人](../Category/同人.md "wikilink")
[Category:表演藝術](../Category/表演藝術.md "wikilink")
[Category:日本動漫術語](../Category/日本動漫術語.md "wikilink")
[Category:電子遊戲術語](../Category/電子遊戲術語.md "wikilink")

1.  有別於cosplay，中文[角色扮演亦可指英語](../Page/角色扮演.md "wikilink")“Role-playing”。
2.
3.  <http://acg.gamer.com.tw/acgDetail.php?s=13623>
4.  杜明等編纂，《an'Cos》，第8、53頁，2008年8月24日，[中國](../Page/中國.md "wikilink")[香港](../Page/香港.md "wikilink")：Angelnights'
    Creative Group。
5.  杜明等編纂，《an'Cos》，第52頁，2008年8月24日，[中國](../Page/中國.md "wikilink")[香港](../Page/香港.md "wikilink")：Angelnights'
    Creative Group。
6.  <http://www.ettoday.com/2006/08/15/327-1978402.htm>
7.  金城等編纂，《漫友．Cosplay100》，第五輯「少年孵化之聲」，第136\~139頁，2008年4月，[中國](../Page/中國.md "wikilink")[內蒙古](../Page/內蒙古.md "wikilink")[呼和浩特](../Page/呼和浩特.md "wikilink")：漫畫世界雜誌社／內蒙古人民出版社。
8.  [鐵道車輛擬人化角色《京急娘》扮演用衣裝於網上販賣](http://mia.shop-pro.jp/?pid=3017058)
    、[鐵道車輛擬人化角色《Fastech娘》扮演用衣裝於網上販賣](http://www.rakuten.co.jp/miacos/858406/842575/803346/)

9.  [轉載2006年6月26日《澳門日報》「角色扮演在澳門的發展」](http://darthmic322.spaces.live.com/?_c11_BlogPart_BlogPart=blogview&_c=BlogPart&partqs=amonth%3D6%26ayear%3D2007)
10. [澳門藝術網：澳門漫畫會](http://www.macauart.net/Organization/ContentC.asp?id=7)

11. [Illusion Feather同人祭活動介紹](http://www.acgmac.org/if/read.php/2.htm)
12. [YACA網站──YACA動漫協會簡介](http://www.yaca.cn/about/index.htm)
13.
14.
15.
16.
17.
18.
19.
20.