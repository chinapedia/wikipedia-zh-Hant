**Cover
Flow**是一個可經由唱片封面來形象化地翻查一個數位音樂資料庫的[三維](../Page/三維.md "wikilink")[圖形用戶界面](../Page/圖形用戶界面.md "wikilink")。

## 歷史

最初由獨立的麥金塔開發者Jonathan del Strother創作，有些人將CoverFlow視作是2005與2006年間[Mac OS
X必備的應用程式](../Page/Mac_OS_X.md "wikilink")，因為它給予用戶直觀、感性以及有效率的方式來形象化地瀏覽某個唱片收藏。

可能由於獲到高度的採用與評價，CoverFlow被[蘋果公司收購並整合到其旗艦播放應用軟體](../Page/蘋果公司.md "wikilink")[iTunes中](../Page/iTunes.md "wikilink")。iTunes在版本7.0中結合了Cover
Flow技術，在2006年9月12日蘋果公司的活動上發佈，當天亦是第二代[iPod
Shuffle與](../Page/iPod_Shuffle.md "wikilink")[Apple
TV](../Page/Apple_TV.md "wikilink")（當時代號為"iTV"）首次亮相的日子。最後一個由Steel
Skies發佈的獨立版本RC1.2在當天的前一天2006年9月11日發佈，加入了對[Apple
Remote的支援](../Page/Apple_Remote.md "wikilink")，一個現己被蘋果公司整合到iTunes的功能；該軟體只限在當天自由分發，但現仍可自[MacUpdate](http://www.macupdate.com)網站上下載。2007年1月9日蘋果公司宣佈
[iPhone將結合Cover](../Page/iPhone.md "wikilink") Flow技術。Cover
Flow在2012年11月30日推出的iTunes 11中被移除掉。

[macOS Mojave](../Page/MacOS_Mojave.md "wikilink")
[Finder](../Page/Finder.md "wikilink") 中的 Cover Flow檢視已被圖庫檢視取代。

## 外部連結

  - [開發者網站（現為蘋果公司的法津措辭）](http://www.steelskies.com/article/48/on-coverflow)
  - [最初的概念（現為蘋果公司的法津措辭）](http://thetreehouseandthecave.blogspot.com/2004/12/dissatisfaction-sows-innovation.html)，[頁庫存檔](http://web.archive.org/web/20060322225313/http://thetreehouseandthecave.blogspot.com/2004/12/dissatisfaction-sows-innovation.html)
  - [CoverFlow圖示第一部份](https://web.archive.org/web/20070322175417/http://www.jasperhauser.nl/weblog/2006/05/coverflow-icon.html)
  - [CoverFlow
    圖示第二部份](https://web.archive.org/web/20061202021316/http://www.jasperhauser.nl/weblog/2006/05/coverflow-part-2.html)
  - [iTunes中的Cover
    Flow](https://web.archive.org/web/20070323141445/http://www.apple.com/itunes/jukebox/coverflow.html)
  - [CoverFlow
    RC1.2下載](https://web.archive.org/web/20070310041925/http://www.macupdate.com/info.php/id/19081)

[Category:苹果公司收购](../Category/苹果公司收购.md "wikilink") [Category:Mac
OS介面](../Category/Mac_OS介面.md "wikilink")
[Category:图形用户界面](../Category/图形用户界面.md "wikilink")