**Crossplay**是人扮成另一個性別的角色的[Cosplay](../Page/Cosplay.md "wikilink")。

「Crossplay」一詞是在日本[ACG](../Page/ACG.md "wikilink")
Cosplay文化進入美國之後，美國人注意到某些Cosplay與美國本土「cross
dressing」的結合而創造的新詞。在日本似乎並沒有將[跨性別的部分獨立討論](../Page/跨性別.md "wikilink")，仍然全部叫做Cosplay。無論如何，日本的[ACG作品中已經出現Crossplay角色](../Page/ACG.md "wikilink")。像是《[烏龍派出所](../Page/烏龍派出所.md "wikilink")》就有好幾位。

Crossplay與傳統[扮裝之間的關係並不親近](../Page/扮裝.md "wikilink")。Crossplay玩家與跨性別[異裝人士並沒有高度聯繫](../Page/異裝.md "wikilink")。但是有些技巧是相通的，例如將男性身材扮成女性的技巧。性質上則近似[戲曲的](../Page/戲曲.md "wikilink")[反串](../Page/反串.md "wikilink")，即同樣是為了扮演異性角色而裝扮成異性，並非在日常生活中以異性打扮示人。不論是在美國日本或台灣，男性扮演女性ACG角色常會引起注意。女性扮成男性角色則比較常見，不會受特別注意。這也許是因為角色扮演玩家女性多於男性的緣故，且男性普遍缺乏在女性身上常見的嬌柔媚態和細緻外表（個別情況例外），故此男性扮演女性ACG角色往往會被大眾賦予的印象，要麼洋相百出、穿崩連連、及至個人聲譽及利益受損，要麼破綻少得使大眾對其性別判斷都一時失語。

除此之外，Crossplay也是一種音樂技巧。

## 參看

  - [反串](../Page/反串.md "wikilink")
  - [偽娘](../Page/偽娘.md "wikilink")
  - [中性](../Page/中性_\(性別\).md "wikilink")

## 外部連結

  - [Lander
    ... 2001](https://web.archive.org/web/20060506160124/http://www.fansview.com/person/1230pers.htm)
    介紹一個美國player的文章。
  - [秋穂リシアのコスプレ中心ウェブサイト Akiho Licia's Cosplay Pics
    Shrine](http://park19.wakwak.com/~licia/index.html) 一個日本player的個人網站

[Category:跨性別](../Category/跨性別.md "wikilink")
[Category:戏剧](../Category/戏剧.md "wikilink")
[Category:人類形象](../Category/人類形象.md "wikilink")
[Category:特定用途或場合所穿服裝](../Category/特定用途或場合所穿服裝.md "wikilink")
[Category:次文化](../Category/次文化.md "wikilink")
[Category:Cosplay](../Category/Cosplay.md "wikilink")