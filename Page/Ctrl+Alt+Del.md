[Three-finger_salute.svg](https://zh.wikipedia.org/wiki/File:Three-finger_salute.svg "fig:Three-finger_salute.svg")上的（高亮显示）\]\]
**Ctrl+Alt+Del**是一個非常廣泛用於[IBM相容電腦上的](../Page/IBM相容電腦.md "wikilink")[鍵盤按鍵組合](../Page/鍵盤.md "wikilink")，其目的是為了立即終結電腦的異常狀態，包括[當機](../Page/當機.md "wikilink")。

代表按住、、這3個按鍵，按法為先按住和後（前兩者無先後順序）再按。

## 由來

這個按鍵組合的發明人為原[IBM工程師](../Page/IBM.md "wikilink")[大衛·布萊德利](../Page/大衛·布萊德利.md "wikilink")（David
Bradley）。當年IBM在設計[個人電腦時就已經常常發生電腦運作出現故障的問題](../Page/個人電腦.md "wikilink")，因此為了方便快速[重新啟動](../Page/重新啟動.md "wikilink")（reboot），布萊德利便負責設計及編寫該[程式碼](../Page/程式碼.md "wikilink")，據說當時他只花5分鐘。

隨著IBM相容電腦及[微軟](../Page/微軟.md "wikilink")[Windows作業系統的普及](../Page/Windows.md "wikilink")，這個按鍵組合幾乎是所有電腦使用者操作電腦時的必備常識。但由於通常只有電腦發生異常時才會使用，而電腦異常正是所有電腦使用者最大的惡夢，因此布萊德利曾表示：

而在某次微軟創辦人[比爾·蓋茨也出席的聚會中](../Page/比爾·蓋茨.md "wikilink")，布萊德利更是[幽默地說](../Page/幽默.md "wikilink")：\[1\]

## 作用

這個按鍵組合的作用隨微軟各代[作業系統的設計而有些許不同](../Page/作業系統.md "wikilink")，但其目的都一樣：“為了立即終結電腦的異常狀態。”例如，在[DOS裡按一次可以重新启动计算机](../Page/DOS.md "wikilink")；在[Windows
9x裡](../Page/Windows_9x.md "wikilink")，这个组合键可以调出“关闭程序”窗口，连续按两次可以重新启动计算机；在[Windows
2000和关闭欢迎界面的](../Page/Windows_2000.md "wikilink")[Windows
XP裡为调出](../Page/Windows_XP.md "wikilink")“Windows安全”窗口；而在Windows
XP裡未关闭欢迎画面时（默认）则是调出“任务管理器”窗口；到了Windows Vista以後的版本，这一组合会唤出“安全选项”界面。

## 影響

這一創舉導致当今很多人在電腦出問題的時候想到的第一組快速鍵不是Ctrl+Alt+Delete，就是後面改成Esc的那一組；所以許多廠商包括微軟都以此為由來搶商機，但也有一些業者逆其道而行，例如[Ubuntu作業系統等就沒有這組快速鍵的功能](../Page/Ubuntu.md "wikilink")，所以微軟愛用者如果在這類系統上遇到問題，就算猛打這組快速鍵也沒辦法求救。但還是有解決方法的：

1.  長按[ESC键](../Page/ESC键.md "wikilink")：這是该**強制結束**按鍵的本意，然而并不一定有效。

2.  按“沒有回應”視窗的關閉鍵後再按弹出的**中止程式**視窗（或类似窗口）的**中止程式**。

3.  長按電源鍵：這是最危險、後遺症最多的解決方式，因為它是強制命令主板关闭各硬件电源的意思，通常到不得已的時候才留到最後使用，若使用太多次很有可能傷害到整台電腦，引发软件乃至硬件故障（特别是硬盘）。

4.
## 參考文獻

[Category:电脑键](../Category/电脑键.md "wikilink") [Category:IBM
PC兼容机](../Category/IBM_PC兼容机.md "wikilink") [Category:Microsoft
Windows](../Category/Microsoft_Windows.md "wikilink")

1.