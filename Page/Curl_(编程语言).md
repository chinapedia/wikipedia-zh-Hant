**Curl语言**是一种被设计来编写[网络程序的](../Page/网络应用程序.md "wikilink")[编程语言](../Page/编程语言.md "wikilink")。由美國的麻省理工學院開發。它的目标是以一种单一的语言来取代[HTML](../Page/HTML.md "wikilink")，[Cascading
Style
Sheets](../Page/Cascading_Style_Sheets.md "wikilink")（[层叠样式表](../Page/层叠样式表.md "wikilink")）和[JavaScript](../Page/JavaScript.md "wikilink")，虽然它目前并未在世界范围内被广泛使用，但在日本有一定的普及。

Curl不像[HTML](../Page/HTML.md "wikilink")，它不是一种文本标记语言，但Curl语言既可以用于普通的文本显示，又可以用于实现大规模的客户端商业软件系统。Curl不利的一面是：需要向[客户端安装](../Page/客户端.md "wikilink")[运行环境](../Page/运行环境.md "wikilink")。

用Curl写的程序既可以运行于浏览器中，又可以像普通[客户端](../Page/客户端.md "wikilink")[程序那样独立于](../Page/程序.md "wikilink")[浏览器运行](../Page/浏览器.md "wikilink")，运行前需要安装SurgeRTE。"SurgeRTE"是一种与[JAVA类似的](../Page/JAVA.md "wikilink")[跨平台](../Page/跨平台.md "wikilink")[运行环境](../Page/运行环境.md "wikilink")（runtime
environment，RTE），其中包含[浏览器的](../Page/浏览器.md "wikilink")[插件](../Page/插件.md "wikilink")。它目前支持[微软视窗](../Page/微软视窗.md "wikilink")（Microsoft
Windows）[操作系统和](../Page/操作系统.md "wikilink")[Linux](../Page/Linux.md "wikilink")[操作系统](../Page/操作系统.md "wikilink")，据传[苹果机版将在不久的未来发布](../Page/苹果机.md "wikilink")。

Curl语言便于学习，编程效率高，是一种支持多重继承，范型等数据类型的面向对象编程语言。

## 外部链接

  - [Curl公司网站](http://www.curl.com/)

[Category:函数式编程语言](../Category/函数式编程语言.md "wikilink")
[Category:标记语言](../Category/标记语言.md "wikilink")
[Category:宣告式編程語言](../Category/宣告式編程語言.md "wikilink")