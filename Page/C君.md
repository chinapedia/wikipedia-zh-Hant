**鄭詩君**（，），藝名「**C君**」，香港[饒舌組合](../Page/饒舌.md "wikilink")「[農夫FAMA](../Page/農夫_\(樂隊\).md "wikilink")」成員及樂隊[The
Island成員](../Page/The_Island.md "wikilink")，前樂隊[System
Map的主音](../Page/System_Map.md "wikilink")。現為香港[無綫電視基本藝人合約藝員及節目主持](../Page/無綫電視.md "wikilink")。

## 個人簡介

鄭詩君在1996年畢業於[軒尼詩道官立小學下午校](../Page/軒尼詩道官立小學.md "wikilink")，後於2001年畢業於[香港鄧鏡波書院中五年級](../Page/香港鄧鏡波書院.md "wikilink")，曾為潮流雜誌《[Milk](../Page/Milk.md "wikilink")》作廣告撰稿員。其後於[香港專業教育學院進修多媒體設計](../Page/香港專業教育學院.md "wikilink")，曾於[香港電台](../Page/香港電台.md "wikilink")[Teenpower擔任客席主持](../Page/Teenpower.md "wikilink")。亦曾於[now.com.hk撰寫樂評](../Page/now.com.hk.md "wikilink")、為[東Touch](../Page/東Touch.md "wikilink")、[東方日報](../Page/東方日報.md "wikilink")、[100毛撰寫專欄](../Page/100毛.md "wikilink")，2009年更推出自己的首本散文集《大C想家》。

鄭詩君比較突出的特徵是無論樣貌與聲綫都與藝人[快必相似](../Page/快必.md "wikilink")。

鄭詩君亦醉心於風水命理、相學及掌學，亦是著名堪輿學家[麥玲玲之愛徒](../Page/麥玲玲.md "wikilink")。

另外，鄭詩君患有[地中海貧血症](../Page/地中海貧血症.md "wikilink")，要終生定期到醫院輸血\[1\]。

2012年，鄭詩君於台灣旅遊，遇上永康街芒果冰店無故結業而成為台灣新聞人物\[2\]。

2014年，鄭詩君以基本藝人合約身份加盟[無綫電視](../Page/無綫電視.md "wikilink")，並投入拍攝電視劇，第一部正式作品是《鬼同你OT》，與[陳豪](../Page/陳豪.md "wikilink")、[田蕊妮等人合作](../Page/田蕊妮.md "wikilink")。近年為無綫電視監製[徐正康的常用演員](../Page/徐正康.md "wikilink")。

2017年12月，鄭詩君接受訪問時表示童年故事相當悲慘，父母在他四、五歲時離異，自己亦患有地中海貧血，每個月都要到醫院輸血續命。幸得父親和繼母悉心照顧，與現任女友[黃天頤拍拖三年](../Page/:zh-yue:黃天頤.md "wikilink")，感情穩定，他坦言有想過結婚，甚至生小孩，他先向大家呼籲，無論有否這個病，生育前都應做身體檢查\[3\]。

## 曾參與音樂會

  - 音樂大亨音樂會 (2006)
  - Nokia 903 Music Talkie 音樂會 (2007)
  - 農夫舉高隻手演嘢會 (2009)
  - Down To Earth 演唱會 (2010)
  - 一尾講自爆 (2012)
  - 張達明X農夫棟篤笑 (2014)
  - 農夫等錢洗世界巡迴演唱會(旺角站) (2017年11月9-14日，共6場)
  - 農夫等錢洗世界巡迴演唱會(澳門站) (2018年5月5日，共1場)
  - 農夫等錢洗世界巡迴演唱會(廣州站) (2018年8月18日，共1場)
  - 農夫等錢洗世界巡迴演唱會(中山站)
    (原定2018年9月22日，受颱風[「山竹」影響](../Page/超強颱風山竹_\(2018年\).md "wikilink")，改為2018年10月20日，共1場)
  - 農夫18歲生日快樂演唱會 (2018年11月9-11日，16-18日，共6場)
  - 農夫等錢洗世界巡迴演唱會(東莞站) (2018年12月8日，共1場)

## 曾參與電影

  - 《[六樓后座2家屬謝禮](../Page/六樓后座2家屬謝禮.md "wikilink")》飾 C君(2008年4月24日上映)
  - 《[內衣少女](../Page/內衣少女.md "wikilink")》 (2008) 飾 CY
  - 《[死神傻了](../Page/死神傻了.md "wikilink")》 (2009)
  - 《[跳出去](../Page/跳出去.md "wikilink")》(客串) (2009)
  - 《[七十二家租客](../Page/七十二家租客.md "wikilink")》 (2010) 饰 旺角攒机王 with 陆永权
  - 《[飛砂風中轉](../Page/飛砂風中轉.md "wikilink")》(2010)
  - 《[人間喜劇](../Page/人間喜劇.md "wikilink")》(2010)
  - 《[我愛HK開心萬歲](../Page/我愛HK開心萬歲.md "wikilink")》 (2011)
  - 《[八星抱喜](../Page/八星抱喜.md "wikilink")》 (2012)
  - 《[在一起](../Page/在一起.md "wikilink")》（2013）
  - 《[重口味](../Page/重口味.md "wikilink")》 (2013)
  - 《[男人唔可以窮](../Page/男人唔可以窮.md "wikilink")》 (2014)
  - 《[ATM提款機](../Page/ATM提款機.md "wikilink")》 (2015)
  - 《[開飯啦！](../Page/開飯啦！.md "wikilink")》 (2016)
  - 《[PG戀愛指引](../Page/PG戀愛指引.md "wikilink")》 (2016)
  - 《[江湖悲劇](../Page/江湖悲劇.md "wikilink")》 (2016)
  - 《[原諒他77次](../Page/原諒他77次.md "wikilink")》 (2017)
  - 《[棟篤特工](../Page/棟篤特工.md "wikilink")》 (2018)

## 配音

  - 《[新喜劇之王](../Page/新喜劇之王.md "wikilink")》
  - 《[妖鈴鈴](../Page/妖鈴鈴.md "wikilink")》
  - 《[貓狗鬥多番](../Page/貓狗鬥多番.md "wikilink")》
  - 《[奇鸚嘉年華](../Page/奇鸚嘉年華.md "wikilink")》
  - 《[極速TURBO](../Page/極速TURBO.md "wikilink")》
  - 《[奇鸚嘉年華2](../Page/奇鸚嘉年華2.md "wikilink")》
  - 《[小企鵝大長征2](../Page/小企鵝大長征2.md "wikilink")》

## 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

|                                                 |                                            |           |           |
| ----------------------------------------------- | ------------------------------------------ | --------- | --------- |
| **首播**                                          | **劇名**                                     | **角色**    | **性質**    |
| 2009年                                           | [老婆大人II](../Page/老婆大人II.md "wikilink")     | B君        | 客串        |
| 2015年                                           | **[鬼同你OT](../Page/鬼同你OT.md "wikilink")**   | **賴亞武**   | **男配角**   |
| [樓奴](../Page/樓奴_\(電視劇\).md "wikilink")          | 區美男                                        | 男配角       |           |
| 2016年                                           | [愛情食物鏈](../Page/愛情食物鏈.md "wikilink")       | 邢燁源       |           |
| [潮流教主](../Page/潮流教主.md "wikilink")              | 黎志全                                        |           |           |
| [殭](../Page/殭.md "wikilink")                    | 餐廳情侶                                       | 特別客串      |           |
| **[來自喵喵星的妳](../Page/來自喵喵星的妳.md "wikilink")**    | **魏帆**                                     | **男配角**   |           |
| [幕後玩家](../Page/幕後玩家.md "wikilink")              | 施醫生                                        | 客串大結局     |           |
| [流氓皇帝](../Page/流氓皇帝_\(2016年電視劇\).md "wikilink") | 學生領袖                                       | 客串        |           |
| 2017年                                           | **[我瞞結婚了](../Page/我瞞結婚了.md "wikilink")**   | **戴堅庭**   | **第四男主角** |
| **[雜警奇兵](../Page/雜警奇兵.md "wikilink")**          | **車奕晨**                                    | **第二男主角** |           |
| [誇世代](../Page/誇世代.md "wikilink")                | 林尚明（Clever）                                | 特別演出      |           |
| 2019年                                           | [廉政行動2019](../Page/廉政行動2019.md "wikilink") | 權哥        | 客串        |
| 拍攝中                                             | [降魔的2.0](../Page/降魔的2.0.md "wikilink")     |           |           |

## 網絡劇（[big big channel](../Page/big_big_channel.md "wikilink")）

|        |                                            |                                |         |
| ------ | ------------------------------------------ | ------------------------------ | ------- |
| **首播** | **劇名**                                     | **角色**                         | **性質**  |
| 2017年  | [西遊之八戒与沙憎](../Page/西遊之八戒与沙憎.md "wikilink") | [八戒](../Page/八戒.md "wikilink") | 兩大男主角之一 |

## 參與/主持電視節目

  - [BOOM部落格](../Page/BOOM部落格.md "wikilink")

  - [大龍鳳茶樓](../Page/大龍鳳茶樓.md "wikilink")

  - [農夫小儀嬉](../Page/農夫小儀嬉.md "wikilink")

  - [FUN識基本法](../Page/FUN識基本法.md "wikilink")

  - [我的故事．我的歌](../Page/我的故事．我的歌.md "wikilink")

  - [撳钱](../Page/撳钱.md "wikilink")

  - [700萬人的數字](../Page/700萬人的數字.md "wikilink")

  - [猩猩補習班](../Page/猩猩補習班.md "wikilink")

  - [Think Big 天地](../Page/Think_Big_天地.md "wikilink")

  - [新春開運王](../Page/新春開運王.md "wikilink")

  - [Do姐有問題](../Page/Do姐有問題.md "wikilink")

  - [myTV SUPER呈獻：萬千星輝睇多D](../Page/myTV_SUPER.md "wikilink") \[4\]

  - [超強選擇1分鐘](../Page/超強選擇1分鐘.md "wikilink")

  - [Do姐去Shopping](../Page/Do姐去Shopping.md "wikilink")

  - [暖DD·食平D](../Page/暖DD·食平D.md "wikilink")

  - [男人食堂](../Page/男人食堂.md "wikilink")

  - [洗衣鋪群星事件簿](../Page/洗衣鋪群星事件簿.md "wikilink")

  - [我愛香港](../Page/我愛香港_\(電視節目\).md "wikilink")

  - [myTV SUPER 呈獻：萬千星輝放暑假](../Page/myTV_SUPER.md "wikilink")

  - [2016香港先生選舉](../Page/香港先生選舉.md "wikilink")

  - [2016香港小姐選舉](../Page/香港小姐選舉.md "wikilink")

  - [嘩鬼上學去之農夫篇](../Page/嘩鬼上學去之農夫篇.md "wikilink")

  - [星和無線電視大獎2016](../Page/星和無綫電視大獎2016.md "wikilink")

  - [TVB馬來西亞星光薈萃頒獎典禮2016](../Page/TVB_馬來西亞星光薈萃頒獎典禮2016.md "wikilink")

  - [嘩鬼上學去之農夫篇](../Page/嘩鬼上學去之農夫篇.md "wikilink")

  -
  - [萬千星輝頒獎典禮2016](../Page/萬千星輝頒獎典禮2016.md "wikilink")

  - [群星拱照big big channel](../Page/群星拱照big_big_channel.md "wikilink")

  - [Do姐再Shopping](../Page/Do姐再Shopping.md "wikilink")

  - [萬千星輝頒獎典禮2017](../Page/萬千星輝頒獎典禮2017.md "wikilink")

## 曾參與電台節目

  - [口水多過浪花](../Page/口水多過浪花.md "wikilink")（2012年8月13日至8月28日下午4時至6時）
  - [好出奇](../Page/好出奇.md "wikilink")（2013年4月22日起至2014年10月31日下午4時至6時）

## 曾拍攝廣告

  - 加州紅唱K廣告
  - KFC Zinger 五星巴辣雞腿包
  - [萬寧廣告](../Page/萬寧.md "wikilink") (2008)
  - [港鐵入](../Page/港鐵.md "wikilink"),深圳出(2009)
  - CRUNCH[朱古力](../Page/朱古力.md "wikilink") (2009)

## 曾代言品牌

  - 黑白淡奶 (2013)

## 曲詞編監作品

### 2010年

  - [農　夫](../Page/農夫_\(組合\).md "wikilink") - Rap Along Song（曲、編、監）

### 2011年

  - [Josie & the Uni Boys](../Page/何超儀.md "wikilink") - 天眼通（詞）
  - [官恩娜](../Page/官恩娜.md "wikilink") - 相對時空（詞）
  - [泳　兒](../Page/泳兒.md "wikilink") - 精心戀愛（詞）
  - [洪卓立](../Page/洪卓立.md "wikilink") - 完美口味（詞）
  - [陳文婷](../Page/陳文婷.md "wikilink") - 單程機票（詞）

### 2012年

  - [AOA](../Page/AOA_\(香港\).md "wikilink") - 放肆（詞）
  - AOA - 大不了（詞）
  - [方大同](../Page/方大同.md "wikilink") - I Want You Back（詞）
  - 方大同 - 麥恩莉（詞）
  - [石詠莉](../Page/石詠莉.md "wikilink") - 一個人的旅行（詞）
  - [林欣彤](../Page/林欣彤.md "wikilink") - 第二次初戀（詞）
  - [洪卓立](../Page/洪卓立.md "wikilink") - 想一個人（詞）
  - 洪卓立 - 某天再會（詞）
  - 洪卓立 - 梵高的耳朵（詞）
  - 洪卓立 - Last Dance（曲、詞）
  - [彭永琛](../Page/彭永琛.md "wikilink") - 天天樂天（詞）

### 2013年

  - [C AllStar](../Page/C_AllStar.md "wikilink") Feat.
    [許志安](../Page/許志安.md "wikilink") - 惺惺相惜（曲、詞）
  - [J.Arie](../Page/J.Arie.md "wikilink") - 拼命虛耗（曲、詞）
  - [Robynn & Kendy](../Page/Robynn_&_Kendy.md "wikilink") - LaLaLa（詞）
  - [官恩娜](../Page/官恩娜.md "wikilink") - So In Love（詞）
  - [洪卓立](../Page/洪卓立.md "wikilink") - 戀一生的意志（詞）
  - 洪卓立 - 無聲佔有（詞）
  - 洪卓立 - 對焦這一秒（詞）
  - 洪卓立 - 留下的房客（詞）
  - 洪卓立 - 緊張你（詞）
  - 洪卓立 - 時間差（詞）
  - 洪卓立 Feat. C君 - "How about you?"（曲、詞）
  - [張紋嘉](../Page/張紋嘉.md "wikilink") - 假裝愉快（詞）
  - [張惠雅](../Page/張惠雅.md "wikilink") - 再見彩虹（詞）
  - [張繼聰](../Page/張繼聰.md "wikilink") - 把自己掌握（詞）
  - [許志安](../Page/許志安.md "wikilink") - 你是你（曲、詞）
  - [許靖韻](../Page/許靖韻.md "wikilink") - 仍未心死（詞）
  - [陳柏宇](../Page/陳柏宇.md "wikilink") - 放空（詞）
  - [陳慧琳](../Page/陳慧琳.md "wikilink") - It's all about timing（詞）
  - [蔡卓妍](../Page/蔡卓妍.md "wikilink") - 錯過（詞）
  - [薛凱琪](../Page/薛凱琪.md "wikilink") - 復原（詞）
  - 薛凱琪 - 宮若梅（曲）
  - [蘇耀宗](../Page/蘇耀宗.md "wikilink")、C君、[朱薰](../Page/朱薰.md "wikilink") -
    最緊要出奇（曲、詞）

### 2014年

  - [方大同](../Page/方大同.md "wikilink") - 小方（詞）
  - 方大同 Feat. [葛仲珊](../Page/葛仲珊.md "wikilink") - 愛不來（詞）
  - [王梓軒](../Page/王梓軒.md "wikilink") - Run（詞）
  - 王梓軒 - 千色（詞）
  - [許靖韻](../Page/許靖韻.md "wikilink") - 不如告白（詞）

### 2015年

  - [Super Girls](../Page/Super_Girls.md "wikilink") - 蓓蕾（國）（詞）
  - [方大同](../Page/方大同.md "wikilink") - 聽（詞）
  - [陳柏宇](../Page/陳柏宇.md "wikilink") - 我瞞你們（詞）
  - [謝安琪](../Page/謝安琪.md "wikilink") - 香女人（詞）
  - [關心妍](../Page/關心妍.md "wikilink") - 關家姐（詞）

### 2016年

  - [A2A](../Page/A2A.md "wikilink") - You Ain't Comin' Back（詞）
  - [Bingo](../Page/Bingo_\(組合\).md "wikilink") - 好好愛（詞）
  - [ReVe](../Page/ReVe.md "wikilink") - What?（詞）
  - [方大同](../Page/方大同.md "wikilink") - 黑夜（詞）
  - 方大同 - 很不低調（詞）
  - 方大同 - 放不過自己（詞）
  - [溫　拿](../Page/溫拿樂隊.md "wikilink") - Friend過打Band（詞）
  - [鍾舒漫](../Page/鍾舒漫.md "wikilink")、[王君馨](../Page/王君馨.md "wikilink") -
    隱型（詞）

### 2017年

  - [Gin Lee](../Page/Gin_Lee.md "wikilink") Feat. C君 - 今朝有酒（詞）
  - [王詩安](../Page/王詩安.md "wikilink") - 徠卡味（詞）
  - [周柏豪](../Page/周柏豪.md "wikilink") - 上下線（詞）
  - [薛凱琪](../Page/薛凱琪.md "wikilink") - 做我的知己（詞）

### 2018年

  - [張可盈](../Page/張可盈.md "wikilink") - 記得你的名字（詞）
  - [薛家燕](../Page/薛家燕.md "wikilink") - Captain Nancy（詞）

## 音樂錄像

  - 2016年：[SIS樂印姊妹](../Page/SIS樂印姊妹.md "wikilink")〈新世界〉男主角

## 獎項

### 電視獎項

  - [萬千星輝頒獎典禮2016](../Page/萬千星輝頒獎典禮2016.md "wikilink") ─ 最佳節目主持《Do姐有問題》
  - [星和無綫電視大獎2016](../Page/星和無綫電視大獎2016.md "wikilink") ─
    我最愛TVB綜藝節目主持人《Do姐有問題》
  - [SINA 頒獎禮- 微博之星](../Page/SINA_頒獎禮-_微博之星.md "wikilink")2015 ─
    微博給力綜藝節目《Do姐去Shopping》
  - [萬千星輝頒獎典禮2015](../Page/萬千星輝頒獎典禮2015.md "wikilink") ─
    最佳綜藝及特備節目《Do姐去Shopping》

### 音樂獎項

  - [SINA 頒獎禮- 微博之星](../Page/SINA_頒獎禮-_微博之星.md "wikilink")2015─微博給力男組合
  - [2012年度叱咤樂壇流行榜頒獎典禮](../Page/2012年度叱咤樂壇流行榜頒獎典禮.md "wikilink") －
    叱咤樂壇組合銅獎
  - [新城勁爆頒獎禮2012](../Page/2012年度新城勁爆頒獎禮得獎名單.md "wikilink") - 新城勁爆組合
  - [新城勁爆頒獎禮2012](../Page/2012年度新城勁爆頒獎禮得獎名單.md "wikilink") - 新城勁爆歌曲 -
    《同伴》
  - 2011年[十大中文金曲頒獎典禮](../Page/第三十四屆十大中文金曲得獎名單.md "wikilink") －
    全年最佳進步獎（銅獎）
  - [2011年度新城勁爆頒獎禮](../Page/2011年度新城勁爆頒獎禮得獎名單.md "wikilink") - 新城勁爆歌曲 -
    《全民合拍》
  - [2011年度新城勁爆頒獎禮](../Page/2011年度新城勁爆頒獎禮得獎名單.md "wikilink") -
    新城勁爆我最欣賞組合
  - [2010年度叱咤樂壇流行榜頒獎典禮](../Page/2010年度叱咤樂壇流行榜頒獎典禮.md "wikilink") －
    叱咤樂壇組合金獎
  - [2010年度新城勁爆頒獎禮](../Page/2010年度新城勁爆頒獎禮得獎名單.md "wikilink") - 新城勁爆專輯 -
    《O'FAMA》
  - [2010年度新城勁爆頒獎禮](../Page/2010年度新城勁爆頒獎禮得獎名單.md "wikilink") -
    新城勁爆我最欣賞組合
  - [2009年度新城勁爆頒獎禮](../Page/2009年度新城勁爆頒獎禮得獎名單.md "wikilink") -
    新城勁爆我最欣賞組合
  - 2009年[十大勁歌金曲頒獎典禮](../Page/十大勁歌金曲頒獎典禮.md "wikilink") - 最受歡迎組合銅獎
  - 2009年[新城国语力颁奖礼](../Page/新城国语力颁奖礼.md "wikilink") －新城國語新勢力組合
  - 2009年[新城国语力颁奖礼](../Page/新城国语力颁奖礼.md "wikilink") －新城国语力合唱歌曲 －
    《话太普通》與[季欣霈](../Page/季欣霈.md "wikilink")
  - 2008年[十大中文金曲頒獎典禮](../Page/十大中文金曲頒獎典禮.md "wikilink") －[十大金曲獎 -
    《全民皆估》](../Page/第三十一屆十大中文金曲得獎名單.md "wikilink")
  - 2008年[十大勁歌金曲頒獎典禮](../Page/十大勁歌金曲頒獎典禮.md "wikilink")
    －[最受歡迎組合獎金獎](../Page/2008年度十大勁歌金曲得獎名單.md "wikilink")
  - 2008年[叱咤樂壇流行榜頒獎典禮](../Page/2008年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink")
    －叱咤樂壇我最喜愛的組合
  - 2008年[叱咤樂壇流行榜頒獎典禮](../Page/2008年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink") －第八位
    舉高隻手
  - 2008年[叱咤樂壇流行榜頒獎典禮](../Page/2008年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink")
    －叱咤樂壇組合金獎
  - [新城勁爆頒獎禮](../Page/新城勁爆頒獎禮.md "wikilink")2008 －新城勁爆我最欣賞組合
  - [YAHOO\!搜尋人氣大獎](../Page/YAHOO!搜尋人氣大獎.md "wikilink")2008 －本地音樂男子組合
  - [第七屆華語音樂傳媒大奖](../Page/第七屆華語音樂傳媒大奖.md "wikilink") － 最佳組合
  - [第七屆華語音樂傳媒大奖](../Page/第七屆華語音樂傳媒大奖.md "wikilink") － 最佳嘻哈藝人
  - [南方都市報](../Page/南方都市報.md "wikilink") - 第八屆華語音樂傳媒大奖2008 －最佳組合
  - [南方都市報](../Page/南方都市報.md "wikilink") - 第八屆華語音樂傳媒大奖2008 －最佳嘻哈藝人
  - 2007年[叱咤樂壇流行榜頒獎典禮](../Page/2007年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink") －
    叱咤樂壇組合金獎
  - 2007年[叱咤樂壇流行榜頒獎典禮](../Page/2007年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink") －
    叱咤樂壇我最喜愛的組合
  - [SINA Music樂壇民意指數頒獎禮](../Page/SINA_Music樂壇民意指數頒獎禮.md "wikilink")2007
    ─ 我最喜愛組合(香港)
  - [SINA Music樂壇民意指數頒獎禮](../Page/SINA_Music樂壇民意指數頒獎禮.md "wikilink")2007
    ─ SINA MUSIC新浪直播室 - 最高收視大獎

## 注釋

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部链接

  -
[Shi詩](../Category/鄭姓.md "wikilink")
[Category:香港男歌手](../Category/香港男歌手.md "wikilink")
[Category:左撇子](../Category/左撇子.md "wikilink")
[Category:香港男演员](../Category/香港男演员.md "wikilink")
[Category:無綫電視男藝員](../Category/無綫電視男藝員.md "wikilink")
[Category:粤语Hip-Hop](../Category/粤语Hip-Hop.md "wikilink")
[Category:軒尼詩道官立小學校友](../Category/軒尼詩道官立小學校友.md "wikilink")
[Category:香港鄧鏡波書院校友](../Category/香港鄧鏡波書院校友.md "wikilink")
[Category:香港專業教育學院校友](../Category/香港專業教育學院校友.md "wikilink")

1.
2.
3.  [【封面故事】C君生母拋夫棄子
    八歲替自己打針須每月輸血續命](http://bka.mpweekly.com/focus/20171216-94040)
4.