**C大調**是一個於C音開始的音樂的[大調](../Page/大調.md "wikilink")，組成的音有**C**、**D**、**E**、**F**、**G**、**A**、**B**及**C**。C大調是一個沒有[升號和](../Page/升號.md "wikilink")[降號的](../Page/降號.md "wikilink")[調](../Page/調.md "wikilink")。

<score>

` \relative c' {`
`    \key c \major  \time 15/1`
`    c1 d1 e1 f1 g1 a1 b1 c1 b1 a1 g1 f1 e1 d1 c1`
`  }`
` `</score>

它的關係小調是[A小調](../Page/A小調.md "wikilink")，同名小調是[C小調](../Page/C小調.md "wikilink")。C大調是[音樂中最普通的調號](../Page/音樂.md "wikilink")，[移調樂器在演奏本身樂器的調性都是以C大調作為它們的記譜](../Page/移調樂器.md "wikilink")。例如，一支[降B大調單簧管演奏的樂譜是](../Page/降B大調單簧管.md "wikilink")[降B大調](../Page/降B大調.md "wikilink")，記譜則是C大調。[鋼琴上的白鍵都是C大調的音](../Page/鋼琴.md "wikilink")。很多在[古典時代寫的樂曲都是以C大調為主調](../Page/古典時代.md "wikilink")，。[舒伯特有兩首交響樂屬於C大調](../Page/弗朗茨·舒伯特.md "wikilink")，第一首是"Little
C major"、第二首是"the great C major"。

[法國的作曲家像](../Page/法國.md "wikilink")[夏邦提耶和](../Page/夏邦提耶.md "wikilink")[拉摩都認為C大調是一個為快樂音樂而設的調](../Page/拉摩.md "wikilink")，但[白遼士在](../Page/路易-埃克多·白遼士.md "wikilink")1856年說C大調"嚴肅但沉悶"。[威廉士聽過](../Page/佛漢·威廉士.md "wikilink")[西貝柳斯的](../Page/讓·西貝柳斯.md "wikilink")7號交響曲後留下很深的印象，並表示只有西貝柳斯才能使到C大調這麼清新。最著名的現代C大調作品有[賴利的](../Page/特里·赖利.md "wikilink")*[In
C](../Page/In_C.md "wikilink")*。

## C大調的著名音樂作品

  - *[大火球](../Page/大火球.md "wikilink")* -
    [杰瑞·李·刘易斯](../Page/杰瑞·李·刘易斯.md "wikilink")
  - *[钢琴男人](../Page/钢琴男人.md "wikilink")* -
    [比利喬](../Page/比利喬.md "wikilink")
  - *[It's Not Unusual](../Page/It's_Not_Unusual.md "wikilink")* -
    [汤姆·琼斯](../Page/汤姆·琼斯.md "wikilink")
  - *[Satin Doll](../Page/Satin_Doll.md "wikilink")* - [jazz
    standard](../Page/jazz_standard.md "wikilink")
  - *[Drops of Jupiter](../Page/Drops_of_Jupiter.md "wikilink")* -
    [Train](../Page/Train_\(band\).md "wikilink")
  - *[第41號交響曲](../Page/第41号交响曲_\(莫扎特\).md "wikilink")* -
    [莫扎特](../Page/莫扎特.md "wikilink")
  - *[Like a Rolling Stone](../Page/Like_a_Rolling_Stone.md "wikilink")*
    - [鮑勃·迪倫](../Page/鮑勃·迪倫.md "wikilink")
  - *[Let It Be](../Page/Let_It_Be.md "wikilink")* -
    [披頭四](../Page/披頭四.md "wikilink")

[Category:調](../Category/調.md "wikilink")