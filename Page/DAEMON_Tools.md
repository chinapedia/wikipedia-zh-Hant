（即）是在[Windows系统上運行](../Page/Windows.md "wikilink")、用於製作或虛擬[CD](../Page/CD.md "wikilink")/[DVD](../Page/DVD.md "wikilink")/[Blu-ray光碟的](../Page/Blu-ray.md "wikilink")[軟體](../Page/軟體.md "wikilink")。其中，DAEMON
Tools Ultra是提供製作光碟映像、光碟燒錄、虛擬燒錄機及掛載映像等進階實用的功能。DAEMON Tools
Lite則是該系列最低階的產品，但對於個人使用是免費的，並提供使用者額外的付費功能。

## 特点

  - 支持含SafeDisc和SecuROM的加密光碟
  - 支持多种光碟映像格式
  - 簡單易用的操作介面

## 支持的映像类型

  - [ISO](../Page/ISO_9660.md "wikilink")（CDRWin或CDWizard生成之镜像
    ，部份壓縮軟體|檔案壓縮軟體如[IZArc亦有支援](../Page/IZArc.md "wikilink")）
  - ISZ（ISO壓縮檔案）
  - CUE/BIN（CDRWin／DiscDump／Blindread生成的映像檔格式）
  - [CCD](../Page/CCD.md "wikilink")（CloneCD生成的映像檔）
  - [CDI](../Page/CDI.md "wikilink")（DiscJuggler生成的映像檔格式）
  - MDS（[Alcohol 120%生成的映像檔格式](../Page/Alcohol_120%.md "wikilink")）
  - [NRG](../Page/NRG.md "wikilink")（[Nero生成的映像檔格式](../Page/Nero.md "wikilink")）
  - B5T/B6T/BWT（BlindWrite生成的映像檔格式）
  - MDX ([DAEMON Tools](../Page/:DAEMON_Tools.md "wikilink") 生成的映象檔格式)
  - VHD ([Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")
    通用虛擬硬碟映像格式)
  - VDI ([VirtualBox](../Page/VirtualBox.md "wikilink") 專用虛擬硬碟映像格式)
  - ZIP ([ZIP格式](../Page/ZIP格式.md "wikilink") 的壓縮檔)

## 支援作業系統

  - [Windows XP](../Page/Windows_XP.md "wikilink") (需要先安裝[.NET
    Framework](../Page/.NET_Framework.md "wikilink") 4或更高的版本)
  - [Windows Vista](../Page/Windows_Vista.md "wikilink")
  - [Windows 7](../Page/Windows_7.md "wikilink")
  - [Windows 8](../Page/Windows_8.md "wikilink")
  - [Windows 8.1](../Page/Windows_8.1.md "wikilink")
  - [Windows 10](../Page/Windows_10.md "wikilink")

## 關連項目

  - [Alcohol 120%](../Page/Alcohol_120%.md "wikilink")

## 外部連結

  - [DAEMON Tools Official site](http://www.daemon-tools.cc/home)
  - [DAEMON Tools Software
    Community](https://www.facebook.com/daemontools)
  - [DAEMON Tools
    Taiwan](https://www.facebook.com/pages/DAEMON-Tools-Taiwan/467711013385862)

[Category:共享软件](../Category/共享软件.md "wikilink")
[Category:免費軟體](../Category/免費軟體.md "wikilink")
[Category:虛擬光碟軟體](../Category/虛擬光碟軟體.md "wikilink")