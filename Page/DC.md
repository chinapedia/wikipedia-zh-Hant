**DC**或**D.C.**是多個詞匯的簡寫，可以指：

## 电器與相關產品

  - [直流電](../Page/直流電.md "wikilink")（Direct Current）
  - [数码照相机](../Page/数码照相机.md "wikilink")（Digital Camera）
  - [Dreamcast](../Page/Dreamcast.md "wikilink")，由[世嘉在](../Page/世嘉.md "wikilink")1998年推出的[家用遊戲機](../Page/家用遊戲機.md "wikilink")。
  - [DreamColor](../Page/DreamColor.md "wikilink")，梦工厂与惠普公司一起研发的一种液晶显示解决方案，使用RGB
    LED背光，可以达到十亿色的显示效果。

## 计算机用户词汇

  - DirectConnect，一种点对点的档案分享协议 。
      - DC++，一个DirectConnect软件。
  - Disconnect，“断线”的缩写。
  - Dot-com，指[.com或](../Page/.com.md "wikilink")[dot-com泡沫](../Page/互聯網泡沫.md "wikilink")。

## 计算机学

  - 十六進制數[220](../Page/220.md "wikilink")。
  - dc，一条类UNIX体统的指令，用于计算。
  - Device Context，设备上下文，[GDI的一种定义](../Page/图形设备接口.md "wikilink")。
  - [域控制器](../Page/域控制器.md "wikilink")（Domain controller）
  - [設備控制](../Page/設備控制.md "wikilink")，在ASCII中編號為17－20。
  - [分布式计算](../Page/分布式计算.md "wikilink")（Distributed computing）
  - [都柏林核心](../Page/都柏林核心.md "wikilink")（Dublin Core）

## 文艺和娱乐

  - [DC漫画](../Page/DC漫画.md "wikilink")，美国的一所漫画出版社。
  - Da capo，音乐学指[返始](../Page/返始.md "wikilink")，意思是“重复一遍”。
  - [天命真女](../Page/天命真女.md "wikilink")（Destiny's Child）。
  - 《[名偵探柯南](../Page/名偵探柯南.md "wikilink")》（*Detective Conan*），日本漫畫。
  - Director's Cut，导演剪辑版
  - Discovery Channel，[探索频道](../Page/探索频道.md "wikilink")。
  - Disney Channel，[迪士尼频道](../Page/迪士尼频道.md "wikilink")。
  - [初音島](../Page/初音島.md "wikilink")（），日本[CIRCUS製作發售的](../Page/CIRCUS_\(遊戲品牌\).md "wikilink")[美少女遊戲系列](../Page/美少女遊戲.md "wikilink")。
  - [大卫·库克](../Page/大卫·库克.md "wikilink")（David Cook），美国摇滚歌手，也可指他的同名专辑。
  - [達倫·克里斯](../Page/達倫·克里斯.md "wikilink") （Darren Criss），美國歌星和演員

## 地理

  - [哥伦比亚特区](../Page/哥伦比亚特区.md "wikilink")（District of
    Columbia），美国首都华盛顿所在的区域名称。
  - Distrito capital，西班牙语“首都地区”的缩写：
      - [波哥大](../Page/波哥大.md "wikilink")
      - [卡拉卡斯](../Page/卡拉卡斯.md "wikilink")

## 交通

  - [戴姆勒-克莱斯勒](../Page/戴姆勒-克莱斯勒.md "wikilink")（DaimlerChrysler），汽车公司。
  - [大衛·庫塔](../Page/大衛·庫塔.md "wikilink")（David Coulthard），一級方程式賽車車手。
  - Douglas Commercial，道格拉斯飞行器公司出产的客机系列代号。