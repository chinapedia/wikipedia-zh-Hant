**DEF.DIVA**是由原[早安少女组成员](../Page/早安少女组.md "wikilink")[後藤真希](../Page/後藤真希.md "wikilink")、[安倍夏美](../Page/安倍夏美.md "wikilink")、[石川梨華以及個人歌手](../Page/石川梨華.md "wikilink")[松浦亞彌共四人組成的](../Page/松浦亞彌.md "wikilink")「[Hello\!
Project](../Page/Hello!_Project.md "wikilink")」特别團體，當中沒有隊長。

[淳君指](../Page/淳君.md "wikilink")「DEF.」在英語中是最強（棒）的意思（Definitive的縮寫）。DIVA是意大利語，主役女性歌手的意思；把兩國語言和起來組成，名為DEF.DIVA代表最強（棒）歌姬。

## 音樂

1.  喜歡太多 就像傻瓜（，2005年10月19日 EPCE-5381）
2.  去吧！樂天金鷹（，職業棒球隊「[東北樂天金鷹](../Page/東北樂天金鷹.md "wikilink")」2006年的官方加油歌曲）

## 外部連結

  - [HP官方网站关于DEF.DIVA的介绍](http://www.helloproject.com/artist/33/index.html)
  - [HP官方网站关于DEF.DIVA成立的新闻](https://web.archive.org/web/20051031201435/http://www.helloproject.com/newslist/050903-1.html)

[Category:Hello\! Project](../Category/Hello!_Project.md "wikilink")
[Category:日本女子演唱團體](../Category/日本女子演唱團體.md "wikilink")
[Category:期間限定團體](../Category/期間限定團體.md "wikilink")