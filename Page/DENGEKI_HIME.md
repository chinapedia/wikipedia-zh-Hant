**DENGEKI
HIME**，（原名“”）是[MediaWorks發行的](../Page/MediaWorks.md "wikilink")[日本成人遊戲月刊雜誌](../Page/日本成人遊戲.md "wikilink")，於1997年發行至2014年\[1\]，2007年將書名從《電擊姬》更改為現名。\[2\]

## 歷史、介紹

自1997年以来以《電擊姬》（）的标题作为《[電擊王](../Page/電擊王.md "wikilink")》的增刊发行，但於2001年成為獨立的月刊。後來開始連載[BL系遊戲情報專欄](../Page/BL.md "wikilink")《電擊若》〔〕，及後更發展成同名增刊。

從創刊數年間是唯一封面不使用插畫，而使用女藝人封面的成人遊戲雜誌。於2003年末開始轉為插畫封面。在2005年1月號使用先月才起用的插畫家[Tony的插圖](../Page/Tony_\(插畫家\).md "wikilink")，更令他擔當人物設定的遊戲《[光明之淚](../Page/光明之淚.md "wikilink")》成為話題。

同時，它和姊妹誌《[電擊G's雜誌](../Page/電擊G's雜誌.md "wikilink")》同様有[讀者参加計劃實施](../Page/讀者参加計劃.md "wikilink")。「[Maid
in Dream](../Page/Maid_in_Dream.md "wikilink")」於2002年被遊戲化。

2006年9月號，G's編集長高野希義兼任電擊姬的編集長，引起兩誌是否統一的謠言。

2007年4月號，雜誌內容大幅重新設計，名稱也變為现在的「DENGEKI
HIME」（電擊姬日文的[羅馬字](../Page/羅馬字.md "wikilink")）。

2014年12月27日發售2015年2月号後休刊，2018年3月26日官方宣布於同年4月2日關閉官方網站「.com」。

## 本誌連載的讀者参加計劃

  - （画：・文：[山下卓](../Page/山下卓.md "wikilink")）

<!-- end list -->

  -
    Vol.7～2003年12月号

<!-- end list -->

  - （画：・文：）

<!-- end list -->

  -
    2001年4月号～2005年5月号

<!-- end list -->

  - （画：・）

<!-- end list -->

  -
    2004年6月号～2005年8月号

<!-- end list -->

  - （画：伊能津）

<!-- end list -->

  -
    2004年10月号～連載中

<!-- end list -->

  - （企劃：・画：YUKIRIN）

<!-- end list -->

  -
    2005年12月号～連載中

<!-- end list -->

  - （企劃：櫻森柚木・画：[水瀨凜](../Page/水瀨凜.md "wikilink")）

<!-- end list -->

  -
    2005年12月号～連載中

## 參考來源

## 外部連結

  - [電撃姫.com](http://www.dengeki-hime.com/)

[Category:日本成人遊戲雜誌](../Category/日本成人遊戲雜誌.md "wikilink") [Category:ASCII
Media Works](../Category/ASCII_Media_Works.md "wikilink")
[Category:1997年創辦的雜誌](../Category/1997年創辦的雜誌.md "wikilink")
[Category:日本月刊](../Category/日本月刊.md "wikilink")

1.
2.