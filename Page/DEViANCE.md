[DEViANCE-NFO.png](https://zh.wikipedia.org/wiki/File:DEViANCE-NFO.png "fig:DEViANCE-NFO.png")
by Strick9.
Source: DEV-U4DV[.NFO](../Page/.nfo.md "wikilink")，screenshot from pftp
viewing deviance.nfo\]\]
[DEViANCE_2005_cracktro.png](https://zh.wikipedia.org/wiki/File:DEViANCE_2005_cracktro.png "fig:DEViANCE_2005_cracktro.png")
loader by alpha one and h2o (2005).\]\]
[Deviance_intro.PNG](https://zh.wikipedia.org/wiki/File:Deviance_intro.PNG "fig:Deviance_intro.PNG")
of Call of Duty 2.\]\]
**DEViANCE**是一个地下游戏破解组织，成立于1999年1月1日。在[Topsite上发布破解和完整的PC游戏](../Page/Topsite.md "wikilink")[ISOs](../Page/ISO镜像.md "wikilink")。组织的成立者是CyberNaj。

DEViANCE以破解和发布有名气的软件而出名。一个很好的例子是他们发布了[虚幻2004](../Page/虚幻2004.md "wikilink")，而时间竟然比官方发布早了24小时，官方发布的时间是2004年[3月16号](../Page/3月16号.md "wikilink")。

DEViANCE也发布游戏单独破解，升级补丁破解，作弊器，[注册机等](../Page/注册机.md "wikilink")（通常术语叫[DOX](../Page/DOX.md "wikilink")）。

## 告别

在2006年12月25号DEViANCE决定停止所有活动。引用DEViANCE官方的说明“"...and allot of people who
spend more time in writing articles in forums instead to get something
done, forces me to make this decision to protect the name and the honour
some of our productions earned in the past.”

DEViANCE停止活动之前的成员有（不包括退休的）：Anesthetic, Vigo, Ghandy, TMB ( SCX DVN Times
), DocD, Interpol, Core, Sir Truck, Hobby1, Wodka, Keito, AM-FM and
Gopher.

然而，DEViANCE曾经声明，如果有人愿意接管这个小组，也许DEViANCE还能继续下去。

## 历史

  - DEViANCE的前身是[DVNiSO](../Page/DVNiSO.md "wikilink")，the PC
    [ISO](../Page/ISO_image.md "wikilink") division of
    [DiViNE](../Page/DiViNE.md "wikilink")。
  - DEViANCE在2006年底停止活动，然后改名为HATRED活动了一段时间。

## 出名的作品

  - [彩虹六号：禁闭](../Page/彩虹六号：禁闭.md "wikilink")
  - [孤岛惊魂](../Page/孤岛惊魂_\(游戏\).md "wikilink")
  - [四海兄弟：失落的天堂](../Page/四海兄弟：失落的天堂.md "wikilink")
  - [使命召唤 (游戏)使命召唤](../Page/使命召唤_\(游戏\)使命召唤.md "wikilink")
  - [使命召唤2](../Page/使命召唤2.md "wikilink")
  - [极品飞车：地下狂飙](../Page/极品飞车：地下狂飙.md "wikilink")
  - [命令与征服：将军](../Page/命令与征服：将军.md "wikilink")（和[将军之零点行动资料片](../Page/《命令与征服：将军之零点行动.md "wikilink")）
  - [虚幻2004](../Page/虚幻2004.md "wikilink")
  - [雷神之锤4](../Page/雷神之锤4.md "wikilink")
  - [黑与白2](../Page/黑与白2.md "wikilink")
  - [无冬之夜](../Page/无冬之夜_\(游戏\).md "wikilink")
  - [罗马：全面战争](../Page/罗马：全面战争.md "wikilink")
  - [魔兽争霸III：冰封王座](../Page/魔兽争霸III：冰封王座.md "wikilink")
  - [恐惧杀手](../Page/恐惧杀手.md "wikilink")

## 参见

  - [Warez](../Page/Warez.md "wikilink")

## 外部链接

  - [官方Deviance站点](http://deviance.untergrund.net/)
  - [在Defacto2关于Deviance的一些东西](http://www.defacto2.net/groups.cfm?mode=detail&org=deviance)

[Category:Warez](../Category/Warez.md "wikilink")