[B727_DHL_AKL.jpg](https://zh.wikipedia.org/wiki/File:B727_DHL_AKL.jpg "fig:B727_DHL_AKL.jpg")貨機\]\]

**DHL快遞**（），是一家創立於[美國的運輸公司](../Page/美國.md "wikilink")，目前由[德國郵政集團全資持有](../Page/德國郵政.md "wikilink")，是目前世界上最大的運輸公司之一。DHL快遞於[中國大陸因与](../Page/中國大陸.md "wikilink")[中國外運股份有限公司](../Page/中國外運股份有限公司.md "wikilink")（中外運）的合作夥伴關係稱爲中外運-{}-敦豪，日常使用**-{DHL}-**；而在[台灣](../Page/台灣.md "wikilink")，其早年進入台灣時曾使用**洋基通運**的譯名，但為了企業識別的統一，在行銷上已經直接稱為**-{DHL}-**，中文名稱僅保留作為公司註冊用名；在[香港](../Page/香港.md "wikilink")，公司註冊名稱為**-{敦豪}-國際**，但日常仍然使用-{DHL}-。

1998年，德國郵政開始收購DHL的股份，且在2001年握有了控股權，並於2002年12月之前收購了所有流通股\[1\]。現今，DHL快遞與及等德國郵政旗下事業單位，共享了“DHL”這個品牌名稱\[2\]。

## 發展

1969年，-{DHL}-開設了他們的第一條從[舊金山到](../Page/舊金山.md "wikilink")[檀香山的快遞運輸航線](../Page/檀香山_\(夏威夷\).md "wikilink")，公司的名稱“-{DHL}-”由三位創始人姓氏的首字母組成（Dalsey,
Hillblom and
Lynn）。DHL成立第3年，香港企業家[鍾普洋因工作關係結識了創辦人之一的Adrian](../Page/鍾普洋.md "wikilink")
Dalsey，加入DHL的創辦行列，於1972年以DHL名稱在香港開設DHL國際（香港），負責美國本土以外的國際快遞業務，將DHL的航線擴展到[香港](../Page/香港.md "wikilink")、[日本](../Page/日本.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[澳大利亞和](../Page/澳大利亞.md "wikilink")[新加坡](../Page/新加坡.md "wikilink")。在DHL致力建立起一個嶄新的、提供全球-{zh-hans:门到门;
zh-hant:戶對戶;}-運輸服務的網絡構想下，在1970年代中後期DHL將他們的航線擴展到[南美洲](../Page/南美洲.md "wikilink")、[中東地區和](../Page/中東.md "wikilink")[非洲](../Page/非洲.md "wikilink")。

2002年開始，[德國郵政控制了其全部股權並將旗下的DHL](../Page/德國郵政.md "wikilink")、丹沙公司（Danzas）以及歐洲快運公司整合為新的DHL公司。2003年，德國郵政又收購了[美國的空運特快公司](../Page/美國.md "wikilink")（Airborne
Express），並將它整合到DHL裏。2005年，德國郵政又收購了[英國的英运公司](../Page/英國.md "wikilink")（Exel
plc），並將它整合到DHL裏。至此DHL公司擁有了世界上最完善的運輸網路之一，可以到達220個國家和地區的12萬個目的地。

2007年1月26日，敦豪宣布正式启动在中国国内的货物空运业务\[3\]。

2018年10月26日，[順豐速運透過順豐香港與](../Page/順豐速運.md "wikilink")[德國郵政敦豪集團](../Page/德國郵政.md "wikilink")(DPDHL)達成戰略合作，順豐香港以7億歐元/55億元人民幣/62億港元現金收購[德國郵政敦豪集團](../Page/德國郵政.md "wikilink")(DPDHL)旗下的[敦豪香港及敦豪北京](../Page/敦豪.md "wikilink")，即[DHL中國內地](../Page/DHL.md "wikilink")、香港及澳門供應鏈業務

## 設施

DHL快遞其下有自己的貨運機隊及車輛：

  - DHL快遞的機隊有122架[飛機](../Page/飛機.md "wikilink")，機型主要包括[空中客車A300型貨機](../Page/空中客車A300.md "wikilink")、[波音757型貨機](../Page/波音757.md "wikilink")；原有的[波音727機隊正逐步退役](../Page/波音727.md "wikilink")。DHL機隊以[萊比錫/哈雷機場作為樞紐](../Page/萊比錫/哈雷機場.md "wikilink")。

DHL快遞的機隊如下：

<center>

| <span style="color:white;">機型                    | <span style="color:white;">數量 | <span style="color:white;">已訂購 | <span style="color:white;">備註 |
| ------------------------------------------------ | ----------------------------- | ------------------------------ | ----------------------------- |
| [空中客车A300-600RF](../Page/空中客车A300.md "wikilink") | 29                            | 0                              |                               |
| [ATR 42-300F](../Page/ATR_42.md "wikilink")      | 3                             | 0                              |                               |
| [波音727-200F](../Page/波音727.md "wikilink")        | 6                             | 0                              |                               |
| [波音737-200CF](../Page/波音737.md "wikilink")       | 1                             | 0                              |                               |
| [波音737-400SF](../Page/波音737.md "wikilink")       | 4                             | 4                              |                               |
| [波音747-400BCF](../Page/波音747.md "wikilink")      | 13                            | 0                              |                               |
| [波音747-8F](../Page/波音747-8.md "wikilink")        | 4                             | 0                              |                               |
| [波音757-200F](../Page/波音757.md "wikilink")        | 44                            | 0                              |                               |
| [波音767-200F](../Page/波音767.md "wikilink")        | 7                             | 0                              |                               |
| [波音767-300ERF](../Page/波音767.md "wikilink")      | 6                             | 0                              |                               |
| [波音777F](../Page/波音777.md "wikilink")            | 12                            | 0                              |                               |
| [圖204-100C](../Page/圖-204.md "wikilink")         | 1                             | 0                              |                               |
| 總數                                               | 122                           | 4                              |                               |

**DHL機隊**

</center>

  - 作业[车辆](../Page/车辆.md "wikilink")：76,200部

## 附屬公司

[Dhl.a300b4-200f.arp.jpg](https://zh.wikipedia.org/wiki/File:Dhl.a300b4-200f.arp.jpg "fig:Dhl.a300b4-200f.arp.jpg")[希斯洛機場的DHL](../Page/希斯洛機場.md "wikilink")[空中巴士A300B4-200F型貨機](../Page/空中巴士A300.md "wikilink")\]\]

  - [中外運敦豪](../Page/中外運敦豪.md "wikilink")
  - [香港華民航空](../Page/香港華民航空.md "wikilink")：2002年10月：國泰航空與DHL結盟，40%的華民股權轉移給DHL。華民開始代操DHL從香港出發往亞洲的貨運航班。
  - [DHL中亞樞紐中心](../Page/DHL中亞樞紐中心.md "wikilink")：位於[香港國際機場境內](../Page/香港國際機場.md "wikilink")、佔地18200平方米的機場速遞貨運中心第一期，是由DHL的香港子公司敦豪國際投入一億美元興建，並於2004年7月16日正式投入運作。在接續的第二期與第三期工程陸續完成後，整個貨運中心的最終處理能力可提升到每日900噸貨件，並達到每小時二萬件的貨件處理量。\[4\]\<\!--Deutsche
    Post owns four main airlines, which provide services by region:\[5\]

<!-- end list -->

  - **[European Air
    Transport](../Page/European_Air_Transport.md "wikilink")** is
    responsible for the major part of the European network and for
    longhaul services to the [Middle
    East](../Page/Middle_East.md "wikilink") and
    [Africa](../Page/Africa.md "wikilink").
  - **[DHL Air UK](../Page/DHL_Air_UK.md "wikilink")** has been recently
    purchased and provides additional services within Europe.
  - **[DHL Aero Expreso](../Page/DHL_Aero_Expreso.md "wikilink")** is
    the subsidiary in [Central and South
    America](../Page/Central_and_South_America.md "wikilink").
  - **[SNAS/DHL](../Page/SNAS/DHL.md "wikilink")** handles [Middle
    East](../Page/Middle_East.md "wikilink") destinations.

Deutsche Post also has stakes in the following airlines, some of which
also operate under the DHL brand or livery:\[6\]

  - [Aero Express Del
    Ecuador](../Page/Aero_Express_Del_Ecuador.md "wikilink"), Guayaquil,
    Ecuador (owned 100%).
  - [Aerologic](../Page/Aerologic.md "wikilink"), Leipzig, Germany
    (50%).
  - [Air Hong Kong](../Page/Air_Hong_Kong.md "wikilink"), Hong Kong
    (40%).
  - [ABX Air, Inc.](../Page/ABX_Air,_Inc..md "wikilink") United States
    (0%, Contract).
  - [Blue Dart Aviation](../Page/Blue_Dart_Aviation.md "wikilink"),
    Chennai, India (32%).
  - [DHL de Guatemala](../Page/DHL_de_Guatemala.md "wikilink"),
    Guatemala City (100%).
  - [Polar Air Cargo](../Page/Polar_Air_Cargo.md "wikilink"), Purchase,
    NY, USA (49%).
  - [Solenta Aviation](../Page/Solenta_Aviation.md "wikilink"),
    Johannesburg, South Africa (unknown).
  - [Tasman Cargo
    Airlines](../Page/Tasman_Cargo_Airlines.md "wikilink"), Sydney,
    Australia (49%).

**[ASTAR Air Cargo](../Page/ASTAR_Air_Cargo.md "wikilink")** was
formerly known as **DHL Airways** (and therefore carries the [ICAO
airline designator](../Page/ICAO_airline_designator.md "wikilink") DHL)
but has been partially sold and renamed.--\>

## 參見

  - [聯邦快遞](../Page/聯邦快遞.md "wikilink")
  - [烏伯林根空難](../Page/烏伯林根空難.md "wikilink")
  - [DHL貨機巴格達遇襲事件](../Page/DHL貨機巴格達遇襲事件.md "wikilink")

## 参考文献

## 外部链接

  - [DHL官方网站](http://www.dhl.com)（多国语言首页）
      - [DHL臺灣](http://www.dhl.com.tw)
      - [DHL中国](http://www.cn.dhl.com/)
      - [DHL香港](http://www.dhl.com.hk)
      - [DHL台灣420手續費](https://www.mytopshopping.com/2018/02/dhl-420-tax-fee.html)

[DHL](../Category/DHL.md "wikilink")
[Category:1969年成立的航空公司](../Category/1969年成立的航空公司.md "wikilink")
[Category:速遞公司](../Category/速遞公司.md "wikilink")
[Category:英國皇家御用企業](../Category/英國皇家御用企業.md "wikilink")

1.
2.
3.  冯亦珍，[敦豪涉足中国内地货物空运市场](http://news.xinhuanet.com/fortune/2007-01/28/content_5663060.htm)，新华网
4.  [DHL機場速遞貨運中心投產](http://www.lgt.polyu.edu.hk/lgtssc/news/2004/DHL_hk4.7.27.htm)

5.
6.