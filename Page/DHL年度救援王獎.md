**[DHL年度救援王](../Page/DHL.md "wikilink")**（**DHL Presents the Delivery
Man Of The Year
Award**）\[1\]是[美國職棒大聯盟的獎項之一](../Page/美國職棒大聯盟.md "wikilink")，由美國公司[DHL所贊助](../Page/DHL.md "wikilink")，專門頒給良好表現的[救援投手](../Page/救援投手.md "wikilink")，是一種由球迷投票所產的獎項。這個獎項在2005年第一次頒發，得獎者是[紐約洋基的](../Page/紐約洋基.md "wikilink")[馬里安諾·李维拉](../Page/馬里安諾·李维拉.md "wikilink")，當前的年度得主是在2012年所頒給[坦帕灣光芒的](../Page/坦帕灣光芒.md "wikilink")[費南多·羅尼](../Page/費南多·羅尼.md "wikilink")、單月得主則為[2013年5月所表揚給](../Page/2013年5月.md "wikilink")[匹茲堡海盜的](../Page/匹茲堡海盜.md "wikilink")。其中單月的獎項為**DHL單月救援王**（DHL
Presents the Delivery Man of the Month
Award）是在季中頒發，且不像年度救援王獎項為球迷投票，是由[丹尼斯·艾克斯利](../Page/丹尼斯·艾克斯利.md "wikilink")、、和所組成的四人評審小組所選出的。

## 年度得主

  - 2005年－[馬里安諾·李維拉](../Page/馬里安諾·李維拉.md "wikilink")
  - 2006年－[馬里安諾·李维拉](../Page/馬里安諾·李维拉.md "wikilink")(2)\[2\]
  - 2007年－[喬納森·派柏邦](../Page/喬納森·派柏邦.md "wikilink")
  - 2008年－[布萊德·李吉](../Page/布萊德·李吉.md "wikilink")
  - 2009年－[馬里安諾·李維拉](../Page/馬里安諾·李維拉.md "wikilink")(3)
  - 2010年－[希斯·貝爾](../Page/希斯·貝爾.md "wikilink")
  - 2011年－[荷西·瓦爾韋德](../Page/荷西·瓦爾韋德.md "wikilink")
  - 2012年－[費南多·羅尼](../Page/費南多·羅尼.md "wikilink")
  - 2013年－[克雷格·金布雷爾](../Page/克雷格·金布雷爾.md "wikilink")

## 單月得主

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>月份</p></th>
<th><p>得主</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>球員</p></td>
<td><p>球隊</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p>四月</p></td>
<td></td>
<td><p><a href="../Page/匹茲堡海盜.md" title="wikilink">匹茲堡海盜</a></p></td>
</tr>
<tr class="odd">
<td><p>五月</p></td>
<td><p><a href="../Page/崔佛·赫夫曼.md" title="wikilink">崔佛·赫夫曼</a>(1)</p></td>
<td><p><a href="../Page/聖地牙哥教士.md" title="wikilink">聖地牙哥教士</a></p></td>
<td><p>[3]</p></td>
</tr>
<tr class="even">
<td><p>六月</p></td>
<td></td>
<td><p><a href="../Page/華盛頓國民.md" title="wikilink">華盛頓國民</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>七月</p></td>
<td><p><a href="../Page/布萊德·李吉.md" title="wikilink">布萊德·李吉</a></p></td>
<td><p><a href="../Page/休士頓太空人.md" title="wikilink">休士頓太空人</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>八月</p></td>
<td></td>
<td><p><a href="../Page/佛羅里達馬林魚.md" title="wikilink">佛羅里達馬林魚</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>九月</p></td>
<td><p><a href="../Page/萊恩·丹普斯特.md" title="wikilink">萊恩·丹普斯特</a></p></td>
<td><p><a href="../Page/芝加哥小熊.md" title="wikilink">芝加哥小熊</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p>四月</p></td>
<td><p><a href="../Page/喬納森·派柏邦.md" title="wikilink">喬納森·派柏邦</a>(1)</p></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="odd">
<td><p>五月</p></td>
<td></td>
<td><p><a href="../Page/聖路易紅雀.md" title="wikilink">聖路易紅雀</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>六月</p></td>
<td><p><a href="../Page/巴比·占克斯.md" title="wikilink">巴比·占克斯</a>(1)</p></td>
<td><p><a href="../Page/芝加哥白襪.md" title="wikilink">芝加哥白襪</a></p></td>
<td><p>[4]</p></td>
</tr>
<tr class="odd">
<td><p>七月</p></td>
<td><p><a href="../Page/乔·内森.md" title="wikilink">乔·内森</a>(1)</p></td>
<td><p><a href="../Page/明尼蘇達雙城.md" title="wikilink">明尼蘇達雙城</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>八月</p></td>
<td><p><a href="../Page/法蘭西斯科·羅德里奎茲.md" title="wikilink">法蘭西斯科·羅德里奎茲</a>(1)</p></td>
<td><p><a href="../Page/洛杉磯安那罕天使.md" title="wikilink">洛杉磯安那罕天使</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>九月</p></td>
<td><p>崔佛·赫夫曼（2）</p></td>
<td><p><a href="../Page/聖地牙哥教士.md" title="wikilink">聖地牙哥教士</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>四月</p></td>
<td><p><a href="../Page/弗朗西斯科·科德罗.md" title="wikilink">弗朗西斯科·科德罗</a></p></td>
<td><p><a href="../Page/密爾瓦基釀酒人.md" title="wikilink">密爾瓦基釀酒人</a></p></td>
</tr>
<tr class="odd">
<td><p>五月</p></td>
<td><p>崔佛·赫夫曼(3)</p></td>
<td><p><a href="../Page/聖地牙哥教士.md" title="wikilink">聖地牙哥教士</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>六月</p></td>
<td><p>(1)</p></td>
<td><p><a href="../Page/西雅圖水手.md" title="wikilink">西雅圖水手</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>七月</p></td>
<td><p><a href="../Page/比利·華格納.md" title="wikilink">比利·華格納</a></p></td>
<td><p><a href="../Page/紐約大都會.md" title="wikilink">紐約大都會</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>八月</p></td>
<td><p><a href="../Page/齋藤隆.md" title="wikilink">齋藤隆</a></p></td>
<td><p><a href="../Page/洛杉磯道奇.md" title="wikilink">洛杉磯道奇</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>九月</p></td>
<td></td>
<td><p><a href="../Page/科羅拉多落磯.md" title="wikilink">科羅拉多落磯</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>四月</p></td>
<td><p><a href="../Page/馬里安諾·李維拉.md" title="wikilink">馬里安諾·李維拉</a>(1)</p></td>
<td><p><a href="../Page/紐約洋基.md" title="wikilink">紐約洋基</a></p></td>
</tr>
<tr class="odd">
<td><p>五月</p></td>
<td><p><a href="../Page/B·J·萊恩.md" title="wikilink">B·J·萊恩</a></p></td>
<td><p><a href="../Page/多倫多藍鳥.md" title="wikilink">多倫多藍鳥</a></p></td>
<td><p>[5]</p></td>
</tr>
<tr class="even">
<td><p>六月</p></td>
<td><p>法蘭西斯科·羅德里奎茲 (2)</p></td>
<td><p><a href="../Page/洛杉磯安那罕天使.md" title="wikilink">洛杉磯安那罕天使</a></p></td>
<td><p>[6]</p></td>
</tr>
<tr class="odd">
<td><p>七月</p></td>
<td><p>乔·内森(2)</p></td>
<td><p><a href="../Page/明尼蘇達雙城.md" title="wikilink">明尼蘇達雙城</a></p></td>
<td><p>[7]</p></td>
</tr>
<tr class="even">
<td><p>八月</p></td>
<td><p><a href="../Page/荷西·瓦爾韋德.md" title="wikilink">荷西·瓦爾韋德</a></p></td>
<td><p><a href="../Page/休士頓太空人.md" title="wikilink">休士頓太空人</a></p></td>
<td><p>[8]</p></td>
</tr>
<tr class="odd">
<td><p>九月</p></td>
<td><p><a href="../Page/喬金·索里亞.md" title="wikilink">喬金·索里亞</a>(1)</p></td>
<td><p><a href="../Page/堪薩斯皇家.md" title="wikilink">堪薩斯皇家</a></p></td>
<td><p>[9]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>四月</p></td>
<td><p><a href="../Page/希斯·贝尔.md" title="wikilink">希斯·贝尔</a></p></td>
<td><p><a href="../Page/聖地牙哥教士.md" title="wikilink">聖地牙哥教士</a></p></td>
</tr>
<tr class="odd">
<td><p>五月</p></td>
<td><p>崔佛·赫夫曼(4)</p></td>
<td><p><a href="../Page/密爾瓦基釀酒人.md" title="wikilink">密爾瓦基釀酒人</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>六月</p></td>
<td><p>喬·內森(3)</p></td>
<td><p><a href="../Page/明尼蘇達雙城.md" title="wikilink">明尼蘇達雙城</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>七月</p></td>
<td><p>馬里安諾·李維拉(2)</p></td>
<td><p><a href="../Page/紐約洋基.md" title="wikilink">紐約洋基</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>八月</p></td>
<td><p><a href="../Page/萊恩·福蘭克林.md" title="wikilink">萊恩·福蘭克林</a></p></td>
<td><p><a href="../Page/聖路易紅雀.md" title="wikilink">聖路易紅雀</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>九月</p></td>
<td><p>喬金·索里亞(2)</p></td>
<td><p><a href="../Page/堪薩斯皇家.md" title="wikilink">堪薩斯皇家</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>四月</p></td>
<td><p><a href="../Page/麥特·卡普斯.md" title="wikilink">麥特·卡普斯</a></p></td>
<td><p><a href="../Page/華盛頓國民.md" title="wikilink">華盛頓國民</a></p></td>
</tr>
<tr class="odd">
<td><p>五月</p></td>
<td><p><a href="../Page/拉斐爾·索利安諾.md" title="wikilink">拉斐爾·索利安諾</a> (1)</p></td>
<td><p><a href="../Page/坦帕灣光芒.md" title="wikilink">坦帕灣光芒</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>六月</p></td>
<td><p>巴比·占克斯 (2)</p></td>
<td><p><a href="../Page/芝加哥白襪.md" title="wikilink">芝加哥白襪</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>七月</p></td>
<td><p>拉斐爾·索利安諾 (2)</p></td>
<td><p><a href="../Page/坦帕灣光芒.md" title="wikilink">坦帕灣光芒</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>八月</p></td>
<td><p>拉斐爾·索利安諾 (3)</p></td>
<td><p><a href="../Page/坦帕灣光芒.md" title="wikilink">坦帕灣光芒</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>九月</p></td>
<td><p><a href="../Page/卡洛斯·馬摩爾.md" title="wikilink">卡洛斯·馬默</a></p></td>
<td><p><a href="../Page/芝加哥小熊.md" title="wikilink">芝加哥小熊</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>四月</p></td>
<td><p><a href="../Page/休斯頓·史崔特.md" title="wikilink">休斯頓·史崔特</a></p></td>
<td><p><a href="../Page/科羅拉多洛磯.md" title="wikilink">科羅拉多洛磯</a></p></td>
</tr>
<tr class="odd">
<td><p>五月</p></td>
<td><p>J.J·帕茲 (2)</p></td>
<td><p><a href="../Page/亞利桑那響尾蛇.md" title="wikilink">亞利桑那響尾蛇</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>六月</p></td>
<td></td>
<td><p><a href="../Page/匹茲堡海盜.md" title="wikilink">匹茲堡海盜</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>七月</p></td>
<td><p><a href="../Page/約翰·艾克斯福德.md" title="wikilink">約翰·艾克斯福德</a></p></td>
<td><p><a href="../Page/密爾瓦基釀酒人.md" title="wikilink">密爾瓦基釀酒人</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>八月</p></td>
<td><p><a href="../Page/克雷格·金布雷爾.md" title="wikilink">克雷格·金布雷爾</a> (1)</p></td>
<td><p><a href="../Page/亞特蘭大勇士.md" title="wikilink">亞特蘭大勇士</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>九月</p></td>
<td><p>JJ Putz (3)</p></td>
<td><p><a href="../Page/亞利桑那響尾蛇.md" title="wikilink">亞利桑那響尾蛇</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>四月</p></td>
<td><p>喬納森·派柏邦(2)</p></td>
<td><p><a href="../Page/費城費城人.md" title="wikilink">費城費城人</a></p></td>
</tr>
<tr class="odd">
<td><p>五月</p></td>
<td><p><a href="../Page/吉姆·詹森.md" title="wikilink">吉姆·強森</a></p></td>
<td><p><a href="../Page/巴爾的摩金鶯.md" title="wikilink">巴爾的摩金鶯</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>六月</p></td>
<td><p><a href="../Page/泰勒·克里帕德.md" title="wikilink">泰勒·克里帕德</a></p></td>
<td><p><a href="../Page/華盛頓國民.md" title="wikilink">華盛頓國民</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>七月</p></td>
<td><p><a href="../Page/阿羅魯迪斯·查普曼.md" title="wikilink">阿羅魯迪斯·查普曼</a> (1)</p></td>
<td><p><a href="../Page/辛辛那提紅人.md" title="wikilink">辛辛那提紅人</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>八月</p></td>
<td><p>阿羅魯迪斯·查普曼 (2)</p></td>
<td><p><a href="../Page/辛辛那提紅人.md" title="wikilink">辛辛那提紅人</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>九月</p></td>
<td><p>克雷格·金布雷爾 (2)</p></td>
<td><p><a href="../Page/亞特蘭大勇士.md" title="wikilink">亞特蘭大勇士</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>四月</p></td>
<td><p>(1)</p></td>
<td><p><a href="../Page/匹茲堡海盜.md" title="wikilink">匹茲堡海盜</a></p></td>
</tr>
<tr class="odd">
<td><p>五月</p></td>
<td><p>傑森·葛利里 (2)</p></td>
<td><p><a href="../Page/匹茲堡海盜.md" title="wikilink">匹茲堡海盜</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>六月</p></td>
<td><p>喬·內森 (4)</p></td>
<td><p><a href="../Page/德州遊騎兵.md" title="wikilink">德州遊騎兵</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>七月</p></td>
<td><p>(1)</p></td>
<td><p><a href="../Page/堪薩斯市皇家.md" title="wikilink">堪薩斯皇家</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>八月</p></td>
<td><p>克雷格·金布雷爾 (3)</p></td>
<td><p><a href="../Page/亞特蘭大勇士.md" title="wikilink">亞特蘭大勇士</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>九月</p></td>
<td><p>(2)</p></td>
<td><p><a href="../Page/堪薩斯市皇家.md" title="wikilink">堪薩斯皇家</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 註釋

[Category:DHL](../Category/DHL.md "wikilink")
[Category:美國職棒獎項](../Category/美國職棒獎項.md "wikilink")

1.  mlb.im.tv.
    [紅襪派柏邦獲選DHL年度救援王獎](http://mlb.im.tv/NewsContent.asp?Newsid=2598)．於2008年7月14日查閱
2.  MLB.com. [Rivera is DHL Delivery Man of the
    Year](http://mlb.mlb.com/news/article.jsp?ymd=20061024&content_id=1722137&vkey=news_mlb&fext=.jsp&c_id=mlb)．於2007年10月11日查閱
3.  MLB.com. [Hoffman named winner of the 'DHL Presents the Major League
    Baseball Delivery Man of the Month Award' for
    May](http://sandiego.padres.mlb.com/news/press_releases/press_release.jsp?ymd=20050606&content_id=1077885&vkey=pr_sd&fext=.jsp&c_id=sd)
    ．於2007年10月11日查閱
4.  [Jenks named June's DHL Delivery
    Man](http://chicago.whitesox.mlb.com/news/article.jsp?ymd=20060714&content_id=1556648&vkey=news_cws&fext=.jsp&c_id=cws)
    ．於2007年10月11日查閱
5.
6.
7.
8.
9.