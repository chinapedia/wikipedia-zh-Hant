[DMB_Phone.jpg](https://zh.wikipedia.org/wiki/File:DMB_Phone.jpg "fig:DMB_Phone.jpg")

**多媒體數碼廣播**（DMB，全稱Digital Multimedia
Broadcasting），是[大韓民國開發的數碼](../Page/大韓民國.md "wikilink")[無線電傳輸](../Page/無線電.md "wikilink")[技術](../Page/技術.md "wikilink")\[1\]\[2\]\[3\]，所制定有關[數位電視和流動](../Page/數位電視.md "wikilink")[數位廣播的制式](../Page/數位廣播.md "wikilink")。該制式將作為國家[資訊科技項目用於發送多媒體](../Page/資訊科技.md "wikilink")，如電視、電台和數據廣播到移動設備上，如手機、手提電腦和GPS導航系統。DMB雖是韓國開發的新一代數碼技術，但技術基礎是由德國教授格特·西格勒和哈米德·阿莫爾博士於羅伯特·博世有限公司奠定。它可以通過衛星（S-DMB）或地面（T-DMB）傳輸操作。

## 參考文獻

<references />

[Category:數碼電視](../Category/數碼電視.md "wikilink")
[Category:电信标准](../Category/电信标准.md "wikilink")

1.
2.
3.