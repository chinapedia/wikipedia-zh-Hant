**DNA旋转酶(DNA
Gyrase)**，经常简称为**旋转酶**，是一种II类的[拓扑异构酶](../Page/拓扑异构酶.md "wikilink")（Type
II
Topoisomerase）（[EC编号](../Page/EC编号.md "wikilink")5.99.1.3），它的酶作用底物为ccc型环状DNA（Covalently
Closed Circular DNA），作用是加入[负超螺旋](../Page/负超螺旋.md "wikilink")（Negative
Supercoil）或松弛[正超螺旋](../Page/正超螺旋.md "wikilink")（Positive
Supercoil）。旋转酶可以让DNA形成一个环状的结构（[拓扑学上来说是一个正超螺旋的环环状的结构](../Page/拓扑学.md "wikilink")），通过切开一段DNA并让另一段DNA通过形成一个负超螺旋的环状结构。在这个过程中[环绕数](../Page/环绕数.md "wikilink")（Linking
Number）减少了2（从正超螺旋Lk=＋1到负超螺旋Lk=－1,ΔLk=－2）。他的主要功用為紓解解旋酶解開DNA旋轉時所造成的壓力，因為解壓縮旋轉時會造成極大的逆旋轉壓。

## 旋转酶的酶学模型

依据现今提出的[酶学模型](../Page/酶学.md "wikilink")（如图）\[1\]
旋转酶和cccDNA接合（G片段）形成Gyrase-DNA复合体；DNA被切开，但是形成的两段DNA末端的磷酸和旋转酶上的酪氨酸形成磷酸酯键，化学能被保留，这个过程不需要ATP；旋转酶将和G片段相邻的一段DNA（T片段）通过G片段的切开处，引入负超螺旋，这个过程消耗两个ATP，称为DNA的易位（DNA
Translocation），[喹诺酮类](../Page/喹诺酮.md "wikilink")[抗生素](../Page/抗生素.md "wikilink")（Quinolones）阻碍这一步酶促反应；当DNA易位完成，切开的DNA的末端的[羟基](../Page/羟基.md "wikilink")（3'-OH）攻击于旋转酶[酪氨酸相结合的磷酸重新形成](../Page/酪氨酸.md "wikilink")[磷酸二酯键](../Page/磷酸二酯键.md "wikilink")，这个过程不需要ATP，DNA-螺旋酶复合体回到图示的Gyrase-DNA复合体状态，螺旋酶可以脱离DNA或进入下一个引入负超螺旋的过程。

## 旋转酶的生物意义

细菌的[染色体和](../Page/染色体.md "wikilink")[质粒大多为环状](../Page/质粒.md "wikilink")，在DNA的[转录和](../Page/转录.md "wikilink")[复制时有严重的拓扑问题](../Page/复制.md "wikilink")（Topological
Problem），因为在转录或复制时在[聚合酶的前端会产生正超螺旋](../Page/聚合酶.md "wikilink")（根据拓扑学公式：Lk=Tw＋Wr，解开双螺旋会使Tw减少，Lk在环状DNA没有打开的情况下不会改变，所以Wr一定增加）。产生的正超螺旋，当达到某种程度时会产生张力阻碍DNA双螺旋的解旋进而阻碍DNA的转录或复制。所以旋转酶必须引入负超螺旋或松弛正超螺旋来保证转录和复制正常进行。

## 针对旋转酶的抗生素

旋转酶只存在于[细菌中](../Page/细菌.md "wikilink")，并没有在[真核细胞发现这种酶](../Page/真核细胞.md "wikilink")，所以很多抗生素如喹诺酮类抗生素和[新生霉素都以这个酶作为目标](../Page/新生霉素.md "wikilink")。喹诺酮类抗生素包括[萘啶酸](../Page/萘啶酸.md "wikilink")（Nalidixic
acid）和[诺氟沙星](../Page/诺氟沙星.md "wikilink")（Norfloxacin），很早就被用于治疗，但是直到发现旋转酶才明白它的作用机理：它们不阻碍旋转酶切开DNA链，但是阻碍DNA易位，DNA不能重新连接而使DNA形成碎片，起到杀菌的作用。

## 参考书籍

<references/>

[Category:DNA](../Category/DNA.md "wikilink") [Category:EC
5.99.1](../Category/EC_5.99.1.md "wikilink")
[Category:酶](../Category/酶.md "wikilink")

1.  Gore J, Bryant Z, Stone MD, Nollmann M, Cozzarelli NR, [Bustamante
    C](../Page/Carlos_Bustamante.md "wikilink"), ["Mechanochemical
    Analysis of DNA Gyrase Using Rotor Bead
    Tracking"](http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids=16397501&query_hl=1&itool=pubmed_docsum),
    Nature 2006 Jan 5 (Vol. 439): 100-104.