[DNA_polymerase.png](https://zh.wikipedia.org/wiki/File:DNA_polymerase.png "fig:DNA_polymerase.png")基序的三维模型\]\]**DNA聚合酶**（DNA
Polymerase，[EC編號](../Page/EC編號.md "wikilink")2.7.7.7）是一種參與[DNA複製的](../Page/DNA複製.md "wikilink")[酶](../Page/酶.md "wikilink")。它主要是以[模板的形式](../Page/模板.md "wikilink")，[催化](../Page/催化.md "wikilink")[去氧核糖核苷酸的](../Page/去氧核糖核苷酸.md "wikilink")[聚合](../Page/聚合.md "wikilink")。[聚合後的](../Page/聚合.md "wikilink")[分子將會組成模板鏈並再進一步參與配對](../Page/分子.md "wikilink")。

[DNA_replication_zh.png](https://zh.wikipedia.org/wiki/File:DNA_replication_zh.png "fig:DNA_replication_zh.png")

DNA聚合酶以[去氧核苷酸三磷酸](../Page/去氧核苷酸.md "wikilink")（dATP、dCTP、dGTP、或dTTP，四者統稱dNTPs）為[底物](../Page/底物.md "wikilink")，沿模板的3'→5'方向，將對應的去氧核苷酸連接到新生DNA鏈的3'端，使新生鏈沿5'→3'方向延長。新鏈與原有的模板鏈[序列互補](../Page/核酸序列.md "wikilink")，亦與模板鏈的原配對鏈序列一致。

已知的所有DNA聚合酶均以5'→3'方向合成DNA，且均不能「重新」（*de
novo*）合成DNA，而只能將去氧核苷酸加到已有的RNA或DNA的3'端[羥基上](../Page/羥基.md "wikilink")。因此，DNA聚合酶除了需要模板做為[序列指導](../Page/核酸序列.md "wikilink")，也必需[-{zh-hans:引物;
zh-hant:引子;}-來起始合成](../Page/引物.md "wikilink")。合成引物的酶叫做[引發酶](../Page/引發酶.md "wikilink")。

反應式：

\[\mbox{dNTP} + \mbox{DNA}_{\mbox{n}}\mbox{-3-OH} \rightarrow \mbox{DNA}_{\mbox{n+1}}\mbox{-3-OH} + 2\mbox{Pi}\]

## 歷史

1957年，[美國](../Page/美國.md "wikilink")[科學家](../Page/科學家.md "wikilink")[阿瑟·科恩伯格](../Page/阿瑟·科恩伯格.md "wikilink")（Arthur
Kornberg）首次在[大腸桿菌中發現DNA聚合酶](../Page/大腸桿菌.md "wikilink")，這種酶被稱為DNA聚合酶I（DNA
polymerase I，簡稱：Pol
I）。1970年，[德國科學家](../Page/德國.md "wikilink")[羅爾夫·克尼佩爾斯](../Page/羅爾夫·克尼佩爾斯.md "wikilink")（Rolf
Knippers）發現DNA聚合酶II（Pol II）。隨後，DNA聚合酶III（Pol
III）被發現。[原核生物中主要的DNA聚合酶及負責](../Page/原核生物.md "wikilink")[染色體複製的是Pol](../Page/染色體.md "wikilink")
III。

## 種類

### 原核生物

[細菌中](../Page/細菌.md "wikilink")，已有五種DNA聚合酶被發現。

  - DNA聚合酶I（Pol
    I）：大腸桿菌K-12株的DNA聚合酶I由[基因polA](../Page/基因.md "wikilink")[編碼](../Page/編碼.md "wikilink")，由928個[氨基酸組成](../Page/氨基酸.md "wikilink")，[分子量](../Page/分子量.md "wikilink")103.1[kDa](../Page/千道爾頓.md "wikilink")，結構類似球狀，直徑約6.5[nm](../Page/納米.md "wikilink")，每個[細胞約有](../Page/細胞.md "wikilink")400個[分子](../Page/分子.md "wikilink")。
  - DNA聚合酶II（Pol II）：在DNA稳定期的损伤修复中起作用。
  - DNA聚合酶III（Pol III）：在大肠杆菌DNA复制过程中起主要作用。
  - DNA聚合酶IV（Pol IV）：与DNA聚合酶II一起负责稳定期的损伤修复。
  - DNA聚合酶V（Pol V）：参与SOS修复。

### 真核生物

  - Pol α：與[引发酶](../Page/引发酶.md "wikilink")(DNA Primase)形成複合體(Pol
    α-primase complex)，合成約10nt
    RNA引子，然後做為DNA合成酶延伸此段RNA引子；合成約20個鹼基(iDNA)後，將後續的延伸過程交給Pol
    δ與ε。
  - Pol β：在[DNA修復中起作用](../Page/DNA修復.md "wikilink")，低保真度的复制
  - Pol γ：複製[線粒體DNA](../Page/線粒體.md "wikilink")。
  - Pol δ：Pol δ與Pol ε是真核細胞的主要DNA聚合酶。
  - Pol ε：填补[引物空隙](../Page/引物.md "wikilink")，切除修复，重组
  - Pol ζ：

## DNA聚合酶介导的DNA合成

[53dna.gif](https://zh.wikipedia.org/wiki/File:53dna.gif "fig:53dna.gif")
DNA聚合酶介导的DNA合成起始于[引物](../Page/引物.md "wikilink")（primer）和DNA配对，配对的[引物](../Page/引物.md "wikilink")3'端带有一个自由的羟基，随后是在DNA聚合酶的催化下由这个自由羟基氧上的配对电子攻击三磷酸碱基上的磷原子的[亲核取代](../Page/亲核取代.md "wikilink")，继而在[戊糖和](../Page/戊糖.md "wikilink")[磷酸之间形成脂键从而完成一个](../Page/磷酸.md "wikilink")[碱基的延伸](../Page/碱基.md "wikilink")。

在整个过程中，[能量是由三磷酸碱基所携带的](../Page/能量.md "wikilink")[高能磷酸键提供的](../Page/高能磷酸键.md "wikilink")，磷酸酯形成以后，一个[焦磷酸分子脱落](../Page/焦磷酸.md "wikilink")，焦磷酸分子再次分裂提供足够的能量给DNA聚合过程。

不同的DNA聚合酶廣泛應用於分子生物學實驗\[1\]。

## 內部連結

  - [DNA複製](../Page/DNA複製.md "wikilink")
  - [DNA修復](../Page/DNA修復.md "wikilink")
  - [聚合酶鏈式反應](../Page/聚合酶鏈式反應.md "wikilink")
  - [RNA聚合酶](../Page/RNA聚合酶.md "wikilink")

## 參考資料

[Category:EC 2.7.7](../Category/EC_2.7.7.md "wikilink")
[Category:DNA复制](../Category/DNA复制.md "wikilink")
[Category:酶](../Category/酶.md "wikilink")

1.