**.doc**，是電腦文件常見[副檔名的一種](../Page/副檔名.md "wikilink")，这三个字母是英文单词document（文件）的缩写。

該[格式原是](../Page/文件格式.md "wikilink")[純文字文件使用的](../Page/純文字.md "wikilink")，多見於不同的[作業系統中](../Page/作業系統.md "wikilink")，軟硬件的使用說明。1980年代，[WordPerfect](../Page/WordPerfect.md "wikilink")
以此作為其專屬文件的副檔名。至1990年代，[微軟在文書處理軟件](../Page/微軟.md "wikilink")
[Word](../Page/Microsoft_Word.md "wikilink") 中，使用了 .doc
作為副檔名，並廣為流行；而前兩者的格式已幾近絕跡。

微軟的“.doc”格式是一種自己的專屬格式，其[檔案可容納更多文字格式](../Page/计算机文件.md "wikilink")、[腳本語言及復原等資訊](../Page/腳本語言.md "wikilink")，比其他的文件檔格式如[RTF](../Page/RTF.md "wikilink")、[HTML等要多](../Page/HTML.md "wikilink")，但因為該格式是屬於專屬格式，因此其[兼容性也較低](../Page/兼容性.md "wikilink")。

在[Palm
OS系統中](../Page/Palm_OS.md "wikilink")，“.doc”是[PalmDoc所使用的副檔名](../Page/PalmDoc.md "wikilink")，一個完全無關的格式，主要用於[電子圖書的編碼](../Page/電子圖書.md "wikilink")。

## 程式／轉換器

  - [Microsoft Word](../Page/Microsoft_Word.md "wikilink")
  - [OpenOffice.org Writer](../Page/OpenOffice.org_Writer.md "wikilink")
    或 [StarOffice](../Page/StarOffice.md "wikilink")
  - [LibreOffice Writer](../Page/LibreOffice#Writer.md "wikilink")
  - [WordPerfect](../Page/WordPerfect.md "wikilink")
  - [AbiWord](../Page/AbiWord.md "wikilink")
  - [KOffice](../Page/KOffice.md "wikilink")
  - [DisplayWrite](../Page/DisplayWrite.md "wikilink")
  - [Interleaf](../Page/Interleaf.md "wikilink")
  - [Wordpad](../Page/Wordpad.md "wikilink")
  - [WordStar](../Page/WordStar.md "wikilink")

## 類似文件格式

  - [DOCX](../Page/DOCX.md "wikilink") – [Microsoft Office Open
    XML](../Page/Microsoft_Office_Open_XML.md "wikilink")
  - [ODT](../Page/ODT.md "wikilink") –
    [OpenDocument](../Page/OpenDocument.md "wikilink") Text
  - [OASIS](../Page/結構化資訊標準促進組織.md "wikilink") [Open Office
    XML](../Page/Open_Office_XML.md "wikilink")（又稱為OpenDocument）
  - [PDF](../Page/PDF.md "wikilink") - Adobe Portable Document Format
  - [RTF](../Page/RTF.md "wikilink") - 微軟的Rich Text Format
  - [XPS](../Page/XPS.md "wikilink") – [XML Paper
    Specification](../Page/XML_Paper_Specification.md "wikilink")

## 参考

[Category:文件格式](../Category/文件格式.md "wikilink")