**DR-DOS**（Digital Research-Disk Operating
System）是[數位研究公司](../Page/數位研究公司.md "wikilink")（Digital
Research，很多人都和[迪吉多](../Page/迪吉多.md "wikilink")（Digital Equipment
Corporation,
DEC）混淆，其實兩家公司完全沒有關係）开发的，最初用于[个人电脑上的一种](../Page/个人电脑.md "wikilink")[DOS](../Page/DOS.md "wikilink")[操作系统](../Page/操作系统.md "wikilink")。主要支持[IBM
PC兼容机](../Page/IBM_PC兼容机.md "wikilink")。

从1990年代PC机广泛使用[Windows
3.x开始](../Page/Windows_3.x.md "wikilink")，DR-DOS系统被逐步被[MS-DOS取代](../Page/MS-DOS.md "wikilink")。到1995年全面采用[Windows
95时](../Page/Windows_95.md "wikilink")，DOS的影响开始减小，DR-DOS也随之在桌面市场迅速衰落。1999年，DR-DOS推出7.03版本，随后在一段时间内一直未有正式更新。目前DR-DOS在单任务[嵌入式设备中还有少量应用](../Page/嵌入式系統.md "wikilink")，另有极少数DOS爱好者还在研究或使用DR-DOS。

2004年，DR-DOS推出了8.0版，功能上仅做了很小修改，支持[FAT32和](../Page/FAT32.md "wikilink")[大容量硬盘](../Page/硬盘.md "wikilink")。2005年又曾一度推出DR-DOS
8.1版，但由于[版权纠纷](../Page/版权.md "wikilink")，随后DR-DOS
8.x从市场上撤回。目前其公司开始重新销售DR-DOS 7.03版本。

## 参见

  - [DOS](../Page/DOS.md "wikilink")
  - [MS-DOS](../Page/MS-DOS.md "wikilink")
  - [PC-DOS](../Page/PC-DOS.md "wikilink")
  - [4DOS](../Page/4DOS.md "wikilink")

## 外部链接

  - [DR-DOS官方主页](http://www.drdos.com)（英文）

[Category:磁盘操作系统](../Category/磁盘操作系统.md "wikilink")