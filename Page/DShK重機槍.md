**DShK 1938**重機槍，又稱
**DShK**（[俄文](../Page/俄文.md "wikilink")：**ДШК**，Дегтярёва-Шпагина
Крупнокалиберный，[英文](../Page/英文.md "wikilink")：Degtyaryov-Shpagin
Large-Calibre），是[蘇聯从](../Page/蘇聯.md "wikilink")1938年[二戰時期开始裝備的重型防空機槍](../Page/二戰.md "wikilink")。

## 設計

DShK發射[12.7×108毫米大口徑彈藥](../Page/12.7×108mm.md "wikilink")，[彈鏈供彈](../Page/彈鏈.md "wikilink")。[二戰時除可作防空機槍外](../Page/二戰.md "wikilink")，亦被裝在輪式射架上用来支援步兵，此射架还可以装上装甲板保护射手。

DShK的名字来源于原設計師[瓦西里·捷格加廖夫及](../Page/瓦西里·捷格加廖夫.md "wikilink")[格里高利·斯帕金](../Page/格里高利·斯帕金.md "wikilink")（改進彈鏈供彈的設計師）的姓氏。DShK有時被縮寫為德什卡（Dushka）。

## 歷史

1929年，[蘇聯軍隊對防空機槍的需求日漸增加](../Page/蘇聯.md "wikilink")，其中第一种防空機槍名為**Degtyaryov
Krupnokalibernyi**缩写作
DK，即**捷格加廖夫大口径机枪**。该武器在1930年設計，在1933年至1935年进行小批量生產。DK防空機槍最初只由30發[彈鼓供彈](../Page/彈鼓.md "wikilink")，射速很低，火力持续性也很差，其後斯帕金（Shpagin）将供彈方式改為[彈鏈供彈](../Page/彈鏈.md "wikilink")，为此重新设计了受弹器，同时也提高了射速。1938年，改進後的DK獲正式採用並被命名為**DShK
1938**，並成為[二戰及之后蘇聯軍隊的制式重機槍](../Page/二戰.md "wikilink")。
[DShK_on_T-55_DD-SD-01-05147.JPEG](https://zh.wikipedia.org/wiki/File:DShK_on_T-55_DD-SD-01-05147.JPEG "fig:DShK_on_T-55_DD-SD-01-05147.JPEG")坦克上的DShKM防空機槍。\]\]

DShK在[二戰時被大量採用](../Page/二戰.md "wikilink")，通常裝在轉軸三腳架作固定防空用途，或裝在[GAZ-AA防空裝甲車上](../Page/GAZ-AA.md "wikilink")。[二戰後期](../Page/二戰.md "wikilink")，DShK亦被在[IS-2坦克及](../Page/IS-2.md "wikilink")[ISU-152自走砲上](../Page/ISU-152.md "wikilink")。DShK也被步兵用作支援用途，裝在輪式射架上，亦有被用作同軸機槍，如[T-40輕型](../Page/T-40.md "wikilink")[兩棲坦克](../Page/兩棲坦克.md "wikilink")。

DShK亦被不少國家特許生產，如[中國](../Page/中國.md "wikilink")（54式或54-1式重機槍）、[巴基斯坦及](../Page/巴基斯坦.md "wikilink")[羅馬尼亞等](../Page/羅馬尼亞.md "wikilink")，[越戰中亦有出現](../Page/越戰.md "wikilink")。自1970年代起，[蘇聯](../Page/蘇聯.md "wikilink")／[俄羅斯軍隊開始以較現代化的](../Page/俄羅斯.md "wikilink")[NSV重機槍和](../Page/NSV重機槍.md "wikilink")[Kord重機槍取代DShK](../Page/Kord重機槍.md "wikilink")，但直至近年仍有部分[俄羅斯軍隊裝備](../Page/俄羅斯.md "wikilink")。

## DShKM

1946年[二戰結束後](../Page/二戰.md "wikilink")，DShK的改進版本**DShK
1938/46**（又稱**DShKM**-[俄文](../Page/俄文.md "wikilink")：ДШКМ）推出。

## 其他圖片

<File:HVO> Army T-55 Glamoc firing MG.jpg|在T-55上開火的DShKM <File:12,7-мм>
станковый пулемёт ДШК образца 1938 года.jpg|裝在三腳架上的DShK
[File:DShK-batey-haosef.jpg|裝上](File:DShK-batey-haosef.jpg%7C裝上)[三腳架的DShK](../Page/三腳架.md "wikilink")
<File:Expomil> 2005 01 TR-85M1 02 Mitraliera PKT.jpg|直到2005年，仍有DShK存在

## 使用國

[Bepo_094.jpg](https://zh.wikipedia.org/wiki/File:Bepo_094.jpg "fig:Bepo_094.jpg")
[12,7-мм_станковый_пулемёт_ДШК_образца_1938_года_(3-1).jpg](https://zh.wikipedia.org/wiki/File:12,7-мм_станковый_пулемёт_ДШК_образца_1938_года_\(3-1\).jpg "fig:12,7-мм_станковый_пулемёт_ДШК_образца_1938_года_(3-1).jpg")
[Jamiat_e-Islami_in_Shultan_Valley_1987_with_Dashaka.jpg](https://zh.wikipedia.org/wiki/File:Jamiat_e-Islami_in_Shultan_Valley_1987_with_Dashaka.jpg "fig:Jamiat_e-Islami_in_Shultan_Valley_1987_with_Dashaka.jpg")期間的[聖戰者與DShK](../Page/聖戰者.md "wikilink")\]\]

  - \[1\]

  - \[2\]

  - \[3\]

  - \[4\]

  - \[5\]

  - \[6\]

  - \[7\]

  - \[8\]

  - \[9\]

  -
  - \[10\]

  -
  - \[11\]

  - \[12\]

  - \[13\]

  - \[14\]

  - \[15\]

  -
  -
  - \[16\]

  - \[17\]

  - \[18\]

  -
  - \[19\]

  - \[20\]

  - \[21\]

  -
  - \[22\]

  - \[23\]

  - \[24\]

  - \[25\]

  - \[26\]

  - \[27\]

  -
  - \[28\]

  - －在當地生產*DShKM*衍生型。\[29\]

  - \[30\]

  -
  - —繳獲使用。\[31\]

  - \[32\]

<!-- end list -->

  - \[33\]

  - \[34\]

  -
  -
  - \[35\]

  - \[36\]

  -
  - \[37\]

  - \[38\]

  - \[39\]

  - \[40\]

  -
  -
  -
  - \[41\]

  -
  - \[42\]

  - \[43\]

  - \[44\]

  - －在當地生產*DShKM*衍生型。\[45\]

  - \[46\]

  -
  - －在當地生產*DShKM*衍生型。

  -
  -
  - \[47\]

  - \[48\]

  - \[49\]

  -
  - \[50\]

  -
  - －在當地生產*DShKM*衍生型。

  - \[51\]

  -
  - \[52\]

  - －為中國[54式重機槍](../Page/54式重機槍.md "wikilink")，泰國內戰時繳獲使用。

  - \[53\]

  -
  -
  - \[54\]

  - \[55\]

  - \[56\]

  -
  - \[57\]

  - \[58\]

  - \[59\]

  - \[60\]

### 前使用國

  - －在當地生產*DShKM*衍生型，命名為「[54式重機槍](../Page/54式重機槍.md "wikilink")」，目前已被[85式重機槍](../Page/85式重機槍.md "wikilink")、[W-85式重機槍和](../Page/W-85式重機槍.md "wikilink")[89式重機槍所取代](../Page/89式重機槍.md "wikilink")。\[61\]

  - －在當地生產*DShKM*衍生型。\[62\]

  - ：已被[白朗寧M2重機槍所取代](../Page/白朗寧M2重機槍.md "wikilink")。\[63\]

  - ：已被[NSV重機槍所取代](../Page/NSV重機槍.md "wikilink")。\[64\]

  - －二戰期間繳獲使用。

  -
  - ：已被[NSV重機槍和](../Page/NSV重機槍.md "wikilink")[Kord重機槍所取代](../Page/Kord重機槍.md "wikilink")。\[65\]

  - ：已被[NSV重機槍所取代](../Page/NSV重機槍.md "wikilink")。\[66\]

  - \[67\]

  - －在當地生產*DShKM*衍生型。\[68\]

### 非國家團体

DShK也是一種廣被[準軍事組織和](../Page/準軍事組織.md "wikilink")[叛軍使用的重機槍之一](../Page/叛軍.md "wikilink")。

  - [阿富汗](../Page/阿富汗.md "wikilink")[聖戰者](../Page/聖戰者.md "wikilink")
  - [車臣武裝份子](../Page/車臣.md "wikilink")
  - [伊拉克叛軍](../Page/伊拉克.md "wikilink")\[69\]
  - [臨時愛爾蘭共和軍](../Page/臨時愛爾蘭共和軍.md "wikilink")\[70\]
  - [越南南方民族解放陣線](../Page/越南南方民族解放陣線.md "wikilink")－[越戰時廣泛使用](../Page/越戰.md "wikilink")。\[71\]
  - [反卡扎菲勢力](../Page/反卡扎菲勢力.md "wikilink")－在[2011年利比亞內戰中使用](../Page/2011年利比亞內戰.md "wikilink")，大部份裝在[武裝改裝車上](../Page/武裝改裝車.md "wikilink")。\[72\]
  - [敘利亞自由軍](../Page/敘利亞自由軍.md "wikilink")
  - [Flag_of_the_Islamic_State_of_Iraq_and_the_Levant2.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Islamic_State_of_Iraq_and_the_Levant2.svg "fig:Flag_of_the_Islamic_State_of_Iraq_and_the_Levant2.svg")
    [伊拉克和沙姆伊斯蘭國](../Page/伊拉克和沙姆伊斯蘭國.md "wikilink")\[73\]

## 參考

<references/>

  - [UB航空機槍](../Page/UB航空機槍.md "wikilink")
  - [NSV重機槍](../Page/NSV重機槍.md "wikilink")
  - [KPV重機槍](../Page/KPV重機槍.md "wikilink")
  - [白朗寧M2重機槍](../Page/白朗寧M2重機槍.md "wikilink")（[美国相同用途的重機槍](../Page/美国.md "wikilink")）

## 外部連結

  - \-[guns.ru的DShK及DShKM介紹](https://web.archive.org/web/20050408063852/http://world.guns.ru/machine/mg03-e.htm)

[Category:重機槍](../Category/重機槍.md "wikilink")
[Category:12.7×108毫米槍械](../Category/12.7×108毫米槍械.md "wikilink")
[Category:俄羅斯槍械](../Category/俄羅斯槍械.md "wikilink")
[Category:蘇聯槍械](../Category/蘇聯槍械.md "wikilink")
[Category:蘇聯二戰武器](../Category/蘇聯二戰武器.md "wikilink")
[DShK](../Category/二戰機槍.md "wikilink")
[Category:越戰武器](../Category/越戰武器.md "wikilink")
[Category:韓戰武器](../Category/韓戰武器.md "wikilink")

1.

2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14. Gander, Terry J.; Hogg, Ian V. *Jane's Infantry Weapons 1995/1996*.
    Jane's Information Group; 21 edition (May 1995). ISBN
    978-0-7106-1241-0.

15.
16.
17.
18.
19.
20.
21.
22.
23.
24.
25.
26.
27.
28.
29.

30.
31.
32.
33.
34.
35.
36.
37.
38.
39.
40.
41.
42.
43.
44.
45.
46.
47.
48.
49.
50.
51.
52.
53.
54.
55.
56.
57.
58.
59.
60.
61.
62.

63.
64.
65.
66.
67.
68.
69.

70. [uncovering the irish republican
    army](http://www.pbs.org/wgbh/pages/frontline/shows/ira/inside/weapons.html)
    pbs.org

71.
72. [Al-Jazeera coverage of Libyan
    uprising](http://www.youtube.com/watch?v=Jr1tm9uMFq4&feature=related)
    Youtube.com

73.