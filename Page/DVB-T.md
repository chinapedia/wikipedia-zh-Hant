**地面數碼視訊廣播**（，縮寫：），是[歐洲廣播聯盟在](../Page/歐洲廣播聯盟.md "wikilink")1997年\[1\]發佈的[數碼地面電視視訊廣播傳輸](../Page/數碼電視.md "wikilink")，最早是1998年在[英國實行廣播](../Page/英國.md "wikilink")。

2007年，歐洲DVB組織推出改良版的[DVB-T2地面數碼電視廣播標準](../Page/DVB-T2.md "wikilink")，頻譜利用率及有效傳輸碼率得到較大提高。
DVB-T2先從沒有採用DVB-T地面數碼電視廣播模式的第三世界開始進行推廣，已逐步完成產業化，價格大幅度下降，隨後歐洲很多國家也開始採用，逐步替換DVB-T。

DVB-T2亮點是能提供較高的系統淨荷碼率（最大淨荷碼率高達51Mbps，比目前有線及衛星數碼電視能提供的單通道淨荷碼率還高），採用分集接收改進了單頻網接收效果，接收門限更低等。

## 使用DVB-T與DVB-T2的地區

[Digital_broadcast_standards.svg](https://zh.wikipedia.org/wiki/File:Digital_broadcast_standards.svg "fig:Digital_broadcast_standards.svg")

## 參考文獻

## 外部連結

  -

[Category:數碼電視](../Category/數碼電視.md "wikilink")
[Category:高清晰度電視](../Category/高清晰度電視.md "wikilink")
[Category:廣播工程](../Category/廣播工程.md "wikilink")
[Category:公開標準](../Category/公開標準.md "wikilink")

1.