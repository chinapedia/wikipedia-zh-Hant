[DVD-Regions_with_key-2.svg](https://zh.wikipedia.org/wiki/File:DVD-Regions_with_key-2.svg "fig:DVD-Regions_with_key-2.svg")
**數碼影碟區域碼限制**，俗稱**DVD區域碼**，即最主要就是保障每個[地區的影音產品經銷商與代理商的權益](../Page/地區.md "wikilink")，而專為[DVD-Video所制定出的](../Page/DVD-Video.md "wikilink")「限定在某區域內才能正常使用」的[區域碼限制](../Page/區域碼.md "wikilink")，用以杜絕產品[平行輸入的問題](../Page/平行輸入.md "wikilink")，避免某區域未上映的[電影的](../Page/電影.md "wikilink")[票房收入因為其DVD](../Page/票房.md "wikilink")-Video的流通而造成損失。

## 概述

[DVD播放機與DVD](../Page/DVD播放機.md "wikilink")-Video的區域碼限制，是由[美國八大影業](../Page/美國.md "wikilink")（[華特迪士尼公司](../Page/華特迪士尼公司.md "wikilink")、[索尼影視娛樂](../Page/索尼影視娛樂.md "wikilink")、[派拉蒙電影公司](../Page/派拉蒙電影公司.md "wikilink")、[環球影業](../Page/環球影業.md "wikilink")、[華納兄弟](../Page/華納兄弟.md "wikilink")、[二十世紀福斯](../Page/二十世紀福斯.md "wikilink")、[米高梅](../Page/米高梅.md "wikilink")、[聯美](../Page/聯美.md "wikilink")）制定，將DVD播放機與DVD-Video區分成九個區域碼來製造與發行。

大多數的DVD播放機只會有一個區域碼，大多數的美國八大影業成員發行的DVD-Video只會有一個區域碼。但是，有些DVD-Video會有兩個以上的區域碼（例如第一區至第三區）。另外，有些非美國八大影業成員發行的DVD-Video沒有區域碼。若想用只有一個區域碼的DVD播放機或光驱播放其他區域碼的DVD-Video，則必須透過[破解芯片](../Page/破解芯片.md "wikilink")、软件破解（如VLC播放器）或用[遙控器輸入一些按鍵來解除DVD播放機本身的](../Page/遙控器.md "wikilink")[區域限制](../Page/區域限制.md "wikilink")，也有厂商生产了能播放所有区域DVD的影碟机。DVD[光碟機通常可重設DVD區域碼](../Page/光碟機.md "wikilink")5次，完成第5次後即固定區域碼而無法再重設。

## 區域碼

目前DVD區域碼總共可以區分為十個不同的區域：

<table>
<thead>
<tr class="header">
<th><p>colspan = "2"| DVD區域碼限制</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>0</p></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
</tr>
<tr class="odd">
<td><p>ALL</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

## 相關條目

  - [DVD-Video](../Page/DVD-Video.md "wikilink")
  - [DVD播放機](../Page/DVD播放機.md "wikilink")
  - [DeCSS](../Page/DeCSS.md "wikilink")
  - [藍光光碟區域碼](../Page/藍光光碟#區碼.md "wikilink")

## 外部链接

  - [DVD region information with regards to
    RCE](http://www.hometheaterinfo.com/dvd3.htm) from Home Theater Info
  - [The World's Largest DVD Codes
    Archive](https://web.archive.org/web/20170912031706/http://alldvdcodes.com/)

[de:DVD-Video\#Regionalcode](../Page/de:DVD-Video#Regionalcode.md "wikilink")
[fi:DVD\#Aluekoodit](../Page/fi:DVD#Aluekoodit.md "wikilink")
[nl:Dvd\#Regiocode](../Page/nl:Dvd#Regiocode.md "wikilink")

[Category:知識產權](../Category/知識產權.md "wikilink")
[Category:DVD](../Category/DVD.md "wikilink")
[Category:數位版權管理](../Category/數位版權管理.md "wikilink")