[IFA_2005_Toshiba_HBS_A_001_HD-DVD_Player_(Dual-Layer_HD-DVD_30GB)_and_(DVD-HD-DVD-Twin-Disc_5GB_15GB)_(by_HDTVTotalDOTcom).jpg](https://zh.wikipedia.org/wiki/File:IFA_2005_Toshiba_HBS_A_001_HD-DVD_Player_\(Dual-Layer_HD-DVD_30GB\)_and_\(DVD-HD-DVD-Twin-Disc_5GB_15GB\)_\(by_HDTVTotalDOTcom\).jpg "fig:IFA_2005_Toshiba_HBS_A_001_HD-DVD_Player_(Dual-Layer_HD-DVD_30GB)_and_(DVD-HD-DVD-Twin-Disc_5GB_15GB)_(by_HDTVTotalDOTcom).jpg")
[Opened_dvd-player.jpg](https://zh.wikipedia.org/wiki/File:Opened_dvd-player.jpg "fig:Opened_dvd-player.jpg")
[Macrovision_in_WaveFormMonitor.jpg](https://zh.wikipedia.org/wiki/File:Macrovision_in_WaveFormMonitor.jpg "fig:Macrovision_in_WaveFormMonitor.jpg")
**數位光碟播放機**（），俗稱**DVD播放機**，即播放[數碼多功能影音光碟的設備](../Page/數碼多功能影音光碟.md "wikilink")。使用方式大多數皆需要连接到[電視機上](../Page/電視機.md "wikilink")。除此之外，還有內含[液晶顯示器式的產品](../Page/液晶顯示器.md "wikilink")。

## 基本功能

DVD播放机的基本功能如下：

  - 读取[DVD-Video光盘](../Page/DVD-Video.md "wikilink")（[UDF版本](../Page/通用光碟格式.md "wikilink")2格式）。
  - 随意解密经过[CSS或](../Page/内容扰乱系统.md "wikilink")[Macrovision处理过的内容](../Page/Macrovision.md "wikilink")。
  - 解码[MPEG-2视频流](../Page/MPEG-2.md "wikilink")，最高峰值速率10Mbit/s，连续速率8
    Mbit/s。
  - 解码声音格式编码[MP2](../Page/MP2\(格式\).md "wikilink")、[PCM或](../Page/PCM.md "wikilink")[AC-3](../Page/AC-3.md "wikilink")，通过立体声连接器[光纤或者电子数字连接器输出](../Page/光纤.md "wikilink")。
  - 输出视频信号，其中模拟信号（[PAL](../Page/PAL.md "wikilink")、[SECAM或](../Page/SECAM.md "wikilink")[NTSC格式](../Page/NTSC.md "wikilink")）使用[彩色视频信号连接器](../Page/彩色视频信号.md "wikilink")，数字信号使用[数字视频接口连接器](../Page/数字视频接口.md "wikilink")。

大多数DVD播放机也能播放音频[CD](../Page/CD.md "wikilink")（[CDDA](../Page/CDDA.md "wikilink")、[MP3等](../Page/MP3.md "wikilink")）和[VCD](../Page/VCD.md "wikilink")，包含一个[家庭影院解码器如](../Page/家庭影院.md "wikilink")[Dolby
Digital](../Page/Dolby_Digital.md "wikilink")、[DTS](../Page/DTS.md "wikilink")；一些新推出的产品还支持[互联网上流行的视频压缩格式](../Page/互联网.md "wikilink")[DivX](../Page/DivX.md "wikilink")。

## 生產國家

最大的DVD播放機生產國是[中國大陸](../Page/中國大陸.md "wikilink")，2002年共生產3000萬部，[世界](../Page/世界.md "wikilink")[市場佔有率](../Page/市場佔有率.md "wikilink")70%。每部DVD播放機大約需要支付[MPEG-2授權費](../Page/MPEG-2.md "wikilink")20[美元給DVD](../Page/美元.md "wikilink")[專利技術持有者](../Page/專利.md "wikilink")（[索尼](../Page/索尼.md "wikilink")、[飛利浦](../Page/飛利浦.md "wikilink")、[先鋒和](../Page/先鋒公司.md "wikilink")[LG](../Page/LG.md "wikilink")）。為避免這一情況，中國大陸為下一代DVD開發了自主[知識產權的](../Page/知識產權.md "wikilink")[EVD標準](../Page/增強型通用光盤.md "wikilink")。到2004年為止，EVD播放機只在中國大陸銷售。

## DVD區域碼

即最主要就是保障每個地區經銷商與代理商的權益，而專為DVD-Video所制定出的「限定在某區域內才能正常使用」的限制，用以杜絕產品[平行輸入的問題](../Page/平行輸入.md "wikilink")，避免某區域未上映的[電影的](../Page/電影.md "wikilink")[票房](../Page/票房.md "wikilink")[收入因為其DVD](../Page/收入.md "wikilink")-Video的流通而造成損失。但現今有許多[電腦軟體甚至播放機擁有對不同區域的DVD解碼的功能](../Page/電腦軟體.md "wikilink")，使得區碼的限制愈來愈小。

## DVD播放軟體

DVD播放軟體是[電腦上借助](../Page/電腦.md "wikilink")[DVD驱动器播放DVD视频的](../Page/DVD驱动器.md "wikilink")[软件](../Page/软件.md "wikilink")。如：[VLC
media
player](../Page/VLC_media_player.md "wikilink")、[MPlayer](../Page/MPlayer.md "wikilink")、[DVD播放程式](../Page/DVD播放程式.md "wikilink")、[WinDVD](../Page/WinDVD.md "wikilink")、[PowerDVD](../Page/PowerDVD.md "wikilink")、[Media
Player Classic](../Page/Media_Player_Classic.md "wikilink")。

## 相關條目

  - [CD](../Page/CD.md "wikilink")
  - [VCD](../Page/VCD.md "wikilink")
  - [DVD](../Page/DVD.md "wikilink")
  - [CD-ROM](../Page/CD-ROM.md "wikilink")

[pl:DVD\#Odtwarzacz DVD](../Page/pl:DVD#Odtwarzacz_DVD.md "wikilink")

[Category:DVD](../Category/DVD.md "wikilink")
[Category:消費電子產品](../Category/消費電子產品.md "wikilink")