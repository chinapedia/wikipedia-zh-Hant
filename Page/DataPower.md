**DataPower**是一家[IBM公司所屬的公司](../Page/IBM.md "wikilink")，該公司主要在於研製與銷售[XML應用機](../Page/XML應用機.md "wikilink")（[XML
appliance](../Page/XML應用機.md "wikilink")），此種設備專門用來執行與處理[XML訊息](../Page/XML.md "wikilink")。DataPower也是第一家針對[XSLT加速處理而為產品](../Page/XSLT.md "wikilink")[研製及運用](../Page/研製.md "wikilink")[特定應用積體電路](../Page/特定應用積體電路.md "wikilink")（[application-specific
integrated circuit](../Page/特殊應用積體電路.md "wikilink")，簡稱：ASIC）的業者。

DataPower很早即將自己定位在「XML處理」領域的創新業者，XML訊息如今已經成為[面向服务的体系结构](../Page/面向服务的体系结构.md "wikilink")（Service
oriented architecture，簡稱：SOA）中的一大要件。

## DataPower歷史

  - 2005年10月－由IBM公司所收併。
  - 2003年1月－首次公開發表，強調在效能之上的安全。
  - 2002年8月26日－交付第一台網路設備。
  - 2002年7月8日 由創投業者投資950萬美元。
  - 2002年4月－[Cheng Wu](../Page/程武.md "wikilink")（諧音：吳成）出任董事會總監。
  - 1999年3月－成立於[麻州的](../Page/麻州.md "wikilink")[坎布里奇](../Page/坎布里奇.md "wikilink")。

有關完整的活動、事件列表，請見DataPower的[發佈消息](http://www.datapower.com/newsroom/index.html)。

## 關連項目

  - [XML應用機](../Page/XML應用機.md "wikilink")

  - [XML appliance](../Page/XML應用機.md "wikilink")

  - －2005年6月[Intel公司所收購的一家公司](../Page/Intel.md "wikilink")。

  - [XSLT](../Page/XSLT.md "wikilink")

  - [WS-Security](../Page/WS-Security.md "wikilink")

## 外部連結

  - [DataPower官方網站](http://www.datapower.com)
  - [IBM
    DataPower網頁](http://www-306.ibm.com/software/integration/datapower/index.html)

[Category:信息系统](../Category/信息系统.md "wikilink")
[Category:電腦整合](../Category/電腦整合.md "wikilink")