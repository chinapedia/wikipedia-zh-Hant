**DeCSS**是一个用于解密[DVD碟片的](../Page/數碼多功能影音光碟.md "wikilink")[自由软件](../Page/自由软件.md "wikilink")。在DeCSS发布之前，基于Linux的电脑系统都无法播放DVD视频。

在开发过程中，DeCSS没有获得来自DVD Copy Control Association（DVD
CCA）的许可，而这个组织则负责[内容扰乱系统](../Page/內容擾亂系統.md "wikilink")（CSS）等用于DVD[数字版权管理的系统](../Page/数字版权管理.md "wikilink")。DeCSS发布之后，其作者之一在挪威被控刑事犯罪，数年后才获判无罪。为了阻止DeCSS的传播，DVD
CCA还在美国发起了一系列诉讼。

## 來源與歷史

[DeCSS.PNG](https://zh.wikipedia.org/wiki/File:DeCSS.PNG "fig:DeCSS.PNG")

  - 1999年10月：DeCSS最初由不知名人士從[網際網路的LiViD](../Page/網際網路.md "wikilink")（Linux
    Video and DVD）mailing list發表。
  - 2000年：程式開發者之一的[挪威](../Page/挪威.md "wikilink")15歲少年[約恩·萊克·約翰森](../Page/約恩·萊克·約翰森.md "wikilink")（Jon
    Lech Johansen，又稱「[DVD
    Jon](../Page/DVD_Jon.md "wikilink")」）（三名作者中唯一公開者）遭挪威警方搜索其住家，並且被起訴，面臨可能的兩年[有期徒刑及高額](../Page/有期徒刑.md "wikilink")[罰金](../Page/罰金.md "wikilink")。
  - 2003年初：Jon Lech Johansen獲判無罪。
  - 2003年3月5日：挪威上訴法庭以Jon Lech
    Johansen觸犯[挪威刑法的](../Page/挪威刑法.md "wikilink")「[駭客防治條款](../Page/駭客.md "wikilink")」（Norwegian
    Criminal Code section 145 (the hacker
    law)）再審此案，理由是[檢察官提出新的](../Page/檢察官.md "wikilink")[證人](../Page/證人.md "wikilink")。
  - 2003年12月22日：Jon Lech Johansen再度獲判無罪。
  - 2004年1月5日：挪威政府的經濟與環保犯罪調查與起訴機關宣佈不再追究此案。

## 技術資訊

通过DeCSS的源码，公众首次得以接触CSS的算法。不久之后，有人发现除DeCSS使用的破解方法之外，还可以用[暴力破解法攻破CSS的加密](../Page/暴力破解法.md "wikilink")。CSS的密钥长度只有40位，并且加密算法没有完全利用密钥。在1999年，高端家用電腦经过优化后在一天內就可以暴力破解CSS，而现代电脑甚至能在数秒内完成破解。\[1\]

此后，许多程序员又开发了类似DeCSS的程序。这些程序中，一些只是为了演示如何轻松绕过CSS系统，还有一些则让开源视频播放器支持了DVD。由于CSS的授权限制，通过官方渠道获取的信息无法用于开发开源软件，而闭源驱动又不支持所有系统，所以甚至有些正版用户也需要使用DeCSS来播放影片。

## 法律行动

1999年11月，第一个针对提供DeCSS下载的网站的法律威胁出现，引起了镜像DeCSS的运动。次年1月，在另一起官司中法院签署了初步禁令。作为对法律威胁的回应，社区开发了一个不相关的同名程序，其功能是从HTML页面中过滤CSS标签。有次一家学校误把这个程序当作原版DeCSS，移除了包含此程序的学生页面，受到了大量的负面媒体关注，而社区开发这个程序就是为了通过这种形式让[MPAA上钩](../Page/美國電影協會.md "wikilink")。\[2\]

[世界知识产权组织版权条约的契约国都立法禁止了可用于绕过版权保护的软件](../Page/世界知识产权组织.md "wikilink")，而一些社区成员则提议了各种分发DeCSS算法的方法作为抗议。这些方法包括[隐写术](../Page/隐写术.md "wikilink")、多种[互联网协议](../Page/网际协议.md "wikilink")、T恤衫、[MIDI文件](../Page/MIDI.md "wikilink")、俳句\[3\]等，甚至还有所谓的[非法质数](../Page/非法質數.md "wikilink")\[4\]。

## 參考資料

## 外部連結

[Category:DVD](../Category/DVD.md "wikilink")
[Category:開放原始碼](../Category/開放原始碼.md "wikilink")
[Category:密碼學法律](../Category/密碼學法律.md "wikilink")

1.
2.
3.
4.