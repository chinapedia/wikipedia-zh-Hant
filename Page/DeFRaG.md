**Defrag**是一个[雷神之锤III的](../Page/雷神之锤III.md "wikilink")[自由软件](../Page/自由软件.md "wikilink")[MOD](../Page/游戏模组.md "wikilink")。利用其[游戏引擎的特殊物理性质进行速度和跳跃比赛的游戏模式](../Page/游戏引擎.md "wikilink")。玩家在各式各样的地图中可以利用各种跳跃方式进行加速或者特殊移动，从而达到使速度变快或者做出不规则路线运动的目的。尽管多数进行在专用地图上，标准的CTF地图也可以最快夺旗模式使用。目前Defrag的最新版本是1.92.01
/ 2009-07-09发布。

## 外部链接

  - [官方主页](http://cggdev.org/)
  - [原来位于PlanetQuake的官方网站](https://web.archive.org/web/20050818201415/http://www.planetquake.com/defrag/)

[Category:雷神之锤系列](../Category/雷神之锤系列.md "wikilink")
[Category:第一人称动作游戏](../Category/第一人称动作游戏.md "wikilink")