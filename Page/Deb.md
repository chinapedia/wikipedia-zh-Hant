[Gdebi.png](https://zh.wikipedia.org/wiki/File:Gdebi.png "fig:Gdebi.png")\]\]
**deb**是[Debian](../Page/Debian.md "wikilink")[软件包格式](../Page/软件包.md "wikilink")，[文件扩展名为](../Page/文件扩展名.md "wikilink")**.deb**，跟*Debian*的命名一样，deb也是因Debra
Murdock（Debian创始人[Ian
Murdock的前妻](../Page/Ian_Murdock.md "wikilink")）而得名。

Debian包是[Unix](../Page/Unix.md "wikilink")[ar的标准归档](../Page/ar.md "wikilink")，将包文件信息以及包内容，经过[gzip和](../Page/gzip.md "wikilink")[tar打包而成](../Page/Tar_\(计算机科学\).md "wikilink")。

处理这些包的经典程序是[dpkg](../Page/dpkg.md "wikilink")，经常是通过[apt来运作](../Page/apt.md "wikilink")。

通过[Alien工具](../Page/Alien.md "wikilink")，可以将deb包转换成其他形式的[软件包](../Page/软件包.md "wikilink")。

## 實做

deb 檔是使用 tar 包裝，包含了三個檔案：

  - debian-binary - deb 格式版本號碼
  - control.tar.gz -
    包含套件的[元数据](../Page/元数据.md "wikilink")，如包名称、版本、维护者、依赖、冲突等等。
    <small>在dpkg 1.17.6 之后添加了对 [xz](../Page/xz.md "wikilink") 压缩和不压缩的
    control 元数据的支持。\[1\]</small>
  - data.tar.\* - 實際安裝的內容

其中，"\*"所指代的内容随压缩算法不同而不同。常见的可能值为[xz](../Page/xz.md "wikilink")、[gz](../Page/gzip.md "wikilink")、或[bz2](../Page/bzip2.md "wikilink")。有时也会使用[lzma](../Page/LZMA.md "wikilink")。

## 參見

  - [歸檔格式列表](../Page/歸檔格式列表.md "wikilink")

## 外部链接

  - [Debian
    FAQ：Debian包管理系统基础](http://www.debian.org/doc/FAQ/ch-pkg_basics)

[Category:归档格式](../Category/归档格式.md "wikilink")
[Category:Dpkg](../Category/Dpkg.md "wikilink")
[Category:扩展名](../Category/扩展名.md "wikilink")
[Category:Ubuntu](../Category/Ubuntu.md "wikilink")
[Category:Debian](../Category/Debian.md "wikilink")

1.  [dpkg
    changelog](http://metadata.ftp-master.debian.org/changelogs/main/d/dpkg/unstable_changelog)