**Deluge**是一个功能齐全的跨平台[BitTorrent](../Page/BitTorrent_\(协议\).md "wikilink")
[客户端软件](../Page/客户端.md "wikilink")，可在[Linux](../Page/Linux.md "wikilink"),
[OS X](../Page/OS_X.md "wikilink"),
[Unix和](../Page/Unix.md "wikilink")[Windows操作系统下工作](../Page/Windows.md "wikilink")。它使用[libtorrent作为其后端](../Page/libtorrent.md "wikilink")，有包括[GTK+](../Page/GTK+.md "wikilink")，网络远程客户端，命令行模式等多种用户界面。其设计方针是体积小巧且节约系统资源，通过丰富的[插件来实现核心以外的众多功能](../Page/插件.md "wikilink")。Deluge响应[Freedesktop.org的倡议](../Page/Freedesktop.org.md "wikilink")，兼容于[GNOME](../Page/GNOME.md "wikilink"),
[KDE](../Page/KDE.md "wikilink"),
[XFCE和其它多种桌面环境](../Page/XFCE.md "wikilink")。它还是一款[自由软件](../Page/自由软件.md "wikilink")，使用[GPLv3进行授权](../Page/GNU通用公共许可证#GPLv3.md "wikilink")。\[1\]

## 历史

Deluge由 ubuntuforum.org 网站的两位成员 Zach Tibbitts和Alan
Zakai建立。在创立之初项目托管曾在[Google
Code之上](../Page/Google_Code.md "wikilink")，之后才建立了自己的网站。

在早期的开发过程中，Deluge曾经被命名为gTorrent，意思是[GNOME桌面环境下的BitTorrent客户端](../Page/GNOME.md "wikilink")。在第一个版本在2006年9月25日释出时，项目被重新命名为Deluge，以避免别人误会gTorrent只能用於GNOME。

利用0.4.x代码重写的0.5穏定版在2007年3月18日释出。在0.5.x版本中，全新支持了[数据加密](../Page/数据加密.md "wikilink")，[Peer
exchange](../Page/Peer_exchange.md "wikilink")，[Si
prefixes和](../Page/Binary_prefix.md "wikilink")[UPnP](../Page/UPnP.md "wikilink")。

## 基本功能

  - 种子建立
  - 插件支持
  - [UPnP和](../Page/UPnP.md "wikilink")[NAT-PMP网络支持](../Page/NAT-PMP.md "wikilink")
  - 数据加密

Deluge支持下列网络功能：

  - DHT支持
  - uTorrent种子交换
  - BitTorrent协定加密
  - UPnP和NAT-PMP
  - 代理支持
  - 私人种子
  - 各种限速
  - RSS

另外，Deluge还支持下列功能：

  - 在一个窗口内下载多个文件
  - 预置文件空间
  - 全局和个别文件速度限制
  - 下载个别文件
  - 文件预览
  - 选择下载目录
  - 使用排队来管理
  - 可以在达到指定的上下载比例後停止上载
  - 可最小化到系统列并指定密码保护

Deluge支持插件系统，包括下面的插件，现在标准发布包含所有的插件：

  - Blocklist Importer
  - Desired Ratio
  - Extrastats
  - Locations
  - Network Activity Graph
  - Network Health Monitor
  - RSS Broadcatcher
  - Torrent Creator
  - Torrent Notification
  - Torrent Search

## 参见

  - [BitTorrent](../Page/BitTorrent.md "wikilink")
  - [BitTorrent软件](../Page/BitTorrent软件.md "wikilink")

## 外部链接

  - [Deluge网站](http://deluge-torrent.org)
  - [Deluge论坛](http://forum.deluge-torrent.org)

### 参与

  - [Deluge开发计划首页](https://launchpad.net/deluge)→Deluge官方使用[Launchpad提供的开发平台](../Page/Launchpad.md "wikilink")
  - [Deluge翻译列表](https://translations.launchpad.net/deluge)→Deluge官方使用Launchpad提供的翻译平台

## 引用

[Category:跨平台软件](../Category/跨平台软件.md "wikilink")
[Category:自由软件](../Category/自由软件.md "wikilink")
[Category:BitTorrent客户端](../Category/BitTorrent客户端.md "wikilink")
[Category:自由跨平台軟體](../Category/自由跨平台軟體.md "wikilink")
[Category:自由檔案分享軟體](../Category/自由檔案分享軟體.md "wikilink")

1.