[Hinomaru_EV-02_Metro_Link_Nihonbashi_LYTTELTON_2004.jpg](https://zh.wikipedia.org/wiki/File:Hinomaru_EV-02_Metro_Link_Nihonbashi_LYTTELTON_2004.jpg "fig:Hinomaru_EV-02_Metro_Link_Nihonbashi_LYTTELTON_2004.jpg")
**Designline**是[紐西蘭的客車製造廠商之一](../Page/紐西蘭.md "wikilink")，產品包括[公交車及城際客車](../Page/公共汽車.md "wikilink")。公司廠房設於[愛師伯頓鎮](../Page/愛師伯頓.md "wikilink")，為當地生產的車輛可於[奧克蘭](../Page/奧克蘭_\(紐西蘭\).md "wikilink")、[漢密爾頓](../Page/汉密尔顿_\(新西兰\).md "wikilink")、[威靈頓](../Page/威靈頓.md "wikilink")、[基督城](../Page/基督城.md "wikilink")、[但尼丁等地方找到](../Page/但尼丁.md "wikilink")。

除紐西蘭的營運商外，Designline的產品也有出口往[澳洲](../Page/澳洲.md "wikilink")、[亞洲及](../Page/亞洲.md "wikilink")[歐洲地區](../Page/歐洲.md "wikilink")。

Designline預計於未來隨著業務擴充，車輛產量將為現時的三倍，每星期預計能生產15輛巴士。

## 城市公交車

Designline是全紐西蘭最大型的公交車製造廠商，起初產品為有登車梯級的巴士，至1996年推出特低地台巴士，其無梯級設計可使輪椅及推嬰兒車的乘客更容易登車。零件供應方面，不少Designline車輛均使用了[猛獅車廠](../Page/猛獅.md "wikilink")（MAN）的底盤及其他零件，除此之外，這些車輛也使用[世冠](../Page/世冠汽車.md "wikilink")（Scania）、[富豪巴士](../Page/富豪集團.md "wikilink")（Volvo
Buses）及Designline原廠等零件，現時大多數產品均以MAN 12.223及MAN
17.223型號為主。營運商方面，[紐西蘭捷達巴士](../Page/紐西蘭捷達巴士.md "wikilink")（Stagecoach
New Zealand）現擁有最多Designline公交車。

## 混合動力巴士

Designline現已開發及生產出柴電[混合動力巴士](../Page/混合動力車輛.md "wikilink")，型號為“Olymbus”。現時紐西蘭當地有7輛柴電巴士行走[免費](../Page/免費.md "wikilink")[穿梭巴士線](../Page/穿梭巴士.md "wikilink")，當中有4輛於基督城行走，3輛於奧克蘭行走。除此之外，這些柴電巴士也出口往[日本](../Page/日本.md "wikilink")、[香港及](../Page/香港.md "wikilink")[英國](../Page/英國.md "wikilink")。

香港[珀麗灣客運曾於](../Page/珀麗灣客運.md "wikilink")2003年引入3輛Designline混合動力巴士。

## 關連項目

  - [五洲龍客車](../Page/五洲龍.md "wikilink")

## 外部連結

  - [Designline官網](http://www.designline.co.nz)
  - [有關Designline授權車身設計給五洲龍客車在國內生產巴士事而](http://www.hkitalk.net/HKiTalk2/viewthread.php?tid=274742&extra=page%3D1&page=2)

[Category:巴士生產商](../Category/巴士生產商.md "wikilink")
[Category:新西蘭公司](../Category/新西蘭公司.md "wikilink")