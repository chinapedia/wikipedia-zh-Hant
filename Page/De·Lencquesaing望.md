**de・Lencquesaing望，ド・ランクザン望**（ド・ランクザン のぞみ、本名：**ド・ランクザン トーマ 望**=**De
Lencquesaing Thomas
Nozomi**、）是日本的[童星](../Page/童星.md "wikilink")[人才](../Page/人才.md "wikilink")。出身於日本[東京都](../Page/東京都.md "wikilink")[文京區](../Page/文京區.md "wikilink")[本鄉](../Page/本鄉.md "wikilink")。是[法國人與日本人的混血兒](../Page/法國.md "wikilink")。[血型是](../Page/血型.md "wikilink")[O型](../Page/O型.md "wikilink")。身高179公分。屬於[Stardust
Promotion的一員](../Page/Stardust_Promotion.md "wikilink")。

## 人物

由2003年度開始到2005年度在『[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")』（[NHK教育頻道](../Page/NHK教育頻道.md "wikilink")）裡作為[TV戰士而演出](../Page/TV戰士.md "wikilink")。因為擅長製造氣氛而受到注目。2005年度中與[前田公輝](../Page/前田公輝.md "wikilink")・[飯田里穗](../Page/飯田里穗.md "wikilink")・[村田千宏作為最年長的戰士而擔任星期四現場直播的經常性主持人](../Page/村田千宏.md "wikilink")。在[片尾曲MTK裡](../Page/片尾曲MTK.md "wikilink")，與村田千寻・[橋本甜歌](../Page/橋本甜歌.md "wikilink")・[木內梨生奈一起組成](../Page/木內梨生奈.md "wikilink")
[小马戏团而活躍](../Page/小马戏团.md "wikilink")。在公演『拯救優克迪爾\!TV戰士史上最大的危機』（ユゲデールを救え\!てれび戦士
史上最大の危機）中、與[篠原愛實一起擔任實際上的主角](../Page/篠原愛實.md "wikilink")。

雖然父親是法國人，且家庭出身自法國的貴族，但他不會說法語。不過由於他在國際學校上學的關係，所以他英語說得很好。

似乎很擅長表演笑料、但無法確定是否真的是如此。代表笑料是「よーい！ハイソックス！ハイソックス！左のハイソックス！」。

[自戀型的角色](../Page/自戀.md "wikilink")、在2004年的人物介紹欄是寫著『自稱天才軍師』。

在2006年天才兒童MAX夏季的公演中，與村田千寻,-{飯田里穗}-，前田公輝一起參觀。

在2003年度與[前田公輝](../Page/前田公輝.md "wikilink")・[山元竜一](../Page/山元竜一.md "wikilink")・[ブライアン・ウォルターズ](../Page/ブライアン・ウォルターズ.md "wikilink")・[マイケル・メンツァー一起在](../Page/Michael\(talent\).md "wikilink")「なりきり\!シンガーズ」的暑假特別編演出、模仿成[SMAP的](../Page/SMAP.md "wikilink")[香取慎吾](../Page/香取慎吾.md "wikilink")（同時也組成了[SMAX](../Page/SMAX.md "wikilink")），熱情演唱[SMAP的暢銷曲](../Page/SMAP.md "wikilink")《[世界上唯一的花](../Page/世界上唯一的花.md "wikilink")》，並由當時擔任[SMAP舞蹈指導師](../Page/SMAP.md "wikilink")\[的[KABA.ちゃん指導舞蹈](../Page/KABA.ちゃん.md "wikilink")。

## 雜誌

  - STREET JACK
  - GET ON

## 關聯項目

  - [井出卓也](../Page/井出卓也.md "wikilink")
  - [堀江幸生](../Page/堀江幸生.md "wikilink")
  - [前田公輝](../Page/前田公輝.md "wikilink")
  - \-{[飯田里穗](../Page/飯田里穗.md "wikilink")}-
  - [村田千宏](../Page/村田千宏.md "wikilink")
  - [笠原拓巳](../Page/笠原拓巳.md "wikilink")

## 外部連結

  - [官方個人簡介](http://www.stardust.co.jp/talent/114.html)
  - [スターダストプロモーション](http://www.stardust.co.jp/)

[Category:TV戰士](../Category/TV戰士.md "wikilink")