《**Di-Dar**》是[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")[王菲的第](../Page/王菲.md "wikilink")12張大碟、第9張[粵語專輯](../Page/粵語.md "wikilink")（1首[國語](../Page/國語.md "wikilink")），於1995年12月在[香港發行](../Page/香港.md "wikilink")。一共10首歌曲，3首是翻唱。

## 簡介

整張專輯瀰漫著濃厚的英倫迷幻、詭譎氣氛，編曲十分精緻、醇厚，有概念碟的意味。當中有一半曲目為[神遊舞曲風格](../Page/神遊舞曲.md "wikilink")，包括「Di-Dar」、「迷路」和「流星」等。王菲的音樂製作班底到此也已經固定。此張專輯王菲本人貢獻了兩首歌曲，分別是「Di-Dar」和「假期」，林夕則負責了9首歌的歌詞。專輯10首歌的歌名全都是雙字，連沒有題目的「（無題）」也如此。

大碟在香港熱賣，有5首歌登上各大排行榜。特別的是，此張大碟曾一度爬上台灣金曲龍虎榜TOP20，這也成為繼[張學友](../Page/張學友.md "wikilink")《不老的傳說》後，第二張登上龍虎榜的粵語專輯\[1\]。

## 唱片版本

  - [香港版](../Page/香港.md "wikilink")：首版為紙盒裝；再版也為紙盒裝，但歌本與首版有兩處不同，一是第二頁照片少了\[sigal\]，二是文字排版略不同
  - [台灣版](../Page/台灣.md "wikilink")：進口香港再版，加上側標
  - [日本版](../Page/日本.md "wikilink")：初回有兩個版本，區別在碟面，一個是一隻大蝴蝶，一個是一群小蝴蝶
  - [大陸版](../Page/大陸.md "wikilink")：名字改為《迷路》，封面有改動，歌曲順序有改動

其他版本：

  - [卡帶](../Page/卡帶.md "wikilink") [錄音帶版](../Page/錄音帶.md "wikilink")
  - 《從頭認識》版：從頭認識是將王菲在新藝寶一共9張粵語大碟捆綁發售。
  - [DSD版](../Page/DSD.md "wikilink")：2003年5月7日。
  - [SACD版](../Page/SACD.md "wikilink")：2004年9月9日。
  - 環球24K Gold金碟週年紀念經典系列: 2010年10月26日。

## 曲目

## 獎項

香港：

  - [香港電台的](../Page/香港電台.md "wikilink")[十大中文金曲頒獎音樂會](../Page/十大中文金曲頒獎音樂會.md "wikilink")：
      - 1996年度：「十大優秀流行歌手大獎」\[2\]
      - 1996年度：「最佳改編歌曲獎」—「曖昧」\[3\]
  - [無綫電視的](../Page/電視廣播有限公司.md "wikilink")[十大勁歌金曲頒獎典禮](../Page/十大勁歌金曲頒獎典禮.md "wikilink")：
      - 1996年度：「亞太區最受歡迎香港女歌星」\[4\]
  - [商業電台的](../Page/商業電台.md "wikilink")[叱咤樂壇流行榜](../Page/叱咤樂壇流行榜.md "wikilink")：
      - 1996年度：「叱咤樂壇女歌手金獎」\[5\]
      - 1996年度：「叱咤樂壇唱作人大獎」\[6\]
  - [新城電台的](../Page/新城電台.md "wikilink")[新城勁爆頒獎禮](../Page/新城勁爆頒獎禮.md "wikilink")：
      - 1996年度：「亞洲勁爆女歌手」\[7\]
      - 1996年度：「另類歌曲獎」—「Di-Dar」\[8\]
      - 1996年度：「改編歌曲獎」—「曖昧」\[9\]

## 注釋

<div class="references-small" style="-moz-column-count:2; column-count:2;">

<references />

</div>

[Category:王菲音樂專輯](../Category/王菲音樂專輯.md "wikilink")
[Category:1995年音樂專輯](../Category/1995年音樂專輯.md "wikilink")

1.  <http://yesufo.blogcn.com/diary,3437558.shtml>
2.  [香港電台官方網站](http://www.rthk.org.hk/classicschannel/goldsong19.htm)
3.  [香港電台官方網站](http://www.rthk.org.hk/classicschannel/goldsong19.htm)
4.
5.  [商業電台官方網站](http://www1.881903.com/framework/pccs.gateway?url=jsp/lsc/index.jsp&menuID=36&pageURL=/fwcontent/lsc/html/lsc_20021206award1996.htm)

6.  [商業電台官方網站](http://www1.881903.com/framework/pccs.gateway?url=jsp/lsc/index.jsp&menuID=36&pageURL=/fwcontent/lsc/html/lsc_20021206award1996.htm)

7.  [Wikipedia](../Page/:1996年度新城勁爆頒獎禮得獎名單.md "wikilink")
8.  [Wikipedia](../Page/:1996年度新城勁爆頒獎禮得獎名單.md "wikilink")
9.  [Wikipedia](../Page/:1996年度新城勁爆頒獎禮得獎名單.md "wikilink")