**Di Gi
Charat**（；又譯**滴骰孖妹\[1\]**（[香港](../Page/香港.md "wikilink")[無綫電視](../Page/無綫電視.md "wikilink")）、**鈴鐺貓娘**（香港譯名）、**叮噹小魔女**（台灣譯名））是[Broccoli為其角色精品店所做的角色企畫](../Page/Broccoli.md "wikilink")，原作者是[小夏鈍帆](../Page/小夏鈍帆.md "wikilink")。由於其獨特的造型，很快就成為了Broccoli的精品連鎖店[GAMERS的](../Page/GAMERS.md "wikilink")[吉祥物](../Page/吉祥物.md "wikilink")，因大受歡迎亦被多次改編[動畫](../Page/動畫.md "wikilink")、[電子遊戲等](../Page/電子遊戲.md "wikilink")。
[缩略图](https://zh.wikipedia.org/wiki/File:Di_Gi_Charat內頁插圖.jpg "fig:缩略图")

## 登場角色

### 主要角色

  - 數碼子（Dejiko）／數碼卡拉／叮叮
    配音：
    [真田麻美](../Page/真田麻美.md "wikilink")（日版第一代）
    [明坂聰美](../Page/明坂聰美.md "wikilink")（日版第二代）
    [鄭麗麗](../Page/鄭麗麗.md "wikilink")（港）
    [林美秀](../Page/林美秀_\(配音員\).md "wikilink")（台）
    小名Chocola（[法文](../Page/法文.md "wikilink")：意指[巧克力](../Page/巧克力.md "wikilink")），Di
    Gi
    Charat星（數碼星）的公主，有著腹黑的性格。可以從眼睛發射出「」（眼睛射線／眼部[火焰炮](../Page/火.md "wikilink")）。因為懷念母星Di
    Gi Charat而改名為與母星同名。說話時句尾會加上「Nyo」（喵）
  - 布子（Puchiko）／布子卡拉／花花
    配音：
    [澤城美雪](../Page/澤城美雪.md "wikilink")（日版第一代）
    [水上菜緒](../Page/水上菜緒.md "wikilink")（日版第二代）
    [陳凱婷](../Page/陳凱婷.md "wikilink")（港）
    小名Cappuccino（[意大利文](../Page/意大利文.md "wikilink")：意指[咖啡](../Page/咖啡.md "wikilink")），與Dejiko一同來自Di
    Gi
    Charat星，毒舌属性。花花她也算是叮叮的親妹妹。眼部[火焰炮仍在修練中](../Page/火.md "wikilink")，偶而才能發射成功（通常失敗時會射出一些泥巴狀的不明物體）。說話時句尾會加上「Nyu」（喵）。口頭禪是：「笨蛋nyu」
  - 兔田光／拉比安露絲／拉比安[玫瑰](../Page/玫瑰.md "wikilink")／芮恩（Rabi.en.Rose）
    配音：
    [冰上恭子](../Page/冰上恭子.md "wikilink")（日版第一代）
    [矢澤利枝香](../Page/矢澤利枝香.md "wikilink")（日版第二代）
    [陸惠玲](../Page/陸惠玲.md "wikilink")（港）
    [雷碧文](../Page/雷碧文.md "wikilink")（台）
    桃華れん（USAエッチ聲優）
    自稱為Dejiko的競敵，特技為「兔耳螺旋槳」，使頭上的兔耳高速轉動，可於天空飛翔。國中學生的身分與偶像歌手身分相差甚多，變身後總要別人叫她的藝名
    Rabi\~en\~rose，不過叮叮依舊會叫其暱稱芮恩。
  - 初古拉模擬星三世／初子／雛子／沛坦（Piyoko）
    配音：
    [林原惠](../Page/林原惠.md "wikilink")（日）
    [林元春](../Page/林元春.md "wikilink")（港）
    黑色加碼軍團之首，是Analogue星（模擬星）的公主。在[OVA](../Page/OVA.md "wikilink")《Di Gi
    Charat劇場「》一集中，亞海中將的作戰計畫是要初子叫數碼卡拉做姐姐，因此初經常叫數碼卡拉做姐姐。
    可以從口發射出。目標是綁架數位子以向數碼星換取贖[金](../Page/金.md "wikilink")，可是從來沒有成功。說話時句尾會加上「Pyo」（飄）
  - 萬事通/加瑪（配音：[龜井芳子](../Page/龜井芳子.md "wikilink")（日）、[沈小蘭](../Page/沈小蘭.md "wikilink")（港）、[雷碧文](../Page/雷碧文.md "wikilink")（台））
    數碼子的保母，跟著數碼子來到地球。古怪的圓形生物，能在半空飄浮。身體碰到[水時會漲大](../Page/水.md "wikilink")。說話時句尾會加上「Gema」（加碼）

### 黑色加碼軍團（B.G.團）

  -
    首次出場於Summer
    Special，模擬星&數碼星本是朋友，而模擬星是全銀河系中最窮，特別在初子的父王對數碼星發動戰爭而打敗後變的更窮（窮得吃雜草），但初子的父王母后卻走了去溫泉區渡假…叫P.K.O.去照顧初子直到她13歲（共5年），因為太窮了，所以要綁架數碼子來勒索錢
    而P.K.O.則是初古拉模擬星三世旗下的保姆，一律制服為狗耳、尾和醫裝，第一次出場是在聖誕電影特輯
  - 阿陸（Rik Heisenberg）
    配音：
    [鳥海浩輔](../Page/鳥海浩輔.md "wikilink")（日）
    [馮錦堂](../Page/馮錦堂.md "wikilink")（港）
    職位元帥，也是獸醫，為人冷靜，對初子充滿敬意，像父親般對待初子，有時會過份保護她。於適當時候也會讚賞她，令她取回自信心。
    但見到動物便發狂和它做朋友，大部分計畫都是他提出，但要把動物們派上場，於Nyo中他是有一組笛子來召喚動物幫手的。
  - 阿海（Ky Schweitzer）
    配音：
    [鈴木千尋](../Page/鈴木千尋.md "wikilink")（日）
    [袁淑珍](../Page/袁淑珍.md "wikilink")（港）
    職位中將，也是齒科醫生，是PK.O.最值信賴，為人又聰明又有責任感，所有可行的計畫是出自他，是初子的臨時「媽媽」，有點女性化，做事一板一眼，負責B.G.團的家務和早午晚三餐，十分愛護初子，而且注重牙齒健康
    為人補牙時，他總是帶著恐怖的笑臉，初子有時有點怕他……
    而平日帶的牙刷和漱口盅其實是阿海的媽媽的遺物。
  - 阿空（Coo Erhard）
    配音：
    （日）
    [林丹鳳](../Page/林丹鳳.md "wikilink")（港）
    職位少校，也是內科醫生，雖是內科醫生，但沒有見過他醫過人，為人和狗一樣不認真、做事衝動、做事不經大腦，喜歡和初子玩(但玩得十分暴力)，看食物便撲去，像初子的寵物一樣。
    他學醫也是為了初子，因為阿陸和阿海都能用醫術照顧她，而自己不是醫生，所以便花一年學醫。
    他帶的熊貓公仔也是當時初子送的。

### 招財貓商店街的居民

  - 面茶康，（配音：[野島健兒](../Page/野島健兒.md "wikilink")（日）、[黃啟昌](../Page/黃啟昌.md "wikilink")（港））
    24歲，是玩具店「超級玩具店」的店長。
  - 面茶清，（配音：[南央美](../Page/南央美.md "wikilink")（日）、[李致林](../Page/李致林.md "wikilink")（港）、[李明幸](../Page/李明幸.md "wikilink")（台））
    14歲，康的弟弟。
  - 庵衣大福，配音：[菊池正美](../Page/菊池正美.md "wikilink")（日）、[黃子敬](../Page/黃子敬.md "wikilink")（港））
    和菓子「庵頃堂」的店長。
  - 庵衣貴奈子，（配音：[金井美香](../Page/金井美香.md "wikilink")（日）、[區瑞華](../Page/區瑞華.md "wikilink")（港））
    庵衣大福的妻子。
  - 米歇爾宇佐田，配音：[一條和矢](../Page/一條和矢.md "wikilink")（日）、[陳欣](../Page/陳欣_\(配音員\).md "wikilink")（港））
    宇佐田光的父親。
  - 法蘭索娃宇佐田，配音：[岡村明美](../Page/岡村明美.md "wikilink")（日）、[林元春](../Page/林元春.md "wikilink")（港））
    宇佐田光的母親。
  - 錢湯先生，配音：[矢部雅史](../Page/矢部雅史.md "wikilink")（日）、[林保全](../Page/林保全.md "wikilink")（港））
    錢湯「日増湯」的店長，是個長髮的頑固老爹。被稱呼為「爺爺」。
  - 錢湯一麿，配音：[宮崎一成](../Page/宮崎一成.md "wikilink")（日）、[黃榮璋](../Page/黃榮璋.md "wikilink")（港））
    先生的孩子，非常怕生。
  - 熊谷金太郎，配音：[松山鷹志](../Page/松山鷹志.md "wikilink")（日）、[梁志達](../Page/梁志達.md "wikilink")（港））
    是成金趣味的社長，被稱為熊金。
  - 比田井廣，配音：[竹本英史](../Page/竹本英史.md "wikilink")（日）、[黃健強](../Page/黃健強.md "wikilink")（港））
    招財貓商店街町內會長。

## 遊戲資料

由於BROCCOLI不是[遊戲開發商](../Page/遊戲開發商.md "wikilink")，所以出產的遊戲數量並不是很多。

### Di Gi Charat Fantasy：仲夏夜之夢

原名：

  - 遊戲平台

<!-- end list -->

  - [PC](../Page/个人电脑.md "wikilink")／[Dreamcast](../Page/Dreamcast.md "wikilink")／[PlayStation
    2](../Page/PlayStation_2.md "wikilink")

<!-- end list -->

  - 發售日期

<!-- end list -->

  - [PC](../Page/个人电脑.md "wikilink")： 2001年2月22日
  - [Dreamcast](../Page/Dreamcast.md "wikilink")： 2001年9月6日
  - [PlayStation 2](../Page/PlayStation_2.md "wikilink")：2003年11月20日

<!-- end list -->

  - 註備

<!-- end list -->

  - 在PS2推出的「Excellent」版附加OVA的第7-8集故事。

<!-- end list -->

  - 官方網站

<!-- end list -->

  - [Di Gi Charat
    Fantasy：仲夏夜之夢：PS2](http://www.broccoli.co.jp/dejiko/excellent/index.html)

###

  - 遊戲平台

<!-- end list -->

  - [Game Boy Advance](../Page/Game_Boy_Advance.md "wikilink")

<!-- end list -->

  - 發售日期

<!-- end list -->

  - 2002年10月25日

<!-- end list -->

  - 官方網站

<!-- end list -->

  - <http://www.broccoli.co.jp/dejiko/dejicomu/>

###

  - 遊戲平台

<!-- end list -->

  - [Game Boy Advance](../Page/Game_Boy_Advance.md "wikilink")

<!-- end list -->

  - 發售日期

<!-- end list -->

  - 2004年7月15日

<!-- end list -->

  - 官方網站

<!-- end list -->

  - <http://www.broccoli.co.jp/dejiko/digicomu2/index.html>

## 同人遊戲

在2012年和2017年，由R18遊戲公司seismic在PC和Android平台上分別發佈了兩款，以拉比安露絲為主角的R18同人遊戲。遊戲內容主要以第一人稱視角進行，會根據玩家選擇的選項來展現不同的動畫，隨著遊戲的進行，拉比安露絲的好感度提升後，對話的內容也會有所改變。

### USAエッチ

**遊戲平台**

  - [PC](../Page/PC.md "wikilink")

<!-- end list -->

  - 發售日期

<!-- end list -->

  - 2012年4月30日

<!-- end list -->

  - 官方網站

<!-- end list -->

  - [USAエッチ遊戲介紹 seismic官網](http://mud9.ga/43JVq/)

### USAエッチP

**遊戲平台**

  - [Android](../Page/Android.md "wikilink")

<!-- end list -->

  - 發售日期

<!-- end list -->

  - 2017年4月4日

<!-- end list -->

  - 官方網站

<!-- end list -->

  - [USAエッチP遊戲介紹 seismic官網](http://mud9.ga/A0Ifj/)

## 播放電視台

## 參考文獻

## 外部連結

  - [DiGiCharat Official WebSite](http://www.broccoli.co.jp/dejiko/)
  - [BROCCOLI Official Homepage](http://www.broccoli.co.jp/)
  - [大阪電視台的 Digi Charat Nyo頁面](http://www.tv-osaka.co.jp/ip/digicharat/)
  - [BROCCOLI
    內的Di.Gi.Charat動畫PanyoPanyo版介紹](http://www.broccoli.co.jp/dejiko/panyo/)
  - [Di.Gi.Charat Fantasy：仲夏夜之夢
    PS2日本官網](http://www.broccoli.co.jp/dejiko/excellent/index.html)
  - [USAエッチP遊戲介紹 seismic官網](http://mud9.ga/A0Ifj/)
  - [USAエッチ遊戲介紹 seismic官網](http://mud9.ga/43JVq/)

[Category:跨媒體製作](../Category/跨媒體製作.md "wikilink")
[Category:Broccoli](../Category/Broccoli.md "wikilink")
[Category:吉祥物](../Category/吉祥物.md "wikilink")
[Category:1999年日本電視動畫](../Category/1999年日本電視動畫.md "wikilink")
[Category:2001年日本劇場動畫](../Category/2001年日本劇場動畫.md "wikilink")
[Category:2003年東京電視網動畫](../Category/2003年東京電視網動畫.md "wikilink")
[Category:無綫電視外購動畫](../Category/無綫電視外購動畫.md "wikilink")
[Category:公主主角題材動畫](../Category/公主主角題材動畫.md "wikilink")
[Category:美少女遊戲](../Category/美少女遊戲.md "wikilink")
[Category:Dreamcast遊戲](../Category/Dreamcast遊戲.md "wikilink")
[Category:PlayStation 2游戏](../Category/PlayStation_2游戏.md "wikilink")
[Category:科幻動畫](../Category/科幻動畫.md "wikilink")
[Category:月刊Comic電擊大王連載作品](../Category/月刊Comic電擊大王連載作品.md "wikilink")
[Category:少年漫畫](../Category/少年漫畫.md "wikilink")
[Category:週刊少年Champion](../Category/週刊少年Champion.md "wikilink")
[Category:四格漫畫](../Category/四格漫畫.md "wikilink")
[Category:科幻漫畫](../Category/科幻漫畫.md "wikilink")
[Category:公主主角題材漫畫](../Category/公主主角題材漫畫.md "wikilink")
[Category:公主主角題材遊戲](../Category/公主主角題材遊戲.md "wikilink")
[Category:電腦遊戲](../Category/電腦遊戲.md "wikilink") [Category:Game
Boy Advance遊戲](../Category/Game_Boy_Advance遊戲.md "wikilink")
[Category:秋葉原背景作品](../Category/秋葉原背景作品.md "wikilink")
[Category:虛構外星角色](../Category/虛構外星角色.md "wikilink")
[Category:東森電視外購動畫](../Category/東森電視外購動畫.md "wikilink")

1.  「滴骰」，又寫作「的骰」，為[粵語](../Page/粵語.md "wikilink")，正寫為「菂薂」。菂為[蓮子](../Page/蓮子.md "wikilink")，薂為[蓮籽](../Page/蓮籽.md "wikilink")；古義指蓮子果實。因其果實細小而引申為現代粵語的「細小」、「小巧」之義。