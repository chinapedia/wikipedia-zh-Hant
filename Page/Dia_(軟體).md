**Dia**
\[1\]是[開放源碼的](../Page/開放源碼.md "wikilink")，是[GNU計劃的一部分](../Page/GNU.md "wikilink")，程式創立者是Alexander
Larsson。Dia使用[單一文件介面模式](../Page/單一文件介面.md "wikilink")，類似於[GIMP與](../Page/GIMP.md "wikilink")[Inkscape](../Page/Inkscape.md "wikilink")。

Dia將多種需求以模組化來設計，如[流程圖](../Page/流程圖.md "wikilink")、、[電路圖等](../Page/電路圖.md "wikilink")。各模組之間的符號仍是可以通用的，並沒有限制。

Dia可以畫多種示意圖，並且藉由[XML可以新增多種圖形](../Page/XML.md "wikilink")。Dia以XML格式（預設以gzip壓縮節省空間）載入及儲存流程圖。

## 匯出

Dia能夠將流程圖匯出為多種格式如下：

  - [EPS](../Page/EPS.md "wikilink")
  - [SVG](../Page/SVG.md "wikilink")
  - [DXF](../Page/AutoCAD_DXF.md "wikilink")（Autocad格式）
  - [CGM](../Page/CGM.md "wikilink")
  - [WMF](../Page/WMF.md "wikilink")
  - [PNG](../Page/PNG.md "wikilink")
  - [JPEG](../Page/JPEG.md "wikilink")
  - [VDX](../Page/VDX.md "wikilink")（Microsoft
    [Visio格式](../Page/Visio.md "wikilink")）

## 參見

  - [Inkscape](../Page/Inkscape.md "wikilink")
  - [UML](../Page/UML.md "wikilink")
  - [Kivio](../Page/Kivio.md "wikilink")
  - [Graphviz](../Page/Graphviz.md "wikilink")

## 外部連結

  - [Dia專案首頁](https://wiki.gnome.org/Apps/Dia)
  - [由Harry
    George所編寫的Dia教學](http://www.seanet.com/~hgg9140/comp/diatut/all/all.html)
  - [給網頁圖表使用的Dia圖形](http://ftp.edgewall.com/pub/people/daniel/dia/web-shapes.tar.gz)
  - [Windows使用的Dia](http://dia-installer.de/download/index.html)
  - [Mac OS X使用的Dia](http://dia-installer.de/download/macosx.html)

[Category:GNOME](../Category/GNOME.md "wikilink")
[Category:自由軟體](../Category/自由軟體.md "wikilink")
[Category:SourceForge專案](../Category/SourceForge專案.md "wikilink")
[Category:流程图软件](../Category/流程图软件.md "wikilink")

1.