**Dillo**是一款小巧的[网页浏览器](../Page/网页浏览器.md "wikilink")，遵循[GPL](../Page/GPL.md "wikilink")。用[C语言编写](../Page/C语言.md "wikilink")，使用了[GTK+](../Page/GTK.md "wikilink")
toolkit，该浏览器特别适合运行于老计算机，以及嵌入系统。

由於GTK+2的功能變更很大，不符合Dillo「短小精悍」的需求，所以Dillo轉向以[FLTK作graphic](../Page/FLTK.md "wikilink")
toolkit。目前porting到[FLTK的工作已經近乎完成](../Page/FLTK.md "wikilink")\[1\]。一些[UNIX](../Page/UNIX.md "wikilink")/[Linux发行版本](../Page/Linux.md "wikilink")，包括[Debian](../Page/Debian.md "wikilink")、[RedHat](../Page/RedHat.md "wikilink")、[NetBSD等](../Page/NetBSD.md "wikilink")，有Dillo的安装包，一些小型的[Linux系统](../Page/Linux.md "wikilink")，包括[Damn
Small Linux](../Page/Damn_Small_Linux.md "wikilink")、[Feather
Linux](../Page/Feather_Linux.md "wikilink")，将Dillo作为主力浏览器。

## 參考資料

## 参见

  - [網頁親和力](../Page/網頁親和力.md "wikilink")
  - [网页浏览器列表](../Page/网页浏览器列表.md "wikilink")
  - [网页浏览器比较](../Page/网页浏览器比较.md "wikilink")

[Category:自由网页浏览器](../Category/自由网页浏览器.md "wikilink")

1.