[DSDlogo.svg](https://zh.wikipedia.org/wiki/File:DSDlogo.svg "fig:DSDlogo.svg")
[PCM-vs-DSD.svg](https://zh.wikipedia.org/wiki/File:PCM-vs-DSD.svg "fig:PCM-vs-DSD.svg")

**Direct Stream
Digital**（**DSD**）是一項屬於[Sony和](../Page/Sony.md "wikilink")[飛利浦的專利](../Page/飛利浦.md "wikilink")，利用[脈衝密度調變](../Page/脈衝密度調變.md "wikilink")（pulse-density
modulation）[編碼將音頻訊號儲存在數位媒體上的科技](../Page/編碼.md "wikilink")，這項技術的應用對象是[SACD](../Page/SACD.md "wikilink")。

訊號本身以[ΔΣ調變後的數位音訊儲存](../Page/ΔΣ調變.md "wikilink")，連續單一個位元的序列以64倍於CD取樣率（44.1
kHz）的頻率來取樣，即2.8224 MHz。藉由64倍過取樣（oversample）來達成[noise
shaping](../Page/noise_shaping.md "wikilink")，把以往由於量化不精確的聲音訊號而造成的噪音和失真，減少至一個位元以內的誤差。可議的是，1-bit
Sigma-Delta運算是否真的可能解決失真問題\[1\]。由於1-bit
Sigma-Delta轉換的運作方式，以DSD編碼的聲音在中低頻上有著比PCM更好的解析度、高頻的[相位誤差更是降到極低](../Page/相位.md "wikilink")，然而在高頻的動態較PCM差。有些人認為，相較於PCM的失真，DSD的失真更不容易被人類的聽覺系統所察覺；但也有人認為，CD音質比SACD差，並不是PCM與DSD的差距、而是CD使用過低的取樣頻率及數位母帶的頻率與CD不同所造成的（SACD所使用的DSD格式，與於176.4Khz/16bits的PCM格式具有相同的未壓縮大小，略大於96/24的PCM格式；而DVD-Audio支援多種解析度，代表可以採取與數位母帶相同的解析度，避免頻率轉換造成的音質減損）。DSD編碼的另一個缺點是無法進行音樂後製，必需轉換成PCM訊號來處理，容易同時繼承兩者的缺點；但也有人認為，DSD的優勢在於多數[DAC是處理DSD數位訊號及類比訊號的互相轉換](../Page/DAC.md "wikilink")，如果要輸出或輸入PCM格式，則必須加上DSD及PCM訊號的轉換機制，這個機制需要相當的計算量、在音樂後製者的電腦處理可以算得比DAC的即時轉換來得精確——只要原始錄音及後製時的解析度夠高就可以了。

在DSD或者PCM編碼方式之間的孰優孰劣之間有著許多爭議。[滑铁卢大学的教授](../Page/滑铁卢大学.md "wikilink")[Stanley
Lipschitz和](../Page/Stanley_Lipschitz.md "wikilink")[John
Vanderkooy主張單位元的轉換器](../Page/John_Vanderkooy.md "wikilink")（如DSD所使用的）有高度失真的緣故，並不適合高階的音訊應用\[2\]。即使只有8-bit和四倍過取樣的PCM和noise
shaping，加上適當的[dithering](../Page/抖動_\(數位訊號處理\).md "wikilink")，僅有DSD一半資料量，在底噪和頻率響應上也比DSD來得好。但是在2002年，飛利浦發佈了一篇論文反駁這樣的說法\[3\]。[James
Angus教授在Audio](../Page/James_Angus.md "wikilink") Engineering
Society發表會上具細節地反駁Lipschitz和Vanderkooy的論文\[4\]。Lipschitz等人對此也做了回應\[5\]。

但是除了規格上的爭論外，SACD與DVD-Audio都面臨了一個問題，就是數位母帶品質的問題，一些SACD/DVD-Audio的規格看似很高，但卻是採用軟體升頻出來的，這些產品的實際音質不可能超過原始取樣解析度的音質。

實用的DSD轉換器領域是由[Ed Meitner開闢的](../Page/Ed_Meitner.md "wikilink")，他是奧地利EMM
Labs的工程師和老闆。而商業化的DSD技術則由Sony和飛利浦開發，標準的CD規格也是由他們所開發的。而飛利浦的DSD部門則在2005年轉移至Sonic
Studio, LLC，做持續的設計和開發。

DSD技術在視訊方面或許有著相同的潛力。[Laserdisc光碟就是使用](../Page/Laserdisc.md "wikilink")[脈衝寬度調變](../Page/脈衝寬度調變.md "wikilink")（pulse-width
modulation）編碼的架構，解碼方式和DSD相同；LD光碟的畫質也廣受稱讚。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [Poking a Round Hole in a Square Wave (DSD vs PCM
    comparison)](http://www.smr-home-theatre.org/surround2002/technology/page_07.shtml)

[Category:訊號處理](../Category/訊號處理.md "wikilink")

1.  見 [Audio Engineering Society Convention Paper 5395: Why 1-Bit
    Sigma-Delta Conversion is Unsuitable for High-Quality
    Applications](http://sjeng.org/ftp/SACD.pdf)

2.
3.  [Audio Engineering Society Convention Paper 5616: Enhanced Sigma
    Delta Structures for Super Audio CD
    Applications](http://www.extra.research.philips.com/mscs/publications2002/dsd-aesformat.pdf)


4.

5.  [Audio Engineering Society Convention Paper 5620: Toward a Better
    Understanding of 1-Bit Sigma-Delta Modulators -
    Part 3](http://www.aes.org/events/112/papers/x.cfm)