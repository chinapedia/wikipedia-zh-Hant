**Discovery Asia**，前身**高清探索頻道**（**Discovery
HD**）是[探索傳播](../Page/探索傳播.md "wikilink")（Discovery
Communications）所創立的[高清電視頻道](../Page/高清.md "wikilink")。Discovery HD
於2005年12月創立，專門播放電視[紀錄片](../Page/紀錄片.md "wikilink")，是第二個探索傳播的同類頻道。

## 標誌

## 外部連結

[\*](../Category/探索傳播.md "wikilink")
[Category:紀錄片頻道](../Category/紀錄片頻道.md "wikilink")
[Category:高清頻道](../Category/高清頻道.md "wikilink")
[探索頻道節目](../Category/探索頻道節目.md "wikilink")