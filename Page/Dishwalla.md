**Dishwalla**是一支成军于1990年，来自美国加州圣塔芭芭拉的[另类摇滚乐队](../Page/另类摇滚.md "wikilink")。
成员由主唱JR Richards，吉他手Rodney Browning，贝斯手Scot Alexander，键盘手Jim Wood，鼓手Pete
Maloney组成，目前发行过5张专辑。

## 成员组成

  - **主唱**:[JR Richards](../Page/JR_Richards.md "wikilink")
  - **吉他手**：[Rodney Browning](../Page/Rodney_Browning.md "wikilink")
  - **贝斯手**：[Scot Alexander](../Page/Scot_Alexander.md "wikilink")
  - **键盘手**：[Jim Wood](../Page/Jim_Wood.md "wikilink")
  - **鼓手**：[Pete Maloney](../Page/Pete_Maloney.md "wikilink")

## 发行专辑

  - **Dishwalla** 2005

<!-- end list -->

1.  [40 Stories](../Page/40_Stories.md "wikilink")
2.  [Collide](../Page/Collide.md "wikilink")
3.  [Ease the Moment](../Page/Ease_the_Moment.md "wikilink")
4.  [Coral Sky](../Page/Coral_Sky.md "wikilink")
5.  [Winter Sun](../Page/Winter_Sun.md "wikilink")
6.  [Creeps in the Stone](../Page/Creeps_in_the_Stone.md "wikilink")
7.  [Surrender the Crown](../Page/Surrender_the_Crown.md "wikilink")
8.  [Bleeding Out](../Page/Bleeding_Out.md "wikilink")
9.  [Life for Sale](../Page/Life_for_Sale.md "wikilink")
10. [Above the Wreckage](../Page/Above_the_Wreckage.md "wikilink")
11. [Far Away](../Page/Far_Away.md "wikilink")
12. [Collide (Massy Mix)](../Page/Collide_\(Massy_Mix\).md "wikilink")

<!-- end list -->

  - ''' Live... Greetings From The Flow State ''' 2003

<!-- end list -->

1.  [Stay Awake](../Page/Stay_Awake.md "wikilink")
2.  [Madlife](../Page/Madlife.md "wikilink")
3.  [One in A while](../Page/One_in_A_while.md "wikilink")
4.  [Home](../Page/Home.md "wikilink")
5.  [Moisture](../Page/Moisture.md "wikilink")
6.  [Angels or Devils](../Page/Angels_or_Devils.md "wikilink")
7.  [Counting Blue Cars](../Page/Counting_Blue_Cars.md "wikilink")
8.  [Somewhere In The
    Middle](../Page/Somewhere_In_The_Middle.md "wikilink")
9.  [Every Little Thing](../Page/Every_Little_Thing.md "wikilink")
10. [Give](../Page/Give.md "wikilink")
11. [Haze](../Page/Haze.md "wikilink")
12. [So Much Time](../Page/So_Much_Time.md "wikilink")

<!-- end list -->

  - ''' Opaline ''' 2003

<!-- end list -->

1.  [Opaline](../Page/Opaline.md "wikilink")
2.  [Angels or Devils](../Page/Angels_or_Devils.md "wikilink")
3.  [Somewhere in the
    Middle](../Page/Somewhere_in_the_Middle.md "wikilink")
4.  [Every Little Thing](../Page/Every_Little_Thing.md "wikilink")
5.  [When Morning Comes](../Page/When_Morning_Comes.md "wikilink")
6.  [Home](../Page/Home.md "wikilink")
7.  [Today, Tonight](../Page/Today,_Tonight.md "wikilink")
8.  [Mad Life](../Page/Mad_Life.md "wikilink")
9.  [Candleburn](../Page/Candleburn.md "wikilink")
10. [Nashville Skyline](../Page/Nashville_Skyline.md "wikilink")
11. [Drawn Out](../Page/Drawn_Out.md "wikilink")

<!-- end list -->

  - ''' And You Think You Know What Life's About ''' 1998

<!-- end list -->

1.  Stay Awake
2.  Once In A While
3.  Bottom Of The Floor
4.  Healing Star
5.  Until I Wake Up
6.  5 Star Day
7.  Truth Serum
8.  So Blind
9.  Gone Upside Down
10. So Much Time
11. The Bridge Song
12. Pop Guru

<!-- end list -->

  - ''' Pet Your Friends ''' 1995

<!-- end list -->

1.  [Pretty Babies](../Page/Pretty_Babies.md "wikilink")
2.  [Haze](../Page/Haze.md "wikilink")
3.  [Counting Blue Cars](../Page/Counting_Blue_Cars.md "wikilink")
4.  [Explode](../Page/Explode.md "wikilink")
5.  [Charlie Brown's
    Parents](../Page/Charlie_Brown's_Parents.md "wikilink")
6.  [Give](../Page/Give.md "wikilink")
7.  [Miss Emma Peel](../Page/Miss_Emma_Peel.md "wikilink")
8.  [Moisture](../Page/Moisture.md "wikilink")
9.  [The Feeder](../Page/The_Feeder.md "wikilink")
10. [All She Can See](../Page/All_She_Can_See.md "wikilink")
11. [Only For So Long](../Page/Only_For_So_Long.md "wikilink")
12. [Bonus Track](../Page/Bonus_Track.md "wikilink")

## 外部連結

  - [官方網站](http://dishwalla.com/)

[Category:美國另類搖滾樂團](../Category/美國另類搖滾樂團.md "wikilink")
[Category:另類搖滾樂團](../Category/另類搖滾樂團.md "wikilink")
[Category:美國搖滾樂團](../Category/美國搖滾樂團.md "wikilink")