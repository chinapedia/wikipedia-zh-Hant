《**Distance**》是[日本歌手](../Page/日本.md "wikilink")[宇多田光的第二張專輯](../Page/宇多田光.md "wikilink")，於2001年3月28日發行，初動300萬張，目前銷售447萬張。創下日本音樂史上最高首周銷售紀錄\[1\]，同時亦為為2001年年度銷量冠軍。

《Distance》曾是史上首周全球總銷量最高的專輯，直至14年後由[愛黛兒於](../Page/愛黛兒.md "wikilink")2015年發行的英文音樂專輯《[25](../Page/25_\(愛黛兒專輯\).md "wikilink")》打破。現為世界第二高，亞洲第一\[2\]\[3\]。

《**Distance**》是日本跨入21世紀後最高銷量專輯，以及歷代第二高銷量的原創專輯 (僅次於前作[First
Love](../Page/First_Love_\(宇多田光專輯\).md "wikilink"))。日本史上總銷量綜合排名則為第四\[4\]。

## 概要

  - 本作是她第一張沒有間奏的專輯。
  - 專題中"[Can You Keep A
    Secret?](../Page/Can_You_Keep_A_Secret?.md "wikilink")"一曲為日劇[HERO的片尾曲](../Page/HERO_\(日本電視劇\).md "wikilink")。
  - [Rodney
    Jerkins](../Page/Rodney_Jerkins.md "wikilink")，西洋R\&B製作人（曾與[唐妮·布蕾斯顿共事](../Page/唐妮·布蕾斯顿.md "wikilink")），監製了"Time
    Limit"，並在其中秀了一段RAP。
  - 美國製作人[Jimmy Jam and Terry
    Lewis監製了](../Page/Jimmy_Jam_and_Terry_Lewis.md "wikilink")[Wait
    & See
    \~Risk\~和UP](../Page/Wait_&_See_~Risk~.md "wikilink")-IN-HEAVEN
    mix版本的[Addicted To You](../Page/Addicted_To_You.md "wikilink")。
  - "無法言喻的心情"的曲調在先前的"Interlude"（收錄於專輯[First
    Love](../Page/First_Love.md "wikilink")）中可以找到。當初因時間所迫無法完成全曲，只能作為一段短短的間奏，故這次將它完成了。
  - "*Hayatochiri*"中的"ri"被刪除（該曲收錄在單曲"[Wait & See
    \~Risk\~](../Page/Wait_&_See_~Risk~.md "wikilink")"中），名稱改為"HAYATOCHI-REMIX"。
  - "Drama"一開頭的數秒與美國樂團[Fuel的Hemorrhage非常相似](../Page/Fuel_\(band\).md "wikilink")。
  - 同名曲"Distance"之後重新編曲為慢板的"[FINAL
    DISTANCE](../Page/FINAL_DISTANCE.md "wikilink")"，是為紀念[附屬池田小學事件中死亡的小女孩](../Page/附屬池田小學事件.md "wikilink")。該曲後被收入日本高中音樂教科書。
  - 2008年10月，相隔七年半後，"Eternally"一曲重新錄製為"[Eternally -Drama
    Mix-](../Page/Eternally_-Drama_Mix-.md "wikilink")"，並作為劇集[Innocent
    Love的主題曲](../Page/Innocent_Love.md "wikilink")。

## 話題

本作發行前夕，[濱崎步的唱片公司](../Page/濱崎步.md "wikilink")[艾迴特意將其精選輯](../Page/艾迴.md "wikilink")《[A
BEST](../Page/A_BEST.md "wikilink")》延後發行日，與《Distance》同日於2001年3月28日發行，正面相碰，加以宣傳，使「歌姬對決」的話題大肆蔓延。為此，兩方公司皆重砸預算，火力全開，[澀谷街頭完全被這兩張專輯的巨型看板佔領](../Page/澀谷.md "wikilink")。

然而兩方其實皆對此一「競爭」的說法表示反對。

其結果，《**Distance**》以300.2萬初動\[5\]創下日本首周最高紀錄，也曾是史上首周全球總銷量最高的唱片\[6\]，比《A
BEST》優勝。唯於2015年被[Adele憑專輯](../Page/Adele.md "wikilink")《[25](../Page/25_\(愛黛兒專輯\).md "wikilink")》
單單在美國售出338萬張、及全球合共賣出超過400萬張取代。《Distance》目前日本國內銷量約為447萬張，為2001年[Oricon年度冠軍](../Page/Oricon.md "wikilink")，也是日本史上銷量第四專輯。
\[7\]全球銷量方面，《Distance》則為2001年年度全球唱片銷量第10位。\[8\]

在發行此張專輯後，宇多田單曲加專輯銷量正式突破2000萬張，以18歲2個月年紀，打破1997年[安室奈美惠所保持](../Page/安室奈美惠.md "wikilink")19歲2個月年紀紀錄，成為日本10代歌手中速度最快。

## 曲目

全曲作詞、作曲：宇多田光 （除「Time
Limit」與「Drama」是本人與[GLAY的](../Page/GLAY.md "wikilink")[TAKURO共同作曲](../Page/TAKURO.md "wikilink")。）

## 收錄單曲

本專輯所收錄單曲為其歌手生涯至今實體銷量最驚人者。四張全為冠軍單曲，而除了[For You/Time
Limit](../Page/For_You/Time_Limit.md "wikilink")，約90萬張外，其他全部約在150萬張以上，並居日本史上單曲TOP
100。

| 發行日                        | 歌曲                                                                     | 最高排名    | 上榜週數 | 銷量        |
| :------------------------- | :--------------------------------------------------------------------- | :------ | :--- | :-------- |
| <small>1999年11月10日</small> | [Addicted To You](../Page/Addicted_To_You.md "wikilink")               | **\#1** | 16   | 1,784,050 |
|                            |                                                                        |         |      |           |
| <small>2000年4月19日</small>  | [Wait & See \~Risk\~](../Page/Wait_&_See_~Risk~.md "wikilink")         | **\#1** | 21   | 1,662,060 |
| <small>2000年6月30日</small>  | [For You/Time Limit](../Page/For_You/Time_Limit.md "wikilink")         | **\#1** | 13   | 888,650   |
|                            |                                                                        |         |      |           |
| <small>2001年2月16日</small>  | [Can You Keep A Secret?](../Page/Can_You_Keep_A_Secret?.md "wikilink") | **\#1** | 11   | 1,484,940 |
|                            |                                                                        |         |      |           |

## 銷售

**Distance** - **[Oricon](../Page/Oricon.md "wikilink")** 銷售榜（日本）

| 公佈日期                      | 排行榜          | 最高排名                        | 首週銷量      | 總銷量       | 上榜週數 |
| :------------------------ | :----------- | :-------------------------- | :-------- | :-------- | :--- |
| <small>2001年3月28日</small> | Oricon 每日銷售榜 | **\#1**                     |           |           |      |
|                           |              |                             |           |           |      |
| <small>2001年3月28日</small> | Oricon 每週銷售榜 | **\#1** <small>（2週）</small> | 3,002,720 | 4,472,353 | 51   |
| <small>2001年3月28日</small> | Oricon 年度銷售榜 | **\#1**                     |           |           |      |
|                           |              |                             |           |           |      |

## 註釋

[Category:宇多田光專輯](../Category/宇多田光專輯.md "wikilink")
[Category:2001年音樂專輯](../Category/2001年音樂專輯.md "wikilink")
[Category:RIAJ百萬認證專輯](../Category/RIAJ百萬認證專輯.md "wikilink")
[Category:Oricon百萬銷量達成專輯](../Category/Oricon百萬銷量達成專輯.md "wikilink")
[Category:Oricon專輯年榜冠軍作品](../Category/Oricon專輯年榜冠軍作品.md "wikilink")
[Category:2001年Oricon專輯月榜冠軍作品](../Category/2001年Oricon專輯月榜冠軍作品.md "wikilink")
[Category:2001年Oricon專輯週榜冠軍作品](../Category/2001年Oricon專輯週榜冠軍作品.md "wikilink")
[Category:EMI日本音樂專輯](../Category/EMI日本音樂專輯.md "wikilink")

1.  <http://www.mediatraffic.de/top-album-achievements.htm>

2.

3.

4.

5.  [ORICON STYLE
    - 2001年04月第2週專輯榜](http://www.oricon.co.jp/search/result.php?kbn=ja&types=rnk&year=2001&month=4&week=2&submit5.x=16&submit5.y=8)

6.
7.

8.  web.archive.org/web/20081117003957/[ifpi Top 50 Global Best Selling
    Albums for 2001](http://www.ifpi.org/content/library/top50-2001.pdf)