****（簡稱****）是[Firefox的一个](../Page/Firefox.md "wikilink")[下载管理器软件](../Page/下载.md "wikilink")。它支持[多线程](../Page/多线程.md "wikilink")、续传、批量文件下载，同时允许[HTTP和](../Page/HTTP.md "wikilink")[FTP协议](../Page/FTP.md "wikilink")。

DTA是在[GNU通用公共许可证协议下的](../Page/GNU通用公共许可证.md "wikilink")[自由軟體](../Page/自由軟體.md "wikilink")。

## 文件过滤

当下载整个[网页](../Page/网页.md "wikilink")，用户可以使用[通配符或](../Page/通配符.md "wikilink")[正则表达式匹配过滤器](../Page/正则表达式.md "wikilink")，从而选择只下载某几类文件（如所有[PDF文件](../Page/PDF.md "wikilink")）。

## 特性

  - 體積和資源耗用輕巧，只有444[KB](../Page/KB.md "wikilink")
  - 可自行調整分割區塊數量
  - 沒有[P2P加速的設計](../Page/P2P.md "wikilink")
  - 僅支援[Firefox不支援](../Page/Firefox.md "wikilink")[Internet
    Explorer](../Page/Internet_Explorer.md "wikilink")、[Opera](../Page/Opera.md "wikilink")、[Chrome](../Page/Chrome.md "wikilink")

## 外部链接

  - [DownThemAll\!的官方网页](http://www.downthemall.net/)
  - [DownThemAll\!在Firefox
    Add-ons网站中的下载页面](https://addons.mozilla.org/firefox/addon/201)

[Category:Firefox 附加组件](../Category/Firefox_附加组件.md "wikilink")