**Download.com**是全球最大的[下載目錄](../Page/下載.md "wikilink")[網站](../Page/網站.md "wikilink")，於1996年作為[CNET的一部份開辦](../Page/CNET.md "wikilink")。而Download.com上的軟體可能已不再更新。

## 主要內容

Download.com提供的內容分為四大類：**軟體**（包括[個人電腦](../Page/個人電腦.md "wikilink")、[麥金塔與手持式裝置](../Page/麥金塔.md "wikilink")）、**音樂**、**遊戲**與**影像**，由Download.com或第三方的[FTP](../Page/FTP.md "wikilink")[伺服器提供下載](../Page/伺服器.md "wikilink")。音樂為完全免費下載的[MP3](../Page/MP3.md "wikilink")、[WMA或串流媒體](../Page/WMA.md "wikilink")。

## 評價共享

軟體部分包含超過100,000款[免費軟體](../Page/免費軟體.md "wikilink")、[共享軟體與先試用軟體可供下載](../Page/共享軟體.md "wikilink")。可供下載檔案通常會有編輯的評分及評語，並會包含軟體發行人摘要以及一到多張截圖，用戶亦可寫下評語以及以一到五顆星來評分。

## 免費下載

所有軟體下載均為免費，其中一些為試用版，大部份是免費或共享軟體；所有音樂下載與串流媒體均為免費。存取該網站任何內容Download.com均不會收費。

軟體發行人可以通過CNET的[Upload.com](http://www.upload.com)網站免費發布他們的作品，亦可選擇不同費率的增強功能。

2004年Download.com
Music取代[MP3.com](../Page/MP3.com.md "wikilink")，可免費下載不同類型及[藝人免費上傳的音樂](../Page/藝人.md "wikilink")。

2005年7月Download.com
Video開辦並提供不同類型的[電影及](../Page/電影.md "wikilink")[電視](../Page/電視.md "wikilink")、[體育](../Page/體育.md "wikilink")、[動畫與](../Page/動畫.md "wikilink")[音樂錄影帶等](../Page/音樂錄影帶.md "wikilink")。

## 安全、廣告軟體與間諜軟體

CNET Download.com資深副[總裁Scott](../Page/總裁.md "wikilink")
Arpajian在2005年4月宣佈開始對所有梱綁[廣告軟體的軟體推行](../Page/廣告軟體.md "wikilink")"零容忍"政策；Download.com過去曾取締[間諜軟體產品](../Page/間諜軟體.md "wikilink")，並透過審核所有上傳的檔案以及利用其經常更新的[間諜軟體中心](http://www.download.com/Spyware-Center/2001-2023_4-0.html)教育使用者來打擊間諜軟體。

## 外部連結

  - [Download.com](http://www.download.com/)
  - [Upload.com](http://www.upload.com/)
  - [CNET.com](http://www.cnet.com/)

[Category:下載網站](../Category/下載網站.md "wikilink")
[Category:CNET](../Category/CNET.md "wikilink")