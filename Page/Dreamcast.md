是[日本](../Page/日本.md "wikilink")[電子遊戲商](../Page/電子遊戲發行商.md "wikilink")[世嘉於](../Page/世嘉.md "wikilink")1998年11月27日在日本、1999年9月9日在[北美](../Page/北美地區.md "wikilink")、1999年10月14日在[歐洲發行的](../Page/歐洲.md "wikilink")[64位元](../Page/64位元.md "wikilink")[家用遊戲機](../Page/家用遊戲機.md "wikilink")，是[第六世代電子遊戲機中第一部推出的主機](../Page/第六世代遊戲機歷史.md "wikilink")，也是世嘉開發的最後一部家用遊戲機。主要競爭對手為[索尼的](../Page/索尼.md "wikilink")[PlayStation
2](../Page/PlayStation_2.md "wikilink")、[任天堂的](../Page/任天堂.md "wikilink")[GameCube及](../Page/GameCube.md "wikilink")[微軟的](../Page/微軟.md "wikilink")[Xbox](../Page/Xbox_\(遊戲機\).md "wikilink")。

與前代主機[世嘉土星不同](../Page/世嘉土星.md "wikilink")，Dreamcast採用了[日立](../Page/日立製作所.md "wikilink")[SH-4](../Page/SuperH.md "wikilink")[中央處理器](../Page/中央處理器.md "wikilink")（CPU）及[日本電氣](../Page/日本電氣.md "wikilink")[PowerVR2](../Page/PowerVR.md "wikilink")[圖形處理器](../Page/圖形處理器.md "wikilink")（GPU）等取代客製化組件，藉以降低成本。儘管Dreamcast在北美地區發售初期頗有斬獲，但在索尼開始為其次世代主機PlayStation
2炒作宣傳後，Dreamcast主機銷量便逐漸滑落；在銷量長期低迷的情況下，世嘉的財務損失持續擴大。在改組領導層之後，世嘉最終於2001年3月31日宣布終止Dreamcast業務、全面退出家用遊戲機硬體市場，並改組為純[第三方遊戲開發商](../Page/游戏开发者.md "wikilink")。Dreamcast在其生命週期中僅售出了913萬部，是世嘉最重大的商業失敗之一。

雖然Dreamcast的生命週期不長、第三方遊戲有限，但也有著《[瘋狂計程車](../Page/瘋狂計程車.md "wikilink")》、《[Jet
Set
Radio](../Page/Jet_Set_Radio.md "wikilink")》和《[莎木](../Page/莎木.md "wikilink")》系列等高品質的獨占遊戲，以及數款來自世嘉自有街機系統[NAOMI的移植作品](../Page/NAOMI.md "wikilink")。Dreamcast也是第一部內建[撥接網路並提供原生](../Page/撥號連線.md "wikilink")[網路遊戲的家用遊戲機](../Page/網路遊戲.md "wikilink")。

## 歷史

### 背景

[Mega
Drive](../Page/Mega_Drive.md "wikilink")（在[北美地區被稱為SEGA](../Page/北美地區.md "wikilink")
Genesis）於1988年發售，銷售量達3075萬部，是世嘉歷年來最成功的主機。Mega
Drive的繼任主機[世嘉土星於](../Page/世嘉土星.md "wikilink")1994年在日本推出，採用雙[中央處理器](../Page/中央處理器.md "wikilink")（CPU）架構以同時處理[2D和](../Page/二维计算机图形.md "wikilink")[3D圖形](../Page/三维计算机图形.md "wikilink")，然而由於其複雜的硬體架構使其比其主要競爭對手、[索尼推出的](../Page/索尼.md "wikilink")[PlayStation硬體成本更加高昂](../Page/PlayStation_\(遊戲機\).md "wikilink")。儘管世嘉土星的首發日期比PlayStation早了將近一個月，但由於倉促上架以至於供貨不足、初期支援遊戲稀少，對其銷量造成了無法挽回的傷害；世嘉土星在價格方面也處於劣勢，其首發售價足足比PlayStation貴上100美元；加上家用遊戲機市場龍頭[任天堂的次世代家用遊戲機研發計畫持續延滯](../Page/任天堂.md "wikilink")，以及Mega
Drive諸多缺乏配套遊戲陣容的配件（尤其是[Super
32X](../Page/Super_32X.md "wikilink")）對世嘉本身的企業聲譽造成的損害，使得索尼迅速在市場上占據了一席之地：在積極的廣告宣傳及強大的第三方遊戲作品支持下，PlayStation在北美地區取得了重大成功。1995年底時，索尼在北美市場已售出80萬部[PlayStation](../Page/PlayStation_\(遊戲機\).md "wikilink")，是世嘉同時間售出之世嘉土星數量的2倍之多，同時Mega
Drive/SEGA
Genesis的銷量也開始銳減；至1996年底，索尼總計在北美市場已售出290萬部PlayStation，大幅超越世嘉售出的120萬部世嘉土星。索尼的優勢在與世嘉的價格戰中得到了進一步的鞏固：儘管雙方皆持續進行[價格戰](../Page/价格战.md "wikilink")，但是由於世嘉土星之主機硬體製程複雜，成本不易壓低，使世嘉的財政赤字日益嚴重，連年虧損。

由於與日本世嘉總部在商業決策上的長期分歧，時任美國世嘉總裁決定離職。1996年7月16日，世嘉宣布前[本田董事](../Page/本田技研工业.md "wikilink")將成為新任的美國世嘉總裁兼執行長，而卡林斯基將在當年9月30日之後離開世嘉。日本世嘉還宣布其聯合創始人和現任總裁[中山隼雄都將辭去職位](../Page/中山隼雄.md "wikilink")，但兩人仍會留在公司中（中山並續任日本世嘉執行長）。同時，前美國[索尼電腦娛樂執行長](../Page/索尼互動娛樂.md "wikilink")被任命為美國世嘉負責產品研發和維持與第三方遊戲開發者關係的執行副總裁。斯托拉爾認為世嘉土星的硬體設計過於複雜，不對世嘉土星抱有期待，還曾在1997年的[E3上公開表示](../Page/E3.md "wikilink")：「（世嘉）土星不是我們的未來。」1996年6月，任天堂的次世代主機[任天堂64發布](../Page/任天堂64.md "wikilink")，其飛躍性的3D運算效能，使世嘉土星原本已搖搖欲墜的銷量急轉直下；1997年初，[史克威爾在PlayStation平臺上發行的](../Page/史克威爾.md "wikilink")《[最終幻想VII](../Page/最终幻想VII.md "wikilink")》獲得極大成功，更對世嘉土星造成毀滅性的打擊。1997年，索尼在北美市場售出的PlayStation主機數量已達世嘉售出之世嘉土星數量的3倍之多，此時索尼已占有北美家用遊戲機市場份額的47%，超越了任天堂的40%及世嘉的僅僅12%；無論是降低售價還是推出任何新遊戲都已經不能扭轉世嘉土星的劣勢。由於世嘉土星在北美地區的表現不佳，美國世嘉在1997年下旬資遣了60名員工。

由於公司財務狀況持續惡化，中山隼雄於1998年1月辭任日本世嘉總裁。1998年3月，由於獲利不斷下降，日本世嘉自1988年在[東京證券交易所上市以來第一次遭受合併財務損失](../Page/東京證券交易所.md "wikilink")，公司合併淨虧損達356億日圓。在日本世嘉公布財報前，美國世嘉宣布世嘉土星將退出北美家用遊戲機市場，「以專注於其繼任者的研發」；日本及歐洲地區的支援及維修服務則延續至2000年。

### 開發

早在1995年時，就已經有傳聞稱世嘉正與[洛克希德·馬丁](../Page/洛克希德·馬丁.md "wikilink")、[3DO公司](../Page/3DO公司.md "wikilink")、[松下電器及聯合半導體](../Page/松下電器.md "wikilink")（Alliance
Semiconductor）等公司合作研發次世代[圖形處理器](../Page/圖形處理器.md "wikilink")（GPU），預備用於新一代的[64位元家用遊戲機](../Page/64位元.md "wikilink")「世嘉土星2」；不過實際上，Dreamcast的研發過程與這些傳聞完全沒有關係。

鑑於由內部部門主導研發的世嘉土星市場表現不佳，入交昭一郎決定在既有的硬體部門外另組開發團隊。自1997年起，入交昭一郎與[IBM的山本龍太郎合作](../Page/IBM.md "wikilink")，籌組了一個約十人左右的團隊，於美國祕密進行次世代硬體研發，計畫代號為「黑帶」（Blackbelt）；同時間，由[佐藤秀樹領導的日本世嘉內部團隊也開始了次世代硬體的研發計畫](../Page/佐藤秀樹.md "wikilink")，計畫代號為「白帶」（Whitebelt），後以格鬥遊戲《[VR快打](../Page/VR快打_\(遊戲\).md "wikilink")》[頭目角色之名改稱](../Page/頭目_\(角色類型\).md "wikilink")「杜拉爾」（Dural）。對於為何日本世嘉會讓內部團隊加入研發行列，主要有兩種解釋：一種說法是日本世嘉希望兩地同時進行開發，另一種說法則是佐藤受高層壓力影響，因而不得不投入新硬體研發計畫。

由佐藤領導的團隊決定採用[日立](../Page/日立製作所.md "wikilink")[SH-4中央處理器](../Page/SuperH.md "wikilink")，以及由[Imagination
Technologies研發](../Page/Imagination_Technologies.md "wikilink")、[日本電氣製造的](../Page/日本電氣.md "wikilink")[PowerVR2圖形處理器作為次世代家用遊戲機](../Page/PowerVR.md "wikilink")[主機板的主要組件](../Page/主機板.md "wikilink")，山本領導的團隊則決定採用由[3dfx
Interactive研發的](../Page/3dfx_Interactive.md "wikilink")[Voodoo
2或](../Page/3dfx_Interactive#Voodoo_2.md "wikilink")[Voodoo
Banshee圖形處理器作為主要組件](../Page/3dfx_Interactive#Voodoo_Banshee.md "wikilink")，並預定採用[摩托羅拉](../Page/摩托羅拉.md "wikilink")中央處理器（不過後來改採用與日本團隊相同的SH-4）；然而，由於3dfx在1997年[首次公開募股](../Page/首次公開募股.md "wikilink")（IPO）時基於法律義務披露了與世嘉的商業合約，使日本世嘉高層感到不滿，最終中止了與3dfx的合約，並全面放棄黑帶計畫，轉向杜拉爾計畫。據前美國世嘉副總裁和日本電氣品牌經理查理斯·貝爾費爾德（Charles
Bellfield）的說法，採用SH-4和PowerVR架構可充分發揮主機的硬體效能，成本也較其他架構低廉許多；他並表示，「世嘉與日本電氣都是源自日本的這個事實，可能也是讓世嘉決定採用其硬體設計的因素之一。」3dfx還曾為此分別向世嘉與日本電氣提起訴訟，不過最終皆於法院外和解。世嘉採用PowerVR架構的原因有一大部分要歸因於長期為其電子遊戲平臺開發作品的[第三方遊戲開發商](../Page/第三方遊戲開發者.md "wikilink")[藝電](../Page/藝電.md "wikilink")（EA）；藝電雖然有嘗試對3dfx的處理器進行一段時間的測試，不過由於開發團隊無法完全活用處理器機能，同時又有傳聞稱3dfx的晶片效能不如他廠，最後仍決定以SH-4為首選。此外，據時任世嘉硬體部門總經理萩原四郎和世嘉子公司Cross
Products的總經理伊恩·奧利弗（Ian
Oliver）的說法，SH-4在世嘉決定採用時仍未完全研發完成，但經過高層漫長的審議過程後仍然決定採用，因為它是當時唯一可以提供次世代硬體所需的3D幾何運算機能的處理器。至1998年2月，世嘉將已經接近完成的次世代家用遊戲機開發計畫代號更名為「武士刀」（Katana）。

以世嘉土星過高的硬體成本為鑑，Dreamcast的整體設計理念雖然與之前的歷代世嘉電子遊戲機大致相同，但世嘉改以[電腦業界普遍常用的](../Page/電腦.md "wikilink")取代客製化組件，藉以降低成本。評論者達米安·麥克費蘭（Damien
McFerran）表示：「Dreamcast的主機板是兼具簡潔的設計與相容性的工業傑作。」此外，儘管因為會使每部主機增加15美元的成本而遭到部分世嘉員工的反對，但華裔經濟學家布拉德·黃（Brad
Huang）仍成功說服了時任日本世嘉總裁[大川功為Dreamcast加入內建](../Page/大川功.md "wikilink")[撥接網路機能](../Page/撥號連線.md "wikilink")，並因應未來數據機技術進展的可能性將其模組化，以便替換。

世嘉以其與[山葉共同研發的](../Page/山葉.md "wikilink")[GD-ROM作為Dreamcast之主要儲存媒體格式](../Page/GD-ROM.md "wikilink")。GD-ROM光碟之製造成本與[CD-ROM接近](../Page/CD-ROM.md "wikilink")，並較[DVD-ROM低廉](../Page/DVD-ROM.md "wikilink")。由於一片普通GD-ROM光碟可以容納約1.2GB的數據，雖然仍無法阻止盜版商[非法複製Dreamcast軟體](../Page/盜版.md "wikilink")，不過在一般CD-ROM光碟容量僅有650MB的情況下，遊戲多半必須進行大量刪減或壓縮，可間接使盜版商喪失盜版興趣。Dreamcast主機搭載之[作業系統是由](../Page/作業系統.md "wikilink")[微軟開發的客製化版本](../Page/微軟.md "wikilink")[Windows
CE](../Page/Windows_CE.md "wikilink")，引入了[DirectX和](../Page/DirectX.md "wikilink")[動態連結函式庫技術](../Page/動態連結函式庫.md "wikilink")，使開發者能更加快速地將[個人電腦平臺上的電子遊戲移植至Dreamcast平臺](../Page/個人電腦.md "wikilink")。

1998年5月21日，世嘉於特別活動「SEGA New Challenge Conference
'98」中首次正式發表次世代家用遊戲機相關資訊，並宣布主機名稱為「Dreamcast」\[1\]。Dreamcast一詞由「夢想」（Dream）與「傳播」（Broadcast）組合而成，意為「傳播夢想」；世嘉為了決定次世代主機的名稱特地舉辦了競賽，並自超過5000個投稿中選出了這個名字；有傳聞稱Dreamcast之名和其識別標誌是由遊戲開發者[飯野賢治設計的](../Page/飯野賢治.md "wikilink")，然而從未得到世嘉官方證實。另外，Dreamcast的開機音樂由著名音樂家[坂本龍一創作](../Page/坂本龍一.md "wikilink")。世嘉原計劃自Dreamcast主機外殼中完全刪除公司名稱，並建立類似於[PlayStation的獨立品牌](../Page/PlayStation.md "wikilink")，但入交最終決定將世嘉的識別標誌保留在主機外部。共計世嘉在Dreamcast之硬體開發上耗資達5千萬至8千萬美元、在軟體服務開發上耗資1.5億至2億美元、在宣傳行銷上又花了至少3億美元，總計耗資達5億至5億8千萬美元，自汽車產業發跡的入交還曾將此一金額比喻為「與設計一部全新汽車相當」。

### 發售

雖然在Dreamcast首發前半年持續遭受財務損失，但世嘉仍對這部次世代主機寄予厚望。Dreamcast獲得許多玩家與零售商的關注，並引來了大量訂單；世嘉並宣布其招牌[音速小子系列的下一款主線遊戲](../Page/音速小子系列.md "wikilink")《[音速小子大冒險](../Page/音速小子大冒險.md "wikilink")》將與Dreamcast同步發售，並先在[東京國際論壇大廳舉辦遊戲試玩活動](../Page/東京國際論壇.md "wikilink")。然而因PowerVR晶片之成品良率不如預期，使Dreamcast主機之預定生產數量無法達到目標；在首批庫存中超過一半以上已經被預訂的情況下，世嘉被迫提早中止在日本的預售活動。

1998年11月27日，Dreamcast在日本首發，售價為29,000[日圓](../Page/日圓.md "wikilink")，首批主機存貨在一天內銷售一空；但在與主機同時首發的四款遊戲中，只有《[VR快打3](../Page/VR快打3.md "wikilink")》達到了世嘉設定的銷量標準。世嘉估計，在足夠的遊戲陣容（如早先已經宣告延遲上市的《音速小子大冒險》及《》等）支援下，可以再額外售出20萬到30萬部主機；然而這些遊戲雖然在接下來的幾週內接連發售，但Dreamcast的銷售速度仍然比預期慢。入交認為世嘉能在1999年2月之前在日本賣出超過100萬部Dreamcast，但是實際上銷量僅有不到90萬部，這使得世嘉無法在索尼及任天堂等其他製造商的次世代主機加入競爭前奠立足夠的市場基礎。甚至有報導稱部分日本消費者將Dreamcast主機退貨，並使用所得退款購買PlayStation遊戲軟體。1999年7月，《》發售並大賣，是Dreamcast在日本的第一次重大成功。在Dreamcast於北美地區發售前，世嘉將Dreamcast的售價降低至19,900日元，雖使硬體毛利下滑，但有效提升了銷量；加上由[南夢宮](../Page/南夢宮.md "wikilink")（後與[萬代合併為](../Page/萬代.md "wikilink")[萬代南夢宮](../Page/萬代南夢宮.md "wikilink")）開發的《[劍魂](../Page/劍魂_\(格鬥遊戲\).md "wikilink")》發售，使世嘉的公司股價回漲約17％。

為了確保Dreamcast在北美地區發售時擁有足夠的遊戲陣容，美國世嘉利用Dreamcast在日本發售至在北美地區發售之間約十個月的時間差與為Dreamcast開發了四款首發遊戲的[Midway進行合作](../Page/Midway.md "wikilink")，確保Dreamcast在北美地區發售時擁有至少15個首發遊戲。儘管一開始因世嘉土星的失敗對世嘉抱持著懷疑，但斯托拉爾仍成功地修復了與美國各大主要零售商的關係，並使世嘉預售了約30萬部Dreamcast。美國世嘉市場行銷部門副總裁與[博達大橋廣告及Access](../Page/博達大橋廣告.md "wikilink")
Communications共同製作了一支長約15秒的[電視廣告](../Page/電視廣告.md "wikilink")「It's
Thinking」，主打Dreamcast的主機硬體機能。摩爾說：「我們期望這支廣告能夠吸引消費者的目光，同時為我們過去犯下的錯誤道歉。」1999年8月11日，美國世嘉宣布斯托拉爾被解雇，由摩爾主導主機發售事宜。

儘管與Midway的合作十分順利，但是原世嘉最大的合作廠商藝電卻在Dreamcast於北美地區發售前表明不會為Dreamcast研發遊戲，對Dreamcast的遊戲陣容完整度造成打擊。時任藝電執行長表示：「（世嘉）已經無法提供我們（藝電）與過去五年內相同等級的待遇。」但斯托拉爾反駁此說法，並聲稱時任藝電總裁拉里·普羅布斯特（Larry
Probst）想要「進一步壟斷Dreamcast平臺上的運動遊戲市場」；由於世嘉甫收購了運動遊戲開發商[Visual
Concepts](../Page/Visual_Concepts.md "wikilink")，他無法接受藝電的條件。雖然Dreamcast不會有任何藝電出品的體育遊戲，但由Visual
Concepts主導研發的《世嘉運動》（SEGA Sports）系列遊戲將適度地填補空缺。

1999年9月9日，Dreamcast於北美發售，售價為199[美元](../Page/美元.md "wikilink")，以「9/9/99，199美元」（9/9/99
for
$199）為促銷口號\[2\]，並有18款首發遊戲。Dreamcast在發售24小時內銷量超過225,132部，收益達9840萬美元，摩爾稱之為「娛樂零售業史上最重要的24小時」。在發售後兩週內，Dreamcast主機銷量突破50萬部。該年聖誕節，世嘉於北美家用遊戲機市場之市占率達到31％。11月4日，美國世嘉宣布Dreamcast銷量突破100萬部。

1999年10月14日，Dreamcast於歐洲地區發售，售價為200[英鎊](../Page/英鎊.md "wikilink")。至11月24日，銷量已達40萬部。該年聖誕節，歐洲世嘉宣布Dreamcast銷量達50萬部，比預定時程提前了六個月。但銷售速率隨後減緩，截至2000年10月，Dreamcast在歐洲銷量僅有100萬部。為了增加曝光度，世嘉贊助了四支足球隊，包括[兵工廠足球俱樂部](../Page/兵工廠足球俱樂部.md "wikilink")（英國）、[聖德田競技俱樂部](../Page/聖德田競技俱樂部.md "wikilink")（法國）、[桑普多利亞足球俱樂部](../Page/桑普多利亞足球俱樂部.md "wikilink")（義大利）及[拉科魯尼亞體育俱樂部](../Page/拉科魯尼亞體育俱樂部.md "wikilink")（西班牙）等。

### 競爭

儘管Dreamcast在首發時取得一定成功，索尼仍然在1999年年底以其初代家用遊戲機PlayStation占有北美地區整體電子遊戲機之市場達60％。1999年3月2日，索尼在一份刻意公開、疑似[霧件的報告中披露了部分](../Page/霧件.md "wikilink")「次世代PlayStation」的硬體細節。索尼的報告重點集中於「次世代PlayStation」（又稱「PlayStation
2」）將採用的中央處理器「」，報告指出該處理器由索尼與[東芝共同開發](../Page/東芝.md "wikilink")，頻率達294
MHz，時任[索尼電腦娛樂社長兼執行長](../Page/索尼電腦娛樂.md "wikilink")[久夛良木健更聲稱該處理器頻寬為當時電腦處理器平均的](../Page/久夛良木健.md "wikilink")1000倍，且[每秒浮點運算次數達](../Page/每秒浮點運算次數.md "wikilink")62億次，可與當時大多數[超級電腦相比](../Page/超級電腦.md "wikilink")。索尼耗資逾12億美元投資興建[大型積體電路](../Page/大规模集成电路.md "wikilink")以製造PlayStation
2的中央處理器，意圖使PlayStation 2可以渲染比任何電子遊戲機都要更多的多邊形。據索尼表示，PlayStation
2每秒可以渲染7500萬個原始多邊形，而在不含任何物理輔助效果下也有3800萬個原始多邊形；索尼並估計，PlayStation
2每秒可以渲染750萬至1600萬個多邊形。相較之下，Dreamcast每秒估計僅能渲染300萬到600萬個多邊形。同時，PlayStation
2將採用[DVD-ROM格式](../Page/數碼多功能影音光碟.md "wikilink")，可以保存較Dreamcast的GD-ROM格式達4倍之多的數據。以主機強大的性能以及與網際網路無限的可能性，索尼期望PlayStation
2成為家庭娛樂未來的標竿。此外，還有謠言聲稱PlayStation
2能夠指示導彈航線、擁有《[玩具總動員](../Page/玩具總動員.md "wikilink")》電影等級的輸出畫質，久夛更表示宣稱PlayStation
2能有讓消費者「身歷《[駭客任務](../Page/駭客任務.md "wikilink")》世界的能力」。此外，索尼宣布PlayStation
2將向下相容數百款的初代PlayStation遊戲作品。雖然後來的深入報告顯示，PlayStation
2沒有如初始預期的那麼強大，而且軟體開發製程十分複雜，但仍然使Dreamcast略顯疲弱。同年，任天堂與微軟皆開始研發次世代家用遊戲機。

世嘉的初始銷售動力證明是短暫的。美國DC的銷售額，自1999年年底超過150萬部，到2000年1月起就開始下降。日本銷售額疲弱導致 世嘉
在截至2000年3月的財政年度累計虧損淨額達42.88億日元（4.04
億美元），與去年的虧損42.881億日元相當，並象徵著世嘉連續的第三年虧損。儘管世嘉的總體銷售額增長了27.4％，而且Dreamcast在北美和歐洲的銷售額大大超過了公司的預期，但銷售額的增長恰好與在北美市場推出Dreamcast及軟件所需的投資相抵。同時，日益惡化的市場條件降低了世嘉日本[街机業務的盈利能力](../Page/街机.md "wikilink")，促使該公司關閉246個據點。知道他們“必須在魚咬人的地方捕魚”，美國世嘉總裁彼得·摩爾（他在
Stolar 被解僱後就職）和日本世嘉的開發者集中心力在美國市場，準備迎擊即將推出的PlayStation
2。為此，美國世嘉推出了自己的互聯網服務[Sega.com](../Page/Sega.com.md "wikilink")，由CEO
Brad Huang 領導。2000年9月7日，Sega.com
推出了[SegaNet](../Page/SegaNet.md "wikilink")，Dreamcast的互聯網游戲服務，每月的訂閱價格為
21.95 美元。雖然世嘉在美國祇發行了一個支援 Dreamcast的在線多人遊戲（《[ChuChu
Rocket！](../Page/ChuChu_Rocket！.md "wikilink")》，由[Sonic
Team所開發的益智遊戲](../Page/Sonic_Team.md "wikilink")），在推出[SegaNet](../Page/SegaNet.md "wikilink")（允許用戶聊天，發送電子郵件和瀏覽網絡）時結合了《[NFL
2K1](../Page/NFL_2K1.md "wikilink")》（橄榄球遊戲，包含強大的在線組件），旨在增加美國市場對Dreamcast的需求。該服務後來支持的遊戲包括*[Bomberman
Online](../Page/Bomberman_Online.md "wikilink")*、《[梦幻之星在线](../Page/梦幻之星在线.md "wikilink")》(
Phantasy Star
Online)、《[雷神之锤III竞技场](../Page/雷神之锤III竞技场.md "wikilink")》和《[魔域幻境之浴血戰場](../Page/魔域幻境之浴血戰場.md "wikilink")》等。

SegaNet在當年9月7日啟用，世嘉舉行了廣告活動，包括連續第二年贊助MTV音樂錄影帶大獎。世嘉對在線遊戲採用了積極的定價策略。在日本，每台出售的
Dreamcast
出售包括免費一年的互聯網，由世嘉總裁大川功個人支付。在SegaNet推出之前，世嘉向從Sega.com購買互聯網服務的Dreamcast用戶提供了200美元的回扣。為了增加SegaNet在美國的吸引力，世嘉將Dreamcast的價格降至
149 美元（PS2 的美國發布價格為299美元），並為Dreamcast（和一個免費的Dreamcast鍵盤）降價至149美元。

摩爾表示，DC將需要在2000年底前在美國賣出500萬部，以保持與PlayStation
2抗衡的實力，但世嘉以大約300萬部的銷售量最終無法達到這個目標。此外，世嘉試圖通過降低價格和現金回扣來刺激Dreamcast的銷售量，導致公司財務損失不斷增加。截至2000年9月的前六個月中，世嘉的預估利潤為179.8億日元（約
1.6311
億美元），預計虧損為236億日元。最終虧損數字增加了一倍多，到了虧損583億日元，2001年3月世嘉公司淨虧損為517億日元（約4.175
億美元）。雖然PlayStation 2由於製造故障，在當年10月26日美國發布時，預定的100萬部PlayStation
2中只有50萬部準時發售，但這並沒有像預期一樣使Dreamcast受益，許多消費者繼續等待PlayStation
2(而PSone，原版 PlayStation 的改裝版本，是2000年旺季時美國最暢銷的遊戲主機。摩爾稱，“我們所依賴的PlayStation
2效果並不奏效......人們會盡可能長時間地等待......實際上發生的是PlayStation
2的缺貨凍結了市場。”最終，索尼和任天堂分別持有美國視頻遊戲機市場的50％和35％，而世嘉只有15％。據貝爾菲爾德稱，Dreamcast軟件與硬件以8比1的比例銷售，但這個比例“過小的
DC 銷售部數無法給我們帶來收入...以保持在市場中的長久性。

### 殞落

2000年5月22日，大川取代了入交昭一郎，擔任世嘉總裁。大川早已公開主張世嘉放棄遊戲主機業務。他並不是唯一持這意見的人：世嘉聯合創始人大衛·羅森（David
Rosen）就曾經說過：“總覺得將精力放在世嘉的硬件是愚蠢的。”前美國世嘉總裁斯托拉曾經建議世嘉應該把公司賣給微軟。2000年9月，摩根和貝爾菲爾德在會見日本世嘉的高管和公司主要遊戲開發工作室的負責人時，就建議世嘉放棄主機業務，僅專注於軟件工作。2001年1月31日，世嘉宣佈在3月31日之後停止Dreamcast開發，並將公司重組為“與主機平部無關”的第三方開發商(這個決定是由摩爾下達)。世嘉還宣布將Dreamcast的價格降至99美元，以消除其庫存(截至2001年4月為止估計為93萬部)。經過進一步減價至79美元之後，Dreamcast以零售價49.95美元清倉。最後一批Dreamcast由世嘉內部遊戲開發工作室的所有九位成員以及Visual
Concepts和Wave
Master的負責人簽名，通過GamePro雜誌的一場比賽與55個第一方Dreamcast遊戲送出。1999年夏天借給世嘉五億元的大川功於2001年3月16日去世，在他去世前不久就放棄了世嘉積欠的債務，並回收了他的6.95億美元世嘉和CSK股票，幫助該公司在重組過程中倖存下來。作為這次重組的一部分，世嘉東京總部的近三分之一雇員在2001年被解僱。

全球一共銷售了約913萬部Dreamcast。Dreamcast業務終止後，遊戲商仍然持續開發該主機之遊戲(特別是在日本)；在美國，遊戲發行一直持續到2002年上半年。日本世嘉繼續提供修理Dreamcast服務直到2007年。截至目前，主機仍然通過各種MIL-CD獨立版本支持。經過連續五年的財務損失，世嘉終於在2003財政年度結束虧損。

Dreamcast失敗的原因包括SONY對PlayStation
2的炒作；缺乏[藝電和](../Page/艺电.md "wikilink")[史克威爾](../Page/史克威爾.md "wikilink")(
分別被認為是美國和日本最受歡迎的第三方廠商)大廠支持；不被看好對世嘉公司的未來有益，大川對產品的承諾不足；世嘉缺乏廣告資金，貝爾菲爾德懷疑世嘉花了1億美元在美國推廣Dreamcast；市場還沒有準備好進行網絡遊戲；世嘉關注核心遊戲玩家超過主流消費者，以及推出時間不佳。大概最常被引用的原因是世嘉的聲譽被之前幾個生命週期短暫的世嘉遊戲主機損害。Blake
Snow在GamePro雜誌上表示：“Dreamcast領先世代幾年，但還是被土星、Sega 32X和[Sega
CD的負面聲譽所拖累](../Page/Mega-CD.md "wikilink")，導致遊戲開發商及一般玩家不信任世嘉的能力。“Dan
Whitehead在Eurogamer雜誌上指出，消費者的”等待觀察“模式和缺乏EA的支持是世嘉下滑的原因，總結出”世嘉上世紀90年代的錯誤決策使遊戲玩家和遊戲出版商對任何新主機都抱有警惕。“[1UP.com的傑里米教區表示](../Page/1UP.com.md "wikilink")：”儘管輿論指控索尼，並責怪他們通過超賣
PlayStation 2 擊敗 Dreamcast ...... 這樣的立場有一定程度的不誠實... (世嘉對[Sega
CD](../Page/Mega-CD.md "wikilink")、32X和土星等硬件的支持令玩家不滿，許多消費者覺得在投資昂貴的世嘉機器後卻發現缺乏遊戲的支持，對他們來說就像燒錢。)”

世嘉重組的消息得到了支持。根據[IGN的Travis](../Page/IGN.md "wikilink") Fahs
的說法，“世嘉是一家具有創造力的公司，擁有迅速擴張的平臺，他們處於開創新生活的最佳地位。”前工作設計總裁維克多·愛爾蘭寫道：“這是一件好事，因為現在世嘉將會生存下去，做最好的軟件。”《[新聞周刊](../Page/新闻周刊.md "wikilink")》的工作人員表示：“從索尼克到沉默，世嘉的程序員已經產生了一些互動媒體歷史上最有吸引力的經歷
...... 在去除主機失敗的干擾後，這些世界級的程式設計師能夠發揮創意，專心做他們想要做的東西。”

## 技術規格

### 硬體

[File:Sega-Dreamcast-Deconstructed-01.jpg|DC](File:Sega-Dreamcast-Deconstructed-01.jpg%7CDC)
主機拆解開來的樣子
[File:Sega-Dreamcast-Internals.jpg|DC](File:Sega-Dreamcast-Internals.jpg%7CDC)
主機內結構，包括硬碟、電源及主機板等
[File:Sega-Dreamcast-Motherboard-Top.jpg|DC](File:Sega-Dreamcast-Motherboard-Top.jpg%7CDC)
主機板，包括 CPU、GPU 等組件
[File:Sega-Dreamcast-Modem-Dialup-Motherboard-T.jpg|DC](File:Sega-Dreamcast-Modem-Dialup-Motherboard-T.jpg%7CDC)
的數據機基板正面
[File:Sega-Dreamcast-Modem-Dialup-Motherboard-B.jpg|DC](File:Sega-Dreamcast-Modem-Dialup-Motherboard-B.jpg%7CDC)
的數據機基板背面 <File:SH7091> 01.jpg|SH-4 HD6417091處理器

Dreamcast的尺寸為 190 [公釐](../Page/毫米.md "wikilink")×195.8 公釐×75.5 公釐（7.48
[英寸](../Page/英寸.md "wikilink")×7.71 英寸×2.97 英寸），重1.5
[公斤](../Page/千克.md "wikilink")（3.3
[英磅](../Page/磅.md "wikilink")）。Dreamcast的主CPU是[對稱性多重處理](../Page/对称多处理.md "wikilink")[超純量體系結構的](../Page/超純量.md "wikilink")[Hitachi
SH-4](../Page/Hitachi_SH-4.md "wikilink") 32
位元[精簡指令集](../Page/精简指令集.md "wikilink")[處理器](../Page/處理器.md "wikilink")\[3\]，[每秒指令](../Page/每秒指令.md "wikilink")（IPS）達360萬次，頻率為200
MHz，具有8 KB的CPU快取、16 KB的數據快取以及128
位元面向圖形浮點運算器，[每秒浮點運算次數](../Page/每秒浮點運算次數.md "wikilink")（FLOPS）達到1.4
億次。DC的[NEC](../Page/日本電氣.md "wikilink")
[PowerVR2](../Page/PowerVR.md "wikilink")（頻率100 MHz）
渲染引擎整合了系统的[全定制芯片](../Page/特殊應用積體電路.md "wikilink")，能夠每秒繪製超過300萬個多邊形和延遲著色。據世嘉估計，理論上Dreamcast每秒能夠渲染700萬個原始多邊形或者600萬個紋理，但是“遊戲邏輯和物理降低了圖形性能”。硬件效果包括[三線性濾波](../Page/三線性濾波.md "wikilink")、[古拉陰影](../Page/古拉陰影.md "wikilink")、[深度緩衝](../Page/深度缓冲.md "wikilink")、[反鋸齒](../Page/反鋸齒.md "wikilink")、[像素半透明分选和](../Page/像素半透明分选.md "wikilink")[凹凸貼圖等](../Page/凹凸贴图.md "wikilink")。DC可以同時輸出約1677萬色（16777216色），並以640×480的視頻[分辨率顯示隔行或逐行掃描視頻](../Page/分辨率.md "wikilink")。DC預設使用[AV端子](../Page/AV端子.md "wikilink")，當時其被認為是影音的連接標準的32位元[Yamaha
AICA聲音處理器](../Page/Yamaha_AICA.md "wikilink")（頻率為67
MHz）為[ARM7架構](../Page/ARM7.md "wikilink")，可使用[脈波編碼調變](../Page/脈衝編碼調變.md "wikilink")（PCM）或[動態脈波編碼調變](../Page/動態脈波編碼調變.md "wikilink")（ADPCM）生成聲音，是世嘉土星音效處理性能的十倍。Dreamcast具有16
MB的主RAM，另外還有8 MB的RAM用於圖形紋理、2 MB的RAM用於聲音。DC使用12倍速的Yamaha
[GD-ROM驅動器讀取媒體](../Page/GD-ROM.md "wikilink")。除了[Windows
CE](../Page/Windows_Embedded_Compact.md "wikilink")，Dreamcast還支持幾個世嘉和[中介軟體的](../Page/中间件.md "wikilink")[應用程式介面](../Page/应用程序接口.md "wikilink")。在大多數地區，Dreamcast包含用於連接網路的[模組化撥接數據機](../Page/模組.md "wikilink")，原設計為可因應未來情況升級，但因DC停產而沒有下文。原始的日本機型和[PAL機型的傳輸速率為每秒](../Page/PAL制式.md "wikilink")33.6
Kb（注意不是KB而是Kb），而在1999年9月9日以後在美國和日本銷售的遊戲機則為每秒56 Kb。

### 機型

[File:Sega-Dreamcast-Sports-Black-Console.jpg|SEGA](File:Sega-Dreamcast-Sports-Black-Console.jpg%7CSEGA)
Sports塗裝黑色限定版機型 <File:Fuji> Divers 2000 series CX-1 Dreamcast
08.jpg|音速小子造型的限定版機型 <File:Hello> Kitty Dreamcast.jpg|Hello
Kitty限量版機型

自1998年11月SEGA在日本推出初代Dreamcast以來，於世界各地推出的機型：

  - 日版 SEGA Dreamcast（HKT-3000）：第一代DC。
  - 臺版 SEGA
    Dreamcast（HKT-3000）：1998年11月由[太電欣榮實業代理同步推出](../Page/太電欣榮實業.md "wikilink")，但移除了預先安裝的數據機。
  - 亞洲版 SEGA
    Dreamcast（HKT-3010）：有鑑於初期DC出現偶發過熱異常的比例偏高，因此SEGA後續強化了DC的散熱設計，推出了使用較強的金屬制散熱風扇的微幅改版機種，不過外觀與功能沒有變化。
  - 北美版 SEGA
    Dreamcast（HKT-3020）：經過1999年5月美國E3的強力宣傳後，於1999年9月9日推出。相較於日版機型，數據機速率由每秒33.6
    Kb升級至56 Kb。
  - 歐版 SEGA
    Dreamcast（HKT-3030）：1999年10月14日推出，設計與北美版大致相同（但螺旋商標因某些因素從橘色改為藍色），數據機則是日版的每秒33.6
    Kb規格。
  - 日 / 亞洲版 SEGA Dreamcast（HKT-5000 / 5010）：DC使用的是日本山葉公司研發的GD-ROM光碟，一般
    CD-ROM
    光碟機無法讀取，具保護功效；不過DC也支援混合音樂CD與多媒體資料的「MIL-CD」多媒體光碟，駭客於是透過此一管道破解了DC的保護措施，將GD-ROM遊戲光碟的資料抽取出來，經過資料刪減壓縮後複製到容量較小的CD-R光碟上執行，造成嚴重盜版問題。為了解決問題，SEGA
    於是修改了 DC
    的設計，自2000年10月起推出取消MIL-CD功能，讓盜版遊戲光碟無法執行的改版機種，後續亞洲版也跟進了此一改版。　　，

除了一般機型外，世嘉還推出了各種特別版機型：

  - Divers 2000
    CX-1（CX-1）：於2000年4月21日由SEGA與[日本富士電視臺合作推出](../Page/富士電視台.md "wikilink")，限量5000臺，價格88888日圓。外型像世嘉的吉祥物[音速小子](../Page/刺猬索尼克.md "wikilink")，內建電視和電話會議用軟體。
  - "Dreamcast R7"：為特種營業場所（如[柏青哥等](../Page/彈珠機.md "wikilink")）所使用的機型。
  - 「Hello Kitty Dreamcast 組合」：由世嘉與[Hello
    Kitty合作所推出的限定版透明粉紅](../Page/Hello_Kitty.md "wikilink")
    / 透明粉藍機型。
  - 「櫻粉紅機」：以《[櫻花大戰](../Page/櫻花大戰_\(遊戲\).md "wikilink")》遊戲為主題的櫻粉紅機。

### 配件

<File:Sega> Dreamcast Controller
(PAL).png|DC的控制器，此控制器為[PAL版](../Page/PAL制式.md "wikilink")。
[File:Sega-Dreamcast-VMU.jpg|DC](File:Sega-Dreamcast-VMU.jpg%7CDC) 的核心組件
VMU。 <File:Dreamcast> Vibration pack.jpg|世嘉為DC推出的震動包。 <File:Dreamcast>
light gun.jpg|世嘉在日本推出的DC專用光線槍。

Dreamcast的[遊戲控制器包括四個基礎按鈕](../Page/游戏控制器.md "wikilink")、兩個[肩鍵](../Page/肩鍵.md "wikilink")、一組[十字鍵](../Page/十字键.md "wikilink")、一個[類比搖桿和一個小型顯示器槽](../Page/类比摇杆.md "wikilink")。DC主機具有四個用於控制器的端口，但它僅隨附一個控制器。DC控制器設計造型特殊，評價普遍不適應此設計：[IGN的工作人員寫道](../Page/IGN.md "wikilink")：“...與大多數控制器不同，世嘉（DC）的控制器迫使用戶的手長時間處於不舒服的位置...“；英國[Edge雜誌工作人員評論](../Page/Edge_\(杂志\).md "wikilink")：“只是（世嘉）土星控制器的醜陋進化罷了”；[Game
Informer雜誌的安迪](../Page/Game_Informer.md "wikilink")·麥克納馬拉（Andy
McNamara）評論：“不是很好”；[1UP.com的Sam](../Page/1UP.com.md "wikilink")
Kennedy評論其為“跛腳”。與SEGA
CD和SEGA土星（使用內部儲存空間）不同，Dreamcast使用[VMU](../Page/VMU.md "wikilink")（Visual
Memory Unit），一張大小約128
KB的[存儲卡](../Page/記憶卡.md "wikilink")，用於數據存儲。VMU正面具有小型[LCD屏幕](../Page/液晶显示器.md "wikilink")（解析度為48\*32），還有一組十字鍵和四個基礎按鍵，能使玩家進行一些基本的[迷你遊戲](../Page/小游戏.md "wikilink")（這些迷你遊戲可以脫離DC主機直接運行在VMU上，當VMU和DC控制器連接後，迷你遊戲中獲得的道具可以在DC主機遊戲中使用）；在插入DC控制器的小型顯示器擴充槽後，VMU將作為第二螢幕使用，可顯示額外的遊戲資訊。每一臺DC控制器都有兩個插槽用於連接VMU或其他配件，甚至還可以連接兩個VMU互相交換數據。在採用將VMU插槽併入控制器的設計之後，世嘉的工程師發現了許多其他擴充槽的用途，因此在控制器上添加了第二個插槽。這個插槽通常用於[振動包](../Page/振動包.md "wikilink")（如世嘉推出的“Jump
Pack”和[Performance公司推出的](../Page/Performance公司.md "wikilink")“Tremor
Pak”等）。它也可以用於其他外設，如支持語音控制和玩家交流的[麥克風等](../Page/麥克風.md "wikilink")。數家第三方廠商也推出存儲卡，有些也像VMU一樣包含LCD屏幕。[iOmega曾預計發布相容於Dreamcast的](../Page/iOmega.md "wikilink")[極碟](../Page/極碟.md "wikilink")（zip
Drive），可使用特製高容量[軟碟片存儲高達](../Page/軟碟片.md "wikilink")100 MB的數據，但從未正式發售。

[美加獅](../Page/美加獅.md "wikilink")（Mad
Catz）等第三方公司也針對Dreamcast推出各種控制器，包括額外的按鈕和其他新增功能；有些第三方公司還為特定格鬥遊戲製作了街機式搖桿控制器，如[Agetec公司推出的](../Page/Agetec.md "wikilink")“Arcade
Stick”和[Interact的](../Page/Interact.md "wikilink")“Alloy Arcade
Stick”等。美加獅和Agetec公司還為賽車遊戲推出了方向盤控制器。世嘉決定不在美國市場推出官方光線槍，但有一些第三方的光線槍是可以使用的。
Dreamcast支援世嘉推出的“reel and rod”釣魚遊戲控制器，雖然這是專為釣魚游戲如《[Sega Bass
Fishing](../Page/Sega_Bass_Fishing.md "wikilink")》製作的，但也可於遊戲《劍魂》中使用，它將控制器垂直和水平的動作轉換成屏幕上的劍術，這被視為是[Wii遙控器的前身](../Page/Wii遙控器.md "wikilink")。自世嘉街機移植的《[電腦戰機
Oratorio Tangram](../Page/電腦戰機.md "wikilink")》（Cyber Troopers Virtual-On
Oratorio
Tangram）日版支援雙搖桿，但該遊戲的美國出版商[動視公司選擇不在美國發行此配件](../Page/动视.md "wikilink")。Dreamcast可以連接到[SNK推出的](../Page/SNK.md "wikilink")[Neo
Geo Pocket
Color掌機](../Page/Neo_Geo_Pocket_Color.md "wikilink")（是後來任天堂[GameCube-Game
Boy
Advance連接線的前身](../Page/GameCube-Game_Boy_Advance連接線.md "wikilink")），透過專屬傳輸線的連動，可以讓相對應的遊戲有所變化，例如快速賺錢，或是解開隱藏人物角色等。世嘉還推出了[Dream
Eye視訊鏡頭](../Page/Dream_Eye.md "wikilink")，可通過互聯網視頻聊天。此外，世嘉還研究了允許用戶與Dreamcast進行電話通話的系統，並與[摩托羅拉討論了開發具有互聯網功能的手機](../Page/摩托羅拉.md "wikilink")，該手機將使用遊戲主機的技術來快速下載遊戲，但最後皆因DC的停產而告終。DC預設使用AV端子，而世嘉和第三方廠商也製造了[RF調製器和](../Page/射頻.md "wikilink")[S-Video電纜](../Page/S-Video.md "wikilink")，[VGA轉接線則允許DC遊戲在支援VGA的顯示器上播放](../Page/视频图形阵列.md "wikilink")。

## 遊戲陣容

## 參見

  - [PlayStation 2](../Page/PlayStation_2.md "wikilink")

  - [Xbox Live](../Page/Xbox_Live.md "wikilink")

  -
## 參考資料

## 外部連結

  - [世嘉硬體大百科](https://sega.jp/history/hard/dreamcast/)
  - [SEGA Retro](https://segaretro.org/Sega_Dreamcast)
  - [Gaming Historian](https://www.youtube.com/watch?v=6xdBVHSrdzg)
  - [Dreamcast宣傳用光碟影音](https://www.youtube.com/watch?v=WIXjCgagBPM)

[Category:1998年面世的產品](../Category/1998年面世的產品.md "wikilink")
[Category:第六世代遊戲機](../Category/第六世代遊戲機.md "wikilink")
[Category:世嘉遊戲機](../Category/世嘉遊戲機.md "wikilink")
[Category:家用遊戲機](../Category/家用遊戲機.md "wikilink")

1.
2.
3.