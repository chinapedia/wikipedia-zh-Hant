[DUGA_Radar_Array_near_Chernobyl,_Ukraine_2014.jpg](https://zh.wikipedia.org/wiki/File:DUGA_Radar_Array_near_Chernobyl,_Ukraine_2014.jpg "fig:DUGA_Radar_Array_near_Chernobyl,_Ukraine_2014.jpg")
[Woodpecker_array.jpg](https://zh.wikipedia.org/wiki/File:Woodpecker_array.jpg "fig:Woodpecker_array.jpg")的Duga-1阵列。右侧成对的圆柱/圆锥笼阵列是[有源振子](../Page/八木天线.md "wikilink")，由悬挂在顶部右侧平台的信号线提供信号。\]\]
[Duga_radar_station_within_the_Chernobyl_Exclusion_Zone,_Ukraine_(09).JPG](https://zh.wikipedia.org/wiki/File:Duga_radar_station_within_the_Chernobyl_Exclusion_Zone,_Ukraine_\(09\).JPG "fig:Duga_radar_station_within_the_Chernobyl_Exclusion_Zone,_Ukraine_(09).JPG")

**Duga远程警戒雷达**（）是[苏联在](../Page/苏联.md "wikilink")[冷战时期所建设的](../Page/冷战时期.md "wikilink")[超视距雷达](../Page/超视距雷达.md "wikilink")，是苏联[反弹道导弹远程警戒网络的一部分](../Page/反弹道导弹.md "wikilink")，于1976年7月至1989年12月期间服役。该系统总共部署了两套，一套位于[乌克兰苏维埃社会主义共和国](../Page/乌克兰苏维埃社会主义共和国.md "wikilink")（今天的[乌克兰](../Page/乌克兰.md "wikilink")）境内的[切尔诺贝利与](../Page/切尔诺贝利.md "wikilink")[切尔尼戈夫附近](../Page/切尔尼戈夫.md "wikilink")，另一套位于东[西伯利亚](../Page/西伯利亚.md "wikilink")。

Duga雷达极为强大，峰值功率约10[MW](../Page/MW.md "wikilink")，工作在[短波波段](../Page/短波.md "wikilink")。其信号往往突然出现，发出10 Hz的尖锐敲击声\[1\]，故得名**俄罗斯啄木鸟**。它随机的[跳频干扰了正常的](../Page/跳频.md "wikilink")[无线电广播](../Page/无线电广播.md "wikilink")、[业余无线电台以及其他传输应用](../Page/业余无线电台.md "wikilink")，在全球范围内的许多国家引发了数以千计的投诉。这个干扰让世界范围内的业余无线电操作员深受其苦，并导致了一个蓬勃发展的行业，研制“啄木鸟[滤波器](../Page/滤波器.md "wikilink")”和噪音屏蔽装置。业余无线电操作员曾经提议了一个对抗干扰的办法，那就尝试是“阻塞”这个信号，通过发送同步、未经[调制的连续波信号](../Page/调制.md "wikilink")，使用同样的脉冲频度来进行信号攻击。这个想法经过考虑后被废弃，因为其不够现实。

不明信号引起了许多猜测，诸如苏联在进行[思想控制或是](../Page/思想控制.md "wikilink")[天气控制实验等](../Page/气象战.md "wikilink")。不过因为其独特的信号特征，很多专家以及业余无线电爱好者很快发现它是超视距雷达的信号。[北约](../Page/北大西洋公约组织.md "wikilink")[军事情报部门给它给予的](../Page/军事情报.md "wikilink")[北约代号为](../Page/北约代号.md "wikilink")
*STEEL WORK*或是*STEEL
YARD*。尽管在业余无线电爱好者社区中广为人知，相关信息仍然等到[苏联解体后才公布](../Page/苏联.md "wikilink")。

## 历史

### 起因

20世纪60年代苏联一直在为其[反弹道导弹系统研制](../Page/反弹道导弹.md "wikilink")[预警雷达](../Page/预警雷达.md "wikilink")，但大部分都是只适用于入侵检测与拦截的视距内雷达。它们都不能在弹道导弹发射后的几秒或几分钟内提供早期预警以准备应对措施。当时苏联的早期预警[星座系统还不完善](../Page/人造卫星.md "wikilink")，而且在敌对地区执行任务时还有包括可能遭遇反卫星攻击在内的问题，而位于苏联境内的超视距雷达没有这些问题。在60年代后期，苏联开始研发这类系统。

## 绰号

「俄罗斯啄木鸟」是指一个源自[苏联的](../Page/苏联.md "wikilink")[无线电讯号](../Page/无线电.md "wikilink")。该讯号自1976年7月到1989年12月间可以在全球范围内的[短波频段上听到](../Page/短波.md "wikilink")。它听起来就像是尖锐的敲击噪音，大约每秒钟重复10次，所以被冠以“俄罗斯啄木鸟”的绰号。

“俄罗斯啄木鸟”被确认为来自苏联开发的[超视距雷达系统](../Page/超视距雷达.md "wikilink")，使用对[电离层反射敏感的短波频率](../Page/电离层.md "wikilink")，可以让苏联探测到[导弹排出的废气中的](../Page/导弹.md "wikilink")[离子对](../Page/离子.md "wikilink")[电离层传播特性的改变](../Page/电离层.md "wikilink")。

1988年，[美国联邦通信委员会对啄木鸟信号进行了测量](../Page/美国联邦通信委员会.md "wikilink")，数据分析显示脉冲重复频率为90毫秒，频率范围从7到19兆赫，带宽为0.02到0.08兆赫，典型的发送时间为7分钟，观测到了3种重复频率：10赫兹、16赫兹和20赫兹。最常见的频率为10赫兹，16赫兹和20赫兹很少出现。啄木鸟传送的脉冲[带宽较宽](../Page/带宽.md "wikilink")，典型情况下为40千赫。

## 位置

Duga是最初的实验性系统\[2\]\[3\]，位于[乌克兰南部的黑海港口](../Page/乌克兰.md "wikilink")[尼古拉耶夫附近](../Page/尼古拉耶夫_\(乌克兰\).md "wikilink")，并成功探测到了约2500公里外[拜科努尔航天发射场的火箭发射](../Page/拜科努尔航天发射场.md "wikilink")。Duga能够探测到从东侧远方以及[太平洋的潜艇中朝北冰洋中的](../Page/太平洋.md "wikilink")[新地岛发射的导弹](../Page/新地岛.md "wikilink")。2002年一场大火使其严重受损，之后人们修复了这个巨大、复杂的雷达系统。Duga的发射机位于
 ，接收机位于 。

## 文化

乌克兰游戏《[潜行者系列](../Page/潜行者系列.md "wikilink")》有情节与[切尔诺贝利核电站以及那里的](../Page/切尔诺贝利核电站.md "wikilink")[核事故有关](../Page/切尔诺贝利核事故.md "wikilink")。游戏着重刻画了包括Duga-1雷达在内的这一区域。雷达出现在游戏《[潜行者：晴空](../Page/潜行者：晴空.md "wikilink")》虚构的城市Limansk-13中。同时《[潜行者：切尔诺贝利的阴影](../Page/潜行者：切尔诺贝利的阴影.md "wikilink")》中的“焦脑器”是受Duga-1是精神控制器而非雷达这一阴谋论启发。

## 参考文献

## 扩展阅读

  -
  -
  - Headrick, James M., Ch. 24: "HF over-the-horizon radar," in: Radar
    Handbook, 2nd ed., Merrill I. Skolnik, ed. \[New York: McGraw-Hill,
    1990\].

  - Kosolov, A. A., ed. Over-the-Horizon Radar (translated by W. F.
    Barton) \[ Norton, Mass.: Artech House, 1987\].

  -
## 外部链接

  - [Chernobyl-2. Secret Military Facility in the territory of exclusion
    zone. Text and
    photos](http://www.chornobyl.in.ua/en/chernobyl-2.htm) 2008
  - [OTH-Radar "Chornobyl - 2" and Center of
    space-communication](http://lplaces.com/en/reports/zona/12-chornobyl-2)
  - ["Circle" is an auxiliary system for OTH-Radar "Chornobyl
    - 2"](http://lplaces.com/en/reports/zona/13-chornobyl-2)
  - [The Russian Woodpecker](http://www.qsl.net/n1irz/woodpeck.html),
    *Miami Herald*, July 1982.
  - [Steel Yard
    OTH](http://www.globalsecurity.org/wmd/world/russia/steel-yard.htm),
    globalsecurity.org
  - [Some pictures of
    Chernobyl-2](http://pix.fine.kiev.ua/egor/gallery/0000b02w)
  - ['Duga' photos at
    englishrussia.com](http://englishrussia.com/index.php/2008/04/28/duga-the-steel-giant-near-chernobyl/)
  - [Chernobyl-2](http://rusue.com/chernobyl-2/)
  - [The Top Secret Military Base Hidden in Chernobyl’s Irradiated
    Forest](http://www.atlasobscura.com/articles/the-top-secret-military-base-hidden-in-chernobyls-irradiated-forest)

[Category:冷战武器](../Category/冷战武器.md "wikilink")
[Category:無線電](../Category/無線電.md "wikilink")
[Category:雷达](../Category/雷达.md "wikilink")
[Category:業餘無線電史](../Category/業餘無線電史.md "wikilink")

1.
2.
3.