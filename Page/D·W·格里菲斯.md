[The_adventures_of_Dollie.webm](https://zh.wikipedia.org/wiki/File:The_adventures_of_Dollie.webm "fig:The_adventures_of_Dollie.webm")
**大衛·卢埃林·沃克·格里菲斯**（[英语](../Page/英语.md "wikilink")：****，），[美国](../Page/美国.md "wikilink")[电影导演](../Page/电影导演.md "wikilink")，代表作是备受争议的《[一个国家的诞生](../Page/一个国家的诞生.md "wikilink")》（1915年）以及随后的《[忍無可忍](../Page/忍無可忍.md "wikilink")》（1916年）\[1\]，两部影片曾分别入选1952年英国《[影像與聲音](../Page/影像與聲音.md "wikilink")》杂志评出的十部世界电影杰作、1958年[布鲁塞尔国际博览会评选的](../Page/布鲁塞尔.md "wikilink")“世界电影十二佳片”。

[美國電影學會曾分別在](../Page/美國電影學會.md "wikilink")1998年、2007年評選[百年百大電影](../Page/AFI百年百大電影.md "wikilink")（AFI's
100 Years... 100
Movies）、[百年百大電影（10週年版）](../Page/AFI百年百大電影.md "wikilink")。《[一个国家的诞生](../Page/一个国家的诞生.md "wikilink")》曾列在1998年的百年百大電影名單第44名，但是在[百年百大電影（10週年版）退出前一百名之外](../Page/AFI百年百大電影.md "wikilink")，不過《[忍無可忍](../Page/忍無可忍.md "wikilink")》在[百年百大電影（10週年版）名列第](../Page/AFI百年百大電影.md "wikilink")49名。兩部影片分別是兩份名單裡年代最早的電影。

他是[聯美製片公司](../Page/聯美.md "wikilink")4位創始人之一（其他3位分別為[玛丽·毕克馥](../Page/玛丽·毕克馥.md "wikilink")、[道格拉斯·费尔班克斯](../Page/道格拉斯·费尔班克斯.md "wikilink")、[查理·卓別林](../Page/查理·卓別林.md "wikilink")）。

## 参考资料

## 延伸阅读

  - 陳陳華：〈[格里菲斯與中國早期電影](http://www.zwwhgx.com/content.asp?id=2679)〉。

  - Lillian Gish, The Movies, Mr. Griffith and Me (Englewood, NJ:
    Prentice Hall, 1969)

  - Karl Brown, Adventures with D. W. Griffith (New York: Farrar, Straus
    and Giroux, 1973)

  - Richard Schickel, D. W. Griffith: An American Life (New York: Simon
    and Schuster, 1984)

  - Robert M. Henderson, D. W. Griffith: His Life and Work (New York:
    Oxford University Press, 1972)

  - William M. Drew, D. W. Griffith's "Intolerance:" Its Genesis and Its
    Vision (Jefferson, NJ: McFarland & Company, 1986)

  - Kevin Brownlow, The Parade's Gone By (New York: Alfred A. Knopf,
    1968)

  - Seymour Stern, "An Index to the Creative Work of D. W. Griffith,"
    (London: The British Film Institute, 1944-47)

  - David Robinson, Hollywood in the Twenties (New York: A. S. Barnes &
    Co, Inc., 1968)

  - Edward Wagenknecht and Anthony Slide, The Films of D. W. Griffith
    (New York: Crown, 1975)

  - William K. Everson, American Silent Film (New York: Oxford
    University Press, 1978)

  - Iris Barry and Eileen Bowser, D. W. Griffith: American Film Master
    (Garden City, NY: Doubleday, 1965)

  -
## 相關條目

  - [西席·地密爾](../Page/西席·地密爾.md "wikilink")

## 外部链接

  -
[Category:美國電影導演](../Category/美國電影導演.md "wikilink")
[Category:好萊塢星光大道](../Category/好萊塢星光大道.md "wikilink")
[Category:奧斯卡終身成就獎獲得者](../Category/奧斯卡終身成就獎獲得者.md "wikilink")
[Category:默片導演](../Category/默片導演.md "wikilink")
[Category:西部片導演](../Category/西部片導演.md "wikilink")
[Category:威爾斯裔美國人](../Category/威爾斯裔美國人.md "wikilink")
[Category:肯塔基州人](../Category/肯塔基州人.md "wikilink")
[Category:葬于肯塔基州](../Category/葬于肯塔基州.md "wikilink")

1.