**<span style="font-size:larger;">e-Sword</span>**是由[美國人](../Page/美國.md "wikilink")「Rick
Meyers」創作的[基督教](../Page/基督教.md "wikilink")[聖經研究](../Page/聖經.md "wikilink")[電腦軟體](../Page/電腦軟體.md "wikilink")，支援[微軟視窗作業系統與](../Page/Microsoft_Windows.md "wikilink")[Pocket
PC系統](../Page/Pocket_PC.md "wikilink")。《e-Sword》的發展開始於2000年一月，自推出以來極受歡迎，到
2009年四月份已經有超過九百萬個使用者由[網路上](../Page/網路.md "wikilink")[下載](http://www.e-sword.net/downloads.html)此軟體。2009年九月9.5版更新後，僅三個月就有250萬次下載。\[1\]

2005年『Bible Software Review』做的一項調查顯示 e-Sword
在免費聖經研究軟體項內是使用者最多的，甚至超過任何商業性的聖經研究軟體。\[2\]

《e-Sword》目前有21種語言的使用者界面，提供100種以上不同語言的聖經版本(包括中文聖經)，十四種不同聖經字典，七種聖經地圖，十八種聖經註解，[NASA的衛星影像以及多種的](../Page/NASA.md "wikilink")
Daily Devotions。 對於Apple 電腦的使用者也有 MAC Sword 可下載。下載與使用《e-Sword》是完全免費的。

## 程式設計理念

在e-Sword 的網頁上，其創辦人Rick Meyers 寫著:『你們白白的得來，也要白白的捨去
([馬太十章八節](../Page/馬太.md "wikilink"))』。\[3\]

每一位渴慕主話的人都可免費的下載 E-Sword 及其外掛的資源。

## 功能

  - 不同版本聖經的對照
  - 能加入近百種版本的聖經
  - 經節搜尋
  - 個人筆記，用螢光筆對經文進行勾勒等許多功能。
  - 匯集有很多歷代解經書
  - 聖經辭典
  - 地圖瀏覽器
  - 圖解輔助和很多相關書籍
  - 能對希臘文和希伯來文版的聖經，進行以單詞，章節，段落為單位的解釋
  - 讀經計畫
  - 禱告單

請至[官方網站](http://translate.google.com/translate?js=n&prev=_t&hl=en&ie=UTF-8&layout=2&eotf=1&sl=auto&tl=zh-TW&u=http%3A%2F%2Fwww.e-sword.net%2Ffeatures.html&act=url)查看詳情，

## 作業平台

  - Windows操作系统
      - Windows NT 4.0
      - Windows 95/98/ME
      - Windows2000
      - Windows XP
      - Windows Vista
      - Windows 7

<!-- end list -->

  - [ReactOS](../Page/ReactOS.md "wikilink")

<!-- end list -->

  - [CrossOver](../Page/CrossOver.md "wikilink")

<!-- end list -->

  - Linux作業系統
  - 透過
    [Wine可以使用於](../Page/Wine.md "wikilink")[Linux作業系統](../Page/Linux.md "wikilink")\[4\]\[5\]

<!-- end list -->

  - [Mac OS X 10.4](../Page/Mac_OS_X_10.4.md "wikilink") (Tiger)\[6\]

## 版本

  - e-Sword
  - Pocket e-Sword
  - e-Sword Live\[7\]：網路版

## Utility Programs

  - Ben's e-Sword Tool 2.0\[8\]
  - e-Sword ToolTip Tool\[9\]
  - e-Sword User Module Conversion Utility v9.5.0\[10\]
  - e-Sword HotKey\[11\]\[12\]

## 評論與獎勵

### 對《e-Sword》的評論

  - Bible Software Review 20 October 2005:[e-Sword version 7.7.7. very
    good
    e-sword](https://web.archive.org/web/20070919220806/http://www.bsreview.org/index.php?modulo=Reviews&id=19)
  - [e-Sword Is Still One of the Best Free Bible
    Programs](http://www.ccmag2.com/2006_09/2006_09techtalk.pdf)
  - [e-Sword Update & Serious Language
    Tools](http://www.e-sword.net/1107techtalk.pdf)
  - [*Studying the Bible for Free* Stimulus Vol 12 Number 3 August 2004
    page
    33-38](http://www.stimulus.org.nz/index_files/Stim12_3StudyingBible.pdf)

### 對《Pocket e-Sword》的評論

  - Bible Software Review 26 November 2005\[13\]

### 得到的獎勵

得到的獎勵 \[14\]\[15\] \[16\]\[17\]

  - [2004 Pocket PC Magazine Text and Reference Bible
    Software](https://web.archive.org/web/20070927035311/http://www.pocketpcmag.com/_archives/feb05/fromthejudges.aspx)
  - [2005 Pocket PC Magazine
    Finalist](https://web.archive.org/web/20070815091842/http://www.pocketpcmag.com/awards/category_2005.asp?catid=42)
  - [2006 Pocket PC
    Magazine](https://web.archive.org/web/20070927035425/http://www.pocketpcmag.com/awards/category_2006.asp?catid=42)
  - [2007 Pocket PC Best Software Awards (Religious) :
    Finalist](https://web.archive.org/web/20081007055232/http://pocketpcmag.com/awards/category_all_2007_newquery.asp)

## 参考文献

## 外部連結

  - [DoctorDaveT.com - conservative & evangelical Bible study
    modules](http://www.doctordavet.com)
  - [e-Sword Smart Starter
    tool](https://web.archive.org/web/20100906135048/http://zefania.blogspot.com/2009/11/e-sword-smart-starter-basic_08.html)
  - [Sharpening The Sword](http://www.sharpeningthesword.net)

[Category:聖經](../Category/聖經.md "wikilink")
[Category:免费軟體](../Category/免费軟體.md "wikilink")
[Category:教育软件](../Category/教育软件.md "wikilink")

1.
2.  [Bible Software Review](http://www.bsreview.org/survey05.htm)
3.
4.  [Scripts for downloading and installing
    e-Sword.](http://ubuntuforums.org/showthread.php?t=404042)
5.  [The *Ubuntu Christian Edition* web
    forum](http://ubuntuforums.org/forumdisplay.php?f=168)
6.  [Mac e-Sword](http://www.whatisrazar.com/macesword/)
7.  [e-Sword Live](http://live.e-sword.net/)
8.  [Ben's e-Sword Tool 2.0](http://e-sword-users.org/users/node/190)
9.  [e-Sword ToolTip Tool](http://e-sword-users.org/users/node/2410)
10. [e-Sword User Module Conversion Utility
    v9.5.0](http://e-sword-users.org/users/node/2054)
11. [e-Sword HotKey](http://e-sword-users.org/users/node/2408?page=753)
12. [e-Sword HotKey](http://www.recipester.org/Software:E-Sword_HotKey)
13. [Pocket
    e-Sword 2.5](http://www.bsreview.org/blog/2004/11/pocket_esword_25.html)

14. [Text and Reference Bible
    Software](http://www.pocketpcmag.com/_archives/feb05/fromthejudges.aspx)

15.
16. [2005 Pocket PC Magazine
    Finalist](http://www.pocketpcmag.com/awards/category_2005.asp?catid=42)

17. [2006 Pocket PC
    Magazine](http://www.pocketpcmag.com/awards/category_2006.asp?catid=42)