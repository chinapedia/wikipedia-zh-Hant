**E**, **e**
是[拉丁字母中的第](../Page/拉丁字母.md "wikilink")5个[字母](../Page/字母.md "wikilink")。

它来源于一个与它形状和功能相像的[希腊字母](../Page/希腊字母.md "wikilink")[Epsilon](../Page/Epsilon.md "wikilink")（Ε,
ε）。 [闪族语单词](../Page/闪族语.md "wikilink")
可能是第一个用来表示起到或称呼人的单词。在闪含语中，这个字母发作/h/（在其它语言中可以发作/e/），在[希腊语中](../Page/希腊语.md "wikilink")
 变成
（Epsilon）发作/e/。[伊特鲁里亚人和](../Page/伊特鲁里亚人.md "wikilink")[罗马人都这样使用它](../Page/罗马.md "wikilink")。由于[元音大推移](../Page/元音大推移.md "wikilink")，[英语的用法比较特殊](../Page/英语.md "wikilink")，英语me或bee中把它发作/i:/，而在bed中的用法则相当接近[拉丁美洲和](../Page/拉丁美洲.md "wikilink")[欧洲的用法](../Page/欧洲.md "wikilink")。

与其他拉丁字母元音一样，e有长元音（例如英语的 ）和短元音的变体（例如英语的
）。在其它语言中这个字母可能有很多音值，有时候要用重音符（）来区分它们。

在电脑中，这个字母大写形式的[ASCII码为](../Page/ASCII.md "wikilink")69，小写为101。这个字母是英语中最常用的字母，而很多的相关语言中，这个字母可以作为密码学的提示字母。

## 字母E的含意

在一些[醫學檢驗報告中](../Page/醫學檢驗.md "wikilink")，參考值標示的E為可能為陽性或陰性（Equivocal）的意思。\[1\]

## 字符编码

| 字符编码                               | [ASCII](../Page/ASCII.md "wikilink") | [Unicode](../Page/Unicode.md "wikilink") | [EBCDIC](../Page/EBCDIC.md "wikilink") | [摩斯电码](../Page/摩斯电码.md "wikilink") |
| ---------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- | ---------------------------------- |
| [大写](../Page/大写字母.md "wikilink") E | 69                                   | 0045                                     | 197                                    | `·`                                |
| [小写](../Page/小写字母.md "wikilink") e | 101                                  | 0065                                     | 133                                    |                                    |

## 其他记法

## 参看

### E的变体

  - [Əə](../Page/Ə.md "wikilink")（[中央元音](../Page/中央元音.md "wikilink")）
  - [Ǝǝ](../Page/Ǝ.md "wikilink")（[泛尼日利亚字母](../Page/泛尼日利亚字母.md "wikilink")）
  - [Ɛɛ](../Page/Ɛ.md "wikilink")（[半開前不圓唇元音](../Page/半開前不圓唇元音.md "wikilink")）
  - [∃](../Page/∃.md "wikilink")（数学上的存在符号）
  - [€](../Page/€.md "wikilink")（[欧元的符号](../Page/欧元.md "wikilink")）

### 其他字母中的相近字母

  - （[希腊字母](../Page/希腊字母.md "wikilink") Epsilon）

  - （[西里尔字母](../Page/西里尔字母.md "wikilink") E）

  - （西里尔字母 Ye）

## 外部連結

  -
  -
  -
[Category:拉丁字母](../Category/拉丁字母.md "wikilink")

1.