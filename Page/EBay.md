**eBay**（，中文：**電子灣**、**億貝**、**易贝**）是可讓全球民眾上[網買賣物品的線上拍賣及購物網站](../Page/網際網路.md "wikilink")。

## 起源與歷史

[Ebayheadquarters.jpg](https://zh.wikipedia.org/wiki/File:Ebayheadquarters.jpg "fig:Ebayheadquarters.jpg")
1995年9月4日，[皮埃爾·歐米迪亞](../Page/皮埃爾·歐米迪亞.md "wikilink")（Pierre
Omidyar）創立Auctionweb網站，總部位於[美國](../Page/美國.md "wikilink")[加利福尼亞州](../Page/加利福尼亞州.md "wikilink")[聖荷西](../Page/聖荷西.md "wikilink")，Auctionweb是eBay的前身。

歐米迪亞第一件販賣的物品是一隻壞掉的[雷射指示器](../Page/雷射.md "wikilink")，以14.83[美元成交](../Page/美元.md "wikilink")。他驚訝地詢問得標者：「您難道不知道這玩意壞了嗎？」歐米迪亞接到了以下的回覆信：「我是個專門收集壞掉的镭射指示器玩家。」（更常被傳頌的故事則是一則1997年一位公關經理向媒體所杜撰的故事：eBay的創立是為了幫助歐米迪亞的未婚妻交換一些[倍滋糖果](../Page/倍滋糖果.md "wikilink")（）的[玩具](../Page/玩具.md "wikilink")。該故事後來在2002年在[亞當斯科漢](../Page/亞當斯科漢.md "wikilink")（）的書中揭露並經eBay官方確認。）

事实并非是故事中说的那样，在YouTube创始人-陈士骏自传，《20个月赚130亿》，第四章：开创YouTube：一个朴实的创业故事，开头中提到——公共关系专家朱莉·苏番(Julie
Supan)，“你们以为别人的故事就是真的？听我说，eBay的创立并不是为了帮助创始人奥米迪亚的未婚妻交换一些PEZ Candy的玩具。”

[傑夫·史科爾在](../Page/傑夫·史科爾.md "wikilink")1996年被聘僱為該公司首任總裁及全職員工。1997年9月該公司正式更名為ebay。起初該網站屬於Omidyar的顧問公司Echo
Bay Technology Group。歐米迪亞曾經嘗試註冊一個*EchoBay.com*的網址，卻發現該網址已被 Echo Bay
礦業註冊了，所以他將EchoBay.com改成他的第二備案：“Ebay.com”。

## 物品與服務和支付

每天都有數以百萬的家具、收藏品、電腦、車輛在eBay上被刊登、販售、賣出。有些物品稀有且珍貴，然而大部分的物品可能只是個滿佈灰塵、看起來毫不起眼的小玩意。這些物品常被他人給忽略，但如果能在全球性的大市場販售，那麼其身價就有可能水漲船高。只要物品不違反法律或是在eBay的禁止販售清單之內，即可以在eBay刊登販售。服務及虛擬物品也在可販售物品的範圍之內。可以公允的說，eBay推翻了以往那種規模較小的跳蚤市場，將買家與賣家拉在一起，創造一個永不休息的市場。大型的跨國公司，像是[IBM會利用eBay的固定價或競價拍賣來銷售他們的新產品或服務](../Page/IBM.md "wikilink")。資料庫的區域搜尋使的運送更加迅捷或是便宜。軟體工程師們藉著加入
[eBay Developers Program](http://developer.ebay.com)，得以使用eBay
[API](../Page/API.md "wikilink")，創造許多與eBay相整合的軟體。截至[2005年6月](../Page/2005年6月.md "wikilink")，已經有超過15,000人加入這個計畫，其中包含許多公司創造許多的軟體使的eBay買家與賣家能夠更方便。

[2004年6月](../Page/2004年6月.md "wikilink")，eBay的英國分站將菸酒類產品的刊登列為禁止項目。少數的例外是數量稀少的陳年酒。

在eBay上也不時會有一些具爭議性且違反道德標準的拍賣。1999年時，有位仁兄看中了龐大（但卻違法）的[器官移植市場](../Page/器官移植.md "wikilink")，在eBay刊登一則[腎臟的拍賣](../Page/腎臟.md "wikilink")，想藉此獲利。在某些場合，一些販售人還是一個小鎮的拍賣佈告，都僅僅只是個笑話。只要eBay接獲檢舉，這些拍賣佈告就會立即被關閉。因為eBay不允許任何違反其政策的拍賣項目。然而eBay也是一個有心人士可以任意刊登虛假拍賣佈告的地方。因為這個緣故，對新手而言，如果沒有詳讀一切訊息的話，很容易陷入被詐騙的陷阱。

eBay在[拉丁美洲的合作夥伴是](../Page/拉丁美洲.md "wikilink")[MercadoLibre](../Page/MercadoLibre.md "wikilink")。eBay於2006年10月在[台灣與](../Page/台灣.md "wikilink")[PChome
Online網路家庭合資成立露天市集國際資訊股份有限公司](../Page/PChome_Online網路家庭.md "wikilink")，網站[露天拍賣](../Page/露天拍賣.md "wikilink")。eBay的主要競爭者是[亚马逊和](../Page/亚马逊.md "wikilink")[雅虎拍賣以及](../Page/雅虎.md "wikilink")[阿里巴巴集团](../Page/阿里巴巴.md "wikilink")。

在eBay上的交易有多種付款方式，但只有使用PayPal付款才享有賣家保護，PayPal（在中国大陆的品牌为贝宝），1998年12月由
Peter Thiel 及 Max Levchin 建立。
是一个总部在美国加利福尼亚州圣荷西市的因特网服务商，允许在使用电子邮件来标识身份的用户之间转移资金，避免了传统的邮寄支票或者汇款的方法。PayPal也和一些电子商务网站合作，成为它们的货款支付方式之一；但是用这种支付方式转账时，PayPal收取一定数额的手续费。

## 獲利與交易

[EBay_localization.png](https://zh.wikipedia.org/wiki/File:EBay_localization.png "fig:EBay_localization.png")
eBay從以下項目獲利：

1.  向每筆拍賣收取刊登費－費用依站點、拍賣類別、拍賣形式、起標價和有無使用eBay商店（eBay Store）而有所不同。
2.  向每筆已成交的拍賣再收取成交費－成交費的計算方式則和刊登費相類似，唯不同點是成交費只以成交價計算，而不是以起標價計算；目前部份站點亦已將運費納入成交費的計算。
3.  由於eBay另外擁有[PayPal](../Page/PayPal.md "wikilink")，所以也從此處產生利益。

該公司目前的經營策略在於增加使用eBay系統的跨國交易。eBay已經將領域延伸至包括[中国大陆及](../Page/中国大陆.md "wikilink")[印度在內的地区](../Page/印度.md "wikilink")。eBay擴張失敗的國家和地区是[中国大陆](../Page/中国大陆.md "wikilink")、[日本及](../Page/日本.md "wikilink")[台灣](../Page/台灣.md "wikilink")。[雅虎在日本經營的拍賣業務在日本國內已佔據領導地位](../Page/雅虎.md "wikilink")（比eBay早六個月），迫使eBay鎩羽而歸。而台灣的eBay亦敵不過[雅虎奇摩拍賣網站的市佔率](../Page/雅虎奇摩.md "wikilink")，也以與[PChome
Online網路家庭聯名的名義間接退出台灣市場](../Page/PChome_Online網路家庭.md "wikilink")。eBay最初通过收购[易趣的方式进入中国大陆市场](../Page/易趣.md "wikilink")，但之后在与[淘宝的竞争中落败](../Page/淘宝.md "wikilink")，最终以与[TOM集團合资成立](../Page/TOM集團.md "wikilink")“新易趣”的方式退出中国大陆市场。eBay於2012年結束与TOM的合作，“易趣網”由TOM獨立運營，不再作為eBay中國網站。

## eBay的合併案

  - 1999年5月，eBay合併線上付款服務公司[Billpoint](../Page/Billpoint.md "wikilink")。該公司於eBay購併[PayPal後結束營業](../Page/PayPal.md "wikilink")
  - 1999年，eBay合併了Butterfield &
    Butterfield。該公司在2002年賣給了[Bonhams](../Page/Bonhams.md "wikilink")。
  - 1999年，eBay以四千三百萬元合併了[Alando](../Page/Alando.md "wikilink")。該公司後被合併在eBay[德國之下](../Page/德國.md "wikilink")。
  - 2000年6月，eBay合併了[Half.com](../Page/Half.com.md "wikilink")，並將其整合在eBay市集中。
  - 2001年8月，eBay合併了[Mercado
    Libre](../Page/Mercado_Libre.md "wikilink")、[Lokau及](../Page/Lokau.md "wikilink")[iBazar等三家](../Page/iBazar.md "wikilink")[拉丁美洲的拍賣網站](../Page/拉丁美洲.md "wikilink")。
  - 2002年2月，eBay以950萬元購併台灣最大的拍賣網站經營者[力傳資訊](../Page/力傳資訊.md "wikilink")（Neocom
    Technology Co.,
    Ltd.），並改成[台灣ebay](http://www.ebay.com.tw/)。力傳資訊設有[C2C網站](../Page/C2C.md "wikilink")「拍賣王」（bid.com.tw）與[B2C網站](../Page/B2C.md "wikilink")「買賣王」（ubid.com.tw）。但數個月之後，力傳資訊的高階主管集體請辭，力傳資訊則成為[興奇科技](../Page/興奇科技.md "wikilink")（monday.com.tw）的前身。[1](http://www.epochtimes.com/b5/2/4/16/n183734.htm)
  - 2002年6月，eBay以價值15億元的[股票合併了](../Page/股票.md "wikilink")[PayPal](../Page/PayPal.md "wikilink")。
  - 2003年7月11日，eBay以一億五千萬元現金合併了[中國大陸最大](../Page/中國大陸.md "wikilink")[電子商務公司](../Page/電子商務.md "wikilink")「[易趣](../Page/易趣.md "wikilink")」（EachNet），並推出聯名拍賣網站「[eBay易趣](../Page/eBay易趣.md "wikilink")」。
  - 2004年6月22日，eBay以五千萬元及額外的現金購併[印度拍賣網站](../Page/印度.md "wikilink")[Baazee.com](../Page/Baazee.com.md "wikilink")。
  - 2004年8月13日，eBay自一位前[craigslist](../Page/craigslist.md "wikilink").org員工取得了該公司25%的股權。
  - 2004年9月，eBay將合併眼光擺到其在[韓國的對手](../Page/韓國.md "wikilink")[Internet
    Auction
    Co.](../Page/Internet_Auction_Co..md "wikilink")（IAC），並以每股125000[韓元](../Page/韓元.md "wikilink")（約合[美金](../Page/美金.md "wikilink")109元）的價格取得該公司近三百萬的股權。
  - 2004年11月，eBay以二億二千五百萬[歐元購併](../Page/歐元.md "wikilink")[Marktplaats.nl](../Page/Marktplaats.nl.md "wikilink")。該公司藉著專注在小型廣告上因而取得在[荷蘭的八成市場](../Page/荷蘭.md "wikilink")，並成為eBay在荷蘭的主要競爭者。
  - 2004年12月16日，eBay以三千萬元現金和總值三億八千五百萬元的eBay股票購併
    [Rent.com](http://www.rent.com)。
  - 2005年5月，eBay合併Gumtree。該公司是一家提供城市分類廣告的網站。
  - 2005年6月，eBay以六億三千五百萬元購併
    [Shopping.com](http://www.ebay.com.tw/)。該公司是一家線上比價網站。
  - 2005年9月，eBay以二十六億元現金及股票購併[VoIP業者](../Page/VoIP.md "wikilink")[Skype](../Page/Skype.md "wikilink")。
  - 2006年6月5日，eBay與[台灣](../Page/台灣.md "wikilink")[PChome
    Online網路家庭宣佈進行合作投資](../Page/PChome_Online網路家庭.md "wikilink")，雙方將於2006年下半年推出聯名拍賣網站。
  - 2006年9月6日，eBay與[台灣](../Page/台灣.md "wikilink")[PChome
    Online網路家庭宣佈](../Page/PChome_Online網路家庭.md "wikilink")，將在2006年9月25日推出聯名拍賣網站「[露天拍賣](../Page/露天拍賣.md "wikilink")」。
  - 2006年10月13日，台灣eBay正式關閉\[1\]。

（以上幣制如未註明者，皆以[美金為單位](../Page/美金.md "wikilink")。）

  - 2006年12月，eBay宣布与[Tom.com宣布成立合资公司](../Page/TOM集团.md "wikilink")，新的合资公司Tom.com占51%股份，eBay占49%，将继续易趣国内交易。另成立新公司CBT负责跨国交易部分的业务。
  - 2012年4月，eBay结束与[Tom.com的合作](../Page/TOM集团.md "wikilink")，易趣网由TOM独资运营，不再作为eBay中国站点\[2\]。

## 爭議

eBay有些爭議，從[隱私權政策](../Page/隱私權.md "wikilink")（eBay往往不經任何程序即將個人資料送給司法單位）到賣家資訊不完全公開（不公佈賣家所涉入的詐欺）。

根據eBay資料來看，不到0.01％的交易被確認為詐欺案件。

### 詐騙

eBay
主要的防[詐騙手段是](../Page/詐騙.md "wikilink")[評價系統](../Page/評價.md "wikilink")。在每筆交易完成後，買、賣家皆可以為彼此評價。她們可以給出像是：「正面」、「負面」或是「中立」的評價，並且為該次交易留下一筆意見。所以，如果買家對該賣家（交易）有所不滿的話，他可以給這位賣家留下一筆負面評價，並且留下如：「物品未收到」的意見，以防下一位買家有可能誤中陷阱。對買家而言，學會並善用評價系統有助於減低被詐騙的機率。

當然，評價系統同時也是保護賣家的（相對於買家，保護力道偏低）。如果有個買家的評價過低，或是負面評價太高，該賣家可以根據其評價，拒絕其交易。

評價系統的弱點有：

  - 無論交易金額的大小，一筆交易的評價機會都只有一次。
  - 用戶可能出於某些原因（像是害怕被給負面評價），而不敢給對方負面評價。
  - 收到負面評價的用戶可以在評價頁面提出一段約80個字母的反駁。不過卻很少人能夠利用如此簡短的文字做反駁。
  - 因為eBay幾乎不會移除任何評價。所以，移除不公平、非真實的評價根本是不大可能的。
  - 依據目前的eBay政策，賣家只能選擇不給予買家評價或正面評價，買家則無此限制－因此某些評價限制對於只買東西的會員無效。

當一位使用者覺得一位賣家（或買家）曾經有過不實，那麼在他的帳號上即會有一筆前科。如果確實有許多使用者與那個賣家有過糾紛，那個賣家（或買家，或是兼具兩者的身分）的帳號將會被取消。當然，該賣家也可循法律途徑進行救濟。

許多抱怨者抱怨eBay的交易系統充滿了詐欺！同時英國著名的消費者權利節目[Watchdog也常常接獲許多關於eBay的申訴](../Page/Watchdog_\(television\).md "wikilink")。內容不外乎是
eBay 對使用者的申訴不理不睬之類的。

咎因於賣家的詐騙有：

  - 收到貨物款項後卻不把物品送出。
  - 買家收到的物品與敘述的完全不同。
  - 寄送已損壞的物品。
  - 仿冒商品。
  - 販賣竊物。
  - 以「誘餌」帳號吸引買家頻頻出高價。

咎因於買家的詐騙有：

  - [Paypal](../Page/Paypal.md "wikilink") 詐騙。
  - [信用卡詐騙](../Page/信用卡.md "wikilink")。
  - 宣稱收到的不是如敘述的物品（買家收到的物品與敘述的完全不同除外）。
  - 剛收到物品就立刻退貨。

### 其他具爭議的行為

  - ****
    指的是在拍賣快結束前的幾分鐘以高價劫標的行為。這種行為在eBay上是允許的。許多拍賣網站都會對在截標前的出價，採取延長拍賣時效的方式，以避免劫標的發生。而eBay則採用自動出價制，在出價時，可以提出一個高價；如果有人的出價高過你的，系統將會根據你的出價，再提出一個較之前為高的新價格\[3\]。
  - 有些使用者會將那種普遍昂貴的東西以便宜的價格刊登，然後在敘述中（暗示）所購買的物品並不含展示的物品。這種行為是eBay所禁止的，但他們卻很難強制關閉這類拍賣。\<\!--語意似乎不完整，所以暫不翻譯。
  - Conversely, sometimes very cheap items, like envelopes, are sold for
    high prices because they come with free airline vouchers or concert
    tickets, in order not to violate the terms on these items.--\>
  - 有些人會將物品的價格壓的特別低（通常都是超低的立刻買價格），然後卻收取相當高額的運費。自從eBay對順利成交的物品收取成交手續費後，這招使得賣家可以大幅減低成交手續費的支出，也可以讓買家省卻一些進口關稅的費用。這招被稱為避費（fee
    avoidance），當然也是eBay政策中不被允許的（[參考資料](http://pages.ebay.com/help/policies/listing-circumventing.html)）。這種避費手段固然可以省卻一些費用，但對買家卻具有相當危險。一旦買家想要退費時，賣家將會以表列的（低）售價退費。因此，目前已有部份站點將運費納入成交費的計算，同時買家退貨後的退款金額也包含此筆交易的運費。

### 其他與eBay有關的爭議

其他與eBay有關的爭議包括了：

  - 2003年5月28日，一個美國地方法院陪審團發現eBay涉及[專利侵權](../Page/專利.md "wikilink")，並據此判決eBay需付出3千5百萬的損害賠償。陪審團發現原告MercExchange在2001年曾指控eBay侵害由該公司創辦人Tom
    Woolston所擁有的三項專利（其中兩項用於eBay的「立刻買」的固定價格販售功能）。而後，eBay向美國[聯邦法院提出上訴](../Page/聯邦法院.md "wikilink")，遭到駁回，並保留原判決。直到2005年11月，eBay再向美國[最高法院提出上訴](../Page/最高法院.md "wikilink")，以防堵對MercExchange進行行政救濟。2006年，美國最高法院同意審理此案。
  - 2003年6月28日，eBay與其子公司[PayPal同意付出](../Page/PayPal.md "wikilink")1000萬美金罰金以解決他們資助海外及線上賭博業的爭議。根據申述的內容，[PayPal涉嫌在](../Page/PayPal.md "wikilink")2000年中到2002年11月，將金錢匯至美國聯邦及擁有賭博法的州。
    [PayPal也被迫取消估計佔有](../Page/PayPal.md "wikilink")6%市佔率的
    [www.paypalwarning.com](http://www.paypalwarning.com)。此一行為發生在eBay購買[PayPal之前](../Page/PayPal.md "wikilink")。
  - 2004年12月17日，eBay印度子公司：Baazee.com的[執行官Avnish](../Page/首席執行官.md "wikilink")
    Bajaj被以線上販售兩名印度學生口交的影片的罪名逮捕。該公司矢口否認對販售物品的內容知情，並且迅速地移除任何可能違法的內容。印度政府企圖以Bajaj違反印度的[IT法](../Page/IT.md "wikilink")：「禁止出版、傳遞、或是提供平台以出版任何猥褻內容」來偵辦此案件。儘管實際內容並未在Baazee的伺服器中出現，eBay仍然強烈支持Baazee。
  - 2005年6月14日，eBay在不顧眾人抱怨之下，移除了原是免費的Live
    8慈善演唱會門票拍賣。根據[鮑勃·格爾多夫的說法](../Page/鮑勃·格爾多夫.md "wikilink")，他將eBay敘述成一個**數位皮條客**。因為許多這類的拍賣都充斥著假的下標。最後eBay同意在[英國法律之下](../Page/英國.md "wikilink")，將慈善類的門票列為合法物品。
  - 2005年，[澳洲](../Page/澳洲.md "wikilink")[橄欖球聯盟試著追回那些被黃牛們在eBay上賣出去的冠軍賽門票](../Page/橄欖球.md "wikilink")，但沒有成功。
  - 被凍結[PayPal帳戶](../Page/PayPal.md "wikilink")（如果不是因為賣方端原因）的eBay使用者之帳戶也有可能被eBay給凍結。
  - eBay限制某些國家/區域的線上付款業務\[4\]。

## 故障和事件

2014年5月21日，美东时间上午九点，eBay表示由于其存储加密口令和非财务资料的数据库遭遇“网络攻击”，公司要求eBay用户能够尽快修改自己的密码。根据eBay公司等机构的初步调查，该事件起源于2014年2月底和三月初之间针对一小部分员工登陆资料的网络攻击。通过这些员工账户，攻击者得以进入到了eBay的管理网络。隨後，2014年5月28日，eBay中國和eBay香港向用戶發送了中文聲明，敦促用戶更改賬戶密碼\[5\]\[6\]\[7\]。

eBay公司表示，这起网络攻击发生于2014年2月底至3月初之间，造成eBay
用户资料库遭未经授权存取，内含资料包括：顾客姓名、加密密码、电邮地址、实际地址、电话号码及出生日期。

早前，eBay在旗下支付平台PayPal的網站上，呼籲所有使用者盡快更改密碼，但沒有交代原因，網站後來也刪除相關通知\[8\]。

## 冷知識

### eBay 上少數特別昂貴的物件

  - [格魯曼公司的](../Page/格魯曼.md "wikilink") [Gulfstream
    II](../Page/Gulfstream_II.md "wikilink") 噴射機（美金490萬）
  - 1909 [Honus Wagner](../Page/Honus_Wagner.md "wikilink") 棒球卡（美金165萬）
  - 西[肯塔基州的鑽石湖酒店](../Page/肯塔基州.md "wikilink")（美金120萬）
  - [Enzo
    Ferrari](../Page/Ferrari_Enzo_Ferrari.md "wikilink")（美金975,000；2004年10月）[4](http://www.carpages.co.uk/ferrari/ferrari_enzo_27_10_04.asp?switched=on&echo=983693210)
  - [Shoeless Joe Jackson的](../Page/Shoeless_Joe_Jackson.md "wikilink")
    "Black Betsy" 球棒（美金577,610）
  - 與[老虎伍茲共同參與一場](../Page/老虎伍茲.md "wikilink")[高爾夫球比賽](../Page/高爾夫球.md "wikilink")（美金425,000）

### 最大的物品

### 賣不出去的最大物品

eBay史上沒賣出去的最大物品是一艘[巴西的](../Page/巴西.md "wikilink")[航空母艦](../Page/航空母艦.md "wikilink")，競標該除役航母[米勒斯吉拿斯號的拍賣由一位匿名賣家登上ebay](../Page/米勒斯吉拿斯號航空母艦.md "wikilink")，引發了眾多新聞報導。截至該商品因違反出售軍火的禁令被下架前，最高下標價甚至來到了400萬英鎊。
\[9\][eBay Motors](http://www.motors.ebay.com)

### 奇怪的拍賣物品

  - 2005年6月，Karolyne Smith將她的右前額以10,000元美金賣給GoldenPalace.com作為廣告之用。
  - 也是在2005年6月，Tim Shaw（一位在Kerrang\!
    105.2電台主持深夜節目的廣播主持人）的夫人以50英鎊的一口价賣出他的高級蓮花跑車，因為她聽見了節目上丈夫與人打情罵俏的內容。
  - 2005年5月，時任教宗[本篤十六世在擔任主教時的座車](../Page/本篤十六世.md "wikilink")（[Volkswagen](../Page/大眾汽車.md "wikilink")
    Golf）以188,938歐元賣出。得標者是Golden
    Palace這間線上[賭場](../Page/賭場.md "wikilink")。該公司以其令人驚異的行為在eBay聞名。
  - 2004年，一個住在[西雅圖的仁兄將他穿著其前妻的洋裝的照片刊登在eBay](../Page/西雅圖.md "wikilink")。當他承認他是為了買到一張球票時，該筆拍賣的價格已經飆到了數千元。最後，賣家還收到許多求婚的請求。
  - 2004年9月，[MagicGoat.com](http://www.magicgoat.com)
    的站長將他垃圾桶中的垃圾賣給一位在中學教授語言藝術的教師。該教師希望他的學生以垃圾寫篇論文。[參考連結](http://www.magicgoat.com/ebay/ebay.htm)
  - 號稱地球上最高的雲霄飛車[Kingda
    Ka也將其處女航拍賣](../Page/Kingda_Ka.md "wikilink")。最後的標價是1691.66元，而且得標者可以坐在最前排的位子\[10\]。

### 二手時裝

自2017年起，再次掀起復古潮流，於是不少消費者到eBay購買二手時裝、配飾。當中舊款的腰包於eBay售出57000個\[11\]。

### 非法的物品

eBay剛創立之時，對於物品的規範其實並不嚴謹，但隨著其成長，eBay發現有必要對某些物品的刊登進行限制或禁止。在數以百計被禁止的類別中，有些限制僅適用在某些歐洲國家（例如，帶有納粹象徵的物品）。

  - [菸品](../Page/香烟.md "wikilink")。但菸品的相關週邊產品或收集品不在此限。
    [6](http://pages.ebay.com/help/policies/tobacco.html)
  - [酒類](../Page/酒類.md "wikilink")：酒類相關的收集品，包括已封裝的酒瓶（as well as wine
    sales by licensed sellers are
    allowed）。[7](http://pages.ebay.com/help/policies/alcohol.html)
  - [納粹用品](../Page/納粹.md "wikilink")。[8](http://pages.ebay.com/help/policies/offensive.html)
  - 私錄的[影音資料](../Page/影音.md "wikilink")。[9](http://pages.ebay.com/help/policies/bootlegs.html)
  - [軍火](../Page/軍火.md "wikilink")。[10](http://pages.ebay.com/help/policies/firearms-weapons-knives.html)
  - 人體殘肢或遺骸。
  - 郵資[蓋印機](../Page/蓋印機.md "wikilink")。

## 參見

  - [電子商務](../Page/電子商務.md "wikilink")
  - [網路拍賣的商業模型](../Page/網路拍賣.md "wikilink")
  - [購物中心](../Page/購物中心.md "wikilink")
  - [eBay drop off](../Page/eBay_drop_off.md "wikilink")
  - [購物網站](../Page/購物網站.md "wikilink")

## 深入閱讀

  - (Hardcover, 246 pages)

  - (Hardcover, 336 pages)

  - [Collier, Marsha](../Page/Collier,_Marsha.md "wikilink") (2004)
    *eBay For Dummies*, 4th Edition, John Wiley ISBN 978-0-7645-5654-8.
    (Softcover 408 pages)

  -
  -
## 參考文獻

\[12\]

<div class="references-small">

1\. <http://auctionessistance.com/ebay-sellers-furious-21-day-hold/>

<references>

</references>

</div>

## 外部連結

  - [ebay 中國](http://www.ebay.cn/)

  - [ebay 台灣](http://www.ebay.com.tw)

  - [ebay 香港](http://www.ebay.com.hk)

[Category:EBay](../Category/EBay.md "wikilink")
[Category:美国互联网公司](../Category/美国互联网公司.md "wikilink")
[Category:總部在美國的跨國公司](../Category/總部在美國的跨國公司.md "wikilink")
[Category:香港網絡購物平台](../Category/香港網絡購物平台.md "wikilink")
[Category:電子商務網站](../Category/電子商務網站.md "wikilink")
[Category:虛擬社群](../Category/虛擬社群.md "wikilink")
[Category:美國品牌](../Category/美國品牌.md "wikilink")
[Category:聖荷西公司](../Category/聖荷西公司.md "wikilink")
[Category:1995年成立的公司](../Category/1995年成立的公司.md "wikilink")
[Category:1995年建立的网站](../Category/1995年建立的网站.md "wikilink")
[Category:1998年IPO](../Category/1998年IPO.md "wikilink")
[Category:MALAYSIA_SARAWAK](../Category/MALAYSIA_SARAWAK.md "wikilink")

1.
2.  [TOM在线或再失双臂
    证实已与eBay分手，续约Skype代理权亦充满变数](http://tech.ifeng.com/gundong/detail_2012_07/17/16066213_0.shtml)
3.  [2](http://pages.ebay.com/help/buy/outbid-ov.html)
4.  [3](http://pages.ebay.com/help/policies/safe-payments-policy.html)
5.
6.
7.
8.
9.  Tweedie, *For internet sale: aircraft carrier, only three owners*
10. [5](http://cgi.ebay.com/ws/eBayISAPI.dll?ViewItem&item=6523923733&category=16071&sspagename=rvi:1:1v_home)
11. [不捨買新貨改買古董？Logo熱潮令
    Vintage店生意急升](https://mings.mpweekly.com/daily/20180115-70126)
12.