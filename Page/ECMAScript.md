是一种由[Ecma国际](../Page/Ecma国际.md "wikilink")（前身为[欧洲计算机制造商协会](../Page/欧洲计算机制造商协会.md "wikilink")）通过ECMA-262标准化的[脚本](../Page/脚本语言.md "wikilink")[程序设计语言](../Page/程序设计语言.md "wikilink")。这种语言在[万维网上应用广泛](../Page/万维网.md "wikilink")，它往往被称为[JavaScript或](../Page/JavaScript.md "wikilink")[JScript](../Page/JScript.md "wikilink")，但实际上后两者是ECMA-262标准的实现和扩展。

## 历史

ECMAScript是由[网景的](../Page/网景公司.md "wikilink")[布蘭登·艾克開發的一種腳本語言的標準化規範](../Page/布蘭登·艾克.md "wikilink")；最初命名為Mocha，後來改名為LiveScript，最後重新命名為JavaScript\[1\]。1995年12月，[升阳与网景聯合發表了JavaScript](../Page/升阳电脑公司.md "wikilink")\[2\]。1996年11月，网景公司将JavaScript提交给欧洲计算机制造商协会进行标准化。ECMA-262的第一个版本于1997年6月被Ecma组织采纳。ECMAScript是由ECMA-262标准化的脚本语言的名称。

儘管JavaScript和JScript与ECMAScript相容，但包含超出ECMAScript的功能\[3\]。

## 版本

至今为止有八个ECMA-262版本发表。

| 版本 | 发表日期     | 与前版本的差异                                                                                                                                                                                                                        |
| -- | -------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| 1  | 1997年6月  | 首版                                                                                                                                                                                                                             |
| 2  | 1998年6月  | 格式修正，以使得其形式与ISO/IEC16262国际标准一致                                                                                                                                                                                                 |
| 3  | 1999年12月 | 强大的正则表达式，更好的词法作用域链处理，新的控制指令，异常处理，错误定义更加明确，数据输出的格式化及其它改变                                                                                                                                                                        |
| 4  | 放棄       | 由於關於語言的複雜性出現分歧，第4版本被放棄，其中的部分成為了第5版本及Harmony的基礎                                                                                                                                                                                 |
| 5  | 2009年12月 | 新增「嚴格模式（strict mode）」，一個子集用作提供更徹底的錯誤檢查,以避免結構出錯。澄清了許多第3版本的模糊規範，並適應了與規範不一致的真實世界實現的行為。增加了部分新功能，如getters及setters，支持[JSON以及在物件屬性上更完整的](../Page/JSON.md "wikilink")[反射](../Page/反射_\(計算機科學\).md "wikilink")\[4\]\[5\]\[6\]\[7\]\[8\] |
| 6  | 2015年6月  | ECMAScript 2015（ES2015），第 6 版，最早被称作是 ECMAScript 6（ES6），添加了类和模块的语法，其他特性包括迭代器，Python风格的生成器和生成器表达式，箭头函数，二进制数据，静态类型数组，集合（maps，sets 和 weak maps），promise，reflection 和 proxies。作为最早的 ECMAScript Harmony 版本，也被叫做ES6 Harmony。          |
| 7  | 2016年6月  | ECMAScript 2016（ES2016），第 7 版，多個新的概念和語言特性\[9\]                                                                                                                                                                                 |
| 8  | 2017年6月  | ECMAScript 2017（ES2017），第 8 版，多個新的概念和語言特性\[10\]                                                                                                                                                                                |
| 9  | 2018年6月  | ECMAScript 2018 （ES2018），第 9 版，包含了异步循环，生成器，新的正则表达式特性和 rest/spread 语法。                                                                                                                                                          |

2004年6月Ecma组织发表了ECMA-357标准，它是ECMAScript的一个扩延，也被称为[E4X](../Page/E4X.md "wikilink")（ECMAScript
for XML）。

## 相容性

许多应用程序支持ECMAScript，尤其是[网页浏览器](../Page/网页浏览器.md "wikilink")。下列表格列出了目前版本的軟體與ECMAScript最新版本的相容性。

| 腳本引擎                                                          | 參考應用程式                                                                                               | 相容性\[11\] |
| ------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------- | --------- |
| ES5\[12\]                                                     | ES6\[13\]                                                                                            | ES7\[14\] |
| [Chakra](../Page/Chakra_\(JavaScript引擎\).md "wikilink")       | [Microsoft Edge](../Page/Microsoft_Edge.md "wikilink") 18                                            | 100%      |
| [SpiderMonkey](../Page/SpiderMonkey.md "wikilink")            | [Firefox](../Page/Firefox.md "wikilink") 63                                                          | 100%      |
| [Chrome V8](../Page/V8_\(JavaScript引擎\).md "wikilink")        | [Google Chrome](../Page/Google_Chrome.md "wikilink") 70、[Opera](../Page/Opera電腦瀏覽器.md "wikilink") 57 | 100%      |
| [JavaScriptCore](../Page/JavaScriptCore.md "wikilink")（Nitro） | [Safari](../Page/Safari.md "wikilink") 12                                                            | 99%       |

## 參考文獻

## 外部連結

  -
{{-}}

[E](../Category/腳本語言.md "wikilink")
[E](../Category/程序設計語言.md "wikilink")
[E](../Category/JavaScript.md "wikilink")

1.
2.
3.
4.  [ECMA-262 5th Edition: ECMAScript Language
    Specification](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-262.pdf)

5.  [Changes to JavaScript, Part 1:
    EcmaScript 5](http://www.youtube.com/watch?v=Kq4FpMe6cRs)
6.  <http://www.ecma-international.org/news/PressReleases/PR_Ecma_finalises_major_revision_of_ECMAScript.htm>
7.  [InfoQ:
    ECMAScript 5正式发布](http://www.infoq.com/cn/news/2009/12/ecmascript5)
8.  [Javascript面面觀：核心篇《ECMA-262
    Edition 5》](http://ithelp.ithome.com.tw/question/10031677)
9.
10.
11. ES5 is the baseline for this test suite. The conformance rate for
    other editions reflects support for new features only, not a
    comprehensive score.
12.
13.
14.