## 系统简介

**eCos**（embedded configurable operating system）是一個在1997年由Cygnus
Solutions
Inc.\[1\]开发的小型[开放原始码](../Page/开放原始码.md "wikilink")[实时操作系统](../Page/实时操作系统.md "wikilink")。后来该公司被[Redhat收购](../Page/Redhat.md "wikilink")。在2002年的早些时候\[2\]，[Redhat停止了eCos开发](../Page/Redhat.md "wikilink")，并解散了开发队伍。很多原开发者继续开发eCos，并且成立了他们自己的公司来为该软件提供服务。迫于开发者的要求，Redhat在2004年一月份将eCos的版权转移给了[自由软件基金会](../Page/自由软件基金会.md "wikilink")\[3\]，并于2008年5月份最终完成了版权转移。

此系统和[嵌入式](../Page/嵌入式.md "wikilink")[Linux系统的差异是它采用静态链接](../Page/Linux.md "wikilink")[(static
library)的方式](../Page/\(static_library\).md "wikilink")，最低编译核心可小至10K的级别，适合用于做[bootloader增强](../Page/bootloader.md "wikilink")，或者用于构建微小型系统。
eCos的发行版还包括[RedBoot](../Page/RedBoot.md "wikilink")，一个[开放原始码的](../Page/开放原始码.md "wikilink")[应用程序](../Page/应用程序.md "wikilink")。它使用[硬件抽象层提供](../Page/硬件抽象层.md "wikilink")[嵌入式系统的启动](../Page/嵌入式系统.md "wikilink")[固件](../Page/固件.md "wikilink")。

除了自由版本以外，eCos还有一个称为**eCosPro**的商业版本。它是eCos的一个商业分支，由[eCosCentric开发](../Page/eCosCentric.md "wikilink")，并包含有私有组件。它是稳定并经过测试和认证的版本\[4\]。在2017年，2017,
eCosCentric发布了\[5\]对于全部树莓派单板计算机的eCos移植，并同时发布了这些移植的自由版本。

## 支持架构

eCos可以在一系列平台上运行，包括如下架构：

  - [ARM](../Page/ARM_architecture.md "wikilink")
  - [CalmRISC](../Page/CalmRISC.md "wikilink")
  - [FR-V](../Page/FR-V.md "wikilink")
  - [Hitachi H8](../Page/Hitachi_H8.md "wikilink")
  - [IA-32](../Page/IA-32.md "wikilink")
  - [Motorola 68000](../Page/Motorola_68000.md "wikilink")
  - [Matsushita AM3x](../Page/Matsushita_AM3x.md "wikilink")
  - [MIPS](../Page/MIPS_architecture.md "wikilink")
  - [NEC V8xx](../Page/NEC_V8xx.md "wikilink")
  - [Nios II](../Page/Nios_II.md "wikilink")
  - [PowerPC](../Page/PowerPC.md "wikilink")
  - [SPARC](../Page/SPARC.md "wikilink")
  - [SuperH](../Page/SuperH.md "wikilink")

## 外部連結

  - [eCos主站点（偏重代码升级类）](http://ecos.sourceware.org/)
  - [eCos主站点（偏重宣传，业内新闻类，解决方案的全套供应等）](http://www.ecoscentric.com/)

## 相关书籍

  - 《嵌入式可配置实时操作系统eCos软件开发》，作者：(美) Anthony J.Massa 译者：颜若麟 孙晓明 尤伟伟 林巧民
  - 《嵌入式可配置实时操作系统eCos技术及实现机制》 ，作者：王京起等

## 参考文献

[Category:实时操作系统](../Category/实时操作系统.md "wikilink")
[Category:自由作業系統](../Category/自由作業系統.md "wikilink")
[Category:嵌入式操作系统](../Category/嵌入式操作系统.md "wikilink")

1.
2.
3.
4.
5.