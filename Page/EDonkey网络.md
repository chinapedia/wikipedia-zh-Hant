**eDonkey網路**（，也称**eDonkey2000
Network**、**eD2k**、**eD2k網路**或**電驢網路**），是一种分布式的、主要基于[服务器的](../Page/服务器.md "wikilink")[P2P](../Page/點對點技術.md "wikilink")[檔案分享](../Page/檔案分享.md "wikilink")[网络](../Page/计算机网络.md "wikilink")。它通常用于共享[电影](../Page/电影.md "wikilink")[视频文件](../Page/视频.md "wikilink")、[音乐专辑和计算机程序](../Page/音乐.md "wikilink")[软件等](../Page/软件.md "wikilink")。和大多数文件共享网络一样，它是分布式的，文件不存储在任何中枢[服务器之上](../Page/服务器.md "wikilink")，服务器参与用户之间基于點對點原理相互数据交换。

现在，eDonkey网络不隶属于任何组织，也不被任何组织所维护。eDonkey协议原先由美国的[MetaMachine公司原创](../Page/MetaMachine公司.md "wikilink")，并用于[eDonkey2000软件](../Page/eDonkey2000.md "wikilink")。2005年，他们遭到[RIAA的侵权控告而关闭了公司](../Page/RIAA.md "wikilink")，并停止了eDonkey2000和协议的开发。

使用eDonkey网络的客户端程序连接到这个网络来共享文件。而eDonkey网络服务器作为一个通讯中心，使用户在eDonkey网络内查找文件。它的客户端和服务端可以工作于[Windows](../Page/Windows.md "wikilink")、[Macintosh](../Page/Macintosh.md "wikilink")、[Linux](../Page/Linux.md "wikilink")、[UNIX操作系统](../Page/UNIX.md "wikilink")。任何人都可以作为服务器加入这个网络。由于服务器经常变化，客户端会经常更新它的服务器列表。

## 特色

eDonkey网络客户端用[eD2k
Hash](../Page/eD2k_Hash.md "wikilink")（一种混合[MD4摘要算法](../Page/MD4.md "wikilink")）来识别文件。这使eDonkey网络可以将不同文件名的同一文件成功识别为一个文件，并使同一文件名的不同文件得以区分。对大于9.28MB的文件，它在下载完成前将其分割；这将加速大型文件的发送。为了便于文件搜索，一些Web站点对比较热门的文件建立eD2k链接。这些网站通常也提供热门服务器列表便于用户更新。\[1\]

与[Kad网络互联](../Page/Kad.md "wikilink")，[emule同时支持ed](../Page/emule.md "wikilink")2k网络和Kad网络,两个网络的档案可以自由分享。

## 歷史

2004年，eDonkey网络超过[FastTrack](../Page/FastTrack.md "wikilink")，成为[互联网上应用最普遍的文件共享网络](../Page/互联网.md "wikilink")。虽然每个小时、每一天数字都在变动，但据估计，在2005年中期，eDonkey网络上按平均水平，大约有两三百万用户通过100到200个服务器共享了5亿到20亿个文件。从前该网络中最热门的服务器是[Razorback2](../Page/Razorback2.md "wikilink")，大约有一百万用户，但在2006年2月21日左右，它被[比利时联邦警察查封](../Page/比利时.md "wikilink")，Razorback2不再提供服务。

有些“审查员服务器”会出现在eDonkey网络上。它们以文件类型（例如：mp3）和某些关键字（例如：“xxx”、“sex”）检索并搜集网络中共享文件的情报。这些服务器包括“Sonny
Boy”、“Byte Devils”、“Pirate's
Lair”等等。其中许多来自于美国，并使用以64.34为开头的IP地址，也有其它国家的。这些服务器标称有大量在线用户（大于150万），和许多曾经连接的用户（1000万-1300万）。无论如何，我们无法确定到底有多少用户真正地连接了它们。这有些服务器假冒“Razorback2”（例如：Razorback2.3、Razorback2.4）并同样使用上述地址段。它们只提供极少的搜索和来源查找功能。有些人认为这些服务器是[美国唱片产业协会或其它类似的组织为了毁灭eD](../Page/美国唱片产业协会.md "wikilink")2k或搜集违反版权法的用户的信息而建立的。自从这些服务器肇始，已无法确定eDonkey网络用户的真实数量了。

这种网络的一个问题就是它需要专用服务器以保证网络的运行。它依赖于乐于花费大量带宽、CPU时间的用户来运行服务器。这些服务器会承受很大的负载并且理论上更容易受到来自互联网的攻击。为了解决这种问题，eDonkey2000的原作者开发了一个eDonkey协议的“继承者”——[Overnet](../Page/Overnet.md "wikilink")。而[eMule自行也开发了](../Page/eMule.md "wikilink")[Kademlia网络](../Page/Kademlia.md "wikilink")，通常称为“KAD网络”。这些协议将克服“服务器依赖”。

2005年9月28日，eDonkey网络客户端的主要开发者[Sam
Yagan](../Page/Sam_Yagan.md "wikilink")，对[RIAA的](../Page/RIAA.md "wikilink")“让eDonkey2000用户不再能够下载有版权保护的内容”的要求进行了让步。这也许会对整个eDonkey网络产生一些影响。即使现在最流行的eD2k客户端是eMule。

## 客戶端

以下是部分**eDonkey网络客户端**（或称**eDonkey软件**、**eD2k软件**、**电驴软件**）的列表：

  - [eMule](../Page/eMule.md "wikilink")：一个开放源代码的Windows客户端；最熱門的客户端，拥有80%的eDonkey网络用户。
  - [aMule](../Page/aMule.md "wikilink")：支持Windows、Mac和类Unix。
  - [xMule](../Page/xMule.md "wikilink")：一个类Unix客户端。
  - [Imule](../Page/Imule.md "wikilink")：一个类Unix客户端，现已停止开发。
  - [eMule Plus](../Page/eMule_Plus.md "wikilink")：另一流行的Windows开源客户端。
  - [Shareaza](../Page/Shareaza.md "wikilink")：一个开源多网络跨平台客户端。
  - [MLdonkey](../Page/MLdonkey.md "wikilink")：自由软件。可运行于许多平台并能够很好的支持许多文件共享协议。
  - [eDonkey2000](../Page/eDonkey2000.md "wikilink")（MetaMachine的客户端）：第一个使用eDonkey网络的软件，商业软件，有收费和免费两种版本。目前已停止维护。
  - [Hydranode](../Page/Hydranode.md "wikilink")：开源。多网络。核心与界面分离。
  - [MediaVAMP](../Page/MediaVAMP.md "wikilink")（later changed to
    Pruna）：基于eMule的韩国专用客户端。
  - [lphant](../Page/lphant.md "wikilink")：运行于[Microsoft
    .NET](../Page/Microsoft_.NET.md "wikilink") 平台。
  - [Jubster](../Page/Jubster.md "wikilink")：多网络客户端（Windows）。

## 参考

[Category:应用层协议](../Category/应用层协议.md "wikilink")
[Category:EDonkey网络](../Category/EDonkey网络.md "wikilink")

1.