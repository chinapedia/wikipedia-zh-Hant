**ENPPI體育會**（，简称：**ENPPI**）是一家位于[埃及](../Page/埃及.md "wikilink")[开罗的职业足球俱乐部](../Page/开罗.md "wikilink")。

## 球队荣誉

### 国内

  - **[埃及足球甲级联赛](../Page/埃及足球甲级联赛.md "wikilink")**
      - *亚军：* 2005年

<!-- end list -->

  - **[埃及足球杯](../Page/埃及足球杯.md "wikilink")**
      - *冠军：* 2005年

### 海外

  - **[阿拉伯冠军联赛](../Page/阿拉伯冠军联赛.md "wikilink")**
      - *亚军：* 2006年

## 外部链接

  - [Official Website 阿拉伯语](http://www.enppiclub.com/)

[Category:埃及足球俱乐部](../Category/埃及足球俱乐部.md "wikilink")