**EO2**是[香港的一隊跳舞組合](../Page/香港.md "wikilink")，於1999年出道，是[Starj &
Snazz娛樂旗下的藝員](../Page/Starj_&_Snazz娛樂.md "wikilink")。2013年10月20日宣佈正式解散\[1\]，組合解散後只有Eddie及Otto活躍於娛樂圈。

## 名字由來

取名**EO2**是因為有兩名成員（**E**ddie、**E**ric）的英文名字的第一字母是**E**，另外兩名成員（**O**sman、**O**tto）的英文名字的第一字母是**O**。

## 成員

EO2由以下四位成員組成的：

<table>
<thead>
<tr class="header">
<th><p>中文姓名</p></th>
<th><p>英文姓名</p></th>
<th><p>出生日期 |- style="background-color: #FFFFFF; text-align: center;" |</p></th>
<th><p><strong><a href="../Page/彭懷安.md" title="wikilink">彭懷安</a></strong></p></th>
<th><p>Eddie Pang</p></th>
<th><p>|- style="background-color: #FFFFFF; text-align: center;" |</p></th>
<th><p><strong><a href="../Page/洪智傑.md" title="wikilink">洪智傑</a></strong></p></th>
<th><p>Osman Hung</p></th>
<th><p>|- style="background-color: #FFFFFF; text-align: center;" |</p></th>
<th><p><strong>王志安</strong></p></th>
<th><p>Otto Wong</p></th>
<th><p>|- style="background-color: #FFFFFF; text-align: center;" |</p></th>
<th><p><strong>謝凱榮</strong></p></th>
<th><p>Eric Tse</p></th>
<th><p>|- style="background-color: #FFFFFF; text-align: center;" |</p></th>
</tr>
</thead>
<tbody>
</tbody>
</table>

## 唱片

  - 2002年5月：《魅力移動》（與[E-kids](../Page/E-kids.md "wikilink")，[張詠詩合輯](../Page/張詠詩.md "wikilink")）

唱片公司：Stareast Music

  - 魅力移動
  - 普通朋友
  - 魂遊
  - 普通朋友 Rage Mix

<!-- end list -->

  - 2002年11月：《我知道》

唱片公司：Stareast Music

1.  Computer Data File
2.  我知道 Music Video
3.  魅力移動（國）Music Video
4.  普通朋友 Music Video
5.  魂遊 Music Video
6.  普通朋友 (Rage Mix) Music Video
7.  我知道
8.  Kiss Kiss - EO2/[李蘢怡](../Page/李蘢怡.md "wikilink")
9.  守護星
10. 魅力移動（國）
11. 第一次擁抱（國）
12. 普通朋友
13. 魂遊
14. 普通朋友 (Rage Mix)

<!-- end list -->

  - 2004年1月：《EO2004》

唱片公司：Stareast Music

1.  愛壞人
2.  A Better Day
3.  兼職綁匪
4.  Cheeze
5.  驚動
6.  大丈夫
7.  真要命（國）
8.  讓我對你好（國）
9.  尋人啟事（國）
10. 真愛入侵（國）
11. 重燃

<!-- end list -->

  - 2005年6月3日：《Hang Over》

唱片公司： StarJ Music

1.  大搖大擺
2.  內疚
3.  大搖大擺（大鑼大鼓mix）featuring Ghoststyle
4.  內疚（純鋼版）

<!-- end list -->

  - 2006年11月9日：《EO2 STAND 4》

唱片公司：Snazz Music Limited

1.  大難不死
2.  手忙腳亂
3.  京華春夢
4.  回憶攻略
5.  上上下下左左右右（B）(A)
6.  繼續唱

<!-- end list -->

  - 2008年11月11日：《Ladies' Nite》

唱片公司：Snazz Music Limited

1.  Ladies' Nite
2.  一天
3.  舞林群英
4.  差不多先生
5.  男人的錯

## 派台歌曲成績

| **派台歌曲成績**                                         |
| -------------------------------------------------- |
| 唱片                                                 |
| **2001年**                                          |
| [我知道](../Page/我知道.md "wikilink")                   |
| **2002年**                                          |
| [我知道](../Page/我知道.md "wikilink")                   |
| [我知道](../Page/我知道.md "wikilink")                   |
| [我知道](../Page/我知道.md "wikilink")                   |
| [我知道](../Page/我知道.md "wikilink")                   |
| **2003年**                                          |
| [EO2004](../Page/EO2004.md "wikilink")             |
| **2004年**                                          |
| [EO2004](../Page/EO2004.md "wikilink")             |
| [EO2004](../Page/EO2004.md "wikilink")             |
| [Hang Over](../Page/Hang_Over.md "wikilink")       |
| [Hang Over](../Page/Hang_Over.md "wikilink")       |
| **2005年**                                          |
| [Stand 4](../Page/Stand_4.md "wikilink")           |
| **2006年**                                          |
| [Stand 4](../Page/Stand_4.md "wikilink")           |
| [Stand 4](../Page/Stand_4.md "wikilink")           |
| [Stand 4](../Page/Stand_4.md "wikilink")           |
| [潮音樂全主打](../Page/潮音樂全主打.md "wikilink")             |
| **2007年**                                          |
| [Stand 4](../Page/Stand_4.md "wikilink")           |
| [Ladies' Nite](../Page/Ladies'_Nite.md "wikilink") |
| [Ladies' Nite](../Page/Ladies'_Nite.md "wikilink") |
| **2008年**                                          |
| [Ladies' Nite](../Page/Ladies'_Nite.md "wikilink") |
| [Ladies' Nite](../Page/Ladies'_Nite.md "wikilink") |
| [Ladies' Nite](../Page/Ladies'_Nite.md "wikilink") |
| **2009年**                                          |
|                                                    |
| **2012年**                                          |
|                                                    |
| **2013年**                                          |
|                                                    |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **2**       |

## 電影

|          |                                                      |                 |
| -------- | ---------------------------------------------------- | --------------- |
| **上映年份** | **電影名稱**                                             | **參與成員**        |
| 2002     | 《[無間道](../Page/無間道.md "wikilink")》                   | 全員              |
| 2002     | 《[飛虎雄師](../Page/飛虎雄師.md "wikilink")》                 | 全員              |
| 2003     | 《[給他們一個機會](../Page/給他們一個機會.md "wikilink")》           | Osman，Eddie     |
| 2003     | 《[一切由零開始](../Page/一切由零開始.md "wikilink")》             | 全員              |
| 2003     | 《[大丈夫](../Page/大丈夫.md "wikilink")》                   | 全員              |
| 2004     | 《[旺角黑夜](../Page/旺角黑夜.md "wikilink")》                 | Eddie           |
| 2004     | 《[精裝追女仔](../Page/精裝追女仔.md "wikilink")2004》           | Eddie           |
| 2005     | 《[回來愛我](../Page/回來愛我.md "wikilink")》                 | Eddie，Otto      |
| 2005     | 《[魔幻賭船](../Page/魔幻賭船.md "wikilink")》                 | Eddie，Otto      |
| 2005     | 《[黑白戰場](../Page/黑白戰場.md "wikilink")》                 | Eddie，Otto      |
| 2005     | 《[偷窺](../Page/偷窺.md "wikilink")》                     | Otto            |
| 2006     | 《[雀聖](../Page/雀聖.md "wikilink")2自摸天后》                | Eddie，Otto      |
| 2006     | 《[斷路](../Page/斷路.md "wikilink")》                     | Eddie           |
| 2006     | 《[半醉人間](../Page/半醉人間.md "wikilink")》                 | 全員              |
| 2006     | 《[危險人物](../Page/危險人物_\(香港電影\).md "wikilink")》        | 全員              |
| 2006     | 《[高踭](../Page/高踭.md "wikilink")》                     | Osman，Otto      |
| 2006     | 《[機動部隊 絕路](../Page/機動部隊_絕路.md "wikilink")》           | Osman，Otto      |
| 2006     | 《[機動部隊 伙伴](../Page/機動部隊_伙伴.md "wikilink")》           | Osman，Otto      |
| 2006     | 《[一樓一鬼](../Page/一樓一鬼.md "wikilink")》                 | Eric，Eddie，Otto |
| 2007     | 《[美女食神](../Page/美女食神.md "wikilink")》                 | Otto            |
| 2009     | 《[永久居留](../Page/永久居留.md "wikilink")》                 | Osman           |
| 2009     | 《[流浪漢世界盃](../Page/流浪漢世界盃.md "wikilink")》             | Otto            |
| 2011     | 《[愛很爛](../Page/愛很爛.md "wikilink")》                   | Osman           |
| 2012     | 《[2012我愛香港喜上加囍](../Page/2012我愛香港喜上加囍.md "wikilink")》 | 全員              |

### [ViuTV](../Page/ViuTV.md "wikilink") 節目

《[精裝送禮仔](../Page/精裝送禮仔.md "wikilink")》Otto
實況娛樂（[真人騷](../Page/真人騷.md "wikilink")）

## 港台電視劇

|          |                                                     |             |
| -------- | --------------------------------------------------- | ----------- |
| **播放年份** | **劇集名稱**                                            | **參與成員**    |
| 2002     | 《[鐵窗邊緣](../Page/鐵窗邊緣.md "wikilink")》—非常普通人，我本大無畏    | EO2全員，eddie |
| 2003     | 《[都市陷阱](../Page/都市陷阱.md "wikilink")》—黑過墨斗           | Otto        |
| 2003     | 《[執法群英II](../Page/執法群英.md "wikilink")》—旱天雷          | EO2全員       |
| 2005     | 《[積金創未來](../Page/積金創未來.md "wikilink")》—一言為定         | Otto        |
| 2005     | 《[反斗多聲道](../Page/反斗多聲道.md "wikilink")》—Let It Be Me | Otto        |
| 2006     | 《[歡樂滿屋](../Page/歡樂滿屋.md "wikilink")》—少林法團           | EO2全員       |
| 2008     | 《[鐵窗邊緣](../Page/鐵窗邊緣.md "wikilink")2008》—假如我是平常人    | Otto        |
| 2013     | 《[申訴](../Page/申訴.md "wikilink")》—尋妻                 | Otto        |

## TVB電視劇

<table>
<tbody>
<tr class="odd">
<td><p><strong>播放年份</strong></p></td>
<td><p><strong>劇集名稱</strong></p></td>
<td><p><strong>參與成員</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p>《<a href="../Page/赤沙印記@四葉草.2.md" title="wikilink">赤沙印記@四葉草.2</a>》</p></td>
<td><p>Osman</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p>2008</p></td>
<td><p>《<a href="../Page/盛裝舞步愛作戰.md" title="wikilink">盛裝舞步愛作戰@四葉草.3</a>》</p></td>
<td><p>EO2全員</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><p>《<a href="../Page/大冬瓜_(電視劇).md" title="wikilink">大冬瓜</a>》</p></td>
<td><p>Eddie 飾演 玉皇大帝<br />
Otto 飾演 粥<br />
Eric 飾演 粉<br />
Osman 飾演 麵</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014</p></td>
<td><p>《<a href="../Page/廉政行动2014.md" title="wikilink">廉政行动2014</a>》</p></td>
<td><p>Eddie</p></td>
<td><p>Eddie 飾演 水牛哥（单元一）</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/使徒行者.md" title="wikilink">使徒行者</a>》</p></td>
<td><p>Eddie、Otto</p></td>
<td><p>Eddie 飾演 -{游}-达富<br />
Otto 飾演 -{姜}-俊新</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/再戰明天.md" title="wikilink">再戰明天</a>》</p></td>
<td><p>Eddie 飾演 黃偉全<br />
Otto 飾演 鍾以碩</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/飛虎II.md" title="wikilink">飛虎II</a>》</p></td>
<td><p>Eddie 飾演 曲尺<br />
Otto 飾演 AK</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td><p>《<a href="../Page/以和為貴.md" title="wikilink">以和為貴</a>》</p></td>
<td><p>Eddie 飾演 車鳴震<br />
Otto 飾演 戴永堅</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/拆局專家.md" title="wikilink">拆局專家</a>》</p></td>
<td><p>Eddie</p></td>
<td><p>Eddie 飾演 大　頭 (第4至8集)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016</p></td>
<td><p>《<a href="../Page/末代御醫.md" title="wikilink">末代御醫</a>》</p></td>
<td><p>Eddie、Otto</p></td>
<td><p>Eddie 飾演 阿虎<br />
Otto 飾演 童鐵骨</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/殭.md" title="wikilink">殭</a>》</p></td>
<td><p>Otto</p></td>
<td><p>Otto 飾演 趙軍</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/流氓皇帝_(2016年電視劇).md" title="wikilink">流氓皇帝</a>》</p></td>
<td><p>Eddie、Otto</p></td>
<td><p>Eddie 飾演 鬼谷子<br />
Otto 飾演 學生領袖</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2018</p></td>
<td><p>《<a href="../Page/波士早晨.md" title="wikilink">波士早晨</a>》</p></td>
<td><p>Otto</p></td>
<td><p>Otto 飾演 路在山</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/大帥哥.md" title="wikilink">大帥哥</a>》</p></td>
<td><p>Eddie</p></td>
<td><p>Eddie 飾演 麥先根 (第1至6集)</p></td>
<td></td>
</tr>
</tbody>
</table>

## TVB電視節目

### Otto

  - 2015 《[學是學非（第二輯）](../Page/學是學非.md "wikilink")》 第13集

## 舞台劇

### Osman

  - 2004 商台森美小儀歌劇團《早安啊\!曼克頓》
  - 2006 《[大煞風景](../Page/大煞風景.md "wikilink")》
  - 2006 商台森美小儀歌劇團《Big Nose》

### Eric

  - 2004
    [商台森美小儀歌劇團](../Page/商台.md "wikilink")《[早安啊\!曼克頓](../Page/早安啊!曼克頓.md "wikilink")》
  - 2006 《[死亡咖啡店](../Page/死亡咖啡店.md "wikilink")》（LOVE & DEATH）

### Eddie

  - 2004
    [商台森美小儀歌劇團](../Page/商台.md "wikilink")《[早安啊\!曼克頓](../Page/早安啊!曼克頓.md "wikilink")》
  - 2006 《[死亡咖啡店](../Page/死亡咖啡店.md "wikilink")》（LOVE & DEATH）

### Otto

  - 2004 商台森美小儀歌劇團《早安啊\!曼克頓》
  - 2006 《大煞風景》
  - 2006 商台森美小儀歌劇團《Big Nose》
  - 2009 《大世界》
  - 2015-2016 DBC 《怪談2.0》主持
  - 2016- 《[怪談直播](https://www.facebook.com/mysteryonlive/)》主持

## 演唱會

  - 2006 [陳奕迅](../Page/陳奕迅.md "wikilink")「Get A Life」演唱會表演嘉賓
  - 2007 商業電台拉闊游擊音樂會「辣妹打怪獸」（與[鄭融合作](../Page/鄭融.md "wikilink")）
  - 2008 [鍾鎮濤演唱會表演嘉賓](../Page/鍾鎮濤.md "wikilink")
  - 2008 **EO2** Ladies'Nite 演唱會
  - 2008 Manhattan id 信用卡 2008年拉闊音樂會第一場:陳奕迅 X 謝安琪 X 方大同 X EO2
  - 2009 [林敏驄](../Page/林敏驄.md "wikilink") 林敏驄＜好歌.好友＞演唱會2009表演嘉賓

## 其他

  - 2004 ITU 香港國際三項鐵人賽（半奧運距離）
  - 2004 TVB 香港步步創高峰-勇闖國金
  - 2004 Reebok 15公里香港挑戰賽
  - 2005 ITU 香港國際三項鐵人賽（繽紛距離）
  - 2005 Columbia & Speedo Fashion Show
  - 2005 Twist Fashion Show
  - 2005 Wrangler Fashion Show
  - 2006 體育節 半奧運距離三項鐵人賽（繽紛距離）

## 幕後

  - 商台森美小儀歌劇團《早安啊！曼克頓》的舞蹈總監

## 獲獎

  - 2008年：勁歌王年度總選頒獎典禮——勁歌王組合獎
  - 2007年：新城勁爆頒獎禮2007——新城勁爆組合獎
  - 2007年：RoadShow頒獎禮2007——至尊組合獎
  - 2007年：TVB 2007年度十大勁歌金曲頒獎禮——組合銀獎
  - 2006年：RoadShow頒獎禮2006——至尊組合獎\>
  - 2006年：TVB 2006年度十大勁歌金曲頒獎禮——組合銅獎
  - 2006年：PM 第五屆夏日人氣歌手頒獎典禮——人氣實力跳舞組合獎
  - 2006年：第三屆勁歌王年度總選頒獎典禮——勁歌王最佳跳舞勁合獎
  - 2006年：君子雜誌——活力演繹獎\>
  - 2006年：車王雜誌
  - 2006年：新城勁爆頒獎禮2006——新城勁爆組合獎
  - 2006年：叱咤樂壇流行榜——叱吒樂壇組合銅獎
  - 2005年：PM第四屆樂壇頒獎典禮——強勢跳舞組合
  - 2005年：PM第四屆夏日人氣歌手頒獎典禮——人氣歌曲獎《繼續唱》
  - 2004年：TVB紅星陸運會4 x 100米——銀牌
  - 2004年：PM第三屆樂壇頒獎典禮——金曲獎《大搖大擺》
  - 2004年：PM人氣奪標頒獎禮——人氣奪標金曲《A Better Day》
  - 2004年：TVB 2003年度十大勁歌金曲頒獎禮——最受歡迎合唱歌曲（銅獎）《Kiss Kiss》
  - 2003年：TVB 2003年勁歌金曲第一季季選——最受歡迎改編歌曲演繹獎《Kiss Kiss》
  - 2003年：第二十五屆十大中文金曲——最有前途新人組合獎 - 金獎
  - 2002年：TVB 2002年度十大勁歌金曲頒獎禮——最受歡迎新人組合（銀獎）
  - 2002年：TVB 2002年度十大勁歌金曲頒獎禮——最受歡迎合唱歌曲（金獎）《魅力移動》
  - 2002年：新城勁爆頒獎禮2002——新城勁爆新登場組合
  - 2002年：PM新勢力頒獎禮——PM勁型火辣組合獎
  - 2002年：TVB 2002年度勁歌金曲第四季季選——最受歡迎改編歌曲演繹獎《我知道》
  - 2002年：TVB 2002年勁歌金曲第二季季選——最受歡迎合唱歌曲《魅力移動》
  - 2002年：PM 新世紀最佳跳舞組合獎
  - 2002年：E\*plus 樂壇新人巡禮2002——新人賞
  - 2002年：E\*plus 樂壇新人巡禮——最具潛質組合獎

## 參考

## 外部連結

  - [Starj &
    Snazz娛樂官方網站](https://archive.is/20070927065057/http://www.snazz.com.hk/)

[Category:香港男子演唱團體](../Category/香港男子演唱團體.md "wikilink")
[\*](../Category/EO2.md "wikilink")

1.