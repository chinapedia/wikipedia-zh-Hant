**ESET NOD32
Antivirus**（通常被稱為**NOD32**）是位於[斯洛伐克的](../Page/斯洛伐克.md "wikilink")[ESET公司](../Page/ESET.md "wikilink")[開發的](../Page/開發.md "wikilink")[防毒軟體](../Page/防毒軟體.md "wikilink")。支援[Windows](../Page/Microsoft_Windows.md "wikilink")、[Linux](../Page/Linux.md "wikilink")、[FreeBSD](../Page/FreeBSD.md "wikilink")、[Mac以及其它](../Page/Mac.md "wikilink")[系統平台](../Page/系統平台.md "wikilink")，分為兩個版本發售，企業版提供[遠端管理工具](../Page/遠端桌面軟體.md "wikilink")。現時最新版本為
ESET NOD32 Antivirus 10，ESET Internet Security，ESET Smart Security
Premium( 12.1.31.0 ) ( [Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink") )。

## 產品

ESET 家用電腦防毒軟體產品列表如下\[1\]\[2\]：

### 目前產品

[Windows](../Page/Windows.md "wikilink")：

  - [ESET Smart Security](../Page/ESET_Smart_Security.md "wikilink")
  - **ESET NOD32 Antivirus**
  - [ESET Multi-Device
    Security](../Page/ESET_Multi-Device_Security.md "wikilink")
    網路安全套裝多平台版
  - [ESET Internet
    Security](../Page/ESET_Internet_Security.md "wikilink")
  - [ESET Smart Security
    Premium](../Page/ESET_Smart_Security_Premium.md "wikilink")

[Mac](../Page/Mac.md "wikilink")：

  - [ESET Cyber Security
    Pro](../Page/ESET_Cyber_Security_Pro.md "wikilink")
  - [ESET Cyber Security](../Page/ESET_Cyber_Security.md "wikilink")

[Linux](../Page/Linux.md "wikilink")：

  - [ESET NOD32 Antivirus 4
    Linux](../Page/ESET_NOD32_Antivirus_4_Linux.md "wikilink") 桌上電腦版本

[行動裝置](../Page/行動裝置.md "wikilink")：

  - ESET Mobile Security for [Android](../Page/Android.md "wikilink")
  - ESET Mobile Security for [Windows
    Mobile](../Page/Windows_Mobile.md "wikilink")
  - ESET Mobile Security for [Symbian](../Page/Symbian.md "wikilink")

### 停產產品

  - Mobile Antivirus

## 版本区别

这里列出了ESET NOD32系列的不同版本的区别\[3\]\[4\]

| 软件名称及功能 | NOD32 Antivirus | Internet Security | Smart Security | Multi-Device Security | Smart Security Premuim |
| ------- | --------------- | ----------------- | -------------- | --------------------- | ---------------------- |
| 注释      | 仅支持杀毒功能         | 多加了网络防火墙功能        | 内置于多平台安全套装     | 多平台安全套装               | 增加密码保护功能               |
| 阻止病毒入侵  | √               | √                 | √              | √                     | √                      |
| 查杀勒索病毒  | √               | √                 | √              | √                     | √                      |
| 安全网购    |                 | √                 | √              | √                     | √                      |
| 网上银行保护  |                 | √                 | √              | √                     | √                      |
| 儿童内容保护  |                 | √                 | √              | √                     | √                      |
| 路由器监管   |                 | √                 | √              | √                     | √                      |
| 网络摄像头保护 |                 | √                 | √              | √                     | √                      |
| 手机防毒    |                 |                   |                | √                     |                        |
| 密码保护    |                 |                   |                |                       | √                      |
| 设备防盗窃   |                 | √                 | √              | √                     | √                      |

## 關連項目

  - [ESET](../Page/ESET.md "wikilink")

## 參考

## 外部連結

  - [ESET臺灣官方網站](http://www.eset.com.tw/)
  - [ESET臺灣官方回報網站](http://phishing.eset.com/report/cht)
  - [ESET臺灣官方序號網站](https://nod32.buzzdope.com/)
  - [ESET中国官方网站](http://www.eset.com.cn/)
  - [ESET香港官方網站](http://www.eset.hk/)
  - [ESET美國官方網站](http://www.eset.com/)

[防毒軟體](../Category/杀毒软件.md "wikilink")
[Category:共享軟件](../Category/共享軟件.md "wikilink")
[Category:商業軟體](../Category/商業軟體.md "wikilink")
[Category:付費軟體](../Category/付費軟體.md "wikilink")
[Category:防火墙软件](../Category/防火墙软件.md "wikilink")
[Category:防火牆軟體](../Category/防火牆軟體.md "wikilink")

1.
2.
3.
4.