**可交换图像文件格式**（，官方簡稱**Exif**），是专门为[数码相机的照片设定的](../Page/数码相机.md "wikilink")，可以记录数-{zh-hans:码;zh-hk:碼;zh-tw:位;}-照片的属性信息和拍摄数据。

Exif最初由[日本電子工業發展協會在](../Page/日本電子工業發展協會.md "wikilink")1996年制定，版本为1.0。1998年，升级到2.1，增加了对音频文件的支持。2002年3月，发表了2.2版。

Exif可以附加于[JPEG](../Page/JPEG.md "wikilink")、[TIFF](../Page/TIFF.md "wikilink")、[RIFF等文件之中](../Page/RIFF.md "wikilink")，为其增加有关数码相机拍摄信息的内容和索引图或图像处理软件的版本信息。

[Windows
7操作系统具备对Exif的原生支持](../Page/Windows_7.md "wikilink")，通过鼠标右键点击图片打开菜单，点击属性并切换到详细信息标签下即可直接查看Exif信息。

Exif信息是可以被任意编辑的，因此只有参考的功能。

Exif信息以0xFFE1作为开头标记，后两个字节表示Exif信息的长度。所以Exif信息最大为64 kB，而内部采用TIFF格式。

## 範例

以下-{表列}-幾項Exif會提供的訊息：

| 項目                                  | 資訊（舉例）                                                                                                                            |
| ----------------------------------- | --------------------------------------------------------------------------------------------------------------------------------- |
| 製造廠商                                | [Canon](../Page/佳能.md "wikilink")                                                                                                 |
| 相機型號                                | [Canon EOS-1Ds Mark III](../Page/Canon_EOS-1Ds_Mark_III.md "wikilink")                                                            |
| 影像方向                                | 正常（upper-left）                                                                                                                    |
| 影像[解析度X](../Page/解析度.md "wikilink") | 300                                                                                                                               |
| 影像解析度Y                              | 300                                                                                                                               |
| 解析度單位                               | [dpi](../Page/dpi.md "wikilink")                                                                                                  |
| 软件                                  | [Adobe](../Page/Adobe.md "wikilink") [Photoshop](../Page/Photoshop.md "wikilink") CS [Macintosh](../Page/Macintosh.md "wikilink") |
| 最後異動時間                              | 2005:10:06 12:53:19                                                                                                               |
| YCbCrPositioning                    | 2                                                                                                                                 |
| 曝光時間                                | 0.00800 (1/125) sec                                                                                                               |
| [光圈值](../Page/光圈.md "wikilink")     | F22                                                                                                                               |
| 拍攝模式                                | 光圈優先                                                                                                                              |
| ISO感光值                              | 100                                                                                                                               |
| Exif資訊版本                            | 30,32,32,31                                                                                                                       |
| 影像拍攝時間                              | 2005:09:25 15:00:18                                                                                                               |
| 影像存入時間                              | 2005:09:25 15:00:18                                                                                                               |
| 曝光補償（EV+-）                          | 0                                                                                                                                 |
| 測光模式                                | 點測光（Spot）                                                                                                                         |
| [閃光燈](../Page/閃光燈.md "wikilink")    | 關閉                                                                                                                                |
| 鏡頭實體焦長                              | 12 mm                                                                                                                             |
| Flashpix版本                          | 30,31,30,30                                                                                                                       |
| 影像色域空間                              | sRGB                                                                                                                              |
| 影像尺寸X                               | 5616 [pixel](../Page/pixel.md "wikilink")                                                                                         |
| 影像尺寸Y                               | 3744 pixel                                                                                                                        |

## 外部链接

  - [Exif 2.3官方标准](http://www.cipa.jp/std/documents/e/DC-008-2012_E.pdf)（PDF文档）
  - [Exif 2.2官方标准](http://www.kodak.com/global/plugins/acrobat/en/service/digCam/exifStandard2.pdf)（PDF文档）
  - [Exif文件格式说明](https://web.archive.org/web/20111025004429/http://park2.wakwak.com/~tsuruzoh/Computer/Digicams/exif-e.html)

相关软件：

  - [Exif查看软件：Opanda IExif](http://www.opanda.com/cn/iexif/)
  - [Exif编辑软件：Opanda PowerExif](http://www.opanda.com/cn/pe/)
  - [Phil Harvey's ExifTool](http://owl.phy.queensu.ca/~phil/exiftool/)
  - [Exif查看软件：KUSO Exif
    Viewer](http://www.google.com/url?q=http%3a%2f%2fwww.kuso.cc/exifviewer)
  - [Exif查看软件：FxIF](http://ted.mielczarek.org/code/mozilla/fxif/)（[Mozilla
    Firefox外掛套件](../Page/Mozilla_Firefox.md "wikilink")）
  - [Exif免费软件：PhotoMe](http://www.photome.de/)

[Category:數位攝影](../Category/數位攝影.md "wikilink")
[Category:元数据](../Category/元数据.md "wikilink")