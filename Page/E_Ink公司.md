**E
Ink公司**（原文是英文的****，中文名**元太科技**）是一家[私人持股公司](../Page/私人持股公司.md "wikilink")，該公司的主要業務在於製造一種以[電泳顯示](../Page/电子纸#电泳显示.md "wikilink")（**e**lectro**p**horetic
**d**isplay；**EPD**）技術的[电子纸](../Page/电子纸.md "wikilink")（electronic
paper）。

E
Ink公司位於[美國](../Page/美國.md "wikilink")[马萨诸塞州的](../Page/马萨诸塞州.md "wikilink")[坎布里奇](../Page/坎布里奇.md "wikilink")，由[Joseph
Jacobson於](../Page/Joseph_Jacobson.md "wikilink")1997年成立，Joseph
Jacobson原是[麻省理工学院](../Page/麻省理工学院.md "wikilink")（MIT）[媒體實驗室的](../Page/麻省理工媒体实验室.md "wikilink")[教授](../Page/教授.md "wikilink")。目前該公司於2009年賣給[元太科技](../Page/元太科技.md "wikilink")。\[1\]E
Ink公司的技術，目前一應用最廣的領域是電子書閲讀器\[2\]。

2012年12月，E Ink收购了一家竞争对手SiPix。\[3\]\[4\]

## 参考来源

## 外部連結

  - [**E Ink公司**的官方網站](http://eink.com/)

[Category:美國電子公司](../Category/美國電子公司.md "wikilink")
[Category:显示科技公司](../Category/显示科技公司.md "wikilink")

1.
2.
3.
4.