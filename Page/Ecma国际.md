**Ecma国际**（）是一家国际性会员制度的[信息和](../Page/信息技术.md "wikilink")[电信](../Page/电信.md "wikilink")[标准组织](../Page/标准组织.md "wikilink")。1994年之前，名为**欧洲计算机制造商协会**（）。因为计算机的国际化，组织的标准牵涉到很多其他国家，因此组织决定改名表明其国际性。现名称已不属于[首字母缩略字](../Page/首字母缩略字.md "wikilink")。

组织在1961年的[日内瓦建立为了标准化欧洲的计算机系统](../Page/日内瓦.md "wikilink")。在欧洲制造、销售或开发计算机和电信系统的公司都可以申请成为会员。

## 目标

Ecma国际的任务包括与有关组织合作开发通信技术和[消费电子标准](../Page/消费电子.md "wikilink")、鼓励准确的标准落实、和标准文件与相关技术报告的出版。四十年来，Ecma建立了很多信息和电信技术标准。组织出版了370[标准](http://www.ecma-international.org/publications/standards/Standard.htm)和90[技术报告](http://www.ecma-international.org/publications/techreports/techrep.htm)，大约三分之二被[国际标准化组织批准为国际标准](../Page/国际标准化组织.md "wikilink")。

与国家政府标准机构不同，Ecma国际是企业会员制的组织。组织的标准化过程比较商业化，自称这种营运方式减少[官僚追求效果](../Page/官僚.md "wikilink")。

## Ecma标准

Ecma国际负责的标准包括：

  - [CD-ROM格式](../Page/CD-ROM.md "wikilink")（之后被[国际标准化组织批准为](../Page/国际标准化组织.md "wikilink")[ISO
    9660](../Page/ISO_9660.md "wikilink")）\[1\]
  - [C\#语言规范](../Page/C♯.md "wikilink")\[2\]
  - [C++/CLI语言规范](../Page/C++/CLI.md "wikilink")
  - [通用語言架構](../Page/通用語言架構.md "wikilink")（CLI）\[3\]
  - [ECMAScript语言规范](../Page/ECMAScript.md "wikilink")（以[JavaScript為基礎](../Page/JavaScript.md "wikilink")）\[4\]
  - [Eiffel语言](../Page/Eiffel.md "wikilink")\[5\]
  - 电子产品环境化设计要素\[6\]
  - [Universal 3D标准](../Page/Universal_3D.md "wikilink")\[7\]
  - [OOXML](../Page/OOXML.md "wikilink")
  - [Dart语言规范](../Page/Dart.md "wikilink")\[8\]

[昇陽一开始申请把](../Page/昇陽.md "wikilink")[Java标准化](../Page/Java.md "wikilink")，但是之后收回其申请，因为昇陽不希望丢失Java的控制权。\[9\]

Ecma被委托引导[HVD标准化的责任](../Page/HVD.md "wikilink")。HVD（全息通用光盘）是一种利用[全息技术的高密度光碟](../Page/全息.md "wikilink")，将能够储存上百[GB](../Page/Gigabyte.md "wikilink")。\[10\]2007年6月11日，Ecma出版了两版HVD标准\[11\]：ECMA-377\[12\]（200GB的HVD插卡）和ECMA-378\[13\]（100GB的HVD-ROM光盘）。他下一步准备建立一个30GB的HVD标准和提交这些标准给[国际标准化组织批准](../Page/国际标准化组织.md "wikilink")。\[14\]

## 参考资料

## 外部链接

  - [Ecma International](http://www.ecma-international.org/)

[Category:标准制订机构](../Category/标准制订机构.md "wikilink")
[Category:電信組織](../Category/電信組織.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.  <http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-408.pdf>
9.
10.
11.
12.
13.
14.