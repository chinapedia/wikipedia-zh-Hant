**EditPlus**是[Windows下的一个](../Page/Windows.md "wikilink")[文本编辑器](../Page/文本编辑器.md "wikilink")。支持[语法高亮](../Page/语法高亮.md "wikilink")，自动完成等功能。对[HTML文件编辑特别方便](../Page/HTML.md "wikilink")。可通过[FTP远程编辑](../Page/FTP.md "wikilink")。EditPlus由[韓國人Kim](../Page/大韓民國.md "wikilink")
Sang-il（）開發。

## 概述

EditPlus是一款小巧但是功能强大的可处理文本、HTML和程序语言的32位编辑器。EditPlus拥有无限制的撤消与重做、英文拼字检查、自动换行、列数标记、搜寻取代、同时编辑多文件、全屏幕浏览甚至监视系统剪切板等功能。EditPlus功能强大，界面简洁美观，且启动速度快；中文支持比较好；支持语法高亮；支持代码折叠；支持代码自动完成，***不支持代码提示功能***；配置功能强大，且比较容易，扩展也比较强。

默认支持HTML、CSS、PHP、ASP、Perl、C/C++、Java、JavaScript和VBScript等语法高亮显示，通过定制语法文件，可以扩展到其他程序语言，在官方网站上可以下载（大部分语言都支持）。

提供了多工作窗口。不用切换到桌面，便可在工作区域中打开多个文档。

正确地配置Java的编译器“Javac”以及解释器“Java”后，使用EditPlus的菜单可以直接编译执行Java程序

EditPlus提供了与Internet的无缝连接，可以在EditPlus的工作区域中打开Intelnet浏览窗口。

[Category:Windows文本编辑器](../Category/Windows文本编辑器.md "wikilink")
[Category:HTML编辑器](../Category/HTML编辑器.md "wikilink")
[Category:Notepad替代](../Category/Notepad替代.md "wikilink")
[Category:共享軟件](../Category/共享軟件.md "wikilink")