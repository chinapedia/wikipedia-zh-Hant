**Eiffel 65**
是一支来自[意大利城市](../Page/意大利.md "wikilink")[都灵的](../Page/都灵.md "wikilink")3人音乐组合。他们主要以[电子乐](../Page/电子乐.md "wikilink")、[eurodance](../Page/eurodance.md "wikilink")、[italodance的风格为主](../Page/italodance.md "wikilink")。最为有名的曲目是：《Blue
(Da Ba Dee)》。

## 成员

  - Jeffrey Jey（1970年1月5日出生于[西西里岛](../Page/西西里岛.md "wikilink")）- 主唱
  - Maurizio Lobina（1973年10月30日出生于 Asti，皮埃蒙特）- 键盘手
  - Gabry Ponte（1973年4月20日出生于[都灵](../Page/都灵.md "wikilink")）- DJ

## 单曲

  - *Blue (Da Ba Dee)*（1999年）
  - *Move your body*（1999年）
  - *Too much of heaven*（2000年）
  - *One Goal*（2000年／法国）
  - *Back in Time*（2000年／意大利）
  - *Lucky (in my life)*（2001年）
  - *80's Stars*（2001年／意大利）
  - *Losing You*（2001年／加拿大）
  - *Cosa resterà (in a Song)*（2002年／意大利）
  - *Quelli che non hanno età*（2003年／意大利）
  - *Viaggia insieme à me*（2003年／意大利）
  - *Una notte e forse mai più*（2003年／意大利）
  - *Voglia di Dance All Night*（2004年／意大利）

## 单曲排行

| 时间    | 歌名                            | 排行    |
| ----- | ----------------------------- | ----- |
| 德国    | 奥地利                           | 瑞士    |
| 1999年 | **Blue (Da Ba Dee)**          | **1** |
| 1999年 | **Move Your Body**            | 4     |
| 2000年 | **Too Much of Heaven**        | 35    |
| 2000年 | **One Goal**                  | \-    |
| 2000年 | **Back in Time**              | \-    |
| 2001年 | **Lucky (in my life)**        | 57    |
| 2001年 | **80's Stars**                | \-    |
| 2001年 | **Losing You**                | \-    |
| 2002年 | **Cosa resterà (in a Song)**  | \-    |
| 2003年 | **Quelli che non hanno età**  | \-    |
| 2003年 | **Viaggia insieme à me**      | \-    |
| 2003年 | **Una notte e forse mai più** | \-    |
| 2004年 | **Voglia di Dance All Night** | \-    |
|       |                               |       |

## 专辑

  - *Europop*（1999年11月）
  - *Contact*（2001年7月）
  - *Eiffel 65*（2003年6月）
  - *Eiffel 65 - Special Edition*（2004年3月）
  - *Crash Test 01*（2006年10月）- als [Bloom
    06](../Page/Bloom_06.md "wikilink")

## 专辑排行

| 时间    | 名称                              | 排行 |
| ----- | ------------------------------- | -- |
| 德国    | 奥地利                             | 瑞士 |
| 1999年 | **Europop**                     | 37 |
| 2001年 | **Contact**                     | \- |
| 2003年 | **Eiffel 65**                   | \- |
| 2004年 | **Eiffel 65 - Special Edition** | \- |

## 链接

  - <https://web.archive.org/web/20080827165206/http://www.bloom06.com/>
    Bloom 06官方主页
  - <http://www.bloom06.ucoz.ru> Bloom 06 Best Fan-Site 官方主页

[Category:意大利音乐](../Category/意大利音乐.md "wikilink")
[Category:Dance音乐](../Category/Dance音乐.md "wikilink")