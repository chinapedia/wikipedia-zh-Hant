《**Elva
First**》是[臺灣](../Page/臺灣.md "wikilink")[歌手](../Page/歌手.md "wikilink")[蕭亞軒於](../Page/蕭亞軒.md "wikilink")2001年10月23日發行的第一張個人英文專輯。第一張英文[EP](../Page/EP.md "wikilink")「[Never
Look
Back](../Page/Never_Look_Back.md "wikilink")」獲得不錯評價，因此在日本發行這張專輯。主打歌「ONE」是翻唱日本女歌手[Sakura的歌曲](../Page/Sakura.md "wikilink")，她也負責填寫英文詞。「Remember」則是「明天」（收錄於《[明天](../Page/明天_\(音樂專輯\).md "wikilink")》專輯）的英文版，由蕭亞軒本人作詞。

台灣銷量12萬張 全亞洲50萬張

## 曲目

## 音樂錄影帶

| 歌曲名稱            | 導演 | 附註   |
| --------------- | -- | ---- |
| Never Look Back |    | 首波主打 |

## 參考文獻

[Category:蕭亞軒音樂專輯](../Category/蕭亞軒音樂專輯.md "wikilink")
[Category:2001年音樂專輯](../Category/2001年音樂專輯.md "wikilink")
[Category:台灣流行音樂專輯](../Category/台灣流行音樂專輯.md "wikilink")