**阿姆**（，另有一別名為），本名**马绍尔·布鲁諾·马瑟斯三世**（，）\[1\]，是一位美國[饒舌歌手](../Page/饒舌歌手.md "wikilink")、[音樂製作人及演員](../Page/音樂製作人.md "wikilink")。他在1997年被饒舌歌手兼製作人[Dr.
Dre發掘](../Page/Dr._Dre.md "wikilink")，其後簽約至所屬的唱片公司。在其音樂生涯中，至今共獲得十五次[格莱美獎的獎項](../Page/格莱美獎.md "wikilink")，以及[奧斯卡最佳電影歌曲獎項](../Page/奧斯卡金像獎.md "wikilink")。阿姆是世界上最暢銷的音樂歌手之一。

阿姆的專輯累積銷售，已使他成為21世紀前十年全球唱片銷量最高的歌手\[2\]，是2000年代最暢銷的藝人\[3\]。世界銷量已超過8000萬張\[4\]。他被[滾石雜誌譽為](../Page/滾石雜誌.md "wikilink")「最偉大的饒舌歌手」\[5\]。

## 生平

### 1972–1997：早期生涯和《Infinite》

生於[密蘇里州聖約瑟夫](../Page/密蘇里州.md "wikilink")（St.
Joseph），出生不久後，父親便離開了他的家庭。在12歲之前，母親帶著他多次搬家，如[堪薩斯](../Page/堪薩斯.md "wikilink")、莎凡娜（Savannah）等地，直到後來他們在[密歇根州](../Page/密歇根州.md "wikilink")[底特律的郊區](../Page/底特律.md "wikilink")[沃倫市定居](../Page/沃倫_\(密西根州\).md "wikilink")。在聽過[Beastie
Boys的專輯](../Page/Beastie_Boys.md "wikilink")《Licensed to
Ill》以及舅舅所送的一首Ice-T的單曲之後，阿姆對[嘻哈樂產生了興趣](../Page/嘻哈.md "wikilink")，14歲便第一次登臺演出。[高中時期經常參加嘻哈比賽](../Page/高中.md "wikilink")（freestyle
battle），在地下嘻哈樂迷中有一定的人氣。兩個使用過的名字 "Manix" 和
"M\&M，後來演變成了Eminem\[6\]。因為經常翹課的緣故，阿姆讀了兩次九年級，在17歲的時候輟學。

從1992年起，阿姆和哥們Jeff和Mark Bass管理的FBT
Productions簽下合約，有時候他也在位於底特律聖克雷爾畔的Gilbert's
Lodge餐廳打一份最低工資的洗碗和烹飪的工作。在1996年，阿姆發行了他的首張專輯《Infinite》，這張專輯是在Bass
Brothers擁有的Bassment工作室錄製的，並以他們的獨立廠牌Web
Entertainment出版。阿姆回憶說：“很明顯，那時候我很年輕並且受到其他音樂人的影響，我得到很多人反應，說我唱得像Nas和AZ。《Infinite》是一張我想去找出我自己的嘻哈風格、怎樣在[麥克風前唱出和展示我自己的專輯](../Page/麥克風.md "wikilink")。這是一個成長的舞臺，我感覺《Infinite》就像一個我剛剛起步的演示。”這張專輯的歌詞裡唱到的內容包括阿姆撫養他剛出生的女兒海莉（Hailie）時，沒有足夠資金的窘迫處境。在事業的早期，阿姆以“Bad
Meets Evil”的藝名和夥伴底特律嘻哈藝人Royce da 5'9"合作。後來他為pornhub 拍攝一系列的廣告，放膽裸體，非常開放。

### 1998–1999: 《The Slim Shady LP》

根據美國《[告示牌雜誌](../Page/告示牌_\(雜誌\).md "wikilink")》的描述，阿姆在人生的這一點上已經到了“實現我的音樂夢想是擺脫痛苦生活的唯一出路”的程度。在1998年阿姆與Aftermath
Entertainment/Interscope Records簽約後，阿姆於1999年推出了他的首張大牌工作室專輯《The Slim
Shady LP》，由Dr.
Dre擔當製作人。關於這張專輯，引用Billboard的原話，是“野蠻的”和“在歌詞材料的幾年之前他已經事先開始寫了”。《The
Slim Shady
LP》成為1999年美國最流行的專輯之一，在年底獲得了鉑金唱片的銷量。但隨著這張唱片的流行，有關其中歌詞的爭議也隨之而來。在歌曲"97
Bonnie and Clyde"中，他描述了一趟與還是嬰兒的女兒的旅行中，是怎樣處置他妻子的屍體。另外一首歌"Guilty
Conscience"中，結束時阿姆鼓勵一個男人去謀殺他的妻子和他妻子的情人。"Guilty Conscience" 標誌著阿姆與Dr.
Dre牢固朋友關係和音樂合作的開始，他們兩人在之後一起合作製作了一連串轟動的歌曲，包括Dr. Dre的專輯《2001》裡的"Forgot
About Dre"和"What's the Difference"，阿姆《The Marshall Mathers LP》裡的"Bitch
Please"、《The -{Eminem}- Show》裡的"Say What You Say"和《Encore》裡的"Curtains
Down" 。所以，Dr. Dre會在阿姆的工作室製作的所有Aftermath廠牌的專輯裡至少客串一次。

### 2000–2001: 《The Marshall Mathers LP》

阿姆的第二張專輯《The Marshall Mathers
LP》於2000年5月發行。在發行後一周內就賣出了176萬張，在美國打破了[Snoop
Dogg的](../Page/Snoop_Dogg.md "wikilink")《Doggystyle》保持的最快銷售嘻哈專輯記錄和[小甜甜布蘭妮](../Page/布蘭妮·斯皮爾斯.md "wikilink")《...Baby
One More Time》保持的最快銷售個人專輯記錄。《The Marshall Mathers LP》裡最先發行的歌曲"The Real
Slim
Shady"很成功，不過也帶來一些爭議，因為阿姆在這首歌中侮辱了和懷疑了許多明星，其中之一是他提及到[克莉絲汀·阿奎萊拉為Carson](../Page/克莉絲汀·阿奎萊拉.md "wikilink")
Daly跟[林普巴茲提特的Fred](../Page/林普巴茲提特.md "wikilink")
Durst[口交](../Page/口交.md "wikilink")。在阿姆的第二首歌"The Way I
Am"中，他揭示唱片公司給了他很多壓力去重奪"My Name Is"的第一寶座和賣出更多的唱片。雖然阿姆在"My
Name
Is"的音樂錄影帶中模仿了搖滾明星[瑪麗蓮·曼森](../Page/瑪麗蓮·曼森.md "wikilink")，可是他們的關係卻很好。他們一起在一個演唱會上合作了一首混音版的"The
Way I Am"。第三首歌曲"Stan"取了[Dido的歌曲](../Page/Dido.md "wikilink")"Thank
You"的音樂樣本，在這首歌曲中，阿姆開始以他的名利為題材，描述到他的一個發瘋的歌迷自杀并且害死怀孕的女友，是《The Slim
Shady LP》的"97 Bonnie &
Clyde"的回應。美國Q雜誌把"Stan"列為史上第三偉大的的饒舌歌曲，這首歌也在Top40-Charts.com網站進行的一個類似調查中排到了第十名。"Stan"獲得了很多好評，在[滾石雜誌的](../Page/滾石雜誌.md "wikilink")"500首史上最偉大歌曲"排行榜中排到了第290位。2000年7月，阿姆成為第一個登上《The
Source》雜誌封面的白人。

阿姆在2001年舉辦了很多巡迴演唱會，其中包括饒舌歌手Dr. Dre、Snoop Dogg、Xzibit和Ice Cube助陣的“Up In
Smoke Tour”和搖滾樂隊[林普巴茲提特助陣的](../Page/林普巴茲提特.md "wikilink")“Family Values
Tour”。

### 2002–2003: 《The -{Eminem}- Show》

阿姆的第三張主打專輯《The -{Eminem}-
Show》在2002年夏天發行，在第一周內賣出了超過一百萬張拷貝，登上唱片銷售榜的第一位，成為又一轟動。裡面精選了歌曲"Without
Me"，一個明顯的"The Real Slim Shady"的續作，裡面作了很多針對男孩樂隊例如Limp Bizkit、Moby和Lynne
Cheney的批評言論。這張專輯反映了阿姆取得名利後的影響、與女兒和妻子的關係和他在嘻哈音樂界的地位。阿姆也提及到2000年他毆打一名親吻他妻子的保鏢。Allmusic網站的Stephen
Thomas Erlewine認為，因為《The -{Eminem}- Show》的很多歌曲展示了真正的憤怒，故這張專輯被認為沒有《The
Marshall Mathers LP》那麼瘋狂。然而，曾經批評過《The Marshall Mathers LP》裡厭惡女人的傾向的L.
Brent Bozell III，認為《The -{Eminem}- Show》裡大量使用了淫穢語言，並且因為阿姆在專輯的潔版（clean
edition）裡任意刪除了裡面到處都有的淫穢詞彙"motherfucker"，所以給了阿姆一個綽號："Eminef"（阿賊）。

### 2004–2005: 《Encore》

2003年12月8日，[美國特勤處認為阿姆的歌曲](../Page/美國特勤處.md "wikilink")"We As
Americans"中的歌詞"Fuck money / I don't rap for 『dead presidents』 / I'd
rather see 『the president dead』 / It's never been said, but I set
precedents..."威脅到[美國總統](../Page/美國總統.md "wikilink")[小布什](../Page/小布什.md "wikilink")。這首歌本來是收錄在新專輯《Encore》的曲目中，但因為以上的原因，最後只能收錄在《Encore》的bonus
CD中。

2004年阿姆發行了他的第四張主打專輯《Encore》，這張專輯也成為了排行榜上的冠軍。阿姆在歌曲"Just Lose
It"中很明顯地表達了對[麥可·傑克森的無禮](../Page/麥可·傑克森.md "wikilink")。2004年10月12日，在《Encore》的第一首單曲"Just
Lose It"發行的一個月後，麥可·傑克森在洛杉磯的Steve Harvey電臺的節目上談到了"Just Lose
It"的音樂錄影帶給他帶來的不快。"Just Lose
It"的音樂錄影帶裡阿姆模仿了麥可·傑克森的性侵兒童案審判，整容手術和1984年拍攝[百事可樂廣告時頭髮不慎著火的事](../Page/百事可樂.md "wikilink")。"Just
Lose It"的歌詞提到了麥可·傑克森法律上的麻煩事，阿姆在歌詞裡提到："...and that's not a stab at
Michael/That's just a metaphor/I'm just
psycho...."（那不是對麥可的諷刺，這只是個比喻，我只是個神經病）
。很多麥可·傑克森的歌迷和朋友都指責了這個音樂錄影帶，包括[史提夫·汪達](../Page/史提夫·汪達.md "wikilink")，他說這個音樂錄影帶"打擊一個正在困境中的人"、"胡扯"。Steve
Harvey也說：“阿姆已經失去了他的貧民區通行證，我們希望他會拿回來。”在這個音樂錄影帶中，阿姆模仿了Pee Wee Herman、MC
Hammer還有[麥當娜](../Page/麥當娜.md "wikilink")。

黑人娛樂電視（Black Entertainment Television）是第一個停止播出“Just Lose
It"音樂錄影帶的頻道。然而MTV卻宣佈將繼續播出這個MV。The
Source的首席執行官Raymond "Benzino"
Scott，不單單要求禁止播出這個MV，還要求阿姆將這首歌從專輯中抹去和對麥可·傑克森道歉。在2007年米高·積遜和索尼公司從Viacom公司收購了Famous
Music
LLC。這個交易給予了[麥可·傑克森](../Page/麥可·傑克森.md "wikilink")、阿姆、[夏奇拉和](../Page/夏奇拉.md "wikilink")[Beck歌曲的版權](../Page/貝克.md "wikilink")。儘管這張專輯的第一首歌曲是喜劇主題的，《Encore》裡面大多都是嚴肅題材的歌曲，包括一首反戰歌曲"Mosh"。2004年10月25日，正好是美國總統選舉的一周前，阿姆在互聯網上發行了"Mosh"的音樂錄影帶。這首歌很明顯地帶有反布希的傾向，裡面的歌詞"fuck
Bush"與"this weapon of mass destruction that we call our
president"（這位我們稱為總統的大殺傷力武器）都體現了這一點。錄影帶裡阿姆召集了一支人民軍隊，裡面包括饒舌歌手Lloyd
Banks，它代表了布希政府統治下的受害者，阿姆把他們指引到了白宮。然而，在這支軍隊的一次休息中，出現了他們在簡單註冊後投票的場景。這個音樂錄影帶以螢幕上出現
「VOTE Tuesday November
2」（十一月二日星期二**去投票**）的文字而結束。在布希贏得選舉之後，錄影帶的結尾改成了阿姆和抗議者們在布希發表演說時突然進入會場搗亂。
同時，阿姆日後發布的《Live From New York
2005》也是在這一階段錄製，由於大量服用成癮性藥物，阿姆這一段時間的狀態非常低落。有人認為阿姆「自從《The
-{Eminem}- Show》之後就沒做過什麼好事」。從《Live From New York
2005》中可以看出，阿姆在台上並沒有表現出其應有的激情，在幾首並不出彩的歌曲（如《Rain
Man》《Ass Like That》等），演唱會的氣氛很大程度上是靠同台的Proof、Bizarre等人以及瘋狂熱情的觀眾營造起來的。

### 2005–2008: 事業中斷

2005年，一些業內人士推測阿姆正在考慮結束他六年的出了不少白金專輯的饒舌生涯。年初有謠言說在年底會有一張雙碟專輯《The
Funeral》發行。這張精選專輯最後叫做《Curtain Call: The
Hits》，在2005年12月6號由Aftermath
Entertainment發行。2005年7月，底特律自由報爆料說阿姆未來可能成為一名個人表演者，引用了阿姆圈子裡的朋友說他會全心全意做一名發行者和唱片管理者的話。在這張精選集發行的同一天，阿姆在底特律本地電臺WKQI的節目"Mojo
in the Morning"中否認他將會退休，不過卻暗示他會在明星生涯中稍作休息，他說道："I'm at a point in my life
right now where I feel like I don't know where my career is going ...
This is the reason that we called it 'Curtain Call', because this could
be the final thing. We don't
know."（我現在處於人生中的這一點，不知道我的事業要怎樣繼續下去……這也是我將之命名為「落幕」(「Curtain
Call」)的原因，因為這有可能是最後的（專輯）。誰都不知道。） 同年，阿姆被作家貝納德·戈德堡（Bernard
Goldberg）在新書《一百個搞砸了美國的人》中排在第58位。戈德堡引用了紐約時報卜·赫伯特（Bob
Herbert）在2001年時說的話：“在阿姆的世界裡，所有的婦女都是妓女，他都想把她們強姦和屠殺掉。”阿姆專輯《The Slim Shady
EP》裡的歌曲"No One's Iller"被戈德堡用來作為表明阿姆的這種厭惡的例子。

2005年夏天，阿姆展開了為期三年的全美巡迴演唱會“the Anger Management 3 Tour"，有包括[50
Cent](../Page/50_Cent.md "wikilink")、G-Unit、Lil'Jon、D12、Obie Trice、The
Alchemist和其他的明星助陣。在同年8月，阿姆取消了歐洲巡演，之後為了逃避Proof死亡所帶來的打擊而開始嗑藥，並且進出勒戒所。

### 2008－2010：《Relapse》和《Recovery》

2007年9月，阿姆給紐約電臺Hot 97在採訪50
Cent時打了一個電話。Eminem表示他正在考慮是否應該發行一張新的專輯。他說：「我一直在工作－－我一直在錄音間裡。現在我感覺很好，動力很充足。有段時間，我不想進到錄音間內……我處理了一些私事。我現在從那些私事的陰影中走出後，感覺真的很好。」

2008年9月，阿姆在他自己的電視臺Shade
45向公眾露了面，他說：「現在我正在集中思想做我的工作，也就是錄錄歌之類的。你們知道，當我做出更多的歌曲我就會漸漸的變得更好。因為我剛剛開始瞭解饒舌界的動態。」正在這段時間，Interscope宣布阿姆將在2009年春天發行新專輯。

2008年12月，阿姆發表關於新專輯的更多細節，並且公布這張專輯的名稱為《Relapse》。他說：「我和Dre又像過去那樣一起混在錄音間內。Dre最後會以發行者身分製作《Relapse》專輯裡大部分的歌。我們又回到過去的形式了……現在就這樣吧。」

2009年3月5日阿姆宣佈他會在2009年發行兩張專輯。第一張《Relapse》在5月19日發行，首發《We Made
You》單曲以及音樂錄影帶提前在4月7日發行。

2009年10月3日阿姆和DJ Whoo Kid一起在[Shade
45電視臺上宣佈Denaun](../Page/Shade_45.md "wikilink") Porter和Just
Blaze已努力完成《Recovery》。

### 2012–2013: 《The Marshall Mathers LP 2》

2012年協助旗下Slaughter House拍攝〈My life〉的MV，以及《Welcome to our :
house》專輯出版，並跨刀參與許多首歌曲的演唱。

2012年底宣布再次回歸樂壇，以一首跟[魔力紅主唱Adam](../Page/魔力紅.md "wikilink") Levine、50
Cent合作的〈My Life〉正式復出。隔周再次與Skylar Grey合作一首歌曲〈C'mon Let Me Ride〉。

2013年8月13日
發佈單曲<Survival>，為電玩[決勝時刻：魅影](../Page/決勝時刻：魅影.md "wikilink")(Call
of Duty: Ghosts)之宣傳曲。

2013年 8月26日 發佈單曲<Berzerk>，空降當周告示牌百強榜第3名,隨後在一週後音樂錄影帶釋出。單曲亦是Beats耳機的宣傳曲之一。

2013年11月5日 《The Marshall Mathers LP 2》正式發行。內容延續13年前《The Marshall Mathers
LP》狂放不羈的形象,連頭髮再度染成金色。
新專輯首周在美國售出79萬2千張，空降Billboard榜第一位，為本年首周第二銷量高的專輯，而且更在短短兩周裏得到了[美國唱片業協會的](../Page/美國唱片業協會.md "wikilink")[白金唱片銷售認證](../Page/白金唱片.md "wikilink")。

### 2014–2016: Shady XV, vinyl box set, 和 Southpaw

### 2017 - 現在：《Revival》 和 《Kamikaze》

## 作品

### 专辑

  - 《[Infinite](https://en.wikipedia.org/wiki/Infinite_\(Eminem_album\))》
    (1996)
  - 《[The Slim Shady
    LP](https://en.wikipedia.org/wiki/The_Slim_Shady_LP)》 (1999)
  - 《[The Marshall Mathers
    LP](https://en.wikipedia.org/wiki/The_Marshall_Mathers_LP)》 (2000）
  - 《[The -{Eminem}-
    Show](https://en.wikipedia.org/wiki/The_Eminem_Show)》 (2002)
  - 《[Encore](https://en.wikipedia.org/wiki/Encore_\(Eminem_album\))》
    (2004)
  - 《[Relapse](https://en.wikipedia.org/wiki/Relapse_\(Eminem_album\))》
    (2009)
  - 《[Recovery](https://en.wikipedia.org/wiki/Recovery_\(Eminem_album\))》
    (2010)
  - 《[The Marshall Mathers
    LP 2](https://en.wikipedia.org/wiki/The_Marshall_Mathers_LP_2)》
    (2013)
  - 《[Revival](https://en.wikipedia.org/wiki/Revival_\(Eminem_album\))》
    (2017)
  - 《[Kamikaze](https://en.wikipedia.org/wiki/Kamikaze_\(Eminem_album\))》
    (2018)

:\* “The Slim Shady LP”、“The Marshall Mathers LP”、“The Eminem
show”、“Relapse”、“Recovery”、“The Marshall Mathers LP
2”曾获格莱美最佳饒舌专辑奖。

:\*“The Marshall Mathers LP”是历史上最为畅销的饒舌专辑。这张专辑為阿姆和他的唱片公司Shady
Records带来知名度，并把他所在的饒舌组合D12带入饒舌界主流。

:\* 在阿姆主演的半自傳電影《[八英里](../Page/八里公路.md "wikilink")》（8 Mile）中的主題單曲"Lose
Yourself"，被獲選為[第75屆奧斯卡金像獎最佳原創歌曲獎](../Page/第75屆奧斯卡金像獎.md "wikilink")，也是第一個以饒舌歌曲拿下這個獎項的藝人。

:\* “Stan”被牛津英語詞典收錄成一詞，這讓 Eminem
的粉絲們激動不已，對這一單詞的解釋是，作為名詞用作指「某個明星的瘋狂粉絲」，此外還可以作為動詞來使用。單詞來自
Eminem 的經典歌曲“Stan”。

### [D12](../Page/D12.md "wikilink")

  - 《[Devil's
    Night](https://en.wikipedia.org/wiki/Devil%27s_Night_\(album\))》(2001)
  - 《[D12 World](https://en.wikipedia.org/wiki/D12_World)》(2004)

### [精選輯](../Page/精選輯.md "wikilink")

  - 《[Curtain Call: The
    Hits](https://en.wikipedia.org/wiki/Curtain_Call:_The_Hits)》(2005)

### [合輯](../Page/合輯.md "wikilink")

  - 《[8 Mile
    電影原聲帶](https://en.wikipedia.org/wiki/8_Mile:_Music_from_and_Inspired_by_the_Motion_Picture)》(2002)
  - 《[The -{Eminem}- Presents: The
    Re-Up](https://en.wikipedia.org/wiki/Eminem_Presents:_The_Re-Up)》(2006)
  - 《[Shady XV](https://en.wikipedia.org/wiki/Shady_XV)》(2014)

### 電影

  - 《[街頭痞子](../Page/街頭痞子.md "wikilink")》(2002)
  - 《[采访](../Page/采访_\(电影\).md "wikilink")》(2014)

### 單曲排名

|            |                                                        |                       |                                      |                         |                |
| ---------- | ------------------------------------------------------ | --------------------- | ------------------------------------ | ----------------------- | -------------- |
| **年 份**    | **歌 曲**                                                | **Billboard Hot 100** | **Billboard Hot R\&B/Hip-Hop Songs** | **Billboard Rap Songs** | **UK Singles** |
| *1999*     | **My Name Is**                                         | *36*                  | *18*                                 | *10*                    | *2*            |
| '' - ''    | **Guilty Conscience**                                  | '' - ''               | *56*                                 | *-*                     | *5*            |
| *2000*     | **The Real Slim Shady**                                | *4*                   | *11*                                 | *7*                     | *1*            |
| '' - ''    | **The Way I Am**                                       | *58*                  | *26*                                 | *-*                     | *8*            |
| '' - ''    | **Stan**                                               | *51*                  | *36*                                 | *-*                     | *1*            |
| *2002*     | **Without Me**                                         | *2*                   | *13*                                 | *5*                     | *1*            |
| '' - ''    | **Cleanin' Out My Closet**                             | *4*                   | *11*                                 | *-*                     | *4*            |
| '' - ''    | **Lose Yourself**                                      | *1 (12週)*             | *4*                                  | *2*                     | *1*            |
| *2003*     | **Superman**                                           | *15*                  | *44*                                 | *10*                    | '' - ''        |
| '' - ''    | **Sing For The Moment**                                | *14*                  | '' - ''                              | *18*                    | *6*            |
| '' - ''    | **Business**                                           | '' - ''               | *77*                                 | *25*                    | *6*            |
| *2004*     | **Just Lose It**                                       | *6*                   | *35*                                 | *7*                     | *1*            |
| '' - ''    | **Encore**                                             | *25*                  | *48*                                 | *20*                    | '' - ''        |
| *2005*     | **Like Toy Soldiers**                                  | *34*                  | *64*                                 | '' - ''                 | *1*            |
| '' - ''    | **Mockingbird**                                        | *11*                  | *51*                                 | *10*                    | *4*            |
| '' - ''    | **Ass Like That**                                      | *60*                  | *93*                                 | '' - ''                 | *4*            |
| '' - ''    | **When I'm Gone**                                      | *8*                   | *96*                                 | *22*                    | *4*            |
| *2006*     | **Shake That**                                         | *6*                   | '' - ''                              | *11*                    | *90*           |
| *2009*     | **Crack A Bottle**(Eminem featuring Dr. Dre & 50 Cent) | *1*                   | *60*                                 | *4*                     | *4*            |
| '' - ''    | **We Made You**                                        | *9*                   | '' - ''                              | '' - ''                 | *4*            |
| '' - ''    | '''3 A.M. '''                                          | *32*                  | '' - ''                              | '' - ''                 | *56*           |
| '' - ''    | **Beautiful**                                          | *17*                  | '' - ''                              | '' - ''                 | *38*           |
| *2010*     | **Not Afraid**                                         | *1 (空降冠軍)*            | *7*                                  | *2*                     | *5*            |
| '' - ''    | **Love The Way You Lie**(Eminem featuring Rihanna)     | *1 (7週)*              | *7*                                  | *1*                     | *2*            |
| '' - ''    | **No Love**(Eminem featuring Lil' Wayne)               | *23*                  | *59*                                 | *9*                     | *33*           |
| *2011*     | **Space Bound**                                        | '' - ''               | '' - ''                              | '' - ''                 | *34*           |
| '' - ''    | **Fast Lane**                                          | *32*                  | '' - ''                              | '' - ''                 | *66*           |
| '' - ''    | **Lighters**                                           | *4*                   | *75*                                 | *6*                     | *10*           |
| *2012*     | **Throw That**(Slaughterhouse featuring Eminem)        | *98*                  | '' - ''                              | '' - ''                 | '' - ''        |
| '' - ''    | **My Life**(50 Cent featuring Eminem and Adam Levine)  | *27*                  | *6*                                  | '' - ''                 | *2*            |
| *2013*     | **Berzerk**                                            | *3*                   | *2*                                  | *1*                     | *2*            |
| '' - ''    | **Survival**                                           | *16*                  | '' - ''                              | *4*                     | *22*           |
| '' - ''    | **Rap God**                                            | *7*                   | *2*                                  | *1*                     | *5*            |
| '' - ''    | **The Monster**(Eminem featuring Rihanna)              | *1 (4週)*              | *1*                                  | *1*                     | *1*            |
| *2014*     | **Headlights**                                         | *45*                  | *11*                                 | *5*                     | *63*           |
| '' - ''    | **Guts Over Fear**                                     | *22*                  | *6*                                  | *4*                     | *10*           |
| *2017*     | **Walk On Water**(Eminem featuring Beyoncé)            | '' 14 ''              | '' 6 ''                              | '' - ''                 | '' 7 ''        |
| '' - ''    | **Untouchable**                                        | '' 86 ''              | '' 36 ''                             | '' - ''                 | '' 73 ''       |
| '' - ''    | **River**(Eminem featuring Ed Sheeran)                 | *11*                  | '' 5 ''                              | '' - ''                 | '' 1 ''        |
| '' 2018 '' | **Killshot**                                           | *-*                   | '' - ''                              | '' - ''                 | '' - ''        |
| '' - ''    | **Not Alike**                                          | *-*                   | '' - ''                              | '' - ''                 | '' - ''        |
| '' - ''    | **fall**                                               | *-*                   | '' - ''                              | '' - ''                 | '' - ''        |
| '' - ''    | **Greatest**                                           | *-*                   | '' - ''                              | '' - ''                 | '' - ''        |
| '' - ''    | **Nice Guy**                                           | *-*                   | '' - ''                              | '' - ''                 | '' - ''        |
| '' - ''    | **Normal**                                             | *-*                   | '' - ''                              | '' - ''                 | '' - ''        |
| '' - ''    | **Lucky you**                                          | *-*                   | '' - ''                              | '' - ''                 | '' - ''        |
| '' - ''    | **Good Guy**                                           | *-*                   | '' - ''                              | '' - ''                 | '' - ''        |
| '' - ''    | **stepping stone**                                     | *-*                   | '' - ''                              | '' - ''                 | '' - ''        |
| '' - ''    | **venom**                                              | *-*                   | '' - ''                              | '' - ''                 | '' - ''        |
| '' - ''    | **Kamikaze**                                           | *-*                   | '' - ''                              | '' - ''                 | '' - ''        |
| '' - ''    | **The ringer**                                         | *-*                   | '' - ''                              | '' - ''                 | '' - ''        |
| '' 2019 '' | **-**                                                  | *-*                   | '' - ''                              | '' - ''                 | '' - ''        |
|            |                                                        |                       |                                      |                         |                |

## 世界紀錄

  - [Facebook按讚次數第](../Page/Facebook.md "wikilink")3高，總共98,809,098按讚(僅次於[夏奇拉](../Page/夏奇拉.md "wikilink"),[C羅](../Page/C羅.md "wikilink"))
  - 專輯總銷售量超過9000萬，是饒舌專輯賣最多的歌手
  - Rap God《饒舌之神》一曲，創下「最多文字的流行歌曲」的金氏世界紀錄

## 注釋

## 外部連結

  - [官方網站](http://www.eminem.com)

  -
  -
  -
  - [阿姆台灣官方部落格](http://eminemrelapse.pixnet.net/blog)

  - [-{Eminem}-中文網](http://www.eminem.com.cn)

  - [Shady唱片公司官方網站](http://www.shadyrecords.com)

  - [IMDB上的條目](http://us.imdb.com/Name?Eminem)

[Category:世界音樂獎獲得者](../Category/世界音樂獎獲得者.md "wikilink")
[Category:葛萊美獎獲得者](../Category/葛萊美獎獲得者.md "wikilink")
[Category:全英音樂獎獲得者](../Category/全英音樂獎獲得者.md "wikilink")
[Category:艾美獎獲獎者](../Category/艾美獎獲獎者.md "wikilink")
[Category:奥斯卡最佳歌曲奖获奖作曲家](../Category/奥斯卡最佳歌曲奖获奖作曲家.md "wikilink")
[Category:美國男歌手](../Category/美國男歌手.md "wikilink")
[Category:美國饒舌歌手](../Category/美國饒舌歌手.md "wikilink")
[Category:美国音乐制作人](../Category/美国音乐制作人.md "wikilink")
[Category:密蘇里州人](../Category/密蘇里州人.md "wikilink")
[Category:美國電影男演員](../Category/美國電影男演員.md "wikilink")
[Category:美国嘻哈歌手](../Category/美国嘻哈歌手.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:瑞士裔美國人](../Category/瑞士裔美國人.md "wikilink")
[Category:波蘭裔美國人](../Category/波蘭裔美國人.md "wikilink")
[Category:盧森堡裔美國人](../Category/盧森堡裔美國人.md "wikilink")
[Category:蘇格蘭裔美國人](../Category/蘇格蘭裔美國人.md "wikilink")
[Category:英格蘭裔美國人](../Category/英格蘭裔美國人.md "wikilink")
[Category:美国自传作家](../Category/美国自传作家.md "wikilink")
[Category:美國電影監製](../Category/美國電影監製.md "wikilink")
[Category:美國慈善家](../Category/美國慈善家.md "wikilink")
[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:21世紀美國男演員](../Category/21世紀美國男演員.md "wikilink")
[Category:Mnet亞洲音樂大獎獲得者](../Category/Mnet亞洲音樂大獎獲得者.md "wikilink")
[Category:日本富士搖滾音樂祭參加歌手](../Category/日本富士搖滾音樂祭參加歌手.md "wikilink")

1.

2.  www.mtv.com/news/articles/1627833/20091208/eminem.jhtml

3.

4.  [MTC.co.uk](http://ema.mtv.co.uk/artists/eminem)

5.

6.