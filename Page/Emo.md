**Emo**（有譯作**情緒搖滾**或**情感硬核**）是-{zh-hans:[硬核朋克](../Page/硬核朋克.md "wikilink");zh-hant:[硬蕊龐克](../Page/硬蕊龐克.md "wikilink");}-樂的一個子類別。1980年代中期在[華盛頓特區舉行的](../Page/華盛頓特區.md "wikilink")[硬核龐克運動中](../Page/硬核龐克.md "wikilink")，它成為[後硬核的風格](../Page/後硬核.md "wikilink")，在當時被稱為情感硬核或emocore，由[Rites
of
Spring和](../Page/Rites_of_Spring.md "wikilink")[Embrace等樂團所開創](../Page/Embrace.md "wikilink")。1990年代初期emo經由[另類搖滾和](../Page/另類搖滾.md "wikilink")[獨立搖滾及](../Page/獨立搖滾.md "wikilink")[流行龐克改造](../Page/流行龐克.md "wikilink")，像是Jawbreaker樂團、Sunny
Day Real Estate樂團、Weezer樂團和Jimmy Eat World樂團。1990年代中期Braid樂團、Promise
Ring樂團和Get Up Kids樂團等在美國中西部爆發式([Midwest
emo](../Page/:en:Midwest_emo.md "wikilink"))場景中脫穎而出，許多獨立唱片公司開始專注發掘此種樂風。同時期更具侵略性的尖叫聲([screamo](../Page/:en:screamo.md "wikilink"))風格出現了，由[加州聖地牙哥的Heroin和Antioch](../Page/加州.md "wikilink")
Arrow等樂團率先推出。

## 起源

Emo音乐逐渐成形于那些活跃于1980年代华盛顿音乐风潮中的那些乐队。后来的几年，“Emocore”一词被当作“Emotional
Hardcore”（情感化硬核）的缩写。同样地，这一词也用来形容华盛顿音乐风潮和由些引发的其他地区的音乐潮流。Emo这一词产生于这样一个既成的事实：即[乐队的成员有时会在表演中自然而然地出现情绪化的表现](../Page/乐队.md "wikilink")（如情绪激动、潸然泪下等表现）。这一时期中最有名的艺人/乐队有[Rites
of
Spring](../Page/Rites_of_Spring.md "wikilink")、[Embrace](../Page/Embrace.md "wikilink")、[One
Last
Wish](../Page/One_Last_Wish.md "wikilink")、[Beefeater](../Page/Beefeater.md "wikilink")、[Gray
Matter](../Page/Gray_Matter.md "wikilink")、[Fire
Party和稍后的](../Page/Fire_Party.md "wikilink")[Moss
Icon](../Page/Moss_Icon.md "wikilink")。但是在进入1990年代后，伴随着大量乐队的出现，Emo的第一次浪潮逐渐退去。

1990年代中期后，Emo一词开始反映出[独立乐界的发展情况](../Page/獨立音樂.md "wikilink")，[Fugazi这支从第一次浪潮中分流出来的乐队对于随后Emo音乐浪潮的发展和影响绝对不可小视](../Page/Fugazi.md "wikilink")。而包括[Sunny
Day Real Estate和](../Page/Sunny_Day_Real_Estate.md "wikilink")[Texas Is
the
Reason在内的一些乐队则将此种音乐向独立音乐的方向上推的更远](../Page/Texas_Is_the_Reason.md "wikilink")，即更加的旋律化并比前辈更加的规整。被称之为“[独立情感](../Page/独立情感.md "wikilink")”的音乐风潮一直存在到1990年代的末期，随后许多乐队要么是解散，要么是将他们的音乐风格改成了[主流](../Page/主流.md "wikilink")。

对于那些进入主流圈子的独立情感乐队，新一些的乐队开始了模仿主流风格的道路，从而创造出一种从属于[大众文化的](../Page/大众文化.md "wikilink")“Emo”。然后即使是在过去，Emo一词也被用来指代很多音乐风格的乐队；今日被归于情绪核的乐队的范围刚更为宽广，从而使得“Emo”一词变为一种很宽松的音乐风格名词，而非用来特指某种音乐的类型。

## 历史

伴随着[硬核音乐的突飞猛进](../Page/硬核.md "wikilink")，Emo——Emo这种音乐风格随之产生。Emo在1990年代的晚些时候成为地下摇滚的重要的生力军，并一直持续影响到今日的朋克和独立摇滚的风格。有些emo更前卫些，充斥着大量复杂的吉他编配、比较古怪的歌曲创作结构、艺术化的噪音和动态极大的变化效果。有些emo更接近于[朋克-流行风格](../Page/朋克-流行.md "wikilink")，虽然要更复杂些。Emo的词作个人化色彩极强，通常要么是自由化的诗性化写作，要么就是非常个人化的忏悔。虽然比起硬核音乐来Emo远不是那么男性化，emo却是诚实与[反商业的硬核音乐的直系后裔](../Page/反商业.md "wikilink")。由于认识到做作而虚伪的商业化音乐根本不能表达真实的情感，Emo应运而生。因为emo追求超越理性的真实而深刻的情感发泄，这种风格以热衷于冲击情感表达的极限见长。

给Emo铺路的是[Hüsker Dü在](../Page/Hüsker_Dü.md "wikilink")1984年发行的里程碑式专辑[Zen
Arcade](../Page/Zen_Arcade.md "wikilink")，它帮助了其他硬核乐队如何能抓住更为个人化的主题，并写出更动听更具技术性的歌曲。Emo在制造出了诸如[Minor
Threat与](../Page/Minor_Threat.md "wikilink")[Bad
Brains这样的乐队的](../Page/Bad_Brains.md "wikilink")**华盛顿硬核音乐圈**中暂露头脚。“Emo”一词最早用来形容那些热衷于使用比咆哮和嘶吼这种唱腔更具表现力、感染力的演唱方式的硬核乐队，首支真正的Emo乐队是[Rites
of Spring](../Page/Rites_of_Spring.md "wikilink")，随后是前Minor
Threat乐队的主唱[Ian
MacKaye组建的短命的乐队](../Page/Ian_MacKaye.md "wikilink")[Embrace](../Page/Embrace.md "wikilink")。MacKaye创建的[Dischord唱片厂牌成为了华盛顿特区Emo音乐成长的支柱](../Page/Dischord唱片.md "wikilink")，发行过的作品包括了Rites
of Srping, [Dag Nasty](../Page/Dag_Nasty.md "wikilink"), [Nation of
Ulysses及MacKaye与Rites](../Page/Nation_of_Ulysses.md "wikilink") of
Spring成员合作的乐队——Fugazi。Fugazi乐队可以说是定义了早期的Emo风格，他们不但招揽了[另类摇滚音乐的听众](../Page/另类摇滚.md "wikilink")，而且因为反商业的绝不妥协的态度而得到媒体的报道。

除了Dischord唱片的中坚地位，多数早期的Emo都很地下。那些流星般出现的乐队在小厂牌下只是录制发行了少量的黑胶唱片；有些演唱者在舞台上演唱到歌曲的高潮部分会在情到深处时潸然泪下，这引起了纯硬核音乐分子的嘲笑。尽管有Fugazi的出现，但是Emo音乐真正的出头之日还是在1990年代中叶时出现的乐队Sunny
Day Real
Estate（SDRE），他们早期的作品在很多人看来是定义了后来的Emo的风格。他们将Fugazi粗糙的吉他演奏精练、加上西雅图的垃圾乐之声、配上直接的前进摇滚和低吟的人声，使得后来出现成千上万的模仿者，学习他们表现力强的旋律和自省的神秘主义式创作。

## 今日的Emo

而新生代的一些乐队，则将Emo中典型的扭曲古怪的内省式创作与朗朗上口的[流行龐克合而为一](../Page/流行龐克.md "wikilink")，代表作如[Weezer乐队的专辑](../Page/Weezer.md "wikilink")《[Pinkerton](../Page/Pinkerton.md "wikilink")》。当一些艺人继续着在Fugazi的道路上继续前行时（包括[Quicksand和](../Page/Quicksand.md "wikilink")[Drive
Like
Jehu乐队](../Page/Drive_Like_Jehu.md "wikilink")），多数1990年代的Emo乐队已经借鉴并综合了Fugazi、Sunny
Day Real Estate和Weezer的东西。乐队如[Promise
Ring](../Page/Promise_Ring.md "wikilink")、[the Get Up
Kids](../Page/the_Get_Up_Kids.md "wikilink")、[Braid](../Page/Braid.md "wikilink")、[Texas
Is the Reason](../Page/Texas_Is_the_Reason.md "wikilink")、[Jimmy Eat
World](../Page/Jimmy_Eat_World.md "wikilink")、[Joan of
Arc和](../Page/Joan_of_Arc.md "wikilink")[Jets to
Brazil引发了独立摇滚界的一轮跟风](../Page/Jets_to_Brazil.md "wikilink")，并使得Emo这种音乐风格在[千禧年之际成为地下摇滚风格中更为大众化](../Page/千禧年.md "wikilink")、流行化的一种风格。

## 外部連結

  - [EmoLife.ru - emo in russia](http://emolife.ru/)

[Category:摇滚乐類型](../Category/摇滚乐類型.md "wikilink")
[Category:硬蕊龐克](../Category/硬蕊龐克.md "wikilink")