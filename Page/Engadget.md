**Engadget**是一個關於消費電子產品的流行科技[網誌與](../Page/網誌.md "wikilink")[播客](../Page/播客.md "wikilink")。該網誌曾贏得数個獎項。現時Engadget擁有九個不同網站，全都以各自的員工同時地運作，以各自的語言覆蓋全球不同地方的科技新聞。美國的Engadget網站於2006年8月25日發出第
[20,000個新聞](http://www.engadget.com/2006/08/25/20-000-posts-yup-20-000-posts/)。Engadget曾以「癮科技」為中文譯名，但2011年6月28日起，[癮科技已為另一網站](../Page/癮科技.md "wikilink")。

## 創立與成員

Engadget由前[Gizmodo科技網誌編輯](../Page/Gizmodo.md "wikilink")[Peter
Rojas所創立](../Page/Peter_Rojas.md "wikilink")。Engadget是[Weblogs,
Inc.成員之一](../Page/Weblogs,_Inc..md "wikilink")，一個擁有超過75個[網誌的](../Page/網誌.md "wikilink")[網誌網絡](../Page/網誌網絡.md "wikilink")，其中包括了[Autoblog與](../Page/Autoblog.md "wikilink")[Joystiq](../Page/Joystiq.md "wikilink")，之前亦包括了[Hack-A-Day](../Page/Hack-A-Day.md "wikilink")。2005年[Weblogs
Inc.被](../Page/Weblogs_Inc..md "wikilink")[AOL收購](../Page/AOL.md "wikilink")。Engadget的總編輯是[Ryan
Block](../Page/Ryan_Block.md "wikilink")。

## 網誌

自2004年3月開始，Engadget每天更新數次[電子玩意與](../Page/電子玩意.md "wikilink")[消費電子產品的文章](../Page/消費電子產品.md "wikilink")。它亦張貼關於電子世界的流行[小道消息](../Page/小道消息.md "wikilink")，加上每週一次的[播客](../Page/播客.md "wikilink")[Engadget
Podcast](../Page/Engadget_Podcast.md "wikilink")，以覆蓋一週以來的科技與電子玩意的新聞及故事。

由其創立開始很多作家曾為Engadget、Engadget Mobile與[Engadget
hd寫作或寫稿](../Page/Engadget_hd.md "wikilink")，包括高知名度的Blogger、分析家與專業新聞記者。那些作家包括了[Jason
Calacanis](../Page/Jason_Calacanis.md "wikilink")、[Paul
Boutin](../Page/Paul_Boutin.md "wikilink")、[Phillip
Torrone與](../Page/Phillip_Torrone.md "wikilink")[Susan
Mernit](../Page/Susan_Mernit.md "wikilink")。

Engadget曾被提名多個獎項，包括2004年的[Bloggie最佳科技網誌與](../Page/Bloggie.md "wikilink")2005年的Bloggie最佳電腦或科技與最佳團隊網誌；Engadget贏得了Weblog
Awards的2004年與2005年最佳科技網誌。

[Google的电子邮件服務](../Page/Google.md "wikilink")[Gmail與許多其他RSS閱讀器一樣](../Page/Gmail.md "wikilink")，將Engadget加到預設的[RSS饋送](../Page/RSS.md "wikilink")，最新的文章會顯示到所有用戶的電子郵箱頂部。

Engadget最开始时在其網站放置[woot.com的廣告](../Page/woot.com.md "wikilink")，為當時剛起步的小公司提供了一個龐大的客戶基礎。

## 語言

為了增加讀者人數該網誌數種語言擁有包括了西班牙語、日語與中文（繁體及簡體）、波蘭文\[1\]、德文和韓文\[2\]。內容主要為[翻譯英文主站的文章為主](../Page/翻譯.md "wikilink")。如該網誌不斷在世界各地增加名聲，語言的數量很有可能會增加。

### 中文（繁體）

Engadget在2005年7月1日與[FunMakr合作](../Page/FunMakr.md "wikilink")，開設Engadget繁體中文版。\[3\]
兩家網站拆夥後，现在原「瘾科技」繁体中文版已改名为 Engadget 中文版。

Engadget繁体版在中国大陆被[GFW封锁](../Page/GFW.md "wikilink")。

### 中文（简体）

Engadget简体版于2005年的10月1日开站\[4\]，同时期也开通了Autoblog中文版，繁体版 Engadget 前期主要写手是
Atticus Wu，目前 Engadget 成为中国最受欢迎的新数码媒体之一。现在原「瘾科技」简体中文版已改名为 Engadget
中国版。但在中國大陸民間沿用舊名來指代engadget的情況不算罕見。

### 繁體中文站拆夥事件

2011年6月28日，Engadget繁體中文站與[FunMakr無預警拆夥](../Page/FunMakr.md "wikilink")\[5\]，原癮科技繁體中文站更名為「Engadget中文版」，並由Engadget香港編輯接手。現在FunMakr與Engadget兩家網站已無任何關係，為獨立經營的兩個不同網站。

## 播客

Engadget的播客於2004年10月启动，由[Phillip
Torrone主持](../Page/Phillip_Torrone.md "wikilink")，一個[Makezine的作家與網誌](../Page/Makezine.md "wikilink")[Make的資深編輯](../Page/Make.md "wikilink")。他主持了22集播客直至[Eric
Rice接替](../Page/Eric_Rice.md "wikilink")。[Eric
Rice因其自己名為](../Page/Eric_Rice.md "wikilink")[The Eric Rice
Show的播客而聞名](../Page/The_Eric_Rice_Show.md "wikilink")，同時他亦為[Weblogs
Inc.製作播客](../Page/Weblogs_Inc..md "wikilink")。Eric主持與製作4集播客直至該節目被[Peter
Rojas與](../Page/Peter_Rojas.md "wikilink")[Ryan
Block接管](../Page/Ryan_Block.md "wikilink")。在兩人主持與製作(38集)約二十集Engadget雇用了播客製作人[Randall
Bennett](../Page/Randall_Bennett.md "wikilink")，成為Engadget與Weblogs,
Inc.的重要媒體製作人直至2006年5月。

該播客的討論話題為科技相關與該週科技世界所發生的事件。該節目一般每週一次，每次半到一小時，不過偶爾會被一些事件中斷，如每年一次的[國際消費電子展](../Page/國際消費電子展.md "wikilink")(CES)與[電子娛樂展覽](../Page/E3.md "wikilink")(E3)，於事件發生時播客會每天廣播以覆蓋電子玩意的最新消息。最近的特別版播客為Engadget的lovecast與一個聽眾的語音信箱播客。

最近該播客受到贊助。作為對贊助商的回報會在節目中被提及。播客主持會放節目初、中段與結尾時提及贊助商。贊助商包括[Best
Buy](../Page/Best_Buy.md "wikilink")、[尼康與最近的](../Page/尼康.md "wikilink")[嘉實多](../Page/嘉實多.md "wikilink")。

Engadget播客可以[RSS饋送通過](../Page/RSS.md "wikilink")[iTunes訂閱或可於該網站上選擇下載](../Page/iTunes.md "wikilink")[MP3](../Page/MP3.md "wikilink")、[Ogg](../Page/Ogg.md "wikilink")、[AAC或](../Page/AAC.md "wikilink")[m4b格式](../Page/m4b.md "wikilink")。m4b版本提供有關當前話題的圖像，可放[iTunes或相容的播放器上顯示](../Page/iTunes.md "wikilink")。

## 被盜用商標

2006年前期Engadget報導他們的名稱被馬來西亞吉隆坡一個購物中心中一間與其名字相似的店鋪所盗用而成為受害者\[6\]。

## 讀者集會

Engadget於不同城市主持讀者集會，參加為完全免費而且會分發獎品。曾舉辦的地點包括[紐約](../Page/紐約.md "wikilink")、[冰島](../Page/冰島.md "wikilink")、[華盛頓](../Page/華盛頓.md "wikilink")、[三藩市](../Page/三藩市.md "wikilink")、[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")、[西雅圖與](../Page/西雅圖.md "wikilink")[波士頓](../Page/波士頓.md "wikilink")。

## 編輯群

### 現時的資深編輯

  - [Peter Rojas](../Page/Peter_Rojas.md "wikilink")
  - [Ryan Block](../Page/Ryan_Block.md "wikilink")
  - [Cyrus Farivar](../Page/Cyrus_Farivar.md "wikilink")
  - [Evan Blass](../Page/Evan_Blass.md "wikilink")

### 投稿編輯

  - [Josh Fruhlinger](../Page/Josh_Fruhlinger.md "wikilink")
  - [Donald Melanson](../Page/Donald_Melanson.md "wikilink")
  - [Paul Miller](../Page/Paul_Miller.md "wikilink")
  - [Darren Murph](../Page/Darren_Murph.md "wikilink")
  - [Thomas Ricker](../Page/Thomas_Ricker.md "wikilink")
  - [Chris Ziegler](../Page/Chris_Ziegler.md "wikilink")
  - [Ross Rubin](../Page/Ross_Rubin.md "wikilink")
  - [Stan Horaczek](../Page/Stan_Horaczek.md "wikilink")
  - [Matt Burns](../Page/Matt_Burns.md "wikilink")
  - [Conrad Quilty-Harper](../Page/Conrad_Quilty-Harper.md "wikilink")
  - [Benjamin Heckendorn](../Page/Benjamin_Heckendorn.md "wikilink")

### 前編輯

  - [Jason Calacanis](../Page/Jason_Calacanis.md "wikilink")
  - [Marc Perton](../Page/Marc_Perton.md "wikilink")
  - [Phillip Torrone](../Page/Phillip_Torrone.md "wikilink")

## 評論

### 侵犯版权

2006年3月一個關於[數位音頻播放器的網站](../Page/數位音頻播放器.md "wikilink")[DAPreview聲稱Engadget使用了一張原為DAPreview所拍攝的照片並裁减掉了DAPreview的標誌來占为己用](../Page/DAPreview.md "wikilink")\[7\]。Engadget的總編輯Ryan
Block承認該照片确实曾遭複製與修剪，为此他做出了道歉并移除了这张图片\[8\]。

## 参考文献

## 外部連結

  - 官方網站

<!-- end list -->

  - [Engadget](http://www.engadget.com/)
  - [Engadget
    Podcast](https://web.archive.org/web/20061111090729/http://podcasts.engadget.com/)
  - [Engadget 中文版](http://chinese.engadget.com/)
  - [Engadget 中國版](http://cn.engadget.com/)

<!-- end list -->

  - 相關或類似網站

<!-- end list -->

  - [Gizmodo](http://www.gizmodo.com/)
  - [Consumer Electronics Daily
    News](https://web.archive.org/web/20070208232955/http://www.cedailynews.com/)

{{-}}

[Category:美國線上公司](../Category/美國線上公司.md "wikilink")
[Category:2004年建立的网站](../Category/2004年建立的网站.md "wikilink")
[E](../Category/網誌.md "wikilink")

1.  [癮科技波蘭站成立](http://chinese.engadget.com/2008/06/03/introducing-engadget-poland/)
2.  [Engadget韓文版與德文版登場了](http://chinese.engadget.com/2008/07/04/introducing-engadget-korean-and-german/)
3.
4.  <http://cn.engadget.com/2005/10/01/engadget-cn/>
5.  [【新聞稿】 - 終止協助經營Engadget中文版網站
    癮科技媒體平台正式成立](http://bbs.cool3c.com/article/47984)

6.  [A visit to the Engadget
    Store](http://www.engadget.com/2006/07/10/a-visit-to-the-engadget-store/)
7.
8.  <http://www.ryanablock.com/archive/2006/03/controversy/>