**Engrish**，源自[日語](../Page/日語.md "wikilink")[转写系统沒有](../Page/转写.md "wikilink")「L」，常用「R」（[IPA](../Page/IPA.md "wikilink"):
）音译英语的「L」，且日本人受[母語影響容易混淆不分](../Page/母語.md "wikilink")\[1\]，也令[英語母語使用者費解](../Page/英語.md "wikilink")，而把「Eng**l**ish」改成「Eng**r**ish」來諷刺。原指**日式英語**，後泛指[亞洲語系錯誤的](../Page/亞洲.md "wikilink")[英語文法](../Page/英語文法.md "wikilink")。

## 基本例子

[Sign_in_a_toilet_in_Shanghai,_2005.jpg](https://zh.wikipedia.org/wiki/File:Sign_in_a_toilet_in_Shanghai,_2005.jpg "fig:Sign_in_a_toilet_in_Shanghai,_2005.jpg")

  - 以母語的理解將句子直接翻譯。
  - [機器翻譯](../Page/機器翻譯.md "wikilink")\[2\]\[3\]。
  - 混合語，[馬來西亞和](../Page/馬來西亞.md "wikilink")[新加坡的](../Page/新加坡.md "wikilink")[英語將](../Page/新加坡英語.md "wikilink")[福建話](../Page/福建話.md "wikilink")（[閩南語](../Page/閩南語.md "wikilink")）及[馬來語在英語中混合使用](../Page/馬來語.md "wikilink")。
  - 相近的發音：例如「**3Q**」在中國大陸和[台灣經常被使用](../Page/台灣.md "wikilink")。
  - 錯誤的翻譯：普遍在各種媒介出現。

## 有名句子

  - [All your base are belong to
    us](../Page/All_your_base_are_belong_to_us.md "wikilink")

<!-- end list -->

  -
    日本Toaplan公司開發的遊戲《[Zero
    Wing](../Page/Zero_Wing.md "wikilink")》，他們在歐洲移植到[Mega
    Drive版本的時候](../Page/Mega_Drive.md "wikilink")，其中一句對話出現了這個句子，因為base除了解作基地，還可以解作根源，使句子誤譯為「你們所有的根源是我們」。自2000年在互聯網廣泛流傳後，在歐美地區成為了流行用語。

## 参考资料

## 參閱

  - [和製英語](../Page/和製英語.md "wikilink")
  - [香港英語](../Page/香港英語.md "wikilink")
  - [中式英語](../Page/中式英語.md "wikilink")
  - [新加坡英语](../Page/新加坡英语.md "wikilink")

## 外部連結

  - [Engrish.com](https://web.archive.org/web/20131104133457/http://engrish.com/)

[Category:英语](../Category/英语.md "wikilink")

1.
2.  <http://evchk.wikia.com/wiki/Translate_server_error>
3.  <http://internet.solidot.org/article.pl?sid=08/08/04/002223>