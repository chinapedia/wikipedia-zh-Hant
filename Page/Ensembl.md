**Ensembl**是一项[生物信息学研究计划](../Page/生物信息学.md "wikilink")，旨在开发一种能够对[真核生物基因组进行自动诠释](../Page/真核生物.md "wikilink")（automatic
annotation）并加以维护的软件。该计划由英国及[欧洲分子生物学实验室所属分部](../Page/欧洲分子生物学实验室.md "wikilink")[欧洲生物信息研究所共同协作运营](../Page/欧洲生物信息研究所.md "wikilink")，这是为了回应[人类基因组计划即将完而于](../Page/人类基因组计划.md "wikilink")1999年启动的\[1\]
。在存在10年之后\[2\]，Ensembl的目标仍然是为遗传学家，分子生物学家和其他研究人员研究我们自己的物种和其他[脊椎动物和](../Page/脊椎动物.md "wikilink")[模式生物的](../Page/模式生物.md "wikilink")[基因组而提供集中的资源](../Page/基因组.md "wikilink")。Ensembl是几个知名的之一，用于检索[基因组学信息](../Page/基因组学.md "wikilink")。

相似的数据库和浏览器还被发现在美国[国家生物技术信息中心](../Page/国家生物技术信息中心.md "wikilink")（National
Center for Biotechnology
Information，简称NCBI）和[加州大学圣克鲁兹分校的](../Page/加州大学圣克鲁兹分校.md "wikilink")。

## 软件及数据

该计划开放所有源信息，所有由该计划所产生的数据及软件都可以免费及自由地从网络上获取并使用。

该计划所开发并使用的大部分软件是用[Perl语言编写的](../Page/Perl.md "wikilink")，并基于[BiopPerl的基础框架](../Page/BiopPerl.md "wikilink")。其他基因组计划亦可轻易使用Perl语言的[应用程序接口](../Page/应用程序接口.md "wikilink")（Application
programming interface，API）。比方说，可以使用它对基因或者克隆目录进行诠释。

## 目前已有物种

## 软件使用

## 参阅

  - [国家生物技术信息中心](../Page/国家生物技术信息中心.md "wikilink")（NCBI）

  -
## 参考资料

## 外部链接

  -
  - [Vega](http://vega.sanger.ac.uk)

  - [Pre-Ensembl](http://pre.ensembl.org)

  - [Ensembl genomes](http://www.ensemblgenomes.org)

  - [UCSC Genome Browser](http://genome.ucsc.edu)

  - [NCBI](http://www.ncbi.nlm.nih.gov/)

  - [Ensembl: Browsing chordate genomes on EBI Train
    OnLine](http://www.ebi.ac.uk/training/online/course/ensembl-browsing-chordate-genomes)

[Category:生物信息學](../Category/生物信息學.md "wikilink")

1.
2.