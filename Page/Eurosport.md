**Eurosport**是歐洲最大型的[衛星和](../Page/衛星.md "wikilink")[有線電視體育頻道](../Page/有線電視.md "wikilink")，由[探索通信擁有](../Page/探索通信.md "wikilink")，能在54個地區收看和發送出20個不同的語言。曾經是[法國廣播業鉅子](../Page/法國.md "wikilink")[TF1集团旗下頻道](../Page/法國電視一台.md "wikilink")。2012年12月，探索通信入股20%，並於幾年後最終向TF1買下Eurosport
100%股份。

## 節目內容

Eurosport會給觀眾播放賽事包括[歐洲聯賽冠軍盃](../Page/歐洲聯賽冠軍盃.md "wikilink")、[歐洲超級盃](../Page/歐洲超級盃.md "wikilink")、[歐洲足協盃](../Page/歐洲足協盃.md "wikilink")、[達卡拉力賽](../Page/達卡拉力賽.md "wikilink")、[蒙特卡洛拉力赛](../Page/蒙特卡洛拉力赛.md "wikilink")、[奧運會](../Page/奧運會.md "wikilink")、單車、網球、[澳洲足球聯賽](../Page/澳洲足球聯賽.md "wikilink")、冬季賽事、[滑冰及](../Page/滑冰.md "wikilink")[衝浪](../Page/衝浪.md "wikilink")。

## 歷史

[Eurosport_logo.svg](https://zh.wikipedia.org/wiki/File:Eurosport_logo.svg "fig:Eurosport_logo.svg")
Eurosport創立於1989年2月5日，是[歐洲廣播聯盟和](../Page/歐洲廣播聯盟.md "wikilink")[天空電視台的合資企業](../Page/天空電視台.md "wikilink")。由於競爭者[Screensport對其企業結構向](../Page/Screensport.md "wikilink")[歐盟委員會提出控訴](../Page/歐盟委員會.md "wikilink")，Eurosport
於1991年5月被關閉。然而[TF1集團臨危受命](../Page/TF1集團.md "wikilink")，取代英國天空廣播公司的角色，使得頻道被保留下來。一個新的
Eurosport 頻道於同月開播。

1993年3月1日，有線和衛星頻道業者 Screensport 併購 Eurosport。Eurosport
後來由一個法國經營聯盟所掌控，其團隊成員包括TF1集團、[Canal+集團和Havas影像等](../Page/Canal+集團.md "wikilink")。自2001年1月起，TF1
已完全擁有經營權。

## 合作伙伴

在歐洲，Eurosport 基本上扮演數位電視服務供應商的角色。自1999年以來，Eurosport
提供多種選擇的體育內容服務，包括不同語言、廣告和評論等選項。Eurosport
通常是一個獨立的頻道，提供標準化的內容版本（以英語播放的Eurosport 國際版）。

除此之外，也有一些地區性的 Eurosport
頻道，包括[法國](../Page/法國.md "wikilink")、[英國](../Page/英國.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[德國](../Page/德國.md "wikilink")、[波蘭](../Page/波蘭.md "wikilink")、[北歐地區和](../Page/北歐地區.md "wikilink")[亞太地區](../Page/亞太地區.md "wikilink")。這些頻道提供了更廣泛的體育內容，包括當地的體育賽事，有時也會利用其他的泛歐洲影片來源。

在亞太地區，Eurosport
提供了一個特別頻道，中文名稱為「[歐洲體育頻道](../Page/歐洲體育頻道.md "wikilink")」，2009年11月15日開播。在[澳大利亞](../Page/澳大利亞.md "wikilink")，[Foxtel](../Page/Foxtel.md "wikilink")、[Austar和](../Page/Austar.md "wikilink")[Optus的訂戶可以直接收看](../Page/Optus.md "wikilink")。

## 其他頻道

  - [Eurosport HD](../Page/Eurosport_HD.md "wikilink")
  - [Eurosport 3D](../Page/Eurosport_3D.md "wikilink")
  - [Eurosport 2](../Page/Eurosport_2.md "wikilink")
  - [Eurosport News](../Page/Eurosport_News.md "wikilink")
  - [British Eurosport](../Page/British_Eurosport.md "wikilink")

## 參見

  - [Now寬頻電視體育平台](../Page/Now寬頻電視體育平台.md "wikilink")
  - [歐洲體育頻道](../Page/歐洲體育頻道.md "wikilink")

## 外部連結

  - [官方網站](http://www.eurosport.com/)
  - [Eurosport節目表](https://web.archive.org/web/20070623022043/http://www.mindthezap.tv/channels/274-today.html)
  - [Eurosport 2節目表](https://web.archive.org/web/20070821094140/http://www.mindthezap.tv/channels/1076-today.html)

[Eurosport](../Category/Eurosport.md "wikilink")
[Category:多語廣播](../Category/多語廣播.md "wikilink")