**Evergreen**是一個[開放原始碼](../Page/開放原始碼.md "wikilink")（[GNU通用公共許可證](../Page/GNU通用公共許可證.md "wikilink")）的[圖書館自動化](../Page/圖書館自動化.md "wikilink")（ILS），讀者可以[萬維網實現圖書的查找和借閱](../Page/萬維網.md "wikilink")，許多北美的[圖書館都使用它](../Page/圖書館.md "wikilink")。\[1\]
\[2\]

## 功能

  - [OPAC](../Page/OPAC.md "wikilink")
  - 圖書館目錄
  - 借閱服務
  - 統計報告

## 系統需求

### 伺服器端

  - [Linux操作系統](../Page/Linux.md "wikilink")
  - [PostgreSQL](../Page/PostgreSQL.md "wikilink")[數據庫管理系統](../Page/數據庫管理系統.md "wikilink")
  - [Apache](../Page/Apache.md "wikilink") HTTP
    Server[網頁伺服器](../Page/網頁伺服器.md "wikilink")

### 客戶端

  - [網頁瀏覽器](../Page/網頁瀏覽器.md "wikilink")

## 參考文獻

## 外部链接

  - [Evergreen開放原始碼計劃主頁](http://www.open-ils.org/)
  - [Evergreen在線示範](https://web.archive.org/web/20080515170744/http://demo.gapines.org/)

[Category:圖書館](../Category/圖書館.md "wikilink")
[Category:圖書資訊科學](../Category/圖書資訊科學.md "wikilink")
[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:免費軟件](../Category/免費軟件.md "wikilink")
[Category:用Perl编程的自由软件](../Category/用Perl编程的自由软件.md "wikilink")

1.  [Evergreen: Your Homegrown
    ILS](http://www.libraryjournal.com/article/CA6396354.html)
2.