[PCCard-ExpressCard.png](https://zh.wikipedia.org/wiki/File:PCCard-ExpressCard.png "fig:PCCard-ExpressCard.png")、[PC
Card的比較圖](../Page/PC卡.md "wikilink")\]\]

**ExpressCard**是一種用來取代[Cardbus的](../Page/Cardbus.md "wikilink")[硬體標準](../Page/硬體.md "wikilink")，是一種信息裝置用的擴充介面，它與CardBus一樣都是由[PCMCIA機構所發展](../Page/PCMCIA.md "wikilink")，ExpressCard介面的骨幹部分可用[USB](../Page/USB.md "wikilink")
2.0或[PCI
Express等介面與](../Page/PCI_Express.md "wikilink")（資訊）裝置本體相連，至於要使用USB抑或PCI
Express介面來相連則由硬體設計師依據應用的需求與場合來決定，同時ExpressCard也與過往的[CardBus](../Page/CardBus.md "wikilink")、[PC
Card相同](../Page/PC卡.md "wikilink")，皆具有[熱插拔的機制功效](../Page/熱插拔.md "wikilink")。

## 構型、外觀

ExpressCard在設計時有兩種外型選擇，分別是ExpressCard/34（34mm的卡寬）與ExpressCard/54（54mm的卡寬，卡形為L狀），雖然外型有兩種，但介面連接（連接器）的部分依舊是一種，即是34mm寬的接槽介面。

## 與CardBus的比較

[Pccards.png](https://zh.wikipedia.org/wiki/File:Pccards.png "fig:Pccards.png")
ExpressCard在諸多特點功效上都超越之前的CardBus，不過最主要也最顯著的超越效益是在傳輸頻寬方面，ExpressCard的最大傳輸率可達500MB/Sec，相對的CardBus則為132MB/Sec，如此ExpressCard在傳輸速度上是CardBus的3.7倍。

ExpressCard之所以能有如此高幅度的傳輸率提升，其實得歸功於[PCI
Express](../Page/PCI_Express.md "wikilink") x1（Lane，傳輸巷）或USB 2.0和USB
3.0等高速介面，ExpressCard原生性地採用此兩種介面（主要是PCI
Express），相對的CardBus採行的是標準[PCI介面](../Page/PCI.md "wikilink")，而往未來看電腦資訊系統將逐漸採行PCI
Express與[USB 2.0和](../Page/USB_2.0.md "wikilink")[USB
3.0介面並逐漸棄捨PCI介面](../Page/USB_3.0.md "wikilink")。

此外，ExpressCard也比CardBus更講究節能省電，CardBus所用的運作電壓（或稱：工作電壓）範疇為3.3V—5.0V，相對的ExpressCard則是1.5V—3.3V，一般而言愈低的運作電壓愈能夠減少功率消耗（簡稱：功耗）。

## 首批採用

以下是較先採行同時也較具代表性的ExpressCard採行業者及其產品：

  - [索尼公司](../Page/索尼公司.md "wikilink")2006年推出的商务旗舰笔记本sz系列同时搭载了pc
    card和express card接口

<!-- end list -->

  - [惠普公司於](../Page/惠普公司.md "wikilink")2004年11月推出的新款[笔记本电脑](../Page/筆記型電腦.md "wikilink")：HP
    Pavilion
    zd8000。[報導](http://www.pcmag.com/article2/0,1759,1706542,00.asp)

<!-- end list -->

  - [聯想集團於](../Page/聯想集團.md "wikilink")2005年5月發表的旗艦級筆記型電腦：[ThinkPad](../Page/ThinkPad.md "wikilink")。

<!-- end list -->

  - [戴爾電腦在Precision](../Page/戴尔.md "wikilink")、Inspiron、Latitude等筆記型電腦產品線的新款機種。

<!-- end list -->

  - [蘋果電腦於](../Page/蘋果電腦.md "wikilink")2006年1月推出的[MacBook
    Pro具備一個ExpressCard](../Page/MacBook_Pro.md "wikilink")/34介面槽。

<!-- end list -->

  - [華碩電腦發表的V](../Page/華碩電腦.md "wikilink")6J型筆記型電腦也改以ExpressCard取代CardBus/PC
    Card。

<!-- end list -->

  - [富士通-西門子電腦於](../Page/富士通-西門子電腦公司.md "wikilink")2006年6月推出的[AMILO
    Pi 1536/7型](http://www.fujitsu-siemens.com/home/products/notebooks/amilo_pi_1536.html)具備一個ExpressCard/54接槽。

從2005年3月[德國](../Page/德國.md "wikilink")[漢諾威的](../Page/漢諾威.md "wikilink")[CeBIT國際電腦貿易展開始](../Page/CeBIT.md "wikilink")，有更多的產品裝置應用與採行ExpressCard技術及介面。

## 外部連結

  - [ExpressCard官方網站](https://web.archive.org/web/20080828023633/http://www.expresscard.org/)
      - [ExpressCard的新聞剪報](https://web.archive.org/web/20050329110333/http://www.expresscard.org/web/site/news.jsp)
  - [PCMCIA機構預測：2007年下半年開始ExpressCard將會廣泛採用](http://www.digitimes.com/news/a20051125PR201.html)

[Category:擴張界面卡](../Category/擴張界面卡.md "wikilink")
[Category:主板](../Category/主板.md "wikilink")
[Category:计算机总线](../Category/计算机总线.md "wikilink")