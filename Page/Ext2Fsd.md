**Ext2Fsd**是一個[開放源碼的驅動開發計畫](../Page/開放源碼.md "wikilink")，允許[WinNT](../Page/Windows_NT.md "wikilink")／[Win2K](../Page/Windows_2000.md "wikilink")／[WinXP等多個平台存取](../Page/Windows_XP.md "wikilink")[ext2](../Page/ext2.md "wikilink")/[ext3](../Page/ext3.md "wikilink")/[ext4的](../Page/ext4.md "wikilink")[檔案系統](../Page/檔案系統.md "wikilink")。ext2主要用於以[Linux為主的](../Page/Linux.md "wikilink")[作業系統](../Page/作業系統.md "wikilink")，而為Windows平台提供對ext2的支援，有助平台間的檔案交流。最新版本是0.6.9。

## 特色

最新版本已支援：

  - ext3
  - ext4
  - htree index
  - uninit_bg

## 參考資料

## 外部連結

  - [計畫網頁](http://ext2fsd.com)

  -
[Category:SourceForge專案](../Category/SourceForge專案.md "wikilink")