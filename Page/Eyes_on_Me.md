《**Eyes on
Me**》是[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")[王菲的第三張個人單曲與第一張英文](../Page/王菲.md "wikilink")[單曲](../Page/單曲.md "wikilink")，於1999年2月24日由[東芝EMI株式會社在](../Page/東芝EMI株式會社.md "wikilink")[日本發行](../Page/日本.md "wikilink")，共收錄3首歌。[日本環球音樂於](../Page/日本環球音樂.md "wikilink")2017年11月3日限量發行7吋黑膠唱片的版本\[1\]\[2\]。

## 簡介

收錄的新歌只有《Eyes on
Me》，該曲是日本[史克威尔發行的](../Page/史克威尔.md "wikilink")[PlayStation](../Page/PlayStation_\(遊戲機\).md "wikilink")[遊戲軟體](../Page/遊戲軟體.md "wikilink")《[最終幻想VIII](../Page/最終幻想VIII.md "wikilink")》的主題曲；另附《Eyes
on
Me》演奏曲版和王菲1998年10月專輯《[唱遊](../Page/唱遊.md "wikilink")》中的《紅豆》。截至2003年3月31日，收入《Eyes
on Me》單曲的《[最終幻想VIII](../Page/最終幻想VIII.md "wikilink")》在全球賣了815萬套\[3\]。

## 曲目

## 背景

在《[最终幻想VII](../Page/最终幻想VII.md "wikilink")》的開發過程當中，[史克威尔曾打算為遊戲創作主題曲](../Page/史克威尔.md "wikilink")，但後來計畫取消\[4\]。在完成《Eyes
on
Me》之後，開發團隊為選擇適合的聲音聽取了無數的CD，直到聽到了王菲的專輯。[植松伸夫表示](../Page/植松伸夫.md "wikilink")「王菲的聲音和感情恰好符合我對歌曲的意象，她的中國人身分也符合最終幻想的國際形象\[5\]。」但由於王菲的行程問題，開發團隊帶著[管弦樂團前往](../Page/管弦樂團.md "wikilink")[香港錄製歌曲](../Page/香港.md "wikilink")\[6\]\[7\]。

## 評價

《Eyes on
Me》發售3週，在日本的銷售賣破50萬份\[8\]，打入日本[公信榜的Top](../Page/公信榜.md "wikilink")
10，創造了中國歌手的新紀錄\[9\]，創下當時遊戲音樂最高銷量的記錄，後來才被[宇多田光為](../Page/宇多田光.md "wikilink")《[王國之心](../Page/王國之心.md "wikilink")》演唱的歌曲《[光](../Page/光_\(宇多田光單曲\).md "wikilink")》打破。此曲在公信榜「公信榜外语单曲排行榜」上連續19週第一\[10\]\[11\]，一共21週占據榜首，打破記錄\[12\]。

《Eyes on
Me》在第14屆[日本金唱片大獎](../Page/日本金唱片大獎.md "wikilink")（）上斬獲洋樂部門「年度歌曲」（）的最高榮譽\[13\]，王菲是第一個獲得此殊榮的亞洲歌手\[14\]，《Eyes
On Me》也是第一支獲得金唱片獎的遊戲歌曲\[15\]\[16\]。

## 備註

## 參考資料

[Category:1999年單曲](../Category/1999年單曲.md "wikilink")
[Category:王菲歌曲](../Category/王菲歌曲.md "wikilink")
[Category:EMI日本歌曲](../Category/EMI日本歌曲.md "wikilink")
[Category:最终幻想的音乐](../Category/最终幻想的音乐.md "wikilink")
[Category:遊戲主題曲](../Category/遊戲主題曲.md "wikilink")

1.

2.

3.

4.

5.

6.
7.  Maeda, Yoshitake (1999). Final Fantasy VIII Original Soundtrack
    (Limited Edition). [DigiCube](../Page/DigiCube.md "wikilink").

8.

9.  2001年2月16日《寓言》日文版中的王菲介紹頁：「」

10.
11.

12. 2001年2月16日《寓言》日文版中的王菲介紹頁：「」

13.

14. 2001年2月16日《寓言》日文版中的王菲介紹頁：「」

15.

16.