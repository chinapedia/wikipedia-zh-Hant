**埃尔齐·克赖斯勒·西格**（，），生於[伊利诺伊州切斯特](../Page/切斯特_\(伊利诺伊州\).md "wikilink")，[美国漫画家](../Page/美国.md "wikilink")，因创作了“[大力水手](../Page/大力水手.md "wikilink")”而闻名。

## 早期生活

西格出生和成長在伊利諾伊州切斯特，密西西比河附近的小鎮。西格是一名犹太人\[1\]\[2\]\[3\]\[4\]\[5\]\[6\]，勤雜工的兒子，他最早的工作經驗包括協助他的父親在房子畫和掛紙。擅長演奏鼓，他還提供音樂伴奏電影和雜耍的行為在當地的劇院，在那裡他最終獲得了電影放映工作。
18歲時，他決定成為一個漫畫家。他在輪候冊漫畫函授課程俄亥俄州克利夫蘭·埃文斯。\[2\]他說，下班後，他“點燃了大約午夜時分，油燈和在球場上一直工作到凌晨3點”

segar搬到了芝加哥，他在那裡會見理查德·F. Outcault，黃河的孩子的創造者和巴斯特·布朗。
outcault鼓勵他，並介紹了他在芝加哥先驅報“。
1916年3月12日，公佈的先驅Segar的第一本漫畫，查理·卓別林的喜劇雀躍，跑了一年多一點。在1918年，他移居到威廉·倫道夫·赫斯特的芝加哥晚報美國，在那裡他創造了循環的循環。
segar特爾·約翰遜結婚那年，他們有兩個孩子。

在长期患病后，西格尔于1938年10月死于[白血病和](../Page/白血病.md "wikilink")[肝病](../Page/肝病.md "wikilink")，享年43岁。\[7\]

## 参考资料

[S](../Category/美国漫畫家.md "wikilink")
[S](../Category/伊利諾州人.md "wikilink")

1.  JWR's Pundits World Editorial Cartoon Showcase Mallard Fillmore ...
    www.jewishworldreview.com/0104/popey_fight.asp
    <http://www.jewishworldreview.com> | (KRT) CHESTER, Ill. — This
    Mississippi River ... The creator of the comic strip, Elzie Segar,
    was born and raised in Chester, ...
2.  Masters of American Comics - Newark Museum - The Jewish Museum ...
    <https://www.nytimes.com/2006/10/13/arts/design/13comi.html> Oct 13,
    2006 - This means Elzie Crisler Segar's “Thimble Theater,” which
    introduced .... Mr. Kurtzman and Mr. Crumb are shortchanged at the
    Jewish Museum.
3.  The Jewish People and America in the 20th Century
    <https://webimages.uclaextension.edu/webimages/prod/FileRoot/Syllabus/208569.pdf>
    Discover the lessons it teaches us about the evolution of the Jewish
    people from hopeful .... Elzie Segar's Popeye and Olive Oyl and
    Bluto appeared in 1929.
4.  E.C. Segar's Popeye volume 4: Plunder Island – Now Read This\!
    <https://www.comicsreview.co.uk/>.../01/.../e-c-segar’s-popeye-volume-4-plunder-island...
    Jan 19, 2010 - In the less than ten years Elzie Crisler Segar worked
    with Popeye, (from .... George W. Geezil wasn't merely a cheap
    Jewish stock figure of fun, ...
5.  E.C. Segar's Popeye volume 3: “Let's You and Him Fight\!” – Now Read
    ...
    <https://www.comicsreview.co.uk/>.../e-c-segar’s-popeye-volume-3-“let’s-you-and-him-...
    Jan 19, 2010 - Elzie Crisler Segar had been producing the Thimble
    Theatre daily ... Geezil, an ethnic Jewish stereotype, who like all
    Segar's characters swiftly ...
6.  What's New With Jewish-American Superheroes? | The Jewish Press ...
    www.jewishpress.com › Sections Oct 4, 2006 - The Jewish Museum ...
    212-423-3200, www.thejewishmuseum.org ... Feininger, George
    Herriman, E.C. Segar, Frank King, Chester Gould, ...
7.