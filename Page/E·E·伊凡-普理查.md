**艾德華·伊凡·伊凡-普理查**爵士（，），[英國](../Page/英國.md "wikilink")[人類學家](../Page/人類學家.md "wikilink")，他結合[結構功能論以及自身在](../Page/結構功能論.md "wikilink")[東非的](../Page/東非.md "wikilink")[民族誌紀錄](../Page/民族誌.md "wikilink")，對[努爾人與](../Page/努爾人.md "wikilink")[亞桑地人的](../Page/亞桑地人.md "wikilink")[親屬制度與](../Page/親屬制度.md "wikilink")[巫術重新整理](../Page/巫術.md "wikilink")，進而重新開啟[西方文化重新詮釋](../Page/西方文化.md "wikilink")[原始社會](../Page/原始社會.md "wikilink")[心靈的理解](../Page/心靈.md "wikilink")

## 生平

艾德華·伊凡普理查生於1902年的[廓市](../Page/廓市.md "wikilink")（Crowborough）
，父親約翰·伊凡普理查（John
Evans-Pritchard）是[聖公會的](../Page/聖公會.md "wikilink")[牧師](../Page/牧師.md "wikilink")。伊凡普理查在家中排行老二，並以母親桃樂絲·艾德華（Dorothea·Edward）之姓為名。

1916年伊凡普理查進入[温切斯特公学接受中學教育](../Page/溫徹斯特公學.md "wikilink")，並於1921年就讀[牛津大學埃克塞特學院](../Page/牛津大學埃克塞特學院.md "wikilink")。

## 理論與貢獻

## 著作

  - 1937 *Witchcraft, Oracles and Magic Among the Azande*. Oxford
    University Press. 1976 abridged edition: ISBN 0-19-874029-8
  - 1940a *The Nuer: A Description of the Modes of Livelihood and
    Political Institutions of a Nilotic People*. Oxford: Clarendon
    Press.
  - 1940b "The Nuer of the Southern Sudan". in *African Political
    Systems*. M. Fortes and E.E. Evans-Prtitchard, eds., London: Oxford
    University Press., p. 272-296.
  - 1949 *The Sanusi of Cyrenaica*. London: Oxford: Oxford University
    Press.
  - 1951a *Kinship and Marriage Among the Nuer*. Oxford: Clarendon
    Press.
  - 1951b "Kinship and Local Community among the Nuer". in *African
    Systems of Kinship and Marriage*. A.R. Radcliffe-Brown and D.Forde,
    eds., London: Oxford University Press. p. 360-391.
  - 1956 *Nuer Religion*. Oxford: Clarendon Press.
  - 1962 *Social Anthropology and Other Essays*. New York: The Free
    Press. [BBC Third
    Programme](../Page/BBC_Third_Programme.md "wikilink") Lectures,
    1950.
  - 1965 *Theories of Primitive Religion*. Oxford University Press. ISBN
    0-19-823131-8
  - 1967 *The Zande Trickster*. Oxford: Clarendon Press.

## 外部連結

  - [伊凡普里查在蘇丹南部的攝影作品(Photography by Evans-Pritchard in the Southern
    Sudan held at the Pitt Rivers Museum
    collection)](http://southernsudan.prm.ox.ac.uk/biography/pritchard/)
  - [社會人類學的學科視野：《社會人類學及其他論文》第一章("The scope of the subject" - first
    chapter from "Social Anthropology and Other
    Essays")](http://www.scribd.com/doc/32740/Edward-Evan-EvansPritchard-The-scope-of-the-subject-)

[E](../Category/英国人类学家.md "wikilink")
[E](../Category/牛津大学埃克塞特学院校友.md "wikilink")
[E](../Category/倫敦政治經濟學院校友.md "wikilink")
[E](../Category/倫敦政治經濟學院教授.md "wikilink")
[E](../Category/從聖公會皈依天主教.md "wikilink")
[E](../Category/英格蘭天主教徒.md "wikilink")
[E](../Category/下級勳位爵士.md "wikilink")
[E](../Category/溫徹斯特公學校友.md "wikilink")