**e小調**是一個於E音開始的音樂的[小調](../Page/小調.md "wikilink")，調號以1個[升號表示](../Page/升號.md "wikilink")，自由小調中的組成的音有E、F、G、A、B、C、D及E。

當e小調以[和聲小調顯現時](../Page/和聲小調.md "wikilink")，[導音D會變成D](../Page/導音.md "wikilink")，而在[旋律小調上](../Page/旋律小調.md "wikilink")，上行[下中音和導音均會升高為C](../Page/下中音.md "wikilink")及D．下行時則還原。

它的相對大調是[G大調](../Page/G大調.md "wikilink")，並行大調是[E大調](../Page/E大調.md "wikilink")。

## 以e小調命名的著名作品

e小調是一個頗常見的調號，因此有不少著名作品都是以此為調號。

  - [布拉姆斯](../Page/布拉姆斯.md "wikilink")：[第4號交響曲](../Page/第4號交響曲_\(布拉姆斯\).md "wikilink")
  - [蕭邦](../Page/蕭邦.md "wikilink")：第19號夜曲，作品72

<!-- end list -->

  -
    　　　[第1號鋼琴協奏曲](../Page/第1號鋼琴協奏曲_\(蕭邦\).md "wikilink")
    　　　第14號圓舞曲，作品B.56（補遺）
    　　　第4號前奏曲，作品28-4

<!-- end list -->

  - [德伏扎克](../Page/德伏扎克.md "wikilink")：[新世界交響曲](../Page/第9號交響曲_\(德沃夏克\).md "wikilink")

<!-- end list -->

  -
    　　　　　第10號斯拉夫舞曲，作品72-2

<!-- end list -->

  - [艾爾加](../Page/艾爾加.md "wikilink")：[大提琴協奏曲](../Page/大提琴協奏曲_\(艾爾加\).md "wikilink")
  - [海頓](../Page/海頓.md "wikilink")：第44號交響曲
  - [弗里茨·克萊斯勒](../Page/弗里茨·克萊斯勒.md "wikilink")：前奏曲與快板
  - [孟德爾遜](../Page/孟德爾遜.md "wikilink")：[小提琴協奏曲](../Page/小提琴協奏曲_\(孟德爾遜\).md "wikilink")，作品64
  - [帕格尼尼](../Page/帕格尼尼.md "wikilink")：24首小提琴獨奏隨想曲－第3及第15首
  - [浦羅哥菲夫](../Page/浦羅哥菲夫.md "wikilink")：《[羅密歐與茱麗葉](../Page/羅密歐與朱麗葉_\(浦羅哥菲夫\).md "wikilink")》中的「武士之歌」
  - [拉赫曼尼諾夫](../Page/拉赫曼尼諾夫.md "wikilink")：[第2號交響曲](../Page/第2號交響曲_\(拉赫曼尼諾夫\).md "wikilink")

<!-- end list -->

  -
    　　　　　　　《無言之歌》，作品34-14

<!-- end list -->

  - [蕭士達高維契](../Page/蕭士達高維契.md "wikilink")：[第10號交響曲](../Page/第10號交響曲_\(蕭士塔高維奇\).md "wikilink")
  - [西貝流士](../Page/西貝流士.md "wikilink")：[第1號交響曲](../Page/第1號交響曲_\(蕭士塔高維奇\).md "wikilink")
  - [柴可夫斯基](../Page/柴可夫斯基.md "wikilink")：[第5號交響曲](../Page/第5號交響曲_\(柴可夫斯基\).md "wikilink")
  - [佛漢·威廉士](../Page/佛漢·威廉士.md "wikilink")：[第6號交響曲](../Page/e小調交響曲_\(佛漢·威廉士\).md "wikilink")
  - [韋華第](../Page/韋華第.md "wikilink")：巴松管協奏曲，RV 484

[E_natural_minor_scale_ascending_and_descending.png](https://zh.wikipedia.org/wiki/File:E_natural_minor_scale_ascending_and_descending.png "fig:E_natural_minor_scale_ascending_and_descending.png")
[E_harmonic_minor_scale_ascending_and_descending.png](https://zh.wikipedia.org/wiki/File:E_harmonic_minor_scale_ascending_and_descending.png "fig:E_harmonic_minor_scale_ascending_and_descending.png")
[E_melodic_minor_scale_ascending_and_descending.png](https://zh.wikipedia.org/wiki/File:E_melodic_minor_scale_ascending_and_descending.png "fig:E_melodic_minor_scale_ascending_and_descending.png")

[Category:調](../Category/調.md "wikilink")