<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><br />
<a href="../Page/HMS_E4.md" title="wikilink">HMS <em>E4</em></a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>E級</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>性能諸元</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>排水量:</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>長:</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>闊:</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>吃水:</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>動力:</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>航速:</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>續航能力:</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>編制:</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>武器:</p></td>
</tr>
</tbody>
</table>

英國皇家海軍的**E級**[柴電潛艇於](../Page/柴電.md "wikilink")1912至1916年間建造，是為[第一次世界大戰時](../Page/第一次世界大戰.md "wikilink")，艦隊潛艇之主力。

包括為[澳大利亞皇家海軍所建之兩艘在內](../Page/澳大利亞皇家海軍.md "wikilink")，該級潛艇共建成58艘。

該艇分三批次建造。第三批次之中六艘的射柱管被除去，改為20 vertical tubes，成為佈雷潛艇。

## 該級諸艦

  - 第一批次
      - *[E1](../Page/HMS_E1.md "wikilink")* - 1912年11月9日下水。
      - *[E2](../Page/HMS_E2.md "wikilink")* - 1912年11月23日下水。
      - *[E3](../Page/HMS_E3.md "wikilink")* -
        1912年10月29日下水。1914年10月18日被*U27*的魚雷擊中。
      - *[E4](../Page/HMS_E4.md "wikilink")* - 1912年2月5日下水。
      - *[E5](../Page/HMS_E5.md "wikilink")* -
        1912年5月17日下水。1916年3月7日在[北海被水雷炸毀及沉沒](../Page/北海_\(大西洋\).md "wikilink")。
      - *[E6](../Page/HMS_E6.md "wikilink")* -
        1912年11月12日下水。1915年12月26日被水雷炸毀。
      - *[E7](../Page/HMS_E7.md "wikilink")* -
        1913年10月2日下水。1915年9月5日於[加里波利之戰中在](../Page/加里波利之戰.md "wikilink")[達達尼爾海峽被鑿沈](../Page/達達尼爾海峽.md "wikilink")。
      - *[E8](../Page/HMS_E8.md "wikilink")* - 1913年10月30日年下水。
      - *[AE1](../Page/HMAS_AE1.md "wikilink")* -
        為澳大利亞皇家海軍建造。1914年9月14日在[巴布亞新畿內亞附近遇難](../Page/巴布亞新畿內亞.md "wikilink")。
      - *[AE2](../Page/HMAS_AE2.md "wikilink")* -
        為澳大利亞皇家海軍建造。1915年4月28日於[加里波利之戰中在](../Page/加里波利之戰.md "wikilink")[馬爾馬拉海沈沒](../Page/馬爾馬拉海.md "wikilink")。
  - 第二批次
      - *[E9](../Page/HMS_E9.md "wikilink")* - 1913年11月29日下水。
      - *[E10](../Page/HMS_E10.md "wikilink")* -
        1913年11月29日下水。1915年1月18日遇難。
      - *[E11](../Page/HMS_E11.md "wikilink")* - 1914年4月23日下水。
      - *[E12](../Page/HMS_E12.md "wikilink")* - 1914年9月5日下水。
      - *[E13](../Page/HMS_E13.md "wikilink")* - 1914年9月22日下水。
      - *[E14](../Page/HMS_E14.md "wikilink")* -
        1914年7月7日下水。1918年1月27日被水雷炸毀及沉沒。
      - *[E15](../Page/HMS_E15.md "wikilink")* -
        1914年4月23日下水。1915年4月19日在達達尼爾海峽被摧毀。
      - *[E16](../Page/HMS_E16.md "wikilink")* - 1914年9月23日下水。
      - *[E17](../Page/HMS_E17.md "wikilink")* - 1915年1月16日下水。
      - *[E18](../Page/HMS_E18.md "wikilink")* - 1915年3月4日下水。 1916年5月遇難。
      - *[E19](../Page/HMS_E19.md "wikilink")* - 1915年5月13日下水。
      - *[E20](../Page/HMS_E20.md "wikilink")* -
        1915年6月12日下水。1915年11月5日在達達尼爾海峽被魚雷擊中。
  - 第三批次
      - *[E21](../Page/HMS_E21.md "wikilink")* - 1915年7月24日下水。
      - *[E22](../Page/HMS_E22.md "wikilink")* -
        1915年8月27日下水。1916年4月25日被魚雷擊中及沉沒。
      - *[E23](../Page/HMS_E23.md "wikilink")* - 1915年9月28日下水。
      - *[E24](../Page/HMS_E24.md "wikilink")* - 1915年12月9日下水。（佈雷艇）
      - *[E25](../Page/HMS_E25.md "wikilink")* - 1915年8月23日下水。
      - *[E26](../Page/HMS_E26.md "wikilink")* -
        1915年11月11日下水。1916年7月6日遇難。
      - *[E27](../Page/HMS_E27.md "wikilink")* - 1917年6月9日下水。
      - *[E29](../Page/HMS_E29.md "wikilink")* - 1915年6月1日下水。
      - *[E30](../Page/HMS_E30.md "wikilink")* -
        1915年6月29日下水。1916年11月22日遇難。
      - *[E31](../Page/HMS_E31.md "wikilink")* - 1915年8月23日下水。
      - *[E32](../Page/HMS_E32.md "wikilink")* - 1916年8月16日下水。
      - *[E33](../Page/HMS_E33.md "wikilink")* - 1916年4月18日下水。
      - *[E34](../Page/HMS_E34.md "wikilink")* - 1917年1月27日下水。（佈雷艇）
      - *[E35](../Page/HMS_E35.md "wikilink")* - 1916年5月20日下水。
      - *[E36](../Page/HMS_E36.md "wikilink")* -
        1916年9月16日下水。1917年1月17日遇難。
      - *[E37](../Page/HMS_E37.md "wikilink")* -
        1915年9月2日下水。1916年12月1日遇難。
      - *[E38](../Page/HMS_E38.md "wikilink")* - 1916年6月13日下水。
      - *[E39](../Page/HMS_E39.md "wikilink")* - 1916年5月18日下水。
      - *[E40](../Page/HMS_E40.md "wikilink")* - 1916年11月9日下水。
      - *[E41](../Page/HMS_E41.md "wikilink")* - 1915年10月22日下水。（佈雷艇）
      - *[E42](../Page/HMS_E42.md "wikilink")* - 1915年10月22日下水。
      - *[E43](../Page/HMS_E43.md "wikilink")* - 1915年11月11日下水。
      - *[E44](../Page/HMS_E44.md "wikilink")* - 1916年2月21日下水。
      - *[E45](../Page/HMS_E45.md "wikilink")* - 1916年1月25日下水。（佈雷艇）
      - *[E46](../Page/HMS_E46.md "wikilink")* - 1916年4月4日下水。（佈雷艇）
      - *[E47](../Page/HMS_E47.md "wikilink")* -
        1916年5月29日下水。1917年8月20日遇難。
      - *[E48](../Page/HMS_E48.md "wikilink")* - 1916年8月2日下水。
      - *[E49](../Page/HMS_E49.md "wikilink")* -
        1916年9月18日下水。1917年3月12日在[設德蘭群島Huney附近被水雷炸毀及沉沒](../Page/設德蘭群島.md "wikilink")。
      - *[E50](../Page/HMS_E50.md "wikilink")* -
        1916年11月13日下水。1918年2月1日被水雷炸毀及沉沒。
      - *[E51](../Page/HMS_E51.md "wikilink")* - 1916年11月30日下水。（佈雷艇）
      - *[E52](../Page/HMS_E52.md "wikilink")* - 1917年1月25日下水。
      - *[E53](../Page/HMS_E53.md "wikilink")* - 1916年下水。
      - *[E54](../Page/HMS_E54.md "wikilink")* - 1916年下水。
      - *[E55](../Page/HMS_E55.md "wikilink")* - 1916年2月5日下水。
      - *[E56](../Page/HMS_E56.md "wikilink")* - 1916年6月19日下水。

## 相關條目

  - [潛艇](../Page/潛艇.md "wikilink")

[category:英國潛艦](../Page/category:英國潛艦.md "wikilink")

[Category:第一次世界大戰](../Category/第一次世界大戰.md "wikilink")
[Category:傳統動力潛艦](../Category/傳統動力潛艦.md "wikilink")
[Category:潛艇級別](../Category/潛艇級別.md "wikilink")
[Category:英国海军舰级](../Category/英国海军舰级.md "wikilink")
[Category:澳大利亞軍事](../Category/澳大利亞軍事.md "wikilink")
[Category:澳洲潛艦](../Category/澳洲潛艦.md "wikilink")
[Category:澳大利亚海军舰艇](../Category/澳大利亚海军舰艇.md "wikilink")