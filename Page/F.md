**F**, **f**
是[拉丁字母中的第](../Page/拉丁字母.md "wikilink")6个[字母](../Page/字母.md "wikilink")，它来源于表示音值
/f/ 的组合FH。

这个字母是由[伊特鲁里亚人发明的](../Page/伊特鲁里亚人.md "wikilink")，在[伊特鲁里亚语中](../Page/伊特鲁里亚语.md "wikilink")，F发与[希腊语相同的](../Page/希腊语.md "wikilink")
/w/（而在希腊语中叫做[Digamma的字母Ϝ则消失了](../Page/Ϝ.md "wikilink")，因为这个音位在希腊语中消失了）。F的原型是[闪族语中的字母](../Page/闪族语.md "wikilink")
，它也发作 /w/，这可能可以提供一个线索。

在一些[醫學檢驗報告中](../Page/醫學檢驗.md "wikilink")，參考值標示的F為女性（Female）的意思。\[1\]

## 字母F的含意

## 字符编码

| 字符编码                               | [ASCII](../Page/ASCII.md "wikilink") | [Unicode](../Page/Unicode.md "wikilink") | [EBCDIC](../Page/EBCDIC.md "wikilink") | [摩斯电码](../Page/摩斯电码.md "wikilink") |
| ---------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- | ---------------------------------- |
| [大写](../Page/大写字母.md "wikilink") F | 70                                   | 0046                                     | 198                                    | `··-·`                             |
| [小写](../Page/小写字母.md "wikilink") f | 102                                  | 0066                                     | 134                                    |                                    |

## 其他表示方法

## 参看

  - （[希腊字母](../Page/希腊字母.md "wikilink") Digamma）

[Category:拉丁字母](../Category/拉丁字母.md "wikilink")

1.