[F9C_Sparrowhawk.jpg](https://zh.wikipedia.org/wiki/File:F9C_Sparrowhawk.jpg "fig:F9C_Sparrowhawk.jpg")
**F9C雀鷹戰鬥機**是世界上唯一專司[飛船安全的](../Page/飛船.md "wikilink")[雙翼](../Page/雙翼機.md "wikilink")[戰鬥機](../Page/戰鬥機.md "wikilink")，該機當時任務是保護隸屬[美國海軍的兩艘飛船](../Page/美國海軍.md "wikilink")**-{艾克隆}-**（USS
Akron）及**麥康**（USS Macon）。
[USS_Macon_F9C.jpg](https://zh.wikipedia.org/wiki/File:USS_Macon_F9C.jpg "fig:USS_Macon_F9C.jpg")

## 歷史

F9C雀鷹戰鬥機在1931年2月首次試飛。上翼面中央有一掛鉤，可將機體懸吊於飛船掛架上，事後並可在空中進行回收。本機的測試結果非常成功。麥康於1935年墜毀時，損失了4架，而兩條飛船相繼發生事故後，此一計畫也無疾而終。

附帶一提的是一艘飛船一次可攜帶5架F9C，因此F9C-2含原型機在內，也只製造了10架，以應計劃需求。

## 性能

## 型號

  - XF9C-1

第一架原型機。

  - XF9C-2

第二架原型機。

  - F9C-3

單座戰鬥機。

[Category:美國戰鬥機](../Category/美國戰鬥機.md "wikilink")
[Category:柯蒂斯公司生产的飞机](../Category/柯蒂斯公司生产的飞机.md "wikilink")