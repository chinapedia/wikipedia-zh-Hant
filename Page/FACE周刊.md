[Face.png](https://zh.wikipedia.org/wiki/File:Face.png "fig:Face.png")
《**FACE**》為香港一份已停刊的[繁體中文雜誌](../Page/繁體中文.md "wikilink")，屬[壹傳媒旗下](../Page/壹傳媒.md "wikilink")，由大股東[黎智英創辦](../Page/黎智英.md "wikilink")，主要以年青讀者和中學生為對象。

## 歷史

《FACE》由《[壹本便利](../Page/壹本便利.md "wikilink")》（1991年9月13日－2007年5月23日）易名而成。

該雜誌在對外發售前，在香港《[蘋果日報](../Page/蘋果日報.md "wikilink")》（2007年5月28日）刊登廣告。廣告由[黎智英演繹受傷造型](../Page/黎智英.md "wikilink")，宣傳由雜誌《[壹本便利](../Page/壹本便利.md "wikilink")》，易名為《FACE》。雜誌出版初期，以「洗心革面，便利換FACE」為宣傳口號，謀求改革雜誌形象。

傳媒權威刊物《Media》，在《2007年度亞太區十大最佳印刷媒體》選舉活動，《FACE》排名第二，僅次於《Vogue》印度版。《Media》評審團認為，從富爭議性的《壹本便利》轉換成《FACE》的宣傳手法，形象新鮮兼消去過去負面印象。\[1\]

《FACE》的雜誌社英文名稱「FACE Magazine
Limited」，為一間獨立註冊的有限公司。前《[壹本便利](../Page/壹本便利.md "wikilink")》的附屬刊物—《交易通／搵車快線》則未結束，並以「FACE
Magazine Marketing
Limited」名義出版第801期，連同《FACE》創刊號一併出售，其後再在同年9月增加以高科技及資訊科技內容為主的《Ketchup》。

《FACE》封面雖然列出「出版時間」為逢星期三，但通常前一天（星期二），市面已經有售。

2016年2月22日，[壹傳媒向員工發內部公告](../Page/壹傳媒.md "wikilink")，指《FACE》印刷版將於3月底停刊。\[2\]

2016年3月29日，最後一期《FACE》出版。\[3\]

## 簡介

### 《FACE》

《**FACE**》內容以娛樂、消閑、[動漫等潮流資訊為主](../Page/動漫.md "wikilink")，亦會報導藝人緋聞等花邊消息。2011年2月加入大專校園版。2010年開始舉辦FACE
USTAR大專校花校草大選，入圍和勝出者不少加入了演藝界、模特兒界等，如[陳婉婷](../Page/陳婉婷_\(演員\).md "wikilink")、周雅霏、[李任燊](../Page/李任燊.md "wikilink")、[何艷娟](../Page/何艷娟.md "wikilink")、[邱念恩](../Page/邱念恩.md "wikilink")、[湯加文](../Page/湯加文.md "wikilink")、[謝文欣](../Page/謝文欣.md "wikilink")、
[陳詩欣](../Page/陳詩欣.md "wikilink")、[羅啟聰](../Page/羅啟聰.md "wikilink")、[羅堃尉](../Page/羅堃尉.md "wikilink")、[李嘉豪](../Page/李嘉豪.md "wikilink")、[鍾名山](../Page/鍾名山.md "wikilink")、[黃凱逸](../Page/黃凱逸.md "wikilink")、[蔡寶欣](../Page/蔡寶欣.md "wikilink")、[吳子冲](../Page/吳子冲.md "wikilink")、[梁嘉琬](../Page/梁嘉琬.md "wikilink")、[蔡嘉欣](../Page/蔡嘉欣.md "wikilink")、[趙詠瑤](../Page/趙詠瑤.md "wikilink")、[馬俊怡](../Page/馬俊怡.md "wikilink")、[張鴻熙](../Page/張鴻熙.md "wikilink")、[鄧鈞耀](../Page/鄧鈞耀.md "wikilink")、[Faith
(組合)](../Page/Faith_\(組合\).md "wikilink")、[何艷娟](../Page/何艷娟.md "wikilink")、[阮嘉敏](../Page/阮嘉敏.md "wikilink")、[關嘉敏](../Page/關嘉敏.md "wikilink")、[黃鍵安](../Page/黃鍵安.md "wikilink")、[劉頌鵬](../Page/劉頌鵬.md "wikilink")、[吳啟洋](../Page/吳啟洋.md "wikilink")、[袁學謙](../Page/袁學謙.md "wikilink")、[易佩詩](../Page/易佩詩.md "wikilink")、[蘇順敏等等](../Page/蘇順敏.md "wikilink")。

### 《交易通》(Trading Express)

[float](https://zh.wikipedia.org/wiki/File:Logo_trad.jpg "fig:float")
《**交易通**》主要報導理財、創業商機、學習進修、就業經驗、潮流商品、找尋工作及二手買賣等資訊。

### 《搵車快線》(Auto Express)

[float](https://zh.wikipedia.org/wiki/File:Logo_auto.jpg "fig:float")
《**搵車快線**》主要報導汽車資訊、新車和[電玩測試](../Page/電玩.md "wikilink")、求助信箱、買賣情報、易手車及其配件等買賣資訊。

### 《Ketchup》

《**Ketchup**》於2007年9月創刊，與《**FACE**》以合併形式出售，以高科技及資訊科技內容為主。\[4\]

在2011年2月22日，Ketchup 獨立出刊，由藝人徐濠縈總監修，改走潮流服飾打扮路線。

由2010年起,FACE 舉辦FACE Ustar 大專校花校草大選，2011年8月開始第二屆招募大專校花校草大選。

## 其他出版物

### 地圖集

沿襲前《[壹本便利](../Page/壹本便利.md "wikilink")》出版的地圖集《地圖王》系列，《FACE》以其雜誌社名字，繼續出版《地圖王》，並由2007年8月出版的《2008地圖王》作為開端。它採用香港[地政總署的數碼地圖製作而成](../Page/地政總署.md "wikilink")，並附有便利店位置、巴士路線標示，及地區特色簡介等資訊。另設有廣告頁，當地潮流及旅遊景點、食肆、商場及商店等介紹頁。

## 參見

  - [壹傳媒有限公司](../Page/壹傳媒有限公司.md "wikilink")
  - [壹本便利](../Page/壹本便利.md "wikilink")

## 参考資料

## 外部連結

  - [FACE周刊](http://hk.face.nextmedia.com)
  - [交易通．搵車快線](http://hk.tradingauto.nextmedia.com)
  - [ketchup](http://hk.ketchup.nextmedia.com)

[Category:香港傳媒爭議](../Category/香港傳媒爭議.md "wikilink")
[Category:壹傳媒](../Category/壹傳媒.md "wikilink")
[Category:被防火长城封锁的网站](../Category/被防火长城封锁的网站.md "wikilink")
[Category:香港已停刊杂志](../Category/香港已停刊杂志.md "wikilink")
[Category:2007年建立的出版物](../Category/2007年建立的出版物.md "wikilink")
[Category:2016年停刊的出版物](../Category/2016年停刊的出版物.md "wikilink")

1.  [《隔牆有耳：《FACE》獲選年度十大第二位》](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20071218&sec_id=4104&subsec_id=15333&art_id=10548540&cat_id=45&coln_id=20)
    ，原載香港《蘋果日報》，2007-12-18。
2.  [《壹週刊》再重組 《FACE》下月停刊
    壹傳媒裁百人](http://hk.apple.nextmedia.com/news/art/20160223/19502244)，原載香港《蘋果日報》，2016-2-23。
3.
4.  [壹傳媒集團企業官方網站 \> 關於我們 \> 「壹」業務 \>
    書籍及雜誌出版](http://www.nextmedia.com/v5/chinese/ourbusinessbottom01a.html)