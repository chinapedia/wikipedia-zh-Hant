是[日本](../Page/日本.md "wikilink")[電子遊戲商](../Page/電子遊戲發行商.md "wikilink")[任天堂於於](../Page/任天堂.md "wikilink")1986年2月推出的[家用遊戲機硬體](../Page/家用遊戲機.md "wikilink")，為任天堂於1983年發售的[8位元家用遊戲機](../Page/8位元.md "wikilink")[FC遊戲機](../Page/FC遊戲機.md "wikilink")（俗稱[紅白機](../Page/紅白機.md "wikilink")）的外接配件之一\[1\]。

FC磁碟機以[軟碟為儲存媒介](../Page/軟碟.md "wikilink")\[2\]\[3\]。軟碟的儲存空間較當時大多數家用遊戲機所使用的[卡帶大上許多](../Page/ROM卡帶.md "wikilink")，使FC磁碟機遊戲之畫面表現及音效多較同期之紅白機遊戲豐富\[4\]\[5\]。

FC磁碟機僅供日版主機使用，並未在北美地區推出。任天堂還曾授權[夏普生產將紅白機及FC磁碟機整合的二合一主機](../Page/夏普公司.md "wikilink")[Twin
Famicom](../Page/Twin_Famicom.md "wikilink")，同樣為日本專屬主機。2003年9月，任天堂終止軟碟改寫服務\[6\]。2007年10月，FC磁碟機本體維修服務亦告終止，宣告主機生命週期正式終結。FC磁碟機在其生命週期中銷量僅達444萬部\[7\]，以商業角度來看是失敗的。

## 歷史

### 開發背景

1980年代初期，市面上大多數的[家用遊戲機皆採用](../Page/家用遊戲機.md "wikilink")[卡帶作為遊戲媒體](../Page/ROM卡帶.md "wikilink")。卡帶雖有著讀取速度快的優勢，但其儲存空間不大，且其成本之高更令遊戲廠商難以賺取利潤。1984年，由於盜版軟體猖獗，在[日本唱片協會及](../Page/日本唱片協會.md "wikilink")的抗議下，日本政府修正《[著作權法](../Page/著作權法_\(日本\).md "wikilink")》，禁止租賃包括電子遊戲在內的各種軟體\[8\]\[9\]。隨著選擇的減少，消費者只能儘量尋求成本更低的遊戲方案。

[Famicom_diskcard.jpg](https://zh.wikipedia.org/wiki/File:Famicom_diskcard.jpg "fig:Famicom_diskcard.jpg")\]\]
[任天堂身為當時的家用遊戲機龍頭](../Page/任天堂.md "wikilink")，尤為卡帶的高成本所苦，於是決定找尋替代方案；在參考其它電子遊戲廠商的作法（如[Hudson
Soft的](../Page/Hudson_Soft.md "wikilink")）之後，最終選擇了[軟碟](../Page/軟碟.md "wikilink")。任天堂認為，軟碟不僅擁有高儲存空間、製造成本低於卡帶，且可重複讀寫以壓低長期成本及遊戲售價，潛力十足；於是以[三美電機的](../Page/三美電機.md "wikilink")軟碟為遊戲媒體基礎，開始研發以軟碟為遊戲媒體的遊戲機，即後來的FC磁碟機\[10\]\[11\]\[12\]。

### 發售

1986年2月21日，FC磁碟機於日本發售，售價15000[日圓](../Page/日圓.md "wikilink")，首發遊戲包括《[棒球](../Page/棒球_\(遊戲\).md "wikilink")》、《[高爾夫球](../Page/高爾夫_\(遊戲\).md "wikilink")》、《[麻將](../Page/麻將_\(1983年遊戲\).md "wikilink")》、《[足球](../Page/足球_\(1985年遊戲\).md "wikilink")》、《[網球](../Page/網球_\(遊戲\).md "wikilink")》、《[超級瑪利歐兄弟](../Page/超級瑪利歐兄弟.md "wikilink")》的軟碟移植版及專為充分運用軟碟媒介設計的全新遊戲《[薩爾達傳說](../Page/薩爾達傳說_\(遊戲\).md "wikilink")》\[13\]\[14\]\[15\]。

任天堂於日本各地的電子遊戲零售商設置了專用軟碟讀寫機，只需一次500日圓的讀寫費即可更換新遊戲，相較當時一般卡帶動輒2600日圓以上的售價便宜許多\[16\]\[17\]\[18\]。部分據點的軟碟讀寫機還具備傳輸功能，玩家可以將支援此功能的軟碟（如《[FC大獎賽系列](../Page/FC大獎賽系列.md "wikilink")》）帶至零售商處，透過讀寫機的傳真機能將玩家的分數整理後發送給任天堂，藉此與全日本的玩家競爭，還有機會得到獎項\[19\]\[20\]。

### 衰頹

雖然FC磁碟機發售時銷量可觀，僅1986年一年就售出200萬部，然其標榜的諸多優勢在推出後僅數年後便消耗殆盡。1986年中，容量超越軟碟的卡帶研發成功，並於該年7月發售的遊戲《[大盜五佑衛門](../Page/大盜五佑衛門.md "wikilink")》中首次應用\[21\]；在[內建電池備份技術出現後](../Page/後備電池.md "wikilink")，即時存檔機能也不再專屬於軟碟\[22\]；同時，卡帶的製造成本也逐漸下降。

1987年後，家用遊戲機邁入[16位元時代](../Page/16位元.md "wikilink")；FC磁碟機面對[超級任天堂及](../Page/超級任天堂.md "wikilink")[Mega
Drive等畫面表現更豐富](../Page/Mega_Drive.md "wikilink")、遊戲陣容更堅強的次世代遊戲機無力競爭，遂逐漸淡出市場。1992年12月，最後一款官方授權遊戲《DISK城》發售；1993年，零售商通路軟碟改寫服務中止；2003年9月，任天堂官方軟碟改寫服務中止\[23\]；2007年10月，FC磁碟機本體維修服務亦告終止，宣告主機生命週期正式終結。FC磁碟機在其生命週期中銷量僅達444萬部，以商業角度來看是失敗的\[24\]\[25\]。

## 硬體

## 遊戲

## 參見

  -
  - [64DD](../Page/64DD.md "wikilink")

## 參考資料

## 外部連結

  - [Famicom
    Dojo](https://web.archive.org/web/20111028223651/http://famicomdojo.tv/season1/episodes)

  - [Famicom
    World](http://www.famicomworld.com/Disk_System/Disk_System_Biography.htm)

  - [N-sider.com -
    NintendOnline](https://web.archive.org/web/20080509161926/http://www.n-sider.com/articleview.php?articleid=258)

  - [Famicom Disk System](http://www.famicomdisksystem.com/)

[Category:1986年面世的產品](../Category/1986年面世的產品.md "wikilink")
[Category:紅白機](../Category/紅白機.md "wikilink")
[Category:第三世代遊戲機](../Category/第三世代遊戲機.md "wikilink")
[Category:任天堂硬體](../Category/任天堂硬體.md "wikilink")

1.
2.
3.

4.

5.

6.

7.
8.

9.

10.

11.
12.

13.
14.

15.
16.
17.

18.

19.
20.

21.

22.
23.
24.
25.