**FLAG**是從2006年6月16日開始在[萬代頻道播放的](../Page/萬代頻道.md "wikilink")[Web動畫](../Page/Web動畫.md "wikilink")。全13話。

描寫[中亞內戰中的小國](../Page/中亞.md "wikilink")裡的攝影師白州冴子。不使用第三者視點，而是從第一人稱，主人公主觀的觀點來描寫。使用非常多由[相機](../Page/相機.md "wikilink")[觀景窗所得到的畫面](../Page/觀景窗.md "wikilink")。標題名稱「FLAG」是劇中主角白州冴子所拍攝的照片名稱。

## 簡介

20XX年，亞洲某小國發生內戰，由于聯合國軍的介入而擴大，逐漸陷入僵持。但是由于攝影師白州冴子偶然拍攝到的一張，名為FLAG的照片，越來越趨向和平。FLAG成了和平的象征。

但是，激進派武裝勢力卻奪走了FLAG。

聯合國為了秘密奪回FLAG，決定投入特殊部隊SDC（Special Development
Command），并命令帶上攝影師記錄他們的全部活動，這個人，正是冴子。

但是，裝備了最新強化裝甲HAVWC（High Agility Versatile Weapon
Carrier），認為這是很容易的任務的特殊小隊，遇到了意料之外的反擊，陷入苦戰……

## 登場人物

  - 白州冴子（、[声優](../Page/声優.md "wikilink")：[田中麗奈](../Page/田中丽奈_\(演员\).md "wikilink")）25歲，[攝影師](../Page/攝影師.md "wikilink")，懷著對戰地報道的純粹夢想，相信照片的力量。因此，對於自己拍的照片能夠偶然對別人的推動而感到責任，欣然接受了跟隨特殊部隊SDC進行采訪的危險工作。
  - 赤城圭一（、声優：[石塚運昇](../Page/石塚運昇.md "wikilink")）白州的朋友，屬於Photo
    Agency“Horizont”的攝影師。對於戰場的採訪有豐富的經驗，因此，和白川正相反，變得相當厭世。在看白川交給他的錄影和照片的過程中，逐漸對旗幟奪還作戰懷有疑問，跟著白川的足跡前進。
  - 库丽斯·艾娃索尔特（、声優：[日高奈留美](../Page/日高奈留美.md "wikilink")）聯合國軍士官、SDC隊隊長
  - 纳迪·奥劳坎蒂（、声優：[長嶝高士](../Page/長嶝高士.md "wikilink")）聯合國軍士官、SDC隊隊員
  - 哈康·阿库巴尔（、声優：[佐藤佑子](../Page/佐藤佑子.md "wikilink")）同上
  - 拉维尔·斯敏（、声優：[淺川悠](../Page/淺川悠.md "wikilink")）同上
  - 一柳信（声優：[川田紳司](../Page/川田紳司.md "wikilink")）同上
  - 阳·尼卡年（、声優：[乃村健次](../Page/乃村健次.md "wikilink")）同上
  - 库里斯特·巴罗奇（、声優：[岩崎博](../Page/岩崎博.md "wikilink")）同上

## 專有名詞解釋

**HAVWC(High Agility Versatile Weapon Carrier)**

  -
    [聯合國與](../Page/聯合國.md "wikilink")[美國共同開發中的多目的機動兵器](../Page/美國.md "wikilink")。一件強化裝甲服具有可以抵擋子彈的防彈性能和裝備各種武器平台。但是，它最大的特點是能實時處理複雜多樣而龐大的戰場資訊的能力。在這個故事發生的時間，已經有一部分投入實戰。但是，仍然是高價的特殊兵器，特別是SDC使用的更是其中最新的實驗機型。

## 工作人員

  - 原作：[高橋良輔](../Page/高橋良輔.md "wikilink")、TEAM FLAG
  - 総監督：高橋良輔
  - 監督：[寺田和男](../Page/寺田和男.md "wikilink")
  - 系列構成・脚本：[野崎透](../Page/野崎透.md "wikilink")
  - 角色設計、總作畫監督：竹内一義
  - 客座角色設計：渡邊裕二
  - 機械設計：[宮武一貴](../Page/宮武一貴.md "wikilink")
  - 美術監督：鈴木俊輔
  - 美術設定：伊井蔵
  - 色彩設定：久力志保
  - 編集：瀨山武司
  - 攝影監督：石原浩二
  - 3D監督：畑田裕之
  - 音響監督：[百瀨慶一](../Page/百瀨慶一.md "wikilink")
  - 音樂：[池頼広](../Page/池頼広.md "wikilink")
  - 動畫製作：[Answer Studio](../Page/Answer_Studio.md "wikilink")
  - 製作：[Aniplex](../Page/Aniplex.md "wikilink")、Answer Studio

## 主題曲

『Lights』

  -
    歌・作詞：／作曲・編曲：[大澤伸一](../Page/大澤伸一.md "wikilink")

## 関連書籍

  - FLAG ～白州冴子的故事～（（[渡邊麻實](../Page/渡邊麻實.md "wikilink")）

<!-- end list -->

  -
    2007年3月30日发售 ISBN 978-4-89425-541-8

## 外部連結

  - [官方網站](http://www.flag20xx.net/index.html)
  - [BANDAI
    CHANNAL中FLAG的專頁](https://web.archive.org/web/20060711161835/http://www.b-ch.com/cgi-bin/contents/ttl/det.cgi?ttl_c=493)

[Category:2006年日本網絡動畫](../Category/2006年日本網絡動畫.md "wikilink")
[Category:Aniplex](../Category/Aniplex.md "wikilink")
[Category:拍照摄影题材作品](../Category/拍照摄影题材作品.md "wikilink")
[Category:日本原創電視動畫](../Category/日本原創電視動畫.md "wikilink")