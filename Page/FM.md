**FM**可能指：

## 科技

  - [频率调制](../Page/频率调制.md "wikilink")（Frequency
    modulation），一种以载波（[無線電波](../Page/無線電波.md "wikilink")）的瞬时[频率变化来表示信息的调制方式](../Page/频率.md "wikilink")。

:\*[调频广播](../Page/调频广播.md "wikilink")（Frequency modulation
Broadcast），調頻廣播電台（Frequency modulation Broadcast station）

:\*（Frequency modulation Broadcast band），通信術語

:\*（Frequency modulation synthesis），通信術語

  - 化学元素[镄](../Page/镄.md "wikilink")（Fermium）的符号

  - [黑色金属](../Page/黑色金属.md "wikilink")（Ferrous
    metal），具[铁磁性的](../Page/铁磁性.md "wikilink")[金屬](../Page/金屬.md "wikilink")，工业上对铁、铬和锰的统称。

  - 英美长度单位[噚](../Page/噚.md "wikilink")（Fathom），主要用于海洋测量

  - 毫微微米（femtometre,
    fm），也称[飛米](../Page/飛米.md "wikilink")（*fermi*），是[国际单位制的长度单位](../Page/国际单位制.md "wikilink")。

  - 每[公升飛摩爾](../Page/公升.md "wikilink")（[femtomolar](../Page/wiktionary:femtomolar.md "wikilink"),
    fM），是[體積莫爾濃度的單位名稱](../Page/體積莫爾濃度.md "wikilink")。

  - 在[電子計算機或](../Page/電子計算機.md "wikilink")[个人电脑中](../Page/个人电脑.md "wikilink")，可能指[文件管理器或](../Page/文件管理器.md "wikilink")[檔案瀏覽器](../Page/檔案瀏覽器.md "wikilink")（File
    manager）

  - [快闪存储器](../Page/快闪存储器.md "wikilink")（Flash
    memory），简称閃存，是一种电子式可清除程序化只读存储器的形式，允许在操作中被多次擦或写的内存。

  - [.fm](../Page/.fm.md "wikilink")，密克罗尼西亚联邦国家及地区顶级域（ccTLD）的域名。

  - [雌性](../Page/雌性.md "wikilink")（Female），生物学中对性别的分类

  - [FM Towns](../Page/FM_Towns.md "wikilink"),
    日本[富士通生產的當地](../Page/富士通.md "wikilink")[个人电脑產品其中之一](../Page/个人电脑.md "wikilink")。

  - （Formula Mazda），SCCA 級。

## 人物

  - [費奧多爾·陀思妥耶夫斯基](../Page/費奧多爾·陀思妥耶夫斯基.md "wikilink")（F.M.
    Dostoevsky），19世纪的俄国作家

## 头衔

  - [财政部长](../Page/财政部长.md "wikilink")（Finance Minister）
  - [外交部长](../Page/外交部长.md "wikilink")（Foreign Minister）
  - 足球经理（Football manager），和[体育教练同义](../Page/体育教练.md "wikilink")

## 地名

  - [密克罗尼西亚联邦](../Page/密克罗尼西亚联邦.md "wikilink")（Federated States of
    Micronesia）的ISO 3166-1国家代码、FIPS 國家代碼 (10-4)及邮政代码

## 组织

  - [房利美](../Page/房利美.md "wikilink")（Fannie Mae），是最大一家美国政府赞助企业。
  - [房地美](../Page/房地美.md "wikilink")（Freddie
    Mac），是美国政府赞助企业中第二大的一家，商业规模仅次于房利美。
  - [上海航空的IATA航空公司代码](../Page/上海航空.md "wikilink")

## 娱乐

  - [足球经理人](../Page/足球经理人.md "wikilink")（Football
    Manager），一款在多種平台上發售的足球模拟经营类游戏。
  - 粉絲見面會，Fans Meeting的簡寫

## 其它

  - FM星球，电子游戏《[流星之洛克人](../Page/流星之洛克人.md "wikilink")》中的虚拟星球。