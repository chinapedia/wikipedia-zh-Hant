**FN CAL**（Carabine Automatique
Légère）是[比利時](../Page/比利時.md "wikilink")[Fabrique
Nationale設計及生產的](../Page/Fabrique_Nationale.md "wikilink")[突擊步槍](../Page/突擊步槍.md "wikilink")，亦是FN的首種[5.56毫米](../Page/5.56×45mm_NATO.md "wikilink")[小口徑步槍](../Page/小口徑步槍.md "wikilink")。

## 設計及相關

FN在推出CAL前，早已推出迷你FAL試驗型，但由於這步槍價格昂貴，兼當年[HK
G3系列已支配市場](../Page/HK_G3自動步槍.md "wikilink")，所以沒有太多國家釆用。CAL採用轉動式槍機設計，裝有單發、三點發及全自動扳機，可算是[FAL的小口徑版本](../Page/FN_FAL自動步槍.md "wikilink")，但[機匣內部與比FAL更加精密](../Page/機匣.md "wikilink")。FN在開發CAL時獲得一些實用技術，這些技術其後應用在著名的[FN
FNC上](../Page/FN_FNC突擊步槍.md "wikilink")。

美國民用市場亦曾輸入20把半自動版本的CAL\[1\]。

## 裝備國

  -
  -
  -
  -
  -
  -
## 參考

  - [FN FAL自動步槍](../Page/FN_FAL自動步槍.md "wikilink")
  - [FN FNC突擊步槍](../Page/FN_FNC突擊步槍.md "wikilink")
  - [SIG SG 540突擊步槍](../Page/SIG_SG_540突擊步槍.md "wikilink")

## 註釋

## 参考文献

  - [remtek.com-FN CAL、FNC](http://remtek.com/arms/fn/fnc/)
  - [.waffeninfo.net-FN
    CAL](https://web.archive.org/web/20071005011500/http://www.waffeninfo.net/waff_cal.php)

[Category:自動步槍](../Category/自動步槍.md "wikilink")
[Category:突擊步槍](../Category/突擊步槍.md "wikilink")
[Category:5.56×45毫米槍械](../Category/5.56×45毫米槍械.md "wikilink")
[Category:比利時槍械](../Category/比利時槍械.md "wikilink")
[Category:試驗和研究槍械](../Category/試驗和研究槍械.md "wikilink")

1.  [www.adamsguns.com](http://www.adamsguns.com/)