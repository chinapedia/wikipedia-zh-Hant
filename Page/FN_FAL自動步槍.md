**FAL**（[法文](../Page/法文.md "wikilink")：）由[比利時](../Page/比利時.md "wikilink")[Fabrique
Nationale設計的](../Page/Fabrique_Nationale.md "wikilink")[7.62×51毫米口径](../Page/7.62×51mm_NATO.md "wikilink")[戰鬥步槍](../Page/戰鬥步槍.md "wikilink")，FAL相對應的英文名稱是，意為「輕型自動步槍」。FAL是世界上著名步槍之一，曾是大量國家的制式裝備。

## 歷史

FAL自動步槍源於[第二次世界大戰結束後](../Page/第二次世界大戰.md "wikilink")[英國新的步槍研製計劃](../Page/英國.md "wikilink")。最初FAL全自動原型槍設計使用[德國](../Page/德國.md "wikilink")[StG44突擊步槍的](../Page/StG44突擊步槍.md "wikilink")[7.92×33毫米](../Page/7.92×33mm_Kurz.md "wikilink")[中間型威力槍彈](../Page/中間型威力槍彈.md "wikilink")，根據[英國的要求改成](../Page/英國.md "wikilink")7毫米口徑（[7×43毫米](../Page/7×43mm.md "wikilink")）。時逢[北約為簡化後勤供應進行彈藥通用化選型](../Page/北大西洋公约组织.md "wikilink")，由於[美國施加影響堅持推行大威力的](../Page/美國.md "wikilink")[7.62×51毫米步槍子彈](../Page/7.62×51mm_NATO.md "wikilink")，1953年北約選擇該款作為標準步槍子彈。FAL最終亦確定採用[7.62×51毫米](../Page/7.62×51mm_NATO.md "wikilink")[NATO標準步槍子彈](../Page/北大西洋公约组织.md "wikilink")，參選美國軍方的新步槍選型試驗的FAL被命名為T48，後來[美軍選擇了](../Page/美軍.md "wikilink")[春田兵工廠的T](../Page/春田兵工廠.md "wikilink")44（定型命名為[M14](../Page/M14自動步槍.md "wikilink")），T48落選。由于比利时感激第二次世界大战中北约各国解放比利时，因此FN兵工厂并不对制造FN
FAL的北约国家收取任何权利金。因此FAL被大量北約國家和非華沙公約國家裝備作制式步槍。相對於該時的冷戰情局，又一度被稱為「自由世界的右手」。

## 設計

[FN_FAL_1.JPG](https://zh.wikipedia.org/wiki/File:FN_FAL_1.JPG "fig:FN_FAL_1.JPG")的FAL\]\]
[ModernparaFAL.JPG](https://zh.wikipedia.org/wiki/File:ModernparaFAL.JPG "fig:ModernparaFAL.JPG")
FAL自動步槍採用氣動式工作原理，槍機採用偏移式閉鎖。導氣裝置位於槍管上方，導氣箍前端有可調整的螺旋氣體調節器，可根據不同的環境狀況調整槍彈發射時進入導氣裝置的火藥氣體壓力，可選擇發射[22毫米](../Page/22毫米槍榴彈.md "wikilink")[槍榴彈](../Page/步槍用榴彈.md "wikilink")。帶空槍掛機機構，不隨槍機運動的拉柄位於[機匣左側](../Page/機匣.md "wikilink")，[快慢機柄可選擇單發和連發射擊模式](../Page/擊發調變槍械.md "wikilink")，機匣上方裝有可摺疊的提把，槍口裝有。FAL自動步槍工藝精良，可靠性好，易於分解，[槍托接近槍管中心線](../Page/槍托.md "wikilink")，有效抑制槍口跳動，單發精度好。問題出在彈藥的選擇上，FAL自動步槍存在與美國裝備的[M14自動步槍類似的彈藥威力大](../Page/M14自動步槍.md "wikilink")，後座力使連發射擊時難以控制，散佈面較大的問題，如此[英聯邦國家制式FAL乾脆取消了連發射擊模式](../Page/英聯邦國家.md "wikilink")，只能單發射擊，作為[半自動步槍使用](../Page/半自動步槍.md "wikilink")。

1953年FAL自動步槍開始投入生產。包括特許生產與仿製，該槍曾被90多個國家和地區的軍隊採用，包括[英國](../Page/英國.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[澳大利亞等](../Page/澳大利亞.md "wikilink")[英聯邦國家](../Page/英聯邦.md "wikilink")，以及[比利時](../Page/比利時.md "wikilink")、德國（[聯邦德國](../Page/聯邦德國.md "wikilink")，即西德）、[奧地利](../Page/奧地利.md "wikilink")、[以色列](../Page/以色列.md "wikilink")、[印度](../Page/印度.md "wikilink")、[墨西哥](../Page/墨西哥.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[阿根廷](../Page/阿根廷.md "wikilink")、[南非等國都裝備了FAL自動步槍系列](../Page/南非.md "wikilink")。FN公司直到1980年代仍在生產。FAL自動步槍成為裝備國家最廣泛的軍用步槍之一。FAL具體產量無法準確統計，估計達到400萬支。

隨著[小口徑步槍的興起](../Page/小口徑步槍.md "wikilink")，1980年代持續到1990年代，許多國家裝備的FAL都被小口徑步槍替換。

此外，在1960年代至1970年代FAL自動步槍是西方[僱傭兵愛用的武器](../Page/僱傭兵.md "wikilink")，因此被美國的僱傭兵雜誌譽為「**二十世紀最偉大的僱傭兵武器之一」**。

## 衍生型

### FAL 50.41 & 50.42

  - 又稱 **FN FALO**
  - [班用機槍版本](../Page/班用機槍.md "wikilink")，採用重型槍管、配30發彈匣、[兩腳架](../Page/兩腳架.md "wikilink")、固定式[槍托](../Page/槍托.md "wikilink")。全長1150毫米，重量6公斤
  - 50.41採用塑料槍托，而50.42採用木製槍托

### FAL 50.61

  - 標凖槍管，摺疊[槍托](../Page/槍托.md "wikilink")

### FAL 50.63

  - 短槍管傘兵型，摺疊[槍托](../Page/槍托.md "wikilink")
  - 有兩種版本，槍管長458毫米或436毫米

### FAL 50.64

  - 標凖槍管，摺疊[槍托](../Page/槍托.md "wikilink")，槍托摺疊長845毫米，重量3.9公斤

### 美國民用型

由於美國入口條例使其難以进口完整的FN FAL，大部份都裝有“非軍事化”套件，但通常套件不協調，影響運作。

  - DSA SA58
    OSW：DSA公司生產的一系列改型，部份型號在機匣頂部和[護木上設有](../Page/護木.md "wikilink")[皮卡汀尼導軌](../Page/皮卡汀尼導軌.md "wikilink")，並有著不同尺寸的版本可供選擇。

## 外國授權生產

[SLRL1A1.jpg](https://zh.wikipedia.org/wiki/File:SLRL1A1.jpg "fig:SLRL1A1.jpg")
FAL除了由[比利時](../Page/比利時.md "wikilink")[Fabrique Nationale de
Herstal](../Page/Fabrique_Nationale_de_Herstal.md "wikilink")（FN）生產外，亦有合法授權給多個國家生產，包括[英國](../Page/英國.md "wikilink")（L1A1
SLR）、[南非](../Page/南非.md "wikilink")（R1）、[巴西](../Page/巴西.md "wikilink")、[澳大利亞](../Page/澳大利亞.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[以色列](../Page/以色列.md "wikilink")、[奧地利及](../Page/奧地利.md "wikilink")[阿根廷](../Page/阿根廷.md "wikilink")，[墨西哥採用FN及自行生產的部件組合而成](../Page/墨西哥.md "wikilink")。FAL亦有出口至多個國家如[委內瑞拉等](../Page/委內瑞拉.md "wikilink")。

各地制造的FAL設計皆有不同，有時又會分為「英制式」及「公制式」\[1\]。總計世界各地的服役數量（包括所有衍生型），FAL生產數量超過1,000,000把。

  - [奧地利](../Page/奧地利.md "wikilink")

[奧地利在對](../Page/奧地利.md "wikilink")[西班牙](../Page/西班牙.md "wikilink")[CETME及](../Page/CETME.md "wikilink")[美國](../Page/美國.md "wikilink")[AR-10估價後](../Page/AR-10.md "wikilink")，奧地利軍決定以FN
FAL作-{制}-式步槍，命名為**Sturmgewehr 58**（**Stg. 58**），Stg.
58在外表上與[德國](../Page/德國.md "wikilink")**G1步槍**相似，Stg.
58其後由[斯太爾AUG](../Page/斯泰爾AUG突擊步槍.md "wikilink")（**Stgw 77**）取代。

  - [巴西](../Page/巴西.md "wikilink")

[巴西在](../Page/巴西.md "wikilink")1954年進口小數量的FN
FAL步槍作評估，部隊在1958年至1962年進行實戰測試，其後在1964年，巴西正式裝備FN
FAL並定名為 **M964**（指1964年製造）。其後開始在巴西軍用物資工業公司（Indústria de Material Bélico
do
Brasil-IMBEL）合法許可生產M964，而摺疊[槍托版本名為](../Page/槍托.md "wikilink")**M969A1**。在約1980年代後期至1990年代早期IMBEL已生產了約200,000把M964。

巴西制的FAL的設計較簡化，可令生產成本降低。早期巴西的FAL有「Type 1」或「Type
2」兩種[機匣](../Page/機匣.md "wikilink")，塑料槍托、握把、謢木，可發射[槍榴彈的](../Page/槍榴彈.md "wikilink")22毫米圓筒狀及D型塑料提把。巴西制的FAL亦有出口至[烏拉圭](../Page/烏拉圭.md "wikilink")。

除公制式的FAL外，巴西亦有製造FAL的變種，亦即是發射[5.56 x
45毫米NATO的](../Page/5.56x45_NATO.md "wikilink")**MD-1**、**MD-2**及**MD-3**突擊步槍，由IMBEL制造。第一种是在1982年推出的MD-1，其後在1985年推出MD-2（FAL式固定槍托）和MD-3（摺疊槍托）並裝備[巴西陸軍](../Page/巴西陸軍.md "wikilink")。MD-2/MD-3與FAL非常相似，但改進了閉鎖系統，裝有M16相同的[轉拴式槍機](../Page/轉拴式槍機.md "wikilink")，對應標準[STANAG彈匣](../Page/STANAG彈匣.md "wikilink")。

IMBEL在1980年代中期亦有推出半自動民用版本的FAL，在美國民間市場名為SAR-48。
[Canadian_soldier_with_C2_DA-SC-84-02213.JPEG](https://zh.wikipedia.org/wiki/File:Canadian_soldier_with_C2_DA-SC-84-02213.JPEG "fig:Canadian_soldier_with_C2_DA-SC-84-02213.JPEG")

  - [加拿大](../Page/加拿大.md "wikilink")

[加拿大軍隊裝備不同版本可空槍掛機的FN](../Page/加拿大軍隊.md "wikilink") FAL，其中最著名是FN
**C1A1**，亦即是英軍的L1A1，加拿大亦有裝備移除了護木及增大腳架的FN FAL
50.41/42作[班用機槍用途](../Page/班用機槍.md "wikilink")，名為**C2A1**。FN
C1A1由[加拿大兵工廠](../Page/加拿大兵工廠.md "wikilink")（Canadian Arsenal
Limited）特許生產，C1及C1A1在1950年代早期開始裝備加拿大軍隊，**C1**在1959年改進成**C1A1**，[加拿大皇家海軍版本可全自動發射的C](../Page/加拿大皇家海軍.md "wikilink")1名為**C1D**及**C1A1D**。在1984年由[迪瑪科C7及](../Page/C7突擊步槍.md "wikilink")[FN
Minimi](../Page/FN_Minimi輕機槍.md "wikilink")（C9輕機槍）取代。
[German_FAL-G1.jpg](https://zh.wikipedia.org/wiki/File:German_FAL-G1.jpg "fig:German_FAL-G1.jpg")

  - [西德](../Page/西德.md "wikilink")

[西德在](../Page/西德.md "wikilink")1955至1956年間裝備加拿大制式的FN
FAL，以替換其[M1加蘭德步槍和](../Page/M1加兰德步枪.md "wikilink")[M1/M2卡賓槍](../Page/M1卡宾枪.md "wikilink")，西德的FAL裝有五叉式及木制槍托，其後改為金屬槍托，最初只有邊防隊裝配，其後[西德聯邦國防軍也跟進](../Page/德國聯邦國防軍.md "wikilink")。在1956年11月，西德裝備了約100,000把FAL，命名為**G1**。G1在1957年4月至1958年5月由[Fabrique
Nationale製造](../Page/Fabrique_Nationale.md "wikilink")，G1在德軍只服役了一段短時間便遭[西班牙的](../Page/西班牙.md "wikilink")[CETME](../Page/CETME.md "wikilink")（技術亦大量用於[HK
G3上](../Page/HK_G3自動步槍.md "wikilink")）所取代，退役的G1被轉交給[土耳其](../Page/土耳其.md "wikilink")。\[2\]

  - [印度](../Page/印度.md "wikilink")

[印度的FN](../Page/印度.md "wikilink") FAL名為**1A
SLR**（自動裝填步槍），印度仿制的FAL是基於英國的L1A1，但瞄準標尺不同。印度在1962年裝備半自動的1A
SLR，1A SLR在1965年[印巴戰爭中首次出現](../Page/印巴戰爭.md "wikilink")。1A
SLR在印度軍隊共服役了45年，其後被[INSAS槍族取代](../Page/INSAS.md "wikilink")，從軍隊退役的1A
SLR被警隊、執法部門採用，亦有部份提供給[尼泊爾](../Page/尼泊爾.md "wikilink")。

  - [以色列](../Page/以色列.md "wikilink")

在1948年[第一次中東戰爭後](../Page/第一次中東戰爭.md "wikilink")，[以色列國防軍](../Page/以色列國防軍.md "wikilink")（IDF）由於大量採用各種舊式槍械，出現了彈藥種類太多、維修保養困難等的後勤問題而需要解決。在1955年以色列國防軍採用由[以色列軍事工業](../Page/以色列軍事工業.md "wikilink")（IMI）設計的[烏茲衝鋒槍](../Page/烏茲衝鋒槍.md "wikilink")，但由於火力不足、有效射程短及後座力大等問題，以色列在同年採用FN
FAL作制式步槍，命名為（Romat-自動裝填步槍的縮寫）。以色列採用的FAL有兩種基本型號，包括只能半自動發射的標準槍管型及用於全自動發射的重槍管型。以色列的重槍管型FAL亦有出口至其他國家。

以色列的FAL在1956年[第二次中東戰爭首次小量出現](../Page/第二次中東戰爭.md "wikilink")，而在1967年7月的[六日戰爭FAL成為了制式步槍](../Page/六日戰爭.md "wikilink")，直至1973年10月[贖罪日戰爭仍然是以色列國防軍前線部隊的制式步槍](../Page/贖罪日戰爭.md "wikilink")。然而，FAL受到多種批評，如步槍尺寸太長令出入車箱不便、相比其他步槍太重（指[AK-47突击步枪](../Page/AK-47突击步枪.md "wikilink")）、在沙漠戰場上需要經常清潔而影響作戰等的問題，甚至令士兵丟棄手上的FAL而改用[AK-47突击步枪](../Page/AK-47突击步枪.md "wikilink")、M16A1突击步枪、[烏茲衝鋒槍等的武器](../Page/烏茲衝鋒槍.md "wikilink")，最後以色列以[5.56
NATO的](../Page/5.56×45_NATO.md "wikilink")[Galil取代FAL](../Page/IMI_Galil突擊步槍.md "wikilink")。

  - [荷蘭](../Page/荷蘭.md "wikilink")

[荷蘭皇家陸軍在](../Page/荷蘭皇家陸軍.md "wikilink")1961年裝備只能半自動發射的FN
FAL，荷蘭的FAL前準星上裝有護圈，德國G1式握把，部份可對應夜視裝置或瞄準鏡，FAL
50.42版本其後亦被裝備作為[班用機槍服役](../Page/班用機槍.md "wikilink")，1990年代被[迪瑪科C7取代](../Page/C7突擊步槍.md "wikilink")。

  - [紐西蘭](../Page/紐西蘭.md "wikilink")

[紐西蘭皇家空軍沒有跟隨陸軍以](../Page/紐西蘭皇家空軍.md "wikilink")[斯泰爾AUG突擊步槍](../Page/斯泰爾AUG突擊步槍.md "wikilink")（Austeyr）取代FAL，空軍繼續裝備FAL（**L1A1**），名為**7.62毫米
SLR**（7.62mm SLR），SLR即是自動裝填步槍（**S**elf **L**oading
**R**ifle），紐西蘭皇家空軍的7.62毫米 SLR裝有輕量化槍管，只能單發射擊。

  - [菲律賓](../Page/菲律賓.md "wikilink")

菲律賓軍隊從摩洛國家解放陣線（Moro National Liberation
Front－MNLF）手中捕獲了小量FAL，包括德國的G1和澳大利亞的L1A1。
[Sempreatentos...aoperigo\!.jpg](https://zh.wikipedia.org/wiki/File:Sempreatentos...aoperigo!.jpg "fig:Sempreatentos...aoperigo!.jpg")

  - [葡萄牙](../Page/葡萄牙.md "wikilink")

[葡萄牙以](../Page/葡萄牙.md "wikilink")[CETME](../Page/CETME.md "wikilink")（[HK
G3原型](../Page/HK_G3自動步槍.md "wikilink")）作主要制式步槍，但亦為其精英部隊裝備其他步槍作代替品，如傘兵的[AR-10](../Page/AR-10自動步槍.md "wikilink")。在反游擊行動後，葡萄牙裝備了輕型槍管的FN
FAL（命名為Espingarda Automática 7,62 mm FN m/62）提供給步兵精英部隊及突擊隊，包括Companhas
de Caçadores Especiais。

  - [南非](../Page/南非.md "wikilink")

在[HK
G3](../Page/HK_G3自動步槍.md "wikilink")、[AR-10及FN](../Page/AR-10自動步槍.md "wikilink")
FAL的競爭後，南非軍隊裝備了3種型號的FN
FAL，[英聯邦版本名為](../Page/英聯邦.md "wikilink")**R1**，FAL
50.64的輕型版本名為**R2**，警用半自動版本名為**R3**。R2由「利特爾頓兵工廠和兵工研製與生產公司」（「LEW，ARMSCOR」*Lyttelton
Engineering Works，Armaments Development and Production Corporation of
South
Africa*）生產，在種族隔離政策時期利特爾頓及其他小型輕武器公司被私有化而停止生產。1980年代中期，R1被新型的[R4突擊步槍](../Page/R4突擊步槍.md "wikilink")（南非版的[Galil](../Page/IMI_Galil突擊步槍.md "wikilink")，並有3種不同長度的型號）所取代。

  - [英國及](../Page/英國.md "wikilink")[英聯邦成員](../Page/英聯邦.md "wikilink")

[英軍在](../Page/英軍.md "wikilink")1954年起正式採用FAL，他們的FAL被命名為**L1A1
SLR**，並只有[半自動射撃功能](../Page/半自動步槍.md "wikilink")。除了英國本土採用之外，大部份的[英聯邦國家皆有採用](../Page/英聯邦.md "wikilink")。L1A1
SLR是由英國本土的[皇家輕兵器工廠或由各](../Page/皇家輕兵器工廠.md "wikilink")[英聯邦國家的兵工廠所生產的](../Page/英聯邦.md "wikilink")。在1985年，英國開始以[L85A1逐步取代L](../Page/SA80突擊步槍.md "wikilink")1A1
SLR，並在1994年完全取代。儘管如此，部份英聯邦國家至今仍有採用。

  - [委內瑞拉](../Page/委內瑞拉.md "wikilink")

[委內瑞拉是繼](../Page/委內瑞拉.md "wikilink")[比利時後第一個裝備FN](../Page/比利時.md "wikilink")
FAL的國家，直到現在FN
FAL仍然是其制式步槍之一。委內瑞拉總統[烏戈·查維茲從](../Page/烏戈·查維茲.md "wikilink")[俄羅斯買下十多萬支](../Page/俄羅斯.md "wikilink")[AK-103全面配發來取代舊式FN](../Page/AK-103突击步枪.md "wikilink")
FAL\[3\]。新型的[AK-103在](../Page/AK-103.md "wikilink")2006年尾全數運抵，而原本的FN
FAL將在後備軍及民防團服役。
[FN_FAL_DA-SD-04-01067.jpg](https://zh.wikipedia.org/wiki/File:FN_FAL_DA-SD-04-01067.jpg "fig:FN_FAL_DA-SD-04-01067.jpg")士兵使用FAL\]\]

## 使用國

  - \[4\]

  - －本土生產。\[5\]

  - －本土生產，命名為**L1A1**，已被[斯泰爾AUG所取代](../Page/斯泰爾AUG.md "wikilink")。

  - －已被[斯泰爾AUG所取代](../Page/斯泰爾AUG.md "wikilink")。

  -
  - \[6\]

  - \[7\]

  - \[8\]

  - －已被[FN FNC所取代](../Page/FN_FNC突擊步槍.md "wikilink")。\[9\]

  - \[10\]

  -
  -
  - \[11\]

  -
  - \[12\]

  -
  - \[13\]

  - \[14\]

  - \[15\]

  - －已被[C7所取代](../Page/C7突擊步槍.md "wikilink")。

  -
  - \[16\]

  - \[17\]

  - \[18\]

  -
  - \[19\]

  - \[20\]

  -
  -
  - \[21\]

  - \[22\]

  - \[23\]

  - \[24\]

  - \[25\]

  - \[26\]

  -
  - －大部份已被[斯泰爾AUG所取代](../Page/斯泰爾AUG.md "wikilink")。

  - \[27\]

  -
  - \[28\]

  -
  - －在1950年代初期以**G1**的名義裝備[德國聯邦國防軍](../Page/德國聯邦國防軍.md "wikilink")，後來被[HK
    G3所取代](../Page/HK_G3.md "wikilink")。部份退役的FAL被轉交給土耳其。

  - \[29\]

  - －已退役。

  - \[30\]

  - \[31\]

  - －已被[INSAS所取代](../Page/INSAS突擊步槍.md "wikilink")。

  - －已退役。

  -
  - －\[32\]已被[斯泰爾AUG所取代](../Page/斯泰爾AUG.md "wikilink")，但現在仍有用作[狙擊步槍](../Page/狙擊步槍.md "wikilink")。\[33\]

  - －已被[IMI
    Galil和](../Page/IMI_Galil.md "wikilink")[M16所取代](../Page/M16突擊步槍.md "wikilink")。

  - \[34\]

  -
  - \[35\]

  - \[36\]

  - \[37\]

  - \[38\]

  - \[39\]

  - －被[斯泰爾AUG所取代](../Page/斯泰爾AUG.md "wikilink")。\[40\]

  - \[41\]

  - －使用[英国仿制的FN](../Page/英国.md "wikilink") FAL (L1A1
    SLR),之后被[M16及](../Page/M16突擊步槍.md "wikilink")[斯泰爾AUG所取代](../Page/斯泰爾AUG.md "wikilink")。\[42\]

  - \[43\]

  -
  - －本土生產。\[44\]

  - \[45\]

  - \[46\]

  - \[47\]

  - \[48\]

  - －已被[C7所取代](../Page/C7突擊步槍.md "wikilink")。

  - －已被[斯泰爾AUG所取代](../Page/斯泰爾AUG.md "wikilink")。

  -
  - －本土生產。\[49\]

  - \[50\]

  - \[51\]

  - \[52\]

  - －使用[澳洲製造的L](../Page/澳洲.md "wikilink")1A1。\[53\]

  - \[54\]

  - \[55\]

  -
  -
  - \[56\]

  - \[57\]

  -
  - \[58\]

  -
  - \[59\]

  -
  - \- 曾使用L1A1 SLR，現已退役。

  -
  -
  - －本土生產，命名為**R1**，現在前線部隊已普遍的被[R4所取代](../Page/R4突擊步槍.md "wikilink")，但仍有部份被改裝成特等射手步槍繼續服役。

  -
  - －曾使用L1A1 SLR，已被[56式自動步槍所取代](../Page/56式自動步槍.md "wikilink")。

  -
  - \[60\]

  - \[61\]

  - \[62\]

  - \[63\]

  -
  - \[64\]

  - \[65\]

  - \[66\]

  - －大部份從西德接收，已被[HK G3所取代](../Page/HK_G3.md "wikilink")，但現在仍有用作訓練用途。

  - \[67\]

  - \[68\]

  - －本土生產，命名為**L1A1**，已被[SA80所取代](../Page/SA80突擊步槍.md "wikilink")。

  - －根据许可生产。\[69\]

  - \[70\]

  - －正被[AK-103所取代](../Page/AK-103突擊步槍.md "wikilink")。

  - \[71\]

  - \[72\]

  - \[73\]

### 前使用國

  - [英屬香港](../Page/英屬香港.md "wikilink")－L1A1
    SLR曾是[駐港英軍](../Page/駐港英軍.md "wikilink")、[皇家香港警察](../Page/皇家香港警察.md "wikilink")、[香港海關及](../Page/香港海關.md "wikilink")[皇家香港軍團的制式步槍](../Page/皇家香港軍團.md "wikilink")。

  - －於1960年代起採用。\[74\]

### 非國家團體的採用

  - [莫洛民族解放陣線](../Page/莫洛民族解放陣線.md "wikilink")－從[利比亞獲得](../Page/利比亞.md "wikilink")1000
    多支FAL。
  - [莫洛伊斯蘭解放陣線](../Page/莫洛伊斯蘭解放陣線.md "wikilink")－同上。
  - [哥倫比亞革命武裝力量](../Page/哥倫比亞革命武裝力量.md "wikilink")
  - [科索沃解放軍](../Page/科索沃解放軍.md "wikilink")

## 犯罪

2011年12月13日[欧洲中部时间](../Page/欧洲中部时间.md "wikilink")12:30（[UTC+01:00](../Page/UTC+1.md "wikilink")）左右，凶手[诺尔丁·阿姆拉尼](../Page/诺尔丁·阿姆拉尼.md "wikilink")（）
手持**FN
FAL自動步槍**在圣朗贝尔广场附近一个面包屋屋顶向購物人群亂槍掃射，并向广场附近公車站投掷3颗[手榴弹](../Page/手榴弹.md "wikilink")，犯人最終在犯罪现场吞槍自盡，另有6人死亡、123人受傷。

## 流行文化

### [電視劇](../Page/電視劇.md "wikilink")

  - 《[陰屍路系列](../Page/陰屍路.md "wikilink")》（The Walking Dead）
  - 《[勇者逆襲系列](../Page/勇者逆襲.md "wikilink")》（Strike Back）

### 電子遊戲

  - 2007年—《[穿越火线](../Page/穿越火线.md "wikilink")》（Crossfire）：型號為FAL，以GP出售的自动步枪之姿出现，全自动射击并使用20发[弹匣供弹](../Page/弹匣.md "wikilink")。无军衔限制，使用18000GP方可购买并永久使用。拥有救世主专用改型，使用迷彩涂装，加装反射式瞄准镜（不可使用）与[M203榴弹发射器](../Page/M203榴弹发射器.md "wikilink")（奇怪的1次装填可发射3发榴弹）并将单弹匣载弹量扩容至30发，只在救世主模式中被选定为救世主的玩家方可使用。
  - 2008年—《[-{zh-cn:孤島求生2;
    zh-tw:極地戰嚎2;}-](../Page/孤島驚魂2.md "wikilink")》：命名為“FN
    FAL”，使用黑色塑料製[護木](../Page/護木.md "wikilink")、手槍握把和[槍托](../Page/槍托.md "wikilink")。武器模組採用鏡像佈局（拋殼口在左邊但拉機柄卻在右邊）。
  - 2009年—《[-{zh-hans:使命召唤：现代战争; zh-hk:決勝時刻：現代戰爭;
    zh-tw:決勝時刻：現代戰爭;}-2](../Page/決勝時刻：現代戰爭2.md "wikilink")》（Call
    of Duty: Modern Warfare 2）：型號為DSA SA58
    OSW，命名為「FAL」，使用30發彈匣供彈但聯機模式中預設載彈量為20發，只能半自動射擊。故事模式中被[俄羅斯](../Page/俄羅斯.md "wikilink")[極端民族主義黨武裝力量](../Page/極端民族主義.md "wikilink")、阿富汗反對勢力和[巴西武裝團匪所使用](../Page/巴西.md "wikilink")。聯機模式時於等級28解鎖，並可使用[榴彈發射器](../Page/M203榴彈發射器.md "wikilink")、[下掛式霰彈槍](../Page/Masterkey霰彈槍.md "wikilink")、[紅點鏡](../Page/紅點鏡.md "wikilink")、[全息瞄準鏡](../Page/全像武器照準器.md "wikilink")、[消音器](../Page/抑制器.md "wikilink")、[心跳探測儀](../Page/心跳探測儀.md "wikilink")、[ACOG光學瞄準鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")、[熱能探測式瞄具](../Page/熱輻射.md "wikilink")、及延長彈匣（增至30發）。
  - 2010年—《[-{zh-hans:使命召唤：黑色行动; zh-hk:使命召喚：黑色行動;
    zh-tw:決勝時刻：黑色行動;}-](../Page/決勝時刻：黑色行動.md "wikilink")》（Call
    of Duty: Black
    Ops）：型號為FAL，命名為「FAL」，使用木製[護木](../Page/護木.md "wikilink")、手槍握把和[槍托](../Page/槍托.md "wikilink")，只能半自動射擊。故事模式中被[古巴軍隊和](../Page/古巴軍事.md "wikilink")[北越軍隊所使用](../Page/越南人民軍.md "wikilink")。在聯機模式時於等級32解鎖，能夠使用[榴彈發射器](../Page/M203榴彈發射器.md "wikilink")、延長彈匣（增至30發）、[雙彈匣](../Page/彈匣並聯.md "wikilink")、[火焰噴射器](../Page/火焰噴射器.md "wikilink")、[紅點鏡](../Page/紅點鏡.md "wikilink")、[下掛式霰彈槍](../Page/Masterkey槍管下掛式霰彈槍.md "wikilink")、[ACOG光學瞄準鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")、[紅外線探測式瞄具](../Page/紅外線.md "wikilink")、[消音器及反射式瞄準鏡](../Page/抑制器.md "wikilink")。在殭屍模式中有一款稱為**EPC
    WN**的改型。
  - 2012年—《[-{zh-hans:使命召唤：黑色行动; zh-hk:使命召喚：黑色行動;
    zh-tw:決勝時刻：黑色行動;}-II](../Page/決勝時刻：黑色行動II.md "wikilink")》（Call
    of Duty: Black Ops II）：型號為FAL和DSA SA58 OSW：
      - FAL命名為「FAL」，使用木製[護木](../Page/護木.md "wikilink")、手槍握把和[槍托](../Page/槍托.md "wikilink")，預設為全自動射擊。只在故事模式和殭屍模式中登場，故事模式中被[美國中央情報局](../Page/美國中央情報局.md "wikilink")[特別活動分部特工亞歷克斯](../Page/特別活動分部.md "wikilink").梅森、傑森·哈迪森、[爭取安哥拉徹底獨立全國聯盟](../Page/爭取安哥拉徹底獨立全國聯盟.md "wikilink")、[安哥拉人民解放運動](../Page/安哥拉人民解放運動.md "wikilink")、梅嫩德斯卡特爾、[巴拿馬國防軍和](../Page/巴拿馬國防軍.md "wikilink")所使用，並是玩家一開始就能夠使用的武器之一。能夠使用：反射式瞄準鏡、[ACOG光學瞄準鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")、[抑制器](../Page/抑制器.md "wikilink")、延長彈匣（增至30發）、[快速重裝彈匣](../Page/彈匣並聯.md "wikilink")、[榴彈發射器以及](../Page/M203榴彈發射器.md "wikilink")[擊發調變式](../Page/擊發調變槍械.md "wikilink")（轉換到半自動射擊）。
      - DSA SA58 OSW命名為「FAL
        OSW」，載彈量25發，預設為半自動射擊（單人模式預設為全自動射擊）。故事模式之中完成「與我一起受苦」（Suffer
        With
        Me）戰役以後解鎖，被[僱傭兵和](../Page/僱傭兵.md "wikilink")[三軍情報局所使用](../Page/三軍情報局.md "wikilink")；聯機模式時於等級22解鎖，並可以使用[反射式瞄準鏡](../Page/紅點鏡.md "wikilink")、快速抽出握把、[快速重裝彈匣](../Page/彈匣並聯.md "wikilink")、[ACOG光學瞄準鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")、[前握把](../Page/輔助握把.md "wikilink")、可調節[槍托](../Page/槍托.md "wikilink")、目標搜索器、[激光瞄準器](../Page/激光瞄準器.md "wikilink")、[擊發調變](../Page/擊發調變槍械.md "wikilink")（單人模式轉換為半自動；聯機模式則轉換為全自動）、[EOTech全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")、[消音器](../Page/抑制器.md "wikilink")、、[混合式光學瞄準鏡](../Page/Leupold_HAMR瞄準鏡.md "wikilink")、[延長彈匣](../Page/彈匣.md "wikilink")、[榴彈發射器](../Page/M320榴彈發射器.md "wikilink")、毫米波掃描器。
  - 2012年—《[战争前线](../Page/战争前线.md "wikilink")》（Warface）：型号为DSA SA58
    OSW普通型、长枪管型与[IMBEL IA2](../Page/IMBEL_IA2突擊步槍.md "wikilink")；
      - DSA SA58 OSW普通型命名为“FN FAL
        DSA-58”，铁灰色枪身，35发弹匣，为步枪手专用武器。只能通过抽奖获得，可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、突击消音器、[突击制退器](../Page/炮口制动器.md "wikilink")、[突击刺刀](../Page/刺刀.md "wikilink")）、战术导轨配件（[通用握把](../Page/辅助握把.md "wikilink")、突击握把、突击握把架、[枪挂型榴弹发射器](../Page/FN_EGLM附加型榴弹发射器.md "wikilink")、FN
        FAL DSA-58战术握把）以及瞄准镜（[EoTech
        553全息瞄准镜](../Page/全息瞄准镜.md "wikilink")、绿点全息瞄准镜、[ELCAN
        SpecterDS瞄准镜](../Page/C79光学瞄准镜.md "wikilink")、[红点瞄准镜](../Page/红点镜.md "wikilink")、[步枪高级瞄准镜](../Page/ELCAN_SpecterDR光学瞄准镜.md "wikilink")、[Trijicon
        ACOG瞄准镜](../Page/先進戰鬥光學瞄準鏡.md "wikilink")），默认安装加装了[激光瞄准器的](../Page/激光瞄准器.md "wikilink")
        M900战术握把（游戏内被命名为FN FAL DSA-58战术握把）。
      - DSA SA58 OSW长枪管型命名为“DSA SA58
        SPR”，黑色枪身，20发[弹匣](../Page/弹匣.md "wikilink")。为狙击手专用武器，只能通过抽奖获得，可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、狙击枪消音器、[狙击枪制退器](../Page/炮口制动器.md "wikilink")）、战术导轨配件（[狙击枪双脚架](../Page/两脚架.md "wikilink")、特殊狙击枪双脚架）以及瞄准镜（[狙击枪5.5x瞄准镜](../Page/瞄准镜.md "wikilink")、狙击枪4.5x中程瞄准镜、[狙击枪4x短程瞄准镜](../Page/Trijicon_TR20瞄准镜.md "wikilink")、狙击槍5x快速瞄准镜）。
          - 有趣的是游戏内该枪为半自动狙击步枪，却拥有着手动狙击步枪才拥有的击倒功能，同时该武器的射速是所有半自动狙击枪中最慢的射速。
      - IMBEL IA2命名为“IMBEL
        IA2”，下机匣、弹匣与[机械瞄具为沙色涂装](../Page/机械瞄具.md "wikilink")，30发弹匣，为步枪手专用武器，在商城内以K点贩卖，并可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、突击消音器、[突击制退器](../Page/炮口制动器.md "wikilink")、[突击刺刀](../Page/刺刀.md "wikilink")）、战术导轨配件（[通用握把](../Page/辅助握把.md "wikilink")、突击握把、突击握把架、[枪挂型榴弹发射器](../Page/FN_EGLM附加型榴弹发射器.md "wikilink")）以及瞄准镜（[EoTech
        553全息瞄准镜](../Page/全息瞄准镜.md "wikilink")、绿点全息瞄准镜、[ELCAN
        SpecterDS瞄准镜](../Page/C79光学瞄准镜.md "wikilink")、[红点瞄准镜](../Page/红点镜.md "wikilink")、[步枪高级瞄准镜](../Page/ELCAN_SpecterDR光学瞄准镜.md "wikilink")、[Trijicon
        ACOG瞄准镜](../Page/先進戰鬥光學瞄準鏡.md "wikilink")）。
  - 2014年—《》（Insurgency）：型號為FAL及L1A1 SLR，前者為叛軍專用武器，後者為安全部隊專用。
  - 2015年—《[-{zh-hans:战地：硬仗;
    zh-hant:戰地風雲：強硬路線;}-](../Page/战地：硬仗.md "wikilink")》（Battlefield
    Hardline）：型號為FAL及DSA SA58 OSW：
      - FAL命名為「FAL」，為資料片「犯罪活動」新增的資料片獨有武器，歸類為[戰鬥步槍](../Page/戰鬥步槍.md "wikilink")，20+1發彈匣，被匪方執行者（Enforcer）所使用（警察解鎖條件為：以任何陣營進行遊戲使用該槍擊殺1250名敵人後購買武器執照），價格為$75,000，預設配備補償器。原裝使用木製[護木](../Page/護木.md "wikilink")、手槍握把和[槍托](../Page/槍托.md "wikilink")，使用“槍托”改裝後會換成黑色塑料部件。可加裝各種瞄準鏡（[反射](../Page/紅點鏡.md "wikilink")、眼鏡蛇、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")（放大1倍）、PKA-S（放大1倍）、Micro
        T1、SRS 02、[Comp
        M4S](../Page/Aimpoint_Comp_M4紅點鏡.md "wikilink")、[M145](../Page/C79光學瞄準鏡.md "wikilink")（放大3.4倍）、PO（放大3.5倍）
        、[ACOG](../Page/先進戰鬥光學瞄準鏡.md "wikilink")（放大4倍）、[PSO-1](../Page/PSO-1光學瞄準鏡.md "wikilink")（放大4倍）、[IRNV](../Page/夜視儀.md "wikilink")（放大1倍）、[FLIR](../Page/熱成像儀.md "wikilink")（放大2倍））、附加配件（傾斜式[機械瞄具](../Page/機械瞄具.md "wikilink")、[電筒](../Page/電筒.md "wikilink")、戰術燈、[激光瞄準器](../Page/激光指示器.md "wikilink")、延長彈匣、[穿甲](../Page/穿甲彈.md "wikilink")[曳光彈](../Page/曳光彈.md "wikilink")、槍托）、槍口零件（[槍口制退器](../Page/砲口制動器.md "wikilink")、補償器、重[槍管](../Page/槍管.md "wikilink")、[抑制器](../Page/抑制器.md "wikilink")、消焰器）及[握把](../Page/輔助握把.md "wikilink")（垂直握把、粗短握把、[直角握把](../Page/直角前握把.md "wikilink")）。
      - DSA SA58 OSW命名為「SA-58
        OSW」，歸類為[戰鬥步槍](../Page/戰鬥步槍.md "wikilink")，20+1發彈匣，被匪方執行者（Enforcer）所使用（警察解鎖條件為：以任何陣營進行遊戲使用該槍擊殺1250名敵人後購買武器執照），價格為$50,000。可加裝各種瞄準鏡（[反射](../Page/紅點鏡.md "wikilink")、眼鏡蛇、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")（放大1倍）、PKA-S（放大1倍）、Micro
        T1、SRS 02、[Comp
        M4S](../Page/Aimpoint_Comp_M4紅點鏡.md "wikilink")、[M145](../Page/C79光學瞄準鏡.md "wikilink")（放大3.4倍）、PO（放大3.5倍）
        、[ACOG](../Page/先進戰鬥光學瞄準鏡.md "wikilink")（放大4倍）、[PSO-1](../Page/PSO-1光學瞄準鏡.md "wikilink")（放大4倍）、[IRNV](../Page/夜視儀.md "wikilink")（放大1倍）、[FLIR](../Page/熱成像儀.md "wikilink")（放大2倍））、附加配件（傾斜式[機械瞄具](../Page/機械瞄具.md "wikilink")、[電筒](../Page/電筒.md "wikilink")、戰術燈、[激光瞄準器](../Page/激光指示器.md "wikilink")、[穿甲](../Page/穿甲彈.md "wikilink")[曳光彈](../Page/曳光彈.md "wikilink")）、槍口零件（[槍口制退器](../Page/砲口制動器.md "wikilink")、補償器、重[槍管](../Page/槍管.md "wikilink")、[抑制器](../Page/抑制器.md "wikilink")、消焰器）及[握把](../Page/輔助握把.md "wikilink")（垂直握把、粗短握把、[直角握把](../Page/直角前握把.md "wikilink")）。劇情模式當中則能夠被主角尼古拉斯·門多薩所使用。
  - 2015年—《[-{zh-hans:彩虹六号：围攻;
    zh-hant:虹彩六號：圍攻行動;}-](../Page/虹彩六號：圍攻行動.md "wikilink")》（Rainbow
    Six: Siege）：型號為C1A1和MBEL
    M964A1，前者命名為「CAMRS」，在Y1S1黑冰行动加入，被[第二联合特遣部队](../Page/第二联合特遣部队.md "wikilink")（JTF2）所使用；後者命名為「PARA-308」在Y1S3骷髅雨行动加入，並被[特別警察行動營](../Page/特別警察行動營.md "wikilink")（BOPE）所使用。
  - 2016年—《[-{zh-hans:使命召唤：现代战争; zh-hk:決勝時刻：現代戰爭;
    zh-tw:決勝時刻：現代戰爭;}-重製版](../Page/決勝時刻4：現代戰爭.md "wikilink")》（Call
    of Duty: Modern Warfare
    Remastered）：於2017年2月7日添加到遊戲內，型號為LAR，命名為「XM-LAR」，使用DSA
    SA58
    OSW的[護木和拉機柄](../Page/護木.md "wikilink")，只於聯機模式登場，可全自動射擊，30發[彈匣](../Page/彈匣.md "wikilink")，初始攜彈量為60發（聯機模式），最高攜彈量為180發（聯機模式）。聯機模式時可以使用[榴彈發射器](../Page/M203榴彈發射器.md "wikilink")、[紅點鏡](../Page/紅點鏡.md "wikilink")、[消音器及](../Page/抑制器.md "wikilink")[ACOG光學瞄準鏡](../Page/ACOG光學瞄準鏡.md "wikilink")。
  - 2016年（臺版2017年）—
    《[少女前線](../Page/少女前线.md "wikilink")》：命名為FAL，為五星戰術人形，可透過工廠製造。
  - 2016年—《[逃離塔科夫](../Page/逃離塔科夫.md "wikilink")》:型號為SA-58
    OSW，可更改槍口、槍管、槍托、握把、彈夾、瞄準器等等的配件
  - 2018年 —
    《[绝地求生](../Page/绝地求生.md "wikilink")》：命名为“自动装填步枪”（英文："SLR"），为半自动版本。雖然名為自動裝填步槍，實則是被定位成一把精確射手步槍（Designated
    Marksman
    Rifle，簡稱：DMR），使用7.62mm口徑子彈，可配備：所有步槍槍口及彈匣、所有瞄具、托腮板。有10、20發彈匣（後者為使用擴容彈匣）

### [輕小說](../Page/輕小說.md "wikilink")

  - 2014年—《[刀劍神域外傳Gun Gale
    Online](../Page/刀劍神域外傳Gun_Gale_Online.md "wikilink")》：FAL傘兵型被“Narrow”小隊所使用；FAL標準型被“NSS”小隊的[羅德西亞傭兵造型玩家所使用](../Page/羅德西亞.md "wikilink")。

## 參考

  - [Fabrique Nationale](../Page/Fabrique_Nationale.md "wikilink")
  - [EM-2突擊步槍](../Page/EM-2突擊步槍.md "wikilink")
  - [M14自動步槍](../Page/M14自動步槍.md "wikilink")
  - [AR-10自動步槍](../Page/AR-10自動步槍.md "wikilink")
  - [SIG SG 510自動步槍](../Page/SIG_SG_510自動步槍.md "wikilink")（Sturmgewehr
    57）
  - [HK G3自動步槍](../Page/HK_G3自動步槍.md "wikilink")
  - [FN CAL突擊步槍](../Page/FN_CAL突擊步槍.md "wikilink")
  - [FN FNC突擊步槍](../Page/FN_FNC突擊步槍.md "wikilink")
  - [各國軍隊制式步槍列表](../Page/各國軍隊制式步槍列表.md "wikilink")
  - [L1A1自动装弹步枪](../Page/L1A1自动装弹步枪.md "wikilink")

## 參考來源

<div class="references-small">

<references />

  - Ezell, 1988
  - [remtek.com-FN FAL](http://www.remtek.com/arms/fn/fal/)
  - Afonso, Aniceto and Gomes, Carlos de Matos, Guerra Colonial, 2000
  - Ezell, Clinton, Small Arms of the World, Stackpole Books (1983)
  - Pikula, Maj. Sam, The Armalite AR-10, 1998
  - Stevens, R. Blake, The FAL Rifle, Collector Grade Publications
    (1993)

</div>

## 外部連結

  - —[D Boy's GunWorld（FN
    FAL系列）](http://firearmsworld.net/fn/fal/fal.htm)

[Category:導氣式槍械](../Category/導氣式槍械.md "wikilink")
[Category:自动步枪](../Category/自动步枪.md "wikilink")
[Category:戰鬥步槍](../Category/戰鬥步槍.md "wikilink")
[Category:7.62×51毫米槍械](../Category/7.62×51毫米槍械.md "wikilink")
[Category:比利時槍械](../Category/比利時槍械.md "wikilink")
[Category:越戰武器](../Category/越戰武器.md "wikilink")
[FN_FAL衍生槍](../Category/FN_FAL衍生槍.md "wikilink")

1.  <http://firearmsworld.net/fn/fal/confor.htm>

2.  [德國的公制式FAL](http://firearmsworld.net/fn/fal/german.htm)

3.  [新華網軍事頻道-委內瑞拉訂購的第三批AK-103突擊步槍月底將交付](http://news.xinhuanet.com/mil/2006-11/20/content_5354131.htm)

4.  Jones, Richard D. *Jane's Infantry Weapons 2009/2010*. Jane's
    Information Group; 35th edition (January 27, 2009). ISBN
    978-0-7106-2869-5.

5.

6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21. <http://books.google.com/books?id=cchLfXrXpH4C&pg=PA62&lpg=PA62>

22.
23.
24.
25.
26.
27.
28.
29.
30.
31.
32.
33.

34.
35.
36.
37.
38.
39.

40.
41.
42.
43.
44.
45.
46.
47.
48.
49.
50.
51.

52.
53.
54.
55.
56.

57.
58.
59.
60.
61.
62.
63.
64.
65.
66.
67.
68.
69. [1](https://m.youtube.com/watch?v=jlyi-iLpgmI)

70.
71.
72.
73.
74. <http://www.thefreelibrary.com/The+military+rifle+cartridges+of+Rhodesia+Zimbabwe%3A+from+Cecil+Rhodes>...-a0234316416