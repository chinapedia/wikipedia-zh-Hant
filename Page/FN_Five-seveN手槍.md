**FN Five-seveN**是[比利時](../Page/比利時.md "wikilink")[Fabrique
Nationale設計製造的](../Page/Fabrique_Nationale.md "wikilink")[半自動手槍](../Page/半自動手槍.md "wikilink")。

## 歷史

Five-seveN是配合[FN
P90而研發的手鎗](../Page/FN_P90衝鋒槍.md "wikilink")。其名稱來自其使用的子彈直徑5.7毫米，同時第一個及最後一個字母以大寫強調是FN的產品。

因為P90所用的子彈是全新研制，不能用於現有的手鎗，為使有手鎗可以與P90同用同一款子彈，所以需要有Five-seveN與之配合，使整個武器系統完整。為使全新的子彈能放進手鎗內，1993年，FN把SS90子彈的彈頭改短了2.7毫米，並由[塑料彈頭改用較重的](../Page/塑料.md "wikilink")[鋁或](../Page/鋁.md "wikilink")[鋼製彈頭](../Page/鋼.md "wikilink")。此新子彈稱為SS190，可同時適用於原有的P90及新研發的Five-seveN，但只銷售給軍方及執法部門。

直到2004年，FN公司推出Five-seveN
IOM版給民用市場，IOM版加裝了[M1913導軌](../Page/皮卡汀尼導軌.md "wikilink")，彈匣保險裝置，可任意調整的準星等。FN公司同年也推出了USG版本以取代IOM版本，USG版本有其他進一步的小改良，被美國[美國菸酒槍炮及爆裂物管理局](../Page/美國菸酒槍炮及爆裂物管理局.md "wikilink")(ATF)認證為運動鎗械。

## 設計

Five-seveN有重量輕、低後座力、高容量彈匣與體積小的優點。由於使用了跟P90一樣的[5.7×28子彈](../Page/5.7×28.md "wikilink")，所以擁有相當的擊穿防彈裝備的能力，攜彈量也較一般手鎗多。

Five-seveN的可卸式[彈匣除了](../Page/彈匣.md "wikilink")10和20發外，還有30發的延長版本。

槍機採延遲式後坐、非剛性閉鎖、隐蔽式[击锤擊發](../Page/击锤.md "wikilink")，[手槍套筒上使用複合結構](../Page/手槍套筒.md "wikilink")，支架用鋼板沖壓成形，擊針室則以機械加工，用固定銷固定在支架上，外面覆上高強度工程塑料，表面並經過磷化處理。

### 子彈

[57lineup.jpg](https://zh.wikipedia.org/wiki/File:57lineup.jpg "fig:57lineup.jpg")子彈\]\]
[FN5711.jpg](https://zh.wikipedia.org/wiki/File:FN5711.jpg "fig:FN5711.jpg")\]\]

Five-sevenN低於手鎗子彈的[後座力及對個人防彈裝備有高的穿透力全靠](../Page/後座力.md "wikilink")5.7×28毫米（SS190）子彈。

SS190的槍口初速高達716 m/s，可以輕易的穿透Level
IIIA級防彈背心，甚至四級甚至於五級防護能力[防彈背心](../Page/防彈背心.md "wikilink")。SS190的總能量與[9毫米相若](../Page/9毫米鲁格弹.md "wikilink")，但速度高一倍，後座力只是9毫米的70%，重量較9毫米輕，彈頭較尖、長。

現在，生產5.7 ×2 8毫米子彈的除了[赫爾斯塔爾國營工廠](../Page/赫爾斯塔爾國營工廠.md "wikilink")(FN
Herstal)外，還有。

#### 北約評估

[4.6x30mm,_5.7x28mm,_.30_M1_Carbine.jpg](https://zh.wikipedia.org/wiki/File:4.6x30mm,_5.7x28mm,_.30_M1_Carbine.jpg "fig:4.6x30mm,_5.7x28mm,_.30_M1_Carbine.jpg")、[5.7×28mm及](../Page/5.7×28mm.md "wikilink")[.30
Carbine](../Page/.30卡賓槍彈.md "wikilink")\]\]
現今彈藥中，性能與SS190（[5.7×28mm](../Page/5.7×28mm.md "wikilink")）相當的只有[H\&K的](../Page/H&K.md "wikilink")[4.6×30mm](../Page/4.6×30mm.md "wikilink")。為了標準化個人防衛武器的彈藥，北約對此兩種子彈作比對性測試，以選其一取代9x19mm\[1\]，測試小組由美國、加拿大、法國及英國等多個專家組成\[2\]，分ETBS及QRT兩工作組。結論是5.7×28毫米的性能明顯比4.6×30毫米高\[3\]，最重要的分別是：
\* 兩款子彈對有穿著防彈裝備的目標有相同的效率，但對無防彈裝備的目標時，5.7×28毫米的效率高27%。\[4\]

  - 在極端溫度下，5.7×28毫米受的影響較低。\[5\]
  - 5.7×28毫米對鎗管的損害較低。\[6\]
  - 5.7×28毫米的設計及生產較接近[5.56×45mm
    NATO子彈](../Page/5.56×45mm_NATO.md "wikilink")，方便以現有生產線大量生產。\[7\]
  - 5.7×28毫米系鎗械較成熟，對鎗械設計要求也較低，同時已有手鎗設計\[8\]\[9\]。

因此北約建議採用5.7×28毫米作標準，但研發4.6×30毫米的德國拒絕接受因而制停了整個標準化程序\[10\]\[11\]

## 使用國

<table>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p>組織名稱</p></th>
<th><p>型號</p></th>
<th><p>數量</p></th>
<th><p>採購年份</p></th>
<th><p>參考資料</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/比利時國防軍.md" title="wikilink">比利時國防軍</a></p></td>
<td><p><em>Mk 2</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[12][13][14]</p></td>
</tr>
<tr class="odd">
<td><p>特別單位</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[15]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/特種部隊群.md" title="wikilink">特種部隊群</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[16][17][18]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/列日.md" title="wikilink">列日</a><a href="../Page/警察.md" title="wikilink">警察部隊</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[19][20]</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[21]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/特種部隊.md" title="wikilink">特種部隊單位</a></p></td>
<td><p><em>Five-seveN</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/塞浦路斯國民警衛隊.md" title="wikilink">塞浦路斯國民警衛隊特種部隊單位</a></p></td>
<td><p><em>Five-seveN</em></p></td>
<td><p>250</p></td>
<td><p>2000</p></td>
<td><p>[22]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/對外安全總局.md" title="wikilink">對外安全總局</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[23]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/法國國家憲兵.md" title="wikilink">法國國家憲兵</a><a href="../Page/國家憲兵干預組.md" title="wikilink">干預組</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[24][25][26]</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[27]</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/希臘警察.md" title="wikilink">希臘警察</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[28][29]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>民間情報總局</p></td>
<td><p><em>USG</em></p></td>
<td><p>12</p></td>
<td><p>2008</p></td>
<td><p>[30]</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><em>USG</em></p></td>
<td><p>—</p></td>
<td><p>2008</p></td>
<td><p>[31]</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[32]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/印尼陸軍.md" title="wikilink">印尼陸軍</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[33]</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/義大利陸軍.md" title="wikilink">義大利陸軍</a></p></td>
<td><p><em>USG</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[34]</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><em>USG</em></p></td>
<td><p>367</p></td>
<td><p>2008</p></td>
<td><p>[35][36]</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[37]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[38][39]</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[40]</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[41]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/三軍情報局.md" title="wikilink">三軍情報局</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/秘魯武裝部隊.md" title="wikilink">秘魯武裝部隊特種部隊單位</a></p></td>
<td><p><em>USG</em></p></td>
<td><p>—</p></td>
<td><p>2009</p></td>
<td><p>[42][43]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/波蘭軍事.md" title="wikilink">波蘭武裝部隊</a><a href="../Page/行動應變及機動組.md" title="wikilink">行動應變及機動組</a></p></td>
<td><p><em>USG</em></p></td>
<td><p>—</p></td>
<td><p>2007</p></td>
<td><p>[44]</p></td>
</tr>
<tr class="even">
<td><p>中央調查局</p></td>
<td><p><em>Mk 2</em></p></td>
<td><p>—</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/沙特阿拉伯武裝部隊.md" title="wikilink">沙特阿拉伯武裝部隊</a></p></td>
<td><p><em>USG</em></p></td>
<td><p>12,000</p></td>
<td><p>2007</p></td>
<td><p>[45]</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>500</p></td>
<td><p>—</p></td>
<td><p>[46][47]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/西班牙武裝部隊.md" title="wikilink">西班牙武裝部隊</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[48]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波蘇埃洛.md" title="wikilink">波蘇埃洛</a>-<a href="../Page/阿拉爾孔.md" title="wikilink">阿拉爾孔警察部隊</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[49]</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>未指定的安全部隊</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[50]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/泰國皇家陸軍.md" title="wikilink">泰國皇家陸軍</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/美國特勤局.md" title="wikilink">美國特勤局</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[51]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/佐治亞州.md" title="wikilink">佐治亞州</a><a href="../Page/德盧斯.md" title="wikilink">德盧斯在內多個警察部門</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[52]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/新澤西州.md" title="wikilink">新澤西州</a><a href="../Page/巴賽克縣.md" title="wikilink">巴賽克縣警察部門</a><a href="../Page/特種武器和戰術部隊.md" title="wikilink">特種武器和戰術部隊</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 衍生型

[FN57mag01.JPG](https://zh.wikipedia.org/wiki/File:FN57mag01.JPG "fig:FN57mag01.JPG")

### Five-seveN

為第一代Five-seveN；採用雙動扳機，無保險裝置，具有弧形護弓，不可調式照門；軌道上有產品編號，只能安裝專用的光學指示器，目前已經停止生產。

### Five-seveN Tactical

採用單動扳機，有保險裝置。

### Five-seveN IOM

2004年上市，IOM的意思是指官員個人型民用型（Individual Officer Model）；採用為可調式照門，彈匣聯動式保險裝置。

### Five-seveN USG

USG是「美國政府」（United States Government）的縮寫，於2005年取代IOM；採用：

  - 可調式照門，準星加高，彈匣採聯動式保險裝置，彈匣釋放鈕可左右對換，主要使用穿透能力較低的的空尖彈或軟尖彈
  - 具有傳統平直式設計護弓，並將軌道規格修正，可通用其他配備

### Five-seveN MK2

最近的機型，是現時FN提供的標準版Five-seveN；採用一體式的金屬滑蓋，可調式和經改良照門和準星；為[比利時國防軍制式手槍](../Page/比利時國防軍.md "wikilink")。

## 犯罪用途

FN
Five-seveN在[墨西哥被廣泛的用於販毒和其他犯罪用途](../Page/墨西哥.md "wikilink")，原因是其發射的5.7毫米口徑子彈能夠輕易地貫穿警察的[防彈衣](../Page/防彈衣.md "wikilink")，而且要從美國走私到當地也相對容易。因此該槍也有著「警察殺手」（Cop
Killer）的惡名。而在[中國](../Page/中國.md "wikilink")[東莞警方也曾在毒販身上繳獲過FN](../Page/東莞.md "wikilink")
Five-seveN\[53\]。

[2009年胡德堡槍擊案當中](../Page/2009年胡德堡槍擊案.md "wikilink")，[美國陸軍](../Page/美國陸軍.md "wikilink")[少校及精神科醫師](../Page/少校.md "wikilink")在美國[得克薩斯州](../Page/得克薩斯州.md "wikilink")[基林市附近的美國陸軍](../Page/基林_\(得克薩斯州\).md "wikilink")[胡德堡基地使用FN](../Page/胡德堡.md "wikilink")
Five-seveN手槍向士兵施襲，案件中造成13人死，30多人受傷。

## 轶事

由于在中国大陸民间中，大部分民众是从电子游戏《[-{zh-cn:反恐精英;
zh-hk:絕對武力}-](../Page/反恐精英.md "wikilink")》中首次得知此枪的存在，对于此枪不甚了解，而**Five-seveN**可以直接翻译为**5-7**，所以部分人就望文生义，把此枪称为**“57式手枪”**。

## 流行文化

### 電影

  - 2006年—《[-{zh-tw:快克殺手;
    zh-cn:怒火攻心;}-](../Page/快克殺手.md "wikilink")》（Crank）：型號為Five-seveN
    USG，使用[黑色](../Page/黑色.md "wikilink")[底把及](../Page/机匣.md "wikilink")[套筒](../Page/手槍套筒.md "wikilink")，裝上[抑制器和](../Page/抑制器.md "wikilink")[激光指示器](../Page/激光指示器.md "wikilink")，於回憶中被主角契夫·查理奧斯（[傑森·史塔森飾演](../Page/傑森·史塔森.md "wikilink")）準備暗殺唐·金時所使用。
  - 2009年—《[-{zh-tw:特種部隊：眼鏡蛇的崛起; zh-hk:義勇群英之毒蛇風暴;
    zh-cn:特种部队：眼镜蛇的崛起;}-](../Page/特種部隊：眼鏡蛇的崛起.md "wikilink")》（G.I.
    Joe: The Rise of
    Cobra）：為第一代Five-seveN，使用[黑色](../Page/黑色.md "wikilink")[底把及](../Page/机匣.md "wikilink")[套筒](../Page/手槍套筒.md "wikilink")，被[蛇眼](../Page/蛇眼_\(特種部隊\).md "wikilink")（[雷·帕克飾演](../Page/雷·帕克.md "wikilink")）所使用。
  - 2015年—《[-{zh-tw:怒火邊界; zh-hk:毒裁者;
    zh-cn:边境杀手;}-](../Page/毒裁者.md "wikilink")》（Sicario）：型號為Five-seveN
    USG，使用[黑色](../Page/黑色.md "wikilink")[底把及](../Page/机匣.md "wikilink")[套筒](../Page/手槍套筒.md "wikilink")，由曼努埃爾·迪亞斯（Bernardo
    Saracino飾演）所持有，直至其被解取武裝。
  - 2017年—《》（Stratton）：型號為Five-seveN
    USG，使用[黑色](../Page/黑色.md "wikilink")[底把及](../Page/机匣.md "wikilink")[套筒](../Page/手槍套筒.md "wikilink")，被格里戈里·巴羅夫斯基（[湯瑪斯·柯瑞奇曼飾演](../Page/湯瑪斯·柯瑞奇曼.md "wikilink")）所使用。

### 電子遊戲

  - 1999年—《[-{zh-hans:反恐精英;
    zh-hant:絕對武力;}-](../Page/反恐精英.md "wikilink")》（Counter-Strike）：型號為第一代Five-seveN，命名為「ES
    Five-Seven」，使用淺[黑色](../Page/黑色.md "wikilink")[底把及](../Page/机匣.md "wikilink")[套筒](../Page/手槍套筒.md "wikilink")，為反恐小組的專用手槍。
  - 2003年—《[-{zh-hans:反恐精英：零点行动;
    zh-hant:絕對武力：一觸即發;}-](../Page/絕對武力：一觸即發.md "wikilink")》（Counter-Strike:
    Condition Zero）：型號為第一代Five-seveN，命名為「ES
    Five-Seven」，使用深[黑色](../Page/黑色.md "wikilink")[底把及](../Page/机匣.md "wikilink")[套筒](../Page/手槍套筒.md "wikilink")，為反恐小組的專用手槍。
  - 2004年—《[-{zh-hans:反恐精英：起源;
    zh-hant:絕對武力：次世代;}-](../Page/絕對武力：次世代.md "wikilink")》（Counter-Strike:
    Source）：同上。
  - 2005年—《[-{zh-hans:生化危机;
    zh-hant:惡靈古堡;}-4](../Page/生化危机4.md "wikilink")》（バイオハザード4、Biohazard
    4、Resident Evil 4）：命名为“惩罚者”（Punisher），奇怪地使用9mm手枪弹，升级后为28发弹匣供弹。
  - 2007年—《[-{zh-hans:反恐精英Online;
    zh-hant:絕對武力Online;}-](../Page/絕對武力Online.md "wikilink")》（Counter-Strike
    Online）：隨遊戲登場武器，型號為第一代Five-seveN，命名為「ES
    Five-Seven」，使用深[黑色](../Page/黑色.md "wikilink")[底把及](../Page/机匣.md "wikilink")[套筒](../Page/手槍套筒.md "wikilink")，為反恐小組專用手槍。
  - 2010年—《[-{zh-hans:细胞分裂：断罪;
    zh-hant:縱橫諜海：斷罪;}-](../Page/细胞分裂：断罪.md "wikilink")》：随剧情解锁武器，可升级瞄准镜，下挂红点瞄准器，發射穿甲弹。
  - 2011年—《[-{zh:決勝時刻：現代戰爭3;zh-hans:使命召唤：现代战争3;zh-hant:決勝時刻：現代戰爭3;zh-cn:使命召唤：现代战争3;zh-tw:決勝時刻：現代戰爭3;zh-hk:決勝時刻：現代戰爭3;zh-mo:使命召喚：現代戰爭3;}-](../Page/決勝時刻：現代戰爭3.md "wikilink")》（Call
    of Duty: Modern Warfare 3）：型號為Five-seveN
    USG，命名为“Five-Seven”，使用[黑色](../Page/黑色.md "wikilink")[底把及](../Page/机匣.md "wikilink")[套筒](../Page/手槍套筒.md "wikilink")，單人模式時可裝彈15發，聯機模式時則為16發。在故事模式之中被[美國陸軍](../Page/美國陸軍.md "wikilink")[三角洲特種部隊合金分隊](../Page/三角洲部隊.md "wikilink")（包括主角德里克·“寒霜”威斯布魯克）、舊[俄羅斯政府軍](../Page/俄羅斯.md "wikilink")（包括主角尤里）、[極端民族主義黨武裝力量](../Page/極端民族主義.md "wikilink")、同心圈成員（包括其領袖弗拉基米爾·馬卡洛夫）及俄羅斯[聯邦警衛局](../Page/聯邦警衛局.md "wikilink")（包括玩家扮演角色安德烈·哈爾科夫）所使用。聯機模式於等級58解鎖，而在生存模式則於等級1解鎖，價格為$250。可使用[消音器](../Page/抑制器.md "wikilink")、[雙持](../Page/雙持.md "wikilink")、戰術刀及延長彈匣（增至24發）。
  - 2012年—《[-{zh-hans:反恐精英：全球攻势;
    zh-hant:絕對武力：全球攻勢;}-](../Page/反恐精英：全球攻势.md "wikilink")》（Counter-Strike:
    Global Offensive）：型號為Five-seveN
    USG，命名為「Five-seveN」，使用[黑色](../Page/黑色.md "wikilink")[底把及](../Page/机匣.md "wikilink")[套筒](../Page/手槍套筒.md "wikilink")，為反恐小組專屬手槍。
  - 2012年—《[-{zh-hans:使命召唤：黑色行动;
    zh-hant:決勝時刻：黑色行動;}-II](../Page/決勝時刻：黑色行動II.md "wikilink")》（Call
    of Duty: Black Ops II）：型號為Five-seveN
    USG，使用[綠色](../Page/綠色.md "wikilink")[底把和](../Page/机匣.md "wikilink")[黑色](../Page/黑色.md "wikilink")[套筒](../Page/手槍套筒.md "wikilink")，命名為「Five-seveN」，20發[彈匣](../Page/彈匣.md "wikilink")（聯機模式時可使用改裝：延長彈匣增至26發，單機模式時則增至27發）。於故事模式、聯機模式和殭屍模式皆有登場。故事模式之中被[美國海軍特戰開發小組](../Page/美國海軍特戰開發小組.md "wikilink")、[美國陸軍](../Page/美國陸軍.md "wikilink")[第75遊騎兵團](../Page/第75遊騎兵團.md "wikilink")、[美國特勤局](../Page/美國特勤局.md "wikilink")、「巨像」公司保安人員、[也門陸軍](../Page/也門陸軍.md "wikilink")、[也門](../Page/也門.md "wikilink")[民兵](../Page/民兵.md "wikilink")、德法爾科、勞爾·梅嫩德斯所使用；聯機模式時於等級4解鎖，並可以使用[反射式瞄準鏡](../Page/紅點鏡.md "wikilink")、[延長彈匣](../Page/彈匣.md "wikilink")、[激光瞄準器](../Page/激光瞄準器.md "wikilink")、[長槍管](../Page/槍管.md "wikilink")、、快速重裝彈匣、[消音器](../Page/抑制器.md "wikilink")、[戰術刀](../Page/戰術刀.md "wikilink")、雙持。
  - 2012年—《[战争前线](../Page/战争前线.md "wikilink")》（Warface）：型号为Five-seveN
    USG，命名为“FN
    Five-seveN”，使用20发[弹匣](../Page/弹匣.md "wikilink")，以副武器之姿出现并可被所有职业使用，专家解锁，可以改裝枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、手槍消音器、[手槍制退器](../Page/炮口制动器.md "wikilink")、[手槍刺刀](../Page/刺刀.md "wikilink")），不可改装瞄准镜。
  - 2013年—《[-{zh-hans:战地;
    zh-hant:戰地風雲;}-4](../Page/戰地風雲4.md "wikilink")》（Battlefield
    4）：型號為Five-seveN
    USG，命名為「FN57」，使用[綠色](../Page/綠色.md "wikilink")[底把及黑色](../Page/机匣.md "wikilink")[套筒](../Page/手槍套筒.md "wikilink")，20+1發彈匣，被歸類為佩槍。只能於多人聯機模式中使用，為所有兵種的解鎖武器。
  - 2013年—《[-{zh-hans:收获日;
    zh-hant:劫薪日;}-2](../Page/劫薪日2.md "wikilink")》（Payday
    2）：型號為Five-seveN
    USG，使用黑色套筒及卡其色底把，子彈數15發，可以打穿盾牌，在2017年的春季活動後可透過登入社群獲得，命名為“5/7
    AP pistol”。
  - 2015年—《[-{zh-hans:彩虹六号：围攻;
    zh-hant:虹彩六號：圍攻行動;}-](../Page/虹彩六號：圍攻行動.md "wikilink")》（Rainbox
    Six: Siege）：型號為Five-seveN USG,
    使用[黑色](../Page/黑色.md "wikilink")[底把及](../Page/机匣.md "wikilink")[套筒](../Page/手槍套筒.md "wikilink")，命名為「5.7
    USG」，被美國[聯邦調查局特種武器和戰術部隊所使用](../Page/聯邦調查局特種武器和戰術部隊.md "wikilink")。
  - 2015年—《[-{zh-hans:战地：硬仗;
    zh-hant:戰地風雲：強硬路線;}-](../Page/战地：硬仗.md "wikilink")》（Battlefield
    Hardline）：型號為Five-seveN
    USG，命名為「FN57」，資料片「劫案」新增武器，使用[綠色](../Page/綠色.md "wikilink")[底把及黑色](../Page/机匣.md "wikilink")[套筒](../Page/手槍套筒.md "wikilink")，20+1發彈匣，被警匪兩方所有職階所使用，價格為$6,000。可加裝各種進階[照準器](../Page/照準器.md "wikilink")（改進版[機械瞄具](../Page/機械瞄具.md "wikilink")、迷你、德爾塔）、附加配件（[電筒](../Page/電筒.md "wikilink")、戰術燈、[激光瞄準器](../Page/激光指示器.md "wikilink")）及槍口零件（[槍口制退器](../Page/砲口制動器.md "wikilink")、補償器、重[槍管](../Page/槍管.md "wikilink")、[抑制器](../Page/抑制器.md "wikilink")、消焰器）。
  - 2017年—《[-{zh-hans:幽灵行动：荒野;
    zh-hant:火線獵殺：野境;}-](../Page/幽灵行动：荒野之地.md "wikilink")》（Tom
    Clancy's Ghost Recon: Wildlands）：型號為Five-seveN USG，命名為「5.7
    USG」，可選彈匣為20發或30發，位置在歐寇羅省份，击败安东尼奥Boss获得的名为为“女士杀手”，可拆卸抑制器及附带[激光瞄準器](../Page/激光指示器.md "wikilink")，第二年季票新增的名為Five
    Seven Blacklist，不可裝卸抑制器，但同樣有消音效果，彈匣 20 發。
  - 2017年—《[少女前線](../Page/少女前線.md "wikilink")》（Girls'
    Frontline）：以萌擬人化的少女戰術人形登場，活動限定獲得。

### [動漫](../Page/動画.md "wikilink")

  - 2003年—《[神槍少女](../Page/神槍少女.md "wikilink")》（Gunslinger
    Girl）：型號為第一代Five-seveN，使用[黑色](../Page/黑色.md "wikilink")[底把及](../Page/机匣.md "wikilink")[套筒](../Page/手槍套筒.md "wikilink")，被喬瑟所使用。
  - 2008年—《[神槍少女II](../Page/神槍少女.md "wikilink")》（Gunslinger Girl: Il
    Teatrino）：型號為Five-seveN
    USG，使用[黑色](../Page/黑色.md "wikilink")[底把及](../Page/机匣.md "wikilink")[套筒](../Page/手槍套筒.md "wikilink")，被喬瑟所使用。
  - 2009年—[-{zh-hant:《CANAAN》;zh-tw:《迦南》;zh-hk:《CANAAN迦南》;zh-hans:《迦南》;zh-mo:《CANAAN迦南》;}-](../Page/CANAAN.md "wikilink")（CANAAN）：型號為Five-seveN
    USG，[黑色](../Page/黑色.md "wikilink")[底把和](../Page/槍身.md "wikilink")[套筒](../Page/手枪套筒.md "wikilink")，被艾琺陀．艾爾．修雅（アルファルド・アル・シュヤ，聲優：日本：[坂本真綾](../Page/坂本真綾.md "wikilink")／香港：[黃玉娟](../Page/黃玉娟.md "wikilink")）所使用。
  - 2014年—《[刀劍神域II](../Page/刀劍神域.md "wikilink")》（Sword Art Online
    II）：型號為Five-seveN
    USG，使用[黑色](../Page/黑色.md "wikilink")[底把及](../Page/机匣.md "wikilink")[套筒](../Page/手槍套筒.md "wikilink")，「幽靈子彈」篇中被「桐人」所使用。

### [輕小說](../Page/輕小說.md "wikilink")

  - 2012年—《[刀劍神域](../Page/刀劍神域.md "wikilink")》（Sword Art
    Online）：被桐人（桐ヶ谷和人）在Gun Gale Online（GGO）中所使用。
  - 2014年—《[刀劍神域外傳Gun Gale
    Online](../Page/刀劍神域外傳Gun_Gale_Online.md "wikilink")》：被“克拉倫斯”作為配槍所使用。

## 參考資料

## 外部链接

  - —[Official
    page](https://web.archive.org/web/20070701175401/http://www.fnhusa.com/products/firearms/family.asp?fid=FNF003&gid=FNG001)

  - —[Guns & Ammo Five-seveN Article
    October 2006](http://www.gunsandammomag.com/ga_handguns/fnh_091106/)

  - —[American Handgunner Five-seveN/P90
    Article](http://www.findarticles.com/p/articles/mi_m0BTT/is_144_24/ai_57886949)

  - —[Modern
    Firearms—Five-seveN](http://world.guns.ru/handguns/hg/be/fn-five-seven-e.html)

  - —[D Boy Gun
    World（Five-seveN®手枪）](http://pewpewpew.work/fn/57/57.htm)

{{-}}

[Category:半自動手槍](../Category/半自動手槍.md "wikilink")
[Category:5.7×28毫米槍械](../Category/5.7×28毫米槍械.md "wikilink")
[Category:比利時半自動手槍](../Category/比利時半自動手槍.md "wikilink")

1.

2.
3.
4.
5.
6.
7.
8.
9.  pistol-training.com[1](http://pistol-training.com/archives/1740)

10.
11. Gourley, S.; Kemp, I (November 26, 2003）. "The Duellists". Jane's
    Defence Weekly (ISSN: 02653818), Volume 40 Issue 21, pp 26–28.

12.

13.

14.

15.

16.

17.
18.

19.
20.

21.

22.
23.

24.

25.

26.

27.

28.

29.

30.

31.

32.

33.
34. "Col Moschin 9º Reggimento d'Assalto Paracadutisti" （in Italian）.
    RAIDS Italia Magazine (ISSN: 1721-3460), 2007.

35.

36.

37.
38.

39.

40.
41.
42.
43.

44.

45.

46.

47.

48.
49.

50.

51.
52.

53. <http://farm9.staticflickr.com/8052/8086808041_5137123acc.jpg>