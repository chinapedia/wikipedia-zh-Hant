**FN
Minimi**（[法文](../Page/法文.md "wikilink")：，解為「迷你型機槍／」）是由[比利時](../Page/比利時.md "wikilink")[Fabrique
Nationale公司設計的](../Page/Fabrique_Nationale.md "wikilink")[氣體傳動式](../Page/氣動式.md "wikilink")[輕機槍](../Page/輕機槍.md "wikilink")，被世界多國採用為制式裝備，當中包括著名的[美國](../Page/美國.md "wikilink")[M249班用自動武器](../Page/M249班用自動武器.md "wikilink")（M249
SAW）。

## 歷史

Minimi在1970年代初期開始研發，當時[北約各國主流仍然是發射](../Page/北約.md "wikilink")[7.62×51毫米NATO口徑的](../Page/7.62×51_NATO.md "wikilink")[通用機槍作班用支援武器](../Page/通用機槍.md "wikilink")。

Minimi的最初原設計是發射[7.62×51毫米彈藥](../Page/7.62×51_NATO.md "wikilink")，但當時的[Fabrique
Nationale](../Page/Fabrique_Nationale.md "wikilink")（FN）為了令他們開發的5.56
SS109彈藥能成為新一代北約制式彈藥，在加入[美國陸軍舉行的班用自動武器](../Page/美國陸軍.md "wikilink")（SAW）評選時改為[5.56×45毫米口徑](../Page/5.56×45mm_NATO.md "wikilink")。

## 設計

[Belt_fed_machine_gun.jpg](https://zh.wikipedia.org/wiki/File:Belt_fed_machine_gun.jpg "fig:Belt_fed_machine_gun.jpg")供彈系統\]\]
[Minimi-img_1022.jpg](https://zh.wikipedia.org/wiki/File:Minimi-img_1022.jpg "fig:Minimi-img_1022.jpg")的伸縮[槍托Minimi](../Page/槍托.md "wikilink")\]\]

Minimi採用5.56x45子彈所製的可散式金屬[彈鏈或北約標準](../Page/彈鏈.md "wikilink")（STANAG）的20/30發[彈匣供彈](../Page/彈匣.md "wikilink")，彈鏈從[機匣左面的彈鏈供彈口進入時](../Page/機匣.md "wikilink")，在彈鏈供彈口下面的彈匣供彈口活門會封閉以防止錯誤操作，而當採用彈匣時需手動打開活門。

Minimi在槍托下裝有摺合式[兩腳架](../Page/兩腳架.md "wikilink")，配有可快速更換及自動歸零的長或短重槍管，而由於採用小口徑彈藥，Minimi的重量比[7.62×51毫米口徑的通用機槍輕得多](../Page/7.62×51_NATO.md "wikilink")，總重量不過7.1公斤，可靠性亦比較高，也更適合作班用支援武器，這亦是各國為其班兵以小口徑輕機槍取代通用機槍的原因。

當5.56口徑Minimi成為世界著名的小口徑輕機槍後，FN在近年以Minimi的設計開發出7.62 x
51毫米口徑的[通用機槍](../Page/通用機槍.md "wikilink")，名為[Minimi
7.62](../Page/FN_Minimi_7.62通用機槍.md "wikilink")\[1\]，美軍的版本名為[Mk 48 Mod
0](../Page/Mk_48輕量化機槍.md "wikilink")\[2\]，而Minimi 7.62亦已推出市場。

## 採用

1980年5月以T9的測試名稱勝出評選後，美國陸軍及[海軍陸戰隊在](../Page/海軍陸戰隊.md "wikilink")1982年2月1日\[3\]正式裝備[5.56×45毫米口徑的改良過Minimi並命名為](../Page/5.56×45mm_NATO.md "wikilink")[M249班用自動武器](../Page/M249班用自動武器.md "wikilink")。

其後，受美國及北約的影響，除了美國和比利時外，世界多達數十個國家亦陸續採用Minimi或M249作為制式班用機槍\[4\]，FN的5.56毫米SS109子彈因效能比舊有的M193子彈更好亦成為北約甚至其他國家的制式彈藥。

[斯里蘭卡陸軍在](../Page/斯里蘭卡.md "wikilink")1978年成為Minimi首個裝備的部隊，當時他們仍然採用7.62口徑的[FN
FAL及](../Page/FN_FAL自動步槍.md "wikilink").303口徑的輕機槍，同時，在他們試驗過多種5.56口徑的步槍（包括M16A1），最終決定採用[FN
FNC及FN](../Page/FN_FNC突擊步槍.md "wikilink") Minimi作新一代制式武器。

[瑞典](../Page/瑞典.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[義大利](../Page/義大利.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[印尼及](../Page/印尼.md "wikilink")[日本獲FN公司合法授權生產Minimi](../Page/日本.md "wikilink")，亦有[中華人民共和國](../Page/中華人民共和國.md "wikilink")、[中華民國和](../Page/中華民國.md "wikilink")[埃及參考設計或仿製品](../Page/埃及.md "wikilink")。

## 型號

| 名稱                                              | 國家                                       | 備註                                                                                                              |
| ----------------------------------------------- | ---------------------------------------- | --------------------------------------------------------------------------------------------------------------- |
| F89                                             | [澳大利亞](../Page/澳大利亞.md "wikilink")       | 合法授權生產的Minimi，[5.56×45毫米Minimi輕機槍](../Page/5.56×45mm_NATO.md "wikilink")                                        |
| [C9](../Page/C9輕機槍.md "wikilink")               | [加拿大](../Page/加拿大.md "wikilink")         | 合法授權生產的Minimi，5.56×45毫米Minimi輕機槍，裝有鋼制管型槍托                                                                       |
| C9A1                                            | 加拿大                                      | C9改進型，裝有[ELCAN](../Page/雷神ELCAN光學技術.md "wikilink") [C79](../Page/C79光學瞄準鏡.md "wikilink") 3.4倍瞄準鏡                |
| C9A2                                            | 加拿大                                      | C9升級改進型，裝有多種戰術配件及[C79A2瞄準鏡](../Page/C79光學瞄準鏡#C79A2.md "wikilink")                                               |
| C9                                              | [紐西蘭](../Page/紐西蘭.md "wikilink")         | 合法授權生產的Minimi，5.56×45毫米Minimi輕機槍，裝有鋼制管型槍托                                                                       |
| Ksp 90                                          | [瑞典](../Page/瑞典.md "wikilink")           | 合法授權生產的Minimi，5.56×45毫米Minimi輕機槍，裝有鋼制管型槍托                                                                       |
| Ksp 90B                                         | [瑞典](../Page/瑞典.md "wikilink")           | 合法授權生產的Minimi，5.56×45毫米傘兵型Minimi輕機槍，裝有短槍管、伸縮槍托及戰術導軌                                                             |
| [T75](../Page/T75班用機槍.md "wikilink")            | [中華民國](../Page/中華民國.md "wikilink")       | 基於Minimi的設計修改而成的本土改良品，5.56×45毫米Minimi輕機槍，裝有鋼制管型槍托                                                               |
| [CS/LM8](../Page/CS/LM8型轻机枪.md "wikilink")      | [中華人民共和國](../Page/中華人民共和國.md "wikilink") | 本土仿製品之一，5.56×45毫米Minimi輕機槍，裝有鋼制管型槍托                                                                             |
| Pindad SM3                                      | [印尼](../Page/印尼.md "wikilink")           | 合法授權生產的Minimi，5.56×45毫米Minimi輕機槍，裝有鋼制管型槍托                                                                       |
| [K3](../Page/大宇K3輕機槍.md "wikilink")             | [韓國](../Page/韓國.md "wikilink")           | Minimi的本土仿製品，5.56×45毫米Minimi輕機槍                                                                                 |
|                                                 | [埃及](../Page/埃及.md "wikilink")           | 本土仿製品，5.56×45毫米Minimi輕機槍                                                                                        |
| 5.56mm機関銃MINIMI                                 | [日本](../Page/日本.md "wikilink")           | 合法授權生產的Minimi，5.56×45毫米Minimi輕機槍                                                                                |
| L108A1                                          | [英國](../Page/英國.md "wikilink")           | 5.56×45毫米Minimi輕機槍                                                                                              |
| L110A1                                          | 英國                                       | 5.56×45毫米傘兵型Minimi輕機槍                                                                                           |
| XM249                                           | [美國](../Page/美國.md "wikilink")           | 5.56×45毫米Minimi輕機槍（參選班用自動武器時的版本）                                                                                |
| XM249E1                                         | 美國                                       | XM249改良型，槍管膛線纏距為1:7                                                                                             |
| M249 PIP                                        | 美國                                       | XM249E1改良型，裝有及產品改進計劃（Product Improvement Program，PIP）的版本                                                        |
| M249傘兵型                                         | 美國                                       | 傘兵型Minimi輕機槍，裝有短槍管                                                                                              |
| M249特種用途武器                                      | 美國                                       | Minimi SPW（Special Purpose Weapon）輕機槍                                                                           |
| Mk 46 Mod 0                                     | 美國                                       | Minimi SPW/M249輕機槍改良型，裝有固定塑料槍托、導軌及前握把                                                                           |
| [Mk 48 Mod 0](../Page/Mk_48輕量化機槍.md "wikilink") | 美國                                       | [7.62×51毫米口徑Minimi通用機槍](../Page/7.62×51mm_NATO.md "wikilink")，用於替換美軍特种部隊的[M60E4](../Page/M60通用機槍.md "wikilink") |
| [Mk 48 Mod 1](../Page/Mk_48輕量化機槍.md "wikilink") | 美國                                       | Mk 48 Mod 0通用機槍改良型                                                                                              |

## 使用國

### 北約

  - （Minimi）－[比利時陸軍及](../Page/比利時陸軍.md "wikilink")[空軍的正規部隊裝備了標準型Minimi](../Page/比利時空軍.md "wikilink")（名為M1）[傘兵部隊採用傘兵型Minimi](../Page/傘兵.md "wikilink")（名為M3）。

  - （C9）－[加拿大軍隊採用加拿大](../Page/加拿大軍隊.md "wikilink")[迪瑪科](../Page/迪瑪科.md "wikilink")（Diemaco）\[5\]合法生產的C9輕機槍，C9原型裝有鋼制管型[槍托](../Page/槍托.md "wikilink")，其後迪瑪科推出了裝有[機匣蓋導軌及](../Page/机匣.md "wikilink")[ELCAN](../Page/雷神ELCAN光學技術.md "wikilink")
    [C79](../Page/C79光學瞄準鏡.md "wikilink")
    3.4倍瞄準鏡的C9A1，而目前最新推出升級計劃的名為C9A2，裝有短槍管、多個部件改用迷彩綠色、以布彈袋取代塑料彈藥、可伸縮折疊的液壓緩衝槍托（類似[C8卡賓槍液壓緩衝槍托的原理](../Page/C8卡賓槍.md "wikilink")）、折疊式[前握把及](../Page/輔助握把.md "wikilink")[雷射瞄準器](../Page/雷射瞄準器.md "wikilink")。

[Canadian_C9A1_LMG.JPG](https://zh.wikipedia.org/wiki/File:Canadian_C9A1_LMG.JPG "fig:Canadian_C9A1_LMG.JPG")使用C9A2\]\]

  - （M249）

  - （Minimi）－被駐[阿富汗的](../Page/阿富汗.md "wikilink")[特種部隊所使用](../Page/特種部隊.md "wikilink")。

  - （Minimi）－命名為Mg M/07 5.56 mm。

  - （Minimi）－[法國陸軍用於取代](../Page/法國陸軍.md "wikilink")7.62毫米[AAT-F1通用機槍](../Page/AA-52通用機槍.md "wikilink")。

[French_Army_soldier.jpg](https://zh.wikipedia.org/wiki/File:French_Army_soldier.jpg "fig:French_Army_soldier.jpg")使用FN
Minimi輕機槍\]\]

  - （Minimi）－裝備[希臘陸軍及其](../Page/希臘陸軍.md "wikilink")[特種部隊](../Page/特種部隊.md "wikilink")。

  - （M249）

  - （Minimi）－由與FN合夥的義大利[貝瑞塔合法生產並裝備](../Page/貝瑞塔.md "wikilink")[義大利軍隊](../Page/義大利軍隊.md "wikilink")，以取代老舊的MG42/59。

  - （Minimi）

  - （Minimi）

  - （Minimi Para）

  - （Minimi）－[荷蘭皇家陸軍裝備了傘兵型Minimi](../Page/荷蘭皇家陸軍.md "wikilink")。

  - （Minimi）：裝備[挪威海岸遊騎兵及](../Page/挪威海岸遊騎兵.md "wikilink")[挪威陸軍遊騎兵](../Page/挪威陸軍遊騎兵.md "wikilink")。

  - （Minimi）－被[波蘭軍隊](../Page/波蘭軍隊.md "wikilink")[行動應變及機動組所採用](../Page/行動應變及機動組.md "wikilink")。

  - （Minimi）

  -
  - （Para）

  - （Minimi）

  - （Minimi）

  - （Minimi）－[土耳其陸軍](../Page/土耳其陸軍.md "wikilink")[特種部隊](../Page/特種部隊.md "wikilink")（[栗色貝雷帽](../Page/栗色貝雷帽.md "wikilink")）並有採用Minimi。

  - （L108A1 SAW/L110A1
    Para）－[英國陸軍為每個四人火力小組裝備兩把傘兵型Minimi以取代](../Page/英國陸軍.md "wikilink")[L86
    LSW](../Page/SA80突擊步槍.md "wikilink")，[皇家海軍及](../Page/皇家海軍.md "wikilink")[皇家空軍團](../Page/皇家空軍團.md "wikilink")，部份Minimi更裝有[SUSAT](../Page/SUSAT光學瞄準鏡.md "wikilink")
    4[倍瞄準鏡](../Page/望远镜放大倍数.md "wikilink")。

[M249_FN_MINIMI_DM-ST-91-11997.jpg](https://zh.wikipedia.org/wiki/File:M249_FN_MINIMI_DM-ST-91-11997.jpg "fig:M249_FN_MINIMI_DM-ST-91-11997.jpg")使用L108A1
SAW\]\]

  - （[M249](../Page/M249班用自動武器.md "wikilink")）－[美軍在](../Page/美軍.md "wikilink")1982年2月1日正式裝備Minimi並命名為M249，但因當時出現可靠性問題，實際上在1980年代後期才全面裝備。美軍的M249由FNMI（FN美國分公司）生產，並衍生出多種版本，如M249
    P.I.P.、M249 S.P.W.及Mk 46 Mod 0等。

### 非北約

  - （M249）

  - （M249）－裝備[阿根廷海軍陸戰隊的步兵](../Page/阿根廷海軍陸戰隊.md "wikilink")、炮兵及偵察兵。

  - （F89）－由澳大利亞國防工業（Australian Defence Industries -
    ADI）生產，裝備[澳大利亞陸軍](../Page/澳大利亞陸軍.md "wikilink")、[海軍及](../Page/澳大利亞海軍.md "wikilink")[空軍](../Page/澳大利亞空軍.md "wikilink")。F89裝有導軌、加長的及[AUG
    F88的](../Page/斯太爾AUG突擊步槍.md "wikilink")1.5倍瞄導鏡，移除槍管上方的隔熱罩及改用固定式提把。

[Australian_F-89_BFA.jpg](https://zh.wikipedia.org/wiki/File:Australian_F-89_BFA.jpg "fig:Australian_F-89_BFA.jpg")使用F89\]\]

  - （M249）

  - （Minimi）

  - （M249）

  - （Minimi）－[巴西海軍陸戰隊在](../Page/巴西海軍陸戰隊.md "wikilink")1990年代中期裝備作排用支援武器。

  - （Minimi）－[智利陸軍在](../Page/智利陸軍.md "wikilink")1990年代開始裝備。

  - （M249及Minimi）

  - （Minimi）

  - （Minimi）

  - （Minimi）－本土生產。

  - （Minimi）

  - （Minimi）－裝備[印尼軍隊及](../Page/印尼軍隊.md "wikilink")[印尼海軍陸戰隊](../Page/印尼海軍陸戰隊.md "wikilink")。

  - （Minimi）－[愛爾蘭遊騎兵裝備傘兵型Minimi](../Page/愛爾蘭遊騎兵.md "wikilink")。

  - （M249）

  - （Minimi）－[以色列國防軍在](../Page/以色列國防軍.md "wikilink")1990年代後期曾經裝備小量Minimi作班用機槍，其後他們以Minimi的設計開發出更適合沙漠使用的[IMI
    Negev](../Page/內蓋夫輕機槍.md "wikilink")。

  - （M249）

  - （Minimi）－由[住友集團合法生產](../Page/住友集團.md "wikilink")，裝備[陸上自衛隊以替代](../Page/陸上自衛隊.md "wikilink")7.62毫米口徑的[豐和64式](../Page/豐和64式.md "wikilink")。

  - （Minimi）

  - （Minimi）－被特警隊所採用。\[6\]

  -
  - （Minimi、M249）

  - （Minimi Mk II）

  - （M249）

  - （Minimi）－裝備[馬來西亞陸軍以取代](../Page/馬來西亞陸軍.md "wikilink")[HK11A1](../Page/HK11.md "wikilink")。

  - （Minimi）

  - （M249及Minimi）－裝備[墨西哥陸軍及](../Page/墨西哥陸軍.md "wikilink")[海軍](../Page/墨西哥海軍.md "wikilink")。

  - （Minimi及M249）

  - （Minimi）－於2002年7月11日由比利時送往尼泊爾，共有5,500把，裝備[尼泊爾陸軍](../Page/尼泊爾陸軍.md "wikilink")。

  - （C9）－[紐西蘭國防軍採用加拿大C](../Page/紐西蘭國防軍.md "wikilink")9。

  - （L110A1）

  - （Minimi Para）

  - （F89）

  - （M249 Minimi）

  - （Minimi）－裝備[菲律賓軍隊](../Page/菲律賓軍隊.md "wikilink")，與[Ultimax 100
    Mk3及K](../Page/Ultimax_100輕機槍.md "wikilink")3輕機槍共用。

  - （Minimi）

  -
  - （Minimi）－被特別旅及反恐部隊所採用。

  - （K3）－本土生產，命名為**[K3輕機槍](../Page/大宇K3輕機槍.md "wikilink")**。

  - （Minimi）－世界上第一個裝備Minimi的國家。

[Ksp90b.JPG](https://zh.wikipedia.org/wiki/File:Ksp90b.JPG "fig:Ksp90b.JPG")

  -
  - （Ksp 90）

  - （LMG05）

[LMG_05.jpg](https://zh.wikipedia.org/wiki/File:LMG_05.jpg "fig:LMG_05.jpg")

  - （Minimi、[T-75班用機槍](../Page/T-75班用機槍.md "wikilink")）－[中華民國軍隊基於Minimi的設計修改而成](../Page/中華民國軍隊.md "wikilink")，名為[T-75班用機槍](../Page/T-75班用機槍.md "wikilink")，但陸軍因已進口大量的Minimi，僅採購少量的T-75。

  - （M249及Minimi）－裝備[泰國皇家陸軍](../Page/泰國皇家陸軍.md "wikilink")。

  - （Minimi）

  -
  - （Minimi）－由1990年代開始裝備[委內瑞拉陸軍](../Page/委內瑞拉陸軍.md "wikilink")。

  - （Minimi Mk 3）

  -
## 資料來源及註解

[Minimiidef2007.JPG](https://zh.wikipedia.org/wiki/File:Minimiidef2007.JPG "fig:Minimiidef2007.JPG")

<div class="references-small">

<references />

  - [gun-world.net-Minimi](http://firearmsworld.net/fn/minimi/inde.htm)
  - [U.S. Army M249 Fact
    File](https://web.archive.org/web/20070316035145/http://www.army.mil/fact_files_site/m-249_saw/)
  - [FN USA
    Webpage](https://web.archive.org/web/20021118102528/http://www.fnhusa.com/contents/home.htm)
  - [Official FN Herstal Minimi
    page](https://web.archive.org/web/20060204103832/http://www.fnherstal.com/html/Index.htm)
  - [Modern Firearms—FN Minimi](http://world.guns.ru/machine/mg17-e.htm)

</div>

## 参见

  - [Fabrique Nationale](../Page/Fabrique_Nationale.md "wikilink")
  - [M249班用自動武器](../Page/M249班用自動武器.md "wikilink")
  - [大宇K3輕機槍](../Page/大宇K3輕機槍.md "wikilink")
  - [CS/LM8型轻机枪](../Page/CS/LM8型轻机枪.md "wikilink")
  - [HK23輕機槍](../Page/HK23輕機槍.md "wikilink")
  - [HK MG4輕機槍](../Page/HK_MG4輕機槍.md "wikilink")
  - [阿瑞斯伯勞鳥5.56輕機槍](../Page/阿瑞斯伯勞鳥5.56輕機槍.md "wikilink")
  - [斯通納輕機槍](../Page/斯通納輕機槍.md "wikilink")
  - [LSAT輕機槍](../Page/LSAT輕機槍.md "wikilink")
  - [輕機槍](../Page/輕機槍.md "wikilink")
  - [FN MAG通用機槍](../Page/FN_MAG通用機槍.md "wikilink")
  - [M240通用機槍](../Page/M240通用機槍.md "wikilink")
  - [FN Minimi 7.62通用機槍](../Page/FN_Minimi_7.62通用機槍.md "wikilink")
  - [Mk 48輕量化機槍](../Page/Mk_48輕量化機槍.md "wikilink")
  - [RPK輕機槍](../Page/RPK輕機槍.md "wikilink")
  - [防禦者M151遙控武器站](../Page/防禦者M151遙控武器站.md "wikilink")

## 外部链接

  - \-[RFP for new U.S.
    LMG](http://www.fbodaily.com/archive/2006/02-February/05-Feb-2006/FBO-00979937.htm)

  - \-[Nazarian\`s Gun\`s Recognition Guide (FILM) FN M249 SAW
    Presentation
    (.MPEG)](http://www.nazarian.no/wep.asp?id=390&group_id=13&country_id=168&lang=0&p=8)

[Category:輕機槍](../Category/輕機槍.md "wikilink")
[Category:5.56×45毫米槍械](../Category/5.56×45毫米槍械.md "wikilink")
[Category:比利時槍械](../Category/比利時槍械.md "wikilink")
[FN_Minimi衍生槍](../Category/FN_Minimi衍生槍.md "wikilink")

1.  [fnherstal.com-Minimi 7.62](http://www.fnherstal.com/html/MINIMI762.htm)

2.  [Small Arms Review article on the
    MK48](http://www.smallarmsreview.com/pdf/jul03.pdf)
3.  [remtek.com-FN
    Minimi](http://www.remtek.com/arms/fn/minimi/index.htm)
4.  [fnhusa.com-M249](http://www.fnhusa.com/products/firearms/family.asp?fid=FNF014&gid=FNG008)

5.  迪瑪科被[柯爾特收購後改名為](../Page/柯爾特.md "wikilink")[加拿大柯爾特](../Page/加拿大柯爾特.md "wikilink")（Colt
    Canada）。
6.  <http://www.youtube.com/watch?v=PbDMvPiERLM>