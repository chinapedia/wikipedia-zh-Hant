**P90**是于1990年由比利時的[Fabrique
Nationale所推出](../Page/Fabrique_Nationale.md "wikilink")，屬於[個人防衛武器類別的一種鎗械](../Page/個人防衛武器.md "wikilink")。P90是全名“FN
Project
1990”的簡寫，意即90年代的武器專項。值得一提的是，P90是世界上第一支使用了全新彈藥的[個人防衛武器](../Page/個人防衛武器.md "wikilink")。

## 歷史

二戰後，突擊步槍開始興起，[衝鋒槍從戰場第一線退下來](../Page/衝鋒槍.md "wikilink")，成為二線後勤人員的配備。非戰鬥人員雖未參與實際的戰鬥，但他們肩負更重要的後勤與支援任務，包含戰車、飛機的油彈整補，營膳，負責戰鬥任務的（包括砲兵）與現代主宰戰場的靈魂：掌管情報與電腦操作人員（C4I）。

現代科技的發展（特別是空軍）使得戰爭中跳過前線直接攻擊後方的戰法變得可能。非戰鬥人員要面對敵方[空降部隊](../Page/空降部隊.md "wikilink")、突擊部隊的機會大增，而科技的進步也使得非戰鬥人員在軍中的比例越來越多。以[1991年海灣戰爭為例](../Page/1991年海灣戰爭.md "wikilink")，各國派駐[沙特阿拉伯的部隊中的戰鬥人員比例僅佔一半](../Page/沙特阿拉伯.md "wikilink")。

正因為非戰鬥人員成為主宰戰場的神經中樞，所以成為敵方派遣特戰人員滲透摧毀的重要目標。面對這群身懷絕技的特種部隊，除配有精良的武器外，防彈衣已是不可少的裝備；而現役的9mm口徑衝鋒槍根本無法貫穿防彈衣，所以在面對敵方特戰人員時，連最基本的自衛能力都將喪失。

這種戰術使得美國同[北約意識到需要為非前線戰鬥人員配備一款特別槍械](../Page/北約.md "wikilink")，讓非作戰人員未經長期訓練下也有能力應付配備突擊步槍、軍用[防彈裝備敵方戰鬥人員的突擊](../Page/防彈.md "wikilink")。於是產生了美國的小火器主導計劃、北約的AC225計劃，當中要求的槍械稱為個人防禦武器，其重要要求如下：

  - 重量輕，體積細小
  - 容易瞄準，且後座力低，以提高命中率
  - 高穿透力，在150m內能擊穿個人防護裝備（如頭盔、防彈背心等）
  - 結構簡單，易於隨時進行保養

FN意識到當時現成的子彈，包括[手槍](../Page/手槍.md "wikilink")、[步槍子彈也不能滿足個人防衛武器的要求](../Page/步槍.md "wikilink")，於是在1986年開始研發全新的子彈[SS90及新款槍械P](../Page/5.7×28mm.md "wikilink")90，原型槍於同年10月試射，曾被使用於1991年[海灣戰爭](../Page/海灣戰爭.md "wikilink")，至1993年共試產了3000枝。

SS90子彈原是[塑料彈頭](../Page/塑料.md "wikilink")，之後的SS190採用較重的半[鋁半](../Page/鋁.md "wikilink")[鋼製彈頭](../Page/鋼.md "wikilink")，並改短彈頭2.7毫米以配合FN研發的新手槍[Five-seveN](../Page/Five-seveN手槍.md "wikilink")，使用SS190的P90彈匣也在1993推出。

雖然P90推出時並無其他槍能達到個人防衛武器的要求（直到2001年，11年後，H\&K才設計出有類似功能的[HK
MP7](../Page/HK_MP7衝鋒槍.md "wikilink")），但剛巧[冷戰結束](../Page/冷戰.md "wikilink")，各國對個人防衛武器的需求突然消失，原先預期的大量軍用訂單落空，而其他市場的推廣上也有著使用不普及子彈而造成的困難。雖然處境如此，在2003年仍有超過23個國家使用，而到2009年有約40個國家同地區使用，而在2005年，FN公司推出P90的半自動版給民用市場，是為[PS90](../Page/FN_PS90半自動卡賓槍.md "wikilink")。

## 特點

P90的發展目標是研制出一種符合北約提出對[個人防衛武器的要求](../Page/個人防衛武器.md "wikilink")，也即美國的小火器主導計劃、北約的AC225計劃中要求的一種鎗械。這種鎗械要求適合射擊技巧不高的駕駛員、後勤人員使用，即使訓練不足能也能有一定射擊效果，同時能穿透軍用[防彈裝備](../Page/避彈衣.md "wikilink")，以應付敵方身穿防彈裝備的前線部隊。

雖然設計原意是給訓練不足的軍方人員作自衛用，但現在使用的的多是受過專業訓練的人員，在有使用P90的40多個國家中，使用者多為[特種部隊](../Page/特種部隊.md "wikilink")、[反恐單位及](../Page/反恐.md "wikilink")[警隊](../Page/警隊.md "wikilink")，用途也當然不只[自衛](../Page/自衛.md "wikilink")。

P90小巧、便攜、高容量彈匣、火力能擊穿軍用防彈背心、後座力低、結構簡單可靠而易於保養。P90使用頂置彈匣、[無托設計](../Page/犢牛式.md "wikilink")，所以雖然鎗身很短，但鎗管仍有263毫米（10.35吋）長，讓子彈有相當高彈速。P90所使用的[5.7×28mm子彈能把後座力降至低於手鎗](../Page/5.7×28mm.md "wikilink")，同時穿透力卻能有效擊穿[手鎗或衝鋒鎗不能擊穿的防彈背心等個人防護裝備](../Page/手鎗.md "wikilink")。

由於性能超出現有不少鎗系，P90能同時取代[衝鋒鎗及](../Page/衝鋒鎗.md "wikilink")[短管突擊步鎗等鎗械](../Page/卡賓槍.md "wikilink")。在1993年FN推出了同樣使用5.7×28毫米子彈的[FN
Five-seveN手槍](../Page/FN_Five-seveN手槍.md "wikilink")，使整個系列更完整。

雖然有高命中精度、高、高穿透性、小巧便攜、易於保養、結構簡單、後座力低及高容量彈匣的優點，但P90的威力較9毫米魯格彈小，且單價高昂。

## 設計

### 結構

P90獨特的外形是建基於深入的[人因工程學研究](../Page/人因工程學.md "wikilink"):握把類似競賽用鎗的設計，讓扣把的手可以與頭部靠近的同時保持舒適，最前方垂直向下的凸起物用作防止副手射擊時意外地伸到鎗口，圓滑的外觀也減少了意外被衣服之類絆著的機會。設計P90時，考慮到要在狹窄環境中通過、使用（例如裝甲車輛內部），P90長度被設計為不長於一個人肩膀的闊度（0.5m），因此採用無托結構（[犢牛式](../Page/犢牛式.md "wikilink")，Bullpup）的設計（也即鎗機藏後鎗托內，而進彈位則在握把後方），目的是保留鎗管長度的同時，盡量把鎗身縮短。P90鎗身全長只有50公分，但鎗管仍有263毫米長，較長的鎗管讓子彈加速時間較長、彈速速高，有助提高射程及穿透力。枪管内有八条右旋膛线，缠距1:9.1英寸。採用無托結構後還有其他附帶優點：鎗身重心部置靠近握把及較貼近射手，因此有利單手操作及可更快速靈活地改變指向。固定鎗托在特發情況也提高了反應速度及射擊精度。

P90的彈匣是位於槍管上方，彈匣直長條形而與槍管平行，子彈放在彈匣內時，方向是和槍管成90度垂直，射擊時會通過匣內螺旋狀滑槽旋轉90度，與槍管平行後送入鎗鏜。彈匣容量50發，提供比大部份其他同類鎗械多60%至100%的持續火力，彈匣以半透明膠料制成，加上位於鎗身上方射手直視可見之處，要檢視子彈餘量非常方便。水平的彈匣也使得P90比其他同類鎗低矮，只有210毫米，伏射時射手可以伏得更低。

P90發射時彈殼往下拋，拋出的彈殼在射手視線以外，對射手的影響減到最低，拋彈口經過小心設計，確保在貼近地面、倒置等各種射擊姿勢下拋彈殼也不會影響射擊。右左都有上鏜捍，射擊模式選擇旋鈕在護環內扳機下方。如此，P90能[左右手操作](../Page/兩手同利#武器.md "wikilink")，採用無托結構的P90卻可隨時採左或右手射擊。射擊射擊模式可選擇半自動或自動，在全自動模式下有類似[AUG步鎗的兩觸發段操作模式](../Page/斯泰爾AUG突擊步槍.md "wikilink")，輕扣是半自動發射，進一步扣下是連射的全自動發射。

整槍包括擊鎚在內大量使用高分子塑料（polymer）製造，以減輕重量及生產成本\[1\]，空重只有2.5kg，加上50發彈匣後也只有3kg。它的擊鎚總成與[AUG](../Page/斯泰爾AUG突擊步槍.md "wikilink")、[FN
F2000設計類似](../Page/FN_F2000突擊步槍.md "wikilink")，扳機釋放壓力大約8磅。

### 運作原理

P90的運作原理採用[單純反沖](../Page/反沖作用_\(槍械\).md "wikilink")（Simple
Blowback）原理，由鎗機重量造成的慣性及復進彈簧的阻力使子彈發射時保持閉鎖，能夠在發射時保持閉鎖使得P90能作精準射擊，射速900rpm。而單純反衝的結構簡單，提高了P90的可靠性，便於保養之餘同時降低生產成本。

低膛壓是能夠採用單純反衝的先決條件，P90採用的子彈SS190鏜壓只有345MPa（5.56毫米是430MPa），這種小口徑高速[5.7×28mm彈藥本身的](../Page/5.7×28mm.md "wikilink")[後座力極輕](../Page/後座力.md "wikilink")，約只有5.56
NATO彈藥的一半，比9毫米手鎗子彈還低，再加上P90本身獨特的雙復進簧與後座緩衝墊使得後座力更進一步的降低。

### 子彈

[57lineup.jpg](https://zh.wikipedia.org/wiki/File:57lineup.jpg "fig:57lineup.jpg")子彈\]\]

P90最突出的性能是低於手鎗子彈的[後座力](../Page/後座力.md "wikilink")，但對個人防彈裝備卻有高於手鎗子彈的穿透力，5.7×28毫米彈藥可以輕易的穿透具有三級、四級甚至於五級防護能力[防彈背心](../Page/防彈背心.md "wikilink")，這全歸功於全新設計的[SS90](../Page/5.7×28mm.md "wikilink")、[SS190子彈](../Page/5.7×28mm.md "wikilink")。SS190的總能量與[9毫米相若](../Page/9毫米鲁格弹.md "wikilink")，但速度高一倍，後座力只有9毫米的70%，重量較9毫米輕，彈頭較尖、長。SS190膛壓只比9毫米高約50%，讓P90可以採用簡單可靠的運作方式，同時擁有良好射擊精準度。

雖然對防彈裝備有很高的穿透力，但5.7×28毫米子彈的[制止作用](../Page/制止作用.md "wikilink")（[stopping
power](../Page/stopping_power.md "wikilink")）並不低，因為彈頭在人體內翻滾足以做成相當的破壞，而對人體的穿透力也不高；因此在警務上使用時有助減少穿過目標後傷及無辜的機會。1999年，[皇家加拿大騎警測試顯示SS](../Page/皇家加拿大騎警.md "wikilink")190打穿25m外的level
II防彈背心後穿越仿人體膠質只有9吋深；候斯頓警方的[SWAT測試也發現SS](../Page/特種武器和戰術部隊.md "wikilink")190對無保護裝備的仿人體膠質只穿越11-13吋深。

因此5.7口徑的子彈除了顯示出高外，也減小警務人員對子彈穿過目標後誤傷無辜的憂慮。

雖然5.7×28毫米子彈在穿透力及制止力的性能同樣優異，但碰巧冷戰結束，在各國一片裁軍潮中，多使用一種新子彈實不可能，造成推廣上的另一困難。

現在，生產5.7×28毫米子彈的除了[赫爾斯塔爾國營工廠](../Page/赫爾斯塔爾國營工廠.md "wikilink")(FN
Herstal）外，還有，使用5.7×28毫米子彈的鎗械除了P90及[FN
Five-seveN外還有由M](../Page/FN_Five-seveN手槍.md "wikilink")4改過來的[AR-57](../Page/AR-57卡賓槍.md "wikilink")。另外，有一些可以選用幾款不同子彈的鎗械可以選用5.7毫米×
28毫米，包括[VBR
CQBW及](../Page/VBR近接戰鬥武器.md "wikilink")[新科動力CPW](../Page/新科動力CPW衝鋒槍.md "wikilink")。

早期的子彈型號為SS90，採塑質蕊的全金屬被覆（[FMJ](../Page/FMJ.md "wikilink")）彈頭，重1.5克，比9毫米手鎗子彈輕，彈速848m/s。1994年被短2.7毫米及略重的SS190所取代。SS190，子彈因為略重，彈速約為712m/s，以鋼及鋁製作彈蕊，穿透力比SS90高。

#### 北約評估

[4.6x30mm,_5.7x28mm,_.30_M1_Carbine.jpg](https://zh.wikipedia.org/wiki/File:4.6x30mm,_5.7x28mm,_.30_M1_Carbine.jpg "fig:4.6x30mm,_5.7x28mm,_.30_M1_Carbine.jpg"),
[5.7×28mm及](../Page/5.7×28mm.md "wikilink")[.30
Carbine](../Page/.30卡賓槍彈.md "wikilink")\]\]
現今彈藥中，性能與SS190（[5.7×28mm](../Page/5.7×28mm.md "wikilink")）相當的只有[H\&K的](../Page/H&K.md "wikilink")[4.6×30mm](../Page/4.6×30mm.md "wikilink")。為了標準化個人防衛武器的彈藥，北約對此兩種子彈作比對性測試，以選其一取代9x19mm\[2\]，測試小組由美國、加拿大、法國及英國等多個專家組成\[3\]，分ETBS及QRT兩工作組。結論是5.7×28mm的性能明顯比4.6×30mm高\[4\]，最重要的分別是：
\* 兩款子彈對有穿著防彈裝備的目標有相同的效率，但對無防彈裝備的目標時，5.7×28毫米的效率高27%。\[5\]

  - 在極端溫度下，5.7×28毫米受的影響較低。\[6\]
  - 5.7×28毫米對鎗管的損害較低。\[7\]
  - 5.7×28毫米的設計及生產較接近[5.56x45mm
    NATO子彈](../Page/5.56x45mm_NATO.md "wikilink")，方便以現有生產線大量生產。\[8\]
  - 5.7×28毫米系鎗械較成熟，對鎗械設計要求也較低，同時已有手鎗設計\[9\]\[10\]。

因此北約建議採用5.7×28毫米作標準，但研發4.6×30毫米的德國拒絕接受，因而制停了整個標準化程序\[11\]\[12\]。之後，在FN的網頁上，在5.7毫米子彈的的資料中加上了北約推薦（recommended
by NATO）的描述\[13\]。

### 瞄具及配件

#### 瞄準器

由於P90鎗身非常短，一般照門、準星的誤差太大，因此P90需使用光學瞄準器。原裝設計使用MC-10-80反射式單點瞄準鏡，是由[Ring
Sights公司設計](../Page/Ring_Sights.md "wikilink")，給FN
P90專用的。它的內部用一束由前方集光的光纖來照亮瞄準線，增加晝間與背景的反差。瞄準鏡視界大約是180
MOA，由一個瞄準中心點與20
MOA的同心圓圈構成整個晝間瞄準線。夜間瞄準線是由一個靠「氚組件」照明的「T」型橘紅色瞄準線構成，夜間瞄準線亦可於晝間背光時使用。此瞄準器可以由射手微調，並能與夜視器配合使用\[14\]。光學瞄準器的兩側都有簡單的準星，是為萬一光學瞄準器有損壞時作後備之用。

#### 配件

其他配件包括有：

  - [戰術燈](../Page/戰術燈.md "wikilink")
  - [雷射瞄準器](../Page/雷射瞄準器.md "wikilink")
  - 鎗帶
  - [滅聲器](../Page/抑制器.md "wikilink")

### 保養

[PS90_breakdown.jpg](https://zh.wikipedia.org/wiki/File:PS90_breakdown.jpg "fig:PS90_breakdown.jpg")

P90採用模組化設計，使得整槍只有69個零件（27件為塑膠造的）及簡化保養工作，而鎗托後部有一空間用作收納清潔工具。整槍大部分分解不須任何工具，經簡單訓練可於15秒內完成。分解後主要分為半透明彈匣，槍身與扳機總成，槍管與光學瞄準鏡及復進簧與槍機總成等4個模組。其鎗管以[冷鍛方式制造](../Page/鍛造.md "wikilink")，設計壽命20,000發子彈。

## 衍生型

除PS90以外，各衍生型均可加裝Gemtech[抑制器](../Page/抑制器.md "wikilink")（Model SP-90）。

  - P90

<!-- end list -->

  - 標準型，可因應[特种部队的需要在瞄准镜两侧增加一段導軌](../Page/特种部队.md "wikilink")

<!-- end list -->

  - P90 TR

<!-- end list -->

  - [機匣上](../Page/機匣.md "wikilink")、左、右部加裝[MIL-STD-1913導軌以對應戰術配件](../Page/皮卡汀尼導軌.md "wikilink")。TR即三導軌（Triple
    Rail）的意思。頂端的導軌較長，用以裝上瞄俱，兩則較短，用以裝上雷射瞄準器及照明用俱。此型號在1999年引入。

<!-- end list -->

  - P90 TAC

<!-- end list -->

  - 最新改型，明顯的特徵是延長了頂部的導軌、新型的和在導軌座下增加了一個後備的簡易[機械瞄具](../Page/機械瞄具.md "wikilink")。

<!-- end list -->

  - P90 USG

<!-- end list -->

  - P90 USG（United States
    Govrnment），跟標準P90基本相同，分別在於瞄準具，鋁合金製MC-10-80光學瞄準鏡內部的瞄準點、圈為黑色，便於日間強光下使用，在暗環境下標示會由氘氣照成紅色。瞄鏡可移除、換成MIL-STD-1913導軌以便裝上其他瞄俱。

<!-- end list -->

  - P90 LV（Laser Visible）及P90 IR（Infrared）

<!-- end list -->

  - P90 LV內置[雷射瞄準器](../Page/雷射.md "wikilink")，P90
    IR內置[紅外線雷射瞄準器](../Page/紅外線.md "wikilink")。LV即雷射瞄准器（Visible
    Laser）的意思，IR的意思为紅外線。兩者都是在1995年推出，由澳洲Laserex
    Technologies生產，內置在P90前方鎗咀下方的凸出位置，重131g，綠開關設於握把下方，可選擇3種模式，關閉、低光度（訓練用）及最大光度（作戰用），電池壽命達250小時（訓練模式）或50小時（戰鬥模式）。P90
    LV的可見光激光功率為8mW，供低光度環境附助瞄準或阻嚇用，而紅外線激光瞄準器則只有在夜視裝備配合下才見得到。

<!-- end list -->

  - [PS90](../Page/FN_PS90半自動卡賓槍.md "wikilink")

<!-- end list -->

  - 美國民用市場專用，半自動。因為美國的稅務因素，鎗管改為407毫米（16英寸）長，射速因此增加至777m/s，若使用其他廠商制造的子彈則可高至930m/s。由於部份地區對彈匣容量有限制，隨鎗附上的彈匣容量限制在10及30發。

## 實戰中採用

P90的首次實戰紀錄是[比利時的特種部隊用於](../Page/比利時.md "wikilink")1991年的[海灣戰爭](../Page/海灣戰爭.md "wikilink")。在1997年[日本駐秘魯大使館脅持人質事件](../Page/日本駐秘魯大使館脅持人質事件.md "wikilink")，拯救人質行動中的[秘魯特種部隊使用上了滅聲器的P](../Page/秘魯.md "wikilink")90。該行動中，14名武裝份子被殺，武裝份子身穿的防彈衣都被P90擊穿，71名人質獲救。2011年，[利比亞領袖](../Page/利比亞.md "wikilink")[卡達菲的部隊有用過P](../Page/卡達菲.md "wikilink")90於利比亞內戰中。

美國[休斯頓警方在](../Page/休斯頓.md "wikilink")1999年使用P90，是美國第一個使用P90的警務單位，並在2003年在實戰中使用P90射擊，也是該國第一次P90的實戰紀錄，而德州的Addison警隊則是美國第一個把P90擺放於巡邏車的警務單位。至2009年，全美有超過200個執法人員使用P90，包括[美國特勤局及](../Page/美國特勤局.md "wikilink")[聯邦保護局](../Page/聯邦保護局.md "wikilink")。因為有不少單位採用，[美國步槍協會把P](../Page/美國步槍協會.md "wikilink")90及PS90列入該會的Tactical
Police Competition standards。在2009年，全世界有40個國家的軍方或警務單位使用P90。

## 使用國

<table>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p>單位名稱</p></th>
<th><p>型號</p></th>
<th><p>數量</p></th>
<th><p>日期</p></th>
<th><p>其他資料</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>{{tsl|en</p></td>
<td><p>Agrupación de Buzos Tácticos|戰術潛水小組}}</p></td>
<td><p><em>P90</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>前<em>PAN</em>國家航空警察</p></td>
<td><p><em>P90</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[15]</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/奧地利聯邦軍.md" title="wikilink">奧地利陸軍</a></p></td>
<td><p><em>P90</em>,<br />
<em>P90 TR</em></p></td>
<td><p>140</p></td>
<td><p>—</p></td>
<td><p>[16]</p></td>
</tr>
<tr class="even">
<td><p>近身護衛隊</p></td>
<td><p><em>P90 TR</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[17]</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>反恐部隊</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/特種部隊.md" title="wikilink">特種部隊單位</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/比利時國防軍.md" title="wikilink">比利時國防軍</a></p></td>
<td><p><em>P90</em>,<br />
<em>P90 TR</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[18][19]</p></td>
</tr>
<tr class="even">
<td><p>保安護衛分隊</p></td>
<td><p><em>P90</em></p></td>
<td><p>53</p></td>
<td><p>—</p></td>
<td><p>[20]</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[21]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>前</p></td>
<td><p><em>P90</em></p></td>
<td><p>114</p></td>
<td><p>—</p></td>
<td><p>[22]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿爾斯科特.md" title="wikilink">阿爾斯科特市警隊</a></p></td>
<td><p><em>P90</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[23]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/列日.md" title="wikilink">列日市警隊</a>（用於取代<a href="../Page/烏茲衝鋒槍.md" title="wikilink">烏茲衝鋒槍</a>）</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>2002-</p></td>
<td><p>[24][25]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Boraine警區</p></td>
<td><p><em>P90 TR</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[26]</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/特別警察行動營.md" title="wikilink">特別警察行動營</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[27]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/第二聯合特遣部隊.md" title="wikilink">第二聯合特遣部隊</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>2005-</p></td>
<td><p>[28]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[29]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>特警部隊</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[30]</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>特種部隊單位</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[31]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>歐米茄特遣部隊</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>總統衛隊</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/賽普勒斯國民警衛隊.md" title="wikilink">賽普勒斯國民警衛隊特種部隊單位</a></p></td>
<td><p><em>P90</em></p></td>
<td><p>350</p></td>
<td><p>2000-</p></td>
<td><p>[32][33]</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><em>P90</em></p></td>
<td><p>—</p></td>
<td><p>2000s-</p></td>
<td><p>[34]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>共和國總統軍事助手總隊</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[35]</p></td>
</tr>
<tr class="even">
<td><p>反恐部隊</p></td>
<td><p>—</p></td>
<td><p>150</p></td>
<td><p>2002-</p></td>
<td><p>[36]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/多米尼加共和國武裝部隊.md" title="wikilink">多米尼加共和國武裝部隊</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[37]</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>350</p></td>
<td><p>2002-</p></td>
<td><p>[38]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>特警單位</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/法國陸軍.md" title="wikilink">法國陸軍</a></p></td>
<td><p><em>P90</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[39]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[40]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/國家憲兵干預組.md" title="wikilink">法國國家憲兵干預組</a></p></td>
<td><p><em>P90 TR</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[41]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/國家警察干預組.md" title="wikilink">法國國家警察干預組</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[42]</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[43][44]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/法國海軍陸戰隊.md" title="wikilink">法國海軍陸戰隊</a></p></td>
<td><p><em>P90</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[45]</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>保護小組</p></td>
<td><p><em>P90 TR</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[46]</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[47][48]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/希臘警察.md" title="wikilink">希臘警察</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[49]</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><em>P90</em></p></td>
<td><p>20</p></td>
<td><p>2009-</p></td>
<td><p>[50]</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><em>P90</em>,<br />
<em>P90 TR</em></p></td>
<td><p>—</p></td>
<td><p>2008-</p></td>
<td><p>[51]</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[52]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/印尼陸軍.md" title="wikilink">印尼陸軍</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/愛爾蘭陸軍遊騎兵.md" title="wikilink">愛爾蘭陸軍遊騎兵</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>2003-</p></td>
<td><p>[53]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/義大利陸軍.md" title="wikilink">義大利陸軍</a></p></td>
<td><p><em>P90 TR</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[54]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/約旦軍事.md" title="wikilink">約旦武裝部隊</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[55]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/科威特軍事.md" title="wikilink">科威特軍隊</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>2003-</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[56]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>（部份被<a href="../Page/利比亞國民解放軍.md" title="wikilink">利比亞國民解放軍繳獲</a>）</p></td>
<td><p>—</p></td>
<td><p>367</p></td>
<td><p>2008-</p></td>
<td><p>[57][58]</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><em>P90 TR</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[59][60]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/馬來西亞皇家海軍.md" title="wikilink">馬來西亞皇家海軍</a><a href="../Page/海军特种作战部队.md" title="wikilink">海军特种作战部队</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[61]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>總統安全大隊</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[62]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>多個警察單位</p></td>
<td><p><em>P90</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[63]</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><em>P90</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[64]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[65]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>特種部隊單位</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[66]</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[67]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>rowspan=|</p></td>
<td><p>安全干預小組</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/荷蘭皇家陸軍.md" title="wikilink">荷蘭皇家陸軍</a>（取代<a href="../Page/烏茲衝鋒槍.md" title="wikilink">烏茲衝鋒槍</a>）</p></td>
<td><p><em>P90 TR</em></p></td>
<td><p>—</p></td>
<td><p>2000-</p></td>
<td><p>[68][69][70]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/荷蘭皇家海軍陸戰隊.md" title="wikilink">荷蘭皇家海軍陸戰隊</a></p></td>
<td><p><em>P90 TR</em></p></td>
<td><p>—</p></td>
<td><p>2001-</p></td>
<td><p>[71][72]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/荷蘭皇家憲兵隊.md" title="wikilink">荷蘭皇家憲兵隊</a></p></td>
<td><p>−</p></td>
<td><p>−</p></td>
<td><p>−</p></td>
<td><p>[73]</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><em>P90</em></p></td>
<td><p>100</p></td>
<td><p>2011-</p></td>
<td><p>[74]</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[75]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>要員保護單位</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[76]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/秘魯武裝部隊.md" title="wikilink">秘魯武裝部隊特種部隊單位</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[77][78][79]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/傘兵.md" title="wikilink">傘兵部隊</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[80]</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/菲律賓國家警察.md" title="wikilink">菲律賓國家警察</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[81]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/波蘭軍事.md" title="wikilink">波蘭武裝部隊</a><a href="../Page/行動應變及機動組.md" title="wikilink">行動應變及機動組</a></p></td>
<td><p><em>P90 TR</em></p></td>
<td><p>—</p></td>
<td><p>2006-</p></td>
<td><p>[82]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><em>P90</em></p></td>
<td><p>—</p></td>
<td><p>2007-</p></td>
<td><p>[83]</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/特別行動組_(葡萄牙).md" title="wikilink">特別行動組</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>2002-</p></td>
<td><p>[84]</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[85]</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><em>P90</em></p></td>
<td><p>500</p></td>
<td><p>1992-</p></td>
<td><p>[86][87][88]</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>500</p></td>
<td><p>2002-</p></td>
<td><p>[89][90][91]</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><em>P90 TR</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[92][93]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西班牙空軍.md" title="wikilink">西班牙空軍特種部隊單位</a></p></td>
<td><p><em>P90</em>,<br />
<em>P90 TR</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[94]</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/蘇利南軍事.md" title="wikilink">蘇利南武裝部隊</a></p></td>
<td></td>
<td><p>900</p></td>
<td><p>2001-</p></td>
<td><p>[95][96]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/中華民國陸軍.md" title="wikilink">中華民國陸軍</a></p></td>
<td><p><em>P90</em></p></td>
<td><p>—</p></td>
<td><p>1992-</p></td>
<td><p>[97][98]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/泰國皇家陸軍.md" title="wikilink">泰國皇家陸軍特別單位</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[99]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/特立尼達和多巴哥國防軍.md" title="wikilink">特立尼達和多巴哥國防軍</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[100]</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[101]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[102]</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[103][104][105]</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>執法機構</p></td>
<td><p><em>P90 LV</em></p></td>
<td><p>30</p></td>
<td><p>2008-</p></td>
<td><p>[106]</p></td>
</tr>
<tr class="odd">
<td><p>總統衞隊</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>西米德蘭茲警察</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>"P90"</p></td>
<td><p>—</p></td>
<td><p>2001-</p></td>
<td><p>[107][108][109]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/美國移民局.md" title="wikilink">美國移民局</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[110]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/美國特勤局.md" title="wikilink">美國特勤局</a></p></td>
<td><p><em>P90 TR</em></p></td>
<td><p>—</p></td>
<td><p>1990s-</p></td>
<td><p>[111][112][113]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>德州Addison警隊</p></td>
<td><p><em>PS90 TR</em></p></td>
<td><p>52</p></td>
<td><p>2007-</p></td>
<td><p>[114][115][116]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿拉斯加州.md" title="wikilink">阿拉斯加州警隊</a></p></td>
<td><p><em>P90 LV</em></p></td>
<td><p>9</p></td>
<td><p>—</p></td>
<td><p>[117]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿拉巴馬州.md" title="wikilink">阿拉巴馬州</a><a href="../Page/特種武器和戰術部隊.md" title="wikilink">特種武器和戰術部隊</a></p></td>
<td><p><em>P90</em></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[118][119]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>德州<a href="../Page/布賴恩.md" title="wikilink">布賴恩市特種武器和戰術部隊</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[120]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/加利福尼亞州.md" title="wikilink">加利福尼亞州Chula</a> Vista特種武器和戰術部隊</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[121]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/密西西比州.md" title="wikilink">密西西比州警察</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[122]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/休斯頓.md" title="wikilink">休斯頓警方</a></p></td>
<td><p><em>P90</em></p></td>
<td><p>5</p></td>
<td><p>1999-</p></td>
<td><p>[123]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/賓夕凡尼亞州.md" title="wikilink">賓夕凡尼亞州警隊</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[124]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/堪薩斯州.md" title="wikilink">堪薩斯州奧拉西警隊</a></p></td>
<td><p>—</p></td>
<td><p>23</p></td>
<td><p>2001-</p></td>
<td><p>[125]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新澤西州.md" title="wikilink">新澤西州</a><a href="../Page/巴賽克縣.md" title="wikilink">巴賽克縣特種武器和戰術部隊</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>2002-</p></td>
<td><p>[126]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>南加州特別反應部隊</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>2000-</p></td>
<td><p>[127]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/南達科他州.md" title="wikilink">南達科他州特種武器和戰術部隊</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[128]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/德克薩斯州.md" title="wikilink">德克薩斯州</a><a href="../Page/麥克倫南縣_(德克薩斯州).md" title="wikilink">麥克倫南縣警局</a></p></td>
<td><p>−</p></td>
<td><p>−</p></td>
<td><p>−</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>德克薩斯州<a href="../Page/薩帕塔縣_(德克薩斯州).md" title="wikilink">薩帕塔縣警局</a></p></td>
<td><p>−</p></td>
<td><p>−</p></td>
<td><p>−</p></td>
<td><p>[129]</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>MPPRE要員保護小組</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[130]</p></td>
</tr>
<tr class="odd">
<td><p>COPEMI</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[131]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>委內瑞拉空軍CSAR</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[132]</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[133][134]</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[135]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>部份警隊</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[136][137]</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 流行文化

### 電影

  - 2002年—《[-{zh-hans:武者回归; zh-hk:武者回歸;
    zh-hant:回歸者;}-](../Page/武者回歸.md "wikilink")》：在未來世界中有P90出現。
  - 2003年—《[-{zh-hk:變種特攻2; zh-tw:X戰警2;
    zh-cn:X战警2;}-](../Page/X戰警2.md "wikilink")》（X-Men 2）
  - 2004年—《[-{zh-hant:暴力特區;
    zh-hans:暴力街区;}-](../Page/暴力特區.md "wikilink")》：被[法國執法機構警員所使用](../Page/法國執法機構.md "wikilink")。
  - 2010年—《[-{zh-hk:轟天猛將; zh-tw:浴血任務;
    zh-cn:敢死队;}-](../Page/浴血任務.md "wikilink")》（The
    Expendables）：型號為P90 TR，被「陰陽」（[李連杰飾演](../Page/李連杰.md "wikilink")）所使用。
  - 2011年—《[-{zh-hk:狂野時速5; zh-tw:玩命關頭5;
    zh-cn:速度与激情5;}-](../Page/玩命關頭5.md "wikilink")》（Fast
    Five）：在槍戰中文斯（麥特·蘇茲飾演）使用P90。
  - 2011年—《[-{zh-hans:机械师;zh-hant:極速秒殺;zh-hk:秒速殺機;}-](../Page/极速秒杀.md "wikilink")》（The
    Mechanic）：型號為P90和P90 TR，被迪恩·桑德森的守衛與殺手所使用。
  - 2013年—《[-{zh-hk:白宫淪陷; zh-tw:全面攻佔：倒數救援;
    zh-cn:奥林匹斯的陷落;}-](../Page/全面攻佔：倒數救援.md "wikilink")》（Olympus
    Has Fallen）：型號為P90 TR，裝上Aimpoint
    Comp[紅點鏡](../Page/紅點鏡.md "wikilink")，被[美國特勤局和緊急應變組的隊員所使用](../Page/美國特勤局.md "wikilink")。
  - 2013年ㄧ《[-{zh-hk:白宮末日; zh-tw:白宮末日;
    zh-cn:惊天危机;}-](../Page/白宮末日.md "wikilink")》：被[美國特勤局及旗下的戰術應變部隊所使用](../Page/美國特勤局.md "wikilink")。
  - 2014年—《[-{zh-hk:轟天猛將3; zh-tw:浴血任務3;
    zh-cn:敢死队3;}-](../Page/敢死队3.md "wikilink")》（The
    Expendables
    3）：為標準型，被露娜（[龍達·魯西飾演](../Page/龍達·魯西.md "wikilink")）和約翰·史麥利（[凱蘭·魯茲飾演](../Page/凱蘭·魯茲.md "wikilink")）所使用。

### 電視片集

  - 《[星際之門](../Page/星際之門.md "wikilink")》

整個《星際之門》影集系列中，主角及配角所使用的主要武器幾乎都是P90。該型槍除了容易隱藏以便外交任務使用外，高穿透力的子彈和大容量彈匣也是有效對付賈法、死靈等外星敵對勢力士兵的利器。

  - 《[CSI犯罪現場](../Page/CSI犯罪現場.md "wikilink")》12季第9及第11集

描述一批FN P90流入拉斯維加斯的一連串故事。

### 電子遊戲

  - 1998年—《[辐射2](../Page/辐射2.md "wikilink")》（Fallout 2）：命名为「H\&K
    P90C」，改为发射10毫米口径子弹。
  - 1999年—《[-{zh-hans:反恐精英;zh-hant:絕對武力}-](../Page/絕對武力.md "wikilink")》系列（1.6、[-{zh-hans:零点行动;zh-hant:一觸即發}-](../Page/絕對武力：一觸即發.md "wikilink")、[-{zh-hans:起源;zh-hant:次世代}-](../Page/絕對武力：次世代.md "wikilink")）（Counter-Strike）：型號為P90，命名為「ES
    C90」，奇怪地發射時彈殼會從側邊拋出（-{zh-hans:起源;zh-hant:次世代}-版本修正了這個問題），而且無法使用瞄準鏡。
  - 2001年—《[辐射战略版：钢铁兄弟会](../Page/辐射战略版：钢铁兄弟会.md "wikilink")》（Fallout
    Tactics: Brotherhood Of Steel）：命名为「FN P90C」，改为发射9毫米口径子弹。
  - 2004年—《[-{zh-hans:孤岛惊魂;zh-hant:孤島驚魂}-](../Page/孤島驚魂_\(遊戲\).md "wikilink")》：型號為P90，奇怪地發射時彈殼會從側邊拋出。
  - 2007年—《[穿越火线](../Page/穿越火线.md "wikilink")》（Crossfire）：型号为P90与P90TR：
      - P90命名为“P90”，50发弹匣，无法使用瞄准镜。没有军衔限制，可使用9500GP购买并永久使用。
      - P90TR命名为“乱世”，采用绿白相间的蜂窝式样涂装，使用50发弹匣却奇怪的可装填60发弹药。可以使用右键切换双持状态，单弹匣变为120发（两枪各60发），落点散布扩大；挑战模式默认为双持状态，单弹匣为180发（两枪各90发）并拥有充能功能，充能完毕后使用右键更换为300发（两枪各150发）的特殊弹匣。奇怪的是弹匣模型依然为50发弹匣的模型。
  - 2007年—《[-{zh-hans:反恐精英;zh-hant:絕對武力}-Online](../Page/絕對武力Online.md "wikilink")》（Counter-Strike
    Online）：型號為P90，命名為「ES C90」，隨遊戲登場武器，奇怪地發射時彈殼會從側邊拋出，而且無法使用瞄準鏡。
  - 2007年—《[-{zh-hans:使命召唤4：现代战争;zh-hant:決勝時刻4：現代戰爭;zh-hk:使命召喚4：現代戰爭}-](../Page/決勝時刻4：現代戰爭.md "wikilink")》（Call
    of Duty 4: Modern
    Warfare）：型號為P90TR，命名為**P90**，配備從導軌裝上的[H\&K樣式鼓型後](../Page/黑克勒-科赫.md "wikilink")[照準器和冠頂狀前](../Page/照準器.md "wikilink")[準星](../Page/準星.md "wikilink")，並在上機匣左邊的[皮卡汀尼導軌裝有](../Page/皮卡汀尼導軌.md "wikilink")[雷射瞄準器](../Page/雷射瞄準器.md "wikilink")（無法使用）。在故事模式中被[俄羅斯](../Page/俄羅斯.md "wikilink")[極端民族主義黨武裝勢力所使用](../Page/極端民族主義.md "wikilink")。聯機模式於等級40解鎖，並可使用[紅點鏡](../Page/紅點鏡.md "wikilink")、[消音器及](../Page/抑制器.md "wikilink")[ACOG光學瞄準鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")。
  - 2007年—《[战地之王](../Page/战地之王.md "wikilink")》：型号为P90，不可改造。可升级为P90
    TR，可以改造。
  - 2009年—《[-{zh-hans:使命召唤：现代战争2;zh-hant:決勝時刻：現代戰爭2;zh-hk:使命召喚：現代戰爭2}-](../Page/決勝時刻：現代戰爭2.md "wikilink")》（Call
    of Duty: Modern Warfare
    2）：型號為P90TR，命名為**P90**，配備從導軌裝上的[H\&K樣式鼓型後](../Page/黑克勒-科赫.md "wikilink")[照準器和冠頂狀前](../Page/照準器.md "wikilink")[準星](../Page/準星.md "wikilink")，並在上機匣左邊的[皮卡汀尼導軌裝有](../Page/皮卡汀尼導軌.md "wikilink")[雷射瞄準器](../Page/雷射瞄準器.md "wikilink")（無法使用）。在故事模式中被[俄羅斯](../Page/俄羅斯.md "wikilink")[極端民族主義黨武裝力量及](../Page/極端民族主義.md "wikilink")所使用。聯機模式於等級24解鎖，並可使用增加射速、[紅點鏡](../Page/紅點鏡.md "wikilink")、[消音器](../Page/抑制器.md "wikilink")、[ACOG光學瞄準鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")、、[雙持](../Page/雙持.md "wikilink")、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")、[熱能探測式瞄具及延長彈匣](../Page/熱輻射.md "wikilink")（增至75發）。
  - 2009年—《[俠盜獵車手：夜生活之曲](../Page/俠盜獵車手：夜生活之曲.md "wikilink")》（Grand Theft
    Auto: The Ballad of Gay Tony）：命名為「突擊衝鋒槍」。
  - 2010年—《[榮譽勳章](../Page/榮譽勳章_\(2010年遊戲\).md "wikilink")》（Medal of
    Honor）：型號為P90TR，奇怪地彈殼會在前下方排出。聯機模式中被聯軍和[塔利班雙方特戰兵所使用](../Page/塔利班.md "wikilink")。
  - 2011年—《[-{zh-hans:战地3;zh-hant:戰地風雲3}-](../Page/戰地風雲3.md "wikilink")》（Battlefield
    3）：型號為P90TR，使用“機械瞄具”時會配備透過位於上[機匣的戰術導軌上安裝的售後市場覘孔式瞄具](../Page/機匣.md "wikilink")。聯機模式中被歸類為個人防衛武器（PDW）並於40級解鎖，能夠搭配多種瞄準具、雷射瞄準器、滅音器等。
  - 2011年—《[-{zh-hans:使命召唤：现代战争3;zh-hant:決勝時刻：現代戰爭3;zh-hk:使命召喚：現代戰爭3}-](../Page/決勝時刻：現代戰爭3.md "wikilink")》（Call
    of Duty: Modern Warfare
    3）：型號為P90TR，命名為**P90**，配備從導軌裝上的[H\&K樣式鼓型後](../Page/黑克勒-科赫.md "wikilink")[照準器和冠頂狀前](../Page/照準器.md "wikilink")[準星](../Page/準星.md "wikilink")，並在上機匣左邊的[皮卡汀尼導軌裝有](../Page/皮卡汀尼導軌.md "wikilink")[雷射瞄準器](../Page/雷射瞄準器.md "wikilink")（無法使用）。在故事模式中被[俄羅斯](../Page/俄羅斯.md "wikilink")[極端民族主義黨武裝力量及同心圈成員所使用](../Page/極端民族主義.md "wikilink")。聯機模式於等級38解鎖，而在生存模式則於等級46解鎖，價錢為$2,000。可使用增加射速、[紅點鏡](../Page/紅點鏡.md "wikilink")、[消音器](../Page/抑制器.md "wikilink")、[ACOG光學瞄準鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")、[HAMR瞄準鏡](../Page/HAMR瞄準鏡.md "wikilink")、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")、[熱能探測式瞄具及延長彈匣](../Page/熱輻射.md "wikilink")（增至75發）。
  - 2012年—《[-{zh-hans:使命召唤Online;zh-hant:決勝時刻Online;zh-hk:使命召喚Online}-](../Page/使命召喚Online.md "wikilink")》（Call
    of Duty Online）： 型號為P90TR，命名為**P90**。在故事模式中被暗影军团及141特勤队成员所使用。
  - 2012年—《[-{zh-hans:反恐精英：全球攻势;zh-hant:絕對武力：全球攻勢}-](../Page/絕對武力：全球攻勢.md "wikilink")》（Counter-Strike:
    Global Offensive）：型號為P90
    TR，與前作一樣，右鍵無功能，命名為「P90」，造型上配備透過位於上機匣的戰術導軌上安裝的售後市場覘孔式瞄具。
      - 由于P90拥有弹量高、射击稳定、射速高等优点，加上游戏内地图“dust2”中恐怖份子陣營能更快地前往爆破点B并易守难攻。所以扮演恐怖份子的玩家经常选择“使用P90，立即冲去占领爆破点B并驻守”的简便战术来取胜，产生了专有名词“Rush
        B”，与P90同义。
  - 2012年—《[自由之心](../Page/自由之心.md "wikilink")》
  - 2012年—《[战争前线](../Page/战争前线.md "wikilink")》（Warface）：型号为P90TAC，命名为“FN
    P90”，在枪托位置与弹匣末端分别卷有一圈绿色胶带，50发[弹匣](../Page/弹匣.md "wikilink")，为工程兵专用武器。在商城内以K点贩卖，可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、冲锋枪消音器、[冲锋枪制退器](../Page/炮口制动器.md "wikilink")、[冲锋枪刺刀](../Page/刺刀.md "wikilink")）以及瞄准镜（[EoTech
    553全息瞄准镜](../Page/全息瞄准镜.md "wikilink")、绿点全息瞄准镜、[红点瞄准镜](../Page/红点瞄准镜.md "wikilink")、冲锋枪普通瞄准镜、冲锋枪高级瞄准镜、[冲锋枪专家瞄准镜](../Page/Leupold_Mark_4_CQ/T光学瞄准镜.md "wikilink")），不可改装战术导轨。
  - 2013年—《[-{zh-hans:收获日;zh-hant:劫薪日}-2](../Page/劫薪日2.md "wikilink")》（Payday
    2）：型號為P90 TR，命名為「Kobus 90」，使用“機械瞄具”時會配備透過位於上機匣的戰術導軌上安裝的售後市場開放式瞄具。
  - 2013年—《[-{zh-hans:战地4;zh-hant:戰地風雲4}-](../Page/戰地風雲4.md "wikilink")》（Battlefield
    4）：型號為P90TR，命名為「P90」，50+1發彈匣，使用“機械瞄具”時會配備透過位於上機匣的戰術導軌上安裝的售後市場覘孔式瞄具。單機模式時能夠被主角丹尼爾·雷克（Daniel
    Recker）所使用，亦是漢娜的常用武器。聯機模式中完成單機戰役小任務「和平使者」後解鎖，被歸類為[個人防衛武器](../Page/個人防衛武器.md "wikilink")，可被工程兵所使用。
  - 2015年—《[-{zh-hans:战地：硬仗;
    zh-hant:戰地風雲：強硬路線;}-](../Page/战地：硬仗.md "wikilink")》（Battlefield
    Hardline）：型號為P90TR（改用重槍管後會變得像PS90），命名為「P90」，歸類為衝鋒槍，50+1發彈匣，預設裝上重[槍管](../Page/槍管.md "wikilink")，被匪方機械師（Mechanic）所使用（警察解鎖條件為：以任何陣營進行遊戲使用該槍擊殺1250名敵人後購買武器執照），價格為$30,000。使用“機械瞄具”時會配備透過位於上機匣的戰術導軌上安裝的售後市場覘孔式瞄具。可加裝各種瞄準鏡（[反射](../Page/紅點鏡.md "wikilink")、眼鏡蛇、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")（放大1倍）、PKA-S（放大1倍）、Micro
    T1、SRS 02、[Comp
    M4S](../Page/Aimpoint_Comp_M4紅點鏡.md "wikilink")、[M145](../Page/C79光學瞄準鏡.md "wikilink")（放大3.4倍）、PO（放大3.5倍）
    、[ACOG](../Page/先進戰鬥光學瞄準鏡.md "wikilink")（放大4倍）、[PSO-1](../Page/PSO-1光學瞄準鏡.md "wikilink")（放大4倍）、[IRNV](../Page/夜視儀.md "wikilink")（放大1倍）、[FLIR](../Page/熱成像儀.md "wikilink")（放大2倍））、附加配件（傾斜式紅點鏡、[電筒](../Page/電筒.md "wikilink")、[戰術燈](../Page/戰術燈.md "wikilink")、[激光瞄準器](../Page/激光指示器.md "wikilink")）及槍口零件（[槍口制退器](../Page/砲口制動器.md "wikilink")、補償器、重[槍管](../Page/槍管.md "wikilink")、[抑制器](../Page/抑制器.md "wikilink")、消焰器）。劇情模式當中則能夠被主角尼古拉斯·門多薩所使用。
  - 2015年—《[-{zh-hans:彩虹六号：围攻;
    zh-hant:虹彩六號：圍攻行動;}-](../Page/虹彩六號：圍攻行動.md "wikilink")》（Rainbow
    Six:
    Siege）：型號為P90TR，命名為「P90」，使用“機械瞄具”時會配備透過位於上[機匣的戰術導軌上安裝的售後市場覘孔式瞄具](../Page/機匣.md "wikilink")，被[法國國家憲兵干預組所使用](../Page/國家憲兵干預組.md "wikilink")。
  - 2016年—《[殺戮空間2](../Page/殺戮空間2.md "wikilink")》：型號為P90TR，命名為「P90
    SMG」，裝上Trijicon RX30紅點鏡。
  - 2016年—《[-{zh-hans:使命召唤：现代战争;zh-hant:決勝時刻：現代戰爭;zh-hk:使命召喚：現代戰爭}-重製版](../Page/決勝時刻4：現代戰爭.md "wikilink")》（Call
    of Duty: Modern Warfare Remastered）：型號為P90
    TR，外觀比前作更為精細，而且能讓玩家透過彈匣檢視殘彈量。
  - 2017年—《[-{zh-hans:幽灵行动：荒野;
    zh-hant:火線獵殺：野境;}-](../Page/火線獵殺：野境.md "wikilink")》（Tom
    Clancy's Ghost Recon: Wildlands)
  - 2019年—《[少女前線](../Page/少女前線.md "wikilink")》：型號為P90，製造時間為2:29。

### 動畫

  - 2002年—《[禁獵魔女](../Page/禁獵魔女.md "wikilink")》（Witch Hunter
    ROBIN）\#15集\[138\]
  - 2002年—《[-{zh-cn:全金属狂潮; zh-tw:驚爆危機;
    zh-hk:驚爆危機;}-](../Page/驚爆危機.md "wikilink")》（Full
    Metal Panic）：動畫第5話，米斯里魯假裝聯合國救援部隊時所使用。
  - 2002年—《[神槍少女](../Page/神槍少女.md "wikilink")》（GUNSLINGER
    GIRL）：型號為P90，被荷莉葉特所使用。
  - 2008年—《[苍色骑士](../Page/苍色骑士.md "wikilink")》（ブラスレイター,BLASSREITER）
  - 2011年—《[日常](../Page/日常.md "wikilink")》（にちじょう,Every Day）
  - 2011年—《[IS〈Infinite
    Stratos〉](../Page/IS〈Infinite_Stratos〉.md "wikilink")》：為其中一位來自法國的女主角─夏綠蒂·迪努亞所操作的IS（作品中的女用空戰人形機甲）「疾風·里凡穆特裝二型」（）所使用，槍身比現實要巨大
  - 2012年—《[軍火女王](../Page/軍火女王.md "wikilink")》（ヨルムンガンド，Jörmungand）
  - 2012年—《[雏蜂](../Page/雛蜂.md "wikilink")》：琉璃所使用的武器
  - 2018年—《[刀劍神域外傳Gun Gale
    Online](../Page/刀劍神域外傳Gun_Gale_Online.md "wikilink")》：為小比類巻香蓮（蓮）在線上遊戲Gun
    Gale Online（GGO）的武器，粉紅色塗裝。

### 輕小說

  - 2014年—《[刀劍神域外傳Gun Gale
    Online](../Page/刀劍神域外傳Gun_Gale_Online.md "wikilink")》：為小比類巻香蓮（蓮）在線上遊戲Gun
    Gale Online（GGO）的武器，粉紅色塗裝。

## 参考文献

  - —[FN product
    description](http://www.fnherstal.com/index.php?id=268&backPID=262&productID=63&pid_product=294&pidList=262&categorySelector=4&detail=)

  - —[FNH USA product
    description](https://web.archive.org/web/20071012093506/http://fnhusa.com/contents/tw_p90.htm)

  - —[Remtek.com: FN P90 examined in
    detail](http://remtek.com/arms/fn/p90/data/concept.htm)

  - —[Modern Firearms—FN P90](http://world.guns.ru/smg/be/fn-p90-e.html)

  - —[Nazarian\`s Gun\`s Recognition Guide: FN P90 presentation video
    （MPEG）](http://www.nazarian.no/wep.asp?id=153&group_id=4&country_id=52&p=8)

  - —[Video of the P90](http://www.youtube.com/watch?v=-UMX3UOh28M)

  - —[D Boy Gun World（P90 PDW）](http://firearmsworld.net/fn/p90/p90.htm)

## 相關

  - 鎗械

<!-- end list -->

  - [CBJ-MS](../Page/CBJ-MS.md "wikilink")
  - [MSMC冲锋枪](../Page/MSMC冲锋枪.md "wikilink")
  - [FN Five-seveN手槍](../Page/FN_Five-seveN手槍.md "wikilink")
  - [AR-57](../Page/AR-57卡賓槍.md "wikilink")
  - [HK MP7衝鋒槍](../Page/HK_MP7衝鋒槍.md "wikilink")
  - [新科動力CPW衝鋒槍](../Page/新科動力CPW衝鋒槍.md "wikilink")
  - [FN F2000突擊步槍](../Page/FN_F2000突擊步槍.md "wikilink")
  - [XM29 OICW](../Page/XM29_OICW.md "wikilink")

<!-- end list -->

  - 彈藥

<!-- end list -->

  - [5.7×28mm](../Page/5.7×28mm.md "wikilink")
  - [4.6×30mm](../Page/4.6×30mm.md "wikilink")
  - [9mm](../Page/9×19mm魯格彈.md "wikilink")

<!-- end list -->

  - [犢牛式槍械列表](../Page/犢牛式槍械列表.md "wikilink")

[Category:衝鋒槍](../Category/衝鋒槍.md "wikilink")
[Category:5.7×28毫米槍械](../Category/5.7×28毫米槍械.md "wikilink")
[Category:比利時槍械](../Category/比利時槍械.md "wikilink")

1.

2.

3.
4.
5.
6.
7.
8.
9.
10. pistol-training.com[1](http://pistol-training.com/archives/1740)

11.
12. Gourley, S.; Kemp, I (November 26, 2003）. "The Duellists". Jane's
    Defence Weekly (ISSN: 02653818), Volume 40 Issue 21, pp 26–28.

13. <http://www.fnherstal.com/index.php?id=267>
    FN的網頁上，在5.7毫米子彈的的資料中加上了北約推薦（recommended
    by NATO）的描述

14.

15.

16.

17.

18.

19.

20.
21.

22.

23.

24.

25.

26.

27. Lasterra, Juan Pablo (2008). "La Police Militaire Brésillienne en
    état de Guerre" (in Spanish). Police Pro No. 11 (September 2008).

28.

29.

30.

31.

32.
33.
34.

35.

36. Cf. Raf Sauviller, l.c.（2004）. "Le Registre est un nid à Problèmes –
    Des P90 de la FN ont été Livrésau Surinam et en République
    Dominicaine: de quoi se Scandaliser?" (in French). La Libre Belgique
    (August 26, 2002).

37.
38. Montes, Julio (2003). "Unidades de élite en Centroamérica" (in
    Spanish). Tecnología Militar, 2003. N. 4, pp 16–20.

39.

40.
41.

42.

43.
44.

45.
46.

47. <http://www.geo-army.ge/index.php?option=com_content&view=article&id=481%3A2012-10-09-20-44-43&catid=51%3A2012-03-25-05-57-06&Itemid=175&lang=ka>

48. <http://www.geo-army.ge/index.php?option=com_content&view=article&id=819%3A-26052014-&catid=46%3A2&lang=en>

49.

50.

51.

52.

53.

54. "Col Moschin 9<sup>o</sup> Reggimento d'Assalto Paracadutisti".
    RAIDS Italia Magazine (ISSN: 1721-3460), 2007.

55.

56.

57.
58.

59.
60.

61.

62.

63.

64.

65.
66.
67.
68.
69.

70.

71.
72.

73.

74.

75.

76.

77.
78.
79.

80.

81.
82.

83.

84.

85.

86.
87. "Small Arms Market Survey: Giat France/FN Herstal Belgium" (January
    25, 1992). Jane's Defence Weekly (ISSN: 02653818), Volume 17 Issue
    4, p 127.

88.

89.

90.

91.

92.

93.

94.

95.
96.

97.
98. "Taiwan Faces G11 Snub" (December 1, 1992). Defence UK, Volume 23
    Issue 12, p 6.

99.

100.

101.

102.

103.
104.

105.

106.

107.
108.
109.

110.
111.
112.

113.

114.

115.

116.

117.

118.

119.

120.

121.

122.

123.
124.

125.

126.

127.

128.

129.

130.

131.

132.
133.
134.

135.
136.

137.

138. [影片來源](https://www.youtube.com/watch?v=mxm44RxBQBA&feature=youtu.be&t=14m00s)第14分鐘