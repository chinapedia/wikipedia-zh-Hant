**PS90**是由[Fabrique
Nationale以](../Page/Fabrique_Nationale.md "wikilink")[FN
P90為基礎為美國民用市場開發](../Page/FN_P90衝鋒槍.md "wikilink")，於2005年尾上市的[半自動卡賓槍](../Page/卡賓槍.md "wikilink")。

## 特性

僅有半自動射擊模式的PS90，槍身有橄欖綠與黑色可供選擇，基本上與有自動射擊模式的P90大同小異。

雖然有短槍管的版本，但為了滿足[美國法律的要求及稅務因素](../Page/美國.md "wikilink")，PS90有較長的16英寸槍管（长度包含一个与枪管外部套管一体的消焰器）、附上的彈匣容量限制在10或30發及僅有半自動射擊模式。

PS90獨特的雙復進簧與後座緩衝墊亦使得PS90的後座力更進一步的降低，初速也增至777m/s，若使用第三方製造的子彈則可以達到930m/s。

Gen1和Gen2的PS90因扳机组可以相对容易的被别有用心者非法改成全自动，因此在gunbroker上的价格一直居高不下。

[PS90_sky.jpg](https://zh.wikipedia.org/wiki/File:PS90_sky.jpg "fig:PS90_sky.jpg")
[SS197_223_comparo.jpg](https://zh.wikipedia.org/wiki/File:SS197_223_comparo.jpg "fig:SS197_223_comparo.jpg")子彈（左）與[5.56×45
NATO子彈](../Page/5.56×45_NATO.md "wikilink")（右）對照\]\]

## 衍生型

  - **PS90 TR（Triple Rail）**—P90
    TR的民用型，[機匣上](../Page/機匣.md "wikilink")、左、右部加裝導軌以對應戰術配件。
  - **PS90 USG**—P90 USG的民用型，改用反射式瞄準鏡取代光學瞄準鏡。

[PS90_1.jpg](https://zh.wikipedia.org/wiki/File:PS90_1.jpg "fig:PS90_1.jpg")
[PS90_breakdown.jpg](https://zh.wikipedia.org/wiki/File:PS90_breakdown.jpg "fig:PS90_breakdown.jpg")

## 使用國

  - ：[德州的Addison警隊](../Page/德州.md "wikilink") (PS90 TR)

## 外部參考

  - [FNH
    USA](https://web.archive.org/web/20070701175325/http://www.fnhusa.com/products/firearms/family.asp?fid=FNF009&gid=FNG006)

  - [FN FiveseveN Forum](http://www.fivesevenforum.com)

  - [PS90 Discussion
    Forum](https://archive.is/20130411163257/http://ps90forum.com/)

  - [PS90 trigger work](http://www.promotedpawn.com)

  - [Remtek.com - FN P90 examined in
    detail](http://remtek.com/arms/fn/p90/data/concept.htm)

  - [FN P90 presentation video
    (MPEG)](http://www.nazarian.no/wep.asp?id=153&group_id=4&country_id=52&p=8)

  - —[D Boy Gun World（FN
    PS90）](http://firearmsworld.net/fn/p90/ps90.htm)

[en:FN P90\#Variants](../Page/en:FN_P90#Variants.md "wikilink")

[Category:半自动步枪](../Category/半自动步枪.md "wikilink")
[Category:5.7×28毫米槍械](../Category/5.7×28毫米槍械.md "wikilink")
[Category:比利時槍械](../Category/比利時槍械.md "wikilink")