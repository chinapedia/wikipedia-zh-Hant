**FTerm**是一個通用仿真[終端軟件](../Page/終端.md "wikilink")。FTerm最初是由[浙江大學笑書亭BBS站長fuse開發用作登錄](../Page/浙江大學.md "wikilink")[笑書亭BBS的工具](../Page/笑書亭BBS.md "wikilink")，隨著版本發展，它除了可用作為[BBS客戶端](../Page/BBS.md "wikilink")，還可以遠程登錄提供終端服務的各種[UNIX類系統主機](../Page/UNIX.md "wikilink")。主要開發者有fuse和[水木清華BBS的kxn](../Page/水木清華BBS.md "wikilink")。

FTerm采用[Delphi作開發環境](../Page/Delphi.md "wikilink")，核心代碼由Francois
Piette編寫的ICS[控件衍生並作較大修改](../Page/控件.md "wikilink")，目前此部分代碼為[開放源代碼](../Page/開放源代碼.md "wikilink")，2007年4月3日發布最後的版本2.5.0.154版。

## 特性

  - 支持[Telnet協議和](../Page/Telnet.md "wikilink")[SSH](../Page/SSH.md "wikilink")；
  - 支持通過[HTTP](../Page/HTTP.md "wikilink")[代理和](../Page/代理服務器.md "wikilink")[Socks4及](../Page/Socks4.md "wikilink")[Socks5代理連接](../Page/Socks5.md "wikilink")；
  - 支持[ZModem仿真](../Page/ZModem.md "wikilink")，可通過Telnet[下載及上傳各類附件](../Page/下載.md "wikilink")；
  - 2.5.89以後版本使用LibCurl，可不打開瀏覽器直接察看BBS Web板面的附件圖片；
  - 通過對QQWry.dat數據庫支持，可以即時察看[IP地址對應來源](../Page/IP地址.md "wikilink")；
  - 支持基本[ANSI控制符](../Page/ANSI控制符.md "wikilink")，能顯示[顏色](../Page/顏色.md "wikilink")、鼠標捲動和隨意變動行列數。FTerm支持行列數根據屏幕大小變換，在變換大小時有時會清歷史屏幕以配合正確的捲動。
  - 特別地，由於在unix中使用mc（Midnight
    Commander）帶來更多方便，因此該[軟件加入](../Page/軟件.md "wikilink")[碼頁](../Page/碼頁.md "wikilink")（codepage）支持。
  - 支持[自定義](../Page/自定義.md "wikilink")[鍵盤與](../Page/鍵盤.md "wikilink")[XTERM](../Page/XTERM.md "wikilink")[鼠標操作](../Page/鼠標.md "wikilink")。
  - 支持[背景](../Page/背景.md "wikilink")、高亮度顯示、閃爍控制符、下劃線等[複制與](../Page/複制.md "wikilink")[貼上](../Page/貼上.md "wikilink")。無太多冗餘控制符。可於不同系統間作複制與貼上。
  - 支持全文下載與支持各種[鼠標操作](../Page/鼠標.md "wikilink")
  - 另外還支持防發呆，自動回覆訊息（小字條），自動應答聊天請求，設置自定義快捷鍵，自定義ANSI色彩等功能。

## 参见

  - [CTerm](../Page/CTerm.md "wikilink")
  - [STerm](../Page/STerm.md "wikilink")
  - [Qterm](../Page/Qterm.md "wikilink")

## 外部連結

  - [Fterm主頁](https://web.archive.org/web/20050507053009/http://fterm.zj001.net/)（已失效）
  - [DelFIG](https://web.archive.org/web/20041016101809/http://fterm.zj001.net/show_hdr.php?xname=6DHH1U0&datapath=mydata%2F317D5U0&xpos=6&ftype=1)字體
  - [kxn的个人blog](http://blog.kangkang.org/)（fterm 2.5.0.139 之后的 build
    在此发布）

[Category:终端软件](../Category/终端软件.md "wikilink")