**FUD**（，意思為**懼、惑、疑**）最早指[IBM销售人员對客戶灌輸](../Page/IBM.md "wikilink")和其他競爭對手產品的負面觀念，在顾客的头脑中注入疑惑与惧怕，使顧客誤以為除了該公司的產品外，他們別無其他選擇。

最早出自[吉恩·阿姆達爾之口](../Page/吉恩·阿姆達爾.md "wikilink")，吉恩·阿姆達爾原為[IBM工程師](../Page/IBM.md "wikilink")，之後離開IBM自行創立，成為IBM競爭對手。

这种行销手法现在经常用于电脑业界，特別是[微软常向客戶宣稱](../Page/微软.md "wikilink")[Linux與其他](../Page/Linux.md "wikilink")[開放原始碼的](../Page/開放原始碼.md "wikilink")[軟體對客戶有弊無利](../Page/軟體.md "wikilink")。\[1\]\[2\]\[3\]

## 參見

  - [訴諸恐懼](../Page/訴諸恐懼.md "wikilink")
  - [萬聖節文件](../Page/萬聖節文件.md "wikilink")

## 外部連結

  - [FUD的定義（英語）](https://web.archive.org/web/20020611090853/http://www.geocities.com/SiliconValley/Hills/9267/fuddef.html)

## 參考資料

[Category:宣傳](../Category/宣傳.md "wikilink")

1.
2.   (NB. This document of the *[Caldera v.
    Microsoft](../Page/Caldera_v._Microsoft.md "wikilink")* case was an
    exhibit in the later *[Comes v.
    Microsoft](../Page/Comes_v._Microsoft.md "wikilink")* case.)
3.