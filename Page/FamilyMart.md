[FamilyMart_logo.svg](https://zh.wikipedia.org/wiki/File:FamilyMart_logo.svg "fig:FamilyMart_logo.svg")

**FamilyMart**（）是源自[日本的](../Page/日本.md "wikilink")[跨國](../Page/跨國公司.md "wikilink")[連鎖](../Page/連鎖店.md "wikilink")[便利商店](../Page/便利商店.md "wikilink")，除了日本之外，在[台灣](../Page/台灣.md "wikilink")、[中國大陸](../Page/中國大陸.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[越南](../Page/越南.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[馬來西亞等地以](../Page/馬來西亞.md "wikilink")[特許經營的方式開設門市](../Page/特許經營.md "wikilink")。目前與[7-Eleven](../Page/7-Eleven.md "wikilink")、[Lawson並列為](../Page/羅森_\(便利店\).md "wikilink")，並在日本所有[都道府縣完成展店](../Page/都道府縣.md "wikilink")。

FamilyMart於1973年由日本連鎖超市[西友創立](../Page/西友.md "wikilink")，原為西友的事業部門之一，至1981年9月1日起成為隸屬於西友旗下的獨立公司；1998年後，西友為解決財務問題，遂將多數股權售予[伊藤忠商事](../Page/伊藤忠商事.md "wikilink")。2016年9月1日，FamilyMart與日本另一連鎖便利商店的母公司（簡稱UGHD）進行股權及業務整合，Circle
K
Sunkus併入FamilyMart品牌，UGHD併入FamilyMart公司，FamilyMart的原公司法人「株式會社FamilyMart」更名轉型為控股公司「」，Circle
K Sunkus的公司法人則同時更名為「株式會社FamilyMart」並繼承FamilyMart的便利商店業務。

## 歷史

[FamilyMart_Niko_chan2.jpg](https://zh.wikipedia.org/wiki/File:FamilyMart_Niko_chan2.jpg "fig:FamilyMart_Niko_chan2.jpg")

  - 1973年：FamilyMart第一家實驗店開設於[埼玉縣](../Page/埼玉縣.md "wikilink")[狹山市水野](../Page/狹山市.md "wikilink")。
  - 1978年：於[千葉縣](../Page/千葉縣.md "wikilink")[船橋市開設第一家特許加盟店](../Page/船橋市.md "wikilink")－大閣三山店，也是FamilyMart第一家[24小時營業的店舖](../Page/24小時.md "wikilink")
  - 1981年：西友商店[資產轉讓](../Page/資產.md "wikilink")，**株式會社FamilyMart**（）成立，同時發表新[Logo](../Page/Logo.md "wikilink")「微笑的[太陽與](../Page/太陽.md "wikilink")[星星](../Page/星星.md "wikilink")」（）
  - 1986年：日本國內店數已達1000家
  - 1987年：成立**沖繩全家股份有限公司**（），開始於[沖繩縣進行展店](../Page/沖繩縣.md "wikilink")
  - 1988年：於[台灣成立](../Page/台灣.md "wikilink")**全家便利商店股份有限公司**並開始展店，係FamilyMart的第一個海外據點
  - 1990年：於[韓國](../Page/韓國.md "wikilink")[首爾成立](../Page/首爾.md "wikilink")**Bokwang
    FamilyMart Co.,Ltd.**，開始在南韓展店
  - 1992年：更換識別標誌，以綠色及藍色為主要顏色；於[泰國](../Page/泰國.md "wikilink")[曼谷市成立](../Page/曼谷市.md "wikilink")**Siam
    FamilyMart Co.,Ltd**，開始在泰國展店
  - 1993年：成立**南九州全家股份有限公司**（），開始於[鹿兒島縣及](../Page/鹿兒島縣.md "wikilink")[宮崎縣展店](../Page/宮崎縣.md "wikilink")
  - 1998年：塊狀商標更改，與1992年發表的識別標誌類似，「微笑的太陽與星星」Logo於日本地區僅標示於店面自動門旁之服務代碼貼紙下方。
  - 2002年：在北韓開設第一家門市（營業至晚間八點）
  - 2002年：阿亞拉集團（The Ayala group）和Rustan超級市場和全家（
    FamilyMart）合作，進軍菲律賓便利商店市場
  - 2004年：開始推廣自家[信用卡](../Page/信用卡.md "wikilink")“Famima Card”（）
  - 2005年：於[美國開設Famima](../Page/美國.md "wikilink")\!\! West Hollywood
    store，FamilyMart開始於美國展店
  - 2006年：成立**北海道全家股份有限公司**（），開始於[北海道地區展店](../Page/北海道.md "wikilink")。
  - 2009年11月13日'''FamilyMart '''同意以120億日圓 (1.33億美元)，自 Rex Holdings
    公司手中收購競爭對手 [Am/pmJapan](../Page/Am/pm.md "wikilink") Co。
  - 2012年6月：韓國FamilyMart宣布，2012年8月起分店將陸續更名為「**CU**」\[1\]。
  - 2016年4月：FamilyMart宣布進軍馬來西亞，並宣佈從2016年12月起的5年內開設300家分店。
  - 2016年9月1日：FamilyMart與的母公司進行業務與股權合併，UGHD與FamilyMart合併為「」，Circle K
    Sunkus的公司法人則同時更名為「株式會社FamilyMart」（第2代）。原屬Circle K Sunkus旗下的“[Circle
    K](../Page/OK便利店.md "wikilink")”和“”2个便利店品牌统一改用「FamilyMart」品牌。
  - 2018年4月19日，[伊藤忠商事以每股](../Page/伊藤忠商事.md "wikilink")11000日圓，斥資1,203億日圓，增持UNY
    FamilyMart控股1090萬股股權，交易完成後，伊藤忠商事於UNY
    FamilyMart控股的持股比例從41.45％增至50.1%。
  - 2018年10月13日，UNY
    FamilyMart控股以2119亿日元（18.9億美元）入股连锁卖场[唐吉訶德母公司](../Page/唐吉訶德_\(企業\).md "wikilink")「」最多
    20.17%的股份，预计成为后者最大股东；唐吉訶德控股同時將支付2.56億美元，收購原屬UNY
    FamilyMart控股旗下的連鎖賣場「[UNY](../Page/UNY.md "wikilink")」全部股權。

## 全球店數

至2018年2月26日，全球总店数为24,346家（中国大陆地区的更新时间为2018年4月24日）\[2\]。

  - ：17,409家

  - ：3,165家

  - ：2,280家

      - [上海](../Page/上海.md "wikilink")：1333家
      - [苏州](../Page/苏州.md "wikilink")：215家
      - [广州](../Page/广州.md "wikilink")：258家
      - [杭州](../Page/杭州.md "wikilink")：162家
      - [成都](../Page/成都.md "wikilink")：87家
      - [深圳](../Page/深圳.md "wikilink")：74家
      - [无锡](../Page/无锡.md "wikilink")：91家
      - [北京](../Page/北京.md "wikilink")：41家
      - [东莞](../Page/东莞.md "wikilink")：19家

  - ：1,138家

  - ：165家

  - ：87家

  - ：66家

  - ：62家\[3\]

      - [雪兰莪](../Page/雪兰莪.md "wikilink")：32家
          - 12家（[八打灵再也](../Page/八打灵再也.md "wikilink")）
          - 8家（[梳邦再也](../Page/梳邦再也.md "wikilink")）
          - 皆2家（[加影](../Page/加影.md "wikilink")、[巴生市](../Page/巴生市.md "wikilink")、[吉隆坡国际机场](../Page/吉隆坡国际机场.md "wikilink")）
          - 皆1家（[赛城](../Page/赛城.md "wikilink")、[黑风洞](../Page/黑风洞.md "wikilink")、[乌鲁冷岳县](../Page/乌鲁冷岳县.md "wikilink")、[沙登](../Page/沙登.md "wikilink")、[莎阿南](../Page/莎阿南.md "wikilink")、[蒲种](../Page/蒲种.md "wikilink")）
      - [吉隆坡](../Page/吉隆坡.md "wikilink")：28家
      - [布城](../Page/布城.md "wikilink")：1家
      - [彭亨](../Page/彭亨.md "wikilink")：1家（[云顶高原](../Page/云顶高原.md "wikilink")）

## 經營特色

### 品牌形象

FamilyMart是以藍色和綠色作為品牌識別基調，主要傳達的印象是洗鍊的都會性及容易親近的信賴感\[4\]。

### 北韓的FamilyMart

南韓BGF零售公司（FamilyMart在南韓的經營業者）在北韓[開城工業區及](../Page/開城工業區.md "wikilink")[金剛山觀光地區開設了三家FamilyMart](../Page/金剛山觀光地區.md "wikilink")，為跨國便利商店首次在北韓開店，引發全球關注。該國為了跟隨[南韓時代的腳步](../Page/南韓.md "wikilink")，但國家也有插手管理，北韓的便利商店和其他的便利商店有很多地方不同，如大部分的便利商店都是二十四小時營業，全年無休，但北韓的FamilyMart卻只有從早上八點到晚上八點，國定假日公休，還有她們沒有像其他國家一樣有現代化的制服，她們統一穿[朝鮮服](../Page/朝鮮服.md "wikilink")。她們不賣咖啡，因為咖啡是美式文化的一種，所以不賣，但有賣紅茶、奶茶、果汁等一些飲料，零食日用品等很少有本國生產，都由[中國大陸](../Page/中國大陸.md "wikilink")、[香港](../Page/香港.md "wikilink")、[臺灣等購買批發](../Page/臺灣.md "wikilink")，其他就和一般的[便利商店都差不多](../Page/便利商店.md "wikilink")。外國人也可去購物，但只能用[歐元](../Page/歐元.md "wikilink")、[美元和](../Page/美元.md "wikilink")[人民幣購買](../Page/人民幣.md "wikilink")。也可以和當地店員談話。

2008年7月，因北韓軍人射殺誤入軍事管制區的南韓觀光客，南韓政府宣佈暫停開放金剛山觀光地區，因此在該地的FamilyMart已暫停營業。2013年亦一度因北韓政府宣布撤離工業區內的北韓工人，工業區內的便利商店亦一同暫時關閉。现时所有北韩的分店不在营运状态。

北韓正討論是否在[平壤](../Page/平壤.md "wikilink")、[開城等城市開設分店](../Page/開城.md "wikilink")。

### 非現金付款機制

  - 日本：可以使用[樂天Edy卡](../Page/Edy.md "wikilink")、[WAON卡和](../Page/永旺集團.md "wikilink")[Suica卡與](../Page/Suica.md "wikilink")[信用卡和](../Page/信用卡.md "wikilink")[银联卡結帳](../Page/中国银联.md "wikilink")（包括在
    [Apple Pay](../Page/Apple_Pay.md "wikilink") 上绑定的
    Suica）以及，微信扫码支付；[支付宝手机扫码支付](../Page/支付宝.md "wikilink")。
  - 台灣：可以使用[悠遊卡](../Page/悠遊卡.md "wikilink")、[一卡通](../Page/一卡通_\(台灣\).md "wikilink")、[信用卡](../Page/信用卡.md "wikilink")(有限定銀行)、感應金融卡以及[中国银联以及結帳](../Page/中国银联.md "wikilink")；手機條碼支付。
  - 南韓：可以使用[T-money卡結帳](../Page/T-money.md "wikilink")。
  - 中国大陸：\[5\]
      - 全国：可在全家发行的积分卡[集享卡中充值余额](../Page/集享卡.md "wikilink")，在开卡所在地的全家店面使用；银联刷卡支付、[云闪付](../Page/云闪付.md "wikilink")（包括
        Apple Pay 与 [Samsung
        Pay](../Page/Samsung_Pay.md "wikilink")）；微信扫码支付；[支付宝手机扫码支付](../Page/支付宝.md "wikilink")。
      - [苏州](../Page/苏州.md "wikilink")：可使用[苏州通市民卡进行消费](../Page/苏州通.md "wikilink")。
      - [广州](../Page/广州.md "wikilink")：可以使用[羊城通卡及其相关产品](../Page/羊城通.md "wikilink")（如：[岭南通](../Page/岭南通.md "wikilink")、[广佛通](../Page/广佛通.md "wikilink")、[岭南通·八达通联名卡](../Page/岭南通·八达通联名卡.md "wikilink")、[天翼羊城通等](../Page/天翼羊城通.md "wikilink")）。

### 门铃

全家在全球各地都使用了[松下](../Page/松下電器.md "wikilink") EC5227WP
门铃。这款门铃于1980年6月1日开始发售\[6\]，由于[稲田康为此款门铃谱写的音乐](../Page/稲田康.md "wikilink")，这种音乐在很多地方成为了全家的标志性特征。当顾客进入全家的时候，门铃就会自动播放这段音乐。\[7\]

## 分店圖片

<File:FamilyMart> Somanikkeshi
Shop.jpg|日本[福島縣](../Page/福島縣.md "wikilink")[相馬市日下石門市](../Page/相馬市.md "wikilink")
<File:FamilyMart> JR Meinohama Station
Shop.jpg|日本[福岡市](../Page/福岡市.md "wikilink")[西区JR](../Page/西区_\(福岡市\).md "wikilink")[姪濱車站門市](../Page/姪濱車站.md "wikilink")
<File:FamilyMart> at Subway Namba Station
01.jpg|日本[大阪市](../Page/大阪市.md "wikilink")[中央区](../Page/中央区_\(大阪市\).md "wikilink")[難波車站南門市](../Page/難波車站.md "wikilink")
<File:FamilyMart> Kyobashi-eki nishi
store.jpg|日本大阪市[都島區](../Page/都島區.md "wikilink")[京橋站前門市](../Page/京橋站_\(大阪府\).md "wikilink")
<File:FamilyMart> Kansai University
store.jpg|日本[大阪府](../Page/大阪府.md "wikilink")[吹田市](../Page/吹田市.md "wikilink")[關西大學門市](../Page/關西大學.md "wikilink")
<File:EXPO> 2005 of Family
Mart.jpg|日本[愛知](../Page/愛知.md "wikilink")[萬國博覽會門市](../Page/萬國博覽會.md "wikilink")
(僅營運於活動期間) <File:Family> Mart
001.jpg|日本[三重縣](../Page/三重縣.md "wikilink")[伊勢市](../Page/伊勢市.md "wikilink")[伊勢神宮内宮前門市](../Page/伊勢神宮.md "wikilink")
<File:FamilyMart> Mintong Store
20120129.jpg|台灣[臺北市](../Page/臺北市.md "wikilink")[松山區民東門市](../Page/松山區.md "wikilink")
<File:Familymart> sunc^n.eco
01.JPG|[南韓](../Page/南韓.md "wikilink")[順天市環保門市](../Page/順天市.md "wikilink")
<File:201701> FamilyMart Store in Soi Chom
Sombun.jpg|[泰国](../Page/泰国.md "wikilink")[曼谷](../Page/曼谷.md "wikilink")[挽叻縣的门店](../Page/挽叻縣.md "wikilink")
<File:FamilyMart> Yoshinoya Jl Bulungan Jakarta
Selatan.JPG|[印尼](../Page/印尼.md "wikilink")[雅加達](../Page/雅加達.md "wikilink")[南雅加達市門市](../Page/南雅加達行政市.md "wikilink")
<File:Kaesong> familymart.jpg|北韓[開城特級市開城門市](../Page/開城特級市.md "wikilink")
(已經閉店) <File:FamilyMart> store at the departure floor of ZBAA T3
(20171207084848).jpg|中国[北京市](../Page/北京市.md "wikilink")[首都机场T](../Page/北京首都国际机场.md "wikilink")3航站楼门市
<File:201609> FamilyMart Store at Disney Resort
Station.jpg|中国[上海](../Page/上海.md "wikilink")[迪士尼站门店](../Page/迪士尼站_\(上海\).md "wikilink")
<File:Suzhou> Shantang
Familymart.JPG|中国[苏州市](../Page/苏州市.md "wikilink")[山塘街门市](../Page/山塘街.md "wikilink")
<File:FM> ML
METRO.jpg|中国[深圳市](../Page/深圳市.md "wikilink")[孖嶺站门市](../Page/孖嶺站.md "wikilink")
<File:FamilyMart> in Xili,
Shenzhen.jpg|深圳[西丽的全家](../Page/西丽街道.md "wikilink")

## 註釋

## 相關條目

  - [全家便利商店股份有限公司](../Page/全家便利商店.md "wikilink")
  - [伊藤忠商事株式會社](../Page/伊藤忠商事株式會社.md "wikilink")

## 外部連結

{{ external media | align = right | width = 180px | image1 =
韓國[東國大學內的](../Page/東國大學.md "wikilink")[FamilyMart](http://web.dongguk.ac.kr/mbs/kr/images/contents/img_06040208.jpg)門市（該門市已改為）
| image2 = | image3 = | audio1 = | audio2 = | audio3 = }}

  - [あなたと、コンビに、FamilyMart｜FamilyMart
    日本FamilyMart](http://www.family.co.jp/)

  - [famima.com ファミマ・ドット・コム －店頭受取りで送料無料－](http://famima.com/)

  - [FamilyMart ::: แฟมิลี่มาร์ท เคียงข้างคุณทุกเวลา :::
    泰國FamilyMart](http://www.familymart.co.th/)

  - [Famima\!\! - 美國FamilyMart](http://famima-usa.com/)

  - [全家冰珍珠飲品](http://wandatw.com/lets-cafe/)

  - [台灣FamilyMart](http://www.family.com.tw)

  - [全家FamilyMart\~全家就是你家 中国FamilyMart](http://www.familymart.com.cn/)

  -
[Category:日本便利商店](../Category/日本便利商店.md "wikilink")
[Category:總部在日本的跨國公司](../Category/總部在日本的跨國公司.md "wikilink")
[Category:豐島區公司](../Category/豐島區公司.md "wikilink")
[Category:1973年建立](../Category/1973年建立.md "wikilink")
[Category:2001年成立的公司](../Category/2001年成立的公司.md "wikilink")
[3337](../Category/東京證券交易所已除牌公司.md "wikilink")

1.  ，并转由BGF零售公司管理，FamilyMart现已全面推出韩国市场。
2.
3.
4.
5.  [便民服務](http://www.familymart.com.cn/service/amusementCommunication)
6.   メロディサイン（２種音）（ホワイト）／Ｐ {{\!}} 品番詳細 {{\!}}
    Panasonic|accessdate=2016-10-10|work=www2.panasonic.biz}}
7.