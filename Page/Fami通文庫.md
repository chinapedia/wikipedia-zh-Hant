**Fami通文庫**（，），現是[日本](../Page/日本.md "wikilink")[出版社](../Page/出版社.md "wikilink")[KADOKAWA旗下](../Page/KADOKAWA.md "wikilink")[enterbrain出版的](../Page/enterbrain.md "wikilink")[輕小說文庫](../Page/輕小說.md "wikilink")。1998年由Aspect出版社創立，2000年ASCII集團重整後轉到enterbrain旗下。

前身為Aspect發行的Logout冒險文庫（1993年－1996年）、Logout文庫（1997年－1998年）和Fami通遊戲文庫（1996年－1998年）。

## 簡介

Logout冒險文庫以出版[Falcom](../Page/Falcom.md "wikilink")、HUMMINGBIRD
SOFT公司作品的小說化、以及紙上RPG遊戲的遊戲歷程為主，Logout文庫則出版原創的作品。Fami通遊戲文庫除了繼承一部分作品外，和前兩者沒有直接關聯。此外，Fami通遊戲文庫在出版大型電玩、家用遊戲的小說作品外，也出版題材廣泛的原創作品，因而改名為「Fami通文庫」。

現在有重新出版紙上RPG的遊戲歷程。在2006年，也出版了規則書籍。

書背的「FB」字樣、紅色代表原創作品，綠色代表是小說化作品與紙上遊戲相關書籍。2005年7月後出版作品的書背設計有部分更動（青色色調沒有改變）。

有新人獎「[Entame大獎小說部門](../Page/Entame大獎小說部門.md "wikilink")」。

新書表原本和enterbrain其他的漫畫、攻略本等合在一起，在2006年10月改為獨立的「Famitsu Bunko News」（FBN）。

## 作品一覽

### 台灣角川

  - 卡莉（[高殿円](../Page/高殿円.md "wikilink")／[羽住都](../Page/羽住都.md "wikilink")）
  - [學校的階梯](../Page/學校的階梯.md "wikilink")（[櫂末高彰](../Page/櫂末高彰.md "wikilink")／）
  - [吉永家的石像怪](../Page/吉永家的石像怪.md "wikilink")（[田口仙年堂](../Page/田口仙年堂.md "wikilink")／[日向悠二](../Page/日向悠二.md "wikilink")）
  - [超妹大戰](../Page/超妹大戰.md "wikilink")（[古橋秀之](../Page/古橋秀之.md "wikilink")／[内藤隆](../Page/内藤隆.md "wikilink")）
  - [東宮家的石像怪](../Page/東宮家的石像怪.md "wikilink")（田口仙年堂／日向悠二）
  - [喪女會的不當日常](../Page/喪女會的不當日常.md "wikilink")（／）
  - [黑鋼的魔紋修復士](../Page/黑鋼的魔紋修復士.md "wikilink")（[嬉野秋彥](../Page/嬉野秋彥.md "wikilink")/）
  - [賢者之孫](../Page/賢者之孫.md "wikilink")（[吉岡剛](../Page/吉岡剛.md "wikilink")/）

### 尖端出版

  - [狂亂家族日記](../Page/狂亂家族日記.md "wikilink")（[日日日](../Page/日日日.md "wikilink")／[x6suke](../Page/x6suke.md "wikilink")）
  - [魔法學園MA](../Page/魔法學園MA.md "wikilink")（[榊一郎](../Page/榊一郎.md "wikilink")／[BLADE](../Page/BLADE.md "wikilink")）
  - [月兔公主](../Page/月兔公主.md "wikilink")（[野村美月](../Page/野村美月.md "wikilink")／）
  - [文學少女](../Page/文學少女.md "wikilink")（野村美月／[竹岡美穗](../Page/竹岡美穗.md "wikilink")）
  - [魔界戰記迪斯凱亞](../Page/魔界戰記.md "wikilink")（[神代創](../Page/神代創.md "wikilink")／[超肉](../Page/超肉.md "wikilink")）
  - [狂戀聲優寶貝](../Page/狂戀聲優寶貝.md "wikilink")（[木本雅彥](../Page/木本雅彥.md "wikilink")／）
  - [笨蛋，測驗，召喚獸](../Page/笨蛋，測驗，召喚獸.md "wikilink")（[井上堅二](../Page/井上堅二.md "wikilink")／）
  - [第108年的初戀](../Page/第108年的初戀.md "wikilink")（[末永外徒](../Page/末永外徒.md "wikilink")／）
  - [魔壺妖精](../Page/魔壺妖精.md "wikilink")（[鯛津裕太](../Page/鯛津裕太.md "wikilink")／）
  - [幽靈少女與科學少年](../Page/幽靈少女與科學少年.md "wikilink")（[飛田甲](../Page/飛田甲.md "wikilink")／）
  - [PEACE@PIECES死神候補生](../Page/PEACE@PIECES死神候補生.md "wikilink")（[小林正親](../Page/小林正親.md "wikilink")／）
  - [七色★星露](../Page/七色★星露.md "wikilink")（[市川環](../Page/市川環.md "wikilink")／）
  - [戰孃交響曲](../Page/戰孃交響曲.md "wikilink")（[築地俊彥](../Page/築地俊彥.md "wikilink")／[赤賀博隆](../Page/赤賀博隆.md "wikilink")）
  - [幽靈勇者](../Page/幽靈勇者.md "wikilink")（神代創／、超肉）
  - [Bad\!
    Daddy親親壞老爹](../Page/Bad!_Daddy親親壞老爹.md "wikilink")（野村美月／[煉瓦](../Page/煉瓦.md "wikilink")）
  - [偶像大師](../Page/偶像大師.md "wikilink")（／）
  - [疾走！青春期的帕拉貝倫](../Page/疾走！青春期的帕拉貝倫.md "wikilink")（[深見真](../Page/深見真.md "wikilink")／[うなじ](../Page/うなじ.md "wikilink")）
  - [我的愛馬很兇惡](../Page/我的愛馬很兇惡.md "wikilink")（[新井輝](../Page/新井輝.md "wikilink")／[緋鍵龍彥](../Page/緋鍵龍彥.md "wikilink")）
  - [深林危機](../Page/深林危機.md "wikilink")（[木村航](../Page/木村航.md "wikilink")／）
  - [魔法米克斯](../Page/魔法米克斯.md "wikilink")（榊一郎／BLADE）
  - [歡迎光臨美少女遊戲世界](../Page/歡迎光臨美少女遊戲世界.md "wikilink")（[田尾典丈](../Page/田尾典丈.md "wikilink")／）
  - [赤裸裸之戰](../Page/赤裸裸之戰.md "wikilink")（[花谷敏嗣](../Page/花谷敏嗣.md "wikilink")／）
  - [心連‧情結](../Page/心連情結.md "wikilink")（[庵田定夏](../Page/庵田定夏.md "wikilink")／[白身魚](../Page/白身魚.md "wikilink")）
  - [B.A.D.事件簿](../Page/B.A.D.事件簿.md "wikilink")（[綾里惠史](../Page/綾里惠史.md "wikilink")／[kona](../Page/kona.md "wikilink")）
  - [空色感染爆發](../Page/空色感染爆發.md "wikilink")（[本田誠](../Page/本田誠.md "wikilink")／[庭](../Page/庭.md "wikilink")）
  - [光在地球之時……](../Page/光在地球之時…….md "wikilink")（野村美月／竹岡美穗）
  - [狗與剪刀必有用](../Page/狗與剪刀必有用.md "wikilink")（[更伊俊介](../Page/更伊俊介.md "wikilink")／）
  - [我與男生與青春期妄想的她們](../Page/我與男生與青春期妄想的她們.md "wikilink")（／）
  - [割耳奈露莉](../Page/割耳奈露莉.md "wikilink")（[石川博品](../Page/石川博品.md "wikilink")／）
  - [廢柴一看就上手的「鐮足同學式」受歡迎入門](../Page/廢柴一看就上手的「鐮足同學式」受歡迎入門.md "wikilink")（[石川博品](../Page/石川博品.md "wikilink")/[一真](../Page/一真.md "wikilink")）
  - [龍孃七七七埋藏的寶藏](../Page/龍孃七七七埋藏的寶藏.md "wikilink")（[鳳乃一真](../Page/鳳乃一真.md "wikilink")／）
  - [東雲侑子熱愛短篇小說](../Page/東雲侑子系列.md "wikilink")（／[Nardack](../Page/Nardack.md "wikilink")）
  - [女裝皇家教師](../Page/女裝皇家教師.md "wikilink")（野村美月／[karory](../Page/karory.md "wikilink")）
  - [四百二十連敗女孩](../Page/四百二十連敗女孩.md "wikilink")（桐山成人／[七桃りお](../Page/七桃りお.md "wikilink")）
  - [這份戀情，與其未來。](../Page/這份戀情，與其未來。.md "wikilink")
    (／[Nardack](../Page/Nardack.md "wikilink"))

### 青文出版社

  - [三月，七日。](../Page/三月，七日。.md "wikilink")（／）
  - [蓓蓓與BB團](../Page/蓓蓓與BB團.md "wikilink")（[田口仙年堂](../Page/田口仙年堂.md "wikilink")／）
  - [貝維克戰記](../Page/貝維克戰記.md "wikilink")（[冰上慧一](../Page/冰上慧一.md "wikilink")／[森田和明](../Page/森田和明.md "wikilink")、[松本規之](../Page/松本規之.md "wikilink")）
  - [Bird Heart Beat
    舞姬天翔](../Page/Bird_Heart_Beat_舞姬天翔.md "wikilink")（[伊東京一](../Page/伊東京一.md "wikilink")／[Pako](../Page/Pako.md "wikilink")）
  - [魔物獵人](../Page/魔物獵人.md "wikilink")（／）
  - [大家的阿葵兒！](../Page/大家的阿葵兒！.md "wikilink")（[淺沼廣太](../Page/淺沼廣太.md "wikilink")／）
  - [鋼鐵白兔騎士團](../Page/鋼鐵白兔騎士團.md "wikilink")（[舞阪洸](../Page/舞阪洸.md "wikilink")／）
  - [君吻](../Page/君吻.md "wikilink")（[日暮茶坊](../Page/日暮茶坊.md "wikilink")／[高山箕犀](../Page/高山箕犀.md "wikilink")、[加茂](../Page/加茂.md "wikilink")）
  - [PULP](../Page/PULP.md "wikilink")（／[隼優紀](../Page/隼優紀.md "wikilink")）
  - [塵骸魔京](../Page/塵骸魔京.md "wikilink")（[海法紀光](../Page/海法紀光.md "wikilink")／）
  - [暴風Girls
    Fight](../Page/暴風Girls_Fight.md "wikilink")（／[倉藤倖](../Page/倉藤倖.md "wikilink")）
  - [噬神戰士
    打破禁忌之人](../Page/噬神戰士_打破禁忌之人.md "wikilink")（／[曾我部修司](../Page/曾我部修司.md "wikilink")）
  - [百億魔女物語](../Page/百億魔女物語.md "wikilink")（[竹岡葉月](../Page/竹岡葉月.md "wikilink")／）
  - [我的她是戰爭妖精](../Page/我的她是戰爭妖精.md "wikilink")（[嬉野秋彥](../Page/嬉野秋彥.md "wikilink")／）
  - [星塵中隊　離星空最近之處](../Page/星塵中隊_離星空最近之處.md "wikilink")（[foca](../Page/foca.md "wikilink")／）
  - [魔物獵人 EPISODE \~
    novel.](../Page/魔物獵人系列.md "wikilink")（[冰上慧一](../Page/冰上慧一.md "wikilink")／[布施龍太](../Page/布施龍太.md "wikilink")）
  - 鼻血店長就不行嗎？（[新木伸](../Page/新木伸.md "wikilink")／[火曜](../Page/火曜.md "wikilink")）
  - [翠星上的加爾岡緹亞](../Page/翠星上的加爾岡緹亞.md "wikilink")（[谷村大四郎](../Page/谷村大四郎.md "wikilink")／[Production
    I.G](../Page/Production_I.G.md "wikilink")）
  - [大圖書館的牧羊人](../Page/大圖書館的牧羊人.md "wikilink")（[田尾典丈](../Page/田尾典丈.md "wikilink")／）
  - [學校的暗殺社](../Page/學校的暗殺社.md "wikilink")（[深見真](../Page/深見真.md "wikilink")／）

### 東立出版社

  - [惡狼難馴\!?](../Page/惡狼難馴!?.md "wikilink")（[黑川實](../Page/黑川實.md "wikilink")／[武藤此史](../Page/武藤此史.md "wikilink")）
  - [斬鬼夜鳥子](../Page/斬鬼夜鳥子.md "wikilink")（[桝田省治](../Page/桝田省治.md "wikilink")／[佐嶋真實](../Page/佐嶋真實.md "wikilink")）
  - [蓋棺論定！](../Page/蓋棺論定！.md "wikilink")（／）
  - [虎躍龍笑](../Page/虎躍龍笑.md "wikilink")（[嬉野秋彦](../Page/嬉野秋彦.md "wikilink")／）
  - [煙囟町的赤魔與絕望少年](../Page/煙囟町的赤魔與絕望少年.md "wikilink")（／[師走貴志](../Page/師走貴志.md "wikilink")）
  - [白銀的卡露](../Page/白銀的卡露.md "wikilink")（／）
  - [開朗的家族砲計畫！](../Page/開朗的家族砲計畫！.md "wikilink")（[新木伸](../Page/新木伸.md "wikilink")／）
  - [掀起世界危機！](../Page/掀起世界危機！.md "wikilink")（[佐藤了](../Page/佐藤了.md "wikilink")／[藤真拓哉](../Page/藤真拓哉.md "wikilink")）
  - [創立\!?三星學生會](../Page/創立!?三星學生會.md "wikilink")（／[大場陽炎](../Page/大場陽炎.md "wikilink")）
  - [家庭遊戲](../Page/家庭遊戲.md "wikilink")（[築地俊彥](../Page/築地俊彥.md "wikilink")／[河原惠](../Page/河原惠.md "wikilink")）
  - [U.F.O.未確認飛行](../Page/U.F.O.未確認飛行.md "wikilink")（[大橋英高](../Page/大橋英高.md "wikilink")／）
  - [魔法少女的黑色狂想曲](../Page/魔法少女的黑色狂想曲.md "wikilink")（[根木健太](../Page/根木健太.md "wikilink")／[kino](../Page/kino.md "wikilink")）

### 天闻角川

  - [笨蛋、测验、召唤兽](../Page/笨蛋、测验、召唤兽.md "wikilink")（[井上坚二](../Page/井上坚二.md "wikilink")／[叶贺唯](../Page/叶贺唯.md "wikilink")）
  - [心灵链环](../Page/心灵链环.md "wikilink")（[庵田定夏](../Page/庵田定夏.md "wikilink")／[白身鱼](../Page/白身鱼.md "wikilink")）
  - [光在地球之时……](../Page/光在地球之时…….md "wikilink")（[野村美月](../Page/野村美月.md "wikilink")／[竹冈美穗](../Page/竹冈美穗.md "wikilink")）
  - [狗与剪刀的正确用法](../Page/狗与剪刀的正确用法.md "wikilink")（[更伊俊介](../Page/更伊俊介.md "wikilink")／[锅岛哲弘](../Page/锅岛哲弘.md "wikilink")）

### 99读书人

  - [文學少女](../Page/文學少女.md "wikilink")（[野村美月](../Page/野村美月.md "wikilink")／[竹岡美穗](../Page/竹岡美穗.md "wikilink")）

## 關聯條目

  - [enterbrain](../Page/enterbrain.md "wikilink")
  - [Entame大獎小說部門](../Page/Entame大獎小說部門.md "wikilink")
  - [Light Novel Award](../Page/Light_Novel_Award.md "wikilink")
      - [富士見Fantasia文庫](../Page/富士見Fantasia文庫.md "wikilink")
      - [富士見Mystery文庫](../Page/富士見Mystery文庫.md "wikilink")
      - [電擊文庫](../Page/電擊文庫.md "wikilink")
      - [角川Sneaker文庫](../Page/角川Sneaker文庫.md "wikilink")

## 外部連結

  - [Fami通文庫官方網站](http://www.enterbrain.co.jp/fb/)
  - [台灣角川官方網站](https://www.kadokawa.com.tw/)
  - [台灣角川線上購物](https://shop.kadokawa.com.tw/index.php)
  - [尖端出版SPP網站](http://www.spp.com.tw/)

[\*](../Category/Fami通文庫.md "wikilink")