**FastMail**一项由[电子邮件供应商](../Page/电子邮件.md "wikilink")[Messaging
Engine提供的](../Page/Messaging_Engine.md "wikilink")[电子邮件服务](../Page/电子邮件.md "wikilink")，因最早提供免费[IMAP邮件服务而出名](../Page/IMAP.md "wikilink")。2010年，該公司被[Opera收購](../Page/Opera_Software.md "wikilink")。2013年9月26日，FastMail宣布從Opera分離，成為一家私人控股的獨立公司\[1\]。至今FastMail在国際上仍是最受欢迎的[IMAP邮件服务之一](../Page/IMAP.md "wikilink")，而在中国则受到[Gmail对](../Page/Gmail.md "wikilink")[IMAP的支持的冲击](../Page/IMAP.md "wikilink")。FastMail拥有数十个提供服务的域名。

在一項非正式的評比中，FastMail的安全性與[Gmail並列A級第二](../Page/Gmail.md "wikilink")，A級第一是[Mail.de](https://Mail.de)。A級的條件：除了User
- Server間的傳輸用TLS 1.2加密之外，Server - Server的送信、收信也必須支援TLS
1.2加密，才能保證傳輸的任何一段過程皆不被偷窺。世界各國Email服務中能達到頂尖的不到5家。但FastMail由於伺服器設在侵犯網路隱私惡名昭彰的美國（關鍵字：[稜鏡計畫](../Page/稜鏡計畫.md "wikilink")），因此雖有技術上的安全性，仍不能保證美國政府不透過施壓業者的方式、不需竊聽即可取得通訊內容；所以FastMail和Gmail在這項評比中被列為第二\[2\]。

## 註腳

## 外部链接

  - [FastMail 主站点](http://www.fastmail.fm/)
  - [FastMail CN域名](http://www.fastmail.cn/)
  - [Messaging Engine 主页](http://www.messagingengine.com/)

[Category:电子邮件网站](../Category/电子邮件网站.md "wikilink")

1.
2.