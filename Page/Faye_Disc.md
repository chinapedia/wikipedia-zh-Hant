《**Faye
Disc**》是[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")[王菲的第二張](../Page/王菲.md "wikilink")[EP](../Page/EP.md "wikilink")，於1994年5月出版。一共4首歌，1首新歌。這是她將中文名從「王靖雯」改回「王菲」後出版的第一張唱片。唱片有橙粉色布袋包裝。

## 曲目

**備註：**

  - <sup></sup>表示聯合監製 。
  - 「只因喜歡Faye」是改编自曲佑良、鄭怡主唱的《從來都沒有》。

[Category:王菲音樂專輯](../Category/王菲音樂專輯.md "wikilink")
[Category:1994年音樂專輯](../Category/1994年音樂專輯.md "wikilink")
[Category:1994年5月](../Category/1994年5月.md "wikilink")