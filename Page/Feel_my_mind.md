《**feel my
mind**》（心靈感應）是[日本歌手](../Page/日本.md "wikilink")[倖田來未的第三張原創專輯](../Page/倖田來未.md "wikilink")，收錄《[COME
WITH ME](../Page/COME_WITH_ME.md "wikilink")》、《[Gentle
Words](../Page/Gentle_Words.md "wikilink")》、《[Crazy 4
U](../Page/Crazy_4_U.md "wikilink")》等三張單曲，此張專輯的成績已到達公信榜第7名，比上張專輯前進一名，由此可見倖田來未的專輯成績已漸漸進步，知名度亦愈來愈高。由於上張專輯中的一張電玩主題曲"real
Emotion
真實情感"漸漸走紅以來，倖田來未的潛力漸漸被大家發覺，而這種情況也可以從這張專輯的成績進步而得到驗證，似乎也預告著下一張專輯她即將爆紅、有如鹹魚翻身的命運…

## 解說

  - 初回盤的Bonus Track為「夢 with
    You」的[混音版](../Page/混音.md "wikilink")，又加入將於後來發行於單曲中的熱門歌曲「[甜心戰士](../Page/LOVE&HONEY.md "wikilink")」。

<!-- end list -->

  - 倖田來未的親妹[misono所屬的團體](../Page/misono.md "wikilink")[近未來](../Page/近未來.md "wikilink")，也在同日發行專輯「primary
    colors」獲得[Oricon第五名](../Page/Oricon.md "wikilink")。

## 發行版本

  - CD ONLY

## 曲目

  - ※僅初回收錄

<!-- end list -->

1.  **Break it down**
2.  **[Crazy 4 U](../Page/Crazy_4_U.md "wikilink")**
      -
        10th
        Single。[動畫](../Page/動畫.md "wikilink")「[甜心戰士](../Page/甜心戰士.md "wikilink")」片頭曲。
3.  **Rock Your Body**
4.  **Rain**
      -
        單曲「[No Regret](../Page/No_Regret.md "wikilink")」收錄本曲的Unplugged
        Version。
5.  **Without Your Love**
      -
        「Gentle Words」的B面曲。
6.  **Talk to you**
7.  **華**
8.  **Get Out The Way**
      -
        [日本電視台](../Page/日本電視台.md "wikilink")「スポーツうるぐす」片尾曲。
9.  **Sweet love・・・**
10. **Gentle Words**
      -
        9th single。[Do As
        Infinity的](../Page/Do_As_Infinity.md "wikilink")[長尾大作曲的情歌](../Page/長尾大.md "wikilink")。
        佐藤製薬「ストナ」廣告曲
11. **magic**
      -
        [SONY](../Page/SONY.md "wikilink")・[Playstation
        2的遊戲](../Page/Playstation_2.md "wikilink")「クリムゾンティアーズ」片尾曲。
12. **COME WITH ME**
      -
        8th single。
        チョーヤ梅酒「梅ゼリー」廣告曲
13. **夢 with You(R．Yamaki’s　Groove　Mix)** ※
      -
        久保田利伸混音。「[Crazy 4 U](../Page/Crazy_4_U.md "wikilink")」收錄的為另一版本。
14. **[甜心戰士](../Page/LOVE&HONEY.md "wikilink")** ※
      -
        收錄於後來5月26日的新單曲「[LOVE\&HONEY](../Page/LOVE&HONEY.md "wikilink")」中。

## 資料來源

[日本維基百科 Feel my mind](http://ja.wikipedia.org/wiki/Feel_my_mind)

## 相關條目

[Category:2004年音樂專輯](../Category/2004年音樂專輯.md "wikilink")
[Category:倖田來未音樂專輯](../Category/倖田來未音樂專輯.md "wikilink")