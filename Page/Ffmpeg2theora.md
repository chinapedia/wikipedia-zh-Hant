**ffmpeg2theora**編碼器，可以將任何格式的影音媒體檔案轉換成 [ogg](../Page/ogg.md "wikilink")
檔案。作者以傳統的[C語言編寫這套軟體](../Page/C語言.md "wikilink")，主要是利用大量的[資料結構和](../Page/資料結構.md "wikilink")[指標來編寫程式](../Page/指標.md "wikilink")。由於這個編碼器是採用[命令列的操作模式](../Page/命令列.md "wikilink")，對於不熟悉命令列介面的人來說較為不便。

## 相關條目

  - [Ogg](../Page/Ogg.md "wikilink")
  - [FFmpeg](../Page/FFmpeg.md "wikilink")
  - [Theora](../Page/Theora.md "wikilink")

## 外部連結

  - [ffmpeg2theora 官方網站](http://v2v.cc/~j/ffmpeg2theora/)

  - [存放 ffmpeg2theora 原始碼的目錄](http://svn.xiph.org/trunk/ffmpeg2theora/)

[F](../Category/自由软件.md "wikilink") [F](../Category/开放源代码.md "wikilink")
[F](../Category/视频编解码器.md "wikilink")
[F](../Category/音频编解码器.md "wikilink")