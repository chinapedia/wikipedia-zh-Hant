<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p><a href="../Page/安德烈·萨哈罗夫.md" title="wikilink">安德烈·萨哈罗夫和</a><a href="../Page/伊戈尔·库尔恰托夫.md" title="wikilink">伊戈尔·库尔恰托夫</a>，<a href="../Page/1958年.md" title="wikilink">1958年</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p><a href="http://www.hro.org/editions/saharov/sah_kurc_1958_1.jpg">http://www.hro.org/editions/saharov/sah_kurc_1958_1.jpg</a></p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p>{{#switch:</p></td>
</tr>
</tbody>
</table>

## 合理使用理据

虽然这图像拥有版权，但我认为这符合美国合理使用法例，因为：

1.  这是一幅摄于1958年的老照片，几乎没有商业价值，版权所有人为俄罗斯政府，其用于商业用途的机会不大；
2.  这是一幅低解像度的对原图像的复制品，把它印刷后照片质量相对较低；
3.  这幅图像罕有，且具有历史价值，同时显示苏联聚变炸弹之父和裂变炸弹之父
4.  照片中两人均已过身，所以没可能有自由版权图像代替