## 摘要

| 描述摘要 | 《納尼亞傳奇：獅子、女巫、魔衣櫥》電影海報                                                                                                              |
| ---- | ---------------------------------------------------------------------------------------------------------------------------------- |
| 來源   | [Internet Movie Poster Awards](http://www.impawards.com/2005/posters/chronicles_of_narnia_the_lion_the_witch_and_the_wardrobe.jpg) |
| 日期   | 2007-11-11                                                                                                                         |
| 作者   | [Internet Movie Poster Awards](http://www.impawards.com/2005/posters/chronicles_of_narnia_the_lion_the_witch_and_the_wardrobe.jpg) |
| 許可   | {{\#switch:                                                                                                                        |

## 许可协议