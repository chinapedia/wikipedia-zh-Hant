<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p><a href="../Page/:en:Darlington_F.C..md" title="wikilink">-{zh-hans:达灵顿;zh-hk:達寧頓}-會徽</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p>英文維基 Image:Darlo.gif</p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td><p>2007.12.14</p></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td><p>作者</p></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p>{{#switch: 用Photoshop Elements更改為png格式</p></td>
</tr>
</tbody>
</table>

### Fair-use rationale in Darlington F.C.

1.  obtained from the above website
    www.football.co.uk/darlington/index.shtml
2.  low resolution image
3.  no non-copyright version available, by definition
4.  the logo is only being used for informational purposes
5.  its inclusion in the article adds significantly to the article
    because it is the primary means of identifying the subject of this
    article

[英格兰](../Category/足球俱乐部标志.md "wikilink")