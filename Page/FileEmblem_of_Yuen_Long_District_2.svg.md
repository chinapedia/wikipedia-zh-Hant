## 摘要

<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p>香港元朗區區徽</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p><a href="http://www.districtcouncils.gov.hk/yl/doc/tc/activities/dc_logo.jpg">http://www.districtcouncils.gov.hk/yl/doc/tc/activities/dc_logo.jpg</a></p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td><p>2006年8月3日</p></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td><p>元朗區區議會，SVG版由<a href="../Page/User:Wrightbus.md" title="wikilink">Wrightbus繪製</a></p></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p>{{#switch: 檔案其他版本（可留空）</p></td>
</tr>
</tbody>
</table>

## 许可协议

[Category:香港區議會徽號](../Category/香港區議會徽號.md "wikilink")