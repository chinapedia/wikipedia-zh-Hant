from English wikipedia
[:en:Image:Google-censorship.png](../Page/:en:Image:Google-censorship.png.md "wikilink")

## Summary

Fair use because no freely replaceable version exists, it is for
non-commercial purposes, and explains the object in question (Censorship
by Google).

PNG replacement of Image:Google-censorship.jpg, except with the main
(.com) Google website instead of the .fr one.

Note that on the Chinese site one image of the man standing in front of
the tank appears on the first page, but only one, and it's from
someone's MySpace.

## Fair use

## Licensing