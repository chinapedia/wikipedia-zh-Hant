### 合理使用

本圖以**合理使用**條目使用，理由為：

1.  本圖是一幅歷史圖片，記錄歷史事件，迄今未有非版權圖片可以取代、
2.  本圖之獨特性不可取代、
3.  圖片解像度低，缺乏可見的商業價值、
4.  圖片僅用於教育用途，存放在位於美國，非牟利的維基基金會伺服器內，以及
5.  條目質素在加入圖片後得到大大提高。

<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p><a href="../Page/1986年.md" title="wikilink">1986年</a><a href="../Page/10月21日.md" title="wikilink">10月21日</a>，英女皇在皇夫及鄧蓮如等人的見證下，主持香港會議展覽中心一期的奠基典禮。後排右一站立者為<a href="../Page/黃宜弘.md" title="wikilink">黃宜弘</a>。</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p><a href="http://www.hktdc.com/tdcnews/chinese/0611/06112901_09.htm"><a href="http://www.hktdc.com/tdcnews/chinese/0611/06112901_09.htm">http://www.hktdc.com/tdcnews/chinese/0611/06112901_09.htm</a></a></p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td><p><a href="../Page/1986年.md" title="wikilink">1986年</a><a href="../Page/10月21日.md" title="wikilink">10月21日</a></p></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td><p><a href="../Page/香港貿易發展局.md" title="wikilink">香港貿易發展局</a></p></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p>{{#switch: 檔案其他版本（可留空）</p></td>
</tr>
</tbody>
</table>