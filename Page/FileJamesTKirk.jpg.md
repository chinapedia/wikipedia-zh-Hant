## 摘要

这是《[星际旅行](../Page/星际旅行.md "wikilink")》角色[詹姆斯·泰比里厄斯·柯克的一幅截图](../Page/詹姆斯·泰比里厄斯·柯克.md "wikilink")，来自《[星际旅行：初代](../Page/星际旅行：初代.md "wikilink")》的“The
Devil in the Dark”一集。它被用作辨识、评论这名虚构角色“詹姆斯·T·柯克”。

### 合理使用于[詹姆斯·泰比里厄斯·柯克之依据](../Page/詹姆斯·泰比里厄斯·柯克.md "wikilink")

1.  There exists no free alternative, nor can one be created. James T.
    Kirk is a fictional character, all images of him are necessarily
    copyrighted.
2.  This image respects commercial opportunties - a cropped screenshot
    cannot meaningfully compete with a 44 minute episode.
3.  The image represents a minimal use - it is a cropped screenshot -
    less than a single frame from an episode that has over 50 000
    frames. It is of a reasonable low resolution.
4.  The image has been previously published, in the broadcasted, as well
    as sold on DVD Star Trek episode "The Balance of Terror"
5.  The image is of a major character from a culturally significant
    series of television shows. It is encyclopaedic.
6.  The image generally meets the image use policy.
7.  The image is used in at least one article, [James T.
    Kirk](../Page/詹姆斯·泰比里厄斯·柯克.md "wikilink").
8.  The image contributes strongly to the article. As a fictional
    character, the subject is more easily visualised.
9.  The image is used only in the article namespace.
10. The image is from
    <http://tos.trekcore.com/gallery/thumbnails.php?album=51>, the
    copyright is owned by [Paramount
    Pictures](../Page/派拉蒙影业.md "wikilink").

## 许可协议