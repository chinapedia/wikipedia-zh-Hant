### 合理使用

本圖以**合理使用**條目使用，理由為：

1.  本圖**並非**僅僅作為對該人長相的辨識、
2.  本圖是一幅獨一無二的**官式場合圖片**，用於描述羅德丞在[1997年](../Page/1997年.md "wikilink")[7月1日獲授GBM勳銜](../Page/7月1日.md "wikilink")。條目正文，以及榮譽一欄已有交代。圖片的加入，補充了文字之不足，使條目質素大大改善。
3.  羅德丞已經在[2006年逝世](../Page/2006年.md "wikilink")，因此**不可能有自由版權圖片存在**、
4.  羅德丞是**首批**GBM獲勳者，此對香港歷史有重要意義、
5.  圖片僅用於教育用途，存放在位於美國，非牟利的維基基金會伺服器內，以及
6.  條目質素在加入圖片後得到大大提高，條文相關資料的篇幅沒有關係。
7.  合理使用守則從沒有規定圖片的使用要取決於條文相關資料的篇幅。

### 致所有使用者

  - 閣下如對圖片有任何疑問，或者希望更改圖片狀態前，煩請先行[到此討論頁提出](../Page/User_talk:Clithering.md "wikilink")，多謝。

<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p>羅德丞（右二）在<a href="../Page/1997年.md" title="wikilink">1997年</a><a href="../Page/7月1日.md" title="wikilink">7月1日於前港督府獲董建華</a>（左二）頒授大紫荊勳章。</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p><a href="http://www.takungpao.com/news/06/12/12/GW-664079.htm"><a href="http://www.takungpao.com/news/06/12/12/GW-664079.htm">http://www.takungpao.com/news/06/12/12/GW-664079.htm</a></a></p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td><p><a href="../Page/1997年.md" title="wikilink">1997年</a><a href="../Page/7月1日.md" title="wikilink">7月1日</a></p></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td><p>大公報</p></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p>{{#switch: 檔案其他版本（可留空）</p></td>
</tr>
</tbody>
</table>