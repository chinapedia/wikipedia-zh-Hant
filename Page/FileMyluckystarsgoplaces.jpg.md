## Summary

<http://koti.mbnet.fi/~vitriini/kannet/luckystarsgoplaces.jpg>

This image is of the Hong Kong DVD cover for the film *[Lucky Stars Go
Places](../Page/Lucky_Stars_Go_Places.md "wikilink")*, the fourth film
in the Lucky Stars film series. It is used in the article about the
film, [Lucky Stars Go
Places](../Page/Lucky_Stars_Go_Places.md "wikilink") and in the article
that discusses the whole film series, [Lucky
Stars](../Page/Lucky_Stars.md "wikilink"). The image is therefore
illustrative of the both articles' content. The justification /
rationale for using this image is:

  - The image is directly relevant to, or is representative of, the
    article in which it appears.
  - The presence of the image improves and/or gives clarity to the
    article in which it appears.
  - The image is of low size and quality.
  - The image depicts a product, or is used to promote a product. Use of
    the image in an encyclopedic article relevant to that product does
    not harm or diminish the product, it's creators, the original artist
    / designer of the image, the film company, publishers or studios
    that market the product, or any other personnel involved in it's
    production in any way.
  - The image was taken from the website that is sourced above. On the
    source website, the image was used to advertise or review the
    product. Use of the image in a relevant encyclopedic article does
    not harm or diminish the source website's continued use of the image
    for their intended purposes.
  - The image is unlikely to have been edited. However, there are two
    circumstances where it may have been edited by the uploader:

<!-- end list -->

1.  If the original image was large, the uploader may have reduced the
    size prior to uploading to Wikipedia.
2.  If borders, shadows or other effects had been added to the image on
    the source website, these may have been removed by the uploader
    purely for the purpose of returning the exact likeness of the
    original product.

<!-- end list -->

  -
    Neither of these forms of editing affect the other points listed
    here.

[Gram123](../Page/User:Gram123.md "wikilink") 14:23, 6 June 2007 (UTC)

## Licensing