### 合理使用于[尼利克斯之依据](../Page/尼利克斯.md "wikilink")

1.  There exists no free alternative, nor can one be created. Neelix is
    a fictional character, all images of him are necessarily
    copyrighted.
2.  This image respects commercial opportunties - a cropped screenshot
    cannot meaningfully compete with a 44 minute episode.
3.  The image represents a minimal use - it is a cropped screenshot -
    less than a single frame from an episode that has over 50 000
    frames. It is of a reasonable low resolution.
4.  The image has been previously published, in the broadcasted, as well
    as sold on DVD Star Trek Voyager episode "Homestead"
5.  The image is of a major character from a culturally significant
    series of television shows. It is encyclopaedic.
6.  The image generally meets the image use policy.
7.  The image is used in at least one article,
    [Neelix](../Page/Neelix.md "wikilink").
8.  The image contributes strongly to the article. As a fictional
    character, the subject is more easily visualised.
9.  The image is used only in the article namespace.
10. The image is from <http://memory-alpha.org/en/wiki/Neelix>, the
    copyright is owned by [Paramount
    Pictures](../Page/Paramount_Pictures.md "wikilink").