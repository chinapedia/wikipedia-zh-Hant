## 摘要

| 描述摘要 | Pinocchio Movie Poster                 |
| ---- | -------------------------------------- |
| 來源   | <http://www.imdb.com/title/tt0032910/> |
| 日期   | 24-08-2007                             |
| 作者   | Pete                                   |
| 許可   | Template:Movie Poster {{\#switch:      |

## 许可协议

[cy:Delwedd:Pinocchio-1940-poster.jpg](../Page/cy:Delwedd:Pinocchio-1940-poster.jpg.md "wikilink")
[en:<File:Pinocchio-1940-poster.jpg>](../Page/en:File:Pinocchio-1940-poster.jpg.md "wikilink")
[fa:پرونده:Pinocchio-1940-poster.jpg](../Page/fa:پرونده:Pinocchio-1940-poster.jpg.md "wikilink")
[ga:Íomhá:Pinocchio-1940-poster.jpg](../Page/ga:Íomhá:Pinocchio-1940-poster.jpg.md "wikilink")
[he:קובץ:Pinocchio-1940-poster.jpg](../Page/he:קובץ:Pinocchio-1940-poster.jpg.md "wikilink")
[id:Berkas:Pinocchio-1940-poster.jpg](../Page/id:Berkas:Pinocchio-1940-poster.jpg.md "wikilink")
[lt:Vaizdas:Pinocchio1940poster.jpg](../Page/lt:Vaizdas:Pinocchio1940poster.jpg.md "wikilink")
[sh:Datoteka:Pinocchio-1940-poster.jpg](../Page/sh:Datoteka:Pinocchio-1940-poster.jpg.md "wikilink")
[sw:Picha:Pinocchio-1940-poster.jpg](../Page/sw:Picha:Pinocchio-1940-poster.jpg.md "wikilink")
[th:ไฟล์:Pinocchio-1940-poster.jpg](../Page/th:ไฟล์:Pinocchio-1940-poster.jpg.md "wikilink")
[tr:Dosya:Pinocchio-1940-poster.jpg](../Page/tr:Dosya:Pinocchio-1940-poster.jpg.md "wikilink")