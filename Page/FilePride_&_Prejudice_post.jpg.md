## 摘要

{{ Non-free use rationale

`| Article = 傲慢與偏見 (2005年電影)`
`| Description = 2005年的美国电影“Pride & Prejudice”的海报`
`| Source = `<http://img3.douban.com/view/photo/raw/public/p452005185.jpg>
`| Portion = The entire poster: because the image is poster art, a form of product packaging or service marketing, the entire image is needed to identify the product or service, properly convey the meaning and branding intended, and avoid tarnishing or misrepresenting the image.`
`| Low_resolution = The copy is of sufficient resolution for commentary and identification but lower resolution than the original poster. Copies made from it will be of inferior quality, unsuitable as counterfeit artwork, pirate versions or for uses that would compete with the commercial purpose of the original artwork.`
`| Purpose = Main infobox. The image is used for identification in the context of critical commentary of the work, product or service for which it serves as poster art. It makes a significant contribution to the user's understanding of the article, which could not practically be conveyed by words alone.The image is placed in the infobox at the top of the article discussing the work, to show the primary visual image associated with the work, and to help the user quickly identify the work product or service and know they have found what they are looking for.Use for this purpose does not compete with the purposes of the original artwork, namely the creator providing graphic design services, and in turn the marketing of the promoted item.`
`| Replaceability = As film poster art, the image is not replaceable by free content; any other image that shows the same artwork or poster would also be copyrighted, and any version that is not true to the original would be inadequate for identification or commentary.`
`| other_information = Use of the poster art in the article complies with Wikipedia non-free content policy and fair use under United States copyright law as described above.`

}}

## 许可协议