## 摘要

| 描述摘要 | Rescuers Poster                        |
| ---- | -------------------------------------- |
| 來源   | <http://www.imdb.com/title/tt0076618/> |
| 日期   | 24-08-2007                             |
| 作者   | Pete                                   |
| 許可   | Template:Movie Poster {{\#switch:      |

## 许可协议

[en:<File:Rescuersposter.jpg>](../Page/en:File:Rescuersposter.jpg.md "wikilink")
[fa:پرونده:Rescuersposter.jpg](../Page/fa:پرونده:Rescuersposter.jpg.md "wikilink")
[jv:Gambar:Rescuersposter.jpg](../Page/jv:Gambar:Rescuersposter.jpg.md "wikilink")
[lt:Vaizdas:TheRescuersposter.jpg](../Page/lt:Vaizdas:TheRescuersposter.jpg.md "wikilink")
[lv:Attēls:Rescuersposter.jpg](../Page/lv:Attēls:Rescuersposter.jpg.md "wikilink")
[pt:Ficheiro:Rescuersposter.jpg](../Page/pt:Ficheiro:Rescuersposter.jpg.md "wikilink")
[ro:Fișier:Rescuersposter.jpg](../Page/ro:Fișier:Rescuersposter.jpg.md "wikilink")
[ru:Файл:Rescuersposter.jpg](../Page/ru:Файл:Rescuersposter.jpg.md "wikilink")
[sw:Picha:Rescuersposter.jpg](../Page/sw:Picha:Rescuersposter.jpg.md "wikilink")
[ta:படிமம்:Rescuersposter.jpg](../Page/ta:படிமம்:Rescuersposter.jpg.md "wikilink")
[th:ไฟล์:Rescuersposter.jpg](../Page/th:ไฟล์:Rescuersposter.jpg.md "wikilink")
[tr:Dosya:Rescuersposter.jpg](../Page/tr:Dosya:Rescuersposter.jpg.md "wikilink")
[vi:Tập
tin:Rescuersposter.jpg](../Page/vi:Tập_tin:Rescuersposter.jpg.md "wikilink")