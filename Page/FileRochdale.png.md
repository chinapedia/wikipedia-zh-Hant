<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p><a href="../Page/罗奇代尔足球俱乐部.md" title="wikilink">罗奇代尔足球俱乐部會徽</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p>英文維基 Image:Rochdale badge.gif</p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td><p>2007.12.06</p></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td><p><a href="../Page/:en:User:Pal.md" title="wikilink">Pal</a></p></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p>{{#switch: 更改為png格式</p></td>
</tr>
</tbody>
</table>

### Fair use in [罗奇代尔足球俱乐部](../Page/罗奇代尔足球俱乐部.md "wikilink")

Though this image is subject to copyright, its use is covered by the
U.S. fair use laws because:

1.  It is the logo of the organisation featured in the article.
2.  It is the primary means of identification for this team.
3.  It is not replaceable by any photograph or any other logo.
4.  It is a low resolution image.
5.  The image is derived from the borough of Rochdale's arms, which
    belongs to the town anyhow.

[英格兰](../Category/足球俱乐部标志.md "wikilink")