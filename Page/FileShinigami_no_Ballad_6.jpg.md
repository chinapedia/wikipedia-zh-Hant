## 摘要

<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p>《死神的歌謠》第6卷封面</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p><a href="http://www.amazon.co.jp/%E3%81%97%E3%81%AB%E3%81%8C%E3%81%BF%E3%81%AE%E3%83%90%E3%83%A9%E3%83%83%E3%83%89%E3%80%82%E3%80%886%E3%80%89-%E9%9B%BB%E6%92%83%E6%96%87%E5%BA%AB-%E3%83%8F%E3%82%BB%E3%82%AC%E3%83%AF-%E3%82%B1%E3%82%A4%E3%82%B9%E3%82%B1/dp/4840230420/ref=sr_1_2?ie=UTF8&amp;s=books&amp;qid=1195942879&amp;sr=1-2">http://www.amazon.co.jp/%E3%81%97%E3%81%AB%E3%81%8C%E3%81%BF%E3%81%AE%E3%83%90%E3%83%A9%E3%83%83%E3%83%89%E3%80%82%E3%80%886%E3%80%89-%E9%9B%BB%E6%92%83%E6%96%87%E5%BA%AB-%E3%83%8F%E3%82%BB%E3%82%AC%E3%83%AF-%E3%82%B1%E3%82%A4%E3%82%B9%E3%82%B1/dp/4840230420/ref=sr_1_2?ie=UTF8&amp;s=books&amp;qid=1195942879&amp;sr=1-2</a></p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td><p>2005年</p></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td><p>長谷川啟介、MediaWorks</p></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p>{{#switch:</p></td>
</tr>
</tbody>
</table>

## 许可协议

[分類:日本輕小說](../Page/分類:日本輕小說.md "wikilink")