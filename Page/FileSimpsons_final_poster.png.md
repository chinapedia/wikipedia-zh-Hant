## 概要

《[辛普森一家大电影](../Page/辛普森一家大电影.md "wikilink")》海报

## 来源

<http://www.collider.com/entertainment/news/article.asp/aid/4685/tcid/1>

## 合理使用于[辛普森一家大电影之依据](../Page/辛普森一家大电影.md "wikilink")

1.  No free equivalent exists that would effectively identify the
    article's subject.
2.  The image does not in any way limit the ability of the copyright
    owners to market or sell their product.
3.  The image is only used once and is rendered in low resolution to
    avoid piracy.
4.  The image has been published outside Wikipedia; see source above.
5.  The image meets general Wikipedia content requirements and is
    encyclopedic.
6.  The image meets Wikipedia's media-specific policy.
7.  The image is used in the article wiki-linked in the section title.
8.  The image is significant in identifying the subject of the article,
    which is the film itself.
9.  The image is used in the article namespace.
10. The image has a brief description that identifies the image, notes
    the source, and provides attribution to the copyright holder.

## 合理使用于[辛普森一家之依据](../Page/辛普森一家.md "wikilink")

  - This is the movie poster for the movie discussed in the article.
  - Its use here is entirely encyclopedic in nature.
  - It is a low resolution copy.
  - This does not in any way limit the ability of the copyright owners
    to market or sell their product.

## 许可协议