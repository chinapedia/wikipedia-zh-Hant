### Fair Use

The image here is claimed to be **fair use** as it is:

1.  A portrait of one of Hong Kong's most famous politicians;
2.  There are no copyleft alternatives of such a photo;
3.  The image used is of a ceremonial and not a commerical value;
4.  The article is greatly improved by the prominence of this photo;
5.  Which is hosted and displayed on the not-for-profit Wikipedia
    Encylopedia for educational purposes.
6.  The person in the photo died in 1999.

<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p>Sir Robert Black, Governor of Hong Kong from 1958 to 1964.</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p><a href="http://www.grs.gov.hk/PRO/srch/english/library_show_detail_full.jsp?recordkey=1000013059&amp;srchscreen=sys_library&amp;version=internet">HK Government Public Record Office</a></p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td><p>c.1958</p></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td><p>Hong Kong Government</p></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p>{{#switch: 檔案其他版本（可留空）</p></td>
</tr>
</tbody>
</table>