## 合理使用理据

  - The image is of a low resolution, not reproducible in a high quality
    format.
  - The image does not harm the marketability of the original work.
  - The image will be used in an encyclopedic manner, to depict the
    character [Xenu](../Page/Xenu.md "wikilink") as shown in the 2005
    *[South Park](../Page/South_Park.md "wikilink")* episode "[Trapped
    in the
    Closet](../Page/Trapped_in_the_Closet_\(South_Park\).md "wikilink")"
  - Will not be used on templates but only on article mainspace.
  - It does not harm the copyright owner commercially.
  - It contributes significantly to the article by showing this specific
    parody of Scientology and being a key image of the episode.