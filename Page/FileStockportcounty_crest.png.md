<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p>斯托克港足球俱乐部‎會徽</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p>英文維基Image:Stockportcounty crest.png</p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td><p>2007.11.20</p></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td><p>User:CharlieT.</p></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p>{{#switch: 檔案其他版本（可留空）</p></td>
</tr>
</tbody>
</table>

### Fair-use rationale

1.  obtained from the club website
2.  no non-copyright version available, by definition
3.  the logo is only being used for informational purposes
4.  its inclusion in the article adds significantly to the article
    because it is the primary means of identifying the subject of this
    article