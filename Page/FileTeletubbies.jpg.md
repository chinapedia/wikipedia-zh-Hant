<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p>天线宝宝</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p><a href="http://www.pbs.org/teletubbies">http://www.pbs.org/teletubbies</a></p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td><p>2007年6月9日</p></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td><p>不详</p></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p>{{#switch: 档案其他版本（可留空）</p></td>
</tr>
</tbody>
</table>

[ar:ملف:Teletubbies.png](../Page/ar:ملف:Teletubbies.png.md "wikilink")
[cy:Delwedd:Teletubbies.png](../Page/cy:Delwedd:Teletubbies.png.md "wikilink")
[en:<File:Teletubbies.png>](../Page/en:File:Teletubbies.png.md "wikilink")
[fi:Tiedosto:Teletubbies.png](../Page/fi:Tiedosto:Teletubbies.png.md "wikilink")
[hr:Datoteka:Teletubbiesi.jpg](../Page/hr:Datoteka:Teletubbiesi.jpg.md "wikilink")
[id:Berkas:Teletubbies.jpg](../Page/id:Berkas:Teletubbies.jpg.md "wikilink")
[it:<File:Teletubbies.jpg>](../Page/it:File:Teletubbies.jpg.md "wikilink")
[jv:Gambar:Teletubbies.png](../Page/jv:Gambar:Teletubbies.png.md "wikilink")
[ka:ფაილი:Teletubbies.png](../Page/ka:ფაილი:Teletubbies.png.md "wikilink")
[lt:Vaizdas:Teletubbies.png](../Page/lt:Vaizdas:Teletubbies.png.md "wikilink")
[ml:പ്രമാണം:Teletubbies.png](../Page/ml:പ്രമാണം:Teletubbies.png.md "wikilink")
[ms:Fail:Teletubbies.png](../Page/ms:Fail:Teletubbies.png.md "wikilink")
[ru:Файл:Teletubbies.jpg](../Page/ru:Файл:Teletubbies.jpg.md "wikilink")
[si:ගොනුව:Teletubbies.png](../Page/si:ගොනුව:Teletubbies.png.md "wikilink")
[ta:படிமம்:Teletubbies.png](../Page/ta:படிமம்:Teletubbies.png.md "wikilink")
[th:ไฟล์:Teletubbies.png](../Page/th:ไฟล์:Teletubbies.png.md "wikilink")