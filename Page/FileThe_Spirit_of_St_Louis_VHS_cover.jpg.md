## 摘要

Though this image is subject to copyright, its use is covered by the
U.S. fair use laws because:

1.  The article for which this use occurs is purely educational in
    nature and, thus, is non-commercial.
2.  The sole purpose of this use of the copyrighted image is to
    illustrate the DVD in question.
3.  Said image is the primary means of visual identification of the main
    topic of the article; that is, the work that is distributed via the
    DVD media.
4.  The image is used only once in the article, and is used in a section
    or "information box" that is neutral in tone.
5.  The cover of the media is not the "content" specifically being sold
    by the publisher of the media.
6.  The market for the original DVD product will not be diminished by
    this use of the image. In fact, this use is likely to encourage the
    increased legitimate sales of said DVDs.
7.  Because the image is provided at a low resolution inappropriate for
    re-printing the DVD cover, its usage on Wikipedia does not pose a
    problem of aiding media piracy (the re-printing of covers to
    accompany pirated discs), or make it so that the copyright holder
    could not in the future successfully re-sell the image as a print or
    poster on its own.
8.  Since it is copyrighted DVD art, the image is not replaceable with
    an uncopyrighted or freely copyrighted image of comparable
    educational value.
9.  Digital versions of this image have been previously published on
    numerous websites, so this use on Wikipedia does not make the image
    significantly more accessible.
10. The image is not used in such a way that a reader would be confused
    into believing that the article is produced, sponsored, or
    authorized by the owner of the image.

## 许可协议