Non-free / fair use media rationale - non-free logo

  - Description This is a logo for The New Saints F.C..
  - Source The logo may be obtained from The New Saints F.C..
  - Article The New Saints F.C.
  - Portion used The entire logo is used to convey the meaning intended
    and avoid tarnishing or misrepresenting the intended image.
  - Low resolution? The logo is a size and resolution sufficient to
    maintain the quality intended by the company or organization,
    without being unnecessarily high resolution.
  - Purpose of use The image is placed in the infobox at the top of the
    article discussing The New Saints F.C., a subject of public
    interest. The significance of the logo is to help the reader
    identify the organization, assure the reader they have reached the
    right article containing critical commentary about the organization,
    and illustrate the organization's intended branding message in a way
    that words alone could not convey.
  - Replaceable? Because it is a logo there is almost certainly no free
    equivalent. Any substitute that is not a derivative work would fail
    to convey the meaning intended, would tarnish or misrepresent its
    image, or would fail its purpose of identification or commentary.
  - Other information Use of the logo in the article complies with
    Wikipedia non-free content policy, logo guidelines, and fair use
    under United States copyright law as described above.

[威尔士](../Category/足球俱乐部标志.md "wikilink")