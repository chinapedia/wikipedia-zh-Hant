## 摘要

这是一幅《[星际旅行：航海家号](../Page/星际旅行：航海家号.md "wikilink")》角色[汤姆·派里斯在](../Page/汤姆·派里斯.md "wikilink")“”一集中的屏幕截图。它被用作辨识、评论这名虚构人物“汤姆·派里斯”。

### 合理使用于[汤姆·派里斯之依据](../Page/汤姆·派里斯.md "wikilink")

1.  There exists no free alternative, nor can one be created. Tom Paris
    is a fictional character, all images of him are necessarily
    copyrighted.
2.  This image respects commercial opportunties - a cropped screenshot
    cannot meaningfully compete with a 44 minute episode.
3.  The image represents a minimal use - it is a cropped screenshot -
    less than a single frame from an episode that has over 50 000
    frames. It is of a reasonable low resolution.
4.  The image has been previously published, in the broadcasted, as well
    as sold on DVD Star Trek Voyager episode "Endgame"
5.  The image is of a major character from a culturally significant
    series of television shows. It is encyclopaedic.
6.  The image generally meets the image use policy.
7.  The image is used in at least one article, [Tom
    Paris](../Page/汤姆·派里斯.md "wikilink").
8.  The image contributes strongly to the article. As a fictional
    character, the subject is more easily visualised.
9.  The image is used only in the article namespace.
10. The image is from <http://memory-alpha.org/en/wiki/Tom_Paris>, the
    copyright is owned by [Paramount
    Pictures](../Page/派拉蒙影业.md "wikilink").

### 来源

取自[阿尔法记忆的](../Page/阿尔法记忆.md "wikilink")[图片](http://memory-alpha.org/en/wiki/Image:Tom_Paris_%282378%29.jpg)。

## 许可协议