## 摘要

这是一张来自《[星际旅行：航海家号](../Page/星际旅行：航海家号.md "wikilink")》“”一集中的角色[杜沃克的屏幕截图](../Page/杜沃克.md "wikilink")。它被用于辨识、评论这名虚构角色杜沃克。

### 合理使用于[杜沃克之依据](../Page/杜沃克.md "wikilink")

1.  There exists no free alternative, nor can one be created. Tuvok is a
    fictional character, all images of him are necessarily copyrighted.
2.  This image respects commercial opportunties - a cropped screenshot
    cannot meaningfully compete with a 44 minute episode.
3.  The image represents a minimal use - it is a cropped screenshot -
    less than a single frame from an episode that has over 50 000
    frames. It is of a reasonable low resolution.
4.  The image has been previously published, in the broadcasted, as well
    as sold on DVD Star Trek Voyager episode "Repentance"
5.  The image is of a major character from a culturally significant
    series of television shows. It is encyclopaedic.
6.  The image generally meets the image use policy.
7.  The image is used in at least one article,
    [Tuvok](../Page/杜沃克.md "wikilink").
8.  The image contributes strongly to the article. As a fictional
    character, the subject is more easily visualised. Additionally, the
    character is not a human. This means the "lifelike" quality of the
    Tuvok cannot hope to be understood with images.
9.  The image is used only in the article namespace.
10. The image is from <http://memory-alpha.org/en/wiki/Tuvok>, the
    copyright is owned by [Paramount
    Pictures](../Page/派拉蒙影业.md "wikilink").

### 来源

[图像](http://memory-alpha.org/en/wiki/Image:Tuvok2377.jpg)来自[阿尔法记忆](../Page/阿尔法记忆.md "wikilink")。

## 许可协议