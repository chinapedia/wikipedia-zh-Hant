## Rationales

  - 此書照片並非使用作顯示人物長相
  - 此書圖像可有利讀者增加對該書的認識，有助介紹該書本身
  - 此書於[1965年發行](../Page/1965年.md "wikilink")，所以沒有非版權圖像可代替
  - 此圖像用作教育用途，存放於美國非牟利的維基百科伺服器內
  - 此圖像用於其所屬的[葛量洪回憶錄條目內](../Page/葛量洪回憶錄.md "wikilink")，而葛量洪回憶錄條目乃[葛量洪條目之一部份](../Page/葛量洪.md "wikilink")

## 摘要

| 描述摘要 | The inner cover page from the book, *Via Ports: From Hong Kong to Hong Kong*         |
| ---- | ------------------------------------------------------------------------------------ |
| 來源   | photo taken from the said book                                                       |
| 日期   | book published in 1965                                                               |
| 作者   | written by Sir Alexander Grantham, published by HKU Press and printed by Cathy Press |
| 許可   | 版權許可資料 {{\#switch: 檔案其他版本（可留空）                                                       |

## 许可协议