## 摘要

这是《[星际旅行：下一代](../Page/星际旅行：下一代.md "wikilink")》的角色[威廉·赖克在](../Page/威廉·赖克.md "wikilink")“”中的截图。此图用于辨识、评论图中的人物[威廉·赖克](../Page/威廉·赖克.md "wikilink")。

### 在[威廉·赖克条目中合理使用之依据](../Page/威廉·赖克.md "wikilink")

1.  There exists no free alternative, nor can one be created. Riker is a
    fictional character, all images of him are necessarily copyrighted.
2.  This image respects commercial opportunties - a cropped screenshot
    cannot meaningfully compete with a 44 minute episode.
3.  The image represents a minimal use - it is a cropped screenshot -
    less than a single frame from an episode that has over 50 000
    frames. It is of a reasonable low resolution.
4.  The image has been previously published, in the broadcasted, as well
    as sold on DVD Star Trek The Next Generation episode "The Best of
    Both Worlds"
5.  The image is of a major character from a culturally significant
    series of television shows. It is encyclopaedic.
6.  The image generally meets the image use policy.
7.  The image is used in at least one article, [William T.
    Riker](../Page/威廉·赖克.md "wikilink").
8.  The image contributes strongly to the article. As a fictional
    character, the subject is more easily visualised.
9.  The image is used only in the article namespace.
10. The image is from <http://memory-alpha.org/en/wiki/William_Riker>
    and the copyright is owned by [Paramount
    Pictures](../Page/Paramount_Pictures.md "wikilink").

### 图片来源

<http://memory-alpha.org/en/wiki/Image:Riker2366.jpg>

## 许可协议