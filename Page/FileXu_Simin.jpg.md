## 附加資料

### Fair Use

The image here is claimed to be **fair use** as it is:

1.  A portrait of one of Hong Kong's most famous politicians;
2.  There are no copyleft alternatives of such a photo;
3.  The image used is of a ceremonial and not a commerical value;
4.  The article is greatly improved by the prominence of this photo;
5.  Which is hosted and displayed on the not-for-profit Wikipedia
    Encylopedia for educational purposes.
6.  The person in the photo died in 2007.

<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p>晚年的徐四民</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p><a href="http://cppcc.people.com.cn/BIG5/35377/5791863.html">人民網</a></p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td><p>2007年</p></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td><p>中新社發，洪少葵攝</p></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p>{{#switch: 檔案其他版本（可留空）</p></td>
</tr>
</tbody>
</table>