**Final Cut
Pro**是一款由[Macromedia](../Page/Macromedia.md "wikilink")（现已被[Adobe收购](../Page/Adobe.md "wikilink")）推出，之後由[蘋果公司接手研發](../Page/蘋果公司.md "wikilink")、銷售的[非線性影片剪輯](../Page/非線性剪輯.md "wikilink")[軟體](../Page/軟體.md "wikilink")。最新版本為Final
Cut Pro X，能在裝載英特爾處理器和[OS X](../Page/OS_X.md "wikilink")
10.6.7以後版本的麥金塔電腦上運行。本軟體能讓使用者將影片記錄和傳輸至硬碟上（內建或外接），之後進行編輯、剪輯、處理和輸出成多種格式。

## 历史

自2000年代初期起，Final Cut
Pro的使用者開始大量增加，尤其是業餘的影片剪輯者和獨立[電影製作者](../Page/電影製作者.md "wikilink")。Final
Cut Pro同時也開始與[Avid
Technology的](../Page/Avid_Technology.md "wikilink")[Media
Composer軟體在電影和電視剪輯業界中競爭](../Page/Media_Composer.md "wikilink")。根據2007年SCRI的研究，Final
Cut
Pro在美國專業剪輯市場中的佔有率為49%，而Avid為22%。\[1\]2008年由美國電影剪輯公會發表的調查報告指出，其會內成員使用Final
Cut Pro的比例為21%。\[2\]

## 資料來源

## 外部連結

  - [Final Cut Pro X](https://www.apple.com/final-cut-pro/)
      - [Final Cut Pro X](https://www.apple.com/cn/final-cut-pro/)
      - [Final Cut Pro X](https://www.apple.com/hk/final-cut-pro/)
      - [Final Cut Pro X](https://www.apple.com/tw/final-cut-pro/)

[Category:蘋果公司軟體](../Category/蘋果公司軟體.md "wikilink")
[Category:苹果公司收购](../Category/苹果公司收购.md "wikilink")
[Category:MacOS軟體](../Category/MacOS軟體.md "wikilink")
[Category:视频编辑软件](../Category/视频编辑软件.md "wikilink")

1.
2.