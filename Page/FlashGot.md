**FlashGot** 是为 [Mozilla Firefox](../Page/Mozilla_Firefox.md "wikilink")
和基于[Mozilla Application
Suite的](../Page/Mozilla_Application_Suite.md "wikilink")[网页](../Page/网页.md "wikilink")[浏览器所开发的](../Page/浏览器.md "wikilink")[扩展](../Page/Firefox扩展列表.md "wikilink")
("Extensions"，现称为
"Add-on's")，是基于[GPL发布的](../Page/GPL.md "wikilink")[自由软件](../Page/自由软件.md "wikilink")
(Freeware)，拥有四十多种各国语言版本。此附加组件（扩展）的作者 Giorgio Maone 亦开发了另一款热门的 Firefox
附加组件 : **[NoScript](../Page/NoScript.md "wikilink")**。

**FlashGot** 允许 Firefox
等[浏览器使用大多数流行的外部下载管理器处理单一的和全部](../Page/浏览器.md "wikilink")("全部"
和 "选择")下载，支持 [Windows](../Page/Windows.md "wikilink"), [Mac OS
X](../Page/Mac_OS_X.md "wikilink"), [Linux](../Page/Linux.md "wikilink")
和 [FreeBSD](../Page/FreeBSD.md "wikilink") 系统。安装它之后，就可以在 [Mozilla
Firefox](../Page/Mozilla_Firefox.md "wikilink") 中像 [Internet
Explorer](../Page/Internet_Explorer.md "wikilink")
一样在下载[链接](../Page/链接.md "wikilink")([URL](../Page/URL.md "wikilink"))上使用右键菜单调用下载软件进行下载。

需要注意的是，由于名称上的相近，FlashGot 和 [FlashGet](../Page/FlashGet.md "wikilink")
很容易使用户发生混淆，实际上 [FlashGet](../Page/FlashGet.md "wikilink")
是一个下载工具，而 FlashGot
则是在浏览器和下载工具传递URL参数的“中间程序”，其本身并不具备下载能力。

## 支持的外部下载管理器

截至目前版本（1.1.8.4），FalshGot 支持的多平台外部下载管理器（下载软件）如下:

（如果有某些下载软件不在下列范围内的，亦可要求 FlashGot 的开发者在后续版本中将其加入支持列表。）

1\.[Windows系统](../Page/Windows.md "wikilink"):

[BitComet](../Page/BitComet.md "wikilink")
（[比特彗星](../Page/比特彗星.md "wikilink")）/ [Download
Accelerator Plus](../Page/Download_Accelerator_Plus.md "wikilink") /
[DownloadStudio](../Page/DownloadStudio.md "wikilink") /
[FlashGet](../Page/FlashGet.md "wikilink")
（[快车](../Page/快车.md "wikilink")） / [Free Download
Manager](../Page/Free_Download_Manager.md "wikilink") / [Fresh
Download](../Page/Fresh_Download.md "wikilink") /
[GetRight](../Page/GetRight.md "wikilink") /
[GigaGet](../Page/GigaGet.md "wikilink")（[迅雷的英文版](../Page/迅雷.md "wikilink")）
/ [HiDownload](../Page/HiDownload.md "wikilink") /
[iGetter](../Page/iGetter.md "wikilink") /
[InstantGet](../Page/InstantGet.md "wikilink") / [Internet Download
Accelerator](../Page/Internet_Download_Accelerator.md "wikilink") /
[Internet Download
Manager](../Page/Internet_Download_Manager.md "wikilink") /
[LeechGet](../Page/LeechGet.md "wikilink") / [Mass
Downloader](../Page/Mass_Downloader.md "wikilink") / [Net
Transport](../Page/Net_Transport.md "wikilink")
（[影音传送带](../Page/影音传送带.md "wikilink")） /
[NetXfer](../Page/NetXfer.md "wikilink") (Net Transport 2) /
[NetAnts](../Page/NetAnts.md "wikilink")
（[网络蚂蚁](../Page/网络蚂蚁.md "wikilink")） /
[Orbit](../Page/Orbit.md "wikilink") /
[ReGet](../Page/ReGet.md "wikilink") /
[Retriever](../Page/Retriever.md "wikilink") / [Star
Downloader](../Page/Star_Downloader.md "wikilink") /
[Thunder](../Page/Thunder.md "wikilink")
（[迅雷](../Page/迅雷.md "wikilink")） /
[TrueDownloader](../Page/TrueDownloader.md "wikilink") / [Ukrainian
Download Master](../Page/Ukrainian_Download_Master.md "wikilink") /
[WellGet](../Page/WellGet.md "wikilink") /
[wxDFast](../Page/wxDFast.md "wikilink")；

2\.[Linux](../Page/Linux.md "wikilink") /
[FreeBSD](../Page/FreeBSD.md "wikilink") / 其他
[Unix](../Page/Unix.md "wikilink") [内核系统](../Page/内核.md "wikilink"):

[Aria](../Page/Aria.md "wikilink") / [cURL](../Page/cURL.md "wikilink")
/ [Downloader 4 X](../Page/Downloader_4_X.md "wikilink") /
[GNOME](../Page/GNOME.md "wikilink")
[Gwget](../Page/Gwget.md "wikilink") /[KDE](../Page/KDE.md "wikilink")
[KGet](../Page/KGet.md "wikilink") /
[wxDownloadFast](../Page/wxDownloadFast.md "wikilink")，可通过[Wine支持很多](../Page/Wine.md "wikilink")[Windows平台的下载软件](../Page/Windows.md "wikilink")。

3\.[Mac OS X](../Page/Mac_OS_X.md "wikilink"):

[iGetter](../Page/iGetter.md "wikilink") /
[Leech](../Page/Leech.md "wikilink") / [Speed
Download](../Page/Speed_Download.md "wikilink") /
[wxDownloadFast](../Page/wxDownloadFast.md "wikilink").

## 支持的产品

FlashGot 适用于以下各版本产品（范围将随下列产品的升级而变更）

[Firefox](../Page/Firefox.md "wikilink") 1.5 - 3.2 alpha1pre

[Flock](../Page/Flock.md "wikilink") 0.4 - 2.0.\*

[Mozilla](../Page/Mozilla.md "wikilink") 1.7 - 1.8+

[SeaMonkey](../Page/SeaMonkey.md "wikilink") 1.0 - 2.0 beta1

[Songbird](../Page/Songbird.md "wikilink") 0.7 - 1.2.0

[Thunderbird](../Page/Thunderbird.md "wikilink") 1.5 - 3.0 beta2pre

## 外部链接

  - [FlashGot 官方网站](http://flashgot.net)

[Category:Firefox 附加组件](../Category/Firefox_附加组件.md "wikilink")