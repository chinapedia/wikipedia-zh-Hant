**Flock**是一個已停止開發的[網頁瀏覽器](../Page/網頁瀏覽器.md "wikilink")，其特點為整合多種[社群網路服務服務](../Page/社群網路服務.md "wikilink")\[1\]。Flock的早期版本使用[Mozilla的](../Page/Mozilla.md "wikilink")[Gecko](../Page/Gecko.md "wikilink")[排版引擎](../Page/排版引擎.md "wikilink")。2.6.2版本發佈於2011年1月27日，是基於[Mozilla
Firefox開發的最後一個版本](../Page/Mozilla_Firefox.md "wikilink")\[2\]\[3\]。從第3版開始，Flock基於[Chromium開發並使用](../Page/Chromium.md "wikilink")[WebKit排版引擎](../Page/WebKit.md "wikilink")\[4\]\[5\]。

2011年1月，Flock公司被[Zynga收購](../Page/Zynga.md "wikilink")\[6\]。2011年4月26日，瀏覽器宣布停止開發和停止支援\[7\]。

## 特色

Flock
2.5整合了社交網絡和媒體服務，包括[MySpace](../Page/MySpace.md "wikilink")\[8\]、[Facebook](../Page/Facebook.md "wikilink")、[YouTube](../Page/YouTube.md "wikilink")、[Twitter](../Page/Twitter.md "wikilink")、[Flickr](../Page/Flickr.md "wikilink")、[Blogger](../Page/Blogger.md "wikilink")、[Gmail](../Page/Gmail.md "wikilink")、[Yahoo\!
Mail等](../Page/Yahoo!_Mail.md "wikilink")。當[登入到任一支援的社群服務時](../Page/登入.md "wikilink")，Flock可以追蹤朋友的個人資料、上傳照片等等\[9\]。

## 参考资料

## 外部連結

  -
## 參見

  - [網頁瀏覽器比較](../Page/網頁瀏覽器比較.md "wikilink")
  - [網頁瀏覽器列表](../Page/網頁瀏覽器列表.md "wikilink")
  - [推播技術](../Page/推播技術.md "wikilink")
  - [RockMelt](../Page/RockMelt.md "wikilink")

[Category:2005年軟體](../Category/2005年軟體.md "wikilink")
[Category:自由网页浏览器](../Category/自由网页浏览器.md "wikilink")
[Category:MacOS軟體](../Category/MacOS軟體.md "wikilink")
[Category:使用XUL的軟體](../Category/使用XUL的軟體.md "wikilink")
[Category:Gecko衍生軟體](../Category/Gecko衍生軟體.md "wikilink")
[Category:Webkit衍生軟體](../Category/Webkit衍生軟體.md "wikilink")
[Category:基於Mozilla
Firefox的網頁瀏覽器](../Category/基於Mozilla_Firefox的網頁瀏覽器.md "wikilink")

1.  [Flock Browser – Built on Mozilla's
    Firefox](http://www.flock.com/mozilla)
2.
3.
4.
5.
6.
7.
8.  [Official release announcement on Shawn Harding's
    blog](http://flock.com/node/64027)
9.