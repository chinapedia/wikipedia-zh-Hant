**Flower**（****）是[日本二人組合](../Page/日本.md "wikilink")[近畿小子的第](../Page/近畿小子.md "wikilink")7張[單曲](../Page/單曲.md "wikilink")。於1999年5月26日由[傑尼斯娛樂唱片公司發行](../Page/傑尼斯娛樂.md "wikilink")。

## 解說

KinKi
Kids開始從令人驚異的銷量沉靜下來，這張單曲的[Oricon統計首批銷量只有](../Page/Oricon.md "wikilink")36.8萬張，僅比上一張單曲稍作輕微上升。不過由於長期受歡迎，所以累積銷量最終仍達104.1萬張。這是相隔3張單曲後，合計第4張唱片達至[百萬銷量](../Page/百萬銷量.md "wikilink")。不過同時亦是直至目前為止KinKi
Kids最後一張擁有百萬銷量的單曲。

歌曲配樂方面，A面歌曲『Flower』的間奏加插了[沖繩](../Page/沖繩.md "wikilink")・[奄美地區的樂器](../Page/奄美諸島.md "wikilink")[三味線作演奏](../Page/三味線.md "wikilink")。而該首歌曲亦曾為了[電台節目而特別製作了一個播放版本](../Page/電台節目.md "wikilink")，但截至現時為止該首特別版本仍沒有發行的打算。作曲作詞方面，找到了同時期為[濱崎步歌曲作曲](../Page/濱崎步.md "wikilink")，例必使歌曲大受歡迎的[HΛL負責](../Page/HΛL.md "wikilink")。整首歌訊息正面，帶給人們夏日清爽氣息的聲音。在[朝日電視台的](../Page/朝日電視台.md "wikilink")[音樂節目](../Page/音樂節目.md "wikilink")『[Music
Station](../Page/Music_Station.md "wikilink")』演出時，[堂本光一經常把](../Page/堂本光一.md "wikilink")[副歌部份的歌詞唱錯](../Page/副歌.md "wikilink")，結果常被該節目的回顧中重提。另外，該歌亦曾在[日本電視台的](../Page/日本電視台.md "wikilink")『[伊東家的食桌](../Page/伊東家的食桌.md "wikilink")』中，以只使用[鋼琴的](../Page/鋼琴.md "wikilink")[黑鍵就能彈奏的方式被介紹](../Page/黑鍵.md "wikilink")，結果曾在[中](../Page/中學生.md "wikilink")[小學生牽起了一股只要找到鋼琴就會奏出](../Page/小學生.md "wikilink")「Flower」的熱潮。但其實雖說只以黑鍵就能彈奏，但其實實際上的[音階會比原曲高半個音](../Page/音階.md "wikilink")。

這首歌曲亦與第3張單曲『[雲霄飛車羅曼史](../Page/Jetcoaster_Romance.md "wikilink")』一樣被選為作[全日空](../Page/全日本空輸.md "wikilink")[航空公司的](../Page/航空公司.md "wikilink")[廣告](../Page/廣告.md "wikilink")「'99
Paradise
沖繩」的[主題曲](../Page/主題曲.md "wikilink")，廣告片連同[音樂錄影帶均在沖繩進行拍攝](../Page/音樂錄影帶.md "wikilink")。

## 名稱

  - 日語原名：****
  - 中文意思：**花朵**
  - [香港](../Page/香港.md "wikilink")[正東譯名](../Page/正東唱片.md "wikilink")：**Flower**
  - [台灣](../Page/台灣.md "wikilink")[豐華譯名](../Page/豐華唱片.md "wikilink")：**Flower**
  - [台灣](../Page/台灣.md "wikilink")[艾迴譯名](../Page/艾迴唱片.md "wikilink")：**Flower**

## 收錄歌曲

1.  **Flower**（****）
      - ※KinKi
        Kids參與演出的日本[全日空](../Page/全日本空輸.md "wikilink")[航空公司廣告](../Page/航空公司.md "wikilink")『'99
        Paradise [沖繩](../Page/沖繩.md "wikilink")』主題曲。
      - 作曲：[HΛL](../Page/HΛL.md "wikilink")、[音妃](../Page/音妃.md "wikilink")
      - 作詞：[HΛL](../Page/HΛL.md "wikilink")
      - 編曲：[船山基紀](../Page/船山基紀.md "wikilink")
      - 和音編排：[松下誠](../Page/松下誠.md "wikilink")
2.  **筋疲力盡**（****）
      - 作曲：[馬飼野康二](../Page/馬飼野康二.md "wikilink")
      - 作詞：[戶澤暢美](../Page/戶澤暢美.md "wikilink")
      - 編曲：[CHOKKAKU](../Page/CHOKKAKU.md "wikilink")
3.  **Flower (Instrumental)**
      - 作曲：HΛL、音妃
      - 編曲：船山基紀
      - 和音編排：松下誠
4.  **筋疲力盡 (Instrumental)**
      - 作曲：馬飼野康二
      - 編曲：CHOKKAKU

[Category:近畿小子歌曲](../Category/近畿小子歌曲.md "wikilink")
[Category:1999年單曲](../Category/1999年單曲.md "wikilink")
[Category:RIAJ百萬認證單曲](../Category/RIAJ百萬認證單曲.md "wikilink")
[Category:Oricon百萬銷量達成單曲](../Category/Oricon百萬銷量達成單曲.md "wikilink")
[Category:1999年Oricon單曲月榜冠軍作品](../Category/1999年Oricon單曲月榜冠軍作品.md "wikilink")
[Category:1999年Oricon單曲週榜冠軍作品](../Category/1999年Oricon單曲週榜冠軍作品.md "wikilink")