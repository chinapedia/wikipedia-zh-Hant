**foobar**是電腦程式領域裏的术语，並無實際用途和參考意義。在[计算机程序设计与计算机技术的相关文档中](../Page/计算机程序设计.md "wikilink")，术语**foobar**是一个常见的[无名氏化名](../Page/无名氏.md "wikilink")，常被作为“”使用。

从技术上讲，“foobar”很可能在1960年代至1970年代初通过[迪吉多的系统手册传播开来](../Page/迪吉多.md "wikilink")。另一种说法是，“foobar”可能来源于电子学中反转的foo信号（\(\bar{q}\)）；这是因为如果一个数字信号是低电平有效（即负压或零电压代表“1”），那么在信号标记上方一般会标有一根水平横线，而横线的英文即为“bar”。在《》中，还提到“foo”可能早于“[FUBAR](../Page/FUBAR.md "wikilink")”出现。\[1\]

## 例子

单词“foobar”或分离的“foo”与“bar”常出现于程序设计的案例中，如同[Hello
World程序一样](../Page/Hello_World.md "wikilink")，它们常被用于向学习者介绍某种程序语言。

“foo”“bar”常被作为函数／方法的名称或变量名。

下面是一段通过“foo”和“bar”来演示如何在[Java](../Page/Java.md "wikilink")、[C及](../Page/C語言.md "wikilink")[C++中连接两个字符串的实例程序](../Page/C++.md "wikilink")：

``` Java
//Java Code
String foo = "Hello, ";
String bar = "World";
System.out.println(foo + bar);
//Hello, World is printed to the screen by printing foo and bar
```



``` c
// C code

#include <stdio.h>

int main()
{
   char foo[] = "Hello,";
   char bar[] = "World!";
   printf("%s %s\n", foo, bar);

   return 0;
}
```

``` cpp
// C++ code

#include <iostream>
#include <string>
using namespace std;

int main()
{
   string foo = "Hello,";
   string bar = "World!";
   cout << foo << " " << bar << endl;

   return 0;
}
```

``` python
# Python code

foo = "Hello,"
bar = "World!"
print(foo + bar)
```

## 参见

  - [无名氏](../Page/无名氏.md "wikilink")
  - [FUBAR](../Page/FUBAR.md "wikilink")
  - [foobar2000](../Page/foobar2000.md "wikilink")

## 参考文献

<references/>

## 外部链接

  - [The Jargon File entry on
    foobar](http://www.catb.org/~esr/jargon/html/F/foobar.html)
  - RFC 3092 - Origin and usage of *foobar*
  - [Foobar2000 is an advanced audio player for the Windows
    platform](https://web.archive.org/web/20160324/http://www.maximumpc.com/how-to-manage-your-music-the-power-user-way-with-foobar/)
  - RFC 1639 - FTP Operation Over Big Address Records (FOOBAR)

[Category:化名](../Category/化名.md "wikilink")
[Category:变量](../Category/变量.md "wikilink")

1.  [《新黑客辞典》](http://www.catb.org/jargon/html/F/foobar.html)（The Jargon
    File）