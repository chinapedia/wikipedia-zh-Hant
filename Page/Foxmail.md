**Foxmail**是最早由[華中科技大學](../Page/華中科技大學.md "wikilink")[張小龍開發的一款](../Page/張小龍.md "wikilink")[電子郵件客戶端](../Page/電子郵件客戶端.md "wikilink")，具有[電子郵件管理功能](../Page/電子郵件.md "wikilink")\[1\]和邮件服务器功能\[2\]。同时它还有[RSS订阅](../Page/RSS.md "wikilink")、[日历](../Page/日历.md "wikilink")、[联系人等功能](../Page/通讯录.md "wikilink")。其名字取自[金庸武侠小说](../Page/金庸.md "wikilink")《[笑傲江湖](../Page/笑傲江湖.md "wikilink")》中的人物[令狐冲](../Page/令狐冲.md "wikilink")\[3\]。

## 歷史

  - 1997年1月，Foxmail 1.0 beta（英文版）誕生。
  - 1998年4月，Foxmail 2.1中文版推出。
  - 2000年4月18日，[張小龍以](../Page/張小龍.md "wikilink")1200萬元的價格将Foxmail賣給[博大公司](../Page/博大公司.md "wikilink")。
  - 2001年1月8日，Foxmail 3.11版推出。
  - 2002年7月4日，Foxmail 4.2版推出。
  - 2003年9月1日，Foxmail 5.0版發佈。
  - 2004年6月18日，Foxmail 5.0.800.0版發佈。
  - 2005年3月16日，[騰訊收購Foxmail軟件](../Page/騰訊.md "wikilink")。
  - 2005年11月21日，Foxmail 6.0beta1版發佈。
  - 2011年9月22日，Foxmail 7.0.1.82版发布。
  - 2013年5月27日，Foxmail Mac 1.1版发布。
  - 2013年7月1日，Foxmail 7.1版发布，该版本开始无法发送纯文本邮件，只能发送HTML邮件。
  - 2013年8月28日，Foxmail 7.1.3版发布。
  - 2013年9月26日，Foxmail Mac 1.2版发布
  - 2014年1月23日，Foxmail 7.2版发布。
  - 2014年12月1日，Foxmail 7.2.6版发布。
  - 2015年6月3日，Foxmail 7.2.7版发布。
  - 2017年1月11日，Foxmail 7.2.8版发布。
  - 2017年11月3日，Foxmail 7.2.9版发布，重新增加了纯文本邮件。

## 相关条目

  - [Microsoft Outlook](../Page/Microsoft_Outlook.md "wikilink")
  - [Dreammail](../Page/Dreammail.md "wikilink")

## 外部連結

  -
## 参考文献

{{-}}

[Category:電子郵件客戶端](../Category/電子郵件客戶端.md "wikilink")
[Category:腾讯软件](../Category/腾讯软件.md "wikilink")

1.  《常用工具软件》第二版,吴文虎、李秋弟、赵红梅著, 全国信息技术及应用远程培训教育工作组2005年出版 第180页
2.  《Foxmail 5.0实用教程》，植国贤著，清华大学出版社2004年出版
3.