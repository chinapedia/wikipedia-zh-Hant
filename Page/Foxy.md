**Foxy**是一個強制上傳分享的[正體中文](../Page/正體中文.md "wikilink")[P2P](../Page/對等網路.md "wikilink")[軟體](../Page/軟體.md "wikilink")，Foxy網站由位思科技負責管理（位思科技曾在Foxy網站聲明由位思科技負責，與景昌資訊科技公司無合作關係）。目前Foxy的網站呈現封鎖狀態，網站連接都會呈現「找不到網頁」的訊息。因Foxy的發行公司是[臺灣](../Page/臺灣.md "wikilink")[公司](../Page/公司.md "wikilink")，故Foxy只有正體中文版，沒有[英文等其它語言的版本](../Page/英文.md "wikilink")，因此主要流行在臺灣、[香港及](../Page/香港.md "wikilink")[澳門](../Page/澳門.md "wikilink")。

2011年10月22日，Foxy宣佈正式終止服務。用戶若開啟Foxy，程式會自行移除，移除後會自動安裝蜂巢安全管家。（此情況僅限於最新版本，舊版仍可使用上傳及下載功能）

## 事件

[中華民國](../Page/中華民國.md "wikilink")[內政部警政署](../Page/內政部警政署.md "wikilink")[刑事警察局科技犯罪防制中心與](../Page/刑事警察局.md "wikilink")[新波科技自](../Page/新波科技.md "wikilink")2006年起，長期深入研究P2P軟體之運作模式，發現Foxy能搜尋到部分[政府機關之文件及表格檔案](../Page/政府.md "wikilink")、[公司行號之內部文件](../Page/公司.md "wikilink")（會議紀錄、採購紀錄等）及個人私密資料（帳號、密碼等）。刑事警察局科技犯罪防制中心主任[李相臣批評](../Page/李相臣.md "wikilink")，Foxy在分享資料夾的設定上容易讓使用者誤設為「全機分享」，「且該軟體本身容易被植入[木馬](../Page/特洛伊木馬_\(電腦\).md "wikilink")\[1\]，所以資料外洩情況最嚴重。」刑事警察局曾經聯絡景昌資訊科技，要求該公司加強Foxy本身的安全性，以及協助控制機密資料外流；景昌資訊科技回應，該公司僅負責代理Foxy台灣區[廣告業務](../Page/廣告.md "wikilink")，Foxy產品研發與技術是由位在[美國的另一家公司負責](../Page/美國.md "wikilink")。

  - 2007年4月，Foxy遭台灣使用者發現可以搜尋到台灣各地[警察局製作的](../Page/警察局.md "wikilink")[筆錄](../Page/筆錄.md "wikilink")；而且只要在Foxy的搜尋欄位中鍵入「帳號」、「密碼」，就可以找到其他Foxy使用者儲存在各自[電腦內的各種帳號與密碼](../Page/電腦.md "wikilink")，包括[線上遊戲](../Page/線上遊戲.md "wikilink")、[即時通訊軟體](../Page/即時通訊軟體.md "wikilink")、[ADSL與](../Page/ADSL.md "wikilink")[網路銀行的帳號與密碼](../Page/網路銀行.md "wikilink")。對此，Foxy官方網站論壇的回應是：

<!-- end list -->

  - 2008年1月至2月，[陳冠希裸照事件時](../Page/陳冠希裸照事件.md "wikilink")，大量網友以Foxy作為傳播裸照的工具。刑事警察局偵九隊表示，[Kuro](../Page/Kuro.md "wikilink")、[ezPeer與](../Page/ezPeer.md "wikilink")[BitTorrent等P](../Page/BitTorrent.md "wikilink")2P軟體必須指定欲分享的資料夾，其他使用者只能在指定的資料夾內搜尋檔案，但是Foxy的分享資料夾設定方式容易讓使用者誤設為「全機分享」。[組合國際電腦股份有限公司技術顧問](../Page/組合國際電腦股份有限公司.md "wikilink")[林宏嘉建議](../Page/林宏嘉.md "wikilink")：在Foxy的安全疑慮被排除之前，Foxy使用者應立刻徹底移除Foxy，並刪除[電腦中的相關資料夾](../Page/電腦.md "wikilink")。

<!-- end list -->

  - 2008年4月，[香港亦發生Foxy洩露政府機密事件](../Page/香港.md "wikilink")，涉及政府部門包括[民航處](../Page/香港民航處.md "wikilink")、[警務處及](../Page/香港警務處.md "wikilink")[入境處](../Page/香港入境事務處.md "wikilink")。\[2\]\[3\]

<!-- end list -->

  - 2008年9月，香港發生[吉野家員工懷疑強姦少女事件](../Page/香港吉野家強姦案.md "wikilink")，一段用手機拍攝的疑似香港[吉野家員工強姦少女的短片在網上流傳並透過Foxy散播](../Page/吉野家.md "wikilink")。最後警方根據有關短片，拘捕了三名青年協助調查。\[4\]當強姦短片首次在網上傳播的翌日，有關短片已在Foxy上快速傳播，來源多達約1200多個，並且出現雙重搜尋效應，可見Foxy的傳播速度比起手機、[BitTorrent及普通網絡都快得多](../Page/BitTorrent.md "wikilink")。

<!-- end list -->

  - 2009年4月15日，登記地址位於台北縣永和市環河東路的景昌資訊科技總部鐵捲門深鎖，原因是[臺灣板橋地方法院檢察署於該日認定Foxy涉及侵權](../Page/臺灣板橋地方法院檢察署.md "wikilink")，可讓人非法下載音樂、影片，侵權金額高達新台幣58.42億元，將設計推出該軟體的景昌資訊科技及其負責人李憲明依違反《著作權法》起訴。\[5\]

<!-- end list -->

  - 2010年3月20日，Foxy因為分享未授權的電影和音樂而被眾多的電影商和唱片公司提告，景昌資訊科技負責人李憲明違反《著作權法》「擅自公開傳輸罪」而被法官判處1年半的徒刑，景昌資訊科技也被判處罰金新台幣70萬元；而這也是《著作權法》增訂「擅自公開傳輸罪」以來的第一起有罪判決。<ref>

</ref>

  - 2011年3月，一名33歲彭姓男子在[新竹市](../Page/新竹市.md "wikilink")[東區中正路租間套房](../Page/東區_\(新竹市\).md "wikilink")，利用FOXY軟體下載持卡人的消費紀錄，取得信用卡卡號、[授權碼等個資](../Page/授權碼.md "wikilink")，冒名盜刷購買[3C產品後立即拍賣掉](../Page/3C_\(商品代稱\).md "wikilink")，作為生活費及購[毒品解癮](../Page/毒品.md "wikilink")。同年4月10日，[基隆市政府警察局第一分局趁彭男取貨物時將他逮捕](../Page/基隆市政府.md "wikilink")，彭男坦稱以此手法盜刷18筆、得款新台幣71951元，訊後被警方依[詐欺](../Page/詐欺.md "wikilink")、[偽造文書及妨害電腦使用罪嫌移送](../Page/偽造文書.md "wikilink")[臺灣新竹地方法院檢察署偵辦](../Page/臺灣新竹地方法院檢察署.md "wikilink")。<ref>

</ref>

## 限制

部分的[防毒軟件如](../Page/防毒軟件.md "wikilink")[AVG](../Page/AVG殺毒軟件.md "wikilink")、[Comodo](../Page/Comodo_AntiVirus.md "wikilink")、[諾頓等會把Foxy當作成木馬程式](../Page/諾頓防毒.md "wikilink")，而無法同時執行兩種程式。另外，如用戶端未關閉上傳之功能，會因上傳流量侵占網路速度，造成下載流量速降低。

## 網路通訊協定

經截取Foxy運行時產生的網路[封包後發現Foxy的封包與另一套](../Page/封包.md "wikilink")[P2P軟體](../Page/對等網路.md "wikilink")：[Gnutella所產生的封包非常類似](../Page/Gnutella.md "wikilink")，故也有Foxy是一套基於Gnutella的p2p軟體一說。

出自Gnutella封包[特徵碼的一段話](../Page/特徵碼.md "wikilink")："*The first part
matches UDP messages - All start with "GND", then have a flag byte which
is either \\x00, \\x01 or \\x02, then two sequence bytes that can be
anything, then a fragment number, which must start at
1.*"。Foxy實際運行的一段封包："47 4e 44 02 00 07 01…"，前三欄位47
4e
44為"GND"的[十六進位表示法](../Page/十六進位.md "wikilink")，第四欄位以後之特徵皆符合Gnutella之特徵碼規範。

如果[防火牆或](../Page/防火牆.md "wikilink")[路由器具有處理](../Page/路由器.md "wikilink")[OSI模型第七層](../Page/OSI模型.md "wikilink")[應用層資料之能力](../Page/應用層.md "wikilink")，可藉由Gnutella特徵碼來識別或阻擋Foxy封包。

### Foxy與電腦保安

Foxy目前的最新版本Foxy.1.9.10.TC.Setup.exe被懷疑夾帶木馬，可能會移除用戶的個人防火牆，修改註冊檔。事實上，在所有[P2P軟體中](../Page/P2P.md "wikilink")，Foxy主程式的安全性頗受關注和質疑。
電腦保安專家指無法安全執行Foxy，令用戶資料容易外洩。\[6\]如果使用者懂得使用[虛擬機器執行Foxy](../Page/虛擬機器.md "wikilink")，如[VirtualBox](../Page/VirtualBox.md "wikilink")、[Virtual
PC](../Page/Virtual_PC.md "wikilink")、[VMware等](../Page/VMware.md "wikilink")，則是安全的。\[7\]

## 參考來源

  - [{{〈}}小心使用P2P軟體，以免你的個人檔案也分享出去{{〉}}](http://www.cib.gov.tw/news/news02_2.aspx?no=289)，內政部警政署刑事警察局科技犯罪防制中心，2006年11月27日
  - {{〈}}Foxy案情擴大專家建議立刻移除{{〉}}，Taiwan.[CNET](../Page/CNET.md "wikilink").com新聞專區，2007年4月13日
  - {{〈}}惡意程式、內容冒充外洩筆錄鑽進Foxy用戶PC{{〉}}，Taiwan.CNET.com新聞專區，2007年4月18日
  - [{{〈}}用FOXY抓檔\!?三思而後行\!（更新）{{〉}}](https://web.archive.org/web/20070705090927/http://www.twbbs.net.tw/1663888.html)，[台灣論壇](../Page/台灣論壇.md "wikilink")（原作者：莫斯科指揮官），2007年4月9日
  - [{{〈}}請不要用Foxy
    \!{{〉}}](https://web.archive.org/web/20080214114529/http://blog.xdite.net/?p=310)，Blog.XDite.net（原作者：xdite），2007年4月8日
  - [\< What's up , Foxy ?
    \>](http://princexxxmusic.blogspot.com/2008/08/whats-up-foxy.html)初剖
    Foxy
  - [8招防Foxy惹禍](http://newnutopia.com/viewtopic.php?t=23041)
  - [{{〈}}侵權爭議　FOXY宣布關站{{〉}}](http://tw.nextmedia.com/rnews/article/SecID/105/art_id/84688/IssueID/20111023)，蘋果日报,2011年10月23日08:39

[Category:下載工具](../Category/下載工具.md "wikilink")
[Category:免費軟體](../Category/免費軟體.md "wikilink")
[Category:檔案分享程式](../Category/檔案分享程式.md "wikilink")
[Category:广告软件](../Category/广告软件.md "wikilink")
[Category:P2P](../Category/P2P.md "wikilink")

1.  此情況為Foxy遭有心人士串改發佈，而非原程式含木馬。
2.  [港入境個資外洩機密檔曝光{{〈}}大紀元時報{{〉}} 2008年5月10日](http://news.epochtimes.com.tw/8/5/10/84026.htm)
3.  [FOXY再一例香港入境處急刪電腦外洩資料{{〈}}資安之眼{{〉}}2008年5月10日](http://www.itis.tw/node/1757)
4.  [星島日報：輪姦片上網　警拘三人](http://hk.news.yahoo.com/article/080910/3/85r2.html)
    ，2008年9月11日
5.   自由時報：FOXY涉侵權逾58億負責人被訴，2009年4月16日
6.
7.