**FreeMind**是一款跨平台的、基于[GPL协议的](../Page/GPL.md "wikilink")[自由软件](../Page/自由软件.md "wikilink")，用[Java編寫](../Page/Java.md "wikilink")，是一個用來繪製[思维导图的软件](../Page/思维导图.md "wikilink")。其产生的[文件格式后缀为](../Page/文件格式.md "wikilink").mm
。可用来做笔记，脑图记录，[腦力激盪等](../Page/腦力激盪.md "wikilink")。

## 特点

FreeMind 包括了许多让人激动的特性，其中包括扩展性，快捷的一键展开和关闭节点，快速记录思维，多功能的定义格式和快捷键。
可设置节点文字和背景颜色,形状(叉状/泡状),自动布局,闪烁节点等. 节点可导出,
并支持多种格式的导出,如HTML,XHTML,OpenOffice Writer, PNG,
JPEG,Applet,Flash,SVG,PDF等. 支持多种语言如中文.

### 扩展性

由于 FreeMind 使用 Java 编写，支持使用 [Python](../Page/Python.md "wikilink")
编写的扩展插件。另外文件格式是[xml的](../Page/xml.md "wikilink")。

## 缺陷

  - 无法多个思维中心点展开（亦有人认为这样是优点可以让人们专心于眼前的事）
  - 部分中文[输入法无法在](../Page/输入法.md "wikilink") FreeMind 输入(只是部分,
    存在可以正常输入中文的输入法)
  - 启动及运行速度较慢(相对一般小型软件而言)

## 版本历史

最新稳定版为：FreeMind 1.0.1 （更新于2014年4月12日）

## 参考文献

<div class="http://freemind.sourceforge.net/wiki/index.php/Main_Page">

<references />

<http://freemind.sourceforge.net/wiki/index.php/Main_Page>

</div>

## 外部連結

  - [FreeMind主網頁](http://freemind.sourceforge.net/)
  - [FreeMind的共筆網](http://freemind.sourceforge.net/wiki/index.php/Main_Page)

[Category:思維導圖軟件](../Category/思維導圖軟件.md "wikilink")
[Category:SourceForge專案](../Category/SourceForge專案.md "wikilink")