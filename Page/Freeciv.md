《**Freeciv**》是一個[回合制策略遊戲](../Page/回合制策略遊戲.md "wikilink")。它的概念源自於[席德·梅爾在](../Page/席德·梅爾.md "wikilink")1991年為美國微文公司（Micro
Prose）所創造出來的一款電腦遊戲《[文明帝國](../Page/文明_\(遊戲\).md "wikilink")》。它是一個依據[GNU通用公共许可证之下開發的](../Page/GNU通用公共许可证.md "wikilink")[自由軟體](../Page/自由軟體.md "wikilink")。其原始碼、圖片、音效等，都是世界各地的開發者供獻而成。

## 遊戲簡述

玩家在这里扮演一个公元前4000年的部落首领，要带领他们的人民度过这几十个世纪。一段时间过去，新的技术发明可以让你建造新的城市建筑或部署新的单位。玩家们可以互相之间进行战争，或者建立复杂的外交关系。

当一种文明消灭了所有其它文明，或者有一个玩家达到了[太空殖民的目标](../Page/太空殖民.md "wikilink")，或者到了一个特定的期限，游戏就会结束。
如果到了最后期限仍然还有几个文明存在，得分最高的玩家为胜者。玩家的分数是按文明的大小，财富，以及文化与科技的进步来取得的。

## 相容性

*Freeciv*可設定性極高，可運行在下列模式：《[文明一代](../Page/文明_\(遊戲\).md "wikilink")》、《[文明二代](../Page/文明_\(遊戲\).md "wikilink")》、*Freeciv*、或是客製設定模式。圖片及音效也可替換。地形套件則有等軸式(斜視角)、2D平面式以及六角式。*Freeciv*使用[TCP/IP進行網路連線](../Page/TCP/IP.md "wikilink")。玩家必須連結到[伺服器](../Page/伺服器.md "wikilink")，通常是在遠端機器但也可設在本機。Freeciv玩法可以是單獨對抗電腦控制的對手，也可以在多用戶模式對抗人類玩家。單人模式實際上是單一玩家連線到本機伺服器的一特殊情況。Freeciv
2.0 可以自動將伺服器設定為單人模式。

遊戲中將會有一或多名玩家扮演管理者並設定遊戲規則。
典型的規則修改包括：

  - 遊戲開始所需要的玩家人數。
  - 科技發展的速度。
  - 是否加入電腦控制的玩家。
  - 野蠻民族（電腦控制）是否會侵略移民。
  - 城市興建的最小間距。
  - 地圖上大陸與島嶼分散的程度。

當遊戲以回合制進行時，一般真人玩家同時移動，而電腦玩家則輪流移動。在現在版本 2.0.0
之前，電腦無法跟真实玩者建立外交關係。電腦的外交行為是依照設定而來（非隨機）的。

*Freeciv*有另一地圖及劇本編輯器，叫做*Civworld*，可另外下載。 *Freeciv*可執行於含有[X
Window系統的各Unix版本及其他包括微軟Windows](../Page/X_Window系統.md "wikilink")，[Amiga及蘋果](../Page/Amiga.md "wikilink")[Mac
OS](../Page/Mac_OS.md "wikilink")。
*Freeciv*可认为是优缺点都鲜明：没有华丽的画面或视觉效果；另一方面，开发形式具有极好的移植性，对系统资源要求很低。
从最初的SGI IRIX基础上，现在Freeciv至少可在以下系统中运行：[SunOS
4](../Page/Sun_OS.md "wikilink")、[Solaris](../Page/Solaris.md "wikilink")、[Ultrix](../Page/Ultrix.md "wikilink")、[QNX](../Page/QNX.md "wikilink")、[Linux](../Page/Linux.md "wikilink")、[FreeBSD](../Page/FreeBSD.md "wikilink")、[OpenBSD](../Page/OpenBSD.md "wikilink")、[NetBSD](../Page/NetBSD.md "wikilink")、[Mac
OS
X](../Page/Mac_OS_X.md "wikilink")、[OS/2](../Page/OS/2.md "wikilink")、[Windows
95](../Page/Windows_95.md "wikilink")、[Windows
98](../Page/Windows_98.md "wikilink")、[Cygwin](../Page/Cygwin.md "wikilink")、[Windows
2000](../Page/Windows_2000.md "wikilink")、[Windows
XP](../Page/Windows_XP.md "wikilink")、[Amiga等](../Page/Amiga.md "wikilink")。

## 其他

  - [开源游戏列表](../Page/开源游戏列表.md "wikilink")
  - [SDL](../Page/SDL.md "wikilink")

## 外部連結

  - [Freeciv主页](http://www.freeciv.org/)
  - [Wenming.io
    文明](https://web.archive.org/web/20180812214257/http://www.wenming.io/)
  - [Freeciv 爱好者
    (english)](https://web.archive.org/web/20100504181445/http://www.freeciv.de.ms/)
  - [Freeciv贡献者名单](https://web.archive.org/web/20051120083849/http://www.freeciv.org/index.php/Special:People)
  - [Freeciv公共服务器列表](http://meta.freeciv.org/metaserver/freeciv.html)
  - [Freeciv
    邮件列表](https://web.archive.org/web/20050817193350/http://www.freeciv.org/index.php/Talk)
  - [Freeciv
    IRC频道](https://web.archive.org/web/20050817193350/http://www.freeciv.org/index.php/Talk)
  - [Freeciv.org论坛](http://forum.freeciv.org)
  - [Apolyton
    forum](https://web.archive.org/web/20050530111859/http://apolyton.net/forums/forumdisplay.php?forumid=111)
  - [Wenming.io](https://web.archive.org/web/20180812214257/http://www.wenming.io/)

[Category:回合制策略游戏](../Category/回合制策略游戏.md "wikilink")
[Category:Linux遊戲](../Category/Linux遊戲.md "wikilink")
[Category:MacOS遊戲](../Category/MacOS遊戲.md "wikilink")
[Category:Windows遊戲](../Category/Windows遊戲.md "wikilink")
[Category:开源電子游戏](../Category/开源電子游戏.md "wikilink")
[Category:自由软件](../Category/自由软件.md "wikilink")
[Category:免费游戏](../Category/免费游戏.md "wikilink")
[Category:1996年电子游戏](../Category/1996年电子游戏.md "wikilink")
[Category:4X電子遊戲](../Category/4X電子遊戲.md "wikilink")
[Category:Amiga遊戲](../Category/Amiga遊戲.md "wikilink")
[Category:AmigaOS 4遊戲](../Category/AmigaOS_4遊戲.md "wikilink")
[Category:網頁遊戲](../Category/網頁遊戲.md "wikilink")
[Category:合作模式遊戲](../Category/合作模式遊戲.md "wikilink")
[Category:跨平台軟體](../Category/跨平台軟體.md "wikilink")
[Category:Android软件](../Category/Android软件.md "wikilink")
[Category:自由SDL軟件](../Category/自由SDL軟件.md "wikilink")
[Category:Lua語言遊戲](../Category/Lua語言遊戲.md "wikilink")
[Category:Maemo遊戲](../Category/Maemo遊戲.md "wikilink")
[Category:MorphOS遊戲](../Category/MorphOS遊戲.md "wikilink")
[Category:熱座模式遊戲](../Category/熱座模式遊戲.md "wikilink")
[Category:開源策略遊戲](../Category/開源策略遊戲.md "wikilink")
[Category:跨時代遊戲](../Category/跨時代遊戲.md "wikilink")
[Category:等距視角遊戲](../Category/等距視角遊戲.md "wikilink")