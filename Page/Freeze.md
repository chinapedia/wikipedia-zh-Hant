**Freeze**為一隊前[香港](../Page/香港.md "wikilink")[女子演唱團體](../Page/女子演唱團體.md "wikilink")，成員包括[甄詠珊](../Page/甄詠珊.md "wikilink")、[陳樂榣及](../Page/陳樂榣.md "wikilink")[石詠莉](../Page/石詠莉.md "wikilink")，於2007年5月6日在活動上宣佈組合三名成員正式加入樂壇。Freeze以金色為隊色，代表著活力、光榮、繁榮以及自信。從出道至解散，Freeze一直賣弄性感形象，更由於她們是整容公司代言人，更獲外界封為「整容三嬌」\[1\]
。解散後，成員石詠莉自稱因要顧及隊友形象才不能公開否認整容。\[2\]

## 簡介

三人原為雜誌[模特兒](../Page/模特兒.md "wikilink")，她們同時亦為[變靚D矯形和](../Page/變靚D.md "wikilink")[整容手術代言人](../Page/整容.md "wikilink")。三人於2006年至2007年不約而同簽約[一元製作室有限公司](../Page/一元製作室有限公司.md "wikilink")。

2007年年底，Freeze推出首張[迷你專輯](../Page/迷你專輯.md "wikilink")《[Golden
eyes](../Page/Golden_eyes.md "wikilink")》，三天內便完全售罄，及後加印至第二版。

2008年7月，一元製作室公佈Freeze會於同年12月13日假[匯星舉行首個](../Page/匯星.md "wikilink")[演唱會](../Page/演唱會.md "wikilink")，後來宣佈由於反應熱烈，公司打算加開一場，但表演場地不許可，因此順延。此外，Freeze更曾到[澳門](../Page/澳門.md "wikilink")、[美國及](../Page/美國.md "wikilink")[加拿大登台演出](../Page/加拿大.md "wikilink")。

2009年以團體身份參與TVB綜藝節目[美女廚房](../Page/美女廚房.md "wikilink")，與[Hotcha和四葉女同場](../Page/Hotcha.md "wikilink")，成員石詠莉疑因不甘主持[方力申和](../Page/方力申.md "wikilink")[梁漢文試食其作品後不斷批評食物過鹹而反擊](../Page/梁漢文.md "wikilink")「似你囉」，對方面怒難色後在試食炸奶的環節反擊「假架D奶！」事件獲傳媒大幅度報導，更為網民熱論\[3\]。

2010年10月，由於甄穎珊與一元製作室解約，陳樂榣被傳媒訪問時透露，Freeze已名存實亡。甄穎珊後加入[電視廣播有限公司成為演員](../Page/電視廣播有限公司.md "wikilink")，石詠莉則作單獨發展，仍然在香港音樂界發展。

## 歌唱及影視作品

### 唱片

## 個人音樂專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行商</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1st</p></td>
<td style="text-align: left;"><p><a href="../Page/Golden_eyes.md" title="wikilink">Golden Eyes</a></p></td>
<td style="text-align: left;"><p>EP</p></td>
<td style="text-align: left;"><p><a href="../Page/一元製作室.md" title="wikilink">一元製作室</a></p></td>
<td style="text-align: left;"><p>2007年11月29日</p></td>
<td style="text-align: left;"><p><strong>CD</strong></p>
<ol>
<li>金睛火眼</li>
<li>情場大後備</li>
<li>踩鋼綫</li>
<li>情難控（<a href="../Page/石詠莉.md" title="wikilink">石詠莉獨唱</a>）</li>
</ol>
<p><strong>DVD</strong></p>
<ol>
<li>情場大後備</li>
<li>踩鋼綫</li>
<li>金睛火眼</li>
<li>情難控</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2nd</p></td>
<td style="text-align: left;"><p><a href="../Page/Elements_(EP).md" title="wikilink">Elements</a></p></td>
<td style="text-align: left;"><p>EP</p></td>
<td style="text-align: left;"><p><a href="../Page/一元製作室.md" title="wikilink">一元製作室</a></p></td>
<td style="text-align: left;"><p>2008年11月21日</p></td>
<td style="text-align: left;"><p><strong>CD</strong></p>
<ol>
<li>Shiba-Za</li>
<li>黑色暴雨</li>
</ol>
<p><strong>DVD</strong></p>
<ol>
<li>Shiba-Za</li>
<li>黑色暴雨</li>
</ol></td>
</tr>
</tbody>
</table>

### 單曲

  - 2007年：Maria（收錄於電影《[醜女大翻身](../Page/醜女大翻身.md "wikilink")》原聲大碟）

### 合演／客串電影

  - 星皓電影《[女人．本色](../Page/女人．本色.md "wikilink") 》（Carisa）
  - 中國星電影《[奪帥](../Page/奪帥.md "wikilink") Fatal Move》 飾 麗莎（Carisa）
  - 星皓電影《[六樓后座2家屬謝禮](../Page/六樓后座2家屬謝禮.md "wikilink") Happy Funeral》飾
    MV少女
  - 《[一半海水、一半火焰](../Page/一半海水、一半火焰.md "wikilink")》\*海外參展版本（Sukie）

### 配音電影

  - 《[醜女大翻身](../Page/醜女大翻身.md "wikilink") 200 Pounds Beauty》（Carisa）

### 電視

  - [香港電台](../Page/香港電台.md "wikilink")：《[非常平等任務](../Page/非常平等任務.md "wikilink")》（Carisa）
  - [香港電台](../Page/香港電台.md "wikilink")：《[父子](../Page/父子.md "wikilink")》（Carisa）
  - [無綫電視](../Page/無綫電視.md "wikilink")：《[盛裝舞步愛作戰](../Page/盛裝舞步愛作戰.md "wikilink")》飾
    幻想女神（客串）

## 派台歌曲成績

| **派台歌曲成績**  |
| ----------- |
| 唱片          |
| **2007**    |
| Golden Eyes |
| Golden Eyes |
| **2008**    |
| Golden Eyes |
| Elements    |
| Elements    |
| **2009**    |
|             |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **0**       |

## 曾參與之電視及網上節目

  - [無綫電視](../Page/無綫電視.md "wikilink")
      - 2007年：《[翡翠歌星賀台慶](../Page/翡翠歌星賀台慶.md "wikilink")》
      - 2007年：《[味分高下](../Page/味分高下.md "wikilink")》(Sukie)
      - 2007年：《[歡樂滿東華](../Page/歡樂滿東華.md "wikilink")》
      - 2008年：《[360℃音樂無邊](../Page/360℃音樂無邊.md "wikilink")》
      - 2008年：《[鐵甲無敵獎門人](../Page/鐵甲無敵獎門人.md "wikilink")》
      - 2008年：《[娛樂金剛圈](../Page/娛樂金剛圈.md "wikilink")》
      - 2008年：《[新聞透視](../Page/新聞透視.md "wikilink")》 (Alley)
      - 2008年：《[翡翠歌星賀台慶](../Page/翡翠歌星賀台慶.md "wikilink")》
      - 2009年：《[愛情研究院》](../Page/愛情研究院》.md "wikilink")》\[J2\]
      - 2009年：《[耳分高下](../Page/耳分高下.md "wikilink")》
      - 2009年：《[美女廚房](../Page/美女廚房.md "wikilink")》
      - 2009年：《[香港先生選舉2009](../Page/香港先生選舉2009.md "wikilink")》
      - 2010年：《[超級遊戲獎門人](../Page/超級遊戲獎門人.md "wikilink")》
  - [無綫收費電視](../Page/無綫收費電視.md "wikilink")
      - 2008年：《[BOOM部落格](../Page/BOOM部落格.md "wikilink")》
      - 2008年：《[星級廚房](../Page/星級廚房.md "wikilink")》
      - 2008年：《[三個女人一個噓](../Page/三個女人一個噓.md "wikilink")》
  - [NetTV77](../Page/NetTV77.md "wikilink")
      - 2007年：《[NET人飯局](../Page/NET人飯局.md "wikilink")》
  - [蘋果動新聞](../Page/蘋果動新聞.md "wikilink")
      - 2008年：《[娛樂開Chat](../Page/娛樂開Chat.md "wikilink")》
  - [MyRadio](../Page/MyRadio.md "wikilink")
      - 2008年：《[人氣在線](../Page/人氣在線.md "wikilink")》
      - 2008年：《[大娛樂家](../Page/大娛樂家.md "wikilink")》

## 演出

### 演唱會

  - [Johnnie Walker Black Label
    FREEZE黑色聖誕演唱會](../Page/Johnnie_Walker_Black_Label_FREEZE黑色聖誕演唱會.md "wikilink")（[Star
    Hall舉行](../Page/Star_Hall.md "wikilink")）

### 音樂會

  - Freeze Mini Concert 2007
  - [新城娛樂台](../Page/新城知訊台.md "wikilink")[07樂壇新人大熱鬥](../Page/07樂壇新人大熱鬥.md "wikilink")
  - 香港各界青少年一心一印迎奧運音樂會
  - 「麗晶慈善繽紛夜」密西沙加華人專商業基金會籌款（加拿大）
  - Joe Jr. Here's a Heart 41 周年演唱會（加拿大）

## 主持節目

  - [無綫電視](../Page/無綫電視.md "wikilink")
    [唔使怕醜](../Page/唔使怕醜.md "wikilink")
      - 主持：Carisa、[陳道然](../Page/陳道然.md "wikilink")（陳淑蘭）
      - 全部播放完畢
  - [MyRadio](../Page/MyRadio.md "wikilink")
    [Freeze新地帶](../Page/Freeze新地帶.md "wikilink")
      - 環節：《Freeze週記》、《串串樂逍遙》、《淑皮Phone人院》及《珊珊來遲DelaisaYan》等
      - 主持：Carisa、Alley、Sukie
      - 全部播放完畢
  - [MyRadio](../Page/MyRadio.md "wikilink")
    [Freeze直播真人Show](../Page/Freeze直播真人Show.md "wikilink")
      - 嘉賓包括：[蕭若元](../Page/蕭若元.md "wikilink")、經理人[蕭定一](../Page/蕭定一.md "wikilink")、[梁雨恩](../Page/梁雨恩.md "wikilink")、[黃宇詩](../Page/黃宇詩.md "wikilink")、[郭思琳](../Page/郭思琳.md "wikilink")、[袁彌明](../Page/袁彌明.md "wikilink")、新城知訊台DJ[新香蕉俱樂部](../Page/新香蕉俱樂部.md "wikilink")、[瑪姬及](../Page/瑪姬.md "wikilink")[陳禮賢等](../Page/陳禮賢.md "wikilink")
      - 全部播放完畢
  - [MyRadio](../Page/MyRadio.md "wikilink") [Lady
    First](../Page/Lady_First.md "wikilink")
      - 主持：Alley、空姐Elanie、Ann Ho[何紫綸](../Page/何紫綸.md "wikilink")、Tina
      - 逢星期一晚上9時00分至晚上10時00分
  - [MyRadio](../Page/MyRadio.md "wikilink")
    [玄學123](../Page/玄學123.md "wikilink")
      - 主持：Carisa、天翼師傅、嘉賓主持Cody
      - 全部播放完畢

## 廣播劇

  - 2009年：《C1色生活》 ([MyRadio](../Page/MyRadio.md "wikilink"))

## 廣告

  - LIFE TEAM限量版Print Tee
  - 《神泣》限制版
  - Be A Lady 變靚D纖體美容中心（[變靚D控股](../Page/變靚D控股.md "wikilink")）
  - B.A.L. Medical Center
    變靚D美容醫療中心（[變靚D控股](../Page/變靚D控股.md "wikilink")）
  - 手機遊戲[美女擂台](../Page/美女擂台.md "wikilink")
  - Alte Native驚世BOTOX 40
  - Peo Trimza食纖秀
  - NARS skin

## 獎項

**2007年度**:

  - Super Brands for HIM 2007 - 最受讀者歡迎女子組合獎
  - [新城勁爆頒獎禮2007](../Page/2007年度新城勁爆頒獎禮得獎名單.md "wikilink") -
    新城勁爆跳舞歌曲《金睛火眼》
  - [新城勁爆頒獎禮2007](../Page/2007年度新城勁爆頒獎禮得獎名單.md "wikilink") - 新城勁爆新登場組合
  - [第5屆「勁歌王」全球華人樂壇音樂盛典](../Page/第5屆「勁歌王」全球華人樂壇音樂盛典獲獎名單.md "wikilink") -
    最有前途新人組合

**2008年度**:

  - [新城勁爆頒獎禮2008](../Page/2008年度新城勁爆頒獎禮得獎名單.md "wikilink") -
    新城勁爆跳舞歌曲《Shiba-Za》

## 相關連結

  - [Freeze直播真人Show](../Page/Freeze直播真人Show.md "wikilink")
  - [Freeze新地帶](../Page/Freeze新地帶.md "wikilink")

## 参考文献

## 外部連結

  - [One Dollar 一元製作室有限公司 - Freeze是旗下藝人](http://www.onedollar.com.hk)
  - [石詠莉網誌](https://web.archive.org/web/20071202112925/http://www.sentfun.hk/sukieshek/)
  - [陳樂榣網誌](https://web.archive.org/web/20071228125219/http://hk.myblog.yahoo.com/angelchen_hk)
  - [甄穎珊網誌](https://web.archive.org/web/20090814000933/http://hk.myblog.yahoo.com/carisa-yanwingshan/)

<center>

<table>
<tbody>
<tr class="odd">
<td><table>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p><small></small></p></td>
<td style="text-align: center;"><p><strong>香港人網MyRadio主持人</strong></p></td>
<td style="text-align: right;"></td>
</tr>
</tbody>
</table></td>
</tr>
<tr class="even">
<td><table>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p><strong><a href="../Page/香港人網MyRadio.md" title="wikilink">一台</a></strong></p></td>
<td style="text-align: left;"><p><a href="../Page/黃毓民.md" title="wikilink">黃毓民</a>｜<a href="../Page/蕭若元.md" title="wikilink">蕭若元</a>｜<a href="../Page/譚志強.md" title="wikilink">譚志強</a>｜<a href="../Page/李逆熵.md" title="wikilink">李逆熵</a>｜<a href="../Page/何秀蘭.md" title="wikilink">何秀蘭</a>｜<a href="../Page/梁國雄.md" title="wikilink">梁國雄</a>｜<a href="../Page/陶君行.md" title="wikilink">陶君行</a>｜<a href="../Page/麥玲玲.md" title="wikilink">麥玲玲</a>｜<a href="../Page/麥國風.md" title="wikilink">麥國風</a>｜<a href="../Page/曾健成.md" title="wikilink">曾健成</a>｜<a href="../Page/梁錦祥.md" title="wikilink">梁錦祥</a>｜<a href="../Page/林紀陶.md" title="wikilink">林紀陶</a>｜<a href="../Page/沈振盈.md" title="wikilink">沈振盈</a>｜<a href="../Page/鄧聲興.md" title="wikilink">鄧聲興</a>｜<a href="../Page/丁世民.md" title="wikilink">丁世民</a>｜<a href="../Page/黃偉康.md" title="wikilink">黃偉康</a>｜<a href="../Page/李愛詩.md" title="wikilink">李愛詩</a>｜<a href="../Page/黃德几.md" title="wikilink">黃德几</a>｜<a href="../Page/彭偉新.md" title="wikilink">彭偉新</a>｜<a href="../Page/鄧澤堂.md" title="wikilink">鄧澤堂</a>｜<a href="../Page/莊志豪.md" title="wikilink">莊志豪</a>｜<a href="../Page/姚浩然.md" title="wikilink">姚浩然</a>｜<a href="../Page/曾永堅.md" title="wikilink">曾永堅</a>｜<a href="../Page/刁民牛.md" title="wikilink">刁民牛</a>｜<a href="../Page/司徒薇.md" title="wikilink">司徒薇</a>｜<a href="../Page/俞若玫.md" title="wikilink">俞若玫</a>｜<a href="../Page/黎則奮.md" title="wikilink">黎則奮</a>｜<a href="../Page/溫石麟.md" title="wikilink">溫石麟</a>｜<a href="../Page/杜耀明.md" title="wikilink">杜耀明</a>｜<a href="../Page/麥洛新.md" title="wikilink">麥洛新</a>｜<a href="../Page/莫佩嫺.md" title="wikilink">莫佩嫺</a>｜<a href="../Page/黃偉鴻.md" title="wikilink">黃偉鴻</a>｜<a href="../Page/李偉儀.md" title="wikilink">李偉儀</a>｜<a href="../Page/張錦雄.md" title="wikilink">張錦雄</a>｜<a href="../Page/朱文俊.md" title="wikilink">朱文俊</a>｜<a href="../Page/何故.md" title="wikilink">何故</a>｜<a href="../Page/Gary_Hui.md" title="wikilink">Gary Hui</a></p></td>
<td style="text-align: right;"></td>
</tr>
</tbody>
</table></td>
</tr>
<tr class="odd">
<td><table>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p><strong><a href="../Page/香港人網MyRadio.md" title="wikilink">二台</a></strong></p></td>
<td style="text-align: left;"><p><a href="../Page/潘紹聰.md" title="wikilink">潘紹聰</a>｜<a href="../Page/蕭定一.md" title="wikilink">蕭定一</a>｜<a href="../Page/袁彌明.md" title="wikilink">袁彌明</a>｜<a href="../Page/梁繼璋.md" title="wikilink">梁繼璋</a>｜<a href="../Page/李麗蕊.md" title="wikilink">李麗蕊</a>｜<strong>Freeze</strong>（<a href="../Page/甄穎珊.md" title="wikilink">甄穎珊</a>、<a href="../Page/陳樂榣.md" title="wikilink">陳樂榣</a>、<a href="../Page/石詠莉.md" title="wikilink">石詠莉</a>）｜<a href="../Page/林雨陽.md" title="wikilink">林雨陽</a>｜<a href="../Page/何紫綸.md" title="wikilink">何紫綸</a>｜<a href="../Page/王穎.md" title="wikilink">王穎</a>｜<a href="../Page/羽翹.md" title="wikilink">羽翹</a>｜<a href="../Page/黃偉鴻.md" title="wikilink">黃偉鴻</a></p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

</center>

[Category:香港女子演唱團體](../Category/香港女子演唱團體.md "wikilink")
[Category:粵語流行音樂歌手](../Category/粵語流行音樂歌手.md "wikilink")
[Category:2007年成立的音樂團體](../Category/2007年成立的音樂團體.md "wikilink")

1.

2.

3.