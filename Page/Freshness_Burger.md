[位於銅鑼灣皇室堡的Freshness_Burger分店.JPG](https://zh.wikipedia.org/wiki/File:位於銅鑼灣皇室堡的Freshness_Burger分店.JPG "fig:位於銅鑼灣皇室堡的Freshness_Burger分店.JPG")

**鮮堡**，英文名：**Freshness
Burger**，是一家[日本](../Page/日本.md "wikilink")[快餐](../Page/快餐.md "wikilink")[連鎖店](../Page/連鎖店.md "wikilink")，於1992年成立。它主要售賣[漢堡包](../Page/漢堡包.md "wikilink")，也售賣[三文治](../Page/三文治.md "wikilink")、[沙律及](../Page/沙律.md "wikilink")[咖啡飲料](../Page/咖啡.md "wikilink")。

它也在[新加玻](../Page/新加玻.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門](../Page/澳門.md "wikilink")、[南韓及](../Page/南韓.md "wikilink")[日本擁有](../Page/日本.md "wikilink")177家分店。分店擴張以[特許經營權方式](../Page/特許經營權.md "wikilink")。

2007年正式登陸香港，現時分店包括：有分並先後在以下地方開設分店：

  - [旺角](../Page/旺角.md "wikilink")[新世紀廣場](../Page/新世紀廣場.md "wikilink")

亦曾於香港以下地方開設分店，但現已結業：

  - [尖沙咀](../Page/尖沙咀.md "wikilink")[海港城](../Page/海港城.md "wikilink")
  - [尖沙咀](../Page/尖沙咀.md "wikilink")[新世界中心](../Page/新世界中心.md "wikilink")
  - [長沙灣亞洲高爾夫球會Asia](../Page/長沙灣.md "wikilink") Golf Club
  - [銅鑼灣](../Page/銅鑼灣.md "wikilink")[皇室堡](../Page/皇室堡.md "wikilink")
  - [半山區般咸道近](../Page/半山區.md "wikilink")[聖保羅書院](../Page/聖保羅書院.md "wikilink")/[香港大學](../Page/香港大學.md "wikilink")
  - [荃灣遠東帝豪大廈](../Page/荃灣.md "wikilink")
  - [沙田](../Page/沙田.md "wikilink")[連城廣場](../Page/連城廣場.md "wikilink")

2009年正式登陸[澳門](../Page/澳門.md "wikilink")

## 同類競爭對手

  - [摩斯漢堡](../Page/摩斯漢堡.md "wikilink")
  - [麥當勞](../Page/麥當勞.md "wikilink")
  - [漢堡王](../Page/漢堡王.md "wikilink")
  - [温娣漢堡](../Page/温娣漢堡.md "wikilink")

## 外部連結

  - [鮮堡Freshness Burger日本官方網站](http://www.freshnessburger.co.jp)
  - [鮮堡Freshness
    Burger南韓官方網站](https://web.archive.org/web/20110207161120/http://freshness-burger.com/)

  - Freshness
    Burger香港[尖沙咀](http://www.openrice.com/restaurant/sr2.htm?shopid=18635)及[銅鑼灣](http://www.openrice.com/restaurant/sr2.htm?shopid=19207)食評

  - [頭條日報 -
    鮮堡](http://202.66.86.28/Repository/GetImage.dll?Path=HeadLine/2008/06/04/3/Img/Pg003.png)
  - [香港、澳門官方**鮮堡**Freshness
    Burger網址](https://web.archive.org/web/20130524210115/http://freshnessburger.com.hk/)

[F](../Category/日本連鎖速食店.md "wikilink")
[Category:日本餐飲公司](../Category/日本餐飲公司.md "wikilink")
[Category:跨國公司](../Category/跨國公司.md "wikilink")
[Category:速食餐廳](../Category/速食餐廳.md "wikilink")
[Category:1992年日本建立](../Category/1992年日本建立.md "wikilink")