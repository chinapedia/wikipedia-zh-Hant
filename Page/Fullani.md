《**Fullani**》（）是[Leaf在](../Page/Leaf.md "wikilink")2006年7月28日發售的[成人遊戲](../Page/日本成人遊戲.md "wikilink")。

## 慨要

雖然是以2人對局的[脫衣麻雀為基礎](../Page/脫衣麻雀.md "wikilink")，但既有[冒險遊戲般的故事元素](../Page/冒險遊戲.md "wikilink")，亦有如標題一樣整個故事都以[動畫進行的特徵](../Page/動畫.md "wikilink")。不過是一個並沒有劇本作家的遊戲，擔任全部登場角色的設定和造型設計都是。由於是動畫的關係，比《[沒有天使的12月](../Page/沒有天使的12月.md "wikilink")》和《[ToHeart2](../Page/ToHeart2.md "wikilink")》少了描繪線，取而代之以鮮明色彩來設計。除此之外，也許是反映了的嗜好，除了人類之外，鳥類也很常出現。

## 故事

非常喜歡[網路遊戲的青年](../Page/網路遊戲.md "wikilink") **諸角勇**，某天也如常地埋首於電腦
時，住在鄰房的[青梅竹馬](../Page/青梅竹馬.md "wikilink")**倫子**弄錯了房間。一如往常地當倫子要往回去的時候卻發生了意外，突然而來的閃電使房內的屏幕發出白光，兩人失去意識。

當勇醒回來的時候，眼前出現的是自己剛才一直沉迷其中的，網路遊戲的世界……

## 登場角色

  -
    主角。超級網路遊戲迷。某日由於突然而來的雷擊而墮入自己正在遊玩的遊戲世界，變成「勇者大人」而去拯救公主。
    雖然公式網頁的設定是住在6[疊](../Page/疊蓆.md "wikilink")1間（約9.7平方公尺的房間）的[公寓裏的窮學生](../Page/公寓.md "wikilink")，實際上他的房間是8疊（約13平方公尺），而且看起來最少也有約4倍大面積的頗大房間。除此之外，房屋也是有8層高的[大樓](../Page/大樓.md "wikilink")（根據[國土交通省的定義](../Page/國土交通省.md "wikilink")，超過3層高的[大廈不叫做](../Page/大廈.md "wikilink")「公寓」）。
  -
    住在勇鄰家的[青梅竹馬](../Page/青梅竹馬.md "wikilink")、同校的女生。健康開朗，經常纏著主角。跟勇一起墮進遊戲世界，在那裏變成了公主，但倫子卻完全失去了記憶。
  -
    遊戲世界裏的人，[吸精女妖](../Page/夢魔.md "wikilink")。好奇心旺盛，與其說是有敵意，倒說是多管閑事地向勇挑戰。
  -
    遊戲世界裏的人，[修女](../Page/修女.md "wikilink")。性格頑固而且過份認真，公主的護衛。也有不太懂世事和笨手笨腳的一面。
  -
    遊戲世界裏的人，貓耳娘。天真無邪而爽朗，天性就是製造麻煩。
  -
    遊戲世界裏的人，見習[魔法使](../Page/魔法使.md "wikilink")。沉默而性格像大人的女孩。
  -
    Uluru的[使魔](../Page/使魔.md "wikilink")，以[鸚鵡的樣子付在](../Page/鸚形目.md "wikilink")[杖的前端的姿態出現](../Page/杖.md "wikilink")。行為比主人還了不起的樣子，不知為甚麼會說[關西腔](../Page/關西腔.md "wikilink")。
  -
    遊戲世界裏的人，女王大人。是個獨裁統治者，那種站著的姿態看起來常常充滿威嚴。

## 工作人員

  - 總合製作人：[下川直哉](../Page/下川直哉.md "wikilink")
  - 動畫製作人：望月雄太郎
  - 導演：鷲見努
  - [角色設計](../Page/角色設計.md "wikilink")：
  - 圖片：、秋葉秀樹等
  - 音樂：松岡純也、石川真也
  - 麻雀引擎製作：株式會社童
  - 動畫角色設計、[作畫監督](../Page/作畫監督.md "wikilink")：
  - 動畫製作：、[京江ANIA](../Page/京江ANIA.md "wikilink")、[Chaos
    Project等](../Page/Chaos_Project.md "wikilink")

## 主題歌

  - 片頭曲：「**just now**」
      - 作詞：須谷尚子，作曲、編曲：松岡純也，歌：Clap
  - 片尾曲：「」（希望的風）
      - 作詞：U，作曲、編曲：石川真也，歌：[元田惠美](../Page/元田惠美.md "wikilink")

## 參考資料

  - [Getchu.com：](http://www.getchu.com/soft.phtml?id=232485)

## 外部連結

  - [Leaf OFFICIAL
    WEBSITE（需確認年齡）](http://leaf.aquaplus.co.jp/product/fullani/)


[Category:Leaf](../Category/Leaf.md "wikilink")
[Category:美少女遊戲](../Category/美少女遊戲.md "wikilink")
[Category:2006年电子游戏](../Category/2006年电子游戏.md "wikilink")
[Category:2006年日本成人遊戲](../Category/2006年日本成人遊戲.md "wikilink")