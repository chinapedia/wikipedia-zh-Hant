**GAGAGA文庫**（）是[小學館在](../Page/小學館.md "wikilink")2007年5月24日創刊的[輕小說文庫系列](../Page/輕小說.md "wikilink")。

是繼2001年同出版社休刊的Super Quest文庫後再度加入輕小說市場，但在創刊時報導的有如新加入市場一般\[1\]\[2\]。

## 簡介

  - 在成立時事實上繼承了Super Quest文庫的路線，初期作品以遊戲和小說化作品為中心。
  - 另一方面一年前的2006年4月開始，和姐妹文庫LuLuLu文庫一同舉辦「小學館輕小說大獎」，同時在網站上招募插畫家。
  - 以「少年向娛樂」為廣告詞，作為新嘗試而成立將日本近代文學加入輕小說色彩改寫的跳譯系列。
  - 有出版能夠分類到官能小說，充滿色情和暴力的作品，在創刊同時也出版了成人遊戲的改編小說，有和廣告詞不符的批評。

## 作品一覽

### 東立出版社

  - [臨界殺機](../Page/臨界殺機.md "wikilink")（[神崎紫電](../Page/神崎紫電.md "wikilink")／[kyo](../Page/kyo.md "wikilink")）
  - [學園歌劇！](../Page/學園歌劇！.md "wikilink")（[山川進](../Page/山川進.md "wikilink")／[よし☆ヲ](../Page/よし☆ヲ.md "wikilink")）
  - [神明大人會把笑心○○○？](../Page/神明大人會把笑心○○○？.md "wikilink")（[荒川工](../Page/荒川工.md "wikilink")／[ことみようじ](../Page/ことみようじ.md "wikilink")）
  - [黑與藍
    Black\&Blue](../Page/黑與藍_Black&Blue.md "wikilink")（[中岡潤一郎](../Page/中岡潤一郎.md "wikilink")／[藤城陽](../Page/藤城陽.md "wikilink")）
  - [M之奇談](../Page/M之奇談.md "wikilink")（[三上康明](../Page/三上康明.md "wikilink")／[水沢深森](../Page/水沢深森.md "wikilink")）
  - [人氣妹妹與受難的我](../Page/人氣妹妹與受難的我.md "wikilink")（[夏綠](../Page/夏綠.md "wikilink")／[GIN](../Page/GIN.md "wikilink")）
  - [利維坦的戀人](../Page/利維坦的戀人.md "wikilink")（[犬村小六](../Page/犬村小六.md "wikilink")／[赤星建次](../Page/赤星建次.md "wikilink")）
  - [羽月莉音的帝國](../Page/羽月莉音的帝國.md "wikilink")（[至道流星](../Page/至道流星.md "wikilink")／[二之膳](../Page/二之膳.md "wikilink")）
  - [有看到我家的魔女嗎？](../Page/有看到我家的魔女嗎？.md "wikilink")（[山川進](../Page/山川進.md "wikilink")／[CUTEG](../Page/CUTEG.md "wikilink")）
  - [於是，他就這樣燒了屋頂](../Page/於是，他就這樣燒了屋頂.md "wikilink")（[カミツキレイニー](../Page/カミツキレイニー.md "wikilink")／[文倉十](../Page/文倉十.md "wikilink")）
  - [脫兔的反擊](../Page/脫兔的反擊.md "wikilink")（[秀章](../Page/秀章.md "wikilink")／[ky](../Page/ky.md "wikilink")）
  - [鎖鎖美小姐@不好好努力](../Page/鎖鎖美小姐@不好好努力.md "wikilink")（[日日日](../Page/日日日.md "wikilink")／[左](../Page/左_\(插畫家\).md "wikilink")）
  - [GJ部](../Page/GJ部.md "wikilink")（[新木伸](../Page/新木伸.md "wikilink")／[あるや](../Page/あるや.md "wikilink")）
  - [她，唯有一死](../Page/她，唯有一死.md "wikilink")（[石川周](../Page/石川周.md "wikilink")／[八重樫南](../Page/八重樫南.md "wikilink")）
  - [CROWN FLINT
    雙合透鏡](../Page/CROWN_FLINT_雙合透鏡.md "wikilink")（[三上康明](../Page/三上康明.md "wikilink")／[純珪一](../Page/純珪一.md "wikilink")）
  - [傀儡師神樂](../Page/傀儡師神樂.md "wikilink")（[賽目和七](../Page/賽目和七.md "wikilink")／[マニャ子](../Page/マニャ子.md "wikilink")）
  - [人生](../Page/人生_\(輕小說\).md "wikilink")（[川岸毆魚](../Page/川岸毆魚.md "wikilink")／[ななせめるち](../Page/ななせめるち.md "wikilink")）
  - [如果有妹妹就好了。](../Page/如果有妹妹就好了。.md "wikilink")（[平坂讀](../Page/平坂讀.md "wikilink")／[カントク](../Page/監督_\(插畫家\).md "wikilink")）
  - [4 cours after
    四季之後](../Page/4_cours_after_四季之後.md "wikilink")（／bun150）

### 尖端出版

  - [旋風管家](../Page/旋風管家.md "wikilink") 在放春假的白皇學院裡，我看到了夢幻的三千院？
    by小颯（原作、作畫：[畑健二郎](../Page/畑健二郎.md "wikilink")
    著：[築地俊彦](../Page/築地俊彦.md "wikilink")）
  - [人類衰退之後](../Page/人類衰退之後.md "wikilink")（[田中羅密歐](../Page/田中羅密歐.md "wikilink")／[山崎透](../Page/山崎透.md "wikilink")）
  - [RIGHT×LIGHT](../Page/RIGHT×LIGHT.md "wikilink")（[司](../Page/司.md "wikilink")／[近衛乙嗣](../Page/近衛乙嗣.md "wikilink")）
  - [對某飛行員的追憶](../Page/對某飛行員的追憶.md "wikilink")（[犬村小六](../Page/犬村小六.md "wikilink")／[森澤晴行](../Page/森澤晴行.md "wikilink")）
  - [AURA
    〜魔龍院光牙最後的戰鬥〜](../Page/AURA_〜魔龍院光牙最後的戰鬥〜.md "wikilink")（田中羅密歐／[mebae](../Page/mebae.md "wikilink")）
  - [只有神知道的世界](../Page/只有神知道的世界.md "wikilink")（[有澤真水](../Page/有澤真水.md "wikilink")／[若木民喜](../Page/若木民喜.md "wikilink")）
  - [罪人與龍共舞](../Page/罪人與龍共舞.md "wikilink")（[浅井ラボ](../Page/浅井ラボ.md "wikilink")／[宮城](../Page/宮城.md "wikilink")）
  - [獻給某飛行員的戀歌](../Page/獻給某飛行員的戀歌.md "wikilink")（犬村小六／森澤晴行）
  - [邪神大沼](../Page/邪神大沼.md "wikilink")（[川岸毆魚](../Page/川岸毆魚.md "wikilink")／[Ixy](../Page/Ixy.md "wikilink")）
  - [綻放花朵的飛行兵器](../Page/綻放花朵的飛行兵器.md "wikilink")（[杉井光](../Page/杉井光.md "wikilink")／[LLO](../Page/LLO.md "wikilink")）
  - [死亡倒數遊戲～他能在當天免於一死嗎～](../Page/死亡倒數遊戲～他能在當天免於一死嗎～.md "wikilink")（[小木君人](../Page/小木君人.md "wikilink")／[植田亮](../Page/植田亮.md "wikilink")）
  - [鋼鐵之翼](../Page/鋼鐵之翼.md "wikilink")（[虛淵玄](../Page/虛淵玄.md "wikilink")／[中央東口](../Page/中央東口.md "wikilink")）
  - [果然我的青春戀愛喜劇搞錯了。](../Page/果然我的青春戀愛喜劇搞錯了。.md "wikilink")（[渡航](../Page/渡航.md "wikilink")／[ぽんかん⑧](../Page/ぽんかん⑧.md "wikilink")）
  - [寄生彼女砂奈](../Page/寄生彼女砂奈.md "wikilink")（[砂義出雲](../Page/砂義出雲.md "wikilink")／[瑠奈璃亞](../Page/瑠奈璃亞.md "wikilink")）
  - [我和妳有致命的認知差異](../Page/我和妳有致命的認知差異.md "wikilink")（[赤月カケヤ](../Page/赤月カケヤ.md "wikilink")／[晩杯あきら](../Page/晩杯あきら.md "wikilink")）
  - [我，要成為雙馬尾](../Page/我，要成為雙馬尾.md "wikilink")（[水澤夢](../Page/水澤夢.md "wikilink")／[春日步](../Page/春日步.md "wikilink")）
  - [下流梗不存在的灰暗世界](../Page/下流梗不存在的灰暗世界.md "wikilink")（[赤城大空](../Page/赤城大空.md "wikilink")／[霜月えいと](../Page/霜月えいと.md "wikilink")）
  - [魔王之類的啦！](../Page/魔王之類的啦！.md "wikilink")（[原田源五郎](../Page/原田源五郎.md "wikilink")／[nyanya](../Page/nyanya.md "wikilink")）
  - [企業傭兵](../Page/企業傭兵.md "wikilink")（虛淵玄／[廣江禮威](../Page/廣江禮威.md "wikilink")）
  - [灼熱的小早川同學](../Page/灼熱的小早川同學.md "wikilink")（田中羅密歐／[西邑](../Page/西邑.md "wikilink")）
  - [獻給某飛行員的夜曲](../Page/獻給某飛行員的夜曲.md "wikilink")（犬村小六／森澤晴行）
  - [弱角友崎同學](../Page/弱角友崎同學.md "wikilink")（[屋久悠樹](../Page/屋久悠樹.md "wikilink")／[フライ](../Page/フライ.md "wikilink")）

### 台灣東販

  - [向森之魔物獻上花束](../Page/向森之魔物獻上花束.md "wikilink")（[小木君人](../Page/小木君人.md "wikilink")／[そと](../Page/そと.md "wikilink")）

### 安徽少年儿童出版社

  - [我的青春恋爱喜剧果然有问题](../Page/我的青春恋爱喜剧果然有问题.md "wikilink")（[渡航](../Page/渡航.md "wikilink")／[ponkan⑧](../Page/ponkan⑧.md "wikilink")）

### 99读书人

  - [对某飞行员的追忆](../Page/对某飞行员的追忆.md "wikilink")（[犬村小六](../Page/犬村小六.md "wikilink")／[森泽晴行](../Page/森泽晴行.md "wikilink")）
  - [献给某飞行员的恋歌](../Page/献给某飞行员的恋歌.md "wikilink")（犬村小六／森泽晴行）
  - [献给某飞行员的夜想曲](../Page/献给某飞行员的夜想曲.md "wikilink")（犬村小六／森泽晴行）
  - [时间商人系列](../Page/时间商人.md "wikilink")（水市惠／）

### 未代理

  - [天元突破
    紅蓮螺巖](../Page/天元突破_紅蓮螺巖.md "wikilink")（原作：[GAINAX](../Page/GAINAX.md "wikilink")
    著：[砂山藏澄](../Page/砂山藏澄.md "wikilink")／[品川宏樹](../Page/品川宏樹.md "wikilink")）
  - [地球防衛少年](../Page/地球防衛少年.md "wikilink")
    〜alternative〜（原作、作畫：[鬼頭莫宏](../Page/鬼頭莫宏.md "wikilink")
    著：[大樹連司](../Page/大樹連司.md "wikilink")）
  - [日本鎖國](../Page/日本鎖國.md "wikilink") 〜my winding
    road〜（[谷崎央佳](../Page/谷崎央佳.md "wikilink")
    原案・監修：[曾利文彥](../Page/曾利文彥.md "wikilink")／[緒方剛志](../Page/緒方剛志.md "wikilink")）

## 註解

<references/>

## 外部連結

  - [小學館·GAGAGA文庫](http://gagagabunko.jp/)
  - [GAGAGA文庫編輯部](https://web.archive.org/web/20071111115801/http://ga3.gagaga-lululu.jp/write/)

[\*](../Category/Gagaga文庫.md "wikilink")
[Category:小學館](../Category/小學館.md "wikilink")

1.  [](http://animeanime.jp/news/archives/2006/04/2007411.html)
2.  [](http://www.shoten.co.jp/nisho/bookstore/shinbun/view.asp?PageViewNo=5675)