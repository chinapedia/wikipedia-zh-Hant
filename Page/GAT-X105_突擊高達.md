**GAT-X105 攻擊鋼彈（Strike）**為[日本動畫](../Page/日本.md "wikilink")[機動戰士鋼彈
SEED中虛構的有人式人型機械兵器](../Page/機動戰士鋼彈_SEED.md "wikilink")（[MS](../Page/機動戰士.md "wikilink")），劇中前半為主人翁[基拉·大和之座機](../Page/基拉·大和.md "wikilink")，後半改由[穆·拉·福拉卡搭乘](../Page/穆·拉·弗拉加.md "wikilink")。機體名「Strike」為英語「攻擊」之意，機體設計為[大河原邦男](../Page/大河原邦男.md "wikilink")。特徵為白、藍、紅此一歷來作品中Gundam
Type常見的三色著色，以及換裝背包對應多樣戰場的泛用機設定。

[gat-x105.jpg](https://zh.wikipedia.org/wiki/File:gat-x105.jpg "fig:gat-x105.jpg")

## 開發經過

長久以來，[MA](../Page/機動裝甲.md "wikilink")（機動堡壘）一直作為人類在[宇宙中搭乘的戰鬥用飛行器](../Page/宇宙.md "wikilink")，而[MS僅用於](../Page/機動戰士.md "wikilink")[宇宙中的工事營造](../Page/宇宙.md "wikilink")。然而[C.E.](../Page/C.E..md "wikilink")
69年，[P.L.A.N.T為了尋求](../Page/P.L.A.N.T.md "wikilink")[自治與](../Page/自治.md "wikilink")[貿易自主權](../Page/貿易.md "wikilink")，其軍事機關[Z.A.F.T.首次將軍事化的](../Page/ZAFT.md "wikilink")[MS](../Page/機動戰士.md "wikilink")—[基恩投入戰場](../Page/ZGMF-1017_GINN.md "wikilink")，並以此為主力迅速排除了[地球方面派駐在](../Page/地球.md "wikilink")[P.L.A.N.T宙域的宇宙軍](../Page/P.L.A.N.T.md "wikilink")。[大西洋聯邦宇宙軍第四艦隊](../Page/大西洋聯邦.md "wikilink")（即後來的[地球聯合軍第八艦隊](../Page/地球聯合#地球連合軍.md "wikilink")）的哈爾巴頓上校注意到了[MS在](../Page/機動戰士.md "wikilink")[宇宙空間中戰鬥的優勢](../Page/宇宙.md "wikilink")，遂向司令部提交了[MS的開發提案](../Page/機動戰士.md "wikilink")。儘管當時軍方高層普遍輕視[Z.A.F.T.的戰力](../Page/ZAFT.md "wikilink")，認為仿傚[調整者以](../Page/调整者_\(GUNDAM\).md "wikilink")[MS為主的戰鬥型態毫無必要](../Page/機動戰士.md "wikilink")。哈爾巴頓上校仍開始了代號「[G兵器](../Page/G兵器.md "wikilink")」的[MS開發計畫](../Page/機動戰士.md "wikilink")，但因未獲重視而進展遲緩。

[C.E.](../Page/C.E..md "wikilink")
70年7月後，大戰爆發已近半年，[地球聯合軍苦於戰事膠著](../Page/地球聯合#地球連合軍.md "wikilink")，[MS開發遲滯的](../Page/機動戰士.md "wikilink")[大西洋聯邦看中了中立國](../Page/大西洋聯邦.md "wikilink")[歐普聯合首長國](../Page/O.R.B..md "wikilink")。其作為地球上唯一[自然人與](../Page/自然人.md "wikilink")[調整者共存共榮的國家](../Page/调整者_\(GUNDAM\).md "wikilink")，國內擁有大量[調整者人才](../Page/调整者_\(GUNDAM\).md "wikilink")，便向[歐普提出協助開發的邀請](../Page/O.R.B..md "wikilink")。儘管[歐普代表](../Page/O.R.B..md "wikilink")[烏茲米·納拉·阿斯哈以戰前發表的](../Page/機動戰士GUNDAM_SEED系列角色列表#第一季登場角色.md "wikilink")「中立宣言」為由予以拒絕，但國內部分反對阿斯哈一貫中立政策的[氏族們仍私下與](../Page/氏族.md "wikilink")[大西洋聯邦秘密接洽](../Page/大西洋聯邦.md "wikilink")，由[歐普的公營軍工企業](../Page/O.R.B..md "wikilink")[曙光社提供技術協力](../Page/曙光社.md "wikilink")，並於[歐普所屬資源](../Page/O.R.B..md "wikilink")[衛星](../Page/衛星.md "wikilink")[殖民地](../Page/殖民地.md "wikilink")「海利歐波里斯」極秘製造代號「[G兵器](../Page/G兵器.md "wikilink")」的前期GAT-X系列[MS共五機](../Page/機動戰士.md "wikilink")。本機是五機之中最後完成的機體，採用了與[決鬥鋼彈](../Page/GAT-X102_Dual.md "wikilink")、[暴風鋼彈同系列的X](../Page/GAT-X103_Buster.md "wikilink")100框架為基本骨架。

[C.E.](../Page/C.E..md "wikilink")
71年1月20日，五機GAT-X系列機體與新型宇宙戰艦兼[MS母艦的](../Page/機動戰士.md "wikilink")[大天使號秘密出廠](../Page/LCAM-01XA_Archangel.md "wikilink")，而本機**GAT-X105
Strike**為其中唯一一機導入「背包」設計，可換裝為不同裝備以對應各種戰況的萬能[MS](../Page/機動戰士.md "wikilink")。

## 機體介紹

本機的最大特徵是獨自的裝備換裝機構「**-{zh-hans:突击;zh-hk:突擊;zh-tw:攻擊;}-者背包系統（Striker Pack
System）**」\[1\]，可對應各種戰況做合適的裝備換裝，達到一機擁有與各種專用機同等以上性能的目的。本機共有**翔翼型（Aile）**、**巨劍型（Sword）**和**重砲型（Launcher）**三種**攻擊者背包（Striker
Pack）**，均裝備了當時戰場上仍屬少見的光束兵器。另外也內藏了兼作機體預備電源的電池，如此一來可在戰鬥中同時接受補給，並即刻返回戰線。

本機的固定武裝僅有**75mm對空自動火神砲砲塔系統「豪豬陣」**，內藏於頭部兩側的兩門對空防禦機關砲，用以迎擊接近的敵機或飛彈，為聯合軍軍機、艦艇的標準火器。兩腰內側配備有**對裝甲戰鬥刀「破甲者」**，通過超振動電機令超硬度金屬制的刀刃產生高周波振動，可切斷除[PS裝甲以外的大部分物體](../Page/PS裝甲.md "wikilink")。戰鬥刀本體內建小型電源，不必擔心耗費本機剩餘電力是使用上一大利點。主武裝為**57mm高能量光束步槍**，聯合軍領先[Z.A.F.T.導入的小型化光束兵器](../Page/Z.A.F.T..md "wikilink")，遠比[基恩的](../Page/ZGMF-1017_Ginn.md "wikilink")「象式改」特火重粒子砲要小型，但卻擁有一擊破壞羅拉西亞級[戰艦外裝的威力](../Page/戰艦.md "wikilink")。通過手掌部的連接元直接從機體供給電力，因此彈數是受到機體活動時間的制約。另外還配備了一面**對光束防盾**，以可將光束擴散、吸收的特殊塗料施作了塗層，材料是由以固有頻率引起特殊共鳴的同類鋼材複合金屬所作成。盾為大型規格，雖然有效防禦面很廣，但對於接近戰的靈活性以及並用大型火器時帶來一些困擾，因此裝備巨劍型與重砲型攻擊者背包時並不攜行防盾。

為了進一步提升本機的作戰能力，同時開發的[空中霸者作為本機的支援機](../Page/FX-550_Skygrasper.md "wikilink")。以聯合軍的垂直起降運輸機為基礎，可載運一套攻擊者背包以解決本機可能的不時之需，更能夠直接運用背包上的武裝協助本機戰鬥。

故事前半由[煌·大和駕駛本機](../Page/煌·大和.md "wikilink")，創下了各式各樣的戰績而被譽為「前線最強」，故事後半改由[穆‧拉‧福拉卡搭乘](../Page/穆‧拉‧福拉卡.md "wikilink")。另外，本機採用的「攻擊者背包系統」確立了換裝作為嶄新的設計思想，對於後來各勢力的[MS帶來很大影響](../Page/機動戰士.md "wikilink")。作為本機直系的量產型有地球聯合軍的[刃式](../Page/GAT-01A1_Dagger.md "wikilink")（通稱105刃），以及簡略化的[攻擊刃被生產出來](../Page/GAT-01_Strike_Dagger.md "wikilink")。[歐普軍通過修復本機](../Page/歐普.md "wikilink")，複製出[嫣紅攻擊鋼彈](../Page/MBF-02_Strike_Rouge.md "wikilink")，並開發出同為X100框架的[曉](../Page/ORB-01_Akatsuki.md "wikilink")，[M1
異端的背包構造亦受到本機翔翼型攻擊者的影響](../Page/MBF-M1_M1_Astray.md "wikilink")。[C.E.](../Page/C.E..md "wikilink")
73年，[Z.A.F.T.亦於其新一代量產型](../Page/ZAFT.md "wikilink")[MS](../Page/機動戰士.md "wikilink")「新千禧系列」導入了換裝機能「巫師系統」，並進一步發展出[脈衝鋼彈的](../Page/ZGMF-X56S_Impulse.md "wikilink")「魅影系統」。

## 機體諸元

  - 型號：GAT-X105
  - 名稱：Strike
  - 建造：[大西洋聯邦](../Page/大西洋聯邦.md "wikilink")、[歐普聯合首長國](../Page/歐普.md "wikilink")
  - 所屬：地球聯合軍→三艦同盟
  - 分類：X100系‧裝備換裝型試作[MS](../Page/機動戰士.md "wikilink")
  - 全高：17.72米\[2\]
  - 重量：64.8噸\[3\]
  - 裝甲材質：[PS裝甲](../Page/相轉移裝甲.md "wikilink")
  - 作業系統：MOBILE SUIT OPERATION SYSTEM「**G**eneral **U**nilateral
    **N**euro-Link **D**ispersive **A**utonomic **M**aneuver Synthesis
    System」
  - 駕駛員：[煌·大和](../Page/煌·大和.md "wikilink")\[4\]、[穆·拉·福拉卡](../Page/穆·拉·福拉卡.md "wikilink")、史溫‧卡爾‧巴揚（再生機）

### 特殊裝備或機能

  - 攻擊者背包系統

## 戰鬥史

### C.E. 71年

  - 1月25日　海利歐波里斯崩壞事件：[歐普聯合首長國的資源](../Page/歐普.md "wikilink")[衛星](../Page/衛星.md "wikilink")[殖民地](../Page/殖民地.md "wikilink")「海利歐波里斯」，[Z.A.F.T.所屬克魯澤隊的](../Page/ZAFT.md "wikilink")「鋼彈奪取作戰」展開。身為工業學院學生的[煌·大和意外搭乘上唯一未被奪去的本機](../Page/煌·大和.md "wikilink")，並擊墜[Z.A.F.T.](../Page/ZAFT.md "wikilink")「黃昏的魔彈」[米蓋爾·艾曼駕駛的](../Page/米蓋爾·艾曼.md "wikilink")[基恩](../Page/ZGMF-1017_GINN.md "wikilink")。
  - 1月29日　脫離[歐亞聯邦所屬](../Page/歐亞聯邦.md "wikilink")[宇宙要塞](../Page/宇宙.md "wikilink")「阿爾緹密斯」。
  - 2月7日　[大天使號與](../Page/LCAM-01XA_Archangel.md "wikilink")[地球聯合軍第八艦隊的先遣隊合流](../Page/地球聯合#地球連合軍.md "wikilink")，遭到[Z.A.F.T.克魯澤隊伏擊](../Page/ZAFT.md "wikilink")，本機雖守住[大天使號](../Page/LCAM-01XA_Archangel.md "wikilink")，但先遣隊全滅，隨行的[大西洋聯邦事務次官阿斯達死亡](../Page/大西洋聯邦.md "wikilink")。
  - 2月13日　低軌道會戰：掩護[大天使號下降的](../Page/LCAM-01XA_Archangel.md "wikilink")[地球聯合軍第八艦隊與](../Page/地球聯合#地球連合軍.md "wikilink")[Z.A.F.T.克魯澤隊交戰](../Page/ZAFT.md "wikilink")，屈於劣勢。[大天使號試圖支援第八艦隊](../Page/LCAM-01XA_Archangel.md "wikilink")，本機在有限時間內出擊助陣，但仍無法阻止第八艦隊的覆滅，司令官哈爾巴頓提督戰死。本機試圖救援被[決鬥鋼彈射擊的逃生艇失敗](../Page/GAT-X102_Duel.md "wikilink")，造成[大天使號降落地點發生誤差](../Page/LCAM-01XA_Archangel.md "wikilink")，來到[北非](../Page/北非.md "wikilink")。
  - 2月28日　於[非洲共同體的](../Page/非洲共同體.md "wikilink")[利比亞境內沙漠](../Page/利比亞.md "wikilink")，[Z.A.F.T.沃特菲德隊追擊](../Page/ZAFT.md "wikilink")[大天使號](../Page/LCAM-01XA_Archangel.md "wikilink")，本機擊破多台[巴庫](../Page/TMF/A-802_巴庫.md "wikilink")，並打敗「沙漠之虎」[安德烈·沃特菲德駕駛的](../Page/安德烈·沃特菲德.md "wikilink")[拉寇](../Page/TMF/A-803_LaGOWE.md "wikilink")。
  - 3月3日　於[紅海遭到](../Page/紅海.md "wikilink")[Z.A.F.T.](../Page/ZAFT.md "wikilink")「[紅海之鯱](../Page/紅海.md "wikilink")」[馬可‧摩拉西姆率領的摩拉西姆隊駕駛](../Page/馬可‧摩拉西姆.md "wikilink")[迪恩襲擊](../Page/AMF-101_Dinn.md "wikilink")，成功擊退之。
  - 3月7日　再度與摩拉西姆隊交戰，本機在海底擊破多台[古恩以及](../Page/UMF-04A_GOOhN.md "wikilink")[馬可‧摩拉西姆的](../Page/馬可‧摩拉西姆.md "wikilink")[佐諾](../Page/UMF-5_Zno.md "wikilink")。
  - 3月15日　[麻六甲海峽突破戰](../Page/麻六甲海峽.md "wikilink")：於[麻六甲海峽與該地的](../Page/麻六甲海峽.md "wikilink")[Z.A.F.T.部隊交戰](../Page/ZAFT.md "wikilink")，[大天使號多處受損](../Page/LCAM-01XA_Archangel.md "wikilink")。
  - 3月23日　於[歐普近海與](../Page/O.R.B..md "wikilink")[阿斯蘭·薩拉率領的薩拉隊交戰](../Page/阿斯蘭·薩拉.md "wikilink")，陷入苦戰的[大天使號偽裝墜落至](../Page/LCAM-01XA_Archangel.md "wikilink")[歐普領海](../Page/O.R.B..md "wikilink")，進入[淤能碁呂島](../Page/淤能碁呂島.md "wikilink")。
  - 4月15日　預計北上[阿拉斯加的](../Page/阿拉斯加.md "wikilink")[大天使號駛出](../Page/LCAM-01XA_Archangel.md "wikilink")[歐普領海](../Page/O.R.B..md "wikilink")，與薩拉隊交戰。本機佔盡上風，擊破[電擊鋼彈](../Page/GAT-X207_Blitz.md "wikilink")，其駕駛員[尼可·阿瑪菲戰死](../Page/尼可·阿瑪菲.md "wikilink")。
  - 4月17日　[奥布附近的](../Page/O.R.B..md "wikilink")[馬紹爾群島](../Page/馬紹爾.md "wikilink")，薩拉隊再度追擊[大天使號](../Page/LCAM-01XA_Archangel.md "wikilink")，本機與[阿斯蘭·薩拉的](../Page/阿斯蘭·薩拉.md "wikilink")[神盾鋼彈展開激鬥](../Page/GAT-X303_Aegis.md "wikilink")，[托爾·肯尼西駕駛](../Page/托爾·肯尼西.md "wikilink")[空中霸者支援本機卻遭](../Page/FX-550_Skygrasper.md "wikilink")[神盾鋼彈擊墜](../Page/GAT-X303_Aegis.md "wikilink")。最終[阿斯蘭·薩拉在](../Page/阿斯蘭·薩拉.md "wikilink")[神盾鋼彈殘餘能量不足的情況下啟動自爆](../Page/GAT-X303_Aegis.md "wikilink")，本機在零距離狀態下大破，[煌·大和下落不明](../Page/煌·大和.md "wikilink")（後於外傳證實煌是被當時路過的異端鋼彈紅色機駕駛羅·裘爾所救，並託于馬爾奇歐導師帶往PLANT）。
  - 6月15～16日　本機被[歐普回收並修復後](../Page/O.R.B..md "wikilink")，以完美攻擊鋼彈（HD
    Remaster版本）之姿登場，改由「安迪米翁之鷹」[穆·拉·福拉卡駕駛](../Page/穆·拉·福拉卡.md "wikilink")，參與[歐普抵禦](../Page/O.R.B..md "wikilink")[大西洋聯邦侵攻的防衛戰](../Page/大西洋聯邦.md "wikilink")。本機擊破眾多[攻擊刃](../Page/GAT-01_Strike_Dagger.md "wikilink")，並與[瘟神鋼彈交手](../Page/GAT-X131_Calamity.md "wikilink")。
  - 7月12日　宙域L4的[孟德爾](../Page/孟德爾.md "wikilink")，本機與[拉烏·魯·克魯澤駕駛的](../Page/劳·鲁·克鲁泽.md "wikilink")[蓋茲在](../Page/ZGMF-600_GuAIZ.md "wikilink")[孟德爾內部交戰](../Page/孟德爾.md "wikilink")，機體受損。
  - 9月26\~27日　第二次[雅金·杜威攻防戰](../Page/雅金·杜威.md "wikilink")：本機出擊並擊墜多數[MS](../Page/機動戰士.md "wikilink")。後來與[拉烏·魯·克魯澤駕駛的](../Page/劳·鲁·克鲁泽.md "wikilink")[天帝鋼彈交戰](../Page/ZGMF-X13A_Providence.md "wikilink")，機體受損嚴重。歸艦時[大天使號遭](../Page/LCAM-01XA_Archangel.md "wikilink")[主天使號趁隙以陽電子砲正面砲擊](../Page/LCAM-02XD_Dominion.md "wikilink")，本機挺身而出抵擋砲擊，成功守護[大天使號](../Page/LCAM-01XA_Archangel.md "wikilink")。但結果是本機全毀，[穆·拉·福拉卡被判定為戰死](../Page/穆·拉·弗拉加.md "wikilink")。

## 機體武裝與型態

### Strike（攻擊鋼彈）

未裝備攻擊者背包時的輕裝型態。 \[5\] \[6\]

  - 75mm對空自動火神砲砲塔系統「」× 2
  - 對裝甲戰鬥刀「破甲者（Armor [Schneider](../Page/裁缝.md "wikilink")）」 × 2
  - 57mm高能量光束步槍
  - 對光束防盾
  - 火箭筒（由第八艦隊提供的對MS用火箭砲，一次可裝填四發，劇中僅登場一次）
  - 巨剑XM404 Grand Slam（仅出现在[GUNDAM
    EVOLVE](../Page/GUNDAM_EVOLVE.md "wikilink") 8中）

### Aile Strike（翔翼型攻擊鋼彈）

最初期開發的兵裝之一，擁有兼用散熱板的大型可變翼和四組高出力推進器的高機動戰鬥用攻擊者背包，可使機體獲得較大的機動力、推進能力與迴旋能力。基本上是宇宙用裝備，不過由於其大推力與主翼的空氣動力效果，在重力下也能做到高跳躍和短時間飛翔。另外，首次採用攻擊者背包的攻擊鋼彈機體本體並未配備光束軍刀，因此搭載了兩柄**光束軍刀**作為追加裝備。機動力的強化使得泛用性很高，[C.E.](../Page/C.E..md "wikilink")
71年的大戰後期無論大氣圈或宇宙空間，本背包是被使用最多的，亦影響了[M1
異端固定裝備的背部大型推進器的設計等](../Page/MBF-M1_Astray.md "wikilink")。[C.E.](../Page/C.E..md "wikilink")
73年，具有大氣圈內完全飛行能力的噴射型攻擊者背包（AQM/E-A4E1 Jet Striker
Pack）被開發出來後，本背包在大氣圈內的需求殆失，除了少數改良型，地上可謂再也看不到了。

背包重量：20.30噸 \[7\] \[8\]

#### 武裝

  - 75mm對空自動火神砲砲塔系統「」× 2
  - 對裝甲戰鬥刀「破甲者（Armor [Schneider](../Page/裁缝.md "wikilink")）」 × 2
  - 57mm高能量光束步槍
  - 對光束防盾
  - AQM/E-X01 翔翼型攻擊者背包（Aile Striker Pack）
      - 光束軍刀 × 2

### Sword Strike（巨劍型攻擊鋼彈）

最初期開發的兵裝之一，非常單純的近接格鬥戰用攻擊者背包。**「槍劍」15.78m對艦刀**，鐳射、實體刃複合對艦刀，刀身長度接近機體高度，可輕鬆切開戰艦與[MS的裝甲](../Page/機動戰士.md "wikilink")。即便沒有產生光束刃，刀背部分的銳利度也能作為實體刃發揮，在無法使用光束的水中是貴重的戰力。**「米達斯刀」光束迴旋鏢**安裝於左肩裝甲，內部和光束軍刀同樣系統，搭載了大容量的電容器，脫離機體獨立飛行中也能保持著光束刃飛翔，以弧形劃出獨特的軌道並切斷軌道上的標的，並回到手中。左腕裝有火箭推進式的錨「**鐵戰車**」，連結著強化高分子纜線，前端可展開爪捕捉目標，或者破壞也是可能的。纜線的捲線盤施作了對光束塗層，有著作為防盾使用的機能。

背包重量：9.30噸 \[9\] \[10\]

#### 武裝

  - 75mm對空自動火神砲砲塔系統「」× 2
  - 對裝甲戰鬥刀「破甲者（Armor [Schneider](../Page/裁缝.md "wikilink")）」 × 2
  - AQM/E-X02 巨劍型攻擊者背包（Sword Striker Pack）
      - 15.78m對艦刀「槍刀（[Schwert](../Page/劍.md "wikilink")
        [Gewehr](../Page/步枪.md "wikilink")）」
      - 光束迴旋鏢「[米達斯刀](../Page/米達斯王.md "wikilink")（[Midas](../Page/迈达斯.md "wikilink")
        [Messer](../Page/小刀.md "wikilink")）」
      - 火箭錨「鐵戰車（Panzer Eisen）」

### Launcher Strike（重砲型攻擊鋼彈）

最初期開發的兵裝之一，特化遠距離砲擊戰的攻擊者背包。擁有最強的火力，但也是背包中耗電最劇烈的，又是相對重裝備，並不適於要求機動性的局面。全長約20公尺的大型光束砲**320mm超高脈衝砲「炎神」**，通過生成、放射高壓縮狀態下的臨界等離子能量作為微秒級爆發衝擊，威力達到足以將太空殖民地破壞的程度。能量消耗率很高，連接外部電源的話可進行高速連射，單機使用則不適合長時間戰鬥。右肩著裝的複合兵裝倉安裝了大口徑的**120mm對艦火神砲**，冠上了對艦之名，威力可輕鬆擊落MS。還有一組二連裝的**350mm發射器**，可射出誘導彈、無誘導彈兩種彈頭。

背包重量：18.90噸 \[11\] \[12\]

#### 武裝

  - 75mm對空自動火神砲砲塔系統「」× 2
  - 對裝甲戰鬥刀「破甲者（Armor [Schneider](../Page/裁缝.md "wikilink")）」 × 2
  - AQM/E-X03 重砲型攻擊者背包（Launcher Striker Pack）
      - 320mm超高脈衝砲「炎神（[阿耆尼](../Page/阿耆尼.md "wikilink")、[Agni](../Page/阿耆尼.md "wikilink")）」
      - 複合兵裝倉
          - 120mm對艦火神砲
          - 350mm發射器 × 2

### Gunbarrel Strike（砲筒型攻擊鋼彈）

被歸類於鋼彈 SEED MSV設定的攻擊者背包。在SEED
MSV戰記中，原本是作為[格利馬爾迪戰線的](../Page/格利馬爾迪戰線.md "wikilink")[梅比烏斯零式部隊唯一殘存者](../Page/TS-MA2mod.00_Mobius_Zero.md "wikilink")「安迪米翁之鷹」[穆‧拉‧福拉卡少校](../Page/穆‧拉‧福拉卡.md "wikilink")（當時為上尉）的專用裝備所開發出來，但是由於他後來隨著大天使號叛離聯合而無從實現。之後，[歐亞聯邦](../Page/歐亞聯邦.md "wikilink")[陸軍](../Page/陸軍.md "wikilink")[上尉](../Page/上尉.md "wikilink")「月下狂犬」[摩根‧雪佛蘭被判定擁有高度的空間認識能力](../Page/摩根‧雪佛蘭.md "wikilink")，本背包便成為其[刃式的專用裝備](../Page/GAT-01A1_Dagger.md "wikilink")，並參加了波亞茲攻略戰和[第二次雅金‧杜威攻防戰](../Page/第二次雅金‧杜威攻防戰.md "wikilink")。

砲筒型攻擊者背包裝備於本機則首見於GBA游戏《》，由[穆‧拉‧福拉卡駕駛](../Page/穆‧拉‧福拉卡.md "wikilink")。后於模型图册\[13\]中展示了機體模型，官方设定集\[14\]亦描述了相對应的设定。

外觀差不多就是依比例縮小的[梅比烏斯零式](../Page/TS-MA2mod.00_Möbius_Zero.md "wikilink")，裝備了4個有線式砲筒的戰機。由於背包的連接插頭相異於其他攻擊者背包，無法利用霸者系支援機輸送。為此，機首有著獨自的駕駛艙，並裝備了**M58E
格林機關砲**，使得背包本體也具有戰鬥能力。著裝時，機首向後折彎，露出連接插頭。著裝後，為了減輕重量，機首就這樣直接切離。有線式砲筒均配備了**GAU-758S
磁軌砲**和兩枚**M70AMSAT 導彈**。

背包重量：11.87噸

#### 武裝

  - 75mm對空自動火神砲砲塔系統「」× 2
  - 對裝甲戰鬥刀「破甲者（Armor [Schneider](../Page/裁缝.md "wikilink")）」 × 2
  - 57mm高能量光束步枪
  - 對光束防盾
  - AQM/E-X04 砲筒型攻擊者背包（Gunbarrel Striker Pack）
      - M58E 格林机機關
      - 有线式砲筒 × 4
          - GAU-758S 磁軌砲
          - M70AMSAT 導彈 × 2

### Perfect Strike（完美攻擊鋼彈）

首見於2012年「[機動戰士GUNDAM SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink") HD
Remaster」，以[穆·拉·福拉卡座機的姿態登場](../Page/穆·拉·福拉卡.md "wikilink")。直接統合**翔翼型（Aile）**、**巨劍型（Sword）**、**重砲型（Launcher）**三種裝備的全領域型攻擊者背包，雖然有著「**多重突擊型攻擊者背包（Multiple
Assault Striker Pack）**」的正式名稱，不過本機著裝時被喚作「**完美攻擊鋼彈（Perfect Strike
Gundam）**」。為了應付膨脹的電力耗費，於翔翼型背包上連結了共計五基的外掛[電池包](../Page/電池.md "wikilink")，每次使用完即脫離丟棄。（本機出處，按照[官方設定資料](http://4.bp.blogspot.com/-9fTz6SysPdo/UDMEW4nhv0I/AAAAAAAADcI/-ELrZW9qPFo/s1600/123.jpg)是來自[SD鋼彈模型的原創三合一型態](../Page/SD鋼彈.md "wikilink")，該版本名為「超級攻擊鋼彈《Super
Strike Gundam》」[模型盒上介紹](http://www.1999.co.jp/itbig03/10035343a.jpg)。）

本背包的開發，有著對關係軍工企業利益供輸的意味在。本背包被搬入大天使號時，見到裝備的煌判斷多機能不易使用，又是輕裝狀態下的攻擊鋼彈的兩倍重導致機動性低下，始終是未使用狀態。後來歐普防衛戰時，穆駕駛本機著裝出擊，雖打出戰績卻也證明了煌的判斷是正確的，之後未再使用。然而，有不同意見指出當時推進力、電池問題已得到一定程度的解決。

在變化並不緩慢的戰場上，片面的武裝很難應對突發狀況，所謂將本背包概念全面實現的想望已然產生。此後I.W.S.P.、漆黑攻擊者背包、鳳背包、命運脈衝鋼彈、命運鋼彈等，自實戰發展出的各種各樣的背包或機體陸續登場。

背包重量：63.05噸

#### 武裝

  - 75mm對空自動火神砲砲塔系統「」× 2
  - 對裝甲戰鬥刀「破甲者（Armor [Schneider](../Page/裁缝.md "wikilink")）」 × 2
  - 57mm高能量光束步槍
  - 火箭砲
  - AQM/E-YM-1 多重突擊型攻擊者背包（Multiple Assault Striker Pack）
      - 光束軍刀 × 2
      - 15.78m對艦刀「槍刀（[Schwert](../Page/劍.md "wikilink")
        [Gewehr](../Page/步枪.md "wikilink")）」
      - 光束迴旋鏢「[米達斯刀](../Page/米達斯王.md "wikilink")（[Midas](../Page/迈达斯.md "wikilink")
        [Messer](../Page/小刀.md "wikilink")）」
      - 火箭錨「鐵戰車」（Panzer Eisen）× 1
      - 320mm
        超高脈衝砲「炎神（[阿耆尼](../Page/阿耆尼.md "wikilink")、[Agni](../Page/阿耆尼.md "wikilink")）」
      - 複合兵裝倉
          - 120mm對艦火神砲
          - 350mm發射器 × 2

### Strike + I.W.S.P.（攻擊鋼彈 + I.W.S.P.）

被歸類於鋼彈 SEED MSV設定的攻擊者背包。I.W.S.P.為**Integrated Weapons Striker
Pack（統合兵裝型攻擊者背包）**的略稱。[大西洋聯邦的PMP社以將](../Page/大西洋聯邦.md "wikilink")**Aile（翔翼型）**的機動性、**Sword（巨劍型）**的格鬥能力與**Launcher（重砲型）**的火力統合在單一攻擊者背包為目的所設計。然而，因為構造複雜化帶來了成本攀升，背包本體的淨重亦造成了機體姿勢控制上的惡化。為了兵裝與控制用電裝系的重裝備化所增加的電力消耗，[PS裝甲有效時間大幅縮短的問題無法解決](../Page/PS裝甲.md "wikilink")。因此，技術的界限使得[C.E.](../Page/C.E..md "wikilink")
71年初完成的本機僅採用了翔翼型、巨劍型和重砲型三種背包。後來，設計資料外流至歐普聯合首長國並被仿造出來（型號P202QX），預定由通過Power
Extender延長活動時間的[嫣紅攻擊鋼彈搭載](../Page/MBF-02_Strike_Rouge.md "wikilink")，並進行了數次實裝測試。但因其駕駛員[卡嘉里‧由拉‧阿斯哈掌握不了複雜化的火器管制系統](../Page/卡嘉里‧由拉‧阿斯哈.md "wikilink")，最終仍未投入實戰。

聯合軍採用I.W.S.P.則要推遲至[C.E.](../Page/C.E..md "wikilink")
73年，本機的再生機才做了運用試驗，並成為[幻痛的主力機](../Page/幻痛.md "wikilink")[屠殺刃的標準裝備之一](../Page/GAT-01A2R_Slaughter_Dagger.md "wikilink")。大戰終了後，地球聯合軍為了開發王牌專用的個人化定製機體，史溫‧卡爾‧巴揚的攻擊鋼彈（再生機）裝備了本背包投入實戰。當時的戰鬥資料後來被利用於噴射型攻擊者背包（AQM/E-A4E1
Jet Striker）與雙角連裝無反動砲（AQM/E-M11
Doppelhorn連装無反動砲）的同時開發上，新一代統合兵裝的概念以聯合的漆黑攻擊者背包（AQM/E-X09S
Noir Striker）與歐普的鳳背包（EW454F
Ootori）等各種各樣的形式誕生。另外，後來的東亞戰線中，盧卡斯‧奧德奈的攻擊鋼彈E（GAT-X105E
Strike E）亦裝備本背包，確認了地球聯合正規軍有少量的量產。

I.W.S.P.裝備於本機則首見於PS3遊戲《機動戦士ガンダム EXTREME
VS.》，由[煌‧大和駕駛](../Page/煌‧大和.md "wikilink")。

背包搭載了**115mm電磁軌砲**，越過肩部射擊目標，通過裝配於同軸上的高精度照準感知器，超長距離射擊時得命中準確度得以提升。並列於肩部的**105mm單裝砲**，同為電磁軌砲，用於中近距離的射擊。接近戰方面有兩柄**9.1m對艦刀**，對MS格鬥戰以及巨大船艦均十分有效的實體劍，駕駛員需要一定技術方能上手。與防盾一體化的**30mm六槍管[格林機關砲](../Page/格林機關砲.md "wikilink")**，連射火力兇猛，但機體左側重心過大不易操控，後來改良型的漆黑攻擊者背包將其完全移除。防盾的表面裝配了一支**光束迴旋鏢**，亦施作了對光束塗層。

#### 武裝

  - 75mm 對空自動火神砲砲塔系統「」× 2
  - 對裝甲戰鬥刀「破甲者（Armor [Schneider](../Page/裁缝.md "wikilink")）」 × 2
  - AQM/E-M1 I.W.S.P.
      - 115mm電磁軌砲 × 2
      - 105mm單裝砲 × 2
      - 9.1m對艦刀 × 2
      - 複合盾
          - 30mm六槍管[格林機關砲](../Page/格林機關砲.md "wikilink")
          - 光束迴旋鏢

### Lightning Strike（闪电强袭高达）

仅在《机动战士GUNDAM SEED MSV》中出现的设定。 以强袭高达的强化电池包，能量补给机和超远距离狙击这三个目的而开发出的背包。
背包搭载的70-31式电磁加农炮可发射71式强化型尖头穿甲弹。

#### 武裝

  - 75mm 對空自動火神砲砲塔系統「」× 2
  - 對裝甲戰鬥刀「破甲者（Armor [Schneider](../Page/裁缝.md "wikilink")）」 × 2
  - P204QX
      - 70-31式电磁加农炮

## 再生機

攻擊鋼彈的再生機，首例是大戰後期歐普的[嫣紅攻擊鋼彈](../Page/MBF-02_Strike_Rouge.md "wikilink")。攻擊鋼彈遭受神盾鋼彈的自爆攻擊而大破，曙光社於修復攻擊鋼彈之際複製了預備部件，後來在草薙號艦內組裝完成。搭載了新開發的大容量電池包Power
Extender，活動時間大幅延長，裝甲啟動色以赤色為主體色，其餘和攻擊鋼彈原型機無異。

大戰終了後，地球聯合軍第81獨立機動群「幻痛」得到以阿克泰昂工業社為中心的數個企業的技術協力，推進了王牌駕駛員專用定製MS的開發計畫，通稱「**阿克泰昂計畫**」。在此基礎上，攻擊鋼彈被二次生產出來，專任駕駛員為華金中校指揮下的特戰MS小隊所屬駕駛員[史溫‧卡爾‧巴揚](../Page/史溫‧卡爾‧巴揚.md "wikilink")。

同[嫣紅攻擊鋼彈一樣](../Page/MBF-02_Strike_Rouge.md "wikilink")，再生產的攻擊鋼彈設計上與原型機完全相同，主電源亦採用了Power
Extender。另PS裝甲的電壓設定也有部分變更，啟動色相比原型機來得暗色化是一大特徵。為了對應各種戰局，著裝的攻擊者背包選擇了全領域型的I.W.S.P.。

I.W.S.P.的確是強力的裝備，但是進一步累積、解析數據也暴露出各式各樣的缺點。特別是機體上半身和裝備了複合盾的左腕部，重量平衡顯著惡化，機動時產生多餘動量是個深刻的問題。之後，在克服這些缺點的同時，各機能得到發展、昇華的新型攻擊者背包「漆黑攻擊者（AQM/E-X09S
Noir Striker Pack）」被開發出來。加以機體也並行修改為攻擊鋼彈E，本機後來作為「漆黑攻擊鋼彈」重生。

## 攻擊鋼彈 E

於『機動戰士鋼彈 SEED C.E.73 STARGAZER』、『機動戰士鋼彈 SEED C.E.73 Δ ASTRAY』、『機動戰士鋼彈
SEED C.E.73 STARGAZER PHANTOM PAIN REPORT』與『機動戰士鋼彈 SEED FRAME
ASTRAYS』中登場。

以再生機積累的實戰數據為基礎，發展強化後的攻擊鋼彈。機體名與型式番號末尾的「E」意為「**強化型（Enhanced）**」，正如其名做了多處的強化修改。頭部冷卻系統的再配置，伴隨著面罩中央的管縫變更，兩肩部追加子推進器強化了機動性。由於各部材的能源耗費效率改善，活動時間亦得到延長。此外，作業系統、接口與無線通信系統也反覆改良，將特殊部隊出擊前的繁瑣設定合併，並擁有高操縱性。PS裝甲啟動色和修改前差不多同配色，但是由於搭載Power
Extender帶來的副效果，各種攻擊者背包著裝時的對應色發生變化，成為了VPS裝甲。

外型上因為兩肩部子推進器的追加，各部裝甲形狀做了一定程度的變更，輪廓剪影看起來和原型機沒有甚麼差異。可裝備各種攻擊者背包，「幻痛」和開發企業從一開始就著眼於本機的高泛用性。

武裝方面亦有所提升。**M2M5
「死之恐怖」12.5mm自動近接防御火器**為[刃式L以後聯合系MS的標準裝備](../Page/GAT-02L2_Dagger_L.md "wikilink")，頭部兩側的對空防禦機關砲。口徑只有「豪豬陣」的六分之一，裝彈數大增，但通過改良彈芯與火藥，將威力的下降抑制在二分之一。**M8F-SB1
短光束步槍**取代了對裝甲戰鬥刀「破甲者」作為基本武裝，配備於兩腰，強調近戰而重視連射性能。但由於是手槍尺寸，光束集束率下降，有效射程降低15%。兩掌、兩腳尖和腳跟裡面內藏**線錨**。此裝備是在機體強化修改之際，依史恩‧卡爾‧巴揚本人的希望加入實裝。由特殊的高分子物質構成的纜線具有可懸吊MS等級重量的強度，使用上有著自機的固定與牽引，敵機的捕獲和直接打擊等多用途。主武裝跟決鬥鋼彈同型，配備**175mm榴彈擊管**的**57mm高能量光束步槍**，聯合製光束兵器最古早的類型之一，不過其高信賴性仍為許多駕駛員所愛用。

### 機體諸元

  - 型號：GAT-X105E
  - 名稱：Strike E
  - 全高：17.72米
  - 重量：64.8噸
  - 裝甲材質：VPS裝甲
  - 駕駛員：史溫‧卡爾‧巴揚、盧卡斯‧奧德奈、艾米利歐·布羅德瑞克、達納‧史尼普

#### 特殊裝備或機能

  - 攻擊者背包系統
  - Power Extender

### 機體武裝與型態

  - M2M5 「死之恐怖」12.5mm自動近接防御火器×2
  - EQS1358 線錨發射器×6
  - M8F-SB1 短光束步槍×2
  - 57mm高能量光束步槍
      - 175mm榴彈擊管
  - 各種攻擊者背包

## 疾风强袭高达

于《机动战士GUNDAM SEED VS ASTRAY》中登场。Librarian（图书管理员）独自修改的强袭高达。

### 机体参数

  - 型号：LG-GAT-X105
  - 名称：Gale Strike
  - 全高：18.01米
  - 重量：65.11吨
  - 装甲材质：PS装甲
  - 驾驶员：ND-HE

## 參考

<div class="references-small">

<references />

</div>

## 参见

  - [MBF-02 Strike Rouge](../Page/MBF-02_Strike_Rouge.md "wikilink")
  - [GAT-X105E Strike
    Noir](../Page/GAT-X105E_Strike_E#GAT-X105E+AQM/E-X09S_Strike_Noir.md "wikilink")

[Category:C.E.兵器](../Category/C.E.兵器.md "wikilink")

1.  GUNDAM戰記超百科
2.  機動戰士-{鋼彈}-SEED{{〈}}1{{〉}}交會而過的羽翼 ISBN 986-7427-27-0
3.  機動戰士-{鋼彈}-SEED{{〈}}1{{〉}}交會而過的羽翼 ISBN 986-7427-27-0
4.  機動戰士-{鋼彈}-SEED{{〈}}1{{〉}}交會而過的羽翼 ISBN 986-7427-27-0
5.  機動戰士-{鋼彈}-SEED{{〈}}1{{〉}}交會而過的羽翼 ISBN 986-7427-27-0
6.  機動戰士-{鋼彈}-SEED{{〈}}2{{〉}}沙漠之虎 ISBN 986-7427-53-X
7.  機動戰士-{鋼彈}-SEED{{〈}}1{{〉}}交會而過的羽翼 ISBN 986-7427-27-0
8.  機動戰士-{鋼彈}-SEED{{〈}}2{{〉}}沙漠之虎 ISBN 986-7427-53-X
9.  機動戰士-{鋼彈}-SEED{{〈}}1{{〉}}交會而過的羽翼 ISBN 986-7427-27-0
10. 機動戰士-{鋼彈}-SEED{{〈}}2{{〉}}沙漠之虎 ISBN 986-7427-53-X
11. 機動戰士-{鋼彈}-SEED{{〈}}1{{〉}}交會而過的羽翼 ISBN 986-7427-27-0
12. 機動戰士-{鋼彈}-SEED{{〈}}2{{〉}}沙漠之虎 ISBN 986-7427-53-X
13. 《》第113页
14. 《Mobile Suit Gundam SEED MS Encyclopedia》第102页