**GAUSSIAN**是一个[量子化学软件包](../Page/量子化学.md "wikilink")，它是目前应用最广泛的计算化学软件之一，其代码最初由理论化学家、1998年[诺贝尔化学奖得主](../Page/诺贝尔化学奖.md "wikilink")[约翰·波普](../Page/約翰·波普_\(化學家\).md "wikilink")[爵士编写](../Page/爵士.md "wikilink")，其名称来自于波普在软件中所使用的[高斯型](../Page/高斯型函数.md "wikilink")[基组](../Page/量子化学中的基组.md "wikilink")。使用高斯型基组是波普为简化计算过程缩短计算时间所引入的一项重要近似。Gaussian软件的出现降低了量子化学计算的门槛，使得[从头计算方法可以广泛使用](../Page/从头计算方法.md "wikilink")，从而极大地推动了其在方法学上的进展。最初，Gaussian的著作权属于[约翰·波普供职的](../Page/約翰·波普_\(化學家\).md "wikilink")[卡内基梅隆大学](../Page/卡内基梅隆大学.md "wikilink")，目前其版权持有者是[Gaussian,
Inc.公司](../Page/Gaussian,_Inc..md "wikilink")

## 禁止使用

许多优秀科学家，甚至包括[约翰·波普和其他许多参与过Gaussian开发的科学家](../Page/約翰·波普_\(化學家\).md "wikilink")，均被[Gaussian,
Inc.公司列入黑名单](../Page/Gaussian,_Inc..md "wikilink")，禁止他们接触和使用Gaussian软件。Gaussian,
Inc.认为黑名单上的学者正在为其竞争对手编写软件，为了维护公司的利益，必须防止这些科学家接触到Gaussian的代码。此外，Gaussian的开发人员指出，黑名单上的学者将其开发的算法公布，使之进入[公共领域](../Page/公共领域.md "wikilink")，可以被其他人自由使用。

## 外部链接

  - [Gaussian公司主页](http://www.gaussian.com/)
  - [banned by gaussian，被禁用者主页](http://www.bannedbygaussian.org)

[Category:计算化学软件](../Category/计算化学软件.md "wikilink")