**GENJI -神威奏亂-**是由Game
Republic開發并由[索尼电脑娱乐在](../Page/索尼电脑娱乐.md "wikilink")[PlayStation
3平台上发行的动作遊戲](../Page/PlayStation_3.md "wikilink")。本作是PlayStation
3的首发游戏之一。

## 故事

GENJI的3年後，[源義經擊敗了平氏一族的平清盛](../Page/源義經.md "wikilink")，將平氏一族逐出平安京。但源氏與平氏依然持續戰爭，更傳出平氏軍隊中出現了非人類的怪物，在平氏的新當家領導下，利用與「天鋼」相對的「魔瘴鋼」來對抗源氏。

## 系統

神威系統是以玩家看準時間按下按鍵，使出連續快速攻擊特殊空間中的敵人的技能。

## 角色

  - [源九郎義經](../Page/源義經.md "wikilink") -
    [浪川大輔](../Page/浪川大輔.md "wikilink")
  - [武藏坊辨慶](../Page/武藏坊辨慶.md "wikilink") -
    [大塚明夫](../Page/大塚明夫.md "wikilink")　
  - [静御前](../Page/静御前.md "wikilink") -
    [戶田惠梨香](../Page/戶田惠梨香.md "wikilink")
  - 九妖 - [長澤美樹](../Page/長澤美樹.md "wikilink")
  - 武尊 - [諏訪部順一](../Page/諏訪部順一.md "wikilink")
  - [源頼朝](../Page/源頼朝.md "wikilink")
  - [平敦盛](../Page/平敦盛.md "wikilink")
  - [平知盛](../Page/平知盛.md "wikilink")
  - [平教經](../Page/平教經.md "wikilink")
  - 旁白 - [津田英三](../Page/津田英三.md "wikilink")

## 外部連結

  - [GENJI-神威奏亂-官方網站](http://www.jp.playstation.com/scej/title/genji_ps3/)

[Category:2006年电子游戏](../Category/2006年电子游戏.md "wikilink")
[Category:動作冒險遊戲](../Category/動作冒險遊戲.md "wikilink")
[Category:砍殺遊戲](../Category/砍殺遊戲.md "wikilink")
[Category:游戏共和国游戏](../Category/游戏共和国游戏.md "wikilink")
[Category:索尼互動娛樂遊戲](../Category/索尼互動娛樂遊戲.md "wikilink")
[Category:PlayStation 3游戏](../Category/PlayStation_3游戏.md "wikilink")
[Category:PlayStation
3独占游戏](../Category/PlayStation_3独占游戏.md "wikilink")
[Category:后传电子游戏](../Category/后传电子游戏.md "wikilink")
[Category:日本背景电子游戏](../Category/日本背景电子游戏.md "wikilink")
[Category:日本開發電子遊戲](../Category/日本開發電子遊戲.md "wikilink")
[Category:官方繁体中文化游戏](../Category/官方繁体中文化游戏.md "wikilink")