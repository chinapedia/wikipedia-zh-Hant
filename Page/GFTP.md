[GFTP.jpg](https://zh.wikipedia.org/wiki/File:GFTP.jpg "fig:GFTP.jpg")
**gFTP**
是一個在\*NIX上使用的（[unix](../Page/unix.md "wikilink")，[linux](../Page/linux.md "wikilink")，[BSD](../Page/BSD.md "wikilink")，etc.）[自由的多線程](../Page/GNU.md "wikilink")[FTP](../Page/FTP.md "wikilink")
[客戶端](../Page/FTP_客戶端.md "wikilink")，其建基於[X11R6](../Page/X11.md "wikilink")
或以後的版本。gFTP在[GPL下發布](../Page/GNU_General_Public_License.md "wikilink")，並且已被翻譯成41種不同語言。

## 特色

  - 使用 C 語言寫成 且有文字介面 和 GTK+ 1.2/2.0 介面
  - 支援 FTP， HTTP 和 SSH 協定
  - 支援 FXP 檔案傳輸（在兩個 ftp server 間傳輸檔案）
  - 允許多檔案傳輸下載佇列
  - 支援下載整個目錄和檔案
  - 有書籤選單讓使用者可以快速選取遠端
  - 支援斷點續傳
  - 支援遠端目錄快取
  - 支援拖曳檔案，即Drag and Drop
  - 支援FTP 和 HTTP 代理伺服器
  - 允許 passive 或是 非 passive 檔案傳輸
  - 支援 Unix， EPLF， Novell， MacOS? ，和 NT(DOS) 格式目錄列表
  - 全面的圖形化設定

## 參考條目

  - [FTP客戶端列表](../Page/FTP客戶端列表.md "wikilink")
  - [FTP客戶端比較](../Page/FTP客戶端比較.md "wikilink")

## 外部連結

  - [gFTP官方主頁](http://gftp.seul.org)

[Category:开放源代码](../Category/开放源代码.md "wikilink")