**GNAT Programming Studio**或**GPS**（過去稱為**GNAT Programming
System**）是一套[自由授權](../Page/自由軟體.md "wikilink")、支援多種程式語言的[整合開發環境](../Page/集成开发环境.md "wikilink")，並可在[Linux](../Page/Linux.md "wikilink")、[Windows及](../Page/MS_Windows.md "wikilink")[Solaris](../Page/Solaris.md "wikilink")（[SPARC機器的版本](../Page/SPARC.md "wikilink")）等作業系統上使用。GPS的[圖像化使用者介面是呼用](../Page/GUI.md "wikilink")[GTK+函式庫](../Page/GTK+.md "wikilink")
來實現，此套整合開發環境是由[AdaCore公司以](../Page/AdaCore公司.md "wikilink")[MGPL授權方式發表](../Page/GNAT_Modified_General_Public_License.md "wikilink")。

**GPS**並不包含編譯器，取而代之的是使用[GNU Compiler
Collection來產生可執行的程式碼](../Page/GCC.md "wikilink")。

新近的公開發表版：3.1.0，支援多種程式語言，包括[Ada](../Page/Ada.md "wikilink")、[C](../Page/C語言.md "wikilink")、[C++](../Page/C++.md "wikilink")、[Fortran
90](../Page/Fortran#Fortran_90.md "wikilink")、[Pascal](../Page/Pascal.md "wikilink")、[Perl](../Page/Perl.md "wikilink")、[Python及](../Page/Python.md "wikilink")[Tcl](../Page/Tcl.md "wikilink")。此外也支援如下的檔案型態及格式[Autoconf](../Page/Autoconf.md "wikilink")、[Awk](../Page/Awk.md "wikilink")、[Changelog](../Page/Changelog.md "wikilink")、[M4](../Page/M4.md "wikilink")、[Makefile](../Page/Makefile.md "wikilink")、[GNAT
Project file](../Page/GNAT.md "wikilink")、[Shell
script](../Page/Shell_script.md "wikilink")、[Texinfo](../Page/Texinfo.md "wikilink")。

更新的3.1.3版也已經開始供應，可以讓用戶透過[CVS進行一般性的錯誤修正](../Page/CVS.md "wikilink")。

現在4.0.0版的開發也已經告一段落，新版（GPS
4）中最令人感到興趣的新功能是讓平台具有「遠端編輯」、「遠端除錯」能力，這在過去的GPS並沒有原生性提供。

## 關連項目

  - [整合開發環境](../Page/集成开发环境.md "wikilink")
      - [GNAVI](../Page/GNAVI.md "wikilink")
      - [KDevelop](../Page/KDevelop.md "wikilink")
  - [Ada](../Page/Ada.md "wikilink")
  - [C](../Page/C語言.md "wikilink")
  - [C++](../Page/C++.md "wikilink")
  - [Python](../Page/Python.md "wikilink")

## 外部連結

  - [GPS（GNAT Programming
    System）的首頁](http://libre.adacore.com/tools/gps/)

[Category:Ada程式語言](../Category/Ada程式語言.md "wikilink")
[Category:C程式語言](../Category/C程式語言.md "wikilink")
[Category:GNU計劃軟體](../Category/GNU計劃軟體.md "wikilink")
[Category:自由的整合開發環境](../Category/自由的整合開發環境.md "wikilink")
[Category:Python程式語言](../Category/Python程式語言.md "wikilink")
[Category:集成开发环境](../Category/集成开发环境.md "wikilink")