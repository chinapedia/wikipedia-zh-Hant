**GNOME**（\[1\]或\[2\]）是一個完全由[自由软件組成的](../Page/自由软件.md "wikilink")[桌面环境](../Page/桌面环境.md "wikilink")。它的目標[作業系統是](../Page/作業系統.md "wikilink")[Linux](../Page/Linux.md "wikilink")，但是大部分的[BSD系統亦支持GNOME](../Page/BSD.md "wikilink")。

GNOME是由志願貢獻者和受僱貢獻者組成的[GNOME計劃開發](../Page/GNOME計劃.md "wikilink")，其最大的公司貢獻者為[紅帽公司](../Page/紅帽公司.md "wikilink")\[3\]\[4\]。它是一個為開發[軟件框架](../Page/軟件框架.md "wikilink")、基於這些框架來開發用戶端軟件及協調軟件[翻譯和開發](../Page/翻譯.md "wikilink")[無障礙軟件的專案](../Page/無障礙.md "wikilink")。

GNOME最初是GNU网络对象模型环境（）的缩写，但是已经被废弃了。\[5\]是[GNU計劃的一部分](../Page/GNU計劃.md "wikilink")，并且是由志愿者开发的。

## 歷史

### GNOME 1

[GNOME_1.0_(1999,_03)_with_GNOME_Panel_1_and_File_Manager.png](https://zh.wikipedia.org/wiki/File:GNOME_1.0_\(1999,_03\)_with_GNOME_Panel_1_and_File_Manager.png "fig:GNOME_1.0_(1999,_03)_with_GNOME_Panel_1_and_File_Manager.png")
1996年[KDE發佈](../Page/KDE.md "wikilink")，但KDE所依賴的[Qt当时并未使用](../Page/Qt.md "wikilink")[GPL授權](../Page/GPL.md "wikilink")。出于这种考虑，两个项目在1997年8月发起：一个是作为Qt库替代品的「Harmony」，另外一个就是建立一个基于非Qt库的桌面系统，即GNOME项目。GNOME的发起者為[米格爾·德伊卡薩和](../Page/米格爾·德伊卡薩.md "wikilink")[費德里科·梅納](../Page/費德里科·梅納.md "wikilink")\[6\]。

[GIMP Toolkit](../Page/GTK.md "wikilink")（GTK+）被选中做为Qt
toolkit的替代，担当GNOME桌面的基础。GTK+使用[LGPL](../Page/GNU宽通用公共许可证.md "wikilink")，允许链接到此库的软件（例如GNOME的应用程序）使用任意的许可协议。GNOME计划的应用程序通常使用GPL许可证\[7\]。

在GNOME变得普及后，1999年Qt加入GPL授權\[8\]\[9\]。Troll Tech在[GNU
GPL和](../Page/GNU_GPL.md "wikilink")双重许可证下发布了[Unix版的Qt库](../Page/Unix.md "wikilink")。Qt加入GPL授權後，在2000年年底Harmony項目停止了開發，而KDE不再依賴非GPL的軟體。2009年3月，Qt
4.5發布，加入了[LGPL授權作為第三選擇](../Page/LGPL.md "wikilink")。

「GNOME」這個名稱最初是「」的縮寫，以反映最初為了開發類似[微軟](../Page/微軟.md "wikilink")[物件連結與嵌入的框架](../Page/物件連結與嵌入.md "wikilink")\[10\]。但這個縮寫最後被放棄，因為它不再反映GNOME專案的遠景\[11\]。

加州初创企业於1999至2001年開發[Nautilus檔案瀏覽器](../Page/Nautilus檔案瀏覽器.md "wikilink")。[米格爾·德伊卡薩和](../Page/米格爾·德伊卡薩.md "wikilink")於1999年創立後來成為[Ximian的Helix](../Page/Ximian.md "wikilink")
Code公司。該公司開發了GNOME的基礎設施和軟件，2003年被[Novell收購](../Page/Novell.md "wikilink")。

### GNOME 2

[Gnome-screenshot-full.jpg](https://zh.wikipedia.org/wiki/File:Gnome-screenshot-full.jpg "fig:Gnome-screenshot-full.jpg")
GNOME 2與傳統桌面界面十分相似，擁有一個用戶可以與不同例如窗口、圖示、檔案等虛擬物件互動的桌面環境。GNOME
2使用[Metacity為它的預設](../Page/Metacity.md "wikilink")[窗口管理器](../Page/窗口管理器.md "wikilink")。GNOME
2的窗口、程式和檔案管理和一般的桌面操作系统十分相似。在預設的設定中，桌面有一個啓動選單，可以用以開啓已安裝的程式及檔案；己存在的窗口在下方的工作列列出；而在右上角則有一個通知區以顯示在背景運行的程式。不過，這些功能可以隨用戶喜好而更改位置、取代或甚至移除。

### GNOME 3

在GNOME 3之前，GNOME是根據傳統的[桌面比擬而設計](../Page/桌面比擬.md "wikilink")，但在GNOME
3便被[GNOME
Shell所取代](../Page/GNOME_Shell.md "wikilink")，所有轉換窗口及虛擬桌面都在「活動」畫面中進行。此外，因為[Mutter取代了Metacity成為預設的窗口管理器](../Page/Mutter.md "wikilink")，最小化及放大按鈕不再預設在名稱列中。取代了成為預設主題。很多都重新設計以提供更連貫的用戶體驗。

這些重大的改變最初引來了廣泛的批評。[MATE桌面環境專案由GNOME](../Page/MATE.md "wikilink")
2的源始碼衍生，目標為保留GNOME
2的傳統界面，同時支援最新的Linux技術，例如[GTK+](../Page/GTK+.md "wikilink")
3。[Linux Mint團隊則以開發](../Page/Linux_Mint.md "wikilink")「Mint GNOME Shell
Extensions」一系列於GNOME 3上執行之外掛程式解決此問題，這些外掛程式使GNOME 3的界面變回傳統比擬界面。最後，Linux
Mint決定從GNOME
3的源代码衍生另外一個桌面環境「[Cinnamon](../Page/Cinnamon.md "wikilink")」。

，對GNOME
3的整體評價已大致轉為正面\[12\]。Linux發行版[Debian於GNOME](../Page/Debian.md "wikilink")
3發佈時把[XFCE改成預設的桌面環境](../Page/XFCE.md "wikilink")，但在Debian 8己改回預設使用GNOME
3\[13\]\[14\]。Linux創始者[林納斯·托瓦茲於](../Page/林納斯·托瓦茲.md "wikilink")2013年已改回使用GNOME
3\[15\]。

### 版本

组成 GNOME 计划的每一部分都有自己的版本号和发布规划，通过各模块的维护者之间的定期协调（六个月），建立一个完整的 GNOME
发布版本。下边的发布版本列表分类属于稳定版。提供给测试和开发者的不稳定版本并未列入。

| 版本歷史    |
| ------- |
| 版本      |
|         |
| 1.0     |
| 1.0.53  |
| 1.2     |
| 1.4     |
| GNOME 2 |
| 2.0     |
| 2.2     |
| 2.4     |
| 2.6     |
| 2.8     |
| 2.10    |
| 2.12    |
| 2.14    |
| 2.16    |
| 2.22    |
| 2.24    |
| 2.26    |
| 2.28    |
| 2.30    |
| 2.32    |
| GNOME 3 |
| 3.0     |
| 3.2     |
| 3.4     |
| 3.6     |
| 3.10    |
| 3.12    |
| 3.14    |
| 3.16    |
| 3.18    |
| 3.20    |
| 3.22    |
| 3.24    |
| 3.26    |
| 3.28    |

## 設計

GNOME的目標是要簡單易用\[16\]。

### GNOME Shell

[GNOME
Shell是GNOME桌面環境的預設](../Page/GNOME_Shell.md "wikilink")[使用者介面](../Page/使用者介面.md "wikilink")。它的上方有一條面版，裏面有（由左至右）「活動」按鈕、正使用程式的選單、時鐘及一個系統選單\[17\]\[18\]。程式選單顯示當前使用程式的名稱及提供例如程式設定、關閉程式等的選項。狀態列有代表電腦不同狀態的圖示、往系統設定的捷徑以及登出、轉換用戶、以及關機的選項。

按下「活動」按鈕、把鼠標移動至左上角或按下[超級鍵會進入](../Page/超級鍵.md "wikilink")「活動」畫面\[19\]。「活動」畫面讓用戶縱覽現時在執行的程式，以及讓用戶轉換窗口、桌面，和執行程式。左邊的Dash面版裏面有最愛程式的捷徑、所有正在執行程式的圖示及往所有已安裝程式列表的按鈕\[20\]。在上方出現一個搜尋框及右邊有一個列出所有桌面的桌面列。通知在按下上方中央的時鐘後的列表內\[21\]。

{{-}}

### GNOME Classic

[GNOME_3_classic_mode.png](https://zh.wikipedia.org/wiki/File:GNOME_3_classic_mode.png "fig:GNOME_3_classic_mode.png")
從GNOME 3.8起，GNOME提供一個經典模式，提供一個較傳統的介面（類似GNOME 2）\[22\]。

### 相容性

GNOME運行在[X11之上](../Page/X11.md "wikilink")，在GNOME
3.10後亦可在[Wayland下執行](../Page/Wayland.md "wikilink")\[23\]。在大部分[Linux發行版上的GNOME都是預設桌面環境或可安裝的](../Page/Linux發行版.md "wikilink")，而在大部分[BSD系統上為](../Page/BSD.md "wikilink")[Ports](../Page/Ports.md "wikilink")。

2011年5月建議把[systemd列為GNOME運行需要的軟件](../Page/systemd.md "wikilink")\[24\]。因為systemd是Linux獨有，這個提議亦引起了未來停止支援其他平台的討論。由GNOME
3.2起，只支援使用systemd的系統\[25\]。2012年11月GNOME發佈小組結論，指非基本的功能可以依賴systemd\[26\]。

#### Freedesktop.org與GNOME

[Freedesktop.org是一项帮助在不同的X](../Page/Freedesktop.org.md "wikilink")
Window桌面——例如GNOME，KDE或者[XFCE](../Page/XFCE.md "wikilink")——之间实现互操作和共享技术的计划。尽管并非正式标准化组织，Freedesktop.org定义了X桌面的基本特征，包括程序之间的拖放、窗口管理器规范、菜单布局、最近文件列表、程序之间复制粘贴和共享的[MIME类型数据库等等](../Page/MIME.md "wikilink")。遵从Freedesktop.org规范使得GNOME应用程序能够集成到其他桌面（反之亦然），并起到鼓励桌面环境之间的竞争和合作的作用。

### 人機界面指南

自GNOME
2以起，可用性是GNOME的主要焦點。所以，GNOME便開始發展。所有GNOME程式都有連貫的[圖形使用者介面](../Page/圖形使用者介面.md "wikilink")，但是不止於使用同一套[控制項](../Page/控制項.md "wikilink")（）。因為GNOME介面的設計是由在指南中的概念所引導，而指南自身則發展自\[27\]。跟從人機界面指南，開發者可以創造優質、連貫和可用性高的介面程式，因為指南裏由介面設計至介面佈局亦有規定。

在GNOME
2的重新編程中，很多對大部分使用者而言用途不大的選項都被移除。在他於2002年發佈的文章「自由軟件的使用者界面」（）中總結了關於GNOME可用性的工作，強調所有偏好設定都有它的代價。使軟件功能在預設時操作暢順，比在設定中加入選項才令軟件操作和預期一樣更好：

### 無障礙

GNOME旨在使並保持桌面環境對[身心障礙者在](../Page/身心障礙.md "wikilink")[物理上和](../Page/人因工程學.md "wikilink")上都符合[人體工學](../Page/人體工學.md "wikilink")。GNOME的人機界面指南嘗試把此因素包個在內，但是某些個別問題使由須由特別的軟件解決。

GNOME透過（）[應用程式介面以解決](../Page/應用程式介面.md "wikilink")[計算機輔助功能上的問題](../Page/計算機輔助功能.md "wikilink")，它容許特別的[輸入法](../Page/輸入法.md "wikilink")、[語音合成及](../Page/語音合成.md "wikilink")[語音識別以改善用戶體驗](../Page/語音識別.md "wikilink")。某些工具在ATK內透過（）註冊，以在整個桌面環境中都可以使用。幾個輔助技術軟件，例如[螢幕閱讀器和](../Page/螢幕閱讀器.md "wikilink")輸入法是針對在GNOME上使用而開發的。

## 軟件

### 核心程式

使用[GTK+編寫或基於](../Page/GTK+.md "wikilink")[Clutter編寫的程式多不勝數](../Page/Clutter.md "wikilink")，由不同開發者開發\[28\]。自GNOME
3以來，[GNOME計劃開發了一系列的程式](../Page/GNOME計劃.md "wikilink")，為。所有GNOME核心程式都是根據人機界面指南而設計的\[29\]。

### 遊戲

GNOME遊戲有與GNOME核心程式外觀，以及與GNOME[軟件版本週期同時發佈](../Page/軟件版本週期.md "wikilink")。它們全部都已根據人機界面指南重寫。

### 開發工具

[Anjuta](../Page/Anjuta.md "wikilink")[整合開發環境](../Page/整合開發環境.md "wikilink")，[Glade使用者介面設計工具及](../Page/Glade.md "wikilink")[應用程式介面閱覽器是為了促進開發與GNOME連貫的軟件而開發的](../Page/應用程式介面.md "wikilink")。[Accerciser計算機輔助閱覽器以及其他](../Page/Accerciser.md "wikilink")[調試工具](../Page/調試工具.md "wikilink")，包括[Nemiver](../Page/Nemiver.md "wikilink")、[GtkInspector和](../Page/GtkInspector.md "wikilink")[Alleyoop亦是為了促進開發GNOME軟件而開發](../Page/Alleyoop.md "wikilink")\[30\]\[31\]。

與第三方軟件整合，例如與[NoFlo整合亦有提供](../Page/NoFlo.md "wikilink")\[32\]。

## 组织

和大多数[自由软件类似](../Page/自由软件.md "wikilink")，GNOME组织也很松散，其关于开发的讨论散布于众多向任何人开放的邮件列表。为了处理管理工作、施加影响以及与同对开发GNOME软件有兴趣的公司联系，2000年8月成立了[GNOME基金会](../Page/GNOME基金会.md "wikilink")。基金会并不直接参與技术决策，而是协调发布和决定哪些对象应该成为GNOME的组成部分。基金会网站将其成员资格定义为：

  -
    “按照GNOME基金会章程，任何对GNOME有贡献者都可能是合格的成员。尽管很难精确定义，贡献者一般必须对GNOME计划有不小帮助。其贡献形式包括代码、文档、翻译、计划范围的资源维护或者其它对GNOME计划有意义的重要活动
    。”

基金会成员每年11月选举董事会，其候选人必须也是贡献者。

## 参见

  - [桌面环境](../Page/桌面环境.md "wikilink")
  - [Ubuntu GNOME](../Page/Ubuntu_GNOME.md "wikilink")
  - [KDE](../Page/KDE.md "wikilink")

## 参考文献

## 外部链接

  - 官方网站

<!-- end list -->

  -
  - [GNOME Wiki](https://live.gnome.org/)

  - \[//www.gnome.org/foundation/ GNOME基金会\]

<!-- end list -->

  - 第三方站点

<!-- end list -->

  - [GnomeFiles](http://www.gnomefiles.com/)——GNOME/GTK+软件库
  - [GNOME新闻](https://web.archive.org/web/20020426042320/http://www.gnomedesktop.org/)
  - [GNOME月报](https://web.archive.org/web/20051001035040/http://gnomejournal.org/)——GNOME桌面的在线杂志
  - [GNOME计划的故事](https://web.archive.org/web/20010224061347/http://primates.ximian.com/~miguel/gnome-history.html)——[Miguel
    de Icaza编写](../Page/Miguel_de_Icaza.md "wikilink")
  - [北京GNOME用户组网站](http://www.bjgug.org/)

<!-- end list -->

  - 參與中文翻譯

<!-- end list -->

  - \[//l10n.gnome.org/teams/zh_CN GNOME 简体中文翻译团队\]
  - \[//l10n.gnome.org/teams/zh_trad GNOME 正體中文翻譯團隊\]
  - \[//groups.google.com.tw/group/chinese-l10n GNOME
    正體中文翻譯團隊網上論壇\]——Google網上論壇
  - \[//www.ubuntu-tw.org/modules/newbb/viewtopic.php?topic_id=8038\&post_id=39539\#forumpost39539
    GNOME 正體中文（臺灣）翻譯步驟教學\]——Ubuntu正體中文站

<!-- end list -->

  - 其他

<!-- end list -->

  - [GNOME vs
    KDE](https://docs.google.com/document/d/15CIS4pg_MdQr7PtfgRniTCfiE76ZQdUE-F-ZddAIHLU/edit?disco=AAAAAFOVrj4/)——GNOME
    vs KDE的比較表

{{-}}

[Category:桌面环境](../Category/桌面环境.md "wikilink")
[Category:GNU計劃軟體](../Category/GNU計劃軟體.md "wikilink")
[GNOME](../Category/GNOME.md "wikilink") [Category:X
Window系统](../Category/X_Window系统.md "wikilink")
[Category:GNU](../Category/GNU.md "wikilink")

1.

2.

3.  .

4.

5.

6.   GNOME |accessdate=2013-03-10}}

7.  The GNOME Project: "[GNOME Foundation Guidelines on Copyright
    Assignment](https://live.gnome.org/CopyrightAssignment/Guidelines)
    ". Accessed March 26, 2013.

8.

9.

10.

11.

12.

13.

14.

15.
16.

17.

18.

19.

20.
21.

22.

23.
24.  }}

25.

26.

27.

28.

29.
30.

31.

32.