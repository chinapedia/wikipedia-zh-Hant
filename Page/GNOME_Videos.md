****（原）是一套在类操作系统上运行的[多媒体播放器](../Page/多媒体.md "wikilink")，建基於桌面环境。默认的多媒体引擎是，但亦可使用程式库。在得到（现在）设为默认的多媒体播放器后大受欢迎。

## 特色

  - 可用或作为引擎

  - 与及紧密结合

  - 可播放，与数码（经由支持）

  - 可调整[亮度](../Page/亮度.md "wikilink")，[对比度](../Page/对比度.md "wikilink")，[色调及](../Page/色调.md "wikilink")[饱和度](../Page/饱和度\(色彩\).md "wikilink")

  - 可以在上实现全屏，支持双头输出及视口

  - 可改变长宽比，重新将影片的大小依比例改变

  - 支持多语系及字幕，而且自动载入外挂字幕

  - 支持4.0，4.1，5.0，5.1声道，[立体声与](../Page/立体声.md "wikilink")[AC3音频输出](../Page/AC3.md "wikilink")

  - 可调校音量

  - 提供音频视觉化插件

  - 可设定电视输出，而且可选择解析度

  - 支持（英：）

  - 支持，，，与播放清单

  - 提供可重播及搁置模式的播放清单，可由滑鼠改变播放次序及储存特色。

  - 可撷取截图

  - 用来支持 [1](http://www.home.unix-ag.org/simon/gromit/)

  - 以图示预览动画

  - 插件

  - 支持东亚右至左语言

## 参见条目

  - [媒体播放器列表](../Page/媒体播放器列表.md "wikilink")
  - [媒体播放器比较](../Page/媒体播放器比较.md "wikilink")

## 外部链接

  -
[Category:媒体播放器](../Category/媒体播放器.md "wikilink")
[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:GNOME](../Category/GNOME.md "wikilink")