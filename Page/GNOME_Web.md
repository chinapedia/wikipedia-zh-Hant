**GNOME
Web**（原名**Epiphany**）是[GNOME桌面下的](../Page/GNOME.md "wikilink")[免費](../Page/自由軟體.md "wikilink")[網頁瀏覽器](../Page/網頁瀏覽器.md "wikilink")。該瀏覽器是[Galeon的](../Page/Galeon.md "wikilink")[分支](../Page/复刻_\(软件工程\).md "wikilink")，由於Galeon開發者希望以功能齊全為目標，因此在發展方向上出現了分歧\[1\]。Web意在构建符合GNOME的浏览器，奉行简单唯美的理念。

Epiphany原本使用[Gecko排版引擎](../Page/Gecko.md "wikilink")，2.28版使用[WebKitGTK+排版引擎](../Page/WebKit.md "wikilink")\[2\]。

## 參考來源

## 参见

  - [网页浏览器比较](../Page/网页浏览器比较.md "wikilink")

## 外部链接

  -
[Category:2002年软件](../Category/2002年软件.md "wikilink")
[Category:用C編程的自由軟體](../Category/用C編程的自由軟體.md "wikilink")
[Category:自由网页浏览器](../Category/自由网页浏览器.md "wikilink")
[Category:GNOME核心应用程序](../Category/GNOME核心应用程序.md "wikilink")
[Category:Webkit衍生軟體](../Category/Webkit衍生軟體.md "wikilink")

1.
2.