[Linux_kernel_System_Call_Interface_and_glibc.svg](https://zh.wikipedia.org/wiki/File:Linux_kernel_System_Call_Interface_and_glibc.svg "fig:Linux_kernel_System_Call_Interface_and_glibc.svg")

**GNU
C函式庫**（，常简称为**glibc**）是一种按照[LGPL许可协议发布的](../Page/LGPL.md "wikilink")，自由的，公开[源代码的](../Page/源代码.md "wikilink")，方便从网络下载的C的编译程序。GNU
C运行期库，是一种C[函式库](../Page/函式库.md "wikilink")，是程序运行时使用到的一些[API集合](../Page/API.md "wikilink")，它们一般是已预先[编译好](../Page/编译.md "wikilink")，以[二进制代码形式存在](../Page/二进制.md "wikilink")[Linux类系统中](../Page/Linux.md "wikilink")，GNU
C运行期库通常作为GNU C编译程序的一个部分发布。

Glibc最初是[自由软件基金会](../Page/自由软件基金会.md "wikilink")（FSF）为其[GNU操作系统所写](../Page/GNU.md "wikilink")，但目前最主要的应用是配合[Linux内核](../Page/Linux内核.md "wikilink")，成为GNU/Linux操作系统一个重要的组成部分。

## 参考文献

## 外部連結

  - [Glibc官方網站](http://www.gnu.org/software/libc/)
  - [GNU
    C函式庫常見問題（FAQ）](http://applezulab.netdpi.net/05-glibc-manual-zhtw/gnu-c-faq-zhtw)

## 相關條目

  - 其他[C標準函式庫](../Page/C標準函式庫.md "wikilink")：

<!-- end list -->

  - [eglibc](../Page/Embedded_GLIBC.md "wikilink")
  - [Bionic libc](../Page/Bionic_\(軟體\).md "wikilink")
  - [dietlibc](../Page/dietlibc.md "wikilink")
  - [klibc](../Page/klibc.md "wikilink")
  - [musl](../Page/musl.md "wikilink")
  - [Newlib](../Page/Newlib.md "wikilink")
  - [uClibc](../Page/uClibc.md "wikilink")
  - [msvcrt](../Page/Visual_C++.md "wikilink")

[Category:C函式庫](../Category/C函式庫.md "wikilink")
[Category:GNU計劃軟體](../Category/GNU計劃軟體.md "wikilink")
[Category:C標準函式庫](../Category/C標準函式庫.md "wikilink")
[Category:自由软件](../Category/自由软件.md "wikilink")
[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:自由發展元件與函式庫](../Category/自由發展元件與函式庫.md "wikilink")