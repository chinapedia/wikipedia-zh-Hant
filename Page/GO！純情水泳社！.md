《**GO！純情水泳社！**》（，直譯為《健康全裸游泳社
海商》\[1\]），簡稱《海商》（），副標題「健康全裸系水泳社」，是於2005年的《[週刊少年Magazine](../Page/週刊少年Magazine.md "wikilink")》33號開始連載的[日本漫畫作品](../Page/日本漫畫.md "wikilink")。於2007年7月3日[動畫化](../Page/動畫.md "wikilink")，更於同年秋季推出[舞台劇](../Page/舞台劇.md "wikilink")，於2007年10月25日發售[5gk.製作的](../Page/5gk..md "wikilink")[PS2平台遊戲](../Page/PS2.md "wikilink")。無論是漫畫版還是動畫版，也是[殺必死極多的作品](../Page/殺必死.md "wikilink")。漫画版至第七卷，累计销量为40万部\[2\]。

## 故事簡介

位於[神奈川縣海猫市緒之島](../Page/神奈川縣.md "wikilink")\[3\]的「**縣立海貓商業高等學校**（簡稱**海商**）」忽然多了一位從[沖繩乘著](../Page/沖繩.md "wikilink")[木筏而來的轉校生](../Page/木筏.md "wikilink")**蜷川亞夢露**\[4\]。就在亞夢露來到的這一天開始，學校游泳社的氣氛起了變化，特別被她影響的是負責游泳社經理人的**沖浦要**……。

## 登場人物

  -
    <small>「聲」為動畫版[聲優](../Page/聲優.md "wikilink")，「演」為[舞台劇](../Page/舞台劇.md "wikilink")[演員](../Page/演員.md "wikilink")。遊戲聲優和動畫一樣。譯名以中文版漫畫單行本為準。身高、體重與[三圍以遊戲版官方網站為準](../Page/三圍.md "wikilink")，漫畫無此三項。</small>

### 海貓商業高等學校水泳部

  -
    2年級8班國際科生，與靜岡同班，身高162cm，體重50kg，[三圍](../Page/三圍.md "wikilink")85-60-85，[左撇子](../Page/左撇子.md "wikilink")。第二代水泳部代理副部長。從沖繩來、非常有朝氣的少女，戴著外婆送給她的「×」形髪飾。原本與父親同住在建在木筏上的小木屋，但因一次[颱風把家全毀而暫住沖浦家](../Page/颱風.md "wikilink")。個性[天然呆](../Page/天然呆.md "wikilink")，缺乏對人際關係的常識，但擅長[游泳](../Page/游泳.md "wikilink")，然而卻是模仿[海豚的奇怪游泳方式](../Page/海豚.md "wikilink")，能夠長距離潛水\[5\]。比賽項目是50、100m[自由泳](../Page/自由泳.md "wikilink")\[6\]（縣大賽、[關東大賽](../Page/關東.md "wikilink")）。有時會以[內衣代替](../Page/內衣.md "wikilink")[泳衣](../Page/泳衣.md "wikilink")，沒有人看到的時候更會[裸泳](../Page/裸泳.md "wikilink")。能操流利[英語](../Page/英語.md "wikilink")，但只限聽和說，不會讀和寫。六人兄弟姊妹中的大姊（下有3個弟弟、2個妹妹），自稱父親和弟弟有時會幫她按摩胸部，很喜歡撫摸魅玲的胸部。
    稱呼沖浦要為「」，在海商、玉塚與四宮的聯合集訓中向他告白。5年後跟沖浦要結婚，頭髮長度及腰，育有1子，全家住在建在木筏上的小木屋，漂流在沖繩。
    漫畫人氣投票第二名。
  -
    2年級4班商業科\[7\]生，身高163cm，體重58kg。住在緒之島一丁目\[8\]。生於只有母親的家庭，另外家中有一隻叫「拉拉」的狗。雖然是海商水泳部第一代經理兼第二代水泳部部長，但完全不懂游泳，討厭走入比膝部深的水中；這是因為他於小學時與家人同遊[沖繩](../Page/沖繩.md "wikilink")，在海中玩水時目擊相貌嚇人的[人魚](../Page/人魚.md "wikilink")。其後因亞夢露諄諄善誘，已可以走入及腰的水中。水泳部唯一正經的男生，因而得到其他女隊員信任。3年級時，經亞夢露外婆告知，他才知道自己小時候看見的人魚其實是亞夢露。5年後，與亞夢露結婚。
  -
    2年級8班國際科生，與亞夢露同班，身高163cm，體重54kg，[三圍](../Page/三圍.md "wikilink")92-60-86。第二代水泳部副部長，擅長[個人四式](../Page/混合泳.md "wikilink")。擁有G罩杯[巨乳的成熟系少女](../Page/巨乳.md "wikilink")，居住在海貓四丁目2番6號。大企業「」\[9\]的千金小姐，其祖父是SIZU集團創始人。外表清純而且不喜歡別人注意自己，但內裡意外地非常的色，經常對異性抱有妄想，內褲也是過分性感的一類（這是因為小學5年級時和堂哥禮一郎的一點事情，詳見單行本第6集特別篇），但這一點只有沖浦知道。怕熱，在家常常穿得很少。亦喜歡蒐集[民間藝術品](../Page/民間藝術.md "wikilink")，尤其[雕像](../Page/雕像.md "wikilink")，但不敢讓外人知道。畢業後在SIZU集團本社工作，私底下匿名寄送內衣設計圖給一家位於海外某小鎮的內衣公司，好幾款內衣設計圖都被採用。
    漫畫人氣投票第一名。
  -
    3年級7班情報處理科生，身高165cm，體重52kg，[三圍](../Page/三圍.md "wikilink")80-58-82。第一代水泳部副部長，實際維持水泳部運作，好勝心強，被全校學生稱為「大姊頭」。游泳的實力派，擅長[自由泳](../Page/自由泳.md "wikilink")，曾參加全國大賽。經常對人「飛腳」，不許別人直呼其名。假日喜愛窩在家看漫畫。畢業後以推薦甄試考進[山梨縣某大學](../Page/山梨縣.md "wikilink")，5年後考取小學教師執照。
    漫畫人氣投票第三名。
  -
    1年級2班商業科生，身高145cm，體重40kg，[三圍](../Page/三圍.md "wikilink")68-60-70。簡稱「」。幼兒體形，亞夢露和她初見面時誤以為她是-{zh-hans:初一生;zh-hk:中一生;zh-tw:國一生;}-。希望自己胸部變大，理由是想嘗試「跑步的時候，因為胸部太大而有[風阻的感覺](../Page/風阻.md "wikilink")」。為了令胸部變大，和真綾組成了「揉乳同盟」。曾為水泳社和揉乳同盟創建網站，但誤解参考書，過分使用[合成圖片而鬧出笑話](../Page/合成圖片.md "wikilink")。加入水泳社是為了學會游泳，在與沖浦要合組「推水同盟」時學會游泳。畢業後以[鐵人三項為目標](../Page/鐵人三項.md "wikilink")，每日自我訓練，曾參加日本全國鐵人三項比賽，在數場比賽中取得好成績。
  -
    1年級8班國際科生，身高160cm，體重38kg，[三圍](../Page/三圍.md "wikilink")76-55-79。暱稱「」，稱呼沖浦要為「小沖沖」、亞夢露為「叉叉女」。性格嬌蠻任性，流行雜誌的人氣[模特兒](../Page/模特兒.md "wikilink")，游泳也很有水準，擅長[背泳](../Page/背泳.md "wikilink")，曾在全國中學運動會背泳項目2連霸。沖浦要的[青梅竹馬](../Page/青梅竹馬.md "wikilink")，似乎對**要**有些好感，小學時住在沖浦家隔壁。單行本第2集開始時搬回海貓市並就讀海商，與父親同住海貓市近郊的高級公寓，初時視亞夢露為競爭對手。為了令胸部變大，與蒔輝組成了「揉乳同盟」。喜歡用「KoLo\[10\]」品牌的傘。畢業後繼續模特兒工作。
  -
    簡稱「」。3年級6班商業科生，第一代水泳部部長，身高182cm，體重80kg。吊兒郎當，家裡開[日本料理店](../Page/日本料理.md "wikilink")「」。喜歡[麻將和](../Page/麻將.md "wikilink")「剃毛」（不論對象），游泳時總是穿怪異的比基尼泳褲。水泳社創社前，曾向校方申請創立麻將社，但未成功。擔任水泳社社長期間沒在維持社務，都是桃子在維持社務。擅長[蝶泳](../Page/蝶泳.md "wikilink")，認真起來時可以非常厲害。與桃子是青梅竹馬，不正經時經常被桃子踢飛或狠踹；但除了幫桃子剃毛以外，對桃子沒興趣，有時也會性騷擾剃其他成員的毛。5年後在「割煮料理·碇矢」旁開了一間酒吧。
  -
    3年級國際科生，身高170cm，體重54kg，[三圍](../Page/三圍.md "wikilink")79-56-82。和桃子、碇雅一樣是水泳社的初期社員，稱呼桃子為「阿塚」。喜歡在[卡啦OK唱老歌](../Page/卡啦OK.md "wikilink")。經常半開玩笑地編故事，沖浦要聽她講水泳社創社故事講了三次。於漫畫第二集成為主要角色之一。擅長[蛙泳](../Page/蛙泳.md "wikilink")。畢業後在棉被公司工作，被社長請去在[電視購物頻道推銷](../Page/電視購物.md "wikilink")，與社長關係曖昧。
  -
    2年級3班商業科生，第二代水泳部副部長輔佐，暱稱「」。[公主髮式](../Page/公主髮式.md "wikilink")。就讀1年級2班時，因為自己會錯意，在水泳社入社申請書上簽名，加入水泳社；但基於自己「武士一言既出，駟馬難追」的信念及當時同年級的沖浦要的勉勵（當時她不認識沖浦要），未曾退出水泳社。和吉澤從[幼稚園時就相識](../Page/幼稚園.md "wikilink")。出自武術世家，家裡開弓道[武館](../Page/武館.md "wikilink")，3歲開始向父母學習[弓道](../Page/弓道.md "wikilink")；父親是警官，擁有弓道七段、[劍道五段](../Page/劍道.md "wikilink")、[柔道三段](../Page/柔道.md "wikilink")；母親是弓道武館師傅；哥哥在[東京都開柔道武館](../Page/東京都.md "wikilink")；嫂嫂是女子[踢拳高手](../Page/踢拳.md "wikilink")。有強烈的正義感，彌真奈說她穿著弓道服時會「鎮靜下來」。平時性格溫和；但情緒激動時，會想起被父親施加的[斯巴達教育](../Page/斯巴達教育.md "wikilink")，然後對對方做出相同的事：向對方射箭。於漫畫第六集開始成為主要角色之一。喜歡的歌手是[男高音歌手春川雅史](../Page/男高音.md "wikilink")。
  -
    2年級5班商業科生，頭的形狀像個[栗子](../Page/栗子.md "wikilink")，喜歡在說話句尾加「」。非常好色，桃子認為他「沒有倫理道德」。玩[電子遊戲非常厲害](../Page/電子遊戲.md "wikilink")，不論是[格鬥遊戲](../Page/格鬥遊戲.md "wikilink")、[射擊遊戲](../Page/射擊遊戲.md "wikilink")、[賽車遊戲](../Page/賽車遊戲.md "wikilink")、[音樂遊戲](../Page/音樂遊戲.md "wikilink")（作品中出現了模仿《[太鼓之達人](../Page/太鼓之達人.md "wikilink")》的《太鼓之超人》），亦擁有一部[PSP](../Page/PSP.md "wikilink")。畢業後開了一家冒險公司，自認社長。
  -
    2年級2班商業科生，「吉澤建設」社長女兒，暱稱是「」。擅長[個人四式](../Page/混合泳.md "wikilink")，其中最擅長蛙式。說話語氣有點像[貓](../Page/貓.md "wikilink")，常以「喵」字結尾。和佳織從[幼稚園時就相識](../Page/幼稚園.md "wikilink")。於漫畫第六集開始活躍起來。在校都穿運動服。假日會去建築工地與父親及現場工人一起工作，或是以「」為名從事[cosplay](../Page/cosplay.md "wikilink")。
  -
    1年級1班商業科生，暱稱「」。褐色皮膚。於漫畫第六集開始活躍起來，擅長料理，午餐都吃親手做的[便當](../Page/便當.md "wikilink")。
  -
    2年級6班商業科生，暱稱是「」。口部為「×」形，[極少說話](../Page/無口.md "wikilink")，有時會用[報紙字剪貼寫信的方式代替說話](../Page/報紙.md "wikilink")。
  -
    2年級6班（連載時是2年級1班）商業科生，兩頰有圓形腮紅。擅長800公尺自由泳，個性熱情大方，喜歡吃[拉麵](../Page/拉麵.md "wikilink")，和保代友好。
  -
    1年級7班情報處理科生。夢想是當個天才[程式員](../Page/程式員.md "wikilink")。有[二次元禁斷症候群](../Page/二次元禁斷症候群.md "wikilink")。和聖斗一樣有一部PSP。
  -
    2年級3班商業科生，長距離泳者。總是在睡覺，到哪裡都能睡，沒有和女性交往的經驗。部長選舉投了聖斗一票。
  -
    1年級1班商業科生，暱稱是「」，經常與千草在一起。
  -
    2年級1班商業科生，擅長自由泳。
  -
    2年級1班商業科生，和實玖友好，兩人常一起聊[同人誌](../Page/同人誌.md "wikilink")。
  -
    1年級生，長得像斑海豹小緒，感覺像是聖斗的部下，但很受女社員喜愛。
  -
    1年級生，小學時曾在游泳教室學游泳。

### 蜷川家

  -
    亞夢露父親，海商約第13屆畢業生，在緒之島長大，總是戴著頭巾和[太陽眼鏡](../Page/太陽眼鏡.md "wikilink")。年輕時獨自出外旅行，在沖繩認識蜷川季-{里}-子，不小心令她懷下了亞夢露，因此在沖繩居住下來。非常的閒，似乎沒有工作。不時會在海邊釣魚，包括誘捕小緒。使用[麥金塔電腦](../Page/麥金塔電腦.md "wikilink")（但因一次風暴而吹到海裡）。
  -
    住在台東島的亞夢露母親，在家中經營的「台東[蕎麥麵](../Page/蕎麥麵.md "wikilink")」（取自[南大東村特產](../Page/南大東村.md "wikilink")「」諧音）店工作。說著一口沖繩方言。亞夢露開朗的性格就是遺傳自她。
  -
    蜷川六兄弟姊妹長子，亞夢露最大的弟弟，就讀台東中學。雖然在兄弟姊妹中較成熟，但喜歡穿女裝及惡作劇，尤其喜歡捉弄亞夢露。成功奪走沖浦要的初吻。台東中學畢業後，就讀海商並加入水泳社。
  -
    亞夢露弟，染金髮。
  -
    亞夢露弟，開朗的黑髮男孩。
  -
    亞夢露妹，丸子頭。
  -
    亞夢露妹，怕生。
  -
    亞夢露外婆，103歲的[海女](../Page/海女.md "wikilink")，全身只穿一件無袖圓領上衣與一件三角內褲，全身貼了好幾個「×」形繃帶。亞夢露非常喜歡她。亞夢露轉入海商前一年，她跑去游泳就再沒有回來，正志認為她已經去世。最後她來到海貓市，證實自己還活著，因抓鯨魚而在海上待了2年。

### 靜岡家

  -
    魅玲父親，SIZU集團社長。
  -
    魅玲母親，SIZU集團社長夫人。喜歡穿布料很少的服裝，在家穿[裸體圍裙下廚](../Page/裸體圍裙.md "wikilink")。
  - 魅玲祖父母
    魅玲祖父母，只在〈魅玲爛漫〉登場，住在[飛驒](../Page/飛驒.md "wikilink")。在魅玲小學5年級時受魅玲父母之託，照顧魅玲。
  -
    魅玲堂哥，只在〈魅玲爛漫〉登場，留著及肩的長髮，嘴邊留著鬍渣，會吸菸。住在東京都。在魅玲小學5年級時，為了研究民間藝術品，他暫住魅玲祖父母家，與魅玲祖父母及魅玲同住；幾年後，他結婚，魅玲則與父母同住在海貓市。

### 四宮大學附屬高校女子水泳部

  -
    女子水泳部部長，身高182公分。視桃子為對手，因魅玲胸部比她大而不服氣。
  -
    可憐奈引退後的女子水泳部部長，游泳及不上魅玲。在緒之島祭時，武田稱她為「」，但覺得她很可愛。
  -
    在緒之島祭時，武田稱她為「」，戶部純覺得她很可愛。
  -
    在緒之島祭時，武田稱她為「」，堤勳覺得她很可愛。

### 玉塚學園女子水泳部

  -
    玉塚學園2年宙班學生，新堂四胞胎姊妹其一，接力賽第四棒。[僕娘](../Page/僕娘.md "wikilink")。對亞夢露的游泳方式感興趣。她號召海商與玉塚聯合集訓，意圖與亞夢露一起裸泳，卻在下水前被聖斗強吻，又被集訓所舍監發現，與聖斗一同被罰禁足一天；之後，她又將亞夢露單獨約出來邀請轉入玉塚，被亞夢露婉拒。
  -
    新堂四胞胎姊妹其二，接力賽第一棒。小心眼。
  -
    新堂四胞胎姊妹其三，接力賽第二棒。怕生。
  -
    新堂四胞胎姊妹其四，接力賽第三棒。活潑。

### 其他

  -
    沖浦要母親。
  -
    真綾父親，開車接送真綾上下學。
  -
    棲息在緒之島附近的[斑海豹](../Page/斑海豹.md "wikilink")，在天弁橋下被亞夢露發現，成為緒之島知名動物。
  -
    海商游泳池管理員。
  -
    碇雅父親，「割煮料理·碇矢」老闆，離婚多年。
  -
    田倉高校水泳部顧問。非常好色，更因此而潛入女隊員較多的海商水泳部。
  -
    真綾專屬的美少男游泳教練，[男同性戀](../Page/男同性戀.md "wikilink")、[手指控](../Page/手指控.md "wikilink")，擅長男扮女裝。被真綾暱稱為。
  -
    海商2年2班學生，新聞社社員。
  -
    海商2年2班學生，新聞社社員。曾因不慎以[數位相機拍到真綾的裸體](../Page/數位相機.md "wikilink")，而與真綾不睦；隨後爬入真綾家道歉，在真綾面前親手砸毀數位相機，與真綾和好。
  -
    前海商水泳部顧問，與桃子、早苗、碇雅共同創辦水泳部，真實外貌是一點都不起眼的中年大叔。早苗假裝有意追求他，他當真而追求早苗，早苗就向校方宣稱被他[性騷擾](../Page/性騷擾.md "wikilink")，導致他被校方開除，水泳部從此沒有顧問。
  -
    中德寺[住持](../Page/住持.md "wikilink")，「割煮料理·碇矢」的常客。性格好色，且會去「割煮料理·碇矢」喝酒，被桃子蔑稱為「破戒和尚」。
  -
    碇雅弟弟，戶一高校2年生，暱稱「小拓」。性格認真，和碇雅完全相反。喜歡桃子，直呼桃子「小桃」，其後更在全國大賽當眾向桃子表白。
  -
    蒔輝表弟，9歲的城市小孩，暱稱「小清」，性格穩重。他於暑假來了海貓市一趟，要求蒔輝、沖浦要與亞夢露幫他捉[鍬形蟲](../Page/鍬形蟲.md "wikilink")、[獨角仙之類的大昆蟲](../Page/獨角仙.md "wikilink")，沖浦要認為他是個只說不做的臭小孩；但一些意外使大昆蟲爬滿他全身，大昆蟲成為了他的心理創傷。
  -
    玉塚學園集訓所舍監，38歲，有著魁梧的身材與恐怖的容貌。

## 用語及設定

  -
    「」是[沖繩方言](../Page/沖繩方言.md "wikilink")，意為「美麗的」。整個詞即是「美麗的海」之意。是亞夢露口中和沖浦要初次見面的地方。另外也是本作連載前的初期標題候選之一。

<!-- end list -->

  -
    以亞夢露說她父親和弟弟會幫她按摩胸部為契機，蒔輝為了胸部變大而「慫恿」真綾加入的怪同盟。其後強拉魅玲當「顧問」。其後因蒔輝建立的揉乳同盟網站內一個拜魅玲為「」的虛構網頁公布魅玲真實姓名及住址，魅玲瞬間成名，揉乳同盟「信徒」遍布全日本，「信徒」更莫名其妙的分成「崇拜派」、「揉乳浪花派」、「矯正假乳派」、「自然揉乳派」、「擦乳派」、「超級揉乳派」、「整形假乳派」、「美乳派」、「古代美術研究派」、「[基因操縱派](../Page/基因.md "wikilink")」、「保健食品派」等分派。「信徒」們甚至集體來海貓市「朝聖」，給魅玲的生活帶來極大困擾；但最後「信徒」們知道胸部不大的蒔輝和真綾是創立成員時，「信徒」們一拍而散，蒔輝永久關閉揉乳同盟網站。

<!-- end list -->

  - 笨蛋獎盃
    海商將校內全部社團按照考試成績高低排名，每年成績最低的社團獲頒的獎盃，底座銘板刻「最笨社團」四字。

<!-- end list -->

  - 福氣賽跑\[11\]
    緒之島從古時就四年一度舉辦的賽跑，跑約兩公里，福氣會降臨在最先到達終點（緒之島岩穴）的男女（稱為「福男」、「福女」）上，早苗更聲稱第一名的男女必定會成為一對。

<!-- end list -->

  - A1
    一種實際存在、在沖繩地區受歡迎的[英國製沾醬](../Page/英國.md "wikilink")，淋在[牛排或](../Page/牛排.md "wikilink")[漢堡排等食物上](../Page/漢堡排.md "wikilink")。亞夢露和正志也喜歡沾來吃。

<!-- end list -->

  -
    故事中以[大東群島](../Page/大東群島.md "wikilink")（主要為[南大東村](../Page/南大東村.md "wikilink")）作藍本的虛構地方，蜷川一家所居住的地方。雖說是虛構，但在設定上有多處和實際一樣，如：四周為懸崖峭壁、因海象不佳而需要以起重機把船隻吊到島上等。但亦有與實際不同的設定：仍使用舊機場、航班只是一日一班而非兩班等。

<!-- end list -->

  -
    蒔輝強拉沖浦要組成的同盟。但組成後第二天，蒔輝學會游泳，隨即宣告解散。

## 出版書籍

<table>
<thead>
<tr class="header">
<th><p>冊數</p></th>
<th><p><a href="../Page/講談社.md" title="wikilink">講談社</a></p></th>
<th><p><a href="../Page/東立出版社.md" title="wikilink">東立出版社</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>發售日期</p></td>
<td><p><a href="../Page/ISBN.md" title="wikilink">ISBN</a></p></td>
<td><p>發售日期</p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>2006年1月17日</p></td>
<td><p>ISBN 4-06-363618-6</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>2006年4月17日</p></td>
<td><p>ISBN 4-06-363662-3</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>2006年8月17日</p></td>
<td><p>ISBN 4-06-363710-7</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>2006年12月15日</p></td>
<td><p>ISBN 4-06-363764-6</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p>2007年4月17日</p></td>
<td><p>ISBN 978-4-06-363820-2</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p>2007年7月17日</p></td>
<td><p>ISBN 978-4-06-363855-4<br />
ISBN 978-4-06-362086-3（限定版）</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>2007年10月17日</p></td>
<td><p>ISBN 978-4-06-363902-5</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p>2008年3月17日</p></td>
<td><p>ISBN 978-4-06-363966-7</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p>2008年6月17日</p></td>
<td><p>ISBN 978-4-06-384002-5<br />
ISBN 978-4-06-362114-3（限定版）</p></td>
</tr>
</tbody>
</table>

  - 第6卷限定版附贈全彩小冊子「」同梱販售
  - 第9卷限定版附贈插畫大師服部充插畫集「」同梱販售

## 電視動畫

2007年7月3日開始播放。故事內容、情節、對白等多以漫畫作藍本。全13集。

### 工作人員

  - 原作：[服部充](../Page/服部充.md "wikilink")
  - 企劃：森田浩章、[片岡義朗](../Page/片岡義朗.md "wikilink")
  - 系列構成：[池田真美子](../Page/池田真美子.md "wikilink")
  - [人物設定](../Page/人物設定.md "wikilink")、總作畫監督：西野理惠
  - 美術監督：柴田千佳子
  - [色彩設計](../Page/色彩設計.md "wikilink")：山崎朋子
  - 撮影監督：藤田智史
  - 編集：松村正宏
  - 音響監督：
  - 音樂：[佐藤泰將](../Page/佐藤泰將.md "wikilink")
  - 音樂製作人：小柳路子
  - 音樂制作：[5pb.](../Page/5pb..md "wikilink")
  - 音響効果：奧田維城、出雲範子（）
  - 錄音室：、整音錄音室
  - 音響制作：
  - 宣傳：長谷川朋子、佐藤輝彦、高橋克典、古屋友梨
  - 關聯製作人：土肥範子、小谷哲子
  - 製作人：小川文平、大森啓幸
  - line producer：渡邊秀信
  - 監督：
  - 動畫制作：[ARTLAND](../Page/ARTLAND.md "wikilink")
  - 制作：[海商製作委員會](../Page/製作委員會方式.md "wikilink")（[Marvelous
    Entertainment](../Page/Marvelous_Entertainment.md "wikilink")、[Geneon
    Entertainment](../Page/Geneon_Entertainment.md "wikilink")、[5pb.](../Page/5pb..md "wikilink")、[波麗佳音企業](../Page/波麗佳音.md "wikilink")）

### 主題曲

  - [片頭曲](../Page/片頭曲.md "wikilink")「DOLPHIN☆JET」
    作詞、作曲：[志倉千代丸](../Page/志倉千代丸.md "wikilink")，編曲：上野浩司
    歌：[彩音](../Page/彩音.md "wikilink")
    CD於2007年7月25日發售，1,260日圓（含稅）

<!-- end list -->

  - [片尾曲](../Page/片尾曲.md "wikilink")「」
    作詞、作曲：，編曲：上野浩司
    歌：[村田步](../Page/村田步.md "wikilink")
    CD於2007年7月25日發售，1,260日圓（含稅）

### 播放電視台

<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放地域</p></th>
<th style="text-align: center;"><p>電視台</p></th>
<th style="text-align: center;"><p>播放期間</p></th>
<th style="text-align: center;"><p>播放時間（<a href="../Page/UTC.md" title="wikilink">UTC</a>+9）</p></th>
<th style="text-align: center;"><p>聯播網</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/東京都.md" title="wikilink">東京都</a></p></td>
<td style="text-align: center;"><p><a href="../Page/東京都會電視台.md" title="wikilink">東京都會電視台</a><br />
（幹事局）</p></td>
<td style="text-align: center;"><p>2007年7月3日－</p></td>
<td style="text-align: center;"><p>星期二 26時00分～26時30分<br />
（星期三 2時00分～2時30分）</p></td>
<td style="text-align: center;"><p><a href="../Page/獨立UHF局.md" title="wikilink">獨立UHF局</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/千葉縣.md" title="wikilink">千葉縣</a></p></td>
<td style="text-align: center;"><p><a href="../Page/千葉電視台.md" title="wikilink">千葉電視台</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/愛知縣.md" title="wikilink">愛知縣</a></p></td>
<td style="text-align: center;"><p><a href="../Page/愛知電視台.md" title="wikilink">愛知電視台</a></p></td>
<td style="text-align: center;"><p>星期二 26時58分～27時28分<br />
（星期三 2時58分～3時28分）</p></td>
<td style="text-align: center;"><p><a href="../Page/TXN.md" title="wikilink">東京電視台系列</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/大阪府.md" title="wikilink">大阪府</a></p></td>
<td style="text-align: center;"><p><a href="../Page/大阪電視台.md" title="wikilink">大阪電視台</a></p></td>
<td style="text-align: center;"><p>2007年7月4日－</p></td>
<td style="text-align: center;"><p>星期三 26時55分～27時25分<br />
（星期四 2時55分～3時25分）</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/埼玉縣.md" title="wikilink">埼玉縣</a></p></td>
<td style="text-align: center;"><p><a href="../Page/埼玉電視台.md" title="wikilink">埼玉電視台</a></p></td>
<td style="text-align: center;"><p>2007年7月7日－</p></td>
<td style="text-align: center;"><p>星期六 26時00分～26時30分<br />
（星期日 2時00分～2時30分）</p></td>
<td style="text-align: center;"><p>獨立UHF局</p></td>
</tr>
</tbody>
</table>

### 各集標題

<table>
<thead>
<tr class="header">
<th><p>集數</p></th>
<th><p>標題</p></th>
<th><p>中文標題</p></th>
<th><p>首播日期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td><p>美麗的海！</p></td>
<td><p>2007年7月3日</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p>或許…<br />
有點…興趣…</p></td>
<td><p>2007年7月10日</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td><p>給你看！！！</p></td>
<td><p>2007年7月17日</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td><p>就說了會給你看</p></td>
<td><p>2007年7月24日</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td><p>你也想要我的嗎？</p></td>
<td><p>2007年7月31日</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td></td>
<td><p>喝！</p></td>
<td><p>2007年8月7日</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td></td>
<td><p>咀咒</p></td>
<td><p>2007年8月14日</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td></td>
<td><p>給我好～好看著</p></td>
<td><p>2007年8月21日</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td></td>
<td><p>給我忘記…</p></td>
<td><p>2007年8月28日</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td></td>
<td><p>揉乳？</p></td>
<td><p>2007年9月4日</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td></td>
<td><p>難不成是…</p></td>
<td><p>2007年9月11日</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td></td>
<td><p>的啦、的啦～♪</p></td>
<td><p>2007年9月18日</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td></td>
<td><p>人魚</p></td>
<td><p>2007年9月25日</p></td>
</tr>
</tbody>
</table>

### 角色CD

1.  vol.1 蜷川亞夢露（豐崎愛生）
      -
        盒裝限定版VGCD-0106，通常版VGCD-0107，2007年8月24日發售。
2.  vol.2 静岡魅鈴（福井裕佳梨）
      -
        VGCD-0108，2007年8月24日發售。
3.  vol.3 織塚桃子（生天目仁美）
      -
        VGCD-0109，2007年10月3日發售。
4.  vol.4 黃瀨早苗（新谷良子）
      -
        VGCD-0110，2007年9月7日發售。
5.  vol.5 生田蒔輝（清水愛）
      -
        VGCD-0111，2007年9月21日發售。
6.  vol.6 魚魚戶真綾（矢作紗友里）
      -
        VGCD-0112，2007年9月21日發售。

<!-- end list -->

  -
    全部通常版1,890日圓（含稅），vol.1 蜷川亞夢露限定版2,310日圓（含稅）。

## 舞台剧

舞台剧首轮於2007年8月2日－8月8日持续7天公演。DVD于2007年11月21日发售，价格为3990[日元](../Page/日元.md "wikilink")。

### 舞台版原創角色

  - （演：[宮下雄也](../Page/宮下雄也.md "wikilink")）
    （演：[深澤優希](../Page/深澤優希.md "wikilink")）
    （演：[吉田仁美](../Page/吉田仁美.md "wikilink")）
    （演：[石橋美佳](../Page/石橋美佳.md "wikilink")）
    陸男、石像、其他（演：[小林健一](../Page/小林健一.md "wikilink")）

## 遊戲

於2007年10月25日由發售5gk.製作的[PS2遊戲](../Page/PS2.md "wikilink")。普通版售价7140日元、初回限定版为9870[日元](../Page/日元.md "wikilink")。遊戲標題是「」。

### 主題曲

  - 片頭曲「1.2.3 SWEET SUMMER\!」
    作詞：[村田步](../Page/村田步.md "wikilink")
    作曲：TARAWO
    歌：村田步
  - 片尾曲「New Breeze」
    作詞：海野真司
    作曲、編曲：上野浩司
    歌：彩音

## 網上廣播

  -
    2007年5月16日至6月27日於[音泉](../Page/音泉.md "wikilink")、[BEWE逢星期三發放](../Page/BEWE.md "wikilink")，全4回。主持人是[生天目仁美](../Page/生天目仁美.md "wikilink")（織塚桃子）。
    2007年7月4日，標題更換為「****」，節目內容翻新。[豐崎愛生](../Page/豐崎愛生.md "wikilink")（蜷川亞夢露）更加入主持。

### 環節

  -
  -
  -
  -
### 嘉賓

  - 準備運動

<!-- end list -->

  - 第一回（5月16日）[服部充](../Page/服部充.md "wikilink")（原作者）

<!-- end list -->

  -
    但在節目介紹的圖像中沒有露面，只有生天目一人。

  - 夏\!\!本番

<!-- end list -->

  - 第一回（7月4日）服部充（原作者）

<!-- end list -->

  -
    同樣沒有在介紹圖像露面，只有生天目和豐崎。

## 關聯項目

  - [服部充](../Page/服部充.md "wikilink")
  - [游泳](../Page/游泳.md "wikilink")
  - [沖繩](../Page/沖繩.md "wikilink")
  - [大東群島](../Page/大東群島.md "wikilink")

## 註釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [服部充個人網站](http://www.studio-hami.net/)

  - [週刊少年Magazine内作品介紹](https://web.archive.org/web/20070703090335/http://www.shonenmagazine.com/works/umisho/index.html)

  - [動畫官方網站](http://www.mmv.co.jp/special/umisho/)

  - [遊戲官方網站](http://www.5pb.jp/games/umisho/index.html)

  - [舞台劇官方網站](https://web.archive.org/web/20070630124906/http://www.nelke.co.jp/umisho/)

  - [音泉](http://www.onsen.ag/)

  - [南大東島網頁](http://www.vill.minamidaito.okinawa.jp/)

  - [北大東島網頁](http://www.vill.kitadaito.okinawa.jp/)

[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:講談社](../Category/講談社.md "wikilink")
[Category:講談社漫畫](../Category/講談社漫畫.md "wikilink")
[Category:少年漫畫](../Category/少年漫畫.md "wikilink")
[Category:體育動畫](../Category/體育動畫.md "wikilink")
[Category:週刊少年Magazine連載作品](../Category/週刊少年Magazine連載作品.md "wikilink")
[Category:2007年UHF動畫](../Category/2007年UHF動畫.md "wikilink")
[Category:PlayStation 2游戏](../Category/PlayStation_2游戏.md "wikilink")
[Category:日本廣播節目](../Category/日本廣播節目.md "wikilink")
[Category:動畫廣播](../Category/動畫廣播.md "wikilink")
[Category:後宮型作品](../Category/後宮型作品.md "wikilink")
[Category:神奈川縣湘南背景作品](../Category/神奈川縣湘南背景作品.md "wikilink")
[Category:2007年電子遊戲](../Category/2007年電子遊戲.md "wikilink")
[Category:游泳題材作品](../Category/游泳題材作品.md "wikilink")
[Category:高中背景漫畫](../Category/高中背景漫畫.md "wikilink")
[Category:高中背景動畫](../Category/高中背景動畫.md "wikilink")

1.  「海商」為故事中的學校簡稱。
2.  [太平洋游戏网 -
    一湿成名\!《健康全裸游泳社》顺利PS2游戏化\!](http://www.pcgames.com.cn/cartoon/news/guowainews/0708/941026.html)

3.  以神奈川縣[藤澤市](../Page/藤澤市.md "wikilink")[江之島作為藍本](../Page/江之島.md "wikilink")。
4.  又譯**蜷川亞室**。
5.  但比賽規定跳水後15公尺內必須浮上水面，否則會失去比賽資格。
6.  [自由泳不同](../Page/自由泳.md "wikilink")[自由式](../Page/捷泳.md "wikilink")，但大多游泳員在自由泳比賽時選擇使用自由式。外界人士經常把兩者混淆，中文版漫畫就有這種錯誤。
7.  海商各年級分為3科：商業科6班，情報處理科1班，國際科2班。
8.  中文版漫畫單行本第1集將緒之島錯譯為「御野島」。
9.  中文版漫畫單行本第1集譯為「SIZU財團」。
10. 作者以前的漫畫中的主角在長大後創立的品牌。
11. 聲稱出自《緒之島的歷史》。