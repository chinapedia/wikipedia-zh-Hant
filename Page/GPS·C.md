**GPS·C**是**GPS修正**的缩写，是一个受加拿大主控系统管理的差分GPS数据修正系统，属于[Natural Resources
Canada](../Page/Natural_Resources_Canada.md "wikilink")。使用支持这个系统的设备可以把普通GPS接收机15米的定位精度提高到实时1-2米的水平。

实时数据由分布在加拿大各地的14个固定站收集转发到位于渥太华的中央控制室“NRC1”进行处理。

## CDGPS

GPS·C信息现在通过卫星[MSAT的](../Page/MSAT.md "wikilink")**CDGPS**（是“加拿大地区DGPS校正服务”缩写）向全加拿大发送。不同于[欧空局开发的](../Page/欧洲航天局.md "wikilink")[EGNOS系统](../Page/歐洲地球同步衛星導航增強服務系統.md "wikilink")，CDGPS需要一个独立的MSAT接收器，用来输出[RTCM格式的校验信息给支持此技术的GPS接收器](../Page/RTCM.md "wikilink")。这就导致了这种接收器不像美国[WAAS或者](../Page/广域增强系统.md "wikilink")[StarFire那样经济](../Page/StarFire_\(导航系统\).md "wikilink")，这两种设备的矫正系统和GPS系统使用同一套天线和接收器。

| 站名   | 位置                                                                                                                                      | 建成时间        |
| ---- | --------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| ALBH | 加拿大军事基地，[Esquimalt](../Page/Esquimalt.md "wikilink")（维多利亚，英属哥伦比亚附近）                                                                     | 1992年5月     |
| ALGO | Algonquin Space Complex, [Algonquin Provincial Park](../Page/Algonquin_Provincial_Park.md "wikilink")，[安大略](../Page/安大略.md "wikilink")  | 1991年1月     |
| CHUR | Geological Survey of Canada 地震监测站，[Churchill, Manitoba](../Page/Churchill,_Manitoba.md "wikilink")                                      | 1993年4月     |
| DRAO | [自治领射电天体物理台](../Page/自治领射电天体物理台.md "wikilink"), [Penticton](../Page/Penticton.md "wikilink")[英属哥伦比亚](../Page/英属哥伦比亚.md "wikilink")      | 1991年2月     |
| EUR2 | 新环境加拿大气象站 [Eureka, Nunavut](../Page/Eureka,_Nunavut.md "wikilink")                                                                      | 2005年10月9日  |
| FRDN | Hugh John Fleming Forestry Complex, New Brunswick大学附近 [Fredericton](../Page/Fredericton.md "wikilink")                                  | 2003年2月     |
| HLFX | Bedford海洋生物学院，[Halifax, Nova Scotia](../Page/Halifax,_Nova_Scotia.md "wikilink")                                                        | 2001年12月19日 |
| NRC1 | 国家计量标准研究所，国家研究中心，[渥太华](../Page/渥太华.md "wikilink")[安大略](../Page/安大略.md "wikilink")                                                       | 1995年4月     |
| PRDS | [Priddis, AlbertaDominion](../Page/Priddis,_Alberta.md "wikilink") 天文台（在[Calgary, Alberta附近](../Page/Calgary,_Alberta.md "wikilink")）   | 1997年1月7日   |
| SCH2 | Transport Canada radio telecommunication facility, [Schefferville](../Page/Schefferville.md "wikilink")[魁北克](../Page/魁北克.md "wikilink") | 1997年6月29日  |
| STJO | 加拿大地址测量站（NRCan）地磁监测站，[圣琼斯](../Page/圣琼斯.md "wikilink")[纽芬兰](../Page/纽芬兰.md "wikilink")                                                   | 1992年5月     |
| WHIT | [Whitehorse, Yukon](../Page/Whitehorse,_Yukon.md "wikilink")                                                                            | 1996年6月     |
| WINN | NavCanada 温尼伯区域控制中心，[Winnipeg, Manitoba](../Page/Winnipeg,_Manitoba.md "wikilink")                                                      | 1997年1月9日   |
| YELL | [Yellowknife, Northwest Territories](../Page/Yellowknife,_Northwest_Territories.md "wikilink")                                          | 1991年1月     |

CDGPS 参考站\[1\]

## 外部链接

  - [CDGPS](https://web.archive.org/web/20070630190448/http://www.cdgps.com/)
  - [CDGPS (Canada-Wide DGPS Correction
    Service)](https://web.archive.org/web/20060711171023/http://ess.nrcan.gc.ca/2002_2006/gnd/cdgps_e.php)
  - [GPS·C Distribution Using
    NTRIP](http://igs.bkg.bund.de/root_ftp/NTRIP/documentation/12_Macleod.pdf)
    — [PDF](../Page/PDF.md "wikilink") format

## 参看

53502M8FSW1101097 cruiser

## 参考文献

<references />

[Category:全球定位系统](../Category/全球定位系统.md "wikilink") [Category:Natural
Resources Canada](../Category/Natural_Resources_Canada.md "wikilink")

1.