**GPS辅助型静地轨道增强导航**或**GPS和型静地轨道增强导航GPS**系统
(**GAGAN**)[印度政府计划建设的](../Page/印度.md "wikilink")[卫星增强系统](../Page/卫星增强系统.md "wikilink")
(SBAS)。（*Gagan*是[印度语天的意思](../Page/印度语.md "wikilink")）通过提供参考信号给[GNSS接收器以提高它的精度](../Page/全球导航卫星系统.md "wikilink")
。[Airports Authority of
India](../Page/Airports_Authority_of_India.md "wikilink")（AAI）和[印度空间探索组织](../Page/印度空间探索组织.md "wikilink")（ISRO）负责项目研发，这个项目选用[地球同步卫星GSAT](../Page/地球同步卫星.md "wikilink")-4，计划2008年投入使用。\[1\]\[2\]

印度政府已经决定通过建造GAGAN系统的获得经验，建立印度自有的导航系统：[印度区域导航卫星系统](../Page/印度区域导航卫星系统.md "wikilink")（IRNSS），这个系统可能使用[GSAT-4卫星作为推荐导航系统的实验阶段示范](../Page/GSAT.md "wikilink")。\[3\]

## 参看

  - [GPS](../Page/GPS.md "wikilink")
  - [广域增强系统](../Page/广域增强系统.md "wikilink")

## 参考文献

<references />

[Category:全球定位系统](../Category/全球定位系统.md "wikilink")
[Category:导航](../Category/导航.md "wikilink")
[Category:同步卫星](../Category/同步卫星.md "wikilink")

1.  [ISRO,
    Raytheon完成GAGAN卫星导航系统测试](http://www.india-defence.com/reports/2239)
     印度国防部网站。2006年6月20日.
2.  K.N. Suryanarayana Rao and S. Pal. [印度SBAS系统 –
    GAGAN](http://www.aiaa.org/indiaus2004/Sat-navigation.pdf)
    。美印空间科学，应用和商业峰会摘要。2004年6月.
3.  [SATNAV Industry
    Meet 2006](http://www.isro.org/newsletters/spaceindia/aprsep2006/Satnavindustry.htm)
    . ISRO Space India Newsletter. 2006年4-9月刊.