**GT**/**Gt**在以下的領域可以指：

## 與[科學](../Page/科學.md "wikilink")、[技術](../Page/技術.md "wikilink")、[工程學及](../Page/工程學.md "wikilink")[度量衡相關](../Page/度量衡.md "wikilink")

  - [燃氣輪機的](../Page/燃氣輪機.md "wikilink")[英語](../Page/英語.md "wikilink")
    **G**as **T**urbine，縮寫“GT”。通過燃燒氣體燃料而獲取動力的一種發動機；
  - 《[幾何與拓撲](../Page/幾何與拓撲.md "wikilink")》英語 ***G**eometry &
    **T**opology*, 一本關於數學的期刊；
  - [珍鰺英語](../Page/珍鰺.md "wikilink") **G**iant **t**revally，是魚的一種，屬於鰺科；
  - [千兆特斯拉](../Page/特斯拉.md "wikilink")（**G**iga**t**esla）,
    [國際單位制](../Page/國際單位制.md "wikilink")[磁感應強度的單位特斯拉換算而來](../Page/磁感應強度.md "wikilink")；
  - [千兆噸](../Page/噸.md "wikilink")（**G**iga**t**onne），國際單位制中質量的單位噸換算而來；
  - [Gigatransfer](../Page/Gigatransfer.md "wikilink"), 一種轉移（或操作）的數據；
  - [全局碼](../Page/全局碼.md "wikilink")（**G**lobal **T**itle）,
    一種用在[信令消息路由電信網絡的地址](../Page/信令消息路由.md "wikilink")；
  - [總噸數/總噸位](../Page/總噸數.md "wikilink")（**G**ross
    **t**onnage），用在[常衡或](../Page/常衡.md "wikilink")[英製度量衡系統](../Page/英製.md "wikilink")；
  - [紮根理論](../Page/紮根理論.md "wikilink")（**G**rounded
    **t**heory），一個關於“[生成理論](../Page/生成理論.md "wikilink")”
    ，尤其是在社會科學方面的理論；

## [汽車](../Page/汽車.md "wikilink")

  - “[Grand
    Tourer](../Page/Grand_Tourer.md "wikilink")”的縮寫“GT”，語出意大利語“*gran
    turismo*”，指為長途駕駛而設計的高性能車，又譯[豪華旅行車](../Page/豪華旅行車.md "wikilink")。
  - [阿爾法·羅密歐GT](../Page/阿爾法·羅密歐GT.md "wikilink")，[阿爾法·羅密歐製造的一](../Page/阿爾法·羅密歐.md "wikilink")[跑車](../Page/跑車.md "wikilink")；
  - [国际汽联GT锦标赛](../Page/国际汽联GT锦标赛.md "wikilink")（FIA GT）；
  - [福特GT](../Page/福特GT.md "wikilink"),，[福特製造的一](../Page/福特汽車.md "wikilink")[超級跑車](../Page/超級跑車.md "wikilink")；
  - *Grand Touring*, used by the [IMSA GT
    Championship](../Page/IMSA_GT_Championship.md "wikilink")
  - [Lightning GT](../Page/Lightning_GT.md "wikilink"), a British
    electric sports car
  - [歐寶GT](../Page/歐寶GT.md "wikilink")，[歐寶製造的一跑車](../Page/歐寶.md "wikilink")；
  - [薩博GT750](../Page/薩博GT750.md "wikilink")，一款1958年至1960年間[薩博製造的汽車](../Page/薩博.md "wikilink")。

## 電子遊戲

  - [GT Interactive](../Page/GT_Interactive.md "wikilink")，一美國電子遊戲開發商；
  - [GameTrailers](../Page/GameTrailers.md "wikilink")，一電子遊戲網站；
  - *[Golden Tee Golf](../Page/Golden_Tee_Golf.md "wikilink")*, 高爾夫電子遊戲；
  - [GT賽車](../Page/GT賽車.md "wikilink")（Gran Turismo），也作《跑車浪漫旅》系列

## 娛樂

  - *[Gay Times](../Page/Gay_Times.md "wikilink")*，一本英國男同性戀文化雜誌。
  - [七龍珠GT](../Page/七龍珠GT.md "wikilink")，一[日本動漫](../Page/日本動漫.md "wikilink")。
  - [經典老爺車](../Page/經典老爺車.md "wikilink")（**G**ran
    **T**orino），一部2008年上映，由克林·伊斯威特主演的戲劇。
  - [*Gran Turismo*
    (album)](../Page/Gran_Turismo_\(album\).md "wikilink")，Cardigans在1998年發行的專輯
  - [格列佛遊記](../Page/格列佛遊記.md "wikilink")（**G**ulliver's
    **T**ravels），一本1700年代出版的小說。
  - [Gyllene
    Tider](../Page/Gyllene_Tider.md "wikilink")，一[瑞典流行團體](../Page/瑞典.md "wikilink")。

## 地域

  - **G**eorgia Institute of
    **T**echnology，[佐治亞理工學院的英語名稱](../Page/佐治亞理工學院.md "wikilink")；
  - [喬治敦](../Page/喬治敦.md "wikilink")（英語：**G**eorge**t**own），[圭亞那首都](../Page/圭亞那.md "wikilink")，也是圭亞那最大城市；
  - [南美國家](../Page/南美洲.md "wikilink")[危地馬拉英語](../Page/危地馬拉.md "wikilink")
    **G**ua**t**emala （[ISO國際代碼](../Page/ISO國際代碼.md "wikilink")）；
  - [St. Joseph's College, Gregory
    Terrace](../Page/St._Joseph's_College,_Gregory_Terrace.md "wikilink"),
    一座位於[澳洲](../Page/澳洲.md "wikilink")[昆士蘭](../Page/昆士蘭.md "wikilink")[Spring
    Hill的天主教男子學校](../Page/Spring_Hill.md "wikilink")；

## 電子資訊

  - [Globus Toolkit](../Page/Globus_Toolkit.md "wikilink")
  - [Google Talk](../Page/Google_Talk.md "wikilink"), Google的即時通訊軟件
  - [Google Translate](../Page/Google_Translate.md "wikilink"),
    Google的翻譯服務

## 公司、企業

## 運輸

## 其它