**GUNDAM**（，）通常是指：

  - [富野由悠季創作的](../Page/富野由悠季.md "wikilink")[科幻](../Page/科幻.md "wikilink")[機器人動畫](../Page/機器人動畫.md "wikilink")：[機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")（初代），亦稱0079
  - 「機動戰士GUNDAM」（初代）及其續作、外傳之總稱。參見[GUNDAM系列作品以及](../Page/GUNDAM系列作品.md "wikilink")[GUNDAM系列作品列表](../Page/GUNDAM系列作品列表.md "wikilink")
  - 「機動戰士GUNDAM」（初代）中登場的主角[机动战士](../Page/机动战士.md "wikilink")：**RX-78-2
    GUNDAM**。參見[RX-78系列机动战士](../Page/RX-78系列机动战士.md "wikilink")
  - 泛稱所有在**[GUNDAM系列作品](../Page/GUNDAM系列作品一覽.md "wikilink")**當中出場，以RX-78為發想基礎的[机动战士](../Page/机动战士.md "wikilink")（Mobile
    Suit），參見**[GUNDAM系機動戰士](../Page/GUNDAM系機動戰士.md "wikilink")**
  - 1980年代在日本掀起動畫製作變革的關鍵性作品之一。

## 名稱

  - [香港官方譯名及](../Page/香港.md "wikilink")[G
    GUNDAM中的支持標語中為](../Page/G_GUNDAM.md "wikilink")「高-{}-達」
  - [台灣官方譯名及某一件官方的設計](../Page/台灣.md "wikilink")[T恤中為](../Page/T恤.md "wikilink")「鋼-{}-彈」（但該T恤上有小字列出香港譯名為高-{}-達）
  - [中國大陸官方譯名為](../Page/中國大陸.md "wikilink")「敢-{}-达」（因“高-{}-达”已被其他公司注册，直至2017年才由[日升动画收回](../Page/日升动画.md "wikilink")），民间一直称“高-{}-达”。
  - [萬代出品的機動戰士系列模型一般稱做為](../Page/萬代.md "wikilink")「[GUNPLA](../Page/GUNPLA.md "wikilink")」，為英文Gundam
    Plastic Model的縮寫。
  - 人名

## 參見

  - [機器人動畫](../Page/機器人動畫.md "wikilink")
  - [超时空要塞](../Page/超时空要塞.md "wikilink")
  - [新世纪福音战士](../Page/新世纪福音战士.md "wikilink")
  - [高达模型](../Page/高达模型.md "wikilink")

## 外部連結

  - [Gundam Perfect Web（日本機動戰士官方網站）](http://www.gundam.channel.or.jp/)
  - [Gundam World
    Web（香港機動戰士官方網站）](https://web.archive.org/web/20060830174702/http://www.g-world.com.hk/)
  - [Gundam World Website（鋼彈官方網站（台灣））](http://www.g-world.com.tw//)
  - [Gundam war官方網站](http://www.carddas.com/cdmasters/gundamwar/)
  - [公式ガンダム情報ポータルサイト「GUNDAM.INFO」](http://www.gundam.info/)
  - \[<http://cn.gundam.info/index/white（中国敢达官方微博>）\]

[GUNDAM](../Category/GUNDAM.md "wikilink")