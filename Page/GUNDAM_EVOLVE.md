**GUN-{}-DAM
EVOLVE**是[Sunrise與](../Page/Sunrise.md "wikilink")[BANDAI在](../Page/BANDAI.md "wikilink")2001年至2003年間所製作的實驗性3D[CG動畫短片](../Page/CG.md "wikilink")，為[GUNDAM系列作品當中第一部全電腦製作的映像作品](../Page/GUNDAM系列作品.md "wikilink")（第一部短片除外）。第一期至2003年為止共五話，於2005年8月決定追加製作至第15話，目前DVD已发售完毕。

## 概要

GUN-{}-DAM
EVOLVE本來是隨著BANDAI特定的數種PG版[鋼普拉當中附贈的映像作品](../Page/鋼普拉.md "wikilink")，以實驗各種2D與3D的動畫技術與表現技法為主要目的。各出場機體的CG原始模型來自於BANDAI的[鋼普拉設計圖](../Page/鋼普拉.md "wikilink")，並加上動畫演出所需要的修飾。在劇情方面則大多是原作沒有出現的小外傳，Evo2與Evo3還帶有一些搞笑的成分。但是隨著本系列在愛好者中打出知名度，在Evo4的GP03劇中不僅出現了新設定，在Evo5還請[富野由悠季為新的if故事親自編劇](../Page/富野由悠季.md "wikilink")。而且也從模型附贈品轉換為實際發售的[OVA](../Page/OVA.md "wikilink")。

## 系列作品解說

### GUNDAM EVOLVE

#### GUNDAM EVOLVE 1 RX-78-2 GUNDAM

  -
    出自《[机动战士GUNDAM](../Page/机动战士GUNDAM.md "wikilink")》。在[一年战争最终决战地](../Page/一年战争.md "wikilink")[ア・バオア・クー](../Page/ア・バオア・クー.md "wikilink")，出击之前的[阿姆羅·雷回想过去](../Page/阿姆羅·雷.md "wikilink")。
    画面采用了电视版的视频剪辑片段，最后手持超级火箭筒从[白色基地出击的画面则是新绘制素材](../Page/白色基地.md "wikilink")。
    目前是系列中唯一一部非3D制作作品。
    于活动中首次放映，其后收录于[MG的购买者通过抽签派发的映像特典DVD上](../Page/MG.md "wikilink")。

##### 登場MS

  - RX-78-2 -{GUNDAM}-
  - 其他机体沿用自电视动画中。

##### 配音演员

  - [阿姆罗·雷](../Page/阿姆罗·雷.md "wikilink") -
    [古谷徹](../Page/古谷徹.md "wikilink")

  - \- [井上瑤](../Page/井上瑤.md "wikilink")

##### 制作人员

  - 監督 - [阿部雄一](../Page/阿部雄一.md "wikilink")

#### GUNDAM EVOLVE II RX-178 GUNDAM Mk-II

  -
    出自《[機動戰士Z高達](../Page/機動戰士Z高達.md "wikilink")》。描写了[高達Mk-II与RMS](../Page/RX-178.md "wikilink")-099
    RICK DIAS在宇宙空间演习的场景。
    高達Mk-II的[可动骨架設定画并非出自TV版](../Page/可动骨架.md "wikilink")，而是基于[PG（パーフェクトグレード）的CAD数据](../Page/ガンプラ#PG（パーフェクトグレード）.md "wikilink")。また[AMBAC](../Page/AMBAC.md "wikilink")（MSの四肢動作による姿勢制御）の描写が試みられている。
    于PG[高達Mk-II](../Page/RX-178.md "wikilink")（白）的首发限量版特典DVD中初次公开。

##### 登場MS

  - \-{GUNDAM}- Mk-II
  - RICK DIAS（黑）

##### 配音演员

  - [嘉美尤·維達](../Page/嘉美尤·維達.md "wikilink") -
    [飛田展男](../Page/飛田展男.md "wikilink")
  - ロベルト - [塩屋浩三](../Page/塩屋浩三.md "wikilink")
  - アストナージ・メドッソ - [拡森信吾](../Page/拡森信吾.md "wikilink")
  - [古華多羅·巴茲拿](../Page/古華多羅·巴茲拿.md "wikilink") -
    [池田秀一](../Page/池田秀一.md "wikilink")

##### 制作人员

  - 監督 - 増尾隆幸

#### GUNDAM EVOLVE 3 GF13-017NJII GOD GUNDAM

  -
    出自《[機動武闘伝Gガンダム](../Page/機動武闘伝Gガンダム.md "wikilink")》。描写了[GOD
    GUNDAM的演舞以及与](../Page/GOD_GUNDAM.md "wikilink")[ライジングガンダム的格斗场面](../Page/シャイニングガンダム#ライジングガンダム.md "wikilink")。
    作中の[MFの動きは原作の設定に基づき](../Page/モビルファイター.md "wikilink")、[JAEの](../Page/ジャパンアクションエンタープライズ.md "wikilink")[山田一善演出による演舞を](../Page/山田一善.md "wikilink")[ロトスコープしたデータが用いられている](../Page/ロトスコープ.md "wikilink")。
    初次公开于MG模型的特典中与「EVOLVE 1」同时收录。

##### 登場MF

  - ゴッドガンダム
  - ライジングガンダム

##### 配音演员

  - ドモン・カッシュ - [関智一](../Page/関智一.md "wikilink")
  - レイン・ミカムラ - [ならはしみき](../Page/ならはしみき.md "wikilink")

##### 制作人员

  - 監督 - [今西隆志](../Page/今西隆志.md "wikilink")

#### GUNDAM EVOLVE 4 RX-78 GP03 DENDROBIUM

  -
    出自《[機動戦士ガンダム0083](../Page/機動戦士ガンダム0083_STARDUST_MEMORY.md "wikilink")》。[シーマ艦隊に襲撃されたラビアンローズの防衛戦をメインに](../Page/シーマ・ガラハウ#シーマ艦隊.md "wikilink")、原作で[コウ・ウラキが搭乗する以前の](../Page/コウ・ウラキ.md "wikilink")[ガンダム試作3号機（デンドロビウム）の活躍を描く](../Page/ガンダム開発計画#ガンダム試作3号機（デンドロビウム）.md "wikilink")。
    本作のステイメンはPスペックと呼ばれるコアブロックシステムを有する物である。また、同じく登場するデンドロビウムは原作の[メカニックデザイナー](../Page/メカニックデザイン.md "wikilink")・[カトキハジメによって新たにリファインされている](../Page/カトキハジメ.md "wikilink")。
    初公開[HGUCガンダム試作](../Page/ハイグレード・ユニバーサルセンチュリー.md "wikilink")3号機デンドロビウムの初回生産分の特典DVDとして。

##### 登場MS/艦艇

  - ガンダムGP03（ステイメン、デンドロビウム）
  - ゲルググM
  - ザクIIF2型
  - 只出现在图纸上 - GP04Gガーベラ、ガンダムGP01、ガンダムGP01-Fb、ガンダムGP02
  - ラビアンローズ

##### 配音演员

  - ルセット・オデビー - [勝生真沙子](../Page/勝生真沙子.md "wikilink")
  - デフラ・カー - [浅野まゆみ](../Page/浅野まゆみ.md "wikilink")

##### 制作人员

  - 監督 - 今西隆志

#### GUNDAM EVOLVE 5 RX-93 ν GUNDAM

  -
    出自《[机动战士GUNDAM
    逆袭的夏亚](../Page/机动战士GUNDAM_逆袭的夏亚.md "wikilink")》。[νガンダムと](../Page/νガンダム.md "wikilink")[α・アジールとの戦闘を描いた作品](../Page/α・アジール.md "wikilink")。それまでの3DCG作品と違い[CGとセル画を合わせる手法で描かれた](../Page/CG.md "wikilink")。
    本作は数多く製作された本シリーズ中において、唯一ガンダムの生みの親である[富野由悠季がストーリーを書き下ろした](../Page/富野由悠季.md "wikilink")“特別版”と言うべき作品である。“映画版にのっとる必要はない”とのコンセプトで作られた本作からは後年製作された劇場版《Ζ
    GUNDAM》の萌芽を感じ取る事も出来る。
    本作品の初公開は便利店[罗森限定予約販売作として](../Page/罗森.md "wikilink")、それまでの作品と合わせた
    DVD**《GUNDAM EVOLVE +》**より。

##### 登場MS

  - ν GUNDAM
  - α・アジール
  - 杰刚

##### 配音演员

  - 阿姆罗・雷 - 古谷徹
  - 珂丝・帕拉娅 - [川村万梨阿](../Page/川村万梨阿.md "wikilink")
  - 哈撒维・诺亚 - [佐佐木望](../Page/佐佐木望.md "wikilink")

##### 制作人员

  - 剧情 - 富野由悠季
  - 監督 - 増尾隆幸

### GUNDAM EVOLVE ../Ω

#### EVOLVE../6 YMF-X000A DREADNAUGHT GUNDAM

##### 登场MS

  - 勇士 GUNDAM/X ASTRAY
  - 亥伯龙 GUNDAM（1号机）
  - 盖茨（GuAIZ）

##### 配音演员

普雷亚·雷腓力 - [小岛幸子](../Page/小島幸子.md "wikilink")

##### 制作人员

  - 导演·分镜 - 堀井慎也
  - 特别监修 - 高桥良辅

#### EVOLVE../7 XXXG-00W0 WING GUNDAM ZERO

#### EVOLVE../8 GAT-X105 STRIKE GUNDAM

记录[基拉在Z](../Page/基拉·大和.md "wikilink").A.F.T.某沙漠基地中作战的作品。Z.A.F.T.的一个秘密沙漠基地突然被[GAT-X105
突擊高達闯入](../Page/GAT-X105_突擊高達.md "wikilink")，面对这偶然的奇袭，Z.A.F.T.出动沙漠战式基恩样迎击，但最终不敌，基恩部队全军覆没。本片以Perfect
Grade的突击敢达为基准，突出PG版突击敢达的可动范围。故事中出现了突击敢达的新武器——长剑“XM 404 全垒打”。

##### 登场MS

  - 空战装突击敢达
  - GINN OCHER TYPE 沙漠战式样基恩

##### 制作人员

  - 导演·分镜 - 铃木健一
  - 配音 - 近藤英明

#### EVOLVE../9 MSZ-006 Z-GUNDAM

##### 登场MS

  - MSZ-006-P2/3C Z GUNDAM3号机P2型(Z -{GUNDAM}- Ⅲ P2 TYPE (RED Z))
  - MSZ-006-3A Z GUNDAM3号机A型(Z -{GUNDAM}- Ⅲ A TYPE (WHITE Z))
  - MSZ-006-3B Z GUNDAM3号机B型(Z -{GUNDAM}- Ⅲ B TYPE (GRAY Z))
  - QRX-006 GENMETH
  - HASTUR

##### 制作人员

  - 导演·作画：松木靖明
  - 剧情：近藤英明
  - 人物设定：安彦良和

##### 配音演员

#### EVOLVE../10 MSZ-010 ΖΖ-GUNDAM

### GUNDAM EVOLVE ../A

#### EVOLVE../11 RB-79 BALL

#### EVOLVE../12 RMS-099 RICK-DIAS

#### EVOLVE../13 RMS-108 MARASAI

#### EVOLVE../14（頑駄無 異歩流武../十四） 武者頑駄無

#### EVOLVE../15 RX-78 / MAN-03 BRAW-BRO

## 主要工作人員

原作：[矢立肇](../Page/矢立肇.md "wikilink")・[富野由悠季](../Page/富野由悠季.md "wikilink")

監督・演出
：[増尾隆幸](../Page/増尾隆幸.md "wikilink")・[今西隆志](../Page/今西隆志.md "wikilink")

畫面設計： 小倉信也・篠原隆了

CG技術統合： 松野美茂

音樂：渡辺岳夫、松山祐士／田中公平／萩田光男／池　頼広

## 各集標題

  - 第一期
      - GUN-{}-DAM EVOLVE 1 RX-78-2
        [高达](../Page/RX-78系列机动战士.md "wikilink")
      - GUN-{}-DAM EVOLVE 2 RX-178
        [鋼彈Mk-Ⅱ](../Page/RX-178系列机动战士.md "wikilink")
      - GUN-{}-DAM EVOLVE 3 GF13-017 -{zh-hans:神;zh-hk:神;zh-tw:神威;}-鋼彈
      - GUN-{}-DAM EVOLVE 4 RX-78 GP03 [鋼彈試作3號機 -{zh-hans:石斛兰;
        zh-hant:典多洛比姆;}-](../Page/GUNDAM開發計劃#RX-78GP03_"Dendrobium".md "wikilink")
      - GUN-{}-DAM EVOLVE 5 RX-93
        [ν鋼彈](../Page/RX-93系列机动战士.md "wikilink")
  - 第二期（GUN-{}-DAM EVOLVE../Ω(Omega)）
      - GUN-{}-DAM EVOLVE 6 YMF-X000A 勇士鋼彈
      - GUN-{}-DAM EVOLVE 7 XXXG-00W0 飛翼鋼彈零式
      - GUN-{}-DAM EVOLVE 8 GAT-X105
        [-{zh-hans:强袭;zh-hk:突擊;zh-tw:攻擊;}-鋼彈](../Page/GAT-X105_Strike.md "wikilink")
      - GUN-{}-DAM EVOLVE 9 MSZ-006-1/2/3
        [Z鋼彈1/2/3號機](../Page/MSZ-006系列机动战士.md "wikilink")
      - GUN-{}-DAM EVOLVE 10 MSZ-010 ZZ鋼彈
  - 第三期（GUN-{}-DAM EVOLVE../A(Alpha)）
      - GUN-{}-DAM EVOLVE 11 RB-79
        [-{zh-hans:铁球;zh-hk:鐵球;zh-tw:鋼球;}-](../Page/RB-79.md "wikilink")；G裝甲戰機
      - GUN-{}-DAM EVOLVE 12 RMS-099
        [夏亞·阿茲納布爾的故事](../Page/夏亞·阿茲納布爾.md "wikilink")（初代到Z）
      - GUN-{}-DAM EVOLVE 13 RMS-108
        [馬拉賽VS](../Page/RMS-108系列机动战士.md "wikilink").鋼彈Mk-Ⅱ+飛行裝甲
      - GUN-{}-DAM EVOLVE 14
        [SD武者高达](../Page/SD_GUNDAM系列.md "wikilink")
      - GUN-{}-DAM EVOLVE 15 RX-78-2
        重新演绎的[初代第](../Page/機動戰士GUNDAM.md "wikilink")39话部分内容，所有机体均经过了几乎无视原设定的再设计

## 外部連結

[GUNDAM EVOLVE官方網頁](http://www.gundam-evolve.net/)

[Evolve](../Category/GUNDAM系列.md "wikilink")