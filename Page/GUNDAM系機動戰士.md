**GUNDAM系機動戰士**的起源是為[機動戰士GUNDAM中出現的](../Page/機動戰士GUNDAM.md "wikilink")[RX-78-2](../Page/RX-78系列機動戰士.md "wikilink")。這機體使用-{**GUNDAM**}-作為名稱，是因為原作者[富野由悠季原來的企劃想把作品名稱稱為GUNBOY](../Page/富野由悠季.md "wikilink")（槍小子）然後在看到英文的DAM（水壩）後覺得有著能夠堅強不屈的精神，所以更改作品名稱為GUNDAM\[1\]，而主角所使用的機體（RX-78-2）名稱亦跟隨著改變。

## 定義

現在並沒有一個實質的官方定義，但是根據原作者富野由悠季的說法，只要有雙眼和有天線的就會被稱為GUNDAM。

但冠上「GUNDAM」名的機體通常有以下特徵：
\*高性能，先進（和當代同期機體比較）。

  - 多為試作機，先行量產機或是ACE專用機，換言之就是數量稀少。
  - 頭部有獨特的設計，主要有雙眼作為副光學感應器（主光學感應器實際上是額頭上的四方格），以及「V」字型天線，以及下巴處的突起物。
  - 在故事的結束（戰爭的結束），幾乎所有的機體都會嚴重受損或者由主角本身親自破壞。

當然在各系列中這些特徵都會有一定程度的改變。最極端的例子為以「鬍子」取天線而代之的[∀GUNDAM](../Page/∀GUNDAM.md "wikilink")，除那之外也有擁有俗稱「Z臉」的獨特設計的Zeta
GUNDAM、在天線基部搭載光束兵器的ZZ GUNDAM、應急修理而裝上吉姆頭的陸戰型GUNDAM、也有當地修改而廢除金屬天線的GUNDAM
Ez8這樣的例子。

不過在[機動戰士V GUNDAM中一開始的Victory](../Page/機動戰士V_GUNDAM.md "wikilink")
GUNDAM除了天線外都不成立，到了換V2才有新型強力機體的感覺。

在九零年代中期之後的作品不只是主角能開GUNDAM，可能連敵方甚至不重要的小角色都會開GUNDAM，其中以向港漫港片致敬的G
GUNDAM最為極端。

在GUNDAM類的一連相承機體之中，也有不含「GUNDAM」之名的機體。在GUNDAM開發計畫之中開發但為了掩飾其來歷連外觀都改變的Gerbera
Tetra（GUNDAM試作４號機）和，在Z GUNDAM開發的過程中製作的百式（δ
GUNDAM）等的例子。力克·迪亞斯雖當代為初次以「Gundarium
γ」為素材使用的MS而預定叫做「γ GUNDAM」，但除了素材以外和GUNDAM的共通點稀少的關係，因克瓦多羅·巴吉納的提案而改名。

## GUNDAM的設計

“機動戰士GUNDAM”的企劃到確定為止，為了主角機商討了各式各樣的機體，例如以強化服為設計的最初期的提案為後續的鋼加農所採用。在GUNDAM的設計確定前的階段，有在GUNDAM上設計類似人類的「嘴巴」。附帶一提這之後製作的“[戰國魔神豪將軍](../Page/戰國魔神豪將軍.md "wikilink")”、“[宇宙戦士バルディオス](../Page/宇宙戦士バルディオス.md "wikilink")”的嘴巴都做成類似GUNDAM的形狀。
[RX-78-2](../Page/RX-78系列機動戰士.md "wikilink")
GUNDAM的設計，可說是擔任從主角級的[超級機器人到兵器級的](../Page/超級機器人.md "wikilink")[寫實機器人的橋樑](../Page/寫實機器人.md "wikilink")。例如類似人類眼睛的雙感應器或副攝影機，明顯受到[鐵人28號之後的主角機器人的影響](../Page/鐵人28號.md "wikilink")。承襲[宇宙超人](../Page/宇宙超人.md "wikilink")、[宇宙鋼人在鎧盔前的裝飾品為基本的角狀天線也是](../Page/宇宙鋼人.md "wikilink")[超級機器人所必備的](../Page/超級機器人.md "wikilink")。另外在白色為底色配上重點式的光的三原色藍、紅、黃的顏色設計，是追求更像兵器的青一色白的工作人員，和要求要像機器人卡通的顏色設計（GUNDAM之前的機器人顏色以紅、藍、黃構成而稱作「機器人三原色」）的出資者（玩具公司等）僵持下所產生的。
“機動戰士GUNDAM”的電影或[鋼普拉因在結果上得到了大成功](../Page/鋼普拉.md "wikilink")，「兩眼、有角」的設計（加上從以前的「人型、五指」）在不論超級機器人、寫實機器人都成為「主角的機器人」的典型設計而定型。另外顏色設計也漸漸不被機器人三原色所束縛。而不拘泥在這之上的主角機器人也多數登場在後續的作品裡。
另外在「GUNDAM系列」，以宇宙世紀為舞台描寫後續的作品中冠上「GUNDAM」之名的機體，對「以傳說的名機GUNDAM為精神而製成」這一點不會特別感到不自然。但是那之後，宇宙世紀以外的世界觀為舞台製作的作品中也完全同樣擁有「GUNDAM」的稱號、「兩眼、有角、白底上配三原色」的「Mobile
Suit」會登場說起來奇怪是很奇怪。雖然這本身以主要資助者[萬代想把](../Page/萬代.md "wikilink")「GUNDAM」作大的意圖影響很大，但理所當然的在迷之間引起了「那些是否也算正當的GUNDAM嗎」的世論。對於這點著手初期的GUNDAM系列的[富野由悠季自身以](../Page/富野由悠季.md "wikilink")“[∀GUNDAM](../Page/∀GUNDAM.md "wikilink")”將這些全部以「[黑歷史](../Page/黑歷史.md "wikilink")」的形式整合。

富野監督以原作者搭上的漫畫讓登場“[機動戰士海盜GUNDAM](../Page/機動戰士海盜GUNDAM.md "wikilink")”的人物「機體只要有兩支眼、長天線的話就會說是GUNDAM」這麼的說了。

## 各系列GUNDAM中的解釋

### [宇宙世紀](../Page/宇宙世紀_\(GUNDAM\).md "wikilink")（Universal Century(U.C.)）

在描寫『機動戰士GUNDAM』以後的宇宙世紀作品中，GUNDAM以特別象徵優秀的試作MS。特別是地球聯邦軍的技術象徵，更也是聯邦軍的象徵，例如迪坦斯把GUNDAM
MK-II當成讓自己正當化的手段使用。之後的Z GUNDAM還是GUNDAM F91等劃世紀的MS，也因外表上和1RX-78-2
GUNDAM有共通點而繼承其名稱。

  - **G**eneral-purpose **U**tility **N**on-**D**iscontinuity
    **A**ugmentation **M**aneuvering weapon system（全領域泛用增強機動兵器）
  - 這個解釋是在後來追加，首先於《**-{GUNDAM}- WARS I PROJECT
    Z**》出現。（同時因此令到月神鈦更名為Gundarium
    Alpha，使用後來使用Gundarium Gamma的Z Project機體全部都被稱為GUNDAM，而不一定有雙眼和V型天線）

### 未來紀元（Future Century(F.C.)，《[機動武鬥傳G GUNDAM](../Page/機動武鬥傳G_GUNDAM.md "wikilink")》）

  - 原本為**G**amma **UN**ificational **D**imalium **A**malgam
    **M**obile-suit，所指的是使用Gundalium合金：**G**amma **UN**ificational
    **D**ima**LIUM** amalgam（簡稱Gundalium）的機體。
  - 不過後來則成為了所有參加“GUNDAM Fight”的機體的統稱**G**overn of **U**niverse
    **N**ation **D**ecide **A**dvanced **M**S。

### 後殖民紀元（After Colony(A.C.)，《[新機動戰記GUNDAM W](../Page/新機動戰記GUNDAM_W.md "wikilink")》）

  - 最初是因為起初的五機使用的金屬是Gundanium，而被稱為**G**enetic on **U**niversal
    **N**eutraly **D**ifferent **A**lloy-nium **M**obile
    suit，即「Gundanium合金製MS」之意。
  - 不過後面其他使用Gundanium的機體如Virgo等則未以鋼彈冠名，也未被歸類為鋼彈系機種。

### 戰後紀元（After War(A.W.)，《[機動新世紀GUNDAM X](../Page/機動新世紀GUNDAM_X.md "wikilink")》）

GUNDAM DOUBLE X的是ADVANCED TACTICAL MOBILE SUIT SYSTEM

### 正曆（Correct Century(C.C.)《[∀GUNDAM](../Page/∀GUNDAM.md "wikilink")》）

  - GUNDAM的稱呼只是因為有角色在看到∀的時候脫口而出喊出「GUNDAM」而就成為了∀GUNDAM。

### [宇宙紀元](../Page/宇宙紀元.md "wikilink")（Cosmic Era(C.E.)，《[機動戰士GUNDAM SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")》系列）

C.E.世界觀裡的鋼彈系MS的定義主要在於OS的全名首字母縮寫為G.U.N.D.A.M，而符合該條件的MS通常也是以V字天線和雙眼為特徵。

  - 地球联合軍的GAT-X系列，CAT1-X（1〜3）/3 Hyperion以及奥布的[ORB-01
    曉稱為](../Page/ORB-01_Akatsuki.md "wikilink")**G**eneral
    **U**nilateral **N**euro-link **D**ispersive **A**utonomic
    **M**aneuver。\[2\]
  - 特指地球联合军自行製作的[GFAS-X1
    Destroy稱為](../Page/GFAS-X1_Destroy.md "wikilink")：**G**igantic
    **U**nilateral **N**umerous **D**ominating **AM**munition。\[3\]
  - 由Z.A.F.T.及克莱因派開發的核能機體為：**G**eneration : **U**nsubdued **N**uclear
    **D**rive/**A**ssault **M**odule。\[4\]
  - Z.A.F.T.的第二代世代GUNDAM，采用外部氘光束供能機體ZGMF-X23S Saviour、ZGMF-X24S
    Chaos、ZGMF-X31S Abyss、ZGMF-X56S Impulse、ZGMF-X88S
    Gaia為：**G**eneration **U**nrestricted **N**etwork
    **D**rive/**A**ssault **M**odule。\[5\]\[6\]
  - Z.A.F.T.采用**核．氘**混合供能（Hyper-Deuterion Nuclear Powered）的機體ZGMF-X42S
    Destiny和ZGMF-X666S Legend為：**G**unnery **U**nited
    **N**uclear-**D**uetrion **A**dvanced **M**aneuver system。\[7\]
  - 特指[機動戰士GUNDAM SEED C.E.73
    STARGAZER中出現的](../Page/機動戰士GUNDAM_SEED_C.E.73_STARGAZER.md "wikilink")[GSX-401FW
    Stargazer為](../Page/GSX-401FW_Stargazer.md "wikilink")**G**uider
    **UN**manned **D**eployment **A**utonomic **M**anlpulation。

<small>備注：CE中的GUNDAM全寫雖然部份機體一樣，但在OS顯示時其他部份有所不同。</small>

值得一提的是，劇中除了正統的鋼彈系機種以外，也有一些不屬於鋼彈系機種、卻帶有鋼彈外觀特徵的機型，例如歐普軍的量產機MVF-M11C
村雨、地球聯合軍的量產機GAT-SO2R N刃式N。

### 西元（Anno Domini(A.D.)《[機動戰士GUNDAM 00](../Page/機動戰士GUNDAM_00.md "wikilink")》）

  - 由目前為止的劇情中僅得知GUNDAM定義為MS頭上有顯示字樣者。
  - 但从剧中准永动机GN动力装置的全称（Gundam Nuclear
    Driver）以及补充设定中以GN动力装置为动力源的巨大MA统御式隐藏着GUNDAM样式的头部来看，或许GUNDAM可以定义为使用GN动力装置或GN粒子罐为动力源的机动兵器。
  - 有說為革新世界的型像代表機體。

### 新進世代（Advanced Generation(A.G.)《[機動戰士GUNDAM AGE](../Page/機動戰士GUNDAM_AGE.md "wikilink")》）

GUNDAM在故事開始以前就持有「傳說中的救世主」的稱號。故事中菲力特以亞斯諾家代代相傳的記憶組件「AGE裝置」裡留下的資料為基礎，開發了名為GUNDAM的機動戰士。而它的胸部中央有個「A」字，是「AGE」的意思。啟動GUNDAM時需要將AGE裝置放在指定位置。

### RC世纪（Regild Century(R.C.)《[GUNDAM_G之复国运动](../Page/GUNDAM_G之复国运动.md "wikilink")》）

宇宙世纪终结后的世纪。

### 戰後世紀(Post Disaster(P.D.)《[機動戰士GUNDAM 鐵血的孤兒](../Page/機動戰士GUNDAM_鐵血的孤兒.md "wikilink")》）

  - 劇中GUNDAM定義為三百年前厄祭戰爭中所開發出來的72具使用鋼彈骨架（Gundam Frame）的機體

## GUNDAM型機種列表

### 機動戰士高達MSV

  - [初代高達](../Page/初代高達.md "wikilink")(**Prototype Gundam, Rx-78-1**)
    [全裝甲型高達](../Page/全裝甲型高達.md "wikilink")(**Gundam Full Armor Type,
    FA-78-1**)
    [水中型高達](../Page/水中型高達.md "wikilink")(**Waterproof Gundam,
    RAG-79-G1**)
    [G-3高達](../Page/G-3高達.md "wikilink")(**G-3Gundam, RX-78-3**)
    [高達6號機 泥岩](../Page/高達6號機_泥岩.md "wikilink")(**Mudrock Gundam,
    RX-78-6**)
    [ν 高達HWS](../Page/ν_高達HWS.md "wikilink")(**ν Gundam Heavy Weapons
    Type, FA-93HWS**)
    [完美高達](../Page/完美高達.md "wikilink")(**Perfect Gundam, PF-78-1**)
    [ν高達雙翼狀感應砲裝備型](../Page/ν高達雙翼狀感應砲裝備型.md "wikilink")(**ν Gundam Double
    Fin Funnel Type, RX-93**)
    [量產型ν高達](../Page/量產型ν高達.md "wikilink")(**Mass Production Type ν
    Gundam, RX-94**)
    [Hi-ν高達](../Page/Hi-ν高達.md "wikilink")(**Hi-ν Gundam, RX-93-ν-2**)
    [全裝甲型高達Mk-Ⅲ](../Page/全裝甲型高達Mk-Ⅲ.md "wikilink")(**Full Armor GUNDAM
    Mark-Ⅲ, FA-007GⅢ**)
    [高達Mk-Ⅲ](../Page/高達Mk-Ⅲ.md "wikilink")(**Gundam Mark Ⅲ, MSF-007**)
    [高達Mk-IV](../Page/高達Mk-IV.md "wikilink")(**Gundam Mark IV,
    MSF-008**)
    [試作型Z高達 X1](../Page/試作型Z高達_X1.md "wikilink")(**Prototype Z Gundam
    X1, MSZ-006-X1**)
    [試作型Z高達 X2](../Page/試作型Z高達_X2.md "wikilink")(**Prototype Z Gundam
    X2, MSZ-006-X2**)
    [試作型Z高達 X3](../Page/試作型Z高達_X3.md "wikilink")(**Prototype Z Gundam
    X3, MSZ-006-X3**)
    [量產型Z高達](../Page/量產型Z高達.md "wikilink")(**Mass Production Type Zeta
    Gundam, MSZ-007**)
    [ZⅡ](../Page/ZⅡ.md "wikilink")(**ZⅡ, MSZ-008**)
    [試作型ZZ高達](../Page/試作型ZZ高達.md "wikilink")(**DOUBLE ZETA GUNDAM
    PROTOTYPE, MSZ-009**)
    [試作型ZZ高達](../Page/試作型ZZ高達.md "wikilink")(**Prototype ZZ Gundam,
    MSZ-009B**)
    [量產型ZZ高達](../Page/量產型ZZ高達.md "wikilink")(**Mass Production Type ZZ
    Gundam, MSZ-013**)
    [高達MK-Ⅱ NT](../Page/高達MK-Ⅱ_NT.md "wikilink")(**Gundam Mk-2 NT,
    RX-178Q**)
    [德爾塔高達](../Page/德爾塔高達.md "wikilink")(**Delta Gundam, MSN-001**)
    [全裝甲型Z高達](../Page/全裝甲型Z高達.md "wikilink")(**Full Armor Zeta Gundam,
    FA-006ZG**)
    [全裝甲型高達MK-Ⅱ](../Page/全裝甲型高達MK-Ⅱ.md "wikilink")(**Full Armor Gundam
    Mk-II. FA-178**)
    [全裝甲型骷髏高達X1](../Page/全裝甲型骷髏高達X1.md "wikilink")(**Crossbone Gundam
    X-1 Full Armor, XM-X1**)
    [試作型精神感應高達](../Page/試作型精神感應高達.md "wikilink")(**Prototype Psyco
    Gundam, MRX-007**)
    [量產型精神感應高達](../Page/量產型精神感應高達.md "wikilink")(**Mass Production Type
    Psyco Gundam, MRX-011**)
    [Z高達3號機](../Page/Z高達3號機.md "wikilink")(**Zeta Gundam:Strike Zeta,
    MSZ-006-3**)

### 機動戰士高達 \[MSV-R\]

  - [全裝甲型高達B型](../Page/全裝甲型高達B型.md "wikilink")(**Full Armor Gundam Type
    B, FA-78-1B**)

### 機動戰士高達

  - [高達](../Page/高達.md "wikilink")(**Gundam, RX-78-2**)

### 機動戰士高達U.C.0079 宇宙閃光的盡頭

  - [高達4號機](../Page/高達4號機.md "wikilink")(**Gundam Unit4, RX-78-4**)
    [高達5號機](../Page/高達5號機.md "wikilink")(**Gundam Unit5, RX-78-5**)

### 機動戰士高達 第08MS小隊

  - [高達Ez-8](../Page/高達Ez-8.md "wikilink")(**Gundam Ez8,
    RX-79\[G\]Ez8**)
    [陸戰型高達](../Page/陸戰型高達.md "wikilink")(**Gundam Ground Type,
    RX-79\[G\]**)

### 機動戰士高達外傳 戰慄的蒼藍

  - [蒼藍宿命一號機](../Page/蒼藍宿命一號機.md "wikilink")(**Blue Destiny Unit 1,
    RX-79BD-1**)
    [蒼藍宿命二號機](../Page/蒼藍宿命二號機.md "wikilink")(**Blue Destiny Unit 2,
    RX-79BD-2**)
    [蒼藍宿命三號機](../Page/蒼藍宿命三號機.md "wikilink")(**Blue Destiny Unit 3,
    RX-79BD-3**)

### 機動戰士高達 0080 口袋裡的戰爭

  - [高達NT-1](../Page/高達NT-1.md "wikilink")(**Gundam Alex, RX-78NT-1**)
    [全裝甲型高達NT1](../Page/全裝甲型高達NT1.md "wikilink")(**Full Armor Gundam
    Alex, RX-78NT-1FA**)

### 機動戰士高達戰記 水天之淚

  - [高達7號機](../Page/高達7號機.md "wikilink")(**Gundam Unit 7, RX-78-7**)
    [全裝甲型高達7號機](../Page/全裝甲型高達7號機.md "wikilink")(**Full Armor 7th
    Gundam, FA-78-3**)
    [重裝全裝甲型高達7號機](../Page/重裝全裝甲型高達7號機.md "wikilink")(**Heavy Full Armor
    7th Gundam, HFA-78-7**)

### 機動戰士高達 0083 星塵的回憶

  - [高達試作1號機 積菲蘭沙斯](../Page/高達試作1號機_積菲蘭沙斯.md "wikilink")(**Gundam GP01
    "Zephyranthes", RX-78-GP01 "Zephyranthes"**)
    [高達試作1號機 全方位推進型](../Page/高達試作1號機_全方位推進型.md "wikilink")(**Gundam GP01
    Full Burnern, RX-78GP01-Fb Full Vernian**)
    [高達試作2號機 賽薩里斯](../Page/高達試作2號機_賽薩里斯.md "wikilink")(**Gundam GP02A
    "Physalis", RX-78GP02A "Physalis"**)
    [高達試作3號機 典多洛比姆](../Page/高達試作3號機_典多洛比姆.md "wikilink")(**GundamGP03
    "Dendrobium", RX-78 GP03D "Dendrobium"**)
    [高達試作3號機 典多洛比姆
    史提蒙](../Page/高達試作3號機_典多洛比姆_史提蒙.md "wikilink")(**GundamGP03
    "Dendrobium Stamen", RX-78 GP03S "Dendrobium Stamen"**)
    [高達試作4號機 卡貝拉](../Page/高達試作4號機_卡貝拉.md "wikilink")(**Gundam GP04
    "Gerbera", RX-78GP-04G "Gerbera"**)
    [高達試作0號機](../Page/高達試作0號機.md "wikilink")(**Gundam GP00 "Blossom",
    RX-78-GP00 "Blossom"**)

### Advance of Ζ: 在迪坦斯的軍旗下

  - [T-高達](../Page/T-高達.md "wikilink")(**T-Gundam, RX-78\[T\]**)
    [高達TR-1(海茲爾)狙擊機](../Page/高達TR-1\(海茲爾\)狙擊機.md "wikilink")(**Gundam
    TR-1 \[Hazel Custom\] w/Sniper Unit, RX-121-1**)
    [高達TR-1（淡褐色）副臂套裝](../Page/高達TR-1（淡褐色）副臂套裝.md "wikilink")(**Gundam
    TR-1 (Hazel Custom) w/Sub Arm Unit, RX-121-1**)
    [高達TR-1](../Page/高達TR-1.md "wikilink")(**Gundam TR-1, RX-121**)
    [高達TR-1(海茲爾
    式樣)全裝甲方案](../Page/高達TR-1\(海茲爾_式樣\)全裝甲方案.md "wikilink")(**Gundam
    TR-1 \[Hazel\] Full Armor Form, RX-121**)
    [高達TR-1(海茲爾)高機動
    方案](../Page/高達TR-1\(海茲爾\)高機動_方案.md "wikilink")(**Gundam
    TR-1\[Hazel\] High Mobility Form(aka Final Form), RX-121**)
    [高達TR-1 (海茲爾改)](../Page/高達TR-1_\(海茲爾改\).md "wikilink")(**Gundam TR-1
    \[Hazel Custom\], RX-121-1**)
    [高達TR-1(海茲爾)W
    /伊卡洛斯型](../Page/高達TR-1\(海茲爾\)W_/伊卡洛斯型.md "wikilink")(**Gundam
    TR-1\[Hazel Custom\]w/Icarus Unit, RX-121-1**)
    [高達TR-1 (海茲爾,拉赫)](../Page/高達TR-1_\(海茲爾,拉赫\).md "wikilink")(**Gundam
    TR-1 \[Hazel-Rah\], RX-121-1+FA-X29A**)
    [高達TR-1
    (海茲爾,偵察)第二型態](../Page/高達TR-1_\(海茲爾,偵察\)第二型態.md "wikilink")(**Gundam
    TR-1 \[Hazel-Rah\] Second Form, RX-121-1+FA-X29A**)
    [高達TR-1 (海茲爾
    II)三重推進器搭載型](../Page/高達TR-1_\(海茲爾_II\)三重推進器搭載型.md "wikilink")(**Gundam
    TR1(Hazel II), RX-121-2**)
    [高達TR-1
    (海茲爾,奧斯華)](../Page/高達TR-1_\(海茲爾,奧斯華\).md "wikilink")(**Gundam
    TR-1 \[Hazel Owsla\], , RX-121-2**)
    [高達TR-1 (海茲爾,奧斯華)
    巨人型](../Page/高達TR-1_\(海茲爾,奧斯華\)_巨人型.md "wikilink")(**Gundam
    TR-1 \[Hazel Owsla\] w/Gigantic Arm Unit, RX-121-1**)
    [高達TR-1 進階型](../Page/高達TR-1_進階型.md "wikilink")(**Gundam TR-1
    \[Advanced Hazel\], RX-121-2A**)
    [高達TR-1 哈慎斯利](../Page/高達TR-1_哈慎斯利.md "wikilink")(**Gundam TR-1
    \[Hyzenthlay\], RX-121-3C**)
    [TR-6高達強化型](../Page/TR-6高達強化型.md "wikilink")(**TR-6 \[Woundwort\]
    High-Speed Fighter Form, RX-124**)

### Advance of Ζ: 時代的反抗者

  - [高達(赤隼)](../Page/高達\(赤隼\).md "wikilink")(**Gundam (Kestrel,
    MSW-004**)
    [高達(赤隼)外裝裝甲裝備](../Page/高達\(赤隼\)外裝裝甲裝備.md "wikilink")(**Gundam
    (Kestrel) Armor Exterior, MSw-004**)
    [高達(赤隼)高機動外裝](../Page/高達\(赤隼\)高機動外裝.md "wikilink")(**Gundam
    (Kestrel) Maneuver Exterior, MSW-004**)
    [全裝甲型紅隼](../Page/全裝甲型紅隼.md "wikilink")(**Full-Armor Kestrel,
    MSW-004**)
    [高達(金鬃)](../Page/高達\(金鬃\).md "wikilink")(**Gundam(Gullinbursti)**)
    [高達(斯庫爾)](../Page/高達\(斯庫爾\).md "wikilink")(**Gundam(Sköll),
    ORX-009**)

### 機動戰士Z高達

  - [超級高達](../Page/超級高達.md "wikilink")(**Super Gundam,
    RX-178+FXA-05D**)
    [高達MK-Ⅱ](../Page/高達MK-Ⅱ.md "wikilink")(**Gundam Mark-Ⅱ, RX-178**)
    [Z高達](../Page/Z高達.md "wikilink")(**Zeta Gundam, MSZ-006**)
    [精神感應高達](../Page/精神感應高達.md "wikilink")(**Psycho Gundam, MRX-009**)
    [精神感應高達Mk-Ⅱ](../Page/精神感應高達Mk-Ⅱ.md "wikilink")(**Psycho Gundam Mk-Ⅱ,
    MRX-010**)

### 機動戰士高達前哨戰

  - [量產型FAZZ](../Page/量產型FAZZ.md "wikilink")(**FAZZ, FA-010-A**)
    [S高達](../Page/S高達.md "wikilink")(**S Gundam, MSA-0011**)
    [S高達（追加推進器）](../Page/S高達（追加推進器）.md "wikilink")(**S Gundam Booster
    Unit Type, MSA-0011(Bst)**)
    [Ex-S高達深度強襲型(追加推進器)](../Page/Ex-S高達深度強襲型\(追加推進器\).md "wikilink")(**Ex-S
    Gundam Booster Type, MSA-0011(Bst)303E**)
    [Ex-S高達](../Page/Ex-S高達.md "wikilink")(**Ex-S Gundam,
    MSA-0011(Est)**)
    [高達Mk-V](../Page/高達Mk-V.md "wikilink")(**Gundam Mark V, ORX-013**)

### 機動戰士ZZ高達

  - [全裝甲型ZZ高達](../Page/全裝甲型ZZ高達.md "wikilink")(**Full Armor Enhanced ZZ
    Gundam (FAZZ), FA-010S**)
    [強化型ZZ高達](../Page/強化型ZZ高達.md "wikilink")(**Enhanced DOUBLE ZETA
    GUNDAM, MSZ-010S(MSZ-010B)**)
    [ZZ高達](../Page/ZZ高達.md "wikilink")(**ZZ Gundam, MSZ-010**)

### 機動戰士高達 逆襲的夏亞

  - [ν高達](../Page/ν高達.md "wikilink")(**ν Gundam, RX-93**)

### 機動戰士高達 UNICORN

  - [獨角獸高達(獨角獸模式)](../Page/獨角獸高達\(獨角獸模式\).md "wikilink")(**Unicorn
    Gundam (unicorn mode), RX-0**)
    [獨角獸高達 (破壞模式)](../Page/獨角獸高達_\(破壞模式\).md "wikilink")(**Unicorn
    Gundam (Destroy mode), RX-0**)
    **-{zh-hk:全武裝;zh-tw:全裝甲型;zh-cn:全装甲型;}-**獨角獸高達(覺醒模式)(**Full Armor
    Unicorn Gundam(Awaken mode), RX-0**)
    **-{zh-hk:全武裝;zh-tw:全裝甲型;zh-cn:全装甲型;}-**獨角獸高達(破壞模式)(**Full Armor
    Unicorn Gundam(Destroy mode), RX-0**)
    **-{zh-hk:全武裝;zh-tw:全裝甲型;zh-cn:全装甲型;}-**獨角獸高達(獨角獸模式)(*' Full Armor
    Unicorn Gundam(Unicorn Mode), RX-0*')
    [高達 德爾塔-改](../Page/高達_德爾塔-改.md "wikilink")(**GUNDAM DELTA KAI,
    MSN-001X**)
    [獨角獸高達2號機
    報喪女妖(破壞模式)](../Page/獨角獸高達2號機_報喪女妖\(破壞模式\).md "wikilink")(**Unicorn
    Gundam 02 Banshee (Destroy mode), RX-0**)
    [獨角獸高達2號機
    報喪女妖(獨角獸模式)](../Page/獨角獸高達2號機_報喪女妖\(獨角獸模式\).md "wikilink")(**Unicorn
    Gundam 02 Banshee (Unicorn mode), RX-0**)
    [獨角獸高達2號機
    報喪女妖命運女神(獨角獸模式)](../Page/獨角獸高達2號機_報喪女妖命運女神\(獨角獸模式\).md "wikilink")(**Unicorn
    Gundam 02 Banshee Norn(unicorn mode), RX-0\[N\]**)

### 機動戰士高達DOUBLE FAKE

  - [D高達1號機](../Page/D高達1號機.md "wikilink")(**D Gundam First,
    MWS-19051G-1**)
    [D高達2號機](../Page/D高達2號機.md "wikilink")(**D Gundam Second,
    MWS-19051G-2**)
    [D高達3號機](../Page/D高達3號機.md "wikilink")(**D Gundam Third, RGX-D3**)

### 機動戰士高達 閃光的哈薩威

  - [Ξ高達](../Page/Ξ高達.md "wikilink")(**Ξ Gundam, RX-105**)
    [奧德賽高達](../Page/奧德賽高達.md "wikilink")(**Odysseys Gundam, RX-104**)

### 機動戰士高達F90

  - [F90ⅡI 高達](../Page/F90ⅡI_高達.md "wikilink")(**Gundam F90II Intercept
    Type, F90ⅡI**)
    [F90Ⅱ 高達](../Page/F90Ⅱ_高達.md "wikilink")(**Gundam F90Ⅱ, F90Ⅱ**)
    [F90 高達](../Page/F90_高達.md "wikilink")(**Gundam F90, F90**)
    [F90ⅡL 高達](../Page/F90ⅡL_高達.md "wikilink")(**Gundam F90 Long Range
    Type, F90ⅡL**)
    [F90A 高達](../Page/F90A_高達.md "wikilink")(**Gundam F90 Assault Type,
    F90A**)
    [F90D 高達](../Page/F90D_高達.md "wikilink")(**Gundam F90 Destroid Type,
    F90D**)
    [F90E 高達](../Page/F90E_高達.md "wikilink")(**Gundam F90 Reconnaissance
    Type, F90E**)
    [F90H 高達](../Page/F90H_高達.md "wikilink")(**Gundam F90 Hover Type,
    F90H**)
    [F90M 高達](../Page/F90M_高達.md "wikilink")(**Gundam F90 Marine Type,
    F90M**)
    [F90P 高達](../Page/F90P_高達.md "wikilink")(**Gundam F90 Plunge Type,
    F90P**)
    [F90S 高達](../Page/F90S_高達.md "wikilink")(**Gundam F90 Support Type,
    F90S**)
    [F90V 高達](../Page/F90V_高達.md "wikilink")(**Gundam F90 VSBR Type,
    F90V**)
    [火星獨立吉翁軍式樣F90](../Page/火星獨立吉翁軍式樣F90.md "wikilink")(**Gundam F90,
    OMS-90R**)

### 機動戰士高達影子方程式91

  - [F90ⅢY 高達](../Page/F90ⅢY_高達.md "wikilink")(**Cluster Gundam,
    F90ⅢY**)
    [新高達一號機](../Page/新高達一號機.md "wikilink")(**Neo Gundam Unit 1,
    RX-99**)
    [新高達二號機](../Page/新高達二號機.md "wikilink")(**Neo Gundam Unit 2,
    RX-99**)
    [影子高達](../Page/影子高達.md "wikilink")(**Silhouette Gundam, RX-F91**)
    [影子高達改](../Page/影子高達改.md "wikilink")(**Silhouette Gundam Kai,
    RX-F91A**)

### 機動戰士高達F91

  - [高達F91](../Page/高達F91.md "wikilink")(**Gundam F91, F-91**)

### 機動戰士海盜高達X

  - [骷髏高達X1](../Page/骷髏高達X1.md "wikilink")(**Crossbone Gundam X-1,
    XM-X1（F-97）**)
    [骷髏高達X1 改](../Page/骷髏高達X1_改.md "wikilink")(**Crossbone Gundam X-1
    Kai, XM-X1（F-97）**)
    [骷髏高達X2](../Page/骷髏高達X2.md "wikilink")(**Crossbone Gundam X-2,
    XM-X2（F-97）**)
    [骷髏高達X2 改](../Page/骷髏高達X2_改.md "wikilink")(**Crossbone Gundam X-2
    Kai, XM-X2（F-97）**)
    [骷髏高達X3](../Page/骷髏高達X3.md "wikilink")(**Crossbone Gundam X-3,
    XM-X3（F-97）**)

### 機動戰士海盜高達 骷髏之心

  - [骷髏高達X1骷髏之心(X1改．改)](../Page/骷髏高達X1骷髏之心\(X1改．改\).md "wikilink")(**Crossbone
    Gundam X-1 Kai Kai, XM-X1(XM-X4)**)
    [天草/木星高達](../Page/天草/木星高達.md "wikilink")(**Amakusa/Jupiter Gundam**)

### 機動戰士V高達

  - [V-Dash高達](../Page/V-Dash高達.md "wikilink")(**V-Dash Gundam,
    LM312V04+SD-VB03A**)
    [V高達](../Page/V高達.md "wikilink")(**Victory Gundam, LM312V04**)
    [V Hexa高達](../Page/V_Hexa高達.md "wikilink")(**V Hexa Gundam,
    LM312V06**)
    [V-Dash高達Hexa](../Page/V-Dash高達Hexa.md "wikilink")(**V-Dash Gundam
    Hexa, LM312V06+SD-VB03A**)
    [V2高達](../Page/V2高達.md "wikilink")(**Victory Two Gundam,
    LM314V21**)
    [V2暴風高達](../Page/V2暴風高達.md "wikilink")(**Victory 2 Buster Gundam,
    LM314V23**)
    [V2突擊高達](../Page/V2突擊高達.md "wikilink")(**Victory 2 Assault Gundam,
    LM314V24**)
    [V2突擊暴風型高達](../Page/V2突擊暴風型高達.md "wikilink")(**V2 Assault-Buster
    Gundam, LM314V23/24**)

### 機動戰士Z高達OVA Gundam Evolve../Ω

  - [Z高達3號機A型:白色Z](../Page/Z高達3號機A型:白色Z.md "wikilink")(**Zeta Gundam 3A
    Type, MSZ-006-3A**)
    [Z高達3號機B型:灰色Z](../Page/Z高達3號機B型:灰色Z.md "wikilink")(**Zeta Gundam 3B
    Type, MSZ-006-3B**)
    [Z高達3號機P2型:紅色Z](../Page/Z高達3號機P2型:紅色Z.md "wikilink")(**Zeta Gundam
    P2/3C Type, MSZ-006P2/3C**)

### 機動武鬥傳G高達

  - [幻想高達](../Page/幻想高達.md "wikilink")(**Mirage Gundam**)
    [密宗高達](../Page/密宗高達.md "wikilink")(**Tantra Gundam, GF11-033NNP**)
    [九龍高達](../Page/九龍高達.md "wikilink")(**Kowloon Gundam,
    GF13-001NH1**)
    [大和高達](../Page/大和高達.md "wikilink")(**Yamato Gundam, GF7-013NJ**)
    [明鏡高達](../Page/明鏡高達.md "wikilink")(**Gundam Spiegel /Shadow Gundam,
    GF13-021NG**)
    [猛獁高達](../Page/猛獁高達.md "wikilink")(**Mammoth Gundam,
    GF11-042NSB**)
    [斑馬高達](../Page/斑馬高達.md "wikilink")(**Zebra Gundam, GF13-020NK**)
    [約翰牛高達](../Page/約翰牛高達.md "wikilink")(**John Bull Gundam,
    GF13-003NEL**)
    [宙斯高達](../Page/宙斯高達.md "wikilink")(**Zeus Gundam, GF13-002NGR**)
    [玫瑰高達](../Page/玫瑰高達.md "wikilink")(**Gundam Rose, GF13-009NF**)
    [飛龍高達](../Page/飛龍高達.md "wikilink")(**Dragon Gundam, GF13-011NC**)
    [巨星高達](../Page/巨星高達.md "wikilink")(**Gundam Maxter, GF13-006NA**)
    [盟主高達](../Page/盟主高達.md "wikilink")(**Master Gundam, GF13-001NHⅡ**)
    [毒蛇高達](../Page/毒蛇高達.md "wikilink")(**Cobra Gundam, GF13-030NIN**)
    [法老高達4世](../Page/法老高達4世.md "wikilink")(**Pharaoh Gundam Ⅳ,
    GF4-001NE**)
    [高達自由](../Page/高達自由.md "wikilink")(**GundamFreedom, GF2-014NA**)
    [法老高達13世](../Page/法老高達13世.md "wikilink")(**Pharaoh Gundam ⅩⅢ,
    GF13-051NE**)
    [飛龍高達](../Page/飛龍高達.md "wikilink")(**Feilong Gundam, GF4-005NC**)
    [伐木高達](../Page/伐木高達.md "wikilink")(**Lumber Gundam, GF13-037N**)
    [阿修羅高達](../Page/阿修羅高達.md "wikilink")(**Ashura Gundam,
    GF13-041NSI**)
    [曼陀羅高達](../Page/曼陀羅高達.md "wikilink")(**Mandala Gundam,
    GF13-044NNP**)
    [諾貝爾高達](../Page/諾貝爾高達.md "wikilink")(**Nobel Gundam,
    GF13-050NSW**)
    [諾貝爾高達(狂暴模式)](../Page/諾貝爾高達\(狂暴模式\).md "wikilink")(**Nobel Gundam
    Berserker Mode, GF13-050NSWⅡ**)
    [鬥牛士高達](../Page/鬥牛士高達.md "wikilink")(**Matador Gundam,
    GF13-045NSP**)
    [骷髏高達](../Page/骷髏高達.md "wikilink")(**Skull Gundam, GF13-047NMA**)
    [鐵木真高達](../Page/鐵木真高達.md "wikilink")(**Temjin Gundam,
    GF13-053NMO**)
    [尖塔高達](../Page/尖塔高達.md "wikilink")(**Minaret Gundam, GF13-052NT**)
    [維京高達](../Page/維京高達.md "wikilink")(**Viking Gundam, GF13-012NN**)
    [尼洛斯高達](../Page/尼洛斯高達.md "wikilink")(**Neros Gundam, GF13-055**)
    [風車高達](../Page/風車高達.md "wikilink")(**Nether Gundam, GF13-066NO**)
    [蜘蛛高達](../Page/蜘蛛高達.md "wikilink")(**Arachno Gundam,
    GF13-083NCB**)
    [人魚高達](../Page/人魚高達.md "wikilink")(**Mermaid Gundam, GF13-206ND**)
    [旭日高達](../Page/旭日高達.md "wikilink")(**Rising Gundam, JMF1336R**)
    [雷霆高達](../Page/雷霆高達.md "wikilink")(**Bolt Gundam, GF13-013NR**)
    [小丑高達](../Page/小丑高達.md "wikilink")(**Jester Gundam, GF13-039NP**)
    [龍舌蘭高達](../Page/龍舌蘭高達.md "wikilink")(**Tequila Gundam,
    GF13-049NM**)
    [神高達](../Page/神高達.md "wikilink")(**God gundam, GF13-017NJⅡ**)
    [閃光高達](../Page/閃光高達.md "wikilink")(**Shining Gundam, GF13-017NJ**)
    [凱撒高達](../Page/凱撒高達.md "wikilink")(**Kaiser Gundam, GF7-021NG**)
    [萊卡高達](../Page/萊卡高達.md "wikilink")(**Leica Gundam**)
    [斯芬克斯高達](../Page/斯芬克斯高達.md "wikilink")(**Sphinx Gundam**)
    [斬術高達](../Page/斬術高達.md "wikilink")(**キリジュツ·ガンダム**)
    [斯庫德高達](../Page/斯庫德高達.md "wikilink")(**Scud Gundam**)
    [火神高達](../Page/火神高達.md "wikilink")(**Vulcan Gundam, GF1-035NGR**)
    [法老高達3世](../Page/法老高達3世.md "wikilink")(**Pharaoh Gundam Ⅲ,
    GF3-013NE**)
    [男爵高達](../Page/男爵高達.md "wikilink")(**Baron Gundam, GF5-026NF**)
    [旋風高達](../Page/旋風高達.md "wikilink")(**Gundam Tornado, GF6-021NI**)
    [哥薩克高達](../Page/哥薩克高達.md "wikilink")(**Cossack Gundam,
    GF8-011NR**)
    [笑傲江湖高達](../Page/笑傲江湖高達.md "wikilink")(**Walter Gundam**)
    [終極高達→魔鬼高達](../Page/終極高達→魔鬼高達.md "wikilink")(**Ultimate Gundam→Devil
    Gundam/Dark Gundam, JDG00X**)
    [巨型盟主高達](../Page/巨型盟主高達.md "wikilink")(**Grand Master Gundam**)

### 新機動戰記W

  - [飛翼高達](../Page/飛翼高達.md "wikilink")(**Wing Gundam, XXXG-01W**)
    [死神高達](../Page/死神高達.md "wikilink")(**Gundam Deathscythe,
    XXXG-01D**)
    [神龍高達](../Page/神龍高達.md "wikilink")(**Shenlong Gundam, XXXG-01S**)
    [飛翼高達零式](../Page/飛翼高達零式.md "wikilink")(**Wing Gundam Zero,
    XXXG-00W0**)
    [雙頭龍高達](../Page/雙頭龍高達.md "wikilink")(**Altron Gundam, XXXG-01S2**)
    [地獄死神高達](../Page/地獄死神高達.md "wikilink")(**Gundam Deathscythe Hell,
    XXXG-01D2**)
    [重武裝高達](../Page/重武裝高達.md "wikilink")(**Gundam Heavyarms,
    XXXG-01H**)
    [沙漠高達](../Page/沙漠高達.md "wikilink")(**Gundam Sandrock, XXXG-01SR**)
    [重武裝高達改](../Page/重武裝高達改.md "wikilink")(**Gundam Heavyarms Custom,
    XXXG-01**)
    [沙漠高達改](../Page/沙漠高達改.md "wikilink")(**Gundam Sandrock Custom,
    XXXG-01SR2**)
    [次代高達](../Page/次代高達.md "wikilink")(**Gundam Epyon, OZ-13MS**)

#### 新機動戰記W G-UNIT

  - [高達傑姆納斯1號機](../Page/高達傑姆納斯1號機.md "wikilink")(**Gundam Geminass 01,
    OZX-GU01A**)
    [高達傑姆納斯2號機](../Page/高達傑姆納斯2號機.md "wikilink")(**Gundam Geminass 02,
    OZX-GU02A**)
    [音速高達L.O.](../Page/音速高達L.O..md "wikilink")(**Booster Gundam L.O.,
    OZX-GU01LOB**)
    [幻象高達](../Page/幻象高達.md "wikilink")(**Griepe Gundam, OZ-19MASX**)
    [海德拉高達](../Page/海德拉高達.md "wikilink")(**Hydra Gundam, OZ-15AGX**)
    [高達班雷普歐斯](../Page/高達班雷普歐斯.md "wikilink")(**Gundam Aesculapius,
    OZ-10VMSX**)
    [高達巴利奧](../Page/高達巴利奧.md "wikilink")(**Gundam Burnlapius,
    OZ-10VMSX-2**)

#### 新機動戰記高達W 無盡的華爾茲

  - [飛翼高達零式(EW)](../Page/飛翼高達零式\(EW\).md "wikilink")(**Wing Gundam Zero,
    XXXG-00W0**)
    [地獄死神高達(EW)](../Page/地獄死神高達\(EW\).md "wikilink")(**Gundam
    Deathscythe Hell(EW), XXXG-01D2**)
    [哪吒高達(EW)](../Page/哪吒高達\(EW\).md "wikilink")(**Gundam Nataku(EW),
    XXXG-01S2**)
    [沙漠高達改(EW)](../Page/沙漠高達改\(EW\).md "wikilink")(**Gundam Sandrock
    Cus, XXXG-01SR2**)
    [重武裝高達改(EW)](../Page/重武裝高達改\(EW\).md "wikilink")(**Gundam Heavyarms
    Custom, XXXG-01H2**)
    [沙漠高達(EW)](../Page/沙漠高達\(EW\).md "wikilink")(**Gundam Sandrock(EW),
    XXXG-01SR**)
    [神龍高達(EW)](../Page/神龍高達\(EW\).md "wikilink")(**Shenlong Gundam(EW),
    XXXG-01S**)
    [死神高達(EW)](../Page/死神高達\(EW\).md "wikilink")(**Gundam
    Deathscythe(EW), XXXG-01D**)
    [重武裝高達(EW)](../Page/重武裝高達\(EW\).md "wikilink")(**Gundam
    Heavyarms(EW), XXXG-01H**)
    [飛翼高達(EW)](../Page/飛翼高達\(EW\).md "wikilink")(**Wing Gundam(EW),
    XXXG-01W**)

#### 新機動戰記W 敗者的榮光

  - [沙漠高達:追加裝備型(裝甲)](../Page/沙漠高達:追加裝備型\(裝甲\).md "wikilink")(**Gundam
    Sandrock ARMADILLO, XXXG-01SR**)
    [重武裝高達
    :追加裝備型(彈庫)](../Page/重武裝高達_:追加裝備型\(彈庫\).md "wikilink")(**Gundam
    Heavyarms igel, XXXG-01H**)
    [神龍高達:追加裝備型(大刀獠牙)](../Page/神龍高達:追加裝備型\(大刀獠牙\).md "wikilink")(**Shenlong
    Gundam Liaoya, XXXG-01S**)
    [次代高達 EW](../Page/次代高達_EW.md "wikilink")(**Gundam Epyon (EW)**)

### 機動新世紀高達X

  - [斑豹高達](../Page/斑豹高達.md "wikilink")(**Gundam Leopard, GT-9600**)
    [毀滅斑豹高達](../Page/毀滅斑豹高達.md "wikilink")(**Gundam Leopard Destroy,
    GT-9600-D**)
    [空中霸王高達](../Page/空中霸王高達.md "wikilink")(**Gundam Airmaster,
    GW-9800**)
    [空中霸王爆裂者高達](../Page/空中霸王爆裂者高達.md "wikilink")(**Airmaster Burst
    Gundam**)
    [高達X](../Page/高達X.md "wikilink")(**GundamX, GX-9900**)
    [高達X 分裂者](../Page/高達X_分裂者.md "wikilink")(**Gundam X Divider,
    GX-9900-DV**)
    [高達DX](../Page/高達DX.md "wikilink")(**Double X Gundam,
    GX-9901-DX**)
    [高達DX G 獵鷹](../Page/高達DX_G_獵鷹.md "wikilink")(**G Falcon,
    GS-9900**)
    [法薩可高達](../Page/法薩可高達.md "wikilink")(**Virsago Gundam, NRX-0013**)
    [法薩可高達B.C.](../Page/法薩可高達B.C..md "wikilink")(**Gundam Virsago Chest
    Break, NRX-0013-CB**)
    [安斯坦洛高達](../Page/安斯坦洛高達.md "wikilink")(**Gundam Ashtaron,
    NRX-0015**)
    [安斯坦洛高達H.C.](../Page/安斯坦洛高達H.C..md "wikilink")(**Gundam Ashtaron
    Hermit Crab, NRX-0015-HC**)

### ∀高達

  - [∀高達](../Page/∀高達.md "wikilink")(**∀ Gundam, WD-M01**)

### 機動戰士高達SEED

  - [獵殺高達](../Page/獵殺高達.md "wikilink")(**Raider Gundam, GAT-X370**)
    [瘟神高達](../Page/瘟神高達.md "wikilink")(**Calamity Gundam, GAT-X131**)
    [禁斷高達](../Page/禁斷高達.md "wikilink")(**Forbidden Gundam, GAT-X252**)
    [神盾高達](../Page/神盾高達.md "wikilink")(**Aegis Gundam, GAT-X303**)
    [閃電高達](../Page/閃電高達.md "wikilink")(**Blitz Gundam, GAT-X207**)
    [暴風高達](../Page/暴風高達.md "wikilink")(**Buster Gundam, GAT-X103**)
    [決鬥高達/決鬥高達AS](../Page/決鬥高達/決鬥高達AS.md "wikilink")(**Duel Gundam／Duel
    Gundam Assaultshroud Armor, GAT-X102**)
    [天意高達](../Page/天意高達.md "wikilink")(**Providence Gundam,
    ZGMF-X13A**)
    [嫣紅突擊高達](../Page/嫣紅突擊高達.md "wikilink")(**Strike Rouge, MBF-02**)
    [突擊高達](../Page/突擊高達.md "wikilink")(**Strike Gundam, GAT-X105**)
    [自由高達](../Page/自由高達.md "wikilink")(**Freedom Gundam, ZGMF-X10A**)
    [正義高達](../Page/正義高達.md "wikilink")(**Justice Gundam, ZGMF-X09A**)

#### 機動戰士高達SEED DESTINY

  - [混沌高達](../Page/混沌高達.md "wikilink")(**Chaos Gundam,
    ZGMF-X24S/RGX-01**)
    [大地高達](../Page/大地高達.md "wikilink")(**Gaia GUNDAM, ZGMF-X88S
    /RGX-03（連合）**)
    [深淵高達](../Page/深淵高達.md "wikilink")(**Abyss Gundam, ZGMF-X31S
    /RGX-02（連合）**)
    [毀滅高達](../Page/毀滅高達.md "wikilink")(**Destroy GUNDAM, GFAS-X1**)
    [禁斷高達(水中用)](../Page/禁斷高達\(水中用\).md "wikilink")(**Forbidden Blue,
    GAT-X255 b**)
    [威力型衝擊高達](../Page/威力型衝擊高達.md "wikilink")(**FORCE Impulse Gundam,
    ZGMF-X56S**)
    [巨劍型衝擊高達](../Page/巨劍型衝擊高達.md "wikilink")(**SWORD Impulse Gundam,
    ZGMF-X56S/β**)
    [轟擊型衝擊高達](../Page/轟擊型衝擊高達.md "wikilink")(**BLAST IMPULSE Gundam,
    ZGMF-X56S/γ**)
    [命運高達](../Page/命運高達.md "wikilink")(**Destiny Gundam, ZGMF-X42S**)
    [救世高達](../Page/救世高達.md "wikilink")(**Saviour Gundam, ZGMF-X23S**)
    [傳說高達](../Page/傳說高達.md "wikilink")(**Legend Gundam, ZGMF-X666S**)
    [曉高達](../Page/曉高達.md "wikilink")(**Akatsuki, ORB-01**)
    [突擊自由高達](../Page/突擊自由高達.md "wikilink")(**Strike Freedom Gundam,
    ZGMF-X20A**)
    [無限正義高達](../Page/無限正義高達.md "wikilink")(**Infinite Justice Gundam,
    ZGMF-X19A**)
    [嫣紅突擊高達 鳳凰裝備](../Page/嫣紅突擊高達_鳳凰裝備.md "wikilink")(**Strike Rouge
    Ootori, MBF-02+EW454F**)

#### 機動戰士高達SEED C.E.73 Stargazer

  - [漆黑突擊高達](../Page/漆黑突擊高達.md "wikilink")(**Strike Noir Gundam,
    GAT-X105E**)
    [翠綠暴風高達](../Page/翠綠暴風高達.md "wikilink")(**Verde Buster Gundam,
    GAT-X103AP**)
    [尉藍決鬥高達](../Page/尉藍決鬥高達.md "wikilink")(**Blu Duel Gundam,
    GAT-X1022**)

#### 機動戰士高達SEED ASTRAY

  - [迷惘高達紅色機](../Page/迷惘高達紅色機.md "wikilink")(**Gundam Astray red FRAME,
    MBF-P02**)
    [迷惘高達藍色機二型L](../Page/迷惘高達藍色機二型L.md "wikilink")(**Gundam Astray Blue
    Frame Second L, MBF-P03 secondL**)
    [迷惘高達藍色機二型Revise](../Page/迷惘高達藍色機二型Revise.md "wikilink")(**Gundam
    Astray Blue Frame 2nd Revise, MBF-P03R**)
    [迷惘高達藍色機](../Page/迷惘高達藍色機.md "wikilink")(**Gundam Astray blue FRAME,
    MBF-P03**)
    [迷惘高達金色機](../Page/迷惘高達金色機.md "wikilink")(**Gundam Astray Gold Frame,
    MBF-P01**)
    [強化型迷惘高達紅色機](../Page/強化型迷惘高達紅色機.md "wikilink")(**Gundam Astray Red
    Frame, MBF-P02**)
    [迷惘高達紅色機強化承載裝備](../Page/迷惘高達紅色機強化承載裝備.md "wikilink")(**Gundam Astray
    Red Frame with Power Loader, MBF-02**)
    [亥伯龍高達（1號機）](../Page/亥伯龍高達（1號機）.md "wikilink")(**HYPERION Gundam,
    CAT1-X1/3**)
    [迷惘高達藍色機二型G](../Page/迷惘高達藍色機二型G.md "wikilink")(**Gundam Astray blue
    Frame Second G**, MBF-03''')
    [勇士高達](../Page/勇士高達.md "wikilink")(**Dreadnought Gundam,
    YMF-X000A/H**)
    [重生高達](../Page/重生高達.md "wikilink")(**Regenerate Gundam,
    ZGMF-X11A**)
    [迷惘高達金色機 天](../Page/迷惘高達金色機_天.md "wikilink")(**Gundam Astray Gold
    Frame Amatsu, MBF-P01-ReAMATU**)
    [巨劍型瘟神高達](../Page/巨劍型瘟神高達.md "wikilink")(**Sword Calamity,
    GAT-X133**)
    [迷惘高達紫色機](../Page/迷惘高達紫色機.md "wikilink")(**gundam astray purple
    frame, MBF-P05**)
    [深藍禁斷高達](../Page/深藍禁斷高達.md "wikilink")(**Forbidden Blue,
    GAT-X255**)
    [閃電突擊高達](../Page/閃電突擊高達.md "wikilink")(**Lightning Strike Gundam,
    GAT-X105+P204QX**)
    [突擊高達.紅 I.W.S.P](../Page/突擊高達.紅_I.W.S.P.md "wikilink")(**STRIKE
    Rouge I.W.S.P, MBF-02 + P202QX**)
    [獵殺高達制式機](../Page/獵殺高達制式機.md "wikilink")(**Type Aero Assault RAIDER
    Full Spec, GAT-333**)
    [漆黑迷惘高達](../Page/漆黑迷惘高達.md "wikilink")(**Gundam Astray Noir,
    MBF-P0X**)

#### 機動戰士SEED C.E.73 DELTA ASTRAY

  - [逆Δ](../Page/逆Δ.md "wikilink")(**Turn DELTA, MMF-JG73L**)
    [迷惘高達紅色機火星裝甲](../Page/迷惘高達紅色機火星裝甲.md "wikilink")(**Gundam Astray Red
    Frame Mars Jacket, MBF-P02MJ**)
    [烏黑閃電高達](../Page/烏黑閃電高達.md "wikilink")(**Nero Blitz, GAT-X207SR**)
    [赭紅神盾高達](../Page/赭紅神盾高達.md "wikilink")(**Rosso Aegis Gundam,
    GAT-X303AA**)

#### 機動戰士SEED FRAME ASTRAYS

  - [迷惘藍色機三型](../Page/迷惘藍色機三型.md "wikilink")(**Gundam Astray blue Frame
    Third, MBF-P03third**)
    [雷轟高達](../Page/雷轟高達.md "wikilink")(**RAIGO Gundam, GAT-FJ108**)
    [叢雲劾 専用亥伯龍高達](../Page/叢雲劾_専用亥伯龍高達.md "wikilink")(**Gai Murakumo's
    Hyperion G, CATI-XG2/12**)
    [迷惘高達綠色機](../Page/迷惘高達綠色機.md "wikilink")(**Gundam ASTRAY GREEN
    FRAME, MBF-P04**)
    [盧卡斯專用突擊高達E](../Page/盧卡斯專用突擊高達E.md "wikilink")(**Lukas's Strike
    Gundam E, GAT-X105E**)
    [盧卡斯專用突擊高達E IWSP](../Page/盧卡斯專用突擊高達E_IWSP.md "wikilink")(**Lukas's
    Strike E IWSP, GAT-X105E+AQM/E-M1**)
    [盧卡斯專用另類試制型砲擊型突擊高達E](../Page/盧卡斯專用另類試制型砲擊型突擊高達E.md "wikilink")(**Lucas's
    Launcher Strike Gundam E, GAT-X105E+AQM/E-X03**)
    [盧卡斯專用另類試制型巨劍型突擊高達E](../Page/盧卡斯專用另類試制型巨劍型突擊高達E.md "wikilink")(**Lucas's
    Sword Strike Gundam E, GAT-X105E+AQM/E-X02**)

#### 機動戰士高達SEED VS ASTRAY

  - [迷惘高達幻惑機](../Page/迷惘高達幻惑機.md "wikilink")(**Gundam ASTRAY MIRAGE
    FRAME, MPF-P05LM**)
    [迷惘高達紅色機改](../Page/迷惘高達紅色機改.md "wikilink")(**Gundam Astray Red Frame
    Kai, MBF-P02Kai**)
    [強風突擊高達](../Page/強風突擊高達.md "wikilink")(**Gale Strike gundam,
    LG-GAT-X105**)
    [雹陣暴風高達](../Page/雹陣暴風高達.md "wikilink")(**HAIL BUSTER Gundam,
    LH-GAT-X103**)
    [暴雨決鬥高達](../Page/暴雨決鬥高達.md "wikilink")(**REGEN DUEL Gundam,
    LN-GAT-X102**)
    [旋風救世高達](../Page/旋風救世高達.md "wikilink")(**Vent Saviour Gundam,
    LV-ZGMF-X23S**)
    [吹雪天意高達](../Page/吹雪天意高達.md "wikilink")(**Nix Providence Gundam,
    LN-ZGMF-X13A**)
    [迷霧閃電高達](../Page/迷霧閃電高達.md "wikilink")(**Nebula Blitz Gundam,
    LN-GAT-X207**)
    [迷惘高達幻惑機二期型](../Page/迷惘高達幻惑機二期型.md "wikilink")(**Gundam ASTRAY
    MIRAGE FRAME 2nd ISSUE, MBF-P05LM2**)
    [迷惘高達幻惑機三期型](../Page/迷惘高達幻惑機三期型.md "wikilink")(**Gundam Astray
    Mirage Frame 3rd Issue, MBF-P05LM3**)

#### 機動戰士SEED DESTINY ASTRAY

  - [迷惘高達非規格機](../Page/迷惘高達非規格機.md "wikilink")(**Gundam Astray Out
    Frame, ZGMF - X12**)
    [混沌型衝擊高達](../Page/混沌型衝擊高達.md "wikilink")(**Chaos Impulse Gundam,
    ZGMF-X56S/δ**)
    [命運型衝擊高達](../Page/命運型衝擊高達.md "wikilink")(**Destiny Impulse Gundam,
    ZGMF-X56S/θ**)
    [深淵型衝擊高達](../Page/深淵型衝擊高達.md "wikilink")(**Abyss Impulse Gundam,
    ZGMF-X56S/ε**)
    [大地型衝擊高達](../Page/大地型衝擊高達.md "wikilink")(**Gaia Impulse,
    ZGMF-X56S/ζ**)
    [試作型救世高達+11](../Page/試作型救世高達+11.md "wikilink")(**Prototype
    Saviour+11, RGX-04**)
    [救世高達(試作型)](../Page/救世高達\(試作型\).md "wikilink")(**Prototype Saviour,
    ZGMF-YX21R\>RGX-04**)
    [傳說型衝擊高達](../Page/傳說型衝擊高達.md "wikilink")(**Legend Impulse Gundam,
    ZGMF-X56S**)
    [神兵型聖約高達(凱特·馬帝坎樣式)](../Page/神兵型聖約高達\(凱特·馬帝坎樣式\).md "wikilink")(**Divine
    Testament Gundam, ZGMF-X12A+AQM/E-X05 (RGX-00+AQM/E-X05)**)

### 機動戰士高達00

  - [量子型00高達](../Page/量子型00高達.md "wikilink")(**00 Qan\[T\],
    GNT-0000**)
    [療天使高達](../Page/療天使高達.md "wikilink")(**Raphael Gundam, CB-002**)
    [妖天使高達](../Page/妖天使高達.md "wikilink")(**Gundam Harute, GN-011**)
    [00 Raiser GN電容型](../Page/00_Raiser_GN電容型.md "wikilink")(**00 Raiser
    GN Condenser Type, GN-0000RE+GNR-010**)
    [熾天使高達II](../Page/熾天使高達II.md "wikilink")(**Seravee Gundam II,
    GN-008RE**)
    [獄天使高達](../Page/獄天使高達.md "wikilink")(**Gundam Zabanya, GN-010**)
    [力天使高達修補型](../Page/力天使高達修補型.md "wikilink")(**Gundam Dynames Repair,
    GN-002RE**)

#### 機動戰士高達00(第1季)

  - [能天使高達](../Page/能天使高達.md "wikilink")(**Gundam Exia, GN-001**)
    [能天使高達1.5](../Page/能天使高達1.5.md "wikilink")(**Gundam Exia 1.5,
    GN-001.5**)
    [力天使高達](../Page/力天使高達.md "wikilink")(**Gundam Dynames, GN-002**)
    [主天使高達](../Page/主天使高達.md "wikilink")(**Gundam Kyrios, GN-003**)
    [德天使高達](../Page/德天使高達.md "wikilink")(**Gundam Virtue, GN-005**)
    [中性高達](../Page/中性高達.md "wikilink")(**Gundam Nadleeh, GN-004**)
    [0高達](../Page/0高達.md "wikilink")(**0 Gundam, GN-000**)
    [座天使高達一型](../Page/座天使高達一型.md "wikilink")(**Gundam Throne Eins,
    GNW-001**)
    [座天使高達二型](../Page/座天使高達二型.md "wikilink")(**Gundam Throne Zwei,
    GNW-002**)
    [座天使高達三型](../Page/座天使高達三型.md "wikilink")(**Gundam Throne Drei,
    GHW-003**)

#### 機動戰士高達00(第2季)

  - [00高達](../Page/00高達.md "wikilink")(**00 Gundam, GN-0000**)
    [智天使高達](../Page/智天使高達.md "wikilink")(**Cherudim Gundam, GN-006**)
    [熾天使高達](../Page/熾天使高達.md "wikilink")(**Seravee Gundam, GN-008**)
    [墮天使高達](../Page/墮天使高達.md "wikilink")(**AriosGundam, GN-007**)
    [能天使高達修補型](../Page/能天使高達修補型.md "wikilink")(**Gundam Exia Repair,
    GN-001RE**)
    [00 Raiser](../Page/00_Raiser.md "wikilink")(**00 Raiser,
    GN-0000+GNR-010**)
    [天使長高達](../Page/天使長高達.md "wikilink")(**Seraphim Gundam, GN-009**)
    [0高達實戰配備型](../Page/0高達實戰配備型.md "wikilink")(**0 Gundam, GN-000 (TYPE
    A.C.D.)**)
    [熾天使高達GNHW/B](../Page/熾天使高達GNHW/B.md "wikilink")(**Seravee
    GundamGNHW/B, GN-008 GNHW/B**)
    [墮天使高達GNHW/M](../Page/墮天使高達GNHW/M.md "wikilink")(**Arios Gundam
    GNHW/M, GN-007GNHW/M**)
    [智天使高達GNHW/R](../Page/智天使高達GNHW/R.md "wikilink")(**Cherudim Gundam
    GNHW/R, GN-006GNHW/R**)
    [弓兵型墮天使高達](../Page/弓兵型墮天使高達.md "wikilink")(**GN-007+GNR-101A**)
    [能天使高達R2](../Page/能天使高達R2.md "wikilink")(**Gundam Exia Repair 2,
    GN-001REⅡ**)
    [再生高達/再生加農](../Page/再生高達/再生加農.md "wikilink")(**Reborns
    Gundam/Reborns Cannon, CB-0000G/C**)
    [權天使高達](../Page/權天使高達.md "wikilink")(**Arche Gundam, GNW-20000**)

#### 機動戰士高達00P

  - [秘天使高達](../Page/秘天使高達.md "wikilink")(**Gundam Rasiel, GN-XXX**)
    [正義女神高達](../Page/正義女神高達.md "wikilink")(**GUNDAM ASTRAEA,
    GNY-001**)
    [星水女神高達](../Page/星水女神高達.md "wikilink")(**GUNDAM SADALSUUD,
    GNY-002**)
    [戰車女神高達](../Page/戰車女神高達.md "wikilink")(**Gundam Abulhool,
    GHY-003**)
    [審判女神高達](../Page/審判女神高達.md "wikilink")(**Gundam Plutone,
    GNY-004**)
    [實彈型德天使高達](../Page/實彈型德天使高達.md "wikilink")(**Gundam Virtue Physical,
    GN-005/PH**)
    [月之女神高達](../Page/月之女神高達.md "wikilink")(**Gundam Artemie,
    GNY-0042-874**)
    [1高達](../Page/1高達.md "wikilink")(**1Gundam, CBY-001**)
    [量產型1.5高達](../Page/量產型1.5高達.md "wikilink")(**1.5 Gundam Type Dark,
    CB-001.5D2**)

#### 機動戰士高達00V

  - [雪崩型能天使高達](../Page/雪崩型能天使高達.md "wikilink")(**Gundam Avalanche Exia,
    GN-001/hs-A01**)
    [魚雷型力天使高達](../Page/魚雷型力天使高達.md "wikilink")(**Gundam Dynames Torpedo,
    GN-002/DG014**)
    [頭領型中性高達](../Page/頭領型中性高達.md "wikilink")(**Gundam Nadleeh Akwos,
    GN-004 /te-a02**)
    [七槍型智天使高達](../Page/七槍型智天使高達.md "wikilink")(**Cherudim Gundam SAGA,
    GN-006/SA**)
    [七劍型00高達](../Page/七劍型00高達.md "wikilink")(**00 Gundam Seven Sword/G,
    GN-0000/7S**)
    [陣風型主天使高達](../Page/陣風型主天使高達.md "wikilink")(**Gundam Kyrios Gust,
    GN-003/af-G02**)
    [屠龍聖劍型墮天使高達](../Page/屠龍聖劍型墮天使高達.md "wikilink")(**Arios Gundam
    Ascalon, GN-007/AL**)
    [亂流型座天使高達一型](../Page/亂流型座天使高達一型.md "wikilink")(**Gundam Throne Eins
    Turbulenz, GNW-001/hs-T01**)
    [熾天使高達GNHW/3G](../Page/熾天使高達GNHW/3G.md "wikilink")(**Seravee Gundam
    GNHW/3G, GN-008GNHW/3G**)
    [雪崩突進型能天使高達](../Page/雪崩突進型能天使高達.md "wikilink")(**Gundam Avalanche
    Exia', GN-001/hs-A01D**)
    [全裝甲型0高達](../Page/全裝甲型0高達.md "wikilink")(**Full-Armor 0 Gundam,
    GN-000FA**)
    [巨蜥型座天使高達](../Page/巨蜥型座天使高達.md "wikilink")(**Throne Varanus,
    GNX-509T**)
    [獵兵型權天使高達](../Page/獵兵型權天使高達.md "wikilink")(**Jagd Arche Gundam,
    GNW-20000/J**)
    [鎮魂犬高達](../Page/鎮魂犬高達.md "wikilink")(**GRM Gundam, GNZ-001**)

#### 機動戰士高達00F

  - [黑色審判女神高達](../Page/黑色審判女神高達.md "wikilink")(**Black Gundam Plutone,
    GNY-004B**)
    [正義女神高達F型](../Page/正義女神高達F型.md "wikilink")(**GUNDAM ASTRAEA TYPE F,
    GNY-001F**)
    [黑色戰車女神高達F型](../Page/黑色戰車女神高達F型.md "wikilink")(**GUNDAM ABULHOOL
    TYPE F Black Type, GNY-003F**)
    [星水女神高達F型](../Page/星水女神高達F型.md "wikilink")(**GUNDAM SADALSUUD TYPE
    F, GNY-002F**)

#### 機動戰士高達00I

  - [1.5高達](../Page/1.5高達.md "wikilink")(**1.5 Gundam, CB-001.5**)
    [雪崩突進型正義女神高達F型](../Page/雪崩突進型正義女神高達F型.md "wikilink")(**Gundam
    Avalanche Astraea Type F’, GNY-001F/hs-A01D**)

#### 機動戰士高達00V戰記

  - [主宰型療天使高達](../Page/主宰型療天使高達.md "wikilink")(**GD Raphael Gundam
    Dominions, CB-002/GD**)
    [量子型00高達全刃式](../Page/量子型00高達全刃式.md "wikilink")(**00 Qan(T) Full
    Saber, GNT-0000/FS**)
    [權天使高達三型](../Page/權天使高達三型.md "wikilink")(**Arche Gundam Drei,
    GNW-20003**)
    [能天使R3](../Page/能天使R3.md "wikilink")(**Gundam Exia Repair III,
    GN-001REIII**)

#### 機動戰士高達00I 2314

  - [鎮魂犬高達 (ELS)](../Page/鎮魂犬高達_\(ELS\).md "wikilink")(**GRM Gundam E,
    GNZ-001E (ELS)**)

### 機動戰士高達AGE

  - [高達AGE-1 基本型](../Page/高達AGE-1_基本型.md "wikilink")(**Gundam Age-1
    Normal, AGE-1**)
    [高達AGE-1 重擊型](../Page/高達AGE-1_重擊型.md "wikilink")(**Gundam AGE-1
    Titus, GUNDAM AGE-1T**)
    [高達AGE-1 速戰型](../Page/高達AGE-1_速戰型.md "wikilink")(**Gundam AGE-1
    Sparrow, GUNDAM AGE-1S**)
    [高達AGE-2 基本型](../Page/高達AGE-2_基本型.md "wikilink")(**Gundam Age-2
    Normal, AGE-2**)
    [高達AGE-2 雙炮型](../Page/高達AGE-2_雙炮型.md "wikilink")(**Gundam AGE-2
    Double Bullet, AGE-2DB**)
    [高達AGE-3 基本型](../Page/高達AGE-3_基本型.md "wikilink")(**Gundam Age-3
    Normal, AGE-3**)
    [高達AGE-3 堡壘型](../Page/高達AGE-3_堡壘型.md "wikilink")(**Gundam AGE-3
    Fortress, AGE-3F**)
    [高達AGE-3 軌道型](../Page/高達AGE-3_軌道型.md "wikilink")(**Gundam AGE-3
    Orbital, AGE-3O**)
    [高達AGE-FX](../Page/高達AGE-FX.md "wikilink")(**GUNDAM AGE-FX,
    AGE-FX**)
    [高達AGE-1 光輝型](../Page/高達AGE-1_光輝型.md "wikilink")(**Gundam AGE-1
    Gransa, AGE-1G**)
    [高達AGE-2．黑獵犬型](../Page/高達AGE-2．黑獵犬型.md "wikilink")(**Gundam AGE-2
    Dark Hound, AGE-2DH**)
    [高達列基路斯](../Page/高達列基路斯.md "wikilink")(**Gundam Legilis, xvm-fzc**)

#### 機動戰士高達AGE外傳: -Unknown Soldiers-

  - [高達AGE-1 剃刀型](../Page/高達AGE-1_剃刀型.md "wikilink")(**Gundam Age-1
    Razor, AGE-1R**)
    [高達AGE-2 雙刀型](../Page/高達AGE-2_雙刀型.md "wikilink")(**Gundam Age-2
    Double Blade, AGE-2DC**)
    [高達AGE-FX A感應砲型](../Page/高達AGE-FX_A感應砲型.md "wikilink")(**Gundam
    Age-FX (A-Funnel Equipment Type), Age-FX (A-Funnel Equipment
    Type)**)
    [高達AGE-1 突擊裝甲](../Page/高達AGE-1_突擊裝甲.md "wikilink")(**Gundam AGE-1
    Assault Jacket, AGE-1AJ/1(AGE-1AJ/2)**)

#### 機動戰士高達AGE外傳: EXA-LOG

  - [高達AGE-2 狩獵型](../Page/高達AGE-2_狩獵型.md "wikilink")(**Gundam Age-2
    Artimes, AGE-2A**)
    [高達列基路斯 R](../Page/高達列基路斯_R.md "wikilink")(**Gundam Legilis R,
    xvm-fzc-zgc**)

### 模型戰士高達模型製作家 起始G

  - [初始高達](../Page/初始高達.md "wikilink")(**Beginning Gundam, GPB-X80**)
    [初始高達G30](../Page/初始高達G30.md "wikilink")(**Beginning Gundam G30,
    GPB-X80-30F**)
    [Hi-ν高達 GPB配色](../Page/Hi-ν高達_GPB配色.md "wikilink")(**Hi-ν Gundam GPB
    COLOR, RX-93-ν2**)
    [永恆高達](../Page/永恆高達.md "wikilink")(**Forever Gundam, GPB-78-30**)

### 機動戰士高達BUILD FIGHTERS

  - [製作突擊高達全裝備型](../Page/製作突擊高達全裝備型.md "wikilink")(**Build Strike Gundam
    Full Package, GAT-X105B/FP**)
    [製作高達 Mk-II](../Page/製作高達_Mk-II.md "wikilink")(**Build Gundam Mk-II,
    RX-178B**)
    [鳳凰飛翼高達](../Page/鳳凰飛翼高達.md "wikilink")(**Wing Gundam Fenice,
    XXXG-01WF**)
    [戰國異端高達](../Page/戰國異端高達.md "wikilink")(**Sengoku Astray Gundam,
    侍ノ弐**)
    [高達X 魔王](../Page/高達X_魔王.md "wikilink")(**Gundam X Maoh, GX-9999**)

### SD高達G GENERATION

  - [重高達Mk-III](../Page/重高達Mk-III.md "wikilink")(**Psyco Gundam Mk-III,
    MRX-012**)
    [鳳凰高達](../Page/鳳凰高達.md "wikilink")(**Phoenix Gundam, GGF-001**)
    [孔雀高達](../Page/孔雀高達.md "wikilink")(**Barbatos GUNDAM, GGV-000**)
    [水瓶高達](../Page/水瓶高達.md "wikilink")(**Gundam Aquarius, OZ-14MS**)
    [黑鳳凰高達](../Page/黑鳳凰高達.md "wikilink")(**Halphas Gundam, GGH-001**)
    [魔鬼高達 jr .](../Page/魔鬼高達_jr_..md "wikilink")(**Devil Gundam Junior,
    JDG-010**)
    [鳳凰高達.零](../Page/鳳凰高達.零.md "wikilink")(**Phoenix Zero, GGS-000**)
    [高達MKIV](../Page/高達MKIV.md "wikilink")(**Gundam Mark IV,
    ORX-012/MSF-008**)
    [獨眼高達(泰拉‧斯歐諾)](../Page/獨眼高達\(泰拉‧斯歐諾\).md "wikilink")(**Tera-S\\'ono,
    LRX-066**)
    [獨眼高達(西斯奎德)](../Page/獨眼高達\(西斯奎德\).md "wikilink")(**Sisquiede,
    LRX-077**)
    [獨眼高達(戴斯帕達)](../Page/獨眼高達\(戴斯帕達\).md "wikilink")(**Dezpada,
    LRX-088**)
    [高達試作2號機 多管火箭炮型](../Page/高達試作2號機_多管火箭炮型.md "wikilink")(**Gundam
    GP02A (Type-MLRS), RX-78GP02A**)
    [高達NT-X](../Page/高達NT-X.md "wikilink")(**GUNDAM NT-X, RX-78NT-X
    (MRX-003)**)
    [高達貝菲格露](../Page/高達貝菲格露.md "wikilink")(**Gundam Belphagor,
    GB-9700**)
    [高達Ez-8 高機動型](../Page/高達Ez-8_高機動型.md "wikilink")(**Gundam Ez8 High
    Mobility Custom,RX-79Ez-8/HMC**)
    [高達Ez-8 重武裝型](../Page/高達Ez-8_重武裝型.md "wikilink")(**Gundam Ez8 Heavy
    Arms Custom, RX-79Ez-8/HAC**)

### 基連的野望

  - [吉翁高達](../Page/吉翁高達.md "wikilink")(**Zeon's Gundam, RX-78-Z1**)
    [卡斯巴爾專用高達](../Page/卡斯巴爾專用高達.md "wikilink")(*Gundam C.A. Custom,
    RX-78/C.A*')
    [高達Mk-II 0號機](../Page/高達Mk-II_0號機.md "wikilink")(**Prototype Gundam
    Mk-II, RX-178-X0**)
    [高達漢堡 1號機](../Page/高達漢堡_1號機.md "wikilink")(**don'tknow heiswho
    help2234**)

### 機動戰士鋼彈 鐵血孤兒

  - [主魔鋼彈](../Page/主魔鋼彈.md "wikilink")(**Gundam Bael, ASW-G-01**)
  - [獵魔鋼彈](../Page/獵魔鋼彈.md "wikilink")(**Gundam Barbatos, ASW-G-08**)
      - [天狼型獵魔鋼彈](../Page/天狼型獵魔鋼彈.md "wikilink")(**Gundam Barbatos
        Lupus, ASW-G-08**)
      - [天狼王型獵魔鋼彈](../Page/天狼王型獵魔鋼彈.md "wikilink")(**Gundam Barbatos
        Lupus Rex, ASW-G-08**)
  - [智魔鋼彈](../Page/智魔鋼彈.md "wikilink")(**Gundam Gusion, ASW-G-11**)
      - [重鍛型智魔鋼彈](../Page/重鍛型智魔鋼彈.md "wikilink")(**Gundam Gusion Rebake,
        ASW-G-11**)
      - [重鍛開鋒型智魔鋼彈](../Page/重鍛開鋒型智魔鋼彈.md "wikilink")(**Gundam Gusion
        Rebake Fullcity, ASW-G-11**)
  - [蔽魔鋼彈（流星號）](../Page/蔽魔鋼彈（流星號）.md "wikilink")(**Gundam
    Flauros（Ryusei-go）, ASW-G-64**)
  - [搜魔鋼彈](../Page/搜魔鋼彈.md "wikilink")(**Gundam Kimaris, ASW-G-66**)
      - [騎兵型搜魔鋼彈](../Page/騎兵型搜魔鋼彈.md "wikilink")(**Gundam Kimaris
        Trooper, ASW-G-66**)
      - [殘命鋼彈](../Page/殘命鋼彈.md "wikilink")(**Gundam Vidar, ASW-G-XX**)
      - [殘命型搜魔鋼彈](../Page/殘命型搜魔鋼彈.md "wikilink")(**Gundam Kimaris Vidar,
        ASW-G-66**)

#### 機動戰士鋼彈 鐵血孤兒 月鋼

  - [君魔鋼彈](../Page/君魔鋼彈.md "wikilink")(**Gundam Astaroth, ASW-G-29**)
      - [君魔鋼彈原貌](../Page/君魔鋼彈原貌.md "wikilink")(**Gundam Astaroth Origin,
        ASW-G-29**)
      - [復興型君魔鋼彈](../Page/復興型君魔鋼彈.md "wikilink")(**Gundam Astaroth
        Rinascimento, ASW-G-29**)
  - [情魔鋼彈](../Page/情魔鋼彈.md "wikilink")(**Gundam Vual, ASW-G-47**)
  - [幻魔鋼彈](../Page/幻魔鋼彈.md "wikilink")(**Gundam Dantalion, ASW-G-71**)

## 參照

<references/>

[Category:GUNDAM](../Category/GUNDAM.md "wikilink")

1.  『ガンダム·エイジ』(洋泉社発行・1999年), 飯塚正夫,
    *「ガンボーイ」じゃ商標的にきつかったし、もうひとつ押しが足らないっていうことで、少しいじろうってことになったんです。それで、山浦さんが「コンボイ、マンダムみたいにもう少し力強くなんないかね？」って。それで最初の「フリーダム·ファイターにもどったんですよ。フリーダムのダムなんかいいんじゃないかって。でも、そのＧＵＮＤＵＭだと、Ｕがふたつ並んで読みにくいでしょ、スペルが。そこで、地球を守るって意味もふくめてＤＡＭに変えちゃおう。これだと洪水を防ぐとか、発電するためとか、建設的な意味に使えるでしょ。*

2.

3.
4.
5.
6.  機動戰士-{高達}- SEED DESTINY{{〈}}1{{〉}}憤怒之瞳 ISBN 986-174-030-9

7.