**Gajim**
是一套採用[Jabber通訊協定的](../Page/Jabber.md "wikilink")[自由的即時通訊軟體](../Page/自由軟體.md "wikilink")，採用[GTK+套件編寫而成](../Page/GTK.md "wikilink")。它支持[Linux](../Page/Linux.md "wikilink")，[BSD及](../Page/BSD.md "wikilink")[Windows平台](../Page/Microsoft_Windows.md "wikilink")。

## 使用

Gajim計劃的目標是提供一個完整及易用的XMPP/Jabber客戶端予GTK使用者。Gajim雖然可以在[GNOME上完美地運行](../Page/GNOME.md "wikilink")，但它不是一定要求[GNOME作運行平台](../Page/GNOME.md "wikilink")。

## 軟體特色

[Server_info_Gajim.png](https://zh.wikipedia.org/wiki/File:Server_info_Gajim.png "fig:Server_info_Gajim.png")

  - 分頁聊天視窗
  - 支持小組討論 (with MUC protocol)
  - 表情圖案，個人頭像，檔案傳輸，書籤
  - 拼字檢測
  - 支持TLS及OpenPGP（並支持SSL加密）
  - 支持在維基百科，字典及搜尋引擎作出搜索
  - 支持同時使用多個帳號
  - 相容[D-BUS](../Page/D-BUS.md "wikilink")
  - [XML命令列](../Page/XML.md "wikilink")
  - OMEMO
  - HTTP file upload

Gajim具有多國語言的使用者介面，已經包括中文

## 網路特色

因為Jabber容許連結至其他服務，因此，Gajim亦能連結至 [Yahoo\!
Messenger](../Page/Yahoo!_Messenger.md "wikilink")，[AIM](../Page/AOL_Instant_Messenger.md "wikilink")，[ICQ及](../Page/ICQ.md "wikilink")[.NET
Messenger
Service網路](../Page/.NET_Messenger_Service.md "wikilink")。其他可使用此通訊閘道的服務，如[RSS及](../Page/RSS.md "wikilink")[ATOM即時新聞更新](../Page/ATOM.md "wikilink")，
[SMS短訊及天氣預報](../Page/簡訊.md "wikilink")。詳情請參看[Jabber](../Page/Jabber.md "wikilink")。

## 參看條目

  - [即時通訊軟體列表](../Page/即時通訊軟體列表.md "wikilink")
  - [即時通訊軟體比較](../Page/即時通訊軟體比較.md "wikilink")

## 外部連結

  - [Gajim官方網址](http://www.gajim.org/)

[Category:Jabber客戶端](../Category/Jabber客戶端.md "wikilink")
[Category:開放源代碼](../Category/開放源代碼.md "wikilink")
[Category:自由的即時通訊軟件](../Category/自由的即時通訊軟件.md "wikilink")