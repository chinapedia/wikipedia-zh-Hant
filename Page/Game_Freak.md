**Game
Freak**（）是[日本遊戲開發](../Page/日本.md "wikilink")、發行商。它的代表作品是「[精灵宝可梦](../Page/精灵宝可梦系列.md "wikilink")」遊戲系列。

## 概略

公司成立前是由[田尻智領導的同人誌組織](../Page/田尻智.md "wikilink")，1989年為發行它們第一款[FC遊戲](../Page/FC遊戲機.md "wikilink")[Quinty前創立](../Page/Quinty.md "wikilink")。自從1996年發售第一款[精靈寶可夢遊戲系列紅](../Page/精靈寶可夢遊戲列表.md "wikilink")、綠版本大受歡迎，自此以後集中開發該遊戲系列。此外還成立子公司[精靈寶可夢有限公司](../Page/精靈寶可夢_\(企業\).md "wikilink")。本质上Game
Freak并不是任天堂的子公司，它也为手机平台甚至其他游戏主机平台制作游戏作品。\[1\]

## 開發作品列表

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>名称</p></th>
<th><p>发行商</p></th>
<th><p>平台</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1989年</p></td>
<td><p><em><a href="../Page/Quinty.md" title="wikilink">Quinty</a></em></p></td>
<td><p>日本：<a href="../Page/南梦宫.md" title="wikilink">南梦宫</a> 北美：<a href="../Page/Hudson_Soft.md" title="wikilink">Hudson Soft</a></p></td>
<td><p>FC</p></td>
</tr>
<tr class="even">
<td><p>1991年</p></td>
<td><p><em><a href="../Page/Jerry_Boy.md" title="wikilink">Jerry Boy</a></em></p></td>
<td><p><a href="../Page/索尼.md" title="wikilink">索尼</a></p></td>
<td><p>超级任天堂</p></td>
</tr>
<tr class="odd">
<td><p>1991年</p></td>
<td><p><em><a href="../Page/耀西的蛋.md" title="wikilink">耀西的蛋</a></em></p></td>
<td><p>任天堂</p></td>
<td><p>FC<br />
Game Boy</p></td>
</tr>
<tr class="even">
<td><p>1992年</p></td>
<td><p><em><a href="../Page/神通小精靈.md" title="wikilink">神通小精靈</a></em></p></td>
<td><p><a href="../Page/Sega.md" title="wikilink">Sega</a></p></td>
<td><p>Mega Drive</p></td>
</tr>
<tr class="odd">
<td><p>1993年</p></td>
<td><p><a href="../Page/马里奥和瓦里奥.md" title="wikilink">马里奥和瓦里奥</a></p></td>
<td><p>任天堂</p></td>
<td><p>超级任天堂</p></td>
</tr>
<tr class="even">
<td><p>1994年</p></td>
<td><p><em>Nontan to Issho! Kuru-Kuru Puzzle</em></p></td>
<td><p><a href="../Page/Victor_Interactive.md" title="wikilink">Victor Interactive</a></p></td>
<td><p>Game Boy</p></td>
</tr>
<tr class="odd">
<td><p>1994年</p></td>
<td><p><em><a href="../Page/Pulseman.md" title="wikilink">Pulseman</a></em></p></td>
<td><p>世嘉</p></td>
<td><p>Mega Drive</p></td>
</tr>
<tr class="even">
<td><p>1994年</p></td>
<td><p><em>Smart Ball 2</em></p></td>
<td><p>索尼</p></td>
<td><p>超级任天堂</p></td>
</tr>
<tr class="odd">
<td><p>1994年</p></td>
<td><p><em>Nontan to Issho: Kuru Kuru Puzzle</em></p></td>
<td><p>Victor Interactive</p></td>
<td><p>超级任天堂</p></td>
</tr>
<tr class="even">
<td><p>1996年</p></td>
<td><p><a href="../Page/精靈寶可夢_紅·綠.md" title="wikilink">精靈寶可夢 紅·綠</a></p></td>
<td><p>Nintendo</p></td>
<td><p>Game Boy</p></td>
</tr>
<tr class="odd">
<td><p>1996年</p></td>
<td><p><a href="../Page/精靈寶可夢_紅·綠.md" title="wikilink">精靈寶可夢 蓝</a></p></td>
<td><p>Nintendo</p></td>
<td><p>Game Boy</p></td>
</tr>
<tr class="even">
<td><p>1996年</p></td>
<td><p><a href="../Page/精靈寶可夢_紅·綠.md" title="wikilink">精靈寶可夢 紅·蓝</a></p></td>
<td><p>Nintendo</p></td>
<td><p>Game Boy</p></td>
</tr>
<tr class="odd">
<td><p>1996年</p></td>
<td><p><em>Bazaar de Gosāru no Game de Gosāru</em></p></td>
<td><p><a href="../Page/NEC.md" title="wikilink">NEC</a></p></td>
<td><p>PC Engine</p></td>
</tr>
<tr class="even">
<td><p>1997年</p></td>
<td><p><em>Bushi Seiryūden: Futari no Yūsha</em></p></td>
<td><p><a href="../Page/T&amp;E_Soft.md" title="wikilink">T&amp;E Soft</a></p></td>
<td><p>超级任天堂</p></td>
</tr>
<tr class="odd">
<td><p>1998年</p></td>
<td><p><a href="../Page/Game_Boy_Camera.md" title="wikilink">Game Boy Camera</a></p></td>
<td><p>任天堂</p></td>
<td><p>Game Boy</p></td>
</tr>
<tr class="even">
<td><p>1998年</p></td>
<td><p><a href="../Page/精靈寶可夢_皮卡丘.md" title="wikilink">精靈寶可夢 黄</a></p></td>
<td><p>任天堂</p></td>
<td><p>Game Boy</p></td>
</tr>
<tr class="odd">
<td><p>1999年</p></td>
<td><p><a href="../Page/精靈寶可夢_金·銀.md" title="wikilink">精靈寶可夢 金·銀</a></p></td>
<td><p>任天堂</p></td>
<td><p>Game Boy Color</p></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td><p><em>Click Medic</em></p></td>
<td><p>索尼</p></td>
<td><p>PlayStation</p></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td><p><a href="../Page/精靈寶可夢_金·銀.md" title="wikilink">精靈寶可夢 水晶</a></p></td>
<td><p>任天堂</p></td>
<td><p>Game Boy Color</p></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p><a href="../Page/精靈寶可夢_紅寶石·藍寶石.md" title="wikilink">精靈寶可夢 紅寶石·藍寶石</a></p></td>
<td><p>任天堂</p></td>
<td><p>Game Boy Advance</p></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p><a href="../Page/精靈寶可夢_火紅·葉綠.md" title="wikilink">精靈寶可夢 火紅·葉綠</a></p></td>
<td><p>任天堂</p></td>
<td><p>Game Boy Advance</p></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p><a href="../Page/精靈寶可夢_紅寶石·藍寶石.md" title="wikilink">精靈寶可夢 綠寶石</a></p></td>
<td><p>Nintendo</p></td>
<td><p>Game Boy Advance</p></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p><em><a href="../Page/Drill_Dozer.md" title="wikilink">Drill Dozer</a></em></p></td>
<td><p>任天堂</p></td>
<td><p>Game Boy Advance</p></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p><a href="../Page/精靈寶可夢_鑽石·珍珠.md" title="wikilink">精靈寶可夢 鑽石·珍珠</a></p></td>
<td><p>任天堂</p></td>
<td><p>Nintendo DS</p></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p><a href="../Page/精靈寶可夢_鑽石·珍珠.md" title="wikilink">精靈寶可夢 白金</a></p></td>
<td><p>Nintendo</p></td>
<td><p>Nintendo DS</p></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p><a href="../Page/精靈寶可夢_心金·魂銀.md" title="wikilink">精靈寶可夢 心金·魂銀</a></p></td>
<td><p>任天堂</p></td>
<td><p>Nintendo DS</p></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p><a href="../Page/精灵宝可梦_黑·白.md" title="wikilink">精灵宝可梦 黑·白</a></p></td>
<td><p>任天堂</p></td>
<td><p>Nintendo DS</p></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td><p><a href="../Page/精灵宝可梦_黑2·白2.md" title="wikilink">精灵宝可梦 黑2·白2</a></p></td>
<td><p>任天堂</p></td>
<td><p>Nintendo DS</p></td>
</tr>
<tr class="odd">
<td><p>2012年</p></td>
<td><p><em><a href="../Page/HarmoKnight.md" title="wikilink">HarmoKnight</a></em></p></td>
<td><p>任天堂</p></td>
<td><p>Nintendo 3DS</p></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p><em>Pocket Card Jockey</em></p></td>
<td><p>日本：Game Freak 全球：任天堂[2]</p></td>
<td><p>Nintendo 3DS<br />
iOS<br />
Android</p></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p><a href="../Page/精灵宝可梦_X·Y.md" title="wikilink">精灵宝可梦 X·Y</a></p></td>
<td><p>任天堂</p></td>
<td><p>Nintendo 3DS</p></td>
</tr>
<tr class="even">
<td><p>2014年</p></td>
<td><p><a href="../Page/精靈寶可夢_終結紅寶石·初始藍寶石.md" title="wikilink">精靈寶可夢 終結紅寶石·初始藍寶石</a></p></td>
<td><p>任天堂</p></td>
<td><p>Nintendo 3DS</p></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td></td>
<td><p>世嘉</p></td>
<td><p>Xbox One<br />
PlayStation 4<br />
Windows</p></td>
</tr>
<tr class="even">
<td><p>2016年</p></td>
<td><p><a href="../Page/精靈寶可夢_太陽·月亮.md" title="wikilink">精靈寶可夢 太陽·月亮</a></p></td>
<td><p>任天堂</p></td>
<td><p>Nintendo 3DS</p></td>
</tr>
<tr class="odd">
<td><p>2017年</p></td>
<td><p>Giga Wrecker</p></td>
<td><p>Game Freak</p></td>
<td><p>Windows</p></td>
</tr>
<tr class="even">
<td><p>2017年</p></td>
<td><p><a href="../Page/精靈寶可夢_究極之日·究極之月.md" title="wikilink">精靈寶可夢 究極之日·究極之月</a></p></td>
<td><p>任天堂</p></td>
<td><p>Nintendo 3DS</p></td>
</tr>
<tr class="odd">
<td><p>2018年</p></td>
<td><p><a href="../Page/寶可夢探險尋寶.md" title="wikilink">寶可夢探險尋寶</a></p></td>
<td><p>The Pokémon Company</p></td>
<td><p>Nintendo Switch<br />
iOS<br />
Android</p></td>
</tr>
<tr class="even">
<td><p>2018年</p></td>
<td><p><a href="../Page/精靈寶可夢_Let&#39;s_Go！皮卡丘／Let&#39;s_Go！伊布.md" title="wikilink">精靈寶可夢 Let's Go！皮卡丘／Let's Go！伊布</a></p></td>
<td><p>任天堂</p></td>
<td><p>Nintendo Switch</p></td>
</tr>
<tr class="odd">
<td><p>2019年</p></td>
<td><p><a href="../Page/寶可夢_劍／盾.md" title="wikilink">寶可夢 劍／盾</a></p></td>
<td><p>任天堂</p></td>
<td><p>Nintendo Switch</p></td>
</tr>
</tbody>
</table>

## 主要職員

  - [杉森健](../Page/杉森健.md "wikilink")
  - [增田順一](../Page/增田順一.md "wikilink")
  - [森本茂樹](../Page/森本茂樹.md "wikilink")
  - [藤原基史](../Page/藤原基史.md "wikilink")

## 參考資料

## 外部連結

  -

  - [公司概要](https://web.archive.org/web/20070928230735/http://www.gamefreak.co.jp/information/company.html)

[Category:1989年開業電子遊戲公司](../Category/1989年開業電子遊戲公司.md "wikilink")
[Category:任天堂的部门与子公司](../Category/任天堂的部门与子公司.md "wikilink")
[Category:电子游戏开发公司](../Category/电子游戏开发公司.md "wikilink")
[Category:日本電子遊戲公司](../Category/日本電子遊戲公司.md "wikilink")
[Category:世田谷區公司](../Category/世田谷區公司.md "wikilink")
[Category:1989年日本建立](../Category/1989年日本建立.md "wikilink")

1.  [POKÉMON DEVS SWAP CUTE FOR CARNAGE WITH NEW
    GAME](http://www.ign.com/articles/2015/03/11/pokemon-devs-swap-cute-for-carnage-with-new-game).IGN.2015-03-11.\[2015-03-12\].
2.