**Gameloft
S.A.**（中文名稱**智樂**）是一家開發和發行基於行動裝置的[電子遊戲的跨國公司](../Page/電子遊戲.md "wikilink")，其總部位於[法國](../Page/法國.md "wikilink")，分公司遍佈全球。其股票在[歐洲證券交易所](../Page/歐洲證券交易所.md "wikilink")（原為[巴黎證券交易所](../Page/巴黎證券交易所.md "wikilink")）掛牌。是[育碧集團旗下的專業遊戲軟件公司](../Page/育碧.md "wikilink")。目前在10個國家有分支機構。包括美國、加拿大、法國、英國、德國、意大利、西班牙、羅馬尼亞和中國。公司除了自主開發新遊戲外，還不斷利用集團優勢將在PC遊戲業取得成功的遊戲移植成為手機遊戲。Gameloft公司於2003年開始盈利，2006年顯示其收入為9千2百萬美元，並預期2007年收入將達到1億3千萬美元。通過與大多數無線運營商、手機製造商以及發行商的在線銷售網站簽訂協議，Gameloft在75個國家都有其銷售網絡。超前的開發資源，使得Gameloft可以開發基於多種手機平台的遊戲，公司為基於[Java](../Page/Java.md "wikilink")，[BREW](../Page/BREW.md "wikilink")，[Symbian](../Page/Symbian.md "wikilink")，[Windows
Mobile](../Page/Windows_Mobile.md "wikilink"), [Windows
Store](../Page/Microsoft商店.md "wikilink")([Windows](../Page/Microsoft_Windows.md "wikilink")
10/8/8.1)，[SMS](../Page/SMS.md "wikilink")，[WAP](../Page/WAP.md "wikilink")，[iOS](../Page/iOS.md "wikilink")，[Android和](../Page/Android.md "wikilink")[I-mode系統運行的手機](../Page/I-mode.md "wikilink")，以及一些掌上遊戲設備，例如[NDS](../Page/NDS.md "wikilink")，[iPod和](../Page/iPod.md "wikilink")[PSP開發遊戲](../Page/PSP.md "wikilink")。同時還為[PALM
OS](../Page/Palm_OS.md "wikilink")。 在172期[Game
Informer杂志对Gameloft创始人Michel](../Page/Game_Informer.md "wikilink")
Guillemot的访谈中提及，开发人员十分渴望能为[Wii开发相应游戏](../Page/Wii.md "wikilink")。
目前Gameloft官方主頁上發佈為[Wii開發的遊戲包括](../Page/Wii.md "wikilink")[Block
Breaker Delux](../Page/Block_Breaker_Delux.md "wikilink")，[TV Show
King](../Page/TV_Show_King.md "wikilink")，[Wild West
Guns和](../Page/Wild_West_Guns.md "wikilink")[Midnight
Pool](../Page/Midnight_Pool.md "wikilink")。

2016年5月30日，由于敌意收购，Gameloft成为[维旺迪的全资子公司](../Page/维旺迪.md "wikilink")。

## 开发游戏列表

## 获奖

**Spike 2007电视游戏奖**\[1\]

  - 年度最佳移动设备游戏（*[Assassin's
    Creed](../Page/Assassin's_Creed.md "wikilink")（刺客信条）*)
  - 最佳移动设备动作游戏（*Assassin's Creed（刺客信条）*)
  - 最佳移动设备虚拟设计（*Assassin's Creed（刺客信条）*)
  - 最佳动作游戏（*Heroes: The Official Mobile Game*）

**Spike 2008电视游戏奖**

  - 年度最佳移动设备游戏（*The Oregon Trail（俄勒冈之旅）*)\[2\]

**[IGN](../Page/IGN.md "wikilink") 2007游戏奖**

  - 最佳战略游戏,无线设备（*Rise of the Lost Empires（失落帝国之崛起）*)\[3\]
  - 最佳故事情节,无线设备（*American Popstar: Road to Celebrity
    (美国流行明星:成名之路）*)\[4\]

**IGN 2008游戏奖:**

  - 最佳平台游戏,无线设备（*Castle of Magic（魔幻城堡）*)\[5\]
  - 最佳图像技术,无线设备（*Hero of Sparta（斯巴达勇士）*, iPhone)\[6\]
  - 最佳动作游戏,无线设备（*Hero of Sparta（斯巴达勇士）*, iPhone)\[7\]
  - 最佳赛车游戏,无线设备（*Asphalt 4: Elite Racing (狂野飚车4:精英竞速）*, iPhone)\[8\]

## 参考文献

## 外部链接

  -
{{-}}

[Category:1999年開業電子遊戲公司](../Category/1999年開業電子遊戲公司.md "wikilink")
[Gameloft](../Category/Gameloft.md "wikilink")
[Category:法國電子遊戲公司](../Category/法國電子遊戲公司.md "wikilink")
[Category:手机游戏公司](../Category/手机游戏公司.md "wikilink")
[Category:電子遊戲開發公司](../Category/電子遊戲開發公司.md "wikilink")
[Category:電子遊戲發行商](../Category/電子遊戲發行商.md "wikilink")
[Category:威望迪子公司](../Category/威望迪子公司.md "wikilink")
[Category:总部在巴黎的跨国公司](../Category/总部在巴黎的跨国公司.md "wikilink")
[Category:歐洲股份公司](../Category/歐洲股份公司.md "wikilink")

1.  [1](http://www.vga.spike.com)
2.  Video Game Awards 2008. Spike TV. [2](http://www.vga.spike.com)
3.
4.
5.
6.
7.
8.