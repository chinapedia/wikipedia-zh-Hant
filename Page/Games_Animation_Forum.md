****，簡稱**GAF**，是一個主要由[香港人管理和使用的](../Page/香港.md "wikilink")[網絡論壇](../Page/網絡論壇.md "wikilink")。這個[網站的討論內容主要是](../Page/網站.md "wikilink")[日本動畫](../Page/日本動畫.md "wikilink")、[日本漫畫和](../Page/日本漫畫.md "wikilink")[電子遊戲等](../Page/電子遊戲.md "wikilink")。隨著開站的日子愈來愈長，該站加入了更多的討論主題例如清談、[學術](../Page/學術.md "wikilink")、[娛樂](../Page/娛樂.md "wikilink")、[體育和](../Page/體育.md "wikilink")[飲食等](../Page/飲食.md "wikilink")。

## 特色

  - **GAF**和其他網絡論壇一般，讓已登記的會員在論壇內發言，不過GAF的管理比較嚴厲，例如會即時封鎖挑釁或說粗言穢語的會員\[1\]。

<!-- end list -->

  - GAF嚴禁討論[翻版](../Page/翻版.md "wikilink")[電子遊戲以及](../Page/電子遊戲.md "wikilink")「金手指」\[2\]
    的取得方法。

<!-- end list -->

  - 在討論主題中，相同主題的項目不會重複，因為該論壇要求會員先找尋最適當的主題在內討論，使到頁面比較簡潔。

<!-- end list -->

  - 該論壇有不少數的[女性會員](../Page/女性.md "wikilink")，令到論壇內的男女比例較平均。[年齡方面](../Page/年齡.md "wikilink")，該論壇偏向吸引[成年人](../Page/成年人.md "wikilink")，所以[小學生及](../Page/小學.md "wikilink")[初中生的會員數量較少](../Page/初中.md "wikilink")。
  - 不過會員的水準逐漸下降,討論風氣變得著重個人偏見,例如清談區討論政治相關議題時,開始把不同政見的人士視為五元黨(大陸五毛黨的香港版)或動漫版討論設定時缺乏搜集資料,電視遊戲指責對方有主機情結同時,卻沒有抑制自己主機情結的情感,發言語調都日漸變得晦氣和憤世嫉俗

<!-- end list -->

  - GAF部分會員曾經任職電玩雜誌的編輯，或擁有很多玩電子遊戲的經驗，因此GAF在香港的遊戲論壇之中有較高的地位。

<!-- end list -->

  - 在動漫及電子遊戲區\[3\]
    中，該版版主每日從[日本](../Page/日本.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[歐洲](../Page/歐洲.md "wikilink")、[美國等地方的大型電子遊戲](../Page/美國.md "wikilink")[網站轉貼各種遊戲的新聞資料](../Page/網站.md "wikilink")，令到GAF對電子遊戲的資訊和[香港海外的電子遊戲廠商同步](../Page/香港.md "wikilink")。

<!-- end list -->

  - 部分在該論壇活躍的會員會在很多與[動漫有關的活動中出現](../Page/動漫.md "wikilink")。\[4\]

## 歷史

**GAF**的由來和「Game Players Forum」有很大關係。Game Players
Forum，簡稱GPF，是已休刊的[香港](../Page/香港.md "wikilink")[電子遊戲](../Page/電子遊戲.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")《Gameplayers》的[網站](../Page/網站.md "wikilink")「Gameplayers.com」的[網絡論壇](../Page/網絡論壇.md "wikilink")。
Gameplayers.com[伺服器關閉之時](../Page/伺服器.md "wikilink")，一個在GPF活躍的會員建設了另一個新的論壇「DVDForum」，吸引了很多本來在GPF活動的會員。之後DVDForum的管理人不和，由當中一個名叫「Yui
Narusawa」的會員在2000年成立了GAF。

GAF成立不久，立即有大量會員加入。直到2005年，該論壇的人流才愈來愈少。GAF在2006年9月時更換了[伺服器](../Page/伺服器.md "wikilink")，但與新的網絡服務公司出現了溝通上的問題，該公司不能提供足夠的[頻寬](../Page/頻寬.md "wikilink")，所以該論壇尋找了另一間網絡服務公司。新的伺服器在9月10日完成，使得GAF重新開放。

## 重要事件

**GAF**每年都有很多事件\[5\]，以下為一些與外界有關係的重要事情。

### 2004年

  - 在2004年年尾，GAF部份版主及會員接受了[香港電台](../Page/香港電台.md "wikilink")[電視節目的訪問](../Page/電視節目.md "wikilink")，內容是有關電力對多媒體和電子遊戲發展的影響。

### 2005年

  - 2005年，[香港有一本叫做](../Page/香港.md "wikilink")《》的電子遊戲[雜誌創刊](../Page/雜誌.md "wikilink")，有會員為它增設了討論區，其後變為討論每一期在香港發售的電子遊戲雜誌質素，引起了香港電子遊戲雜誌界的關注。

<!-- end list -->

  - 原創[漫畫](../Page/漫畫.md "wikilink")《[神龍刺青](../Page/神龍刺青.md "wikilink")》在GAF內發表後十分受歡迎，後來令該故事在論壇外的名氣大增\[6\]。

### 2006年

  - 2006年6月，[雜誌](../Page/雜誌.md "wikilink")《》盜用了[日本漫畫](../Page/日本漫畫.md "wikilink")《[涼宮春日的憂鬱](../Page/涼宮春日的憂鬱.md "wikilink")》在GAF的[同人插圖](../Page/同人.md "wikilink")\[7\]。該出版社未經繪畫該插圖的GAF會員同意，而擅自採用有關圖像。其後被該會員發現此事，並發現該雜誌沒有電話號碼和出現錯誤的地址，後來經過GAF會員查出有關編輯的電話號碼。

### 2011年

  - 8月，東方報業集團入稟香港高等法院，要求一名人士就gaforum.org出現的涉嫌[誹謗原告言論賠償](../Page/誹謗.md "wikilink")。（案件編號：HCA
    1390/2011）\[8\]

## 各個討論區

**GAF**有多個不同主題的討論區，部分討論區裡有一些子討論區。

  - GAF告示版：這裡列出了主要的版規，以及封鎖會員的情況。
      - 子討論區包括：測試區、申訴及解答區。

### 動漫及電子遊戲區

  - [日本動漫畫討論版](../Page/日本動漫.md "wikilink")：
      - 主要談及在[日本或](../Page/日本.md "wikilink")[香港播放或售賣的日本動漫畫](../Page/香港.md "wikilink")，如有日本以外的動漫畫亦可。
      - 此區只是討論，會員不可以提供下載的路徑。
      - 如有談及劇情的主題即會寫明，以免令未觀看某故事的會員看了結局。

<!-- end list -->

  - [電視遊戲討論版](../Page/電視遊戲.md "wikilink")
      - 子討論區：攻略及心得交流區

<!-- end list -->

  - [街機遊戲討論版](../Page/街機遊戲.md "wikilink")
      - 子討論區：街機心得交流討論區

<!-- end list -->

  - PC[電腦遊戲討論版](../Page/電腦遊戲.md "wikilink")

<!-- end list -->

  - [電腦軟硬問題討論區](../Page/電腦.md "wikilink")：有關電腦常識或[軟件](../Page/軟件.md "wikilink")、[硬件難題的討論](../Page/硬件.md "wikilink")。

<!-- end list -->

  - 手提遊戲機討論版
      - 子討論區：攻略及心得交流區

### 其他區

  - 清談館：討論日常生活的事情或新聞，即不屬其他討論主題的都在此討論。
      - 吹水專區：會員可在這區的指定主題中發表任何文章，但不會計算文章數目，這裡有一個「搶千」的文化\[9\]。

<!-- end list -->

  - [H-GAME之部屋](../Page/H-GAME.md "wikilink")：
      - 主要是H-GAME的討論，聲稱只限18歲以上的會員使用。

<!-- end list -->

  - 收藏品討論版：
      - 主要討論[模型](../Page/模型.md "wikilink")、美少女／型男Figure及其他收藏品。亦有該版版主的模型示範。

<!-- end list -->

  - [學術](../Page/學術.md "wikilink")[文化綜合研討區](../Page/文化.md "wikilink")：
      - 討論學術界的新聞，和向他人請教[學校功課裡的難題](../Page/學校.md "wikilink")。

<!-- end list -->

  - 繼續談情：
      - 主要討論人間感情事。

<!-- end list -->

  - Cosplay[同人MIX討論區](../Page/同人.md "wikilink")：
      - 以往被放在「[日本動漫畫討論版](../Page/日本動漫.md "wikilink")」中。主要談及[角色扮演和同人漫畫等事](../Page/角色扮演.md "wikilink")。

<!-- end list -->

  - 02專區：
      - 以「靈異」事件為主題。

<!-- end list -->

  - 買賣專區：讓會員進行買賣交易。
      - 子討論區包括：水★專區、TK專區

### 娛樂興趣區

  - [娛樂資訊潮流頻道](../Page/娛樂.md "wikilink")：只包含以下4個子討論區。
      - ：主要是[電視劇集等主題](../Page/電視.md "wikilink")。

      - [電影版](../Page/電影.md "wikilink")：主要是電影等主題。

      - 潮流版：包括衫褲鞋襪等潮流主題。

      - ：主要是[音樂等主題](../Page/音樂.md "wikilink")。

<!-- end list -->

  - [照相館](../Page/照相.md "wikilink")：包括所有[照相機和拍照技術等主題](../Page/照相機.md "wikilink")。
      - 子討論區包括：照相館專用貼圖區、照相館研習區

<!-- end list -->

  - 影音館
      - 子討論區：熱賣站

<!-- end list -->

  - [體育版](../Page/體育.md "wikilink")
      - 子討論區包括：[國家隊討論區](../Page/國家.md "wikilink")、[球會討論區](../Page/球.md "wikilink")、車迷討論區

<!-- end list -->

  - [飲食版](../Page/飲食.md "wikilink")：
      - 討論[香港的飲食新聞及煮食求助](../Page/香港.md "wikilink")。

## 註解

## 外部連結

  - [Games Animation Forum](http://www.gaforum.org/)

[Category:香港網上論壇](../Category/香港網上論壇.md "wikilink")
[Category:電子遊戲網路論壇](../Category/電子遊戲網路論壇.md "wikilink")

1.  通常封鎖會員只是一段日子，如3日或7日，較嚴重的才會永久封鎖。
2.  「金手指」是指一些以非正式的方法來在[電子遊戲中取勝的手段](../Page/電子遊戲.md "wikilink")。
3.  動漫及電子遊戲區是指：日本動漫畫討論版、電視遊戲討論版、街機遊戲討論版、PC電腦遊戲討論版和手提遊戲機討論版。
4.  例如在一個[同人活動](../Page/同人.md "wikilink")「超級機械人大戰魂」[演唱會中出現](../Page/演唱會.md "wikilink")。
5.  其他論壇內的事件可參考：[GAF04大事回顧](http://www.gaforum.org/showthread.php?t=104637)、[2005年GAF大事回顧](http://www.gaforum.org/showthread.php?t=130946)
    和 [2006年GAF大事回顧](http://www.gaforum.org/showthread.php?t=149344)。
6.  《[神龍刺青](../Page/神龍刺青.md "wikilink")》後來在論壇外亦很出名，該故事的單行本創下了在一小時內售罄的記錄，以及被拍成[電影](../Page/電影.md "wikilink")。
7.  此事件發生於6月1日出版的第16期《》第66頁「動漫遊」的右上方。
8.  [《東方入稟高院禁網民誹謗》](http://orientaldaily.on.cc/cnt/news/20110818/00176_025.html)，《東方日報》，2011年8月18日。
9.  「搶千」指一個主題每有近1000篇文章時，會員便爭著發出第1000篇回應文章，成功的會員有權建立新的主題。