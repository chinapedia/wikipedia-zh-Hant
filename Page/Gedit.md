**gedit**是一个[GNOME](../Page/GNOME.md "wikilink")[桌面环境下兼容](../Page/桌面环境.md "wikilink")[UTF-8的](../Page/UTF-8.md "wikilink")[文本编辑器](../Page/文本编辑器.md "wikilink")。它简单易用，有良好的语法高亮，对中文支持很好，支持包括[GB2312](../Page/GB2312.md "wikilink")、[GBK在内的多种字符编码](../Page/GBK.md "wikilink")。gedit是一款[自由软件](../Page/自由软件.md "wikilink")。

## 特点

gedit包含[语法高亮和标签编辑多个文件的功能](../Page/语法高亮.md "wikilink")。利用[GNOME
VFS库](../Page/GNOME_VFS.md "wikilink")，它还可以编辑远程文件。它支持完整的恢复和重做系统以及查找和替换。

它还支持包括多语言[拼写检查和一个灵活的](../Page/拼写检查.md "wikilink")[插件系统](../Page/插件.md "wikilink")，可以动态地添加新特性。例如[snippets和外部程序的整合](../Page/Snippet_management.md "wikilink")。

gedit还包括一些小特性，包括行号显示，括号匹配，文本自动换行，[自動完成](../Page/自動完成.md "wikilink")，代碼折疊，批次縮排，批次註解，嵌入式終端，当前行高亮以及自动文件备份。

## 架构

由于是为[X Window
System设计](../Page/X_Window_System.md "wikilink")，它使用[GTK+](../Page/GTK+.md "wikilink")
2.0和GNOME
2.0[库](../Page/函式庫.md "wikilink")，能支援視窗桌面環境下的[拖放功能](../Page/拖放.md "wikilink")；並可在GNOME中的說明文件獲取此編輯器的使用方法。

近來以Gedit為架構基礎的新一代編輯器[gPHPedit](../Page/gPHPedit.md "wikilink")
;將提升[PHP與](../Page/PHP.md "wikilink")[HTML及](../Page/HTML.md "wikilink")[CSS開發編輯方面的支援](../Page/CSS.md "wikilink")。

## 参见

  - [文本编辑器列表](../Page/文本编辑器列表.md "wikilink")
  - [文本编辑器比较](../Page/文本编辑器比较.md "wikilink")

## 外部链接

  - [gedit主页](http://www.gnome.org/projects/gedit/)

[Category:GNOME核心应用程序](../Category/GNOME核心应用程序.md "wikilink")
[Category:使用GTK+的文本编辑器](../Category/使用GTK+的文本编辑器.md "wikilink")
[Category:用C編程的自由軟體](../Category/用C編程的自由軟體.md "wikilink")
[Category:用Python編程的自由軟體](../Category/用Python編程的自由軟體.md "wikilink")
[Category:Linux文本编辑器](../Category/Linux文本编辑器.md "wikilink")
[Category:Unix文本编辑器](../Category/Unix文本编辑器.md "wikilink")
[Category:自由文本编辑器](../Category/自由文本编辑器.md "wikilink")
[Category:OS X文本编辑器](../Category/OS_X文本编辑器.md "wikilink")
[Category:Windows文本编辑器](../Category/Windows文本编辑器.md "wikilink")
[Category:Notepad替代](../Category/Notepad替代.md "wikilink")
[Category:使用GPL许可证的软件](../Category/使用GPL许可证的软件.md "wikilink")