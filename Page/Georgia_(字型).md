[GeorgiatTmes.svg](https://zh.wikipedia.org/wiki/File:GeorgiatTmes.svg "fig:GeorgiatTmes.svg")

**Georgia**是一種[襯線字體](../Page/襯線字體.md "wikilink")，為著名[字型設計師](../Page/字型.md "wikilink")[馬修·卡特](../Page/馬修·卡特.md "wikilink")（Matthew
Carter）於1993年為[微軟所設計的作品](../Page/微軟.md "wikilink")，具有在小字下仍能清晰辨識的特性，可讀性十分優良。其命名發想自一份[小報報導在](../Page/小報.md "wikilink")[美國](../Page/美國.md "wikilink")[喬治亞州發現](../Page/喬治亞州.md "wikilink")[外星人](../Page/外星人.md "wikilink")[頭顱的測試性](../Page/頭顱.md "wikilink")[頭條](../Page/頭條.md "wikilink")。

乍看之下，Georgia與[Times New
Roman相當類似](../Page/Times_New_Roman.md "wikilink")，但它們有多處不同。首先，在相同的字号下，Georgia的字元比Times
New
Roman的字元略大；其次，Georgia的字元線條較粗，[襯線部份也比較鈍而平](../Page/襯線.md "wikilink")。另外在[數字部份也非常不同](../Page/數字.md "wikilink")，Georgia採用稱為「[不齐线数字](../Page/不齐线数字.md "wikilink")」的數字，特色在於數字會像西文[字母般有高矮大小之別](../Page/字母.md "wikilink")\[1\]。

[微軟將Georgia列入](../Page/微軟.md "wikilink")[網頁核心字型](../Page/網頁核心字型.md "wikilink")，是[視窗作業系統的內建字型之一](../Page/視窗作業系統.md "wikilink")。[蘋果電腦的](../Page/蘋果電腦.md "wikilink")[麥金塔系統之後也跟進採用Georgia作為內建字型之一](../Page/麥金塔.md "wikilink")。

使用時，需留意：[數字](../Page/數字.md "wikilink")“0”與[字母小寫](../Page/字母.md "wikilink")“o”在Georgia字體下可能很接近，乍看近乎一模一樣，使用時需特別注意。（在Georgia字體下：數字“0”比字母“o”顯示可能較寬，數字“0”較為圓形。）

## 註釋

## 外部連結

  - [Georgia字體資訊](http://www.microsoft.com/typography/fonts/font.aspx?FID=4&FNAME=Georgia)
  - [Georgia字體下載](http://prdownloads.sourceforge.net/corefonts/georgi32.exe?download)

[Category:網頁核心字型](../Category/網頁核心字型.md "wikilink") [Category:Windows
XP字體](../Category/Windows_XP字體.md "wikilink")
[Category:微软字体](../Category/微软字体.md "wikilink")
[Category:衬线字体](../Category/衬线字体.md "wikilink")

1.  另有字型的數字也採「[不齐线数字](../Page/不齐线数字.md "wikilink")」設計。