[GMailDriveScreenCapture.png](https://zh.wikipedia.org/wiki/File:GMailDriveScreenCapture.png "fig:GMailDriveScreenCapture.png")

**GMail
Drive**是過往一個附加於[微軟](../Page/微軟.md "wikilink")[Windows檔案總管的免費介殼](../Page/Windows.md "wikilink")
(Shell)
[Namespace擴展程式](../Page/命名空间.md "wikilink")，使用它可以在[工作站](../Page/工作站.md "wikilink")（[個人電腦](../Page/個人電腦.md "wikilink")）上建立一個新的[網路分享空間](../Page/網路.md "wikilink")。你需要一個[Gmail賬戶才能使用這個擴展程式](../Page/Gmail.md "wikilink")。這個擴展程式可以讓使用者利用Windows中的檔案「複製」和「貼上」命令以在你的電腦和Gmail賬戶間傳送檔案，就像Gmail帳戶空間是一個在你的區域網路中的一個[硬碟](../Page/硬碟.md "wikilink")，又名[網上硬碟](../Page/網上硬碟.md "wikilink")。

這個程式自2015年起就已停止維護。

## 功能

每當您直接拖放或複製檔案到Gmail
Drive之後，在網路上你的個人Gmail信箱中就會收到一封電子郵件，並紀錄了你的傳送時間、名稱與附加檔（就是上傳的檔案）。由於Gmail
Drive可以達到網絡Gmail
Drive容許多個使用者同時連接一個Gmail帳戶。要模擬一個多人使用的檔案[伺服器](../Page/伺服器.md "wikilink")，可以先創造一個Gmail賬戶，再把使用者名稱與密碼分發，但是這個方法不太安全，但如果將Gmail信箱中的主題名稱移除「FW：」字眼後直接寄出，對方的Gmail
Drive也會收到這檔案 。Gmail Drive會定時檢查郵件賬戶（使用Gmail搜索功能）以檢查有沒有新的檔案及重建目錄架構。

## 限制

因為GMail賬戶的收件箱會被用來儲存檔案，而[Google限制一封](../Page/Google.md "wikilink")[電子郵件的附件不得大於](../Page/電子郵件.md "wikilink")25[MB](../Page/MB.md "wikilink")，所以GMail
Drive不能接受大於25MB的文件。另外經過實地測試，雖然支援大部分的中文字，仍有少數中文無法被顯示，及每個檔案加上檔案夾在內字元上限為40個，這是因為基於GMail在設計時限制了附加檔的字元上限。而且亦因安全理由，不支援zip和exe格式（即[副檔名為zip或exe的檔案](../Page/副檔名.md "wikilink")）。

## 警告

Gmail
Drive是一個還在研究階段的[軟件](../Page/軟件.md "wikilink")，一旦Google改變Gmail的[編程介面](../Page/編程介面.md "wikilink")，Gmail
Drive即可能會無法運作；可是Gmail Drive很可能會被更新以反映這種變化。

Gmail服務條款并没有明文限制Gmail Drive的使用。

## 參看

  - [虛擬磁碟](../Page/虛擬磁碟.md "wikilink")
  - [Gmail](../Page/Gmail.md "wikilink")
  - [GmailFS](../Page/GmailFS.md "wikilink")
  - [RoamDrive](http://www.roamdrive.com/) 與Gmail drive並不相容。

## 外部链接

  - [Gmail Drive網頁](http://www.viksoe.dk/code/gmail.htm)
  - [Gmail
    Drive繁體中文化版本](http://www.softking.com.tw/soft/clickcount.asp?fid3=22521)
  - [Gmail](https://gmail.google.com/)
  - [GmailFS](http://arquivo.pt/wayback/20090707151753/http%3A//richard.jones.name/google%2Dhacks/gmail%2Dfilesystem/gmail%2Dfilesystem.html)
    - 與Gmail Drive一樣功能的軟件，在GNU/Linux運行。

[Category:Google](../Category/Google.md "wikilink")
[Category:免費軟件](../Category/免費軟件.md "wikilink")
[Category:网络软件](../Category/网络软件.md "wikilink")