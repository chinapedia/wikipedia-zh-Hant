**gnuplot**是一套[跨平臺的數學繪圖](../Page/跨平臺.md "wikilink")[自由軟體](../Page/自由軟體.md "wikilink")。使用[交互式介面](../Page/交互式.md "wikilink")，可以繪製數學函數圖形，也可以從純文字檔讀入簡單格式的座標資料，繪製統計圖表等等。它不是統計軟體，也不是數學軟體，它純粹只是一套函數／資料繪圖軟體。它可以產生[PNG](../Page/PNG.md "wikilink")，[SVG](../Page/SVG.md "wikilink")，[PS](../Page/PostScript.md "wikilink")，HPGL，……等等開放的圖形檔案格式的輸出，供文書處理／簡報／試算表／……等等軟體匯入。

gnuplot是有版权的，但自由分发；无须付费。

功能：

  - 繪畫[二維或](../Page/二維.md "wikilink")[三維的圖像](../Page/三維.md "wikilink")
  - 繪畫[數學函數](../Page/數學函數.md "wikilink")
  - 從其他文檔讀入數據，繪畫[統計圖表](../Page/統計圖.md "wikilink")
  - 被外部程式（如[GNU Octave](../Page/GNU_Octave.md "wikilink")）調用

## 脚注

## 外部連結

  - [gnuplot homepage](http://www.gnuplot.info/)

  - [Gnuplot
    導讀](https://web.archive.org/web/20050405091640/http://phi.sinica.edu.tw/aspac/reports/95/95006/)

  - [地圖／統計圖／3d 函數圖／實驗報告圖 -- Gnuplot
    純畫圖](https://web.archive.org/web/20111231052553/http://people.ofset.org/~ckhung/b/ma/gnuplot.php)

[Category:数学软件](../Category/数学软件.md "wikilink")
[Category:SourceForge专案](../Category/SourceForge专案.md "wikilink")
[Category:免費軟件](../Category/免費軟件.md "wikilink")
[Category:自由圖表軟件](../Category/自由圖表軟件.md "wikilink")
[Category:圖表軟件](../Category/圖表軟件.md "wikilink")