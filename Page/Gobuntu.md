**Gobuntu**是[Ubuntu](../Page/Ubuntu.md "wikilink")[操作系统的一个官方衍生版](../Page/操作系统.md "wikilink")，目的是提供一个完全由[自由软件组成的发行版](../Page/自由软件.md "wikilink")。

## 历史和开发进程

[马克·沙特尔沃思](../Page/马克·沙特尔沃思.md "wikilink")（Mark
Shuttleworth）在2005年11月24日首先提出要建立一个名叫*Gnubuntu*的完全由自由软件组成的Ubuntu衍生版。\[1\]
由于[Richard
Stallman不赞成这个名字](../Page/Richard_Stallman.md "wikilink")，因此这个项目后来改名为*Ubuntu-libre*.\[2\]
Stallman曾经支持过一个名叫[gNewSense的基于Ubuntu的发行版](../Page/gNewSense.md "wikilink")，并且批评Ubuntu在后续的发行版中使用了有版权的和非自由的软件，尤指Ubuntu
7.04。\[3\]

在介绍Ubuntu 7.10时，Mark Shuttleworth表示它将Gobuntu在2007年7月10日由Mark
Shuttleworth正式发布\[4\]，并且Gobuntu
7.10的[每日构建开始公开发行](../Page/每日构建.md "wikilink")。最初的版本，Gobuntu
7.10，於2007年10月18日發行，但只提供文字安裝介面，第一個預期的長期支援版本代號為“Hardy Heron”。

## 参见

  - [Ubuntu](../Page/Ubuntu.md "wikilink")
  - [gNewSense](../Page/gNewSense.md "wikilink")

## 注释

## 外部链接

  - [Gobuntu主页](https://web.archive.org/web/20080516115000/http://www.ubuntu.com/products/whatisubuntu/gobuntu)
  - [Gobuntu page in Ubuntu Wiki](https://wiki.ubuntu.com/Gobuntu)

[Category:Ubuntu衍生版](../Category/Ubuntu衍生版.md "wikilink")
[Category:自由作業系統](../Category/自由作業系統.md "wikilink")

1.
2.
3.
4.