『**Good-bye
days**』是[日本唱作女歌手](../Page/日本.md "wikilink")[YUI的第五張單曲碟](../Page/YUI.md "wikilink")，於2006年6月14日所推出的單曲碟。
本單曲碟為配合電影「[太陽之歌](../Page/太陽之歌.md "wikilink")」，以「YUI for 雨音薫」的名義推出。這是首張由
[STUDIOSEVEN Recordings](../Page/STUDIOSEVEN_Recordings.md "wikilink")
發行的單曲碟。簡介：實力創作女新人YUI去年以一曲「feel my
soul」而備受注目。歌而優則演的她將會首次主演電影，與塚本高史合演純愛電影《太陽之歌》。電影講述患有[著色性乾皮症](../Page/著色性乾皮症.md "wikilink")（Xeroderma
Pigmentosum，簡稱XP），不能被陽光照射的少女雨音薰與陽光男孩的愛情故事。由YUI主唱的主題曲「Good-bye
days」，更是在她拍攝時以電影的內容寫下的。

大碟另收錄電影插曲「Skyline」和「It's Happy Line」。

## 收錄歌曲

1.  **Good-bye days**
      -
        作詞・作曲：YUI　編曲：Akihisa Matsuura
    <!-- end list -->
      - YUI參演的電影「[太陽之歌](../Page/太陽之歌.md "wikilink")」之主題曲，另收錄於第二張專輯「[CAN'T
        BUY MY LOVE](../Page/CAN'T_BUY_MY_LOVE.md "wikilink")」。
2.  **Skyline**
      -
        作詞・作曲：YUI　編曲：Akihisa Matsuura
    <!-- end list -->
      - YUI為「[太陽之歌](../Page/太陽之歌.md "wikilink")」創作的插入歌曲。
3.  **It's happy line**
      -
        作詞・作曲：YUI　編曲：
    <!-- end list -->
      - 收錄於首張獨立單曲「[It's happy line/I
        know](../Page/It's_happy_line/I_know.md "wikilink")」。由於只曾在九州作限量發售，便響應歌迷請求重新收錄在此單曲中。
4.  **Good-bye days～Instrumental～**

## 銷售紀錄

  - 累積發售量：241,873
  - [Oricon最高排名](../Page/Oricon.md "wikilink")：第3名（11次）
  - [Oricon上榜次數](../Page/Oricon.md "wikilink")：44次

\[1\]

| 發行         | 排行榜       | 最高位     | 總銷量 |
| ---------- | --------- | ------- | --- |
| 2006年6月14日 | Oricon 日榜 | 1       |     |
| Oricon 週榜  | 3         | 226,155 |     |
| Oricon 年榜  | 36        | 241,873 |     |

## 資料來源

<div class="references-small">

<references />

</div>

[分類:NHK紅白歌合戰演唱歌曲](../Page/分類:NHK紅白歌合戰演唱歌曲.md "wikilink")

[Category:YUI歌曲](../Category/YUI歌曲.md "wikilink")
[Category:2006年單曲](../Category/2006年單曲.md "wikilink")
[Category:電影主題曲](../Category/電影主題曲.md "wikilink")
[Category:RIAJ手機片段下載百萬認證歌曲](../Category/RIAJ手機片段下載百萬認證歌曲.md "wikilink")

1.  [Oricon](http://www.oricon.co.jp/music/release/d/651433/1/)