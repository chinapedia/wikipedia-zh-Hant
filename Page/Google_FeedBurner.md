**FeedBurner**是一個於2004年在[美国](../Page/美国.md "wikilink")[芝加哥市成立的](../Page/芝加哥.md "wikilink")[網站饋送管理供應商](../Page/饋送.md "wikilink")\[1\]。FeedBurner為[網誌作者](../Page/網誌作者.md "wikilink")、[播客與其他基於網絡的內容發佈者提供訂製的](../Page/播客.md "wikilink")[RSS饋送與管理工具](../Page/RSS.md "wikilink")，提供予發佈者的服務包括[流量分析](../Page/流量.md "wikilink")\[2\]以及一個可供選擇的廣告系統。儘管初時尚未清晰廣告是否適合RSS格式\[3\]，作者現在可選擇包含廣告到三分之二的FeedBurner饋送內\[4\]。

出版的饋送可以數個方式修改，包括自動鏈結到[Digg與](../Page/Digg.md "wikilink")[del.icio.us](../Page/del.icio.us.md "wikilink")，以及「接合」多個饋送的資訊\[5\]。FeedBurner是一個典型[Web
2.0的服務](../Page/Web_2.0.md "wikilink")，提供了可與其他軟體互動的[網絡服務](../Page/網絡服務.md "wikilink")[应用程序接口](../Page/应用程序接口.md "wikilink")（API）。截至2007年5月為止FeedBurner已為410,769名發佈者託管饋送\[6\]。

[Google於](../Page/Google.md "wikilink")2007年5月23日以1亿美元現金[收购FeedBurner](../Page/收购.md "wikilink")\[7\]。

## 在[中国大陆的现状](../Page/中国大陆.md "wikilink")

FeedBurner在中国大陆的最大競争对手为[抓虾與](../Page/抓虾.md "wikilink")[Feedsky](../Page/Feedsky.md "wikilink")。用户可在FeedBurner中免费烧制自己的[RSS](../Page/RSS.md "wikilink")，另外FeedBurner亦提供了专业版[服务](../Page/服务.md "wikilink")。FeedBurner致力于提供全球用户免费饋送烧制，目前只支持小部分[语言](../Page/语言.md "wikilink")，但其他语言正在翻译之中。

至今，FeedBurner的RSS烧录服务仍然遭到[防火长城的关键词过滤而被屏蔽](../Page/防火长城.md "wikilink")，过滤的关键词是“feeds.feedburner.com”\[8\]，致使FeedBurner的RSS烧录服务在中国大陆境内无法正常访问。但有部分用户发现了FeedBurner的RSS烧录服务拥有其它子域名，并成功地在中国大陆境内绕过防火长城的[审查使用](../Page/审查.md "wikilink")\[9\]。

## 註釋

## 参见

  - [Google产品列表](../Page/Google产品列表.md "wikilink")

## 外部連結

  - [FeedBurner.com](http://www.feedburner.com)
  - [採訪Feedburner與BlogBeat的創辦者](http://www.blogherald.com/2006/07/17/an-interview-with-feedburners-steve-olechowski-and-jeff-turner/)
  - [FeedBurner RSS 廣告網路](http://www.feedburner.com/fb/a/adnetwork)

[Category:網站供稿](../Category/網站供稿.md "wikilink")
[Category:Google](../Category/Google.md "wikilink") [Category:Web
2.0](../Category/Web_2.0.md "wikilink")
[Category:网站](../Category/网站.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.