**Googlepedia**是一套適用於[Mozilla瀏覽器](../Page/Mozilla瀏覽器.md "wikilink")（例如：[Mozilla
Firefox](../Page/Mozilla_Firefox.md "wikilink")）的[自由](../Page/自由軟體.md "wikilink")[插件](../Page/插件.md "wikilink")。這插件會在用戶找尋時，在屏幕的一邊顯示[Google的搜尋結果](../Page/Google.md "wikilink")，並在另一邊同時顯示[維基百科的搜尋結果](../Page/維基百科.md "wikilink")。「Googlepedia」是一個由「google」和「Wikipedia」兩字結合而成的[合體字](../Page/合體字.md "wikilink")（*portmanteau*）。

## 功能

  - 將 Wikipedia 內部鏈結轉成 Google 搜尋連結
  - 利用 Google[好手氣功能搜尋相關條目](../Page/手气不错.md "wikilink")
  - 按下圖片鏈結時顯示完整大小
  - 移除 [Google AdWords](../Page/Google_AdWords.md "wikilink")
  - 可延展成整個瀏覽器顯示的畫面
  - 依個人 Google 的語言設定，可搜尋到特定語文的維基百科相關內容

## 外部連結

  - Googlepedia at
    [addons.mozilla.org](http://arquivo.pt/wayback/20091001213557/https://addons.mozilla.org/firefox/2517/)

[Category:Google](../Category/Google.md "wikilink") [Category:Firefox
附加组件](../Category/Firefox_附加组件.md "wikilink")