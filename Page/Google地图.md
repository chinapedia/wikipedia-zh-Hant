[Google_Maps.png](https://zh.wikipedia.org/wiki/File:Google_Maps.png "fig:Google_Maps.png")的螢幕截圖\]\]

**Google地图**（；前稱：Google
Local）是公司向全球提供的[电子地图服务](../Page/电子地图服务.md "wikilink")，地图包含[地標](../Page/地標.md "wikilink")、線條、形狀等信息，提供[矢量](../Page/矢量.md "wikilink")[地图](../Page/地图.md "wikilink")、[卫星照片](../Page/卫星照片.md "wikilink")、[地形图等三种視图](../Page/地形图.md "wikilink")。其姊妹產品包括[Google地球](../Page/Google地球.md "wikilink")、[Google月球](../Page/Google月球.md "wikilink")、[Google火星](../Page/Google火星.md "wikilink")、[Google星空](../Page/Google星空.md "wikilink")、[Google海洋](../Page/Google海洋.md "wikilink")。

Google地图于2005年2月8日在谷歌博客上首次公布，并于2005年6月20日将覆盖范围从原先的[美国](../Page/美国.md "wikilink")、[英国](../Page/英国.md "wikilink")、[加拿大扩大至全球](../Page/加拿大.md "wikilink")。目前在全球多國开通了“[街景](../Page/Google街景.md "wikilink")”（）服务，使用者可以通过由Google[金龜車在街道上拍到的影像查看街景](../Page/金龜車.md "wikilink")。2011年10月7日，Google地图新增了[3D](../Page/三維空間.md "wikilink")[鳥瞰功能](../Page/鳥瞰.md "wikilink")。

Google和[北京图盟科技有限公司](../Page/北京图盟科技有限公司.md "wikilink")（）于2005年9月合作出版[中国大陆的](../Page/中国大陆.md "wikilink")本地，\[1\]后被重新命名为Google地图。[臺灣的圖資是由](../Page/臺灣.md "wikilink")[勤崴國際科技所提供](../Page/勤崴國際科技.md "wikilink")，並於2007年10月11日推出臺灣版地圖。地图的[香港版本於](../Page/香港.md "wikilink")2008年7月推出。

## 简介

### 视图

  - **[矢量地图](../Page/矢量.md "wikilink")（傳統地圖）：**可提供[國界](../Page/國界.md "wikilink")、[行政區劃和交通以及商業信息](../Page/行政區劃.md "wikilink")。Google地图中的矢量视图目前采用将矢量数据按照制图样式预先渲染成图像并切割为瓦片（tiles），浏览器取得这些瓦片并按照其相互位置关系在[浏览器内拼接成整体地图的方法](../Page/浏览器.md "wikilink")。此方案使得浏览器内用户看到的地图其实是栅格形式，道路、水体、建筑、行政区等地理对象并不能被选择交互，因此应用功能受到一定限制。2011年10月，Google
    Maps推出WebGL版本实现版本，试验在客户端实时渲染取自服务器的矢量数据的技术方案。这是Google地图发布以来在地图应用架构上最大的变化\[2\]。2018年8月，Google改以3D
    Globe模式以3D立體球狀顯示全球地圖，就像[地球儀一樣](../Page/地球儀.md "wikilink")，以解決以往平面模式顯示造成的過大比例誤差\[3\]。
  - **[卫星照片](../Page/卫星照片.md "wikilink")：**提供俯視圖或45°影像，跟[Google地球上的卫星照片基本一样](../Page/Google地球.md "wikilink")，也能看到水下地形。
  - **地形图：**可以用以显示陸地[地形和](../Page/地形.md "wikilink")[等高线](../Page/等高线.md "wikilink")，能看到山巒起伏的形態。

### 功能

  - **周边搜索**
  - 地址储存
  - **规划路线**
  - **分类搜索**
  - **分享位置**：2017年3月23日宣布「位置資訊分享」功能，所有Android與iOS的使用者都可輕鬆從Google地圖App中分享所在位置給親友，而收到位置資訊的親友則可透過Android、iPhone手機、行動網頁或桌機網頁即時查看\[4\]。
  - **[街景服务](../Page/街景地图.md "wikilink")：**[Google街景是一项于](../Page/Google街景.md "wikilink")2007年五月开始的新功能，提供水平方向360°及垂直方向290°的街道全景。现在该功能已经覆盖了全世界数十个国家和地区的主要城市。
  - **相片瀏覽：**自2010年6月中開始，Google將來自相片分享網站[Panoramio](../Page/Panoramio.md "wikilink")，
    由用戶提供的相片整合到Google地圖內。當拖拉Google地圖內的衣夾人時，除了街景服務已上線地區會顯示為藍色外，有用戶提供相片的地點亦會以藍點表示。
  - **我的地图：**「我的地图」功能使用户可以在Google地图上使用记号或者线进行标记，或者追加注释、照片或影片，从而作成一份专属于自己的地图。用户亦可以把作成的「我的地图」通过互联网进行共享，甚至邀请他人来一同编辑。

## 评价

### 正面

  - 可搜索，快速查找到地点。
  - 放大与缩小。
  - 可安装在[移动设备运行](../Page/移动设备.md "wikilink")，移动设备作为现代人类最常随身㩦帶的工具，轻便、可随时获取。
  - 近年来移动设备的智能化，网络速度的提升，更是让Google地图与人们的生活产生更为紧密的联系，因而倍受人们的赞誉。
  - 可觀察交通流量，從而避開擠塞的路段。

### 爭議

  - 隱私權爭議
  - 街景服務大部分僅能看到地面,即使地下有景點
  - Google公司依各國政府要求將軍事基地的圖資刪除並將衛星空拍照片模糊處理，但反而洩漏各國的軍事基地的大致位置
  - 地图资料数据由[勤崴國際科技](../Page/勤崴國際科技.md "wikilink")（KingwayTek）、Mapabc和Europa
    Technologies等公司提供，地名或行政區資訊不能保證完全正確，資訊更新頻率需时較久，有时甚至需等待超過1個月\[5\]。
  - 部份地方的衛星空拍照片過久未更新，例如2009年即完工啟用的台北市[交九轉運站](../Page/交九轉運站.md "wikilink")，在2017年8月時所見的Google地圖仍是未完工時衛星空拍照片。
  - 路綫規劃有簡化和缩短空間
  - GPS有時出錯（在小路上行駛時卻標示在公路上，使導航出錯）
  - 2010年3月，[香港版本使用了](../Page/香港.md "wikilink")[簡體中文及](../Page/簡體中文.md "wikilink")[漢語拼音而非香港原本的港式譯音](../Page/漢語拼音.md "wikilink")（不同於[粵語拼音](../Page/粵語拼音.md "wikilink")）。
  - 2010年4月17日起，Google地图對全中文用戶，將全世界地名含街道名均改為簡體中文表示。
  - 2010年4月23日起，中国版Google地图中大量的香港地名改用音譯，例如[佐敦变成](../Page/佐敦.md "wikilink")[约旦](../Page/约旦.md "wikilink")、[赤鱲角機場變成](../Page/香港國際機場.md "wikilink")「切克拉普科克艾爾波特」\[6\]。现已修正，简体版与繁体版都统一使用香港原本的中文命名。
  - 2010年5月2日，Google地图將[中華民國](../Page/中華民國.md "wikilink")[宜蘭縣屬地的](../Page/宜蘭縣.md "wikilink")[龜山島歸類為](../Page/龜山島.md "wikilink")「日本[沖繩縣](../Page/沖繩縣.md "wikilink")」領土。\[7\]

## \-{zh-tw:行動裝置版;zh-hans:移动版}-

  - [Android](../Page/Android.md "wikilink")：於Android版的Google Maps
    5.0支援羅盤模式及離線地圖功能。
  - [iOS](../Page/iOS.md "wikilink")
  - [Windows Mobile](../Page/Windows_Mobile.md "wikilink")
  - [Symbian](../Page/Symbian.md "wikilink")
    [S60](../Page/S60.md "wikilink") 3或更新
  - [Symbian](../Page/Symbian.md "wikilink")
    [UIQ](../Page/UIQ.md "wikilink") 3或更新
  - [BlackBerry OS](../Page/BlackBerry_OS.md "wikilink")
  - [Java](../Page/Java.md "wikilink")：2006年，Google發佈了一個[Java程式稱為](../Page/Java.md "wikilink")[Google
    Maps for Mobile](http://www.google.com/gmm)，可以用在Java-based的手機上。
  - [Palm
    OS](../Page/Palm_OS.md "wikilink")（[Centro或更新](../Page/Palm_Centro.md "wikilink")）
  - [WebOS](../Page/WebOS.md "wikilink")（[Palm
    Pre及](../Page/Palm_Pre.md "wikilink")[Palm
    Pixi](../Page/Palm_Pixi.md "wikilink")）

## 在[中国大陆的情况](../Page/中国大陆.md "wikilink")

[中華人民共和國](../Page/中華人民共和國.md "wikilink")[国家测绘地理信息局在](../Page/国家测绘地理信息局.md "wikilink")2012年2月宣布，Google在中国合资公司的互联网地图服务目前可维持现状，显示中国官方和Google在互联网地图服务上的争执有所缓和。测绘局公告称，Google在华合资公司——北京谷翔信息技术有限公司已于2011年11月提出互联网地图服务测绘资质申请，在资质审批过程中，其互联网地图服务可维持现状，但不得增加新的互联网地图服务内容。\[8\]这意味中国版地图被当局禁止推出街景服务。

中国版Google地图的行政边界及国境线一律按照中华人民共和国主张的边界线描绘，而国际版则有所区别。如[藏南地区](../Page/藏南地区.md "wikilink")（印度称[阿鲁纳恰尔邦](../Page/阿鲁纳恰尔邦.md "wikilink")）在中国版Google地图被完全划入中国领土，而在国际版被当作争议领土单独划出。

Google地图还推出过春运地图服务，该服务是在原地图上拉架售票点信息，还能查询机场、铁路、公路班次等的信息。\[9\]

### 偏移問題

受[中國測繪法規所限](../Page/中华人民共和国测绘限制.md "wikilink")，Google的[中国大陸地圖均需要經過](../Page/中国大陸.md "wikilink")「保障国家安全」的偏移處理，全中國平均向東南移位半公里。一些国际性的互联网应用服务（如[Foursquare等Apps](../Page/Foursquare.md "wikilink")），通常使用国际版地图的[API来显示位置](../Page/API.md "wikilink")，这会造成显示中国地图视图时出现偏差、漂移的情况。[Android系统手机在中国使用](../Page/Android.md "wikilink")[GPS定位也会出现地圖顯示不准确的情况](../Page/GPS.md "wikilink")。[Google地球的路網亦有位移情況](../Page/Google地球.md "wikilink")。Google地圖國際版和Google地球的衛星圖均無偏移。

Google地圖的中國版（“ditu.google.cn”（主要）及“maps.google.cn”）的衛星圖亦有等量偏移，藉此掩蓋偏移情況。因中国当局的政策，主要域名不能使用“我的地图”及“我的地点”功能。因位置漂移，两个域名都屏蔽了[维基百科](../Page/维基百科.md "wikilink")、[Panoramio照片](../Page/Panoramio.md "wikilink")、网络摄像头及[YouTube视频功能](../Page/YouTube.md "wikilink")。

<table>
<caption>Google中国地圖偏移一覽</caption>
<thead>
<tr class="header">
<th></th>
<th><p><a href="https://www.google.com/maps">国际版</a></p></th>
<th><p><a href="https://www.google.cn/maps">中國版</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>正確</p></td>
<td><p>偏移</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>偏移</p></td>
<td><p>偏移</p></td>
</tr>
<tr class="odd">
<td></td>
<td><ol>
<li>衛星與地圖不對應。</li>
<li><a href="../Page/香港.md" title="wikilink">香港與</a><a href="../Page/深圳.md" title="wikilink">深圳接壤地圖深圳部份移位</a>，深圳河消失及變形；<a href="../Page/澳門.md" title="wikilink">澳門與</a><a href="../Page/珠海.md" title="wikilink">珠海接壤地圖珠海部份移位</a>，河流消失及變形；大部分外国与中国接壤的地方中国部分地图移位或变形。</li>
</ol></td>
<td><ol>
<li>衛星與地圖巧合對應，這是由於移位的距離與角度剛好相同。</li>
<li><a href="../Page/海峽兩岸.md" title="wikilink">海峽兩岸距離縮少了約半公里</a>。</li>
<li><a href="../Page/香港.md" title="wikilink">香港部分</a><a href="../Page/新界.md" title="wikilink">新界地區消失</a>；<a href="../Page/澳門.md" title="wikilink">澳門嚴重變形</a>；大部分外国与中国接壤的地方外国部分卫星视图被中国部分覆盖，中国偏移地图和国际正常地图重叠。</li>
</ol></td>
</tr>
</tbody>
</table>

### 服务现状

2014年5月31日起，Google所有服务（各个IP段）在中国被大规模干扰至今\[10\]，但因为[翻译及地图网页版服务有域名后缀为](../Page/Google翻译.md "wikilink")[.cn的入口](../Page/.cn.md "wikilink")，并且使用了专门设立在中国境内的服务器和IP地址，所以翻译及[地图网页版仍然能正常服务](../Page/Google地图.md "wikilink")。而由于手机App端指向的是非.cn的入口，因此手机端无法使用Google地图App端。

## 彩蛋

2012年4月1日[愚人節](../Page/愚人節.md "wikilink")，Google推出了水下搜索及[勇者鬥惡龍的](../Page/勇者鬥惡龍.md "wikilink")「8[位元探索地圖](../Page/位元.md "wikilink")」（日版為「探究」），地圖右上方點擊探究即可進入，而街景同樣是8bit\[11\]。大城市以城堡表示（如[上海](../Page/上海.md "wikilink")、[北京](../Page/北京.md "wikilink")、[廣州等](../Page/廣州.md "wikilink")），其他城市以村莊表示，眾多著名建築都有特別的繪圖，Google分部以兩名小童表示。

2013年4月1日愚人節，Google推出藏寶圖風格的地圖，褐色牛皮紙與濃濃古風讓你宛如置身大航海時代，讓使用者可以用相當復古的舊式紙本地圖外觀進行查找。另外Google推出嗅覺搜尋功能，稱只要貼近螢幕就會聞到味道，結果這當然是場惡作劇。
地圖充滿濃厚復古風，以牛皮紙手繪風格亮相。只要點選藏寶圖選項，就可讓地圖變為藏寶圖，延續2012年的探索精神，只是2012年是由勇者探索大地，2013年顯然是由網友扮演寶藏獵人或海盜了。點選街景則可看到老照片風格的街景圖。

2014年4月1日愚人節，Google於智慧型手機Google Map
APP推出「[神奇寶貝訓練師專用地圖](../Page/神奇寶貝.md "wikilink")」，共150種不同類型的神奇寶貝，數以萬計地分布於世界各角落，供使用者抓取神奇寶貝，體驗一日神奇寶貝訓練師。除了延續傳統探險精神，更滿足了許許多多訓練師的蒐集慾望。

2015年4月1日愚人节，Google推出了「[小精靈
(Pac-Man)地图](../Page/吃豆人.md "wikilink")」，在地图左下角即可进入\[12\]。使用者可以选择有足够密度街道的区域，在真实的地图下体验吃豆人游戏。

## 參見

  - [Google地球](../Page/Google地球.md "wikilink")
  - [Google街景](../Page/Google街景.md "wikilink")
  - [Google Map Maker](../Page/Google_Map_Maker.md "wikilink")
  - [WikiMapia](../Page/WikiMapia.md "wikilink")：結合Google Maps及Wiki引擎的計劃
  - [Google产品列表](../Page/Google产品列表.md "wikilink")
  - [电子地图](../Page/电子地图.md "wikilink")
  - [电子地图服务](../Page/电子地图服务.md "wikilink")
  - [街景地图](../Page/街景地图.md "wikilink")

## 参考资料

## 外部链接

  - 国际版

<!-- end list -->

  - [Google Maps](https://www.google.com/maps?hl=en)
      - [Google地图](https://www.google.com/maps?hl=zh-TW)
      - [Google地图](https://www.google.com/maps?hl=zh-CN)

<!-- end list -->

  - 地区版

<!-- end list -->

  - [中国大陆版](http://www.google.cn/maps?hl=zh-CN)
  - [台灣版](https://www.google.com.tw/maps?hl=zh-TW)
  - [香港版](https://www.google.com.hk/maps?hl=zh-TW)

<!-- end list -->

  - 开发者

<!-- end list -->

  - [Google Maps API](https://developers.google.com/maps/)

[Category:Google](../Category/Google.md "wikilink")
[Category:电子地图](../Category/电子地图.md "wikilink") [Category:Web
2.0](../Category/Web_2.0.md "wikilink")
[Category:Android软件](../Category/Android软件.md "wikilink")

1.
2.  [Prime Time For WebGL: Google Maps Gets an
    Upgradel](http://www.tomshardware.com/news/chrome-firefox-webgl-maps-mapsgl,13719.htm)
3.  [Google地圖以立體球狀似地球儀的樣式呈現
    減少比例誤差值](https://www.cool3c.com/article/136876)
4.
5.
6.
7.
8.
9.  [春运地图](http://www.google.cn/landing/chunyun/)
10.
11.
12.