**Google日曆**（**Google
Calendar**），原始代碼名稱“CL2”，是一款由[Google提供的免費聯絡人管理](../Page/Google.md "wikilink")、時間管理的[網頁應用程式](../Page/網頁應用程式.md "wikilink")。Google日曆允許使用者使用網路日曆同步他們的[Gmail聯絡人](../Page/Gmail.md "wikilink")。Google
日曆於2006年4月13日開放使用，於2009年7月結束beta測試。\[1\]
為了使用此軟體，使用者不需擁有Gmail帳戶，而是需要免費的[Google帳戶](../Page/Google帳戶.md "wikilink")。

## 特性

  - [Ajax界面](../Page/Ajax.md "wikilink")
  - 数据在线存储
  - 用户可创建多个日历
  - 多种视图：日视图，周视图，月视图，任务列表视图，自定义视图
  - 共享日历给他人
  - [邮件](../Page/邮件.md "wikilink")／[短信提醒](../Page/短信.md "wikilink")
  - 可以和[微软的](../Page/微软.md "wikilink")[Outlook或](../Page/Outlook.md "wikilink")[Mozilla的](../Page/Mozilla.md "wikilink")[Sunbird](../Page/Mozilla_Sunbird.md "wikilink")
    / [Lightning等第三方日历软件进行数据同步](../Page/Lightning.md "wikilink").
  - 自2014年8月1日起，无法再使用Google官方Outlook同步插件google calendar
    sync进行日历同步，用户需要自行使用第三方软件，如支持Google CalDav
    API的第三方插件

## 參考資料

## 参见

  - [Google产品列表](../Page/Google产品列表.md "wikilink")

## 外部链接

  - [Google Calendar](http://www.google.com/calendar/render?hl=en)
      - [Google日历](http://www.google.com/calendar/render?hl=zh-CN)
      - [Google日历](http://www.google.com/calendar/render?hl=zh-TW)
      - [Https安全连接](https://www.google.com/calendar/)

[Category:Google](../Category/Google.md "wikilink")
[Category:日程管理軟體](../Category/日程管理軟體.md "wikilink")

1.  西安商赢信息，[Gmail取消beta](http://www.029soho.com/Internet/174.htm)
    ，2009年7月8日