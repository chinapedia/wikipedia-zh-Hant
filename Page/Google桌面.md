**Google桌面**（Google
Desktop）是[Google公司的一款](../Page/Google公司.md "wikilink")[桌面搜索软件](../Page/桌面搜索.md "wikilink")，在[Windows
XP或](../Page/Windows_XP.md "wikilink")[Windows
2000的SP](../Page/Windows_2000.md "wikilink")3以上版本的[个人电脑以及](../Page/个人电脑.md "wikilink")[Mac](../Page/Mac.md "wikilink")，[Linux上本地运行](../Page/Linux.md "wikilink")。
该桌面搜索程序可以对一个人的[电子邮件](../Page/电子邮件.md "wikilink")、[电子文档](../Page/电子文档.md "wikilink")、[音乐](../Page/音乐.md "wikilink")、[照片](../Page/照片.md "wikilink")、聊天记录和使用者浏览过的网页进行全文搜索。

“Google桌面”不是[开源](../Page/开放源代码.md "wikilink")、[自由的软件](../Page/自由软件.md "wikilink")，但是在遵守[最终用户使用条款](../Page/EULA.md "wikilink")（EULA）的前提下，用户可以免费下载使用。安装完成后，“Google桌面”会花费数百MB的空间和一定时间来建立[索引](../Page/索引.md "wikilink")，并在每次开机的时候自动启动，以实现搜索本地资源的功能。用户也可以自由选择关闭、删除该软件。

## 历史

“Google桌面”最初是在[Windows
Vista中可能提供的文件和网络搜索功能的影响下开发](../Page/Windows_Vista.md "wikilink")。一些人声称“Google桌面”和[Mac
OS X
v10.4操作系统的](../Page/Mac_OS_X_v10.4.md "wikilink")[Spotlight部分一样](../Page/Spotlight.md "wikilink")，都从微软2003年的专业开发者大会（PDC）上演示的Vista桌面搜索中获得了灵感。因为“Google桌面”或许可以进行对Google的私有搜索[算法进行](../Page/算法.md "wikilink")[逆向工程](../Page/逆向工程.md "wikilink")，所以它得到了许多关注。

“Google桌面”2.0版的测试版（Beta版）於2005年8月22日发布。它和1.0版的主要区别是增加了补充工具栏（sidebar）。补充工具栏是一个可以显示个性化信息的[面板](../Page/面板.md "wikilink")，它可以放在Windows桌面某一侧，显示实时的新闻、电子邮件、照片、股票和天气等信息。补充工具栏还包括了一个可以搜索个人电脑或其他Google搜索类型（如网页、图片、资讯、论坛等）的搜索框。

“Google桌面”2.0版于2005年11月3日正式推出，新的特色包括[Google
Maps的补充工具栏插件和更多对开发人员的帮助支持](../Page/Google_Maps.md "wikilink")。

“Google桌面”3.0版的测试版于2006年2月9日发布。它开始支持对网络上多台计算机进行检索，这是本版中最为重大的改进，可以实现搜索远程电脑桌面的功能。无论你在家中还是在单位，都可以共享不同电脑中的资源。这一功能看上去很美，不过也备受争议。要实现此功能，用户必须允许程序将你的搜索资料复制到Google的服务器上去，虽然文件传送过程是加密的，但不可否认，存在一定安全隐患。

“Google桌面”3.0版于2006年3月14日正式推出，这个版本中的快速查找框，在你的桌面上的任意地方只要按两次“[ctrl](../Page/ctrl.md "wikilink")”键，它就会出现。

2011年9月2日，Google進行業務大整頓，將關閉Google桌面。

## 版本歷史

[Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")

  - Google Desktop beta:2004年10月14日
  - Google Desktop 2 beta:2005年8月22日
  - Google Desktop 2 graduated from beta:2005年11月3日
  - Google Desktop 3 Beta:2006年2月9日
  - Google Desktop 3 graduated from beta:2006年3月14日
  - Google Desktop 4 Beta:2006年5月10日
  - Google Desktop 4 graduated from beta:2006年6月27日
  - Google Desktop 4.5:2006年12月14日
  - Google Desktop 5 Beta:2007年3月6日
  - Google Desktop 5.1:2007年4月27日
  - Google Desktop 5.5:2007年10月2日
  - Google Desktop 5.7:2008年8月22日
  - Google Desktop 5.8:2009年1月30日
  - Google Desktop 5.9:2009年7月8日

[Mac OS X](../Page/Mac_OS_X.md "wikilink")

  - Google Desktop 4.5:2006年11月14日
  - Google Desktop 5 Beta:2007年3月6日
  - Google Desktop 5.1:2007年4月27日
  - Google Desktop 5.5:2007年10月2日
  - Google Desktop 5.7:2008年8月22日
  - Google Desktop 5.8:2009年1月30日

[Mac OS](../Page/Mac_OS.md "wikilink")

  - Google Desktop 1.0:2007年4月
  - Google Desktop 1.4 beta:2007年11月29日

[Red Hat Linux](../Page/Red_Hat_Linux.md "wikilink")

  - Google Desktop 1.0:2007年6月27日
  - Google Desktop 1.1:2007年12月18日

## 技术

在第一次安装的时候，“Google桌面”会进行一次完整的本地文件索引编制。在此以后，软件就只会根据需要进行局部的索引编制。

## 參考資料

## 参见

  - [Google产品列表](../Page/Google产品列表.md "wikilink")
  - [Spotlight](../Page/Spotlight.md "wikilink")，[Mac下的桌面搜索工具](../Page/麦金塔电脑.md "wikilink")
  - [Beagle](../Page/Beagle.md "wikilink")，[Gnome下的搜索工具](../Page/Gnome.md "wikilink")
  - [Strigi](../Page/Strigi.md "wikilink")，[KDE下的搜索工具](../Page/KDE.md "wikilink")

## 外部链接

  - [desktop.google.com/](https://www.google.com/)

[Category:桌面搜索引擎](../Category/桌面搜索引擎.md "wikilink")
[Category:已終止開發的Google軟體](../Category/已終止開發的Google軟體.md "wikilink")
[Category:免費軟件](../Category/免費軟件.md "wikilink")