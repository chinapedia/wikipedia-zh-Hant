[Google_pages_logo.gif](https://zh.wikipedia.org/wiki/File:Google_pages_logo.gif "fig:Google_pages_logo.gif")
**Google Page
Creator**是[Google实验室在](../Page/Google实验室.md "wikilink")2006年2月23日推出的一个在线网页编辑工具。Google已宣布，该产品已完全转移至[Google协作平台](../Page/Google协作平台.md "wikilink")。

## 历史

  - 2006年2月23日，Google Page
    Creator公开[Beta测试版本发布](../Page/软件测试.md "wikilink")。
  - 2009年12月2日，Google Page Creator網頁全面轉移至[Google
    Sites](../Page/Google_Sites.md "wikilink")。

## 特点

[Google_Page_Creator_1.PNG](https://zh.wikipedia.org/wiki/File:Google_Page_Creator_1.PNG "fig:Google_Page_Creator_1.PNG")
[Google](../Page/Google.md "wikilink")[网页上表示这个服务](../Page/网页.md "wikilink")“Create
your own web pages, quickly and easily.”（快速、简单地创造属于你自己的网页）。它的特点如下：

  - 不需要专业技术知识
  - [所见即所得编辑](../Page/所见即所得.md "wikilink")
  - 为每个用户免费提供100[MB空间](../Page/百萬位元組.md "wikilink")
  - 提供多种[模板](../Page/模板.md "wikilink")
  - 可以上传文件
  - 支援用[HTML建立和修改网页](../Page/HTML.md "wikilink")
  - 自动儲存

此外同一帳戶可以在已有的網頁空間裡開啟另外四個網頁空間，而這四個網頁空間的網址都是不同的，網頁空間和流量也是獨立計算的。

## 要求

  - 使用Google Page
    Creator需要擁有一個[GMail帳戶](../Page/GMail.md "wikilink")，並且需使用[Internet
    Explorer](../Page/Internet_Explorer.md "wikilink")6.0或[Firefox](../Page/Firefox.md "wikilink")
    1.0以上的浏览器。

## 网页与网站

在2006年2月23日[Google公司发布Google](../Page/Google公司.md "wikilink") Page
Creator时表示，其认为[网页是拥有自有](../Page/网页.md "wikilink")[网址的单一文件](../Page/网址.md "wikilink")，而[网站则是使用共有的次域名的网页集合](../Page/网站.md "wikilink")。Google还表示，在Google
Page Creator测试的初期，人们只能通过它制作网页，而不能制作网站。

## 竞争和优势

有媒体评论认为Google Page Creator的功能可以与[Microsoft Office
FrontPage的基本功能相媲美](../Page/Microsoft_FrontPage.md "wikilink")，并且由于是在线免费版本，可能会冲击[Microsoft
Office FrontPage](../Page/Microsoft_FrontPage.md "wikilink")。\[1\]

此外Google Page
Creator和[微软](../Page/微软.md "wikilink")、[苹果](../Page/苹果.md "wikilink")、[Adobe及](../Page/Adobe.md "wikilink")[MySpace等提供的相关](../Page/MySpace.md "wikilink")[在线服务类似](../Page/在线.md "wikilink")，可以说它们存在着竞争关系。而Google
Page
Creator创建的网页可以在发布数小时内被[Google数据库收入](../Page/Google.md "wikilink")，并即刻可以在[Google上被搜索到](../Page/Google.md "wikilink")，这可以说是Google
Page Creator的一个极大的优势。

## 批評與漏洞

Google Page
Creator使用者的網址命名法被指出會讓廣告商容易地搜集網頁主人的[電子郵件地址](../Page/電子郵件.md "wikilink")，從而向其大量發放廣告郵件。此外，有使用者批評Google
Page
Creator不能修改模板和加入[CSS及](../Page/CSS.md "wikilink")[JavaScript](../Page/JavaScript.md "wikilink")，可自訂的項目太少。也有某些使用者在使用Google
Page Creator時遇上各種問題，例如：

  - 部分使用者指出使用IE 7.0時不能正確載入主頁，可能是由于IE7不支持W3C标准所致；
  - 登入時有時會出現「網頁數量已到達極限」的錯誤訊息，Google Page Creator限制使用者的網頁數量不超出100。

## 未来发展

有媒体报道认为，Google Page
Creator会与Google此前或即将推出的各种服务形成关联关系，比如和[Blogger](../Page/Blogger.md "wikilink")、[Google
Base](../Page/Google_Base.md "wikilink")、[Picasa](../Page/Picasa.md "wikilink")，甚至[Gmail或](../Page/Gmail.md "wikilink")[Google
Talk互相关联](../Page/Google_Talk.md "wikilink")，形成一个[社会网络服务](../Page/社会网络.md "wikilink")。\[2\]

## 参考资料

## 参见

  - [Google产品列表](../Page/Google产品列表.md "wikilink")

## 外部链接

  - [Google Page
    Creator](https://web.archive.org/web/20111118235135/http://pages.google.com/)
  - [Google Page
    Creator](https://web.archive.org/web/20111118235135/http://pages.google.com/)（Https安全连接）

[Category:已終止的Google服務](../Category/已終止的Google服務.md "wikilink")

1.
2.