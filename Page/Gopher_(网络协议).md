**Gopher**是一个[互联网上使用的分布型的文件搜集获取](../Page/互联网.md "wikilink")[网络协议](../Page/网络协议.md "wikilink")。它是1991年由[明尼苏达大学的保羅](../Page/明尼苏达大学.md "wikilink")·林德納（Paul
Lindner）和[馬克·麥卡希爾发明的](../Page/馬克·麥卡希爾.md "wikilink")\[1\]。

## 起源

“Gopher”（地鼠）这个名字有三层含义：第一是“挖掘信息”；第二，使用菜单形式搜集来的信息与[地鼠洞相类似](../Page/地鼠.md "wikilink")\[2\]；第三，[明尼苏达大学有一支运动队名叫](../Page/明尼苏达大学.md "wikilink")「」\[3\]。

Gopher最初的设计目标与[万维网类似](../Page/万维网.md "wikilink")：共享文档，今天的万维网几乎已经替代了Gopher。但Gopher协议还提供了一些万维网先天缺乏的功能，比如在Gopher中所有信息都以层级形式存储，这被认为是存储大量信息的最好方式之一。

## 衰微的原因

[全球資訊網在](../Page/全球資訊網.md "wikilink")1991年被發明，由於耗用頻寬較少，Gopher网络当时仍然是非常流行和制作精良的。1993年2月，明尼苏达大学宣布他们将对Gopher的使用收取执照费\[4\]\[5\]，这就一部分的减少了Gopher服务器数量。一些人相信这是Gopher变成[網際網路歷史的原因](../Page/網際網路歷史.md "wikilink")。

很多人相信Gopher的衰微实际上是它那有限制的结构造成的，这种结构使得它没有自由形态的[HTML网页灵活](../Page/HTML.md "wikilink")。使用Gopher时，每个文档都已有一个预定义的格式和类型，一个Gopher用户必须通过一个服务器定义的系统菜单导航进某一个特定的文档。很多人不喜欢Gopher系统中这种人为制造的菜单和文件的区分，而Web网络上使用的[超文本协议和交互式应用程序显得更为开放灵活](../Page/超文本.md "wikilink")。

现代，一些人建议说，在使用[宽带方式存取的](../Page/宽带.md "wikilink")[移动电话和](../Page/移动电话.md "wikilink")[PDA上](../Page/個人數碼助理.md "wikilink")，Gopher将会非常适合。但是，现在的市场似乎更偏向于[WML](../Page/WML.md "wikilink")-[WAP](../Page/WAP.md "wikilink")，[DoCoMo
i-mode或其他基于HTML的应用](../Page/I-mode.md "wikilink")。

## 相关技术

[Veronica是一个主要的Gopher搜索引擎](../Page/維羅妮卡.md "wikilink")。Veronica提供對Gopher[服务器菜单标题和Gopher网络的](../Page/服务器.md "wikilink")[关键字搜索](../Page/关键字.md "wikilink")。一次Veronica上的搜索产生一个Gopher项目菜单，其中每一项直接指向Gopher数据源。

## 今天的Gopher

在2004年，世界[互联网上仍然运行着少数Gopher服务器](../Page/互联网.md "wikilink")，如[美国政府和](../Page/美国政府.md "wikilink")[史密森尼學會仍運作各自的Gopher](../Page/史密森尼學會.md "wikilink")，他们仍然被协议狂热者维护着。

2002年6月，Gopher协议在[Internet
Explorer的补丁中已被禁用](../Page/Internet_Explorer.md "wikilink")，仅仅因为微软宣称发现其中有一个安全漏洞；通过编辑[-{zh-hans:注册表;
zh-hant:登錄檔;}-可以重新启用Gopher协议](../Page/注册表.md "wikilink")\[6\]。

其他的[浏览器](../Page/浏览器.md "wikilink")，包括[AOL仍然支持这个协议](../Page/AOL.md "wikilink")，但是他们支持的并不完全。其中最严重的不足就是不能够渲染显示出菜单页中包括的信息文本。Mozilla已從[Firefox
4.0起移除瀏覽Gopher的功能](../Page/Firefox_4.0.md "wikilink")\[7\]。一个位于
[Floodgap.com](http://gopher.floodgap.com/gopher/)
的公共[代理服务器允许用户使用任何浏览器访问Gopher页面](../Page/代理服务器.md "wikilink")，这个代理服务器将Gopher转换为[HTTP](../Page/HTTP.md "wikilink")／[HTML网页](../Page/HTML.md "wikilink")。

## 參考文獻

## 外部链接

  - [Web雖勝，Gopher猶存](https://arstechnica.com/tech-policy/news/2009/11/the-web-may-have-won-but-gopher-tunnels-on.ars)
    技術討論網站Ars Technica發表的一篇關於Gopher社群愛好者的文章

[Category:網際網路標準](../Category/網際網路標準.md "wikilink")
[Category:網際網路的歷史](../Category/網際網路的歷史.md "wikilink")
[Category:1991年面世](../Category/1991年面世.md "wikilink")

1.

2.

3.

4.

5.
6.

7.