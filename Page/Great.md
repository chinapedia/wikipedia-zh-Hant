[Great_Food_Hall_in_Pacific_Place_201312.jpg](https://zh.wikipedia.org/wiki/File:Great_Food_Hall_in_Pacific_Place_201312.jpg "fig:Great_Food_Hall_in_Pacific_Place_201312.jpg")[金鐘](../Page/金鐘.md "wikilink")[太古廣場分店](../Page/太古廣場.md "wikilink")\]\]

**Great**是[長江和記實業旗下](../Page/長江和記實業.md "wikilink")[屈臣氏集團於](../Page/屈臣氏集團.md "wikilink")[香港和](../Page/香港.md "wikilink")[中國大陸設立的](../Page/中國大陸.md "wikilink")[連鎖](../Page/連鎖店.md "wikilink")[超級市場](../Page/超級市場.md "wikilink")，成立於2000年12月，主要對象是[上層階級的消費者](../Page/上層階級.md "wikilink")，[市場定位在](../Page/市場定位.md "wikilink")[百佳](../Page/百佳超級市場.md "wikilink")、[Taste及](../Page/Taste.md "wikilink")[Gourmet之上](../Page/Gourmet.md "wikilink")，售賣世界各地產品。[中国大陆的店铺於](../Page/中国大陆.md "wikilink")2014年1月成立，首店位于[成都](../Page/成都.md "wikilink")。現時在[香港有](../Page/香港.md "wikilink")1間位於[金鐘](../Page/金鐘.md "wikilink")[太古廣場地庫之分店](../Page/太古廣場.md "wikilink")，在[中国大陆有](../Page/中国大陆.md "wikilink")1間位於[成都国际金融中心之分店](../Page/成都国际金融中心.md "wikilink")。

Great的分店是翻新[屈臣氏集團旗下](../Page/屈臣氏集團.md "wikilink")[百佳超級市場](../Page/百佳.md "wikilink")，並改成新模式經營，店內出售的產品部份不能在[屈臣氏集團旗下其他品牌的超級市場中找到](../Page/屈臣氏集團.md "wikilink")，[在香港主要競爭對手為](../Page/在香港.md "wikilink")[citysuper及](../Page/citysuper.md "wikilink")[牛奶國際旗下的Oliver](../Page/牛奶國際.md "wikilink")'s
The Delicatessen和[Jasons Food &
Living](../Page/Jasons_Food_&_Living.md "wikilink")。

## 分店

  - [香港](../Page/香港.md "wikilink")[金鐘](../Page/金鐘.md "wikilink")[太古廣場地庫](../Page/太古廣場.md "wikilink")[Great
    Pacific
    Place](../Page/Great_Pacific_Place.md "wikilink")（營業時間：10:00am-10:00pm）
  - [成都](../Page/成都.md "wikilink")[春熙路](../Page/春熙路.md "wikilink")[成都国际金融中心](../Page/成都国际金融中心.md "wikilink")[Great
    Chengdu International Finance
    Square](../Page/Great_Chengdu_International_Finance_Square.md "wikilink")（營業時間：10:00am-10:00pm）

## 出售產品

**Great**出售來自全球的產品，包括：

  - 來自全球的[雜貨](../Page/雜貨.md "wikilink")
  - 新鮮[海產](../Page/海產.md "wikilink")
  - 來自[亞洲及](../Page/亞洲.md "wikilink")[歐洲的產品](../Page/歐洲.md "wikilink")
  - [Great](../Page/Great.md "wikilink")
    Bakery的[麵包](../Page/麵包.md "wikilink")
  - 即叫即造的[三文治及](../Page/三文治.md "wikilink")[沙律](../Page/沙律.md "wikilink")
  - 環球[熟食](../Page/熟食.md "wikilink")
  - 意大利[雪糕](../Page/雪糕.md "wikilink")[all-SCREAM](../Page/all-SCREAM.md "wikilink")
  - Watson's Wine Cellar 酒窖出售的[酒](../Page/酒.md "wikilink")
  - **[Triple-O's](../Page/wikipedia:White_Spot.md "wikilink") 漢堡包專門店**
  - [Godiva專門店](../Page/歌帝梵巧克力.md "wikilink")

[缩略图](https://zh.wikipedia.org/wiki/File:Triple_O_in_Pacific_Place.JPG "fig:缩略图")

## 參考資料

  - [Great Foot Hall](http://www.greatfoodhall.com/)
  - [A.S. Watson -
    Great](https://web.archive.org/web/20080116094822/http://www.aswatson.com/c_retail_fegm_221_great.htm)

[Category:2000年成立的公司](../Category/2000年成立的公司.md "wikilink")
[Category:香港超級市場](../Category/香港超級市場.md "wikilink")
[Category:屈臣氏集團](../Category/屈臣氏集團.md "wikilink")