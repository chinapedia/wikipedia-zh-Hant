**Group Sounds**（或 **Group
Sound**）是日本1960年代後半期以結他為主、由數人組成的[搖滾樂隊種類](../Page/日本搖滾.md "wikilink")，簡稱
**GS**。自1966年[披頭四到日本公演以後](../Page/披頭四.md "wikilink")，一邊唱演唱一邊親自演奏[電吉他等樂器的樂隊在日本不斷冒起](../Page/電吉他.md "wikilink")，以年輕讀者為主的藝能雜誌《[週刊明星](../Page/週刊明星.md "wikilink")》把這些樂隊和音樂通稱為「Group
Sounds」或「Group Sound」，及後被廣泛使用。

1960年代末期，GS的熱潮急速減退，踏入新搖滾音樂年代，部份GS樂隊也開始解散，而The Spiders、The Tigers和The
Tempters解散後，部份成員於1971年組成日本首個[超級組合](../Page/:en:Super_Group.md "wikilink")「[PYG](../Page/PYG.md "wikilink")」。

現在，一般提及的Group Sounds都是指1960年代後半期活躍於咖啡店的流行曲樂隊和搖滾樂隊。

另外，樂隊以[和製英語命名都是GS的特色](../Page/和製英語.md "wikilink")。

## 著名樂隊

*（粗體字標記為隊長）*

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 2%" />
<col style="width: 1%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><strong>樂隊名稱</strong></p></td>
<td><p><strong>譯名</strong></p></td>
<td><p><strong>出道年份</strong></p></td>
<td><p><strong>隊員</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:ja:ザ・スパイダース.md" title="wikilink">ザ・スパイダース</a><br />
（田辺昭知とザ・スパイダース）</p></td>
<td><p>The Spiders（蜘蛛樂隊）</p></td>
<td><p>1961</p></td>
<td><p><strong><a href="../Page/田邊昭知.md" title="wikilink">田邊昭知</a></strong>、<a href="../Page/加藤充.md" title="wikilink">加藤充</a>、<a href="../Page/かまやつひろし.md" title="wikilink">かまやつひろし</a>、<a href="../Page/大野克夫.md" title="wikilink">大野克夫</a>、<a href="../Page/井上堯之.md" title="wikilink">井上堯之</a>（井上孝之）、<a href="../Page/堺正章.md" title="wikilink">堺正章</a>、<a href="../Page/井上順.md" title="wikilink">井上順</a>（井上順之）、<a href="../Page/前田富雄.md" title="wikilink">前田富雄</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/:ja:ザ・タイガース.md" title="wikilink">ザ・タイガース</a></p></td>
<td><p>The Tigers（<a href="../Page/老虎樂隊.md" title="wikilink">老虎樂隊</a>）</p></td>
<td><p>1967</p></td>
<td><p><a href="../Page/澤田研二.md" title="wikilink">澤田研二</a>、<strong><a href="../Page/岸部一德.md" title="wikilink">岸部一德</a></strong>（岸部修三）、<a href="../Page/加橋勝美.md" title="wikilink">加橋勝美</a>、<a href="../Page/森本太郎.md" title="wikilink">森本太郎</a>、<a href="../Page/岸部四郎.md" title="wikilink">岸部四郎</a>、<a href="../Page/瞳實.md" title="wikilink">瞳實</a>（已離隊）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:ja:ザ・ワイルドワンズ.md" title="wikilink">ザ・ワイルドワンズ</a><br />
（ザ・ワイルド・ワンズ、<br />
加瀬邦彦とザ・ワイルドワンズ等）</p></td>
<td><p>The Wild Ones</p></td>
<td><p>1966</p></td>
<td><p><strong><a href="../Page/加瀬邦彥.md" title="wikilink">加瀬邦彥</a></strong>、<a href="../Page/鳥塚繁樹.md" title="wikilink">鳥塚繁樹</a>、<a href="../Page/島英二.md" title="wikilink">島英二</a>、<a href="../Page/植田芳曉.md" title="wikilink">植田芳曉</a>、<a href="../Page/渡邊茂樹.md" title="wikilink">渡邊茂樹</a>（已離隊）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/:ja:ザ・テンプターズ.md" title="wikilink">ザ・テンプターズ</a></p></td>
<td><p>The Tempters（誘惑者樂隊）</p></td>
<td><p>1967</p></td>
<td><p><strong><a href="../Page/松崎由治.md" title="wikilink">松崎由治</a></strong>、<a href="../Page/萩原健一.md" title="wikilink">萩原健一</a>、<a href="../Page/田中俊夫.md" title="wikilink">田中俊夫</a>（已故）、<a href="../Page/高久昇.md" title="wikilink">高久昇</a>、<a href="../Page/大口廣司.md" title="wikilink">大口廣司</a>（已故）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:ja:ジャッキー吉川とブルーコメッツ.md" title="wikilink">ジャッキー吉川とブルーコメッツ</a><br />
（ジャッキー吉川とブルー・コメッツ）</p></td>
<td><p>Jackey吉川與Blue Comets</p></td>
<td><p>1957</p></td>
<td><p><strong><a href="../Page/ジャッキー吉川.md" title="wikilink">ジャッキー吉川</a></strong>、<a href="../Page/小田啓義.md" title="wikilink">小田啓義</a>、<a href="../Page/高橋健二.md" title="wikilink">高橋健二</a>、<a href="../Page/三原綱木.md" title="wikilink">三原綱木</a>、<a href="../Page/井上大輔.md" title="wikilink">井上大輔</a>（井上忠夫，已故）等</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/:ja:ザ・ゴールデン・カップス.md" title="wikilink">ザ・ゴールデン・カップス</a></p></td>
<td><p>The Golden Cups</p></td>
<td><p>1966</p></td>
<td><p><strong>デイヴ平尾</strong>（已故）、エディ藩、ケネス伊東（已故）、ルイズルイス加部、マモル・マヌー、ミッキー吉野、林恵文、アイ高野（已故）、柳ジョージ、ジョン山崎</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:ja:ヴィレッジシンガーズ.md" title="wikilink">ヴィレッジシンガーズ</a><br />
（ヴィレッジ・シンガーズ）</p></td>
<td><p>Village Singers</p></td>
<td><p>1966</p></td>
<td><p><a href="../Page/小松久.md" title="wikilink">小松久</a>、<a href="../Page/清水道夫.md" title="wikilink">清水道夫</a>、<a href="../Page/小池哲夫.md" title="wikilink">小池哲夫</a>、<a href="../Page/林ゆたか.md" title="wikilink">林ゆたか</a>、<a href="../Page/笹井一臣.md" title="wikilink">笹井一臣</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/:ja:オックス.md" title="wikilink">オックス</a></p></td>
<td><p>OX</p></td>
<td><p>1968</p></td>
<td><p><a href="../Page/福井利男.md" title="wikilink">福井利男</a>、<a href="../Page/岩田裕二.md" title="wikilink">岩田裕二</a>、<a href="../Page/岡田志郎.md" title="wikilink">岡田志郎</a>、<a href="../Page/真木ひでと.md" title="wikilink">真木ひでと</a>（野口ヒデト）、<a href="../Page/赤松愛.md" title="wikilink">赤松愛</a>、<a href="../Page/夏夕介.md" title="wikilink">夏夕介</a>（田浦幸）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:ja:ザ・カーナビーツ.md" title="wikilink">ザ・カーナビーツ</a></p></td>
<td><p>Carnabeats</p></td>
<td><p>1967</p></td>
<td><p><a href="../Page/アイ高野.md" title="wikilink">アイ高野</a>（已故）、<a href="../Page/臼井啓吉.md" title="wikilink">臼井啓吉</a>、<a href="../Page/越川ひろし.md" title="wikilink">越川ひろし</a>、<a href="../Page/岡忠夫.md" title="wikilink">岡忠夫</a>、<a href="../Page/喜多村次郎.md" title="wikilink">喜多村次郎</a>、<a href="../Page/ポール岡田.md" title="wikilink">ポール岡田</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/:ja:ザ・ジャガーズ.md" title="wikilink">ザ・ジャガーズ</a></p></td>
<td><p>The Jaguars</p></td>
<td><p>1967</p></td>
<td><p><a href="../Page/宮ユキオ.md" title="wikilink">宮ユキオ</a>、<a href="../Page/岡本信.md" title="wikilink">岡本信</a>（已故）、<a href="../Page/沖津ひさゆき.md" title="wikilink">沖津ひさゆき</a>、<a href="../Page/宮崎こういち.md" title="wikilink">宮崎こういち</a>、<a href="../Page/佐藤安治.md" title="wikilink">佐藤安治</a>、<a href="../Page/森田巳木夫.md" title="wikilink">森田巳木夫</a>、<a href="../Page/浜野たけし.md" title="wikilink">浜野たけし</a>、<a href="../Page/橋アキラ.md" title="wikilink">橋アキラ</a>、<a href="../Page/牧ツトム.md" title="wikilink">牧ツトム</a>、<a href="../Page/石井芳夫.md" title="wikilink">石井芳夫</a></p></td>
</tr>
</tbody>
</table>

## 其他樂隊

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 2%" />
<col style="width: 1%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><strong>樂隊名稱</strong></p></td>
<td><p><strong>譯名</strong></p></td>
<td><p><strong>出道年份</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:ja:パープルシャドウズ.md" title="wikilink">パープルシャドウズ</a><br />
（パープル・シャドウズ）</p></td>
<td><p>Purple Shadows</p></td>
<td><p>1968</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/:ja:ザ・モップス.md" title="wikilink">ザ・モップス</a><br />
（モップス）</p></td>
<td><p>The Mops</p></td>
<td><p>1966</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:ja:ザ・フィンガーズ.md" title="wikilink">ザ・フィンガーズ</a></p></td>
<td><p>The Fingers</p></td>
<td><p>1962</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/:ja:ジェット・ブラザース.md" title="wikilink">ザ・ジェット・ブラザーズ</a></p></td>
<td><p>The Jet Brothers</p></td>
<td><p>1968</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:ja:ザ・マミーズ.md" title="wikilink">ザ・マミーズ</a></p></td>
<td><p>The Mammys</p></td>
<td><p>1968</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/:ja:ズー・ニー・ヴー.md" title="wikilink">ズー・ニー・ヴー</a></p></td>
<td><p>Zoo Nee Voo</p></td>
<td><p>1968</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:ja:スウィング・ウエスト.md" title="wikilink">ザ・スウィング・ウエスト</a><br />
（ザ・スィング・ウェスト等）</p></td>
<td><p>Swing West</p></td>
<td><p>1957</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/:ja:寺内タケシとブルージーンズ.md" title="wikilink">寺内タケシとブルージーンズ</a><br />
（寺内タケシとバニーズ）</p></td>
<td><p>寺内タケシ與Blue Jeans</p></td>
<td><p>1962 (第一期)<br />
1969 (第二期)</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參見

  - [日本搖滾樂](../Page/日本搖滾樂.md "wikilink")

[Category:日本音樂風格](../Category/日本音樂風格.md "wikilink")
[Category:流行音樂類型](../Category/流行音樂類型.md "wikilink")
[Category:搖滾樂類型](../Category/搖滾樂類型.md "wikilink")
[Category:戰後昭和時代文化](../Category/戰後昭和時代文化.md "wikilink")