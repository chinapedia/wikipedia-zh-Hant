\-{T|Gtk\#}-  {{ infobox software | name = Gtk\# | developer =
[Novell](../Page/Novell.md "wikilink") | latest release version =
2.12.10 | latest release date =  | operating system =
[跨平台](../Page/跨平台.md "wikilink") | genre =
[部件工具箱](../Page/部件工具箱.md "wikilink") | license =
[GNU較寬鬆公共許可證](../Page/GNU較寬鬆公共許可證.md "wikilink") | website =
<http://www.mono-project.com/GtkSharp> }}

**Gtk\#**是個[.NET的函式庫](../Page/.NET_Framework.md "wikilink")，用來[繫結](../Page/綁紮.md "wikilink")[GTK+](../Page/GTK+.md "wikilink")
[GUI函式庫](../Page/圖形用戶界面.md "wikilink")。它讓你可以使用[Mono或其他相容](../Page/Mono.md "wikilink")[CLR的語言來開發GNOME應用程式](../Page/公共語言運行庫.md "wikilink")。

Gtk\#像其他現在的視窗函式庫一樣，採用事件驅動，讓開發者可以在視窗元件的事件被觸發時，處理要做的事情。

以Gtk\#建立的應用程式可以執行在許多平台上，如[Linux](../Page/Linux.md "wikilink")、[Microsoft
Windows與](../Page/Microsoft_Windows.md "wikilink")[Mac OS
X等](../Page/Mac_OS_X.md "wikilink")。[Mono的Windows版本裡面就直接將GTK](../Page/Mono.md "wikilink")+、Gtk\#包在裡面，並且提供了可以讓應用程式看起來像原生Windows應用程式的主題。從[Mono](../Page/Mono.md "wikilink")
1.9開始，在[Mac OS X上執行Gtk](../Page/Mac_OS_X.md "wikilink")\#應用程式將不再需要[X
Window系統](../Page/X_Window系統.md "wikilink")。

在[GUI設計上](../Page/圖形用戶界面.md "wikilink")，[Glade是個方便的設計工具](../Page/Glade.md "wikilink")，可以搭配Glade\#來使用。此外還有Stetic（整合在[MonoDevelop裡面](../Page/MonoDevelop.md "wikilink")）也可以使用。

## 參考

''此文章主要翻譯自同樣以[GNU自由文檔許可證文本授權的](../Page/Wikipedia:GNU自由文檔許可證文本.md "wikilink")[Mono網站](http://www.mono-project.com/)上。

## 外部連結

  - [Gtk\#官方網站](http://gtk-sharp.sourceforge.net/)
  - [Mono網站上對Gtk\#的說明](http://www.mono-project.com/GtkSharp)

[Category:应用程序接口](../Category/应用程序接口.md "wikilink")
[Category:部件工具箱](../Category/部件工具箱.md "wikilink")
[Category:软件开发](../Category/软件开发.md "wikilink")