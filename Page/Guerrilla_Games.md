**Guerrilla
Games**（有译为“游骑兵工作室”）是一家隸屬於[索尼互動娛樂的](../Page/索尼互動娛樂.md "wikilink")[荷蘭第一方遊戲開發商](../Page/荷蘭.md "wikilink")，曾為索尼電腦娛樂開發[殺戮地帶以及為](../Page/殺戮地帶.md "wikilink")[Eidos
Interactive開發](../Page/Eidos_Interactive.md "wikilink")[越南1967](../Page/越南1967.md "wikilink")。

該公司原名為**Lost Boys Games**，是荷蘭多媒體公司Lost
Boys的一部份，2003年7月以Guerrilla的名字重新開辦。Lost
Boys Games在Lost
Boys收購兩家有希望的[荷蘭遊戲開發商後組成](../Page/荷蘭.md "wikilink")，分別是由Arjan
Brussee創辦的Orangegames以及由Arnout van der Kamp創辦的Digital Infinity。

Lost Boys公司曾經過數次兼併，根據推測不希望保留其遊戲分部，Lost Boys的創辦人Michiel
Mol將其歸入他新創立的媒體企業Media
Republic中並將其重命名為Guerrilla Games。

2004年Guerrilla
Games與索尼電腦娛樂簽訂一個獨佔協議，在這個協議下Guerrilla將會專為[索尼的遊戲平台](../Page/索尼.md "wikilink")[PS2](../Page/PS2.md "wikilink")、[PS3](../Page/PS3.md "wikilink")、[PS4](../Page/PS4.md "wikilink")、[PSP與](../Page/PSP.md "wikilink")[PSVita等開發獨佔遊戲](../Page/PSVita.md "wikilink")。

2005年12月索尼宣佈收購Guerrilla Games，Guerrilla
Games將繼續開發[PlayStation遊戲機的獨佔遊戲](../Page/PlayStation.md "wikilink")。本次收購背後的財務細節並未提供。
\[1\]

除了自身开发电子游戏软件以外，Guerrilla
Games还有为索尼的其他工作室和游戏作品提供协助，例如[小岛秀夫的](../Page/小岛秀夫.md "wikilink")《[死亡搁浅](../Page/死亡搁浅.md "wikilink")》\[2\]

## 公司作品

### Lost Boy Games时期開發的遊戲

  - [Tiny Toon Adventures: Dizzy's Candy
    Quest](../Page/Tiny_Toon_Adventures:_Dizzy's_Candy_Quest.md "wikilink")（2001年）
    GBC
  - [Rhino Rumble](../Page/Rhino_Rumble.md "wikilink")（2002年）
    GBC
  - [Black Belt
    Challenge](../Page/Black_Belt_Challenge.md "wikilink")（2002年）
    GBA
  - [Invader](../Page/Invader.md "wikilink")（2002年）
    GBA

### Guerrilla Games时期開發的遊戲

<table>
<thead>
<tr class="header">
<th><p>发售时间</p></th>
<th><p>游戏名称</p></th>
<th><p>游戏类别</p></th>
<th><p>发行平台</p></th>
<th><p>发行商</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2004</p></td>
<td><p><br />
Shellshock: Nam '67</p></td>
<td><p><a href="../Page/第三人称射击游戏.md" title="wikilink">第三人称射击</a></p></td>
<td><p><a href="../Page/PlayStation_2.md" title="wikilink">PlayStation 2</a>、<a href="../Page/Xbox.md" title="wikilink">Xbox</a>、<a href="../Page/Windows.md" title="wikilink">Windows</a></p></td>
<td><p><a href="../Page/Eidos.md" title="wikilink">Eidos</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p><a href="../Page/殺戮地帶.md" title="wikilink">殺戮地帶</a><br />
Killzone</p></td>
<td><p><a href="../Page/第一人称射击游戏.md" title="wikilink">第一人称射击</a></p></td>
<td><p>PlayStation 2</p></td>
<td><p><a href="../Page/索尼电脑娱乐.md" title="wikilink">索尼电脑娱乐</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006</p></td>
<td><p><br />
Killzone: Liberation</p></td>
<td><p>第三人称射击</p></td>
<td><p><a href="../Page/PlayStation_Portable.md" title="wikilink">PlayStation Portable</a></p></td>
<td><p>索尼电脑娱乐</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><p><a href="../Page/殺戮地帶2.md" title="wikilink">殺戮地帶2</a><br />
Killzone 2</p></td>
<td><p>第一人称射击</p></td>
<td><p><a href="../Page/PlayStation_3.md" title="wikilink">PlayStation 3</a></p></td>
<td><p>索尼电脑娱乐</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011</p></td>
<td><p><a href="../Page/殺戮地帶3.md" title="wikilink">殺戮地帶3</a><br />
Killzone 3</p></td>
<td><p>第一人称射击</p></td>
<td><p>PlayStation 3</p></td>
<td><p>索尼电脑娱乐</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013</p></td>
<td><p><br />
Killzone: Mercenary</p></td>
<td><p>第一人称射击</p></td>
<td><p><a href="../Page/PlayStation_Vita.md" title="wikilink">PlayStation Vita</a></p></td>
<td><p>索尼电脑娱乐</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p><br />
Killzone Shadow Fall</p></td>
<td><p>第一人称射击</p></td>
<td><p><a href="../Page/PlayStation_4.md" title="wikilink">PlayStation 4</a></p></td>
<td><p>索尼电脑娱乐</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017</p></td>
<td><p><a href="../Page/地平線_黎明時分.md" title="wikilink">地平线：零之曙光</a><br />
Horizon Zero Dawn</p></td>
<td><p><a href="../Page/動作角色扮演遊戲.md" title="wikilink">動作角色扮演</a></p></td>
<td><p>PlayStation 4</p></td>
<td><p>索尼互动娱乐</p></td>
<td></td>
</tr>
</tbody>
</table>

### 游戏引擎

在其发售后，游戏信息内声称为一款自有的未命名引擎开发；之后于2015年6月，宣布《地平线：零之曙光》的引擎与《杀戮地带：暗影坠落》相同\[3\]。

Guerrilla Games董事Hermen Hulst在PlayStation Experience
2016的说法，Decima具备构建人工智能（AI）、物理效果和逻辑的工具和功能，以及用于创建整个游戏世界的资源库\[4\]Decima引擎发布将原生支持[4K解析度与](../Page/4K解析度.md "wikilink")[HDR](../Page/高动态范围成像.md "wikilink")。\[5\]
引擎以日本江户时期的人工岛[出島命名](../Page/出島.md "wikilink")（拼写为Dejima），正是17世纪时荷兰商馆所在地。

## 参见

  - [Decima (游戏引擎)](../Page/Decima_\(游戏引擎\).md "wikilink")

  - ，原为**Millennium Interactive Ltd.**，后改建为**SCEE
    Cambridge**，2012年成为Guerrilla Games分部门。

## 参考资料

## 外部連結

  -
[Category:2000年開業電子遊戲公司](../Category/2000年開業電子遊戲公司.md "wikilink")
[Category:Guerrilla Games](../Category/Guerrilla_Games.md "wikilink")
[Category:索尼互動娛樂遊戲製作工作室](../Category/索尼互動娛樂遊戲製作工作室.md "wikilink")
[Category:電子遊戲開發公司](../Category/電子遊戲開發公司.md "wikilink")
[Category:荷蘭電子遊戲公司](../Category/荷蘭電子遊戲公司.md "wikilink")
[Category:阿姆斯特丹公司](../Category/阿姆斯特丹公司.md "wikilink")

1.  [Sony acquires Guerrilla
    Games](http://www.gamespot.com/articles/sony-acquires-guerrilla-games/1100-6140877/).GameSpot.2005-12-07.\[2016-12-04\].
2.  [Kojima Partnering With Killzone, Horizon Dev Guerrilla for Death
    Stranding](http://www.gamespot.com/articles/kojima-partnering-with-killzone-horizon-dev-guerri/1100-6445954/).GameSpot.2016-12-03.\[2016-12-04\].
3.
4.
5.