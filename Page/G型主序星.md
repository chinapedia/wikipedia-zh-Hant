[Sun920607.jpg](https://zh.wikipedia.org/wiki/File:Sun920607.jpg "fig:Sun920607.jpg")是黃矮星的標準範例。\]\]
**黃矮星**，在[天文學上的正式名稱為GV恆星](../Page/天文學.md "wikilink")，是光譜型態為[G](../Page/恆星光譜.md "wikilink")，發光度為[V的](../Page/矮星.md "wikilink")[主序星](../Page/主序星.md "wikilink")。這一類恆星的質量大約在0.8至1.0[太陽質量](../Page/太陽質量.md "wikilink")，表面的有效溫度在5,300至6,000[K](../Page/熱力學溫標.md "wikilink")\[1\]<sup>,表VII、VIII。</sup>。與其他的[主序星一樣](../Page/主序星.md "wikilink")，GV恆星在核心進行將[氫融合成](../Page/氫.md "wikilink")[氦的](../Page/氦.md "wikilink")[核融合反應](../Page/核融合.md "wikilink")。\[2\][太陽是最著名的](../Page/太陽.md "wikilink")，也是最易見到的GV恆星（黃矮星），每秒鐘將大約6億噸的[氫轉換成](../Page/氫.md "wikilink")[氦](../Page/氦.md "wikilink")，將400萬噸的質量轉換成[能量](../Page/能量.md "wikilink")。\[3\]\[4\]
其他的GV恆星，還有[南門二](../Page/南門二.md "wikilink")（半人馬座α）、[鯨魚座τ和](../Page/鯨魚座τ.md "wikilink")[飛馬座51](../Page/飛馬座51.md "wikilink")。\[5\]\[6\]\[7\]

黃矮星其實是一個錯誤的名稱，因為光譜類型為G的恆星實際顏色是從白色到黃色的，像太陽在早期的顏色是白色，只有到晚期才會成為黃色。\[8\]
參考[光譜分類的顏色與光譜圖](../Page/恆星分類#耶基光譜分類法.md "wikilink")，太陽實際上是白色的，被錯誤的看成黃色是因為背景的藍天造成的（在藍色的襯托下變得偏黃），而在地平線附近的太陽呈現紅色，則是大氣層的[瑞利散射造成的](../Page/瑞利散射.md "wikilink")。

一顆GV恆星燃燒[氫的生命期大約是](../Page/氫.md "wikilink")100億年，當他耗盡了核心的氫燃料之後，這一類恆星便會膨脹許多倍，發展成一顆[紅巨星](../Page/紅巨星.md "wikilink")，像是[畢宿五](../Page/畢宿五.md "wikilink")（金牛座α）。\[9\]
最後，這顆紅巨星會拋掉它外層的氣體，成為[行星狀星雲](../Page/行星狀星雲.md "wikilink")，而這時裸露出來的核心便是一顆體積小、密度高的[白矮星](../Page/白矮星.md "wikilink")，在往後的歲月中只會持續的降溫與壓縮。\[10\]

## 參考資料

## 外部連結

  - [Star
    Types](http://www.enchantedlearning.com/subjects/astronomy/stars/startypes.shtml)
    at Enchanted Learning.

## 相關條目

  - [太陽相似體](../Page/太陽相似體.md "wikilink")
  - [紅矮星](../Page/紅矮星.md "wikilink")
  - [棕矮星](../Page/棕矮星.md "wikilink")
  - [橙矮星](../Page/橙矮星.md "wikilink")
  - [黃巨星](../Page/黃巨星.md "wikilink")

[Category:主序星](../Category/主序星.md "wikilink")
[黃矮星](../Category/黃矮星.md "wikilink")
[Category:恆星類型](../Category/恆星類型.md "wikilink")
[Category:矮恒星](../Category/矮恒星.md "wikilink")

1.  \[<http://adsabs.harvard.edu/abs/1981A&AS>...46..193H Empirical
    bolometric corrections for the main-sequence\], G. M. H. J. Habets
    and J. R. W. Heintze, *Astronomy and Astrophysics Supplement* **46**
    (November 1981), pp. 193–237.

2.  [Stellar Evolution: Main Sequence to
    Giant](http://physics.valpo.edu/courses/a101/notes/070409_LowMass.pdf),
    class notes, Astronomy 101, [Valparaiso
    University](../Page/Valparaiso_University.md "wikilink"), accessed
    on line [June 19](../Page/June_19.md "wikilink"), 2007.

3.  [Why Does The Sun
    Shine?](http://www.astronomy.ohio-state.edu/~ryden/ast162_1/notes2.html),
    lecture, Barbara Ryden, Astronomy 162, [Ohio State
    University](../Page/Ohio_State_University.md "wikilink"), accessed
    on line [June 19](../Page/June_19.md "wikilink"), 2007.

4.  [Sun](http://www.ari.uni-heidelberg.de/aricns/cnspages/4c00001.htm)
    , entry at [ARICNS](../Page/ARICNS.md "wikilink"), accessed [June
    19](../Page/June_19.md "wikilink"), 2007.

5.  [Alpha Centauri
    A](http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Alpha+Centauri+A),
    [SIMBAD](../Page/SIMBAD.md "wikilink") query result. Accessed on
    line [December 4](../Page/December_4.md "wikilink"), 2007.

6.  [Tau Ceti](http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Tau+Ceti),
    [SIMBAD](../Page/SIMBAD.md "wikilink") query result. Accessed on
    line [December 4](../Page/December_4.md "wikilink"), 2007.

7.  [51
    Pegasi](http://simbad.u-strasbg.fr/simbad/sim-id?Ident=51+Pegasi),
    [SIMBAD](../Page/SIMBAD.md "wikilink") query result. Accessed on
    line [December 4](../Page/December_4.md "wikilink"), 2007.

8.  [What Color Are the
    Stars?](http://www.vendian.org/mncharity/dir3/starcolor/), Mitchell
    N. Charity's webpage, accessed [November
    25](../Page/November_25.md "wikilink"), 2007

9.  [SIMBAD](../Page/SIMBAD.md "wikilink"),
    [entry](http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Aldebaran)
    for [Aldebaran](../Page/Aldebaran.md "wikilink"), accessed on line
    [June 19](../Page/June_19.md "wikilink"), 2007.

10.