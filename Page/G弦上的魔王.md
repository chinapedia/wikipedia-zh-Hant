《**G弦上的魔王**》（）是[AKABEiSOFT2](../Page/AKABEiSOFT2.md "wikilink")（）於2008年5月29日發售的[十八禁](../Page/成人遊戲.md "wikilink")[冒險遊戲](../Page/冒險遊戲.md "wikilink")，自2007年11月29日開始因品質提升及開發遲緩的原因三度延期。在發售前已獲得高評價，Getchu.com
2008年上半年統計中是第三名\[1\]。

## 故事簡介

主角**淺井京介**日間在學校只是個喜歡[古典音樂的普通少年](../Page/西洋古典音樂.md "wikilink")，和同學**美輪樁姬**、**相澤榮一**、義妹**淺井花音**相熟。而在夜晚，則是養父**淺井權三**的公司中的重要人物。在冬天，少女**宇佐美哈爾**轉校來到京介的學校，她就是以前和京介一起的「勇者」。在這時，「**魔王**」出現在這個城市，一連串事件亦開始出現。勇者與魔王間的心理戰，鬥智的純愛故事就如此揭幕。

## 登場人物

  -
    故事男主角，非常喜歡[巴哈的](../Page/约翰·塞巴斯蒂安·巴赫.md "wikilink")[G弦上的詠嘆調](../Page/G弦上的詠嘆調.md "wikilink")。入夜後為權三工作，且表現出色，不過並不參與非法工作。原本沒有必要上學，聲稱上學是為了減壓和娛樂。日夜中的京介是性格完全不同的兩人。校內只有水羽知道他有另一個身份。

<!-- end list -->

  -
    說話古怪，要別人稱自己作「勇者」，但行事深思熟慮，懂得拉[小提琴](../Page/小提琴.md "wikilink")。自稱是[北極](../Page/北極.md "wikilink")（或[南極](../Page/南極.md "wikilink")）出身，喜歡[企鵝](../Page/企鵝.md "wikilink")。因魔王而入讀京介的學校。
    注意雖然可以寫作，但據原畫有葉繪製的人物色紙\[2\]和角色歌CD封面可見其名字的罗马字標記為"Hal"，即是外來語名字。故正确的方式为依照外來語形式译作“哈爾”。

<!-- end list -->

  -
    京介的同年[義妹](../Page/義妹.md "wikilink")，讀同一所學校但非同居。[花式溜冰的實力是全國級水準](../Page/花式溜冰.md "wikilink")。對溜冰以外的事情並不關心。

<!-- end list -->

  -
    京介的同班同學，而且是班長，在家中是長女。總是拿著日記和寫備忘。

<!-- end list -->

  -
    京介的同班同學，學校理事長的女兒，似乎對任何人都不關心。兩年前曾送給京介巧克力，卻被丟進垃圾桶。

<!-- end list -->

  -
    哈爾的朋友，因和哈爾一樣的原因而入讀學校，頭腦和溝通能力非常高。

<!-- end list -->

  -
    京介的義父，想要讓京介承繼自己。放[高利貸賺錢](../Page/高利貸.md "wikilink")，完全的自我中心，喜歡[打獵](../Page/打獵.md "wikilink")。

<!-- end list -->

  -
    京介的朋友，平常裝作可愛以接近女生，實際十分[腹黑](../Page/腹黑.md "wikilink")，卻只有京介知道其本性。自稱興趣是對抗[戀愛資本主義](../Page/戀愛資本主義.md "wikilink")。

<!-- end list -->

  -
    一切不明。概念來自舒伯特所作的藝術歌曲「魔王」，取該歌曲中誘拐孩童的情節。

## 製作人員

  - 企劃、劇本：
  - 原畫：[有葉](../Page/有葉.md "wikilink")

## 音樂

  - 開頭歌曲「Answer」
    編曲：藤田淳平（[Elements
    Garden](../Page/Elements_Garden.md "wikilink")），作詞：Kanoko，歌：[片霧烈火](../Page/片霧烈火.md "wikilink")
  - 插入歌曲「Close Your Eyes」
    作詞、作曲：[志倉千代丸](../Page/志倉千代丸.md "wikilink")，編曲：[磯江俊道](../Page/磯江俊道.md "wikilink")，歌：[彩音](../Page/彩音.md "wikilink")
  - 結尾歌曲「雪の羽 時の風」
    作詞：[wight](../Page/wight.md "wikilink")，作曲、編曲：[bassy](../Page/bassy.md "wikilink")、歌：[Barbarian
    On The Groove](../Page/Barbarian_On_The_Groove.md "wikilink") feat.
    [茶太](../Page/茶太.md "wikilink")
  - 背景音樂
    大部分都是[古典音樂名曲的改編](../Page/西洋古典音樂.md "wikilink")。

## 註解

<div class="references-small">

<references />

</div>

## 參考資料

  - [Getchu.com：](http://www.getchu.com/soft.phtml?id=297425)

## 參見

  - [催淚遊戲](../Page/催淚遊戲.md "wikilink")
  - [AKABEiSOFT2](../Page/AKABEiSOFT2.md "wikilink")
  - [巴赫](../Page/巴赫.md "wikilink")：[G弦上的咏叹调](../Page/G弦上的咏叹调.md "wikilink")
  - [舒伯特](../Page/舒伯特.md "wikilink")：[魔王
    (歌剧)](../Page/魔王_\(歌剧\).md "wikilink")

## 外部連結

  - [遊戲官方網站](https://web.archive.org/web/20081211052523/http://www.akabeesoft2.com/g_sen/)（需年齡確認），存於[網際網路檔案館](../Page/網際網路檔案館.md "wikilink")
  - [AKABEiSOFT2 OFFICIAL WEB
    SITE](https://web.archive.org/web/20081217095923/http://akabeesoft2.com/)（需年齡確認），存於[網際網路檔案館](../Page/網際網路檔案館.md "wikilink")

[Category:美少女遊戲](../Category/美少女遊戲.md "wikilink")
[Category:冒險遊戲](../Category/冒險遊戲.md "wikilink")
[Category:2008年日本成人遊戲](../Category/2008年日本成人遊戲.md "wikilink")

1.  [](http://www.getchu.com/pc/2008-1salesranking.html)
2.  [](http://www.akabeesoft2.com/g_sen/shikishi/009.jpg)