**H.263**是由[ITU-T用於](../Page/ITU-T.md "wikilink")[視訊會議的低](../Page/視訊會議.md "wikilink")[码率](../Page/码率.md "wikilink")[影像编码标准](../Page/影像编码.md "wikilink")，属于[影像编解码器](../Page/影像编解码器.md "wikilink")。H.263最初设计为基于H.324的系统进行传输（即基于[公共交换电话网和其它基于](../Page/公共交换电话网.md "wikilink")[电路交换的](../Page/电路交换.md "wikilink")[网络进行視訊会议和視訊电话](../Page/网络.md "wikilink")）。后来发现H.263也可以成功的应用於[H.323](../Page/H.323.md "wikilink")（基于[RTP](../Page/RTP.md "wikilink")／[IP网络的視訊会议系统](../Page/IP.md "wikilink")），[H.320](../Page/H.320.md "wikilink")（基于[综合业务数字网的視訊会议系统](../Page/综合业务数字网.md "wikilink")），[RTSP](../Page/RTSP.md "wikilink")（[流式媒体传输系统](../Page/流式媒体.md "wikilink")）和[SIP](../Page/SIP.md "wikilink")（基于[因特网的視訊会议](../Page/因特网.md "wikilink")）。

基于之前的影像编码国际标准（[H.261](../Page/H.261.md "wikilink")，[MPEG-1和](../Page/MPEG-1.md "wikilink")[H.262](../Page/H.262.md "wikilink")／[MPEG-2](../Page/MPEG-2.md "wikilink")），H.263的性能有了革命性的提高。它的第一版于1995年完成，在所有码率下都优于之前的[H.261](../Page/H.261.md "wikilink")。之后还有在1998年增加了新的功能的第二版H.263+，或者叫H.263v2，以及在2000年完成的第三版H.263++，即H.263v3。早期的H.263新增以下的附加（annexes）:

  - Annex A - Inverse transform accuracy specification
  - Annex B - Hypothetical Reference Decoder
  - Annex C - Considerations for Multipoint
  - Annex D - Unrestricted Motion Vector mode
  - Annex E - Syntax-based Arithmetic Coding mode
  - Annex F - Advanced Prediction mode
  - Annex G - PB-frames mode
  - Annex H - Forward Error Correction for coded video signal

在H.263之后，[ITU-T](../Page/ITU-T.md "wikilink")（在与[MPEG的合作下](../Page/MPEG.md "wikilink")）的下一代影像编解码器是[H.264](../Page/H.264.md "wikilink")，或者叫[AVC以及](../Page/AVC.md "wikilink")[MPEG-4第](../Page/MPEG-4.md "wikilink")10部分。由于H.264在性能上超越了H.263很多，现在通常认为H.263是一个过时的标准（虽然它的开发完成并不是很久以前的事情）。大多数新的[視訊会议产品都已经支持了H](../Page/視訊会议.md "wikilink").264影像编解码器，就像以前支持H.263和H.261一样。

## H.263v2

**H.263v2**（通常也叫做**H.263+**或者1998年版H.263）是[ITU-T](../Page/ITU-T.md "wikilink")
H.263
[影像编码标准第二版的非正式名称](../Page/影像.md "wikilink")。它保持了原先版本H.263的所有技术，但是通过增加了几个附录显著的提高了编码效率并提供了其它的一些能力，例如增强了抵抗传输[信道的数据丢失的能力](../Page/信道.md "wikilink")（Robustness）。

H.263+项目于1998年2月在ITU正式通过。接下来一个被称为"H.263++"的项目被随即推出，在H.263+的基础上增加了更多的新的功能。H.263++（亦称H.263v3或2000版H.263）于2000年底完成。

1998年版的H.263新增如下的附加（annexes）:

  - Annex I - Advanced INTRA Coding mode
  - Annex J - [Deblocking Filter mode](../Page/去區塊濾波器.md "wikilink")
  - Annex K - Slice Structured mode
  - Annex L - Supplemental Enhancement Information Specification
  - Annex M - Improved PB-frames mode
  - Annex N - Reference Picture Selection mode
  - Annex O - Temporal, SNR, and Spatial Scalability mode
  - Annex P - Reference picture resampling
  - Annex Q - Reduced-Resolution Update mode (see implementors' guide
    correction as noted below)
  - Annex R - Independent Segment Decoding mode
  - Annex S - Alternative INTER VLC mode
  - Annex T - Modified Quantization mode
  - Annex X - Profiles and levels definition

## 外部链接

  - [H.263影像编码比较](https://web.archive.org/web/20050615002323/http://www-mobile.ecs.soton.ac.uk/peter/h263/h263.html)
  - [IETF
    AVT工作组](https://web.archive.org/web/20011030094328/http://www.ietf.org/html.charters/avt-charter.html)
    - 分析编解码器的输出码流在[实时传输协议上打包传输的组织](../Page/实时传输协议.md "wikilink")。
      - [H.263影像码流的RTP负载格式（RFC 2190）](http://www.ietf.org/rfc/rfc2190.txt)
      - [H.263+影像码流的RTP负载格式（RFC 2429）](http://www.ietf.org/rfc/rfc2429.txt)
  - [ITU官方网站上的H.263规范](https://web.archive.org/web/20010630082835/http://www.itu.int/itudoc/itu-t/rec/h/)
  - [[英特尔公司提供的H](../Page/英特尔公司.md "wikilink").263解码器的WINDOWS作業系統安装程序](http://support.intel.com/support/createshare/camerapack/CODINSTL.HTM)
  - [I263影像编解码器](https://web.archive.org/web/19981202084714/http://members.aol.com/SlavTrainr/STsPage.html)
  - [H.263规范](https://web.archive.org/web/20030627164952/http://standards.pictel.com/ftp/video-site/)
  - [在vic上的H.263实现（有源码）](http://www4.informatik.uni-erlangen.de/Projects/ScalVico/vic/)
  - [SourceForge的OpenH263项目](https://web.archive.org/web/20050403191441/http://sourceforge.net/projects/openh263-s/)
  - [Video Demystified（一本介绍H.263的书）](http://www.video-demystified.com/)

[Category:影像编解码器](../Category/影像编解码器.md "wikilink")
[Category:ITU-T标准](../Category/ITU-T标准.md "wikilink")
[Category:影像壓縮技術](../Category/影像壓縮技術.md "wikilink")