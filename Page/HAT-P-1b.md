RJ | mass = 0.59 ± 0.04 木星質量 | density =290 ± 30 kg/m³ | surface_grav =
0.75 m/s² | temperatures = | temp_name1 = | mean_temp_1 = }}

**HAT-P-1b**是一顆圍繞[ADS 16402
B的](../Page/ADS_16402_B.md "wikilink")[太陽系外行星](../Page/太陽系外行星.md "wikilink")，位於[蠍虎座](../Page/蠍虎座.md "wikilink")，距離地球約450光年。ADS
16402 B是[ADS
16402](../Page/ADS_16402.md "wikilink")[雙星系統的其中之一](../Page/雙星.md "wikilink")。
HAT-P-1b是由哈佛-史密松天文物理中心的天文學家利用一個稱為HAT的自動小型望遠鏡網路所發現的，當時HAT-P-1b讓ADS 16402
B的亮度下降了0.6%。
它的軌道非常接近恆星，距離僅有824萬公里，公轉週期也只有4.46天\[1\]，被認為是[熱木星型的行星](../Page/熱木星.md "wikilink")。

## 參考資料

[Category:蠍虎座](../Category/蠍虎座.md "wikilink")
[Category:太陽系外行星](../Category/太陽系外行星.md "wikilink")

1.