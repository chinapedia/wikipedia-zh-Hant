**HBO
Asia**是位於[新加坡的](../Page/新加坡.md "wikilink")[電視頻道](../Page/電視頻道.md "wikilink")，服務範圍為亞洲地區，主要播出下檔的[院線片和自製的](../Page/電影.md "wikilink")[電視影集](../Page/電視影集.md "wikilink")，此外也有[電視電影](../Page/電視電影.md "wikilink")、[紀錄片等節目](../Page/紀錄片.md "wikilink")。HBO
Asia使用了美國[HBO電視網的品牌](../Page/HBO.md "wikilink")，由[時代華納和](../Page/時代華納.md "wikilink")[派拉蒙影業共同出資成立](../Page/派拉蒙影業.md "wikilink")，總公司位於[新加坡](../Page/新加坡.md "wikilink")，並在[台灣和](../Page/台灣.md "wikilink")[印度都設有分公司](../Page/印度.md "wikilink")。

## 歷史

[派拉蒙影業與](../Page/派拉蒙影業.md "wikilink")[華納兄弟在](../Page/華納兄弟.md "wikilink")1992年合作出資成立了HBO
Asia，初期透過「新加坡電纜電視」（Singapore Cable
Vision，簡稱SCV，[星和视界前身之一](../Page/星和视界.md "wikilink")）播出。在1992年於[新加坡](../Page/新加坡.md "wikilink")"SCV"
正式啟播，每天播放18小時的節目。1994年開始，HBO正式24小時播放荷里活電影，每月播放約120個節目。在2001年開始引入HBO原創系列節目，如2001年的"Band
of Brothers"等。1993年HBO Asia位於[新加坡新科技產業園](../Page/新加坡.md "wikilink")（New
Tech Park）的總部啓用\[1\]\[2\]\[3\]。其後HBO
Asia不斷在亞洲區內拓展播放涵蓋範圍，在1993年開始於[泰國和](../Page/泰国.md "wikilink")[菲律賓播放](../Page/菲律宾.md "wikilink")；1994年開始在[台灣地區和](../Page/台灣地區.md "wikilink")[印尼播放](../Page/印度尼西亚.md "wikilink")，接著[香港和](../Page/香港.md "wikilink")[馬來西亞在](../Page/馬來西亞.md "wikilink")1995年也引入HBO。而[中華人民共和國](../Page/中華人民共和國.md "wikilink")（[中國大陸](../Page/中國大陸.md "wikilink")）則也在1995年起，可在三星級以上酒店和外籍人士居住的區域收看HBO和Cinemax頻道，部分地方有线电视也有落地。

HBO
Asia在亞洲也經營多個電視頻道，除了1992年啟播的HBO之外、還有[Cinemax](../Page/Cinemax.md "wikilink")（1996年開播）、2005年開播的[HBO原創鉅獻](../Page/HBO_Signature.md "wikilink")（HBO
Signature）、2006年開播的[HBO強檔鉅獻](../Page/HBO_HiTS.md "wikilink")（HBO
HITS）及[HBO溫馨家庭](../Page/HBO_Family.md "wikilink")（HBO Family）。

## 節目

HBO
Asia從1994年開始增加影片授權來源，陸續與[索尼影視娛樂和](../Page/索尼影視娛樂.md "wikilink")[環球片場簽訂獨家播放協議](../Page/環球片場.md "wikilink")；[迪士尼所屬的](../Page/华特迪士尼公司.md "wikilink")[好萊塢影業](../Page/好萊塢影業.md "wikilink")（Hollywood
Pictures）和[試金石影業之後在](../Page/試金石影業.md "wikilink")1996年也開始獨家供應影片給HBO
Asia。[夢工廠亦於](../Page/夢工廠.md "wikilink")1998年成為HBO Asia的獨家供應片商。

HBO會播放首輪電影，即大部份系列頻道播放的電影會先在HBO播放，當中包括百大電影。2007年美國票房前百大電影有約百分之七十已在HBO播放。另外，HBO也設立不同節目時段，提供不同類型的電影。

新上檔的電影通常在週日晚間10點首播。首播後可能會在接下來的一週內多次重播。此外每月更新的「HBO
Central」節目則提供了當月播出的節目資訊，以及好萊塢的娛樂新聞和電影幕後花絮。HBO原創節目則在週一晚間10點首播。另外於每日晚間七時、九時及十一時播放精選電影。

### 內容審查

由於HBO
Asia的總部位於[新加坡](../Page/新加坡.md "wikilink")，因此必須遵循新加坡法律修剪限制級節目，被修剪的節目包括《[慾望城市](../Page/慾望城市.md "wikilink")》和《[黑道家族](../Page/黑道家族.md "wikilink")》等。

HBO的原創影集《[我家也有大明星](../Page/我家也有大明星.md "wikilink")》在[菲律賓因為在當時尚未通過政府單位](../Page/菲律賓.md "wikilink")[MTRCB的分級審查](../Page/MTRCB.md "wikilink")，在其中一個有線電視系統上曾停播了三週。

在[越南](../Page/越南.md "wikilink")，所有被認定內容包含扭曲或不正確的[越戰](../Page/越戰.md "wikilink")、[越南共產黨和](../Page/越南共產黨.md "wikilink")[共產主義資訊的電影](../Page/共產主義.md "wikilink")，都會以黑色畫面遮蓋。此外，如果越南文化部認定內容包含社會無法接受的過度性描寫或恐怖影像，影片也會遭到禁播。

在[中国](../Page/中国.md "wikilink")，[中国中央电视台旗下的](../Page/中国中央电视台.md "wikilink")[中国国际电视总公司境外卫星代理部接收HBO信号](../Page/中国国际电视总公司.md "wikilink")，通过亚太6号卫星（东经134度）发射KU波段信号。该服务一般只提供给三星级或以上的涉外宾馆酒店，外国人居住区，领事馆及大使馆。同时，央视旗下中数传媒、[上海广播电视台旗下](../Page/上海广播电视台.md "wikilink")[百视通均与HBO合作](../Page/百视通.md "wikilink")，在付费电视频道——CCTV第一剧场频道、SITV都市剧场频道均提供播放HBO剧集时段，另外，全国各省市广电网络有线互动电视均陆续上线HBO鼎级剧场等专区，只需开通业务了即可享受点播美剧服务。2014年，[腾讯视频与HBO达成战略合作](../Page/腾讯视频.md "wikilink")，HBO出品的多部剧集由腾讯视频独家代理，成为HBO在中国内地互联网视频上独家官方授权播放平台。

## 旗下頻道

### HBO品牌頻道

  - HBO
  - HBO HD
  - [HBO原創鉅獻](../Page/HBO原創鉅獻.md "wikilink")（HBO Signature）
  - HBO原創鉅獻 HD（HBO Signature HD）
  - [HBO溫馨家庭](../Page/HBO溫馨家庭.md "wikilink")（HBO Family）
  - HBO溫馨家庭 HD（HBO Family HD）
  - [HBO強檔鉅獻](../Page/HBO強檔鉅獻.md "wikilink")（HBO Hits）
  - HBO強檔鉅獻（HBO Hits HD）
  - HBO On Demand
  - [Cinemax](../Page/Cinemax.md "wikilink")

### 其他/代理頻道

  - [RED by HBO](../Page/RED_by_HBO.md "wikilink")
  - [華納電視頻道](../Page/華納電視頻道.md "wikilink")（WarnerTV）
  - [寶寶世界頻道](../Page/寶寶世界頻道.md "wikilink")（BabyFirst）
  - [夢工廠頻道](../Page/夢工廠頻道.md "wikilink")（DreamWorks Animation
    Channel）（預計2015年6月於亞洲19個地區開播，其中包括韓國、台灣、香港。）

## 播映區域

亞洲目前在以下的22個國家或地區可收看HBO Asia提供的的全部或部分頻道與服務。

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
<!-- end list -->

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 資料來源

## 外部連結

  -
[Category:HBO](../Category/HBO.md "wikilink")
[Category:新加坡媒體](../Category/新加坡媒體.md "wikilink")
[Category:新加坡公司](../Category/新加坡公司.md "wikilink")
[Category:印度電視台](../Category/印度電視台.md "wikilink")
[Category:台灣電視台](../Category/台灣電視台.md "wikilink")
[Category:香港電視播放頻道](../Category/香港電視播放頻道.md "wikilink")
[Category:台灣電視播放頻道](../Category/台灣電視播放頻道.md "wikilink")
[Category:電影頻道](../Category/電影頻道.md "wikilink")
[Category:1992年成立的电视台或电视频道](../Category/1992年成立的电视台或电视频道.md "wikilink")

1.  <http://sg.kompass.com/c/home-box-office-singapore-pte-ltd/sg002104/>

2.  <http://www.mda.gov.sg/Documents/Ebrochures/7910_-_MediaServices_Brochure_FINAL.pdf>

3.  [](https://books.google.com.tw/books?id=z2SUAgAAQBAJ&pg=PA66&lpg=PA66&dq=New+Tech+Park+HBO++1993&source=bl&ots=RgFlAGvIm7&sig=DPMwR12yBIdghaQj8TOcLIBrVJY&hl=zh-TW&sa=X&ved=0CC4Q6AEwA2oVChMI98bM0IrQyAIVSpKUCh1yAwG9#v=onepage&q=New%20Tech%20Park%20HBO%20%201993&f=false)