**高畫質多媒體介面**（，簡稱**HDMI**）是一種全[數位化](../Page/數位.md "wikilink")[影像和](../Page/影像.md "wikilink")[聲音傳送介面](../Page/聲音.md "wikilink")，可以傳送未[壓縮的](../Page/數位壓縮.md "wikilink")[音訊及](../Page/音頻.md "wikilink")[視訊訊號](../Page/視頻.md "wikilink")。HDMI可用於[機上盒](../Page/機上盒.md "wikilink")、[DVD播放機](../Page/DVD播放機.md "wikilink")、[個人電腦](../Page/個人電腦.md "wikilink")、[電視遊樂器](../Page/電視遊樂器.md "wikilink")、綜合擴大機、數位音響與[電視機等設備](../Page/電視機.md "wikilink")。HDMI可以同時傳送音訊和視訊訊號，由於音訊和視訊訊號採用同一條線材，大大簡化系統線路的安裝難度。

## 簡述

HDMI是被設計來取代較舊的[類比訊號影音傳送介面如](../Page/類比訊號.md "wikilink")或[RCA等端子的](../Page/RCA端子.md "wikilink")。它支援各類[電視與](../Page/電視.md "wikilink")[電腦影像格式](../Page/電腦.md "wikilink")，包括[SDTV](../Page/SDTV.md "wikilink")、[HDTV視訊畫面](../Page/HDTV.md "wikilink")，再加上多聲道數位音訊。HDMI与去掉音频传输功能的[UDI都继承](../Page/UDI.md "wikilink")[DVI的核心技术](../Page/DVI.md "wikilink")「传输最小化差分訊號」TMDS，从本质上来说仍然是DVI的扩展。DVI、HDMI、UDI的视频内容都以即时、专线方式进行传输，这可以保证视频流量大时不会发生堵塞的现象。每个像素数据量为24位。信号的时序与[VGA极为类似](../Page/VGA.md "wikilink")。画面是以逐行的方式被发送，并在每一行与每祯画面发送完毕后加入一个特定的空白时间（类似模拟扫描线），并没有将数据“Micro-Packet
Architecture（微封包架构）”化，也不会只更新前后两帧画面改变的部分。每张画面在该更新时都会被完整的重新发送。規格初制訂時其最大[畫素傳輸率為](../Page/畫素.md "wikilink")165Mpx/sec，足以支援[1080p畫質每秒](../Page/1080p.md "wikilink")60張畫面，或者[UXGA](../Page/UXGA.md "wikilink")[解像度](../Page/解像度.md "wikilink")（1600x1200）；後來在HDMI
1.3規格中擴增為340Mpx/秒，以符合未來可能的需求。

而[DisplayPort一开始则面向液晶显示器开发](../Page/DisplayPort.md "wikilink")，采用“Micro-Packet
Architecture(微封包架构)”传输架构，视频内容以封包方式传送，这一点同DVI、HDMI等视频传输技术有着明显区别。也就是说，HDMI的出现取代模拟信号视频，而DisplayPort的出现则取代的是DVI和VGA接口。

HDMI也支援非壓縮的8聲道數位音訊傳送（取樣率192[kHz](../Page/kHz.md "wikilink")，資料長度24bits/sample），以及任何壓縮音訊串流如[Dolby
Digital或](../Page/Dolby_Digital.md "wikilink")[DTS](../Page/DTS.md "wikilink")，亦支援[SACD所使用的](../Page/SACD.md "wikilink")8聲道的1bit
DSD訊號。在HDMI 1.3規格中，又追加超高資料量的無失真壓縮音訊串流如[Dolby
TrueHD與](../Page/Dolby_TrueHD.md "wikilink")[DTS-HD的支援](../Page/DTS-HD.md "wikilink")。

標準的Type A HDMI接頭有19個腳位，另有一種支援更高解像度的Type B接頭被定義出來，但目前仍無任何廠商使用Type
B接頭。Type
B接頭有29個腳位，容許其傳送擴展的視訊通道以應付未來的高[畫質需求](../Page/畫質.md "wikilink")，如[WQSXGA](../Page/WQSXGA.md "wikilink")（3200x2048）。

Type A
HDMI可向下相容於現今多數[顯示器與](../Page/顯示器.md "wikilink")[顯示卡所使用的Single](../Page/顯示卡.md "wikilink")-link
[DVI](../Page/DVI.md "wikilink")-D或DVI-I介面（但不支援DVI-A），這表示採用DVI-D介面的訊號來源可以透過[轉換線驅動HDMI螢幕](../Page/轉換線.md "wikilink")，但是此種轉換方案並不支援音訊傳送與[遙控機能](../Page/遙控.md "wikilink")。此外如無[HDCP認證的DVI螢幕也將不能收看從HDMI所輸出帶有HDCP](../Page/HDCP.md "wikilink")[加密保護的視訊資料](../Page/加密.md "wikilink")（所有HDMI螢幕皆支援HDCP，但大多數DVI介面的顯示器不支援HDCP），Type
B HDMI接頭也將向下相容於Dual-link DVI介面。

[HDMI組織的發起者包括各大](../Page/HDMI組織.md "wikilink")[消費電子產品製造商](../Page/消費電子產品.md "wikilink")，如[日立製作所](../Page/日立製作所.md "wikilink")、[松下電器](../Page/松下電器.md "wikilink")、[Quasar](../Page/Quasar.md "wikilink")、[飛利浦](../Page/飛利浦.md "wikilink")、[索尼](../Page/索尼.md "wikilink")、[湯姆生RCA](../Page/湯姆生RCA.md "wikilink")、[東芝](../Page/東芝.md "wikilink")、[Silicon
Image](../Page/Silicon_Image.md "wikilink")。[數位內容保護公司](../Page/數位內容保護公司.md "wikilink")（Digital
Content Protection,
LLC）\[1\]提供HDMI介面相關的防拷保護技術。此外，HDMI也受到各主要電影製作公司如[20世紀福斯](../Page/20世紀福斯.md "wikilink")、[華納兄弟](../Page/華納兄弟.md "wikilink")、[迪士尼](../Page/迪士尼.md "wikilink")，包括[三星電子在內的各大消費電子產品製造商](../Page/三星電子.md "wikilink")，以及多家[有線電視系統業者的支援](../Page/有線電視.md "wikilink")。

## 規格

### HDMI接頭

[HDMI-HDMImini-HDMImicro.png](https://zh.wikipedia.org/wiki/File:HDMI-HDMImini-HDMImicro.png "fig:HDMI-HDMImini-HDMImicro.png")、[mini-HDMI（C）](../Page/#HDMI_C_Type.md "wikilink")、[Micro
HDMI（D）](../Page/#HDMI_D_Type.md "wikilink") \]\]

HDMI的規格書中規定四種HDMI接口，分別是：

#### HDMI A Type

[HDMI.socket.png](https://zh.wikipedia.org/wiki/File:HDMI.socket.png "fig:HDMI.socket.png")

应用于HDMI1.0版本，總共有19pin，规格为4.45 mm×13.9 mm，為最常見的HDMI接頭規格，相對等於DVI
Single-Link傳輸。在HDMI 1.2a之前，最大能傳輸165MHz的TMDS，所以最大傳輸規格只能在於1600×1200（TMDS
162.0 MHz）。

|     |                          |
| --- | ------------------------ |
| Pin | Pin定義                    |
| 1   | TMDS Data2+              |
| 2   | TMDS Data2 Shield        |
| 3   | TMDS Data2–              |
| 4   | TMDS Data1+              |
| 5   | TMDS Data1 Shield        |
| 6   | TMDS Data1–              |
| 7   | TMDS Data0+              |
| 8   | TMDS Data0 Shield        |
| 9   | TMDS Data0–              |
| 10  | TMDS Clock+              |
| 11  | TMDS Clock Shield        |
| 12  | TMDS Clock–              |
| 13  | CEC                      |
| 14  | Reserved（N.C. on device） |
| 15  | SCL                      |
| 16  | SDA                      |
| 17  | DDC/CEC Ground           |
| 18  | \+5V Power               |
| 19  | Hot Plug Detect          |

#### HDMI B Type

应用于HDMI1.0版本，规格为4.45 mm×21.2 mm，總共有29pin,可傳輸HDMI A
type兩倍的TMDS資料量，相對等於DVI Dual-Link傳輸，用於傳輸高解析度（WQXGA
2560×1600以上）。因為HDMI A type只有Single-Link的TMDS傳輸，如果要傳輸成HDMI B
type的訊號，則必須要兩倍的傳輸效率，會造成TMDS的Tx、Rx的工作頻率必須提高至270MHz以上。而在HDMI
1.3 IC出現之前，市面上大部分的TMDS Tx、Rx只能穩定在165MHz以下工作。此类接口未应用在任何产品中。

|     |                          |
| --- | ------------------------ |
| Pin | Pin定義                    |
| 1   | TMDS Data2+              |
| 2   | TMDS Data2 Shield        |
| 3   | TMDS Data2–              |
| 4   | TMDS Data1+              |
| 5   | TMDS Data1 Shield        |
| 6   | TMDS Data1–              |
| 7   | TMDS Data0+              |
| 8   | TMDS Data0 Shield        |
| 9   | TMDS Data0–              |
| 10  | TMDS Clock+              |
| 11  | TMDS Clock Shield        |
| 12  | TMDS Clock–              |
| 13  | TMDS Data5+              |
| 14  | TMDS Data5 Shield        |
| 15  | TMDS Data5-              |
| 16  | TMDS Data4+              |
| 17  | TMDS Data4 Shield        |
| 18  | TMDS Data4-              |
| 19  | TMDS Data3+              |
| 20  | TMDS Data3 Shield        |
| 21  | TMDS Data3-              |
| 22  | CEC                      |
| 23  | Reserved（N.C. on device） |
| 24  | Reserved（N.C. on device） |
| 25  | SCL                      |
| 26  | SDA                      |
| 27  | DDC/CEC Ground           |
| 28  | \+5V Power               |
| 29  | Hot Plug Detect          |

#### HDMI C Type

[Graphics_board_mini-HDMI_and_DVI-I_connectors_IMGP0972_wp.jpg](https://zh.wikipedia.org/wiki/File:Graphics_board_mini-HDMI_and_DVI-I_connectors_IMGP0972_wp.jpg "fig:Graphics_board_mini-HDMI_and_DVI-I_connectors_IMGP0972_wp.jpg")
俗稱mini-HDMI，应用于HDMI1.3版本，總共有19pin，可以說是縮小版的HDMI A
type，规格为2.42 mm×10.42 mm，但腳位定義有所改變。主要是用在攜帶型裝置上，例如DV、數位相機、攜帶型多媒體播放機等。由於大小所限，一些顯示卡會使用mini-HDMI，用家須使用轉接頭轉成標準大小的Type
A再連接[顯示器](../Page/顯示器.md "wikilink")。

|     |                          |
| --- | ------------------------ |
| Pin | Pin定義                    |
| 1   | TMDS Data2 Shield        |
| 2   | TMDS Data2+              |
| 3   | TMDS Data2–              |
| 4   | TMDS Data1 Shield        |
| 5   | TMDS Data1+              |
| 6   | TMDS Data1–              |
| 7   | TMDS Data0 Shield        |
| 8   | TMDS Data0+              |
| 9   | TMDS Data0–              |
| 10  | TMDS Clock Shield        |
| 11  | TMDS Clock+              |
| 12  | TMDS Clock–              |
| 13  | DDC/CEC Ground           |
| 14  | CEC                      |
| 15  | SCL                      |
| 16  | SDA                      |
| 17  | Reserved（N.C. on device） |
| 18  | \+5V Power               |
| 19  | Hot Plug Detect          |

#### HDMI D Type

[ASUS_Zenbook_UX305.jpg](https://zh.wikipedia.org/wiki/File:ASUS_Zenbook_UX305.jpg "fig:ASUS_Zenbook_UX305.jpg")
Zenbook UX305右側的[耳機孔](../Page/耳機.md "wikilink")、micro HDMI (D
Type)接口、[USB 3.0接口和電源接口](../Page/USB_3.0.md "wikilink")\]\]
应用于HDMI1.4版本，總共有19pin，规格为2.8 mm×6.4 mm，但腳位定義有所改變。新的Micro
HDMI接口将比现在19针MINI
HDMI版接口小50％左右，可为相机、手机等便携设备带来最高1080p的分辨率支持及最快5GB的传输速度。

|     |                   |
| --- | ----------------- |
| Pin | Pin定義             |
| 1   | Hot Plug Detect   |
| 2   | Utility           |
| 3   | TMDS Data2+       |
| 4   | TMDS Data2 Shield |
| 5   | TMDS Data2-       |
| 6   | TMDS Data1+       |
| 7   | TMDS Data1 Shield |
| 8   | TMDS Data1-       |
| 9   | TMDS Data0+       |
| 10  | TMDS Data0 Shield |
| 11  | TMDS Data0-       |
| 12  | TMDS Clock+       |
| 13  | TMDS Clock Shield |
| 14  | TMDS Clock-       |
| 15  | CEC               |
| 16  | DDC/CEC Ground    |
| 17  | SCL               |
| 18  | SDA               |
| 19  | \+5V Power        |

### TMDS通道

  - 傳送音頻、視訊及各種輔助資料
  - 訊號編碼方式：遵循[DVI](../Page/DVI.md "wikilink") 1.0規格。Single-link (Type A
    HDMI) 或 dual-link (Type B HDMI)
  - 視頻像素頻寬：從25 MHz到340 MHz（Type A, HDMI 1.3）或至680 MHz (Type
    B)。頻寬低於25MHz的視頻訊號如NTSC
    [480i將以倍頻方式輸出](../Page/480i.md "wikilink")。每個像素的容許資料量從24位元至48位元。支援每秒120張畫面[1080p解析度畫面傳送以及WQSXGA解析度](../Page/1080p.md "wikilink")[1](http://www.hdmi.org/consumer/faq.asp)
  - 像素編碼方式：[RGB](../Page/RGB.md "wikilink") 4:4:4, YCbCr 4:4:4（8-16 bits
    per component）; YCbCr 4:2:2（12 bits per component）; YCbCr 4:2:0（HDMI
    2.0）
  - 音頻取樣率：32 kHz, 44.1 kHz, 48 kHz, 88.2 kHz, 96 kHz, 176.4 kHz,
    192 kHz, 1536 kHz（HDMI 2.0）。
  - 音頻聲道數量：最大8聲道。HDMI 2.0支持32声道。
  - 音頻串流規格：IEC61937相容串流，包括高流量無失真訊號如[Dolby
    TrueHD](../Page/Dolby_TrueHD.md "wikilink")、[DTS](../Page/DTS.md "wikilink")-HD
    Master Audio。

### DDC通道

  - DDC全文為Display Data Channel
  - 傳送端與接收端可利用DDC通道得知彼此的傳送與接收能力，但HDMI僅需單向獲知接收端（顯示器）的能力。
  - 使用100kHz時脈的[I²C訊號](../Page/I2C.md "wikilink")
  - 傳送資料結構為[VESA](../Page/VESA.md "wikilink") Enhanced EDID（V1.3）。

### CEC通道

  - CEC全文為Consumer Electronics Control
  - 必須預留線路，但可以不必實作
  - 用來傳送工業規格的AV Link協定訊號，以便支援單一遙控器操作多台AV機器
  - 為單芯線雙向串列匯流排
  - 在HDMI 1.0協定中制訂，在1.2a版中更新
  - 一些制造商可能使用HDMI CEC，但是可能使用不同的名称来代表CEC功能：

:\# Samsung - AnyNet+

:\# Sharp - Aquos Link

:\# Sony - BRAVIA Link and BRAVIA Sync

:\# Hitachi - HDMI-CEC

:\# AOC - E-Link

:\# Pioneer - Kuro Link

:\# Toshiba - CE-Link and Regza Link

:\# Onkyo - RIHD (Remote Interactive over HDMI)

:\# LG - SimpLink

:\# Panasonic - HDAVI Control, EZ-Sync, VIERA Link

:\# Philips - EasyLink

### 線材規格

在HDMI 2.1版本前，根據規範，所有的HDMI線分為五種，線材的種類，HDMI的版本為規範連接器，大部分情況下線材部分沒有更動。

  - 標準纜線 （支援1080i及720p）
  - 標準纜線附帶[乙太網路](../Page/乙太網路.md "wikilink")
  - 高速纜線 （支援1080p, 4K@30fps, 3D 與 Deep Color）
  - 高速纜線附帶[乙太網路](../Page/乙太網路.md "wikilink")
  - 車用纜線

### 防拷機制

  - 強制支援[HDCP規格](../Page/HDCP.md "wikilink")1.10版

## 版本演化

### HDMI 1.1 / 1.2

  - HDMI 1.1

2004年5月提出

  - 支援[DVD-Audio](../Page/DVD-Audio.md "wikilink")

<!-- end list -->

  - HDMI 1.2

2005年8月提出

  - 支援8聲道1bit音頻（[SACD所使用者](../Page/Super_Audio_CD.md "wikilink")）
  - 讓PC訊源可使用HDMI Type A接頭
  - 在維持YCbCr CE[色域前提之下開放PC訊源使用原生RGB色域](../Page/色域.md "wikilink")
  - 要求HDMI 1.2以上顯示器支援低電壓訊源

<!-- end list -->

  - HDMI 1.2a

2005年12月提出

  - 完全確立CEC通道的功能，指令集，以及相容性測試程序

### HDMI 1.3

2006年6月22日提出\[2\]

  - 擴增single-link模式的頻寬至340 Mhz（資料傳送速度10.2 Gbps）
  - 從24bit色域（1677萬色）擴張支援至30-bit, 36-bit,與48-bit（RGB or
    YCbCr）色域（相當於超過十億色顯示）
  - 支援新的[xvYCC色彩標準](../Page/xvYCC.md "wikilink")
  - 支援自動語音同步（台詞對嘴）機能
  - 支援[Dolby
    TrueHD以及](../Page/Dolby_TrueHD.md "wikilink")[DTS](../Page/DTS.md "wikilink")-HD
    Master Audio訊號輸出至外接解碼器\[3\]如果播放機具有直接將此二種訊號解碼的能力，則不需要支援HDMI
    1.3，因為所有的HDMI規格都可以傳送未壓縮的音頻訊號。
  - 提出新的小型化接頭以支援輕便型攝錄影機\[4\]
  - Sony [PlayStation 3是第一個上市的HDMI](../Page/PlayStation_3.md "wikilink")
    1.3播放機。
  - Sony BRAVIA KDL- 46X2500、KDL-40X2500是第一個上市的HDMI
    1.3螢幕。（1080p支援新的xvYCC色彩標準, 36bits deep color）
  - EPSON EMP-TW1000是第一個上市的HDMI 1.3的投影機（支援30-bit deep color）

<!-- end list -->

  - HDMI 1.3a

2006年11月10日提出

  - 修改Cable and Sink的HDMI C Type接頭
  - Source termination recommendation
  - 移除上升時間（rise time）和下降時間（fall time）的最高最低限制
  - 改變CEC電容限制
  - 澄清RGB影像量化範圍
  - 增加CEC指令關於時間及聲音控制
  - 同時Released認證的測試規格文件

<!-- end list -->

  - HDMI 1.3b

2007年3月26日提出

  - 主要是修改HDMI測試規格（HDMI Testing specification），而HDMI Specification依然是HDMI
    1.3a。所以第一次出現當時的主要的HDMI Specification跟測試規格（HDMI Testing
    specification）是不同版本的情況發生。
  - 1.3b 2007/03/26 Modifications to TE overview and policy
    description（4.1）
  - Addition of Agilent TDR to Recommended TE（4.2.1.11）
  - Clarification of tentative cable emulators（4.2.1.17）
  - Jitter tolerance test changes (8-7)
  - Added cable tests for TMDS_CLOCK channel (5-3)
  - New VL triggering (7-2)
  - Editorial and clarifications on CEC Line Degradation（7-15, 8-14）
  - Added testing of additional source-supported Deep Color formats
    (7-34)
  - Additional HDMI VSDB EDID checks (8-3)
  - Additional TTC usage（5-3, 8-5, 8-6, 8-7）
  - Incorporated Tek-recommended setup and calibration for TDR (8-8)
  - Clarification on Sink Deep Color Recommended Test Method (8-25)
  - Added long cable or cable emulator use for Repeater test (9-3)
  - Added color-depths for each format in Source_Video_Formats（App. 3）
  - Removed test for filler bytes (8-3)
  - Removed Tektronix part number of cable emulator EFF-HDMI-CE-01

<!-- end list -->

  - HDMI 1.3b1

2007年8月1日提出

  - 僅修正關於測試設備上Type C connector的固定器（fixture）的些微內容

<!-- end list -->

  - HDMI 1.3c

2008年7月25日提出

  - 和1.3b、1.3b1一樣是為1.3a制訂的測試標準
  - 與之前版本的主要差異為線材的測試（增加線材測試條目或修正其內容有助於HDMI裝置的互連相容性）
  - 亦有部份修改與repeater和CEC相關

### HDMI 1.4

2009年5月28日提出

  - 新增HDMI百兆以太网通道，允许基于互联网的HDMI设备和其它HDMI设备共享互联网接入，无需另接一条以太网线。
  - 新增音频回授通道(ARC, Audio return
    channel)，让高清电视可通过HDMI线把音频訊號獨立传送到A/V功放接收机、音響設備上輸出。
  - 定义通用3D格式和分辨率。实现家庭3D系统输入输出部分的标准化，最高支持两条1080p分辨率的视频流。
  - 最高支持4K×2K（3840×2160p@24 Hz/25 Hz/30 Hz或4096×2160p@24 Hz）
  - 拓展支持色彩空间，专为数码相机设计的色彩空间，包括sYCC601、Adobe
    RGB、AdobeYCC601，可在连接数码相机的时候显示更精确的逼真色彩。
  - 新增Micro HDMI迷你接口，新的Micro HDMI接口将比现在的19针普通接口小50％左右。
  - 支持汽车连接系统，一种为车载高清内容传输设计的线缆规范，可避免发热、震动、噪音等汽车内部常见环境的影响，也为汽车制造商在车内传送高清内容提供一套切实可行的解决方案。[2](http://news.mydrivers.com/1/135/135877.htm)

<!-- end list -->

  - HDMI 1.4a

2010年3月4日提出

  - 新增Top-and-Bottom（上下）格式
  - 新增两套应用于广播系统中的强制性3D画面传输格式标准
      - Side-by-Side Horizontal（並行水平）
      - Top-and-Bottom（上下）
  - 定义强制性广播、游戏、电影3D标准
  - HDMI 1.4a标准要求
      - 3D显示设备帧封装水平须达到720p@50、1080p@24或720p@60、1080p@24
      - Side-by-Side Horizontal需达到1080i@50或1080i@60
      - top-and-bottom需达到720p@50、1080p@24或720p@60、1080p@24

<!-- end list -->

  - HDMI 1.4b

2011年10月11日提出

  - 支援3D [1080p](../Page/1080p.md "wikilink") 120Hz

### HDMI 2.0

2013年9月4日提出

  - 新增2160p@50 YCbCr 4:2:0、2160p@60 YCbCr
    4:2:0（[4K解析度](../Page/4K解析度.md "wikilink")）
  - 支援21:9長寬比
  - 32聲道，4组音频流
  - 傳輸頻寬18Gbit/s
  - 線材兼容HDMI 1.4（沒有定義新的數據線和接頭）
  - 支持CEC扩展
  - 支持双画面
  - 动态自动声画同步

<!-- end list -->

  - HDMI 2.0a

2015年4月8日提出

  - 支援[高動態範圍成像](../Page/高動態範圍成像.md "wikilink")（HDR）

<!-- end list -->

  - HDMI 2.0b

支援[高動態範圍成像](../Page/高動態範圍成像.md "wikilink")（HDR）视频的传输 带宽高达18Gbps 4K @
50与/ 60（2160P），这是1080/60的视频分辨率的清晰的4倍 多达32个音频通道进行多维身临其境的音频体验
最多的最高音频保真度1536kHz音频采样频率 双视频的同时递送流提供给多个用户在同一屏幕上
同时传送多路音频多用户（最多4） 9视频宽高比：为广角戏剧21支持 视频和音频流的动态同步
CEC扩展通过单一控制点提供更多的扩展命令和消费电子设备的控制

### HDMI 2.1

HDMI 2.1根据飞利浦撰写的白皮书增加支持“动态元”

简而言之：“HDMI 2.0A涵盖HDR EOTF信令和静态元数据元数据的动态是HDMI 2.1所涵盖。”

2017年1月4日提出

  - 頻寬提升至48Gbps
  - 支援4K 120Hz及8K 60Hz
  - 支援[高動態範圍成像](../Page/高動態範圍成像.md "wikilink")（HDR），可以針對場景或幀數進行優化
  - 支援eARC功能
  - 可針對遊戲幀數進行信號同步，減少[畫面撕裂](../Page/畫面撕裂.md "wikilink")
  - 向下相容HDMI 2.0、HDMI 1.4

## 長度限制問題

HDMI的纜線長度限制是其主要的問題之一，在部分消費者自行測試當中回報標準的28[AWG](../Page/AWG.md "wikilink")（American
Wire
Gauge,美國纜線度量）規格HDMI銅線大約在超過5公尺之後開始訊號衰減。此長度通常不足以滿足[投影機與電腦的連接](../Page/投影機.md "wikilink")。

但HDMI組織網頁並不認可此限制，其網頁常見問答集[HDMI FAQ
page](http://www.hdmi.com/learningcenter/faq.aspx#q7_1)當中記載：「我們見過有纜線在沒有"轉接器"之情況下通過不短於十米的HDMI
"標準纜線"承諾測試」。

現有通過HDMI認證的10M cable（未使用HDMI equalizer IC），大都使用24AWG的線材去製作。

一些報告指出，增加纜線中銅線的直徑以降低[阻抗](../Page/阻抗.md "wikilink")，是有效延長纜線長度的方法之一。另外也有報告指出，24AWG規格的纜線表現較28AWG好。另外也有人使用[光纖或兩條](../Page/光纖.md "wikilink")[CAT-5網路纜線來取代標準HDMI銅線](../Page/CAT-5.md "wikilink")。某些廠商也製造HDMI訊號增強器以因應使用者的需求。

現有單一一顆HDMI equalizer IC，可讓24AWG HDMI cable最大使用長度變成50公尺。

## 版本对比

  - **標準HDMI**
  - **高速HDMI**
  - **超高速HDMI (48G)**

**標準HDMI**数据线可选支持HDMI版本特定的子技术规范，而**高速HDMI**數據線皆支援，如Deep
Color和xvYCC等規範。標準與高速HDMI數據線皆可選配支援[乙太網路](../Page/乙太網路.md "wikilink")。HDMI
2.1版本則提供新的48G線材規格，包含所有功能。\[5\]

**主要規格**

<table>
<thead>
<tr class="header">
<th></th>
<th><p>HDMI 版本</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1.0–1.2a</p></td>
<td><p>1.3–1.3a</p></td>
</tr>
<tr class="even">
<td><p>發布日期</p></td>
<td><p>2002年12月 (1.0)<br />
2004年5月 (1.1)<br />
2005年8月 (1.2)<br />
2005年12月 (1.2a)</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>傳輸頻寬</p></td>
<td><p>4.95Gbit/s</p></td>
</tr>
<tr class="odd">
<td><p>最大傳輸資料速率</p></td>
<td><p>3.96Gbit/s</p></td>
</tr>
<tr class="even">
<td><p>TMDS Clock</p></td>
<td><p>165MHz</p></td>
</tr>
<tr class="odd">
<td><p>最小化傳輸差分訊號通道</p></td>
<td><p>3</p></td>
</tr>
<tr class="even">
<td><p>編碼方式</p></td>
<td><p>8b/10b</p></td>
</tr>
<tr class="odd">
<td><p>壓縮 （可選）</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>RGB</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>YC<sub>B</sub>C<sub>R</sub> 4:4:4</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>YC<sub>B</sub>C<sub>R</sub> 4:2:2</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>YC<sub>B</sub>C<sub>R</sub> 4:2:0</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>8bpc (24bit/px)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>10bpc (30bit/px)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12bpc (36bit/px)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>16bpc (48bit/px)</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>SMPTE 170M</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>ITU-R BT.601</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>ITU-R BT.709</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>sRGB</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>xvYCC</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>sYCC<sub>601</sub></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Adobe<sub>YCC601</sub></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Adobe RGB (1998)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>ITU-R BT.2020</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最大通道的採樣率</p></td>
<td><p>192kHz</p></td>
</tr>
<tr class="even">
<td><p>總採樣率</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>樣本大小</p></td>
<td><p>16–24bits</p></td>
</tr>
<tr class="even">
<td><p>最大音訊通道</p></td>
<td><p>8</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1.0–1.2a</p></td>
</tr>
<tr class="even">
<td><p>HDMI 版本</p></td>
<td></td>
</tr>
</tbody>
</table>

**分辨率刷新頻率限制**

<table>
<thead>
<tr class="header">
<th><p>colspan=4 |HDMI 版本</p></th>
<th><p>2.0a–2.0b</p></th>
<th><p>2.1</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>colspan=4 |最大傳輸資料速率</p></td>
<td><p>14.4 Gbit/s</p></td>
<td><p>42. Gbit/s</p></td>
</tr>
<tr class="even">
<td><p>視頻格式</p></td>
<td><p>刷新頻率(Hz)</p></td>
<td><p>資料速率要求</p></td>
</tr>
<tr class="odd">
<td><p>1080p</p></td>
<td><p>1920×1080</p></td>
<td><p>60</p></td>
</tr>
<tr class="even">
<td><p>120</p></td>
<td><p>8.24Gbit/s</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>144</p></td>
<td><p>10.00Gbit/s</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>240</p></td>
<td><p>17.50Gbit/s</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1440p</p></td>
<td><p>2560×1440</p></td>
<td><p>60</p></td>
</tr>
<tr class="even">
<td><p>100</p></td>
<td><p>11.96Gbit/s</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>120</p></td>
<td><p>14.49Gbit/s</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>240</p></td>
<td><p>30.77Gbit/s</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>4K</p></td>
<td><p>3840×2160</p></td>
<td><p>50</p></td>
</tr>
<tr class="even">
<td><p>60</p></td>
<td><p>15.68Gbit/s</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>120</p></td>
<td><p>32.27Gbit/s</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>144</p></td>
<td><p>39.19Gbit/s</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5K</p></td>
<td><p>5120×2880</p></td>
<td><p>30</p></td>
</tr>
<tr class="even">
<td><p>60</p></td>
<td><p>27.72Gbit/s</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>120</p></td>
<td><p>57.08Gbit/s</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8K</p></td>
<td><p>7680×4320</p></td>
<td><p>30</p></td>
</tr>
<tr class="odd">
<td><p>60</p></td>
<td><p>62.06Gbit/s</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>120</p></td>
<td><p>127.75Gbit/s</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th><p>HDMI版本</p></th>
<th><p>1.0–1.2a</p></th>
<th><p>1.3</p></th>
<th><p>1.4 <a href="http://cn.hdmi.org/press/press_release.aspx?prid=101">3</a></p></th>
<th><p>2.0</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>最大signal视频带宽（MHz）</p></td>
<td><p>165</p></td>
<td><p>340</p></td>
<td><p>340 [6]</p></td>
<td><p>600</p></td>
</tr>
<tr class="even">
<td><p>最大TMDS带宽（Gbit/s）</p></td>
<td><p>4.95</p></td>
<td><p>10.2</p></td>
<td><p>10.2</p></td>
<td><p>18</p></td>
</tr>
<tr class="odd">
<td><p>最大视频带宽（Gbit/s）</p></td>
<td><p>3.96</p></td>
<td><p>8.16</p></td>
<td><p>8.16</p></td>
<td><p>14.4</p></td>
</tr>
<tr class="even">
<td><p>最大音频带宽（Mbit/s）</p></td>
<td><p>36.86</p></td>
<td><p>36.86</p></td>
<td><p>36.86</p></td>
<td><p>49.152（IEC61937以及DST audio）</p></td>
</tr>
<tr class="odd">
<td><p>最大<a href="../Page/色深.md" title="wikilink">色深</a>（bit/px）</p></td>
<td><p>24</p></td>
<td><p>48</p></td>
<td><p>48</p></td>
<td><p>48</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Truecolor.md" title="wikilink">24-bit/px</a> HDMI单通道最大分辨率</p></td>
<td><p>1920×1200p 60 Hz</p></td>
<td><p>2560×1600p 75 Hz</p></td>
<td><p>4096×2160p 24 Hz</p></td>
<td><p>3840×2160p 60 Hz</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Deep_Color.md" title="wikilink">30-bit/px</a> HDMI单通道最大分辨率</p></td>
<td></td>
<td><p>2560×1600p 60 Hz</p></td>
<td><p>3840×2160p 30 Hz</p></td>
<td><p>3840×2160p 60 Hz</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Deep_Color.md" title="wikilink">36-bit/px</a> HDMI单通道最大分辨率</p></td>
<td></td>
<td><p>1920×1200p 75 Hz</p></td>
<td><p>3840×2160p 25 Hz</p></td>
<td><p>3840×2160p 50 Hz</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Deep_Color.md" title="wikilink">48-bit/px</a> HDMI单通道最大分辨率</p></td>
<td></td>
<td><p>1920×1200p 60 Hz</p></td>
<td><p>3840×2160p 24 Hz</p></td>
<td><p>3840×2160p 30 Hz</p></td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th><p>HDMI版本</p></th>
<th><p>1.0</p></th>
<th><p>1.1</p></th>
<th><p>1.2<br />
1.2a</p></th>
<th><p>1.3</p></th>
<th><p>1.3a<br />
1.3b<br />
1.3b1<br />
1.3c</p></th>
<th><p>1.4[7]</p></th>
<th><p>2.0</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/sRGB.md" title="wikilink">sRGB</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/YCbCr.md" title="wikilink">YCbCr</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>8 声道<a href="../Page/脈衝編號調變.md" title="wikilink">LPCM</a>，192 <a href="../Page/Hertz#Order_of_magnitude.md" title="wikilink">kHz</a>，24 bit音频传输</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Blu-ray_Disc.md" title="wikilink">Blu-ray Disc视频音频全分辨率支持</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>消费电子控制（CEC）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/DVD-Audio.md" title="wikilink">DVD-Audio支持</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Super_Audio_CD.md" title="wikilink">Super Audio CD</a>（<a href="../Page/Direct_Stream_Digital.md" title="wikilink">DSD</a>）支持</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Deep_Color.md" title="wikilink">Deep Color</a><a href="../Page/色深.md" title="wikilink">色深技术</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/xvYCC.md" title="wikilink">xvYCC</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Auto_lip-sync.md" title="wikilink">自动声画同步</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Dolby_TrueHD.md" title="wikilink">Dolby TrueHD音频</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/DTS-HD_Master_Audio.md" title="wikilink">DTS-HD Master Audio音频</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>CEC命令列表更新</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>以太网络通道</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>音频回传通道</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>HDMI 3D功能</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>4K×2K分辨率支持</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>4K@50/60 Hz</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>CEC扩展</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>双画面</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>4个音频流</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>32个音频通道</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1536 kHz音频采样</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>21:9宽高比</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>动态自动声画同步</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 授權

HDMI是HDMI Licensing, LLC的登录商标。使用HDMI规格需缴付版权费。版权费分为年会费和产品销售费。\[8\]

  - 年会费：使用HDMI的企业每年缴付10,000美元（2006年7月以前年费为15,000美元）
  - 产品销售费：使用HDMI的规格的产品收取0.15美元，如果产品上有HDMI的标记可享0.05美元折扣，如果该产品含有HDCP保护协议可再享有0.04美元折扣。

产品销售费按最终产品为单位计算。例如一条HDMI导线收取0.15美元，而一个电视机厂商把这条导线和提供HDMI端口的电视一同捆绑销售也只征收0.15美元。

## 參考文獻

## 外部連結

  - [HDMI Licensing, LLC.](http://www.hdmi.org/)
  - [All About the HDMI
    Interface](http://www.datapro.net/techinfo/hdmi_info.html)
  - [HDMI Tutorial](http://www.pacificcable.com/HDMI_Tutorial.htm)
  - [HDMI
    Switches](https://web.archive.org/web/20061212034641/http://octavainc.com/HDMI%20switch%20Insiders%20Guide.htm)
  - [Going the Distance With
    HDMI](http://www.hometoys.com/htinews/dec05/articles/accell/hdmi.htm)
  - [HDMI Upgraded To Support 'Deep
    Color'](https://web.archive.org/web/20100304184912/http://www.extremetech.com/article2/0%2C1558%2C1975596%2C00.asp?kc=ETRSS02129TX1K0000532)
  - [Interview with Steve Venuti, Director of Marketing for HDMI
    Licensing
    (Part 1)](https://web.archive.org/web/20061222063122/http://www1.electronichouse.com/info/specials/hdmi_basics.html)
  - [Interview with Steve Venuti, Director of Marketing for HDMI
    Licensing
    (Part 2)](https://web.archive.org/web/20061027201054/http://www.electronichouse.com/info/specials/hdmi_basics2.html)
  - [What is
    HDMI?](https://web.archive.org/web/20070311192215/http://www.hidefhardware.com/2006/07/what_is_hdmi.html)
  - [HDMI Cable Benchmark
    Reviews](http://hometheaterhifi.com/other/cable-benchmark/hdmi-cable-benchmark.html)
  - [HDMI Cable www.hdmi-cable.com.tw](http://www.hdmi-cable.com.tw/)
  - [HDMI Cable，DVI
    Cable](https://web.archive.org/web/20100305221243/http://www.opticis.com/english/02_product/product00.htm)
  - [HDMI图片](http://img.tfd.com/cde/_HDMITYP.GIF)
  - [av%E7%AB%AF%E5%AD%90 HDMI S
    AV色差比較](http://sunny080914.pixnet.net/blog/post/28873344-%5B%E7%AD%86%E8%A8%98%5D%E8%89%B2%E5%B7%AE%E7%AB%AF%E5%AD%90-v.s.-)

[Category:數碼顯示接口](../Category/數碼顯示接口.md "wikilink")
[Category:日本發明](../Category/日本發明.md "wikilink")

1.  HDCP管理機構，是[英特爾的](../Page/英特爾.md "wikilink")[子公司](../Page/子公司.md "wikilink")。
2.
3.
4.
5.
6.
7.
8.  [HDMI :: Manufacturer :: Becoming an Adopter ::
    Terms](http://www.hdmi.org/manufacturer/terms.aspx)