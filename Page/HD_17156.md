**HD
17156**是一颗黄色的[次巨星](../Page/次巨星.md "wikilink")，位于[仙后座](../Page/仙后座.md "wikilink")，距离地球约255光年。它的[视星等是](../Page/视星等.md "wikilink")8.17，这意味着肉眼不能看到它，但较好的望远镜就可以看到。太阳到此恒星的距离是255.19光年或78.24秒差距\[1\]。

此恒星的质量比太阳大20％，体积则比太阳大47％。由于其绝对星等为3.70以及[光谱型为G](../Page/光谱型.md "wikilink")0，此恒星被认为比太阳温度高。根据[色球观测的结果](../Page/色球.md "wikilink")，它的年龄为57±13亿年\[2\]。而1.2倍太阳质量恒星的总寿命应该是1.2<sup>-2.5</sup>年，也就是小于64亿年。这颗恒星正处于主序星的最后阶段，其附近的行星将很快被其吞没。

根据光谱观测，这颗恒星[金属量较高](../Page/金属量.md "wikilink")，金属比太阳多74％。截止2008年，天文学家认为有两颗[太阳系外行星围绕它公转](../Page/太阳系外行星.md "wikilink")，但目前只有一个已被证实。

## 行星系

这是仙后座中第一颗使用[径向速度法发现的行星](../Page/径向速度法.md "wikilink")：[HD 17156
b](../Page/HD_17156_b.md "wikilink")（2007年）。稍后的观测说明这颗行星也会[凌恒星](../Page/凌日法.md "wikilink")\[3\]。

2008年2月，推测存在第二颗行星[HD 17156
c](../Page/HD_17156_c.md "wikilink")，它与内层的行星HD 17156
b的轨道成5比1的[轨道共振](../Page/轨道共振.md "wikilink")\[4\]。

## 參見

  - [HD 17156b](../Page/HD_17156b.md "wikilink")

## 参考资料

## 外部連結

  - [SIMBAD Query result for
    HD 17156](http://simbad.u-strasbg.fr/simbad/sim-id?protocol=html&Ident=HD+17156&NbIdent=1&Radius=10&Radius.unit=arcmin&submit=submit+id)
  - [Notes for star
    HD 17156](https://web.archive.org/web/20080225140015/http://vo.obspm.fr/exoplanetes/encyclo/star.php?st=HD+17156)

[Category:仙后座](../Category/仙后座.md "wikilink")
[Category:行星凌變星](../Category/行星凌變星.md "wikilink")
[Category:行星系](../Category/行星系.md "wikilink")
[Category:G-型次巨星](../Category/G-型次巨星.md "wikilink")
[017156](../Category/HD和HDE天體.md "wikilink")
[013192](../Category/HIP天體.md "wikilink")

1.
2.
3.
4.   [ArXiv Abstract](http://fr.arxiv.org/abs/0803.2935)