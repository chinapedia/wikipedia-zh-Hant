**HK69**是[德國](../Page/德國.md "wikilink")[黑克勒-科赫推出的](../Page/黑克勒-科赫.md "wikilink")40毫米獨立式單發[榴彈發射器](../Page/榴彈發射器.md "wikilink")，是當時[德國聯邦國防軍的制式裝備](../Page/德國聯邦國防軍.md "wikilink")，其後被[HK79及](../Page/HK79附加型榴彈發射器.md "wikilink")[AG-36取代](../Page/HK_AG36附加型榴彈發射器.md "wikilink")。

## 設計

HK69的設計與獨立式[M203榴彈發射器相似](../Page/M203榴彈發射器.md "wikilink")，主結構包括單動式板機以及外露式擊槌擊發結構，槍身採折開式，裝有及金屬製T型伸縮[槍托](../Page/槍托.md "wikilink")，並裝有握把保險及手動安全制等雙從保險系統。槍管內刻有膛線，使榴彈發射後可產生旋轉效果，有助於維持彈道的穩定。而照門採直立式設計，照門分為大型及小型兩種，小型作為50m與100m的近距離，主要提供作為警用。而大型則作為150m\~350m的遠距離，主要是做為軍用。德國聯邦國防軍在1980年代初正式採用改進版本的HK69A1\[1\]，並使用由德國Diehl開發的[Diehl
DM-41](../Page/:en:Diehl_DM-41.md "wikilink")
[40×46毫米破片高爆彈](../Page/40毫米榴彈.md "wikilink")，亦可發射其他40毫米彈藥。

HK69（37毫米版本）成為了多國警隊用於發射[催淚彈的防暴武器](../Page/催淚彈.md "wikilink")，意大利的Luigi
Franchi SpA亦有推出特制[槍托的改進版本](../Page/槍托.md "wikilink")。

## 使用國

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 流行文化

### [電子遊戲](../Page/電子遊戲.md "wikilink")

  - 2009年—《[俠盜獵車手IV：失落與詛咒](../Page/俠盜獵車手IV：失落與詛咒.md "wikilink")》（Grand
    Theft Auto: The Lost and Damned）：命名為「榴彈發射器」。
  - 2009年—《[俠盜獵車手：夜生活之曲](../Page/俠盜獵車手：夜生活之曲.md "wikilink")》（Grand Theft
    Auto: The Ballad of Gay Tony）：命名為「榴彈發射器」。

### [动画](../Page/动画.md "wikilink")

  - 2014年—《[ALDNOAH.ZERO](../Page/ALDNOAH.ZERO.md "wikilink")》：第3話被界塚伊奈帆（かいづか
    いなほ，CV：[花江夏樹](../Page/花江夏樹.md "wikilink")）所使用。

## 参考文献

<div class="references-small">

<references />

  - [HKPRO-HK69](https://web.archive.org/web/20071009181602/http://www.hkpro.com/hk69.htm)

</div>

## 参见

  - [HK79附加型榴彈發射器](../Page/HK79附加型榴彈發射器.md "wikilink")
  - [HK AG36附加型榴彈發射器](../Page/HK_AG36附加型榴彈發射器.md "wikilink")
  - [HK AG-C/EGLM附加型榴彈發射器](../Page/HK_AG-C/EGLM附加型榴彈發射器.md "wikilink")
  - [M203榴彈發射器](../Page/M203榴彈發射器.md "wikilink")
  - [M320榴彈發射器](../Page/M320榴彈發射器.md "wikilink")
  - [B\&T GL-06榴彈發射器](../Page/B&T_GL-06榴彈發射器.md "wikilink")

## 外部链接

  - —[heckler-koch.de-HK69](http://www.heckler-koch.de/HKWebText/detailProd/2003/93/4/17)

  - —[Modern Firearms—HK69 40mm grenade
    launcher](http://world.guns.ru/grenade/de/hk69-e.html)

  - —[HKPRO—HK69](http://www.hkpro.com/index.php?option=com_content&view=article&id=35:the-hk69-grenade-launcher&catid=20:all-other-weapons&Itemid=5)

  - —[D boy Gun
    World（HK69A1榴弹发射器）](http://firearmsworld.net/german/hk/hk69/HK69.htm)

[Category:40毫米口径武器](../Category/40毫米口径武器.md "wikilink")
[Category:黑克勒-科赫](../Category/黑克勒-科赫.md "wikilink")
[Category:榴弹发射器](../Category/榴弹发射器.md "wikilink")
[6](../Category/黑克勒-科赫榴彈發射器.md "wikilink")
[Category:德國槍械](../Category/德國槍械.md "wikilink")
[Category:37毫米口徑武器](../Category/37毫米口徑武器.md "wikilink")

1.  [HK69A1榴彈發射器](http://www.gun-world.net/german/hk/hk69/HK69.htm)