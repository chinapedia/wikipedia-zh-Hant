**G11**是[西德在](../Page/西德.md "wikilink")60年代後期開始開發，80年代原成樣版的[無殼彈](../Page/無殼彈.md "wikilink")[犢牛式](../Page/犢牛式.md "wikilink")[突擊步槍計劃](../Page/突擊步槍.md "wikilink")。原意為取代[HK
G3成為新一代制式步槍](../Page/HK_G3自動步槍.md "wikilink")，西德把步槍整體項目交由[黑克勒-科赫負責](../Page/黑克勒-科赫.md "wikilink")、[諾貝爾炸藥](../Page/諾貝爾炸藥.md "wikilink")（）負責無殼彈技術、Hensoldt
Wetzlar則負責[瞄準鏡部份](../Page/瞄準鏡.md "wikilink")，三家公司組成了GSHG（）共同開發。

整個G11的開發計劃長達20年，投入經費極高，購置计划也非同小可。正當G11完成研發之際，90年代因[華沙公約组织解散](../Page/華沙公約组织.md "wikilink")，[冷戰結束](../Page/冷戰.md "wikilink")、[兩德統一](../Page/兩德統一.md "wikilink")，動用大量經費購置新鎗械及新子彈的需求突然消失，G11計劃雖然在技術上成功完成，但卻無訂單、從未大量生產。H\&K也因G11的研發費用及訂單落空而陷入困境，1991年被[英國航太的](../Page/英國航太.md "wikilink")公司收購，其後在2002年德國某集團向英國航太購回H\&K。

## 槍械設計

[G11_Schnittmodell.jpg](https://zh.wikipedia.org/wiki/File:G11_Schnittmodell.jpg "fig:G11_Schnittmodell.jpg")
G11使用4.73×33mm無殼彈，由包著彈頭的推進火藥被壓成正方體而成，顧名思意並無彈殼，因此無拋殼位。鎗身是[犢牛式設計](../Page/犢牛式.md "wikilink")，鎗機藏於後鎗托內，而彈匣置於上方與鎗管平行(與[P90類似](../Page/P90.md "wikilink"))，子彈則與鎗管成90度，彈頭向下指。

### 運作原理

[Evers_HK_G11_Feed.PNG](https://zh.wikipedia.org/wiki/File:Evers_HK_G11_Feed.PNG "fig:Evers_HK_G11_Feed.PNG")
上鏜時，子彈被向下推進旋轉式鎗鏜，然後旋轉式鎗鏜旋轉90度，使子彈與鎗管平行並且彈頭指向鎗管並同時使撞針被拉到待發狀態，如此就已經完成上鏜程序。扣把，撞針擊發子彈並發射，子彈加速時，後座力使鎗管、鎗鏜、彈匣及相關機械組件在鎗身內後移，後移的目的在單發及全自動模式是為了將後座能量消散以減少後座力，在三連發模式就會觸動其他機械使得有多兩發子彈在後移期間發射，當後移完畢時，總共三發就都已經射了出去，這就做到在非常短的時間內完成發射三發子彈，其目的下面會再有說明。當後移過程完成，鎗管內的氣體會推動旋轉式鎗鏜繼續旋轉，完成將下一枚子彈送入鎗鏜及餘下上鏜動作，並準備好再次發射。

如果扣把時不能發射，射手只要上鏜兩次就能在緊急排彈口將有問題的子彈排出。

### 性能特點

G11射擊模式有3種：單發、3連發及全自動連續射擊。考慮到要盡量提高命中率，及減小[全自動射擊時浪射而導致浪費彈藥的情況](../Page/自動火器.md "wikilink")，G11的性能有下列特點：
當处在單發及連發模式，在子彈加速時，整組機械（鎗管、鎗鏜、彈匣及相關機械組件）在鎗身內後移能消除大部份後座力，而全自動射擊時，後移使發射週期增長，使得射速被故意降低到460發/分鐘，而減少不必要的彈藥浪費，同時減低後座力以改善可控性及提高命中率。

在3連發射擊時，射擊速度達2000發/分，即36發/秒，3連發所需時間只需1/18秒。如此短暫的時間，加上整組發射機械在鎗身內後移產生緩衝作用，在後座力還未對鎗身指向有显著改變前，3發子彈就已經全數射出，確保了第2同第3發的精確度與第1發不會相差太多，因而提高了命中率。

由於使用無殼彈，子彈重量較北約5.56mm子彈輕50%，體積也只有其43%，因此士兵可以攜帶更多子彈，彈匣也可以裝有較多子彈，減少換匣次數。無拋殼位使整枝鎗可以右左手互換操作。

## 子彈設計

[G11Cartridge.jpg](https://zh.wikipedia.org/wiki/File:G11Cartridge.jpg "fig:G11Cartridge.jpg")
無殼子彈\]\] 研發無殼彈是一项極大的挑戰，一般子彈的彈殼有下列非常重要的作用：

  - 承載推進火藥
  - 在發射時彈殼膨脹，壓向鎗鏜，減小了與鎗鏜之間的縫隙，有助形成密氣作用
  - 發射後被拋出時把火藥燒不盡的殘餘物帶走
  - 拋殼時把部份熱量一起帶走，讓鎗鏜不至過熱而使得下一發上鏜的子彈自燃走火。

研發無殼彈就必须解決上述問題。為此，負責研發無殼子彈的諾貝爾炸藥公司研發出高燃點溫度推進劑（HITP），使4.73x33mm子彈在較一般子彈高100[℃才會發射](../Page/℃.md "wikilink")，避免子彈自燃走火。子彈设计為方形，使子彈在彈匣內時可以用盡空間以容納多一點推進劑。

## 使用與後續發展

1990年，G11研發完成时剛巧冷戰結束，加上東西德統一，原準備裝備G11的[德國聯邦國防軍及](../Page/德國聯邦國防軍.md "wikilink")[北約取消了購置計劃](../Page/北約.md "wikilink")，最後只小批量生產了1000枝，部份交到德國聯邦國防軍中使用。

2004年，[美國陸軍購入了G](../Page/美國陸軍.md "wikilink")11的技術受權用於其計劃，該計劃預定研發一系列鎗械，考慮以[塑膠作子彈殼或使用無殼彈](../Page/塑膠.md "wikilink")。

## 流行文化

儘管HK G11從未被任何國家正式採用，但它有出现在一些[電子遊戲之中](../Page/電子遊戲.md "wikilink")。

### 電子遊戲

  - 1998年—《[輻射2](../Page/輻射2.md "wikilink")》（Fallout 2）：型號為G11
    K2，命名為「H\&K G11」和「H\&K G11E」。
  - 2000年—《[三角洲部队：大地勇士](../Page/三角洲部队：大地勇士.md "wikilink")》（Delta force:
    Land
    Warrior）：配备一体式2x瞄准镜，弹夹容量48发，可[全自動射擊](../Page/自動火器.md "wikilink")，具有全游戏登场枪支中最快的射速。
  - 2009年—《[穿越火线](../Page/穿越火线.md "wikilink")》（Cross
    Fire）：命名为G11，擁有多种塗裝，45发弹匣供弹并且可以切换到射速较高的[三發點放模式](../Page/三發點放.md "wikilink")，是BOSS挑战箱的特殊奖励武器。
  - 2007年—《[-{zh-hans:反恐精英;
    zh-hant:絕對武力;}-Online](../Page/絕對武力Online.md "wikilink")》（Counter-Strike
    Online）：型號為G11 K2，命名為「HK
    G11」，使用啞黑色槍身，50發彈匣供彈並可切換到射速較高的[三發點放模式](../Page/三發點放.md "wikilink")。
  - 2010年—《[-{zh-hans:使命召唤：黑色行动; zh-hk:使命召喚：黑色行動;
    zh-hant:決勝時刻：黑色行動;}-](../Page/決勝時刻：黑色行動.md "wikilink")》（Call
    of Duty: Black Ops）：型號為G11
    K2，命名為「G11」，使用48發彈匣供彈並只有[三發點放模式](../Page/三發點放.md "wikilink")，奇怪地會出現於1960年代並且預設地配備機械瞄具（奇怪地後照準器被前後倒置的安裝）。故事模式中只於「數字」中出現過並被克拉克博士所使用，聯機模式可於購買所有其他[突擊步槍後解鎖](../Page/突擊步槍.md "wikilink")，並可改用低倍率或高倍率瞄準鏡。
  - 2012年— 《[-{zh-hans:命令与征服：将军;
    zh-hant:終極動員令：將軍;}-](../Page/終極動員令：將軍.md "wikilink")》：为欧盟菲林步兵所使用。
  - 2016年— 《[少女前線](../Page/少女前線.md "wikilink")》：命名為 G11。立繪型號為G11
    K2，五星槍娘，重型製造或一般建造取得(04:04:00)，404小隊成員之一。

### 動畫

  - 2007年—《[DARKER THAN BLACK －
    黑之契約者](../Page/DARKER_THAN_BLACK.md "wikilink")》：型號為G11
    K2，被潘朵拉安全部隊所使用。

## 相關條目

  - （LSAT）

  - [無殼彈](../Page/無殼彈.md "wikilink")

  - [HK G36](../Page/HK_G36突擊步槍.md "wikilink")－德國現役制式突擊步槍

  - [XM29 OICW](../Page/XM29_OICW.md "wikilink")－美國的新一代整合式單兵戰鬥武器

## 参考文献

  - [gun-world.net -
    G11無殼彈步槍](http://firearmsworld.net/german/hk/g11/G11.htm)
  - [HKPRO—HK
    G11](https://web.archive.org/web/20101221022959/http://www.hkpro.com/index.php?option=com_content&view=article&id=23:the-g11-caseless-military-rifle&catid=11:rare-prototypes&Itemid=5)
  - [Remtek—HK G11](http://www.remtek.com/arms/hk/mil/g11/g11.htm)

## 外部連結

  - [Civiliangunner](https://web.archive.org/web/20111202024557/http://www.civiliangunner.com/HKG11.htm)
  - [Industrie Werke Karlsruhe
    G11](http://www.google.com/patents/US3954042#v=onepage&q&f=false)
  - [IWK G11](http://tinypic.com/view.php?pic=iv90r9&s=8)
  - [IWK G11](http://i62.tinypic.com/64gqe0.jpg)
  - [Youtube上的介紹影片](../Page/Youtube.md "wikilink")([日語](../Page/日語.md "wikilink")):<https://www.youtube.com/watch?v=UoTU-X0qbnQ&feature=youtu.be>

[Category:黑克勒-科赫](../Category/黑克勒-科赫.md "wikilink")
[Category:自動步槍](../Category/自動步槍.md "wikilink")
[Category:突擊步槍](../Category/突擊步槍.md "wikilink")
[Category:德國槍械](../Category/德國槍械.md "wikilink")
[Category:試驗和研究槍械](../Category/試驗和研究槍械.md "wikilink")