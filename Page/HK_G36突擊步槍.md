**G36**是[德國](../Page/德國.md "wikilink")[黑克勒-科赫](../Page/黑克勒-科赫武器列表.md "wikilink")(H\&K)在1995年推出的[現代化](../Page/現代化.md "wikilink")[突擊步槍](../Page/突擊步槍.md "wikilink")，發射[5.56×45毫米北約制式](../Page/5.56×45mm_NATO.md "wikilink")[子彈](../Page/子彈.md "wikilink")，用来取代同为H\&K公司产品的[G3步枪](../Page/HK_G3自動步槍.md "wikilink")。G36大量采用了以不锈钢为骨架的玻璃钢加强复合材料，因此武器重量较轻。\[1\]
在开发阶段其型号为**HK50**。

## 歷史

黑克勒-科赫在1980年代已向德軍提交了[G11及](../Page/HK_G11突擊步槍.md "wikilink")[G41](../Page/HK_G41突擊步槍.md "wikilink")，但前者因兩德統一而中止，後者則被德軍否決。

1990年代，[德國聯邦國防軍提出新的制式步槍計劃](../Page/德國聯邦國防軍.md "wikilink")，以取代[7.62×51毫米的](../Page/7.62×51_NATO.md "wikilink")[HK
G3](../Page/HK_G3自動步槍.md "wikilink")。1993年9月，由德國聯邦國防技術署對多種突擊步槍進行評選，許多槍型因為未達到標準而遭到淘汰，只剩下德國本土的HK50、奧地利的[AUG和英國的](../Page/斯泰爾AUG突擊步槍.md "wikilink")[L85A1](../Page/SA80.md "wikilink")，其中L85A1因為故障率太高最先被淘汰，而AUG也因為它的兩段式板機系統（扣壓板機一半為半自動射擊
/ 扣壓板機到底為[全自動射擊](../Page/自動火器.md "wikilink")）而落敗，最終由HK50勝出。

經過這次評選之後，德國聯邦國防軍在1995年決定採用HK50，並要求黑克勒-科赫對它進行改良，並將軍用代號設為Gewehr
36（36號步槍），簡稱G36突擊步槍。G36的槍械運作原理與[AR-18比較相似](../Page/AR-18突擊步槍.md "wikilink")，採用短行程活塞導氣系统，並承襲其槍機結構；AR-18採用單連桿、雙復進導桿設計，而G36則是單一復進簧、瓦斯缸管及活塞設計也有差異。

1990年代，黑克勒-科赫承包了美軍[理想單兵戰鬥武器計劃的步槍和榴彈發射器部份](../Page/XM29_OICW.md "wikilink")，黑克勒-科赫推出以G36的設計改進出[XM8突擊步槍](../Page/XM8突擊步槍.md "wikilink")，但XM8計畫在2005年被取消。

## 採用

G36是[德國聯邦國防軍](../Page/德國聯邦國防軍.md "wikilink")、[西班牙陸軍及](../Page/西班牙陸軍.md "wikilink")[西班牙海軍的制式突擊步槍](../Page/西班牙海軍.md "wikilink")，多個[國家及](../Page/國家.md "wikilink")[地區的](../Page/地區.md "wikilink")[軍隊及](../Page/軍隊.md "wikilink")[警察亦有裝備](../Page/警察.md "wikilink")，在[沙烏地阿拉伯獲授權生產](../Page/沙烏地阿拉伯.md "wikilink")。

G36在1997年取代[G3成為](../Page/HK_G3自動步槍.md "wikilink")[德國聯邦國防軍的制式步槍](../Page/德國聯邦國防軍.md "wikilink")，命名為（36型步槍）。在1999年[西班牙陸軍以G](../Page/西班牙陸軍.md "wikilink")36取代[CETME
Model
L](../Page/CETME_L型突擊步槍.md "wikilink")，挪威的在1990年代末期亦有裝備。[英國陸軍亦有入口小量G](../Page/英國陸軍.md "wikilink")36作制式武器評估，但最後沒有大量取代原本的[L85A2](../Page/SA80突擊步槍.md "wikilink")。

G36亦是多個國家軍隊及警隊的武器，包括[英國各個應變部隊](../Page/英國.md "wikilink")、[法國警察總署特勤隊](../Page/法國.md "wikilink")、[葡萄牙共和國民警衛](../Page/葡萄牙.md "wikilink")、[荷蘭警隊](../Page/荷蘭.md "wikilink")、[波蘭警察](../Page/波蘭.md "wikilink")（G36C、G36E）、[美國](../Page/美國.md "wikilink")[國會警察及](../Page/國會警察.md "wikilink")[洛杉磯警局](../Page/洛杉磯警局.md "wikilink")（之後被[HK416取代](../Page/HK416.md "wikilink")）、[菲律賓海軍特種作戰部隊及輕裝快速反應部隊](../Page/菲律賓.md "wikilink")（LRB）、[葡萄牙海軍陸戰隊](../Page/葡萄牙.md "wikilink")、[葡萄牙空軍及NFOT](../Page/葡萄牙.md "wikilink")、[立陶宛特種部隊](../Page/立陶宛.md "wikilink")（G36及AG36）、[印尼特種部隊](../Page/印尼.md "wikilink")（G36及G36C）、[香港](../Page/香港.md "wikilink")[特別任務連](../Page/特別任務連.md "wikilink")（G36KV）、[澳門](../Page/澳門.md "wikilink")[特別行動組](../Page/特別行動組.md "wikilink")（G36C）、[泰國皇家海豹部隊](../Page/泰國.md "wikilink")、[聯合國維和部隊](../Page/聯合國.md "wikilink")（G36K）等。

在2006年[墨西哥仿制了G](../Page/墨西哥.md "wikilink")36及加入改進，名為[FX-05
Xiuhcoatl](../Page/FX-05突擊步槍.md "wikilink")。

G36在[2008年南奧塞梯衝突中出現過](../Page/2008年南奧塞梯戰爭.md "wikilink")（當時為[格魯吉亞特種部隊所持有](../Page/格魯吉亞.md "wikilink")）。[2011年利比亞內戰中也有士兵试射从卡扎菲卫队处缴获的G](../Page/2011年利比亞內戰.md "wikilink")36的镜头\[2\]并引发了HK公司对枪支流入的调查，\[3\]该照片还被一些人当作“西方佣兵投入战争”的证据\[4\]。

2015年4月22日，德国联邦国防部正式宣布不再采购和使用G36，并将尽快将其替换。

## 過熱问题

2012年4月德国[明镜周刊报道](../Page/明镜周刊.md "wikilink")，[德国联邦国防军在试验后证实](../Page/德国联邦国防军.md "wikilink")，G36在“连续射击数百发子弹后”会因为过热造成300米外距离上的射击精度急剧下降。\[5\]
随后[图片报根据德国联邦国防军下属](../Page/图片报.md "wikilink")[第91国防技术试验所](../Page/第91国防技术试验所.md "wikilink")（Wehrtechnische
Dienststelle
91）的一项内部测试，证实了这个报道。在一份交给联邦国防部长的报告中指出“所有测试过的G36在射击过热的情况下，落点中心都产生了偏差，（因此）无法和距离200米以外的敌人进行安全的交战”。联邦国防军内部在解释武器进行连射之后进行冷却的必要性时也认为，和100米以外的目标战斗时如果不进行冷却，战斗将会变得很困难。但是在公开报道中，并没有看到来自士兵在实战中对武器的批评。\[6\]

2013年9月明镜周刊报道，[第91国防技术试验所在](../Page/第91国防技术试验所.md "wikilink")2012年7月提交了一份机密的总结报告。报告称，G36在90发射击过后，100米上彈著的[圆概率误差达到](../Page/圆概率误差.md "wikilink")50-60厘米。造成这一问题的原因是G36使用的[聚合物零件](../Page/聚合物.md "wikilink")，这种材料在23摄氏度就开始失去刚性。如果把枪放在太阳下或者把一面加热，材料变形就会造成弹着点的偏移。“首发命中率”降低，弹药消耗量也随之增加，士兵也失去了“对武器射击性能的信心”。\[7\]

2014年2月17日，明镜周刊网站报道，下降的射击精度还与弹药品質的不稳定有关，因为使用某个特定厂商的弹药会造成包裹有聚合物材料的枪管过热严重。\[8\]

2015年3月30日，联邦国防军总参谋长[沃克尔·维克](../Page/沃克尔·维克.md "wikilink")（Volker
Wieker）的一封信曝光，信中说已证实了G36在高温或者高射速下与弹药无关的失準问题。在同一天，联邦国防部长[乌尔苏拉·冯德莱恩在新闻发布会上承认](../Page/乌尔苏拉·冯德莱恩.md "wikilink")，“G36很明显在高温或者高射速情况下有准确性的问题”。\[9\]

制造商[黑克勒-科赫随后在公告中拒绝承认批评](../Page/黑克勒-科赫.md "wikilink")\[10\]
，并且特别指责了联邦国防部的测试条件，称之为“有系统的”对[黑克勒-科赫的攻击](../Page/黑克勒-科赫.md "wikilink")。其认为，在国防部的测试中，使用的对比武器同样是[黑克勒-科赫的产品](../Page/黑克勒-科赫.md "wikilink")，而并没有竞争对手的产品。不同级别，不同口径的武器之间进行的比较难以保证说服力。G36也并不是设计用来在战斗中提供持续火力的。\[11\]

2015年4月22日，联邦国防部长[乌尔苏拉·冯德莱恩正式宣布](../Page/乌尔苏拉·冯德莱恩.md "wikilink")，联邦国防军将不会再采购和使用G36，它在联邦国防军中“已没有未来”。\[12\]

在2016年9月3日，據德國《鏡報》消息指，科布倫茨地方法院發現控方的指控和實際研究測試結果出現矛盾，因此宣判黑克勒-科赫勝訴並駁回控方要該公司為4000支有問題的步槍作出賠償的要求，而且在聯邦軍內部G36仍廣受各部隊士兵的信賴，此宣判亦有機會令國防軍暫時擱置汰換G36的計劃。\[13\]

2017年4月19日，德國國防軍裝備資訊科技暨支援(BAAINBw)聯邦辦公室，已經宣布展開旨在取代H\&K
G36突擊步槍的下一代制式步槍，並於2018年開始遴選，最終預計在2019年4月1日完對槍款的遴選作業。而在選定下一代步槍之後，換裝作業將會持續至2026年3月31日，而該為期7年的合同估計總價值將上看2.6億美元。目前參與競標的槍款有黑克勒&科赫(H\&K)的[HK433](../Page/HK433.md "wikilink")、史泰爾-萊茵金屬(Steyr-Rheinmetall)的RS556以及SIG
Sauer的[SIG
MCX突擊步槍](../Page/SIG_MCX突擊步槍.md "wikilink")，而MilMag表示貝瑞塔(Beretta)的ARX-100/200也很可能投標。新型步槍須具備短槍管(Kurzrohr)以及長槍款(Langrohr)等兩種版本，並能適應左/右撇射手操作且具備標準的NATO戰術滑軌，此外，德國國防軍並不特別規定口徑，因此步槍並不限定是5.56x45mm
NATO或7.62x51mm
NATO。此外，步槍的重量不可超過3.6公斤(即使是7.62mm口徑)，且必須要有30,000發的機匣/結構壽命，而槍管則規定在使用標準彈藥的情況下擁有15,000發的壽命。

2018年10月14日，由於參與取代G36突擊步槍的競標活動中沒一支步槍能符合到需求，使到德國國防部決定將取代G36的計劃延遲八個月。\[14\]

## 過熱问题實際狀況

原本的HK-50槍身是用耐高溫的PA(聚醯胺-防火材料跟軍用防爆胎的主要材料之一)製作的，而HK所有的塑膠槍身都是這個材料
[2](https://en.wikipedia.org/wiki/Heckler_%26_Koch_G36#cite_note-11)
，但是交給國防軍的第一批G36卻將材料換成了廉價的聚乙烯(PE)，之後的G36包含標準版的A2跟G36C/G36K
HK又將材料換回了PA
包含所有的外銷版也通通都是PA槍身，這也是為什麼美國民間槍店以及槍匠瘋狂連發測試G36(HK對外販售的E版以及K版)之後的結果
是沒問題的，甚至出現槍管冷槍跟熱槍時(槍管達到工作溫度)時的準確度有落差,熱槍後的G36在射擊準確度上都有相當的提升

## 使用國

[040610-N-1823S-348_G36andpracticenade.jpg](https://zh.wikipedia.org/wiki/File:040610-N-1823S-348_G36andpracticenade.jpg "fig:040610-N-1823S-348_G36andpracticenade.jpg")
[Balts_Will_2008_2.jpg](https://zh.wikipedia.org/wiki/File:Balts_Will_2008_2.jpg "fig:Balts_Will_2008_2.jpg")
[Latvian_G36KV.JPEG](https://zh.wikipedia.org/wiki/File:Latvian_G36KV.JPEG "fig:Latvian_G36KV.JPEG")、伸縮摺疊式[槍托](../Page/槍托.md "wikilink")、鋁合金導軌和另加一段延長導軌。\]\]
[G36V_Spain_Navy.jpg](https://zh.wikipedia.org/wiki/File:G36V_Spain_Navy.jpg "fig:G36V_Spain_Navy.jpg")
[G36_2.JPEG](https://zh.wikipedia.org/wiki/File:G36_2.JPEG "fig:G36_2.JPEG")使用G36進行訓練的[美軍士兵](../Page/美軍.md "wikilink")\]\]
[Albanian_Soldier_with_G36.jpg](https://zh.wikipedia.org/wiki/File:Albanian_Soldier_with_G36.jpg "fig:Albanian_Soldier_with_G36.jpg")
[AGIGN8_Domenjod_110217.jpg](https://zh.wikipedia.org/wiki/File:AGIGN8_Domenjod_110217.jpg "fig:AGIGN8_Domenjod_110217.jpg")[干預組裝備的G](../Page/國家憲兵干預組.md "wikilink")36K\]\]
[British_PMC_with_G36K_and_ANA_soldier.jpg](https://zh.wikipedia.org/wiki/File:British_PMC_with_G36K_and_ANA_soldier.jpg "fig:British_PMC_with_G36K_and_ANA_soldier.jpg")\]\]

[G36_Select-Fire_Carbine.JPEG](https://zh.wikipedia.org/wiki/File:G36_Select-Fire_Carbine.JPEG "fig:G36_Select-Fire_Carbine.JPEG")

  -   - （G36、G36C）

  -
  -   - [澳大利亞聯邦警察](../Page/澳大利亞聯邦警察.md "wikilink") \[15\]\[16\]

  -
  -   - 特別支援小隊

  -   - \[17\]
        \[18\]（G36C、G36KV）

  -
  -   - [巴西聯邦警察](../Page/巴西聯邦警察.md "wikilink")\[19\]\[20\]（G36K、G36C）

  -
  -   - \[21\]（G36）

  -
  - \[22\]（G36C）

      - [中華人民共和國武裝警察部隊特戰單位](../Page/中華人民共和國武裝警察部隊.md "wikilink")

  -   - [克羅地亞武裝部隊](../Page/克羅地亞武裝部隊.md "wikilink")

      - 特別單位 \[23\]\[24\]

  -   - [塞浦路斯國民警衛隊](../Page/塞浦路斯國民警衛隊.md "wikilink")

  -   - 特別單位 \[25\]（G36C、G36K）

  -   - [警察行動部隊](../Page/警察行動部隊.md "wikilink") \[26\]\[27\]（G36C）

  -   - \[28\]（G36K）

  -
  -   - [埃及武裝力量特種部隊單位](../Page/埃及武裝力量.md "wikilink")\[29\]

      -
  -   - （G36K）

  -   - \[30\]（G36C）

      -
  -   - [法國陸軍](../Page/法國陸軍.md "wikilink") \[31\]（G36E）

      - [干預組](../Page/國家警察干預組.md "wikilink")\[32\]\[33\]（G36C）

      -
      - 法國國家警察反罪案旅 \[34\]\[35\]\[36\] （G36K、G36C）

      - [法國國家憲兵](../Page/法國國家憲兵.md "wikilink")[干預組](../Page/國家憲兵干預組.md "wikilink")

  -   - \[37\]（G36C、G36K、G36E）

      - [格魯吉亞武裝部隊](../Page/格魯吉亞軍.md "wikilink")

  -   - [德國聯邦國防軍](../Page/德國聯邦國防軍.md "wikilink") \[38\]\[39\]\[40\]
        （G36A1、G36A2、G36K、G36C）

      - [德國聯邦警察](../Page/德國聯邦警察.md "wikilink") \[41\]

      -
  -
  -
  -   - [香港警務處](../Page/香港警務處.md "wikilink")[特別任務連](../Page/特別任務連.md "wikilink")
        \[42\]（G36KV）

  -   -
      - 及其所屬的 \[43\]

  -   - [印尼陸軍](../Page/印尼陸軍.md "wikilink") \[44\]（G36C）

      - \[45\]（G36V、G36C）

  -   - 要人保護小組

  -   - [自由鬥士](../Page/佩什梅格.md "wikilink")\[46\]

  -   - [愛爾蘭國防軍](../Page/愛爾蘭國防軍.md "wikilink")

  -   - \[47\]（G36C）

      - [卡賓槍騎兵](../Page/卡賓槍騎兵.md "wikilink")[特別干預組](../Page/特別干預組_\(義大利\).md "wikilink")

      - [意大利陸軍](../Page/意大利陸軍.md "wikilink")

  -   - [日本陸上自衛隊](../Page/日本陸上自衛隊.md "wikilink")\[48\]

  -   - [約旦武裝部隊](../Page/約旦軍事.md "wikilink")\[49\]\[50\]（G36C）

  -   - \[51\]\[52\]（G36V）

  -   - \[53\]\[54\]（G36KV）

  -   - \[55\]（G36C）

      -
  -   - \[56\]\[57\]\[58\]
        \[59\]\[60\]（G36KV、G36E）

      - [的黎波里旅](../Page/的黎波里旅.md "wikilink")\[61\]

  -   - 邦警察干預單位（G36C）

  -   - \[62\]\[63\] \[64\]（G36KA4、G36KV1、G36C、G36KA4M1）

  -   - [治安警察局](../Page/治安警察局.md "wikilink")[特別行動組](../Page/特別行動組_\(澳門\).md "wikilink")（G36CV、G36KV）

  -   - [馬來西亞皇家海軍](../Page/馬來西亞皇家海軍.md "wikilink")[特種作戰部隊](../Page/海軍特種作戰部隊.md "wikilink")
        \[65\]\[66\]\[67\]\[68\]（G36C、G36E、G36KE）
      - [馬來西亞皇家警察](../Page/馬來西亞皇家警察.md "wikilink")[特別行動指揮部](../Page/特別行動指揮部.md "wikilink")
        \[69\]（G36C）

  -   - （G36A2、G36K、G36C）

  -   - \[70\]

  -   - [蒙古國武裝部隊](../Page/蒙古國軍事.md "wikilink") \[71\]

  -   - \[72\]

  -
  -   -
  -
  -   - [挪威皇家海軍](../Page/挪威皇家海軍.md "wikilink")（G36KV2）

  -   - [菲律賓武裝部隊](../Page/菲律賓武裝部隊.md "wikilink") \[73\]（G36C、G36K）

  -   - 特別單位 \[74\]（G36V、G36K、G36C）

      - \[75\]（G36C）

      - [波蘭武裝部隊](../Page/波蘭軍事.md "wikilink")[行動應變及機動組](../Page/行動應變及機動組.md "wikilink")

      - 波蘭武裝部隊[福爾摩沙部隊](../Page/福爾摩沙部隊.md "wikilink") \[76\]（G36KV3、G36C）

  -   -
      - \[77\]

      - [特別行動組](../Page/特別行動組_\(葡萄牙\).md "wikilink") \[78\]

  -   - \[79\]（G36K、G36C）

  -   - [沙特阿拉伯武裝部隊](../Page/沙特阿拉伯軍事.md "wikilink") \[80\]（G36C）

  -   -
      - 特警單位

  -
  -   - [新加坡警察部隊](../Page/新加坡警察部隊.md "wikilink")

  -   -
  -
  -   - [大韓民國海洋警察廳海上特別突擊隊](../Page/大韓民國海洋警察廳.md "wikilink") \[81\]

  -   - [西班牙武裝部隊](../Page/西班牙軍隊.md "wikilink")
        \[82\]\[83\]\[84\]（G36E、G36KE、G36CE）

  -   - \[85\]（G36C）

      - 瑞典警察

      - [瑞典國防軍](../Page/瑞典國防軍.md "wikilink")[特別行動任務組](../Page/特別行動任務組.md "wikilink")
        \[86\]（G36K、G36C）

  -
  -   - [泰國皇家武裝部隊](../Page/泰國皇家軍隊.md "wikilink")
        \[87\]\[88\]\[89\]（G36K、G36KE、G36KV、G36C、G36E、MG36）

  -
  -
  -   -
  -   - [英國陸軍](../Page/英國陸軍.md "wikilink")[空降特勤隊](../Page/空降特勤隊.md "wikilink")（G36K、G36C）
        \[90\]

      - \[91\]（G36CSF）

      - [倫敦警察廳](../Page/倫敦警察廳.md "wikilink")[專業槍械司令部](../Page/專業槍械司令部.md "wikilink")（G36CSF）

      - 多個警察部門（G36K、G36C）

  -   - [美國國會警察](../Page/美國國會警察.md "wikilink") \[92\]
      - 多個執法機構
      - [美國特種作戰司令部](../Page/美國特種作戰司令部.md "wikilink")

  -   - （G36、G36K、G36C）

  -
  -   -
      - [聯合國維持和平行動部](../Page/聯合國維持和平行動部.md "wikilink")

## 設計

[G36_CMag.jpg](https://zh.wikipedia.org/wiki/File:G36_CMag.jpg "fig:G36_CMag.jpg")100發[彈鼓的G](../Page/彈鼓.md "wikilink")36（這並非MG36）。\]\]
G36發射[5.56×45毫米](../Page/5.56×45mm_NATO.md "wikilink")[北約標準步槍子彈](../Page/北約.md "wikilink")，射速每分鐘750發，有單發、二連發、三連發、全自動發射模式（取決於不同型號的板機組）。G36採用[轉拴式槍機](../Page/轉拴式槍機.md "wikilink")、[短行程活塞導氣系統設計](../Page/氣動式_\(槍械\).md "wikilink")，比[M16突擊步槍的氣動系统更可靠](../Page/M16突擊步槍.md "wikilink")，清槍次數可大為降低。G36的人體工學設計優良，雙手亦可靈活操作，對應[AG36](../Page/HK_AG36附加型榴彈發射器.md "wikilink")
40毫米榴彈發射器及[AK-74式刺刀](../Page/AK-74突击步枪.md "wikilink")（來自前東德的剩餘物資）。

所有型號的G36皆附有摺疊[槍托](../Page/槍托.md "wikilink")，摺疊時不妨礙排殼口運作，槍機拉柄在[機匣上方](../Page/機匣.md "wikilink")，左右手皆可操作。機匣以[玻璃纖維](../Page/玻璃纖維.md "wikilink")[聚合物制造](../Page/聚合物.md "wikilink")，清槍分解時無須專用工具。G36配備30發透明塑料彈匣，彈匣上附有彈匣連接釦，亦對應專用的[Beta
C-Mag](../Page/C-Mag彈鼓.md "wikilink")100發[彈鼓](../Page/彈鼓.md "wikilink")。另外，雖然彈匣與[SIG
SG 550突擊步槍外觀上甚為相似](../Page/SIG_SG_550突擊步槍.md "wikilink")，但兩者不可通用。

G36的準確度雖然不錯，但並非最精準的，從100公尺外以半自動模式快速射擊，彈著的[圆概率误差分佈在](../Page/圆概率误差.md "wikilink")2\~2.5英吋(5.08\~6.35公分)之間，而[SG550以相同的方式射擊](../Page/SIG_SG_550突擊步槍.md "wikilink")，100公尺外的彈著圆概率误差分佈則是在2.5公分以內。
但根據近年資料顯示,原型G36在靶場測試時,由專業射手射擊時卻打出了驚人的0.6MOA (散佈圓直徑15mm)

### 瞄準具

[德國聯邦國防軍為了令士兵射擊準確度提高](../Page/德國聯邦國防軍.md "wikilink")，因此他們選用標準型G36，標準型G36附有兩種瞄準鏡的可拆式提把，提把下部為3倍光學瞄準鏡；上部為[內紅點光學瞄準鏡](../Page/內紅點光學瞄準鏡.md "wikilink")，其後推出的氚光夜視鏡亦可完全對應紅點瞄準鏡裝在提把上。

出口型G36E的瞄具為1.5倍光學瞄準鏡，而G36C則採用M1913型戰術導軌（備有機械照門），可在導軌上安裝其他瞄準鏡。

Image:HKV G36.jpg|G36的3倍光學瞄準鏡及內紅點瞄準鏡
Image:HKG36Reticle.svg|G36及G36K光學瞄準鏡分劃圖
<small>（按圖觀看說明）</small> Image:HK G36 with
AG36.jpg|裝上[AG36及LLM](../Page/HK_AG36附加型榴彈發射器.md "wikilink")01雷射指示器的G36
Image:AN-PAS-13B (V2) Thermal Weapon Sight
(TWS).jpg|G36的AN/PAS-13B（V2）熱影像鏡

## 衍生型

[Unidad_de_Operaciones_Especiales_in_Search_And_Rescue_exercise.jpg](https://zh.wikipedia.org/wiki/File:Unidad_de_Operaciones_Especiales_in_Search_And_Rescue_exercise.jpg "fig:Unidad_de_Operaciones_Especiales_in_Search_And_Rescue_exercise.jpg")[紅點瞄準鏡](../Page/紅點鏡.md "wikilink")。\]\]
[H\&K-SL8.jpg](https://zh.wikipedia.org/wiki/File:H&K-SL8.jpg "fig:H&K-SL8.jpg")，留意它故意模仿30發彈匣，實際上只有10發。\]\]
[MP7Sept2006.jpg](https://zh.wikipedia.org/wiki/File:MP7Sept2006.jpg "fig:MP7Sept2006.jpg")上的[蔡司Z](../Page/蔡司公司.md "wikilink")-Point紅點瞄準鏡，安裝這種瞄準鏡的G36亦頗為常見。\]\]

G36有四种主要衍生型，包括G36、G36C、G36K、MG36，其他型號亦是基於以上四种改進而成，另外亦有一種半自動民用型[SL8](../Page/HK_SL8半自動步槍.md "wikilink")。

部分挪威軍隊亦有裝備名為G36KV3的專門改進型（挪威軍隊的制式步槍是HK 416
D16.5RS），G36KV3裝有不同於原本G36系列的伸縮摺疊式[槍托](../Page/槍托.md "wikilink")，改進了導氣箍、彈匣卡榫及空槍掛機釋放鈕，並將導軌改為鋁合金製。

G36亦是[德國未來士兵系統](../Page/德國未來士兵系統.md "wikilink")（IdZ-）\[93\]\[94\]的一部份。

<table>
<thead>
<tr class="header">
<th><p>型號</p></th>
<th><p>長度（毫米）：<br />
<a href="../Page/槍托.md" title="wikilink">槍托伸出</a>/槍托摺疊</p></th>
<th><p>槍管（毫米）</p></th>
<th><p>重量（公斤）：<br />
空槍/全重</p></th>
<th><p>瞄準具</p></th>
<th><p>彈匣</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>G36</strong>，標準型</p></td>
<td><p>1002/755</p></td>
<td><p>480</p></td>
<td><p>3.63/3.77</p></td>
<td><p>3倍光學瞄準鏡、<a href="../Page/紅點鏡.md" title="wikilink">紅點瞄準鏡</a></p></td>
<td><p>標準（30發）、<br />
<a href="../Page/C-Mag彈鼓.md" title="wikilink">C-Mag彈鼓</a>（100發）</p></td>
</tr>
<tr class="even">
<td><p><strong>G36K</strong>，短型（Kurz）</p></td>
<td><p>833/613</p></td>
<td><p>318</p></td>
<td><p>3.37/3.51</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>G36KV3</strong>，挪威軍隊專用型</p></td>
<td><p>833/613</p></td>
<td><p>318</p></td>
<td><p>3.37/3.51</p></td>
<td><p>導軌、後備<a href="../Page/機械瞄具.md" title="wikilink">機械瞄具</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>G36C</strong>，緊湊／突擊型</p></td>
<td><p>716/500</p></td>
<td><p>228</p></td>
<td><p>2.988/3.128</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>MG36</strong>，輕機槍型</p></td>
<td><p>999/758</p></td>
<td><p>480</p></td>
<td><p>3.6/4.0</p></td>
<td><p>3倍光學瞄準鏡、<a href="../Page/紅點鏡.md" title="wikilink">紅點瞄準鏡</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>G36E（V）</strong>，出口型</p></td>
<td><p>1002/755</p></td>
<td><p>480</p></td>
<td><p>3.63/3.77</p></td>
<td><p>1.5倍光學瞄準鏡</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>G36KE（K）</strong>，出口短型</p></td>
<td><p>716/500</p></td>
<td><p>228</p></td>
<td><p>2.988/3.128</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>MG36E</strong>，出口輕機槍型</p></td>
<td><p>999/758</p></td>
<td><p>480</p></td>
<td><p>3.3/3.7</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/HK_SL8半自動步槍.md" title="wikilink">SL8</a></strong>半自動民用型</p></td>
<td><p>980–1030<br />
（固定槍托）</p></td>
<td><p>510</p></td>
<td><p>4.2/4.4</p></td>
<td><p><a href="../Page/機械瞄具.md" title="wikilink">機械瞄具</a><br />
（可加裝瞄準鏡）</p></td>
<td><p>標準（10發）</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/HK_SL9SD消聲狙擊步槍.md" title="wikilink">SL9SD</a></strong>消聲狙擊型</p></td>
<td><p>1150<br />
（固定槍托）</p></td>
<td><p>510</p></td>
<td><p>4.5/4.6（連消聲器）</p></td>
<td><p><a href="../Page/機械瞄具.md" title="wikilink">機械瞄具</a><br />
（可加裝瞄準鏡）</p></td>
<td><p>標準（10發）</p></td>
</tr>
</tbody>
</table>

  - 扳機系统：

**G36K／C／KE設有四段式扳機系统：**

  - **S**（安全）、**E**（單發）、**2**（二連發）、**F**（全自動）

**G36／G36E有以下不同扳機系统：**

  - **S**（安全）、**E**（單發）、**F**（全自動，[德國聯邦國防軍制式版本](../Page/德國聯邦國防軍.md "wikilink")）
  - **S**（安全）、**E**（單發）、**2**（二連發）、**F**（全自動）
  - **0**（安全）、**1**（單發）、**2**（二連發）
  - **0**（安全）、**1**（單發）、**2**（二連發）、**3**（三連發）

## 流行文化

G36系列也出現在許多電影、電視劇、電子遊戲和動畫當中，例如：

### [電影](../Page/電影.md "wikilink")

  - 2001年—《[-{zh-tw:救世主;zh-hk:最後一強;zh-cn:宇宙追缉令;}-](../Page/宇宙追缉令.md "wikilink")》（The
    One）：型號為G36K，被蓋布（[李連杰飾演](../Page/李連杰.md "wikilink")）與警察同僚所使用。
  - 2002年—《[-{zh-hk:未來殺人網絡; zh-tw:重裝任務;
    zh-cn:撕裂的末日;}-](../Page/重裝任務.md "wikilink")》（Equilibrium）：型號為標準型和G36K，被「清道夫」部隊、警察和安德烈·勃蘭特（飾演）所使用，其中一枝標準型亦曾被主角約翰·普雷斯頓（[克里斯汀·貝爾飾演](../Page/克里斯汀·貝爾.md "wikilink")）所繳獲。
  - 2002年—《[-{zh-hk:生化危機; zh-tw:惡靈古堡;
    zh-cn:生化危机;}-](../Page/生化危機_\(電影\).md "wikilink")》（Resident
    Evil）：型號為G36K，部份裝上[HK
    AG36榴彈發射器](../Page/HK_AG36榴彈發射器.md "wikilink")（不曾使用），被[保護傘公司的突擊隊員們所使用](../Page/保護傘公司.md "wikilink")。
  - 2003年—《[-{zh-hk:廿二世紀殺人網絡2：決戰未來; zh-tw:駭客任務：重裝上陣;
    zh-cn:黑客帝国2：重装上阵;}-](../Page/黑客帝國2：重裝上陣.md "wikilink")》（The
    Matrix Reloaded）：型號為G36K，裝上[HK
    AG36榴彈發射器](../Page/HK_AG36榴彈發射器.md "wikilink")（不曾使用），被梅羅文加的一名手下所使用。
  - 2006年—《[-{zh-hk:職業特工隊3; zh-tw:不可能的任務3;
    zh-cn:碟中谍3;}-](../Page/不可能的任務3.md "wikilink")》（Mission:
    Impossible III）：型號為G36K，主角伊森·韓特於跨海大橋上使用，擊落了無人攻擊機。
  - 2007年—《[-{zh-hk:辣手槍; zh-tw:狙擊生死線;
    zh-cn:生死狙击;}-](../Page/狙擊生死線.md "wikilink")》（The
    Shooter）：型號為G36KE和G36C，前者被桑德爾的守衛所使用；後者於故事尾段被約翰遜上校的兩名守衛所使用。
  - 2007年—《[變形金剛](../Page/變形金剛.md "wikilink")》（Transformer）：型號為G36C，被[聯邦調查局探員所使用](../Page/聯邦調查局.md "wikilink")。
  - 2008年—《[-{zh-hk:鐵甲奇俠; zh-tw:鋼鐵人;
    zh-cn:钢铁侠;}-](../Page/鋼鐵人_\(電影\).md "wikilink")》（Iron
    Man）
  - 2008年—《[-{zh-hk:叛諜同謀; zh-tw:謊言對決;
    zh-cn:谎言之躯;}-](../Page/謊言對決.md "wikilink")》（Body of
    Lies）：型號為G36K和G36C，前者於故事開頭時被英國武裝特警隊隊員所使用；後者裝上H\&K原廠的瞄準鏡和提把並被主角羅傑·費里斯（[里安納度·狄卡比奧飾演](../Page/里安納度·狄卡比奧.md "wikilink")）所使用。
  - 2008年—《[-{zh-hk:殺神特工; zh-tw:刺客聯盟;
    zh-cn:通缉令;}-](../Page/刺客聯盟.md "wikilink")》（Wanted）：型號為G36C，裝上H\&K原廠的瞄準鏡和提把，被一名殺手所使用。
  - 2009年—《[-{zh-hk:未來戰士2018; zh-tw:魔鬼終結者：未來救贖;
    zh-cn:终结者2018;}-](../Page/魔鬼終結者：未來救贖.md "wikilink")》（Terminator
    Salvation）：型號為G36C，被人類抵抗軍所使用。
  - 2011年—《[-{zh-hans:机械师;zh-hant:極速秒殺;zh-hk:秒速殺機;}-](../Page/极速秒杀.md "wikilink")》（The
    Mechanic）：型號為G36C，被史提夫·麥肯納（[班·佛斯特飾演](../Page/班·佛斯特.md "wikilink")）所使用。
  - 2012年—《[-{zh-hk:生化危機之滅絕真相; zh-tw:惡靈古堡5：天譴日;
    zh-cn:生化危机5：惩罚;}-](../Page/惡靈古堡5：天譴日.md "wikilink")》（Resident
    Evil:
    Retribution）：型號為G36C，有著銀黑兩色的機匣並裝上[全息瞄準鏡和](../Page/全息瞄準鏡.md "wikilink")[前握把](../Page/輔助握把.md "wikilink")，被路瑟·威斯特（布利斯·高祖飾演）所使用。
  - 2012年—《[-{zh-hk:轟天猛將2; zh-tw:浴血任務2;
    zh-cn:敢死队2;}-](../Page/敢死队2.md "wikilink")》（The
    Expendables 2）：型號為G36KV和G36C：
      - G36C裝上[紅點鏡和](../Page/紅點鏡.md "wikilink")3排並聯彈匣，被獨行俠布克（[查克·羅禮士飾演](../Page/查克·羅禮士.md "wikilink")）所使用。
      - G36KV被桑族武裝份子所使用。
  - 2012年—《[-{zh-hk:追擊拉登行動; zh-tw:00:30凌晨密令;
    zh-cn:猎杀本·拉登;}-](../Page/00:30凌晨密令.md "wikilink")》（Zero
    Dark
    Thirty）：型號為G36C，裝上[前握把](../Page/輔助握把.md "wikilink")，被[中央情報局特工所使用](../Page/中央情報局.md "wikilink")。
  - 2014年—《[-{zh-hk:轟天猛將3; zh-tw:浴血任務3;
    zh-cn:敢死队3;}-](../Page/敢死队3.md "wikilink")》（The
    Expendables 3）：型號為G36K和G36C：
      - G36K被康拉德·史東班克斯的手下和士兵所使用，其中一枝被主角巴尼·羅斯（[席維斯·史特龍飾演](../Page/席維斯·史特龍.md "wikilink")）所繳獲。
      - G36C裝上瞄準鏡並被康拉德·史東班克斯（[梅爾·吉勃遜飾演](../Page/梅爾·吉勃遜.md "wikilink")）狙擊海爾·凱撒（[泰瑞·克魯斯飾演](../Page/泰瑞·克魯斯.md "wikilink")）時所使用，也被敵軍士兵和李·聖誕（[傑森·史塔森飾演](../Page/傑森·史塔森.md "wikilink")）所使用。
  - 2016年—《[-{zh-hk:白宮淪陷2：倫敦淪陷; zh-tw:全面攻佔2：倫敦救援;
    zh-cn:倫敦陷落;}-](../Page/全面攻佔2：倫敦救援.md "wikilink")》（London Has
    Fallen）：型號為G36K和G36C：
      - G36K被[英國陸軍第](../Page/英國陸軍.md "wikilink")22[特種空勤團隊員所使用](../Page/特種空勤團.md "wikilink")。
      - G36C被恐怖份子所使用，也被主角麥克·班寧（[傑拉德·巴特勒飾演](../Page/傑拉德·巴特勒.md "wikilink")）所繳獲。
  - 2016年—《[-{zh-hans:会计刺客;zh-hant:會計師;zh-hk:暗算;}-](../Page/會計師_\(2016年電影\).md "wikilink")》（The
    Accountant）：型號為G36C，被布拉克斯頓·「布拉克斯」·沃夫的手下所使用。
  - 2017年—《[-{zh-cn:王牌特工2：黄金圈; zh-hk:皇家特工：金圈子;
    zh-tw:金牌特務：機密對決;}-](../Page/金牌特務：機密對決.md "wikilink")》（Kingsman:
    The Golden
    Circle）：型號為G36C，被罌粟·亞當斯的手下所使用，亦被哈利·哈特（[柯林·佛斯飾演](../Page/柯林·佛斯.md "wikilink")）所繳獲。

### [電視劇](../Page/電視劇.md "wikilink")

  - 《[24](../Page/24_\(電視劇\).md "wikilink")》
  - 《》（Chris Ryan's Strike Back）

### [電子遊戲](../Page/電子遊戲.md "wikilink")

  - 2004年—《[-{zh-hans:孤岛惊魂;
    zh-hant:極地戰嚎;}-](../Page/極地戰嚎.md "wikilink")》（Far
    Cry）：型號為G36KV，命名為「AG36」，裝上[HK
    AG36榴彈發射器](../Page/HK_AG36榴彈發射器.md "wikilink")。
  - 2005年—《[-{zh-hans:战地;
    zh-hant:戰地風雲;}-2](../Page/戰地風雲2.md "wikilink")》（Battlefield
    2）：型號為G36C、G36E、G36KV和MG36E。
  - 2005年—《[真實計劃](../Page/真實計劃.md "wikilink")》（Project
    Reality）：型號為標準型和G36K。
  - 2006年—《[-{zh-hans:幽灵行动：尖峰战士;
    zh-hant:火線獵殺：先進戰士;}-](../Page/火線獵殺：先進戰士.md "wikilink")》（Ghost
    Recon Advanced Warfighter）：型號為G36K，無法被玩家使用。
  - 2007年—《[-{zh-hans:幽灵行动：尖峰战士;
    zh-hant:火線獵殺：先進戰士;}-2](../Page/火線獵殺：先進戰士2.md "wikilink")》（Ghost
    Recon Advanced Warfighter 2）：型號為G36C、G36KV和MG36。
  - 2007年—《[戰地之王](../Page/戰地之王.md "wikilink")》（Alliance of Valiant Arms）
  - 2007年—《[-{zh-hans:武装突袭;
    zh-hant:武裝行動;}-](../Page/武裝行動.md "wikilink")》（ArmA:
    Armed Assault）
  - 2007年—《[-{zh-hans:潜行者：切尔诺贝利的阴影;
    zh-hant:浩劫殺陣：車諾比之影;}-](../Page/浩劫殺陣：車諾比之影.md "wikilink")》（S.T.A.L.K.E.R:
    Shadow of Chernobyl）：型號為標準型，命名為「GP37」。
  - 2007年—《[-{zh-hans:使命召唤4：现代战争; zh-hk:使命召喚4：現代戰爭;
    zh-hant:決勝時刻4：現代戰爭;}-](../Page/決勝時刻4：現代戰爭.md "wikilink")》（Call
    of Duty 4: Modern
    Warfare）：型號為G36C，故事模式中被[英國陸軍](../Page/英國陸軍.md "wikilink")[空降特勤隊](../Page/空降特勤隊.md "wikilink")、[俄羅斯政府軍及](../Page/俄羅斯.md "wikilink")[極端民族主義黨武裝勢力所使用](../Page/極端民族主義.md "wikilink")。聯機模式可使用[榴彈發射器](../Page/M203榴彈發射器.md "wikilink")、[紅點鏡](../Page/紅點鏡.md "wikilink")、[消音器及](../Page/抑制器.md "wikilink")[ACOG光學瞄準鏡](../Page/ACOG光學瞄準鏡.md "wikilink")。
  - 2007年—《[-{zh-hans:反恐精英Online;
    zh-hant:絕對武力Online;}-](../Page/絕對武力Online.md "wikilink")》（Counter-Strike
    Online）：型號為MG36和G36C，
      - MG36使用啞黑色槍身和原廠的瞄準鏡（可用）並以100發[C-Mag彈鼓供彈](../Page/C-Mag彈鼓.md "wikilink")。此外具黃金噴漆版和聖誕限定版（奇怪地發射冰子彈）。
      - G36C以「Balrog-V」的虛構武器登場，裝上[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")（奇怪地具放大功能）和[前握把](../Page/輔助握把.md "wikilink")，並以40發彈匣供彈。在对同一个目标连射超过20发子弹后的每一发弹药都会在目标身上产生小型爆炸造成更高的伤害（对机械类敌人无效），转移射击目标后爆炸效果会重置。
  - 2007年—《[穿越火线](../Page/穿越火线.md "wikilink")》（Crossfire）：型号为G36K以及标准型：
      - G36K使用不加装红点瞄准镜的原厂光学瞄准镜，命名为“G36K”，使用30发弹匣。需要玩家达到少尉1军衔后方可使用25000GP购买并永久使用。右键可使用瞄准镜瞄准，拥有出色的稳定性与威力，但远距离散布较大是其明显缺点。
      - 标准型使用原厂瞄准镜，加装100发C-Mag弹鼓以及双脚架被用作充当“MG36”。没有军衔限制，可使用16000GP购买并永久使用。右键可使用红点镜瞄准，拥有突击步枪级别的重量，后坐力、稳定性以及弹道散布在所有武器中均属上乘，但威力较低是其唯一的缺点。
  - 2007年—《[-{zh-hans:军团要塞;
    zh-hant:絕地要塞;}-2](../Page/絕地要塞2.md "wikilink")》（Team
    Fortress 2）：型號為標準型，命名為「經典」，以[狙擊步槍的形象登場](../Page/狙擊步槍.md "wikilink")。
  - 2008年—《[-{zh-hans:镜之边缘;
    zh-hant:靚影特務;}-](../Page/镜之边缘.md "wikilink")》（Mirror's
    Edge）：型号为G36C。故事模式中被PK组织的突击手所使用，亦可被女主角绯丝·康纳斯（Faith Connors）所缴获。
  - 2008年—《[-{zh-hans:战地：叛逆连队;
    zh-hant:戰地風雲：惡名昭彰;}-](../Page/战地：叛逆连队.md "wikilink")》（Battlefield:
    Bad Company）：型號為MG36。
  - 2009年—《[-{zh-hans:武装突袭;
    zh-hant:武裝行動;}-2](../Page/武裝行動2.md "wikilink")》（ArmA
    2）：型號為標準型、G36C、G36K和MG36。
  - 2010年—《[-{zh-hans:战地：叛逆连队2;
    zh-hant:戰地風雲：惡名昭彰2;}-](../Page/战地：叛逆连队2.md "wikilink")》（Battlefield:
    Bad Company 2）：型號為MG36。
  - 2011年—《[-{zh-hans:战地;
    zh-hant:戰地風雲;}-3](../Page/戰地風雲3.md "wikilink")》（Battlefield
    3）：型號為標準型和G36C。其中標準型被用作充當「MG36」。G36C於故事模式中被法國警察和[國家憲兵干預組所使用](../Page/國家憲兵干預組.md "wikilink")。
  - 2011年—《[-{zh-hans:使命召唤：现代战争; zh-hk:使命召喚：現代戰爭;
    zh-hant:決勝時刻：現代戰爭;}-3](../Page/決勝時刻：現代戰爭3.md "wikilink")》（Call
    of Duty: Modern Warfare 3）：型號為G36C和G36KV。
      - G36C於故事模式中被尤里和[國家憲兵干預組所使用](../Page/國家憲兵干預組.md "wikilink")。聯機模式和生存模式可使用改裝包括：[榴彈發射器](../Page/M320榴彈發射器.md "wikilink")、[下掛式霰彈槍](../Page/Masterkey槍管下掛式霰彈槍.md "wikilink")、[紅點鏡](../Page/紅點鏡.md "wikilink")、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")、[消音器](../Page/抑制器.md "wikilink")、[心跳探測儀](../Page/心率.md "wikilink")、[ACOG光學瞄準鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")、[熱能探測式瞄具](../Page/熱輻射.md "wikilink")、混合式瞄準鏡及延長[彈匣](../Page/彈匣.md "wikilink")（增至45發）。
      - G36KV被用作充當「MG36」，載彈量為100發。故事模式中被[俄羅斯舊政府軍所使用](../Page/俄羅斯.md "wikilink")。聯機模式時可使用改裝包括，並能使用[紅點鏡](../Page/紅點鏡.md "wikilink")、[ACOG光學瞄準鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")、[熱能探測式瞄具](../Page/熱輻射.md "wikilink")、混合式瞄準鏡、[消音器](../Page/抑制器.md "wikilink")、心率探測儀、[前握把](../Page/輔助握把.md "wikilink")、射速增加和增大容量[彈鼓](../Page/彈鼓.md "wikilink")（增至200發）。
  - 2012年—《[-{zh-hans:荣誉勋章：战士;
    zh-hant:榮譽勳章：鐵血悍將;}-](../Page/榮譽勳章：鐵血悍將.md "wikilink")》（Medal
    of Honor:
    Warfighter）：型號為G36C，命名為「G36C」，為[SASR](../Page/特種空勤團_\(澳大利亞\).md "wikilink")、FSK/HJK和[KSK特戰兵可用武器](../Page/特種部隊指令部.md "wikilink")，並能夠按用戶需求進行改裝。
  - 2012年—《[战争前线](../Page/战争前线.md "wikilink")》（Warface）：型号为G36KV、G36C和MG36，均使用导轨提把且均为普通解锁武器；
      - G36KV命名为“H\&K
        G36K”，使用30发弹匣，枪身与弹匣上用蓝色油漆写上“P18”字样。为步枪手专用武器，可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、突击消音器、[突击制退器](../Page/炮口制动器.md "wikilink")\]）、战术导轨配件（[通用握把](../Page/辅助握把.md "wikilink")、突击握把）以及瞄准镜（[EoTech
        553全息瞄准镜](../Page/全息瞄准镜.md "wikilink")、绿点全息瞄准镜、[ELCAN
        SpecterDS瞄准镜](../Page/C79光学瞄准镜.md "wikilink")、[红点瞄准镜](../Page/红点镜.md "wikilink")、[步枪高级瞄准镜](../Page/ELCAN_SpecterDR光学瞄准镜.md "wikilink")）。拥有万圣节涂装版本与丛林迷彩涂装版本，强化威力与射程，可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、突击消音器、[突击制退器](../Page/炮口制动器.md "wikilink")、[突击刺刀](../Page/刺刀.md "wikilink")）、战术导轨配件（[通用握把](../Page/辅助握把.md "wikilink")、突击握把、突击握把架、[枪挂型榴弹发射器](../Page/FN_EGLM附加型榴弹发射器.md "wikilink")）以及瞄准镜（[EoTech
        553全息瞄准镜](../Page/全息瞄准镜.md "wikilink")、绿点全息瞄准镜、[ELCAN
        SpecterDS瞄准镜](../Page/C79光学瞄准镜.md "wikilink")、[红点瞄准镜](../Page/红点镜.md "wikilink")、[步枪高级瞄准镜](../Page/ELCAN_SpecterDR光学瞄准镜.md "wikilink")、[Trijicon
        ACOG瞄准镜](../Page/先進戰鬥光學瞄準鏡.md "wikilink")）。
          - 丛林迷彩版本为活动专属武器，万圣节版本可以通过万圣节期间购买时限以及通关生存模式“Cyber
            horde”地图奖励箱获得，对生化僵尸的伤害增加20%。
      - MG36命名为“H\&K
        MG36”，使用100发C-mag弹鼓，枪身上用绿色油漆写上“S28”字样。为步枪手专用武器，并可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、突击消音器、[突击制退器](../Page/炮口制动器.md "wikilink")）、战术导轨配件（[通用握把](../Page/辅助握把.md "wikilink")、突击握把、[机枪双脚架](../Page/两脚架.md "wikilink")）以及瞄准镜（[EoTech
        553全息瞄准镜](../Page/全息瞄准镜.md "wikilink")、绿点全息瞄准镜、[ELCAN
        SpecterDS瞄准镜](../Page/C79光学瞄准镜.md "wikilink")、[红点瞄准镜](../Page/红点镜.md "wikilink")、[步枪高级瞄准镜](../Page/ELCAN_SpecterDR光学瞄准镜.md "wikilink")）。拥有万圣节涂装版本与丛林迷彩涂装版本，强化威力与射程，可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、突击消音器、[突击制退器](../Page/炮口制动器.md "wikilink")）、战术导轨配件（[通用握把](../Page/辅助握把.md "wikilink")、突击握把、突击握把架、[机枪双脚架](../Page/两脚架.md "wikilink")）以及瞄准镜（[EoTech
        553全息瞄准镜](../Page/全息瞄准镜.md "wikilink")、绿点全息瞄准镜、[ELCAN
        SpecterDS瞄准镜](../Page/C79光学瞄准镜.md "wikilink")、[红点瞄准镜](../Page/红点镜.md "wikilink")、[步枪高级瞄准镜](../Page/ELCAN_SpecterDR光学瞄准镜.md "wikilink")、[Trijicon
        ACOG瞄准镜](../Page/先進戰鬥光學瞄準鏡.md "wikilink")）。
          - 万圣节版本可以通过万圣节期间购买时限以及通关生存模式“Cyber
            horde”地图奖励箱获得，对生化僵尸的伤害增加20%。
      - G36C命名为“H\&K
        G36C”，使用30发弹匣，造型上没有安装枪托，枪身上用红色油漆写上“S35”字样。为工程师专用武器，可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、冲锋枪消音器、[冲锋枪制退器](../Page/炮口制动器.md "wikilink")）、战术导轨配件（[通用握把](../Page/辅助握把.md "wikilink")、[冲锋枪握把](../Page/宽型前握把.md "wikilink")）以及瞄准镜（[EoTech
        553全息瞄准镜](../Page/全息瞄准镜.md "wikilink")、绿点全息瞄准镜、[红点瞄准镜](../Page/红点瞄准镜.md "wikilink")、冲锋枪普通瞄准镜、冲锋枪高级瞄准镜）。拥有万圣节涂装版本，强化威力与射程，可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、冲锋枪消音器、[冲锋枪制退器](../Page/炮口制动器.md "wikilink")、[冲锋枪刺刀](../Page/刺刀.md "wikilink")）、战术导轨配件（[通用握把](../Page/辅助握把.md "wikilink")、[冲锋枪握把](../Page/宽型前握把.md "wikilink")、冲锋枪握把架）以及瞄准镜（[EoTech
        553全息瞄准镜](../Page/全息瞄准镜.md "wikilink")、绿点全息瞄准镜、[红点瞄准镜](../Page/红点瞄准镜.md "wikilink")、冲锋枪普通瞄准镜、冲锋枪高级瞄准镜、[冲锋枪专家瞄准镜](../Page/Leupold_Mark_4_CQ/T光学瞄准镜.md "wikilink")）。
          - 万圣节版本可以通过万圣节期间购买时限以及通关生存模式“Cyber
            horde”地图奖励箱获得，对生化僵尸的伤害增加20%。
  - 2013年—《[-{zh-hans:战地;
    zh-hant:戰地風雲;}-4](../Page/戰地風雲4.md "wikilink")》（Battlefield
    4）：型號為G36C，歸類為[卡賓槍](../Page/卡賓槍.md "wikilink")，30+1發彈匣，單機模式中能夠被主角丹尼爾·雷克（Daniel
    Recker）所使用。聯機模式中為所有兵種的可解鎖武器。
  - 2013年—《[-{zh-hans:收获日;
    zh-hant:劫薪日;}-2](../Page/劫薪日2.md "wikilink")》：型號為G36C(預設)，命名為「JP-36」，可在黑市購買後並另外花費自行組裝。
  - 2014年—《合同戰爭》（Contract Wars）
  - 2014年—《[特種部隊2 Online](../Page/特種部隊2_Online.md "wikilink")》
  - 2014年—《[俠盜獵車手V](../Page/俠盜獵車手V.md "wikilink")》(Grand Theft Auto
    V)：更改了部份結構設計並改名為「特製卡賓步槍」(Special
    Carbine）隨著線上模式的商務內容更新登場並開放玩家購買。可加裝裝60發擴充彈夾、[100發彈鼓](../Page/C-Mag彈鼓.md "wikilink")、[消音器](../Page/抑制器.md "wikilink")、[瞄準鏡等改裝套件](../Page/瞄準鏡.md "wikilink")，也可視喜好塗裝成數種色調。
  - 2015年—《[-{zh-hans:彩虹六号：围攻;
    zh-hant:虹彩六號：圍攻行動;}-](../Page/虹彩六號：圍攻行動.md "wikilink")》（Rainbow
    Six:
    Siege）：型號為G36C，被[聯邦調查局特種武器和戰術部隊所使用](../Page/聯邦調查局特種武器和戰術部隊.md "wikilink")。
  - 2015年—《[-{zh-hans:战地：硬仗;
    zh-hant:戰地風雲：強硬路線;}-](../Page/战地：硬仗.md "wikilink")》（Battlefield
    Hardline）：型號為G36C和G36E。
      - G36C命名為「G36C」，30+1發[彈匣](../Page/彈匣.md "wikilink")，歸類為[卡賓槍](../Page/卡賓槍.md "wikilink")，被警方操作者（Operator）所使用（罪犯解鎖條件為：以任何陣營進行遊戲使用該槍擊殺1250名敵人後購買武器執照），價格為$42,000。可加裝各種瞄準鏡（[反射](../Page/紅點鏡.md "wikilink")、眼鏡蛇、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")（放大1倍）、PKA-S（放大1倍）、Micro
        T1、SRS 02、[Comp
        M4S](../Page/Aimpoint_Comp_M4紅點鏡.md "wikilink")、[M145](../Page/C79光學瞄準鏡.md "wikilink")（放大3.4倍）、PO（放大3.5倍）
        、[ACOG](../Page/先進戰鬥光學瞄準鏡.md "wikilink")（放大4倍）、[PSO-1](../Page/PSO-1光學瞄準鏡.md "wikilink")（放大4倍）、[IRNV](../Page/夜視儀.md "wikilink")（放大1倍）、[FLIR](../Page/熱成像儀.md "wikilink")（放大2倍））、附加配件（傾斜式[機械瞄具](../Page/機械瞄具.md "wikilink")、傾斜式紅點鏡、延長彈匣（增至35+1發）、[電筒](../Page/電筒.md "wikilink")、[戰術燈](../Page/戰術燈.md "wikilink")、[激光瞄準器](../Page/激光指示器.md "wikilink")、[穿甲](../Page/穿甲彈.md "wikilink")[曳光彈](../Page/曳光彈.md "wikilink")）、槍口零件（[槍口制退器](../Page/砲口制動器.md "wikilink")、補償器、重[槍管](../Page/槍管.md "wikilink")、[抑制器](../Page/抑制器.md "wikilink")、消焰器）及[握把](../Page/輔助握把.md "wikilink")（垂直握把、粗短握把、[直角握把](../Page/直角前握把.md "wikilink")）。劇情模式當中則能夠被主角尼古拉斯·門多薩所使用。
      - G36E命名為「MG36」，100+1發[彈鼓](../Page/彈鼓.md "wikilink")，被設定為戰鬥拾取武器，預設配備Aimpoint
        Comp
        M4S[紅點鏡](../Page/紅點鏡.md "wikilink")、垂直[握把及](../Page/輔助握把.md "wikilink")[槍口制退器](../Page/砲口制退器.md "wikilink")。
  - 2016年—《[-{zh-hans:使命召唤：现代战争; zh-hk:使命召喚：現代戰爭;
    zh-hant:決勝時刻：現代戰爭;}-重製版](../Page/決勝時刻4：現代戰爭.md "wikilink")》（Call
    of Duty: Modern Warfare Remastered）：型號為G36C，外觀比前作更為精細。
  - 2016年—《[湯姆克蘭西：全境封鎖](../Page/湯姆克蘭西：全境封鎖.md "wikilink")》(Tom Clancy's
    The Division): 型號為軍用G36, 加強型G36 與 G36C,於1.3 改版時加入,歸類為突擊步槍
  - 2016年—《[少女前線](../Page/少女前線.md "wikilink")》：型號為G36和G36C的少女戰術人形，G36被歸類為突擊步槍，而G36C被歸類為衝鋒槍，兩個戰術人形在遊戲中為姐妹關係。
  - 2017年—《[硝云弹雨](../Page/硝云弹雨.md "wikilink")》（Ballistic
    Overkill）：型号为MG36，作为“冲锋队”职业可用武器之姿出现，命名为“鳏夫”，使用原厂瞄准镜、[C-Mag弹鼓装弹并加装](../Page/C-Mag弹鼓.md "wikilink")[消音器](../Page/抑制器.md "wikilink")，在冲锋队职业等级达到17级时可解锁并使用。
  - 2018年—《[絕地求生](../Page/絕地求生.md "wikilink")》（*PlayerUnknown's
    Battlegrounds*）會在最新的雪地地圖刷新G36C,
    目前是在體驗服將在2018年12月19日(三)跟新正式版.裝彈量為30（擴容後為40），可配備：所有突擊步槍槍口及彈匣、所有握把、紅點瞄準鏡、全息瞄準鏡、6倍及以下高倍瞄準鏡。
  - 2019年—《[湯姆克蘭西：全境封鎖2](../Page/湯姆克蘭西：全境封鎖2.md "wikilink")》(Tom
    Clancy's The Division 2)：以軍用G36的名稱登場。

### [動畫](../Page/動畫.md "wikilink")

  - 2009年—《[幻靈鎮魂曲](../Page/Phantom_-PHANTOM_OF_INFERNO-.md "wikilink")》：型號為G36K，被Vier和Acht所使用。
  - 2012年—《[軍火女王](../Page/軍火女王.md "wikilink")》（Jormungand）

### [輕小說](../Page/輕小說.md "wikilink")

  - 2014年—《[刀劍神域外傳Gun Gale
    Online](../Page/刀劍神域外傳Gun_Gale_Online.md "wikilink")》：型號為G36K，被“MMTM”小隊的“健太”和“勒克斯”所使用。

## 參考文獻

  - [HKPRO-G36C](https://web.archive.org/web/20100102071441/http://hkpro.com/index.php?option=com_content&view=article&id=81:g36c&catid=8:the-automatic-rifles&Itemid=5)
  - [HKPRO-G36系列](https://web.archive.org/web/20100102071509/http://hkpro.com/index.php?option=com_content&view=article&id=82:g36-series&catid=8:the-automatic-rifles&Itemid=5)
  - [Modern
    Firearms-G36頁面](https://web.archive.org/web/20100902030204/http://world.guns.ru/assault/as14-e.htm)

## 資料來源

## 相關條目

  - [XM8](../Page/XM8突擊步槍.md "wikilink")
  - [HK36](../Page/HK36突擊步槍.md "wikilink")
  - [HK G3](../Page/HK_G3自動步槍.md "wikilink")
  - [HKG41](../Page/HK_G41突擊步槍.md "wikilink")
  - [HK416](../Page/HK416突擊步槍.md "wikilink")
  - [HK433](../Page/HK433突擊步槍.md "wikilink")
  - [HK M27 IAR](../Page/HK_M27步兵自動步槍.md "wikilink")
  - [HK G11](../Page/HK_G11突擊步槍.md "wikilink")
  - [FX-05 Xiuhcoatl](../Page/FX-05突擊步槍.md "wikilink")
  - [CETME L型](../Page/CETME_L型突擊步槍.md "wikilink")
  - [CZ-805 Bren](../Page/CZ-805_Bren突擊步槍.md "wikilink")
  - [StG-940](../Page/StG-940突擊步槍.md "wikilink")
  - [各國軍隊制式步槍列表](../Page/各國軍隊制式步槍列表.md "wikilink")
  - [HK SL8](../Page/HK_SL8半自動步槍.md "wikilink")

## 外部链接

  - \-[黑克勒-科赫G36產品頁面](http://www.heckler-koch.de/core.php?dat=Y29tcG9uZW50PWFydGljbGVzJmFjdGlvbj1zaG93JnhJRD1wcm9kdWN0QXJ0aWNsZXMmY2F0SUQ9MTMwMiZuYXZpZ2F0aW9uSUQ9NzI4JnBhcmVudElEPTcyNCZsYW5nSUQ9NSZ1c2VGbGFzaD0x)

  - \-[G36分解圖及介紹](http://www.bimbel.de/artikel/artikel-23.html)

  - \-[G36 及 FX-05對比](http://www.mexidata.info/id1289.html)

  - \-[Modern
    Firearms-G36頁面](https://web.archive.org/web/20100902030204/http://world.guns.ru/assault/as14-e.htm)

  - [G36C射擊短片](https://web.archive.org/web/20070927132929/http://www.wildwestguns.com/Bushwacker/Machine_Gun_Fun/MOV00005.MPG)

  - \-[枪炮世界—G36 自动步枪系列](http://firearmsworld.net/german/hk/g36/G36.htm)

[Category:黑克勒-科赫](../Category/黑克勒-科赫.md "wikilink")
[Category:自動步槍](../Category/自動步槍.md "wikilink")
[Category:突擊步槍](../Category/突擊步槍.md "wikilink")
[Category:5.56×45毫米槍械](../Category/5.56×45毫米槍械.md "wikilink")
[Category:德國槍械](../Category/德國槍械.md "wikilink")

1.

2.  <http://www.iol.co.za/news/africa/german-rifles-in-gaddafi-compound-1.1128424>

3.  <http://nottsantimilitarism.wordpress.com/2011/10/24/heckler-koch-investigated-for-arms-to-gaddafi/>

4.  王伟《再谈利比亚》舰载武器2012年1月

5.

6.

7.

8.  Matthias Gebauer: *[Waffe der Bundeswehr: Deutsche Soldaten schossen
    wegen Mangel-Munition
    daneben](http://www.spiegel.de/politik/deutschland/sturmgewehr-g36-mangel-munition-soll-probleme-verursacht-haben-a-954014.html).*
    In: *[Spiegel Online](../Page/Spiegel_Online.md "wikilink").* 17.
    Februar 2014

9.  [Standardgewehr der Bundeswehr Von der Leyen räumt
    „Präzisionsproblem“ beim G36-Gewehr
    ein](http://www.sueddeutsche.de/politik/standardgewehr-der-bundeswehr-von-der-leyen-raeumt-praezisionsproblem-beim-g-gewehr-ein-1.2416922)
    sueddeutsche.de vom 30. März 2015

10.

11.

12. FAZ vom 22.04.2015
    [1](http://www.faz.net/aktuell/politik/verteidigungsministerin-leyen-sturmgewehr-g36-hat-in-der-bundeswehr-keine-zukunft-13552681.html).

13. <http://www.spiegel.de/politik/deutschland/g36-bundesregierung-verliert-prozess-gegen-heckler-koch-a-1110631.html>

14.  DW {{\!}}
    14.10.2018|url=[https://www.dw.com/en/germanys-new-assault-rifles-fail-bundeswehr-tests/a-45879052|work=DW.COM|accessdate=2019-02-16|language=en-GB|first=Deutsche|last=Welle](https://www.dw.com/en/germanys-new-assault-rifles-fail-bundeswehr-tests/a-45879052%7Cwork=DW.COM%7Caccessdate=2019-02-16%7Clanguage=en-GB%7Cfirst=Deutsche%7Clast=Welle)
    (www.dw.com)}}

15. ['G36C Firearms' sold to the Australian Federal Police by HK Systems
    Australia](https://www.tenders.gov.au/?event=public.cn.view&cnUUID=5167C895-9D62-5238-52616E7F121E90AE)

16. [3](http://www.responseaustralia.net/issues/Issue08.pdf)

17. THE JOURNAL OF THE ROYAL ANGLIAN REGIMENT: *Going Forward – The
    Bermuda Regiment Embraces New Challenges*, by Major Joe Carnegie,
    Bermuda Regiment Staff Officer. December 2012, Vol 17 No 2

18.

19. [Folha de São
    Paulo](http://www1.folha.uol.com.br/folha/galeria/imagemdodia/p_20070623_11.shtml)

20. [Folha de
    S.Paulo](http://www1.folha.uol.com.br/folha/galeria/imagemdodia/p_20070623_11.shtml).
    Retrieved June 23rd, 2007

21. [4](http://www.globaltvbc.com/world/Photos+Greater+Victoria+Emergency+Response+Team/3563625/story.html).
    Retrieved July 30th, 2011

22. [China - SALW Guide](https://salw-guide.bicc.de/en/country?id=44)

23.

24.

25.

26.

27.

28.

29.

30.

31.

32.

33.

34.

35.

36. <http://www.politico.eu/article/paris-police-receive-assault-rifles-and-shields-terror-attacks-counter-terrorism-isil-islamic-state-isis-europe-news/>

37.

38.

39. \[<http://www.streitkraeftebasis.de/portal/a/streitkraeftebasis/kcxml/04_Sj9SPykssy0xPLMnMz0vM0Y_QjzKLNwyON3Y0cgRJmsV7x5sEW-pHghimppYQMbACQwOQKExtUEqqvq9Hfm6qvrd-gH5BbmhEuaOjIgDK8ZeR/delta/base64xml/L2dJQSEvUUt3QS80SVVFLzZfMVNfM0E5Mg>\!\!?yw_contentURL=%2F01DB040000000001%2FW26A9DZN077INFODE%2Fcontent.jsp
    Gewehr G 36.\] Bundeswehr.

40. <http://www.n-tv.de/politik/Heckler-Koch-will-BKA-einschalten-article14877066.html>

41. Weisswange, Jan-Phillip (2009). ASSIK. Der Arbeitsstab
    Schutzaufgaben der Bundespolizei. In: Strategie & Technik. Jg. 52,
    Nr. 5, Mai 2009. ISSN 1860-5311, S. 73–74.

42.

43. "Með Glock 17 og MP5". *Fréttatíminn*. 23. 09. 2011. p. 12-14.

44.

45.

46. <http://www.bild.de/politik/inland/isis/diese-waffen-liefert-deutschland-an-die-kurden-37478284.bild.html>

47. [Italian Ministry of Interior – Decree n° 559/A/1/ORG/DIP.GP/14 of
    March 6, 2009, concerning weapons and equipment in use with the
    Italian National Police – in
    Italian](http://www.siulproma.com/pdf/circolari/2009/Circ._Armi_in_uso_alla_Polizia_di_Stato.pdf)
     Retrieved on August 25, 2010.

48. <http://www.mod.go.jp/gsdf/gmcc/hoto/hkou/14hk093.pdf>

49. Shea, Dan (Spring 2009). "SOFEX 2008". *Small Arms Defense Journal*,
    p. 29.

50. Tactical Weapons January 2011 Issue, Page 94.

51.

52. <http://www.mksf-ks.org/repository/docs/Koha_Ditore_per_FSK-ne.pdf>

53.

54.

55.

56.
57.

58.

59.

60. <https://www.youtube.com/watch?v=1BtKiDvpf8M>

61. <http://blogs.aljazeera.net/sites/default/files/imagecache/BlogsMainImage/680_77.jpg>

62. [Lietuva perka papildomą partiją modernizuotų automatinių ginklų
    G-36 KA4M1 ir 40 mm.
    granatsvaidžių](http://kam.lt/lt/naujienos_874/aktualijos_875/pasirasyta_sutartis_del_modernizuotu_automatiniu_ginklu_g-36_ka4m1_ir_40_mm._granatsvaidziu_pirkimo.html)

63.

64.

65.

66.

67.

68.

69.
70.

71.

72. [Ministar odbrane Boro Vučinić održao godišnju konferenciju za
    novinare](http://www.gov.me/odbrana/index.php?akcija=vijesti&id=167468)


73.

74.

75.

76.

77.

78.

79.

80.

81.

82.
83. <http://www.navy.mil/view_single.asp?id=13987>

84.

85. . Retrieved August 11th, 2011

86. [5](http://imageshack.us/photo/my-images/231/swedsfej6.jpg/).
    Retrieved August 11th, 2011

87.

88.

89.

90.

91. *Jane's Police Review*, 4 March 2007

92.

93. [army-technology.com Infanterist der
    Zukunft](http://www.army-technology.com/projects/idz/)

94. [Heckler & Koch subsystem leader
    IdZ](http://www.heckler-koch.de/HKWebNews/byItemID///19/3/3/15)