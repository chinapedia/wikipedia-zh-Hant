**HK
MSG90**是[黑克勒-科赫以](../Page/黑克勒-科赫.md "wikilink")[PSG1為基礎所改進的半自動軍用](../Page/HK_PSG1狙擊步槍.md "wikilink")[狙擊步槍](../Page/狙擊步槍.md "wikilink")，口徑為[7.62×51毫米](../Page/7.62×51mm_NATO.md "wikilink")[NATO](../Page/北大西洋公约组织.md "wikilink")，當中的90解為推出年份（1990年）。

## 歷史

[黑克勒-科赫公司的](../Page/黑克勒-科赫.md "wikilink")[PSG1狙擊步槍擁有極高的射擊精度而且性能優異](../Page/HK_PSG1狙擊步槍.md "wikilink")，不過其價格太高，重量也太重，且射擊時彈殼彈出的力道太大，射擊後常常找不到彈出的彈殼，雖然這些缺點對於[特警隊而言並不會造成太大的問題](../Page/特警隊.md "wikilink")，但是對軍方在戰場上運用的情形來說，就會造成極大的不便，所以當時的[PSG1並沒有受到](../Page/HK_PSG1狙擊步槍.md "wikilink")[德國聯邦國防軍的採用](../Page/德國聯邦國防軍.md "wikilink")。

於是黑克勒-科赫對PSG1開始著手進行改良，試圖讓其符合軍事用途需求，HK將PSG1的設計簡化，減輕槍身各部的重量，並使用輕量化的槍管，達到了降低成本及減輕重量的目標，而成品就是HK
MSG90。

## 設計

MSG是德文「」的縮寫，意思是「軍用射手步槍」，MSG90採用較輕且直徑較小的槍管，槍管前端有一個套管，目的是為了增加槍口的重量，[槍托與](../Page/槍托.md "wikilink")[PSG1一樣具有可調整功能](../Page/HK_PSG1狙擊步槍.md "wikilink")，不過MSG-90的槍托重量較輕，瞄準鏡不用時也可以拆卸下來，防止碰撞所造成的損壞。

## 配備

MSG90配備專用的10×42瞄準鏡，射程可以進行調整，另外也可以選用兩腳架或是三腳架。

## 使用國

[PSG1_and_MSG_90.jpg](https://zh.wikipedia.org/wiki/File:PSG1_and_MSG_90.jpg "fig:PSG1_and_MSG_90.jpg")（下）和**MSG90**（上）\]\]

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 參見

  - [HK G3自動步槍](../Page/HK_G3自動步槍.md "wikilink")
  - [HK PSG1狙擊步槍](../Page/HK_PSG1狙擊步槍.md "wikilink")
  - [HK SL9SD消聲狙擊步槍](../Page/HK_SL9SD消聲狙擊步槍.md "wikilink")
  - [HK G28狙擊步槍](../Page/HK_G28狙擊步槍.md "wikilink")

## 外部链接

  - —[D Boy's Gun
    World—（槍炮世界）MSG90军用狙击步枪](http://firearmsworld.net/german/hk/msg90/MSG90.htm)

  - —[HKPRO-MSG90](https://web.archive.org/web/20070715163019/http://www.hkpro.com/msg90.htm)

  - —[Modern Firearms—Heckler-Koch MSG90 sniper
    rifle](http://world.guns.ru/sniper/sniper-rifles/de/hk-msg-90-e.html)

[Category:黑克勒-科赫](../Category/黑克勒-科赫.md "wikilink")
[Category:半自動步槍](../Category/半自動步槍.md "wikilink")
[Category:狙击步枪](../Category/狙击步枪.md "wikilink")
[Category:精確射手步槍](../Category/精確射手步槍.md "wikilink")
[Category:7.62×51毫米槍械](../Category/7.62×51毫米槍械.md "wikilink")
[Category:德國槍械](../Category/德國槍械.md "wikilink") [Category:HK
G3衍生槍](../Category/HK_G3衍生槍.md "wikilink")