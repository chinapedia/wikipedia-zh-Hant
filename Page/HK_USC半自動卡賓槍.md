**USC**（，中文譯作：通用自動裝填卡賓槍）是[黑克勒-科赫](../Page/黑克勒-科赫.md "wikilink")（H\&K）以[UMP45改造而成的](../Page/HK_UMP衝鋒槍.md "wikilink")[半自動步槍](../Page/半自動步槍.md "wikilink")。

為了符合[美國](../Page/美國.md "wikilink")1994年成立的聯邦突擊武器禁制法案（Federal Assault
Weapons
Ban，AWB），USC裝有16寸槍管、不能與UMP45相容的10發彈匣、半自動扳機及運動步槍式[槍托](../Page/槍托.md "wikilink")（槍托與握把相連），保留[機匣頂部的戰術導軌](../Page/機匣.md "wikilink")，亦可在其他位置增加導軌\[1\]。價格比[HK
SL8](../Page/HK_SL8半自動步槍.md "wikilink")（[HK
G36民用型](../Page/HK_G36突擊步槍.md "wikilink")）便宜。

## 参见

  - [HK UMP衝鋒槍](../Page/HK_UMP衝鋒槍.md "wikilink")

## 参考文献

## 外部链接

  - —[H\&K美國官方網頁](https://web.archive.org/web/20071027074508/http://www.hecklerkoch-usa.com/rifles_usc_general.html)

  - —[Modern Firearms—Heckler-Koch HK USC
    carbine](https://web.archive.org/web/20090228120015/http://world.guns.ru/civil/civ010-e.htm)

  - —[HKPRO—HK
    USC](https://web.archive.org/web/20100102060144/http://hkpro.com/index.php?option=com_content&view=article&id=16:the-usc&catid=13:the-sporting-rifles&Itemid=5)

  - —[D Boy Gun
    World—USC45卡宾枪](http://firearmsworld.net/german/hk/ump/USC.htm)

[en:Heckler & Koch
UMP\#Variants](../Page/en:Heckler_&_Koch_UMP#Variants.md "wikilink")

[Category:半自动步枪](../Category/半自动步枪.md "wikilink")
[Category:黑克勒-科赫](../Category/黑克勒-科赫.md "wikilink")
[Category:.45 ACP口徑槍械](../Category/.45_ACP口徑槍械.md "wikilink")
[Category:德國槍械](../Category/德國槍械.md "wikilink")

1.