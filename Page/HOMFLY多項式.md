在[紐結理論中](../Page/紐結理論.md "wikilink")，**HOMFLY多項式**或**HOMFLY-PT多項式**是一種雙變元的[多項式](../Page/多項式.md "wikilink")[紐結不變量](../Page/紐結不變量.md "wikilink")；透過變元代換，它可以涵括[瓊斯多項式與](../Page/瓊斯多項式.md "wikilink")[亞歷山大多項式在三維的情形](../Page/亞歷山大多項式.md "wikilink")。

「HOMFLY」一名得自該多項式的發現者：Hoste、Ocneanu、Millet、Freyd、Lickorish、Yetter；「PT」二字旨在紀念另兩位獨立發現此結不變量的數學家
Przytycki 與 Traczyk。

## 拆接關係

HOMFLY多項式 \(P_K(\ell, m) = P(K)\)
由下述[拆接關係唯一地定義](../Page/絞關係.md "wikilink")：

  -
    \(P( \mathrm{unknot} ) = 1,\,\)

<!-- end list -->

  -
    \(\ell P(L_+) + \ell^{-1}P(L_-) + mP(L_0)=0,\,\)

其中 \(L_+, L_-, L_0\) 代表結圖表在某個交點附近的性狀，如次圖所示：

[`Skein_(HOMFLY).png`](https://zh.wikipedia.org/wiki/File:Skein_\(HOMFLY\).png "fig:Skein_(HOMFLY).png")

上述關係可用以遞迴計算任一紐結之HOMFLY多項式，亦可導出

  -
    \(P(L_1 \sqcup L_2) = \frac{-(l+l^{-1})}{m} P(L_1)*P(L_2)\)

## 其它拆接關係

透過適當的變元代換，上節的拆接關係可換為

  -
    \(\alpha P(L_+) - \alpha^{-1}P(L_-) = zP(L_0)\)
    或者
    \(x P(L_+)+yP(L_-)+zP(L_0)=0\)

## 主要性質

與瓊斯多項式的關係：

  -
    \(V(t)=P(\alpha=t,z=t^{1/2}-t^{-1/2})\)

與亞歷山大多項式的關係：

  -
    \(\Delta(t)=P(\alpha=1,z=t^{1/2}-t^{-1/2})\)

對鏡像與連通和的關係：

  -
    \(P(L_1 \# L_2)=P(L_1)P(L_2),\,\)
    \(P_K(\ell,m)=P_\mathrm{Mirror Image(K)}(\ell^{-1},m)\)

## 相關文獻

  - Peter Cromwell (2004), *Knots and Links*, Cambridge University
    Press. ISBN 0-521-54831-4

[Category:紐結理論](../Category/紐結理論.md "wikilink")
[Category:多项式](../Category/多项式.md "wikilink")