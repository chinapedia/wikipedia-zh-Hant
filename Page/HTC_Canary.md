**HTC
Canary**（研發代號），是台灣[宏達電公司所推出的全球第一款微軟](../Page/宏達電.md "wikilink")[智慧型手機](../Page/智慧型手機.md "wikilink")，2002年10月23日以Orange
SPV的身份在倫敦正式公開，引發全球通訊業界的高度矚目。2003年8月15日大陸多普達通訊與微軟又再度推出記憶體升級至64MB的HTC
Canary，即全球首部採用中文Windows Mobile 2003的智慧型手機Dopod 515。客製版本Qtek 6080，Dopod
515，Orange SPV。

## Canary技術規格

  - [處理器](../Page/處理器.md "wikilink")：TI OMAP710 133MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Microsoft SmartPhone 2002
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：32MB，RAM：16MB
  - 尺寸：119.8 毫米 X 50.1 毫米 X 23.5 毫米
  - 重量：126.5g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：QCIF 解析度、2.2 吋 TFT-LCD 螢幕
  - [網路](../Page/網路.md "wikilink")：GSM/GPRS
  - [相機](../Page/相機.md "wikilink")：可外接，32萬像素
  - [電池](../Page/電池.md "wikilink")：1100mAh充電式鋰或鋰聚合物電池

## Canary升級版技術規格

  - [處理器](../Page/處理器.md "wikilink")：TI OMAP710 133MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 2003 SmartPhone
    Edition
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：32MB，RAM：32MB
  - 尺寸：119.8mm X 50.1mm X 23.5mm
  - 重量：126.5g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：QCIF 解析度、2.2 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：GSM/GPRS
  - [相機](../Page/相機.md "wikilink")：可外接，32萬像素
  - [電池](../Page/電池.md "wikilink")：1100mAh充電式鋰或鋰聚合物電池

## 參見

  - [HTC](../Page/HTC.md "wikilink")
  - [Qtek](../Page/Qtek.md "wikilink")
  - [Dopod](../Page/Dopod.md "wikilink")

## 外部連結

  - [HTC](http://www.htc.com/)
  - [Qtek](https://web.archive.org/web/20080707182953/http://www.myqtek.com/)
  - [Dopod](http://www.dopod.com/)

[H](../Category/智能手機.md "wikilink")
[Canary](../Category/宏達電手機.md "wikilink")