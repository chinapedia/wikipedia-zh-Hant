**HTC Hermes**，原廠型號**HTC Z，HTC
TyTN**，是台灣[宏達電公司所推出的](../Page/宏達電.md "wikilink")[智慧型手機](../Page/智慧型手機.md "wikilink")，搭載微軟
[Windows Mobile](../Page/Windows_Mobile.md "wikilink") 5，是全球第一臺三頻3G
PDA手機，也是作為以hTC品牌進軍歐洲與日本的首發產品之一，配備[三星](../Page/三星電子.md "wikilink")400
MHz處理器，配有側滑動式[QWERTY鍵盤](../Page/QWERTY.md "wikilink")，性能強大。2006年2月於歐洲首度發表。已知客製版本HTC
P4500，HTC Z，HTC TyTN，Rogers＆HTC TyTN，Qtek 9600，Dopod 838 Pro，Dopod
CHT9000，i-mate JASJAM，O2 Xda Trion，T-Mobile MDA Vario II，Vodafone VPA
Compact III，Vodafone v1605，SFR v1605，Orange SPV M3100，AT\&T
8525，Cingular 8525，Swisscom XPA v1605，SoftBank X01HT。

## 技術規格

  - [處理器](../Page/處理器.md "wikilink")：Samsung SC32442 400MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 5.0 PocketPC Phone
    Edition
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：128MB，RAM：64MB
  - 尺寸：112.5mm X 58mm X 21.95mm
  - 重量：176g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：QVGA 解析度、2.8 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：HSDPA/WCDMA/GSM
  - [藍牙](../Page/藍牙.md "wikilink")：2.0 with EDR
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")：IEEE 802.11 b/g
  - [相機](../Page/相機.md "wikilink")：200萬畫素相機，支援自動對焦功能
  - [電池](../Page/電池.md "wikilink")：1350mAh充電式鋰或鋰聚合物電池

## 參見

  - [HTC](../Page/HTC.md "wikilink")
  - [Qtek](../Page/Qtek.md "wikilink")
  - [Dopod](../Page/Dopod.md "wikilink")
  - [TouchFLO](../Page/TouchFLO.md "wikilink")

## 外部連結

  - [HTC TyTN
    系列](https://web.archive.org/web/20080622102130/http://tytnseries.htc.com/)
  - [HTC Z 概觀](http://www.htc.com/jp/product.aspx?id=24016)
  - [HTC Z 技術規格](http://www.htc.com/jp/product.aspx?id=28602)
  - [HTC TyTN
    概觀](https://web.archive.org/web/20081115061945/http://www.htc.com/us/product.aspx?id=32824)
  - [HTC TyTN
    技術規格](https://web.archive.org/web/20081204185210/http://www.htc.com/us/product.aspx?id=32828)

[H](../Category/智能手機.md "wikilink")
[Hermes](../Category/宏達電手機.md "wikilink")
[Category:2006年面世的手機](../Category/2006年面世的手機.md "wikilink")