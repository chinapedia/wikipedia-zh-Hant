**HTC Kaiser**，原廠型號**HTC TyTN
II**，是台灣[宏達電公司所推出的](../Page/宏達電.md "wikilink")[智慧型手機](../Page/智慧型手機.md "wikilink")，搭載微軟
[Windows Mobile](../Page/Windows_Mobile.md "wikilink") 6，是以性能強大著稱的[HTC
TyTN系列的第二款](../Page/HTC_TyTN.md "wikilink")，[HTC
Hermes的升級版](../Page/HTC_Hermes.md "wikilink")，首次配備[高通處理器](../Page/高通.md "wikilink")，配有側滑動式[QWERTY鍵盤](../Page/QWERTY.md "wikilink")，2007年10月於歐洲首度發表。已知客製版本HTC
P4550，HTC TyTN II，Orange＆HTC TyTN II，Vodafone VPA Compact V，Vodafone
v1615，SFR v1615，Swisscom XPA v1615，T-Mobile MDA Vario III，AT\&T Tilt，O2
Xda Stellar，EMobile Emonster S11HT。

## 技術規格

  - [處理器](../Page/處理器.md "wikilink")：Qualcomm MSM7200 400 MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 6.0 Professional
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：256MB，RAM：128MB
  - 尺寸：112 毫米 X 59 毫米 X 19 毫米
  - 重量：190g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：QVGA 解析度、2.8 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：HSDPA/WCDMA/GSM
  - [GPS](../Page/GPS.md "wikilink")：配備GPS及A-GPS
  - [藍牙](../Page/藍牙.md "wikilink")：2.0 with EDR
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")：IEEE 802.11 b/g
  - [相機](../Page/相機.md "wikilink")：300萬畫素相機，支援自動對焦功能
  - [電池](../Page/電池.md "wikilink")：1350mAh充電式鋰或鋰聚合物電池

## 參見

  - [HTC](../Page/HTC.md "wikilink")
  - [Qtek](../Page/Qtek.md "wikilink")
  - [Dopod](../Page/Dopod.md "wikilink")
  - [TouchFLO](../Page/TouchFLO.md "wikilink")

## 外部連結

  - [HTC TyTN
    系列](https://web.archive.org/web/20080622102130/http://tytnseries.htc.com/)
  - [HTC TyTN II 概觀](http://www.htc.com/tw/product.aspx?id=22822)
  - [HTC TyTN II 技術規格](http://www.htc.com/tw/product.aspx?id=22826)

[H](../Category/智能手機.md "wikilink")
[Kaiser](../Category/宏達電手機.md "wikilink")
[Category:2007年面世的手機](../Category/2007年面世的手機.md "wikilink")