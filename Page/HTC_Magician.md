**HTC
Magician**（研發代號），是台灣[宏達電公司所推出的](../Page/宏達電.md "wikilink")[智慧型手機](../Page/智慧型手機.md "wikilink")，曾是全球最迷你的PDA
Phone。適合女性消費者的PDA手機，2004年9月於歐洲首度發表，2005年3月推出適合男性消費者的升級版Magician Refresh。

已知客製版本：

Magician：Qtek S100，Qtek S110，Dopod 818，O2 Xda II Mini，T-Mobile MDA
Compact，i-mate JAM，i-mate JAM（Limited Charcoal Edition）

Magician Refresh：Dopod 828，Dopod 828+，Orange SPV M500，Vodafone VPA
Compact，Krome Spy

## Magician技術規格

  - [處理器](../Page/處理器.md "wikilink")：Intel XScale PXA270 416MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 2003 Pocket PC
    Phone Second Edition
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：64MB，RAM：64MB
  - 尺寸：108.2 毫米 X 58 毫米 X 18.2 毫米
  - 重量：150g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：QVGA 解析度、2.8 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：GSM/GPRS
  - [藍牙](../Page/藍牙.md "wikilink")：Bluetooth 1.2
  - [相機](../Page/相機.md "wikilink")：130萬畫素相機
  - 擴充：支援MMC/SDIO介面記憶卡
  - [電池](../Page/電池.md "wikilink")：1200mAh充電式鋰或鋰聚合物電池

## Magician Refresh技術規格

  - [處理器](../Page/處理器.md "wikilink")：Intel XScale PXA270 416MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 2003 Pocket PC
    Phone Second Edition
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：64MB，RAM：128MB
  - 尺寸：108.2mm X 58mm X 18.2mm
  - 重量：150g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：QVGA 解析度、2.8 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：GSM/GPRS
  - [藍牙](../Page/藍牙.md "wikilink")：Bluetooth 1.2
  - [相機](../Page/相機.md "wikilink")：130萬畫素相機
  - 擴充：支援MMC/SDIO介面記憶卡
  - [電池](../Page/電池.md "wikilink")：1200mAh充電式鋰或鋰聚合物電池

## 參見

  - [HTC](../Page/HTC.md "wikilink")
  - [Qtek](../Page/Qtek.md "wikilink")
  - [Dopod](../Page/Dopod.md "wikilink")

## 外部連結

  - [HTC](http://www.htc.com/)
  - [Qtek](https://web.archive.org/web/20080707182953/http://www.myqtek.com/)
  - [Dopod](http://www.dopod.com/)

[H](../Category/智能手機.md "wikilink")
[Magician](../Category/宏達電手機.md "wikilink")
[Category:2005年面世的手機](../Category/2005年面世的手機.md "wikilink")