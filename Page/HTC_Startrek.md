**HTC StarTrek**，原廠型號**HTC S410，HTC
S411**，是台灣[宏達電公司所推出的](../Page/宏達電.md "wikilink")[智慧型手機](../Page/智慧型手機.md "wikilink")，全球首只超薄折疊Windows
Mobile 5智慧型手機，首次搭載Push Mail功能。2006年5月於亞洲首度發表，2006年12月推出升級版。客製版本HTC
S410，HTC S411，Qtek 8500，Dopod S300，Dopod S301，Dopod 710，Dopod
710+，Orange SPV F600，i-mate SmartFlip，Cingular 3125。

## 技術規格

  - [處理器](../Page/處理器.md "wikilink")：TI OMAP850 195MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 5.0 SmartPhone
    Edition
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：64MB，RAM：64MB
  - 尺寸：98.5 毫米 X 51.4 毫米 X 15.8 毫米
  - 重量：99g（含電池）
  - 功能按鍵：音樂快捷鍵、照相鍵、語音控制鍵、音量調整鍵
  - [主螢幕](../Page/主螢幕.md "wikilink")：QVGA 解析度、2.2 吋 TFT-LCD 螢幕
  - [外螢幕](../Page/外螢幕.md "wikilink")：QQVGA 解析度、1.2 吋 TFT-LCD 螢幕
  - [網路](../Page/網路.md "wikilink")：GSM/GPRS/EDGE
  - [藍牙](../Page/藍牙.md "wikilink")：Bluetooth 1.2
  - [相機](../Page/相機.md "wikilink")：130萬畫素相機，支援自動對焦功能
  - [電池](../Page/電池.md "wikilink")：750mAh充電式鋰或鋰聚合物電池

## 升級版技術規格

  - [處理器](../Page/處理器.md "wikilink")：TI OMAP850 195MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 5.0 SmartPhone
    Edition
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：128MB，RAM：64MB
  - 尺寸：98.5mm X 51.4mm X 15.8mm
  - 重量：99g（含電池）
  - 功能按鍵：音樂快捷鍵、照相鍵、語音控制鍵、音量調整鍵
  - [主螢幕](../Page/主螢幕.md "wikilink")：QVGA 解析度、2.2 吋 TFT-LCD 平面式觸控感應螢幕
  - [外螢幕](../Page/外螢幕.md "wikilink")：QQVGA 解析度、1.2 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：GSM/GPRS/EDGE
  - [藍牙](../Page/藍牙.md "wikilink")：Bluetooth 1.2
  - [相機](../Page/相機.md "wikilink")：130萬畫素相機，支援自動對焦功能
  - [電池](../Page/電池.md "wikilink")：1100mAh充電式鋰或鋰聚合物電池

## 參見

  - [HTC](../Page/HTC.md "wikilink")
  - [Qtek](../Page/Qtek.md "wikilink")
  - [Dopod](../Page/Dopod.md "wikilink")
  - [TouchFLO](../Page/TouchFLO.md "wikilink")

## 外部連結

  - [HTC S411 概觀](http://www.htc.com/la/product.aspx?id=18930)
  - [HTC S411 技術規格](http://www.htc.com/la/product.aspx?id=18934)

[H](../Category/智能手機.md "wikilink")
[Startrek](../Category/宏達電手機.md "wikilink")