**HTC
Touch**，產品代號Elf、Elfin、Vogue、Fuwa，是台灣[宏達電公司所推出的](../Page/宏達電.md "wikilink")[智慧型手機](../Page/智慧型手機.md "wikilink")，搭載微軟
[Windows Mobile](../Page/Windows_Mobile.md "wikilink")
6，俗稱阿福機，該手機作為以hTC品牌進軍歐洲的首發產品之一，外形時尚，首次內置了革命性[HTC
TouchFLO介面](../Page/HTC_TouchFLO.md "wikilink")，使用者只要在觸控螢幕上利用手指滑動，手機就可以根據使用者的動作產生相對的反應，如捲動捲軸、開啟選單等，單手即可操作，但因使用单点触控荧幕，该手机無法同時用兩根手指在螢幕上控制，做出如iPhone放大縮小圖片般的效果。2007年6月5日於歐洲首度發表，其後又推出CDMA（Vogue）與TD-SCDMA（Fuwa）版本。

已知客製版本：

Elf：HTC P3450，HTC Touch，HTC Ted Baker Needle，Orange＆HTC Touch
P3450，Dopod S1，T-Mobile MDA Touch，O2 Xda Nova，Vodafone VPA Touch

Elfin：HTC P3452，HTC Touch（Enhanced Version），HTC Touch Color，Rogers＆HTC
Touch，Dopod S1（Enhanced Version），T-Mobile MDA Touch 256

Vogue（CDMA）：HTC P3050，HTC Touch CDMA，Alltel＆HTC Touch，Bell＆HTC
Touch，Sprint＆HTC Touch，Telus＆HTC Touch，Dopod S500，UTStarcom
MP6900，Verizon Touch XV6900，Okta Touch

Fuwa（TD-SCDMA）：Dopod S700

## Elf、Elfin技術規格

  - [處理器](../Page/處理器.md "wikilink")：TI OMAP850 201MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 6.0 Professional
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：128MB，RAM：64MB
  - 尺寸：99.9mm X 58mm X 13.9mm
  - 重量：112g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：QVGA 解析度、2.8 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：GSM/EDGE
  - [藍牙](../Page/藍牙.md "wikilink")：2.0 with EDR
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")：IEEE 802.11 b/g
  - [相機](../Page/相機.md "wikilink")：200萬畫素相機，支援自動對焦功能
  - [電池](../Page/電池.md "wikilink")：1100mAh充電式鋰或鋰聚合物電池

## Vogue技術規格

  - [處理器](../Page/處理器.md "wikilink")：Qualcomm MSM7500 400MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 6.0 Professional
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：256MB，RAM：128MB
  - 尺寸：101mm X 59.6mm X 14.1mm
  - 重量：112g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：QVGA 解析度、2.8 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：CDMA 2000 1x EV-DO
  - [藍牙](../Page/藍牙.md "wikilink")：2.0 with EDR
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")：IEEE 802.11 b/g
  - [相機](../Page/相機.md "wikilink")：200萬畫素相機，支援自動對焦功能
  - [電池](../Page/電池.md "wikilink")：1100mAh充電式鋰或鋰聚合物電池

## Fuwa技術規格

  - [處理器](../Page/處理器.md "wikilink")：Samsung SC32442B 500MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 6.0 Professional
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：256MB，RAM：128MB
  - 尺寸：106mm X 65mm X 22mm
  - 重量：112g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：QVGA 解析度、2.8 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：GSM/EDGE/TD-SCDMA
  - [GPS](../Page/GPS.md "wikilink")：配備GPS及A-GPS
  - [藍牙](../Page/藍牙.md "wikilink")：2.0 with EDR
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")：IEEE 802.11 b/g
  - [相機](../Page/相機.md "wikilink")：200萬畫素相機，支援自動對焦功能
  - [電池](../Page/電池.md "wikilink")：1100mAh充電式鋰或鋰聚合物電池

## 參見

  - [HTC](../Page/HTC.md "wikilink")
  - [Qtek](../Page/Qtek.md "wikilink")
  - [Dopod](../Page/Dopod.md "wikilink")
  - [TouchFLO](../Page/TouchFLO.md "wikilink")

## 外部連結

  - [HTC
    Touch](https://web.archive.org/web/20080623202900/http://www.htctouch.com/)
  - [HTC Touch
    概觀](https://web.archive.org/web/20080721073709/http://www.htc.com/www/product.aspx?id=356)
  - [HTC Touch 技術規格](http://www.htc.com/www/product.aspx?id=362)

[H](../Category/智能手機.md "wikilink") [H](../Category/觸控手機.md "wikilink")
[Touch](../Category/宏達電手機.md "wikilink")
[Category:2007年面世的手機](../Category/2007年面世的手機.md "wikilink")