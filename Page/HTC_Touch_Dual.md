**HTC Touch
Dual**，產品代號Niki、Neon，是台灣[宏達電公司所推出的](../Page/宏達電.md "wikilink")[智慧型手機](../Page/智慧型手機.md "wikilink")，搭載微軟
[Windows Mobile](../Page/Windows_Mobile.md "wikilink") 6，[HTC
Touch的滑蓋式鍵盤版本](../Page/HTC_Touch.md "wikilink")，分為16鍵與20鍵版，一樣具備[TouchFLO介面](../Page/TouchFLO.md "wikilink")，單手即可操作，2007年10月於歐洲首度發表。2008年7月1日，HTC與韓國SK電訊共同推出此機的白色款，HTC正式進軍韓國。

已知客製版本：

Niki：HTC P5500，HTC P5520，HTC Touch Dual，Orange＆HTC Touch Dual，SFR＆HTC
Touch Dual，Swisscom＆HTC Touch Dual，Vodafone＆HTC Touch Dual，Dopod
S600，T-Mobile MDA Touch Plus，O2 Xda Star

Neon：HTC P5510，HTC P5530，HTC Touch Dual，HTC Touch Dual 850，NTT DoCoMo
FOMA HT1100

## 技術規格

HTC Touch
Dual目前共有九种类型NIKI100，NIKI200，NIKI300，NIKI400，NIKI500，NEON100，NEON200，NEON300，NEON400。區別在於外觀，按鍵數，處理器（支持的網路），有無Wi-Fi，電池容量。下面僅列出兩種版本的規格。

### NIKI100

  - [處理器](../Page/處理器.md "wikilink")：Qualcomm MSM7200 400 MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 6.0 Professional
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：256MB，RAM：128MB
  - 尺寸：107mm X 55mm X 15.8mm（16键）
  - 重量：120g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：[QVGA](../Page/QVGA.md "wikilink")
    解析度、2.6 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：[HSDPA](../Page/HSDPA.md "wikilink")/[UMTS](../Page/UMTS.md "wikilink")：2100
    MHz，[GSM](../Page/GSM.md "wikilink")/[GPRS](../Page/GPRS.md "wikilink")/[EDGE](../Page/EDGE.md "wikilink")[三頻](../Page/三頻.md "wikilink")：
    900、1800 及 1900 MHz
  - [藍牙](../Page/藍牙.md "wikilink")：2.0 with EDR
  - [相機](../Page/相機.md "wikilink")：主相機：兩百萬畫素 CMOS 相機，副相機：CIF CMOS 鏡頭
  - [電池](../Page/電池.md "wikilink")：1120mAh充電式鋰或鋰聚合物電池（NIKI160）

### NEON100

  - [處理器](../Page/處理器.md "wikilink")：Qualcomm MSM7200 400 MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 6.0 Professional
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：256MB，RAM：128MB
  - 尺寸：107mm X 55mm X 18.2mm
  - 重量：128g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：QVGA 解析度、2.6 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：HSDPA/WCDMA
  - [GPS](../Page/GPS.md "wikilink")：配備GPS及[A-GPS](../Page/A-GPS.md "wikilink")
  - [藍牙](../Page/藍牙.md "wikilink")：2.0 with EDR
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")：IEEE 802.11 b/g
  - [相機](../Page/相機.md "wikilink")：200萬畫素相機，支援自動對焦功能
  - [電池](../Page/電池.md "wikilink")：1000mAh充電式鋰或鋰聚合物電池

## 參見

  - [HTC](../Page/HTC.md "wikilink")
  - [Qtek](../Page/Qtek.md "wikilink")
  - [Dopod](../Page/Dopod.md "wikilink")
  - [TouchFLO](../Page/TouchFLO.md "wikilink")

## 外部連結

  - [HTC Touch Dual 概觀](http://www.htc.com/tw/product.aspx?id=22796)
  - [HTC Touch Dual 技術規格](http://www.htc.com/tw/product.aspx?id=22800)

[H](../Category/智能手機.md "wikilink") [H](../Category/觸控手機.md "wikilink")
[Touch Dual](../Category/宏達電手機.md "wikilink")
[Category:2007年面世的手機](../Category/2007年面世的手機.md "wikilink")