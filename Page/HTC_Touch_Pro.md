**HTC Touch
Pro**，產品代號**Raphael**，是台灣[宏達電公司所推出的旗艦級](../Page/宏達電.md "wikilink")[智慧型手機](../Page/智慧型手機.md "wikilink")，搭載微軟
[Windows Mobile](../Page/Windows_Mobile.md "wikilink")
6.1，是一種具有3D立體觸控介面（[TouchFLO](../Page/TouchFLO.md "wikilink")）的手機。2008年6月4日於英國倫敦首度發表，8月12日於英國正式發售，目前是宏達電Touch家族旗下的最高端型號。

HTC Touch
Pro具有鑽石切割風格的外觀（亦設計有類似[P3702的圓潤外形](../Page/HTC_Touch_Diamond.md "wikilink")，代號[Herman](../Page/HTC_Herman.md "wikilink")），不同之處在於HTC
Touch Pro配備了QWERTY鍵盤，RAM增至288MB，沒有内建快閃記憶體。

## 技術規格

HTC Touch
Pro目前共有十二種類型RAPH100，RAPH110，RAPH200，RAPH210，RAPH300，RAPH400，RAPH500，RAPH600，RAPH700，RAPH800。區別在於外觀，處理器（支持的網路），有無Wi-Fi，電池容量。下面僅列出兩種版本的規格。

### RAPH100

  - [處理器](../Page/處理器.md "wikilink")：Qualcomm MSM7201A 528MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 6.1 Professional
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：512MB，RAM：288MB
  - 尺寸：102mm X 51mm X 18.05mm
  - 重量：165g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：VGA 解析度、2.8 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：GSM/WCDMA/HSDPA/HSUPA
  - [藍牙](../Page/藍牙.md "wikilink")：2.0 with EDR
  - [GPS](../Page/GPS.md "wikilink")：配備GPS及A-GPS
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")：IEEE 802.11 b/g
  - [電池](../Page/電池.md "wikilink")：1340mAh充電式鋰或鋰聚合物電池

### RAPH800

  - [處理器](../Page/處理器.md "wikilink")：Qualcomm MSM7501A 528MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 6.1 Professional
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：512MB，RAM：288MB
  - 尺寸：102mm X 51mm X 19.1mm
  - 重量：165g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：VGA 解析度、2.8 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：CDMA 2000 1x RTT/CDMA 2000 1x EV-DO
  - [藍牙](../Page/藍牙.md "wikilink")：2.0 with EDR
  - [GPS](../Page/GPS.md "wikilink")：配備GPS及A-GPS
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")：IEEE 802.11 b/g
  - [電池](../Page/電池.md "wikilink")：1340mAh充電式鋰或鋰聚合物電池

## 參見

  - [HTC](../Page/HTC.md "wikilink")
  - [Qtek](../Page/Qtek.md "wikilink")
  - [Dopod](../Page/Dopod.md "wikilink")
  - [TouchFLO](../Page/TouchFLO.md "wikilink")

## 外部連結

  - [HTC Touch Pro
    概觀](https://web.archive.org/web/20110716061123/http://www.htc.com/www/product.aspx?id=49518)
  - [HTC Touch Pro
    技術規格](https://web.archive.org/web/20081026100833/http://www.htc.com/www/product.aspx?id=49524)

[H](../Category/智能手機.md "wikilink") [H](../Category/觸控手機.md "wikilink")
[Touch Pro](../Category/宏達電手機.md "wikilink")