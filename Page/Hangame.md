**Hangame**（、），為[韓國的](../Page/韓國.md "wikilink")[NHN股份公司於](../Page/NHN.md "wikilink")1998年在韓國開始營運、2000年透過NHN
Japan開始在[日本營運的](../Page/日本.md "wikilink")[網絡遊戲](../Page/網絡遊戲.md "wikilink")[社群網站](../Page/社區.md "wikilink")。Hangame的**han**（、）在[韓語中為](../Page/韓語.md "wikilink")「一」的意思，而Hangame本身就是「真的只有一個遊戲？」的意思。目前在韓國的會員有約2500萬人、日本的有約1700萬人，因此也確立為世界上最大型的網絡遊戲社群網站。

## 概要

從經典的[撲克遊戲與](../Page/撲克.md "wikilink")[桌球](../Page/桌球.md "wikilink")、[日本將棋](../Page/日本將棋.md "wikilink")、[圍棋等](../Page/圍棋.md "wikilink")，到（從兩張圖片中）[找不同](../Page/找不同.md "wikilink")、[動作遊戲](../Page/動作遊戲.md "wikilink")、[電腦角色扮演遊戲等都有提供](../Page/電子角色扮演遊戲.md "wikilink")，而且大多數是免費的（除了一部分遊戲之外，其他遊戲都需要會員登錄）。另外，也有提供如[奇跡及](../Page/奇跡_\(遊戲\).md "wikilink")[天上碑等收費遊戲](../Page/天上碑.md "wikilink")，而每個收費遊戲都有它們各自的官方網站，所以用戶的管理也有分別。還有，在官方網站中已經要完全收費但在Hangame裡卻仍在免費階段的遊戲也有不少。另外除了遊戲之外，還有[頭像與](../Page/頭像.md "wikilink")[小組](../Page/小組.md "wikilink")（、Circle）。展示角色的大頭部的設計是頭像的特徵。

另外，[小組與Mini](../Page/小組.md "wikilink")
Mail、以至網站中到處都有提供的[BBS](../Page/BBS.md "wikilink")、[聊天室等功能](../Page/聊天室.md "wikilink")，令到Hangame已不只是一個遊戲網站了，而且以一個社群網站來說，其質素也很高。

很多知名的[ISP與](../Page/互聯網服務提供商.md "wikilink")[門戶網站都與此網站合作](../Page/門戶網站.md "wikilink")。

如果用戶不是使用[微軟的](../Page/微軟.md "wikilink")[Windows](../Page/Windows.md "wikilink")[作業系統](../Page/作業系統.md "wikilink")，以及同公司出品的[Internet
Explorer的話](../Page/Internet_Explorer.md "wikilink")，該用戶便不能使用Hangame上的功能。這是因為其遊戲等功能使用了[ActiveX](../Page/ActiveX.md "wikilink")。如果使用其他諸如[Netscape
Netvigator等的瀏覽器](../Page/網景領航員.md "wikilink")[存取網站的話](../Page/存取.md "wikilink")，那就不能使用套用到字句上的效果，同時也會顯示一個穿着企鵝服裝的頭像。

對於日文版Hangame來說，截至2005年5月31日為止，用戶[ID可通用於多種服務](../Page/ID.md "wikilink")（同公司營運的[NAVER也包括在內](../Page/NAVER.md "wikilink")），使用者能於[知識plus](../Page/知識plus.md "wikilink")、NAVER博客等服務中使用同一個ID。

然而，其後從2005年6月1日起，[知識plus被轉移成為Hangame旗下的服務](../Page/知識plus.md "wikilink")，NAVER博客則更改服務名稱為[CURURU並需要用戶建立獨立的ID](../Page/CURURU.md "wikilink")，而NAVER所提供的服務都停止了。

在2005年7月21日，同時在網站中遊玩的人數超過了10萬人，同年11月12日更達到15萬人。

現在，用戶除了能透過個人電腦上網站玩之外，一部分遊戲更能透過手提電話遊玩。

## 廣告

### 日本

Hangame從2005年開始推出廣告。

  - 2005年度
      - 由[長瀨智也演出](../Page/長瀨智也.md "wikilink")。

<!-- end list -->

  - 2006年度
      - 由[塚本高史演出](../Page/塚本高史.md "wikilink")。

## 外部連結

  - [韓國版Hangame](http://www.hangame.com/)

  - [日本版Hangame](https://web.archive.org/web/20060822143213/http://www.hangame.co.jp/index.html)

  - [K-Tai Hangame](http://k-tai.hangame.co.jp/index.php)（手提電話用）

  - [NHN](http://www.nhncorp.com/)

  - [NHN Japan](http://www.nhncorp.jp/)

[Category:網路遊戲運營商](../Category/網路遊戲運營商.md "wikilink")
[Category:網站](../Category/網站.md "wikilink")
[Category:Naver](../Category/Naver.md "wikilink")