**Happy\!**（），又译**網壇小魔女**，是[浦澤直樹畫的](../Page/浦澤直樹.md "wikilink")[網球漫畫](../Page/體育漫畫#網球漫畫.md "wikilink")。

## 概要

  - 1994年至1999年在[Big Comic
    Spirits連載](../Page/Big_Comic_Spirits.md "wikilink")。
  - 2006年4月7日於[TBS播放電視劇](../Page/東京廣播公司.md "wikilink")。2006年12月26日播放第二輯「Happy！2」。

## 故事簡介

高中3年級的少女海野幸在父母双亡之后独自照顾年幼的弟弟和妹妹，还要背负失业失败人间蒸发的哥哥借下的2億5000萬日元债款。她下定决心退学进军职业[网球界](../Page/网球.md "wikilink")。

## 主要角色

### 海野家人

  - 海野 幸（うみの みゆき）。主人公。天才网球选手。为了偿还债务进军职业网球界，终于获得世界冠军。
  - 海野 家康（うみの　いえやす）。小幸的哥哥。投机失败欠下2億5000萬日元债款后失踪。
  - 海野 舵樹（うみの　かじき）。海野家次男。
  - 海野 沙代里（うみの　さより）。海野家次女。
  - 海野 三悟（うみの さんご）。海野家幺弟。
  - 海野 洋平太（うみの ようへいた）。小幸已故的父亲。也是网球选手。

### 鳳财阀

  - 鳳 圭一郎（おおとり
    けいいちろう）。鳳财阀的继承人，温文尔雅的白马王子，与小幸两情相悦。在小幸的感染下决心摆脱母亲的控制独立奋斗，也成为了职业网球选手。
  - 鳳鳴子（おおとり うたこ）。鳳财阀的会长。曾经称霸日本女子网球界，单恋过海野洋平太。为了与對手龙之崎俱乐部對抗，不计前嫌培養小幸。
  - 賀来 菊子（かく きくこ）。鳳俱乐部会员。小幸的好友。

### 龙之崎财阀

  - 龙之崎蝶子（りゅうがさき ちょうこ）。实力与外貌兼具的女子网球选手。外表可爱，实际喜欢耍弄阴谋。认为圭一郎是合适的对象而对小幸百般打压。
  - 龙之崎花江（りゅうがさき はなえ）。龙之崎财阀的会长。鳳鳴子的对头。蝶子之母。

### 其他

  - 樱田纯二（さくらだ じゅんじ）。讨债公司成员，负责向小幸收取债款，但后来被小幸打动。
  - 鰐淵 京平（わにぶち　きょうへい）。小幸的债主。

## 电视剧集

### 主要演員

  - 海野 幸 - [相武紗季](../Page/相武紗季.md "wikilink")
  - 桜田 純二 - [宮迫博之](../Page/宮迫博之.md "wikilink")
  - 鳳 圭一郎 -
    [田口淳之介](../Page/田口淳之介.md "wikilink")（[KAT-TUN](../Page/KAT-TUN.md "wikilink")）
  - 竜ヶ崎 蝶子 - [小林麻央](../Page/小林麻央.md "wikilink")
  - 賀来 菊子 - [夏川純](../Page/夏川純.md "wikilink")
  - 海野 家康 - [荒川良良](../Page/荒川良良.md "wikilink")
  - 桂木 - [沼田爆](../Page/沼田爆.md "wikilink")
  - 山口 百太郎 - [森下能幸](../Page/森下能幸.md "wikilink")
  - 竜ヶ崎 花江 - [渡辺えり子](../Page/渡辺えり子.md "wikilink")
  - 鰐淵 京平 - [哀川翔](../Page/哀川翔.md "wikilink")
  - 空き地の男 - [濱田雅功](../Page/濱田雅功.md "wikilink")（特別出演）
  - サンダー牛山 - [笑福亭鶴瓶](../Page/笑福亭鶴瓶.md "wikilink")（特別出演）
  - 鳳 唄子 - [片平なぎさ](../Page/片平なぎさ.md "wikilink")
  - 海野 洋平太 - [村田雄浩](../Page/村田雄浩.md "wikilink")（友情出演））
  - 海野 舵樹 - [岩沼佑亮](../Page/岩沼佑亮.md "wikilink")
  - 海野 沙代里 - [奈良瞳](../Page/奈良瞳.md "wikilink")
  - 海野 三悟 - [中野目崇真](../Page/中野目崇真.md "wikilink")
  - 辯天橋 雛 - [青木裕子](../Page/青木裕子.md "wikilink")（TBS電視廣播員）

### 工作人員

  - 原作：[浦澤直樹](../Page/浦澤直樹.md "wikilink")（小学館「ビッグスピリッツコミックス」刊）
  - 脚本：[土田英生](../Page/土田英生.md "wikilink")
  - 原案協力：星野博規、由田和人、宮下雅之（小学館「週刊ビッグコミックスピリッツ」編集部）
  - 監修：[長崎尚志](../Page/長崎尚志.md "wikilink")（スタジオ·ビー）
  - 製作人：[伊與田英德](../Page/伊與田英德.md "wikilink")、壁谷悌之
  - 演出：[川嶋龍太郎](../Page/川嶋龍太郎.md "wikilink")
  - CG監督：[曽利文彦](../Page/曽利文彦.md "wikilink")
  - 制作：[TBS電視](../Page/TBS.md "wikilink")
  - 製作著作：[TBS](../Page/TBS.md "wikilink")

### 片尾曲

  - 「Real Face\#1」（作詞：，作曲：松本孝弘、編曲：松本孝弘、德永曉人，Rap詞：JOKER，歌：KAT-TUN）

## 外部連結

  - [電視劇「Happy！」官方網頁](http://www.tbs.co.jp/happy2006/happy/index-j.html)
  - [電視劇「Happy！2」官方網頁](http://www.tbs.co.jp/happy2006/)

[Category:浦澤直樹](../Category/浦澤直樹.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:網球漫畫](../Category/網球漫畫.md "wikilink")
[Category:TBS特別劇集](../Category/TBS特別劇集.md "wikilink")
[Category:2006年日本電視劇集](../Category/2006年日本電視劇集.md "wikilink")
[Category:2006年電視特別劇集](../Category/2006年電視特別劇集.md "wikilink")
[Category:日本漫畫改編日本電視劇](../Category/日本漫畫改編日本電視劇.md "wikilink")
[Category:與運動有關的電視劇](../Category/與運動有關的電視劇.md "wikilink")