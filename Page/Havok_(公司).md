**Havok**是开发Havok物理引擎的公司的名字。这个公司于1998年成立，创始人为休·雷诺德（Hugh
Reynolds）和斯蒂芬·科林斯博士（Dr. Steven
Collins），他们两个人都是[都柏林](../Page/都柏林.md "wikilink")[三一学院大学计算机科学部门的教师](../Page/三一学院.md "wikilink")。

Havok與多間公司或者電子遊戲商合作，Havok物理引擎可以應用在[PlayStation
2](../Page/PlayStation_2.md "wikilink")、[PlayStation
3](../Page/PlayStation_3.md "wikilink")、[PlayStation
Portable](../Page/PlayStation_Portable.md "wikilink")、[Xbox](../Page/Xbox.md "wikilink")、[Xbox
360](../Page/Xbox_360.md "wikilink")、[Wii和](../Page/Wii.md "wikilink")[GameCube中](../Page/GameCube.md "wikilink")。另外，有超過150款電子遊戲使用了該引擎，比較著明的有[半条命2](../Page/半条命2.md "wikilink")、[Halo
2和](../Page/Halo_2.md "wikilink")[上古卷轴IV：湮没](../Page/上古卷轴IV：湮没.md "wikilink")。有电影亦採用了Havok的物理的特效，例如[海神号](../Page/海神号.md "wikilink")、[黑客帝国和](../Page/黑客帝国.md "wikilink")[查理与巧克力工厂](../Page/查理与巧克力工厂.md "wikilink")。

2007年9月14日，英特尔宣佈收購Havok公司，Havok會成為前者的全资子公司。\[1\]由於[英特尔收購了Havok](../Page/英特尔.md "wikilink")，前者顯然希望物理計算完全由CPU負責，所以由顯示卡加速Havok
FX的開發似乎已經被取消。此後，[NVIDIA亦收購了Havok的對手](../Page/NVIDIA.md "wikilink")－[AGEIA](../Page/AGEIA.md "wikilink")。

除了物理引擎外，Havok在GDC2009上展示了AI
SDK，用來協助開發遊戲中的[人工智能](../Page/人工智能.md "wikilink")\[2\]。

## 參考

<div class="references-small">

<references/>

</div>

## 外部链接

  -
[Category:1998年開業電子遊戲公司](../Category/1998年開業電子遊戲公司.md "wikilink")
[Category:爱尔兰公司](../Category/爱尔兰公司.md "wikilink")
[Category:英國電子遊戲公司](../Category/英國電子遊戲公司.md "wikilink")
[Category:電子遊戲開發公司](../Category/電子遊戲開發公司.md "wikilink")
[Category:英特爾](../Category/英特爾.md "wikilink")
[Category:物理引擎](../Category/物理引擎.md "wikilink")

1.  [英特尔收购世界领先的交互软件与服务供应商
    Havok](http://www.intel.com/cd/corporate/pressroom/apac/zho/date/2007/370702.htm)
2.  [Havok研发AI SDK](http://news.mydrivers.com/1/130/130709.htm)