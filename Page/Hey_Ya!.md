**Hey Ya\!**，是[André 3000撰寫的一首歌](../Page/André_3000.md "wikilink")，收錄於的
2003年[專輯](../Page/專輯.md "wikilink")「The Love
Below」，這首歌仿效[披頭四於](../Page/披頭四.md "wikilink")1964年的[單曲](../Page/單曲.md "wikilink")「Hey
Ya\!」。Hey Ya\!拿下「最佳都會／另類演出」等3項大獎，轟動搖滾樂界，另外 Hey Ya\!的 MV
也獲得MTV音樂獎「年度最佳音樂錄影帶」、「最佳Hip-Hop錄影帶」、「最佳特效」以及「最佳藝術指導」等4項獎項。

[Category:2003年音樂專輯](../Category/2003年音樂專輯.md "wikilink")