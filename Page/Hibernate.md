**Hibernate**是一种[Java语言下的](../Page/Java.md "wikilink")[对象关系映射解决方案](../Page/对象关系映射.md "wikilink")。它是使用[GNU宽通用公共许可证发行的](../Page/GNU宽通用公共许可证.md "wikilink")[自由](../Page/自由软件.md "wikilink")、[开源的软件](../Page/开源软件.md "wikilink")。它为[面向对象的](../Page/面向对象.md "wikilink")[领域模型到传统的](../Page/领域模型.md "wikilink")[关系型数据库的映射](../Page/关系数据库.md "wikilink")，提供了一个使用方便的框架。

## 概览

它的设计目标是将软件开发人员从大量相同的[数据持久层相关编程工作中解放出来](../Page/数据持久层.md "wikilink")。无论是从设计草案还是从一个[遗留数据库开始](../Page/遗留系统.md "wikilink")，开发人员都可以采用Hibernate。

Hibernate不仅负责从Java[类到数据库表的映射](../Page/类_\(计算机科学\).md "wikilink")（还包括从Java数据类型到SQL数据类型的映射），还提供了面向对象的数据查询检索机制，从而极大地缩短了手动处理[SQL和](../Page/SQL.md "wikilink")[JDBC上的开发时间](../Page/JDBC.md "wikilink")。

## 发展历程

2001年，澳大利亚墨尔本一位名为Gavin
King的27岁的程序员，上街买了一本SQL编程的书，他厌倦了实体bean，认为自己可以开发出一个符合对象关系映射理论，并且真正好用的Java持久化层框架，因此他需要先学习一下SQL。这一年的11月，Hibernate的第一个版本发布了。

2002年，已经有人开始关注和使用Hibernate了。

2003年9月，Hibernate开发团队进入JBoss公司，开始全职开发Hibernate，从这个时候开始Hibernate得到了突飞猛进的普及和发展。

2004年，整个Java社区开始从实体bean向Hibernate转移，特别是在Rod Johnson的著作《Expert One-on-One
J2EE Development without
EJB》出版后，由于这本书以扎实的理论、充分的论据和详实的论述否定了EJB，提出了轻量级敏捷开发理念之后，以Hibernate和Spring为代表的轻量级开源框架开始成为Java世界的主流和事实标准。在2004年Sun领导的J2EE5.0标准制定当中的持久化框架标准正式以Hibernate为蓝本。

2006年，J2EE5.0标准正式发布以后，持久化框架标准Java Persistent
API（简称JPA）基本上是参考Hibernate实现的，而Hibernate在3.2版本开始，已经完全兼容JPA标准。

## 编程开发

### 编程环境

Hibernate是一个以LGPL（Lesser GNU Public
License）许可证形式发布的开源项目。在Hibernate官网上有下载Hibernate包的说明。Hibernate包以源代码或者二进制的形式提供。

### 编程工具

[Eclipse](../Page/Eclipse.md "wikilink")：一个开放源代码的、基于Java的可扩展开发平台。

[NetBeans](../Page/NetBeans.md "wikilink")：开放源码的Java集成开发环境，适用于各种客户机和Web应用。

[IntelliJ
IDEA](../Page/IntelliJ_IDEA.md "wikilink")：在代码自动提示、代码分析等方面的具有很好的功能。

MyEclipse：由Genuitec公司开发的一款商业化软件，是应用比较广泛的Java应用程序集成开发环境。

## .NET上的Hibernate

Hibernate有個在.NET Framework上的實作版本，稱為NHibernate，在[ADO.NET Entity
Framework發表之前](../Page/ADO.NET_Entity_Framework.md "wikilink")，NHibernate是在.NET
Framework上經常被使用的ORM實作。

## 参考文献

## 外部連結

  - [hibernate官方网站](http://www.hibernate.org/)
  - <http://www.open-open.com/open2618.htm>
  - <http://dev.yesky.com/312/2549312.shtml>

{{-}}

[Category:Java](../Category/Java.md "wikilink")
[Category:红帽软件](../Category/红帽软件.md "wikilink")