[Hit_Fm_Logo.jpg](https://zh.wikipedia.org/wiki/File:Hit_Fm_Logo.jpg "fig:Hit_Fm_Logo.jpg")

**臺北之音廣播股份有限公司**（簡稱：**臺北之音hit FM聯播網**、**hit FM**、**hit
FM聯播網**，前名**臺北之音音樂聯播網**）是以[臺灣](../Page/臺灣.md "wikilink")[臺北市為基地的](../Page/臺北市.md "wikilink")[廣播聯播網](../Page/廣播聯播網.md "wikilink")，目前是「環球七福廣告」旗下的媒體，開播於1998年9月17日，全天候播放以[華語流行音樂為主的](../Page/華語流行音樂.md "wikilink")[音樂](../Page/音樂.md "wikilink")[歌曲與](../Page/歌曲.md "wikilink")[廣播節目](../Page/廣播節目.md "wikilink")。[台呼為](../Page/Jingle.md "wikilink")“只想聽音樂”。

## 簡歷

該電台原名稱是「[台北之音音樂網](../Page/台北之音.md "wikilink")」，申請時是屬於[小功率電台](../Page/小功率.md "wikilink")，原只限於[大台北地區播音](../Page/大台北地區.md "wikilink")（[調頻](../Page/調頻.md "wikilink")91.7[兆赫](../Page/兆赫.md "wikilink")，原定頻FM90.9，後為了遵照[交通部電信總局於](../Page/交通部電信總局.md "wikilink")1996年12月24日所頒佈的「小功率電台頻率調案方案」的規定，於1998年1月10日移頻至FM91.7，當時電台正式名稱「**台北女性生活廣播電台**」）。1998年8月12日起，與中台灣廣播電台、高屏廣播電台結盟（其中中台灣廣播電台原為[地下電台](../Page/地下電台.md "wikilink")，於1994年向[行政院新聞局提出申設小功率社區電台獲准](../Page/行政院新聞局.md "wikilink")，轉為合法電台，開播最初是屬於從事公益性活動的電台\[1\]），並採用聯播網的播出方式，同年9月17日正式開播。

Hit Fm
定位以播放[華語流行音樂為主](../Page/華語流行音樂.md "wikilink")，但除華語外尚有相當豐富的[東洋](../Page/東洋.md "wikilink")、[西洋音樂資訊](../Page/西洋.md "wikilink")，是一相當全方位的音樂電台。2005年，Hit
Fm
與其母公司[台北之音聯手設立](../Page/台北之音.md "wikilink")「**www.Hitoradio.com**」[網站](../Page/網站.md "wikilink")（2008年曾更换[域名为](../Page/域名.md "wikilink")**hitoradio.im.tv**，後改回**www.Hitoradio.com**），定位成為華語流行音樂的[入口網站](../Page/入口網站.md "wikilink")。在2005年年底，「台北女性生活廣播電台」更名為「台北流行音樂廣播電台」。

Hit Fm
除媒體主業之外，也積極舉辦許多文教與娛樂性活動，如每年四月的「[台客搖滾嘉年華](../Page/台客搖滾嘉年華.md "wikilink")」、五月的「[流行音樂獎頒獎典禮](../Page/HITO流行音樂獎頒獎典禮.md "wikilink")」、流行歌手發片的宣傳活動等。此外，每年12月份Hit
FM舉辦的「百首單曲票選」也是台內的年度重頭戲。其台呼更換過一次，但均由[順子演唱](../Page/順子.md "wikilink")。

## 歷史

1997年9月10日，Hit FM的前身「台北之音音樂網」開播，頻率為FM 90.9\[2\]。

1998年1月10日，台北之音音樂網移頻至FM 91.7。

1998年8月15日，「Hito中文排行榜」開播。

1998年9月17日，Hit FM聯播網開播。

2001年7月，Hit
FM的南部分台（高屏廣播電台）開始在平日中午12點播出2分鐘的地方新聞（2009年4月起改為12點5分、14點5分播出）。同年8月，台北流行音樂廣播（Hit
FM的總台）開始在平日中午12點32分播出2分鐘的國際新聞。

2006年10月，Hit
FM和[亞洲廣播網](../Page/亞洲廣播網.md "wikilink")、蘋果線上、後山電台共組台灣音樂大聯盟（現已結束）\[3\]。

2009年1月1日起，由於原電台經營者無意繼續電台事務，加上與台北流行音樂廣播的經營理念迥異，Hit Fm
結束與台北流行音樂廣播（現為[台北流行廣播電台](../Page/台北流行廣播電台.md "wikilink")）的合作關係，台北之音遂與Hit
FM聯播網合併成「台北之音Hit
Fm聯播網」，[大台北地區頻率移至調頻](../Page/大台北地區.md "wikilink")107.7兆赫（原「台北之音」），並於2009年2月底完成股權轉讓\[4\]\[5\]。因應經營型態的轉變，在同年3月26日，Hit
Fm
進行全面改版，改版內容於當日下午5點起上線。除企業識別標誌更改、第2代台呼上線、節目表及[DJ主持時段的大幅度更動之外](../Page/DJ.md "wikilink")，新聞時段也由以往的半點播報改為整點播報新聞，並執行開台以來首度裁撤凌晨部分時段的DJ主持。另外，由於新任接手者「七福傳播」為前[中廣音樂網](../Page/中廣音樂網.md "wikilink")（Wave
Radio，現更名為i Radio）的經營者，因此改版中也加入了原Wave
Radio的主持班底。2012年11月8日起，更首次在音樂電台內開闢[政論節目](../Page/政論節目.md "wikilink")《蔻蔻早餐》，希望藉此吸引新聽眾，但也引發電台台性質變的疑慮。

高屏廣播電台在[農曆新年期間會開放電話CALL](../Page/農曆新年.md "wikilink")-IN，電話為（07）333-2036。

高屏廣播電台與[高雄捷運共同合作](../Page/高雄捷運.md "wikilink")，結合搭乘提醒語、教育宣導、生活資訊、工商廣告及音樂，不同時段亦提供不同情境音樂與單元資訊，不定時在各車站播送。

2012年12月17日起，宜蘭中山廣播電台（FM 97.1）開始以「Hit FM聯播網 97.1」台呼試播，並於2013年元旦起加入Hit
FM聯播網\[6\]\[7\]。

2013年8月6日起，每週二到週六凌晨2點重播[魏如萱主持的](../Page/魏如萱.md "wikilink")《OH夜DJ》（只在[台北之音播出](../Page/台北之音.md "wikilink")），是Hit
FM聯播網首次於凌晨時段重播節目。同年10月，Hit FM聯播網和17所大學結盟，成立「Hito校園聯盟」。

2014年1月6日起，中台灣廣播電台、高屏廣播電台、中山廣播電台開始在週二到週六凌晨2點重播《OH夜DJ》\[8\]。

2014年5月11日，台北之音（台北總台）的總部從醒吾大樓10樓搬到1樓（目前10樓的舊總部由台北流行廣播電台使用）\[9\]。

2014年11月10日，因母公司「環球七福廣告」收購台北流行廣播電台，當天起週一至週五的整點新聞新增2節（22:00、23:00），台北流行廣播電台的整點新聞也同步和Hit
FM聯播。

2014年12月29日起，中山廣播電台的地方新聞（平日12:05、14:05）改由台北之音的新聞主播播報（現場播出）。目前Hit
FM在[台北市](../Page/台北市.md "wikilink")、[新北市](../Page/新北市.md "wikilink")、台中市、[高雄市的部分](../Page/高雄市.md "wikilink")[全家便利商店](../Page/全家便利商店.md "wikilink")、[萊爾富](../Page/萊爾富.md "wikilink")、[OK超商店內](../Page/OK超商.md "wikilink")[公開播放](../Page/公開播放.md "wikilink")。

2015年7月，網站新域名「www.hitfm.taipei」、「www.hitoradio.taipei」啟用。

2016年5月1日起，花蓮東台灣廣播電台（FM 107.7）加入Hit FM聯播網\[10\]\[11\]。

2018年4月1日，[TOYOTA冠名贊助](../Page/和泰汽車.md "wikilink")《HIT週末！》，節目名稱改為《TOYOTA
HIT週末！》（只在週日冠名贊助）。

2018年5月18日，[土耳其航空冠名贊助](../Page/土耳其航空.md "wikilink")《HITO中文排行榜》（冠名至2018年8月10日）。

2019年3月29日，土耳其航空2度冠名贊助《HITO中文排行榜》。

目前東台灣廣播電台的所有非聯播時段節目皆無DJ主持（2017年1月1日起）。

## 收聽頻率及發射功率

<table>
<thead>
<tr class="header">
<th><p>地區</p></th>
<th><p>電台名稱</p></th>
<th><p>收聽頻率/發射功率</p></th>
<th><p>收聽地區</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>北部</strong></p></td>
<td><p><strong>台北之音廣播電台</strong></p></td>
<td><p>FM107.7㎒/ 3Kw（兆赫）</p></td>
<td><p><a href="../Page/臺北盆地.md" title="wikilink">臺北盆地</a>、<a href="../Page/基隆市.md" title="wikilink">基隆</a>、<a href="../Page/桃園市.md" title="wikilink">桃園市大部分地區及</a><a href="../Page/新竹.md" title="wikilink">新竹部份地區</a></p></td>
</tr>
<tr class="even">
<td><p><strong>中部</strong></p></td>
<td><p><strong>中台灣廣播電台</strong></p></td>
<td><p>FM 91.5㎒/ 2Kw（兆赫）</p></td>
<td><p><a href="../Page/台中市.md" title="wikilink">台中市</a>、<a href="../Page/彰化縣.md" title="wikilink">彰化縣</a>、<a href="../Page/南投縣.md" title="wikilink">南投縣</a>、<a href="../Page/雲林縣.md" title="wikilink">雲林縣大部分地區</a>、<a href="../Page/苗栗縣.md" title="wikilink">苗栗縣造橋以南地區及</a><a href="../Page/嘉義縣.md" title="wikilink">嘉義縣部份地區</a>，不包括日月潭、<a href="../Page/阿里山鄉.md" title="wikilink">阿里山</a>，頻率原為FM 89.7[12]</p></td>
</tr>
<tr class="odd">
<td><p><strong>南部</strong></p></td>
<td><p><strong>高屏廣播電台</strong></p></td>
<td><p>FM 90.1㎒ / 1Kw（兆赫）</p></td>
<td><p>涵蓋<a href="../Page/高雄市.md" title="wikilink">高雄市</a>、<a href="../Page/屏東縣.md" title="wikilink">屏東縣大部份地區</a>、<a href="../Page/台南市.md" title="wikilink">台南市部分地區</a>，<a href="../Page/湖內區.md" title="wikilink">湖內</a>、茄萣易受到濁水溪廣播電台干擾[13]</p></td>
</tr>
<tr class="even">
<td><p><strong>宜蘭</strong></p></td>
<td><p><strong>中山廣播電台</strong></p></td>
<td><p>FM 97.1㎒ / 1Kw（兆赫）</p></td>
<td><p><a href="../Page/宜蘭縣.md" title="wikilink">宜蘭縣及</a><a href="../Page/新北市.md" title="wikilink">新北市</a><a href="../Page/坪林區.md" title="wikilink">坪林區</a>、<a href="../Page/烏來區.md" title="wikilink">烏來區</a>、<a href="../Page/貢寮區.md" title="wikilink">貢寮區</a>、<a href="../Page/雙溪區.md" title="wikilink">雙溪區</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>花東</strong></p></td>
<td><p><strong>東台灣廣播電台</strong></p></td>
<td><p>FM 107.7㎒ / 2Kw（兆赫）</p>
<p>舞鶴同頻轉播站：0.2Kw（兆赫）[14]</p></td>
<td><p><a href="../Page/花蓮縣.md" title="wikilink">花蓮縣及</a><a href="../Page/台東縣.md" title="wikilink">台東縣</a><a href="../Page/池上鄉.md" title="wikilink">池上鄉</a>、<a href="../Page/關山鎮.md" title="wikilink">關山鎮</a>、<a href="../Page/海端鄉.md" title="wikilink">海端鄉</a>、<a href="../Page/鹿野鄉.md" title="wikilink">鹿野鄉</a>、<a href="../Page/成功鎮.md" title="wikilink">成功鎮部分地區</a>（含<a href="../Page/三仙台.md" title="wikilink">三仙台</a>）[15]、宜蘭縣<a href="../Page/南澳鄉.md" title="wikilink">南澳鄉</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

  - 目前其電波涵蓋範圍包括[北北基](../Page/北北基.md "wikilink")、[中彰投](../Page/中彰投.md "wikilink")、[高雄](../Page/高雄.md "wikilink")、[宜蘭縣和](../Page/宜蘭縣.md "wikilink")[花蓮縣](../Page/花蓮縣.md "wikilink")；除了台北之音、宜蘭中山廣播電台、花蓮東台灣廣播電台為中功率電台，其餘為小功率，涵蓋範圍仍相當有限。

<!-- end list -->

  - 澎湖西瀛之聲廣播電台曾在2004年（2010年11月改名為「澎湖灣廣播電台」；因營運虧損，2017年8月29日起停播）和台北之音聯播，西瀛之聲百分之六十的節目由台北之音提供，現已結束合作。

<!-- end list -->

  - 台北之音的發射站設置於醒吾大樓樓頂，由於地處[台北盆地內部](../Page/台北盆地.md "wikilink")，收訊範圍較其他兩者為小。中台灣廣播電台的發射站設置於[大肚臺地](../Page/大肚臺地.md "wikilink")，周邊縣市可清晰收到Hit
    FM的訊號，宜蘭中山電台的發射站也設置於電台大樓樓頂。另外2010年高屏電台更改台址之後，亦將發射台位置遷往樓層更高的大樓（81樓），使收訊範圍得以進一步擴大。

<!-- end list -->

  - Hit
    Fm以台北之音為主台，並製作聯播節目向其它分台播出，但各地方分台仍有不少自製節目，對於重要的地方資訊也都會有所報導，可說兼具全國性與地方性。

<!-- end list -->

  - 台北之音和東台灣廣播電台是目前全台唯二使用FM107.7 Mhz的廣播電台。

<!-- end list -->

  - 網路收聽
      - 音質為192
        Kbps（請用「新視窗」開啟連結）：[北部](http://hichannel.hinet.net/player/radio/index.jsp?radio_id=222)、[南部](http://hichannel.hinet.net/radio/index.do?id=90)、[宜蘭](http://hichannel.hinet.net/radio/index.do?id=301)
      - 音質為128
        Kbps（請用「新視窗」開啟連結）：[中部](http://hichannel.hinet.net/radio/index.do?id=88)、[花東](http://hichannel.hinet.net/radio/index.do?id=1568)

## DJ

### 北部DJ

|             **DJ**             |
| :----------------------------: |
|          周玉蔻（周玉蔻嗆新聞）           |
|           蔣卓嘉（翹班DJ ）           |
|         Bibi趙之璧（不累DJ）          |
| Bryan（耐玩DJ & TOYOTA HITO中文排行榜） |
|          Dennis（夜貓DJ）          |
|                                |

### 中部DJ

|     **DJ**      |
| :-------------: |
| Michelle（早安DJ）  |
| Erin（RELAX DJ）  |
| Alice（FRESH DJ） |
|                 |

### 南部DJ

|     **DJ**      |
| :-------------: |
| Tracy（HAPPY DJ） |
|   FIFI（元氣DJ）    |
|                 |

### 宜蘭DJ

|   **DJ**    |
| :---------: |
|  偉苓（翹班DJ）   |
| 宥鈞（LOVE DJ） |
|             |

### 前DJ

  - [黃韻玲](../Page/黃韻玲.md "wikilink")：曾任《Freezy DJ》主持人
  - [黃小楨](../Page/黃小楨.md "wikilink")
  - 何語蓉：曾任中台灣廣播電台《俏迷DJ》主持人， 曾參選台中市議員\[16\]。
  - 凱莉：現為[港都電台](../Page/好事聯播網.md "wikilink")《港都好心情》主持人

## 整點新聞時段

  - 整點新聞（5分鐘）：平日 08:00、10:00、16:00、20:00、22:00、23:00
  - 整點新聞（6分鐘）：平日 12:00、14:00、18:00
      - NEWS UPDATE：平日
        12:05、14:05、18:05（中台灣廣播電台因18:05\~19:00播出《馬路DJ》，18:05不播出）
  - 整點新聞（3分鐘）
      - 平日 09:00、11:00、13:00、19:00
      - 週末 09:00 - 17:00
  - 整點新聞（10分鐘）：平日07:00
  - 明日天氣概況：平日 18:04（預錄，[中央氣象局預報員播報](../Page/交通部中央氣象局.md "wikilink")）

## 主播

### 台北之音（總台）

#### 平日

  - 邵永輝（週一至週五07：00、08：00新聞主播，09：00代班主播）
  - 顏詩雲（週一至週五10：00新聞主播）
  - 高珮萱
    (週一到週五09：00\~11：00、12：00、14：00、18:00整點新聞、16：33娛樂新聞主播，宜蘭地方新聞（12:05、14:05）主播)
  - 林羽婕（週一至週五17:00整點新聞主播）
  - 李學叡（週一至週五22:00、23:00整點新聞主播）21：33娛樂新聞主播
  - 劉家燕（代班主播）

#### 假日

  - 張普熏（週六）
  - 劉家燕（週日）

### 高雄

  - 劉尹淳（兼任高屏廣播電台記者）
  - 周姮君（兼任高屏廣播電台記者）

## 獲獎紀錄

### 廣播金鐘獎

|- |2004年 |《校園青春錄》（中台灣廣播電台）
|[第39屆廣播金鐘獎少年節目獎](../Page/第39屆金鐘獎.md "wikilink")
| |- |2004年 |《校園青春錄》／洪-{韵}-婷（Sharon）\[17\]（中台灣廣播電台）
|[第39屆廣播金鐘獎少年節目主持人獎](../Page/第39屆金鐘獎.md "wikilink")
| |- |2006年 |《音樂五六七》（高屏廣播電台）
|[第41屆廣播金鐘獎流行音樂節目獎](../Page/第41屆金鐘獎.md "wikilink")
| |- |2007年 |《校園青春錄》／黃攸真(Gracie)（中台灣廣播電台） |第三屆廣播電視小金鐘獎\[18\]最佳少年節目主持人獎 |
|- |2013年 |《聖誕版臺呼—A-Lin+小宇版》
|[第48屆廣播金鐘獎電臺臺呼獎](../Page/第48屆金鐘獎.md "wikilink")
| |- |2015年 |《毛起來愛–流浪動物認養活動》
|[第50屆廣播金鐘獎電臺品牌行銷創新獎](../Page/第50屆金鐘獎.md "wikilink")
| |- |2015年 |《Hit FM聯播網官方APP》
|[第50屆廣播金鐘獎電臺數位應用獎](../Page/第50屆金鐘獎.md "wikilink")
| |- |2015年 |《生活在宜蘭》（宜蘭中山廣播電台）
|[第50屆廣播金鐘獎社區節目獎](../Page/第50屆金鐘獎.md "wikilink")
| |- |2016年 |《2015 Hit Fm年度百首單曲票選-我的年度一歌》
|[第51屆廣播金鐘獎電臺品牌行銷創新獎](../Page/第51屆金鐘獎.md "wikilink")
|

## 年度百首單曲

**Hit-Fm年度百首單曲**起於1998年，是Hit-Fm每年固有的傳統，由聽眾上網票選出該年度最Hito的前一百首歌曲，是個兼具意義與紀念性的活動。

  - [1998年 Hit Fm年度百首單曲](../Page/1998年_Hit_Fm年度百首單曲.md "wikilink")
  - [1999年 Hit Fm年度百首單曲](../Page/1999年_Hit_Fm年度百首單曲.md "wikilink")
  - [2000年 Hit Fm年度百首單曲](../Page/2000年_Hit_Fm年度百首單曲.md "wikilink")
  - [2001年 Hit Fm年度百首單曲](../Page/2001年_Hit_Fm年度百首單曲.md "wikilink")
  - [2002年 Hit Fm年度百首單曲](../Page/2002年_Hit_Fm年度百首單曲.md "wikilink")
  - [2003年 Hit Fm年度百首單曲](../Page/2003年_Hit_Fm年度百首單曲.md "wikilink")
  - [2004年 Hit Fm年度百首單曲](../Page/2004年_Hit_Fm年度百首單曲.md "wikilink")
  - [2005年 Hit Fm年度百首單曲](../Page/2005年_Hit_Fm年度百首單曲.md "wikilink")
  - [2006年 Hit Fm年度百首單曲](../Page/2006年_Hit_Fm年度百首單曲.md "wikilink")
  - [2007年 Hit Fm年度百首單曲](../Page/2007年_Hit_Fm年度百首單曲.md "wikilink")
  - [2008年 Hit Fm年度百首單曲](../Page/2008年_Hit_Fm年度百首單曲.md "wikilink")
  - [2009年 Hit Fm年度百首單曲](../Page/2009年_Hit_Fm年度百首單曲.md "wikilink")
  - [2010年 Hit Fm年度百首單曲](../Page/2010年_Hit_Fm年度百首單曲.md "wikilink")
  - [2011年 Hit Fm年度百首單曲](../Page/2011年_Hit_Fm年度百首單曲.md "wikilink")
  - [2012年 Hit Fm年度百首單曲](../Page/2012年_Hit_Fm年度百首單曲.md "wikilink")
  - [2013年 Hit Fm年度百首單曲](../Page/2013年_Hit_Fm年度百首單曲.md "wikilink")
  - [2014年 Hit Fm年度百首單曲](../Page/2014年_Hit_Fm年度百首單曲.md "wikilink")
  - [2015年 Hit Fm年度百首單曲](../Page/2015年_Hit_Fm年度百首單曲.md "wikilink")
  - [2016年 Hit Fm年度百首單曲](../Page/2016年_Hit_Fm年度百首單曲.md "wikilink")
  - [2017年 Hit Fm年度百首單曲](../Page/2017年_Hit_Fm年度百首單曲.md "wikilink")
  - [2018年 Hit Fm年度百首單曲](../Page/2018年_Hit_Fm年度百首單曲.md "wikilink")

## 參見

  - [臺灣廣播電台列表](../Page/臺灣廣播電台列表.md "wikilink")
  - [台北之音](../Page/台北之音.md "wikilink")
  - [聯播網](../Page/聯播網.md "wikilink")
  - [HITO流行音樂獎](../Page/HITO流行音樂獎.md "wikilink")
  - [全球流行音樂金榜](../Page/全球流行音樂金榜.md "wikilink")
  - [全球華語歌曲排行榜](../Page/全球華語歌曲排行榜.md "wikilink")

## 參考文獻

## 外部連結

  - [Hitoradio HitFM](http://www.hitoradio.com)

  -
  -
  -
  -
  -
  -
  -
  -
[Category:台灣廣播電台](../Category/台灣廣播電台.md "wikilink")
[Category:電台網](../Category/電台網.md "wikilink")
[Category:1998年開播的電台](../Category/1998年開播的電台.md "wikilink")
[Category:1998年台灣建立](../Category/1998年台灣建立.md "wikilink")

1.  [Hit FM聯播網
    台灣流行音樂指標](http://mol.mcu.edu.tw/showmore_search_2009.php?enid=91159)

2.  〈台北之音音樂台 明天開播 明星DJ打頭陣〉，《民生報》1997/09/09 第14版
3.  [台灣音樂大聯盟
    打造具公信力音樂排行榜](http://www.epochtimes.com/b5/6/10/2/n1474018.htm)
4.
5.
6.  [2013.1.1起宜蘭嘛開始Hito！](http://www.hitoradio.com/newweb/827hitpoint)
7.  [NCC第363次分組委員會議紀錄](http://www.ncc.gov.tw/chinese/files/12121/68_27194_121212_1.pdf)
8.  [01/06 OH夜 DJ實況文](http://archive.is/aqwOA)，2014/01/06
9.  [HITO
    -{佈}-告欄](https://www.facebook.com/hitfmhitoradio/posts/10152357908024194)，Hit
    FM聯播網 [Facebook粉絲專頁](../Page/Facebook.md "wikilink")
10. [20160501 Hit Fm 107.7
    前進東台灣](https://www.youtube.com/watch?v=s3A7wrRtT-c)
11. [NCC第536次分組委員會議紀錄](http://www.ncc.gov.tw/chinese/files/16042/68_35521_160428_1.pdf)
12. 1998年，**蔡念中**等合著，《大眾傳播概論》，ISBN 978-957-11-1673-0，第106頁
13. [Re: 台北之音Hit
    Fm聯播網](http://www.ptt.cc/bbs/radio/M.1356640617.A.EB7.html)，[PTT電台版](../Page/批踢踢.md "wikilink")（因「濁水溪廣播電台」和高屏廣播電台頻率都是FM
    90.1）
14. [頻率資料庫查詢系統](http://i.imgur.com/jFVCI8M.png)，國家通訊傳播委員會
15. [台東根本收不到hitfm](http://www.hitoradio.com/newweb/forum/viewtopic.php?f=83&t=40509)，hitoradio
16. [歐巴桑聯盟台中市議員第一波候選人今天出擊](http://www.chinatimes.com/realtimenews/20180308001819-260407)
17. [校園青春錄](https://web.archive.org/web/20040612005733/http://www.hitfm.com.tw/dj_prifiles/sharon.htm)
18. [2007年第三屆廣播電視小金鐘獎入圍名單-來源:PTT](https://www.ptt.cc/bbs/Golden-Award/M.1192919578.A.FDB.html)