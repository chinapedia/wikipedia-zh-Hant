**Huygens**是[荷兰语](../Page/荷兰语.md "wikilink")、[法语等语言中的姓](../Page/法语.md "wikilink")。

## 惠更斯

**惠更斯** （**Huygens**，**Huijgens**，**Huigens**，**Huijgen/Huygen**，或
**Huigen**）是一个[荷兰语](../Page/荷兰语.md "wikilink")[源于父名的](../Page/源于父名的.md "wikilink")（patronymic）姓，意为“[胡果之子](../Page/胡果.md "wikilink")”（英文："son
of Hugo"）。该姓名人有：

  - [康斯坦丁·惠更斯Constantijn](../Page/康斯坦丁·惠更斯.md "wikilink") Huygens
    (1596–1687)- [荷兰诗人和作曲家](../Page/荷兰.md "wikilink")
  - [克里斯蒂安·惠更斯Christiaan](../Page/克里斯蒂安·惠更斯.md "wikilink") Huygens
    (1629–1695)-
    [荷兰](../Page/荷兰.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")、[天文学家和](../Page/天文学家.md "wikilink")[数学家](../Page/数学家.md "wikilink")，[康斯坦丁·惠更斯之子](../Page/康斯坦丁·惠更斯.md "wikilink")
  - [小康斯坦丁·惠更斯Constantijn](../Page/小康斯坦丁·惠更斯.md "wikilink") Huygens, Jr.
    (1628-1697)-
    [荷兰政治家](../Page/荷兰.md "wikilink")、军人和[望远镜制造者](../Page/望远镜.md "wikilink")，[康斯坦丁·惠更斯之子](../Page/康斯坦丁·惠更斯.md "wikilink")，[克里斯蒂安·惠更斯之兄长](../Page/克里斯蒂安·惠更斯.md "wikilink")
  - [洛德韦克·惠更斯Lodewijck](../Page/洛德韦克·惠更斯.md "wikilink") Huygens (1631 -
    1699)-[康斯坦丁·惠更斯第三个儿子](../Page/康斯坦丁·惠更斯.md "wikilink")
  - [扬·惠更斯·范·林斯霍滕Jan](../Page/扬·惠更斯·范·林斯霍滕.md "wikilink") Huyghen van
    Linschoten (1563–1611)-[荷兰航海家和历史学家](../Page/荷兰.md "wikilink")

<!-- end list -->

  - 以惠更斯为名的机构有：

<!-- end list -->

  - [惠更斯荷兰历史研究所](../Page/惠更斯荷兰历史研究所.md "wikilink")（）（Huygens ING）

<!-- end list -->

  - 以[克里斯蒂安·惠更斯命名的事物有](../Page/克里斯蒂安·惠更斯.md "wikilink")：

<!-- end list -->

  - [惠更斯目镜](../Page/惠更斯目镜.md "wikilink")
  - [霍夫韦克惠更斯博物馆](../Page/霍夫韦克惠更斯博物馆.md "wikilink")（）
  - [惠更斯－菲涅耳原理Huygens](../Page/惠更斯－菲涅耳原理.md "wikilink")-Fresnel_principle
  - [卡西尼－惠更斯号](../Page/卡西尼－惠更斯号.md "wikilink")(Cassini-Huygens) -
    [土星](../Page/土星.md "wikilink")[探测器](../Page/空间探测器.md "wikilink")
  - [惠更斯号](../Page/惠更斯号.md "wikilink")(Huygens) -
    卡西尼－惠更斯号的着陆器，2005年1月14日降落在[土卫六上](../Page/土卫六.md "wikilink")
  - [2801 Huygens](../Page/小行星2801.md "wikilink")-小行星
  - [惠更斯软件Huygens](../Page/惠更斯软件.md "wikilink")
    Software-[微软的一款图像处理软件包](../Page/微软.md "wikilink")
  - [惠更斯Huygens](../Page/惠更斯_\(火星陨石坑\).md "wikilink") (Martian
    Crater)-一座[火星陨石坑](../Page/火星陨石坑.md "wikilink")
  - [惠更斯山Mons](../Page/惠更斯山.md "wikilink")
    Huygens-[月球上的一座山](../Page/月球.md "wikilink")

## 于让

**于让**（Huygens）是一个[法语姓](../Page/法语.md "wikilink")。

  - [莱昂·于让Léon](../Page/莱昂·于让.md "wikilink") Huygens (1876–1918)-
    [比利时画家](../Page/比利时.md "wikilink")