[dagen_h.png](https://zh.wikipedia.org/wiki/File:dagen_h.png "fig:dagen_h.png")

**H日**（[瑞典語](../Page/瑞典語.md "wikilink")：**Dagen
H**）指[瑞典在](../Page/瑞典.md "wikilink")[1967年](../Page/1967年.md "wikilink")[9月3日將](../Page/9月3日.md "wikilink")[道路通行方向由靠左改為靠右當日](../Page/道路通行方向.md "wikilink")。H代表瑞典語中的*Högertrafik*，即「右行交通」。

推行新制的兩主因為：

  - 所有與瑞典接壤的國家都是靠右行駛（包括與瑞典有很長接壤邊界的[挪威](../Page/挪威.md "wikilink")）
  - 瑞典國內民眾駕駛的汽車、及瑞典車廠出口至國外的汽車，駕駛方向盤大部分位於左邊（適於靠右行駛）

[Vote_no.jpg](https://zh.wikipedia.org/wiki/File:Vote_no.jpg "fig:Vote_no.jpg")
不過，改變通行方向的建議並不受歡迎，在40年來不斷被否決。[1955年](../Page/1955年.md "wikilink")10月16日的[全民公決](../Page/瑞典公民投票.md "wikilink")，83%選民支持維持靠左行駛。不過，[1963年](../Page/1963年.md "wikilink")，[瑞典議會仍通過改變通行方向的法案](../Page/瑞典議會.md "wikilink")，並成立*Statens
Högertrafikkommission（HTK）*（「全國右行交通委員會」）統籌新制。為期四年的教育計劃隨即開展，利用[心理學理論潛移默化](../Page/心理學.md "wikilink")，如將H日標誌放在各種日用產品上，包括女裝[內衣](../Page/內衣.md "wikilink")。

[Kungsgatan_1967.jpg](https://zh.wikipedia.org/wiki/File:Kungsgatan_1967.jpg "fig:Kungsgatan_1967.jpg")[國王街上一片混亂](../Page/國王街.md "wikilink")\]\]
隨著H日逼近，每一個交匯點都裝上用黑膠袋包著的新燈柱及路牌。1967年9月3日（星期日），工人在清晨拿走膠袋，所有非繁忙道路由凌晨1:00至清晨6:00停用，而任何在該段時間行駛的車輛需遵守特別規則：所有車輛在清晨4:45停駛，等候五分鐘，然後小心轉線，使車輛靠路的右邊行駛，再停下直至5:00才開車。各地的停駛時間有所不同，在[斯德哥爾摩和](../Page/斯德哥爾摩.md "wikilink")[馬爾默為](../Page/馬爾默.md "wikilink")10:00至15:00，有些則由周六15:00至周日的15:00。

單行道有較大問題，巴士站亦要在對面街重置。斯德哥爾摩的[電車被](../Page/電車.md "wikilink")[巴士取代](../Page/巴士.md "wikilink")。為配合新制，當地添置逾1000辆車門設於右邊的新巴士，並將8000輛舊巴士加上右邊車門。

[Dagen_H_Headlamp.jpg](https://zh.wikipedia.org/wiki/File:Dagen_H_Headlamp.jpg "fig:Dagen_H_Headlamp.jpg")
所有瑞典車輛要將車頭照明燈換成適合靠右行駛的型號。雖然H日不受大眾歡迎，瑞典議會仍要大力推動，其中一個原因是當時大部分瑞典車輛使用廉價的標準圓形車頭照明燈，但歐洲大陸以至全世界當時已轉用價錢較高的照明燈。[瑞典政府遂藉新制推動司機轉用新型照明燈](../Page/瑞典政府.md "wikilink")。

在H日後的第一個星期一，共有125宗交通意外報告，而之前的星期一宗數則介乎130至198宗。並無致命意外被指與轉換通行方向有關。不過，有不少長者因不適應新制而放棄駕駛。

## 參閱

  - [道路通行方向](../Page/道路通行方向.md "wikilink")
  - [瑞典交通](../Page/瑞典交通.md "wikilink")
  - [730](../Page/730_\(交通\).md "wikilink") -
    1978年[日本](../Page/日本.md "wikilink")[沖繩更改行車方向之措施](../Page/沖繩.md "wikilink")。

## 參考

  -
<!-- end list -->

  -
[Category:瑞典歷史](../Category/瑞典歷史.md "wikilink")
[Category:瑞典交通](../Category/瑞典交通.md "wikilink")
[Category:1967年9月](../Category/1967年9月.md "wikilink")