**i-mate**（[阿拉伯文](../Page/阿拉伯文.md "wikilink")：****），即原Carrier
Devices公司，成立於2001年1月，是一家[Windows
Mobile](../Page/Windows_Mobile.md "wikilink")[智慧型手機生產商](../Page/智慧型手機.md "wikilink")，[Microsoft的金牌認證合作夥伴](../Page/Microsoft.md "wikilink")。公司總部位於[阿拉伯聯合大公國的](../Page/阿拉伯聯合大公國.md "wikilink")[杜拜](../Page/杜拜.md "wikilink")，在澳大利亞，意大利，英國，南非和美國設有分公司，2005年9月公司重組，之後組建於蘇格蘭的母公司i-mate
plc在英國[倫敦證券交易所AIM](../Page/倫敦證券交易所.md "wikilink") Market上市。

## 產品

i-mate產品為[貼牌生產](../Page/ODM.md "wikilink")，主要分為兩大系列Ultimate，JAMA，早期由台灣的[宏達電與](../Page/宏達電.md "wikilink")[英業達代工](../Page/英業達.md "wikilink")，目前由台灣[華冠通訊](../Page/華冠通訊.md "wikilink")，[誠實科技與](../Page/誠實科技股份有限公司.md "wikilink")[德信無線三家企業代工](../Page/德信無線.md "wikilink")。

[華冠通訊與](../Page/華冠通訊.md "wikilink")[誠實科技代工的Ultimate系列](../Page/誠實科技.md "wikilink")：

  - Ultimate 6150
  - Ultimate 8150
  - Ultimate 8502
  - Ultimate 9502

[德信無線代工的JAMA系列](../Page/德信無線.md "wikilink")：

  - JAMA
  - JAMA 101
  - JAMA 201

[宏達電代工的早期著名产品](../Page/宏達電.md "wikilink")：

  - [Smartphone](../Page/HTC_Tanager.md "wikilink")
  - [Smartflip](../Page/HTC_Startrek.md "wikilink")
  - [JAM](../Page/HTC_Magician.md "wikilink")
  - [K-JAM](../Page/HTC_Wizard.md "wikilink")
  - [JAMin](../Page/HTC_Prophet.md "wikilink")
  - [JASJAM](../Page/HTC_Hermes.md "wikilink")
  - [JASJAR](../Page/HTC_Universal.md "wikilink")

## 外部連結

  - [i-mate](https://web.archive.org/web/20081009151319/http://imate.com/)

<!-- end list -->

  - [泛鸿海集团智能型手机再下一城 诚实科技首获i-mate订单
    与宏达电间接竞争](http://www.handsetinfo.com/info/info_detail.asp?id=41789)

[Category:電子公司](../Category/電子公司.md "wikilink")
[Category:行動電話製造商](../Category/行動電話製造商.md "wikilink")
[Category:跨国公司](../Category/跨国公司.md "wikilink")
[Category:行動電話](../Category/行動電話.md "wikilink")
[Category:智能手機](../Category/智能手機.md "wikilink")
[Category:个人数码助理](../Category/个人数码助理.md "wikilink")
[Category:阿拉伯聯合大公國公司](../Category/阿拉伯聯合大公國公司.md "wikilink")