**《I.Q.成熟時》**
是[香港](../Page/香港.md "wikilink")[麗的電視](../Page/麗的電視.md "wikilink")1980年代的青春[愛情](../Page/愛情.md "wikilink")[廣東話](../Page/廣東話.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")。1981年6月8日晚上七時零五分首播，共有20集。本劇播出後很受歡迎，收視高於無綫同時段同屬青春劇的劇集《[荳芽夢](../Page/荳芽夢.md "wikilink")》，成功打破翡翠台的[慣性收視](../Page/慣性收視.md "wikilink")，成為麗的電視經典劇集之一。

IQ成熟時拍攝期間，據聞[蔡楓華向傳媒表示自己戲份不及](../Page/蔡楓華.md "wikilink")[鍾保羅](../Page/鍾保羅.md "wikilink")，在IQ成熟時首播前宣佈轉投[無綫電視](../Page/無綫電視.md "wikilink")，電視台更削減他在劇中的戲份。

## 劇情大綱

花洒、朱仔、事頭婆、吱喳妹及阿姐等，是一所私立女校「聖羅蘭英文書院」的預科班學生，其中大部分因成績低落而留班。校長大白鯊為了重建校譽，打破傳統招收男生，於是預科班有三位男生加入，他們分別為阿Man、阿怪及阿強。此外，從越南來港的胡老師亦受聘任該班的班主任。

阿Man文武全才，在校大出風頭。他因時常照顧膽小之細細粒，兩人感情大進。

眾同學往夜店消遣，朱仔之表哥Peter在店內任歌手。Peter對花洒一見鍾情，主動替她補習，但她愛理不理，令Peter不安。

阿怪暗戀胡老師，而阿怪之父亦對胡老師有好感，遂產生三角戀情，導致父子不和。

朱仔之母為過氣名模，現開設一家時裝店，勸朱仔繼承母業，但朱仔不願意，欲致力讀書，希望成為女強人。

這群學生頑皮過人，時常以戲弄校長老師為樂，大白鯊與訓導主任塘虱決定進行分化政策，成立風紀隊，並任朱仔為Head
Prefect。某次花洒作怪，被塘虱處罰，花洒以為朱仔告密，因而反臉。花洒報復，阿Man見不平，幫助朱仔，令細細粒不悅。

朱仔母欲再嫁，朱仔反對，其母再嫁之日，朱仔照常上學，回家見母自殺，即送母入院，阿Man助之，朱仔對阿Man生好感，細細粒傷心。後細細粒在阿Man鼓勵之下入選民歌比賽，兩人和好如初。

花洒富有，Peter卻因家貧而自卑，故決意多找幾份工作，賺多些錢陪花洒遊玩。

大白鯊命學生參加校際戲劇節，並點花洒、朱仔和阿Man為主角，惜未能進入決賽。花洒和朱仔被其死黨同學指責因無愛情生活經驗，所以落敗，故眾人提議兩人同時追求阿Man，看誰能勝出，於是他們倆就展開了一場愛情魅力比賽。在此期間，阿Man以為她們兩個都是他的好朋友，所以阿Man有心事會跟她倆個談，花洒聽過阿Man的傾訴會不以為意，不理解；而朱仔聽過阿Man的心事，會為他分擔，並開解阿Man。
因此，阿Man開始認定朱仔是紅顏知己。
原來，這次的愛情遊戲，勝出的標準是看誰首先得到阿Man的[献吻](../Page/親吻.md "wikilink")，

## 演員

  - [蔡楓華](../Page/蔡楓華.md "wikilink") 飾 彭濟財 (Peter
    Pang)，[香港大學](../Page/香港大學.md "wikilink")[歷史系](../Page/歷史學.md "wikilink")[學生](../Page/學生.md "wikilink")
  - [鍾保羅](../Page/鍾保羅.md "wikilink") 飾 文樹森（阿Man）
  - [鄧藹霖](../Page/鄧藹霖.md "wikilink") 飾 黃玉珠（朱仔）
  - [莊靜而](../Page/莊靜而.md "wikilink") 飾 湯嘉棣（花灑）
  - [鄧慧詩](../Page/鄧慧詩.md "wikilink") 飾 許洵麗（細細粒／阿Cat）
  - [車保羅](../Page/車保羅.md "wikilink") 飾 楊　熹（阿怪／汽水怪）
  - [張　錚](../Page/張錚.md "wikilink") 飾
    阿怪之[父親](../Page/父親.md "wikilink")，[小食店](../Page/小食店.md "wikilink")[老闆](../Page/老闆.md "wikilink")
  - [楊詩蒂](../Page/楊詩蒂.md "wikilink") 飾 胡老師 (Miss Woo)
  - [莫少聰](../Page/莫少聰.md "wikilink") 飾 古耀輝（古惑仔）
  - [譚德成](../Page/譚德成.md "wikilink") 飾 張永強（阿強）
  - [何淑嫻](../Page/何淑嫻.md "wikilink") 飾 何美芬（吱喳妹）
  - [冼金華](../Page/冼金華.md "wikilink") 飾 冼美華（事頭婆）
  - [梁淑蘭](../Page/梁淑蘭.md "wikilink") 飾 梁珍納（阿姐）
  - [陳敏兒](../Page/陳敏兒.md "wikilink") 飾 林敏兒（林黛玉）
  - [周美玲](../Page/周美玲_\(演員\).md "wikilink") 飾 周小玲（阿蒙）
  - [李燕萍](../Page/李燕萍.md "wikilink") 飾 校　長（戴夫人/大白鯊）
  - [伍永森](../Page/伍永森.md "wikilink") 飾 唐主任（塘虱）
  - [吳　回](../Page/吳回.md "wikilink") 飾 老先生
  - [蕭山仁](../Page/蕭山仁.md "wikilink") 飾 阿Man之父親
  - [梁舜燕](../Page/梁舜燕.md "wikilink") 飾
    阿Man之[母親](../Page/母親.md "wikilink")
  - [良　鳴](../Page/良鳴.md "wikilink") 飾 Peter
    Pang之父親，[的士](../Page/的士.md "wikilink")[司機](../Page/車長.md "wikilink")
  - [鄧碧梅](../Page/鄧碧梅.md "wikilink") 飾 Peter
    Pang之[姐姐](../Page/姊.md "wikilink")
  - [馮　真](../Page/馮真.md "wikilink") 飾 朱仔之母親
  - [鄭文霞](../Page/鄭文霞.md "wikilink") 飾 花灑之母親
  - [許淑嫻](../Page/許淑嫻.md "wikilink") 飾 花灑之[細媽](../Page/庶母.md "wikilink")
  - [黃鈺賢](../Page/黃鈺賢.md "wikilink") 飾 體育老師

## 有趣情節

  - 特約演員[周星馳](../Page/周星馳.md "wikilink")：第一集下半部份，鍾保羅等三個男生，到處找學校報讀中六[預科班](../Page/預科.md "wikilink")，在一家男校被[周星馳客串演出的](../Page/周星馳.md "wikilink")[學生會會長擋路](../Page/學生會.md "wikilink")，強迫鍾保羅等入會，接受保護。
    因此，令至鍾保羅等十分害怕，走投無路，最後選擇入讀聖羅蘭英文書院，成為女校男生。\[1\]

<!-- end list -->

  - 友情客串[黃夏蕙](../Page/黃夏蕙.md "wikilink")：第十一集的中段，阿怪之父親因感情失落而借酒消愁，後來接受阿Man父親的慫恿，被安排與飾演富有寡婦的[黃夏蕙相親約會](../Page/黃夏蕙.md "wikilink")，但過程使怪父十分尷尬；之後怪父更以「[異形](../Page/異形.md "wikilink")」來形容黃。

## 相關

  - [香港式BBQ](../Page/香港燒烤文化.md "wikilink")
  - [慈善](../Page/慈善.md "wikilink")[獎券](../Page/獎券.md "wikilink")
  - [麥理浩徑](../Page/麥理浩徑.md "wikilink")
  - [戶口調查](../Page/戶口調查.md "wikilink")
  - [風紀](../Page/風紀.md "wikilink")、[校規](../Page/校規.md "wikilink")、[心理戰術](../Page/心理戰術.md "wikilink")
  - [黑膠唱片](../Page/黑膠唱片.md "wikilink")
  - [派對](../Page/派對.md "wikilink")
  - [啟德遊樂場](../Page/啟德遊樂場.md "wikilink")
  - [山頂公園](../Page/山頂公園_\(香港\).md "wikilink")
  - [愛情遊戲](../Page/愛情遊戲.md "wikilink")
  - [香港高等程度會考](../Page/香港高等程度會考.md "wikilink")

## 外部参考

  - [IQ成熟人面全非
    那時那地那人：鄧藹霖痛惜鍾保羅、蔡楓華](http://hk.apple.nextmedia.com/entertainment/art/20070417/7008172)
  - [《I.Q.成熟時》此劇出現的香港電視劇時代背景](http://bbs.ent.163.com/tvb/188492,0,50,0,40,2.html)
  - [I.Q.成熟時2007](http://hejnom.blogspot.com/2007_04_07_archive.html)
  - [周星馳 處男演出@麗的電視
    I.Q.成熟時 1981](http://www.youtube.com/watch?v=rIq9U0HB7KA)
  - [周星馳同學@IQ成熟時](https://www.youtube.com/watch?v=Ni6qUje1R14)

## 參考資料

[Category:麗的電視劇集](../Category/麗的電視劇集.md "wikilink")
[Category:1981年香港電視劇集](../Category/1981年香港電視劇集.md "wikilink")
[Category:香港電視喜劇](../Category/香港電視喜劇.md "wikilink")
[Category:香港校園劇](../Category/香港校園劇.md "wikilink")
[Category:香港愛情劇](../Category/香港愛情劇.md "wikilink")
[Category:校園電視喜劇](../Category/校園電視喜劇.md "wikilink")
[Category:大學背景電視劇](../Category/大學背景電視劇.md "wikilink")

1.  [蘋果日報
    話你知：星爺茄喱啡收保護費](http://hk.apple.nextmedia.com/entertainment/art/20070417/7008173)