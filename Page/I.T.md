[IT_Hysan_One_shop_at_night_20120418.JPG](https://zh.wikipedia.org/wiki/File:IT_Hysan_One_shop_at_night_20120418.JPG "fig:IT_Hysan_One_shop_at_night_20120418.JPG")[希慎道一號旗艦店](../Page/希慎道.md "wikilink")\]\]
[It_shop_in_Langham_Place_2014.jpg](https://zh.wikipedia.org/wiki/File:It_shop_in_Langham_Place_2014.jpg "fig:It_shop_in_Langham_Place_2014.jpg")
[It_shop_in_New_Town_Plaza_2006.jpg](https://zh.wikipedia.org/wiki/File:It_shop_in_New_Town_Plaza_2006.jpg "fig:It_shop_in_New_Town_Plaza_2006.jpg")
[HK_觀塘_Kwun_Tong_創紀之城五期_APM_mall_shop_IT_Clothing_Xmas_Dec-2013.JPG](https://zh.wikipedia.org/wiki/File:HK_觀塘_Kwun_Tong_創紀之城五期_APM_mall_shop_IT_Clothing_Xmas_Dec-2013.JPG "fig:HK_觀塘_Kwun_Tong_創紀之城五期_APM_mall_shop_IT_Clothing_Xmas_Dec-2013.JPG")[APM的i](../Page/APM.md "wikilink").t.[專門店](../Page/專門店.md "wikilink")\]\]
[I.t_blue_block_in_Festival_Walk_2019.jpg](https://zh.wikipedia.org/wiki/File:I.t_blue_block_in_Festival_Walk_2019.jpg "fig:I.t_blue_block_in_Festival_Walk_2019.jpg")i.t
blue block\]\]
[The_ONE_Level_2_it_brands_shops_2010.jpg](https://zh.wikipedia.org/wiki/File:The_ONE_Level_2_it_brands_shops_2010.jpg "fig:The_ONE_Level_2_it_brands_shops_2010.jpg")商場在2010年至2014年設多個i.t代理品牌，其後已結業\]\]

I.T（），於1988年成立，集團主要從事銷售時裝及配飾，為香港著名的[時裝零售商之一](../Page/時裝.md "wikilink")，在[香港](../Page/香港.md "wikilink")、[澳門](../Page/澳門.md "wikilink")、[中國大陸及](../Page/中國大陸.md "wikilink")[台灣銷售](../Page/台灣.md "wikilink")，設計及製造時裝的品牌服饰。

I.T代理多家[歐洲及](../Page/歐洲.md "wikilink")[日本時裝品牌](../Page/日本.md "wikilink")，包括[French
Connection及](../Page/Fcuk.md "wikilink")[A Bathing
Ape](../Page/A_Bathing_Ape.md "wikilink")，亦有設計及製造多個自家品牌，包括[AAPE](../Page/AAPE.md "wikilink")、[b+ab](../Page/b+ab.md "wikilink")、[:CHOCOOLATE](../Page/:CHOCOOLATE.md "wikilink")、[izzue](../Page/izzue.md "wikilink")、[Mini
Cream](../Page/Mini_Cream.md "wikilink")、[tout ā
coup](../Page/tout_ā_coup.md "wikilink")、[5cm等](../Page/5cm.md "wikilink")。

[沈嘉偉是I](../Page/沈嘉偉.md "wikilink").T主席兼行政總裁，其弟[沈健偉是創作總監](../Page/沈健偉.md "wikilink")；另外，[施俊偉是I](../Page/施俊偉.md "wikilink").T採購部副經理。大股東沈嘉偉家族持股量為89.56%。

## 財務狀況

2012/13財政年度截止日，I.T在[香港共有](../Page/香港.md "wikilink")295家店舖，[澳門](../Page/澳門.md "wikilink")10家香港附屬店舖，在[中國大陸及](../Page/中國大陸.md "wikilink")[台灣則分別擁有](../Page/台灣.md "wikilink")237及21家店舖。集團之總營業額增長14.0%達65.43億港元。每股基本盈利減少20.5%至0.31港元。每股攤薄盈利減少18.9%至0.30港元。

香港繼續是集團的主要市場以及最大的銷售與盈利貢獻來源。於2012/13財政年度，香港業務的總營業額達37.08億港元，較去年增加8.8%，佔集團總營業額之56.7%。中國大陸業務之總營業額再錄得另一年的強勁增長，上升至20.364億港元，較去年增加31.8%，佔集團總營業額之31.1%。雖然中國大陸之市場環境充滿挑戰，集團繼續以在所有營運地區之中最快的速度來推動擴展中國大陸之零售網絡。[日本業務之營業額在](../Page/日本.md "wikilink")2011/12財政年度的較高基數下減至5.144億港元，較去年減少8.1%，佔集團總營業額之7.9%。

2015年10月底，I.T公佈2015/16財政年度的中期業績，除受早前公佈、因人民幣貶值造成的一次性大額外滙虧損影響外，上半年香港業務疲弱，錄得逾9,000萬[港元虧損](../Page/港元.md "wikilink")，為2005年上市以來首次「見紅」。儘管業績欠佳，惟公司指仍考慮審慎擴張，無意大規模關舖。\[1\]

## 歷史

1988年11月，沈嘉偉與其弟[沈健偉在香港](../Page/沈健偉.md "wikilink")[銅鑼灣](../Page/銅鑼灣.md "wikilink")[伊利莎伯大廈開設](../Page/伊利莎伯大廈.md "wikilink")「Green
Peace」，銷售當時甚為流行的[英國](../Page/英國.md "wikilink")[Dr.
Martens皮鞋及](../Page/Dr._Martens.md "wikilink")[Levi's](../Page/Levi's.md "wikilink")
501牛仔褲，亦引入不少當時於香港甚罕見的[歐洲時裝品牌](../Page/歐洲.md "wikilink")。店舖其後不斷擴展，分店一家接一家地設立。1993年在同區[信和廣場開設首間旗艦店](../Page/信和廣場.md "wikilink")。1997年，由於店舖名稱與[環保組織](../Page/環保.md "wikilink")[綠色和平相同](../Page/綠色和平.md "wikilink")，Green
Peace易名為**I.T** （Income Team）
繼續經營。I.T在2005年3月4日在[香港交易所上市](../Page/香港交易所.md "wikilink")。

目前，I.T公司與香港另一間上市的[旭日企業合股成立](../Page/旭日企業.md "wikilink")[旭日宜泰公司](../Page/旭日宜泰公司.md "wikilink")，合作發展香港、澳門、台灣、以及大陸的業務。各地共有店鋪180餘間。2008年的全年零售業績為20億2100萬[港幣](../Page/港幣.md "wikilink")。

2011年1月，I.T公司以2.3億元[日圓](../Page/日圓.md "wikilink")（約2185萬港元）代價，收購日本品牌[A
Bathing Ape的控股公司Nowhere約](../Page/A_Bathing_Ape.md "wikilink")
90.27%股權。\[2\]同年7月，沈嘉偉表示由於加租壓力很大，不排除未來會關閉本港部分店舖，但他強調，旗下產品短期不會因為租金急升而加價，希望以增加生意抵銷有關影響。他亦指出內地租金增幅亦達到雙位數，但增幅低於香港，未來會於[杭州](../Page/杭州.md "wikilink")、[南京](../Page/南京.md "wikilink")、[天津](../Page/天津.md "wikilink")、[深圳及](../Page/深圳.md "wikilink")[廣州等地開設更多分店](../Page/廣州.md "wikilink")。

同年，I.T位於[銅鑼灣](../Page/銅鑼灣.md "wikilink")[希慎道一號的旗艦店](../Page/希慎道.md "wikilink")「**I.T
HYSAN ONE**」開幕，樓高四層，分層設立多個自家品牌之店舖，總面積高達接近二萬五千平方呎。

2014年，I.T得到香港眾多明星的站台支持，包括巨星[張學友](../Page/張學友.md "wikilink")、[張曼玉及其他明星如](../Page/張曼玉.md "wikilink")[張智霖](../Page/張智霖.md "wikilink")、[袁詠儀](../Page/袁詠儀.md "wikilink")、[吳君如](../Page/吳君如.md "wikilink")、[蔡一智及](../Page/蔡一智.md "wikilink")[上山詩鈉等](../Page/上山詩鈉.md "wikilink")。

2018年，I.T邀請了來自世界各地的紋身師參與在希慎道一號旗艦店的Pop-up店開幕，當中更邀請了被《NEW YORK
TIMES》喻為世上最知名的美國紋身師之一的紋身大師Dr. Woo\[3\]。

2018年12月24日，I.T斥資4.86億瑞典克朗，即大約4.23億港元，購入瑞典服飾品牌[Acne
Studios的](../Page/Acne_Studios.md "wikilink")10.9%權益

## I.T與i.t.之分別

[大寫字母](../Page/大寫.md "wikilink")「I.T」是售賣高級時裝，價錢較高，目前只在[港島及](../Page/港島.md "wikilink")[九龍區](../Page/九龍區.md "wikilink")，[中國](../Page/中國.md "wikilink")[廣州設有分店](../Page/廣州.md "wikilink")。小寫字母「i.t.」則是售賣年青人的時裝，價錢較適中，在港九[新界](../Page/新界.md "wikilink")、廣州均設有多家分店。

## 旗下品牌

[Izzue_in_Festival_Walk_2017.jpg](https://zh.wikipedia.org/wiki/File:Izzue_in_Festival_Walk_2017.jpg "fig:Izzue_in_Festival_Walk_2017.jpg")izzue\]\]
[Double_park_in_YOHO_Mall_2015.jpg](https://zh.wikipedia.org/wiki/File:Double_park_in_YOHO_Mall_2015.jpg "fig:Double_park_in_YOHO_Mall_2015.jpg")double-park\]\]

### 自家品牌

  - [I.T](../Page/I.T.md "wikilink")
  - [i.t](../Page/i.t.md "wikilink")
  - [i.t blue
    block](../Page/i.t_blue_block.md "wikilink")（2018年創立，時尚與生活結合，並引入日本人氣班戟店[FLIPPER'S](../Page/FLIPPER'S.md "wikilink")）
  - [b+ab](../Page/b+ab.md "wikilink")（1995年創立）
  - [izzue](../Page/izzue.md "wikilink")（1999年創立）
  - [double-park](../Page/double-park.md "wikilink")（2000年創立）
  - [5cm](../Page/5cm.md "wikilink")
  - [CHOCOOLATE](../Page/CHOCOOLATE.md "wikilink") （2006年創立）
  - [Venilla Suite](../Page/Venilla_Suite.md "wikilink")
  - [EXI.T](../Page/EXI.T.md "wikilink")
  - [ete\!](../Page/ete!.md "wikilink")
  - [fingercroxx](../Page/fingercroxx.md "wikilink")
  - [tout à coup](../Page/tout_à_coup.md "wikilink")
  - [Katie Judith](../Page/Katie_Judith.md "wikilink")
  - [Mini Cream](../Page/Mini_Cream.md "wikilink")

### 部份代理品牌

  - [A Bathing Ape](../Page/A_Bathing_Ape.md "wikilink")
  - [Alexander McQueen](../Page/Alexander_McQueen.md "wikilink")
  - [ANNA SUI](../Page/ANNA_SUI.md "wikilink")
  - [Ann Demeulemeester](../Page/Ann_Demeulemeester.md "wikilink")
  - [as know as de base](../Page/as_know_as_de_base.md "wikilink")
  - [BEAMS](../Page/BEAMS.md "wikilink")
  - [BEAMS BOY](../Page/BEAMS_BOY.md "wikilink")
  - [Comme Des Garçons](../Page/Comme_Des_Garçons.md "wikilink")
  - [Camper](../Page/Camper.md "wikilink")
  - [French Connection](../Page/French_Connection.md "wikilink")（fcuk）
  - [Fred Perry](../Page/Fred_Perry.md "wikilink")
  - [Gravis](../Page/Gravis.md "wikilink")
  - [HYOMA](../Page/HYOMA.md "wikilink")
  - [Journal Standard](../Page/Journal_Standard.md "wikilink")
  - [MUSIUM DIV.](../Page/MUSIUM_DIV..md "wikilink")
  - [Maison Martin
    Margiela](../Page/Maison_Martin_Margiela.md "wikilink")
  - [NEIGHBORHOOD](../Page/NEIGHBORHOOD.md "wikilink")
  - [SOPHNET.](../Page/SOPHNET..md "wikilink")
  - [STYLENANDA](../Page/STYLENANDA.md "wikilink")
  - [Tsumori Chisato](../Page/Tsumori_Chisato.md "wikilink")
  - [Zadig & Voltaire](../Page/Zadig_&_Voltaire.md "wikilink")
  - [zucca](../Page/zucca.md "wikilink")

## 參考

## 外部链接

  - [I.T](http://www.ithk.com/)
  - <http://www.izzue.com/>

[Category:1988年成立的公司](../Category/1988年成立的公司.md "wikilink")
[Category:香港交易所上市公司](../Category/香港交易所上市公司.md "wikilink")
[Category:香港上市服務業公司](../Category/香港上市服務業公司.md "wikilink")
[Category:香港服装公司](../Category/香港服装公司.md "wikilink")
[Category:服裝品牌](../Category/服裝品牌.md "wikilink")
[Category:香港時裝品牌](../Category/香港時裝品牌.md "wikilink")
[Category:香港零售商](../Category/香港零售商.md "wikilink")
[Category:香港上市服裝公司](../Category/香港上市服裝公司.md "wikilink")

1.
2.  [I.T購 A Bathing
    Ape](http://hk.apple.nextmedia.com/template/apple/art_main.php?iss_id=20110201&sec_id=15307&subsec_id=15320&art_id=14932539)
    蘋果日報，2011年2月1日
3.  [專訪最時尚的紋身師 Dr.
    Woo︰我就像一個心理醫生](https://www.mings-fashion.com/tattoos-i-t-dr-woo-3332/)
    MING'S