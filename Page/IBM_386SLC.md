**IBM
386SLC**是一顆[處理器](../Page/處理器.md "wikilink")，是由[Intel公司將](../Page/Intel.md "wikilink")[386SX處理器的技術授權給](../Page/Intel_80386.md "wikilink")[IBM公司](../Page/IBM.md "wikilink")，IBM公司取得技術後以此技術為基礎所再行發展，並於1991年正式推出、量產的處理器。

IBM
386SLC內部具有電源管理的功效機能、8KB容量的內部[快取記憶體](../Page/快取記憶體.md "wikilink")，所以IBM
386SLC在相同時脈下能夠比Intel的[80386DX處理器還快速](../Page/Intel_80386.md "wikilink")，所以價格上也比較昂貴。此外IBM內部在此處理器研發的最初時還將其稱為「超級小晶片，Super
Little Chip」。

IBM 386SLC用於[IBM
PS/2的](../Page/PS/2.md "wikilink")35、40、56等系列的個人電腦上，不過這並沒有為這些系列的個人電腦帶來更高的市場佔有率。除此之外IBM
386SLC處理器也以一種「選用性[升級](../Page/升級.md "wikilink")」方式來推銷，即是過去IBM PS/2
25系列的個人電腦，可以在機內加裝此顆處理器，並取代原有機內的[8086處理器](../Page/Intel_8086.md "wikilink")，讓原有的電腦獲得運算效能的提升。

## 設計及技術

IBM
386SLC是用[CMOS的製程技術所製造成](../Page/CMOS.md "wikilink")，[裸晶的面積為](../Page/積體電路.md "wikilink")161平方公釐，工作時脈的頻率有16[MHz](../Page/赫茲_\(單位\).md "wikilink")、20MHz、及25MHz，其中25MHz頻率的運作下用電僅2.5[瓦](../Page/瓦特.md "wikilink")，極低的功耗用電使IBM
SLC386特別適合用在[筆記型電腦](../Page/筆記型電腦.md "wikilink")（Laptop）或其他的可攜式裝置上。

## IBM 486SLC

**IBM 486SLC**是IBM 386SLC處理器的強化版本，它具有以下的主要特點：

  - 32位元的執行處理核心，並相容於[Intel
    80486SX處理器](../Page/Intel_80486SX.md "wikilink")。
  - 外部連結的協同處理器為[Intel 80387](../Page/Intel_80387.md "wikilink")
  - 外部匯流排相同於[Intel
    80386SX處理器](../Page/Intel_80386SX.md "wikilink")：16條資料線與24條位址線
  - 內部具有16KB容量的快取記憶體
  - 具有與[Intel
    80386SL處理器](../Page/Intel_80386SL.md "wikilink")（專供筆記型電腦用）相同的電源管理機制
  - 採用與Intel 80386SX相同的[PLCC晶片封裝](../Page/PLCC.md "wikilink")。

## IBM 486SLC2

**IBM 486SLC2**是倍頻版本的IBM 486SLC，即是內頻為外頻的兩倍速率，例如IBM
486SLC-50即有50MHz的內頻與25MHz的外頻，IBM
486SLC2-50用於[IBM公司PS/2系列的PS](../Page/PS/2.md "wikilink")/2E（IBM
Model 9533）型個人電腦中。

## 關連

  - [Intel 386](../Page/Intel_80386.md "wikilink")
  - [IBM PS/2系列的個人電腦](../Page/PS/2.md "wikilink")
  - [ThinkPad](../Page/ThinkPad.md "wikilink") - IBM於1992年發創的筆記型電腦系列

## 參考

  - [PC
    Magazine的文章，報導IBM 386SLC處理器](http://www.pcmag.com/encyclopedia_term/0,2542,t=386SLC&i=37060,00.asp)
  - [IBM
    SLC386處理器的描述及圖片](https://web.archive.org/web/20030419172541/http://www.mic-d.com/gallery/chips/ibm386slclow.html)

[Category:微處理器](../Category/微處理器.md "wikilink")
[x86](../Category/X86架構.md "wikilink")
[Category:IBM处理器](../Category/IBM处理器.md "wikilink")