[IBook_flavors.jpg](https://zh.wikipedia.org/wiki/File:IBook_flavors.jpg "fig:IBook_flavors.jpg")
**iBook**是[苹果电脑公司設計生產的](../Page/苹果电脑公司.md "wikilink")[筆記型电脑系列產品](../Page/筆記型电脑.md "wikilink")。

隨著 [iMac](../Page/iMac.md "wikilink")
的成功和[蘋果電腦公司的市場策略轉變](../Page/蘋果電腦.md "wikilink")，針對消費者和教育市場推出了一個全新的[筆記型電腦產品系列](../Page/筆記型電腦.md "wikilink")：“iBook”。

雖然瞄準低端的消費群體，但相對於多數的其他電腦生產商常見的市場策略：把過時的專業科技銷售給一般消費市場，蘋果電腦獨創地把 iBook
設計為其專業筆記型電腦 [PowerBook
G3系列的一種衍生性產品系列](../Page/PowerBook_G3.md "wikilink")，採用多種關鍵性的特色使得它能在早期市場上成功。

iBook最终與PowerBook一起在2006年被[MacBook所取代](../Page/MacBook.md "wikilink")。

## iBook: iMac to go.

经过一番深思熟虑，[史蒂夫·賈伯斯](../Page/史蒂夫·賈伯斯.md "wikilink")
在1999年7月21日於[紐約的](../Page/紐約.md "wikilink")[Macworld
Conference &
Expo](../Page/Macworld_Conference_&_Expo.md "wikilink")，发布了以消費者為導向的
iBook
[筆記型電腦](../Page/筆記型電腦.md "wikilink")。它的設計哲學受到[蘋果的桌上型電腦](../Page/蘋果.md "wikilink")
[iMac](../Page/iMac.md "wikilink") 之影響，擁有大的特殊外觀，半透明，和彩色的塑膠外殼。它的市場口號是
"iMac to go"。

針對的消費者群包含年輕的小孩，因此在中心部位裝有手提把手。[蘋果電腦讓Phil](../Page/蘋果電腦.md "wikilink")
Schiller提著 iBook 然後從高處跳下（到墊子上），來展示他的外殼的耐久性。和 iMac 一樣，iBook 使用 [PowerPC
G3](../Page/PowerPC_G3.md "wikilink")
晶片，且包含非蘋果的傳統介面。[USB](../Page/USB.md "wikilink")，[Ethernet](../Page/Ethernet.md "wikilink")，和[數據機埠都是標準配備](../Page/數據機.md "wikilink")，以及[光碟機](../Page/光碟機.md "wikilink")。這些沒有遮蓋的輸出輸入埠擺放在側邊，因為遮蓋被認為是容易損壞的。同樣地，iBook的彈簧鎖是隱藏的，也就是關閉螢幕時為了嵌合螢幕與主機所使用的勾子。在螢幕蓋上主機前的時候，螢幕上的小勾子會被磁鐵吸下來，然後嵌合主機。iBook
在底下側邊有電源連接頭，允許讓多個 iBook 可以在客制化的充電架上輕易的充電。

第一代的 iBook
曾經是售出擁有內建[無線網路的第一部主流電腦](../Page/無線網路.md "wikilink")，他的天線內建在顯示面板的邊緣（註，最原先的
iBook 需要一塊選購的 AirPort
擴充卡來使用無線網路）。[蘋果電腦與](../Page/蘋果電腦.md "wikilink")[朗訊電訊合作](../Page/朗訊電訊.md "wikilink")，開創
iBook
的無線網路能力，成為一個工業的標準。蘋果也在同時推出[AirPort無線基地台](../Page/AirPort.md "wikilink")。

任何東西都變成熱烈的爭議 - 設計[美學](../Page/美學.md "wikilink")，特色，重量，效能，價格等等。iBook
在那時候比 PowerBook 稍微重，但只有更少的規格。很久的謠傳特色有觸控式螢幕，超長時效的電池都不存在。iBook
由於它獨特的設計，被標榜為 "蛤殼"，在中國也被戲稱為"馬桶座"。

不管這些，iBook
有成功的銷售佳績。產品線不斷地得到[處理器](../Page/處理器.md "wikilink")、[記憶體](../Page/記憶體.md "wikilink")，和[硬碟的升級](../Page/硬碟.md "wikilink")。就像
iMac，推出多種新的顏色機種；[FireWire](../Page/FireWire.md "wikilink") 和視訊輸出也被加入。

### 型式

[Clamshell_iBook_G3.jpg](https://zh.wikipedia.org/wiki/File:Clamshell_iBook_G3.jpg "fig:Clamshell_iBook_G3.jpg")

  - iBook（1999年6月21日）- 第一部 iBook（橘子，藍莓）
      - 12-inch 主動矩陣 TFT 顯示（800x600 最大解析度）
      - G3 300 MHz
      - 32/64 MB 記憶體
      - 6 GB 硬碟
      - CD-ROM
      - USB, Ethernet
      - Airport (B)
      - Mac OS 8.6
  - iBook 第二版（2000年2月16日）- 次要的擴充到現存的產品線（鉛黑色）
      - 366 MHz
      - Mac OS 9.0.2
      - （其他的規格與 iBook 相同）
  - iBook Firewire/SE（2000年9月13日）- 主要的改版（鉛黑色，靛藍色，Key-lime）
      - 12-inch 主動矩陣 TFT 顯示（800x600 最大解析度）
      - G3 366/466 MHz
      - 64 MB 記憶體
      - 10 GB 硬碟
      - CD/DVD-ROM
      - USB, Firewire, Video Out, Ethernet
      - Airport (B)
      - Mac OS 9.0.4

原本的 iBook 設計到 2001年5月 就中止，轉而推出新的 "雙 USB" iBook.

### 擴充性／升級

iBook 可以讓消費者自行安裝的部份只有擴充記憶體跟 AirPort
無線網卡，透過兩個鍵盤上的卡榫可以輕易的移開鍵盤。在保固內沒有其他的安裝或變更可以作，而且沒有
[PCMCIA](../Page/PCMCIA.md "wikilink")
擴充埠來提供額外的擴充功能。要存取到內部的元件的話，像是硬碟驱动器或是光碟驱动器，需要複雜的過程和數不盡的螺絲起子來打開。這個限制到現今生產的所有
iBook 都還是如此。

## iBook 雙 USB 埠（12.1吋 & 14.1吋）

下一代的 iBook 在
2001年3月1日於[庫比提諾的研討發表會上首次露臉](../Page/庫比提諾.md "wikilink")。重要的是，這部機器已經重新發明，擁有新的特色和新的設計。

美學上來講，先前 iBook 的強烈顏色和根本的外觀（比較有競爭）都捨棄了，轉變為單純白色和輕薄的外觀。這個更小的機器重量比較輕，有更高品質的
12吋 [LCD](../Page/LCD.md "wikilink")
螢幕和更大的想法成為優越的設計。[蘋果電腦的精采設計得到工業讚譽](../Page/蘋果電腦.md "wikilink")，因此被廣泛的複製。

iBook 的設計，跟隨著從他的姊妹產品來的元素，[PowerBook
G4是目前](../Page/PowerBook_G4.md "wikilink")[蘋果電腦整個產品使用的母體](../Page/蘋果電腦.md "wikilink")。只有幾個比較少的例外，在消費者產品線使用白色的[塑膠](../Page/塑膠.md "wikilink")，像是[iMac](../Page/iMac.md "wikilink")、[eMac](../Page/eMac.md "wikilink")、和
iBook，而飛機使用的[鋁則在專業的產品像是](../Page/鋁.md "wikilink")[Power Mac
G5和](../Page/Power_Mac_G5.md "wikilink")[PowerBook
G4中被採用](../Page/PowerBook_G4.md "wikilink")。

之後 iBook
的設計就大部分維持一樣。在2002年1月7日於[舊金山的](../Page/舊金山.md "wikilink")[Macworld
Conference &
Expo中](../Page/Macworld_Conference_&_Expo.md "wikilink")，現有的
12吋型號中加入14吋的型號。

後來，[PowerPC
G4的晶片和吸入式光碟](../Page/PowerPC_G4.md "wikilink")，於2003年10月23日加入到
iBook，最後結束了[蘋果電腦使用](../Page/蘋果電腦.md "wikilink") G3
的晶片。及後，[蘋果電腦的膝上型](../Page/蘋果電腦.md "wikilink")／可攜式產品線只包含
iBook 和 [PowerBook G4](../Page/PowerBook_G4.md "wikilink")。

### 型號

[Ibook12.jpg](https://zh.wikipedia.org/wiki/File:Ibook12.jpg "fig:Ibook12.jpg")

  - iBook 雙 USB（2001年5月1日）- 第二代 iBook
      - 12-inch 主動矩陣 TFT 顯示（1024x768 最大解析度）
      - G3 500 MHz
      - 128 MB 記憶體
      - 10 GB 硬碟
      - CD/CDRW/DVD/Combo
      - USB, Firewire, Video Out, Ethernet
      - Airport (B)
      - Mac OS 9.1
  - iBook 雙 USB 2001年後期（2001年10月16日）- 小改版
      - 600 MHz
      - 15 GB 硬碟（大部份的型號）
      - Mac OS X 10.1
      - （其他規格與雙 USB 版本相同）
  - iBook 14吋（2002年1月7日）- 新型號，更大的 14 吋顯示
      - 14-inch 主動矩陣 TFT 顯示（1024x768 最大解析度）
      - 256 MB 記憶體
      - （其他規格與 2001 年後的雙 USB 版本相同）
  - iBook 2002年中（2002年5月20日）- 小改版
      - 600/700 MHz
      - Mac OS X 10.1
      - （其他的規格與 14吋版本相同）
  - iBook 2003年早期（2003年4月22日）- 小改版
      - 800/900 MHz
      - Mac OS X 10.2
      - （其他規格與 2002年中期版本相同\]\]）

[IBook_G4.jpg](https://zh.wikipedia.org/wiki/File:IBook_G4.jpg "fig:IBook_G4.jpg")

  - iBook G4（2003年10月22日）- 主要大改版，更換處理器
      - 12吋 or 14吋 主動矩陣 TFT 顯示器（1024x768 最大解析度）
      - G4 800/933/1000 MHz
      - 256 MB 記憶體
      - 30/40/60 GB 硬碟
      - 吸入式 Combo (CD-RW/DVD-ROM)
      - USB, Firewire, Video Out, Ethernet
      - Airport Extreme (G)
      - Mac OS X 10.3 "Panther"
  - iBook G4 2004年早期（2004年4月19日）- 小改版
      - G4 1.0/1.25 GHz
      - 吸入式 SuperDrive (DVD-R) 成為選購配備
      - （其他的規格與 2003年 iBook G4 相同）
  - iBook G4 2004年後期（2004年10月19日）- 小改版
      - G4 1.2/1.33 GHz
      - 30/60/80 GB 硬碟
      - 吸入式 Combo/SuperDrive
      - （其他的規格與 iBook G4 2004年早期版本相同）
  - iBook G4 2005年中期（2005年7月26日）
      - M9846LL/A: 1.33 GHz; 12吋顯示；40GB 硬碟；吸入式 Combo Drive DVD-ROM/CD-RW
      - M9848LL/A: 1.42 GHz; 14吋顯示；60GB 硬碟；吸入式 SuperDrive DVD±RW/CD-RW
      - 兩個型號都有新的特色：512MB 記憶體（可擴充至 1.5GB）；ATI Mobility Radeon 9550
        圖形處理器搭配 32MB
        視訊記憶體；[防震感測裝置](../Page/防震感測裝置.md "wikilink")（Sudden
        Motion Sensor，如果 iBook 掉落則會把硬碟磁頭歸位）；支援捲動的觸控板；藍芽 2.0+EDR；
      - （其他規格與 2004年 後期 iBook G4 一樣）
      - （Apple 原先搭配 Mac OS X 10.3 Panther 一起出貨，但由於 Mac OS X 10.4 Tiger
        的推出，故所有 iBook 都包含Mac OS X 10.4 Tiger。）

### 擴充性／升級

客戶可以自行安裝的部份，像是 AirPort（無線）擴充卡，或者是額外的[記憶體](../Page/記憶體.md "wikilink")，安裝到
iBook 是相當的容易，因為鍵盤的設計，只要把兩個彈簧扣打開就可以輕易的移下鍵盤，如果想要的話也可以用螺絲起子把它鎖起來。

然而，現在的 iBook
設計也特別顯得很難打開，像是硬碟升級對於外行人來說就不太可能。要更換或者是拿到硬碟機，需要至少三種起螺絲起子，轉下超過三十個螺絲，而且過程中要拿下許多不相干的零件。比較起來，大部份
[Intel](../Page/Intel.md "wikilink")
的筆記型電腦之外觀因素，只要使用一或兩種螺絲起子，就可以把硬碟盒子拆下。

## 外部連結

  - [Apple - iBook](http://www.apple.com/ibook/)
  - [AppleSpec
    消費者規格列表](http://www.info.apple.com/support/applespec.html)
  - [Apple - 如何辨識你的
    iBook](http://docs.info.apple.com/article.html?artnum=n88039)
  - [綜合的技術細節](https://web.archive.org/web/20040803000939/http://developer.apple.com/documentation/Hardware/hardware2.html)
  - [iBook at
    Everymac](http://www.everymac.com/systems/apple/ibook/index.html)
  - [Article: iBook: An iMac to
    Go](http://www.tidbits.com/tb-issues/TidBITS-490.html#lnk2) from
    [TidBITS](../Page/TidBITS.md "wikilink") issue 490
  - [Do-It-Yourself](http://www.pbfixit.com/Guide/50.0.0.html) 升級指南。

[Category:麦金塔](../Category/麦金塔.md "wikilink")
[Category:筆記型電腦](../Category/筆記型電腦.md "wikilink")