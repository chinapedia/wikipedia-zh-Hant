[Db-410001-01.jpg](https://zh.wikipedia.org/wiki/File:Db-410001-01.jpg "fig:Db-410001-01.jpg")中央車站\]\]

**試驗型城際列車特快**（，即ICE-V，V = Versuch(德文) =
Experiment）是[德國鐵路為](../Page/德國鐵路.md "wikilink")[德國發展](../Page/德國.md "wikilink")[高速鐵路的第一輛試作型列車](../Page/高速鐵路.md "wikilink"),
它亦是[城際特快列車](../Page/城際特快列車.md "wikilink")(InterCityExpress)的先驅者。

## 歷史

1974年，德國國鐵開始計畫在德國建設一個高速的鐵路網路。在一輪研究、探勘及試建後，在1984年InterCityExperimental正式開始建造。1985年3月15日，410號列車正式在Essen介紹予公眾，並在不久之後開始產生。其9400萬德國馬克的發展費用由德國研究部支付。

在1988年5月1日一次由[漢諾威至](../Page/漢諾威.md "wikilink")[維爾茨堡的在新建鐵路高速試車中](../Page/維爾茨堡.md "wikilink")，由5節車廂組成的ICE-V做出406.9公里每小時的高速，這是鐵路列車一個新的[世界記錄](../Page/世界記錄.md "wikilink")。但兩年後被法國TGV-Atlantique編號325號列車，在1990年5月18日以每小時515.3公里的速度超越。

InterCityExperimental在1989年6月中作出了特別行駛。[北威州州長讓當時正在國事訪問德國的](../Page/北威州.md "wikilink")[苏联共产党中央委员会总书记](../Page/苏联共产党中央委员会总书记.md "wikilink")[米哈伊尔·戈尔巴乔夫在一之社交聚會上登上這輛列創紀錄的火車](../Page/米哈伊尔·戈尔巴乔夫.md "wikilink")。到這個時，北威州州旗和[蘇聯的聯邦旗被共同放在車上](../Page/蘇聯.md "wikilink")。

直到1991年市場成熟時，InterCityExperimental數據再一次被整理。技術人員重新估計它的安全最大速度、制動器，並研究量產的可行性。ICE-1型示範車輛面世時，它與InterCityExperimental非常相似，參觀數據亦表示InterCityExperimental只是被修改了很小的地方，這可說明InterCityExperimental的泛用性極高。

在ICE-1實行量產並開始生產後
，InterCityExperimental重新被命名為「V」和用於進一步的嘗試，例如在1995年它換裝了ICE2的車頭蓋子。經過超過10年的使用，在1998年夏季，ICE-V作它退役前最後一次行駛。

ICE-V在退役後，被其繼承者[ICE-1迅速取代](../Page/ICE-1列車.md "wikilink")。它在德國國鐵多個機械廠房中被分拆。動力車頭一號在Minden作紀念碑，二號放置在[慕尼黑](../Page/慕尼黑.md "wikilink")，直至2006年4月7日轉移至[德國博物館交通中心作展覽](../Page/德國博物館交通中心.md "wikilink")。

## 參見

  - [高速鐵路](../Page/高速鐵路.md "wikilink")
  - [城際列車](../Page/城際列車.md "wikilink")

## 参考文献

<div class="references-small">

<references>

</references>

</div>

  -

[Category:城际快车](../Category/城际快车.md "wikilink")
[Category:15千伏16.7赫兹交流电力动车组](../Category/15千伏16.7赫兹交流电力动车组.md "wikilink")