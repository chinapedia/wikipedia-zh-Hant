**ICE**（[日文](../Page/日文.md "wikilink")：****）是一部[OVA作品](../Page/OVA.md "wikilink")，全3卷。

## 簡介

接近現在的未來時，全人類男性死绝，最後世界上僅有2萬名女性還存在[新宿](../Page/新宿.md "wikilink")。眼見人類世界已經註定凋亡，少女們為著僅憑女性也可以繁衍的技術「ICE」展開了生死搏鬥。

## 人物

  - ヒトミ：[皆川純子](../Page/皆川純子.md "wikilink")
    ユキ：[小野惠令奈](../Page/小野惠令奈.md "wikilink")（[AKB48](../Page/AKB48.md "wikilink")）
    サツキ：[間宮くるみ](../Page/間宮くるみ.md "wikilink")
    キサラギ：[鳳芳野](../Page/鳳芳野.md "wikilink")
    ジュリア：[石田彰](../Page/石田彰.md "wikilink")
    リンネ：[大島優子](../Page/大島優子.md "wikilink")（[AKB48](../Page/AKB48.md "wikilink")）
    アオイ：[河西智美](../Page/河西智美.md "wikilink")（[AKB48](../Page/AKB48.md "wikilink")）
    ムラサキ：[佐藤夏希](../Page/佐藤夏希.md "wikilink")（[AKB48](../Page/AKB48.md "wikilink")）
    ウスハ：[今井優](../Page/今井優.md "wikilink")（[AKB48](../Page/AKB48.md "wikilink")）
    ミント：[喜多村英梨](../Page/喜多村英梨.md "wikilink")
    瞳：[池澤春菜](../Page/池澤春菜.md "wikilink")

## 製作

  - 企劃・原作 - [秋元康](../Page/秋元康.md "wikilink")
  - 監督・機械設計 - [小林誠](../Page/小林誠_\(插畫家\).md "wikilink")
  - 腳本 - [平野靖士](../Page/平野靖士.md "wikilink")、小林誠
  - 角色設定 - [大西雅也](../Page/大西雅也.md "wikilink")
  - 作画監督 - 大西雅也、金子誠、[増尾昭一](../Page/増尾昭一.md "wikilink")
  - 分鏡 - 小林誠
  - 演出 - 水野健太郎
  - 美術監督 - 小林誠
  - 色彩設計 - 和田秀美、小林誠
  - 撮影監督 - 桑良人、小林誠、箭内光一
  - 編集 - 布施由美子
  - 音樂 - [ウォン・イル](../Page/ウォン・イル.md "wikilink")
  - 音響監督 - [鶴岡陽太](../Page/鶴岡陽太.md "wikilink")
  - 製作者 - 澤昌樹、宮本継一、[井上博明](../Page/井上博明.md "wikilink")、千葉博己
  - 動畫制作 - [P・P・M](../Page/P・P・M.md "wikilink")
  - 制作 - オニロ
  - 製作 - [E-NET
    FRONTIER](../Page/E-NET_FRONTIER.md "wikilink")、PROJECT-ICE

## 主題曲

  - 「所谓被爱（）」
    作詞 - 秋元康 / 作曲 - 山崎燿 / 編曲 - 松浦晃久 / 歌 - ICE from AKB48

## 副標題

1.  一日目：はと-HEART-
2.  二日目：るる-RULE-
3.  三日目：あさ-ANSWER-

## 相關項目

  - [AKB48](../Page/AKB48.md "wikilink")

## 外部連結

  - [ICE　- アイス
    -　project-ice.com](https://web.archive.org/web/20081217063018/http://www.project-ice.com/)，ICE動畫官方站台。
  - [ICE (OAV) - Anime News
    Network](http://www.animenewsnetwork.com/encyclopedia/anime.php?id=7528&page=25)。

[Category:2007年日本OVA動畫](../Category/2007年日本OVA動畫.md "wikilink")
[Category:科幻動畫](../Category/科幻動畫.md "wikilink") [Category:百合
(類型)](../Category/百合_\(類型\).md "wikilink")
[Category:秋元康](../Category/秋元康.md "wikilink")
[Category:新宿背景作品](../Category/新宿背景作品.md "wikilink")