[ICQ_Logo_noText.svg](https://zh.wikipedia.org/wiki/File:ICQ_Logo_noText.svg "fig:ICQ_Logo_noText.svg")
**ICQ**是最早出現的[即時通訊軟件之一](../Page/即時通訊軟件.md "wikilink")。ICQ源自[以色列](../Page/以色列.md "wikilink")[特拉维夫的](../Page/特拉维夫.md "wikilink")公司（成立於1996年7月），由幾個以色列青年（包括Chan
si hou Yair Goldfinger、Sefi Vigiser、Amnon Amir、Arik
Vardi以及Arik之父親在1996年11月發明。

ICQ是[英文](../Page/英文.md "wikilink")“I SEEK
YOU”諧音，[中文意思是](../Page/中文.md "wikilink")：「我找你」。

## 概要

ICQ是一款网络即時通訊軟件，支持透過[網際網路聊天](../Page/網際網路.md "wikilink")，以及发送訊息、[网址及文件等功能](../Page/网址.md "wikilink")。朋友雙方装上ICQ软件，上网时可以互相聯絡。現時的還可以與朋友玩小遊戲、進行[視訊](../Page/視訊.md "wikilink")。

在1998年，-{zh-hant:美國在線; zh-cn:美国在线;
zh-tw:美國線上;}-（[AOL](../Page/AOL.md "wikilink")）购买下ICQ以后推出更多功能的99a、99B、2000等版本，但同時於ICQ內加插了廣告。內建了搜索功能使軟件變得臃腫，加上不太受歡迎的廣告欄，且[Windows
Live
Messenger](../Page/Windows_Live_Messenger.md "wikilink")、[Skype等軟體推出](../Page/Skype.md "wikilink")，令使用ICQ的人有下降的趨勢。后来Mirabilis公司推出了简化版的ICQ
lite，只含有ICQ最基本的功能。

2005年2月7日ICQ
5發佈。不久，ICQ與[香港](../Page/香港.md "wikilink")[電訊盈科合作推出](../Page/電訊盈科.md "wikilink")**Netvigator
ICQ
5**，除了部份界面的文字被翻譯成[粵語外](../Page/粵語.md "wikilink")，更加入多項切合香港網民使用習慣的新功能。

2010年1月19日ICQ
7發佈。不過，官方版本不再支持[繁體中文](../Page/繁體中文.md "wikilink")。在畫面上，排版更像[MSN](../Page/MSN.md "wikilink")
8的設計，而且可以與[Facebook等交友網站消息互通及匯入各](../Page/Facebook.md "wikilink")[電郵地址的通訊錄](../Page/電郵地址.md "wikilink")，所以較之前一直以來的版本更用戶化。4月28日，[AOL將ICQ出售給](../Page/AOL.md "wikilink")[俄羅斯的網路公司Mail](../Page/俄羅斯.md "wikilink").Ru\[1\]，

2011年11月15日新版ICQ官方免費[iPhone及](../Page/iPhone.md "wikilink")[Android應用程式發佈](../Page/Android.md "wikilink")。加入類似[WhatsApp的用法及功能](../Page/WhatsApp.md "wikilink")，啟動後就不用每次登入，長期在線，用法與流行的即時通訊軟體類似。由於ICQ突然發佈了[手機程式](../Page/手機應用程式.md "wikilink")，迅速引起了大量用家的注意，並有不少玩家重新開始使用ICQ。

## 参见

  - [即时通讯软件列表](../Page/即时通讯软件列表.md "wikilink")
  - [騰訊QQ](../Page/騰訊QQ.md "wikilink")

## 外部链接

  - [ICQ官方網頁](http://www.icq.com)
  - [web ICQ登入介面](http://www.icq.com/download/webicq/en)

## 參考資料

[Category:即时通讯软件](../Category/即时通讯软件.md "wikilink")
[Category:廣告軟件](../Category/廣告軟件.md "wikilink")
[Category:美國線上公司](../Category/美國線上公司.md "wikilink")

1.