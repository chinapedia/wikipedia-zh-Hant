**iDVD**是一款由[蘋果公司编写的](../Page/蘋果公司.md "wikilink")[DVD制作软件](../Page/DVD.md "wikilink")，只能运行在[Mac
OS
X上](../Page/Mac_OS_X.md "wikilink")。iDVD允许用户把[QuickTime电影](../Page/QuickTime.md "wikilink")、[MP3音乐和数码图片制作为一片可以在DVD机中播放的DVD](../Page/MP3.md "wikilink")。它通常作为苹果[iLife套装的最后一步](../Page/iLife.md "wikilink")，把所有其它iLife应用软件所制作的结果输入到[移动媒体中](../Page/移动媒体.md "wikilink")。

最初只出现在带[Superdrive的Mac上](../Page/Superdrive.md "wikilink")，现在已停止开发，从OS X
Lion中去除。升级系统后想要继续使用iDVD，可以下载市面上支持更高版本的iDVD Alternative软体（for
Lion/Mountain lion/Mavericks），但是蘋果公司并没有发布官方的替代解决方案。

## 版本歷史

| **版本**          | **日期**     | **更新**                                                                                                                    |
| --------------- | ---------- | ------------------------------------------------------------------------------------------------------------------------- |
| iDVD            | 2001年1月9日  |                                                                                                                           |
| iDVD 2          | 2001年7月18日 |                                                                                                                           |
| iDVD 3          | 2003年1月7日  |                                                                                                                           |
| iDVD 4          | 2004年1月6日  | iLife '04 的一部分                                                                                                            |
| iDVD 5          | 2005年1月11日 | iLife '05 的一部分                                                                                                            |
| iDVD 6          | 2006年1月10日 | iLife '06 的一部分,第一個版本 iDVD to be made available as a Universal binary. Also sports a refined look based on iTunes 5 and 6. |
| iDVD 7/iDVD '08 | 2007年8月7日  | iLife '08 的一部分                                                                                                            |

## 外部链接

  - [Apple: iDVD](http://www.apple.com/idvd/)
  - [Apple Discussions: iDVD
    Alternative](https://discussions.apple.com/thread/3997630)

[ru:ILife\#iDVD](../Page/ru:ILife#iDVD.md "wikilink")

[Category:蘋果公司軟體](../Category/蘋果公司軟體.md "wikilink")
[Category:DVD](../Category/DVD.md "wikilink")
[Category:视频编辑软件](../Category/视频编辑软件.md "wikilink")
[Category:MacOS軟體](../Category/MacOS軟體.md "wikilink")