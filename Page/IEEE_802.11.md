**IEEE
802.11**是現今[无线局域网通用的标准](../Page/无线局域网.md "wikilink")，它是由[國際電機電子工程學會](../Page/國際電機電子工程學會.md "wikilink")（IEEE）所定义的[无线网络通信的标准](../Page/无线网络.md "wikilink")。

虽然经常将[Wi-Fi与](../Page/Wi-Fi.md "wikilink")802.11混为一谈，但两者并不等同。

## 历史

自第二次世界大战，无线通讯因在军事上应用的成果而受到重视，无线通讯一直发展，但缺乏广泛的[通讯标准](../Page/通讯标准.md "wikilink")。于是，IEEE在1997年为无线局域网制定了第一个版本标准──[IEEE
802.11](../Page/IEEE_802.11_\(原始标准\).md "wikilink")。其中定义了[媒体存取控制层](../Page/媒体存取控制层.md "wikilink")（MAC层）和[物理层](../Page/物理层.md "wikilink")。物理层定义了工作在2.4GHz的[ISM频段上的两种](../Page/ISM频段.md "wikilink")[扩频作](../Page/扩频.md "wikilink")[调制方式和一种](../Page/调制.md "wikilink")[红外線传输的方式](../Page/红外線传输.md "wikilink")\[1\]，总数据传输速率设计为2Mbit/s。两个设备可以自行构建[临时网络](../Page/无线临时网络.md "wikilink")，也可以在[基站](../Page/基站.md "wikilink")（Base
Station, BS）或者接入点（Access
Point，AP）的协调下通信。为了在不同的通讯环境下取得良好的通讯质量，采用[CSMA/CA](../Page/CSMA/CA.md "wikilink")（Carrier
Sense Multiple Access/Collision Avoidance）硬件沟通方式。

1999年加上了两个补充版本：802.11a定义了一个在5GHz
ISM频段上的数据传输速率可达54Mbit/s的物理层，802.11b定义了一个在2.4GHz的ISM频段上但数据传输速率高达11Mbit/s的物理层。
2.4GHz的[ISM频段为世界上绝大多数国家通用](../Page/ISM频段.md "wikilink")，因此802.11b得到了最为广泛的应用。苹果公司把自己开发的802.11标准起名叫AirPort。1999年工业界成立了[Wi-Fi联盟](../Page/Wi-Fi.md "wikilink")，致力解决符合802.11标准的产品的生产和设备兼容性问题。

## IEEE 802.11標準列表

  - [IEEE
    802.11](../Page/IEEE_802.11_\(原始标准\).md "wikilink")，1997年，原始标准（2Mbit/s，播在2.4GHz）。
  - [IEEE
    802.11a](../Page/IEEE_802.11a.md "wikilink")，1999年，物理层补充（54Mbit/s，播在5GHz）。
  - [IEEE
    802.11b](../Page/IEEE_802.11b.md "wikilink")，1999年，物理层补充（11Mbit/s，播在2.4GHz）。
  - [IEEE
    802.11c](../Page/IEEE_802.11c.md "wikilink")，符合802.1D的媒体接入控制层桥接（MAC
    Layer Bridging）。
  - [IEEE 802.11d](../Page/IEEE_802.11d.md "wikilink")，根据各国无线电规定做的调整。
  - [IEEE 802.11e](../Page/IEEE_802.11e.md "wikilink")，对服务等级（Quality of
    Service, [QoS](../Page/QoS.md "wikilink")）的支持。
  - [IEEE
    802.11f](../Page/IEEE_802.11f.md "wikilink")，基站的互连性（[IAPP](../Page/IAPP.md "wikilink")，Inter-Access
    Point Protocol），2006年2月被IEEE批准撤销。
  - [IEEE
    802.11g](../Page/IEEE_802.11g.md "wikilink")，2003年，物理层补充（54Mbit/s，播在2.4GHz）。
  - [IEEE
    802.11h](../Page/IEEE_802.11h.md "wikilink")，2004年，无线覆盖半径的调整，室内（indoor）和室外（outdoor）信道（5GHz频段）。
  - [IEEE
    802.11i](../Page/IEEE_802.11i.md "wikilink")，2004年，无线网络的安全方面的補充。
  - [IEEE 802.11j](../Page/IEEE_802.11j.md "wikilink")，2004年，根据日本规定做的升级。
  - [IEEE
    802.11k](../Page/IEEE_802.11k.md "wikilink")，该协议规范规定了无线局域网络频谱测量规范。该规范的制订体现了无线局域网络对频谱资源智能化使用的需求。
  - [IEEE 802.11l](../Page/IEEE_802.11l.md "wikilink")，预留及准备不使用。
  - [IEEE 802.11m](../Page/IEEE_802.11m.md "wikilink")，维护标准；互斥及极限。
  - [IEEE
    802.11n](../Page/IEEE_802.11n.md "wikilink")，更高传输速率的改善，基础速率提升到72.2Mbit/s，可以使用双倍带宽40MHz，此时速率提升到150Mbit/s。支持多输入多输出技术（Multi-Input
    Multi-Output，[MIMO](../Page/MIMO.md "wikilink")）。
  - [IEEE 802.11o](../Page/IEEE_802.11o.md "wikilink")，针对VOWLAN（Voice
    over WLAN）而制订，更快速的无限跨区切换，以及读取语音（voice）比数据（Data）有更高的传输优先权。
  - [IEEE
    802.11p](../Page/IEEE_802.11p.md "wikilink")，這個通訊協定主要用在車用電子的無線通訊上。它設定上是從IEEE
    802.11來擴充延伸，來符合[智慧型運輸系統](../Page/智慧型運輸系統.md "wikilink")（Intelligent
    Transportation Systems，ITS）的相關應用。
  - [IEEE 802.11q](../Page/IEEE_802.11q.md "wikilink")
  - [IEEE 802.11r](../Page/IEEE_802.11r.md "wikilink")：快速BSS切换（FT）（2008）
  - [IEEE 802.11s](../Page/IEEE_802.11s.md "wikilink")：Mesh Networking,
    [Extended Service
    Set](../Page/Extended_Service_Set.md "wikilink")（ESS）（July 2011）
  - [IEEE 802.11t](../Page/IEEE_802.11t.md "wikilink")：Wireless
    Performance Prediction (WPP)—test methods and metrics Recommendation
    <sub>cancelled</sub>
  - [IEEE 802.11u](../Page/IEEE_802.11u.md "wikilink")：Improvements
    related to HotSpots and 3rd party authorization of clients, e.g.
    cellular network offload (February 2011)
  - [IEEE 802.11v](../Page/IEEE_802.11v.md "wikilink")：Wireless [network
    management](../Page/network_management.md "wikilink")（February 2011）
  - [IEEE 802.11w](../Page/IEEE_802.11w.md "wikilink")：Protected
    Management Frames (September 2009)
  - [IEEE 802.11x](../Page/IEEE_802.11x.md "wikilink")
  - [IEEE 802.11y](../Page/IEEE_802.11y.md "wikilink")：3650–3700 MHz
    Operation in the U.S.（2008）
  - [IEEE 802.11z](../Page/IEEE_802.11z.md "wikilink")：Extensions to
    Direct Link Setup (DLS)（September 2010）
  - IEEE 802.11-2012：A new release of the standard that includes
    amendments k, n, p, r, s, u, v, w, y and z (March 2012)
  - [IEEE 802.11aa](../Page/IEEE_802.11aa.md "wikilink")：Robust
    streaming of Audio Video Transport Streams (June 2012)
  - [IEEE 802.11ab](../Page/IEEE_802.11ab.md "wikilink")
  - [IEEE
    802.11ac](../Page/IEEE_802.11ac.md "wikilink")，802.11n的潜在继承者，更高传输速率的改善，当使用多基站时将无线速率提高到至少1Gbps，将单信道速率提高到至少500Mbps。使用更高的无线带宽（80MHz-160MHz，802.11n只有40MHz），更多的MIMO流（最多8条流），更好的调制方式（QAM256）。目前是草案标准（draft），预计正式标准于2012年晚些时间推出。Quantenna公司在2011年11月15日推出了世界上第一只采用802.11ac的无线路由器。Broadcom公司于2012年1月5日也发布了它的第一支支持802.11ac的芯片。
  - [IEEE 802.11ad](../Page/IEEE_802.11ad.md "wikilink")：Very High
    Throughput 60 GHz (December 2012) - see
    [WiGig](../Page/WiGig.md "wikilink")
  - [IEEE 802.11ae](../Page/IEEE_802.11ae.md "wikilink")：Prioritization
    of Management Frames（2012年3月）
  - [IEEE 802.11af](../Page/IEEE_802.11af.md "wikilink")：運用過往電視白區（TV
    White
    Space，TVWS）的頻段所訂立標準，由於使用白區頻段（VHS的54MHz～216MHz及UHF的470MHz～698MHz），有時IEEE
    802.11af也稱為White-Fi（取Wi-Fi一詞的衍生變化）。
  - [IEEE
    802.11ah](../Page/IEEE_802.11ah.md "wikilink")：用來支援無線感測器網路（Wireless
    Sensor Network，WSN），以及支援物聯網（Internet of Thing，IoT）、智慧型電網（Smart
    Grid）的智慧電錶（Smart Meter）等應用。
  - [IEEE 802.11ai](../Page/IEEE_802.11ai.md "wikilink")：為IEEE
    802.11的修正案，新增部分機制，以及加速建立網路連線的等待時間。
  - [IEEE 802.11aj](../Page/IEEE_802.11aj.md "wikilink")：為[IEEE
    802.11ad的增補標準](../Page/IEEE_802.11ad.md "wikilink")，開放45GHz的未授權頻寬帶使世界上部分地區可以使用。
  - [IEEE 802.11aq](../Page/IEEE_802.11aq.md "wikilink")：為IEEE
    802.11的修正案，增加網路探索的效率，以加快網路傳輸速度。
  - [IEEE 802.11ax](../Page/IEEE_802.11ax.md "wikilink")：以現行的[IEEE
    802.11ac做為基底的草案](../Page/IEEE_802.11ac.md "wikilink")，以提供比現行的傳輸速率加快4倍為目標。

除了上面的IEEE标准，另外有一个被称为IEEE 802.11b+的技术，通过PBCC技术（Packet Binary
Convolutional Code）在IEEE
802.11b（2.4GHz频段）基础上提供22Mbit/s的数据传输速率。但这事实上并不是一个IEEE的公开标准，而是一项产权私有的技术，产权属于[德州仪器](../Page/德州仪器.md "wikilink")。

## IEEE 802.11a

[IEEE
802.11a是](../Page/IEEE_802.11a.md "wikilink")802.11原始标准的一个修订标准，于1999年获得批准。802.11a标准采用了与原始标准相同的核心协议，工作频率为5GHz，使用52个[正交频分多路复用副载波](../Page/OFDM.md "wikilink")，最大原始数据传输率为54Mb/s，这达到了现实网络中等吞吐量（20Mb/s）的要求。

由于2.4G频段日益拥挤，使用5G频段是802.11a的一个重要的改进。但是，也带来了问题。传输距离上不及802.11b/g；理论上5G信号也更容易被墙阻挡吸收，所以802.11a的覆盖不及801.11b。802.11a同样会被干扰，但由于附近干扰信号不多，所以802.11a通常吞吐量比较好。

## IEEE 802.11b

[IEEE
802.11b是](../Page/IEEE_802.11b.md "wikilink")[无线局域网的一个标准](../Page/无线局域网.md "wikilink")。其载波的频率为2.4GHz，可提供1、2、5.5及11Mbit/s的多重传送速度\[2\]。在2.4-GHz的[ISM频段共有](../Page/ISM频段.md "wikilink")11个频宽为22MHz的频道可供使用，它是11个相互重叠的频段。IEEE
802.11b的后继标准是[IEEE 802.11g](../Page/IEEE_802.11g.md "wikilink")。

## IEEE 802.11g

[IEEE
802.11g在](../Page/IEEE_802.11g.md "wikilink")[2003年7月被通過](../Page/2003年7月.md "wikilink")。其载波的频率为2.4GHz（跟802.11b相同），共14个频段，原始传送速度为54Mbit/s，净传输速度约为24.7Mbit/s（跟802.11a相同）。802.11g的设备向下与802.11b兼容。

其后有些无线路由器厂商因应市场需要而在IEEE
802.11g的标准上另行开发新标准，并将理论传输速度提升至108Mbit/s或125Mbit/s。

## IEEE 802.11i

**IEEE
802.11i**是IEEE为了弥补802.11脆弱的安全加密功能（[WEP](../Page/WEP.md "wikilink")，Wired
Equivalent
Privacy）而制定的修正案，于2004年7月完成。其中定义了基于[AES的全新加密协议](../Page/高级加密标准.md "wikilink")[CCMP](../Page/CCMP.md "wikilink")（CTR
with CBC-MAC Protocol）。

无线网络中的安全问题从暴露到最终解决经历了相当的时间，而各大厂通信芯片商显然无法接受在这期间什么都不出售，所以迫不及待的[Wi-Fi厂商采用](../Page/Wi-Fi.md "wikilink")802.11i的草案3为蓝图设计了一系列通信设备，随后称之为支持[WPA](../Page/WPA.md "wikilink")（Wi-Fi
Protected
Access）的，這個協定包含了向前兼容[RC4的加密协议](../Page/RC4.md "wikilink")[TKIP](../Page/TKIP.md "wikilink")（Temporal
Key Integrity
Protocol），它沿用了WEP所使用的硬體並修正了一些缺失，但可惜仍然不是毫無安全弱點的；之后称将支持802.11i最终版协议的通信设备称为支持[WPA2](../Page/WPA2.md "wikilink")（Wi-Fi
Protected Access 2）的。

## IEEE 802.11k

IEEE
802.11k阐述了无线局域网中频谱测量所能提供的服务，并以协议方式规定了测量的类型及接收发送的格式。此协议制定了几种有测量价值的频谱资源信息，并建立了一种请求／报告机制，使测量的需求和结果在不同终端之间进行通信。协议制定小组的工作目标是要使终端设备能够通过对测量信息的量读做出相应的传输调整，为此，协议制定小组定义了测量类型\[3\]。

这些测量报告使在IEEE
802.11规范下的无线网络终端可以收集临近AP的信息（信标报告）和临近终端链路性质信息（帧报告，隐藏终端报告和终端统计报告）。测量终端还可以提供信道干扰水平（噪声柱状报告）和信道使用情况（信道负荷报告和媒介感知柱状图）。

## IEEE 802.11n

**IEEE
802.11n**，是由IEEE在组成的一个新的工作组在802.11-2007的基础上发展出来的标准，于[2009年9月正式批准](../Page/2009年9月.md "wikilink")。该标准增加了对[MIMO的支持](../Page/MIMO.md "wikilink")，允许40MHz的无线频宽，最大传输速度理論值為600Mbit/s。同时，通过使用Alamouti提出的[空时分组码](../Page/空时分组码.md "wikilink")，该标准扩大了数据传输范围。

## IEEE 802.11ac

IEEE
802.11ac是一個正在發展中的802.11無線[計算機網路通信標準](../Page/計算機網路.md "wikilink")，它透過6GHz頻帶（也就是一般所說的5GHz頻帶）進行[無線區域網](../Page/無線區域網.md "wikilink")（WLAN）通信。理論上，它能夠提供最少每秒1
Gigabit頻寬進行多站式[無線區域網](../Page/無線區域網.md "wikilink")（WLAN）通訊，或是最少每秒500
megabits（500 Mbit/s）的單一連線傳輸頻寬。

它採用並擴展了源自[802.11n的空中接口](../Page/#802.11n.md "wikilink")（air
interface）概念，包括：更寬的RF頻寬（提升至160
MHz），更多的[MIMO](../Page/MIMO.md "wikilink")[空間串流](../Page/空間串流.md "wikilink")（spatial
streams，增加到8），MU-MIMO，以及高密度的[解調變](../Page/解調變.md "wikilink")（modulation，最高可達到256
[QAM](../Page/QAM.md "wikilink")）。它是IEEE 802.11n的繼任者。

## IEEE 802.11ad

無線千兆聯盟（Wireless Gigabit Alliance，WiGig），工業組織，致力於推動在無執照的60
GHz頻帶上，進行數千兆位元（multi-gigabit）速度的無線裝置資料傳輸技術。此聯盟於2009年5月7日宣布成立，於2009年12月推出第一版1.0
WiGig技術規格（802.11ad）。

## IEEE 802.11ax

2017年,Broadcom率先推出802.11ax無線晶片，由於先前802.11ad主要在於60GHZ頻段，雖然增長了傳輸速度，但是其覆蓋範圍受到限制，便成為輔助802.11ac的功能性技術。
依照IEEE的官方計畫，繼承802.11ac的第六代Wifi為802.11ax，自2018年起推出支援的分享器。

## 各国适用频道

802.11b和802.11g将2.4
GHz的频段区分为14个重复，标记的频道;每个频道的中心频率相差5兆赫兹（MHz）。一般常常被误认的是频道1，6和11（还有有些地区的频道14）是互不重叠所以利用这些不重叠的频道，多组无线网络的互相涵盖，互不影响，这种看法太过简单。802.11b和802.11g并没有规范每个频道的频宽，规范的是中心频率和频谱屏蔽（spectral
mask）。802.11b的频谱屏蔽需求为：在中心频率±11 MHz处，至少衰减30 dB，±22 MHz处要衰减50 dB.

由于频谱屏蔽只规定到±22
MHz处的能量限制，所以通常认定使用频宽不会超过这个范围。实际上，当发射端距离接收端非常近时，接收端接受到的有效能量频谱，有可能会超过22
MHz的区域。所以，一般认定频道1，6和11互不重叠的说法。应该要修正为：频道1，6和11，三个频段互相之间的影响比使用其它频段小。然而，要注意的是，一个使用频道1的高功率发射端，可以轻易的干扰到一个使用频道6的，功率较低的发射站。在实验室的测试中发现，当使用频道11来传递档案时，一个使用频道1的发射台也在通讯时，会影响到频道11的档案传输，让传输速率稍稍降低。所以，即使是频段相差最远的频道1和11，也是会互相干扰的。

虽然频道1、6和11互不重叠的说法是不正确的，但是这个说法至少可以用来说明：频道距离在1，6和11之间虽然会对彼此造成干扰，而却不会強烈地影响到通讯的传输速率。

高功率AP为了因应较高的振幅，将中心频率±11
MHz处的频谱屏蔽提高到-40dB，所以才会由ch1干扰到ch6，ch6干扰到ch11。至于ch1干扰到ch11是因为AP功率放大到非线性饱和区，某些厂商制造的AP确实全盖台（Ch1－Ch11）。而正好那些产品又是卖最多的，也就是号称功率最高的。只要符合FCC规范压在±11
MHz -30dB、±22 MHz -50dB就不会出现互相干扰问题。某些芯片制造商在量产或技术上接近ACPR（Adjacent Channel
Power Ratio鄰近通道功率比例）不合格边缘，透过放大器放大会导致上述情形出现。

[2.4_GHz_Wi-Fi_channels_(802.11g_WLAN).svg](https://zh.wikipedia.org/wiki/File:2.4_GHz_Wi-Fi_channels_\(802.11g_WLAN\).svg "fig:2.4_GHz_Wi-Fi_channels_(802.11g_WLAN).svg")

### 总结

| 协议       | 发布年/月   | Op.标准频宽                              | 实际速度（標準）                              | 实际速度（最大）                                                        | 半径范围（室内）         | 半径范围（室外）  |
| -------- | ------- | ------------------------------------ | ------------------------------------- | --------------------------------------------------------------- | ---------------- | --------- |
| Legacy   | 1997    | 2.4-2.5 GHz                          | 1 Mbit/s                              | 2 Mbit/s                                                        | ?                | ?         |
| 802.11a  | 1999    | 5.15-5.35/5.47-5.725/5.725-5.875 GHz | 25 Mbit/s                             | 54 Mbit/s                                                       | 约30米             | 約45米\[4\] |
| 802.11b  | 1999    | 2.4-2.5 GHz                          | 6.5 Mbit/s                            | 11 Mbit/s                                                       | 约30米             | 约100米     |
| 802.11g  | 2003    | 2.4-2.5 GHz                          | 25 Mbit/s                             | 54 Mbit/s                                                       | 约30米             | 约100米     |
| 802.11n  | 2009    | 2.4 GHz or 5 GHz bands               | 300 Mbit/s（20MHz\*4 MIMO）             | 600 Mbit/s（40MHz\*4 MIMO）                                       | 约70米             | 约250米     |
| 802.11p  | 2009    | 5.86-5.925 GHz                       | 3 Mbit/s                              | 27 Mbit/s                                                       | 约300米            | 约1000米    |
| 802.11ac | 2011.11 | 5 GHz                                | 433Mbit/s, 867Mbit/s（80MHz，160MHz为可选） | 867Mbit/s, 1.73 Gbit/s, 3.47 Gbit/s, 6.93 Gbit/s（8 MIMO，160MHz） | 約35米\[5\]        |           |
| 802.11ad | 2019.12 | 2.4/5/60 GHz                         | 4620Mbps                              | 7Gbps(6756.75Mbps)                                              | 約1-10米\[6\]\[7\] |           |
| 802.11ax | 2018.12 | 2.4/5 GHz                            |                                       | 10.53Gbps                                                       | 10米              | 100米\[8\] |

## 標準及延伸標準

## 参考文献

## 外部链接

  - [IEEE 802.11 Wireless
    LAN网络](http://www.cs.nthu.edu.tw/%7Enfhuang/chap13.htm)

## 参见

  - [WiFi](../Page/WiFi.md "wikilink")
  - [WLAN信道列表](../Page/WLAN信道列表.md "wikilink")
  - [IEEE 802](../Page/IEEE_802.md "wikilink")
  - [IEEE 802.1](../Page/IEEE_802.1.md "wikilink")

{{-}}

[Category:Wi-Fi](../Category/Wi-Fi.md "wikilink")
[Category:无线网络](../Category/无线网络.md "wikilink") [Category:IEEE
802.11](../Category/IEEE_802.11.md "wikilink")

1.  《无线区域网络技术白皮书》 ISBN 986-125-599-0

2.
3.  IEEE 802.11 WORKING GROUP (2003) Draft Supplement to STANDARD FOR
    Telecommunications and Information Exchange Between Systems-LAN/MAN
    Specific Requirements-Part 11：Wireless Medium Access Control (MAC)
    and Physical Layer (PHY)specifications：Specification for Radio
    Resource Measurement,IEEE 802.11k/D0.7.New York USA：The Institute of
    Electrical and Electronics Engineers,Inc.

4.  802.11a uses the 5GHz frequency to communicate. It can communicate
    at 54Mbps, which is fast for wireless, but it has a short range of
    150 feet.

5.  <http://www.digitimes.com.tw/tw/showimg.asp?source=0000259572_WOQ6HN646RRBQ93CFVH4D&filename=259572-2-FVH4D.jpg&Sourcetype=1>

6.  <http://www.radio-electronics.com/info/wireless/wi-fi/ieee-802-11ad-microwave.php>

7.  <https://en.wikipedia.org/wiki/Wireless_Gigabit_Alliance>

8.  <http://www.ni.com/white-paper/53150/zht/>