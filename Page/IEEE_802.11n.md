**IEEE 802.11n-2009**，對於[IEEE
802.11-2007](../Page/IEEE_802.11#IEEE_802.11-2007.md "wikilink")[無線區域網路標準的修正規格](../Page/無線區域網路.md "wikilink")。它的目標在於改善先前的兩項無線網路標準，包括[802.11a與](../Page/802.11a.md "wikilink")[802.11g](../Page/802.11g.md "wikilink")，在網路流量上的不足。它的最大传输速度理論值為600Mbit/s，與先前的54Mbit/s相比有大幅提升，傳輸距離也會增加。2004年1月时[IEEE宣布组成一个新的单位来发展的新的](../Page/IEEE.md "wikilink")802.11标准，于2009年9月正式批准。

对802.11n的后续研究正在[IEEE
802.11ac草案中进行](../Page/IEEE_802.11ac.md "wikilink")，预计可以于2014年2月正式发布，将提供8xMIMO，最高160MHz带宽和最高866.7Mbit/s的理论速度。\[1\]

## 技術特點

  - 802.11n增加了对于[MIMO的标准](../Page/MIMO.md "wikilink")，使用多个发射和[接收天线来允许更高的数据传输率](../Page/接收天线.md "wikilink")，并使用了Alamouti于1998年提出的[空时分组码来增加传输范围](../Page/空时分组码.md "wikilink")。
  - 802.11n支持在标准带宽（20MHz）上的速率包括有（单位Mbit/s）：7.2, 14.4, 21.7, 28.9, 43.3,
    57.8, 65, 72.2（短保护间隔，单数据流）。使用4xMIMO时速度最高为300Mbit/s。
  - 802.11n也支持双倍带宽（40MHz），当使用40MHz带宽和4\*MIMO时，速度最高可达600Mbit/s。

## 限制

2.4GHz和5GHz都可自由選擇20MHz或40MHz[頻寬](../Page/頻寬.md "wikilink")，但一些裝置只允許在5GHz下使用40MHz頻寬，例如[MacBook](../Page/MacBook.md "wikilink")。如果用家購買一台只支援2.4GHz的150Mbps路由器，就只能使到72Mbps，即是20MHz頻寬下的最快速度。

## 802.11网络标准

## 参考文献

## 外部連結

  - [IEEE 802.11n-2009標準文件](http://standards.ieee.org/getieee802/download/802.11n-2009.pdf)

{{-}}

[Category:Wi-Fi](../Category/Wi-Fi.md "wikilink")
[Category:无线网络](../Category/无线网络.md "wikilink") [Category:IEEE
802.11](../Category/IEEE_802.11.md "wikilink")

1.