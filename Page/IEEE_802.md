**IEEE 802**
指[IEEE标准中关于](../Page/IEEE.md "wikilink")[局域网和](../Page/局域网.md "wikilink")[城域网的一系列标准](../Page/城域网.md "wikilink")。更确切的说，IEEE
802标准仅限定在传输可变大小数据包的网络。其中最广泛使用的有[以太网](../Page/以太网.md "wikilink")、[令牌环](../Page/令牌环.md "wikilink")、[无线局域网等](../Page/无线局域网.md "wikilink")。这一系列标准中的每一个子标准都由委员会中的一个专门工作组负责。

IEEE
802中定义的服务和协议限定在[OSI模型的最低两层](../Page/OSI模型.md "wikilink")（即物理层和数据链路层）。事实上，IEEE
802将OSI的数据链路层分为两个子层，分别是[逻辑链路控制](../Page/逻辑链路控制.md "wikilink")（LLC,
Logical Link Control）和[介质访问控制](../Page/介质访问控制.md "wikilink")（MAC, Media
Access Control），如下所示：

  - [数据链路层](../Page/数据链路层.md "wikilink")
      - [逻辑链路控制子层](../Page/逻辑链路控制.md "wikilink")
      - [介质访问控制子层](../Page/介质访问控制.md "wikilink")
  - [物理层](../Page/物理层.md "wikilink")

## 现有标准

  - [IEEE
    802.1](../Page/IEEE_802.1.md "wikilink")：[高层局域网协议](../Page/高层局域网协议.md "wikilink")（Bridging
    (networking) and Network Management）
  - [IEEE
    802.2](../Page/IEEE_802.2.md "wikilink")：[逻辑链路控制](../Page/逻辑链路控制.md "wikilink")（Logical
    link control）
  - [IEEE
    802.3](../Page/IEEE_802.3.md "wikilink")：[以太网](../Page/以太网.md "wikilink")（Ethernet）
  - [IEEE
    802.4](../Page/IEEE_802.4.md "wikilink")：[令牌总线](../Page/令牌总线.md "wikilink")（Token
    bus）
  - [IEEE
    802.5](../Page/IEEE_802.5.md "wikilink")：[令牌環](../Page/令牌環.md "wikilink")（Token-Ring）
  - [IEEE
    802.6](../Page/IEEE_802.6.md "wikilink")：[城域網](../Page/城域網.md "wikilink")（MAN,
    Metropolitan Area Network）
  - [IEEE
    802.7](../Page/IEEE_802.7.md "wikilink")：[宽带TAG](../Page/宽带TAG.md "wikilink")（Broadband
    LAN using Coaxial Cable）
  - [IEEE
    802.8](../Page/IEEE_802.8.md "wikilink")：[光纤分布式数据接口](../Page/光纤分布式数据接口.md "wikilink")（FDDI）
  - [IEEE
    802.9](../Page/IEEE_802.9.md "wikilink")：[综合业务局域网](../Page/综合业务局域网.md "wikilink")（Integrated
    Services LAN）
  - [IEEE
    802.10](../Page/IEEE_802.10.md "wikilink")：[局域网网络安全](../Page/局域网网络安全.md "wikilink")（Interoperable
    LAN Security）
  - [IEEE
    802.11](../Page/IEEE_802.11.md "wikilink")：[无线局域网](../Page/无线局域网.md "wikilink")（Wireless
    LAN & Mesh）
  - [IEEE
    802.12](../Page/IEEE_802.12.md "wikilink")：[需求优先级](../Page/需求优先级.md "wikilink")（Demand
    priority）
  - IEEE 802.13：（未使用）
  - [IEEE
    802.14](../Page/IEEE_802.14.md "wikilink")：[电缆调制解调器](../Page/电缆调制解调器.md "wikilink")（Cable
    modems）
  - [IEEE
    802.15](../Page/IEEE_802.15.md "wikilink")：[无线个人网](../Page/无线个人网.md "wikilink")（Wireless
    PAN）
      - [IEEE
        802.15.1](../Page/IEEE_802.15.1.md "wikilink")：[无线个人网络](../Page/无线个人网络.md "wikilink")（WPAN,
        Wireless Personal Area Network）
      - [IEEE
        802.15.4](../Page/IEEE_802.15.4.md "wikilink")：[低速无线个人网络](../Page/低速无线个人网络.md "wikilink")（LR-WPAN,
        Low Rate Wireless Personal Area Network）
  - [IEEE
    802.16](../Page/IEEE_802.16.md "wikilink")：[宽带无线接入](../Page/宽带无线接入.md "wikilink")（Broadband
    Wireless Access）
  - [IEEE
    802.17](../Page/IEEE_802.17.md "wikilink")：[彈性封包環傳輸技術](../Page/彈性封包環傳輸技術.md "wikilink")（Resilient
    packet ring）
  - [IEEE
    802.18](../Page/IEEE_802.18.md "wikilink")：[無線電管制技術](../Page/無線電管制技術.md "wikilink")（Radio
    Regulatory TAG）
  - [IEEE
    802.19](../Page/IEEE_802.19.md "wikilink")：[共存標籤](../Page/共存標籤.md "wikilink")（Coexistence
    TAG）
  - [IEEE
    802.20](../Page/IEEE_802.20.md "wikilink")：[移動寬頻無線接入](../Page/移動寬頻無線接入.md "wikilink")（Mobile
    Broadband Wireless Access）
  - [IEEE
    802.21](../Page/IEEE_802.21.md "wikilink")：[媒介獨立換手](../Page/媒介獨立換手.md "wikilink")（Media
    Independent Handover）
  - [IEEE 802.22](../Page/IEEE_802.22.md "wikilink")：無線区域網（Wireless
    Regional Area Network）
  - [IEEE
    802.23](../Page/IEEE_802.23.md "wikilink")：[紧急服务工作组](../Page/紧急服务工作组.md "wikilink")（Emergency
    Services Working Group），2010年3月新发布

## 另见

  - [计算机网络](../Page/计算机网络.md "wikilink")

## 外部連結

  - [IEEE 802 LAN/MAN Standards
    Committee](http://grouper.ieee.org/groups/802/)
  - [令牌环（token）协议详细资料](http://www.networkdictionary.com/chinese/protocols/token.php)

[IEEE_802](../Category/IEEE_802.md "wikilink")