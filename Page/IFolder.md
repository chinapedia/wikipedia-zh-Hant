**iFolder** 是一個由[Novell,
Inc.開發的](../Page/Novell.md "wikilink")[开放源代码軟體](../Page/开放源代码.md "wikilink")，目的是讓不同的[不同的操作系統在網絡上進行](../Page/跨平台.md "wikilink")[檔案分享](../Page/檔案分享.md "wikilink")。

iFolder
用了[目錄分享的概念](../Page/目錄.md "wikilink")，若有一個目錄標籤成要分享的目錄，目錄裡的內容就會和網絡內其他電腦進行同步，可以以[點對點技術或透過](../Page/點對點技術.md "wikilink")[伺服器傳送](../Page/伺服器.md "wikilink")。這要讓單一用戶和不同的電腦進行檔案同步或者與其他用戶分享檔案。

iFolder的核心是一個稱為[Simias的專案](../Page/Simias.md "wikilink")。Simias監察檔案的改變，把這些改變同步和控制目錄的存取控制。真實的iFolder客戶端（包括一個圖形介面客戶端和一個網頁介面客戶端）是獨立的程式和Simias
back-end 溝通。

## 歷史

2001年3月19日 Novell宣佈，iFolder 在2001年6月29日在[Windows
NT](../Page/Windows_NT.md "wikilink")／[2000平台和](../Page/Windows_2000.md "wikilink")[Novell
NetWare](../Page/Novell_NetWare.md "wikilink") 5.1還有後來的Novell NetWare
6.0，同時也可以透過網頁瀏覽器存取要分享的檔案。[1](http://www.novell.com/news/press/archive/2001/06/pr01064.html)

iFolder Professional Edition
2在2002年3月13日發行，一個月後，加入了對[Linux和](../Page/Linux.md "wikilink")[Solaris的支持](../Page/Solaris.md "wikilink")，同時讓[Windows
CE和](../Page/Windows_CE.md "wikilink")[Palm
OS透過網頁存取](../Page/Palm_OS.md "wikilink")。這個版本同時是為大機構內該過百萬用戶分享檔案而設計，也加入對系統管理員的報告支持。[2](http://www.novell.com/news/press/archive/2002/03/pr02021.html)

2004年3月22日，在收購了Linux軟件公司[Ximian和](../Page/Ximian.md "wikilink")[SUSE後](../Page/SUSE_Linux.md "wikilink")，Novell宣佈會把iFolder以
[GPL](../Page/GNU_General_Public_License.md "wikilink")
開源軟件專案形式發行。同時開源版本的iFolder會採用[Mono](../Page/Mono.md "wikilink")
Framework以簡化開發過程。

iFolder 3.0在2005年6月22日發行。

2006年3月31日，Novell宣佈把iFolder Enterprise Server的源碼開放。

## 外部連結

  - [Official
    site](https://web.archive.org/web/20130921180524/http://www.ifolder.com/)

[Category:檔案分享程式](../Category/檔案分享程式.md "wikilink")
[Category:Netware](../Category/Netware.md "wikilink")
[Category:跨平台軟體](../Category/跨平台軟體.md "wikilink")