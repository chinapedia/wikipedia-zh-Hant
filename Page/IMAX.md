[Imax.svg](https://zh.wikipedia.org/wiki/File:Imax.svg "fig:Imax.svg")
[London_IMAX_cinema_2.jpg](https://zh.wikipedia.org/wiki/File:London_IMAX_cinema_2.jpg "fig:London_IMAX_cinema_2.jpg")[倫敦的IMAX影院](../Page/倫敦.md "wikilink")-BFI
IMAX\]\]
[HST_3D_IMAX_Screening.jpg](https://zh.wikipedia.org/wiki/File:HST_3D_IMAX_Screening.jpg "fig:HST_3D_IMAX_Screening.jpg")

**IMAX**（全稱：**I**mage
**MAX**imum），意指最大影像，為一種能夠放映比傳統底片更大和更高解像度的[電影放映系統](../Page/電影.md "wikilink")。IMAX是大格式及需在特定場館播放的影像展示系統中最為成功的。

眾多IMAX系統中，有一種適合在傾斜的天文館圓頂播放的IMAX系統，稱為[全天域電影](../Page/全天域電影.md "wikilink")。

## 歷史

IMAX由三名[加拿大人發明](../Page/加拿大.md "wikilink")：、及。由於在1967年於[蒙特利爾舉行的](../Page/蒙特利爾.md "wikilink")[世界博覽會中](../Page/世界博覽會.md "wikilink")，他們的多投影機式大銀幕投影系統出現不少技術問題，促使他們設計單投影機、單攝影機式的新系統。

首部IMAX電影於1970年在[日本](../Page/日本.md "wikilink")[大阪舉行的世界博覽會中播放](../Page/大阪.md "wikilink")，而長期的IMAX系統則於1971年在[多倫多安裝](../Page/多倫多.md "wikilink")。

在1974年於[美國](../Page/美國.md "wikilink")[華盛頓州](../Page/華盛頓州.md "wikilink")[斯波坎舉行的世界博覽會中](../Page/斯波坎.md "wikilink")，美國館展出了一塊27.3
x
19.7米的巨型IMAX銀幕，觀眾向正前方觀看時，畫面足以充滿整個視界。共有5百萬人次觀看過，絕大部分觀眾認為它呈視了強烈的動感，少部分觀眾甚至產生暈船的現象。這個銀幕只展出了六個月，數年後，一塊標準尺寸的IMAX銀幕於Riverfront
Park IMAX Theatre安裝，並使用至今。

首個半球形銀幕的IMAX電影院於1973年，在美國[加里福尼亞州](../Page/加里福尼亞州.md "wikilink")[聖地牙哥](../Page/聖地牙哥_\(加利福尼亞州\).md "wikilink")[Balboa
Park的](../Page/Balboa_Park.md "wikilink")開幕。直至2003年5月，全球共有230間IMAX電影院，分布於34個國家，其中約半為商業電影院，另一半則為博物館等教育機構。

截至2015年10月，IMAX戲院在全球達到1000個影廳，分布在40个国家和地区。在发达的国家和地区，建造一个IMAX影院的成本是100万美元至150万美元。由于建造费用昂贵，电影院的投资者往往与电影发行商预先谈好或者预先签订今后若干时期内的IMAX电影放映合同，然后才投资和动工兴建IMAX影院。

## 技術规格

### 攝影機

[IMAX_camera_1.jpg](https://zh.wikipedia.org/wiki/File:IMAX_camera_1.jpg "fig:IMAX_camera_1.jpg")

IMAX为了增强图像需要一个更大的电影的分辨率，而35毫米胶片的摄影机提供的水平分辨率约为6000，一个IMAX摄影机提供的水平分辨率为18000。

### IMAX 3D数字摄影机

3D digital camera，即IMAX公司於2011年推出的「Phantom 65 IMAX 3D
camera」，為4K雙鏡頭3D攝影機，畫面比例1.9:1。

[麥可·貝的](../Page/麥可·貝.md "wikilink")《[變形金剛4：絕跡重生](../Page/變形金剛4：絕跡重生.md "wikilink")》是首部採用數位IMAX
3D攝影機拍攝的電影，其余画面使用一般數位攝影機和35mm底片拍攝。
在系列第五部《[變形金剛5：最終騎士](../Page/變形金剛5：最終騎士.md "wikilink")》中，[麥可·貝首度採用IMAX](../Page/麥可·貝.md "wikilink")
3D技术（即同时使用两台數位IMAX 3D攝影機交叠拍攝）实时拍攝了大量的3D画面。

### IMAX 2D数字摄影机

2D digital camera，即2015年推出的和Arri共同研發的「Arri Alexa
65」，為4K解析度攝影機，畫面比例2.1:1。雖然聽名字感覺比3D要低一個等級，但實際上2D
IMAX使用的成本遠遠低於3D，猶如在家裡用電腦房一個影片一般一樣輕鬆容易、但畫質卻能保持不變甚至更好。

《[美國隊長3：英雄內戰](../Page/美國隊長3：英雄內戰.md "wikilink")》：首次使用數位IMAX 2D攝影機拍攝。

《[复仇者联盟3：无限战争](../Page/复仇者联盟3：无限战争.md "wikilink")》：首部全片使用數位Arri Alexa
IMAX攝影機拍攝的商业电影。映前会播放IMAX全畫幅特制倒计時。

### 底片

[IMAX_Film.JPG](https://zh.wikipedia.org/wiki/File:IMAX_Film.JPG "fig:IMAX_Film.JPG")

IMAX為了大幅增加影像的解像度，採用了特別的70毫米底片。傳統65毫米底片的影像尺寸為48.5毫米×22.1毫米（），IMAX底片的影像尺寸69.6毫米×48.5毫米。為了以標準電影的每秒24格來曝光，IMAX底片的進片速度是一般底片的三倍。

要投影大片幅底片有一定的技術困難——傳統70毫米底片在放大586倍後不夠穩定。因此IMAX需要一些新技術，IMAX公司的威廉肖採用了一項澳洲的名為「滾動循環」的專利技術用於影片放映，主要是增加了一個壓縮空氣裝置來加速底片滾動，並將一個圓柱形鏡頭置於放映機前端，在放映過程中保持真空狀態。IMAX放映機用螺釘固定，四顆螺釘與齒輪將放映機固定在完全水-{平}-的狀態。還通過增加凸輪控制臂來抵消放映過程中的細微晃動。放映機的[快門長度也比傳統設備長大约](../Page/快門.md "wikilink")20%，燈管的亮度更高，最大的12-18[千瓦](../Page/千瓦.md "wikilink")[氙氣](../Page/氙氣.md "wikilink")-[弧光燈甚至需要水冷的](../Page/弧光燈.md "wikilink")[電極](../Page/電極.md "wikilink")。因此IMAX系統造價並不便宜，而且重量達1.8[公噸](../Page/公噸.md "wikilink")。

IMAX的片基為[柯達的ESTAR](../Page/柯達.md "wikilink")（即[PET](../Page/PET.md "wikilink")，亦即杜邦化工的），原因並非在片基的強度，而在於此片基不會被顯像的化學液影響了尺寸，IMAX的進片裝置對底片邊孔或厚度的變化有嚴格的要求，IMAX底片可歸類為「15/70」底片，意指這種70毫米底片每格有15個邊孔。IMAX的底片非常笨重，底片盤比一般電影的大得多。

### 声音系统

IMAX底片為了盡用底片面積而沒有[聲軌](../Page/聲軌.md "wikilink")，它採用了六聲道35毫米磁帶播放與畫面同步的聲音。在1990年代前期，與畫面分離的數碼化六聲道聲源改為以更精確的脈衝產生器作為傳統[SMPTE時碼的同步訊號源](../Page/SMPTE.md "wikilink")。這是日後[杜比數碼及](../Page/杜比數碼.md "wikilink")[DTS等電影院多聲道系統的前身](../Page/DTS.md "wikilink")。

IMAX繼續發展，出現了新的表現手法，例如立體影像及每秒高達48格的畫格數。音響系統方面則有Sonic-DDP（Direct Disc
Playback，無壓縮LPCM環繞聲軌）、立體音響系統及呈橢圓形分布的揚聲器群。

2015年，IMAX公司因應[Dolby等音效大廠推出多聲道系統](../Page/Dolby.md "wikilink")，推出名為「IMAX
12 track」的13聲道（12獨立聲道和一個超重低音）多聲道聲音系統抗衡。

#### 亚洲12聲道音響系統影院分布

亚洲IMAX数字 12聲道音響系統影院

<table>
<thead>
<tr class="header">
<th><p>影城名称</p></th>
<th><p>放映系统</p></th>
<th><p>開幕時間</p></th>
<th><p>寬度</p></th>
<th><p>高度</p></th>
<th><p>面積</p></th>
<th><p>座位数</p></th>
<th><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/東京都.md" title="wikilink">東京都</a><a href="../Page/新宿.md" title="wikilink">新宿</a>-TOHO Cinemas Shinjuku</p></td>
<td><p>IMAX数字</p></td>
<td><p>2015年4月17日</p></td>
<td><p>19.5</p></td>
<td><p>10.9</p></td>
<td><p>212.55</p></td>
<td><p>311</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/東京都.md" title="wikilink">東京都</a><a href="../Page/世田谷區.md" title="wikilink">世田谷區</a>-109 Cinemas Futakotamagawa</p></td>
<td><p>IMAX数字</p></td>
<td><p>2015年4月24日</p></td>
<td><p>18.5</p></td>
<td><p>9.9</p></td>
<td><p>183.15</p></td>
<td><p>224</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/神奈川県.md" title="wikilink">神奈川県</a><a href="../Page/横浜市.md" title="wikilink">横浜市</a>-TOHO Cinemas LaLaport Yokohama</p></td>
<td><p>IMAX数字</p></td>
<td><p>2016年12月16日</p></td>
<td></td>
<td></td>
<td></td>
<td><p>373</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大阪府.md" title="wikilink">大阪府</a><a href="../Page/大阪市.md" title="wikilink">大阪市</a><a href="../Page/中央区_(大阪市).md" title="wikilink">中央区</a>-TOHO Cinemas Namba main building</p></td>
<td><p>IMAX数字</p></td>
<td><p>2016年12月16日</p></td>
<td></td>
<td></td>
<td></td>
<td><p>455</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p>香港-BEA IMAX @ <a href="../Page/娱艺.md" title="wikilink">UA</a> <a href="../Page/MegaBox.md" title="wikilink">MegaBox</a></p></td>
<td><p>IMAX数码</p></td>
<td><p>2007年6月-2010年（IMAX MPX）<br />
2010-2017年7月11日（IMAX数码 5.1聲道音響系統）<br />
2017年7月11日（IMAX数码 12聲道音響系統）</p></td>
<td><p>17.7[1]</p></td>
<td><p>11.36</p></td>
<td><p>201.072</p></td>
<td><p>2D：260<br />
3D：198</p></td>
<td><p>12聲道音響系統<br />
IMAX中國第一家IMAX数码 12聲道音響系統影院</p></td>
</tr>
<tr class="even">
<td><p>上海-幸福蓝海国际影城(龙湖宝山天街IMAX店)</p></td>
<td><p>IMAX数字</p></td>
<td><p>2017年12月16日</p></td>
<td><p>25.44</p></td>
<td><p>13.23</p></td>
<td><p>336.5712</p></td>
<td><p>288</p></td>
<td><p>12聲道音響系統<br />
IMAX中國第二家IMAX数字 12聲道音響系統影院</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新加坡.md" title="wikilink">新加坡</a>-Shaw Theatres Lido IMAX</p></td>
<td><p>IMAX数字</p></td>
<td><p>2018年1月24日</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/東京都.md" title="wikilink">東京都</a><a href="../Page/千代田区.md" title="wikilink">千代田区</a>-TOHOシネマズ日比谷</p></td>
<td><p>IMAX数字</p></td>
<td><p>2018年3月29日</p></td>
<td></td>
<td></td>
<td></td>
<td><p>341</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

亚洲IMAX激光 12聲道音響系統影院

<table>
<thead>
<tr class="header">
<th><p>影城名称</p></th>
<th><p>放映系统</p></th>
<th><p>開幕時間</p></th>
<th><p>寬度</p></th>
<th><p>高度</p></th>
<th><p>面積</p></th>
<th><p>座位数</p></th>
<th><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>迪拜VOX Cinemas &amp; IMAX</p></td>
<td><p>IMAX激光</p></td>
<td><p>2015年9月28日</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p>吹田市109 Cinemas Osaka Expo City</p></td>
<td><p>IMAX激光</p></td>
<td><p>2015年11月19日</p></td>
<td><p>26</p></td>
<td><p>18</p></td>
<td><p>468</p></td>
<td><p>407</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p><a href="http://www.wandafilm.com/?cityCode=5396544539">东莞万达影城(华南MALL店)</a></p></td>
<td><p>IMAX激光</p></td>
<td><p>2007年5月1日(IMAX GT)<br />
2013年11月20日(IMAX数字)<br />
2015年11月25日(IMAX激光)</p></td>
<td><p>27.6</p></td>
<td><p>21</p></td>
<td><p>579.6</p></td>
<td><p>503</p></td>
<td><p>12聲道音響系統<br />
IMAX中國旗下第9家IMAX影院</p></td>
</tr>
<tr class="even">
<td><p><a href="http://film.eontime.com.cn/">哈尔滨泰莱时代影城</a></p></td>
<td><p>IMAX激光</p></td>
<td><p>2015年2月1日(IMAX数字)<br />
2016年1月9日(IMAX激光)</p></td>
<td><p>28.65</p></td>
<td><p>21.30</p></td>
<td><p>610.245</p></td>
<td><p>539</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p><a href="http://www.lnkjg.cn/">辽宁科技馆</a></p></td>
<td><p>IMAX激光</p></td>
<td><p>2015年4月29日（IMAX GT）<br />
2016年3月12日（IMAX激光）</p></td>
<td><p>29</p></td>
<td><p>22</p></td>
<td><p>638</p></td>
<td><p>596</p></td>
<td><p>12聲道音響系統<br />
（停止放映IMAX科普影片，现放映IMAX激光版本科普影片）</p></td>
</tr>
<tr class="even">
<td><p>台北<a href="../Page/美麗華大直影城.md" title="wikilink">美麗華大直影城</a>（ Da-Zhi Cinema IMAX）</p></td>
<td><p>IMAX雷射</p></td>
<td><p>2004年11月19日－2016年2月15日(IMAX GT)<br />
2013年11月－2016年2月15日(IMAX數位)<br />
2016年3月24日(IMAX雷射)</p></td>
<td><p>28.80</p></td>
<td><p>21.16</p></td>
<td><p>609</p></td>
<td><p>404</p></td>
<td><p>於影城架構時期即設計為IMAX影廳<br />
於2013年11月安装IMAX数位放映系统并正式开启IMAX与IMAX数位同时放映时代<br />
於2016年2月15日同时停止IMAX与IMAX數位放映，新安装IMAX雷射數位放映系統<br />
於2016年3月24日正式开始IMAX雷射數位放映系統放映，同时有原有的5.1聲道音響系統升级为12聲道音響系統<br />
由于影院股东内斗2018年6月22起停業1個月<br />
2018年7月27起重新试营业<br />
2018年8月8日正式重新开幕</p></td>
</tr>
<tr class="odd">
<td><p>昆明LCC光魔激光影城</p></td>
<td><p>IMAX激光</p></td>
<td><p>2016年6月8日(IMAX激光)</p></td>
<td><p>23.1</p></td>
<td><p>17.37</p></td>
<td><p>401.247</p></td>
<td><p>363</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p>多哈 Novo Mall of Qatar &amp; IMAX</p></td>
<td><p>IMAX激光</p></td>
<td><p>2017年1月1日</p></td>
<td><p>28.7</p></td>
<td><p>15.9</p></td>
<td><p>456.33</p></td>
<td></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p>首尔 CGV YONGSAN I'PARK mall</p></td>
<td><p>IMAX激光</p></td>
<td><p>2017年7月19日[2]</p></td>
<td><p>31</p></td>
<td><p>22.4</p></td>
<td><p>694.4</p></td>
<td><p>624</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p>薩利米耶 IMAX, The Scientific Center Kuwait</p></td>
<td><p>IMAX GT<br />
IMAX激光</p></td>
<td><p>2017年10月23日</p></td>
<td><p>20</p></td>
<td><p>14.8</p></td>
<td><p>296</p></td>
<td></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p>贵阳越界影城（未来方舟店）</p></td>
<td><p>IMAX激光</p></td>
<td><p>2018年9月30日</p></td>
<td><p>32.16</p></td>
<td><p>18.17</p></td>
<td><p>584.3472</p></td>
<td><p>714</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p>迪拜 Novo IMG Dubai &amp; IMAX</p></td>
<td><p>IMAX激光</p></td>
<td><p>2019年2月14日</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p>東京都豐島區 キュープラザ池袋</p></td>
<td><p>IMAX激光</p></td>
<td><p>预计2019年7月</p></td>
<td><p>25.849</p></td>
<td><p>18.91</p></td>
<td><p>488.80459</p></td>
<td></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p>襄阳华夏东宝影城（襄悦城店）</p></td>
<td><p>IMAX激光</p></td>
<td><p>预计2022年</p></td>
<td><p>28</p></td>
<td><p>17</p></td>
<td><p>476</p></td>
<td></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

亚洲新一代IMAX激光 12聲道音響系統影院

<table>
<thead>
<tr class="header">
<th><p>影城名称</p></th>
<th><p>放映系统</p></th>
<th><p>開幕時間</p></th>
<th><p>寬度</p></th>
<th><p>高度</p></th>
<th><p>面積</p></th>
<th><p>座位数</p></th>
<th><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>上海寰映影城（大融城店)</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2018年6月29日</p></td>
<td><p>25.88</p></td>
<td><p>13.46</p></td>
<td><p>348.345</p></td>
<td><p>300</p></td>
<td><p>12聲道音響系統<br />
為中國第一家新一代IMAX激光放映系统影院[3]</p></td>
</tr>
<tr class="even">
<td><p>巴科奧爾Vista Cinemas at Evia Lifestyle Center</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2018年11月22日[4]</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/川崎市.md" title="wikilink">川崎市</a>109 Cinemas Kawasaki</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2018年11月23日[5]</p></td>
<td><p>17.4</p></td>
<td><p>9</p></td>
<td><p>156.6</p></td>
<td><p>458</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/名古屋市.md" title="wikilink">名古屋市</a>109 Cinemas Nagoya</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2018年11月23日[6]</p></td>
<td><p>17.1</p></td>
<td><p>8.8</p></td>
<td><p>155.76</p></td>
<td><p>407</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p>淄博齐纳国际影城（银座店）</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2018年12月23日</p></td>
<td><p>22.04</p></td>
<td><p>11.55</p></td>
<td><p>254.562</p></td>
<td><p>317</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p>兰州万达影城（城关万达广场店）</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2014年10月24日<br />
2018年12月26日</p></td>
<td><p>21.60</p></td>
<td><p>11.39</p></td>
<td><p>246.024</p></td>
<td><p>410</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/威秀影城.md" title="wikilink">花蓮新天堂樂園威秀影城</a></p></td>
<td><p>新一代IMAX雷射</p></td>
<td><p>2018年12月26日試營運<br />
2018年12月29日開幕</p></td>
<td><p>21.5</p></td>
<td><p>12.04</p></td>
<td><p>258.86</p></td>
<td><p>368</p></td>
<td><p>12聲道音響系統<br />
於影城架構時期即設計為IMAX影廳</p></td>
</tr>
<tr class="even">
<td><p>柳州万达影城（城中万达广场店）</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2015年11月27日<br />
2018年12月27日</p></td>
<td><p>22.13</p></td>
<td><p>11.58</p></td>
<td><p>256.2654</p></td>
<td><p>362</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p>兰州空间站影城（兰州中心店）</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2018年12月28日</p></td>
<td><p>21.98</p></td>
<td><p>11.35</p></td>
<td><p>249.473</p></td>
<td><p>441</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p>太原万达影城（龙湖万达广场店）</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2015年9月30日<br />
2018年12月30日</p></td>
<td><p>26.11<br />
26.51</p></td>
<td><p>14.06<br />
14.06</p></td>
<td><p>367.1066<br />
372.7306</p></td>
<td><p>430</p></td>
<td><p>12聲道音響系統<br />
IMAX公司全球第1000家IMAX®影院</p></td>
</tr>
<tr class="odd">
<td><p>上海万达影城（宝山万达广场店）</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2012年6月29日<br />
2017年3月21日<br />
2019年1月4日</p></td>
<td><p>19.12<br />
20.05<br />
20.05</p></td>
<td><p>10.05<br />
10.488<br />
10.48</p></td>
<td><p>192.156<br />
210.2844<br />
210.124</p></td>
<td><p>241<br />
229</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p>郑州万达影城（二七万达广场店）</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2012年10月13日<br />
2019年1月4日</p></td>
<td><p>23.21</p></td>
<td><p>11.70</p></td>
<td><p>271.557</p></td>
<td><p>470<br />
423</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p>广州美亚影城（云门店）</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2019年2月15日</p></td>
<td><p>23.55</p></td>
<td><p>12.31</p></td>
<td><p>289.9005</p></td>
<td><p>349</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p>呼和浩特万达影城（万达广场店）</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2011年11月5日<br />
2019年4月5日</p></td>
<td><p>21.24</p></td>
<td><p>10.87</p></td>
<td><p>230.8788</p></td>
<td><p>398</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p>南宁万达影城（青秀万达广场店）</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2014年12月17日<br />
2019年4月14日</p></td>
<td><p>22.76</p></td>
<td><p>11.67</p></td>
<td><p>265.6092</p></td>
<td><p>412</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p>贵阳万达影城（中大广场店）</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2013年12月20日<br />
预计2019年4月24日</p></td>
<td></td>
<td></td>
<td></td>
<td><p>281</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p>新加坡 Shaw Theatres Jewel</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>预计2019年4月24日</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/久喜市.md" title="wikilink">久喜市</a>109 Cinemas Shobu</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2009年6月19日[7]<br />
预计2019年4月26日</p></td>
<td><p>18.4</p></td>
<td><p>9.8</p></td>
<td><p>180.32</p></td>
<td><p>349</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p>大连万达影城(经开万达广场店)</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2015年8月29日<br />
预计2019年</p></td>
<td></td>
<td></td>
<td></td>
<td><p>362</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p>包头万达影城（青山万达广场店）</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2011年11月10日<br />
预计2019年</p></td>
<td></td>
<td></td>
<td></td>
<td><p>314</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p>绵阳万达影城（涪城万达广场店）</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2012年12月21日<br />
预计2019年</p></td>
<td></td>
<td></td>
<td></td>
<td><p>347</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p>宁波万达影城（鄞州万达广场店）</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2011年7月21日<br />
预计2019年</p></td>
<td></td>
<td></td>
<td></td>
<td><p>336</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p>北京万达影城（首创奥特莱斯店）</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>预计2019年5月1日</p></td>
<td><p>22.85</p></td>
<td><p>11.76</p></td>
<td><p>268.716</p></td>
<td></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p>沖縄県浦添市-ユナイテッド・シネマ PARCO CITY 浦添</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>预计2019年7月</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p>北京万达影城（通州万达广场店）</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2014年11月29日<br />
预计2019年8月</p></td>
<td><p>22.36</p></td>
<td><p>11.70</p></td>
<td><p>261.612</p></td>
<td><p>424</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p>北京万达影城（石景山万达广场店）</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2009年1月1日<br />
预计2019年12月</p></td>
<td><p>21.3</p></td>
<td><p>12.6</p></td>
<td><p>268</p></td>
<td><p>433</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p>天津万达影城（河东万达广场店）</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2011年11月8日<br />
2017年3月20日<br />
预计2019年</p></td>
<td><p>23.77</p></td>
<td><p>12.34</p></td>
<td><p>293.3218</p></td>
<td><p>398</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="even">
<td><p>石家庄万达影城（裕华万达广场店）</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2011年9月23日<br />
2017年3月22日<br />
预计2019年</p></td>
<td><p>23.21</p></td>
<td><p>11.95</p></td>
<td><p>277.3595</p></td>
<td><p>385</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td><p>长沙万达影城(解放路悦荟广场店)</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2008年7月10日<br />
预计2019年</p></td>
<td><p>22.42</p></td>
<td><p>11.51</p></td>
<td><p>258.0542</p></td>
<td><p>430</p></td>
<td><p>12聲道音響系統<br />
IMAX中國第8家IMAX影院</p></td>
</tr>
<tr class="even">
<td><p>青岛万达影城(CBD万达广场店)</p></td>
<td><p>新一代IMAX激光</p></td>
<td><p>2011年2月1日<br />
2017年9月7日<br />
预计2019年</p></td>
<td><p>21.93<br />
21.20</p></td>
<td><p>10.10<br />
10.997</p></td>
<td><p>221.493<br />
233.1364</p></td>
<td><p>374</p></td>
<td><p>12聲道音響系統</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### IMAX放映机

[Xenon_IMAX_1.jpg](https://zh.wikipedia.org/wiki/File:Xenon_IMAX_1.jpg "fig:Xenon_IMAX_1.jpg")
IMAX放映机重达1.8吨和超过178厘米高195厘米长。

IMAX公司已经发布了使用其15格，70毫米胶片格式以四个投影机类型：GT（大剧院），GT
3D（双转子），SR（小转子）和MPX（这是专为改装剧院）。

### IMAX數位放映机

Digital
IMAX，在2008年7月，IMAX公司推出了數位投影系统，投影機以[DLP技術支持](../Page/DLP.md "wikilink")，數位IMAX解析度為2K，與一般數位投影相同畫質，因其解析度相較膠卷IMAX低階非常之多。為了保障電影畫面不因放大投影巨型螢幕而使畫面清晰度降低，數位IMAX銀幕都縮小不少，甚至銀幕僅僅8公尺高（臺灣高雄大遠百威秀數位IMAX），IMAX原始的「巨型銀幕、超高畫質」兩樣皆失。

數位IMAX的優點主要在成本方面，因为免除了70毫米的笨重底片，放映机和放映室的体积都大大地减少。畫面亮度仅有略微提高，但是分辨率大大地降低。

### IMAX激光放映机

於2015年，IMAX公司推出改善數位IMAX系統令人詬病的解析度、色域的解決方案—雷射投影技術，采用全新的12声道音响系统，全新的IMAX
3D眼镜采用杜比3D系统。雷射投影技術名為「IMAX WITH LASER」。IMAX WITH
LASER可放映畫面和IMAX相同為1.44:1之比例，改善數位IMAX受到硬體限制的裁切情況；解析度方面為4K解析度，儘管不如IMAX的8K\~18K，但相較數位IMAX的2K解析度已有相當程度的改善。

### 新一代IMAX激光放映机

在2018年4月24日，IMAX宣布他们将在今年晚些时候开始推出新的单机版激光放映系统，此次迭代旨在取代用于 1.90:1
屏幕的IMAX氙氣燈数字放映系统。与IMAX激光放映系统不同的是，新一代IMAX激光放映系统采用单机双镜头放映，所能提供的最大画幅为
1.90:1，IMAX 3D眼镜采用圆偏振3D系统。

新一代IMAX激光放映系统是IMAX为商业多厅式影院全新研发的新一代激光放映系统。

该系统是IMAX公司巨资投入的研发项目，从设计之初便为IMAX银幕量身打造，其与众不同的体验源于新一代4K激光放映系统，拥有崭新的光学引擎和多项IMAX专有技术，使更高的分辨率，更清晰、更明亮的画面，更强烈的对比度和更鲜明、绚丽的色彩汇聚于IMAX之上。

2018年8月31日，全球首套新一代IMAX激光放映系统正式落户上海寰映影城大融城店。

### 影廳設計

IMAX的構造亦與普通電影院有很大分別。由於畫面解析度提高，觀眾可以更靠近銀幕，基本上IMAX設計為座位第一排至最後一排的長度均在一個銀幕的高度内（傳統影院座位跨度可達到8－12個銀幕倍數），但此設計不適用於部分數位IMAX、部分一般影廳改建之數位IMAX影廳。此外，座位傾斜度亦較大（在半球形銀幕的放映室可傾斜達23度），便觀眾能夠面向銀幕中心。

普遍认为IMAX銀幕為22米×16.1米（72英尺×53英尺），但可以在更大的銀幕播放。世界上最大的IMAX銀幕為[悉尼](../Page/悉尼.md "wikilink")[達令港](../Page/達令港.md "wikilink")，銀幕为35.72米×29.57米（117英尺x97英尺）（已经拆除重新改造为IMAX
激光放映系统）。

IMAX数字、IMAX激光、新一代IMAX激光影厅银幕尺寸根据场地实际情况建造。没有统一银幕尺寸标准，

## 技术变革

### IMAX Dome

**IMAX
Dome**（舊稱**OMNIMAX**，中譯**[全天域電影](../Page/全天域電影.md "wikilink")**）是用作投影於[天文館這一類傾斜的半球形銀幕的](../Page/天文館.md "wikilink")。此系統採用一[魚眼鏡頭拍攝](../Page/魚眼鏡頭.md "wikilink")，使180度的景物能成像於平坦的底片上。投影時再採用另一魚眼鏡頭即可讓全景重現銀幕。IMAX
Dome水平投影角達180度，而垂直則為122度，其中22度低於投影機水平線，100度高於投影機水平線，這是配合傾斜半球形銀幕而設的。有部分天文館（例如[香港太空館](../Page/香港太空館.md "wikilink")）採用沒有傾斜的半球銀幕，因此影像總是高於投影機水平線，而觀眾的座椅則較一般影院更為後傾。

### IMAX 3D

[3d_glasses.jpg](https://zh.wikipedia.org/wiki/File:3d_glasses.jpg "fig:3d_glasses.jpg")
立體版本的IMAX技術。為營造出立體景深，IMAX 3D採用了雙攝影機及雙投映機拍攝及放映。

目前IMAX
3D放映時採用[偏振光式放映](../Page/偏振光.md "wikilink")，觀看時以配戴偏光眼鏡來分析立體影像。多數IMAX
3D影院採用線偏振鏡片。而另外有的公司採用的是圓偏振眼镜，圓偏振眼睛受眼鏡旋轉角度影響較小，當觀眾歪頭時基本不影響效果，而不像線偏振眼鏡在觀眾歪頭時會出現立體畫面的虛影。

### IMAX HD (48格/秒)

新版的IMAX技術，幀速率由原本24格/秒增加至48格/秒，以減少畫面閃爍問題。於1992年的[西班牙](../Page/西班牙.md "wikilink")[世界博覽會發表](../Page/1992年世界博覽會.md "wikilink")。但因為拍攝成本比一般的多出一倍，至2000年被放棄使用。

因2008年數位IMAX的產生，使得IMAX HD (48格/秒)不再遙不可及。2012年底的《哈比人：意外旅程》為IMAX首部DMR
48格/秒的數位IMAX電影。

### IMAX数字

由于70毫米胶片和投影仪价格昂贵且难以大规模生产，并且由于放置全尺寸IMAX屏幕的放映厅的尺寸使其构造成本高昂，因此IMAX于2008年推出数字投影系统，以使用更短的1.90：1宽高比屏幕。它使用两个2K分辨率的投影仪，可以在DCI中呈现2D或3D内容或IMAX数字格式（IDF）（本身就是DCI的超集）。数字装置引起了一些争议，因为在仅用IMAX数字投影仪改装标准礼堂后，许多影院都将其屏幕标记为IMAX。这些放映厅的屏幕尺寸远小于原始15/70
IMAX格式的专用放映厅，并且仅限于1.90：1的宽高比。另一个缺点是数字IMAX的分辨率要低得多。与传统的IMAX
70mm投影相比，该技术的最大感知分辨率为2.9K，估计分辨率为12K。一些评论家还指出，许多非IMAX影院正以4K分辨率投影电影通过竞争品牌，如杜比影院和UltraAVX。

IMAX在各种基础技术和屏幕尺寸方面坚持“IMAX体验”的统一品牌。、有些人批评该公司的营销方式，其格式被称为“Lie-MAX”。该公司为该格式辩护说，它具有比标准影院更大的屏幕，更亮的画面和更好的声音。尽管与数字IMAX存在差异，但这种具有成本效益的格式有助于公司的全球增长，特别是在俄罗斯和中国。

### IMAXShift

2016年5月，IMAX宣布推出IMAXShift，这是一种多媒体室内自行车概念，但决定于2017年6月4日停止使用。

### IMAX VR

2016年5月20日，[谷歌和IMAX在谷歌年度开发者大会上宣布](../Page/谷歌.md "wikilink")，两家公司将共同开发一款影院级虚拟现实（VR）摄影机，即IMAX®
VR 摄影机，让当今顶尖的大导演和内容创作者们可以为全世界观众呈现最高质量的3D
360度内容体验。IMAX工程师团队和摄影机专家们将与谷歌团队一起从头设计这款全新的高分辨率摄影机，借助IMAX的顶尖影像捕捉技术。这款摄影机的研发将能够最大化的利用谷歌Jump这个创作和观看3D
360度视频的平台，为用户提供最高质量、最具有沉浸感的VR内容。

2016年9月1日，IMAX宣布计划将虚拟现实技术纳入IMAX影院体验，并在洛杉矶开设新的VR中心，使用由宏碁创建的全新StarVR耳机。

## 电影

#### IMAX DMR（數位媒體修復）

**D**igital
**R**e-**M**astering，可譯作數位原底翻版技術，IMAX為了推廣其影院系統，推出了一個DMR技術應用於更多商業片的發行。首先，製片公司把影片的底片拷貝或數位拷貝交給IMAX，然後IMAX以最高分辨率掃描每個畫格，再對每幅畫面進行銳化以及降噪，最後沖印在70毫米底片上。

在2002年秋，IMAX與[環球影片公司聯手推出](../Page/環球影片公司.md "wikilink")《*[阿波罗13號](../Page/阿波罗13号_\(電影\).md "wikilink")》*（1995）的IMAX版本，是首次利用IMAX獨家的"DMR"重新製作技術將傳統電影轉換成IMAX格式。其他一些已經公映的影片如《[星球大战](../Page/星球大战.md "wikilink")》等也相繼利用DMR技術處理被搬上IMAX。由於技術上的制約，早期DMR處理的影片長度最長不過2小時。在2003年，《[-{zh-hans:黑客帝国2：重装上阵;zh-hk:廿二世紀殺人網絡2：決戰未來;zh-tw:駭客任務2：重裝上陣;}-](../Page/黑客帝国2.md "wikilink")》突破了這一限制，成為IMAX發展歷程中的重要里程碑。在2003年末，續集《[-{zh-hans:黑客帝国3：矩阵革命;zh-hk:廿二世紀殺人網絡3：驚變世紀;zh-tw:駭客任務3：最後戰役;}-](../Page/黑客帝国3.md "wikilink")》成為首部在IMAX和傳統影院同步上映的影片。

## 娱乐

雖然從技術角度來看，IMAX是相當出色的電影格式，但在過去二十年中一直以來它並未能普及，而在近五年內IMAX呈現爆發性的增長。製作與播放IMAX的費用與運送困難使它的播放時間較普通電影為短（一般為40分鐘，近年亦有90分鐘的IMAX影片出現），題材大多為適合於天文館等科普機構播放的[紀錄片](../Page/紀錄片.md "wikilink")。IMAX攝影機曾於[太空梭](../Page/太空梭.md "wikilink")、[喜瑪拉雅山](../Page/喜瑪拉雅山.md "wikilink")、[大西洋海底及](../Page/大西洋.md "wikilink")[南極洲拍攝過](../Page/南極洲.md "wikilink")。

部分IMAX影院亦會利用傳統放映設備播放普通電影，以增加客源。1990年代後期，開始一股增加IMAX娛樂元素的風潮，開攝了一些純娛樂的題材，例如《[絕地暴龍](../Page/絕地暴龍.md "wikilink")》（1998）、《[Haunted
Castle](../Page/Haunted_Castle.md "wikilink")》（2001）（兩部皆為IMAX
3D電影）。1999年，[迪士尼製作了](../Page/迪士尼.md "wikilink")《[幻想曲2000](../Page/幻想曲2000.md "wikilink")》——首部正常長度的IMAX動畫（後來亦推出了傳統戲院版本）。迪士尼在2003年末還發行了首部真人演出的IMAX影片《[黑神駒前傳](../Page/黑神駒前傳.md "wikilink")》。

著名導演[詹姆斯·卡梅隆曾執導了一部關於](../Page/詹姆斯·卡梅隆.md "wikilink")《[鐵達尼號](../Page/鐵達尼號.md "wikilink")》的
IMAX 3D 格式電影《》，而在2009年執導了另一部IMAX
3D格式電影《[阿凡達](../Page/阿凡達.md "wikilink")》，引起各界對
IMAX 3D 技術的興趣。

截至2002年，共有8部IMAX電影被提名奥斯卡獎，其中《老人與海》榮獲了2000年最佳動畫短片獎。

還有很多IMAX電影被重新製作成[高清晰度电视格式](../Page/HDTV.md "wikilink")，在[INHD頻道播放](../Page/INHD.md "wikilink")。

2018年9月6日，IMAX宣布了一项名为IMAX
Enhanced的新计划，该计划是与DTS联合推出的。这是一个认证和许可服务，旨在为用户通过家庭娱乐设备实现最高品质的4K图像及音频回放。该计划与索尼电子、索尼影业、派拉蒙影业和Sound
United等合作伙伴共同推出，为了被IMAX Enhanced认证接受，家庭影音设备厂商必须设计家庭影院设备“以满足由
IMAX、DTS工程师以及好莱坞领先的技术专家认证委员会设定的最高端音频和视频性能标准的精心规定”。这些增强型设备还内置了“IMAX模式”，将有效地实现观看的电影达到电影制作者最初的要实现的标准，所有这一切都可以在家中舒适地进行。IMAX和DTS正在与工作室和内容合作伙伴合作，以数字方式为IMAX模式重新打造热门电影和其他内容。IMAX纪录片《美丽星球》和《南太平洋之旅》将是第一款采用HDR10+格式及DTS:X音频规格的IMAX
Enhanced 4K UHD蓝光影碟，预计于2018年12月11日于北美发售。

### 部分镜头使用IMAX 70mm胶片摄影机拍摄的电影

近年已经有一些电影使用了IMAX摄影机拍摄部分场景，然而IMAX摄影机比普通摄影机更大、更重，更大的工作噪音也给对白录音带来了困难。\[8\]它的摄影机仅能装载30秒到2分钟的胶片\[9\]，并且它的胶片相对于标准的35mm胶片价格更加昂贵。\[10\]由于这些难题，至今还没有一部完整长度的电影完全使用IMAX摄影机拍摄。

《[黑暗騎士](../Page/黑暗騎士.md "wikilink")》：有共30分鐘場景用IMAX攝影機拍攝。這是“有史以來第一次，一個電影的主要場景是用IMAX攝影機拍攝的。”即使在黑暗騎士，諾蘭本來想拍攝的IMAX格式的電影，將它使用在形像上比較安靜的場景，他認為將會是有趣的。諾蘭說，他這是希望可以用IMAX拍攝整部影片：“如果你可以用IMAX攝影機珠穆朗瑪峰或外太空，你可以在一個故事中使用它”。

《[變形金剛：復仇之戰](../Page/變形金剛：復仇之戰.md "wikilink")》：一年後，導演麥可·貝的攝影靈感來自[黑暗騎士IMAX的運用也能用在](../Page/黑暗騎士.md "wikilink")《[變形金剛：復仇之戰](../Page/變形金剛：復仇之戰.md "wikilink")》。影片的聯合編劇[羅伯托·奧奇認為](../Page/羅伯托·奧奇.md "wikilink")，IMAX的畫面解析度、畫面比例延展度對於IMAX戲院的觀眾來說是另種定義的3D視覺，並補充說，在IMAX拍攝是比使用立體攝像機更加容易。最後影院的呈現，變形金剛:復仇之戰IMAX版本，包含十分鐘的IMAX鏡頭拍攝出來的共兩個半小時的電影頭。但他後來他拍攝的[變形金剛3部分場景使用](../Page/變形金剛3.md "wikilink")3D攝影，但沒有用IMAX攝影機。

《[不可能的任務：鬼影行動](../Page/不可能的任務：鬼影行動.md "wikilink")》：大約30分鐘的使用IMAX攝影機拍攝的鏡頭。導演布萊德柏德認為使用IMAX格式提供的比數位3D更能夠呈現明亮且更高品質的影像，這是投射在大銀幕上，而不需要專門的眼鏡即可享受到的臨場感。

《[黑暗騎士：黎明升起](../Page/黑暗騎士：黎明升起.md "wikilink")》：儘管華納兄弟多番說服導演克里斯多夫·諾蘭使用3D拍攝、3D放映蝙蝠俠第三部曲，但導演聲明不會選擇拍攝3D格式以及放映3D影像，並說，他打算把重點放在改善影像畫質規模和更長時間的使用IMAX格式。在好萊塢的記錄中，《[黑暗騎士：黎明升起](../Page/黑暗騎士：黎明升起.md "wikilink")》是目前出現IMAX攝影畫面時間最長的電影，在片中出現約一個小時（相比之下黑暗騎士多了30分鐘的IMAX鏡頭）。

《[星際迷航：暗黑無界](../Page/星際迷航：暗黑無界.md "wikilink")》：導演J·J·亞柏拉罕決定採用IMAX攝影機拍攝部分片段，總共約30分鐘，為影史首部IMAX拍攝並後製為IMAX
3D的商業電影。但因大量後製鏡頭因素，製作團隊最終小幅裁切原始1.44:1的畫面比，故70mm
IMAX版本播映時並沒有完直整全銀幕放映。導演[J·J·亞柏拉罕在其之後拍攝的](../Page/J·J·亞柏拉罕.md "wikilink")《[星際大戰七部曲：原力覺醒](../Page/星際大戰七部曲：原力覺醒.md "wikilink")》亦使用IMAX攝影機拍攝部分畫面。

《[飢餓遊戲：星火燎原
(電影)](../Page/飢餓遊戲：星火燎原_\(電影\).md "wikilink")》：導演決議在競技場內的所有畫面皆使用IMAX攝影機拍攝，導演[Francis
Lawrence表示](../Page/Francis_Lawrence.md "wikilink"):「IMAX將帶領觀眾如同到達另個世界，並且身歷其境體驗競技場的戰鬥」。

《[星際效應](../Page/星際效應.md "wikilink")》：導演[克里斯多福·諾蘭再次使用擅長的IMAX攝影機拍攝](../Page/克里斯多福·諾蘭.md "wikilink")，而[星際效應的IMAX片段長達](../Page/星際效應.md "wikilink")75分鐘，則創下目前影史最長的IMAX拍攝記錄。

《[星際大戰七部曲：原力覺醒](../Page/星際大戰七部曲：原力覺醒.md "wikilink")》：導演[J·J·亞柏拉罕二度使用IMAX攝影機拍攝部分畫面](../Page/J·J·亞柏拉罕.md "wikilink")，IMAX片段约5分鐘。

《[蝙蝠俠對超人：正義曙光](../Page/蝙蝠俠對超人：正義曙光.md "wikilink")》：部分畫面採用IMAX攝影機拍攝。

《[敦克爾克大行動](../Page/敦克爾克大行動.md "wikilink")》：導演[克里斯多福·諾蘭再次使用擅長的IMAX攝影機拍攝](../Page/克里斯多福·諾蘭.md "wikilink")，IMAX片段長達67分鐘。

《[星球大戰：最後的絕地武士](../Page/星球大戰：最後的絕地武士.md "wikilink")》：影片使用IMAX攝影機拍攝了部分畫面，但后期剪辑时没有采用，故最终影片上映时没有IMAX畫面。

《[登月先鋒](../Page/登月先鋒.md "wikilink")》：约10分鐘的月球畫面使用IMAX攝影機拍攝，全球仅发行IMAX激光版本，并未发行IMAX版本。

## 技術標準

**IMAX (15/70)**

  - 球面鏡頭組
  - 每格底片15孔
  - 水平下拉放映，從右至左（從觀者視角）
  - 每秒24格底片
  - 镜頭尺寸：2.772"英寸（70.41毫米）乘2.072"英寸（52.63毫米）
  - 投射尺寸：至少垂直距離鏡頭0.80"英寸，水平距離0.016"英寸
  - 縱橫比:1.43:1
  - DMR縱橫比:1.89:1、2.39:1

**IMAX Dome / OMNIMAX** 與IMAX相同，除了：

  - 特製魚眼鏡頭組
  - 鏡頭的光學中心高於水平基準線0.37"英寸
  - 橢圓形投射到拱頂銀幕，低於平面20度到高於平面110度之間為最佳觀賞角度。

## 著名的IMAX电影目录

  - 《》（1973）：第一部全天域电影。
  - 《》（1976）：票房第二高的IMAX电影（总票房8250万美元）。
  - 《》（1984）：参考《[大峡谷](../Page/大峡谷.md "wikilink")》。
  - 《》（1985）：
  - 《》（1985）：
  - 《[Everest
    (film)](../Page/Everest_\(film\).md "wikilink")》（1998）：票房最高的IMAX电影（全球总票房1.206亿美元，其中美加票房8440万美元）；参考[珠穆朗玛峰](../Page/珠穆朗玛峰.md "wikilink")。
  - 《》（1998）：
  - 《》（1998）：
  - 《[Cirque du Soleil: Journey of
    Man](../Page/Cirque_du_Soleil:_Journey_of_Man.md "wikilink")》（2000）：参考[太陽劇團](../Page/太陽劇團.md "wikilink")。
  - 《》(2000)：
  - 《》（2001）：参考[欧内斯特·沙克尔顿](../Page/Ernest_Shackleton.md "wikilink")。
  - 《》（2001）：
  - 《》（2002）：

## 著名的IMAX影廳

  - CINESPHERE：位於加拿大安大略省多倫多,首家IMAX影院，1971年5月31日落成（银幕高18.3米，宽24.4米）目前是加拿大唯一IMAX放映系统跟IMAX激光放映系统共有影城。IMAX公司全球第1家IMAX影院

  - ：位於[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[聖地牙哥](../Page/聖地牙哥_\(加利福尼亞州\).md "wikilink")
    ，首家IMAX球幕影院，1973年落成（银幕直径23.2米）。IMAX公司全球第2家IMAX影院

  - [洛克希德马丁IMAX影院](../Page/洛克希德马丁.md "wikilink")：位於[华盛顿特区](../Page/华盛顿特区.md "wikilink")[美国国家航空航天博物馆](../Page/美国国家航空航天博物馆.md "wikilink")，主要放映[科技和](../Page/科技.md "wikilink")[太空旅行类的影片](../Page/太空旅行.md "wikilink")（银幕高14米，宽23米）。IMAX公司全球第3家IMAX影院

  - ：位於[新泽西州](../Page/新泽西州.md "wikilink")[澤西城](../Page/澤西城.md "wikilink")，有世界上最大的IMAX球幕银幕（银幕直径27米）。

  - Samuel J. & Ethel LeFrak IMAX
    Theater：位於史密斯索宁协会的[美國自然史博物館](../Page/美國自然史博物館.md "wikilink")，主要放映[自然和](../Page/自然.md "wikilink")[历史类影片](../Page/历史.md "wikilink")（银幕高12.2米，宽18.3米）

  - [明尼苏达科学博物馆Omni](../Page/明尼苏达科学博物馆.md "wikilink")
    Theater：拥有一个双银幕系统，可以放映IMAX影片，和一个可旋转穹顶，放下后可放映全天域电影（银幕高20.1米，宽27.4米）。

  - [波士顿科技博物馆的](../Page/科学博物馆_\(波士顿\).md "wikilink")[Mugar Omni
    Theater](../Page/Mugar_Omni_Theater.md "wikilink")：（银幕直径21.9米）

  - 印度：（银幕高21.9米，宽28.9米）

  - IMAX Theatre
    Sydney：位于澳大利亚[悉尼](../Page/悉尼.md "wikilink")[达令港](../Page/达令港.md "wikilink")，（银幕高29.57米，宽35.72米）（已经停止胶片IMAX系统放映，已经拆除即将在2019年重新改造为IMAX激光放映系统）。

  - [中國科學技术館](../Page/中國科學技术館.md "wikilink")：位于[北京市](../Page/北京市.md "wikilink")[朝阳区](../Page/朝阳区.md "wikilink")[北辰东路](../Page/北辰东路.md "wikilink")5号，拥有70mm
    IMAX巨幕影院（银幕高21.93米，宽29.58米）

  - [國立海洋科技博物館](../Page/國立海洋科技博物館.md "wikilink")：位於[基隆市](../Page/基隆市.md "wikilink")[中正區](../Page/中正區_\(基隆市\).md "wikilink")[北寧路](../Page/北寧路.md "wikilink")（银幕高22米，宽29米）。

  - [美麗華大直影城](../Page/美麗華影城.md "wikilink")：位於[台北市](../Page/台北市.md "wikilink")[中山區敬業三路](../Page/中山區_\(台北市\).md "wikilink"),（銀幕高21.16米，寬28.8米）截至2015年為東亞區唯一運行之商業70mm
    IMAX影廳。2016年3月24日正式启用雷射數位IMAX放映機。

  - [TCL中國戲院](../Page/中國戲院.md "wikilink")：位於[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[好莱坞](../Page/好莱坞.md "wikilink")[好莱坞大道](../Page/好莱坞大道.md "wikilink")6925号，（银幕高14米，宽28.7米）\[11\]2013年原影廳改建為數位IMAX規格，2014年增加70mm
    IMAX放映機，2015年4月1日正式启用雷射數位IMAX放映機。

## IMAX全球临时影院

  - 东京巨蛋体育场：2008年 《极速赛车手》日本首映禮 银幕宽度40米，可以容纳25000名观众。
  - 阿布扎比酋长国宫殿酒店：2009年 《麦加之旅》首映禮
    银幕尺寸宽100英尺（约30.5米）高70英尺（约21.3米）,首个户外IMAX影院，可以容纳6500名观众。
  - 香港文化中心：2014年6月20日 《变形金刚4：绝迹重生》全球首映禮，银幕尺寸宽18.29米，高10.36米。
  - 北京奥体中心国奥体育馆：2014年12月15日 《一步之遥》全球首映禮， 银幕尺寸宽26米，高14.5米，可以容纳2828名观众。
  - 维也纳国家歌剧院：2015年7月22日 《碟中谍5：神秘国度》全球首映禮，历史最悠久的搭建场地及欧洲首个IMAX临时影院。
  - 上海大剧院：2015年12月27日 《星球大战7：原力觉醒》 中国首映禮，银幕尺寸宽17.75米，高9.88米。
  - 圣迭戈：2016年7月21日 《星际迷航3：超越星辰》 全球首映禮。
  - 北京水立方：2016年12月21日 《星球大战外传：侠盗一号》中国首映禮，银幕尺寸宽27.40米，高11.51米。
  - 巴黎Palais de Chaillot剧院：2018年7月12日 《碟中谍6：全面瓦解》全球首映禮。

## 参见

  - [IMAX DMR影片列表](../Page/IMAX_DMR影片列表.md "wikilink")
  - [IMAX影院名單](../Page/IMAX影院名單.md "wikilink")
  - [3D電影](../Page/3D電影.md "wikilink")
  - [4D超立體巨幕影館](../Page/4D超立體巨幕影館.md "wikilink")

## 参考文献

## 外部链接

  - [IMAX官方网站](http://www.imax.com/)

  - [IMAX票房纪录](https://web.archive.org/web/20050404220057/http://www.the-numbers.com/movies/records/IMAX.html)

  -
  -
  -
[IMAX電影](../Category/IMAX電影.md "wikilink")
[Category:加拿大发明](../Category/加拿大发明.md "wikilink")

1.

2.  <http://www.cinema.com.my/articles/news_details.aspx?search=2017.n_worldsbiggestimaxcinemaopens_36657&title=World-s-biggest-IMAX-cinema-opens-in-Seoul#Bos9zQPFYhP412C1.97>

3.

4.  <https://www.bworldonline.com/southeast-asias-1st-imax-with-laser/>

5.  <https://av.watch.impress.co.jp/docs/news/1144016.html>

6.  <https://av.watch.impress.co.jp/docs/news/1144016.html>

7.  <https://av.watch.impress.co.jp/docs/news/1144016.html>

8.

9.
10.

11.