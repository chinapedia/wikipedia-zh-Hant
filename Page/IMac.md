**iMac**是一系列由[蘋果公司設計與生產的一體化](../Page/蘋果公司.md "wikilink")[Mac桌上型電腦品牌](../Page/Mac.md "wikilink")，從1998年推出以來一直是蘋果針對消費者市場的主力機種。

iMac的特點是它的設計。早在1998年，蘋果總裁[史蒂夫·乔布斯就將](../Page/史蒂夫·賈伯斯.md "wikilink")「What's
not a computer\!」（［看起來］不是電腦的電腦）概念應用於設計iMac的過程。結果造就了果凍－G3、檯燈－iMac
G4和相框－iMac G5。

有關其名稱，字母“i”的含義包括Internet、Individual、Instruct、Inform以及Inspire，在1998年iMac的發佈會上史蒂夫·賈伯斯對此作了介紹。

## 世代

|                                                     |                                                                                                                                                                                                                                                |                                                                                                                                               |                                                                                            |                                              |                                                                                                                                          |                                                                                           |                                                                                                                                                                                                       |
| --------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------ | -------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **世代**                                              | **iMac G3**                                                                                                                                                                                                                                    | **iMac G4**                                                                                                                                   | **iMac G5**                                                                                | **iMac（Intel）**                              | **iMac（鋁製外殼）**                                                                                                                           | **iMac（鋁製外殼、16:9）**                                                                       | **iMac（最新）**                                                                                                                                                                                          |
| **螢幕**                                              | 15吋 [CRT](../Page/CRT.md "wikilink")                                                                                                                                                                                                           | 15吋, 17吋, 及20吋 [LCD](../Page/LCD.md "wikilink")                                                                                               | 17吋, 及20吋 LCD                                                                              | 17吋, 20吋, 及24吋 LCD                           | 20吋 及24吋 LCD                                                                                                                             | 21.5吋 及 27吋 [LED](../Page/LED.md "wikilink")                                              | 21.5吋 及 27吋 [LED](../Page/LED.md "wikilink")                                                                                                                                                          |
| **[硬碟](../Page/硬碟.md "wikilink")**                  | 4GB 至 60GB                                                                                                                                                                                                                                     | 40GB 至 160GB                                                                                                                                  | 40GB 至 500GB                                                                               | 80GB 至 750GB                                 | 250GB 至 1TB                                                                                                                              | 500GB 至 2TB 或 256GB[固態硬碟](../Page/SSD.md "wikilink")                                      | 1TB 或 3TB ，768GB[固態硬碟](../Page/SSD.md "wikilink") 、1TB 或 3TB [Fusion Drive](../Page/Fusion_Drive.md "wikilink")                                                                                       |
| **內建[Mac OS版本](../Page/Mac_OS.md "wikilink")**\[1\] | [8.1](../Page/Mac_OS_8#Mac_OS_8.1.md "wikilink"), [8.5](../Page/Mac_OS_8#Mac_OS_8.5.md "wikilink"), [8.6](../Page/Mac_OS_8#Mac_OS_8.6.md "wikilink"), [9.0](../Page/Mac_OS_9.md "wikilink"), 9.1, [10.0](../Page/Mac_OS_X_v10.0.md "wikilink") | 9.2, [10.1](../Page/Mac_OS_X_v10.1.md "wikilink"), [10.2](../Page/Mac_OS_X_v10.2.md "wikilink"), [10.3](../Page/Mac_OS_X_v10.3.md "wikilink") | [10.3](../Page/Mac_OS_X_v10.3.md "wikilink"), [10.4](../Page/Mac_OS_X_v10.4.md "wikilink") | [10.4](../Page/Mac_OS_X_v10.4.md "wikilink") | [10.4](../Page/Mac_OS_X_v10.4.md "wikilink"), [10.5](../Page/Mac_OS_X_v10.5.md "wikilink"), [10.6](../Page/Mac_OS_X_v10.6.md "wikilink") | [10.6](../Page/Mac_OS_X_v10.6.md "wikilink"), [10.7](../Page/Mac_OS_X_Lion.md "wikilink") | [10.8](../Page/OS_X_Mountain_Lion.md "wikilink"), [10.9](../Page/OS_X_Mavericks.md "wikilink"), [10.9](../Page/OS_X_Mavericks.md "wikilink"), [10.10](../Page/OS_X_Yosemite.md "wikilink")(Retina 5K) |
| **發佈日期**                                            | 1998年8月15日                                                                                                                                                                                                                                     | 2002年1月7日                                                                                                                                     | 2004年8月31日                                                                                 | 2006年1月10日                                   | 2007年8月7日                                                                                                                                | 2009年10月20日                                                                               | 2012年10月23日                                                                                                                                                                                           |
| **停產日期**                                            | 2003年3月                                                                                                                                                                                                                                        | 2004年7月                                                                                                                                       | 2006年3月                                                                                    | 2007年8月                                      | 2009年10月                                                                                                                                 | 2012年10月                                                                                  | 尚未停產                                                                                                                                                                                                  |

## 型號

### iMac G3（托盤式光碟機）

[IMac_Bondi_Blue.jpg](https://zh.wikipedia.org/wiki/File:IMac_Bondi_Blue.jpg "fig:IMac_Bondi_Blue.jpg")

  - 1998年8月15日,– *iMac 233MHz* (修訂版 A),233 MHz 處理器。顯示卡為配備 2MB
    [SGRAM](../Page/SGRAM.md "wikilink") 的
    [ATI](../Page/ATI.md "wikilink") Rage IIc ,機殼顏色只有Bondi Blue（）可供選擇。
  - 1998年10月17日,– *iMac 233MHz* (修訂版 B),更用新的[Mac OS
    8.5及ATI](../Page/Mac_OS_8#Mac_OS_8.5.md "wikilink") Rage Pro
    顯示卡（配備6MB [SGRAM](../Page/SGRAM.md "wikilink")）。
  - 1999年1月5日，– *iMac 266MHz* (修訂版 C, "Five Flavors")，266 MHz
    處理器。刪除[IrDA接頭和夾層插槽](../Page/IrDA.md "wikilink")。 ATI
    Rage Pro Turbo 顯示卡(配備 6MB SGRAM)。 機殼顏色有 Strawberry（草莓紅）,
    Blueberry（藍莓蘭）, Lime（青擰綠）, Grape（葡萄灰）和 Tangerine（越橘橙）。比上一代便宜了
    100 美元。
  - 1999年4月14日，– *iMac 333MHz* (修訂版 D),333 MHz 處理器。更新游標上的縮放按鈕。

### iMac G3（吸入式光碟機）

[IMac_G3_flavors.jpg](https://zh.wikipedia.org/wiki/File:IMac_G3_flavors.jpg "fig:IMac_G3_flavors.jpg")

  - 1999年10月5日 – iMac/iMac DV/iMac DV
    SE。第一代,支援[FireWire](../Page/FireWire.md "wikilink")。350或400
    MHz 處理器,吸入式光碟機,與rev C/D iMac同樣顏色,但加上特別版的石墨色。
  - 2000年7月19日 – iMac/iMac DV/iMac DV+/iMac DV SE。350 or 400 or 450 or
    500 MHz 處理器,機殼顏色有Indigo（藍色）, Ruby（紅色）, Sage（綠色）, Snow（白色）and
    Graphite（灰色）。
  - 2001年2月22日 – (patterns)。400, 500 (PPC750CXe), or 600 (PPC750CXe) MHz
    處理器,機殼顏色有Indigo（藍色）, Graphite（灰色）及 Blue Dalmatian 。另外,有 "Flower
    Power" 模式.
  - 2001年7月18日 – (Summer 2001)。500, 600, or 700 MHz (PPC750CXe)
    處理器。機殼顏色有Indigo（藍色）, Graphite（灰色）及Snow(白色)。

### iMac G4 ("The New iMac"/"Flat panel iMac")

[IMac_G4_sunflower8.png](https://zh.wikipedia.org/wiki/File:IMac_G4_sunflower8.png "fig:IMac_G4_sunflower8.png")

  - 2002年1月7日 – [蘋果電腦推出新的](../Page/蘋果電腦.md "wikilink") iMac
    產品線，共有三種形式。他們具有未來感的外表，包含 700 或 800
    [MHz](../Page/MHz.md "wikilink") 的 G4 處理器，顏色皆為白色。顯示器是 15 吋
    [LCD](../Page/LCD.md "wikilink")，使用一個可擺動的器臂來定位。
  - 2002年7月17日 – 新的型號推出，擁有 17 吋 [LCD](../Page/LCD.md "wikilink") 和
    800 MHz 或是 1 GHz 的處理器，更大的[硬碟空間](../Page/硬碟.md "wikilink")（最大到 80
    GB）和更新的 [繪圖處理單元](../Page/GPU.md "wikilink")。
  - 2003年2月4日 – 產品線減為兩種型號，一種是 15 吋 LCD，另一種是 17 吋 LCD。[AirPort
    Extreme](../Page/AirPort_Extreme.md "wikilink") 也和
    [藍芽](../Page/藍芽.md "wikilink")，和 DDR 記憶體（第一次在 iMac 的歷史出現）都含在
    17 吋的型號。
  - 2003年8月 – iMac 15 和 17 吋的型號分別升級到 1 GHz 和 1.25 GHz G4 處理器。兩者都支援
    [USB](../Page/USB.md "wikilink") 2.0，AirPort
    Extreme，[藍芽](../Page/藍芽.md "wikilink")，以及 DDR
    [記憶體](../Page/記憶體.md "wikilink")。
  - 2003年11月18日 – 20 吋的螢幕型號推出，可以顯示 1680 \(\times\)1050 的螢幕解析度，以及
    1.25 GHz 的 G4 處理器。

### iMac G5

2004年8月31日 – [蘋果電腦推出全新的](../Page/蘋果電腦.md "wikilink") iMac 產品線，擁有 LCD
螢幕（17或20吋寬螢幕）和電腦（包含電源供應）本身，包含在一個 2 吋厚的平面顯示器上，使用 1.6 或 1.8 GHz
[64位元](../Page/64位元.md "wikilink") 處理器，特色為一個 [Serial
ATA](../Page/Serial_ATA.md "wikilink")
[硬碟](../Page/硬碟.md "wikilink")（在教育版型號是[Parallel
ATA](../Page/ATA.md "wikilink")）和 [nVidia](../Page/nVidia.md "wikilink")
GeForce 5200 繪圖處理晶片。USB 2、FireWire 400、和 10/100Base-T
[乙太網路埠](../Page/乙太網路.md "wikilink")、V.92
[數據機](../Page/數據機.md "wikilink")、視訊輸出埠、類比語音輸入孔、和結合類比／mini-[TOSLINK的語音輸出孔](../Page/TOSLINK.md "wikilink")（類似在
AirPort Express 的單元）和獨立擺置在後方的電源按鈕。整個外觀是由一個鋁製的懸臂撐在桌面上，也可以使用
[VESA](../Page/VESA.md "wikilink") 掛載平板來取代，讓整個機身可以用任何 VESA
標準的掛載點來懸掛。[蘋果電腦聲稱這是市場上最薄的桌上型電腦](../Page/蘋果電腦.md "wikilink")。iMac
G5 有三種零售型號，加上一種只供教育用沒有光碟機的型號，沒有數據機，和最簡單的 GeForce MX4000 繪圖顯示系統。

2005年3月3日，– [蘋果電腦](../Page/蘋果電腦.md "wikilink") 極大地更新 iMac
產品線，把主機板上的標準記憶體加到 512MB。入門型號現在為
1.8 GHz，最高型號達到 2 GHz。可選購的升級配備包含雙層 8x SuperDrive。所有的型號都含有
AirPort Extreme 無線網路，[藍芽](../Page/藍芽.md "wikilink") 2.0，和 [ATI
Technologies](../Page/ATI.md "wikilink")
[Radeon](../Page/Radeon.md "wikilink") 9600 顯示晶片及 標準 128MB
的視訊記憶體空間。所有的型號都與
[iLife](../Page/iLife.md "wikilink") '05 和 [Mac OS X
10.4](../Page/Mac_OS_X.md "wikilink") "Tiger" 一起搭配出售。

2005年8月18日 蘋果電腦推出iMac G5 Repair Extension Program for Video and Power
Issues。受影響的電腦系統會出現下列視訊或電源相關徵候之一：

  - 視訊畫面雜亂或扭曲失真
  - 沒有視訊畫面
  - 沒有電源

這項專案適用於約在 2004 年 9 月至 2005 年 6 月之間售出，配備 17 與 20 吋顯示器以及 1.6 GHz 與 1.8 GHz
G5 處理器的 iMac G5 機型。 序號前五碼在下列標示範圍之內的 iMac G5 電腦可能會受到影響：

  - W8435 - W8522
  - QP435 - QP522
  - CK435 - CK522
  - YD435 - YD522

### iMac G5(iSight)

帶有iSight相機和遙控器（Frontrow）的 Rev.C（第三次更改）iMac，也是最後一代採用PowerPC的iMac

### iMac Core Duo

[Imac_core_duo.jpg](https://zh.wikipedia.org/wiki/File:Imac_core_duo.jpg "fig:Imac_core_duo.jpg")

  - 2006年1月11日 – 蘋果電腦推出使用 [Intel](../Page/Intel.md "wikilink")
    處理器（[Intel](../Page/Intel.md "wikilink") [Core
    Duo](../Page/Core_Duo.md "wikilink")）的iMac，速度較G5處理器的iMac快了2至3倍。
      - 17" 機型 (MA199LL), 1.83 GHz [Intel Core
        Duo](../Page/Intel_Core.md "wikilink")
      - 20" 機型 (MA200LL), 2.0 GHz [Intel Core
        Duo](../Page/Intel_Core.md "wikilink")
      - 一個[PCI-Express架構下](../Page/PCI-Express.md "wikilink")、128MB DDR
        [VRAM的ATI](../Page/VRAM.md "wikilink") [Radeon
        X1600顯示晶片](../Page/Radeon_X1_Series.md "wikilink")，2.0
        GHz機型搭載256 MB DDR VRAM。

### iMac Core 2 Duo

  - 2006年9月6日 – 蘋果電腦推出使用 [Intel Core 2
    Duo](../Page/Intel_Core_2.md "wikilink") 處理器的 iMac
    ，速度較[Intel](../Page/Intel.md "wikilink") [Core
    Duo的](../Page/Core_Duo.md "wikilink") iMac 快了1.5倍。
      - 17" 機型，1.83/2.0 GHz [Intel Core 2
        Duo](../Page/Intel_Core_2.md "wikilink") ， Intel GMA 950
        顯示晶片／ATI Radeon X1600 顯示卡
      - 20" 機型，2.16 GHz [Intel Core 2
        Duo](../Page/Intel_Core_2.md "wikilink") ，ATI Radeon X1600 顯示卡
      - 24" 機型，2.16 GHz [Intel Core 2
        Duo](../Page/Intel_Core_2.md "wikilink") ，nVidia GeForce 7300 GT
        顯示卡，客戶也可以選配NVIDIA GeForce 7600 GT 顯示卡。

### iMac（鋁製外殼）

  - 2007年8月7日 – 蘋果電腦推出鋁製外殼的 iMac。
      - 20" 機型，2.0/2.4 GHz [Intel](../Page/Intel.md "wikilink") [Core 2
        Duo](../Page/Intel_Core_2.md "wikilink") ，ATI Radeon HD 2400 XT/
        2600 PRO 顯示卡
      - 24" 機型，2.4 GHz [Intel](../Page/Intel.md "wikilink") [Core 2
        Duo](../Page/Intel_Core_2.md "wikilink")／2.8 GHz
        [Intel](../Page/Intel.md "wikilink") [Core 2
        Extreme](../Page/Intel_Core_2.md "wikilink") ，ATI Radeon HD 2600
        PRO 顯示卡

### iMac（鋁製外殼，16:9寬螢幕）

  - 2009年10月20日發佈。

[Imac_16-9.png](https://zh.wikipedia.org/wiki/File:Imac_16-9.png "fig:Imac_16-9.png")

  -   - 21.5"機型，16:9顯示比例，解析度為1920 x
        1080像素的21.5吋（實際顯示範圍）鏡面寬螢幕TFT主動式矩陣平面液晶顯示器，同時支援IPS技術，3.06 GHz
        [Intel Core 2
        Duo](../Page/Intel_Core_2.md "wikilink")（可升級3.33 GHz
        [Intel Core 2 Duo](../Page/Intel_Core_2.md "wikilink")），4GB
        1066 MHz [DDR3 SDRAM](../Page/DDR3_SDRAM.md "wikilink")（可升級8GB
        1066 MHz [DDR3 SDRAM](../Page/DDR3_SDRAM.md "wikilink")），500GB
        7200 rpm Serial ATA Drive/1TB Serial ATA Drive（可升級2TB7200 rpm
        Serial ATA Drive），NVIDIA GeForce 9400M顯示卡/ATI Radeon HD 6750M顯示卡
      - 27"機型，16:9顯示比例 ，解析度為2560 x
        1440像素的27吋（實際顯示範圍）鏡面寬屏幕TFT主動式矩陣平面液晶顯示器，同時支援IPS技術，3.06 GHz
        [Intel Core 2 Duo](../Page/Intel_Core_2.md "wikilink")，2.66 GHz
        [Intel Core i5](../Page/Intel_Core_i5.md "wikilink")（可升級2.8 GHz
        [Intel Core i7](../Page/Intel_Core_i7.md "wikilink")），4GB
        1066 MHz [DDR3 SDRAM](../Page/DDR3_SDRAM.md "wikilink")（可升級16GB
        1066 MHz [DDR3 SDRAM](../Page/DDR3_SDRAM.md "wikilink")），1TB
        7200 rpm Serial ATA Drive（可升級2TB 7200 rpm Serial ATA Drive），ATI
        Radeon HD 4670顯示卡/ATI Radeon HD 6970M顯示卡，且支持[Mini
        DisplayPort輸入](../Page/Mini_DisplayPort.md "wikilink")。

### iMac（不設光碟機）

  - 2012年10月23日發佈，透過採用新焊接技術（[搅拌摩擦焊](../Page/搅拌摩擦焊.md "wikilink")），令其邊緣最薄處僅
    5 毫米，體積亦比上一代小 40%。
      - 21.5" 機型，16:9 顯示比例，解析度為 1920 x 1080 像素的21.5 吋(實際顯示範圍) LED
        背光顯示器，同時支援 IPS 技術。蘋果公司表示其新顯示器採用了全平面壓合（Full
        lamination）和電漿沉積（Plasma deposition）技術，令反光度減低了 75%，並改善了色彩品質。\[2\]
          - 1.4 GHz/2.7 GHz/2.9 GHz 双核心/四核心 [Intel Core
            i5](../Page/Intel_Core_i5.md "wikilink") 處理器(可升級 3.1 GHz 四核心
            [Intel Core i7](../Page/Intel_Core_i7.md "wikilink") 處理器)。
          - 8GB 1600 MHz [DDR3
            記憶體](../Page/DDR3_SDRAM.md "wikilink")(可升級16GB
            1600 MHz [DDR3 記憶體](../Page/DDR3_SDRAM.md "wikilink"))，1TB
            硬碟，[NVIDIA](../Page/NVIDIA.md "wikilink") GeForce GT
            640M/650M 圖像處理器。
      - 27" 機型，16:9 顯示比例 ，解析度為 2560 x 1440 像素的27 吋(實際顯示範圍) LED
        背光顯示器，同時支援 IPS 技術，並採用了全平面壓合（Full
        lamination）和電漿沉積（Plasma deposition）技術。
          - 2.9 GHz/3.2 GHz 四核心 [Intel Core
            i5](../Page/Intel_Core_i5.md "wikilink")
            處理器(3.2 GHz可升級3.4 GHz 四核心 [Intel Core
            i7](../Page/Intel_Core_i7.md "wikilink") 處理器)。
          - 8GB 1600 MHz [DDR3
            記憶體](../Page/DDR3_SDRAM.md "wikilink")(可升級16/32GB
            1600 MHz [DDR3 記憶體](../Page/DDR3_SDRAM.md "wikilink"))，1TB
            硬碟(可升級 3TB 硬碟、1TB 或 3TB [Fusion
            Drive](../Page/Fusion_Drive.md "wikilink") 或 768GB
            [SSD](../Page/固態硬碟.md "wikilink"))，[NVIDIA](../Page/NVIDIA.md "wikilink")
            GeForce GTX 660M/675MX 圖像處理器。\[3\]\[4\]

### 配备 Retina 显示屏的 iMac

  - 2014年10月17日在苹果总部Town
    Hall发布，此iMac配备了5120\*2880分辨率的27英寸显示屏，与此同时，仍然维持了与上一代相同的5毫米边缘厚度。
  - 2015年10月13日發布了配備4096\*2304分辨率的21.5英寸显示屏，与此同时，仍然维持了与上一代相同的5毫米边缘厚度。
  - 目前有27和21.5英寸机型，分辨率为5120\*2880 27"或4096\*2304 21.5"，像素密度为217 ppi。
  - 内存为 8GB 1866 MHz DDR3 ，可升级为 16GB 和 32GB，与此同时，机身后部还有两个空闲的 SO-DIMM
    内存插槽可供用户访问。
  - 储存设备默认为 1TB ，可升级为 2TB或3TB [Fusion
    Drive](../Page/Fusion_Drive.md "wikilink")，也可配备 256GB、512GB、1TB
    的[SSD](../Page/固態硬碟.md "wikilink")。
  - 为了应对如此大的像素处理量，苹果专门为此研发了一颗新的时序控制器（TCON），目前关于此颗TCON的信息不详。
  - 其它硬件配置与现有27/21.5英寸 iMac 保持一致。

## 發展線

{{\#tag:timeline| DateFormat=mm/dd/yyyy Define $now = 10/15/2013 \#
Please keep updated Period = from:07/15/1998 till:$now Define $skip =
at:end \# Force a blank line Define $dayunknown = 15 \# what day to use
if it's actually not known ImageSize= width:1000 height:auto
barincrement:22 TimeAxis = orientation:horizontal PlotArea = right:4
left:0 bottom:100 top:5

Colors =

`    id:bg         value:white`
`    id:g3         value:rgb(1,0.85,0.85) legend:iMac_(PowerPC_G3)`
`    id:g32        value:rgb(1,0.9,0.9)`
`    id:g4         value:rgb(1,0.85,0.65) legend:iMac_(PowerPC_G4)`
`    id:g42        value:rgb(1,0.9,0.7)`
`    id:g5         value:rgb(1,0.85,0.45) legend:iMac_(PowerPC_G5)`
`    id:g52        value:rgb(1,0.9,0.5)`
`    id:cored       value:rgb(0.8,1,0.8) legend:iMac_(Intel_Core_Duo)`
`    id:core2d      value:rgb(0.5,1,0.8) legend:iMac_(Intel_Core_2_Duo)`
`    id:core2d2     value:rgb(0.2,1,0.8)`
`    id:corei     value:rgb(0.1,1,0.5) legend:iMac_(Intel_Core_2_Duo/_Core_i)`
`    id:2008      value:rgb(0.1,0.9,0.8)`

1.  id:core2e value:rgb(0,0.9,0.8)
    legend:iMac_(Intel_Core_2_Extreme) \#
2.  id:blank value:rgb(0.99,0.99,0.99) legend:\~_\#

`    id:corei2     value:rgb(0.78,1,0.35) legend:iMac_(Intel_Core_i)`
`    id:emac       value:rgb(0.65,0.65,0.9) legend:eMac_(PowerPC_G4)`
`    id:emacl      value:rgb(0.85,0.85,0.95) legend:Limited_to_education`
`    id:current    value:rgb(0.9,0.9,0.9) legend:Still_currently_produced`
`    id:lightline  value:rgb(0.9,0.9,0.9) legend:Other_Apple_products`
`    id:lighttext  value:rgb(0.5,0.5,0.5)`

BackgroundColors = canvas:bg ScaleMajor = gridcolor:lighttext unit:year
increment:1 start:12/31/1998 ScaleMinor = gridcolor:lightline unit:month
increment:1 start:07/15/1998 Legend = orientation:vertical
position:bottom columns:4

BarData =

` barset:main`
` barset:other`

PlotData=

` width:15 textcolor:black`

` barset:main`
`   shift:(5,-5) anchor:from fontsize:s`
`   $skip`
`   $skip`
`   $skip`
`   color:g3 from:08/15/1998 till:10/05/1999 text:"`[`iMac``
 ``G3`](../Page/iMac_G3.md "wikilink")`"`
` barset:break`
`   $skip`
`   $skip`
`   $skip`
`   color:g32 from:10/05/1999 till:03/18/2003 text:"`[`iMac``   ``G3``
 ``(slot``   ``loading)`](../Page/iMac_G3.md "wikilink")`"`
` barset:break`
`   color:emacl from:04/29/2002 till:06/04/2002`
`   color:g4 from:01/07/2002 till:08/30/2004 text:"`[`iMac``   ``G4``
 ``15″`](../Page/iMac_G4.md "wikilink")`"`
`   color:g4 from:07/17/2002 till:08/30/2004 text:"`[`iMac``   ``G4``
 ``17″`](../Page/iMac_G4.md "wikilink")`"`
`   color:g4 from:11/18/2003 till:08/30/2004 text:"`[`iMac``   ``G4``
 ``20″`](../Page/iMac_G4.md "wikilink")`"`
` barset:break`
`   color:emac from:06/04/2002 till:10/12/2005 text:"`[`eMac`](../Page/eMac.md "wikilink")`"`
`   $skip`
`   color:g5 from:08/31/2004 till:01/10/2006 text:"`[`iMac``   ``G5``
 ``17″`](../Page/iMac_G5.md "wikilink")`"`
`   color:g5 from:08/31/2004 till:03/20/2006 text:"`[`iMac``   ``G5``
 ``20″`](../Page/iMac_G5.md "wikilink")`"`
` barset:break`
`   color:emacl from:10/12/2005 till:07/05/2006`
`   $skip`
`   color:cored from:01/10/2006 till:09/06/2006 text:"`[`Core`](../Page/Intel_iMac.md "wikilink")`"`
`   color:cored from:01/10/2006 till:09/06/2006 text:"`[`Core`](../Page/Intel_iMac.md "wikilink")`"`
` barset:break`
`   $skip`
`   $skip`
`   color:core2d from:09/06/2006 till:08/07/2007 text:"`[`Core``
 ``2``   ``17″`](../Page/Intel_iMac.md "wikilink")`"`
`   color:core2d from:09/06/2006 till:08/07/2007 text:"`[`Core``
 ``2``   ``20″`](../Page/Intel_iMac.md "wikilink")`"`
`   color:core2d from:09/06/2006 till:08/07/2007 text:"`[`Core``
 ``2``   ``24″`](../Page/Intel_iMac.md "wikilink")`"`
` barset:break`
`   $skip`
`   $skip`
`   $skip`
`   color:core2d2 from:08/07/2007 till:10/20/2009 text:"`[`Core``
 ``2``   ``(Aluminum)``   ``20″`](../Page/Intel_iMac.md "wikilink")`"`
`   color:core2d2 from:08/07/2007 till:10/20/2009 text:"`[`Core``
 ``2``   ``(Aluminum)``   ``24″`](../Page/Intel_iMac.md "wikilink")`"`
` barset:break`
`   $skip`
`   $skip`
`   $skip`
`   color:corei from:10/20/2009 till:10/23/2012 text:"`[`Core``
 ``2/Core``   ``i``   ``21.5″`](../Page/Intel_iMac.md "wikilink")`"`
`   color:corei from:10/20/2009 till:10/23/2012 text:"`[`Core``
 ``2/Core``   ``i``   ``27″`](../Page/Intel_iMac.md "wikilink")`"`
` barset:break`
`   $skip`
`   $skip`
`   $skip`
`   color:corei2 from:10/23/2012 till:end text:"`[`Core``   ``i``
 ``21.5″`](../Page/Intel_iMac.md "wikilink")`"`
`   color:corei2 from:10/23/2012 till:end text:"`[`Core``   ``i``
 ``27″`](../Page/Intel_iMac.md "wikilink")`"`
` barset:break`
`   $skip`
`   $skip`
`   $skip`
`   color:current from:$now till:end text:`
`   color:current from:$now till:end text:`

` barset:other`
`   shift:(5,-5) anchor:from fontsize:s`
`   color:lightline from:start till:07/23/1999 text:"`[`Power``
 ``Mac``   ``G3`](../Page/Power_Mac_G3.md "wikilink")`"`
` barset:break`
`   color:lightline from:07/23/1999 till:07/19/2000 text:"|`[`iBook`](../Page/iBook.md "wikilink")`"`
` barset:break`
`   color:lightline from:07/19/2000 till:10/23/2001 text:"|`[`Power``
 ``Mac``   ``G4``
 ``Cube`](../Page/Power_Mac_G4_Cube.md "wikilink")`"`
` barset:break`
`   color:lightline from:10/23/2001 till:06/23/2003 text:"|`[`iPod`](../Page/iPod.md "wikilink")`"`
` barset:break`
`   color:lightline from:06/23/2003 till:01/11/2005 text:"|`[`Power``
 ``Mac``   ``G5`](../Page/Power_Mac_G5.md "wikilink")`"`
` barset:break`
`   color:lightline from:01/11/2005 till:05/16/2006 text:"|`[`Mac``
 ``Mini`](../Page/Mac_Mini.md "wikilink")`"`
` barset:break`
`   color:lightline from:05/16/2006 till:06/29/2007 text:"|`[`MacBook`](../Page/MacBook.md "wikilink")`"`
` barset:break`
`   color:lightline from:06/29/2007 till:01/15/2008 text:"|`[`iPhone`](../Page/iPhone.md "wikilink")`"`
` barset:break`
`   color:lightline from:01/15/2008 till:01/27/2010 text:"|`[`MacBook``
 ``Air`](../Page/MacBook_Air.md "wikilink")`"`

` barset:break`
`   color:lightline from:01/27/2010 till:$now text:"|`[`iPad`](../Page/iPad.md "wikilink")`"`
` barset:break`
`   color:current from:$now till:end`

}}

## 参考文献

## 參見

  - [eMac](../Page/eMac.md "wikilink")
  - [Macintosh](../Page/Macintosh.md "wikilink")
  - [蘋果電腦機種的比較](../Page/蘋果電腦機種的比較.md "wikilink")

## 外部連結

  - [iMac美國](http://www.apple.com/imac/)
  - [iMac台灣](https://web.archive.org/web/20060901153931/http://www.apple.com.tw/imac/)
  - [iMac香港](https://web.archive.org/web/20060824130702/http://appleclub.com.hk/imac/)
  - [iMac中國](https://web.archive.org/web/20011022023136/http://www.apple.com.cn/imac/)
  - [2007iMac電腦電視廣告](http://www.youtube.com/watch?v=8YfCkaadnkE)

{{-}}

[Category:麦金塔](../Category/麦金塔.md "wikilink")

1.  <http://support.apple.com/kb/HT1159>
2.
3.
4.