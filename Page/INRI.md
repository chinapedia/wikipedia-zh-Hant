[Cristo_crucificado.jpg](https://zh.wikipedia.org/wiki/File:Cristo_crucificado.jpg "fig:Cristo_crucificado.jpg")画家[委拉斯开兹作品](../Page/迪亚哥·委拉斯开兹.md "wikilink")\]\]
[La_descente_de_croix_Rubens.jpg](https://zh.wikipedia.org/wiki/File:La_descente_de_croix_Rubens.jpg "fig:La_descente_de_croix_Rubens.jpg")画家[鲁本斯作品](../Page/彼得·保罗·鲁本斯.md "wikilink")\]\]

“**[耶稣](../Page/耶稣.md "wikilink")，[拿撒勒人](../Page/拿撒勒.md "wikilink")，[犹太人的](../Page/犹太人.md "wikilink")[君王](../Page/犹太君主列表.md "wikilink")**”（），是中的一个短语，常缩写为。

是这样描述的：

有些[东正教教會和](../Page/东正教.md "wikilink")[東儀天主教會使用](../Page/東儀天主教會.md "wikilink")[希腊字母](../Page/希腊字母.md "wikilink")****，是基于[耶稣](../Page/耶稣.md "wikilink")[十字架上的希腊文](../Page/十字架.md "wikilink")：**。在许多东正教的[圣像画中](../Page/圣像画.md "wikilink")，会将其改为**（世界之王），这并不表明原来写的就是这个，而是表明原来写的文字的意义。[羅馬尼亞正教會使用](../Page/羅馬尼亞正教會.md "wikilink")[羅馬尼亞語](../Page/羅馬尼亞語.md "wikilink")

的縮寫，即還是****。[俄羅斯正教會則使用](../Page/俄羅斯正教會.md "wikilink")****，是基於[教會斯拉夫語](../Page/教會斯拉夫語.md "wikilink")
 (拿撒勒的耶穌，猶太人之王) 的縮寫。

当[彼拉多不愿意未经审判就钉死耶稣时](../Page/彼拉多.md "wikilink")，他引用的是当时[罗马帝国政府允许](../Page/罗马帝国.md "wikilink")[犹太人有限自治的条约](../Page/犹太人.md "wikilink")。而[犹太教](../Page/犹太教.md "wikilink")[大祭司们起诉耶稣](../Page/大祭司_\(犹太教\).md "wikilink")，认为他违背了这个有限自治的条约，因为耶稣声称他拥有他实际所没有的权柄。彼拉多于是希望耶稣否认他自己是“[犹太人的君王](../Page/犹太君主列表.md "wikilink")”，可是耶稣没有否认。

## 版本

<table>
<thead>
<tr class="header">
<th><p>经节</p></th>
<th></th>
<th></th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/通用希腊语.md" title="wikilink">通用希腊语原文</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Template:圣经英文译本.md" title="wikilink">英语译文</a></p></td>
<td><p>The King of the Jews</p></td>
<td><p>This is the King of the Jews</p></td>
<td><p>This is Jesus, the King of the Jews</p></td>
<td><p>Jesus of Nazareth, the King of the Jews</p></td>
</tr>
<tr class="odd">
<td><p>经文对牌子上文字的语种及其顺序的描述</p></td>
<td><p><em>无描述</em></p></td>
<td><p>“<a href="../Page/希腊文.md" title="wikilink">希腊文</a>、<a href="../Page/拉丁文.md" title="wikilink">拉丁文</a>、<a href="../Page/希伯来文.md" title="wikilink">希伯来文</a>”</p></td>
<td><p><em>无描述</em></p></td>
<td><p>“<a href="../Page/希伯来文.md" title="wikilink">希伯来文</a>、<a href="../Page/拉丁文.md" title="wikilink">拉丁文</a>、<a href="../Page/希腊文.md" title="wikilink">希腊文</a>”</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/英王钦定本.md" title="wikilink">英王钦定本的完整经节</a></p></td>
<td><p>And the superscription of his accusation was written over, THE KING OF THE JEWS.</p></td>
<td><p>And a superscription also was written over him in letters of Greek, and Latin, and Hebrew, THIS IS THE KING OF THE JEWS.</p></td>
<td><p>And set up over His head His accusation written, THIS IS JESUS THE KING OF THE JEWS</p></td>
<td><p>And Pilate wrote a title, and put it on the cross. And the writing was JESUS OF NAZARETH THE KING OF THE JEWS.</p></td>
</tr>
</tbody>
</table>

## 参考文献

## 参见

  - [耶稣](../Page/耶稣.md "wikilink")、[耶穌被釘十字架](../Page/耶穌被釘十字架.md "wikilink")、[十字架](../Page/十字架.md "wikilink")

  - [彌賽亞](../Page/彌賽亞.md "wikilink")/[基督](../Page/基督.md "wikilink")、[以马内利](../Page/以马内利.md "wikilink")

  - [犹太君主列表](../Page/犹太君主列表.md "wikilink")、[犹太教](../Page/犹太教.md "wikilink")

  -
{{-}}

[Category:拉丁文首字母缩写](../Category/拉丁文首字母缩写.md "wikilink")
[Category:耶稣的称号](../Category/耶稣的称号.md "wikilink")
[Category:圣经短语](../Category/圣经短语.md "wikilink")
[Category:基督教术语](../Category/基督教术语.md "wikilink")