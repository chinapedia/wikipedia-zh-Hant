</ref>

2016年9月7日（当地时间）苹果公司在美国加州比尔·格雷厄姆市政礼堂发布了iPhone 7和iPhone 7
Plus，尺寸和前代保持一致，加入新颜色“亮黑”，取消了耳机接口，拥有双扬声器，搭配上了全新taptic
engine，支持IP67级的防水。其中iPhone 7 Plus拥有双摄像头，能进行景深虚化，为此苹果在iPhone 7
Plus的相机中加入了“人像拍摄”。\[1\]

2017年9月12日（当地时间）苹果公司在美国加州圣何塞的乔布斯剧院发布了iPhone 8、iPhone 8 Plus和iPhone
X，其中iPhone 8和8 Plus和iPhone
X均采用了玻璃后盖，使之能进行无线充电，屏幕均支持“[原彩显示](../Page/True_tone.md "wikilink")”\[2\]。iPhone
X的工业设计是自iPhone诞生以来变化最大的，它采用全面屏设计，取消了home键，改用类似[web
os](../Page/WebOS.md "wikilink")、[meego的滑动交互](../Page/MeeGo.md "wikilink")，采用了[三星提供的OLED显示面板](../Page/三星.md "wikilink")，首次加入了Face
ID，前置摄像头加入了[Animoji的功能](../Page/Animoji.md "wikilink")。\[3\] (暂 2019年 9月
新 Touch ID 暫名SCreen fingerprint 可會出現 )

## 硬件

### 螢幕與按键

[Apple_iPhone.jpg](https://zh.wikipedia.org/wiki/File:Apple_iPhone.jpg "fig:Apple_iPhone.jpg")
iPhone首4代系列的機型均使用9厘米（3.5英吋）[LCD液晶體](../Page/LCD.md "wikilink")[多點觸控防刮玻璃顯示器](../Page/多點觸控.md "wikilink")\[4\]，而[iPhone
5則增至](../Page/iPhone_5.md "wikilink")4英吋\[5\]，[iPhone
6至](../Page/iPhone_6.md "wikilink")[iPhone
8这四款机型屏幕大小](../Page/iPhone_8.md "wikilink")4.7英寸，[iPhone
6 Plus至](../Page/iPhone_6_Plus.md "wikilink")[iPhone 8
Plus这四款机型屏幕则为](../Page/iPhone_8_Plus.md "wikilink")5.5英寸。2017年发布的[iPhone
X与次年的](../Page/iPhone_X.md "wikilink")[XS屏幕均为](../Page/iPhone_XS.md "wikilink")5.8英寸OLED全面屏，次年发布的XS
Max屏幕达到6.5英寸，[XR达到](../Page/iPhone_XR.md "wikilink")6.1英寸。「電容式觸控螢幕」是專為一指或多指觸控而設的[多點觸控感應](../Page/多點觸控.md "wikilink")。

iPhone的[多點觸控和手勢操控是基於](../Page/多點觸控.md "wikilink")發展的技術\[6\]，截止到2018年7月，iPhone拥有两种螢幕类别，分別是LCD螢幕與OLED螢幕，後者僅在2017年發布的iPhone
X與2018年發布的[iPhone XS及](../Page/iPhone_XS.md "wikilink")[iPhone XS
Max上](../Page/iPhone_XS_Max.md "wikilink")。自iPhone
3GS以來就有有耐[指紋](../Page/指紋.md "wikilink")塗層\[7\]，能避免使用者的指纹留在iPhone的螢幕上\[8\]。

[IPhone_3G_S_sides.jpg](https://zh.wikipedia.org/wiki/File:IPhone_3G_S_sides.jpg "fig:IPhone_3G_S_sides.jpg")

iPhone正面有一个叫做“home键”的圆形按键（除iPhone X、XS、XS
Max、XR外)，它主要用於關閉使用中的應用程式，導航到主螢幕界面，或是喚醒手機。iPhone還有額外5個按鈕：開關按鈕
-
位於電話的頂部，作為電源按鈕，它亦能作為控制來電的控制，當接到來電時，按一次開關按鈕可讓來電鈴聲消失，按兩次讓來電轉駁至語言信箱，長按會顯示關機選項，關機後長按會開啟手機；位於機身左側是靜音和音量控制鍵，iPhone
4擁有兩個圓形按鍵來增加或減少音量，靜音鍵能使電話鈴聲、提示音、推送通知、相機快門等快速轉換為靜音，這鍵不會停止鬧鈴應用程式，在一些國家或地區，它不會把相機快門聲、語音備忘錄音等音效關閉。

在第一代iPhone上，除Home Button外，其他按鈕均為塑膠材料；但自iPhone 3G起，除Home
Button外，按鈕全由金屬製\[9\]。

2017年发布的iPhone X与2018年发布的XS、XS
Max使用来自三星的OLED螢幕，并使用了[COP](../Page/Chip_on_plastic.md "wikilink")（Chip
On Plastic）的工艺。

### 感應器

#### 近距感應器

當通話時，人的臉部會很接近顯示屏，近距感應器會讓屏幕停用，這是為了節省電力和防止用戶的耳朵、臉部造成誤觸。

#### 周邊光線感應器

周邊光線感應器能感應周遭的光源來調節電話的亮度，從而節省電池用量。

#### 磁力感應器

磁力感應器自iPhone3S內置的，用於測量磁力強度或磁場的方向。有時候某些設備或是無線電信號會干擾磁力感應器，要求遠離干擾或是以8字形移動裝置時重新校準。自iPhone
3S，iPhone設有指南針的應用程式，那是在發布的時候獨一無二的功能，指針在磁場上顯示出當時的方向。

#### 加速感應器

一個用於手機定位及相應地調整屏幕的三軸加速器，讓用戶輕鬆地切換頁面方向\[10\]。圖片及網頁瀏覽和音樂播放，支援全螢幕時直立或向左或向右播放\[11\]。不像[iPad](../Page/iPad.md "wikilink")，當裝置反轉時，iPhone反轉時螢幕不會旋轉，Home鍵置於屏幕上方，除非是運作的應用程式專門這樣做。3.0版本更新加入其他的應用程序的平面支援，例如是電郵和晃動時輸入的單位形式\[12\]\[13\]。加速感應器還可以用於控制第三方應用程序的軟件，特別是遊戲。从iPhone
5s开始，他还用做于健身方面，主要用作计步器，此功能包含在M7协处理器和它的后续版本中。

#### 液體接觸感應器

所有iPhone（許多蘋果公司的產品）的耳機插槽都有一塊小圓片，當接觸到水分等液體時會由白色轉成紅色。
iPhone 3G和之後的機型也有類似的感應器於Dock連接埠\[14\]。
由於蘋果公司的保養範圍不涵蓋產品被水令其損壞，員工會先檢查此感應器，才批核[維修保養或更換](../Page/AppleCare.md "wikilink")。

iPhone的感應器比其他廠商的手機較為外露，目的是讓電池更受保護，例如讓電池在外殼裡的下方。用戶日常使用的汗水\[15\]、浴室的蒸氣和其他環境中的濕度\[16\]也能觸發此感應器。批評導致蘋果公司改變它們對iPhone及類似產品的防水政策，如觸發內部液體濺到感應器，容許用戶要求進一步的手機內部檢查驗證\[17\]。

#### 迴轉儀感應器

由iPhone
4那時開始，[蘋果公司的智能手機亦包括陀螺傳感器](../Page/蘋果公司.md "wikilink")，提高iPhone感知如何移動的能力。

#### 无线电传感器

以前iPhone都包含一个可以接收无线电信号的芯片；即使苹果已经关闭了收音机功能，但iPhone7之后的机型才取消了接收无线电的芯片。

#### 指紋感應器（触控ID）

iPhone
5S在圍繞Home鍵周邊設有指紋感應器，能夠360°採集及分析用戶的指紋特徵，配合內置触控ID功能即可實現包括屏幕解鎖、[iTunes購買應用程式](../Page/iTunes.md "wikilink")、可以當成貨幣等功能。

#### 面部识别传感器（面容ID）

iPhone
X上的面容ID会先用泛光感应元件照亮用户的脸部获取2D红外照片，然后再用红外摄像头识别，接下来再用点阵投影器向物体的表面投出三万多个特定编码的红外点，再通过反射回到红外摄像头接收器，利用红外照片和反射回去的红外点间的偏移，就可以物体获得脸部表面的景深信息，从而构建一个3D精确模型，然后就会将红外图像和3D精准模型发送到处理器中，并转化成一道数学表达式，比对之前已注册的面部数据后，就会得出结论。\[18\]

### 聲音與輸出

[IPhone-bottom.jpg](https://zh.wikipedia.org/wiki/File:IPhone-bottom.jpg "fig:IPhone-bottom.jpg")
在iPhone的底部，左方是揚聲器，右方是麥克風，在屏幕的上方有一個額外的揚聲器，作為電話的聽筒，在iPhone
7及以后的机型中它也充当扬声器。iPhone
4還包括一個用於通话噪聲消除的小型聽筒\[19\]，在這個基礎上，是可以切換揚聲器與麥克風的位置—揚聲器置於右方\[20\]。所有iPhone的音量控制都是位於左側。

在首5個系列的iPhone（直至iPhone
4S），電話的左上方有一個直徑3.5毫米[TRS端子的装置](../Page/TRS端子.md "wikilink")，用来連接聲音輸出（例如耳機）\[21\]，其後的iPhone系列將置於左下方\[22\]。第一代iPhone的耳機插糟由於設計特殊，以致大部分的音頻設備都不適用，需要加上一個轉接器才能夠使用\[23\]，隨後的系列透過嵌入式安裝的耳機插座，使大部分音響裝置都能與iPhone相配，問題得以消除。汽車裝配[輔助插孔](../Page/电子连接器.md "wikilink")，透過內置的[藍牙v](../Page/藍牙.md "wikilink")2.1（v4.0用於iPhone
4S和iPhone 5），允許iPhone支援藍牙設備\[24\]\[25\]，能於駕駛時免提使用。

蘋果公司的EarPods在靠近麥克風的位置有一個多功能按鈕，可以用於播放或暫停音樂，跳過，和接聽或結束通話，而不需觸碰電話。少數專門為iPhone而設的第三方耳機亦包括麥克風和控制按鈕\[26\]，目前的耳機設計亦包括音量控制，能兼容最近幾個iPhone機型
\[27\]。在耳機插線增至第四個環，以便進行額外的訊息交換。

內置的[藍牙2.x+EDR支援無線耳機](../Page/藍牙.md "wikilink")，這需要[HSP](../Page/藍牙規範.md "wikilink")[簡介](../Page/簡介.md "wikilink")。立體聲音頻中增加至3.0更新至支援[A2DP](../Page/A2DP.md "wikilink")\[28\]\[29\]，雖然存在不認可的第三方解決方案，iPhone不能正式支援[OBEX](../Page/OBEX.md "wikilink")的\[30\]。有了這些配置，用戶能與其他具有藍牙功能的手機交換多媒體檔案，如圖片、音樂和視頻。

### 電池

[IPhone_Internals.jpg](https://zh.wikipedia.org/wiki/File:IPhone_Internals.jpg "fig:IPhone_Internals.jpg")
iPhone的电池不可更换。\[31\]\[32\]。當iPhone連接到電腦時，可以透過[USB從Dock連接線進行同步及充電](../Page/USB.md "wikilink")。\[33\]\[34\]

對於iPhone電池的質量，蘋果公司在預備生產的時候進行約400次的測試，以確定電池的壽命。蘋果公司的網站寫著電池的壽命在經過約400次充電循環的測試後，它的設計能使其保持原有總用電的80%\[35\]。

早期的iPhone電池壽命曾一度被一些科技版記者批評不足以應付和低於蘋果公司的說法\[36\]\[37\]\[38\]\[39\]，這與所進行的顧客滿意度調查報告中亦反映，在「電池方面」的滿意度十分低，iPhone
3G最低評級為2/5分\[40\]\[41\]\[42\]\[43\]\[44\]\[45\]。如電池在保養期內出現故障或過早地耗尽，用戶可把手機送回蘋果公司進行免費更換\[46\]，保養期為購買日起一年內\[47\]\[48\]，如購買[Apple
Care全方位服務計劃的話](../Page/AppleCare.md "wikilink")，保養期可延長至兩年\[49\]。電池更換服務的收費是基於用戶的產品推出日而定\[50\]\[51\]，類似蘋果公司及第三方公司提供的iPods電池更換服務。關注納稅人及消費者權利的[消費者倡導組織](../Page/消费者保护.md "wikilink")，投訴[蘋果公司和](../Page/蘋果公司.md "wikilink")[AT\&T為用戶更換電池時必須收費](../Page/AT&T.md "wikilink")\[52\]。自2007年7月，第三方的更換電池套件已推出，價錢比蘋果公司的電池更換計劃更便宜\[53\]。這些套件包括一個小螺絲刀和指引單張，但很多新型的iPod被焊接了第一代iPhone的電池，因此安裝新電池是需要焊接的。iPhone
3G使用不同的電池，它需要配合一個連接器，使它更容易更換\[54\]。

同时苹果也在2013年7月申请了一则关于电池的专利，欲查看请翻到“知识产权”一栏。

欲查看“降速门”请翻到“事件”一栏。

在iPhone X上苹果公司使用了双电池的方案，使iPhone X的电池容量达到了2716mAh。\[55\]

### 相機

[iPhone_4_cameras.png](https://zh.wikipedia.org/wiki/File:iPhone_4_cameras.png "fig:iPhone_4_cameras.png")閃光燈，前置鏡頭置於正面頂部，適用於iPhone
4及往後的機型。\]\] 第一代iPhone及iPhone
3G都有一個固定焦距的200萬像素後置鏡頭，它們沒有閃光燈、光學焦距及[自動對焦](../Page/自動對焦.md "wikilink")，不支援拍攝視頻。iPhone
3G的用戶只能在[應用程式商店購買这类的第三方應用程式](../Page/App_Store.md "wikilink")，或者是[越獄](../Page/IOS越獄.md "wikilink")。

iPhone
3GS的相機鏡頭由製造，320萬像素鏡頭，支援[自動對焦](../Page/自動對焦.md "wikilink")、自動[白平衡](../Page/白平衡.md "wikilink")、自動微距（長達10厘米），可拍攝640
x
480短片（[VGA解像度](../Page/VGA.md "wikilink")），每秒30幀\[56\]。雖然跟高端技術的[CCD攝錄機不同](../Page/感光耦合元件.md "wikilink")，它顯示出滾動快門效果\[57\]，短片可以於iPhone上裁剪，並直接上傳到[YouTube](../Page/YouTube.md "wikilink")、[MobileMe或其他網絡](../Page/MobileMe.md "wikilink")。

iPhone
4有500萬像素鏡頭（2,592×1,936解像度），能夠拍攝[720p的短片](../Page/720p.md "wikilink")，支援高解像度拍攝。它亦有一個背照式傳感器，能夠在微弱的光線下拍攝，[LED閃光燈可在拍攝時一直亮著](../Page/LED.md "wikilink")\[58\]，這是能夠做到高動態範圍攝影的第一部iPhone\[59\]。iPhone
4還有個前置鏡頭，用於[視像通話](../Page/FaceTime.md "wikilink")，能拍攝[VGA照片和](../Page/VGA.md "wikilink")[SD短片](../Page/SD.md "wikilink")。

iPhone
4S的後鏡頭功能提升至800萬像素，支援[1080p短片拍攝](../Page/1080p.md "wikilink")\[60\]，內置的迴轉儀能使錄製短片時保持影像穩定。使用[iOS
6或更高版本的iPhone](../Page/iOS_6.md "wikilink") 4S及iPhone
5，可以透過內置的相機程式拍攝全景照片\[61\]，iPhone
5爾更能在拍攝短片的同時拍照\[62\]。

iPhone
5據說能拍到從光源中出來的紫霧\[63\]，雖然消費者報告中指它「比起有精細鏡頭的前幾代iPhone或其他[Android手機](../Page/Android.md "wikilink")，沒有那麼容易拍攝得到明亮光源所產生的紫霧...\[64\]。」

搭载iOS7或更高系统版本的iPhone只要向左滑就可以打开相机功能，iPhone X只要重按锁屏的相机图标就可以打开相机，亦可以向左滑动打开。

[IOS
7的測試版程式指出蘋果公司可能會在下一代iPhone的鏡頭裝配慢動作的拍攝模式](../Page/IOS_7.md "wikilink")\[65\]。

iPhone 6 Plus首次支持光学防抖（OIS）。

此后的iPhone 6s Plus也支持光学防抖，同时像素数也提升到1200万。

iPhone 7及以后的iPhone全部支持光学防抖，iPhone 7 Plus支持主摄像头防抖。

iPhone 8、iPhone 8 Plus及iPhone X均支持双摄像头防抖。

### 儲存與SIM卡

[Iphonesimcard.JPG](https://zh.wikipedia.org/wiki/File:Iphonesimcard.JPG "fig:Iphonesimcard.JPG")|date=2008-07-10|accessdate=2009-06-11|deadurl=no|archiveurl=[https://web.archive.org/web/20090629051335/http://support.apple.com/kb/HT1438|archivedate=2009-06-29](https://web.archive.org/web/20090629051335/http://support.apple.com/kb/HT1438%7Carchivedate=2009-06-29)}}</ref>。
SIM卡開啟器置於SIM卡位置的插孔，SIM卡插槽就會彈出。\]\]
第一代iPhone有兩種內置儲存容量可供選擇，4GB和8GB版本。2007年9月5日，4GB版本停产\[66\]。2008年2月5日，蘋果公司增加16GB版本\[67\]。

iPhone 3G有8GB和16GB兩種選擇，而iPhone 3GS有16GB及32GB版本，推出三年後的2012年9月更推出8GB版本。

iPhone 4有16GB和32GB兩種選擇，8GB版本以較低價格與iPhone 4S同步出售。iPhone
4S在三種容量可供選擇，分別為16GB、32GB和64GB，當中所有數據都儲存在內置的閃存裝置上。iPhone並不支援透過外置記憶卡或[SIM卡儲存空間](../Page/SIM卡.md "wikilink")。

[GSM的型號iPhone使用](../Page/GSM.md "wikilink")[SIM卡以識別手機於GSM網絡上](../Page/SIM卡.md "wikilink")。第一代iPhone、iPhone
3G及iPhone
3GS的SIM卡安放於手機頂部的插槽的托盤上。SIM卡開啟器\[68\]或[迴紋針都能打開SIM卡托盤](../Page/迴紋針.md "wikilink")，SIM卡開啟器是一個簡單的[液態金屬切片](../Page/液態金屬_\(商品名\).md "wikilink")\[69\]，購買iPhone時隨機附送\[70\]。iPhone
4及iPhone 4S使用[Micro
SIM咭](../Page/Micro_SIM.md "wikilink")，它置於手機的右側插槽\[71\]。[CDMA的iPhone](../Page/CDMA.md "wikilink")
4，如其他只有CDMA制式的手機一樣，只有一個SIM卡插槽或是不使用SIM卡。

iPhone
4S如使用[CDMA的話](../Page/CDMA.md "wikilink")，然而確實需要[SIM卡槽](../Page/SIM卡.md "wikilink")，但這不依賴SIM卡來啟動CDMA網絡。[CDMA啟動的iPhone](../Page/CDMA.md "wikilink")
4S通常有一個電訊商允許的[漫遊SIM卡於購買時預載在其SIM卡槽上](../Page/漫遊.md "wikilink")，只適用於某電訊商認可的國際[GSM網絡](../Page/GSM.md "wikilink")。SIM卡槽是鎖定用於CDMA提供的SIM卡漫遊服務\[72\]。

以[Verizon為例](../Page/Verizon.md "wikilink")，若用戶在過去的60天內信譽良好，可以致電他們的支援電話，要求SIM卡解鎖來作國際通話\[73\]，此方法只能應用於iPhone
4S在國際電訊商作解鎖使用。iPhone
4S使用這個方法來解鎖將會拒絕任何非國際的SIM卡使用（例如[AT\&T或](../Page/AT&T.md "wikilink")[T-Mobile](../Page/T-Mobile.md "wikilink")）。

iPhone 5提供16GB、32GB和64GB的容量選擇，與之前的iPhone 4S一樣。它使用新設計的[Nano
SIM卡](../Page/Nano_SIM.md "wikilink")，體積較[Micro
SIM還要小](../Page/Micro_SIM.md "wikilink")，為的是為內部組件提供更多空間而設，Nano
SIM還適用於[iPad mini](../Page/iPad_mini.md "wikilink")。

iPhone 6提供16GB、64GB和128GB的容量選擇，3年后的2017年推出32GB版本。

iPhone 6S最初提供16GB、64GB和128GB的容量選擇，但iPhone 7推出後將iPhone
6S的16GB和64GB停產，重新推出32GB並且與128GB的容量選擇。

iPhone 7提供32GB、128GB和256GB的容量選擇，與之前的iPhone
6不一樣的是取消了16GB及64GB(除了曜石黑iPhone7及iPhone 7
Plus 僅有128GB以及256GB容量以及iPhone SE仍有16GB和64GB容量)。

iPhone8及iPhone8 Plus以及iPhoneX只有64GB、256GB兩種容量。

从iPhone XS，iPhone XS Plus和iPhone
XR开始支持同时使用兩張SIM卡，一張為SIM卡，另一張為沒有實體的eSIM卡，但是中国、香港及澳门的iPhone
Xs Max和iPhone Xr两种机型可以使用双实体Nano SIM卡。

### 隨機配件

[IPhone_4_box_no_lid.JPG](https://zh.wikipedia.org/wiki/File:IPhone_4_box_no_lid.JPG "fig:IPhone_4_box_no_lid.JPG")
所有iPhone的包裝盒中均包括一份說明文件、與[USB的連接埠及AC充電插座](../Page/USB.md "wikilink")。

第一代iPhone及iPhone
3G也隨機附有清潔屏幕用的抹布。除此之外，還包括一個附有麥克風的立體聲耳機，和一個直立式用於充電及同步功能的塑膠[Dock連接埠](../Page/Dock.md "wikilink")；iPhone
3G包括一個類似上一代的耳機，加上一個SIM卡彈出工具；iPhone
3GS有一個SIM卡彈出工具和經改良的耳機，與之前的耳機不同，新增音量控制功能的按鈕\[74\]\[75\]。

iPhone 3G與iPhone
3GS使用同一款的Dock連接埠，但跟大部分的手機不同的是，充電插座及USB連接埠是可以分開獨立出售的，但不是第一代iPhone的Dock\[76\]。USB連接埠與充電插座的電線為同一條電線\[77\]。連同超小型USB電源適配器的iPhone
3G及iPhone
3GS於[北美](../Page/北美.md "wikilink")、[日本](../Page/日本.md "wikilink")、[哥倫比亞](../Page/哥倫比亞.md "wikilink")、[厄瓜多爾和](../Page/厄瓜多爾.md "wikilink")[秘魯出售](../Page/秘魯.md "wikilink")\[78\]\[79\]。

所有iPhone都包括一個USB電源適配器，或稱為充電插座，讓電話能透過AC充電插座進行充電。

截止到2018年，蘋果公司的iPhone隨機配件如下：

<table>
<thead>
<tr class="header">
<th><p>iPhone機型</p></th>
<th><p>快速入门指南</p></th>
<th><p>抹布</p></th>
<th><p>USB傳輸線</p></th>
<th><p>電源供應器</p></th>
<th><p>SIM卡開啟器</p></th>
<th><p>耳機</p></th>
<th><p>MicroUSB轉接器<br />
（歐洲及中國大陸）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第一代iPhone</p></td>
<td></td>
<td></td>
<td><p>30針USB傳輸線</p></td>
<td></td>
<td></td>
<td><p>附有麥克風的耳機</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>iPhone 3G</p></td>
<td></td>
<td></td>
<td><p>30針USB傳輸線</p></td>
<td></td>
<td></td>
<td><p>附有麥克風的耳機</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>iPhone 3GS</p></td>
<td></td>
<td></td>
<td><p>30針USB傳輸線</p></td>
<td></td>
<td></td>
<td><p>附有麥克風的耳機</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>iPhone 4</p></td>
<td></td>
<td></td>
<td><p>30針USB傳輸線</p></td>
<td></td>
<td></td>
<td><p>附有麥克風的耳機</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>iPhone 4s</p></td>
<td></td>
<td></td>
<td><p>30針USB傳輸線</p></td>
<td></td>
<td></td>
<td><p>附有麥克風的耳機</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>iPhone 5</p></td>
<td></td>
<td></td>
<td><p>Lightning傳輸線</p></td>
<td></td>
<td></td>
<td><p>附有麥克風的EarPods</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>iPhone 5c</p></td>
<td></td>
<td></td>
<td><p>Lightning傳輸線</p></td>
<td></td>
<td></td>
<td><p>附有麥克風的EarPods</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>iPhone 5s</p></td>
<td></td>
<td></td>
<td><p>Lightning傳輸線</p></td>
<td></td>
<td></td>
<td><p>附有麥克風的EarPods</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>iPhone 6</p></td>
<td></td>
<td></td>
<td><p>Lightning傳輸線</p></td>
<td></td>
<td></td>
<td><p>附有麥克風的EarPods</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>iPhone 6 Plus</p></td>
<td></td>
<td></td>
<td><p>Lightning傳輸線</p></td>
<td></td>
<td></td>
<td><p>附有麥克風的EarPods</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>iPhone 6s</p></td>
<td></td>
<td></td>
<td><p>Lightning傳輸線</p></td>
<td></td>
<td></td>
<td><p>附有麥克風的EarPods</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>iPhone 6s Plus</p></td>
<td></td>
<td></td>
<td><p>Lightning傳輸線</p></td>
<td></td>
<td></td>
<td><p>附有麥克風的EarPods</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>iPhone SE</p></td>
<td></td>
<td></td>
<td><p>Lightning傳輸線</p></td>
<td></td>
<td></td>
<td><p>附有麥克風的EarPods</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>iPhone 7</p></td>
<td></td>
<td></td>
<td><p>Lightning傳輸線</p></td>
<td></td>
<td></td>
<td><p>附有麥克風的Lightning EarPods</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>iPhone 7 Plus</p></td>
<td></td>
<td></td>
<td><p>Lightning傳輸線</p></td>
<td></td>
<td></td>
<td><p>附有麥克風的Lightning EarPods</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>iPhone 8</p></td>
<td></td>
<td></td>
<td><p>Lightning傳輸線</p></td>
<td></td>
<td></td>
<td><p>附有麥克風的Lightning EarPods</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>iPhone 8 Plus</p></td>
<td></td>
<td></td>
<td><p>Lightning傳輸線</p></td>
<td></td>
<td></td>
<td><p>附有麥克風的Lightning EarPods</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>iPhone X</p></td>
<td></td>
<td></td>
<td><p>Lightning傳輸線</p></td>
<td></td>
<td></td>
<td><p>附有麥克風的Lightning EarPods</p></td>
<td></td>
</tr>
</tbody>
</table>

## 軟體

iPhone运行由蘋果公司研發的iOS作業系统（前身稱為iPhone OS）\[80\]\[81\]\[82\]，它是由[Apple
Darwin的核心發展出來的變體](../Page/Apple_Darwin.md "wikilink")，程式也可見於[Mac OS
X](../Page/Mac_OS_X.md "wikilink")，亦包括在[Mac OS
X](../Page/Mac_OS_X.md "wikilink") 10.5 Leopard的「Core
Animation」的軟體組件，負責在使用者界面上提供平滑順暢的[動畫效果](../Page/動畫.md "wikilink")。與[PowerVR硬件一同運作的話](../Page/PowerVR.md "wikilink")，它是負責運作介面動作畫面的。iPhone自带苹果开发的应用程序，并支持通过App
Store来下载第三方程序。

苹果通过无线或iTunes为iPhone搭载的iOS提供免费更新。\[83\]

### 用户界面

[IOS7-CHN.png](https://zh.wikipedia.org/wiki/File:IOS7-CHN.png "fig:IOS7-CHN.png")下載其他開發者的應用程式，建立剪輯、圖示重新排序、建立及刪除資料夾。\]\]
iPhone主螢幕畫面是建基於[圖形使用者界面](../Page/圖形使用者界面.md "wikilink")，可用的應用程式以圖形化列表顯示，通常每次只運作一个，从iPhone
4开始，多任务的雏形诞生，用户可以双击home键以选择最近打开的应用程序，但在ios7之后，应用程序真正实现了多任务，每个应用程序都可以在后台运行。在iPhone
X中，可以通过手势来关闭应用或切换程序。

在預設的情況下，主畫面包括以下圖示：消息（[SMS和](../Page/SMS.md "wikilink")[MMS](../Page/MMS.md "wikilink")）、日曆、相片、相機、[YouTube](../Page/YouTube.md "wikilink")（中国大陆版本是“视频”应用）、股票、[Safari瀏覽器](../Page/Safari.md "wikilink")、地圖（iOS6后的是[苹果地图](../Page/苹果地图.md "wikilink")）（中国大陆版本是[高德地图](../Page/高德地圖.md "wikilink")）、天氣、語音備忘錄、备忘录、時鐘、計算機、設定、[iTunes商店](../Page/iTunes商店.md "wikilink")（中国大陆版本被取消）和[App
Store](../Page/App_Store.md "wikilink")，还有FaceTime和其他程序。使用者還可以在主畫面的Dock上增加或刪除圖示，在主屏幕内，使用者可左右撥動進行翻頁操作，但Dock上圖示不會隨之移動。使用者可隨時透過長按第三方應用程式，進行移動及刪除，或重按图标执行其他指令（要求机型至少是iPhone
6s），苹果自带的应用程序在iOS10或以上就可以删除，在屏幕中央下拉会出现搜尋功能[Spotlight](../Page/Spotlight.md "wikilink")。\[84\]\[85\]。

2008年1月15日，苹果公司发布了iPhone OS1.1.3,允许用用户创建“Wed
Clips”，用户可以在九个其他相邻的主屏幕三重新放置和排列图标（按住图标待到它们抖动时候，就可以移动并放置了），可以通过水平滑动到另一页。

在应用中，通過iPhone交互技術，使用者能夠把手指向上和向下撥來看網頁或文字的更多內容，例如，把兩根手指置於螢幕上作開合動作就能縮放網頁和相片，這種技術稱為「[捏](../Page/多點觸控.md "wikilink")」，或者重按某个网页的链接，就可以预览该网页（支持至少iPhone6s以上机型）。

其他[以使用者而設的](../Page/以用户为中心的设计.md "wikilink")[互動效果包括季水平滑動選擇](../Page/互動設計.md "wikilink")、垂直滑動鍵盤和書籤選單，及鎖定方向，在沒有鎖定時容許畫面隨著裝置的方向而改變。選單欄在必要時設於螢幕的頂部及底部，它們會根據應用程序的不同而有所分別，但會依照一貫的風格顯示。在選單的層次結構中，向左滑动可以返回，也可以点击左上角的“返回”字样。

### 通話

iPhone支援電話會議、暫停通話、來電顯示、合併通話等功能，並整合iPhone與其他網絡功能，例如音樂播放時接到來電，此時音樂淡出，並於通話結束後音樂繼續播放。

近距感應器會因通話時臉部貼近電話時關閉屏幕和觸感電路，以節省電源並防止無意觸碰。在iPhone4之前的型號並不支援視像通話或視像會議\[86\]，後來蘋果公司於iPhone
4的設計上加裝一個前置鏡頭，才開始支援視像通話\[87\]。iPhone
4發布後，蘋果公司推出[FaceTime視像通話應用程式](../Page/FaceTime.md "wikilink")\[88\]，讓iPhone用户利用[Wi-Fi或數據網絡進行視像通話](../Page/Wi-Fi.md "wikilink")。初時僅支援通過第三方應用程式的語音撥號\[89\]。Siri和语音控制可讓用戶語音說出聯絡人姓名或電話號碼並直接撥號打出\[90\]。iPhone還包括語音信箱功能（某些國家或地區適用）\[91\]，讓用戶只要直接查看屏幕上的語音留言清單，而不需致電語音信箱進行操作。

2007年9月5日，蘋果公司在[美國推出音樂](../Page/美國.md "wikilink")[鈴聲](../Page/鈴聲.md "wikilink")，用戶能夠從歌曲中自行製作鈴聲，或從iTunes商店以小額額外費用就能購買。歌曲可以從一首歌曲中任何部分抽出來，長度為3至30秒\[92\]，可以加入淡入及淡出效果，當循環播放時可設置半秒至5秒的停頓，或是循環播放。所有定製可於iTunes中\[93\]或以蘋果公司的[GarageBand](../Page/GarageBand.md "wikilink")
4.1.1或以上版本中完成製作（僅用於[Mac OS
X](../Page/Mac_OS_X.md "wikilink")）\[94\]，第三方應用程式也能達到\[95\]。

2012年9月19日發布[iOS
6](../Page/iOS_6.md "wikilink")，蘋果公司新增用戶能選擇拒絕來電的功能，他們可以選擇以訊息回覆，或設定提醒功能於稍後時間回覆\[96\]。

iPhone 7通话时被爆会有电流声，引起了用户的诟病。\[97\]

### 多媒體

iPhone提供Apple
Music三个月的免费试用\[98\]，支援AAC、MP3、AAX、WAV等格式的音樂及MP4影片格式的输入，而用戶需要透過iTunes才能把電腦的多媒體檔案傳送至iPhone。除了播放列表外，所有選項都是按字母順序排列。

蘋果公司允許用戶直接從[iTunes
Store購買和下載正版音樂到他們的iPhone](../Page/iTunes_Store.md "wikilink")，或付费订阅Apple
Music。

苹果截止到2018年7月，有两款官方耳机产品，一款是有线耳机[Earpods](../Page/Earpods.md "wikilink")，一款是无线耳机[Airpods](../Page/AirPods.md "wikilink")，还有一款是针对没有耳机孔的iPhone机型推出的lightning接口的Earpods。

iPhone 5s及后续机型且运行至少iOS11.4系统的手机可以将Apple
Music上的音乐放到HomePod上收听（中国大陆版HomePod需要至少IOS12系统的手机）。

#### Flash功能

由於iPhone並不支援[Flash](../Page/Flash.md "wikilink")，所以iPhone上的YouTube是直接連結影片格式（flv,
MPEG4 AVC Video+ AAC Audio檔案）進行播放\[99\]。

### 通讯与网络

第一代iPhone能夠連接[Wi-Fi或以](../Page/Wi-Fi.md "wikilink")[2G和](../Page/2G.md "wikilink")[3G](../Page/3G.md "wikilink")（[GSM或](../Page/GSM.md "wikilink")[EDGE](../Page/EDGE.md "wikilink")）網絡連上互聯網。[iPhone
3G推出時支援第三代流動通訊](../Page/iPhone_3G.md "wikilink")[UMTS和](../Page/UMTS.md "wikilink")[HSUPA](../Page/HSUPA.md "wikilink")
3.6\[100\]，[iPhone 3GS及](../Page/iPhone_3GS.md "wikilink")[iPhone
4支援](../Page/iPhone_4.md "wikilink")[HSUPA](../Page/HSUPA.md "wikilink")
7.2\[101\]，只有iPhone
4S支援[HSUPA](../Page/HSUPA.md "wikilink")（14.4Mbit/s）。[AT\&T早在](../Page/AT&T.md "wikilink")2004年7月推出3G網絡\[102\]，但喬布斯於2007年下旬指3G網絡在美國仍未有被廣泛普及，以及iPhone上的晶片能源效率不夠高\[103\]\[104\]，而且生產價錢不菲，故決定第一代iPhone不支援3G網絡\[105\]\[106\]。在2.0更新版本中，加入一些大學或企業常用的認證系統，支援[IEEE
802.1X](../Page/IEEE_802.1X.md "wikilink")\[107\]。

在點認的情況下，iPhone會詢問用戶加入新發現的[Wi-Fi網絡](../Page/Wi-Fi.md "wikilink")\[108\]，並需要密碼加入，另外，它也可以手動關閉Wi-Fi網絡\[109\]，它會自動按照訊號強度來排序，並在可用時選擇最強訊號的Wi-Fi網絡，而不是[EDGE網絡](../Page/EDGE.md "wikilink")\[110\]。同樣地，iPhone3G、iPhone3GS及iPhone4可以選擇[2G和](../Page/2G.md "wikilink")[3G或](../Page/3G.md "wikilink")[Wi-Fi](../Page/Wi-Fi.md "wikilink")\[111\]。

在流動數據網絡下，電郵附件及由蘋果公司和各種的網上商店每次的下載量必須少於20MB\[112\]。容量較大的檔案，經常接收電郵附件及播客，都需要在Wi-Fi網絡下載，因為這裡沒有檔案容量的限制。若Wi-Fi網絡不適用，解決辦法就是在[Safari中開啟檔案](../Page/Safari.md "wikilink")\[113\]

iPhone 3GS和iPhone
4可以結合地圖為重心的數碼指南針\[114\]。蘋果公司開發一個讓用戶在iPhone上觀看[YouTube短片的獨立應用程式](../Page/YouTube.md "wikilink")，它以[H.264編碼](../Page/H.264/MPEG-4_AVC.md "wikilink")，需要互聯網的支援才能觀看影片。

iPhone用戶可以經常在任何地方進入互聯網，根據[谷歌於](../Page/谷歌.md "wikilink")2008年的調查，iPhone是搜尋次數最多的手機品牌，超過其他品牌手機搜尋次數的50倍\[115\]，而內置的天氣和股市報價程式都需要互聯網的支援。

根據[德國電信的CEO](../Page/德國電信.md "wikilink")
稱，「iPhone用戶的平均互聯網使用率是超過100MB，這是我們基本合約客戶平均用量的30倍\[116\]。」[尼爾森公司研究發現](../Page/尼爾森NV.md "wikilink")，有98%的iPhone用戶會使用2G、3G及Wi-fi數據服務，以及當中的88%會使用iPhone上網\[117\]。但在[中國方面](../Page/中國.md "wikilink")，iPhone
3G和iPhone 3GS都沒有建構和分發給[Wi-Fi網絡](../Page/Wi-Fi.md "wikilink")\[118\]。

2008年1月的軟件更新\[119\]允許第一代iPhone使用手機訊號塔與Wi-Fi網絡的位置進行「定位」\[120\]，但由於缺乏[GPS硬件配合](../Page/GPS.md "wikilink")，iPhone
3G之後的智能電話採用[AGPS](../Page/AGPS.md "wikilink")，此技術由[美國營運](../Page/美國.md "wikilink")。由於iPhone
4S支援[格洛納斯全球定位系統](../Page/格洛納斯.md "wikilink")，此技術由[俄羅斯營運](../Page/俄羅斯.md "wikilink")。

2011年1月，[verizon引進iPhone銷售](../Page/威訊.md "wikilink")，引起公眾注意的是手機已可連接互聯網。在兩個美國電訊商的發展下，AT\&T網絡已可同時使用電話及互聯網，verizon網絡只支援兩者單獨使用\[121\]。

2013的WWDC中，苹果介绍了能通过蓝牙无线的传输文件的[AirDrop](../Page/隔空投送.md "wikilink")。

2014年1月17日苹果公司发布了支持中国移动的3G和4G的iPhone 5s和iPhone
5c。此前iPhone并不支持中国移动。\[122\]

2015年9月30日，苹果公司正式向中国大陆用户提供[Apple
Music](../Page/Apple_Music.md "wikilink")、[iTunes电影](../Page/iTunes电影.md "wikilink")、[iBooks](../Page/Apple_Books.md "wikilink")。

2016年4月，苹果公司停止向中国大陆用户提供iTunes电影与iBooks，有媒体认为是版权问题\[123\]，也有媒体认为是中国共产党利用监管部门关闭了iTunes电影与iBooks。\[124\]

到了2018年发布的iPhone X与iPhone 8和iPhone 8
Plus则支持了日本的[准天顶卫星系统](../Page/準天頂衛星系統.md "wikilink")。

### 文字输入

對於文字輸入，iPhone於觸控屏幕中內置[虚擬鍵盤](../Page/虚擬鍵盤.md "wikilink")，支援拼寫檢查及改正、預測輸入字功能，和用戶能加入新詞的動態字典，iOS11或以后的操作系统Siri可以预判用户想要输入的内容。\[125\]。橫向模式時，按鍵會變得稍大和間隔稍遠，更方便於文字輸入，但只得到少量應用程式支援。

iPhone支援多國語言輸入法\[126\]，可以輸入21種語言\[127\]。iPhone可以輸入重音符號的字詞，只要按著字母兩秒便可，再從彈出的選擇中揀選適合的字便完成操作\[128\]。

### 邮件與短訊

iPhone設有支援[HTML電郵程式](../Page/HTML.md "wikilink")，讓用戶把檔案嵌入電郵訊息中，包括相片、[PDF](../Page/PDF.md "wikilink")、[Word](../Page/Microsoft_Word.md "wikilink"),
[Excel](../Page/Microsoft_Excel.md "wikilink"), and
[PowerPoint附件](../Page/Microsoft_PowerPoint.md "wikilink")，還可在電話中檢視\[129\]。

iPhone第一個版本的韌體，是透過[Microsoft
Exchange伺服器上開放IMAP來運作](../Page/Microsoft_Exchange_Server.md "wikilink")。蘋果公司也容許（至2010年7月）支援平台發布的iPhone2.0韌體（包括電郵推送）\[130\]\[131\]。iPhone會把電郵帳號的設定同步至蘋果公司的[電郵應用程式](../Page/Mail.md "wikilink")、[Microsoft
Outlook和](../Page/Microsoft_Outlook.md "wikilink")[Microsoft
Entourage](../Page/Microsoft_Entourage.md "wikilink")，或可以本機手動設置。如設定正確，電郵程式能進入多個IMAP或POP3帳戶\[132\]。

短訊的排列模式類似於郵箱的格式，所有短訊都是把回覆對話置於同一版面中，界面以對話氣泡顯示（與[iChat類似](../Page/iChat.md "wikilink")），根據每位收件人姓名排列，於同一個版面中顯示出來。iPhone內置支援電郵的轉發、草稿及把圖片嵌入一併發送功能。在1.1.3版本更新中加入支援多個收件人功能\[133\]。

### 第三方應用程式

在2007年6月11日的WWDC上，時任蘋果公司CEO的喬布斯公布iPhone將會支援第三方應用程式\[134\][Ajax](../Page/AJAX.md "wikilink")，用以分享iPhone界面的外觀\[135\]。2007年10月17日，喬布斯在蘋果公司"熱點新聞"的日誌中張貼一封公開信，公布於2008年2月將會提供一個[軟件開發工具包](../Page/軟件開發工具包.md "wikilink")（SDK）予第三方軟件開發者\[136\]。

iPhone的SDK於2008年3月6日正式發布\[137\]。這是一個需要向蘋果公司註冊的免費下載軟件，它讓軟件開發者進行iPhone的開發，然後在「iPhone模擬器」中進行測試。然而，在支付的會員費後才可以把研發的應用程式載入及套用到真實的裝置。應用程式開發者可以在軟件於[App
Store
(iOS)上架時任意為自己的程式定價](../Page/App_Store_\(iOS\).md "wikilink")，而他們將獲得收益的70%，而蘋果公司將佔收入的30%\[138\]。應用程式開發者亦可以選擇發布免費的應用程式，在會員費後不需要支付發布的任何費用。

[應用程式商店於](../Page/App_Store.md "wikilink")2008年7月11日連同預載iOS 2.0的iPhone
3G一同發布\[139\]，連同保養首次一同支援。iPhone用戶能夠免費更新。\[140\]。

从2010年开始，截止到2018年5月31日，App Store应用下载量达到了1700亿次。\[141\]

### Siri

[Siri是自iPhone](../Page/Siri.md "wikilink")
4S以后就搭载在iPhone上的语音助手，与2012年9月支持中文，在iOS7中它的声音得到了优化，同时加入了男声，在iOS11中它的声音再次得到优化，且在iOS11中它开对用户的行为作出判断，如在短信中或是在搜索中，它都会给出建议，同时它的图标也被改变了。在iOS12中，苹果公司推出了Siri
Shortcuts的功能，搭配[Workflow](../Page/Workflow.md "wikilink")，给用户通过语音自定义快捷方式。\[142\]

### Apple Pay

这是苹果在2014年秋季发布会所推出的移动支付和电子现金服务，集成在Apple
Wallet应用中，适用于iPhone6或后续的iPhone，在锁屏界面按下home键就可以打开[Apple
Pay](../Page/Apple_Pay.md "wikilink")（在iPhone
X及以后机型中是双击侧边按键），在中国大陆的一些城市的付费设施与一些商店支持Apple
Pay。

### 其他

#### iCloud和iMessage

iCloud的前身是[mobileme](../Page/MobileMe.md "wikilink")，iCloud附带5GB存储空间，能付费扩充，曾在2017年降价过一次\[143\]，可以存储用户的部分第三方应用数据、照片、电邮等。iMessage是苹果推出的即时通讯软件，要求聊天双方都使用苹果设备，不同于短信与彩信，它是免费的，在iOS10的中，苹果允许第三方开发者制作iMessage的表情\[144\]。在iOS11的更新中，iMessage可以在美国使用Apple
Pay转账。\[145\]

#### Handoff和Game Center

Handoff（接力）在iOS8中首次發佈，该功能可以在iOS8及以上的任意一台苹果裝置（Mac需要yosemite或更高版本）中开始一项工作，让后继续在另外一台iOS8或以上的苹果裝置中继续进行。[Game
Center是专门为游戏玩家设计的社交網路平台](../Page/Game_Center.md "wikilink")，发布于iOS4.1，开始是作为独立的應用程式出现，到iOS10后成为一种服务存在。

#### 易用性

iPhone為不同人士提供一個[無障礙環境](../Page/無障礙環境.md "wikilink")，例如可以把文字放大來協助視障人士\[146\]和使用[隱藏字幕和外置](../Page/隱藏字幕.md "wikilink")來協助聽障人士\[147\]。iPhone
3GS亦有為視障人士而設的黑底白字的模式、縮放功能、[VoiceOver](../Page/VoiceOver.md "wikilink")（[屏幕閱讀器](../Page/屏幕閱讀器.md "wikilink")）、為聽障人士而設的[單聲道功能](../Page/單聲道.md "wikilink")（適用於單耳有限聽力人士）\[148\]。蘋果公司會根據美國法例，自願性定期發布輔助功能的模板\[149\]。

### 規格一覽表

|                                       |                                     |                                  |
| ------------------------------------- | ----------------------------------- | -------------------------------- |
| <abbr title="現正出售的版本"發售中>已停止發售</abbr> | <abbr title="現正出售的版本">軟體有限支援</abbr> | <abbr title="現正出售的版本">發售中</abbr> |

<table>
<thead>
<tr class="header">
<th><p>型號</p></th>
<th><p><strong>IPhone</strong></p></th>
<th><p><a href="../Page/iPhone_3G.md" title="wikilink">iPhone 3G</a></p></th>
<th><p><a href="../Page/iPhone_3GS.md" title="wikilink">iPhone 3GS</a></p></th>
<th><p><a href="../Page/iPhone_4.md" title="wikilink">iPhone 4</a></p></th>
<th><p><a href="../Page/iPhone_4S.md" title="wikilink">iPhone 4S</a></p></th>
<th><p><a href="../Page/iPhone_5.md" title="wikilink">iPhone 5</a></p></th>
<th><p><a href="../Page/iPhone_5C.md" title="wikilink">iPhone 5C</a></p></th>
<th><p><a href="../Page/iPhone_5S.md" title="wikilink">iPhone 5S</a></p></th>
<th><p><a href="../Page/iPhone_6.md" title="wikilink">iPhone 6</a></p></th>
<th><p><a href="../Page/iPhone_6_Plus.md" title="wikilink">iPhone 6 Plus</a></p></th>
<th><p><a href="../Page/iPhone_6S.md" title="wikilink">iPhone 6S</a></p></th>
<th><p><a href="../Page/iPhone_6S_Plus.md" title="wikilink">iPhone 6S Plus</a></p></th>
<th><p><a href="../Page/iPhone_SE.md" title="wikilink">iPhone SE</a></p></th>
<th><p><a href="../Page/iPhone_7.md" title="wikilink">iPhone 7</a></p></th>
<th><p><a href="../Page/iPhone_7_Plus.md" title="wikilink">iPhone 7 Plus</a></p></th>
<th><p><a href="../Page/iPhone_8.md" title="wikilink">iPhone 8</a></p></th>
<th><p><a href="../Page/iPhone_8_Plus.md" title="wikilink">iPhone 8 Plus</a></p></th>
<th><p><a href="../Page/iPhone_X.md" title="wikilink">iPhone X</a></p></th>
<th><p><a href="../Page/IPhone_XS.md" title="wikilink">iPhoneXs</a></p></th>
<th><p><strong>iPhone XR</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>rowspan="1"  class="table-rh" |<a href="../Page/預載作業系統.md" title="wikilink">預載作業系統</a></p></td>
<td><p>iOS 1.0</p></td>
<td><p>iOS 2.0</p></td>
<td><p>iOS 3.0</p></td>
<td><p>iOS 4.0</p></td>
<td><p>iOS 5.0</p></td>
<td><p>iOS 6.0</p></td>
<td><p>iOS 7.0</p></td>
<td><p>iOS 7.0</p></td>
<td><p>iOS 8.0</p></td>
<td><p>iOS 9.0</p></td>
<td><p>iOS 9.3</p></td>
<td><p>iOS 10.0</p></td>
<td><p>iOS 11.0</p></td>
<td><p>iOS 12.0</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>rowspan="1"  class="table-rh" |<small>升至最新iOS版本</small></p></td>
<td><p>iPhone OS 3.1.3</p></td>
<td><p>iOS 4.2.1</p></td>
<td><p>iOS 6.1.6</p></td>
<td><p>iOS 7.1.2</p></td>
<td><p>iOS 9.3.5</p></td>
<td><p>iOS 10.3.3</p></td>
<td><p>iOS 10.3.3</p></td>
<td><p>iOS 12.1</p></td>
<td><p>iOS 12.1</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>rowspan="1"  class="table-rh" |<a href="../Page/顯示器.md" title="wikilink">顯示器</a></p></td>
<td><p><small>8.9厘米（3.5吋），<br />
<a href="../Page/長寬比.md" title="wikilink">長寬比為</a>3：2[150]，<br />
多點觸控螢幕，<br />
螢幕由防刮鏡面玻璃覆蓋[151]，<br />
262,144色（18位元）LCD螢幕，<br />
480×320像素（HVGA），<br />
解析度163 PPI，<br />
200:1對比度。</small></p></td>
<td><p><small>跟之前型號一樣，設有耐指紋疏油塗層[152]，262,144色（18位元）LCD液晶體螢幕[153]。</small></p></td>
<td><p><small>8.9厘米（3.5吋），<a href="../Page/長寬比.md" title="wikilink">長寬比為</a>3：2，多點觸控螢幕，螢幕由矽酸鋁玻璃覆蓋，16,777,216色（24位元）IPS LCD液晶體螢幕，960×640像素，解像度326 PPI，800:1對比度，500 cd⁄m²最大亮度。</small></p></td>
<td><p>10.2厘米（4吋），<a href="../Page/長寬比.md" title="wikilink">長寬比為</a>16:9，1136×640像素，解像度326 PPI，<br />
搭載In Cell技術的多點觸控螢幕。</p></td>
<td><p>4.7吋，<a href="../Page/長寬比.md" title="wikilink">長寬比為</a>16:9，1334×750像素，解像度326 PPI，<br />
搭載In Cell技術的多點觸控螢幕。</p></td>
<td><p>5.5吋，<a href="../Page/長寬比.md" title="wikilink">長寬比為</a>16:9，1920×1080像素，解像度401 PPI，<br />
搭載In Cell技術的多點觸控螢幕。</p></td>
<td><p>4.7吋，<a href="../Page/長寬比.md" title="wikilink">長寬比為</a>16:9，1334×750像素，解像度326 PPI，<br />
搭載In Cell技術的多點觸控螢幕。</p></td>
<td><p>5.5吋，<a href="../Page/長寬比.md" title="wikilink">長寬比為</a>16:9，1920×1080像素，解像度401 PPI，<br />
搭載In Cell技術的多點觸控螢幕。</p></td>
<td><p>10.2厘米（4吋），<a href="../Page/長寬比.md" title="wikilink">長寬比為</a>16:9，1136×640像素，解像度326 PPI，<br />
搭載In Cell技術的多點觸控螢幕。</p></td>
<td><p>4.7吋，<a href="../Page/長寬比.md" title="wikilink">長寬比為</a>16:9，1334×750像素，解像度326 PPI，<br />
搭載In Cell技術的多點觸控螢幕。</p></td>
<td><p>5.5吋，<a href="../Page/長寬比.md" title="wikilink">長寬比為</a>16:9，1920×1080像素，解像度401 PPI，<br />
搭載In Cell技術的多點觸控螢幕。</p></td>
<td><p>4.7吋，<a href="../Page/長寬比.md" title="wikilink">長寬比為</a>16:9，1334×750像素，解像度326 PPI，<br />
搭載In Cell技術的多點觸控螢幕。</p></td>
<td><p>5.5吋，<a href="../Page/長寬比.md" title="wikilink">長寬比為</a>16:9，1920×1080像素，解像度401 PPI，<br />
搭載In Cell技術的多點觸控螢幕。</p></td>
<td><p>5.8吋，2436×1125像素，解像度458 PPI，<br />
搭載全螢幕 OLED Multi-Touch 顯示器。</p></td>
<td><p>6.1吋，1792×828像素，解像度326 PPI，<br />
搭載全螢幕 LCD Multi-Touch 顯示器。</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>class="table-rh" |<a href="../Page/容量.md" title="wikilink">容量</a></p></td>
<td><p>4, 8或16 <a href="../Page/Gigabyte.md" title="wikilink">GB</a></p></td>
<td><p>8或16 GB</p></td>
<td><p>8, 16或32 GB</p></td>
<td><p>8, 16, 32或64 GB</p></td>
<td><p>16, 32或64 GB</p></td>
<td><p>8, 16或32 GB</p></td>
<td><p>16, 32或64 GB</p></td>
<td><p>16, 64或128 GB</p></td>
<td><p>16, 64或128 GB</p></td>
<td><p>16, 64或128 GB</p></td>
<td><p>16, 64或128 GB</p></td>
<td><p>32或128 GB</p></td>
<td><p>32, 128或256 GB</p></td>
<td><p>64或256 GB</p></td>
<td><p>64,256或512 GB</p></td>
<td><p>64,128或256 GB</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>class="table-rh" |<a href="../Page/中央處理器.md" title="wikilink">中央處理器</a></p></td>
<td><p>620 <a href="../Page/Hertz.md" title="wikilink">MHz</a>（<a href="../Page/降頻.md" title="wikilink">降頻至</a>412MHz）<br />
<a href="../Page/Samsung.md" title="wikilink">Samsung</a> <a href="../Page/32-bit.md" title="wikilink">32-bit</a> <a href="../Page/精简指令集.md" title="wikilink">RISC</a> <a href="../Page/ARM_architecture.md" title="wikilink">{{nowrap</a>[154][155]<br />
（32 <a href="../Page/kilobyte.md" title="wikilink">KB</a><a href="../Page/CPU缓存.md" title="wikilink">L1</a>）v1.0[156]</p></td>
<td><p>833 <a href="../Page/MHz.md" title="wikilink">MHz</a><br />
（降頻至600MHz）<br />
<a href="../Page/ARM_architecture.md" title="wikilink">{{nowrap</a>[157][158]<br />
Samsung S5PC100[159]<br />
（64 KB L1 + 256 KB L2）</p></td>
<td><p>1 GHz<br />
（降頻至800MHz）[160]<br />
<a href="../Page/ARM_architecture.md" title="wikilink">{{nowrap</a><br />
<a href="../Page/Apple_A4.md" title="wikilink">Apple A4</a>（<a href="../Page/系统芯片.md" title="wikilink">SoC</a>）[161]</p></td>
<td><p>1 GHz<br />
（降頻至800MHz）[162]<br />
<a href="../Page/ARM_architecture.md" title="wikilink">{{nowrap</a><br />
<a href="../Page/雙核心.md" title="wikilink">雙核心</a><a href="../Page/Apple_A5.md" title="wikilink">Apple A5</a>（<a href="../Page/系统芯片.md" title="wikilink">SoC</a>）[163]</p></td>
<td><p>1.3 GHz <a href="../Page/雙核心.md" title="wikilink">雙核心</a><br />
<a href="../Page/Apple_A6.md" title="wikilink">Apple A6處理器</a><br />
32位元手機</p></td>
<td><p>1.3 GHz <a href="../Page/雙核心.md" title="wikilink">雙核心</a><br />
<a href="../Page/Apple_A7.md" title="wikilink">Apple A7處理器</a><br />
（搭配M7協處理器）<br />
64位元手機</p></td>
<td><p><a href="../Page/Apple_A8.md" title="wikilink">Apple A8處理器</a><br />
（搭配M8協處理器）<br />
64位元手機</p></td>
<td><p><a href="../Page/Apple_A9.md" title="wikilink">Apple A9處理器</a><br />
（搭配嵌入式 M9 動作感應協同處理器）<br />
64位元手機</p></td>
<td><p><a href="../Page/Apple_A10.md" title="wikilink">Apple A10</a> Fusion處理器 (搭配嵌入式 M10 動作感應協同處理器)</p>
<p>64 位元手機</p></td>
<td><p><a href="../Page/Apple_A11.md" title="wikilink">Apple A11</a> Bionic處理器 (搭配嵌入式 M11 動作感應協同處理器)</p>
<p>64 位元手機</p></td>
<td><p><a href="../Page/Apple_A12_Bionic.md" title="wikilink">Apple A12</a> Bionic處理器(搭配嵌入式 M12 動作感應協同處理器 64為</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>class="table-rh" |<a href="../Page/匯流排.md" title="wikilink">匯流排</a></p></td>
<td><p>103 MHz（32位元）</p></td>
<td><p>100 MHz（32位元）</p></td>
<td><p>100 MHz（32位元）</p></td>
<td><p>250 MHz（64位元）</p></td>
<td></td>
<td></td>
<td></td>
<td><p>1.39 GHz (64位元)</p></td>
<td><p>1.8 GHz (64位元)</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>class="table-rh" |<a href="../Page/圖形處理器.md" title="wikilink">圖形處理器</a></p></td>
<td><p><a href="../Page/PowerVR.md" title="wikilink">PowerVR</a> <a href="../Page/PowerVR#MBX.md" title="wikilink">MBX</a> Lite 3D <a href="../Page/圖形處理器.md" title="wikilink">GPU</a>[164]（103 MHz）</p></td>
<td><p><a href="../Page/PowerVR.md" title="wikilink">PowerVR</a> <a href="../Page/PowerVR#Series5_(SGX).md" title="wikilink">SGX535</a> GPU<br />
（iPhone 3GS: 150 MHz;）<br />
（iPhone 4: 200 MHz）[165][166][167]</p></td>
<td><p><a href="../Page/PowerVR.md" title="wikilink">PowerVR</a> <a href="../Page/PowerVR#Series5_(SGX).md" title="wikilink">SGX543MP2</a><br />
（<a href="../Page/雙核心.md" title="wikilink">雙核心</a>200MHz）GPU[168]</p></td>
<td><p><a href="../Page/PowerVR.md" title="wikilink">PowerVR</a> SGX543MP3<br />
（<a href="../Page/多核心處理器.md" title="wikilink">多核心處理器</a>266 MHz）GPU</p></td>
<td><p><a href="../Page/PowerVR.md" title="wikilink">PowerVR</a> <a href="../Page/PowerVR#Series_6_(Rogue).md" title="wikilink">G6430</a>（4個<a href="../Page/計算機叢集.md" title="wikilink">計算機叢集</a>）[169]</p></td>
<td><p><a href="../Page/PowerVR#Series_6.md" title="wikilink">PowerVR G6450</a>（4個<a href="../Page/計算機叢集.md" title="wikilink">計算機叢集</a>)</p></td>
<td><p><a href="../Page/PowerVR#Series_7.md" title="wikilink">PowerVR GT7600</a>（6個<a href="../Page/計算機叢集.md" title="wikilink">計算機叢集</a>)</p></td>
<td><p><a href="../Page/PowerVR#Series7XT_.28Rogue.29.md" title="wikilink">PowerVR Series 7XT GT7600 Plus</a> （6個<a href="../Page/計算機叢集.md" title="wikilink">計算機叢集</a>)</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>class="table-rh" |<a href="../Page/主記憶體.md" title="wikilink">主記憶體</a></p></td>
<td><p>128 <a href="../Page/megabyte.md" title="wikilink">MB</a> <a href="../Page/Mobile_DDR.md" title="wikilink">LPDDR</a> <a href="../Page/動態隨機存取存储器.md" title="wikilink">DRAM</a>[170]（137 MHz）</p></td>
<td><p><a href="../Page/Mobile_DDR.md" title="wikilink">LPDDR</a> DRAM[171][172]<br />
（200 MHz）</p></td>
<td><p><a href="../Page/Mobile_DDR.md" title="wikilink">LPDDR</a>2 DRAM[173][174][175][176][177]<br />
（200 MHz）</p></td>
<td><p>1 GB DRAM LPDDR2 DRAM[178]</ref>[179]</p></td>
<td><p>1 GB DRAM LPDDR3 DRAM[180]</p></td>
<td><p>1 GB DRAM LPDDR3 DRAM[181]</p></td>
<td><p>1 GB DRAM LPDDR3 DRAM[182]</p></td>
<td><p>2 GB LPDDR4 DRAM</p></td>
<td><p>3 GB LPDDR4 DRAM</p></td>
<td><p>2 GB LPDDR4 DRAM</p></td>
<td><p>3 GB LPDDR4 DRAM</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>class="table-rh" |<strong><a href="../Page/連接器.md" title="wikilink">連接器</a></strong></p></td>
<td><p><a href="../Page/Universal_Serial_Bus.md" title="wikilink">{{nowrap</a> </p></td>
<td><p><a href="../Page/Lightning接頭.md" title="wikilink">Lightning連接器</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>class="table-rh" |<strong>網路連結</strong></p></td>
<td><p><a href="../Page/Wi-Fi.md" title="wikilink">Wi-Fi</a>（<a href="../Page/IEEE_802.11.md" title="wikilink">802.11</a> <a href="../Page/IEEE_802.11b-1999.md" title="wikilink">b</a>/<a href="../Page/IEEE_802.11g-2003.md" title="wikilink">g</a>）</p></td>
<td><p><a href="../Page/Wi-Fi.md" title="wikilink">Wi-Fi</a>（<a href="../Page/IEEE_802.11.md" title="wikilink">802.11</a> <a href="../Page/IEEE_802.11b-1999.md" title="wikilink">b</a>/<a href="../Page/IEEE_802.11g-2003.md" title="wikilink">g</a>/<a href="../Page/IEEE_802.11n-2009.md" title="wikilink">n</a>）</p></td>
<td><p><a href="../Page/Wi-Fi.md" title="wikilink">Wi-Fi</a>（<a href="../Page/IEEE_802.11.md" title="wikilink">802.11</a> <a href="../Page/IEEE_802.11a.md" title="wikilink">a</a>/<a href="../Page/IEEE_802.11b-1999.md" title="wikilink">b</a>/<a href="../Page/IEEE_802.11g-2003.md" title="wikilink">g</a>/<a href="../Page/IEEE_802.11n-2009.md" title="wikilink">n</a>）</p></td>
<td><p><a href="../Page/Wi-Fi.md" title="wikilink">Wi-Fi</a>（<a href="../Page/IEEE_802.11.md" title="wikilink">802.11</a> <a href="../Page/IEEE_802.11a.md" title="wikilink">a</a>/<a href="../Page/IEEE_802.11b-1999.md" title="wikilink">b</a>/<a href="../Page/IEEE_802.11g-2003.md" title="wikilink">g</a>/<a href="../Page/IEEE_802.11n-2009.md" title="wikilink">n</a>/<a href="../Page/IEEE_802.11ac.md" title="wikilink">ac</a>）</p></td>
<td><p>802.11a/b/g/n/ac Wi‑Fi w/MIMO</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>class="table-rh" |<a href="../Page/Global_Positioning_System.md" title="wikilink">衛星導航</a></p></td>
<td><p>colspan=1 </p></td>
<td><p>colspan=17 </p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>class="table-rh" |<a href="../Page/磁强计.md" title="wikilink">數碼方向儀</a></p></td>
<td><p>colspan=2 </p></td>
<td><p>colspan=16 </p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>colspan="1"  class="table-rh" |<a href="../Page/藍牙.md" title="wikilink">藍牙</a></p></td>
<td><p>Bluetooth 2.0 + EDR<br />
（CSR藍芽晶片Bluecore4）[183]</p></td>
<td><p>Bluetooth 2.1 + EDR（<a href="../Page/Broadcom.md" title="wikilink">Broadcom</a> 4325）[184]</p></td>
<td><p>Bluetooth 4.0（低功耗蓝牙）</p></td>
<td><p>Bluetooth 4.2</p></td>
<td><p>Bluetooth 5.0</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>rowspan="2"  class="table-rh" |<strong>網絡制式</strong></p></td>
<td><p><a href="../Page/四頻.md" title="wikilink">四頻</a><br />
<small>GSM/<a href="../Page/GPRS.md" title="wikilink">GPRS</a>/<a href="../Page/EDGE.md" title="wikilink">EDGE</a><br />
（850, 900, 1800, 1900 MHz）</small></p></td>
<td><p>加入前配置：<a href="../Page/三頻.md" title="wikilink">三頻</a><br />
<small>3.6 Mbit/s<br />
<a href="../Page/UMTS.md" title="wikilink">UMTS</a>/<a href="../Page/HSDPA.md" title="wikilink">HSDPA</a><br />
（850, 1900, 2100 MHz）[185]</small></p></td>
<td><p>加入前配置：<br />
7.2 Mbit/s HSDPA</p></td>
<td><p><em>GSM：</em><br />
加入前配置：五頻<br />
UMTS/<a href="../Page/HSUPA.md" title="wikilink">HSUPA</a><br />
（800, 900,1900, 2100 MHz）[186][187]</p></td>
<td><p>加入前配置：14.4 Mbit/s HSDPA<br />
重新設計的天線位置[188]、結合支援<a href="../Page/GSM.md" title="wikilink">GSM和</a><a href="../Page/CDMA.md" title="wikilink">CDMA網絡</a></p></td>
<td><p>加入前配置：<br />
LTE（支援HSPA+及DC-HSDPA）</p></td>
<td><p>GSM, CDMA, 3G, EVDO, HSPA+, LTE</p></td>
<td><p>GSM/EDGE, UMTS/HSPA+,DC-HSDPA, CDMA EV-DO Rev. A (部分機型), 4G LTE Advanced.</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><em>CDMA：</em><br />
<a href="../Page/雙頻.md" title="wikilink">雙頻</a><a href="../Page/CDMA.md" title="wikilink">CDMA</a>/EV-DO Rev. A（800, 1900MHz）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>class="table-rh" |<strong><a href="../Page/Touch_ID.md" title="wikilink">Touch ID</a></strong></p></td>
<td><p>colspan=7 </p></td>
<td><p>colspan=10 </p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>class="table-rh" |<strong><a href="../Page/Face_ID.md" title="wikilink">Face ID</a></strong></p></td>
<td><p>colspan=17 </p></td>
<td><p>colspan=1 </p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>class="table-rh" |<strong><a href="../Page/SIM卡.md" title="wikilink">SIM卡</a></strong></p></td>
<td><p>Mini-SIM</p></td>
<td><p>Micro-SIM</p></td>
<td><p>Nano-SIM</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>class="table-rh" |<strong>額外功能</strong></p></td>
<td><p><a href="../Page/Wi-Fi.md" title="wikilink">Wi-Fi</a>（<a href="../Page/IEEE_802.11.md" title="wikilink">802.11</a> <a href="../Page/IEEE_802.11b-1999.md" title="wikilink">b</a>/<a href="../Page/IEEE_802.11g-2003.md" title="wikilink">g</a>）<br />
<a href="../Page/Universal_Serial_Bus.md" title="wikilink">{{nowrap</a><br />
30針Dock連接埠<br />
備有遙控及麥克風功能的耳機</p></td>
<td><p><a href="../Page/AGPS.md" title="wikilink">內建AGPS全球衞星定位系統</a></p></td>
<td><p>聲控<br />
數碼指南針<br />
<a href="../Page/Nike+iPod.md" title="wikilink">Nike+</a><br />
觸控對焦相機（iOS 4.0+）<br />
備有音量遙控及麥克風的耳機</p></td>
<td><p><a href="../Page/Wi-Fi.md" title="wikilink">Wi-Fi</a>（<a href="../Page/IEEE_802.11.md" title="wikilink">802.11</a> <a href="../Page/IEEE_802.11b-1999.md" title="wikilink">b</a>/<a href="../Page/IEEE_802.11g-2003.md" title="wikilink">g</a>/<a href="../Page/IEEE_802.11n-2009.md" title="wikilink">n</a>）<br />
（<a href="../Page/IEEE_802.11n-2009.md" title="wikilink">IEEE 802.11n用於</a>2.4 GHz上）<br />
三軸<a href="../Page/陀螺儀.md" title="wikilink">陀螺儀</a><br />
雙麥克風通話降噪效果</p></td>
<td><p>語音助理系統<a href="../Page/Siri.md" title="wikilink">Siri</a><br />
支援<a href="../Page/格洛納斯系統.md" title="wikilink">格洛納斯全球衞星導航系統</a><br />
後置鏡頭<a href="../Page/LED.md" title="wikilink">LED閃光燈</a></p></td>
<td><p><a href="../Page/Wi-Fi.md" title="wikilink">Wi-Fi</a>（<a href="../Page/IEEE_802.11.md" title="wikilink">802.11</a> <a href="../Page/IEEE_802.11a-1999.md" title="wikilink">a</a>/<a href="../Page/IEEE_802.11b-1999.md" title="wikilink">b</a>/<a href="../Page/IEEE_802.11g-2003.md" title="wikilink">g</a>/<a href="../Page/IEEE_802.11n-2009.md" title="wikilink">n</a>）<br />
(<a href="../Page/IEEE_802.11n-2009.md" title="wikilink">IEEE 802.11n用於</a>2.4GHz和2.5GHz上[189])<br />
三麥克風通話降噪效果<br />
配備Apple EarPods</p></td>
<td><p>多色聚碳酸酯外殼</p></td>
<td><p>指紋身份識別感應器<br />
雙<a href="../Page/LED.md" title="wikilink">LED閃光燈</a></p></td>
<td><p>Wi-Fi (802.11 AC) NFC</p></td>
<td><p>主畫面按鈕內建第二代指紋感應器</p>
<p>3D Touch</p></td>
<td></td>
<td><p>全新主畫面按鈕內建指紋感應器 IP67防潑、抗水與防塵</p>
<p>移除3.5mm耳機孔, 改由Lightning 對 3.5 公釐耳機插孔轉接器</p>
<p>四合一 LED True Tone 閃光燈</p></td>
<td><p>無線充電 (可搭配 Qi 充電器)</p></td>
<td><p>Face ID 透過 TrueDepth 相機進行面孔辨識</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>rowspan="2"  class="table-rh" |相機</p></td>
<td><p>後鏡頭：<br />
2<a href="../Page/像素.md" title="wikilink">MP</a><br />
1/4µ感光元件<br />
<a href="../Page/焦比.md" title="wikilink">f/2.8</a></p></td>
<td><p>後鏡頭：<br />
3<a href="../Page/像素.md" title="wikilink">MP</a><br />
1/4µ感光元件<br />
每秒30fps<a href="../Page/480p.md" title="wikilink">480p的</a><a href="../Page/VGA.md" title="wikilink">VGA短片</a></p></td>
<td><p>後鏡頭：<br />
5<a href="../Page/像素.md" title="wikilink">MP</a><br />
1/3.2µ感光元件<br />
<a href="../Page/焦比.md" title="wikilink">f/2.8</a><br />
每秒30fps<a href="../Page/720p.md" title="wikilink">720p的</a><a href="../Page/高清.md" title="wikilink">高清短片</a><br />
<a href="../Page/LED.md" title="wikilink">LED閃光燈</a><br />
背照式傳感器</p></td>
<td><p>後鏡頭：<br />
8<a href="../Page/像素.md" title="wikilink">MP</a><br />
1/3.2µ感光元件<br />
<a href="../Page/焦比.md" title="wikilink">f/2.4</a><br />
每秒30fps<a href="../Page/1080p.md" title="wikilink">1080p的</a><a href="../Page/高清.md" title="wikilink">高清短片拍攝</a><br />
背照式傳感器<br />
臉孔偵測<br />
拍攝防震<br />
全景拍攝</p></td>
<td><p>後鏡頭：<br />
8<a href="../Page/像素.md" title="wikilink">MP</a><br />
1.4µ感光元件<br />
<a href="../Page/焦比.md" title="wikilink">f/2.4</a><br />
每秒30fps<a href="../Page/1080p.md" title="wikilink">1080p的</a><a href="../Page/高清.md" title="wikilink">高清短片拍攝</a><br />
紅外線截止濾光器<br />
背照式傳感器<br />
臉孔偵測<br />
拍攝防震<br />
全景拍攝和短片拍攝同時拍照</p></td>
<td><p>後鏡頭：<br />
8<a href="../Page/像素.md" title="wikilink">MP</a><br />
1.5µ感光元件<br />
<a href="../Page/焦比.md" title="wikilink">f/2.2</a><br />
每秒30fps<a href="../Page/1080p.md" title="wikilink">1080p的</a><a href="../Page/高清.md" title="wikilink">高清短片拍攝</a><br />
<small>（或每秒120fps的<a href="../Page/720p.md" title="wikilink">720p的</a><a href="../Page/高清.md" title="wikilink">高清慢鏡短片拍攝</a>）</small><br />
加強拍攝防震<br />
暖色閃光燈<br />
紅外線截止濾光器<br />
背照式傳感器<br />
臉孔偵測<br />
全景拍攝和短片拍攝同時拍照</p></td>
<td><p>後鏡頭：<br />
8<a href="../Page/像素.md" title="wikilink">MP</a><br />
1.5µ感光元件<br />
<a href="../Page/焦比.md" title="wikilink">f/2.2</a><br />
每秒30/60fps<a href="../Page/1080p.md" title="wikilink">1080p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質短片拍攝</a><br />
<small>（或每秒240/120fps的<a href="../Page/720p.md" title="wikilink">720p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質慢動作短片拍攝</a>）</small><br />
加強拍攝防震<br />
暖色閃光燈<br />
紅外線截止濾光器<br />
背照式傳感器<br />
臉孔偵測<br />
全景拍攝和短片拍攝同時拍照</p>
<p>3倍數位變焦</p></td>
<td><p>後鏡頭：<br />
8<a href="../Page/像素.md" title="wikilink">MP</a><br />
1.5µ感光元件<br />
<a href="../Page/焦比.md" title="wikilink">f/2.2</a><br />
每秒30/60fps<a href="../Page/1080p.md" title="wikilink">1080p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質短片拍攝</a><br />
<small>（或每秒240/120fps的<a href="../Page/720p.md" title="wikilink">720p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質慢動作短片拍攝</a>）</small><br />
加強拍攝防震<br />
暖色閃光燈<br />
紅外線截止濾光器<br />
背照式傳感器<br />
臉孔偵測<br />
全景拍攝和短片拍攝同時拍照</p>
<p>3倍數位變焦</p>
<p>光學影像穩定功能</p></td>
<td><p>後鏡頭：<br />
12<a href="../Page/像素.md" title="wikilink">MP</a><br />
1.22µ感光元件<br />
<a href="../Page/焦比.md" title="wikilink">f/2.2</a><br />
每秒30fps 4K超高畫質錄影, 每秒30/60fps<a href="../Page/1080p.md" title="wikilink">1080p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質短片拍攝</a><br />
<small>（或每秒240fps的<a href="../Page/1080p.md" title="wikilink">1080p</a>/120fps的<a href="../Page/720p.md" title="wikilink">720p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質慢動作短片拍攝</a>）</small><br />
加強拍攝防震<br />
暖色閃光燈<br />
紅外線截止濾光器<br />
背照式傳感器<br />
臉孔偵測<br />
全景拍攝和短片拍攝同時拍照</p>
<p>5倍數位變焦</p></td>
<td><p>後鏡頭：<br />
12<a href="../Page/像素.md" title="wikilink">MP</a><br />
1.22µ感光元件<br />
<a href="../Page/焦比.md" title="wikilink">f/2.2</a><br />
每秒30fps 4K超高畫質錄影, 每秒30/60fps<a href="../Page/1080p.md" title="wikilink">1080p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質短片拍攝</a><br />
<small>（或每秒240fps的<a href="../Page/1080p.md" title="wikilink">1080p</a>/120fps的<a href="../Page/720p.md" title="wikilink">720p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質慢動作短片拍攝</a>）</small><br />
加強拍攝防震<br />
暖色閃光燈<br />
紅外線截止濾光器<br />
背照式傳感器<br />
臉孔偵測<br />
全景拍攝和短片拍攝同時拍照</p>
<p>5倍數位變焦</p>
<p>光學影像穩定功能</p></td>
<td><p>後鏡頭：<br />
12<a href="../Page/像素.md" title="wikilink">MP</a><br />
1.22µ感光元件<br />
<a href="../Page/焦比.md" title="wikilink">f/2.2</a><br />
每秒30fps 4K超高畫質錄影, 每秒30/60fps<a href="../Page/1080p.md" title="wikilink">1080p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質短片拍攝</a><br />
<small>（或每秒240fps的<a href="../Page/1080p.md" title="wikilink">1080p</a>/120fps的<a href="../Page/720p.md" title="wikilink">720p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質慢動作短片拍攝</a>）</small><br />
加強拍攝防震<br />
暖色閃光燈<br />
紅外線截止濾光器<br />
背照式傳感器<br />
臉孔偵測<br />
全景拍攝和短片拍攝同時拍照</p>
<p>5倍數位變焦</p></td>
<td><p>後鏡頭：<br />
12<a href="../Page/像素.md" title="wikilink">MP</a><br />
1.22µ感光元件<br />
<a href="../Page/焦比.md" title="wikilink">f/1.8</a><br />
每秒30fps 4K超高畫質錄影, 每秒30/60fps<a href="../Page/1080p.md" title="wikilink">1080p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質短片拍攝</a><br />
<small>（或每秒240fps的<a href="../Page/1080p.md" title="wikilink">1080p</a>/120fps的<a href="../Page/720p.md" title="wikilink">720p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質慢動作短片拍攝</a>）</small><br />
加強拍攝防震<br />
暖色閃光燈<br />
紅外線截止濾光器<br />
背照式傳感器<br />
臉孔偵測<br />
全景拍攝和短片拍攝同時拍照</p>
<p>5倍數位變焦</p>
<p>光學影像穩定功能</p></td>
<td><p>後鏡頭：<br />
12<a href="../Page/像素.md" title="wikilink">MP</a><br />
1.22µ感光元件<br />
廣角<a href="../Page/焦比.md" title="wikilink">f/1.8</a> 長焦f/2.8<br />
每秒30fps 4K超高畫質錄影, 每秒30/60fps<a href="../Page/1080p.md" title="wikilink">1080p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質短片拍攝</a><br />
<small>（或每秒240fps的<a href="../Page/1080p.md" title="wikilink">1080p</a>/120fps的<a href="../Page/720p.md" title="wikilink">720p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質慢動作短片拍攝</a>）</small><br />
加強拍攝防震<br />
暖色閃光燈<br />
紅外線截止濾光器<br />
背照式傳感器<br />
臉孔偵測<br />
全景拍攝和短片拍攝同時拍照</p>
<p>5倍數位變焦</p>
<p>光學影像穩定功能</p></td>
<td><p>後鏡頭：<br />
12<a href="../Page/像素.md" title="wikilink">MP</a><br />
1.22µ感光元件<br />
<a href="../Page/焦比.md" title="wikilink">f/1.8</a><br />
每秒24/30/60fps 4K超高畫質錄影, 每秒30/60fps<a href="../Page/1080p.md" title="wikilink">1080p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質短片拍攝</a><br />
<small>（或每秒120/240fps的<a href="../Page/1080p.md" title="wikilink">1080p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質慢動作短片拍攝</a>）</small><br />
加強拍攝防震<br />
暖色閃光燈<br />
紅外線截止濾光器<br />
背照式傳感器<br />
臉孔偵測<br />
全景拍攝和短片拍攝同時拍照 5倍數位變焦</p>
<p>光學影像穩定功能</p></td>
<td><p>後鏡頭：<br />
12<a href="../Page/像素.md" title="wikilink">MP</a><br />
1.22µ感光元件<br />
廣角<a href="../Page/焦比.md" title="wikilink">f/1.8</a> 長焦f/2.8<br />
每秒24/30/60fps 4K超高畫質錄影, 每秒30/60fps<a href="../Page/1080p.md" title="wikilink">1080p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質短片拍攝</a><br />
<small>（或每秒120/240fps的<a href="../Page/1080p.md" title="wikilink">1080p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質慢動作短片拍攝</a>）</small><br />
加強拍攝防震<br />
暖色閃光燈<br />
紅外線截止濾光器<br />
背照式傳感器<br />
臉孔偵測<br />
全景拍攝和短片拍攝同時拍照 10倍數位變焦</p>
<p>光學影像穩定功能</p></td>
<td><p>後鏡頭：<br />
12<a href="../Page/像素.md" title="wikilink">MP</a><br />
1.22µ感光元件<br />
廣角<a href="../Page/焦比.md" title="wikilink">f/1.8</a> 長焦f/2.4<br />
每秒24/30/60fps 4K超高畫質錄影, 每秒30/60fps<a href="../Page/1080p.md" title="wikilink">1080p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質短片拍攝</a><br />
<small>（或每秒120/240fps的<a href="../Page/1080p.md" title="wikilink">1080p的</a><a href="../Page/高畫質.md" title="wikilink">高畫質慢動作短片拍攝</a>）</small><br />
加強拍攝防震<br />
暖色閃光燈<br />
紅外線截止濾光器<br />
背照式傳感器<br />
臉孔偵測<br />
全景拍攝和短片拍攝同時拍照 10倍數位變焦</p>
<p>光學影像穩定雙鏡頭</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>colspan=2 </p></td>
<td><p>前鏡頭：<br />
0.3<a href="../Page/像素.md" title="wikilink">MP</a><br />
每秒30fps的<a href="../Page/VGA.md" title="wikilink">VGA相片</a><br />
<a href="../Page/480p.md" title="wikilink">480p短片拍攝</a></p></td>
<td><p>前鏡頭：<br />
1.2<a href="../Page/像素.md" title="wikilink">MP</a><br />
1.75µ感光元件<br />
每秒30fps<a href="../Page/720p.md" title="wikilink">720p</a><a href="../Page/高清.md" title="wikilink">高清短片</a><br />
背照式傳感器</p></td>
<td><p>前鏡頭：<br />
1.2<a href="../Page/像素.md" title="wikilink">MP</a><br />
1.9µ感光元件<br />
每秒30fps<a href="../Page/720p.md" title="wikilink">720p</a><a href="../Page/高畫質.md" title="wikilink">高畫質短片</a><br />
背照式傳感器</p></td>
<td><p>前鏡頭：<br />
5<a href="../Page/像素.md" title="wikilink">MP</a><br />
1.9µ感光元件<br />
每秒30fps<a href="../Page/720p.md" title="wikilink">720p</a><a href="../Page/高畫質.md" title="wikilink">高畫質短片</a><br />
背照式傳感器</p></td>
<td><p>前鏡頭：<br />
1.2<a href="../Page/像素.md" title="wikilink">MP</a><br />
1.75µ感光元件/f2.4<br />
每秒30fps<a href="../Page/720p.md" title="wikilink">720p</a><a href="../Page/高畫質.md" title="wikilink">高畫質短片</a><br />
背照式傳感器</p></td>
<td><p>前鏡頭：<br />
7<a href="../Page/像素.md" title="wikilink">MP</a><br />
2.2µ感光元件<br />
每秒30fps<a href="../Page/1080p.md" title="wikilink">1080p</a><a href="../Page/高畫質.md" title="wikilink">高畫質短片</a><br />
背照式傳感器, 拍攝廣色域照片, 自動影像穩定功能</p></td>
<td><p>前鏡頭：<br />
7<a href="../Page/像素.md" title="wikilink">MP</a><br />
2.2µ感光元件<br />
每秒30fps<a href="../Page/1080p.md" title="wikilink">1080p</a><a href="../Page/高畫質.md" title="wikilink">高畫質短片</a><br />
背照式傳感器, 拍攝廣色域照片, 自動影像穩定功能, 人像模式, Animoji.</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>class="table-rh" |物料</p></td>
<td><p>鋁<br />
玻璃<br />
鋼<br />
黑色塑膠</p></td>
<td><p>玻璃<br />
塑膠<br />
鋼<br />
黑色或白色<br />
（8GB版本不設白色）</p></td>
<td><p><a href="../Page/硅酸铝.md" title="wikilink">矽酸鋁玻璃</a><br />
<a href="../Page/不鏽鋼.md" title="wikilink">不鏽鋼</a><br />
黑色或白色</p></td>
<td><p>黑色：陽極化鋁合金、板石礦<br />
白色：銀鋁合金</p></td>
<td><p>聚碳酸酯<br />
<small>（白色、粉紅色、黃色、藍色或綠色）</small></p></td>
<td><p>太空灰：<br />
黑色陽極化鋁<br />
太空灰金屬背板<br />
<br />
銀色：<br />
白色陽極化鋁<br />
銀色金屬背板<br />
<br />
金色：<br />
白色陽極化鋁<br />
金色金屬背板</p></td>
<td><p>太空灰：<br />
黑色陽極化鋁<br />
太空灰金屬背板<br />
銀色：<br />
白色陽極化鋁<br />
銀色金屬背板<br />
金色：<br />
白色陽極化鋁<br />
金色金屬背板</p>
<p>玫瑰金：</p>
<p>白色陽極化鋁<br />
玫瑰金色金屬背板</p></td>
<td><p>曜石黑色：</p>
<p>黑色：<br />
黑色陽極化鋁<br />
黑色金屬背板<br />
銀色：<br />
白色陽極化鋁<br />
銀色金屬背板<br />
金色：<br />
白色陽極化鋁<br />
金色金屬背板</p>
<p>玫瑰金：<br />
白色陽極化鋁<br />
玫瑰金色金屬背板</p>
<p>紅色：(2017/03/24)<br />
白色陽極化鋁<br />
紅色金屬背板</p></td>
<td><p>太空灰：<br />
黑色陽極化鋁<br />
太空灰金屬背板<br />
銀色：<br />
白色陽極化鋁<br />
銀色金屬背板<br />
金色：<br />
白色陽極化鋁<br />
金色金屬背板 紅色：(2018/04/10)<br />
白色陽極化鋁<br />
紅色金屬背板</p></td>
<td><p>太空灰：<br />
黑色陽極化鋁<br />
太空灰全貼合玻璃<br />
銀色：<br />
白色陽極化鋁<br />
銀色全貼合玻璃</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>rowspan="2"  class="table-rh" |電力</p></td>
<td><p>內置不能移除的<a href="../Page/鋰電池.md" title="wikilink">鋰電池</a>[190][191][192][193]</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>3.7<a href="../Page/伏特.md" title="wikilink">V</a><br />
5.18<a href="../Page/千瓦時.md" title="wikilink">W-h</a><br />
1,400<a href="../Page/mAh.md" title="wikilink">mAh</a>[194][195][196]</p></td>
<td><p>3.7<a href="../Page/伏特.md" title="wikilink">V</a><br />
4.12<a href="../Page/千瓦時.md" title="wikilink">W-h</a><br />
1,150<a href="../Page/mAh.md" title="wikilink">mAh</a>[197]</p></td>
<td><p>3.7<a href="../Page/伏特.md" title="wikilink">V</a><br />
4.51<a href="../Page/千瓦時.md" title="wikilink">W-h</a><br />
1,219<a href="../Page/mAh.md" title="wikilink">mAh</a>[198]</p></td>
<td><p>3.7<a href="../Page/伏特.md" title="wikilink">V</a><br />
5.25<a href="../Page/千瓦時.md" title="wikilink">W-h</a><br />
1,420<a href="../Page/mAh.md" title="wikilink">mAh</a>[199]</p></td>
<td><p>3.7<a href="../Page/伏特.md" title="wikilink">V</a><br />
5.3<a href="../Page/千瓦時.md" title="wikilink">W-h</a><br />
1,432<a href="../Page/mAh.md" title="wikilink">mAh</a>[200]</p></td>
<td><p>3.8<a href="../Page/伏特.md" title="wikilink">V</a><br />
5.45<a href="../Page/千瓦時.md" title="wikilink">W-h</a><br />
1,440 mAh[201]</p></td>
<td><p>3.8<a href="../Page/伏特.md" title="wikilink">V</a><br />
5.73<a href="../Page/千瓦時.md" title="wikilink">W-h</a><br />
1,507<a href="../Page/mAh.md" title="wikilink">mAh</a>[202]</p></td>
<td><p>3.8<a href="../Page/伏特.md" title="wikilink">V</a><br />
5.96<a href="../Page/千瓦時.md" title="wikilink">W-h</a><br />
1,560<a href="../Page/mAh.md" title="wikilink">mAh</a>[203]</p></td>
<td><p>3.82<a href="../Page/伏特.md" title="wikilink">V</a><br />
6.91<a href="../Page/千瓦時.md" title="wikilink">W-h</a><br />
1,810<a href="../Page/mAh.md" title="wikilink">mAh</a></p></td>
<td><p>3.82<a href="../Page/伏特.md" title="wikilink">V</a><br />
11.1<a href="../Page/千瓦時.md" title="wikilink">W-h</a><br />
2,915<a href="../Page/mAh.md" title="wikilink">mAh</a></p></td>
<td><p>3.82<a href="../Page/伏特.md" title="wikilink">V</a><br />
6.55<a href="../Page/千瓦時.md" title="wikilink">W-h</a><br />
1,715<a href="../Page/mAh.md" title="wikilink">mAh</a></p></td>
<td><p>3.8<a href="../Page/伏特.md" title="wikilink">V</a><br />
10.45<a href="../Page/千瓦時.md" title="wikilink">W-h</a><br />
2,750<a href="../Page/mAh.md" title="wikilink">mAh</a></p></td>
<td><p>3.82<a href="../Page/伏特.md" title="wikilink">V</a><br />
6.21<a href="../Page/千瓦時.md" title="wikilink">W-h</a><br />
1,624<a href="../Page/mAh.md" title="wikilink">mAh</a></p></td>
<td><p>3.80<a href="../Page/伏特.md" title="wikilink">V</a><br />
7.45<a href="../Page/千瓦時.md" title="wikilink">W-h</a><br />
1,960<a href="../Page/mAh.md" title="wikilink">mAh</a></p></td>
<td><p>3.82<a href="../Page/伏特.md" title="wikilink">V</a><br />
11.10<a href="../Page/千瓦時.md" title="wikilink">W-h</a><br />
2,900<a href="../Page/mAh.md" title="wikilink">mAh</a></p></td>
<td><p>3.82V 6.96Wh</p>
<p>1821mAh</p></td>
<td><p>3.82V 10.28Wh</p>
<p>2691mAh</p></td>
<td><p>3.81V 10.35Wh 2716mAh</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>class="table-rh" |電池壽命</p></td>
<td><p>音訊：24小時<br />
視頻：7小時<br />
2G通話：8小時<br />
瀏覽互聯網：6小時<br />
待命：250小時</p></td>
<td><p>音訊：24小時<br />
視頻：7小時<br />
3G通話：5小時<br />
3G瀏覽：5小時<br />
Wi-Fi瀏覽：9小時<br />
待命：300小時</p></td>
<td><p>音訊：30小時<br />
視頻：10小時<br />
3G通話：5小時<br />
3G瀏覽：5小時<br />
Wi-Fi瀏覽：9小時<br />
待命：300小時</p></td>
<td><p>音訊：40小時<br />
視頻：10小時<br />
3G通話：7小時<br />
3G瀏覽：6小時<br />
Wi-Fi瀏覽：10小時<br />
待命：300小時[204]</p></td>
<td><p>音訊：40小時<br />
視頻：10小時<br />
3G通話：8小時<br />
3G瀏覽：6小時<br />
Wi-Fi瀏覽：9小時<br />
待命：200小時</p></td>
<td><p>音訊：40小時<br />
視頻：10小時<br />
3G通話：8小時<br />
3G瀏覽：8小時<br />
LTE瀏覽：8小時<br />
<small>WiFi瀏覽：10小時</small><br />
待命：225小時</p></td>
<td><p>音訊：40小時<br />
視頻：10小時<br />
3G通話：10小時<br />
3G瀏覽：8小時<br />
LTE瀏覽：10小時<br />
<small>WiFi瀏覽：10小時</small><br />
待命：250小時</p></td>
<td><p>音訊：50小時<br />
視頻：11小時<br />
3G通話：14小時<br />
3G瀏覽：10小時<br />
LTE瀏覽：10小時<br />
<small>WiFi瀏覽：11小時</small><br />
待命：250小時</p></td>
<td><p>音訊：80小時<br />
視頻：14小時<br />
3G通話：24小時<br />
3G瀏覽：12小時<br />
LTE瀏覽：12小時<br />
<small>WiFi瀏覽：12小時</small><br />
待命：384小時</p></td>
<td><p>音訊：50小時<br />
視頻：11小時<br />
3G通話：14小時<br />
3G瀏覽：10小時<br />
LTE瀏覽：10小時<br />
<small>WiFi瀏覽：11小時</small><br />
待命：240小時</p></td>
<td><p>音訊：80小時<br />
視頻：14小時<br />
3G通話：24小時<br />
3G瀏覽：12小時<br />
LTE瀏覽：12小時<br />
<small>WiFi瀏覽：12小時</small><br />
待命：384小時</p></td>
<td><p>音訊：50小時<br />
視頻：13小時<br />
3G通話：14小時<br />
3G瀏覽：12小時<br />
LTE瀏覽：13小時<br />
<small>WiFi瀏覽：13小時</small><br />
待命：240小時</p></td>
<td><p>音訊：40小時<br />
視頻：13小時<br />
3G通話：14小時<br />
3G瀏覽：12小時<br />
LTE瀏覽：12小時<br />
<small>WiFi瀏覽：14小時</small><br />
待命：240小時</p></td>
<td><p>音訊：60小時<br />
視頻：14小時<br />
3G通話：21小時<br />
3G瀏覽：13小時<br />
LTE瀏覽：13小時<br />
<small>WiFi瀏覽：15小時</small><br />
待命：384小時</p></td>
<td><p>電池使用時間約與 iPhone 7 相同</p></td>
<td><p>電池使用時間約與 iPhone 7 Plus 相同</p></td>
<td><p>電池使用時間比 iPhone 7 長達 2 小時</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>class="table-rh" |尺寸</p></td>
<td><p>長：115mm（4.5吋）<br />
闊：61mm（2.4吋）<br />
深：11.6mm（0.46吋）</p></td>
<td><p>長：115.5mm（4.55吋）<br />
闊：62.1mm（2.44吋）<br />
深：12.3mm（0.48吋）</p></td>
<td><p>長：115.2mm（4.55吋）<br />
闊：58.6mm（2.31吋）<br />
深：9.3mm（0.37吋）</p></td>
<td><p>長：123.8mm（4.87吋）<br />
闊：58.6mm（2.31吋）<br />
深：7.6mm（0.3吋）</p></td>
<td><p>長：124.4mm（4.9吋）<br />
闊：59.2mm（2.33吋）<br />
深：8.97mm（0.35吋）</p></td>
<td><p>長：123.8mm（4.87吋）<br />
闊：58.6mm（2.31吋）<br />
深：7.6mm（0.3吋）</p></td>
<td><p>長：138.1mm（5.44吋）<br />
闊：67.0mm（2.64吋）<br />
深：6.9mm（0.27吋）</p></td>
<td><p>長：158.1mm（6.22吋）<br />
闊：77.8mm（3.06吋）<br />
深：7.1mm（0.28吋）</p></td>
<td><p>長：138.3mm（5.44吋）<br />
闊：67.1mm（2.64吋）<br />
深：7.1mm（0.28吋）</p></td>
<td><p>長：158.2mm（6.23吋）<br />
闊：77.9mm（3.07吋）<br />
深：7.3mm（0.29吋）</p></td>
<td><p>長：123.8mm（4.87吋）<br />
闊：58.6mm（2.31吋）<br />
深：7.6mm（0.3吋）</p></td>
<td><p>長：138.3mm（5.44吋）<br />
闊：67.1mm（2.64吋）<br />
深：7.1mm（0.28吋）</p></td>
<td><p>長：158.2mm（6.23吋）<br />
闊：77.9mm（3.07吋）<br />
深：7.3mm（0.29吋）</p></td>
<td><p>長：138.4mm（5.45吋）<br />
闊：67.3mm（2.65吋）<br />
深：7.3mm（0.29吋）</p></td>
<td><p>長：158.4mm（6.24吋）<br />
闊：78.1mm（3.07吋）<br />
深：7.5mm（0.3吋）</p></td>
<td><p>長：143.6mm（5.65吋）<br />
闊：70.9mm（2.79吋）<br />
深：7.7mm（0.3吋）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>class="table-rh" |重量</p></td>
<td><p>135 g（4.8 oz）</p></td>
<td><p>133 g（4.7 oz）</p></td>
<td><p>135 g（4.8 oz）</p></td>
<td><p>137 g（4.8 oz）</p></td>
<td><p>140 g（4.9 oz）</p></td>
<td><p>112 g（4.0 oz）</p></td>
<td><p>132 g（4.65 oz）</p></td>
<td><p>112 g（3.95 oz）</p></td>
<td><p>129 g（4.55 oz）</p></td>
<td><p>172 g（6.07 oz）</p></td>
<td><p>143 g (5.04 oz)</p></td>
<td><p>192 g (6.77 oz)</p></td>
<td><p>113 g (3.99 oz)</p></td>
<td><p>138 g (4.87 oz)</p></td>
<td><p>188 g (6.63 oz)</p></td>
<td><p>148 g (5.22 oz)</p></td>
<td><p>202 g (7.13 oz)</p></td>
<td><p>174 g (6.14 oz)</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>class="table-rh" |產品編號</p></td>
<td><p>A1203</p></td>
<td><p>A1241<br />
A1324（中國）</p></td>
<td><p>A1303<br />
A1325（中國）</p></td>
<td><p>A1332（GSM型號）<br />
A1349（CDMA型號）</p></td>
<td><p>A1387<br />
A1431（中國GSM型號）</p></td>
<td><p>A1428（GSM型號）<br />
A1429（GSM及CDMA型號）<br />
A1442（中國CDMA型號）</p></td>
<td><p>A1532（北美地區）<br />
A1456（美國及日本）<br />
A1507（歐洲）<br />
A1529（亞洲及大洋洲）</p></td>
<td><p>A1533（北美地區）<br />
A1453（美國及日本）<br />
A1457（歐洲）<br />
A1530（亞洲及大洋洲）</p></td>
<td><p>A1549</p>
<p>A1586</p></td>
<td><p>A1522</p>
<p>A1524</p></td>
<td><p>A1633</p>
<p>A1688</p></td>
<td><p>A1634</p>
<p>A1687</p></td>
<td><p>A1662</p>
<p>A1723</p></td>
<td><p>A1660</p>
<p>A1778</p></td>
<td><p>A1778</p>
<p>A1784</p></td>
<td><p>A1863 A1905</p></td>
<td><p>A1864 A1897</p></td>
<td><p>A1865 A1901</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>rowspan="2"  class="table-rh" |發布日期</p></td>
<td><p>4GB和8GB：<br />
2007年6月29日</p></td>
<td><p>8GB和16GB：<br />
2008年7月11日</p></td>
<td><p>16GB和32GB：<br />
2009年6月19日</p></td>
<td><p>16GB和32GB：<br />
2010年6月24日</p></td>
<td><p>16GB, 32GB和64GB：<br />
2011年10月14日</p></td>
<td><p>16GB, 32GB和64GB：<br />
2012年9月21日</p></td>
<td><p>16GB和32GB：<br />
2013年9月20日<br />
8GB:<br />
2014年3月18日</p></td>
<td><p>16GB, 32GB和64GB：<br />
2013年9月20日</p></td>
<td><p>16GB, 64GB和128GB：<br />
2014年9月9日</p></td>
<td><p>16GB, 64GB和128GB： 2015年9月9日</p></td>
<td><p>16GB和64GB：<br />
2016年3月31日</p></td>
<td><p>32GB, 128GB和256GB：<br />
2016年9月7日</p></td>
<td><p>64GB和256GB：<br />
2017年9月12日</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>16GB：<br />
2008年2月5日</p></td>
<td><p>8GB黑色版本：<br />
2010年6月24日</p></td>
<td><p>CDMA版本：<br />
2011年2月10日<br />
<br />
白色版本：<br />
2011年4月28日<br />
<br />
8GB版本：<br />
2011年10月14日</p></td>
<td><p>8GB版本：<br />
2013年9月10日</p></td>
<td><p>32GB<br />
2016年9月16日</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>rowspan="2"  class="table-rh" |<strong>停售日期</strong></p></td>
<td><p>4GB：<br />
2007年9月5日</p></td>
<td><p>16GB：<br />
2009年6月8日</p></td>
<td><p>16GB和32GB：<br />
2010年6月24日</p></td>
<td><p>16GB和32GB：<br />
2011年10月4日</p></td>
<td><p>32GB和64GB：<br />
2012年9月12日</p></td>
<td><p>16GB, 32GB和64GB：<br />
2013年9月10日</p></td>
<td><p>16GB和32GB：<br />
2014年9月10日</p></td>
<td><p>64GB：<br />
2014年9月10日</p></td>
<td></td>
<td><p>16GB和64GB：<br />
2016年9月16日</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>16GB：<br />
2008年7月11日</p></td>
<td><p>8GB黑色版本：<br />
2010年6月7日</p></td>
<td><p>8GB黑色版本：<br />
2012年9月12日</p></td>
<td><p>8GB版本：<br />
2013年9月10日</p></td>
<td><p>16GB：<br />
2014年9月10日</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>型號</p></td>
<td><p><a href="../Page/iPhone_(第一代).md" title="wikilink">iPhone (第一代)</a></p></td>
<td><p><a href="../Page/iPhone_3G.md" title="wikilink">iPhone 3G</a></p></td>
<td><p><a href="../Page/iPhone_3GS.md" title="wikilink">iPhone 3GS</a></p></td>
<td><p><a href="../Page/iPhone_4.md" title="wikilink">iPhone 4</a></p></td>
<td><p><a href="../Page/iPhone_4S.md" title="wikilink">iPhone 4S</a></p></td>
<td><p><a href="../Page/iPhone_5.md" title="wikilink">iPhone 5</a></p></td>
<td><p><a href="../Page/iPhone_5C.md" title="wikilink">iPhone 5C</a></p></td>
<td><p><a href="../Page/iPhone_5S.md" title="wikilink">iPhone 5S</a></p></td>
<td><p><a href="../Page/iPhone_6.md" title="wikilink">iPhone 6</a></p></td>
<td><p><a href="../Page/iPhone_6_Plus.md" title="wikilink">iPhone 6 Plus</a></p></td>
<td><p><a href="../Page/iPhone_6S.md" title="wikilink">iPhone 6S</a></p></td>
<td><p><a href="../Page/iPhone_6S_Plus.md" title="wikilink">iPhone 6S Plus</a></p></td>
<td><p><a href="../Page/iPhone_SE.md" title="wikilink">iPhone SE</a></p></td>
<td><p><a href="../Page/iPhone_7.md" title="wikilink">iPhone 7</a></p></td>
<td><p><a href="../Page/iPhone_7_Plus.md" title="wikilink">iPhone 7 Plus</a></p></td>
<td><p><a href="../Page/iPhone_8.md" title="wikilink">iPhone 8</a></p></td>
<td><p><a href="../Page/iPhone_8_Plus.md" title="wikilink">iPhone 8 Plus</a></p></td>
<td><p><a href="../Page/iPhone_X.md" title="wikilink">iPhone X</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 知識產權

[IPhone_sales_per_quarter.png](https://zh.wikipedia.org/wiki/File:IPhone_sales_per_quarter.png "fig:IPhone_sales_per_quarter.png")
蘋果公司就iPhone的科技提出超過200項知識產權的專利申請\[205\]\[206\]。

[LG指iPhone的設計是抄襲](../Page/LG.md "wikilink")，該公司的研發中心主管Woo-Young
Kwak在一個新聞發布會上說：「我們認為，Prada手機於2006年9月亮相於iF設計頒獎禮上，並獲得該獎項，蘋果公司就在那時抄襲它的設計。\[207\]」

1993年9月3日，提出以"I PHONE"作為商標的申請\[208\]，並於1996年3月20日已申請"I
PHONE"作商標\[209\]。"I
Phone"在1998年3月註冊\[210\]，而"IPhone"則在1999年註冊\[211\]，自此"I
PHONE"的商標被棄用\[212\]。Infogear的商標涵蓋通訊端子的東西，包括電腦硬件及軟件提供了一個一體化的電話、數據通訊和個人電腦的功能（1993年的存檔）\[213\]。

1998年，Infogear推出結合電話與網頁瀏覽器的[iPhone](../Page/IPhone_\(Linksys\).md "wikilink")\[214\]。

2000年，Infogear從iphones.com的域名侵權案中勝出\[215\]。2000年6月，[思科系統收購了Infogear](../Page/思科系統.md "wikilink")，包括了iphone的商標\[216\]。

2002年10月，蘋果公司在[英國](../Page/英國.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[新加坡及](../Page/新加坡.md "wikilink")[歐盟申請為iPhone的名稱註冊商標](../Page/歐盟.md "wikilink")。隨後在2004年10月，蘋果公司在[加拿大為iPhone的名稱申請註冊](../Page/加拿大.md "wikilink")，並在2006年9月在[新西蘭申請註冊商標](../Page/新西蘭.md "wikilink")。同月，一間名為「海洋通訊服務」的公司在[美國](../Page/美國.md "wikilink")、[英國及](../Page/英國.md "wikilink")[香港為iPhone申請註冊](../Page/香港.md "wikilink")\[217\]，隨後在[千里達和托貝哥](../Page/千里達.md "wikilink")（西印度群島一國）入稟申請\[218\]。，那些申請只得[澳洲和](../Page/澳洲.md "wikilink")[新加坡獲得通過](../Page/新加坡.md "wikilink")。

2005年8月，在蘋果公司在[加拿大的商標註冊申請被否決](../Page/加拿大.md "wikilink")，原因是加拿大一間名為的公司早於蘋果公司申請的三個月前獲得申請，Comwave自2004年已售賣[Voice
over IP](../Page/網際協議通話技術.md "wikilink")（VoIP）設備\[219\]。

2006年12月18日，Infogear以iPhone名義下的[VoIP重新發布一系列的產品](../Page/網際協議通話技術.md "wikilink")\[220\]。

由於海洋通訊服務的商標註冊與蘋果公司在[新西蘭申請時使用完全相同的措辭](../Page/新西蘭.md "wikilink")，所以假設他們是代表蘋果公司提出註冊商標的申請\[221\]。

2007年1月9日，喬布斯宣布將於2007年6月售賣一個稱為iPhone的產品不久之後，[思科公司發表一份聲明](../Page/思科公司.md "wikilink")，指他們已跟蘋果公司就商標許可權展開商談，預計蘋果公司於前一天晚上已同意他們提交的最後文件\[222\]。翌日，思科公司公布已入稟向蘋果公司提出訴訟，指控他們在iPhone的商標中侵權，要求[聯邦法院頒下禁令](../Page/聯邦法院.md "wikilink")，禁止蘋果公司使用該名稱\[223\]\[224\]。同年2月2日，蘋果公司與思科公司宣布和解，兩間公司均表示將iPhone商標的爭議暫時擱置\[225\]。同月20日，兩間公司發表聲明指雙方已達成協議，他們共同擁有使用iPhone商標的權利\[226\]。後來在2012年4月，思科公司稱商標訴訟案只是一個「小衝突」，並不是因為金錢的問題，而是讓軟硬體在多種品牌產品上能有意義的溝通\[227\]。

iPhone的研發同時也激發一些領先高科技仿製電腦\[228\]品牌，推動蘋果公司的普及性和消費者願意為iPhone進行升級\[229\]。

2009年10月22日，[諾基亞向蘋果公司提出訴訟](../Page/諾基亞.md "wikilink")，指控他們在GSM、UMTS和WLAN網絡的專利作出侵權行為，諾基亞指蘋果公司自iPhone剛發布後已侵犯了10項諾基亞的專利權\[230\]。

2010年12月，[路透社報導](../Page/路透社.md "wikilink")，一些iPhone和iPad用戶控告蘋果公司，因為一些第三方應用程式在未經用戶批准下把用戶的資料傳遞出去，一些應用程式開發者，例如《Textplus4》、《》、《》、《》、《會說話的湯姆貓》和《南瓜製造者》在訴訟中同樣成為被告\[231\]。

2012年8月，蘋果公司在美國對[三星的訴訟案中](../Page/三星電子.md "wikilink")，陪審團裁定三星6項侵權罪，被判需賠償10.51亿美元給蘋果公司\[232\]\[233\]。

2013年3月，蘋果公司專利的環繞式屏幕被泄露\[234\]。

2013年7月下旬，苹果公司申請專利，透露新iPhone的電池可利用手機位置數據，與用戶習慣使用的數據結合，使相應的手機電源設置至中度。蘋果公司正致力於研發電源管理系統，提供的功能包括：改進能源使用率，估計用戶離開電源的時間距離；檢測功能，透過調整充電使用率，尋找最合適正在使用中的電源類型\[235\]。

## 秘密追蹤

從2011年4月20日，傳媒廣泛談論iPhone和其他iOS裝置中的一個隱藏已[加密的檔案](../Page/加密.md "wikilink")\[236\]\[237\]。據稱該檔案標示著“consolidated.db”，會不斷儲存由手機訊號塔發出的用戶移動位置定位，但此技術被證明有時候是不準確的\[238\]。此檔案是在2010年6月與蘋果公司更新的iOS
4一同發布，並可能載有接近一年的數據價值。以往版本的iOS儲存類似的訊息於一個稱為"h-cells.plist"的檔案中\[239\]。

[F-Secure公司發現一天兩次把數據傳送到蘋果公司](../Page/F-Secure.md "wikilink")，推測蘋果公司正利用這些資料來建構他們的全球定位數據庫，類似於通過[戰爭駕駛來建構的](../Page/戰爭駕駛.md "wikilink")[谷歌和](../Page/Google.md "wikilink")\[240\]。然而，不像[谷歌的應用程式](../Page/Google.md "wikilink")"Latitude"，它執行類似[Android手機的任務](../Page/Android.md "wikilink")，此檔案是不依賴一個特定的[最終使用者協議（EULA）](../Page/最终用户许可协议.md "wikilink")，或甚至是在用戶不知情的情況下進行，但它已在iPhone的合約條款中以15,200字訂明，「蘋果公司跟他們的合作伙伴及持牌人可能會收集、使用和分享這些精確的位置數據，包括用戶的蘋果電腦或裝置的實時地理位置\[241\]。」

當iPhone連接電腦同步時，該檔案會自動複製到用戶的電腦。在2011年4月公開發布的一個名為iPhoneTracker的開源碼，能夠把數據轉成一個視覺化的地圖\[242\]。該檔案會被加密，如沒有[越獄手機](../Page/越獄_\(iOS\).md "wikilink")，將不能刪除\[243\]。

在用戶及[美聯社以及其他人士提出相關問題後](../Page/美联社.md "wikilink")，蘋果公司在2011年4月27日於其網站上作了官方回應\[244\]\[245\]，他們澄清那些資料只是[WiFi熱點和手機訊號發射塔發出人群數據來源裡的一小部分](../Page/WiFi.md "wikilink")，那些都是蘋果公司下載到iPhone的，為了比較GPS進行定位服務更快的效果，因此，那些數據並不代表iPhone的位置。保留數據的容量是一個錯誤。及後蘋果公司發布一個[IOS
4.3.3的更新](../Page/IOS版本历史.md "wikilink")（[IOS
4.2.8用於CDMA版iPhone](../Page/IOS版本历史.md "wikilink")4），用來減少緩衝的容量、停止備份到iTunes，和每當定位服務被停用時把資料完全抹去\[246\]，上傳到蘋果公司的資料可以由用戶從"系統服務"的"手機網絡搜索"中停用。

### 情報存取

在2013年期間，據報大量監控披露英國及美國的情報機關，分別包括[美國國家安全局（NSA）](../Page/美國國家安全局.md "wikilink")、[政府通訊總局（GCHQ）曾進入iPhone](../Page/政府通信总部.md "wikilink")、[BlackBerry和](../Page/BlackBerry.md "wikilink")[Android手機](../Page/Android.md "wikilink")。當局幾乎能夠讀取所有智能電話的資訊，包括短訊、位置、電郵和筆記\[247\]。

## 限制

蘋果公司對iPhone的某些方面作出嚴格的控制，根據所述，如iPhone般的的出現，比早期版本的[微軟視窗更專門](../Page/微軟視窗.md "wikilink")\[248\]。

[黑客組織已發現許多解決辦法](../Page/黑客.md "wikilink")，大部分都是蘋果公司不允許的，並且很難甚至是不能獲得保養服務\[249\]。[iOS越獄讓用戶可以安裝不在](../Page/iOS越獄.md "wikilink")[應用程式商店下載的應用程式或是修改其基本功能](../Page/App_Store.md "wikilink")。SIM卡解鎖是容許iPhone於不同電訊商的網絡上使用\[250\]，然而在美國，蘋果公司不可以把保養服務判定無效，除非它顯示出現於售後安裝或安置後或組件故障的問題，例如未經授權的應用程式，受到[聯邦貿易委員會的](../Page/聯邦貿易委員會.md "wikilink")監管\[251\]。

iPhone還備有區域和設定，能讓家長設置限制下載的應用程式，或是對使用中的應用程式作出控制和限制\[252\]，而這些限制是需要密碼的輸入\[253\]。

### 啓動

iPhone一般會阻擋其他媒體播放器和網絡功能，除非是受到認可電訊商的授權。在2007年7月3日，[挪威程式員](../Page/挪威.md "wikilink")[Jon
Lech
Johansen在他的博客上指](../Page/约恩·莱克·约翰森.md "wikilink")，他已成功繞過這個要求並破解iPhone的其他功能及結合其他自訂應用程式及修改[iTunes的二進制程式](../Page/iTunes.md "wikilink")，並發布軟件供其他人使用\[254\]。

[英國的](../Page/英國.md "wikilink")[O2網絡](../Page/O2_\(英國\).md "wikilink")，可以讓用戶在網上購買早期版本的iPhone並利用iTunes來把它啓動\[255\]，即使不需要，廠商通常會為買家方便而提供啓動服務；在[美國](../Page/美國.md "wikilink")，蘋果公司已開始為購買iPhone3G和iPhone
3GS買家提供免費送貨服務，改变设备須於店內啓動的方式。[百思买及](../Page/百思买.md "wikilink")[沃爾瑪也將會出售iPhone](../Page/沃爾瑪.md "wikilink")\[256\]。

## 售价

[Iphone_sales_worldwide.svg](https://zh.wikipedia.org/wiki/File:Iphone_sales_worldwide.svg "fig:Iphone_sales_worldwide.svg")

自2007年6月29日起，iPhone开始在美國的蘋果經銷店、蘋果網路商店以及美國電話電報公司（AT\&T
Mobility）的门店以綁約價格499美元（4GB版）以及599美元（8GB版）出售。同年9月5日苹果宣布减价，8GB版改售$399，并撤销4GB版。

9月6日，-{zh-cn:乔布斯;
zh-tw:賈伯斯;}-在公司网站上刊登一封致全体iPhone用户的公开信，对降价一事表示歉意，并承诺对老用户作出补偿。\[257\]2008年6月9日-{zh-cn:乔布斯;
zh-tw:賈伯斯;}-在[WWDC上發報](../Page/WWDC.md "wikilink")3G版本，功能不但有所提升，而8GB版本的售價更由$399美元割降一半至$199美元（全球均不會高於此價，但此售價僅為-{zh-cn:签约价;
zh-hk:上台價;}-，消費者須接受預先綁定的移動運營商服務）；16GB版本售$299美元。

2009年6月9日在[WWDC上宣佈](../Page/WWDC.md "wikilink")3GS版本后，原3G版本的16GB版本停止銷售，而8GB版本售價割降至99美元。

2009年9月9日，在香港Apple Store中，

  - iPhone3G 8GB版售價為4088港币，16G版停止銷售
  - iPhone 3GS 16GB版售價為5388港币，32GB版售價為6288港币。

2009年10月30日，[中国联通版iPhone开始销售](../Page/中国联通.md "wikilink")。用户可以选择与套餐搭配购买或者单独购买裸机。iPhone
3GS 16GB售价为5880元人民币；iPhone 3GS
32GB售价为6999元人民币。上述首次在中国销售的iPhone不含有Wi-Fi功能\[258\]。2010年8月9日和9月25日，含有Wi-Fi功能的8GB版iPhone
3GS和iPhone4分别推出市场。

苹果公司在中国大陆售卖的iPhone5比iPhone4S的原始价格要贵300元人民币。\[259\]

2016年9月，iPhone7在中国大陆的价格比iPhone6s原始价贵100元人民币。\[260\]

2017年11月，iPhone X的售价达到了8316元人民币起售。

### 利潤

市場研究機構iSuppli過去亦曾發佈多次的iPhone拆解報告，包括2007年的首款iPhone，材料成本為217.73美元，2008年的iPhone
3G，材料成本為166.31美元；2009年的3GS，材料成本為170.80美元；2010年6日28日iPhone
4拆機報告指出，儘管設計概念與以往不同，但是以16GB版本的材料成本僅187.51美元，\[261\]而iPhone
4於美國的淨機價為599美元，蘋果公司的毛利率大約在60%到70%之間。相對[三星手機的利潤率為](../Page/三星電子.md "wikilink")10%，而[諾基亞的利潤率為](../Page/諾基亞.md "wikilink")8.9%，可見蘋果公司維持其一貫的高毛利策略。蘋果有很多代工廠，他們核算代工成本構成時，大多按照當地政府規定的最低工資標準來核算人工成本，離任的代工廠[富士康人士說](../Page/富士康.md "wikilink")，蘋果公司是成本控制最嚴格的客戶。業內代工[惠普和](../Page/惠普.md "wikilink")[戴爾的毛利率一般是在](../Page/戴爾.md "wikilink")5到8個點，而蘋果公司代工廠毛利僅2%，但因iPhone在市場上的龐大銷售量，薄利多銷誘因下，仍使代工廠趨之若鶩地貼單生產，甚至彼此搶單競爭。\[262\]

### 銷售量（千部）

| 財政年度 | Q1            | Q2            | Q3            | Q4            | 總銷量       |
| ---- | ------------- | ------------- | ------------- | ------------- | --------- |
| 2007 |               |               | 270\[263\]    | 1,119\[264\]  | 1,389     |
| 2008 | 2,315\[265\]  | 1,703\[266\]  | 717\[267\]    | 6,890\[268\]  | 11,625    |
| 2009 | 4,363\[269\]  | 3,793\[270\]  | 5,208\[271\]  | 7,367\[272\]  | 20,731    |
| 2010 | 8,737\[273\]  | 8,752\[274\]  | 8,398\[275\]  | 14,100\[276\] | 39,987    |
| 2011 | 16,240\[277\] | 18,650\[278\] | 20,340\[279\] | 17,070\[280\] | 72,300    |
| 2012 | 37,044\[281\] | 35,064\[282\] | 26,028\[283\] | 26,910\[284\] | 125,046   |
| 2013 | 47,789\[285\] | 37,430\[286\] | 31,241\[287\] | 33,797\[288\] | 150,257   |
| 2014 | 51,025\[289\] | 43,719\[290\] | 35,203\[291\] | 39,272\[292\] | 169,219   |
| 2015 | 74,468\[293\] | 61,170\[294\] | 47,534\[295\] | 48,046\[296\] | 231,218   |
| 2016 | 74,779\[297\] | 51,193\[298\] | 40,399\[299\] | 45,513\[300\] | 211,884   |
| 2017 | 78,290\[301\] | 50,763\[302\] | 41,026\[303\] | 46,677\[304\] | 216,756   |
| 2018 | 77,316\[305\] | 52,217\[306\] |               |               | 129,533   |
| 財政年度 | Q1            | Q2            | Q3            | Q4            | 2,531,979 |

## 事件

## 參見

  - [iPhone (第一代)](../Page/iPhone_\(第一代\).md "wikilink")
  - [iPhone 3G](../Page/iPhone_3G.md "wikilink")
  - [iPhone 3GS](../Page/iPhone_3GS.md "wikilink")
  - [iPhone 4](../Page/iPhone_4.md "wikilink")
  - [iPhone 4S](../Page/iPhone_4S.md "wikilink")
  - [iPhone 5](../Page/iPhone_5.md "wikilink")
  - [iPhone 5s](../Page/iPhone_5s.md "wikilink")
  - [iPhone 5c](../Page/iPhone_5c.md "wikilink")
  - [iPhone 6](../Page/iPhone_6.md "wikilink")
  - [iPhone 6s](../Page/iPhone_6s.md "wikilink")
  - [iPhone SE](../Page/iPhone_SE.md "wikilink")
  - [iPhone 7](../Page/iPhone_7.md "wikilink")
  - [iPhone 8](../Page/iPhone_8.md "wikilink")
  - [iPhone X](../Page/iPhone_X.md "wikilink")
  - [iPhone XS](../Page/iPhone_XS.md "wikilink")/[iPhone XS
    Max](../Page/iPhone_XS_Max.md "wikilink")
  - [iPhone XR](../Page/iPhone_XR.md "wikilink")
  - [iOS](../Page/iOS.md "wikilink")
  - [苹果公司](../Page/苹果公司.md "wikilink")
  - [iOS設備清單](../Page/iOS設備清單.md "wikilink")
  - [Apple Store](../Page/Apple_Store.md "wikilink")

## 參考文獻

## 外部連結

  - [iPhone美国官网](https://www.apple.com/iphone/)
  - [iPhone臺灣官網](https://www.apple.com/tw/iphone/)
  - [iPhone中国官网](https://www.apple.com/cn/iphone/)
  - [iPhone香港官網](https://www.apple.com/hk/iphone/)

[IPhone](../Category/IPhone.md "wikilink")
[Category:苹果手机](../Category/苹果手机.md "wikilink")
[Category:多點觸控手機](../Category/多點觸控手機.md "wikilink")
[Category:ITunes](../Category/ITunes.md "wikilink")
[Category:行動電話](../Category/行動電話.md "wikilink")
[Category:数字音频播放器](../Category/数字音频播放器.md "wikilink")
[Category:Wi-Fi](../Category/Wi-Fi.md "wikilink")
[Category:IOS](../Category/IOS.md "wikilink")
[Category:IPod](../Category/IPod.md "wikilink")
[Category:觸控手機](../Category/觸控手機.md "wikilink")
[Category:2007年面世的手機](../Category/2007年面世的手機.md "wikilink")
[Category:蘋果公司](../Category/蘋果公司.md "wikilink")

1.   MacRumors|accessdate=2018-08-05|language=en}}

2.

3.

4.

5.
6.

7.

8.

9.  [manuals.info.apple.com/en_US/iphone_user_guide.pdf iPhone user
    guide](http://manuals.info.apple.com/en_US/iphone_user_guide.pdf)
    .（PDF）. Retrieved November 6, 2011\]

10.

11.

12.

13.

14.

15.

16.

17.

18.

19.

20.
21.

22.
23.
24.
25.
26.

27.

28.
29.
30.

31.

32.

33.
34.

35.

36.

37.

38.

39.

40.

41.

42.

43.

44.

45.

46.

47.
48.

49.

50.
51.

52.
53.

54.

55.

56.

57.

58.

59. [HDR photography with iPhone 4 and iOS 4.1: how good is
    it?](http://arstechnica.com/apple/news/2010/09/hdr-photography-with-iphone-4-and-ios-41.ars)
    , Chris Foresman, September 13, 2010, Ars Technica, retrieved at
    June 19, 2011

60.

61.

62.

63.

64.

65. [iOS 7 Beta code reveals new "Mogul" camera mode in new
    iPhone](http://mindofthegeek.com/2013/07/09/ios-7-beta-code-reveals-new-mogul-camera-mode-in-new-iphone/)
    . Mind Of The Geek. Retrieved on July 10, 2013.

66.

67.

68.

69.

70.

71.

72.

73.

74.

75.

76.

77.

78.

79.

80.

81.

82.

83.

84.
85.
86.

87.

88.

89.

90.

91.

92.

93.

94.

95.

96.

97.

98.

99.

100.

101.

102.

103.

104.

105.
106.

107.

108.

109.

110.

111.

112.

113.

114.

115.

116.

117.

118.

119.

120.

121.

122.

123.

124.

125.

126.

127.

128.

129.

130.

131.
132.

133.

134.

135.
136.

137.

138.

139.

140.

141.

142.

143.

144.

145.

146.

147.

148.

149.

150.

151.

152.

153.
154.

155.

156.

157.
158.

159.

160.

161.

162.

163.

164.
165.
166.
167.
168.

169.

170.

171.
172.
173.

174.

175.

176.

177. [iPhone 4S Teardown – Page 2 –
     iFixit](http://www.ifixit.com/Teardown/iPhone-4S-Teardown/6610/2)

178.
179.

180.

181.

182.

183.

184.

185.

186.
187. [iPhone 4 hits FCC, becomes world's second announced pentaband 3G
     phone](https://archive.is/20120730020104/http://www.engadget.com/2010/06/07/iphone-4-hits-fcc-becomes-worlds-second-announced-pentaband-3g/2).
     Engadget. Retrieved on 2011-11-06.

188. [Inside Apple's iPhone 4S and its improved antenna: 'S' is for
     Signal](http://www.appleinsider.com/articles/11/10/13/inside_apples_iphone_4s_and_its_improved_antenna_s_is_for_signal.html)
     . AppleInsider (2011-10-13). Retrieved on 2011-11-06.

189.

190.

191.

192.

193.
194.
195.
196.
197.

198.
199.
200.

201.
202.
203.
204.

205.

206.

207.

208.

209.

210.
211.
212.
213.

214.

215.

216.

217.

218.

219.
220.

221.

222.

223.

224.

225.

226.

227.

228.

229.

230.

231.

232.

233.

234.

235.

236.

237.

238.

239.

240.

241.

242.

243.

244.

245.

246.
247.

248.

249.

250.

251. [FAQ
     Details](http://eshop.macsales.com/Search/displayFAQDetails.cfm?ID=656)
     . Eshop.macsales.com（March 27, 2013）. Retrieved on July 30, 2013.

252.

253.

254.

255.

256.

257. [-{zh-cn:乔布斯; zh-tw:賈伯斯;}-为iPhone降价致歉
     承诺对老用户作出补偿](http://news.xinhuanet.com/newscenter/2007-09/08/content_6685754.htm)
     ，新华网

258.

259.

260.

261. [iSuppli拆解iPhone 4：硬體成本188美元](http://www.ithome.com.tw/itadm/article.php?c=62021)
     iThome每日新聞報文/范眠2010年06月29日

262. [蘋果有血淚：iPhone暴利200%代工廠毛利僅2%](http://finance.sina.com/bg/economy/ausdaily/20100410/185230687.html)
     財經頻道新浪網北美2010年04月10日18:52

263. [Apple Inc.](../Page/Apple_Inc..md "wikilink")（25 July 2007）.
     [Apple Reports Third Quarter
     Results](http://images.apple.com/pr/pdf/q307data_sum.pdf) . Press
     release. Retrieved on 2008-01-23.

264. [Apple Inc.](../Page/Apple_Inc..md "wikilink")（22 October 2007）.
     [Apple Reports Fourth Quarter
     Results](http://www.apple.com/pr/library/2007/10/22results.html) .
     Press release. Retrieved on 2008-01-23.

265. [Apple Inc.](../Page/Apple_Inc..md "wikilink")（22 January 2008）.
     [Apple Reports First Quarter
     Results](http://www.apple.com/pr/library/2008/01/22results.html) .
     Press release. Retrieved on 2008-01-23.

266. [Apple Inc.](../Page/Apple_Inc..md "wikilink")（23 April 2008）.
     [Apple Reports Record Second Quarter
     Results](http://www.apple.com/pr/library/2008/04/23results.html) .
     Press release. Retrieved on 2008-04-23.

267. [Apple Inc.](../Page/Apple_Inc..md "wikilink")（21 July 2008）.
     [Apple Reports Record Third Quarter
     Results](http://www.apple.com/pr/library/2008/07/21results.html) .
     Press release. Retrieved on 2008-07-21.

268. [Apple Inc.](../Page/Apple_Inc..md "wikilink")（21 October 2008）.
     [Apple Reports Record Fourth Quarter
     Results](http://www.apple.com/pr/library/2008/10/21results.html) .
     Press release. Retrieved on 2008-10-21.

269. [Apple Inc.](../Page/Apple_Inc..md "wikilink")（21 January 2009）.
     [Apple Reports First Quarter
     Results](http://www.apple.com/pr/library/2009/01/21results.html) .
     Press release. Retrieved on 2009-01-22.

270. [Apple Inc.](../Page/Apple_Inc..md "wikilink")（22 April 2009）.
     [Apple Reports Second Quarter
     Results](http://www.apple.com/pr/library/2009/04/22results.html) .
     Press release. Retrieved on 2009-04-22.

271. [Apple Inc.](../Page/Apple_Inc..md "wikilink")（22 July 2009）.
     [Apple Reports Third Quarter
     Results](http://www.apple.com/pr/library/2009/07/21results.html) .
     Press release. Retrieved on 2009-07-21.

272. [Apple Inc.](../Page/Apple_Inc..md "wikilink")（19 October 2009）.
     [Apple Reports Fourth Quarter
     Results](http://www.apple.com/pr/library/2009/10/19results.html) .
     Press release. Retrieved on 2009-10-19.

273. [Apple Inc.](../Page/Apple_Inc..md "wikilink")（25 January 2010）.
     [Apple Reports First Quarter
     Results](http://www.apple.com/pr/library/2010/01/25results.html) .
     Press release. Retrieved on 2010-01-25.

274. [Apple Inc.](../Page/Apple_Inc..md "wikilink")（20 April 2010）.
     [Apple Reports Second Quarter
     Results](http://www.apple.com/pr/library/2010/04/20results.html) .
     Press release. Retrieved on 2010-04-20.

275. [Apple Inc.](../Page/Apple_Inc..md "wikilink")（23 July 2010）.
     [Apple Reports Second Quarter
     Results](http://www.apple.com/pr/library/2010/07/20results.html) .
     Press release. Retrieved on 2010-04-20.

276. [Apple Inc.](../Page/Apple_Inc..md "wikilink")（18. October 2010）.
     [Apple Reports Fourth Quarter
     Results](http://www.apple.com/de/pr/library/2010/10/18results.html)
     . Press release. Abgerufen am: 27. Oktober 2010

277. apple.com（18. January 2011）: *[Apple gibt Ergebnisse für das erste
     Quartal
     bekannt](http://www.apple.com/de/pr/library/2011/01/18results.html)
     *, Zugriff am 11. April 2011

278. apple.com（20. April 2011）: *[Apple gibt Ergebnisse für das zweite
     Quartal
     bekannt](http://www.apple.com/de/pr/library/2011/04/20results.html)
     *, Zugriff am 20. April 2011

279. apple.com（19. July 2011）: *[Apple gibt Ergebnisse für das dritte
     Quartal
     bekannt](http://www.apple.com/de/pr/library/2011/04/20results.html)
     *, Zugriff am 20. Juli 2011

280. apple.com（18. October 2011）: *[Apple gibt Ergebnisse für das Vierte
     Quartal
     bekannt](http://www.apple.com/de/pr/library/2011/10/18Apple-Reports-Fourth-Quarter-Results.html)
     *, Zugriff am 22. Oktober 2011

281. apple.com（24. January 2012）: *[Apple gibt Ergebnisse für das erste
     Quartal bekannt](http://images.apple.com/pr/pdf/q1fy12datasum.pdf)
     *, Zugriff am 24. January 2012

282. apple.com（25. April 2012）: *[Apple gibt Ergebnisse für das zweite
     Quartal bekannt](http://images.apple.com/pr/pdf/q2fy12datasum.pdf)
     *, Zugriff am 25. April 2012

283. apple.com（24. July 2012）: *[Apple gibt Ergebnisse für das dritte
     Quartal bekannt](http://images.apple.com/pr/pdf/q3fy12datasum.pdf)
     *, Zugriff am 24. Juli 2012

284. apple.com（25. Oct 2012）: "[Apple Reports Fourth Quarter
     Results](http://images.apple.com/pr/pdf/q4fy12datasum.pdf) ", Jack
     Su pm 28.Oct 2012

285. apple.com（23. January 2013）: *[Q1 2013 Unaudited Summary
     Data](http://images.apple.com/pr/pdf/q1fy13datasum.pdf) *, Press
     release. Retrieved on 2013-01-23

286. apple.com（23. April 2013）: *[Q2 2013 Unaudited Summary
     Data](http://images.apple.com/pr/pdf/q2fy13datasum2.pdf) *, Press
     release. Retrieved on 2013-04-23

287. apple.com（23. July 2013）: *[Q3 2013 Unaudited Summary
     Data](http://images.apple.com/pr/pdf/q3fy13datasum.pdf) *, Press
     release. Retrieved on 2013-07-23

288. apple.com（28. Oct 2013）: *[Q4 2013 Unaudited Summary
     Data](http://images.apple.com/pr/pdf/q4fy13datasum.pdf) *, Press
     release. Retrieved on 2013-10-28

289. apple.com（27. January 2014）: *[Q1 2014 Unaudited Summary
     Data](https://www.apple.com/pr/pdf/q1fy14datasum.pdf) *, Press
     release. Retrieved on 2014-01-27

290. apple.com（23. April 2014）: *[Q2 2014 Unaudited Summary
     Data](http://images.apple.com/pr/pdf/q2fy14datasum.pdf) *, Press
     release. Retrieved on 2014-04-23

291. apple.com（22. July 2014）: *[Q3 2014 Unaudited Summary
     Data](https://www.apple.com/pr/pdf/q3fy14datasum.pdf) *, Press
     release. Retrieved on 2014-07-22

292. apple.com（20. Oct 2014）: *[Q4 2014 Unaudited Summary
     Data](https://www.apple.com/pr/pdf/q4fy14datasum.pdf) *, Press
     release. Retrieved on 2014-10-20

293. apple.com（27. January 2015）: *[Q1 2015 Unaudited Summary
     Data](https://www.apple.com/pr/pdf/q1fy15datasum.pdf) *, Press
     release. Retrieved on 2015-01-27

294. apple.com（27. April 2015）: *[Q2 2015 Unaudited Summary
     Data](http://images.apple.com/pr/pdf/q2fy15datasum.pdf) *, Press
     release. Retrieved on 2015-04-27

295. apple.com（21. July 2015）: *[Q3 2015 Unaudited Summary
     Data](https://www.apple.com/pr/pdf/q3fy15datasum_2.pdf) *, Press
     release. Retrieved on 2015-07-21

296.

297.

298.

299.

300.

301.

302.

303.

304.

305.

306.