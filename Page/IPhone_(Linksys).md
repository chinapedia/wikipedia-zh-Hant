**iPhone**是最初由[Infogear開發的](../Page/Infogear.md "wikilink")[資訊家電](../Page/資訊家電.md "wikilink")，現由[Linksys開發](../Page/Linksys.md "wikilink")。

## 歷史

**iPhone**由Infogear公司首次於1997年發售，為一台外形尺寸像[傳真機的大型桌上型](../Page/傳真機.md "wikilink")[電話](../Page/電話.md "wikilink")，擁有一個[觸控屏與改版的](../Page/觸控屏.md "wikilink")[JavaOS](../Page/JavaOS.md "wikilink")，能運作為網頁瀏覽器或電話\[1\]。該電話由於銷量並不理想而被終止。Infogear被[思科系統收購後繼續保留該名稱為註册](../Page/思科系統.md "wikilink")[商標](../Page/商標.md "wikilink")。

2006年，思科旗下公司[Linksys開發及發售iPhone的新型號](../Page/Linksys.md "wikilink")，這一系列
Cisco/LinkSys iPhone
是使用[Skype系統的](../Page/Skype.md "wikilink")[DECT](../Page/DECT.md "wikilink")[無線式](../Page/无绳电话.md "wikilink")[IP電話](../Page/IP電話.md "wikilink")。與其前作不同，它使用現有網絡與一個私有的操作系統。許多分析家推測思科重新啓用這個長期沒有使用的商標是為了在蘋果電腦知名音頻播放器[iPod的](../Page/iPod.md "wikilink")[月暈效應中獲益](../Page/月暈效應.md "wikilink")。\[2\]

## 機型

  - Cordless Internet Telephony Kit (iPhone) CIT200

<!-- end list -->

  -
    使用[DECT技術的無線式網路電話](../Page/DECT.md "wikilink")，一台母機最多可配4-{台}-子機。以
    USB 介面與電腦相連，使用電腦上的 Skype

<!-- end list -->

  - Dual-Mode Cordless Internet Telephony Kit (iPhone) CIT300

<!-- end list -->

  -
    可使用Skype與一般電話線路的雙模電話暨DECT無線電話。一台母機一樣最多可配4-{台}-子機。

<!-- end list -->

  - Dual-Mode Cordless Internet Telephony Kit (iPhone) CIT400

<!-- end list -->

  -
    具有[免持聽筒對話功能的Skype與一般電話線路的雙模電話暨DECT無線電話](../Page/免持聽筒.md "wikilink")，並有新的造型。直接連接網路線與電話線，無須電腦。

## 參考

<references/>

## 外部連結

  - [CIT200
    技術支援/概要](https://archive.is/20130425133805/http://homesupport.cisco.com/en-apac/support/voip/CIT200)
  - [CIT300
    技術支援/概要](https://web.archive.org/web/20121213152554/http://homesupport.cisco.com/en-apac/support/voip/CIT300)
  - [CIT400
    技術支援/概要](https://web.archive.org/web/20120317040025/http://homesupport.cisco.com/en-apac/support/voip/CIT400)

[Category:資訊設備](../Category/資訊設備.md "wikilink")
[Category:电信设备](../Category/电信设备.md "wikilink")
[Category:VoIP](../Category/VoIP.md "wikilink")

1.  ["Telephone meets touch-screen Internet
    appliance"](http://www.cnn.com/TECH/computing/9811/02/netphone.idg/)摘自[CNN](../Page/CNN.md "wikilink")，1998年11月2日。
2.  ["Here comes the iPhone, but it's not from
    Apple"](http://www.canada.com/topics/technology/news/gizmos/story.html?id=ee82bef1-e58c-454b-b959-dac8aabd1e15&k=68995)
    摘自[Canada.com](../Page/Canada.com.md "wikilink")，2006年12月19日。