**網際網路協定位址**（，又譯為**网际协议地址**），縮寫為**IP地址**（），是分配給[網路上使用](../Page/網路.md "wikilink")[網際協議](../Page/網際協議.md "wikilink")（）的裝置的數字標籤。常見的IP位址分為[IPv4與](../Page/IPv4.md "wikilink")[IPv6兩大類](../Page/IPv6.md "wikilink")，但是也有其他不常用的小分類。

## IPv4位址

IP地址由32位[二进制数组成](../Page/二进制.md "wikilink")，为便于使用，常以XXX.XXX.XXX.XXX形式表现，每组XXX代表小于或等于255的10进制数。例如[維基媒體的一个IP位址是](../Page/維基媒體.md "wikilink")208.80.152.2。位址可分为A、B、C、D、E五大类，其中**E类**属于特殊保留地址。

IP地址是唯一的。目前IPv4技术可能使用的IP地址最多可有4,294,967,296個（即2<sup>32</sup>）。骤看可能觉得很难会用尽，但由于早期编码和分配上的問題，使很多区域的编码实际上被空出或不能使用。加上互联网的普及，使大部分家庭都至少有一部电脑，连同公司的电脑，以及连接网络的各种设备都消耗大量IPv4地址资源。

隨著[互聯網的快速成長](../Page/互聯網.md "wikilink")，IPv4的42億個地址的分配最終於2011年2月3日用盡\[1\]\[2\]。相应的科研组织已研究出128位的[IPv6](../Page/IPv6.md "wikilink")，其IP位址数量最高可达3.402823669
× 10<sup>38</sup>个，屆時每個人家居中的每件電器，每件物件，甚至地球上每一粒沙子都可以擁有自己的IP位址。

在A类、B类、C类IP地址中，如果主机号是全1，那么这个地址为直接广播地址，它是用来使路由器将一个分组以广播形式发送给特定网络上的所有主机。[32位全为](../Page/32位.md "wikilink")1的**IP地址**「255.255.255.255」为**受限广播地址**（"limited
broadcast" destination
address），用来将一个分组以广播方式发送给本[网络中的所有](../Page/电脑网络.md "wikilink")[主机](../Page/主机.md "wikilink")，[路由器则阻挡该分组通过](../Page/路由器.md "wikilink")，将其广播功能限制在本网内部。

## IPv6位址

### IPv6编址

从IPv4到IPv6最显著的变化就是网络地址的长度。RFC 2373和RFC
2374定义的IPv6地址，就像下面章节所描述的，有128位长；IPv6地址的表达形式，一般采用32个十六进制数。

IPv6中可能的地址有2<sup>128</sup>≈3.4×10<sup>38</sup>个，具体数量为340,282,366,920,938,463,463,374,607,431,768,211,456个。也可以想象为16<sup>32</sup>个，因为32位地址每位可以取16个不同的值。
在很多场合，IPv6地址由两个逻辑部分组成：一个64位的网络前缀和一个64位的主机地址，主机地址通常根据物理地址自动生成，叫做EUI-64（或者64-位扩展唯一标识）

### IPv6地址表示

IPv6地址为128位元长，但通常写作8组，每组四个十六进制数的形式。例如：

  - 2001:0db8:85a3:08d3:1319:8a2e:0370:7344

是一个合法的IPv6地址。

如果四个数字都是0，可以被省略。例如：

  - 2001:0db8:85a3:0000:1319:8a2e:0370:7344

等价于

  - 2001:0db8:85a3::1319:8a2e:0370:7344

遵从这些规则，如果因为省略而出现了两个以上的冒号的话，可以压缩为一个，但这种零压缩在地址中只能出现一次。因此：

  - 2001:0DB8:0000:0000:0000:0000:1428:57ab
  - 2001:0DB8:0000:0000:0000::1428:57ab
  - 2001:0DB8:0:0:0:1428:57ab
  - 2001:0DB8:0::0:1428:57ab
  - 2001:0DB8::1428:57ab

都是合法的地址，并且他们是等价的。但

  - 2001::25de::cade

是非法的。（因为这样会使得搞不清楚每个压缩中有几个全零的分组）

同时前导的零可以省略，因此：

  - 2001:0DB8:02de::0e13

等价于

  - 2001:DB8:2de::e13

如果这个地址实际上是IPv4的地址，后32位元可以用10进制数表示；因此：

  -

      -
        ffff:192.168.89.9等价于::ffff:c0a8:5909。
        ffff:1.2.3.4格式叫做**IPv4映射地址**。

IPv4地址可以很容易的转化为IPv6格式。举例来说，如果IPv4的一个地址为135.75.43.52（十六进制为0x874B2B34），即可转化为0000:0000:0000:0000:0000:FFFF:874B:2B34或者::FFFF:874B:2B34。同时，还可以使用混合符号（IPv4-compatible
address），则地址可为::FFFF:135.75.43.52。

## 参考资料

## 參見

  - [分类网络](../Page/分类网络.md "wikilink")
  - [在线IP地址查询工具](https://myipaddress.pro/cn)
  - [ipip.net](https://ipip.net) 通常IP库较新

[Category:网际协议](../Category/网际协议.md "wikilink")
[Category:网络层协议](../Category/网络层协议.md "wikilink")
[Category:網路結構](../Category/網路結構.md "wikilink")
[Category:網路定址](../Category/網路定址.md "wikilink")

1.
2.