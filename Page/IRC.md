**IRC**（的缩写，“**因特网中继聊天**”）是一个基于[应用层协议的](../Page/应用层.md "wikilink")[多平台](../Page/多平台.md "wikilink")[即时通信软件](../Page/即时通信软件.md "wikilink")。其主要用于群体聊天，但同样也可以用于个人对个人的聊天。IRC使用的伺服器埠有6667（明文傳輸，如<irc://irc.freenode.net>）、6697（[SSL加密傳輸](../Page/SSL.md "wikilink")，如<ircs://irc.freenode.net:6697>）等。

[芬兰人雅爾可](../Page/芬兰.md "wikilink")·歐伊卡利寧（Jarkko
Oikarinen）于1988年8月创造了IRC来取代一个叫做[MUT的程序](../Page/MUT.md "wikilink")。

## 技术信息

IRC是一种公开的协议，采用[TCP和](../Page/傳輸控制協定.md "wikilink")[SSL协议](../Page/SSL.md "wikilink")。一个IRC服务器可以连接其他的IRC服务器以扩展为一个IRC网络。IRC用户透過客户端软件和服务器相连。大多数的IRC服务器不需要客户注册登录，虽然在连接前必须设定好昵称（nickname），但客户端一般都会自动分配一个。

## IRC结构

### IRC服务器

IRC是一个分布式的[客户端/服务器结构](../Page/客户端/服务器结构.md "wikilink")。通过连接到一个IRC服务器，我们可以访问这个服务器以及它所连接的其他服务器上的频道。
要使用IRC，必须先登录到一个IRC服务器上，最常见的为[`irc.freenode.net`](../Page/freenode.md "wikilink")。

### IRC频道

频道存在于一个IRC服务器上。一个频道类似于一个聊天室，频道名称必须以\#符号开始，例如`#irchelp`。

### 用戶端

#### 应用程序

  - [mIRC曾被認為是Windows操作系统下最受歡迎](../Page/mIRC.md "wikilink")、應用最廣的IRC用戶端軟件\[1\]。
  - [ChatZilla是](../Page/ChatZilla.md "wikilink")[Mozilla瀏覽器下的IRC用戶端程序](../Page/Mozilla.md "wikilink")，基於[JavaScript和](../Page/JavaScript.md "wikilink")[XUL語言編寫的](../Page/XUL.md "wikilink")。
  - [Opera瀏覽器有內建的IRC用戶端軟件](../Page/Opera.md "wikilink")。
  - [Pidgin支持IRC網路](../Page/Pidgin.md "wikilink")。
  - [XChat跨平台IRC客户端軟件](../Page/XChat.md "wikilink")
  - [HydraIRC](https://web.archive.org/web/20110128122510/http://hydrairc.com/)是[GPL授權對應Windows系統的IRC客户端軟件](../Page/GPL.md "wikilink")
  - [KVIrc是一款注重圖形的](../Page/KVIrc.md "wikilink")[GPL授權IRC客户端軟件](../Page/GPL.md "wikilink")，基於[Qt](../Page/Qt.md "wikilink")。
  - [Irssi](../Page/Irssi.md "wikilink")
    [類UNIX系統下](../Page/類UNIX.md "wikilink")[命令行界面的IRC用戶端](../Page/命令行界面.md "wikilink")，常常與[Screen一起搭配使用](../Page/GNU_Screen.md "wikilink")。
  - [Colloquy](http://colloquy.info/)是[Mac OS
    X下的免费IRC客户端](../Page/Mac_OS_X.md "wikilink")，界面设计友好，还有Console可以监控背后针对每个频道执行所有IRC命令。
  - [WeeChat也是一款类UNIX系统下的](../Page/WeeChat.md "wikilink")[CLI界面的IRC客户端](../Page/CLI.md "wikilink")。
  - [QuasselIRC](http://www.quassel-irc.org/)是一款多平台下IRC客户端。
  - [HexChat](http://hexchat.github.io/)支持Windows、Linux、OSX平台，並且開放原始碼。
  - [LimeChat](http://limechat.net/)由日本人开发多平台IRC客户端软件。
  - [HoloIRC](https://github.com/tilal6991/HoloIRC)是由tilal6991开发的安卓客户端，界面采用了[质感设计](../Page/质感设计.md "wikilink")。

#### 网页端

  - [Kiwi IRC](https://kiwiirc.com/nextclient/)

## IRC机器人

IRC机器人是一些运行在后台或者服务器上的程序，透過登陆某一个频道，分析接受到的内容并做出相应的动作。
最著名的是[mobibot](http://www.mobitopia.org/mobibot/)，可以通过命令`mobibot:
weather
ZBAA`查询天气预报。[ChanServ提供IRC頻道註冊與登入服務的功能](../Page/ChanServ.md "wikilink")，也可以透過它取得頻道管理者的權限。

## 参考文献

## 外部連結

  - [irchelp.org](http://www.irchelp.org/) IRC帮助文件

  - [Chatcafe，香港唯一一個IRC網路](http://www.chatcafe.net)

  - [KVIrc的官方網址](http://www.kvirc.net/)

  -
  - [IRC Numerics List](https://defs.ircdocs.horse/defs/numerics.html)

  - [History of IRC](http://www.irc.org/history.html) - IRC的历史

  - [IRC.org](http://www.irc.org/) – IRC官方网站

  - [IRCv3](http://ircv3.net/) – IRC开发人员工作组，他们为IRC协议添加新功能并编写规范

  - [IRC-Source](https://irc-source.com/) – 可以查询IRC频道历史数据的搜索引擎

  - [irc.netsplit.de](http://irc.netsplit.de/) –
    包含历史数据的Internet中继聊天（IRC）网络列表

## RFC技術規格文件链接

  - [Internet Relay Chat:
    Architecture](http://www.ietf.org/rfc/rfc2810.txt)
  - [Internet Relay Chat: Channel
    Management](http://www.ietf.org/rfc/rfc2811.txt)
  - [Internet Relay Chat: Client
    Protocol](http://www.ietf.org/rfc/rfc2812.txt)
  - [Internet Relay Chat: Server
    Protocol](http://www.ietf.org/rfc/rfc2813.txt)

[Category:IRC](../Category/IRC.md "wikilink")
[Category:芬兰发明](../Category/芬兰发明.md "wikilink")
[Category:1988年软件](../Category/1988年软件.md "wikilink")
[Category:網路術語](../Category/網路術語.md "wikilink")
[Category:虛擬社群](../Category/虛擬社群.md "wikilink")
[Category:应用层协议](../Category/应用层协议.md "wikilink")

1.