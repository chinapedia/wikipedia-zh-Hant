**IRT百老匯-第七大道線**（），亦稱為**第七大道線**，是[紐約地鐵](../Page/紐約地鐵.md "wikilink")[IRT系統中的一條主要路線](../Page/跨區捷運公司.md "wikilink")。[紐約地鐵1號線](../Page/紐約地鐵1號線.md "wikilink")、[2號線](../Page/紐約地鐵2號線.md "wikilink")、[紐約地鐵3號線](../Page/紐約地鐵3號線.md "wikilink")、以及過去的[9號線等服務路線行經於此路線](../Page/紐約地鐵9號線.md "wikilink")。此路線主要行經上西城、[中城](../Page/曼哈頓中城.md "wikilink")、[紐約蘇豪區](../Page/紐約蘇豪區.md "wikilink")、以及下城，位於[布朗克斯](../Page/布朗克斯.md "wikilink")[范科特蘭公園-242街車站與](../Page/范科特蘭公園-242街車站_\(IRT百老匯-第七大道線\).md "wikilink")[曼哈頓](../Page/曼哈頓.md "wikilink")[南碼頭-白廳街車站之間](../Page/南碼頭-白廳街車站.md "wikilink")\[1\]\[2\]
。另外在[錢伯斯街車站分支出後往東南方走的](../Page/錢伯斯街車站_\(IRT百老匯－第七大道線\).md "wikilink")**布魯克林支線**雖然經過前往[布魯克林區政府大樓](../Page/布魯克林區.md "wikilink")，但是還是屬於百老匯-第七大道線的一部分\[3\]
。

## 服務

以下路線使用全部或部分百老匯-第七大道線，服務路線顏色為紅色：

<table>
<thead>
<tr class="header">
<th><p>路線</p></th>
<th><p>時段</p></th>
<th><p>路段</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/范科特蘭公園-242街車站_(IRT百老匯-第七大道線).md" title="wikilink">242街</a><br />
與<a href="../Page/96街車站_(IRT百老匯-第七大道線).md" title="wikilink">96街之間</a></p></td>
<td><p>96街<br />
與<a href="../Page/時報廣場-42街車站_(IRT百老匯-第七大道線).md" title="wikilink">42街之間</a></p></td>
<td><p>42街<br />
與<a href="../Page/14街車站_(IRT百老匯-第七大道線).md" title="wikilink">14街之間</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>任何時候</p></td>
<td><p>慢車</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>平日及平日深夜</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>週末及週末深夜</p></td>
<td></td>
<td><p>慢車</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>平日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>平日深夜</p></td>
<td></td>
<td><p>快車</p></td>
</tr>
<tr class="odd">
<td><p>週末及週末深夜</p></td>
<td></td>
<td><p>快車</p></td>
</tr>
</tbody>
</table>

## 車站列表

<table>
<thead>
<tr class="header">
<th><p>鄰里<br />
（大約）</p></th>
<th></th>
<th><p>中文站名</p></th>
<th><p>英文站名</p></th>
<th><p>軌道</p></th>
<th><p>服務</p></th>
<th><p>啟用</p></th>
<th><p>轉乘和備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/范科特蘭公園-242街車站_(IRT百老匯-第七大道線).md" title="wikilink">范科特蘭公園-242街</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>1908年8月1日[4]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>中央快速軌道開始（無服務）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>軌道連往<a href="../Page/紐約地鐵車廠#240街車廠.md" title="wikilink">240街車廠</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>和</p></td>
<td></td>
<td><p><a href="../Page/238街車站_(IRT百老匯-第七大道線).md" title="wikilink">238街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1908年8月1日[5]</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/231街車站_(IRT百老匯-第七大道線).md" title="wikilink">231街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1907年1月27日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大理石山.md" title="wikilink">大理石山</a></p></td>
<td></td>
<td><p><a href="../Page/大理石山-225街車站_(IRT百老匯-第七大道線).md" title="wikilink">大理石山-225街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1907年1月14日[6]</p></td>
<td><p><a href="../Page/大都會北方鐵路.md" title="wikilink">大都會北方鐵路</a>（，）</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/英伍德.md" title="wikilink">英伍德</a></p></td>
<td></td>
<td><p><a href="../Page/221街車站_(IRT百老匯-第七大道線).md" title="wikilink">221街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1906年3月12日[7]</p></td>
<td><p>1907年1月14日關閉</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/215街車站_(IRT百老匯-第七大道線).md" title="wikilink">215街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1906年3月12日[8]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>軌道連往<a href="../Page/紐約地鐵車廠#207街車廠.md" title="wikilink">207街車廠</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/207街車站_(IRT百老匯-第七大道線).md" title="wikilink">207街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1907年[9]</p></td>
<td><p>Bx12選擇巴士服務</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>中央快車軌道結束</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p> ↓</p></td>
<td><p><a href="../Page/迪克曼街車站_(IRT百老匯-第七大道線).md" title="wikilink">迪克曼街</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>1906年3月12日[10]</p></td>
<td><p>車站只限南行可<a href="../Page/美國殘疾人法案.md" title="wikilink">無障礙通行</a>。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/華盛頓高地.md" title="wikilink">華盛頓高地</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aiga_elevator.svg" title="fig:Aiga_elevator.svg">Aiga_elevator.svg</a></p></td>
<td><p><a href="../Page/191街車站_(IRT百老匯-第七大道線).md" title="wikilink">191街</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>1911年1月14日[11]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aiga_elevator.svg" title="fig:Aiga_elevator.svg">Aiga_elevator.svg</a></p></td>
<td><p><a href="../Page/181街車站_(IRT百老匯-第七大道線).md" title="wikilink">181街</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>1906年5月30日[12]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aiga_elevator.svg" title="fig:Aiga_elevator.svg">Aiga_elevator.svg</a></p></td>
<td><p><a href="../Page/168街車站_(IRT百老匯-第七大道線).md" title="wikilink">168街</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>1906年4月14日[13]</p></td>
<td><p><a href="../Page/IND第八大道線.md" title="wikilink">IND第八大道線</a>（）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/157街車站_(IRT百老匯-第七大道線).md" title="wikilink">157街</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>1904年11月12日[14]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>中央快速軌道開始（無服務）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/哈萊姆區.md" title="wikilink">哈萊姆區</a></p></td>
<td></td>
<td><p><a href="../Page/145街車站_(IRT百老匯-第七大道線).md" title="wikilink">145街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[15]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/紐約地鐵車廠#137街車廠.md" title="wikilink">137街車廠軌道圍繞主線</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/137街-市立學院車站_(IRT百老匯-第七大道線).md" title="wikilink">137街-市立學院</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[16]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/125街車站_(IRT百老匯-第七大道線).md" title="wikilink">125街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[17]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/晨邊高地.md" title="wikilink">晨邊高地</a></p></td>
<td></td>
<td><p><a href="../Page/116街-哥倫比亞大學車站_(IRT百老匯-第七大道線).md" title="wikilink">116街-哥倫比亞大學</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[18]</p></td>
<td><p>M60選擇巴士服務往<a href="../Page/拉瓜地亞機場.md" title="wikilink">拉瓜地亞機場</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>nowrap|<a href="../Page/教堂大道-110街車站_(IRT百老匯-第七大道線).md" title="wikilink">教會大道-110街</a></p></td>
<td><p>nowrap|</p></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[19]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/上西城.md" title="wikilink">上西城</a></p></td>
<td></td>
<td><p><a href="../Page/103街車站_(IRT百老匯-第七大道線).md" title="wikilink">103街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[20]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>中央快速軌道結束</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/IRT萊諾克斯大道線.md" title="wikilink">IRT萊諾克斯大道線匯入快車軌道</a>（）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/96街車站_(IRT百老匯-第七大道線).md" title="wikilink">96街</a></p></td>
<td></td>
<td><p>全部</p></td>
<td><p>nowrap|</p></td>
<td><p>1904年10月27日[21]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/91街車站_(IRT百老匯-第七大道線).md" title="wikilink">91街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[22]</p></td>
<td><p>1959年2月2日</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/86街車站_(IRT百老匯-第七大道線).md" title="wikilink">86街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[23]</p></td>
<td><p>M86選擇巴士服務</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/79街車站_(IRT百老匯-第七大道線).md" title="wikilink">79街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[24]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/72街車站_(IRT百老匯-第七大道線).md" title="wikilink">72街</a></p></td>
<td></td>
<td><p>全部</p></td>
<td></td>
<td><p>1904年10月27日[25]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/66街-林肯中心車站_(IRT百老匯-第七大道線).md" title="wikilink">66街-林肯中心</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[26]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/曼哈頓中城.md" title="wikilink">曼哈頓中城</a></p></td>
<td></td>
<td><p><a href="../Page/59街-哥倫布圓環車站_(IRT百老匯-第七大道線).md" title="wikilink">59街-哥倫布圓環</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[27]</p></td>
<td><p><a href="../Page/IND第八大道線.md" title="wikilink">IND第八大道線</a>（）</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/50街車站_(IRT百老匯-第七大道線).md" title="wikilink">50街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[28]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>匯入<a href="../Page/紐約地鐵42街接駁線.md" title="wikilink">紐約地鐵42街接駁線北行慢車軌道</a>（無服務）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/時報廣場-42街車站_(IRT百老匯-第七大道線).md" title="wikilink">時報廣場-42街</a></p></td>
<td></td>
<td><p>全部</p></td>
<td></td>
<td><p>1917年6月3日[29]</p></td>
<td><p><a href="../Page/IRT法拉盛線.md" title="wikilink">IRT法拉盛線</a>（）<br />
<a href="../Page/IND第八大道線.md" title="wikilink">IND第八大道線</a>（），<a href="../Page/42街-航港局客運總站車站_(IND第八大道線).md" title="wikilink">42街-航港局客運總站</a><br />
<a href="../Page/BMT百老匯線.md" title="wikilink">BMT百老匯線</a>（）<br />
<a href="../Page/紐約地鐵42街接駁線.md" title="wikilink">紐約地鐵42街接駁線</a>（）<br />
<a href="../Page/紐新航港局客運總站.md" title="wikilink">紐新航港局客運總站</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/34街-賓州車站_(IRT百老匯-第七大道線).md" title="wikilink">34街-賓州車站</a></p></td>
<td></td>
<td><p>全部</p></td>
<td></td>
<td><p>1917年6月3日[30]</p></td>
<td><p>可連接<a href="../Page/美鐵.md" title="wikilink">美鐵</a>、<a href="../Page/長島鐵路.md" title="wikilink">長島鐵路及</a><a href="../Page/新澤西通勤鐵路.md" title="wikilink">新澤西通勤鐵路</a>，<a href="../Page/賓夕法尼亞車站_(紐約市).md" title="wikilink">賓夕法尼亞車站</a><br />
M34 / M34A選擇巴士服務</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/雀兒喜_(紐約).md" title="wikilink">雀兒喜</a></p></td>
<td></td>
<td><p><a href="../Page/28街車站_(IRT百老匯-第七大道線).md" title="wikilink">28街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1918年7月1日[31]</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/23街車站_(IRT百老匯-第七大道線).md" title="wikilink">23街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1918年7月1日[32]</p></td>
<td><p>M23選擇巴士服務</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/18街車站_(IRT百老匯-第七大道線).md" title="wikilink">18街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1918年7月1日[33]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/14街車站_(IRT百老匯-第七大道線).md" title="wikilink">14街</a></p></td>
<td></td>
<td><p>全部</p></td>
<td></td>
<td><p>1918年7月1日[34]</p></td>
<td><p><a href="../Page/IND第六大道線.md" title="wikilink">IND第六大道線</a>（），<a href="../Page/14街車站_(IND第六大道線).md" title="wikilink">14街</a><br />
<a href="../Page/BMT卡納西線.md" title="wikilink">BMT卡納西線</a>（），<a href="../Page/第六大道車站_(BMT卡納西線).md" title="wikilink">第六大道</a><br />
連往<a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/14街車站_(PATH).md" title="wikilink">14街</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/格林尼治村.md" title="wikilink">格林尼治村</a></p></td>
<td></td>
<td><p><a href="../Page/克里斯多福街-謝里登廣場車站_(IRT百老匯-第七大道線).md" title="wikilink">克里斯多福街-謝里登廣場</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1918年7月1日[35]</p></td>
<td><p>連往<a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/克里斯托弗街車站_(PATH).md" title="wikilink">克里斯托弗街</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/豪斯頓街車站_(IRT百老匯-第七大道線).md" title="wikilink">豪斯頓街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1918年7月1日[36]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/翠貝卡.md" title="wikilink">翠貝卡</a></p></td>
<td></td>
<td><p><a href="../Page/堅尼街車站_(IRT百老匯-第七大道線).md" title="wikilink">堅尼街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1918年7月1日[37]</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/富蘭克林街車站_(IRT百老匯-第七大道線).md" title="wikilink">富蘭克林街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1918年7月1日[38]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曼哈頓金融區.md" title="wikilink">金融區</a></p></td>
<td></td>
<td><p><a href="../Page/錢伯斯街車站_(IRT百老匯-第七大道線).md" title="wikilink">錢伯斯街</a></p></td>
<td></td>
<td><p>全部</p></td>
<td></td>
<td><p>1918年7月1日[39]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>快速軌道分支往布魯克林支線（）；慢車軌道作為主線繼續（）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/世貿科特蘭車站_(IRT百老匯-第七大道線).md" title="wikilink">世貿科特蘭</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1918年7月1日[40]</p></td>
<td><p>自2001年9月11日關閉，2018年9月8日重啟<br />
連往<a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/世界貿易中心車站_(PATH).md" title="wikilink">世界貿易中心</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/雷克托街車站_(IRT百老匯-第七大道線).md" title="wikilink">雷克托街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1918年7月1日[41]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>主線和外圈於<a href="../Page/南碼頭-白廳街車站#舊站.md" title="wikilink">南碼頭分支</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/南碼頭-白廳街車站#舊站.md" title="wikilink">南碼頭</a><br />
<small>（舊站）</small></p></td>
<td></td>
<td><p>外環</p></td>
<td></td>
<td><p>1918年7月1日[42]</p></td>
<td><p>2009年3月16日新總站啟用時關閉<br />
2013年4月4日作為臨時1號線總站重啟，2017年6月27日再次關閉</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/南碼頭-白廳街車站#新站.md" title="wikilink">南碼頭</a><br />
<small>（新月台）</small></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>2009年3月16日[43]</p></td>
<td><p>2012年11月因颶風桑迪的破壞而關閉，2017年6月27日重開<br />
<a href="../Page/BMT百老匯線.md" title="wikilink">BMT百老匯線</a>（），<a href="../Page/白廳街-南碼頭車站_(BMT百老匯線).md" title="wikilink">白廳街-南碼頭</a><br />
M15選擇巴士服務<br />
<a href="../Page/史泰登島渡輪.md" title="wikilink">史泰登島渡輪</a>，</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>主線總站（）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>布魯克林支線（）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曼哈頓金融區.md" title="wikilink">金融區</a></p></td>
<td></td>
<td><p><a href="../Page/帕克廣場車站_(IRT百老匯-第七大道線).md" title="wikilink">帕克廣場</a></p></td>
<td></td>
<td><p>快車</p></td>
<td></td>
<td><p>1918年8月1日</p></td>
<td><p><a href="../Page/IND第八大道線.md" title="wikilink">IND第八大道線</a>（），<a href="../Page/錢伯斯街車站_(IND第八大道線).md" title="wikilink">錢伯斯街</a><br />
IND第八大道線（），<a href="../Page/世界貿易中心車站_(IND第八大道線).md" title="wikilink">世界貿易中心</a><br />
連往<a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/世界貿易中心車站_(PATH).md" title="wikilink">世界貿易中心</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/福爾頓街車站_(IRT百老匯-第七大道線).md" title="wikilink">福爾頓街</a></p></td>
<td></td>
<td><p>快車</p></td>
<td></td>
<td><p>1918年8月1日</p></td>
<td><p><a href="../Page/IRT萊辛頓大道線.md" title="wikilink">IRT萊辛頓大道線</a>（）<br />
<a href="../Page/IND第八大道線.md" title="wikilink">IND第八大道線</a>（）<br />
<a href="../Page/BMT納蘇街線.md" title="wikilink">BMT納蘇街線</a>（）<br />
連往<a href="../Page/BMT百老匯線.md" title="wikilink">BMT百老匯線</a>（），<a href="../Page/科特蘭街車站_(BMT百老匯線).md" title="wikilink">科特蘭街經</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/華爾街車站_(IRT百老匯-第七大道線).md" title="wikilink">華爾街</a></p></td>
<td></td>
<td><p>快車</p></td>
<td></td>
<td><p>1918年8月1日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>克拉克街隧道</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/布魯克林高地.md" title="wikilink">布魯克林高地</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aiga_elevator.svg" title="fig:Aiga_elevator.svg">Aiga_elevator.svg</a></p></td>
<td><p><a href="../Page/克拉克街車站_(IRT百老匯-第七大道線).md" title="wikilink">克拉克街</a></p></td>
<td></td>
<td><p>快車</p></td>
<td></td>
<td><p>1919年4月15日</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/區公所車站_(IRT百老匯-第七大道線).md" title="wikilink">區公所</a></p></td>
<td></td>
<td><p>快車</p></td>
<td></td>
<td><p>1919年4月15日</p></td>
<td><p><a href="../Page/IRT東公園道線.md" title="wikilink">IRT東公園道線</a>（）<br />
<a href="../Page/BMT第四大道線.md" title="wikilink">BMT第四大道線</a>（），<a href="../Page/法庭街車站_(BMT第四大道線).md" title="wikilink">法庭街</a></p></td>
</tr>
<tr class="odd">
<td><p>成為<a href="../Page/IRT東公園道線.md" title="wikilink">IRT東公園道線慢車軌道</a>（）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [nycsubway.org —
    IRT西城線](http://www.nycsubway.org/wiki/IRT_West_Side_Line)

[Category:紐約地鐵路線](../Category/紐約地鐵路線.md "wikilink")
[Category:1904年啟用的鐵路線](../Category/1904年啟用的鐵路線.md "wikilink")

1.   - [南碼頭總站計劃](../Page/南碼頭-白廳街車站.md "wikilink"), [Environmental
    Assessment and Section 4(f)
    Evaluation](http://www.mta.info/capconstr/sft/dea.htm),

2.  [大都會運輸署](../Page/大都會運輸署.md "wikilink"),

3.  , [第二大道地鐵線](../Page/紐約地鐵第二大道線.md "wikilink"), [Supplemental Draft
    Environmental Impact
    Statement](http://www.mta.info/capconstr/sas/sdeis.htm),

4.
5.
6.
7.
8.
9.
10.
11.
12.
13.

14.

15.

16.
17.
18.
19.
20.
21.
22.
23.
24.
25.
26.
27.
28.
29.
30.

31.
32.
33.
34.
35.
36.
37.
38.
39.
40.
41.
42.
43. [MTA Opens New South Ferry
    Station](http://web.mta.info/mta/news/releases/?en=090316-HQ8)
    Retrieved March 16, 2009