**ISDB**（）即**綜合數碼服務廣播**，是[日本自主制定的](../Page/日本.md "wikilink")[數碼電視和](../Page/數碼電視.md "wikilink")[數碼聲音廣播標準制式](../Page/數碼聲音廣播.md "wikilink")，由[電波產業會制定](../Page/電波產業會.md "wikilink")，用於該國的廣播電視網絡。ISDB取代了以前使用的[MUSE高清晰度的](../Page/Hi-Vision.md "wikilink")[模拟](../Page/類比電視.md "wikilink")[HDTV系統](../Page/HDTV.md "wikilink")。

ISDB-T国际版，又称SBTVD（，巴西数字电视系统）、ISDB-Tb，是ISDB的衍生制式，由[巴西政府制定](../Page/巴西.md "wikilink")，並在[南美洲廣泛採用](../Page/南美洲.md "wikilink")。

## 介紹

[Digital_broadcast_standards.svg](https://zh.wikipedia.org/wiki/File:Digital_broadcast_standards.svg "fig:Digital_broadcast_standards.svg")

ISDB有兩種接收方法：[電視機和](../Page/電視機.md "wikilink")[機頂盒](../Page/機頂盒.md "wikilink")。舊電視機可加裝機頂盒。機頂盒內除基本的[晶片組外](../Page/晶片組.md "wikilink")，需內建ISDB-T[模組](../Page/模組.md "wikilink")。ISDB設備廠商以日系為主，例如[日立及](../Page/日立.md "wikilink")[SHARP等](../Page/夏普.md "wikilink")。在日本，除了[Sky
PerfecTV\!採用DVB以外](../Page/Sky_PerfecTV!.md "wikilink")，都采用了ISDB。繼日本之後，部份中美洲國家及多數南美洲國家（僅[哥倫比亞及](../Page/哥倫比亞.md "wikilink")[法屬圭亞納採用](../Page/法屬圭亞納.md "wikilink")[DVB-T](../Page/DVB-T.md "wikilink")）也採用ISDB-T
International（又稱SBTVD，）系統。

ISDB的核心標準是ISDB-S（[衛星電視](../Page/衛星電視.md "wikilink")）、ISDB-T（[地面電視](../Page/地面電視.md "wikilink")）和ISDB-C（[有線電視](../Page/有線電視.md "wikilink")），並且都是基於MPEG-2或MPEG-4標準的多址接入傳輸，2.6
GHz頻段移動廣播數據架構及視頻和音頻編碼（MPEG-2或H.264），並且能夠進行高清晰度電視（HDTV）和標準清晰度電視。
ISDB-T和ISDB-TSB是移動電視接收的頻段。
1Seg和Tmm則是用於接收ISDB-T服務手機、筆記本電腦和汽車上的导航系统内建电视功能。

## 信号形式

ISDB-T中，根据[MPEG-2实施传送信号的复用](../Page/MPEG-2.md "wikilink")。每一个物理通道（6MHz）为一个基本TS（传送流），其中13个OFDM段可构成有统一参数选择的单一大块，也可以分为具有不同参数选择的几个块层，最多为4个块层。接收端接收时，可以对13段OFDM整体接收，也可以部分接收，即只接收13段OFDM里中央的一段OFDM。

## MPEG-2 TS上承载的协议

<table>
<thead>
<tr class="header">
<th><p>MPEG-2 TS协议层</p></th>
<th><p>ISDB-T</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>ES</p></td>
<td><p>MPEG-2 视频<br />
（ISO/IEC 13818-2）</p></td>
</tr>
<tr class="even">
<td><p>（加密）<br />
（<a href="../Page/ARIB.md" title="wikilink">ARIB</a> STD-B25）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>PES</p></td>
<td><p>MPEG-2 PES<br />
（ISO/IEC 13818-1、<a href="../Page/ARIB.md" title="wikilink">ARIB</a> STD-B32）</p></td>
</tr>
<tr class="even">
<td><p>TS</p></td>
<td><p>MPEG-2 TS<br />
（ISO/IEC 13818-1）</p></td>
</tr>
</tbody>
</table>

## 服務

  - 一個頻譜可包含一個高清频道或最多三個標清頻道，以及一个[1seg移动电视频道](../Page/1seg.md "wikilink")。
  - 提供声音频道
  - 通過[電話或](../Page/電話.md "wikilink")[寬頻提供互動服務](../Page/寬頻.md "wikilink")
  - [電子節目指南](../Page/電子節目指南.md "wikilink")
  - 通過[大氣電波為](../Page/大氣電波.md "wikilink")[機頂盒更新](../Page/機頂盒.md "wikilink")[韌體](../Page/韌體.md "wikilink")

## 採用的國家和地區

### 正式廣播

#### 亞洲

  -
  - （2011年10月19日通過正式採用ISDB-T）\[1\]\[2\]

#### 美洲

  - （2006年6月29日通過決定，採用ISDB-T國際，2007年12月2日開始廣播）

  - （2009年4月23日通過決定，正式採用ISDB-T國際，2010年3月30日開始廣播）

  - （2009年8月28日通過正式採用ISDB-T國際，2010年4月28日開始廣播）

  - （2009年9月14日通過正式採用ISDB-T國際，2010年9月14日開始廣播）

  - （2009年10月6日通過，決定正式採用ISDB-T國際，2013年2月20開始廣播）

  - （2010年3月26日通過，決定正式採用ISDB-T國際）

  - （在2010年5月25日通過，決定採用ISDB-T國際，2012年3月19開始廣播）\[3\]

  - （2010年6月1日通過採用ISDB-T國際，2011年8月15日開始播出）\[4\]

  - （2010年7月5日通過採用ISDB-T國際，2011年6月開始廣播）\[5\] \[6\]

  - （2010年8月10日通過正式採用ISDB-T國際）\[7\]

  - （2010年12月27日通過採用ISDB-T國際，2012年8月開始廣播）\[8\]\[9\]

  - （2013年5月30日通過採用ISDB-T國際，並開始前期實施階段）

  - （2013年9月26日通過採用ISDB-T國際，並開始前期實施階段）

#### 非洲

  - （正式採用ISDB-T國際，並開始前期實施階段）\[10\]\[11\]\[12\]

### 試驗廣播

#### 非洲

  - \[13\]

  -
#### 美洲

  - （目前正在評估數碼平台）

  -
#### 亞洲

  - （目前正在評估到數碼化平台的[DVB-T2](../Page/DVB-T2.md "wikilink")）

  - （採用ISDB-T現正進行試播，2013年11月5日重新評估）\[14\]\[15\]

## 參見

  - [數碼電視](../Page/數碼電視.md "wikilink")
  - [地面數碼電視](../Page/地面數碼電視.md "wikilink")
  - [數碼聲音廣播](../Page/數碼聲音廣播.md "wikilink")
  - [DMB](../Page/DMB.md "wikilink")
  - [1seg](../Page/1seg.md "wikilink")
  - [標清電視](../Page/標清電視.md "wikilink")
  - [高清電視](../Page/高清電視.md "wikilink")
  - [東京晴空塔](../Page/東京晴空塔.md "wikilink")

傳輸技術

  - [單頻網](../Page/單頻網.md "wikilink")（SFN）、[多頻網](../Page/多頻網.md "wikilink")（MFN）

## 注釋

<div class="references-small">

<references />

</div>

[Category:數碼電視](../Category/數碼電視.md "wikilink")
[Category:高清晰度电视](../Category/高清晰度电视.md "wikilink")

1.
2.  [Maldives introduces the Japanese ISDB-T system at
    MNBC](http://www.maldivesembassy.jp/cat_054/7707)
3.
4.  [1](http://www.presidencia.gov.py/v1/?p=25005)
5.
6.
7.
8.
9.  Gobierno se decidió por la norma japonesa
10.
11.
12.
13.
14. [Malaya Business Insight](http://www.malaya.com.ph/aug24/busi5.html)

15. [DTV Pilipinas: News5 InterAksyon: We're "turning Japanese" on
    digital TV
    platform](http://dtvpilipinas.blogspot.com/2011/08/news5-interaksyon-were-turning-japanese.html)