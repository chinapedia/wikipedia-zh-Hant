**ISM频段**（**I**ndustrial **S**cientific **M**edical
Band），中文意思分別是**工业的**(Industrial)、**科学的**(Scientific)和**医学的**(Medical)，因此顧名思義ISM频段就是各國挪出某一段頻段主要開放給[工业](../Page/工业.md "wikilink")，[科学和](../Page/科学.md "wikilink")[医学機構使用](../Page/医学.md "wikilink")。应用这些频段无需许可证或費用，只需要遵守一定的发射功率（一般低于1W），并且不要对其它频段造成[干扰即可](../Page/干扰.md "wikilink")。ISM频段在各国的规定并不统一。如在[美国有三个频段](../Page/美国.md "wikilink")902-928
MHz、2400-2484.5 MHz及5725-5850
MHz，而在[欧洲](../Page/欧洲.md "wikilink")900MHz的频段则有部份用于[GSM通信](../Page/GSM.md "wikilink")。而2.4G[Hz为各国共同的ISM频段](../Page/赫.md "wikilink")。因此[无线局域网](../Page/无线局域网.md "wikilink")（IEEE
802.11b/IEEE
802.11g）、[蓝牙](../Page/蓝牙.md "wikilink")、[ZigBee等无线网络](../Page/ZigBee.md "wikilink")，均可工作在2.4GHz频段上。

在國際電聯無線電規則[ITU-R第](../Page/ITU-R.md "wikilink")5條腳註5.138，5.150和5.280
中指定的ISM频段如下：

| 频率范围                                         | 中心频率       | 可用性                                         |
| -------------------------------------------- | ---------- | ------------------------------------------- |
| 6.765–6.795 [MHz](../Page/MHz.md "wikilink") | 6.780 MHz  |                                             |
| 13.553–13.567 MHz                            | 13.560 MHz |                                             |
| 26.957–27.283 MHz                            | 27.120 MHz |                                             |
| 40.66–40.70 MHz                              | 40.68 MHz  |                                             |
| 433.05–434.79 MHz                            | 433.92 MHz | 仅限[ITU](../Page/ITU.md "wikilink") Region 1 |
| 902–928 MHz                                  | 915 MHz    | 仅限[ITU](../Page/ITU.md "wikilink") Region 2 |
| 2.400–2.500 [GHz](../Page/GHz.md "wikilink") | 2.450 GHz  |                                             |
| 5.725–5.875 GHz                              | 5.800 GHz  |                                             |
| 24–24.25 GHz                                 | 24.125 GHz |                                             |
| 61–61.5 GHz                                  | 61.25 GHz  |                                             |
| 122–123 GHz                                  | 122.5 GHz  |                                             |
| 244–246 GHz                                  | 245 GHz    |                                             |

## 外部連結

  - [ITU page on definitions of ISM
    bands](http://www.itu.int/ITU-R/terrestrial/faq/index.html)
  - [ITU page on Radio
    Regulations](http://www.itu.int/publications/productslist.aspx?lang=e&CategoryID=R-REG&product=R-REG-RR)
  - [ITU
    region](http://en.wikipedia.org/wiki/International_Telecommunication_Union_region)


[Category:无线电频谱](../Category/无线电频谱.md "wikilink")