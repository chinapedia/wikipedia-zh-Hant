**ISO
15924**是[国际标准化组织为全世界的](../Page/国际标准化组织.md "wikilink")[文字定义的一套编码标准](../Page/文字.md "wikilink")，每种文字对应一个四位的字母编码和一个三位的数字编码，其中四位字母编码广泛用于[IETF语言标签中](../Page/IETF语言标签.md "wikilink")。

ISO 15924由[统一码联盟负责维护](../Page/统一码联盟.md "wikilink")，目前已有近两百种文字得到编码。

## 编码

### 數字碼段

| 码段          | 类别                                                                  |
| ----------- | ------------------------------------------------------------------- |
| `000`–`099` | [聖書體和](../Page/聖書體.md "wikilink")[楔形文字](../Page/楔形文字.md "wikilink") |
| `100`–`199` | 從右向左書寫的[音素文字](../Page/字母系統.md "wikilink")                           |
| `200`–`299` | 從左向右書寫的音素文字                                                         |
| `300`–`399` | [音素音節文字](../Page/音素音節文字.md "wikilink")                              |
| `400`–`499` | [音節文字](../Page/音節文字.md "wikilink")                                  |
| `500`–`599` | [表意文字](../Page/表意文字.md "wikilink")                                  |
| `600`–`699` | [未解讀文字](../Page/未解讀文字.md "wikilink")                                |
| `700`–`799` | [速记和其他符号](../Page/速记.md "wikilink")                                 |
| `800`–`899` | 未分配                                                                 |
| `900`–`999` | 私人使用、别名、特殊代碼                                                        |

### 常見编码

| **文种**                               | **字母編碼** | **数字編碼** |
| ------------------------------------ | -------- | -------- |
| [腓尼基字母](../Page/腓尼基字母.md "wikilink") | `Phnx`   | `115`    |
| [阿拉伯字母](../Page/阿拉伯字母.md "wikilink") | `Arab`   | `160`    |
| [希臘字母](../Page/希臘字母.md "wikilink")   | `Grek`   | `200`    |
| [拉丁字母](../Page/拉丁字母.md "wikilink")   | `Latn`   | `215`    |
| [西里爾字母](../Page/西里爾字母.md "wikilink") | `Cyrl`   | `220`    |
| [朝鲜语字母](../Page/朝鲜语字母.md "wikilink") | `Hang`   | `286`    |
| [婆羅米文](../Page/婆羅米文.md "wikilink")   | `Brah`   | `300`    |
| [藏文](../Page/藏文.md "wikilink")       | `Tibt`   | `330`    |
| [平假名](../Page/平假名.md "wikilink")     | `Hira`   | `410`    |
| [片假名](../Page/片假名.md "wikilink")     | `Kana`   | `411`    |
| [漢字](../Page/漢字.md "wikilink")       | `Hani`   | `500`    |
| [簡體字](../Page/簡體字.md "wikilink")     | `Hans`   | `501`    |
| [正體字](../Page/正體字.md "wikilink")     | `Hant`   | `502`    |

### 特殊编码

| **类型**                               | **字母編碼**      | **數字編碼**    |
| ------------------------------------ | ------------- | ----------- |
| 私人使用                                 | `Qaaa`–`Qabx` | `900`–`949` |
| [Emoji](../Page/Emoji.md "wikilink") | `Zsye`        | `993`       |
| [数学公式](../Page/数学公式.md "wikilink")   | `Zmth`        | `995`       |
| [符号](../Page/符号.md "wikilink")       | `Zsym`        | `996`       |
| 非文字                                  | `Zxxx`        | `997`       |
| 不能確定                                 | `Zyyy`        | `998`       |
| 未編碼文字                                | `Zzzz`        | `999`       |

## 參見

  - [ISO 15924 列表](../Page/ISO_15924_列表.md "wikilink")
  - [ISO 639](../Page/ISO_639.md "wikilink")
  - [ISO 3166](../Page/ISO_3166.md "wikilink")
  - [IETF语言标签](../Page/IETF语言标签.md "wikilink")

## 外部連結

  - [ISO 15924](https://www.iso.org/standard/29546.html)
  - [ISO 15924维护机构](http://www.unicode.org/iso15924/)
  - [ISO 15924編碼列表](http://www.unicode.org/iso15924/codelists.html)

[\#15924](../Category/ISO標準.md "wikilink")
[\*](../Category/有_ISO_15924_四字母編碼的文字.md "wikilink")