**ISO 3166-2:CN**是用來定義[中國](../Page/中國.md "wikilink")
[地理區碼的](../Page/地理.md "wikilink")[ISO標準](../Page/ISO.md "wikilink")。它是[ISO
3166-2子集](../Page/ISO_3166-2.md "wikilink")。編碼範圍為由[中華人民共和國實際統治的](../Page/中華人民共和國.md "wikilink")[中國大陸](../Page/中國大陸.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門以及](../Page/澳門.md "wikilink")[中華人民共和國宣称并未實際統治的](../Page/中華人民共和國.md "wikilink")[台灣](../Page/台灣.md "wikilink")。

[ISO
3166-1目錄中](../Page/ISO_3166-1.md "wikilink")，[中国](../Page/中国.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳门](../Page/澳门.md "wikilink")、[臺灣](../Page/臺灣.md "wikilink")（以[中国台湾省名義](../Page/中国台湾省.md "wikilink")）予以分别列出，而只把中国列为主权独立国家。\[1\]。而在ISO
3166-2:CN中，则将香港特别行政区（编码CN-HK）、澳门特别行政区（编码CN-MO）、台湾省（编码CN-TW）等与中华人民共和国的其他省、直辖市、自治区并列，從而使香港、澳门、台灣同時出現於[ISO
3166-1及](../Page/ISO_3166-1.md "wikilink")[ISO
3166-2中出現](../Page/ISO_3166-2.md "wikilink")\[2\]。

本編碼之編定依據為[中華人民共和國國家標準GB](../Page/中華人民共和國國家標準.md "wikilink")/T 2260-2007。

## 代碼

<table>
<thead>
<tr class="header">
<th><p>代碼</p></th>
<th><p>ISO名稱[3]</p></th>
<th><p>行政區名稱</p></th>
<th><p>類型</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>Anhui Sheng</p></td>
<td><p><a href="../Page/安徽省.md" title="wikilink">安徽省</a></p></td>
<td><p><a href="../Page/中國一級行政區.md" title="wikilink">省</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Beijing Shi</p></td>
<td><p><a href="../Page/北京市.md" title="wikilink">北京市</a></p></td>
<td><p><a href="../Page/直轄市.md" title="wikilink">直轄市</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Chongqing Shi</p></td>
<td><p><a href="../Page/重庆市.md" title="wikilink">重庆市</a></p></td>
<td><p>直轄市</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Fujian Sheng</p></td>
<td><p><a href="../Page/福建省.md" title="wikilink">福建省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Guangdong Sheng</p></td>
<td><p><a href="../Page/广东省.md" title="wikilink">广东省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Gansu Sheng</p></td>
<td><p><a href="../Page/甘肃省.md" title="wikilink">甘肃省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Guangxi Zhuangzu Zizhiqu</p></td>
<td><p><a href="../Page/广西壮族自治区.md" title="wikilink">广西壮族自治区</a></p></td>
<td><p><a href="../Page/自治區.md" title="wikilink">自治區</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Guizhou Sheng</p></td>
<td><p><a href="../Page/贵州省.md" title="wikilink">贵州省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Henan Sheng</p></td>
<td><p><a href="../Page/河南省.md" title="wikilink">河南省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Hubei Sheng</p></td>
<td><p><a href="../Page/湖北省.md" title="wikilink">湖北省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Hebei Sheng</p></td>
<td><p><a href="../Page/河北省.md" title="wikilink">河北省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Hainan Sheng</p></td>
<td><p><a href="../Page/海南省.md" title="wikilink">海南省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Hong Kong SAR<br />
Xianggang Tebiexingzhengqu</p></td>
<td><p><a href="../Page/香港特别行政区.md" title="wikilink">香港特别行政区</a></p></td>
<td><p><a href="../Page/特別行政區.md" title="wikilink">特別行政區</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Heilongjiang Sheng</p></td>
<td><p><a href="../Page/黑龙江省.md" title="wikilink">黑龙江省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Hunan Sheng</p></td>
<td><p><a href="../Page/湖南省.md" title="wikilink">湖南省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Jilin Sheng</p></td>
<td><p><a href="../Page/吉林省.md" title="wikilink">吉林省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Jiangsu Sheng</p></td>
<td><p><a href="../Page/江苏省.md" title="wikilink">江苏省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Jiangxi Sheng</p></td>
<td><p><a href="../Page/江西省.md" title="wikilink">江西省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Liaoning Sheng</p></td>
<td><p><a href="../Page/辽宁省.md" title="wikilink">辽宁省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Macao SAR<br />
Macau SAR<br />
Aomen Tebiexingzhengqu</p></td>
<td><p><a href="../Page/澳门特别行政区.md" title="wikilink">澳门特别行政区</a></p></td>
<td><p>特別行政區</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Nei Mongol Zizhiqu</p></td>
<td><p><a href="../Page/内蒙古自治区.md" title="wikilink">内蒙古自治区</a></p></td>
<td><p>自治區</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Ningxia Huizu Zizhiqu</p></td>
<td><p><a href="../Page/宁夏回族自治区.md" title="wikilink">宁夏回族自治区</a></p></td>
<td><p>自治區</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Qinghai Sheng</p></td>
<td><p><a href="../Page/青海省.md" title="wikilink">青海省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Sichuan Sheng</p></td>
<td><p><a href="../Page/四川省.md" title="wikilink">四川省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Shandong Sheng</p></td>
<td><p><a href="../Page/山东省.md" title="wikilink">山东省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Shanghai Shi</p></td>
<td><p><a href="../Page/上海市.md" title="wikilink">上海市</a></p></td>
<td><p>直轄市</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Shaanxi Sheng</p></td>
<td><p><a href="../Page/陕西省.md" title="wikilink">陕西省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Shanxi Sheng</p></td>
<td><p><a href="../Page/山西省.md" title="wikilink">山西省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Tianjin Shi</p></td>
<td><p><a href="../Page/天津市.md" title="wikilink">天津市</a></p></td>
<td><p>直轄市</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Taiwan Sheng</p></td>
<td><p><a href="../Page/台湾省_(中华人民共和国).md" title="wikilink">台湾省</a>[4]</p></td>
<td><p>省</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Xinjiang Uygur Zizhiqu</p></td>
<td><p><a href="../Page/新疆维吾尔自治区.md" title="wikilink">新疆维吾尔自治区</a></p></td>
<td><p>自治區</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Xizang Zizhiqu</p></td>
<td><p><a href="../Page/西藏自治区.md" title="wikilink">西藏自治区</a></p></td>
<td><p>自治區</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Yunnan Sheng</p></td>
<td><p><a href="../Page/云南省.md" title="wikilink">云南省</a></p></td>
<td><p>省</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Zhejiang Sheng</p></td>
<td><p><a href="../Page/浙江省.md" title="wikilink">浙江省</a></p></td>
<td><p>省</p></td>
</tr>
</tbody>
</table>

<references group="註" />

## ISO 3166-1中港澳台的代碼

臺灣、香港與澳門除了被列入本編碼外，也在[ISO
3166-1中列有其自己的編碼](../Page/ISO_3166-1.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>代碼</p></th>
<th><p>行政實體</p></th>
<th><p>ISO 3166-2:CN行政區名稱</p></th>
<th><p>ISO 3166-1名稱</p></th>
<th><p>ISO 3166-2代碼</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td><p>香港特別行政區</p></td>
<td><p>香港</p></td>
<td><p><a href="../Page/ISO_3166-2:HK.md" title="wikilink">{{mono</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>澳門特別行政區</p></td>
<td><p>澳門</p></td>
<td><p><a href="../Page/ISO_3166-2:MO.md" title="wikilink">{{mono</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/臺灣地區.md" title="wikilink">臺灣地區</a></p></td>
<td><p>台灣省</p></td>
<td><p><a href="../Page/中國臺灣省.md" title="wikilink">中國臺灣省</a></p></td>
<td><p><a href="../Page/ISO_3166-2:TW.md" title="wikilink">{{mono</a></p></td>
</tr>
</tbody>
</table>

在ISO 3166-1中，台灣被稱為[中國臺灣省](../Page/中國臺灣省.md "wikilink")（）。

## 參考來源

## 變更歷史

<table>
<thead>
<tr class="header">
<th><p>來源</p></th>
<th><p>日期</p></th>
<th><p>描述</p></th>
<th><p>代碼/行政區變更</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="http://www.iso.org/iso/iso_3166-2_newsletter_i-2_en.pdf"></a></p></td>
<td></td>
<td><p>新增一個行政區，並列出其漢語拼音名稱。更新列表來源。</p></td>
<td><p><strong>行政區變更</strong><br />
 <a href="../Page/澳門.md" title="wikilink">澳門</a></p></td>
</tr>
<tr class="even">
<td><p><a href="http://www.iso.org/iso/iso_3166-2_newsletter_i-6_en.pdf"></a></p></td>
<td></td>
<td><p>刪除CN-15的其中一個名稱</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://www.iso.org/obp/ui/#iso:code:3166:CN">OBP</a></p></td>
<td></td>
<td><p>變更行政區代碼<br />
變更各行政區代碼的名稱<br />
為CN-HK、CN-MO兩代碼添加英語名稱<br />
新增CN-MO的葡萄牙語名稱<br />
更新代碼來源<br />
更新列表來源</p></td>
<td><p><strong>變更行政區代碼：</strong><br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
 → <br />
</p></td>
</tr>
</tbody>
</table>

## 參見

  - [ISO 3166-2](../Page/ISO_3166-2.md "wikilink")，所有國家的地區代碼
  - [ISO
    3166-1](../Page/ISO_3166-1.md "wikilink")，所有國家的[互聯網代碼](../Page/互聯網.md "wikilink")
  - [中国行政区划](../Page/中国行政区划.md "wikilink")
  - [中華民國行政區劃](../Page/中華民國行政區劃.md "wikilink")
  - [中华人民共和国行政区划](../Page/中华人民共和国行政区划.md "wikilink")

[2:CN](../Category/ISO_3166.md "wikilink")
[\*](../Category/中华人民共和国行政区划制度.md "wikilink")

1.
2.
3.  除非另有標示，皆以[漢語拼音列出](../Page/漢語拼音.md "wikilink")。
4.  由中華民國實際管轄；參見[ISO 3166-2:TW](../Page/ISO_3166-2:TW.md "wikilink")。