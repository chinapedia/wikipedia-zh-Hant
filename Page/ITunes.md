**iTunes**是一款[媒體播放器的應用程式](../Page/媒體播放器.md "wikilink")，2001年1月10日由[蘋果電腦在](../Page/蘋果電腦.md "wikilink")[舊金山的](../Page/舊金山.md "wikilink")推出，用來播放以及管理數位音樂與視訊檔案，是最受歡迎的管理[iPod与](../Page/iPod.md "wikilink")[iOS设备檔案的主要工具之一](../Page/iOS.md "wikilink")。此外，iTunes能連線到[iTunes
Store](../Page/iTunes_Store.md "wikilink")（在有[網路連線且蘋果公司在當地有開放該服務的情況下](../Page/互聯網.md "wikilink")），以便下載購買的[數位音樂](../Page/數位音訊.md "wikilink")、[音樂影片](../Page/音樂影片.md "wikilink")、[電視節目](../Page/電視節目.md "wikilink")、[iOS应用](../Page/iOS.md "wikilink")（在iOS
11推出后去除）、各種[Podcast以及](../Page/Podcast.md "wikilink")[標準長片](../Page/電影.md "wikilink")。2015年起电脑端的[Apple
Music服务也被整合进了iTunes中](../Page/Apple_Music.md "wikilink")。

此程式原來由和開發，當初稱作，1999年由發表。2000年被蘋果電腦買下，更換新的使用者介面，增加[燒錄CD的功能](../Page/燒錄.md "wikilink")，並且移除[面板支援](../Page/面板.md "wikilink")，改名為iTunes。起初只能在[Mac
OS 9](../Page/Mac_OS_9.md "wikilink")
[作業系統上運行](../Page/作業系統.md "wikilink")，九個月後，第二版支援Mac
OS X系統，第三版停止支援Mac OS 9。2003年10月，iTunes 4.1版本發佈，增加了對[Windows
2000和](../Page/Windows_2000.md "wikilink")[XP的支援](../Page/Windows_XP.md "wikilink")。2007年，[微軟發佈最新的](../Page/微軟.md "wikilink")[作業系統](../Page/作業系統.md "wikilink")[Windows
Vista](../Page/Windows_Vista.md "wikilink")，與iTunes有數個已知的相容性問題，蘋果已發行新版本以修正問題。

iTunes可從蘋果的網站免費下載取得，[麥金塔電腦和一些iPod產品也有附帶此軟體](../Page/麥金塔.md "wikilink")。它也曾是蘋果[iLife多媒體應用程式套件的一部分](../Page/iLife.md "wikilink")。

## 歷史

iTunes是從[SoundJam
MP](../Page/SoundJam_MP.md "wikilink")——由Mac軟件公司[Casady &
Greene發行的一個流行的商業MP](../Page/Casady_&_Greene.md "wikilink")3軟件——發展而來的。蘋果公司買下了SoundJam
MP的版權並且僱傭了創建了SoundJam的三位程序員[比尔·金凯德](../Page/比尔·金凯德.md "wikilink")、[杰夫·罗宾和](../Page/杰夫·罗宾.md "wikilink")[戴夫·海勒](../Page/戴夫·海勒.md "wikilink")。iTunes的第一個發行版與SoundJam
MP十分相似，增加了CD燒錄功能與一個更好的用戶界面。蘋果已經在iTunes的後續版本中增加了許多重要的功能。

蘋果也已開發在行動電話上（例如[Motorola
ROKR](../Page/Motorola_ROKR.md "wikilink")、[Motorola
RAZR與](../Page/Motorola_RAZR.md "wikilink")[Motorola
SLVR](../Page/Motorola_SLVR_L7.md "wikilink")）執行的iTunes商標軟體。

## 功能

使用者能夠將他們的音樂組成[播放清單](../Page/播放清單.md "wikilink")、編輯檔案資訊、燒錄CD、透過它內建的Music
Store購買音樂、下載Podcast、備份歌曲至電腦中、執行視覺化、及時配合音樂表現生動的效果和編碼音樂成為許多不同的音訊格式。

### 媒體管理

iTunes在兩個檔案中存儲關於音頻檔案的[元數據](../Page/元數據.md "wikilink")。

第一個是名為*iTunes Library*的二進位檔案（先前版本中名叫''iTunes *x* Music
Library''），它使用了自己專有的曲庫格式。它不僅緩衝類似藝術家和類型這樣的儲存在音頻檔案的標記空間（例如[ID3標記](../Page/ID3.md "wikilink")）中的信息，也存一些像「播放計數」和「採樣率」的iTunes特殊信息。

第二個檔案名為*iTunes Music
Library.xml*，它在iTunes中的信息改變時被刷新。它是[XML格式的](../Page/XML.md "wikilink")，允許開發者容易的開發像[Apple的](../Page/蘋果電腦.md "wikilink")[iDVD](../Page/iDVD.md "wikilink")、[iMovie和](../Page/iMovie.md "wikilink")[iPhoto或Freshly](../Page/iPhoto.md "wikilink")
Squeezed Software's Rock Star.一樣的可以存取它的信息的軟件。

這一系統防止廠商佔有對一條音軌的元數據的任何改變，除了「播放計數」、「最後播放」和「採樣率」這些被存儲在專有檔案中的元數據，其它所有信息通過XML檔案而變得有效。

#### 資料庫檢視

iTunes用戶可選擇以不同方式瀏覽iTunes影音資料庫，例如目錄、目錄附有相應的封面圖片、網格瀏覽模式或Coverflow。

#### 資料庫共享

iTunes音樂資料庫中的歌曲可以使用[Bonjour](../Page/Bonjour.md "wikilink")（之前稱作Rendezvous）而在區域網路中分享－這是Apple的[Zeroconf](../Page/Zeroconf.md "wikilink")（無需設定，Zero
Configuration
Required）開放式網路標準實做。這個技術可以讓同一個子網段中的歌曲可以自動的被分享而且偵測。當一首歌曲被分享的時候，iTunes會將這首歌曲串流到另外一台電腦上，但是為了防止被拷貝而不能被儲存。那些使用[版權保護AAC的歌曲也可以被存取](../Page/AAC.md "wikilink")，但是需要由iTunes
Music Store取得授權。每24個小時之中最大容許五個使用者連線。

曾經在iTunes
4.0中，使用者可以透過網際網路自由的存取被分享的音樂，除了透過同一子網路外，也可以透過指定那些遠端分享音樂資料庫的[IP地址](../Page/IP地址.md "wikilink")。蘋果電腦很快地在4.0.1版中移除了這個功能，它們聲明這違反了[終端使用者許可協定](../Page/終端使用者許可協定.md "wikilink")（[EULA](../Page/EULA.md "wikilink")）。

音樂分享使用了[數位音樂存取協定](../Page/數位音樂存取協定.md "wikilink")（DAAP, Digital Audio
Access Protocol），這是由蘋果電腦所創立的協定專門用於分享音樂的用途。\[1\]
DAAP已經由[逆向工程而解開了](../Page/逆向工程.md "wikilink")，而目前則常被用於非蘋果公司出的軟體來分享播放清單。

#### 檔案格式支援

iTunes
9目前可以讀取、寫入以及轉換於[MP3](../Page/MP3.md "wikilink")、[AIFF](../Page/AIFF.md "wikilink")、[WAV](../Page/WAV.md "wikilink")、[MPEG-4](../Page/MPEG-4.md "wikilink")、[AAC與](../Page/AAC.md "wikilink")[Apple
Lossless之間](../Page/Apple_Lossless.md "wikilink")。

iTunes也能播放任何[QuickTime能播放的](../Page/QuickTime.md "wikilink")（甚至是一些視訊格式），包括來自iTunes
Store與[Audible.com有聲書的](../Page/Audible.com.md "wikilink")[受保護的AAC檔案](../Page/FairPlay.md "wikilink")。為了播放其他格式，例如內含[Ogg的](../Page/Ogg.md "wikilink")[Vorbis或](../Page/Vorbis.md "wikilink")[Speex編碼](../Page/Speex.md "wikilink")，iTunes需要安裝[Xiph
QuickTime元件](../Page/Xiph_QuickTime元件.md "wikilink")。

曾經有一些對蘋果公司MP3編碼器對於以[可變位元率編碼的品質評論](../Page/可變位元率.md "wikilink")。在2004年1月，雙重匿名的六種MP3以128
kbit/s編碼的編碼器，由Roberto Amorim帶領公開收聽測試，iTunes MP3
VBR編碼器排名最後。該帶領人稍後以iTunes如何被測試有嚴重問題的經過證實。\[2\]

iTunes的Windows版本能自動地轉換未受保護的[WMA](../Page/Windows_Media_Audio.md "wikilink")（包括版本9）檔案至其他的音訊格式，但它不支援直接播放或編譯過的WMA格式。

#### 聲音處理

iTunes使用電腦的[音效卡播放音樂](../Page/音效卡.md "wikilink")。iTunes也包含了音頻處理的功能，叫做[等化器](../Page/等化器.md "wikilink")（[equalizing](../Page/equalization.md "wikilink")），和「音頻增強」（sound
enhancer），有些語言也翻譯成「音頻改進」（sound improver）。

#### 視訊支援

於2005年5月9日，iTunes
4.8及iTunes視訊支援一同推出。用戶可直接從電腦把影像片段拉入iTunes資料庫中加以分類及管理。影像可分別在iTunes主視窗中的小框，分開的視窗或全螢幕瀏覽。在iTunes中，視訊支援的功能不太完善：雖然在資料庫中的影像是由電視或顯示器的小圖示與音樂分開，但仍然與普通音樂檔放在一起及由相同的音樂類別（如"專輯"及"作曲者"）所分類。iTunes也與一些主流的格式不兼容，包括[AVI及](../Page/AVI.md "wikilink")[WMV](../Page/WMV.md "wikilink")。

在2005年10月12日，蘋果公司推出能於iTunes音樂商城購買及收看視訊內容的iTunes
6.0.當時iTunes音樂商城推出了過千支精選的音樂影像及5套電視節目，包括ABC's的*[LOST檔](../Page/LOST檔.md "wikilink")*，在節目播放後24小時即時可在商城購買，還包括了一些上一季度的精選節目；當時的精選節目同時也包括了NBC
Universal, USA Network, Sci-Fi Channel shows及Viacom，期後也加入了一些Disney-owned
networks'的節目。商城同時也公開了一些蘋果公司的精選電影預告片。在商城購買的視訊格式是128 kbit/s Protected
[MPEG-4](../Page/MPEG-4.md "wikilink")
video（[H.264](../Page/H.264/MPEG-4_AVC.md "wikilink")）1.

到2006年1月26日為止，iTMS提供超過40個電視節目下載，包括最新添加的[Nickelodeon以及](../Page/Nickelodeon_\(電視頻道\).md "wikilink")[MTV的多個系列如](../Page/MTV.md "wikilink")*[SpongeBob
SquarePants](../Page/SpongeBob_SquarePants.md "wikilink")*以及[明星大整蛊](../Page/明星大整蛊.md "wikilink")。目前許多的視訊和Video
Podcast需要Quicktime（7）的最新版本，而與MacOS的舊版本不相容（僅v10.3.9與其以後版本才被支援）。

到2006年7月4日為止，iTunes音樂商店提供超過了一百五十種電視節目供人下載；其中包括最近新加入的[Discovery
Channel](../Page/Discovery_Channel.md "wikilink")，[MTV](../Page/MTV.md "wikilink")
and [FOX的節目等](../Page/Fox_Broadcasting_Company.md "wikilink")。

音樂商城的影片內容為受保護的，含有大約128 kbit/s AAC音軌的540
kbit/s編碼[MPEG-4視訊](../Page/MPEG-4.md "wikilink")（[H.264](../Page/H.264.md "wikilink")）。
許多影片及視訊Podcast需要最新版的Quicktime，Quicktime
7在較古早的MacOS上可能不相容（只有v10.3.9及後續版本可以支援）。

#### 播放清單

第三代iTunes引進了「智能播放列表」，以取代靜態播放列表，智能播放列表可被設定為基於一個自訂的列表選擇標準自動更新（線上更新），（像一個[資料庫查詢](../Page/資料庫.md "wikilink")）。用戶可設定多個選擇標準控制播放列表。

所有使用者都可iTunes Store發佈播放清單，這是所謂的iMix。

播放列表可以[隨機或按順序播放](../Page/隨機.md "wikilink")。「隨機性」亂序算法可以從一張[專輯或一位](../Page/專輯.md "wikilink")[藝術家的不同的音軌中選出不同的音軌進行播放](../Page/藝術家.md "wikilink")（iTunes
5.0版本中的一個新特性）。派對隨機播放也可以從評定等級較高的曲目中選取音軌進行播放。

派對隨機播放列表被用作一個簡單的DJ助手。預設情況下，它隨機的從曲庫或其它播放列表裡選取音樂；用戶可以通過刪除其中的音樂來凌駕它的自動選取的結果（iTunes將選取新的樂曲來替代它們），也可以通過[即拖即放或右鍵選單添加自己的音樂來達到同樣的效果](../Page/即拖即放.md "wikilink")。這樣就可以使一個播放列表中同時包含使用者預選的和隨機選取的音樂。派對隨機播放列表可以在運行中擷取；這使所有隨機選取的音樂在播放列表播放時被替換。

#### iTunes DJ

此為8.1派對隨選歌曲的更新

#### Genius播放列表

iTunes
8.0引進了Genius播放列表。Genius播放列表能從使用者的音樂資料庫自動產生包含一個25，50，75，或100首和已選取的歌曲類似的播放列表。Genius播放列表的結果可重新整理或儲存。Genius欄會根據已選取的歌曲從iTunes
Store推薦歌曲。為使Genius播放列表變得完整，使用者需要一個iTunes
Store帳號，一些訊息和使用者的音樂資料庫都會被傳送到蘋果資料庫。Genius會因編製用戶的音樂資料庫已變得「聰明」，Genius產生的結果也會隨時間變得更加準確。
[1](https://web.archive.org/web/20090217094054/http://apple.com.cn/itunes/features/#genius)

### iTunes -{zh-hans:商店; zh-hant:Store;}-

[iTunes-aacp.png](https://zh.wikipedia.org/wiki/File:iTunes-aacp.png "fig:iTunes-aacp.png")
自iTunes 4開始，蘋果為iTunes引入了[iTunes
Store音樂商城](../Page/iTunes_Store.md "wikilink")，iTunes使用者可以從商店中購買並下載歌曲，這些歌曲可以被使用在有限個數的電腦以及無限台iPod。從iTunes
Store中購買的歌曲會使用蘋果的[FairPlay數位版權管理](../Page/FairPlay.md "wikilink")（[DRM](../Page/數字版權管理.md "wikilink")）系統防止用戶自行拷貝。從2003年四月28號該商店開張以來至2006年2月22號為止，已經有超過1億首歌被下載了。\[3\]

有些使用者開始抱怨iTunes Music Store和iTunes的緊密整合導致了購買的音樂沒有辦法被使用[Mac OS
X或是](../Page/Mac_OS_X.md "wikilink")[Microsoft
Windows以外作業系統](../Page/Microsoft_Windows.md "wikilink")（像是[Linux](../Page/Linux.md "wikilink")，以及其他所有不被iTunes支援的作業系統）的使用者播放。這些抱怨導致了許多其他的播放程式開始被開發，使用了一些小技巧或是被公開的破解方式，讓iTunes
Music
Store的顧客可以使用自己所想用的播放軟體或是作業系統。在這些軟體中，最引人注目的是[PyMusique](../Page/PyMusique.md "wikilink")，蘋果電腦公司曾多次想要封鎖這套軟體但都失敗了。

其他的抱怨來自於他們的音樂只能夠在iTunes或是iPod中輕鬆的播放（雖然事實上，這些歌曲是可以被合法的燒錄成光碟的）。這些抱怨啟發了如[Hymn這類軟體的開發](../Page/Hymn_\(software\).md "wikilink")，這些軟體可以將已經購買的音樂解密（只支援iTunes
6.0以前的版本），讓他們可以在任何播放器上播放甚至是分享出去。

其他一些比較小的抱怨像是缺乏回覆的功能，像是使用者可以重新下載那些不小心丟失的音樂。iTunes的許多競爭者就提供了這樣的服務，但是iTunes只允許重新授權（或者恢復已購買音樂的授權）。

美國的iPod主人們當第一次連接他們的iPod到電腦上時，他們會被帶到一個特殊一次性的iTunes Music
Store頁面。這個頁面目前提供了一個由Lava和[Atlantic
Records所免費提供的試聽專輯](../Page/Atlantic_Records.md "wikilink")，您可以下載整張專輯或是只下載單首歌。一個[Universal
Records提供的試聽專輯曾經可以在這個頁面中找到](../Page/Universal_Records.md "wikilink")，而現在可能依然可以透過特殊的網址而取得。

目前iTunes Music
Store在以下國家和地区提供服務：香港、美国、英国、法国、德国、奥地利、比利时、芬兰、希腊、意大利、卢森堡、荷兰、葡萄牙、西班牙、加拿大、爱尔兰、瑞典、挪威、瑞士、丹麦、日本、澳大利亚、新西兰、墨西哥、保加利亚、塞浦路斯、捷克、爱沙尼亚、匈牙利、拉托维亚、立陶宛、马耳他、波兰、罗马尼亚、斯洛文尼亚、斯洛伐克、阿根廷、巴西、玻利维亚、智利、哥伦比亚、哥斯达黎加、多米尼加、厄瓜多尔、萨尔瓦多、危地马拉、洪都拉斯、尼加拉瓜、巴拿马、巴拉圭、秘鲁、委内瑞拉、文莱、柬埔寨、老挝、澳门、马来西亚、菲律宾、新加坡、斯里兰卡、台湾、泰国和越南。

MiniStore在iTunes
6.0.2版本中加入。MiniStore是在主視窗的底部增加了一個可以顯示／隱藏的小視窗。當用戶在曲庫中選擇了一個條目，條目的相關信息將被送往iTunes商店，MiniStore將顯示相關的歌曲或者影片。起初，由於擔心這個特性會被用做[間諜軟件](../Page/間諜軟件.md "wikilink")，人們對MiniStore存有爭議。\[4\]蘋果聲明MiniStore並沒有從用戶處獲得任何信息並在之後將其作為可選項。\[5\]

#### iTunes U

2007年5月30日，蘋果在iTunes Store上發布[iTunes
U](../Page/iTunes_U.md "wikilink")。iTunes
U用iTunesU這種格式把大學課程發表在Store上供用戶下載\[6\]。iTunes
U涵蓋美國、英國、澳大利亞、加拿大、愛爾蘭和新西蘭的一些大學課程。

### Podcast

2005年6月28日發佈的4.9版本的iTunes增加了內建[Podcasting](../Page/Podcasting.md "wikilink")（中文譯名「播客」）支持。用戶可以在iTunes
Music Store或者透過登入feed
URL訂閱podcasts。訂閱了之後，podcast將被自動的下載。用戶可以選擇每週、每日、每小時或手動更新podcast。蘋果維護了四個「官方的」podcast：Podfinder（[Adam
Curry](../Page/Adam_Curry.md "wikilink")）、Street Official Real
Talk（採訪[hip-hop藝術家](../Page/hip-hop.md "wikilink")）、iTunes New
Music Tuesday和Apple Quarterly Earning
Call。目錄的首頁同樣顯示來自商業廣播電台和獨立播放的引人注目的Podcast。要獲得關於Podcasting的資料，請看[外部連結](../Page/#外部連結.md "wikilink")。

#### Video Podcast

iTunes
6介紹了對[視訊播客的支持](../Page/視訊播客.md "wikilink")，儘管video和[RSS支持已經非正式的存在於iTunes](../Page/RSS.md "wikilink")
4.9了。用戶可以通過iTunes Music Store和feed URL訂閱RSS feed。Video
podcast不但可以包含可下載的視訊檔（MOV、MP4、M4V、MPG），而且可以包括串流甚至IPTV。可下載的檔可以傳輸到新型的iPod上，而且可下載的檔案和串流可以在蘋果新的[Front
Row媒體中心程序中播放](../Page/Front_Row.md "wikilink")。

### 同步iPod與其它播放器

每當一個iPod與它相連時，iTunes會自動的[同步它的音樂與影片庫](../Page/同步.md "wikilink")（OS
X版本的iTunes可以同步幾種不同的[數字音頻播放器](../Page/數字音頻播放器.md "wikilink")[2](http://docs.info.apple.com/article.html?artnum=93548)；Windows版本的iTunes只支持iPod[3](http://docs.info.apple.com/article.html?artnum=93377)）。新的曲目與播放列表被自動的複製到iPod上，電腦曲庫裡刪除了的曲目也會在iPod中被刪除。iPod中對曲目的評價將被反饋到iTunes曲庫裡，[有聲書將記住最後一次播放的位置](../Page/有聲書.md "wikilink")。

自動同步可以關閉，以便手動複製單獨的曲目或完整的播放列表；然而，iTunes只可以把音樂複製到iPod上卻不可以反向進行，這啟發了第三方軟件支持把音樂從iPod中複製出來。通過一些軟件，在[Unix命令行也可以做這一工作](../Page/Unix.md "wikilink")。

當與電腦相連的iPod沒有足夠的剩餘空間來同步iTunes中的曲庫時，將會創立一個與其相連的iPod同名的播放列表。這個播放列表可以被用戶更改以充當可用的空間。

### 與其他應用程式整合

iTunes與[蘋果電腦](../Page/蘋果電腦.md "wikilink")[Macintosh中的](../Page/Macintosh.md "wikilink")[Mac
OS
X操作系统下的數位生活套裝軟體](../Page/Mac_OS_X.md "wikilink")[iLife和其專業文案製作套裝軟體](../Page/iLife.md "wikilink")[iWork能夠緊密配合無間](../Page/iWork.md "wikilink")。這些蘋果電腦軟體套裝下的各個成員都可以直接的讀取iTunes數據庫裡的[播放清單和](../Page/播放清單.md "wikilink")[MP3文檔](../Page/MP3.md "wikilink")。iTunes下的音樂檔可以直接地植入[Pages的檔案裡或用於提供配樂給](../Page/Pages.md "wikilink")[iWeb](../Page/iWeb.md "wikilink")、[iDVD](../Page/iDVD.md "wikilink")、[iMovie和](../Page/iMovie.md "wikilink")[Keynote下的製作](../Page/Keynote.md "wikilink")。[GarageBand所制作和輸出的音檔會自動地寫入iTunes的數據庫裡](../Page/GarageBand.md "wikilink")。同時[Front
Row也是從iTunes和](../Page/Front_Row.md "wikilink")[iPhoto中讀取數據](../Page/iPhoto.md "wikilink")，所以iTunes因此也能與FrontRow配合無間。

[iTunes
Artwork.saver是一個包括在](../Page/iTunes_Artwork.saver.md "wikilink")[10.4中的屏幕保護程序](../Page/Mac_OS_X_v10.4.md "wikilink")，它將專輯插圖顯示作屏幕保護。iTunes.widget是一個用於控制iTunes的[Dashboard](../Page/Dashboard.md "wikilink")
[Widget](../Page/widget_\(computing\)#Desktop_widgets.md "wikilink")。

眾多的第三方應用程序也使用者iTunes[數據庫](../Page/數據庫.md "wikilink")。[iEatBrainz是一個](../Page/iEatBrainz.md "wikilink")[MusicBrainz通過](../Page/MusicBrainz.md "wikilink")[AppleScript查詢和更正iTunes數據庫的標籤添加程序](../Page/AppleScript.md "wikilink")。

### iPhone啟用

### 打印

為彌補物理CD的不足，iTunes能打印自訂的[珍藏版插頁及歌曲](../Page/珍藏版.md "wikilink")／專輯列表。在從播放列表中燒製CD完成後，用戶可以選擇這個播放列表並點按檔案-打印，打開一個有著打印選項的對話框。這些用戶能選擇打印單專輯封頁（購買的iTunes專輯）或者一個組合封頁（用戶自創列表）。iTunes能自動設置一個一面包含專輯圖片，另一面包含軌道標題的模板。

### iMix

### 網路廣播

iTunes
1.0附帶了對[Kerbango網際網路廣播電台服務的支持](../Page/Kerbango.md "wikilink")，提供給iTunes用戶一個另一個可用的流行線上廣播電台串流媒體的選擇。2001年，[Kerbango公司推出了商業](../Page/Kerbango.md "wikilink")，蘋果公司為iTunes
2.0及其後續版本建立了它自己的網上廣播服務。截止於[2005年7月](../Page/2005年7月.md "wikilink")，iTunes廣播服務已經可以支持大約200到300個不同的「廣播站」（網上共有超過400個支持多比特率的串流媒體），大多數是MP3串流的格式。節目涵蓋了許多種類的音樂和談話，其中包括了像[Radio
Paradise](../Page/Radio_Paradise.md "wikilink")、[DI.fm](../Page/DI.fm.md "wikilink")、[SomaFM之類的主要的線上廣播以及以](../Page/SomaFM.md "wikilink")[KKJZ](../Page/KKJZ.md "wikilink")、[WFMU和](../Page/WFMU.md "wikilink")[WMVY為代表的地面電台](../Page/WMVY.md "wikilink")。iTunes同樣支持可以用[Winamp播放的](../Page/Winamp.md "wikilink").pls和.m3u格式檔，iTunes幾乎使用了所有的串流媒體格式。

蘋果公司目前不再升級Internet廣播電台了，iTunes網站上也沒有任何消息被提起。

### Plug-ins

iTunes支持視覺化和設備插件。視覺效果插件允許開發者建立音樂驅動的視覺效果（iTunes包括一個預設的視覺效果，G-Force，許可證來自[SoundSpectrum](../Page/SoundSpectrum.md "wikilink")）。視覺效果插件SDK
for
Mac與Windows可以從蘋果網站上免費下載。設備插件允許對附加音樂播放設備的支持，但是蘋果將只授權[API給那些簽署了保密協議的](../Page/API.md "wikilink")*bona
fide*[OEM](../Page/OEM.md "wikilink")。

### Apple Music

2015年6月30日，随着Apple Music在iOS上率先推出，苹果公司也正式宣布Apple Music同时会整合进iTunes中。

## 參考資料

## 外部連結

  - [iTunes - Apple](https://www.apple.com/itunes/)
  - [iTunes - Apple (中国)](https://www.apple.com/cn/itunes/)
  - [iTunes - Apple (台灣)](https://www.apple.com/tw/itunes/)
  - [iTunes - Official Apple Support](https://support.apple.com/itunes)

## 參閱

  -
  - [媒體播放器比較](../Page/媒體播放器比較.md "wikilink")

  -
  -
[iTunes](../Category/iTunes.md "wikilink")
[Category:蘋果公司軟體](../Category/蘋果公司軟體.md "wikilink")
[Category:Mac OS软件](../Category/Mac_OS软件.md "wikilink")
[Category:MacOS軟體](../Category/MacOS軟體.md "wikilink")
[Category:IOS软件](../Category/IOS软件.md "wikilink")
[Category:Windows軟體](../Category/Windows軟體.md "wikilink")
[Category:數位音訊](../Category/數位音訊.md "wikilink")
[Category:媒体播放器](../Category/媒体播放器.md "wikilink")

1.
2.
3.
4.
5.
6.