**iWork**是[蘋果公司為](../Page/蘋果公司.md "wikilink")[OS
X以及](../Page/OS_X.md "wikilink")[iOS](../Page/iOS.md "wikilink")
[作業系統開發的](../Page/作業系統.md "wikilink")[辦公室軟體](../Page/辦公室軟體.md "wikilink")。第一版的iWork
- iWork
'05是在2005年發表。最初的套裝包含[Keynote](../Page/Keynote.md "wikilink")，一套原先獨立銷售的簡報軟體，以及[Pages](../Page/Pages.md "wikilink")，一套整合[文書處理及頁面排版的應用程式](../Page/文書處理.md "wikilink")。2007年[蘋果公司發表iWork](../Page/蘋果公司.md "wikilink")
'08，其中包含一套新的[試算表軟體](../Page/電子數據表.md "wikilink") -
Numbers。iWork同時提供[iWork.com](../Page/iWork.com.md "wikilink")，一個提供使用者上傳文件同時允許其他使用者下載或給予回饋意見的測試版線上服務。iWork透過媒體瀏覽器整合[蘋果公司的](../Page/蘋果公司.md "wikilink")[iLife套裝軟體以及其他軟體](../Page/iLife.md "wikilink")，讓使用者能夠用拖放的方式從[iTunes加入音樂](../Page/iTunes.md "wikilink")，從[iMovie加入影片以及從](../Page/iMovie.md "wikilink")[iPhoto和](../Page/iPhoto.md "wikilink")[Aperture加入影像到iWork文件](../Page/Aperture.md "wikilink")。

iWork被[蘋果公司視為](../Page/蘋果公司.md "wikilink")[AppleWorks套裝軟體的升級版本](../Page/AppleWorks.md "wikilink")，但它並沒有沿用[AppleWorks的](../Page/AppleWorks.md "wikilink")[資料庫和繪圖工具](../Page/資料庫.md "wikilink")。雖然每一台新的[Mac電腦都搭載](../Page/Mac.md "wikilink")[iLife套裝](../Page/iLife.md "wikilink")，但iWork需要單獨購買。每台[Mac電腦上都有免費的iWork](../Page/Mac.md "wikilink")
30天試用版本。舊版本的[iLife用戶要升級到新版本時也會同時獲得一套免費的iWork試用版](../Page/iLife.md "wikilink")。

iWork原本售價79美元，其後改為三個程式獨立銷售，在OS
X和iOS平台分別售19.99和9.99美元。2013年尾，蘋果公司發布新一代[OS
X Mavericks](../Page/OS_X_Mavericks.md "wikilink")，同時宣佈新一代的iWork
2013改為免費，但僅限新購買的Mac機和iOS裝置。但有網民發現只要在舊的Mac機上安裝iWork試用版，就能升級到最新版，並在Apple
ID上會有購買記錄。蘋果公司表示不會封鎖這個漏洞，因為要確保所有購買了舊版iWork的使用者都能升級到最新版本。\[1\]

## 历史版本

### iWork '05

第一版的iWork是在2005年1月11日的[Macworld Conference &
Expo發表](../Page/Macworld_Conference_&_Expo.md "wikilink")，1月22日在[美國上市之後](../Page/美國.md "wikilink")，接著在1月29日全球上市。iWork
'05包含兩個應用程式：Keynote
2，一個簡報製作軟體，以及Pages，一個[文書處理軟體](../Page/文書處理.md "wikilink")。iWork
'05美國官方定價為79[美元](../Page/美元.md "wikilink")，台灣蘋果官方定價為[新台幣](../Page/新台幣.md "wikilink")1300元。蘋果官方網站另外提供30天試用版下載。

### iWork '08

iWork
'08在2007年8月7日[加州](../Page/加州.md "wikilink")[庫比蒂諾蘋果總部的新聞發表會上發表](../Page/庫比蒂諾.md "wikilink")。如同過去的版本，iWork
'08提供更新版本的[Keynote和](../Page/Keynote.md "wikilink")[Pages](../Page/Pages.md "wikilink")。[Numbers](../Page/Numbers.md "wikilink")，一套新的[試算表軟體同時包含在這次發表的套裝裡](../Page/電子數據表.md "wikilink")。不同於包括[Microsoft
Excel等其他](../Page/Microsoft_Excel.md "wikilink")[試算表應用程式](../Page/電子數據表.md "wikilink")，[Numbers允許使用者利用一些內建的樣板建立包含多個試算表的文件](../Page/Numbers.md "wikilink")。另外雖然iWork
'08的應用程式、功能都有所增加，但經過壓縮處理，安裝所需要的空間從上一版的1.85 GB降低到iWork '08的690 MB。

### iWork '09

最新版本的iWork '09在2009年1月7日的[Macworld Conference &
Expo發表](../Page/Macworld_Conference_&_Expo.md "wikilink")，並且在同一天上市。iWork
'09包含[Keynote](../Page/Keynote.md "wikilink")、[Pages和](../Page/Pages.md "wikilink")[Numbers的更新版本](../Page/Numbers.md "wikilink")，同時提供[iWork.com](../Page/iWork.com.md "wikilink")，一個允許使用者線上分享文件的測試版服務。iWork
'09的使用者可以直接從[Keynote](../Page/Keynote.md "wikilink")、[Pages或](../Page/Pages.md "wikilink")[Numbers上傳文件到](../Page/Numbers.md "wikilink")[iWork.com](../Page/iWork.com.md "wikilink")，並且邀請其他人在線上檢視這份文件。其他使用者可以在文件上加入註解或給予回饋意見，同時也能夠用iWork、[Microsoft
Office或](../Page/Microsoft_Office.md "wikilink")[PDF格式下載文件](../Page/PDF.md "wikilink")。

### iWork 2013

在 2013 年的 iPhone
發佈會上，[蘋果公司宣佈會免費提供新一代iWork](../Page/蘋果公司.md "wikilink")，它只能運行於OS
X
10.9上。新版本升級為[64位元](../Page/64位元.md "wikilink")，代碼都是重新編寫，故此一些功能沒有移植至新版。有用家就不滿有些原有的功能消失了，蘋果表示將於未來六個月陸續補回一系列功能。\[2\]

### iWork with Apple Pencil

2017年苹果公司发布[iPad Pro发布的特别针对Apple](../Page/iPad_Pro.md "wikilink")
Pencil的版本，基于iWork 2013。

## iOS

2010年1月27日[蘋果公司在發表](../Page/蘋果公司.md "wikilink")[iPad的同時也發表](../Page/iPad.md "wikilink")[iPad版的iWork應用程式](../Page/iPad.md "wikilink")。不同於[Mac
OS
X的iWork](../Page/Mac_OS_X.md "wikilink")，[iPad版的iWork並非以套裝軟體銷售](../Page/iPad.md "wikilink")，而是個別在[App
Store銷售](../Page/App_Store.md "wikilink")。三個應用程式的售價皆為9.99[美元](../Page/美元.md "wikilink")。在這之前iOS上僅有能夠遙控簡報播放的Keynote
Remote。

## 應用程式

### Pages

[Pages是一個兼具文書處理與頁面排版功能的應用程式](../Page/Pages.md "wikilink")。它內建蘋果公司為多種類別設計的樣板，包括：

  - 書信
  - 信封
  - 表單
  - 履歷
  - 報告
  - 新聞信
  - 小冊子
  - 傳單
  - 海報
  - 卡片與邀請函
  - 名片
  - 證書

Pages和[Keynote可以與](../Page/Keynote.md "wikilink")[iLife套裝整合](../Page/iLife.md "wikilink")。它包含媒體瀏覽器，可以從[iTunes加入音樂](../Page/iTunes.md "wikilink")，從
[iMovie加入影片以及從](../Page/iMovie.md "wikilink")[iPhoto加入影像](../Page/iPhoto.md "wikilink")。Pages允許使用者儲存或輸出以下檔案格式：

  - [Pages文件](../Page/Pages.md "wikilink")（.[pages](../Page/pages.md "wikilink")）
  - [微軟的](../Page/微軟.md "wikilink")[Word文件](../Page/Word.md "wikilink")（.[docx和](../Page/docx.md "wikilink").[doc](../Page/doc.md "wikilink")）
  - [Adobe的](../Page/Adobe.md "wikilink")[PDF文件](../Page/PDF.md "wikilink")（.[pdf](../Page/pdf.md "wikilink")）
  - 網頁文件（.[html](../Page/html.md "wikilink")）
  - RTF文件（.rtf）
  - 純[文本文件](../Page/文本文件.md "wikilink")（.txt）
  - ePub

### Keynote

[Keynote是一個跟](../Page/Keynote.md "wikilink")[Microsoft
PowerPoint相似的簡報程式](../Page/Microsoft_PowerPoint.md "wikilink")，著重於展示發表幻燈片的應用程式。和Pages一樣，它可以藉由媒體瀏覽器從[iTunes加入音樂](../Page/iTunes.md "wikilink")，從
[iMovie加入影片以及從](../Page/iMovie.md "wikilink")[iPhoto加入影像](../Page/iPhoto.md "wikilink")。它還可以建立幻燈片秀的動畫文件。[Keynote支援很多檔案格式](../Page/Keynote.md "wikilink")，包括：

  - [Keynote文件](../Page/Keynote.md "wikilink")（.[key](../Page/key.md "wikilink")）
  - [微軟的](../Page/微軟.md "wikilink")[Powerpoint文件](../Page/Powerpoint.md "wikilink")（.[pptx和](../Page/pptx.md "wikilink").[ppt](../Page/ppt.md "wikilink")）
  - [QuickTime影片](../Page/QuickTime.md "wikilink")（.mov），此格式能夠選擇以互動模式進行
  - [PDF文件](../Page/PDF.md "wikilink")（.pdf）

### Numbers

Numbers是在2007年加入iWork '08套裝的一個試算表應用程式。類似於[Microsoft
Excel以及其他試算表應用程式](../Page/Microsoft_Excel.md "wikilink")，Numbers讓使用者將數據組織成表單，利用公式進行運算及建立圖表。不同於其他的試算表應用程式，Numbers允許使用者在一個文件中建立多個試算表，並且提供包含設計給個人金融、教育、商業使用的樣板。当前版本的Numbers支持存储或输出的格式有：

  - Numbers ’09（2.0或更高版本)
  - [Microsoft
    Excel](../Page/Microsoft_Excel.md "wikilink")（.[xlsx和](../Page/xlsx.md "wikilink").[xls](../Page/xls.md "wikilink")）
  - [制表符分隔文本文件](../Page/制表符.md "wikilink")
  - 逗号分隔值（CSV）
  - PDF文件

## 線上服務

iWork.com是一個允許使用者直接將Pages、Keynote和Numbers文件線上分享的服務。使用者只需要點選iWork.com的工具列圖示並且輸入AppleID即可上傳文件並且邀請其他人檢視。其他使用者可以在文件上加入註解或給予回饋意見，同時也能夠用iWork、[Microsoft
Office或](../Page/Microsoft_Office.md "wikilink")[PDF格式下載文件](../Page/PDF.md "wikilink")。文件持有人可以在iWork.com網站查看回饋意見。这个服务于2012年7月31日关闭，并由iCloud取代。

## 版本

| iWork版本           | Keynote版本 | Pages版本 | Numbers版本 | 作業系統                                                           | 發表日期             |
| ----------------- | --------- | ------- | --------- | -------------------------------------------------------------- | ---------------- |
| iWork '05         | 2.0       | 1.0     | \--       | 10.3.6                                                         | 2005年1月11日       |
| iWork '06         | 3.0       | 2.0     | \--       | 10.3.9                                                         | 2006年1月10日       |
| iWork '08         | 4.0       | 3.0     | 1.0       | 10.4.10                                                        | 2007年8月7日        |
| iWork '09         | 5.0       | 4.0     | 2.0       | 10.4.11（x.0）; 10.6.6（x.1）; 10.7.4（x.2）; 10.7.4（x.3）; 10.8（x.3） | 2009年1月7日        |
| iWork 2013 update | 6.0       | 5.0     | 3.0       | 10.9                                                           | October 22, 2013 |
| iWork for iOS     | 2.0       | 2.0     | 2.0       | iOS 7                                                          | October 22, 2013 |

## 批評

由於早期iWork早期版本沒有包含試算表、資料庫軟體和項目管理程式而一直備受爭議。雖然在Keynote 3和Pages
2中有一些新功能，像是在文件內建立可進行運算的表格等，這些功能可以讓使用者不需要一個專門的試算表軟體來完成一些操作，但是卻造成與其他程式的資料交換變得困難。隨著Numbers的加入，這些爭議已基本獲得解決。

其他問題還包含，在Pages中打字反應較慢，調整字體大小顏色比較麻煩，不支援多種語言輸入下的復合字體，導出Word文件時會遺失非英語語系的字體設定等等。

## 參考文獻

<div class="references-small">

<references />

</div>

## 參見

  - [辦公室套裝軟件](../Page/辦公室套裝軟件.md "wikilink")
  - [Microsoft Office for
    Mac](../Page/Microsoft_Office_for_Mac.md "wikilink")
  - [iLife](../Page/iLife.md "wikilink")

## 外部連結

  - [iWork首頁 (台灣)](http://www.apple.com/tw/iwork/)

[Category:办公室自动化软件](../Category/办公室自动化软件.md "wikilink")
[Category:蘋果公司軟體](../Category/蘋果公司軟體.md "wikilink")
[Category:MacOS軟體](../Category/MacOS軟體.md "wikilink")

1.
2.