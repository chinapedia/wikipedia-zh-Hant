《**I LOVED
YESTERDAY**》是[日本唱作女歌手](../Page/日本.md "wikilink")[YUI于](../Page/YUI.md "wikilink")2008年4月9日所推出的第三張大碟。《I
LOVED YESTERDAY》發行首日即登上日本ORICON大碟榜冠軍，單日銷量已超越54,000張。

[YUI表示新碟的主題就是活就不後悔做過的每一件事](../Page/YUI.md "wikilink")，每一首歌均講述了曲中人物的過去。

## 收錄歌曲

通常版

  - 限量版

通常版 + DVD

  - oricon 2008年度專輯銷售排行榜 第19位 \*,475,709 I LOVED YESTERDAY / YUI
    08/04/09

## 注釋

<div class="references-small">

<references />

</div>

[Category:2008年音樂專輯](../Category/2008年音樂專輯.md "wikilink")
[Category:YUI音樂專輯](../Category/YUI音樂專輯.md "wikilink")
[Category:2008年Oricon專輯週榜冠軍作品](../Category/2008年Oricon專輯週榜冠軍作品.md "wikilink")