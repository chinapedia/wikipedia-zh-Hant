**I Love You
Boyz**（簡稱“ILUB”，又作**艾粒**或**艾粒仔**），是[香港一隊](../Page/香港.md "wikilink")「[搞笑](../Page/搞笑.md "wikilink")」二人男子組合，由分別化名「[陸加俊](../Page/陸家俊.md "wikilink")」和「[蔡輕麟](../Page/蔡興麟.md "wikilink")」的兩位[叱咤903](../Page/叱咤903.md "wikilink")[唱片騎師](../Page/唱片騎師.md "wikilink")[少爺占及](../Page/少爺占.md "wikilink")[Donald組成](../Page/唐劍康.md "wikilink")。

## 組成過程

《I Love You Boyz》原本是一套廣播連續劇，敍述二人組合「I Love You
Boyz」的發展，這隊二人組合的成員為陸加俊及蔡輕麟。「陸加俊」和「蔡輕麟」名字的由來是來自1990年代[軟硬天師的歌曲](../Page/軟硬天師.md "wikilink")《[廣播道](../Page/廣播道.md "wikilink")-{}-fans殺人事件》中的歌詞：「嘩！陸家俊我哋支持你，喂！蔡興麟我哋支持你！」詞中的[陸家俊及蔡興麟同是](../Page/陸家俊.md "wikilink")1992年的香港新晉歌手\[1\]\[2\]，二人的經理人叫「許揪怡」([謝茜嘉飾](../Page/謝茜嘉.md "wikilink"))，取藝人[許秋怡的諧音](../Page/許秋怡.md "wikilink")。

他們首次一同主持是頂替當時生病的[鄭家輝的節目](../Page/鄭家輝.md "wikilink")，之後主持好薯嘜，把時下新聞寫成趣劇。\[3\]

## 歌唱事業

### 早期 （2004 - 2009年）

他們二人在自己的節目裡，經常把時下[流行曲改編成搞笑版本播放](../Page/粵語流行曲.md "wikilink")，大受香港年青人歡迎。其後，他們索性在現實組織二人組合，把精選歌曲輯錄成唱片。除了在電台以外，當時更在公共交通工具上的[路訊通播放](../Page/路訊通.md "wikilink")。他們形象怪異，早期經常會戴着大型假髮出席各種宣傳場合。

他們2004年推出首張專輯《我想買隻I Love You
Boyz》，由[英皇娛樂發行](../Page/英皇娛樂.md "wikilink")。其後他們加盟[Silly
Thing](../Page/Silly_Thing.md "wikilink")，2005年7月發行《Silly
King》專輯。2006年二人推出《艾粒電視台CDVD》。2007年更到日本拍攝推出《艾粒電視台Vol.2-奸爸DVD》，大受歡迎，大賣10,000多張。2008年推出《艾粒電視台3-巨棒堂》，大賣15,000多張，直迫金唱片。2009年1月19日，他們由[Silly
Thing轉投](../Page/Silly_Thing.md "wikilink")[金牌大風](../Page/金牌大風.md "wikilink")，宣佈計劃到中國內地舉行舞台劇及發展電影事業。2009年7月22日推出《艾粒1979
口牽口三十年》初版，在[香港書展舉行期間已告沽清](../Page/香港書展.md "wikilink")，其後於8月初推出第二版CDVD。

在2009年12月28日，I Love You
Boyz（艾粒）演出舞台劇《艾粒普世歡騰核能人》尾聲時，少爺占向觀眾宣佈艾粒正式解散。但Donald補充說，如果2012年沒有世界末日，艾粒可能會復合，更笑稱請看尾場舞台劇的觀眾留起票尾，作為將來購買復出舞台劇門票的折扣。

### 艾粒復合（2012 - 2017年）

在2012年1月尾，適逢艾粒成立十週年，著名社交網站有消息指出艾粒將在2012年復合，為期一年，組合更名為**ILUB**，並在夏天演出舞台劇，更透露ILUB即將發佈新歌\[4\]，同年舉辦《艾粒一百周年呈獻—TENMEME》慶祝組合成立十周年。

於2016年艾粒及其分身組成新組合「五行欠」舉辦一共3場《Yo！聖艾粒Battle五行欠》及1場澳門站演出，期後於2017年再舉辦《艾粒拾伍年開光大典》因門票快速售罄而由最初2場一再加場至5場及後艾粒與音樂平台[Joox合作舉辦了](../Page/Joox.md "wikilink")1場《ILUB
C ALL MOMENT 演唱會》演出，並免費送出整場演唱會之所有門票以回饋一眾Fans。二人於《ILUB C ALL MOMENT
演唱會》上稱能夠舉辦一共10場之舞台演出作為紀念I Love You
Boyz成立15周年實在非常難得及感恩，並指這場演唱會為艾粒組成15周年畫上完美句號。二人在台上表示於短期內他們將不會再舉辦舞台演出，因此在表演期間二人因不捨和感觸而多次眼有淚光，但堅持強忍淚水完成演出，更兩度Encore表演以答謝Fans支持。

### 進軍紅館 (2018年至今)

艾粒於2018年10月在[BOY'Z演唱會宣布](../Page/BOY'Z.md "wikilink")，有意進軍[紅磡體育館舉辦演唱會](../Page/紅磡體育館.md "wikilink")，並已經入紙申請。同時艾粒於社交網絡發起[hashtag](../Page/hashtag.md "wikilink")「**\#細姨媽**」行動以示其人氣。\[5\]艾粒於2018年12月發布多套網上短片，最後一集邀請得與少爺占真名相差一個字的[甄子丹參與](../Page/甄子丹.md "wikilink")，宣布成功獲得2019年4月的紅館檔期。\[6\]

《艾粒Boom 紅館激戰》演唱會最終於2019年4月5-6日成功舉行，演唱會分別有[Supper
Moment](../Page/Supper_Moment.md "wikilink")（4月5日）、[C
AllStar](../Page/C_AllStar.md "wikilink")（4月6日）和[BOY'Z](../Page/BOY'Z.md "wikilink")（4月5-6日）擔任嘉賓。當奴與少爺占更於台上即場表演吞下他們恐懼的食物：少爺占於4月5日吞下[納豆](../Page/納豆.md "wikilink")、當奴於4月6日吞下[榴槤](../Page/榴槤.md "wikilink")，寓意為演藝生涯寫下光輝一頁。\[7\]

## 派台歌曲與成績

| **派台歌曲成績**                                                           |
| -------------------------------------------------------------------- |
| 唱片                                                                   |
| **2004年**                                                            |
| [我想買隻I Love You Boyz大碟](../Page/我想買隻I_Love_You_Boyz大碟.md "wikilink") |
| 我想買隻I Love You Boyz大碟                                                |
| 我想買隻I Love You Boyz大碟                                                |
| 我想買隻I Love You Boyz大碟                                                |
| 我想買隻I Love You Boyz大碟                                                |
| 我想買隻I Love You Boyz大碟                                                |
| **2005年**                                                            |
| 我想買隻I Love You Boyz大碟                                                |
| 我想買隻I Love You Boyz大碟                                                |
| [Silly King](../Page/Silly_King.md "wikilink")                       |
| Silly King                                                           |
| Silly King                                                           |
| Silly King                                                           |
| Silly King                                                           |
| [艾粒電視台](../Page/艾粒電視台.md "wikilink")                                 |
| **2006年**                                                            |
| 艾粒電視台                                                                |
| 艾粒電視台                                                                |
| 艾粒電視台                                                                |
| 艾粒電視台                                                                |
| **2007年**                                                            |
| [艾粒電視台3 艾粒巨棒堂](../Page/艾粒電視台3_艾粒巨棒堂.md "wikilink")                   |
| 艾粒電視台3 艾粒巨棒堂                                                         |
| **2008年**                                                            |
| 艾粒電視台3 艾粒巨棒堂                                                         |
| 艾粒電視台3 艾粒巨棒堂                                                         |
| **2009年**                                                            |
| [艾粒電視台4 艾粒1979 口牽口三十年](../Page/艾粒電視台4_艾粒1979_口牽口三十年.md "wikilink")   |
| 艾粒電視台4 艾粒1979 口牽口三十年                                                 |
| 艾粒電視台4 艾粒1979 口牽口三十年                                                 |
| To Be Continued                                                      |
| To Be Continued                                                      |
| **2012年**                                                            |
|                                                                      |
|                                                                      |
|                                                                      |
| **2016年**                                                            |
|                                                                      |
|                                                                      |
| **2017年**                                                            |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
| **2019年**                                                            |
|                                                                      |
|                                                                      |

| **各台冠軍歌總數**                          |
| ------------------------------------ |
| [叱咤903](../Page/叱咤903.md "wikilink") |
| **4**                                |

  - 上榜中（\*）
  - **粗體顯示**為冠軍歌曲
  - **(1)**表示兩週冠軍

## 演藝事業

二人首次於[TVB主持的節目名為](../Page/TVB.md "wikilink")《[零舍扮野](../Page/零舍扮野.md "wikilink")》，是[日本一個模仿式節目](../Page/日本.md "wikilink")，算是二人在電視演出上的一個踏腳石。其後[TVB在](../Page/TVB.md "wikilink")2006年及2007年[外購了兩輯](../Page/外購.md "wikilink")[日本節目](../Page/日本.md "wikilink")《[阿笨與阿占](../Page/阿笨與阿占.md "wikilink")》，更請來二人分別為阿笨、阿占配上生鬼對白。於2007年[TVB又為二人推出了一個共兩集的特輯名為](../Page/TVB.md "wikilink")《[愚樂
I Love U](../Page/愚樂_I_Love_U.md "wikilink")》及一個共五集的《[愚樂 I Love You
Summer](../Page/愚樂_I_Love_You_Summer.md "wikilink")》節目。《[愚樂 I Love You
Summer](../Page/愚樂_I_Love_You_Summer.md "wikilink")》更請來多位嘉賓主持，分別為[澳門篇的](../Page/澳門.md "wikilink")[賈曉晨和](../Page/賈曉晨.md "wikilink")[蔣家旻](../Page/蔣家旻.md "wikilink")、[廣州篇的](../Page/廣州.md "wikilink")[袁彌明和](../Page/袁彌明.md "wikilink")[李綺雯及](../Page/李綺雯.md "wikilink")[海南島篇的](../Page/海南島.md "wikilink")[HotCha](../Page/HotCha.md "wikilink")。2008年二人更主持了兩集《[好友移城](../Page/好友移城.md "wikilink")》、第三季《[筋肉擂台](../Page/筋肉擂台.md "wikilink")》及為[TVB兒童節擔任大使](../Page/TVB兒童節.md "wikilink")（同年其他大使還有[謝安琪及](../Page/謝安琪.md "wikilink")[商天娥](../Page/商天娥.md "wikilink")）。

## 參與媒體

### 唱片

  - 21/08/2004 我想買隻I Love You Boyz大碟
  - 13/07/2005 [Silly King](../Page/Silly_King.md "wikilink")
  - 23/09/2006 艾粒電視台
  - 18/07/2007 艾粒電視台Vol.2 奸爸DVD
  - 23/07/2008 艾粒電視台3 艾粒巨棒堂 CDVD
  - 22/07/2009 艾粒電視台4 艾粒1979 口牽口三十年
  - 18/12/2009 To Be Continued(新歌+精選)

### 主演舞-{台}-劇/演唱會

  - 2005 《歡樂滿TOUR之娛樂大飲茶》
  - 2005 《I Love You Boyz 歡樂滿Tour Part II 之 05壓軸導人向笑劇場 – 龍兄鳳弟》
  - 2006 《歡樂粒宵》
  - 2007 《I Love You Boyz GaGame Show 鬆啲\!衣池瀨》
  - 2009 《2009 X'MAS ILUB FINAL SHOW 艾粒"普世歡騰"核能人》
  - 2012 《艾粒一百周年呈獻—TENMEME》
  - 2016 《Yo！聖艾粒Battle五行欠》
  - 2017 《艾粒拾伍年開光大典》
  - 2017 《ILUB C ALL MOMENT 演唱會》
  - 2019 《艾粒Boom紅館激戰》

### 電影

  - 2004 《[甜絲絲](../Page/甜絲絲.md "wikilink")》
  - 2008 《[六樓后座2家屬謝禮](../Page/六樓后座2家屬謝禮.md "wikilink")》
  - 2009 《[保持愛你](../Page/保持愛你.md "wikilink")》
  - 2009 《[矮仔多情](../Page/矮仔多情.md "wikilink")》

### 電視節目主持

  - TVB 2004年 [零舍扮嘢](../Page/零舍扮嘢.md "wikilink")
  - 有線娛樂台 2005年 [玩死男朋友](../Page/玩死男朋友.md "wikilink")
  - TVB 2006年7月
    [阿笨與阿占](../Page/阿笨與阿占.md "wikilink")（配音+主持）（[Donald聲演阿笨](../Page/唐劍康.md "wikilink")；[少爺占聲演阿占](../Page/少爺占.md "wikilink")）
  - TVB 2007年4月 [愚樂I love U](../Page/愚樂I_love_U.md "wikilink")
  - TVB 2007年7月
    [阿笨與阿占](../Page/阿笨與阿占.md "wikilink")（第二輯）（配音+主持）（[少爺占聲演阿占](../Page/少爺占.md "wikilink")；[Donald聲演阿笨](../Page/唐劍康.md "wikilink")）
  - TVB 2007年9月1日 [愚樂I love U
    Summer](../Page/愚樂I_love_U_Summer.md "wikilink")
  - TVB 2008年6月22及29日 [好友移城(第三集)](../Page/好友移城.md "wikilink")
  - TVB 2008年8月28日 [筋肉擂臺](../Page/筋肉擂臺.md "wikilink")
  - TVB 2010年 [逐格鬥](../Page/逐格鬥.md "wikilink")
  - ViuTV 2016年 [對不起 標籤你](../Page/對不起_標籤你.md "wikilink")
  - ViuTV 2017年 [黑料理](../Page/黑料理.md "wikilink")

### 電視嘉賓

  - [ViuTV](../Page/ViuTV.md "wikilink") 2016年
    [總有一隻喺左近](../Page/總有一隻喺左近.md "wikilink")

### 書籍

  - 2007年 [東京勁作狀](../Page/東京勁作狀.md "wikilink")
  - 2008年 [艾粒老爺車](../Page/艾粒老爺車.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [艾粒Forum](https://web.archive.org/web/20110131023504/http://iloveyouboyz.com/)
  - [iLub in pinkwork (sound &
    video)](http://www.pink.com.hk/musicshop/iloveuboyz.htm)
  - [少爺占+野仔訪問 in pinkwork (sound &
    video)](http://www.pinkwork.com/shortnews/wildchild.htm)
  - 艾粒專訪@娛樂直播2007-04-19(youtube)[片段一](http://www.youtube.com/watch?v=JQeEOaNaIHM)[片段二](http://www.youtube.com/watch?v=nmKb8UmrhMM&mode=related&search=)
  - [少爺占&野仔相片及文字（按標題）](http://www.pinkmessage.com/talk/viewforum.php?f=7)
  - [ILUB FACEBOOK官方群組](https://www.facebook.com/iloveILUB)
  - [艾粒2012舞台劇](https://web.archive.org/web/20120607045117/http://www.my903.com/my903/tenmeme/video.jsp)
  - [MyDonald.com](http://mydonald.com)

[Category:單飛不解散團體](../Category/單飛不解散團體.md "wikilink")
[Category:香港男子演唱團體](../Category/香港男子演唱團體.md "wikilink")
[Category:惡搞文化](../Category/惡搞文化.md "wikilink")
[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")

1.
2.  [1](https://web.archive.org/web/20060410102839/http://hk.geocities.com/yccchris/LukKaChun.htm)
3.  [My Name Is 邦](../Page/My_Name_Is_邦.md "wikilink")，CH63
4.  [2](https://www.facebook.com/iloveILUB)
5.  [明抽暗寸
    艾粒跪求集氣紅館開騷](http://www.metrodaily.hk/metro_news/明抽暗寸-艾粒跪求集氣紅館開騷)
6.  [艾粒網上集氣成功！　搵甄子丹宣布明年4月殺入紅館](https://www.hk01.com/即時娛樂/268014/艾粒網上集氣成功-搵甄子丹宣布明年4月殺入紅館)
7.  [3](https://www.hk01.com/演唱現場/315176/艾粒演唱會兩度落淚-做細場冇所謂-但其實我哋都想上紅館)