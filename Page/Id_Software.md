[Doom-boxart.jpg](https://zh.wikipedia.org/wiki/File:Doom-boxart.jpg "fig:Doom-boxart.jpg")
**id
Software**是一家在[美國](../Page/美國.md "wikilink")[得克薩斯州的](../Page/得克薩斯州.md "wikilink")[電子遊戲](../Page/電子遊戲.md "wikilink")[開發公司](../Page/開發.md "wikilink")。公司除了從事[電腦遊戲的開發以外](../Page/電腦遊戲.md "wikilink")，亦有自行研發[遊戲引擎](../Page/遊戲引擎.md "wikilink")。知名的[第一人稱射擊遊戲大作早期版的](../Page/第一人稱射擊遊戲.md "wikilink")《[戰慄時空](../Page/戰慄時空.md "wikilink")》和早期版的《[反恐精英](../Page/反恐精英.md "wikilink")》（*Counter-Strike*，簡稱CS）就是利用[Quake的引擎制作的](../Page/Quake.md "wikilink")。該公司在1993年推出的作品《[毀滅戰士](../Page/毀滅戰士.md "wikilink")》（*DOOM*）徹底改變了電腦遊戲產業，在當時有著里程碑的意義。在2004年該公司推出的作品《[毀滅戰士III](../Page/毀滅戰士III.md "wikilink")》在[E3遊戲大展上一鳴驚人](../Page/E3.md "wikilink")，包攬了5項大獎。

公司於2009年6月24日被[貝塞斯達軟件公司](../Page/貝塞斯達軟件公司.md "wikilink")（Bethesda
Software）的母公司[ZeniMax
Media收歸其下](../Page/ZeniMax_Media.md "wikilink")，成為其子公司。\[1\]

## 公司歷史

公司在1991年2月1日由[約翰·卡馬克](../Page/約翰·卡馬克.md "wikilink")（John
Carmack）等人創辦。他們當初在一家名叫[Softdisk的公司當小](../Page/Softdisk.md "wikilink")[程序員](../Page/程序員.md "wikilink")，期間由他們編寫的[電腦遊戲](../Page/電腦遊戲.md "wikilink")《[指揮官基恩](../Page/指揮官基恩.md "wikilink")》（*Commander
Keen*）在几個月賣出了3万套，這在當時正處在萌芽階段的[電腦遊戲產業來說是一個极其惊人的數字](../Page/電腦遊戲.md "wikilink")。在一個月後約翰·卡馬克等人集体辭職，並成立了id公司。1993年《[毀滅戰士](../Page/毀滅戰士.md "wikilink")》的成功讓id公司真正走向了業界之顛，這對于一家只有13人的小公司來說的确是個不小的成就，僅在1995年公司的純[利潤就達](../Page/利潤.md "wikilink")1500多万[美圓](../Page/美圓.md "wikilink")，讓公司几乎所有的人都變成了百万富翁。

## 公司名字來源

id
Software的正確寫法是以英文的小寫“id”。類似于「kid」或者是「did」的字尾念法，音「意得」。此詞源自心理學中[西格蒙德·弗洛伊德的](../Page/西格蒙德·弗洛伊德.md "wikilink")[本我](../Page/本我、自我與超我.md "wikilink")（英文）的學說而來。德軍總部3D的遊戲說明書中就有西格蒙德的名言「that's
Id, as in the id, ego, and superego in the psyche」。即使現在﹐id
Software的官方網站還有弗洛伊德的足跡。

在約翰·羅梅洛和藍·羅司在1989年創立Softdisk時，id Software的名字被提及是從英文的句子“Ideas from the
Deep”而來。當初的公司縮名為“IFD傢伙們”，而id又是IFD的縮寫。所以有時會被以單字發音為「愛低」。之前id
Software的徽標是英文大寫，但在毀滅戰士出版時是小寫的「id」。

有人認為“id”跟德文單詞「idee」（同英文idea，主意、念頭）很像，但這已經被證不屬實。也有人認為是英文的“Die”（死亡）的反寫“eiD”而來的。但此猜測也不屬實，因為id
Software從來就沒有以混寫過「iD」。此猜測基於id Software的遊戲都是第一人稱射擊遊戲，所以此聯想較合理。

在傳記[Masters of Doom裡提到](../Page/Masters_of_Doom.md "wikilink")，id
Software是從英文「in demand」的縮寫而來的。

## 公司關鍵人物

公司的固定員工只有十幾人，但他們每個人都有至少一輛法拉利停在公司樓下。約翰·卡馬克曾經把自己一輛法拉利作為該公司舉辦的[電子競技比賽獲胜者的獎品](../Page/電子競技.md "wikilink")。

兩千零三年的[Masters of Doom的傳記書述說了id](../Page/Masters_of_Doom.md "wikilink")
Software起家過程，其書有提到約翰·卡馬克和約翰·羅梅洛性格和兩人的互動。以下是有為id Software的成功付出貢獻的關鍵人物。

### 約翰·卡馬克

[約翰·卡馬克是id](../Page/約翰·卡馬克.md "wikilink")
Software的主程式設計師。他的立體電腦圖形能力是在電子遊戲產業受到重視的。id的幾個暢銷遊戲都是以他編寫的繪圖引擎做為基礎。卡馬克在2013年離開id
Software，以技術長（CTO）身份加入Oculus VR公司。

### 約翰·羅梅洛

[約翰·羅梅洛在](../Page/約翰·羅梅洛.md "wikilink")[雷神之鎚的推出之後就被強迫離職](../Page/雷神之錘_\(遊戲\).md "wikilink")。接著他成立了只有經營短暫的[離子風暴公司](../Page/離子風暴.md "wikilink")。約翰·羅梅洛曾經因為[大刀的拖延而漫長的開發過程而受到大眾的矚目](../Page/大刀_\(遊戲\).md "wikilink")。約翰·羅梅洛現在正他新公司開發一個新的[大型多人線上遊戲](../Page/大型多人線上遊戲.md "wikilink")。

### 湯姆·霍爾

[湯姆·霍爾在毀滅戰士早期開發過程中就被強迫離職](../Page/湯姆·霍爾.md "wikilink")。但是霍爾在走之前也有對毀滅戰士有所貢獻。他的貢獻包括了遊戲裡的瞬間移動裝置。霍爾是在毀滅戰士的共享版本推出前離職的。離職後霍爾加入了[Apogee
Software](../Page/Apogee_Software.md "wikilink")，同「擁有難以置信的力量的創作家」合作開發[Rise
of the
Triad](../Page/Rise_of_the_Triad.md "wikilink")。之後霍爾因和[獵魂的製作員工不和而再次離職](../Page/獵魂.md "wikilink")。離開Apogee
Software後，霍爾加入了約翰·羅梅洛的離子風暴公司。霍爾常提如果id
Software肯賣出[指揮官基恩的知識產權](../Page/指揮官基恩.md "wikilink")，他就會馬上開發指揮官基恩的遊戲新版本。

### 桑迪·皮特森

[桑迪·皮特森是毀滅戰士的關卡設計家](../Page/桑迪·皮特森.md "wikilink")。毀滅戰士其中的十九關和毀滅戰士II其中的十七關都是他設計的。皮特森是[霍華德·菲利普·洛夫克拉夫特的愛好者](../Page/霍華德·菲利普·洛夫克拉夫特.md "wikilink")。其雷神之鎚中的怪物和敵人都有洛夫克拉夫特作品的影響。他開發了雷神之鎚的第四劇本﹐也是雷神之鎚的最後一個劇本。皮特森在雷神之鎚II的開發過程中離職。而他的所作都在雷神之鎚II推出前廢除掉了。

### 亞美利堅·麥基

[亞美利堅·麥基是毀滅戰士II](../Page/亞美利堅·麥基.md "wikilink")、究極毀滅戰士、雷神之鎚和雷神之鎚II的關卡設計家。麥基在雷神之鎚II推出之後就被id
Software解僱了。之後麥基移至[美國藝電](../Page/美國藝電.md "wikilink")，在美國藝電麥基開發了[愛麗絲夢遊仙境的遊戲](../Page/愛麗絲夢遊仙境.md "wikilink")，其遊戲叫[愛麗絲驚魂記](../Page/愛麗絲驚魂記.md "wikilink")。離開美國藝電之後麥基成為了獨立的企業家和遊戲開發家。麥基現在主導著上海的[Spicy
Horse](../Page/Spicy_Horse.md "wikilink")，曾開發[愛麗絲的第二部遊戲](../Page/愛麗絲驚魂記：瘋狂再臨.md "wikilink")。\[2\]\[3\]

## 技術

從他們的第一個共享系列遊戲指揮官基恩，id
Software就開始授權他們的遊戲源碼（遊戲引擎）給其他的遊戲製作公司。原本為約翰·羅梅洛的點子。在1991年的夏天，id
Software舉辦了一個週末講座給一些預期的買家﹐其中包括了[斯科·特米勒](../Page/斯科·特米勒.md "wikilink")、[喬治·布魯薩爾德](../Page/喬治·布魯薩爾德.md "wikilink")、[肯·羅勾衛](../Page/肯·羅勾衛.md "wikilink")、[吉姆·諾伍德和](../Page/吉姆·諾伍德.md "wikilink")[托德·雷普洛格爾](../Page/托德·雷普洛格爾.md "wikilink")。其中一晚包括了一個臨時製作的遊戲叫做“瓦克男”。“瓦克男”發揮了在指揮官基恩中使用的遊戲引擎技術和遊戲引擎的內部過程。

之後id
Software授權出了指揮官基恩遊戲引擎、德軍總部3D遊戲引擎、暗影法師遊戲引擎\[4\]、毀滅戰士遊戲引擎、雷神之鎚I、II、III遊戲引擎和毀滅戰士III中使用的技術。這些遊戲引擎被編用在許多的遊戲中﹐其中最成功的是毀滅戰士III的遊戲引擎。

約翰·卡馬克有公開分享源碼的習慣。卡馬克透過[GPL公開分享了大部份id](../Page/GNU通用公共許可證.md "wikilink")
Software的遊戲引擎。以之前來看，卡馬克都是在其遊戲引擎問世五年以後公開分享遊戲的源碼。借著因為卡馬克這樣的分享，許多私家工程就慢慢出現。導致此遊戲引擎被移殖至不同的平台，像[掌上型電腦](../Page/掌上型電腦.md "wikilink")、[iPod](../Page/iPod.md "wikilink")、[PlayStation
Portable和](../Page/PlayStation_Portable.md "wikilink")[任天堂DS等等](../Page/任天堂DS.md "wikilink")。其他加強像是[Dark
Places在雷神之鎚遊戲引擎中增加](../Page/Dark_Places.md "wikilink")[陰影體特效和更有效率的](../Page/陰影體.md "wikilink")[網絡傳輸協議方式](../Page/網絡傳輸協議.md "wikilink")。還有[ioquake3](../Page/ioquake3.md "wikilink")，難其目標是[id
Tech 3引擎的源碼簡化](../Page/id_Tech_3引擎.md "wikilink")，功能加強和減少程序錯誤。

原本雷神之鎚III的遊戲引擎要在從2004年底公開的，但被推後至於2005年八月。因為雷神之鎚III的遊戲引擎還有被授權給其他公司做為商業用途。

id Software公開發表說他們不會在[Wii主機出遊戲](../Page/Wii.md "wikilink")\[5\]，但是之後id
Software又發表說他們也許會出一些遊戲。\[6\]

自從id Software公開了他們的[id Tech 5遊戲引擎](../Page/id_Tech_5.md "wikilink")，id
Software開始命名他們的遊戲引擎為[id
Tech和加上版本編號](../Page/id_Tech.md "wikilink")\[7\]。之前的遊戲引擎也被加上此編號，像毀滅戰士遊戲引擎被命名為id
Tech 1.

## 遊戲以外

id
Software從的毀滅戰士的小說發表出版後就一直有著出版小說的發展。但從兩千零八年又從新開始出版小說，從[馬修·科斯特洛的毀滅戰士III的](../Page/馬修·科斯特洛.md "wikilink")[毀滅戰士III:火燒全世界和](../Page/毀滅戰士III:火燒全世界.md "wikilink")[毀滅戰士III:漩渦小說從新開始](../Page/毀滅戰士III:漩渦.md "wikilink")。

id
Software從兩千零五年毀滅戰士的電影就有電影製作的參與。托德·雷普洛格爾在兩千零七2007年八月的雷神之錘大會發表說重返德軍總部的電影正在製作當中。其電影的作家和監製是當年拍製[寂靜嶺的](../Page/寂靜嶺_\(電影\).md "wikilink")[羅傑·艾法瑞和](../Page/羅傑·艾法瑞.md "wikilink")[塞繆爾·哈帝達](../Page/塞繆爾·哈帝達.md "wikilink")。

2013年8月7日，John Carmack加入[Oculus
Rift](../Page/Oculus_Rift.md "wikilink")\[[http://en.wikipedia.org/wiki/Oculus_Rift\]任CTO](http://en.wikipedia.org/wiki/Oculus_Rift%5D任CTO)。[1](http://www.theverge.com/2013/8/7/4597678/doom-and-quake-id-software-john-carmack-joins-oculus-as-cto)[2](http://www.36kr.com/p/205271.html)

## 爭議

id
Software曾經因為他們最有名的[毀滅戰士和](../Page/毀滅戰士.md "wikilink")[德軍總部3D而常被社會當作爭議的話題](../Page/德軍總部3D.md "wikilink")。

### 毀滅戰士

毀滅戰士一直是因為遊戲內容參有許多[暴力](../Page/暴力.md "wikilink")﹐[血腥和](../Page/血腥.md "wikilink")[邪教的象征而觸怒了許多不同的團體](../Page/撒旦教.md "wikilink")。[Yahoo\!
Games把毀滅戰士列為史上前十個中最亂的遊戲](../Page/Yahoo!_Games.md "wikilink")。毀滅戰士因為邪神的象征已多次被宗教信仰團體批評。大衛·格羅斯曼稱毀滅戰士為“殺人模擬器”。因為毀滅戰士，[立體技術曾一度被認為會被用來模擬殺人的用途](../Page/虛擬現實.md "wikilink")。1994年的時候，華盛頓參議員菲爾·塔爾梅奇曾試著推廣立體技術需要強制授權。

毀滅戰士曾因[校園槍擊案而再次成為了社會話題](../Page/校園槍擊案.md "wikilink")。[科倫拜校園事件的埃里克](../Page/科倫拜校園事件.md "wikilink")·哈里斯和迪倫·克萊伯德曾狂熱毀滅戰士。哈里斯曾說此槍擊事件就會跟「玩毀滅戰士一樣」。哈里斯也提說他的霰彈槍「根本就是遊戲裡來的」。槍擊事件後有人傳說哈里斯曾經有在毀滅戰士裡做和科倫拜學校一樣的關卡﹐其關卡還有許多哈里斯的同學和老師在裡面。謠言也傳說哈里斯曾多次在此關卡練習他的犯案過程。雖然哈里斯曾經開發過毀滅戰士的關卡，但並不是和科倫拜學校一樣的關卡。

### 德軍總部3D

[Wolf3d_swastika.png](https://zh.wikipedia.org/wiki/File:Wolf3d_swastika.png "fig:Wolf3d_swastika.png")
德軍總部3D的爭議是落在採用卐字標誌和採用納粹黨的國歌[霍斯特·威塞爾之歌當作遊戲音樂](../Page/霍斯特·威塞爾之歌.md "wikilink")。電腦版的德軍總部3D在1994年的時候被慕尼黑地方法院的判決後在德國完全回收。因為在德國卐字標誌只能在教書的用途才能使用。[雅達利捷豹的主機版本也在蒂爾加滕地方法院的判決後被完全回收](../Page/Atari_Jaguar.md "wikilink")。

經過任天堂的注目，卐字標誌在[超級任天堂的德軍總部](../Page/超級任天堂.md "wikilink")3D版本被刪除。為了降低遊戲的暴力尺度，連血滴都被改為汗滴，狼狗也被改為大只的怪老鼠。id
Software的員工在“The Official DOOM Player
Guide”表示說為什麼可以開槍射人和老鼠可是不可以射狗？因此超級任天堂的德軍總部3D版本並沒有達到電腦版本的銷售程度。

## 作品列表

### 製作

  - [危險的戴夫](../Page/危險的戴夫.md "wikilink")（1988年）
  - [指揮官基恩](../Page/指揮官基恩.md "wikilink")
      - 一代: 在火星孤立無援（1990年）
      - 二代: 地球爆炸了（1991年）
      - 三代: 基恩必死（1991年）
      - 基恩夢遊記（1991年）
      - 四代: 神諭的秘密（1991年）
      - 五代: 大決戰的機器（1991年）
      - 六代: 外星人吃掉了我的保姆（1991年）
  - [危險的戴夫闖鬼屋](../Page/危險的戴夫闖鬼屋.md "wikilink")（1991年）
  - [急救探測器](../Page/急救探測器.md "wikilink")（1991年）
  - [急救探測器II](../Page/急救探測器II.md "wikilink")（1991年）
  - [影武士](../Page/影武士.md "wikilink")（1991年）
  - [立體氣墊坦克](../Page/立體氣墊坦克.md "wikilink")（1991年）
  - [地下皇陵3D:新的領域](../Page/地下皇陵3D.md "wikilink")（1991年） 新出版為地下皇陵3D:侵襲
  - [德軍總部3D](../Page/德軍總部3D.md "wikilink")（又名
    [重返狼穴3D](../Page/重返狼穴3D.md "wikilink")）（1992年）
      - [命運之矛(電腦遊戲)](../Page/命運之矛\(電腦遊戲\).md "wikilink")（1992年）
  - [毀滅戰士](../Page/毀滅戰士.md "wikilink")（1993年）
      - [究極毀滅戰士](../Page/究極毀滅戰士.md "wikilink")（1995年）\[8\]
  - [毀滅戰士II](../Page/毀滅戰士II.md "wikilink")（1994年）
      - [毀滅戰士II加強關卡](../Page/毀滅戰士II加強關卡.md "wikilink")（1995年）
      - [最後的毀滅戰士](../Page/最後的毀滅戰士.md "wikilink")（1996年）
  - [雷神之錘](../Page/雷神之锤_\(游戏\).md "wikilink")（1996年）
  - [id Software選集](../Page/id_Software選集.md "wikilink")（1996年）
  - [雷神之錘II](../Page/雷神之錘II.md "wikilink")（1997年）
  - [雷神之錘III競技場](../Page/雷神之錘III競技場.md "wikilink")（1999年）
      - [雷神之錘III:團隊競技場](../Page/雷神之錘III競技場.md "wikilink")（2000年）
  - [毀滅戰士:收藏版](../Page/毀滅戰士:收藏版.md "wikilink")（2001年）
  - [毀滅戰](../Page/毀滅戰.md "wikilink")（2004年）
  - [現場雷神之錘](../Page/Quake_Live.md "wikilink")（2009年 - 外測）
  - [經典德軍總部3D](../Page/德軍總部3D.md "wikilink")（2009年）\[9\]
  - [經典毀滅戰士](../Page/毀滅戰士.md "wikilink")（2009年）\[10\]
  - [狂怒煉獄﹕高清版](../Page/狂怒煉獄﹕高清版.md "wikilink")（2010年）\[11\]
  - [狂怒煉獄](../Page/狂怒煉獄.md "wikilink")（2011年）
  - [毀滅戰士3: BFG Edition](../Page/毀滅戰士3.md "wikilink")（2012年）
  - [毁灭战士](../Page/毁灭战士_\(2016年电子游戏\).md "wikilink")（2016年）
  - [狂怒煉獄2](../Page/狂怒煉獄2.md "wikilink")（TAB）

### 監製/出版

  - [異教徒](../Page/異教徒.md "wikilink") - Raven Software（1994年）
  - [毀滅巫師](../Page/毀滅巫師.md "wikilink") - Raven Software（1995年）
  - [毀滅巫師II](../Page/毀滅巫師II.md "wikilink") - Raven Software（1997年）
  - 烏黑之塔：超越異教徒和毀滅巫師（1997年）\[12\]
  - 雷神之錘加強版
      - 雷神之錘外加任務I阿瑪鋼的災害 - Ritual Entertainment（1997年）
      - 雷神之錘外加任務II永恆的解化 - Rogue Entertainment（1997年）
      - 雷神之錘：奉現
  - 雷神之錘II加強版
      - 冥想 - Gray Matter Interactive（1998年）
      - 災難現場 - Rogue Entertainment（1998年）
      - 雷神之錘II：四倍擊傷
  - [重返德軍總部](../Page/重返德軍總部.md "wikilink") - Gray Matter Interactive,
    Nerve Software（連線版）（2001年）
  - [德軍總部：敵軍領域](../Page/德軍總部:敵軍領域.md "wikilink") - Splash Damage（2003年）
  - [毀滅戰士III](../Page/毀滅戰士III.md "wikilink") - Nerve Software（2005年）
  - [雷神之錘4](../Page/雷神之錘4.md "wikilink") - Raven Software（2005年）
  - [毀滅戰士文字遊戲版](../Page/毀滅戰士文字遊戲版.md "wikilink") - Fountainhead
    Entertainment（2005年）
  - [魔獸與精靈](../Page/魔獸與精靈.md "wikilink") - Fountainhead
    Entertainment（2006年）
  - [深入敵後：雷神戰爭](../Page/深入敵後：雷神戰爭.md "wikilink") - Splash Damage（2007年）
  - [德軍總部文字遊戲版](../Page/德軍總部文字遊戲版.md "wikilink") - Electronic
    Arts（2008年）
  - [毀滅戰士重生](../Page/毀滅戰士重生.md "wikilink") - Escalation Studios（2009年）
  - [德軍總部：黑曜陰謀](../Page/德軍總部：黑曜陰謀.md "wikilink") - Raven Software（2009年）

## 外部連結

  - [id software](http://www.idsoftware.com/)

## 參考文獻

[i](../Category/美國電子遊戲公司.md "wikilink")
[i](../Category/電子遊戲開發公司.md "wikilink")
[Category:1991年開業電子遊戲公司](../Category/1991年開業電子遊戲公司.md "wikilink")
[Id_Software](../Category/Id_Software.md "wikilink") [Category:ZeniMax
Media](../Category/ZeniMax_Media.md "wikilink")

1.
2.
3.
4.
5.
6.  ["id Properties Coming to
    Wii"](https://web.archive.org/web/20070928001953/http://www.cubed3.com/news/6961/1/id_Properties_Coming_to_Wii)
    from Cubed3
7.
8.
9.  [Announcement of *Wolfenstein 3D
    Classic*](http://www.idsoftware.com/wolfenstein3dclassic/)  from
    official id home page
10. [Announcement of *Doom Classic*
    progress](http://www.idsoftware.com/iphone-doom-classic-progress/)
    from official id home page
11. from official id home page
12.