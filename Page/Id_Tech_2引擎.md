**id Tech
2引擎**，以前称为**雷神之锤II引擎**，是由开发的用于多种游戏的引擎，主要用于开发《[雷神之锤II](../Page/雷神之锤II.md "wikilink")》。自从其发布后就被多个游戏用于自己的开发。

id Tech
2引擎的最大特性就是对[硬件加速的](../Page/硬件加速.md "wikilink")[显卡进行了全方位的支持](../Page/显卡.md "wikilink")，并且在传统[软件渲染模式下优化了](../Page/软件渲染.md "wikilink")[OpenGL性能](../Page/OpenGL.md "wikilink")。之后产生的[雷神之锤III引擎则对此引擎进行了大刀阔斧的改动](../Page/雷神之锤III引擎.md "wikilink")，成为了新的引擎。

id Tech
2引擎的另外一个有趣的特性是对[动态链接库](../Page/动态链接库.md "wikilink")（[DLL](../Page/DLL.md "wikilink")）的支持。从而实现了同时支持软件和OpenGL渲染的方式，可以在载入/卸载不同链接库的时候进行切换。链接库的引入是有好处的，因为：

  - id可以发布源代码供游戏修改者修改，但是又保持了自己的特性
  - 因为使用了更原始的平台，而不是使用编译器对游戏进行编译，所以可以获得更快的运行速度。

id Tech
2引擎和它的前身一样使用了[BSP](../Page/BSP.md "wikilink")。这样一来对地图的光源的处理可以让光线数据在每个面上的情况都进行计算，然后再存储已经渲染好的图片，这种方法决定每个模型可以接受多少光源，而不是计算从多少方向来的光源。

[約翰·卡馬克在遵循](../Page/約翰·卡馬克.md "wikilink")[GNU和](../Page/GNU.md "wikilink")[GPL准则的情况下于](../Page/GPL.md "wikilink")2001年12月22日公布了此引擎的全部源代码。

## 使用id Tech 2引擎的遊戲

  - [雷神之鎚II](../Page/雷神之鎚II.md "wikilink")（Quake II）
  - [時空傳奇](../Page/時空傳奇.md "wikilink")（Anachronox）
  - [大刀](../Page/大刀_\(遊戲\).md "wikilink")（Daikatana）

<!-- end list -->

  - [命運戰士](../Page/命運戰士.md "wikilink")（Soldier of Fortune）

## 包含id Tech 2引擎代码的遊戲

  - [UFO: Alien Invasion](../Page/UFO:_Alien_Invasion.md "wikilink")
  - [Alien Arena](../Page/Alien_Arena.md "wikilink")
  - [Warsow](../Page/Warsow.md "wikilink")
  - [Egoboo](../Page/Egoboo.md "wikilink")

## 参考文献

## 外部链接

  - [使用Id Tech 2引擎的游戏](http://www.uvlist.net/groups/info/idtech2)
    (english)

[Category:1997年软件](../Category/1997年软件.md "wikilink")
[Category:游戏引擎](../Category/游戏引擎.md "wikilink")
[Category:雷神之锤系列](../Category/雷神之锤系列.md "wikilink")