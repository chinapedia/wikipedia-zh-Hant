**id Tech 4引擎**，以前称为**毁灭战士3引擎**，是個由[id
Software所開發的](../Page/id_Software.md "wikilink")[遊戲引擎](../Page/遊戲引擎.md "wikilink")，首度使用這個引擎的遊戲是電腦PC遊戲[毀滅戰士3](../Page/毀滅戰士3.md "wikilink")。這個遊戲引擎由[約翰·卡馬克](../Page/約翰·卡馬克.md "wikilink")（John
Carmack）領導設計，他同時也設計了《[雷神之鎚](../Page/雷神之鎚.md "wikilink")》遊戲所使用的引擎。

## 歷史

id Tech 4 引擎原先為[id Tech
3的加強版](../Page/id_Tech_3.md "wikilink")，接著計畫要重新改寫[渲染](../Page/渲染.md "wikilink")（Render）引擎，但仍然保留其它副系統的功能，例如檔案存取（File
Access）和記憶體管理（Memory
Management）。在渲染引擎完成後，他們決定要將引擎從[C語言改寫為](../Page/C語言.md "wikilink")[C++語言](../Page/C++.md "wikilink")，最後導致整個引擎重寫。現在所看到的id
Tech 4保有部份id Tech 3的程式碼，但大部分的程式碼皆已經被重寫。

id Tech 4
和他的舊引擎相同，最後將會[開放原始碼](../Page/開放源碼.md "wikilink")，在2007年的[QuakeCon](../Page/QuakeCon.md "wikilink")，引擎開發者約翰·卡馬克（John
Carmack）表示"I mean I won't commit to a date, but the Doom 3 stuff will be
open source."（我不會說出一個確切的釋出日期，但是毀滅戰士3引擎最後會開放原始碼。）\[1\]

## 特色

id Tech 4 在id Tech
3的基礎下，又增加了許多功能，例如[凸凹紋理映射](../Page/凸凹紋理映射.md "wikilink")（Bump
Mapping）、[法向映射](../Page/法向映射.md "wikilink")（Normal Mapping）和Specular
Highlighting。

遊戲引擎還可以進行動態像素打光（dynamic per-pixel
lighting）。先前的3D引擎皆是依賴在事先運算好的打光特效或是產生好的光影地圖（Lightmap）。即使是使用了動態光影也只能少部分的影響物體的明亮度。而這項技術讓光影變得更加真實\[2\]。

### MegaTexture技術

舊版本的毀滅戰士3引擎常被批評缺乏繪製大型戶外場景的功能，而[MegaTexture技術解決了這個問題](../Page/MegaTexture.md "wikilink")，透過繪製一個巨型的材質（32,768x32,768像素，新版本的MegaTexture可以支援更大的材質貼圖）覆蓋整個地形，並且儲存該地形的相關物理資料。這些物理資料可以告訴引擎你現在處在什麼環境，例如在石頭上行走和草地上行走的聲音將會有相當大的不同\[3\]。相對於現有使用「套用材質在地形上」的技術，MegaTexture也將讓地圖能夠呈現更精細的景觀。目前唯一使用MegaTexture技術的遊戲是[深入敵後：雷神戰争](../Page/深入敵後：雷神戰争.md "wikilink")。

## 使用id Tech 4引擎的遊戲

  - [毀滅戰士3](../Page/毀滅戰士3.md "wikilink")
      - [毀滅戰士3：邪惡復甦](../Page/毀滅戰士3：邪惡復甦.md "wikilink")（恶魔归来）
  - [雷神之錘4](../Page/雷神之錘4.md "wikilink")
  - [深入敵後：雷神戰争](../Page/深入敵後：雷神戰争.md "wikilink")
  - [獵魂](../Page/獵魂.md "wikilink")（Prey）
  - [德軍總部：黑曜陰謀](../Page/德軍總部：黑曜陰謀.md "wikilink")
  - [边缘战士](../Page/边缘战士.md "wikilink")
  - [獵魂2](../Page/獵魂2.md "wikilink")

## 內部連結

  - [id Tech 2](../Page/id_Tech_2.md "wikilink")
  - [id Tech 3](../Page/id_Tech_3.md "wikilink")
  - [id Tech 5](../Page/id_Tech_5.md "wikilink")
  - [遊戲引擎](../Page/遊戲引擎.md "wikilink")

## 參考資料

## 外部链接

[Category:2004年软件](../Category/2004年软件.md "wikilink")
[Category:游戏引擎](../Category/游戏引擎.md "wikilink")
[Category:毀滅戰士系列](../Category/毀滅戰士系列.md "wikilink")
[Category:雷神之锤系列](../Category/雷神之锤系列.md "wikilink")

1.
2.
3.