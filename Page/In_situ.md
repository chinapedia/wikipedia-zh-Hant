***In
situ***是一個[拉丁文片語](../Page/拉丁文.md "wikilink")，字面上的意思是指「在原本位置」，於不同領域中有不同用法，包括[航天學](../Page/航天學.md "wikilink")、[考古學](../Page/考古學.md "wikilink")、[建築學](../Page/建築學.md "wikilink")、[生物學](../Page/生物學.md "wikilink")、[法律](../Page/法律.md "wikilink")、[文學](../Page/文學.md "wikilink")、[天文學](../Page/天文學.md "wikilink")、[化學](../Page/化學.md "wikilink")、[計算機科學](../Page/計算機科學.md "wikilink")、[地球科學](../Page/地球科學.md "wikilink")、[环境学與](../Page/环境学.md "wikilink")[大氣科學等](../Page/大氣科學.md "wikilink")，皆有使用此片語。

## 天文學

在我們[銀河系中部分的](../Page/銀河系.md "wikilink")[球狀星團](../Page/球狀星團.md "wikilink")，以及其他大質量[星系中的球狀星團](../Page/星系.md "wikilink")，可能是原生的(*in
situ*)。其餘的可能是從現在已經瓦解的矮星系中滋生的。

## 生物學及醫學

在生物學[實驗上](../Page/實驗.md "wikilink")，*in
situ*是指進行於原發生位置的試驗（而不是將其移入特殊培養基中），有時意義大致介於*[in
vivo](../Page/in_vivo.md "wikilink")*與*[in
vitro](../Page/in_vitro.md "wikilink")*之間。當研究者對某一處於人工環境下的[器官中之特定](../Page/器官.md "wikilink")[細胞作實驗時](../Page/細胞.md "wikilink")，就可稱為*in
situ*。由於不在完整活體中，因此不是*in vivo*；也不是單純以人工環境下的細胞作實驗，因此也不是*in
vitro*。例如[分子生物學上一種用來測定特定核酸序列位置的](../Page/分子生物學.md "wikilink")[原位雜交](../Page/原位雜交.md "wikilink")（In
situ hybridization）技術，就是一種針對特定組織所作的*in situ*實驗。

而在[腫瘤學上](../Page/腫瘤學.md "wikilink")，*in
situ*可指任何一個並非從他處轉移（metastasize）或侵入（invade）的惡性腫瘤細胞，也就是處於原始腫瘤發生位置的腫瘤細胞，可發生於身體各處。

另外生態保育上的[原位保育](../Page/原位保育.md "wikilink")（In-situ
conservation），是一種於原生地點保育動植物的方式。

## 化学

化学中的in
situ（原位）是指“在反应过程中”。主要指的是在科学中实时的测试分析，将待测的目标置于原来的体系中进行检测，而不是单独的将某一目标分离出来使用单变量方式进行测定，或模拟条件体系进行检测。这样做可以最大限度的在有接近现实情况的条件下进行分析，尽可能的还原现状，得到准确的数据。化学研究和化工生产中很多情况下要对反应的中间产物进行分析研究，但由于中间产物的不稳定性和难以分离的特点，所以阻止了分析进程的继续，故而只能够采用原位技术。

## 环境学

在环境学中，原位是指在受污染地区就地进行对污染物的治理等，即现场治理。

## 參見

  - [原位癌](../Page/原位癌.md "wikilink")（*carcinoma in situ*）
  - *[ex vivo](../Page/ex_vivo.md "wikilink")*
  - *[in silico](../Page/in_silico.md "wikilink")*
  - *[in utero](../Page/in_utero.md "wikilink")*
  - *[in vitro](../Page/in_vitro.md "wikilink")*
  - *[in vivo](../Page/in_vivo.md "wikilink")*

## 参考资料

[Category:拉丁文單字及片語](../Category/拉丁文單字及片語.md "wikilink")
[Category:生物學與醫學拉丁文片語](../Category/生物學與醫學拉丁文片語.md "wikilink")
[Category:拉丁語法律術語](../Category/拉丁語法律術語.md "wikilink")