[Ic-photo-intel-D4004.png](https://zh.wikipedia.org/wiki/File:Ic-photo-intel-D4004.png "fig:Ic-photo-intel-D4004.png")
**4004**是[美國](../Page/美國.md "wikilink")[英特爾公司](../Page/英特爾公司.md "wikilink")
(Intel)
推出的第1款[微處理器](../Page/微處理器.md "wikilink")，也是全球第一款微處理器；1971年11月15日发布。

4004處理器的尺寸為3mm×4mm，外層有16隻針腳，內有2,300個[晶體管](../Page/晶體管.md "wikilink")，採用五层設計、[10微米製程](../Page/10微米製程.md "wikilink")。

4004最初的主频是108KHz，最高[時脈有](../Page/時脈.md "wikilink")740KHz，能執行[4位元運算](../Page/4位元.md "wikilink")，支援8位元指令集及12位元位址集，使用10.8[微秒和](../Page/微秒.md "wikilink")21.6微秒运行周期。当使用10.8微秒运行周期时，可以每秒运算9万次，成本低于100美元。4004處理器的性能與早期电子计算机[ENIAC相若](../Page/ENIAC.md "wikilink")。[ENIAC是在](../Page/電子數值積分計算機.md "wikilink")1946年推出，機器體積庞大，需占用一个房间。ENIAC擁有18000个[真空管](../Page/真空管.md "wikilink")。

該款處理器原先是為一家名為[Busicom的](../Page/Busicom.md "wikilink")[日本公司而設計](../Page/日本.md "wikilink")，用來生產[計算機](../Page/計算機.md "wikilink")。

## 外部連結

  - [Intel 4004官方页面](http://www.intel.com/museum/archives/4004.htm?iid=about+spot_4004)
  - [相关的图片和资料](http://www.intel.com/museum/archives/history_docs/index.htm#4004)
  - [简史](http://www.intel.com/pressroom/kits/quickrefyr.htm)
  - [Intel 4004非官方网站](http://www.4004.com/)
  - [Intel 4004内部结构图（PDF）](http://www.4004.com/assets/redrawn-4004-schematics-2006-11-12.pdf)
  - [Intel 4004的Java模拟器](http://www.4004.com/assets/4004-chipsim-demo-2006-11-13.zip)

[Category:微處理器](../Category/微處理器.md "wikilink")