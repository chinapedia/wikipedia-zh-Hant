**Intel 8088** 是一個 [Intel](../Page/Intel.md "wikilink") 以
[8086](../Page/Intel_8086.md "wikilink") 為基礎的
[微處理器](../Page/微處理器.md "wikilink")，擁有 16
[位元](../Page/位元.md "wikilink") [暫存器](../Page/暫存器.md "wikilink")
和 8 位元外部 [資料匯流排](../Page/資料匯流排.md "wikilink")。這是原本 [IBM
PC](../Page/IBM_PC.md "wikilink") 上所使用的處理器。

8088 使用 8 位元的設計，所針對的是較為經濟之系統。在它推出時候，大的資料匯流排寬度電路板還是相當地昂貴。8088
的預取(prefetch) 貯列(queue) 是 4 位元組，相對於 8086 的是 6 位元組。8088
的後代包括到現在還在使用的[intel
80188](../Page/intel_80188.md "wikilink")、[80288](../Page/80288.md "wikilink")（不再製造或使用）、和
[80388](../Page/80388.md "wikilink")
[微控制器](../Page/微控制器.md "wikilink")（microcontroller）。

到目前為止使用 8088 之最重要的微電腦是 [IBM PC](../Page/IBM_PC.md "wikilink")。原本的 PC
處理器是以 4.77 MHz 的時脈頻率執行。

顯然地 IBM 自家工程師想要使用 [Motorola
68000](../Page/Motorola_68000.md "wikilink")，並且它後來被用在已經遺忘的 IBM 儀器
[9000 實驗室電腦中](../Page/9000_實驗室電腦.md "wikilink")，但是 IBM 已經擁有製造 8086
家族的授權，以作為給 [Intel](../Page/Intel.md "wikilink")
[氣泡式記憶體](../Page/氣泡式記憶體.md "wikilink") 設計的授權之交換。使用 8 位元 8088
版本的一個因素是，它可以使用現有 [Intel 8085](../Page/Intel_8085.md "wikilink")
形態的元件，允許以修改 8085 設計的方式為基礎製造電腦。68000
的元件在當時並非廣泛可以得到，雖然可以使用 [Motorola
6800](../Page/Motorola_6800.md "wikilink") 元件來達成一種程度。Intel
的泡沫式記憶體在當時市場上推出有一段時間，但是由於從[日本公司來的低廉價格所產生的激烈競爭](../Page/日本.md "wikilink")，Intel
離開了記憶體市場並且專注在處理器上。

相容可以取代的晶片 [NEC V20](../Page/NEC_V20.md "wikilink") 是由
[NEC](../Page/NEC.md "wikilink") 所製造，改進了約 20 % 的功率改善。

[es:Intel 8086 y 8088\#Pines del
8088](../Page/es:Intel_8086_y_8088#Pines_del_8088.md "wikilink")

[Category:Intel x86处理器](../Category/Intel_x86处理器.md "wikilink")
[Category:1979年面世](../Category/1979年面世.md "wikilink")