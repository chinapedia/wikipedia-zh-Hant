**埃培智集团有限公司**（**Interpublic Group of Companies, Inc.
(IPG)**、中国大陆翻译为“埃培智市场咨询”），世界四大[广告](../Page/广告.md "wikilink")[传播集团之一](../Page/传播.md "wikilink")(其他三家为[宏盟集团](../Page/宏盟集团.md "wikilink")、[WPP集团和](../Page/WPP集团.md "wikilink")[阳狮集团](../Page/阳狮.md "wikilink"))，总部在[纽约市](../Page/纽约市.md "wikilink")。旗下拥有[万博宣伟公关](../Page/万博宣伟.md "wikilink")（Weber
Shandwick）、McCann-Erikson[麦肯环球广告](../Page/麦肯环球广告.md "wikilink")、Lowe\&Partners[灵狮广告](../Page/灵狮广告.md "wikilink")、FCB[博达大桥广告和优势麦肯](../Page/博达大桥广告.md "wikilink")、以及Initiative媒介等。\[1\]

Michael Roth是现任IPG[首席执行官](../Page/首席执行官.md "wikilink").

## 历史

1902年， Alfred W. Erickson 成立； 1911年， H.K. McCann 成立； 1930年，两家公司合并为
McCann-Erickson（麦肯公司现在的全称）。 1960年， McCann Erickson 在 Marion Harper Jr.
的带领下成为了股份公司，公司也改名为 Interpublic（IPG），并在1971年上市。 IPG
下一步就是寻找合适的收购对象，灵狮成为了第一个猎物。

1928年，[联合利华](../Page/联合利华.md "wikilink")（Unilever）下属的广告公司—— Lintas（Lever
International Advertising Services ，灵狮）成立。 Frank Lowe 在
1981年在[伦敦创建](../Page/伦敦.md "wikilink") Lowe 集团， 1990年该集团被
IPG 收购。 1999年， Lowe & Partners 与 Lintas 合并； 2000年改名为 Lowe Lintas &
Partners Worldwide ； 2002年1月，又改名为 Lowe & Partners Worldwide（励富）。

2001年3月，在经过了与 Havas 的一番角逐后， IPG 以 21 亿美元收购 True North 传播集团。世界广告史上的元老之一的
[FCB](../Page/FCB.md "wikilink")（[博达大桥广告](../Page/博达大桥广告.md "wikilink")）正式被纳入
IPG 的帐下。

## 参考

## 外部链接

  - [Interpublic.com](http://www.interpublic.com/) - 埃培智集团公司主页

[Category:跨国公司](../Category/跨国公司.md "wikilink")

1.