[Irem_Logo.png](https://zh.wikipedia.org/wiki/File:Irem_Logo.png "fig:Irem_Logo.png")
**Irem**（全稱 Innovation in Recreational Electronic
Media）**股份有限公司**是一家[日本](../Page/日本.md "wikilink")[电子游戏公司](../Page/电子游戏.md "wikilink")，其主要业务為开发及发行电子游戏。公司成立於1974年，起初名为IPM，並於1978年發行了第一款街机游戏。1979年正式更名为Irem，该公司迄今为止最成功的游戏是[R-TYPE](../Page/R-TYPE.md "wikilink")，中文译名《异型战机》。此外该公司还出品过一些比较成功的作品比如[Photoboy](../Page/Photoboy.md "wikilink")、[最後忍道](../Page/最後忍道.md "wikilink")、[海底大戰爭](../Page/海底大戰爭.md "wikilink")、[战火惊魂2](../Page/战火惊魂2.md "wikilink")。

1997年4月15日Irem软件工程公司成立并於7月接管了Irem股份有限公司。此后一些公司的成员离开该公司并组建了Nazca股份有限公司，后并入[SNK公司成为其一个团队](../Page/SNK.md "wikilink")，此团队的成功作品有[合金弹头系列](../Page/合金弹头系列.md "wikilink")。

## IREM公司歷年的作品

### 1979

  - Andromeda
  - Space Beam
  - Head On

### 1980

  - IPM Invader
  - Green Beret（中文名为"[綠色軍團](../Page/綠色軍團.md "wikilink")" 的Green
    Beret，是後期較多人認識由Konami 製作的橫向Action ，並非這個作品）
  - Sky Chuter
  - UniWar S / Gingateikoku No Gyakushu

### 1981

  - Demoneye-X
  - Oli-Boo-Chu (with GDI)/ Punching Kid
  - Red Alert (with GDI)

### 1982

  - Moon Patrol（授权给Williams公司发行）

### 1983

  - 10-Yard Fight
  - Traverse USA / Zippy Race / MotoRace USA（授权给Williams公司发行）
  - Tropical Angel

### 1984

  - The Battle Road
  - Kung-Fu Master（授权给Data
    East公司发行，在日本发行时使用的游戏名为"スパルタンX"）（中文名稱为"[成龍踢館](../Page/成龍踢館.md "wikilink")"）
  - Lode
    Runner（Broderbund公司授权发行）（中文名稱为"[淘金者](../Page/淘金者_\(遊戲\).md "wikilink")"）
  - Load Runner II - The Bungeling Strikes
    Back（Broderbund公司授权发行）（中文名稱为"淘金者2"）
  - Wily Tower

### 1985

  - Atomic Boy（授权给Memetron公司发行） (variant of Wily
    Tower)（中文名稱为"[原子男孩](../Page/原子男孩.md "wikilink")"）
  - Horizon
  - Kung Fu (Playchoice-10)（授权给任天堂公司发行）
  - Lode Runner III - The Golden Labyrinth (licensed from
    Broderbund)（中文名稱为"淘金者3"）
  - Lot Lot
  - Spelunker（Broderbund公司授权发行）

### 1986

  - Kid Niki - Radical Ninja / Kaiketsu Yanchamaru
  - Lode Runner IV - Teikoku Kara no Dasshutsu（中文名稱為"淘金者4"）
  - Spelunker II（Broderbund公司授权发行）
  - Youjyuden

### 1987

  - Battle Chopper / Mr. HELI no Dai-Bouken
  - R-Type（授权给任天堂美国分公司）（中文名稱為"[異形戰機](../Page/異形戰機.md "wikilink")"）

### 1988

  - Image Fight
  - Kickle Cubicle ()（中文名稱為"[迷宮島](../Page/迷宮島.md "wikilink")"）
  - Ninja Spirit / Saigo no
    Nindou（中文名稱為"[最後忍道](../Page/最後忍道.md "wikilink")"）
  - Vigilante（授权给Data East公司）

### 1989

  - Dragon Breed（中文名稱為"[神龍太子](../Page/神龍太子_\(遊戲\).md "wikilink")"）
  - Legend of Hero Tonma
  - R-Type II（中文名稱為"異形戰機2"）
  - X-Multiply

### 1990

  - Air Duel
  - Hammerin' Harry / Daiku no
    Gensan（中文名稱為"[大力工頭](../Page/大力工頭.md "wikilink")"）
  - Major Title
  - Pound for Pound

### 1991

  - Blade Master
  - Cosmic Cop / Gallop - Armed Police Unit
  - Tumble pop（中文名稱為"[捉鬼大師](../Page/捉鬼大師.md "wikilink")"）)
  - Bomber Man / Dynablaster / Atomic Punk（Hudson Soft公司授权发行）
  - Gunforce - Battle Fire Engulfed Terror Island
  - Hasamu
  - Ken-Go
  - Lethal Thunder / Thunder Blaster

### 1992

  - Bomber Man World / New Dyna Blaster - Global Quest / New Atomic
    Punk-Global Quest（Hudson Soft公司授權發行）
  - Hook（中文名为"[鐵鈎船長](../Page/鐵鈎船長_\(遊戲\).md "wikilink")"）
  - Major Title 2 - Tournament Leader / The Irem Skins Game
  - Mystic Riders / Gun Hohki
  - Quiz F-1
  - R-Type Leo
  - Skins Game（为超级任天堂平台制作）
  - Undercover Cops（中文名"[暴力刑警](../Page/暴力刑警.md "wikilink")"）

### 1993

  - Fire Barrel
  - In The Hunt / Kaitei
    Daisensou（中文名为"[海底大戰爭](../Page/海底大戰爭.md "wikilink")"）
  - Ninja Baseball Batman / Yakyuu Kakutou
    League-Man（中文名为"[棒球忍者](../Page/棒球忍者.md "wikilink")"）
  - Risky Challenge
  - Perfect Soldiers / Superior Soldiers

### 1994

  - Dream Soccer '94（授权给Data East公司）
  - Gunforce 2 / Geostorm（中文名"[战火惊魂2](../Page/战火惊魂2.md "wikilink")"）
  - Joe\&Mac return（中文名"[戰鬥原始人](../Page/戰鬥原始人.md "wikilink")"）
  - R-Type III - The 3rd Lightning（为超级任天堂平台制作）

### 1999

  - R-Type Delta（中文名为"异型战机 Delta"）（为Sony Playstation平台制作）

### 2004

  - 桜坂消防隊（PS2）
  - R-Type Final（中文名为"异型战机最终版"）（为Sony Playstation 2平台制作）

### 2007

  - R-Type Tactics（中文名为"[異形戰機戰略版](../Page/異形戰機戰略版.md "wikilink")"）

### 2008

  - 夕陽大工物語（PSP）
  - 浪漫大活劇（PSP）
  - 戰國繪札遊戯 不如歸 -HOTOTOGISU- 乱（PSP）

### 2009

  - 絶体絶命都市3（PSP）
  - 乳臭未乾英雄譚-太陽和月亮的故事 （PSP）
  - 異形戰機戰略版2
  - [地下冒險](../Page/地下冒險.md "wikilink") XP版

## 外部链接

  -

  - [translated version of Japanese-language IREM Webpage
    Menu](http://translate.google.com/translate?hl=en&sl=ja&u=http://www.irem.co.jp/menu/index.html&sa=X&oi=translate&resnum=2&ct=result&prev=/search%3Fq%3Direm%2Bhomepage%2Bmenu%26hl%3Den%26sa%3DG)

  - [Irem公司历史](https://web.archive.org/web/20090812060445/http://www.jap-sai.com/Games/Irem/Irem.htm)
    at Jap-Sai.com

  - [*Irem*](http://www.mobygames.com/company/irem-software-engineering-inc)在
    [MobyGames](../Page/MobyGames.md "wikilink") 的档案

  - [Irem@Wiki](https://web.archive.org/web/20081020180152/http://www.erc-j.com/irem_wiki/)
    Irem Games (Japanese)

[Category:1974年開業電子遊戲公司](../Category/1974年開業電子遊戲公司.md "wikilink")
[Category:1997年成立的公司](../Category/1997年成立的公司.md "wikilink")
[Category:日本電子遊戲公司](../Category/日本電子遊戲公司.md "wikilink")
[Category:電子遊戲開發公司](../Category/電子遊戲開發公司.md "wikilink")
[Category:電子遊戲發行商](../Category/電子遊戲發行商.md "wikilink")
[Irem游戏](../Category/Irem游戏.md "wikilink")