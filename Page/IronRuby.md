**IronRuby**是[Ruby程式語言在微軟](../Page/Ruby.md "wikilink").NET平台上的一種實作。

IronRuby與Wilco Bauwer的IronRuby project同名\[1\]
2007年4月30日由微軟於[MIX上發表](../Page/MIX_\(Microsoft\).md "wikilink")。並計劃於2007年的[OSCON公開推出](../Page/O'Reilly_Open_Source_Convention.md "wikilink")。\[2\].

2007年8月31日，John Lam與 DLR設計團隊在RubyForge上推出pre-alpha版\[3\]。

## 注釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [S. Somasegar's blog entry announcing
    IronRuby](http://blogs.msdn.com/somasegar/archive/2007/04/30/mix-07-silverlight-shines-brighter.aspx)
  - [John Lam's IronRuby blog
    entry](https://web.archive.org/web/20071009213434/http://www.iunknown.com/2007/04/introducing_iro.html#comments)
  - [John Lam's IronRuby release
    blog](https://web.archive.org/web/20070726181932/http://www.iunknown.com/2007/07/a-first-look-at.html)
  - [IronRuby home page](http://www.ironruby.net)

[I](../Category/.NET程式語言.md "wikilink")
[I](../Category/Ruby程式語言.md "wikilink")

1.
2.
3.