《**J-Top
冠軍精選**》是臺灣歌手[蔡依林的第四張](../Page/蔡依林.md "wikilink")[精選輯](../Page/精選輯.md "wikilink")，由[新力博德曼於](../Page/台灣索尼音樂娛樂.md "wikilink")2006年5月5日發行。該專輯內容包括蔡依林於新力博德曼時期的23首歌曲、4支音樂錄影帶與1部紀錄片\[1\]。

## 曲目

## 音樂錄影帶

| 歌曲名稱                                                 | 導演                               | 附註       |
| ---------------------------------------------------- | -------------------------------- | -------- |
| [我要的選擇](https://www.youtube.com/watch?v=8Lv-q0qkA44) | 金卓                               | MV出演：邱鈺玲 |
| [衣服占心術](https://www.youtube.com/watch?v=1QBgRDgWK_Q) | [比爾賈](../Page/比爾賈.md "wikilink") |          |

## 參考資料

## 外部連結

  -
[Category:蔡依林合輯](../Category/蔡依林合輯.md "wikilink")
[Category:台灣索尼唱片音樂專輯](../Category/台灣索尼唱片音樂專輯.md "wikilink")
[Category:2006年合輯](../Category/2006年合輯.md "wikilink")

1.  [蔡依林 (Jolin Tsai) - Jolin J-Top冠軍精選 專輯 -
    KKBOX](https://www.kkbox.com/tw/tc/album/c6tgH57WCuHQrkw0FGGB008l-index.html)