**J**，**j**，是[拉丁字母中的第](../Page/拉丁字母.md "wikilink")10个[字母](../Page/字母.md "wikilink")。它最初只作为一个[大写字母](../Page/大写字母.md "wikilink")，因此一些德语使用者以及一些[意大利人现在仍把他们的名字](../Page/意大利.md "wikilink")，例如Isabel、Ines，拼写为Jsabel、
Jnes。在一些古写法中，人们仍会把 J 作为 I 的大写字母。

[人文主义学者Pierre](../Page/人文主义.md "wikilink") de la
Ramée（1515年－1572年）是第一个指明 I 和 J 之间差别的学者。开始的时候，I 和
J 都发作 \[i\]、\[i:\] 以及 \[j\]。但是罗曼语族语言產生了一些新的发音（从先前的 \[j\] 和 \[g\]
中产生），使得它们分别用 I 和 J 来表示。因此，英文中的 I 和 J 的发

在其它一些[日耳曼语言中](../Page/日耳曼语言.md "wikilink")，J 表示/j/。

在现代标准[意大利语中](../Page/意大利语.md "wikilink")，只有外来语或[拉丁语单词含有](../Page/拉丁语.md "wikilink")
J。直到19世纪，J 在双元音中代替 I，作为结尾形式“-ii”的替代写法，或者在一组元音字母中。这个规则在政府公文中有很严格的要求。J
也用来书写方言的发音，表示/j/，例如罗马式的“ajo”代替标准式的“aglio”。

在[土耳其语](../Page/土耳其语.md "wikilink")、[阿塞拜疆语和](../Page/阿塞拜疆语.md "wikilink")[鞑靼语中](../Page/鞑靼语.md "wikilink")，J
发作\[ʒ\]。

## 字符编码

| 字符编码                               | [ASCII](../Page/ASCII.md "wikilink") | [Unicode](../Page/Unicode.md "wikilink") | [EBCDIC](../Page/EBCDIC.md "wikilink") | [摩斯电码](../Page/摩斯电码.md "wikilink") |
| ---------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- | ---------------------------------- |
| [大写](../Page/大写字母.md "wikilink") J | 74                                   | 004A                                     | 209                                    | `·---`                             |
| [小写](../Page/小写字母.md "wikilink") j | 106                                  | 006A                                     | 145                                    |                                    |

## 其他表示方法

### 其他字母中的相近字母

  - <span lang="en" style="font-size:120%;"></span>（不带点的小写j）
  - <span lang="ru" style="font-size:120%;">[Й
    й](../Page/Й.md "wikilink")</span>（[西里尔字母](../Page/西里尔字母.md "wikilink")）
  - <span lang="ru" style="font-size:120%;">[Ј
    ј](../Page/Ј.md "wikilink")</span>（西里尔字母
    Je，只在[塞尔维亚语及](../Page/塞尔维亚语.md "wikilink")[马其顿语使用](../Page/马其顿语.md "wikilink")）
  - <span lang="el" style="font-size:120%;">[Ϳ
    ϳ](../Page/Ϳ.md "wikilink")</span>（[希臘字母](../Page/希臘字母.md "wikilink")）

[Category:拉丁字母](../Category/拉丁字母.md "wikilink")