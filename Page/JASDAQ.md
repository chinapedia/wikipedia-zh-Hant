|                                                      |
| :--------------------------------------------------: |
|                      **JASDAQ**                      |
|                         ****                         |
|                          概要                          |
|                       **發行日期**                       |
|   **[識別代碼](../Page/:en:ISO_10383.md "wikilink")**    |
|     **[FX識別代碼](../Page/金融資訊交換協定.md "wikilink")**     |
|                        交易所資訊                         |
|                      **交易所全銜**                       |
| **[交易所BIC識別代碼](../Page/:en:ISO_9362.md "wikilink")** |

**JASDAQ**（）是[東京證券交易所所營運的股票](../Page/東京證券交易所.md "wikilink")[證券市場](../Page/證券市場.md "wikilink")，主要以[新興企業為取向](../Page/新興企業.md "wikilink")。過去曾由同名的Jasdaq證券交易所負責營運。

Jasdaq證券交易所（，）是過去位于[東京都](../Page/東京都.md "wikilink")[中央區](../Page/中央區_\(東京都\).md "wikilink")[日本橋茅場町一丁目](../Page/日本橋.md "wikilink")（[東京證券會館内](../Page/東京證券會館.md "wikilink")）的一家[證券交易所](../Page/證券交易所.md "wikilink")。該證券交易所有日本的[納斯達克之稱](../Page/納斯達克.md "wikilink")；不過，與[NASDAQ日本](../Page/NASDAQ日本.md "wikilink")（後來改稱，即Hercules）無關，也與美國納斯達克無任何關係。

Jasdaq公司由在成立，當時名稱為**日本店頭證券**公司（****），專門經營店頭有價證券買賣業務。1998年實施，同年引入[造市商制度](../Page/造市商.md "wikilink")，公司名稱改為Jasdaq
Service公司（）。2001年公司名稱再改為Jasdaq公司（，）。2004年轉型為Jasdaq證券交易所（）。

2010年4月1日，與[大阪證券交易所合併成為交易市場一環](../Page/大阪證券交易所.md "wikilink")，公司法人消滅。2013年7月16日，隨著大阪證交所股票交易市場一起併入[東京證交所](../Page/東京證交所.md "wikilink")。

## 參見

  - [大阪證券交易所](../Page/大阪證券交易所.md "wikilink")
  - [東京證券交易所](../Page/東京證券交易所.md "wikilink")

<!-- end list -->

  - 另見

<!-- end list -->

  - [KOSDAQ](../Page/KOSDAQ.md "wikilink")

## 外部連結

  - [JASDAQ證券交易所](http://www.jasdaq.co.jp/)
  - [JASDAQ](http://jasdaq.tse.or.jp/)

[Category:1976年成立的公司](../Category/1976年成立的公司.md "wikilink")
[Category:2010年結業公司](../Category/2010年結業公司.md "wikilink")
[Category:日本證券交易所](../Category/日本證券交易所.md "wikilink")
[Category:日本公司](../Category/日本公司.md "wikilink")