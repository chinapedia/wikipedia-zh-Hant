**JET綜合台**，是[年代電視的旗下頻道](../Page/年代電視.md "wikilink")，前身是於1996年成立於[新加坡Asia](../Page/新加坡.md "wikilink")
Broadcast
Center，過去為[台灣三個專門提供](../Page/台灣.md "wikilink")[日本](../Page/日本.md "wikilink")[電視節目的電視](../Page/電視節目.md "wikilink")[頻道之一](../Page/頻道.md "wikilink")；2010年元旦起更名。

由[日本](../Page/日本.md "wikilink")[住友商事發起](../Page/住友商事.md "wikilink")，加上[台灣](../Page/台灣.md "wikilink")**衛星娛樂傳播股份有限公司**（Satellite
Entertainment Communication Co., Ltd.；簡稱SEC），結合日本、台灣兩地股東注資。

## 歷史

[logojettv.jpg](https://zh.wikipedia.org/wiki/File:logojettv.jpg "fig:logojettv.jpg")
[JET_TV_black_baseball_cap_20150420.jpg](https://zh.wikipedia.org/wiki/File:JET_TV_black_baseball_cap_20150420.jpg "fig:JET_TV_black_baseball_cap_20150420.jpg")

  - 1996年10月，JET日本台成立於新加坡Asia Broadcast
    Center，SEC成立於[台北市](../Page/台北市.md "wikilink")。
  - 1997年1月，JET日本台正式於台灣首播，統一以「JET TV」名稱對外，SEC為總代理。
  - 1997年3月，JET日本台於[東南亞](../Page/東南亞.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[泰國首播](../Page/泰國.md "wikilink")，提供[英語](../Page/英語.md "wikilink")、[泰語](../Page/泰語.md "wikilink")、[華語配音服務](../Page/華語.md "wikilink")。
  - 1998年，JET日本台於[香港正式首播](../Page/香港.md "wikilink")。
  - 1999年2月，[住友商事正式與SEC簽訂合約](../Page/住友商事.md "wikilink")，將JET日本台經營權轉讓給SEC。
  - 1999年6月底，JET日本台新加坡公司正式宣佈關閉。
  - 1999年11月，JET日本台於[北美地區透過](../Page/北美.md "wikilink")[東森美洲台播出](../Page/東森美洲衛視.md "wikilink")。
  - 2000年2月，JET日本台於[中國大陸地區取得落地審批](../Page/中國大陸.md "wikilink")，正式播出。
  - 2001年10月，JET日本台於新加坡、[澳洲正式播出](../Page/澳洲.md "wikilink")。
  - 2002年6月，JET日本台於馬來西亞正式復播。
  - 2003年4月，JET日本台於中國大陸停播。
  - 2003年10月，JET日本台於香港正式復播。
  - 2009年12月31日，JET日本台發出[新聞稿](../Page/新聞稿.md "wikilink")，決定於2010年1月4日轉型為綜合台並播放[超視原有之兩個節目](../Page/超級電視台.md "wikilink")。
  - 2010年1月1日，JET日本台改名為JET綜合台，播出[超視](../Page/超級電視台.md "wikilink")《[新聞挖挖哇](../Page/新聞挖挖哇.md "wikilink")》、《[命運好好玩](../Page/命運好好玩_\(電視節目\).md "wikilink")》與全新製作的台灣綜藝節目。
  - 2017年4月，啟用高畫質版本「**JET綜合HD**」。

## 海外可見的國家與地區

  - [新加坡](../Page/新加坡.md "wikilink")：1996年10月開播(至2011/12/29)。
  - [台灣](../Page/台灣.md "wikilink")：1997年1月開播、2012/8/22 - 2012/9/12(重播)。
  - [泰國](../Page/泰國.md "wikilink")：1997年3月開播。
  - [香港](../Page/香港.md "wikilink")：1998年開播。
  - [北美地區](../Page/北美.md "wikilink")：1999年11月。
  - [澳洲](../Page/澳大利亚.md "wikilink")：2001年10月。
  - [馬來西亞](../Page/馬來西亞.md "wikilink")：2002年6月。

## 播放狀況

主要在臺灣的有線電視頻道播出，另有以下地區的播映。

### 亞太地區

1997年3月，於東南亞、馬來西亞、泰國首播(提供英語、泰語、中文配音服務)。2001年10月，於新加坡、澳洲正式播出。2002年6月，在馬來西亞正式播出。2004年起，於泰國試播。

#### 香港

1998年於香港首播，2003年正式播出，曾經是[香港有線電視其中一個收費頻道](../Page/香港有線電視.md "wikilink")，但於2018年10月1日起停播。

### 北美洲

1999年10月起，透過東森美洲台於北美地區播出。2002年7月1日，[陽光文化網絡電視控股有限公司宣佈](../Page/陽光文化網絡電視控股有限公司.md "wikilink")，北美觀眾能透過[Dish
Network和](../Page/Dish_Network.md "wikilink")[AT\&T網絡收看JET綜合台](../Page/AT&T.md "wikilink")。

### 中國大陸

2000年2月，取得落地審批；2003年3月6日，[陽光衛視宣佈計劃與](../Page/陽光衛視.md "wikilink")[東森和JET日本台合組頻道管理公司](../Page/東森電視.md "wikilink")\[1\]，為陽光衛視每天提供部分節目，如《搶救貧窮大作戰》等。但2003年4月1日，中國國家廣電總局下達禁播令，宣布「即時起，暫停香港的陽光衛視與JET日本台兩家電視台在內地的播放權」，也就是說，JET綜合台目前無法在[中國大陸落地播放](../Page/中國大陸.md "wikilink")。

## 節目

### 播出中

### 自製綜藝節目

<table>
<thead>
<tr class="header">
<th><p>節目名稱</p></th>
<th><p>播出時間</p></th>
<th><p>主持人</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><center>
<p>《<a href="../Page/料理美食王.md" title="wikilink">料理美食王</a>》精華版</p></td>
<td><p>週一至週五上午10:00-11:00</p></td>
<td><p><a href="../Page/焦志方.md" title="wikilink">焦志方</a></p></td>
</tr>
<tr class="even">
<td><center>
<p>《<a href="../Page/女人要有錢之武花教主.md" title="wikilink">女人要有錢之武花教主</a>》</p></td>
<td><p>週六至週日21:30-22:30</p></td>
<td><p><a href="../Page/謝祖武.md" title="wikilink">謝祖武</a>、<a href="../Page/張嘉雲.md" title="wikilink">花花</a></p></td>
</tr>
<tr class="odd">
<td><center>
<p>《<a href="../Page/新聞挖挖哇.md" title="wikilink">新聞挖挖哇</a>》</p></td>
<td><p>週一至週五20:00-21:30</p></td>
<td><p><a href="../Page/鄭弘儀.md" title="wikilink">鄭弘儀</a></p></td>
</tr>
<tr class="even">
<td><center>
<p>《<a href="../Page/命運好好玩_(電視節目).md" title="wikilink">命運好好玩</a>》</p></td>
<td><p>週一至週五21:30-23:00</p></td>
<td><p><a href="../Page/何篤霖.md" title="wikilink">何篤霖</a>、<a href="../Page/郭靜純.md" title="wikilink">郭靜純</a></p></td>
</tr>
<tr class="odd">
<td><center>
<p>《<a href="../Page/寶貝小廚神.md" title="wikilink">寶貝小廚神</a>》</p></td>
<td><p>週六11:00-12:00</p></td>
<td><p><a href="../Page/李宣萱.md" title="wikilink">李宣萱</a></p></td>
</tr>
<tr class="even">
<td><center>
<p>《<a href="../Page/那些年我們的青春旋律.md" title="wikilink">那些年我們的青春旋律</a>－余茵繞樑》</p></td>
<td><p>週六至週日下午12:00-13:00</p></td>
<td><p><a href="../Page/朱衛茵.md" title="wikilink">朱衛茵</a>、<a href="../Page/余光_(音樂人).md" title="wikilink">余光</a></p></td>
</tr>
</tbody>
</table>

### 電視音樂特輯

<table>
<thead>
<tr class="header">
<th><p>節目名稱</p></th>
<th><p>播出時間</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><center>
<p>《<a href="../Page/電視音樂特輯彭佳慧.md" title="wikilink">電視音樂特輯彭佳慧</a>》</p></td>
<td><p>07:00-07:30</p></td>
</tr>
<tr class="even">
<td><center>
<p>《<a href="../Page/電視音樂特輯_武神.md" title="wikilink">電視音樂特輯 武神</a>》</p></td>
<td><p>17:00-18:00</p></td>
</tr>
</tbody>
</table>

### 戲劇

<table>
<thead>
<tr class="header">
<th><p>韓劇名稱</p></th>
<th><p>播出時間</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><center>
<p>MBC/《<a href="../Page/武神_(韓國電視劇).md" title="wikilink">武神</a>》</p></td>
<td><p>11月24日起週一至週五19:00-20:00</p></td>
</tr>
</tbody>
</table>

## 曾播放之日本節目

過去節目以日本[綜藝節目和動畫為主](../Page/綜藝節目.md "wikilink")，2005年11月起增設日劇時段。

### 綜藝節目

| 節目名稱                                                                                                         |
| ------------------------------------------------------------------------------------------------------------ |
| [TBS電視台](../Page/TBS電視台.md "wikilink")《》（；已播畢）                                                               |
| [日本電視台](../Page/日本電視台.md "wikilink")《[超級變變變](../Page/超級變變變.md "wikilink")》（）                                 |
| TBS電視台《》（；已播畢）                                                                                               |
| [MBS電視台](../Page/每日放送.md "wikilink")《》（；最初播出時譯名為《價格知多少》，已停播）                                                 |
| [東京電視台](../Page/東京電視台.md "wikilink")《》（；後另名《賺錢達人GoGoGo》，已停播）                                                 |
| TBS電視台《[挑戰冠軍王](../Page/挑戰冠軍王.md "wikilink")》()                                                               |
| TBS電視台《[百戰百勝王](../Page/百戰百勝王.md "wikilink")》()(已播畢)                                                          |
| [朝日電視台](../Page/朝日電視台.md "wikilink")《[辣妹圍裙](../Page/辣妹圍裙.md "wikilink")》()(已停播)                              |
| 朝日電視台《[阿Q猜謎王](../Page/阿Q猜謎王.md "wikilink")》({{lang|ja|{{link-ja|クイズプレゼンバラエティー Qさま                            |
| [東京電視台](../Page/東京電視台.md "wikilink")《》（；已於2006年3月播畢）                                                         |
| 日本電視台《[狗狗猩猩大冒險](../Page/狗狗猩猩大冒險.md "wikilink")》()(已於2007年2月播映完畢)                                             |
| TBS電視台《[極限體能王](../Page/極限體能王SASUKE.md "wikilink")》()                                                         |
| [富士電視台](../Page/富士電視台.md "wikilink")《[富士海筋肉王](../Page/富士海筋肉王.md "wikilink")》()                               |
| 日本電視台《[世界紀錄王](../Page/世界紀錄王.md "wikilink")》()(已播畢)                                                           |
| [讀賣電視台](../Page/讀賣電視台.md "wikilink")《[花的料理人](../Page/花的料理人.md "wikilink")》()(已播畢)                            |
| 日本電視台《[戀愛特訓班](../Page/戀愛特訓班.md "wikilink")》()(已播畢)                                                           |
| 富士電視台《[老天幫幫忙](../Page/老天幫幫忙.md "wikilink")》()(已播畢)                                                           |
| 朝日電視台《[川口浩叢林冒險王](../Page/川口浩叢林冒險王.md "wikilink")》()(已播畢)、《[藤岡弘叢林冒險王](../Page/藤岡弘叢林冒險王.md "wikilink")》()(已播畢) |
| 朝日電視台《[超級奶爸奮鬥記](../Page/超級奶爸奮鬥記.md "wikilink")》()(已移至[國興衛視不定期播出](../Page/國興衛視.md "wikilink"))                |
| 東京電視台《[逛街天國](../Page/逛街天國.md "wikilink")》()                                                                  |
| [BS朝日](../Page/BS朝日.md "wikilink")《[結婚奮鬥記](../Page/結婚奮鬥記.md "wikilink")》()                                   |
| 朝日電視台《[超級挑戰](../Page/超級挑戰.md "wikilink")》()                                                                  |
| TBS電視台《[超級運動王](../Page/超級運動王.md "wikilink")》({{lang|ja|{{link-ja|スポーツマンNo.1決定戦|最強の男は誰だ\!壮絶筋肉バトル              |
| 富士電視台《[世界衝擊映像社](../Page/世界衝擊映像社.md "wikilink")》()                                                            |

### 日劇

JET日本台曾在[週一至](../Page/週一.md "wikilink")[週五下午](../Page/週五.md "wikilink")4:00播出過的[連續劇](../Page/連續劇.md "wikilink")：

| 電視劇名稱                                                                        |
| ---------------------------------------------------------------------------- |
| [NHK](../Page/NHK.md "wikilink")《[阿信](../Page/阿信_\(電視劇\).md "wikilink")》     |
| NHK《[請問芳名](../Page/請問芳名.md "wikilink")》                                      |
| [東海電視台](../Page/東海電視台.md "wikilink")《[牡丹與薔薇](../Page/牡丹與薔薇.md "wikilink")》() |
| 東海電視台《[冬之輪舞](../Page/冬之輪舞.md "wikilink")》()                                  |
| TBS電視台《[砂時計](../Page/砂時計_\(日本電視劇\).md "wikilink")》                           |

JET日本台的[日劇時段](../Page/日本電視劇.md "wikilink")《超人氣偶像劇》，於星期一至五22:00([台灣時間](../Page/台灣時間.md "wikilink"))播出。

| 電視劇名稱                                                                                    |
| ---------------------------------------------------------------------------------------- |
| [TBS電視台](../Page/TBS電視台.md "wikilink")《[Orange Days](../Page/Orange_Days.md "wikilink")》 |
| [朝日電視台](../Page/朝日電視台.md "wikilink")《[蜜桃17](../Page/蜜桃17.md "wikilink")》                 |
| TBS電視台《》                                                                                 |
| [東京電視台](../Page/東京電視台.md "wikilink")《[第一女公關](../Page/孃王.md "wikilink")》                  |
| 朝日電視台《》                                                                                  |
| [富士電視台](../Page/富士電視台.md "wikilink")《》                                                   |
| 富士電視台《[一公升的眼淚](../Page/一公升的眼淚_\(電視劇\).md "wikilink")》                                    |
| [DVD偶像劇](../Page/DVD.md "wikilink")《》                                                    |
| [日本電視台](../Page/日本電視台.md "wikilink")《[唯一的愛](../Page/唯一的愛.md "wikilink")》                 |
| 朝日電視台《[女排No.1](../Page/排球甜心.md "wikilink")》                                              |
| TBS電視台《\[\[夢想飛行Good_Luck|Good Luck                                                      |
| 富士電視台《[秀逗小護士4](../Page/秀逗小護士.md "wikilink")》                                             |
| 富士電視台《[海猿](../Page/海猿.md "wikilink")》                                                    |
| 富士電視台《[大和拜金女](../Page/大和拜金女.md "wikilink")》                                              |
| 富士電視台《[庶務二課](../Page/庶務二課.md "wikilink")》                                                |
| 日本電視台《》                                                                                  |
| 富士電視台《[東京鐵塔](../Page/東京鐵塔：我的母親父親.md "wikilink")》                                         |
| 富士電視台《》                                                                                  |
| 日本電視台《[有閑俱樂部](../Page/有閑俱樂部.md "wikilink")》                                              |
| 日本電視台《[偵探學園Q](../Page/偵探學園Q.md "wikilink")》                                              |
| TBS電視台《[Love Story](../Page/Love_Story.md "wikilink")》                                   |
| 日本電視台《》                                                                                  |
| 富士電視台《[將太的壽司](../Page/將太的壽司.md "wikilink")》                                              |
| 富士電視台《》                                                                                  |
| TBS電視台《[美人](../Page/美人_\(日本電視劇\).md "wikilink")》                                         |
| 日本電視台《[考試之神](../Page/考試之神.md "wikilink")》                                                |
| 富士電視台《[富家女與窮酸郎](../Page/名人與貧乏太郎.md "wikilink")》                                          |
| TBS電視台《\[\[至愛寶貝～天使少女的育兒日記～|DAISUKI 最喜歡                                                   |
| 富士電視台《》                                                                                  |
| 富士電視台《[戀愛世代](../Page/戀愛世代.md "wikilink")》                                                |
| 富士電視台《[美女或野獸](../Page/美女或野獸.md "wikilink")》                                              |

### 電影

| 電影名稱                               |
| ---------------------------------- |
| [跳水男孩](../Page/跳水男孩.md "wikilink") |

### 動畫

  - 《卡通館》時段

| 卡通名稱                                     |
| ---------------------------------------- |
| [小松君](../Page/阿松_\(漫畫\).md "wikilink")   |
| [小天使](../Page/小天使.md "wikilink")         |
| [一休和尚](../Page/一休和尚.md "wikilink")       |
| [平成天才](../Page/天才妙老爹.md "wikilink")      |
| [十二國記](../Page/十二國記.md "wikilink")       |
| [神秘花園](../Page/神秘花園.md "wikilink")       |
| [天地無用\! 魎皇鬼](../Page/天地無用.md "wikilink") |
| [魔法少女砂沙美](../Page/魔法少女砂沙美.md "wikilink") |

  - 《美少女動畫館》時段

| 卡通名稱                                            |
| ----------------------------------------------- |
| [青出於藍](../Page/青出於藍_\(漫畫\).md "wikilink")       |
| [D.C. ～戀愛學園～](../Page/初音島.md "wikilink")        |
| [圓盤皇女](../Page/圓盤皇女.md "wikilink")              |
| [迷糊天使](../Page/迷糊天使.md "wikilink")              |
| [愛的魔法](../Page/愛的魔法.md "wikilink")              |
| [宇宙學園](../Page/宇宙學園.md "wikilink")              |
| [瓶詰妖精](../Page/瓶詰妖精.md "wikilink")              |
| [銀河天使](../Page/銀河天使.md "wikilink")              |
| [真實夢境](../Page/真實夢境.md "wikilink")              |
| [爆彈小新娘](../Page/爆彈小新娘.md "wikilink")            |
| [成惠的世界](../Page/成惠的世界.md "wikilink")            |
| [魔力女管家](../Page/魔力女管家.md "wikilink")            |
| [星空的邂逅](../Page/星空的邂逅.md "wikilink")            |
| [星空的邂逅2](../Page/星空的邂逅2.md "wikilink")          |
| [青春草莓蛋](../Page/青春草莓蛋.md "wikilink")            |
| [奇鋼仙女樓蘭](../Page/奇鋼仙女樓蘭.md "wikilink")          |
| [袖珍女侍小梅](../Page/袖珍女侍小梅.md "wikilink")          |
| [純情房東俏房客](../Page/純情房東俏房客_\(動畫\).md "wikilink") |
| [G-on少女騎士團](../Page/G-on少女騎士團.md "wikilink")    |

### 台灣情報節目

| 節目名稱                                                                                                                                                                                                                                                                                                                                                             |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 《[瀨上剛 in 台灣](../Page/瀨上剛_in_台灣.md "wikilink")》([瀨上剛主持](../Page/瀨上剛.md "wikilink")，JET日本台自製節目。此節目在台灣[廣電基金](../Page/廣電基金.md "wikilink")2006年首季評鑑中獲優良電視節目，執行長[林育卉表示](../Page/林育卉.md "wikilink")：該節目讓更多國外人士了解台灣之美，進而把台灣觀光產業推向國際舞台，因而獲獎。並且入圍2007年電視[金鐘獎](../Page/金鐘獎.md "wikilink")。[2010年4月起改至](../Page/2010年4月.md "wikilink")[國興衛視播出](../Page/國興衛視.md "wikilink")。) |
| 《優美の台灣》([小林優美主持](../Page/小林優美_\(藝人\).md "wikilink")，JET日本台自製節目)                                                                                                                                                                                                                                                                                                  |
| 《[大口吃遍台灣](../Page/大口吃遍台灣.md "wikilink")》([MATSU主持](../Page/松田直彌.md "wikilink")，JET日本台合作製播，2010年4月起改至國興衛視繼續播出。)                                                                                                                                                                                                                                                   |
| 《台灣一人觀光局》([青木由香主持](../Page/青木由香.md "wikilink")，JET日本台自製節目)                                                                                                                                                                                                                                                                                                       |
| 《約．會不會》([陳致遠和](../Page/陳致遠.md "wikilink")[佐藤麻衣主持](../Page/佐藤麻衣.md "wikilink")，JET日本台自製節目)                                                                                                                                                                                                                                                                        |

### 其他類型

| 節目名稱                                                                                                                                   |
| -------------------------------------------------------------------------------------------------------------------------------------- |
| 《東京夜未眠》(JET日本台第一個自製的[華語談話性節目](../Page/華語.md "wikilink")，[媒體棧國際行銷製作](../Page/媒體棧國際行銷.md "wikilink")，[楊思敏主持](../Page/楊思敏.md "wikilink")) |

## 大陸劇

『周一至五』早上06:00 - 07:30

<table>
<thead>
<tr class="header">
<th><p>電視劇名稱</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>《來不及說我愛你》(2013.02.04 - 2013.03.07)</p></td>
</tr>
<tr class="even">
<td><p>《紙醉金迷》(2013.05.07 - 2013.06.14)</p></td>
</tr>
<tr class="odd">
<td><p>《大漢盛世》(2013.05.17 - 2013.07.30)<br />
7/19和7/26播出的節目：06:00 - 06:30荳荳快樂學堂06:30 - 07:30寶貝上課了</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/故夢.md" title="wikilink">故夢</a>》(2013.07.31 - 2013.09.06)</p></td>
</tr>
</tbody>
</table>

JET綜合台【週一至週五】13:30\~15:00 (10/01起改為 14:00\~15:00)

| 電視劇名稱                                                          |
| -------------------------------------------------------------- |
| 《飛花如蝶》(2012年7月23日 - 2012年8月21日)                                |
| 《夜深沉》(2012年8月22日 - 2012年9月21日)                                 |
| 《[玫瑰江湖](../Page/玫瑰江湖.md "wikilink")》(2012年9月24日 - 2012年10月31日) |
| 《女人花(2007年陸劇)》(2012年11月1日 - 2012年12月26日)                       |

## 參見

  - [日本電視劇](../Page/日本電視劇.md "wikilink")
  - [緯來日本台](../Page/緯來日本台.md "wikilink")
  - [國興衛視](../Page/國興衛視.md "wikilink")
  - [臺灣電視台列表](../Page/臺灣電視台列表.md "wikilink")

## 參考資料

## 外部連結

  -
[Category:台灣電視播放頻道](../Category/台灣電視播放頻道.md "wikilink")
[Category:年代電視](../Category/年代電視.md "wikilink")

1.  魏颖.[杨澜将阳光卫视70%股权作价8000万售予星美传媒](http://it.sohu.com/74/89/article209558974.shtml).IT
    频道.2003-05-27