**jEdit**是一个用[Java语言开发的](../Page/Java.md "wikilink")[文本编辑器](../Page/文本编辑器.md "wikilink")，在[GPL下发布](../Page/GPL.md "wikilink")。它可以在[Windows](../Page/Microsoft_Windows.md "wikilink")、[Linux](../Page/Linux.md "wikilink")、[Mac
OS等多种平台下运行](../Page/Mac_OS.md "wikilink")，并且有很多插件，可以扩充基本功能。它也支持80多种文件类型的文法加亮显示。支持包括[UTF-8在内的多种字符编码](../Page/UTF-8.md "wikilink")。

jEdit也有很方便的[宏定义功能](../Page/宏.md "wikilink")，可以用[BeanShell](../Page/BeanShell.md "wikilink")、[Jython和](../Page/Jython.md "wikilink")[JavaScript等脚本语言](../Page/JavaScript.md "wikilink")。

## 参见

  - [文本编辑器列表](../Page/文本编辑器列表.md "wikilink")
  - [文本编辑器的比较](../Page/文本编辑器的比较.md "wikilink")

## 外部链接

  - [jEdit主页](http://www.jedit.org/index.php)
  - [专门针对Wikipedia写的插件](https://archive.is/20040917205251/http://www.djini.de/software/wikipedia/)

[Category:自由文本编辑器](../Category/自由文本编辑器.md "wikilink")
[Category:Windows文本编辑器](../Category/Windows文本编辑器.md "wikilink")
[Category:Java平台軟體](../Category/Java平台軟體.md "wikilink") [Category:OS
X文本编辑器](../Category/OS_X文本编辑器.md "wikilink")
[Category:1998年软件](../Category/1998年软件.md "wikilink")
[Category:用Java編程的自由軟體](../Category/用Java編程的自由軟體.md "wikilink")
[Category:自由HTML编辑器](../Category/自由HTML编辑器.md "wikilink")
[Category:Linux文本编辑器](../Category/Linux文本编辑器.md "wikilink")
[Category:使用GPL许可证的软件](../Category/使用GPL许可证的软件.md "wikilink")