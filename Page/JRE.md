{{ Infobox Software | name = Java Runtime Environment (JRE) | logo = |
screenshot = | caption = | developer =
[昇陽電腦](../Page/昇陽電腦.md "wikilink")（母公司[甲骨文公司](../Page/甲骨文公司.md "wikilink")）
| latest release version = 8 Update 151 | latest release date =  |
latest preview version = 9.0.1 | latest preview date =  | operating
system = [跨平台](../Page/跨平台.md "wikilink") | programming language =
[Java](../Page/Java.md "wikilink") | genre =
[Java虚拟机](../Page/Java虚拟机.md "wikilink") | license = Sun协议
(大部分基于 [GPL](../Page/GPL.md "wikilink")) | website =
<http://www.oracle.com/technetwork/java/javase/> }}

**Java執行環境**（，簡稱）是一個軟體，由昇陽電腦所研發，可以讓[電腦系統執行Java應用程式](../Page/電腦系統.md "wikilink")（Java
Application）。

JRE的內部有一個Java虛擬機器（）以及一些標準的[類別](../Page/類別.md "wikilink")[函数庫](../Page/庫.md "wikilink")（）。

## 參見

  - [Java platform](../Page/Java平臺.md "wikilink") - Java平台（英文）
  - [Java programming language](../Page/Java.md "wikilink") - Java程式語言
  - [Java Virtual Machine](../Page/JVM.md "wikilink") - Java虛擬機器
  - [Java Development Kit](../Page/JDK.md "wikilink") - Java開發套件
  - [Sun Microsystems](../Page/昇陽電腦.md "wikilink") - Sun

[en:Java virtual machine\#Execution
environment](../Page/en:Java_virtual_machine#Execution_environment.md "wikilink")

[Category:Java](../Category/Java.md "wikilink")
[Category:Java平台](../Category/Java平台.md "wikilink")
[Category:Java平台軟體](../Category/Java平台軟體.md "wikilink")
[Category:Java规范请求](../Category/Java规范请求.md "wikilink")
[Category:Java虚拟机](../Category/Java虚拟机.md "wikilink")