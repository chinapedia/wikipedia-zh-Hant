[JR_Freight_Class_EH500-38_with_Cheer_up_Tohoku_banner-P1030014.JPG](https://zh.wikipedia.org/wiki/File:JR_Freight_Class_EH500-38_with_Cheer_up_Tohoku_banner-P1030014.JPG "fig:JR_Freight_Class_EH500-38_with_Cheer_up_Tohoku_banner-P1030014.JPG")
**EH500型電力機車**，是供[日本貨物鐵道使用的交流傳動八軸](../Page/日本貨物鐵道.md "wikilink")[電力機車](../Page/電力機車.md "wikilink")，適用於交直流電化區段。

這款機車的暱稱為「ECO-POWER
[金太郎](../Page/金太郎.md "wikilink")」，以對應[EF210型的暱稱](../Page/JR貨物EF210型電力機車.md "wikilink")「ECO-POWER
[桃太郎](../Page/桃太郎.md "wikilink")」。

## 概要

這款機車使用雙節重聯設計，牽引逆變器使用了[IGBT元件](../Page/IGBT.md "wikilink")，整車最高輸出4,000
kW（每軸輸出500
kW），用作取代車齡漸高的[ED75型及](../Page/日本國鐵ED75型電力機車.md "wikilink")[ED79型](../Page/日本國鐵ED79型電力機車.md "wikilink")。首台試製車（EH500-901）於1997年由東芝廠房下線，至2000年起開始量產，編號由EH500-1起。

截至2010年年底，東芝共造出73台EH500型機車（量產車的最大號為72），當中2006年度及2007年度先後交付10台及9台\[1\]\[2\]。在2008年、2009年及2010年，東芝向JR貨物分別交付3輛、4輛及6輛新車。\[3\]\[4\]\[5\]

## 其他

東芝與[中國](../Page/中國.md "wikilink")[大連機車合作研製的](../Page/大連機車.md "wikilink")[HXD3型](../Page/中国铁路HXD3型电力机车.md "wikilink")（原稱DJ3）機車，也使用了EH500型作技術平台\[6\]，但單軸輸出則提升至1,200
kW，外觀設計則改為與歐洲生產的電力機車相似的造型。

## 參考資料

<div class="references-small">

<references/>

</div>

## 外部連結

  - [官方產品資料](https://web.archive.org/web/20071010005429/http://www3.toshiba.co.jp/sic/english/railway/products/vehicle/locomotive2_popup.htm)


[EH500](../Category/日本貨物鐵道車輛.md "wikilink")
[EH500](../Category/日本電力機車.md "wikilink")
[Category:20千伏50赫茲交流電力機車](../Category/20千伏50赫茲交流電力機車.md "wikilink")
[Category:20千伏60赫茲交流電力機車](../Category/20千伏60赫茲交流電力機車.md "wikilink")
[Category:Bo-Bo+Bo-Bo軸式機車](../Category/Bo-Bo+Bo-Bo軸式機車.md "wikilink")
[Category:1500伏直流电力机车](../Category/1500伏直流电力机车.md "wikilink")
[Category:东芝制铁路机车](../Category/东芝制铁路机车.md "wikilink")

1.  <span lang="ja">[JR貨物Webサイト
    ニュースリリース「平成18年度の車両等の設備投資について」](http://www.jrfreight.co.jp/press/pdf/200512-02.pdf)
    </span>
2.  <span lang="ja">[JR貨物Webサイト
    ニュースリリース「平成19年度の車両等の設備投資について」](http://www.jrfreight.co.jp/common/pdf/news/200612-04.pdf)
    </span>
3.  <span lang="ja">[JR貨物Webサイト
    ニュースリリース「平成20年度の車両等の設備投資について」](http://www.jrfreight.co.jp/common/pdf/news/200709-03.pdf)
    </span>
4.  <span lang="ja">[JR貨物Webサイト
    ニュースリリース「平成21年度の車両等の設備投資について」](http://www.jrfreight.co.jp/common/pdf/news/200806-02.pdf)
    </span>
5.  <span lang="ja">[JR貨物Webサイト
    ニュースリリース「平成22年度の機関車の新製について」](http://www.jrfreight.co.jp/common/pdf/news/200906-02.pdf)
    </span>
6.  杨守君、刘会岩．HXD3型交流传动电力机车\[J\]．电力机车与城轨车辆，2007（4）：9～19