《**JSA安全地帶**》（，），是一部於2000年拍攝的[韓國電影](../Page/大韓民國.md "wikilink")。

## 劇情介紹

某夜，位於[南](../Page/大韓民國.md "wikilink")[北韓邊界](../Page/朝鮮民主主義人民共和國.md "wikilink")[板門店的](../Page/板門店.md "wikilink")[共同警備區發生槍擊案](../Page/共同警備區.md "wikilink")，兩名[朝鮮人民軍士兵被殺](../Page/朝鮮人民軍.md "wikilink")、二名[大韓民國國軍士兵被懷疑涉及其中](../Page/大韓民國國軍.md "wikilink")，經雙方協商決定交由[中立國監察委員會調查](../Page/中立國監察委員會.md "wikilink")。委員會逐派遣了[瑞士籍韓裔軍官蘇菲和一位](../Page/瑞士.md "wikilink")[瑞典籍軍官到板門店調查](../Page/瑞典.md "wikilink")。

調查中，雙方證人均以不合作的態度對待蘇菲，後來蘇菲更被人揭發她的父親曾是[韓戰中的一位北韓將領](../Page/韓戰.md "wikilink")。原來韓戰後，北韓俘虜可選擇定居南北韓其中一方，但部份人拒絕居住北韓或南韓，最終離開了朝鮮半島；蘇菲的父親則是後輾轉到了[中立國居住](../Page/中立國.md "wikilink")。蘇菲的身份遭受到南韓質疑，委員會決定另派人員代替她。

最後，兩名南韓軍人一人自殺，一人自殺未遂卻重傷，兩人以死來保護服役期間秘密和北韓軍人發展出來的友誼。

本片表達了對南北人民超越政治紛爭的友情被國家對抗的政治現實所壓碎的慨歎。

## 演員陣容

  - [李炳憲](../Page/李炳憲.md "wikilink") 飾演
    李壽赫（[南韓陸軍兵長](../Page/南韓陸軍.md "wikilink")
    Lee Soo-hyeok）（粵語配音：[蘇強文](../Page/蘇強文.md "wikilink")）
  - [宋康昊](../Page/宋康昊.md "wikilink") 飾演
    吳景弼（[北韓軍隊中士](../Page/北韓.md "wikilink") Oh
    Kyeong-pil）（粵語配音：[潘文柏](../Page/潘文柏.md "wikilink")）
  - [李英愛](../Page/李英愛.md "wikilink") 飾演
    張蘇菲（[中立國監察委員會調查員](../Page/中立國監察委員會.md "wikilink")
    韓裔[瑞士陸軍少校](../Page/瑞士陸軍.md "wikilink") Sophie E.
    Jean）（粵語配音：[程文意](../Page/程文意.md "wikilink")）
  - [金太祐](../Page/金太祐.md "wikilink") 飾演
    南成植（[南韓陸軍一等兵](../Page/南韓陸軍.md "wikilink")
    Nam Sung-shik）（粵語配音：[陳卓智](../Page/陳卓智.md "wikilink")）
  - [申河均](../Page/申河均.md "wikilink") 飾演 鄭宇真（北韓列兵 Jeong
    Woo-jin）（粵語配音：[雷霆](../Page/雷霆.md "wikilink")）
  - [李漢偉](../Page/李漢偉.md "wikilink")
  - [高仁裴](../Page/高仁裴.md "wikilink")
  - [李對淵](../Page/李對淵.md "wikilink")
  - [基周峯](../Page/基周峯.md "wikilink")
  - [元根熙](../Page/元根熙.md "wikilink")
  - [高銀亞](../Page/高銀亞.md "wikilink")
  - Herbert Ulrich 飾演 [中立國監察委員會調查員](../Page/中立國監察委員會.md "wikilink") 瑞典中校
  - Christoph Hofrichter 飾演 Bruno
    Botta（[中立國監察委員會調查員](../Page/中立國監察委員會.md "wikilink")
    瑞士少將 MG Bruno Botta）

### 特別出演

  - 飾演 崔滿洙（北韓軍隊上尉 Choi Man-soo）（粵語配音：[梁志達](../Page/梁志達.md "wikilink")）

## 外部連結

  -
[Category:韓語電影](../Category/韓語電影.md "wikilink")
[Category:2000年電影](../Category/2000年電影.md "wikilink")
[Category:韓國背景電影](../Category/韓國背景電影.md "wikilink")
[Category:韓國劇情片](../Category/韓國劇情片.md "wikilink")
[Category:朴贊郁電影](../Category/朴贊郁電影.md "wikilink")
[Category:朝鮮半島南北關係題材電影](../Category/朝鮮半島南北關係題材電影.md "wikilink")
[Category:朝鮮民主主義人民共和國背景電影](../Category/朝鮮民主主義人民共和國背景電影.md "wikilink")
[Category:朝韩非军事区](../Category/朝韩非军事区.md "wikilink")