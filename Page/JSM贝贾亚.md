**贝贾亚青年体育马迪纳特俱乐部**（**Jeunesse Sportive Madinet de Béjaïa** ,**JSM
Béjaïa**）是位于[阿尔及利亚](../Page/阿尔及利亚.md "wikilink")[贝贾亚的职业足球俱乐部](../Page/贝贾亚.md "wikilink")。俱乐部创建于1936年。球队的主体育场为马格里布团结体育场（Stade
de l'Unité Maghrébine）。

## 外部链接

  - [Official Site](http://www.jsmbejaia.com)

[Category:阿尔及利亚足球俱乐部](../Category/阿尔及利亚足球俱乐部.md "wikilink")