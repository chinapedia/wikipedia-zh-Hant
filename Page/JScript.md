**JScript**是由[微软公司开发的動態脚本语言](../Page/微软.md "wikilink")，是微软对[ECMAScript规范的实现](../Page/ECMAScript.md "wikilink")\[1\]。

JScript最初随[Internet Explorer
3.0于](../Page/Internet_Explorer_3.0.md "wikilink")1996年8月发布。在网络程序员谈论[Internet
Explorer中的](../Page/Internet_Explorer.md "wikilink")[JavaScript的时候](../Page/JavaScript.md "wikilink")，他们实际上是指JScript。和其他活动脚本一样，它后来也被[Windows
Script Host和](../Page/Windows_Script_Host.md "wikilink")[Active Server
Pages所支持](../Page/Active_Server_Pages.md "wikilink")\[2\]。典型的JScript源文件使用的[扩展名是](../Page/扩展名.md "wikilink")`.js`。

JScript最新的版本是基于尚未定稿的ECMAScript 4.0版规范的[JScript
.NET](../Page/JScript_.NET.md "wikilink")，并且可以在微软的[.Net环境下编译](../Page/.Net.md "wikilink")。JScript在ECMA的规范上增加了许多特性。

## 版本

### JScript

| 版本  | 發佈日期     | 實現\[3\]                                                                                                                          | 基於                                                                                    | 類似的[JavaScript版本](../Page/JavaScript.md "wikilink") |
| --- | -------- | -------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------- | --------------------------------------------------- |
| 1.0 | 1996年8月  | [Internet Explorer 3.0](../Page/Internet_Explorer_3.0.md "wikilink")                                                             | Netscape JavaScript                                                                   | 1.0                                                 |
| 2.0 | 1997年1月  | [Windows IIS](../Page/Internet_Information_Services.md "wikilink") 3.0                                                           | Netscape JavaScript                                                                   | 1.1                                                 |
| 3.0 | 1997年10月 | [Internet Explorer 4.0](../Page/Internet_Explorer_4.0.md "wikilink")                                                             | ECMA-262 1st edition                                                                  | 1.3                                                 |
| 4.0 |          | [Visual Studio](../Page/Microsoft_Visual_Studio.md "wikilink") 6.0（[Visual InterDev的一部分](../Page/Visual_InterDev.md "wikilink")） | ECMA-262 1st edition                                                                  | 1.3                                                 |
| 5.0 | 1999年5月  | [Internet Explorer 5.0](../Page/Internet_Explorer_5.0.md "wikilink")                                                             | ECMA-262 2nd edition                                                                  | 1.4                                                 |
| 5.1 |          | Internet Explorer 5.01                                                                                                           | ECMA-262 2nd edition                                                                  | 1.4                                                 |
| 5.5 | 2000年7月  | Internet Explorer 5.5 & [Windows CE](../Page/Windows_CE.md "wikilink") 4.2                                                       | ECMA-262 3rd edition                                                                  | 1.5                                                 |
| 5.6 | 2001年10月 | [Internet Explorer 6.0](../Page/Internet_Explorer_6.0.md "wikilink") & [Windows CE](../Page/Windows_CE.md "wikilink") 5.0        | ECMA-262 3rd edition                                                                  | 1.5                                                 |
| 5.7 | 2006年11月 | [Internet Explorer 7.0](../Page/Internet_Explorer_7.0.md "wikilink")                                                             | ECMA-262 3rd edition + ECMA-327（ES-CP）                                                | 1.5                                                 |
| 5.8 | 2009年5月  | [Internet Explorer 8.0](../Page/Internet_Explorer_8.0.md "wikilink")                                                             | ECMA-262 3rd edition + ECMA-327（ES-CP） + [JSON](../Page/JSON.md "wikilink")（RFC 4627） | 1.5                                                 |
| 9.0 | 2011年3月  | [Internet Explorer 9.0](../Page/Internet_Explorer_9.0.md "wikilink")                                                             | ECMA-262 5th edition                                                                  | 1.8.1                                               |

### JScript .NET

| 版本   | 平台                                                               | 發佈日期       | 實現                                                           | 基於                   |
| ---- | ---------------------------------------------------------------- | ---------- | ------------------------------------------------------------ | -------------------- |
| 7.0  | Desktop [CLR](../Page/Common_Language_Runtime.md "wikilink") 1.0 | 2002年1月5日  | [Microsoft .NET Framework](../Page/.NET框架.md "wikilink") 1.0 | ECMA-262 3rd edition |
| 7.1  | Desktop [CLR](../Page/Common_Language_Runtime.md "wikilink") 1.1 | 2003年4月1日  | [Microsoft .NET Framework](../Page/.NET框架.md "wikilink") 1.1 | ECMA-262 3rd edition |
| 8.0  | Desktop [CLR](../Page/Common_Language_Runtime.md "wikilink") 2.0 | 2005年11月7日 | [Microsoft .NET Framework](../Page/.NET框架.md "wikilink") 2.0 | ECMA-262 3rd edition |
| 10.0 | Desktop [CLR](../Page/Common_Language_Runtime.md "wikilink") 4.0 | 2010年8月3日  | [Microsoft .NET Framework](../Page/.NET框架.md "wikilink") 4.0 | ECMA-262 3rd edition |

## 參考文獻

## 參見

  - [腳本語言](../Page/腳本語言.md "wikilink")
      - [DMDScript](../Page/DMDScript.md "wikilink")
      - [ECMAScript](../Page/ECMAScript.md "wikilink")
      - [JavaScript](../Page/JavaScript.md "wikilink")

{{-}}

[Category:Internet
Explorer](../Category/Internet_Explorer.md "wikilink")
[Category:基於對象的程式語言](../Category/基於對象的程式語言.md "wikilink")
[Category:脚本语言](../Category/脚本语言.md "wikilink")

1.
2.
3.