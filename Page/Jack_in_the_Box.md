**Jack in the
Box**是一家美國[快餐連鎖店](../Page/快餐.md "wikilink")，於1951年2月21日由羅伯特·奧斯卡·彼得森在美國加利福尼亞州聖地亞哥總部成立\[1\]。

集團目前共有2,200家分店，主力分佈於[美國西岸和東部少數大城市](../Page/美國.md "wikilink")。

## 歷史

[1951年以路邊餐廳起家](../Page/1951年.md "wikilink")；到1960年，成立Foodmaker
Inc.作為控股公司，門店數目增至180家，主力分佈於[加利福尼亞州](../Page/加利福尼亞州.md "wikilink")；[1968年開始推出特許經營](../Page/1968年.md "wikilink")。

早期Jack in the
Box主力是與麥當勞直接競爭，但到了1980年代起，所走路線有所更改，除了有漢堡包，亦推出了諸如[墨西哥卷餅](../Page/墨西哥卷餅.md "wikilink")、[墨西哥玉米餅](../Page/墨西哥玉米餅.md "wikilink")，以及[沙拉](../Page/沙拉.md "wikilink")、[三明治等等](../Page/三明治.md "wikilink")。1987年，成為上市公司，並且有897家餐廳。

## 食品

Jack in the
Box與其他美式速食店一樣售賣[漢堡包](../Page/漢堡包.md "wikilink")，但最受歡迎卻是[墨西哥夹饼](../Page/墨西哥夹饼.md "wikilink")。另又提供其他墨西哥食品，包括[墨西哥捲饼](../Page/墨西哥捲饼.md "wikilink")。

## 参考資料

[Category:1951年成立的公司](../Category/1951年成立的公司.md "wikilink")
[Category:速食餐廳](../Category/速食餐廳.md "wikilink")
[Category:美國連鎖速食店](../Category/美國連鎖速食店.md "wikilink")
[Category:美国西部经济](../Category/美国西部经济.md "wikilink")
[Category:美国中西部经济](../Category/美国中西部经济.md "wikilink")
[Category:美国东南部经济](../Category/美国东南部经济.md "wikilink")

1.