**Jasper**單曲，為[木村KAELA的個人第](../Page/木村KAELA.md "wikilink")11張單曲，
於2008年2月6日由哥倫比亞音樂發行。

## 曲目

1.  **Jasper**
      - 作詞：[木村KAELA](../Page/木村KAELA.md "wikilink")／作曲・編曲：[石野卓球](../Page/石野卓球.md "wikilink")
      - 為[角川電影](../Page/角川電影.md "wikilink")『[魔法玩具城](../Page/魔法玩具城.md "wikilink")』日本區主題曲。
      - 為神奈川音樂節目「SakuSaku」2008年2月份片尾曲。
2.  **Dive Into Shallow**
      - 作詞：日高央／作曲・編曲：[BEAT
        CRUSADERS](../Page/BEAT_CRUSADERS.md "wikilink")
3.  **Jasper（Instrumental）**
4.  **Dive Into Shallow（Instrumental）**

## 解説

  - 初回限定盤附DVD，收錄2首「No Reason Why」「Honey B〜蜜蜂之舞」的PV，且初回盤與通常盤封面不同。
  - 「Jasper」為2008年2月16日於日本上映的電影**『魔法玩具城』**的日本語版主題曲。
  - 石野卓球（日本電音教父）製作，且為本人首度嘗試的Techno電子曲風。
  - C/W曲「Dive Into
    Shallow」是自「[Snowdome](../Page/Snowdome.md "wikilink")」以來2度為BEAT
    CRUSADERS的作曲。
  - 台灣於2008年3月7日由風雲唱片代理發行台壓（CD+DVD版本）。

[en:Jasper (song)](../Page/en:Jasper_\(song\).md "wikilink")

[Category:木村KAELA歌曲](../Category/木村KAELA歌曲.md "wikilink")
[Category:2008年單曲](../Category/2008年單曲.md "wikilink")
[Category:電影主題曲](../Category/電影主題曲.md "wikilink")