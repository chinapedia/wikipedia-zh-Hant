**JasperReports**是一個用[Java開發的](../Page/Java.md "wikilink")[開源的](../Page/開源軟件.md "wikilink")[程式庫](../Page/程式庫.md "wikilink")，使用戶能夠透過它，利用Java語言來開發具有報告功能的程式。JasperReports的模板採用[XML格式](../Page/XML.md "wikilink")，從[JDBC資料庫中擷取合適的資料](../Page/JDBC.md "wikilink")，並把資料在屏幕、打印機顯示，或以[PDF](../Page/PDF.md "wikilink")、[HTML](../Page/HTML.md "wikilink")、[XLS](../Page/Microsoft_Excel.md "wikilink")、[CSV及](../Page/CSV.md "wikilink")[XML等各種格式儲存](../Page/XML.md "wikilink")。

JasperReports的報告模板可以以[iReport之類的工具來製作](../Page/iReport.md "wikilink")，只要把報告儲存成XML格式，就可以讓JasperReport閱讀，然後再編譯成為.jasper檔。

## 參看

  - [OpenReports](../Page/OpenReports.md "wikilink")

## 外部連結

  - [帮助你书写软件报告之JasperReports](https://archive.is/20060707202908/http://share.ccw.com.cn/show.jsp?digestID=180)
  - [JasperReports 在 SourceForge
    的項目頁面](http://sourceforge.net/projects/jasperreports/)
  - [JasperReports官方主頁](http://jasperreports.sourceforge.net/)
  - [JasperReports: Tips and
    Information](https://web.archive.org/web/20060720165442/http://www.brianburridge.com/tag/jasper-reports/)

  - [JasperReports Tutorial/Getting Started
    Guide](http://ensode.net/jasperreports_intro.html)
  - [**DynamicReports**](http://dynamicreports.sourceforge.net/)
  - [JasperWave Report Designer - 是一个 Eclipse 插件，用来设计 JasperReports
    报表。](https://web.archive.org/web/20180210145042/http://jasperwave.com/)

[Category:Java軟件](../Category/Java軟件.md "wikilink")
[Category:報告軟件](../Category/報告軟件.md "wikilink")