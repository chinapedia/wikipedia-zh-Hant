**JavaFX**是由[甲骨文公司推出的一系列的产品和技术](../Page/甲骨文公司.md "wikilink")，该产品于2007年5月在JavaOne大会上首次对外公布。JavaFX技术主要应用于创建Rich
Internet
application（[RIAs](../Page/RIA.md "wikilink")）。当前的JavaFX包括JavaFX脚本和JavaFX
Mobile（一种运营于行動裝置的操作系统），今后JavaFX将包括更多的产品。

JavaFX脚本的前身是一个由Chris Oliver开发的一个叫做F3的项目\[1\]。

JavaFX期望能够在桌面应用的开发领域与Adobe公司的AIR、[OpenLaszlo以及](../Page/OpenLaszlo.md "wikilink")[微软公司的](../Page/微软.md "wikilink")[Silverlight相竞争](../Page/Silverlight.md "wikilink")，它也可应用于Blu-Ray的交互平台[BD-J](../Page/BD-J.md "wikilink")，但目前尚未宣布对Blu-Ray的支援计划。

## 参见

  - [JavaFX脚本](../Page/JavaFX脚本.md "wikilink")
  - [Adobe AIR](../Page/Adobe_AIR.md "wikilink")（前身是Adobe Apollo）
  - [Curl](../Page/Curl变成语言.md "wikilink") 5.0 from [Curl
    Inc.](http://www.curl.com) for off-line RIA\\
  - 支援JavaFX的[Lobo Web浏览器](../Page/Lobo_\(浏览器\).md "wikilink")
  - [Microsoft Silverlight](../Page/Microsoft_Silverlight.md "wikilink")
  - [Rebol编程语言作为Java虚拟机的替代](../Page/Rebol.md "wikilink")
  - 来自[Mozilla基金会的](../Page/Mozilla基金会.md "wikilink")[XUL和](../Page/XUL.md "wikilink")[XULRunner](../Page/XULRunner.md "wikilink")
  - [JavaFX mobile](../Page/JavaFX_mobile.md "wikilink")

## 註釋

<references/>

## 外部链接

  - [Sun.com - JavaFX](http://www.sun.com/software/javafx/)
  - [Sun.com - Learning JavaFX Script: An Introduction for Java
    Programmers](http://java.sun.com/developer/technicalArticles/scripting/javafxpart1/)
  - [A Digg style community site for JavaFX articles,Blogs,Videos,Books
    and everything else on
    JavaFX.](https://web.archive.org/web/20080321070419/http://www.javafxworld.net/)
  - [Planet JFX is an open-source documentation wiki for the new JavaFX
    scripting platform.](http://jfx.wikia.com/wiki/Main_Page)
  - [James Weaver's Learn JavaFX Weblog](http://learnjavafx.typepad.com)
  - [Crash Course in Next-Gen RIA: AIR, Silverlight, and
    JavaFX](http://www.devx.com/RichInternetApps/Article/35208/1954?pf=true)
  - [CRUDFx -
    JavaFx/JDBC](https://web.archive.org/web/20080302110659/http://molgav.nn.ru/index.php?option=com_content&view=article&id=229%3Apractical-guide-to-javafx&Itemid=56)
  - [Samples-on-javafx.com](http://javafx.com/samples/)

[Category:Java](../Category/Java.md "wikilink") [Category:Mobile
software](../Category/Mobile_software.md "wikilink")

1.  [F3项目](http://blogs.sun.com/chrisoliver/entry/f3)