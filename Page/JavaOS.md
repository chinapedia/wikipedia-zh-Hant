**JavaOS**（也稱：**Java作業系統**、**爪哇作業系統**）是一套[作業系統](../Page/作業系統.md "wikilink")，是以[Java
Virtual
Machine](../Page/JVM.md "wikilink")（簡稱：[JVM](../Page/JVM.md "wikilink")，也稱：[爪哇虛擬機器](../Page/JVM.md "wikilink")）與一些基礎軟體組件所構成，由[昇陽電腦所開發](../Page/昇陽電腦.md "wikilink")。Java作業系統如同[UNIX作業系統或](../Page/UNIX.md "wikilink")[類UNIX作業系統一樣](../Page/类_Unix.md "wikilink")，UNIX作業系統、[類UNIX作業系統的主要本體皆是用](../Page/类_Unix.md "wikilink")[C語言所開發撰寫成](../Page/C語言.md "wikilink")，而Java作業系統的主要本體則是用[Java程式語言所撰寫成](../Page/Java.md "wikilink")。

## 微核心

JavaOS的系統是：以硬體架構的[原生](../Page/原生.md "wikilink")（native）[微核心](../Page/微核心.md "wikilink")（[microkernel](../Page/microkernel.md "wikilink")）為基礎的：

JavaOS核心可以執行的平台包括：

  - [ARM](../Page/Advanced_RISC_Machines.md "wikilink")
  - [PowerPC](../Page/PowerPC.md "wikilink")
  - [RISC](../Page/RISC.md "wikilink")
  - [SPARC](../Page/SPARC.md "wikilink")
  - [StrongARM](../Page/StrongARM.md "wikilink")
  - [x86](../Page/x86.md "wikilink")

## 虛擬機器

以[微核心為基礎](../Page/微核心.md "wikilink")，爪哇虛擬機器（Java virtual
Machine，JVM）在微核心之上執行。

## 驅動程式

所有[裝置驅動程式皆是以](../Page/裝置驅動程式.md "wikilink")[Java程式撰寫成](../Page/Java.md "wikilink")，並在JVM之上執行。

## 視窗系統

JavaOS中的圖形、視窗系統部分是用[AWT](../Page/AWT.md "wikilink")（[Abstract Windowing
Toolkit](../Page/AWT.md "wikilink")）的[API方式來實現](../Page/API.md "wikilink")，也是完全用Java語言所撰寫成。

## 應用

JavaOS是針對[嵌入式系統的應用所設計](../Page/嵌入式系統.md "wikilink")，例如[視訊機頂盒](../Page/機頂盒.md "wikilink")（[Set-Top
Box](../Page/机顶盒.md "wikilink")，[STB](../Page/机顶盒.md "wikilink")）、網路基礎建設、[自動提款機](../Page/自動櫃員機.md "wikilink")（[Automatic
Teller
Machine](../Page/自動櫃員機.md "wikilink")，[ATM](../Page/自動櫃員機.md "wikilink")）等，也用於[網路電腦](../Page/網路電腦.md "wikilink")（[Network
Computer](../Page/Network_Computer.md "wikilink")）：[JavaStation中](../Page/JavaStation.md "wikilink")

## 參見

  - [Java](../Page/Java.md "wikilink") - Java程式語言
  - [JNode](../Page/JNode.md "wikilink")（英文）
  - [Inferno](../Page/Inferno_\(operating_system\).md "wikilink") -
    Inferno作業系統（英文）

## 外部連結

  - \[ 進可契機、退可籌碼的「Java作業系統」手機\]－2006年6月19日（中文）

<!-- end list -->

  - [Inside the
    深入IBM的JavaOS專案（英文）](https://web.archive.org/web/20060406232245/http://www.itmweb.com/f031098.htm)

<!-- end list -->

  - [Sun對JavaOS架構的解說（英文）](http://java.sun.com/javaone/sessions/slides/TT04/startit.html)

<!-- end list -->

  - [JNode](http://jnode.org) -
    一個仍在持續進行發展的程式開發專案，目標是盡可能完全只用Java程式語言來開發撰寫出一套作業系統，以便盡可能以原生方式執行Java程式（英文）

<!-- end list -->

  - [SavaJe科技公司](http://www.savaje.com) - 專注於開發極近似JavaOS的嵌入式作業系統（英文）

[Java platform](../Category/昇陽電腦.md "wikilink")