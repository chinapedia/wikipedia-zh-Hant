[Sun_microsystems_java_desktop.png](https://zh.wikipedia.org/wiki/File:Sun_microsystems_java_desktop.png "fig:Sun_microsystems_java_desktop.png")
**Sun Java Desktop
System**（**JDS**）是一個用於[昇陽的](../Page/昇陽.md "wikilink")[Solaris及其原來](../Page/Solaris.md "wikilink")[Linux](../Page/Linux.md "wikilink")（Linux的支援於2006年5月30日中斷）\[1\]
的[桌面環境](../Page/桌面環境.md "wikilink")。

JDS目的為提供一般電腦用戶一個熟悉（意即其外觀與[Microsoft
Windows相似](../Page/Microsoft_Windows.md "wikilink")）以及擁有一整套如[辦公軟體套裝](../Page/辦公軟體套裝.md "wikilink")、[網頁瀏覽器](../Page/網頁瀏覽器.md "wikilink")、[電子郵件](../Page/電子郵件.md "wikilink")、日程表與即時訊息等辦公室生產力軟體的系統。

其名稱反映出昇陽有意將其作為一個推動公司用戶部署為[Java平台編寫的軟體的場所](../Page/Java平台.md "wikilink")；JDS建基於[GNOME之上](../Page/GNOME.md "wikilink")，主要採用[自由軟體](../Page/自由軟體.md "wikilink")。

## 版本

JDS 3包含在Solaris
10內——在安裝Solaris時可選擇使用[CDE或JDS](../Page/Common_Desktop_Environment.md "wikilink")。JDS
Release 2亦可在其中一個基於[SuSE的Linux分發版上使用](../Page/SuSE.md "wikilink")。

JDS第二版包括

  - [Java](../Page/Java平台.md "wikilink")

  - [GNOME](../Page/GNOME.md "wikilink")（使用Blueprint佈景主題）

  - [StarOffice](../Page/StarOffice.md "wikilink")

  - [Mozilla](../Page/Mozilla.md "wikilink")

  - [Evolution](../Page/Novell_Evolution.md "wikilink")

  - [MP3與光碟播放器](../Page/MP3.md "wikilink")

  - 的Java Media Player

  - [Gaim多個服務即時通訊軟體](../Page/Gaim.md "wikilink")

  - [RealPlayer](../Page/RealPlayer.md "wikilink")

JDS Release 3基於GNOME 2.6，只有Solaris 10平台可使用，已被甲骨文併購的昇陽先前曾在內部的Solaris
10桌面電腦上佈署JDS 3。

[OpenSolaris使用的JDS常稱作OpenSolaris](../Page/OpenSolaris.md "wikilink")
Desktop。OpenSolaris Desktop 01（於2005年10月28日發佈）基於GNOME 2.10，OpenSolaris
Desktop 02（於2005年12月23日）基於GNOME 2.12。

最初時昇陽為Solaris 8同梱了一張GNOME 1.4的預覽版獨立光碟。

## 註解

## 請參閱

  -
## 外部連結

  - [Sun Java Desktop
    System](https://web.archive.org/web/20041215060534/http://wwws.sun.com/software/javadesktopsystem/)
  - [Java.net上的Java Desktop社群](http://www.javadesktop.org/)
  - [OpenSolaris上的Java Desktop
    System社群](https://archive.is/20121209032612/http://www.opensolaris.org/os/community/desktop/communities/jds/)
  - [OS News的評論](http://www.osnews.com/story.php?news_id=5286)－2003年12月
  - [eWeek的評論](http://www.eweek.com/article2/0,1759,1396988,00.asp)－2003年12月

[Category:昇陽電腦軟體](../Category/昇陽電腦軟體.md "wikilink")
[Category:操作系統](../Category/操作系統.md "wikilink")
[Category:Java平台軟體](../Category/Java平台軟體.md "wikilink")

1.