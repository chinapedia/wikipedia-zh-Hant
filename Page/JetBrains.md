**JetBrains**是一家[捷克的软件开发公司](../Page/捷克.md "wikilink")，该公司位于[捷克的](../Page/捷克.md "wikilink")[布拉格](../Page/布拉格.md "wikilink")，并在[俄罗斯的](../Page/俄罗斯.md "wikilink")[圣彼得堡及](../Page/圣彼得堡.md "wikilink")[美国](../Page/美国.md "wikilink")[麻州](../Page/麻州.md "wikilink")[波士頓都设有办公室](../Page/波士頓.md "wikilink")，該公司最為人所熟知的產品是[Java程式語言開發撰寫時所用的](../Page/Java.md "wikilink")[整合開發環境](../Page/整合開發環境.md "wikilink")：[IntelliJ
IDEA](../Page/IntelliJ_IDEA.md "wikilink")。

JetBrains成立於2000年，是一家[私人持股的公司](../Page/私人持股.md "wikilink")，該公司的合夥創辦人有：Sergey
Dmitriev、Eugene Belyaev及Valentin Kipiatkov。

截至2017年6月，该公司共发布了24款开发工具与及相关产品。

## 相關產品

### 集成开发环境\[1\]

  - [AppCode](../Page/AppCode.md "wikilink") - Swift 和 Objective-C
    IDE開發工具。

<!-- end list -->

  - [CLion](https://www.jetbrains.com/clion/) -
    跨平台的[C](../Page/C语言.md "wikilink")/[C++](../Page/C++.md "wikilink")
    [IDE](../Page/集成开发环境.md "wikilink")
    开发工具，支持[C++11](../Page/C++11.md "wikilink")
    、[C++14](../Page/C++14.md "wikilink")、[libc++以及](../Page/C++標準函式庫.md "wikilink")[Boost](../Page/Boost_C++_Libraries.md "wikilink")。

<!-- end list -->

  - [DataGrip](../Page/DataGrip.md "wikilink") - 一款数据库客户端工具

<!-- end list -->

  - [GoLand](../Page/GoLand.md "wikilink") -
    [Go語言的整合開發環境](../Page/Golang.md "wikilink")。

<!-- end list -->

  - [IntelliJ IDEA](../Page/IntelliJ_IDEA.md "wikilink") -
    2001年发布。一套智慧型的 Java 整合開發環境，特別專注與強調程式師的開發撰寫效率提升。

<!-- end list -->

  - [PhpStorm](../Page/PhpStorm.md "wikilink") - PHP IDE開發工具。

<!-- end list -->

  - [PyCharm](../Page/PyCharm.md "wikilink") - 一款結合了Django框架的Python
    IDE開發工具。

<!-- end list -->

  - [Rider](../Page/Rider.md "wikilink") - 一款快速，功能强大，跨平台的.NET IDE开发工具。

<!-- end list -->

  - [RubyMine](../Page/RubyMine.md "wikilink") - 一套強大的Ruby on Rails
    IDE開發工具。

<!-- end list -->

  - [WebStorm](../Page/WebStorm.md "wikilink") - JavaScript的開發工具。

### 插件\[2\]

  - [ReSharper](../Page/ReSharper.md "wikilink") - 一套用來搭配
    [Microsoft](../Page/Microsoft.md "wikilink") 公司 [Visual Studio
    .NET](../Page/Visual_Studio_.NET.md "wikilink")
    整合開發環境的[外掛程式](../Page/插件.md "wikilink")（Plug-In），此一外掛的功效在於讓程式進行再分拆、增進撰寫效率，並且能支援[C\#程式語言](../Page/C_Sharp.md "wikilink")。

<!-- end list -->

  - [dotCover](../Page/dotCover.md "wikilink") - VB.Net
    集成的單元測試運行和代碼覆蓋工具。

<!-- end list -->

  - [dotTrace](../Page/dotTrace.md "wikilink") -
    一套效能分析軟體，能有效、輕易的找到[.NET應用程式中最耗佔處理器運算資源的效能瓶頸癥結](../Page/.NET.md "wikilink")、環節。

<!-- end list -->

  - [dotPeek](../Page/dotPeek.md "wikilink") - .Net平台的免费反編譯工具。

### 编程语言\[3\]

  - [Kotlin](../Page/Kotlin.md "wikilink") -
    基於JVM的現代編程語言。Kotlin是一門靜態類型、面向對象，旨在避免由Java的向後兼容性引起的問題的編程語言。JetBrains公司于2010年開發出該編程語言并于2012年2月宣佈將其開源。Kotlin語言最初設計的目的是替代Java語言。

  - \-
    MPS给编程语言带来了很大的灵活性。与具有严格的语法和语义限制的传统编程语言不同，MPS允许用户创建新的编程语言，更改或扩展现有的编程语言。\[4\]

### 团队工具\[5\]

  - [TeamCity](../Page/TeamCity.md "wikilink") - 支持Java和 .Net 持續集成工具。

<!-- end list -->

  - [YouTrack](../Page/YouTrack.md "wikilink") - 一款項目跟踪和問題跟踪工具。

<!-- end list -->

  - [YouTrack](../Page/YouTrack.md "wikilink") - 一款用于代码审查，团队协作项目分析的工具。

<!-- end list -->

  - [Hub](../Page/Hub.md "wikilink") - 一款将JetBrains的团队工具集成到一起的工具

### 其他

  - [Omea](../Page/Omea.md "wikilink") -
    一套"個人資訊環境"軟體，可整合各種類型的資源，如電子信件、新聞訊息、網頁書籤、工作事項、聯絡人等，並讓這些資源以單一位置的方式來存取使用。

## 開發中的產品

  - [Meta Programming System](http://www.jetbrains.com/mps) -
    一套新的程式撰寫環境，使程式師能輕易的定義自屬的專有程式語言，並且能用專有語言與其他程式語言共同使用，此新軟體期望能讓「程式語言導向的開發撰寫」的概念獲得實現。

## 參考資料

## 外部連結

  -
<!-- end list -->

  - [Android
    Rivers：用Kotlin语言写出的新闻应用](http://www.csdn.net/article/2013-02-06/2814100-use-kotlin-to-develop-android-apps)

[Category:软件公司](../Category/软件公司.md "wikilink")

1.

2.
3.

4.

5.