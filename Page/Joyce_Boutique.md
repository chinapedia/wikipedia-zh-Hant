[One_Island_South_2013.jpg](https://zh.wikipedia.org/wiki/File:One_Island_South_2013.jpg "fig:One_Island_South_2013.jpg")[One
Island South](../Page/One_Island_South.md "wikilink")\]\] **Joyce
Boutique**（**載思**，）是一家在[香港交易所上市的](../Page/香港交易所.md "wikilink")[時裝集團](../Page/時裝.md "wikilink")，由兩大[香港望族](../Page/香港望族.md "wikilink")[永安](../Page/永安百貨.md "wikilink")[郭氏家族的第四代](../Page/郭沛勳家族.md "wikilink")[郭志清與丈夫](../Page/郭志清.md "wikilink")[先施馬氏家族後人](../Page/先施.md "wikilink")[馬景華於](../Page/馬景華.md "wikilink")1970年創立<small>\[1\]</small>。主要業務是透過旗下的店舖以自己的品牌或代理外國著名品牌的時裝、化妝品及飾物銷售，在香港、[台灣及](../Page/台灣.md "wikilink")[中國均有分店或品牌專門店](../Page/中國.md "wikilink")。1990年10月16日在香港聯交所[上市](../Page/首次公开募股.md "wikilink")。

2000年[會德豐斥資](../Page/會德豐.md "wikilink")2億港元買入51%的控制性股權。2003年3月會德豐將所持的股權轉讓予[吳光正家族信託](../Page/吳光正.md "wikilink")，現時持有Joyce
Boutique
51.99%股權，而馬景華則持23%股權，保持第二大股東地位。2007年11月19日馬景華、馬郭志清及二人的女兒馬美儀齊齊辭職，轉任非執行董事，吳天海接任非執行主席。

## 董事會成員

  - [吳天海先生](../Page/吳天海.md "wikilink")（非執行主席）
  - [馬景華先生](../Page/馬景華.md "wikilink")（2007年11月19日辭任主席，改任非執行董事，由吳天海接任<small>\[2\]</small>）
  - [馬郭志清女士](../Page/馬郭志清.md "wikilink")（2007年11月19日辭任行政總裁，改任非執行董事）
  - [馬美儀女士](../Page/馬美儀.md "wikilink")（2007年11月19日辭任總裁兼董事總經理，改任非執行董事）
  - [盧美高先生](../Page/盧美高.md "wikilink")\*
  - [陳思孝先生](../Page/陳思孝.md "wikilink")\*
  - [霍華士先生](../Page/霍華士.md "wikilink")（2007年10月1日辭任，由[麥社安先生接任](../Page/麥社安.md "wikilink")<small>\[3\]</small>）
  - [李玉芳女士](../Page/李玉芳.md "wikilink")
  - [李福全先生](../Page/李福全.md "wikilink")\*
  - [李唯仁先生](../Page/李唯仁.md "wikilink")
  - [麥社安先生](../Page/麥社安.md "wikilink")
  - [羅啟堅先生](../Page/羅啟堅.md "wikilink")\*
  - [馬美域女士](../Page/馬美域.md "wikilink")
  - [吳梓源先生](../Page/吳梓源.md "wikilink")
  - [徐耀祥先生](../Page/徐耀祥.md "wikilink")

（\* 為獨立非執行董事）

## 店舖

[HK_JoyceBoutique_NewWorldTower.JPG](https://zh.wikipedia.org/wiki/File:HK_JoyceBoutique_NewWorldTower.JPG "fig:HK_JoyceBoutique_NewWorldTower.JPG")[新世界大廈的旗艦店](../Page/新世界大廈.md "wikilink")\]\]
[Joyce_Beauty_in_YOHO_Mall_2017.jpg](https://zh.wikipedia.org/wiki/File:Joyce_Beauty_in_YOHO_Mall_2017.jpg "fig:Joyce_Beauty_in_YOHO_Mall_2017.jpg")分店\]\]

  - 香港

位於[香港](../Page/香港.md "wikilink")[中環](../Page/中環.md "wikilink")[皇后大道中](../Page/皇后大道中.md "wikilink")[新世界大廈](../Page/新世界大廈.md "wikilink")（佔地16,000[平方呎](../Page/平方呎.md "wikilink")）的旗艦店是Joyce店舖中面積最大的一間，售賣Joyce全線於香港代理的品牌，於2007年斥資2,000多萬停業整修，由馬郭志清親自挑選店內陳設的古董傢具<small>\[4\]</small>。

其他分店分佈於[尖沙咀](../Page/尖沙咀.md "wikilink")[海港城](../Page/海港城.md "wikilink")、[金鐘](../Page/金鐘.md "wikilink")[太古廣場](../Page/太古廣場.md "wikilink")、[銅鑼灣](../Page/銅鑼灣.md "wikilink")[利園一期](../Page/利園一期.md "wikilink")。

太古廣場分店於2001年曾進行擴充，由原本一層變成兩層的分店。為隆重其事，Joyce更邀請了紐約著名室內設計師Calvin
Tsao，負責新舖的室內設計。除了增加空間感，令顧客可以逛得更舒適外，最特別的是連接上下兩層的旋轉樓梯，是以Joyce的「J」字作為設計基礎。經過擴充後的太古廣場分店，售賣的時裝品牌比以往更高檔，且還增設謢膚及化妝品專櫃Joyce
Beauty，取代尖沙咀分店的地位。<small>\[5\]</small>。

其他店舖分佈於[時代廣場](../Page/時代廣場_\(香港\).md "wikilink")（[Anna
Sui](../Page/Anna_Sui.md "wikilink")
店）、[國際金融中心](../Page/國際金融中心.md "wikilink")（Etro
店）、[崇光百貨](../Page/崇光百貨.md "wikilink")（BOSS
專櫃）及[海洋中心](../Page/海洋中心.md "wikilink")（Jil
Sander、Anna Sui 和 Etro 店）。

## 已結業店舖

Joyce在80年代時於[半島酒店開設首間店舖](../Page/半島酒店.md "wikilink")，在90年代時於皇后大道中九號[嘉軒廣場開設旗艦店](../Page/嘉軒廣場.md "wikilink")，樓高2層，並設Joyce
Café。後來Joyce Café更於[交易廣場開設分店](../Page/交易廣場.md "wikilink")。

## 代理品牌

[Victoria_Beckham_in_The_Landmark_Hong_Kong_2016.jpg](https://zh.wikipedia.org/wiki/File:Victoria_Beckham_in_The_Landmark_Hong_Kong_2016.jpg "fig:Victoria_Beckham_in_The_Landmark_Hong_Kong_2016.jpg")[置地廣場的Victoria](../Page/置地廣場.md "wikilink")
Beckham是首間亞洲專門店\]\]

  - [Alexander McQueen](../Page/Alexander_McQueen.md "wikilink")
  - Alexander Wang
  - Ann Demeulemeester
  - [Anna Sui](../Page/Anna_Sui.md "wikilink")
  - Balenciaga
  - Balmain
  - Celine
  - Chloe
  - Christoper Kane
  - Comme Des Garcons
  - Donna Karan
  - Dries Van Noten
  - Dior Homme
  - Dsquared2
  - Emilio Pucci
  - Etro
  - [Givenchy](../Page/Givenchy.md "wikilink")
  - [Hugo Boss](../Page/Hugo_Boss.md "wikilink")
  - Issey Miyake
  - Jeremy Scott
  - Jil Sander
  - [Jimmy Choo](../Page/Jimmy_Choo.md "wikilink")
  - John Galliano
  - Junya Watanabe
  - Lanvin
  - Marni
  - [Mastermind Japan](../Page/Mastermind_Japan.md "wikilink")
  - Moncler
  - Mugler
  - Neil Barrett
  - Oscar de la Renta
  - Raf Simons
  - Rick Owens
  - Saint Laurent
  - Stella McCartney
  - Vera Wang
  - [Versace](../Page/Versace.md "wikilink")
  - Viktor & Rolf
  - [Victoria Beckham](../Page/Victoria_Beckham.md "wikilink")
  - [Yohji Yamamoto](../Page/Yohji_Yamamoto.md "wikilink")

## 參考

<div class="references-small">

<references />

</div>

## 外部連結

  - [Joyce Boutique Holdings Limited](http://www.joyce.com/)

[Category:香港交易所上市公司](../Category/香港交易所上市公司.md "wikilink")
[Category:香港上市服務業公司](../Category/香港上市服務業公司.md "wikilink")
[Category:香港零售商](../Category/香港零售商.md "wikilink")
[Category:香港服装公司](../Category/香港服装公司.md "wikilink")
[Category:香港時裝品牌](../Category/香港時裝品牌.md "wikilink")
[Category:會德豐](../Category/會德豐.md "wikilink")
[Category:香港上市服裝公司](../Category/香港上市服裝公司.md "wikilink")
[Category:1970年成立的公司](../Category/1970年成立的公司.md "wikilink")

1.  [禤中怡](../Page/禤中怡.md "wikilink")，(2007年)，{{〈}}中環博客：Joyce
    Ma放手{{〉}}，《[am730](../Page/am730.md "wikilink")》2007年11月14日號，P.26
    INVESTMENT。
2.  [主席變動及放棄董事會上的執行職銜](http://main.ednews.hk/listedco/listconews/sehk/20071113/LTN20071113201_C.PDF)
3.
4.  [時裝女王Joyce
    Ma二千萬豪裝反擊戰](http://hk.lifestyle.yahoo.com/050928/62/1h3oy.html)

5.  太陽報： Joyce 三十周年金鐘分店旋轉出擊 2001年9月23日