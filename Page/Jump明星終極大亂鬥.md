[任天堂DS平台的一個](../Page/任天堂DS.md "wikilink")[動作遊戲](../Page/動作遊戲.md "wikilink")，於2006年11月23日發行，為2005年8月8日發行的的繼承作品。

此[電子遊戲集合了](../Page/電子遊戲.md "wikilink")[日本杂志](../Page/日本.md "wikilink")《[週刊少年Jump](../Page/週刊少年Jump.md "wikilink")》刊登的41部[漫畫中的角色](../Page/漫畫.md "wikilink")，共305人。

## 遊戲系統

遊戲主要以傳統的十字鍵及按鈕、並搭配任天堂DS特色之一的輕觸式螢幕以進行遊戲。遊戲分為故事模式、排名模式以及問答遊戲等，並支援Wi-Fi網絡對戰功能。玩者需在4x5大小的空格內放置不同角色漫畫格組合成卡組以進行遊戲，並需最少配置戰鬥角色（4-8格）、支援角色（2-3格）及援助角色（1格純角色）各一才能使用，而不同角色則通常需以通關的方式獲得。

另外，本作使用了角色進化系統，不同於前作採用漫畫格合併的形式獲得新人物，而是以在戰鬥中獲得的點數為角色進化，從而獲得更強的人物或隱藏關卡。

## 外部链接

  - [官方网站](http://www.nintendo.co.jp/ds/ajuj/index.html)

[Category:2006年电子游戏](../Category/2006年电子游戏.md "wikilink")
[Category:動作遊戲](../Category/動作遊戲.md "wikilink")
[Category:任天堂遊戲](../Category/任天堂遊戲.md "wikilink")
[Category:任天堂DS遊戲](../Category/任天堂DS遊戲.md "wikilink")
[Category:任天堂DS独占游戏](../Category/任天堂DS独占游戏.md "wikilink")
[Category:任天堂Wi-Fi连接游戏](../Category/任天堂Wi-Fi连接游戏.md "wikilink")
[Category:后传电子游戏](../Category/后传电子游戏.md "wikilink")
[Category:漫畫改編電子遊戲](../Category/漫畫改編電子遊戲.md "wikilink")
[Category:電子遊戲跨界作品](../Category/電子遊戲跨界作品.md "wikilink")
[Category:日本开发电子游戏](../Category/日本开发电子游戏.md "wikilink")