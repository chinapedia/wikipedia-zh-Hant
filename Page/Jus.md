__NOTOC__
[Beard_token.jpg](https://zh.wikipedia.org/wiki/File:Beard_token.jpg "fig:Beard_token.jpg")

（**小Jus**）和（**大Jus**）是[西里尔字母](../Page/西里尔字母.md "wikilink")\[1\]，在[早期西里尔字母和](../Page/早期西里尔字母.md "wikilink")[格拉哥里字母当中表示](../Page/格拉哥里字母.md "wikilink")[通用斯拉夫语的两个](../Page/原始斯拉夫语.md "wikilink")[鼻化元音](../Page/鼻化元音.md "wikilink")。两者都有形式（），是与[І组成的](../Page/І.md "wikilink")[合字](../Page/合字.md "wikilink")。其他jus字母包括**混合Jus**（）、**闭合小Jus**（）和**Iota化的闭合小Jus**（）。

[Yus.svg](https://zh.wikipedia.org/wiki/File:Yus.svg "fig:Yus.svg")
[Handwritten_Little_Yus.svg](https://zh.wikipedia.org/wiki/File:Handwritten_Little_Yus.svg "fig:Handwritten_Little_Yus.svg")

语音学上，小jus表示一个鼻化的前元音，可能是，而大jus表示一个鼻化的后元音，像是。还有建议认为两者分别表示am和om的发音。

所有使用西里尔字母的现代斯拉夫语族的语言失去了鼻化元音，使得jus失去了作用。

大jus作为[保加利亚语的一部分一直持续到](../Page/保加利亚语.md "wikilink")1945年。然而在那时，东部方言里鼻化后音在发音上等同于[ъ](../Page/ъ.md "wikilink")
。由于保加利亚语主要基于东部方言，西部方言的发音被看作非书面语，该字母也被去除。

希腊北部[塞萨洛尼基和](../Page/塞萨洛尼基.md "wikilink")[卡斯托里亚一带的一些保加利亚语方言还保留着鼻化音](../Page/卡斯托里亚.md "wikilink")：（你要去哪，亲爱的孩子？）\[2\]

在[俄罗斯](../Page/俄罗斯.md "wikilink")，小jus被用来表示词中或词尾的iota化（），现代字母出自其17世纪的写法，由1708年的文字改革推广。（这也是为什么俄语常常对应[波兰语](../Page/波兰语.md "wikilink")；对比俄语与波兰语）

在使用[拉丁字母的斯拉夫语言](../Page/拉丁字母.md "wikilink")[波兰语当中](../Page/波兰语.md "wikilink")，[Ę ę的发音相当于小jus](../Page/Ę.md "wikilink")，而[Ą ą的发音相当于大jus](../Page/Ą.md "wikilink")。[Iota化的形式则写作ię](../Page/Iota化.md "wikilink")、ią、ję、ją。然而ę和ą的发音并不是直接发源于由大jus或小jus表示的发音，却是出自其他波兰语中融合的鼻化音并再次演变。（与波兰语最密切的[卡舒比语使用](../Page/卡舒比语.md "wikilink")[ã而不是ę](../Page/ã.md "wikilink")。）

小jus和大jus也在一直用到大约1860年。小jus用来表示，而大jus用来表示。

## 字符编码

## 参考资料

<references>

\[3\]

</references>

## 参看

  - <span lang="ru" style="font-size:120%;">[Я
    я](../Page/Я.md "wikilink")</span>（西里尔字母）
  - <span lang="ru" style="font-size:120%;">[Ѣ
    ѣ](../Page/Ѣ.md "wikilink")</span>（西里尔字母）

[Category:西里尔字母](../Category/西里尔字母.md "wikilink")

1.
2.  October 27, 1955 entry in Bernstein's diary, [Зигзаги
    памяти](http://macedonia.kroraina.com/sb2/sb_1955.htm).
    Bernstein transcribed the words as рънка, чендо.

3.