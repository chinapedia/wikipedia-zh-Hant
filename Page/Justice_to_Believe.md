****是[水樹奈奈第](../Page/水樹奈奈.md "wikilink")14枚[單曲CD](../Page/單曲.md "wikilink")。
於2006年11月15日由KING RECORDS發售。

## 收錄曲

1.  Justice to Believe

      - 作詞：水樹奈奈、作曲・編曲：[上松範康](../Page/上松範康.md "wikilink")（[Elements
        Garden](../Page/Elements_Garden.md "wikilink")）
      - PS2用遊戲『[WILD ARMS the Vth
        Vanguard](../Page/狂野歷險_the_Vth_Vanguard.md "wikilink")』主題曲
      - 於精選碟『[THE MUSEUM](../Page/THE_MUSEUM.md "wikilink")』中收錄MUSEUM
        STYLE版本

2.    - 作詞：Bee'、作曲・編曲：AGENT-MR
      - 東京電視台『』片尾曲

3.  Justice to Believe (without NANA)

4.  (without NANA)

[Category:2006年單曲](../Category/2006年單曲.md "wikilink")
[Category:水樹奈奈單曲](../Category/水樹奈奈單曲.md "wikilink")
[Category:遊戲主題曲](../Category/遊戲主題曲.md "wikilink")