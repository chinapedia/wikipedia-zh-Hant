**Jython**（原**JPython**），是一个用[Java语言写的](../Page/Java.md "wikilink")[Python](../Page/Python.md "wikilink")[解释器](../Page/解释器.md "wikilink")。

## 概述

Jython程序可以和Java无缝集成。除了一些标准模块，Jython使用Java的模块。Jython几乎拥有标准的Python中不依赖于[C语言的全部模块](../Page/C语言.md "wikilink")。比如，Jython的用户界面将使用[Swing](../Page/Swing_\(Java\).md "wikilink")，[AWT或者](../Page/AWT.md "wikilink")[SWT](../Page/SWT.md "wikilink")。Jython可以被动态或静态地编译成Java[字节码](../Page/字节码.md "wikilink")。

Jython还包括[jythonc](https://web.archive.org/web/20090606230123/http://www.jython.org/docs/jythonc.html)，一个将Python代码转换成Java代码的编译器。这意味着Python程序员能够将自己用Python代码写的类库用在Java程序里。

## 许可协议

Jython同时使用以下三种协议授权：\[1\]

1.  [Python软件基金会许可证](../Page/Python软件基金会许可证.md "wikilink")（v2）
2.  Jython 2.0, 2.1授權條款
3.  JPython 1.1.x軟體授權條款

前两个是[自由软件协议](../Page/自由软件协议.md "wikilink")。第三个还不清楚，因为还没有被[自由软件基金会接受](../Page/自由软件基金会.md "wikilink")。

## 历史

[Jim
Hugunin于](../Page/Jim_Hugunin.md "wikilink")1997年创造了Jython，并将它发展到1999年。1999年2月，[Barry
Warsaw接管了他的工作](../Page/Barry_Warsaw.md "wikilink")。2000年[十月](../Page/十月.md "wikilink")，Jython项目被移动到[SourceForge](../Page/SourceForge.md "wikilink")。很长一段时间内主要由[Samuele
Pedroni负责维护和开发Jython](../Page/Samuele_Pedroni.md "wikilink")。2004年底，Pedroni将精力集中在[PyPy](../Page/PyPy.md "wikilink")，但他仍然被认为是Jython内部管理层的一员。2005年1月，[Brian
Zimmer因开发Jython而得到](../Page/Brian_Zimmer.md "wikilink")[Python软件基金会的资助](../Page/Python软件基金会.md "wikilink")。2005年12月，[Frank
Wierzbicki作为主要开发者接替了Zimmer的工作](../Page/Frank_Wierzbicki.md "wikilink")。2005年，Jython的发展因为缺少优秀的开发者而放缓

2008年3月3日，-{zh-hans:[太阳计算机公司](../Page/太阳计算机公司.md "wikilink");zh-hant:[昇陽電腦公司](../Page/昇陽電腦公司.md "wikilink");}-就像之前雇佣2名[JRuby开发者那样请来](../Page/JRuby.md "wikilink")[Ted
Leung和Frank](../Page/Ted_Leung.md "wikilink")
Wierzbicki为Jython和Python工作。\[2\] 开发进度稳步增长。\[3\]\[4\]
Jython现在甚至可以运行[Django](../Page/Django.md "wikilink")（类似于[Ruby on
Rails的](../Page/Ruby_on_Rails.md "wikilink")[框架](../Page/框架.md "wikilink")）.\[5\]

## 现状和未来

当前的Jython版本是2017年7月1日发布的Jython 2.7.1

## Jython的优点

  - 与相似的[Java程序相比](../Page/Java.md "wikilink")，Jython极大的的减少了编程代码量。
  - Jython同时拥有[解释器和](../Page/解释器.md "wikilink")[编译器](../Page/编译器.md "wikilink")，使其无需编译就可以测试程序代码。

## 参见

  - [IronPython](../Page/IronPython.md "wikilink")，Python的.NET平台解释器。
  - [Python](../Page/Python.md "wikilink")
  - [PyPy](../Page/PyPy.md "wikilink")

## 参考资料

## 外部链接

  - [Jython Home Page](http://www.jython.org/)
  - [Jython Sourceforge Page](http://sourceforge.net/projects/jython/)
  - [differences between CPython and
    Jython](http://jython.sourceforge.net/docs/differences.html)
  - [Charming Jython: Learn how the Java implementation of Python can
    aid your development
    efforts](http://www-106.ibm.com/developerworks/java/library/j-jython.html)
  - [Get to know
    Jython](http://www-106.ibm.com/developerworks/library/j-alj07064/)
  - [Learn how to write DB2 JDBC tools in
    Jython](http://www-106.ibm.com/developerworks/db2/library/techarticle/dm-0404yang/index.html)
  - [Tips for Scripting Java with
    Jython](http://www.onjava.com/pub/a/onjava/2002/03/27/jython.html)
  - [Jython tips for Python
    programmers](http://www.onlamp.com/pub/a/python/2002/04/11/jythontips.html)
  - [Jython license
    information](http://www.jython.org/Project/license.html)
  - [Scripting on the Java
    platform](http://www.javaworld.com/javaworld/jw-11-2007/jw-11-jsr223.html)
  - [Jython
    Bibliography](https://web.archive.org/web/20090215204309/http://www.fishandcross.com/blog/?page_id=214)

[Category:Python解释器](../Category/Python解释器.md "wikilink")
[Category:脚本语言](../Category/脚本语言.md "wikilink")
[Category:面向对象的程序设计](../Category/面向对象的程序设计.md "wikilink")
[Category:JVM程式語言](../Category/JVM程式語言.md "wikilink")

1.
2.
3.
4.
5.