**K-Meleon**是一個[開放原始碼的](../Page/開源軟體.md "wikilink")[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")[網頁瀏覽器](../Page/網頁瀏覽器.md "wikilink")。基於和[Mozilla
Firefox](../Page/Mozilla_Firefox.md "wikilink")、[SeaMonkey相同的](../Page/SeaMonkey.md "wikilink")[Gecko](../Page/Gecko.md "wikilink")[排版引擎](../Page/排版引擎.md "wikilink")。K-Meleon的設計目標是提供一種快速和可靠的網路瀏覽器，同時提供高度定制的介面，並且有效地利用系統資源。在[GNU通用公共授權條款下發佈](../Page/GNU通用公共授權條款.md "wikilink")。

因其更少的資源需求，K-Meleon比大多其他基於Gecko的網頁瀏覽器反應更靈敏。這對資源有限的老硬件系統有益。

## 參考資料

<div class="references-small">

<references />

</div>

## 参见

  - [網頁瀏覽器比較](../Page/網頁瀏覽器比較.md "wikilink")
  - [網頁瀏覽器列表](../Page/網頁瀏覽器列表.md "wikilink")

## 外部链接

  - [K-Meleon](http://kmeleon.sourceforge.net/)
  - [K-Meleon 非官方正體中文](http://kmeleon-tw.wikidot.com/)

[Category:2000年軟體](../Category/2000年軟體.md "wikilink")
[Category:自由网页浏览器](../Category/自由网页浏览器.md "wikilink")
[Category:Gecko衍生軟體](../Category/Gecko衍生軟體.md "wikilink")
[Category:綠色軟件](../Category/綠色軟件.md "wikilink")
[Category:Windows獨占自由軟體](../Category/Windows獨占自由軟體.md "wikilink")