**K3b**（來自**KDE Burn Baby Burn**）\[1\] 是一個設計給
[KDE在](../Page/KDE.md "wikilink")[Linux或其它](../Page/Linux.md "wikilink")[類Unix系統上的](../Page/類Unix系統.md "wikilink")[CD](../Page/CD.md "wikilink")／[DVD燒錄的](../Page/DVD.md "wikilink")[自由軟體](../Page/自由軟體.md "wikilink")
。K3b 是用[C++](../Page/C++.md "wikilink")
[程式語言和](../Page/程式語言.md "wikilink")[Qt圖形界面開發工具寫成的](../Page/Qt.md "wikilink")。
K3b提供了圖形用戶介面給用戶進行CD/DVD燒錄工作，例如複製CD/DVD、製作音樂光碟，或者進一步燒錄[eMoviX光碟](../Page/eMoviX.md "wikilink")，更可以直接從光碟複製到另一片光碟，程式也提供很多可以讓進階用戶修改的設定。K3b只提供圖形介面，實際上光碟燒錄是由[CLI工具](../Page/命令行界面.md "wikilink")
[cdrecord](../Page/:en:cdrecord.md "wikilink")、[wodim](../Page/:en:wodim.md "wikilink")、[cdrdao和](../Page/:en:cdrdao.md "wikilink")[growisofs完成的](../Page/:en:growisofs.md "wikilink")。

從1.0版開始，K3b提供了內建的音樂／DVD光碟讀取程式。

K3b 在 2006 年
[LinuxQuestions.org](../Page/LinuxQuestions.org.md "wikilink") 的票選活動以
70% 得票率，獲選為年度多媒體工具。(Multimedia Utility of the Year) \[2\]

## 特色

K3b 的主要特色如下:

  - 燒錄資料 CD/DVD
  - 燒錄音樂 CD
  - 支援 [CD Text](../Page/CD_Text.md "wikilink")
  - 支援
    [藍光光碟](../Page/藍光光碟.md "wikilink")\[3\]/[DVD-R](../Page/DVD-R.md "wikilink")/[DVD+R](../Page/DVD+R.md "wikilink")/[DVD-RW](../Page/DVD-RW.md "wikilink")/[DVD+RW](../Page/DVD+RW.md "wikilink")
  - 支援
    [CD-R](../Page/CD-R.md "wikilink")/[CD-RW](../Page/CD-RW.md "wikilink")
  - 支援 [Mixed Mode CD](../Page/Mixed_Mode_CD.md "wikilink")
    (一片光碟上可以同時儲存音樂及資料)
  - 支援多段燒錄
  - [eMovix](../Page/eMovix.md "wikilink")
    CD/[eMovix](../Page/eMovix.md "wikilink") DVD
  - 複製 CD 到 CD 或是 DVD 到 DVD
  - 抹除 CD-RW/DVD-RW/DVD+RW
  - 支援 ISO 映像檔

## 參考

## 外部連結

  - [K3b website](http://www.k3b.org/)
  - [The K3b
    Handbook](http://docs.kde.org/stable/en/extragear-multimedia/k3b/index.html)
  - [The K3b Handbook
    (pdf)](http://docs.kde.org/stable/en/extragear-multimedia/k3b/k3b.pdf)

<!-- end list -->

  - [Tutorial to burn a multisession CD in linux using
    K3b](https://web.archive.org/web/20071009213048/http://www.theblackpawn.com/step-by-step-tutorial-to-burn-a-multisession-cd-in-linux.php)

[Category:KDE](../Category/KDE.md "wikilink") [Category:KDE
Extragear](../Category/KDE_Extragear.md "wikilink")
[Category:SourceForge专案](../Category/SourceForge专案.md "wikilink")
[Category:免費軟體](../Category/免費軟體.md "wikilink")
[Category:光盘创作软件](../Category/光盘创作软件.md "wikilink")

1.
2.
3.