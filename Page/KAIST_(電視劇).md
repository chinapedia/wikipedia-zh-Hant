《**KAIST**》（，港譯：**科技大學**），是[韓國](../Page/大韓民國.md "wikilink")[SBS電視台製作的電視劇](../Page/SBS_\(韓國\).md "wikilink")，[宋智娜作家執筆](../Page/宋智娜.md "wikilink")，於1999年1月24日開始播出，劇情關於在[韓國科學技術院](../Page/韓國科學技術院.md "wikilink")（KAIST）就讀的大學生的生活。由於本劇很受大眾歡迎，所以其後又再製作續集，共兩季。第1集\~第67集為第一季，第68集\~第81集為第二季。

## 演員陣容

  - [李輝香](../Page/李輝香.md "wikilink") 飾演 李熙貞教授
  - [金基賢](../Page/金基賢.md "wikilink") 飾演 金大賢教授
  - [安政勳](../Page/安政勳.md "wikilink") 飾演 朴基勳教授
  - [白鐘學](../Page/白鐘學.md "wikilink") 飾演 徐教授
  - [李珉宇](../Page/李珉宇.md "wikilink") 飾演 李民才
  - [蔡琳](../Page/蔡琳.md "wikilink") 飾演 朴采瑩
  - [申恩廷](../Page/申恩廷.md "wikilink") 飾演 申南熙
  - [李恩宙](../Page/李恩宙.md "wikilink") 飾演 邱志苑
  - [鄭成化](../Page/鄭成化.md "wikilink") 飾演 鄭萬秀
  - [金正鉉](../Page/金正鉉.md "wikilink") 飾演 金正太
  - [鄭民](../Page/鄭民.md "wikilink") 飾演 崔載明
  - [許英蘭](../Page/許英蘭.md "wikilink") 飾演 吳玉珠
  - Michael 飾演 Michael
  - [姜成妍](../Page/姜成妍.md "wikilink") 飾演 閔卿珍
  - [秋瓷炫](../Page/秋瓷炫.md "wikilink") 飾演 秋瓷炫
  - [崔明洙](../Page/崔明洙.md "wikilink") 飾演 崔一漢教授
  - [李奈映](../Page/李奈映.md "wikilink") 飾演 李惠成
  - [池晟](../Page/池晟.md "wikilink") 飾演 姜大旭
  - [金柱赫](../Page/金柱赫.md "wikilink") 飾演 鄭明煥
  - [金正閔](../Page/金正閔.md "wikilink") 飾演 鄭進秀
  - [延政勳](../Page/延政勳.md "wikilink") 飾演 梁炳錫
  - [李奎翰](../Page/李奎翰.md "wikilink") 飾演 李奎翰
  - [柳重熙](../Page/柳重熙.md "wikilink") 飾演 柳重熙（真正的KAIST在學生）
  - [金燦宇](../Page/金燦宇.md "wikilink") 飾演 金碩佑
  - [李原在](../Page/李原在.md "wikilink") 飾演 鄭慶植
  - [李在煌](../Page/李在煌.md "wikilink") 飾演 李在誠（第二季）
  - [洪洙賢](../Page/洪洙賢.md "wikilink") 飾演 洪洙進（第二季）
  - [奇太映](../Page/奇太映.md "wikilink") 飾演 奇太勳（第二季）
  - [金玟廷](../Page/金玟廷.md "wikilink") 飾演 金玟熙（第二季）
  - [金在仁](../Page/金在仁.md "wikilink") 飾演 華卿（第二季）
  - [宋恩英](../Page/宋恩英.md "wikilink") 飾演 金寒兒（第二季）
  - [高斗旭](../Page/高斗旭.md "wikilink") 飾演 尚哲（第二季）
  - [李自英](../Page/李自英.md "wikilink") 飾演 珍熙（第二季）
  - [趙顯宰](../Page/趙顯宰.md "wikilink") 飾演 張東和（第二季）
  - [洪景仁](../Page/洪景仁.md "wikilink") 飾演 洪樹利（第二季）
  - [金麗珍](../Page/金麗珍.md "wikilink") 飾演 殷奈映教授（第二季）
  - [金承洙](../Page/金承洙.md "wikilink") 飾演 吳賢民教授（第二季）
  - [金昌完](../Page/金昌完.md "wikilink") 飾演 航空科教授（第二季）
  - [白一燮](../Page/白一燮.md "wikilink") 飾演 系主任（第二季）
  - [尹汝貞](../Page/尹汝貞.md "wikilink") 飾演 指導教授（第二季）
  - [金美京](../Page/金美京.md "wikilink") 飾演 金美順
  - [李斗日](../Page/李斗日.md "wikilink") 飾演 校園警察（12集\~81集）
  - [安貞渙](../Page/安貞渙.md "wikilink")
  - [金貞賢](../Page/金貞賢.md "wikilink")
  - [丁汶](../Page/丁汶.md "wikilink")
  - [尹智敏](../Page/尹智敏.md "wikilink")
  - [鄭仁先](../Page/鄭仁先.md "wikilink")
  - [韓尚進](../Page/韓尚進.md "wikilink") 飾演 徐東植
  - [金正旭](../Page/金正旭.md "wikilink")

### 特別演出

  - [李秉憲](../Page/李秉憲.md "wikilink")
  - [金明民](../Page/金明民.md "wikilink")
  - [李亨哲](../Page/李亨哲.md "wikilink")
  - [宋在浩](../Page/宋在浩.md "wikilink")
  - [鄭永琡](../Page/鄭永琡.md "wikilink")
  - [鄭泰宇](../Page/鄭泰宇.md "wikilink")

## 外部連結

  - [SBS：KAIST電視劇網頁](https://web.archive.org/web/20040804155624/http://wizard.sbs.co.kr/template/wzdtv/wzdtv_FormProgramIntro.jhtml?programId=V0000225001&menuId=4)

[Category:1999年韓國電視劇集](../Category/1999年韓國電視劇集.md "wikilink")
[Category:2000年韓國電視劇集](../Category/2000年韓國電視劇集.md "wikilink")
[Category:SBS電視劇](../Category/SBS電視劇.md "wikilink")
[Category:大學背景電視劇](../Category/大學背景電視劇.md "wikilink")
[Category:韓國校園劇](../Category/韓國校園劇.md "wikilink")