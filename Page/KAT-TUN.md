**KAT-TUN**是日本[傑尼斯事務所旗下的藝人組合](../Page/傑尼斯事務所.md "wikilink")；於2001年3月16日組成，2006年3月22日正式發行CD出道。

團名原是由6名成員的姓氏羅馬拼音首字母所組成，因為在日文發音中與Cartoon（[卡通](../Page/卡通.md "wikilink")／[動畫](../Page/動畫.md "wikilink")）及“勝利的運氣”（）相近，而有著像卡通般精采以及勝利的含意；至於KAT-TUN中間的「-」（ハイフン），則代表的是粉絲。

出道發行的單曲、専輯與DVD，在發行首週即達成三項[公信榜冠軍](../Page/公信榜.md "wikilink")，成為日本史上甫出道即達成三項冠軍的團體
/ 歌手；另外也是USEN（有線）綜合排行冠軍、原唱鈴聲下載排行冠軍，而達成前所未有的五冠王佳績\[1\]。

## 概要

  - 於2001年3月16日組成，2006年3月22日正式發行CD出道。
    自組成至出道一共經過5年時間，是截至目前為止傑尼斯旗下除[Kis-My-Ft2之外組成最久才出道的團體](../Page/Kis-My-Ft2.md "wikilink")。
  - 2010年8月31日赤西仁因個人理念而退團。
  - 2013年9月30日田中聖因違反事務所合約而退團。
  - 2015年11月24日，田口淳之介於直播音樂節目中宣布2016年春天退團，並退出傑尼斯事務所。
  - 2016年3月31日田口淳之介退團並退出傑尼斯事務所。
  - 2016年5月1日進入團體充電期，暫停團體活動，只進行成員的個人活動。
  - 2018年1月1日於傑尼斯跨年演唱會宣布開始回歸。

## 成員

### 現任成員

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>羅馬拼音</p></th>
<th><p>代表字母</p></th>
<th><p>生日</p></th>
<th><p>出身地</p></th>
<th><p>代表色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/龜梨和也.md" title="wikilink">龜梨和也</a></strong> (かめなし かずや)</p></td>
<td><p><strong>Ka</strong>menashi Kazuya</p></td>
<td><center>
<p>KA</p>
</center></td>
<td></td>
<td><p><a href="../Page/東京都.md" title="wikilink">東京都</a><a href="../Page/江戶川區.md" title="wikilink">江戶川區</a></p></td>
<td><center>
<p><font color=pink>粉紅</font></p>
</center></td>
<td><p>擔任主唱。<br />
赤西仁退團前，代表字母為K</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/上田龍也.md" title="wikilink">上田龍也</a></strong> (うえだ たつや)</p></td>
<td><p><strong>U</strong>eda <strong>T</strong>a<strong>T</strong>suya</p></td>
<td><center>
<p>TTU</p>
</center></td>
<td></td>
<td><p><a href="../Page/神奈川縣.md" title="wikilink">神奈川縣</a></p></td>
<td><center>
<p><font color=blue>藍色</font></p>
</center></td>
<td><p>田中聖、田口淳之介退團前，代表字母為U</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/中丸雄一.md" title="wikilink">中丸雄一</a></strong><br />
(なかまる ゆういち)</p></td>
<td><p><strong>N</strong>akamaru Yuichi</p></td>
<td><center>
<p>N</p>
</center></td>
<td></td>
<td><p><a href="../Page/東京都.md" title="wikilink">東京都北區</a></p></td>
<td><center>
<p><font color=purple>紫色</font></p>
</center></td>
<td><p><a href="../Page/節奏口技.md" title="wikilink">Beat Box</a> 擔當</p></td>
</tr>
</tbody>
</table>

### 前成員

（2010年8月31日-2016年3月31日）

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>羅馬拼音</p></th>
<th><p>代表字母</p></th>
<th><p>生日</p></th>
<th><p>出身地</p></th>
<th><p>代表色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/赤西仁.md" title="wikilink">赤西仁</a></strong> (あかにし じん)</p></td>
<td><p><strong>A</strong>kanishi Jin</p></td>
<td><center>
<p>A</p>
</center></td>
<td></td>
<td><p><a href="../Page/東京都.md" title="wikilink">東京都</a></p></td>
<td><center>
<p><font color=red>紅色</font></p>
</center></td>
<td><p>與龜梨和也擔任雙主唱。<br />
2010年8月31日退團。<br />
2014年约满不续約退出杰尼斯事务所。</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/田口淳之介.md" title="wikilink">田口淳之介</a></strong><br />
(たぐち　じゅんのすけ)</p></td>
<td><p><strong>T</strong>aguchi Junnosuke</p></td>
<td><center>
<p>T</p>
</center></td>
<td></td>
<td><p><a href="../Page/神奈川縣.md" title="wikilink">神奈川縣</a></p></td>
<td><center>
<p><font color=orange>橘色</font></p>
</center></td>
<td><p>2016年3月31日退團及退出事务所。</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/田中聖.md" title="wikilink">田中聖</a></strong> (たなか　こうき)</p></td>
<td><p><strong>T</strong>anaka Koki</p></td>
<td><center>
<p>T</p>
</center></td>
<td></td>
<td><p><a href="../Page/千葉縣.md" title="wikilink">千葉縣</a><a href="../Page/柏市.md" title="wikilink">柏市</a></p></td>
<td><center>
<p><font color=gold>黃色</font></p>
</center></td>
<td><p><a href="../Page/饒舌.md" title="wikilink">Rap</a> 擔當<br />
2013年9月30日退團并被杰尼斯事务所解約。</p></td>
</tr>
</tbody>
</table>

## 入社時間及原由

  - [龜梨和也](../Page/龜梨和也.md "wikilink")
    ：1998年11月8日入社。親戚為其投遞的簡歷，在不知情的情況下被父親帶到甄選現場，硬著頭皮上場後還是甄選合格了。（與[中丸雄一及](../Page/中丸雄一.md "wikilink")
    [赤西仁同期](../Page/赤西仁.md "wikilink")。）
  - [上田龍也](../Page/上田龍也.md "wikilink")
    ：1998年6月22日入社。對什麼行業都興趣缺缺而嘗試了投遞簡歷，抱著預期落選的心情參加了甄試會，卻意外得到社長的賞識而合格。
  - [中丸雄一](../Page/中丸雄一.md "wikilink")
    ：1998年11月8日入社。同年級要好的女孩子幫忙準備了簡歷，因為在甄試過程中想要表現自己而大膽向社長提問：是不是帥才可進入事務所，由於這一問，得到了入社的機會。

## 簡歷

**2001年**

  - 3月16日，KAT-TUN組成。
  - 3月27日，在《[POP
    JAM](../Page/POP_JAM.md "wikilink")》音樂節目中，公開發表成為主持人[堂本光一](../Page/堂本光一.md "wikilink")（[KinKi
    Kids](../Page/KinKi_Kids.md "wikilink")）的專屬伴舞團體。（該集後於4月7日播出，也是該組合首次在電視上亮相。）
  - 3月30日，於《[MUSIC
    STATION](../Page/MUSIC_STATION.md "wikilink")》中以組合名義初登場。
  - 4月7日，傑尼斯Jr.主持的《[裸之少年](../Page/裸之少年.md "wikilink")》開始播出，KAT-TUN成員為節目固定班底；官網上登記此日為正式結成日。
  - 4月8日，於傑尼斯常態節目《[少年俱樂部](../Page/少年俱樂部.md "wikilink")》初登場。

**2002年**

  - 5月17\~19日，隨[瀧與翼參與](../Page/瀧與翼.md "wikilink") 「Tackey & Tsubasa in
    Taipei 從台灣開始\!...From us with LOVE」台灣演唱會。
  - 8月10日\~11日，在東京國際論壇展開「歌迷至聖 夏 Concert～回應55萬人愛的支持！！」首次演唱會。
      - 原本預計4場公演共2萬人次的演唱會，卻因來自全國55萬人次的申請人數太多，因而緊急追加2場，2天共舉行6場公演。
  - 8月26日\~28日，在[大阪](../Page/大阪.md "wikilink")·松竹座舉行追加公演。
      - 28日史無前例舉行了1天11次的公演，平均每場演出約45分鐘，打破同事務所前輩[TOKIO和](../Page/TOKIO.md "wikilink")[V6一天內](../Page/V6.md "wikilink")10次公演的日本記錄。
  - 9月3日，第一支單獨CM 「Lotte CRUNKY」播放。

**2003年**

  - 2月26日發行「歌迷至聖 夏
    Concert～回應55萬人愛的支持！！」演唱會DVD與VHS；這是尚未正式CD出道的團體裡首度獲得[ORICON排行榜DVD](../Page/ORICON.md "wikilink")、VHS、綜合榜三項冠軍的創舉。
  - 3月21\~22日，與[瀧澤秀明](../Page/瀧澤秀明.md "wikilink")、[YA-YA-YAH到](../Page/YA-YA-YAH.md "wikilink")[泰國舉行](../Page/泰國.md "wikilink")「PATAYA
    MUSIC FESTIVAL 2003」演唱會。

**2004年**

  - 12月28日\~2005年1月5日，東名阪召開巡迴演唱會「KAT-TUN Live
    海賊帆」。1月5日的2場演唱會，也是[橫濱公演的最終回](../Page/橫濱.md "wikilink")，是KAT-TUN的第100場公演。

**2005年**

  - 1月15日，由龜梨和也與赤西仁主演電視劇[極道鮮師2](../Page/極道鮮師.md "wikilink")（[日本電視台](../Page/日本電視台.md "wikilink")）開始播送。初回收視率為26.5%，為當週同時段播出戲劇之冠，也是該台的史上戲劇初回收視率第五名以及戲劇平均收視率第一。
  - 5月3日，「KAT-TUN Live
    海賊帆」DVD發售。初次銷售額12.9萬張，為ORICON音樂DVD初次銷售張數排行榜上歷代第3位，第2次DVD綜合排行榜第1位。
  - 11月2日，龜梨和也與[山下智久共組期間限定團體](../Page/山下智久.md "wikilink")「[修二與彰](../Page/修二與彰.md "wikilink")」，演唱電視劇「[野豬大改造](../Page/野豬大改造.md "wikilink")」主題曲「青春Amigo」，首度推出CD。該單曲發售4週後就達到百萬銷售量。成為2005年ORICON年終單曲榜冠軍。銷售量超過百萬是自2004年[森山直太朗的](../Page/森山直太朗.md "wikilink")「[櫻花](../Page/櫻花_\(森山直太朗\).md "wikilink")」以來，相隔了1年又7個月的壯舉。

**2006年**

  - 1月29日，宣布將於3月22日以單曲「[Real
    Face](../Page/Real_Face.md "wikilink")」CD正式出道。
  - 3月17日，在[東京巨蛋舉行](../Page/東京巨蛋.md "wikilink")「Live of KAT-TUN 'Real
    Face'」演唱會，並與MUSIC STATION節目現場連線直播Real
    Face出道曲。是史上首次出道前就在東京巨蛋舉行演唱會的團體\[2\]。
  - 3月22日，出道記者會，以單曲「[Real
    Face](../Page/Real_Face.md "wikilink")」正式出道。同日發售專輯「[Best
    of KAT-TUN](../Page/Best_of_KAT-TUN.md "wikilink")」和DVD「Real Face
    Film」。並同時成立J-One Records。
      - 創造「Real Face」75.4萬張，「Best of KAT-TUN」55.7萬張，「Real Face
        Film」37.4萬張首週銷售額的記錄，達成三冠王\[3\]，成為史上首次新人出道即達成三冠王的紀錄，同時也是自2000年10月9日[濱崎步的單曲](../Page/濱崎步.md "wikilink")《[SURREAL](../Page/SURREAL.md "wikilink")（超現實）》、專輯《[Duty](../Page/Duty.md "wikilink")（以聲作責）》及演唱會DVD《[ayumi
        hamasaki concert tour 2000 A
        第2幕](../Page/ayumi_hamasaki_concert_tour_2000_A_第2幕.md "wikilink")》以來，隔5年半歷史上的第2組。另外也是USEN（有線）綜合排行冠軍、原唱鈴聲下載排行冠軍，因此擁有五冠王的佳績。
      - 單曲連續三週獲得冠軍。是自KinKi
        Kids「[Anniversary](../Page/Anniversary.md "wikilink")」以來，相隔了1年3個月再次出現的連續三周冠軍。至於出道單曲3週冠軍的成就，是自KinKi
        Kids「[玻璃少年](../Page/玻璃少年.md "wikilink")」以來隔了8年8個月，而作為2000年以後出道的團體則是第一個。
  - 3月28日\~5月14日，出道後的首次全國巡迴演唱會「Spring Tour'06 Live of KAT-TUN“Real
    Face”」，總計共43場公演。
  - 4月3日，「You &
    J」即KAT-TUN、[NEWS](../Page/NEWS.md "wikilink")、[關∞的聯合歌迷俱樂部正式開始](../Page/關∞.md "wikilink")。
  - 5月23日，首張單曲「Real
    Face」發售9週後達到百萬銷售量。同時，在年度內（前一年12月第一週到該年11月最後一周）的成就，是自2003年3月24日
    [SMAP](../Page/SMAP.md "wikilink")「[世界上唯一的花](../Page/世界上唯一的花.md "wikilink")」以來的百萬單曲，相隔了3年又2月。另外以出道曲而言，是自2001年6月25日（發售15週後）的[CHEMISTRY](../Page/CHEMISTRY.md "wikilink")「PIECES
    OF A DREAM」以來，相隔了約5年的壯舉。
  - 10月12日，媒體報導赤西仁將休止今年內的活動，當晚事務所網站公告其將暫停活動到[洛杉磯留學](../Page/洛杉磯.md "wikilink")。13日赤西本人於事務所內召開記者招待會，宣布並非引退或退團，但未說明復歸日期，及未承諾必定復歸，留給媒體更多的討論空間。
  - 10月16日，赤西仁出國到LA留學。
  - 12月7日，發售第三張單曲《[僕らの街で](../Page/僕らの街で.md "wikilink")（[在我們的城市裡](../Page/在我們的城市裡.md "wikilink")）》。出道以來連續三張單曲皆突破40萬大關。此外更獨佔日本2006年首週銷售排行榜的前三名：《Real
    Face》75.4萬、《[SIGNAL](../Page/SIGNAL.md "wikilink")》45.1萬和《僕らの街で》的41萬。再加上《Best
    of KAT-TUN》專輯跟《Real Face
    Film》DVD等五項作品在出道當年都獲得第一，超越了2001年CHEMISTRY、1986年[少年隊以及](../Page/少年隊.md "wikilink")1981年的[近藤真彥的四項作品](../Page/近藤真彥.md "wikilink")，成為五冠王。

**2007年**

  - 4月4日，冠名節目「[cartoon
    KAT-TUN](../Page/cartoon_KAT-TUN.md "wikilink")」正式播出。
  - 4月18日，第二張專輯《[cartoon KAT-TUN II
    You](../Page/cartoon_KAT-TUN_II_You.md "wikilink")》發售。達成組團以來包含單曲、專輯，DVD連續十張作品第一名的紀錄。
  - 4月19日，活動休止的赤西仁歸國。20日，6名成員於[六本木召開記者會](../Page/六本木.md "wikilink")，宣布赤西仁將於5人正進行的巡迴演唱會中，在[仙台站以客串身份露面](../Page/仙台.md "wikilink")，並於5月3日大阪場開始參與演出；完整的6人公演是6月開始的四場巨蛋公演。

**2008年**

  - 2月6日，第六張單曲《[LIPS](../Page/LIPS.md "wikilink")》發售。KAT-TUN連續六張單曲首週銷售量皆突破30萬張。
  - 5月14日，第七張單曲《[DON'T U EVER
    STOP](../Page/DON'T_U_EVER_STOP.md "wikilink")》發售，不但打破上張單曲「LIPS」的紀錄，更拿下2008年首週銷售（381,672張）最高的寶座。
  - 6月21日\~8月5日，開始「KAT-TUN Concert Tour 2008」（6月13日更名為「KAT-TUN LIVE TOUR
    2008 QUEEN OF PIRATES」）全國巡迴演唱會，總計31場，觀眾人次高達50萬。成為連續4日在東京巨蛋公演的日本歌手首例。
  - 8月10日，成員中第一位展開個人公演的中丸雄一，在東京首演「中丸的快樂時光」舞台劇，並親自構思製作所有的周邊商品。東京公演日期為8月10日\~8月24日；大阪公演則為8月27日\~9月2日。
  - 9月8日，成員中首位展開個人演唱會的上田龍也，以「Mouse Peace Tatsuya Ueda Live
    2008」為名稱，在東京[台場開演](../Page/台場.md "wikilink")。從9月8日\~9月21日，共14場公演。

**2009年**

  - 5月15日\~5月22日東京巨蛋創紀錄連續8天公演\[4\]。製作費高達32億日圓，動用Jr.約250人，總計現場工作人員6000人、警衛人員12000人。並突破消防法規的限制、首度於東京巨蛋內實行吊鋼索飛行。8天連續公演記錄將列入[金氏世界紀錄之中](../Page/金氏世界紀錄.md "wikilink")。另外夏季在6城市追加17場公演，動員人数達到81萬5千。
  - 11月23日，職棒[讀賣巨人隊感謝祭於東京巨蛋舉行](../Page/讀賣巨人.md "wikilink")。因與[中居正廣主持的](../Page/中居正廣.md "wikilink")「黑色綜藝」節目交好，特別以黑玫瑰隊名舉行一場三局對抗賽。其中龜梨擔任第一打擊及[投手特別出演](../Page/投手.md "wikilink")。最終黑玫瑰以3:2擊敗巨人隊。隔天報紙大肆報導龜梨與剛加入巨人隊首度亮相的[長野久義選手間的對決](../Page/長野久義.md "wikilink")。
  - 11月25日，赤西以「[LANDS](../Page/LANDS.md "wikilink")」的名義（個人姿態）發表《[BANDAGE](../Page/BANDAGE.md "wikilink")》單曲\[5\]。
  - 12月13日，隔了5年才舉辦的「傑尼斯選拔大運動會」在東京巨蛋展開，分成瀧澤率領紅組對抗龜梨率領的白組。競賽分為棒球、足球及接力賽跑三項。白組獲得優勝。龜梨為棒球[MVP](../Page/MVP.md "wikilink")、中丸為足球MVP，各得獎金100萬日幣。

**2010年**

  - 1月19日，赤西仁為電影《BANDAGE》主演以及樂隊LANDS的一員在東京[涉谷AX舉行了一夜限定的演唱會](../Page/涉谷.md "wikilink")，2場演出一共吸引了2800名觀眾。
  - 3月25日，公布除了赤西仁以外的五名團員自5月開始展開日本國內巡迴（10地共21場）；7月中旬展開首度世界巡迴，行程：東京巨蛋（7/16，7/17，7/24，7/25）、泰國（7/31）、[韓國](../Page/韓國.md "wikilink")[首爾](../Page/首爾.md "wikilink")（8/6，8/7）、[大阪巨蛋](../Page/大阪巨蛋.md "wikilink")（8/21，8/22）、台灣[台北](../Page/台北.md "wikilink")（8/27，8/28）。26日台灣媒體各報多以「KAT-TUN登台
    赤西仁缺席」為標題。\[6\]
  - 5月16日，新聞公布，原訂7月31日在泰國舉行的演唱會，因政情局勢不穩定而取消。
  - 6月13日，在公布售票時間不到一天之內，台灣演唱會發售兩個小時內售票超過九成，並一度癱瘓系統\[7\]，締造國外藝人來台舉辦演唱會售票的銷售新紀錄。
  - 6月24日，2萬張的韓國演唱會門票，在發售20分鐘內銷售完畢。
  - 7月1日，為宣傳《[NO MORE
    PAIN](../Page/NO_MORE_PAIN.md "wikilink")》專輯在韓發行，團員應[Mnet媒體邀請](../Page/Mnet媒體.md "wikilink")，至韓國參加音樂節目表演，並首度於海外舉行記者招待會。於隔日返國。
  - 7月3日\~4日，舉行「KAT-TUN SUMMER PREMIUM
    2010」首次粉絲見面握手會，7/3於大阪（龜梨）、7/4於[札幌](../Page/札幌.md "wikilink")（田口）、仙台（上田）、[熊本](../Page/熊本.md "wikilink")（中丸）、東京（田中）。
  - 7月16日，[Johnny喜多川社長在世界巡迴開演首日](../Page/Johnny喜多川.md "wikilink")，宣佈赤西將脫離團體。其他五人在開演前記者會則強調「KAT-TUN不會解散」。
  - 7月20日，赤西本人在傑尼斯官方KAT-TUN的手機網誌中，承認退團的事實，並表達其單飛並非與團員有所不合。
  - 8月7日\~8日，首次海外巡迴的韓國場。
  - 8月27日\~28日，海外巡迴的最終站台灣場。總計今年巡迴動員人數高達97萬2千5百人。
  - 8月31日，傑尼斯官方網站正式將赤西仁從「KAT-TUN」頁面移除。
  - 10月5日，龜梨與女歌手[倖田來未連繼](../Page/倖田來未.md "wikilink")5年連霸[牛仔褲名人獎](../Page/牛仔褲.md "wikilink")「Best
    Jeanist」而進入牛仔褲名人堂\[8\]，也是史上首次男女同時入殿堂。
  - 11月28日，推出的新單曲《[CHANGE UR
    WORLD](../Page/CHANGE_UR_WORLD.md "wikilink")》中的通常版第三首〈NEVER x OVER
    ～｢-｣ IS YOUR
    PART～〉爆發前奏與副歌盜用[AVTechNO於](../Page/AVTechNO.md "wikilink")2010年2月1日上傳至[NICONICO動畫的](../Page/NICONICO動畫.md "wikilink")〈DYE〉旋律之疑雲。

**2011年**

  - 1月19日，〈NEVER x OVER ～｢-｣ IS YOUR
    PART～〉作曲者承認抄襲，AVTechNO認為這不是KAT-TUN本身的錯\[9\]。
  - 5月30日，原先預計於七月中旬舉行的[工業區演唱會及](../Page/工業區.md "wikilink")5月至10月舉行的「5大巨蛋巡迴」宣佈中止，原因為3月的[東日本大震災及與職業棒球的開幕時間衝突](../Page/東日本大震災.md "wikilink")。
  - 6月14日，龜梨和也、田中聖及中丸雄一出席[帝國劇場](../Page/帝國劇場.md "wikilink")100周年舞台劇《[DREAM
    BOYS](../Page/DREAM_BOYS.md "wikilink")》製作發表會，並宣布此劇在9月3至25日公演期間，組合將改名為「勝運」，意思是希望為觀眾帶來勝利的運氣\[10\]。

**2012年**

  - 8月24日，KAT-TUN全體主持的[TBS節目](../Page/TBS.md "wikilink")「KAT-TUN的世界上最糟糕的夜晚」冠名節目開播。

**2013年**

  - 5月15日，第21張單曲「[Face to
    Face](../Page/Face_to_Face.md "wikilink")」發行，此單曲達成出道以來21張單曲首週登上公信榜冠軍的連冠紀錄\[11\]。
  - 10月9日，宣布田中聖因嚴重違規被傑尼斯事務所解約而脫離團體。

**2014年**

  - 4月，KAT-TUN開始主持「少年俱樂部premium」。

**2015年**

  - 11月24日，田口淳之介在[日本電視台的直播節目中宣布將於](../Page/日本電視台.md "wikilink")2016年春天退團。\[12\]。

**2016年**

  - 2月13日，透過官網宣布5月後將進入充電期，團體活動暫停、團員各自發展但不解散。
  - 3月2日，發行第26張單曲「UNLOCK」，此單曲達成出道以來連續26張單曲首週登上公信榜冠軍的26連冠紀錄。\[13\]。
  - 3月31日，田口淳之介正式退團。
  - 4月3日\~5月1日，舉行「KAT-TUN 10TH ANNIVERSARY LIVE TOUR 10Ks！」出道10週年紀念公演。
  - 5月1日，最後一場10周年紀念公演後，KAT-TUN正式進入充電期。

**2018年**

  - 1月1日，在[傑尼斯跨年演唱會](../Page/傑尼斯跨年演唱會.md "wikilink")2017-2018宣布復出。

## 作品集

*※有關成員個人名義之演出，請參見個別項目。*

### 單曲

|                                                                            |                                                                         |                                                                                          |                                                                        |
| -------------------------------------------------------------------------- | ----------------------------------------------------------------------- | ---------------------------------------------------------------------------------------- | ---------------------------------------------------------------------- |
| 1\.[Real Face](../Page/Real_Face.md "wikilink")(2006年3月22日)                | 2\.[SIGNAL](../Page/SIGNAL.md "wikilink")(2006年7月19日)                   | 3\.[在我們的城市裡](../Page/在我們的城市裡.md "wikilink")(2006年12月7日)                                  | 4\.[歡喜之歌](../Page/歡喜之歌.md "wikilink")(2007年6月6日)                       |
| 5\.[Keep the faith](../Page/Keep_the_faith.md "wikilink")(2007年11月21日)     | 6\.[LIPS](../Page/LIPS.md "wikilink")(2008年2月6日)                        | 7\.[DON'T U EVER STOP](../Page/DON'T_U_EVER_STOP.md "wikilink")(2008年5月14日)              | 8\.[White X'mas](../Page/White_X'mas.md "wikilink")(2008年12月3日)        |
| 9\.[ONE DROP](../Page/ONE_DROP.md "wikilink")(2009年2月11日)                  | 10\.[RESCUE](../Page/RESCUE.md "wikilink")(2009年3月11日)                  | 11\.[Love yourself～喜歡你所討厭的你～](../Page/Love_yourself～喜歡你所討厭的你～.md "wikilink")(2010年2月10日) | 12\. [Going\!](../Page/Going!.md "wikilink")(2010年5月12日)               |
| 13\. [CHANGE UR WORLD](../Page/CHANGE_UR_WORLD.md "wikilink")(2010年11月17日) | 14\.[ULTIMATE WHEELS](../Page/ULTIMATE_WHEELS.md "wikilink")(2011年2月2日) | 15\.[WHITE](../Page/WHITE_\(KAT-TUN單曲\).md "wikilink")(2011年5月18日)                       | 16\.[RUN FOR YOU](../Page/RUN_FOR_YOU.md "wikilink")(2011年8月3日)        |
| 17\.[BIRTH](../Page/BIRTH.md "wikilink")(2011年11月30日)                      | 18\.[TO THE LIMIT](../Page/TO_THE_LIMIT.md "wikilink")(2012年6月27日)      | 19\.[永不滅的團結](../Page/永不滅的團結.md "wikilink")(2012年9月12日)                                   | 20\.[EXPOSE](../Page/EXPOSE.md "wikilink")(2013年2月6日)                  |
| 21\.[FACE to Face](../Page/FACE_to_Face.md "wikilink")(2013年5月15日)         | 22\.[In Fact](../Page/In_Fact.md "wikilink")(2014年6月4日)                 | 23\.[Dead or Alive](../Page/Dead_or_Alive.md "wikilink")(2015年1月21日)                     | 24\.[KISS KISS KISS](../Page/KISS_KISS_KISS.md "wikilink")（2015年3月11日） |
| 25\.[TRAGEDY](../Page/TRAGEDY.md "wikilink")(2016年2月10日)                   | 26\.[UNLOCK](../Page/UNLOCK.md "wikilink")(2016年3月2日)                   | 27\.[Ask Yourself](../Page/Ask_Yourself.md "wikilink")（2018年5月18日）                       |                                                                        |

### 專輯

|                                                                                                                |                                                                                       |                                                                                                      |                                                                                                                   |
| -------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------- |
| 1\. [Best of KAT-TUN](../Page/Best_of_KAT-TUN.md "wikilink")(2006年3月22日)                                       | 2\.[cartoon KAT-TUN II You](../Page/cartoon_KAT-TUN_II_You.md "wikilink")(2007年4月18日) | 3\.[KAT-TUN III -QUEEN OF PIRATES-](../Page/KAT-TUN_III_-QUEEN_OF_PIRATES-.md "wikilink")(2008年6月4日) | 4\.[Break the Records -by you & for you-](../Page/Break_the_Records_-by_you_&_for_you-.md "wikilink")(2009年4月29日) |
| 5\. [NO MORE PAIИ](../Page/NO_MORE_PAIИ.md "wikilink")(2010年6月16日)                                             | 6\. [CHAIN](../Page/CHAIN.md "wikilink")(2012年2月22日)                                  | 7\.[楔-kusabi-](../Page/楔-kusabi-.md "wikilink")(2013年11月27日)                                         | 8\.[come Here](../Page/come_Here.md "wikilink")(2014年6月25日)                                                       |
| 9\. [KAT-TUN 10TH ANNIVERSARY BEST 10Ks](../Page/KAT-TUN_10TH_ANNIVERSARY_BEST_10Ks.md "wikilink")(2016年3月22日) | 10\. [CAST](../Page/CAST.md "wikilink")（2018年7月18日）                                   |                                                                                                      |                                                                                                                   |

### DVD

|                                                      |                                                   |                                                           |                                                                      |
| ---------------------------------------------------- | ------------------------------------------------- | --------------------------------------------------------- | -------------------------------------------------------------------- |
| 1.歌迷至聖 夏 Concert～回應55萬人愛的支持！！(2003年2月26日)            | 2.SUMMARY of Johnnys World(2005年4月20日)            | 3.KAT-TUN Live 海賊帆()(2005年5月3日)                           | 4.Real Face Film(2006年3月22日)                                         |
| 5.DREAM BOYS(2006年6月28日)                             | 6.Live of KAT-TUN "Real Face"(2007年4月11日)         | 7.TOUR 2007 cartoon KAT−TUN II You(2007年11月21日)           | 8.DREAM BOYS(2008年2月27日)                                             |
| 9.KAT-TUN LIVE TOUR 2008 QUEEN OF PIRATES(2009年1月1日) | 10.KAT-TUN LIVE Break the Records(2009年12月16日)    | 11.KAT-TUN-NO MORE PAIИ-WORLD TOUR 2010(2010年12月29日)      | 12.KAT-TUN LIVE TOUR 2012 CHAIN TOKYO DOME(2012年11月21日)              |
| 13.COUNTDOWN LIVE 2013 KAT-TUN(2014年5月14日)           | 14\. KAT-TUN LIVE TOUR 2014 come Here(2015年4月22日) | 15.KAT-TUN LIVE 2015 "quarter" in TOKYO DOME(2015年10月14日) | 16.KAT-TUN 10TH ANNIVERSARY LIVE TOUR 10Ks IN TOKYO DOME(2016年8月17日) |

### 寫真集

  - 『KAT-TUN 1st in New York』（2003年）

  - 『KAT-TUNライブ・ドキュメント・フォトブック“BREAK the RECORDS”』（2009年8月31日）

### 未收錄於單曲及專輯的原唱曲

  -
  -
  -
  -
  - Six Groove

  -
  - Change

  - 　

  - DESTINY

  - DRIVE ME DRIVE ON

  - TEN-G

  -
  - FREAK OUT\!\!～TRT ME ONE TIME,TRY ME ONE KNIG～

  - Messenger

  - RED SUN

  - Rockin'

## 演出情報

### 綜藝節目

固定班底或主持

<table>
<tbody>
<tr class="odd">
<td><p><br />
（テレビ朝日、1998年4月 - 1999年9月）</p></td>
<td><p><br />
（テレビ朝日、2001年4月19日 - 2002年3月14日）</p></td>
</tr>
<tr class="even">
<td><p><br />
(<a href="../Page/NHK_BS2.md" title="wikilink">NHK BS2</a>（NHK衛星第２テレビジョン）</p></td>
<td><p><br />
（<a href="../Page/日本放送協會.md" title="wikilink">NHK</a>、2001年4月－2002年3月）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/裸之少年.md" title="wikilink">裸の少年</a><br />
（<a href="../Page/朝日電視台.md" title="wikilink">朝日電視台</a>、2001年4月－07年）</p></td>
<td><p><br />
（<a href="../Page/NHK衛星第2台.md" title="wikilink">NHK BS2</a>)</p></td>
</tr>
<tr class="even">
<td><p><br />
（日本電視台、2005年4月－2005年9月）</p></td>
<td><p><br />
（日本電視台、2005年7月30日・8月6日）</p></td>
</tr>
<tr class="odd">
<td><p><br />
(日本電視台、2005年10月－2007年1月）</p></td>
<td><p><a href="../Page/24小時電視.md" title="wikilink">24小時電視 29</a><br />
（日本電視台、2006年8月26-27日）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/You們！.md" title="wikilink">You們！</a><br />
（日本電視台、2006年10月8日－2007年9月30日）田中、中丸</p></td>
<td><p><a href="../Page/Dance甲子園特別節目.md" title="wikilink">Dance甲子園特別節目</a><br />
（日本電視台、2006年12月30日）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/cartoon_KAT-TUN.md" title="wikilink">cartoon KAT-TUN</a>（日本電視台、2007年4月4日－2010年3月24日）</p></td>
<td><p><a href="../Page/KAT-TUN之前輩請教教我!大人的守則100!.md" title="wikilink">KAT-TUN之前輩請教教我!大人的守則100!</a><br />
（日本電視台、2008年1月12日、2009年4月5日、2010年1月6日）</p></td>
</tr>
<tr class="even">
<td><p><br />
（<a href="../Page/朝日電視台.md" title="wikilink">朝日電視台</a>、2010年－）中丸</p></td>
<td><p><a href="../Page/爆笑！大日本アカン警察.md" title="wikilink">爆笑！大日本アカン警察</a><br />
（<a href="../Page/富士電視台.md" title="wikilink">富士電視台</a>、2011年5月－）田中</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/教科書にのせたい!.md" title="wikilink">教科書にのせたい!</a><br />
（<a href="../Page/TBS.md" title="wikilink">TBS</a>、2011年－）上田</p></td>
<td><p><br />
（日本テレビ系、2011年10月18日－12月20日）</p></td>
</tr>
<tr class="even">
<td><p><br />
（TBS、2012年1月1日、2012年4月3日、2012年8月24日－12月18日)</p></td>
<td><p>炎の体育会TV<br />
(TBS、2012年10月－）上田</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/KAT-TUNの世界一タメになる旅!.md" title="wikilink">KAT-TUNの世界一タメになる旅!</a><br />
(TBS、2014年1月10日、2014年4月18日－2016年3月25日)</p></td>
<td><p><a href="../Page/少年俱樂部少premium.md" title="wikilink">少年俱樂部少premium</a>（）<br />
(<a href="../Page/NHKBSプレミアム.md" title="wikilink">NHKBSプレミアム</a>、2014年4月16日－2016年3月16日)</p></td>
</tr>
</tbody>
</table>

### 新聞節目

  - [Going\!
    Sports\&News](../Page/Going!_Sports&News.md "wikilink")（[日本電視台](../Page/日本電視台.md "wikilink")）

<!-- end list -->

  -
    播放時間：每週六～日 23:55～24:55（龜梨於每週日出演、擔任棒球特殊支援採訪員）

<!-- end list -->

  - [SUNDAY COWNTDOWN SHOW
    シューイチ](../Page/SUNDAY_COWNTDOWN_SHOW_シューイチ.md "wikilink")(shu
    1)（[日本電視台](../Page/日本電視台.md "wikilink")）中丸

<!-- end list -->

  -
    播放時段：每週日08:00～09:55（中丸擔任評論員）
    2011年7月10日起，新增以中丸所主持的新單元：「KAT-TUN 中丸雄一のまじっすか\!?」（簡稱「まじっすか(真的嗎)」）

### 戲劇類

#### 連續劇

<table>
<thead>
<tr class="header">
<th><p>放映日</p></th>
<th><p>名稱</p></th>
<th><p>style="background: #FFFAF0" "|頻道</p></th>
<th><p>出演成員</p></th>
<th><p>性質</p></th>
<th><p>style="background: #FFFAF0" width=""|同事務所共演者</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1999年5月9日~1999年6月27日</p></td>
<td><p><a href="../Page/青春男孩.md" title="wikilink">青春男孩</a></p></td>
<td><p>日本電視台</p></td>
<td><p>中丸、赤西</p></td>
<td><p>客串</p></td>
<td><p>山下智久、<a href="../Page/相葉雅紀.md" title="wikilink">相葉雅紀</a></p></td>
</tr>
<tr class="even">
<td><p>1999年10月~2000年3月</p></td>
<td><p><a href="../Page/3年B班金八老師.md" title="wikilink">3年B班金八老師</a> 第5季</p></td>
<td><p>TBS</p></td>
<td><p>龜梨</p></td>
<td><p>配角</p></td>
<td><p><a href="../Page/風間俊介.md" title="wikilink">風間俊介</a></p></td>
</tr>
<tr class="odd">
<td><p>1999年10月~12月</p></td>
<td><p><a href="../Page/真情姊妹花.md" title="wikilink">真情姊妹花</a></p></td>
<td><p>朝日電視台</p></td>
<td><p>赤西</p></td>
<td><p>配角</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2000年3月~6月</p></td>
<td><p><a href="../Page/霓裳夢想家.md" title="wikilink">霓裳夢想家</a></p></td>
<td><p>NHK</p></td>
<td><p>赤西</p></td>
<td><p>配角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000年4月~6月</p></td>
<td><p><a href="../Page/天使絕跡的城市.md" title="wikilink">天使絕跡的城市</a></p></td>
<td><p>日本電視台</p></td>
<td><p>田中</p></td>
<td><p>客串</p></td>
<td><p>堂本光一</p></td>
</tr>
<tr class="even">
<td><p>2001年1月~3月</p></td>
<td><p><a href="../Page/搶錢大作戰.md" title="wikilink">搶錢大作戰</a></p></td>
<td><p>朝日電視台</p></td>
<td><p>田口、赤西</p></td>
<td><p>配角</p></td>
<td><p><a href="../Page/東山紀之.md" title="wikilink">東山紀之</a>、<a href="../Page/國分太一.md" title="wikilink">國分太一</a></p></td>
</tr>
<tr class="odd">
<td><p>2001年7月~9月</p></td>
<td><p><a href="../Page/Never_Land.md" title="wikilink">Never Land</a></p></td>
<td><p>TBS</p></td>
<td><p>田中</p></td>
<td><p>配角</p></td>
<td><p><a href="../Page/今井翼.md" title="wikilink">今井翼</a>、<a href="../Page/三宅健.md" title="wikilink">三宅健</a>、<a href="../Page/村上信五.md" title="wikilink">村上信五</a>、<a href="../Page/生田斗真.md" title="wikilink">生田斗真</a></p></td>
</tr>
<tr class="even">
<td><p>2005年1月~3月</p></td>
<td><p><a href="../Page/極道鮮師.md" title="wikilink">極道鮮師2</a></p></td>
<td><p>日本電視台</p></td>
<td><p>龜梨、赤西</p></td>
<td><p>男配</p></td>
<td><p>|</p></td>
</tr>
<tr class="odd">
<td><p>2005年4月~6月</p></td>
<td><p><a href="../Page/熟女真命苦.md" title="wikilink">熟女真命苦</a>(anego)</p></td>
<td><p>日本電視台</p></td>
<td><p>赤西</p></td>
<td><p>男配</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年7月~9月</p></td>
<td><p><a href="../Page/一起加油吧.md" title="wikilink">一起加油吧</a></p></td>
<td><p>富士電視台</p></td>
<td><p>田口<br />
（第4話起代替<a href="../Page/內博貴.md" title="wikilink">內博貴演出</a>）</p></td>
<td><p>男配</p></td>
<td><p><a href="../Page/錦戶亮.md" title="wikilink">錦戶亮</a></p></td>
</tr>
<tr class="odd">
<td><p>2005年10月~12月</p></td>
<td><p><a href="../Page/改造野豬妹.md" title="wikilink">改造野豬妹</a></p></td>
<td><p>日本電視台</p></td>
<td><p>龜梨</p></td>
<td><p>男主角</p></td>
<td><p>山下智久</p></td>
</tr>
<tr class="even">
<td><p>2006年7月~9月</p></td>
<td><p><a href="../Page/My_Boss_My_Hero.md" title="wikilink">My Boss My Hero</a></p></td>
<td><p>日本電視台</p></td>
<td><p>田中</p></td>
<td><p>配角</p></td>
<td><p><a href="../Page/長瀨智也.md" title="wikilink">長瀨智也</a>、<a href="../Page/手越祐也.md" title="wikilink">手越祐也</a></p></td>
</tr>
<tr class="odd">
<td><p>2006年7月~9月</p></td>
<td><p><a href="../Page/戀愛維他命.md" title="wikilink">戀愛維他命</a></p></td>
<td><p>富士電視台</p></td>
<td><p>龜梨</p></td>
<td><p>男主角</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年10月~12月</p></td>
<td><p><a href="../Page/唯一的愛.md" title="wikilink">唯一的愛</a></p></td>
<td><p>日本電視台</p></td>
<td><p>龜梨、田中</p></td>
<td><p>男主角、男配</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年4月~6月</p></td>
<td><p><a href="../Page/新娘與爸爸.md" title="wikilink">新娘與爸爸</a></p></td>
<td><p>富士電視台</p></td>
<td><p>田口</p></td>
<td><p>男配</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年4月~6月</p></td>
<td><p><a href="../Page/特急田中3號.md" title="wikilink">特急田中3號</a></p></td>
<td><p>TBS</p></td>
<td><p>田中<br />
(龜梨特別客串一集)</p></td>
<td><p>男主角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年7月~9月</p></td>
<td><p><a href="../Page/壽司王子！.md" title="wikilink">壽司王子！</a></p></td>
<td><p>朝日電視台</p></td>
<td><p>中丸</p></td>
<td><p>配角</p></td>
<td><p><a href="../Page/堂本光一.md" title="wikilink">堂本光一</a></p></td>
</tr>
<tr class="even">
<td><p>2007年10月16日~12月18日</p></td>
<td><p><a href="../Page/有閑俱樂部.md" title="wikilink">有閑俱樂部</a></p></td>
<td><p>日本電視台</p></td>
<td><p>赤西、田口</p></td>
<td><p>男主角、男配</p></td>
<td><p><a href="../Page/橫山裕.md" title="wikilink">橫山裕</a></p></td>
</tr>
<tr class="odd">
<td><p>2008年1月12日~3月8日</p></td>
<td><p><a href="../Page/一磅的福音.md" title="wikilink">一磅的福音</a></p></td>
<td><p>日本電視台</p></td>
<td><p>龜梨</p></td>
<td><p>男主角</p></td>
<td><p><a href="../Page/山田涼介.md" title="wikilink">山田涼介</a>、<a href="../Page/知念侑李.md" title="wikilink">知念侑李</a></p></td>
</tr>
<tr class="even">
<td><p>2009年1月9日~6月26日</p></td>
<td><p><a href="../Page/必殺仕事人2009.md" title="wikilink">必殺仕事人2009</a></p></td>
<td><p>朝日</p></td>
<td><p>田中</p></td>
<td><p>男配</p></td>
<td><p><a href="../Page/東山紀之.md" title="wikilink">東山紀之</a>、<a href="../Page/松岡昌宏.md" title="wikilink">松岡昌宏</a></p></td>
</tr>
<tr class="odd">
<td><p>2009年1月13日~3月10日</p></td>
<td><p><a href="../Page/神之雫.md" title="wikilink">神之雫</a></p></td>
<td><p>日本電視台</p></td>
<td><p>龜梨</p></td>
<td><p>男主角</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009年1月24日~3月21日</p></td>
<td><p><a href="../Page/RESCUE〜特別高度救助隊.md" title="wikilink">RESCUE〜特別高度救助隊</a></p></td>
<td><p>TBS</p></td>
<td><p>中丸</p></td>
<td><p>男主角</p></td>
<td><p><a href="../Page/增田貴久.md" title="wikilink">增田貴久</a></p></td>
</tr>
<tr class="odd">
<td><p>2009年4月20日~6月29日</p></td>
<td><p><a href="../Page/結婚萬歲.md" title="wikilink">結婚萬歲</a></p></td>
<td><p>富士</p></td>
<td><p>上田</p></td>
<td><p>配角</p></td>
<td><p><a href="../Page/中居正廣.md" title="wikilink">中居正廣</a></p></td>
</tr>
<tr class="even">
<td><p>2010年1月15日~3月19日</p></td>
<td><p><a href="../Page/完美小姐進化論.md" title="wikilink">完美小姐進化論</a></p></td>
<td><p>TBS</p></td>
<td><p>龜梨</p></td>
<td><p>男主角</p></td>
<td><p><a href="../Page/手越祐也.md" title="wikilink">手越祐也</a>、內博貴</p></td>
</tr>
<tr class="odd">
<td><p>2011年4月15日~2011年6月</p></td>
<td><p><a href="../Page/養狗這回事.md" title="wikilink">養狗這件事~Sky與我家的180天</a></p></td>
<td><p>朝日電視台</p></td>
<td><p>田口</p></td>
<td><p>配角</p></td>
<td><p>錦戶亮</p></td>
</tr>
<tr class="even">
<td><p>2011年10月5日~10月25日</p></td>
<td><p><a href="../Page/愛的售價.md" title="wikilink">愛的售價</a></p></td>
<td><p>NHK</p></td>
<td><p>中丸</p></td>
<td><p>配角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011年10月22日~12月24日</p></td>
<td><p><a href="../Page/妖怪人間貝姆.md" title="wikilink">妖怪人間貝姆</a></p></td>
<td><p>日本電視台</p></td>
<td><p>龜梨</p></td>
<td><p>男主角</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011年10月27日~12月22日</p></td>
<td><p><a href="../Page/RUNAWAY~為了所愛的你.md" title="wikilink">RUNAWAY~為了所愛的你</a></p></td>
<td><p>TBS</p></td>
<td><p>上田</p></td>
<td><p>男配</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012年1月15日~3月18日</p></td>
<td><p><a href="../Page/被稱作早海的日子.md" title="wikilink">被稱作早海的日子</a></p></td>
<td><p>富士電視台</p></td>
<td><p>中丸</p></td>
<td><p>男配</p></td>
<td><p><a href="../Page/井之原快彥.md" title="wikilink">井之原快彥</a></p></td>
</tr>
<tr class="even">
<td><p>2012年4月17日~6月26日</p></td>
<td><p><a href="../Page/Legal_High.md" title="wikilink">Legal High</a>(王牌大律師)</p></td>
<td><p>富士電視台</p></td>
<td><p>田口</p></td>
<td><p>配角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012年7月6日~9月7日</p></td>
<td><p><a href="../Page/敏行快跑.md" title="wikilink">敏行快跑</a></p></td>
<td><p>朝日電視台</p></td>
<td><p>上田</p></td>
<td><p>配角</p></td>
<td><p><a href="../Page/丸山隆平.md" title="wikilink">丸山隆平</a></p></td>
</tr>
<tr class="even">
<td><p>2012年7月7日~9月8日</p></td>
<td><p><a href="../Page/總在哭泣.md" title="wikilink">總在哭泣</a></p></td>
<td><p>富士電視台</p></td>
<td><p>中丸</p></td>
<td><p>男配</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012年10月12日~2012年12月14日</p></td>
<td><p><a href="../Page/大奧_～誕生【有功·家光篇】.md" title="wikilink">大奧～誕生【有功·家光篇】</a></p></td>
<td><p>富士電視台</p></td>
<td><p>田中</p></td>
<td><p>配角</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012年10月23日~2012年12月25日</p></td>
<td><p><a href="../Page/遲開的向日葵.md" title="wikilink">遲開的向日葵</a></p></td>
<td><p>富士電視台</p></td>
<td><p>田口</p></td>
<td><p>配角</p></td>
<td><p><a href="../Page/生田斗真.md" title="wikilink">生田斗真</a></p></td>
</tr>
<tr class="odd">
<td><p>2013su064月13日~6月22日</p></td>
<td><p><a href="../Page/被搞錯的男人.md" title="wikilink">被搞錯的男人</a></p></td>
<td><p>富士電視台</p></td>
<td><p>中丸</p></td>
<td><p>男配</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年7月13~8月3日</p></td>
<td><p><a href="../Page/七場會議.md" title="wikilink">七場會議</a></p></td>
<td><p>NHK</p></td>
<td><p>田口</p></td>
<td><p>配角</p></td>
<td><p>東山紀之</p></td>
</tr>
<tr class="odd">
<td><p>2013年10月9日~2013年12月18</p></td>
<td><p><a href="../Page/Legal_High.md" title="wikilink">Legal High第二季</a></p></td>
<td><p>富士電視台</p></td>
<td><p>田口</p></td>
<td><p>配角</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年10月12日~12月14日</p></td>
<td><p><a href="../Page/東京下町古書店.md" title="wikilink">東京下町古書店</a></p></td>
<td><p>日本電視台</p></td>
<td><p>龜梨</p></td>
<td><p>男主角</p></td>
<td><p><a href="../Page/井之原快彥.md" title="wikilink">井之原快彥</a>、<a href="../Page/中島裕翔.md" title="wikilink">中島裕翔</a></p></td>
</tr>
<tr class="odd">
<td><p>2013年10月21日~12月23日</p></td>
<td><p><a href="../Page/變身訪問者的憂鬱.md" title="wikilink">變身訪問者的憂鬱</a></p></td>
<td><p>TBS</p></td>
<td><p>中丸</p></td>
<td><p>男主角</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014年4月19日~2014年6月21日</p></td>
<td><p><a href="../Page/FIRST_CLASS.md" title="wikilink">FIRST CLASS</a></p></td>
<td><p>富士電視台</p></td>
<td><p>中丸</p></td>
<td><p>男主角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014年10月15日~2014年12月17日</p></td>
<td><p><a href="../Page/今天不上班.md" title="wikilink">今天不上班</a></p></td>
<td><p>日本電視台</p></td>
<td><p>田口</p></td>
<td><p>配角</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015年2月6日~2015年3月20日</p></td>
<td><p><a href="../Page/Second_Love.md" title="wikilink">Second Love</a></p></td>
<td><p>朝日電視台</p></td>
<td><p>龜梨</p></td>
<td><p>男主角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016年1月16日~2016年3月19日</p></td>
<td><p><a href="../Page/怪盜山貓.md" title="wikilink">怪盜山貓</a></p></td>
<td><p>日本電視台</p></td>
<td><p>龜梨</p></td>
<td><p>男主角</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017年1月22日~017年3月19日</p></td>
<td><p><a href="https://ja.wikipedia.org/wiki/%E6%8E%A2%E5%81%B5%E3%83%BB%E6%97%A5%E6%9A%AE%E6%97%85%E4%BA%BA">視覺偵探 日暮旅人</a></p></td>
<td><p>日本電視台</p></td>
<td><p>上田</p></td>
<td><p>配角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017年4月9日~2017年7月2日</p></td>
<td><p><a href="https://ja.wikipedia.org/wiki/%E3%83%9E%E3%83%83%E3%82%B5%E3%83%BC%E3%82%B8%E6%8E%A2%E5%81%B5%E3%82%B8%E3%83%A7%E3%83%BC">按摩偵探 丈</a></p></td>
<td><p>東京電視台</p></td>
<td><p>中丸</p></td>
<td><p>男主角</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017年4月15日~2017年6月17日</p></td>
<td><p><a href="../Page/真命天菜.md" title="wikilink">真命天菜</a></p></td>
<td><p>日本電視台</p></td>
<td><p>龜梨</p></td>
<td><p>男主角</p></td>
<td><p><a href="../Page/山下智久.md" title="wikilink">山下智久</a></p></td>
</tr>
<tr class="odd">
<td><p>2017年10月13日~2017年12月22日</p></td>
<td><p><a href="../Page/新宿セブン.md" title="wikilink">新宿セブン</a></p></td>
<td><p>東京電視台</p></td>
<td><p>上田</p></td>
<td><p>男主角</p></td>
<td></td>
</tr>
</tbody>
</table>

#### 單元劇、客串

| 放映日                   | 名稱                                                                     | 頻道                                       | 出演成員           | 性質        | 同事務所共演者                                 |
| --------------------- | ---------------------------------------------------------------------- | ---------------------------------------- | -------------- | --------- | --------------------------------------- |
| 1999年1月-5月            | [熱血戀愛道](../Page/熱血戀愛道.md "wikilink")                                   | 日本電視台                                    | 赤西、田中、中丸       | 主演        |                                         |
| 1999年7月-9月            | [恐怖星期天](../Page/恐怖星期天.md "wikilink")                                   | 日本電視台                                    | 龜梨、赤西、田中       | 主演        |                                         |
| 1999年10月-12月          | 恐怖星期天\~新章\~                                                            | 日本電視台                                    | 赤西、田中、中丸       | 主演        |                                         |
| 1999年7月-9月            | 恐怖星期天\~2000\~                                                          | 日本電視台                                    | 赤西、田口、田中、上田、中丸 | 主演        |                                         |
| 2000年4月-6月            | [太陽不西沉](../Page/太陽不西沉.md "wikilink")                                   | 富士電視台                                    | 赤西             | 客串第6集     | 瀧澤秀明                                    |
| 2000年7月8日             | 父さん                                                                    | 富士電視台                                    | 中丸             | 客串        | [櫻井翔](../Page/櫻井翔.md "wikilink")        |
| 2000年7月8日             | Age                                                                    | NHK                                      | 田中             |           |                                         |
| 2000年7月26日            | 超速V6 big大作戰                                                            | 富士電視台                                    | 田中             | 客串        | [V6](../Page/V6.md "wikilink")          |
| 2000年12月3日-2001年4月29日 | [史上最惡的約會](../Page/史上最惡的約會.md "wikilink")                               | 日本電視台                                    | 龜梨、赤西、田中、上田    | 主演        |                                         |
| 2001年4月1日             | [變成鳥的少年](../Page/變成鳥的少年.md "wikilink")                                 | [TBS](../Page/TBS.md "wikilink")         | 田口             |           | 山下智久                                    |
| 2001年10月-2002年3月      | [三年B組金八先生第六季](../Page/3年B組金八先生#第6季（2001年）.md "wikilink")               | TBS                                      | 龜梨             | 客串第11、12集 |                                         |
| 2004年12月13日-12月16日    | [Xmas最討厭了！](../Page/Xmas最討厭了！.md "wikilink")                           | 日本電視台                                    | 赤西             |           |                                         |
| 2005年9月24日            | [金田一少年之事件簿](../Page/金田一少年之事件簿_\(電視劇\).md "wikilink")「吸血鬼傳說殺人事件」        | 日本電視台                                    | 龜梨（田中、中丸客串）    | 男主角       |                                         |
| 2005年12月28日           | [熟女真命苦～特別篇～](../Page/熟女真命苦～特別篇～.md "wikilink")                         | 日本電視台                                    | 赤西             | 男主角       |                                         |
| 2006年4月4日             | [很多愛謝謝你](../Page/很多愛謝謝你.md "wikilink")                                 | 日本電視台                                    | 田中             |           |                                         |
| 2006年4月7日             | [Happy\!](../Page/Happy!.md "wikilink")                                | TBS                                      | 田口             | 男主角       |                                         |
| 2006年8月26日            | [勇氣](../Page/勇氣.md "wikilink")                                         | 日本電視台                                    | 龜梨             | 男主角       |                                         |
| 2006年9月30日            | [美食偵探王特別篇](../Page/美食偵探王.md "wikilink") 吃遍香港                           | 日本電視台                                    | 龜梨             |           | 東山紀之、[森田剛](../Page/森田剛.md "wikilink")   |
| 2006年12月26日           | [Happy\!](../Page/Happy!.md "wikilink")2                               | TBS                                      | 田口             | 男主角       |                                         |
| 2007年1月6、7日           | [少年白虎隊](../Page/白虎隊_\(電視劇\).md "wikilink")                             | 朝日電視台                                    | 田中             |           | 山下智久、[藤谷太輔](../Page/藤谷太輔.md "wikilink") |
| 2009年6月6日             | [MR.BRAIN](../Page/MR.BRAIN.md "wikilink")                             | TBS                                      | 龜梨             | 主演第3集     | [木村拓哉](../Page/木村拓哉.md "wikilink")      |
| 2009年8月15日            | [烏龍派出所](../Page/烏龍派出所.md "wikilink")                                   | TBS                                      | 田口             | 主演第3集     | [香取慎吾](../Page/香取慎吾.md "wikilink")      |
| 2009年9月14日            | [向田邦子誕生八十年記念公演特別篇母親的贈禮](../Page/向田邦子誕生八十年記念公演特別篇母親的贈禮.md "wikilink")   | TBS                                      | 中丸             |           |                                         |
| 2010年6月30日            | [離婚症候群](../Page/離婚症候群.md "wikilink")                                   | 日本電視台                                    | 田中             | 主演        |                                         |
| 2010年7月10日            | [必殺仕事人2010](../Page/必殺仕事人2010.md "wikilink")                           | 朝日電視台                                    | 田中             | 主演        | 東山紀之、松岡昌宏                               |
| 2010年9月6日             | [神南署安積班－第三季](../Page/神南署安積班－第三季.md "wikilink")                         | TBS                                      | 中丸             | 客串第10集    |                                         |
| 2011年3月27日            | [三年B組金八先生最終章～「最後的贈言」](../Page/3年B組金八先生#最終特集（2011年）.md "wikilink")      | TBS                                      | 龜梨             | 客串        |                                         |
| 2011年10月5日            | [ダチタビ第一章ダルセーニョなキャンプ](../Page/ダチタビ第一章ダルセーニョなキャンプ.md "wikilink")         | 日本電視台                                    | 中丸             | 主演        | [增田貴久](../Page/增田貴久.md "wikilink")      |
| 2012年2月19日            | [必殺仕事人2012](../Page/必殺仕事人2012.md "wikilink")                           | 朝日電視台                                    | 田中             | 主演        | 東山紀之、松岡昌宏                               |
| 2012年6月10日、17日        | [被稱作早海的日子特別篇前篇](../Page/被稱作早海的日子.md "wikilink")、後篇                     | 富士電視台                                    | 中丸             |           |                                         |
| 2012年9月25日、27日        | dragon青年團                                                              | TBS                                      | 龜梨             | 出演最終回     | [安田章大](../Page/安田章大.md "wikilink")      |
| 2013年1月3日             | [幸運7人組特別篇](../Page/幸運7人組.md "wikilink")                                | 富士電視台                                    | 中丸             | 客串        | 松本潤                                     |
| 2013年4月13日            | [Legal high特別篇](../Page/Legal_high.md "wikilink")                      | 富士電視台                                    | 田口             | 配角        |                                         |
| 2013年9月29日            | [The Partner ～給摯愛的百年朋友～](../Page/The_Partner_～給摯愛的百年朋友～.md "wikilink") | TBS、[越南電視台](../Page/越南電視台.md "wikilink") | 中丸             | 配角        | 東山紀之                                    |
| 2014年12月24日           | [FIRST_CLASS](../Page/FIRST_CLASS.md "wikilink")2                     | 富士電視台                                    | 中丸             | 客串        | 岡本圭人                                    |
| 2015年8月12日            | [刑警七人](../Page/刑警七人.md "wikilink")                                     | 朝日電視台                                    | 中丸             | 客串        | 東山紀之                                    |
| 2016年6月19日            | [99.9_-刑事專門律師-](../Page/99.9_-刑事專門律師-.md "wikilink")                  | TBS                                      | 中丸             | 客串        | 松本潤                                     |
| 2017年8月26日            | 時代を作った男 阿久悠物語                                                          | 日本電視台                                    | 龜梨             | 主演        | 加藤成亮                                    |

#### 舞台劇

  - [SHOCK系列](../Page/SHOCK.md "wikilink")，帝國劇場
      - MILLENNIUM SHOCK（2000年11月2日－11月26日）中丸、赤西
      - SHOW劇・SHOCK(2001年12月1日－2002年1月27日，2002年6月4日－6月28日)KAT-TUN全員
      - SHOCK is Real Shock(2003年1月8日－2月25日)KATT-TUN全員
  - [DREAM BOY](../Page/DREAM_BOY.md "wikilink")(DREAM BOYS)系列
      - DREAM BOY(2004年1月8日－1月31，帝國劇場)KAT-TUN全員
      - 「KAT-TUN&関ジャニ∞編」(2004年4月30日-5月7日，梅田藝術劇場)KAT-TUN出演
      - 「瀧澤秀明編」(2004年5月8日-5月23日，梅田藝術劇場)KAT-TUN除田口(膝傷)以外全員出演
      - [Hey\! Say\! Dream
        Boy](../Page/DREAM_BOY#Hey!_Say!_Dream_Boy.md "wikilink")（2005年4月27日-5月15日，[梅田芸術劇場](../Page/梅田芸術劇場.md "wikilink")）
      - [KAT-TUN vs 関ジャニ∞ Musical Dream
        Boys](../Page/DREAM_BOY#DREAM_BOYS（2006年）.md "wikilink")（2006年1月3日-1月29日，帝国劇場）
      - [DREAM
        BOYS](../Page/DREAM_BOY#DREAM_BOYS（2007年）.md "wikilink")（2007年9月5日-9月30日，帝国劇場、38公演）龜梨、田中
      - [DREAM
        BOYS](../Page/DREAM_BOY#DREAM_BOYS（2008年）.md "wikilink")（2008年3月4日-3月30日，帝国劇場、2008年4月4日-4月16日、梅田芸術劇場）龜梨、田中
      - [DREAM
        BOYS](../Page/DREAM_BOY#DREAM_BOYS（2009年）.md "wikilink")（2009年9月4日-9月29日，帝国劇場、2009年10月13日-10月25，梅田芸術劇場）龜梨和也
      - [DREAM
        BOYS](../Page/DREAM_BOY#DREAM_BOYS（2011年）.md "wikilink")（2011年9月3日-9月25日，帝国劇場、38公演）龜梨、田中、中丸
      - [DREAM
        BOYS](../Page/DREAM_BOY#DREAM_BOYS（2012年）.md "wikilink")（2012年9月3日-9月29日，帝国劇場）龜梨

### 廣播

  - [KAT-TUN
    STYLE](../Page/KAT-TUN_STYLE.md "wikilink")（[ニッポン放送](../Page/ニッポン放送.md "wikilink")，田口・田中・龜梨
    主持）

<!-- end list -->

  -
    播放時間：每週一至週四 23:50～24:00 每週五 23:40～24：00。
    2007年10月1日－2008年3月28日期間，主持人為田口・赤西。
    2008年4月開始，每週一至週四，主持人為田口・田中。
    2011年10月7日改至每週五23:40～24:00。
    2012年3月30日播出最後一集。

<!-- end list -->

  - [龜梨和也的 KS by
    KS](../Page/龜梨和也的_KS_by_KS.md "wikilink")（[ニッポン放送](../Page/ニッポン放送.md "wikilink")，龜梨
    主持）

<!-- end list -->

  -
    2008年4月4日起，每週五 22:40～22:50 23:30～23:40 。
    2009年12月開始，每週五時間更改為23:40～24：00。
    2011年9月30日，播出最後一集。

<!-- end list -->

  - [R-One
    KAT-TUN](../Page/R-One_KAT-TUN.md "wikilink")（[文化放送](../Page/文化放送.md "wikilink")，上田・中丸
    主持）

<!-- end list -->

  -
    播放時間：每週二 24:00～24:30
    註：2006年三月至2007年三月，廣播時間為每週三。
    2007年四月開始，為了不與2007年四月四日（星期三）起播放的綜藝節目[cartoon
    KAT-TUN](../Page/cartoon_KAT-TUN.md "wikilink")（23：55～24：26）衝突，調整廣播時間至週二。
    2011年10月起，撥放時間改至每週一。
    2012年3月26日，播出最後一集。

<!-- end list -->

  - [KAT-TUN 亀梨和也のHANG OUT](../Page/KAT-TUN_亀梨和也のHANG_OUT.md "wikilink")
    （[NACK 5](../Page/NACK_5.md "wikilink")，龜梨 主持）

<!-- end list -->

  -
    播放時間：2011年10月1日起，每週六10:20～10:50

<!-- end list -->

  - [KAT-TUNのがつーん](../Page/KAT-TUNのがつーん.md "wikilink")
    （[文化放送](../Page/文化放送.md "wikilink")，田口・田中・中丸
    主持；田中退團後由田口、中丸繼續主持）

<!-- end list -->

  -
    播放時間：2012年4月2日起，每週一24:00～24:30。

### 演唱會

  - 歌迷至聖 夏 Concert～回應55萬人愛的支持\!\!（2002年8月10日 - 8月28日、2都市26公演）
  - Ko年モ Ah Taiヘン Thank U Natsu（2003年8月8日 - 8月28日、3都市10公演）
  - KAT-TUNの大冒険 de SHOW（2003年8月12日 - 8月20日、ホテル・グランパシフィック・メリディアン、20公演）
  - KAT-TUN Live海賊帆（2004年12月28日 - 2005年1月10日、3都市14公演）
  - Spring 05 Looking KAT-TUN（2005年3月25日 - 3月27日、横濱ARENA、4公演）
  - Looking 05 KAT-TUN（2005年5月29日 - 6月26日、5都市14公演）
  - Looking KAT-TUN 2005ing（2005年8月26日 - 8月28日、横濱ARENA、6公演）
  - KAT-TUN Special TOKYODOME CONCERT Debut "Real
    Face"（2006年3月17日、東京巨蛋、1公演）
  - Spring Tour '06 Live of KAT-TUN "Real Face"（2006年3月28日 -
    5月7日、8都市40公演）
  - Spring Tour '06 in TOKYO DOME Live of KAT-TUN "Real
    Face"（2006年5月13日-14日、東京巨蛋、2公演）
  - TOUR 2007 cartoon KAT-TUN II You（2007年4月3日 - 6月17日、7都市28公演）
  - KAT-TUN LIVE TOUR 2008 QUEEN OF PIRATES（2008年6月21日 - 8月5日、8都市31公演）
  - KAT-TUN Break the Records
    東京巨蛋10days・大阪巨蛋3days（2009年5月15-22日、6月14-15日、東京巨蛋、10公演；5月29-31日、京瓷巨蛋、3公演）
  - KAT-TUN SUMMER '09 Break the Records Tour（2009年7月22日 -
    8月23日、6都市28公演）
  - KAT-TUN LIVE TOUR 2010 PART1:ARENA TOUR（2010年5月2日 - 6月27日、10都市26公演）
  - KAT-TUN LIVE TOUR 2010 PART2:WORLD BIG
    TOUR（2010年7月16-17日及8月24-25日、東京巨蛋；8月6-7日、韓國首爾；8月21-22日、大阪巨蛋；8月27-28日、台灣台北，共計10公演）
  - KAT-TUN LIVE TOUR 2012 CHAIN （2012年2月11日 - 2012年4月28日、12都市23公演）
  - KAT-TUN LIVE 2013 COUNTDOWN (2013年12月30日-2013年12月31日、大阪巨蛋第一次單獨跨年)
  - KAT-TUN FAN MEETING 2014 新春勝詣(2014年1月2日 - 2014年1月4日、橫濱ARENA、5公演)
  - KAT-TUN LIVE TOUR 2014 come Here (2014年7月7日 - 2014年12月31日、10都市21公演)
  - KAT-TUN LIVE 2015 9uarter (2015年5月9日-2015年5月10日、東京巨蛋2公演)
  - KAT-TUN LIVE 2016 10ks
    (2016年4月3日名古屋巨蛋、2016年4月20日大阪京瓷巨蛋、2016年4月29日-2016年5月1日東京巨蛋，共計5公演)
  - KAT-TUN LIVE 2018 UNION (2018年4月20日-2018年4月22日、東京巨蛋3公演)
  - KAT-TUN LIVE TOUR 2018 CAST (2018年8月4日-2018年10月21日、8都市23公演)

### CM 廣告

  - [NTT DOCOMO](../Page/NTT_DOCOMO.md "wikilink")
      - [FOMA
        new9（902iS）](../Page/FOMA#902iSシリーズ.md "wikilink")（2006年6月）
      - NTT DoCoMo 903i（2006年11月26日 - 12月30日、JR西日本・大阪環状線的車身廣告）
      - 企業廣告「我們的自由篇」（2007年8月）
  - [Sky PerfecTV\!](../Page/Sky_PerfecTV!.md "wikilink")
      - 「新魔球篇」（2006年3月）
      - 「Free Kick篇」（2006年5月）
      - 「MUSIC LIFE\! 篇」（2006年8月）
      - 「スカパー\! よくばりパックキャンペーン トナカイチャンネル? 篇」（2006年12月）
  - [樂敦製藥](../Page/樂敦製藥.md "wikilink")
      - もぎたて果実 護脣膏（2005年7月-）
      - Ｃキューブシリーズ」隱形眼鏡藥水（2005年10月）
  - [LOTTE](../Page/LOTTE.md "wikilink")
      - Crunky
          - 「CMが地味? 篇」（2002年9月）
          - 「クセなんだ♪篇」
          - 「クセなんだ♪篇II」
          - 「おいしい食べ方篇」
          - 「ウォーキングバー篇」
      - PLUSX
          - 「グリーンアップル&レッドベリー篇」（2003年11月）
          - 「ブルーシトラス篇」（2004年）
          - 「ピンクベリー篇」（2005年）
          - 「チームプラスエックス篇」（2005年11月22日 - 27日、全5話）
          - 「ゴールドパイン衣装篇」（2006年2月）
          - 「ゴールドパインガム3箱篇」（2006年2月）
          - 「レインボーフルーティア篇」（2006年7月）
          - 「オリジナルボトルキャンペーン篇」（2007年1月）
          - 「マジックキウイ篇」（2007年7月）
          - 「パワーオブフルーツ篇」（2007年11月）
  - [music.jp](../Page/music.jp.md "wikilink")
      - デコともDX（2009年12月25日-）
  - [鈴木株式會社](../Page/鈴木株式會社.md "wikilink")
      - [鈴木Solio](../Page/鈴木Solio.md "wikilink")（2010年12月-）

## 參見

  - [J-FRIENDS](../Page/J-FRIENDS.md "wikilink")
  - [修二與彰](../Page/修二與彰.md "wikilink")

## 参考文献

## 外部連結

  - [Johnny's
    net](http://www.johnnys-net.jp/page?id=profile&artist=14)官方網站

  - [カートゥンKAT-TUN](https://web.archive.org/web/20070623050804/http://www.ntv.co.jp/cartoonkat-tun/)
    - 節目官網

  - [J Storm官方網站](http://www.j-storm.co.jp/) -
    TOKIO、ARASHI、KAT-TUN、Hey\!Say\!JUMP官方網站

  -
[\*](../Category/KAT-TUN.md "wikilink")
[Category:日本男子演唱團體](../Category/日本男子演唱團體.md "wikilink")
[Category:傑尼斯事務所正式團體](../Category/傑尼斯事務所正式團體.md "wikilink")
[Category:日本男子偶像團體](../Category/日本男子偶像團體.md "wikilink")
[Category:Oricon單曲年榜冠軍獲得者](../Category/Oricon單曲年榜冠軍獲得者.md "wikilink")
[Category:Oricon音樂相關影像作品年榜冠軍獲得者](../Category/Oricon音樂相關影像作品年榜冠軍獲得者.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.