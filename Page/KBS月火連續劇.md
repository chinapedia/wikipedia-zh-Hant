**KBS月火連續劇**（，舊稱：）是指[韓國放送公社](../Page/韓國放送公社.md "wikilink")[第2頻道](../Page/KBS第2頻道.md "wikilink")（KBS
2TV）於[星期一及星期二播放的](../Page/星期一.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")，放映時段是22:00至23:10（[KST](../Page/KST.md "wikilink")）。此時段播放的劇集絕大多數是時裝劇，每套劇集大約16集，有時長達20集或短至2集。以下是KBS
2TV歷年來所播放的劇集：

## 劇集列表

### 2000年以前

<table style="width:326%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>导演</p></th>
<th style="text-align: center;"><p>编剧</p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>1994年1月3日－<br />
1994年12月27日</p></td>
<td style="text-align: center;"><p><a href="../Page/韓明澮_(電視劇).md" title="wikilink">韓明澮</a><br />
</p></td>
<td style="text-align: center;"><p>104</p></td>
<td style="text-align: center;"><p><a href="../Page/李德華.md" title="wikilink">李德華</a>、<a href="../Page/李應敬.md" title="wikilink">李應敬</a>、<a href="../Page/徐仁錫.md" title="wikilink">徐仁錫</a>、<a href="../Page/金英蘭_(演員).md" title="wikilink">金英蘭</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金在亨.md" title="wikilink">金在亨</a></p></td>
<td style="text-align: center;"><p><a href="../Page/辛奉承.md" title="wikilink">辛奉承</a></p></td>
<td style="text-align: center;"><p>原作：<a href="../Page/申奉升.md" title="wikilink">申奉升</a>《韓明澮》</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1995年1月2日－<br />
1995年6月27日</p></td>
<td style="text-align: center;"><p><a href="../Page/張綠水_(電視劇).md" title="wikilink">張綠水</a><br />
</p></td>
<td style="text-align: center;"><p>52</p></td>
<td style="text-align: center;"><p><a href="../Page/柳東根.md" title="wikilink">柳東根</a>、<a href="../Page/朴智英.md" title="wikilink">朴智英</a>、<a href="../Page/朴胤先.md" title="wikilink">朴胤先</a>、<a href="../Page/金昭怡.md" title="wikilink">金昭怡</a>、<a href="../Page/任秉基.md" title="wikilink">任秉基</a>、<a href="../Page/申信愛.md" title="wikilink">申信愛</a>、<a href="../Page/金敏貞.md" title="wikilink">金敏貞</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李英國_(導演).md" title="wikilink">李英國</a></p></td>
<td style="text-align: center;"><p><a href="../Page/鄭夏淵.md" title="wikilink">鄭夏淵</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1995年7月3日－<br />
1995年12月26日</p></td>
<td style="text-align: center;"><p><a href="../Page/西宮_(電視劇).md" title="wikilink">西宮</a><br />
</p></td>
<td style="text-align: center;"><p>52</p></td>
<td style="text-align: center;"><p><a href="../Page/金圭哲.md" title="wikilink">金圭哲</a>、<a href="../Page/李甫姬.md" title="wikilink">李甫姬</a>、<a href="../Page/金寶成.md" title="wikilink">金寶成</a>、<a href="../Page/李英愛.md" title="wikilink">李英愛</a>、<a href="../Page/徐仁錫.md" title="wikilink">徐仁錫</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金在亨.md" title="wikilink">金在亨</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴燦聖.md" title="wikilink">朴燦聖</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1996年1月1日－<br />
1996年6月25日</p></td>
<td style="text-align: center;"><p><a href="../Page/趙光祖_(電視劇).md" title="wikilink">趙光祖</a><br />
</p></td>
<td style="text-align: center;"><p>52</p></td>
<td style="text-align: center;"><p><a href="../Page/柳東根.md" title="wikilink">柳東根</a>、<a href="../Page/金慈玉.md" title="wikilink">金慈玉</a>、<a href="../Page/李珍雨.md" title="wikilink">李珍雨</a>、<a href="../Page/金東賢.md" title="wikilink">金東賢</a>、<a href="../Page/金炳基.md" title="wikilink">金炳基</a></p></td>
<td style="text-align: center;"><p><a href="../Page/嚴基白.md" title="wikilink">嚴基白</a></p></td>
<td style="text-align: center;"><p><a href="../Page/鄭夏淵.md" title="wikilink">鄭夏淵</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1998年12月14日－<br />
1999年2月2日</p></td>
<td style="text-align: center;"><p><a href="../Page/天使之吻_(電視劇).md" title="wikilink">天使之吻</a><br />
</p></td>
<td style="text-align: center;"><p>18</p></td>
<td style="text-align: center;"><p><a href="../Page/車勝元.md" title="wikilink">車勝元</a>、<a href="../Page/李在銀.md" title="wikilink">李在銀</a>、<a href="../Page/趙敏基.md" title="wikilink">趙敏基</a>、<a href="../Page/裴斗娜.md" title="wikilink">裴斗娜</a>、<a href="../Page/尹文植.md" title="wikilink">尹文植</a>、<a href="../Page/金勝熙.md" title="wikilink">金勝熙</a>、<a href="../Page/朴相雅.md" title="wikilink">朴相雅</a>、<a href="../Page/林河龍.md" title="wikilink">林河龍</a>、<a href="../Page/柳好貞.md" title="wikilink">柳好貞</a></p></td>
<td style="text-align: center;"><p><a href="../Page/全山.md" title="wikilink">全山</a></p></td>
<td style="text-align: center;"><p><a href="../Page/孫英睦.md" title="wikilink">孫英睦</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1999年9月20日－<br />
1999年11月9日</p></td>
<td style="text-align: center;"><p><a href="../Page/最愛是誰.md" title="wikilink">最愛是誰</a><br />
</p></td>
<td style="text-align: center;"><p>18</p></td>
<td style="text-align: center;"><p><a href="../Page/李珉宇.md" title="wikilink">李珉宇</a>、<a href="../Page/李英愛.md" title="wikilink">李英愛</a>、<a href="../Page/金相慶.md" title="wikilink">金相慶</a>、<a href="../Page/秋相美.md" title="wikilink">秋相美</a>、<a href="../Page/李昌勳.md" title="wikilink">李昌勳</a>、<a href="../Page/金敏貞.md" title="wikilink">金敏貞</a></p></td>
<td style="text-align: center;"><p><a href="../Page/尹錫湖.md" title="wikilink">尹錫湖</a></p></td>
<td style="text-align: center;"><p><a href="../Page/崔允晶.md" title="wikilink">崔允晶</a></p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 2000年

<table style="width:326%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>導演</p></th>
<th style="text-align: center;"><p>編劇</p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2000年1月3日－<br />
2000年2月22日</p></td>
<td style="text-align: center;"><p><a href="../Page/天定情緣.md" title="wikilink">天定情緣</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/安在煥.md" title="wikilink">安在煥</a>、<a href="../Page/明世彬.md" title="wikilink">明世彬</a>、<a href="../Page/李在恩.md" title="wikilink">李在恩</a>、<a href="../Page/金正鉉.md" title="wikilink">金正鉉</a></p></td>
<td style="text-align: center;"><p><a href="../Page/全山.md" title="wikilink">全山</a></p></td>
<td style="text-align: center;"><p><a href="../Page/韓俊英.md" title="wikilink">韓俊英</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2000年2月28日－<br />
2000年4月18日</p></td>
<td style="text-align: center;"><p><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/朴真熙.md" title="wikilink">朴真熙</a>、<a href="../Page/姜錫宇.md" title="wikilink">姜錫宇</a>、<a href="../Page/李恩宙.md" title="wikilink">李恩宙</a>、<a href="../Page/李珉宇.md" title="wikilink">李珉宇</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李民洪.md" title="wikilink">李民洪</a>、<a href="../Page/李元益.md" title="wikilink">李元益</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金英燦.md" title="wikilink">金英燦</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2000年4月24日－<br />
2000年6月27日</p></td>
<td style="text-align: center;"><p><a href="../Page/傻瓜般的愛.md" title="wikilink">傻瓜般的愛</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/裴宗玉.md" title="wikilink">裴宗玉</a>、<a href="../Page/李在龍.md" title="wikilink">李在龍</a>、<a href="../Page/房恩真.md" title="wikilink">房恩真</a>、<a href="../Page/金英浩.md" title="wikilink">金英浩</a>、<a href="../Page/林世美.md" title="wikilink">林世美</a></p></td>
<td style="text-align: center;"><p><a href="../Page/表民秀.md" title="wikilink">表民秀</a></p></td>
<td style="text-align: center;"><p><a href="../Page/盧熙京.md" title="wikilink">盧熙京</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2000年7月10日－<br />
2000年9月5日</p></td>
<td style="text-align: center;"><p><a href="../Page/RNA_(電視劇).md" title="wikilink">RNA</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/裴斗娜.md" title="wikilink">裴斗娜</a>、<a href="../Page/金元俊.md" title="wikilink">金元俊</a>、<a href="../Page/金孝珍.md" title="wikilink">金孝珍</a>、<a href="../Page/金彩燕.md" title="wikilink">金彩燕</a>、<a href="../Page/朴廣賢.md" title="wikilink">朴廣賢</a></p></td>
<td style="text-align: center;"><p><a href="../Page/全啟相.md" title="wikilink">全啟相</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李弘求.md" title="wikilink">李弘求</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2000年9月18日－<br />
2000年11月7日</p></td>
<td style="text-align: center;"><p><a href="../Page/秋日童話.md" title="wikilink">秋日童話</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/宋承憲.md" title="wikilink">宋承憲</a>、<a href="../Page/宋慧喬.md" title="wikilink">宋慧喬</a>、<a href="../Page/元斌.md" title="wikilink">元斌</a>、<a href="../Page/韓彩英.md" title="wikilink">韓彩英</a>、<a href="../Page/韓娜娜.md" title="wikilink">韓娜娜</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/尹錫湖.md" title="wikilink">尹錫湖</a></p></td>
<td style="text-align: center;"><p><a href="../Page/吳秀妍.md" title="wikilink">吳秀妍</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2000年11月13日－<br />
2001年1月2日</p></td>
<td style="text-align: center;"><p><a href="../Page/雪花_(2000年電視劇).md" title="wikilink">雪花</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/尹孫河.md" title="wikilink">尹孫河</a>、<a href="../Page/金相慶.md" title="wikilink">金相慶</a>、<a href="../Page/朴容夏.md" title="wikilink">朴容夏</a>、<a href="../Page/蔡貞安.md" title="wikilink">蔡貞安</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/金平中.md" title="wikilink">金平中</a></p></td>
<td style="text-align: center;"><p><a href="../Page/陳秀完.md" title="wikilink">陳秀完</a></p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 2001年

<table style="width:326%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>導演</p></th>
<th style="text-align: center;"><p>編劇</p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2001年1月8日－<br />
2001年2月27日</p></td>
<td style="text-align: center;"><p><a href="../Page/漂亮淑女.md" title="wikilink">漂亮淑女</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/李昌勳.md" title="wikilink">李昌勳</a>、<a href="../Page/朴宣暎.md" title="wikilink">朴宣暎</a>、<a href="../Page/金彩燕.md" title="wikilink">金彩燕</a>、<a href="../Page/安在模.md" title="wikilink">安在模</a></p></td>
<td style="text-align: center;"><p><a href="../Page/尹昌范.md" title="wikilink">尹昌范</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金善英.md" title="wikilink">金善英</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2001年3月5日－<br />
2001年5月8日</p></td>
<td style="text-align: center;"><p><a href="../Page/綢緞花香木.md" title="wikilink">綢緞花香木</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/朴真熙.md" title="wikilink">朴真熙</a>、<a href="../Page/柳鎮.md" title="wikilink">柳鎮</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴燦鍾.md" title="wikilink">朴燦鍾</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金智友.md" title="wikilink">金智友</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2001年5月14日－<br />
2001年7月3日</p></td>
<td style="text-align: center;"><p><a href="../Page/人生多美好.md" title="wikilink">人生多美好</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/河智苑.md" title="wikilink">河智苑</a>、<a href="../Page/金來沅.md" title="wikilink">金來沅</a>、<a href="../Page/尹海英.md" title="wikilink">尹海英</a>、<a href="../Page/柳俊相.md" title="wikilink">柳俊相</a></p></td>
<td style="text-align: center;"><p><a href="../Page/閔寶賢.md" title="wikilink">閔寶賢</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李弘求.md" title="wikilink">李弘求</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2001年7月9日－<br />
2001年8月28日</p></td>
<td style="text-align: center;"><p><a href="../Page/COOL.md" title="wikilink">COOL</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/九玻承.md" title="wikilink">九玻承</a>、<a href="../Page/蘇有珍.md" title="wikilink">蘇有珍</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李民洪.md" title="wikilink">李民洪</a>、<a href="../Page/李政燮.md" title="wikilink">李政燮</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴彥熙.md" title="wikilink">朴彥熙</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2001年9月3日－<br />
2001年10月30日</p></td>
<td style="text-align: center;"><p><a href="../Page/純情.md" title="wikilink">純情</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/柳鎮.md" title="wikilink">柳鎮</a>、<a href="../Page/李枖原.md" title="wikilink">李枖原</a>、<a href="../Page/李鐘原.md" title="wikilink">李鐘原</a>、<a href="../Page/廉晶雅.md" title="wikilink">廉晶雅</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/鄭成孝.md" title="wikilink">鄭成孝</a>、<a href="../Page/金政珉.md" title="wikilink">金政珉</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李慶熙.md" title="wikilink">李慶熙</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2001年11月5日－<br />
2001年12月25日</p></td>
<td style="text-align: center;"><p><a href="../Page/美娜_(韓國電視劇).md" title="wikilink">美娜</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/蔡貞安.md" title="wikilink">蔡貞安</a>、<a href="../Page/金思朗.md" title="wikilink">金思朗</a>、<a href="../Page/金承洙.md" title="wikilink">金承洙</a>、<a href="../Page/安在模.md" title="wikilink">安在模</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/金載順.md" title="wikilink">金載順</a></p></td>
<td style="text-align: center;"><p><a href="../Page/徐賢珠.md" title="wikilink">徐賢珠</a></p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 2002年

<table style="width:326%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>導演</p></th>
<th style="text-align: center;"><p>編劇</p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2002年1月14日－<br />
2002年3月19日</p></td>
<td style="text-align: center;"><p><a href="../Page/冬日戀歌.md" title="wikilink">冬日戀歌</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/裴勇浚.md" title="wikilink">裴勇浚</a>、<a href="../Page/崔智友.md" title="wikilink">崔智友</a>、<a href="../Page/朴容夏.md" title="wikilink">朴容夏</a>、<a href="../Page/朴帥眉.md" title="wikilink">朴帥眉</a></p></td>
<td style="text-align: center;"><p><a href="../Page/尹錫湖.md" title="wikilink">尹錫湖</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金銀姬.md" title="wikilink">金銀姬</a>、<a href="../Page/尹恩景.md" title="wikilink">尹恩景</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2002年3月25日－<br />
2002年5月14日</p></td>
<td style="text-align: center;"><p><a href="../Page/陽光狩獵.md" title="wikilink">陽光狩獵</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/金浩鎮.md" title="wikilink">金浩鎮</a>、<a href="../Page/金智秀.md" title="wikilink">金智秀</a>、<a href="../Page/河智苑.md" title="wikilink">河智苑</a>、<a href="../Page/地成.md" title="wikilink">地成</a>、<a href="../Page/崔哲浩.md" title="wikilink">崔哲浩</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金宗昌.md" title="wikilink">金宗昌</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金慧貞.md" title="wikilink">金慧貞</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2002年5月20日－<br />
2002年7月14日</p></td>
<td style="text-align: center;"><p><a href="../Page/無法阻擋的愛.md" title="wikilink">無法阻擋的愛</a><br />
</p></td>
<td style="text-align: center;"><p>19</p></td>
<td style="text-align: center;"><p><a href="../Page/吳娟受.md" title="wikilink">吳娟受</a>、<a href="../Page/趙敏基.md" title="wikilink">趙敏基</a>、<a href="../Page/宋善美.md" title="wikilink">宋善美</a>、<a href="../Page/徐泰華.md" title="wikilink">徐泰華</a>、<a href="../Page/劉惠晶.md" title="wikilink">劉惠晶</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李康賢.md" title="wikilink">李康賢</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李善熙.md" title="wikilink">李善熙</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2002年7月29日－<br />
2002年9月3日</p></td>
<td style="text-align: center;"><p><a href="../Page/人魚公主_(電視劇).md" title="wikilink">人魚公主</a><br />
</p></td>
<td style="text-align: center;"><p>12</p></td>
<td style="text-align: center;"><p><a href="../Page/柳真.md" title="wikilink">柳真</a>、<a href="../Page/朴容夏.md" title="wikilink">朴容夏</a>、<a href="../Page/李棟旭.md" title="wikilink">李棟旭</a>、<a href="../Page/李幼梨.md" title="wikilink">李幼梨</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李健俊.md" title="wikilink">李健俊</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金宗賢.md" title="wikilink">金宗賢</a>、<a href="../Page/申慧珍.md" title="wikilink">申慧珍</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2002年9月9日－<br />
2002年10月15日</p></td>
<td style="text-align: center;"><p><a href="../Page/天國之子.md" title="wikilink">天國之子</a><br />
</p></td>
<td style="text-align: center;"><p>12</p></td>
<td style="text-align: center;"><p><a href="../Page/金烔完.md" title="wikilink">金烔完</a>、<a href="../Page/楊美蘿.md" title="wikilink">楊美蘿</a>、<a href="../Page/李珉宇.md" title="wikilink">李珉宇</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/金勇奎.md" title="wikilink">金勇奎</a></p></td>
<td style="text-align: center;"><p><a href="../Page/尹正健.md" title="wikilink">尹正健</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2002年10月21日－<br />
2002年12月24日</p></td>
<td style="text-align: center;"><p><a href="../Page/孤獨.md" title="wikilink">孤獨</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/李美淑.md" title="wikilink">李美淑</a>、<a href="../Page/柳承範.md" title="wikilink">柳承範</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/表民秀.md" title="wikilink">表民秀</a></p></td>
<td style="text-align: center;"><p><a href="../Page/盧熙京.md" title="wikilink">盧熙京</a></p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 2003年

<table style="width:326%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>導演</p></th>
<th style="text-align: center;"><p>編劇</p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2003年1月6日－<br />
2003年7月1日</p></td>
<td style="text-align: center;"><p><a href="../Page/妻子_(2003年電視劇).md" title="wikilink">妻子</a><br />
</p></td>
<td style="text-align: center;"><p>52</p></td>
<td style="text-align: center;"><p><a href="../Page/柳東根.md" title="wikilink">柳東根</a>、<a href="../Page/金喜愛.md" title="wikilink">金喜愛</a>、<a href="../Page/嚴正化.md" title="wikilink">嚴正化</a>、<a href="../Page/鄭普碩.md" title="wikilink">鄭普碩</a>、<a href="../Page/文瑾瑩.md" title="wikilink">文瑾瑩</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金賢俊.md" title="wikilink">金賢俊</a>、<a href="../Page/金元勇.md" title="wikilink">金元勇</a></p></td>
<td style="text-align: center;"><p><a href="../Page/鄭夏淵.md" title="wikilink">鄭夏淵</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2003年7月7日－<br />
2003年9月8日</p></td>
<td style="text-align: center;"><p><a href="../Page/夏日香氣.md" title="wikilink">夏日香氣</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/宋承憲.md" title="wikilink">宋承憲</a>、<a href="../Page/孫藝真.md" title="wikilink">孫藝真</a>、<a href="../Page/柳鎮_(韓國).md" title="wikilink">柳鎮</a>、<a href="../Page/韓智慧.md" title="wikilink">韓智慧</a></p></td>
<td style="text-align: center;"><p><a href="../Page/尹錫湖.md" title="wikilink">尹錫湖</a></p></td>
<td style="text-align: center;"><p><a href="../Page/崔昊燕.md" title="wikilink">崔昊燕</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2003年9月15日－<br />
2003年11月4日</p></td>
<td style="text-align: center;"><p><a href="../Page/尚道呀，上學去.md" title="wikilink">小爸爸上學去</a><br />
</p></td>
<td style="text-align: center;"><p>14</p></td>
<td style="text-align: center;"><p><a href="../Page/鄭智薰.md" title="wikilink">鄭智薰</a>、<a href="../Page/孔曉振.md" title="wikilink">孔曉振</a>、<a href="../Page/李東健.md" title="wikilink">李東健</a>、<a href="../Page/洪洙賢.md" title="wikilink">洪洙賢</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李亨民.md" title="wikilink">李亨民</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李慶熙.md" title="wikilink">李慶熙</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2003年11月10日－<br />
2004年1月13日</p></td>
<td style="text-align: center;"><p><a href="../Page/她是最棒的.md" title="wikilink">她是最棒的</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/姜成妍.md" title="wikilink">姜成妍</a>、<a href="../Page/柳時元.md" title="wikilink">柳時元</a>、<a href="../Page/安在模.md" title="wikilink">安在模</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金勇奎.md" title="wikilink">金勇奎</a></p></td>
<td style="text-align: center;"><p><a href="../Page/趙希.md" title="wikilink">趙希</a></p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 2004年

<table style="width:326%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>導演</p></th>
<th style="text-align: center;"><p>編劇</p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2004年1月19日－<br />
2004年3月9日</p></td>
<td style="text-align: center;"><p><a href="../Page/新娘18歲.md" title="wikilink">新娘18歲</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/韓智慧.md" title="wikilink">韓智慧</a>、<a href="../Page/李東健.md" title="wikilink">李東健</a>、<a href="../Page/李多海.md" title="wikilink">李多海</a>、<a href="../Page/朴亨才.md" title="wikilink">朴亨才</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金正奎.md" title="wikilink">金正奎</a>、<a href="../Page/金明旭.md" title="wikilink">金明旭</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金銀姬.md" title="wikilink">金銀姬</a>、<a href="../Page/尹恩景.md" title="wikilink">尹恩景</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2004年3月15日－<br />
2004年5月4日</p></td>
<td style="text-align: center;"><p><a href="../Page/白雪公主_(電視劇).md" title="wikilink">白雪公主</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/金晶和.md" title="wikilink">金晶和</a>、<a href="../Page/李莞.md" title="wikilink">李莞</a>、<a href="../Page/延政勳.md" title="wikilink">延政勳</a>、<a href="../Page/吳承賢.md" title="wikilink">吳承賢</a>、<a href="../Page/趙胤熙.md" title="wikilink">趙胤熙</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李在祥.md" title="wikilink">李在祥</a></p></td>
<td style="text-align: center;"><p><a href="../Page/具善慶.md" title="wikilink">具善慶</a>、<a href="../Page/李善英.md" title="wikilink">李善英</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2004年5月10日－<br />
2004年7月13日</p></td>
<td style="text-align: center;"><p><a href="../Page/北京，我的愛.md" title="wikilink">北京，我的愛</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/金載沅.md" title="wikilink">金載沅</a>、<a href="../Page/韓彩英.md" title="wikilink">韓彩英</a>、<a href="../Page/孫菲菲.md" title="wikilink">孫菲菲</a>、<a href="../Page/郭小冬.md" title="wikilink">郭小冬</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李教旭.md" title="wikilink">李教旭</a>、<a href="../Page/衛列.md" title="wikilink">衛列</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金均泰.md" title="wikilink">金均泰</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2004年7月19日－<br />
2004年9月7日</p></td>
<td style="text-align: center;"><p><a href="../Page/九尾狐外傳.md" title="wikilink">九尾狐外傳</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/金泰希.md" title="wikilink">金泰希</a>、<a href="../Page/趙顯宰.md" title="wikilink">趙顯宰</a>、<a href="../Page/韓藝瑟.md" title="wikilink">韓藝瑟</a>、<a href="../Page/前進.md" title="wikilink">前進</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金亨一.md" title="wikilink">金亨一</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李慶美.md" title="wikilink">李慶美</a>、<a href="../Page/黃成燕.md" title="wikilink">黃成燕</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2004年9月13日－<br />
2004年11月2日</p></td>
<td style="text-align: center;"><p><a href="../Page/OH!必勝奉順英.md" title="wikilink">OH!必勝奉順英</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/安在旭.md" title="wikilink">安在旭</a>、<a href="../Page/蔡琳.md" title="wikilink">蔡琳</a>、<a href="../Page/柳鎮_(韓國).md" title="wikilink">柳鎮</a>、<a href="../Page/朴宣暎.md" title="wikilink">朴宣暎</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/池英洙.md" title="wikilink">池英洙</a></p></td>
<td style="text-align: center;"><p><a href="../Page/姜銀慶.md" title="wikilink">姜銀慶</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2004年11月8日－<br />
2004年12月28日</p></td>
<td style="text-align: center;"><p><a href="../Page/對不起，我愛你_(電視劇).md" title="wikilink">對不起，我愛你</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/蘇志燮.md" title="wikilink">蘇志燮</a>、<a href="../Page/林秀晶.md" title="wikilink">林秀晶</a>、<a href="../Page/鄭敬淏.md" title="wikilink">鄭敬淏</a>、<a href="../Page/徐智英.md" title="wikilink">徐智英</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/李亨民.md" title="wikilink">李亨民</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李慶熙.md" title="wikilink">李慶熙</a></p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 2005年

<table style="width:326%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>導演</p></th>
<th style="text-align: center;"><p>編劇</p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2005年1月3日－<br />
2005年3月1日</p></td>
<td style="text-align: center;"><p><a href="../Page/豪傑春香.md" title="wikilink">豪傑春香</a><br />
</p></td>
<td style="text-align: center;"><p>17</p></td>
<td style="text-align: center;"><p><a href="../Page/韓彩英.md" title="wikilink">韓彩英</a>、<a href="../Page/在喜.md" title="wikilink">在喜</a>、<a href="../Page/嚴泰雄.md" title="wikilink">嚴泰雄</a>、<a href="../Page/朴詩恩.md" title="wikilink">朴詩恩</a></p></td>
<td style="text-align: center;"><p><a href="../Page/全啟相.md" title="wikilink">全啟相</a>、<a href="../Page/池炳賢.md" title="wikilink">池炳賢</a></p></td>
<td style="text-align: center;"><p><a href="../Page/洪貞恩.md" title="wikilink">洪貞恩</a>、<a href="../Page/洪美蘭.md" title="wikilink">洪美蘭</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2005年3月7日－<br />
2005年4月28日</p></td>
<td style="text-align: center;"><p><a href="../Page/18．29.md" title="wikilink">18．29</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/柳秀榮.md" title="wikilink">柳秀榮</a>、<a href="../Page/朴宣暎.md" title="wikilink">朴宣暎</a>、<a href="../Page/朴恩惠.md" title="wikilink">朴恩惠</a>、<a href="../Page/李重文.md" title="wikilink">李重文</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金元勇.md" title="wikilink">金元勇</a>、<a href="../Page/韓永勳.md" title="wikilink">韓永勳</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金京熙.md" title="wikilink">金京熙</a>、<a href="../Page/高博皇.md" title="wikilink">高博皇</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2005年5月2日－<br />
2005年6月21日</p></td>
<td style="text-align: center;"><p><a href="../Page/愛情中毒.md" title="wikilink">愛情中毒</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/安七炫.md" title="wikilink">安七炫</a>、<a href="../Page/金玟善.md" title="wikilink">金玟善</a>、<a href="../Page/李善均.md" title="wikilink">李善均</a>、<a href="../Page/劉仁英.md" title="wikilink">劉仁英</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李健俊.md" title="wikilink">李健俊</a>、<a href="../Page/閔俊河.md" title="wikilink">閔俊河</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李香熙.md" title="wikilink">李香熙</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2005年6月27日－<br />
2005年8月16日</p></td>
<td style="text-align: center;"><p><a href="../Page/她回來了.md" title="wikilink">她回來了</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/金洙承.md" title="wikilink">金洙承</a>、<a href="../Page/金孝珍.md" title="wikilink">金孝珍</a>、<a href="../Page/金男珍.md" title="wikilink">金男珍</a>、<a href="../Page/徐智慧.md" title="wikilink">徐智慧</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金明旭.md" title="wikilink">金明旭</a>、<a href="../Page/李振書.md" title="wikilink">李振書</a></p></td>
<td style="text-align: center;"><p><a href="../Page/文銀雅.md" title="wikilink">文銀雅</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2005年8月23日－<br />
2005年10月25日</p></td>
<td style="text-align: center;"><p><a href="../Page/結婚_(電視劇).md" title="wikilink">結婚</a><br />
</p></td>
<td style="text-align: center;"><p>18</p></td>
<td style="text-align: center;"><p><a href="../Page/張娜拉.md" title="wikilink">張娜拉</a>、<a href="../Page/柳時元.md" title="wikilink">柳時元</a>、<a href="../Page/明世彬.md" title="wikilink">明世彬</a>、<a href="../Page/李賢宇.md" title="wikilink">李賢宇</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/鄭海龍.md" title="wikilink">鄭海龍</a>、<a href="../Page/黃義慶.md" title="wikilink">黃義慶</a></p></td>
<td style="text-align: center;"><p><a href="../Page/吳秀妍.md" title="wikilink">吳秀妍</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2005年10月31日－<br />
2005年12月20日</p></td>
<td style="text-align: center;"><p><a href="../Page/這該死的愛情.md" title="wikilink">這該死的愛情</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/鄭智薰.md" title="wikilink">鄭智薰</a>、<a href="../Page/新慜娥.md" title="wikilink">新慜娥</a>、<a href="../Page/金思朗.md" title="wikilink">金思朗</a>、<a href="../Page/李己雨.md" title="wikilink">李己雨</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/金圭泰.md" title="wikilink">金圭泰</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李慶熙.md" title="wikilink">李慶熙</a></p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 2006年

<table style="width:326%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>導演</p></th>
<th style="text-align: center;"><p>編劇</p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2006年1月9日－<br />
2006年2月28日</p></td>
<td style="text-align: center;"><p><a href="../Page/你好，上帝.md" title="wikilink">你好，上帝</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/劉健.md" title="wikilink">劉健</a>、<a href="../Page/金玉彬.md" title="wikilink">金玉彬</a>、<a href="../Page/李鐘赫.md" title="wikilink">李鐘赫</a></p></td>
<td style="text-align: center;"><p><a href="../Page/池英洙.md" title="wikilink">池英洙</a></p></td>
<td style="text-align: center;"><p><a href="../Page/姜銀慶.md" title="wikilink">姜銀慶</a></p></td>
<td style="text-align: center;"><p>原作：<a href="../Page/丹尼爾·凱斯.md" title="wikilink">丹尼爾·凱斯</a>《<a href="../Page/獻給阿爾吉儂的花束.md" title="wikilink">獻給阿爾吉儂的花束</a>》</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2006年3月6日－<br />
2006年5月16日</p></td>
<td style="text-align: center;"><p><a href="../Page/春日華爾滋.md" title="wikilink">春日華爾滋</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/徐道永.md" title="wikilink">徐道永</a>、<a href="../Page/韓孝周.md" title="wikilink">韓孝周</a>、<a href="../Page/丹尼爾·海尼.md" title="wikilink">丹尼爾·海尼</a>、<a href="../Page/李昭娟.md" title="wikilink">李昭娟</a></p></td>
<td style="text-align: center;"><p><a href="../Page/尹錫湖.md" title="wikilink">尹錫湖</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金智妍.md" title="wikilink">金智妍</a>、<a href="../Page/黃多恩.md" title="wikilink">黃多恩</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2006年5月22日－<br />
2006年7月18日</p></td>
<td style="text-align: center;"><p><a href="../Page/再見先生.md" title="wikilink">再見先生</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/安在旭.md" title="wikilink">安在旭</a>、<a href="../Page/李寶英.md" title="wikilink">李寶英</a>、<a href="../Page/吳允兒.md" title="wikilink">吳允兒</a>、<a href="../Page/趙東赫.md" title="wikilink">趙東赫</a></p></td>
<td style="text-align: center;"><p><a href="../Page/黃義慶.md" title="wikilink">黃義慶</a></p></td>
<td style="text-align: center;"><p><a href="../Page/徐淑香.md" title="wikilink">徐淑香</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2006年7月24日－<br />
2006年9月12日</p></td>
<td style="text-align: center;"><p><a href="../Page/葡萄園裏的那男子.md" title="wikilink">葡萄園裏的那男子</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/尹恩惠.md" title="wikilink">尹恩惠</a>、<a href="../Page/吳萬石.md" title="wikilink">吳萬石</a>、<a href="../Page/鄭素英.md" title="wikilink">鄭素英</a>、<a href="../Page/金智錫.md" title="wikilink">金智錫</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴萬英.md" title="wikilink">朴萬英</a>、<a href="../Page/金英兆.md" title="wikilink">金英兆</a></p></td>
<td style="text-align: center;"><p><a href="../Page/趙明珠.md" title="wikilink">趙明珠</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2006年9月18日－<br />
2006年11月7日</p></td>
<td style="text-align: center;"><p><a href="../Page/白雲階梯.md" title="wikilink">白雲階梯</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/申東旭.md" title="wikilink">申東旭</a>、<a href="../Page/韓智慧.md" title="wikilink">韓智慧</a>、<a href="../Page/林晶恩.md" title="wikilink">林晶恩</a>、<a href="../Page/金正鉉.md" title="wikilink">金正鉉</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/金勇奎.md" title="wikilink">金勇奎</a>、<a href="../Page/柳賢基.md" title="wikilink">柳賢基</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李善美.md" title="wikilink">李善美</a>、<a href="../Page/金基浩.md" title="wikilink">金基浩</a>、<a href="../Page/朴相喜.md" title="wikilink">朴相喜</a></p></td>
<td style="text-align: center;"><p>原作：<a href="../Page/渡邊淳一.md" title="wikilink">渡邊淳一</a>《白雲階梯》</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2006年11月13日－<br />
2007年1月8日</p></td>
<td style="text-align: center;"><p><a href="../Page/雪之女王_(電視劇).md" title="wikilink">雪之女王</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/玄彬.md" title="wikilink">玄彬</a>、<a href="../Page/成宥利.md" title="wikilink">成宥利</a>、<a href="../Page/林周煥.md" title="wikilink">林周煥</a>、<a href="../Page/劉仁英.md" title="wikilink">劉仁英</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/李亨民.md" title="wikilink">李亨民</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金銀姬.md" title="wikilink">金銀姬</a>、<a href="../Page/尹恩景.md" title="wikilink">尹恩景</a></p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 2007年

<table style="width:326%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>導演</p></th>
<th style="text-align: center;"><p>編劇</p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2007年1月15日－<br />
2007年3月13日</p></td>
<td style="text-align: center;"><p><a href="../Page/春暖花開的時候.md" title="wikilink">春暖花開的時候</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/朴健衡.md" title="wikilink">朴健衡</a>、<a href="../Page/李荷娜.md" title="wikilink">李荷娜</a>、<a href="../Page/朴詩妍.md" title="wikilink">朴詩妍</a>、<a href="../Page/金南佶.md" title="wikilink">金南佶</a></p></td>
<td style="text-align: center;"><p><a href="../Page/陳亨旭.md" title="wikilink">陳亨旭</a></p></td>
<td style="text-align: center;"><p><a href="../Page/權民洙.md" title="wikilink">權民洙</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2007年3月19日－<br />
2007年5月8日</p></td>
<td style="text-align: center;"><p><a href="../Page/你好！小姐.md" title="wikilink">你好！小姐</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/李多海.md" title="wikilink">李多海</a>、<a href="../Page/李志勳.md" title="wikilink">李志勳</a>、<a href="../Page/河錫辰.md" title="wikilink">河錫辰</a>、<a href="../Page/延美珠.md" title="wikilink">延美珠</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李民弘.md" title="wikilink">李民弘</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴英淑.md" title="wikilink">朴英淑</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2007年5月14日－<br />
2007年7月3日</p></td>
<td style="text-align: center;"><p><a href="../Page/為尋花而來.md" title="wikilink">為尋花而來</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/車太鉉.md" title="wikilink">車太鉉</a>、<a href="../Page/姜惠貞.md" title="wikilink">姜惠貞</a>、<a href="../Page/孔賢珠.md" title="wikilink">孔賢珠</a>、<a href="../Page/金智勳.md" title="wikilink">金智勳</a></p></td>
<td style="text-align: center;"><p><a href="../Page/池英洙.md" title="wikilink">池英洙</a></p></td>
<td style="text-align: center;"><p><a href="../Page/尹成熙.md" title="wikilink">尹成熙</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2007年7月9日－<br />
2007年7月31日</p></td>
<td style="text-align: center;"><p><a href="../Page/漢城別曲-正.md" title="wikilink">漢城別曲-正</a><br />
</p></td>
<td style="text-align: center;"><p>8</p></td>
<td style="text-align: center;"><p><a href="../Page/真理翰.md" title="wikilink">真理翰</a>、<a href="../Page/金賀恩.md" title="wikilink">金賀恩</a>、<a href="../Page/李天熙.md" title="wikilink">李天熙</a></p></td>
<td style="text-align: center;"><p><a href="../Page/郭正煥.md" title="wikilink">郭正煥</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴進宇.md" title="wikilink">朴進宇</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2007年8月6日－<br />
2007年10月2日</p></td>
<td style="text-align: center;"><p><a href="../Page/我是老師.md" title="wikilink">我是老師</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/梁東根.md" title="wikilink">梁東根</a>、<a href="../Page/朴敏英.md" title="wikilink">朴敏英</a>、<a href="../Page/孫泰英.md" title="wikilink">孫泰英</a>、<a href="../Page/朴俊奎.md" title="wikilink">朴俊奎</a>、<a href="../Page/T.O.P..md" title="wikilink">T.O.P.</a>、<a href="../Page/朴彩京.md" title="wikilink">朴彩京</a>、<a href="../Page/朴載正.md" title="wikilink">朴載正</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/金正奎.md" title="wikilink">金正奎</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李鎮梅.md" title="wikilink">李鎮梅</a></p></td>
<td style="text-align: center;"><p>原作：<a href="../Page/岡田和人.md" title="wikilink">岡田和人</a>《<a href="../Page/教科書沒教的事.md" title="wikilink">教科書沒教的事</a>》</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2007年10月8日－<br />
2007年11月27日</p></td>
<td style="text-align: center;"><p><a href="../Page/詐騙信用調查所.md" title="wikilink">詐騙信用調查所</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/李民基.md" title="wikilink">李民基</a>、<a href="../Page/芮智媛.md" title="wikilink">芮智媛</a>、<a href="../Page/柳承秀.md" title="wikilink">柳承秀</a>、<a href="../Page/李恩誠.md" title="wikilink">李恩誠</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/車榮勳.md" title="wikilink">車榮勳</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴妍善.md" title="wikilink">朴妍善</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2007年12月3日－<br />
2008年2月12日</p></td>
<td style="text-align: center;"><p><a href="../Page/壞愛情.md" title="wikilink">壞愛情</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/權相佑.md" title="wikilink">權相佑</a>、<a href="../Page/李枖原.md" title="wikilink">李枖原</a>、<a href="../Page/金聖洙.md" title="wikilink">金聖洙</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/權啟洪.md" title="wikilink">權啟洪</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李宇珍.md" title="wikilink">李宇珍</a></p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 2008年

<table style="width:326%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>導演</p></th>
<th style="text-align: center;"><p>編劇</p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2008年2月18日－<br />
2008年4月8日</p></td>
<td style="text-align: center;"><p><a href="../Page/單身爸爸戀愛中.md" title="wikilink">單身爸爸戀愛中</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/吳智昊.md" title="wikilink">吳智昊</a>、<a href="../Page/姜成妍.md" title="wikilink">姜成妍</a>、<a href="../Page/許怡才.md" title="wikilink">許怡才</a></p></td>
<td style="text-align: center;"><p><a href="../Page/文寶賢.md" title="wikilink">文寶賢</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金相熙.md" title="wikilink">金相熙</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2008年4月14日－<br />
2008年6月3日</p></td>
<td style="text-align: center;"><p><a href="../Page/強敵們.md" title="wikilink">強敵們</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/蔡琳.md" title="wikilink">蔡琳</a>、<a href="../Page/李陣郁.md" title="wikilink">李陣郁</a>、<a href="../Page/李鐘赫.md" title="wikilink">李鐘赫</a></p></td>
<td style="text-align: center;"><p><a href="../Page/韓俊瑞.md" title="wikilink">韓俊瑞</a></p></td>
<td style="text-align: center;"><p><a href="../Page/姜銀慶.md" title="wikilink">姜銀慶</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2008年6月17日－<br />
2008年8月19日</p></td>
<td style="text-align: center;"><p><a href="../Page/最強七迂.md" title="wikilink">最強七迂</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/文晸赫.md" title="wikilink">文晸赫</a>、<a href="../Page/劉亞仁.md" title="wikilink">劉亞仁</a>、<a href="../Page/具慧善.md" title="wikilink">具慧善</a>、<a href="../Page/全盧民.md" title="wikilink">全盧民</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴萬英.md" title="wikilink">朴萬英</a></p></td>
<td style="text-align: center;"><p><a href="../Page/白雲哲.md" title="wikilink">白雲哲</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2008年8月25日－<br />
2008年10月20日</p></td>
<td style="text-align: center;"><p><a href="../Page/戀愛結婚_(電視劇).md" title="wikilink">戀愛結婚</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/金智勳.md" title="wikilink">金智勳</a>、<a href="../Page/金敏喜.md" title="wikilink">金敏喜</a>、<a href="../Page/朴基雄.md" title="wikilink">朴基雄</a>、<a href="../Page/尹世雅.md" title="wikilink">尹世雅</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金亨錫.md" title="wikilink">金亨錫</a></p></td>
<td style="text-align: center;"><p><a href="../Page/仁恩雅.md" title="wikilink">仁恩雅</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2008年10月27日－<br />
2008年12月26日</p></td>
<td style="text-align: center;"><p><a href="../Page/他們生活的世界.md" title="wikilink">他們生活的世界</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/宋慧喬.md" title="wikilink">宋慧喬</a>、<a href="../Page/玄彬.md" title="wikilink">玄彬</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/表民秀.md" title="wikilink">表民秀</a>、<a href="../Page/金圭泰.md" title="wikilink">金圭泰</a></p></td>
<td style="text-align: center;"><p><a href="../Page/盧熙京.md" title="wikilink">盧熙京</a></p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 2009年

<table style="width:326%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>導演</p></th>
<th style="text-align: center;"><p>編劇</p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2009年1月5日－<br />
2009年3月31日</p></td>
<td style="text-align: center;"><p><a href="../Page/流星花園_(韓國電視劇).md" title="wikilink">花樣男子</a><br />
</p></td>
<td style="text-align: center;"><p>25</p></td>
<td style="text-align: center;"><p><a href="../Page/具惠善.md" title="wikilink">具惠善</a>、<a href="../Page/李敏鎬.md" title="wikilink">李敏鎬</a>、<a href="../Page/金賢重.md" title="wikilink">金賢重</a>、<a href="../Page/金汎.md" title="wikilink">金汎</a>、<a href="../Page/金俊.md" title="wikilink">金俊</a></p></td>
<td style="text-align: center;"><p><a href="../Page/全啟相.md" title="wikilink">全啟相</a>、<a href="../Page/李民佑.md" title="wikilink">李民佑</a></p></td>
<td style="text-align: center;"><p><a href="../Page/尹池蓮.md" title="wikilink">尹池蓮</a></p></td>
<td style="text-align: center;"><p>原作：<a href="../Page/神尾葉子.md" title="wikilink">神尾葉子</a>《<a href="../Page/流星花園.md" title="wikilink">花樣男子</a>》</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2009年4月6日－<br />
2009年6月9日</p></td>
<td style="text-align: center;"><p><a href="../Page/男人的故事.md" title="wikilink">男人的故事</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/朴容夏.md" title="wikilink">朴容夏</a>、<a href="../Page/朴詩妍.md" title="wikilink">朴詩妍</a>、<a href="../Page/金剛于.md" title="wikilink">金剛-{于}-</a></p></td>
<td style="text-align: center;"><p><a href="../Page/尹成植.md" title="wikilink">尹成植</a></p></td>
<td style="text-align: center;"><p><a href="../Page/宋智娜.md" title="wikilink">宋智娜</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2009年6月15日－<br />
2009年8月4日</p></td>
<td style="text-align: center;"><p><a href="../Page/不能結婚的男人_(韓國電視劇).md" title="wikilink">不能結婚的男人</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/池珍熙.md" title="wikilink">池珍熙</a>、<a href="../Page/嚴正化.md" title="wikilink">嚴正化</a>、<a href="../Page/金素恩.md" title="wikilink">金素恩</a>、<a href="../Page/梁瀞疋.md" title="wikilink">梁瀞疋</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金正奎.md" title="wikilink">金正奎</a></p></td>
<td style="text-align: center;"><p><a href="../Page/余志娜.md" title="wikilink">余志娜</a></p></td>
<td style="text-align: center;"><p>原作：<a href="../Page/尾崎將也.md" title="wikilink">尾崎將也</a>《<a href="../Page/不能結婚的男人_(日本電視劇).md" title="wikilink">不能結婚的男人</a>》</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2009年8月10日－<br />
2009年9月8日</p></td>
<td style="text-align: center;"><p><a href="../Page/傳說的故鄉.md" title="wikilink">傳說的故鄉</a><br />
</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p><a href="../Page/全慧彬.md" title="wikilink">全慧彬</a>、<a href="../Page/金志錫.md" title="wikilink">金志錫</a>、<a href="../Page/李英恩.md" title="wikilink">李英恩</a>、<a href="../Page/鄭糠雲.md" title="wikilink">鄭糠雲</a>、<a href="../Page/金奎哲.md" title="wikilink">金奎哲</a>、<a href="../Page/趙胤熙.md" title="wikilink">趙胤熙</a>、<a href="../Page/崔真赫.md" title="wikilink">崔真赫</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李民洪.md" title="wikilink">李民洪</a>、<a href="../Page/金政珉.md" title="wikilink">金政珉</a>、<a href="../Page/洪碩九.md" title="wikilink">洪碩九</a>、<a href="../Page/閔英鎮.md" title="wikilink">閔英鎮</a>、<a href="../Page/李應福.md" title="wikilink">李應福</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金正淑.md" title="wikilink">金正淑</a>、<a href="../Page/金郎.md" title="wikilink">金郎</a>、<a href="../Page/閔恩晶.md" title="wikilink">閔恩晶</a>、<a href="../Page/朴亨鎮.md" title="wikilink">朴亨鎮</a>、<a href="../Page/彩惠英.md" title="wikilink">彩惠英</a></p></td>
<td style="text-align: center;"><p><strong>納涼特輯電視劇</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2009年9月14日－<br />
2009年11月3日</p></td>
<td style="text-align: center;"><p><a href="../Page/公主歸來.md" title="wikilink">公主歸來</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/黃薪惠.md" title="wikilink">黃薪惠</a>、<a href="../Page/吳娟受.md" title="wikilink">吳娟受</a>、<a href="../Page/卓在勛.md" title="wikilink">卓在勛</a>、<a href="../Page/李在皇.md" title="wikilink">李在皇</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/朴基鎬.md" title="wikilink">朴基鎬</a></p></td>
<td style="text-align: center;"><p><a href="../Page/林賢京.md" title="wikilink">林賢京</a>、<a href="../Page/李初恩.md" title="wikilink">李初恩</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2009年11月9日－<br />
2009年12月29日</p></td>
<td style="text-align: center;"><p><a href="../Page/天下無敵李平岡.md" title="wikilink">天下無敵李平岡</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/南相美.md" title="wikilink">南相美</a>、<a href="../Page/智鉉寓.md" title="wikilink">智鉉寓</a>、<a href="../Page/徐道永.md" title="wikilink">徐道永</a>、<a href="../Page/車藝蓮.md" title="wikilink">車藝蓮</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/李政燮.md" title="wikilink">李政燮</a>、<a href="../Page/朴鉉錫.md" title="wikilink">朴鉉錫</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴繼玉.md" title="wikilink">朴繼玉</a></p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 2010年

<table style="width:326%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>導演</p></th>
<th style="text-align: center;"><p>編劇</p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2010年1月4日－<br />
2010年2月23日</p></td>
<td style="text-align: center;"><p><a href="../Page/學習之神.md" title="wikilink">學習之神</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/金秀路.md" title="wikilink">金秀路</a>、<a href="../Page/裴斗娜.md" title="wikilink">裴斗娜</a> 、<a href="../Page/俞承豪.md" title="wikilink">俞承豪</a>、<a href="../Page/高我星.md" title="wikilink">高我星</a>、<a href="../Page/朴智妍.md" title="wikilink">朴智妍</a>、<a href="../Page/李玹雨.md" title="wikilink">李玹雨</a></p></td>
<td style="text-align: center;"><p><a href="../Page/柳賢基.md" title="wikilink">柳賢基</a></p></td>
<td style="text-align: center;"><p><a href="../Page/尹景雅.md" title="wikilink">尹景雅</a></p></td>
<td style="text-align: center;"><p>原作：<a href="../Page/三田紀房.md" title="wikilink">三田紀房</a>《<a href="../Page/東大特訓班.md" title="wikilink">東大特訓班</a>》</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2010年3月1日－<br />
2010年5月4日</p></td>
<td style="text-align: center;"><p><a href="../Page/富翁的誕生.md" title="wikilink">富翁的誕生</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/李寶英.md" title="wikilink">李寶英</a>、<a href="../Page/智鉉寓.md" title="wikilink">智鉉寓</a>、<a href="../Page/南宮民.md" title="wikilink">南宮民</a>、<a href="../Page/李詩英.md" title="wikilink">李詩英</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李振書.md" title="wikilink">李振書</a></p></td>
<td style="text-align: center;"><p><a href="../Page/崔民基.md" title="wikilink">崔民基</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2010年5月10日－<br />
2010年6月29日</p></td>
<td style="text-align: center;"><p><a href="../Page/國家的呼喚.md" title="wikilink">國家的呼喚</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/李水京.md" title="wikilink">李水京</a>、<a href="../Page/金相慶.md" title="wikilink">金相慶</a>、<a href="../Page/柳鎮_(韓國).md" title="wikilink">柳鎮</a>、<a href="../Page/蝴瓓.md" title="wikilink">蝴瓓</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金正奎.md" title="wikilink">金正奎</a></p></td>
<td style="text-align: center;"><p><a href="../Page/崔伊朗.md" title="wikilink">崔伊朗</a>、<a href="../Page/李珍梅.md" title="wikilink">李珍梅</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2010年7月5日－<br />
2010年8月24日</p></td>
<td style="text-align: center;"><p><a href="../Page/九尾狐：狐狸小妹傳.md" title="wikilink">九尾狐：狐狸小妹傳</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/韓銀貞.md" title="wikilink">韓銀貞</a>、<a href="../Page/張鉉誠.md" title="wikilink">張鉉誠</a>、<a href="../Page/徐新愛.md" title="wikilink">徐新愛</a>、<a href="../Page/金有貞.md" title="wikilink">金有貞</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李健俊.md" title="wikilink">李健俊</a>、<a href="../Page/李在尚.md" title="wikilink">李在尚</a></p></td>
<td style="text-align: center;"><p><a href="../Page/吳善亨.md" title="wikilink">吳善亨</a>、<a href="../Page/鄭惠媛.md" title="wikilink">鄭惠媛</a></p></td>
<td style="text-align: center;"><p><strong>納涼迷你劇</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2010年8月30日－<br />
2010年11月2日</p></td>
<td style="text-align: center;"><p><a href="../Page/成均館緋聞.md" title="wikilink">成均館緋聞</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/朴有天.md" title="wikilink">朴有天</a>、<a href="../Page/朴敏英.md" title="wikilink">朴敏英</a>、<a href="../Page/劉亞仁.md" title="wikilink">劉亞仁</a>、<a href="../Page/宋仲基.md" title="wikilink">宋仲基</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/金元錫.md" title="wikilink">金元錫</a>、<a href="../Page/黃仁赫.md" title="wikilink">黃仁赫</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金泰姬.md" title="wikilink">金泰姬</a></p></td>
<td style="text-align: center;"><p>原作：廷銀闕《成均館儒生們的日子》</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2010年11月8日－<br />
2011年12月28日</p></td>
<td style="text-align: center;"><p><a href="../Page/瑪莉外宿中.md" title="wikilink">瑪莉外宿中</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/張根碩.md" title="wikilink">張根碩</a>、<a href="../Page/文瑾瑩.md" title="wikilink">文瑾瑩</a>、<a href="../Page/金材昱.md" title="wikilink">金材昱</a>、<a href="../Page/金孝珍.md" title="wikilink">金孝珍</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/洪碩九.md" title="wikilink">洪碩九</a>、<a href="../Page/金應奎.md" title="wikilink">金應奎</a></p></td>
<td style="text-align: center;"><p><a href="../Page/仁恩雅.md" title="wikilink">仁恩雅</a></p></td>
<td style="text-align: center;"><p>原作：<a href="../Page/元秀蓮.md" title="wikilink">元秀蓮</a>《<a href="../Page/瑪莉外宿中.md" title="wikilink">瑪莉外宿中</a>》</p></td>
</tr>
</tbody>
</table>

### 2011年

<table style="width:326%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>導演</p></th>
<th style="text-align: center;"><p>編劇</p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2011年1月3日－<br />
2011年3月1日</p></td>
<td style="text-align: center;"><p><a href="../Page/Dream_High.md" title="wikilink">Dream High</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/金秀賢_(男演員).md" title="wikilink">金秀賢</a>、<a href="../Page/裴秀智.md" title="wikilink">裴秀智</a>、<a href="../Page/玉澤演.md" title="wikilink">玉澤演</a>、<a href="../Page/咸恩炡.md" title="wikilink">咸恩炡</a>、<a href="../Page/IU.md" title="wikilink">IU</a>、<a href="../Page/張祐榮.md" title="wikilink">張祐榮</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李應福.md" title="wikilink">李應福</a>、<a href="../Page/毛完日.md" title="wikilink">毛完日</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴惠蓮.md" title="wikilink">朴惠蓮</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2011年3月7日－<br />
2011年4月26日</p></td>
<td style="text-align: center;"><p><a href="../Page/重案組_(韓國電視劇).md" title="wikilink">重案組</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/宋一國.md" title="wikilink">宋一國</a>、<a href="../Page/李鐘赫.md" title="wikilink">李鐘赫</a>、<a href="../Page/朴宣暎.md" title="wikilink">朴宣暎</a>、<a href="../Page/鮮于善.md" title="wikilink">鮮-{于}-善</a>、<a href="../Page/宋智孝.md" title="wikilink">宋智孝</a>、<a href="../Page/金俊.md" title="wikilink">金俊</a>、<a href="../Page/李珉宇.md" title="wikilink">李珉宇</a></p></td>
<td style="text-align: center;"><p><a href="../Page/權啟洪.md" title="wikilink">權啟洪</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴成鎮.md" title="wikilink">朴成鎮</a> 、<a href="../Page/李秀賢.md" title="wikilink">李秀賢</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2011年5月2日－<br />
2011年7月5日</p></td>
<td style="text-align: center;"><p><a href="../Page/童顏美女.md" title="wikilink">童顏美女</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/張娜拉.md" title="wikilink">張娜拉</a>、<a href="../Page/崔丹尼爾.md" title="wikilink">崔丹尼爾</a>、<a href="../Page/柳鎮_(韓國).md" title="wikilink">柳鎮</a>、<a href="../Page/金旼序.md" title="wikilink">金旼序</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李振書.md" title="wikilink">李振書</a>、<a href="../Page/李昭妍.md" title="wikilink">李昭妍</a></p></td>
<td style="text-align: center;"><p><a href="../Page/吳善馨.md" title="wikilink">吳善馨</a>、<a href="../Page/鄭道允.md" title="wikilink">鄭道允</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2011年7月11日－<br />
2011年9月6日</p></td>
<td style="text-align: center;"><p><a href="../Page/間諜明月.md" title="wikilink">間諜明月</a><br />
</p></td>
<td style="text-align: center;"><p>18</p></td>
<td style="text-align: center;"><p><a href="../Page/韓藝瑟.md" title="wikilink">韓藝瑟</a>、<a href="../Page/文晸赫.md" title="wikilink">文晸赫</a>、<a href="../Page/李陣郁.md" title="wikilink">李陣郁</a>、<a href="../Page/張熙軫.md" title="wikilink">張熙軫</a></p></td>
<td style="text-align: center;"><p><a href="../Page/黃仁赫.md" title="wikilink">黃仁赫</a>、<a href="../Page/金應奎.md" title="wikilink">金應奎</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金恩英.md" title="wikilink">金恩英</a> 、<a href="../Page/金貞兒.md" title="wikilink">金貞兒</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2011年9月19日－<br />
2011年11月8日</p></td>
<td style="text-align: center;"><p><a href="../Page/波塞冬_(電視劇).md" title="wikilink">Poseidon</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/崔始源.md" title="wikilink">崔始源</a>、<a href="../Page/李成宰.md" title="wikilink">李成宰</a>、<a href="../Page/李詩英.md" title="wikilink">李詩英</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/劉哲龍.md" title="wikilink">劉哲龍</a>、<a href="../Page/吳尚源.md" title="wikilink">吳尚源</a></p></td>
<td style="text-align: center;"><p><a href="../Page/趙奎元.md" title="wikilink">趙奎元</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2011年11月14日－<br />
2012年1月17日</p></td>
<td style="text-align: center;"><p><a href="../Page/Brain.md" title="wikilink">Brain</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/申河均.md" title="wikilink">申河均</a>、<a href="../Page/鄭進永.md" title="wikilink">鄭進永</a>、<a href="../Page/崔貞媛.md" title="wikilink">崔貞媛</a>、<a href="../Page/趙東赫.md" title="wikilink">趙東赫</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/柳賢基.md" title="wikilink">柳賢基</a>、<a href="../Page/宋賢旭.md" title="wikilink">宋賢旭</a></p></td>
<td style="text-align: center;"><p><a href="../Page/尹景雅.md" title="wikilink">尹景雅</a></p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 2012年

<table style="width:326%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>導演</p></th>
<th style="text-align: center;"><p>編劇</p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2012年1月30日－<br />
2012年3月20日</p></td>
<td style="text-align: center;"><p><a href="../Page/Dream_High_2.md" title="wikilink">Dream High 2</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/姜素拉.md" title="wikilink">姜素拉</a>、<a href="../Page/鄭珍雲.md" title="wikilink">鄭珍雲</a>、<a href="../Page/朴智妍_(1993年出生).md" title="wikilink">朴智妍</a>、<a href="../Page/金孝靜.md" title="wikilink">孝琳</a>、<a href="../Page/林在範.md" title="wikilink">JB</a>、<a href="../Page/朴書俊.md" title="wikilink">朴書俊</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李應福.md" title="wikilink">李應福</a>、<a href="../Page/毛完日.md" title="wikilink">毛完日</a></p></td>
<td style="text-align: center;"><p><a href="../Page/許星慧.md" title="wikilink">許星慧</a>、<a href="../Page/張恩美.md" title="wikilink">張恩美</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2012年3月26日－<br />
2012年5月29日</p></td>
<td style="text-align: center;"><p><a href="../Page/愛情雨.md" title="wikilink">愛情雨</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/張根碩.md" title="wikilink">張根碩</a>、<a href="../Page/潤娥.md" title="wikilink">潤娥</a>、<a href="../Page/鄭進永.md" title="wikilink">鄭進永</a>、<a href="../Page/李美淑.md" title="wikilink">李美淑</a>、<a href="../Page/金時厚.md" title="wikilink">金時厚</a>、<a href="../Page/金英光.md" title="wikilink">金英光</a>、<a href="../Page/孫恩書.md" title="wikilink">孫恩書</a>、<a href="../Page/黃寶羅.md" title="wikilink">黃寶羅</a>、<a href="../Page/徐仁國.md" title="wikilink">徐仁國</a></p></td>
<td style="text-align: center;"><p><a href="../Page/尹錫湖.md" title="wikilink">尹錫湖</a></p></td>
<td style="text-align: center;"><p><a href="../Page/吳秀妍.md" title="wikilink">吳秀妍</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2012年6月4日－<br />
2012年7月24日</p></td>
<td style="text-align: center;"><p><a href="../Page/Big.md" title="wikilink">Big</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/孔劉.md" title="wikilink">孔劉</a>、<a href="../Page/李珉廷.md" title="wikilink">李珉廷</a>、<a href="../Page/裴秀智.md" title="wikilink">裴秀智</a>、<a href="../Page/張熙軫.md" title="wikilink">張熙軫</a>、<a href="../Page/申園昊.md" title="wikilink">申園昊</a>、<a href="../Page/白成鉉.md" title="wikilink">白成鉉</a></p></td>
<td style="text-align: center;"><p><a href="../Page/池炳賢.md" title="wikilink">池炳賢</a>、<a href="../Page/金善允.md" title="wikilink">金善允</a></p></td>
<td style="text-align: center;"><p><a href="../Page/洪貞恩.md" title="wikilink">洪貞恩</a>、<a href="../Page/洪美蘭.md" title="wikilink">洪美蘭</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2012年8月6日－<br />
2012年9月25日</p></td>
<td style="text-align: center;"><p><a href="../Page/海雲臺戀人們.md" title="wikilink">海雲臺戀人們</a><br />
</p></td>
<td style="text-align: center;"><p>17</p></td>
<td style="text-align: center;"><p><a href="../Page/金剛于.md" title="wikilink">金剛-{于}-</a>、<a href="../Page/趙如晶.md" title="wikilink">趙如晶</a>、<a href="../Page/鄭錫遠.md" title="wikilink">鄭錫遠</a>、<a href="../Page/南奎麗.md" title="wikilink">南奎麗</a>、<a href="../Page/姜珉耿.md" title="wikilink">姜珉耿</a></p></td>
<td style="text-align: center;"><p><a href="../Page/宋賢旭.md" title="wikilink">宋賢旭</a>、<a href="../Page/朴鎮錫.md" title="wikilink">朴鎮錫</a></p></td>
<td style="text-align: center;"><p><a href="../Page/黃恩京.md" title="wikilink">黃恩京</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2012年11月7日－<br />
2012年12月27日</p></td>
<td style="text-align: center;"><p><a href="../Page/嗚啦啦夫婦.md" title="wikilink">嗚啦啦夫婦</a><br />
</p></td>
<td style="text-align: center;"><p>18</p></td>
<td style="text-align: center;"><p><a href="../Page/申鉉濬.md" title="wikilink">申鉉濬</a>、<a href="../Page/金廷恩.md" title="wikilink">金廷恩</a>、<a href="../Page/韓載錫.md" title="wikilink">韓載錫</a>、<a href="../Page/韓彩雅.md" title="wikilink">韓彩雅</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/李政燮.md" title="wikilink">李政燮</a>、<a href="../Page/全雨盛.md" title="wikilink">全雨盛</a></p></td>
<td style="text-align: center;"><p><a href="../Page/崔順植.md" title="wikilink">崔順植</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2012年12月3日－<br />
2013年1月28日</p></td>
<td style="text-align: center;"><p><a href="../Page/學校2013.md" title="wikilink">學校2013</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/張娜拉.md" title="wikilink">張娜拉</a>、<a href="../Page/崔丹尼爾.md" title="wikilink">崔丹尼爾</a>、<a href="../Page/李鍾碩.md" title="wikilink">李鍾碩</a>、<a href="../Page/金宇彬.md" title="wikilink">金宇彬</a>、<a href="../Page/朴世榮.md" title="wikilink">朴世榮</a>、<a href="../Page/柳孝榮.md" title="wikilink">柳孝榮</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/李民洪.md" title="wikilink">李民洪</a>、<a href="../Page/李應福.md" title="wikilink">李應福</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李賢珠.md" title="wikilink">李賢珠</a>、<a href="../Page/高正沅.md" title="wikilink">高正沅</a></p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 2013年

<table style="width:326%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>導演</p></th>
<th style="text-align: center;"><p>編劇</p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2013年2月4日－<br />
2013年3月26日</p></td>
<td style="text-align: center;"><p><a href="../Page/廣告天才李太白.md" title="wikilink">廣告天才李太白</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/晉久.md" title="wikilink">晉久</a>、<a href="../Page/朴河宣.md" title="wikilink">朴河宣</a>、<a href="../Page/趙顯宰.md" title="wikilink">趙顯宰</a>、<a href="../Page/韓彩英.md" title="wikilink">韓彩英</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴基鎬.md" title="wikilink">朴基鎬</a>、<a href="../Page/李昭妍.md" title="wikilink">李昭妍</a></p></td>
<td style="text-align: center;"><p><a href="../Page/薛俊錫.md" title="wikilink">薛俊錫</a>、<a href="../Page/李載河.md" title="wikilink">李載河</a>、<a href="../Page/李允鍾.md" title="wikilink">李允鍾</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2013年4月1日－<br />
2013年5月21日</p></td>
<td style="text-align: center;"><p><a href="../Page/職場之神.md" title="wikilink">職場之神</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/金惠秀.md" title="wikilink">金惠秀</a>、<a href="../Page/吳智昊.md" title="wikilink">吳智昊</a>、<a href="../Page/鄭有美.md" title="wikilink">鄭有美</a>、<a href="../Page/李熙俊.md" title="wikilink">李熙俊</a>、<a href="../Page/全慧彬.md" title="wikilink">全慧-{}-彬</a>、<a href="../Page/趙權.md" title="wikilink">趙權</a></p></td>
<td style="text-align: center;"><p><a href="../Page/全昌根.md" title="wikilink">全昌根</a>、<a href="../Page/盧尚勳.md" title="wikilink">盧尚勳</a></p></td>
<td style="text-align: center;"><p><a href="../Page/尹蘭中.md" title="wikilink">尹蘭中</a></p></td>
<td style="text-align: center;"><p>原作：<a href="../Page/中園美保.md" title="wikilink">中園美保</a>《<a href="../Page/派遣女王.md" title="wikilink">派遣女王</a>》</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2013年5月27日－<br />
2013年7月30日</p></td>
<td style="text-align: center;"><p><a href="../Page/鯊魚_(韓國電視劇).md" title="wikilink">鯊魚</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/金南佶.md" title="wikilink">金南佶</a>、<a href="../Page/孫藝真.md" title="wikilink">孫藝真</a>、<a href="../Page/河錫辰.md" title="wikilink">河錫辰</a>、<a href="../Page/李荷妮.md" title="wikilink">李荷妮</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴燦弘.md" title="wikilink">朴燦弘</a>、<a href="../Page/車榮勳.md" title="wikilink">車榮勳</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金志宇.md" title="wikilink">金志宇</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2013年8月5日－<br />
2013年10月8日</p></td>
<td style="text-align: center;"><p><a href="../Page/Good_Doctor.md" title="wikilink">Good Doctor</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/周元_(韓國).md" title="wikilink">周元</a>、<a href="../Page/文彩元.md" title="wikilink">文彩元</a>、<a href="../Page/朱相昱.md" title="wikilink">朱相昱</a>、<a href="../Page/金旼序.md" title="wikilink">金旼序</a>、<a href="../Page/千虎珍.md" title="wikilink">千虎珍</a>、<a href="../Page/郭度沅.md" title="wikilink">郭度沅</a></p></td>
<td style="text-align: center;"><p><a href="../Page/奇民秀.md" title="wikilink">奇民秀</a>、<a href="../Page/金振宇_(導演).md" title="wikilink">金振宇</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴才範.md" title="wikilink">朴才範</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2013年10月14日－<br />
2013年12月3日</p></td>
<td style="text-align: center;"><p><a href="../Page/未來的選擇.md" title="wikilink">未來的選擇</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/尹恩惠.md" title="wikilink">尹恩惠</a>、<a href="../Page/李東健.md" title="wikilink">李東健</a>、<a href="../Page/鄭容和.md" title="wikilink">鄭容和</a>、<a href="../Page/韓彩雅.md" title="wikilink">韓彩雅</a>、<a href="../Page/崔明吉_(藝人).md" title="wikilink">崔明吉</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/權啟洪.md" title="wikilink">權啟洪</a>、<a href="../Page/尹鐘善.md" title="wikilink">尹鐘善</a></p></td>
<td style="text-align: center;"><p><a href="../Page/洪氏姊妹#洪珍兒、洪紫蘭.md" title="wikilink">洪珍兒</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2013年12月9日－<br />
2014年2月4日</p></td>
<td style="text-align: center;"><p><a href="../Page/總理與我.md" title="wikilink">總理與我</a><br />
</p></td>
<td style="text-align: center;"><p>17</p></td>
<td style="text-align: center;"><p><a href="../Page/李凡秀.md" title="wikilink">李凡秀</a>、<a href="../Page/林潤娥.md" title="wikilink">潤娥</a>、<a href="../Page/尹施允.md" title="wikilink">尹施允</a>、<a href="../Page/蔡貞安.md" title="wikilink">蔡貞安</a>、<a href="../Page/柳鎮_(韓國).md" title="wikilink">柳鎮</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/李昭妍.md" title="wikilink">李昭妍</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金銀姬.md" title="wikilink">金銀姬</a>、<a href="../Page/尹恩景.md" title="wikilink">尹恩景</a></p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 2014年

<table style="width:326%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>導演</p></th>
<th style="text-align: center;"><p>編劇</p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2014年2月17日－<br />
2014年4月8日</p></td>
<td style="text-align: center;"><p><a href="../Page/陽光滿溢.md" title="wikilink">陽光滿溢</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/尹啟相.md" title="wikilink">尹啟相</a>、<a href="../Page/韓智慧.md" title="wikilink">韓智慧</a>、<a href="../Page/趙震雄.md" title="wikilink">趙震雄</a>、<a href="../Page/金釉利.md" title="wikilink">金釉利</a></p></td>
<td style="text-align: center;"><p><a href="../Page/裴景修.md" title="wikilink">裴景修</a>、<a href="../Page/金政賢.md" title="wikilink">金政賢</a></p></td>
<td style="text-align: center;"><p><a href="../Page/許星慧.md" title="wikilink">許星慧</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2014年4月28日－<br />
2014年6月17日</p></td>
<td style="text-align: center;"><p><a href="../Page/Big_Man.md" title="wikilink">Big Man</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/姜至奐.md" title="wikilink">姜至奐</a>、<a href="../Page/李多熙.md" title="wikilink">李多熙</a>、<a href="../Page/崔丹尼爾.md" title="wikilink">崔丹尼爾</a>、<a href="../Page/庭沼珉.md" title="wikilink">庭沼珉</a></p></td>
<td style="text-align: center;"><p><a href="../Page/池英洙.md" title="wikilink">池英洙</a></p></td>
<td style="text-align: center;"><p><a href="../Page/崔鎮源.md" title="wikilink">崔鎮源</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2014年6月23日－<br />
2014年8月12日</p></td>
<td style="text-align: center;"><p><a href="../Page/Trot戀人.md" title="wikilink">Trot戀人</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/智鉉寓.md" title="wikilink">智鉉寓</a>、<a href="../Page/鄭恩地.md" title="wikilink">鄭恩地</a>、<a href="../Page/申成祿.md" title="wikilink">申成祿</a>、<a href="../Page/孫浩俊.md" title="wikilink">孫浩俊</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李在尚.md" title="wikilink">李在尚</a>、<a href="../Page/李恩鎮.md" title="wikilink">李恩鎮</a></p></td>
<td style="text-align: center;"><p><a href="../Page/吳善馨.md" title="wikilink">吳善馨</a>、<a href="../Page/姜允京.md" title="wikilink">姜允京</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2014年8月18日－<br />
2014年10月7日</p></td>
<td style="text-align: center;"><p><a href="../Page/戀愛的發現.md" title="wikilink">戀愛的發現</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/文晸赫.md" title="wikilink">文晸赫</a>、<a href="../Page/鄭有美.md" title="wikilink">鄭有美</a>、<a href="../Page/盛駿.md" title="wikilink">盛駿</a>、<a href="../Page/尹賢旻.md" title="wikilink">尹賢旻</a>、<a href="../Page/金瑟琪.md" title="wikilink">金瑟琪</a>、<a href="../Page/尹真伊.md" title="wikilink">尹真伊</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金成允.md" title="wikilink">金成允</a>、<a href="../Page/李應福.md" title="wikilink">李應福</a></p></td>
<td style="text-align: center;"><p><a href="../Page/鄭賢貞.md" title="wikilink">鄭賢貞</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2014年10月13日－<br />
2014年12月2日</p></td>
<td style="text-align: center;"><p><a href="../Page/交響情人夢最終樂章_(韓國電視劇).md" title="wikilink">明日如歌</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/周元_(韓國).md" title="wikilink">周元</a>、<a href="../Page/沈恩敬.md" title="wikilink">沈恩敬</a>、<a href="../Page/白潤植.md" title="wikilink">白潤植</a>、<a href="../Page/閔都凞.md" title="wikilink">閔都凞</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/韓相佑.md" title="wikilink">韓相佑</a>、<a href="../Page/李廷美_(導演).md" title="wikilink">李廷美</a></p></td>
<td style="text-align: center;"><p><a href="../Page/申在元.md" title="wikilink">申在元</a>、<a href="../Page/朴畢珠.md" title="wikilink">朴畢珠</a></p></td>
<td style="text-align: center;"><p>原作：<a href="../Page/二之宮知子.md" title="wikilink">二之宮知子</a>《<a href="../Page/交響情人夢.md" title="wikilink">交響情人夢</a>》</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2014年12月8日－<br />
2015年2月10日</p></td>
<td style="text-align: center;"><p><a href="../Page/Healer.md" title="wikilink">Healer</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/池昌旭.md" title="wikilink">池昌旭</a>、<a href="../Page/劉智泰.md" title="wikilink">劉智泰</a>、<a href="../Page/朴敏英.md" title="wikilink">朴敏英</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/李政燮.md" title="wikilink">李政燮</a>、<a href="../Page/金振宇_(導演).md" title="wikilink">金振宇</a></p></td>
<td style="text-align: center;"><p><a href="../Page/宋智娜.md" title="wikilink">宋智娜</a></p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 2015年

<table style="width:326%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>導演</p></th>
<th style="text-align: center;"><p>編劇</p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2015年2月16日－<br />
2015年4月21日</p></td>
<td style="text-align: center;"><p><a href="../Page/Blood_(電視劇).md" title="wikilink">Blood</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/安宰賢.md" title="wikilink">安宰賢</a>、<a href="../Page/池珍熙.md" title="wikilink">池珍熙</a>、<a href="../Page/具惠善.md" title="wikilink">具惠善</a>、<a href="../Page/孫秀賢.md" title="wikilink">孫秀賢</a>、<a href="../Page/丁海寅.md" title="wikilink">丁海寅</a>、<a href="../Page/鄭惠成.md" title="wikilink">鄭惠成</a>、<a href="../Page/陳慶_(南韓).md" title="wikilink">陳　慶</a>、<a href="../Page/金佑錫.md" title="wikilink">金佑錫</a>、<a href="../Page/權賢尚.md" title="wikilink">權賢尚</a></p></td>
<td style="text-align: center;"><p><a href="../Page/奇民秀.md" title="wikilink">奇民秀</a>、<a href="../Page/李在勳.md" title="wikilink">李在勳</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴才範.md" title="wikilink">朴才範</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2015年4月27日－<br />
2015年6月16日</p></td>
<td style="text-align: center;"><p><a href="../Page/Who_Are_You－學校2015.md" title="wikilink">Who Are You－學校2015</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p>|<a href="../Page/金所炫.md" title="wikilink">金所炫</a>、<a href="../Page/陸星材.md" title="wikilink">陸星材</a>、<a href="../Page/南柱赫.md" title="wikilink">南柱赫</a>、<a href="../Page/全美善.md" title="wikilink">全美善</a>、<a href="../Page/李必模.md" title="wikilink">李必模</a>、<a href="../Page/曹秀香.md" title="wikilink">曹秀香</a>、<a href="../Page/金希珍.md" title="wikilink">金希珍</a>、<a href="../Page/李礎熙.md" title="wikilink">李礎熙</a>、<a href="../Page/李大衛_(演員).md" title="wikilink">李大衛</a></p></td>
<td style="text-align: center;"><p><a href="../Page/白尚勳.md" title="wikilink">白尚勳</a>、<a href="../Page/金成允.md" title="wikilink">金成允</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金賢貞.md" title="wikilink">金賢貞</a>、<a href="../Page/金敏貞_(編劇).md" title="wikilink">金敏貞</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2015年6月22日－<br />
2015年8月11日</p></td>
<td style="text-align: center;"><p><a href="../Page/記得你.md" title="wikilink">記得你</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/徐仁國.md" title="wikilink">徐仁國</a>、<a href="../Page/張娜拉.md" title="wikilink">張娜拉</a>、<a href="../Page/崔元英.md" title="wikilink">崔元英</a>、<a href="../Page/李天熙.md" title="wikilink">李天熙</a>、<a href="../Page/朴寶劍.md" title="wikilink">朴寶劍</a></p></td>
<td style="text-align: center;"><p><a href="../Page/盧尚勳.md" title="wikilink">盧尚勳</a>、<a href="../Page/金鎮元.md" title="wikilink">金鎮元</a></p></td>
<td style="text-align: center;"><p><a href="../Page/權基瑛.md" title="wikilink">權基瑛</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2015年8月17日－<br />
2015年9月22日</p></td>
<td style="text-align: center;"><p><a href="../Page/奇怪的兒媳.md" title="wikilink">奇怪的兒媳</a><br />
</p></td>
<td style="text-align: center;"><p>12</p></td>
<td style="text-align: center;"><p><a href="../Page/高斗心.md" title="wikilink">高斗心</a>、<a href="../Page/金多絮.md" title="wikilink">金多絮</a>、<a href="../Page/柳秀榮.md" title="wikilink">柳秀榮</a>、<a href="../Page/奇太映.md" title="wikilink">奇太映</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李德健.md" title="wikilink">李德健</a>、<a href="../Page/朴萬英.md" title="wikilink">朴萬英</a></p></td>
<td style="text-align: center;"><p><a href="../Page/文善熙.md" title="wikilink">文善熙</a>、<a href="../Page/劉南京.md" title="wikilink">劉南京</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2015年10月5日－<br />
2015年11月10日</p></td>
<td style="text-align: center;"><p><a href="../Page/無理的前進.md" title="wikilink">無理的前進</a><br />
</p></td>
<td style="text-align: center;"><p>12</p></td>
<td style="text-align: center;"><p><a href="../Page/鄭恩地.md" title="wikilink">鄭恩地</a>、<a href="../Page/李源根.md" title="wikilink">李源根</a>、<a href="../Page/蔡秀彬.md" title="wikilink">蔡秀彬</a>、<a href="../Page/VIXX.md" title="wikilink">車學沇</a>、<a href="../Page/Jisoo.md" title="wikilink">Jisoo</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李恩鎮.md" title="wikilink">李恩鎮</a>、<a href="../Page/金正玄.md" title="wikilink">金正玄</a></p></td>
<td style="text-align: center;"><p><a href="../Page/尹秀貞.md" title="wikilink">尹秀貞</a>、<a href="../Page/鄭燦美.md" title="wikilink">鄭燦美</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2015年11月16日－<br />
2016年1月5日</p></td>
<td style="text-align: center;"><p><a href="../Page/Oh_My_Venus.md" title="wikilink">Oh My Venus</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/蘇志燮.md" title="wikilink">蘇志燮</a>、<a href="../Page/新慜娥.md" title="wikilink">新慜娥</a>、<a href="../Page/鄭糠雲.md" title="wikilink">鄭糠雲</a>、<a href="../Page/柳仁英.md" title="wikilink">柳仁英</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金亨碩.md" title="wikilink">金亨碩</a>、<a href="../Page/李娜靜.md" title="wikilink">李娜靜</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金恩智.md" title="wikilink">金恩智</a></p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 2016年

<table style="width:326%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>导演</p></th>
<th style="text-align: center;"><p>编剧</p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2016年1月11日－<br />
2016年3月8日</p></td>
<td style="text-align: center;"><p><a href="../Page/武林學校.md" title="wikilink">武林學校</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/申鉉濬.md" title="wikilink">申鉉濬</a>、<a href="../Page/李玹雨.md" title="wikilink">李玹雨</a>、<a href="../Page/徐睿知.md" title="wikilink">徐睿知</a>、<a href="../Page/李弘彬.md" title="wikilink">李弘彬</a>、<a href="../Page/鄭釉珍.md" title="wikilink">鄭釉珍</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李昭妍.md" title="wikilink">李昭妍</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金賢熙.md" title="wikilink">金賢熙</a>、<a href="../Page/梁真雅.md" title="wikilink">梁真雅</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2016年3月14日－<br />
2016年3月22日</p></td>
<td style="text-align: center;"><p><a href="../Page/Baby_Sitter.md" title="wikilink">Baby Sitter</a><br />
</p></td>
<td style="text-align: center;"><p>4</p></td>
<td style="text-align: center;"><p><a href="../Page/趙如晶.md" title="wikilink">趙如晶</a>、<a href="../Page/金敏俊.md" title="wikilink">金敏俊</a>、<a href="../Page/申潤珠.md" title="wikilink">申潤珠</a>、<a href="../Page/李承俊.md" title="wikilink">李承俊</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金容秀.md" title="wikilink">金容秀</a></p></td>
<td style="text-align: center;"><p><a href="../Page/崔孝妃.md" title="wikilink">崔孝妃</a></p></td>
<td style="text-align: center;"><p><strong>特別劇</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2016年3月28日－<br />
2016年5月31日</p></td>
<td style="text-align: center;"><p><a href="../Page/鄰家律師趙德浩.md" title="wikilink">鄰家律師趙德浩</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/朴新陽.md" title="wikilink">朴新陽</a>、<a href="../Page/姜素拉.md" title="wikilink">姜素拉</a>、<a href="../Page/柳秀榮.md" title="wikilink">柳秀榮</a>、<a href="../Page/朴帥眉.md" title="wikilink">朴帥眉</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李政燮.md" title="wikilink">李政燮</a>、<a href="../Page/李恩真.md" title="wikilink">李恩真</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李香姬.md" title="wikilink">李香姬</a></p></td>
<td style="text-align: center;"><p>原作：《鄰家律師趙德浩》</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2016年6月6日－<br />
2016年6月14日</p></td>
<td style="text-align: center;"><p><a href="../Page/白熙回來了.md" title="wikilink">白熙回來了</a><br />
</p></td>
<td style="text-align: center;"><p>4</p></td>
<td style="text-align: center;"><p><a href="../Page/強藝元.md" title="wikilink">強藝元</a>、<a href="../Page/金盛吳.md" title="wikilink">金盛吳</a>、<a href="../Page/崔弼立.md" title="wikilink">崔弼立</a>、<a href="../Page/陳智熙.md" title="wikilink">陳智熙</a></p></td>
<td style="text-align: center;"><p><a href="../Page/車榮勳.md" title="wikilink">車榮勳</a></p></td>
<td style="text-align: center;"><p><a href="../Page/林尚春.md" title="wikilink">林尚春</a></p></td>
<td style="text-align: center;"><p><strong>特別劇</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2016年6月20日－<br />
2016年8月2日</p></td>
<td style="text-align: center;"><p><a href="../Page/Beautiful_Mind.md" title="wikilink">Beautiful Mind</a><br />
</p></td>
<td style="text-align: center;"><p>14</p></td>
<td style="text-align: center;"><p><a href="../Page/張赫_(韓國).md" title="wikilink">張赫</a>、<a href="../Page/樸素淡.md" title="wikilink">樸素淡</a>、<a href="../Page/尹賢旻.md" title="wikilink">尹賢旻</a>、<a href="../Page/朴世榮.md" title="wikilink">朴世榮</a></p></td>
<td style="text-align: center;"><p><a href="../Page/毛完日.md" title="wikilink">毛完日</a>、<a href="../Page/李帝勳_(導演).md" title="wikilink">李帝勳</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金泰姬.md" title="wikilink">金泰姬</a></p></td>
<td style="text-align: center;"><p>原作：<a href="../Page/瑪莉·雪萊.md" title="wikilink">瑪莉·雪萊</a> 《<a href="../Page/科學怪人.md" title="wikilink">科學怪人</a>》</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2016年8月22日－<br />
2016年10月18日</p></td>
<td style="text-align: center;"><p><a href="../Page/雲畫的月光.md" title="wikilink">雲畫的月光</a><br />
</p></td>
<td style="text-align: center;"><p>18</p></td>
<td style="text-align: center;"><p><a href="../Page/朴寶劍.md" title="wikilink">朴寶劍</a>、<a href="../Page/金裕貞.md" title="wikilink">金裕貞</a>、<a href="../Page/鄭振永.md" title="wikilink">鄭振永</a>、<a href="../Page/蔡秀彬.md" title="wikilink">蔡秀彬</a>、<a href="../Page/郭東延.md" title="wikilink">郭東延</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金成允.md" title="wikilink">金成允</a>、<a href="../Page/白尚勳.md" title="wikilink">白尚勳</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金敏貞_(編劇).md" title="wikilink">金敏貞</a>、<a href="../Page/林藝珍_(編劇).md" title="wikilink">林藝珍</a></p></td>
<td style="text-align: center;"><p>原作：《雲畫的月光》</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2016年10月24日－<br />
2016年12月13日</p></td>
<td style="text-align: center;"><p><a href="../Page/住在我家的男人.md" title="wikilink">住在我家的男人</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/秀愛.md" title="wikilink">秀愛</a>、<a href="../Page/金英光.md" title="wikilink">金英光</a>、<a href="../Page/趙寶兒.md" title="wikilink">趙寶兒</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金政珉.md" title="wikilink">金政珉</a></p></td>
<td style="text-align: center;"><p><a href="../Page/金恩晶.md" title="wikilink">金恩晶</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2016年12月19日－<br />
2017年2月21日</p></td>
<td style="text-align: center;"><p><a href="../Page/花郎_(電視劇).md" title="wikilink">花郎</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/朴敘俊.md" title="wikilink">朴敘俊</a>、<a href="../Page/朴炯植.md" title="wikilink">朴炯植</a>、<a href="../Page/高雅羅.md" title="wikilink">高雅羅</a>、<a href="../Page/崔珉豪.md" title="wikilink">崔珉豪</a>、<a href="../Page/都枝寒.md" title="wikilink">都枝寒</a>、<a href="../Page/金泰亨_(1995年生).md" title="wikilink">金泰亨</a></p></td>
<td style="text-align: center;"><p><a href="../Page/尹成植.md" title="wikilink">尹成植</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴恩英.md" title="wikilink">朴恩英</a></p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 2017年

<table style="width:326%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>导演</p></th>
<th style="text-align: center;"><p>编剧</p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2017年2月27日－<br />
2017年5月2日</p></td>
<td style="text-align: center;"><p><a href="../Page/完美的妻子.md" title="wikilink">完美的妻子</a><br />
</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/高素榮.md" title="wikilink">高素榮</a>、<a href="../Page/尹相鉉.md" title="wikilink">尹相鉉</a>、<a href="../Page/趙如晶.md" title="wikilink">趙如晶</a>、<a href="../Page/盛駿.md" title="wikilink">盛駿</a></p></td>
<td style="text-align: center;"><p><a href="../Page/洪碩九.md" title="wikilink">洪碩九</a></p></td>
<td style="text-align: center;"><p><a href="../Page/尹景雅.md" title="wikilink">尹景雅</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2017年5月8日－<br />
2017年5月9日</p></td>
<td style="text-align: center;"><p><a href="../Page/個人主義者智英小姐.md" title="wikilink">個人主義者智英小姐</a><br />
</p></td>
<td style="text-align: center;"><p>2</p></td>
<td style="text-align: center;"><p><a href="../Page/閔孝琳.md" title="wikilink">閔孝琳</a>、<a href="../Page/孔明_(藝人).md" title="wikilink">孔明</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴鉉錫.md" title="wikilink">朴鉉錫</a></p></td>
<td style="text-align: center;"><p><a href="../Page/權慧智.md" title="wikilink">權慧智</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2017年5月22日－<br />
2017年7月11日</p></td>
<td style="text-align: center;"><p><a href="../Page/三流之路.md" title="wikilink">三流之路</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/朴敘俊.md" title="wikilink">朴敘俊</a>、<a href="../Page/金智媛.md" title="wikilink">金智媛</a>、<a href="../Page/安宰弘.md" title="wikilink">安宰弘</a>、<a href="../Page/宋昰昀.md" title="wikilink">宋昰昀</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李娜靜.md" title="wikilink">李娜靜</a>、<a href="../Page/金東輝.md" title="wikilink">金東輝</a></p></td>
<td style="text-align: center;"><p><a href="../Page/林尚春.md" title="wikilink">林尚春</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2017年7月17日－<br />
2017年9月5日</p></td>
<td style="text-align: center;"><p><a href="../Page/學校2017.md" title="wikilink">學校2017</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/金世正.md" title="wikilink">金世正</a>、<a href="../Page/金正鉉_(演員).md" title="wikilink">金正鉉</a>、<a href="../Page/張東尹.md" title="wikilink">張東尹</a>、<a href="../Page/韓周完.md" title="wikilink">韓周完</a>、<a href="../Page/韓善伙.md" title="wikilink">韓善伙</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴珍錫.md" title="wikilink">朴珍錫</a>、<a href="../Page/宋民葉.md" title="wikilink">宋民葉</a></p></td>
<td style="text-align: center;"><p><a href="../Page/鄭燦美.md" title="wikilink">鄭燦美</a>、<a href="../Page/金勝元.md" title="wikilink">金勝元</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2017年9月11日－<br />
2017年10月3日</p></td>
<td style="text-align: center;"><p><a href="../Page/內衣少女時代.md" title="wikilink">內衣少女時代</a><br />
</p></td>
<td style="text-align: center;"><p>8</p></td>
<td style="text-align: center;"><p><a href="../Page/苞娜.md" title="wikilink">苞娜</a>、<a href="../Page/蔡舒辰.md" title="wikilink">蔡舒辰</a>、<a href="../Page/徐榮柱.md" title="wikilink">徐榮柱</a>、<a href="../Page/李宗泫.md" title="wikilink">李宗泫</a>、<a href="../Page/呂會鉉.md" title="wikilink">呂會鉉</a>、<a href="../Page/閔都凞.md" title="wikilink">閔都凞</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/洪碩九.md" title="wikilink">洪碩九</a></p></td>
<td style="text-align: center;"><p><a href="../Page/尹景雅.md" title="wikilink">尹景雅</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2017年10月9日－<br />
2017年11月28日</p></td>
<td style="text-align: center;"><p><a href="../Page/魔女的法庭.md" title="wikilink">魔女的法庭</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/鄭麗媛.md" title="wikilink">鄭麗媛</a>、<a href="../Page/尹賢旻.md" title="wikilink">尹賢旻</a>、<a href="../Page/田光烈_(韓國).md" title="wikilink">田光烈</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/金永鈞.md" title="wikilink">金永鈞</a></p></td>
<td style="text-align: center;"><p><a href="../Page/鄭道允.md" title="wikilink">鄭道允</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2017年12月4日－<br />
2018年1月日</p></td>
<td style="text-align: center;"><p><a href="../Page/Jugglers.md" title="wikilink">Jugglers</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/崔丹尼爾.md" title="wikilink">崔丹尼爾</a>、<a href="../Page/白珍熙.md" title="wikilink">白珍熙</a>、<a href="../Page/姜惠貞.md" title="wikilink">姜惠貞</a>、<a href="../Page/李源根.md" title="wikilink">李源根</a></p></td>
<td style="text-align: center;"><p>|<a href="../Page/金正鉉_(導演).md" title="wikilink">金正鉉</a></p></td>
<td style="text-align: center;"><p><a href="../Page/趙容_(編劇).md" title="wikilink">趙容</a></p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 2018年

<table style="width:343%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 90%" />
<col style="width: 12%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>導演</p></th>
<th style="text-align: center;"><p>編劇</p></th>
<th style="text-align: center;"><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2018年1月29日－<br />
2018年3月20日</p></td>
<td style="text-align: center;"><p><a href="../Page/Radio_Romance.md" title="wikilink">Radio Romance</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/尹斗俊.md" title="wikilink">尹斗俊</a>、<a href="../Page/金所炫.md" title="wikilink">金所炫</a>、<a href="../Page/尹博.md" title="wikilink">尹博</a>、<a href="../Page/Yura.md" title="wikilink">Yura</a></p></td>
<td style="text-align: center;"><p><a href="../Page/文浚河.md" title="wikilink">文浚河</a></p></td>
<td style="text-align: center;"><p><a href="../Page/全有利.md" title="wikilink">全有利</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2018年4月2日－<br />
2018年5月29日</p></td>
<td style="text-align: center;"><p><a href="../Page/我們遇見的奇蹟.md" title="wikilink">我們遇見的奇蹟</a><br />
</p></td>
<td style="text-align: center;"><p>18</p></td>
<td style="text-align: center;"><p><a href="../Page/金明民.md" title="wikilink">金明民</a>、<a href="../Page/金賢珠.md" title="wikilink">金賢珠</a>、<a href="../Page/羅美蘭.md" title="wikilink">羅美蘭</a>、<a href="../Page/高昌錫.md" title="wikilink">高昌錫</a>、</p></td>
<td style="text-align: center;"><p><a href="../Page/李亨民.md" title="wikilink">李亨民</a></p></td>
<td style="text-align: center;"><p><a href="../Page/白美瓊.md" title="wikilink">白美瓊</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2018年6月4日－<br />
2018年8月7日</p></td>
<td style="text-align: center;"><p><a href="../Page/你也是人類嗎.md" title="wikilink">你也是人類嗎</a><br />
</p></td>
<td style="text-align: center;"><p>36</p></td>
<td style="text-align: center;"><p><a href="../Page/徐康俊.md" title="wikilink">徐康俊</a>、<a href="../Page/孔昇延.md" title="wikilink">孔昇延</a>、<a href="../Page/李浚赫.md" title="wikilink">李浚赫</a>、<a href="../Page/朴煥熙.md" title="wikilink">朴煥熙</a></p></td>
<td style="text-align: center;"><p><a href="../Page/車榮勳.md" title="wikilink">車榮勳</a></p></td>
<td style="text-align: center;"><p><a href="../Page/趙靜珠.md" title="wikilink">趙靜珠</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2018年8月13日－<br />
2018年10月2日</p></td>
<td style="text-align: center;"><p><a href="../Page/Lovely_Horribly.md" title="wikilink">Lovely Horribly</a><br />
</p></td>
<td style="text-align: center;"><p>32</p></td>
<td style="text-align: center;"><p><a href="../Page/朴施厚.md" title="wikilink">朴施厚</a>、<a href="../Page/宋智孝.md" title="wikilink">宋智孝</a>、<a href="../Page/李起光.md" title="wikilink">李起光</a>、<a href="../Page/咸𤨒晶.md" title="wikilink">咸𤨒晶</a>、<a href="../Page/崔汝珍.md" title="wikilink">崔汝珍</a></p></td>
<td style="text-align: center;"><p><a href="../Page/姜敏慶.md" title="wikilink">姜敏慶</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴敏珠.md" title="wikilink">朴敏珠</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2018年10月8日－<br />
2018年11月27日</p></td>
<td style="text-align: center;"><p><a href="../Page/最完美的離婚_(韓國電視劇).md" title="wikilink">最完美的離婚</a><br />
</p></td>
<td style="text-align: center;"><p>32</p></td>
<td style="text-align: center;"><p><a href="../Page/車太鉉.md" title="wikilink">車太鉉</a>、<a href="../Page/裴斗娜.md" title="wikilink">裴斗娜</a>、<a href="../Page/李伊.md" title="wikilink">李伊</a>、<a href="../Page/孫錫久.md" title="wikilink">孫錫久</a></p></td>
<td style="text-align: center;"><p><a href="../Page/柳賢基.md" title="wikilink">柳賢基</a></p></td>
<td style="text-align: center;"><p><a href="../Page/文正民.md" title="wikilink">文正民</a></p></td>
<td style="text-align: center;"><p>原作：<a href="../Page/坂元裕二.md" title="wikilink">坂元裕二</a>《<a href="../Page/最完美的離婚.md" title="wikilink">最完美的離婚</a>》</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2018年12月3日－<br />
2018年12月25日</p></td>
<td style="text-align: center;"><p><a href="../Page/國標舞女孩.md" title="wikilink">國標舞女孩</a><br />
</p></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/朴世婉.md" title="wikilink">朴世婉</a>、<a href="../Page/張東尹.md" title="wikilink">張東尹</a></p></td>
<td style="text-align: center;"><p><a href="../Page/朴鉉錫.md" title="wikilink">朴鉉錫</a></p></td>
<td style="text-align: center;"><p>權惠智</p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 2019年

<table style="width:343%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 90%" />
<col style="width: 12%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>導演</p></th>
<th style="text-align: center;"><p>編劇</p></th>
<th style="text-align: center;"><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>2019年1月7日－<br />
2019年3月26日</p></td>
<td style="text-align: center;"><p><a href="../Page/鄰家律師趙德浩2：罪與罰.md" title="wikilink">鄰家律師趙德浩2：罪與罰</a><br />
</p></td>
<td style="text-align: center;"><p>40</p></td>
<td style="text-align: center;"><p><a href="../Page/朴新陽.md" title="wikilink">朴新陽</a>、<a href="../Page/高賢廷.md" title="wikilink">高賢廷</a></p></td>
<td style="text-align: center;"><p><a href="../Page/韓相佑.md" title="wikilink">韓相佑</a></p></td>
<td style="text-align: center;"><p><a href="../Page/崔完圭.md" title="wikilink">崔完圭</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2019年4月1日－<br />
2019年5月28日</p></td>
<td style="text-align: center;"><p><a href="../Page/各位國民.md" title="wikilink">各位國民</a><br />
</p></td>
<td style="text-align: center;"><p>36</p></td>
<td style="text-align: center;"><p><a href="../Page/崔始源.md" title="wikilink">崔始源</a>、<a href="../Page/李宥英.md" title="wikilink">李宥英</a>、<a href="../Page/金玟廷.md" title="wikilink">金玟廷</a>、<a href="../Page/太仁鎬.md" title="wikilink">太仁鎬</a></p></td>
<td style="text-align: center;"><p>金正賢</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2019年6月3日－<br />
2019年</p></td>
<td style="text-align: center;"><p><a href="../Page/Perfume_(電視劇).md" title="wikilink">Perfume</a><br />
</p></td>
<td style="text-align: center;"><p>32</p></td>
<td style="text-align: center;"><p><a href="../Page/申成祿.md" title="wikilink">申成祿</a>、<a href="../Page/高媛熙.md" title="wikilink">高媛熙</a>、<a href="../Page/金旻奎.md" title="wikilink">金旻奎</a>、<a href="../Page/車藝蓮.md" title="wikilink">車藝蓮</a></p></td>
<td style="text-align: center;"><p>金尚輝</p></td>
<td style="text-align: center;"><p>崔賢玉</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2019年8月日－<br />
2019年</p></td>
<td style="text-align: center;"><p><a href="../Page/讓我聆聽你的歌.md" title="wikilink">讓我聆聽你的歌</a><br />
</p></td>
<td style="text-align: center;"><p>32</p></td>
<td style="text-align: center;"><p><a href="../Page/延宇振.md" title="wikilink">延宇振</a>、<a href="../Page/金世正.md" title="wikilink">金世正</a>、<a href="../Page/宋再臨.md" title="wikilink">宋再臨</a>、<a href="../Page/朴芝妍.md" title="wikilink">朴芝妍</a></p></td>
<td style="text-align: center;"><p>李貞美</p></td>
<td style="text-align: center;"><p>金敏珠</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

## 相關連結

  - [MBC月火連續劇](../Page/MBC月火連續劇.md "wikilink")
  - [SBS月火連續劇](../Page/SBS月火連續劇.md "wikilink")
  - [tvN月火連續劇](../Page/tvN月火連續劇.md "wikilink")
  - [JTBC月火連續劇](../Page/JTBC月火連續劇.md "wikilink")
  - [Channel A月火連續劇](../Page/Channel_A月火連續劇.md "wikilink")

## 外部連結

  - [韓國KBS官方網站](http://www.kbs.co.kr/)
  - [KBS 2TV節目表](https://web.archive.org/web/20070812055613/http://www.kbs.co.kr/plan_table/channel/2tv/index.html)

  - [KBS
    Global中文網站](https://archive.is/20130502030351/http://contents.kbs.co.kr/chinese/)

  - [KBS
    Global英文網站](https://archive.is/20130502013340/http://contents.kbs.co.kr/)


[Category:動態列表](../Category/動態列表.md "wikilink")
[\*](../Category/KBS月火連續劇.md "wikilink")
[KBS](../Category/韓國電視台電視劇列表.md "wikilink")
[Category:韓國電視劇播放時段](../Category/韓國電視劇播放時段.md "wikilink")