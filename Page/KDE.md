**KDE**（）是一個國際性的[自由軟體社区](../Page/自由軟體.md "wikilink")\[1\]，开发运行在[Linux](../Page/Linux.md "wikilink")、[BSD](../Page/BSD.md "wikilink")、[Solaris](../Page/Solaris.md "wikilink")、[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink") 與
[macOS](../Page/macOS.md "wikilink")\[2\]
等平台上的一系列[跨平台应用程序](../Page/跨平台.md "wikilink")。它最著名的产品是
[Plasma 桌面](../Page/KDE_Plasma_Workspaces.md "wikilink")，是許多 [Linux
發行版的預設桌面環境](../Page/Linux發行版.md "wikilink")，例如[openSUSE](../Page/openSUSE.md "wikilink")\[3\]、[Mageia](../Page/Mageia.md "wikilink")、[Linux
Mint](../Page/Linux_Mint.md "wikilink")、[Kubuntu](../Page/Kubuntu.md "wikilink")、[PCLinuxOS](../Page/PCLinuxOS.md "wikilink")
與 [Chakra GNU/Linux](../Page/Chakra_GNU/Linux.md "wikilink")。\[4\]

社群的目標是開發基本的桌面功能和日常必需的[應用程式](../Page/應用程式.md "wikilink")，以及提供開發者編寫獨立的應用程式的工具和文檔。許多獨立應用程式和規模較小的項目是基於
KDE 的技術，這些軟體包括 [Calligra
Suite](../Page/Calligra_Suite.md "wikilink")、[digiKam](../Page/digiKam.md "wikilink")、[Rekonq](../Page/Rekonq.md "wikilink")、[K3b](../Page/K3b.md "wikilink")
和許多其他應用程式。KDE 軟體是基於[Qt框架所开发](../Page/Qt.md "wikilink")。

多个国家的政府部门也使用 KDE 软件，興建位於瑞士的[大型強子對撞機也使用](../Page/大型強子對撞機.md "wikilink")
KDE 軟體。\[5\]

## 概况

KDE 社区的工作可以用以下特征来衡量：

  - KDE 是最大的自由软件社区之一。\[6\]
  - 超过1800名贡献者参与开发 KDE 软件。\[7\] 每月大约20位新开发者贡献他们的第一行代码。\[8\] On October
    11, 2009, [Cornelius
    Schumacher](../Page/Cornelius_Schumacher.md "wikilink"), a main
    developer within KDE,\[9\]
  - KDE 软件包含600多万行代码（不包括Qt）。\[10\]
  - KDE 软件被翻译成超过108种语言。\[11\]
  - KDE 软件可在超过34个国家和地区的114多个官方 FTP 镜像中使用。\[12\]
  - 在 [Github](../Page/Github.md "wikilink") 上可以找到所有储存库的唯讀映像。\[13\]

## 歷史

### 起源

[Matthias_Ettrich_LinuxTag_2005-06-23.jpg](https://zh.wikipedia.org/wiki/File:Matthias_Ettrich_LinuxTag_2005-06-23.jpg "fig:Matthias_Ettrich_LinuxTag_2005-06-23.jpg")

KDE 是由德國人 Matthias Ettrich
於1996年就讀於[蒂宾根大學開始的一個計劃](../Page/蒂宾根大學.md "wikilink")。當時，他憂心於[Unix桌面沒有一個應用程式外觀](../Page/Unix.md "wikilink")、感受或工作方式和其他程式一樣。他建議不僅是建立一套應用程序，而是一個桌面環境，用戶可以得到统一的外觀、感受和工作方式。他還希望這個桌面易於使用及更人性化；他当时对桌面应用的抱怨之一是对最终用户来说太复杂了。他在
[Usenet](../Page/Usenet.md "wikilink") 發表的文章引發了很大的迴響，意味着 KDE
项目就此誕生了。\[14\]

*KDE*這個名字是來自於現有的[Common Desktop
Environment](../Page/CDE.md "wikilink")（CDE）的文字遊戲，其用於Unix系統。CDE是一個由惠普、IBM和Sun透過[X/Open公司共同開發的基於X](../Page/X/Open.md "wikilink")11使用者環境，介面和生產工具都基於Motif圖形部件工具包。它被期望是一個直觀易於使用的桌面電腦環境。\[15\]
*K*原本代表"[Kool](../Page/wiktionary:kool.md "wikilink")"，但很快就決定K沒有什麼特別意義——因此*KDE*這個縮寫擴展為“*K
Desktop Environment”*。而这个意涵最终亦被废止，由于重塑品牌而提出“KDE=社区”的概念。

### 產品發布

[KDE_4.png](https://zh.wikipedia.org/wiki/File:KDE_4.png "fig:KDE_4.png")
Mathias
Ettrich選擇使用[Qt程式庫開發](../Page/Qt.md "wikilink")桌面系統，很快地他和其他志願開發人員於1997年初發佈了一些應用程序。\[16\]

在1998年7月12日，[KDE 1](../Page/KDE_1.md "wikilink").0發佈。之后[KDE
2發布](../Page/KDE_2.md "wikilink")，提出了重大技術改進。\[17\] [KDE
3是遠遠大於以前的系列](../Page/KDE_3.md "wikilink")，包括6個主要版本。KDE
2和KDE 3之間的API變化則較少，即KDE 3可以被看作主要是延續KDE 2。[KDE
4包含了許多新技術的變化](../Page/KDE_4.md "wikilink")。

### 品牌重新定位

2009年11月24日，KDE行銷團隊宣布正式更名KDE專案的組成部分。其動機是認為從建構桌面環境，轉向一個｢創造軟體｣社群的完整專案。更名重點在於不再強調桌面環境而「只是一個產品」，並強調社群與其他技術一起提供KDE軟體。重新定位後，KDE不再是過去的*K
Desktop Environment*縮寫，現在充當不同軟體組件的品牌。\[18\]

### 授權风波

儘管KDE作爲自由的[开放源軟體](../Page/开放源代码.md "wikilink")，但由於它使用了开放源始码但當時並非自由軟體授權的Qt程序库，有許多人在擔心日後可能會出現的版權問題。

很慶幸地是，1998年11月之後，程序库所屬的[Trolltech公司發佈了第一份自由軟體許可](../Page/Trolltech.md "wikilink")[Q
Public
License](../Page/Q_Public_License.md "wikilink")（QPL）的程序库授權。同年不久，KDE
Free
Qt基金會\[19\]保證假若Trolltech在任意連續12個月期間沒有釋出新的自由版本，那麼Qt程序將更改為基於[BSD許可證授權散佈](../Page/BSD許可證.md "wikilink")。

很多人依然爭議著認爲授權與[GPL上的條款會不相容](../Page/GNU通用公共许可证.md "wikilink")。[Red
Hat公司因此始終無法把KDE作為默認桌面](../Page/Red_Hat.md "wikilink")，而[Mandriva
Linux即趁此時機憑藉KDE襲捲了歐洲的市場](../Page/Mandriva_Linux.md "wikilink")；2000年9月，一個基於協議的版程序库成功釋出，大部份用戶方才對生出信心。\[20\]

Qt 4.5在2009年3月3日發布，遵循了[LGPL](../Page/LGPL.md "wikilink")
2.1协议，放宽了KDE函數庫的授權，使得平臺下商業私有版權程式的開發較原來自由\[21\]\[22\]。

## 社群结构与特征

[缩略图](https://zh.wikipedia.org/wiki/File:KDE_Mascot_Konqi_by_Tyson_Tan.png "fig:缩略图")

### 吉祥物与标志

KDE 社群的[吉祥物是一個名為](../Page/吉祥物.md "wikilink")
[Konqi](../Page/Konqi.md "wikilink") 的小绿龙，在KDE软件的版本对话框、各种有关印刷物、会议演示文档和众多
KDE 的相关网站上均可见到他的身影。\[23\]Konqi 有一位名为 Katie 的女朋友，代表了 KDE 女性成员。他们在 KDE 4.0
发布活动和 KDE 2010 活动中扮演他们的角色。\[24\]此外 Konqi
还有一些不同颜色，不同职业，不同随身物品和不同形状犄角的派生角色，各自对应在
KDE 社区不同方面活动的成员们。[Kandalf](../Page/Kandalf.md "wikilink")
这位巫师在1.x和2.x版本中曾是 KDE 社区的前吉祥物，但由于版权问题（他与甘道夫相似）而被抛弃。现在的
Konqi 的外观由于 Plasma 5 的推出而正式重新设计，Tyson Tan
的参赛作品（见右图）在KDE论坛上赢得重新设计比赛。\[25\]

KDE有一個community identity
guidelines（CIG）來定義和建議，幫助社群建立獨特的、有特色及吸引人的設計。\[26\]
KDE官方標誌是一個藍色方形中有白色的K與齒輪。\[27\]
KDE軟體標籤是給軟體的開發者用的。證明他們是KDE社群的一分子，或者他們使用KDE平台。有包含包含三種KDE軟體標籤。被標為*Powered
by KDE*軟體標籤代表此軟體是KDE社群的主力軟體並且源自KDE發展平台。使用KDE平台的應用程式則被選擇標示為*Built on the
KDE Platform*。應用程式標為*Part of the KDE family*則代表此應用程式作者為KDE社群中的一份子。\[28\]

Image:KDE Logo Official Lineart Detailed.svg|Line art標誌 Image:KDE
logo.svg|Oxygen標誌 Image:Klogo-official-crystal.svg|Crystal標誌

### 应用命名

許多的KDE應用程序名稱中有一個*K*字，多數是在第一個字母和大寫表示。然而，有明顯的例外如[kynaptic和](../Page/kynaptic.md "wikilink")[Amarok](../Page/Amarok.md "wikilink")（以前稱amaroK）。*K*在許多KDE應用程序來自於*C*或*Q*拼寫單詞不同，例如[Konsole和](../Page/Konsole.md "wikilink")[Kuickshow](../Page/Kuickshow.md "wikilink")。另外，一些是在一個常用詞前綴上*K*，例如[KOffice](../Page/KOffice.md "wikilink")。但是在[KDE
4應用程序和技術目前的趨勢是名字中不包含](../Page/KDE_4.md "wikilink")*K*，如[Plasma](../Page/Plasma_\(KDE\).md "wikilink")、[Phonon和](../Page/Phonon_\(KDE\).md "wikilink")[Dolphin](../Page/Dolphin_\(KDE軟體\).md "wikilink")。

### 组织与社区

KDE
的财务和法律事宜由[德国](../Page/德国.md "wikilink")[非营利组织](../Page/非营利组织.md "wikilink")
[KDE e.V.](../Page/KDE_e.V..md "wikilink")
负责处理，该组织位于德国[柏林](../Page/柏林.md "wikilink")。该组织还协助社区成员举办会议和会议。\[29\]
KDE e.V. 帮助运营 KDE
社区所需的服务器，它拥有商标“*KDE*”和相应的徽标。它还支付会议的旅行费用和补贴活动。\[30\]组建的工作组旨在将
KDE 中的某些角色形式化，并改进 KDE 内部各部分的协调与通信工作。\[31\] KDE e.V.不影响软件开发。 KDE
e.V.的徽标由提供。标志顶部的三个标志代表 KDE e.V. 的三项主要任务：支持社区、代表社区、管理社区。\[32\]

KDE 社区的开发者会议、服务器和相关活动经常由个人、大学和企业赞助。\[33\] KDE e.V.的支持成员（）是通过财务或物质捐助支持
KDE 的特别成员。\[34\]支持会员有权在其网站或印刷材料上展示“KDE会员”徽标。
KDE的赞助人（）是支持会员的最高级别，其也有权在其网站或印刷品上展示独家的“KDE赞助人”标志。\[35\]
2006年10月15日，社区宣布[马克·沙特尔沃思成为KDE的第一个赞助人](../Page/马克·沙特尔沃思.md "wikilink")。\[36\]
2007年7月7日，又宣布[英特尔公司和](../Page/英特尔.md "wikilink")
[Novell](../Page/Novell.md "wikilink") 公司也成为 KDE 的赞助商。\[37\]
2010年1月，Google成为支持会员。 2010年6月9日，KDE
e.V.推出了“加入游戏”活动。这项运动促进了成为个人支持成员的想法。它提供给那些想要支持
KDE 但没有足够的时间来这样做的人。（FSFE）的创始人首先“加入游戏”。\[38\]

在很多国家和地区，KDE 同样拥有地方分支机构。这些组织可以是非正式组织（印度），也可以是具有像 KDE e.V.
一样具有法定形式（法国）。当地社区常常主办和维护区域性网站，组织当地活动，如贸易展览、参与者会议和社区会议。

## 貢獻

和許多自由/開放源碼軟件專案一樣，KDE的開發主要是來自志願者的貢獻。儘管有各種公司，如[Novell](../Page/Novell.md "wikilink")、[諾基亞和](../Page/諾基亞.md "wikilink")[Mandriva](../Page/Mandriva.md "wikilink")，僱用員工開發KDE。由於大量的個人貢獻以各種方式（如代碼、翻譯、美工）給KDE，組織這樣一個專案是十分複雜的。

### 开发团队

KDE 平台的整体方向由*KDE核心团队*（KDE Core Team）完成。这些开发人员在 KDE
内长时间做出重大贡献。该团队使用*kde-core-devel*邮件列表进行通信，该邮件列表公开存档且可读，但加入需要批准。
KDE没有一个可以否决重要决定的中央领导人。相反，KDE的核心团队由几十名参与者作出决定。这些决定不是通过正式投票，而是通过讨论。\[39\]开发人员还与专题团队一起组织。例如，[KDE
Edu
团队开发免费教育软件](../Page/KDE教育計劃.md "wikilink")。虽然这些团队主要独立工作，并不都遵循共同的发布时间表。每个团队都在IRC和邮件列表上都有自己的消息传递渠道。他们有导师计划以帮助初学者开始。\[40\]\[41\]KDE社区目前使用[Git版本控制系统](../Page/Git.md "wikilink")。

2009年7月20日，KDE宣布已经向其Subversion版本库提交了第100万次提交。\[42\]
2009年10月11日，KDE的主要开发人员 Cornelius Schumacher \[43\]
提及了（运用了SLOCCount与[COCOMO模型](../Page/构造性成本模型.md "wikilink")）开发KDE软件包的工作量：4,273,291个LoC（代码行数），约为1,7536,4716美元。\[44\]此估算不包括Qt、Calligra
Suite、Amarok、Digikam和其他不属于KDE核心的应用程序。

## 軟體

*主条目：*

KDE 軟體是基於 [Qt](../Page/Qt.md "wikilink") 框架開發的，具備了可於多數的
[Unix](../Page/Unix.md "wikilink") 及其他類 Unix 系統下運行的能力，此外還包括有苹果平臺上的 [OS
X](../Page/OS_X.md "wikilink") 系統和[微軟的](../Page/微軟.md "wikilink")
[Windows](../Page/Windows.md "wikilink") 系統。目前使用的建構工具是
[CMake](../Page/CMake.md "wikilink")，並可以用在多個平台上。\[45\][gettext](../Page/gettext.md "wikilink")
用於作翻譯。[Doxygen](../Page/Doxygen.md "wikilink") 用來產生API文件。\[46\]

### 发行版总览

| 版本                                                 | 日期          | 信息                                                                   |
| -------------------------------------------------- | ----------- | -------------------------------------------------------------------- |
|                                                    | 1996年10月14日 | KDE宣布开发                                                              |
| 1.0                                                | 1998年7月12日  |                                                                      |
| 2.0                                                | 2000年10月23日 |                                                                      |
| 3.0                                                | 2002年4月3日   |                                                                      |
| [4.0](../Page/KDE_Plasma_Workspaces.md "wikilink") | 2008年1月11日  |                                                                      |
| [4.0后版本](../Page/KDE_Plasma_5.md "wikilink")       | 2014年7月15日  | 之前的KDE/KDE SC 被分成 KDE Plasma、 KDE Frameworks 和 KDE Applications 三个部分 |

一开始，Matthias Ettrich 就选择为 KDE 计划使用
[Trolltech](../Page/Qt公司.md "wikilink") 的 Qt
框架。\[47\]其他开发者迅速开始了KDE/Qt应用的开发。到1997年初，一些应用已经被发布。1998年7月12日，称为
KDE 1.0 的桌面环境被发布，它是KDE环境的第一个版本。

这个工具包的原始 [GPL](../Page/GNU通用公共许可证.md "wikilink") 授权版本仅适用于使用 [X11
显示服务器的平台](../Page/X_Window系統.md "wikilink")，但随着Qt
4的发布，[LGPL](../Page/GNU宽通用公共许可证.md "wikilink")
授权的版本可用于更多的平台。这使得基于 Qt 4 或更新版本的 KDE
软件理论上可以被分发到微软 Windows 和 苹果 OS X 平台。\[48\]

切换到 Qt 5 之后，原来被称为 KDE 或 KDE SC 的核心组件现由三部分组成：

  - KDE Plasma （提供不同的工作平台）
  - KDE Frameworks （基于Qt 的库，之前被称为“kdelibs”或“KDE Platform”（KDE 平台））
  - KDE Applications （建构在 KDE 平台上的相关应用）

### KDE Plasma

*主条目：[KDE Plasma 4](../Page/KDE_Plasma_Workspaces.md "wikilink") 与 [KDE
Plasma 5](../Page/KDE_Plasma_5.md "wikilink")*

KDE Plasma 提供了一个统一的桌面环境，用于运行和管理普通计算机、上网本、平板电脑及智能手机等不同形式的应用程序。\[49\]

[Plasma](../Page/KDE_Plasma_Workspaces.md "wikilink") 首先作为第四代 KDE
[图形环境](../Page/桌面环境.md "wikilink")（KDE4 SC）的一部分引入。其后继者 KDE
Plasma 5 于2014年7月15日发布，目前具有以下工作空间：

  - 在普通计算机上工作的 *Plasma Desktop*

<!-- end list -->

  - 在上网本上工作的 *Plasma Netbook*
  - 在电视和机顶盒上工作的 *Plasma Media Center*
  - 在平板电脑上工作的 *Plasma Active*\[50\]
  - 用于智能手机和普通触摸式设备的 **

### KDE Frameworks

*主条目：[KDE Platform 4](../Page/KDELibs.md "wikilink") 与 [KDE Frameworks
5](../Page/KDE_Frameworks_5.md "wikilink")*

在 KDE 4 SC 系列中，[KDE 平台](../Page/KDELibs.md "wikilink")（即 KDE
Platform）由运行 KDE 应用程序所需的库和服务组成。当转换到 Qt 5 时，KDE 平台被转换成现在被称为 [KDE
框架](../Page/:KDE_Frameworks_5.md "wikilink")（即 KDE
Frameworks）的模块化集合。所需的库包括 Solid、Nepomuk、Phonon
等，并且必须根据 LGPL 、[BSD
许可证](../Page/BSD许可证.md "wikilink")、[MIT
许可证或](../Page/MIT許可證.md "wikilink") X11 许可证进行许可。\[51\]

虽然 KDE 框架主要是用 [C++](../Page/C++.md "wikilink")
编写的，但是它包含了同其他编程语言的接合模块。\[52\]它们使用以下通用技术：

  - Smoke：用于同
    [Ruby](../Page/Ruby.md "wikilink")、[C\#](../Page/C#.md "wikilink")
    和 [PHP](../Page/PHP.md "wikilink") 接合

<!-- end list -->

  - SIP：用于同 [Python](../Page/Python.md "wikilink") 接合
  - Kross： C++ 应用程序的嵌入式脚本，支持
    Ruby、Python、[JavaScript](../Page/JavaScript.md "wikilink")、[QtScript](../Page/QtScript.md "wikilink")、[Falcon和](../Page/Falcon.md "wikilink")[Java](../Page/Java.md "wikilink")

稳定和成熟的接合模块可用于以下编程语言：\[53\]

  - Python
  - Ruby（Korundum，基于 [QtRuby](../Page/QtRuby.md "wikilink")）
  - [C\#](../Page/C#.md "wikilink")（然而目前对C\#和其他.Net语言进行接合的框架已被弃用，且其替代品仅能在Windows上编译）\[54\]
  - [Perl](../Page/Perl.md "wikilink")

### KDE Applications

[KDE 应用程序](../Page/:en:KDE_Applications.md "wikilink")（即 KDE
Applications，它们包括
[Okular](../Page/Okular.md "wikilink")、[KTorrent](../Page/KTorrent.md "wikilink")、[Kile](../Page/Kile.md "wikilink")
和 ）建立在 KDE 框架之上。 KDE
应用程序努力实现在各个操作系统之间的可移植性，并且独立于某种特定的工作空间或桌面环境。某些品牌识别的应用程序套件常常由多个应用程序构建，例如
[KDE Network](../Page/Kdenetwork.md "wikilink")、[KDE
Graphics](../Page/Kdegraphics.md "wikilink") 和 。
某些应用程序是常规软件编译版本的一部分；其他应用程序则是
Extragear 的一部分，并按照自己的时间表发布。

### KDE neon

[KDE neon](https://neon.kde.org/)是一个以Ubuntu LTS作为核心的软件仓库。
它旨在为用户提供快速更新的Qt和KDE软件，同时以正常的速度更新Ubuntu库中的其余OS组件。\[55\]\[56\]KDE
认为它并非是“KDE 发行版”，而是 KDE/Qt 软件包的最新版本。

它提供用户和开发者两种版本。

### WikiToLearn

（简称WTL）是KDE最新和最独特的努力之一。
它是一个基于[MediaWiki](../Page/MediaWiki.md "wikilink")（如维基百科）的百科，提供了一个平台来创建和共享开源教科书。这个计划的目标是创造一个人人都能使用并参与其中的庞大的教科书库。这个想法肇始于一群[米兰大学物理专业的学生](../Page/米蘭大學.md "wikilink")，他们一开始仅仅想要分享笔记。之后，他们决定分享给每个人而不仅仅是他们的朋友圈内。它现在是一个完整的KDE项目，许多大学都支持它。

## 與其他組織的合作

KDE还和各类组织、公司进行合作，如Google、维基媒体基金會等。

2005年6月23日，[维基媒体基金會宣布KDE社群和基金會開始合作](../Page/维基媒体基金會.md "wikilink")。\[57\]
这次合作的成果包括[Kate中](../Page/Kate.md "wikilink")[MediaWiki的](../Page/MediaWiki.md "wikilink")[语法高亮](../Page/语法高亮.md "wikilink")，以及从KDE程序如[Amarok和](../Page/Amarok.md "wikilink")[Marble访问維基百科的内容](../Page/Marble_\(KDE\).md "wikilink")。

[Amarok_2_3_0.png](https://zh.wikipedia.org/wiki/File:Amarok_2_3_0.png "fig:Amarok_2_3_0.png")

### 發行商

許多Linux發行版和其他自由的作業系統都參與了軟體的開發、銷售，因此它們也活跃於KDE社群。

## 參見

  - [KDE Software
    Compilation](../Page/KDE_Software_Compilation.md "wikilink")
  - [KDE程序列表](../Page/KDE程序列表.md "wikilink")
  - [KDE Platform](../Page/KDE_Platform.md "wikilink")
  - [Gnome](../Page/Gnome.md "wikilink")

## 参考文献

## 外部链接

  -
  - [KDE.news](http://dot.kde.org/)：KDE官方的新聞和公告。

  - [KDE論壇](http://forum.kde.org/)：KDE社群的官方論壇。

  - [KDE
    UserBase](http://userbase.kde.org/Welcome_to_KDE_UserBase/zh-tw)：提供使用者教學、指南和實用的小提示

  - [KDE
    UserBase](http://userbase.kde.org/Welcome_to_KDE_UserBase/zh-cn)：提供使用者教學、指南和實用的小提示

### 中文社群

  - [KDE简体中文论坛](https://forum.kde-china.org/)
  - [Planet KDE中文區](http://planetkde.org/zh/)
  - [KDE台灣官方網站](http://kde.linux.org.tw/)
  - [KDE@Taiwan正體中文新聞站](http://kdetw.blogspot.com/)
  - [kde-tw -
    Google網上論壇](https://groups.google.com/forum/?hl=zh-TW#!forum/kde-tw)
  - [KDE@Taiwan討論區](https://moto.debian.tw/viewforum.php?f=39)
  - [I, KDE——关注KDE软件集](http://www.ikde.org/)

### 參與中文翻譯

  - [KDE正體中文（臺灣）翻譯說明](https://web.archive.org/web/20080126091945/http://kde.linux.org.tw/kde-i18n/trans-po.html)
  - [KDE正體中文（臺灣）翻譯計劃](https://web.archive.org/web/20080522161209/http://kde.linux.org.tw/kde-i18n/)
  - [Tryneeds（「KDE」類別）](http://tryneeds.westart.tw/)→KDE正體中文翻譯團隊維護，[相關說明](http://moto.debian.org.tw/viewtopic.php?t=13793)
  - [KDE简体中文翻译平台](https://crowdin.com/project/kdeorg)
  - [KDE中文化的相关介绍](https://community.kde.org/KDE_Localization/zh-cn)

### 其他

  - [GNOME vs
    KDE](https://docs.google.com/document/d/15CIS4pg_MdQr7PtfgRniTCfiE76ZQdUE-F-ZddAIHLU/edit?disco=AAAAAFOVrj4/)：GNOME
    vs KDE的比較表

{{-}}

[KDE](../Category/KDE.md "wikilink")
[Category:自由軟體](../Category/自由軟體.md "wikilink")
[Category:桌面环境](../Category/桌面环境.md "wikilink")
[Category:自由軟體組織](../Category/自由軟體組織.md "wikilink")

1.

2.

3.

4.

5.

6.
7.
8.

9.

10.

11.

12.

13.

14. {{ cite newsgroup |title=New Project: Kool Desktop Environment (KDE)
    |url=<http://groups.google.com/group/de.comp.os.linux.misc/msg/cb4b2d67ffc3ffce>
    |first= Matthias |last=Ettrich |newsgroup= de.comp.os.linux.misc
    |id=53tkvv$b4j@newsserv.zdv.uni-tuebingen.de |date=14 October 1996
    |accessdate=2006-12-29}}

15.

16.

17. [KDE 2.0 Release
    Announcement](http://www.kde.org/announcements/announce-2.0.php)

18.

19.

20.

21.

22. <http://www.kde.org/announcements/4.2/index.php>

23.

24.

25.

26.

27.

28.

29.

30.

31.

32.

33.

34.

35.

36.

37.

38.

39.

40.

41.

42.

43.

44.

45.

46.

47.

48.

49.

50.

51.

52.

53.

54.

55.

56.

57.