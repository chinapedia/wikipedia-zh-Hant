**KEI**是日本男性\[1\][插畫家](../Page/插畫家.md "wikilink")、[漫畫家](../Page/漫畫家.md "wikilink")。工作主要是[輕小說插畫](../Page/輕小說.md "wikilink")、[人物設計](../Page/人物設計.md "wikilink")。[北海道](../Page/北海道.md "wikilink")[千歲市](../Page/千歲市.md "wikilink")\[2\]出身。

## 畫風

出道作是[初音未來](../Page/初音未來.md "wikilink")。在接受記者採訪時說，被要求以“[Yamaha生產的](../Page/Yamaha.md "wikilink")[電子樂器為主題](../Page/電子樂器.md "wikilink")”設計一個女性角色，音樂設備知識缺乏使他苦戰。

畫風受作家[天野喜孝影響](../Page/天野喜孝.md "wikilink")，特別關注作家[深崎暮人](../Page/深崎暮人.md "wikilink")。

## 主要作品

### 人物設定

  - [音樂軟件](../Page/音樂軟件.md "wikilink")[角色主唱系列](../Page/角色主唱系列.md "wikilink")：CV01
    、CV02 、CV03 （[CRYPTON FUTURE
    MEDIA](../Page/CRYPTON_FUTURE_MEDIA.md "wikilink")）
  - [日本偶像](../Page/日本偶像.md "wikilink")： (  )\[3\]\[4\]
  - [果酒](../Page/果酒.md "wikilink")：[梅酒](../Page/梅酒.md "wikilink")
    、[日本柚子](../Page/日本柚子.md "wikilink")[果酒](../Page/果酒.md "wikilink")
    ()\[5\]
  - [世嘉硬件女孩](../Page/世嘉硬件女孩.md "wikilink")（[世嘉](../Page/世嘉.md "wikilink")）
  - [VOCALOID](../Page/VOCALOID.md "wikilink")：[Galaco](../Page/Galaco.md "wikilink")、[Lily](../Page/Lily_\(VOCALOID\).md "wikilink")
  - [虚拟YouTuber](../Page/虚拟YouTuber.md "wikilink")：[Mirai
    Akari](../Page/Mirai_Akari.md "wikilink")

### 漫畫

  - （[COMIC
    RUSH](../Page/COMIC_RUSH.md "wikilink")2008年1月號開始連載。初音未來的漫畫化作品）

### 插畫

  - [奇蹟的表現](../Page/奇蹟的表現.md "wikilink")（[結城充孝著](../Page/結城充孝.md "wikilink")、[電撃文庫](../Page/電撃文庫.md "wikilink")）

  - [魔像怪X少女](../Page/魔像怪X少女.md "wikilink")（[大凹友數著](../Page/大凹友數.md "wikilink")、[MF文庫J](../Page/MF文庫J.md "wikilink")、[東立出版社](../Page/東立出版社.md "wikilink")）

  - [魔王的憂鬱](../Page/魔王的憂鬱.md "wikilink")（[相川直樹著](../Page/相川直樹.md "wikilink")、[新風舎文庫](../Page/新風舎文庫.md "wikilink")）

  - [魔王的復活](../Page/魔王的復活.md "wikilink")（相川直樹著、新風舎文庫）

  - （[松山剛著](../Page/松山剛.md "wikilink")、新風舎文庫）

  - （著、）

### 著作

  - （）

### 遊戲

  - [NUGA-CEL！](../Page/NUGA-CEL！.md "wikilink")（人物設定）

### 畫集

  - KEI畫廊（）ISBN 978-4861005749
  - KEI畫集
    mikucolor（[角川書店](../Page/角川書店.md "wikilink")、[天聞角川](../Page/天聞角川.md "wikilink")）ISBN
    978-4041102121

## 同人誌

<table>
<tbody>
<tr class="odd">
<td><ul>
<li>Bme:00〜01</li>
<li>capbon</li>
<li>Keidle</li>
<li>Monochrome</li>
<li>moon light</li>
<li>nanako</li>
<li>RozenBon</li>
</ul></td>
<td><ul>
<li>To the Doll Lovers.</li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
</tr>
</tbody>
</table>

## 参考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [KEI畫廊](http://www.kei-garou.net/)
  - [北乃神威計画](https://web.archive.org/web/20130531214902/http://kitano-kamui.com/)

[Category:北海道出身人物](../Category/北海道出身人物.md "wikilink")
[Category:日本插畫家](../Category/日本插畫家.md "wikilink")
[Category:日本漫畫家](../Category/日本漫畫家.md "wikilink")

1.  [KEI畫廊：VOCALOID2
    鏡音鈴、連](http://kei-garou.net/blog/archives/2007/12/vocaloid2_1.html)

2.
3.  [在小樽動漫尼克斯 “北乃神威”展覽 -
    插圖疼痛開車門(日文)](http://otaru.keizai.biz/headline/98/)
    小樽経済新聞 - 2013年06月05日 \[2013年07月18日\]
4.  [北海道出發的日本偶像“北乃神威”出租車外觀一輛賽車風/北海道(日文)](http://sapporo.keizai.biz/photoflash/2097/)　札幌経済新聞
    -2013年8月26日 \[2013年08月26日\]
5.  [熱門畫家，催生了初音未來 KEI設計 人物萌 梅子酒&日本柚子果酒
    限量發售(日文)](http://news.mynavi.jp/news/2013/07/09/134/index.html)
    [每日新聞社](../Page/每日新聞社.md "wikilink") - 2013年07月09日 \[2013年07月18日\]