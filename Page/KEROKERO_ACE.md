**KEROKERO
ACE**（）是[日本](../Page/日本.md "wikilink")[角川書店於](../Page/角川書店.md "wikilink")2007年10月26日創刊的漫畫雜誌並兼具[特攝](../Page/特攝.md "wikilink")[模型](../Page/模型.md "wikilink")[電玩與](../Page/電玩.md "wikilink")[動畫情報](../Page/動畫.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")，在電視廣告時，曾經請來聲優[池田秀一](../Page/池田秀一.md "wikilink")（成名角色：[夏亞](../Page/夏亞.md "wikilink")）與[渡邊久美子](../Page/渡邊久美子.md "wikilink")（成名角色：[KERORO軍曹](../Page/KERORO軍曹.md "wikilink")）擔任[旁白](../Page/旁白.md "wikilink")。

## 連載作品

### あ行

  - **あっぱれ\!との丸くん**
  - アングラーヒーロー（尾頭しいた）
  - **うんP先生**（[大和田秀樹](../Page/大和田秀樹.md "wikilink")）

### か行

  - ガルルレロ（後藤ゆうた）
  - [カンフーくん](../Page/カンフーくん.md "wikilink")（[モリモト](../Page/モリモト.md "wikilink")）
  - **ガンプラエクストリーム**（[岩本佳浩](../Page/岩本佳浩.md "wikilink")）
  - '''[機動戰士鋼彈 00](../Page/機動戰士鋼彈_00.md "wikilink") '''（作画：大森倖三）
  - **くるくる☆ちょボット**（泉原れな）
  - **ケロケロおもちゃ研究所**（[新居さとし](../Page/新居さとし.md "wikilink")、カラーで記事扱い。モノクロで漫画扱いの**スペシャル**も不定期掲載）
  - **[Keroro軍曹](../Page/Keroro軍曹.md "wikilink")**（[吉崎観音](../Page/吉崎観音.md "wikilink")、[少年エース掲載分の再録](../Page/月刊少年エース.md "wikilink")）
  - [Keroro軍曹
    特別訓練☆戦国ラン星（スター）大バトル\!](../Page/Keroro軍曹#ケロロ軍曹_特別訓練☆戦国ラン星大バトル!.md "wikilink")（夢唄、原作：吉崎観音）
  - **ケロロ軍曹 特別訓練☆大コウカイ星の秘宝\!**（大槻朱留、原作：吉崎観音）

### さ行

  - **[スレイヤーズ ライト☆マジック](../Page/スレイヤーズ.md "wikilink")**
  - **[セットイン\!データップ\!\!](../Page/データップSDガンダム.md "wikilink")**（すぎたにコージ）
  - [ソウルキャリバーレジェンズ](../Page/ソウルキャリバーレジェンズ.md "wikilink")（作画：小枕チヨリ）
  - **[それいけ\!桃太郎電鉄](../Page/桃太郎電鉄シリーズ.md "wikilink")**（脚本：浜崎達也、作画：高内優向）
  - **ゾンビの神様**（きんこうじたま）

### た行

  - **[大怪獣バトル
    ウルトラアドベンチャー](../Page/大怪獣バトルウルトラアドベンチャー.md "wikilink")**（脚本：[荒木憲一](../Page/荒木憲一.md "wikilink")、作画：[西川伸司](../Page/西川伸司.md "wikilink")）
  - 大怪獣バトル ウルトラアドベンチャー**NEO**（脚本：荒木憲一、作画：西川伸司）
  - [宝島Z バラ色の真珠](../Page/宝島Z.md "wikilink")（作画：マイケル・原腸）
  - 超世紀α（小笠原智史、原案：[吉崎觀音](../Page/吉崎觀音.md "wikilink")）
  - [.hack//LINK](../Page/.hack.md "wikilink")（原作：[サイバーコネクトツー](../Page/サイバーコネクトツー.md "wikilink")、作画：喜久屋めがね）
  - [ドラゴンテイマー サウンドスピリットF](../Page/ドラゴンテイマー.md "wikilink")（あつし）
  - [變形金剛進化版](../Page/變形金剛進化版.md "wikilink")：The Cool
    (NAOTO、目次用[津島直人的名義](../Page/津島直人.md "wikilink"))

### な行

  - **[なるほどことわざガンダムさん](../Page/機動戦士ガンダムさん.md "wikilink")**（大和田秀樹）
  - **[光速大冒險PIPOPA](../Page/光速大冒險PIPOPA.md "wikilink")**（作画：鈴木小波）
  - **ののの の みどりちゃん**

### は行

  - **爆釣伝説アングラーヒーロー**（[田守涉](../Page/田守涉.md "wikilink")）
  - [幕末異聞録 コードギアス
    反逆のルルーシュ](../Page/コードギアス_\(漫画\)#幕末異聞録_コードギアス_反逆のルルーシュ.md "wikilink")（シナリオ：村松由二郎、作画：銃爺）
  - '''[Battle Spirits
    少年突破馬神](../Page/Battle_Spirits_少年突破馬神.md "wikilink")
    '''（作画：[藤異秀明](../Page/藤異秀明.md "wikilink")、原作：[サンライズ](../Page/サンライズ_\(アニメ制作会社\).md "wikilink")）
  - '''[BB戰士三國傳 英雄激突編](../Page/BB戰士三國傳_英雄激突編.md "wikilink")
    '''（[矢野健太郎](../Page/矢野健太郎_\(漫画家\).md "wikilink")）
  - **[ぷちえう゛ぁ　ぼくら探検同好会](../Page/ぷちえう゛ぁ.md "wikilink")**（作画：おおぞらまき）

### ま行

  - **魔界王子プリマオ**
  - [無限邊疆 超級機器人大戰OG
    SAGA](../Page/無限邊疆_超級機器人大戰OG_SAGA.md "wikilink")（作画：斉藤和衛）
  - [メイプルストーリー](../Page/メイプルストーリー.md "wikilink")（岩田鷹吉）

### ら行

  - **LEGENTAIL千年太**（[森野達弥](../Page/日本の漫画家_ま行#森野達弥.md "wikilink")）

[\*](../Category/KEROKERO_ACE.md "wikilink")
[Category:角川書店的漫畫雜誌](../Category/角川書店的漫畫雜誌.md "wikilink")
[Ace](../Category/日本漫畫雜誌.md "wikilink")
[Category:少年漫畫雜誌](../Category/少年漫畫雜誌.md "wikilink")
[Category:月刊漫畫雜誌](../Category/月刊漫畫雜誌.md "wikilink")
[漫畫](../Category/Keroro軍曹.md "wikilink")
[-](../Category/月刊Newtype.md "wikilink")
[\*](../Category/GUNDAM_ACE.md "wikilink")
[Category:2007年創辦的雜誌](../Category/2007年創辦的雜誌.md "wikilink")
[Category:2013年停刊的雜誌](../Category/2013年停刊的雜誌.md "wikilink")
[Category:2007年日本建立](../Category/2007年日本建立.md "wikilink")
[Category:2013年日本廢除](../Category/2013年日本廢除.md "wikilink")