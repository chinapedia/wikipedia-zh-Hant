**KISS原则**是英语
的[首字母缩略字](../Page/首字母缩略字.md "wikilink")，是一種[歸納過的](../Page/歸納.md "wikilink")[經驗原則](../Page/經驗.md "wikilink")。KISS
原则是指在设计当中應當**注重簡約**的原则。總結工程専業人員在設計過程中的經驗，大多數系統的設計應保持簡潔和單純，而不摻入非必要的複雜性，這樣的系統運作成效會取得最優；因此簡單性應該是設計中的關鍵目標，尽量迴避免不必要的複雜性。同時這原則亦有應用在商業書信\[1\]、設計[電腦](../Page/電腦.md "wikilink")[軟件](../Page/軟件.md "wikilink")、[動畫](../Page/動畫.md "wikilink")、[工程上](../Page/工程.md "wikilink")。

## 词源

這個首字母縮略詞根據報導，是由洛克希德公司的首席工程師凱利約翰遜（U-2 和 SR-71
黑鳥飛機等的設計者）所創造的。雖然長久以來，它一直是被寫為“保持簡潔，愚蠢”，但約翰遜將其轉化成“保持簡單愚蠢”（無逗號），而且這種寫法仍然被許多作者使用。
詞句中最後的 **S**並沒有任何隱涵工程師是愚蠢的含義，而是恰好相反的要求設計是易使人理解的。

說明這個原則最好的實例，是約翰遜向一群設計噴射引擎飛機工程師提供了一些工具，他們所設計的機具，必須可由一名普通機械師只用這些工具修理。
因此，“愚蠢”是指被設計的物品在損壞與修復的關聯之間，它們的難易程度。這個縮寫詞已被美國軍方，以及軟件開發領域的許多人所使用。

另外相類似的概念也可作 KISS原則的起源。例如“奧卡姆剃刀”，愛因斯坦的“一切盡可能簡單”、達芬奇的“簡單是最終的複雜性”
、安德魯·聖艾修伯里的“完美不是當它不能再添加時，它似乎是在它不能被進一步刮除時實現的”。

## 詞條變種

原文當中有很多其他版本，包括：

  - Keep It Simple &
    Stupid（在[西歐](../Page/西歐.md "wikilink")[文學中最常使用](../Page/文學.md "wikilink")）
  - Keep It Sweet & Simple
  - Keep It Short & Simple
  - Keep it Simple, Sweetheart
  - Keep it Simple, Sherlock

## 在電影動畫中

## 在軟件開發中

## 參考文献

[Category:设计](../Category/设计.md "wikilink")
[Category:軟件開發哲學](../Category/軟件開發哲學.md "wikilink")

1.