**KI克拉克斯维克**（**KÍ
Klaksvík**）是[法罗群岛的一支足球俱乐部](../Page/法罗群岛.md "wikilink")，球队位于岛上的第二大城市[克拉克斯维克](../Page/克拉克斯维克.md "wikilink")，创建于1904年。KI克拉克斯维克在1992年时球队第一次出现在欧洲赛场上，在[欧洲冠军联赛资格赛中两回合](../Page/欧洲冠军联赛.md "wikilink")1-6输给了拉脱维亚的[里加斯孔托](../Page/里加斯孔托足球俱乐部.md "wikilink")。

俱乐部的球衣色是蓝色和白色。主体育场为可容纳3000人的[KÍ体育场](../Page/KÍ体育场.md "wikilink")。

## 球队荣誉

  - **[法羅群島足球超級聯賽](../Page/法羅群島足球超級聯賽.md "wikilink")**
      - *冠军 (16):* 1942, 1952, 1953, 1954, 1956, 1957, 1958, 1961, 1966,
        1967, 1968, 1969, 1970, 1972, 1991, 1999
  - **[法羅群島盃](../Page/法羅群島盃.md "wikilink")**
      - *冠军 (6):* 1966, 1967, 1990, 1994, 1999, 2016
      - *亚军 (9):* 1955, 1957, 1973, 1976, 1979, 1992, 1998, 2001, 2006

## 歐洲賽紀錄

截止2007年KI克拉克斯维克7次出線參加歐洲賽事

  - **1992/93**
      - 歐聯外圍賽：KI Klaksvik 1-3 [Skonto
        Riga](../Page/Skonto_Riga.md "wikilink") (Lat) | [Skonto
        Riga](../Page/Skonto_Riga.md "wikilink") 3-0 KI Klaksvik | (Agg:
        1-6)
  - **1995/96**
      - 歐洲盃賽冠軍盃外圍賽：[Maccabi Haifa](../Page/Maccabi_Haifa.md "wikilink")
        (Isr) 4-0 KI Klaksvik | KI Klaksvik 3-2 [Maccabi
        Haifa](../Page/Maccabi_Haifa.md "wikilink") | (Agg: 3-6)
  - **1997/98**
      - 歐洲足協盃第一圈外圍賽：[Ujpest TE](../Page/Ujpest_TE.md "wikilink") (Hun)
        6-0 KI Klaksvik | KI Klaksvik 2-3 [Ujpest
        TE](../Page/Ujpest_TE.md "wikilink") | (Agg: 2-9)
  - **1999/00**
      - 歐洲足協盃外圍賽：KI Klaksvik 0-5 [Grazer
        AK](../Page/Grazer_AK.md "wikilink") (Aut) | [Grazer
        AK](../Page/Grazer_AK.md "wikilink") 4-0 KI Klaksvik | (Agg:
        0-9)
  - **2000/01**
      - 歐聯第一圈外圍賽：KI Klaksvik 0-3 [Red Star
        Belgrade](../Page/Red_Star_Belgrade.md "wikilink") (Srb) | [Red
        Star Belgrade](../Page/Red_Star_Belgrade.md "wikilink") 2-0 KI
        Klaksvik | (Agg: 0-5)
  - **2002/03**
      - 歐洲足協盃外圍賽：KI Klaksvik 2-2 [Ujpest
        TE](../Page/Ujpest_TE.md "wikilink") (Hun) | [Ujpest
        TE](../Page/Ujpest_TE.md "wikilink") 1-0 KI Klaksvik | (Agg:
        2-3)
  - **2003/04**
      - 歐洲足協盃外圍賽：[Molde FK](../Page/Molde_FK.md "wikilink") (Nor) 2-0 KI
        Klaksvik | KI Klaksvik 0-4 [Molde
        FK](../Page/Molde_FK.md "wikilink") | (Agg: 0-6)

## 外部链接

  - [Homepage](https://web.archive.org/web/20070311093113/http://www.ki-klaksvik.fo/)

[Category:法罗群岛足球俱乐部](../Category/法罗群岛足球俱乐部.md "wikilink")