**KKCity網路城邦**是由[台灣的](../Page/台灣.md "wikilink")[願境網訊股份有限公司經營的電子佈告欄系統](../Page/願境網訊股份有限公司.md "wikilink")（[BBS](../Page/BBS.md "wikilink")）網站。系統是基於[Telnet通訊協定所建立的](../Page/Telnet.md "wikilink")，所以只有純文字加上文字超連結的功能，文字編碼為[Big5碼](../Page/Big5碼.md "wikilink")，圖片則以網址連結到WWW的空間才能看到。[2012年](../Page/2012.md "wikilink")10月，telnet://bbs.kkcity.com.tw
已無法連上。

## 概要

KKCity的系統特色在於除了主站之外，也供網友在主站架構下創立BBS分站，因此KKCity不僅主站有各式各樣的討論板，不同主題的各個分站亦有符合分站取向的各種討論板，而各分站享有高度的自治權，一般而言主站營運者並不過問分站的管理事務，也因此有許多學校班級、團體也在KKCity架設「自己的」BBS站，而每個分站與主站的使用者名單則是共用的。

KKCity與其他架設在[學術網路](../Page/學術網路.md "wikilink")（TANet）的BBS站的另一個不同之處，在於可供使用者自行選擇成人模式和[同志模式](../Page/同性戀.md "wikilink")，其中成人模式必須輸入[國民身分證統一編號](../Page/中華民國國民身分證.md "wikilink")。

由於KKCity的歷史悠久，在主站以外另有許多五花八門的專門討論版（程式語言、遊戲專版、生活用品等），又因為BBS系統本身具有一定的隱密性（KKCity允許秘密站與秘密板的存在等），所以常常會有一些私底下的情報交流。其中關於討論[羅莉的專版](../Page/羅莉.md "wikilink")，因為談論內容不合社會風俗被報導並受到[終止童妓協會的關切](../Page/終止童妓協會.md "wikilink")\[1\]。

利用KKCity的使用者最常使用的瀏覽器是願境網訊所開發，在2001年推出的[KKman](../Page/KKman.md "wikilink")\[2\]。該軟體的特色與成功之處在於將WWW網際網路與Telnet
BBS結合，以工作列中的一格位置同時開啟BBS軟體和WWW軟體，以簡化工作列。而後來台灣人洪任諭開發出[PCMan](../Page/PCMan.md "wikilink")
BBS連線軟體，以及Firefox用的BBS模組的出現，使得KKman不再是WWW+BBS的唯一選擇。

KKCity於2002年1月1日正式招收VIP加值服務會員，VIP會員享有在尖峰時間優先登入的權利，也享有其它的加值服務。如不需連署即能開設個人板（可設成隱板）、隱藏個人發文IP等等..。

2010年8月底，站方宣布因為[母公司認為BBS站沒有提供預期的收益](../Page/願境網訊股份有限公司.md "wikilink")，因此決定自2011年1月1日起無限期將KKCity改為唯讀狀態，而稍早站方則提供備份方式因應。

[2012年](../Page/2012.md "wikilink")10月，已無法連上。

## 参考資料

## 參見

  - [BBS列表](../Page/BBS列表.md "wikilink")
  - [KKman](../Page/KKman.md "wikilink")
  - [KKBOX](../Page/KKBOX.md "wikilink")
  - [KKCity使用者](../Category/KKCity_使用者.md "wikilink")

## 外部連結

  - [KKCity BBS](telnet://bbs.kkcity.com.tw)（telnet）已無法連上

  - [KKCity](https://archive.is/20000408185115/http://www.kkcity.com.tw/)已停止服務

  -
  - [KKCity公告資訊](http://kkcitybbs.blogspot.com)（[Blogger](../Page/Blogger.md "wikilink")）

  - [KKBlog（KK部落閣）](http://blog.kkcity.com/) 已停止服務

  - [KKMan](http://www.kkman.com.tw/)

  - [KKBOX](http://www.kkbox.com.tw/)

[Category:臺灣BBS](../Category/臺灣BBS.md "wikilink")

1.  [國內赫見羅莉控戀童癖討論區 2004.12.21 中國時報
    林倖妃/台北報導](http://intermargins.net/repression/deviant/Age/AgeLove/pedophilia/news/2004Jul-Dec/20041228a.htm)
2.  [e天下網站-KKman: 64萬網友的online不夜城](http://www.techvantage.com.tw/content/032/032112.asp)