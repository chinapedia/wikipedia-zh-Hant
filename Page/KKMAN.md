**KKMAN**由[願境網訊於](../Page/願境網訊.md "wikilink")2000年1月推出，是第一個結合[WWW及](../Page/WWW.md "wikilink")[Telnet分頁瀏覽的綜合瀏覽器](../Page/Telnet.md "wikilink")。使用者在KKMAN下可以很方便地在一個視窗中瀏覽多個網頁及[BBS站台](../Page/BBS.md "wikilink")。

KKMAN目前僅支援[Windows作業系統](../Page/Windows作業系統.md "wikilink")，[網頁瀏覽器部分是嵌入](../Page/網頁瀏覽器.md "wikilink")[微軟公司的](../Page/微軟.md "wikilink")[Internet
Explorer核心](../Page/Internet_Explorer.md "wikilink")。

## 軟體特性

  - 首創結合[WWW和](../Page/WWW.md "wikilink")[Telnet分頁瀏覽](../Page/Telnet.md "wikilink")。
  - 首創bbs://的[URL表示法](../Page/URL.md "wikilink")（但也因此引發爭議，參閱下文）
  - [Telnet下的](../Page/Telnet.md "wikilink")[BBS設置](../Page/BBS.md "wikilink")：
      - [ANSI彩色編輯器](../Page/ANSI.md "wikilink")
      - 表情符號快捷鍵
      - 防閒置
      - 自動偵測[URL成為](../Page/統一資源定位符.md "wikilink")[超連結](../Page/超連結.md "wikilink")，讓使用者可以用滑鼠操作[Telnet介面](../Page/Telnet.md "wikilink")。
      - 支援[SSH1](../Page/SSH.md "wikilink")。

## 批評

1.  早期的版本對於[Flash的支援較差](../Page/Flash.md "wikilink")，現已有所修正。
2.  為[廣告軟體](../Page/廣告軟體.md "wikilink")，瀏覽器右上角會出現廣告（多為歌手和新專輯訊息廣告）。
3.  KKMAN使用的「bbs://」[URL表示法不符合](../Page/URL.md "wikilink")[RFC規範](../Page/RFC.md "wikilink")。而目前多數BBS站台使用[telnet協定](../Page/telnet.md "wikilink")，遵循RFC的合理[URL表示法應為](../Page/URL.md "wikilink")「telnet://」；2009年4月更新的3.2Beta版本已修改為「telnet://」的形式，使用「bbs://」亦會自動轉換。
4.  瀏覽器預設無支援日文和簡體中文等字形的顯示。

## 外部連結

  - [2010KKMAN新功能：儲存備份BBS文章](http://ptt-kkman-pcman.org/kkman-ptt-trick01.html)
  - [KKMAN軟體下載KKMAN操作介紹與使用KKMAN開始登入PTT
    BBS站](http://ibb.com.tw/kkman-ptt-bbs.html)
  - [有關bbs://](http://groups.google.com.tw/group/tw.bbs.config/browse_thread/thread/8869a0fb9b702e36/a7c3dff68a29f48e?hl=zh-TW)
  - [Re：有沒有人知道KKCITY的BBS要怎麼聯？](http://groups.google.com.tw/group/tw.bbs.comp.hacker/msg/fb043d3a3b3c7688?hl=zh-TW)
  - [KKman](http://www.kkman.com.tw/)
  - [Toget軟體下載：KKman全方位的上網工具](http://toget.pchome.com.tw/intro/network_www/network_www_browser/5170.html)

[Category:網頁瀏覽器](../Category/網頁瀏覽器.md "wikilink")
[Category:免費軟件](../Category/免費軟件.md "wikilink")
[Category:廣告軟件](../Category/廣告軟件.md "wikilink")