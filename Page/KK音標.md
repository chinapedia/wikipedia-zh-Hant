**KK音標**（），是一種[美式英語發音的](../Page/美式英語.md "wikilink")[音素](../Page/音素.md "wikilink")[音標](../Page/音標.md "wikilink")，它所使用的符號幾乎與[國際音標](../Page/國際音標.md "wikilink")（IPA）相符。KK音標最初使用在1944年出版的《美式英語發音辭典》（*[A
Pronouncing Dictionary of American
English](http://www.archive.org/details/pronouncingdicti00unse)*），由於兩位作者及[托馬斯·A·克諾特](../Page/托馬斯·A·克諾特.md "wikilink")（）的姓皆以K為開頭，所以此辭典俗稱為KK（Kenyon
and Knott）。KK音標

## 歷史背景

### KK音標在台灣

[梁實秋](../Page/梁實秋.md "wikilink")（1903年－1987年），在1949-1969年服務於[臺灣省立師範學院及其改制的](../Page/臺灣省立師範學院.md "wikilink")[國立臺灣師範大學](../Page/國立臺灣師範大學.md "wikilink")，任英語系專任教授兼系主任；在師大服務期間，主編大學英文教科書，並與[遠東書局合作](../Page/遠東書局.md "wikilink")，出版[遠東版初中及高中英文課本](../Page/遠東版初中及高中英文課本.md "wikilink")、[遠東版英漢詞典](../Page/遠東版英漢詞典.md "wikilink")（[最新實用英漢辭典](../Page/最新實用英漢辭典.md "wikilink")、[最新實用英漢辭典增訂本](../Page/最新實用英漢辭典增訂本.md "wikilink")、[遠東英漢大辭典等](../Page/遠東英漢大辭典.md "wikilink")）。當時中國大陸、香港等地採用的是[英式發音的](../Page/英式英語.md "wikilink")[DJ音標](../Page/DJ音標.md "wikilink")，但在台灣主要接觸到的是[美式英語](../Page/美式英語.md "wikilink")，因此梁實秋在編輯英文教材與參考書時，便引進美式發音的KK音標，此舉對台灣的英語文教育影響深遠\[1\]。至今KK音標仍然是台灣學生學習英語的主要注音方式，除了台灣出版的教科書與英漢辭典以外，仍經常使用於[電子辭典中](../Page/電子辭典.md "wikilink")。

## 比較

以下是KK音標、DJ音標及IPA音標的比較。

### [母(元)音](../Page/母音.md "wikilink")

<table>
<tbody>
<tr class="odd">
<td><table>
<caption><a href="../Page/單元音.md" title="wikilink">單元音</a></caption>
<thead>
<tr class="header">
<th><p>KK</p></th>
<th><p>DJ</p></th>
<th><p>IPA</p></th>
<th><p>單字</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>[i]</p></td>
<td><p>[iː]</p></td>
<td></td>
<td><p>s<strong><u>ea</u></strong>t</p></td>
</tr>
<tr class="even">
<td><p>[ɪ]</p></td>
<td><p>[ɪ]</p></td>
<td></td>
<td><p>s<strong><u>i</u></strong>t</p></td>
</tr>
<tr class="odd">
<td><p>[e]</p></td>
<td><p>[eɪ]</p></td>
<td></td>
<td><p>p<strong><u>ai</u></strong>n</p></td>
</tr>
<tr class="even">
<td><p>[ɛ]</p></td>
<td><p>[e]</p></td>
<td></td>
<td><p>h<strong><u>ea</u></strong>d</p></td>
</tr>
<tr class="odd">
<td><p>[æ]</p></td>
<td><p>[æ]</p></td>
<td></td>
<td><p>f<strong><u>a</u></strong>t</p></td>
</tr>
<tr class="even">
<td><p>[ɑ]</p></td>
<td><p>[ɑː]</p></td>
<td></td>
<td><p>h<strong><u>o</u></strong>t</p></td>
</tr>
<tr class="odd">
<td><p>[o]</p></td>
<td><p>[əʊ]</p></td>
<td></td>
<td><p>n<strong><u>o</u></strong>se</p></td>
</tr>
<tr class="even">
<td><p>[ɑ]</p></td>
<td><p>[ɔ]</p></td>
<td></td>
<td><p>d<strong><u>o</u></strong>g</p></td>
</tr>
<tr class="odd">
<td><p>[ɔ]</p></td>
<td><p>[ɔː]</p></td>
<td></td>
<td><p>f<strong><u>ou</u></strong>r</p></td>
</tr>
<tr class="even">
<td><p>[u]</p></td>
<td><p>[uː]</p></td>
<td></td>
<td><p>t<strong><u>oo</u></strong></p></td>
</tr>
<tr class="odd">
<td><p>[ᴜ]</p></td>
<td><p>[ʊ]</p></td>
<td></td>
<td><p>p<strong><u>u</u></strong>t</p></td>
</tr>
<tr class="even">
<td><p>[ʌ]</p></td>
<td><p>[ʌ]</p></td>
<td></td>
<td><p>s<strong><u>u</u></strong>n</p></td>
</tr>
<tr class="odd">
<td><p>[ə]</p></td>
<td><p>[ə]</p></td>
<td></td>
<td><p><strong><u>a</u></strong>gain</p></td>
</tr>
<tr class="even">
<td><p>[ɪr]</p></td>
<td><p>[ɪə]</p></td>
<td></td>
<td><p>r<strong><u>ear</u></strong></p></td>
</tr>
<tr class="odd">
<td><p>[ɝ]</p></td>
<td><p>[ɜː]</p></td>
<td><p>[ər]</p></td>
<td><p>b<strong><u>ir</u></strong>d</p></td>
</tr>
</tbody>
</table></td>
<td><table>
<caption><a href="../Page/雙元音.md" title="wikilink">雙元音</a></caption>
<thead>
<tr class="header">
<th><p>KK</p></th>
<th><p>DJ</p></th>
<th><p>IPA</p></th>
<th><p>單字</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>[aɪ]</p></td>
<td><p>[aɪ]</p></td>
<td></td>
<td><p>p<strong><u>ie</u></strong></p></td>
</tr>
<tr class="even">
<td><p>[aᴜ]</p></td>
<td><p>[aʊ]</p></td>
<td></td>
<td><p>h<strong><u>ou</u></strong>se</p></td>
</tr>
<tr class="odd">
<td><p>[ɔɪ]</p></td>
<td><p>[ɔɪ]</p></td>
<td></td>
<td><p>c<strong><u>oi</u></strong>n</p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

### [子音](../Page/子音.md "wikilink")

<table>
<tbody>
<tr class="odd">
<td><table>
<caption><a href="../Page/清音.md" title="wikilink">清音</a></caption>
<thead>
<tr class="header">
<th><p>KK</p></th>
<th><p>DJ</p></th>
<th><p>IPA</p></th>
<th><p>單字</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>[p]</p></td>
<td><p>[p]</p></td>
<td></td>
<td><p><strong><u>p</u></strong>et</p></td>
</tr>
<tr class="even">
<td><p>[t]</p></td>
<td><p>[t]</p></td>
<td></td>
<td><p><strong><u>t</u></strong>en</p></td>
</tr>
<tr class="odd">
<td><p>[k]</p></td>
<td><p>[k]</p></td>
<td></td>
<td><p><strong><u>k</u></strong>ey</p></td>
</tr>
<tr class="even">
<td><p>[f]</p></td>
<td><p>[f]</p></td>
<td></td>
<td><p><strong><u>f</u></strong>at</p></td>
</tr>
<tr class="odd">
<td><p>[s]</p></td>
<td><p>[s]</p></td>
<td></td>
<td><p><strong><u>s</u></strong>ing</p></td>
</tr>
<tr class="even">
<td><p>[θ]</p></td>
<td><p>[θ]</p></td>
<td></td>
<td><p><strong><u>th</u></strong>ank</p></td>
</tr>
<tr class="odd">
<td><p>[ʃ]</p></td>
<td><p>[ʃ]</p></td>
<td></td>
<td><p><strong><u>sh</u></strong>ort</p></td>
</tr>
<tr class="even">
<td><p>[tʃ]</p></td>
<td><p>[tʃ]</p></td>
<td></td>
<td><p><strong><u>ch</u></strong>air</p></td>
</tr>
<tr class="odd">
<td><p>[h]</p></td>
<td><p>[h]</p></td>
<td></td>
<td><p><strong><u>h</u></strong>at</p></td>
</tr>
</tbody>
</table></td>
<td><table>
<caption><a href="../Page/濁音.md" title="wikilink">濁音</a></caption>
<thead>
<tr class="header">
<th><p>KK</p></th>
<th><p>DJ</p></th>
<th><p>IPA</p></th>
<th><p>單字</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>[b]</p></td>
<td><p>[b]</p></td>
<td></td>
<td><p><strong><u>b</u></strong>ook</p></td>
</tr>
<tr class="even">
<td><p>[d]</p></td>
<td><p>[d]</p></td>
<td></td>
<td><p><strong><u>d</u></strong>esk</p></td>
</tr>
<tr class="odd">
<td><p>[<span style="font-family:Georgia;">g</span>]</p></td>
<td><p>[<span style="font-family:Georgia;">g</span>]</p></td>
<td></td>
<td><p><strong><u>g</u></strong>et</p></td>
</tr>
<tr class="even">
<td><p>[v]</p></td>
<td><p>[v]</p></td>
<td></td>
<td><p><strong><u>v</u></strong>est</p></td>
</tr>
<tr class="odd">
<td><p>[z]</p></td>
<td><p>[z]</p></td>
<td></td>
<td><p><strong><u>z</u></strong>oo</p></td>
</tr>
<tr class="even">
<td><p>[ð]</p></td>
<td><p>[ð]</p></td>
<td></td>
<td><p><strong><u>th</u></strong>is</p></td>
</tr>
<tr class="odd">
<td><p>[ʒ]</p></td>
<td><p>[ʒ]</p></td>
<td></td>
<td><p>mea<strong><u>s</u></strong>ure</p></td>
</tr>
<tr class="even">
<td><p>[dʒ]</p></td>
<td><p>[dʒ]</p></td>
<td></td>
<td><p><strong><u>j</u></strong>ohn</p></td>
</tr>
<tr class="odd">
<td><p>[m]</p></td>
<td><p>[m]</p></td>
<td></td>
<td><p><strong><u>m</u></strong>om</p></td>
</tr>
<tr class="even">
<td><p>[n]</p></td>
<td><p>[n]</p></td>
<td></td>
<td><p><strong><u>n</u></strong>ose</p></td>
</tr>
<tr class="odd">
<td><p>[ŋ]</p></td>
<td><p>[ŋ]</p></td>
<td></td>
<td><p>si<strong><u>ng</u></strong></p></td>
</tr>
<tr class="even">
<td><p>[l]</p></td>
<td><p>[l]</p></td>
<td></td>
<td><p><strong><u>l</u></strong>ong</p></td>
</tr>
<tr class="odd">
<td><p>[r]</p></td>
<td><p>[r]</p></td>
<td></td>
<td><p><strong><u>r</u></strong>ed</p></td>
</tr>
<tr class="even">
<td><p>[j]</p></td>
<td><p>[j]</p></td>
<td></td>
<td><p><strong><u>y</u></strong>es</p></td>
</tr>
<tr class="odd">
<td><p>[w]</p></td>
<td><p>[w]</p></td>
<td></td>
<td><p><strong><u>w</u></strong>e</p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

\[2\]

## 與國際音標之異同

KK音標是[寬式音標](../Page/寬式音標.md "wikilink")，"toe"的長*o*[雙元音只以一個](../Page/雙元音.md "wikilink")\[o\]表示，而不是，"may"的長*e*[雙元音只以一個](../Page/雙元音.md "wikilink")\[e\]表示，而不是。

KK音標和IPA有一些違背之處，大多是為了印刷方便：

  - 符號（縮小的大寫U）取代IPA的，這是*foot*的元音和*mouth*的雙元音的後半部分。
  - 「迴尾g」[Looptail_g.svg](https://zh.wikipedia.org/wiki/File:Looptail_g.svg "fig:Looptail_g.svg")取代IPA的「開尾g」[Opentail_g.svg](https://zh.wikipedia.org/wiki/File:Opentail_g.svg "fig:Opentail_g.svg")。
  - 符號取代IPA的，這表示的是美式英語的[齒齦近音](../Page/齒齦近音.md "wikilink")。
  - [重音與](../Page/重音.md "wikilink")[次重音記號稍微向中間傾斜](../Page/次重音記號.md "wikilink")，不是垂直的。也就是說，寫成像<sup>\\</sup>和<sub>/</sub>，而不是<sup>|</sup>和<sub>|</sub>。
  - 冒號\[:\]取代IPA的長音記號，但KK通常不標出長音。
  - 外來語中用加橫槓的g（）代替IPA的，即[濁軟顎擦音](../Page/濁軟顎擦音.md "wikilink")。

## 參考文献

## 外部連結

  - [1949 年版 A Pronouncing Dictionary of American English
    下載](http://www.archive.org/details/pronouncingdicti00unse)

  -
## 參見

  - [音標](../Page/音標.md "wikilink")
      - [國際音標](../Page/國際音標.md "wikilink")
      - [DJ音標](../Page/DJ音標.md "wikilink")
      - [韋氏音標](../Page/韋氏音標.md "wikilink")

{{-}}

[Category:英语音标](../Category/英语音标.md "wikilink")

1.
2.