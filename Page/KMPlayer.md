**KMPlayer**（[Konqueror](../Page/Konqueror.md "wikilink") Media
Player），是一款在[Linux](../Page/Linux.md "wikilink")／[UNIX操作系统中运行的视频播放器](../Page/UNIX.md "wikilink")，使用[Mplayer](../Page/Mplayer.md "wikilink")、[xine和](../Page/xine.md "wikilink")[GStreamer作为解码后端](../Page/GStreamer.md "wikilink")，为[KDE的组件之一](../Page/KDE.md "wikilink")。

## 功能

KMPlayer的[解码能力决定于所采用的](../Page/解码.md "wikilink")[后端](../Page/前端和后端.md "wikilink")，这与其他[Linux下的音视频](../Page/Linux.md "wikilink")[播放](../Page/播放.md "wikilink")[软件是一模一样的](../Page/软件.md "wikilink")。该[媒体播放器采用了](../Page/媒体播放器.md "wikilink")[Mplayer](../Page/Mplayer.md "wikilink")、[xine和](../Page/xine.md "wikilink")[GStreamer等等数个Linux下当下较为完善的](../Page/GStreamer.md "wikilink")[媒体](../Page/媒体.md "wikilink")[解码](../Page/解码.md "wikilink")[构件](../Page/构件.md "wikilink")，几乎支持了所有格式[影视和](../Page/影视.md "wikilink")[音频](../Page/音频.md "wikilink")[文件的](../Page/文件.md "wikilink")[播放](../Page/播放.md "wikilink")。此外它还可以[放映](../Page/放映.md "wikilink")[ISO格式的](../Page/ISO_9660.md "wikilink")[DVD](../Page/DVD.md "wikilink")[光盘镜像](../Page/光盘镜像.md "wikilink")[文件](../Page/文件.md "wikilink")。

## 界面

KMPlayer的[人机接口是基于](../Page/人机接口.md "wikilink")[Qt而](../Page/Qt.md "wikilink")[编写](../Page/编程.md "wikilink")，可以[插件方式良好](../Page/插件.md "wikilink")[整合进](../Page/嵌入.md "wikilink")[Konqueror](../Page/Konqueror.md "wikilink")\[1\]
。

## 同名软件

Windows操作系统下另有一个名为“[The
KMPlayer](../Page/The_KMPlayer.md "wikilink")”的播放软件，最初是由姜勇囍（）开发的，名字缩写也是“KMP”。虽为播放软件，但除了名称近似之外，事实上两者并无任何关联。

在
[Linux](../Page/Linux.md "wikilink")／[UNIX](../Page/UNIX.md "wikilink")
操作系统中运行的 KMPlayer，全名为“[Konqueror](../Page/Konqueror.md "wikilink") Media
Player”（字面上直译为“征服者媒体播放器”）。

## 外部链接

  - [KMPlayer媒体播放器的主页](http://kmplayer.kde.org/)
  - [移动电话用途版本的KMPlayer](http://kmplayer.garage.maemo.org/)

## 参考资料

<div class="references-small">

<references />

</div>

[Category:KDE Extragear](../Category/KDE_Extragear.md "wikilink")
[Category:自由媒體播放器](../Category/自由媒體播放器.md "wikilink")

1.  <http://kmplayer.kde.org/>