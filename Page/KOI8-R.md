**KOI8-R**是[KOI-8系列的](../Page/KOI-8.md "wikilink")[斯拉夫文字](../Page/斯拉夫语族.md "wikilink")8位元编码，供[俄语及保加利亚语使用](../Page/俄语.md "wikilink")。在[Unicode未流行之前](../Page/Unicode.md "wikilink")，KOI8-R
是最为广泛使用的俄语编码，使用率甚至比[ISO/IEC
8859-5还高](../Page/ISO/IEC_8859-5.md "wikilink")。

*Andrey Chernov* 把 KOI8-R
整理后，提交了给[互联网协会](../Page/互联网协会.md "wikilink")（ISOC），成了
RFC 1489 文件 ("Registration of a cyrillic Character Set")。

| **KOI8-R** |
| ---------- |
|            |
| 0x         |
| 1x         |
| 2x         |
| 3x         |
| 4x         |
| 5x         |
| 6x         |
| 7x         |
| 8x         |
| 9x         |
| Ax         |
| Bx         |
| Cx         |
| Dx         |
| Ex         |
| Fx         |

在上表中，0x20是空格、0x9A是不换行空格。

0x95 在 RFC 1489 之中是 U+2219 (∙)，但有时会使用 U+2022 (·) 以配合 Windows-1251 编码。

## 参看

  - [KOI-8](../Page/KOI-8.md "wikilink")
  - [KOI8-U](../Page/KOI8-U.md "wikilink")

## 外部链接

  - RFC 1489

[Category:字符集](../Category/字符集.md "wikilink")
[Category:西里尔字母](../Category/西里尔字母.md "wikilink")