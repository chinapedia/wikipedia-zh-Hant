[KOS-MOS_XS1.jpg](https://zh.wikipedia.org/wiki/File:KOS-MOS_XS1.jpg "fig:KOS-MOS_XS1.jpg")

**KOS-MOS**是[PS2遊戲系列](../Page/PS2.md "wikilink")[Xenosaga中的一個主要角色](../Page/Xenosaga.md "wikilink")。

## 基本資訊

  - **性別：**外表為女性
  - **身高：**167公分
  - **體重：**92公斤（第一版兵裝）
  - **年齡：**外表為18歲
  - **配音員（日版）：** [鈴木麻里子](../Page/鈴木麻里子.md "wikilink")

## 一些相關資料

KOS-MOS是一個由[貝克達公司](../Page/貝克達公司_\(異域傳說\).md "wikilink")（Vector
Industries）開發的武裝戰鬥[機器人](../Page/機器人.md "wikilink")，用以對付[格諾西斯](../Page/格諾西斯_\(異域傳說\).md "wikilink")（Gnosis），而其全名為**K**osmos
**O**bey **S**trategical **M**ultiple **O**peration
**S**ystems（中譯：**宇宙秩序維持用多功能戰略系統**）的簡寫。和當時以合成人（Realian）與[生化人](../Page/生化人.md "wikilink")（Cyborg）等為主流的狀況所不同的是，KOS-MOS是以純粹的機械部件所構成的。

她所遵守的原則是奠基於[邏輯](../Page/邏輯.md "wikilink")、[機率](../Page/機率.md "wikilink")、和完成她所有被給定的任務。

她亦無條件地保護著[卯月紫苑](../Page/卯月紫苑.md "wikilink")（シオン・ウヅキ），KOS-MOS的創造者之一。同時她內裝著「*模擬人格[操作系统](../Page/操作系统.md "wikilink")*」來協助和他人的溝通，且縱使從她所講的話可看出她存在的目的是為[人類服務](../Page/人類.md "wikilink")，她猶然趨向於把邏輯和機率放在首位考慮，而這使得她在有些時候變得難以捉摸。由於KOS-MOS行事時將邏輯、機率和任務放在首位，因此她在早期曾有為了戰鬥效率而將維吉爾中尉犧牲掉的事情發生。

KOS-MOS開發時的代號為*KP-X*，出廠時序號為*00-00-00-00-1*，據稱她有一部份的能量來源是來自於上位領域的U-Do透過索哈爾（Zohar）到下位領域的能量；另外KOS-MOS可使用[希爾伯特效應使格諾西斯](../Page/希爾伯特效應.md "wikilink")「實數領域化」。

貝克達公司CEO威廉事實上以KOS-MOS為瑪麗亞（命之力在實數領域理的化身）意識覺醒用的容器，但後來卻因為擔心KOS-MOS這個容器變質，於是便以瑪麗亞的肉體另外製造出來的T-elos，並使T-elos攻擊KOS-MOS以搶奪KOS-MOS體內的瑪麗亞的意識，不過到了後來KOS-MOS擊倒了T-elos，兩人融合為一，KOS-MOS進入藍眼狀態，不過到了遊戲劇情幾乎最後的地方，KOS-MOS已有了自身的意識，不再只是瑪麗亞意識的容器了。

## 「邪神MOCCOS」（邪神モッコス）

Xenosaga
EPII發行時，[Namco公司另有發行限量版的特典](../Page/Namco.md "wikilink")，而其內容為KOS-MOS的人物模型，但是這個模型使某些玩家失望、甚而憤怒，因為他們認為這個人物模型長得不好看，也就是說該人物模型被做壞了，因此他們開始稱這個模型為「邪神像」，甚至於給該邪神起了一個名字─MOCCOS（モッコス），甚至於有個日本的玩家還架了一個網站，大肆惡搞該模型，以表他心中的不滿\[1\]。

## 联动

在《[异度神剑2](../Page/异度神剑2.md "wikilink")》中，KOS-MOS可作为稀有异刃被召唤。她也是《[异度神剑2](../Page/异度神剑2.md "wikilink")》官方日文推特在游戏发行前公布的最后一个异刃。\[2\]

## 參見

  - [Xenosaga](../Page/Xenosaga.md "wikilink")
  - [A.G.W.S](../Page/A.G.W.S_\(異域傳說\).md "wikilink")
  - [卯月紫苑](../Page/卯月紫苑.md "wikilink")
  - [卡歐斯](../Page/卡歐斯.md "wikilink")
  - [瑪麗亞](../Page/瑪麗亞_\(異域傳說\).md "wikilink")

## 参考资料

[Category:电子游戏角色](../Category/电子游戏角色.md "wikilink")
[Category:異域傳說系列](../Category/異域傳說系列.md "wikilink")
[Category:虛構機器人兵器](../Category/虛構機器人兵器.md "wikilink")
[Category:虛構人形機器人](../Category/虛構人形機器人.md "wikilink")
[Category:南夢宮角色](../Category/南夢宮角色.md "wikilink")
[Category:2002年推出的電子遊戲角色](../Category/2002年推出的電子遊戲角色.md "wikilink")

1.  [1](https://web.archive.org/web/20060905220649/http://moccosep2.hp.infoseek.co.jp/)
2.   Twitter|accessdate=2017-12-06|work=twitter.com|language=en}}