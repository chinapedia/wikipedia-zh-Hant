**KTSF**是[美國](../Page/美國.md "wikilink")[加州](../Page/加州.md "wikilink")[舊金山灣區一家主要服務亞裔社群](../Page/舊金山灣區.md "wikilink")、不屬於任何電視網的獨立[廣播](../Page/廣播.md "wikilink")[電視台](../Page/電視台.md "wikilink")，由[林肯廣播公司](../Page/林肯廣播公司.md "wikilink")（Lincoln
Broadcasting
Company）擁有。KTSF的主頻道26.1﹐於晚上7點至10點的黃金時段，主要播放[粵語和](../Page/粵語.md "wikilink")[普通話節目](../Page/普通話.md "wikilink")，包括自製的新聞和外購的[臺灣](../Page/臺灣.md "wikilink")、[中國大陸](../Page/中國大陸.md "wikilink")、和[香港影劇](../Page/香港.md "wikilink")。同時於早上6點至8點以[粵語播放本地製作的晨早新聞](../Page/粵語.md "wikilink")﹐包括即時交通及天氣。其它時段播放[閩南語](../Page/臺灣閩南語.md "wikilink")、[印度語](../Page/印度語.md "wikilink")、[菲律賓語和](../Page/菲律賓語.md "wikilink")[越南語的節目](../Page/越南語.md "wikilink")，以及[英語](../Page/英語.md "wikilink")[付費資訊性廣告](../Page/電視購物.md "wikilink")。

## 歷史

有見於[三藩市灣區的主流電視台未能滿足區内亞裔社群所需](../Page/三藩市.md "wikilink")，商人[Lillian
Lincoln
Howell於](../Page/Lillian_Lincoln_Howell.md "wikilink")[1960年代申請在灣區創立一條服務該社群的免費電視頻道](../Page/1960年代.md "wikilink")，並於[1965年獲發牌照](../Page/1965年.md "wikilink")。經過重重波折後，該台終於在[1976年](../Page/1976年.md "wikilink")[9月4日啓播](../Page/9月4日.md "wikilink")，取[呼號KTSF](../Page/無綫電台呼號.md "wikilink")（代表「**T**elevision
**S**an **F**rancisco」），並以UHF26台播放。\[1\]

KTSF於[1989年成立新聞部](../Page/1989年.md "wikilink")，並於[1989年](../Page/1989年.md "wikilink")[2月6日推出一週五天現場直播粵語新聞報導](../Page/2月6日.md "wikilink")，成為全美國首家製作現場直播中文節目的電視台。[1989年](../Page/1989年.md "wikilink")[2月11日至](../Page/2月11日.md "wikilink")[2006年](../Page/2006年.md "wikilink")[3月18日期間](../Page/3月18日.md "wikilink")，時名為《[香港一周新聞](../Page/香港一周新聞.md "wikilink")》（）。KTSF後來開始製作[普通話新聞及普通話](../Page/普通話.md "wikilink")[現場討論節目](../Page/Phone-in.md "wikilink")《[今夜有話要説](../Page/今夜有話要説.md "wikilink")》，並於[2006年](../Page/2006年.md "wikilink")[3月25日起增設周末版粵語和普通話新聞](../Page/3月25日.md "wikilink")。

KTSF的[數碼電視頻道](../Page/數碼電視.md "wikilink")（KTSF-DT）於[2002年在UHF](../Page/2002年.md "wikilink")27台啓播\[2\]，其數碼接收器頻道號碼則設為26台，與其原有[模擬電視頻道號碼對應](../Page/模擬電視.md "wikilink")。隨著美國地面電視新舊制式過渡於[2009年](../Page/2009年.md "wikilink")[6月12日結束](../Page/6月12日.md "wikilink")，KTSF於當日終止模擬電視廣播，進入全數碼廣播年代。\[3\]KTSF的26.2副頻道曾由三藩市灣區的東京電視廣播公司租用並播放由日本各電視台製作的日語節目，但相關公司已把頻道改經其它電視台播放，原本的26.2副頻道現播放[印度節目](../Page/印度.md "wikilink")。而26.3副頻道則播放[韓國](../Page/韓國.md "wikilink")[文化廣播公司旗下的MBC頻道](../Page/文化廣播_\(韓國\).md "wikilink")。26.4及26.5副頻道則播放[越南節目](../Page/越南.md "wikilink")。

## 節目

### 新聞節目

#### 本地新聞

| 節目名稱                                       | 播放時段                            |
| ------------------------------------------ | ------------------------------- |
| 《[7點鐘新聞](../Page/7點鐘新聞.md "wikilink")》（）   | 周一至周五晚上7時至8時﹐周六周日晚上7時至7時30分     |
| 《[10點鐘新聞](../Page/10點鐘新聞.md "wikilink")》（） | 周一至周五晚上10時至11時﹐周六周日晚上10時至10時30分 |
| 《[晨早新聞](../Page/晨早新聞.md "wikilink")》（）     | 周一至周五早上6時至8時                    |

### 現職主播

  - [黃侯彬](../Page/黃侯彬.md "wikilink")
  - [嚴劍蓉](../Page/嚴劍蓉.md "wikilink")
  - [古琳嘉](../Page/古琳嘉.md "wikilink")
  - [萬若全](../Page/萬若全.md "wikilink")

### 前任記者／主播

  - [梁家榮](../Page/梁家榮.md "wikilink")\[4\]（香港[廣播處長](../Page/廣播處長.md "wikilink")）
  - [鄧景輝](../Page/鄧景輝.md "wikilink")\[5\]（[1992年至](../Page/1992年.md "wikilink")[2000年擔任](../Page/2000年.md "wikilink")[亞視新聞記者和主播](../Page/亞視新聞.md "wikilink")）
  - [斯美玲](../Page/斯美玲.md "wikilink")（從[2009年至](../Page/2009年.md "wikilink")[2011年](../Page/2011年.md "wikilink")，其間在[台灣擔任](../Page/台灣.md "wikilink")[壹電視新聞總監](../Page/壹電視.md "wikilink")，[2013年重返KTSF](../Page/2013年.md "wikilink")）\[6\]
  - [劉浩任](../Page/劉浩任.md "wikilink")
  - [梁國書](../Page/梁國書.md "wikilink")
  - [林子謙](../Page/林子謙.md "wikilink")
  - [廖培君](../Page/廖培君.md "wikilink")
  - [陳捷](../Page/陳捷.md "wikilink")

### 海外新聞

隨了自製新聞外，KTSF播放海外電視台的新聞節目，包括[中國中央電視台的](../Page/中國中央電視台.md "wikilink")《[中國新聞](../Page/中國新聞.md "wikilink")》、[台灣的](../Page/台灣.md "wikilink")《[中天新聞](../Page/中天新聞.md "wikilink")》、以及[菲律賓ABS](../Page/菲律賓.md "wikilink")-CBN的《TV
Patrol
World》。過去亦曾播放[香港](../Page/香港.md "wikilink")[亞洲電視的](../Page/亞洲電視.md "wikilink")《[新聞報道](../Page/亞視新聞.md "wikilink")》，後來被取消。

### 本地製作

<table>
<thead>
<tr class="header">
<th><p>節目名稱</p></th>
<th><p>主持</p></th>
<th><p>播放時段</p></th>
<th><p>内容</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>《<a href="../Page/有話要説.md" title="wikilink">有話要説</a>》（）</p></td>
<td><p><a href="../Page/鄭家瑜.md" title="wikilink">鄭家瑜</a></p></td>
<td><p>周五中午12時至12時半現場直播。周五晚11點及周日晚7點半重播</p></td>
<td><p>國語互動式直播訪談節目</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/華人叢刊.md" title="wikilink">華人叢刊</a>》（）</p></td>
<td><p><a href="../Page/鍾月娟.md" title="wikilink">鍾月娟</a>（粵語）<br />
<a href="../Page/李適萍.md" title="wikilink">李適萍</a>（普通話）</p></td>
<td><p>周日下午5時至5時半</p></td>
<td><p>華人社區公共事務及資訊節目，以普通話及粵語隔星期交替製作</p></td>
</tr>
</tbody>
</table>

## 數碼廣播

KTSF的數碼頻道現時分為下列副頻道：

| 副頻道號 | 節目供應商            |
| ---- | ---------------- |
| 26.1 | KTSF主頻道          |
| 26.2 | Diya TV          |
| 26.3 | MBC America      |
| 26.4 |                  |
| 26.5 | Viet Today TV    |
| 26.6 | Viet Shopping TV |
| 26.7 |                  |

## 頻道口號

  - 1988年-1998年：Your Window to the World／ 觀覽世界
  - 2003年-：The Face of the Bay Area／ 灣區風貌

## 參考文獻及備註

### 備註

<div class="references-small">

</div>

## 外部連結

  - [官方網頁](http://www.ktsf.com)

  -
[Category:美国中文电视台](../Category/美国中文电视台.md "wikilink")
[Category:旧金山组织](../Category/旧金山组织.md "wikilink")
[Category:粵語電視頻道](../Category/粵語電視頻道.md "wikilink")
[Category:华语电视频道](../Category/华语电视频道.md "wikilink")
[Category:1976年美国建立](../Category/1976年美国建立.md "wikilink")
[Category:1976年成立的电视台或电视频道](../Category/1976年成立的电视台或电视频道.md "wikilink")

1.
2.  [BIAfn's Media Web Database -- Information on
    KTSF-TV](http://www.bia.com/resources_search_result.asp?calls=KTSF&media=TV)

3.

4.  [傳媒透視：三藩市華語電視傳媒](http://www.rthk.org.hk/mediadigest/md9709/Sep6.html)

5.  [信用卡防盜系統（控股）有限公司 2006年2月24日新聞稿](http://www.hkexnews.hk/listedco/listconews/sehk/20060523/LTN20060523126_C.pdf)

6.  [26台資深主播
    斯美玲回灣區了](http://sf.worldjournal.com/view/full_sf/21412360/article-26台資深主播-斯美玲回灣區了)
    ，世界日報，[2013年](../Page/2013年.md "wikilink")[1月15日](../Page/1月15日.md "wikilink")