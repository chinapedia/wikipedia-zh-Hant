[Computers-kvm-switch-amoswolfe.png](https://zh.wikipedia.org/wiki/File:Computers-kvm-switch-amoswolfe.png "fig:Computers-kvm-switch-amoswolfe.png")
**KVM切換器**（），一般簡稱**KVM**，又名**多電腦切換器**，是一種[電腦](../Page/電腦.md "wikilink")[硬體設備](../Page/硬體.md "wikilink")，可以使用户透過一組[鍵盤](../Page/鍵盤.md "wikilink")、[螢幕和](../Page/螢幕.md "wikilink")[滑鼠控制多台電腦](../Page/滑鼠.md "wikilink")。KVM，即键盘、显示器、鼠标的英文首字母缩写（**K**eyboard、**V**ideo、**M**ouse）。

## 使用

透過KVM切換器，可用一組鍵盤、顯示器與滑鼠控制二台以上電腦；雖然有多台電腦連接到一台KVM設備上，但用户同時只能控制其中的一個。現在一些比較新的KVM切換器還支援[USB](../Page/USB.md "wikilink")、[音效等設備的共享](../Page/音效.md "wikilink")。

使用者連接一組螢幕、鍵盤和滑鼠至KVM切換器，然後再利用特殊的線材（通常為USB或[VGA線](../Page/VGA.md "wikilink")）去將切換器連接至其餘的電腦主機。接著透過切換器的開關或鍵盤上的按鍵去切換控制不同的電腦主機，可以將當前在使用的電腦主機切換成另一台所選擇到的電腦主機訊號。大部份的電子裝置也可以被切換器的鍵盤指令所控制，俗稱『[快速鍵](../Page/快速鍵.md "wikilink")』（HotKey）（依據製造商不同，所設定的快速鍵皆不同）。

多電腦切換器可以控制的數量不同，從2台到64台電腦主機都有方法可以解決。商用等級的裝置甚至可以用一組鍵盤、滑鼠和螢幕，經由[菊鏈式](../Page/菊鏈式.md "wikilink")（Daisy-chained）架構控制更多台電腦主機。現在更有網路化的KVM，可透過網路遠端控制。

## 分類

[ATEN_CS52A_KVM_switch_2009.jpg](https://zh.wikipedia.org/wiki/File:ATEN_CS52A_KVM_switch_2009.jpg "fig:ATEN_CS52A_KVM_switch_2009.jpg")的CS52A是電子式KVM切換器，以線控（左下）切換操作2台電腦主機。\]\]
KVM切換器根據運作方式，可以分為機械式及電子式兩種。這兩種各有不同的原理運作。

「IP KVM」是基于KVM Over
IP的产品概念的中国式的常规称呼，主要的应用概念就是：通过[IP技术](../Page/IP技术.md "wikilink")（[LAN](../Page/LAN.md "wikilink")、[WAN](../Page/WAN.md "wikilink")）的[通讯技术](../Page/通讯.md "wikilink")，实现对[键盘](../Page/键盘.md "wikilink")、显示图像、[鼠标的远程操作和控制](../Page/鼠标.md "wikilink")。

一般来说，IP
KVM基本等同于目前KVM的基本分类，如[多用户](../Page/多用户.md "wikilink")，普通线缆连接或CAT5线缆连接，但是就其IP
KVM的个体来说，还是另外增加了一个单口的IP KVM。

## 優點

  - 節省不必要的設備投資。
  - 節省空間，可有效利用狹窄的機房空間。
  - [工程師可在同一地點工作](../Page/工程師.md "wikilink")，不必在機房間疲於奔命，提高工作效率。

## 参见

  - [Synergy+](../Page/Synergy+.md "wikilink") 类似用途的软件,需要配备多台显示器
  - [VNC](../Page/VNC.md "wikilink") 类似用途的软件

[Category:系统管理](../Category/系统管理.md "wikilink")
[Category:远程管理软件](../Category/远程管理软件.md "wikilink")
[Category:输入/输出](../Category/输入/输出.md "wikilink")
[Category:電腦週邊設備](../Category/電腦週邊設備.md "wikilink")