**KWord**，是一個[Adobe](../Page/Adobe.md "wikilink") FrameMaker-like
文書編輯軟體，也是[KOffice的組件之一](../Page/KOffice.md "wikilink")，於1998年推出，屬於KOffice中的新成員。地位相當於[Microsoft
Office的](../Page/Microsoft_Office.md "wikilink")[Word](../Page/Word.md "wikilink")，Microsoft
Word做到的事KWord也做得到，例如在KWord之中插入並修改由[KSpread所打造出來的表格](../Page/KSpread.md "wikilink")（Table），KWord
支援多種文件檔格式：[odt](../Page/开放文档格式.md "wikilink")、[kwd](../Page/kwd.md "wikilink")、[txt以及](../Page/txt.md "wikilink")[html格式](../Page/html.md "wikilink")。

KWord已因為[Calligra
Words](../Page/Calligra_Words.md "wikilink")（隸屬於[Calligra
Suite](../Page/Calligra_Suite.md "wikilink")）的出現而過時。

## 參見

## 外部連結

  - [Project
    Homepage](https://web.archive.org/web/20081011052044/http://koffice.org/kword)
  - [KWord user
    manual](http://www.linuxtopia.org/online_books/office_guides/koffice_kword_guide/index.html)

[de:KOffice\#Textverarbeitung](../Page/de:KOffice#Textverarbeitung.md "wikilink")

[Category:办公室自动化软件](../Category/办公室自动化软件.md "wikilink")
[Category:KOffice](../Category/KOffice.md "wikilink")
[Category:KDE](../Category/KDE.md "wikilink")