**KAORI**（，）、是[日本](../Page/日本.md "wikilink")[女性](../Page/女性.md "wikilink")[聲優](../Page/聲優.md "wikilink")、[歌手](../Page/歌手.md "wikilink")。於[神奈川縣](../Page/神奈川縣.md "wikilink")[横濱市出生](../Page/横濱市.md "wikilink")。在2001年前以「川菜翠」和「川菜明子」的身分出道。KAORI會使用「鈴木香織」（）的名義出席在歌手及演員界中的活動，為搖滾樂隊「Spunky
Strider」的主音，由於患上「痙攣性發聲障礙」和「頸部肌張力障礙話語」，目前休養中及暫停樂隊活動。
2013年出席了印尼的雅加達JavaJazz
音樂節的第3天活動（並且於Youtube網路直播）\[1\]。

## 出演作品

※ **粗字**代表主要角色

### 電視動畫

  - 1999年

<!-- end list -->

  - D4公主（銀乃翼）

'''2000年

  - [深藍救兵](../Page/深藍救兵.md "wikilink")（**淺蔥馬林**）

<!-- end list -->

  - 2001年

<!-- end list -->

  - [コスモウォーリアー零](../Page/コスモウォーリアー零.md "wikilink")（レ・シルビアーナ）
  - [神鵰俠侶](../Page/神鵰俠侶.md "wikilink")（陸無雙）
  - [最终幻想：無限](../Page/最终幻想：無限.md "wikilink")（阿露）

<!-- end list -->

  - 2002年

<!-- end list -->

  - [寵物小精靈超世代](../Page/寵物小精靈超世代.md "wikilink")（**小遙**）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [星夢美少女](../Page/星夢美少女.md "wikilink")（ミロ）
  - [週刊寵物小精靈放送局](../Page/週刊寵物小精靈放送局.md "wikilink")（[露力麗](../Page/露力麗.md "wikilink")）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [NANA](../Page/NANA.md "wikilink")（**小松奈奈**）
  - [新登場\!飛天小女警Z](../Page/新登場!飛天小女警Z.md "wikilink")（明日香）
  - [黑貓](../Page/黑貓.md "wikilink")（サキ）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [神奇寶貝鑽石&珍珠](../Page/神奇寶貝鑽石&珍珠.md "wikilink")（小遙）\*以「」名義

### 劇場版動畫

  - 寵物小精靈劇場版：七夜的許願星（**小遙**）
  - 寵物小精靈劇場版：裂空之訪問者（**小遙**）
  - 寵物小精靈劇場版：夢夢與波導勇者（**小遙**）
  - 寵物小精靈劇場版：寵物小精靈保育家與蒼海的王子（**小遙**）

### 遊戲

  - [どきどきポヤッチオ](../Page/どきどきポヤッチオ.md "wikilink")（**ルフィー**）
  - [ユア・メモリーズオフ〜Girl's
    Style〜](../Page/ユア・メモリーズオフ〜Girl's_Style〜.md "wikilink")（**月岡海**）

### 廣播劇CD

  - （ルフィー）

  - （ライム）

  - （ライム）

  - [英雄傳說IV 朱紅的淚](../Page/英雄傳說IV_朱紅的淚.md "wikilink")（アルチェム）

  - 英雄傳說IV 朱紅的淚 第2章（リタ）

  - [TARAKOぱっぱらパラダイス](../Page/TARAKOぱっぱらパラダイス.md "wikilink")（ライム）

  - （ニーナ）

  - （クレール）

  - FALCOM SPECIAL BOX '97 MIDORI KAWANA SINGS
    [Ys](../Page/イースシリーズ.md "wikilink")

### 廣播

  -
  -
  -
  - （[新潟放送](../Page/新潟放送.md "wikilink")）

  - （アシスタント）

  -
## 作品

以下是用單獨名義發表的專輯。由5pb.（Five Recordsレーベル）發行

### 單曲

  - BLUE BLUE WAVE（2006年10月6日發售）

<!-- end list -->

1.  BLUE BLUE WAVE

      -
        遊戲《[水星領航員](../Page/水星領航員.md "wikilink")》片頭歌

2.    -
        遊戲《水星領航員》片尾歌

3.  BLUE BLUE WAVE（off vocal）

4.  （off vocal）

<!-- end list -->

  - （2007年4月25日發售）

<!-- end list -->

1.    -
        動畫《[青空下的約定](../Page/青空下的約定.md "wikilink")》片頭歌

2.    -
        動畫《》片尾歌（COVER VERSION）

3.  （off vocal）

4.  （off vocal）

<!-- end list -->

  - Tears Infection（2007年10月24日發售）

<!-- end list -->

1.  Tears Infection
      -
        動畫《[Myself ;
        Yourself](../Page/Myself_;_Yourself.md "wikilink")》片頭歌
2.  Day-break
      -
        遊戲《Myself ; Yourself》主題歌
3.  Tears Infection（off vocal）
4.  Day-break（off vocal）

<!-- end list -->

  - （2008年2月27日發售）

<!-- end list -->

1.    -
        動畫《[AYAKASHI](../Page/AYAKASHI.md "wikilink")》片尾歌

2.
3.  （off vocal）

4.  （off vocal）

<!-- end list -->

  - （2008年4月23日發售）

\#: 遊戲《[12RIVEN -the Ψcliminal of
integral-](../Page/12RIVEN_-the_Ψcliminal_of_integral-.md "wikilink")》片尾歌

1.  third bridge

      -
        遊戲《12RIVEN -the Ψcliminal of integral-》片頭歌

2.  （off vocal）

3.  third bridge（off vocal）

### 專輯

  - re;stratosphere（2007年8月10日發售）

<!-- end list -->

1.    -
        遊戲《[青空下的約定](../Page/青空下的約定.md "wikilink")》片頭歌

2.  BLUE BLUE WAVE

      -
        遊戲《》片頭歌

3.  Separate Hearts

      -
        遊戲《》片頭歌

4.  little prophet -ambient mix-

      -
        遊戲《Remember11 -the age of infinity-》片頭歌（再錄音）

5.  \-naked mix-

      -
        遊戲《》片頭歌（再錄音）

6.    -
        遊戲《ARIA The NATURAL 〜遠い記憶のミラージュ〜》片尾歌

7.  \-psycho mix-

      -
        遊戲《[Ever17 -the out of
        infinity-](../Page/Ever17_-the_out_of_infinity-.md "wikilink")》片頭歌（再錄音）

8.    -
        遊戲《》片頭歌

9.    -
        遊戲《》片頭歌

10.   -
        動畫《[青空下的約定](../Page/青空下的約定.md "wikilink")》片頭歌

11. \-acoustic mix-

      -
        [OVA](../Page/OVA.md "wikilink")《[Memories
        Off](../Page/Memories_Off.md "wikilink")》片尾歌（再錄音）

12. stratosphere

## 參考資料

## 外部連結

  - [KAORI Official Site](http://www.tosp.co.jp/i.asp?i=228kaori)
  - [Spunky Strider
    オフィシャルサイト](https://web.archive.org/web/20080317110858/http://spunkystrider.good.cx/html/spunky.html)
  - [「re;stratosphere」 CD特設サイト](http://5pb.jp/records/sp/stratosphere/)
  - [アニメイトTV アーティストインタビュー
    KAORI](http://www.animate.tv/column/singer/070816.php)

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:日本女歌手](../Category/日本女歌手.md "wikilink")
[Category:1976年出生](../Category/1976年出生.md "wikilink")
[Category:神奈川縣出身人物](../Category/神奈川縣出身人物.md "wikilink")

1.  2013/03/03 Youtube直播 印尼雅加達 JavaJazz爪哇爵士音樂節 第三天螢幕截圖，[2013/03/03
    Youtube直播JavaJazz爪哇爵士音樂節 第三天螢幕截圖](http://imgur.com/Bichcip)