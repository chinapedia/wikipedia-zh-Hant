**Kellylicious**是[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")[陳慧琳的第十八張個人粵語大碟](../Page/陳慧琳.md "wikilink")，於2008年5月16日正式發售。此專輯一共收錄了10首粵語歌曲和1首國語歌曲。此專輯只於[香港](../Page/香港.md "wikilink")、[馬來西亞及](../Page/馬來西亞.md "wikilink")[新加坡發售](../Page/新加坡.md "wikilink")，並未開始於[中國內地發售](../Page/中國內地.md "wikilink")。

## 介紹

### 製作歷程

**Kellylicious**是[陳慧琳自](../Page/陳慧琳.md "wikilink")2006年8月的[Happy
Girl後睽違已久的專輯](../Page/Happy_Girl.md "wikilink")，而陳慧琳亦自稱此專輯已於2007年頭已開始著手籌備。因此，此專輯是一個跨年度製作的粵語專輯。陳慧琳亦因需要拍製電影[江山美人](../Page/江山美人_\(2008年電影\).md "wikilink")，並已於2007年3月開始投入忙錄狀態，正式開始拍製，因此陳慧琳於2007年並沒有推出任何唱片。陳慧琳亦自稱，於拍攝期間，不停聆聽專輯歌曲的範本。直至2007年12月，陳慧琳已完成拍攝工作，她才正式投入專輯製作工作。

直至2008年，陳慧琳需要為電影作出宣傳，亦要排練即將於2008年6月舉行的Love
Fighters演唱會2008。陳慧琳自稱於2008年3月29日前往[韓國宣傳才正式錄製於演唱會主題曲](../Page/韓國.md "wikilink")《主打愛》（專輯曲目04）。直至2008年5月7日，陳慧琳的官方網站正式公佈於2008年5月10日出售此專輯予各大音樂公司（包括：[MOOV](../Page/MOOV.md "wikilink").now.com,
[yesasia](../Page/yesasia.md "wikilink")，[HMV等](../Page/HMV.md "wikilink")），並於2008年5月16日正式發售。

### 製作及設計

此專輯名字**Kellylicious**是由陳慧琳本人構思的，意思是指專輯中的歌曲就如同美食般美味（Delicious）。此專輯找來七大監製合作，包括[王雙駿](../Page/王雙駿.md "wikilink")、[舒文](../Page/舒文.md "wikilink")、[C.Y.Kong](../Page/C.Y.Kong.md "wikilink")、[張佳添](../Page/張佳添.md "wikilink")、[陳西敏](../Page/陳西敏.md "wikilink")、[馮翰銘及合作多年的](../Page/馮翰銘.md "wikilink")[雷頌德](../Page/雷頌德.md "wikilink")。她自稱希望不想再唱跟以往差不多的歌曲，因此找了7個監製，但合作多年的[林夕在此專輯中只負責填江山美人主題曲的歌詞](../Page/林夕.md "wikilink")。

### 發售地點

此專輯只於[香港](../Page/香港.md "wikilink")、[馬來西亞及](../Page/馬來西亞.md "wikilink")[新加坡](../Page/新加坡.md "wikilink")，但並未於[中國內地發售](../Page/中國內地.md "wikilink")，因此，於[中國內地的歌迷](../Page/中國內地.md "wikilink")，只可向香港公司或網上訂購。

## 專輯包裝

這專輯包裝包括一隻[CD](../Page/CD.md "wikilink")、一隻[DVD及一本](../Page/DVD.md "wikilink")21頁相片歌詞集。部分商店亦附送**Kellylicious**海報一張。

### CD

1.  大
2.  有時寂寞
3.  中毒太甚
4.  主打愛（Love Fighters演唱會08主題曲）
5.  傻男
6.  太悶
7.  抱歉柯德莉夏萍
8.  知我莫問
9.  我的脆弱與堅強
10. 最好給最好（美心月餅廣告歌）
11. 隨夢而飛（國）（[黎明合唱](../Page/黎明.md "wikilink")）（電影《[江山美人](../Page/江山美人_\(2008年電影\).md "wikilink")》主題曲）

### DVD

1.  有時寂寞 MV
2.  傻男 MV
3.  抱歉柯德莉夏萍 MV
4.  最好給最好 MV

### 派台歌曲

1.  最好給最好
2.  隨夢而飛
3.  抱歉柯德莉夏萍
4.  有時寂寞
5.  大

## 參見

  - [陳慧琳](../Page/陳慧琳.md "wikilink")
  - [陳慧琳歷年推出專集](../Page/陳慧琳#.E6.AD.B7.E5.B9.B4.E6.8E.A8.E5.87.BA.E5.B0.88.E9.9B.86.md "wikilink")

## 相關網站

  - [陳慧琳官方網站](https://web.archive.org/web/20070124122828/http://www.kellychen.hk/)
  - [環球及正東唱片官方網站](http://www.umg.com.hk)
  - [Kellylicious官方網站](http://www.umg.com.hk/minisite/Kelly_licious)

[category:香港音樂專輯](../Page/category:香港音樂專輯.md "wikilink")
[category:陳慧琳音樂專輯](../Page/category:陳慧琳音樂專輯.md "wikilink")

[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")
[Category:2008年音樂專輯](../Category/2008年音樂專輯.md "wikilink")