《**Kenny's
Essentials**》是香港男歌手[關智斌](../Page/關智斌.md "wikilink")（Kenny）的第5張專輯，共分成兩個版本雙封面發行，隨碟加送60頁寫真集，以及卡拉OK
DVD，收錄了兩首新曲以及13首精選歌的MV作品。

## 曲目

| \#   | 曲名                                                                                                       |
| ---- | -------------------------------------------------------------------------------------------------------- |
| 1\.  | 代表作（新歌）                                                                                                  |
| 2\.  | 白木屋（新歌）                                                                                                  |
| 3\.  | 小學館（與[吳浩康](../Page/吳浩康.md "wikilink")、[泳兒](../Page/泳兒.md "wikilink")、[鍾舒漫合唱](../Page/鍾舒漫.md "wikilink")） |
| 4\.  | Book B                                                                                                   |
| 5\.  | 預言書                                                                                                      |
| 6\.  | 3+1=1（與[Sun Boy'z合唱](../Page/Sun_Boy'z.md "wikilink")）                                                   |
| 7\.  | 助聽器                                                                                                      |
| 8\.  | 再見．美惠 (Radio Mix)                                                                                        |
| 9\.  | 閒人勿近                                                                                                     |
| 10\. | 良心發現（與[鍾欣桐合唱](../Page/鍾欣桐.md "wikilink")）                                                                |
| 11\. | 靈犀                                                                                                       |
| 12\. | 月光光 (Radio Mix)                                                                                          |
| 13\. | Let It Be                                                                                                |
| 14\. | 先知                                                                                                       |
| 15\. | 分手不要太悲哀                                                                                                  |
| 16\. | 眼紅館                                                                                                      |

## 參考資料

<div class="references-small">

<references />

</div>

## 相關網站

  - [關智斌個人網誌](https://web.archive.org/web/20070602004222/http://hk.myblog.yahoo.com/kenny-kwanjr)
  - [英皇娛樂](https://web.archive.org/web/20070608095353/http://eegmusic.com/artists/details.aspx?CTID=244940ef-d8aa-4de7-9fc3-42c5e030574b)

[Category:關智斌音樂專輯](../Category/關智斌音樂專輯.md "wikilink")
[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:2008年音樂專輯](../Category/2008年音樂專輯.md "wikilink")