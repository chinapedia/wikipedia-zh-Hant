《**Keroro軍曹**》（）是[日本漫畫家](../Page/日本漫畫家.md "wikilink")[吉崎觀音所創作的](../Page/吉崎觀音.md "wikilink")[日本漫畫作品](../Page/日本漫畫.md "wikilink")。於1999年開始連載於《[月刊少年Ace](../Page/月刊少年Ace.md "wikilink")》雜誌。描述一群青蛙型外星人來到地球後，策劃各種侵略地球的計劃並引發笑料的故事。

本作品以其對日本各大經典[ACG作品的](../Page/ACG.md "wikilink")[戲仿而聞名](../Page/戲仿.md "wikilink")。還曾出現針對[政治家](../Page/政治家.md "wikilink")、[綜藝節目](../Page/綜藝節目.md "wikilink")、[特攝](../Page/特攝.md "wikilink")、[連續劇和](../Page/連續劇.md "wikilink")[電影所做的嘲諷](../Page/電影.md "wikilink")。

## 劇情簡介

「伽瑪（Gamma）[星雲第](../Page/星雲.md "wikilink")58號[行星](../Page/行星.md "wikilink")·宇宙侵略軍特殊先鋒部隊隊長」Keroro軍曹，為了要進行征服藍星（劇中設定為全宇宙對[地球的稱呼](../Page/地球.md "wikilink")，但地球人除外。漫畫版原文為「」\[1\]，因帶藐視之意而在日本境內為禁播的詞彙，在動畫版中改為「」。[香港有線電視版本直接翻譯為地球](../Page/香港有線電視.md "wikilink")。）前的準備工作，而潛入日向家。但卻輕易的被冬樹和夏美擄獲，侵略用武器Kero球也落入冬樹手中。其後，侵略總部以將有被發現的危險性為由，緊急撤離地球，留下了在藍星的先鋒部隊。於是，Keroro軍曹開始了隸屬於日向家的生活。隨後軍曹的部下們—「雙重人格」Tamama二等兵、「燃燒鬥士」Giroro伍長、「電脑系」Kururu曹長、「暗殺兵」Dororo兵長和「恐怖大王」安哥爾·摩亞也相繼登場……。

原先在1998年連載前的短篇中，設定為夏美從祖母手上接收了一隻異常大隻的蝌蚪，並把夏美的友人嚇了一跳，而蝌蚪雖然聲明自己只是落難於地球的外星人，但實際上前來探查並準備侵略地球的故事，而這個故事在原作225話和232話中作了延伸與後續的交代。

## 登場人物

  - **[Keroro](../Page/Keroro.md "wikilink")**
  - **[Giroro](../Page/Giroro.md "wikilink")**
  - **[Tamama](../Page/Tamama.md "wikilink")**
  - **[Dororo](../Page/Dororo.md "wikilink")**
  - **[Kururu](../Page/Kururu.md "wikilink")**

<!-- end list -->

  - **[日向冬樹](../Page/日向冬樹.md "wikilink")**
  - **[日向夏美](../Page/日向夏美.md "wikilink")**
  - **[西澤桃華](../Page/西澤桃華.md "wikilink")**

## 出版書籍

<table>
<thead>
<tr class="header">
<th><p>卷數</p></th>
<th><p><a href="../Page/角川書店.md" title="wikilink">角川書店</a></p></th>
<th><p><a href="../Page/台灣角川.md" title="wikilink">台灣角川</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>發售日期</p></td>
<td><p><a href="../Page/ISBN.md" title="wikilink">ISBN</a></p></td>
<td><p>發售日期</p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>1999年12月1日</p></td>
<td><p>ISBN 4-04-713307-8</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>2000年7月1日</p></td>
<td><p>ISBN 4-04-713344-2</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>2001年3月1日</p></td>
<td><p>ISBN 4-04-713396-5</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>2001年10月1日</p></td>
<td><p>ISBN 4-04-713455-4</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p>2002年6月1日</p></td>
<td><p>ISBN 4-04-713496-1</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p>2003年2月1日</p></td>
<td><p>ISBN 4-04-713532-1</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>2003年10月1日</p></td>
<td><p>ISBN 4-04-713574-7</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p>2004年3月27日</p></td>
<td><p>ISBN 4-04-713613-1</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p>2004年8月10日</p></td>
<td><p>ISBN 4-04-713651-4</p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p>2005年2月26日</p></td>
<td><p>ISBN 4-04-713705-7</p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td><p>2005年10月8日</p></td>
<td><p>ISBN 4-04-713762-6</p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td><p>2006年2月25日</p></td>
<td><p>ISBN 4-04-713791-X</p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p>2006年7月26日</p></td>
<td><p>ISBN 4-04-713837-1</p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td><p>2007年2月26日</p></td>
<td><p>ISBN 4-04-713903-9</p></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td><p>2007年7月26日</p></td>
<td><p>ISBN 4-04-713939-4</p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td><p>2008年3月3日</p></td>
<td><p>ISBN 978-4-04-715021-8</p></td>
</tr>
<tr class="even">
<td><p>17</p></td>
<td><p>2008年8月8日</p></td>
<td><p>ISBN 978-4-04-715092-8</p></td>
</tr>
<tr class="odd">
<td><p>18</p></td>
<td><p>2009年3月13日</p></td>
<td><p>ISBN 978-4-04-715179-6</p></td>
</tr>
<tr class="even">
<td><p>19</p></td>
<td><p>2009年8月4日</p></td>
<td><p>ISBN 978-4-04-715295-3</p></td>
</tr>
<tr class="odd">
<td><p>20</p></td>
<td><p>2010年2月26日</p></td>
<td><p>ISBN 978-4-04-715386-8</p></td>
</tr>
<tr class="even">
<td><p>21</p></td>
<td><p>2010年9月25日</p></td>
<td><p>ISBN 978-4-04-715529-9</p></td>
</tr>
<tr class="odd">
<td><p>22</p></td>
<td><p>2011年7月26日</p></td>
<td><p>ISBN 978-4-04-715747-7</p></td>
</tr>
<tr class="even">
<td><p>23</p></td>
<td><p>2012年3月26日</p></td>
<td><p>ISBN 978-4-04-120170-1</p></td>
</tr>
<tr class="odd">
<td><p>24</p></td>
<td><p>2013年1月26日</p></td>
<td><p>ISBN 978-4-04-120555-6</p></td>
</tr>
<tr class="even">
<td><p>25</p></td>
<td><p>2014年3月26日</p></td>
<td><p>ISBN 978-4-04-120979-0</p></td>
</tr>
<tr class="odd">
<td><p>26</p></td>
<td><p>2015年3月26日</p></td>
<td><p>ISBN 978-4-04-102865-0</p></td>
</tr>
<tr class="even">
<td><p>27</p></td>
<td><p>2016年5月26日</p></td>
<td><p>ISBN 978-4-04-104273-1</p></td>
</tr>
<tr class="odd">
<td><p>28</p></td>
<td><p>2017年4月26日</p></td>
<td><p>ISBN 978-4-04-105544-1</p></td>
</tr>
<tr class="even">
<td><p>29</p></td>
<td><p>2018年5月26日</p></td>
<td><p>ISBN 978-4-04-105545-8</p></td>
</tr>
</tbody>
</table>

  - 限定版贈品列表〈Comics Ace〉

<!-- end list -->

  - 第7冊：附贈Keroro小隊5人的模型。2003年9月18日發售、ISBN 4-04-900756-8
  - 第9冊：附贈Keroro小隊5人的徽章，原作者特製限定版書衣。2004年8月6日發售、ISBN 4-04-713654-0
  - 第10冊：附贈[帕瓦德夏美模型](../Page/帕瓦特·奥特曼.md "wikilink")，內附[泳裝夏美替換零件](../Page/日本學生泳衣.md "wikilink")，原作者特製限定版書衣。2005年2月17日發售、ISBN
    4-04-900770-1
  - 第11冊：附贈Action
    Keroro模型。原型設計者為[淺井真紀](../Page/淺井真紀.md "wikilink")。特別繪製書衣由負責。2005年9月26日發售、ISBN
    4-04-900771-8
  - 第12冊：
  - 第14冊：
  - 第16冊：
  - 第17冊：
  - 第18冊：
  - 第23冊：

### 特別編輯版

有些漫畫編集會依照特定主題，將分散於各單行本中，與主題相關的故事重新集合、收錄，並增加特別內容的「特別編集版」漫畫。全5冊。

<table>
<thead>
<tr class="header">
<th><p>日文書名</p></th>
<th><p>中文書名</p></th>
<th><p>角川書店</p></th>
<th><p>台灣角川</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>發售日期</p></td>
<td><p>ISBN</p></td>
<td><p>發售日期</p></td>
<td><p>ISBN</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Keroro軍曹Pink 女性角色大活躍篇</p></td>
<td><p>2004年3月30日</p></td>
<td><p>ISBN 4-04-924969-3</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Keroro軍曹Red 狂熱的宅魂篇</p></td>
<td><p>ISBN 4-04-924968-5</p></td>
<td><p>ISBN 978-986-174-427-8</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Keroro軍曹Green Keroro小隊5人眾集結編</p></td>
<td><p>ISBN 4-04-924967-7</p></td>
<td><p>ISBN 978-986-174-423-0</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Keroro軍曹Yellow K隆星的驚異機械篇</p></td>
<td><p>2006年11月30日</p></td>
<td><p>ISBN 4-04-925037-3</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Keroro軍曹Black Keroro小隊24小時篇</p></td>
<td><p>ISBN 4-04-925036-5</p></td>
<td><p>ISBN 978-986-174-424-7</p></td>
</tr>
</tbody>
</table>

## 動畫

### 電視動畫

由[日昇動畫製作](../Page/日昇動畫.md "wikilink")，並先後於日本[東京電視台](../Page/東京電視台.md "wikilink")、台灣[卡通頻道](../Page/卡通頻道.md "wikilink")、[華視](../Page/中華電視公司.md "wikilink")、香港[有線電視](../Page/香港有線電視.md "wikilink")、[無綫電視](../Page/電視廣播有限公司.md "wikilink")[翡翠台與中國](../Page/翡翠台.md "wikilink")[華娛衛視播放](../Page/華娛衛視.md "wikilink")。

於2004年開播為止，到2011年已有358集（最終集數），每一季有51集（第二季則是多一話有52集），現在動畫已經在2011年3月底後於第七季系列播放[完結](../Page/完結篇.md "wikilink")。

### 劇場版

目前為止，[東京電視台](../Page/東京電視台.md "wikilink")、[NAS與](../Page/NAS.md "wikilink")[日昇動畫總共製作了五部Keroro軍曹劇場版電影](../Page/日昇動畫.md "wikilink")，並先後於[日本](../Page/日本.md "wikilink")、[台灣與](../Page/台灣.md "wikilink")[香港上映](../Page/香港.md "wikilink")。

## 商品與音樂

## 爭議

  - [韓國與](../Page/韓國.md "wikilink")[香港的部份人士指責](../Page/香港.md "wikilink")《Keroro軍曹》鼓吹[軍國主義](../Page/軍國主義.md "wikilink")，理由包括：Keroro小隊侵略地球的背景設定，實際上是暗指[日本侵略](../Page/日本.md "wikilink")[亞洲](../Page/亞洲.md "wikilink")；Keroro的帽子為[第二次世界大戰時](../Page/第二次世界大戰.md "wikilink")[日本皇軍的軍帽](../Page/日本軍.md "wikilink")\[2\]；主題曲中曾出現作為[軍國主義象徵](../Page/軍國主義.md "wikilink")[太陽旗的K隆星旗](../Page/太陽旗.md "wikilink")。

<!-- end list -->

  - Tamama對Keroro的傾慕，也曾經被[基督教團體指為鼓吹](../Page/基督教.md "wikilink")[同性戀](../Page/同性戀.md "wikilink")
    \[3\]。但同時也有人反駁這只是用來娛樂觀眾的情節。

<!-- end list -->

  - 一個存在於讀者間的爭議為漫畫風格的改變：有讀者認為《Keroro軍曹》在獲得小學館漫畫賞並改編成動畫之後，風格為了迎合新增加的讀者（主要為兒童）而有了變化。被許多讀者認為是早期經典的半成人[黑色幽默與廣泛的影射明顯變少甚至完全消失](../Page/黑色幽默.md "wikilink")。《Keroro軍曹》似乎正漸漸變成單純賣弄K隆星人，而這也造成部份早期讀者流失。

## 註釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [吉崎觀音社](http://mnet.nicomi.com/index2.html)

  - [角川書店《Keroro軍曹》綜合公式網站「」](http://www.keroro.com/)

  - [東京電視台官方網站](http://www.tv-tokyo.co.jp/anime/keroro/)

  - [超劇場版
    Keroro軍曹](https://web.archive.org/web/20111018185702/http://www.keroro-movie.net/)

  - [Keroro軍曹りいたします。](http://www.sunrise-inc.co.jp/keroro/)

  - [Keroro同盟](http://keroro-doumei.net/)

  - [《Keroro軍曹》台灣官方網站](https://web.archive.org/web/20070313131608/http://www.keroro.com.tw/)

[\*](../Category/Keroro軍曹.md "wikilink")
[K](../Category/日本漫畫作品.md "wikilink")
[K](../Category/月刊少年Ace連載作品.md "wikilink")
[K](../Category/KEROKERO_ACE.md "wikilink")
[K](../Category/搞笑漫畫.md "wikilink")
[K](../Category/Game_Boy_Advance遊戲.md "wikilink")
[K](../Category/多摩地域背景作品.md "wikilink")
[K](../Category/无尾目主角故事.md "wikilink")

1.  「ポコペン」的[漢字為](../Page/漢字.md "wikilink")「[不彀本](../Page/不彀本.md "wikilink")」（即「不夠本」），原是[日軍在中日](../Page/日軍.md "wikilink")[甲午戰爭](../Page/甲午戰爭.md "wikilink")（日清戰爭）時以[中文](../Page/中文.md "wikilink")[普通話來稱呼](../Page/普通話.md "wikilink")[清軍的詞彙](../Page/清軍.md "wikilink")，ポコペン則是以日語[假名來表示的發音](../Page/假名.md "wikilink")。
2.  [Keroro軍曹頂帽激似皇軍](http://hk.apple.nextmedia.com/news/art/20060823/6245071)《[蘋果日報
    (香港)](../Page/蘋果日報_\(香港\).md "wikilink")》，2006年8月23日
3.