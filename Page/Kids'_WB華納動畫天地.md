《**Kids'
WB華納動畫天地**》（），原本是[美國的一個電視動畫時段](../Page/美國.md "wikilink")，創立於1995年9月，目前星期六[早晨在CW](../Page/早晨.md "wikilink")[電視頻道中播放](../Page/電視.md "wikilink")。該節目時段亦在香港播出，但播出時間不同。

2006年9月，[澳洲Nine](../Page/澳洲.md "wikilink")
Network開播同名節目，但播放內容僅有澳洲本土製作之動畫節目。

## 歷史

1995年9月9日，Kids'
WB華納動畫天地在WB[電視頻道啟播](../Page/電視.md "wikilink")，以後每逢星期六早上8:00-11:00及下午4:00
p.m.-5:00播出。1996年9月7日，Kids' WB星期六的早上時段延長至12:00。1997年9月1日，Kids'
WB首次出現在平日時段，逢星期一至五早上7:00-8:00及下午3:00-5:00播出。

2001年9月3日，Kids'
WB在未知原因的情況下取消了平日[早晨時段](../Page/早晨.md "wikilink")。2006年1月2日，Kids'
WB的所有下午時段被其他節目取代，剩下的星期六時段加長至早上7:00-12:00。2006年9月23日，Kids'
WB\!重新安排在CW[電視頻道中播放](../Page/電視.md "wikilink")，時間不變。

## 節目表

這最新時間表於2007年6月2日生效。

  - 07:00: [超級狗英雄](../Page/超級狗英雄.md "wikilink")（Krypto the
    Superdog，兩集連播）
  - 08:00: [樂一通超能特攻隊](../Page/樂一通超能特攻隊.md "wikilink")（Loonatics
    Unleashed）
  - 08:30: [湯姆傑利小故事](../Page/湯姆傑利小故事.md "wikilink")（Tom and Jerry Tales）
  - 09:00: [胆小狗](../Page/胆小狗.md "wikilink")（Shaggy & Scooby-Doo Get a
    Clue\!）
  - 09:30: [阿尼正传](../Page/阿尼正传.md "wikilink")（Johnny Test）
  - 10:00: [超能英雄團](../Page/超能英雄團.md "wikilink")（Legion of Super Heroes）
  - 10:30: [蝙蝠俠](../Page/蝙蝠俠.md "wikilink")（The Batman）
  - 11:00: [小林寺大决战](../Page/小林寺大决战.md "wikilink")（Xiaolin Showdown）
  - 11:30: [阿尼正传](../Page/阿尼正传.md "wikilink")（Johnny Test）

## 澳洲

在[澳洲](../Page/澳洲.md "wikilink")，《Kids'
WB華納動畫天地》在2006年9月16日於[澳洲Nine](../Page/澳洲.md "wikilink")
Network啟播。但這本土化的動畫時段只是播放由[澳洲本土製作的](../Page/澳洲.md "wikilink")[動畫影集](../Page/動畫影集.md "wikilink")，與[美國的Kids](../Page/美國.md "wikilink")'
WB截然不同。

### 節目表

  - [The Shapies](../Page/The_Shapies.md "wikilink")
  - 經典[樂一通](../Page/樂一通.md "wikilink")
  - [The Shak](../Page/The_Shak.md "wikilink")
  - [Deadly](../Page/Deadly_\(TV_series\).md "wikilink")
  - [The Sleepover Club](../Page/The_Sleepover_Club.md "wikilink")

## 香港

在[香港](../Page/香港.md "wikilink")，Kids'
WB華納動畫天地於[香港有線電視兒童台的星期一至五上午](../Page/香港有線電視兒童台.md "wikilink")10:15至11:15、下午4:15至5:15及晚上7:00至8:00播放。

## 外部連結

  - [官方網站](http://www.kidswb.com)
  - [CW[電視頻道](../Page/電視.md "wikilink")](http://www.cwtv.com)

[Category:華納兄弟動畫](../Category/華納兄弟動畫.md "wikilink")
[Category:華納媒體](../Category/華納媒體.md "wikilink")