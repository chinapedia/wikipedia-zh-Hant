**Kopete**是[KDE的一个子项目](../Page/KDE.md "wikilink")，支持多[协议的](../Page/协议.md "wikilink")[即时通信](../Page/即时通信.md "wikilink")，包括[ICQ](../Page/ICQ.md "wikilink")、[AIM](../Page/AIM.md "wikilink")、[Gadu-Gadu](../Page/Gadu-Gadu.md "wikilink")、[IRC](../Page/IRC.md "wikilink")、[.NET
Messenger
Service](../Page/.NET_Messenger_Service.md "wikilink")、[Yahoo\!
Messenger等协议](../Page/Yahoo!_Messenger.md "wikilink")。

## 支援的通訊協定

Kopete 所支援的通訊協定列表：

  - [AOL即時通訊](../Page/AOL即時通訊.md "wikilink")
  - [Bonjour](../Page/Bonjour.md "wikilink")
  - [Facebook](../Page/Facebook_Chat.md "wikilink")
    ([1](https://web.archive.org/web/20090528221749/http://duncan.mac-vicar.com/blog/archives/545))
  - [Gadu-Gadu](../Page/Gadu-Gadu.md "wikilink")
  - [ICQ](../Page/ICQ.md "wikilink")
  - [IRC](../Page/IRC.md "wikilink")
  - [Jabber](../Page/Jabber.md "wikilink")（[XMPP](../Page/XMPP.md "wikilink")）
  - [Lotus Sametime](../Page/Lotus_Sametime.md "wikilink") via the
    [Meanwhile](http://meanwhile.sourceforge.net/) plugin
  - [Google Talk](../Page/Google_Talk.md "wikilink")（即時傳訊部份使用
    [Jabber](../Page/Jabber.md "wikilink") 協定，語音部份使用
    [jingle](../Page/jingle.md "wikilink") 協定）
  - [Messenger service](../Page/Messenger_service.md "wikilink")
  - [.NET Messenger
    Service](../Page/.NET_Messenger_Service.md "wikilink")
  - [Novell GroupWise](../Page/Novell_GroupWise.md "wikilink")
  - [QQ](../Page/QQ.md "wikilink")
  - [SMS（手機簡訊）](../Page/簡訊.md "wikilink")
  - [Skype](../Page/Skype.md "wikilink")（需安裝 [Kopete
    Skype](http://extragear.kde.org/apps/kopete%20skype/) 擴充程式）
  - [Xfire](../Page/Xfire.md "wikilink") via the
    [kopete-xfire](http://code.google.com/p/kopete-xfire) plugin
  - [Yahoo\! Messenger](../Page/Yahoo!_Messenger.md "wikilink")

## 功能

  - 整合 [KAddressBook](../Page/KAddressBook.md "wikilink") 和
    [KMail](../Page/KMail.md "wikilink")
  - MSN 與 Yahoo\! messenger 支援 webcam

## 外掛程式

## 参见

  - [KMess](../Page/KMess.md "wikilink")
  - [即时通讯软件列表](../Page/即时通讯软件列表.md "wikilink")
  - [即时通讯软件比较](../Page/即时通讯软件比较.md "wikilink")

## 外部链接

  - [Kopete website](http://kopete.kde.org/)

[Category:自由的即时通讯软件](../Category/自由的即时通讯软件.md "wikilink")