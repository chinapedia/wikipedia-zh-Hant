**酷洛米**（香港譯名：**可羅米**，半意譯：**黑咪**），是[Sanrio另一隻以](../Page/Sanrio.md "wikilink")[惡魔造型的](../Page/惡魔.md "wikilink")[卡通女角色](../Page/卡通.md "wikilink")，在2005年10月31日誕生。在《[奇幻魔法Melody](../Page/奇幻魔法Melody.md "wikilink")》（おねがいマイメロディ）動畫系列初次出場。酷洛米原是[My
Melody的好友兼同學](../Page/My_Melody.md "wikilink")，但My
Melody時常無意間不小心傷害到Kuromi，故反目成仇視My
Melody為敵人。可羅米筆記（Kuromi Note），全部都是記載著自己被My
Melody害到的事。

酷洛米在馬里蘭樂園因思想偏激、經常故意作弄My
Melody、令父母擔心、採摘[皇宮](../Page/皇宮.md "wikilink")[花園的花草](../Page/花園.md "wikilink")、騎著三輪單車在街上亂駛，和偷去皇宮一大批[麵包這幾項罪名而被判入反省室](../Page/麵包.md "wikilink")（即[監獄](../Page/監獄.md "wikilink")），只是後來逃獄到人間去，所以她是馬里蘭樂園的通緝犯。行為舉止雖然粗魯，但實際上就像一般女孩那樣，非常喜歡帥氣的男生。喜歡的食物是[薤](../Page/薤.md "wikilink")（蕗藠、藠頭）。

酷洛米有一位部下名叫巴庫（Baku），是一隻以[貘及](../Page/貘.md "wikilink")[食夢獸作造型的角色](../Page/食夢獸.md "wikilink")，由於感激酷洛米在他最沮喪的時候（被父親罵）去教他怎麼忘記悲傷，又見他的十多個弟弟挨餓而到皇宮偷麵包，所以即使經常被酷洛米拳打腳踢，仍然忠心耿耿。巴庫有飛行的技能，所以也同時充當可羅米的私人座駕。除此之外，酷洛米亦是可羅米軍團（Kuromi's
5）的首領，有四位組員追隨。

在動畫23集《要是能夠和他跳支舞就好了》用魔法變成人，自稱kurumi。

## 名稱意義與發音

三麗鷗台灣分公司 及三麗鷗中國官網正式中文名稱均為「酷洛米」（Ku-luò-mǐ）。「クロ（kuro）」的意思是體色一部或全部為黑色（くろ）。

Ro的日言發音與「Law」接近，台灣不少人把她的名稱唸作Ku-lu-Mi是錯誤的。而「kuro」的漢字是「黑」，也就是她最喜歡的顏色。

Sanrio Japan和草莓新聞合辦的2010 Sanrio Character Ranking，Kuromi是第五位，2011年是第六位。

## 參看

  - [Hello Kitty](../Page/Hello_Kitty.md "wikilink")
  - [My Melody](../Page/My_Melody.md "wikilink")
  - [Sanrio](../Page/Sanrio.md "wikilink")
  - [Pom Pom Purin](../Page/Pom_Pom_Purin.md "wikilink")
  - [Bad Badtz-maru](../Page/Bad_Badtz-maru.md "wikilink")

## 外部網站

  - [台灣三麗鷗官方網站 -
    三麗鷗明星家族Kuromi](https://web.archive.org/web/20070601162151/http://www.sanrio.com.tw/html/2_star/1_detail.php?ID=21)
  - [Kuromi官方網站（日語）](https://web.archive.org/web/20071104061513/http://www.sanrio.co.jp/characters/detail/kuromi/index.html)
  - [奇幻魔法Melody官方網站 -
    第一輯（日語）](http://www.tv-osaka.co.jp/mymelo/index.html)
  - [奇幻魔法Melody官方網站 -
    第二輯（日語）](http://www.tv-osaka.co.jp/mymelo_kurukuru/)
  - [奇幻魔法Melody官方網站 -
    第三輯（日語）](http://www.tv-osaka.co.jp/animelobby/story_chara_m.html)
  - [奇幻魔法Melody官方網站 -
    第四輯（日語）](http://www.tv-osaka.co.jp/ip4/mymelo_kirara/)

[Category:三麗鷗角色](../Category/三麗鷗角色.md "wikilink")
[Category:虛構兔子](../Category/虛構兔子.md "wikilink")