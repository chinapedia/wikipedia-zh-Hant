**《L.A.刺青客》（***LA
Ink***）**是一個[真人秀的節目](../Page/真人秀.md "wikilink")，內容取材於一間位在[加州](../Page/加州.md "wikilink")[洛杉磯的](../Page/洛杉磯.md "wikilink")「High
Voltage
Tattoo」刺青店。此節目為《[邁阿密刺青客](../Page/邁阿密刺青客.md "wikilink")》的延伸，於2007年8月7日在美國首播\[1\]。

第二季已於2008年1月8日開始播映。

## 歷史

此刺青店由刺青師[凱特·方迪經營](../Page/凱特·方迪.md "wikilink")，先前她曾於《[邁阿密刺青客](../Page/邁阿密刺青客.md "wikilink")》節目中擔任其中一位刺青師，之後因與[艾米·詹姆士有了爭吵後便離開了](../Page/艾米·詹姆士.md "wikilink")\[2\]，轉而回到洛杉磯去開創自己的刺青店並主持自己的節目。

此店的刺青師的陣容為：[柯瑞·米勒](../Page/柯瑞·米勒.md "wikilink")（Corey
Miller）、[漢娜·艾琪生](../Page/漢娜·艾琪生.md "wikilink")（Hannah
Aitchison）、[金姆·賽](../Page/金姆·賽.md "wikilink")（Kim
Saigh）和店經理安柏·[彼西](../Page/彼西.md "wikilink")·艾希亞。

每一集的內容主要呈現每個顧客背後帶有什麼樣的故事，而這些故事又如何影響他們選擇刺青的樣式。由於刺青店就位在[好萊塢附近](../Page/好萊塢.md "wikilink")，所以常常會有名人出現。有時節目也會將重點放在這些刺青師的個人生活上，以及女刺青師如何在這男性主導的職業中拓展勢力。

## 另見

  - [邁阿密刺青客](../Page/邁阿密刺青客.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [台灣Discovery旅遊生活頻道網站的節目介紹](https://web.archive.org/web/20080109081530/http://www.travelandliving.com.tw/asia/LAInk.htm)
  - [High Voltage Tattoo官方網站](http://highvoltagetattoo.com)
  - [Google地圖](http://maps.google.com/maps?q=1259+N+La+Brea+Ave,+Los+Angeles,+CA+90038,+USA&sa=X&oi=map&ct=image)
  - [LA Ink @
    TLC.com](https://web.archive.org/web/20070701170115/http://tlc.discovery.com/tv/la-ink/la-ink.html)
  - [L.A.刺青客部落格](https://web.archive.org/web/20180617101642/http://miamiink.contentquake.com/)
  - [凱特·方迪官方網站](http://www.katvond.net)
  - [金姆·賽官方網站](http://www.kimsaigh.com)

[Category:真人秀節目](../Category/真人秀節目.md "wikilink")
[Category:美國電視節目](../Category/美國電視節目.md "wikilink")

1.
2.