[LBmedium.JPG](https://zh.wikipedia.org/wiki/File:LBmedium.JPG "fig:LBmedium.JPG")
**LB培养基**（****，LB），是[微生物學實驗中最常用的](../Page/微生物學.md "wikilink")[營養性](../Page/營養.md "wikilink")[培養基](../Page/培養基.md "wikilink")，用於培養[大腸桿菌等](../Page/大腸桿菌.md "wikilink")[細菌](../Page/細菌.md "wikilink")，分為液態培养基和加入[瓊脂製成的固態培養基](../Page/瓊脂.md "wikilink")。加入抗生素的
LB 培養基可用於篩選以大腸杆菌爲宿主的[選殖](../Page/選殖.md "wikilink")。

## 名稱

儘管該培養基的名稱被廣泛解釋爲**Luria-Bertani 培養基**，然而根據其發明人（Giuseppe
Bertani）的說法，這個名字來源於英語的 lysogeny
broth，即**溶菌肉湯**\[1\]。

## 配方

**LB
培養基**的配方發表於1951年有關的論文中。用於使[志賀氏菌屬的細菌增長](../Page/志賀氏菌屬.md "wikilink")\[2\]\[3\]。

LB培養基在1950年代曾被用作培養[大腸桿菌的業界標準配方](../Page/大腸桿菌.md "wikilink")\[4\]\[5\]\[6\]\[7\]\[8\]。它還被廣泛應用於分子微生物學以及[質體](../Page/質體.md "wikilink")[DNA的製備上](../Page/DNA.md "wikilink")，是重組大腸桿菌DNA時最常用的培養基\[9\]。但LB培養基不鼓勵應用於生理學研究上\[10\]。

每個LB培養基的配方不盡相同，但大概都會包含下列幾項：

一升水中加入：

  - [胰蛋白腖](../Page/胰蛋白腖.md "wikilink")（Tryptone）10[克](../Page/克.md "wikilink")
  - [酵母粉](../Page/酵母粉.md "wikilink")（Yeast extract）5克
  - [氯化鈉](../Page/氯化鈉.md "wikilink")10克
  - [維生素](../Page/維生素.md "wikilink")（包含維生素B群）
  - 微量元素（包含[氮](../Page/氮.md "wikilink")、[硫](../Page/硫.md "wikilink")、[鎂等等](../Page/鎂.md "wikilink")）
  - 礦物質

1951年的原始論文中，貝爾塔尼在1公升的水中加入了10克的氯化鈉、1克的[葡萄糖](../Page/葡萄糖.md "wikilink")\[11\]，但之後便剔除掉了葡萄糖。

[氯化鈉的量經常隨細菌的生長需求不同而有所改變](../Page/氯化鈉.md "wikilink")，像鹽分較低的Lennox和Luria培養基就適用於對於鹽分較敏感的[抗生素實驗上](../Page/抗生素.md "wikilink")\[12\]。

  - LB-Miller (10 g/L NaCl)
  - LB-Lennox (5 g/L NaCl)
  - LB-Luria (0.5 g/L NaCl)

## 製備方法

[Laboratoorne_sööde_"LB"_ehk_"lysogeny_broth"_on_sobiv_bakterite_kasvatamiseks..JPG](https://zh.wikipedia.org/wiki/File:Laboratoorne_sööde_"LB"_ehk_"lysogeny_broth"_on_sobiv_bakterite_kasvatamiseks..JPG "fig:Laboratoorne_sööde_\"LB\"_ehk_\"lysogeny_broth\"_on_sobiv_bakterite_kasvatamiseks..JPG")
加[水至](../Page/水.md "wikilink")1[L并調](../Page/升.md "wikilink")[pH值至](../Page/pH值.md "wikilink")7.0後[高溫滅菌](../Page/高溫滅菌.md "wikilink")。如需要製成固態培養基，在調pH值後加入1.5%（质量体积比，即每升培养基中加入15克）的[瓊脂再滅菌](../Page/瓊脂.md "wikilink")。

  - 將上述成分溶於800 ml的[去離子水中](../Page/Deionized_water.md "wikilink")。
  - 加入[蒸餾水或去離子水至一公升](../Page/Distilled_water.md "wikilink")。
  - 用[高壓釜以](../Page/高壓釜.md "wikilink") 121 °C 滅菌20分鐘。
  - 冷卻後即可使用

## 參見

  - [大腸桿菌](../Page/大腸桿菌.md "wikilink")
  - [瓊脂平板](../Page/瓊脂平板.md "wikilink")
  - [薩爾瓦多·盧瑞亞](../Page/薩爾瓦多·盧瑞亞.md "wikilink")

## 參考文獻

[Category:微生物培养基](../Category/微生物培养基.md "wikilink")

1.

2.
3.  Bertani, G. (1951). Studies on lysogenesis. I. The mode of phage
    liberation by lysogenic Escherichia coli. J. Bacteriol. 62:293-300.
    PMID 14888646
    [PDF](http://www.pubmedcentral.nih.gov/picrender.fcgi?artid=386127&blobtype=pdf)

4.
5.
6.
7.
8.
9.
10. Nikaido, H. (2009). The Limitations of LB Medium. Small things
    considered - The Microbe Blog. ASM.
    <http://schaechter.asmblog.org/schaechter/2009/11/the-limitations-of-lb-medium.html>

11.
12.  <http://www.exptec.com/Reagents/Common%20Media.htm>