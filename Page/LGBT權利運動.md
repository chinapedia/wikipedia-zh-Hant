**LGBT權利運動**（又稱**同志權利運動**、**同志社會運動**）是指一批鬆散結合的[公民權利團體](../Page/公民權利.md "wikilink")、[人權團體](../Page/人權.md "wikilink")、支持[LGBT權利的團體](../Page/LGBT權利.md "wikilink")、政治行動者或[社會運動人士](../Page/社會運動.md "wikilink")，以追求性別少數群體（包括[女同性戀](../Page/女同性戀.md "wikilink")、[男同性戀](../Page/男同性戀.md "wikilink")、[雙性戀](../Page/雙性戀.md "wikilink")、[跨性別](../Page/跨性別.md "wikilink")，或[雙性人](../Page/雙性人.md "wikilink")）達到，以及獲得法律上的基本[權利為目標](../Page/權利.md "wikilink")。參与者不一定為該群體，也包括[順性別以及](../Page/順性別.md "wikilink")[異性戀者](../Page/異性戀.md "wikilink")。

## 概述

LGBT權利運動組織來自於包括個人、以及宗教與政治團體。在華語文世界，也用「[同志](../Page/同志_\(LGBT\).md "wikilink")」一詞來稱呼。該詞最初指涉[同性戀](../Page/同性戀.md "wikilink")，後來有時候也會含括[雙性戀](../Page/雙性戀.md "wikilink")、[跨性別在內](../Page/跨性別.md "wikilink")。

即便LGBT群體對權利運動內涵、[信念有不同想像而難以概括與歸納](../Page/信念.md "wikilink")，不過通常參与成員相信：

  - 一個人的[性別](../Page/多元性別.md "wikilink")（包含[心理性別](../Page/心理性別.md "wikilink")）、[性別氣質和](../Page/性別氣質.md "wikilink")[性傾向為個人與生俱來不可分離的內在特質](../Page/性傾向.md "wikilink")，反對基於[性別偏見所產生的](../Page/性別偏見.md "wikilink")[歧視行為](../Page/歧視.md "wikilink")。
  - 無論一個人的[性別為何](../Page/多元性別.md "wikilink")，都應當在[法律中享有平等的權利與價值](../Page/法律.md "wikilink")。
  - 反對社會對同志的非理性恐懼、[偏見和](../Page/偏見.md "wikilink")[仇恨](../Page/仇恨.md "wikilink")，不單只是對同志，對整個社會來說也是危險的。
  - 反對負面的描繪同志的形象（意即將同志視為一種罪惡或變態）是不恰當、受到誤導、或甚至完全就是惡意的。
  - [性別是自然和天生的](../Page/多元性別.md "wikilink")，並非一種選擇，是無法改變的。
  - 反對企圖嘗試改變一個人對[性別的認同](../Page/多元性別.md "wikilink")，如果不是危險的企圖，那頂多也只是一種誤導（如[走出埃及條目與](../Page/走出埃及.md "wikilink")[補償療法等](../Page/補償療法.md "wikilink")）。

最先的一次同性恋权利运动发生在[二战前的](../Page/二战.md "wikilink")[德国](../Page/德国.md "wikilink")，以Magnus
Hirschfeld为中心。这次同性恋权利运动完全被[阿道夫·希特勒的](../Page/阿道夫·希特勒.md "wikilink")[纳粹黨政權镇压](../Page/纳粹黨.md "wikilink")（参看[纳粹德国的同性恋者](../Page/纳粹德国的同性恋者.md "wikilink")）。

## 與性解放的關連

「性解放」一詞有時會被理解成「追求性慾滿足」，而引起眾多批評。然而，解放(liberation)的原意是指從[壓迫中獲得解放](../Page/壓迫.md "wikilink")，破除壓迫而獲得[自由](../Page/自由.md "wikilink")，因此性解放是在追求對於自我性實踐掌控的自由、破除對於性的壓抑(repress)。

簡言之，性解放或可被視為一種對於性的[自主權](../Page/自主權.md "wikilink")（但有些人也可能在此種理解外，再加以擴張對於性解放的定義和行動）。

[西方世界的同志運動之所以曾與美國](../Page/西方世界.md "wikilink")1960年代的[性解放運動有過些許關連](../Page/性解放.md "wikilink")，是因為在「[歐美歷史](../Page/歐美.md "wikilink")」上，同性之間的情慾遭受壓抑、排斥以及刑罰管制，另外也與[女性主義中的](../Page/女性主義.md "wikilink")[性積極女性主義流派影響有關](../Page/性積極女性主義.md "wikilink")。

不過，同志運動未必即等同於性解放運動，兩者也未必有共通目標。例如，同志社會運動可能會要求[同性婚姻的立法](../Page/同性婚姻.md "wikilink")，來達到社會平等的目標（參見[社會不平等](../Page/社會不平等.md "wikilink")），並實現[基本權利的保障](../Page/基本權利.md "wikilink")。然而有些性解放人士會將[婚姻視為一種對於性的壓抑和](../Page/婚姻.md "wikilink")[社會控制手段](../Page/社會控制.md "wikilink")，而反對同性婚姻立法。

## 與反色情的關連

由於同志運動與[婦女運動密切相關](../Page/女性主義.md "wikilink")，牽涉到中女性主義者對[色情](../Page/色情.md "wikilink")、[BDSM與](../Page/BDSM.md "wikilink")[娼妓制度的意見分歧](../Page/性剝削.md "wikilink")，並非所有同志運動者皆為[性解放論者](../Page/性解放.md "wikilink")，同志運動中的反色情論者將色情視作[性別歧視](../Page/性別歧視.md "wikilink")、[性剝削](../Page/性剝削.md "wikilink")、[性暴力與](../Page/性暴力.md "wikilink")[仇恨言論](../Page/仇恨言論.md "wikilink")。[反色情女性主義就起源於女同志社群](../Page/反色情女性主義.md "wikilink")，並引發女同志社群內部爭論，女同志運動者[安德里亞·德沃金與男同志運動者](../Page/安德里亞·德沃金.md "wikilink")皆為早期著名的[反色情運動者](../Page/反色情運動.md "wikilink")，跨性別女性主義學者兼運動者[喬勒·萊恩也在](../Page/喬勒·萊恩.md "wikilink")「色情作為性暴力」的學術研討會發表論文，主張[跨性別女性受到色情文化的傷害](../Page/跨性別女性.md "wikilink")。\[1\]

## 各地發展

### 美国

#### 性学研究

在[美国](../Page/美国.md "wikilink")，紧接着二战结束后的几年就有了一些同性恋权利运动的步伐。

在这段时间里，[阿爾弗雷德·金賽](../Page/阿爾弗雷德·金賽.md "wikilink")（Alfred
Kinsey）发表了《[人類男性性行為](../Page/金賽報告.md "wikilink")》（*Sexual
Behavior in the Human
Male*）一书，这是第一本以科学眼光研究性主题的著作。金塞通过大量的研究发表了一个惊人论断：总人口中大约有4%的人是絕對的同性恋者，直接向当时流行的观念提出了质疑。在这本著作发表以前，同性恋通常不会成为讨论作的主题，但是在它发表之后，同性恋话题开始出现在一些刊物中，甚至是像《[时代杂志](../Page/时代杂志.md "wikilink")》（*Time
Magazine*）、《[生活杂志](../Page/生活杂志.md "wikilink")》（*Life Magazine*）等中。

#### 同志社群形成

虽然同性恋话题进入了主流杂志的讨论中，但是直到1960年代以前，社会的意识或法律都没有多少的改变。直到1960年代的[性革命和民權運動](../Page/性革命.md "wikilink")，在女權及少數族裔的公民權得到進展後。这是一段很多社会领域放生巨变的时期，包括性方面。

这些著作，连同二战后开始的向城市的大量移民，同性恋社区开始在市中心形成，而同性恋者也开始意识到他们是作为社会少数族群而不是一些少数的“性别颠倒”的孤岛而存在。虽然在20世纪早期，同性恋酒吧就已经存在，但是数量仍然很小。

1950年代到60年代，随著同性恋社区的发展，同性恋酒吧的普及，以及同性恋身份认同的加深，同性恋开始对他们的作为社会“流浪者”和“犯罪者”的地位日益不满。然而，在1960年代晚期以前，他们仍然只有很少的社会力量，與女權及黑人民權組織相比。

#### 同志运动

1969年6月28日的[石墙骚乱](../Page/石墙骚乱.md "wikilink")（Stonewall
riots）被认为是现代同性恋运动的出发点。当时所有相关的秘密变化都达到了转折点。同性恋者开始大规模的组织起来要求合法的地位、社会认同和平等权利。

石墙骚动的一个結果是“[同性恋解放阵线](../Page/同性恋解放阵线.md "wikilink")”的建立（Gay Liberation
Front，GLF，建立于[纽约市](../Page/纽约市.md "wikilink")）。这个组织的“[一个同性恋者的宣言](../Page/一个同性恋者的宣言.md "wikilink")”（A
Gay
Manifesto）为刚刚形成的同性恋运动设立了目标。阵线的分支开始遍及全美。这些组织成为全球各种争取同性恋平等权利的组织创立的基础。而在這段時間，不少國家立法把同性戀除罪化，

今天，保卫同性恋者免受[憎恨](../Page/对同性恋的恐惧.md "wikilink")、[暴力和其他形式的歧视是美国同性恋权利的主要议题](../Page/同性恋打压.md "wikilink")，他们把这些描述为[人权的本质](../Page/人权.md "wikilink")。确实，在美国最有影响力的同性恋权利组织之一就称为“[人权战线](../Page/人权战线.md "wikilink")”（Human
Rights
Campaign）。其他的同性恋权利组织包括“[全国男女同性恋工作组织](../Page/全国男女同性恋工作组织.md "wikilink")”（National
Gay and Lesbian Task
force，NGLTF），“[同性恋的双亲和朋友](../Page/同性恋的双亲和朋友.md "wikilink")”（Parents
and Friends of Lesbians and
Gays，PFLAG）和“[反诽谤男女同性恋联盟](../Page/反诽谤男女同性恋联盟.md "wikilink")”（Gay
and Lesbian Alliance Against Defamation，GLAAD）。

同性恋權利运动在1990年代後，在罪一些地区開始取得成效。[鸡奸法在美国很多州都于](../Page/鸡奸法.md "wikilink")20世纪被取消或推翻（2003年的[劳伦斯对决德克萨斯州的案件中](../Page/劳伦斯对决德克萨斯州.md "wikilink")，所有的鸡奸法都被判定违宪的，至此美国才正式同性恋除罪化，在法律上享有平等地位）。很多公司和地方政府都在他们的非歧视规定中增加禁止基于[性取向的歧视](../Page/性取向.md "wikilink")。在美国一些地区，对同性恋的暴力被视为是[仇恨罪行](../Page/仇恨罪行.md "wikilink")，并会受到严厉的惩罚。

美国的[佛蒙特州在](../Page/佛蒙特州.md "wikilink")1990年代後期为婚姻提供了另一种选择：[民事结合](../Page/民事结合.md "wikilink")（civil
union）。同性恋者在全國一些地区可以领养子女，虽然要經過繁複的程序。

2013年6月26日，[美國最高法院宣判](../Page/美國最高法院.md "wikilink")[捍衛婚姻法案第三章違憲](../Page/捍衛婚姻法案.md "wikilink")。

2015年6月26日，[美國最高法院承認並使](../Page/美國最高法院.md "wikilink")[全国同性婚姻合法化](../Page/美國同性婚姻.md "wikilink")。

在文化方面，相似的变化正在进行。正面和写实的同性恋者角色，在[电视和](../Page/电视.md "wikilink")[电影上开始增多](../Page/电影.md "wikilink")，不少製作亦加入同性恋者的元素，如[摩登家庭](../Page/摩登家庭.md "wikilink")。

#### 宗教反对势力的发展

反对同性恋权利运动发展的主要阻力通常是主流[基督教](../Page/基督教.md "wikilink")[福音派](../Page/福音派.md "wikilink")[教會](../Page/教會.md "wikilink")、[天主教](../Page/天主教.md "wikilink")、[摩門教和其他社会保守派人士](../Page/摩門教.md "wikilink")，自1980年代开始反对同性恋权利运动。反对同性恋权利运动与社会[世俗主义快速发展相对应的](../Page/世俗主义.md "wikilink")，是被部分人称为[第四次大覺醒的宗教运动](../Page/第四次大覺醒.md "wikilink")。一些学者，特别是历史经济学家[罗伯特·福格尔](../Page/罗伯特·福格尔.md "wikilink")（英文：Robert
Fogel，1993年[诺贝尔经济学奖获得者](../Page/诺贝尔经济学奖.md "wikilink")）认为其发生于1960年代到70年代早期。其概念有争议，许多历史学家认为，这些年里面在美国发生的宗教变化没有达到前三次[大覺醒運動的等级](../Page/大覺醒運動.md "wikilink")。因此第四次大觉醒本身没有很广泛的被接受。\[2\]

无论是否构成一次觉醒，变化确实发生了。[主流新教徒](http://en.wikipedia.org/wiki/Mainline_\(Protestant\))（Mainline
Protestant）在成员和影响力方面大大降低，而最保守的[福音派会员](../Page/福音派.md "wikilink")（如[美南浸信会](../Page/美南浸信会.md "wikilink")（Southern
Baptist），和[密苏里路德宗大会](http://en.wikipedia.org/wiki/Lutheran_Church%E2%80%93Missouri_Synod)（Missouri
Synod
Lutherans））数量上迅速的增长，在全美发展开来。宗教内部发生了很严重的神学斗争及分裂，并形成政治力量。[福音神学](../Page/福音神学.md "wikilink")（Evangelicalism）和[原教旨主义](../Page/原教旨主义.md "wikilink")（Fundamentalism）的[基督教保守派系也在快速的增加](../Page/基督教.md "wikilink")。同时[世俗主义](../Page/世俗主义.md "wikilink")（Secularism）引人注目的迅速增长（即使在教會內亦然），不少[保守基督教会](http://en.wikipedia.org/wiki/Conservative_Christianity)发现他们在[同性恋权利](../Page/同性恋权利.md "wikilink")，[堕胎及](../Page/堕胎.md "wikilink")[创世论等主题上与之进行斗争](../Page/创世论.md "wikilink")。\[3\]\[4\]

#### 生活中的反歧视

在美国，在全国[私营企业中](../Page/私营企业.md "wikilink")，没有防止雇员因为[性取向受到就业歧视的联邦法律](../Page/性取向.md "wikilink")。但是，目前大多数美国的跨国大企業（例如IBM、微软、福特汽车、可口可乐、波音、迪士尼、AOL等），都制定了公司内部的反歧视政策，适用于公司内部的就业、福利、升职、工作环境和公司文化、公司决策等各方面。并且，在22个州，包括[哥伦比亚特区](../Page/哥伦比亚特区.md "wikilink")，以及超过140个城市中都有禁止歧视的禁令。颁布在私营企业中禁止基于性取向歧视的有[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")、[康涅狄格州](../Page/康涅狄格州.md "wikilink")、[夏威夷州](../Page/夏威夷州.md "wikilink")、[马里兰州](../Page/马里兰州.md "wikilink")、[马萨诸塞州](../Page/马萨诸塞州.md "wikilink")、[明尼苏达州](../Page/明尼苏达州.md "wikilink")、[内华达州](../Page/内华达州.md "wikilink")、[新罕布什尔州](../Page/新罕布什尔州.md "wikilink")、[新泽西州](../Page/新泽西州.md "wikilink")、[新墨西哥州](../Page/新墨西哥州.md "wikilink")、[纽约州](../Page/纽约州.md "wikilink")、[罗得岛州](../Page/罗得岛州.md "wikilink")、[佛蒙特州和](../Page/佛蒙特州.md "wikilink")[威斯康星州](../Page/威斯康星州.md "wikilink")（\[5\]）。这些禁令也包含了很多的其他方面，例如住房或公共设施。

一个禁止全美公共范围内反同性恋就业歧视的议案已经交给[美国国会讨论](../Page/美国国会.md "wikilink")，也就是所谓的《[就业非歧视法案](../Page/就业非歧视法案.md "wikilink")》（Employment
Nondiscrimination
Act，ENDA）。2013年11月7日，該法案以64-32票參議院獲支持通過，當中獲部分共和黨參議員支持，其後獲美國總統奧巴馬簽署生效，但法案沒有限制私人企業。

1998年3月4日，[美国最高法院在对](../Page/美国最高法院.md "wikilink")*Oncale v. Sundowner
Offshore
Services*的判决中表示，联邦法律对发生在工作中的[性骚扰](../Page/性骚扰.md "wikilink")，无论双方是异性还是同性同样适用。但是，较低级的法院关于这个判决是否适用于有反同性恋意图的骚扰仍存在分歧。

### 加拿大

加拿大在1969年將同性戀除罪化，此後LGBT權利運動開始發展。2003年[加拿大的](../Page/加拿大.md "wikilink")[魁北克省和](../Page/魁北克.md "wikilink")[新斯科舍省](../Page/新斯科舍省.md "wikilink")、[安大略省承认两个同性间的](../Page/安大略省.md "wikilink")[共同法婚姻](../Page/共同法婚姻.md "wikilink")（common-law
marriages）。[安大略省法院和魁北克最高法院要求联邦政府在未来两年内为同性恋者提供完全的权利](../Page/安大略省.md "wikilink")。2005年加拿大正式承認[全国同性婚姻](../Page/加拿大同性婚姻.md "wikilink")。至此，加拿大成為首個同性婚姻合法化的美洲國家。

### 歐洲

[欧洲的一些国家早在](../Page/欧洲.md "wikilink")1960年代已把同性戀除罪化，並發展LGBT權利運動，[歐洲LGBT權益普遍較世界其他地區更進步](../Page/歐洲LGBT權益.md "wikilink")。

1989年，[丹麥成為首個允許](../Page/丹麥.md "wikilink")[民事结合的國家](../Page/民事结合.md "wikilink")。1999年，法國允許[民事结合](../Page/民事结合.md "wikilink")。2001年，[荷兰成為首個同性婚姻合法化的國家](../Page/荷兰.md "wikilink")。此後[比利时於](../Page/比利时.md "wikilink")2003年允许[同性婚姻](../Page/同性婚姻.md "wikilink")；[西班牙於](../Page/西班牙.md "wikilink")2005年允许[同性婚姻](../Page/同性婚姻.md "wikilink")。此後，[葡萄牙](../Page/葡萄牙.md "wikilink")、[冰島](../Page/冰島.md "wikilink")、[英國](../Page/英國.md "wikilink")、[法國](../Page/法國.md "wikilink")、[挪威](../Page/挪威.md "wikilink")、[瑞典](../Page/瑞典.md "wikilink")、[愛爾蘭](../Page/愛爾蘭.md "wikilink")、[丹麥](../Page/丹麥.md "wikilink")、[奧地利等亦允许](../Page/奧地利.md "wikilink")[同性婚姻](../Page/同性婚姻.md "wikilink")。[瑞士](../Page/瑞士.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[希臘則允许](../Page/希臘.md "wikilink")[民事结合](../Page/民事结合.md "wikilink")。

### 台灣

台灣對於同志群體相對寬容，同志權利運動1990年代開始發展。一些同志團體及支持者在2012年聯署提交[多元成家立法草案](../Page/多元成家立法草案.md "wikilink")，該草案僅婚姻平權法案得到特定立委支持，伴侶法與家屬法皆因立委連署支持不夠而未能進入立法院，婚姻平權法案因被[國民黨主控的立法院退回而無法交付司法委員會](../Page/國民黨.md "wikilink")，擱置在[立法院未有進展](../Page/立法院.md "wikilink")。2016年[民進黨為立法院多數後](../Page/民進黨.md "wikilink")，婚姻平權法案重新提上議程，並經立法院一讀交付司法委員會審查。

### 香港

[香港在](../Page/香港.md "wikilink")1991年把同性戀非刑事化，此後LGBT權利運動開始發展。早在1990年代開始討論[性傾向歧視條例](../Page/性傾向歧視條例.md "wikilink")，但因社會的主流意見普遍反對而一直無進行公眾諮詢，而[香港政府亦不大重視相關的議題](../Page/香港政府.md "wikilink")。

香港一些基督教保守派團體一直反對就[性傾向歧視條例進行任何諮詢和](../Page/性傾向歧視條例.md "wikilink")[立法](../Page/立法.md "wikilink")。2013年在[香港特別行政區行政長官](../Page/香港特別行政區行政長官.md "wikilink")[梁振英發表](../Page/梁振英.md "wikilink")[2013年度香港行政長官施政報告前夕](../Page/2013年度香港行政長官施政報告.md "wikilink")，於[添馬艦政府總部的](../Page/添馬艦政府總部.md "wikilink")[添馬公園集會](../Page/添馬公園.md "wikilink")，反對就[性傾向歧視條例進行任何諮詢和](../Page/性傾向歧視條例.md "wikilink")[立法](../Page/立法.md "wikilink")。他們擔心立法會造成[逆向歧視](../Page/逆向歧視.md "wikilink")，並會限制香港人的[言論自由](../Page/言論自由.md "wikilink")。施政報告最終表明沒有計劃立法。

香港首位同志[立法會議員](../Page/立法會議員.md "wikilink")[陳志全及其他香港同志團體認為](../Page/陳志全.md "wikilink")，香港政府不必諮詢，應該立即通過有關保護同志權利的香港法例，甚至接受[同性婚姻](../Page/同性婚姻.md "wikilink")。而特區政府方面，[政制及內地事務局](../Page/政制及內地事務局.md "wikilink")[譚志源表示](../Page/譚志源.md "wikilink")，同志平權不可「一步登天」，因為社會未有共識。\[6\]

## 参考文献

<references/>

## 外部連結

  - [Norris v.
    Ireland](http://hudoc.echr.coe.int/hudoc/ViewRoot.asp?Item=0&Action=Html&X=501232531&Notice=0&Noticemode=&RelatedMode=0)
    (European Court of Human Rights case law)
  - [Report of the European Commission Of Human Rights on the case of
    Sutherland v. the United Kingdom (Application 25186/94), 1
    July 1997](http://hudoc.echr.coe.int/Hudoc2doc/herep/sift/555.txt)
  - [LookSmart - GLB
    Rights](https://web.archive.org/web/20041012064015/http://search.looksmart.com/p/browse/us1/us317836/us317916/us53716/us74414/us10022019/us74480)
  - [Open Directory Project - GLB
    Issues](http://dmoz.org/Society/Issues/Gay,_Lesbian,_and_Bisexual/)
  - [Yahoo\! - GLB Politics and Civil
    Rights](https://web.archive.org/web/20041010092521/http://dir.yahoo.com/Society_and_Culture/Cultures_and_Groups/Lesbians__Gays__and_Bisexuals/Politics_and_Civil_Rights/)

{{-}}

[LGBT權利運動](../Category/LGBT權利運動.md "wikilink")
[Category:LGBT權利](../Category/LGBT權利.md "wikilink")
[Category:LGBT歷史](../Category/LGBT歷史.md "wikilink")

1.
2.  Robert William Fogel (2000), *The Fourth Great Awakening & the
    Future of Egalitarianism*; see the review by Randall Balmer,
    *Journal of Interdisciplinary History* 2002 33(2): 322-325
3.  [William G. McLoughlin](../Page/William_G._McLoughlin.md "wikilink")
    (1978), *Revivals, Awakenings and Reform: An Essay on Religion and
    Social Change in America, 1607-1977*
4.  Randall Balmer (2001), *Religion in Twentieth Century America*
5.  [Ata State](http://www.hrc.org/worknet/nd/states_ban_dso.asp)
6.  [1](http://www.metroradio.com.hk/997/News/Default.aspx?NewsID=20130113202455)
    新城電台新聞