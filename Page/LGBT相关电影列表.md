涉及[同性恋](../Page/同性恋.md "wikilink")、[雙性戀與](../Page/雙性戀.md "wikilink")／或[跨性別话题的](../Page/跨性別.md "wikilink")[电影](../Page/电影.md "wikilink")，塑造了重要的同志[角色和](../Page/角色.md "wikilink")／或把同性恋、雙性戀以及／或跨性別身份或关系作为一个重要的剧情的有：

## 華語電影

<table>
<thead>
<tr class="header">
<th><p>名</p></th>
<th><p>出版地</p></th>
<th><p>出版年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/爱奴.md" title="wikilink">爱奴</a></p></td>
<td><p>香港</p></td>
<td><p>1972</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/爱奴新传.md" title="wikilink">爱奴新传</a></p></td>
<td><p>香港</p></td>
<td><p>1984</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/孽子.md" title="wikilink">孽子</a></p></td>
<td><p>臺灣</p></td>
<td><p>1986</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雙鐲.md" title="wikilink">雙鐲</a></p></td>
<td><p>香港</p></td>
<td><p>1989</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/喜宴_(電影).md" title="wikilink">喜宴</a></p></td>
<td><p>臺灣</p></td>
<td><p>1992</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/霸王別姬_(電影).md" title="wikilink">霸王別姬</a></p></td>
<td><p>中國大陸</p></td>
<td><p>1992</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/青少年哪吒.md" title="wikilink">青少年哪吒</a></p></td>
<td><p>臺灣</p></td>
<td><p>1993</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/三個相愛的少年.md" title="wikilink">三個相愛的少年</a></p></td>
<td><p>香港</p></td>
<td><p>1994</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛情萬歲_(臺灣電影).md" title="wikilink">愛情萬歲</a></p></td>
<td><p>臺灣</p></td>
<td><p>1994</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/假男假女.md" title="wikilink">假男假女</a></p></td>
<td><p>香港</p></td>
<td><p>1996</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/東宮西宮_(電影).md" title="wikilink">東宮西宮</a></p></td>
<td><p>中国大陆</p></td>
<td><p>1996</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/基佬四十.md" title="wikilink">基佬四十</a></p></td>
<td><p>香港</p></td>
<td><p>1997</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/春光乍洩.md" title="wikilink">春光乍洩</a></p></td>
<td><p>香港</p></td>
<td><p>1997</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/自梳_(電影).md" title="wikilink">自梳</a></p></td>
<td><p>香港</p></td>
<td><p>1997</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/河流_(电影).md" title="wikilink">河流</a></p></td>
<td><p>臺灣</p></td>
<td><p>1997</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/史丹利_(電影).md" title="wikilink">史丹利</a>（短片）</p></td>
<td><p>香港</p></td>
<td><p>1998</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/美少年之戀.md" title="wikilink">美少年之戀</a></p></td>
<td><p>香港</p></td>
<td><p>1998</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/愈快樂愈墮落.md" title="wikilink">愈快樂愈墮落</a></p></td>
<td><p>香港</p></td>
<td><p>1998</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/古惑仔情義篇之洪興十三妹.md" title="wikilink">古惑仔情義篇之洪興十三妹</a></p></td>
<td><p>香港</p></td>
<td><p>1998</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/心動.md" title="wikilink">心動</a></p></td>
<td><p>香港</p></td>
<td><p>1999</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/心猿意馬.md" title="wikilink">心猿意馬</a></p></td>
<td><p>香港</p></td>
<td><p>1999</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/心灰.md" title="wikilink">心灰</a>（短片）</p></td>
<td><p>香港</p></td>
<td><p>1999</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/美麗少年.md" title="wikilink">美麗少年</a>（<em>Boys For Beauty</em>）</p></td>
<td><p>臺灣</p></td>
<td><p>1999</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/男男女女.md" title="wikilink">男男女女</a></p></td>
<td><p>中國大陸</p></td>
<td><p>1999</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夜奔_(电影).md" title="wikilink">夜奔</a></p></td>
<td><p>臺灣</p></td>
<td><p>2000</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/江湖告急.md" title="wikilink">江湖告急</a></p></td>
<td><p>香港</p></td>
<td><p>2000</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/藍宇.md" title="wikilink">藍宇</a></p></td>
<td><p>中國大陸</p></td>
<td><p>2001</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/今年夏天.md" title="wikilink">今年夏天</a></p></td>
<td><p>中國大陸</p></td>
<td><p>2001</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/雨後的天空.md" title="wikilink">雨後的天空</a>（短片）</p></td>
<td><p>臺灣</p></td>
<td><p>2001</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/情色地圖.md" title="wikilink">情色地圖</a></p></td>
<td><p>香港</p></td>
<td><p>2001</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/遊園驚夢_(電影).md" title="wikilink">遊園驚夢</a></p></td>
<td><p>香港</p></td>
<td><p>2001</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/天使_(電影).md" title="wikilink">天使</a>（短片）</p></td>
<td><p>香港</p></td>
<td><p>2001</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/藍色大門.md" title="wikilink">藍色大門</a></p></td>
<td><p>臺灣</p></td>
<td><p>2002</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/跳格子(短片).md" title="wikilink">跳飛機</a>（短片）</p></td>
<td><p>臺灣</p></td>
<td><p>2002</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鍾意冇罪.md" title="wikilink">鍾意冇罪</a></p></td>
<td><p>香港</p></td>
<td><p>2002</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/台北朝九晚五.md" title="wikilink">台北朝九晚五</a></p></td>
<td><p>臺灣</p></td>
<td><p>2002</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/妖夜迴廊.md" title="wikilink">妖夜迴廊</a></p></td>
<td><p>香港</p></td>
<td><p>2003</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/幸福備忘錄.md" title="wikilink">幸福備忘錄</a>（<em>Memorandum On Happiness</em>）</p></td>
<td><p>臺灣</p></td>
<td><p>2003</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/飛躍情海.md" title="wikilink">飛躍情海</a>（<em>Love Me,If You Can</em>）</p></td>
<td><p>臺灣</p></td>
<td><p>2003</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/十七歲的天空.md" title="wikilink">十七歲的天空</a></p></td>
<td><p>臺灣</p></td>
<td><p>2004</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/豔光四射歌舞團.md" title="wikilink">豔光四射歌舞團</a></p></td>
<td><p>臺灣</p></td>
<td><p>2004</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蝴蝶_(2004年電影).md" title="wikilink">蝴蝶</a></p></td>
<td><p>香港</p></td>
<td><p>2004</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大佬愛美麗.md" title="wikilink">大佬愛美麗</a></p></td>
<td><p>香港</p></td>
<td><p>2004</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/只愛陌生人_(電影).md" title="wikilink">只愛陌生人</a></p></td>
<td><p>香港</p></td>
<td><p>2005</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/孤戀花_(2005年電視劇).md" title="wikilink">孤戀花</a></p></td>
<td><p>臺灣</p></td>
<td><p>2005</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/人面桃花.md" title="wikilink">人面桃花</a>（<em>Beautiful Men</em>）</p></td>
<td><p>中國大陸</p></td>
<td><p>2005</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/無偶之家，往事之城.md" title="wikilink">無偶之家，往事之城</a>（<em>Scars on Memory</em>）</p></td>
<td><p>臺灣</p></td>
<td><p>2005</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/面对面_(电影).md" title="wikilink">面對面</a></p></td>
<td><p>香港</p></td>
<td><p>2006</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/植物学家的女儿.md" title="wikilink">植物学家的女儿</a></p></td>
<td><p>中国大陆</p></td>
<td><p>2005</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/六四吧－人、情、空間.md" title="wikilink">六四吧－人、情、空間</a>（<em>Club 64: Folks, Affections and Space</em>）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/盛夏光年.md" title="wikilink">盛夏光年</a></p></td>
<td><p>臺灣</p></td>
<td><p>2006</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/攣攣寶島心.md" title="wikilink">攣攣寶島心</a>（<em>Queerly Sensitive: Two Tales from Taiwan</em>）：《<a href="../Page/寫字魔法.md" title="wikilink">寫字魔法</a>》、《<a href="../Page/愛，可能.md" title="wikilink">愛，可能</a>》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/當我們同在一起.md" title="wikilink">當我們同在一起</a>（<em>GO GO G-Boys</em>）</p></td>
<td><p>臺灣</p></td>
<td><p>2006</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/無浪城市.md" title="wikilink">無浪城市</a></p></td>
<td><p>香港</p></td>
<td><p>2007</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/除却巫山.md" title="wikilink">除却巫山</a></p></td>
<td><p>中国大陆</p></td>
<td><p>2007</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/愛麗絲的鏡子.md" title="wikilink">愛麗絲的鏡子</a></p></td>
<td><p>臺灣</p></td>
<td><p>2007</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/刺青_(電影).md" title="wikilink">刺青</a></p></td>
<td><p>臺灣</p></td>
<td><p>2007</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/少年不戴花.md" title="wikilink">少年不戴花</a>（<em>花を挂けない少年</em>、<em>It Seems to Rain</em>）</p></td>
<td><p>臺灣</p></td>
<td><p>2007</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/沿海岸線徵友.md" title="wikilink">沿海岸線徵友</a></p></td>
<td><p>臺灣</p></td>
<td><p>2007</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/愛到盡.md" title="wikilink">愛到盡</a></p></td>
<td><p>香港</p></td>
<td><p>2008</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/漂浪青春.md" title="wikilink">漂浪青春</a></p></td>
<td><p>臺灣</p></td>
<td><p>2008</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/花吃了那女孩.md" title="wikilink">花吃了那女孩</a></p></td>
<td><p>臺灣</p></td>
<td><p>2008</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/艾草_(公視人生劇展).md" title="wikilink">艾草</a></p></td>
<td><p>臺灣</p></td>
<td><p>2008</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>中國</p></td>
<td><p>2009</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/春風沉醉的夜晚.md" title="wikilink">春風沉醉的夜晚</a></p></td>
<td><p>中國大陆</p></td>
<td><p>2009</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/永久居留.md" title="wikilink">永久居留</a></p></td>
<td><p>香港</p></td>
<td><p>2009</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/霓虹心.md" title="wikilink">霓虹心</a>（Miss Kicki）</p></td>
<td><p>瑞典/臺灣</p></td>
<td><p>2009</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亂青春.md" title="wikilink">亂青春</a></p></td>
<td><p>臺灣</p></td>
<td><p>2009</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/帶我去遠方.md" title="wikilink">帶我去遠方</a></p></td>
<td><p>臺灣</p></td>
<td><p>2009</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/得閒炒飯.md" title="wikilink">得閒炒飯</a>（<em>All about love</em>）</p></td>
<td><p>香港</p></td>
<td><p>2010</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/安非他命_(电影).md" title="wikilink">安非他命</a>（<em>Amphetamine</em>）</p></td>
<td><p>香港</p></td>
<td><p>2010</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小狗情人.md" title="wikilink">小狗情人</a></p></td>
<td><p>臺灣</p></td>
<td><p>2010</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/女朋友。男朋友.md" title="wikilink">女朋友。男朋友</a> (<em>GF*BF</em>)</p></td>
<td><p>臺灣</p></td>
<td><p>2012</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雙_Pair_of_Love.md" title="wikilink">雙</a>（<em>Pair of Love</em>）</p></td>
<td><p>臺灣</p></td>
<td><p>2010</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/滿月酒.md" title="wikilink">滿月酒</a> (<em>Baby Steps</em>)</p></td>
<td><p>臺灣</p></td>
<td><p>2015</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/醉·生夢死.md" title="wikilink">醉·生夢死</a> (<em>Thanatos, Drunk</em>)</p></td>
<td><p>臺灣</p></td>
<td><p>2015</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/錯了性別不錯愛_(微電影).md" title="wikilink">錯了性別不錯愛</a></p></td>
<td><p>中國大陸</p></td>
<td><p>2016</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/同流合烏.md" title="wikilink">同流合烏</a> (<em>Utopians</em>)</p></td>
<td><p>香港</p></td>
<td><p>2016</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/日常對話.md" title="wikilink">日常對話</a> (<em>Small Talk</em>)</p></td>
<td><p>臺灣</p></td>
<td><p>2017</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿莉芙.md" title="wikilink">阿莉芙</a> (<em>Alifu, the Prince/ss</em>)</p></td>
<td><p>臺灣</p></td>
<td><p>2017</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/寻找罗麦.md" title="wikilink">寻找罗麦</a> (<em>Seek McCartney</em>)</p></td>
<td><p>中國大陸</p></td>
<td><p>2018</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/紅樓夢_(2018年電影).md" title="wikilink">紅樓夢</a> (<em>The Story of the Stone</em>)</p></td>
<td><p>臺灣</p></td>
<td><p>2018</p></td>
</tr>
</tbody>
</table>

## 外語電影

__NOTOC__

### 123

  - ，美国（1997）

  - 《[你黑我未黑](../Page/你黑我未黑.md "wikilink")》／《[冰點下的幸福](../Page/冰點下的幸福.md "wikilink")》／《[最后一夜偷情](../Page/最后一夜偷情.md "wikilink")》，冰岛（2000）

  - 《[八美图](../Page/八美图.md "wikilink")》，法国（2002）

  - ，西班牙（2005）

### A

  - 《[同窗之戀](../Page/同窗之戀.md "wikilink")》，英国（1984）

  - 《[世紀的哭泣](../Page/世紀的哭泣.md "wikilink")》，美国（1993）

  - 《[沙漠妖姬](../Page/沙漠妖姬.md "wikilink")》，澳大利亚（1994年）

  - ，美国（1994）

  - ，美国（1997）

  - 《[貓屎先生](../Page/貓屎先生.md "wikilink")》／《[愛在心裡口難開](../Page/愛在心裡口難開.md "wikilink")》（[尽善尽美](../Page/尽善尽美.md "wikilink")），美国（1997）

  - ，德国（1999）

  - 《[暈基浪](../Page/暈基浪.md "wikilink")》（），美国（2001）

  - 《[不羈美少年](../Page/不羈美少年.md "wikilink")》（/），法国（2002）

  - 《[鎖不住的基情](../Page/鎖不住的基情.md "wikilink")》（），印尼（2003）

  - 《[天涯家園](../Page/天涯家園.md "wikilink")》（），美国（2004）

### B

  - ，美国（1970）

  - 《[咖啡，茶\&Tequila](../Page/咖啡，茶&Tequila.md "wikilink")》／《[瀟洒有情天](../Page/瀟洒有情天.md "wikilink")》（），美国（1995）

  - 《[蝴蝶女之吻](../Page/蝴蝶女之吻.md "wikilink")》（），英国（1995）

  - 《[美好事物](../Page/美好事物.md "wikilink")》，英国（1996）

  - 《[大膽的愛，小心的偷](../Page/大膽的愛，小心的偷.md "wikilink")》／《[驚世狂花](../Page/驚世狂花.md "wikilink")》（），美国（1996）

  - 《[假鳳虛凰](../Page/假鳳虛凰.md "wikilink")》（[鳥籠](../Page/鳥籠_\(電影\).md "wikilink")），美国（1996）

  - 《[生命中不能承受之情](../Page/生命中不能承受之情.md "wikilink")》，英国/日本（1997）

  - 《[不羁夜](../Page/不羁夜.md "wikilink")》／《[一舉成名](../Page/一舉成名.md "wikilink")》（），美国（1997）

  - 《[慾甫團](../Page/慾甫團.md "wikilink")》／《[肉譜團](../Page/肉譜團.md "wikilink")》（），加拿大/英国/法国（1999）

  - 《[攣攣少女心](../Page/攣攣少女心.md "wikilink")》／《[甜過巧克力](../Page/甜過巧克力.md "wikilink")》（），加拿大（1999）

  - 《[沒哭聲的抉擇](../Page/沒哭聲的抉擇.md "wikilink")》／《[男孩別哭](../Page/男孩別哭.md "wikilink")》，美国（1999）

  - 《[戀戀模範生](../Page/戀戀模範生.md "wikilink")》／《[啦啦队长](../Page/啦啦队长_\(电影\).md "wikilink")》（[戀戀模範生](../Page/戀戀模範生.md "wikilink")），美国（1999）

  - 《[攣攣俱樂部](../Page/攣攣俱樂部.md "wikilink")》／《[傷心同志俱樂部](../Page/傷心同志俱樂部.md "wikilink")》（），美国（2000）

  - 《[在夜幕降臨前](../Page/在夜幕降臨前.md "wikilink")》（[Before Night
    Falls](../Page/Before_Night_Falls.md "wikilink")），美国（2001）

  - 《[男式伊甸園](../Page/男式伊甸園.md "wikilink")》（），美国（2001）

  - [Bully](../Page/霸凌.md "wikilink")，美国（2001）

  - 《[4x香蕉同志](../Page/4x香蕉同志.md "wikilink")》（*Banana Queers*），香港（2003）

  - 《[美麗拳王](../Page/美麗拳王.md "wikilink")》（），泰国（2003）

  - 《[斷背山](../Page/斷背山.md "wikilink")》，加拿大/美国（2005）

  - 《[男孩的愛](../Page/男孩的愛.md "wikilink")》（），日本（2006）

  - 《[曼谷愛情故事](../Page/曼谷愛情故事.md "wikilink")》，泰國（2007）

  - 《[男國少年夢](../Page/男國少年夢.md "wikilink")》（），美国（2007）

  - 《[女愛人](../Page/女愛人.md "wikilink")》（*Bloomington*），美國（2010）

  - 《[黑天鵝](../Page/黑天鹅_\(电影\).md "wikilink")》，美国（2010）

  - 《[藍色是最溫暖的顏色](../Page/藍色是最溫暖的顏色.md "wikilink")》（*Blue Is the Warmest
    Colour*），法國（2013）

  - 《[再見，我的新郎](../Page/再見，我的新郎.md "wikilink")》（*Bridegroom*），美國（2013）

### C

  - 《[雙姝怨](../Page/雙姝怨.md "wikilink")》，美国（1961）

  - 《[歌廳](../Page/歌廳_\(電影\).md "wikilink")》（），美国（1972）

  - 《[一籠傻鳥](../Page/一籠傻鳥.md "wikilink")》，法国（1978）

  - ，美国（1980）

  - ，德国（1989）

  - 《[克萊爾](../Page/克萊爾.md "wikilink")》（），美国（1992）

  - 《[亂世浮生](../Page/亂世浮生.md "wikilink")》（[哭泣游戏](../Page/哭泣游戏.md "wikilink")），英国（1992）

  - 《[電影中的同志](../Page/電影中的同志.md "wikilink")》，美国（1996）

  - 《[愛，上了癮](../Page/愛，上了癮.md "wikilink")》／《[猜·情·尋](../Page/猜·情·尋.md "wikilink")》（）,美国（1997）

  - 《[同志們的快樂世界](../Page/同志們的快樂世界.md "wikilink")》（），美国（2000）

  - 《[迷恋](../Page/迷恋.md "wikilink")》（*Crush*）美国(2000)

  - 《[叻女夏令營](../Page/叻女夏令營.md "wikilink")》（），法国（2002）

  - 《[熊熊俱樂部](../Page/熊熊俱樂部.md "wikilink")》（），西班牙（2004）

  - 《[閉門誘惑一家親](../Page/閉門誘惑一家親.md "wikilink")》（/），法国（2005）

  - 《[-{zh-hans:疯狂; zh-tw:愛瘋狂;
    zh-hk:愛斷背;}-](../Page/愛瘋狂.md "wikilink")》（），加拿大（2005）

  - 《[斷袖男](../Page/斷袖男.md "wikilink")》／《[我愛斷背衫](../Page/我愛斷背衫.md "wikilink")》（），香港（2006）

  - 《[情碎的天空](../Page/情碎的天空.md "wikilink")》（/），墨西哥（2006）

  - 《[再見](../Page/再見_\(電影\).md "wikilink")》／《[你好，再見](../Page/你好，再見.md "wikilink")》／《[兩廂情願](../Page/兩廂情願.md "wikilink")》（[Ciao](../Page/Ciao.md "wikilink")），美國（2008）

  - ，美国（2017）

### D

  - 《[黃金萬兩](../Page/黃金萬兩.md "wikilink")》（[熱天午後](../Page/熱天午後.md "wikilink")），美国（1975）

  - ，美国（1982）

  - 《[寂寞芳心](../Page/寂寞芳心_\(電影\).md "wikilink")》／《[愛的甘露](../Page/愛的甘露.md "wikilink")》（），美国（1985）

  - 《[三角洲孽子](../Page/三角洲孽子.md "wikilink")》 （*The Delta*），越南（1996）

  - 《[巧克力男孩](../Page/巧克力男孩.md "wikilink")》（/），法国（2000）

  - 《[漂流三部曲](../Page/漂流三部曲_\(電影\).md "wikilink")》（*Drift*），加拿大（2000）

  - 《[暗夜搖籃曲](../Page/暗夜搖籃曲.md "wikilink")》（），美国（2001）

  - 《[危險人物](../Page/危險人物_\(2003年電影\).md "wikilink")》（），美国（2003）

  - 《[少女特攻隊](../Page/少女特攻隊.md "wikilink")》（），美国（2004）

  - 《[皇帝出巡記](../Page/皇帝出巡記.md "wikilink")》（*Drag Kings on
    Tour*），美国（2004）

  - 《[兄弟情人](../Page/兄弟情人.md "wikilink")》，巴西（2009）

### E

  - 《[修女愛瘋狂](../Page/修女愛瘋狂.md "wikilink")》／《[修女夜難熬](../Page/修女夜難熬.md "wikilink")》（/），西班牙（1983）
  - 《[愛德華二世](../Page/愛德華二世_\(電影\).md "wikilink")》（），英国（1991）
  - 《[17歲的邊緣](../Page/17歲的邊緣.md "wikilink")》／《[17歲的疑惑](../Page/17歲的疑惑.md "wikilink")》（），美国（1998）
  - 《[最愛還是他](../Page/最愛還是他.md "wikilink")》（/），丹麦（2001）
  - 《[開心告別派對](../Page/開心告別派對.md "wikilink")》（），加拿大/美国（2003）
  - 《[外出用餐](../Page/外出用餐.md "wikilink")》（[外出就餐1：憨直处男](../Page/外出就餐1：憨直处男.md "wikilink")），美国（2004）
  - 《[裸露媒體E.K.G.](../Page/裸露媒體E.K.G..md "wikilink")》(*E.K.G.
    Expositus*)，德國（2004）
  - 《[志同盜合](../Page/志同盜合.md "wikilink")》（），加拿大/美国（2004）
  - 《[外出用餐2：調情時刻](../Page/外出用餐2：調情時刻.md "wikilink")》（），美国（2006）
  - 《埃倫娜》（*Elena Undone*），美國（2010）

### F

  - 《[酷兒出品，必屬佳品](../Page/酷兒出品，必屬佳品.md "wikilink")》／《[豪華同志電影史](../Page/豪華同志電影史.md "wikilink")》（）

  - 《[天上人間](../Page/天上人間_\(2002年電影\).md "wikilink")》（），美国（2002）

  - 《[黑色芳心](../Page/黑色芳心.md "wikilink")》（），美国/德国（1996）

  - ，意大利/英国/捷克(2007)

  - 《[出賣高潮](../Page/出賣高潮.md "wikilink")》（*Fiona*），美国（1998）

  - 《[我心所向](../Page/我心所向.md "wikilink")》（），加拿大(1996)

  - 《[蕭公子](../Page/蕭公子.md "wikilink")》／《[A片猛男日記](../Page/A片猛男日記.md "wikilink")》（），美国（2001）

  - 《[校園古惑女](../Page/校園古惑女.md "wikilink")》／《[怒火赤子心](../Page/怒火赤子心.md "wikilink")》（），美国（1996）

  - 《[掀起我的愛](../Page/掀起我的愛.md "wikilink")》（/*Unveiled*），德国/奥地利（2005）

  - 《[筆姬別戀](../Page/筆姬別戀.md "wikilink")》／《[揮灑烈愛](../Page/揮灑烈愛.md "wikilink")》，加拿大/墨西哥/美国（2002）

  - 《[油炸綠蕃茄](../Page/油炸綠蕃茄.md "wikilink")》／《[笑傲同行](../Page/笑傲同行.md "wikilink")》（），美国（1991）

  - 《[同志家族](../Page/同志家族.md "wikilink")》（），美国（2001）

  - 《[紐約斷背衫](../Page/紐約斷背衫.md "wikilink")》（Front Cover），美國（2015）

  - 《[同窗的愛](../Page/同窗的愛.md "wikilink")》《[小鎮春情](../Page/小鎮春情.md "wikilink")》《[對面的女孩看過來](../Page/對面的女孩看過來_\(電影\).md "wikilink")》（[同窗的愛](../Page/同窗的愛.md "wikilink")/），瑞典（1998）

### G

  - 《[花園](../Page/花園_\(電影\).md "wikilink")》（），英国（1990）

  - 《[浪蕩基情70年代](../Page/浪蕩基情70年代.md "wikilink")》（），美国（2005）

  - 《[二性三人痕](../Page/二性三人痕.md "wikilink")》／《[我的心裡只有妳沒有他](../Page/我的心裡只有妳沒有他.md "wikilink")》（/），法国（1995）

  - 《[愛的初體驗](../Page/愛的初體驗.md "wikilink")》（），英国（1999）

  - 《[霓裳情挑](../Page/霓裳情挑.md "wikilink")》，美国（1998）

  - 《[叻女釣魚記趣](../Page/叻女釣魚記趣.md "wikilink")》（），美国（1994）

  - 《[魂斷夢工場](../Page/魂斷夢工場.md "wikilink")》（），英国（1998）

  - 《[御法度](../Page/御法度.md "wikilink")》（[Gohatto](../Page/御法度.md "wikilink")/[Taboo](../Page/禁忌.md "wikilink")），日本（1999）

  - ，美国（2003）

  - 《[乾柴烈火](../Page/乾柴烈火.md "wikilink")》（/），法国（2000）

  - 《[男生愛的學府](../Page/男生愛的學府.md "wikilink")》／《[顛覆性學校](../Page/顛覆性學校.md "wikilink")》（/），法国（2004）

### H

  - 《[千年血后](../Page/千年血后.md "wikilink")》（[血魔](../Page/血魔.md "wikilink")），英国（1983）
  - 《[罪孽天使](../Page/罪孽天使.md "wikilink")》／《[夢幻天堂](../Page/夢幻天堂（电影）.md "wikilink")》（），新西兰（1994）
  - 《[男慾天堂](../Page/男慾天堂.md "wikilink")》（/），意大利/土耳其/西班牙（1997）
  - 《[绞死人的花园](../Page/绞死人的花园.md "wikilink")》（），英国/加拿大（1997）
  - [Head On](../Page/窗外有男天.md "wikilink")，澳大利亚（1998）
  - 《[粉身碎骨](../Page/粉身碎骨.md "wikilink")》／《[高潮艺术](../Page/高潮艺术.md "wikilink")》／《[高档货](../Page/高档货.md "wikilink")》（），美国（1998）
  - 《[妖型樂與怒](../Page/妖型樂與怒.md "wikilink")》（），美国（2001）
  - 《[冬蔭鐵娘子](../Page/冬蔭鐵娘子.md "wikilink")》（/），泰国（2003）

### I

  - 《[雙姝奇戀](../Page/雙姝奇戀.md "wikilink")》／《[遇上100%的女孩](../Page/遇上100%的女孩.md "wikilink")》（），美国（1995）

  - ，美国（1996）

  - 《[忽然囉囉攣](../Page/忽然囉囉攣.md "wikilink")》（），美国（1997）

  - 《[你的生命,
    我的決定2](../Page/你的生命,_我的決定2.md "wikilink")》／《[愛妳鍾情](../Page/愛妳鍾情.md "wikilink")》（），美国（2000）

  - 《[新娘向後跑](../Page/新娘向後跑.md "wikilink")》（），英國（2005）

  - 《[同心難改](../Page/同心難改.md "wikilink")》，美国/英國/印度（2007）

  - 《[基志雙雄](../Page/基志雙雄.md "wikilink")》（[娘子漢大丈夫](../Page/娘子漢大丈夫.md "wikilink")），美国/法国（2009）

### J

  - ，美国（1995）

  - 《[活得比你好](../Page/活得比你好.md "wikilink")》（），美国（2001）

### K

  - ，美国（1997）

  - 《[少男心事](../Page/少男心事.md "wikilink")》（/），西班牙（2000）

  - 《[-{zh-hk:眾裡尋他遇上她;zh-hans:亲亲杰西卡;zh-tw:誰吻了潔西卡;}-](../Page/眾裡尋他遇上她.md "wikilink")》（），美国（2002）

  - 《[金賽性學教室](../Page/金賽性學教室.md "wikilink")》／《[引人入性](../Page/引人入性.md "wikilink")》，美国（2004）

  - 《[靚佬搵靴著](../Page/靚佬搵靴著.md "wikilink")》，英国/美国（2005）

  - 《[吻兩下打兩槍](../Page/吻兩下打兩槍.md "wikilink")》，美國（2005）

  - 《[非單親關係](../Page/非單親關係.md "wikilink")》，美國（2010）

  - 《[吻我](../Page/吻我.md "wikilink")》（*Kyss Mig*），瑞典（2011）

### L

  - 《[英倫末路](../Page/英倫末路.md "wikilink")》（），英国（1987）

  - 《[慾望法則](../Page/慾望法則.md "wikilink")》／《[慾望的規條](../Page/慾望的規條.md "wikilink")》（/[Law
    of Desire](../Page/慾望之規條.md "wikilink")），西班牙（1987）

  - 《[长期伴侣](../Page/长期伴侣.md "wikilink")》／《[愛是生死相許](../Page/愛是生死相許.md "wikilink")》（），美国（1990）

  - 《[末路後庭花](../Page/末路後庭花.md "wikilink")》（**），美国（1992）

  - 《[-{zh-hant:男情難了;
    zh-hans:百合花;}-](../Page/男情難了.md "wikilink")》（），加拿大（1995）

  - 《[愛情爬蟲類](../Page/愛情爬蟲類.md "wikilink")》／《[愛的路上五個人](../Page/愛的路上五個人.md "wikilink")》（），澳大利亚（1996）

  - 《[反串仍是愛](../Page/反串仍是愛.md "wikilink")》（），美国（1997）

  - 《[拳師禁戀](../Page/拳師禁戀.md "wikilink")》（），英国（1998）

  - 《[畫室培慾](../Page/畫室培慾.md "wikilink")》（），英国（1998）

  - ，美国（2001）

  - 《[-{zh-tw:意亂情迷; zh-cn:意乱情迷;
    zh-hk:少女情懷總是攣;}-](../Page/意亂情迷_\(2001年電影\).md "wikilink")》（），加拿大（2001）

  - 《[情慾軀殼](../Page/情慾軀殼.md "wikilink")》（），美国（2002）

  - 《[闪亮的日子](../Page/閃亮的日子_\(電影\).md "wikilink")》（[遇見好男孩](../Page/遇見好男孩.md "wikilink")），美國（2003）

  - 《[海灘鬥一番](../Page/海灘鬥一番.md "wikilink")》／《[0與1的誘惑](../Page/0與1的誘惑.md "wikilink")》（），美国（2005）

  - 《[痛愛](../Page/痛愛.md "wikilink")》（/），罗马尼亚/法国（2006）

  - 《[戀戀師生情](../Page/戀戀師生情.md "wikilink")》（），美国（2006）

### M

  - 《[午夜牛郎](../Page/午夜牛郎_\(電影\).md "wikilink")》，美国（1969）

  - 《[魂断威尼斯](../Page/魂斷威尼斯_\(電影\).md "wikilink")》，意大利/法國（1971）

  - 《[-{zh-hk:年少輕狂; zh-cn:我的美丽洗衣房;
    zh-tw:豪華洗衣店}-](../Page/豪華洗衣店.md "wikilink")》（My
    Beautiful Laundrette），英国（1985）

  - 《[穿梭夢美人](../Page/穿梭夢美人.md "wikilink")》（[神气活现](../Page/神气活现.md "wikilink")），美国（1987）

  - 《[-{zh-tw:墨利斯的情人; zh-hk:情難禁;
    zh-hans:莫里斯的情人;}-](../Page/莫里斯的情人.md "wikilink")》（），英国（1987）

  - [Mannequin 2](../Page/神氣活現2.md "wikilink")，美国（1991）

  - 《[我私人的爱达荷](../Page/我私人的爱达荷.md "wikilink")》，美国（1991）

  - ，英国（1996）

  - ，法国/比利时/英国（1997）

  - 《[善恶花园之午夜](../Page/善恶花园之午夜.md "wikilink")》（Midnight in the Garden of
    Good and Evil），美国（1997）

  - 《[真的想嫁你](../Page/真的想嫁你.md "wikilink")》，美國（1997）

  - 《[穆荷蘭大道](../Page/穆赫兰大道_\(电影\).md "wikilink")》（Mulholland
    Drive\]），美国（2001）

  - 《[壞教慾](../Page/壞教慾.md "wikilink")》（La mala educación），西班牙（2003）

  - 《[女魔头](../Page/女魔头.md "wikilink")》（Monster），美国/德国（2003）

  - 《[夏日午後的初纏愛戀](../Page/夏日午後的初纏愛戀.md "wikilink")》（My Summer of
    Love），英国（2004）

  - 《[-{zh-hk:誘惑肌膚;zh-hans:神秘肌肤;zh-tw:神秘肌膚;}-](../Page/神秘肌膚.md "wikilink")》（Mysterious
    Skin），美国/荷兰（2004）

  - 《[米尔克](../Page/米尔克_\(电影\).md "wikilink")》（Milk），美国（2008）

  - 《[霓虹心](../Page/霓虹心.md "wikilink")》，瑞典/台灣（2009）

  - 《[我的兄弟情人](../Page/我的兄弟情人.md "wikilink")》（My Bromance），泰国（2013）

  - 《[月亮喜歡藍](../Page/月亮喜歡藍.md "wikilink")》(Moonlight)，美國（2016）

### N

  - *The Fish Child*（），阿根廷(2009)
  - 《[玩轉墜落街](../Page/玩轉墜落街.md "wikilink")》（**），美国/法国（1997）
  - 《[黑夜第一線](../Page/黑夜第一線.md "wikilink")》（），美国（2006）
  - 《[蓮娜的甜美生活](../Page/蓮娜的甜美生活.md "wikilink")》（），英国（2006）
  - 《[醜聞日記](../Page/醜聞日記.md "wikilink")》（**），英国（2006）

### O

  - 《[奧蘭度](../Page/奧蘭度.md "wikilink")》（），英国（1992）

  - 《[夜魔](../Page/夜魔.md "wikilink")》（/），葡萄牙（2000）

  - ，德国（2000）

### P

  - 《[粉色水仙](../Page/粉色水仙.md "wikilink")》（），美国（1971）

  - ，美國 (1982)

  - 《[臨別秋波](../Page/臨別秋波.md "wikilink")》（），美国（1986）

  - 《[留心那話兒](../Page/留心那話兒.md "wikilink")》（），英国（1987）

  - 《[费城故事](../Page/費城故事_\(1993年\).md "wikilink")》（[费城故事](../Page/费城故事_\(1993年\).md "wikilink")），美国（1993）

  - 《[神父同志](../Page/神父同志.md "wikilink")》（），英国（1994）

  - 《[活著就是為了作證](../Page/活著就是為了作證.md "wikilink")》，英国/德国/美国（2000）

  - 《[烈焰焚幣](../Page/烈焰焚幣.md "wikilink")》（/[Burnt
    Money](../Page/烈燄焚幣.md "wikilink")），西班牙/法国/阿根廷/乌拉圭（2000）

  - 《[青春夢裡人](../Page/青春夢裡人.md "wikilink")》（/），法国/比利时（2000）

  - ，美国（2002）

  - 《[攣愛砒霜](../Page/攣愛砒霜.md "wikilink")》（[Proteus](../Page/Proteus.md "wikilink")），加拿大/南非（2003）

  - 《[巴黎，我爱你](../Page/巴黎，我爱你.md "wikilink")》，法国（2006）

  - 《[姣妹日記](../Page/姣妹日記.md "wikilink")》，加拿大/香港（2009）

### Q

  - 《[霧港水手](../Page/霧港水手.md "wikilink")》，德国/法国（1982）
  - 《[弊傢伙，我有咗！](../Page/弊傢伙，我有咗！.md "wikilink")》（），美国（2006）

### R

  - 《[奪魂索](../Page/奪魂索.md "wikilink")》，美国（1948）

  - 《[无因的反叛](../Page/无因的反叛.md "wikilink")》（[阿飛正傳](../Page/阿飛正傳_\(1955年電影\).md "wikilink")），美国（1955）

  - 《[洛基恐怖秀](../Page/洛基恐怖秀.md "wikilink")》／《[洛奇恐怖晚會](../Page/洛奇恐怖晚會.md "wikilink")》（），英国/美国（1975）

  - ，美国（1984）

  - 《[野芦苇](../Page/野芦苇.md "wikilink")》（/），法国（1994）

  - 《[放輕鬆…隨性做](../Page/放輕鬆…隨性做.md "wikilink")》（），美国（1998）

  - 《[吉屋出租](../Page/吉屋出租.md "wikilink")》（），美国（2005）

  - 《[我的酷兒婚禮](../Page/我的酷兒婚禮.md "wikilink")》（/），西班牙（2006）

  - 《[愛在暹羅](../Page/愛在暹羅.md "wikilink")》／《[暹罗之恋](../Page/暹罗之恋.md "wikilink")》（[爱在暹逻](../Page/爱在暹逻.md "wikilink")/），泰国（2007）

  - 《[羅馬慾樂園](../Page/羅馬慾樂園.md "wikilink")》（*Room In Rome*），西班牙（2010）

### S

  - 《[魚娃子](../Page/魚娃子.md "wikilink")》（），德国（1991）

  - 《[人妖打排球](../Page/人妖打排球.md "wikilink")》（/），泰国（2008）

  - 《[-{zh-hk:愛·面子;zh-hans:面子;zh-tw:面子;}-](../Page/面子_\(電影\).md "wikilink")》（），美国（2004）

  - 《[童子也是基](../Page/童子也是基.md "wikilink")》（），美国（1980）

  - 《[薩巴斯津的誘惑](../Page/薩巴斯津的誘惑.md "wikilink")》（[塞巴斯蒂安](../Page/塞巴斯蒂安_\(电影\).md "wikilink")），英国（1976）

  - 《[少年塞巴斯蒂安](../Page/少年塞巴斯蒂安.md "wikilink")》，挪威/瑞典（1995）

  - ，美国（1968）

  - 《[慾蓋弄潮](../Page/慾蓋弄潮.md "wikilink")》（），美国（2007）

  - 《[人海奇花](../Page/人海奇花.md "wikilink")》（**），加拿大（1997）

  - 《[單身男人](../Page/單身男人.md "wikilink")》（[摯愛無盡](../Page/摯愛無盡.md "wikilink")），美国（2009）

  - ，法国（1998）

  - 《[打開心門向藍天](../Page/打開心門向藍天.md "wikilink")》（/），德国（2000）

  - 《[夏夜迷情](../Page/夏夜迷情.md "wikilink")》（[熱帶幻夢](../Page/熱帶幻夢.md "wikilink")/），泰国（2004）

  - 《[極樂森林](../Page/極樂森林.md "wikilink")》（/），泰国（2002）

  - 《[夏日痴魂](../Page/夏日痴魂.md "wikilink")》/《[忽爾昨夏](../Page/忽爾昨夏.md "wikilink")》（），美国（1959）

  - 《[激情姐妹花](../Page/激情姐妹花.md "wikilink")》（），英国 (1994)

  - 《[踢出我衣櫃](../Page/踢出我衣櫃.md "wikilink")》（/），冰岛/芬兰/英国（2005）

  - 《[血籠於女](../Page/血籠於女.md "wikilink")》（），美国（2001）

  - 《[霜花店](../Page/霜花店.md "wikilink")》，韩国（2008）

  - 《[情迷意亂](../Page/情迷意亂.md "wikilink")》（**），美國（1992）

### T

  - ，德国（1980）

  - 《[寂寞城市](../Page/寂寞城市.md "wikilink")》《[五顆寂寞的心](../Page/五顆寂寞的心.md "wikilink")》（），美国（2000）

  - 《[雙身](../Page/雙身.md "wikilink")》（*Tintenfischalarm*/*Octopus Alarm*）

  - *To Wong Foo, Thank you for Everything\! Julie Newmar*，美国（1995）

  - 《[芬兰汤姆](../Page/芬兰汤姆_\(电影\).md "wikilink")》（*Tom of
    Finland*），芬兰（2017）

  - 《[全蚀](../Page/全蚀.md "wikilink")》（*Total Eclipse*），美国（1990）

  - 《[全搞砸了](../Page/全搞砸了.md "wikilink")》（），美国（1993）

  - 《[桃色接觸](../Page/桃色接觸.md "wikilink")》《[加利格蘭的粉紅婚禮](../Page/加利格蘭的粉紅婚禮.md "wikilink")》（），加拿大/英国（2004）

  - 《[捨不得哥哥](../Page/捨不得哥哥.md "wikilink")》（/），法国（2002）

  - 《[-{zh-hans:穿越美国;zh-hk:尋找他媽...的故事;zh-tw:窈窕老爸;}-](../Page/窈窕老爸.md "wikilink")》（[Transamerica](../Page/窈窕老爸.md "wikilink")），美国（2005）

  - 《[-{zh-hk:成晚籮籮攣;zh-hans:天雷勾动地火;zh-tw:天雷勾動地火;}-](../Page/天雷勾动地火.md "wikilink")》（），美国（1999）

  - 《[男式旅程](../Page/男式旅程.md "wikilink")》（），美国（2002）

  - 《[同志兄弟](../Page/两只老虎_\(电影\).md "wikilink")》（[兩隻老虎](../Page/兩隻老虎_\(電影\).md "wikilink")），法国/英国（2004）

  - 《[看不見的世界](../Page/看不見的世界.md "wikilink")》，英國/南非（2008）

  - 《[兩個婚禮一個葬禮](../Page/兩個婚禮一個葬禮.md "wikilink")》（*Two Weddings And A
    Funeral*），韓國（2012）

  - 《[安妮‧李斯特的秘密日記](../Page/安妮‧李斯特的秘密日記.md "wikilink")》（*The Secret
    Diaries of Miss Anne Lister*），英國（2010）

### U

  - 《[基慾都市](../Page/基慾都市.md "wikilink")》（），美国（2000）

  - /，德國(2005)

### V

  - 《[受害者](../Page/受害者_\(电影\).md "wikilink")》（），英国（1961）
  - 《[給遠方的兵哥](../Page/军官和男孩_\(电影\).md "wikilink")》／《[軍官與男孩](../Page/军官和男孩_\(电影\).md "wikilink")》（/[For
    a Lost Soldier](../Page/軍官和男孩_\(電影\).md "wikilink")），荷兰（1992）

### W

  - 《[情迷女人心](../Page/情迷女人心.md "wikilink")》／《[夜幕低垂](../Page/夜幕低垂_\(電影\).md "wikilink")》（），加拿大（1995）
  - 《[尋找西瓜女](../Page/尋找西瓜女.md "wikilink")》（），美国（1996）
  - 《[迷魂井](../Page/迷魂井.md "wikilink")》（），澳大利亚（1997）
  - 《[心太羈](../Page/心太羈.md "wikilink")》，英国（1997）
  - 《[狂野三人行](../Page/狂野三人行.md "wikilink")》／《[慾望單程路](../Page/慾望單程路.md "wikilink")》（/），比利时/法国/英国（2004）
  - 《[廣告同志](../Page/廣告同志.md "wikilink")》（*World Gay
    Commercials*），香港（2006）
  - 《[愛上壞女孩](../Page/愛上壞女孩.md "wikilink")》（/)，法國（2007）
  - 《[看不見的世界](../Page/看不見的世界.md "wikilink")》，英國（2007）
  - 《[小生夢非分](../Page/小生夢非分.md "wikilink")》（），美国（2008）

### X

  - 《[變種特攻電影系列](../Page/X戰警電影系列.md "wikilink")》，美國

### Y

  - 《[情慾報復](../Page/情慾報復.md "wikilink")》／《[慾望男情](../Page/慾望男情.md "wikilink")》（*Yok
    mang*），韓國（2002）
  - 《[我的軍中情人](../Page/我的軍中情人.md "wikilink")》（[我的军中情人](../Page/我的军中情人.md "wikilink")），以色列（2002）
  - 《[情迷禁果](../Page/情迷禁果.md "wikilink")》（/），俄罗斯（2004）
  - 《[想愛就愛](../Page/想愛就愛.md "wikilink")》，泰國（2010）

### Z

## 另見

  - [酷儿文化](../Page/酷儿文化.md "wikilink")
  - [泰迪熊奖](../Page/泰迪熊奖.md "wikilink")
  - [LGBT相关电视列表](../Page/LGBT相关电视列表.md "wikilink")

## 外部链接

  - [关于同性恋电影](http://www.aboutgaymovies.info/)：关于同性恋主题电影、电视剧、电影历史、墙纸、演员、同性恋，双性恋，变性者电影节以及其它的信息

[LGBT相關電影](../Category/LGBT相關電影.md "wikilink")
[Category:LGBT文化](../Category/LGBT文化.md "wikilink")
[Category:電影列表](../Category/電影列表.md "wikilink")
[Category:LGBT相關列表](../Category/LGBT相關列表.md "wikilink")