**樂金顯示公司**（英文：**LG
Display**），簡稱**LGD**，是一間設立於[韓國的](../Page/韓國.md "wikilink")[液晶面板的製造商](../Page/液晶.md "wikilink")，為[LG集團的子公司](../Page/LG集團.md "wikilink")。

## 簡介

前身是**LG Philips
Display**，由[韓國](../Page/韓國.md "wikilink")[樂金電子公司與](../Page/LG.md "wikilink")[荷蘭](../Page/荷蘭.md "wikilink")[皇家飛利浦電子公司兩家公司合資組成](../Page/飛利浦公司.md "wikilink")，但[飛利浦公司在](../Page/飛利浦公司.md "wikilink")2008年賣出所持股份，公司從此成為[LG集團的子公司](../Page/LG集團.md "wikilink")。

## 歷史

**LG
Display**（[諺文](../Page/諺文.md "wikilink")：엘지디스플레이）是[韓國](../Page/韓國.md "wikilink")[樂金電子公司與](../Page/樂金電子.md "wikilink")[荷蘭](../Page/荷蘭.md "wikilink")[皇家飛利浦電子公司在](../Page/飛利浦公司.md "wikilink")1999年合資組成生產主動矩陣[液晶顯示器](../Page/液晶顯示器.md "wikilink")（LCD）的公司。LG
Display與[三星電子為成為最大的液晶顯示器供應商而激烈競爭](../Page/三星電子.md "wikilink")；2006年4月它們分別擁有22%市場份額。

該公司在南韓[龜尾市與](../Page/龜尾市.md "wikilink")[坡州市擁有七間生產工廠](../Page/坡州市.md "wikilink")，在中國[南京擁有一家模組裝配廠](../Page/南京.md "wikilink")，並計劃在中國[廣州與](../Page/廣州.md "wikilink")[波蘭](../Page/波蘭.md "wikilink")[弗羅茨瓦夫興建兩間工廠](../Page/弗羅茨瓦夫.md "wikilink")。2006年8月18日，[路透社報道](../Page/路透社.md "wikilink")，LG.Philips
LCD決定不興建第8代面板工廠，改為興建5.5代的面板工廠；據LG Display副總裁Bock
Kwon的說法，是為了集中生產更易獲利的[桌上型與](../Page/個人電腦.md "wikilink")[筆記本電腦LCD顯示面板](../Page/筆記本電腦.md "wikilink")。

2004年7月，LG
Display成為獨立公司，並同時在[紐約證券交易所](../Page/紐約證券交易所.md "wikilink")（NYSE:
LPL）與[韓國證券交易所](../Page/韓國證券交易所.md "wikilink")（KRX:
034220）上市。現時樂金擁有該公司37.9%股權，飛利浦擁有32.9%股權。2006年8月17日，[華爾街日報報道](../Page/華爾街日報.md "wikilink")，[美林集團寄給客戶的一封電郵中指出飛利浦正為其LG](../Page/美林集團.md "wikilink").Philips
LCD的股份尋找買家，其中包括日本[松下電器](../Page/松下電器.md "wikilink")。

飛利浦是歐洲最大的[液晶電視生產商](../Page/液晶電視.md "wikilink")，樂金是韓國第二大生產商。2006年3月LG-Philips硏發出對角尺寸為100寸（約2.2米），當時最大的LCD面板。該公司尚未有發售使用100寸面板商品的計劃。LG
Display現時生產[Apple Cinema
Display的液晶面板](../Page/Apple_Cinema_Display.md "wikilink")、[戴爾Ultrasharp](../Page/戴爾.md "wikilink")
2005FPW與30寸液晶面板。

2008年11月12日，LG
Display和[華映](../Page/華映.md "wikilink")、[夏普共同被美國判決違反](../Page/夏普.md "wikilink")[反壟斷法](../Page/反壟斷法.md "wikilink")，私下共同壟斷手機用螢幕的價格，共被罰款5億8500萬美金；其中LG
Display被罰4億美金，為美國該類罪行中被罰款史上第2高。

截至2014年，LG Display在大型TFT-LCD市场实现了23.7%的市场占有率，位居世界第一\[1\]。

## 參考

  - [樂金集團](../Page/樂金.md "wikilink")

## 參考資料

## 外部連結

  - [LG Display 主頁](http://www.lgdisplay.com)（韓國語）

[Category:韓國電子公司](../Category/韓國電子公司.md "wikilink")
[Category:LG集團](../Category/LG集團.md "wikilink")
[Category:显示科技公司](../Category/显示科技公司.md "wikilink")

1.