**LHA**是一種[檔案壓縮](../Page/檔案壓縮.md "wikilink")[電腦軟體](../Page/電腦軟體.md "wikilink")，也是此壓縮格式的名稱，其對應的[副檔名有](../Page/副檔名.md "wikilink")`.lha`及`.lzh`。發明人為[日本業餘](../Page/日本.md "wikilink")[程式設計師](../Page/程式設計師.md "wikilink")[吉崎榮泰](../Page/吉崎榮泰.md "wikilink")（），在日本仍是常見的壓縮格式之一。

## 歷史

擔任[內科](../Page/內科.md "wikilink")[醫師的吉崎榮泰利用業餘時間](../Page/醫師.md "wikilink")，在計算機科學家[奧村晴彥所發明的](../Page/奧村晴彥.md "wikilink")[演算法之基礎上開發了名為LHarc的檔案壓縮軟體及壓縮格式](../Page/演算法.md "wikilink")，1988年首次於[網路上公開](../Page/網路.md "wikilink")。1990年左右全面改寫[程式](../Page/程式.md "wikilink")，並改名為LHA。原本是要命名為LH，但是因為和[MS-DOS的內部指令](../Page/MS-DOS.md "wikilink")`loadhigh`的[縮寫相同](../Page/縮寫.md "wikilink")，為避免誤解，所以改為LHA。依作者當時的說明，LHA的發音為「拉」（），但後來的新版本中沒有說明，所以很多人讀為「L·H·A」三個字母。很多日本人還讀為「」、「」等。

同時期盛行的壓縮格式尚有[ZIP](../Page/ZIP_\(文件格式\).md "wikilink")，但因為ZIP格式的專用程式PKZIP為[共享軟體](../Page/共享軟體.md "wikilink")，所以免費的LHA自1988年公開以來便迅速成為非常受歡迎的替代方案，許多[作業系統也陸續出現可以使用LHA格式的軟體](../Page/作業系統.md "wikilink")。除了在日本國內幾乎成為共通標準之外，LHA透過當時的[磁片流通與網路傳輸](../Page/磁片.md "wikilink")，在其他國家也成為廣泛使用的壓縮格式之一。不過由於LHA格式後來沒有繼續開發，以致和其他壓縮格式相比有著壓縮比較低、不支援[Unicode檔名](../Page/Unicode.md "wikilink")、不能[加密保護等缺點](../Page/加密.md "wikilink")，再加上[Mac
OS X與](../Page/Mac_OS_X.md "wikilink")[Windows
XP內建支援](../Page/Windows_XP.md "wikilink")[ZIP](../Page/ZIP_\(文件格式\).md "wikilink")，所以LHA漸漸被其他壓縮格式所取代，流行範圍也縮回日本國內。

LHA對日本的檔案壓縮發展史是一個重要的里程碑，日本人稱「解壓縮」為「」、壓縮檔（特別是LHA格式的壓縮檔）為「」的原因正是起源於LHA的說明文件。

2010年6月5日，廣受使用的[动态链接库](../Page/动态链接库.md "wikilink")「UNLHA32.DLL」的作者Micco因LHA格式存在被植入[病毒的安全性問題](../Page/病毒_\(電腦\).md "wikilink")，在其個人網站上呼籲停止使用LHA，同時也中止UNLHA32.DLL的開發。\[1\]

## 注釋

## 外部連結

  - [](http://oku.edu.mie-u.ac.jp/~okumura/compression/oldstory.html)－[奧村晴彥所搜集的LHA開發歷史](../Page/奧村晴彥.md "wikilink")。

[Category:归档格式](../Category/归档格式.md "wikilink")
[Category:无损压缩算法](../Category/无损压缩算法.md "wikilink")
[Category:日本發明](../Category/日本發明.md "wikilink")

1.  [圧縮・解凍用DLL「UNLHA32.DLL」が開発中止、作者はLZHの利用中止を呼びかけ](http://www.forest.impress.co.jp/docs/news/20100607_372729.html)