『**LIFE**』是日本唱作女歌手[YUI](../Page/YUI.md "wikilink")，於2005年11月9日所推出的單曲碟，並是在[Sony
Music
Records旗下的第三張單曲碟](../Page/日本新力音樂.md "wikilink")。初回版本附送印有「[死神](../Page/BLEACH.md "wikilink")」插圖的貼紙。

## 收錄歌曲

1.  **LIFE**
      -
        作詞・作曲：YUI　編曲：[northa+](../Page/northa+.md "wikilink")
    <!-- end list -->
      - [東京電視系動畫](../Page/TXN.md "wikilink")「[死神](../Page/BLEACH.md "wikilink")」的片尾曲，是首次與動漫作品的合作。
2.  **crossroad**
      -
        作詞・作曲：YUI　編曲：
3.  **Tomorrow's way～YUI Acoustic Version～**
      -
        作詞・作曲：YUI　編曲：鈴木Daichi秀行　[弦樂團編曲](../Page/弦樂團.md "wikilink")：
    <!-- end list -->
      - 每張新單曲會收錄前一單曲的Acoustic Version。
4.  **LIFE～Instrumental～**

## 銷售紀錄

  - 累積發售量：44,708
  - [Oricon最高排名](../Page/Oricon.md "wikilink")：第9名
  - [Oricon上榜次數](../Page/Oricon.md "wikilink")：12次

| 發行         | 排行榜       | 最高位 | 總銷量    |
| ---------- | --------- | --- | ------ |
| 2005年11月9日 | Oricon 週榜 | 9   | 44,708 |

[Category:YUI歌曲](../Category/YUI歌曲.md "wikilink")
[Category:2005年單曲](../Category/2005年單曲.md "wikilink")
[Category:BLEACH歌曲](../Category/BLEACH歌曲.md "wikilink")
[Category:東京電視台動畫主題曲](../Category/東京電視台動畫主題曲.md "wikilink")