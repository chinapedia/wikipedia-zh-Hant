**LIFE
is...**（中譯：生命之歌）是[日本男歌手](../Page/日本.md "wikilink")[平井堅的第五張原創專輯](../Page/平井堅.md "wikilink")，日本地區於2003年1月22日發行。發行首周空降[Oricon日本公信榜冠軍](../Page/Oricon.md "wikilink")，登場回數89周，總銷量超過80萬張，獲得[日本唱片協會百萬唱片認證](../Page/日本唱片協會.md "wikilink")。

## 概述

  - 距離上一張原創專輯『[gaining through
    losing](../Page/gaining_through_losing.md "wikilink")』約一年六個月的第五張原創專輯。
  - 收錄了上一張原創專輯『gaining through losing』後發行的所有單曲「[Missin' you ～It will
    break my
    heart～](../Page/missin'_you～It_will_break_my_heart～.md "wikilink")」、「[Strawberry
    Sex](../Page/Strawberry_Sex.md "wikilink")」、「[古老的大鐘](../Page/爺爺的古老大鐘_\(平井堅單曲\).md "wikilink")」、「[Ring](../Page/Ring_\(平井堅單曲\).md "wikilink")」，以及兩首c/w曲「ex-girlfriend」、「somebody's
    girl」。
  - ORICON日本公信榜登場回數89周，是自身登場回數最多的原創專輯。
  - 音樂錄影帶DVD『Ken Hirai Films Vol.5』同時發行。

## 單曲銷量

  - [Missin' you ～It will break my
    heart～](../Page/missin'_you～It_will_break_my_heart～.md "wikilink")：ORICON最高第4名。銷量約11.5萬張
  - [Strawberry
    Sex](../Page/Strawberry_Sex.md "wikilink")：ORICON最高第13名。銷量約4.9萬張
  - [古老的大鐘](../Page/爺爺的古老大鐘_\(平井堅單曲\).md "wikilink")：ORICON最高第1名。銷量約77.2萬張
  - [Ring](../Page/Ring_\(平井堅單曲\).md "wikilink")：ORICON最高第1名。銷量約32.2萬張

## 收錄曲

### CD

1.  **[Strawberry Sex](../Page/Strawberry_Sex.md "wikilink")**
      - 作詞：多田塚、平井堅／作曲：中野雅人、平井堅／編曲：中野雅仁
      - [HONDA](../Page/HONDA.md "wikilink")「That's」電視廣告曲
2.  **REVOLVER**
      - 作詞：平井堅、Sean Garren／作曲：T.KURA、Sean Garren／編曲：T.KURA
3.  **[ex-girlfriend](../Page/Strawberry_Sex.md "wikilink")**
      - 作詞：平井堅／作曲・編曲：村山普一郎
4.  **[Ring](../Page/Ring_\(平井堅單曲\).md "wikilink")**
      - 作詞・作曲：平井堅／編曲：富田惠一
      - [日本電視台連續劇](../Page/日本電視台.md "wikilink")「心理醫生」主題歌
5.  **Come Back**
      - 作詞：平井堅、Angie Irons／作曲：Angie Irons、T.KURA／編曲：T.KURA
6.  **[somebody's girl](../Page/Ring_\(平井堅單曲\).md "wikilink")**
      - 作詞：藤林聖子／作曲・編曲：URU
7.  **I'm so drunk**
      - 作詞・作曲：平井堅／編曲：AKIRA
8.  **[Missin' you ～It will break my
    heart～](../Page/missin'_you～It_will_break_my_heart～.md "wikilink")**
      - 作詞：平井堅／作曲・編曲：Babyface
9.  **世界上最愛的就是妳？**
      - 作詞：平井堅／作曲・編曲：Makoto Kuriya
10. **Memories**
      - 作詞：平井堅、鈴木大／作曲：MATALLY
11. **LIFE is...**
      - 作詞・作曲：平井堅／編曲：鈴木大
      - [任天堂](../Page/任天堂.md "wikilink")「Fire Emblem」電視廣告曲
      - [TBS電視台連續劇](../Page/TBS電視台.md "wikilink")「帥哥醫生」主題歌
12. **[古老的大鐘](../Page/爺爺的古老大鐘_\(平井堅單曲\).md "wikilink")**
      - 作詞・作曲：Henry Clay Work／譯詞：保富康午／編曲：龜田誠治
      - [NHK](../Page/NHK.md "wikilink") 2002年8、9月「大家的歌」
      - au by KDDI「EZ配信」電視廣告曲
      - 2003年第75回[選拔高等學校野球大會開會式入場曲](../Page/選拔高等學校野球大會.md "wikilink")

## 參考資料

[Sony Music的作品介紹](../Page/索尼音樂娛樂.md "wikilink")

  - [通常盤](http://www.sonymusic.co.jp/Music/Arch/DF/KenHirai/DFCL-1092/index.html)

[Category:平井堅音樂專輯](../Category/平井堅音樂專輯.md "wikilink")
[Category:2003年音樂專輯](../Category/2003年音樂專輯.md "wikilink")
[Category:RIAJ百萬認證專輯](../Category/RIAJ百萬認證專輯.md "wikilink")
[Category:2003年Oricon專輯週榜冠軍作品](../Category/2003年Oricon專輯週榜冠軍作品.md "wikilink")