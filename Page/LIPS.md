《**LIPS**》是日本男性偶像團體[KAT-TUN的第](../Page/KAT-TUN.md "wikilink")6張單曲作品。

## 概要

  - 成員[龜梨和也所主演的](../Page/龜梨和也.md "wikilink")[日本電視台系星期六連續劇](../Page/日本電視放送網.md "wikilink")《[1磅的福音](../Page/1磅的福音.md "wikilink")》主題曲。
  - 前一張作品是《[Keep the
    faith](../Page/Keep_the_faith.md "wikilink")》，僅相隔約2個月的單曲發售，發售速度有別於以往的快速。
  - 自《[在我們的城市裡](../Page/在我們的城市裡.md "wikilink")》連續4作品皆是連續劇的主題曲。其中3張單曲是日本電視台的連續劇。（《[在我們的城市裡](../Page/在我們的城市裡.md "wikilink")》→《[唯愛](../Page/唯愛.md "wikilink")》、《[歡喜之歌](../Page/歡喜之歌.md "wikilink")》→《特急田中3号》、《[Keep
    the
    faith](../Page/Keep_the_faith.md "wikilink")》→《[有閑俱樂部](../Page/有閑俱樂部.md "wikilink")》、《**LIPS**》→《[1磅的福音](../Page/1磅的福音.md "wikilink")』の主題歌。）而且此四項作品皆是成員所主演的連續劇。（《[唯愛](../Page/唯愛.md "wikilink")》《[1磅的福音](../Page/1磅的福音.md "wikilink")》是[龜梨和也](../Page/龜梨和也.md "wikilink")。《特急田中3号》是[田中聖](../Page/田中聖.md "wikilink")。《有閑俱樂部》是[赤西仁和](../Page/赤西仁.md "wikilink")[田口淳之介](../Page/田口淳之介.md "wikilink")。）
  - 獲得日本公信榜2008年單曲榜年終排名第9名的成績。也是[KAT-TUN連續三年的第](../Page/KAT-TUN.md "wikilink")6張年終排名前10名的單曲。

## 收錄曲

### 初回限定盤

  - CD

<!-- end list -->

1.  LIPS
      - 作詞：[Axel-G](../Page/Axel-G.md "wikilink")、RAP詞：[JOKER](../Page/田中聖.md "wikilink")、作曲・編曲：
        [Yukihide"YT"Takiyama](../Page/Yukihide"YT"Takiyama.md "wikilink")
      - [龜梨和也主演的電視劇](../Page/龜梨和也.md "wikilink")《[1磅的福音](../Page/1磅的福音#電視連續劇.md "wikilink")》（[日本電視台系](../Page/日本電視放送網.md "wikilink")）[主題曲](../Page/主題曲.md "wikilink")。
2.  LOVE
      - 作詞: Axel-G、作曲: Erik Lidbom、編曲: Erik Lidbom・Yukihide“YT”Takiyama

<!-- end list -->

  - DVD

<!-- end list -->

  - 「LIPS」VIEDO CLIP+MAKING

### 通常盤初回版

1.  LIPS
2.  LOVE
3.  MESSAGE FOR YOU
      - 作詞: soba、作曲・編曲: Erik Lidbom
4.  LIPS（Original・卡拉OK）
5.  LOVE（Original・卡拉OK）
6.  MESSAGE FOR YOU（Original・卡拉OK）

### 通常盤

1.  LIPS
2.  LOVE
3.  LIPS（Original・卡拉OK）
4.  LOVE（Original・卡拉OK）

[Category:KAT-TUN歌曲](../Category/KAT-TUN歌曲.md "wikilink")
[Category:2008年單曲](../Category/2008年單曲.md "wikilink")
[Category:2008年Oricon單曲月榜冠軍作品](../Category/2008年Oricon單曲月榜冠軍作品.md "wikilink")
[Category:2008年Oricon單曲週榜冠軍作品](../Category/2008年Oricon單曲週榜冠軍作品.md "wikilink")
[Category:2008年Japan Hot
100冠軍歌曲](../Category/2008年Japan_Hot_100冠軍歌曲.md "wikilink")
[Category:日本電視台週六連續劇主題曲](../Category/日本電視台週六連續劇主題曲.md "wikilink")