[Candylol.jpg](https://zh.wikipedia.org/wiki/File:Candylol.jpg "fig:Candylol.jpg")
**LOL**（也寫作**lol**）是常見的[網路語言用語](../Page/網路語言.md "wikilink")，最初在[Usenet上流行](../Page/Usenet.md "wikilink")，及後在其他[電腦輔助溝通工具中廣泛使用](../Page/電腦輔助溝通.md "wikilink")，甚至在其他[媒體也有使用](../Page/媒體.md "wikilink")。「LOL」是[首字母縮略字](../Page/首字母縮略字.md "wikilink")，指*Laugh
Out Loud*\[1\]\[2\],*Laugh Over Loud*,或*Laugh Out
Loudly*\[3\]，意即大聲地笑。「LOL」是眾多以文字代表笑的[縮略語之一](../Page/縮略語.md "wikilink")，其他有「ROTFL」
（代表「roll(ing) on the floor
laughing」，即笑到在地上翻滾）\[4\]\[5\]和「BWL」（代表「bursting
with laughter」，意即爆發出笑聲）\[6\]。另外，LOL也有其他與笑無關的含意，但較為少用，如「lots of
luck」（滿載幸運）或「lots of love」（滿載愛）\[7\]。

這些縮略字與日俱增\[8\]，並與[表情符號一起被](../Page/表情符號.md "wikilink")[網友收集](../Page/網友.md "wikilink")，在Usenet、[IRC和其他電腦輔助溝通工具的用戶間流傳](../Page/IRC.md "wikilink")，\[9\]。不過，使用這些縮略字引起爭議，部分人反對使用，也有人認為應避免在特定場合如商業溝通中使用。

## 變化形

雖然LOL是[英語縮略字](../Page/英語.md "wikilink")，但英語並非母語的人也直接使用，更以其他[文字拼寫](../Page/文字.md "wikilink")，如[希伯來字母](../Page/希伯來字母.md "wikilink")：[לול](../Page/wikt:לול.md "wikilink")、[西里爾字母](../Page/西里爾字母.md "wikilink")：лол、[亚美尼亚字母](../Page/亚美尼亚字母.md "wikilink")：լոլ。

### 其他地方的使用

大部分的變化形是小寫字母。

  - [lal或lawl](../Page/Wiktionary:lal.md "wikilink")：可視作標示*LOL*發音的變化形，或者[德語的變化形](../Page/德語.md "wikilink")。
  - [草](../Page/草.md "wikilink")、ww：日文版的lol，笑的日語讀音是wara，取其首字母w，又因多個w組成很像大草原，亦引申成「草」(音kusa)。
  - [lolz](../Page/Wiktionary:lolz.md "wikilink")：有時與LOL替換使用。
  - lulz：LOL的變形，常用作名詞，如「for the lul」（指「for
    laughs」）\[10\]\[11\]，尤其指[幸災樂禍的笑](../Page/幸災樂禍.md "wikilink")，常見於地下[駭客網站的](../Page/駭客.md "wikilink")[匿名留言](../Page/匿名.md "wikilink")。
  - [mdr](../Page/Wiktionary:mdr.md "wikilink")：對於[法語使用者](../Page/法語.md "wikilink")，相當於LOL，由「mort
    de rire」而來，意即「笑死」。
  - חחחחח：希伯來語版本的LOL，字母[ח發音為kh](../Page/ח.md "wikilink")，串起來就成「khkhkhkhkh」，儼如嗤嗤地笑。
  - ㅋㅋㅋㅋㅋ：[韓語版本的LOL](../Page/韓語.md "wikilink")，[ㅋ發音為k](../Page/ㅋ.md "wikilink")，串起來就成「kkkkk」，亦如嗤嗤地笑。
  - [555](../Page/Wiktionary:555.md "wikilink")：[泰語版本](../Page/泰語.md "wikilink")，因為泰語中「5」的發音如「ha」，笑聲般。
  - [asg](../Page/Wiktionary:asg.md "wikilink")：[瑞典語版本](../Page/瑞典語.md "wikilink")，「Asgarv」的縮略，意即大笑。
  - [XD](../Page/XD.md "wikilink")：網路語言，表示「張嘴大笑」的臉部表情。
  - [ㄎㄎ](../Page/ㄎㄎ.md "wikilink")：[注音文網路語言](../Page/注音文.md "wikilink")，音同「科科」，表示無言的笑。
  - [ㄏㄏ](../Page/ㄏㄏ.md "wikilink")：[注音文網路語言](../Page/注音文.md "wikilink")，音同「呵呵」，表示冷冷的笑。
  - [233](../Page/233.md "wikilink")：以[中国大陆常见](../Page/中国.md "wikilink")，来源于[猫扑社区](../Page/猫扑.md "wikilink")233号表情“锤地大笑”。
  - [ROFL](../Page/Wiktionary:ROFL.md "wikilink")：代表「roll(ing) on the
    floor laughing my ass off」，即笑到在地上翻滾。

### 其他語言

[荷蘭語中](../Page/荷蘭語.md "wikilink")，[lol真有其字](../Page/Wiktionary:lol#Dutch.md "wikilink")，巧合地意指「好玩的」。

### 其他

  - LOL 也是遊戲[英雄聯盟的簡寫](../Page/英雄聯盟.md "wikilink")

## 參考

[pl:Slang
internetowy\#Przykłady](../Page/pl:Slang_internetowy#Przykłady.md "wikilink")

[Category:俗語](../Category/俗語.md "wikilink")
[Category:網路文化](../Category/網路文化.md "wikilink")
[Category:网络流行语](../Category/网络流行语.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.
9.

10.

11.