“**LOVE &
TRUTH**”是[日本唱作女歌手](../Page/日本.md "wikilink")[YUI](../Page/YUI.md "wikilink")，于2007年9月26日所推出的第十張[單曲碟](../Page/單曲.md "wikilink")。由[STUDIOSEVEN
Recordings发行](../Page/STUDIOSEVEN_Recordings.md "wikilink")。YUI的最新單曲「LOVE
&
TRUTH」為由[澤尻英龍華](../Page/澤尻英龍華.md "wikilink")、[竹內結子和](../Page/竹內結子.md "wikilink")[伊勢谷友介主演的新片](../Page/伊勢谷友介.md "wikilink")《[Closed
Note](../Page/Closed_Note.md "wikilink")》的主題曲。YUI在看過原作和劇本之後，便立即創作出這首主題曲。「LOVE
& TRUTH」的曲風與YUI以的作品大為不同，在弦樂中加入搖滾的色彩。

## 收錄歌曲

1.  **LOVE & TRUTH**
      -
        作詞・作曲：YUI　編曲：norta+
    <!-- end list -->
      - 東寶電影「Closed Note」主題曲
2.  **Jam**
      -
        作詞・作曲：YUI　編曲：SHIGEZO
3.  **My Generation \~YUI Acoustic Version\~**
      -
        作詞・作曲：YUI　編曲：YUI & norta+
4.  **LOVE & TRUTH\~Instrumental\~**
      -
        作詞・作曲：YUI　編曲：norta+

## 销售纪录

| 發行         | 排行榜       | 最高位    | 首周銷量    | 總銷量 | 上榜次數 |
| ---------- | --------- | ------ | ------- | --- | ---- |
| 2007年9月29日 | Oricon 日榜 | 1      |         |     |      |
| Oricon 週榜  | 1         | 87,491 | 144,651 | 9   |      |
| Oricon 月榜  | 4         |        |         |     |      |
| Oricon 年榜  | 51        |        |         |     |      |

[Category:YUI歌曲](../Category/YUI歌曲.md "wikilink")
[Category:2007年單曲](../Category/2007年單曲.md "wikilink")
[Category:2007年Oricon單曲週榜冠軍作品](../Category/2007年Oricon單曲週榜冠軍作品.md "wikilink")