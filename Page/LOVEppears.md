《**LOVEppears**》（愛現）為日本歌手[濱崎步的第二張專輯](../Page/濱崎步.md "wikilink")，與單曲《[appears](../Page/appears.md "wikilink")》一起於1999年11月10日在日本發行，首周銷量即超過一百二十萬張。如同前作的首張專輯，《LOVEppears》在公信榜上也停留了高達64個星期，成為日本史上最佳銷售量第40名的專輯。

## 背景與主題

《LOVEppears》是取自單曲的「LOVE」與「appears」兩字合併的自創字，這樣的名稱意指「表象與真實的落差」。《LOVEppears》的發行距離前作《[A
Song for
××](../Page/A_Song_for_××.md "wikilink")》僅短短不到一年。在1999年7月至2000年初這段期間，她整整發行了五張的[單曲](../Page/單曲.md "wikilink")，而其中兩張銷量甚至超過百萬；其他三張則是限量單曲，但是銷售量也逼近了出貨數量。

這張專輯開始，濱崎步開始使用[電子音樂元素](../Page/電子音樂.md "wikilink")，同時也與[D・A・I](../Page/長尾大.md "wikilink")、[HΛL合作混音以及編曲的工作](../Page/HΛL.md "wikilink")。專輯裡面收錄的單曲，大多經過重新混音，以符合專輯電子音樂的風格,除「Trauma」、「Boys\&Girls」、「End
roll」3首，皆與單曲收錄的版本不同。〈LOVE ～refrain～〉一曲是取材自〈[LOVE
〜Destiny〜](../Page/LOVE_〜Destiny〜.md "wikilink")〉的旋律而經過重新混音。〈P.S
II〉是第一張專輯收錄曲〈POWDER SNOW〉的續集，使用了一樣的副歌。

而專輯末的〈Who...〉是濱崎步獻給聽眾的感謝曲，自此後成為她演唱會結尾的固定歌曲。

## 收錄歌曲

## 統計

| 榜單                                       | 最高排名 | 銷量        | 在榜時數 |
| ---------------------------------------- | ---- | --------- | ---- |
| Oricon日間專輯榜 <small>(1999年11月10日)</small> | 1    | —         | 64週  |
| Oricon週間專輯榜 <small>(1999年11月第4週)</small> | 1    | 1,201,870 |      |
| Oricon月間專輯榜 <small>(1999年11月)</small>    | 1    | —         |      |
| Oricon年間專輯榜 <small>(1999年)</small>       | 15   | —         |      |
| Oricon年間專輯榜 <small>(2000年)</small>       | 14   | —         |      |
| Oricon累積銷量                               | —    | 2,562,130 |      |

[Category:濱崎步專輯](../Category/濱崎步專輯.md "wikilink")
[Category:1999年音樂專輯](../Category/1999年音樂專輯.md "wikilink")
[Category:RIAJ百萬認證專輯](../Category/RIAJ百萬認證專輯.md "wikilink")
[Category:Oricon百萬銷量達成專輯](../Category/Oricon百萬銷量達成專輯.md "wikilink")
[Category:1999年Oricon專輯週榜冠軍作品](../Category/1999年Oricon專輯週榜冠軍作品.md "wikilink")