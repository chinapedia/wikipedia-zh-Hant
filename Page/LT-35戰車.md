**LT-35**或**LT vz.
35**是[捷克斯洛伐克制造的輕型](../Page/捷克斯洛伐克.md "wikilink")[坦克](../Page/坦克.md "wikilink")，在[二戰中被](../Page/二戰.md "wikilink")[納粹德國採用](../Page/納粹德國.md "wikilink")，德軍稱為**Panzerkampfwagen
35(t)**（**Pz.Kpfw. 35(t)**）或**Panzer 35(t)**。

## 衍生型

  - S-ll-a - 原型樣車
  - LT vz. 35 - 捷克斯洛伐克的基本衍生型，裝有37毫米A-3炮
  - T-11 - 出口至[保加利亞的衍生型](../Page/保加利亞.md "wikilink")，裝有37毫米A-7炮
  - LTM 35 - 在1940年1月德軍騎兵部隊採用型號
  - Panzerkampfwagen 35(t) - 德軍的LT-35
  - Panzerbefehlswagen 35(t) - 德軍指揮車型
  - Mörserzugmittel 35(t) - 德軍迫擊砲牽引車型
  - R2 - 羅馬尼亞的LT-35
  - TACAM R2 - 羅馬尼亞以LT-35車體改裝的R-2驅逐戰車
  - T21 - 加大車體樣車，由匈牙利生產

## 使用國

  -
  -
  -
  -
  -
[Category:輕型坦克](../Category/輕型坦克.md "wikilink")
[Category:骑兵坦克](../Category/骑兵坦克.md "wikilink")
[Category:德國二戰坦克](../Category/德國二戰坦克.md "wikilink")
[Category:捷克坦克](../Category/捷克坦克.md "wikilink")