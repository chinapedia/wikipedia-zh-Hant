**酩悅·軒尼詩－路易·威登集團**（），又称**路威酩轩集团**（****），是當今世界最大的跨國[奢侈品](../Page/奢侈品.md "wikilink")[綜合企業](../Page/綜合企業.md "wikilink")，雄踞在《財富》雜誌[世界500強公司排行榜上](../Page/世界500強.md "wikilink")，總部位於[巴黎](../Page/巴黎.md "wikilink")。該公司是1987年[時裝品牌](../Page/時尚設計.md "wikilink")[路易威登與酩悅軒尼詩](../Page/路易威登.md "wikilink")（Moët
Hennessy）合併而成立，而酩悅軒尼詩則是在1971年由[香檳酒製造商](../Page/香檳酒.md "wikilink")[酩悅和](../Page/酩悅香檳.md "wikilink")[干邑白蘭地製造商](../Page/干邑白蘭地.md "wikilink")[軒尼詩](../Page/軒尼詩_\(白蘭地\).md "wikilink")[併購而成](../Page/併購.md "wikilink")。\[1\]\[2\]\[3\]該集團控制著大約60個[子公司](../Page/子公司.md "wikilink")，每個子公司負責管理少量具有高度聲望的品牌。這些子公司通常以獨立管理方式運作。

奢侈品集團[克里斯汀·迪奧是LVMH的主要](../Page/克里斯汀·迪奧_\(品牌\).md "wikilink")[控股公司](../Page/控股公司.md "wikilink")，擁有其41％的股份和56.8％的投票權\[4\]。[貝爾納·阿爾諾](../Page/贝尔纳·阿尔诺.md "wikilink")，迪奧的大股東，是這兩家公司的[主席和LVMH的](../Page/主席.md "wikilink")[CEO](../Page/首席执行官.md "wikilink")。\[5\]

## 歷史

1987年，酩悦·轩尼诗与路易威登合并成LVMH，奠定了一个奢侈品帝国的雏形。阿尔诺也通过迪奥公司大幅增持LVMH股票，意图以最少的投入取得LVMH的控制权。他的行为引起了时任管理层的强烈不满，但1989年的股灾却帮助他实现了自己的野心，一年多时间，他以低价增持LVMH股份至44%，迅速换掉原有管理层，并为LVMH树立做全世界最大奢侈品集团的雄心。如今，集团已拥有134,476名員工，旗下擁有70多個品牌，门店总数达到3948家，是當今世界最大的精品集團。集團主要業務包括以下五個領域：[葡萄酒及](../Page/葡萄酒.md "wikilink")[烈酒](../Page/烈酒.md "wikilink")、[時裝及皮革製品](../Page/時裝.md "wikilink")、[香水及](../Page/香水.md "wikilink")[化妝品](../Page/化妝品.md "wikilink")、[鐘錶及](../Page/鐘錶.md "wikilink")[珠寶](../Page/珠寶.md "wikilink")、[精品零售](../Page/精品.md "wikilink")。

根據2013年LVMH中期業績報告的數據顯示，以港元發票計的銷售額達10.96億歐元，相等於約118.4億港元（下同），按年同期大升40.8%，

De Beers Diamond
Jewellers成立於2001年，由全球最大的精品集團LVMH（路威酩軒）集團和國際舉足輕重的鑽石開採及市場營銷公司De
Beers SA負責獨立管理及運作。

1996年，LVMH以24.7亿美元的高价收购Charles Feeney和公司律师Alan
Parker手中接近60%的DFS股權，Robert Miller仍保留其持有的38.75%股权

2010年10月24曰LVMH集團購入[愛馬仕Hermes集團](../Page/愛馬仕.md "wikilink")14.2%的股份，相當於持有1501萬6000股愛馬仕股份，若再成功把其手持約300萬股Hermes可換股衍生工具轉換普通股，其總持股量將增加至17.1%，相當於1807萬股，總「入股」作價則高達14.5億歐元（20億美元）。LVMH仍然不斷增持愛馬仕，去到22.3%。2013年7月增持愛馬仕股份至23.1%

2011年3月8曰LVMH將發行1,650萬股公司股份支付[寶格麗Bvlgari家族持有的](../Page/寶格麗.md "wikilink")1.525億股或50.4%Bvlgari股份，涉18.7億歐元，LVMH亦同時向Bvlgari小股東提出全面收購，每股作價12.25歐元，涉最多約37億歐元（約405.6億港元）
交易完成後，Bvlgari家族將是LVMH第二大股東3.5%的股份，

2012年4月25日LVMH旗下的投資基金L Capital，已聯同中信產業基金購入Ochirly的10%股份。該投資涉及二億美元，而重點是，L
Capital首次持有中國公司股份，亦即間接代表[法國LVMH集團首次買入中國時裝品牌](../Page/法國.md "wikilink")

2013年2月21日LVMH表示已買進法國設計公司Maxime Simoens的少量股份，

2013年7月9日LVMH斥资高達20億歐元（約200億港元，約合25.8億美元），收購意大利羊絨（cashmere）衣服生產商Loro
Piana80%股權，Loro
Piana創辦人家族將持有餘下20%股權，該公司料今年營業額約7億歐元（約70億港元，約合8.9776億美元），亞洲市場佔約三成比重。[香港設](../Page/香港.md "wikilink")7間分店。

2013年9月21日LVMH發布聲明宣布收購英國奢侈鞋履設計師品牌Nicholas Kirkwood的多數股權将成为LVMH旗下时尚部门LVMH
Fashion Group的品牌，

2013年9月26日LVMH發布聲明宣布收購新銳設計師Jonathan William Anderson同名時尚品牌JW
Anderson的少數股權

2014年4月15日LVMH透過旗下的私募基金L Capital Management及L Capital
Asia收購義大利精品鞋履集團Vicini S.p.A.旗下品牌Giuseppe Zanotti
Design 30％的股權。

2014年4月29日，LVMH透過旗下的私募基金L Capital
Asia斥資1億美元收購[翡翠餐飲集團](../Page/翡翠餐飲集團.md "wikilink")（Crystal
Jade）逾90%股份，翡翠餐飲在新加坡、中、港及美國等9個國家和地區開設129家食肆。2013年營業銷售額約2.5億美元\[6\]\[7\]\[8\]\[9\]\[10\]。

2016年7月26日LVMH集團将旗下高级女装品牌Donna Karan及其副线品牌DKNY以6.5亿美元的价格售予美国服装制造商G-III
Apparel。

2016年10月4日LVMH集團斥資6.4億歐元(約7.1616億美元)，向[日默瓦創辦人的孫子Dieter](../Page/日默瓦.md "wikilink")
Morszeck收購[德國高級](../Page/德國.md "wikilink")[行李箱生產商](../Page/行李箱.md "wikilink")[日默瓦](../Page/日默瓦.md "wikilink")(Rimowa)80%的股權

2017年2月3日LVMH集團
和意大利眼镜制造商Marcolin共同成立一家從市眼鏡設計和生産的合資公司，同时，酩悅·軒尼詩－路易·威登集團将收購Marcolin集團10%的股權。

2017年3月23日[戴比爾斯向酩悅](../Page/戴比爾斯.md "wikilink")·軒尼詩－路易·威登集團收購De Beers
Diamond Jewellers50%的股权。自此，戴比爾斯100%控股该品牌的零售业务。

2017年4月25日[貝爾納·阿爾諾家族斥資](../Page/貝爾納·阿爾諾.md "wikilink")121億歐元（約131.6億美元）收購[克里斯汀·迪奧餘下的](../Page/克里斯汀·迪奧_\(品牌\).md "wikilink")25.9%股權，按收購要約，LVMH將向Dior股東提出每股Dior支付172歐元現金、0.192股愛馬仕收購；次級收購方案包括以全現金形式、每股Dior支付260歐元；或以每股0.566
[愛馬仕換](../Page/愛馬仕.md "wikilink")1股Dior股份。

2017年宣佈將會創立 “La Maison des Startups"計劃，支援各類新創公司。而Ian Rogers
接受訪問時表示眼見近年新創公司愈來愈多，希望能夠集合力量，並與企業建立良好合作關係。計劃對象為全球時裝新創公司，每年分為兩期，一期6個月，分別有50個名額\[11\]。

阿爾諾（Bernard Arnault）家族現持有Dior74.1%和
[愛馬仕](../Page/愛馬仕.md "wikilink")8.3%股份
2017年4月25日酩悅·軒尼詩－路易·威登集團斥資65億歐元收購克麗絲汀迪奧高級時裝公司（Christian
Dior Couture）

2017年12月LVMH和意大利眼镜制造商Marcolin集团签署合资協議，共同成立一个各佔51%和49%股份的合資公司Thélios，按照协议，Marcolin集團将負责LVMH旗下幾个品牌眼镜系列的设计、生產、分销和推廣

2018年，LVMH宣布於10月12至13日為開放日，開放旗下56個品牌予全球公眾報名參觀，當中開放的76個工藝坊地點其中一半是從未公開的，包括[紐西蘭](../Page/紐西蘭.md "wikilink")、[美國](../Page/美國.md "wikilink")、[阿根廷](../Page/阿根廷.md "wikilink")、法國等等\[12\]。

2018年7月11日LVMH出售主打可持續發展的時裝品牌Edun
49%的股份給創始人，並停止有關品牌一切業務，而Edun在完成業務重組後會回歸市場，其現任首席執行長Julien
Labat也將繼續在品牌留任。

2018年12月14日LVMH集團以每股25美元的價格，連同債務及優先股計算在內斥資32億美元收購酒店集團[Belmond](../Page/Belmond.md "wikilink")，Belmond在全球24个国家有46家高端酒店，2017年总收入为5.61亿美元。除了酒店业务，Belmond还拥有纽约“21俱乐部”地下酒吧、法国游轮、伦敦-威尼斯专线火车以及博茨瓦纳野生动物园营地，旗下业务主要涵盖酒店、火车、轮船、餐厅4大版块

## 旗下品牌

  - 葡萄酒及烈酒
      - [酩悅香檳](../Page/酩悅香檳.md "wikilink")（[Moët &
        Chandon](../Page/Moët_&_Chandon.md "wikilink")）：成立於1743年，主營[香檳](../Page/香檳.md "wikilink")。
      - [唐培里侬香槟王](../Page/唐培里侬香槟王.md "wikilink")（[Dom
        Pérignon](../Page/Dom_Pérignon.md "wikilink")）：成立於18世紀，主營香檳。
      - [凱歌香檳](../Page/凱歌香檳.md "wikilink")（Veuve
        Clicquot）：成立於1772年，主營香檳。
      - [庫克香檳](../Page/庫克香檳.md "wikilink")（Krug）：成立於1843年，主營香檳。
      - [梅西耶香檳](../Page/梅西耶香檳.md "wikilink")（Mercier）：成立於1858年，主營香檳。
      - [修納爾香檳](../Page/修納爾香檳.md "wikilink")（Ruinart）：成立於1729年，主營香檳。
      - [迪襟莊園](../Page/迪襟莊園.md "wikilink")：（Château
        d'Yquem）：成立於1593年，主營高級葡萄酒。
      - [軒尼詩](../Page/軒尼詩_\(白蘭地\).md "wikilink")（Hennessy）：成立於1765年，主營[干邑](../Page/干邑白蘭地.md "wikilink")。
      - [格蘭傑](../Page/格蘭傑.md "wikilink")（Glenmorangie）：成立於1843年，主營[蘇格蘭](../Page/蘇格蘭.md "wikilink")[威士忌](../Page/威士忌.md "wikilink")。
      - [雅柏](../Page/雅柏.md "wikilink")（Ardbeg）：成立於1798年，主營蘇格蘭威士忌。
      - [Domaine Chandon
        (California)](../Page/Domaine_Chandon_\(California\).md "wikilink")：成立於1973年，主營[气泡酒及](../Page/气泡酒.md "wikilink")[加州葡萄酒](../Page/加州.md "wikilink")。
      - [Bodegas Chandon
        (Argentina)](../Page/Bodegas_Chandon_\(Argentina\).md "wikilink")：成立於1959年，主營气泡酒及[阿根廷葡萄酒](../Page/阿根廷.md "wikilink")。
      - [Domaine Chandon (Australia) Green
        Point](../Page/Domaine_Chandon_\(Australia\)_Green_Point.md "wikilink")：成立於1986年，主營气泡酒及[澳洲葡萄酒](../Page/澳洲.md "wikilink")。
      - [雲灣](../Page/雲灣.md "wikilink")（Cloudy
        Bay）：成立於1985年，主營[紐西蘭葡萄酒](../Page/紐西蘭.md "wikilink")。
      - [曼達岬](../Page/曼達岬.md "wikilink")（Cape
        Mentelle）：成立於1977年，主營澳洲葡萄酒。
      - [紐頓](../Page/紐頓.md "wikilink")（Newton）：成立於1977年，主營加州葡萄酒。
      - [安地斯之階](../Page/安地斯之階.md "wikilink")（Terrazas de los
        Andes）：成立於1999年，主營葡萄酒。
      - [雪樹](../Page/雪樹.md "wikilink")（Belvedere）：主營[伏特加](../Page/伏特加.md "wikilink")
      - 10 Cane：成立於2005年，主營[朗姆酒](../Page/朗姆酒.md "wikilink")
      - [文君酒](../Page/文君酒.md "wikilink")：成立於1951年，主營中國白酒
      - [敖雲](../Page/敖雲.md "wikilink")：成立於2013年，主營中國葡萄酒
      - 安地斯白馬（Cheval des Andes）：成立於1999年，主營阿根廷葡萄酒
      - Numanthia：主營[西班牙葡萄酒](../Page/西班牙.md "wikilink")
      - 酩悅軒尼詩帝亞吉歐洋酒香港有限公司
  - 時裝及皮革製品
      - [路易威登](../Page/路易威登.md "wikilink")（Louis
        Vuitton）：成立於1854年，主營[皮革製品](../Page/皮革.md "wikilink")、[成衣](../Page/成衣.md "wikilink")、[鞋履](../Page/皮鞋.md "wikilink")、[腕錶](../Page/手錶.md "wikilink")、[珠寶](../Page/珠寶.md "wikilink")、[紡織品及](../Page/紡織品.md "wikilink")[書寫用品](../Page/文具.md "wikilink")。
      - [克里斯汀·迪奥 (品牌)](../Page/克里斯汀·迪奥_\(品牌\).md "wikilink")（Christian
        Dior）：成立於1947年，主營高級訂製服、成衣、鞋履、皮革製品及飾品。
      - [羅威](../Page/羅威.md "wikilink")（LOEWE）：成立於1846年，主營皮革製品、成衣、絲绸飾品及香水。
      - [思琳](../Page/思琳.md "wikilink")（Céline）：成立於1945年，主營成衣、皮革製品、鞋履、飾品及香水。
      - [貝魯堤](../Page/貝魯堤.md "wikilink")（Berluti）：成立於1895年，主營鞋履。
      - [高田賢三](../Page/高田賢三.md "wikilink")（Kenzo）：成立於1970年，主營成衣、皮革製品、鞋履及飾品。
      - [紀梵希](../Page/紀梵希.md "wikilink")（Givenchy）：成立於1952年，主營高級訂製服、成衣、鞋履、皮革製品及飾品。
      - [馬克·雅各布斯](../Page/馬克·雅各布斯.md "wikilink")（Marc
        Jacobs）：成立於1984年，主營男女成衣、皮革製品、飾品及香水。
      - [芬迪](../Page/Fendi.md "wikilink")（Fendi）：成立於1925年，主營成衣、皮革製品、飾品及香水。
      - [史提芬諾偪](../Page/史提芬諾偪.md "wikilink")（StefanoBi）：成立於1991年，主營鞋履。
      - [艾米里歐普奇](../Page/艾米里歐普奇.md "wikilink")（Emilio
        Pucci）：成立於1947年，主營成衣及飾品。
      - [湯瑪斯品克](../Page/湯瑪斯品克.md "wikilink")（Thomas
        Pink）：成立於1984年，主營襯衫、領帶及服飾用品。
      - [Nowness](../Page/Nowness.md "wikilink")：成立於2009年，主營線上精品雜誌
      - [Loro Piana](../Page/Loro_Piana.md "wikilink")：
      - [J.W. Anderson](../Page/J.W._Anderson.md "wikilink")
      - [Nicholas Kirkwood](../Page/Nicholas_Kirkwood.md "wikilink")
      - [Maxime Simoens](../Page/Maxime_Simoens.md "wikilink")
      - [日默瓦](../Page/日默瓦.md "wikilink")(Rimowa)：成立於1898年，高級行李箱生產商
  - 香水及化妝品
      - [克里斯汀·迪奥](../Page/克里斯汀·迪奥.md "wikilink")（Parfums Christian
        Dior）：成立於1947年，主營香水、化妝品及保養品。
      - [嬌蘭](../Page/嬌蘭.md "wikilink")（Guerlain）：成立於1828年，主營香水、化妝品及保養品。
      - [茶靈](../Page/茶靈.md "wikilink") （Cha Ling L'esprit Du Thé）：LVMH
        Research研發，2016年1月正式推出市場，主營護膚，美體，香水，室內香氛，古樹茶葉，茶具。
      - [紀梵希](../Page/紀梵希.md "wikilink")（Parfums
        Givenchy）：成立於1957年，主營香水、化妝品及保養品。
      - [高田賢三](../Page/高田賢三.md "wikilink")（Kenzo
        Parfums）：成立於1988年，主營香水、沐浴系列及保養品。
      - [Laflachère](../Page/Laflachère.md "wikilink")：成立於1987年，主營衛生、美容及家用清潔製品。
      - [貝玲妃](../Page/貝玲妃.md "wikilink")（BeneFit
        Cosmetics）：成立於1976年，主營化妝品、美容及保養品。
      - [Fresh](../Page/Fresh.md "wikilink")：成立於1991年，主營護膚、美體、香水、化妝品及蠟燭。
      - [Make Up for
        Ever](../Page/Make_Up_for_Ever.md "wikilink")：成立於1984年，主營化妝師專用產品及普通消費化妝品。
      - [帕爾馬之水](../Page/帕爾馬之水.md "wikilink")（Acqua di
        Parma）：成立於1916年，主營香水、古龍水。
      - [羅威](../Page/羅威.md "wikilink")（Perfumes Loewe）：成立於1972年，主營香水。
      - [丝芙兰](../Page/丝芙兰.md "wikilink")（Sephora )
      - Maison Francis Kurkdjian
      - Fresh和Fenty Beauty by
        Rihanna（與流行天后[蕾哈娜聯手打造](../Page/蕾哈娜.md "wikilink")）

<!-- end list -->

  - 鐘錶及珠寶
      - [寶格麗](../Page/寶格麗.md "wikilink")（Bulgari）：成立於1884年，
      - [豪雅錶](../Page/豪雅錶.md "wikilink")（TAG Heuer）：成立於1860年，主營鐘錶及計時器。
      - [真力時](../Page/真力時.md "wikilink")（Zenith）：成立於1865年，主營鐘錶及計時器。
      - [Hublot](../Page/Hublot.md "wikilink")：成立於1980年，主營鐘錶及計時器。
      - [迪奧](../Page/迪奧.md "wikilink")（Dior）：成立於1975年，主營腕錶及書寫用品。
      - [佛列德](../Page/佛列德.md "wikilink")（Fred）：成立於1936年，主營珠寶、頂級珠寶及腕錶。
      - [尚美巴黎](../Page/尚美巴黎.md "wikilink")（Chaumet）：成立於1780年，主營珠寶、頂級珠寶及腕表。

<!-- end list -->

  - 精品零售
      - [DFS
        Galleria](../Page/DFS_Galleria.md "wikilink")：成立於1961年，主營免稅商品銷售。
      - [Miami Cruiseline
        Services](../Page/Miami_Cruiseline_Services.md "wikilink")：成立於1963年，主營遊輪免稅商品銷售。
      - [絲芙蘭](../Page/絲芙蘭.md "wikilink")（Sephora）：成立於1969年，主營香水、化妝品、保養品及美容用品。
      - [絲芙蘭](../Page/絲芙蘭.md "wikilink")（Sephora.com）：成立於1999年，主營香水、化妝品、保養品及美容用品網路銷售。
      - [玻瑪榭百貨](../Page/玻瑪榭百貨.md "wikilink")（Le Bon
        Marché）：成立於1852年，主營百貨商品、Franck &
        Fils商店、La Grande Epicerie de Paris。
      - [薩瑪利丹百貨](../Page/薩瑪利丹百貨.md "wikilink")（Samaritaine）：成立於1870年，主營百貨商品。
      - La Grande Epicerie de Paris：成立於1923年，高端美食超市

<!-- end list -->

  - 其他領域
      - [Groupe Les Echos](../Page/Groupe_Les_Echos.md "wikilink")：Les
        Echos、Investir、Classica、Radio Classique
      - [Royal Van Lent](../Page/Royal_Van_Lent.md "wikilink")
      - [斯達伯德郵輪服務公司Starboard](../Page/斯達伯德郵輪服務公司.md "wikilink") Cruise
        Services.Inc
      - [Cova](../Page/Cova.md "wikilink")
      - [NOWNESS](http://cn.nowness.com/)
      - [回聲報](../Page/回聲報.md "wikilink")
      - [巴黎人報](../Page/巴黎人報.md "wikilink")
      - [Jardin
        d'Acclimatation法國第一家休閒娛樂公園](../Page/Jardin_d'Acclimatation.md "wikilink")。1860年開放，目前每年迎接150萬名遊客
      - [24 Sèvres](../Page/24_Sèvres.md "wikilink")
      - [Belmond持有](../Page/Belmond.md "wikilink")36間主打高檔市場的酒店

## 海外投資

透過旗下私募基金L Capital Asia，大手投資

  - 赫基國際集團10%
  - 中国欣贺（厦门）服饰有限公司
  - 新加坡Charles& Keith
  - 明豐珠寶8.61%
  - 印度Genesis Luxury25.5％
  - 印度Fabindia 8%
  - 廣東丸美生物技術股份有限公司49％
  - 台灣DR.WU（達爾膚生醫，舊稱天昱）20％
  - [翡翠餐飲集團](../Page/翡翠餐飲集團.md "wikilink") （Crystal Jade）90％
  - 上海[砂之船集團](../Page/砂之船集團.md "wikilink") 注資逾1億美元（約7.8億港元）
  - 韓國珂莉奧（Clio）7%股權
  - [YG娛樂](../Page/YG娛樂.md "wikilink")
  - 澳洲功能服裝品牌[2XU](../Page/2XU.md "wikilink")40%股權
  - R.M. Williams 49.9%股權
  - 寧波中哲慕尚控股有限公司（GXG）70%股權

## 参考资料

## 外部連結

  - [LVMH官方網站](http://www.lvmh.com/)

[LVMH](../Category/LVMH.md "wikilink")
[Category:法国综合企业公司](../Category/法国综合企业公司.md "wikilink")
[Category:总部在巴黎的跨国公司](../Category/总部在巴黎的跨国公司.md "wikilink")
[Category:歐洲股份公司](../Category/歐洲股份公司.md "wikilink")
[Category:1987年成立的公司](../Category/1987年成立的公司.md "wikilink")

1.
2.
3.
4.
5.
6.  [LV斥7.7億買翡翠小籠包](http://hk.apple.nextmedia.com/financeestate/art/20140501/18706078)[蘋果日報](../Page/蘋果日報.md "wikilink")2014年5月1日
7.  [传LVMH斥1亿美元收购翡翠拉面小笼包](http://finance.ifeng.com/a/20140430/12243934_0.shtml)[阿思达克财经网](../Page/阿思达克财经网.md "wikilink")2014年4月30日
8.  [LVMH筍價8億買翡翠拉麵亞太逾百分店估值遠遜翠華](http://www.mpfinance.com/htm/finance/20140501/news/eb_eba1.htm)[明報](../Page/明報.md "wikilink")2014年5月1日
9.  [LVMH收購翡翠拉麵小籠包](http://news.takungpao.com.hk/paper/q/2014/0501/2451585.html)[大公報](../Page/大公報.md "wikilink")2014年5月1日
10. [LVMH 7.8億購翡翠拉麵小籠包](http://paper.wenweipo.com/2014/05/01/FI1405010022.htm)[文匯報](../Page/文匯報.md "wikilink")2014年5月1日
11. [LVMH 集團不止經營大品牌，支援全球 Startup
    計劃起動](https://www.mings-fashion.com/lvmh-bernard-arnault-1330/)
12. [你也有機會到參觀 Dior和Givenchy 等絕密時裝工作室｜LVMH
    工坊開放日](https://www.mings-fashion.com/dior-givenchy-lvmh-14308/)