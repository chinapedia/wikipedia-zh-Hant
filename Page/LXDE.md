{{ Infobox Software | logo = Lxde-logo2.png | logo size = 96px |
screenshot = LXDE-screenshot.png | screenshot size = 300px | caption =
LXDE運行於[Ubuntu](../Page/Ubuntu.md "wikilink") Linux 8.04 LTS上 | author =
[洪任諭](../Page/洪任諭.md "wikilink")（PCMan） | developer = LXDE開發群 | released
=  | latest_release_version = 0.14.0 (LXQt) | latest_release_date =
\[1\] | latest_preview_version = 0.99.0 | latest_preview_date =
\[2\] | frequently_updated = no | programming language =
[C](../Page/C語言.md "wikilink")（[GTK+](../Page/GTK+.md "wikilink")），[C](../Page/C語言.md "wikilink")/[C++](../Page/C++.md "wikilink")（[Qt](../Page/Qt.md "wikilink")）
| operating_system = [跨平台](../Page/跨平台.md "wikilink") | language = 多種語言
| status = 活躍 | genre = [桌面環境](../Page/桌面環境.md "wikilink") | license =
[GNU GPL](../Page/GPL.md "wikilink")、[GNU
LGPL](../Page/LGPL.md "wikilink") | website =
[lxde.org](http://lxde.org/)
[lxqt.org](http://lxqt.org/) }} **LXDE**，全名為**Lightweight
[X11](../Page/X11.md "wikilink") Desktop
Environment**，是[自由](../Page/自由軟體.md "wikilink")[桌面環境](../Page/桌面環境.md "wikilink")，可在[Unix以及如](../Page/Unix.md "wikilink")[Linux](../Page/Linux.md "wikilink")、[BSD等](../Page/BSD.md "wikilink")[POSIX相容平台執行](../Page/POSIX.md "wikilink")。

LXDE專案旨在提供新的輕量而快速的桌面環境。相較於功能強大與伴隨而來的膨脹性，LXDE重視實用性和輕巧性，並且盡力降低其所耗[系統資源](../Page/系統資源.md "wikilink")。不同於Linux的其他桌面環境，其[元件相依性極少](../Page/依赖关系.md "wikilink")。取而代之的是各元件可以獨立運作，大多數的元件都不須倚賴其它[套件而可以獨自執行](../Page/軟體套件.md "wikilink")。

LXDE使用[Openbox為其預設](../Page/Openbox.md "wikilink")[視窗管理器](../Page/視窗管理器.md "wikilink")，並且希望能夠提供建立在互相獨立套件上的[輕量級快速桌面](../Page/轻量级Linux发行版.md "wikilink")\[3\]。

ubuntu在8.10版時將LXDE收錄進套件庫，但一直僅是選用套件。而據LXDE主要開發者之一的PCMan透露，以LXDE為預設桌面環境的ubuntu分支[Lubuntu原定在](../Page/Lubuntu.md "wikilink")2009年10月發行第一個版本\[4\]，然而因故未能发行。随着Ubuntu
10.04发行，Lubuntu 10.04才正式发行。

## 开发历史

### 移植到Qt

2013年7月21日，LXDE專案團隊與[Razor-qt團隊宣佈他們將會合併這兩個專案](../Page/Razor-qt.md "wikilink")\[5\]。

2014年5月7日，合併後的LXQt專案發佈首個測試版本，0.7.0\[6\]\[7\]。

2014年10月14日，發佈0.8.0版本\[8\]\[9\]。

2015年2月8日，發佈0.9.0版本\[10\]\[11\]。

2015年11月2日，發佈0.10.0版本\[12\]\[13\]。

2016年9月24日，發佈0.11.0版本\[14\]。

## 包含元件

LXDE專案包括以下的獨立元件\[15\]\[16\]：

  - GPicView：單純的圖片瀏覽器，類似[Windows
    XP預設的圖片瀏覽程式](../Page/Windows_XP.md "wikilink")。
  - GtkNetCat：[netcat的圖形介面前端](../Page/netcat.md "wikilink")。
  - LXappearance：調整LXDE外觀、字型、佈景主題。
  - LXMusic：基於[XMMS2的音樂播放程式](../Page/XMMS2.md "wikilink")。
  - LXLauncher：提供與[EeePC預設使用的程式選單相似但更加強化的介面](../Page/EeePC.md "wikilink")。
  - LXNM：有線／[無線網路管理程式](../Page/無線網路.md "wikilink")。
  - LXRandR：簡單的螢幕設定工具。
  - LXpanel：LXDE的面板程式。
  - LXsession-lite：管理工作階段，為lxsession套件的輕量化版。
  - LXSession Edit：編輯登入時自動啟動程式的工具。
  - LXShortcut：編輯捷徑的工具。
  - LXtask：系統資源監控以及工作管理員。
  - LXTerminal：效能佳的終端機程式。
  - [PCManFM](../Page/PCManFM.md "wikilink")：輕巧的檔案及桌面管理程式。

以下程式並不是為LXDE專案開發的元件，但是被LXDE所採用：

  - [Leafpad](../Page/Leafpad.md "wikilink")：輕巧的文字編輯程式。
  - [Openbox](../Page/Openbox.md "wikilink")：輕巧而又功能強大的視窗管理程式。
  - [XArchiver](../Page/XArchiver.md "wikilink")：獨立的壓縮檔管理程式。

## 使用LXDE的Linux發行版

以下為預設使用LXDE的[Linux發行版](../Page/Linux發行版.md "wikilink")\[17\]：

  - [Greenie](../Page/Greenie.md "wikilink")：基於Ubuntu的發行版，特別為[斯洛伐克和](../Page/斯洛伐克.md "wikilink")[捷克使用者所設計](../Page/捷克.md "wikilink")。
  - [Linux Mint](../Page/Linux_Mint.md "wikilink"):基於Linux Mint的LXDE發行版。
  - [Myah OS](../Page/Myah_OS.md "wikilink") 3.0 Box edition：使用LXDE的Myah
    OS發行版。
  - [Parted Magic](../Page/Parted_Magic.md "wikilink")：硬碟分割工具的Live系統。
  - [PUD
    GNU/Linux](../Page/PUD_GNU/Linux.md "wikilink")：基於[Ubuntu的](../Page/Ubuntu.md "wikilink")[Live
    CD](../Page/Live_CD.md "wikilink")。
  - [Slitaz](../Page/Slitaz.md "wikilink")：超輕量級的Live CD，使用絕大部分的LXDE套件。
  - [TinyMe](../Page/TinyMe.md "wikilink")：基於[PCLinuxOS的輕量級發行版](../Page/PCLinuxOS.md "wikilink")，使用部份LXDE套件。
  - [U-lite](../Page/U-lite.md "wikilink")：在Ubuntu Base之上安裝的套件組合。
  - [VectorLinux](../Page/VectorLinux.md "wikilink")
    Lite：VectorLinux的輕量發行版。
  - [Lubuntu](../Page/Lubuntu.md "wikilink")：採用LXDE為預設桌面系統的Ubuntu分支，第一版已於2009年10月推出。

以下的發行版預設已經將LXDE收入套件庫：

  - [ArchLinux](../Page/ArchLinux.md "wikilink") \[18\]
  - [Debian](../Page/Debian.md "wikilink") 5.0 "Lenny" \[19\]
  - [Fedora](../Page/Fedora.md "wikilink") 10\[20\]
  - [Gentoo](../Page/Gentoo.md "wikilink")\[21\]
  - [gOS](../Page/gOS.md "wikilink") 3.0 \[22\]
  - [Mandriva Linux](../Page/Mandriva_Linux.md "wikilink") 2009\[23\]
  - [Ubuntu](../Page/Ubuntu.md "wikilink") 8.10 Intrepid Ibex\[24\]
  - [openSUSE](../Page/openSUSE.md "wikilink") 11.3\[25\]

## LXDE 屏幕截图

<File:LXDE> Gpicview.png|LXDE Picture Viewer
[File:LXappearance.png|LXappearance](File:LXappearance.png%7CLXappearance)
<File:Lxpanel> menu.png|LXpanel <File:Lxpanel> pref.png|LXpanel
Preferences [File:Lxtask.png|LXtask](File:Lxtask.png%7CLXtask)
[File:Pcmanfm.png|PCManFM](File:Pcmanfm.png%7CPCManFM)
[File:Pcmanfm2.png|PCManFM](File:Pcmanfm2.png%7CPCManFM) <File:Run>
dlg.png|Autocompletion of Panel tasks

## 外部連結

  - [LXDE.org英文首頁](http://lxde.org/)[中文首頁](https://web.archive.org/web/20081217194458/http://lxde.org/zh-tw)
  - [LXDE專案頁面](http://sourceforge.net/projects/lxde/)
  - [LXDE中文討論區](http://forum.lxde.org/viewforum.php?f=19)

## 註解

[LXDE](../Category/LXDE.md "wikilink")
[Category:基于GTK+的桌面环境](../Category/基于GTK+的桌面环境.md "wikilink")
[Category:从GTK+移植到Qt的软件](../Category/从GTK+移植到Qt的软件.md "wikilink")
[Category:自由桌面环境](../Category/自由桌面环境.md "wikilink")
[Category:2006年软件](../Category/2006年软件.md "wikilink")
[Category:使用Qt的軟體](../Category/使用Qt的軟體.md "wikilink")

1.

2.

3.  LXDE官方網站的[LXDE介紹](http://lxde.org/zh-tw/lxde)

4.  <http://www.ubuntu-tw.org/modules/newbb/viewtopic.php?topic_id=16099&forum=33&post_id=82439#forumpost82439>

5.

6.

7.

8.

9.

10.

11.

12.

13.

14.

15.
16. [SourceForge上的](../Page/SourceForge.md "wikilink")[LXDE專案網站](http://sourceforge.net/project/showfiles.php?group_id=180858)

17. LXDE網站中紀錄的[Linux發行版](http://wiki.lxde.org/zh/Category:Linux_%E7%99%BC%E8%A1%8C%E7%89%88)

18.

19.

20.

21.

22.

23.

24.

25.