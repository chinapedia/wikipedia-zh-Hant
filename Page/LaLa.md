《****》是[白泉社發行的少女漫畫雜誌](../Page/白泉社.md "wikilink")。1976年創刊。原為「[花與夢](../Page/花與夢.md "wikilink")」的別冊，之後獨立月刊。

## 目前連載作品

  - [赤髮白雪姬](../Page/赤髮白雪姬.md "wikilink")（[秋月空太](../Page/秋月空太.md "wikilink")）
  - [假装女友](../Page/假装女友.md "wikilink")（[林みかせ](../Page/林みかせ.md "wikilink")）
  - [幕後之人](../Page/幕後之人.md "wikilink")
    ([叶鸟螺子](../Page/叶鸟螺子.md "wikilink"))
  - [狼陛下的花嫁](../Page/狼陛下的花嫁.md "wikilink")（[可歌真都](../Page/可歌真都.md "wikilink")）
  - [學園奶爸](../Page/學園奶爸.md "wikilink")（[時計野はり](../Page/時計野はり.md "wikilink")）
  - [絕不放過你](../Page/絕不放過你.md "wikilink")（[田中機械](../Page/田中機械.md "wikilink")）
  - [圖書館戰爭LOVE](../Page/圖書館戰爭.md "wikilink")\&WAR（[弓黃色](../Page/弓黃色.md "wikilink")，[原作：](../Page/原作：.md "wikilink")[有川浩](../Page/有川浩.md "wikilink")）
  - [夏目友人帳](../Page/夏目友人帳.md "wikilink")（[绿川幸](../Page/绿川幸.md "wikilink")）
  - [ヒノコ](../Page/ヒノコ.md "wikilink")（[津田雅美](../Page/津田雅美.md "wikilink")）
  - [水珠甜心男孩](../Page/水珠甜心男孩.md "wikilink")（[池ジュン子](../Page/池ジュン子.md "wikilink")）
  - [遊星の惡作劇](../Page/遊星の惡作劇.md "wikilink")（[吳由姬](../Page/吳由姬.md "wikilink")）
  - [小雪會墮入地獄嗎](../Page/小雪會墮入地獄嗎.md "wikilink")（[藤原飛呂](../Page/藤原飛呂.md "wikilink")）
  - [Last game
    青春角力賽](../Page/Last_game_青春角力賽.md "wikilink")（[天乃忍](../Page/天乃忍.md "wikilink")）

## 過去連載作品

  - [手裡劍與百褶裙](../Page/手裡劍與百褶裙.md "wikilink")（[樋野茉理](../Page/樋野茉理.md "wikilink")）

  - [吸血鬼騎士](../Page/吸血鬼騎士.md "wikilink")（[樋野茉理](../Page/樋野茉理.md "wikilink")）

  -
  -
  - [龍族花印記](../Page/龍族花印記.md "wikilink")（[草川為](../Page/草川為.md "wikilink")）

  - [ZIG★ZAG](../Page/ZIG★ZAG.md "wikilink")（[中路有紀](../Page/中路有紀.md "wikilink")）

  - [兄妹一家親](../Page/兄妹一家親.md "wikilink")（時計野叶里）

  - [親愛的總長大人！](../Page/親愛的總長大人！.md "wikilink")（[藤方真由](../Page/藤方真由.md "wikilink")）

  - [出雲傳奇](../Page/出雲傳奇.md "wikilink")（[樹夏實](../Page/樹夏實.md "wikilink")）

  - [獸王星](../Page/獸王星.md "wikilink")（樹夏實）

  - [天國少女](../Page/天國少女.md "wikilink")（树夏实）

  - [Eensy-Weensy小怪物](../Page/Eensy-Weensy小怪物.md "wikilink")（[津田雅美](../Page/津田雅美.md "wikilink")）

  - [他與她的事情](../Page/他與她的事情.md "wikilink")（津田雅美）

  - [企鵝革命](../Page/企鵝革命.md "wikilink")（[筑波櫻](../Page/筑波櫻.md "wikilink")）

  - [當現在遇上未來](../Page/當現在遇上未來.md "wikilink")（筑波櫻）

  - [遙遠時空](../Page/遙遠時空.md "wikilink")（[水野十子](../Page/水野十子.md "wikilink")）

  - [艾丽安大道](../Page/艾丽安大道.md "wikilink")（[成田美名子](../Page/成田美名子.md "wikilink")）

  - （成田美名子）

  - [亚历山大](../Page/亚历山大_\(漫画\).md "wikilink")（成田美名子）

  - [辉夜姬](../Page/辉夜姬_\(漫画\).md "wikilink")（[清水玲子](../Page/清水玲子.md "wikilink")）

  - [魔法王子](../Page/魔法王子.md "wikilink")（樋野茉理）

  - [火宵之月](../Page/火宵之月.md "wikilink")（[平井摩利](../Page/平井摩利.md "wikilink")）

  - [蜜柑绘日记](../Page/蜜柑绘日记.md "wikilink")（[安孙子三和](../Page/安孙子三和.md "wikilink")）

  - [青春男孩](../Page/青春男孩.md "wikilink")（[山崎贵子](../Page/山崎贵子.md "wikilink")）

  - [樱兰高校男公关部](../Page/樱兰高校男公关部.md "wikilink")（[叶鸟螺子](../Page/叶鸟螺子.md "wikilink")）

  - [金色琴弦](../Page/金色琴弦.md "wikilink")（[吳由姬](../Page/吳由姬.md "wikilink")）

  - [少年改造計劃](../Page/少年改造計劃.md "wikilink")（[一之瀨薰](../Page/一之瀨薰.md "wikilink")）

  - [會長是女僕大人](../Page/會長是女僕大人.md "wikilink")（[藤原飛呂](../Page/藤原飛呂.md "wikilink")）

  - [親吻前、結婚後](../Page/親吻前、結婚後.md "wikilink")（[田中機械](../Page/田中機械.md "wikilink")）

  - [金色琴弦blue♪sky](../Page/金色琴弦blue♪sky.md "wikilink")（[吳由姬](../Page/吳由姬.md "wikilink")）

## 外部連結

  - [公式網站](http://www.hakusensha.co.jp/cgi-bin/mag/magazine.cgi?mode=magazine&magmode=mag03&day=now)

[\*](../Category/LaLa.md "wikilink")
[Category:日本漫畫雜誌](../Category/日本漫畫雜誌.md "wikilink")
[Category:少女漫畫雜誌](../Category/少女漫畫雜誌.md "wikilink")
[Category:月刊漫畫雜誌](../Category/月刊漫畫雜誌.md "wikilink")
[Category:白泉社](../Category/白泉社.md "wikilink")
[\*](../Category/花與夢.md "wikilink")