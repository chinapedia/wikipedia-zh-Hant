</ref> |operating_system =
[Linux](../Page/Linux.md "wikilink")、[Windows](../Page/Microsoft_Windows.md "wikilink")、[macOS與其他](../Page/macOS.md "wikilink")
|genre = [個人資訊管理系統](../Page/個人資訊管理系統.md "wikilink") |license =
[MPL](../Page/Mozilla公共許可證.md "wikilink") 2.0 |website =
[Lightning附加元件](https://addons.mozilla.org/zh-TW/thunderbird/addon/lightning/)
| }} **Lightning**是一個將日曆與行事曆功能加入到郵件與新聞組客戶端[Mozilla
Thunderbird的擴充套件](../Page/Mozilla_Thunderbird.md "wikilink")，从1.0版本起也可用于[SeaMonkey](../Page/SeaMonkey.md "wikilink")。Lightning於2004年12月22日對外宣佈，現由[Mozilla基金會開發](../Page/Mozilla基金會.md "wikilink")。

與已中止開發的[Mozilla
Sunbird或是](../Page/Mozilla_Sunbird.md "wikilink")不同，Lightning的目標是與Thunderbird緊緊整合\[1\]。Lightning計劃與Sunbird計劃同時開發，兩個工具使用的編碼亦完全相同，每個程式分別擁有數個個外的檔案來指示它們該如何運作\[2\]。

Lightning首個測試版本0.1版於2006年3月14日發佈，0.3版於2006年10月10日發佈，1.0版于2011年11月7日发布。

## 參考

## 外部連結

  - [官方Lightning網站](https://www.mozilla.org/projects/calendar/lightning/index.html)
  - [Lightning 0.3發佈記事](https://www.mozilla.org/projects/calendar/releases/lightning0.3.html)

## 請參閱

  -
  - [Mozilla Sunbird](../Page/Mozilla_Sunbird.md "wikilink")

  -
  - [Mozilla Thunderbird](../Page/Mozilla_Thunderbird.md "wikilink")

[Category:Mozilla](../Category/Mozilla.md "wikilink")
[Category:Mozilla附加组件](../Category/Mozilla附加组件.md "wikilink")
[Category:個人資訊管理](../Category/個人資訊管理.md "wikilink")

1.  [Lightning Project Launched to Provide Calendar Features for Mozilla
    Thunderbird-
    MozillaZine](http://www.mozillazine.org/talkback.html?article=5816)
    - MozillaZine宣佈Lightning計劃與其目標的文章。
2.  [Calendar:Dev Guide -
    MozillaWiki](http://wiki.mozilla.org/Calendar:Dev_Guide#Folder_Structure)
    - 一個說明各個日曆計劃運作方式的Wiki頁面。