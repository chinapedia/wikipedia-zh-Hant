**Links**是個[開放原始碼的純文字](../Page/開放原始碼.md "wikilink")[網頁瀏覽器](../Page/網頁瀏覽器.md "wikilink")。此軟體具有一個拉下式選單系統，能呈現複雜的[網頁](../Page/網頁.md "wikilink")（部分支持[HTML](../Page/HTML.md "wikilink")
4.0，包括表格和框架\[1\]，也支援數個字集），支援多色和單色的[终端](../Page/终端.md "wikilink")，而且支援水平捲動。与[Lynx以及](../Page/Lynx.md "wikilink")[w3m不同](../Page/w3m.md "wikilink")，此軟體是針對无视觉障碍的用户，以純文字介面工作但仍設計使用了許多[圖形使用者介面的常見元素](../Page/圖形用戶界面.md "wikilink")（彈出視窗、拉下式選單等）。開發的重點是確保在低階终端（如圖書館）下的正常使用。

Links原先由[捷克人Mikuláš](../Page/捷克.md "wikilink")
Patočka開發。後來他的研究小組Twibright
Labs，開發了第二個版本，能呈現圖形還有字體大小變化（有[反鋸齒效果](../Page/反鋸齒.md "wikilink")），並且支援[JavaScript](../Page/JavaScript.md "wikilink")。软件运行速度较快，但能正常显示的網頁较少。圖形模式能在[Unix下運行](../Page/Unix.md "wikilink")，依靠的甚至不是[X
Window或是其他視窗環境](../Page/X_Window.md "wikilink")，而是靠[SVGALib](../Page/SVGALib.md "wikilink")。

## 分支

### ELinks

ELinks（E代表實驗型或加強型）是Links的分支，由Petr Baudis領導開發。此軟體是基於Links
0.9版開發的。此軟體的開發更加開放，並且加入了來自其他版Links和網際網路使用者的補強。

### Hacked Links

Hacked Links又是另一版Links瀏覽器，結合了Elinks和Links 2的功能。

## 參考文獻

## 參見

  - [網頁親和力](../Page/網頁親和力.md "wikilink")
  - [網頁瀏覽器列表](../Page/網頁瀏覽器列表.md "wikilink")
  - [網頁瀏覽器比較](../Page/網頁瀏覽器比較.md "wikilink")

## 外部連結

  -
  - [ELinks首頁](http://elinks.or.cz/)

  - [Links
    Hacked網頁](https://web.archive.org/web/20160221211421/http://xray.sai.msu.ru/~karpov/links-hacked/)

[Category:1999年軟體](../Category/1999年軟體.md "wikilink")
[Category:自由网页浏览器](../Category/自由网页浏览器.md "wikilink")

1.