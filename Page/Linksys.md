[Linksys_WRT54G.jpg](https://zh.wikipedia.org/wiki/File:Linksys_WRT54G.jpg "fig:Linksys_WRT54G.jpg")\]\]

**Linksys**是一間網路設備公司。Linksys最初於1988年創立，2003年3月被思科收購。儘管Linksys最知名的是其[寬頻調製解調器](../Page/調製解調器.md "wikilink")，[無線接入點與](../Page/無線接入點.md "wikilink")[無線路由器](../Page/路由器#无线网络路由器.md "wikilink")，但亦有生產[網絡交換機與](../Page/網絡交換機.md "wikilink")[VoIP裝置以及多種其他產品](../Page/VoIP.md "wikilink")。在2013年，思科把
Linksys 賣給 [Belkin](../Page/Belkin.md "wikilink")，並於同年完成併購。

鴻海集團（Foxconn）旗下的鴻騰精密科技（Foxconn Interconnect
Technology）2018/03/27宣布，將以8.66億美元的現金買下美國消費者電子產品製造商Belkin，取得Linksys、Wemo與Phyn等品牌。

## 路由器列表

### WRT54G

或許由Linksys生產最著名的產品為WRT54G，為降低成本Linksys決定使用基於[Linux](../Page/Linux.md "wikilink")[作業系統的](../Page/作業系統.md "wikilink")[固件](../Page/固件.md "wikilink")。消費級的路由器包含一個[中央處理單元與作業系統](../Page/中央處理單元.md "wikilink")，而多數功能透過[軟體編碼來實行](../Page/軟體.md "wikilink")，以將實體[硬體保持到最低數來節省生產成本](../Page/硬體.md "wikilink")；但消費級路由器已知會因依賴軟體來提供功能而不可靠，基於軟體的路由器未配備快速處理器可能會在指向網絡流量時緩慢。WRT54G因擁有快速的處理器而知名，但其固件並不完整及缺乏高階功能。

2002年，[哥倫比亞大學法學院教授](../Page/哥倫比亞大學.md "wikilink")[Eben
Moglen指出](../Page/Eben_Moglen.md "wikilink")，由於其固件本質上是基於Linux，思科在法律上有責任以[GPL條款釋出路由器的](../Page/GNU通用公共許可證.md "wikilink")[原始碼](../Page/原始碼.md "wikilink")。經過一段時間後，思科承認其責任，並釋出編碼及透露了軟體編碼與硬體溝通方式的秘密。

隨後形成了一個專門修改Linksys路由器[固件的](../Page/固件.md "wikilink")[開放原始碼社區](../Page/開放原始碼.md "wikilink")，業餘程式設計員很快地認識到如何將價值600[美元的功能加入價值](../Page/美元.md "wikilink")60美元的路由器中。這改變了[路由器市場的動態](../Page/路由器.md "wikilink")，同樣在預料中加強了部份消費級產品的穩定性與功能。

Linksys與其他銷售商當時不得不作出反應，因為開放原始碼固件現時可免費取得授權與訂製，令新的路由器銷售商可在沒有以往需要開發固件編碼的障礙下進入市場，最好的消費級路由器現時可認為是比得上以往的高階路由器。

WRT54G與WRT54GS系列無線路由器後來改為使用可減低[快閃記憶體需求的](../Page/快閃記憶體.md "wikilink")[VxWorks](../Page/VxWorks.md "wikilink")[核心](../Page/核心.md "wikilink")，WRT54GS第三版擁有8MB快閃記憶體，第四版擁有4MB而第五版只有2MB，縮減的快閃記憶體容量限制了功能強大的開放原始碼固件加入到標準的Linksys路由器中。此時Linksys仍有生產基於Linux的路由器版本，名為WRT54GL，但未作為其中一款旗艦產品進行推廣。

後期Linksys發售了WRT54GL路由器，與原版WRT54G相似均是基於Linux，因而可輕易地更新第三方固件，儘管Linksys仍未直接支持這種做法。事實上愛好者仍有興趣修改與重新定義他們路由器的多數用法。

## NSLU2開放源始碼計劃

[NSLU2是一個網絡儲存裝置](../Page/NSLU2.md "wikilink")，本身並未有大量的儲存容量，但擁有兩個[USB端口可加載外接硬碟](../Page/USB.md "wikilink")，現有多個計劃令[Linux能在該機器上使用](../Page/Linux.md "wikilink")。

## Sipura Technologies的收購

母公司[思科系統於](../Page/思科系統公司.md "wikilink")2005年4月26日為其部門Linksys收購了VoIP製造商*Sipura
Technologies*。

## 外部連結

  - [官方網站](http://www.linksys.com)
  - <http://www.ithome.com.tw/node/78546> Belkin買下思科的Linksys家用網路事業
  - [使用者論譠與支援網站](http://forums.linksys.com) - 提供幫助與支援
  - [GPL code center](http://support.linksys.com/en-apac/gplcodecenter)
    於Linksys.com
  - [WET54G Wireless-G Ethernet
    Bridge](http://www.linksys.com/servlet/Satellite?childpagename=US%2FLayout&packedargs=c%3DL_Product_C2%26cid%3D1134692497433&pagename=Linksys%2FCommon%2FVisitorWrapper)，可將[音響連接到個人電腦已播放下載的音樂](../Page/音響.md "wikilink")
  - [Cisco Systems to Acquire Sipura
    Technology](https://web.archive.org/web/20050427012633/http://newsroom.cisco.com/dlls/2005/corp_042605.html?CMP=ILC-001)
    - 思科新聞稿
  - [The iPhone Is Launched ... By Linksys, Not
    Apple](http://www.pcworld.com/article/id,128236-c,cellphones/article.html)
    於[PC World](../Page/PC_World.md "wikilink")

[Category:美國電子公司](../Category/美國電子公司.md "wikilink")