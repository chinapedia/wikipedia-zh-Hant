**Linux**（ ）是一種[自由和開放源碼的](../Page/自由及开放源代码软件.md "wikilink")[類
UNIX](../Page/类Unix系统.md "wikilink")
[作業系統](../Page/作業系統.md "wikilink")。该操作系统的[内核由](../Page/内核.md "wikilink")[林纳斯·托瓦兹在](../Page/林纳斯·托瓦兹.md "wikilink")
1991 年 10 月 5
日首次发布\[1\]\[2\]，在加上[使用者空間的](../Page/使用者空間.md "wikilink")[應用程式之後](../Page/應用程式.md "wikilink")，成為
Linux 作業系統。Linux
也是[自由软件和](../Page/自由软件.md "wikilink")[开放源代码软件发展中最著名的例子](../Page/开放源代码软件.md "wikilink")。只要遵循
[GNU 通用公共许可证](../Page/GNU通用公共许可证.md "wikilink")（GPL），任何个人和机构都可以自由地使用
Linux 的所有底层[源代码](../Page/源代码.md "wikilink")，也可以自由地修改和再发布。大多數 Linux
系統還包括像提供 [GUI](../Page/GUI.md "wikilink") 的 [X
Window](../Page/X_Window.md "wikilink") 之類的程序。除了一部分專家之外，大多數人都是直接使用
[Linux 發行版](../Page/Linux發行版.md "wikilink")，而不是自己選擇每一樣組件或自行設置。

**Linux**嚴格來說是單指作業系統的内核，因作業系統中包含了許多[用戶圖形介面和其他实用工具](../Page/GUI.md "wikilink")。如今Linux常用来指基于Linux的完整操作系统，內核則改以[Linux内核稱之](../Page/Linux内核.md "wikilink")。由于这些支持用户空间的系统工具和库主要由[理查德·斯托曼于](../Page/理查德·斯托曼.md "wikilink")1983年发起的[GNU计划提供](../Page/GNU計劃.md "wikilink")，[自由软件基金会提议将其组合系统命名为](../Page/自由软件基金会.md "wikilink")**GNU/Linux**\[3\]\[4\]，但Linux不屬於[GNU計劃](../Page/GNU計劃.md "wikilink")，這個名稱並沒有得到社群的一致認同。

Linux最初是作为支持[英特尔](../Page/英特尔.md "wikilink")[x86架构的个人电脑的一个自由操作系统](../Page/x86.md "wikilink")。目前Linux已经被移植到更多的计算机[硬件平台](../Page/硬件.md "wikilink")，远远超出其他任何操作系统。Linux可以运行在[服务器和其他大型平台之上](../Page/服务器.md "wikilink")，如[大型计算机和](../Page/大型计算机.md "wikilink")[超级计算机](../Page/超级计算机.md "wikilink")。世界上500个最快的超级计算机90％以上运行Linux发行版或变种\[5\]，包括最快的前10名超级电脑运行的都是基于Linux内核的操作系统\[6\]。Linux也广泛应用在[嵌入式系统上](../Page/嵌入式系统.md "wikilink")，如[手机](../Page/手机.md "wikilink")（Mobile
Phone）、[平板电脑](../Page/平板電腦.md "wikilink")（Tablet）、[路由器](../Page/路由器.md "wikilink")（Router）、[电视](../Page/电视.md "wikilink")（TV）和[电子游戏机等](../Page/电子游戏机.md "wikilink")。在[移动设备上广泛使用的](../Page/移动设备.md "wikilink")[Android操作系统就是建立在Linux内核之上](../Page/Android.md "wikilink")。

通常情况下，Linux被打包成供个人计算机和服务器使用的Linux发行版，一些流行的主流Linux发布版，包括[Debian](../Page/Debian.md "wikilink")（及其衍生版本[Ubuntu](../Page/Ubuntu.md "wikilink")、[Linux
Mint](../Page/Linux_Mint.md "wikilink")）、[Fedora](../Page/Fedora.md "wikilink")（及其相关版本[Red
Hat Enterprise
Linux](../Page/Red_Hat_Enterprise_Linux.md "wikilink")、[CentOS](../Page/CentOS.md "wikilink")）和[openSUSE等](../Page/openSUSE.md "wikilink")。Linux发行版包含Linux内核和支撑内核的实用[程序和库](../Page/计算机程序.md "wikilink")，通常还带有大量可以满足各类需求的应用程序。个人计算机使用的Linux发行版通常包含[X
Window和一个相应的桌面环境](../Page/X_Window.md "wikilink")，如[GNOME或](../Page/GNOME.md "wikilink")[KDE](../Page/KDE.md "wikilink")。桌面Linux操作系统常用的[应用程序](../Page/应用程序.md "wikilink")，包括[Firefox网页浏览器](../Page/Firefox.md "wikilink")、[LibreOffice办公软件](../Page/LibreOffice.md "wikilink")、[GIMP图像处理工具等](../Page/GIMP.md "wikilink")。由于Linux是自由软件，任何人都可以创建一个符合自己需求的Linux发行版。

## 发音

根據托瓦兹的說法，Linux的發音和「[Minix](../Page/MINIX.md "wikilink")」是押韻的。

「Li」中「i」的發音類似於「Minix」中「i」的發音，而「nux」中「u」的發音類似於英文單詞「profess」中「o」的發音。依照國際音標應該是\['linэks\]\[ˈlɪnəks\]。\[7\]

此外有一份林纳斯·托瓦兹本人說話的錄音，錄音內容為「Hello, this is Linus Torvalds, and I pronounce
Linux as *Linux*」，也表明了作者对单词的读法。

## 歷史

[Linus_Torvalds_(cropped).jpg](https://zh.wikipedia.org/wiki/File:Linus_Torvalds_\(cropped\).jpg "fig:Linus_Torvalds_(cropped).jpg")，[Linux内核首创者](../Page/Linux内核.md "wikilink")。\]\]

### UNIX渊源

[UNIX操作系统](../Page/UNIX.md "wikilink")（），是[美国](../Page/美国.md "wikilink")[AT\&T公司](../Page/AT&T.md "wikilink")[贝尔实验室于](../Page/贝尔实验室.md "wikilink")1969年完成的[操作系统](../Page/操作系统.md "wikilink")。最早由[肯·湯普遜](../Page/肯·湯普遜.md "wikilink")（Ken
Thompson），[丹尼斯·里奇](../Page/丹尼斯·里奇.md "wikilink")（Dennis
Ritchie），[道格拉斯·麥克羅伊](../Page/道格拉斯·麥克羅伊.md "wikilink")（Douglas
McIlroy），和[喬伊·歐桑納于](../Page/喬伊·歐桑納.md "wikilink")1969年在[AT\&T](../Page/AT&T.md "wikilink")[贝尔实验室开发](../Page/贝尔实验室.md "wikilink")。于1971年首次发布，最初是完全用[汇编语言编写](../Page/汇编语言.md "wikilink")。后来，在1973年用一个重要的开拓性的方法，Unix被[丹尼斯·里奇用](../Page/丹尼斯·里奇.md "wikilink")[编程语言](../Page/编程语言.md "wikilink")[C](../Page/C语言.md "wikilink")（内核和I/O例外）重新编写\[8\]。[高级语言编写的操作系统具有更佳的兼容性](../Page/高级语言.md "wikilink")，能更容易地[移植到不同的计算机平台](../Page/移植_\(軟體\).md "wikilink")。

1983年，[理查德·馬修·斯托曼創立](../Page/理查德·斯托曼.md "wikilink")[GNU計劃](../Page/GNU計劃.md "wikilink")。這個計劃有一個目標，是為了發展一個完全自由的類Unix作業系統。自1984年發起這個計劃以來\[9\]，在1985年，[理查德·馬修·斯托曼發起](../Page/理查德·馬修·斯托曼.md "wikilink")[自由軟體基金會並且在](../Page/自由軟體基金會.md "wikilink")1989年撰寫[GPL](../Page/GNU_General_Public_License.md "wikilink")。1990年代早期，GNU開始大量地產生或收集各種系統所必備的元件，像是——函式庫、編譯器、偵錯工具、文字編輯器，以及一個Unix的使用者介面（Unix
[shell](../Page/shell.md "wikilink")）——但是像一些底層環境，如[硬體驅動](../Page/驅動程式.md "wikilink")、[守護進程](../Page/守护进程.md "wikilink")、系统核心（kernel）仍然不完整和陷于停顿，GNU計劃中是在[Mach微内核的架構之上開發系統核心](../Page/Mach.md "wikilink")，也就是所謂的GNU
[Hurd](../Page/Hurd.md "wikilink")，但是這個基於Mach的設計異常複雜，發展進度則相對緩慢。\[10\][林納斯·托瓦茲曾說過如果GNU核心在](../Page/林納斯·托瓦茲.md "wikilink")1991年時可以用，他不會自己去寫一個。\[11\]

386[BSD涉及的法律問題直到](../Page/BSD.md "wikilink")1992年還沒有解決，[NetBSD和](../Page/NetBSD.md "wikilink")[FreeBSD是](../Page/FreeBSD.md "wikilink")386BSD的後裔，早于Linux。林纳斯·托瓦兹曾说，当时如果有可用的386BSD，他就可能不會編寫Linux。\[12\]

[MINIX是一個轻量小型并采用](../Page/MINIX.md "wikilink")[微内核](../Page/微內核.md "wikilink")(Micro-Kernel)架构的類Unix作業系統，是[安德鲁·斯圖爾特·塔能鲍姆為在電腦科學用作教學而設計的](../Page/安德鲁·斯图尔特·塔能鲍姆.md "wikilink")。

### 创立

1991年，[林納斯·托瓦茲在](../Page/林纳斯·托瓦兹.md "wikilink")[赫爾辛基大學上學时](../Page/赫爾辛基大學.md "wikilink")，對作業系統很好奇\[13\]。他對[MINIX只允許在教育上使用很不滿](../Page/MINIX.md "wikilink")（在当时MINIX不允許被用作任何商業使用），於是他便開始寫他自己的作業系統，這就是後來的[Linux核心](../Page/Linux核心.md "wikilink")。

[林納斯·托瓦茲開始在](../Page/林纳斯·托瓦兹.md "wikilink")[MINIX上開發](../Page/MINIX.md "wikilink")[Linux內核](../Page/Linux內核.md "wikilink")，為[MINIX寫的軟體也可以在](../Page/MINIX.md "wikilink")[Linux內核上使用](../Page/Linux內核.md "wikilink")。\[14\]后来使用GNU軟體代替MINIX的軟體，因為使用從GNU系統來的原始碼可以自由使用，這對Linux的发展是有益。使用GNU
GPL協議的原始碼可以被其他項目所使用，只要這些項目使用同樣的協議發布。為了讓Linux可以在商業上使用，[林納斯·托瓦茲決定更改他原來的協議](../Page/林納斯·托瓦茲.md "wikilink")（這個協議會限制商業使用），以GNU
GPL協議來代替。\[15\]之后许多開發者致力融合GNU元素到Linux中，做出一個有完整功能的、自由的作業系統。\[16\]

### 命名

Linux的第一個版本在1991年9月被大學FTP server管理員-{Ari
Lemmke}-發佈在[Internet上](../Page/網際網路.md "wikilink")，最初Torvalds稱這個核心的名稱為"Freax"，意思是自由（"free"）和奇異（"freak"）的結合字，並且附上"X"這個常用的字母，以配合所謂的類Unix的系統。但是FTP伺服器管理員嫌原來的命名「Freax」的名稱不好聽，把核心的稱呼改成「Linux」，當時僅有10000行程式碼，仍必須執行於Minix作業系統之上，並且必須使用硬碟開機；隨後在10月份第二個版本（0.02版）發佈，同時這位芬蘭赫爾辛基的大學生在comp.os.minix上發佈一則訊息

> Hello everybody out there using minix- I'm doing a (free) operation
> system (just a hobby, won't be big and professional like gnu) for
> 386(486) AT clones.

Linux的[標誌和](../Page/標誌.md "wikilink")[吉祥物是一隻名字叫做](../Page/吉祥物.md "wikilink")[Tux的](../Page/Tux.md "wikilink")[企鵝](../Page/企鵝.md "wikilink")，標誌的由來是因為Linus在澳洲時曾被一隻動物園裡的企鵝咬了一口，便選擇企鵝作為Linux的標誌。更容易被接受的說法是：企鵝代表南極，而南極又是全世界所共有的一塊陸地。這也就代表Linux是所有人的Linux。

### 发展现状

1994年3月，Linux1.0版正式發佈，Marc Ewing成立Red
Hat軟件公司，成為最著名的Linux經銷商之一。早期Linux的開機管理程式（boot
loader）使用[LILO](../Page/LILO.md "wikilink")（Linux
Loader），早期的LILO存在著一些難以容忍的缺陷，例如無法識別1024柱面以後的硬碟空間，後來的[GRUB](../Page/GRUB.md "wikilink")（GRand
Unified
Bootloader）克服這些缺點，具有『動態搜尋核心檔案』的功能，可以讓用户在開機的時候，自行編輯開機設定系統檔案，透過[ext2或](../Page/ext2.md "wikilink")[ext3檔案系統中載入Linux](../Page/ext3.md "wikilink")
Kernel（GRUB通過不同的文件系統驅動可以識別幾乎所有Linux支援的文件系統，因此可以使用很多文件系統來格式化核心文件所在的磁區，並不侷限於ext文件系統）。

今天由Linus
Torvalds带领下，众多开发共同参与开发和维护Linux内核。\[17\]理查德·斯托曼领导的自由软件基金会，继续提供大量支持Linux内核的GNU组件。\[18\]一些个人和企业开发的第三方的非GNU组件也提供对Linux内核的支持，这些第三方组件包括大量的作品，有内核模块和用户应用程序和库等内容。Linux社区或企业都推出一些重要的[Linux发行版](../Page/Linux发行版.md "wikilink")，包括Linux内核、GNU组件、非GNU组件，以及其他形式的软件包管理系统软件。

## 系统架构

[Linux_kernel_ubiquity.svg](https://zh.wikipedia.org/wiki/File:Linux_kernel_ubiquity.svg "fig:Linux_kernel_ubiquity.svg")）提供公共平台。\]\]
基于Linux的系统是一个模块化的类Unix操作系统。Linux操作系统的大部分设计思想来源于20世纪70年代到80年代的Unix操作系统所建立的基本设计思想。Linux系统使用[宏内核](../Page/宏内核.md "wikilink")，由Linux内核负责处理[进程控制](../Page/进程.md "wikilink")、[网络](../Page/互联网.md "wikilink")，以及[外围设备和](../Page/外部设备.md "wikilink")[文件系统的访问](../Page/文件系统.md "wikilink")。在系统运行的时候，设备驱动程序要么与内核直接整合，要么以加载模块形式添加。

Linux具有设备独立性，它内核具有高度适应能力，从而给系统提供了更高级的功能。GNU用户界面组件是大多数Linux操作系统的重要组成部分，提供常用的[C函数库](../Page/C標準函式庫.md "wikilink")，[Shell](../Page/殼層.md "wikilink")，还有许多常见的[Unix实用工具](../Page/Unix实用程序列表.md "wikilink")，可以完成许多基本的操作系统任务。大多数Linux系统使用的[图形用户界面建立在](../Page/图形用户界面.md "wikilink")[X窗口系统之上](../Page/X_Window系統.md "wikilink")，由X窗口(XWindow)系统通过软件工具及架构协议来建立操作系统所用的图形用户界面。

已安装Linux操作系统包含的一些组件：

  - [启动程序](../Page/啟動程式.md "wikilink")：例如[GRUB或](../Page/GNU_GRUB.md "wikilink")[LILO](../Page/LILO.md "wikilink")。该程序在计算机开机启动的时候运行，并将Linux内核加载到内存中。
  - [init程序](../Page/Init.md "wikilink")：init是由Linux内核创建的第一个进程，称为根进程，所有的系统进程都是它的子进程，即所有的进程都是通过init启动。init启动的进程如系统服务和登录提示（图形或终端模式的选择）。
  - 软件库包含代码：可以通过运行的进程在Linux系统上使用[ELF格式来执行文件](../Page/可執行與可鏈接格式.md "wikilink")，负责管理库使用的[动态链接器是](../Page/Ld.so.md "wikilink")“ld-linux.so”。Linux系统上最常用的软件库是[GNU
    C库](../Page/GNU_C函式庫.md "wikilink")。
  - 用户界面程序：如命令行Shell或窗口环境。

## Linux發行版

[Ubuntu-18.04-LTS-Wikipedia.png](https://zh.wikipedia.org/wiki/File:Ubuntu-18.04-LTS-Wikipedia.png "fig:Ubuntu-18.04-LTS-Wikipedia.png")是一個流行的[桌面Linux發行版](../Page/Linux發行版.md "wikilink")。\]\]
**Linux發行版**指的就是通常所說的「Linux作業系統」，它一般是由一些組織、团体、公司或者個人制作并發行的。Linux内核主要作為Linux發行版的一部分而使用。通常來講，一個Linux發行版包括Linux核心，以及將整個軟體安裝到電腦上的一套安裝工具，还有各種GNU軟體，和其他的一些[自由軟體](../Page/自由软件.md "wikilink")，在一些Linux發行版中可能会包含一些[專有軟體](../Page/专有软件.md "wikilink")。發行版為許多不同的目的而製作，包括對不同[電腦硬體結構的支援](../Page/硬件.md "wikilink")，对普通用户或开发者使用方式的调整，针对[實時應用或](../Page/实时操作系统.md "wikilink")[嵌入式系統的开发等等](../Page/嵌入式系统.md "wikilink")。目前，超過三百個發行版被積極的開發，最普遍被使用的發行版有大約十二個。較為知名的有[Debian](../Page/Debian.md "wikilink")、[Ubuntu](../Page/Ubuntu.md "wikilink")、[Fedora和](../Page/Fedora.md "wikilink")[openSUSE等](../Page/openSUSE.md "wikilink")。\[19\]

一個典型的發行版包括：[Linux核心](../Page/Linux核心.md "wikilink")，GNU[函式庫和各种系统工具](../Page/函式庫.md "wikilink")，命令行[Shell](../Page/Unix_shell.md "wikilink")，圖形界面底层的[X
Window系統和上层的](../Page/X_Window系統.md "wikilink")[桌面環境等](../Page/桌面环境.md "wikilink")。桌面环境有如[KDE或](../Page/KDE.md "wikilink")[GNOME等](../Page/GNOME.md "wikilink")，並包含數千種從[辦公套件](../Page/辦公套件.md "wikilink")，[編譯器](../Page/編譯器.md "wikilink")，[文字編輯器](../Page/文字編輯器.md "wikilink")，小遊戲，兒童教育軟體，到科學工具的應用軟體。

很多發行版含有[LiveCD的方式](../Page/Live_CD.md "wikilink")，就是不需要安裝，放入系统光碟或其它介质進行启动，就能够在不改变现有系统的情况下使用。比較著名的有[Damn
Small
Linux](../Page/Damn_Small_Linux.md "wikilink")，[Knoppix等](../Page/Knoppix.md "wikilink")。[LiveCD的相關技術進步至此](../Page/Live_CD.md "wikilink")，很多的發行版本身的安裝光碟也有[LiveCD的功能](../Page/Live_CD.md "wikilink")。

## Linux的应用

今天各種場合都有使用各種Linux套件，從嵌入式設備到超級電腦（Super
Computer），\[20\]\[21\]並且在伺服器領域確定了地位，通常伺服器使用[LAMP組合](../Page/LAMP.md "wikilink")。\[22\]在家庭與企業中使用Linux套件的情況越來越多。\[23\]\[24\]\[25\]\[26\]\[27\]\[28\]\[29\]並且在政府中也很受歡迎，巴西聯邦政府由於支持Linux而世界聞名。\[30\]\[31\]有新聞報道俄羅斯軍隊自己製造的Linux發行版的，做為G.H.ost項目已經取得成果.\[32\]印度的[喀拉拉邦計劃在向全聯邦的高中推廣使用Linux](../Page/喀拉拉邦.md "wikilink")。\[33\]\[34\]中華人民共和國為取得技術獨立，在[龍芯過程中排他性地使用Linux](../Page/龍芯.md "wikilink")。\[35\]
在西班牙的一些地區開發了自己的Linux發行版，並且在政府與教育領域廣泛使用，如[埃斯特雷马杜拉地區的](../Page/埃斯特雷马杜拉.md "wikilink")[gnuLinEx和](../Page/gnuLinEx.md "wikilink")[安達盧西亞地區的](../Page/安達盧西亞.md "wikilink")[Guadalinex](../Page/Guadalinex.md "wikilink")。[葡萄牙同樣使用自己的Linux發行版](../Page/葡萄牙.md "wikilink")[Caixa
Mágica](../Page/Caixa_Mágica.md "wikilink")，用於Magalh?es筆記型電腦\[36\]和e-escola政府軟體。\[37\]法國和德國同樣開始逐步採用Linux。\[38\]

傳統的Linux使用者一般都是專業人士。他們願意安裝並設置自己的作業系統，往往比其他作業系統的用戶花更多的時間在安裝並設置自己的作業系統。這些用戶有時被稱作「[駭客](../Page/黑客.md "wikilink")」或是「[極客](../Page/极客.md "wikilink")」。

使用Linux主要的成本為移植、培訓和學習的費用，早期由於會使用Linux的人較少，並且在軟體設計時並未考慮非專業者的使用，導致這方面費用極高。但這方面的費用已經隨著Linux的日益普及和Linux上的軟體越來越多、越來越方便而降低，但專業仍是使用Linux的主要成本。

然而隨著Linux慢慢開始流行，有些[原始設備製造商](../Page/代工生产.md "wikilink")（OEM）開始在其銷售的電腦上預裝上Linux，Linux的用戶中也有了普通電腦用戶，Linux系統也開始慢慢出現在[個人電腦作業系統市場](../Page/个人电脑.md "wikilink")。Linux在歐洲、美國和日本的流行程度較高，歐美地區還發行Linux平台的遊戲和其他家用軟體。Linux開源社群方面也是以歐洲、美國、日本等已開發國家的人士居多。

[每個孩子一台筆記型電腦這一項目正在催生新的更為龐大的Linux用戶群](../Page/OLPC.md "wikilink")，計劃將包括發展中國家的幾億學童、他們的家庭和社區。在2007年，已經有六個國家訂購了至少每個國家一百萬台以上免費發放給學生。[Google](../Page/Google.md "wikilink")、[Red
Hat和](../Page/Red_Hat.md "wikilink")[eBay是該項目的主要支持者](../Page/eBay.md "wikilink")。

基於其低廉成本與高度可設定性，Linux常常被應用於[嵌入式系統](../Page/嵌入式系统.md "wikilink")，例如[機上盒](../Page/數位視訊轉換盒.md "wikilink")、[行動電話及](../Page/移动电话.md "wikilink")[行動裝置等](../Page/移动设备.md "wikilink")。在行動電話上，Linux已經成為[IOS的主要競爭者](../Page/IOS.md "wikilink")；而在行動裝置上，則成為[Windows
CE與](../Page/Windows_Embedded_Compact.md "wikilink")[Palm
OS外之另一個選擇](../Page/Palm_OS.md "wikilink")。目前流行的[TiVo數位攝影機使用了經過客製化後的Linux](../Page/TiVo.md "wikilink")。此外，有不少硬體式的[網路防火牆及](../Page/防火墙.md "wikilink")[路由器](../Page/路由器.md "wikilink")，例如部份[LinkSys的產品](../Page/Linksys.md "wikilink")，其內部都是使用Linux來驅動、並採用了作業系統提供的防火牆及路由功能。

### 個人電腦

[Free_and_open-source-software_display_servers_and_UI_toolkits.svg](https://zh.wikipedia.org/wiki/File:Free_and_open-source-software_display_servers_and_UI_toolkits.svg "fig:Free_and_open-source-software_display_servers_and_UI_toolkits.svg")、和一些更加广为流传的[部件工具箱](../Page/部件工具箱.md "wikilink")。还有些部件对最终用户不可见，包括[D-Bus和](../Page/D-Bus.md "wikilink")[PulseAudio](../Page/PulseAudio.md "wikilink")。\]\]
轉換作業系統的開銷：自由開源意識形態與商業用途的衝突、缺乏強而有力的推廣廠商、缺乏對特殊的硬件和應用程序的支援、電腦技術人員不願再花費時間重覆學習、對已有平台的依賴，是制約Linux被採納的主要因素。

目前能在Windows或Mac
OS上執行的應用軟體大部分都沒有Linux的版本，不過在Linux平台上通常可以找到類似功能的應用軟件。大多數在Windows平台上廣泛使用的[自由軟體都有相應的Linux版本](../Page/自由软件.md "wikilink")，例如[LibreOffice](../Page/LibreOffice.md "wikilink")、[Mozilla
Firefox](../Page/Mozilla_Firefox.md "wikilink")、[Apache
OpenOffice](../Page/Apache_OpenOffice.md "wikilink")、[Pidgin](../Page/Pidgin.md "wikilink")、[VLC](../Page/VLC.md "wikilink")、[GIMP](../Page/GIMP.md "wikilink")；部分流行的[专有软件也有相應的Linux版本](../Page/专有软件.md "wikilink")，如[Adobe
Flash Player](../Page/Adobe_Flash_Player.md "wikilink")、[Adobe
Reader](../Page/Adobe_Reader.md "wikilink")、[Google
Earth](../Page/Google_Earth.md "wikilink")、[Nero Burning
ROM](../Page/Nero_Burning_ROM.md "wikilink")、[Opera](../Page/Opera電腦瀏覽器.md "wikilink")、[Maple](../Page/Maple.md "wikilink")、[MATLAB](../Page/MATLAB.md "wikilink")、[Skype](../Page/Skype.md "wikilink")、[Maya](../Page/Maya.md "wikilink")、[SPSS](../Page/SPSS.md "wikilink")、[Google
Chrome](../Page/Google_Chrome.md "wikilink")。

另外，相當多的Windows應用程式可以通過[Wine和一些基於Wine的項目如](../Page/Wine.md "wikilink")[CrossOver正常運行和工作](../Page/CrossOver.md "wikilink")。如[Microsoft
Office](../Page/Microsoft_Office.md "wikilink")、[Adobe
Photoshop](../Page/Adobe_Photoshop.md "wikilink")、[暴雪娛樂的遊戲](../Page/暴雪娛樂.md "wikilink")、[Picasa其中對於Photoshop的Crossover](../Page/Picasa.md "wikilink")（Wine）相容性工作有Disney、DreamWorks、Pixar投資支援，等。[Google大力參與Wine項目改進](../Page/Google.md "wikilink")，Picasa的GNU/Linux版本也是經Wine測試的Windows平台編譯版本。\[39\]

整個亞洲，比较缺乏對Linux的支援，硬體和應用程式皆只考量微軟作業系統設計的需求。例如：[Internet
Explorer及](../Page/Internet_Explorer.md "wikilink")[ActiveX需求](../Page/ActiveX.md "wikilink")、[Microsoft
Office相容性](../Page/Microsoft_Office.md "wikilink")、[網絡遊戲以及一般用戶都傾向於使用](../Page/網絡遊戲.md "wikilink")[Windows](../Page/Windows.md "wikilink")。

但是，Linux下也有相當多不能在[Windows平台下執行的軟體](../Page/Windows.md "wikilink")，主要是依靠[X
Window系統和其他Windows無法利用的資源](../Page/X_Window系統.md "wikilink")，或者是因為穩定性等其他方面的考慮並不準備支援Windows。不過近年來，也不斷向其移植。有如[KDE
SC](../Page/KDE_Software_Compilation_4.md "wikilink")、[Cinepaint正在進行向Windows的移植](../Page/Cinepaint.md "wikilink")。Linux使用的增多也使得Windows開源軟體(Open
Source)向Linux移植，比如[Filezilla](../Page/Filezilla.md "wikilink")。

<File:CDE> Debian Workspace 1.png|[CDE](../Page/CDE.md "wikilink")
<File:Fedora> Showing Gnome 3.22.2 showing
overview.png|[GNOME](../Page/GNOME.md "wikilink") <File:Ubuntu> Mate
traditional.png|[MATE](../Page/MATE.md "wikilink") <File:KDE> plasma
5.png|[KDE Plasma](../Page/KDE_Plasma_5.md "wikilink")
<File:XFCE-4.12-Desktop-standard.png>|[Xfce](../Page/Xfce.md "wikilink")
<File:Lxde-desktop-full.png>|[LXDE](../Page/LXDE.md "wikilink")
<File:LXQt> 0.10 - Ambiance.png|
<File:2010-04-24-133031_1280x800_scrot.png>|[Openbox](../Page/Openbox.md "wikilink")
<File:Fluxbox.png>|[Fluxbox](../Page/Fluxbox.md "wikilink")
<File:I3_window_manager_screenshot.png>|[i3](../Page/i3_\(窗口管理器\).md "wikilink")
<File:E17>
screenshot.jpg|[Enlightenment](../Page/Enlightenment.md "wikilink")
<File:App> Lens on Ubuntu
16.04LTS.png|[Unity](../Page/Unity_\(使用者介面\).md "wikilink")
<File:Linux> Mint 19 "Tara"
(Cinnamon).png|[Cinnamon](../Page/Cinnamon.md "wikilink")
<File:Elementary> OS Loki
0.4.png|[Pantheon](../Page/elementary_OS.md "wikilink")
<File:Budgie_Desktop_Environment.png>| <File:Screenshot> of Trinity
Desktop Environment (TDE) R14.0.5
Development.png|[Trinity](../Page/Trinity桌面环境.md "wikilink")
<File:Sugar-home-view-0.82.jpg>|[Sugar](../Page/Sugar_\(用户界面\).md "wikilink")

### 上网本

Linux發行版同樣在[上网本市場很受歡迎](../Page/上网本.md "wikilink")，像[ASUS Eee
PC和](../Page/Eee_pc.md "wikilink")[Acer Aspire
One](../Page/Aspire_One.md "wikilink")，銷售時安裝有訂製的Linux發行版。

### 服务器、主机和超级计算机

[LAMP_software_bundle.svg](https://zh.wikipedia.org/wiki/File:LAMP_software_bundle.svg "fig:LAMP_software_bundle.svg")的梗概。\]\]
[Linux发行版一直被用来作为服务器的操作系统](../Page/Linux发行版.md "wikilink")，并且已经在该领域中占据重要地位。根据2006年9月的报告显示，十个最大型的网络托管公司有八个公司在其Web服务器运行Linux发行版。

Linux发行版是构成[LAMP](../Page/LAMP.md "wikilink")（Linux操作系统，Apache，MySQL，Perl
/ PHP / Python）的重要部分，LAMP是一个常见的网站托管平台，在开发者中已经得到普及。

Linux发行版也经常使用作为超级计算机的操作系统，2010年11月公布的超级计算机前500强，有459个（91.8％）运行Linux发行版\[40\]。曾经是世界上最强大的超级计算机\[41\]——IBM的红杉（[IBM
Sequoia](../Page/IBM_Sequoia.md "wikilink")），已於2011年交付[勞倫斯利福摩爾國家實驗室](../Page/勞倫斯利福摩爾國家實驗室.md "wikilink")，並於2012年6月开始运作，也是选择Linux作为操作系统。\[42\]

### 嵌入式设备

[Samsung_Galaxy_Note_series_2.jpg](https://zh.wikipedia.org/wiki/File:Samsung_Galaxy_Note_series_2.jpg "fig:Samsung_Galaxy_Note_series_2.jpg")是建基於Linux的作業系統，在[智能手機上相當熱門](../Page/智能手機.md "wikilink")。\]\]
Linux的低成本、强大的定制功能以及良好的移植性能，使得Linux在嵌入式系统方面也得到广泛应用。流行的TiVo数字视频录像机还采用了定制的Linux，思科在网络[防火墙和](../Page/防火墙.md "wikilink")[路由器也使用了定制的Linux](../Page/路由器.md "wikilink")。Korg
OASYS、Korg的KRONOS、雅马哈的YAMAHA MOTIF XS/Motif
XF音乐工作站、雅马哈的S90XS/S70XS、雅马哈MOX6/MOX8次合成器、雅马哈MOTIF-RACK
XS音源模块，以及Roland RD-700GX数码钢琴均运行Linux。Linux也用于舞台灯光控制系统，如WholeHogIII控制台。

在[智能手机](../Page/智能手机.md "wikilink")、[平板电脑等移动设备方面](../Page/平板電腦.md "wikilink")，Linux也得到重要发展，基于Linux内核的[Android操作系统已经超越](../Page/Android.md "wikilink")[诺基亚的](../Page/诺基亚.md "wikilink")[Symbian操作系统](../Page/Symbian.md "wikilink")，成为当今全球最流行的智能手机操作系统。在2010年第三季度，销售全球的全部智能手机中使用[Android的占据](../Page/Android.md "wikilink")25.5%（所有的基于Linux的手机操作系统在这段时间为27.6%）。

从2007年起，手机和[掌上电脑上运行基于Linux的操作系统变得更加普遍](../Page/掌上电脑.md "wikilink")，例如：[诺基亚
N810](../Page/诺基亚_N810.md "wikilink")、[OpenMoko的Neo](../Page/OpenMoko.md "wikilink")1973、摩托罗拉的ROKR
E8。[Palm](../Page/Palm.md "wikilink")（后来被[HP公司收购](../Page/惠普.md "wikilink")）推出了一个新的基于Linux的[webOS操作系统](../Page/webOS.md "wikilink")，并使用在新生产的Palm
Pre智能手机上。

[MeeGo是诺基亚和英特尔于](../Page/MeeGo.md "wikilink")2010年2月联合推出的基于Linux的操作系统，诺基亚也推出了使用MeeGo操作系统的N9手机。2011年9月28日，继诺基亚宣布放弃开发MeeGo之后，英特尔也正式宣布将MeeGo与LiMo合并成为新的系统[Tizen](../Page/Tizen.md "wikilink")。[Jolla](../Page/Jolla.md "wikilink")
Mobile公司成立并推出了由MeeGo发展而来的[Sailfish操作系统](../Page/Sailfish.md "wikilink")。2012年，[Mozilla推出基於Linux核心的](../Page/Mozilla基金會.md "wikilink")[Firefox
OS操作系统](../Page/Firefox_OS.md "wikilink")\[43\]。

### 使用比例

很多開源軟體的定量研究聚焦在市場佔有率（雖然多數不算作市場中的商品）和可靠性，包括數不清的Linux分析報告。Linux份額成長迅速。[IDC的](../Page/國際數據資訊公司.md "wikilink")2008年第二季度報告指出，Linux在所有伺服器市場的市佔率已經達到29%，比該研究機構在2007年的調查大幅增加了12個百分點。這個估計數字基於Linux伺服器的銷售額。

所以，Linux的市佔率是不可小覷的。Netcraft報告，在2010年二月，每10台可靠的伺服器中[Linux發行版占](../Page/Linux發行版.md "wikilink")6台，[FreeBSD占](../Page/FreeBSD.md "wikilink")2台，[Windows占](../Page/Windows.md "wikilink")1台。而[Debian](../Page/Debian.md "wikilink")、[CentOS兩個至關重要的發行版並不銷售](../Page/CentOS.md "wikilink")。

## 版权，商标和命名

*Linux*的[註冊商標是Linus](../Page/商标.md "wikilink")
Torvalds所有的。這是由於在1996年，一個名字叫做[William R.
Della
Croce的律師開始向各個Linux發佈商發信](../Page/William_R._Della_Croce.md "wikilink")，聲明他擁有*Linux*[商標的所有權](../Page/商标.md "wikilink")，並且要求各個發佈商支付版稅，這些發行商集體進行上訴，要求將該註冊商標重新分配給Linus
Torvalds。Linus Torvalds一再聲明Linux是自由且免費的，他本人可以賣掉，但Linux絕不能賣。

“GNU/Linux”此名稱是[GNU计划的支持者与开发者](../Page/GNU.md "wikilink")，特别是其创立者[理查德·斯托曼對於Linux作業系統的主張](../Page/理查德·斯托曼.md "wikilink")。由于此类操作系统使用了众多GNU程序，包含[Bash](../Page/Bash.md "wikilink")（[Shell程式](../Page/Unix_shell.md "wikilink")）、[函式庫](../Page/函式庫.md "wikilink")、[編譯器等等作為Linux核心上的系統套件](../Page/編譯器.md "wikilink")，理查德·斯托曼认为应该将该操作系统称为「GNU/Linux」或「GNU+Linux」較為恰当，但現今多數人仍稱其為Linux。就1997年之前的Linux來看，一間CD-ROM的供應商所提供的資料顯示在他们的“Linux
发行版”中，GNU 软件所占最大的比重，大约占全部源代码的28%，且还包括一些关键的部件，如果没有这些部件，系统就无法工作，而Linux
本身占大约3%。\[44\]

Linux社群中的一些成員，如[埃里克·雷蒙](../Page/埃里克·雷蒙.md "wikilink")、[林纳斯·托瓦兹等人](../Page/林纳斯·托瓦兹.md "wikilink")，偏好Linux的名稱，认为Linux朗朗上口，短而好记，拒绝使用「GNU/Linux」作为操作系统名称。并且认为Linux並不屬於[GNU計劃的一部份](../Page/GNU計劃.md "wikilink")，斯托曼直到1990年代中期Linux开始流行后才要求更名。有部分[Linux套件](../Page/Linux套件.md "wikilink")，如[Debian](../Page/Debian.md "wikilink")，采用了「GNU/Linux」的称呼。但大多数商业Linux套件依然将操作系统称为Linux。而有些人则认为「操作系统」一词指的只是系统的内核(Kernel)，其他程序都只能算是[应用软件](../Page/应用软件.md "wikilink")，因而，该操作系统应叫Linux，但Linux系统套件是在Linux內核的基础上加入各种GNU软件套件集合而成的。

在這兩個主要名稱之外，也有其他名稱的提議。1992年，[Yggdrasil
Linux主張命名為Linux](../Page/Yggdrasil_Linux.md "wikilink")/GNU/X，因為除了GNU計劃軟體之外，很多Linux還採用了[X视窗系统](../Page/X_Window系統.md "wikilink")。

## 評價

### 正面

  - 開放原始碼的Linux可以讓知識延續下去，新興的軟體公司可以從開放原始碼上快速、低價的建立專業能力，豐富市場的競爭，防止獨霸軟體巨獸的存在。
  - 個人使用很少有版權問題，絕大多數都是免費使用，幾乎無所謂盜版問題。
  - 新的[Linux發行版大多數軟體都有伺服器的服務](../Page/Linux發行版.md "wikilink")，只要點選就可以自動下載、安裝經過認證的軟體，不需要到市面購買、安裝。
  - Linux學習的投資有效時間較長。舊版軟體、系統都還是存在，有原始碼可以衍生、分支，維護週期普遍比[Windows長很多](../Page/Windows.md "wikilink")。就算被放棄，還是可以憑藉原始碼衍生。新的軟體更新發展多樣化，容易養成使用者習慣掌握原理，而不是養成操作習慣。
  - 強大的[Shell及](../Page/Shell.md "wikilink")[脚本支援](../Page/脚本语言.md "wikilink")，容易組合出符合需求的環境或創造自動程序。
  - 預設安全設定相對於目前主流的[Windows](../Page/Windows.md "wikilink")[作業系統安全很多](../Page/作業系統.md "wikilink")。\[45\]Windows作業系統為了非專業使用者降低了預設安全性的設定，導致系統容易受到木馬、病毒的侵害。[盜版的Windows更糟糕](../Page/盗版.md "wikilink")，可能隨盜版作業系統捆绑[木馬](../Page/特洛伊木马_\(电脑\).md "wikilink")、[惡意程式](../Page/恶意软件.md "wikilink")，部分預設[超級使用者](../Page/超级用户.md "wikilink")（Administrator）登录、關閉安全更新等問題導致安全性更差。

### 負面

  - [BSD的開發人員曾經批評過Linux核心開發人員過於重視新功能的添加而不是踏踏實實的把程式碼寫好](../Page/BSD.md "wikilink")、整理好。
  - [Minix愛好者認為](../Page/Minix.md "wikilink")[微核心是將來技術發展的方向](../Page/微核心.md "wikilink")，Linux在技術上是落伍陳舊的。（参见[塔能鲍姆-林纳斯辩论](../Page/塔能鮑姆-托瓦茲辯論.md "wikilink")）
  - 軟硬體支援性較差。大部份的軟、硬體廠商沒有或者不會優先開發Linux平台的版本，或者Linux平台的版本功能較少，致使可用的應用程式、硬體周邊支援性相較於[Windows](../Page/Windows.md "wikilink")、[Mac平台差](../Page/Mac.md "wikilink")。
  - 相當多的發行版（超過200個以上），使程式開發者無法針對所有發行版做測試，使用Linux平台的應用軟體安裝在非主流發行版可能遭遇預料之外的問題或甚至於無法使用。
  - Linux系統及相關應用軟體主要是由[黑客等](../Page/黑客.md "wikilink")[程式設計師及其它Linux愛好者共同合作開發出來的](../Page/程式設計師.md "wikilink")，所以缺少了[商業軟體基於商業利益而調整操作界面使之更適合不同使用者的行为](../Page/商業軟體.md "wikilink")。对Linux使用方式的不習慣，以及不同軟體操作方式缺乏一致性使得使用者產生Linux系統難以使用的感受。

## 參考文獻

### 引用

### 来源

  -
  -
  -
  -
  -
## 外部連結

  - [Linux内核官方网站](https://www.kernel.org/)
  - [Linux基金会官方网站](https://www.linuxfoundation.org/)
  - [GNU项目官方网站](https://www.gnu.org/)
  - [The Linux文档项目](https://www.tldp.org/)
  - [Linux25 週年：專訪 Linux 之父 Linus
    Torvalds](https://www.inside.com.tw/2016/04/04/linux-at-25-qa-with-linus-torvalds)

## 参见

  - [作業系統列表](../Page/作業系統列表.md "wikilink")、[Microsoft
    Windows与Linux的比较](../Page/Microsoft_Windows與Linux的比較.md "wikilink")
  - [Linux内核](../Page/Linux内核.md "wikilink")
  - [Linux發行版](../Page/Linux發行版.md "wikilink")：[Linux發行版列表](../Page/Linux发行版列表.md "wikilink")
  - [Tux](../Page/Tux.md "wikilink")
  - [Linux的採用](../Page/Linux的採用.md "wikilink")

{{-}}

[Category:计算平台](../Category/计算平台.md "wikilink")
[Linux](../Category/Linux.md "wikilink")
[Category:自由作業系統](../Category/自由作業系統.md "wikilink")
[Category:LAMP](../Category/LAMP.md "wikilink")

1.  {{ cite newsgroup | title = Free minix-like kernel sources for
    386-AT | author = Linus Benedict Torvalds | date = 1991-10-05 |
    newsgroup = comp.os.minix | id = | url =
    <https://groups.google.com/group/comp.os.minix/msg/2194d253268b0a1b?pli=1>
    | accessdate =2011-09-30 }}

2.

3.  {{ cite book | url =
    <https://www.tldp.org/LDP/sag/html/sag.html#GNU-OR-NOT> | title =
    Linux System Administrator's Guide | chapter = 1.1 | edition =
    version 0.9 | year = 2004 | accessdate =2007-01-18 | first = Alex |
    last = Weeks }}

4.

5.

6.

7.  [1](http://www.shortfamilyonline.com/tech/unix/history-of-linux/reference/23-Apr-1992-re-how-to-pronounce-linux.html)

8.

9.

10.

11.

12.

13. {{ cite newsgroup | title = What would you like to see most in
    minix? | newsgroup = comp.os.minix | id =
    1991Aug25.205708.9541@klaava.Helsinki.FI | url =
    <https://groups.google.com/group/comp.os.minix/msg/b813d52cbc5a044b>
    | last = Torvalds | first = Linus | accessdate = 2006-09-09 }}

14.

15.

16.
17.

18.

19.

20.

21.

22.

23.

24.

25.

26.

27.

28.

29.

30.

31.

32.

33.

34.

35.

36.

37.

38.

39.

40.

41.

42.

43.

44. [Linux和GNU](http://www.gnu.org/gnu/linux-and-gnu.html)

45.