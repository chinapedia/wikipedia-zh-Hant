**Linux中文文件計劃**目的在為[Linux作業系統提供豐富](../Page/Linux.md "wikilink")，完整，高品質的中文文件。但目前已停止更新。

## 緣起

90年代中期，Linux逐漸在[台灣風行](../Page/台灣.md "wikilink")，不過仍是學生以及電腦高手的玩物。中文文件的缺乏，使得Linux的推廣與普及誠屬不易。有鑑於此，當時任職於[晶誠興業工程師的](../Page/晶誠興業.md "wikilink")[黃志偉](../Page/黃志偉.md "wikilink")，開始[翻譯](../Page/翻譯.md "wikilink")[Linux文件計劃](../Page/Linux文件計劃.md "wikilink")（LDP）上的HOWTOs文件，並透過[BBS等論壇](../Page/BBS.md "wikilink")，號召熱心志工加入。隨後[黃志偉將此計劃命名為](../Page/黃志偉.md "wikilink")「Linux中文文件計劃」，英文簡稱CLDP。

## 歷史

  - 1997年8月，Linux中文文件計劃正式開始。黃志偉完成了HOWTO-INDEX、Chinese
    HOWTO等文件翻譯。許多Linux玩家開始熱心響應。
  - 1997年10月，CLDP的[ftp站台建置於](../Page/ftp.md "wikilink")[台大](../Page/國立臺灣大學.md "wikilink")[物理系](../Page/物理系.md "wikilink")，[sunsite開始mirror這些中譯文件](../Page/sunsite.md "wikilink")，使得CLDP成為LDP計劃的一部分。隨後，[大眾網路提供網站空間和頻寬供CLDP使用](../Page/大眾網路.md "wikilink")，網址正式改為http://www.linux.org.tw/CLDP／。同月也開始提供[簡體字版本](../Page/簡體.md "wikilink")。
  - 1997年11月，成立[通信論壇](https://web.archive.org/web/20080510072855/http://www.linux.org.tw/CLDP/OLD/mailing.html)：ldp-trans@linux.org.tw
  - 1998年1月，黃志偉等改寫Chinese
    HOWTO，成為第一份關於Linux中文化技術的參考文件。這也是第一份中英文同步發佈的HOWTO文件。
  - 1998年5月，首度網頁格式改版。更為清楚而易於閱讀。同時也開始加入非HOWTOs的翻譯或創作中文文件。
  - 1998年7月，新增許多映射站台，包括[中國大陸地區](../Page/中國大陸.md "wikilink")。[CLDP
    FAQ](https://web.archive.org/web/20080421131113/http://www.linux.org.tw/CLDP/OLD/faq.html)完成。
  - 1998年8月，黃志偉發表[SGMLtools中文套件](https://web.archive.org/web/20080421131118/http://www.linux.org.tw/CLDP/OLD/zh-sgmltools.html)v0.7，以解決中文[SGML文件的轉換問題](../Page/SGML.md "wikilink")。
  - 1998年9月，開始提供[rpm套件包裝格式](../Page/RPM套件管理員.md "wikilink")。安裝rpm後可得到所有翻譯文件。
  - 1998年11月，ftp主站台移至ftp.linux.org.tw，由[中央研究院提供的網站主機與頻寬](../Page/中央研究院.md "wikilink")。
  - 1998年12月，搜尋引擎上線，由[吳慶鴻協助架設](../Page/吳慶鴻.md "wikilink")。
  - 1999年8月，[GNU GPL中譯版上線](../Page/GNU_GPL.md "wikilink")。
  - 1999年12月，[翻譯統計](https://web.archive.org/web/20080504154923/http://www.linux.org.tw/CLDP/OLD/summary.html)上線。
  - 2000年1月，SGMLtools中文套件v1.1版發佈。
  - 2002年12月，宣佈重新改版。
  - 2003年起，改由[citybjc接手此計劃](../Page/citybjc.md "wikilink")，並正式改版。

## 成果

根據該網站至2003年二月為止的統計，已翻釋或翻譯中的HOWTOs和mini-HOWTOs約兩百五十餘篇，完成度約百分之五十。

此計劃最活躍時期約為1998-1999年間。當時有數十名志工的參與，完成上百篇翻譯文章。在台灣各[大學院校](../Page/大學.md "wikilink")、[香港](../Page/香港.md "wikilink")、[中國大陸等地區都有映射站台](../Page/中國大陸.md "wikilink")\[1\]。至少兩家出版社將CLDP文件[集結出書](../Page/#出版.md "wikilink")。成為華文地區最大型的[自由軟體計劃](../Page/自由軟體.md "wikilink")，對Linux的[中文化與普及化幫助很大](../Page/中文化.md "wikilink")。計劃主持人[黃志偉亦被評選為中國Linux傑出代表人物之一](../Page/黃志偉.md "wikilink")\[2\]。

然而自2000年起，隨著Linux的商業化發展日益成熟，各類中文Linux資訊網站日漸增加，商業出版品也不虞匱乏，使得此計劃重要性日減。另一方面有些[譯者未能及時更新文件](../Page/譯者.md "wikilink")，使得內容過時與老舊，也讓此計劃參考性大失。

2003年由[citybjc接手後](../Page/citybjc.md "wikilink")，改弦易轍，以翻譯Linux電子報（[Linux
Gazette](../Page/Linux_Gazette.md "wikilink")）為主。然而原先翻譯的HOWTOs文件並未重新上線，使得前人努力無法延續。同時也未積極號召新血加入。終使此計劃成為一人計劃，翻譯進度嚴重落後。2003年三月左右已不見更新。

所幸由台灣中央研究院所提供的網站主機與頻寬，在專人與社群志工維護下，所有資料得以保存至今。部分資料仍具參考價值（如[GNU
GPL](https://web.archive.org/web/20080418035153/http://www.linux.org.tw/CLDP/OLD/doc/GPL.html)之中譯）。

## 出版

已知以CLDP內容集結成書的有：

  - [百資科技股份有限公司](http://www.linpus.com)出版
      - Linux HOWTO ISBN 957-97830-1-2
      - Linux mini-HOWTO ISBN 957-97830-2-0
  - [松崗電腦圖書資料股份有限公司](http://www.unalis.com.tw)出版
      - Linux技術參考手冊 系統·雜類篇ISBN 957-22-3225-8
      - Linux技術參考手冊 週邊設備·網路篇ISBN 957-22-3236-3

## 類似計劃

  - [Linux中文延伸套件計劃](../Page/Linux中文延伸套件計劃.md "wikilink")

## 参考文献

## 外部連結

  - [CLDP舊版](https://web.archive.org/web/20090120025549/http://www.linux.org.tw/CLDP/OLD/)
  - [CLDP新版](https://web.archive.org/web/20080421183423/http://www.linux.org.tw/CLDP/)
  - [The Linux Documentation Project](http://www.tldp.org)

[Category:翻譯](../Category/翻譯.md "wikilink")
[Category:自由軟體](../Category/自由軟體.md "wikilink")
[Category:Linux軟體](../Category/Linux軟體.md "wikilink")

1.  [台灣的開放源碼運動](http://apt.nc.hcc.edu.tw/pub/FreeSoftware/%AE%D5%B6%E9%A6%DB%A5%D1%B3n%C5%E9%A6h%A4%B8%B1%C0%BCs%C0%B3%A5%CE%BA%F4%AD%B6/open_tw.html)

2.  [中國Linux的傑出人物](http://blog.csdn.net/ramacess/archive/2005/09/29/492470.aspx)