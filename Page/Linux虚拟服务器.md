**Linux虛擬服务器**（Linux Virtual
Server，LVS）是一個虛擬的服务器集群系统，用于实现[负载平衡](../Page/负载平衡.md "wikilink")。项目在1998年5月由[章文嵩成立](../Page/章文嵩.md "wikilink")，是中国国内最早出现的[自由软件项目之一](../Page/自由软件.md "wikilink")。

## 目标

[Wikimedia_Server_Architecture_(simplified).svg](https://zh.wikipedia.org/wiki/File:Wikimedia_Server_Architecture_\(simplified\).svg "fig:Wikimedia_Server_Architecture_(simplified).svg")

  - 使用集群技术和Linux操作系统实现一个高性能、高可用的服务器。
  - 很好的可伸缩性。
  - 很好的可靠性。
  - 很好的可管理性。
  - 目前已是[Linux內核的一部分](../Page/Linux內核.md "wikilink")，可通過ipvsadm管理LVS。

## 外部链接

  - <http://www.linuxvirtualserver.org/>

[Category:网络软件](../Category/网络软件.md "wikilink")
[Category:并发计算](../Category/并发计算.md "wikilink")