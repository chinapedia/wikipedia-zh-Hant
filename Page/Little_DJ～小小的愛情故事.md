《**Little
DJ～小小的愛情故事**》（****）是一本[POPLAR社於](../Page/POPLAR社.md "wikilink")2007年出版的[日本小說](../Page/日本小說.md "wikilink")（由[鬼塚忠所著](../Page/鬼塚忠.md "wikilink")），以及一套改編自此小說的[電影](../Page/日本電影.md "wikilink")。

## 小說

## 電影

電影版與小說於同一時期製作（在小說出版前的2006年9月到11月期間拍攝；2007年3月製作完成），並於2007年12月15日公映。

雖然電影版的基本情節與小說相同，然而在設定與故事上，如故事的舞台（小說-[橫須賀](../Page/橫須賀市.md "wikilink")、電影-[函館](../Page/函館市.md "wikilink")）、太郎與的年齡（原作-小學生、電影-初中生）等，亦有些不同的地方。

### 角色及演員

  - 高野太郎（初中1年級生）：[神木隆之介](../Page/神木隆之介.md "wikilink")

  - （13歲，初中2年級生）：[福田麻由子](../Page/福田麻由子.md "wikilink")

  - （29歲，電台監製）：[廣末涼子](../Page/廣末涼子.md "wikilink")

  - 高崎太郎（太郎的主診醫生，通稱年輕醫生）：[佐藤重幸](../Page/戶次重幸.md "wikilink")（現名戶次重幸）

  - （太郎的叔母，護士）：[村川繪梨](../Page/村川繪梨.md "wikilink")

  - 中村捨次（與太郎同房的住院病人）：[松重豐](../Page/松重豐.md "wikilink")

  - 結城道夫（與太郎同房的住院病人）：[光石研](../Page/光石研.md "wikilink")

  - （與太郎同房的住院病人）：[森康子](../Page/森康子.md "wikilink")

  - 結城周平（結城的兒子）：[賀來賢人](../Page/賀來賢人.md "wikilink")

  - 護養教師：

  - 中嶋洋子（護士）：[池田香織](../Page/池田香織.md "wikilink")

  - 海乃惠（的母親）：[宮田早苗](../Page/宮田早苗.md "wikilink")

  - 結城的妻子：[長谷川紀子](../Page/長谷川紀子.md "wikilink")

  - （太郎的友人，原名倉一）：[十川史也](../Page/十川史也.md "wikilink")

  - （兒童病房的病人）：[土屋匠](../Page/土屋匠.md "wikilink")

  - （的弟弟、兒童病房的病人）：[中村咲哉](../Page/中村咲哉.md "wikilink")

  - DJ：

  - 部長：[近江谷太朗](../Page/近江谷太朗.md "wikilink")

  - ：[小林且彌](../Page/小林且彌.md "wikilink")

  - ：[小橋亞樹](../Page/小橋亞樹.md "wikilink")

  - 男病人們：[俄克拉荷馬](../Page/俄克拉荷馬_\(漫才師\).md "wikilink")

  - 尾崎誠（電台DJ）：[小林克也](../Page/小林克也.md "wikilink")（特別演出）

  - （太郎的母親）：[西田尚美](../Page/西田尚美.md "wikilink")

  - 高野正彥（太郎的父親）：[石黑賢](../Page/石黑賢.md "wikilink")

  - 高崎雄二（院長，通稱大醫生）：[原田芳雄](../Page/原田芳雄.md "wikilink")

### 製作人員

  - 原作：[鬼塚忠](../Page/鬼塚忠.md "wikilink")《Little DJ
    小小的愛情故事》（[POPLAR社出版](../Page/POPLAR社.md "wikilink")）
  - 導演：[永田琴](../Page/永田琴.md "wikilink")
  - 編劇：[三浦有為子](../Page/三浦有為子.md "wikilink")、永田琴
  - 音樂：[佐藤直紀](../Page/佐藤直紀.md "wikilink")
  - 監製：[森谷雄](../Page/森谷雄.md "wikilink")、[千葉伸大](../Page/千葉伸大.md "wikilink")、[遠藤日登思](../Page/遠藤日登思.md "wikilink")
  - 攝影：[福本淳](../Page/福本淳.md "wikilink")
  - 照明：[市川德充](../Page/市川德充.md "wikilink")
  - 美術：[佐佐木記貴](../Page/佐佐木記貴.md "wikilink")
  - 錄音：[渡邊真司](../Page/渡邊真司.md "wikilink")
  - 編輯：[今井剛](../Page/今井剛.md "wikilink")
  - 影像編輯：[石上正治](../Page/石上正治.md "wikilink")
  - 執行製作：[須永裕之](../Page/須永裕之.md "wikilink")
  - 助導：[宮下健作](../Page/宮下健作.md "wikilink")
  - 劇本指導：[增田千尋](../Page/增田千尋.md "wikilink")
  - 服裝：[高橋智加江](../Page/高橋智加江.md "wikilink")、[今枝千佳](../Page/今枝千佳.md "wikilink")
  - 化妝：[清水惇子](../Page/清水惇子.md "wikilink")
  - 副監製：[盛夏子](../Page/盛夏子.md "wikilink")
  - 製作：Little DJ film partners（[Amuse Soft
    Entertainment](../Page/Amuse_Soft_Entertainment.md "wikilink")、[Amuse](../Page/Amuse.md "wikilink")、[東京電視台](../Page/東京電視台.md "wikilink")、[SKY
    Perfect Well
    Think](../Page/SKY_Perfect_Well_Think.md "wikilink")、[DesperaDo](../Page/DesperaDo.md "wikilink")、[IMAGICA](../Page/IMAGICA.md "wikilink")、POPLAR社、[atmovie](../Page/atmovie.md "wikilink")）
  - 企劃、製作公司：atmovie
  - 發行：DesperaDo

## 外部連結

  - [電影「Little DJ」官方網站](http://www.little-dj.com/)
  - [poplarbeech 小說「Little
    DJ」](https://web.archive.org/web/20071217073724/http://www.poplarbeech.com/book/index313.html)

[Category:2007年日本小說](../Category/2007年日本小說.md "wikilink")
[Category:改編成電影的日本小說](../Category/改編成電影的日本小說.md "wikilink")
[Category:醫院背景小說](../Category/醫院背景小說.md "wikilink")
[Category:橫須賀市背景作品](../Category/橫須賀市背景作品.md "wikilink")
[Category:日本電影作品](../Category/日本電影作品.md "wikilink")
[Category:2007年電影](../Category/2007年電影.md "wikilink")
[Category:2000年代愛情片](../Category/2000年代愛情片.md "wikilink")
[Category:日本愛情片](../Category/日本愛情片.md "wikilink")
[Category:北海道背景電影](../Category/北海道背景電影.md "wikilink")
[Category:東京電視台製作的電影](../Category/東京電視台製作的電影.md "wikilink")
[Category:醫院背景電影](../Category/醫院背景電影.md "wikilink")
[Category:廣末涼子](../Category/廣末涼子.md "wikilink")