《**Little Wish ～lyrical
step～**》是[日本](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")[田村由香里的第](../Page/田村由香里.md "wikilink")9張單曲，2004年10月21日由[科樂美發行](../Page/科樂美.md "wikilink")，並由[KING
RECORDS負責分銷](../Page/KING_RECORDS.md "wikilink")。商品編號為KMCM-40。

## 收錄歌曲

1.  Little Wish ～lyrical step～
      - 作詞：[椎名可憐](../Page/椎名可憐.md "wikilink")；作曲、編曲：[太田雅友](../Page/太田雅友.md "wikilink")
      - 動畫「[魔法少女奈葉](../Page/魔法少女奈葉.md "wikilink")」片尾曲
2.  永遠
      - 作詞、作曲、編曲：太田雅友
      - [文化放送系電台節目](../Page/文化放送.md "wikilink")「」結尾曲（2004年10月～2005年4月）
3.  Sweet Darlin'
      - 作詞：；作曲、編曲：[小松一也](../Page/小松一也.md "wikilink")
      - 文化放送系電台節目「」開首曲（2004年10月～2005年4月）

## ～first step～和～lyrical step～

「Little Wish」共有兩個版本，原曲「Little Wish ～first step～」（下稱「～first
step～」）收錄於2005年3月2日發行的第四張專輯「[琥珀の詩、ひとひら](../Page/琥珀の詩、ひとひら.md "wikilink")」，另一個版本「Little
Wish ～lyrical step～」（下稱「～lyrical
step～」）則收錄於此單曲和2007年3月28日發行的精選專輯「[Sincerely
Dears...](../Page/Sincerely_Dears....md "wikilink")」內。兩個歌曲版本的歌詞有少許不同。

田村在精選大碟「Sincerely Dears...」的附錄中提到自己比較喜歡「～first
step～」版本，在個人演唱會的時候也會選唱「～first
step～」的版本。但在出席「[魔法少女奈葉](../Page/魔法少女奈葉.md "wikilink")」的相關活動時則演唱「～lyrical
step～」版本。

[Category:2004年單曲](../Category/2004年單曲.md "wikilink")
[Category:田村由香里單曲](../Category/田村由香里單曲.md "wikilink")
[Category:魔法少女奈葉歌曲](../Category/魔法少女奈葉歌曲.md "wikilink")
[Category:電視動畫主題曲](../Category/電視動畫主題曲.md "wikilink")