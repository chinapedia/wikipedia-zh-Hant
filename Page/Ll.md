[Latin_LL.png](https://zh.wikipedia.org/wiki/File:Latin_LL.png "fig:Latin_LL.png")
**Ll**在多種採用[拉丁字母的語言都被當作一個單一的字母](../Page/拉丁字母.md "wikilink")。這些語言包括以下各種語言：

## ll 的各種發音

### //

在[西班牙语及](../Page/西班牙语.md "wikilink")[加泰隆尼亚语](../Page/加泰隆尼亚语.md "wikilink")，**ll**被用來標示
 這個發音。這個字母組合在很多西班牙語地區會被讀作  或
（與字母[y同音](../Page/y.md "wikilink")，稱[Yeísmo](../Page/Yeísmo.md "wikilink")）。過去“ll”都被當作一个單一字母，但在1998年和字母[ch一起被](../Page/ch.md "wikilink")[西班牙科学院取消](../Page/西班牙科学院.md "wikilink")。對於1998年以前的排序方式，稱之為“傳統西班牙語排序”，而現代不把“ll”和“ch”作特殊處理的排序，稱之為“現代西班牙語排序”。

### /l/

[阿爾巴尼亞語的情況和西班牙語相反](../Page/阿爾巴尼亞語.md "wikilink")：雖然“ll”在阿爾巴尼亞語亦是當作一個字母處ill理，但阿爾巴尼亞語的“ll”卻是用來標示／l／音，反而“l”才是用來標示滑音
 的字母。

### //

在[威爾斯語裡](../Page/威爾斯語.md "wikilink")，**ll**
代表一種[清齒齦邊擦音](../Page/清齒齦邊擦音.md "wikilink")，國際音標將它拼為一個帶有結的
l（），這個音在許多地名中都會出現，因為威爾斯語的 *llan* 正是「教堂」的意思（如
*Llanelli*「聖艾利[教堂](../Page/教堂.md "wikilink")」），也因此這些地名常很糟糕地被英語使用者發錯，尤其是那些不是住在[英國本島的人](../Page/英國.md "wikilink")。\[1\]

## 其它語言

此音在其他語言中的拼法：

  - tl：[納瓦荷語](../Page/納瓦荷語.md "wikilink")
  - [љ](../Page/љ.md "wikilink")（相當於 l
    加[硬音符號](../Page/硬音符號.md "wikilink")）：[塞爾維亞語和](../Page/塞爾維亞語.md "wikilink")[馬其頓語](../Page/馬其頓語.md "wikilink")
  - lh：[康瓦爾語](../Page/康瓦爾語.md "wikilink")、[辛達林](../Page/辛達林.md "wikilink")
  - hl：[昆雅](../Page/昆雅.md "wikilink")

## 參看

  - [l](../Page/l.md "wikilink")
  - [tl](../Page/tl.md "wikilink")
  - [lh](../Page/lh.md "wikilink")
  - [hl](../Page/Hl_\(二合字母\).md "wikilink")

## 註釋

[Category:二合字母](../Category/二合字母.md "wikilink")

1.  Everson, Michael & al. "[Proposal to add medievalist characters to
    the UCS](http://www.mufi.info/proposals/n3027-medieval.pdf) ". 30
    Jan 2006. Accessed 29 January 2013.