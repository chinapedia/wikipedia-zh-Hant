**Lotus 1-2-3**，是[Lotus
Software](../Page/Lotus_Software.md "wikilink")（美國蓮花軟體公司）於1983年起所推出的[電子試算表](../Page/電子試算表.md "wikilink")[軟體](../Page/軟體.md "wikilink")，在[DOS時期廣為](../Page/DOS.md "wikilink")[個人電腦使用者所使用](../Page/個人電腦.md "wikilink")，是一套殺手級應用軟體。但在[Windows興起後](../Page/Microsoft_Windows.md "wikilink")，[微軟強力](../Page/微軟.md "wikilink")-{zh-hans:营销;zh-hant:營銷;zh-tw:行銷;}-其[Microsoft
Office軟體](../Page/Microsoft_Office.md "wikilink")，因此Lotus 1-2-3就逐漸式微。

## 起點

Lotus，是由[米奇·卡普爾](../Page/米奇·卡普爾.md "wikilink")（Mitch
Kapor）在1982年創建的软件公司，他們推出的Lotus
1-2-3紅極一時，擊敗了微軟的[Multiplan](../Page/Multiplan.md "wikilink")，幾乎壟斷了電子試算表。Lotus
1-2-3的命名原因是它本身所擁有的三大功能：第一、強大的[試算表](../Page/試算表.md "wikilink")（spreadsheet）功能；第二、圖形整合功能；第三、簡易[資料庫功能](../Page/資料庫.md "wikilink")，故稱為1-2-3，在當時這三種功能原本是由三個不同的軟體分別來執行。事實上其本身還具有文字處理、文件管理的功能。1983年首次發行，成為IBM相容電腦的第一套殺手級應用軟體，功能多而且运算速度快，很快就成为世界上第一个销售超过100萬套的软件，當時微軟的[查尔斯·西蒙尼](../Page/查尔斯·西蒙尼.md "wikilink")（Charles
Simonyi）回憶說：“我第一次看到Lotus 1-2-3，我就知道我们遇到麻烦了（I knew we were in trouble the
first time I saw it.）”。第二版的Lotus 1-2-3，被稱為Symphony，有人叫它Lotus
1-2-3-4-5，在1-2-3的基础上又拼装了[文字处理和](../Page/文字处理.md "wikilink")[通訊功能](../Page/通訊功能.md "wikilink")。

## 微軟的反攻

1980年代後期，Lotus
1-2-3因微軟推出「[Excel](../Page/Microsoft_Excel.md "wikilink")」而受到極大威脅，1988年微軟從蓮花手上奪下12%的市場，还在不断扩大战果。Lotus
1-2-3因為是用[汇编语言撰寫的](../Page/汇编语言.md "wikilink")，在[移植上有一定的難度](../Page/軟件移植.md "wikilink")，遲至二年後才推出。微軟在[作業系統](../Page/作業系統.md "wikilink")[平台的優勢下](../Page/軟件平臺.md "wikilink")，Excel逐漸取代了Lotus
1-2-3 ，成為主流的電子試算表軟體。

## 现状

1995年6月11日，[IBM于以](../Page/IBM.md "wikilink")35亿美元收购了蓮花公司，现在的蓮花成为了IBM的一家子公司，而Lotus
1-2-3也成为了IBM公司的五大软件产品线（[Information
Management](../Page/IBM_Information_Management.md "wikilink")、Lotus、[Rational](../Page/IBM_Rational.md "wikilink")、[Tivoli](../Page/IBM_Tivoli.md "wikilink")、[WebSphere](../Page/IBM_WebSphere.md "wikilink")）之一。

## 停售

IBM Lotus 1-2-3已于2013年6月11日停止销售，软件支持服务将持续到2014年9月30日。\[1\]

## 参见

  - [Google文件](../Page/Google文件.md "wikilink")
  - [AceyOffice](../Page/AceyOffice.md "wikilink")
  - [Ability Spreadsheet](../Page/Ability_Spreadsheet.md "wikilink")
  - [EditGrid](../Page/EditGrid.md "wikilink")
  - [Framework](../Page/Framework_\(office_suite\).md "wikilink")
  - [Gnumeric](../Page/Gnumeric.md "wikilink")
  - [KSpread](../Page/KSpread.md "wikilink")
  - [OpenOffice.org Calc](../Page/OpenOffice.org_Calc.md "wikilink")
  - [Origin](../Page/Microcal_Origin.md "wikilink")
  - [Quattro Pro](../Page/Quattro_Pro.md "wikilink")
  - [The Cruncher](../Page/The_Cruncher.md "wikilink")（for MacIntosh）
  - [VisiCalc](../Page/VisiCalc.md "wikilink")
  - [WPS表格](../Page/WPS.md "wikilink")
  - [LibreOffice](../Page/LibreOffice.md "wikilink")

## 参考资源

[Category:1983年软件](../Category/1983年软件.md "wikilink")
[Category:电子制表](../Category/电子制表.md "wikilink")
[Category:莲花公司软件](../Category/莲花公司软件.md "wikilink")

1.  [Software withdrawal and discontinuance of support: Lotus SmartSuite
    , Lotus Organizer and
    Lotus 123](http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=ca&infotype=an&appname=iSource&supplier=897&letternum=ENUS913-091)