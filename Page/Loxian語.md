**Loxian**
是一種[藝術語言和](../Page/藝術語言.md "wikilink")[人工文字](../Page/人工文字.md "wikilink")(constructed
script)，其名稱的來源顯然來自[希臘語的](../Page/希臘語.md "wikilink")
*Loxos*，意思是「間接的」，是作家及作詞家[羅馬·萊恩為](../Page/羅馬·萊恩.md "wikilink")[愛爾蘭](../Page/愛爾蘭.md "wikilink")[新世紀音樂天后](../Page/新世紀音樂.md "wikilink")[恩雅於](../Page/恩雅.md "wikilink")2005年發行的專輯《[永恆之約](../Page/永恆之約.md "wikilink")（Amarantine）》所發明的語言；在該專輯裡，Loxian
共出現在三首歌曲中，分別是 *Less Than A Pearl*「微弱的存在」、*The River Sings*「大河之歌」及*Water
Shows The Hidden Heart*「水影心事」。

## 緣起

萊恩發明這個語言的原因是因為，當初她發現自己無法以[英語](../Page/英語.md "wikilink")、[愛爾蘭語或](../Page/愛爾蘭語.md "wikilink")[拉丁語等之前常出現在恩雅專輯歌曲中的語言來填出令她滿意的歌詞](../Page/拉丁語.md "wikilink")，就如恩雅本人所說，『有時候某些旋律只適合哼唱，任何語言的搭配好像都不對』，加之先前在[托爾金的](../Page/托爾金.md "wikilink")[奇幻文學電影](../Page/奇幻文學.md "wikilink")「[魔戒](../Page/魔戒.md "wikilink")」中以托爾金本人發明的[人造語言](../Page/人造語言.md "wikilink")[精靈語填詞的經驗](../Page/精靈語.md "wikilink")，因而萌生以自己發明的語言為恩雅新專輯的歌曲填詞的靈感。

根據 Loxian 的發明者萊恩的敘述，這種語言就像是一種『來自遙遠星球的未來語言』，而恩雅也形容 Loxian
人就好像一群『由彼方另一顆星球上向外觀望，好奇著『我們是否是唯一的生命』』的人，而它文字的創作靈感（見《永恆之約》的CD）則被認為是得自托爾金的人造文字、[盧恩字母以及](../Page/盧恩字母.md "wikilink")[皮特曼速記法](../Page/皮特曼速記法.md "wikilink")(Pitman
shorthand)等元素。

[羅馬·萊恩並在](../Page/羅馬·萊恩.md "wikilink")2005年12月出版了一本書 *Water Shows The
Hidden Heart*，書中除了這三首以 Loxian 創作的歌曲的背景資料，也談到此語言發明的經過\[1\]。

## 批評

[都柏林大學的英語教授](../Page/都柏林大學.md "wikilink") Terry Dolan 在檢視過 Loxian
之後，認為其語言本身含有許多其他語言的元素，例如[盎格魯撒克遜語](../Page/盎格魯撒克遜語.md "wikilink")、[北印度語](../Page/北印度語.md "wikilink")、[威爾斯語及](../Page/威爾斯語.md "wikilink")[西伯利亞](../Page/西伯利亞.md "wikilink")[尤皮克语](../Page/尤皮克语.md "wikilink")，也批評它是一種「混雜拼湊組合的[語言](../Page/語言.md "wikilink")」（very
mixum-gatherum
linguistically），毫無[文法或](../Page/文法.md "wikilink")[詞序](../Page/詞序.md "wikilink")，令人難以理解。

## 著作權

目前 Loxian
仍在[著作權的保護之下](../Page/著作權.md "wikilink")，任何未經萊恩或恩雅所屬[唱片公司](../Page/唱片公司.md "wikilink")
Aigle Records 允許的重製均會被視為違反著作權。

## 參考來源

## 外部連結

  - [Loxian on the
    BBC](http://news.bbc.co.uk/2/hi/uk_news/northern_ireland/4445716.stm)
  - [More about Loxian](http://enya.szm.com/amarantine/loxian.htm)
  - [The Sunday Times -
    Ireland](http://www.timesonline.co.uk/article/0,,2091-1892820,00.html)
  - ["Amarantine" press
    release](https://web.archive.org/web/20060217174240/http://xml.sys-con.com/read/150110.htm)

[Category:人工語言](../Category/人工語言.md "wikilink")

1.