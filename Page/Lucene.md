**Lucene**是一套用于[全文检索和](../Page/全文检索.md "wikilink")[搜尋的](../Page/搜尋.md "wikilink")[開放源碼](../Page/開放源碼.md "wikilink")[程式庫](../Page/程式庫.md "wikilink")，由[Apache软件基金会支持和提供](../Page/Apache软件基金会.md "wikilink")。Lucene提供了一個簡單卻強大的應用程式介面，能夠做全文索引和搜尋，在[Java开发环境裡Lucene是一個成熟的免費開放原始碼工具](../Page/Java.md "wikilink")；就其本身而論，Lucene是現在並且是這幾年，最受歡迎的免費Java資訊檢索程式庫。

## 历史

Lucene最初是由Doug
Cutting所撰寫的，他是一位資深的全文索引及檢索專家，曾經是V-Twin搜索引擎的主要開發者，後來在Excite擔任高級系統架構設計師，目前從事於一些互联网底層架構的研究。他貢獻出Lucene的目標是為各種中小型應用程式加入全文檢索功能。

## 基於Lucene的項目

  - Apache Nutch — 提供成熟可用的网络爬虫\[1\]
  - [Apache Solr](../Page/Solr.md "wikilink") —
    基于Lucene核心的高性能搜索服务器，提供JSON/Python/Ruby
    API\[2\]
  - Elasticsearch —企业搜索平台，目的是组织数据并使其易于获取\[3\]
  - DocFetcher — 跨平台的本机文件搜索桌面程序\[4\]
  - Lucene.NET — 提供给.Net平台用户的Lucene类库的封装\[5\]
  - Swiftype — 基于Lucene的企业级搜索\[6\]
  - Apache Lucy — 为动态语言提供全文搜索的能力，是Lucene Java 库的C接口\[7\]
  - Luke — Java编写的用户界面用于编辑Lucene的索引，此项目已停止开发\[8\]

## 參見

  - [Solr](../Page/Solr.md "wikilink") －
    使用Lucene的企業搜索伺服器，亦由Apache軟件基金會所研發。

## 參考資料

## 外部連結

  - [Lucene homepage](http://lucene.apache.org/)

  - Article "[Behind the Scenes of the SourceForge.net Search
    System](https://web.archive.org/web/20060713193801/http://blog.dev.sf.net/index.php?%2Farchives%2F10-Behind-the-Scenes-of-the-SourceForge.net-Search-System.html)"
    by [Chris Conrad](../Page/Chris_Conrad.md "wikilink")

  -
  - [Simple Lucene
    Examples](https://web.archive.org/web/20070521024500/http://www.budget-ha.com/lucene)

  - [Apache Lucene popular
    APIs](http://apiwave.com/java/api/org.apache.lucene) in
    [GitHub](../Page/GitHub.md "wikilink")

[Category:Apache软件基金会](../Category/Apache软件基金会.md "wikilink")
[Category:搜索](../Category/搜索.md "wikilink")
[Category:Java](../Category/Java.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.