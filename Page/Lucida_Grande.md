**Lucida
Grande**是一种西文[无衬线体](../Page/无衬线.md "wikilink")[字体](../Page/字体.md "wikilink")，属于人文主义体。它曾是[苹果公司](../Page/苹果公司.md "wikilink")[Mac
OS
X](../Page/Mac_OS_X.md "wikilink")[操作系统的預設字体](../Page/操作系统.md "wikilink")。作为[Lucida字体家族的一员](../Page/Lucida.md "wikilink")，它的设计师是[查尔斯·毕格罗](../Page/查尔斯·毕格罗.md "wikilink")（Charles
Bigelow）和[克里斯·霍尔姆斯](../Page/克里斯·霍尔姆斯.md "wikilink")（Kris
Holmes）。它被用于1999年至2014年的macOS用户界面，以及Safari for
Windows，直到2009年5月12日发布的3.2.3版。从OS X
Yosemite（10.10版本）开始，系统字体已从Lucida
Grande更改为[Helvetica
Neue](../Page/Helvetica.md "wikilink")。\[1\]在OS X El
Capitan（10.11版本）中，系统字体再次改变为[San
Francisco](../Page/San_Francisco_\(2014年的字体\).md "wikilink")。\[2\]

和[微软](../Page/微软.md "wikilink")[Windows系统中的](../Page/Windows.md "wikilink")[Lucida
Sans
Unicode类似](../Page/Lucida_Sans_Unicode.md "wikilink")，它支持[Unicode](../Page/Unicode.md "wikilink")2.0版本中大多数常用字符。

## 脚本和编码范围

Lucida Grande目前最新版本为10.0d1e2
(2014-09-12)，包含2,826个[Unicode码](../Page/Unicode.md "wikilink")，包括[字形](../Page/字形.md "wikilink")（2,245字），涵盖了：

  - [拉丁字母](../Page/拉丁字母.md "wikilink")（[英文](../Page/英文.md "wikilink")，[丹麦文](../Page/丹麦文.md "wikilink")，[冰岛文](../Page/冰岛文.md "wikilink")，
    [凯尔特文](../Page/凯尔特.md "wikilink")，[加利西亚文](../Page/加利西亚.md "wikilink")，
    [加泰罗尼亚文](../Page/加泰罗尼亚文.md "wikilink")，
    [匈牙利文](../Page/匈牙利语.md "wikilink")，
    [南非](../Page/南非.md "wikilink")[荷兰文](../Page/荷兰文.md "wikilink")，
    [印度尼西亚文](../Page/印度尼西亚文.md "wikilink")，[夏威夷文](../Page/夏威夷语.md "wikilink")，
    [奥洛莫文](../Page/奥洛莫文.md "wikilink")，
    [威尔士文](../Page/威尔士文.md "wikilink")，
    [巴斯克文](../Page/巴斯克文.md "wikilink")，
    [德文](../Page/德文.md "wikilink")，
    [意大利文](../Page/意大利文.md "wikilink")，
    挪威[博克马尔文](../Page/博克马尔文.md "wikilink")，
    [挪威](../Page/挪威.md "wikilink")[尼诺斯克文](../Page/尼诺斯克文.md "wikilink")，
    [捷克文](../Page/捷克语.md "wikilink")，
    [斯洛伐克文](../Page/斯洛伐克语.md "wikilink")，
    [斯瓦希里文](../Page/斯瓦西里语.md "wikilink")，
    [格陵兰文](../Page/格陵兰.md "wikilink")，
    [法文](../Page/法文.md "wikilink")，
    [法罗文](../Page/法罗语.md "wikilink")，
    [波兰文](../Page/波兰文.md "wikilink")，
    [爱尔兰文](../Page/爱尔兰文.md "wikilink")，
    [瑞典文](../Page/瑞典文.md "wikilink")，
    [罗马尼亚文](../Page/罗马尼亚文.md "wikilink")，
    [芬兰文](../Page/芬兰文.md "wikilink")，
    [英文](../Page/英文.md "wikilink")，
    [荷兰文](../Page/荷兰文.md "wikilink")，
    [葡萄牙文](../Page/葡萄牙文.md "wikilink")，
    [西班牙文](../Page/西班牙文.md "wikilink")，
    [越南文](../Page/越南文.md "wikilink")，
    [马其顿文](../Page/马其顿语.md "wikilink")， 马恩岛文，
    [马来文](../Page/马来文.md "wikilink")，
    [马耳他文](../Page/马耳他.md "wikilink")）
  - [希腊字母](../Page/希腊字母.md "wikilink")
  - [西里尔字母](../Page/西里尔字母.md "wikilink")（[乌克兰文](../Page/乌克兰文.md "wikilink")，
    [乌兹别克文](../Page/乌兹别克语.md "wikilink")，
    [俄文](../Page/俄文.md "wikilink")，
    [保加利亚文](../Page/保加利亚语.md "wikilink")，
    [克罗地亚文](../Page/克罗地亚文.md "wikilink")，[哈萨克文](../Page/哈萨克文.md "wikilink")，
    [土耳其文](../Page/土耳其文.md "wikilink")，
    [塞尔维亚文](../Page/塞尔维亚语.md "wikilink")，[拉脱维亚文](../Page/拉脱维亚语.md "wikilink")，
    [斯洛文尼亚文](../Page/斯洛文尼亚语.md "wikilink")，[爱沙尼亚文](../Page/爱沙尼亚文.md "wikilink")，[白俄罗斯文](../Page/白俄罗斯文.md "wikilink")，
    [立陶宛文](../Page/立陶宛语.md "wikilink")，
    [索马里文](../Page/索马里语.md "wikilink")，[阿塞拜疆文](../Page/阿塞拜疆语.md "wikilink")，
    [阿尔巴尼亚文](../Page/阿尔巴尼亚语.md "wikilink")）
  - [阿拉伯字母](../Page/阿拉伯字母.md "wikilink")
  - [希伯来文](../Page/希伯来文.md "wikilink")
  - [泰文](../Page/泰文.md "wikilink")

## 参见

  - [Lucida Sans Unicode](../Page/Lucida_Sans_Unicode.md "wikilink")
  - [Mac OS X字体列表](../Page/Mac_OS_X字体列表.md "wikilink")
  - [Unicode字型](../Page/Unicode字型.md "wikilink")
  - [无衬线体](../Page/无衬线体.md "wikilink")

## 参考

[Category:无衬线字体](../Category/无衬线字体.md "wikilink")
[Category:蘋果公司字體](../Category/蘋果公司字體.md "wikilink")

1.  <https://www.theverge.com/2014/6/2/5773838/apple-os-x-yosemite-changes-system-font-for-first-time>
2.