**LumaQQ**是[腾讯QQ的非官方客户端程序](../Page/腾讯QQ.md "wikilink")，是一个遵循[GPL的](../Page/GPL.md "wikilink")[自由软件](../Page/自由软件.md "wikilink")，由独立的开发小组进行开发，使用[Java语言编写](../Page/Java.md "wikilink")，采用[SWT的仿](../Page/SWT.md "wikilink")[QQ界面](../Page/QQ.md "wikilink")。由于Java的[跨平台特性](../Page/跨平台.md "wikilink")，LumaQQ可以用于多种操作系统，包括[Windows](../Page/Windows.md "wikilink")、[Linux](../Page/Linux.md "wikilink")、[FreeBSD等](../Page/FreeBSD.md "wikilink")。

LumaQQ实现了腾讯QQ官方客户端的大部分功能，包括参加群聊、接收自定义图像，但还有一些功能尚没有实现，包括文件传输等。它还扩充了一些官方客户端不具备的功能，如显示[IP等](../Page/IP地址.md "wikilink")，但是LumaQQ最著名的，也是最具有争议的扩充[功能](../Page/功能.md "wikilink")，就是能够显示对方[隐身状态的](../Page/隐身.md "wikilink")“如来神掌”。

## 发布规则

LumaQQ一般情况下会不定期发布新的版本，而对于安全更新以及技术更新，则采用发布[Patch的方式](../Page/Patch.md "wikilink")，而且每一个LumaQQ的版本，都是[绿色软件](../Page/绿色软件.md "wikilink")，可以解压缩直接运行。不过LumaQQ不可以采用覆盖的方式来进行除Patch之外的升级，而需要重新安装。

## 历史版本

LumaQQ分为Java版、Mac版和iPhone版。以下是不同版本的发展历史：

  - Java版（目前，作者已经基本放弃开发此版本。\[1\]）
      - LumaQQ 2003：基于QQ 2003协议，采用[JDK](../Page/JDK.md "wikilink")
        1.4.x，开发已停止。此版本协议已过时，无法登录。
      - LumaQQ 2004：基于QQ 2004协议，采用[JDK](../Page/JDK.md "wikilink")
        1.4.x，开发已停止。此版本协议已过时，无法登录。
      - LumaQQ 2004T：基于QQ 2004协议，采用[JDK](../Page/JDK.md "wikilink")
        1.5.x，开发已停止。 此版本协议已过时，无法登录。
      - LumaQQ 2005：基于QQ 2005协议，采用[JDK](../Page/JDK.md "wikilink")
        1.5.x，开发已停止。
      - LumaQQ 2006：基于QQ 2006协议，采用[JDK](../Page/JDK.md "wikilink")
        1.5.x，开发停滞。

<!-- end list -->

  - Mac版
      - LumaQQ for Mac 2006 Farewell Version
      - LumaQQ for Mac 2007 build 200712061810

<!-- end list -->

  - iPhone
      - LumaQQ for iPhone 200712270000\[2\]

## “如来神掌”

在LumaQQ的2004版本中，增加了一个名为“如来神掌”的功能，这个功能能够显示对方是否隐身。在发布这个版本之后，LumaQQ的[下载人数突然增加](../Page/下载.md "wikilink")，也由此产生了赞成和反对的两种声音。后来[腾讯公司改进了官方](../Page/腾讯公司.md "wikilink")[QQ客户端](../Page/QQ.md "wikilink")，如来神掌已经对QQ
2005 Beta1以上的版本失效。LumaQQ 2005中已经取消了“如来神掌”的功能。

LumaQQ通过传输一个不可能拒绝的自定义表情给对方来实现探测，因为对方的QQ客户端即使在隐身时也会对这个数据包自动回应，所以可以探测到对方的隐身状态。而腾讯公司改进客户端的方式也是在隐身时禁止接受自定义表情。

由于LumaQQ是一个[自由软件](../Page/自由软件.md "wikilink")，所以作者也公布了这一个功能的主要实现[代码](../Page/代码.md "wikilink")（[Java语言](../Page/Java.md "wikilink")）。\[3\]

## 相关软件

[TextQQ是在](../Page/TextQQ.md "wikilink")[命令行](../Page/命令行.md "wikilink")（console、dos窗口）下运行的[QQ客户端](../Page/QQ.md "wikilink")，使用java技术，基于LumaQQ的核心库开发，可以跨多个平台运行。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [LumaQQ官方网站](https://web.archive.org/web/20051001071028/http://lumaqq.linuxsir.org/main/)

[Category:Java](../Category/Java.md "wikilink")
[Category:自由的即时通讯软件](../Category/自由的即时通讯软件.md "wikilink")

1.
2.
3.