**\(\mathbf{L}\!{}_\mathbf{\displaystyle Y}\!\mathbf{X}\)**是一個「所見即所指」（what
you see is what you
mean）的利用[來排版的文件編輯軟體](../Page/LaTeX.md "wikilink")。相對於其它標榜[所見即所得的編輯器而言](../Page/所見即所得.md "wikilink")，LyX標榜只顯示你真正的關心的內容。

## 功能

  - [圖形用戶界面與選單](../Page/圖形用戶界面.md "wikilink")
  - 標題、標題、段落和目錄自動編號
  - 文本按照標準的排版規則編排，如：縮進、間距及斷字
  - 標準文檔編輯功能，如：剪截/粘貼，拼法檢查（使用[GNU
    Aspell套件](../Page/GNU_Aspell.md "wikilink")）
  - 注記
  - 文本集以及模版十分類似于LaTeX中的`\documentclass[arguments]{theclass}`指令
  - 支持[BibTeX](../Page/BibTeX.md "wikilink")
  - 表單編輯器（所見即所得）
  - 數學編輯器（所見即所得）
  - 具備導入各種常用文本格式的能力[LyXScreen_Result_en.png](https://zh.wikipedia.org/wiki/File:LyXScreen_Result_en.png "fig:LyXScreen_Result_en.png")
  - 具備將文檔導出為[DocBook和](../Page/DocBook.md "wikilink")[SGML文本格式的能力](../Page/SGML.md "wikilink")

## 發音

根據該項目的Wiki網頁介紹，開發者對LyX的發音為或.\[1\]

## 版本

| 版本号   | 发布日期                 |
| ----- | -------------------- |
| 0.7.0 | October 24, 1995     |
| 1.0.0 | February 1, 1999     |
| 1.2.0 | May 29, 2002         |
| 1.3.0 | February 7, 2003     |
| 1.4.0 | March 8, 2006        |
| 1.5.0 | July 27, 2007        |
| 1.6.0 | November 10, 2008    |
| 2.0.0 | May 8, 2011.\[2\]    |
| 2.1.0 | April 25, 2014.\[3\] |
| 2.2.0 | 26 May, 2016         |
| 2.2.1 | 25 July, 2016        |
| 2.2.2 | 15 October, 2016     |
| 2.2.3 | 15 May, 2017         |
| 2.3.0 | March 16, 2018       |

除了需要额外安装的LyX的主要发行版之外，还有一个与[TeX
Live集成的称为LyTeX的非官方便携](../Page/TeX_Live.md "wikilink")（过时）版本。\[4\]

## 参见

  - [](../Page/LaTeX.md "wikilink")
  - [文字处理器列表](../Page/文字处理器列表.md "wikilink")
  - [文字处理器比较](../Page/文字处理器比较.md "wikilink")
  - [Qt](../Page/Qt.md "wikilink") toolkit，用於LyX用戶界面\[5\].

## 參考文獻

## 外部链接

  - [LyX官方网站](http://www.lyx.org/)

  - [LyX wiki](http://wiki.lyx.org/)

  - [Reviews of LyX](http://www.lyx.org/PressAboutLyX)

  - [Donations and funding](http://www.lyx.org/Donate)

  - [A comparative review of Scientific WorkPlace and
    LyX](http://www.jstatsoft.org/v17/s01) in Journal of Statistical
    Software

  - [Collection of thesis and dissertation
    LyX-templates](http://www.thesis-template.com)

  - [Self-publishing with
    LYX](https://web.archive.org/web/20110709083743/http://web.aanet.com.au/sage/lyx.pdf)

  - [LyX和XeLaTeX的配合](https://web.archive.org/web/20090327053457/http://blog.bs2.to/post/EdwardLee/8545)

  - [LyX和LaTeX
    CJK的配合](https://web.archive.org/web/20090327053148/http://blog.bs2.to/post/EdwardLee/9952)

[Category:自由软件](../Category/自由软件.md "wikilink")
[Category:使用Qt的軟體](../Category/使用Qt的軟體.md "wikilink")
[Category:桌面出版软件](../Category/桌面出版软件.md "wikilink")
[Category:自由排版软件](../Category/自由排版软件.md "wikilink")

1.  [FAQ/Pronunciation](http://wiki.lyx.org/FAQ/Pronunciation), LyX
    Wiki. Retrieved 4 April 2008.
2.
3.
4.  [LyTeX](http://code.google.com/p/lytex/)
5.  <http://wiki.lyx.org/LyX/NewInLyX15#toc13>