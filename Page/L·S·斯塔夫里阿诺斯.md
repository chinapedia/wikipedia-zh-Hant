**L·S·斯塔夫里阿诺斯**（，1913年2月5日－2004年3月23日），[加拿大出生的](../Page/加拿大.md "wikilink")[希腊裔](../Page/希腊.md "wikilink")[美国](../Page/美国.md "wikilink")[历史学家](../Page/历史学家.md "wikilink")，早年专攻[巴尔干史](../Page/巴尔干.md "wikilink")，后致力于全球史观的[通史编纂](../Page/通史.md "wikilink")，1971年出版《[全球通史](../Page/全球通史.md "wikilink")》一书。

## 生平

1913年，斯塔夫里阿诺斯出生于[加拿大](../Page/加拿大.md "wikilink")[温哥华](../Page/温哥华.md "wikilink")。他本科毕业于[不列顛哥倫比亞大學](../Page/不列顛哥倫比亞大學.md "wikilink")，在马萨诸塞州[伍斯特的](../Page/伍斯特_\(马萨诸塞州\).md "wikilink")[克拉克大學获得硕士和博士学位](../Page/克拉克大學.md "wikilink")。

1946年起，斯塔夫里阿诺斯任[美国西北大学的荣誉教授和行为科学高级研究中心的研究员](../Page/西北大学_\(美国伊利诺州\).md "wikilink")，直至1973年退休。随后他在[-{zh-hans:加利福尼亚大学圣迭戈分校;
zh-hant:聖地牙哥加利福尼亞大學;}-担任兼职教授](../Page/加利福尼亚大学圣迭戈分校.md "wikilink")，直到1992年停止教学。

斯塔夫里阿诺斯博士曾因杰出的学术成就而荣获古根海姆奖（1951年）、福特学院奖（1953年）和洛克菲勒基金奖（1967年）\[1\]。

2004年3月23日，斯塔夫里阿诺斯在[美国](../Page/美国.md "wikilink")[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[圣迭戈逝世](../Page/圣迭戈.md "wikilink")，享年91岁。

## 著作

  - 《[全球通史](../Page/全球通史.md "wikilink")》——至今已出第七版
  - 《1453年来的巴尔干各国》
  - 《奥斯曼帝国：欧洲病夫？》
  - 《即将来到的黑暗时代的前途》
  - 《全球分裂：第三世界充分发展》
  - 《源自我们过去的生命线：新世界史》

## 参考文献

{{-}}

[S](../Category/美国历史学家.md "wikilink")
[S](../Category/1913年出生.md "wikilink")
[S](../Category/2004年逝世.md "wikilink")
[S](../Category/不列顛哥倫比亞大學校友.md "wikilink")
[S](../Category/聖地牙哥加州大學教師.md "wikilink")
[Category:克拉克大學校友](../Category/克拉克大學校友.md "wikilink")
[Category:古根海姆学者](../Category/古根海姆学者.md "wikilink")

1.  [In Memoriam: Professor Leften Stavros Stavrianos.
    UCSD.](http://ucsdnews.ucsd.edu/newsrel/arts/Stavrianos.asp)
    纪念斯塔夫里阿诺斯教授