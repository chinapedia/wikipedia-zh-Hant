[NWA869Meteorite.jpg](https://zh.wikipedia.org/wiki/File:NWA869Meteorite.jpg "fig:NWA869Meteorite.jpg")
**L球粒隕石**（又稱低鐵群球粒隕石）是一種[普通球粒隕石](../Page/普通球粒隕石.md "wikilink")，亦是第二常見的[隕石](../Page/隕石.md "wikilink")。大約40%被記載的隕石屬於此類，在普通球粒隕石中，L球粒隕石佔40%\[1\]。

此類隕石含[鐵量較低](../Page/鐵.md "wikilink")，佔其重量的20-25%，其名字中的“L”代表低鐵含量（英語：***L**ow
iron
abundance*，與[H球粒隕石相對](../Page/H球粒隕石.md "wikilink")。約有4-10%的[鎳](../Page/鎳.md "wikilink")－鐵為自由金屬，因此這些隕石也具有[磁性](../Page/磁性.md "wikilink")，但不及H球粒隕石強。

L球粒隕石中含有最多的[礦物為](../Page/礦物.md "wikilink")[橄欖石和](../Page/橄欖石.md "wikilink")[紫蘇輝石](../Page/紫蘇輝石.md "wikilink")（[斜方輝石的一種](../Page/斜方輝石.md "wikilink")），還有[磁鐵礦和鎳](../Page/磁鐵礦.md "wikilink")-鐵金屬。大部分（超過60%）屬於球粒隕石岩石學分類中的第6型，暗示母天體具有足夠巨大（直徑大於100千米）來產生較強的加熱\[2\]。

與其它球粒隕石相比，很大比例的L球粒隕石曾經過嚴重的衝擊作用，天文學家認為母天體曾經歷毁滅性的撞擊。[放射性定年法指出該事件發生於約](../Page/放射性定年法.md "wikilink")5億年前\[3\]。

這類隕石的母天體仍未找到，但[愛神星](../Page/愛神星.md "wikilink")、[花神星或整個花神星族也有可能](../Page/花神星.md "wikilink")。愛神星被發現與L球粒隕石的[光譜相似](../Page/光譜.md "wikilink")，而花神星族的環境証據則包括：

1.  花神星族被認為形成於10億至5億年前；
2.  它處於小行星帶上一個容易將隕石送到地球的軌道。
3.  該族[小行星由](../Page/小行星.md "wikilink")[S-型小行星組成](../Page/S-型小行星.md "wikilink")，其成分與球粒隕石相似；
4.  花神星族的母天體花神星直徑大於100公里。

從前，L球粒隕石曾以其主要礦物而被稱為“紫蘇輝石球粒隕石”但今已不用。

## 参考文献

<div class="references-small">

<references />

  -

</div>

## 外部連結

  -
  -
[fi:L-kondriitti](../Page/fi:L-kondriitti.md "wikilink")

[Category:陨石](../Category/陨石.md "wikilink")
[Category:隕石類型](../Category/隕石類型.md "wikilink")

1.
2.
3.