**M**, **m**
是[拉丁字母中的第](../Page/拉丁字母.md "wikilink")13个[字母](../Page/字母.md "wikilink")，在古典语言和现代语言中都发双音。它起源于[希腊的](../Page/希腊.md "wikilink")[Μ,
μ](../Page/Μ.md "wikilink")。[闪族人起初用它来表述](../Page/闪族.md "wikilink")[水](../Page/水.md "wikilink")。

[漢語拼音有一個](../Page/漢語拼音.md "wikilink") Unicode
未收的帶[重音符的](../Page/重音符.md "wikilink") m
（̀\[1\]）用來表示「呣」的發音。

在一些[醫學檢驗報告中](../Page/醫學檢驗.md "wikilink")，參考值標示的M為男性（Male）的意思。\[2\]

## 字符编码

| 字符编码                               | [ASCII](../Page/ASCII.md "wikilink") | [Unicode](../Page/Unicode.md "wikilink") | [EBCDIC](../Page/EBCDIC.md "wikilink") | [摩斯电码](../Page/摩斯电码.md "wikilink") |
| ---------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- | ---------------------------------- |
| [大写](../Page/大写字母.md "wikilink") M | 77                                   | 004D                                     | 212                                    | `--`                               |
| [小写](../Page/小写字母.md "wikilink") m | 109                                  | 006D                                     | 148                                    |                                    |

## 其他表示法

## 参看

### M的變體

  - \[\] [閉後不圓唇元音](../Page/閉後不圓唇元音.md "wikilink")

### 其他字母中的相近字母

  - （[希腊字母](../Page/希腊字母.md "wikilink") Mu）

  - （[西里尔字母](../Page/西里尔字母.md "wikilink") Em）

## 注釋

<references />

[Category:拉丁字母](../Category/拉丁字母.md "wikilink")

1.  要靠[组合字符](../Page/组合字符.md "wikilink") ̀ 顯示
2.