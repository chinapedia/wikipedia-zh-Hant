**窈窕美眉**（）是由來自[挪威的](../Page/挪威.md "wikilink")[玛丽特·拉森](../Page/玛丽特·拉森.md "wikilink")（中文譯為：乖乖美眉、玛丽美眉、瑪莉特）及[瑪麗安·雷文](../Page/瑪麗安·雷文.md "wikilink")（中文譯為：好動美眉、落比美眉、瑪莉安）組成的雙人女子團體。因兩人名字開頭均為，故取名為。（已於2002年9月23日解散）

## 歷史

窈窕美眉於1998年7月和[大西洋唱片公司簽約](../Page/大西洋唱片.md "wikilink")，首支單曲*Don't Say You
Love
Me*，成為《[神奇寶貝劇場版：超夢的逆襲](../Page/神奇寶貝劇場版：超夢的逆襲.md "wikilink")》電影主題曲而在全球聲名大噪。

並在2000年發行首張專輯*Shades Of
Purple*《紫色戀情》這使她們成為挪威當年的排行榜第一名的音樂團體組合。這張專輯CD在全球共銷售了兩百萬張的好成績。

這張專輯當時在亞洲地區非常流行，為此她們還在臺灣發售版中特地將*Pretty Boy*改唱成中文版《美美少男》，來感謝樂迷的支持。

2002年發表第二張專輯*The Big Room*《白木屋之戀》專輯中，作了一些改變，將原本甜美溫柔的少女聲加入一些附有自信搖滾感的元素進來。

但由於2002年發行的*The Big
Room*專輯並不如預期，沒得到好成績，[大西洋唱片唱片公司決定和她們解約](../Page/大西洋唱片.md "wikilink")，窈窕美眉正式於2002年9月23日宣告解散。解散後，兩人各自單飛。

在解散後的隔年2003年，以窈窕美眉名義發行最後一張專輯*The Day You Went Away - The Best
Of*《珍重再見：精選＋新歌》。

## 专辑

  - Shades of Purple

`  01 Don't Say You Love Me`
`  02 The Day You Went Away`
`  03 Girl In Your Dreams`
`  04 Mirror Mirror`
`  05 Pretty Boy`
`  06 Give A Little Love`
`  07 Everything You Do`
`  08 Don't Mess With My Love`
`  09 Dear Diary`
`  10 Do You Know What You Want`
`  11 Smiling Face`
`  12 Our Song`
`  13 Why`
`  14 Pretty boy(mandarin version) 臺灣華納發行版中額外收錄中文版本`**`美美少男`**`。`

  - The Big Room

`  01 What You Do About Me`
`  02 Everything`
`  03 Wanna Be Where You Are`
`  04 Miss Popular`
`  05 Jennifer`
`  06 Don't`
`  07 Love Left For Me`
`  08 Payphone`
`  09 Leave Me Alone`
`  10 Sometimes`
`  11 Eventually`

  - The Day You Went Away: The Best of M2M

`  01 The Day You Went Away (M2M song)`
`  02 Mirror Mirror`
`  03 Pretty Boy`
`  04 Don't Say You Love Me`
`  05 Everything`
`  06 Everything You Do (re-recorded vocals version)`
`  07 Girl In Your Dreams`
`  08 What You Do About Me`
`  09 Don't (Acoustic version)`
`  10 Wanna Be Where You Are`
`  11 Not to Me`
`  12 Is You`
`  13 Wait for Me`
`  14 Jennifer (acoustic version)`
`  15 Love Left For Me (acoustic version)`
`  16 Pretty Boy (Mandarin Chinese version)`
`  17 Everything (acoustic version)`
`  18 Don't Say You Love Me (Tin Tin Out Remix)`
`  19 Todo Lo Que Haces ("Everything You Do" Spanish version)`
`  20 Mirror, Mirror (Power Dance Remix)`

## 華語翻唱

  - *The Day You Went
    Away*→[王心凌](../Page/王心凌.md "wikilink")《第一次愛的人》→二次翻唱[橙子焦糖](../Page/橙子焦糖.md "wikilink")*The
    Day You Went Away*《第一次爱的人》
  - *Pretty Boy*→[Popu Lady](../Page/Popu_Lady.md "wikilink")《一直一直愛》

## 連結

  - [Marit Larsen的个人官方网页](http://www.maritlarsen.com)
  - [Marion Raven的个人官方网页](http://www.marion-raven.com)
  - [Marit Larsen official MySpace](http://www.myspace.com/maritlarsen)
  - [Marion Raven official MySpace](http://www.myspace.com/marionraven)

[Category:挪威女子演唱团体](../Category/挪威女子演唱团体.md "wikilink")
[Category:北欧演唱团体](../Category/北欧演唱团体.md "wikilink")