**M3偵察車**（M3 Scout
Car）是[美國在](../Page/美國.md "wikilink")[二戰時期裝備的一種四輪驅動輪型](../Page/二戰.md "wikilink")[裝甲車](../Page/裝甲車.md "wikilink")，主要用於巡邏、偵察、指揮、救護和火炮牽引用途。

## 設計及歷史

M3偵察車於1938年由懷特汽車公司（White
Motor）設計，原本訂單是為[美國陸軍第](../Page/美國陸軍.md "wikilink")7騎兵旅提供64架服役，其後陸軍決定採用車體更長更闊、車頭保險槓設有拉索滾筒的改進版本，並定名為M3A1。M3A1在1941量產，至1944年終止，各種衍生型共生產了20,918輛。

M3A1可搭載8人（1名駕駛員和7名乘客），開放式車殼裝有1門.50
[白朗寧M2重機槍及](../Page/白朗寧M2重機槍.md "wikilink")2門.30
[M1919機槍](../Page/白朗寧M1919中型機槍.md "wikilink")。

它的設計影响了二戰後推出的[蘇聯](../Page/蘇聯.md "wikilink")[BTR-40及後來的](../Page/BTR-40.md "wikilink")[M2半履帶車](../Page/M2半履帶車.md "wikilink")。

## 服役

[Scout-car-british.jpg](https://zh.wikipedia.org/wiki/File:Scout-car-british.jpg "fig:Scout-car-british.jpg")
M3A1首次是在1941年至1942年的[菲律賓戰場](../Page/菲律賓.md "wikilink")，亦裝備了位於[北非戰場及](../Page/北非戰場.md "wikilink")[西西里島的美國陸軍騎兵部隊](../Page/西西里島登陸戰役.md "wikilink")，主要用作偵察、指揮和火力支援用途。直至1943年中期，由於M3A1採用開放式車殼令其防護能力低，4輪設計對山地及非平地的適應能力不足，美國陸軍在1943年開始以[M8裝甲車和](../Page/M8裝甲車.md "wikilink")[M20通用裝甲車作取代](../Page/M8裝甲車.md "wikilink")，只有小量的M3A1服役於[諾曼第及](../Page/諾曼第.md "wikilink")[太平洋戰場的](../Page/太平洋戰場.md "wikilink")[美國海軍陸戰隊二線部隊](../Page/美國海軍陸戰隊.md "wikilink")。

除了美國外，二戰時的M3A1亦有通過[租借法案交給同盟國部隊](../Page/租借法案.md "wikilink")，[蘇聯紅軍接收了](../Page/蘇聯紅軍.md "wikilink")3034輛，主要作偵察用途和作為[ZIS-3榴彈炮的火炮牽引車](../Page/ZIS-3.md "wikilink")（一直服役至1947年），而[英國和](../Page/英國.md "wikilink")[自由法國部隊則用作火炮觀測](../Page/自由法國.md "wikilink")、救護車和偵察用途，其他還有[比利時](../Page/比利時.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[柬埔寨](../Page/柬埔寨.md "wikilink")、[智利](../Page/智利.md "wikilink")、[寮國](../Page/寮國.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[葡萄牙](../Page/葡萄牙.md "wikilink")、[南越](../Page/南越.md "wikilink")、[捷克斯洛伐克](../Page/捷克斯洛伐克.md "wikilink")、[波蘭和](../Page/波蘭.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")，[國軍自](../Page/國民革命軍.md "wikilink")1942年起接收M3偵察車，用於[抗日戰爭](../Page/抗日戰爭.md "wikilink")，後來在[國共內戰當中又有若干被](../Page/國共內戰.md "wikilink")[解放軍擄獲](../Page/解放軍.md "wikilink")。

戰後大部份曾服役過的M3A1被賣至[亞洲和](../Page/亞洲.md "wikilink")[拉丁美洲國家](../Page/拉丁美洲.md "wikilink")，以色列在[獨立戰爭中也有採用](../Page/第一次中東戰爭.md "wikilink")，小數甚至加裝了頂部裝甲和旋轉式砲塔，而法國亦有將戰時的M3A1部署在法屬[阿爾及利亞和](../Page/阿爾及利亞.md "wikilink")[法屬印度支那](../Page/法屬印度支那.md "wikilink")。

最後一個使用國是[多明尼加共和國](../Page/多明尼加共和國.md "wikilink")，他們一直採用M3A1至1990年代後期。

## 使用國家

  -
  -
  -
  -
  -
  -
  -
  - \- 擄獲使用

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 型號

  - **M3**：1938年原型版本，共制造了64輛
  - **M3A1**：1941年改良型版本，加大車體及車頭保險槓加設拉索滾筒，[量產型](../Page/量產.md "wikilink")
  - **M3A1E1**：裝有美國Buda引擎公司的柴油引擎版本，共制造了100輛
  - **M3A1E2**：加裝頂部裝甲的版本
  - **M3A1E3**：加裝M3 37毫米炮的版本，沒有量產
  - **M3A1指揮車**：1943年推出的指揮車版本，裝甲加厚，裝有1門[白朗寧M2重機槍](../Page/白朗寧M2重機槍.md "wikilink")

## 参考文献

<div class="references-small">

<references />

  - *The Encyclopedia of Tanks and Armored Fighting Vehicles*, Amber
    Books, 2002
  - M. Baryatinskiy - *US APCs of World War II*, Modelist-Konstruktor,
    Bronekollektsiya 05-2004 (М.Барятинский - *Американские
    бронетранспортеры Второй мировой войны*,
    Mоделист-Конструктор, Бронеколлекция 05-2004).

</div>

## 相關條目

  - [M3半履帶車](../Page/M3半履帶車.md "wikilink")

## 外部链接

  - [WWIIvehicles.com-M3A1偵察車](http://www.wwiivehicles.com/united-states/vehicle/armored-cars/m3a1-scout-car.asp)
  - [OldCMP.net-M3偵察車圖集](https://web.archive.org/web/20051119223548/http://www.oldcmp.net/whitesc.html)
  - [PrimePortal.net-M3A1偵察車圖集](http://www.primeportal.net/trucks/m3a1_wsc.htm)

[Category:二戰美國裝甲戰鬥車輛](../Category/二戰美國裝甲戰鬥車輛.md "wikilink")
[Category:裝甲運兵車](../Category/裝甲運兵車.md "wikilink")
[Category:裝甲偵察車](../Category/裝甲偵察車.md "wikilink")
[Category:抗戰時期中國武器](../Category/抗戰時期中國武器.md "wikilink")
[Category:二戰軍用車輛](../Category/二戰軍用車輛.md "wikilink")
[Category:越戰武器](../Category/越戰武器.md "wikilink")