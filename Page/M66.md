**M66**（也稱為NGC
3627）是一個位於[獅子座的](../Page/獅子座.md "wikilink")[螺旋星系](../Page/螺旋星系.md "wikilink")，距離地球約3600萬光年。[查爾斯·梅西耶在](../Page/查爾斯·梅西耶.md "wikilink")1780年發現該天體，它與[M65](../Page/M65.md "wikilink")、[NGC
3628構成著名的](../Page/NGC_3628.md "wikilink")[獅子座三胞胎](../Page/獅子座三胞胎.md "wikilink")。

## 參考資料

## 外部連結

  - [**SEDS**: Spiral Galaxy
    M66](https://web.archive.org/web/20071211221723/http://www.seds.org/messier/m/m066.html)
  - [**WIKISKY.ORG**: SDSS image,
    M66](http://www.wikisky.org/?object=M66&img_source=SDSS&zoom=9)
  - [Spiral Galaxy M66 at the astro-photography site of **Mr. Takayuki
    Yoshida.**](http://ryutao.main.jp/english/st2k_m66_m300.html)

[Category:螺旋星系](../Category/螺旋星系.md "wikilink")
[Category:獅子座](../Category/獅子座.md "wikilink")
[066](../Category/梅西耶天體.md "wikilink")
[Category:獅子座NGC天體](../Category/獅子座NGC天體.md "wikilink")
[Category:阿普天體](../Category/阿普天體.md "wikilink")
[Category:1780年發現的天體](../Category/1780年發現的天體.md "wikilink")