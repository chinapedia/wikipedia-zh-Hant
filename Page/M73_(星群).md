**梅西爾73**（也稱為**M73**或**NGC
6994**）是位於[寶瓶座內](../Page/寶瓶座.md "wikilink")，以四顆[恆星為主體的](../Page/恆星.md "wikilink")[星群](../Page/星群.md "wikilink")。所謂星群，只是從地球上看天空中聚在一起，但彼此完全沒有關聯的恆星。M73是天空中最著名，並且曾被仔細的研究過的星群之一。

## 歷史

M73是[梅西爾在](../Page/查爾斯·梅西耶.md "wikilink")1780年10月4日發現的，他最初描述這是依個由四顆恆星與一些氣體組成的[星團](../Page/星團.md "wikilink")。然而，之後的觀測者，[約翰·赫歇爾卻未能看見任何的雲氣](../Page/約翰·赫歇爾.md "wikilink")，而且赫歇爾還注意到將M73歸類為星團是有問題的。但是，赫歇爾還是將M73編入[一般星雲星團目錄](../Page/一般星雲星團目錄.md "wikilink")（）中，[John
Dreyer重新編輯](../Page/John_Dreyer.md "wikilink")[新星雲星團表](../Page/星雲和星團新總表.md "wikilink")（）時，也仍然將M73收入。\[1\].

## 科學的研究：星群還是星團？

M73稀稀落落分布的恆星曾經被視為有潛力的[疏散星團](../Page/疏散星團.md "wikilink")，存在於該處天空的恆星彼此之間似乎有所聯繫。到底是星群還是疏散星團還引起了一場小而有趣的辯論。

在2000年，L. P. Bassino、S. Waldhausen、和R. E.
Martinez發表了對M73及其附近恆星的顏色和光度分析報告。他們認為，中央明亮的四顆星和附近一些恆星的顏色-光度關係和疏散星團的一致（參考赫羅圖），因而認為M73是一個寬約9角分的古老疏散星團\[2\]。然而，G.
Carraro根據相似的分析，也在2000年發表了結果，確認為顏色-光度之間是沒有關聯性的，因此Carraro認為M73是星群\[3\]。E.
Bica和共同研究者也加入了爭論，認為再M73中心的四顆亮星和其他的恆星被對準在相同的方向上是不太可能的，所以M73應該是一個鬆散的疏散星團\[4\]。這個爭議在2002年獲得解決，M.
Odenkirchen和C.
Soubiran發表了對M73中心6角分內恆星的高解析[光譜](../Page/光譜.md "wikilink")，結果顯示出主要的6顆恆星到地球的距離都有很大的差異，因此確定M73只是個星群\[5\]。

雖然M73只是機緣湊巧對準在一起的恆星，進一步的分析對判定是否為鬆散的疏散星團仍是很重要的。這樣的星團在展示[銀河系的](../Page/銀河系.md "wikilink")[萬有引力是如何將疏散星團的恆星剝離掉的過程中是很重要的](../Page/萬有引力.md "wikilink")。

## 位置

這個星團在天空中的位置顯示在下面[寶瓶座的星圖中](../Page/寶瓶座.md "wikilink")：

[Aquarius_constellation_map.png](https://zh.wikipedia.org/wiki/File:Aquarius_constellation_map.png "fig:Aquarius_constellation_map.png")

## 相關條目

  - [M40](../Page/M40.md "wikilink") - *是在梅西爾目錄中曾經被誤認為星雲的一對雙星。*

## 外部連結

  - [Messier 73, SEDS Messier
    pages](https://web.archive.org/web/20080315070526/http://seds.org/Messier/m/m073.html)
  - [Messier 73, LRGB CCD image based on two-hours total
    exposure](http://www.perseus.gr/Astro-DSO-NGC-6994.htm)

## 註解和參考資料

<div class="references-small">

<references/>

</div>

[Messier 073](../Category/星群.md "wikilink") [Messier
073](../Category/寶瓶座.md "wikilink")
[073](../Category/梅西耶天體.md "wikilink")
[6994](../Category/寶瓶座NGC天體.md "wikilink")
[Category:獵戶臂](../Category/獵戶臂.md "wikilink")
[Category:1780年發現的天體](../Category/1780年發現的天體.md "wikilink")

1.
2.
3.
4.
5.