**M95**（也稱為**NGC
3351**）是一個位於[獅子座的](../Page/獅子座.md "wikilink")[棒旋星系](../Page/棒旋星系.md "wikilink")，由法國天文學家[皮埃爾·梅香](../Page/皮埃爾·梅香.md "wikilink")（Pierre
Méchain）於1781年所發現，[查爾斯·梅西耶於發現](../Page/查爾斯·梅西耶.md "wikilink")4天後將它登記為[梅西耶天體](../Page/梅西耶天體.md "wikilink")。
M95也是獅子座I星系群（即M96星系群）中的一個星系，此星系群中還包括[M96與](../Page/M96.md "wikilink")[M105兩個梅西耶天體](../Page/M105.md "wikilink")。

2012年3月16日在M95內發現一顆[II型超新星SN](../Page/II型超新星.md "wikilink")
2012aw\[1\]\[2\]\[3\]。

## 參考資料

## 外部連結

  - [**SEDS**: Spiral Galaxy
    M95](https://web.archive.org/web/20071217115024/http://www.seds.org/messier/m/m095.html)
  - [**WIKISKY.ORG**: SDSS image,
    M95](http://www.wikisky.org/?object=M95&img_source=SDSS&zoom=10)
  - [**Astronomy Picture of the Day**: M95 on
    3/14/07](http://antwrp.gsfc.nasa.gov/apod/ap070314.html)

[Category:棒旋星系](../Category/棒旋星系.md "wikilink")
[Category:螺旋星系](../Category/螺旋星系.md "wikilink")
[Category:M96星系群](../Category/M96星系群.md "wikilink")
[Category:獅子座](../Category/獅子座.md "wikilink")
[095](../Category/梅西耶天體.md "wikilink") [Messier
095](../Category/NGC天體.md "wikilink")
[05850](../Category/UGC天體.md "wikilink")
[32007](../Category/PGC天體.md "wikilink")
[Category:1781年發現的天體](../Category/1781年發現的天體.md "wikilink")

1.
2.
3.