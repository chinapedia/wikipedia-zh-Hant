**MAC**，全名**Makeup Art
Cosmetics**，或寫作**M·A·C**，[加拿大](../Page/加拿大.md "wikilink")[化妝品](../Page/化妝品.md "wikilink")[品牌](../Page/品牌.md "wikilink")。

## 歷史

1985年於加拿大[多倫多由Frank](../Page/多倫多.md "wikilink") Toskan及Frank Angelo創立。

Toskan是專業[化妝師及](../Page/化妝師.md "wikilink")[攝影師](../Page/攝影師.md "wikilink")，而Angelo則是商人。二人認為在時裝攝影中沒有適合而可靠的化妝品，因此合作創辦MAC。Frank
Angelo在1997年1月12日逝世。

MAC現由化妝品牌[Estée
Lauder擁有](../Page/雅诗兰黛公司.md "wikilink")，總部位於[美國](../Page/美國.md "wikilink")[紐約市](../Page/紐約市.md "wikilink")。品牌共有4個社會公益計劃。

## 代言人

  - Sandra Bernhard
  - K.D. Lang
  - RuPaul
  - Lil Kim
  - [玛丽·布莱姬](../Page/玛丽·布莱姬.md "wikilink")
  - Shirley Manson
  - [帕米拉·安德森](../Page/帕米拉·安德森.md "wikilink") (Pamela Anderson)
  - Missy Elliott
  - Chloë Sevigny
  - [佐治童子](../Page/佐治童子.md "wikilink") ([Boy
    George](../Page/Boy_George.md "wikilink"))
  - [瑪麗蓮·曼森](../Page/瑪麗蓮·曼森.md "wikilink") (Marilyn Manson)
  - [克莉絲汀](../Page/克莉絲汀.md "wikilink") (Christina Aguilera)
  - [琳達·伊凡吉莉絲塔](../Page/琳達·伊凡吉莉絲塔.md "wikilink")（Linda Evangelista）
  - [黛安娜·羅斯](../Page/黛安娜·羅斯.md "wikilink")（Diana Ross）
  - [麗莎·明妮莉](../Page/麗莎·明妮莉.md "wikilink")（Liza Minnelli）
  - [艾頓莊](../Page/艾頓莊.md "wikilink") (Elton John)
  - Catherine Deneuve
  - Eve
  - [Dita von Teese](../Page/蒂塔·万提斯.md "wikilink")
  - [黛比·哈利](../Page/黛比·哈利.md "wikilink")
  - Lisa Marie Presley
  - Lady Gaga
  - [張藝興](../Page/張藝興.md "wikilink")

## 外部連結

  - [MAC](http://www.maccosmetics.com/)

  - [MAC Pro](http://www.macpro.com/)

  - [Specktra.Net - comprehensive MAC
    resource](http://www.specktra.net/)

  - [Mask House](http://www.mask-house.com/)

  -
  -
  -
  -
[Category:1985年成立的公司](../Category/1985年成立的公司.md "wikilink")
[Category:加拿大化妆品公司](../Category/加拿大化妆品公司.md "wikilink")