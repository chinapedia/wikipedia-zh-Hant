**MADHOUSE**（）是日本一家動畫製作公司，主要業務為動畫相關企畫、製作及著作權管理。

## 簡介

1972年10月，[丸山正雄](../Page/丸山正雄.md "wikilink")、[出崎統](../Page/出崎統.md "wikilink")、、[川尻善昭等人](../Page/川尻善昭.md "wikilink")，在公司『[蟲製作公司](../Page/蟲製作公司.md "wikilink")』出現經營危機下於成立了『MADHOUSE』（馬多浩斯），首任社長為（網田）靖夫。MADHOUSE名稱的誕生，其實是從位於千葉縣內的老牌店家「」轉變而來。這個餐廳的日文念法是「」，丸山正雄將它改成「（Maruyama
And Dezaki）」，於是定名為Madhouse\[1\]。

70年代，隨著內部成員參與東映動畫的電視動畫製作，出崎統與杉野昭夫的組合也成為『TMS』（東京Movie新社）動畫製作的核心人物。

80年代，出崎統與[杉野昭夫退出MADHOUSE另外成立](../Page/杉野昭夫.md "wikilink")『』；丸山正雄就任社長，開始主要以林太郎監督、角川書店製作的劇場版動畫及OVA作品。

90年代，從89年的[柔道英雌開始](../Page/柔道英雌.md "wikilink")，正式展開自己的電視動畫作品。98年推出[淺香守生監督的](../Page/淺香守生.md "wikilink")[庫洛魔法使後](../Page/庫洛魔法使.md "wikilink")，其製作水準讓原本主要製作OVA與劇場版的Madhouse更廣為人知。

2000年以後持續製作多元動畫，除了知名導演[川尻善昭和](../Page/川尻善昭.md "wikilink")[今敏等人的電影外](../Page/今敏.md "wikilink")，也經手[駭客任務動畫外傳等高藝術性的作品](../Page/駭客任務動畫外傳.md "wikilink")，或是富娛樂性的[銀河天使等](../Page/銀河天使.md "wikilink")。

## 作品一覽

### 電視動畫

未標示西元年者說明此作於該年度之內播畢

#### 1970年代

  - [網球甜心](../Page/網球甜心.md "wikilink")（）（總承包商：[東京電影新社](../Page/TMS娛樂.md "wikilink")，1973－1974年）

  - （）（總承包商：東京電影新社，1975年）

  - [大空魔龍](../Page/大空魔龍.md "wikilink")（）（總承包商：[東映動畫](../Page/東映動畫.md "wikilink")，協力製作，1976年－1977年）

  - （）（總承包商：，負責全127集之中的第1－52集，1976－1977年）

  - [咪咪流浪記](../Page/咪咪流浪記.md "wikilink")（）（總承包商：東京電影新社，1977－1978年）

  - [無敵小超人](../Page/鐵臂馬魯斯.md "wikilink")（）（總承包商：東映動畫，協力製作，1977年）

  - [金銀島](../Page/金銀島_\(動畫\).md "wikilink")（）（總承包商：東京電影新社，1978年－1979年）

  - [馬可·波羅遊記](../Page/馬可·波羅遊記.md "wikilink")（）（1979年－1980年）

#### 1980年代

  - [柔道英雌](../Page/以柔克剛.md "wikilink")，又譯**以柔克剛**（）（總承包商：[Kitty
    film](../Page/Kitty_film.md "wikilink")，1989年－1992年）

  - （）（總承包商：Kitty film，1989年－1990年）

#### 1990年代

  - （）（總承包商：Kitty film，1990年）

  - [DNA²](../Page/DNA².md "wikilink")（）（與[STUDIO
    DEEN共同製作](../Page/STUDIO_DEEN.md "wikilink")，1994年－1995年）

  - [幽☆遊☆白書](../Page/幽遊白書.md "wikilink")（總承包商：[Studio
    Pierrot](../Page/Studio_Pierrot.md "wikilink")，背景\[2\]、動畫
    ，1992年－1995年）

  - [小紅豆](../Page/小紅豆_\(漫畫\).md "wikilink")（）（1995年－1998年）

  - [炸彈人 彈珠人爆外傳](../Page/炸彈人_彈珠人爆外傳.md "wikilink")（）（1998年－1999年）

  - [槍神Trigun](../Page/槍神Trigun.md "wikilink")（）（1998年）

  - [庫洛魔法使](../Page/百變小櫻.md "wikilink")（）（1998年－2000年）

  - [危險調查員](../Page/危險調查員.md "wikilink")（）（1998年－1999年）

  - [麗佳公主](../Page/麗佳公主.md "wikilink")（）（協力製作：→[SSJ](../Page/Synergy_SP.md "wikilink")，1998年－1999年）

  - [亞歷山大戰記](../Page/亞歷山大戰記.md "wikilink")（）（製作人，1999年）

  - [蠟筆小新](../Page/蠟筆小新.md "wikilink")（）（總承包商：[SHIN-EI動畫](../Page/SHIN-EI動畫.md "wikilink")，各話協力製作，1999年）

  - （與[TRIANGLE STAFF共同製作](../Page/TRIANGLE_STAFF.md "wikilink")，1999年）

  - （）（1999年－2000年）

  - [十兵衛 心形眼罩的秘密](../Page/十兵衛.md "wikilink")（）（1999年）

  - [恐怖寵物店](../Page/恐怖寵物店.md "wikilink")（）（1999年）

  - [鈴鐺貓娘系列](../Page/Di_Gi_Charat.md "wikilink")（）

      - 鈴鐺貓娘（1999年）

      - （）（2002年）

      - [Di Gi Charat
        Nyo](../Page/Di_Gi_Charat.md "wikilink")（）（2003年－2004年）

#### 2000年代前半

  - 2000年

<!-- end list -->

  - [幻影死神](../Page/幻影死神.md "wikilink")（）（協力製作：TRIANGLE STAFF）

  - （）

  - （）

  - [櫻花大戰TV](../Page/櫻花大戰_\(電視動畫\).md "wikilink")（）（協力製作：TRIANGLE STAFF）

  - [第一神拳系列](../Page/第一神拳.md "wikilink")（）

      - 第1季（2000年－2002年）
      - 第2季：第一神拳 新挑戰者（）（2009年）
      - 第3季：第一神拳
        Rising（）（與[MAPPA共同製作](../Page/MAPPA.md "wikilink")，2013年－2014年）

  - （總承包商：，2000年－2003年）

<!-- end list -->

  - 2001年

<!-- end list -->

  - [戰鬥陀螺](../Page/戰鬥陀螺.md "wikilink")（）（協助製作：[Studio
    Matrix](../Page/Studio_Matrix.md "wikilink")、Seoul Animation、）

  - [銀河天使](../Page/銀河天使.md "wikilink")（）（2001年－2004年）

  - （）（與[TNK共同製作](../Page/TNK.md "wikilink")，協立製作：Studio Matrix）

  - （）

  - （）

  - [X](../Page/X_\(漫畫\).md "wikilink")（TV版）（）（2001年－2002年）

  - [足球小將GOAL\!](../Page/足球小將.md "wikilink")（第3作），又譯**足球小將
    邁向2002之路**（）（動畫製作：，2001年－2002年）

<!-- end list -->

  - 2002年

<!-- end list -->

  - [馭龍少年](../Page/馭龍少年.md "wikilink")（）（協力製作：Studio Matrix，2002年－2003年）
  - [迷糊天使](../Page/迷糊天使.md "wikilink")（）（協力製作：Studio Matrix）
  - [Chobits](../Page/Chobits.md "wikilink")（）
  - [水瓶戰記Sign for Evolution](../Page/水瓶戰記.md "wikilink")（）
  - [炎之蜃氣樓](../Page/炎之蜃氣樓.md "wikilink")（）（協力製作：Studio Matrix）
  - [爆彈小新娘](../Page/爆彈小新娘.md "wikilink")（）（與共同製作，協力製作：）
  - [花田少年史](../Page/花田少年史.md "wikilink")（2002年－2003年）
  - [阿倍野橋魔法商店街](../Page/阿倍野橋魔法商店街.md "wikilink")（）（共同製作：[GAINAX](../Page/GAINAX.md "wikilink")）

<!-- end list -->

  - 2003年

<!-- end list -->

  - （）

  -
  - [神槍少女](../Page/神槍少女.md "wikilink")（）（2003年－2004年）

  - [槍墓](../Page/槍墓.md "wikilink")（）（2003年－2004年）

  - [星球流浪記](../Page/星球流浪記.md "wikilink")（）（與共同製作，2003年－2004年）

  - （總承包商：[Studio
    Fantasia](../Page/Studio_Fantasia.md "wikilink")，各話協力製作）

  - （）

<!-- end list -->

  - 2004年

<!-- end list -->

  - [天上天下](../Page/天上天下.md "wikilink")（協助製作：DR MOVIE、VEGA
    Entertainment等）

  - [極道鮮師](../Page/極道鮮師.md "wikilink")（）

  - （）

  - [MONSTER](../Page/MONSTER.md "wikilink")（2004年－2005年）

  - [十兵衛2 西伯利亞柳生的逆襲](../Page/十兵衛.md "wikilink")（）

  - [妄想代理人](../Page/妄想代理人.md "wikilink")

  - [BECK](../Page/BECK_\(漫畫\).md "wikilink")（）（2004年－2005年）

  - [琉球武士瘋雲錄](../Page/琉球武士瘋雲錄.md "wikilink")（）（總承包商：[Manglobe](../Page/Manglobe.md "wikilink")，OP協力）

  - [薔薇少女](../Page/薔薇少女.md "wikilink")（）（總承包商：[NOMAD](../Page/Nomad_\(公司\).md "wikilink")，各話協力製作）

  - [抓鬼天狗幫](../Page/抓鬼天狗幫.md "wikilink")（）（總承包商：STUDIO
    DEEN，協助各集原畫、動畫，2004年－2005年）

#### 2000年代後半

  - 2005年

<!-- end list -->

  - [草莓100%](../Page/草莓100%.md "wikilink")（TV版）（）（協力製作：STUDIO MATRIX、DR
    TOKYO）
  - [天國之吻](../Page/天國之吻.md "wikilink")（）
  - [我太太是高中生](../Page/我太太是高中生.md "wikilink")（）（協力製作：IMAGIN）
  - [鬥牌傳說](../Page/鬥牌傳說.md "wikilink")（）（協力製作：DR MOVIE，2005－2006年）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [NANA](../Page/NANA.md "wikilink")（2006年－2007年）

  - [草莓狂熱](../Page/草莓狂熱.md "wikilink")（）（協助製作：IMAGIN）

  - [彩雲國物語](../Page/彩雲國物語.md "wikilink")（）（協力製作：IMAGIN，2006年－2008年）

  - [夢使者](../Page/夢使者.md "wikilink")（）（協助製作：DR MOVIE）

  - [企業傭兵](../Page/企業傭兵.md "wikilink")（）

  - （）

  - [死亡筆記](../Page/死亡筆記_\(動畫\).md "wikilink")（）（2006年－2007年）

  - [童話槍手小紅帽](../Page/童話槍手小紅帽.md "wikilink")（）（協力製作：Group
    Tac，2006年－2007年）

  - （與Studio Live共同製作，2006年－2007年）

  - （）（2006年－2007年）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [大江戶火箭](../Page/大江戶火箭.md "wikilink")（）
  - [怪物王女](../Page/怪物王女.md "wikilink")（2007年，協助製作：IMAGIN）
  - [惡魔獵人](../Page/惡魔獵人系列.md "wikilink")（）
  - [劍豪生死鬥](../Page/劍豪生死鬥.md "wikilink")（）
  - [獵魔戰記](../Page/獵魔戰記.md "wikilink")（）（協力製作：DR MOVIE）
  - [電腦線圈](../Page/電腦線圈.md "wikilink")（）
  - [賭博默示錄](../Page/賭博默示錄.md "wikilink")（）（協助製作：DR MOVIE，2007年－2008年）
  - [魔人偵探腦嚙涅羅](../Page/魔人偵探腦嚙涅羅.md "wikilink")（）（協助製作：Studio
    Live，2007年－2008年）
  - [楓之谷](../Page/新楓之谷.md "wikilink")（）
      - 第1季（2007－2008年）
      - 第2季：破戒録篇（2011年）
  - [意外](../Page/意外.md "wikilink")（）（與[手塚製作公司共同製作](../Page/手塚製作公司.md "wikilink")，2007年－2008年）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [假面女僕衛士](../Page/假面女僕衛士.md "wikilink")（）（協力製作：IMAGIN）

  - [奇奇的異想世界系列](../Page/奇奇的異想世界.md "wikilink")（）

      - 奇奇的異想世界
      - 奇奇的異想世界 （2009年）

  - [艾莉森與莉莉亞](../Page/艾莉森與莉莉亞.md "wikilink")（）（協力製作：手塚製作公司）

  - [最高機密](../Page/秘密_〜The_Revelation〜.md "wikilink")（）（協力製作：）

  - [海馬](../Page/海馬_\(電視動畫\).md "wikilink")（）

  - （）（協力製作：手塚製作公司）

  - [魍魎之匣](../Page/魍魎之匣.md "wikilink")（）（2008年）

  - [ONE OUTS 超智遊戲](../Page/ONE_OUTS.md "wikilink")（）（2008年－2009年）

  - [CHAOS;HEAD](../Page/CHAOS;HEAD.md "wikilink")（）（協力製作：IMAGIN）

  - [再造人卡辛](../Page/再造人卡辛.md "wikilink")（）（2008年－2009年）

  - [黑塚](../Page/黑塚.md "wikilink")（）

  - [星際寶貝系列](../Page/星際寶貝：神奇大冒險.md "wikilink")（）

      - 星際寶貝（協力製作：MOI ANIMATION、DR MOVIE，2008年－2009年）
      - 星際寶貝 （2009年－2010年）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [背騎少女](../Page/背騎少女.md "wikilink")（）
  - [蒼天航路](../Page/蒼天航路.md "wikilink")（協力製作：）
  - [超能力大戰](../Page/超能力大戰.md "wikilink")（）（協力製作：IMAGIN）
  - [青澀文學系列](../Page/青澀文學系列.md "wikilink")（）（2009年）
  - [奇蹟少女KOBATO](../Page/奇蹟少女KOBATO.md "wikilink")（）（2009年－2010年）

#### 2010年代前半

  - 2010年

<!-- end list -->

  - [四疊半神話大系](../Page/四疊半神話大系.md "wikilink")（）
  - [少年犯之七人](../Page/少年犯之七人.md "wikilink") （）（協力製作：Studio Live）
  - [學園默示錄](../Page/學園默示錄.md "wikilink")（）
  - [世紀末超自然學院](../Page/世紀末超自然學院.md "wikilink")（）（總承包商：[A-1
    Pictures](../Page/A-1_Pictures.md "wikilink")，ED協力）
  - [漫威娛樂電視動畫影集](../Page/漫威娛樂.md "wikilink")：
      - [鋼鐵人](../Page/鋼鐵人.md "wikilink")（）（）（[漫威娛樂](../Page/漫威娛樂.md "wikilink")，協力製作：DR
        MOVIE）
      - [金鋼狼](../Page/金鋼狼.md "wikilink")（）（）（2011年）
      - [X戰警](../Page/X戰警.md "wikilink")（）（）（2011年）
      - [刀鋒戰士](../Page/幽靈刺客.md "wikilink")（[2011年電視動畫](../Page/刀锋战士_\(2011年动画\).md "wikilink")）（）（2011年）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [魔法少女小圓](../Page/魔法少女小圓.md "wikilink")（）（總承包商：[SHAFT](../Page/SHAFT.md "wikilink")，各話協力製作企劃）
  - [A頻道](../Page/A頻道.md "wikilink")（）（總承包商：[Studio五組](../Page/Studio五組.md "wikilink")，各話協力製作）
  - [花牌情緣系列](../Page/花牌情緣.md "wikilink")（）
      - 第1季（2011年－2012年）
      - 第2季（2013年）
  - [HUNTER×HUNTER
    (2011年日本電視台版)](../Page/HUNTER×HUNTER.md "wikilink")（協力製作：Studio
    Live，2011年－2014年）

<!-- end list -->

  - 2012年

<!-- end list -->

  - （2012年－）

  - [織田信奈的野望](../Page/織田信奈的野望.md "wikilink")（）（與Studio五組共同製作）

  - [輪迴的拉格朗日
    第2季](../Page/輪迴的拉格朗日.md "wikilink")（）（總承包商：[XEBEC](../Page/XEBEC.md "wikilink")，各話協力製作）

  - [驚爆遊戲](../Page/BTOOOM!.md "wikilink")（）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [戀曲寫真](../Page/戀曲寫真.md "wikilink")（）
  - [神不在的星期天](../Page/神不在的星期天.md "wikilink")（）
  - [鑽石王牌](../Page/鑽石王牌.md "wikilink")（）（與[Production
    I.G共同製作](../Page/Production_I.G.md "wikilink")）
      - 第1季（2013年－2015年）
      - 第2季（2015年－2016年）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [魔法戰爭](../Page/魔法戰爭.md "wikilink")（）
  - [魔法科高中的劣等生](../Page/魔法科高中的劣等生.md "wikilink")（）
  - [NO GAME NO LIFE 遊戲人生](../Page/NO_GAME_NO_LIFE_遊戲人生.md "wikilink")（）
  - [花舞少女](../Page/花舞少女.md "wikilink")（）
  - [寄生獸 生命的準則](../Page/寄生獸.md "wikilink")（）（2014年－2015年）

#### 2010年代後半

  - 2015年

<!-- end list -->

  - [死亡遊行](../Page/死亡遊行.md "wikilink")（）
  - [俺物語！！](../Page/俺物語！！.md "wikilink")（）
  - [OVERLORD](../Page/OVERLORD_\(小說\).md "wikilink")（）
  - [一拳超人系列](../Page/一拳超人.md "wikilink")（）
      - 第一季（2015年）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [疾走王子](../Page/疾走王子.md "wikilink")（）
  - [發條精靈戰記 天鏡的極北之星](../Page/發條精靈戰記_天鏡的極北之星.md "wikilink")（）
  - [ALL OUT\!\!](../Page/ALL_OUT!!.md "wikilink")（與[TMS
    Entertainment共同製作](../Page/TMS娛樂.md "wikilink")）

<table>
<thead>
<tr class="header">
<th><p>中文名稱</p></th>
<th><p>日文名稱</p></th>
<th><p>播出時間</p></th>
<th><p>導演</p></th>
<th><p>原作類別</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2017年</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/ACCA13區監察課.md" title="wikilink">ACCA13區監察課</a></p></td>
<td></td>
<td><p>1月10日－3月28日</p></td>
<td><p><a href="../Page/夏目真悟.md" title="wikilink">夏目真悟</a></p></td>
<td><p>漫畫</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/漫威未來復仇者.md" title="wikilink">漫威未來復仇者</a></p></td>
<td></td>
<td><p>2017年7月22日－<br />
2018年1月20日</p></td>
<td><p><a href="../Page/佐藤雄三.md" title="wikilink">佐藤雄三</a></p></td>
<td><p>原創</p></td>
<td><p>Season 1</p></td>
</tr>
<tr class="even">
<td><p>2018年</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/比宇宙還遠的地方.md" title="wikilink">比宇宙還遠的地方</a></p></td>
<td></td>
<td><p>1月2日－3月27日</p></td>
<td><p><a href="../Page/石塚敦子.md" title="wikilink">石塚敦子</a></p></td>
<td><p>原創</p></td>
<td><p>rowspan="2" </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/庫洛魔法使_透明牌篇.md" title="wikilink">庫洛魔法使 透明牌篇</a></p></td>
<td></td>
<td><p>1月7日－6月10日</p></td>
<td><p><a href="../Page/淺香守生.md" title="wikilink">淺香守生</a></p></td>
<td><p>漫畫</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/OVERLORD_(小說).md" title="wikilink">OVERLORDⅡ</a></p></td>
<td></td>
<td><p>1月9日－4月3日</p></td>
<td><p><a href="../Page/伊藤尚往.md" title="wikilink">伊藤尚往</a></p></td>
<td><p>小說</p></td>
<td><p>第2期</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/溫泉屋小女將.md" title="wikilink">溫泉屋小女將</a></p></td>
<td></td>
<td><p>4月8日－9月23日</p></td>
<td><p>增原光幸、谷東</p></td>
<td><p>與DLE共同製作</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中間管理錄利根川.md" title="wikilink">中間管理錄利根川</a></p></td>
<td></td>
<td><p>7月4日－12月25日</p></td>
<td><p><a href="../Page/川口敬一郎.md" title="wikilink">川口敬一郎</a></p></td>
<td><p>漫畫</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>OVERLORDⅢ</p></td>
<td></td>
<td><p>7月10日－10月2日</p></td>
<td><p><a href="../Page/伊藤尚往.md" title="wikilink">伊藤尚往</a></p></td>
<td><p>小說</p></td>
<td><p>第3期</p></td>
</tr>
<tr class="odd">
<td><p>漫威未來復仇者</p></td>
<td></td>
<td><p>7月30日－10月22日</p></td>
<td><p>佐藤雄三</p></td>
<td><p>原創</p></td>
<td><p>Season 2</p></td>
</tr>
<tr class="even">
<td><p>2019年</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/不吉波普不笑.md" title="wikilink">不吉波普不笑</a></p></td>
<td></td>
<td><p>1月5日－<strong>播放中</strong></p></td>
<td><p>-</p></td>
<td><p>小說</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>未定</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/花牌情緣.md" title="wikilink">花牌情緣</a></p></td>
<td></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>漫畫</p></td>
<td><p>第3期</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/消滅都市.md" title="wikilink">消滅都市</a></p></td>
<td></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>遊戲</p></td>
<td></td>
</tr>
</tbody>
</table>

### 電視動畫特別篇

  - [少爺](../Page/少爺.md "wikilink")（）（1980年6月13日，東京電影新社）

  - （1988年8月7日）

  - （手塚製作公司，1989年8月27日）

  - （1993年8月6日）

  - [柔道英雌特別篇](../Page/以柔克剛#電視動畫特別篇.md "wikilink")（）（1996年7月19日）

  - [第一神拳TV特別版『冠軍之路』](../Page/第一神拳.md "wikilink")（）（2003年4月18日）

  - [銀河天使S](../Page/銀河天使.md "wikilink")（）（2003年12月22日）

  - （）（協力製作：，2006年）

      - 前篇「海峽」9月17日公開，後篇「國境」9月18日公開。

  - 死亡筆記 加長完全決着版 ～幻影之神～（）（2007年8月31日）

<!-- end list -->

  -
    *DVD發行時改名《死亡筆記 〜幻影之神〜（）》。*

<!-- end list -->

  - 死亡筆記 L的繼承者（）（2008年8月22日）

### 電影動畫

#### 1970年代

  - [網球甜心（劇場版）](../Page/網球甜心.md "wikilink")（）（東京電影新社，1979年9月8日）

#### 1980年代

  - （）（[三麗鷗](../Page/三麗鷗.md "wikilink")，1981年3月14日）

  - （[東映動畫](../Page/東映動畫.md "wikilink")，1981年3月20日）

  - （東映動畫，1982年4月24日）

  - [幻魔大戰](../Page/幻魔大戰.md "wikilink")（，1983年3月12日）

  - （）（三麗鷗，1983年7月16日）

  - [赤腳小元](../Page/赤足小子.md "wikilink")（）（GEN PRODUCTION，1983年7月21日）

  - [透鏡人](../Page/銀河戰士.md "wikilink")（）（[講談社](../Page/講談社.md "wikilink")，1984年7月7日）

  - （1985年3月9日）

  - [神劍](../Page/神劍.md "wikilink")（）（1985年3月9日）

  - 赤腳小元2（）（GEN PRODUCTION，1986年6月14日）

  - [火鳥 鳳凰篇](../Page/火鳥_\(漫畫\).md "wikilink")（）（角川春樹事務所／，1986年12月20日）

  - （角川春樹事務所／[角川書店](../Page/角川書店.md "wikilink")，1986年12月20日）

  - [格林童話 金鳥](../Page/格林童話.md "wikilink")（）（2007年12月22日）

  - （2007年12月22日）

  - （）（2008年7月5日）

  - （第21回[東京國際電影節TIFF公式上映作品](../Page/東京國際電影節.md "wikilink")，2008年）

  - [BLEACH劇場版系列](../Page/BLEACH_\(動畫\).md "wikilink")（）（總承包商：Studio
    Pierrot，協力製作）

      - [BLEACH
        漂靈：劇場版－別處的記憶](../Page/BLEACH_漂靈：劇場版－別處的記憶.md "wikilink")（）（2006年12月16日）
      - 鑽石星塵的反叛：另一把冰輪丸（）（2007年12月22日）
      - Fade to Black 呼喚你的名字（）（2008年12月13日）

  - [夏日大作戰](../Page/夏日大作戰.md "wikilink")（）（2009年8月1日）

  - [新子與千年魔法](../Page/新子與千年魔法.md "wikilink")（）（2009年11月21日）

  - （2009年12月23日）

#### 2010年代前半

  - [槍神Trigun Badlands
    Rumble](../Page/槍神Trigun.md "wikilink")（2010年4月24日）

  - [超時空甩尾](../Page/超時空甩尾.md "wikilink")（）（2010年10月9日）

  - 藏獒多吉（與[中國電影集團共同製作](../Page/中國電影集團.md "wikilink")，2011年）

  - （總承包商：Studio Pierrot，協力製作，2011年4月29日）

  - [對某飛行員的追憶](../Page/對某飛行員的追憶.md "wikilink")（）（2011年10月1日）

  - （2012年1月7日）

  - [狼的孩子雨和雪](../Page/狼的孩子雨和雪.md "wikilink")（）（總承包商：，協力製作企劃，2012年7月21日）

  - （總承包商：[日昇動畫](../Page/日昇動畫.md "wikilink")，協力製作，2012年11月10日）

  - [劇場版 HUNTER×HUNTER
    緋色幻影](../Page/HUNTER×HUNTER.md "wikilink")（）（2013年1月12日）

  - [死亡遊行](../Page/死亡遊行.md "wikilink")（）（[青年動畫製作者育成計劃的作品](../Page/青年動畫製作者育成計劃.md "wikilink")，2013年3月2日）

  - [輝耀姬物語](../Page/輝耀姬物語.md "wikilink")（）（總承包商：吉卜力工作室，協力作畫，2013年11月23日）

  - 劇場版 HUNTER×HUNTER -最終任務-（）（2013年12月27日）

#### 2010年代後半

  - 遊戲人生ZERO（）（2017年7月15日）
  - [夢想機械（暫譯）](../Page/夢想機械.md "wikilink")（）（日期未定）\[3\]

### OVA

  - 1980年代

<!-- end list -->

  - 追傷人（ 1986年）
  - [火鳥](../Page/火鳥_\(漫畫\).md "wikilink")
      - 大和篇（ 1987年）
      - 宇宙篇（ 1987年）
  - （ 1987年）
  - [JUNK BOY](../Page/JUNK_BOY.md "wikilink")（1987年）： ；極品男孩。
  - [宮澤賢治作品集](../Page/宮澤賢治.md "wikilink")
      - 風之又三郎（ 1988年）
      - 橡子與山貓（ 1988年）
  - [惡魔的新娘～蘭之組曲](../Page/惡魔的新娘.md "wikilink")（ 1988年）
  - [魔界都市 新宿](../Page/魔界都市_新宿.md "wikilink")（ 1988年）
  - （ 1988年）
  - [妖精王](../Page/妖精王.md "wikilink")（1988年）
  - [午夜之眼](../Page/午夜之眼.md "wikilink")（ 1989年）
  - [福星小子系列](../Page/福星小子#OVA.md "wikilink")
      - 和羊笑一個（ 1989年）
      - 抓住愛人的心（ 1989年）
      - 恐怖的少女麻疹（ 1991年）
      - 和靈魂約會（ 1991年）
  - 午夜之眼II（ 1989年）

<!-- end list -->

  - 1990年代

<!-- end list -->

  - （ 1990年）
  - （CYBER CITY OEDO 808 1990年－1991年）
  - [羅德斯島戰記](../Page/羅德斯島戰記_\(OVA\).md "wikilink")（ 1990年－1991年）
  - （NINETEEN 19 1990年）
  - [森林大帝](../Page/森林大帝.md "wikilink")（ 1991年，參與製作；原製作承攬：手塚製作）
  - [帝都物語](../Page/帝都物語.md "wikilink")（1991年－1992年）
  - [OZ VOL.1～VOL.2](../Page/OZ.md "wikilink")（1992年）
  - [絕愛-1989-](../Page/尾崎南.md "wikilink")（ 1992年）
  - [東京巴比倫](../Page/東京巴比倫.md "wikilink")（東京BABYLON 1992年，參與製作）
  - （ 1992年）
  - [魔物獵人妖子系列](../Page/魔物獵人妖子.md "wikilink")（ 1992年－1995年）
  - （ 1993年）
  - （SINGLES 1993年）
  - [銃夢](../Page/銃夢.md "wikilink")（ 1993年）
  - （POPS 1993年）
  - （ 1993年）
  - （ 1993年）
  - [人魚傷痕](../Page/人魚森林.md "wikilink")（ 1993年）
  - （A-Girl 1993年）
  - （ 1994年，短篇中的1集）
  - [嬌娃夏生之危機](../Page/嬌娃夏生之危機.md "wikilink")（ 1994年）
  - [東京巴比倫2](../Page/東京巴比倫.md "wikilink")（東京BABYLON2 1994年，參與製作）
  - [最終幻想：水晶傳說](../Page/最終幻想：水晶傳說.md "wikilink")（FINAL FANTASY 1994年）
  - [孔雀王](../Page/孔雀王.md "wikilink")（真·孔雀王 1994年）
  - （BRONZE KOJI NANJO cathexis 1994年）
  - CLAMP夢遊仙境（CLAMP IN WONDERLAND 1994年）
  - [幽幻怪社](../Page/幽幻怪社.md "wikilink")（1994年－1995年）
  - [DNA²](../Page/DNA².md "wikilink")（ 1995年）
  - [美幸夢遊仙境](../Page/美幸夢遊仙境.md "wikilink")（ 1995年）
  - [鐵腕女警](../Page/鐵腕女警.md "wikilink")（ 1996年－1997年）
  - [魔獸之狩](../Page/魔獸之狩.md "wikilink")（ 1997年）
  - [魔域幽靈](../Page/魔域幽靈.md "wikilink")（VAMPIRE HUNTER The Animated
    Series 1997年－1998年）
  - （ 1998年）
  - （ 1999年）
  - 亞歷山大戰記（ 1999年）
  - [危險調查員](../Page/危險調查員.md "wikilink")（MASTER KEATON（OVA版）1999年）

<!-- end list -->

  - 2000年代

<!-- end list -->

  - （TRAVA FIST PLANET episode1 2001年）
  - [銀河天使](../Page/銀河天使.md "wikilink")（ 2002年－2003年）
  - [Di Gi Charat](../Page/Di_Gi_Charat.md "wikilink")（ 2002年）
  - [駭客任務動畫外傳](../Page/駭客任務動畫外傳.md "wikilink")（ANIMATRIX 2003年）
  - [羔羊之歌](../Page/羔羊之歌.md "wikilink")（ 2003年－2004年）
  - [第一神拳OVA版『柴間VS木村 死刑執行』](../Page/第一神拳.md "wikilink")（ 2003年）
  - [炎之蜃氣樓](../Page/炎之蜃氣樓.md "wikilink")（ 2004年）
  - [童話槍手小紅帽](../Page/童話槍手小紅帽#OVA.md "wikilink")（2005年）
  - [天上天下 ULTIMATE FIGHT](../Page/天上天下.md "wikilink")（2005年）
  - [草莓100%](../Page/草莓100%#OVA標題.md "wikilink")（ 2005年）
  - [最終幻想VII：最終密令](../Page/最終幻想VII：最終密令.md "wikilink")（[LAST ORDER FINAL
    FANTASY VII](../Page/最终命令_-最终幻想VII-.md "wikilink") 2005年）
  - （ 2005年－2006年）
  - （ 2007年）
  - CLAMP夢遊仙境（CLAMP IN WONDERLAND 2 1995-2006 2007年）

### 其他動畫

  - [池田聰](../Page/池田聰.md "wikilink")/Je Reviens 音樂影像特輯插播動畫（1987年）
  - Let's Talk\!（瀬戸大橋博'88・NTT「宇宙帆船NTT号」内宣傳動畫）（1988年）
  - PlayStation電玩[狂野兵器內插播動畫](../Page/狂野兵器系列.md "wikilink")（1996年）
  - 龍神沼（石ノ森萬画館館內宣傳主題動畫 2001年）
  - 消えた赤ずきんちゃん（石ノ森萬画館館內宣傳主題動畫 2001年）
  - 小川のメダカ（石ノ森萬画館館內宣傳主題動畫 2002年）
  - MMORPG[仙境傳說](../Page/仙境傳說.md "wikilink")（網絡遊戲宣傳動畫 2002年）
  - Playstation2電玩[狂野兵器F內插播動畫](../Page/狂野兵器系列.md "wikilink")（2003年）
  - [Young Alive\!
    iPS細胞がひらく未来](../Page/Young_Alive!_iPS細胞がひらく未来.md "wikilink")
    （[日本科学未来館館內宣傳動畫](../Page/日本科学未来館.md "wikilink") 2010年）
  - NintendoDS電玩[天空機器人](../Page/天空機器人.md "wikilink")（[Solatorobo
    それからCODAへ內插播動畫](../Page/Solatorobo_それからCODAへ.md "wikilink")
    2010年）
  - 柏青歌遊藝機[侍魂主題動畫](../Page/侍魂.md "wikilink")（2011年）
  - [株式会社明治Tweet](../Page/株式会社明治.md "wikilink") Love Story動畫廣告（2011年）
  - [学校法人モード学園](../Page/学校法人モード学園.md "wikilink") HAL TVCM「強く望め」篇
    動畫廣告（2011年）
  - [のらのらの〜ら電視綜藝特邀製作動畫項目](../Page/声優バラエティー_SAY!YOU!SAY!ME!#のらのらの〜ら.md "wikilink")（2011年）

## 相關項目

  - [MAPPA](../Page/MAPPA.md "wikilink")－由MADHOUSE創設元老之一的[丸山正雄退職之後創社](../Page/丸山正雄.md "wikilink")，2011年6月成立\[4\]。
  - [日本動畫工作室列表](../Page/日本動畫工作室列表.md "wikilink")

## 參考文獻

## 外部連結

  - [株式會社MADHOUSE公式官網](http://www.madhouse.co.jp/)

  -

  - [MADHOUSE官方頻道](http://ch.nicovideo.jp/channel/ch266)
    －[niconico頻道](../Page/niconico頻道.md "wikilink")

[Category:日本動畫工作室](../Category/日本動畫工作室.md "wikilink")
[\*](../Category/Madhouse.md "wikilink")
[Category:中野區公司](../Category/中野區公司.md "wikilink")
[Category:1972年成立的公司](../Category/1972年成立的公司.md "wikilink")

1.  [阿佐ヶ谷探偵団第一回：「マッドハウスのネーミングの由来は？」](https://web.archive.org/web/20040205024039/http://www.madhouse.co.jp/column/asagaya/asagaya1.html)
    - 2004年2月5日
2.  第112話（最終話）ED字幕
3.
4.