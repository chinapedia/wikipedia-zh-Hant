**曼恩歐洲股份公司**（**-{MAN}- SE**，其前身為**-{MAN}-
AG**)，亦可称其为**曼恩集团**，-{MAN}-可译作**曼**、**曼-{}-恩**、**猛-{}-狮**，这三个[字母由公司前身](../Page/字母.md "wikilink")**Maschinenfabrik
Augsburg
Nürnberg（奥格斯堡-纽伦堡机械工厂股份公司）**的第一个字母组成，是一家總部位於[德國](../Page/德國.md "wikilink")[巴伐利亚州](../Page/巴伐利亚州.md "wikilink")[慕尼黑的机械制造公司](../Page/慕尼黑.md "wikilink")。曼恩集团是一个[欧洲领先的工程集团](../Page/欧洲.md "wikilink")，[业务领域涉及](../Page/业务.md "wikilink")[商用车](../Page/商用车.md "wikilink")、[柴油发动机](../Page/柴油发动机.md "wikilink")、[涡轮机](../Page/涡轮机.md "wikilink")、[汽轮机](../Page/汽轮机.md "wikilink")、[印刷机械和其他](../Page/印刷.md "wikilink")[机械](../Page/机械.md "wikilink")，在各个业务领域都处于全球领先地位。

曼恩集团是[德國](../Page/德國.md "wikilink")[法蘭克福證券交易所的](../Page/法蘭克福證券交易所.md "wikilink")[上市公司](../Page/上市公司.md "wikilink")，也是30個[德國DAX指數成分股中](../Page/德國DAX指數.md "wikilink")[歷史最悠久者之一](../Page/歷史.md "wikilink")，同时是[世界500强之一](../Page/世界500强.md "wikilink")。2008年，曼恩集团迎来其250周年庆典，旗下来自120个不同[国家的](../Page/国家.md "wikilink")51,300名[员工](../Page/员工.md "wikilink")[创造了](../Page/创造.md "wikilink")150亿欧元的年销售额。曼恩集团通过成立全资子公司和与当地公司[合资](../Page/合资.md "wikilink")[经营的模式](../Page/经营.md "wikilink")，在[印度](../Page/印度.md "wikilink")、[波兰](../Page/波兰.md "wikilink")、[土耳其](../Page/土耳其.md "wikilink")、[中国](../Page/中国.md "wikilink")、[美国](../Page/美国.md "wikilink")、[阿联酋](../Page/阿联酋.md "wikilink")、[南非](../Page/南非.md "wikilink")、[乌兹别克斯坦](../Page/乌兹别克斯坦.md "wikilink")、[葡萄牙和](../Page/葡萄牙.md "wikilink")[奥地利設有子公司](../Page/奥地利.md "wikilink")。

## 概况

### 旗下公司

目前曼恩集团主要下辖三大子公司，每个子公司供应全球各个不同[地区](../Page/地区.md "wikilink")[市场](../Page/市场.md "wikilink")：

  - **曼恩商用车公司（-{MAN}- Truck & Bus
    AG）**：总部位于德国慕尼黑的曼恩商用车公司是曼恩集团中最重要的，也是最大的企业，是世界领先的商用车和柴油发动机制造商之一。
      - 工厂分布：[德国](../Page/德国.md "wikilink")、[奥地利](../Page/奥地利.md "wikilink")、[英国](../Page/英国.md "wikilink")、[波兰](../Page/波兰.md "wikilink")、[土耳其](../Page/土耳其.md "wikilink")、[南非](../Page/南非.md "wikilink")、[澳大利亚](../Page/澳大利亚.md "wikilink")、[埃及](../Page/埃及.md "wikilink")
      - 2002年占有[西欧商用车](../Page/西欧.md "wikilink")[市场份额的](../Page/市场份额.md "wikilink")13.7%，研发资金1.71亿[欧元](../Page/欧元.md "wikilink")；2004财報：员工约34,000名，年销售额74亿[欧元](../Page/欧元.md "wikilink")，其中：63,300辆卡车，6,100辆公车
      - 商用车辆：卡车（占集团销售额的61%），售后零部件和服务（18%），客车（14%），发动机和零部件（7%）
          - **卡车**产品线从6吨到50吨，涵盖各种用途的轻、中、重型
          - **客车**品牌：-{MAN}-，[尼奧普蘭](../Page/尼奧普蘭.md "wikilink")
          - **发动机**领域：德国独家生产慢速二冲程柴油发动机的企业，也生产[天然气发动机](../Page/天然气.md "wikilink")，开发、生产、销售卡车、公交车、旅游客车和大中型客车底盘及110马力至1400马力的发动机，其中[纽伦堡工厂所生产的发动机可用于驱动车辆](../Page/纽伦堡.md "wikilink")、船舶和为工业生产提供能量
          - **汽车零部件**：轴、分动器、卡车和轿车的车架和底盘冲压件
  - **曼恩动力设备公司（-{MAN}- Diesel & Turbo SE）**：2010年**（-{MAN}- Diesel
    SE）**与**（-{MAN}- Turbo
    AG）**合并组建为曼恩动力设备公司，曼恩动力设备公司是一家[跨国公司](../Page/跨国公司.md "wikilink")，总部设在[德国](../Page/德国.md "wikilink")[奥格斯堡](../Page/奥格斯堡.md "wikilink")，生产大口径[柴油发动机和](../Page/柴油发动机.md "wikilink")[涡轮机用于海洋和固定应用](../Page/涡轮机.md "wikilink")，如[船舶推进系统](../Page/船舶.md "wikilink")，动力装置的应用和[涡轮增压器](../Page/涡轮增压器.md "wikilink")。是全球领先的船用和固定应用大口径[柴油机以及](../Page/柴油机.md "wikilink")[透平机械供应商之一](../Page/透平.md "wikilink")。
  - **曼恩拉丁美洲公司（-{MAN}- Latin
    America）**：前[大众](../Page/大众汽车.md "wikilink")[卡](../Page/卡車.md "wikilink")[客車公司](../Page/客車.md "wikilink")，是[巴西卡车市场领军者](../Page/巴西.md "wikilink")。

### 其他股份

  - **伦克公司（RENK AG）**（持有76%的股份）
      - 汽车变速箱，工业变速箱，海洋变速箱，滑动轴承，联轴器
  - **中國重汽（香港）**（持有25%+1股份）
  - **Ferrostaal公司（-{MAN}- Ferrostaal AG）**（持有30%的股份，其餘歸International
    Petroleum Investment公司)
      - 工业服务：预测，交付，组装，包括[钢铁结构](../Page/钢铁.md "wikilink")

### 其他信息

  - MAN集团内还有为集团（[康采恩](../Page/康采恩.md "wikilink")）服务的[金融机构](../Page/金融机构.md "wikilink")。

## 历史

[MAN_logo.JPG](https://zh.wikipedia.org/wiki/File:MAN_logo.JPG "fig:MAN_logo.JPG")

### 成立

[St_Antony.jpg](https://zh.wikipedia.org/wiki/File:St_Antony.jpg "fig:St_Antony.jpg")
曼恩的[历史根源可以追溯至](../Page/历史.md "wikilink")1758年，曼恩集团前身“圣安东尼炼铁厂”（St. Antony
ironworks）作为第一大重工业[企业在](../Page/企业.md "wikilink")[德国](../Page/德国.md "wikilink")[奥伯豪森开业](../Page/奥伯豪森.md "wikilink")，地处德国[重工业之都](../Page/重工业.md "wikilink")[鲁尔区](../Page/鲁尔区.md "wikilink")。1808年，“圣·安东尼”与其他两家炼铁厂“Gute
Hoffnung”和“Neue Essen”合并，名为“铁矿开采和贸易公司（Hüttengewerkschaft und Handlung
Jacobi）”，仍位于德国奥伯豪森，之后公司更名为Gute Hoffnungshütte（GHH）。

1840年，德国[工程师Ludwig](../Page/工程师.md "wikilink")
Sander在德国南部的奥格斯堡成立了“Sander'sche
Maschinenfabrik（机械制造厂）"，即曼恩集团的雏形。而后曾改名为“"C. Reichenbach'sche
Maschinenfabrik（机械制造厂）",这是以印刷机先驱Carl August
Reichenbach的名字命名的。最后成为“奥格斯堡机械工厂”。

1898年“[纽伦堡机械制造股份公司](../Page/纽伦堡.md "wikilink")”（1841年建立）与“[奥格斯堡机械工厂股份公司](../Page/奥格斯堡.md "wikilink")”（1840年建立）合并为“奥格斯堡联合机械工厂和纽伦堡机械制造公司股份公司”。1908年改名为“奥格斯堡-纽伦堡机械工厂股份公司”，简称M.A.N.。

当时，[矿石开采和炼铁依然是鲁尔地区](../Page/矿石.md "wikilink")[生产的主要核心](../Page/生产.md "wikilink")，然而[工程机械却成为奥格斯堡和纽伦堡的主导业务分支](../Page/工程.md "wikilink")。在Heinrich
von
Buz（海因里希·冯·布斯）的领导下，奥格斯堡机械工厂由一个只有400名员工的中型工厂增长发展成为1913年拥有12000名员工的骨干企业。

动力[运输和钢结构](../Page/运输.md "wikilink")[建筑是这个时期的主要话题](../Page/建筑.md "wikilink")。曼恩早期的先辈们负责过许多技术革新项目，对新技术的完全开放是这些早期[企业家和](../Page/企业家.md "wikilink")[工程师的成功的基石](../Page/工程师.md "wikilink")。他们在乌珀塔尔市修建了悬轨[列车](../Page/列车.md "wikilink")（Wuppertaler
Schwebebahn），1857年在慕尼黑建成钢铁桥Großhesseloher
Brücke，1893年到1897年建造的铁路钢轨Müngsten。[Wuppertaler_Schwebebahn_c1913_LOC_03961u.jpg](https://zh.wikipedia.org/wiki/File:Wuppertaler_Schwebebahn_c1913_LOC_03961u.jpg "fig:Wuppertaler_Schwebebahn_c1913_LOC_03961u.jpg")

早期未更名的MAN经历过许多次[技术革新](../Page/技术.md "wikilink")，如德国第一台旋转报纸印刷机，第一台[制冷](../Page/制冷.md "wikilink")[机器](../Page/机器.md "wikilink")“林德系统”（1873年），以及参与1893年至1897年柴油发动机的制造。著名工程师[鲁道夫·狄塞尔在](../Page/鲁道夫·狄塞尔.md "wikilink")1897年替奥格斯堡机械工厂制造了世界上第一台功能优异的[柴油发动机](../Page/柴油发动机.md "wikilink")，后来此類動力系統就被命名为「狄塞尔机」，这项发明可以说是今天商用车诞生的基石。1924年，第一台装置MAN[柴油发动机的卡车在](../Page/柴油发动机.md "wikilink")[柏林](../Page/柏林.md "wikilink")[汽车展展示](../Page/汽车展.md "wikilink")，[巴伐利亚](../Page/巴伐利亚.md "wikilink")[邮政局购买了大量的此款卡车](../Page/邮政局.md "wikilink")。

1930年代，MAN用于[轮船的](../Page/轮船.md "wikilink")[柴油发动机授权給](../Page/柴油发动机.md "wikilink")[美国國內生產](../Page/美国.md "wikilink")，并安装在[海軍的舰队](../Page/海軍.md "wikilink")[潜艇上](../Page/潜艇.md "wikilink")。在[太平洋战争证明了這些發動機容易故障而不受欢迎](../Page/太平洋战争.md "wikilink")。

1942年初，在[欧洲](../Page/欧洲.md "wikilink")，利用MAN设计的車体以及[德国](../Page/德国.md "wikilink")[莱茵金属-博尔西希设计的](../Page/莱茵金属-博尔西希.md "wikilink")[炮塔制造和部署了](../Page/炮塔.md "wikilink")[豹式戰車](../Page/豹式戰車.md "wikilink")。[德国在](../Page/德国.md "wikilink")[第二次世界大战中便使用了该](../Page/第二次世界大战.md "wikilink")[坦克](../Page/坦克.md "wikilink")。

### 战后

[二次大战后](../Page/二次大战.md "wikilink")，M.A.N.
GHH失去了所有在国外的经营活动，并且在[战争中损失惨重](../Page/战争.md "wikilink")。盟军部队控制了所有GHH公司，分割公司，把钢铁生产设施分给独立的个体所有。因此，公司将重点转向[德国的南方](../Page/德国.md "wikilink")[工厂和](../Page/工厂.md "wikilink")[商业车辆](../Page/商业.md "wikilink")。这个过程一直支持战略性[收购和处置](../Page/收购.md "wikilink")，最重要的是接管商用车制造商（1971年）、[德意志造船厂](../Page/德意志.md "wikilink")（1966年/67年）的股份处置，和收购印刷机械制造商Faber\&Schleicher，以及将其融合到[曼罗兰股份公司](../Page/曼罗兰.md "wikilink")（1979年）。

1971年，[奥地利](../Page/奥地利.md "wikilink")与[奥地利](../Page/奥地利.md "wikilink")合并为ÖAF-Gräf
& Stift AG，并于同年被MAN完全接管。

1972年，在[德国](../Page/德国.md "wikilink")[巴伐利亚州](../Page/巴伐利亚州.md "wikilink")[纽伦堡的姊妹城市](../Page/纽伦堡.md "wikilink")[埃尔朗根的一条](../Page/埃尔朗根.md "wikilink")900米长的环行试验线路上，MAN制造了[磁浮](../Page/磁浮.md "wikilink")“[埃尔朗根试验车](../Page/埃尔朗根.md "wikilink")01号”（EET-01）。

1986年，并入MAN集团并将总部设在[慕尼黑](../Page/慕尼黑.md "wikilink")，新的公司名为**-{MAN}-
AG**。

1991年，MAN集团接管[奥地利](../Page/奥地利.md "wikilink")[维也纳的卡车制造商](../Page/维也纳.md "wikilink")——卡车商用车股份公司（原斯太尔-[戴姆勒](../Page/戴姆勒.md "wikilink")-普赫股份公司），同时在[土耳其和](../Page/土耳其.md "wikilink")[印度分别有卡车建筑工程](../Page/印度.md "wikilink")-{MAN}--A.S和Shakti--{MAN}-。

1999年底，Star Trucks Sp. z o.o.被MAN集团接管。2003年8月1日，-{MAN}- Star Trucks Sp.
z o.o.与-{MAN}- Bus Poland Sp. z o.o.合并为-{MAN}- Star Trucks & Busses Sp.
z o.o.。2009年1月9日，-{MAN}- Star Trucks & Busses Sp. z
o.o.从名字中淘汰“Star”，现在被称为-{MAN}- Bus Sp. z o.o.。

2000年，成为MAN集团的一部分，2007年，MAN集团对做停产处理。

2001年，MAN集团收购世界知名豪华大客车制造商，德国[尼奥普兰](../Page/尼奥普兰.md "wikilink")。

2006年9月18日，MAN集团以103亿欧元收购[斯堪尼亚公司](../Page/斯堪尼亚.md "wikilink")（Scania
AB）—[瑞典领先的卡车和巴士制造公司](../Page/瑞典.md "wikilink")。猛獅集团购买了[斯堪尼亚公司](../Page/斯堪尼亚.md "wikilink")\]15.6％的具有表决权的股份。[大众汽车集团](../Page/大众汽车.md "wikilink")—[斯堪尼亚的最大](../Page/斯堪尼亚.md "wikilink")[股东](../Page/股东.md "wikilink")，从原先拥有猛獅的20%的[股份到](../Page/股份.md "wikilink")29%的[股份](../Page/股份.md "wikilink")。[大众汽車擁有](../Page/大众汽車.md "wikilink")[斯堪尼亚的大多数](../Page/斯堪尼亚.md "wikilink")[股权和超过](../Page/股权.md "wikilink")51%的股份。人们普遍认为这两个卡车制造商将合并，将接手[大众汽車的巴西重型卡车业务](../Page/大众汽車.md "wikilink")。

最近的一次采访中，[大众汽车公司](../Page/大众汽车.md "wikilink")[首席执行官马丁](../Page/首席执行官.md "wikilink")·温特科恩说：[大众汽車目前没有计划尝试合并](../Page/大众汽車.md "wikilink")[斯堪尼亚公司和MAN](../Page/斯堪尼亚.md "wikilink")，但他也没有排除猛獅收购[大众汽车的巴西重型卡车的可能性](../Page/大众汽车.md "wikilink")。

至2009年，[大众汽車的卡车和客车已经属于猛獅集团的一部分了](../Page/大众汽車.md "wikilink")。也有人推测，[大众汽车公司最初打算在](../Page/大众汽车.md "wikilink")2008年年底收购猛獅的卡车和客车，但这并没有得到官方证实，目前也没有任何行动表明[大众公司在试图占有猛獅的](../Page/大众公司.md "wikilink")[股份或购买猛獅的商用汽车](../Page/股份.md "wikilink")。

在2008年，猛獅的250周年庆典，猛獅集团在[慕尼黑举行盛大典礼](../Page/慕尼黑.md "wikilink")，并在[慕尼黑的德国](../Page/慕尼黑.md "wikilink")[博物馆](../Page/博物馆.md "wikilink")、[奥格斯堡的企业博物馆和Rheinisches](../Page/奥格斯堡.md "wikilink")
Industriemuseum举办展览。其中令人注目的莫过于6月的“老车护送队”。汽车行驶过一条经典航线——从慕尼黑穿越纽伦堡到达奥格斯堡。

2009年5月19日猛獅集团圆满地完成了从[德国](../Page/德国.md "wikilink")[股份公司](../Page/股份公司.md "wikilink")（-{MAN}-
AG）转变为一家[歐洲股份公司](../Page/歐洲股份公司.md "wikilink")，称之为**-{MAN}-
SE**（Societas Europaea）。

2011年7月，MAN集团被德国[大众集团收购](../Page/大众集团.md "wikilink")。

## 各地概況

### 中國

[Man_ackerdiesel_sst.jpg](https://zh.wikipedia.org/wiki/File:Man_ackerdiesel_sst.jpg "fig:Man_ackerdiesel_sst.jpg")
[MAN-NL262.JPG](https://zh.wikipedia.org/wiki/File:MAN-NL262.JPG "fig:MAN-NL262.JPG")
[KR5701.jpg](https://zh.wikipedia.org/wiki/File:KR5701.jpg "fig:KR5701.jpg")
[Faroese_beer_truck.jpg](https://zh.wikipedia.org/wiki/File:Faroese_beer_truck.jpg "fig:Faroese_beer_truck.jpg")

  - MAN集团的产品在1940年代即进入[中国的货运](../Page/中国.md "wikilink")、[消防](../Page/消防.md "wikilink")、[石油钻探](../Page/石油.md "wikilink")、[客运](../Page/客运.md "wikilink")、[公路养护市场](../Page/公路.md "wikilink")。
  - MAN集团目前在中国的合作或合资伙伴：
      - [丹东](../Page/丹东.md "wikilink")[黄海客车](../Page/黄海客车.md "wikilink")
      - [郑州](../Page/郑州.md "wikilink")[宇通客车](../Page/宇通客车.md "wikilink")
      - [厦门](../Page/厦门.md "wikilink")[金龙客车](../Page/金龙联合汽车.md "wikilink")（[大金龙](../Page/大金龙.md "wikilink")）
      - [金華](../Page/金華.md "wikilink")[青年汽車](../Page/青年汽車.md "wikilink")
      - [陕汽集团](../Page/陕汽集团.md "wikilink")
      - [重庆红岩](../Page/重庆.md "wikilink")
      - [上汽集团](../Page/上汽集团.md "wikilink")
      - [中國重汽集团](../Page/中國重汽集团.md "wikilink")
      - [中國中車](../Page/中國中車.md "wikilink")

<!-- end list -->

  - 1983年，MAN集团旗下的商用车股份公司与中国重汽集团签订技术许可协议。斯太尔卡车在中国几乎就是重型卡车的代名词。
  - 1986年，[尼奥普兰客车通过北方车辆集团有限公司引进其客车技术而进入中国](../Page/尼奥普兰.md "wikilink")。
  - 1997年，曼恩商用车公司中国代表处在[北京成立](../Page/北京.md "wikilink")。
  - 2000年，[金华青年汽车集团与MAN合作成立金华尼奥普兰车辆有限公司](../Page/金华.md "wikilink")，并引进德国尼奥普兰公司先进的客车生产技术。
  - 2002年1月18日，MAN和中国最大的客车厂家宇通在郑州合资组建了猛狮客车有限公司，双方各占50%的[股份](../Page/股份.md "wikilink")。2009年2月，MAN将50%的猛狮股权以1欧元的价格转让给郑州[宇通集团](../Page/宇通集团.md "wikilink")。
  - 2003年9月，[陕西重型汽车有限公司全面引进MANF](../Page/陕西.md "wikilink")2000重卡技术，开发出德龙F2000系列重型[卡车](../Page/卡车.md "wikilink")。
  - 2004年5月18日，陕西重型汽车有限公司与MAN扩大合作协议在[北京钓鱼台国](../Page/北京.md "wikilink")[宾馆签署](../Page/宾馆.md "wikilink")。
  - 2004年8月24日，MAN商用车股份公司在北京注册成立[资本金](../Page/资本.md "wikilink")100万[欧元的MAN商用车企业管理](../Page/欧元.md "wikilink")（北京）有限公司。
  - 2004年8月25日，带有MAN品牌的豪华重型卡车F2000在浙江金华青年尼奥普兰集团的生产线上正式投入批量生产。
  - 2004年11月5日，[广州宝龙汽车与MAN合伙打造的挂有MAN品牌的豪华旅游客车隆重亮相](../Page/广州.md "wikilink")。
  - 2005年1月19日，两辆MAN[VIP商务车在](../Page/VIP.md "wikilink")[金华尼奥普兰车辆有限公司下线](../Page/金华.md "wikilink")。
  - 2009年,MAN集团收购[中国重汽集团](../Page/中国重汽集团.md "wikilink")（CNHTC）旗下[中國重汽
    (香港)](../Page/中國重汽_\(香港\).md "wikilink")（Sinotruk）25%加一股权，双方展开长期战略性合作。

### 香港

1987年[大昌行集團](../Page/大昌行集團.md "wikilink")[合德汽車成為MAN全線商用車系](../Page/合德汽車.md "wikilink")[香港及](../Page/香港.md "wikilink")[澳門的總代理](../Page/澳門.md "wikilink")，為配合業務發展，更代理[德國](../Page/德國.md "wikilink")[Neoplan](../Page/Neoplan.md "wikilink")、[荷蘭](../Page/荷蘭.md "wikilink")[Berkhof](../Page/Berkhof.md "wikilink")、[葡萄牙Alfredo](../Page/葡萄牙.md "wikilink")
Caetano、[馬來西亞MTrans及Gemilang](../Page/馬來西亞.md "wikilink")、中國-{YOUNGMAN}-（[青年汽車](../Page/青年汽車.md "wikilink")）。

MAN在香港的譯名為「德國猛-{}-獅」。

### 臺灣

[至懋汽車為MAN集團重型](../Page/至懋汽車.md "wikilink")[卡車](../Page/卡車.md "wikilink")、[巴士及特種車在](../Page/巴士.md "wikilink")[臺灣的總代理](../Page/臺灣.md "wikilink")。

## 主要竞争对手

  - [瑞典](../Page/瑞典.md "wikilink")[沃尔沃](../Page/沃尔沃.md "wikilink")
  - [瑞典](../Page/瑞典.md "wikilink")[斯堪尼亚](../Page/斯堪尼亚.md "wikilink")
  - [意大利](../Page/意大利.md "wikilink")[依維柯](../Page/依維柯.md "wikilink")
  - [德国](../Page/德国.md "wikilink")[梅赛德斯-奔驰](../Page/梅赛德斯-奔驰.md "wikilink")
  - [法國](../Page/法國.md "wikilink")[雷諾卡車](../Page/雷諾卡車.md "wikilink")
  - [荷蘭](../Page/荷蘭.md "wikilink")[DAF](../Page/DAF.md "wikilink")

## 參考文獻

  - Johannes Bähr, Ralf Banken, Thomas Flemming: *Die MAN. Eine deutsche
    Industriegeschichte.* C.H. Beck, München 2008, ISBN
    978-3-406-57762-8.
  - Anne Dreesbach, Michael Kamp, Maximilian Schreiber: *Seit 90 Jahren
    auf der Achse. MAN Nutzfahrzeuge und ihre Geschichte 1915 bis 2005.*
    August-Dreesbach-Verlag, München 2005, ISBN 978-3-926163-36-3.
  - Addi Janssen: *Ideale sind wie Sterne.* Rheinware Verlag,
    Mönchengladbach, ASIN: B00AINY00A. (Der Autor war ab 1957 bei der
    MAN in Hamburg beschäftigt und 20 Jahre Betriebsratsvorsitzender,
    auf den Seiten 111 bis 124 berichtet er über den Arbeitskampf)
  - Henning Stibbe, Matthias Georgi: *MAN. Ein Jahrhundert.*
    August-Dreesbach-Verlag, München 2015, ISBN 978-3-944334-56-1.
  - Lutz Uebel, Wolfgang-D. Richter (Hrsg.): *MAN – 150 Jahre
    Schienenfahrzeuge aus Nürnberg.* EK-Verlag, Freiburg 1994, ISBN
    3-88255-562-9.

## 外部链接

  - [MAN公司网站](http://www.man.de/MAN/en/index.html)
  - [MAN公司网站](http://www.man.de/)
  - [英國MAN网站](http://www.man-mn.co.uk/en/en.jsp)
  - [MAN B\&W柴油机网页](http://www.manbw.com/)
  - [MAN商用车股份公司](http://www.man-nutzfahrzeuge.de/)
  - [MAN罗兰印刷机械股份公司](https://web.archive.org/web/20060210023147/http://www.man-roland.de/)
  - [MAN涡轮股份公司](http://www.manturbo.com/)
  - [MAN
    DWE有限公司](https://web.archive.org/web/20060211200958/http://www.dwe.de/)
  - [MAN Ferrostaal股份公司](http://www.manferrostaal.de/)
  - [RENK股份公司](http://www.renk.biz/)
  - [中国MAN网站](http://www.man-mn.cn/cn/)

{{-}}

[Category:汽车品牌](../Category/汽车品牌.md "wikilink")
[Category:德國引擎製造商](../Category/德國引擎製造商.md "wikilink")
[Category:巴士生產商](../Category/巴士生產商.md "wikilink")
[Category:貨車生產商](../Category/貨車生產商.md "wikilink")
[Category:德國軍事工業](../Category/德國軍事工業.md "wikilink")
[Category:德國汽車公司](../Category/德國汽車公司.md "wikilink")
[Category:1758年建立](../Category/1758年建立.md "wikilink")
[Category:歐洲股份公司](../Category/歐洲股份公司.md "wikilink")
[Category:大眾汽車](../Category/大眾汽車.md "wikilink")