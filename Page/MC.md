**MC**或**Mc**可能有以下的含意：

  - [美国海军陆战队](../Page/美国海军陆战队.md "wikilink")（United States Marine
    Corps.），是美國軍中的海軍陸戰隊及兩棲作戰部隊

  - [主持人或](../Page/主持人.md "wikilink")[司儀](../Page/司儀.md "wikilink")（Master
    of Ceremonies、Master of Ceremony），負責场所或節目、儀式等各式項目的主持者

  - [饒舌者](../Page/MC_\(音樂\).md "wikilink")，嘻哈音樂定義中的代稱，即[麥克風](../Page/麥克風.md "wikilink")

  - [萬事達卡](../Page/萬事達卡.md "wikilink")（MasterCard），一个美国纽约证券交易所上市的有限公司及其发行的万事达信用卡、借记卡

  - [瑪麗亞·凱莉](../Page/瑪麗亞·凱莉.md "wikilink")（Mariah Carey），美國女歌手

  - ，美国女歌手

  - [.mc](../Page/.mc.md "wikilink")，摩纳哥的國際域名

  - [選擇題](../Page/選擇題.md "wikilink")（Multiple Choice/Multiple choice
    question/MCQ），一種題型或方法

  - [医疗证明书或](../Page/医疗证明书.md "wikilink")[健康证明](../Page/健康证明.md "wikilink")（Medical
    Certificate）

  - [月經](../Page/月經.md "wikilink")（Menstrual
    Cycle），指女性血液或黏膜定期從子宮內膜經陰道排出體外的現象

  - [電磁接觸器](../Page/電磁接觸器.md "wikilink")（Magnetic Contactor）

  - [熔火之心](../Page/熔火之心.md "wikilink")（网络游戏“魔兽世界”中的一个副本）

  - ，一款免费跨平台的文件管理器

  - [精神控制](../Page/精神控制.md "wikilink") （Mind
    Control），透過系統性方法，有意圖地向別人灌輸思想，來符合操縱者的意願的一連串的手法與過程

  - [軍功十字勳章](../Page/軍功十字勳章.md "wikilink")（Military Cross），英國一種軍事勳章

  - 《[我的世界](../Page/我的世界.md "wikilink")》（minecraft）的英語縮寫，一款沙盒建造类独立游戏。

  - [Media Create](../Page/Media_Create.md "wikilink")，日本市场调查与数据分析公司。

  - [多人战役](../Page/多人战役.md "wikilink")（Multiplayer Campaign）

  - [马尔可夫链](../Page/马尔可夫链.md "wikilink") （Markov
    Chain），为狀態空間中经过从一个状态到另一个状态的转换的随机过程

  - [镆](../Page/镆.md "wikilink")（Moscovium），一種[人工合成的](../Page/人工合成元素.md "wikilink")[放射性元素](../Page/放射性.md "wikilink")，[原子序為](../Page/原子序.md "wikilink")115。

  - [西洋占星術中代表第十宮](../Page/西洋占星術.md "wikilink")。

  - [麥當勞的縮寫](../Page/麥當勞.md "wikilink")。*McDonalds*。