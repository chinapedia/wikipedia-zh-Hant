**姚中仁**（），[藝名](../Page/藝名.md "wikilink")**MC
HotDog**，又稱**mc熱狗**或簡稱**熱狗**，生於[台北](../Page/台北.md "wikilink")，[饒舌](../Page/饒舌.md "wikilink")[歌手](../Page/歌手.md "wikilink")。[台北市立建國中學補校](../Page/臺北市立建國高級中學附設高級進修補習學校.md "wikilink")、[輔仁大學影像傳播學士](../Page/輔仁大學.md "wikilink")。他的歌曲大多使用直白的語言對各種社會現象進行赤裸裸的揭露。

## 年表

  - 1996年，MC Hotdog
    錄取[輔仁大學影像傳播學系第二屆](../Page/輔仁大學.md "wikilink")，於該屆迎新晚會上表演
    Free style rap，開始小規模舞台表演。
  - 1998年，於饒舌樂相關[網站](../Page/網站.md "wikilink")《Master
    U》陸續公開個人創作的饒舌歌曲，成為網路饒舌愛好者口中的風雲人物。
  - 1999年，自製的音樂在[校園與](../Page/校園.md "wikilink")[網路間四處流傳](../Page/網路.md "wikilink")，成為校園發燒話題；自製音樂甚至出現[盜版](../Page/盜版.md "wikilink")[CD於](../Page/CD.md "wikilink")[光華商場販售](../Page/光華商場.md "wikilink")，音樂瞬間蔓延[華人市場](../Page/華人.md "wikilink")（台灣、[香港](../Page/香港.md "wikilink")、[中國大陆](../Page/中國大陆.md "wikilink")），成為網路最神秘的人物，被譽為「台灣地下饒舌第一人」。後來受[學校社團邀請](../Page/學校.md "wikilink")，於[Pub演唱](../Page/Pub.md "wikilink")，被Pub經營者發現而引薦給[魔岩唱片](../Page/魔岩唱片.md "wikilink")。
  - 2000年，簽約加入魔岩唱片旗下之「大馬戲團音樂工作室」，成員有好友[大支等人](../Page/大支.md "wikilink")。
  - 2001年，連續發行四張[EP](../Page/EP.md "wikilink")，引起樂壇轟動，唱片累計銷量逾30萬張。自輔仁大學文學院影像傳播學系畢業（非應屆畢業生，為延畢；領取學位為藝術學士）；年底，入伍服役。

[呂學淵塗鴉作品，《犬》，2001。.JPG](https://zh.wikipedia.org/wiki/File:呂學淵塗鴉作品，《犬》，2001。.JPG "fig:呂學淵塗鴉作品，《犬》，2001。.JPG")街頭[塗鴉先驅呂學淵為MC](../Page/塗鴉.md "wikilink")
HotDog 第二張[EP](../Page/EP.md "wikilink")《犬》封面製作的同名作品\]\]

  - 2003年年底，退伍。
  - 2004年，受[張震嶽邀請](../Page/張震嶽.md "wikilink")，跑遍全台puB巡迴演出。10月，受[美國最大連鎖Pub](../Page/美國.md "wikilink")「House
    of Blues」邀請，與張震嶽一起前往美國十大城市演出。
  - 2005年6月，受邀前往[香港擔任張震嶽演唱會嘉賓](../Page/香港.md "wikilink")。
  - 2006年1月，推出個人首張完整[專輯](../Page/錄音室專輯.md "wikilink")《[Wake
    Up](../Page/Wake_Up_\(MC_HotDog专辑\).md "wikilink")》。發行了個人專輯後慢慢走出自我的風格，和好友[大支也開始結束彼此的合作關係](../Page/大支.md "wikilink")。4月，於[台北世貿二館舉辦個人首次售票演唱會](../Page/台北世貿.md "wikilink")；門票在演唱會前兩週即售罄，[黃牛票在網路上喊價十倍](../Page/黃牛票.md "wikilink")。6月，參加[第17屆金曲獎演出](../Page/第17屆金曲獎.md "wikilink")。
  - 2007年6月，《[Wake
    Up](../Page/Wake_Up_\(MC_HotDog专辑\).md "wikilink")》專輯獲[第18屆金曲獎最佳國語專輯獎](../Page/第18屆金曲獎.md "wikilink")，表示其另類風格受評審肯定。（兩人在金曲獎頒獎典禮上的表演片段在[中國大陸地區禁止播出](../Page/中國大陸.md "wikilink")，其帶有粗口的專輯在中國大陸地區禁止發行。）
  - 2008年10月，推出第二張專輯《[差不多先生](../Page/差不多先生.md "wikilink")》。
  - 2012年7月，推出第三張專輯《[貧民百萬歌星](../Page/貧民百萬歌星.md "wikilink")》。
  - 2013年2月27日，首度登上[香港](../Page/香港.md "wikilink")[紅磡體育館舉辦個人售票演唱會](../Page/紅磡體育館.md "wikilink")。
  - 2013年8月3日，首度登上[台北小巨蛋舉辦個人售票演唱會](../Page/台北小巨蛋.md "wikilink")。
  - 2014年8月21日，向《[ETtoday
    東森新聞雲](../Page/ETtoday_東森新聞雲.md "wikilink")》坦言，在大學時曾在好奇心與朋友慫恿下[吸毒兩年](../Page/吸毒.md "wikilink")，曾經吸食[搖頭丸](../Page/搖頭丸.md "wikilink")、[大麻及](../Page/大麻_\(藥用\).md "wikilink")[K他命](../Page/K他命.md "wikilink")，吸毒亢奮完後反而心靈空虛、食慾不振、面黃肌瘦、沒精神；到了後期，吸毒後，心中會浮現悔意；直到有一天，突然想通了，決定把[手機裡藥頭及吸毒朋友們的電話號碼全部刪掉](../Page/手機.md "wikilink")，「吸毒傷身又花錢，沒意思，也沒必要一直玩」。坦率講出吸毒往事，他說：「玩過，沒什麼好避諱！」
  - 2015年7月10日，創作過無數經典歌曲的創作歌手[張震嶽](../Page/張震嶽.md "wikilink")，與華語饒舌指標性歌手MC
    HotDog(熱狗)，再加上華語音樂圈最潮嘻哈新人記錄王[頑童MJ116](../Page/頑童MJ116.md "wikilink")，在北京召開新聞發布會，宣布「兄弟本色日落黑趴世界巡迴演唱會」正式起航，並宣告「[兄弟本色](../Page/兄弟本色.md "wikilink")」時代正式降臨。
  - 2017年，擔任[《中國有嘻哈》明星製作人兼評審](../Page/中国有嘻哈.md "wikilink")
  - 2018年，擔任[《中國新說唱》明星製作人兼評審](../Page/中國新說唱.md "wikilink")
  - 2019年，加盟[我是唱作人](../Page/我是唱作人.md "wikilink")

## 音樂作品

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>曲目</p></th>
<th style="text-align: left;"><p>專輯資訊</p></th>
<th style="text-align: left;"><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>《MC HotDog》</p></td>
<td style="text-align: left;"><ol>
<li>Intro</li>
<li>讓我RAP</li>
<li>你要去那裡</li>
</ol></td>
<td style="text-align: left;"><p><a href="../Page/魔岩唱片.md" title="wikilink">魔岩唱片</a><br />
<a href="../Page/2001年1月.md" title="wikilink">2001年1月</a><br />
<a href="../Page/EP.md" title="wikilink">EP</a></p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>《犬》</p></td>
<td style="text-align: left;"><ol>
<li>Intro</li>
<li>我的生活</li>
<li>十三號天使</li>
<li>韓流來襲</li>
<li>西門町老人</li>
<li>Skit</li>
<li>畢業</li>
<li>韓流來襲─現場演唱版</li>
</ol></td>
<td style="text-align: left;"><p>魔岩唱片<br />
<a href="../Page/2001年4月.md" title="wikilink">2001年4月</a><br />
EP</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>《哈狗幫》</p></td>
<td style="text-align: left;"><ol>
<li>信仰</li>
<li>哈狗幫</li>
<li>快樂</li>
<li>補補補</li>
<li>Skit</li>
<li>1030</li>
<li>18歲</li>
</ol></td>
<td style="text-align: left;"><p>魔岩唱片<br />
<a href="../Page/2001年6月.md" title="wikilink">2001年6月</a><br />
EP</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>《九局下半》</p></td>
<td style="text-align: left;"><ol>
<li>九局下半</li>
<li>男生女生呸</li>
<li>快樂／十八歲／讓我RAP（VCD）</li>
<li>還沒玩完─記錄片+<a href="../Page/MV.md" title="wikilink">MV全紀錄</a></li>
</ol></td>
<td style="text-align: left;"><p>魔岩唱片<br />
<a href="../Page/2001年10月.md" title="wikilink">2001年10月</a><br />
EP+<a href="../Page/VCD.md" title="wikilink">VCD</a></p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>《<a href="../Page/Wake_Up_(MC_HotDog专辑).md" title="wikilink">Wake Up</a>》</p></td>
<td style="text-align: left;"><ol>
<li>Intro</li>
<li>我行我素</li>
<li>新人王</li>
<li>志玲姊姊來找我</li>
<li>我愛台妹（feat.<a href="../Page/張震嶽.md" title="wikilink">張震嶽</a>）</li>
<li>毋忘在莒</li>
<li>馬祖小夜曲</li>
<li>Introducing啞色小狗</li>
<li>2006冠軍情歌</li>
<li>Tiger Style</li>
<li>母老虎</li>
<li>我們的Party最扯</li>
<li>我小時候都去TU</li>
<li>煩死人</li>
<li>我的生活Ⅱ</li>
<li>Outro</li>
<li>安可</li>
</ol></td>
<td style="text-align: left;"><p><a href="../Page/滾石唱片.md" title="wikilink">滾石唱片</a><br />
<a href="../Page/2006年1月.md" title="wikilink">2006年1月</a><br />
專輯</p></td>
<td style="text-align: left;"><p><a href="../Page/第18屆金曲獎.md" title="wikilink">第18屆金曲獎最佳國語專輯獎</a></p></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>《差不多先生》</p></td>
<td style="text-align: left;"><ol>
<li>我瘋了</li>
<li>差不多先生</li>
<li>我哈你</li>
<li>海洋</li>
<li>謝謝啞唬</li>
<li>瞎王之王</li>
<li>毒</li>
<li>請拍手</li>
<li>怎麼能夠（feat.<a href="../Page/馬念先.md" title="wikilink">馬念先</a>）</li>
<li>他們覺得</li>
</ol></td>
<td style="text-align: left;"><p>滾石唱片<br />
2008年10月<br />
專輯</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>《貧民百萬歌星》</p></td>
<td style="text-align: left;"><ol>
<li>貧民百萬歌星(INTRO)</li>
<li>貧民百萬歌星</li>
<li>不吃早餐才是一件很嘻哈的事（feat.<a href="../Page/蛋堡_(音樂人).md" title="wikilink">蛋堡</a>）</li>
<li>和你巧遇在這裡SKIT</li>
<li>輕熟女 27（feat.<a href="../Page/關彥淳.md" title="wikilink">關彥淳</a>）</li>
<li>MC來了</li>
<li>好無聊（feat. <a href="../Page/A-Yue.md" title="wikilink">A-Yue</a>）</li>
<li>離開（feat.<a href="../Page/張震嶽.md" title="wikilink">張震嶽</a>）</li>
<li>嗨嗨人生（feat.張震嶽、<a href="../Page/關穎.md" title="wikilink">關穎</a>）</li>
<li>散場之後SKIT</li>
<li>After Party（feat. <a href="../Page/Scottie.md" title="wikilink">Scottie</a>）</li>
<li>嘿嘿Taxi</li>
<li>新聞快報SKIT</li>
<li>Live @ Legacy SKIT</li>
<li>Party Like HotDog</li>
<li>老子有錢</li>
<li>Woman 27（Acoustic Version）（feat.關穎）</li>
</ol></td>
<td style="text-align: left;"><p>滾石唱片<br />
2012年7月13日<br />
專輯</p></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

## 主持作品

  - [音樂電視網台灣頻道](../Page/音樂電視網.md "wikilink")（MTV.tw）《狗嘴不吐象牙》（*Hot Dog
    Show*，與大支合作）（播映期間：2007年9月11日）
  - 愛奇藝《[中國有嘻哈](../Page/中國有嘻哈.md "wikilink")》導師之一，與張震嶽搭檔講評。
  - 愛奇藝《[中國新說唱](../Page/中國新說唱.md "wikilink")》導師之一，與張震嶽再次搭檔。

## 其他作品

| 日期      | 作者        | 來源                                    | 作品    |
| ------- | --------- | ------------------------------------- | ----- |
| 1999年   | MC HotDog | 網路上傳                                  | 「釣蝦場」 |
| 2010年9月 | MC HotDog | 電影《[酷馬](../Page/酷馬.md "wikilink")》主題曲 | 「酷馬」  |

個人單曲

| 日期       | 合作                                                                                                                                    | 专辑                                               | 作品                                                     |
| -------- | ------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------ | ------------------------------------------------------ |
| 2002年4月  | 大支                                                                                                                                    | 电影《[愛情靈藥](../Page/愛情靈藥_\(電影\).md "wikilink")》主題曲 | 「命運青紅燈」                                                |
| 2007年    | [馬念先](../Page/馬念先.md "wikilink")                                                                                                      | 《不可能的Mix(2) 》                                    | 「斷背山」                                                  |
| 2007年7月  | [張震嶽](../Page/張震嶽.md "wikilink") , [侯佩岑](../Page/侯佩岑.md "wikilink")                                                                   | 《OK》                                             | 「就讓這首歌」                                                |
| 2007年7月  | [南拳媽媽](../Page/南拳媽媽.md "wikilink")                                                                                                    | 《藏寶圖》                                            | 「Here we go」                                           |
| 2008年4月  | [張信哲](../Page/張信哲.md "wikilink")                                                                                                      | 《逃生》                                             | 「單車與跑車」                                                |
| 2008年7月  | [參劈](../Page/參劈.md "wikilink")                                                                                                        | 《押韻的開始》                                          | 「巨蟒」                                                   |
| 2008年7月  | [丁噹](../Page/丁噹.md "wikilink")                                                                                                        | 《我愛上的》                                           | 「我不怕」                                                  |
| 2009年5月  | [馬念先](../Page/馬念先.md "wikilink")                                                                                                      | 女性保健合作                                           | 「我愛Yes Girl」                                           |
| 2009年6月  | [曹格](../Page/曹格.md "wikilink")                                                                                                        | 《超級4th場》                                         | 「咖哩熱狗」                                                 |
| 2009年7月  | [范瑋琪](../Page/范瑋琪.md "wikilink")                                                                                                      | 《F ONE》                                          | 「1到10=我和你」                                             |
| 2009年12月 | [林俊傑](../Page/林俊傑.md "wikilink")                                                                                                      | 《100天》                                           | 「加油」                                                   |
| 2010年6月  | [鄭秀文](../Page/鄭秀文.md "wikilink")                                                                                                      | 《信者得愛》                                           | 「信者得愛」(國語)                                             |
| 2011年8月  | [安心亞](../Page/安心亞.md "wikilink")                                                                                                      | 《[惡女](../Page/惡女_\(專輯\).md "wikilink")》          | 「快快愛」                                                  |
| 2011年12月 | [大支](../Page/大支.md "wikilink")                                                                                                        | 《人》                                              | 「芭樂」                                                   |
| 2011年12月 | [陳冠希](../Page/陳冠希.md "wikilink"),[李璨琛](../Page/李璨琛.md "wikilink"), [MC仁](../Page/MC仁.md "wikilink"), [廚房仔](../Page/廚房仔.md "wikilink") | 《幾點?》                                            | 「幾點?」                                                  |
| 2013年6月  | [小人](../Page/小人.md "wikilink"), [大支](../Page/大支.md "wikilink")                                                                        | 《小人國》                                            | 「變了好多」                                                 |
| 2013年8月  | 蛋堡, 葛仲珊                                                                                                                               | 合作單曲                                             | 「walk this way」                                        |
| 2013年9月  | [蔡健雅](../Page/蔡健雅.md "wikilink")                                                                                                      | 《天使與魔鬼的對話》                                       | 「Easy Come Easy GO」                                    |
| 2014年2月  | 黃崇旭, J Wu                                                                                                                             | 合作單曲                                             | 「Welcome back」                                         |
| 2014年4月  | [頑童MJ116](../Page/頑童MJ116.md "wikilink")                                                                                              | 《Fresh Game》                                     | 「[MJ FRESH GANG](../Page/MJ_FRESH_GANG.md "wikilink")」 |
| 2015年7月  | [岳云鹏](../Page/岳云鹏.md "wikilink")                                                                                                      | 电影《[煎饼侠](../Page/煎饼侠.md "wikilink")》插曲           | 「[五环之歌](../Page/五环之歌.md "wikilink")」                   |
| 2016年7月  | 陳冠希, 鄭秀文                                                                                                                              | 合作單曲                                             | 「MR SANDMAN」                                           |
| 2018年6月  | 熊仔, BR, 韓森, 小人, 大支                                                                                                                    | 「Peace?零皮草演唱會」                                   | 「PEACE?」                                               |
| 2018年7月  | 老莫 ILL MO                                                                                                                             | 合作單曲                                             | 「Nightclub love」                                       |
| 2018年9月  | 張震嶽, 派克特, 功夫胖                                                                                                                         | 合作單曲                                             | 「再見Hip hop」                                            |
| 2018年9月  | Tizzy T, Vava, 滿舒克                                                                                                                    | 合作單曲                                             | 「貧民百萬男孩」                                               |

**合作單曲**

| 日期      | 合作           | 組合名稱                                               | 专辑/EP            |
| ------- | ------------ | -------------------------------------------------- | ---------------- |
| 2013年9月 | 陳冠希          | 超級兄弟Super brothers                                 | 《Super brothers》 |
| 2016年1月 | 張震嶽, 頑童MJ116 | [兄弟本色G.U.T.S.](../Page/兄弟本色G.U.T.S..md "wikilink") | 《FLY OUT》        |
| 2017年4月 | 張震嶽, 頑童MJ116 | [兄弟本色G.U.T.S.](../Page/兄弟本色G.U.T.S..md "wikilink") | 《SKRU UP》        |

限定組合

| 年份     | 作詞/作曲     | 作品          |
| ------ | --------- | ----------- |
| 2019年  | MC HotDog | Hip-hop沒有派對 |
| 失眠是一種病 |           |             |
|        |           |             |

[我是唱作人創作單曲](../Page/我是唱作人.md "wikilink")

## 网络剧

  - 2015年
    [搜狐視頻](../Page/搜狐視頻.md "wikilink")《[極品女士4](../Page/極品女士4.md "wikilink")》

## 電影

  - 《[猛蟲過江](../Page/猛蟲過江.md "wikilink")》飾在虎

## 廣告

  - 銀河線上《老子有錢Online》
  - 逍遙互動娛樂《[犬夜叉](../Page/犬夜叉.md "wikilink") 尋玉之旅》

## 參考文獻

<div style="font-size: small">

<references/>

</div>

## 相關網站

  -
  -
  -
  - [MC HotDog 熱狗的影音日誌](http://www.im.tv/myvlog/mchotdog)

  - [狗嘴不吐象牙](http://www.mtvchinese.com/Channel/Show/HotDogShow/Index.html)

[M](../Category/台灣饒舌歌手.md "wikilink")
[Y姚](../Category/臺北市立建國高級中學校友.md "wikilink")
[Y姚](../Category/輔仁大學校友.md "wikilink")
[Y姚](../Category/金曲獎獲獎者.md "wikilink")
[Category:姚姓](../Category/姚姓.md "wikilink")
[Category:台北市人](../Category/台北市人.md "wikilink")
[Category:台灣嘻哈歌手](../Category/台灣嘻哈歌手.md "wikilink")
[Category:台灣嘻哈音樂家](../Category/台灣嘻哈音樂家.md "wikilink")