**奥兰莫罗迪亚俱乐部**（**Mouloudia Club of Oran**（简称：**MC
Oran**））（[阿拉伯语](../Page/阿拉伯语.md "wikilink")：نادي مولودية
وهران）是一家位于[阿尔及利亚](../Page/阿尔及利亚.md "wikilink")[奥兰的足球俱乐部](../Page/奥兰.md "wikilink")，俱乐部创建于1946年。球队的主场为。

## 球队荣誉

  - **[阿爾及利亞足球甲級聯賽冠军](../Page/阿爾及利亞足球甲級聯賽.md "wikilink")：4次**

<!-- end list -->

  -

      -
        1971年，1988年，1992年，1993年

<!-- end list -->

  - **[阿尔及利亚盃](../Page/阿尔及利亚盃.md "wikilink")**: 4次

<!-- end list -->

  -

      -
        1975年，1984年，1985年，1996年

<!-- end list -->

  - **[非洲冠军联赛亚军](../Page/非洲冠军联赛.md "wikilink")：1次**

<!-- end list -->

  -

      -
        1989年

## 外部链接

  - [Official website](http://www.mouloudia.com/)

[Category:阿尔及利亚足球俱乐部](../Category/阿尔及利亚足球俱乐部.md "wikilink")