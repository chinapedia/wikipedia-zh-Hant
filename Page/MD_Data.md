[Sony_MMD-140A.jpg](https://zh.wikipedia.org/wiki/File:Sony_MMD-140A.jpg "fig:Sony_MMD-140A.jpg")

**MD
Data**或**MD-Data**的全文為**[MiniDisc](../Page/MiniDisc.md "wikilink")-Data**，是一種用來儲存電腦資料的[磁光碟媒體](../Page/磁光碟.md "wikilink")（magneto-optical
medium），由[Sony於](../Page/Sony.md "wikilink")1993年發表。Sony原先期望用MD
Data取代[軟碟片](../Page/軟碟片.md "wikilink")，不過[艾美加](../Page/艾美加.md "wikilink")（Iomega）的[Zip軟碟機填補了這個市場的需求](../Page/Zip_Drive.md "wikilink")，之後又出現價位逐漸可被人接受的[CD-R](../Page/CD-R.md "wikilink")[光碟燒錄器以及相當低廉的空白可燒錄光碟片](../Page/光碟燒錄器.md "wikilink")，最後[記憶卡的普及成為](../Page/記憶卡.md "wikilink")[壓垮駱駝的最後一根稻草](../Page/壓垮駱駝的最後一根稻草.md "wikilink")，使MD-Data始終無法成為主流的電腦儲存媒體。

一張MD
Data碟片可提供140MB的資料儲存量，不過其存取效率緩慢且價格昂貴；此外，其存取碟機在播放模式時只能讀取或寫入MD音樂碟片，而不能供電腦存取資料。這表示：MD音樂片（MD）與MD資料片（MD
Data）被硬是區分，無法彈性混用。

Sony發表MD
Data後的不久，也推出了相關的應用週邊產品，如MD型[照相機或MD型文件](../Page/照相機.md "wikilink")[掃描器等](../Page/掃描器.md "wikilink")。到了1990年代後期，MD
Data也能使用4軌及8軌的多軌記錄層，這意味著使用MD
Data的MD型錄音設備可加速取代已普遍用於錄音工作室中的4軌帶匣錄音設備，不僅更低廉且更為彈性，同時在[Windows電腦](../Page/Windows.md "wikilink")、[Macintosh電腦上更可將錄音直接送入硬碟中](../Page/Macintosh.md "wikilink")。不過，MD
Data的儲存記錄格式卻阻礙了MD Data碟機的推行（即便MD Data碟機相當簡單易用），同時也阻礙將MD
Data用於電腦資料[備份的運用推行](../Page/備份.md "wikilink")。

2004年底，[Hi-MD出現](../Page/Hi-MD.md "wikilink")，允許在一張Hi-MD格式的MD碟片上儲存各種型態的資料（檔案、音樂、圖像等），且能讓標準MD達到約305MB的儲存容量，在新款的高密度Hi-MD碟片上更可達1GB容量。

附帶一提的是，1997年Sony推出過第二代的MD Data，稱為[MD
Data2](../Page/MD_Data2.md "wikilink")，單片容量從140MB增至650MB，不過只用於MD型[攝影機中](../Page/攝影機.md "wikilink")（1999年發表的Sony
DCM-M1），然而該攝影機的銷售並不久。

## 關連項目

  - [MiniDisc，簡稱：MD](../Page/MiniDisc.md "wikilink")

## 外部連結

  - [MiniDisc Data Product table 能存取MD
    Data碟片的應用產品表](http://www.minidisc.org/md_data_table.html)

[Category:電腦儲存媒體](../Category/電腦儲存媒體.md "wikilink")
[Category:聲音儲存](../Category/聲音儲存.md "wikilink")
[Category:索尼](../Category/索尼.md "wikilink")