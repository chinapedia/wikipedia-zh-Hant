**MDict**是一款用于[PC及](../Page/PC.md "wikilink")[移动设备的](../Page/移动设备.md "wikilink")[电子辞典软件](../Page/电子辞典.md "wikilink")，可以用于查看mdx[格式的词典文件](../Page/文件格式.md "wikilink")。
MDict软件在PC及绝大部分[智能手机有相应的客户端](../Page/智能手机.md "wikilink")，支持微软WM6.5系统、安卓系统的版本最早放出，支持[魅族M8的测试版本已放出](../Page/魅族M8.md "wikilink")，支持[iPhone及](../Page/iPhone.md "wikilink")[iPad的版本已在App](../Page/iPad.md "wikilink")
Store上線，支持[Bada版本已在](../Page/Bada.md "wikilink")[三星app](../Page/三星電子.md "wikilink")
Store上線。

MDict软件本身并不提供“词库”（mdx文件），但软件作者提供了词库制作工具(MDXBuilder)，词库格式采用压缩[算法来缩小体积](../Page/算法.md "wikilink")，同时支持读取多个词库文件。有民间爱好者自制的兼容软件也可读取此类词库，如[Android平台上的](../Page/Android.md "wikilink")
BlueDict
深藍，[EBPocket](../Page/EBPocket.md "wikilink")，EBDic，[SeederDict](../Page/SeederDict.md "wikilink")，Qdict，欧路词典都提供了读取Mdict词库的功能。另有对此文件的[格式分析](http://bitbucket.org/xwang/mdict-analysis)，以及多种开源实现[GoldenDict](../Page/GoldenDict.md "wikilink")，[C\#](https://mdict.codeplex.com)，[mdict-js:JavaScript实现的MDict字典解析器](https://github.com/fengdh/mdict-js)。

目前，由熱心網友为MDict制作的“词库”包括：中国大百科全书，百度百科，互动百科，大英百科，中日英德法韩文维基百科、维基字典、维基文库，天涯网文，现代汉语词典，成语词典，新华词典，唐诗、宋词、元曲鉴赏词典，牛津、朗文、剑桥英语（双解）词典；花鸟娱乐辞典，医学辞典，数理化辞典，美食词典等，截止2013年6月28日，Mdict“词典”共计1789部。

MDict支持简／繁体转换和真人发音。MDict软件本身也并不提供真人语音库，但词库制作工具可以制作语音库。目前，由網友为MDict制作的語音庫包括英文美音、英音，中文，日文，葡語，西班牙語，粵語等。

## 外部链接

  - [下载 MDict 程序](http://www.mdict.cn/wp/?page_id=5227&lang=zh)
  - [MDICT官方微博](http://weibo.com/mdictcn/)

[Category:应用软件](../Category/应用软件.md "wikilink")