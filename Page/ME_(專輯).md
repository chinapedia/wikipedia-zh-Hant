**《ME》**是香港歌手[薛凱琪的個人專輯](../Page/薛凱琪.md "wikilink")，由[華納唱片公司製作](../Page/華納唱片_\(唱片\).md "wikilink")，於2005年12月22日推出，截至2006年2月底為止，銷量估計約為20,000張。

## 簡介

**《ME》**乃薛凱琪自2003年出道以來第三張個人專輯，碟中收錄了6首[粵語歌曲及一首改編自前作的](../Page/粵語.md "wikilink")[國語歌曲](../Page/國語.md "wikilink")，由多位不同監製操刀，碟中歌曲仍以抒發少女情懷為主，歌詞除了談情說愛外，亦不乏偏向正面的題材。專輯並附有歌手自行創作的畫作及熱火樂團演唱會DVD，連同來自所拍[電影的歌曲](../Page/電影.md "wikilink")，這張專輯可說總結了薛凱琪全年的工作。

在本CD中共有7首歌曲，除派台歌《有隻雀仔》、《尋找獨角獸》外，尚有來自電影《[摯愛](../Page/摯愛.md "wikilink")》的插曲《愛》、其他歌曲《水》、《白色戀人》，在廣播劇《[亞卡比槍擊事件](../Page/亞卡比槍擊事件.md "wikilink")》中的《你在哪裡？》及改編自成名作《奇洛李維斯回信》的國語歌曲《從金銀島寄來的信》。

## 專輯元素

1.  CD一張，含7首歌曲
2.  熱火樂團音樂會DVD一片
3.  歌手創作畫作一本

## 曲目

[Category:薛凱琪音樂專輯](../Category/薛凱琪音樂專輯.md "wikilink")
[Category:2005年音樂專輯](../Category/2005年音樂專輯.md "wikilink")