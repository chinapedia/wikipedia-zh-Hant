**MF文庫J**（），是[日本](../Page/日本.md "wikilink")[出版社](../Page/出版社.md "wikilink")[KADOKAWA旗下](../Page/KADOKAWA.md "wikilink")[Media
Factory的](../Page/Media_Factory.md "wikilink")[輕小說](../Page/輕小說.md "wikilink")[文庫系列](../Page/文庫.md "wikilink")。2002年7月25日創刊。

## 簡介

「MF文庫J」命名自主要出版國外翻譯書籍（電影的小說作品）的[MF文庫](../Page/MF文庫.md "wikilink")，加上「日本」（Japan），以及「少年」（Juvenile）的第一個字母作為命名。特徵是綠色書背。

創刊初期，除了[輕小說外還有出版許多](../Page/輕小說.md "wikilink")[動畫](../Page/動畫.md "wikilink")、[遊戲等的改編小說作品](../Page/遊戲.md "wikilink")，從2004年舉辦[MF文庫J輕小說新人獎開始出版原創作品後](../Page/MF文庫J輕小說新人獎.md "wikilink")，評價才逐漸上升。2006年有《[陰守忍者](../Page/陰守忍者.md "wikilink")》（，[阿智太郎著](../Page/阿智太郎.md "wikilink")）、《[神樣家族](../Page/神樣家族.md "wikilink")》（，[桑島由一著](../Page/桑島由一.md "wikilink")）、《[零之使魔](../Page/零之使魔.md "wikilink")》（，[山口昇著](../Page/山口昇.md "wikilink")）等熱門作品改編成動畫。

MF文庫J近年來積極進行電子出版。比較新的書籍，有發行價格較低的「最強☆讀書生活」系列[電子書](../Page/電子書.md "wikilink")。

不知道編輯部是否刻意，以四個[平假名為標題的作品相當多](../Page/平假名.md "wikilink")，如《[我的狐仙女友](../Page/我的狐仙女友.md "wikilink")》（）、《[神燈女僕](../Page/神燈女僕.md "wikilink")》（）、《[MM一族](../Page/MM一族.md "wikilink")》（）、《[骷髏戀人](../Page/骷髏戀人.md "wikilink")》（）等。

2015年4月，角川公司廢止[分公司制](../Page/分公司制.md "wikilink")，並於2016年1月起不在出版品上使用Media
Factory商標，但仍保留「MF文庫J」這個書系、設立編輯部，並續辦MF文庫J輕小說新人獎。

## 作品列表

下列僅列出已翻譯成中文的作品，並以中譯版出版社為分類，部份作品的正、簡體中文版分別由不同出版社出版，亦分別列出。括號內分別為作者及插畫繪者。

### [東立出版社](../Page/東立出版社.md "wikilink")

  - [青葉君與宇宙人](../Page/青葉君與宇宙人.md "wikilink")（[松野秋鳴](../Page/松野秋鳴.md "wikilink")／[超肉](../Page/超肉.md "wikilink")）
  - [拯救你的最初咒語](../Page/拯救你的最初咒語.md "wikilink")（[須堂項](../Page/須堂項.md "wikilink")／）
  - [MM一族](../Page/MM一族.md "wikilink")（松野秋鳴／[QP:flapper](../Page/QP:flapper.md "wikilink")）
  - [萬歲系列](../Page/萬歲系列.md "wikilink")（[三浦勇雄](../Page/三浦勇雄.md "wikilink")／[屡那](../Page/屡那.md "wikilink")）
  - [月兔升空時](../Page/月兔升空時.md "wikilink")（[平坂読](../Page/平坂読.md "wikilink")／）
  - [幽靈戀人](../Page/幽靈戀人.md "wikilink")（平坂読／[片瀨優](../Page/片瀨優.md "wikilink")）
  - [魔像怪X少女](../Page/魔像怪X少女.md "wikilink")（[大凹友數](../Page/大凹友數.md "wikilink")／[KEI](../Page/KEI.md "wikilink")）
  - [在暗夜中尋找羔羊](../Page/在暗夜中尋找羔羊.md "wikilink")（[穗史賀雅也](../Page/穗史賀雅也.md "wikilink")／）
  - [櫻乃綺羅帆系列](../Page/櫻乃綺羅帆系列.md "wikilink")（[月見草平](../Page/月見草平.md "wikilink")／）
  - [骷髏戀人](../Page/骷髏戀人.md "wikilink")（平坂読／）
  - [姬宮學姊的內在美](../Page/姬宮學姊的內在美.md "wikilink")（月見草平／[Ein](../Page/Ein.md "wikilink")）
  - [魔女的紅線](../Page/魔女的紅線.md "wikilink")（[田口一](../Page/田口一.md "wikilink")／）
  - [輕小說社](../Page/輕小說社.md "wikilink")（平坂読／）
  - [聖劍鍛造師](../Page/聖劍鍛造師.md "wikilink")（三浦勇雄／屡那）
  - [我倆開始征服世界](../Page/我倆開始征服世界.md "wikilink")（／[高階＠聖人](../Page/高階＠聖人.md "wikilink")）
  - [魔界新娘！](../Page/魔界新娘！.md "wikilink")（[阿智太郎](../Page/阿智太郎.md "wikilink")／[x6suke](../Page/x6suke.md "wikilink")）
  - [我甜蜜的苦澀委內瑞拉](../Page/我甜蜜的苦澀委內瑞拉.md "wikilink")（[森田季節](../Page/森田季節.md "wikilink")／[文倉十](../Page/文倉十.md "wikilink")）
  - [九之契約書](../Page/九之契約書.md "wikilink")（[二階堂紘嗣](../Page/二階堂紘嗣.md "wikilink")／）
  - [輝夜魔王式！](../Page/輝夜魔王式！.md "wikilink")（月見草平／[水澤深森](../Page/水澤深森.md "wikilink")）
  - [菖蒲的少女革命！](../Page/菖蒲的少女革命！.md "wikilink")（[志茂文彦](../Page/志茂文彦.md "wikilink")／）
  - [戀愛製造機！](../Page/戀愛製造機！.md "wikilink")（[本田透](../Page/本田透.md "wikilink")／[鶴崎貴大](../Page/鶴崎貴大.md "wikilink")）
  - [德魯伊來囉！](../Page/德魯伊來囉！.md "wikilink")（[志瑞祐](../Page/志瑞祐.md "wikilink")／[絶叫](../Page/絶叫.md "wikilink")）
  - [水仙花narcissu](../Page/narcissu.md "wikilink")（／／）
  - [劍之女王與烙印之子](../Page/劍之女王與烙印之子.md "wikilink")（[杉井光](../Page/杉井光.md "wikilink")／[夕仁](../Page/夕仁.md "wikilink")）
  - [黑姬柚葉](../Page/黑姬柚葉.md "wikilink")（田口一／）
  - [戀愛超能力](../Page/戀愛超能力.md "wikilink")（[樋口司](../Page/樋口司.md "wikilink")／）
  - [天川天音的否定公式](../Page/天川天音的否定公式.md "wikilink")（[葉村哲](../Page/葉村哲.md "wikilink")／）
  - [X的魔王](../Page/X的魔王.md "wikilink")（[伊都工平](../Page/伊都工平.md "wikilink")／）
  - [我心之劍](../Page/我心之劍.md "wikilink")（[十文字青](../Page/十文字青.md "wikilink")／[kaya8](../Page/kaya8.md "wikilink")）
  - [HURTLESS/HURTFUL](../Page/HURTLESS/HURTFUL.md "wikilink")（／）
  - [原點回歸Walkers](../Page/原點回歸Walkers.md "wikilink")（森田季節／[深崎暮人](../Page/深崎暮人.md "wikilink")）
  - [極道彼女！](../Page/極道彼女！.md "wikilink")（／）
  - [御火槌](../Page/御火槌.md "wikilink")（[榊一郎](../Page/榊一郎.md "wikilink")／[中村龍徳](../Page/中村龍徳.md "wikilink")）
  - [失禮了！我是垃圾桶妖怪](../Page/失禮了！我是垃圾桶妖怪.md "wikilink")（[岩波零](../Page/岩波零.md "wikilink")／[異識](../Page/異識.md "wikilink")）
  - [放學後的悠閒時光](../Page/放學後的悠閒時光.md "wikilink")（[比嘉智康](../Page/比嘉智康.md "wikilink")／）
  - [星刻龍騎士](../Page/星刻龍騎士.md "wikilink")（[瑞智士記](../Page/瑞智士記.md "wikilink")／）
  - [第101篇百物語](../Page/第101篇百物語.md "wikilink")（／[涼香](../Page/涼香.md "wikilink")）
  - [其中1個是妹妹！](../Page/其中1個是妹妹！.md "wikilink")（田口一／[CUTEG](../Page/CUTEG.md "wikilink")）
  - [我與一乃的遊戲同好會活動日誌](../Page/我與一乃的遊戲同好會活動日誌.md "wikilink")（葉村哲／）
  - [就算是哥哥，有愛就沒問題了，對吧](../Page/就算是哥哥，有愛就沒問題了，對吧.md "wikilink")（[鈴木大輔](../Page/鈴木大輔.md "wikilink")／[閏月戈](../Page/閏月戈.md "wikilink")）
  - [月神來我家！](../Page/月神來我家！.md "wikilink")（[後藤祐迅](../Page/後藤祐迅.md "wikilink")／）
  - [美少女幽靈的戀愛效應！](../Page/美少女幽靈的戀愛效應！.md "wikilink")（／）
  - [黑之夜魔](../Page/黑之夜魔.md "wikilink")（十文字青／[硯](../Page/硯_\(插畫家\).md "wikilink")）
  - [斬光王劍錄](../Page/斬光王劍錄.md "wikilink")（[穗村元](../Page/穗村元.md "wikilink")／）
  - [白銀的城姬](../Page/白銀的城姬.md "wikilink")（志瑞祐／[上田夢人](../Page/上田夢人.md "wikilink")）
  - [精靈使的劍舞](../Page/精靈使的劍舞.md "wikilink")（志瑞祐／）
  - [這間教室被不回家社占領了](../Page/這間教室被不回家社占領了.md "wikilink")（／）
  - [魔彈之王與戰姬](../Page/魔彈之王與戰姬.md "wikilink")（[川口士](../Page/川口士.md "wikilink")／）
  - [夢魔少年](../Page/夢魔少年.md "wikilink")（[三門鐵狼](../Page/三門鐵狼.md "wikilink")／）
  - [修羅場戀人](../Page/修羅場戀人.md "wikilink")（[岸杯也](../Page/岸杯也.md "wikilink")／）
  - [會飛的豬還是豬](../Page/會飛的豬還是豬.md "wikilink")（[涼木行](../Page/涼木行.md "wikilink")／[白身魚](../Page/白身魚.md "wikilink")）
  - [NOBLE
    LIGE！](../Page/NOBLE_LIGE！.md "wikilink")（[TAMAMI](../Page/TAMAMI.md "wikilink")／）
  - [不吸血的吸血鬼](../Page/不吸血的吸血鬼.md "wikilink")（／[Yuyi](../Page/Yuyi.md "wikilink")）
  - [NO GAME NO LIFE
    遊戲人生](../Page/NO_GAME_NO_LIFE_遊戲人生.md "wikilink")（[榎宮祐](../Page/榎宮祐.md "wikilink")）
  - [你並不孤單！](../Page/你並不孤單！.md "wikilink")（[小岩井蓮二](../Page/小岩井蓮二.md "wikilink")／）
  - [Over Image
    超異能遊戲](../Page/Over_Image_超異能遊戲.md "wikilink")（[遊佐真弘](../Page/遊佐真弘.md "wikilink")／）
  - [歌姬少女的創樂譜](../Page/歌姬少女的創樂譜.md "wikilink")（[雨野智晴](../Page/雨野智晴.md "wikilink")／）
  - [穿越時空的龍王與邁向滅亡的魔女之國](../Page/穿越時空的龍王與邁向滅亡的魔女之國.md "wikilink")（[舞阪洸](../Page/舞阪洸.md "wikilink")／）
  - [終焉之栞](../Page/終焉之栞.md "wikilink")（／[主犯：１５０Ｐ](../Page/主犯：１５０Ｐ.md "wikilink")／／）
  - [無力氣勇者與好奇的魔王](../Page/無力氣勇者與好奇的魔王.md "wikilink")（[冬木冬樹](../Page/冬木冬樹.md "wikilink")／[水月悠](../Page/水月悠.md "wikilink")）
  - [巴洛克騎士](../Page/巴洛克騎士.md "wikilink")（葉村哲／）
  - [不可以玩這種遊戲！](../Page/不可以玩這種遊戲！.md "wikilink")（岩波零／[皆村春樹](../Page/皆村春樹.md "wikilink")）
  - [獻給失落之神的聖謠譚](../Page/獻給失落之神的聖謠譚.md "wikilink")（[内田俊](../Page/内田俊.md "wikilink")／硯）
  - [悠久騎士](../Page/悠久騎士.md "wikilink")（[七烏未奏](../Page/七烏未奏.md "wikilink")／）
  - [舞風的鎧姬](../Page/舞風的鎧姬.md "wikilink")（／[片桐雛太](../Page/片桐雛太.md "wikilink")）
  - [椎名町學姊的安全日](../Page/椎名町學姊的安全日.md "wikilink")（／[CARNELIAN](../Page/CARNELIAN.md "wikilink")）
  - [話說當時我還不是主角](../Page/話說當時我還不是主角.md "wikilink")（二階堂紘嗣／）
  - [沉睡魔女](../Page/沉睡魔女.md "wikilink")（[真野真央](../Page/真野真央.md "wikilink")／）

###

  - [陰守忍者](../Page/陰守忍者.md "wikilink")（[阿智太郎](../Page/阿智太郎.md "wikilink")／）
  - [神樣家族](../Page/神樣家族.md "wikilink")（[桑島由一](../Page/桑島由一.md "wikilink")／）
  - [風水學園](../Page/風水學園.md "wikilink")（[夏綠](../Page/夏綠.md "wikilink")／[凪良](../Page/凪良.md "wikilink")）

### [尖端出版](../Page/尖端出版.md "wikilink")

  - [巖窟王](../Page/巖窟王.md "wikilink")（[神山修一](../Page/神山修一.md "wikilink")／[松原秀典](../Page/松原秀典.md "wikilink")）
  - [零之使魔](../Page/零之使魔.md "wikilink")（[山口昇](../Page/山口昇.md "wikilink")／）
  - [銃姬](../Page/銃姬.md "wikilink")（[高殿円](../Page/高殿円.md "wikilink")／）
  - [星之聲](../Page/星之聲.md "wikilink")（原作、原畫：[新海誠](../Page/新海誠.md "wikilink")、[大場惑](../Page/大場惑.md "wikilink")／[柳沼行](../Page/柳沼行.md "wikilink")）
  - [蟲，眼球系列](../Page/蟲，眼球系列.md "wikilink")（[日日日](../Page/日日日.md "wikilink")／）
  - [陰沉少女與黑魔法之戀](../Page/陰沉少女與黑魔法之戀.md "wikilink")（[熊谷雅人](../Page/熊谷雅人.md "wikilink")／）
  - [我的狐仙女友](../Page/我的狐仙女友.md "wikilink")（／[狐印](../Page/狐印.md "wikilink")）
  - [神燈女僕\!](../Page/神燈女僕!.md "wikilink")（[夏綠](../Page/夏綠.md "wikilink")／）
  - [肯普法](../Page/肯普法.md "wikilink")（[築地俊彥](../Page/築地俊彥.md "wikilink")／）
  - [天空與蒼月之王](../Page/天空與蒼月之王.md "wikilink")（[霜島桂](../Page/霜島桂.md "wikilink")／[Ginka](../Page/Ginka.md "wikilink")）
  - [PiPit \!\!
    天線少女](../Page/PiPit_!!_天線少女.md "wikilink")（[和智正喜](../Page/和智正喜.md "wikilink")／）
  - [玩伴貓耳娘](../Page/玩伴貓耳娘.md "wikilink")（[神野奧那](../Page/神野奧那.md "wikilink")／[放電映像](../Page/放電映像.md "wikilink")）
  - [瀆神之主](../Page/瀆神之主.md "wikilink")（[榊一郎](../Page/榊一郎.md "wikilink")／[kyo](../Page/kyo.md "wikilink")）
  - [緋彈的亞莉亞](../Page/緋彈的亞莉亞.md "wikilink")（[赤松中學](../Page/赤松中學.md "wikilink")／[小舞一](../Page/小舞一.md "wikilink")）
  - [愛說謊的精靈妹妹](../Page/愛說謊的精靈妹妹.md "wikilink")（／[toi8](../Page/toi8.md "wikilink")）
  - [渚的極強音](../Page/渚的極強音.md "wikilink")（[城崎火也](../Page/城崎火也.md "wikilink")／[桐野霞](../Page/桐野霞.md "wikilink")）
  - [失落的碎片](../Page/失落的碎片.md "wikilink")（／）
  - [少女大神！](../Page/少女大神！.md "wikilink")（[比嘉智康](../Page/比嘉智康.md "wikilink")／[河原惠](../Page/河原惠.md "wikilink")）
  - [變裝魔界留學生](../Page/變裝魔界留學生.md "wikilink")（[野島研二](../Page/野島研二.md "wikilink")／[武藤此史](../Page/武藤此史.md "wikilink")）
  - [絕境行星](../Page/絕境行星.md "wikilink")（[柿沼秀樹](../Page/柿沼秀樹.md "wikilink")／[駒都英二](../Page/駒都英二.md "wikilink")）
  - [魔女學生會長](../Page/魔女學生會長.md "wikilink")（日日日／[鈴見敦](../Page/鈴見敦.md "wikilink")）
  - [IS〈Infinite
    Stratos〉](../Page/IS〈Infinite_Stratos〉.md "wikilink")（／[okiura](../Page/okiura.md "wikilink")）
  - [我的朋友很少](../Page/我的朋友很少.md "wikilink")（[平坂讀](../Page/平坂讀.md "wikilink")／）
  - [迷茫管家與膽怯的我](../Page/迷茫管家與膽怯的我.md "wikilink")（[朝野始](../Page/朝野始.md "wikilink")／[菊池政治](../Page/菊池政治.md "wikilink")）
  - [蒼柩的青金石](../Page/蒼柩的青金石.md "wikilink")（[朝野始](../Page/朝野始.md "wikilink")／[菊池政治](../Page/菊池政治.md "wikilink")）
  - [少女，兵器，潘朵拉](../Page/少女，兵器，潘朵拉.md "wikilink")（[西野勝海](../Page/西野勝海.md "wikilink")／[蔓木鋼音](../Page/蔓木鋼音.md "wikilink")）
  - [宙士諾德！](../Page/宙士諾德！.md "wikilink")（赤松中學／[bomi](../Page/bomi.md "wikilink")）
  - [變態王子與不笑貓](../Page/變態王子與不笑貓.md "wikilink")（[相樂總](../Page/相樂總.md "wikilink")／）
  - [推薦一起共度初體驗的她](../Page/推薦一起共度初體驗的她.md "wikilink")（朝野始／[高苗京鈴](../Page/高苗京鈴.md "wikilink")）
  - [機巧少女不會受傷](../Page/機巧少女不會受傷.md "wikilink")（／）
  - [T、內褲、好運到](../Page/T、內褲、好運到.md "wikilink")（[木村大志](../Page/木村大志.md "wikilink")／[前田理想](../Page/前田理想.md "wikilink")）
  - [拿出男子氣概吧！倉田君！](../Page/拿出男子氣概吧！倉田君！.md "wikilink")（[齋藤真也](../Page/齋藤真也.md "wikilink")／[Fumio](../Page/Fumio.md "wikilink")）
  - [盟約的利維坦](../Page/盟約的利維坦.md "wikilink")（[丈月城](../Page/丈月城.md "wikilink")／[仁村有志](../Page/仁村有志.md "wikilink")）
  - [學戰都市Asterisk](../Page/學戰都市Asterisk.md "wikilink")（／[okiura](../Page/okiura.md "wikilink")）
  - [劍神的繼承者](../Page/劍神的繼承者.md "wikilink")（[鏡遊](../Page/鏡遊.md "wikilink")／[MIKEOU](../Page/みけおう.md "wikilink")）
  - [魔法戰爭](../Page/魔法戰爭.md "wikilink")（／[瑠奈璃亞](../Page/瑠奈璃亞.md "wikilink")）
  - [聲優公主](../Page/聲優公主.md "wikilink")（[太田顯喜](../Page/太田顯喜.md "wikilink")／[雛咲](../Page/雛咲.md "wikilink")）
  - [銀彈的銃劍姬](../Page/銀彈的銃劍姬.md "wikilink")（[紫雪夜](../Page/紫雪夜.md "wikilink")／[鶴崎貴大](../Page/鶴崎貴大.md "wikilink")）
  - [Cheers\!愛的鼓勵](../Page/Cheers!愛的鼓勵.md "wikilink")（[赤松中學](../Page/赤松中學.md "wikilink")／[小舞一](../Page/小舞一.md "wikilink")）

### [台灣角川](../Page/台灣角川.md "wikilink")

  - [我被女生倒追，惹妹妹生氣了？](../Page/我被女生倒追，惹妹妹生氣了？.md "wikilink")（／[武藤此史](../Page/武藤此史.md "wikilink")）
  - [絕對雙刃](../Page/絕對雙刃.md "wikilink")（／）
  - [小惡魔緹莉與救世主\!?](../Page/小惡魔緹莉與救世主!?.md "wikilink")（[衣笠彰梧](../Page/衣笠彰梧.md "wikilink")／）
  - 我們就愛肉麻放閃耍甜蜜（[風見周](../Page/風見周.md "wikilink")／[高品有桂](../Page/高品有桂.md "wikilink")）

### [青文出版社](../Page/青文出版社.md "wikilink")

  - [在那個夏天等待](../Page/在那個夏天等待.md "wikilink")（[豐川一夏](../Page/豐川一夏.md "wikilink")／）
  - [森羅萬象統御者](../Page/森羅萬象統御者.md "wikilink")（[水月紗鳥](../Page/水月紗鳥.md "wikilink")／[有河聖](../Page/有河聖.md "wikilink")）
  - [少女與戰車](../Page/少女與戰車.md "wikilink")（／、）

### [天聞角川](../Page/天聞角川.md "wikilink")

  - [緋彈的亞莉亞](../Page/緋彈的亞莉亞.md "wikilink")（[赤松中学](../Page/赤松中学.md "wikilink")／[小舞一](../Page/小舞一.md "wikilink")）
  - [我与亲爱哥哥的日常](../Page/我与亲爱哥哥的日常.md "wikilink")（[铃木大辅](../Page/铃木大辅.md "wikilink")／[闰月戈](../Page/闰月戈.md "wikilink")）
  - [迷茫管家与懦弱的我](../Page/迷茫管家与懦弱的我.md "wikilink")（[朝野始](../Page/朝野始.md "wikilink")／[菊池政治](../Page/菊池政治.md "wikilink")）
  - [变态王子与不笑猫](../Page/变态王子与不笑猫.md "wikilink")（[相乐总](../Page/相乐总.md "wikilink")／[监督](../Page/監督_\(插畫家\).md "wikilink")）
  - [圣剑锻造师](../Page/圣剑锻造师.md "wikilink")（[三浦勇雄](../Page/三浦勇雄.md "wikilink")／[屡那](../Page/屡那.md "wikilink")）
  - [遊戲人生](../Page/遊戲人生.md "wikilink")（[榎宮祐](../Page/榎宮祐.md "wikilink")）
  - [我被女生倒追，惹妹妹生气了？](../Page/我被女生倒追，惹妹妹生气了？.md "wikilink")（[野島けんじ](../Page/野島けんじ.md "wikilink")／[武藤此史](../Page/武藤此史.md "wikilink")）
  - [月神来我家](../Page/月神來我家！.md "wikilink")（[後藤祐迅](../Page/後藤祐迅.md "wikilink")／[梱枝Riko](../Page/梱枝Riko.md "wikilink")）
  - [机巧少女不受伤](../Page/機巧少女不會受傷.md "wikilink")（[海冬零兒](../Page/海冬零兒.md "wikilink")／[LLO](../Page/LLO.md "wikilink")）
  - [其中一个是妹妹](../Page/其中1個是妹妹！.md "wikilink")（[田口一](../Page/田口一.md "wikilink")／[CUTEG](../Page/CUTEG.md "wikilink")）
  - [星刻的龙骑士](../Page/星刻龙骑士.md "wikilink")（[瑞智士記](../Page/瑞智士記.md "wikilink")／[染鲭小鳍](../Page/染鲭小鳍.md "wikilink")）

### [新星出版社](../Page/新星出版社.md "wikilink")

  - [幽灵恋人](../Page/幽灵恋人.md "wikilink")（[平坂读](../Page/平坂读.md "wikilink")／[片濑优](../Page/片濑优.md "wikilink")）

## 關連項目

  - [月刊Comic Alive](../Page/月刊Comic_Alive.md "wikilink")
  - [MF文庫J輕小說新人獎](../Page/MF文庫J輕小說新人獎.md "wikilink")
  - [Media Factory](../Page/Media_Factory.md "wikilink")

## 外部連結

  - [MF文庫J官方網站](http://www.mediafactory.co.jp/bunkoj/index.html)
  - [MF文庫J | ストリエ](https://storie.jp/official/channel/mfbunkoj) -
    Web小說連載
  - [尖端出版](http://www.spp.com.tw/)
  - [東立漫遊網](http://www.tongli.com.tw)
  - [銘顯文化](https://web.archive.org/web/20131205234405/http://www.ezla.com.tw/)

[\*](../Category/MF文庫J.md "wikilink")