**MF文庫J輕小說新人獎**（****），是[Media
Factory所主辦的](../Page/Media_Factory.md "wikilink")[輕小說新人獎](../Page/輕小說.md "wikilink")。以三個月為單位，每年舉辦四次的預備審查，然後再從中選出整年的最優秀獎（亦有從缺的情況：到2007年九月為止，沒有出現過最優秀獎）。佳作以上的作品會由同社的輕小說文庫《[MF文庫J](../Page/MF文庫J.md "wikilink")》出版。

最優秀獎得獎者可以得到正賞的盾與副賞100萬日元，優秀獎50萬日元，佳作10萬日元。

## 得獎作品

系列作有不同副標題時，可能有會省去獲獎作副標題的情況。未有中文書名以日文原名表示。本表不含以最终审核作者身份出道之作者。

<table>
<thead>
<tr class="header">
<th><p>回數</p></th>
<th><p>獎項</p></th>
<th><p>標題<br />
（出版時標題）</p></th>
<th><p>作者<br />
（出版時<a href="../Page/筆名.md" title="wikilink">筆名</a>）</p></th>
<th><p>其他</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第0回[1]<br />
（2004年）</p></td>
<td><p>優秀獎</p></td>
<td><p><a href="../Page/幽靈戀人.md" title="wikilink">幽靈戀人</a></p></td>
<td><p><a href="../Page/平坂讀.md" title="wikilink">平坂讀</a></p></td>
<td><p><a href="../Page/東立出版社.md" title="wikilink">東立出版社代理</a><br />
<a href="../Page/新星出版社.md" title="wikilink">新星出版社代理</a></p></td>
</tr>
<tr class="even">
<td><p>佳作</p></td>
<td></td>
<td><p>羽田奈緒子</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第1回[2]<br />
（2005年）</p></td>
<td><p>優秀獎</p></td>
<td><p><br />
（<a href="../Page/青葉君與宇宙人.md" title="wikilink">青葉君與宇宙人</a>）</p></td>
<td><p>秋鳴<br />
（<a href="../Page/松野秋鳴.md" title="wikilink">松野秋鳴</a>）</p></td>
<td><p>東立出版社代理</p></td>
</tr>
<tr class="even">
<td><p>佳作</p></td>
<td><p><a href="../Page/陰沉少女與黑魔法之戀.md" title="wikilink">陰沉少女與黑魔法之戀</a></p></td>
<td><p><a href="../Page/熊谷雅人.md" title="wikilink">熊谷雅人</a></p></td>
<td><p><a href="../Page/尖端出版.md" title="wikilink">尖端出版代理</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><br />
（<a href="../Page/我的狐仙女友.md" title="wikilink">我的狐仙女友</a>）</p></td>
<td><p>名波薰2号<br />
（<a href="../Page/西野勝海.md" title="wikilink">西野勝海</a>）</p></td>
<td><p>尖端代理</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/魔像怪X少女.md" title="wikilink">魔像怪X少女</a></p></td>
<td><p><a href="../Page/大凹友数.md" title="wikilink">大凹友数</a></p></td>
<td><p>東立出版社代理</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>編輯長<br />
特別獎</p></td>
<td><p><a href="../Page/蟲，眼球系列.md" title="wikilink">蟲、眼球、泰迪熊</a></p></td>
<td><p><a href="../Page/日日日.md" title="wikilink">日日日</a></p></td>
<td><p>尖端代理</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>審査員<br />
特別獎</p></td>
<td><p><br />
（）</p></td>
<td><p><a href="../Page/月見草平.md" title="wikilink">月見草平</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/萬歲系列.md" title="wikilink">聖誕節萬歲</a></p></td>
<td><p><a href="../Page/三浦勇雄.md" title="wikilink">三浦勇雄</a></p></td>
<td><p>東立出版社代理</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><br />
（）</p></td>
<td><p><br />
（<a href="../Page/須堂項.md" title="wikilink">須堂項</a>）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第2回<br />
（2006年）</p></td>
<td><p>優秀獎</p></td>
<td><p><br />
（<a href="../Page/在暗夜中尋找羔羊.md" title="wikilink">在暗夜中尋找羔羊</a>）</p></td>
<td><p><a href="../Page/穂史賀雅也.md" title="wikilink">穂史賀雅也</a></p></td>
<td><p>東立出版社代理</p></td>
</tr>
<tr class="even">
<td><p>佳作</p></td>
<td></td>
<td><p>岡崎新之助<br />
（<a href="../Page/内山靖二郎.md" title="wikilink">内山靖二郎</a>）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>World's tale 〜a girl meets the boy〜<br />
（）</p></td>
<td><p>矢塚<br />
（<a href="../Page/早矢塚かつや.md" title="wikilink">早矢塚かつや</a>）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第3回<br />
（2007年）</p></td>
<td><p>優秀獎</p></td>
<td><p><br />
（<a href="../Page/少女大神！地方都市傳說大全.md" title="wikilink">少女大神！地方都市傳說大全</a>）</p></td>
<td><p>安宅代智<br />
（<a href="../Page/比嘉智康.md" title="wikilink">比嘉智康</a>）</p></td>
<td><p>尖端代理</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/宙士諾德.md" title="wikilink">宙士諾德</a></p></td>
<td><p><a href="../Page/赤松中学.md" title="wikilink">赤松中学</a></p></td>
<td><p>尖端代理</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>佳作</p></td>
<td><p><a href="../Page/魔女的紅線.md" title="wikilink">魔女的紅線</a></p></td>
<td><p><a href="../Page/田口一.md" title="wikilink">田口一</a></p></td>
<td><p>東立出版社代理</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/失落的碎片.md" title="wikilink">失落的碎片</a></p></td>
<td><p><a href="../Page/星家なこ.md" title="wikilink">星家なこ</a></p></td>
<td><p>尖端代理</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/七位連一.md" title="wikilink">七位連一</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第4回<br />
（2008年）</p></td>
<td><p>優秀獎</p></td>
<td><p><a href="../Page/我甜蜜的苦澀委內瑞拉.md" title="wikilink">我甜蜜的苦澀委內瑞拉</a></p></td>
<td><p><a href="../Page/森田季節.md" title="wikilink">森田季節</a></p></td>
<td><p>東立出版社代理</p></td>
</tr>
<tr class="even">
<td><p>審査員<br />
特別獎</p></td>
<td></td>
<td><p>悠レイ<br />
（<a href="../Page/石川ユウヤ.md" title="wikilink">石川ユウヤ</a>）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/我倆開始征服世界.md" title="wikilink">我倆開始征服世界</a></p></td>
<td><p>岡崎登<br />
（<a href="../Page/おかざき登.md" title="wikilink">おかざき登</a>）</p></td>
<td><p>東立出版社代理</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>佳作</p></td>
<td></td>
<td><p>磯葉哲<br />
（<a href="../Page/葉村哲.md" title="wikilink">葉村哲</a>）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/德魯伊來囉！.md" title="wikilink">德魯伊來囉！</a></p></td>
<td><p>志瑞祐麒<br />
（<a href="../Page/志瑞祐.md" title="wikilink">志瑞祐</a>）</p></td>
<td><p>東立出版社代理</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><br />
（<a href="../Page/九之契約書.md" title="wikilink">九之契約書</a>）</p></td>
<td><p>二階堂紘史<br />
（<a href="../Page/二階堂紘嗣.md" title="wikilink">二階堂紘嗣</a>）</p></td>
<td><p>東立出版社代理</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><br />
（<a href="../Page/戀愛超能力.md" title="wikilink">戀愛超能力</a>）</p></td>
<td><p>樋口モグラ<br />
（<a href="../Page/樋口司.md" title="wikilink">樋口司</a>）</p></td>
<td><p>東立出版社代理</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第5回<br />
（2009年）</p></td>
<td><p>最優秀獎</p></td>
<td><p><br />
（<a href="../Page/迷茫管家與膽怯的我.md" title="wikilink">迷茫管家與膽怯的我</a>）</p></td>
<td><p><a href="../Page/朝野始.md" title="wikilink">朝野始</a></p></td>
<td><p>尖端代理<br />
<a href="../Page/天聞角川.md" title="wikilink">天聞角川代理</a></p></td>
</tr>
<tr class="odd">
<td><p>優秀獎</p></td>
<td><p><a href="../Page/失禮了！我是垃圾桶妖怪.md" title="wikilink">失禮了！我是垃圾桶妖怪</a></p></td>
<td><p><a href="../Page/岩波零.md" title="wikilink">岩波零</a></p></td>
<td><p>東立出版社代理</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>審査員<br />
特別獎</p></td>
<td><p><br />
（<a href="../Page/極道彼女.md" title="wikilink">極道彼女</a>）</p></td>
<td><p><a href="../Page/三原みつき.md" title="wikilink">三原みつき</a></p></td>
<td><p>東立出版社代理</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>佳作</p></td>
<td><p><br />
（）</p></td>
<td><p><a href="../Page/斉藤真也_(作家).md" title="wikilink">斉藤真也</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>One-seventh Dragon Princess<br />
（）</p></td>
<td><p><a href="../Page/北元あきの.md" title="wikilink">北元あきの</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>不吸血的吸血鬼</p></td>
<td><p><a href="../Page/刈野ミカタ.md" title="wikilink">刈野ミカタ</a></p></td>
<td><p>東立出版社代理</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><br />
（）</p></td>
<td><p><a href="../Page/三門鉄狼.md" title="wikilink">三門鉄狼</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第6回<br />
（2010年）</p></td>
<td><p>最優秀獎</p></td>
<td><p><br />
（<a href="../Page/變態王子與不笑貓.md" title="wikilink">變態王子與不笑貓</a>）</p></td>
<td><p>天出だめ<br />
（<a href="../Page/相乐总.md" title="wikilink">相乐总</a>）</p></td>
<td><p>尖端代理<br />
天聞角川代理</p></td>
</tr>
<tr class="even">
<td><p>優秀獎</p></td>
<td><p>食神<br />
（）</p></td>
<td><p>やすだ柿<br />
（<a href="../Page/内田俊_(小説家).md" title="wikilink">内田俊</a>）[3]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>佳作</p></td>
<td><p><br />
（）</p></td>
<td><p>無一<br />
（<a href="../Page/伊上円.md" title="wikilink">伊上円</a>）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/冬木冬樹.md" title="wikilink">冬木冬樹</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><br />
（）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><br />
（<a href="../Page/月神來我家！.md" title="wikilink">月神來我家！</a>）</p></td>
<td><p><a href="../Page/後藤祐迅.md" title="wikilink">後藤祐迅</a></p></td>
<td><p>東立出版社代理<br />
天聞角川代理</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><br />
（<a href="../Page/美少女幽靈的戀愛效應.md" title="wikilink">美少女幽靈的戀愛效應</a>）</p></td>
<td><p><a href="../Page/壱日千次.md" title="wikilink">壱日千次</a></p></td>
<td><p>東立出版社代理</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第7回<br />
（2011年）</p></td>
<td><p>最優秀獎</p></td>
<td><p><a href="../Page/會飛的豬還是豬？.md" title="wikilink">會飛的豬還是豬？</a></p></td>
<td><p>猫飯美味し<br />
（<a href="../Page/涼木行.md" title="wikilink">涼木行</a>）</p></td>
<td><p>東立出版社代理</p></td>
</tr>
<tr class="odd">
<td><p>優秀獎</p></td>
<td><p><br />
（T、內褲、好運到）</p></td>
<td><p>木村大志<br />
（<a href="../Page/本村大志.md" title="wikilink">本村大志</a>）</p></td>
<td><p>尖端代理</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>審査員<br />
特別獎</p></td>
<td></td>
<td><p><a href="../Page/千羽カモメ.md" title="wikilink">千羽カモメ</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>佳作</p></td>
<td><p><a href="../Page/Over_Image_超異能遊戲.md" title="wikilink">Over Image 超異能遊戲</a><br />
</p></td>
<td><p>永藤<br />
（<a href="../Page/遊佐真弘.md" title="wikilink">遊佐真弘</a>）</p></td>
<td><p>東立出版社代理</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/你並不孤單！.md" title="wikilink">你並不孤單！</a><br />
</p></td>
<td><p>こいわいハム<br />
（<a href="../Page/小岩井蓮二.md" title="wikilink">小岩井蓮二</a>）</p></td>
<td><p>東立出版社代理</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第8回<br />
（2012年）</p></td>
<td><p>最優秀獎</p></td>
<td><p><br />
（<a href="../Page/白銀的救世機.md" title="wikilink">白銀的救世機</a>）</p></td>
<td><p>天埜冬景</p></td>
<td><p>尖端代理</p></td>
</tr>
<tr class="even">
<td><p>優秀獎</p></td>
<td><p><br />
（失敗禁止！美少女的祕密不側漏！）</p></td>
<td><p>真崎政宗</p></td>
<td><p>尖端代理</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>審査員<br />
特別獎</p></td>
<td><p><br />
</p></td>
<td><p>方玖舞文<br />
</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><br />
（<a href="../Page/沉睡魔女.md" title="wikilink">沉睡魔女 1 轉學少女的魔術戰略</a>）</p></td>
<td><p>真野真央<br />
</p></td>
<td><p>東立代理</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>[4]</p></td>
<td><p>緋奈川イド</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>佳作</p></td>
<td><p><br />
（瑠璃色的瞎扯淡日常）</p></td>
<td><p>伊達康</p></td>
<td><p>尖端代理</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><br />
（<a href="../Page/拜託，再給我五分鐘！.md" title="wikilink">拜託，再給我五分鐘！</a>）</p></td>
<td><p>境京亮<br />
</p></td>
<td><p>尖端代理</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><br />
（<a href="../Page/忘却の軍神と装甲戦姫.md" title="wikilink">忘却の軍神と装甲戦姫</a>）</p></td>
<td><p>ノベロイド二等兵<br />
（<a href="../Page/鏡銀鉢.md" title="wikilink">鏡銀鉢</a>）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>中島三四郎<br />
（<a href="../Page/弥生志郎.md" title="wikilink">弥生志郎</a>）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><br />
（<a href="../Page/舞風的鎧姬.md" title="wikilink">舞風的鎧姬</a>）</p></td>
<td><p>肉Q<br />
（<a href="../Page/小山タケル.md" title="wikilink">小山タケル</a>）</p></td>
<td><p>東立出版社代理</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><br />
（<a href="../Page/森羅万象統御者.md" title="wikilink">森羅万象統御者</a>）</p></td>
<td><p>水月紗鳥<br />
</p></td>
<td><p>青文出版社代理</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第9回<br />
（2013年）</p></td>
<td><p>最優秀獎</p></td>
<td><p>人間と魔物がいる世界<br />
（<a href="../Page/MONSTER_DAYS.md" title="wikilink">MONSTER DAYS</a>）</p></td>
<td><p>そぼろそぼろ<br />
（<a href="../Page/扇友太.md" title="wikilink">扇友太</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>優秀獎</p></td>
<td><p>パンツ・ミーツ・ガール<br />
（<a href="../Page/ストライプ・ザ・パンツァー.md" title="wikilink">ストライプ・ザ・パンツァー</a>）</p></td>
<td><p><a href="../Page/為三.md" title="wikilink">為三</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>審査員<br />
特別獎</p></td>
<td><p><a href="../Page/穢れ聖者のエク・セ・レスタ.md" title="wikilink">穢れ聖者のエク・セ・レスタ</a></p></td>
<td><p><a href="../Page/新見聖.md" title="wikilink">新見聖</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>佳作</p></td>
<td><p>サイコロの裏<br />
（<a href="../Page/ダイス・マカブル_～definitional_random_field～.md" title="wikilink">ダイス・マカブル　～definitional random field～</a>）</p></td>
<td><p><a href="../Page/草木野鎖.md" title="wikilink">草木野鎖</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>シンクロ・インフィニティ――Synchro ∞――<br />
（<a href="../Page/仮想領域のエリュシオン.md" title="wikilink">仮想領域のエリュシオン</a>）</p></td>
<td><p><a href="../Page/上智一麻.md" title="wikilink">上智一麻</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/猫耳天使と恋するリンゴ.md" title="wikilink">猫耳天使と恋するリンゴ</a></p></td>
<td><p><a href="../Page/花間燈.md" title="wikilink">花間燈</a><br />
</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第10回<br />
（<a href="../Page/2014年.md" title="wikilink">2014年</a>）</p></td>
<td><p>最優秀獎</p></td>
<td><p><a href="../Page/Yの紋章師.md" title="wikilink">Yの紋章師</a></p></td>
<td><p><a href="../Page/越智文比古.md" title="wikilink">越智文比古</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>優秀獎</p></td>
<td><p>阿頼耶識冥清の非日常<br />
（<a href="../Page/逃奏劇リアクターズ.md" title="wikilink">逃奏劇リアクターズ</a>）</p></td>
<td><p><a href="../Page/塀流通留.md" title="wikilink">塀流通留</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>審査員<br />
特別獎</p></td>
<td><p>救われる世界と生贄少女の変なカミサマ 〜でも、願いはちゃんと叶えてくれます〜<br />
（<a href="../Page/イケニエハッピートリガー.md" title="wikilink">イケニエハッピートリガー</a>）</p></td>
<td><p><a href="../Page/未味なり太.md" title="wikilink">未味なり太</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>佳作</p></td>
<td><p>底辺かける高嶺の花<br />
（<a href="../Page/ひとりで生きるもん!.md" title="wikilink">ひとりで生きるもん!</a>）</p></td>
<td><p>鎌倉となり<br />
（<a href="../Page/暁雪.md" title="wikilink">暁雪</a>）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第11回<br />
（<a href="../Page/2015年.md" title="wikilink">2015年</a>）</p></td>
<td><p>最優秀獎</p></td>
<td><p>白盾騎士団・営業部<br />
（<a href="../Page/ギルド〈白き盾〉の夜明譚.md" title="wikilink">ギルド〈白き盾〉の夜明譚</a>）</p></td>
<td><p><a href="../Page/方波見咲.md" title="wikilink">方波見咲</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>優秀獎</p></td>
<td><p><a href="../Page/ざるそば（かわいい）.md" title="wikilink">ざるそば（かわいい）</a></p></td>
<td><p>つちせ八十五<br />
（<a href="../Page/つちせ八十八.md" title="wikilink">つちせ八十八</a>）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>審査員<br />
特別獎</p></td>
<td><p>『レストラン・フォリオーズ』の歪な業務記録<br />
（<a href="../Page/俺と魔物の異世界レストラン.md" title="wikilink">俺と魔物の異世界レストラン</a>）</p></td>
<td><p><a href="../Page/落合祐輔.md" title="wikilink">落合祐輔</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>佳作</p></td>
<td><p>球形世界の定戯式士<br />
（<a href="../Page/天牢都市〈セフィロト〉.md" title="wikilink">天牢都市〈セフィロト〉</a>）</p></td>
<td><p>秋月コウ<br />
（<a href="../Page/秋月煌介.md" title="wikilink">秋月煌介</a>）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>ダメ魔騎士の英雄道<br />
（<a href="../Page/ダメ魔騎士の英雄煌路.md" title="wikilink">ダメ魔騎士の英雄煌路</a>）</p></td>
<td><p>和白<br />
（<a href="../Page/藤木わしろ.md" title="wikilink">藤木わしろ</a>）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第12回<br />
（<a href="../Page/2016年.md" title="wikilink">2016年</a>）</p></td>
<td><p>最優秀獎</p></td>
<td><p>テクノマギア・モンスターズ!<br />
（<a href="../Page/境域のアルスマグナ.md" title="wikilink">境域のアルスマグナ 緋の龍王と恋する蛇女神</a>）</p></td>
<td><p>えどたろう<br />
（<a href="../Page/絵戸太郎.md" title="wikilink">絵戸太郎</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>優秀獎</p></td>
<td><p><br />
（）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>佳作</p></td>
<td><p><br />
（）</p></td>
<td><p><br />
（）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第13回<br />
（<a href="../Page/2017年.md" title="wikilink">2017年</a>）</p></td>
<td><p>最優秀獎</p></td>
<td><p>（<a href="../Page/我的快轉戀愛喜劇.md" title="wikilink">我的快轉戀愛喜劇</a>）</p></td>
<td><p><br />
（<a href="../Page/樫本燕.md" title="wikilink">樫本燕</a>）</p></td>
<td><p><a href="../Page/台灣角川.md" title="wikilink">台灣角川代理</a></p></td>
</tr>
<tr class="odd">
<td><p>優秀獎</p></td>
<td><p><br />
（<a href="../Page/戀愛必勝女神！.md" title="wikilink">戀愛必勝女神！</a>）</p></td>
<td><p>銀乃英介<br />
（）</p></td>
<td><p>台灣角川代理</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>審査員<br />
特別獎</p></td>
<td><p><br />
（<a href="../Page/廢柴勇者下剋上.md" title="wikilink">廢柴勇者下剋上</a>）</p></td>
<td></td>
<td><p>台灣角川代理</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><br />
（<a href="../Page/口是心非的冰室同學_從好感度100％開始的毒舌女子追求法.md" title="wikilink">口是心非的冰室同學 從好感度100％開始的毒舌女子追求法</a>）</p></td>
<td><p><br />
（）</p></td>
<td><p>台灣角川代理</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>佳作</p></td>
<td><p><br />
（<a href="../Page/老師的新娘是16歲的合法蘿莉？.md" title="wikilink">老師的新娘是16歲的合法蘿莉？</a>）</p></td>
<td><p>青空雲<br />
（<a href="../Page/さくらいたろう.md" title="wikilink">さくらいたろう</a>）</p></td>
<td><p>台灣角川代理</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><br />
（<a href="../Page/交叉連結——與電腦神姬春風的互換身體完全遊戲攻略.md" title="wikilink">交叉連結——與電腦神姬春風的互換身體完全遊戲攻略</a>）</p></td>
<td><p><a href="../Page/久追遥希.md" title="wikilink">久追遥希</a></p></td>
<td><p>台灣角川代理</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第14回<br />
（<a href="../Page/2018年.md" title="wikilink">2018年</a>）</p></td>
<td><p>最優秀獎</p></td>
<td></td>
<td><p>林若葉</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>優秀獎</p></td>
<td></td>
<td><p>春楡遥</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>審査員<br />
特別獎</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>佳作</p></td>
<td></td>
<td><p>hiro</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 關聯條目

  - [MF文庫J](../Page/MF文庫J.md "wikilink")

## 腳註

## 外部連結

  - [MF文庫J輕小說新人獎](http://www.mediafactory.co.jp/bunkoj/rookie/index)

[Category:MEDIA FACTORY](../Category/MEDIA_FACTORY.md "wikilink")
[Category:日本輕小說文學獎](../Category/日本輕小說文學獎.md "wikilink")
[\*](../Category/MF文庫J.md "wikilink")
[Category:角川集團主辦的獎項](../Category/角川集團主辦的獎項.md "wikilink")

1.  以2004年7月，新人賞創設以前投稿到編輯部的作品為對象。
2.  本屆編輯長特別獎因評審期間日日日以《[在遙遠彼方的小千](../Page/在遙遠彼方的小千.md "wikilink")》（新風舍）出道，不合參賽條件（僅限未在他社出道的新人）而特別設置。
3.  與第1回[史克威爾艾尼克斯輕小說大賞](../Page/史克威爾艾尼克斯輕小說大賞.md "wikilink")（入選）・第2回[MEGAMI小說大賞](../Page/MEGAMI小說大賞.md "wikilink")（金賞）同時獲獎，出版順序為本作最先。
4.  [作者提交了謝絕出版申請。](http://www.mediafactory.co.jp/bunkoj/rookie/ann08)