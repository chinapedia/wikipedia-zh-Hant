[DCB_Shooting_MG42_Roller_system.JPG](https://zh.wikipedia.org/wiki/File:DCB_Shooting_MG42_Roller_system.JPG "fig:DCB_Shooting_MG42_Roller_system.JPG")
**MG3**是[德國](../Page/德國.md "wikilink")[萊茵金屬所生產的](../Page/萊茵金屬.md "wikilink")[彈鏈供彈](../Page/彈鏈.md "wikilink")[通用機槍](../Page/通用機槍.md "wikilink")，以[二戰德國的](../Page/二戰.md "wikilink")[7.92×57毫米](../Page/7.92×57mm毛瑟.md "wikilink")[MG42通用機槍改為](../Page/MG42通用機槍.md "wikilink")[北約](../Page/北約.md "wikilink")[7.62×51毫米](../Page/7.62×51mm_NATO.md "wikilink")[口徑而成](../Page/口徑.md "wikilink")。

## 歷史

最早期的MG3是按[德國聯邦國防軍的要求](../Page/德國聯邦國防軍.md "wikilink")，由萊茵金屬在1958年以二戰中德國的[MG42為藍本](../Page/MG42通用機槍.md "wikilink")，改為[7.62×51毫米北約口徑作生產的版本](../Page/7.62×51mm_NATO.md "wikilink")，名為MG1，其後再將瞄準具修改以合乎7.62×51毫米北約[子彈的彈道及改用鍍](../Page/子彈.md "wikilink")[鉻槍管](../Page/鉻.md "wikilink")，命名為MG1A1（又名MG42/58）。MG1A1的改良版本為1959年的MG1A2（MG42/59），主要改為較重的擊鎚（950克，原為550克）、加入新式環形緩衝器以對應美國的[M13彈鏈及DM](../Page/M13彈鏈.md "wikilink")1彈鏈。再後來的又加入了槍口制退裝置、改良[兩腳架及擊鎚](../Page/兩腳架.md "wikilink")，命名為MG1A3。而以沿用的MG42直接改裝成7.62×51毫米北約的版本名為MG2。

至1968年，MG3正式進行生產，相比MG1A3，MG3改良了供彈系統的運作，使其可同時使用當時美軍制式的可分離式金屬彈鏈M13與德軍制式的連結式金屬彈鏈DM11，亦加入了防空用的瞄準照門，部份零件仍可與原MG42互換。MG3在超過30個國家使用，合法授權生產的包括[意大利](../Page/意大利.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")（MG42/59）、[巴基斯坦](../Page/巴基斯坦.md "wikilink")（MG1A3）、[希臘](../Page/希臘.md "wikilink")、[伊朗](../Page/伊朗.md "wikilink")、[蘇丹及](../Page/蘇丹.md "wikilink")[土耳其](../Page/土耳其.md "wikilink")。

## 設計

MG3以鋼板壓制方式生產，採用[後座力槍管後退式](../Page/後座力槍管後退式.md "wikilink")（管退式）作用運作，內有一對滾軸的滾軸式閉鎖槍機系統\[1\]，這種設計令槍管在發射時會不斷水平來回移動，當槍管移至[機匣內部到盡時](../Page/機匣.md "wikilink")，閉鎖會開啟，在MG3的槍管進行連續射擊時，這個過程會在槍管護套內不斷地快速重覆，此系統屬於一種全閉鎖系統，而槍管亦會溢出射擊時的瓦斯，並在槍口四周呈星形噴出，在夜間容易產生巨大的射擊火焰。MG3-{}-只能全自動發射，當開啟保險制時擊鎚會鎖定，無法釋放。

MG3的供彈位置在[機匣左面](../Page/機匣.md "wikilink")，以DM1不可散式彈鏈、M13或DM6可散式彈鏈供彈，而在[輕機槍模式時](../Page/輕機槍.md "wikilink")，會將100發彈鏈裝進彈鼓內並裝在機匣左面。安裝[彈鏈時須打開頂部機匣蓋](../Page/彈鏈.md "wikilink")，再將彈鏈前端的引導帶放進機匣內，再由退殼口拉出並把首發子彈對正位置，關閉機匣蓋再拉動才可完成整個步驟。MG3採用可快速更換的鍍鉻槍管，[膛線為](../Page/膛線.md "wikilink")4條右旋、纏距為305毫米，更換槍管須打開護木（）右面近機匣位置的護木蓋，如果在射擊後更換槍管則需要戴上專用的耐熱手套。而當槍管裝回護木時對正原有位置，因此它的槍口裝置獨立於槍管而位於護木末端，並有消焰制退及後座緩衝功能。

MG3的[槍托以聚合物料制造](../Page/槍托.md "wikilink")，護木下方裝有[兩腳架及採用射程可調的開放式照門](../Page/兩腳架.md "wikilink")（200米至1,200米），[機匣頂部亦有一個防空用的照門](../Page/機匣.md "wikilink")。當加裝三腳架（德語名為）作陣地固定式機槍時，會加裝一個機槍用望遠式[瞄準鏡作長程瞄準用途](../Page/瞄準鏡.md "wikilink")。

## 衍生型

[MG3.jpg](https://zh.wikipedia.org/wiki/File:MG3.jpg "fig:MG3.jpg")
[Schnittbild.MG3.jpg](https://zh.wikipedia.org/wiki/File:Schnittbild.MG3.jpg "fig:Schnittbild.MG3.jpg")
[Leopard_2A5.jpg](https://zh.wikipedia.org/wiki/File:Leopard_2A5.jpg "fig:Leopard_2A5.jpg")坦克上的MG3\]\]
[Dingo_2.jpg](https://zh.wikipedia.org/wiki/File:Dingo_2.jpg "fig:Dingo_2.jpg")
[MG3_Tripod.JPEG](https://zh.wikipedia.org/wiki/File:MG3_Tripod.JPEG "fig:MG3_Tripod.JPEG")

  - **MG1**：萊茵金屬生產的7.62×51 NATO口徑型MG42
  - **MG1A1（MG42/58）**：與MG1相似，但瞄準具修改以對應7.62×51 NATO子彈的彈道及改用鍍鉻槍管
  - **MG1A2（MG42/59）**：MG1A1改良型，加大退殼口、改用較重的擊鎚及加入新式環形緩衝器
  - **MG1A3**：MG1A2改良型，加入了槍口制退裝置、改良兩腳架及擊鎚
  - **MG1A4**：MG1改良型，為車用固定版本
  - **MG1A5**：MG1A3改良型，以MG1A3改為MG1A4的版本
  - **MG2**：以二戰時的MG42直接修改成7.62 NATO口徑
  - **MG3**：MG1A3改良型，正式生產版本，加入防空用照門
  - **MG3E**：MG3減重改良型（約減輕了1.3公斤），亦有參與1970年代後期[北約輕武器試驗](../Page/北約.md "wikilink")
  - **MG3A1**：MG3改良型，為車用固定版本
  - **MG14z**:

\[2\]\[3\].

  - **MG3 KWS**: 於2014年,HK MG5全面取代MG3時,所推出的戰術套件

## 現役

[MG3_Training.JPEG](https://zh.wikipedia.org/wiki/File:MG3_Training.JPEG "fig:MG3_Training.JPEG")
[MG74_auf_Dreibein.jpg](https://zh.wikipedia.org/wiki/File:MG74_auf_Dreibein.jpg "fig:MG74_auf_Dreibein.jpg")
[Norwegian_soldier_-_Battle_Griffin_2005.jpg](https://zh.wikipedia.org/wiki/File:Norwegian_soldier_-_Battle_Griffin_2005.jpg "fig:Norwegian_soldier_-_Battle_Griffin_2005.jpg")的MG3\]\]
MG3至今仍然是現代德國部隊[裝甲戰鬥車輛及其他軍用車輛的主要副武器](../Page/裝甲戰鬥車輛.md "wikilink")，如[豹2型坦克](../Page/豹2型坦克.md "wikilink")、[PzH
2000](../Page/PzH_2000.md "wikilink")、Marder步兵戰-{}-車、[ATF
Dingo及](../Page/澳洲犬ATF.md "wikilink")[LKW
2to等](../Page/LKW_2to.md "wikilink")，亦是步兵的班／排用機槍。然而在2013年德軍已決定以MG5取代MG3。

### 使用國

[北約](../Page/北約.md "wikilink")：

  - [阿爾巴尼亞](../Page/阿爾巴尼亞.md "wikilink")\[4\]

  - [加拿大](../Page/加拿大.md "wikilink")

  - [克羅地亞](../Page/克羅地亞.md "wikilink")

  - [丹麥](../Page/丹麥.md "wikilink") -
    即將被[M60E6所取代](../Page/M60通用機槍.md "wikilink")

  - [愛沙尼亞](../Page/愛沙尼亞.md "wikilink")

  - [法國](../Page/法國.md "wikilink")

  - [德國](../Page/德國.md "wikilink") -
    將被[MG5](../Page/HK_MG5通用機槍.md "wikilink")（HK121）所取代

  - [希臘](../Page/希臘.md "wikilink")－由EAS合法授權生產

  - [冰島](../Page/冰島.md "wikilink")

  - [意大利](../Page/意大利.md "wikilink")－由[貝瑞塔合法授權生產](../Page/貝瑞塔.md "wikilink")（MG42/59），部件由Motofides-Whitehead及Franchi制造

  - [拉脫維亞](../Page/拉脫維亞.md "wikilink")

  - [立陶宛](../Page/立陶宛.md "wikilink")\[5\]

  - [挪威](../Page/挪威.md "wikilink")

  - [波蘭](../Page/波蘭.md "wikilink")－用於豹2型坦克

  - [葡萄牙](../Page/葡萄牙.md "wikilink")－命名為M960

  - [斯洛文尼亞](../Page/斯洛文尼亞.md "wikilink")

  - [西班牙](../Page/西班牙.md "wikilink")－由通用動力聖塔芭芭拉分公司（Santa Bárbara
    Sistemas）合法授權生產

  - [土耳其](../Page/土耳其.md "wikilink")－1974年起由MKEK合法授權生產

非北約：

  - [阿根廷](../Page/阿根廷.md "wikilink")

  - [澳大利亞](../Page/澳大利亞.md "wikilink")－用於豹1型坦克

  - [奧地利](../Page/奧地利.md "wikilink")－本土生產，命名為**[MG74](../Page/MG74.md "wikilink")**

  - [阿塞拜疆](../Page/阿塞拜疆.md "wikilink") - 採用土耳其仿製版本

  - [孟加拉](../Page/孟加拉.md "wikilink")－[孟加拉国步枪队採用](../Page/孟加拉国步枪队.md "wikilink")

  - [波斯尼亞和黑塞哥維那](../Page/波斯尼亞和黑塞哥維那.md "wikilink")

  - [巴西](../Page/巴西.md "wikilink")－用於豹1型坦克

  - [佛得角](../Page/佛得角.md "wikilink")

  - [智利](../Page/智利.md "wikilink")

  - [哥倫比亞](../Page/哥倫比亞.md "wikilink")

  - [塞普勒斯](../Page/塞普勒斯.md "wikilink")

  - [芬蘭](../Page/芬蘭.md "wikilink")－命名為KK MG3，用於豹2型坦克及NH90直升機

  - [加納](../Page/加納.md "wikilink")

  - [伊朗](../Page/伊朗.md "wikilink")－由伊朗國防工業組織（DIO）合法授權生產，命名為MGA3

  -
  - [利比亞](../Page/利比亞.md "wikilink")

  - [馬其頓](../Page/馬其頓.md "wikilink")

  - [馬爾他](../Page/馬爾他.md "wikilink")－命名為MG42/59

  - [墨西哥](../Page/墨西哥.md "wikilink")－由SEDENA合法授權生產

  - [摩洛哥](../Page/摩洛哥.md "wikilink")－使用[奧地利製](../Page/奧地利.md "wikilink")**MG74**

  - [緬甸](../Page/緬甸.md "wikilink")－由Ka Pa Sa合法授權生產，命名為MA15

  - [北塞浦路斯](../Page/北塞浦路斯.md "wikilink")

  - [巴基斯坦](../Page/巴基斯坦.md "wikilink")－由巴基斯坦軍械廠（POF）合法授權生產

  - [聖多美和普林西比](../Page/聖多美和普林西比.md "wikilink")

  - [沙烏地阿拉伯](../Page/沙烏地阿拉伯.md "wikilink")

  - [塞爾維亞](../Page/塞爾維亞.md "wikilink")

  - [新加坡](../Page/新加坡.md "wikilink")

  - [蘇丹](../Page/蘇丹.md "wikilink")－由Military Industry
    Corporation合法授權生產，命名為Karar

  - [瑞典](../Page/瑞典.md "wikilink")－名為Ksp 94，用於豹2型坦克上

  - [瑞士](../Page/瑞士.md "wikilink")－本土生產，命名為MG-710

  - [泰國](../Page/泰國.md "wikilink")－用於[V-150裝甲運兵車](../Page/V-150.md "wikilink")

  - [多哥](../Page/多哥.md "wikilink")

  - [烏拉圭](../Page/烏拉圭.md "wikilink")

## 相關條目

  - [MG34](../Page/MG34通用機槍.md "wikilink")－MG42的前身
  - [MG42](../Page/MG42通用機槍.md "wikilink")
  - [M53](../Page/Zastava_M53通用機槍.md "wikilink")－二戰後由[南斯拉夫仿製的MG](../Page/南斯拉夫.md "wikilink")42，算是MG3的近親
  - [MG4](../Page/HK_MG4輕機槍.md "wikilink")（MG43）
  - [MG5](../Page/HK_MG5通用機槍.md "wikilink")（HK121）
  - [M60通用機槍](../Page/M60通用機槍.md "wikilink")－美軍於1960年代至1990年代的通用機槍
  - [FN MAG](../Page/FN_MAG通用機槍.md "wikilink")－[Fabrique
    Nationale生產的通用機槍](../Page/Fabrique_Nationale.md "wikilink")
  - [M240通用機槍](../Page/M240通用機槍.md "wikilink")－美軍於1990年代至現在的通用機槍，FN
    MAG的改良型
  - [PK通用機槍](../Page/PK通用機槍.md "wikilink")－[蘇聯及](../Page/蘇聯.md "wikilink")[俄羅斯的通用機槍](../Page/俄羅斯.md "wikilink")
  - [SIG MG
    710-3通用機槍](../Page/SIG_MG_710-3通用機槍.md "wikilink")－[瑞士的MG](../Page/瑞士.md "wikilink")42改裝版本，發射7.62
    NATO口徑

## 参考來源及文献

<div class="references-small">

<references />

  -
  -

</div>

## 外部链接

  - [MG3的分解圖片](http://www.bimbel.de/artikel/artikel-14.html)
  - [Modern
    Firearms－MG42及MG3](https://web.archive.org/web/20060409195838/http://world.guns.ru/machine/mg33-e.htm)
  - [三腳架上的圖片](http://www.bimbel.de/artikel/artikel-26.html)
  - [D boy gun
    world-MG3通用机枪](http://firearmsworld.net/german/mg/mg3/mg3.htm)

[Category:通用機槍](../Category/通用機槍.md "wikilink")
[Category:7.62×51毫米槍械](../Category/7.62×51毫米槍械.md "wikilink")
[Category:德國槍械](../Category/德國槍械.md "wikilink")

1.  [Modern Firearms -
    MG42及MG3](http://world.guns.ru/machine/mg33-e.htm)
2.  <http://www.all4shooters.com/en/specials/Trade-shows-2014/IWA-2014-new-products/enforcetac/Tactics-Group-MG14Z/>
3.  <http://topwar.ru/42882-tactics-group-mg-14z.html>
4.  <http://www.armyrecognition.com/almex_2011_daily_news_albania_defence_exhibition/the_45_albanian_army_special_forces_face-to-face_with_the_taliban_s_in_kandahar_afghanistan.html>
5.