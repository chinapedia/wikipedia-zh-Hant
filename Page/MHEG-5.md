**MHEG-5**（信息技术-多媒体和超媒体信息编码 第5部分，
[ISO](../Page/國際標準化組織.md "wikilink")／[IEC](../Page/国际电工委员会.md "wikilink")
IS
13522-5:1997），是展現[多媒體和](../Page/多媒體.md "wikilink")[超媒體內容的國際技術標準](../Page/超媒體.md "wikilink")，由「多媒體超媒體標準專家組織」（Multimedia
and Hypermedia Experts Group，
MHEG）制訂，最常用於提供[互動電視服務](../Page/互動電視.md "wikilink")。MHEG-5與另一個[互動電視技術](../Page/互動電視.md "wikilink")[MHP的分別](../Page/MHP.md "wikilink")，在於MHEG-5所需的[CPU的](../Page/中央處理器.md "wikilink")[MIPS數值和](../Page/MIPS.md "wikilink")[記憶體遠比MHP為少](../Page/記憶體.md "wikilink")，[中間件和](../Page/中間件.md "wikilink")[機頂盒的成本亦較MHP便宜](../Page/數位視訊轉換盒.md "wikilink")，而且完全不需要支付授權費。\[1\]

MHEG-5在[英國和](../Page/英國.md "wikilink")[新西蘭的免費電視台和香港的](../Page/新西蘭.md "wikilink")[無綫電視皆有選用](../Page/無綫電視.md "wikilink")\[2\]，而[台灣](../Page/台灣.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[印度](../Page/印度.md "wikilink")、[土耳其](../Page/土耳其.md "wikilink")、[愛爾蘭和](../Page/愛爾蘭.md "wikilink")[俄羅斯等地亦正在對MHEG](../Page/俄羅斯.md "wikilink")-5技術進行測試和評估。\[3\]

## 參見

  - [MHP](../Page/多媒体家庭服务平台.md "wikilink")，另一個在歐洲使用的互動電視技術。
  - [Teletext](../Page/Teletext.md "wikilink")：相關服務之前身

## 注釋

## 外部連結

  - [MHEG 官方網頁](http://www.mheg.org)
  - [IMPALA (International MHEG Promotion
    Alliance)](https://web.archive.org/web/20150801161946/http://impala.org/)
  - [Hello World in
    MHEG-5](http://www.digvid.info/mheg5/hello_world.php)
  - [ASN.1 encoder/decoder + source
    code](http://lionet.info/asn1c/download.html)
  - [Open Source MHEG-5 engine for
    Linux](http://redbutton.sourceforge.net/)
  - [CREATION DTTR-2008PLUS , DTTR-1338N 已支援MHEG-5
    互動功能](https://web.archive.org/web/20070320214115/http://www.creation-digital.jp/)

[Category:ISO標準](../Category/ISO標準.md "wikilink")
[Category:影像科技](../Category/影像科技.md "wikilink")
[Category:電腦術語](../Category/電腦術語.md "wikilink")
[Category:影片和電影技術](../Category/影片和電影技術.md "wikilink")
[Category:电视](../Category/电视.md "wikilink")

1.  [授權費全免　MHEG-5力爭互動電視市場](http://www.dtvc.org.tw/Detail.asp?num=387)
2.  [要享受TVB互動功能服務
    MHEG-5中間件為必然之選](http://www.tvb.com/affairs/faq/press/20071128.html)
3.  [授權費全免　MHEG-5力爭互動電視市場](http://www.dtvc.org.tw/Detail.asp?num=387)