**MIM**或**M.I.M.**可能指：

  - 教育

<!-- end list -->

  - [管理硕士](../Page/管理硕士.md "wikilink")，原文，[縮寫MiM或MM](../Page/縮寫.md "wikilink")。

<!-- end list -->

  - 政治

<!-- end list -->

  - [馬提尼克獨立運動](../Page/馬提尼克獨立運動.md "wikilink")，原文，[縮寫MIM](../Page/縮寫.md "wikilink")。
  - [毛主义国际运动](../Page/毛主义国际运动.md "wikilink")，原文，[縮寫MIM](../Page/縮寫.md "wikilink")，源自美國。

<!-- end list -->

  - 遺傳學

<!-- end list -->

  - [人類孟德爾遺傳學](../Page/人類孟德爾遺傳學.md "wikilink")，原文，[縮寫MIM](../Page/縮寫.md "wikilink")。

<!-- end list -->

  - 治金

<!-- end list -->

  - [金屬射出成型](../Page/金屬射出成型.md "wikilink")，原文，[縮寫MIM](../Page/縮寫.md "wikilink")。

<!-- end list -->

  - 武器 -{H|zh-hans:导弹; zh-tw:飛彈;}-

<!-- end list -->

  - [MIM-23鷹式飛彈](../Page/MIM-23鷹式飛彈.md "wikilink")
  - [MIM-72/M48欉樹飛彈](../Page/MIM-72/M48欉樹飛彈.md "wikilink")
  - [勝利女神飛彈系列](../Page/勝利女神飛彈.md "wikilink")，第1代飛彈、第2代飛彈
  - MIM-104[愛國者飛彈](../Page/愛國者飛彈.md "wikilink")
  - MIM-7陸基麻雀飛彈，[AIM-7麻雀飛彈陸基型](../Page/AIM-7麻雀飛彈.md "wikilink")

<!-- end list -->

  - 虛擬人物

<!-- end list -->

  - [石中劍 (電影)劇中人物](../Page/石中劍_\(電影\).md "wikilink")「蠻夫人」，原文。
  - [密米爾](../Page/密米爾.md "wikilink")，北歐神話中的智慧巨人，原文。
  - 在托爾金（J.R.R.
    Tolkien）小說的中土大陸裡，[密姆](../Page/密姆.md "wikilink")（）是最後一位小矮人（Petty-dwarves）。

<!-- end list -->

  - 學術術語

<!-- end list -->

  - [多指令流多数据流](../Page/多指令流多数据流.md "wikilink")，原文，縮寫MIMD。
  - [多用途互聯網郵件擴展](../Page/多用途互聯網郵件擴展.md "wikilink")，原文，縮寫MIME。
  - 多輸入多輸出，原文，[縮寫](../Page/縮寫.md "wikilink")[MIMO](../Page/MIMO.md "wikilink")。

<!-- end list -->

  - 其他

<!-- end list -->

  -
  - [景順MIM日本增長基金](../Page/景順MIM日本增長基金.md "wikilink")