**MIT授權條款**（）是許多[軟體](../Page/軟體.md "wikilink")[授權條款中](../Page/授權_\(法律\).md "wikilink")，被廣泛使用的其中一種。與其他常見的軟體授權條款（如[GPL](../Page/GPL.md "wikilink")、[LGPL](../Page/LGPL.md "wikilink")、[BSD](../Page/BSD_license.md "wikilink")）相比，MIT是相對寬鬆的軟體授權條款。

## 簡介

MIT授權條款之名源自麻省理工學院（Massachusetts Institute of Technology,
MIT），又稱「X授權條款」（X License）或「X11授權條款」（X11 License）

MIT內容與三條款[BSD授權條款](../Page/BSD授權條款.md "wikilink")（3-clause BSD
license）內容頗為近似，但是賦予軟體被授權人更大的權利與更少的限制。

## 運用情形

有許多團體均採用MIT許可證。例如著名的[SSH連線軟體](../Page/Secure_Shell.md "wikilink")[PuTTY與](../Page/PuTTY.md "wikilink")[X視窗系統即為例子](../Page/X視窗系統.md "wikilink")。、[Mono開發平台函式庫](../Page/Mono.md "wikilink")、[Ruby
on
Rails](../Page/Ruby_on_Rails.md "wikilink")、[Lua等等也都採用MIT授權條款](../Page/Lua.md "wikilink")。

## 條款內容

普遍的MIT协议声明原文如下： \[1\]

中文解释如下：

### 被授權人權利

被授權人有權利使用、複製、修改、合併、出版發行、散布、再授權和/或販售軟體及軟體的副本，及授予被供應人同等權利，惟服從以下義務。

### 被授權人義務

在軟體和軟體的所有副本中都必須包含以上版權聲明和本許可聲明。

### 其他重要特性

此授權條款並非屬[copyleft的自由軟體授權條款](../Page/copyleft.md "wikilink")，允許在[自由及開放原始碼軟體或非自由軟體](../Page/自由及開放原始碼軟體.md "wikilink")（proprietary
software）所使用。

MIT的內容可依照程式著作權者的需求更改內容。此亦為MIT與[BSD](../Page/BSD_license.md "wikilink")（The
BSD license, 3-clause BSD license）本質上不同處。

MIT授權條款可與其他授權條款並存。另外，MIT條款也是自由軟體基金會（FSF）所認可的自由軟體授權條款，與[GPL相容](../Page/GPL.md "wikilink")。

## 参考文献

## 外部链接

  - [MIT协议的模板](http://opensource.org/licenses/MIT)

{{-}}

[Category:自由軟體授權](../Category/自由軟體授權.md "wikilink") [Category:X
Window系统](../Category/X_Window系统.md "wikilink")

1.