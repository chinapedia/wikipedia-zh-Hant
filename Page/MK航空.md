**MK航空**（）是[迦納一家已結業的航空公司](../Page/迦納.md "wikilink")，營運於1990年至2010年間，提供來往[非洲的貨運服務](../Page/非洲.md "wikilink")。名字中的“MK”是其创始人迈克尔·克鲁格（Michael
C.
Kruger）的姓名缩写。是其主要的[歐洲貨運航點包括](../Page/歐洲.md "wikilink")、[蓋威克機場](../Page/蓋威克機場.md "wikilink")、和[盧森堡-芬德爾國際機場](../Page/盧森堡-芬德爾國際機場.md "wikilink")\[1\]。該公司於2006年在[英國重新註冊](../Page/英國.md "wikilink")，並以[東薩塞克斯郡為新總部](../Page/東薩塞克斯郡.md "wikilink")\[2\]。

## 歷史

[mk.airlines.b747-200.9g-mkj.arp.jpg](https://zh.wikipedia.org/wiki/File:mk.airlines.b747-200.9g-mkj.arp.jpg "fig:mk.airlines.b747-200.9g-mkj.arp.jpg")客機（攝於2004年10月10日）。\]\]
[MK_Airlines_Douglas_DC-8-55(F)_Lebeda-2.jpg](https://zh.wikipedia.org/wiki/File:MK_Airlines_Douglas_DC-8-55\(F\)_Lebeda-2.jpg "fig:MK_Airlines_Douglas_DC-8-55(F)_Lebeda-2.jpg")起飛的一架MK航空[道格拉斯DC-8客機](../Page/道格拉斯DC-8.md "wikilink")（攝於2004年3月11日）。\]\]
MK航空由麥可·克魯格（）於1990年創辦，時名「」\[3\]（取自[本名](../Page/本名.md "wikilink")[首字母](../Page/首字母縮寫.md "wikilink")），註冊地點和總部均在[迦納](../Page/迦納.md "wikilink")，並隨後建立了來往於迦納[阿克拉](../Page/阿克拉.md "wikilink")[科托卡國際機場與](../Page/科托卡國際機場.md "wikilink")[英國](../Page/英國.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")[蓋威克機場之間的航線](../Page/蓋威克機場.md "wikilink")，採用[DC-8貨機來營運](../Page/DC-8.md "wikilink")\[4\]。1993年，公司改名為「」\[5\]。1995年至1996年間，公司總部改設[尼日利亞](../Page/尼日利亞.md "wikilink")\[6\]。1999年起，公司開始陸續購入[波音747-200貨機](../Page/波音747-200.md "wikilink")\[7\]。

2004年10月14日，一架在[哈利法克斯羅伯特·洛恩·斯坦菲爾德國際機場起飛前往](../Page/哈利法克斯羅伯特·洛恩·斯坦菲爾德國際機場.md "wikilink")[西班牙](../Page/西班牙.md "wikilink")[薩拉戈薩時](../Page/薩拉戈薩.md "wikilink")，客機無法起飛並衝出跑道，引致機身起火，最終無人生還。事後調查發現，該意外是由於機組人員使用錯誤的計算起飛推力所導致的\[8\]\[9\]。

2006年9月，MK航空取得[英國民航局發出的](../Page/英國民航局.md "wikilink")，2007年11月，宣佈計劃把公司更名為「英國全球航空」（）\[10\]，[航空代碼亦而改為](../Page/國際航空運輸協會航空公司代碼.md "wikilink")「」\[11\]，但在2008年3月，計劃被無限期擱置\[12\]。

由於財政問題，MK航空所有航班在2008年6月10日起終止，公司被接管。MK航空獲得貝爾弗爾斯集團（）注資，並可能最快在6月20日恢復營運。航空公司進行重組後，最快可能在6月24日脫離接管。\[13\]

在航空公司仍然面對沈重的經濟壓力下，終在2010年4月9日再度終止營運，並在當日放棄营运许可证\[14\]\[15\]\[16\]。之後航空公司以「岩漿航空」（）名義提供非航空的物流服務，直至年底才正式終止\[17\]。

## 機隊

[MK_Airlines_B747-200F.JPG](https://zh.wikipedia.org/wiki/File:MK_Airlines_B747-200F.JPG "fig:MK_Airlines_B747-200F.JPG")的一架MK航空波音747-200F客機。\]\]
[Boeing_747-2J6B(SF),_MK_Airlines_JP6158815.jpg](https://zh.wikipedia.org/wiki/File:Boeing_747-2J6B\(SF\),_MK_Airlines_JP6158815.jpg "fig:Boeing_747-2J6B(SF),_MK_Airlines_JP6158815.jpg")起飛（原屬於[中國國際航空](../Page/中國國際航空.md "wikilink")、尚未變更塗裝）的一架MK航空波音747-2J6B(SF)客機。\]\]
MK航空曾營運的機隊如下：

  - [波音727-200](../Page/波音727-200.md "wikilink")
  - [波音747-200F](../Page/波音747-200F.md "wikilink")
  - [道格拉斯DC-8](../Page/道格拉斯DC-8.md "wikilink")
  - [HS748](../Page/HS748.md "wikilink")

## 註釋

## 參考資料

[Category:英國已結業航空公司](../Category/英國已結業航空公司.md "wikilink")
[Category:迦納已結業航空公司](../Category/迦納已結業航空公司.md "wikilink")
[Category:1990年成立的航空公司](../Category/1990年成立的航空公司.md "wikilink")
[Category:2010年結業航空公司](../Category/2010年結業航空公司.md "wikilink")

1.

2.  [Information about MK Airlines at the Aero Transport Data
    Bank](http://aerotransport.org)

3.
4.
5.
6.
7.
8.  [Flight 1602 at the Aviation Safety
    Network](http://aviation-safety.net/database/record.php?id=20041014-0)

9.  [Flight 1602: Official TSB
    report](http://www.tsb.gc.ca/eng/rapports-reports/aviation/2004/a04h0004/a04h0004.asp)

10.
11. Civil Aviation Authority Consumer Protection Group Official Record
    Series 2 Number 1836, 12 February 2008 (ISSN 0306-4654)

12.
13.
14. [MK Airlines Forced from the
    Sky](http://www.aircargonews.net/News/MK-Airlines-forced-from-the-sky.aspx)
    09-Apr-2010

15. ["Grounded MK Airlines returns operating licences", at
    ifw-net.com](http://www.ifw-net.com/freightpubs/ifw/index/mk-airlines-to-suspend-operations-due-to-financial-problems/20017766407.htm;jsessionid=09BAFAEF173F949018E558584D471833.5d25bd3d240cca6cbbee6afc8c3b5655190f397fhttp:/www.google.be)

16.
17.