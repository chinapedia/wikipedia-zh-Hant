**MLDonkey**是一个[开源](../Page/开源软件.md "wikilink")[免费的多协议P](../Page/免费软件.md "wikilink")2P应用程序。起初它只是一个[Linux下的](../Page/Linux.md "wikilink")[eDonkey协议客户端](../Page/eDonkey网络.md "wikilink")，现在它支持多种点对点协议，并能在在各种不同风格的[类Unix系统](../Page/类Unix.md "wikilink")、[Mac
OS
X](../Page/Mac_OS_X.md "wikilink")、[Windows以及](../Page/Microsoft_Windows.md "wikilink")[MorphOS下运行](../Page/MorphOS.md "wikilink")。它使用[OCaml语言编写](../Page/OCaml.md "wikilink")，同时有些部分使用了一些[C语言以及](../Page/C语言.md "wikilink")[汇编语言的代码](../Page/汇编语言.md "wikilink")，从而保证了它的高效能。

## 历史

MLDonkey的开发始于2001年，最初的开发者是[INRIA的Fabrice](../Page/INRIA.md "wikilink")
Le Fessant。

## 功能

MLdonkey的核心功能包括：

  - [P2P](../Page/P2P.md "wikilink")[文件共享](../Page/文件共享.md "wikilink")，支持如下协议：
      - [EDonkey Network](../Page/EDonkey_Network.md "wikilink")

      - [Overnet](../Page/Overnet.md "wikilink")

      - [Kad Network](../Page/Kad_Network.md "wikilink")

      - [BitTorrent](../Page/BitTorrent.md "wikilink")

      - [Gnutella](../Page/Gnutella.md "wikilink")

      - [Gnutella2](../Page/Gnutella2.md "wikilink")

      - [FastTrack](../Page/FastTrack.md "wikilink")

      - [OpenNap](../Page/OpenNap.md "wikilink")（现不可用）

      - [SoulSeek](../Page/SoulSeek.md "wikilink")（现不可用）

      - （现不可用）

      - [HTTP](../Page/HTTP.md "wikilink")／[FTP](../Page/File_Transfer_Protocol.md "wikilink")
  - 以C/S结构设计，后台的下载核心可以通过内置的Telnet以及Web界面操纵，也可以使用各种图形前端程序。

## 参看

  - [eDonkey软件比较](../Page/eDonkey软件比较.md "wikilink")
  - [eDonkey网络](../Page/eDonkey网络.md "wikilink")
  - [檔案分享](../Page/檔案分享.md "wikilink")

## 图形用户前端

  - [KMldonkey](http://kmldonkey.org/)
  - [Sancho](http://sancho-gui.sourceforge.net/)
  - [Burro](http://burro.sf.net)

## 外部链接

  - [MLDonkey
    SourceForge项目页面](http://sourceforge.net/projects/mldonkey/)
  - [MLDonkey 主页](http://mldonkey.sourceforge.net/)

[Category:SourceForge专案](../Category/SourceForge专案.md "wikilink")
[Category:自由跨平台軟體](../Category/自由跨平台軟體.md "wikilink")
[Category:EDonkey客戶端](../Category/EDonkey客戶端.md "wikilink")
[Category:BitTorrent客戶端](../Category/BitTorrent客戶端.md "wikilink")
[Category:自由檔案分享軟體](../Category/自由檔案分享軟體.md "wikilink")