**MOS
6502**是1975年由[MOS科技所研發的](../Page/MOS科技.md "wikilink")[8位元](../Page/8位元.md "wikilink")[微處理器](../Page/微處理器.md "wikilink")。當年6502剛問世時是當時效能最強的8位元[CPU](../Page/CPU.md "wikilink")，且價格只有大型業者（如[Motorola](../Page/Motorola.md "wikilink")、[Intel](../Page/Intel.md "wikilink")）相近產品的六分之一甚至更低；且除了公司的[Z80外](../Page/Zilog_Z80.md "wikilink")，6502幾乎快過多數業者的相近產品，進而激起一系列的的電腦專案，並在之後的1980年代帶來一場[個人電腦的革命](../Page/個人電腦.md "wikilink")。MOS科技僅授權兩家業者能相容研製6502，即是所謂的“第二供貨源”，此分別是[洛克威爾國際公司](../Page/罗克韦尔.md "wikilink")（）與，更之後才有更多的業者獲得相容研製的授權，並仍持續在[嵌入式系統的市場中供貨](../Page/嵌入式系統.md "wikilink")。

[MOS_6502AD_4585_top.jpg](https://zh.wikipedia.org/wiki/File:MOS_6502AD_4585_top.jpg "fig:MOS_6502AD_4585_top.jpg")塑膠封裝的MOS
6502處理器。\]\]

## 以6502系列為CPU的設備

[Apple
II的采用使得](../Page/Apple_II.md "wikilink")[6502成了廣為人知的](../Page/6502.md "wikilink")[CPU](../Page/CPU.md "wikilink")，歐美地區發售的[Commodore系列](../Page/康懋达国际.md "wikilink")8位元電腦也大量使用了6502系列CPU。

俗稱「紅白機」的「[FC](../Page/FC游戏机.md "wikilink")」電視遊樂器(使用6502相容指令集的Ricoh
2A03)、[文曲星电子词典等也采用它](../Page/文曲星_\(电子词典\).md "wikilink")。后来的「[超級任天堂](../Page/超級任天堂.md "wikilink")」使用了（16位元版的6502）。

## 附註

1.  \-
    详见[家用電腦分類列表](../Page/家用電腦分類列表.md "wikilink")，該列表清楚顯示在市場上6502與Z80的對立、對峙態勢。

## 相關參見

  -
  - [家用電腦分類列表](../Page/家用電腦分類列表.md "wikilink")

## 參考依據

  - Bagnall, Brian (2005). [On the Edge: The Spectacular Rise and Fall
    of Commodore](http://www.commodorebook.com)。Variant Press. ISBN
    0-9738649-0-7.
  - Leventhal, Lance A. (1986). *6502 Assembly Language Programming 2nd
    Edition*. Osborne/McGraw-Hill. ISBN 0-07-881216-X.
  - Leventhal, Lance A. (1982). *6502 Assembly Language Subroutines*.
    Osborne/McGraw-Hill. ISBN 0-931988-59-4.
  - Mansfield, Richard (1983). *[Machine Language For
    Beginners.](http://www.atariarchives.org/mlb/) Personal Computer
    Machine Language Programming For The Atari, VIC, Apple, Commodore
    64, And PET/CBM Computers* (or, *Machine Language Programming For
    BASIC Language Programmers*). Greensboro, North Carolina: Compute\!
    Publications, Inc. Copyright © 1983, Small System Services, Inc.
    ISBN 0-942386-11-6.
  - Zaks, Rodnay (1983). *Programming the 6502 (Fourth Edition)*. Sybex,
    Inc. ISBN 0-89588-135-7.
  - Zaks, Rodnay (1982). *Advanced 6502 Programming*. Sybex, Inc. ISBN
    0-89588-089-X.

## 外部連結

  - [www.6502.org](http://www.6502.org/)

<!-- end list -->

  - [6502 images and description at
    cpu-collection.de](http://www.cpu-collection.de/?tn=1&l0=cl&l1=650x)
  - [List of 6502 software emulators](http://www.zophar.net/6502.html)
  - [6502 simulator for Windows](http://atarihq.com/danb/6502.shtml)
  - [Apple I Owners Club](http://www.applefritter.com/apple1)
  - [KIM-1 schematics](http://oldcomputers.net/kim1.html)
  - [Adding 64 user-defined opcodes to
    the 6502](http://www.wiz-worx.com/resume/byte8010.htm)
  - [6502 accelerator information
    (apple2history.org)](http://apple2history.org/history/ah12.html#09)
  - [6502 acceleration article
    (*C=Hacking*)](http://www.canberra.edu.au/~scott/C=Hacking/C-Hacking12/cmdcpu.html)
  - [History of 6502](http://www.pc-history.org/digital.htm) used by
    [The Digital Group](../Page/The_Digital_Group.md "wikilink")。
  - [DigiBarn Systems
    reference](http://www.digibarn.com/collections/systems/digitalgroup/)
    to [The Digital Group](../Page/The_Digital_Group.md "wikilink")
    Systems.

[Category:微處理器](../Category/微處理器.md "wikilink")