**MOTSS**為[英文](../Page/英文.md "wikilink")「**Members of the same
sex**」的縮寫，意為**同性別的成員**，表示[同性戀關係或同性戀伴侶](../Page/同性戀.md "wikilink")。MOTSS起源於1970年[美國人口調查的表格中](../Page/美國.md "wikilink")，隨後逐漸成為[同性戀社群以及](../Page/同性戀社群.md "wikilink")[性學研究者的常用名詞](../Page/性學.md "wikilink")。相較於「Gay」或「Lesbian」專指男同性戀及女同性戀者，「Motss」顯得較為中性，故流行於同性戀之間。大概在1990年代，該詞相繼進入[台灣](../Page/台灣.md "wikilink")、[中國大陸等中文社會](../Page/中國大陸.md "wikilink")。

## 中文社會的使用情況

### 臺灣

臺灣最早的**MOTSS**版設於[國立中央大學資管龍貓BBS站台](../Page/國立中央大學.md "wikilink")，該站成立於1994年4月，由於受傳統思想影響，臺灣社會當時對同性戀多採取保守態度，版務工作亦相對低調。近年來，年青人對於性別觀念逐漸開放，目前臺灣高中及大學[BBS站台上設有](../Page/BBS.md "wikilink")「MOTSS」版的情況十分普遍，多為相關學生自行籌立。

其中，臺灣最大BBS[批踢踢之相關議題看板有](../Page/批踢踢.md "wikilink")「GAY」、「lesbian」兩看板取代常見之MOTSS板，該站另有「bi-sexual」、「transgender」兩板展現出批踢踢的性別與性向認同之多元性。

### 中國大陸

中國大陸首先設有「MOTSS」版的BBS站台為原中山医科大学的[BBS杏林站](../Page/BBS杏林站.md "wikilink")，设立时间不迟于1998年11月。其间BBS站台因各种原因曾多次关闭和复开。2001年10月[BBS杏林站随同其大学被合并](../Page/BBS杏林站.md "wikilink")，MOTSS版被关闭。2003年6月在合并后的BBS上重新申请开MOTSS版，未获得通过。

在北大[一塌糊涂开出](../Page/一塌糊涂.md "wikilink")**酷儿**版前，大陆教育网上曾有[水木清华](../Page/水木清华.md "wikilink")，[北大新青年设立同志版面](../Page/北大新青年.md "wikilink")。

## 外部連結

  - [Google
    討論區檢索中最早出現「MOTSS」的文章](http://groups.google.com.tw/groups?hl=zh-TW&lr=&selm=bnews.pyuxcc.420)

<!-- end list -->

  - [酷儿论坛](http://www.motss.info)

[Category:互联网用语](../Category/互联网用语.md "wikilink")
[Category:LGBT用語](../Category/LGBT用語.md "wikilink")