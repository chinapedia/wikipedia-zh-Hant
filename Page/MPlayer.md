**MPlayer**是一款[開源的多媒體播放器](../Page/開源.md "wikilink")，以[GNU通用公共许可证發佈](../Page/GNU通用公共许可证.md "wikilink")。此款軟體可在各主流[作業系統使用](../Page/作業系統.md "wikilink")，例如[Linux和其他](../Page/Linux.md "wikilink")[類Unix作業系統](../Page/類Unix.md "wikilink")、[微軟的](../Page/微軟.md "wikilink")[Windows系統及蘋果電腦的](../Page/Windows.md "wikilink")[Mac
OS
X系統](../Page/Mac_OS_X.md "wikilink")。MPlayer是建基於[命令行界面](../Page/命令行界面.md "wikilink")，在各作業系統可選擇安裝不同的[圖形界面](../Page/圖形界面.md "wikilink")。

## 开发

MPlayer的开发开始于2000年。最初的作者是Árpád
Gereöffy（在[Demoscene裡也被称为A](../Page/Demoscene.md "wikilink")'rpi
/
Astral），之后马上便有更多的开发者加入进来。这个项目启动是由于，当[XAnim于](../Page/XAnim.md "wikilink")1999年停止开发以后，A'rpi找不到满意的Linux视频播放器。最初的版本名为mpg12play
v0.1，是在半小时之内使用*libmpeg3*拼凑出的。在mpg12play
v0.95pre5版之后，它与一个基于avifile的[Win32](../Page/Win32.md "wikilink")
[DLL加载器的AVI播放器进行了代码合并](../Page/DLL.md "wikilink")，于是形成了2000年11月的MPlayer
v0.3。

最初绝大多数的开发者都来自于[匈牙利](../Page/匈牙利.md "wikilink")，但是现在，开发者遍布全球。自从2003年Alex
Beregszászi开始接替准备开发第二代MPlayer的Árpád Gereöffy来维护该项目。但是现在**MPlayer
G2**由于多种原因暂停开发。\[1\]

MPlayer最初的名字叫做"MPlayer - The Movie Player for
Linux"，不过后来开发者们简称其为"MPlayer - The Movie
Player"，原因是MPlayer已经不仅可以用于Linux而可以在所有平台上运行。

2004年到2005年，一个非官方的Mac OS X移植以比原版更高的版本号发布，名字叫MPlayer OS X
2[1](http://mplayerosx.sourceforge.net/) 。不久，OS
X版本在官方网站出现。由于版本号的冲突，官方的OS
X版MPlayer 1.0rc2，虽然版本号较低，但是实际上使用了更新更稳定的代码。2008年，MPlayer OS
X的非官方图形界面MPlayer OSX Extended诞生，是现在还在开发中的MPlayer OS X
[前端](../Page/前端.md "wikilink")
[2](https://web.archive.org/web/20090427035402/http://mplayerosx.sttz.ch/)
之一，另外还有一个叫MPlayerX [3](http://mplayerx.org/)，目前已上架Mac App Store。

## 支持的媒体文件格式

  - **實體媒介**:
    [CD](../Page/CD.md "wikilink")、[DVD](../Page/DVD.md "wikilink")、[Video
    CD](../Page/Video_CD.md "wikilink")、[Bluray
    Disc](../Page/Bluray_Disc.md "wikilink")
  - **[容器格式](../Page/容器格式.md "wikilink")**:
    [3GP](../Page/3GP.md "wikilink")、[AVI](../Page/AVI.md "wikilink")、[ASF](../Page/ASF.md "wikilink")、[FLV](../Page/FLV.md "wikilink")、[Matroska](../Page/Matroska.md "wikilink")、[MOV
    (QuickTime)](../Page/QuickTime.md "wikilink")、[MP4](../Page/MP4.md "wikilink")、[NUT](../Page/NUT.md "wikilink")、[Ogg](../Page/Ogg.md "wikilink")、[OGM](../Page/OGM.md "wikilink")、[RealMedia](../Page/RealMedia.md "wikilink")
  - **[视频格式](../Page/视频文件格式.md "wikilink")**:
    [Cinepak](../Page/Cinepak.md "wikilink")、[DV](../Page/DV.md "wikilink")、[H.263](../Page/H.263.md "wikilink")、[H.264/MPEG-4
    AVC](../Page/H.264/MPEG-4_AVC.md "wikilink")、[HuffYUV](../Page/HuffYUV.md "wikilink")、[Indeo](../Page/Indeo.md "wikilink")、[MJPEG](../Page/MJPEG.md "wikilink")、[MPEG-1](../Page/MPEG-1.md "wikilink")、[MPEG-2](../Page/MPEG-2.md "wikilink")、[MPEG-4
    Part
    2](../Page/MPEG-4_Part_2.md "wikilink")、[RealVideo](../Page/RealVideo.md "wikilink")、[Sorenson](../Page/Sorenson.md "wikilink")、[Theora](../Page/Theora.md "wikilink")、[WMV](../Page/WMV.md "wikilink")、[H.265/MPEG-H
    HEVC](../Page/H.265/MPEG-H_HEVC.md "wikilink")
  - **[音频格式](../Page/音频格式.md "wikilink")**:
    [AAC](../Page/AAC.md "wikilink")、[AC3](../Page/AC3.md "wikilink")、[ALAC](../Page/ALAC.md "wikilink")、[AMR](../Page/AMR.md "wikilink")、[FLAC](../Page/FLAC.md "wikilink"),
    Intel Music Coder, [Monkey's
    Audio](../Page/Monkey's_Audio.md "wikilink")、[MP3](../Page/MP3.md "wikilink")、[Musepack](../Page/Musepack.md "wikilink")、[RealAudio](../Page/RealAudio.md "wikilink")、[Shorten](../Page/Shorten.md "wikilink")、[Speex](../Page/Speex.md "wikilink")、[Vorbis](../Page/Vorbis.md "wikilink")、[WMA](../Page/WMA.md "wikilink")
  - **[字幕格式](../Page/字幕格式.md "wikilink")**: AQTitle,
    [ASS/SSA](../Page/ASS/SSA.md "wikilink")、[CC](../Page/CC.md "wikilink"),
    JACOsub, [MicroDVD](../Page/MicroDVD.md "wikilink"), MPsub, OGM,
    PJS, RT,
    [Sami](../Page/Sami.md "wikilink")、[SRT](../Page/SubRip.md "wikilink"),
    SubViewer, [VOBsub](../Page/VOBsub.md "wikilink"), VPlayer
  - **[图像格式](../Page/图像格式.md "wikilink")**:
    [BMP](../Page/BMP.md "wikilink")、[JPEG](../Page/JPEG.md "wikilink")、[PCX](../Page/PCX.md "wikilink"),
    PTX ,
    [TGA](../Page/TGA.md "wikilink")、[TIFF](../Page/TIFF.md "wikilink")、[SGI](../Page/Silicon_Graphics_Image.md "wikilink"),
    Sun Raster
  - **[网络协议](../Page/网络协议.md "wikilink")**:
    [RTP](../Page/RTP.md "wikilink")、[RTSP](../Page/RTSP.md "wikilink")、[HTTP](../Page/HTTP.md "wikilink")、[FTP](../Page/FTP.md "wikilink")、[MMS](../Page/MMS.md "wikilink"),
    Netstream (mpst://), [SMB](../Page/SMB.md "wikilink")

MPlayer还支持不同的驱动程序，包括[VDPAU](../Page/VDPAU.md "wikilink")、[X11](../Page/X_Window_System.md "wikilink")、[OpenGL](../Page/OpenGL.md "wikilink")、[DirectX](../Page/DirectX.md "wikilink")、[Quartz
Compositor](../Page/Quartz_Compositor.md "wikilink")、[VESA](../Page/VESA.md "wikilink")、[Framebuffer](../Page/Framebuffer.md "wikilink")、[SDL以及较少使用的](../Page/Simple_DirectMedia_Layer.md "wikilink")[ASCII
art和](../Page/ASCII_art.md "wikilink")[Blinkenlights](../Page/Project_Blinkenlights.md "wikilink")。它还能在装有电视卡的计算机上使用<tv://频道>收看电视节目，或者通过`radio://频道或频率`收听广播

自从1.0RC1版，能够使用[libass库来支持](../Page/libass.md "wikilink")[ASS/SSA字幕](../Page/SubStation_Alpha.md "wikilink")，虽然对于一些语言还存在问题

## 法律问题

大部分视频和音频格式都能通过[FFmpeg项目的](../Page/FFmpeg.md "wikilink")[libavcodec](../Page/libavcodec.md "wikilink")[函数库原生支持](../Page/函数库.md "wikilink")。对于那些没有开源解码器的格式，MPlayer使用二进制的函数库。它能直接使用Windows的[DLL](../Page/DLL.md "wikilink")。

[专有的](../Page/专有软件.md "wikilink")[CSS解析软件和相关格式使MPlayer成为被众多开放源代码播放器所使用的](../Page/Content-scrambling_system.md "wikilink")[后端](../Page/前端和后端.md "wikilink")。过去，MPlayer曾经包括[OpenDivX](../Page/OpenDivX.md "wikilink")，一个不兼容[GPL的解码器](../Page/GPL.md "wikilink")。这已经被删除，使MPlayer成为完全的[自由软件](../Page/自由软件.md "wikilink")。但是在自由软件中使用专有的解码器仍然是影响[FFmpeg](../Page/FFmpeg.md "wikilink")、MPlayer以及其他相关软件的潜在问题。

2004年1月，Mplayer的网站指控丹麦的DVD播放器制造商[KISS
Technology](../Page/KISS_Technology.md "wikilink")，在出售的播放器[固件中包括MPlayer的代码](../Page/固件.md "wikilink")，却没有用GPL发布这个固件，这违反了GPL协议。KISS的经理Peter
Wilmar Christensen反驳说，两段代码的相同不能说明KISS团队使用了MPlayer的代码。

## 参考文献

## 外部連結

  - [Official MPlayer
    Website](http://www.mplayerhq.hu/design7/news.html)，with extensive
    HTML documentation

:\* [List of supported
codecs](http://www.mplayerhq.hu/DOCS/codecs-status.html)

:\* [Projects related to
MPlayer](http://www.mplayerhq.hu/design7/projects.html)

  - [Documentation](http://wiki.linuxquestions.org/wiki/MPlayer) at the
    LinuxQuestions wiki
  - [MPlayer browser plugin for
    Mozilla](http://mplayerplug-in.sourceforge.net/)
  - [MPUI](http://mpui.sourceforge.net)，MPlayer User Interface for
    Windows ( 暫停開發 )
  - [MPUI-HCB](http://mpui-hcb.sourceforge.net)，MPlayer User Interface
    for Windows , MPUI 後繼開發
  - [MPUI](http://code.google.com/p/mulder/)，MuldeR's OpenSource
    projects
  - [RulesPlayer](https://web.archive.org/web/20120307123511/http://rulesplayer.altervista.org/)，clean
    and easy-to-use MPlayer GUI for Windows
  - [NMC
    player](https://web.archive.org/web/20071215121216/http://nmcplayer.sourceforge.net/)，MPlayer
    GUI in .NET 2.0 ( 暫停開發 )
  - [Videotranscoding
    Wiki](https://web.archive.org/web/20080915200703/http://videotranscoding.wikispaces.com/)
    (documentation on server-side usage of MPlayer for transcoding)
  - [SMPlayer](http://smplayer.sourceforge.net/)，another MPlayer based
    media player for Windows/Linux
  - [MPlayerXP](http://mplayerxp.sourceforge.net) is a branch of the
    well known MPlayer which is based on the new (thread based) core.
  - [MPlayer
    Portable](http://portableapps.com/apps/music_video/mplayer_portable)
  - [MPlayer WW](http://www.mplayer-ww.com) MPlayer Windows中文版（多国语言）

## 参见

  - [媒体播放器列表](../Page/媒体播放器列表.md "wikilink")

{{-}}

[Category:媒體播放器](../Category/媒體播放器.md "wikilink")
[Category:开源软件](../Category/开源软件.md "wikilink")
[Category:跨平台軟體](../Category/跨平台軟體.md "wikilink")

1.