**MT**或**mt**可能指以下其中一項：

**地理**

  - [馬耳他](../Page/馬耳他.md "wikilink")（[標準國家代碼](../Page/ISO_3166.md "wikilink")
    **MT**或[國家及地區頂級域](../Page/國家及地區頂級域.md "wikilink") **.mt**）
  - [美國](../Page/美國.md "wikilink")[蒙大拿州的英語縮寫](../Page/蒙大拿州.md "wikilink")
  - [巴西的](../Page/巴西.md "wikilink")[馬托格羅索州](../Page/馬托格羅索州.md "wikilink")（葡萄牙語：Mato
    Grosso）
  - [山的英語縮寫](../Page/山.md "wikilink")（**Mt.**）
  - [山地時區](../Page/山地時區.md "wikilink")（Mountain Time
    Zone），美國和[加拿大的一個](../Page/加拿大.md "wikilink")[時區](../Page/時區.md "wikilink")
  - [馬耳他語](../Page/馬耳他語.md "wikilink")（[ISO
    639](../Page/ISO_639.md "wikilink") alpha-2, **mt**）

**度量衡單位**

  - [百萬噸](../Page/百萬噸.md "wikilink") -
    英美的舊式重量單位，見[噸](../Page/噸.md "wikilink")
  - [百萬公頓](../Page/公頓.md "wikilink")

**電腦科學與互聯網**

  - [機器翻譯](../Page/機器翻譯.md "wikilink")（Machine Translation）的英語縮寫。
  - [多重執行緒](../Page/執行緒.md "wikilink")（Multi-threading）的英語縮寫。
  - [Movable
    Type](../Page/Movable_Type.md "wikilink")，[網志發布系統](../Page/網志.md "wikilink")。

**交通運輸**

  - [手動變速器](../Page/手動變速器.md "wikilink")（Manual Transmission），汽車部件。
  - [Thomas Cook
    Airlines的](../Page/Thomas_Cook_Airlines.md "wikilink")[國際航空運輸協會航空公司代碼](../Page/國際航空運輸協會航空公司代碼.md "wikilink")。

**科學**

  - [大地電磁探測](../Page/大地電磁探測.md "wikilink")（Magnetotellurics）
  - [䥑](../Page/䥑.md "wikilink")（Meitnerium），一種[人工合成的](../Page/人工合成元素.md "wikilink")[放射性元素](../Page/放射性.md "wikilink")，[原子序為](../Page/原子序.md "wikilink")109。
  - [微管](../Page/微管.md "wikilink")（Microtubule）
  - [線粒體](../Page/線粒體.md "wikilink")，在[mtDNA中](../Page/mtDNA.md "wikilink")
  - [顳葉](../Page/顳葉.md "wikilink")（medial
    temporal）[腦中](../Page/腦.md "wikilink")[新皮質的一部分](../Page/新皮質.md "wikilink")

**通信**

  - Mobile-terminated -
    [移動電話用語](../Page/移動電話.md "wikilink")，指移動設備收到的電話或[短訊](../Page/短訊.md "wikilink")，與MO（Mobile-originated）相對

**娛樂**

  - 主[坦克](../Page/坦克_\(電子遊戲\).md "wikilink")（Main-Tank），[電子遊戲術語](../Page/電子遊戲術語.md "wikilink")，[MMORPG](../Page/MMORPG.md "wikilink")（大型多人在線角色扮演遊戲）中承受攻擊的一種人物
  - Muscle Tracer，[裝甲核心遊戲中的敵人](../Page/裝甲核心.md "wikilink")
  - [Modern Talking](../Page/Modern_Talking.md "wikilink")，一支德國樂隊
  - [我叫MT](../Page/我叫MT.md "wikilink")
  - [韓國的一種修業旅行](../Page/韓國的一種修業旅行.md "wikilink")
  - [Masking Tape](../Page/Masking_Tape.md "wikilink")

**宗教**

  - 《[聖經](../Page/聖經.md "wikilink")·[新約全書](../Page/新約全書.md "wikilink")·[馬太福音](../Page/馬太福音.md "wikilink")》的英語縮寫。

**管理學**

  - [管理培訓生](../Page/管理培訓生.md "wikilink")（Management Trainee）