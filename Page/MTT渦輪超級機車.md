[Y2K_Turbine_Jet_bike.jpg](https://zh.wikipedia.org/wiki/File:Y2K_Turbine_Jet_bike.jpg "fig:Y2K_Turbine_Jet_bike.jpg")

**MTT渦輪超級機車**（）又稱**Y2K渦輪超級機車**，是[美國](../Page/美國.md "wikilink")[路易斯安那州](../Page/路易斯安那州.md "wikilink")[海軍陸戰隊渦輪科技公司所生產之](../Page/海軍陸戰隊渦輪科技.md "wikilink")[摩托车](../Page/摩托车.md "wikilink")，常簡稱為**Y2K**。該款車採接單製造，售價為150,000[美元](../Page/美元.md "wikilink")（至2004年為止售價為185,000美元）。

**Y2K**採用[勞斯萊斯製造的Allison](../Page/勞斯萊斯.md "wikilink")
250系列[渦輪軸引擎](../Page/渦輪軸引擎.md "wikilink")，為[貝爾直升機的發動機](../Page/貝爾直升機.md "wikilink")，最高[馬力為](../Page/馬力.md "wikilink")
320匹（238kW），後輪動力為286匹，最高轉速可達52,000[rpm](../Page/rpm.md "wikilink")，最大[扭力為](../Page/扭力.md "wikilink")425ft/lbs，名列為[探索頻道世界十大機車排名第四名](../Page/探索頻道.md "wikilink")。

## 外部連結

  - [Y2K
    官方網頁](https://web.archive.org/web/20070802141443/http://www.marineturbine.com/motorsports.asp)
  - [Y2K 相關影片](http://www.youtube.com/watch?v=NFGy-A68_vY)

## 參考資料

[分类:运动摩托车](../Page/分类:运动摩托车.md "wikilink")

[Category:海軍陸戰隊渦輪科技車輛](../Category/海軍陸戰隊渦輪科技車輛.md "wikilink")