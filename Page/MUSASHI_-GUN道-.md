『**MUSASHI -GUN道-**』是[Monkey
Punch原作時代劇槍械動作](../Page/Monkey_Punch.md "wikilink")[動畫作品](../Page/日本動畫.md "wikilink")。於2006年4月9日起[BS-i逢星期日上午](../Page/BS-i.md "wikilink")9:30～10:00放映，一共26集。此外，從2006年5月13日起，在[GyaO免費收看](../Page/GyaO.md "wikilink")。

標題的讀法為「GUN道MUSASHI」，但是在不同的媒介有不同的標題。於[BS-i以](../Page/BS-i.md "wikilink")『MUSASHI』（ムサシ）、イーネットフロンティア為『GUN道
MUSASHI』（ガンどう ムサシ）、其他名稱為『GUNDOH ムサシ』（ガンドー
ムサシ），有這樣的記述，表題在[GyaO以](../Page/GyaO.md "wikilink")『**MUSASHI
-GUN道-**』，同時是一般對此作品的稱呼。

## 概要

這作品是『[魯邦三世](../Page/魯邦三世.md "wikilink")』的原作者[Monkey
Punch花了十二年的時間所構成的成品](../Page/Monkey_Punch.md "wikilink")。由於低素質的動畫制作引起了廣泛的話題作品。\[1\]本作的話題性及人氣，2006年5月中旬在[GyaO綜合節目排名為第一位](../Page/GyaO.md "wikilink")、而無修改版本的DVD在日本的排名位於前列。

## 故事

與現實世界不同的是，由豐臣家統治日本，此外各地出現妖怪，武藏開始了討伐妖怪的工作。

## 登場人物

  - 表記是「登場人物名：[声優](../Page/声優.md "wikilink")」。

### 武藏一方

與武藏同行以及跟武藏為友好關係的人物。

  - 宮本武藏（）：[浪川大輔](../Page/浪川大輔.md "wikilink")
    本作的武藏，名字以[平假名顯示](../Page/平假名.md "wikilink")。二丁拳銃對特殊用的彈藥，以（陰陽彈？）和作戰的少年。

<!-- end list -->

  - 浪人（）：[堀川仁](../Page/堀川仁.md "wikilink")

<!-- end list -->

  - 忍者太郎（）：[小林优](../Page/小林优.md "wikilink")

<!-- end list -->

  - デスペラード：[柳沢真由美](../Page/柳沢真由美.md "wikilink")

<!-- end list -->

  - 辉夜(カグヤ)：[河原木志穂](../Page/河原木志穂.md "wikilink")
    [豐臣秀吉養女於大阪城居住的姫](../Page/豐臣秀吉.md "wikilink")。

<!-- end list -->

  - ダンジョウ：[山野井仁](../Page/山野井仁.md "wikilink")

<!-- end list -->

  - タクアン和尚：[田中総一郎](../Page/田中総一郎.md "wikilink")

<!-- end list -->

  - 茶毘之字 ：[廣田行生](../Page/廣田行生.md "wikilink")

<!-- end list -->

  - 猿飛佐助（さるとび さすけ）：小林优

<!-- end list -->

  - 夢姫（ゆめひめ）：[中村千繪](../Page/中村千繪.md "wikilink")

<!-- end list -->

  - ウラシマ：[五十嵐麗](../Page/五十嵐麗.md "wikilink")

<!-- end list -->

  - 馬

### アヤカシ

與武藏敵對的怪物。。

  - [夜叉](../Page/夜叉.md "wikilink")：五十嵐麗

<!-- end list -->

  - リョウゲン：[斎藤志郎](../Page/斎藤志郎.md "wikilink")
    正體為[德川家康](../Page/德川家康.md "wikilink")。

<!-- end list -->

  - ジジョウダ

<!-- end list -->

  - ガンダダーン：[根本圭子](../Page/根本圭子.md "wikilink")

<!-- end list -->

  - アヤカシ[カラス](../Page/カラス属.md "wikilink")：[桐井大介](../Page/桐井大介.md "wikilink")

<!-- end list -->

  - アヤカシ[フクロウ](../Page/フクロウ.md "wikilink")

<!-- end list -->

  - アヤカシ[武田信玄](../Page/武田信玄.md "wikilink")（たけだ
    しんげん）：[西凛太朗](../Page/西凛太朗.md "wikilink")

<!-- end list -->

  - 気の塊：[赤城進](../Page/赤城進.md "wikilink")

<!-- end list -->

  - 餓鬼魂（がきこん）

### 其他人物

  - 吉岡傳七郎（よしおか でんしちろう）：赤城進
    兄・吉岡清十郎仇恨帶領超過50人與武藏決鬥、在一城寺的決鬥敗給武藏。

<!-- end list -->

  - 師範：赤城進

<!-- end list -->

  - [明智光秀桐井大介](../Page/明智光秀.md "wikilink")

<!-- end list -->

  - 石川五右衛門赤城進
    繼續了第一代[石川五右衛門的第二代](../Page/石川五右衛門.md "wikilink")。

<!-- end list -->

  - [小早川秀秋](../Page/小早川秀秋.md "wikilink")：[武藤正史](../Page/武藤正史.md "wikilink")

<!-- end list -->

  - [真田幸村](../Page/真田幸村.md "wikilink")（さなだ
    ゆきむら）：[木下紗華](../Page/木下紗華.md "wikilink")

<!-- end list -->

  - [三好清海入道](../Page/三好清海入道.md "wikilink")（みよし
    せいかいにゅうどう）：[飯島肇](../Page/飯島肇.md "wikilink")

<!-- end list -->

  - [霧隱才藏](../Page/霧隱才藏.md "wikilink")：[増田裕生](../Page/増田裕生.md "wikilink")
    真田十勇士其一，身型瘦削。使用[手裡劍](../Page/手裡劍.md "wikilink")。

<!-- end list -->

  - [根津甚八](../Page/根津甚八.md "wikilink")（ねづ じんぱち）：赤城進
    真田十勇士其一。

<!-- end list -->

  - [豐臣秀賴](../Page/豐臣秀賴.md "wikilink")（とよとみ
    ひでより）：[笹島かほる](../Page/笹島かほる.md "wikilink")
    豊臣秀吉兒子。

<!-- end list -->

  - [石田三成](../Page/石田三成.md "wikilink")（いしだ みつなり）：赤城進
    關原之戰西軍總大將。協助對政治沒有興趣的秀賴。

<!-- end list -->

  - [德川秀忠](../Page/德川秀忠.md "wikilink")（とくがわ　ひでただ）：[木下尚紀](../Page/木下尚紀.md "wikilink")
    德川家康的三男。

<!-- end list -->

  - ガン鬼

<!-- end list -->

  - [ラセツ](../Page/羅刹天.md "wikilink")

<!-- end list -->

  - [佐佐木小次郎](../Page/佐佐木小次郎.md "wikilink")（ささき
    こじろう）：[櫻井孝宏](../Page/櫻井孝宏.md "wikilink")
    武藏的宿敵、槍道的大師。
    與史實不符的是本作品的小次郎是女性，卻由男性擔當配音。

<!-- end list -->

  - [豐臣秀吉](../Page/豐臣秀吉.md "wikilink")（とよとみひでよし）

## MUSASHI世界裡的「歷史」

  - 1500年初期　[列奥纳多·达·芬奇死亡](../Page/列奥纳多·达·芬奇.md "wikilink")
  - 1573年　[武田信玄死亡](../Page/武田信玄.md "wikilink")
  - 1582年　[明智光秀發起](../Page/明智光秀.md "wikilink")[本能寺之變](../Page/本能寺之變.md "wikilink")。光秀和[織田信長死亡](../Page/織田信長.md "wikilink")。
  - 1597年－[慶長之役豊臣軍取得勝利](../Page/慶長之役.md "wikilink")。反對出兵的德川家康失去了權力。
  - 1600年7月 石田三成出兵。アヤカシ自千年的沉睡中覺醒。
  - 1600年9月 關原之戰爆發。在小早川秀秋活躍下，西軍取得勝利。
  - 豐臣秀賴就任征夷大將軍。
  - 於大阪創世豐臣幕府。
  - 1603年 家康與見面。
  - 江戶被妖怪佔領。
  - 1605年 在一城寺下進行決鬥（第1話）。

## 製作團隊

  - 原作 - [Monkey Punch](../Page/Monkey_Punch.md "wikilink")
  - 監督 - [木下勇喜](../Page/木下勇喜.md "wikilink")
  - 人物設計 - [須田正巳](../Page/須田正己.md "wikilink")
  - 主要腳本統籌 - [酒井直行](../Page/酒井直行.md "wikilink")
  - 美術監督 - 中原英統
  - 音響監督 - [高桑一](../Page/高桑一.md "wikilink")
  - 音響製作 - 平田哲、明瀬礼洋（[Dax
    Production](../Page/Dax_International.md "wikilink")）
  - 音樂監督 - 石黒典生
  - 劇中音楽 - 小池敦、[杉本善德](../Page/杉本善德.md "wikilink")
  - 編輯 - 西山茂（リアル・ティ）
  - 製作統括製作人 - 菅谷信行
  - 動畫製作 - [ACC Production製作工作室](../Page/ACC_Production.md "wikilink")
  - [製作](../Page/製作.md "wikilink") - 「MUSASHI」製作委員会（ACC Production、E-NET
    Frontier）

## 主題曲

  - 片頭曲

:; 「GHOST BUSTERZ」（1 - 14話）

:: 歌 - [ULTRA BRAiN](../Page/ULTRA_BRAiN.md "wikilink")

:; 「Glitter」（15話 - ）

  -

      -
        歌 - [Phantasmagoria](../Page/Phantasmagoria.md "wikilink")

  - 片尾曲

:; 「STYLE」（1 - 14話）

:: 歌 - [Kimeru](../Page/Kimeru.md "wikilink")

:; 「艶色の光」（15話 - ）

  -

      -
        歌 - 青山愛（[野本愛](../Page/野本愛.md "wikilink")）

## 各話列表

### 各話標題

各方面的預告和播放時的標題和簡介有時會不一致。各種媒體是指以[BS-i開始的作品介紹和](../Page/BS-i.md "wikilink")[動畫雜誌的發表](../Page/動畫雜誌.md "wikilink")。

<table>
<thead>
<tr class="header">
<th></th>
<th><p>實際播放</p></th>
<th><p>各種媒體發表</p></th>
<th><p>下回預告</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第1話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第2話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第3話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第4話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第5話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第6話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第7話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第8話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第9話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第10話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第11話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>總集篇</p></td>
<td></td>
<td><p>総集編</p></td>
<td><p>（無預告）</p></td>
</tr>
<tr class="odd">
<td><p>第12話</p></td>
<td><p>猿飛佐助</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第13話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第14話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第15話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第16話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第17話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第18話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第19話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第20話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第21話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第22話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第23話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第24話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第25話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第26話</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>總集編1</p></td>
<td></td>
<td><p>預定標題</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>總集編2</p></td>
<td></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>總集完結編</p></td>
<td><p>總集完結編</p></td>
<td><p>colspan="2" </p></td>
<td></td>
</tr>
</tbody>
</table>

### 各話製作人員

<table>
<thead>
<tr class="header">
<th><p>話數</p></th>
<th><p>標題</p></th>
<th><p>腳本</p></th>
<th><p>分鏡</p></th>
<th><p>演出</p></th>
<th><p>作畫監督</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td><p>酒井直行</p></td>
<td><p><a href="../Page/木下勇喜.md" title="wikilink">木下勇喜</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p><a href="../Page/福冨博.md" title="wikilink">福富博</a></p></td>
<td><p>国立宏</p></td>
<td><p>木下勇喜<br />
倲偉文</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td><p>深大寺ワタル</p></td>
<td><p>陳龍<br />
倲偉文</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td><p><a href="../Page/勝間田具治.md" title="wikilink">勝間田具治</a></p></td>
<td><p>陳龍</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td><p>福富博</p></td>
<td><p>陳龍<br />
倲偉文</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td></td>
<td><p>木下勇喜</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td></td>
<td><p>福富博</p></td>
<td><p>秋津稔</p></td>
<td><p>白南烈</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td></td>
<td><p>勝間田具治</p></td>
<td><p>深大寺ワタル</p></td>
<td><p>白南烈<br />
姜致根</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td></td>
<td><p>福富博</p></td>
<td><p>秋津稔</p></td>
<td><p>木下勇喜</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td></td>
<td><p>渡部英雄</p></td>
<td><p>国立秀雄</p></td>
<td><p>倲偉文</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td></td>
<td><p>勝間田具治</p></td>
<td><p>深大寺ワタル</p></td>
<td><p>姜致根</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td></td>
<td><p>福冨博</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td></td>
<td><p>渡部英雄<br />
木下勇喜</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td></td>
<td><p>勝間田具治</p></td>
<td><p>秋津稔</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td></td>
<td><p>福富博</p></td>
<td><p>国立秀雄</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td></td>
<td><p>勝間田具治</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td></td>
<td><p>福富博</p></td>
<td><p>秋津稔</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td></td>
<td><p>渡部英雄<br />
木下勇喜</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td></td>
<td><p>福富博</p></td>
<td><p>国立秀雄</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td></td>
<td><p>勝間田具治</p></td>
<td><p>秋津稔</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td></td>
<td><p>福富博</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td></td>
<td><p>勝間田具治</p></td>
<td><p>国立秀雄</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td></td>
<td><p>福富博</p></td>
<td><p>秋津稔</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td></td>
<td><p>木下勇喜</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td></td>
<td><p>勝間田具治</p></td>
<td><p>国立秀雄</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>26</p></td>
<td></td>
<td><p>福富博</p></td>
<td><p>秋津稔</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

</div>

</div>

## 相關商品

  - 電視播映原版DVD-BOX（2006年7月7日發售。1話-8話。初回限定版本・5枚一組）

<!-- end list -->

  - Orginal Soundtrack（預定2006年9月20日發行，但似乎中止了發行）

## 參考

<references />

## 相關條目

  - [作畫崩壞](../Page/作畫崩壞.md "wikilink")
  - [GUNDRESS](../Page/GUNDRESS.md "wikilink")（MUSASHI的製作公司、ACC製作攝影與參與的作品）
  - [Answer
    Studio](../Page/Answer_Studio.md "wikilink")（第24話的作畫擔當、本回作畫崩壞發生次數很少）
  - [RGB Adventure](../Page/RGB_Adventure.md "wikilink")

## 外部連結

  - [BS-i介紹網頁](https://web.archive.org/web/20060714092521/http://www.bs-i.co.jp/main/entertainment/show.php?0050)
  - [イーネット・フロンティア
    作品紹介サイト](https://web.archive.org/web/20060831060204/http://enet-dvd.com/musashi/)
  - [MUSASHI -GUN道-
    Wiki（日語）](https://archive.is/20130427053831/http://musashigundo.com/)

[Category:2006年日本電視動畫](../Category/2006年日本電視動畫.md "wikilink")
[Category:宮本武藏題材作品](../Category/宮本武藏題材作品.md "wikilink")
[Category:TBS動畫](../Category/TBS動畫.md "wikilink")

1.