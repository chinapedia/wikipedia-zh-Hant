《**MUSIC
STATION**》（）是[日本](../Page/日本.md "wikilink")[朝日電視台的](../Page/朝日電視台.md "wikilink")[音樂](../Page/音樂.md "wikilink")[直播節目](../Page/現場直播.md "wikilink")，自1986年10月24日起於[日本時間每星期五](../Page/日本時間.md "wikilink")20時整至20時54分播出。該節目在日本經常被簡稱為「****」（[日語羅馬字](../Page/日語羅馬字.md "wikilink")：M
Sute）。現任[主持人為](../Page/主持人.md "wikilink")[塔摩利](../Page/塔摩利.md "wikilink")，副主持人為朝日電視台主播[並木万里菜](../Page/並木万里菜.md "wikilink")。由於節目內容以[日本流行音樂為主](../Page/日本流行音樂.md "wikilink")，在日本以外地區亦有播出。

## 沿革

《MUSIC
STATION》是每週播放[一小時的音樂節目](../Page/小時.md "wikilink")，與[美國的](../Page/美國.md "wikilink")《TRL》和[英國的](../Page/英國.md "wikilink")《[British
Top of the
Pops](../Page/British_Top_of_the_Pops.md "wikilink")》節目形式上相似，均是形形色色的演出，提供歌曲排名和其他小單元的節目。許多日本[歌手和](../Page/歌手.md "wikilink")[組合都選擇在](../Page/組合.md "wikilink")《MUSIC
STATION》首次公開演出。《MUSIC STATION》亦有許多世界級的歌手曾經在節目中演出，包括[Avril
Lavigne](../Page/Avril_Lavigne.md "wikilink")、[Katy
Perry](../Page/Katy_Perry.md "wikilink")、[Lady
Gaga](../Page/Lady_Gaga.md "wikilink")、[Mariah
Carey](../Page/Mariah_Carey.md "wikilink")、[Beyoncé](../Page/Beyoncé.md "wikilink")、[Maroon
5](../Page/Maroon_5.md "wikilink")、[U2](../Page/U2.md "wikilink")、[Oasis](../Page/Oasis.md "wikilink")、[Greenday](../Page/Greenday.md "wikilink")、[Taylor
Swift和](../Page/Taylor_Swift.md "wikilink")[Red Hot Chili
Peppers等等](../Page/Red_Hot_Chili_Peppers.md "wikilink")。

《MUSIC
STATION》現任主持人為[田森](../Page/田森.md "wikilink")，他自1987年起擔任該節目的主持，至今已主持超過1000集；而他的招牌墨鏡打扮更多次被多位嘉賓歌手模仿。副主持人則經過多次更換，在1996年4月起固定由朝日電視台的[新進](../Page/新人.md "wikilink")[播報員擔任](../Page/播報員.md "wikilink")，現由[並木万里菜擔任](../Page/並木万里菜.md "wikilink")。

## 節目單元

節目內包含數種不同的小單元，以下依單元在節目中的出現順序進行介紹。

### BIRTH YEAR SONGS

  -
    2010年2月19日起播出（日本時間），選出某年某月的某週播放當週的單曲排名，讓相同月分出生的人說出他們對歌手或歌曲的第一印象與看法。
    此單元目前是放在開場歌手演出完畢時播放。

### MUSIC QUESTION?

  -
    2009年10月16日起播出（日本時間），製作單位會向觀眾募集與音樂或歌手相關的問題並向歌手提問。原先是預錄好以影片形式播出，現在則融入節目慣例的交談環節，由副主持人在節目當中向嘉賓提問。
    此單元目前是放在 BIRTH YEAR SONGS 之後，各組歌手上台準備前的交談當中。

### SINGLE RANKING BEST10

  -
    此單元揭示每星期銷量最高的十支單曲唱片。這排行榜與[Oricon銷量榜有所不同](../Page/Oricon.md "wikilink")，Oricon計算由每週的星期一至星期日銷量，而SINGLE
    RANKING
    BEST10則計算由每週星期五至下一星期的星期四銷量，這與節目的播放天一致。這單元自1989年啟播開始，已經幾乎每集都會出現。如果歌手的單曲銷量打破自己或全球銷售紀錄，通常都會於節目中演出。
    此單元目前是放在 BIRTH YEAR SONGS 之後的一到兩組歌手演出完畢時。

| 年度    | 曲名                                                                     | 歌手名                                                                                    |
| ----- | ---------------------------------------------------------------------- | -------------------------------------------------------------------------------------- |
| 1995年 | [TOMORROW](../Page/TOMORROW_\(岡本真夜單曲\).md "wikilink")                  | [岡本真夜](../Page/岡本真夜.md "wikilink")                                                     |
| 1996年 | [LA·LA·LA LOVE SONG](../Page/LA·LA·LA_LOVE_SONG.md "wikilink")         | [久保田利伸](../Page/久保田利伸.md "wikilink")&[娜歐蜜·坎貝兒](../Page/娜歐蜜·坎貝兒.md "wikilink")          |
| 1997年 | [CAN YOU CELEBRATE?](../Page/CAN_YOU_CELEBRATE?.md "wikilink")         | [安室奈美惠](../Page/安室奈美惠.md "wikilink")                                                   |
| 1998年 | [夜空的彼方](../Page/夜空的彼方.md "wikilink")                                   | [SMAP](../Page/SMAP.md "wikilink")                                                     |
| 1999年 | [Automatic](../Page/Automatic/time_will_tell.md "wikilink")            | [宇多田光](../Page/宇多田光.md "wikilink")                                                     |
| 2000年 | [TSUNAMI](../Page/海嘯_\(單曲\).md "wikilink")                             | [南方之星](../Page/南方之星.md "wikilink")                                                     |
| 2001年 | [Can You Keep A Secret?](../Page/Can_You_Keep_A_Secret?.md "wikilink") | [宇多田光](../Page/宇多田光.md "wikilink")                                                     |
| 2002年 | [H](../Page/H_\(濱崎步單曲\).md "wikilink")                                 | [濱崎步](../Page/濱崎步.md "wikilink")                                                       |
| 2003年 | [世界上唯一的花](../Page/世界上唯一的花.md "wikilink")                               | [SMAP](../Page/SMAP.md "wikilink")                                                     |
| 2004年 | [花](../Page/花_\(橘子新樂園單曲\).md "wikilink")                               | [橘子新樂園](../Page/橘子新樂園.md "wikilink")                                                   |
| 2005年 | [青春Amigo](../Page/青春Amigo.md "wikilink")                               | [修二與彰](../Page/修二與彰.md "wikilink")                                                     |
| 2006年 | [Real Face](../Page/Real_Face.md "wikilink")                           | [KAT-TUN](../Page/KAT-TUN.md "wikilink")                                               |
| 2007年 | [化為千風](../Page/化為千風_\(秋川雅史\).md "wikilink")                            | [秋川雅史](../Page/秋川雅史.md "wikilink")                                                     |
| 2008年 | [truth/前往風的彼方](../Page/truth/前往風的彼方.md "wikilink")                     | [嵐](../Page/嵐.md "wikilink")                                                           |
| 2009年 | [Believe/烏雲散去、天氣晴](../Page/Believe/烏雲散去、天氣晴.md "wikilink")             | [嵐](../Page/嵐.md "wikilink") / [矢野健太 starring Satoshi Ohno](../Page/大野智.md "wikilink") |

獲得全年第一位的歌曲（1995年－2009年）

### Young Guns

  -
    2005年2月18日起播出（日本時間），之後採不定期播放。本單元會邀請近期較活躍的新進歌手或樂隊出席。自2012年5月4日起改以網路節目的形式，使用
    Young Guns on the Web
    之名義於[YouTube官方頻道播出](../Page/YouTube.md "wikilink")。

## 特別節目

### 春、秋季特別節目

  -
    每年春季（三、四月）及秋季（九、十月)電視節目改組時期會請六到七組藝人來參與現場直播三小時（日本時間19:00\~21:48）的特別節目。

### 冬、夏季特別節目

  -
    每年冬季（一月初）及夏季（六月底七月初）會錄影播出兩小時（日本時間20:00\~21:48）的特別節目，內容包含過去的交談經典畫面或單曲排行榜。但因為是不邀請嘉賓，只有主持人作簡單評論的形式，故海外多不引進。

### MUSIC STATION SUPER LIVE

  -
    自1992年起，每年[聖誕節的當週或前一週的週五舉行](../Page/聖誕節.md "wikilink")，會在所租借的場館開
    MUSIC STATION SUPER LIVE
    演唱會，並以[現場直播的形式播出](../Page/現場直播.md "wikilink")。至2011年已屆滿廿年，節目拍攝目前位於[東京市郊的](../Page/東京.md "wikilink")[千葉縣](../Page/千葉縣.md "wikilink")[幕張展覽館](../Page/幕張展覽館.md "wikilink")。
    從第一次撥出的2小時30分（1992\~1993）、3小時（1994）、2小時45分（1995）、3小時45分（1996\~2002）、4小時5分（2003）、4小時10分(2004\~現在)，逐年增加播出時間。
    [台灣亦播出該特別節目](../Page/台灣.md "wikilink")，並譯名為《MUSIC
    STATION新春豪華版》。日本方面播出後，由[緯來日本台在每年的](../Page/緯來日本台.md "wikilink")[台灣時間](../Page/台灣時間.md "wikilink")1月1日（[元旦](../Page/元旦.md "wikilink")）晚間六點播出，並於[農曆](../Page/農曆.md "wikilink")[春節期間選擇一天重播](../Page/春節.md "wikilink")。

### MUSIC STATION ULTRA FES

  -
    2015年9月23日，以纪念MUSIC
    STATION节目开播第三十年，朝日电视台放送10小时特别直播节目“进入第三十年！史上首次10小时特别节目
    MUSIC STATION ULTRA
    FES”，邀请日本国内外各类风格的大牌明星、乐队、艺人、偶像组合、演奏家等共计约60组进行表演。并且与身在全球各地的音乐人连线直播。节目中还发表了“给日本人充满能量，世界所夸赞的日本歌曲BEST100”榜单。
    此后ULTRA FES就成为了MUSIC STATION在每年9月公休日固定的一档超长时间直播特别节目。
    2016年9月19日，“30周年纪念特别节目 MUSIC STATION ULTRA
    FES”播出。发布了“影响日本的歌曲BEST100”榜单。
    2017年9月18日，“MUSIC STATION ULTRA FES 2017”播出，发布了“振奋人心的超级歌曲BEST100”榜单。

## 演出人員

### 主持人

| 時間          | |正主持人       | |副主持人                                | |助理主持人                               |
| ----------- | ----------- | ------------------------------------ | ------------------------------------ |
| 1986年10月24日 | 1987年3月27日  | [關口宏](../Page/關口宏.md "wikilink")     | [中原理惠](../Page/中原理惠.md "wikilink")   |
| 1987年4月3日   | 1987年12月25日 | [田森](../Page/田森.md "wikilink")       | [松井康真](../Page/松井康真.md "wikilink")   |
| 1988年1月8日   | 1990年3月23日  | [松井康真](../Page/松井康真.md "wikilink")   | [木下智佳子](../Page/木下智佳子.md "wikilink") |
| 1990年4月13日  | 1993年3月19日  | [生島博](../Page/生島博.md "wikilink")     | （空缺）                                 |
| 1993年4月13日  | 1993年9月24日  | [有賀五月](../Page/有賀五月.md "wikilink")   |                                      |
| 1993年10月15日 | 1996年3月22日  | [有賀五月](../Page/有賀五月.md "wikilink")   | （廢止）                                 |
| 1996年4月5日   | 2000年3月31日  | [下平沙耶加](../Page/下平沙耶加.md "wikilink") |                                      |
| 2000年4月14日  | 2004年3月12日  | [武內繪美](../Page/武內繪美.md "wikilink")   |                                      |
| 2004年4月9日   | 2008年9月12日  | [堂真理子](../Page/堂真理子.md "wikilink")   |                                      |
| 2008年10月3日  | 2013年9月27日  | [竹內由惠](../Page/竹內由惠.md "wikilink")   |                                      |
| 2013年10月18日 | 2018年9月17日  | [弘中綾香](../Page/弘中綾香.md "wikilink")   |                                      |
| 2018年10月19日 | 現任          | [並木万里菜](../Page/並木万里菜.md "wikilink") |                                      |

歷任主持人

### 旁白

  - 現在

<!-- end list -->

  - Ward Sexton（嘉賓出場、單元標題、單曲名稱等，1994年4月 - ）
  - 服部潤（BIRTH YEAR SONGS 解說，2005年4月1日 - ）
  - [上坂堇](../Page/上坂堇.md "wikilink")（2017年7月28日 - ）
  - [佐仓绫音](../Page/佐仓绫音.md "wikilink")（2017年8月25日 - ）

<!-- end list -->

  - 過去

<!-- end list -->

  - [村井每早](../Page/村井每早.md "wikilink")（ - 2005年6月）

  - [皆口裕子](../Page/皆口裕子.md "wikilink")

  - （2005年7月 - 2017年7月）

## 來賓類型

### J-POP歌手

  -
    除屬[傑尼斯事務所的歌手外](../Page/傑尼斯事務所.md "wikilink")，其他主要為[愛貝克思及](../Page/愛貝克思.md "wikilink")[索尼BMG](../Page/索尼BMG.md "wikilink")（Sony
    Music）的歌手都會於每星期出演。

### 演歌歌手

  -
    演歌歌手在節目播放初期經常出演但開始減少，而在1992年以後開始不演出。除了傑尼斯的[關西傑尼斯8](../Page/關西傑尼斯8.md "wikilink")（）外、最近偶爾會有[冰川清志](../Page/冰川清志.md "wikilink")（）出演，不過多以流行歌手“KIYOSHI”的身份出演。其他例如在1992年9月2日出演的[堀内孝雄和](../Page/堀内孝雄.md "wikilink")[桂銀淑](../Page/桂銀淑.md "wikilink")。

### 古典音樂歌手

  -
    [古典音樂歌手在本節目出演的機會非常小](../Page/古典音樂.md "wikilink")，而憑一首〈〉而紅的[秋川雅史能於](../Page/秋川雅史.md "wikilink")2007年4月6日的Music
    Station Special和同年的12月21日的Music Station Super Live演出。

### 動畫歌手

  -
    近年有少量動畫歌手在本節目出演，[藍井艾露和](../Page/藍井艾露.md "wikilink")[LiSA都曾經出演過](../Page/LiSA.md "wikilink")；[聲優方面](../Page/聲優.md "wikilink")，[椎名碧流為首位在本節目出演的聲優](../Page/椎名碧流.md "wikilink")，18年後[水樹奈奈才成為第二位出演的聲優](../Page/水樹奈奈.md "wikilink")。2015年9月23日，虛擬歌手[初音未來於本節目](../Page/初音未來.md "wikilink")30週年10小時特別節目出演，收視率暴增，成為一時的話題。

### 海外歌手

  -
    從1986年10月24日[成龍出演以來](../Page/成龍.md "wikilink")，海外歌手樂隊例如：[阿兰·达瓦卓玛](../Page/阿兰·达瓦卓玛.md "wikilink")（Alan）、[女子十二乐坊](../Page/女子十二乐坊.md "wikilink")、[五月天](../Page/五月天.md "wikilink")、[Kiss合唱團](../Page/Kiss合唱團.md "wikilink")（KISS）、[快嘴約翰](../Page/快嘴約翰.md "wikilink")（Scatman
    John）、[-{zh-hans:空中铁匠;zh-hk:史密夫飛船;zh-tw:史密斯飛船;}-](../Page/空中铁匠.md "wikilink")（Aerosmith）、[藍尼·克羅維茲](../Page/藍尼·克羅維茲.md "wikilink")（Lenny
    Kravitz\]）、[瑪麗亞·凱莉](../Page/瑪麗亞·凱莉.md "wikilink")（Mariah
    Carey）、[希拉里·达夫](../Page/希拉里·达夫.md "wikilink")（Hilary
    Duff）、[艾薇兒·拉維尼](../Page/艾薇兒·拉維尼.md "wikilink")（Avril
    Lavigne）、[後街男孩](../Page/後街男孩.md "wikilink")（Backstreet
    Boys）、[史提夫·汪達](../Page/史提夫·汪達.md "wikilink")（Stevie
    Wonder）、[U2](../Page/U2.md "wikilink")、[Greenday](../Page/Greenday.md "wikilink")、[嗆辣紅椒](../Page/嗆辣紅椒.md "wikilink")（Red
    Hot Chili Peppers）、[魔力紅](../Page/魔力紅.md "wikilink")（Maroon
    5）、[黑眼豆豆](../Page/黑眼豆豆.md "wikilink")（The Black Eyed
    Peas）、[Lady Gaga](../Page/Lady_Gaga.md "wikilink")（-{Lady
    Gaga}-）、[-{zh-cn:利昂娜·刘易斯; zh-tw:里歐娜; zh-hk:Leona Lewis;
    }-](../Page/利昂娜·劉易斯.md "wikilink")（Leona
    Lewis）、[诺拉·琼斯](../Page/诺拉·琼斯.md "wikilink")（Norah
    Jones）、[碧昂絲·諾利斯](../Page/碧昂絲·諾利斯.md "wikilink")（Beyoncé）等亦曾經出演。至2011年3月為止，出演最多次數的海外歌手是[艾薇兒·拉維尼](../Page/艾薇兒·拉維尼.md "wikilink")，次數達6次。

### 傑尼斯事務所歌手

  -
    傑尼斯事務所的歌手每週也會出演。但是在1997年11月14日播出的節目，[DA
    PUMP在緊接直播播出前決定出演](../Page/DA_PUMP.md "wikilink")，以至於[KinKi
    Kids的演出被取消](../Page/KinKi_Kids.md "wikilink")。
    另外，其他[小傑尼斯也會出演](../Page/小傑尼斯.md "wikilink")，把同屬傑尼斯事務所的前輩的樂曲以組曲形成表演（基本上都是當晚第一組表演者或沒有訪問）。另一方面，[Ya-Ya-yah和](../Page/Ya-Ya-yah.md "wikilink")[Kis-My-Ft.2也試過在節目內表現原創的樂曲](../Page/Kis-My-Ft.2.md "wikilink")（這跟[KAT-TUN](../Page/KAT-TUN.md "wikilink")
    Jr.時期的演出一樣）。
    此外，1988年－1992年期間，[光GENJI作常規演出](../Page/光GENJI.md "wikilink")。但是在1993年、1994年期間，也有沒有傑尼斯藝人演出的回數。

### 演員

  -
    曾上節目的男演員例如[織田裕二](../Page/織田裕二.md "wikilink")、[福山雅治](../Page/福山雅治.md "wikilink")、[陣内孝則](../Page/陣内孝則.md "wikilink")、[中井貴一](../Page/中井貴一.md "wikilink")、[江口洋介](../Page/江口洋介.md "wikilink")、[片岡鶴太郎](../Page/片岡鶴太郎.md "wikilink")、[石原裕次郎](../Page/石原裕次郎.md "wikilink")、[高橋克典](../Page/高橋克典.md "wikilink")、[藤木直人](../Page/藤木直人.md "wikilink")、[松平健](../Page/松平健.md "wikilink")、[玉木宏](../Page/玉木宏.md "wikilink")、[植木等等](../Page/植木等.md "wikilink")。
    女演員例如[中山美穗](../Page/中山美穗.md "wikilink")、[永作博美](../Page/永作博美.md "wikilink")、[篠原涼子](../Page/篠原涼子.md "wikilink")、[瀨戶朝香](../Page/瀨戶朝香.md "wikilink")、[友坂理惠](../Page/友坂理惠.md "wikilink")、[廣末涼子](../Page/廣末涼子.md "wikilink")、[松隆子](../Page/松隆子.md "wikilink")、[觀月亞里莎](../Page/觀月亞里莎.md "wikilink")、[仲間由紀惠及](../Page/仲間由紀惠.md "wikilink")[新垣結衣等亦曾出演](../Page/新垣結衣.md "wikilink")。
    藝人方面，[明石家秋刀魚與](../Page/明石家秋刀魚.md "wikilink")[所喬治](../Page/所喬治.md "wikilink")（作為三人組合跟[工藤静香出演](../Page/工藤静香.md "wikilink")。所喬治也有單獨出演過）、[隧道二人組](../Page/隧道二人組.md "wikilink")（以野猿第份出演）、[海王星（ネプチューン）](../Page/海王星（ネプチューン）.md "wikilink")、[小堺一機](../Page/小堺一機.md "wikilink")、[松嶋尚美](../Page/松嶋尚美.md "wikilink")（作為搖滾樂隊KILLERS出演）、[藤井隆](../Page/藤井隆.md "wikilink")、[鈴木紗理奈](../Page/鈴木紗理奈.md "wikilink")、倫敦靴子1号2号（[田村淳以視覺系樂隊](../Page/田村淳.md "wikilink")[jealkb一員出演](../Page/jealkb.md "wikilink")）等等。另外，[DOWNTOWN](../Page/DOWNTOWN.md "wikilink")、[今田耕司](../Page/今田耕司.md "wikilink")、[東野幸治](../Page/東野幸治.md "wikilink")、[130R等作為エキセントリック少年ボウイオールスターズ也出演過](../Page/130R.md "wikilink")。

## Young Guns登場的歌手

### 2005年

  - [HIGH and MIGHTY
    COLOR](../Page/HIGH_and_MIGHTY_COLOR.md "wikilink")『PRIDE』（2月18日）
  - [UNDER GRAPH](../Page/UNDER_GRAPH.md "wikilink")『』（2月18日）
  - [YUI](../Page/YUI.md "wikilink")『feel my soul』（2月25日）
  - [SE7EN](../Page/SE7EN.md "wikilink")『光』（3月4日）
  - [K](../Page/K_\(歌手\).md "wikilink")『over...』（3月11日）
  - [湘南乃風](../Page/湘南乃風.md "wikilink")『』（4月22日）
  - [Rie fu](../Page/Rie_fu.md "wikilink")『I Wanna Go To A
    Place...』（4月29日）
  - [奧田美和子](../Page/奧田美和子.md "wikilink")『』（5月27日）
  - [DEPAPEPE](../Page/DEPAPEPE.md "wikilink")『START』（6月10日）
  - [椿屋四重奏](../Page/椿屋四重奏.md "wikilink")『紫陽花』（6月17日）
  - [Def Tech](../Page/Def_Tech.md "wikilink")『My Way』『KONOMAMA』（6月24日）
  - [Younha](../Page/高允河.md "wikilink")『』（9月2日）
  - [BEAT CRUSADERS](../Page/BEAT_CRUSADERS.md "wikilink")『I CAN SEE
    CLEARLY NOW/FEEL』（9月16日）
  - [AAA](../Page/AAA_\(團體\).md "wikilink")『[BLOOD on
    FIRE](../Page/BLOOD_on_FIRE.md "wikilink")』（9月16日）
  - [加藤米莉亞](../Page/加藤米莉亞.md "wikilink")『』（11月4日）

### 2006年

  - [HOME MADE 家族](../Page/HOME_MADE_家族.md "wikilink")『』（1月20日）

  - 『』（1月27日）

  - [安藤裕子](../Page/安藤裕子.md "wikilink")『』（2月10日）

  - [三枝夕夏 IN db](../Page/三枝夕夏_IN_db.md "wikilink")『』（2月17日）

  - [SunSet Swish](../Page/SunSet_Swish.md "wikilink")『』（3月17日）

  - 『SAKURA』（4月21日）

  - [上木彩矢](../Page/上木彩矢.md "wikilink")『』（4月28日）

  - [大和美姬丸](../Page/大和美姬丸.md "wikilink")『』（5月5日）

  - [安潔拉·亞季](../Page/安潔拉亞季.md "wikilink")『This Love』（6月2日）

  - 『』（6月9日）

  - 『』（8月4日）

  - 『雷音』（8月18日）

  - 『Astaire』（8月25日）

  - [風味堂](../Page/風味堂.md "wikilink")『』（9月1日）

  - 『』（9月8日）

  - 『』（11月17日）

### 2007年

  - 『Lovin' Life』（2月2日）

  - 『Possession』（2月23日）

  - 『』（4月6日）

  - [中孝介](../Page/中孝介.md "wikilink")『花』（5月18日）

  - [Stephanie](../Page/Stephanie.md "wikilink")『』

  - 『I'll be there』（6月15日）

  - [松下奈緒](../Page/松下奈緒.md "wikilink")『』（7月13日）

  - [MONKEY MAJIK](../Page/MONKEY_MAJIK.md "wikilink")『』（7月27日）

  - [RSP](../Page/RSP.md "wikilink")『』（8月17日）

### 2008年

  - [Perfume](../Page/Perfume.md "wikilink")「Baby cruising Love」（1月18日）

  - [青山黛瑪](../Page/青山黛瑪.md "wikilink") feat.
    [SoulJa](../Page/SoulJa.md "wikilink")「」（1月25日）

  - [清水翔太](../Page/清水翔太.md "wikilink")「HOME」（2月22日）

  - [Sotte Bosse](../Page/Sotte_Bosse.md "wikilink")「」（3月7日）

  - [福原美穗](../Page/福原美穗.md "wikilink")「CHANGE」

  - [Base Ball
    Bear](../Page/Base_Ball_Bear.md "wikilink")「changes」（5月9日）

  - 「」

  - 「LIFE」（5月30日）

  - [秦基博](../Page/秦基博.md "wikilink")「」（6月6日）

  - [NICO Touches the
    Walls](../Page/NICO_Touches_the_Walls.md "wikilink")「Broken
    Youth」（8月15日）

  - [GIRL NEXT DOOR](../Page/GIRL_NEXT_DOOR.md "wikilink") 「」（9月5日）

  - [flumpool](../Page/flumpool.md "wikilink")「」（10月17日）

  - [SCANDAL](../Page/SCANDAL.md "wikilink")「DOLL」（10月24日）

  - [MiChi](../Page/MiChi.md "wikilink")「PROMiSE」（11月7日）

  - [Lil'B](../Page/Lil'B.md "wikilink")「」

  - 「冒険彗星」（11月28日）

### 2009年

  - monobright「アナタMAGIC」（1月16日）
  - Lego big morl「Ray」
  - Dew 「Thank you」
  - [シド](../Page/SID.md "wikilink")「嘘」（5月1日）
  - 9mm Parabellum Bullet 「Black Market Blues」（6月5日）
  - May J. 「Garden」（6月19日）

### 2016年

  - La PomPon 「運命のルーレット廻して」（3月25日）

## 主題曲

### 現在

  - 片頭曲：[\#1090\~Million
    Dreams\~](../Page/Enigma_\(松本孝弘專輯\).md "wikilink")（2016年3月25日－）
  - 片尾曲：[\#1090\[千夢一夜](../Page/華_\(專輯\).md "wikilink")\]（2002年3月1日－）

<!-- end list -->

  -
    （音樂：[松本孝弘](../Page/松本孝弘.md "wikilink")（[B'z](../Page/B'z.md "wikilink")））

### 過去

  - 節目初期：「LOVE STATION」主唱：[早見優](../Page/早見優.md "wikilink")
  - 節目開始－1990年3月23日：「」音樂：[前田憲男](../Page/前田憲男.md "wikilink")
  - 1990年4月13日－1992年3月20日：「The
    Desire」（原題：「DESIRE,THE＊欲望」）音樂：[横關敦](../Page/横關敦.md "wikilink")
  - 1992年4月10日－2016年3月11日：[\#1090\~Thousand
    Dreams\~](../Page/1090_〜Thousand_Dreams〜.md "wikilink")（片頭曲）音樂：松本孝弘（B'z）
  - 1992年4月10日－2002年2月22日：「」（片尾曲）音樂：[松本孝弘](../Page/松本孝弘.md "wikilink")（[B'z](../Page/B'z.md "wikilink")）、合唱：[大黑摩季](../Page/大黑摩季.md "wikilink")

## 播出回數記念日

| 播出日         | 節目播出回数     |
| ----------- | ---------- |
| 1986年10月24日 | 節目播出第1集    |
| 1988年12月9日  | 節目播出第100集  |
| 1991年4月12日  | 節目播出第200集  |
| 1993年8月27日  | 節目播出第300集  |
| 1995年11月24日 | 節目播出第400集  |
| 1997年12月26日 | 節目播出第500集  |
| 2000年5月5日   | 節目播出第600集  |
| 2002年8月9日   | 節目播出第700集  |
| 2005年2月18日  | 節目播出第800集  |
| 2007年8月3日   | 節目播出第900集  |
| 2010年2月12日  | 節目播出第1000集 |

## 日本國內播映時間

  - 朝日電視台系列（[ANN](../Page/ANN.md "wikilink")）-
    每[星期五](../Page/星期五.md "wikilink")20:00～20:54（2000年4月至9月止為19:54～20:48）
  - [高知放送](../Page/高知放送.md "wikilink")（日本電視台系列）-
    每[星期三](../Page/星期三.md "wikilink")24:29 - 25:24（延遲5日播出）

## 曾經播映的電視台（國內）

  - [山陰放送](../Page/山陰放送.md "wikilink")（[TBS系列](../Page/JNN.md "wikilink")）-
    每[星期三](../Page/星期三.md "wikilink")23:50 -
    24:50（2000年10月\~2013年10月1日播出（延遲5日播出））

## 國外地區播出概況

### 台灣

  - [中視數位台](../Page/中視數位台.md "wikilink") -
    自2000年4月12日至2000年12月20日播映，加註[中文名稱](../Page/中文.md "wikilink")《哈日音樂現場》。
  - [緯來日本台](../Page/緯來日本台.md "wikilink") -
    自2002年11月1日至2013年10月20日播映，播出期間實際節目內容一般比日本方面延遲約一個月。此外，固定於每年[元旦晚間以](../Page/元旦.md "wikilink")「Music
    Station新春豪華版」之名播出[MUSIC STATION SUPER
    LIVE](../Page/MUSIC_STATION_SUPER_LIVE.md "wikilink")，並延續至緯來日本台停播《MUSIC
    STATION》後。

<!-- end list -->

  -
    附註：2011年3月11日發生[東日本大震災後](../Page/東日本大震災.md "wikilink")，《MUSIC
    STATION》在2011年3月18日製作特別企劃。同時，緯來方面也在此時為了震災節目跟進速度，於2011年3月25日播出，此後的集數播出時間皆僅比日本晚2週，但2011年4月30日起恢復以前播出模式。

<!-- end list -->

  - [華視主頻](../Page/華視主頻.md "wikilink") -
    2014年5月4日至2014年6月8日播映。加註中文名稱《超級巨星秀》。
  - [曼迪日本台](../Page/曼迪日本台.md "wikilink") -
    2014年6月17日開始於週日20:00播映，之後改為週日21:00－23:00連播兩集。

### 香港

  - [Animax](../Page/Animax.md "wikilink") - 僅在2007年2月至2008年6月13日間播映。

<!-- end list -->

  -
    原時間：[香港時間逢星期五晚上](../Page/香港時間.md "wikilink")10時，星期六凌晨12時30分、下午2時、晚上9時，星期日下午1時及6時播映。
    頻道所播放的為四週前的節目，並附有中文字幕，但不會播放Music Station Special。如遇停播，頻道會重播之前的集數。

<!-- end list -->

  - [無綫電視](../Page/無綫電視.md "wikilink")[J2台](../Page/J2台.md "wikilink") -
    [香港時間逢星期六](../Page/香港時間.md "wikilink")08:30播映，同日26:35、下星期五15:05重播。頻道播映的為兩週前的節目，並提供隱蔽式繁體中文字幕。

### 中国大陆

  - [中国中央电视-{台}-风云音乐频道](../Page/中國中央電視台風雲音樂頻道.md "wikilink")（数字付费频道）[北京时间每星期六晚上](../Page/北京时间.md "wikilink")8時左右播出约半年之前的节目，日语原声，采用纬来日本台的中文字幕。其他时间该频道也会时常播出往期节目，但时间较不固定。另外，该频道播出的版本经常被观众反映，存在随意剪接的现象，并已暫停播出。

## 相關條目

  - [現場直播](../Page/現場直播.md "wikilink")
  - [J-POP](../Page/J-POP.md "wikilink")
  - [傑尼斯事務所](../Page/傑尼斯事務所.md "wikilink")
  - [The Best Ten](../Page/The_Best_Ten.md "wikilink")
  - [HEY\!HEY\!HEY\!MUSIC
    CHAMP](../Page/HEY!HEY!HEY!MUSIC_CHAMP.md "wikilink")
  - [歌番](../Page/歌番.md "wikilink")

## 參考資料

## 外部連結

  - [朝日電視台《MUSIC STATION》官方網頁](http://www.tv-asahi.co.jp/music/)
  - [緯來日本台《MUSIC
    STATION》官方網頁](http://japan.videoland.com.tw/channel/music_sta/)
  - [香港電視廣播有限公司 J2 台《MUSIC
    STATION》官方網頁](http://programme.tvb.com/music/musicstation/)

## 節目的變遷

[\*](../Category/MUSIC_STATION.md "wikilink")
[Category:朝日電視台音樂節目](../Category/朝日電視台音樂節目.md "wikilink")
[Category:中視外購電視節目](../Category/中視外購電視節目.md "wikilink")
[Category:緯來電視外購節目](../Category/緯來電視外購節目.md "wikilink")
[Category:1986年日本電視節目](../Category/1986年日本電視節目.md "wikilink")
[Category:無綫電視外購節目](../Category/無綫電視外購節目.md "wikilink")
[Category:華視外購電視節目](../Category/華視外購電視節目.md "wikilink")