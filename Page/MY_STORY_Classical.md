《**MY STORY
Classical**》（古典私物語）是日本歌手[濱崎步的混音專輯](../Page/濱崎步.md "wikilink")，2005年3月24日於日本發售。

## 說明

  - 本作以濱崎步第六張原創專輯《[MY
    STORY](../Page/MY_STORY.md "wikilink")》之收錄曲為中心，並以古典音樂方式重新編曲、演奏而成。
  - 本作內音樂是由[佐渡裕所指揮的](../Page/佐渡裕.md "wikilink")[巴黎拉姆爾管絃樂團](../Page/巴黎.md "wikilink")（Concerts
    Lamoureux）負責演奏。

## 收錄歌曲

1.  WONDERLAND
2.  Moments
3.  HAPPY ENDING
4.  GAME
5.  HOPE or PAIN
6.  Kaleidoscope
7.  CAROLS
      -
        Panasonic「LUMIX」FX-7廣告歌曲
8.  walking proud
9.  Catcher In The Light
10. HONEY
11. winding road
12. A Song is born ※Bonus曲目
      -
        此曲是2001年和[globe女主唱](../Page/globe.md "wikilink")[KEIKO的對唱曲](../Page/KEIKO.md "wikilink")。在[愛知縣舉行的](../Page/愛知縣.md "wikilink")[世界博覽會濱崎步演唱了本曲](../Page/2005年世界博覽會.md "wikilink")

[Category:濱崎步專輯](../Category/濱崎步專輯.md "wikilink")
[Category:2005年音樂專輯](../Category/2005年音樂專輯.md "wikilink")