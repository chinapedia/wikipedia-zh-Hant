**Mac OS
8**，是[蘋果電腦公司開發的](../Page/蘋果電腦.md "wikilink")[作業系統](../Page/作業系統.md "wikilink")[Mac
OS的一個版本](../Page/Mac_OS.md "wikilink")，於1997年7月26日發佈。

## Copland

## Mac OS 8.0

Mac OS 8.0 帶來多线程 [Finder](../Page/Finder.md "wikilink")，三維的 Platinum
界面，以及新的電腦幫助（輔助說明）系統。

## Mac OS 8.1

Mac OS 8.1 於 1998年1月19日發佈，最大的一條新聞是全新 HFS Plus（Mac OS 操作系統是對於非 PowerPC
的蘋果電腦的最後一個操作系統，自 8.5 起，要使用 Mac OS 8.5，必須具備 PowerPC Mac。

## Mac OS 8.5

1998年10月17日發佈的 Mac OS 8.5 加上了強大的 Sherlock 程式。

### Mac OS 8.5.1

於 1998年12月7日 發佈的 Mac OS 8.5.1 修改了在 8.5 中的一些問題。

## Mac OS 8.6

蘋果電腦於 1999年5月10日 發佈了 Mac OS 8.6，其穩定是最大的長處。

## Mac OS 8的版本歷史

| 版本    | 發佈日期        | 更新                                          | 使用電腦               | 代號                   | 價格    |
| ----- | ----------- | ------------------------------------------- | ------------------ | -------------------- | ----- |
| 8.0   | 1997年7月26日  | 初始版本                                        | Power Macintosh G3 | Tempo                | US$99 |
| 8.1   | 1998年1月19日  | HFS+ 文件系統                                   | iMac (Bondi Blue)  | Bride of Buster      | 免費更新  |
| 8.5   | 1998年10月17日 | 仅PPC、Sherlock、主题、32位元圖標                     | iMac (Bondi Blue)  | Allegro              | US$99 |
| 8.5.1 | 1998年12月7日  | [當機](../Page/當機.md "wikilink")、記憶體遺失及資料損壞修復 | iMac (5 flavors)   | The Ric Ford Release | 免費更新  |
| 8.6   | 1999年5月10日  | 新的超微内核支持Multiprocessing Services 2.0        | iBook              | Veronica             | 免費更新  |
|       |             |                                             |                    |                      |       |

## 参考资料

[Category:Mac OS](../Category/Mac_OS.md "wikilink")
[Category:已停止開發的作業系統](../Category/已停止開發的作業系統.md "wikilink")
[Category:1997年软件](../Category/1997年软件.md "wikilink")
[Category:微內核](../Category/微內核.md "wikilink")