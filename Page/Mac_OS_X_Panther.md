**Mac OS X v10.3「Panther」** 是[蘋果电脑](../Page/蘋果电脑.md "wikilink")[Mac OS
X](../Page/Mac_OS_X.md "wikilink")[作業系統的第四個主要發行版本](../Page/作業系統.md "wikilink")。它是在[Mac
OS X v10.2](../Page/Mac_OS_X_v10.2.md "wikilink")「Jaguar」之後和[Mac OS X
v10.4](../Page/Mac_OS_X_v10.4.md "wikilink")「Tiger」之前。

蘋果電腦在2003年10月24日發行Panther。蘋果定的銷售價格是，單一使用者授權是$129.00美元，家庭包裝（五個使用者，一個戶口）授權是$199.00美元。

## 新特性

蘋果電腦宣傳Panther擁有至少150個新特色，包含：

  - 更新過的[Finder介面](../Page/Finder.md "wikilink")
      - 髮絲紋金屬質感介面
      - 即時搜尋（比如[iTunes](../Page/iTunes.md "wikilink")）
      - 可自訂的側邊欄位（比如[iTunes](../Page/iTunes.md "wikilink")）

<!-- end list -->

  - [快速使用者切換](../Page/快速使用者切換.md "wikilink")，允許一個使用者維持登入狀態，使用另外一個使用者登入，以動畫的立方體桌面為特色（這個效果只能在有[Quartz
    Extreme的系統看到](../Page/Quartz_Extreme.md "wikilink")）。[Windows
    XP擁有類似的特色但是沒有立方體動畫](../Page/Windows_XP.md "wikilink")。

<!-- end list -->

  - [Exposé](../Page/Exposé_\(Mac_OS_X\).md "wikilink")，是用來幫助使用者管理視窗，特別是很大量視窗的程式。

<!-- end list -->

  - 內建支援[傳真功能](../Page/傳真.md "wikilink")。
  - 內建支援[X11](../Page/Apple_X11.md "wikilink")。
  - [TextEdit支援](../Page/TextEdit.md "wikilink")[Microsoft
    Word文件](../Page/Microsoft_Word.md "wikilink")。
  - 改進與 [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")
    之間相互溝通能力，包含了[SecurID為基礎的](../Page/SecurID.md "wikilink")[VPN支援](../Page/VPN.md "wikilink")。

<!-- end list -->

  - 安全性
      - [FileVault](../Page/FileVault.md "wikilink")：直接針對使用者的目錄作[加密和解密](../Page/加密.md "wikilink")。

<!-- end list -->

  -   - 安全刪除

  - [Xcode開發者工具](../Page/Xcode.md "wikilink")，與[gcc](../Page/gcc.md "wikilink")
    3.3極大地加速了[編譯時間](../Page/編譯.md "wikilink")。

  - [iChat
    AV](../Page/iChat_AV.md "wikilink")，[視訊會議軟體](../Page/視訊會議.md "wikilink")

  - [PDF呈現速度的增快](../Page/PDF.md "wikilink")

  - [檔案標籤](../Page/檔案標籤.md "wikilink")

  - [Pixlet的高解析度](../Page/Pixlet.md "wikilink")[video
    codec](../Page/video_codec.md "wikilink")

## 版本歷史

| 版本     | 建造編號  | 日期          |
| ------ | ----- | ----------- |
| 10.3.0 | 7B85  | 2003年10月24日 |
| 10.3.1 | C107  | 2003年11月10日 |
| 10.3.2 | 7D24  | 2003年12月17日 |
| 10.3.3 | 7F44  | 2004年3月15日  |
| 10.3.4 | 7H63  | 2004年5月26日  |
| 10.3.5 | 7M34  | 2004年8月9日   |
| 10.3.6 | 7R28  | 2004年11月5日  |
| 10.3.7 | 7S215 | 2004年12月15日 |
| 10.3.8 | 7U16  | 2005年2月9日   |
| 10.3.9 | 7W98  | 2005年4月15日  |

## 來源

## 外部連結

  - [Apple: Mac OS X 10.3: Chart of available Mac OS software
    updates](http://docs.info.apple.com/article.html?artnum=25633)
  - [Ars Technica: Mac OS X 10.3
    Panther](http://arstechnica.com/reviews/os/macosx-10.3.ars)

[Category:MacOS](../Category/MacOS.md "wikilink")
[Category:2003年软件](../Category/2003年软件.md "wikilink")
[Category:已停止開發的作業系統](../Category/已停止開發的作業系統.md "wikilink")