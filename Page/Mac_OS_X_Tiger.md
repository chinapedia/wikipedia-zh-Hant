**Mac OS X
v10.4**，开发代号“Tiger”（老虎），是[苹果电脑公司开发的](../Page/苹果电脑公司.md "wikilink")[操作系统](../Page/操作系统.md "wikilink")，于2005年4月29日正式上市销售。

## 概述

Mac OS X v10.4 "Tiger"作为Mac OS X v10.3
"Panther"的继任者，相比前版操作系统有显著功能增强，据官方数据，它包括超过200项新的改进。

Mac OS X
v10.4系统引入了[Spotlight](../Page/Spotlight.md "wikilink")、[Dashboard等新技术](../Page/Dashboard.md "wikilink")，Spotlight类似[Microsoft
Windows平台的Google](../Page/Microsoft_Windows.md "wikilink") Desktop
Search，可以快速搜索计算机中的各种资源。Dashboard是一个易于使用的工具集，只需要按下F12快捷键，即可快速打开。

同时，Mac OS X
v10.4还改进了即时通信程序[iChat](../Page/iChat.md "wikilink")，提供对[Jabber协议的支持](../Page/Jabber.md "wikilink")，改进[万维网](../Page/万维网.md "wikilink")[浏览器](../Page/浏览器.md "wikilink")[Safari](../Page/Safari.md "wikilink")，提供对[RSS技术的支持](../Page/RSS.md "wikilink")，改进[Mail](../Page/Mail.md "wikilink")，提供对邮件的智能分类并加强了邮件的组织能力，增加了辞典程序和[Automator程序](../Page/Automator.md "wikilink")，后者用来自动执行一系列任务而不需编写脚本程序。改进了开发工具[Xcode](../Page/Xcode.md "wikilink")，改进了许多命令行工具，并提供了gcc编译器的最新版本gcc
4.0等。

## 关于英特尔处理器

苹果已经随硬件发布了能够使用[英特尔处理器的Mac](../Page/英特尔.md "wikilink")
OS版本。该版本能够同时兼容[英特尔以及](../Page/英特尔.md "wikilink")[PowerPC的](../Page/PowerPC.md "wikilink")[中央处理器](../Page/中央处理器.md "wikilink")。虽然苹果公司[CEO](../Page/CEO.md "wikilink")[史提夫·賈伯斯在WWDC演讲时谈到所有已有的Mac](../Page/史提夫·賈伯斯.md "wikilink")
OS X的版本都有Intel CPU的版本，但目前最终用户能有用到的只有Mac OS X v10.4

Tiger 于2007年10月被新版[Mac OS X v10.5](../Page/Mac_OS_X_v10.5.md "wikilink")
"Leopard"（美洲豹）取代。

## 歷史

Mac OS X 10.4
"Tiger"最初是在2004年6月28日的WWDC大會上由蘋果公司CEO史蒂夫。賈伯斯的主題演講中曝光的。當年12月，一些非商業開發者將Tiger在[因特網外泄](../Page/因特網.md "wikilink")。2005年4月12日正式宣布Tiger將於4月29日正式發售。全世界的[蘋果專賣店都舉辦了Tiger的研討會](../Page/蘋果專賣店.md "wikilink")，展示會等活動。

2005年6月6日，在[舊金山的WWDC大會上](../Page/舊金山.md "wikilink")，賈伯斯宣布Tiger發布6周以來銷量近200萬
，使之成為蘋果歷史上最成功的[操作系统](../Page/操作系统.md "wikilink")。同時批漏出的消息還有，Mac OS
X在[PowerPC之外支持](../Page/PowerPC.md "wikilink")[英特爾的](../Page/英特爾.md "wikilink")[*x*86線處理器](../Page/X86.md "wikilink")。在2006年6月蘋果宣布計劃支持第一個基於*x*86的計算機，並在2007年6月實現[蘋果英特爾轉換](../Page/苹果的英特尔平台迁移.md "wikilink")。

2006年1月10日 ，蘋果發布了第一臺使用[Intel Core
Duo處理器的臺式一體機](../Page/Intel_Core_Duo.md "wikilink")[iMac和高端筆記本](../Page/iMac.md "wikilink")[MacBook
Pro](../Page/MacBook_Pro.md "wikilink")
，並宣布所有蘋果產品將在2006年末轉向英特爾處理器。之後蘋果發布了高端臺式機[Mac
Pro](../Page/Mac_Pro.md "wikilink")，並於2006年8月8日發布了新的[Xserve](../Page/Xserve.md "wikilink")，在210天內實現了向英特爾的轉換。
比預想的年內完成的計劃快了將近十個月。

10.4也是第一個使用DVD安裝光碟的Mac OS X版本。

## 系统要求

Mac OS X 10.4 升级的系统要求是

  - PowerPC G3, G4，G5或Intel Core 2 Duo处理器
  - 内建 FireWire
  - 256MB 的RAM以上
  - 一个内置显示器或者一个连接到苹果显卡能支持的显示器。
  - 至少 3.0 GB 硬盘空间；若安装Xcode2开发工具，至少需要 4GB硬盘空间。
  - DVD 光驱（安装光盘可以更換成CD）

2006年出场的[英特尔芯片麦金塔电脑内置](../Page/英特尔.md "wikilink")[通用二进制的Mac](../Page/通用二进制.md "wikilink")
OS X 10.4，盒装 10.4升级DVD只能在PowerPC麦金塔电脑上运行。

## 新特性

苹果声称Mac OS X 10.4 Tiger 有200多种新特性，包括：

  - [Spotlight](../Page/Spotlight.md "wikilink") -
    Spotlight是一个强有力的全文本和[元数据搜索引擎](../Page/元数据.md "wikilink")，无论是[Word文档还是](../Page/Microsoft_Word.md "wikilink")[iCal日历](../Page/iCal.md "wikilink")，或者
    [Address Book
    ards](../Page/vCard.md "wikilink")，或者[PDF文件中的任何文本都可以被检索出来](../Page/PDF.md "wikilink")。这个功能也可以用在[Finder中加入](../Page/Macintosh_Finder.md "wikilink")
    *[智能文件夹](../Page/智能文件夹.md "wikilink")*的概念。Spotlight将在文件保存的时候组织归档，所以它能在菜单栏的“随输入而搜索”的文本框中快速准确的找出信息。

<!-- end list -->

  - [iChat AV](../Page/iChat.md "wikilink") - Tiger中新的iChat AV
    3.0裡，将支持最多4位成员参加一个視訊會議，或者10位成员参加音频会议。现在也支持使用
    [Jabber](../Page/Jabber.md "wikilink") 协议。在n Mac OS X Tiger
    Server中包含一个叫*iChat Server*的Jabber服务器
  - [Safari RSS](../Page/Safari.md "wikilink") - Tiger中新的 Safari 2.0
    [Web浏览器有内置的](../Page/Web浏览器.md "wikilink")[RSS阅读器和](../Page/RSS.md "wikilink")[Atom](../Page/Atom_\(standard\).md "wikilink")
    [web syndication](../Page/web_syndication.md "wikilink")
    ，通过浏览器窗口地址栏的RSS按钮即可轻松接入。Mac OS 10.4.3中包含了
    Safari的更新版本，它能通过[Acid2网络标准测试](../Page/Acid2.md "wikilink")。

<!-- end list -->

  - [Mail 2](../Page/Mail_\(application\).md "wikilink") - 新版本 Mail.app
    [email client](../Page/email_client.md "wikilink") 拥有新界面， "智慧型信箱"
    配备了 Spotlight搜索系统，parental 控制，还有一些其它功能。

<!-- end list -->

  - [Dashboard](../Page/Dashboard.md "wikilink") -
    Dashboard是一个新的基于[HTML](../Page/HTML.md "wikilink")，[CSS](../Page/CSS.md "wikilink")，和[JavaScript的小应用程序](../Page/JavaScript.md "wikilink")，作为MacOS的[桌面附件](../Page/桌面附件.md "wikilink")
    。这些附件称为“小玩意”[widget](../Page/Widget_\(computing\).md "wikilink")。常见的有天气，世界时钟，单位换算和词典／语汇工具等等。
    在網路上还有更多的免费工具可供下載使用。

<!-- end list -->

  - [Automator](../Page/Automator.md "wikilink")
    -一个称为*Automator*脚本工具，将应用程序连接到格式复杂的自动流程中（用[AppleScript](../Page/AppleScript.md "wikilink")，[Cocoa](../Page/Cocoa_\(software\).md "wikilink")，或者二者同时编码）。

<!-- end list -->

  - [VoiceOver](../Page/VoiceOver.md "wikilink") -
    VoiceOver是一个辅助用户界面，向用户提供放大选项、键盘操作和用英语语音提示屏幕事件的功能。VoiceOver也可让用户实现语音命令使用应用程序。通过允许多用户给出滚动文字等等的语音指令，它也允许一名用户与其他用户一起在一台Mac机上协同工作。VoiceOver能够将诸如网页、邮件和文字处理等文件的内容朗读出来。完整的键盘导航功能能使用户完全使用键盘，脱离鼠标来操控电脑，此时，会有一个窗口显示所有可用的键盘命令以给用户提供协作。

<!-- end list -->

  - 一个完全内置的
    [词典和](../Page/词典.md "wikilink")[同义语辞典](../Page/同义语辞典.md "wikilink")，基于[牛津美语词典](../Page/牛津.md "wikilink")，可以通过应用程序，Dashboard词典，或者系统命令来调用。

<!-- end list -->

  - [.Mac同步](../Page/.Mac.md "wikilink") -
    虽然这不是一个新功能，但是Tiger中的.Mac同步比Panther有了较大改进。Tiger中同步任务已经不用[iSync程序](../Page/iSync.md "wikilink")，而是直接集成在.Mac系统配置面板中。

<!-- end list -->

  - [QuickTime](../Page/QuickTime.md "wikilink") 7 - 苹果的多媒体软件的最新版本支持新的
    [H.264/AVC](../Page/H.264.md "wikilink")
    编码，能比其他视频编码提供更好质量。该编码也适用于iChat
    AV，可得到更清晰视频会议。Cocoa内部的新级数为Cocoa应用程序提供完整接入QuickTime。与Mac OS X
    捆绑的新QuickTime 7播放器包括改进的音频、视频控制，还有更详尽的信息对话框。新播放器是利用苹果的[Cocoa
    API重新编写的](../Page/Cocoa_\(API\).md "wikilink")，以便利用更方便地新技术的优势。

<!-- end list -->

  - [64位](../Page/64位.md "wikilink")[architecture](../Page/软件构建.md "wikilink")
    - Mac OS X Tiger是第一个支持64位计算的Mac OS X版本，发挥PowerMac
    G5的长处。它也向后兼容[32位程序](../Page/32位.md "wikilink")

<!-- end list -->

  - 新[Unix特性](../Page/Unix.md "wikilink") -
    新版本的[cp](../Page/cp_\(Unix\).md "wikilink")，[mv](../Page/mv.md "wikilink")，和[rsync](../Page/rsync.md "wikilink")，支持带[resource
    fork的文件](../Page/resource_fork.md "wikilink")。还包括支持类似上述Spotlight功能的命令行。

<!-- end list -->

  - [Xcode 2.0](../Page/Xcode.md "wikilink") - Xcode 2.0，苹果的 Cocoa
    发展工具现在包括视觉模型，一个集成的
    [苹果参考库和图像遥控](../Page/苹果参考库.md "wikilink")[debugging](../Page/debugging.md "wikilink")。

<!-- end list -->

  - [Grapher](../Page/Grapher.md "wikilink") -
    Grapher是一个全新的应用程序，能够创建二维和三维的数学函数图像。

<!-- end list -->

  - [Dictionary](../Page/Dictionary_\(software\).md "wikilink") -
    一个基于《新牛津美语词典（[New Oxford American
    Dictionary](../Page/New_Oxford_American_Dictionary.md "wikilink")）》的[词典和](../Page/词典.md "wikilink")[同义语程序](../Page/同义语.md "wikilink")。此程序采用了一种快速的图形界面来显示字典程序本身，并且允许用户使用Spotlight来搜寻单字，打印词语解释，以及将文本复制粘贴到文档之中。Dictionary程序还同样在应用程序菜单中提供了Dictionary服务，另外Cocoa和[WebKit提供了一个全局性的键盘迅捷](../Page/WebKit.md "wikilink")（默认为⌃⌘D）以供显示文本的程序屏幕取词之用。Dictionary应用程序相对于Dictionary
    widget而且功能更为完善。

<!-- end list -->

  - [Quartz Composer](../Page/Quartz_Composer.md "wikilink") - Quartz
    Composer是一个用于处理和渲染图形数据的开发工具。

<!-- end list -->

  - [AU Lab](../Page/AU_Lab.md "wikilink") - AU
    Lab是一个用于测试和混合[音频单元的开发工具](../Page/音频单元.md "wikilink")。

## 改进

  - 采用了更新了的
    [内核结构](../Page/内核.md "wikilink")，优化了内核资源锁定机制，并为[64位](../Page/64位.md "wikilink")[内存](../Page/内存.md "wikilink")[指针和](../Page/指针.md "wikilink")[访问控制列表提供支持](../Page/访问控制列表.md "wikilink")。
  - 采用了称为“[launchd](../Page/launchd.md "wikilink")”的新启动[守护进程](../Page/守护进程.md "wikilink")，使得启动速度有了提升。
  - Tiger的打印对话框中有了一个下拉菜单目录供创造[PDF文件](../Page/PDF.md "wikilink")、将[PDF文件存至](../Page/PDF.md "wikilink")[Mail以供发送以及其他与PDF相关的动作之用](../Page/Mail.md "wikilink")。但这样的用户界面也因创造出了一个外观是按钮，实际上是下拉菜单的混生[widget招致批评](../Page/widget.md "wikilink")。此处是在整个Mac
    OS X界面中，拥有此中混生按钮的三处之一。
  - Dock上的应用程序菜单中如今有了“登录时打开”和“从Dock中移去”的选项。
  - Finder的窗口菜单中有了“循环显示窗口”的菜单项。
  - Finder中项目的“显示简介”窗口现在有了一个叫做“更多信息”的部分，此部分中包括Spotlight的信息标签，如图像的长度和高度，文件的最后打开时间，以及文件所在的文件夹。

## 技术

  - 一套新的图像处理 [API](../Page/应用程序编程接口.md "wikilink")，[Core
    Image](../Page/Core_Image.md "wikilink")，能充分发挥现有的高速显示卡的力量。

<!-- end list -->

  -

      -
        Core Image 使程序员能轻松地将可编程 GPUs
        用于添加特殊效果和图像修正程序中的快速图像处理。包括的图像单元有Blur,
        Color Blending, Generator Filters, Distortion Filters, Geometry
        Filters, Halftone 功能以及其他许多单元。

<!-- end list -->

  - 一套新的数据保存 API, [Core
    Data](../Page/Core_Data.md "wikilink")，使开发人员能够更容易地在他们的应用程序中处理结构化数据。

<!-- end list -->

  -

      -
        Mac OS X [Core Data](../Page/Core_Data.md "wikilink") API
        帮助开发人员为他们的应用程序建立数据结构。Core Data
        为开发人员提供撤销，重复以及保存例程，同时他们不用为此编写任何代码。

<!-- end list -->

  - 一套新的视频 API, [Core Video](../Page/Core_Video.md "wikilink")，借助 Core
    Image 提供实时视频处理。

<!-- end list -->

  -

      -
        Apple 的 [Motion](../Page/Apple_Motion.md "wikilink")
        实时视频特效处理程序利用了 Mac OS X Tiger 中的 [Core
        Video](../Page/Core_Video.md "wikilink") 的优势。 Core Video
        使开发人员能够轻松地将实时视频特效与处理功能与他们的程序进行整合。

<!-- end list -->

  - [Core Audio](../Page/Core_Audio.md "wikilink")，在 Mac OS X 10.3
    Panther 中引入，将一部分与音频相关的功能直接整合进了操作系统。

## 界面差异

蘋果電腦在每次推出 Mac OS X 的升級都會對[图形用户界面作出一些修改](../Page/图形用户界面.md "wikilink")。在
Tiger 的介面上角的選單列的右上角現在多了一個蓝色的 Spotlight 鍵；在 OS X Tiger
的選單列換上一個更閃亮的玻璃式透亮的放大鏡圖案取代10.3的間條圖案。

值得留意的是，10.4 引入一個新的—通常被叫作 'Unified' 的主題。 它是一個自從 Mac OS X 誕生所採用的
non-brushed metal theme 改變的版本。 這個主題融合了視窗的标题栏與工作欄。 其中一個用這個主題的軟件是
[Mail](../Page/Mail_\(application\).md "wikilink")。

## Tiger x86

在 2005 年的 [Worldwide Developers
Conference](../Page/Worldwide_Developers_Conference.md "wikilink")，[斯蒂夫·乔布斯](../Page/斯蒂夫·乔布斯.md "wikilink")
宣報蘋果電腦會開始售賣採 [Intel在](../Page/Intel.md "wikilink")2006年的處理器的 Mac。
為了讓軟件開發者能夠開始為新的 Intel Macs 編寫軟件，蘋果電腦制作了一套包含了可在
[x86](../Page/x86.md "wikilink") 處理器運行的 Mac OS X v10.4 的 Developer
Transition Kits。 它亦被稱為 **Tiger x86** 又或是 **osx86**.

這個版本包括了蘋果電腦的[Rosetta](../Page/Rosetta.md "wikilink") — 一個容許
[英特尔处理器版的OS去運行](../Page/英特尔.md "wikilink")[PowerPC軟體的翻譯器](../Page/PowerPC.md "wikilink")。這就與現有的
Mac OS 9 [Classic](../Page/Classic_\(Mac_OS_X\).md "wikilink")
的模式有很大不同原因是 Mac OS 9 耗用較大量的系統資源。

Developer Transition Kits 開始面世後不久， Tiger x86
被洩露到[檔案共享的網絡上](../Page/檔案共享.md "wikilink")。雖然蘋果電腦在硬件與軟建的轉換時已經採用
[Trusted Computing](../Page/Trusted_Computing.md "wikilink")
[DRM](../Page/Digital_Rights_Management.md "wikilink") 以杜絕 Tiger x86
在非蘋果電腦的 PC 上安裝，但黑客不久仍然能夠解取蘋果電腦的限制。 \[1\]
每當蘋果電腦推出載有防止被安裝於非蘋果的電腦的更新版本的時候，黑客都相應推出他們的版本去繞過蘋果電腦的限制。但在
10.4.6 and 10.4.7
上，黑客版本仍使用10.4.5的核心（[Kernel](../Page/Kernel.md "wikilink")）。

在2006年旧金山的MacWorld 上，乔布斯宣布了Mac OS X
10.4.4可以马上面世。这是第一次向公众发布同时针对PowerPC和Intel
x86的Tiger系统。

## 杂谈

  - 正式发布6周后，苹果发售了200万份拷贝，代表了所有Mac OS
    X用户的16%。到2006年中，苹果预计将有半数Mac用户使用Tiger。苹果声称
    Tiger 是公司历史上最成功的操作系统。
  - 当Dashboard曝光时，曾有人声称他是仿造了第三方软件[Konfabulator](../Page/Konfabulator.md "wikilink")。虽然二者的技术有显著的不同，外观和基本功能还是非常相似的。

## 版本歷史

<table>
<thead>
<tr class="header">
<th><p>版本</p></th>
<th><p>更新</p></th>
<th><p>建造編號</p></th>
<th><p>日期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Mac OS X v10.4</p></td>
<td><p>10.4.0</p></td>
<td><p>8A428</p></td>
<td><p>2005年4月29日</p></td>
</tr>
<tr class="even">
<td><p>10.4.1</p></td>
<td><p>8B15</p></td>
<td><p>2005年5月16日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>10.4.2</p></td>
<td><p>8C64</p></td>
<td><p>2005年7月12日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8E102</p></td>
<td><p>2005年10月12日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>8E45</p></td>
<td><p>2005年10月19日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>8E90</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>10.4.3</p></td>
<td><p>8F46</p></td>
<td><p>2005年10月31日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10.4.4</p></td>
<td><p>8G32 for <a href="../Page/PowerPC.md" title="wikilink">PowerPC</a><br />
8G1165 for <a href="../Page/Intel.md" title="wikilink">Intel</a></p></td>
<td><p>2006年1月10日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>10.4.5</p></td>
<td><p>8H14 for PowerPC<br />
8G1454 for Intel</p></td>
<td><p>2006年2月14日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10.4.6</p></td>
<td><p>8I127 for PowerPC<br />
8I1119 for Intel</p></td>
<td><p>2006年4月3日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>10.4.7</p></td>
<td><p>8J135 for PowerPC<br />
8J2135a for Intel</p></td>
<td><p>2006年6月27日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8K1079</p></td>
<td><p>2006年8月7日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>8N5107</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>10.4.8</p></td>
<td><p>8L127 for PowerPC<br />
8L2127 for Intel</p></td>
<td><p>2006年9月29日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>10.4.9</p></td>
<td><p>8P135 for PowerPC<br />
8P2137 for Intel</p></td>
<td><p>2007年3月13日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10.4.10</p></td>
<td><p>8R218 for PowerPC<br />
8R2218 for Intel</p></td>
<td><p>2007年6月20日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>10.4.11</p></td>
<td><p>8S165 for PowerPC<br />
8S2167 for Intel</p></td>
<td><p>2007年11月14日</p></td>
<td></td>
</tr>
</tbody>
</table>

## 请参阅

  - [Mac OS X](../Page/Mac_OS_X.md "wikilink")
  - [苹果电脑](../Page/苹果电脑.md "wikilink")

## 相关链接

  - [Mac OS
    X 10.4的官方網站](https://web.archive.org/web/20061219023837/http://www.apple.com/macosx/tiger/)

## 參考文獻

[Category:MacOS](../Category/MacOS.md "wikilink")
[Category:2005年软件](../Category/2005年软件.md "wikilink")
[Category:已停止開發的作業系統](../Category/已停止開發的作業系統.md "wikilink")

1.