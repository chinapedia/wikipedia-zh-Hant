**Mac Pro**（相對於[68k Mac](../Page/Macintosh.md "wikilink")、[PPC
Mac](../Page/Power_Macintosh.md "wikilink")、也被稱為**Intel
Mac**）是[蘋果公司推出的高階桌上型電腦系列產品](../Page/蘋果公司.md "wikilink")，搭載[英特爾](../Page/英特爾.md "wikilink")（Intel）[Xeon](../Page/Xeon.md "wikilink")
[CPU以及](../Page/CPU.md "wikilink")[PCI
Express匯流排架構](../Page/PCI_Express.md "wikilink")。

## 概要

蘋果電腦公司在2006年8月7日的全球研發者大會（[WWDC](../Page/WWDC.md "wikilink")）上發表了這台電腦。它是接替[Power
Macintosh的系列產品](../Page/Power_Macintosh.md "wikilink")，與[Xserve同為最後](../Page/Xserve.md "wikilink")[移至Intel
x86平台的蘋果電腦產品系列](../Page/蘋果的英特爾平台遷移.md "wikilink")\[1\]。其[鋁製機殼與](../Page/铝.md "wikilink")[Power
Mac
G5的幾乎相同](../Page/Power_Mac_G5.md "wikilink")，但擴充了[光碟機的插槽](../Page/光碟機.md "wikilink")，以及前後重新排列設計的[I/O連接埠](../Page/I/O.md "wikilink")。不像其他的麥金塔系列產品，Mac
Pro並沒有內建可接收[Apple
Remote訊號的](../Page/Apple_Remote.md "wikilink")[红外线裝置](../Page/红外线.md "wikilink")。

以2006年的標準衡量，Mac
Pro是高階機種，擁有兩個[中央處理器](../Page/中央處理器.md "wikilink")（總共4顆核心）、4個[硬碟位置以及](../Page/硬碟.md "wikilink")8個[記憶體](../Page/記憶體.md "wikilink")（[RAM](../Page/隨機存取記憶體.md "wikilink")）插槽。Mac
Pro的主力規格應該是定在搭載2.66GHz處理器的中階機種。在過去，蘋果電腦公司常會使用「起始於…」（starting
at）或「從…」（from）來說明基本電腦款式的價格，但是這次在蘋果電腦商店（[Apple
Store](../Page/Apple_Store.md "wikilink")）中，價格的標示為「Mac Pro **at**
$2,799」，這是中階規格的價格。但透過自行選擇Mac
Pro的零件，最基本機種從$2299起跳，較為接近以往的[雙核心G](../Page/雙核心.md "wikilink")5機型的價錢，但是相對的也提供了更強大的運算能力。

Mac
Pro的其中一項特色在於不需要任何工具即可拆開機殼進行擴充（螺絲起子僅是備用，以防萬一），而硬碟的安裝也是使用滑入的方式，同樣也不須任何工具。

蘋果已於2013年6月10日的WWDC上發表最新型號的Mac
Pro，外形與以往產品不同，呈黑色圓筒狀，並於同年12月開始銷售。\[2\]新一代Mac
Pro採用[AMD](../Page/AMD.md "wikilink")
[FirePro而不是](../Page/FirePro.md "wikilink")[NVIDIA
Quadro是因為前者的](../Page/NVIDIA_Quadro.md "wikilink")[OpenCL運算能力較佳](../Page/OpenCL.md "wikilink")，這項技術正是由[蘋果公司開發](../Page/蘋果公司.md "wikilink")。\[3\]

2017年推出[iMac Pro](../Page/iMac_Pro.md "wikilink")。

## 規格

### 第一代

<table>
<thead>
<tr class="header">
<th><p>Component</p></th>
<th><p>Intel <a href="../Page/Xeon.md" title="wikilink">Xeon</a> (基於)</p></th>
<th><p>Intel <a href="../Page/Xeon.md" title="wikilink">Xeon</a> (基於)</p></th>
<th><p>Intel <a href="../Page/Xeon.md" title="wikilink">Xeon</a> (基於 and )</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Model</p></td>
<td><p>Mid 2006[4]</p></td>
<td><p>Early 2008[5]</p></td>
<td><p>Early 2009[6]</p></td>
</tr>
<tr class="even">
<td><p><strong>Release date</strong></p></td>
<td><p>August 7, 2006<br />
<span style="color:#969696">April 4, 2007 <em>Optional 3.0 GHz Quad-core Xeon "Clovertown"</em></span></p></td>
<td><p>January 8, 2008</p></td>
<td><p>March 3, 2009<br />
<span style="color:#969696">December 4, 2009 <em>Optional 3.33 GHz Quad-core Xeon "Bloomfield"</em></span></p></td>
</tr>
<tr class="odd">
<td><p><strong>Model Numbers</strong></p></td>
<td><p>MA356*/A</p></td>
<td><p>MA970*/A</p></td>
<td><p>MB871*/A MB535*/A</p></td>
</tr>
<tr class="even">
<td><p><strong>Machine Model</strong></p></td>
<td><p>MacPro1,1<br />
<span style="color:#969696">MacPro2,1 <em>Optional 3.0 GHz Quad-core Xeon "Clovertown"</em></span></p></td>
<td><p>MacPro3,1</p></td>
<td><p>MacPro4,1</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/Extensible_Firmware_Interface.md" title="wikilink">EFI</a> Mode</strong></p></td>
<td><p>EFI32</p></td>
<td><p>EFI64</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong> Default Mode</strong></p></td>
<td><p><a href="../Page/32-bit.md" title="wikilink">32-bit</a></p></td>
<td><p>32-bit in Mac OS X (client), 64-bit in Mac OS X Server</p></td>
<td><p><a href="../Page/64-bit_computing.md" title="wikilink">64-bit</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/Chipset.md" title="wikilink">Chipset</a></strong></p></td>
<td><p><a href="http://www.intel.com/products/server/chipsets/5000x/5000x-overview.htm">Intel 5000X</a></p></td>
<td><p><a href="http://www.intel.com/products/server/chipsets/5400/5400-overview.htm">Intel 5400</a></p></td>
<td><p><a href="http://www.intel.com/products/desktop/chipsets/x58/x58-overview.htm">Intel X58</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/Central_processing_unit.md" title="wikilink">Processor</a></strong></p></td>
<td><p>Two 2.66 GHz (5150) <a href="../Page/Multi-core_processor.md" title="wikilink">Dual-core</a> <a href="../Page/Intel_Xeon.md" title="wikilink">Intel Xeon</a> "<a href="../Page/Woodcrest_(microprocessor).md" title="wikilink">Woodcrest</a>"<br />
<span style="color:#969696"><em>Optional 2.0 GHz (5130), 2.66 GHz or 3.0 GHz (5160) Dual-core or 3.0 GHz (X5365) Quad-core Intel Xeon "<a href="../Page/Clovertown_(microprocessor).md" title="wikilink">Clovertown</a>"</em></span></p></td>
<td><p>Two 2.8 GHz (E5462) <a href="../Page/Multi-core_processor.md" title="wikilink">Quad-Core</a> <a href="../Page/Intel_Xeon.md" title="wikilink">Intel Xeon</a> "<a href="../Page/Harpertown_(microprocessor).md" title="wikilink">Harpertown</a>"<br />
<span style="color:#969696"><em>Optional two 3.0 GHz (E5472) or 3.2 GHz (X5482) Quad-core processors or one 2.8 GHz (E5462) Quad-core processor</em></span></p></td>
<td><p>One 2.66 GHz (W3520) Quad-Core Intel Xeon "<a href="../Page/Bloomfield_(microprocessor).md" title="wikilink">Bloomfield</a>" or two 2.26 GHz (E5520) Quad-core <a href="../Page/Intel_Xeon.md" title="wikilink">Intel Xeon</a> "<a href="../Page/Gainestown_(microprocessor).md" title="wikilink">Gainestown</a>" with 8 MB of L3 cache<br />
<span style="color:#969696"><em>Optional 2.93 GHz (W3540) or 3.33 GHz (W3580) Intel Xeon Quad-core Intel Xeon "Bloomfield" processors or two 2.66 GHz (X5550) or 2.93 GHz (X5570) Quad-core Intel Xeon "Gainestown" processors</em></span></p></td>
</tr>
<tr class="odd">
<td><p><strong>System bus</strong></p></td>
<td><p>1333 MHz</p></td>
<td><p>1600 MHz</p></td>
<td><p>4.8 <a href="../Page/Transfer_(computing).md" title="wikilink">GT/s</a><span style="color:#969696"><em>(Quad-core models only)</em></span> or 6.4 GT/s</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Front-side_bus.md" title="wikilink">Front-side bus</a></p></td>
<td><p><a href="../Page/Intel_QuickPath_Interconnect.md" title="wikilink">QuickPath Interconnect</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/隨機存取記憶體.md" title="wikilink">Memory</a></strong></p></td>
<td><p>1 GB (two 512 MB) of 667 MHz <a href="../Page/DDR2_SDRAM.md" title="wikilink">DDR2</a> <a href="../Page/ECC_memory.md" title="wikilink">ECC</a> fully buffered <a href="../Page/DIMM.md" title="wikilink">DIMM</a><br />
<span style="color:#969696"><em>Expandable to 16 GB (Apple), 32 GB (Actual)</em></span></p></td>
<td><p>2 GB (two 1 GB) of 800 MHz <a href="../Page/DDR2_SDRAM.md" title="wikilink">DDR2</a> <a href="../Page/ECC_memory.md" title="wikilink">ECC</a> fully buffered <a href="../Page/DIMM.md" title="wikilink">DIMM</a><br />
<span style="color:#969696"><em>Expandable to 64 GB</em></span></p></td>
<td><p>3 GB (three 1 GB) for UP quad-core or 6 GB (six 1 GB) for DP 8-core of 1066 MHz <a href="../Page/DDR3.md" title="wikilink">DDR3</a> <a href="../Page/ECC_memory.md" title="wikilink">ECC</a> <a href="../Page/DIMM.md" title="wikilink">DIMM</a><br />
<span style="color:#969696"><em>Expandable to 16 GB on Quad-core models (although expandable to 48GB using 3rd party 3×16GB DIMMs), and 32 GB in 8-core models (128 GB using 3rd party 8x16GB DIMMs, OSX 10.9/Windows)</em></span></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/Computer_graphics.md" title="wikilink">Graphics</a></strong><br />
Expandable to four graphics cards</p></td>
<td><p><a href="../Page/Nvidia.md" title="wikilink">nVidia</a> <a href="../Page/GeForce_7300.md" title="wikilink">GeForce 7300</a> GT with 256 MB of <a href="../Page/GDDR3.md" title="wikilink">GDDR3</a> <a href="../Page/SDRAM.md" title="wikilink">SDRAM</a> (single-link and dual-link <a href="../Page/DVI.md" title="wikilink">DVI</a> ports)<br />
<span style="color:#969696"><em>Optional ATI <a href="../Page/Radeon_X1900.md" title="wikilink">Radeon X1900</a> XT with 512 MB GDDR3 SDRAM (two dual-link DVI ports) or nVidia <a href="../Page/Quadro_FX_4500.md" title="wikilink">Quadro FX 4500</a> with 512 MB GDDR3 SDRAM (stereo 3D and two dual-link DVI ports)</em></span></p></td>
<td><p><a href="../Page/ATI_Technologies.md" title="wikilink">ATI</a> <a href="../Page/Radeon.md" title="wikilink">Radeon</a> <a href="../Page/Radeon_HD_2600.md" title="wikilink">HD 2600</a> XT with 256 MB of GDDR3 SDRAM (two dual-link DVI ports)<br />
<span style="color:#969696"><em>Optional nVidia <a href="../Page/GeForce_8800.md" title="wikilink">GeForce 8800</a> GT with 512 MB GDDR3 SDRAM (two dual-link DVI ports) or nVidia <a href="../Page/Quadro_FX_5600.md" title="wikilink">Quadro FX 5600</a> 1.5 GB (stereo 3D, two dual-link DVI ports)</em></span></p></td>
<td><p>nVidia <a href="../Page/GeForce_GT_120.md" title="wikilink">GeForce GT 120</a> with 512 MB of GDDR3 SDRAM (one <a href="../Page/Mini_DisplayPort.md" title="wikilink">mini-DisplayPort</a> and one dual-link DVI port)<br />
<span style="color:#969696"><em>Optional ATI <a href="../Page/Radeon_HD_4870.md" title="wikilink">Radeon HD 4870</a> with 512 MB of GDDR5 SDRAM (one Mini DisplayPort and one dual-link DVI port)</em></span></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/Hard_drive.md" title="wikilink">Hard drive</a></strong><br />
7200-rpm unless specified</p></td>
<td><p>250 <a href="../Page/gigabyte.md" title="wikilink">GB</a> <a href="../Page/Serial_ATA.md" title="wikilink">Serial ATA</a> with 8 MB cache<br />
<span style="color:#969696"><em>Optional 500 GB with 8 MB cache or 750 GB with 16 MB cache</em></span></p></td>
<td><p>320 GB <a href="../Page/Serial_ATA.md" title="wikilink">Serial ATA</a> with 8 MB cache<br />
<span style="color:#969696"><em>Optional 500 GB, 750 GB, or 1 TB Serial ATA with 16 MB cache or 300 GB Serial Attached <a href="../Page/SCSI.md" title="wikilink">SCSI</a>, 15,000-rpm with 16 MB cache</em></span></p></td>
<td><p>640 GB <a href="../Page/Serial_ATA.md" title="wikilink">Serial ATA</a> with 16 MB cache<br />
<span style="color:#969696"><em>Optional 1 TB or 2 TB Serial ATA drives with 32 MB cache</em></span></p></td>
</tr>
<tr class="even">
<td><p>'''<a href="../Page/Optical_drive.md" title="wikilink">Optical drive</a></p></td>
<td><p>16× <a href="../Page/SuperDrive.md" title="wikilink">SuperDrive</a> with double-layer support (DVD±R DL/DVD±RW/CD-RW)</p></td>
<td><p>18× SuperDrive with double-layer support (DVD±R DL/DVD±RW/CD-RW)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/AirPort#AirPort_Extreme_802.11g_cards.md" title="wikilink">AirPort Extreme</a></strong></p></td>
<td><p>Optional <a href="../Page/IEEE_802.11.md" title="wikilink">802.11a/b/g and draft-n</a> (n disabled by default)</p></td>
<td><p>Optional <a href="../Page/IEEE_802.11.md" title="wikilink">802.11a/b/g and draft-n</a> (n-enabled)</p></td>
<td><p>Built-in <a href="../Page/IEEE_802.11.md" title="wikilink">802.11a/b/g/n</a></p></td>
</tr>
<tr class="even">
<td><p><strong>Maximum Supported Operating System</strong></p></td>
<td><p><a href="../Page/Mac_OS_X_Lion.md" title="wikilink">OS X 10.7</a> "Lion"<br />
<span style="color:#969696"><em>(Unofficially, can run <a href="../Page/OS_X_Mountain_Lion.md" title="wikilink">OS X 10.8</a> "Mountain Lion" and <a href="../Page/OS_X_Mavericks.md" title="wikilink">10.9</a> "Mavericks" with an upgrade to a supported graphics card and EFI64 emulation)[7][8]</em></span></p></td>
<td><p><a href="../Page/OS_X_El_Capitan.md" title="wikilink">OS X 10.11</a> "El Capitan"</p></td>
<td><p><a href="../Page/MacOS_Mojave.md" title="wikilink">macOS 10.14</a> "Mojave" for models that supports Metal API<br />
<a href="../Page/MacOS_High_Sierra.md" title="wikilink">macOS 10.13</a> "High Sierra" for devices without Metal API support</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 第二代

<table>
<tbody>
<tr class="odd">
<td><p><strong>型號</strong></p></td>
<td><p>Late 2013[9]</p></td>
</tr>
<tr class="even">
<td><p><strong>Component</strong></p></td>
<td><p>Intel <a href="../Page/Xeon.md" title="wikilink">Xeon</a> (<a href="../Page/Ivy_Bridge.md" title="wikilink">Ivy Bridge-EP</a>)</p></td>
</tr>
<tr class="odd">
<td><p><strong>推出日期</strong></p></td>
<td><p>2013年12月</p></td>
</tr>
<tr class="even">
<td><p><strong>Model Numbers</strong></p></td>
<td><p>A1481</p></td>
</tr>
<tr class="odd">
<td><p><strong>型號識別元</strong></p></td>
<td><p>MacPro6,1</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/Extensible_Firmware_Interface.md" title="wikilink">EFI</a></strong></p></td>
<td><p>EFI64</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/内核.md" title="wikilink">内核</a></strong></p></td>
<td><p><a href="../Page/64位元.md" title="wikilink">64位元</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/晶片組.md" title="wikilink">晶片組</a></strong></p></td>
<td><p><a href="http://ark.intel.com/zh-tw/products/66243/Intel-BD82C602J-PCH">C602J</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/CPU.md" title="wikilink">CPU</a></strong></p></td>
<td><p>One 3.7 GHz Quad-Core Intel Xeon (E5-1620 v2) with 10MB L3 cache or one 3.5 GHz 6-Core Intel Xeon (E5-1650 v2) with 12MB L3 cache<br />
<span style="color:#969696"><em>Optional one 3.0 GHz 8-Core Intel Xeon (E5-1680 v2) with 25MB L3 cache or one 2.7 GHz 12-Core Intel Xeon (E5-2697 v2) with 30MB L3 cache</em></span></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/I/O总线.md" title="wikilink">系統總線</a></strong></p></td>
<td><p>DMI 2.0 or 2 x 8.0 GT/s <span style="color:#969696"><em>(12-core model only)</em></span></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/DRAM.md" title="wikilink">記億體</a></strong></p></td>
<td><p>四核心型號：12GB（三條4GB）的1866MHz DDR3 ECC記憶體<br />
六核心型號：16GB（四條4GB）的1866MHz DDR3 ECC記憶體<br />
<span style="color:#969696"><em>四核心型號可訂製16GB（四條4GB）、32GB（四條8GB），或64GB（四條16GB）；六核心型號可訂製32GB（四條 8GB）或64GB（四條16GB）</em></span></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/顯示卡.md" title="wikilink">顯示卡</a></strong></p></td>
<td><p>四核心型號：雙AMD FirePro D300繪圖處理器，各配備2GB GDDR5 VRAM<br />
六核心型號：雙AMD FirePro D500繪圖處理器，各配備3GB GDDR5 VRAM<br />
<span style="color:#969696"><em>四核心型號可訂製雙AMD FirePro D500，各配備3GB GDDR5 VRAM；六核心型號可訂製雙AMD FirePro D700，各配備6GB GDDR5 VRAM</em></span></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/儲存裝置.md" title="wikilink">儲存裝置</a></strong></p></td>
<td><p>256GB PCIe快閃儲存<br />
<span style="color:#969696"><em>可訂製512GB或1TB</em></span></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/AirPort#AirPort_Extreme_802.11g_cards.md" title="wikilink">AirPort Extreme</a></strong></p></td>
<td><p>Built-in <a href="../Page/IEEE_802.11.md" title="wikilink">802.11a/b/g/n/ac</a> (up to 1.3 Gbit/s)</p></td>
</tr>
<tr class="odd">
<td><p><strong>最高支援作業系統</strong></p></td>
<td><p><a href="../Page/MacOS_Mojave.md" title="wikilink">macOS 10.14</a> "Mojave"</p></td>
</tr>
</tbody>
</table>

## 資料來源

<references />

## 外部連結

  -
  - [近距離拍攝的Mac
    Pro照片](http://www.engadget.com/2006/08/07/apple-mac-pro-hands-on/)

[Category:麦金塔](../Category/麦金塔.md "wikilink")
[Category:2006年面世的產品](../Category/2006年面世的產品.md "wikilink")

1.  2006年8月7日[全球研發者大會](../Page/WWDC.md "wikilink")（[WWDC](../Page/WWDC.md "wikilink")）上的簡報。
2.  [Apple - Mac Pro](http://www.apple.com/hk/mac-pro/)
3.
4.
5.
6.
7.
8.
9.