《**Magic
Cyndi**》[臺灣](../Page/臺灣.md "wikilink")[歌手](../Page/歌手.md "wikilink")[王心凌的第五張](../Page/王心凌.md "wikilink")[錄音室專輯](../Page/錄音室專輯.md "wikilink")。共有兩版，一版為Magic
Cyndi，於2007年4月30日正式發行。「Magic
Cyndi」，共有10首歌以及加贈純真輕熟豪華尺寸寫真海報。預購附送粉红爱神与酷黑精灵双封面歌词写真。一版為「Magic
Cyndi（衣櫥的秘密冠軍影音CD+DVD）」，在2007年6月2日發行。王心凌在此專輯中首次作曲「衣櫥的秘密」。

## 曲目

## 音樂錄影帶

<table style="width:25%;">
<colgroup>
<col style="width: 6%" />
<col style="width: 5%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th><p>歌名</p></th>
<th><p>導演</p></th>
<th><p>附註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>愛的天靈靈</p></td>
<td><p><a href="../Page/賴偉康.md" title="wikilink">賴偉康</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>那年夏天寧靜的海</p></td>
<td><p><a href="../Page/黃中平.md" title="wikilink">黃中平</a></p></td>
<td><p><a href="../Page/阮經天.md" title="wikilink">阮經天擔任男主角</a></p></td>
</tr>
<tr class="odd">
<td><p>原來這才是真的你</p></td>
<td><p><a href="../Page/林錦和.md" title="wikilink">林錦和</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>衣櫥的秘密</p></td>
<td><p>黃中平</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>蝴蝶</p></td>
<td><p>賴偉康<br />
<a href="../Page/周小民.md" title="wikilink">周小民</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>迷你電影</p></td>
<td></td>
<td><p>畫面剪輯自〈那年夏天寧靜的海〉漏網片段</p></td>
</tr>
</tbody>
</table>

[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")
[Category:台灣音樂專輯](../Category/台灣音樂專輯.md "wikilink")
[Category:2007年音樂專輯](../Category/2007年音樂專輯.md "wikilink")
[Category:王心凌音樂專輯](../Category/王心凌音樂專輯.md "wikilink")