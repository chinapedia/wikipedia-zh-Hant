《**Magical☆Antique**》（），是日本[Leaf公司制作](../Page/Leaf.md "wikilink")，2000年4月28日发行的一款[戀愛模擬類型](../Page/戀愛模擬.md "wikilink")[成人遊戲](../Page/日本成人遊戲.md "wikilink")。\[1\]

## 故事簡介

## 登場人物

  -
    本作男主角。

  -
    來自魔法世界「グエンディーナ」的少女。

  -
    スフィー的妹妹。

  -
    健太郎的青梅足馬，喫茶店「HONEY BEE」的[看板娘](../Page/看板娘.md "wikilink")。

  -
    高倉財閥的大小姐。

  -

## 主題曲

  - 片頭曲「Littlestone」\[2\]
    作詞：須谷尚子　作曲：米村高廣　歌：菅原露見
  - 片尾曲「歩み」
    作詞：須谷尚子　作曲：下川直哉　編曲：松岡純也　歌：AKKO

## 製作人員

  - 企劃、劇情：[椎原旬](../Page/椎原旬.md "wikilink")
  - 支線劇情：[高橋龍也](../Page/高橋龍也.md "wikilink")、[雨之幸永](../Page/雨之幸永.md "wikilink")
  - 角色設計及原畫：
  - 原畫協力：[水無月徹](../Page/水無月徹.md "wikilink")
  - 角色原畫：
  - 背景：[木村隆夫](../Page/木村隆夫.md "wikilink")
  - CG：[木村隆夫](../Page/木村隆夫.md "wikilink")、、[閂夜明](../Page/閂夜明.md "wikilink")、[柏木和宏](../Page/柏木和宏.md "wikilink")
  - 音樂：[米村高廣](../Page/米村高廣.md "wikilink")、[石川真也](../Page/石川真也.md "wikilink")、[中上和英](../Page/中上和英.md "wikilink")、[下川直哉](../Page/下川直哉.md "wikilink")
  - 程式：[中島祐治](../Page/中島祐治.md "wikilink")、[中尾佳祐](../Page/中尾佳祐.md "wikilink")
  - 製作人：[下川直哉](../Page/下川直哉.md "wikilink")
  - 制作及發售：[Leaf](../Page/Leaf.md "wikilink")
  - 販售：[株式会社 AQUA PLUS](../Page/株式会社_AQUA_PLUS.md "wikilink")

## 參考來源

## 外部链接

  - [Leaf公司官方网站](http://leaf.aquaplus.co.jp/)
  - [Magical☆Antique官方网站](http://leaf.aquaplus.jp/product/ma/)

[Category:Leaf](../Category/Leaf.md "wikilink")
[Category:2000年日本成人遊戲](../Category/2000年日本成人遊戲.md "wikilink")
[Category:美少女遊戲](../Category/美少女遊戲.md "wikilink")
[Category:经营模拟游戏](../Category/经营模拟游戏.md "wikilink")
[Category:Windows遊戲](../Category/Windows遊戲.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:月刊GFantasy](../Category/月刊GFantasy.md "wikilink")
[Category:魔法少女題材遊戲](../Category/魔法少女題材遊戲.md "wikilink")

1.  [Magical☆Antique](https://vndb.org/v186)The Visual Novel Database
2.  [まじかる☆アンティーク](http://erogetrailers.com/soft/6448)ErogeTrailers