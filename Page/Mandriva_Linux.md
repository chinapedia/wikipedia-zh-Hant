**Mandriva
Linux**是一个由[Mandriva开发的](../Page/Mandriva.md "wikilink")[Linux发行版](../Page/Linux.md "wikilink")，它使用[RPM包管理器及](../Page/RPM.md "wikilink")[KDE
SC](../Page/KDE_Software_Compilation_4.md "wikilink")、[GNOME等桌面环境](../Page/GNOME.md "wikilink")。每个发布版本提供12个月的桌面软件更新，以及18个月的基础组件更新。

Mandriva
Linux的前身为[欧洲最大的](../Page/欧洲.md "wikilink")[Linux厂商](../Page/Linux.md "wikilink")，[法国](../Page/法国.md "wikilink")[Mandriva公司所擁有的](../Page/Mandriva.md "wikilink")**Mandrake
Linux**。 2015年5月26日Business Inside報導Mandriva公司已經正式宣告結束營運\[1\]。

## 历史发展

1998年7月发布的最初发行版是基于[Red Hat Linux](../Page/Red_Hat_Linux.md "wikilink")
5.1和[KDE](../Page/KDE.md "wikilink") 1.0。后来它与Red
Hat分道扬镳，并加入了一些原有的工具用以方便配置系统。Mandriva
Linux由[Gaël
Duval所创建](../Page/Gaël_Duval.md "wikilink")，其目标是使得新用户更容易使用Linux。\[2\]。Mandrake项目是世界上第一个为非技术类用户设计的易于使用，安装和管理的linux发行版本。\[3\]

Mandrake
Linux早期方便的字体安装工具和默认的中文支持，为Linux普及做出了很大的贡献。但是2004年前后Mandrakesoft陷入财务危机，濒临破产。公司于2005年2月24日与[拉丁美洲最大的Linux厂商](../Page/拉丁美洲.md "wikilink")[Conectiva达成了收购协议](../Page/Conectiva.md "wikilink")，金额为170万[欧元](../Page/欧元.md "wikilink")，约合223万[美元](../Page/美元.md "wikilink")，以[股票形式交易](../Page/股票.md "wikilink")，新公司[Mandriva旗下品牌Mandrake](../Page/Mandriva.md "wikilink")
Linux更名为Mandriva Linux。但是Mandrakesoft的创始人Gaël Duval，却于2006年被新公司解雇\[4\]。

## 名字的由来

从创立开始一直到8.0版，Mandrake将其品牌名称一直为Linux-Mandrake。从8.1版到9.2版，其名称为Mandrake
Linux。

在2004年2月，MandrakeSoft输掉了与[King
Features集团的所有者](../Page/King_Features.md "wikilink")[赫斯特公司之间的官司](../Page/Hearst.md "wikilink")。赫斯特认为，MandrakeSoft的Mandrake魔术样式商标对King
Features集团的商标造成了侵权。作为预防措施，MandrakeSoft将其产品名称作了更改，将品牌名字和产品名称之间的空白字符去掉，并将产品名称的首字母改为小写，因此而成为了一个单词。从10.0版本开始，Mandrake
Linux便成为现在熟悉的Mandrakelinux，而它的品牌标志也因此更改。同样地，[MandrakeMove也改为了Mandrakemove](../Page/MandrakeMove.md "wikilink")。

在2005年4月，Mandrakesoft发布公告称，该公司与[巴西的](../Page/巴西.md "wikilink")[Conectiva](../Page/Conectiva.md "wikilink")、[法国的](../Page/法国.md "wikilink")[Edge
IT和](../Page/Edge_IT.md "wikilink")[美国的](../Page/美国.md "wikilink")[Lycoris收购合并](../Page/Lycoris.md "wikilink")。
由于这次收购行动以及和赫特斯公司之间的法律纠纷，使得Mandrakesoft宣布新公司的名称是Mandriva，而Mandriva
Linux也成为新产品的名称。\[5\]

## 特点

以下是Mandriva Linux的主要特点

### 安装、控制及管理

Mandriva Linux包含了[Mandriva Control
Center](../Page/drakconf.md "wikilink")，这是用于易于进行系统设置的控制中心。它有许多像Deakes或Draks这样的众所周知的程序组成，并被统一命名为**drakxtools**，用于配置更多不同的系统设置。例如*MouseDrake*用来配置鼠标参数，*DiskDrake*用于设置磁盘分区，*drakconnect*用于配置网络连接。这些都是使用[GTK+和](../Page/GTK+.md "wikilink")[Perl所编写的](../Page/Perl.md "wikilink")，大多数的这类程序都能够在图形模式和命令行字符模式下使用[ncurses界面运行](../Page/ncurses.md "wikilink")。

### 桌面

Mandriva Linux使用[KDE
SC或](../Page/KDE_Software_Compilation_4.md "wikilink")[GNOME作为标准桌面](../Page/GNOME.md "wikilink")，但实际上它还包括了其它的像[Xfce](../Page/Xfce.md "wikilink")、[twm这样的桌面系统](../Page/twm.md "wikilink")。

### 主题

它有一个提供了[软件与](../Page/软件.md "wikilink")[桌面环境保持一致的独特的](../Page/桌面环境.md "wikilink")[主题](../Page/主题.md "wikilink")。
Mandrake银河桌面背景在9.1版出现，而Mandrake银河II则出现于10.0，作为桌面背景。Mandrake银河的一个变体是"Mandrake平方银河"，这是使用[平方窗口按钮而不是以往的](../Page/平方.md "wikilink")[圆方形](../Page/圆.md "wikilink")。一个叫做"Ia
Ora"的新的默认主题则出现于Mandriva Linux 2007正式版中，但"银河"依旧作为可选的喜好设置保留在系统中。

Mandriva以[rpm作为软件管理工具](../Page/rpm.md "wikilink")，部分兼容[Red Hat
Linux](../Page/Red_Hat_Linux.md "wikilink")／[Fedora
Core的预编译包](../Page/Fedora_Core.md "wikilink")。

  - 方便：
  - 高效：
  - 华丽：

Mandriva Linux 目前的发行周期约为半年，在每年的4月和10月发行一个新的版本。

## 版本

| 年份   | 版本     | 名稱                         |
| ---- | ------ | -------------------------- |
| 1998 | 5.1    | Venice                     |
| 1998 | 5.2    | Leeloo                     |
| 1999 | 5.3    | Festen                     |
| 1999 | 6.0    | Venus                      |
| 1999 | 6.1    | Helios                     |
| 2000 | 7.0    | Air                        |
| 2000 | 7.1    | Helium                     |
| 2000 | 7.2    | Odyssey(前稱Ulysses)         |
| 2001 | 8.0    | Traktopel                  |
| 2001 | 8.1    | Vitamin                    |
| 2002 | 8.2    | Bluebird                   |
| 2002 | 9.0    | Dolphin                    |
| 2003 | 9.1    | Bamboo                     |
| 2003 | 9.2    | FiveStar                   |
| 2004 | 10.0   | Community and Official     |
| 2004 | 10.1   | Community                  |
| 2004 | 10.1   | Official                   |
| 2005 | 10.2   | Limited Edition 2005       |
| 2005 | 2006.0 | Mandriva Linux 2006        |
| 2006 | 2007   | Mandriva Linux 2007        |
| 2007 | 2007.1 | Mandriva Linux 2007 Spring |
| 2007 | 2008.0 | Mandriva Linux 2008        |
| 2008 | 2008.1 | Mandriva Linux 2008 Spring |
| 2008 | 2009.0 | Mandriva Linux 2009        |
| 2009 | 2009.1 | Mandriva Linux 2009 Spring |
| 2009 | 2010.0 | Mandriva Linux 2010        |
| 2010 | 2010.1 | Mandriva Linux 2010 Spring |
| 2010 | 2010.2 |                            |
| 2011 | 2011.0 | Mandriva 2011 (Hydrogen)   |
|      |        |                            |

## 参见

  - [Linux发行版列表](../Page/Linux发行版列表.md "wikilink")
  - [Mageia](../Page/Mageia.md "wikilink")

## 资料来源

<references />

## 外部链接

  - [Mandriva_Linux的官方网站](https://web.archive.org/web/20150524002157/http://www.mandriva.com/en/)

  -
[Category:Linux](../Category/Linux.md "wikilink")
[Category:KDE](../Category/KDE.md "wikilink")

1.  [1](http://www.businessinsider.com/mandriva-goes-out-of-business-2015-5)

2.

3.

4.
5.