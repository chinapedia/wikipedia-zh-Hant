**Matroska**（）是一種[多媒體封裝格式](../Page/多媒體封裝格式.md "wikilink")，這個封裝格式可把多種不同[編碼的](../Page/編碼.md "wikilink")[影像及](../Page/影像.md "wikilink")16條或以上不同格式的[音頻和](../Page/音頻.md "wikilink")[語言不同的](../Page/語言.md "wikilink")[字幕封裝到一個Matroska](../Page/字幕.md "wikilink")
Media檔內。它也是其中一種[開放原始碼的多媒體封裝格式](../Page/開放原始碼.md "wikilink")。很多人把Matroska當作為mkv，其實mkv只是Matroska媒體系列的其中一種檔案格式。

俄文是（[俄羅斯套娃](../Page/俄罗斯套娃.md "wikilink")）的誤讀，因為Matroska的工作原理就跟層層套疊的俄羅斯娃娃一樣，是「愈包愈緊」的，故得名。

## 檔案類型

Matroska媒體定義了三種類型的檔：

1.  '''MKV '''（Matroska Video
    File）：[視訊檔](../Page/視訊.md "wikilink")，可以包含[音訊和](../Page/音訊.md "wikilink")[字幕](../Page/字幕.md "wikilink")；
2.  '''MKA '''（Matroska Audio
    File）：單一的音訊檔，可以有多條及多種類型的[音軌](../Page/音軌.md "wikilink")；
3.  '''MKS '''（Matroska Subtitles）：[字幕文件](../Page/字幕.md "wikilink")。

這三種文件中以MKV最為常見。

## 開發歷史

该项目发起于2002年12月6日\[1\]，[派生自](../Page/复刻_\(软件工程\).md "wikilink")[多媒體封裝格式](../Page/多媒體封裝格式.md "wikilink")（）。在此之前，MCF
的主开发者 Lasse Kärkkäinen 曾与 Steve Lhomme
就使用[可扩展二进制元语言](../Page/可扩展二进制元语言.md "wikilink")（）还是自己的二进制格式发生过争吵，Steve
随后创建了 Matroska 项目。\[2\]与此同时 MCF 的主开发者因为服军役而中断开发达6个月，导致社区大部分开发者都转向了新项目。

2010年，[WebM](../Page/WebM.md "wikilink") 音视频格式发布，它基于 Matroska 容器的一个采用
[VP8](../Page/VP8.md "wikilink") 视频和
[Vorbis](../Page/Vorbis.md "wikilink") 音频的预设。\[3\]

2014年10月31日，[微软公司确认](../Page/微软公司.md "wikilink") [Windows
10](../Page/Windows_10.md "wikilink") 将「开箱即用」地支持
[HEVC](../Page/High_Efficiency_Video_Coding.md "wikilink") 和
Matroska。\[4\]\[5\]

## 目標

采用 [EBML](../Page/多媒體封裝格式.md "wikilink") 使得 Matroska
的格式未来可以继续扩充。Matroska 团队曾在
[Doom9.org](../Page/Doom9.org.md "wikilink")
等论坛上表达过他们的一些长远目标（以下「目标」并不代表现有特性）：\[6\]

  - 建立一个现代、灵活、可扩展、跨平台的多媒体容器格式
  - 开发强壮的[流媒体支持](../Page/流媒体.md "wikilink")
  - 基于 EBML 开发一套类似于 DVD 的菜单系统
  - 开发一套用于创建和编辑 Matroska 文件的工具
  - 开发一系列[函数库](../Page/库_\(计算机\).md "wikilink")，以便别的开发者可以在他们的应用程序里添加
    Matroska 支持
  - 与硬件生产商合作，在嵌入式多媒体设备内置 Matroska 支持
  - 致力于提供不同[操作系统和不同硬件平台上的原生](../Page/操作系统.md "wikilink") Matroska 支持

## 特點

Matroska最大的特點就是能容納多種不同類型的影像編碼、音頻編碼及字幕流，並且它能把非常高密的[RealMedia及](../Page/RealMedia.md "wikilink")[QuickTime檔案也容納在内](../Page/QuickTime.md "wikilink")，同時將它們的音頻和影像重新組織起來，從而達到一個更好和鮮明的效果。

Matroska的開發是對多種傳統[媒體格式的一次大挑戰](../Page/媒體格式.md "wikilink")，雖則如此，Matroska也被開發成一個多功能的多媒體容器。

此外，根據網上資料的研究顯示，MKV比普通的影片格式如[AVI更為優異](../Page/AVI.md "wikilink")。

以下列表是MKV與[AVI的研究列表](../Page/AVI.md "wikilink")：

<div style="text-align: center;">

| 格式  | 錯誤檢測 | 可變[幀率](../Page/幀率.md "wikilink") | 內建多組可選字幕 | 多[音軌](../Page/音軌.md "wikilink") | 串流傳輸 | 選單  | 非[微軟](../Page/微軟.md "wikilink")[作業系統](../Page/作業系統.md "wikilink") |
| --- | ---- | -------------------------------- | -------- | ------------------------------- | ---- | --- | ----------------------------------------------------------------- |
| MKV | 有    | 支援                               | 支援       | 支援                              | 支援   | 支援  | 支援                                                                |
| AVI | 沒有   | 不支援                              | 不支援      | 不支援                             | 不支援  | 不支援 | 不夠支援                                                              |
|     |      |                                  |          |                                 |      |     |                                                                   |

</div>

## 播放

現時播放Matroska這類格式並不需要專用的播放器，基本上任何播放器都可以播放MKV檔。在[微軟](../Page/微軟.md "wikilink")[作業系統下](../Page/作業系統.md "wikilink")，一般解碼方法，是通過[DirectShow分流器](../Page/DirectShow.md "wikilink")*（DirectShow
Filters）*，「直接地」把[視訊流及](../Page/視訊流.md "wikilink")／或[音訊流以](../Page/音訊流.md "wikilink")[DirectX輸出至](../Page/DirectX.md "wikilink")[硬件的](../Page/硬件.md "wikilink")[驅動程序](../Page/驅動程序.md "wikilink")。

例如常見的：

  - [VLC media player](../Page/VLC_media_player.md "wikilink")
  - [Media Player Classic](../Page/Media_Player_Classic.md "wikilink")
  - [GOM Player](../Page/GOM_Player.md "wikilink")
  - [rat DVD](../Page/rat_DVD.md "wikilink")
  - [Zoom Player](../Page/Zoom_Player.md "wikilink")
  - [BS Player](../Page/BS_Player.md "wikilink")
  - [KMPlayer](../Page/KMPlayer.md "wikilink")
  - [DivX Player](../Page/DivX_Player.md "wikilink")
  - [PotPlayer](../Page/PotPlayer.md "wikilink")

## 参见

  - [媒體播放器列表](../Page/媒體播放器列表.md "wikilink")
  - [媒體播放器比較](../Page/媒體播放器比較.md "wikilink")
  - [MKVToolNix](../Page/MKVToolNix.md "wikilink")

## 参考资料

## 外部連結

  - [Matroska in Hydrogenaudio.org
    wiki](http://wiki.hydrogenaudio.org/index.php?title=Matroska)
  - [MKV編碼程式](https://web.archive.org/web/20060517090926/http://packs.matroska.org/)
  - [MKV剪輯器](https://web.archive.org/web/20040707171532/http://haali.cs.msu.ru/mkv/)
  - [Mac下的MKV转换器](https://web.archive.org/web/20111226031217/http://www.mkvconverterformac.com/)
  - [SourceForge](http://sourceforge.net/projects/guliverkli/)
  - [Linux和Windows相關的MKV工具](http://www.bunkus.org/videotools/mkvtoolnix/)

[Category:音频格式](../Category/音频格式.md "wikilink")
[Category:音频编解码器](../Category/音频编解码器.md "wikilink")
[Category:免費軟件](../Category/免費軟件.md "wikilink")
[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:视频文件格式](../Category/视频文件格式.md "wikilink")
[Category:俄羅斯發明](../Category/俄羅斯發明.md "wikilink")

1.
2.
3.  [*Frequently Asked
    Questions*](http://www.webmproject.org/about/faq/), the
    [WebM](../Page/WebM.md "wikilink") project
4.
5.
6.  <http://www.matroska.org/technical/whatis/index.html>