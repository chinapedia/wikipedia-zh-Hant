**MaxDB**是[SAP公司的一种兼容](../Page/SAP公司.md "wikilink")[ANSI](../Page/ANSI.md "wikilink")
[SQL-92的](../Page/SQL-92.md "wikilink")[关系数据库管理系统](../Page/关系数据库管理系统.md "wikilink")，在2003年至2007年间，[MySQL公司也曾参与过该数据库](../Page/MySQL公司.md "wikilink")。MaxDB适用于如[mySAP
Business
Suite的大型SAP环境](../Page/mySAP_Business_Suite.md "wikilink")，以及其它需企业级数据库功能的应用环境。MaxDB能够持续运行T字节范围内的数据。

## 历史

数据库的开发始于1977年，起先是作为[柏林工业大学的一个研究项目](../Page/柏林工业大学.md "wikilink")。80年代初它成为一种数据库产品，先后被[利多富电脑公司](../Page/利多富电脑公司.md "wikilink")、[西门子利多富资讯系统](../Page/西门子利多富资讯系统.md "wikilink")、[Software
AG拥有](../Page/Software_AG.md "wikilink")，如今它属于SAP公司。其间，它曾被命名为VDN、Reflex、Supra
2、DDB/4、Entire SQL-DB-Server和ADABAS。1997年，SAP公司从Software
AG购下该软件并为其命名为SAP
DB。2000年10月起，在[GPL下SAP](../Page/GPL.md "wikilink")
DB的附加代码开放。2003年，SAP公司与MySQL公司建立合作伙伴关系，并将SAP DB重新命名为MaxDB。

2007年10月，SAP停止了与MySQL公司在数据库销售与服务方面的合作，对数据库的销售和服务提供权也由SAP收回\[1\]。现在，MaxDB的开发、发布和支持由SAP公司来完成。MaxDB的源代码也不再遵循GPL而公开。SAP同时声明：“MaxDB在非SAP环境下的使用，对此SAP是否提供进一步的商业支持，仍在讨论中。”\[2\]

7.5版的MaxDB是对7.4版的SAP DB的代码直接的改进。因此，7.5版可被用于早先7.2.04及以上版本的SAP DB的升级。

SAP对MaxDB有一套完整的质量保证流程，确保之后顺利在SAP解决方案中的发布，或在['SAP
Network'](https://web.archive.org/web/20081022085614/https://www.sdn.sap.com/irj/sdn/maxdb)上供下载。

## 特性

MaxDB包括一系列的管理和开发工具。这些工具大多都基于[图形用户界面](../Page/图形用户界面.md "wikilink")，并有[命令行界面副本](../Page/命令行界面.md "wikilink")。它提供对[JDBC](../Page/JDBC.md "wikilink")、[ODBC](../Page/ODBC.md "wikilink")、[SQLDBC](../Page/SQLDBC.md "wikilink")、Precompiler、[PHP](../Page/PHP.md "wikilink")、[Perl](../Page/Perl.md "wikilink")、[Python](../Page/Python.md "wikilink")、[WebDAV的接口](../Page/WebDAV.md "wikilink")，[OLE
DB](../Page/OLE_DB.md "wikilink")、[ADO](../Page/ADO.md "wikilink")、[DAO](../Page/DAO.md "wikilink")、[RDO和](../Page/Remote_Data_Objects.md "wikilink")[.NET则可通过](../Page/.NET_Framework.md "wikilink")[ODBC与MaxDB连接](../Page/ODBC.md "wikilink")，[Delphi与](../Page/Delphi.md "wikilink")[TCL可通过第三方程序接口与MaxDB连接](../Page/TCL.md "wikilink")。MaxDB可[跨平台工作](../Page/跨平台.md "wikilink")，有[HP-UX](../Page/HP-UX.md "wikilink")、[AIX](../Page/AIX.md "wikilink")、[Linux](../Page/Linux.md "wikilink")、[Solaris](../Page/Solaris.md "wikilink")、[Windows
2000](../Page/Windows_2000.md "wikilink")、[Windows Server
2003和](../Page/Windows_Server_2003.md "wikilink")[Windows
XP的版本](../Page/Windows_XP.md "wikilink")。SAP用户可在SAP产品页面上查到能与MaxDB配套使用的产品的平台的详细可用信息。

### 特色

MaxDB提供内置的热备份，而不需要任何的在线重组，且在条目级兼容SQL-92。当前，一个开发目标是“零管理”以降低TCO成本。MaxDB用较低的硬件需求实现了优越的[线上交易处理性能](../Page/线上交易处理.md "wikilink")。

## 未来版本

下一版本的MaxDB将被命名为MaxDB 7.7.00。

在7.7.00版中，Multiversion Concurrency Control（MVCC）技术可能会被用来替代当前的锁机制。

## 许可

从7.2至7.6版的MaxDB遵循GPL。编程界面遵循GPL，不过也有部分工程遵循开放源码许可。

7.3和7.4版的SAP DB遵循[LGPL](../Page/LGPL.md "wikilink")。

7.5和7.6版的MaxDB提供双重许可，包括GPL和商业许可，7.5版已停止维护\[3\]。

7.5和7.6系列后续版本的MaxDB的开发由开源社区完成，SAP公司也曾作出过贡献。\[4\]

2007年10月，SAP承担MaxDB的全部销售和商业支持。目前，7.6版的MaxDB属于[专有软件](../Page/专有软件.md "wikilink")，非SAP程序仍供免费使用，但是不提供支持，同时对使用也有一定限制。MaxDB在非SAP环境中使用的商业支持正在考虑中。

## 参考

<div class="references-small">

<references/>

</div>

## 参见

  - [关系数据库管理系统列表](../Page/关系数据库管理系统列表.md "wikilink")

## 外部链接

  - [官方网站](https://www.sdn.sap.com/irj/sdn/maxdb)
  - [MaxDB
    Wiki](https://www.sdn.sap.com/irj/sdn/wiki?path=/display/MaxDB/MaxDB)

[Category:開源資料庫管理系統](../Category/開源資料庫管理系統.md "wikilink")
[Category:跨平台軟體](../Category/跨平台軟體.md "wikilink")

1.  [MySQL AB :: MySQL AB to Optimize its Open Source Database for SAP
    NetWeaver](http://www.mysql.com/news-and-events/press-release/release_2007_40.html)
2.  [*MaxDB back under the SAP
    roof\!*](https://www.sdn.sap.com/irj/sdn/weblogs?blog=/pub/wlg/7514)

3.  <https://www.sdn.sap.com/irj/sdn/maxdb?rid=/webcontent/uuid/30501665-8e9b-2a10-9fa9-cedf4c9d2095>

4.  <https://wiki.sdn.sap.com/wiki/display/MaxDB/FAQ#FAQ-WhatistherelationshipofMaxDBandSAPDBandopensource%3F>