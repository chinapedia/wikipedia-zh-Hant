**Maxima**是一种用[LISP编写的](../Page/LISP.md "wikilink")[计算机代数系统](../Page/计算机代数系统.md "wikilink")（Computer
Algebra
System），用于公式推导和符号计算，它是一套[自由软件](../Page/自由软件.md "wikilink")，在[GNU通用公共许可证下發行](../Page/GNU通用公共许可证.md "wikilink")。它由[麻省理工学院在美国能源部的支持下于](../Page/麻省理工学院.md "wikilink")60年代末创造的[Macsyma演变而来](../Page/Macsyma.md "wikilink")。Macsyma後來慢慢走上商業化的道路，自1982年开始，[Bill
Schelter教授默默地维护基于能源部获得的代碼的Macsyma](../Page/Bill_Schelter.md "wikilink")，他把這個軟體叫做Maxima，開放源码。因為版權的問題，Maxima一直没有公開發行，-{只}-有少數人知道有這個軟體的存在。1998年，Maxima終於得到公開發行的許可，這已是Schelter教授努力了16年之後的事。Schelter教授在2001年去世，不過已經正式成為合法開放源碼軟體，因此陸續有支持開放源碼的程式設計師，學者投入Maxima的開發工作。

Maxima的前身Macsyma在當時是非常創新的軟體。現在流行的商業[计算机代数系统軟體Maple及Mathematica](../Page/计算机代数系统.md "wikilink")，都是受到Macsyma的啟發而設計出來的。

## 產品功能

  - 符號運算
  - 各種基礎數學函數庫
  - 複數和指定精度數
  - 利用gnuplot進行2維及3維函數繪圖
  - 符號遞歸運算
  - 可編程
  - 可計算文檔，支持插入，標題單元，章節單元，備註，輸入單元，文字單元，圖片
  - 匯出LaTeX格式
  - 載入Package
  - 微積分運算
  - 矩陣運算
  - 三角函數展開化簡缩併
  - 上千頁的手冊

## 版本历史

Maxima的原始碼可在Linux, Mac OS X，及Windows下編譯。
[Maxima已经发布了如下Maxima版本](../Page/Maxima.md "wikilink")

  - Maxima 5.41 2017年10月3日

## 界面

Maxima原本是純文字界面，這在數學式子的顯示上就沒有Maple或Mathematica等軟體來得美觀。不過Maxima也有幾種圖形界面。第一個選擇是使用GNU的[TeXmacs](../Page/TeXmacs.md "wikilink")。TeXmacs是一套所見即所得的文書處理程式，可以很方便的編輯數學式子。它同時也提供許多數學軟體一個美觀的界面，Maxima就是其中之一。其他可能的選擇還有[wxMaxima](../Page/wxMaxima.md "wikilink")，[imaxima等等](../Page/imaxima.md "wikilink")。

## 特殊功能

變數及函式名稱自動補齊: 在命令列按下Ctrl-TAB鍵，Maxima會提示或自動補齊變數，函式或檔案的名稱。

## Maxima語言

  - 常用數學

返回x以上最小的整數

    ceiling(x);

高斯地板函數:返回x一下最大的整數

    floor(x);

返回最接近的整數

    round(x);

操作分子分母

    denom(p/q);
    num(p/q);

ev賦值

    (%i1) f: a*x^k+b*x+c$
    (%i2) ev(f, a=1, b=2, c=3, k=2,)

比較大小

    compare(%e^%pi,%pi^%e）;
    compare(1^999,1^99);
    compare(%pi,%e);

  - 2D繪圖

指定值域

    wxplot2d([%], [x,-5,5], [y,-2,2])$

對數尺度繪圖

    wxplot2d([%], [x,-5,5], [logx])$

  - 迴圈

<!-- end list -->

    for i=1 thou 20
    do (if mod(i,3)=0
         then print(i));

    series: 1$
    term: exp(sin(x))$
     for p: 1 unless p>7 do
     (term:diff(term, x) /p,
     series: series + subst(x=0, term)*x^p)$
     series;

  - 矩陣

M . M k次

    M^^k

  - 其他

搜尋手冊

    ? x

模糊搜索

    ?? x

數學式轉換LaTeX

    tex(%);

## 相似軟體

  - [Maple](../Page/Maple.md "wikilink")
  - [MATLAB](../Page/MATLAB.md "wikilink")
  - [Mathematica](../Page/Mathematica.md "wikilink")
  - [GNU Octave](../Page/GNU_Octave.md "wikilink")
  - [Scilab](../Page/Scilab.md "wikilink")
  - [Sage](../Page/Sage.md "wikilink")

## 外部链接

  -
  - [Maxima入门介绍](https://web.archive.org/web/20060509180941/http://dsec.pku.edu.cn/~yuhj/wiki/maxima.html)（簡）

  - [Maxima手冊（英）](http://maxima.sourceforge.net/docs/manual/maxima.html)

[Category:Common Lisp软件](../Category/Common_Lisp软件.md "wikilink")
[Category:Linux计算机代数系统软件](../Category/Linux计算机代数系统软件.md "wikilink")
[Category:MacOS计算机代数系统软件](../Category/MacOS计算机代数系统软件.md "wikilink")
[Category:Windows计算机代数系统软件](../Category/Windows计算机代数系统软件.md "wikilink")
[Category:自由跨平台軟體](../Category/自由跨平台軟體.md "wikilink")
[Category:自由计算机代数系统](../Category/自由计算机代数系统.md "wikilink")
[Category:自由教育軟件](../Category/自由教育軟件.md "wikilink")
[Category:用Lisp語言編輯的自由軟件](../Category/用Lisp語言編輯的自由軟件.md "wikilink")
[Category:使用wxWidgets的软件](../Category/使用wxWidgets的软件.md "wikilink")