****是國際通用的[無線電通話](../Page/無線電.md "wikilink")。該訊號通常為[航海](../Page/航海.md "wikilink")、[航空器遇到危及性命的緊急情況時呼救所用](../Page/航空.md "wikilink")，但個別地區的[警察](../Page/警察.md "wikilink")、[消防](../Page/消防員.md "wikilink")、[交通相關人員亦以此作為求援訊號](../Page/交通.md "wikilink")。發出訊號時必須連呼三次（“Mayday,
Mayday, Mayday”）避免誤聽、混淆或噪音干擾等情況，以及將真正的求援訊號與其他只是字面上描述到求援訊號的正常通訊作出區分。

## 歷史

「Mayday」作為求救訊號為[英國人莫克佛](../Page/英國人.md "wikilink")（Federick Stanley
Mockford）所創。1923年他於[克羅伊登機場任高級](../Page/克羅伊登機場.md "wikilink")[無線電通訊員](../Page/無線電.md "wikilink")，機場要求他提出一個簡單明瞭的單字給所有機師及地勤人員作為緊急求救訊號。當時該機場的大部分航線為來往[法國](../Page/法國.md "wikilink")[巴黎-勒布尔热机场](../Page/巴黎-勒布尔热机场.md "wikilink")，故他取法文“venez
m'aider（速來援助）”\[1\]的“m'aider”改為音近的[英文單詞](../Page/英文.md "wikilink")“Mayday”作為提議。

## 使用

當船隻、航機遇上即時的嚴重危難、威脅人命安全、無法自救等十萬火急，需要立即宣告周知相關單位並獲取救援時，應發出Mayday求救訊號。例如船舶正在沉沒、遇劫、發生大火、爆炸；或航空器因鳥擊、機械故障、急遽失壓、引擎失效等足以擴大危害飛行的緊急情況時，都應該使用。

Mayday求救是透過無線電通話發出。船隻或飛機在任何頻道發出Mayday訊號都可被聽懂。世界各地海事救援中心、航空管制中心、[海巡部隊等專責救援組織](../Page/海巡.md "wikilink")，都有二十四小時專人監聽特定的專用遇險[頻道](../Page/頻道.md "wikilink")。海事救援使用的是[中頻](../Page/中頻.md "wikilink")2182千[赫及海事](../Page/赫.md "wikilink")[甚高頻](../Page/甚高頻.md "wikilink")（VHF）第十六频道（156.8
MHz）；航空使用的是121.5 MHz及243.0 MHz;業餘無線電使用145.0及431.0 MHz。

發出Mayday訊號時，先呼叫三次（Mayday, Mayday,
Mayday）；然後說出船隻／飛機代號，描述自己位置，所遇上的險狀，船上／飛機上的人數，將作何打算，以及需要哪種協助。說完一遍後應重複一次，然後等待回覆。兩分鐘後若沒有收到回覆應再次重複。

在海上，當救援中心收到Mayday訊號，可能會派出飛機或船隻到場搜救。附近的船隻亦可能改道往現場協助。在無線電通話內發出Mayday，相同於在無線電電報上發出[SOS](../Page/SOS.md "wikilink")，或在地上撥緊急電話。發出虛假求救訊號在大部分的地方都是刑事罪行，因為這樣可能危及救援者的性命，亦浪費資源和其它救援機會。在美國，發出虛假求救訊號是違反聯邦法例，可被判處六年監禁及罰款250,000美元。

如果船隻因為某些原因（例如無線電損壞）而不能發出Mayday訊號，亦可用其他方法求救。其他船隻亦可代發Mayday求救。一般船隻若聽到其他船隻發出Mayday，而兩分鐘內未有聽到有關當局回覆，應嘗試代為傳遞Mayday求救。

## 其他訊號

### Pan-Pan

Pan-Pan（法文*panne*，故障的意思）是另一種緊急求援訊號，表示比Mayday較為次要的緊急狀況。例如船隻或飛機出現故障引致失控，被海盜襲擊，或有人需要即時醫療协助。使用時同樣是先呼叫三次（Pan-Pan,
Pan-Pan, Pan-Pan）。有些時候搜救機構亦會以Pan-Pan代表發報緊急通知。

### Silence

法語發音\[si'lɑ̃s\]；英文寫成Seelonce，發音\[si:'lɔns\]。發出求救或作出回應的救援機構可能使用以下訊號：**Seelonce
Mayday**（**Silence, M'aider**）或**Seelonce Distress**（**Silence
Détresse**）：（法文：靜默－求救）宣佈該頻只供求救的船隻，對答的救援機構或协助救援的船隻使用，其他一般通訊暫停使用（[無線電靜默](../Page/無線電靜默.md "wikilink")），直至宣佈**Seelonce
feenee**（**Silence fini**，法文：靜默－完結）。

而航空管制中另有宣佈[無線電靜默以及解除靜默狀態的信號](../Page/無線電靜默.md "wikilink")。**Stop
Transmitting—Mayday**或**Stop
Transmitting—Distress**（意為“停止發話，有遇險活動”）用於通知所轄其他航空器停止通話，以保證只有遇險航空器和搜救機構使用該頻率。**Distress
Traffic Ended**則等效于“Silence fini”，意為“遇險狀態結束”。

## 注释

## 參考

  -
  - [1980年船隻撞斷美國佛羅里達州Skyway橋後向海岸巡邏隊發出Mayday求救訊號的錄音](http://www.islander.org/5-11-05/skyway.mp3)

[Category:無線電](../Category/無線電.md "wikilink")
[Category:应急管理](../Category/应急管理.md "wikilink")

1.  正常语法中“M'aider”不單獨使用。而且法語內，“Venez
    m'aider.”也不代表特別緊急，在法語中一般人高叫「救命」時多數會說的是“À
    l'aide\!”或“Au secours\!”。