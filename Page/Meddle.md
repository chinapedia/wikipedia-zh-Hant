《》是於1971年[英國](../Page/英國.md "wikilink")[前衛搖滾團](../Page/前衛搖滾.md "wikilink")[平克·弗洛伊德的專輯](../Page/平克·弗洛伊德.md "wikilink")。本專輯錄音於1971年的一月與八月，在同年的10月30日於美國由Capitol公司發行，在11月於英國由EMI公司發行。Meddle在1973年的十月獲頒金唱片認證，在1994年3月榮獲RIAA白金以及雙白金的認證。重新混音的版本於1994年8月發行於歐洲，95年5月發行於美國。

## 曲目

*Meddle*有六首歌，最後一首歌整整佔了一張[黑膠唱片](../Page/黑膠唱片.md "wikilink")。

## 參與人員

  - [David Gilmour](../Page/David_Gilmour.md "wikilink") -
    [吉他](../Page/吉他.md "wikilink")、[貝斯吉他](../Page/貝斯吉他.md "wikilink")、[和聲以及](../Page/和聲.md "wikilink")[音帶效果](../Page/音帶效果.md "wikilink")。
  - [Roger Waters](../Page/Roger_Waters.md "wikilink") -
    貝斯吉他」主唱、音帶效果以及伴奏吉他。
  - [Richard Wright](../Page/Richard_Wright.md "wikilink") -
    [鍵盤](../Page/鍵盤.md "wikilink")、音帶效果以及和聲。
  - [Nick Mason](../Page/Nick_Mason.md "wikilink") -
    [鼓](../Page/鼓.md "wikilink")、[敲擊樂器](../Page/敲擊樂器.md "wikilink")、音帶效果以及One
    of These Days中的和聲。

另見：

  - [Seamus](../Page/Seamus.md "wikilink")（[Steve
    Marriott](../Page/Steve_Marriott.md "wikilink")'的狗。）

## 評論

儘管專輯中所傳達的情緒極其多樣，大多樂迷認為*Meddle*比起前張專輯 *[Atom Heart
Mother](../Page/Atom_Heart_Mother.md "wikilink")*更有自身的主體性，更像是一張成熟的專輯。*Meddle*的頭兩首歌透過風的聲音效果得以不間斷地連續起來，在往後的[月之暗面與](../Page/月之暗面.md "wikilink")
*[Wish You Were
Here](../Page/Wish_You_Were_Here_\(album\).md "wikilink")*都有著同樣的表現。*Meddle*另一個有趣的地方在於同時包括了團體所致做的「最好」（"Echos"）與「最差」的歌曲（"Seamus"）。

## 引言

> Meddle 一直是我的最愛。我的意思是，對我來說，這是Pink Floyd真正向前邁進的起點。

  -
    —David Gilmour, 1988年2月，在 Australian Radio。

## 銷售排行

**專輯** - [Billboard](../Page/Billboard_magazine.md "wikilink")（北美）

| 年份   | 排行榜        | 排名 |
| :--- | :--------- | :- |
| 1971 | Pop Albums | 70 |
|      |            |    |

## 外部連結

  - [Pink Floyd
    *Meddle*的歌詞](http://www.pink-floyd-lyrics.com/html/meddle-lyrics.html)

[Category:1971年音樂專輯](../Category/1971年音樂專輯.md "wikilink")
[Category:平克·弗洛伊德音樂專輯](../Category/平克·弗洛伊德音樂專輯.md "wikilink")
[Category:搖滾音樂專輯](../Category/搖滾音樂專輯.md "wikilink")
[Category:前衛搖滾音樂專輯](../Category/前衛搖滾音樂專輯.md "wikilink")
[Category:英國音樂專輯](../Category/英國音樂專輯.md "wikilink")
[Category:国会唱片音乐专辑](../Category/国会唱片音乐专辑.md "wikilink")
[Category:EMI Records音乐专辑](../Category/EMI_Records音乐专辑.md "wikilink")