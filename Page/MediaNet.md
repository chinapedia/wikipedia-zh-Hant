是[日本](../Page/日本.md "wikilink")[東京電視台的](../Page/東京電視台.md "wikilink")[子公司](../Page/子公司.md "wikilink")，其製作的[動畫主要供應給東京電視台的主頻道及其附屬日本動畫頻道](../Page/動畫.md "wikilink")[AT-X播放](../Page/AT-X.md "wikilink")。該公司製作的動畫至今已播放的有二十套，包括、及等。

## 沿革

1978年4月1日，東京電視台的前身成立。1981年10月1日，東京12頻道改名為東京電視台，東京12節目販賣改名為。1997年12月24日，東京電視MediaNet創辦的專業動畫頻道[AT-X開播](../Page/AT-X.md "wikilink")。1999年10月，東京電視MediaNet吸收合併「東京電視軟體」（，簡稱為「SOFTX」）。2000年6月26日，東京電視MediaNet與東京電視台合資成立。2000年9月，東京電視MediaNet將AT-X經營權移交株式會社AT-X。

## 代表作

  - [神秘的世界](../Page/神秘的世界.md "wikilink")（原名：神秘の世界エルハザード）
  - [巴比倫2世](../Page/巴比倫2世.md "wikilink")（原名：バビル2世）
  - [零戰士](../Page/零戰士.md "wikilink")（原名：コスモウォーリアー零）
  - [幻魔大戰](../Page/幻魔大戰.md "wikilink")（原名：幻魔大戦）
  - [魔獸戰線](../Page/魔獸戰線.md "wikilink")（原名：魔獣戦線）
  - [寵物小精靈](../Page/寵物小精靈.md "wikilink")（亞洲）（原名：ポケットモンスター）
  - [男女蹺蹺板](../Page/男女蹺蹺板.md "wikilink")
  - [哈姆太郎](../Page/哈姆太郎.md "wikilink")（原名：とっとこハム太郎）
  - [星夢美少女](../Page/星夢美少女.md "wikilink")（原名：カレイドスター）
  - [魔偵探洛基](../Page/魔偵探洛基.md "wikilink")（原名：魔探偵ロキ）
  - [守護甜心\!](../Page/守護甜心!.md "wikilink")
  - [戀愛班長](../Page/戀愛班長.md "wikilink")
  - [寶石寵物 Tinkle☆](../Page/寶石寵物.md "wikilink")

## 相關網頁

  - [MediaNet官方網頁](http://www.medianet.co.jp)

[Category:电视制作公司](../Category/电视制作公司.md "wikilink")
[Category:東京電視台](../Category/東京電視台.md "wikilink")
[Category:1978年成立的公司](../Category/1978年成立的公司.md "wikilink")
[Category:港區公司 (東京都)](../Category/港區公司_\(東京都\).md "wikilink")