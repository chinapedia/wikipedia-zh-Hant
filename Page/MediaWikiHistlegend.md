<div id="histlegend" style="border-style: solid; background-color: #f9f9f9; border-color: #e9e9e9;  border-width: 1px; font-size: 90%; margin-top: 2px; margin-bottom: 2px; padding: 0 5px 5px 5px; clear: both">

选择下列任何一个版本的日期点击可以浏览。需要更多帮助请参看[Help:页面历史和](../Page/Help:页面历史.md "wikilink")[Help:编辑摘要](../Page/Help:编辑摘要.md "wikilink")。

<div class="hlist inline">

  - <span class="plainlinks">\[ 页面信息\]</span>
  - <span class="plainlinks">\[ 过滤器运作记录\]</span> | 外部工具：
  - \[<http://vs.aka-online.de/cgi-bin/wppagehiststat.pl?lang=zh.wikipedia&page=>
    修订历史统计\]
  - \[<http://wikipedia.ramselehof.de/wikiblame.php?article=>\&lang=\&user_lang={{\#switch:|zh|zh-cn|zh-my|zh-sg=zh-hans|zh-hk|zh-mo|zh-tw=zh-hant|\#default=}}
    搜索编辑历史\]
  - \[//tools.wmflabs.org/pageviews/?start={{\#time:Y-m-d|-31
    day}}\&end={{\#time:Y-m-d|-1
    day}}\&project=zh.wikipedia.org\&platform=all-access\&agent=user\&pages=
    本月页面浏览统计\]
  - \[//tools.wmflabs.org/iabot/index.php?page=runbotsingle\&pagesearch=
    修复死链\]
    </div>

-----

（）= 与最后修订版本的差别，（）= 与前一个修订版本的差别，**** =
[小修改](../Page/Help:小修改.md "wikilink")，→ =
[章节编辑](../Page/Help:章节#章节的编辑.md "wikilink")，← =
[自动编辑摘要](../Page/WP:AES.md "wikilink")

</div>