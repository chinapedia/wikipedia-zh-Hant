<div class="plainlinks" id="noarticletext" style="padding: 7px;">

{{\#ifeq:||

<div class="infobox" id="sisterproject" style="width: 20em; font-size: 90%;float:right;padding: 0.5em; background:white;">

查询维基百科[姊妹计划上的](../Page/Special:SiteMatrix.md "wikilink")****：

<span>

<table style="background:none, align:center" cellpadding="1" cellspacing="0">

<tr>

<td align="center" >

[Wiktionary-logo-zh.png](https://zh.wikipedia.org/wiki/File:Wiktionary-logo-zh.png "fig:Wiktionary-logo-zh.png")

</td>

<td>

[维基词典](../Page/wikt:Special:Search/{{PAGENAME}}.md "wikilink")（多语言字典和词典）

</td>

</tr>

<tr>

<td align="center" >

[Wikibooks-logo.svg](https://zh.wikipedia.org/wiki/File:Wikibooks-logo.svg "fig:Wikibooks-logo.svg")

</td>

<td>

[维基教科书](../Page/b:Special:Search/{{PAGENAME}}.md "wikilink")（教科书和手册）

</td>

</tr>

<tr>

<td align="center" >

[Wikiquote-logo.svg](https://zh.wikipedia.org/wiki/File:Wikiquote-logo.svg "fig:Wikiquote-logo.svg")

</td>

<td>

[维基语录](../Page/q:Special:Search/{{PAGENAME}}.md "wikilink")（名人名言的集锦）

</td>

</tr>

<tr>

<td align="center" >

[Wikisource-logo.svg](https://zh.wikipedia.org/wiki/File:Wikisource-logo.svg "fig:Wikisource-logo.svg")

</td>

<td>

[维基文库](../Page/s:Special:Search/{{PAGENAME}}.md "wikilink")（自由的图书馆）

</td>

</tr>

<tr>

<td align="center" >

[Commons-logo.svg](https://zh.wikipedia.org/wiki/File:Commons-logo.svg "fig:Commons-logo.svg")

</td>

<td>

[维基共享资源](../Page/commons:Special:Search/{{PAGENAME}}.md "wikilink")（共享的多媒体数据库）

</td>

</tr>

<tr>

<td align="center" >

[Wikinews-logo.svg](https://zh.wikipedia.org/wiki/File:Wikinews-logo.svg "fig:Wikinews-logo.svg")

</td>

<td>

[维基新闻](../Page/n:Special:Search/{{PAGENAME}}.md "wikilink")（自由的新闻源）

</td>

</tr>

<tr>

<td align="center" >

[Wikispecies-logo.svg](https://zh.wikipedia.org/wiki/File:Wikispecies-logo.svg "fig:Wikispecies-logo.svg")

</td>

<td>

[维基物种](../Page/Wikispecies:Special:Search/{{PAGENAME}}.md "wikilink")（物种目录）

</td>

</tr>

<tr>

<td align="center" >

[Wikivoyage-Logo-v3-icon.svg](https://zh.wikipedia.org/wiki/File:Wikivoyage-Logo-v3-icon.svg "fig:Wikivoyage-Logo-v3-icon.svg")

</td>

<td>

[维基导游](../Page/voy:Special:Search/{{PAGENAME}}.md "wikilink")（自由的旅行指南）

</td>

</tr>

<tr>

<td align="center" >

[Wikiversity-logo-41px.png](https://zh.wikipedia.org/wiki/File:Wikiversity-logo-41px.png "fig:Wikiversity-logo-41px.png")

</td>

<td>

[维基学院](../Page/v:Special:Search/{{PAGENAME}}.md "wikilink")（自由的研习社群）

</td>

</tr>

</table>

</span>

</div>

}} {{\#ifeq:|User
talk|这位用户的讨论页尚未贴有**留言**。|**维基百科目前还没有与上述标题相同的{{\#switch:
|=条目 |=[讨论页](../Page/{{ns:Help}}:讨论页.md "wikilink")
|=[分类](../Page/{{ns:Project}}:分类.md "wikilink")
|=[帮助页](../Page/{{ns:Help}}:内容.md "wikilink")
|=[主题](../Page/{{ns:Project}}:主题.md "wikilink")
|=[模板](../Page/{{ns:Project}}:模板消息.md "wikilink")
|=[用户页](../Page/{{ns:Project}}:用户页.md "wikilink")
|=[图像页](../Page/{{ns:Project}}:图像.md "wikilink") |= 計劃页 |页
}}。**{{\#ifeq:||{{\#switch:
|=请先在维基百科上[查找“”是否已有名称相近或不同文字写法的条目](../Page/{{ns:Special}}:Search/{{PAGENAME}}.md "wikilink")。
|=请浏览[现有的分类来检查您需要的分类是否使用了其他的名称](../Page/{{ns:Project}}:探索.md "wikilink")。
|=请浏览[现有的帮助页来检查您需要的帮助主题是否使用了其他的名称](../Page/{{ns:Help}}:内容.md "wikilink")。
|=请浏览[现有的主题来检查是否已有相似的主题](../Page/{{ns:Portal}}:首页.md "wikilink")。
|=请浏览[现有的模板来检查您需要的模板是否使用了其他的名称](../Page/{{ns:Project}}:模板消息.md "wikilink")。
|=通常来说，这一页只应由[:来创建和编辑](../Page/{{ns:User}}:{{PAGENAME}}.md "wikilink")。如果您对此抱有疑惑，请验证用户“”<span class="plainlinksneverexpand">\[//zh.wikipedia.org/wiki/Special:Listusers?username=\&limit=1\&group=
**是否已被注册**\]</span>。 }}
|在创建这一页面前，请详阅[:子页面](../Page/{{ns:Project}}:子页面.md "wikilink")。}}}}

  - **\[ {{\#switch:|User
    talk=给“”留言|开始编辑“”{{\#if:|页面|条目}}}}\]**{{\#switch:|=，你也可以使用[创建條目向导](../Page/Wikipedia:創建條目精靈.md "wikilink")，或者[向其他编者请求这个条目](../Page/{{ns:Project}}:条目请求.md "wikilink")}}。

{{\#switch:|User talk= |\*
在现有{{\#if:|页面|条目}}中[搜索“”](../Page/{{ns:Special}}:Search/{{PAGENAME}}.md "wikilink")。

  - [搜索链接到这个标题的页面](../Page/{{ns:Special}}:Whatlinkshere/{{FULLPAGENAME}}.md "wikilink")。}}

<div id="noarticletext_technical">

-----

**可能出现此提示的其他原因：**

  - 如果您刚刚创建了{{\#switch:|Image=这一图像|这一页面}}，有可能是数据库太忙而没来得及更新；请等待几分钟并尝试\[
    刷新缓存\]。

<!-- end list -->

  - 在维基百科中，标题是**大小写敏感**（首字母除外）且不能**繁简混用**的；请检查是否有相似的标题存在。如果您没有发现混用繁简但却依然不能指向正确的标题，可以向我们[提交错误报告](../Page/{{ns:Project}}:字词转换.md "wikilink")。

<!-- end list -->

  - 还有可能是{{\#switch:|Image=这一图像|这一页面}}已被删除。请检查**\[
    删除日志\]**和[Wikipedia:删除方针上的一些理由](../Page/Wikipedia:删除方针.md "wikilink")。

{{\#ifeq:||

}}

</div>

</div>