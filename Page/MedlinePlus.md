**MedlinePlus**與**MedlinePlus醫學百科**（**MedlinePlus Medical
Encyclopedia**）是一個存放有关健康与医疗訊息的網站，內容來自屬於[美國](../Page/美國.md "wikilink")[國家衛生研究院的](../Page/國家衛生研究院_\(美國\).md "wikilink")[美國國家醫學圖書館](../Page/美國國家醫學圖書館.md "wikilink")（United
States National Library of Medicine）。除此之外，也提供了線上醫學辭典，以及藥物索引、醫學新聞報導等服務。

## 参见

  - [醫學分類](../Page/醫學分類.md "wikilink")
  - [疾病資料庫](../Page/疾病資料庫.md "wikilink")

## 參考來源

  - "MedlinePlus Health Information" (home page, overview), MedlinePlus
    website, U.S. National Library of Medicine, 8600 Rockville Pike,
    Bethesda, MD, and National Institutes of Health, USA, October 2007,
    webpage: [MedlinePlus-Gov](http://medlineplus.gov)。

<!-- end list -->

  - "MedlinePlus Medical Encyclopedia: Basal cell carcinoma"
    (symptoms/treatment), *MedlinePlus Medical Encyclopedia*, U.S.
    National Library of Medicine, Bethesda, MD, and National Institutes
    of Health, USA, October 2007, webpage:
    [BCC](http://www.nlm.nih.gov/medlineplus/ency/article/000824.htm)。

## 外部連結

  -
[Category:生物資訊資料庫](../Category/生物資訊資料庫.md "wikilink")
[Category:線上資料庫](../Category/線上資料庫.md "wikilink")