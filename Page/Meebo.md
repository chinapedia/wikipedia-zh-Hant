**Meebo**是一個用[AJAX編寫的網上](../Page/AJAX.md "wikilink")[即時通訊軟體](../Page/即時通訊.md "wikilink")，並與主要的即時通訊軟體，如：[AOL](../Page/AOL_Instant_Messenger.md "wikilink")、[ICQ](../Page/ICQ.md "wikilink")、[Yahoo\!
Messenger](../Page/Yahoo!_Messenger.md "wikilink")、[MSN
Messenger](../Page/MSN_Messenger.md "wikilink")、[Jabber及](../Page/Jabber.md "wikilink")[Google
Talk相容的系統](../Page/Google_Talk.md "wikilink")，使用戶免除安裝即時通訊軟體之餘，就能與其他人保持聯繫。

Meebo服務於2005年9月4日開始運作，並於9月14日正式開放與公眾使用。

2012年6月5日Meebo方面證實了消息，表示會加入[Google+的研發團隊](../Page/Google+.md "wikilink")，有傳是次收購金額高達1億美元。

## 成就

2006年6月，Meebo被[PCWorld列為](../Page/PCWorld.md "wikilink")100大世界級產品。8月，Meebo被選為2006年度50大最酷的網站。

## 參看

  - [Gaim](../Page/Gaim.md "wikilink") -
    [Trillian](../Page/Trillian.md "wikilink") - [Miranda
    IM](../Page/Miranda_IM.md "wikilink")

## 外部連結

  - [Meebo em
    Português\*](https://web.archive.org/web/20070127023445/http://www.meebo.com.br/)
  - [Meebo login
    page](https://web.archive.org/web/20070127023445/http://www.meebo.com.br/)
  - [meebo me widget](http://www.meebome.com/)
  - [Meebo
    blog](https://web.archive.org/web/20080414032545/http://wwwl.meebo.com.br/)
  - [Meebo
    wiki](https://web.archive.org/web/20080414032545/http://wwwl.meebo.com.br/)
  - [Meebo
    forum](https://web.archive.org/web/20080414032545/http://wwwl.meebo.com.br/)

[Category:即时通讯软件](../Category/即时通讯软件.md "wikilink")
[Category:Chat-related
websites](../Category/Chat-related_websites.md "wikilink") [Category:Web
2.0](../Category/Web_2.0.md "wikilink")
[Category:Android软件](../Category/Android软件.md "wikilink")