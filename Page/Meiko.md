**MEIKO**是[山葉以](../Page/山葉_\(公司\).md "wikilink")[Vocaloid](../Page/Vocaloid.md "wikilink")[語音合成引擎為基礎開發](../Page/語音合成.md "wikilink")，[Crypton
Future
Media販售的虛擬女性歌手軟件第一作](../Page/Crypton_Future_Media.md "wikilink")；或此軟件的印象角色（這只是軟件的象徵，不會在實際使用時出現）。於2004年11月發售，開放價格，官方估計系列軟件實際價格約19,950日圓。由提供原聲\[1\]，軟件名稱也是取自拜鄉芽衣子的名稱\[2\]。日語第一套VOCALOID的產品，擅長[流行曲](../Page/流行曲.md "wikilink")、[搖滾樂](../Page/搖滾樂.md "wikilink")、[爵士樂](../Page/爵士樂.md "wikilink")、[R\&B](../Page/R&B.md "wikilink")、[童謠等廣範圍歌種](../Page/童謠.md "wikilink")\[3\]。VOCALOID
3引擎聲庫MEIKO V3是由[Crypton Future
Media開發并販售](../Page/Crypton_Future_Media.md "wikilink")，于2014年2月4日发售。

## 發展歷史

在MEIKO前，VOCALOID已推出LEON和LOLA，但成績欠佳。當時CRYPTON認為，如果製品包裝設計為動畫風格則可以吸引不止電子音樂製作的客戶層，於是把包裝設計為一個「手握麥克風、有朝氣的女孩」。雖然因當時的大多職業音樂製作人認為VOCALOID的發音不自然，專業的雜誌也幾乎沒有介紹，但吸引了非常多的業餘製作者。發售首年售出3000套，在業界中是個優異的成績\[4\]\[5\]，截至2007年10月23日，軟件共售出4000套\[6\]，至2009年10月27日共售出約5000套\[7\]。2006年2月，同公司發售同系列的軟件[KAITO](../Page/KAITO.md "wikilink")。2007年下旬，CRYPTON發售下一代的同類型軟件[初音未來](../Page/初音未來.md "wikilink")，並爆發熱潮，間接令MEIKO重新引起注意。其後成為[四格漫畫](../Page/四格漫畫.md "wikilink")《[小初音未來的日常](../Page/小初音未來的日常.md "wikilink")》的主角之一。2008年1月20日，更在[PlayStation
3免費小遊戲](../Page/PlayStation_3.md "wikilink")《》的情報單元《》中出現以MEIKO主唱，初音未來伴唱的歌曲\[8\]。同年7月25日發表預定於[C74發售](../Page/Comic_Market.md "wikilink")，收錄初音未來、鏡音鈴、連、MEIKO、[KAITO的五首原創曲](../Page/KAITO.md "wikilink")，是初次包含MEIKO的非同人CD\[9\]。

## 參見

  - [拜鄉芽衣子](../Page/拜鄉芽衣子.md "wikilink")
  - [KAITO](../Page/KAITO.md "wikilink")
  - [角色主唱系列](../Page/角色主唱系列.md "wikilink")
  - [VOCALOID](../Page/VOCALOID.md "wikilink")
  - [Crypton Future Media](../Page/Crypton_Future_Media.md "wikilink")

## 参考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - 官方

<!-- end list -->

  - [MEIKO](http://www.crypton.co.jp/mp/do/prod?id=25220)

  - [MEDIA
    PHAGE](https://web.archive.org/web/20071226012725/http://blog.crypton.co.jp/)（製作人員blog）

<!-- end list -->

  - 其他

<!-- end list -->

  - [拜鄉芽衣子官方網站](http://www.haigoumeiko.com/)

  - [Vocaloid Music Search](http://bokasachi.natsu.gs/index.html)

  - [「○○P」列表Wiki](http://wikiwiki.jp/oop/)

[Category:Vocaloid
(第一代)引擎产品](../Category/Vocaloid_\(第一代\)引擎产品.md "wikilink")
[Category:Vocaloid3引擎产品](../Category/Vocaloid3引擎产品.md "wikilink")
[Category:Vocaloid角色](../Category/Vocaloid角色.md "wikilink")
[Category:2004年音樂](../Category/2004年音樂.md "wikilink")
[Category:应用软件](../Category/应用软件.md "wikilink")
[Category:音樂軟件](../Category/音樂軟件.md "wikilink")

1.  [CRYPTON
    VOCALOID](http://www.crypton.co.jp/mp/pages/download/pdf/extra/vocaloid_catalog.pdf)
2.
3.  [CRYPTON 製品情報 MEIKO](http://www.crypton.co.jp/mp/do/prod?id=25220)
4.
5.
6.
7.
8.  《》第438回
9.