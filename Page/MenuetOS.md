**MenuetOS**由[芬蘭人](../Page/芬蘭.md "wikilink")開發，是一個[操作系统](../Page/操作系统.md "wikilink")，用於和[IBM
PC相容的電腦](../Page/IBM_PC.md "wikilink")。它由[汇編語言寫成](../Page/汇編語言.md "wikilink")，可以存入一隻1.44MB的软盘中。Menuet
OS
的32位版本Menuet32使用[GNU通用公共许可证發放](../Page/GNU通用公共许可证.md "wikilink")，但64位版本Menuet64使用自己的协议发放。\[1\]

MenuetOS拥有[实时](../Page/实时操作系统.md "wikilink")，[抢占式的](../Page/抢占式多任务处理.md "wikilink")[宏内核](../Page/宏内核.md "wikilink")。2010年2月24日，开发者加入了对多核的支持。最新进展包括加入了一个数学库，对MPEG视频音频库的支持以及对USB的支持。1.0版预计将会在2014年年底左右发布。\[2\]

## 外部連結

  - [MenuetOS 官方网站](http://www.menuetos.net/)
  - [Menuet OS
    簡體中文版網站](https://web.archive.org/web/20071009112309/http://www.xemean.net/menuet/download.htm)(存檔於2007年10月9日)

## 参考资料

<references/>

[Category:实时操作系统](../Category/实时操作系统.md "wikilink")

1.  [Offical](http://www.menuetos.net/)
2.  [1](http://www.solidot.org/story?sid=37240), 2013-11-17