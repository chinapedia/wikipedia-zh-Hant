**Metacritic**是一個專門收集對於[電影](../Page/電影.md "wikilink")、[電視節目](../Page/電視節目.md "wikilink")、[音樂專輯](../Page/音樂專輯.md "wikilink")、[遊戲的評論的](../Page/遊戲.md "wikilink")[網站](../Page/網站.md "wikilink")，網站會整合每個評價的分數再做出一個總評分做為這個項目的評分，在項目的多項短評中，從最好的分數到最坏的分數由下排列，最好的評價分數是綠色到最低的評價分數的[黃色](../Page/黃色.md "wikilink")、[紅色排列](../Page/紅色.md "wikilink")。

## 歷史

Metacritic由馬克·道爾（Marc Doyle）、朱莉·道爾·羅伯茨（Julie Doyle Roberts）和詹森·迪茨（Jason
Dietz）花費兩年時間创建，2001年1月上線\[1\]。2005年被[CNET收购](../Page/CNET.md "wikilink")，而現在它與CNET均位於[CBS公司旗下](../Page/CBS公司.md "wikilink")\[2\]。2010年8月網站改版，但受到用戶差評\[3\]\[4\]。

## 各項高分

### 影片

滿分（100/100）

  - [大國民](../Page/大國民.md "wikilink") 1941年\[5\]
  - [北非諜影](../Page/北非諜影.md "wikilink") 1942年\[6\]
  - [后窗](../Page/後窗.md "wikilink") 1954年\[7\]
  - [教父](../Page/教父_\(電影\).md "wikilink") 1972年\[8\]
  - [紅色情深](../Page/紅色情深.md "wikilink") 1994年\[9\]
  - [少年时代](../Page/少年时代.md "wikilink") 2014年\[10\]

### 电子遊戲

  - [碧血狂殺2](../Page/碧血狂殺2.md "wikilink")（97/100）
  - [薩爾達傳說 時之笛](../Page/薩爾達傳說_時之笛.md "wikilink") （[Nitendo
    64](../Page/任天堂64.md "wikilink")）（99/100）\[11\]
  - [托尼·霍克职业滑板2](../Page/托尼·霍克职业滑板2.md "wikilink")
    （[PS](../Page/PlayStation_\(遊戲機\).md "wikilink")）（98/100）\[12\]
  - [俠盜獵車手IV](../Page/俠盜獵車手IV.md "wikilink")
    （[PS3](../Page/PlayStation_3.md "wikilink")、[Xbox
    360](../Page/Xbox_360.md "wikilink")）（98/100）\[13\]\[14\]
  - [灵魂能力](../Page/劍魂.md "wikilink")
    （[DC](../Page/街机.md "wikilink")）（98/100）\[15\]
  - [俠盜獵車手V](../Page/俠盜獵車手V.md "wikilink") （[Xbox
    One](../Page/Xbox_One.md "wikilink")、[PS3](../Page/PlayStation_3.md "wikilink")、[Xbox
    360](../Page/Xbox_360.md "wikilink")、[PS4](../Page/PlayStation_4.md "wikilink")）（97/100）\[16\]\[17\]\[18\]\[19\]
  - [薩爾達傳說 旷野之息](../Page/塞尔达传说_旷野之息.md "wikilink")
    （[Switch](../Page/任天堂Switch.md "wikilink")）（97/100）\[20\]
  - [超级马里奥 银河](../Page/超级马里奥银河.md "wikilink")
    （[Wii](../Page/Wii.md "wikilink")） （97/100）\[21\]
  - [超级马里奥 银河2](../Page/超级马里奥银河2.md "wikilink")
    （[Wii](../Page/Wii.md "wikilink")）（97/100）\[22\]
  - [超级马里奥 奥德赛](../Page/超级马里奥_奥德赛.md "wikilink")
    （[Switch](../Page/任天堂Switch.md "wikilink")）（97/100）\[23\]
  - [戰慄時空2](../Page/戰慄時空2.md "wikilink")（96/100）\[24\]
  - [戰慄時空](../Page/戰慄時空.md "wikilink")（96/100）\[25\]
  - [傳送門2](../Page/傳送門2.md "wikilink")（95/100）\[26\]
  - [戰神4](../Page/戰神_\(2018年遊戲\).md "wikilink")（95/100）
  - [質量效應2](../Page/質量效應2.md "wikilink")（[Xbox
    360](../Page/Xbox_360.md "wikilink")）（96/100）\[27\]
  - [傳送門](../Page/傳送門.md "wikilink")（90/100）
  - [絕地要塞2](../Page/絕地要塞2.md "wikilink")（92/100）
  - [當個創世神](../Page/當個創世神.md "wikilink")（93/100）
  - [秘境探險4：盜賊末路](../Page/秘境探險4：盜賊末路.md "wikilink")（93/100）
  - [上古卷軸IV：遺忘之都](../Page/上古卷軸IV：遺忘之都.md "wikilink")（94/100）
  - [上古卷軸V：無界天際](../Page/上古卷軸V：無界天際.md "wikilink")（94/100）
  - [終極動員令](../Page/終極動員令_\(1995年遊戲\).md "wikilink")（94/100）
  - [柏德之門II：安姆疑雲](../Page/柏德之門II：安姆疑雲.md "wikilink")（95/100）
  - [柏德之門](../Page/柏德之門.md "wikilink")（91/100）
  - [暗黑破壞神](../Page/暗黑破壞神_\(遊戲\).md "wikilink")（94/100）
  - [星際爭霸II：自由之翼](../Page/星海爭霸2.md "wikilink")（94/100）
  - [魔獸爭霸III：混沌之治](../Page/魔獸爭霸III：混沌之治.md "wikilink")（92/100）
  - [世紀帝國II：帝王世紀](../Page/世紀帝國II：帝王世紀.md "wikilink")（92/100）
  - [俠盜獵車手III](../Page/俠盜獵車手III.md "wikilink")（93/100）
  - [俠盜獵車手：聖安地列斯](../Page/俠盜獵車手：聖安地列斯.md "wikilink")（93/100）
  - [摇滚史密斯2014](../Page/摇滚史密斯2014.md "wikilink")（91/100）
  - [最後生還者](../Page/最後生還者.md "wikilink")（95/100）
  - [血源詛咒](../Page/血源詛咒.md "wikilink") （92/100）
  - [Undertale](../Page/Undertale.md "wikilink") （94/100）
  - [秘境探險2：盜亦有道](../Page/秘境探險2：盜亦有道.md "wikilink")（96/100）
  - [秘境探險3：德瑞克的騙局](../Page/秘境探險3：德瑞克的騙局.md "wikilink")（92/100）

## 参考文献

## 外部連結

  - [Metacritic](http://www.metacritic.com/)

  -
  -
{{-}}

[Category:2001年建立的网站](../Category/2001年建立的网站.md "wikilink")
[Category:电影主题网站](../Category/电影主题网站.md "wikilink")
[Category:電子遊戲評論網站](../Category/電子遊戲評論網站.md "wikilink")
[Category:CNET](../Category/CNET.md "wikilink")

1.  ["Metacritic: The
    History"](http://www.metacritic.com/about-metacritic),
    Metacritic.com
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.
22.
23.
24.
25.
26.
27.