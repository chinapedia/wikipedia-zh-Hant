[Micro_SD_-_Memory_Stick_Pro_Duo_Adaptor.JPG](https://zh.wikipedia.org/wiki/File:Micro_SD_-_Memory_Stick_Pro_Duo_Adaptor.JPG "fig:Micro_SD_-_Memory_Stick_Pro_Duo_Adaptor.JPG")轉接成[MS
Pro Duo的轉卡](../Page/MS_Pro_Duo.md "wikilink")\]\]
[MicroSD與SD記憶卡.JPG](https://zh.wikipedia.org/wiki/File:MicroSD與SD記憶卡.JPG "fig:MicroSD與SD記憶卡.JPG")

**microSD**是一種極細小的[快閃記憶體卡](../Page/快閃記憶體.md "wikilink")，基於由[SanDisk製造的](../Page/SanDisk.md "wikilink")**[TransFlash](../Page/TransFlash.md "wikilink")**卡格式所創立。這類記憶卡主要於[流動電話使用](../Page/流動電話.md "wikilink")，但因其擁有體積極小的優點，隨著不斷提升的容量，它慢慢開始於[GPS設備](../Page/GPS.md "wikilink")、攜帶型音樂播放器和一些[快閃記憶體盤中使用](../Page/快閃記憶體盤.md "wikilink")。microSD的體積為
15 毫米 x 11 毫米 x 1 毫米 -
约为SD卡的1/4，相当於手指甲蓋的大小，是目前（2011年）最小的[記憶卡](../Page/記憶卡.md "wikilink")。它亦能夠以[轉接器來接駁於](../Page/轉接器.md "wikilink")[SD卡或](../Page/SD卡.md "wikilink")[Memory
Stick插槽中使用](../Page/Memory_Stick.md "wikilink")。

## 歷史

microSD卡原本稱為TF卡（T-Flash卡或TransFlash），由[摩托罗拉与](../Page/摩托罗拉.md "wikilink")[SanDisk共同研发](../Page/SanDisk.md "wikilink")，在2004年推出。不過[SanDisk無法自行將它推廣普及化](../Page/SanDisk.md "wikilink")，前期僅有[摩托罗拉的手機支援](../Page/Motorola.md "wikilink")**TransFlash**。為了能將銷路完全拓展，[SanDisk於是將](../Page/SanDisk.md "wikilink")**TransFlash**規格併入[SD協會](../Page/SD協會.md "wikilink")，成為SD家族產品之一，造就了目前使用最廣泛的手機記憶卡；而重新命名為microSD的原因是因為被[SD協會](../Page/SD協會.md "wikilink")（SDA）采纳。**TransFlash**納入[SD協會後](../Page/SD協會.md "wikilink")，僅有在實體規格的電性接觸端（[印刷電路板的金屬端子](../Page/印刷電路板.md "wikilink")，俗稱**[金手指](../Page/金手指.md "wikilink")**）縮短一點，其他部分完全沿用完全相容。另一些被SD協會採立的記憶卡包括[miniSD和](../Page/miniSD.md "wikilink")[SD卡](../Page/Secure_Digital.md "wikilink")。

SD協會在2005年3月14日於2005年度的美國[行動通信暨無線網路年會](../Page/行動通信暨無線網路年會.md "wikilink")（CTIA
Wireless
2005）中公佈microSD的格式，以及於2005年7月13日批准了microSD最終的規格。在最初公開發售時，microSD的儲存容量包括32MB、64MB和128MB。

2014年[世界移动通信大会](../Page/世界移动通信大会.md "wikilink")（MWC）期间，[SanDisk发布了一款容量高达](../Page/SanDisk.md "wikilink")128GB的
Micro [SD XC储存卡](../Page/SDXC.md "wikilink")。

## 各式類別

  - micro SD : 速度慢，普遍以大卡為主。用途廣，電腦，相機，手機皆可。
  - micro [SDHC](../Page/SDHC.md "wikilink") ：速度中，廣泛用於手機。
  - micro [SDXC](../Page/SDXC.md "wikilink") ：速度高，一秒30
    MB以上，主要支援uhs,class10以上。主要為相機使用

相互使用：若想在手機上使用相機的SD卡，則應該購買小卡附大卡。若只購買大卡，即不支援單獨使用小卡。不同的轉卡不能交互使用，而上述的SD區別並沒有詳細定義，只有速度差別。

## 參考

  - [SD卡](../Page/SD卡.md "wikilink")
  - [SD協會](http://www.sdcard.org/)

[uk:Secure Digital\#MicroSD та
MiniSD](../Page/uk:Secure_Digital#MicroSD_та_MiniSD.md "wikilink")

[Category:記憶卡](../Category/記憶卡.md "wikilink")