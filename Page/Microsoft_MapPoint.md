[Microsoft_mappoint_north_america_2001_virtual_globe.gif](https://zh.wikipedia.org/wiki/File:Microsoft_mappoint_north_america_2001_virtual_globe.gif "fig:Microsoft_mappoint_north_america_2001_virtual_globe.gif")
[437
px](https://zh.wikipedia.org/wiki/File:Map_detail_comparison_-_mappoint_and_encarta.gif "fig:437 px")

**微軟MapPoint**是[微軟經年打造](../Page/微軟.md "wikilink")、允許用戶觀看、編輯和集成地圖的技術和軟體的一個獨特作品。

MapPoint
意欲為商業用戶，因為它包括先進的功能，譬如與[微軟Office整合](../Page/Microsoft_Office.md "wikilink")，並且從[微軟Excel在一張被創造的地圖上顯示趨向和資料](../Page/Microsoft_Excel.md "wikilink")。

MapPoint技術用在：

  - 終端用戶軟體：
      - MapPoint（為[北美和](../Page/北美.md "wikilink")[西歐商業用戶](../Page/西歐.md "wikilink")）
      - AutoRoute（為[西歐家庭和商業用戶](../Page/西歐.md "wikilink")），2007版為最後一個版本。
      - [微軟Streets and
        Trips](../Page/微軟Streets_and_Trips.md "wikilink")（為北美一般家庭用戶。和MapPoint相比較，少了商業分析的功能。）
      - [Encarta](../Page/Encarta.md "wikilink")[百科全書](../Page/百科全書.md "wikilink")
  - 網路為基礎的服務：
      - [MapPoint
        Web服務](http://www.microsoft.com/mappoint/products/webservice)
      - 微軟地圖網站[mappoint.msn.com](http://mappoint.msn.com)
      - [MSN為本的](../Page/MSN.md "wikilink")[Virtual
        Earth](../Page/Virtual_Earth.md "wikilink")[虛擬地球儀服務](../Page/虛擬地球儀.md "wikilink")

微軟MapPoint每年更新。最新版本為MapPoint 2010。該軟體支援三個平台：Pocket PC,
[SmartPhone](../Page/Windows_Mobile.md "wikilink")，Windows。

## 參見

  - [Google Earth](../Page/Google_Earth.md "wikilink")

## 外部連結

  - [Microsoft MapPoint
    Review](http://www.laptopgpsworld.com/2736-review-microsoft-mappoint)

[MapPoint](../Category/微软软件.md "wikilink")