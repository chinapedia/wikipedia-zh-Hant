[Microsoft_Max.jpg](https://zh.wikipedia.org/wiki/File:Microsoft_Max.jpg "fig:Microsoft_Max.jpg")
**Microsoft Max**，或称**Microsoft Codename
Max**，是[美国](../Page/美国.md "wikilink")[微软公司推出的一款基于](../Page/微软公司.md "wikilink")[WinFX技术的](../Page/WinFX.md "wikilink")[图片](../Page/图像.md "wikilink")[软件](../Page/软件.md "wikilink")。这个软件的主要作用是用本地的图片生成列表，并与好友共享图片。该软件的界面采取了[Windows
Vista风格](../Page/Windows_Vista.md "wikilink")。

2006年10月31日，Microsoft Max
Blog宣佈Max計畫終止[1](https://web.archive.org/web/20070127013459/http://blogs.msdn.com/max/archive/2006/10/31/thank-you-the-max-project-has-concluded.aspx)。

## 安装

微软只提供過Max的在线安装程序，并暂时仅能在[英文版](../Page/英文.md "wikilink")[Windows
XP或加载多国](../Page/Windows_XP.md "wikilink")[语言包的英文版Windows](../Page/语言.md "wikilink")
XP上安装，界面也暂时只有英文版。由于WinFX技术属于下一代微软产品使用的技术，因此，正式安装前会被安装程序引导安装WinFX Runtime
Components，并有可能需要重新启动系统。注意，有时候重新启动后，安装程序会在中途报错并退出，只需在桌面点击安装程序生成的快捷方式就可以继续完成安装。

系统要求

  - 基本配置
      - 升级到SP2的Windows XP（包括家庭版和专业版，但不包括[64位版的Windows
        XP](../Page/Windows_XP_Professional_x64_Edition.md "wikilink")）
      - 1.0GHz的[中央处理器](../Page/中央处理器.md "wikilink")
      - 256MB的[内存](../Page/内存.md "wikilink")
      - 200MB可用[硬盘空间](../Page/硬盘.md "wikilink")
      - [VGA](../Page/VGA.md "wikilink")[显示器](../Page/显示器.md "wikilink")，[分辨率在](../Page/分辨率.md "wikilink")800x600[象素](../Page/象素.md "wikilink")
  - 推荐配置
      - 升级到SP2的Windows XP（包括家庭版和专业版，但不包括64位版的Windows XP）
      - 2.4GHz的中央处理器
      - 512MB内存
      - 200MB可用硬盘空间
      - 支持[Avalon技术的](../Page/Avalon.md "wikilink")[3D](../Page/3D.md "wikilink")[图形加速卡](../Page/显卡.md "wikilink")
      - [宽带](../Page/宽带.md "wikilink")[互联网连接](../Page/互联网.md "wikilink")

## 使用

Microsoft Max需要使用[Microsoft Passport
Network账号登录](../Page/Microsoft_Passport_Network.md "wikilink")，并在第一次使用某一个Microsoft
Passport
Network账号登录时，会被要求在[电子邮件中确认该账号](../Page/电子邮件.md "wikilink")（同时包含取消该账号使用Microsoft
Max资格的[超链接](../Page/超链接.md "wikilink")）。

登录后，可以从本地图片创建图片共享列表，允许在你的Microsoft
Max好友列表上的账号查看这些图片，同时也可以查看对方图片。该软件也可以创建本地的图片相册，并以多种方式显示，并可以放大缩小的三维视觉感受观看，这在微软以前的作品中几乎难以体会。

## 外部链接

  - [Microsoft Codename
    Max](https://web.archive.org/web/20051001063547/http://www.microsoft.com/max/)
  - [Max
    Blog](https://web.archive.org/web/20050924145501/http://blogs.msdn.com/max/)

[Max](../Category/微软软件.md "wikilink")
[Category:.NET](../Category/.NET.md "wikilink")