**Microsoft
Money**是由[微軟推出的](../Page/微軟.md "wikilink")[個人理財軟體](../Page/個人理財.md "wikilink")，適用於使用[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")[操作系統的電腦](../Page/操作系統.md "wikilink")，另有推出用於[Windows
Mobile的版本](../Page/Windows_Mobile.md "wikilink")（現有Money
2000-2006版，但不支援Windows Mobile 5.0）。名為Microsoft Money
Plus的最新版於2007年8月發售，共分為四個版本：Essentials、Deluxe、Premium與Home &
Business。

Microsoft
Money的主要競爭對手為[Intuit公司的個人理財軟體](../Page/Intuit.md "wikilink")[Quicken及中小企業會計軟體](../Page/Quicken.md "wikilink")[QuickBooks](../Page/QuickBooks.md "wikilink")，雖然相較之下，Intuit是間小公司，然而在會計理財軟體的市場佔有率，QuickBooks卻一直在美國的中小型企業中佔有重要地位。Microsoft於2009年6月計畫停止發展及販售Microsoft
Money，技術支援目前計畫將到2011年1月為止。\[1\] 另一方面，新推出的[Microsoft Office
Accounting](../Page/Microsoft_Office_Accounting.md "wikilink") 2009
Express提供免費下載，另有Professional版本可供購買；將整合Office套裝軟體的優勢，與Intuit的產品競爭。

Microsoft
Money現推出了[美國](../Page/美國.md "wikilink")、[英國](../Page/英國.md "wikilink")、[法國](../Page/法國.md "wikilink")、[日本](../Page/日本.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[中国以及其他英語地區使用的國際英語版](../Page/中国.md "wikilink")，但英國、法國與國際英語版自Money
2005起便未有更新，亦沒有會發佈新版的跡象。加拿大版的最新版為Money 2006。简体中文版和繁体中文版的最新版本为Money
2010。\[2\]

過去曾推出如[德國與](../Page/德國.md "wikilink")[意大利等其他國家的版本](../Page/意大利.md "wikilink")，但均被中止，相信是由於沒有足夠多的用戶来提供销售收入，以便推出更多地區化新版本。

Microsoft Money的公開[FAQ可於](../Page/FAQ.md "wikilink")
[money.mvps.org](http://money.mvps.org/) 上取得。

## 版本历史

| 上市版本                        | 实际版本 | 备注                                                                      |
| --------------------------- | ---- | ----------------------------------------------------------------------- |
| Microsoft Money Plus (2010) | 19.0 |                                                                         |
| Microsoft Money Plus (2009) | 18.0 |                                                                         |
| Microsoft Money Plus (2008) | 17.0 | 第一次在Microsoft Money使用[产品啟動](../Page/产品啟動.md "wikilink")。                |
| Microsoft Money 2007        | 16.0 |                                                                         |
| Microsoft Money 2006        | 15.0 | 最后一个加拿大本地版。最后一个Windows Mobile平台版。                                       |
| Microsoft Money 2005        | 14.0 | 最后一个[英国](../Page/英国.md "wikilink")、[法国及国际化版](../Page/法国.md "wikilink")。 |
| Microsoft Money 2004        | 12.0 |                                                                         |
| Microsoft Money 2003        | 11.0 |                                                                         |
| Microsoft Money 2002        | 10.0 |                                                                         |
| Microsoft Money 2001        | 9.0  |                                                                         |
| Microsoft Money 2000        | 8.0  | 第一个可用于Windows Mobile平台的版本。                                              |
| Microsoft Money 99          | 7.0  | 最后一个[德國版和](../Page/德國.md "wikilink")[巴西版](../Page/巴西.md "wikilink")。    |
| Microsoft Money 98          | 6.0  |                                                                         |
| Microsoft Money 97          | 5.0  |                                                                         |
| Microsoft Money 95          | 4.0  |                                                                         |
| Microsoft Money 3.0         | 3.0  |                                                                         |
| Microsoft Money 2.0         | 2.0  |                                                                         |
| Microsoft Money             | 1.0  | 初始版。                                                                    |

注意:版本13.x从未发表及销售过。

## 外部連結

  - [Microsoft
    Money官方網站](https://web.archive.org/web/20110720071422/http://www.microsoft.com/money/default.mspx)
  - [Microsoft
    Money中国大陆官方网站](https://web.archive.org/web/20100724062913/http://www.microsoft.com/china/money/default.aspx)（简体中文）

## 資料來源

<references />

[Category:會計軟體](../Category/會計軟體.md "wikilink")
[Category:微軟](../Category/微軟.md "wikilink")

1.  <http://news.cnet.com/8301-13860_3-10261742-56.html>
2.