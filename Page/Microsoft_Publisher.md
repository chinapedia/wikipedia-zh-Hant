**Microsoft Publisher**，舊稱**Microsoft Office
Publisher**，是[微软公司发行的桌面出版应用软件](../Page/微软.md "wikilink")。它常被人们认为是一款入门级的桌面出版应用软件，它能提供比Microsoft
Word更强大的页面元素控制功能，但比起专业的页面布局软件，比如说[Adobe公司的](../Page/Adobe.md "wikilink")[InDesign以及](../Page/InDesign.md "wikilink")[Quark公司的](../Page/Quark.md "wikilink")[QuarkXPress来还略逊一筹](../Page/QuarkXPress.md "wikilink")。

比起其它桌面出版软件来，Publisher在历史上一直不受高端商务打印商店欢迎。它只能用于[Windows](../Page/Windows.md "wikilink")，而业界占相当份额的系统运行的是苹果公司的Mac
OS
X。Publisher定位于入门级的应用软件造成了很多的问题，比如在服务提供商的电脑上没有相应字体和嵌入对象等等（虽然Publisher提供了可以把相关联的文件压缩成一个自释放应用程序的工具）。许多高端特性，比如透明化，对象阴影，slugs，路径文本，内嵌的[PDF输出等等](../Page/PDF.md "wikilink")，或者没有完全实现，或者干脆没有。但最近的一些版本有了很大的提高，开始关注色彩分离以及正确地处理彩色输出。Publisher
2007还包含了期待已久的特性，可以输出商业出版品质带内嵌字体的PDF格式，作为选件可以从微软网站下载。

Publisher被包含在高端的Microsoft
Office版本中。这反映了微软对Publisher的定位，它是一款易用的，相比于那些重量级产品而言价格适中的产品，它的目标用户主要是那些没有专业人员制作市场推广材料以及其它文档的中小型企业。

Publisher的大部份替代品，LibreOffice
4.0外，都不提供导入Publisher的功能；但是，Publisher可以导出成EMF（Enhanced
Metafile）格式，它可以被其它软件支持。

## 版本

Microsoft Publisher的版本包括：

  - 1991年Microsoft Publisher for
    [Windows](../Page/Windows.md "wikilink")
  - 1993年Microsoft Publisher 2.0
  - 1995年Microsoft Publisher 3.0
  - 1996年Microsoft Publisher 97（[Windows
    95或更高](../Page/Windows_95.md "wikilink")）
  - 1998年Microsoft Publisher 98（[Windows
    95或更高](../Page/Windows_95.md "wikilink")）
  - 1999年Microsoft Publisher 2000（[Windows
    95或更高](../Page/Windows_95.md "wikilink")）
  - 2001年Microsoft Publisher 2002（[Windows
    98或更高](../Page/Windows_98.md "wikilink")）
  - 2003年Microsoft Office Publisher 2003（[Windows
    2000](../Page/Windows_2000.md "wikilink") SP3\]\]或更高）
  - 2007年Microsoft Office Publisher 2007（[Windows
    XP](../Page/Windows_XP.md "wikilink")
    SP2\]\]或[Vista](../Page/Vista.md "wikilink")）
  - 2010年Microsoft Office Publisher 2010（[Windows
    XPSP](../Page/Windows_XP.md "wikilink")3或更高）
  - 2012年Microsoft Office Publisher 2013（[Windows
    7或更高](../Page/Windows_7.md "wikilink")）
  - 2015年Microsoft Office Publisher 2016（[Windows
    7](../Page/Windows_7.md "wikilink") SP1或更高）
  - 2018年Microsoft Office Publisher 2019（[Windows
    10或更高](../Page/Windows_10.md "wikilink")）

[Category:Microsoft Office](../Category/Microsoft_Office.md "wikilink")
[Category:數碼字體排印](../Category/數碼字體排印.md "wikilink")