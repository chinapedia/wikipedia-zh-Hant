**Midori**，是一個輕量級的[跨平台](../Page/跨平台.md "wikilink")[網絡瀏覽器](../Page/網絡瀏覽器.md "wikilink")。用[C語言編寫](../Page/C語言.md "wikilink")，使用[GTK+
2](../Page/GTK+_2.md "wikilink")，搜索部分基於[OpenSearch的搜索框](../Page/OpenSearch.md "wikilink")。其名稱來自於[日文的](../Page/日文.md "wikilink")「」（）。是[Xfce的一部分](../Page/Xfce.md "wikilink")。

## 特點

  - 使用[WebKit](../Page/WebKit.md "wikilink")[排版引擎](../Page/排版引擎.md "wikilink")。
  - 擴展使用[C語言編寫](../Page/C語言.md "wikilink")

:\*自帶的包括[鼠標手勢](../Page/鼠標手勢.md "wikilink")，彩色標籤頁，廣告過濾等。

  - 全面整合[GTK+ 2和](../Page/GTK+#GTK+_2.md "wikilink")[GTK+
    3](../Page/GTK+.md "wikilink")。
  - 支持用戶腳本和用戶樣式
  - 支持標籤分頁
  - 支持[html5和部份](../Page/html5.md "wikilink")[CSS3](../Page/CSS3.md "wikilink")

:\*使用者可自由決定要不要啟用html5的localStorage和sessionStorage，默認為否。

  - 支持[國際化域名](../Page/國際化域名.md "wikilink")
  - 支持[快速撥號](../Page/快速撥號.md "wikilink")

## 參見

  - [網頁瀏覽器列表](../Page/網頁瀏覽器列表.md "wikilink")
  - [網頁瀏覽器比較](../Page/網頁瀏覽器比較.md "wikilink")

## 外部連結

  - [Midori官方主頁](http://midori-browser.org)

  - [Midori开发者主頁](http://www.twotoasts.de)

  - [launchpad頁面](https://launchpad.net/midori)

## 參考文獻

[Category:Webkit衍生軟體](../Category/Webkit衍生軟體.md "wikilink")
[Category:自由網頁瀏覽器](../Category/自由網頁瀏覽器.md "wikilink")
[Category:自由軟件](../Category/自由軟件.md "wikilink")
[Category:Linux軟件](../Category/Linux軟件.md "wikilink")