[Minami_Kuribayashi,_Kimi_ga_Nozomu_Eien_20100701.jpg](https://zh.wikipedia.org/wiki/File:Minami_Kuribayashi,_Kimi_ga_Nozomu_Eien_20100701.jpg "fig:Minami_Kuribayashi,_Kimi_ga_Nozomu_Eien_20100701.jpg")
**Minami**（）是[日本](../Page/日本.md "wikilink")[靜岡縣](../Page/靜岡縣.md "wikilink")[静冈市](../Page/静冈市.md "wikilink")[葵區出身的女](../Page/葵區.md "wikilink")[歌手](../Page/歌手.md "wikilink")、[作詞家及](../Page/作詞家.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")，她除了正職歌手以外還做兼職[聲優](../Page/聲優.md "wikilink")。舊名為「-{栗}-林美奈實」。暱稱「」，[血型AB型](../Page/血型.md "wikilink")。

## 人物

常葉学園橘高等学校音樂科、[代代木動畫學院声優人才科](../Page/代代木動畫學院.md "wikilink")（[特待生](../Page/特待生.md "wikilink")）畢業。幼稚園到[短期大學畢業都在學習鋼琴](../Page/短期大學.md "wikilink")，短大還專攻[声樂](../Page/声樂.md "wikilink")。從短大入学到2000年頃曾組過樂團，演唱、作詞、作曲一手包辦，當時拔擢為廣播節目助手，製作[廣播劇主題曲為契機](../Page/廣播劇.md "wikilink")\[1\]，以本名**村上久美子**名義從事音樂活動，但不順遂；直到改[藝名為](../Page/藝名.md "wikilink")-{栗}-林美奈實，參與成人遊戲《[你所期望的永遠](../Page/你所期望的永遠.md "wikilink")》配音並演唱主題曲才聲名大噪。2016年以出道15週年為由，宣布改用現在藝名Minami\[2\]。

2015年宣佈已育有一男\[3\]。

## 出演作品

### 舞臺

  - 「[Sound Horizon](../Page/Sound_Horizon.md "wikilink") 6th Story
    Concert「『Moira』 〜其れでも、お征きなさい仔等よ〜」」

### 電視動畫

  - [你所期望的永遠](../Page/你所期望的永遠.md "wikilink")（涼宮遙）

  - [舞-乙HiME](../Page/舞-乙HiME.md "wikilink")（Erstin Ho）

  - （社霞／涼宮遙）

  - [School Days](../Page/School_Days.md "wikilink")（小淵南）

### OVA

  - Akane Maniax（涼宮遙）
  - [你所期望的永遠\~Next Season\~](../Page/你所期望的永遠.md "wikilink")（涼宮遙）
  - [舞-乙HiME Zwei](../Page/舞-乙HiME_Zwei.md "wikilink")（）

### 遊戲

  - [你所期望的永遠](../Page/你所期望的永遠.md "wikilink")（**涼宮遙**）
  - [Muv-Luv](../Page/Muv-Luv.md "wikilink")（社霞）
  - [Muv-Luv Alternative](../Page/Muv-Luv_Alternative.md "wikilink")（社霞
    / 涼宮遙）
  - [Muv-Luv ALTERED
    FABLE](../Page/Muv-Luv_ALTERED_FABLE.md "wikilink")（社霞 / 涼宮遙）
  - [化石之歌](../Page/化石之歌.md "wikilink")（プリエ）
  - [School Days](../Page/School_Days.md "wikilink")（小淵南）
  - [School Days L×H](../Page/School_Days_L×H.md "wikilink")（小淵南）
  - ["Hello, world."](../Page/"Hello,_world.".md "wikilink")（友永遥香）
  - 舞-乙HiME ～乙女舞闘史！！～（**埃爾斯特·霍**）
  - シェイプシフター（アーエス・カーニュエール）
  - [Cross Days](../Page/Cross_Days.md "wikilink")（小淵南）

## 樂曲

### 單曲

  - Crying for Rain／カワキヲアメク／Kawakiwoameku
  - マブラヴ
  - 風のゆくえ／yours
  - Blue tears
  - Precious Memories
  - 翼はPleasure Line
  - Shining☆Days
  - beginning
  - Shining☆Days Re-Product & Remix
  - マブラヴ／桜の花が咲くまえに～muv-Luv After　Episode～
  - Blue treasure
  - Dream☆Wing
  - Crystal Energy
  - Yell\!
  - United force
  - BUT, metamorphosis
  - Next Season
  - Love Jump
  - unripe hero
  - Finality blue
  - sympathizer
  - Miracle Fly
  - earth trip
  - あんりある♡パラダイス
  - 冥夜花伝廊
  - STRAIGHT JET
  - 君の中の英雄
  - HAPPY CRAZY BOX
  - signs 〜朔月一夜〜
  - BELIEVE
  - Doubt the World
  - ZERO\!\!
  - True Blue Traveler
  - moving soul
  - Patria

### 單曲詳情

  - \[20020403\]**マブラヴ**

<!-- end list -->

1.  マブラヴ（PC游戏[マブラヴ主题曲](../Page/マブラヴ.md "wikilink")）
2.  君が望む永遠（日本動畫[你所期望的永遠片尾曲](../Page/你所期望的永遠.md "wikilink")）

<!-- end list -->

  - \[20030205\]**風のゆくえ／yours**

<!-- end list -->

1.  風のゆくえ（[你所期望的永遠PS](../Page/你所期望的永遠.md "wikilink")2版主题歌）
2.  yours（[你所期望的永遠DC版主题歌](../Page/你所期望的永遠.md "wikilink")）

<!-- end list -->

  - \[20031029\]**Precious Memories**

<!-- end list -->

1.  Precious Memories（日本動畫[你所期望的永遠主題曲](../Page/你所期望的永遠.md "wikilink")）
2.  おなじ月をみてる

<!-- end list -->

  - \[2004年1月21日\]**翼はPleasure Line**

<!-- end list -->

1.  翼はPleasure Line（日本動畫[聖槍修女主題曲](../Page/聖槍修女.md "wikilink")）
2.  snow

<!-- end list -->

  - \[20041103\]**Shining☆Days**

<!-- end list -->

1.  Shining☆Days（日本動畫[舞-HiME主題曲](../Page/舞-HiME.md "wikilink")）
2.  小さな星が降りる時（日本動畫[舞-HiME插入曲](../Page/舞-HiME.md "wikilink")）

<!-- end list -->

  - \[20050324\]**Shining☆Days Re-Product\&Remix**

<!-- end list -->

1.  Shining☆Days BURAT GROOVE MIX
2.  Shining☆Days verbose：trance MIX
3.  Shining☆Days LOVE FLARE EASY FILTER MM MIX

<!-- end list -->

  - \[20050810\]**マブラヴ（再录音版）**

<!-- end list -->

1.  マブラヴ（再录音版）
2.  桜の花が咲くまえに（PC游戏[桜の花が咲くまえに 〜Muv-Luv After
    Episode〜主題曲](../Page/桜の花が咲くまえに_〜Muv-Luv_After_Episode〜.md "wikilink")）

<!-- end list -->

  - \[20050824\]**Blue treasure**

<!-- end list -->

1.  Blue treasure（日本動畫[蓝色潮痕主題曲](../Page/蓝色潮痕.md "wikilink")）
2.  人魚姫

<!-- end list -->

  - \[20051102\]**Dream☆Wing**

<!-- end list -->

1.  Dream☆Wing（日本動畫[舞-乙HiME主題曲](../Page/舞-乙HiME.md "wikilink")）
2.  あなたがそばにいるだけで

<!-- end list -->

  - \[20060222\]**Crystal Energy**

<!-- end list -->

1.  Crystal Energy（日本動畫[舞-乙HiME主題曲](../Page/舞-乙HiME.md "wikilink")2）
2.  風と星に抱かれて…（日本動畫[舞-乙HiME插入曲](../Page/舞-乙HiME.md "wikilink")）

<!-- end list -->

  - \[20061122\]**Yell\!**

<!-- end list -->

1.  Yell\!（日本動畫[超级机器人大战OG 第一季片尾曲](../Page/超级机器人大战OG_第一季.md "wikilink")）
2.  冬色のメロディ

<!-- end list -->

  - \[20070516\]**United Force**

<!-- end list -->

1.  United Force（日本動畫[機神大戰主題曲](../Page/機神大戰.md "wikilink")）
2.  immature

<!-- end list -->

  - \[20071226\]**BUT, metamorphosis**

<!-- end list -->

1.  BUT, metamorphosis
2.  winter fairy

<!-- end list -->

  - \[20080312\]**Next Season／sweet passion**

<!-- end list -->

1.  Next Season（日本動畫[你所期望的永遠OVA主題曲](../Page/你所期望的永遠.md "wikilink")）
2.  sweet passion(PC游戏[你所期望的永遠Vista版主题曲](../Page/你所期望的永遠.md "wikilink")）

<!-- end list -->

  - \[20080423\]**Love Jump**

<!-- end list -->

1.  Love Jump（日本動畫[紅主題曲](../Page/紅.md "wikilink")）
2.  桜時計

<!-- end list -->

  - \[20080723\]**unripe hero**

<!-- end list -->

1.  unripe hero（日本動畫[蒼色騎士主題曲](../Page/蒼色騎士.md "wikilink")）
2.  ひとことだけの勇気 re-product version
3.  ここにあったね

<!-- end list -->

  - \[20081022\]**Finality blue**

<!-- end list -->

1.  Finality blue（日本動畫[舞-乙HiMEOVA主題曲](../Page/舞-乙HiME.md "wikilink")）
2.  Heart All Green（日本動畫[舞-乙HiMEOVA主題曲](../Page/舞-乙HiME.md "wikilink")）
3.  ここにあったね（日本動畫[舞-乙HiMEOVA主題曲](../Page/舞-乙HiME.md "wikilink")）

<!-- end list -->

  - \[20090121\]**sympathizer**

<!-- end list -->

1.  sympathizer（日本動畫[黑神主題曲](../Page/黑神.md "wikilink")）
2.  空のこたえ

<!-- end list -->

  - \[20090422\]**Miracle Fly**

<!-- end list -->

1.  Miracle Fly（日本動畫[穿越宇宙的少女主題曲](../Page/穿越宇宙的少女.md "wikilink")）
2.  Little Promise

<!-- end list -->

  - \[20090722\]**earth trip**

<!-- end list -->

1.  earth trip
2.  pregare
3.  earth trip 〜reprise〜

<!-- end list -->

  - \[20091021\]**あんりある・パラダイス**

<!-- end list -->

1.  あんりある・パラダイス（日本動畫[肯普法主題曲](../Page/肯普法.md "wikilink")）
2.  静寂の腕輪

<!-- end list -->

  - \[20100127\]**冥夜花伝廊**

<!-- end list -->

1.  冥夜花伝廊（日本動畫[刀语主題曲](../Page/刀语.md "wikilink")）
2.  微笑みの彼方

<!-- end list -->

  - \[20110126\]**STRAIGHT JET**

<!-- end list -->

1.  STRAIGHT JET（日本動畫[IS〈Infinite
    Stratos〉主題曲](../Page/IS〈Infinite_Stratos〉.md "wikilink")）
2.  First Addition

<!-- end list -->

  - \[20130424\]**ZERO\!\!**

<!-- end list -->

1.  ZERO\!\!（日本動畫[打工吧！魔王大人主題曲](../Page/打工吧！魔王大人.md "wikilink")）
2.  好きじゃなかったら…

<!-- end list -->

  - \[20131106\]**True Blue Traveler**

<!-- end list -->

1.  True Blue Traveler（日本動畫[IS〈Infinite
    Stratos〉2主題曲](../Page/IS〈Infinite_Stratos〉.md "wikilink")）
2.  ONE

<!-- end list -->

  - \[20140730\]**moving soul**

<!-- end list -->

1.  moving soul（日本動畫[Fate/kaleid liner 魔法少女☆伊莉雅
    2wei主題曲](../Page/Fate/kaleid_liner_魔法少女☆伊莉雅.md "wikilink")）
2.  導きの書

<!-- end list -->

  - \[20160824\]**Patria**

<!-- end list -->

1.  Patria（日本動畫[Regalia 三圣星片尾曲](../Page/Regalia_三圣星.md "wikilink")）
2.  君になるために

### 專輯

  - \[20041201\]**Overture**

<!-- end list -->

1.  Precious Memories
2.  Blue tears
3.  いっしょにいたいね
4.  風のゆくえ
5.  yours
6.  HAPPY♥BIRTHDAY
7.  Rumbling hearts
8.  星空のワルツ
9.  ひとことだけの勇気
10. I will
11. 遙かなる地球の歌
12. マブラヴ

<!-- end list -->

  - \[20060426\]**passage**

<!-- end list -->

1.  passage
2.  Shining☆Days
3.  小さな星が降りる時
4.  真夏のBirthday
5.  Blue treasure
6.  あなたが…いない
7.  翼はPleasure Line
8.  マブラヴ
9.  beginning
10. 桜の花が咲くまえに
11. Dream☆Wing

<!-- end list -->

  - \[20061221\]**fantastic arrow**

（注：全作詞、作曲：栗林みな実）

1.  fantastic arrow
2.  ココロノカケゴト
3.  ある冬の日の午後
4.  ふたりきりのプラネタリウム
5.  人魚姫
6.  affection
7.  硝子の小瓶
8.  boyfriend
9.  桜の光の中で
10. Crystal Energy
11. アプローチ
12. 絆

<!-- end list -->

  - \[20080827\]**dream link**

<!-- end list -->

1.  dream link
2.  Love Jump
3.  wonderful worker
4.  抱きしめたその後で
5.  But,metamorphosis
6.  涙の理由
7.  United Force
8.  海から始まる物語
9.  Rain Rain
10. Yell\!
11. eternity
12. divergence
13. 風と星に抱かれて…
14. Next Season

<!-- end list -->

  - \[20100421\]**mind touch**

<!-- end list -->

1.  mind touch
2.  earth trip
3.  Miracle Fly
4.  unripe hero
5.  桜時計
6.  Finality blue
7.  Heart All Green
8.  ここにあったね
9.  Spring Chime
10. pregare
11. sympathizer
12. CHANGE THE FUTURE
13. あんりある♡パラダイス

<!-- end list -->

  - \[20110309\]**miracle fruit**

（注：全作詞、作曲：栗林みな実）

1.  miracle fruit
2.  secret moment
3.  氷の結晶
4.  one drop
5.  stage
6.  静寂の腕輪
7.  Luminous Beat
8.  虹色の永遠
9.  空のこたえ
10. Beautiful Blaze
11. planet earth

### 精选集

  - \[20110803\]**stories**

(注：栗林みな実 10th Anniversary Best Album）

CD1

1.  Rumbling hearts
2.  翼はPleasure Line
3.  STRAIGHT JET
4.  Love Jump
5.  涙の理由
6.  冥夜花伝廊
7.  かがやく時空が消えぬ間に
8.  だってMUV-LUVなんだもんっ☆
9.  Next Season
10. 君が望む永遠
11. マブラヴ
12. 遙かなる地球の歌
13. I will
14. Shining☆Days

CD2

1.  Precious Memories
2.  fantastic arrow
3.  boyfriend
4.  Blue treasure
5.  wonderful worker
6.  Crystal Energy
7.  おなじ月をみてる
8.  あんりある♡パラダイス
9.  mind touch
10. 星空のワルツ
11. 眠り姫
12. eternity
13. dream link
14. Yell\!

### 其他歌曲

  - \[20030724\]**SONGS FROM âge**
      - 1.君がいた季節(BASXI ver.)
      - 5.化石の歌
      - 7.いっしょにいたいね
      - 8.Rumbling hearts(Game ver.)
      - 9.ウインターラブ
      - 11.Rumbling hearts(Acoustic ver.)

<!-- end list -->

  - \[20031226\]**Portrait2 遙**
      - 1.星のワルツ
      - 2.眠り姫

<!-- end list -->

  - \[20081225\]**SONGS FROM âge2**
      - 1.だってMUV-LUVなんだもんっ☆
      - 2.かがやく時空が消えぬ間に

<!-- end list -->

  - \[20071221\]**OVA 君が望む永遠 ～Next Season～ 初回限定版 特典CD キャラクターイメージCD
    vol.①**
      - 2.Still I love you …

<!-- end list -->

  - \[20041027\]**闇鍋CD・極**
      - 6.天使の真実

<!-- end list -->

  - \[20110302\]'''刀語 第十二巻 炎刀 銃 完全生産限定版 特典CD '''
      - 1.時すでに始まりを刻む

<!-- end list -->

  - \[20110420\]'''Choose my love\!／妄想少女A '''
      - 1.Choose my love\!

<!-- end list -->

  - \[20110511\]**戰場女武神3 為誰而負的槍傷**
      - いつか咲く光の花

### DVD

  - 栗林みな実『1st Tour 2007 fantastic arrow LIVE DVD』
  - 栗林みな実『2nd TOUR 2008 dream link LIVE DVD』

## 註釋

## 外部連結

  -
  - [](https://web.archive.org/web/20060307094357/http://www.interq.or.jp/gemini/minami/)

  -
  - [（日本後援會網站）](https://web.archive.org/web/20061231210341/http://mizuno.pekori.to/kuribayashiminami.htm)

  - [Lantis站內的個人網頁](https://web.archive.org/web/20080325224129/http://lantis.jp/artist/data.php?id=0fdf611ec580068bf00d6c1352c81db2)

  -

<div class="notice metadata"  id="spoiler">

<small>**※村上久美子時期（至2001年止）**</small>

</div>

  - voice
  - 防音室935

[Category:日本女歌手](../Category/日本女歌手.md "wikilink")
[Category:日本作詞家](../Category/日本作詞家.md "wikilink")
[Category:日本作曲家](../Category/日本作曲家.md "wikilink")
[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:日本成人遊戲配音員](../Category/日本成人遊戲配音員.md "wikilink")
[Category:靜岡縣出身人物](../Category/靜岡縣出身人物.md "wikilink")
[Category:動畫歌手](../Category/動畫歌手.md "wikilink")
[Category:Lantis旗下歌手](../Category/Lantis旗下歌手.md "wikilink")

1.  [](http://www45.tok2.com/home/minami/index_rekishi2125.htm)
2.   Minamiオフィシャルブログ|accessdate=2016-07-31|last=m-kuribayashi}}
3.   Minamiオフィシャルブログ|accessdate=2016-07-31|last=m-kuribayashi}}