**Mini-VGA**是一種用於筆記本電腦以及其它系統上的用於替代標準[VGA的影像接口](../Page/VGA.md "wikilink")。除開它的緊密的組成以及輸出VGA影像信號之外，mini-VGA端口還增加了通過[擴展顯示識別數據](../Page/擴展顯示識別數據.md "wikilink")（Extended
display identification
data）來輸出[AV端子信號以及](../Page/AV端子.md "wikilink")[S-Video信號的功能](../Page/S-Video.md "wikilink")。

今天[mini-DVI接口已經在大多數應用上替代了mini](../Page/mini-DVI.md "wikilink")-VGA。mini-VGA多數被應用在[蘋果公司的](../Page/蘋果公司.md "wikilink")[iBook](../Page/iBook.md "wikilink")，[eMac](../Page/eMac.md "wikilink")，早期型號的[PowerBook](../Page/PowerBook.md "wikilink")（12英吋），以及一些[iMac上](../Page/iMac.md "wikilink")。[Sony也在自己的幾種型號的筆記本上添加了這種接口](../Page/Sony.md "wikilink")。

## 外部連結

  - [iBook Developer
    Note：外部顯示端口](https://web.archive.org/web/20040918144618/http://developer.apple.com/documentation/Hardware/Developer_Notes/Macintosh_CPUs-G3/iBook/3_Input-Output/chapter_4_section_14.html)

[Category:蘋果公司硬體](../Category/蘋果公司硬體.md "wikilink")
[Category:數碼顯示接口](../Category/數碼顯示接口.md "wikilink")