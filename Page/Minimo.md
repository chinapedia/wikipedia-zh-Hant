**Minimo**為[Mozilla精簡版的](../Page/Mozilla.md "wikilink")[瀏覽器](../Page/瀏覽器.md "wikilink")，這個計畫在於減少記憶體佔用與程式碼的大小，並讓此瀏覽器可在更小的螢幕上增加其可用性，並在小型消費性的裝置上運作。Minimo計畫讓所有小型裝置或系統資源受限的裝置都可以執行，加上已證實的安全性與[跨平台的執行能力](../Page/跨平台.md "wikilink")\[1\]。

在桌上型或筆記型電腦上運行的Mozilla瀏覽器或将縮減成可在掌上型裝置運作的版本，但瀏覽網頁功能毫不遜色。對於內容開發人員，將會有更多人以[無線網路來访问這些基於相同標準的網頁內容](../Page/無線網路.md "wikilink")。Minimo計畫將使用32至64MB左右的[記憶體](../Page/記憶體.md "wikilink")\[2\]。

2007年11月13日，Minimo計劃的負責人宣布停止Minimo計劃\[3\]。到2008年，Mozilla將掌上型裝置瀏覽器的重點放在另一個計劃[Fennec上](../Page/Firefox_for_mobile.md "wikilink")。

## 參考資料

## 外部链接

  - [Minimo專案首頁](http://www-archive.mozilla.org/projects/minimo/)

[Category:網頁瀏覽器](../Category/網頁瀏覽器.md "wikilink")
[Category:自由网页浏览器](../Category/自由网页浏览器.md "wikilink")
[Category:Mozilla](../Category/Mozilla.md "wikilink")

1.
2.
3.