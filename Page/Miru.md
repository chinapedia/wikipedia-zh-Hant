****是一位[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")。主要為[十八禁遊戲配音](../Page/十八禁遊戲.md "wikilink")。自稱147cm\[1\]。

## 主要演出作品

### 成人遊戲

  - 2002年

<!-- end list -->

  - （和泉紫苑）

  - Men at Work\! 3（Lisa）

<!-- end list -->

  - 2003年

<!-- end list -->

  - （）

  - （三島 輝）

  - [最終痴漢電車2](../Page/最終痴漢電車2.md "wikilink")（片瀬麻穗）

  - （澤村 菜乃）

  - [没有天使的12月](../Page/没有天使的12月.md "wikilink")（木田 惠美梨）

  - （東條院 遙）

  - （麻生 こま）

  - （里村 茜）

  - [V.G.NEO](../Page/V.G.NEO.md "wikilink")（岡本歩美）

<!-- end list -->

  - 2004年

<!-- end list -->

  - （ミリオム）

  - [IZUMO2](../Page/IZUMO.md "wikilink")（）

  - [瀨里奈](../Page/瀨里奈.md "wikilink")（周防 雪奈）

  - [空之色，水之色](../Page/空之色，水之色.md "wikilink")（空山 菜摘芽）

  - （神崎 千尋）

  - [Forest](../Page/Forest.md "wikilink")（九月 周）

<!-- end list -->

  - 2005年

<!-- end list -->

  - （）

  - （林檎姫）

  - [塵骸魔京](../Page/塵骸魔京.md "wikilink")（九門 惠）

  - （子安愛）

  - （）

  - [Happiness\!](../Page/Happiness!.md "wikilink")（式守 伊吹）

  - （）

  - [SEVEN-BRIDGE](../Page/SEVEN-BRIDGE.md "wikilink")（）

<!-- end list -->

  - 2006年

<!-- end list -->

  - （春日 更紗）

  - （）

  - （牧場 汐）

  - [Gore Screaming Show](../Page/Gore_Screaming_Show.md "wikilink")（）

  - [StarTRain](../Page/StarTRain.md "wikilink")（夢原 飛鳥）

  - （子安愛）

  - [Triptych](../Page/Triptych.md "wikilink")（）

  - [Happiness\! Re:Lucks](../Page/Happiness!.md "wikilink")（式守 伊吹）

  - [BALDR BULLET REVELLION](../Page/機甲戰線.md "wikilink")（蕾貝卡·普爾榭可）

  - （）

  - [BRA-BAN\! -The bonds of
    melody-](../Page/BRA-BAN!_-The_bonds_of_melody-.md "wikilink")（御影
    須美）

  - （）

  - （野乃崎 翼）

  - [夢見師](../Page/夢見師.md "wikilink")（森田奈緒）

  - [四娘物語](../Page/四娘物語.md "wikilink")（雪 亞凜沙）

  - （）

  - [IZUMO2 -學園狂想曲-](../Page/IZUMO2.md "wikilink")（サクヤ）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [染紅的街道](../Page/染紅的街道.md "wikilink")（）

  - [E×E](../Page/E×E.md "wikilink")（籠 夏希）

  - （佐藤鈴季／吉野）

  - （セレス）

  - [きみはぐ](../Page/きみはぐ.md "wikilink")（）

  - [Rondo Leaflet](../Page/Rondo_Leaflet.md "wikilink") （ナー）

  - [Xross Scramble](../Page/Xross_Scramble.md "wikilink")（蕾貝卡·普爾榭可）

  - [人工少女3](../Page/人工少女3.md "wikilink")（U型）

  - [Cheerful\!](../Page/Cheerful!.md "wikilink")（秋野 紅葉）

  - [Primitive Link](../Page/Primitive_Link.md "wikilink")（泉　雨音）

  - [恋姬†無双](../Page/恋姬†無双.md "wikilink")（荀彧）

  - [MP ～Maid promotion
    master～](../Page/MP_～Maid_promotion_master～.md "wikilink")（）

  - [桃華月憚](../Page/桃華月憚.md "wikilink")（胡蝶三姊妹）

  - [GUN-KATANA（銃刀）
    ―Non-Human-Killer―](../Page/GUN-KATANA（銃刀）_―Non-Human-Killer―.md "wikilink")（）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [真·戀姬†無雙](../Page/真·戀姬†無雙.md "wikilink")（荀彧）
  - [あいれぼ-IDOL☆REVOLUTION-](../Page/あいれぼ-IDOL☆REVOLUTION-.md "wikilink")（**桜井
    桃花**）
  - [それは舞い散る桜のように 完全版](../Page/それは舞い散る桜のように_完全版.md "wikilink")（**芹沢
    かぐら**）
  - [天ツ風 〜傀儡陣風帖〜](../Page/天ツ風_〜傀儡陣風帖〜.md "wikilink")（**九鬼 朱火**）
  - [11eyes -罪與罰與贖的少女-](../Page/11eyes_-罪與罰與贖的少女-.md "wikilink")（**広原
    雪子**）
  - [ENGAGE
    LINKS](../Page/ENGAGE_LINKS.md "wikilink")（ティア＝バルドー、ブリジット＝バルドー）
  - [コンチェルトノート](../Page/コンチェルトノート.md "wikilink")（**東条 白雪**）
  - [ステルラ エクエス 〜贖罪の姫騎士〜](../Page/ステルラ_エクエス_〜贖罪の姫騎士〜.md "wikilink")（児珠
    十六夜）
  - [超昂閃忍遙](../Page/超昂閃忍遙.md "wikilink")（ルリー）
  - [夏神](../Page/夏神.md "wikilink")（**高里 七瀬**）
  - [夏空彼方](../Page/夏空彼方.md "wikilink")（**七條 沙沙羅**）
  - [春色桜瀬](../Page/春色桜瀬.md "wikilink")（沖田 綾乃）
  - [ぴこぴこ 〜恋する気持の眠る場所〜](../Page/ぴこぴこ_〜恋する気持の眠る場所〜.md "wikilink")（有川 蓮）
  - [Before Dawn Daybreak
    〜深淵の歌姫〜](../Page/Before_Dawn_Daybreak_〜深淵の歌姫〜.md "wikilink")（**レオノーラ**）
  - [ファンタジカル](../Page/ファンタジカル.md "wikilink")（**ダイナ**）
  - [プリマ☆ステラ](../Page/Prima☆Stella.md "wikilink")（**東方院 静歌**）
  - [ミンナノウタ](../Page/ミンナノウタ.md "wikilink")（**鵠沼 綾音**）
  - [やみツキ\!](../Page/やみツキ!.md "wikilink")（**霧島 耶絵**）
  - [ユニティマリアージュ](../Page/ユニティマリアージュ.md "wikilink")（**水奈月 柚乃**）
  - [らぶデス3 〜Realtime
    Lovers〜](../Page/らぶデス3_〜Realtime_Lovers〜.md "wikilink")（潮崎
    みなも）
  - [びんかんアスリート「そ、そこダメっ！　……おしお噴いちゃう！」](../Page/びんかんアスリート「そ、そこダメっ！_……おしお噴いちゃう！」.md "wikilink")（**八木沼
    靜**）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [天神乱漫](../Page/天神乱漫.md "wikilink")（木賊 朋花）
  - [アンバークォーツ](../Page/アンバークォーツ.md "wikilink")（**水那倉 智**、トモ）
  - [才気煥発才色兼備の君たちへ](../Page/才気煥発才色兼備の君たちへ.md "wikilink") （**天野 まみる**）
  - [ツンな彼女 デレな彼女](../Page/ツンな彼女_デレな彼女.md "wikilink") （**紫藤 遥**）
  - [Marginal Skip](../Page/Marginal_Skip.md "wikilink")（リッタ）
  - [祝福的鐘聲](../Page/祝福的鐘聲.md "wikilink")（ミリアム）
  - [夏色ストレート\!](../Page/夏色ストレート!.md "wikilink")（**有馬 かぐや**）
  - [聖剣のフェアリース](../Page/聖剣のフェアリース.md "wikilink")（春日井 春菜）
  - [DEVILS DEVEL
    CONCEPT](../Page/DEVILS_DEVEL_CONCEPT.md "wikilink")（**美波 睦月**）
  - [真説 猟奇の檻 第2章](../Page/真説_猟奇の檻_第2章.md "wikilink")（宮下 たまみ）
  - [ヘリオトロープ－それは死に至る神の愛－](../Page/ヘリオトロープ－それは死に至る神の愛－.md "wikilink")
    （**アイリス・ラーゲルゲーヴ**）
  - [メモリア](../Page/メモリア.md "wikilink")（**アリサ＝クラウス**）
  - [ステルラエクエスコーデックス](../Page/ステルラエクエスコーデックス.md "wikilink")（**児珠 十六夜**）
  - [純白交響曲](../Page/純白交響曲.md "wikilink")（**安潔莉娜·菜夏·史威爾**）
  - [77 ～And, two stars meet
    again～](../Page/77_～And,_two_stars_meet_again～.md "wikilink")（モコ）
  - [マジカルウィッチコンチェルト](../Page/マジカルウィッチコンチェルト.md "wikilink")（**フランチェスカ
    アークライト**） P.S 未確認
  - [幼なじみは大統領 My girlfriend is the
    PRESIDENT.](../Page/幼なじみは大統領_My_girlfriend_is_the_PRESIDENT..md "wikilink")（**ヱゼキエル**）
  - [だっこしてぎゅっ\!
    〜オレの嫁は抱き枕〜](../Page/だっこしてぎゅっ!_〜オレの嫁は抱き枕〜.md "wikilink")（**枕子**）
  - [クレナイノツキ](../Page/クレナイノツキ.md "wikilink")（**天野 心亞**）
  - [トロピカルKISS](../Page/トロピカルKISS.md "wikilink")（**葵 祭**）
  - [とらぶる@ヴァンパイア\!
    ～あの娘は俺のご主人さま～](../Page/とらぶる@ヴァンパイア!_～あの娘は俺のご主人さま～.md "wikilink")（近藤
    美奈子）

2010年

  - se・きらら（**秋山 望美**）
  - あまあね（今里 真）
  - 置き場がない\!（**シャノン**）
  - うたてめぐり（憐音／野狐）
  - こんそめ\!〜combination somebody〜（双葉 紺乃）
  - ななプリ。（ティア）
  - 涼風のメルト -Where wishes are drawn to each other-（**楸木原 羽衣**）
  - すてぃ〜るMyはぁと（**シャルロット＝ブランシュ**）
  - [NOBLE☆WORKS](../Page/NOBLE☆WORKS.md "wikilink")（安綱 蛍）
  - [Orange Memories](../Page/Orange_Memories.md "wikilink")（**華薪 萌衣**）
  - さくらビットマップ（**室戸 鼎**）
  - [七色航路](../Page/七色航路.md "wikilink")（**弁財天 縁**）
  - 恋神 -ラブカミ-（米望　ひなた）
  - 桜花センゴク〜信長ちゃんの恋して野望\!?〜 （**伊達 政宗ちゃん**）
  - [Sacred†Vampire](../Page/Sacred†Vampire.md "wikilink")（**遊佐 星来**）
  - よう∽ガク〜妖学園の未来は会長次第\!?〜（**猫宮 彩葉**）
  - 恋色空模様（榊）
  - RGH〜恋とヒーローと学園と〜（**寧々宮 ここ**）
  - 天の光は恋の星（**羽衣 桃花**）
  - 色に出でにけり わが恋は（楓 柚菜）
  - FATA・ANE 〜ふたあね〜 bitter\&sweet（江口 茜）
  - カスタードクリームたい焼き（霧島 かなえ）
  - [真·戀姬†無雙 〜萌將傳〜](../Page/真·戀姬†無雙.md "wikilink")（荀彧）
  - 幼なじみは大統領 My girlfriend is the PRESIDENT. ファンディスク（ヱゼキエル）
  - ふぇいばりっとSweet\!（夏山 真希奈）
  - [祝祭のカンパネラ\! -la campanella di
    festività-](../Page/祝福的鐘聲.md "wikilink")（ミリアム・ローランド）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [BLOODY†RONDO](../Page/BLOODY†RONDO.md "wikilink")（二階堂 凛子）
  - [AQUA](../Page/AQUA_\(遊戲\).md "wikilink")（月代 奈々瑠）
  - [Marguerite Sphere](../Page/Marguerite_Sphere.md "wikilink")（**此花
    茉莉**）
  - ひなたテラス 〜We don't abandon you.〜（**梶原 雛**）
  - [11eyes -Resona Forma-](../Page/11eyes_-罪與罰與贖的少女-.md "wikilink")（広原
    雪子）
  - 君を仰ぎ乙女は姫に（**星河 舞輝**）
  - カミカゼ☆エクスプローラー\!（宇佐美 沙織）
  - ダイヤミック・デイズ（**姫野川 コトラ**）
  - [特別婚\!](../Page/特別婚!.md "wikilink")（**佐渡咲**）
  - 妹恋〜しすこい〜（**如月 穂花**）
  - とらバ\!（山下 朱莉）
  - すきま桜とうその都会（春日井 咲良）
  - オトミミ∞インフィニティー（**草原 羽根美**）
  - どうして抱いてくれないのっ\!? 〜女の子だってヤりたいの\!〜（白樺 靖子）
  - やや置き場がない\!（**シャノン**）
  - 舞風のメルト- Where leads to feeling destination -（楸木原 羽衣）
  - あまあねアフター（今里 真）
  - あっぱれ\!天下御免（八坂 平和）
  - 恋色空模様 after happiness and extra hearts（榊）

<!-- end list -->

  - 2012年

<!-- end list -->

  - かみのゆ（**神品 あや乃**）
  - プリズマティックプリンセス☆ユニゾンスターズ（ファニー、水奈月 柚乃）
  - [Princess-Style](../Page/Princess-Style.md "wikilink")（**一宮 菖蒲**）
  - [英雄\*戰姬](../Page/英雄*戰姬.md "wikilink")（ドレイク）
  - 終わる世界とバースデイ（**千ヶ崎 入莉**）
  - [戀劍少女](../Page/戀劍少女.md "wikilink")（石橋美佐）
  - ヒメゴト・マスカレイド 〜お嬢様たちの戯れ〜（**瀬古 智里**）

<!-- end list -->

  - 2013年

<!-- end list -->

  - ナマイキデレーション（**夏島 みさき**）
  - お嬢様はご機嫌ナナメ（赤城 凉子）
  - ノスフェラトゥのオモチャ☆彡（メロディ・フォーク）
  - なつくもゆるる（**鹿島 ユウリ**）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [戀愛姐妹六重奏](../Page/戀愛姐妹六重奏.md "wikilink")（**美濱紗香**）
  - 恋する夏のラストリゾート（賀來小波）
  - [Innocent Girl](../Page/Innocent_Girl.md "wikilink")（天衣莓）
  - ヤキモチストリーム（浅間 花梨）

<!-- end list -->

  - 2015年

<!-- end list -->

  - ウルスラグナ〜征戦のデュエリスト〜（蒼乃 史帆）
  - 真・恋姫†英雄譚 1 〜乙女艶乱☆三国志演義［蜀］〜（荀彧）
  - スクイの小夜曲（神浦 小夜）
  - 剣聖機 アルファライド（セレネ）
  - アンラッキーリバース Unlucky Re:Birth/Reverse（アイリーン・ニルヴァーナ）
  - 花嫁ふたりは淫魔と天使。（アルマ）

<!-- end list -->

  - 2016年

<!-- end list -->

  - あけいろ怪奇譚（原田 望）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [與你相戀的戀愛配方](../Page/與你相戀的戀愛配方.md "wikilink")（**白咲美繪瑠**）\[2\]
  - 真・恋姫†夢想-革命- 蒼天の覇王\~（荀彧）

<!-- end list -->

  - 2018年

<!-- end list -->

  - 真・戀姬†夢想-革命- 孫吳的血脈（荀彧）

<!-- end list -->

  - 2019年

<!-- end list -->

  - [由夢想與色彩組成的](../Page/由夢想與色彩組成的.md "wikilink")（學園長）

### OVA

  - 女系家族～淫謀～（有宮詩苑）

  - （岡村里穗）

  - きみはぐ（芹沢まどか）

  - [空之色，水之色](../Page/空之色，水之色.md "wikilink")（空山菜摘芽）

  - （柊海 ※第3卷後）

  - 凌辱人妻温泉

  - ストリンジェンド ～エンジェルたちのプライベートレッスン～ MY BLOW JOBER （宮沢）

  - トロピカルKISS♡ 〜キュンっ♡となった花火はキレイでしょ？編（葵 祭）

### 廣播

  -
  - （[studio774内](../Page/studio774.md "wikilink")）

### CD

  - [私立秋葉原學園](../Page/私立秋葉原學園.md "wikilink") 原創廣播劇專輯「」（）

## 参考文献

<div class="references-small">

<references />

</div>

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:日本成人遊戲配音員](../Category/日本成人遊戲配音員.md "wikilink")

1.  《[假如明日天放晴](../Page/假如明日天放晴.md "wikilink")》廣播劇CD
2.