[Modt.gif](https://zh.wikipedia.org/wiki/File:Modt.gif "fig:Modt.gif")的MoDT
LOGO\]\]

**MoDT**，即**M**obile **o**n **D**esk**T**op的缩写，指的是在桌面平台上使用移动平台。

## 背景

自从有力竞争对手[AMD领先自己一步推出](../Page/AMD.md "wikilink")[Giga](../Page/Giga.md "wikilink")[Hz处理器](../Page/Hz.md "wikilink")，Intel意识到自己并不继续拥有业内龙头老大的地位，此时的Intel选择了一条错误的道路，即一味地追求高频率而放弃一直沿袭的P6架构转而使用[NetBurst架构](../Page/NetBurst.md "wikilink")。NetBurst架构的一大特点就是利用长流水线，牺牲单个周期的效率，而轻松获得较高的频率。此时出现在市场上的[Pentium
4](../Page/Pentium_4.md "wikilink")，在一些测试与应用中不如上一代较低频率的[Pentium
III处理器](../Page/Pentium_III.md "wikilink")。

随着频率的快速提升，功耗就是一个不得不忽视的问题。在上一代的长寿[Socket
370插座上](../Page/Socket_370.md "wikilink")，没有任何一款[功率高于](../Page/功率.md "wikilink")35W的处理器，而在NetBurst架构下，特别是该架构的最后一款核心[Prescott上](../Page/Pentium_4.md "wikilink")，功耗动辄上百瓦。这样的情况一方面给个人消费者在选购装配计算机时，为挑选合适的电源和散热系统增添了麻烦，另一方面也为一些计算机集群工作的单位企业增添了额外的能源支出。在同一时期，对手AMD的能耗控制却把握的比较好，也导致了一些Intel的客户开始流失、

在这样的情况下，Intel祭出MoDT这一概念，力图以[Pentium
M来挽救桌面平台高频低能的颓势](../Page/Pentium_M.md "wikilink")。后期Pentium
M的位置由[Core与](../Page/Intel_Core.md "wikilink")[Core
2继承](../Page/Intel_Core_2.md "wikilink")。

最后不得不提到一点，AMD平台似乎并不需要什么特别的MoDT宣传——[Socket
462与](../Page/Socket_462.md "wikilink")[Socket
754的针脚定义没有区分移动与桌面平台](../Page/Socket_754.md "wikilink")，移动CPU可以直接使用在桌面主板上。倒是Intel自己，当初为了划分市场，移动平台使用的[Socket
479与桌面平台](../Page/Socket_479.md "wikilink")[Socket
478并不兼容](../Page/Socket_478.md "wikilink")。

## 优势

MoDT的应用优势是很明显的：

  - 使用的是移动平台的设计，功耗相比台式机平台大大降低
  - 随着功耗降低，散热系统和电源方面的压力也大大下降，在一定程度上降低噪音，改善作业环境

此外，由于移动CPU的低功耗，而较桌面产品有着更加大的超频幅度，[超频发烧友也使用移动CPU在桌面平台的散热条件下超频而获得较好成绩](../Page/超频.md "wikilink")。

## 其他移动型产品的桌面化

  - [显卡](../Page/显卡.md "wikilink")：随着显卡频率的提升，功耗也有如CPU般愈演愈烈的趋势，商家也适时推出基于移动版本核心的桌面显卡，如在[Radeon
    9550风靡之时出现在市场的大量移动版核心](../Page/Radeon_9550.md "wikilink")9600显卡。
  - 键盘：仿笔记本键位高集中度的键盘。部分此类型产品上甚至有可代替鼠标使用的[触摸板](../Page/触摸板.md "wikilink")。

## 参见

  - [Pentium M](../Page/Pentium_M.md "wikilink")
  - [Celeron M](../Page/Celeron_M.md "wikilink")
  - [Turion 64](../Page/Turion_64.md "wikilink")
  - [AOpen](../Page/AOpen.md "wikilink")

## 外部链接

  - [MoDT平台核心CPU构成](http://www.intel.com/cd/channel/reseller/asmo-na/eng/270518.htm)
  - [Intel统一台式机笔记本CPU品牌是为了推进MoDT](http://news.ccidnet.com/art/1032/20060509/548021_1.html)

[Category:微處理器](../Category/微處理器.md "wikilink")