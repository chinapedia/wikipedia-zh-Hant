**MobileMe**是一項由[蘋果公司於](../Page/蘋果公司.md "wikilink")[WWDC
2008發佈的網路服務](../Page/WWDC#WWDC_2008.md "wikilink")，以取代[.Mac及強化其功能](../Page/.Mac.md "wikilink")。原有的.Mac服務及會員已自動轉移提升至MobileMe。此新服務自2008年7月9日開始。在[WWDC
2011](../Page/WWDC#WWDC_2011.md "wikilink")，蘋果宣布，MobileMe將不再接受新的訂閱\[1\]，並會於2012年6月30日停止MobileMe服務，原本的功能將會整合至免費的[iCloud](../Page/iCloud.md "wikilink")。\[2\]

## .Mac與MobileMe的分別

在取代而有的.Mac服務的同時（主要為網頁寄存服務），並把原來的[Mac OS
X及](../Page/Mac_OS_X.md "wikilink")[iPhone
OS用戶層面](../Page/iPhone_OS.md "wikilink")，擴展至[微軟視窗及](../Page/微軟視窗.md "wikilink")[iPod
touch等裝置的用戶上](../Page/iPod_touch.md "wikilink")。這表示此項服務不再如.Mac般只能局限同步於[蘋果公司的軟件](../Page/蘋果公司.md "wikilink")，如Mail及iCal上，現在的MobileMe能於任何能連接上MobileMe的裝置上使用。

## 特點

### 電子郵件

[電子郵件](../Page/電子郵件.md "wikilink")（[推送電子郵件](../Page/Push_Mail.md "wikilink")）服務。包括一個「@me.com」字尾的電郵地址。當收到電郵後，用戶所有連接上MobileMe的裝置均能夠即時收到該電郵。

### 通訊錄

通訊錄（Address Book）含推送功能，當任何裝置更新了通訊錄內的資料後，所有裝置均會即時同步更新。

### 行事曆

含推送功能的行事曆。當在任何裝置上更新了新的約會後，其他裝置均會即時同步更新，但實際應用不能設定自動更新，需要手動完成。

### 網路相簿

可公開的網路相簿，相片能透過網路瀏覽器上載，或通過[蘋果電腦內建的](../Page/蘋果電腦.md "wikilink")[iPhoto同步更新](../Page/iPhoto.md "wikilink")。

### iDisk

[iDisk是一個可透過網路瀏覽器](../Page/iDisk.md "wikilink")、[蘋果電腦上的](../Page/蘋果電腦.md "wikilink")[Finder或](../Page/Finder.md "wikilink")[微軟視窗上的遠端磁碟機](../Page/微軟視窗.md "wikilink")（Remote
Disk）控制的虛擬網路儲存空間，而iDisk內的檔案可以經由連結下載，也可通過電子郵件共享。

此外iDisk的流量可以由用戶控制，即用戶可以控制iDisk內的檔案，當流量或下載次數到達指定時，服務系統將停止檔案下載。

現今已被蘋果的iCloud所取代。

### Web 2.0

MobileMe使用[web
2.0技術](../Page/web_2.0.md "wikilink")，即不用安裝程式，透過網頁瀏覽器便能完全操作，並提供一個類似桌面的使用者界面作為控制操作。

### Find My iPhone

在[iPhone及](../Page/iPhone.md "wikilink")[iPad安裝Find](../Page/iPad.md "wikilink")
My
iPhone之後，遺失時可尋找iPhone及iPad位於何處，並在螢幕顯示訊息。必要時亦可遠端將iPhone及iPad鎖定或是完全刪除其資料使手機回到出廠預設狀態，以避免重要資料外洩。

## 收費

MobileMe的計劃分為兩種，分別為個人版及家庭版（一個個人帳戶），一年的收費為US$99（HK$750/NT$3,300）及US$149（HK$1150/NT$4900）。另外，可選擇加購儲存空間，額外20GB及40GB的儲存空間一年的收費為US$49（HK$400/NT$1,490）及US$99（HK$750/NT$3100）。一如以往的[.Mac](../Page/.Mac.md "wikilink")，在購買新的一台[麥金塔電腦並同時購買MobileMe服務者](../Page/麥金塔.md "wikilink")，於首年使用有特別的折扣優惠。

現時用戶可享有免費六十天共10GB容量的試用，唯用戶需登記其信用卡資料，以便在六十天試用期過後直接向用戶收取試用期過後次年的服務費。然而，服務啟用後不久，有些歐洲及澳洲的用戶發覺被蘋果公司意外地提早收取了往後一年的服務費，顯示MobileMe的服務尚未穩定，蘋果公司為了平息事件而將試用帳戶延長至四個月。\[3\]\[4\]

## 競爭

[Microsoft
Exchange亦提供類似MobileMe所提供的服務](../Page/Microsoft_Exchange.md "wikilink")。

[Google Accounts](../Page/Google_Accounts.md "wikilink")
Google提供免费Google帐号，该帐号可同时用于Gmail，Google Calendar和Google
Contacts等所有Google服务。这些服务基本涵盖MobileMe的服务，而且Mac程式如Mail，Address
Book都能够较好集成这些服务。

[HTC針對其](../Page/HTC.md "wikilink")[Android作業系統的部份手機](../Page/Android.md "wikilink")，推出[HTCSense.com的服務](../Page/HTCSense.com.md "wikilink")。

[Nokia針對部分](../Page/Nokia.md "wikilink")[S40](../Page/S40.md "wikilink")、[Symbian作業系統手機提供](../Page/Symbian.md "wikilink")[Ovi服務](../Page/Ovi_\(諾基亞\).md "wikilink")

## 瀏覽器支援

MobileMe類似桌面界面，將只支援個別的瀏覽器：

  - [Safari](../Page/Safari.md "wikilink")
    3或更新版本（[Mac](../Page/Mac.md "wikilink") +
    [PC](../Page/PC.md "wikilink")）
  - [Firefox](../Page/Firefox.md "wikilink") 2或更新版本（Mac + PC）
  - 部分支持[Internet Explorer](../Page/Internet_Explorer.md "wikilink") 7
    (PC)

## 参考文献

## 外部連結

  - [Apple - me.com](http://www.me.com)

  - [Apple - MobileMe](http://www.apple.com/mobileme)

  - [Apple - MobileMe台灣官方網頁](http://www.apple.com/tw/mobileme/)

  - [Apple - MobileMe香港官方網頁](http://www.apple.com/hk/mobileme/)

{{-}}

[Category:蘋果公司服務](../Category/蘋果公司服務.md "wikilink")
[Category:雲端運算](../Category/雲端運算.md "wikilink")
[Category:云存储](../Category/云存储.md "wikilink")
[Category:日程管理軟體](../Category/日程管理軟體.md "wikilink")
[Category:网络相册](../Category/网络相册.md "wikilink")
[Category:电子邮件网站](../Category/电子邮件网站.md "wikilink")
[Category:2008年建立的网站](../Category/2008年建立的网站.md "wikilink")
[Category:2012年廢除](../Category/2012年廢除.md "wikilink")

1.
2.
3.
4.