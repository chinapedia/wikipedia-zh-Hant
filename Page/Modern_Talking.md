****
是一支[德国流行音乐](../Page/德国.md "wikilink")[双人组合](../Page/双人组合.md "wikilink")，由
[Thomas Anders](http://de.wikipedia.org/wiki/Thomas_Anders) 和 [Dieter
Bohlen](http://de.wikipedia.org/wiki/Dieter_Bohlen) 组成。

直到目前为止，Modern
Talking在商业上最成功的德国流行组合。他们的曲调给人印象深刻，演唱的歌词则采用[英语](../Page/英语.md "wikilink")。

## Modern Talking的职业生涯

## \* [Modern Talking Chile](http://www.moderntalking.cl)

### 1984至1987间

80年代初作曲及音乐制作人Dieter Bohlen第一次遇到了在Bernd Weidung出生的歌手Thomas
Anders。起初Bohlen为Anders制作谱曲的唱片并不非常成功。

[Category:德国音乐](../Category/德国音乐.md "wikilink")