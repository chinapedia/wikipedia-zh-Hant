**Mondex**是一款使用[智能卡的](../Page/智能卡.md "wikilink")[電子收費系統](../Page/電子收費系統.md "wikilink")，最初於1990年由[英國](../Page/英國.md "wikilink")[國民西敏銀行開發](../Page/國民西敏銀行.md "wikilink")，經過5年的試驗，於1995年開始於Swindon作測試，及後多個[國家及地區均引進這套系統](../Page/國家.md "wikilink")。1996年[萬事達國際組織買下了Mondex](../Page/MasterCard.md "wikilink")
51%的股份\[1\]。1997年，英國的De La Rue公司成為了Mondex的合作伙伴。

在1996年買下Mondex後，萬事達國際組織放棄了本身開發的Mastercard
Cash[電子收費系統](../Page/電子收費系統.md "wikilink")\[2\]。然而，隨著科技的進步，傳統的Mondex接觸式電子現金業務已過時，取而代之的是非接觸式電子現金業務。在台灣，萬事達國際組織重新將非接觸式電子現金業務命名為Mastercard
Cash。\[3\]

## 香港

1990年代末，Mondex曾在[香港推廣](../Page/香港.md "wikilink")，在一些[專線小巴](../Page/專線小巴.md "wikilink")、[邨巴使用Mondex系統](../Page/邨巴.md "wikilink")，但付款不及輕觸式的[八達通系統方便](../Page/八達通.md "wikilink")，及後陸續被八達通所取代。中文大學一度聯同恆生銀行推出Mondex「中大通」作為學生證，並用作付費之用，及後在學生反對下停用。

## 台灣

2003年12月27日，[萬事達國際組織與](../Page/萬事達.md "wikilink")[宏碁團隊標得](../Page/宏碁.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")（[台灣](../Page/台灣.md "wikilink")）[高雄市政府交通局主辦之南部七縣市電子票證系統](../Page/高雄市政府.md "wikilink")。2005年9月5日起，開始試行[PayPass卡片](../Page/PayPass.md "wikilink")。2006年6月20日，結合MasterCard
Cash的[TaiwanMoney儲值卡與學生卡正式於](../Page/TaiwanMoney.md "wikilink")[台灣南部七縣市上市](../Page/台灣南部.md "wikilink")。2007年6月1日，高雄市政府交通局與TaiwanMoney
Card建置團隊宣佈TaiwanMoney Card正式運轉。

## 相關條目

  - [Taiwan Money卡](../Page/Taiwan_Money卡.md "wikilink")
  - [Visa Cash](../Page/Visa_Cash.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:英国电子公司](../Category/英国电子公司.md "wikilink")
[Category:電子貨幣](../Category/電子貨幣.md "wikilink")

1.
2.
3.  <http://www.104.com.tw/cfdocs/2000/job2000/jobmanage.cfm?invoice=70560167000&jobnum=0>