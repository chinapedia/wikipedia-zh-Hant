**Moodle**是一個[開源及](../Page/開源軟件.md "wikilink")[自由的](../Page/自由軟件.md "wikilink")[電子學習](../Page/電子學習.md "wikilink")[軟件平台](../Page/軟件平台.md "wikilink")，亦稱為[課程管理系統](../Page/課程管理系統.md "wikilink")、[學習管理系統或](../Page/學習管理系統.md "wikilink")[虛擬學習環境](../Page/虛擬學習環境.md "wikilink")。它有一個很有分量的用戶群體：根據其2010年1月的統計，現時有45,721個已註冊及查核的網站，為3200萬位用戶提供約300萬個課程\[1\]。

為一個[線上學習系統](../Page/線上學習.md "wikilink")，為全世界有150餘國70種語言所使用，其特色異於其他商業[線上教學平臺](../Page/線上教學平臺.md "wikilink")，屬於[開放原始碼的類別](../Page/開放原始碼.md "wikilink")。創建者為[馬丁·多基馬](../Page/馬丁·多基馬.md "wikilink")（）。

Moodle 的軟件最低安裝要求為 PHP 5.6.5 及 MariaDB 5.5.31 / MySQL 5.5.31 / Postgres
9.3 / MSSQL 2008 / Oracle 10.2。

## 名稱來源

Moodle 的名字源於英文簡寫
**，即[模組化](../Page/模組化.md "wikilink")[物件導向動態學習](../Page/物件導向.md "wikilink")[環境](../Page/環境.md "wikilink")，儘管其最初名稱裡，開首的"M"字本來是創建者[馬丁·多基馬](../Page/馬丁·多基馬.md "wikilink")（）的名字的第一個字母。

## 其他類似平台

  - [ANGEL LMS](../Page/ANGEL_Learning.md "wikilink")
  - [ATutor](../Page/ATutor.md "wikilink")
  - [Blackboard](../Page/Blackboard_Inc..md "wikilink")
  - [Claroline](../Page/Claroline.md "wikilink")
  - [Desire2Learn](../Page/Desire2Learn.md "wikilink")
  - [Dokeos](../Page/Dokeos.md "wikilink")
  - [eFront](../Page/EFront_-_eLearning_and_Human_Capital_Development.md "wikilink")
  - [ILIAS](../Page/ILIAS.md "wikilink")
  - [OLAT](../Page/OLAT.md "wikilink")
  - [Sakai](../Page/Sakai项目.md "wikilink")
  - [WebCT](../Page/WebCT.md "wikilink")
  - [WMPro](../Page/WMPro.md "wikilink")

## 參看

  - [學習管理系統](../Page/學習管理系統.md "wikilink")
  - [線上學習社群](../Page/線上學習社群.md "wikilink")

## 參考資料

## 外部連結

### 官方Moodle資源

  - [Moodle.org](http://moodle.org/)

  - [如何安裝Moodle](http://docs.moodle.org/en/Installing_Moodle)

  - [Moodle的所有志願翻譯者](http://docs.moodle.org/en/Translation_credits)

  - [Moodle Partners](http://moodle.com/partners/) private companies
    affiliated with Moodle

  - [Demo, comparisons with other e-Learning solutions and video
    tutorials](http://demolabo.com/moodle)

### 非官方Moodle資源

  - [Moodle中文加油站（Moodle Support in
    Taiwan）](https://web.archive.org/web/20140214040045/http://moodle.tw/moodle/)—Moodle的正體中文化、教育訓練課程、教學研究與技術支援服務。
  - [易魔灯（Easymoodle）](http://www.emoodle.org/)—Moodle简体中文化网站、致力于Moodle主程序及其插件模块的简体中文化，并提供Windows平台下的一体化安装使用程序EasyMoodle。
  - [魔灯中国（CMOODLE）](http://cm2.cmoodle.cn/)—Moodle简体中文体验平台。
  - [爱班moodle](https://archive.is/20130426170514/http://www.iclass.com/)-Moodle简体校内平台。
  - [The Hungarian Moodle Community](http://moodlemoot.hu/)
  - [Free Moodle Hosting](http://www.keytoschool.com/)

### 報告及文章

  - [Article explaining Moodle for beginning users. Published on
    techsoup.org](https://web.archive.org/web/20080606225006/http://www.techsoup.org/learningcenter/internet/page4814.cfm)
  - [Blackboard vs. Moodle. A Comparison of Satisfaction with Online
    Teaching and Learning
    Tools](https://archive.is/20050309042101/http://www.humboldt.edu/~jdv1/moodle/all.htm)
  - [Blackboard Online System to be Replaced -New System Moodle More
    Effective](https://web.archive.org/web/20100819015024/http://xpress.sfsu.edu/archives/tech/003770.html)
  - [Bob McDonald "E-Learning at Cranbrook: Up Close and Personal"
    (2004) Cranbrook
    Schools](https://web.archive.org/web/20071012203651/http://scholarsearchassoc.com/MICRA043004.htm)
  - Graf S., List, B. (2005) *[An Evaluation of Open Source E-Learning
    Platforms Stressing Adaptation
    Issues](https://web.archive.org/web/20100816055302/http://www.wit.at/people/list/publications/icalt2005.pdf)*
    - an evaluation of 9 open source E-Learning Platforms.

[Category:PHP](../Category/PHP.md "wikilink")
[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:虛擬學習環境](../Category/虛擬學習環境.md "wikilink")

1.