**NCSA
Mosaic**，是一個早期停產的[網頁瀏覽器](../Page/網頁瀏覽器.md "wikilink")，也是早期網際網路[協議的客戶端如](../Page/網路傳輸協定.md "wikilink")[FTP](../Page/FTP.md "wikilink")、[NNTP和](../Page/NNTP.md "wikilink")[Gopher](../Page/Gopher_\(網路協定\).md "wikilink")。該瀏覽器因支援多種網際網路協定而命名\[1\]。其直觀的介面、可靠性和簡易安裝，因此在當時人氣爆發的大受歡迎\[2\]。Mosaic也是第一個可以在文字中嵌入圖片，而不是在單獨的視窗中顯示圖片的瀏覽器\[3\]\[4\]。雖然經常被誤認為世界上第一個[圖形介面瀏覽器](../Page/圖形使用者介面.md "wikilink")，但事實上比Mosaic更早的還有[WorldWideWeb](../Page/WorldWideWeb.md "wikilink")、鮮少人知的[Erwise](../Page/Erwise.md "wikilink")\[5\]和[ViolaWWW](../Page/ViolaWWW.md "wikilink")。

Mosaic於1992年底由[美國](../Page/美國.md "wikilink")[伊利諾大學厄巴納香檳分校的](../Page/伊利諾大學厄巴納香檳分校.md "wikilink")[NCSA開發](../Page/国家超级电脑应用中心.md "wikilink")\[6\]。NCSA於1993年發布瀏覽器\[7\]，並於1997年1月7日正式停止發展和支援\[8\]。

## 歷史

[Mosaic_1.0_for_Mac.png](https://zh.wikipedia.org/wiki/File:Mosaic_1.0_for_Mac.png "fig:Mosaic_1.0_for_Mac.png")，進入Mosaic通訊公司（後來的[Netscape](../Page/Netscape.md "wikilink")）官方網站\]\]
David
Thompson向NCSA的軟體設計小組展示了[ViolaWWW瀏覽器](../Page/ViolaWWW.md "wikilink")\[9\]。[馬克·安德森和](../Page/馬克·安德森.md "wikilink")[埃里克·比納最初為](../Page/埃里克·比納.md "wikilink")[UNIX的](../Page/UNIX.md "wikilink")[X
Window編寫了NCSA](../Page/X_Window.md "wikilink")
Mosaic，名為*xmosaic*\[10\]\[11\]\[12\]\[13\]。1991年12月，參議員[艾爾·高爾提出的](../Page/艾爾·高爾.md "wikilink")得到通過，為Mosaic計畫提供了資金。馬克·安德森於1993年1月23日宣佈了這個計畫\[14\]。第一個Alpha版本（編號0.1a）於1993年6月發布，1993年9月，第一個Beta版本（編號0.6b）緊接著發布。1993年11月11日，Microsoft
Windows的1.0版本發布\[15\]\[16\]。1993年11月10日，Unix（X-Windows）的2.0版本發布\[17\]。從1994年至1997年，[國家科學基金會資助了Mosaic的進一步發展](../Page/國家科學基金會.md "wikilink")\[18\]。

馬克·安德森是開發Mosaic的團隊領導人，他離開了NCSA，後來與[Silicon
Graphics](../Page/Silicon_Graphics.md "wikilink")（SGI）公司的創始人之一[吉姆·克拉克以及伊利諾大學的其他四名學生和員工共同創辦了Mosaic](../Page/吉姆·克拉克_\(企業家\).md "wikilink")
Communication Corporation\[19\]，Mosaic Communications最後改名為[Netscape
Communications
Corporation](../Page/Netscape_Communications_Corporation.md "wikilink")，並推出他們的瀏覽器[Netscape
Navigator](../Page/Netscape_Navigator.md "wikilink")。在1994年安德森的Netscape
Navigator發布之後，Mosaic瀏覽器的受歡迎程度開始下降。這一點在*The HTML Sourcebook: The Complete
Guide to
HTML*已經提到：「Netscape通訊公司設計了一個全新的WWW瀏覽器Netscape，比原來的Mosaic程式有明顯的改進。」\[20\]

## 授權

NCSA Mosaic的授權條款對於專有軟體程式是慷慨的。一般而言，所有版本的非商業用途都是免費的（有某些限制）。此外，-{X
Window}-系統／Unix版本公開提供[原始碼](../Page/原始碼.md "wikilink")（其他版本的原始碼在協議簽署後可用）。截至1993年，授權持有者包括：\[21\]

  -
  - [Fujitsu公司](../Page/Fujitsu.md "wikilink")（產品：Infomosaic，Mosaic日文版。價格：日元5,000（約合50美元）\[22\]

  - （產品：Mosaic非商業版本，可以使用Mosaic作為商業資料庫工作的一部分）

  - Quadralay公司（產品：Mosaic消費者版本。價格：249美元）

  - Quarterdeck公司

  - [Santa Cruz Operation公司](../Page/聖克魯茲作業.md "wikilink")

  - [SPRY公司](../Page/SPRY.md "wikilink")（產品：Air Mail、Air News、Air
    Mosaic通訊套件。價格：149至399美元）

  - [Spyglass公司](../Page/Spyglass.md "wikilink")（產品：重新授權給其他供應商）

## 影響

Mosaic是導致1990年代[網路熱潮的網頁瀏覽器](../Page/網際網路泡沫.md "wikilink")。在此期間存在的其他瀏覽器，特別是[Erwise](../Page/Erwise.md "wikilink")、[ViolaWWW](../Page/ViolaWWW.md "wikilink")、[MidasWWW和](../Page/MidasWWW.md "wikilink")，在公眾使用網際網路上並沒有與Mosaic相同的效應\[23\]。

Mosaic不是第一個用於Windows的瀏覽器，而是Thomas R.
Bruce鮮為人知的[Cello](../Page/Cello_\(瀏覽器\).md "wikilink")。在Windows版本支援之前，Unix版本的Mosaic就已經很出名了，可以在文字中顯示圖片，而不是顯示在單獨的視窗。Mosaic是第一個由全職程式設計師團隊編寫和支援的瀏覽器，對於初學者來說相當可靠又簡易。1995年的[市佔率已經達到](../Page/網頁瀏覽器的使用分佈#2000年以前的報告.md "wikilink")53%。

1992年11月，世界上只有26個網站，每一個網站都受人注目\[24\]。1993年，Mosaic推出了一個叫做What’s
New的頁面，每天都會提供給大家一個全新網站的連結\[25\]。這是一個網際網路的使用率在學術界和大型工業研究機構領域之外迅速擴張的時代。由於瀏覽器本身的易用性，推動了網路爆炸性的成長，到了1995年8月網站數量已經超過了1萬個，1998年達到了數百萬個網站\[26\]。Metcalfe表達了Mosaic的關鍵作用：

## 分支

[Spyglass公司從NCSA獲得技術和商標授權](../Page/Spyglass公司.md "wikilink")，用於開發自己的網頁瀏覽器，但從未使用任何NCSA
Mosaic原始碼\[27\]。[微軟於](../Page/微軟.md "wikilink")1995年以200萬美元獲得Spyglass
Mosaic的授權進行了修改，並重新命名為[Internet
Explorer](../Page/Internet_Explorer.md "wikilink")\[28\]。但後來因為專利方面的糾紛，微軟向Spyglass賠償了800萬美元\[29\]。在1995年的使用者指南*The
HTML Sourcebook: The Complete Guide to HTML*，特別指出，在一個名為*Coming
Attractions*的章節中，Internet Explorer「將以Mosaic程式為基礎」\[30\]。

在NCSA停止Mosaic開發之後，-{X Window}-系統的NCSA
Mosaic原始碼由幾個獨立的小組繼續進行開發。這些獨立的開發工作包括2004年初停止開發的mMosaic（multicast
Mosaic）\[31\]、Mosaic-CK和VMS Mosaic。

## 參考文獻

## 外部链接

  -
## 参见

  - [网页浏览器列表](../Page/网页浏览器列表.md "wikilink")
  - [网页浏览器比较](../Page/网页浏览器比较.md "wikilink")

[Category:1993年軟體](../Category/1993年軟體.md "wikilink")
[Category:跨平台軟體](../Category/跨平台軟體.md "wikilink")
[Category:軟體史](../Category/軟體史.md "wikilink")
[Category:網際網路的歷史](../Category/網際網路的歷史.md "wikilink")
[Category:網頁瀏覽器](../Category/網頁瀏覽器.md "wikilink")

1.

2.

3.

4.

5.

6.
7.

8.

9.

10.
11.
12.
13.

14.

15.

16.

17.

18.

19.

20.

21.

22.

23.

24.

25.

26.

27.

28.

29.

30.

31.