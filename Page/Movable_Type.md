**Movable
Type**，简称MT，是由位于[美国](../Page/美国.md "wikilink")[加州的](../Page/加州.md "wikilink")[Six
Apart公司推出的](../Page/Six_Apart.md "wikilink")[網誌](../Page/網誌.md "wikilink")（blog）发布系统。它是全球最受欢迎的網誌系统之一，包含多用户，评论，引用（[TrackBack](../Page/TrackBack.md "wikilink")），主题等功能，并广泛的支持各种[第三方](../Page/第三方.md "wikilink")[插件](../Page/插件.md "wikilink")。

Movable
Type不仅可以应用于个人的[網誌系统](../Page/網誌.md "wikilink")，而且可以应用于商业、教育等领域。Movable
Type于2007年12月12日正式宣布以GPLv2的协议开源。\[1\]

## 竞争对手

  - [WordPress](../Page/WordPress.md "wikilink")

## 外部链接

  - [Movable Type官方网站](http://www.sixapart.com/)
  - [Movable Type社群网站](http://www.movabletype.org/)

## 参考文献

<div class='references-small'>

<references/>

</div>

[Category:內容管理系統](../Category/內容管理系統.md "wikilink")
[Category:網誌軟體](../Category/網誌軟體.md "wikilink")

1.