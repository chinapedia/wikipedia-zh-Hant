下列是**[Mozilla基金会](../Page/Mozilla基金会.md "wikilink")／[Mozilla公司产品列表](../Page/Mozilla公司.md "wikilink")**。除特别说明外，所有的产品从设计上都是[跨平台的](../Page/跨平台.md "wikilink")。

## 客户端程序

  - [SeaMonkey](../Page/SeaMonkey.md "wikilink")（原先的[Mozilla Application
    Suite](../Page/Mozilla_Application_Suite.md "wikilink")）–
    一个[互联网套件](../Page/互联网套件.md "wikilink")。
      - [Mozilla Mail &
        Newsgroups](../Page/Mozilla_Mail_&_Newsgroups.md "wikilink") –
        [电子邮件及](../Page/电子邮件.md "wikilink")[新闻组组件](../Page/新闻组.md "wikilink")。
      - [Mozilla Composer](../Page/Mozilla_Composer.md "wikilink") –
        [HTML编辑器组件](../Page/HTML编辑器.md "wikilink")。
      - [ChatZilla](../Page/ChatZilla.md "wikilink") –
        [IRC组件](../Page/IRC.md "wikilink")。
  - [Mozilla Firefox](../Page/Mozilla_Firefox.md "wikilink") –
    一个[网页浏览器](../Page/网页浏览器.md "wikilink")。
  - [Mozilla Firefox
    Mobile](../Page/Mozilla_Firefox_Mobile.md "wikilink") –
    一個基於行動裝置的[网页浏览器](../Page/网页浏览器.md "wikilink")。
  - [Mozilla Thunderbird](../Page/Mozilla_Thunderbird.md "wikilink") –
    一个[电子邮件及新闻组客户端](../Page/电子邮件客户端.md "wikilink")。

## 组件

  - [Gecko](../Page/Gecko.md "wikilink") –
    一个[渲染引擎](../Page/渲染引擎.md "wikilink")。
  - [Necko](../Page/Necko.md "wikilink") – 一个网络库。
  - [SpiderMonkey](../Page/SpiderMonkey.md "wikilink") –
    由[C语言写成的](../Page/C语言.md "wikilink")[JavaScript引擎](../Page/JavaScript引擎.md "wikilink")。
  - [Rhino](../Page/Rhino_\(JavaScript引擎\).md "wikilink") –
    由[Java语言写成的](../Page/Java.md "wikilink")[JavaScript引擎](../Page/JavaScript引擎.md "wikilink")。
  - [DOM Inspector](../Page/DOM_Inspector.md "wikilink") –
    一个[DOM查看器](../Page/DOM.md "wikilink")。
  - [Venkman](../Page/Venkman.md "wikilink") –
    一个[JavaScript调试器](../Page/JavaScript.md "wikilink")。
  - [Rust](../Page/Rust.md "wikilink") – 一个程式語言。
  - [Servo](../Page/Servo.md "wikilink") –
    一个[渲染引擎](../Page/渲染引擎.md "wikilink")。

## 开发者工具

  - [Bugzilla](../Page/Bugzilla.md "wikilink") –
    一个[缺陷跟踪管理系统](../Page/缺陷跟踪管理系统.md "wikilink")。

  - [Bonsai](../Page/Bonsai_\(軟體\).md "wikilink") –
    一个[网络](../Page/网络应用程序.md "wikilink")[CVS前端界面](../Page/CVS.md "wikilink")。

  - [Treeherder](../Page/mozillawiki:Auto-tools/Projects/Treeherder.md "wikilink")
    – 一个允许开发者管理软件编译以及确定在特定平台和个别代码更改情况下的编译失败的探测工具。

  - – 一個開放、可擴充的雲端程式碼編輯器。

  - [Pontoon](../Page/Pontoon.md "wikilink") – 網站在地化工具。

## API及库文件

  - （NSPR） – 一个平台抽象庫。

  - [Personal Security
    Manager](../Page/Personal_Security_Manager.md "wikilink")（PSM）

  - [Network Security Services for
    Java](../Page/Network_Security_Services_for_Java.md "wikilink")（JSS）

  - [Network Security
    Services](../Page/Network_Security_Services.md "wikilink")（NSS）

## 其他工具

  - [Mozbot](../Page/Mozbot.md "wikilink") –
    一个[IRC机器人](../Page/IRC.md "wikilink")。

  - [Mstone](../Page/Mstone.md "wikilink")

  - [Client Customization
    Kit](../Page/Client_Customization_Kit.md "wikilink")（CCK）

  - [Mozilla Directory SDK](../Page/Mozilla_Directory_SDK.md "wikilink")

  - – 一個可以同時提供電子郵件、Twitter、Youtube影音播放等功能的平台。

## 技术

  - [XUL](../Page/XUL.md "wikilink") – 一種使用者介面標示語言。

  - [XBL](../Page/XBL.md "wikilink") –
    一種基於XML的置標語言用於聲明XUL-widgets和XML元素的行為和外觀。

  - – 允許擴充套件開發者使用ECMAScript或C++在建立新的XML名稱空間。

  - [JavaScript](../Page/JavaScript.md "wikilink") – 原先[Netscape
    Navigator使用的事实上的客户端脚本语言](../Page/Netscape_Navigator.md "wikilink")。

  - [NPAPI](../Page/NPAPI.md "wikilink") – 建構外掛程式和瀏覽器之間的介面。

  - [XPCOM](../Page/XPCOM.md "wikilink") – 一種跨平台元件物件模型。

  - [XPConnect](../Page/XPConnect.md "wikilink") –
    一種在XPCOM和JavaScript之間綁定的技術。

  - [XPInstall](../Page/XPInstall.md "wikilink") – 一種用於安裝擴充套件的技術。

## 手機作業系統

  - [Firefox OS](../Page/Firefox_OS.md "wikilink") –
    一個基於[開放原始碼以](../Page/開放原始碼.md "wikilink")[HTML5建構的手機和平板電腦作業系統](../Page/HTML5.md "wikilink")。

## 被放弃的产品

  - [Camino](../Page/Camino.md "wikilink") – 一个[Mac OS
    X下的浏览器](../Page/Mac_OS_X.md "wikilink")。

  - [Mariner](../Page/Mariner.md "wikilink") – 網景時期的渲染引擎，取而代之的是Gecko。

  - [ElectricalFire](../Page/ElectricalFire.md "wikilink") –
    一個基於[即時編譯的](../Page/即時編譯.md "wikilink")[虛擬機](../Page/虛擬機.md "wikilink")。

  - [Xena](../Page/Xena_\(web_browser\).md "wikilink") ("Javagator") –
    一種[Java](../Page/Java.md "wikilink")[程式語言](../Page/程式語言.md "wikilink")。

  - –一個能收發電子郵件與新聞閱讀的Java開源專案。

  - [Minimo](../Page/Minimo.md "wikilink") –
    一個基於行動裝置的[网页浏览器](../Page/网页浏览器.md "wikilink")。

  - [Mozilla Application
    Suite](../Page/Mozilla_Application_Suite.md "wikilink") –
    結合網頁瀏覽器、電子郵件客戶端、HTML編輯器的套裝軟體。

  - [Mozilla Sunbird](../Page/Mozilla_Sunbird.md "wikilink") –
    一个[日程表客户端](../Page/日程表.md "wikilink")。

      - –
        原先计划成为套件中的日历组件，现在成为[Lightning的基础](../Page/Lightning.md "wikilink")。

## 参见

  - [Mozilla基金会](../Page/Mozilla基金会.md "wikilink")

## 外部链接

  - [Mozilla.org-{zh-hans:项目; zh-hant:專案; zh-tw:專案;}-列表
    ](http://www.mozilla.org/projects/)

[Category:Mozilla](../Category/Mozilla.md "wikilink")
[Category:软件列表](../Category/软件列表.md "wikilink")