**MyIE**是一位网名为changyou（畅游）的[程序员在](../Page/程序员.md "wikilink")1999年左右编写和发布的基于[Trident引擎](../Page/Trident_\(排版引擎\).md "wikilink")（[IE核心](../Page/Internet_Explorer.md "wikilink")）的多页面[浏览器](../Page/浏览器.md "wikilink")。MyIE采用标签式界面，占用资源低于[IE6](../Page/IE6.md "wikilink")，且具有[鼠标手势](../Page/鼠标手势.md "wikilink")、视觉化书签等实用功能，因此在发布后广为流传。该软件为[免费软件](../Page/免费软件.md "wikilink")，作者畅游此后还将其[开放源代码](../Page/开放源代码.md "wikilink")，[源程序采用](../Page/源程序.md "wikilink")[Visual
C++编写](../Page/Visual_C++.md "wikilink")。中国浏览器MyIE2\[1\]（后改名[Maxthon](../Page/Maxthon.md "wikilink")\[2\]，2.x起为重新开发）与网际畅游（后改名[GreenBrowser](../Page/GreenBrowser.md "wikilink")）等早期都是基于MyIE的源代码改进而成的[复刻软件](../Page/复刻_\(软件工程\).md "wikilink")。

MyIE作者畅游曾在其MyIE软件官方网站[友情链接](../Page/友情链接.md "wikilink")“[转法轮](../Page/转法轮_\(书\).md "wikilink")”网站以及公布可以[突破网络审查的](../Page/突破网络审查.md "wikilink")[代理服务器地址](../Page/代理服务器.md "wikilink")，后来此人不知所踪，MyIE的官方网站也遭[封禁](../Page/中华人民共和国网络审查.md "wikilink")。软件最终版本定格在3.2版。

## 参考资料

## 外部连接

  - [MyIE官方网站的网页存档](https://web.archive.org/web/20050207092439/http://web.qx.net/nemesis2/myie.html)

[Category:自由网页浏览器](../Category/自由网页浏览器.md "wikilink")
[Category:免費軟件](../Category/免費軟件.md "wikilink")
[Category:开放源代码](../Category/开放源代码.md "wikilink")

1.
2.