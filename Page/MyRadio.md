**MyRadio**是香港一個[網上電台](../Page/網上電台.md "wikilink")，由資深傳媒人[黃毓民及](../Page/黃毓民.md "wikilink")[梁錦祥於](../Page/梁錦祥.md "wikilink")2007年創辦。

MyRadio曾於2008年與網站[香港人網合併](../Page/香港人網.md "wikilink")。2010年9月6日，與香港人網分拆，重新獨立運作。MyRadio與同由黃毓民創辦的[普羅政治學苑合作緊密](../Page/普羅政治學苑.md "wikilink")，是其網上平台。

MyRadio每天提供6.5小時多元化廣播節目，內容包括資訊、政治、娛樂與文藝等。節目也透過其網站「myradio.hk」作網上即時廣播，亦提供網上重溫節目服務。

## 網台節目

### 節目直播時間表

<table style="width:80%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 60%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><strong>時間</strong></p></td>
<td><p><strong>第一台</strong>[1]</p></td>
<td><p><strong>第二台</strong>[2]</p></td>
</tr>
<tr class="even">
<td><p><strong>星期一</strong></p></td>
<td><p><strong>星期二</strong></p></td>
<td><p><strong>星期三</strong></p></td>
</tr>
<tr class="odd">
<td><p>18:00-18:30</p></td>
<td><p>贏爆洲際盃</p></td>
<td><p>rowspan=1 </p></td>
</tr>
<tr class="even">
<td><p>18:30-19:00</p></td>
<td></td>
<td><p>rowspan=2 </p></td>
</tr>
<tr class="odd">
<td><p>19:00-19:30</p></td>
<td><p>rowspan=1 </p></td>
<td></td>
</tr>
<tr class="even">
<td><p>19:30-20:00</p></td>
<td><p>梁錦祥經典重溫</p></td>
<td><p>澳門街</p></td>
</tr>
<tr class="odd">
<td><p>20:00-20:30</p></td>
<td><p>崑崙風</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>20:30-21:00</p></td>
<td><p>天天天藍</p></td>
<td><p>colspan=5 </p></td>
</tr>
<tr class="odd">
<td><p>21:00-21:30</p></td>
<td><p>今晚開你波</p></td>
<td><p>今晚開你波</p></td>
</tr>
<tr class="even">
<td><p>21:30-22:00</p></td>
<td></td>
<td><p>富易袋</p></td>
</tr>
<tr class="odd">
<td><p>22:00-23:00</p></td>
<td><p>毓民踩場</p></td>
<td><p>三個女人蜜蜜針</p></td>
</tr>
<tr class="even">
<td><p>23:00-23:30</p></td>
<td><p>colspan=1 </p></td>
<td><p>colspan=3 </p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><p>海外友好網台節目</p></td>
<td><p>第一期網台班節目</p></td>
<td><p>重播節目</p></td>
</tr>
</tbody>
</table>

### 其他節目

那些年

#### 毓民踢爆

不定時在黃毓民的 Facebook 戶口raymond yukman wong直播\[3\]，然後上傳至 MyRadio Hong Kong 在
YouTube 的頻道。\[4\]

## 歷史

2007年，由於[社會民主連線的領導層不滿](../Page/社會民主連線.md "wikilink")[人民台節目](../Page/香港人民廣播電台.md "wikilink")《風波裡的龍門陣》經常批評該黨[立法會議員](../Page/立法會議員.md "wikilink")[梁國雄](../Page/梁國雄.md "wikilink")，故該黨主席[黃毓民與副台長](../Page/黃毓民.md "wikilink")[梁錦祥另起爐灶](../Page/梁錦祥.md "wikilink")，成立MyRadio。電台在2007年2月5日開始試播，隨後於3月18日正式啟播，梁錦祥擔任創台台長。MyRadio原欲透過網上電台的經驗從而建立一個[大氣電波電台](../Page/大氣電波.md "wikilink")，但[香港特區政府每次皆以不同理由駁回電台新頻譜的申請](../Page/香港特區政府.md "wikilink")。\[5\]\[6\]\[7\]\[8\]

2008年初，MyRadio與當時節目主持之一[蕭若元創立的香港人網合併](../Page/蕭若元.md "wikilink")。\[9\]MyRadio的主持多為知名人士，星期一至星期日均24小時提供時事、財經、消閒及娛樂節目，當中包括一星期6日6.5小時直播節目。MyRadio以三種不同方式提供聲音廣播，包括使用[SHOUTcast串流直播](../Page/SHOUTcast.md "wikilink")、下載重溫和[Podcast訂閱服務](../Page/Podcast.md "wikilink")。除提供聲音廣播和[Phone-in節目](../Page/Phone-in.md "wikilink")，由2008年6月25日起，大部分直播節目亦提供視像直播，或是將某部分節目影像上載到[YouTube](../Page/YouTube.md "wikilink")，令香港人網成為香港首個進行跨聲音及影像直播及重溫的[網上電台](../Page/網上電台.md "wikilink")。\[10\]\[11\]

2008年4月，MyRadio的節目每日點擊率成為香港網上電台之冠。\[12\]
同時MyRadio亦被《[PCM電腦廣場](../Page/PCM電腦廣場.md "wikilink")》稱為「收聽人數最多的一個網上電台」，每周收聽率約3萬人。\[13\]
香港人網自開台後，節目內容亦不乏得到傳媒報導。\[14\]\[15\]\[16\]\[17\]\[18\]

2008年12月1日，蕭若元透露，由於資金問題，MyRadio節目會由兩個台合併為一個台，播放時間大約會縮減少三分之一。\[19\]

2009年6月1日，梁錦祥因與蕭若元不和而辭去MyRadio台長職位，部份原MyRadio節目交由其他[獨立媒體自行製作](../Page/獨立媒體.md "wikilink")，並以合作形式於MyRadio播放。

2010年年中，蕭若元透露，由於受到[中國共產黨對他的朋友施以壓力](../Page/中國共產黨.md "wikilink")，MyRadio與香港人網分拆。2010年9月6日，MyRadio與香港人網分拆，重新以獨立網上電台形式運作，再次由梁錦祥任台長。

2013年4月，黃毓民因與蕭若元意見不合而分拆。

2014年3月17日，MyRadio與[熱血時報合作開設早晨節目](../Page/熱血時報.md "wikilink")《大香港早晨》，早上8時至10時在MyRadio與熱血時報同步直播。而在搬台期間，[救生員Marco](../Page/救生員.md "wikilink")
Lau為技術總監，是一位熱心義工，他在[投資銀行工作](../Page/投資銀行.md "wikilink")，曾為[林匡正助選](../Page/林匡正.md "wikilink")。

2015年9月，黃毓民宣佈將會進行節目重組，開設第二台。

2016年10月，MyRadio推出新節目「毓民踢爆」。節目的具體安排是，黃毓民會在不預設的時間及地點，以 Facebook
作出直播。Facebook 直播之後，MyRadio會將該片存檔到 MyRadio 的 YouTube 視像重溫頻道。

2017年3月31日起，因為資金問題，MyRadio不再與熱血時報聯播節目《大香港早晨》。

## 主持

### 現任

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>主持節目</p></th>
<th><p>簡介</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/黃毓民.md" title="wikilink">黃毓民</a>（教主）</p></td>
<td><p>毓民踩場<br />
毓民踢爆<br />
毓民踢爆 之 說文解字（已完結）<br />
歷史在笑（已完結）<br />
毓民會客室<br />
今晚開你波(暫代主持)<br />
毓民射馬<br />
那些年（已完結）</p></td>
<td><p><a href="../Page/時事評論員.md" title="wikilink">時事評論員</a>、<a href="../Page/癲狗日報.md" title="wikilink">癲狗日報</a>、<a href="../Page/普羅政治學苑.md" title="wikilink">普羅政治學苑創辦人</a>[20]及校長[21]。曾任<a href="../Page/立法會議員.md" title="wikilink">立法會議員</a>(2008-2016)。並為<a href="../Page/人民力量.md" title="wikilink">人民力量創黨主席</a>（已退黨）、<a href="../Page/社會民主連線.md" title="wikilink">社會民主連線創黨主席</a>（已退黨）。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梁錦祥.md" title="wikilink">梁錦祥</a>（台長）</p></td>
<td><p>毓民踩場<br />
神秘之夜<br />
MyMusic<br />
MyUniversity<br />
梁錦祥一週時事<br />
人文講場<br />
四眼乜乜乜 (暫代主持/已完結)<br />
城邦論壇（已腰斬）<br />
崑崙風（嘉賓主持）</p></td>
<td><p>資深傳媒人、普羅政治學苑副主席、MyRadio創台台長。現時是MyRadio台長、<a href="../Page/癲狗日報.md" title="wikilink">癲狗日報電子版總編輯</a>。曾任<a href="../Page/癲狗日報.md" title="wikilink">癲狗日報總編輯</a>、<a href="../Page/香港人民廣播電台.md" title="wikilink">香港人民廣播電台副台長</a>（註冊社團副主席）、網媒<a href="../Page/本土新聞.md" title="wikilink">本土新聞總編輯</a>、OurTV及OurRadio台長。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/何故.md" title="wikilink">何故</a></p></td>
<td><p>神秘之夜</p></td>
<td><p>跨媒體創作人、作家、編劇、影評人、大學講師、文化研究員、遊戲設計師、演員、電視台及電台節目主持</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/林紀陶.md" title="wikilink">林紀陶</a>（紀陶）</p></td>
<td><p>神秘之夜</p></td>
<td><p>多媒體創作人、電影編劇、影評人、電台主持及動漫編導</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/謝義方.md" title="wikilink">謝義方</a>（謝公）</p></td>
<td><p>天天天藍</p></td>
<td><p><a href="../Page/中國國民黨.md" title="wikilink">中國國民黨黨員</a>。神州青年服務社兩岸事務發言人。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/錢建榮.md" title="wikilink">錢建榮</a></p></td>
<td><p>天天天藍</p></td>
<td><p>中國國民黨黨員</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/宋景輝.md" title="wikilink">宋景輝</a></p></td>
<td><p>天天天藍</p></td>
<td><p>中國國民黨黨員。前屯門<a href="../Page/區議員.md" title="wikilink">區議員</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張三_(香港).md" title="wikilink">張三</a></p></td>
<td><p>天天天藍</p></td>
<td><p>中國國民黨黨員</p></td>
</tr>
<tr class="odd">
<td><p>Kenny</p></td>
<td><p>天天天藍</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張文龍.md" title="wikilink">張文龍</a>（阿龍）</p></td>
<td><p>天天天藍</p></td>
<td><p>中國國民黨黨員、<a href="../Page/神州青年服務社.md" title="wikilink">神州青年服務社成員</a></p></td>
</tr>
<tr class="odd">
<td><p>鐵男</p></td>
<td><p>崑崙風<br />
坐風雲起時（已完結）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳達仁.md" title="wikilink">陳達仁</a>（阿仁）</p></td>
<td><p>達仁在線</p></td>
<td><p>資深殯儀禮儀經理、《小仁物製作》製作經理。曾參與多套電影殯儀場景指導及演出。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/安采妮.md" title="wikilink">安采妮</a></p></td>
<td><p>達仁在線</p></td>
<td><p>香港演員及電台DJ</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/無妄齋.md" title="wikilink">無妄齋</a>（齋主）</p></td>
<td><p>合眾唯識（嘉賓主持）(已完結)<br />
本土最前線</p></td>
<td><p>網絡專欄作家</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳浩天.md" title="wikilink">陳浩天</a></p></td>
<td><p>合眾唯識(已完結)<br />
本土最前線</p></td>
<td><p><a href="../Page/香港民族黨.md" title="wikilink">香港民族黨召集人</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/周浩輝.md" title="wikilink">周浩輝</a></p></td>
<td><p>合眾唯識(已完結)<br />
本土最前線</p></td>
<td><p><a href="../Page/香港民族黨.md" title="wikilink">香港民族黨發言人</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/周竪峰.md" title="wikilink">周竪峰</a>（張翼）</p></td>
<td><p>學生主場 (已完結)<br />
誰主香江(已完結)<br />
本土最前線</p></td>
<td><p><a href="../Page/香港中文大學學生會.md" title="wikilink">香港中文大學學生會會長</a></p></td>
</tr>
<tr class="odd">
<td><p>何鈞（HK）</p></td>
<td><p>學生主場 (已完結)<br />
誰主香江</p></td>
<td><p><a href="../Page/中大本土學社.md" title="wikilink">中大本土學社召集人</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/賴珮.md" title="wikilink">賴珮</a></p></td>
<td><p>誰主香江(已完結)<br />
本土最前線</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳志宏_(香港).md" title="wikilink">陳志宏博士</a></p></td>
<td><p>MyUniversity<br />
四眼乜乜乜 (暫代主持/已完結)<br />
科學新知</p></td>
<td><p><a href="../Page/香港大學.md" title="wikilink">香港大學理學院講師</a>、<a href="../Page/紐約大學.md" title="wikilink">紐約大學博士</a>，主修物理學，同時為澳門政團<a href="../Page/新澳門學社.md" title="wikilink">新澳門學社成員</a>、<a href="../Page/神州青年服務社.md" title="wikilink">神州青年服務社社長</a>。</p></td>
</tr>
<tr class="even">
<td><p>Dennis</p></td>
<td><p>玩樂高地<br />
</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/錢偉洛.md" title="wikilink">錢偉洛</a>（鹵味男、Lak Lak）</p></td>
<td><p>馬場USB<br />
毓民射馬</p></td>
<td><p><a href="../Page/普羅政治學苑.md" title="wikilink">普羅政治學苑成員</a>，前<a href="../Page/社會民主連線.md" title="wikilink">社會民主連線</a>、<a href="../Page/人民力量.md" title="wikilink">人民力量成員</a>，曾任黃毓民議員助理。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/尤達_(馬評人).md" title="wikilink">尤達</a></p></td>
<td><p>馬場USB<br />
毓民射馬</p></td>
<td><p>馬評人</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張錦滿.md" title="wikilink">張錦滿</a></p></td>
<td><p>人文講場</p></td>
<td><p>作家、文化人，在出版行業服務接近二十年，曾擔任多份雜誌周刊的編輯。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張鶩.md" title="wikilink">張　鶩</a>（溝兒、Koey）</p></td>
<td><p>三個女人蜜蜜針</p></td>
<td><p>法式用品店創辦人、外籍英語中心課程總監、酒店營運總監，《<a href="../Page/癲狗日報.md" title="wikilink">癲狗日報</a>》專欄作家</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/棗兒.md" title="wikilink">棗　兒</a>（Joey）</p></td>
<td><p>三個女人蜜蜜針</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Katie.md" title="wikilink">Katie</a>（K）</p></td>
<td><p>三個女人蜜蜜針</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/葭葭.md" title="wikilink">葭葭</a></p></td>
<td><p>三個女人蜜蜜針</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/姚冠東.md" title="wikilink">姚冠東</a></p></td>
<td><p>梁錦祥一週時事（嘉賓主持）<br />
天地友政氣</p></td>
<td><p>廣告創作總監。前<a href="../Page/低俗頻道.md" title="wikilink">低俗頻道台長</a>、前<a href="../Page/香港人網.md" title="wikilink">香港人網</a>、<a href="../Page/謎米香港.md" title="wikilink">謎米香港主持</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳嘉輝.md" title="wikilink">陳嘉輝</a></p></td>
<td><p>天地友政氣</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Allen</p></td>
<td><p>予豈好辯哉之政論（已完結）<br />
本土最前線</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Ken</p></td>
<td><p>予豈好辯哉之政論（已完結）<br />
</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/姜廣華.md" title="wikilink">姜廣華</a>（姜哥）</p></td>
<td><p>夠姜至啱聽<br />
</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張珈衍.md" title="wikilink">張珈衍</a>（法國佬）</p></td>
<td><p>本土最前線<br />
梁錦祥一週時事（嘉賓主持）</p></td>
<td><p>駕駛教練。前<a href="../Page/熱血公民.md" title="wikilink">熱血公民成員</a>、前<a href="../Page/熱血時報.md" title="wikilink">熱血時報主持</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/楊逸朗.md" title="wikilink">楊逸朗</a>（Joe Yeung）</p></td>
<td><p>本土最前線</p></td>
<td><p>社運人士。曾任香港輔助警察。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/祖利安.md" title="wikilink">祖利安</a></p></td>
<td><p>本土最前線</p></td>
<td><p>占卜師、藏傳佛教修行者。《癲狗日報》專欄作家。前城邦派「護國大法師」。</p></td>
</tr>
</tbody>
</table>

### 已離任

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>曾主持節目</p></th>
<th><p>出生日期及生日/主持人簡介</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/陳偉業.md" title="wikilink">陳偉業</a>（大嚿）</p></td>
<td><p>毓民踩場 (已離任)<br />
人民力量 (已完結)<br />
MyUniversity</p></td>
<td><p><a href="../Page/人民力量.md" title="wikilink">人民力量成員</a>。前<a href="../Page/社會民主連線.md" title="wikilink">社會民主連線成員</a>、前<a href="../Page/立法會議員.md" title="wikilink">立法會議員</a>、前荃灣區議會（麗興選區）議員。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/湯詠芝.md" title="wikilink">湯詠芝</a>（Carol／嘉露）</p></td>
<td><p>毓民踩場 (曾客席)</p></td>
<td><p>曾任陳偉業議員助理</p></td>
</tr>
<tr class="odd">
<td><p>Micheal Lu（小米）</p></td>
<td><p>智Goal無敵 (已完結)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/車車.md" title="wikilink">車車</a></p></td>
<td><p>智Goal無敵 (已完結)</p></td>
<td><p>電視女演員與節目主持</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/任亮憲.md" title="wikilink">任亮憲</a>（馬草泥／維園阿哥）</p></td>
<td><p>毓民踩場 (已離任)<br />
城市再論壇 (已完結)</p></td>
<td><p>傲明集團聯席董事</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/麥業成.md" title="wikilink">麥業成</a></p></td>
<td><p>新界最前線 (已完結)<br />
辛亥百年話滄桑 (已完結)</p></td>
<td><p>現任<a href="../Page/元朗區議會.md" title="wikilink">元朗區議會議員</a>，政團<a href="../Page/民主陣線.md" title="wikilink">民主陣線</a>（前稱元朗天水圍民主陣線）主席。前社會民主連線行政委員、前人民力量副主席及成員。<a href="../Page/中國國民黨.md" title="wikilink">中國國民黨黨員</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/甄燊港.md" title="wikilink">甄燊港</a></p></td>
<td><p>新界最前線 (已完結)</p></td>
<td><p>人民力量副主席、<a href="../Page/前綫_(政黨).md" title="wikilink">前線召集人</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳志全.md" title="wikilink">陳志全</a>（慢必）</p></td>
<td><p>新界最前線 (已完結)</p></td>
<td><p>香港新界東立法會議員(2012-)，人民力量執行委員會委員。前<a href="../Page/香港人網.md" title="wikilink">香港人網行政總裁</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/歐陽英傑.md" title="wikilink">歐陽英傑</a>（星屑醫生）</p></td>
<td><p>城市再論壇 (已完結)</p></td>
<td><p>私人執業醫生、人民力量委員</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/劉嘉鴻.md" title="wikilink">劉嘉鴻</a>（劉嗡）</p></td>
<td><p>城市再論壇 (已完結)</p></td>
<td><p>人民力量前主席、<a href="../Page/選民力量.md" title="wikilink">選民力量司庫</a></p></td>
</tr>
<tr class="odd">
<td><p>嚴達明</p></td>
<td><p>城市再論壇 (已完結)</p></td>
<td><p>人民力量委員、牙科醫生</p></td>
</tr>
<tr class="even">
<td><p>周峻翹</p></td>
<td><p>立睇九龍 (已完結)</p></td>
<td><p>人民力量委員、黃毓民議員助理</p></td>
</tr>
<tr class="odd">
<td><p>黎樂民</p></td>
<td><p>今晚返埋黎 (已完結)</p></td>
<td><p>文化推廣公司董事</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/袁文傑.md" title="wikilink">袁文傑</a></p></td>
<td><p>足你好運 (已完結)<br />
智Goal無敵 (已完結)<br />
天下足球 (已完結)<br />
兵工廠世界 (已完結)<br />
熱血大球場 (已完結)</p></td>
<td><p><br />
香港著名影視男演員、節目主持及模特兒。現為香港<a href="../Page/電視廣播有限公司.md" title="wikilink">無綫電視男藝員</a>。<a href="../Page/無綫電視藝員訓練班.md" title="wikilink">無綫電視藝員訓練班第四期藝員進修班畢業藝員</a>、第二屆寶麗金卡拉OK模特兒大賽冠軍、2015年全球粵語主持人大賽冠軍並獲得最有台型大獎。曾為<a href="../Page/亞洲電視.md" title="wikilink">亞洲電視</a>、<a href="../Page/Now寬頻電視.md" title="wikilink">Now寬頻電視及</a><a href="../Page/香港有線電視.md" title="wikilink">香港有線電視男藝員</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/何志光_(香港).md" title="wikilink">何志光</a>（CK）</p></td>
<td><p>建國弟兄會 (已完結)</p></td>
<td><p><a href="../Page/香港本土力量.md" title="wikilink">香港本土力量主席</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/安德烈_(香港).md" title="wikilink">安德烈</a>（蒸魚安）</p></td>
<td><p>建國弟兄會 (已完結)</p></td>
<td><p>作家、文化研究者</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄺頌晴.md" title="wikilink">鄺頌晴</a>（阿Bi）</p></td>
<td><p>建國研習班 (曾有宣傳但未曾主持節目)</p></td>
<td><p><a href="../Page/鍵盤戰線.md" title="wikilink">鍵盤戰線發言人</a>。前熱血公民成員。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李偉儀.md" title="wikilink">李偉儀</a></p></td>
<td><p>天天天藍（已離任）</p></td>
<td><p>中文大學人類學學士和哲學碩士、浸會大學青年輔導學社會科學碩士、中文大學性別研究課程博士候選人；具美國醫學及牙科催眠學會專業牌照，美國執業性治療師。早年曾任報章專欄作家。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃台仰.md" title="wikilink">黃台仰</a></p></td>
<td><p>素人起義 (已完結)<br />
吾國吾聞 (已完結)</p></td>
<td><p><a href="../Page/本土民主前線.md" title="wikilink">本土民主前線召集人</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梁天琦.md" title="wikilink">梁天琦</a></p></td>
<td><p>吾國吾聞 (已完結)</p></td>
<td><p><a href="../Page/本土民主前線.md" title="wikilink">本土民主前線發言人</a>，曾參與2016年新界東補選。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曾焯文.md" title="wikilink">曾焯文</a></p></td>
<td><p>本土粵文 (已腰斬)<br />
粵字匯唐文 (已腰斬)</p></td>
<td><p><a href="../Page/本土新聞.md" title="wikilink">本土新聞翻譯主管</a></p></td>
</tr>
<tr class="even">
<td><p>周韋樂（阿樂、周圍轆）</p></td>
<td><p>學生主場 (已完結)</p></td>
<td><p>嶺南大學學生會退出學聯關注組發起人</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/盧斯達.md" title="wikilink">盧斯達</a>（無待堂）</p></td>
<td><p>勞思動眾 (已腰斬)</p></td>
<td><p>作家、時事評論員</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/仇思達.md" title="wikilink">仇思達</a>（無敵神駒）</p></td>
<td><p>勞思動眾 (曾客席、已腰斬)</p></td>
<td><p>作家，曾任<a href="../Page/香港城市大學.md" title="wikilink">香港城市大學校董</a>，現為個人網絡電台<a href="../Page/本土台.md" title="wikilink">本土台台長</a>、時事評論員，並著有《屹屹屹》一書</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/北條彰.md" title="wikilink">北條彰</a></p></td>
<td><p>瀛能無雙（已改在熱血時報播放）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沈四海.md" title="wikilink">沈四海</a></p></td>
<td><p>天天天藍（已離任）</p></td>
<td><p>資深傳媒人，<a href="../Page/中國國民黨.md" title="wikilink">中國國民黨黨員</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭松泰.md" title="wikilink">鄭松泰</a>（泰博）</p></td>
<td><p>建國研習班（已改在熱血時報播放）</p></td>
<td><p><a href="../Page/香港理工大學.md" title="wikilink">香港理工大學應用社會科學系講師</a>、立法會議員(2016-)、熱血公民主席(2016-)、熱血時報節目主持</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬愉生.md" title="wikilink">馬愉生</a></p></td>
<td><p>毓民踩場（已離任）<br />
立睇九龍 (已完結)</p></td>
<td><p>普羅政治學苑副主席。前社會民主連線副秘書長、社區服務主任，曾參選<a href="../Page/2016年香港立法會選舉.md" title="wikilink">2016年立法會九龍西選區選舉</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳云根.md" title="wikilink">陳-{云}-根</a>（陳雲、國師）</p></td>
<td><p>城邦論壇 (已腰斬)<br />
華夏建國論壇 (已腰斬)</p></td>
<td><p><a href="../Page/香港復興會.md" title="wikilink">香港復興會創會主席</a>。前<a href="../Page/嶺南大學.md" title="wikilink">嶺南大學中文系助理教授</a>，<a href="../Page/香港自治運動.md" title="wikilink">香港自治運動倡議者及前顧問</a>。著有《<a href="../Page/香港城邦論.md" title="wikilink">香港城邦論</a>》。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/莊元生.md" title="wikilink">莊元生</a></p></td>
<td><p>華夏建國論壇（已腰斬）</p></td>
<td><p>作家、出版社編輯。曾任中學中文教師。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蔡淑梅.md" title="wikilink">蔡淑梅</a></p></td>
<td><p>仁心·人生 (已完結)</p></td>
<td><p>醫生</p></td>
</tr>
<tr class="even">
<td><p>Janice</p></td>
<td><p>仁心·人生 (已完結)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭錦滿.md" title="wikilink">鄭錦滿</a>（四眼哥哥）</p></td>
<td><p>四眼乜乜乜（已完結）<br />
素人起義（已完結）</p></td>
<td><p>熱血公民副主席，曾參與2016年立法會港島區選舉。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/靳民知.md" title="wikilink">靳民知</a>（阿靳）</p></td>
<td><p>毓民踩場 (已離任)<br />
建國研習班 (已完結)<br />
熱血大球場 (已完結)</p></td>
<td><p>鄭松泰議員助理。曾任黃毓民議員助理、前<a href="../Page/香港人網.md" title="wikilink">香港人網節目主持</a>。前<a href="../Page/選民力量.md" title="wikilink">選民力量成員</a>。<a href="../Page/巴塞隆拿.md" title="wikilink">巴塞隆拿香港官方球迷會幹事</a>。</p></td>
</tr>
<tr class="odd">
<td><p>Jerry</p></td>
<td><p>崑崙風 (已離任)<br />
梁錦祥一週時事 (已離任)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吳家汶.md" title="wikilink">吳家汶</a>（Gaman）</p></td>
<td><p>香港風情（已完結）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/方智龍.md" title="wikilink">方智龍</a>（Addie）</p></td>
<td><p>網上辣辛聞（已完結）</p></td>
<td><p>調解員。前<a href="../Page/熱血公民.md" title="wikilink">熱血公民成員</a>、前<a href="../Page/熱血時報.md" title="wikilink">熱血時報主持</a>，曾參與<a href="../Page/2015年香港區議會選舉.md" title="wikilink">2015年區議會元州及蘇屋區選舉</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李政熙.md" title="wikilink">李政熙</a></p></td>
<td><p>網上辣辛聞（嘉賓主持）（已完結）</p></td>
<td><p>前<a href="../Page/熱血公民.md" title="wikilink">熱血公民成員</a>，曾參與<a href="../Page/2015年香港區議會選舉.md" title="wikilink">2015年區議會天平西區選舉</a>。</p></td>
</tr>
<tr class="odd">
<td><p>Vega</p></td>
<td><p>網上辣辛聞（嘉賓主持）（已完結）</p></td>
<td><p>前<a href="../Page/熱血公民.md" title="wikilink">熱血公民成員</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雯雯.md" title="wikilink">雯雯</a></p></td>
<td><p>三個女人蜜蜜針（已離任）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曹撕達.md" title="wikilink">曹撕達</a>（Marco Lau、救生員／Lifeguard）</p></td>
<td><p>風火水電（已完結）</p></td>
<td><p>MyRadio技術總監，前香港人網工作人員。《癲狗日報》專欄作家。</p></td>
</tr>
<tr class="even">
<td><p>布萊恩</p></td>
<td><p>風火水電（已完結）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳樹鵬.md" title="wikilink">陳樹鵬</a>（阿鵬）</p></td>
<td><p>今晚開你波（已完結）<br />
贏爆洲際盃（已完結）</p></td>
<td><p>足球評述員、<a href="../Page/香港有線電視球彩台.md" title="wikilink">香港有線電視球彩台主持</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/盧振聲.md" title="wikilink">盧振聲</a>（阿聲）</p></td>
<td><p>今晚開你波（已完結）<br />
贏爆洲際盃（已完結）</p></td>
<td><p>足球評述員、<a href="../Page/香港有線電視球彩台.md" title="wikilink">香港有線電視球彩台主持</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/謝蕙妍.md" title="wikilink">謝蕙妍</a>（Wendy）</p></td>
<td><p>今晚開你波（已完結）<br />
贏爆洲際盃（已完結）</p></td>
<td><p>足球評述員、<a href="../Page/香港有線電視球彩台.md" title="wikilink">香港有線電視球彩台主持</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/盤神.md" title="wikilink">盤　神</a></p></td>
<td><p>今晚開你波（已完結）<br />
贏爆洲際盃（已完結）</p></td>
<td><p><a href="../Page/香港有線電視球彩台.md" title="wikilink">香港有線電視球彩台主持</a></p></td>
</tr>
<tr class="odd">
<td><p>安　娜（Anna）</p></td>
<td><p>三個女人蜜蜜針（已完結）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭兆明.md" title="wikilink">郭兆明</a>（郭博士）</p></td>
<td><p>那些年（已完結）</p></td>
<td><p>商人、佛學博士，<a href="../Page/香港顯密學會.md" title="wikilink">香港顯密學會會長</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張展豪.md" title="wikilink">張展豪</a>（文豪）</p></td>
<td><p>坐看雲起時（已完結）</p></td>
<td><p>攝影師</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭嘉熹.md" title="wikilink">郭嘉熹</a>（阿雲）</p></td>
<td><p>天地友政氣(已離任)</p></td>
<td><p>舞台劇演員</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陸威廉.md" title="wikilink">陸威廉</a>（William）</p></td>
<td><p>文圖萬里（第二季）（已完結）<br />
梁錦祥一週時事（已離任）</p></td>
<td><p>旅遊達人、<a href="../Page/暨南大學.md" title="wikilink">暨南大學國際政治學學士</a>、<a href="../Page/馬德里體育會.md" title="wikilink">馬德里體育會香港球迷會召集人</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

備註：分拆前的節目主持請參看[香港人網](../Page/香港人網.md "wikilink")。另外聯播節目《大香港早晨》的主持請參看[熱血時報](../Page/熱血時報.md "wikilink")。

## 工作人員

| 姓名                                   | 職位   | 簡介                                             |
| ------------------------------------ | ---- | ---------------------------------------------- |
| Marco Lau（救生員／Lifeguard）             | 技術總監 | 前香港人網工作人員。                                     |
| 陳金                                   | 秘書   |                                                |
| [傅偉聰](../Page/傅偉聰.md "wikilink")（溫鞭） | 法律顧問 | 大律師、[香港法學研究會會長](../Page/香港法學研究會.md "wikilink") |

## 參考資料

## 外部連結

  - [MyRadio 官方網站](http://myradio.hk)

  -
  - [MyRadio MP3下載](http://archive.org/details/server.00.myradio.hk)

  -
  - 官方 Facebook 群組：「MyRadio.HK 群組」

\]

[Category:香港網絡電台](../Category/香港網絡電台.md "wikilink")
[Category:2013年成立的公司](../Category/2013年成立的公司.md "wikilink")
[Category:香港本土派組織](../Category/香港本土派組織.md "wikilink")
[Category:被防火长城封锁的网站](../Category/被防火长城封锁的网站.md "wikilink")

1.
2.
3.
4.
5.  [毓民開台繼續噏，2007年1月31日《蘋果日報》](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20070131&sec_id=4104&subsec_id=15333&art_id=6769049&cat_id=45&coln_id=20)
6.  [2007年3月《E-Zone》P.1](http://i30.tinypic.com/2qs6rdj.jpg)
7.  [2007年3月《E-Zone》P.2](http://i28.tinypic.com/2zzjif5.jpg)
8.  [2007年3月《E-Zone》P.3](http://i28.tinypic.com/2zf722d.jpg)
9.  [香港電台《議事論事》(節錄) -
    訪問香港人網創辦人蕭若元](http://www.youtube.com/watch?v=tfs8n7MiyQo)
10. [香港人網收看直播節目公告](http://www.hkreporter.com/talks/viewthread.php?tid=171521)
11. [黃毓民:
    {{〈}}網上烽煙也是煙{{〉}}，2008年3月10日《明報》D04](http://i31.tinypic.com/2d55xy.jpg)
12. [2008年4月24日Alexa.com網站訪問量統計](http://i32.tinypic.com/9vcjsy.jpg)
13. [《PCM電腦廣場》773期，L09 Tech for
    Living](http://i29.tinypic.com/2rx7c4w.jpg)
14. [開咪辣火頭蕭定一袁彌明:
    唔驚得咁多\! 2008年7月9日《FACE週刊》娛樂真面目F058](http://i36.tinypic.com/v3fu38.jpg)
15. [開咪辣火頭蕭定一袁彌明:
    唔驚得咁多\! 2008年7月9日《FACE週刊》娛樂真面目F059](http://i38.tinypic.com/zuk26t.jpg)
16. [袁彌明炮轟葉翠翠兩宗罪，2008年6月27日《星島日報》娛樂版C1](http://hk.news.yahoo.com/080626/60/2wev0.html)

17. [新紀完周刊: 九七前後港新聞自由](http://mag.epochtimes.com/026/3254.htm)
18. [梁繼璋網上開咪再會李麗蕊，2008年7月25日《蘋果日報》娛樂名人C36](http://www.hkreporter.com/talks/viewthread.php?tid=229320)
19. [香港人網減少節目的公告](http://www.hkreporter.com/talks/thread-264243-1-1.html)
20.
21.