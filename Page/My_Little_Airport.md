**My Little
Airport**為[香港的獨立音樂組合](../Page/香港.md "wikilink")，由阿P（[林鵬](../Page/林鵬_\(樂隊成員\).md "wikilink")）和主音Nicole（[區健瑩](../Page/區健瑩.md "wikilink")）所組成的二人組合。My
Little
Airport作品常配以[英語及](../Page/英語.md "wikilink")[法語大膽歌詞](../Page/法語.md "wikilink")，\[1\]包括《Gigi
Leung is dead》、《失落沮喪歌》及《讓我搭一班會爆炸的飛機》等。

## 背景

2003年，阿P（主曲／詞／編）和Nicole（主唱）是[樹仁學院](../Page/樹仁學院.md "wikilink")[新聞系同班同學](../Page/新聞.md "wikilink")，\[2\]因為上課太無聊，下課後便經常到同學Josephine
所開的[精品店](../Page/精品店.md "wikilink")（《[在動物園散步才是正經事](../Page/在動物園散步才是正經事.md "wikilink")》專輯內的[Josephine's
shop](../Page/Josephine's_shop.md "wikilink")）流連。當時店內放有一支木[結他](../Page/結他.md "wikilink")，阿P拿來創作了一些以女同學名字為歌名的歌，並引誘同學們加入唱作，樂隊初時有三位主音兩位結他手，但最後只剩下阿P和Nicole二人一作一唱，並在睡房錄製作了50張限量CDR
demo《singing in the
phone》，以$18強迫同學購買，一星期內售罄。\[3\]其作曲風格自由，除一般的歌曲外也有類似詩歌朗誦配以音樂的形態。\[4\]早期作品多以兩位主唱身邊的人和事為題材，但逐漸加入政治、社會等題材。尤其是有歌曲，例如《Donald
Tsang, Please
die》（意指請[曾蔭權自行了斷](../Page/曾蔭權.md "wikilink")）批評[香港](../Page/香港.md "wikilink")[特首](../Page/特首.md "wikilink")，政府官員等。也有描述性愛行為、開房等題材；封面經常使用素人肖像，但一般不是主唱本人。\[5\]

### 音樂發展\[6\]

2004年，My Little
Airport加入「維港唱片」，正式推出首張專輯《[在動物園散步才是正經事](../Page/在動物園散步才是正經事.md "wikilink")》，碟名的「動物園」是指[旺角](../Page/旺角.md "wikilink")[信和中心地庫的另類唱片店](../Page/信和中心.md "wikilink")「Zoo
Records」，歌曲繼續貫徹以人名為題，內容圍繞阿P成長中散失的朋友，音樂以廉價[Casio小型電子琴聲](../Page/卡西歐.md "wikilink")（pt-30、sa-5）為主。2005年推出第二張專輯《[只因當時太緊張](../Page/只因當時太緊張.md "wikilink")》，歌曲關於暗戀，音樂開始加入結他噪音。兩年後推出的第三張專輯《[我們在炎熱與抑鬱的夏天，無法停止抽煙](../Page/我們在炎熱與抑鬱的夏天，無法停止抽煙.md "wikilink")》，歌曲關於畢業、上班、自殺等，以較陰暗的歌詞配搭甜美音樂見稱，同年加盟[西班牙廠牌](../Page/西班牙.md "wikilink")「Elefant
Records」，推出一張專輯「」，裡面大多數歌曲來自舊專輯。

2008年，My Little
Airport舉行一連三場「[浪漫九龍塘音樂會](../Page/浪漫九龍塘音樂會.md "wikilink")」，加入更多樂隊成員，翌年11月20日再推出專輯《[介乎旺角與法國的詩意](../Page/介乎旺角與法國的詩意.md "wikilink")》，該專輯內容涉及香港民主發展、上班族壓力、日常生活以及[性愛等題材](../Page/性愛.md "wikilink")。2011年8月26日他們發行專輯《[香港是個大商場](../Page/香港是個大商場.md "wikilink")》，唱片名字源自系列紀錄片《時代精神》（Zeitgeist），內容是探討世界金融經濟體系為全球帶來的負面影響；2012年10月20日又發行《[寂寞星期五](../Page/寂寞星期五.md "wikilink")》，收錄關於遠離人群、無業、放售唱片、等待暴動、偷情、靈魂轉換、無賴、嫖客、菊花的味道等十七首歌曲，主題圍繞人生各種荒謬事情。

2014年10月1日，My Little
Airport推出專輯《[適婚的年齡](../Page/適婚的年齡.md "wikilink")》，\[7\]
描寫社會給年輕人的壓力與暴力：包括欠缺自我空間、政治人物與財閥對公共與私人空間的搶奪、因成長感朋友流失從而續漸孤獨、獨身的壓力、暗戀單戀卻不能戀愛、社會給予男生於婚姻上的壓力、\[8\]一夜情觀感、時代的無能、別國的旅後感、對香港的失望從而引發的過客感，探討香港令年輕人絶望的因由。

## 影響

My Little
Airport的唱片至今已先後被[西班牙](../Page/西班牙.md "wikilink")、[中國大陸](../Page/中國大陸.md "wikilink")、[臺灣](../Page/臺灣.md "wikilink")、[韓國廠牌發行成當地版](../Page/韓國.md "wikilink")，並被Allmusic.com選為2007年全球「The
Twee-est Band」。\[9\]而1980至90年代[蘇格蘭Indie](../Page/蘇格蘭.md "wikilink")
pop樂隊「[BMX Bandits](../Page/BMX_Bandits.md "wikilink")」曾翻唱了My Little
Airport的《You don't wanna be my girlfriend,
Phoebe》。\[10\]\[11\]作品曾獲[塞爾維亞遊戲公司](../Page/塞爾維亞.md "wikilink")「Dreamlore」使用於旗下製作的[電腦遊戲](../Page/電腦遊戲.md "wikilink")《[Stalin
vs.
Martians](../Page/Stalin_vs._Martians.md "wikilink")》中。\[12\]此外，他們亦曾多次出版不少支持香港民主的歌曲，\[13\]有如《Donald
tsang, Please
die》及《瓜分[林瑞麟三十萬薪金](../Page/林瑞麟.md "wikilink")》。\[14\]林瑞麟到台灣訪問時，中天在介紹林瑞麟時放的背景音樂就是《瓜分[林瑞麟三十萬薪金](../Page/林瑞麟.md "wikilink")》；\[15\]更因涉及敏感政治話題而常被禁止在內地演出。\[16\]\[17\]

## 音樂作品

### 參與歌曲

  - 2004年：《**Come Out And Play**》
      - Tim, Do You Really Wanna Make A Film?

<!-- end list -->

  - 2004年：《**In the name of [AMK](../Page/AMK.md "wikilink")**》
      - 山頂．公仔．波板糖

<!-- end list -->

  - 2005年：《**草地音樂合輯**》
      - 在動物園散步才是正經事
      - Victor，Fly Me To Stafford

<!-- end list -->

  - 2005年：《**【FC5-118】HitoRadio 開站紀念專輯**》
      - I Don't Know How To Download Good AV Like Iris Does

<!-- end list -->

  - 2005年：《**\#is3**》
      - Victor，Fly Me To Stafford

### 單曲

  - 2005年：《**When I listen to the field mice**》 僅100張限量CDR
      - When i listen to the field mice
      - When i listen to the field mice (Catalog mix)

<!-- end list -->

  - 2005年：《**Gigi Leung is dead**》
      - Gigi Leung is dead
      - Washide
      - 你的微笑像朵花 (Guitar version)
      - Dee, it may all end tomorrow (Guitar version)

<!-- end list -->

  - 2008年：《和陳五MSN》
  - 2009年：《瓜分[林瑞麟三十萬的薪金](../Page/林瑞麟.md "wikilink")》
      - 《Donald Tsang, Please die》
      - 《給Face雜誌的記者Ivy》
      - 《我愛郊野但不愛派對》
  - 2014年：
      - 《今晚到干诺道中一起瞓》
  - 2015年：
      - 《獨身的理由》
  - 2016年：
      - 《今天沒大麻在身》
      - 《攻陷你的西》
  - 2017年：
      - 《彌敦道的一晚 good trip》（僅釋出MV版）

### 專輯

  - 2004年8月1日：《**[在動物園散步才是正經事](../Page/在動物園散步才是正經事.md "wikilink")**》
  - 2005年10月1日：《**[只因當時太緊張](../Page/只因當時太緊張.md "wikilink")**》
  - 2007年10月1日：《**[我們在炎熱與抑鬱的夏天，無法停止抽煙](../Page/我們在炎熱與抑鬱的夏天，無法停止抽煙.md "wikilink")**》
  - 2009年11月20日：《**[介乎法國與旺角的詩意](../Page/介乎法國與旺角的詩意.md "wikilink")**》\[18\]
  - 2011年8月26日：《**[香港是個大商場](../Page/香港是個大商場.md "wikilink")**》
  - 2012年10月20日：《**[寂寞的星期五](../Page/寂寞的星期五.md "wikilink")**》
  - 2014年10月1日：《**[適婚的年齡](../Page/適婚的年齡.md "wikilink")**》
  - 2016年9月15日：《**[火炭麗琪](../Page/火炭麗琪.md "wikilink")**》\[19\]
  - 2018年9月30日：《**[你說之後會找我](../Page/你說之後會找我.md "wikilink")**》

## 演唱會

  - 2008年5月30日及5月31日：《浪漫九龍塘音樂會》
  - 2009年11月20日及11月22日：《失業救濟音樂會》
  - 2011年8月26日及8月27日：《香港是個大商場音樂會》
  - 2012年10月20日及10月21日：《菊花的味道音樂會》
  - 2013年10月31日及11月1日：《金石為開音樂會》
  - 2014年10月3日及10月4日：《攻陷你的心十週年音樂會》
  - 2015年10月10日至10月13日：《今晚不如唔好走音樂會》
  - 2016年9月12日至9月14日：《今年我想同你去音樂會》
  - 2017年12月23日至12月27日：《聖誕留一晚陪我音樂會》
  - 2018年9月30日至10月6日：《你說之後會找我音樂會》\[20\]

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  -
  -
  -
  - [My Little Airport 官方網頁](http://www.mylittleairport.com/)

  - [My Little Airport 豆瓣小站](http://site.douban.com/mylittleairport/)

My Little Airport成員社交網站：

  -
  -
[Category:2004年成立的音樂團體](../Category/2004年成立的音樂團體.md "wikilink")
[Category:香港演唱團體](../Category/香港演唱團體.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14. [globalvoicesonline.org/2009/05/18/hong-kong-donald-tsang-please-die](https://globalvoices.org/2009/05/18/hong-kong-donald-tsang-please-die/)
15.
16.
17.
18.
19.
20.