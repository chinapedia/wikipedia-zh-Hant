[IPod_Myriad.svg](https://zh.wikipedia.org/wiki/File:IPod_Myriad.svg "fig:IPod_Myriad.svg")的Myriad体被[苹果电脑公司作为市场宣传](../Page/苹果电脑公司.md "wikilink")\]\]
**Myriad**是一种西文[无衬线字体](../Page/无衬线字体.md "wikilink")，属于[人文主义体](../Page/无衬线体#分類.md "wikilink")，是由罗伯特·斯林巴赫（Robert
Slimbach，1956年－）和卡罗·图温布利（Carol
Twombly，1959年－）在1990年到1992年左右以[Frutiger字体为藍本為](../Page/Frutiger.md "wikilink")[Adobe公司设计的](../Page/Adobe.md "wikilink")。

## 特征

Myriad拥有一个多控制界面可以与一个智能[软件](../Page/软件.md "wikilink")“引擎”共同工作，可以让用户通过接入一个控制界面按照各个[字形的笔画粗细范围生成各种不同宽度](../Page/字形.md "wikilink")、字重的变体版本
。

人文主义[无衬线字体是一个有机的结构](../Page/无衬线字体.md "wikilink")，吸收了旧式的[衬线](../Page/衬线.md "wikilink")[字体的元素](../Page/字体.md "wikilink")。和罗曼时代文书一样，大写字母有一个水平轴线，而小写字母基于格里高利文书的模型制作。人文主义无衬线字体动态的形状和单色调颜色，通过不同的字母宽度调解平衡，给人温暖友好的感觉。可读性和友好的界面使得Myriad在印刷和显示两方面都表现出良好的适应性。

## 应用

自2002年发布[eMac开始](../Page/eMac.md "wikilink")，Myriad取代Apple
Garamond（[Garamond的一种变体](../Page/Garamond.md "wikilink")）成为[苹果电脑公司新的企业指定字体](../Page/苹果电脑公司.md "wikilink")。此后，其所有的市场宣传和产品都是使用Myriad字体。从[iPod
photo开始](../Page/iPod_photo.md "wikilink")，其界面字体Podium
Sans，与Myriad有极大相似性。不过，自2016年9月起，[苹果公司用自己设计的](../Page/苹果公司.md "wikilink")[San
Francisco替代了Myriad](../Page/San_Francisco_\(2014年的字体\).md "wikilink")，作为产品和宣传的新字体。

用户可以通过[Adobe Acrobat](../Page/Adobe_Acrobat.md "wikilink") Standard
7.0光碟中的data1.cab提出这个字体。

另外，[香港](../Page/香港.md "wikilink")[地鐵](../Page/香港地鐵.md "wikilink")（現為[港鐵一部分](../Page/港鐵.md "wikilink")）在1998年以後建築的[車站](../Page/車站.md "wikilink")，以及[荔景](../Page/荔景站.md "wikilink")、[油麻地](../Page/油麻地站.md "wikilink")、[九龍灣](../Page/九龍灣站.md "wikilink")、[牛頭角及](../Page/牛頭角站.md "wikilink")[觀塘站的站名](../Page/觀塘站.md "wikilink")，以及沿途的大部份指示牌及標語，英文均是使用Myriad字形，其餘1997年前啟用的車站纖維板上及浮雕的文字則是使用[Helvetica體](../Page/Helvetica.md "wikilink")。

而[九廣鐵路在](../Page/九廣鐵路.md "wikilink")1996年把Myriad字體加工成為[Casey字體](../Page/Casey.md "wikilink")，用於其[企業形象上](../Page/企業形象.md "wikilink")。

## 参照

  - Blackwell, Lewis. *20th Century Type.* Yale University Press: 2004.
    ISBN 0-300-10073-6.
  - Fiedl, Frederich, Nicholas Ott and Bernard Stein. *Typography: An
    Encyclopedic Survey of Type Design and Techniques Through History.*
    Black Dog & Leventhal: 1998. ISBN 1-57912-023-7.
  - Macmillan, Neil. *An A–Z pf Type Designers.* Yale University Press:
    2006. ISBN 0-300-11151-7.

## 外部链接

  - [Typowiki: Myriad](http://typophile.com/wiki/myriad)
  - [Adobe Systems information on Myriad multiple
    masters](https://web.archive.org/web/20140802113913/http://www.adobe.com/support/techdocs/328600.html)
  - [Frutiger字体和Myriad字体的对比](http://typophile.com/wiki/Myriad)
  - [Linotype.com中展示的Myriad字体家族](http://www.linotype.com/1257/myriad-family.html)

[Category:蘋果公司字體](../Category/蘋果公司字體.md "wikilink")
[Category:无衬线字体](../Category/无衬线字体.md "wikilink")
[Category:Adobe字体](../Category/Adobe字体.md "wikilink")