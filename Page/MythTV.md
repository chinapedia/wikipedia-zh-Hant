**MythTV**是用來把[Linux電腦變成](../Page/Linux.md "wikilink")[數位錄影機](../Page/數位錄影機.md "wikilink")、[HTPC的軟體](../Page/HTPC.md "wikilink")，而且採用了[GNU通用公共許可證](../Page/GNU通用公共許可證.md "wikilink")。

## 歷史

**MythTV**計劃由[Isaac
Richards在](../Page/Isaac_Richards.md "wikilink")2002年4月開始，Richards解釋了他開始計劃的原因：

MythTV的開發版本是放在可以讓公眾存取的[SVN](../Page/Subversion.md "wikilink")[伺服器上](../Page/伺服器.md "wikilink")，一些活躍的[IRC頻道和電郵列表讓開發者和用戶可以溝通](../Page/IRC.md "wikilink")。

## 功能

MythTV的功能有：

  - 後端伺服器和前端客戶端的架構，容許多個前端客戶端遙距觀看一個或多個後端伺服器的內容。
  - 獨立的電腦可以同時是前端客戶端和後端伺服器。
  - 後端伺服器同時有多張電視調解卡。
  - 可選擇性的分析內容以剔除廣告。
  - 加快或減慢播放內容的速度，同可選擇調校音調。
  - 有智能預錄功能以避免錄影的時間重疊。
  - 提供電視節目表的資料介面，在美加以外地區透過[XMLTV提供節目資料](../Page/XMLTV.md "wikilink")。
  - 暫停、skip和重播即時電視節目。
  - 透過網頁介面預設和執行系統功能。
  - 支援[ATSC](../Page/ATSC.md "wikilink")、[DMB-TH](../Page/DMB-TH.md "wikilink")、[ISDB-T和](../Page/ISDB-T.md "wikilink")[DVB](../Page/DVB.md "wikilink")
    [高清電視制式](../Page/高清電視.md "wikilink")（即是
    [LinuxTV](../Page/LinuxTV.md "wikilink") 支援的所有制式）。
  - 透過紅外線遙控（[Irblaster](../Page/Irblaster.md "wikilink")）控制解碼器／機上盒。

## 硬體支援

MythTV軟體支援完全相容於[Video4Linux或Video](../Page/Video4Linux.md "wikilink")4Linux2
[核心驅動程式的電視調解卡](../Page/Linux_kernel.md "wikilink")，用來自[IVTV計劃的驅動程式](../Page/IVTV.md "wikilink")，MythTV支援[Hauppauge的WinTV](../Page/Hauppauge_Computer_Works.md "wikilink")-PVR
250/350 PCI卡或類似內置了iTVC15/16
硬體[MPEG-2壓縮處理器的電視調解卡](../Page/MPEG-2.md "wikilink")。有Video4Linux驅動程式的DVB和[HDTV卡都有支援](../Page/HDTV.md "wikilink")，MythTV
同時支援透過[FireWire從數位機上盒得到軟體](../Page/FireWire.md "wikilink")。

## 另見

  - [Video4Linux](../Page/Video4Linux.md "wikilink")
  - [LinuxTV](../Page/LinuxTV.md "wikilink")
  - [HTPC](../Page/HTPC.md "wikilink")

## 注釋

<references />

## 外部連結

  - [Official website](http://www.mythtv.org/)

  - [MythTV against other FLOSS Media Centers in Comparison
    Chart](http://www.telematicsfreedom.org/en/flossmediacenter)

[Category:自由軟體](../Category/自由軟體.md "wikilink")
[Category:Linux軟體](../Category/Linux軟體.md "wikilink")
[Category:MacOS軟體](../Category/MacOS軟體.md "wikilink")
[Category:使用Qt的軟體](../Category/使用Qt的軟體.md "wikilink")