**múm**，[冰島的](../Page/冰島.md "wikilink")[實驗](../Page/實驗音樂.md "wikilink")[音樂團體](../Page/樂團.md "wikilink")。

## 作品列表

### 專輯

  - Yesterday Was Dramatic - Today Is OK（2000）
  - Finally We Are No One（2002）
  - Summer Make Good（2004）
  - Go Go Smear the Poison Ivy（2007）
  - Sing Along to Songs You Don't Know（2009）
  - Smilewound（2013）

## 台灣巡演

2005年12月3日在[這牆音樂藝文展演空間](../Page/這牆音樂藝文展演空間.md "wikilink")；2013年6月11日在[Legacy](../Page/Legacy.md "wikilink")。

## 外部連結

### 官方網站

  - [官方推特](https://twitter.com/mumtheband)
  - [官網](http://www.mum.is/)

[Category:冰島樂團](../Category/冰島樂團.md "wikilink")
[Category:後搖滾樂團](../Category/後搖滾樂團.md "wikilink")
[Category:電子音樂團體](../Category/電子音樂團體.md "wikilink")
[Category:日本富士搖滾音樂祭參加歌手](../Category/日本富士搖滾音樂祭參加歌手.md "wikilink")
[Category:實驗音樂樂團](../Category/實驗音樂樂團.md "wikilink")