***N*-溴代丁二酰亚胺**或稱***N*-溴琥珀醯亞胺**\[1\]（），是[有机合成中的重要](../Page/有机合成.md "wikilink")[试剂](../Page/试剂.md "wikilink")。它被广泛应用于[自由基取代反应和](../Page/自由基取代反应.md "wikilink")[亲电加成反应中](../Page/亲电加成反应.md "wikilink")。NBS可以當作[溴的替代物](../Page/溴.md "wikilink")。

## 製備

NBS在市面上是可以取得的，但也可以在實驗室中被合成出來。合成方法為將0.16[莫耳](../Page/摩尔_\(单位\).md "wikilink")（約16
g）的[丁二醯亞胺溶於至於冰的](../Page/丁二醯亞胺.md "wikilink")[氫氧化鈉溶液中](../Page/氫氧化鈉.md "wikilink")（6.4
g溶入40 mL水中及30 g碎冰）並加入26.38 g的溴（0.165莫耳，約8.5
mL），持續攪拌約5分鐘後，[過濾並以清水清洗產物](../Page/過濾.md "wikilink")，最後將產物以40 °C乾燥。此作法[產率約為](../Page/產率.md "wikilink")69.5
%（約20 g）。\[2\]

除了[沃爾–齊格勒溴化反應使用不純的NBS能有比較高的產率](../Page/沃爾–齊格勒溴化反應.md "wikilink")，在進行其他反應時通常不會使用不純的NBS（略黃），這是因为使用不純的NBS可能會產生不可預期的結果。而純化NBS的方法為使用90–95 °C
的水（10 g NBS溶入100 mL水中）進行[再結晶](../Page/再結晶.md "wikilink")。\[3\]

## 反應

### 對[烯烴的加成反應](../Page/烯烴.md "wikilink")

NBS能跟烯烴（如下圖 **1**）在水溶液中反應產生（如下圖
**2**）。進行此反應的方法是將NBS分批加入烯烴的[DMSO或](../Page/二甲基亞碸.md "wikilink")[DME或](../Page/二甲氧基乙烷.md "wikilink")[THF或](../Page/四氫呋喃.md "wikilink")[叔丁醇其中之一的](../Page/叔丁醇.md "wikilink")50 %水溶液中。\[4\]
的生成和水分子的快速攻擊使這個反應為[馬可尼可夫加成反應且為反式加成](../Page/馬氏規則.md "wikilink")（[立體選擇性](../Page/立體選擇性.md "wikilink")）。\[5\]

  -
    [NBS_Bromohydrin_Formation_Scheme.png](https://zh.wikipedia.org/wiki/File:NBS_Bromohydrin_Formation_Scheme.png "fig:NBS_Bromohydrin_Formation_Scheme.png")

副產物包括α-溴代酮跟二溴取代化合物，而使用再結晶過的NBS即可以減少這些副產物的產生。

若此反應改使用其他[親核基](../Page/親核基.md "wikilink")（而非以水當親核基），則可以合成其他各式各樣擁有雙取代基的烷類，下圖即為一例。\[6\]

  -
    [NBS_Fluorination.png](https://zh.wikipedia.org/wiki/File:NBS_Fluorination.png "fig:NBS_Fluorination.png")（一對外消旋體）

### [烯丙基類與](../Page/烯丙基.md "wikilink")[苯甲基類化合物的溴化反應](../Page/苯甲基.md "wikilink")

進行烯丙基類與苯甲基類化合物的溴化反應的基本條件是在[迴流裝置中將NBS加入無水](../Page/回流_\(實驗裝置\).md "wikilink")[四氯化碳](../Page/四氯化碳.md "wikilink")，並加入[自由基引發劑](../Page/自由基引發劑.md "wikilink")（如[偶氮二異丁腈](../Page/偶氮二異丁腈.md "wikilink")（AIBN）、[過氧化苯甲醯](../Page/過氧化苯甲醯.md "wikilink")）或照以輻射抑或兩者同时使用促使[自由基](../Page/自由基.md "wikilink")[起始反應的發生](../Page/起始反應.md "wikilink")。\[7\]\[8\]
反應過程中產生的烯丙基自由基或苯甲基自由基是相對穩定的[中間產物](../Page/中間產物.md "wikilink")，所以這個反應的主要產物依舊為烯丙基類與苯甲基類化合物（溴化物）。此反應亦稱作[沃爾–齊格勒溴化反應](../Page/沃爾–齊格勒溴化反應.md "wikilink")。\[9\]\[10\]

  -
    [NBS_Allylic_Bromination_Scheme.png](https://zh.wikipedia.org/wiki/File:NBS_Allylic_Bromination_Scheme.png "fig:NBS_Allylic_Bromination_Scheme.png")

本反應中使用的四氯化碳溶劑必須是無水的，若水存在則產物可能會发生[水解](../Page/水解.md "wikilink")。\[11\]
因此通常會加入[碳酸鋇維持反應過程中無水和無酸的狀態](../Page/碳酸鋇.md "wikilink")。

### 羰基衍生物的溴化反應

羰基衍生物的α-溴化反應可以利用NBS進行自由基反應或酸催化反應。舉例來說，[己醯氯](../Page/己醯氯.md "wikilink")（如下圖**1**）經過酸和NBS的催化反應後，α位即接上溴原子完成溴化。\[12\]

  -
    [NBS_Alpha-Bromination_Scheme.png](https://zh.wikipedia.org/wiki/File:NBS_Alpha-Bromination_Scheme.png "fig:NBS_Alpha-Bromination_Scheme.png")

然而為了使此反應有較高的產率和較少其他副產物產生，通常會使用NBS加上[烯醇鹽或](../Page/烯醇.md "wikilink")[烯醇醚](../Page/烯醇醚.md "wikilink")，這樣也能產生α-溴化反應。\[13\]\[14\]

### 芳香類化合物的溴化反應

擁有高電子密度的[芳香類化合物像是](../Page/芳香性.md "wikilink")[酚](../Page/酚.md "wikilink")、[苯胺或某些](../Page/苯胺.md "wikilink")[雜環化合物](../Page/雜環化合物.md "wikilink")\[15\]也可以用NBS溴化。\[16\]\[17\]
如果此反應以[二甲基甲醯胺等](../Page/二甲基甲醯胺.md "wikilink")[極性](../Page/極性.md "wikilink")[非質子性溶劑](../Page/非質子性溶劑.md "wikilink")，則可以很容易地讓溴原子接在對位上。\[18\]

### 霍夫曼重排反應

在霍夫曼重排反應中，NBS可以用來代替單質溴，在強鹼作用下與一級[醯胺反應](../Page/醯胺.md "wikilink")。

  -
    [Hoffmann_Rearrangement_NBS.png](https://zh.wikipedia.org/wiki/File:Hoffmann_Rearrangement_NBS.png "fig:Hoffmann_Rearrangement_NBS.png")

### 醇類的選擇性氧化反應

NBS被發現可以[氧化醇類](../Page/氧化還原反應.md "wikilink")。[艾里亞斯·詹姆斯·科里等人發現同時存在有一級醇與二級醇的](../Page/艾里亞斯·詹姆斯·科里.md "wikilink")[DME溶液中](../Page/二甲氧基乙烷.md "wikilink")，NBS可以選擇性地氧化二級醇。\[19\]

  -
    [NBS_Oxidation_Corey.png](https://zh.wikipedia.org/wiki/File:NBS_Oxidation_Corey.png "fig:NBS_Oxidation_Corey.png")

## 注意事項

相較於溴，NBS是個相對安全的反應試劑，但還是要注意避免吸入體內。純的NBS為白色固體，但會隨時間逐漸分解產生溴，這會使之略帶黃色。NBS應該儲藏在冰箱中。

大部分使用NBS試劑的反應都為[放熱反應](../Page/放熱.md "wikilink")，所以進行大規模反應的時候應特別注意安全。

## 參見

  - [自由基取代反應](../Page/自由基取代反應.md "wikilink")
  - [親電加成反應](../Page/親電加成反應.md "wikilink")

## 参考文献

[Category:有机溴化合物](../Category/有机溴化合物.md "wikilink")
[Category:有机化学试剂](../Category/有机化学试剂.md "wikilink")
[Category:丁二酰亚胺](../Category/丁二酰亚胺.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.