《**NANA
2**》，中文名又叫《**世上的另一個我2**》，是電影《[NANA](../Page/Nana_\(電影\).md "wikilink")》的续作，由[東寶株式會社製作](../Page/東寶株式會社.md "wikilink")，于2006年12月6日在[日本上映](../Page/日本.md "wikilink")。剧本基于[矢泽爱](../Page/矢泽爱.md "wikilink")[漫画](../Page/漫画.md "wikilink")《》的剧情，顺着上一部电影的剧情向下发展。

## 主要角色

  - 大崎娜娜（）：[中島美嘉](../Page/中島美嘉.md "wikilink") 飾。
  - 小松奈奈（）：[市川由衣](../Page/市川由衣.md "wikilink")
    飾（第一集中原本是由[宮崎葵飾演](../Page/宮崎葵.md "wikilink")）。
  - 本城莲：[姜暢雄](../Page/姜暢雄.md "wikilink")
    飾（第一集中原本是由[松田龍平飾演](../Page/松田龍平.md "wikilink")）。
  - 一之濑巧（）：[玉山鐵二](../Page/玉山鐵二.md "wikilink") 飾。
  - 冈崎真一：[本鄉奏多](../Page/本鄉奏多.md "wikilink")
    飾。本鄉原本在第一集中就是飾演岡崎真一的預定演員，但卻因故沒有實際參與電影的拍攝（在第一集中飾演該角色的演員原本是[松山研一](../Page/松山研一.md "wikilink")）。
  - 寺岛伸夫：[成宮寬貴](../Page/成宮寬貴.md "wikilink") 飾。
  - 芹泽蕾拉（）：[伊藤由奈](../Page/伊藤由奈.md "wikilink") 飾。
  - 早乙女淳子：[能世安奈](../Page/能世安奈.md "wikilink")（）飾。
  - 高倉京助：[高山猛久](../Page/高山猛久.md "wikilink") 飾。
  - 高木泰士：（）：[丸山智己](../Page/丸山智己.md "wikilink") 飾。
  - 藤枝直樹：（）：[水谷百輔](../Page/水谷百輔.md "wikilink") 飾。

## 外部連結

  -
  - [《NANA 2 @ D\&HLiT》](http://www.damagedsoul.net/nana/movie2)

[ja:NANA\#NANA2](../Page/ja:NANA#NANA2.md "wikilink")

[Category:NANA](../Category/NANA.md "wikilink")
[Category:2006年日本電影](../Category/2006年日本電影.md "wikilink")
[Category:日語電影](../Category/日語電影.md "wikilink")
[Category:愛情片](../Category/愛情片.md "wikilink")
[Category:續集電影](../Category/續集電影.md "wikilink")
[Category:日本漫畫改編真人電影](../Category/日本漫畫改編真人電影.md "wikilink")
[Category:TBS製作的電影](../Category/TBS製作的電影.md "wikilink")
[Category:淺野妙子劇本作品](../Category/淺野妙子劇本作品.md "wikilink")