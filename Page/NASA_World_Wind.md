[Globe-worldwind.jpg](https://zh.wikipedia.org/wiki/File:Globe-worldwind.jpg "fig:Globe-worldwind.jpg")\]\]

**NASA World
Wind**（有直譯為**世界风**者），是[NASA发布的一个](../Page/NASA.md "wikilink")[开放源代码的](../Page/开放源代码.md "wikilink")[地理](../Page/地理.md "wikilink")[科普](../Page/科普.md "wikilink")[软件](../Page/软件.md "wikilink")，由[NASA
Research开发](../Page/NASA_Research.md "wikilink")，[NASA Learning
Technologies來發展](../Page/NASA_Learning_Technologies.md "wikilink")，它是一个可视化[地球仪](../Page/地球仪.md "wikilink")，将NASA、USGS以及其它[WMS服务商提供的图像通过一个三维的](../Page/WMS.md "wikilink")[地球模型展现](../Page/地球.md "wikilink")，近期还包含了[火星和](../Page/火星.md "wikilink")[月球的展现](../Page/月球.md "wikilink")。

用户可在所观察的行星上随意地旋转、放大、缩小，同时可以看到地名和行政区划。软件还包含了一个软件包，能够浏览地图及其它由[因特网上的](../Page/因特网.md "wikilink")[OpenGIS
Web Mapping
Service提供的图像](../Page/OpenGIS_Web_Mapping_Service.md "wikilink")。

World Wind（version
1.2e）现在已经包含了到[维基百科的](../Page/维基百科.md "wikilink")[超链接](../Page/超链接.md "wikilink")。

## 目前软件所使用的数据

[120.14979E_30.26805N.png](https://zh.wikipedia.org/wiki/File:120.14979E_30.26805N.png "fig:120.14979E_30.26805N.png")
低分辨率的[Blue
marble数据现在包含的初始安装内](../Page/藍色彈珠.md "wikilink")，当用户放大到特定区域时，附加的高分辨率数据将会自动从NASA服务器上被下载。

<table>
<tbody>
<tr class="odd">
<td><p>图像／地形数据：</p>
<ul>
<li><a href="../Page/藍色彈珠.md" title="wikilink">藍色彈珠</a> 图像</li>
<li><a href="../Page/陸地衛星計畫.md" title="wikilink">陸地衛星計畫</a> 图像</li>
<li><a href="../Page/美國地質調查局.md" title="wikilink">美國地質調查局</a> 图像</li>
<li><p>地形数据</p></li>
</ul></td>
<td></td>
<td><p>动画数据层：</p>
<ul>
<li></li>
<li><a href="../Page/中分辨率成像光谱仪.md" title="wikilink">中分辨率成像光谱仪</a></li>
<li></li>
</ul></td>
</tr>
</tbody>
</table>

附加的数据也可以被加入，如行政边界，地名以及经纬度等。

## 參見

  - [Google地图](../Page/Google地图.md "wikilink")
  - [Google地球](../Page/Google地球.md "wikilink")

## 外部链接

  - [NASA World Wind首頁](http://worldwind.arc.nasa.gov)
  - [World Wind Central](http://www.worldwindcentral.com)
  - [World Wind Wiki](http://wiki.worldwindcentral.com)
  - [上帝之眼](http://www.godeyes.cn/) - Google Earth与World
    Wind爱好者园地，自行开发了World Wind的简体中文版。
  - [NASA World Wind SDK
    Tutorial](https://web.archive.org/web/20121224015417/http://ifgi.uni-muenster.de/worldwind-tutorial/)

[Category:虛擬地球](../Category/虛擬地球.md "wikilink")
[Category:开放源代码](../Category/开放源代码.md "wikilink")