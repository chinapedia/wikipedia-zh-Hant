**NBA G聯盟**（NBA G
League）是[美國](../Page/美國.md "wikilink")[國家籃球協會官方支持篮球发展之组织](../Page/國家籃球協會.md "wikilink")。联盟在2001年秋季開始時一共有8支队伍。直至2005年[夏季](../Page/夏季.md "wikilink")，曾被稱爲**国家篮球发展联盟**（[英语](../Page/英语.md "wikilink")：**N**ational
**B**asketball **D**evelopment
**L**eague，[縮寫](../Page/縮寫.md "wikilink")：**NBDL**）；此後改名為NBA发展联盟（[英语](../Page/英语.md "wikilink")：**NBA
Development
League**，[縮寫](../Page/縮寫.md "wikilink")：**D-League**）。2017年起，與[佳得樂簽下多年合約](../Page/佳得樂.md "wikilink")，並把品牌重塑為NBA
G聯盟。截至2018/19球季，參賽球隊數目為27支。目前大部份NBA球隊均有其所專屬的G League球隊。

## 歷史

### 國家籃球發展聯盟(NBDL) (2001–2005)

這個聯盟最早在2001-02賽季以**國家籃球發展聯盟**(，簡稱**NBDL**)的名稱成立。最早的八支球队\[1\]\[2\]全都位於美國東南部地區(即[弗吉尼亚州](../Page/弗吉尼亚州.md "wikilink")，[南卡罗来纳州](../Page/南卡罗来纳州.md "wikilink")，[北卡罗来纳州](../Page/北卡罗来纳州.md "wikilink")，[阿拉巴马州和](../Page/阿拉巴马州.md "wikilink")[喬治亞州](../Page/喬治亞州.md "wikilink"))。

### NBA發展聯盟 (2005–2017)

2005年，聯盟被更名為**NBA發展聯盟**(，簡稱**NBA
D-League**)\[3\]，發展聯盟改名是NBA集體談判協議的其中一環，也讓發展聯盟透過與NBA的聯繫來吸引更多球迷\[4\]。在當年的休賽季，由[大衛·卡恩領導的西南籃球有限公司獲得聯盟許可經營四支球隊](../Page/大衛·卡恩.md "wikilink")\[5\]西南籃球公司隨後買下了三支現有球隊與成立了一支新球隊：分別是[阿爾伯克基雷鳥](../Page/阿爾伯克基雷鳥.md "wikilink")、[奧斯汀公牛與](../Page/奧斯汀公牛.md "wikilink")[沃思堡飞翔者](../Page/沃思堡飞翔者.md "wikilink")\[6\]和新成立的[土爾沙66人](../Page/土爾沙66人.md "wikilink")\[7\]。[ABA聯盟的](../Page/美国篮球协会_\(21世纪\).md "wikilink")[阿肯色震圈在](../Page/阿肯色震圈.md "wikilink")2005-06賽季時加盟發展聯盟。2006年2月,贝克斯菲尔德果醬队宣布加入联盟，成为第一支进入发展联盟的[加州球队](../Page/加州.md "wikilink")。兩個月後,联盟宣布四支原属[大陆篮球协会](../Page/大陆篮球协会.md "wikilink")（**C**ontinental
**B**asketball
**A**ssociation，簡稱**[CBA](../Page/CBA.md "wikilink")**）的球队加盟，分別是四支球队分别是[達科他巫師](../Page/達科他巫師.md "wikilink")、[蘇瀑天空力量](../Page/蘇瀑天空力量.md "wikilink")、[愛達荷奔腾和一支原訂於CBA下個賽季加盟的](../Page/愛達荷奔腾.md "wikilink")[科罗拉多十四人](../Page/科罗拉多十四人.md "wikilink")\[8\]。不久之後，聯盟宣布新的擴增球隊[阿納海姆兵工廠](../Page/阿納海姆兵工廠.md "wikilink")。\[9\]與[洛杉磯防禦者](../Page/洛杉磯防禦者.md "wikilink")。防禦者是第一支由NBA球隊直接經營的發展聯盟隊伍，由[洛杉磯湖人單獨經營](../Page/洛杉磯湖人.md "wikilink")。\[10\]

然而,不斷的向西擴張導致了NBA擁有的[羅阿諾克炫目](../Page/羅阿諾克炫目.md "wikilink")\[11\]與[費耶特維爾愛國者宣告解散](../Page/費耶特維爾愛國者.md "wikilink")\[12\]。[佛羅里達火焰也因為場館時間表調度上的困難而宣布暫停運作](../Page/佛羅里達火焰.md "wikilink")。\[13\]2006-07賽季結束後，美國東南部都沒有其他的球隊加入聯盟，直到2016年的擴張隊伍[格林斯博羅蜂群加盟](../Page/格林斯博羅蜂群.md "wikilink")。
在2006年到2009年不斷的擴大聯盟後，聯盟內部的球隊數量相當穩定，只有零星的幾個球隊加盟。2009年，[休斯頓火箭與](../Page/休斯頓火箭.md "wikilink")[里奧格蘭德山谷毒蛇簽訂了第一個專一的隊伍合作關係](../Page/里奧格蘭德山谷毒蛇.md "wikilink")，稱為複合模式。這同時也讓NBA球隊紛紛收購或是與發展聯盟球隊以複合模式簽約。隨著更多的NBA球隊參與，聯盟又在一次的擴大規模。

到了2015年時，最後一支同時隸屬於多支NBA球隊的[韋恩堡瘋蟻被](../Page/韋恩堡瘋蟻.md "wikilink")[印第安納溜馬收購](../Page/印第安納溜馬.md "wikilink")，讓發展聯盟正式進入了第一個所有球隊都只從屬於單一NBA隊伍的賽季。由於不再有任何無關聯的發展聯盟球隊離開，剩下的NBA球隊開始收購或向發展聯盟球隊尋求合作，並將其搬遷到母隊附近。2015年，[多倫多猛龍成立了](../Page/多倫多猛龍.md "wikilink")[暴龍905](../Page/暴龍905.md "wikilink")。\[14\]\[15\]
2016年，發展聯盟擴增了三個由NBA球隊擁有的下屬球隊，成為自2007年以來最大的發展聯盟擴張。夏洛特黃蜂隊創造了[格林斯博羅蜂群](../Page/格林斯博羅蜂群.md "wikilink")，[布魯克林籃網成立](../Page/布魯克林籃網.md "wikilink")[長島籃網和](../Page/長島籃網.md "wikilink")[芝加哥公牛成立](../Page/芝加哥公牛.md "wikilink")[風城公牛](../Page/風城公牛.md "wikilink")。

### NBA G聯盟 (2017–至今)

在2017 - 18賽季，發展聯盟與佳得樂簽下了長期合約，並宣布將改名為NBA佳得樂聯盟，賽季開始前名稱正式訂為“NBA
G聯賽”。\[16\]\[17\]\[18\]\[19\]聯盟中許多隊伍也趁勢重塑了球隊品牌[伊利海鷹搬遷到](../Page/伊利海鷹_\(2008–2017\).md "wikilink")[佛罗里达州](../Page/佛罗里达州.md "wikilink")[莱克兰並更名為](../Page/莱克兰_\(佛罗里达州\).md "wikilink")[萊克蘭魔術](../Page/萊克蘭魔術.md "wikilink")，[新的伊利海鷹](../Page/伊利海鷹.md "wikilink")。在[加利福尼亞州](../Page/加利福尼亞州.md "wikilink")[安大略成立了](../Page/安大略_\(加利福尼亞州\).md "wikilink")[阿瓜卡連特快艇](../Page/阿瓜卡連特快艇.md "wikilink")。[密西西比州](../Page/密西西比州.md "wikilink")[南海文](../Page/南海文_\(密西西比州\).md "wikilink")
的孟菲斯疾行，[威斯康辛州](../Page/威斯康辛州.md "wikilink")[奧什科甚成立了](../Page/奧什科甚.md "wikilink")[威斯康辛鹿群](../Page/威斯康辛鹿群.md "wikilink")。洛杉磯防禦者改為[南灣湖人隊](../Page/南灣湖人隊.md "wikilink")，以反映聯盟名稱的變化。

## 球員

部分從前在[NBA选秀中被選出和被NBA解约的球员都参加过这个联盟的比赛](../Page/NBA选秀.md "wikilink")。NBA球队亦开始签一些在D-League中成功的球员。2005-2006賽季增加召回和下放的規定：每隊NBA球隊整季保持15名球員名額，包括3名不活躍名額。被下放的NBA合約球員需要不多於2年的NBA經驗。無論此球員一直在D-League打球期間、被召回或下放，其名須在3名不活躍名額中，直至NBA主隊的主[教練在NBA賽前決定是否將其換入](../Page/教練.md "wikilink")12名出場名額。每一位可被下放的球員可以被最多3次召回和3次下放。由D-League選秀或個別簽約的球員則不受此限，可以獨自與NBA球隊簽約。

在NBA成功的部分球员曾在D-League打过球，包括[拉夫·阿尔斯通](../Page/拉夫·阿尔斯通.md "wikilink")\[20\]、[克利斯·安德森](../Page/克利斯·安德森.md "wikilink")、[迪文·布朗](../Page/迪文·布朗.md "wikilink")\[21\]、[鲍比·西蒙森](../Page/鲍比·西蒙森.md "wikilink")\[22\]與[马库斯·费泽尔](../Page/马库斯·费泽尔.md "wikilink")\[23\]。[东亚球員方面](../Page/东亚.md "wikilink")，[孟克·巴特尔曾经在NBDL](../Page/孟克·巴特尔.md "wikilink")2004-2005赛季短暂效力过亨茨维尔飞行队，而[陳信安也曾於](../Page/陳信安.md "wikilink")2002年在莫比爾狂歡者隊見習練球三星期。[田臥勇太則在](../Page/田臥勇太.md "wikilink")2005-2006赛季效力阿尔伯克基雷鸟队至3月16日\[24\]，並在2006-2007赛季效力贝克斯菲尔德果酱队。[河升镇在](../Page/河升镇.md "wikilink")2005-2006赛季效力沃斯堡飞行队，並在2006-2007赛季效力阿纳海姆兵工厂队。

## 小聯盟系統與相關觀感

在2005-2006賽季以前，NBDL和CBA都與NBA並無從屬關係。而歷史上比NBDL更悠久的CBA擔任了NBA農場系統的部分角色，其中包括十天合約的規定：通常在NBA球隊因傷需要更多球員或青睞某個別CBA球員的情形下，NBA球隊以NBA最低工資簽下CBA球員十天。合約可以續簽十天。共二十天后，NBA球隊必須續簽此名球員至賽季完結或將他退回CBA。但由於CBA和NBA的合作關係一向強差人意，雙方並無締結類似[美國職棒大聯盟與](../Page/美國職棒大聯盟.md "wikilink")[小聯盟球的農場系統關係](../Page/美國職棒小聯盟.md "wikilink")。另外由於NBA球隊可自由與各方球員簽約，包括來自歐洲、南美洲、非洲和亞洲等地的球員，NBA對比賽水平較NBA低的CBA球員並不一定青睞。

2005年3月，國家籃球協會主席\[25\][大卫·斯特恩宣布一个將联盟扩充为擁有](../Page/大卫·斯特恩.md "wikilink")15队的[小联盟](../Page/小联盟.md "wikilink")[農場系统的计划](../Page/農場系统.md "wikilink")。两队NBA球队將共享一支发展联盟球队。2006/07赛季，联盟拥有12支球队\[26\]，每队发展联盟球队将隶属于1至3队NBA球队。2005-2006賽季是小聯盟計劃的第一個賽季；NBA球隊對於D-League作爲NBA小聯盟的計劃的觀感不一\[27\]。底特律活塞队[經理Joe](../Page/經理.md "wikilink")
Dumars認爲作出評論前應先知道更多。一名NBA西部联盟的[總經理認爲NBA球隊對被下放的球員被培訓的程度沒有控制](../Page/總經理.md "wikilink")，因此整個計劃浪費時閒。沃斯堡飞行队的[教練Sam](../Page/教練.md "wikilink")
Vincent認爲與其讓球員在NBA坐冷板凳，還不如在D-League上場打球。丹佛掘金隊的教練George
Karl在D-League宣布四支球队加盟的記者招待會中發言支持NBA應有農場系统。George
Karl本人曾在CBA的Montana Golden
Nuggets（蒙大拿金磚隊）執教，並於1981和1983年度贏得兩次CBA最佳教練獎。曾在D-League和多伦多猛龙队打球的前球員[控球後衛](../Page/控球後衛.md "wikilink")[安德烈·巴瑞特](../Page/安德烈·巴瑞特.md "wikilink")\[28\]與目前在受傷名單中的[前鋒](../Page/前鋒.md "wikilink")[佩普·索乌](../Page/佩普·索乌.md "wikilink")\[29\]在一次[NBATV播出的訪問中認爲D](../Page/NBATV.md "wikilink")-League提供上場機會，讓他們在NBA打球時會有信心和更加珍惜機會\[30\]。

## 球隊

目前聯盟總計有27支球隊，每支球隊都有從屬的NBA母隊。NBA聯盟中鵜鶘、金塊、拓荒者還沒有自己的發展聯盟球隊。

### 現有球隊

| 東部賽區                                             |
| ------------------------------------------------ |
| 賽區                                               |
| 大西洋組                                             |
| **[缅因红爪](../Page/缅因红爪.md "wikilink")**           |
| **[暴龍905](../Page/暴龍905.md "wikilink")**         |
| **[威斯特徹斯特尼克](../Page/威斯特徹斯特尼克.md "wikilink")**   |
| 中央組                                              |
| **[韋恩堡瘋蟻](../Page/韋恩堡瘋蟻.md "wikilink")**         |
| **[大急流城驅動](../Page/大急流城驅動.md "wikilink")**       |
| **[風城公牛](../Page/風城公牛.md "wikilink")**           |
| **[威斯康辛鹿群](../Page/威斯康辛鹿群.md "wikilink")**       |
| 東南組                                              |
| **[德拉瓦藍衣](../Page/德拉瓦藍衣.md "wikilink")**         |
| **[艾利灣海鷹](../Page/艾利灣海鷹.md "wikilink")**         |
| **[格林斯博羅蜂群](../Page/格林斯博羅蜂群.md "wikilink")**     |
| **[萊克蘭魔術](../Page/萊克蘭魔術.md "wikilink")**         |
| 西部賽區                                             |
| 賽區                                               |
| 中西組                                              |
| **[孟菲斯疾行](../Page/孟菲斯疾行.md "wikilink")**         |
| **[奧克拉荷馬城藍色](../Page/奧克拉荷馬城藍色.md "wikilink")**   |
| **[蘇瀑天空力量](../Page/蘇瀑天空力量.md "wikilink")**       |
| 西南組                                              |
| **[里奧格蘭德山谷毒蛇](../Page/里奧格蘭德山谷毒蛇.md "wikilink")** |
| **[鹽湖城明星](../Page/鹽湖城明星.md "wikilink")**         |
| **[德克薩斯傳奇](../Page/德克薩斯傳奇.md "wikilink")**       |
| 太平洋組                                             |
| **[北亞利桑那太陽](../Page/北亞利桑那太陽.md "wikilink")**     |
| **[雷諾大角羊](../Page/雷諾大角羊.md "wikilink")**         |
| **[圣克鲁斯勇士](../Page/圣克鲁斯勇士.md "wikilink")**       |
| **[南灣湖人](../Page/南灣湖人.md "wikilink")**           |

### 預計擴增隊伍

| 預計擴增隊伍 |
| ------ |
| 隊伍     |

### 球隊經營權和NBA的隸屬關係

整個NBA G聯盟的經營模式不盡相同。當NBA球隊越來越願意投資NBA G聯盟時，發展出了兩套相異的經營模式：

  - NBA球隊擁有發展聯盟球隊的經營權。
  - NBA球隊與發展聯盟球隊保持合作關係，發展聯盟球隊依然獨立經營，由NBA球隊贊助與指導。

NBA球隊持有完整經營權的模式始於2006年，當時NBA球隊開始逐漸收購發展聯盟球隊。當時[洛杉磯湖人收購了屬於自己的發展聯盟球隊](../Page/洛杉磯湖人.md "wikilink")，最早被稱為[洛杉磯防禦者](../Page/洛杉磯防禦者.md "wikilink")(現為[南灣湖人](../Page/南灣湖人.md "wikilink"))，隨後[聖安東尼奧馬刺在](../Page/聖安東尼奧馬刺.md "wikilink")2007年收購[奧斯汀公牛](../Page/奧斯汀公牛.md "wikilink")(現為奧斯汀馬刺隊）和[俄克拉荷馬城雷霆於](../Page/俄克拉荷馬城雷霆.md "wikilink")2008年收購了[土爾沙66人](../Page/土爾沙66人.md "wikilink")（現為[俄克拉荷馬城藍色](../Page/俄克拉荷馬城藍色.md "wikilink")）。
這讓NBA球隊投入發展聯盟市場，除了收購現有球隊的經營權，也有球隊以擴張球隊的方式創建專屬於自己的發展聯盟隊伍。2011年，[克利夫蘭騎士收購](../Page/克利夫蘭騎士.md "wikilink")[新墨西哥雷鳥成為](../Page/新墨西哥雷鳥.md "wikilink")[坎頓劍客](../Page/坎頓劍客.md "wikilink")，[金州勇士收購](../Page/金州勇士.md "wikilink")[達科他巫師](../Page/達科他巫師.md "wikilink")，一年後勇士將巫師搬遷並改名為[聖克魯斯勇士](../Page/聖克魯斯勇士.md "wikilink")。2013年，費城76人隊收購經營困難的[猶他閃電](../Page/猶他閃電.md "wikilink")，並將他們遷移到[特拉華州的](../Page/特拉華州.md "wikilink")[紐瓦克](../Page/紐瓦克.md "wikilink")，成為[特拉華87人](../Page/特拉華87人.md "wikilink")（現為[特拉華藍衣](../Page/特拉華藍衣.md "wikilink")，並在該州最大的城市[威明頓打球](../Page/威尔明顿_\(特拉华州\).md "wikilink")）。2014年，[紐約尼克成立](../Page/紐約尼克.md "wikilink")[威斯特徹斯特尼克](../Page/威斯特徹斯特尼克.md "wikilink")，成為第七支擁有發展聯盟球隊的隊伍。2015年，[多倫多暴龍成立暴龍](../Page/多倫多暴龍.md "wikilink")905。2017年，[明尼蘇達灰狼收購](../Page/明尼蘇達灰狼.md "wikilink")[愛荷華能源](../Page/愛荷華能源.md "wikilink")，並將球隊改名為[愛荷華灰狼](../Page/愛荷華灰狼.md "wikilink")。
2009年，[休斯頓火箭和](../Page/休斯頓火箭.md "wikilink")[里奧格蘭特德山谷毒蛇發展一套新的單一隊伍合作模式](../Page/里奧格蘭特德山谷毒蛇.md "wikilink")，也被稱為複合模式。2010年11月，[新澤西網和](../Page/新澤西網.md "wikilink")[斯普林菲爾德鎧甲宣布他們將在](../Page/斯普林菲爾德鎧甲.md "wikilink")2011-12賽季開始建立了合作關係。2011年6月，紐約尼克和[伊利灣海鷹宣布他們將建立合作關係](../Page/伊利灣海鷹.md "wikilink")。2012年5月，[波特蘭開拓者與](../Page/波特蘭開拓者.md "wikilink")[愛達荷牛仔建立了合作關係](../Page/愛達荷牛仔.md "wikilink")。接下來的一個月，[波士頓凱爾特人和](../Page/波士頓凱爾特人.md "wikilink")[緬因紅爪建立了合作關係](../Page/緬因紅爪.md "wikilink")。2013年6月，[邁阿密熱火宣布與](../Page/邁阿密熱火.md "wikilink")[蘇瀑天空力量建立了單一合作關係](../Page/蘇瀑天空力量.md "wikilink")。2013年7月，[薩克拉門托國王和雷诺大角羊](../Page/薩克拉門托國王.md "wikilink")（現為斯托克頓國王隊）合作。2013-14賽季結束後，牛仔隊結束了他們與開拓者的合作，並在2014年6月宣布與[猶他爵士合作](../Page/猶他爵士.md "wikilink")。在2013-14賽季之後，鎧甲隊搬到了密歇根州的大急流城，並與[底特律活塞隊有合作](../Page/底特律活塞隊.md "wikilink")。從2014年到2017年，[孟菲斯灰熊與愛荷華能源有單一隊伍合作](../Page/孟菲斯灰熊.md "wikilink")。2015年，最後一支從屬於多支球隊的[韋恩堡瘋蟻被](../Page/韋恩堡瘋蟻.md "wikilink")[印第安納步行者收購](../Page/印第安納步行者.md "wikilink")，使2015-16賽季成為第一個所有球隊都是從屬單一NBA球隊的賽季。

在某些情況下，NBA球隊會收購原本是合作關係的球隊，進而擁有發展聯盟球隊的完整經營權。2015年3月24日，猶他爵士收購了他們的下屬球隊愛荷華奔騰，一個賽季後遷移並改名為鹽湖城明星。2016年4月11日，菲尼克斯太陽收購他們的下屬球隊貝克斯菲爾德果醬，並宣布將球隊搬遷到[亞利桑那州](../Page/亞利桑那州.md "wikilink")[普雷斯科特山谷](../Page/普雷斯科特山谷.md "wikilink")，改名為北亞利桑那太陽。2016年10月20日，薩克拉門托國王收購了前八個賽季的下屬球隊雷諾大角羊多數股權，並在2017-18賽季後將球隊移至加州斯托克頓，成為斯托克頓國王。2016年12月14日，魔術隊購買了他們的下屬球隊伊利灣海鷹，在2017年將球隊搬遷到佛羅里達州萊克蘭。2017年，邁阿密熱火收購了蘇瀑天空力量的多數股權。

#### 球隊經營權詳細狀況

  - **NBA球隊持有完整經營權：**阿瓜卡連特快船（洛杉磯快船），奧斯汀馬刺（聖安東尼奧馬刺），坎頓劍客（克利夫蘭騎士），首都Go-Go（華盛頓奇才），特拉華藍衣（費城76人），伊利灣海鷹（亞特蘭大老鷹），韋恩堡瘋蟻（印第安納步行者），格林斯博羅蜂群（夏洛特黃蜂），愛荷華灰狼（明尼蘇達森林狼），萊克蘭魔術（奧蘭多魔術），長島籃網（布魯克林籃網），孟菲斯疾行（孟菲斯灰熊），北亞利桑那太陽（菲尼克斯太陽），俄克拉荷馬城藍色（俄克拉荷馬城雷霆），暴龍905（多倫多暴龍），鹽湖城明星（猶他爵士），聖克魯斯勇士（金州勇士），蘇瀑天空力量（邁阿密熱火），南灣湖人（洛杉磯湖人），斯托克頓國王（薩克拉門托國王)，威斯特徹斯特尼克（紐約尼克），風城公牛（芝加哥公牛）和威斯康辛鹿群（密爾沃基雄鹿）。

<!-- end list -->

  - **獨立經營/複合模式經營：**大急流城驅動（底特律活塞），緬因紅爪（波士頓凱爾特人），里奧格蘭特德山谷毒蛇（休斯頓火箭）和德克薩斯傳奇（達拉斯小牛）。

<!-- end list -->

  - **沒有發展聯盟的NBA球隊：**丹佛掘金，新奧爾良鵜鶘和波特蘭拓荒者。

### 不再存在的隊伍

<table>
<thead>
<tr class="header">
<th><p>球队</p></th>
<th><p>城市</p></th>
<th><p>活跃年份</p></th>
<th><p>前NBA从属关系</p></th>
<th><p>注释</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/Greenville,_South_Carolina.md" title="wikilink">Greenville, South Carolina</a></p></td>
<td><p>2001–2003</p></td>
<td><p>无</p></td>
<td><p>联盟宣布解散</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/Mobile,_Alabama.md" title="wikilink">Mobile, Alabama</a></p></td>
<td><p>2001–2003</p></td>
<td><p>無</p></td>
<td><p>联盟宣布解散</p></td>
</tr>
<tr class="odd">
<td><p>（北）查尔斯顿鳄鱼</p></td>
<td><p><a href="../Page/Charleston,_South_Carolina.md" title="wikilink">Charleston, South Carolina</a></p></td>
<td><p>2001–2004</p></td>
<td><p>无</p></td>
<td><p>改名</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/Asheville,_North_Carolina.md" title="wikilink">Asheville, North Carolina</a></p></td>
<td><p>2001–2005</p></td>
<td><p>无</p></td>
<td><p>改名</p></td>
</tr>
<tr class="odd">
<td><p>哥伦布河龙</p></td>
<td><p><a href="../Page/Columbus,_Georgia.md" title="wikilink">Columbus, Georgia</a></p></td>
<td><p>2001–2005</p></td>
<td><p>无</p></td>
<td><p>今<a href="../Page/奥斯汀馬刺.md" title="wikilink">奥斯汀馬刺</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亨特维尔飞行.md" title="wikilink">亨特维尔飞行</a></p></td>
<td><p><a href="../Page/Huntsville,_Alabama.md" title="wikilink">Huntsville, Alabama</a></p></td>
<td><p>2001–2005</p></td>
<td><p>无</p></td>
<td><p>更名<a href="../Page/阿尔伯克基雷鸟.md" title="wikilink">阿尔伯克基雷鸟</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/Fayetteville,_North_Carolina.md" title="wikilink">Fayetteville, North Carolina</a></p></td>
<td><p>2001–2006</p></td>
<td><p><a href="../Page/夏洛特山貓.md" title="wikilink">夏洛特山貓</a>、<a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a>、<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a></p></td>
<td><p>联盟宣布解散</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/Roanoke,_Virginia.md" title="wikilink">Roanoke, Virginia</a></p></td>
<td><p>2001–2006</p></td>
<td><p><a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a>、<a href="../Page/費城76人.md" title="wikilink">費城76人</a>、<a href="../Page/華盛頓巫師.md" title="wikilink">華盛頓巫師</a></p></td>
<td><p>联盟宣布解散</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/Fort_Myers,_Florida.md" title="wikilink">Fort Myers, Florida</a></p></td>
<td><p>2001–2007</p></td>
<td><p><a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>、<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>、<a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a>、<a href="../Page/明尼蘇達灰狼.md" title="wikilink">明尼蘇達灰狼</a></p></td>
<td><p>所有者宣布解散</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿肯色震圈队.md" title="wikilink">阿肯色震圈队</a></p></td>
<td><p><a href="../Page/Little_Rock,_Arkansas.md" title="wikilink">Little Rock, Arkansas</a></p></td>
<td><p>2004–2007</p></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a>、<a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a>、<a href="../Page/孟菲斯灰熊.md" title="wikilink">孟菲斯灰熊</a>、<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>、<a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a></p></td>
<td><p>所有者停办</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/Fort_Worth,_Texas.md" title="wikilink">Fort Worth, Texas</a></p></td>
<td><p>2005–2007</p></td>
<td><p><a href="../Page/夏洛特山貓.md" title="wikilink">夏洛特山貓</a>、<a href="../Page/達拉斯獨行俠.md" title="wikilink">達拉斯獨行俠</a>、<a href="../Page/金州勇士.md" title="wikilink">金州勇士</a>、<a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>、<a href="../Page/費城76人.md" title="wikilink">費城76人</a>、<a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a></p></td>
<td><p>所有者停办</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/新墨西哥雷鸟.md" title="wikilink">阿尔伯克基/新墨西哥雷鸟队</a></p></td>
<td><p><a href="../Page/Albuquerque,_New_Mexico.md" title="wikilink">Albuquerque, New Mexico</a></p></td>
<td><p>2005–2011</p></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a>、<a href="../Page/達拉斯獨行俠.md" title="wikilink">達拉斯獨行俠</a>、<a href="../Page/印第安納溜馬.md" title="wikilink">印第安納溜馬</a>、<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>、<a href="../Page/新奥尔良黄蜂.md" title="wikilink">新奥尔良黄蜂</a>、<a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a>、<a href="../Page/費城76人.md" title="wikilink">費城76人</a>、<a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a>、<a href="../Page/沙加緬度國王.md" title="wikilink">沙加緬度國王</a>、<a href="../Page/西雅图超音速.md" title="wikilink">西雅图超音速</a>、<a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a></p></td>
<td><p>今<a href="../Page/坎頓劍客.md" title="wikilink">坎頓劍客</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/Tulsa,_Oklahoma.md" title="wikilink">Tulsa, Oklahoma</a></p></td>
<td><p>2005–2014</p></td>
<td><p><a href="../Page/奧克拉荷馬雷霆.md" title="wikilink">奧克拉荷馬雷霆</a>、<a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a>、<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a></p></td>
<td><p>今</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/Anaheim,_California.md" title="wikilink">Anaheim, California</a></p></td>
<td><p>2006–2009</p></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a>、<a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a>、<a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a>、<a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a></p></td>
<td><p>今</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/科罗拉多14人.md" title="wikilink">科罗拉多14人</a></p></td>
<td><p><a href="../Page/Broomfield,_Colorado.md" title="wikilink">Broomfield, Colorado</a></p></td>
<td><p>2006–2009</p></td>
<td><p><a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a>、<a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a>、<a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a></p></td>
<td><p>今<a href="../Page/德克薩斯傳奇.md" title="wikilink">德克薩斯傳奇</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/Bismarck,_North_Dakota.md" title="wikilink">Bismarck, North Dakota</a></p></td>
<td><p>2006–2012</p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a>、<a href="../Page/金州勇士.md" title="wikilink">金州勇士</a>、<a href="../Page/孟菲斯灰熊.md" title="wikilink">孟菲斯灰熊</a>、<a href="../Page/華盛頓巫師.md" title="wikilink">華盛頓巫師</a></p></td>
<td><p>今<a href="../Page/圣克鲁斯勇士.md" title="wikilink">圣克鲁斯勇士</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/Orem,_Utah.md" title="wikilink">Orem, Utah</a></p></td>
<td><p>2007–2011</p></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a>、<a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>、<a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a></p></td>
<td><p>今</p></td>
</tr>
</tbody>
</table>

### 球隊變遷

<small>褐色為現有球隊。
藍色為已解散球隊或更名前的球隊。
綠色為預計成立的球隊。
</small> <timeline> DateFormat = mm/dd/yyyy ImageSize = width:900
height:auto barincrement:25 Period = from:01/01/2001 till:01/01/2020
TimeAxis = orientation:horizontal PlotArea = right:140 left:20 bottom:20
top:0

Colors = id:barcolor value:rgb(0.99,0.7,0.7)

`        id:line     value:black`
`        id:bg       value:white`

PlotData=

` width:20 textcolor:black shift:(5,-5) anchor:from fontsize:s`
` bar:1  color:powderblue from:07/01/2001 till:06/01/2005 text:`[`阿什維爾高地`](../Page/阿什維爾高地.md "wikilink")` (2001–05)`
` bar:1  color:powderblue from:07/01/2005 till:06/01/2014 text:[土爾沙66人]] (2005–14)`
` bar:1  color:tan1 from:07/01/2014 till:end  text:`[`奧克拉荷馬藍色`](../Page/奧克拉荷馬藍色.md "wikilink")` (2014–至今)`

` bar:2  color:powderblue from:07/01/2001 till:06/01/2004 text:`[`（北）查爾斯頓鱷魚`](../Page/北查爾斯頓鱷魚.md "wikilink")` (2001–04)`
` bar:2  color:powderblue from:07/01/2004 till:06/01/2006 shift:60 text:`[`佛羅里達火焰`](../Page/佛羅里達火焰.md "wikilink")` (2004–06)`

` bar:3  color:powderblue from:07/01/2001 till:06/01/2005 text:`[`哥倫布河龍`](../Page/哥倫布河龍.md "wikilink")` (2001–05)`
` bar:3  color:powderblue from:07/01/2005 till:06/01/2014 text:`[`奧斯汀公牛`](../Page/奧斯汀公牛.md "wikilink")` (2005–14)`
` bar:3  color:tan1 from:07/01/2014 till:end  text:`[`奧斯汀馬刺`](../Page/奧斯汀馬刺.md "wikilink")` (2014–至今)`

` bar:4  color:powderblue from:07/01/2001 till:06/01/2006 text:`[`費耶特維爾愛國者`](../Page/費耶特維爾愛國者.md "wikilink")` (2001–06)`

` bar:5  color:powderblue from:07/01/2001 till:06/13/2003 text:`[`格林維爾律動`](../Page/格林維爾律動.md "wikilink")` (2001–03)`

` bar:6  color:powderblue from:07/01/2001 till:06/01/2005 text:`[`亨茨維爾飛翔`](../Page/亨茨維爾飛翔.md "wikilink")` (2001–05)`
` bar:6  color:powderblue from:07/01/2005 till:06/07/2011 text:`[`阿爾伯克基/新墨西哥雷鳥`](../Page/新墨西哥雷鳥.md "wikilink")` (2005–11)`
` bar:6  color:tan1 from:07/07/2011 till:end text:`[`坎頓劍客`](../Page/坎頓劍客.md "wikilink")` (2011–至今)`

` bar:7  color:powderblue from:07/01/2001 till:06/13/2003 text:`[`莫比爾狂歡者`](../Page/莫比爾狂歡者.md "wikilink")` (2001–03)`

` bar:8  color:powderblue from:07/01/2001 till:06/01/2006 text:`[`羅阿諾克炫目`](../Page/羅阿諾克炫目.md "wikilink")` (2001–06)`

` bar:9  color:powderblue from:07/01/2005 till:06/01/2007 text:`[`沃思堡飛翔者`](../Page/沃思堡飛翔者.md "wikilink")` (2005–07)`

` bar:10 color:powderblue from:07/01/2005 till:06/01/2007 text:`[`阿肯色震圈`](../Page/阿肯色震圈.md "wikilink")` (2005–07)`

` bar:11 color:powderblue from:07/01/2006 till:06/01/2009 text:`[`阿納海姆兵工廠`](../Page/阿納海姆兵工廠.md "wikilink")` (2006–09)`
` bar:11 color:powderblue from:07/01/2009 till:06/01/2014 text:`[`斯普林菲爾德鎧甲`](../Page/斯普林菲爾德鎧甲.md "wikilink")` (2009–14)`
` bar:11 color:tan1 from:07/01/2014 till:end text:`[`大急流城驅動`](../Page/大急流城驅動.md "wikilink")` (2014–至今)`

` bar:12 color:powderblue from:07/01/2006 till:06/01/2010 text:`[`洛杉磯防禦者`](../Page/洛杉磯防禦者.md "wikilink")` (2006–10, 2011–2017)`
` bar:12 color:powderblue from:07/09/2011 till:06/01/2017`
` bar:12 color:tan1 from:07/01/2017 till:end text:`[`南灣湖人`](../Page/南灣湖人.md "wikilink")` (2017–至今)`

` bar:13 color:powderblue from:07/01/2006 till:06/01/2016 text:`[`貝克斯菲爾德果醬`](../Page/貝克斯菲爾德果醬.md "wikilink")` (2006–16)`
` bar:13 color:tan1 from:07/01/2016 till:end text:`[`北亞利桑那太陽`](../Page/北亞利桑那太陽.md "wikilink")` (2016–至今)`

` bar:14 color:powderblue from:07/01/2006 till:06/01/2009 text:`[`科羅拉多14人`](../Page/科羅拉多14人.md "wikilink")` (2006–09)`
` bar:14 color:tan1 from:07/01/2010 till:end text:`[`德克薩斯傳奇`](../Page/德克薩斯傳奇.md "wikilink")` (2010–至今)`

` bar:15 color:powderblue from:07/01/2006 till:06/01/2012 text:`[`達科他巫師`](../Page/達科他巫師.md "wikilink")` (2006–12)`
` bar:15 color:tan1 from:07/01/2012 till:end text:`[`聖克魯茲勇士`](../Page/聖克魯茲勇士.md "wikilink")` (2012–至今)`

` bar:16 color:powderblue from:07/01/2006 till:06/01/2016 text:`[`愛達華奔騰`](../Page/愛達華奔騰.md "wikilink")` (2006–16)`
` bar:16 color:tan1 from:07/01/2016 till:end text:`[`鹽湖城明星`](../Page/鹽湖城明星.md "wikilink")` (2016–至今)`

` bar:17 color:tan1 from:07/01/2006 till:end text:`[`蘇瀑天空力量`](../Page/蘇瀑天空力量.md "wikilink")` (2006–至今)`

` bar:18 color:tan1 from:07/01/2007 till:end text:`[`韋恩堡瘋蟻`](../Page/韋恩堡瘋蟻.md "wikilink")` (2007–至今)`

` bar:19 color:powderblue from:07/01/2007 till:06/01/2017 text:`[`愛荷華能量`](../Page/愛荷華能量.md "wikilink")` (2007–2017)`
` bar:19 color:tan1 from:07/01/2017 till:end text:`[`愛荷華灰狼`](../Page/愛荷華灰狼.md "wikilink")` (2017–至今)`

` bar:20 color:tan1 from:07/01/2007 till:end text:`[`里奧格蘭特德山谷毒蛇`](../Page/里奧格蘭特德山谷毒蛇.md "wikilink")` (2007–至今)`

` bar:21 color:powderblue from:07/01/2007 till:06/18/2011 text:`[`猶他閃電`](../Page/猶他閃電.md "wikilink")` (2007–11)`
` bar:21 color:powderblue from:07/01/2013 till:06/01/2018 text:`[`德拉瓦87人`](../Page/德拉瓦87人.md "wikilink")` (2013–2018)`
` bar:21 color:tan1 from:07/01/2018 till:end text:`[`德拉瓦藍衣`](../Page/德拉瓦藍衣.md "wikilink")` (2018–至今)`

` bar:22 color:powderblue from:07/01/2008 till:06/01/2017 text:`[`伊利海鷹`](../Page/Erie_BayHawks_\(2008–2017\).md "wikilink")` (2008–2017)`
` bar:22 color:tan1 from:07/01/2017 till:end text:`[`萊克蘭魔術`](../Page/萊克蘭魔術.md "wikilink")` (2017–至今)`

` bar:23 color:powderblue from:07/01/2008 till:06/01/2018 text:`[`雷諾大角羊`](../Page/雷諾大角羊.md "wikilink")` (2008–2018)`
` bar:23 color:tan1 from:07/01/2018 till:end text:`[`史塔克頓國王`](../Page/史塔克頓國王.md "wikilink")` (2018–至今)`

` bar:24 color:tan1 from:07/01/2009 till:end text:`[`緬因紅爪`](../Page/緬因紅爪.md "wikilink")` (2009–至今)`

` bar:25 color:tan1 from:07/01/2014 till:end text:`[`威斯特徹斯特尼克`](../Page/威斯特徹斯特尼克.md "wikilink")` (2014–至今)`

` bar:26 color:tan1 from:06/29/2015 till:end text:`[`暴龍905`](../Page/暴龍905.md "wikilink")` (2015–至今)`

` bar:27 color:tan1 from:07/01/2016 till:end text: `[`格林斯博羅蜂群`](../Page/格林斯博羅蜂群.md "wikilink")` (2016–至今)`

` bar:28 color:tan1 from:07/01/2016 till:end text:`[`長島籃網`](../Page/長島籃網.md "wikilink")` (2016–至今)`

` bar:29 color:tan1 from:07/01/2016 till:end text:`[`風城公牛`](../Page/風城公牛.md "wikilink")` (2016–至今)`

` bar:30 color:tan1 from:07/01/2017 till:end shift:0 text:`[`阿瓜卡連特快艇`](../Page/阿瓜卡連特快艇.md "wikilink")` (2017–至今)`

` bar:31 color:tan1 from:07/01/2017 till:06/01/2019 shift:-30 text:`[`伊利海鷹`](../Page/伊利海鷹.md "wikilink")` (2017–19)`
` bar:31 color:limegreen from:07/01/2019 till:end shift:0 text:`[`College``
 ``Park`](../Page/College_Park_NBA_Development_League_team.md "wikilink")` (2019)`

` bar:32 color:tan1 from:07/01/2017 till:end shift:0 text:`[`曼非斯疾行`](../Page/曼非斯疾行.md "wikilink")` (2017–至今)`

` bar:33 color:tan1 from:07/01/2017 till:end shift:0 text:`[`威斯康辛鹿群`](../Page/威斯康辛鹿群.md "wikilink")` (2017–至今)`

` bar:34 color:tan1 from:07/01/2018 till:end shift:0 text:`[`首都Go-Go`](../Page/首都Go-Go.md "wikilink")` (2018–至今)`

ScaleMajor = gridcolor:line unit:year increment:1 start:2002

</timeline>

## 聯盟榮譽

### 歷屆總冠軍

| 年份   | 總冠軍                                          |
| ---- | -------------------------------------------- |
| 2002 | [格林維爾律動](../Page/格林維爾律動.md "wikilink")       |
| 2003 | [莫爾比狂歡者](../Page/莫爾比狂歡者.md "wikilink")       |
| 2004 | [阿什維爾高地](../Page/阿什維爾高地.md "wikilink")       |
| 2005 | [阿什維爾高地](../Page/阿什維爾高地.md "wikilink")       |
| 2006 | [阿爾伯克基雷鳥](../Page/阿爾伯克基雷鳥.md "wikilink")     |
| 2007 | [達科他巫師](../Page/達科他巫師.md "wikilink")         |
| 2008 | [愛達荷奔騰](../Page/愛達荷奔騰.md "wikilink")         |
| 2009 | [科羅拉多14人](../Page/科羅拉多14人.md "wikilink")     |
| 2010 | [里奧格蘭德山谷毒蛇](../Page/里奧格蘭德山谷毒蛇.md "wikilink") |
| 2011 | [愛荷華能量](../Page/愛荷華能量.md "wikilink")         |
| 2012 | [奧斯汀公牛](../Page/奧斯汀公牛.md "wikilink")         |
| 2013 | [里奧格蘭德山谷毒蛇](../Page/里奧格蘭德山谷毒蛇.md "wikilink") |
| 2014 | [韋恩堡瘋蟻](../Page/韋恩堡瘋蟻.md "wikilink")         |
| 2015 | [聖克魯斯勇士](../Page/聖克魯斯勇士.md "wikilink")       |
| 2016 | [蘇瀑天空力量](../Page/蘇瀑天空力量.md "wikilink")       |
| 2017 | [暴龍905](../Page/暴龍905.md "wikilink")         |
| 2017 | [奧斯汀馬刺](../Page/奧斯汀馬刺.md "wikilink")         |

### 獎項

  - [最有價值球員](../Page/NBA發展聯盟最有價值球員.md "wikilink")
  - [總決賽MVP](../Page/NBA發展聯盟總決賽最有價值球員.md "wikilink")
  - [全明星賽MVP](../Page/NBA發展聯盟全明星賽最有價值球員.md "wikilink")
  - [年度最佳防守球员](../Page/NBA發展聯盟年度最佳防守球员.md "wikilink")
  - [年度最具影響力球員](../Page/NBA發展聯盟年度最具影響力球員.md "wikilink")
  - [最佳進步球員](../Page/NBA發展聯盟最佳進步球員.md "wikilink")
  - [年度最佳新秀](../Page/NBA發展聯盟年度最佳新秀.md "wikilink")
  - [年度最佳教練](../Page/丹尼斯·強森年度最佳教練獎.md "wikilink")
  - [最佳運動精神獎](../Page/杰森·科勒尔最佳運動精神獎.md "wikilink")
  - [年度最佳球隊行政人員](../Page/NBA發展聯盟年度最佳球隊行政人員.md "wikilink")
  - [年度最佳籃球行政人員](../Page/NBA發展聯盟年度最佳籃球行政人員.md "wikilink")
  - [NBA發展聯盟最佳陣容](../Page/NBA發展聯盟最佳陣容.md "wikilink")
  - [最佳防守陣容](../Page/NBA發展聯盟最佳防守陣容.md "wikilink")
  - [最佳新秀陣容](../Page/NBA發展聯盟最佳新秀陣容.md "wikilink")
  - [發展聯盟總冠軍](../Page/NBA發展聯盟總冠軍.md "wikilink")

#### 国家篮球协会发展联盟历届最有价值球员

  - 2001-2002 [安苏·萨塞](../Page/安苏·萨塞.md "wikilink") ，格林维尔最佳队
  - 2002-2003 [迪文·布朗](../Page/迪文·布朗.md "wikilink")\[31\] ，费耶特维尔爱国者队
  - 2003-2004 [提雷·布朗](../Page/提雷·布朗.md "wikilink") ，北查尔斯顿队
  - 2004-2005 [马特·卡洛](../Page/马特·卡洛.md "wikilink")\[32\] ，罗诺克炫耀队
  - 2005-2006 [马库斯·费泽尔](../Page/马库斯·费泽尔.md "wikilink")\[33\] ，奥斯汀公牛队
  - 2006-2007 [兰迪·利文斯顿](../Page/兰迪·利文斯顿.md "wikilink") ，爱达荷奔腾队

## 注釋

<div class="references-small">

<references />

</div>

## 外部链接

  - [官方網頁（英語）](http://www.nbadleague.com/)

[NBA_G联盟](../Category/NBA_G联盟.md "wikilink")
[Category:籃球聯盟](../Category/籃球聯盟.md "wikilink")
[Category:2001年美國建立](../Category/2001年美國建立.md "wikilink")

1.

2.

3.
4.

5.

6.

7.

8.

9.

10.

11.

12.

13.

14.

15.
16.
17.
18.

19.

20.  此等譯名根據NBA中國官方網站的相關球員和教練名稱，參考於UTC時間2006年8月13日19:35。

21.
22.
23.
24.

25.
    [NBA全球社区公益活动「NBA关怀行动」展开](http://china.nba.com/20051019/n227248385.shtml)
    一文中Commisioner譯作主席。

26.

27.

28.
29.
30.

31.
32.
33.