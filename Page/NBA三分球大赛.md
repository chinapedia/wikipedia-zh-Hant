**NBA三分球大赛**（）是[NBA全明星周末中的一项带有表演性质的比赛](../Page/NBA全明星周末.md "wikilink")，通常是在[NBA全明星赛前的周六举办](../Page/NBA全明星赛.md "wikilink")，每年有六名球员入选此赛事，每人在五個不同位置各投五顆[三分球](../Page/三分球.md "wikilink")，分别是左侧底线、左侧45度角、弧顶、右侧45度角、右侧底线處出手，共計25球。其中每组位置需投五颗球，前四球若命中則为一分，最后一个花色球（Money
Ball）若命中則為兩分，所以總分為30分。从[2014年NBA全明星赛起](../Page/2014年NBA全明星赛.md "wikilink")，參賽球員可要求大會把一組五顆花色球放在其認爲最有信心的位置，而總分亦順勢變爲34分。前三名进入复赛，预赛成绩不带入复赛計算，复赛中的胜出者为当届的三分球冠军。

## 三分球大賽歷屆冠軍

[Dirkn.jpg](https://zh.wikipedia.org/wiki/File:Dirkn.jpg "fig:Dirkn.jpg")贏得[2006年的三分球大賽](../Page/2005-06_NBA賽季.md "wikilink")\]\]
[Jason_Kapono.JPG](https://zh.wikipedia.org/wiki/File:Jason_Kapono.JPG "fig:Jason_Kapono.JPG")在[2008年所屬](../Page/2007-08_NBA賽季.md "wikilink")[多倫多暴龍隊時](../Page/多倫多暴龍隊.md "wikilink")，贏得三分球大賽冠軍。\]\]

|         |                                                     |
| ------- | --------------------------------------------------- |
| ^       | 以此顏色標示者表示為現役球員                                      |
| \*      | 以此顏色標示者表示為入選[籃球名人堂之球員](../Page/籃球名人堂.md "wikilink") |
| 球員名（\#） | 表示該球員在三分球大賽的累積冠軍數                                   |
| 球隊名（\#） | 表示該球隊所屬的球員在三分球大賽的累積冠軍數                              |

<table>
<thead>
<tr class="header">
<th><p>球季</p></th>
<th><p>奪冠球員</p></th>
<th><p>所屬隊伍</p></th>
<th><p>決賽分數</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1985-86_NBA賽季.md" title="wikilink">1986</a></p></td>
<td><p><a href="../Page/拉里·伯德.md" title="wikilink">拉里·伯德</a>*</p></td>
<td><p><a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1986-87_NBA賽季.md" title="wikilink">1987</a></p></td>
<td><p><a href="../Page/拉里·伯德.md" title="wikilink">拉里·伯德</a>*（2）</p></td>
<td><p><a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>（2）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1987-88_NBA賽季.md" title="wikilink">1988</a></p></td>
<td><p><a href="../Page/拉里·伯德.md" title="wikilink">拉里·伯德</a>*（3）</p></td>
<td><p><a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>（3）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1988-89_NBA賽季.md" title="wikilink">1989</a></p></td>
<td><p><a href="../Page/达勒·艾利斯.md" title="wikilink">达勒·艾利斯</a></p></td>
<td><p><a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1989-90_NBA賽季.md" title="wikilink">1990</a></p></td>
<td><p><a href="../Page/克莱格·霍奇斯.md" title="wikilink">克雷格·霍奇斯</a></p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1990-91_NBA賽季.md" title="wikilink">1991</a></p></td>
<td><p><a href="../Page/克莱格·霍奇斯.md" title="wikilink">克雷格·霍奇斯</a>（2）</p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a>（2）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1991-92_NBA賽季.md" title="wikilink">1992</a></p></td>
<td><p><a href="../Page/克莱格·霍奇斯.md" title="wikilink">克雷格·霍奇斯</a>（3）</p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a>（3）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1992-93_NBA賽季.md" title="wikilink">1993</a></p></td>
<td><p><a href="../Page/馬克·普萊斯.md" title="wikilink">馬克·普萊斯</a></p></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1993-94_NBA賽季.md" title="wikilink">1994</a></p></td>
<td><p><a href="../Page/馬克·普萊斯.md" title="wikilink">馬克·普萊斯</a>（2）</p></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a>（2）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1994-95_NBA賽季.md" title="wikilink">1995</a></p></td>
<td><p><a href="../Page/格倫·萊斯.md" title="wikilink">格倫·萊斯</a></p></td>
<td><p><a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1995-96_NBA賽季.md" title="wikilink">1996</a></p></td>
<td><p><a href="../Page/提姆·雷格勒.md" title="wikilink">提姆·雷格勒</a></p></td>
<td><p><a href="../Page/華盛頓子彈.md" title="wikilink">華盛頓子彈</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1996-97_NBA賽季.md" title="wikilink">1997</a></p></td>
<td><p><a href="../Page/史提夫·科爾.md" title="wikilink">史提夫·科爾</a></p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a>（4）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1997-98_NBA賽季.md" title="wikilink">1998</a></p></td>
<td><p><a href="../Page/傑夫·霍納塞克.md" title="wikilink">傑夫·霍納塞克</a></p></td>
<td><p><a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1998-99_NBA賽季.md" title="wikilink">1999</a></p></td>
<td><p>因<a href="../Page/1998–99_NBA封館.md" title="wikilink">1998–99 NBA封館而</a><strong>取消</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1999-00_NBA賽季.md" title="wikilink">2000</a></p></td>
<td><p><a href="../Page/傑夫·霍納塞克.md" title="wikilink">傑夫·霍納塞克</a>（2）</p></td>
<td><p><a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a>（2）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2000-01_NBA賽季.md" title="wikilink">2001</a></p></td>
<td><p><a href="../Page/雷·阿倫.md" title="wikilink">雷·阿倫</a></p></td>
<td><p><a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2001-02_NBA賽季.md" title="wikilink">2002</a></p></td>
<td><p><a href="../Page/佩賈·斯托贾科维奇.md" title="wikilink">佩賈·斯托贾科维奇</a></p></td>
<td><p><a href="../Page/沙加緬度國王隊.md" title="wikilink">沙加緬度國王隊</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2002-03_NBA賽季.md" title="wikilink">2003</a></p></td>
<td><p><a href="../Page/佩賈·斯托贾科维奇.md" title="wikilink">佩賈·斯托贾科维奇</a>（2）</p></td>
<td><p><a href="../Page/沙加緬度國王隊.md" title="wikilink">沙加緬度國王隊</a>（2）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2003-04_NBA賽季.md" title="wikilink">2004</a></p></td>
<td><p><a href="../Page/沃雄·雷納德.md" title="wikilink">沃雄·雷納德</a></p></td>
<td><p><a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2004-05_NBA賽季.md" title="wikilink">2005</a></p></td>
<td><p><a href="../Page/昆汀·理察德森.md" title="wikilink">昆汀·理察德森</a></p></td>
<td><p><a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2005-06_NBA賽季.md" title="wikilink">2006</a></p></td>
<td><p><a href="../Page/德克·諾威斯基.md" title="wikilink">德克·諾威斯基</a>^</p></td>
<td><p><a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2006-07_NBA賽季.md" title="wikilink">2007</a></p></td>
<td><p><a href="../Page/傑森·卡波諾.md" title="wikilink">傑森·卡波諾</a></p></td>
<td><p><a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>（2）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2007-08_NBA賽季.md" title="wikilink">2008</a></p></td>
<td><p><a href="../Page/傑森·卡波諾.md" title="wikilink">傑森·卡波諾</a>（2）</p></td>
<td><p><a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2008-09_NBA賽季.md" title="wikilink">2009</a></p></td>
<td><p><a href="../Page/戴奎恩·庫克.md" title="wikilink">戴奎恩·庫克</a></p></td>
<td><p><a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>（3）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2009-10_NBA賽季.md" title="wikilink">2010</a></p></td>
<td><p><a href="../Page/保羅·皮爾斯.md" title="wikilink">保羅·皮爾斯</a>^</p></td>
<td><p><a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>（4）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2010-11_NBA賽季.md" title="wikilink">2011</a></p></td>
<td><p><a href="../Page/詹姆斯·瓊斯.md" title="wikilink">詹姆斯·瓊斯</a>^</p></td>
<td><p><a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>（4）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2011-12_NBA賽季.md" title="wikilink">2012</a></p></td>
<td><p><a href="../Page/凱文·乐福.md" title="wikilink">凱文·乐福</a>^</p></td>
<td><p><a href="../Page/明尼蘇達灰狼.md" title="wikilink">明尼蘇達灰狼</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2012-13_NBA賽季.md" title="wikilink">2013</a></p></td>
<td><p><a href="../Page/凯里·欧文.md" title="wikilink">凯里·欧文</a>^</p></td>
<td><p><a href="../Page/克里夫兰骑士.md" title="wikilink">克里夫兰骑士</a>（3）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2013-14_NBA賽季.md" title="wikilink">2014</a></p></td>
<td><p><a href="../Page/馬科·貝里內利.md" title="wikilink">馬可·貝里納利</a>^</p></td>
<td><p><a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2014-15_NBA賽季.md" title="wikilink">2015</a></p></td>
<td><p><a href="../Page/斯蒂芬·库里.md" title="wikilink">斯蒂芬·库里</a>^</p></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2015-16_NBA賽季.md" title="wikilink">2016</a></p></td>
<td><p><a href="../Page/克雷·湯普森.md" title="wikilink">克雷·湯普森</a>^</p></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a>（2）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2016-17_NBA賽季.md" title="wikilink">2017</a></p></td>
<td><p><a href="../Page/埃里克·戈登.md" title="wikilink">埃里克·戈登</a>^</p></td>
<td><p><a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2017-18_NBA賽季.md" title="wikilink">2018</a></p></td>
<td><p><a href="../Page/德文·布克.md" title="wikilink">德文·布克</a>^</p></td>
<td><p><a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2018-19_NBA賽季.md" title="wikilink">2019</a></p></td>
<td><p><a href="../Page/喬·哈里斯.md" title="wikilink">喬·哈里斯</a>^</p></td>
<td><p><a href="../Page/布魯克林籃網.md" title="wikilink">布魯克林籃網</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2019-20_NBA賽季.md" title="wikilink">2020</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>



## 记录与轶闻

2015年開始三分球大賽由舊制的30分滿分調整為34分滿分，其中由選手自選一個點的5顆球都改為花球(花球若投進均可得兩分)，[斯蒂芬·库里](../Page/斯蒂芬·库里.md "wikilink")2015年&[克雷·湯普森](../Page/克雷·湯普森.md "wikilink")2016年所創下的27分紀錄若用舊制計算則為23分。

[2018年](../Page/2018年NBA全明星賽.md "wikilink")[德文·布克以](../Page/德文·布克.md "wikilink")34分總分內奪得28分締造历史最佳成绩(PS:若以舊制來看，分數為25分)，而舊制的最佳成績則由[傑森·卡波諾於](../Page/傑森·卡波諾.md "wikilink")2008年的比賽中，以30分總分內得25分奪得，他當年同時蟬聯了這項比賽的冠軍。

[迈克尔·乔丹曾经尝试挑战](../Page/迈克尔·乔丹.md "wikilink")1990年的三分球大赛，第一轮只得5分，不但被刷掉，同時创下历史最低。

1986年三分球大賽，完全是大鳥柏德的表演時間。賽前笑問各路射手誰準備拿第二名；比賽時游刃有餘到試著打板進球；賽後收下1萬美元支票時更笑說這支票上幾個星期前就寫上他的名字。

## 参见

  - [国家篮球协会](../Page/国家篮球协会.md "wikilink")（NBA）
  - [国际篮球联合会](../Page/国际篮球联合会.md "wikilink")（FIBA）

## 參考資料

[Category:NBA](../Category/NBA.md "wikilink")
[Category:NBA全明星赛](../Category/NBA全明星赛.md "wikilink")