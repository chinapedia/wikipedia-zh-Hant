**NBA全明星赛最有價值球員**（），通常簡稱為「**All-Star Game
MVP**」，是由[美國](../Page/美國.md "wikilink")[國家籃球協會頒發給每年於](../Page/國家籃球協會.md "wikilink")[NBA全明星赛中表現最優秀的球員的獎項](../Page/NBA全明星赛.md "wikilink")。该奖项是由一组媒体成员在[全明星赛结束后投票选出的](../Page/NBA全明星赛.md "wikilink")，得票最高的球员或者是并列得票最高的两位球员将会获奖。.\[1\]

从1953年起NBA官方决定为每年的[NBA全明星赛选出一位](../Page/NBA全明星赛.md "wikilink")[最有价值球员](../Page/最有价值球员.md "wikilink")（MVP），于是设立了这个奖项。而NBA亦为1953年之前两年的[NBA全明星赛补选出了](../Page/NBA全明星赛.md "wikilink")[最有价值球员](../Page/最有价值球员.md "wikilink")，[愛德·麥考利和](../Page/愛德·麥考利.md "wikilink")[保羅·阿里金分别被选为](../Page/保羅·阿里金.md "wikilink")[1951年和](../Page/1951年NBA全明星赛.md "wikilink")[1952年的全明星赛MVP](../Page/1952年NBA全明星赛.md "wikilink")。\[2\]

## 历史纪录

  - [鲍勃·佩蒂特与](../Page/鲍勃·佩蒂特.md "wikilink")[科比·布莱恩特分別四次当选全明星赛MVP](../Page/科比·布莱恩特.md "wikilink")，是[NBA史上获得该奖项最多的球员](../Page/NBA.md "wikilink")。[勒布朗·詹姆斯](../Page/勒布朗·詹姆斯.md "wikilink")、[奥斯卡·罗伯森](../Page/奥斯卡·罗伯森.md "wikilink")、[迈克尔·乔丹和](../Page/迈克尔·乔丹.md "wikilink")[沙奎尔·奥尼尔都是三次当选全明星赛MVP](../Page/沙奎尔·奥尼尔.md "wikilink")；[鲍勃·库西](../Page/鲍勃·库西.md "wikilink")、[朱利叶斯·欧文](../Page/朱利叶斯·欧文.md "wikilink")、[伊塞亚·托马斯](../Page/伊塞亚·托马斯.md "wikilink")、[魔术师约翰逊](../Page/魔术师约翰逊.md "wikilink")、[卡尔·马龙](../Page/卡尔·马龙.md "wikilink")、[阿伦·艾弗森和](../Page/阿伦·艾弗森.md "wikilink")[拉塞爾·威斯布魯克则两次获得该奖项](../Page/拉塞爾·威斯布魯克.md "wikilink")。

<!-- end list -->

  - [NBA史上总共出现过四次共享全明星赛MVP的情况](../Page/NBA.md "wikilink")——[1959年的](../Page/1959年NBA全明星赛.md "wikilink")[埃尔金·贝勒与](../Page/埃尔金·贝勒.md "wikilink")[鲍勃·佩蒂特](../Page/鲍勃·佩蒂特.md "wikilink")；[1993年的](../Page/1993年NBA全明星赛.md "wikilink")[约翰·斯托克顿和](../Page/约翰·斯托克顿.md "wikilink")[卡尔·马龙](../Page/卡尔·马龙.md "wikilink")；[2000年的](../Page/2000年NBA全明星赛.md "wikilink")[沙奎尔·奥尼尔和](../Page/沙奎尔·奥尼尔.md "wikilink")[蒂姆·邓肯](../Page/蒂姆·邓肯.md "wikilink")；[2009年的](../Page/2009年NBA全明星赛.md "wikilink")[科比·布莱恩特和](../Page/科比·布莱恩特.md "wikilink")[沙奎尔·奥尼尔](../Page/沙奎尔·奥尼尔.md "wikilink")（[沙奎尔·奥尼尔亦成为](../Page/沙奎尔·奥尼尔.md "wikilink")[NBA史上首位两次获得共享MVP的球员](../Page/NBA.md "wikilink")）。

<!-- end list -->

  - [勒布朗·詹姆斯在](../Page/勒布朗·詹姆斯.md "wikilink")[2006年获得的第一座全明星赛最有价值球员奖杯使他成为了历史上最年轻的全明星赛MVP](../Page/2006年NBA全明星赛.md "wikilink")，那年他只有21岁。

<!-- end list -->

  - [NBA史上只有两位球员成功蝉联NBA全明星赛MVP](../Page/NBA.md "wikilink")，他们分別是[鲍勃·佩蒂特和](../Page/鲍勃·佩蒂特.md "wikilink")[拉塞爾·威斯布魯克](../Page/拉塞爾·威斯布魯克.md "wikilink")。

## 历届获奖者

[Bob_Pettit_1961.jpeg](https://zh.wikipedia.org/wiki/File:Bob_Pettit_1961.jpeg "fig:Bob_Pettit_1961.jpeg")职业生涯4次当选NBA全明星赛MVP，他亦是20世纪唯一成功蝉联该奖项的球員\]\]
[Oscar_Robertson_1960s.jpeg](https://zh.wikipedia.org/wiki/File:Oscar_Robertson_1960s.jpeg "fig:Oscar_Robertson_1960s.jpeg")职业生涯3次当选NBA全明星赛MVP\]\]
[Jordan_by_Lipofsky_16577.jpg](https://zh.wikipedia.org/wiki/File:Jordan_by_Lipofsky_16577.jpg "fig:Jordan_by_Lipofsky_16577.jpg")职业生涯3次当选NBA全明星赛MVP\]\]
[Lipofsky_Shaquille_O'Neal.jpg](https://zh.wikipedia.org/wiki/File:Lipofsky_Shaquille_O'Neal.jpg "fig:Lipofsky_Shaquille_O'Neal.jpg")职业生涯3次当选NBA全明星赛MVP\]\]
[Kobe_Bryant_Drives2.jpg](https://zh.wikipedia.org/wiki/File:Kobe_Bryant_Drives2.jpg "fig:Kobe_Bryant_Drives2.jpg")职业生涯4次当选NBA全明星赛MVP\]\]
[Lebronred.jpg](https://zh.wikipedia.org/wiki/File:Lebronred.jpg "fig:Lebronred.jpg")的[勒布朗·詹姆斯是NBA史上最年轻的NBA全明星赛MVP](../Page/勒布朗·詹姆斯.md "wikilink")\]\]
[Russell_Westbrook_dribbling_vs_Cavs_(cropped).jpg](https://zh.wikipedia.org/wiki/File:Russell_Westbrook_dribbling_vs_Cavs_\(cropped\).jpg "fig:Russell_Westbrook_dribbling_vs_Cavs_(cropped).jpg")是21世纪首位成功蝉联NBA全明星赛MVP的球員\]\]
[Anthony_Davis_Feb_2014.jpg](https://zh.wikipedia.org/wiki/File:Anthony_Davis_Feb_2014.jpg "fig:Anthony_Davis_Feb_2014.jpg")是2017年全明星賽MVP\]\]

|        |                                                    |
| ------ | -------------------------------------------------- |
| ^      | 现役球员                                               |
| \*     | 入选[奈史密斯篮球名人纪念堂](../Page/奈史密斯篮球名人纪念堂.md "wikilink") |
| 球员 (X) | 该球员获奖次数                                            |
| 球队 (X) | 历史上该队球员获奖次数                                        |
|        |                                                    |

<table>
<thead>
<tr class="header">
<th><p>赛季</p></th>
<th><p>球员</p></th>
<th><p>位置</p></th>
<th><p>国籍</p></th>
<th><p>球队</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1951年NBA全明星赛.md" title="wikilink">1951</a></p></td>
<td><p><a href="../Page/愛德·麥考利.md" title="wikilink">愛德·麥考利</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/波士顿凯尔特人.md" title="wikilink">波士顿凯尔特人</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1952年NBA全明星赛.md" title="wikilink">1952</a></p></td>
<td><p><a href="../Page/保羅·阿里金.md" title="wikilink">保羅·阿里金</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/費城勇士.md" title="wikilink">費城勇士</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1953年NBA全明星赛.md" title="wikilink">1953</a></p></td>
<td><p><a href="../Page/乔治·迈肯.md" title="wikilink">乔治·迈肯</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/明尼亞波利斯湖人.md" title="wikilink">明尼亞波利斯湖人</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1954年NBA全明星赛.md" title="wikilink">1954</a></p></td>
<td><p><a href="../Page/鲍勃·库锡.md" title="wikilink">鲍勃·库锡</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/波士顿凯尔特人.md" title="wikilink">波士顿凯尔特人</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1955年NBA全明星赛.md" title="wikilink">1955</a></p></td>
<td><p><a href="../Page/比爾·夏曼.md" title="wikilink">比爾·夏曼</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/波士顿凯尔特人.md" title="wikilink">波士顿凯尔特人</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1956年NBA全明星赛.md" title="wikilink">1956</a></p></td>
<td><p><a href="../Page/鲍勃·佩蒂特.md" title="wikilink">鲍勃·佩蒂特</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/聖路易老鷹.md" title="wikilink">聖路易老鷹</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1957年NBA全明星赛.md" title="wikilink">1957</a></p></td>
<td><p><a href="../Page/鲍勃·库锡.md" title="wikilink">鲍勃·库锡</a>(2)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/波士顿凯尔特人.md" title="wikilink">波士顿凯尔特人</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1958年NBA全明星赛.md" title="wikilink">1958</a></p></td>
<td><p><a href="../Page/鲍勃·佩蒂特.md" title="wikilink">鲍勃·佩蒂特</a>(2)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/聖路易老鷹.md" title="wikilink">聖路易老鷹</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1959年NBA全明星赛.md" title="wikilink">1959</a></p></td>
<td><p><a href="../Page/埃尔金·贝勒.md" title="wikilink">埃尔金·贝勒</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/明尼亞波利斯湖人.md" title="wikilink">明尼亞波利斯湖人</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鲍勃·佩蒂特.md" title="wikilink">鲍勃·佩蒂特</a>(3)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/聖路易老鷹.md" title="wikilink">聖路易老鷹</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1960年NBA全明星赛.md" title="wikilink">1960</a></p></td>
<td><p><a href="../Page/威尔特·张伯伦.md" title="wikilink">威尔特·张伯伦</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/費城勇士.md" title="wikilink">費城勇士</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1961年NBA全明星赛.md" title="wikilink">1961</a></p></td>
<td><p><a href="../Page/奥斯卡·罗伯森.md" title="wikilink">奥斯卡·罗伯森</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/薩克拉門托國王.md" title="wikilink">辛辛那提皇家</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1962年NBA全明星赛.md" title="wikilink">1962</a></p></td>
<td><p><a href="../Page/鲍勃·佩蒂特.md" title="wikilink">鲍勃·佩蒂特</a>(4)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/聖路易老鷹.md" title="wikilink">聖路易老鷹</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1963年NBA全明星赛.md" title="wikilink">1963</a></p></td>
<td><p><a href="../Page/比尔·拉塞尔.md" title="wikilink">比尔·拉塞尔</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/波士顿凯尔特人.md" title="wikilink">波士顿凯尔特人</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1964年NBA全明星赛.md" title="wikilink">1964</a></p></td>
<td><p><a href="../Page/奥斯卡·罗伯森.md" title="wikilink">奥斯卡·罗伯森</a>(2)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/薩克拉門托國王.md" title="wikilink">辛辛那提皇家</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1965年NBA全明星赛.md" title="wikilink">1965</a></p></td>
<td><p><a href="../Page/傑里·盧卡斯.md" title="wikilink">傑里·盧卡斯</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/薩克拉門托國王.md" title="wikilink">辛辛那提皇家</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1966年NBA全明星赛.md" title="wikilink">1966</a></p></td>
<td><p><a href="../Page/阿德里安·史密斯_(篮球运动员).md" title="wikilink">阿德里安·史密斯</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/薩克拉門托國王.md" title="wikilink">辛辛那提皇家</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1967年NBA全明星赛.md" title="wikilink">1967</a></p></td>
<td><p><a href="../Page/里克·巴里.md" title="wikilink">里克·巴里</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/舊金山勇士.md" title="wikilink">舊金山勇士</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1968年NBA全明星赛.md" title="wikilink">1968</a></p></td>
<td><p><a href="../Page/豪爾·格里爾.md" title="wikilink">豪爾·格里爾</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1969年NBA全明星赛.md" title="wikilink">1969</a></p></td>
<td><p><a href="../Page/奥斯卡·罗伯逊.md" title="wikilink">奥斯卡·罗伯逊</a>(3)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/薩克拉門托國王.md" title="wikilink">辛辛那提皇家</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1970年NBA全明星赛.md" title="wikilink">1970</a></p></td>
<td><p><a href="../Page/威利斯·里德.md" title="wikilink">威利斯·里德</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/纽约尼克斯.md" title="wikilink">纽约尼克斯</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1971年NBA全明星赛.md" title="wikilink">1971</a></p></td>
<td><p><a href="../Page/兰尼·威尔肯斯.md" title="wikilink">兰尼·威尔肯斯</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1972年NBA全明星赛.md" title="wikilink">1972</a></p></td>
<td><p><a href="../Page/杰里·韦斯特.md" title="wikilink">杰里·韦斯特</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1973年NBA全明星赛.md" title="wikilink">1973</a></p></td>
<td><p><a href="../Page/戴夫·考恩斯.md" title="wikilink">戴夫·考恩斯</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/波士顿凯尔特人.md" title="wikilink">波士顿凯尔特人</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1974年NBA全明星赛.md" title="wikilink">1974</a></p></td>
<td><p><a href="../Page/鲍伯·兰尼爾.md" title="wikilink">鲍伯·兰尼爾</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1975年NBA全明星赛.md" title="wikilink">1975</a></p></td>
<td><p><a href="../Page/沃爾特·費雷澤.md" title="wikilink">沃爾特·費雷澤</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/纽约尼克斯.md" title="wikilink">纽约尼克斯</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1976年NBA全明星赛.md" title="wikilink">1976</a></p></td>
<td><p><a href="../Page/戴夫·宾.md" title="wikilink">戴夫·宾</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/華盛頓巫師.md" title="wikilink">華盛頓子彈</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1977年NBA全明星赛.md" title="wikilink">1977</a></p></td>
<td><p><a href="../Page/朱利叶斯·欧文.md" title="wikilink">朱利叶斯·欧文</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1978年NBA全明星赛.md" title="wikilink">1978</a></p></td>
<td><p><a href="../Page/蘭迪·史密斯.md" title="wikilink">蘭迪·史密斯</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/水牛城勇士.md" title="wikilink">水牛城勇士</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1979年NBA全明星赛.md" title="wikilink">1979</a></p></td>
<td><p><a href="../Page/大衛·湯普森.md" title="wikilink">大衛·湯普森</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1980年NBA全明星赛.md" title="wikilink">1980</a></p></td>
<td><p><a href="../Page/乔治·格温.md" title="wikilink">乔治·格温</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1981年NBA全明星赛.md" title="wikilink">1981</a></p></td>
<td><p><a href="../Page/奈特·阿奇博尔德.md" title="wikilink">奈特·阿奇博尔德</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/波士顿凯尔特人.md" title="wikilink">波士顿凯尔特人</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1982年NBA全明星赛.md" title="wikilink">1982</a></p></td>
<td><p><a href="../Page/拉里·伯德.md" title="wikilink">拉里·伯德</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/波士顿凯尔特人.md" title="wikilink">波士顿凯尔特人</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1983年NBA全明星赛.md" title="wikilink">1983</a></p></td>
<td><p><a href="../Page/朱利叶斯·欧文.md" title="wikilink">朱利叶斯·欧文</a>(2)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1984年NBA全明星赛.md" title="wikilink">1984</a></p></td>
<td><p><a href="../Page/伊塞亚·托马斯.md" title="wikilink">伊塞亚·托马斯</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1985年NBA全明星赛.md" title="wikilink">1985</a></p></td>
<td><p><a href="../Page/拉爾夫·桑普森.md" title="wikilink">拉爾夫·桑普森</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/休斯敦火箭.md" title="wikilink">休斯敦火箭</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1986年NBA全明星赛.md" title="wikilink">1986</a></p></td>
<td><p><a href="../Page/伊塞亚·托马斯.md" title="wikilink">伊塞亚·托马斯</a>(2)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/底特律活塞隊.md" title="wikilink">底特律活塞隊</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1987年NBA全明星赛.md" title="wikilink">1987</a></p></td>
<td><p><a href="../Page/湯姆·錢伯斯.md" title="wikilink">湯姆·錢伯斯</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1988年NBA全明星赛.md" title="wikilink">1988</a></p></td>
<td><p><a href="../Page/迈克尔·乔丹.md" title="wikilink">迈克尔·乔丹</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1989年NBA全明星赛.md" title="wikilink">1989</a></p></td>
<td><p><a href="../Page/卡爾·馬龍.md" title="wikilink">卡爾·馬龍</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1990年NBA全明星赛.md" title="wikilink">1990</a></p></td>
<td><p><a href="../Page/魔术师约翰逊.md" title="wikilink">魔术师约翰逊</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1991年NBA全明星赛.md" title="wikilink">1991</a></p></td>
<td><p><a href="../Page/查尔斯·巴克利.md" title="wikilink">查尔斯·巴克利</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1992年NBA全明星赛.md" title="wikilink">1992</a></p></td>
<td><p><a href="../Page/魔术师约翰逊.md" title="wikilink">魔术师约翰逊</a>(2)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1993年NBA全明星赛.md" title="wikilink">1993</a></p></td>
<td><p><a href="../Page/卡爾·馬龍.md" title="wikilink">卡爾·馬龍</a>(2)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/約翰·斯托克頓.md" title="wikilink">約翰·斯托克頓</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1994年NBA全明星赛.md" title="wikilink">1994</a></p></td>
<td><p><a href="../Page/斯科蒂·皮蓬.md" title="wikilink">斯科蒂·皮蓬</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1995年NBA全明星赛.md" title="wikilink">1995</a></p></td>
<td><p><a href="../Page/米奇·里奇蒙德.md" title="wikilink">米奇·里奇蒙德</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/薩克拉門托國王.md" title="wikilink">薩克拉門托國王</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1996年NBA全明星赛.md" title="wikilink">1996</a></p></td>
<td><p><a href="../Page/迈克尔·乔丹.md" title="wikilink">迈克尔·乔丹</a>(2)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1997年NBA全明星赛.md" title="wikilink">1997</a></p></td>
<td><p><a href="../Page/格倫·萊斯.md" title="wikilink">格倫·萊斯</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/夏洛特黃蜂.md" title="wikilink">夏洛特黃蜂</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1998年NBA全明星赛.md" title="wikilink">1998</a></p></td>
<td><p><a href="../Page/迈克尔·乔丹.md" title="wikilink">迈克尔·乔丹</a>(3)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1999年NBA全明星赛.md" title="wikilink">1999</a>（停辦）[3].[4]</p></td>
<td><p>—</p></td>
<td></td>
<td><p>|—</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2000年NBA全明星赛.md" title="wikilink">2000</a></p></td>
<td><p><a href="../Page/沙奎尔·奥尼尔.md" title="wikilink">沙奎尔·奥尼尔</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蒂姆·邓肯.md" title="wikilink">蒂姆·邓肯</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2001年NBA全明星赛.md" title="wikilink">2001</a></p></td>
<td><p><a href="../Page/阿伦·艾弗森.md" title="wikilink">阿伦·艾弗森</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2002年NBA全明星赛.md" title="wikilink">2002</a></p></td>
<td><p><a href="../Page/科比·布莱恩特.md" title="wikilink">科比·布莱恩特</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2003年NBA全明星赛.md" title="wikilink">2003</a></p></td>
<td><p><a href="../Page/凱文·加內特.md" title="wikilink">凱文·加內特</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/明尼蘇達森林狼.md" title="wikilink">明尼蘇達森林狼</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2004年NBA全明星赛.md" title="wikilink">2004</a></p></td>
<td><p><a href="../Page/沙奎尔·奥尼尔.md" title="wikilink">沙奎尔·奥尼尔</a>(2)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2005年NBA全明星赛.md" title="wikilink">2005</a></p></td>
<td><p><a href="../Page/阿伦·艾弗森.md" title="wikilink">阿伦·艾弗森</a>(2)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2006年NBA全明星赛.md" title="wikilink">2006</a></p></td>
<td><p><a href="../Page/勒布朗·詹姆斯.md" title="wikilink">勒布朗·詹姆斯</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2007年NBA全明星赛.md" title="wikilink">2007</a></p></td>
<td><p><a href="../Page/科比·布莱恩特.md" title="wikilink">科比·布莱恩特</a>(2)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2008年NBA全明星赛.md" title="wikilink">2008</a></p></td>
<td><p><a href="../Page/勒布朗·詹姆斯.md" title="wikilink">勒布朗·詹姆斯</a>(2)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2009年NBA全明星赛.md" title="wikilink">2009</a></p></td>
<td><p><a href="../Page/沙奎尔·奥尼尔.md" title="wikilink">沙奎尔·奥尼尔</a>(3)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/菲尼克斯太阳.md" title="wikilink">菲尼克斯太阳</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/科比·布莱恩特.md" title="wikilink">科比·布莱恩特</a>(3)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉矶湖人.md" title="wikilink">洛杉矶湖人</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2010年NBA全明星赛.md" title="wikilink">2010</a></p></td>
<td><p><a href="../Page/德韦恩·韦德.md" title="wikilink">德韦恩·韦德</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/迈阿密热火.md" title="wikilink">迈阿密热火</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2011年NBA全明星赛.md" title="wikilink">2011</a></p></td>
<td><p><a href="../Page/科比·布莱恩特.md" title="wikilink">科比·布莱恩特</a>(4)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2012年NBA全明星赛.md" title="wikilink">2012</a></p></td>
<td><p><a href="../Page/凱文·杜蘭特.md" title="wikilink">凱文·杜蘭特</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/俄克拉何马城雷霆.md" title="wikilink">俄克拉何马城雷霆</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2013年NBA全明星赛.md" title="wikilink">2013</a></p></td>
<td><p><a href="../Page/克里斯·保羅.md" title="wikilink">克里斯·保羅</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2014年NBA全明星赛.md" title="wikilink">2014</a></p></td>
<td><p><a href="../Page/凯里·欧文.md" title="wikilink">凯里·欧文</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2015年NBA全明星赛.md" title="wikilink">2015</a></p></td>
<td><p><a href="../Page/拉塞尔·威斯布鲁克.md" title="wikilink">拉塞尔·威斯布鲁克</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/俄克拉何马城雷霆.md" title="wikilink">俄克拉何马城雷霆</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2016年NBA全明星赛.md" title="wikilink">2016</a></p></td>
<td><p><a href="../Page/拉塞尔·威斯布鲁克.md" title="wikilink">拉塞尔·威斯布鲁克</a>(2)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/俄克拉何马城雷霆.md" title="wikilink">俄克拉何马城雷霆</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2017年NBA全明星赛.md" title="wikilink">2017</a></p></td>
<td><p><a href="../Page/安東尼·戴維斯_(籃球運動員).md" title="wikilink">安東尼·戴維斯</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/紐奧良鵜鶘.md" title="wikilink">紐奧良鵜鶘</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2018年NBA全明星赛.md" title="wikilink">2018</a></p></td>
<td><p><a href="../Page/勒布朗·詹姆斯.md" title="wikilink">勒布朗·詹姆斯</a>(3)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2019年NBA全明星赛.md" title="wikilink">2019</a></p></td>
<td><p><a href="../Page/凱文·杜蘭特.md" title="wikilink">凱文·杜蘭特</a>(2)</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a></p></td>
</tr>
</tbody>
</table>

<div class="references-small">

<references />

</div>

## 參見

  - [NBA最有价值球员](../Page/NBA最有价值球员.md "wikilink")
  - [NBA总决赛最有价值球员](../Page/NBA总决赛最有价值球员.md "wikilink")
  - [NBA全明星赛](../Page/NBA全明星赛.md "wikilink")
  - [NBA](../Page/NBA.md "wikilink")

## 参考资料

  - 全局

<!-- end list -->

  -
<!-- end list -->

  - 特定

[Category:NBA全明星赛](../Category/NBA全明星赛.md "wikilink")
[Category:NBA獎項](../Category/NBA獎項.md "wikilink")
[Category:最有價值球員](../Category/最有價值球員.md "wikilink")

1.
2.
3.  1999年NBA联盟因为劳资纠纷取消了当年的[NBA全明星赛](../Page/NBA全明星赛.md "wikilink")，所以1999年没有颁发该奖项。
4.