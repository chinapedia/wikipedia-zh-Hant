**NBA技巧赛**或譯做**NBA技術挑戰賽**始創於2003年[NBA全明星周末](../Page/NBA全明星周末.md "wikilink")，是一项考验球员基本功的比赛。現是由[塔可鐘冠名贊助](../Page/塔可鐘.md "wikilink")。全明星技巧大赛共有四名球员参赛，比赛分为两轮：球员需进行穿越障碍、传球、定点投篮和上篮，所用时间较快的两位球员进入第二轮以争夺冠军。2015年起賽制改爲共八位球員參賽，采取淘汰賽制，最快完成所有指定動作者爲勝。最後由進入決賽的兩位球員爭奪冠軍。而2016年起在2015年的賽制基礎下再把八位球員分前場\[1\]及後場\[2\]（其中四位為前場球員，四位為後場球員），最後由前場勝者及後場勝者爭奪冠軍。[邁阿密熱火的](../Page/邁阿密熱火.md "wikilink")[德韦恩·韦德和](../Page/德韦恩·韦德.md "wikilink")[波特蘭拓荒者的](../Page/波特蘭拓荒者.md "wikilink")[达米恩·林纳德連續兩年奪標](../Page/达米恩·林纳德.md "wikilink")，唯二球員所創下這個比賽的紀錄。

## 胜出者

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>冠军得主</p></th>
<th><p>球队</p></th>
<th><p>成绩</p></th>
<th><p>举办地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2019</p></td>
<td><p><a href="../Page/傑森·塔圖姆.md" title="wikilink">傑森·塔圖姆</a></p></td>
<td><p><a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a></p></td>
<td><p>—</p></td>
<td><p><a href="../Page/夏洛特_(北卡羅來納州).md" title="wikilink">夏洛特</a></p></td>
</tr>
<tr class="even">
<td><p>2018</p></td>
<td></td>
<td><p><a href="../Page/布魯克林籃網.md" title="wikilink">布魯克林籃網</a></p></td>
<td><p>—</p></td>
<td><p><a href="../Page/洛杉磯.md" title="wikilink">洛杉磯</a></p></td>
</tr>
<tr class="odd">
<td><p>2017</p></td>
<td><p><a href="../Page/克里斯塔普斯·波爾津吉斯.md" title="wikilink">克里斯塔普斯·波爾津吉斯</a></p></td>
<td><p><a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a></p></td>
<td><p>－</p></td>
<td><p><a href="../Page/紐奧良.md" title="wikilink">紐奧良</a></p></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td><p><a href="../Page/卡爾-安東尼·唐斯.md" title="wikilink">卡爾-安東尼·唐斯</a></p></td>
<td><p><a href="../Page/明尼蘇達灰狼.md" title="wikilink">明尼蘇達灰狼</a></p></td>
<td><p>－</p></td>
<td><p><a href="../Page/多倫多.md" title="wikilink">多倫多</a></p></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td><p><a href="../Page/帕特里克·贝弗利.md" title="wikilink">帕特里克·贝弗利</a></p></td>
<td><p><a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a></p></td>
<td><p>－</p></td>
<td><p><a href="../Page/紐約.md" title="wikilink">紐約</a></p></td>
</tr>
<tr class="even">
<td><p>2014</p></td>
<td><p><a href="../Page/达米恩·林纳德.md" title="wikilink">达米恩·林纳德</a><br />
<a href="../Page/特雷·伯克.md" title="wikilink">特雷·伯克</a></p></td>
<td><p><a href="../Page/波特兰开拓者.md" title="wikilink">波特兰开拓者</a><br />
<a href="../Page/犹他爵士.md" title="wikilink">犹他爵士</a></p></td>
<td><p>45.2秒</p></td>
<td><p><a href="../Page/新奥尔良.md" title="wikilink">新奥尔良</a>*</p></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p><a href="../Page/达米恩·林纳德.md" title="wikilink">达米恩·林纳德</a></p></td>
<td><p><a href="../Page/波特兰开拓者.md" title="wikilink">波特兰开拓者</a></p></td>
<td><p>29.8秒</p></td>
<td><p><a href="../Page/休士頓.md" title="wikilink">休士頓</a></p></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p><a href="../Page/東尼·柏加.md" title="wikilink">東尼·柏加</a></p></td>
<td><p><a href="../Page/圣安东尼奥马刺.md" title="wikilink">圣安东尼奥马刺</a></p></td>
<td><p>29.2秒</p></td>
<td><p><a href="../Page/奧蘭多.md" title="wikilink">奧蘭多</a></p></td>
</tr>
<tr class="odd">
<td><p>2011</p></td>
<td><p><a href="../Page/斯蒂芬·科里.md" title="wikilink">斯蒂芬·科里</a></p></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a></p></td>
<td><p>28.2秒</p></td>
<td><p><a href="../Page/洛杉磯.md" title="wikilink">洛杉磯</a></p></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td><p><a href="../Page/史蒂夫·纳什.md" title="wikilink">史蒂夫·纳什</a></p></td>
<td><p><a href="../Page/菲尼克斯太阳.md" title="wikilink">菲尼克斯太阳</a></p></td>
<td><p>29.9秒</p></td>
<td><p><a href="../Page/达拉斯.md" title="wikilink">达拉斯</a></p></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p><a href="../Page/德里克·罗斯.md" title="wikilink">德里克·罗斯</a></p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
<td><p>35.3秒</p></td>
<td><p><a href="../Page/凤凰城.md" title="wikilink">凤凰城</a></p></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p><a href="../Page/德隆·威廉姆斯.md" title="wikilink">德隆·威廉姆斯</a></p></td>
<td><p><a href="../Page/犹他爵士.md" title="wikilink">犹他爵士</a></p></td>
<td><p>25.5秒（记录）</p></td>
<td><p><a href="../Page/新奥尔良.md" title="wikilink">新奥尔良</a></p></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p><a href="../Page/德韦恩·韦德.md" title="wikilink">德韦恩·韦德</a></p></td>
<td><p><a href="../Page/迈阿密热火.md" title="wikilink">迈阿密热火</a></p></td>
<td><p>26.4秒</p></td>
<td><p><a href="../Page/拉斯维加斯.md" title="wikilink">拉斯维加斯</a></p></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p><a href="../Page/德韦恩·韦德.md" title="wikilink">德韦恩·韦德</a></p></td>
<td><p><a href="../Page/迈阿密热火.md" title="wikilink">迈阿密热火</a></p></td>
<td><p>26.1秒</p></td>
<td><p><a href="../Page/休斯顿.md" title="wikilink">休斯顿</a></p></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p><a href="../Page/史蒂夫·纳什.md" title="wikilink">史蒂夫·纳什</a></p></td>
<td><p><a href="../Page/菲尼克斯太阳.md" title="wikilink">菲尼克斯太阳</a></p></td>
<td><p>25.8秒</p></td>
<td><p><a href="../Page/丹佛.md" title="wikilink">丹佛</a></p></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p><a href="../Page/貝倫·戴維斯.md" title="wikilink">貝倫·戴維斯</a></p></td>
<td><p><a href="../Page/新奥尔良黄蜂.md" title="wikilink">新奥尔良黄蜂</a></p></td>
<td><p>31.6秒</p></td>
<td><p><a href="../Page/洛杉矶.md" title="wikilink">洛杉矶</a></p></td>
</tr>
<tr class="odd">
<td><p>2003</p></td>
<td><p><a href="../Page/贾森·基德.md" title="wikilink">贾森·基德</a></p></td>
<td><p><a href="../Page/新泽西网.md" title="wikilink">新泽西网</a></p></td>
<td><p>35.1秒</p></td>
<td><p><a href="../Page/亚特兰大.md" title="wikilink">亚特兰大</a></p></td>
</tr>
</tbody>
</table>

  - 2014年 NBA技巧赛採兩人組

## 参考资料

## 外部連結

  - [NBA全明星周末-NBA技巧赛](https://web.archive.org/web/20061216110704/http://inicia.es/de/allstar/skill.htm)

[Category:NBA](../Category/NBA.md "wikilink")
[Category:NBA全明星赛](../Category/NBA全明星赛.md "wikilink")

1.  即小前鋒、大前鋒及中鋒。
2.  即控球後衛及得分後衛。