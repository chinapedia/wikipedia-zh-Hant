**NBA新秀挑战赛**（Rookie
Challenge）是[NBA全明星周末的其中一場重要表演賽](../Page/NBA全明星周末.md "wikilink")。第一屆舉辦於1994年。歷史上大部分比賽雙方是由應屆新秀隊挑戰上屆新秀隊（二年生隊），一眾教練團會於1月選出當時最佳的9名新秀球員和二年級球員，組成新秀隊和二年生隊，由東西部成績最好的球隊的助理教練分別擔任球隊臨時主教練，有時也會邀請NBA的明星球員擔任球隊的掛名經理以增加話題性。2012年起，比賽改為由兩位NBA退役名宿在優秀的年輕球員中挑選各自屬意的隊員組隊比賽。2015年起比賽改爲美國隊對世界隊。2019年首次使用兩位現役球員[凯里·欧文和](../Page/凯里·欧文.md "wikilink")[德克·诺维茨基作為教練](../Page/德克·诺维茨基.md "wikilink")，且比賽由兩節改為四節。比賽日期多數於[NBA全明星週末的第一天即週五](../Page/NBA全明星週末.md "wikilink")。賽事贊助商為[T-Mobile](../Page/T-Mobile.md "wikilink")。

## 歷史

於1994年首次舉行，當年分為優異隊及轟動隊，全由新秀球員組成。1995年改為白隊及綠隊，依然全由新秀球員組成。而兩隊的教練與全明星賽一樣都是由目前東西岸聯盟成績最好球隊的領隊，但鑒于東西岸聯盟成績最好球隊的領隊需要帶領東西岸球隊參與全明星賽，因此當晚比賽的領隊都是球隊的教練團中其中一位。1996年開始把新秀以球隊的所屬聯盟俱分。直至2000年改由新秀隊對二年生隊至2011年。每場比賽後會選出當場最有價值球員
，2012年由两位退役名宿輪流選人，新秀或者二年生皆可，組成两队進行對抗（2012、2013年为[查尔斯·巴克利和](../Page/查尔斯·巴克利.md "wikilink")[沙奎尔·奥尼尔](../Page/沙奎尔·奥尼尔.md "wikilink")，2014年为[克里斯·韦伯和](../Page/克里斯·韦伯.md "wikilink")[格兰特·希尔](../Page/格兰特·希尔.md "wikilink")）。鑒于進軍NBA的海外球員越來越多，NBA決定於2015年起改爲美國新秀及二年生組成的美國隊和其他國家新秀及二年生組成的世界隊。為求公平，NBA要求兩隊都必須至少選出三個新秀及三個二年生，而且兩隊中必須有四個後衛，四個前場球員及兩個[搖擺人](../Page/搖擺人.md "wikilink")。

## 比賽規則

1.  比賽共分為上下各20分鐘兩個半場（似NCAA美國大學籃球賽一樣規格）。
2.  在上下半場各有一次100秒的長暫停，除此以外，上下半場各存在兩次強制性[商業](../Page/商業.md "wikilink")[廣告插播暫停](../Page/廣告.md "wikilink")，分別在半場的7分鐘與14分鐘的時候強制進行。兩只球隊將自動失去一次100秒長暫停如果他們沒有在14分鐘（新秀隊失去暫停機會）以及7分鐘（2年級失去暫停機會）時用掉各自的暫停。
3.  每支球隊半場各有一次20秒短暫停機會。
4.  球員的犯規行為將被記錄台統計，但是不存在6犯離場的情況。而記錄台將在任何一方累計犯規次數達到第11次的時候通知裁判進入罰球程序，而半場結束前的2分鐘內任何一支隊伍的第二次犯規開始，同樣判罰罰球。
5.  新秀對抗賽中如果在常規時間雙方比分持平，將進行長度為2分鐘的加時賽。
6.  進入到上下半場的最後1分鐘內的投籃命中將導致停鐘。
7.  20分鐘賽前熱身。
8.  MVP獎項會在比賽結束後頒發。

## 歷屆球員陣容

### 2018年

#### 阵容

<table>
<caption><strong>世界隊</strong></caption>
<thead>
<tr class="header">
<th><p>位置</p></th>
<th><p>國籍</p></th>
<th><p>球員</p></th>
<th><p>球隊</p></th>
<th><p>級別</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>C</p></td>
<td></td>
<td><p><a href="../Page/喬爾·恩比德.md" title="wikilink">喬爾·恩比德</a></p></td>
<td><p><a href="../Page/費城七六人.md" title="wikilink">費城七六人</a></p></td>
<td><p>二年級</p></td>
</tr>
<tr class="even">
<td><p>G/F</p></td>
<td></td>
<td><p><a href="../Page/班·西蒙斯.md" title="wikilink">班·西蒙斯</a></p></td>
<td><p><a href="../Page/費城七六人.md" title="wikilink">費城七六人</a></p></td>
<td><p>新秀</p></td>
</tr>
<tr class="odd">
<td><p>F</p></td>
<td></td>
<td><p><a href="../Page/達里奧·薩利奇.md" title="wikilink">達里奧·薩利奇</a></p></td>
<td><p><a href="../Page/費城七六人.md" title="wikilink">費城七六人</a></p></td>
<td><p>二年級</p></td>
</tr>
<tr class="even">
<td><p>G</p></td>
<td></td>
<td><p><a href="../Page/波格丹·博格達諾維奇.md" title="wikilink">波格登·博格達諾維奇</a></p></td>
<td><p><a href="../Page/沙加緬度國王.md" title="wikilink">沙加緬度國王</a></p></td>
<td><p>二年級</p></td>
</tr>
<tr class="odd">
<td><p>G</p></td>
<td></td>
<td><p><a href="../Page/巴迪·希爾德.md" title="wikilink">巴迪·希爾德</a></p></td>
<td><p><a href="../Page/沙加緬度國王.md" title="wikilink">沙加緬度國王</a></p></td>
<td><p>二年級</p></td>
</tr>
<tr class="even">
<td><p>G/F</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/曼菲斯灰熊.md" title="wikilink">曼菲斯灰熊</a></p></td>
<td><p>新秀</p></td>
</tr>
<tr class="odd">
<td><p>F</p></td>
<td></td>
<td><p><a href="../Page/勞里·馬爾卡寧.md" title="wikilink">勞里·馬爾卡寧</a></p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
<td><p>新秀</p></td>
</tr>
<tr class="even">
<td><p>G</p></td>
<td></td>
<td><p><a href="../Page/賈邁爾·穆雷.md" title="wikilink">賈邁爾·莫瑞</a></p></td>
<td><p><a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a></p></td>
<td><p>二年級</p></td>
</tr>
<tr class="odd">
<td><p>G</p></td>
<td></td>
<td><p><a href="../Page/法蘭克·尼利基納.md" title="wikilink">法蘭克·尼利基納</a></p></td>
<td><p><a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a></p></td>
<td><p>新秀</p></td>
</tr>
<tr class="even">
<td><p>F/C</p></td>
<td></td>
<td><p><a href="../Page/多曼塔斯·薩博尼斯.md" title="wikilink">多曼塔斯·薩博尼斯</a></p></td>
<td><p><a href="../Page/印第安那溜馬.md" title="wikilink">印第安那溜馬</a></p></td>
<td><p>二年級</p></td>
</tr>
<tr class="odd">
<td><p><strong>總教練</strong>: 雷克斯·卡拉邁（<a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a>）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<caption><strong>美國隊</strong></caption>
<thead>
<tr class="header">
<th><p>位置</p></th>
<th><p>球员</p></th>
<th><p>球队</p></th>
<th><p>級別</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>G</p></td>
<td><p><a href="../Page/朗佐·鮑爾.md" title="wikilink">朗佐·鮑爾</a></p></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a></p></td>
<td><p>新秀</p></td>
</tr>
<tr class="even">
<td><p>G/F</p></td>
<td><p><a href="../Page/班頓·恩格林.md" title="wikilink">班頓·恩格林</a></p></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a></p></td>
<td><p>二年級</p></td>
</tr>
<tr class="odd">
<td><p>F</p></td>
<td><p><a href="../Page/凱爾·庫茲馬.md" title="wikilink">凱爾·庫茲馬</a></p></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a></p></td>
<td><p>新秀</p></td>
</tr>
<tr class="even">
<td><p>G</p></td>
<td><p><a href="../Page/麥爾坎·波格登.md" title="wikilink">麥爾坎·波格登</a></p></td>
<td><p><a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a></p></td>
<td><p>二年級</p></td>
</tr>
<tr class="odd">
<td><p>G/F</p></td>
<td><p><a href="../Page/賈倫·布朗.md" title="wikilink">賈倫·布朗</a></p></td>
<td><p><a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a></p></td>
<td><p>二年級</p></td>
</tr>
<tr class="even">
<td><p>F</p></td>
<td><p><a href="../Page/傑森·塔圖姆.md" title="wikilink">傑森·塔圖姆</a></p></td>
<td><p><a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a></p></td>
<td><p>新秀</p></td>
</tr>
<tr class="odd">
<td><p>F/C</p></td>
<td></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a></p></td>
<td><p>新秀</p></td>
</tr>
<tr class="even">
<td><p>G</p></td>
<td><p><a href="../Page/克里斯·鄧恩.md" title="wikilink">克里斯·鄧恩</a></p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
<td><p>二年級</p></td>
</tr>
<tr class="odd">
<td><p>G</p></td>
<td><p><a href="../Page/多諾萬·米契爾.md" title="wikilink">多諾萬·米契爾</a></p></td>
<td><p><a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a></p></td>
<td><p>新秀</p></td>
</tr>
<tr class="even">
<td><p>G</p></td>
<td><p><a href="../Page/小丹尼斯·史密斯.md" title="wikilink">小丹尼斯·史密斯</a></p></td>
<td><p><a href="../Page/達拉斯獨行俠.md" title="wikilink">達拉斯獨行俠</a></p></td>
<td><p>新秀</p></td>
</tr>
<tr class="odd">
<td><p><strong>總教練</strong>: 羅伊·羅傑斯（<a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a>）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<section end=2018/>

### 2016年

#### 阵容

| 位置                                                                                     | 球员                                           | 球队                                       | 球龄 |
| -------------------------------------------------------------------------------------- | -------------------------------------------- | ---------------------------------------- | -- |
| SG/PG                                                                                  | [乔丹·克拉克森](../Page/乔丹·克拉克森.md "wikilink")     | [洛杉矶湖人](../Page/洛杉矶湖人.md "wikilink")     | 2  |
| SF/SG                                                                                  | [罗德尼·胡德](../Page/罗德尼·胡德.md "wikilink")       | [犹他爵士](../Page/犹他爵士.md "wikilink")       | 2  |
| SG/PG                                                                                  | [扎克·拉文](../Page/扎克·拉文.md "wikilink")         | [明尼苏达森林狼](../Page/明尼苏达森林狼.md "wikilink") | 2  |
| PF/C                                                                                   | [诺伦斯·诺尔](../Page/诺伦斯·诺尔.md "wikilink")       | [费城76人](../Page/费城76人.md "wikilink")     | 2  |
| C                                                                                      | [贾希尔·奥卡福](../Page/贾希尔·奥卡福.md "wikilink")     | [费城76人](../Page/费城76人.md "wikilink")     | 1  |
| SF/PF                                                                                  | [贾巴里·帕克](../Page/贾巴里·帕克.md "wikilink")       | [密尔沃基雄鹿](../Page/密尔沃基雄鹿.md "wikilink")   | 2  |
| PG                                                                                     | [埃尔弗里德·佩顿](../Page/埃尔弗里德·佩顿.md "wikilink")   | [奥兰多魔术](../Page/奥兰多魔术.md "wikilink")     | 2  |
| PG/SG                                                                                  | [德安杰洛·拉塞尔](../Page/德安杰洛·拉塞尔.md "wikilink")   | [洛杉矶湖人](../Page/洛杉矶湖人.md "wikilink")     | 1  |
| PG/SG                                                                                  | [马库斯·斯马特](../Page/马库斯·斯马特.md "wikilink")     | [波士顿凯尔特人](../Page/波士顿凯尔特人.md "wikilink") | 2  |
| C/PF                                                                                   | [卡尔-安东尼·唐斯](../Page/卡尔-安东尼·唐斯.md "wikilink") | [明尼苏达森林狼](../Page/明尼苏达森林狼.md "wikilink") | 1  |
| SG                                                                                     | [德文·布克](../Page/德文·布克.md "wikilink")         | [菲尼克斯太阳](../Page/菲尼克斯太阳.md "wikilink")   | 1  |
| **主教练**: [拉里·德鲁](../Page/拉里·德鲁.md "wikilink") （[克利夫兰骑士](../Page/克利夫兰骑士.md "wikilink")） |                                              |                                          |    |

**美国队**\[1\]

<table>
<caption><strong>国际队</strong>[2]</caption>
<thead>
<tr class="header">
<th><p>位置</p></th>
<th><p>國家</p></th>
<th><p>姓名</p></th>
<th><p>球隊</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>SG/SF</p></td>
<td></td>
<td><p><a href="../Page/博扬·博格达诺维奇.md" title="wikilink">博扬·博格达诺维奇</a></p></td>
<td><p><a href="../Page/布鲁克林篮网.md" title="wikilink">布鲁克林篮网</a></p></td>
</tr>
<tr class="even">
<td><p>PF/C</p></td>
<td></td>
<td><p><a href="../Page/克林特·卡佩拉.md" title="wikilink">克林特·卡佩拉</a></p></td>
<td><p><a href="../Page/休斯顿火箭.md" title="wikilink">休斯顿火箭</a></p></td>
</tr>
<tr class="odd">
<td><p>SG/SF</p></td>
<td></td>
<td><p><a href="../Page/马里奥·海佐尼亚.md" title="wikilink">马里奥·海佐尼亚</a></p></td>
<td><p><a href="../Page/奥兰多魔术.md" title="wikilink">奥兰多魔术</a></p></td>
</tr>
<tr class="even">
<td><p>C</p></td>
<td></td>
<td><p><a href="../Page/尼古拉·约基奇.md" title="wikilink">尼古拉·约基奇</a></p></td>
<td><p><a href="../Page/丹佛掘金.md" title="wikilink">丹佛掘金</a></p></td>
</tr>
<tr class="odd">
<td><p>PF/SF</p></td>
<td></td>
<td><p><a href="../Page/尼古拉·米罗蒂奇.md" title="wikilink">尼古拉·米罗蒂奇</a></p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
</tr>
<tr class="even">
<td><p>PG</p></td>
<td></td>
<td><p><a href="../Page/伊曼纽尔·穆迪埃.md" title="wikilink">伊曼纽尔·穆迪埃</a></p></td>
<td><p><a href="../Page/丹佛掘金.md" title="wikilink">丹佛掘金</a></p></td>
</tr>
<tr class="odd">
<td><p>PG</p></td>
<td></td>
<td><p><a href="../Page/劳尔·内托.md" title="wikilink">劳尔·内托</a></p></td>
<td><p><a href="../Page/犹他爵士.md" title="wikilink">犹他爵士</a></p></td>
</tr>
<tr class="even">
<td><p>C/PF</p></td>
<td></td>
<td><p><a href="../Page/克里斯塔普斯·波尔津吉斯.md" title="wikilink">克里斯塔普斯·波尔津吉斯</a></p></td>
<td><p><a href="../Page/纽约尼克斯.md" title="wikilink">纽约尼克斯</a></p></td>
</tr>
<tr class="odd">
<td><p>F/C</p></td>
<td></td>
<td><p><a href="../Page/德怀特·鲍威尔.md" title="wikilink">德怀特·鲍威尔</a></p></td>
<td><p><a href="../Page/达拉斯小牛.md" title="wikilink">达拉斯小牛</a></p></td>
</tr>
<tr class="even">
<td><p>SG/SF</p></td>
<td></td>
<td><p><a href="../Page/安德鲁·维金斯.md" title="wikilink">安德鲁·维金斯</a></p></td>
<td><p><a href="../Page/明尼苏达森林狼.md" title="wikilink">明尼苏达森林狼</a></p></td>
</tr>
<tr class="odd">
<td><p>PF/SF</p></td>
<td></td>
<td><p><a href="../Page/特雷·莱尔斯.md" title="wikilink">特雷·莱尔斯</a></p></td>
<td><p><a href="../Page/犹他爵士.md" title="wikilink">犹他爵士</a></p></td>
</tr>
<tr class="even">
<td><p><strong>主教练</strong>: <a href="../Page/埃托·墨西拿.md" title="wikilink">埃托·墨西拿</a> (<a href="../Page/圣安东尼奥马刺.md" title="wikilink">圣安东尼奥马刺</a>)</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

诺伦斯·诺尔因伤病退出比赛。\[3\]
德文·布克替换诺伦斯·诺尔。\[4\]
尼古拉·米罗蒂奇因伤病退出比赛。\[5\]
特雷·莱尔斯替换尼古拉·米罗蒂奇。\[6\]

#### 比赛结果

### 2013年

| 位置  | 球員                                               | 球隊                                     |
| --- | ------------------------------------------------ | -------------------------------------- |
| G   | [達米恩·林納德](../Page/達米恩·林納德.md "wikilink")         | [波特蘭拓荒者](../Page/波特蘭拓荒者.md "wikilink") |
| G   | [凱里·厄文](../Page/凱里·厄文.md "wikilink")             | [克里夫蘭騎士](../Page/克里夫蘭騎士.md "wikilink") |
| F/C | [安德烈·德拉蒙德](../Page/安德烈·德拉蒙德.md "wikilink")       | [底特律活塞](../Page/底特律活塞.md "wikilink")   |
| G/F | [克雷·汤普森](../Page/克雷·汤普森.md "wikilink")           | [金州勇士](../Page/金州勇士.md "wikilink")     |
| F   | [哈里森·巴恩斯](../Page/哈里森·巴恩斯.md "wikilink")         | [金州勇士](../Page/金州勇士.md "wikilink")     |
| F   | [錢德勒·帕森斯](../Page/錢德勒·帕森斯.md "wikilink")         | [休士頓火箭](../Page/休士頓火箭.md "wikilink")   |
| G   | [迪昂·维特斯](../Page/迪昂·维特斯.md "wikilink")           | [克里夫蘭騎士](../Page/克里夫蘭騎士.md "wikilink") |
| G   | [迈克尔·基德-克里斯特](../Page/迈克尔·基德-克里斯特.md "wikilink") | [夏洛特山貓](../Page/夏洛特山貓.md "wikilink")   |
| F   | [泰勒·泽勒](../Page/泰勒·泽勒.md "wikilink")             | [克里夫蘭騎士](../Page/克里夫蘭騎士.md "wikilink") |
| G   | [肯巴·沃克](../Page/肯巴·沃克.md "wikilink")             | [夏洛特山貓](../Page/夏洛特山貓.md "wikilink")   |
| F   | [安德鲁·尼克尔森](../Page/安德鲁·尼克尔森.md "wikilink")       | [奧蘭多魔術](../Page/奧蘭多魔術.md "wikilink")   |

**[歐尼爾隊](../Page/沙奎尔·奥尼尔.md "wikilink")**

| 位置  | 球員                                                 | 球隊                                       |
| --- | -------------------------------------------------- | ---------------------------------------- |
| F   | [安東尼·戴維斯](../Page/安東尼·戴維斯_\(篮球运动员\).md "wikilink") | [紐奧良黃蜂](../Page/紐奧良黃蜂.md "wikilink")     |
| F   | [肯尼斯·法里德](../Page/肯尼斯·法里德.md "wikilink")           | [丹佛掘金](../Page/丹佛掘金.md "wikilink")       |
| F   | [科懷·倫納德](../Page/科懷·倫納德.md "wikilink")             | [聖安東尼奧馬刺](../Page/聖安東尼奧馬刺.md "wikilink") |
| G   | [布拉德利·比尔](../Page/布拉德利·比尔.md "wikilink")           | [華盛頓巫師](../Page/華盛頓巫師.md "wikilink")     |
| G   | [歷奇·盧比奧](../Page/歷奇·盧比奧.md "wikilink")             | [明尼蘇達灰狼](../Page/明尼蘇達灰狼.md "wikilink")   |
| F/C | [特里斯坦·汤普森](../Page/特里斯坦·汤普森.md "wikilink")         | [克里夫蘭騎士](../Page/克里夫蘭騎士.md "wikilink")   |
| C   | [尼古拉·武切维奇](../Page/尼古拉·武切维奇.md "wikilink")         | [奧蘭多魔術](../Page/奧蘭多魔術.md "wikilink")     |
| G   | [以赛亚·托马斯](../Page/以赛亚·托马斯.md "wikilink")           | [沙加緬度國王](../Page/沙加緬度國王.md "wikilink")   |
| G   | [布兰登·奈特](../Page/布兰登·奈特.md "wikilink")             | [底特律活塞](../Page/底特律活塞.md "wikilink")     |
| F   | [阿莱克斯·舍维德](../Page/阿莱克斯·舍维德.md "wikilink")         | [明尼蘇達灰狼](../Page/明尼蘇達灰狼.md "wikilink")   |

**[巴克利隊](../Page/查尔斯·巴克利.md "wikilink")**

### 2012年

| 位置  | 球員                                         | 球隊                                     |
| --- | ------------------------------------------ | -------------------------------------- |
| F   | [布莱克·格里芬](../Page/布莱克·格里芬.md "wikilink")   | [洛杉磯快艇](../Page/洛杉磯快艇.md "wikilink")   |
| G   | [林書豪](../Page/林書豪.md "wikilink")           | [紐約尼克](../Page/紐約尼克.md "wikilink")     |
| G   | [歷奇·盧比奧](../Page/歷奇·盧比奧.md "wikilink")     | [明尼蘇達灰狼](../Page/明尼蘇達灰狼.md "wikilink") |
| F/C | [格雷格·门罗](../Page/格雷格·门罗.md "wikilink")     | [底特律活塞](../Page/底特律活塞.md "wikilink")   |
| F   | [馬基夫·莫里斯](../Page/馬基夫·莫里斯.md "wikilink")   | [鳳凰城太陽](../Page/鳳凰城太陽.md "wikilink")   |
| G   | [肯巴·沃克](../Page/肯巴·沃克.md "wikilink")       | [夏洛特山貓](../Page/夏洛特山貓.md "wikilink")   |
| G   | [蘭德里·費斯](../Page/蘭德里·費斯.md "wikilink")     | [紐約尼克](../Page/紐約尼克.md "wikilink")     |
| G   | [諾里斯·科爾](../Page/諾里斯·科爾.md "wikilink")     | [邁阿密熱火](../Page/邁阿密熱火.md "wikilink")   |
| G   | [布兰登·奈特](../Page/布兰登·奈特.md "wikilink")     | [底特律活塞](../Page/底特律活塞.md "wikilink")   |
| F   | [特里斯坦·汤普森](../Page/特里斯坦·汤普森.md "wikilink") | [克里夫蘭騎士](../Page/克里夫蘭騎士.md "wikilink") |
|     |                                            |                                        |

**[歐尼爾隊](../Page/沙奎尔·奥尼尔.md "wikilink")**

| 位置  | 球員                                         | 球隊                                       |
| --- | ------------------------------------------ | ---------------------------------------- |
| F/C | [卡爾·艾榮](../Page/卡爾·艾榮.md "wikilink")       | [克里夫蘭騎士](../Page/克里夫蘭騎士.md "wikilink")   |
| C   | [德马库斯·考辛斯](../Page/德马库斯·考辛斯.md "wikilink") | [沙加緬度國王](../Page/沙加緬度國王.md "wikilink")   |
| G/F | [保罗·乔治](../Page/保罗·乔治.md "wikilink")       | [印第安納溜馬](../Page/印第安納溜馬.md "wikilink")   |
| F   | [德里克·威廉姆斯](../Page/德里克·威廉姆斯.md "wikilink") | [明尼蘇達灰狼](../Page/明尼蘇達灰狼.md "wikilink")   |
| G/F | [马尚·布鲁克斯](../Page/马尚·布鲁克斯.md "wikilink")   | [布鲁克林篮网](../Page/布鲁克林篮网.md "wikilink")   |
| G   | [約翰·沃爾](../Page/約翰·沃爾.md "wikilink")       | [華盛頓巫師](../Page/華盛頓巫師.md "wikilink")     |
| F   | [戈登·海沃德](../Page/戈登·海沃德.md "wikilink")     | [猶他爵士](../Page/猶他爵士.md "wikilink")       |
| F/C | [蒂亚戈·斯普利特](../Page/蒂亚戈·斯普利特.md "wikilink") | [聖安東尼奧馬刺](../Page/聖安東尼奧馬刺.md "wikilink") |
| F   | [科懷·倫納德](../Page/科懷·倫納德.md "wikilink")     | [聖安東尼奧馬刺](../Page/聖安東尼奧馬刺.md "wikilink") |
| G/F | [埃文·特纳](../Page/埃文·特纳.md "wikilink")       | [費城76人](../Page/費城76人.md "wikilink")     |
| F   | [德里克·费佛斯](../Page/德里克·费佛斯.md "wikilink")   | [猶他爵士](../Page/猶他爵士.md "wikilink")       |

**[巴克利隊](../Page/查尔斯·巴克利.md "wikilink")**

## 歷屆成績

| 年份   | 勝方   | 負方   | 賽果          | 最有價值球員                                                                            |
| ---- | ---- | ---- | ----------- | --------------------------------------------------------------------------------- |
| 2020 |      |      | –           |                                                                                   |
| 2019 | 美國隊  | 世界隊  | 161 – 144   | [凱爾·庫茲馬](../Page/凱爾·庫茲馬.md "wikilink")                                            |
| 2018 | 世界隊  | 美國隊  | 155 – 124   | [波格丹·博格達諾維奇](../Page/波格丹·博格達諾維奇.md "wikilink")                                    |
| 2017 | 世界隊  | 美國隊  | 150 – 141   | [賈邁爾·穆雷](../Page/賈邁爾·穆雷.md "wikilink")                                            |
| 2016 | 美國隊  | 世界隊  | 157 – 154   | [扎克·拉文](../Page/扎克·拉文.md "wikilink")                                              |
| 2015 | 世界隊  | 美國隊  | 121 – 112   | [安德魯·威金斯](../Page/安德魯·威金斯.md "wikilink")                                          |
| 2014 | 希爾隊  | 韋伯隊  | 142 - 136   | [安德烈·德拉蒙德](../Page/安德烈·德拉蒙德.md "wikilink")                                        |
| 2013 | 巴克利隊 | 奧尼爾隊 | 163 - 135   | [肯尼斯·法里德](../Page/肯尼斯·法里德.md "wikilink")                                          |
| 2012 | 巴克利隊 | 奧尼爾隊 | 146 - 133   | [凯里·欧文](../Page/凯里·欧文.md "wikilink")                                              |
| 2011 | 新秀隊  | 二年生隊 | 148 - 140   | [约翰·沃尔](../Page/约翰·沃尔.md "wikilink")                                              |
| 2010 | 新秀隊  | 二年生隊 | 140 - 128   | [泰瑞克·埃文斯](../Page/泰瑞克·埃文斯.md "wikilink")、[德胡安·布莱尔](../Page/德胡安·布莱尔.md "wikilink") |
| 2009 | 二年生隊 | 新秀隊  | 122 - 116   | [凯文·杜兰特](../Page/凯文·杜兰特.md "wikilink")                                            |
| 2008 | 二年生隊 | 新秀隊  | 136 - 109   | [丹尼尔·吉布森](../Page/丹尼尔·吉布森.md "wikilink")                                          |
| 2007 | 二年生隊 | 新秀隊  | 155 - 114   | [大卫·李](../Page/大卫·李.md "wikilink")                                                |
| 2006 | 二年生隊 | 新秀隊  | 106 - 96    | [安德烈·伊格达拉](../Page/安德烈·伊格达拉.md "wikilink")                                        |
| 2005 | 二年生隊 | 新秀隊  | 133 - 106   | [卡梅隆·安東尼](../Page/卡梅隆·安東尼.md "wikilink")                                          |
| 2004 | 二年生隊 | 新秀隊  | 142 - 118   | [阿玛雷·斯塔德迈尔](../Page/阿玛雷·斯塔德迈尔.md "wikilink")                                      |
| 2003 | 二年生隊 | 新秀隊  | 132 - 112   | [吉爾伯特·阿瑞納斯](../Page/吉爾伯特·阿瑞納斯.md "wikilink")                                      |
| 2002 | 新秀隊  | 二年生隊 | 103 - 97    | [賈森·理查德森](../Page/賈森·理查德森.md "wikilink")                                          |
| 2001 | 二年生隊 | 新秀隊  | 121 - 113   | [沃利·瑟比亞克](../Page/沃利·斯澤比亞克.md "wikilink")                                         |
| 2000 | 新秀隊  | 二年生隊 | 92 - 83（加時） | [埃爾頓·布蘭德](../Page/埃爾頓·布蘭德.md "wikilink")                                          |
| 1998 | 東岸隊  | 西岸隊  | 85 - 80     | [扎伊德鲁纳斯·伊尔格斯卡斯](../Page/扎伊德鲁纳斯·伊尔格斯卡斯.md "wikilink")                              |
| 1997 | 東岸隊  | 西岸隊  | 96 - 91     | [艾倫·艾佛森](../Page/艾倫·艾佛森.md "wikilink")                                            |
| 1996 | 東岸隊  | 西岸隊  | 94 - 92     | [戴蒙·斯塔德迈尔](../Page/戴蒙·斯塔德迈尔.md "wikilink")                                        |
| 1995 | 白隊   | 綠隊   | 83 - 79（加時） | [埃迪·瓊斯](../Page/埃迪·瓊斯.md "wikilink")                                              |
| 1994 | 優異隊  | 轟動隊  | 74 - 68     | [安芬尼·哈達威](../Page/安芬尼·哈達威.md "wikilink")                                          |

## 參考資料

## 外部連結

  - [2007 Rookie
    Challenge](http://www.nba.com/allstar2007/rookie_challenge/)
  - [2008 Rookie
    Challenge](http://www.nba.com/allstar2008/rookie_challenge/)
  - [Rookie Challenge Recaps and Box
    Scores](http://www.nba.com/history/allstar/rookie_alltime_results.html)
  - [Rookie Challenge
    Coaches](http://www.nba.com/history/allstar/rookie_alltime_coaches.html)
  - [Team-by-Team
    Participants](http://www.nba.com/history/allstar/rookie_alltime_teambyteam.html)

[Category:NBA](../Category/NBA.md "wikilink")
[Category:NBA全明星赛](../Category/NBA全明星赛.md "wikilink")

1.

2.
3.

4.
5.

6.