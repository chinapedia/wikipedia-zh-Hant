本表列出NBA生涯[抢断排行榜](../Page/抢断.md "wikilink")。(数据截至2018年12月2日)

[John_Stockton_Lipofskydotcom-32245.jpg](https://zh.wikipedia.org/wiki/File:John_Stockton_Lipofskydotcom-32245.jpg "fig:John_Stockton_Lipofskydotcom-32245.jpg")\]\]
[KiddFTline2.jpg](https://zh.wikipedia.org/wiki/File:KiddFTline2.jpg "fig:KiddFTline2.jpg")\]\]
[Jordan_by_Lipofsky_16577.jpg](https://zh.wikipedia.org/wiki/File:Jordan_by_Lipofsky_16577.jpg "fig:Jordan_by_Lipofsky_16577.jpg")\]\]

|    |              |
| -- | ------------ |
| ^  | 现役球员         |
| \* | 入选了奈史密斯篮球名人堂 |

<table>
<thead>
<tr class="header">
<th><p>排名[1]</p></th>
<th><p>球員</p></th>
<th><p>場上位置</p></th>
<th><p>效力球隊及賽季</p></th>
<th><p>總計[2]</p></th>
<th><p>每場平均[3]</p></th>
<th><p>入選名人堂</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/約翰·史托頓.md" title="wikilink">約翰·史托頓</a></p></td>
<td><p><a href="../Page/控球後衛.md" title="wikilink">控球後衛</a></p></td>
<td><p><a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a>（<a href="../Page/1984-85_NBA賽季.md" title="wikilink">1984-85</a>–<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>）</p></td>
<td><p>3,265</p></td>
<td><p>2.2</p></td>
<td><p>2009</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/傑森·基德.md" title="wikilink">傑森·基德</a></p></td>
<td><p><a href="../Page/控球後衛.md" title="wikilink">控球後衛</a></p></td>
<td><p><a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a>（<a href="../Page/1994-95_NBA賽季.md" title="wikilink">1994-95</a>–<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a>）<br />
<a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a>（<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a>–<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>）<br />
<a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a>（<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>–<a href="../Page/2007-08_NBA賽季.md" title="wikilink">2007-08</a>）<br />
<a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a>（<a href="../Page/2007-08_NBA賽季.md" title="wikilink">2007-08</a>–<a href="../Page/2011-12_NBA賽季.md" title="wikilink">2011-12</a>）<br />
<a href="../Page/纽约尼克斯.md" title="wikilink">纽约尼克斯</a>（<a href="../Page/2012-13_NBA赛季.md" title="wikilink">2012-13</a>）</p></td>
<td><p>2,684</p></td>
<td><p>1.9</p></td>
<td><p>2018</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/麥可·喬丹.md" title="wikilink">麥可·喬丹</a></p></td>
<td><p><a href="../Page/得分後衛.md" title="wikilink">得分後衛</a></p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a>（<a href="../Page/1984-85_NBA賽季.md" title="wikilink">1984-85</a>–<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a>）<br />
<a href="../Page/華盛頓巫師.md" title="wikilink">華盛頓巫師</a>（<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>–<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>）</p></td>
<td><p>2,514</p></td>
<td><p>2.3</p></td>
<td><p>2009</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/蓋瑞·裴頓.md" title="wikilink">蓋瑞·裴頓</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a> (<a href="../Page/1990-91_NBA賽季.md" title="wikilink">1990-91</a>–<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>)<br />
<a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a> (<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>)<br />
<a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a> (<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>)<br />
<a href="../Page/波士頓凱爾特人.md" title="wikilink">波士頓凱爾特人</a> (<a href="../Page/2004-05_NBA賽季.md" title="wikilink">2004-05</a>)<br />
<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a> (<a href="../Page/2005-06_NBA賽季.md" title="wikilink">2005-06</a>–<a href="../Page/2006-07_NBA賽季.md" title="wikilink">2006-07</a>)</p></td>
<td><p>2,445</p></td>
<td><p>1.8</p></td>
<td><p>2013</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/莫里斯·奇克斯.md" title="wikilink">莫里斯·奇克斯</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a>（<a href="../Page/1978-79_NBA賽季.md" title="wikilink">1978-79</a>–<a href="../Page/1988-89_NBA賽季.md" title="wikilink">1988-89</a>）<br />
<a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a>（<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a>）<br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>（<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a>–<a href="../Page/1990-91_NBA賽季.md" title="wikilink">1990-91</a>）<br />
<a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a>（<a href="../Page/1991-92_NBA賽季.md" title="wikilink">1991-92</a>）<br />
<a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a>（<a href="../Page/1992-93_NBA賽季.md" title="wikilink">1992-93</a>）</p></td>
<td><p>2,310</p></td>
<td><p>2.1</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/斯科蒂·皮蓬.md" title="wikilink">斯科蒂·皮蓬</a></p></td>
<td><p><a href="../Page/小前鋒.md" title="wikilink">小前鋒</a></p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a>（<a href="../Page/1987-88_NBA賽季.md" title="wikilink">1987-88</a>–<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a>，<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>）<br />
<a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a>（<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>）<br />
<a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a>（<a href="../Page/1999-2000_NBA賽季.md" title="wikilink">1999-2000</a>–<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>）</p></td>
<td><p>2,307</p></td>
<td><p>2.0</p></td>
<td><p>2010</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="../Page/克萊德·德雷克斯勒.md" title="wikilink">克萊德·德雷克斯勒</a></p></td>
<td><p>得分後衛</p></td>
<td><p><a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a>（<a href="../Page/1983-84_NBA賽季.md" title="wikilink">1983-84</a>–<a href="../Page/1994-95_NBA賽季.md" title="wikilink">1994-95</a>）<br />
<a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a>（<a href="../Page/1994-95_NBA賽季.md" title="wikilink">1994-95</a>–<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a>）</p></td>
<td><p>2,207</p></td>
<td><p>2.0</p></td>
<td><p>2004</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><a href="../Page/阿基姆·奧拉朱旺.md" title="wikilink">阿基姆·奧拉朱旺</a></p></td>
<td><p><a href="../Page/中鋒.md" title="wikilink">中鋒</a></p></td>
<td><p><a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a>（<a href="../Page/1984-85_NBA賽季.md" title="wikilink">1984-85</a>–<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>）<br />
<a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a>（<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>）</p></td>
<td><p>2,162</p></td>
<td><p>1.7</p></td>
<td><p>2008</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p><a href="../Page/阿尔文·罗伯特森.md" title="wikilink">阿尔文·罗伯特森</a></p></td>
<td><p>得分後衛<br />
控球後衛</p></td>
<td><p><a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a>（<a href="../Page/1984-85_NBA賽季.md" title="wikilink">1984-85</a>–<a href="../Page/1988-89_NBA賽季.md" title="wikilink">1988-89</a>）<br />
<a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a>（<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a>–<a href="../Page/1992-93_NBA賽季.md" title="wikilink">1992-93</a>）<br />
<a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a>（<a href="../Page/1992-93_NBA賽季.md" title="wikilink">1992-93</a>）<br />
<a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a>（<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>）</p></td>
<td><p>2,112</p></td>
<td><p>2.7</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p><a href="../Page/卡爾·馬龍.md" title="wikilink">卡爾·馬龍</a></p></td>
<td><p><a href="../Page/大前鋒.md" title="wikilink">大前鋒</a></p></td>
<td><p><a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a>（<a href="../Page/1985-86_NBA賽季.md" title="wikilink">1985-86</a>–<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>）<br />
<a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>（<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>）</p></td>
<td><p>2,085</p></td>
<td><p>1.4</p></td>
<td><p>2010</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p><a href="../Page/穆基·布莱洛克.md" title="wikilink">穆基·布莱洛克</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a>（<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a>–<a href="../Page/1991-92_NBA賽季.md" title="wikilink">1991-92</a>）<br />
<a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a>（<a href="../Page/1992-93_NBA賽季.md" title="wikilink">1992-93</a>–<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>）<br />
<a href="../Page/金州勇士.md" title="wikilink">金州勇士</a>（<a href="../Page/1999-2000_NBA賽季.md" title="wikilink">1999-2000</a>–<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>）</p></td>
<td><p>2,075</p></td>
<td><p>2.3</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p><a href="../Page/艾倫·艾佛森.md" title="wikilink">艾倫·艾佛森</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a>（<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a>–<a href="../Page/2006-07_NBA賽季.md" title="wikilink">2006-07</a>）<br />
<a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a>（<a href="../Page/2006-07_NBA賽季.md" title="wikilink">2006-07</a>–<a href="../Page/2008-09_NBA賽季.md" title="wikilink">2008-09</a>）<br />
<a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a>（<a href="../Page/2008-09_NBA賽季.md" title="wikilink">2008-09</a>）<br />
<a href="../Page/孟菲斯灰熊.md" title="wikilink">孟菲斯灰熊</a>（<a href="../Page/2009-10_NBA賽季.md" title="wikilink">2009-10</a>）<br />
<a href="../Page/費城76人.md" title="wikilink">費城76人</a>（<a href="../Page/2009-10_NBA賽季.md" title="wikilink">2009-10</a>）</p></td>
<td><p>1,983</p></td>
<td><p>2.2</p></td>
<td><p>2016</p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td><p><a href="../Page/德里克·哈珀.md" title="wikilink">德里克·哈珀</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a>（<a href="../Page/1983-84_NBA賽季.md" title="wikilink">1983-84</a>–<a href="../Page/1993-94_NBA賽季.md" title="wikilink">1993-94</a>，<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a>）<br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>（<a href="../Page/1993-94_NBA賽季.md" title="wikilink">1993-94</a>–<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>）<br />
<a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a>（<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a>）<br />
<a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>（<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>）</p></td>
<td><p>1,957</p></td>
<td><p>1.6</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p><a href="../Page/克里斯·保罗.md" title="wikilink">克里斯·保罗</a></p></td>
<td><p>控球后卫</p></td>
<td><p><a href="../Page/新奥尔良黄蜂.md" title="wikilink">新奥尔良黄蜂</a><br />
<a href="../Page/洛杉矶快船.md" title="wikilink">洛杉矶快船</a>（<a href="../Page/2011-12_NBA赛季.md" title="wikilink">2011-12</a>-<a href="../Page/2016-17_NBA赛季.md" title="wikilink">2016-17</a>[4]）</p></td>
<td><p>2,042</p></td>
<td><p>2.3</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p><a href="../Page/科比·布萊恩.md" title="wikilink">科比·布萊恩</a></p></td>
<td><p>得分後衛</p></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>(<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a>--<a href="../Page/2015-16_NBA賽季.md" title="wikilink">2015-16</a>）</p></td>
<td><p>1,944</p></td>
<td><p>1.4</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p><a href="../Page/伊塞亞·托馬斯.md" title="wikilink">伊塞亞·托馬斯</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a>（<a href="../Page/1981-82_NBA賽季.md" title="wikilink">1981-82</a>–<a href="../Page/1993-94_NBA賽季.md" title="wikilink">1993-94</a>）</p></td>
<td><p>1,861</p></td>
<td><p>1.9</p></td>
<td><p>2000</p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p><a href="../Page/凱文·賈奈特.md" title="wikilink">凱文·賈奈特</a></p></td>
<td><p>大前鋒</p></td>
<td><p><a href="../Page/明尼蘇達灰狼.md" title="wikilink">明尼蘇達灰狼</a>（<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>–<a href="../Page/2006-07_NBA賽季.md" title="wikilink">2006-07</a>，<a href="../Page/2014-15_NBA赛季.md" title="wikilink">2014-15</a>-<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>[5]）<br />
<a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>（<a href="../Page/2007-08_NBA賽季.md" title="wikilink">2007-08</a>–<a href="../Page/2012-13_NBA賽季.md" title="wikilink">2012-13</a>）<br />
<a href="../Page/布鲁克林篮网.md" title="wikilink">布鲁克林篮网</a>（<a href="../Page/2014-15_NBA赛季.md" title="wikilink">2014-15</a>-<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>）</p></td>
<td><p>1,859</p></td>
<td><p>1.3</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p><a href="../Page/勒布朗·詹姆斯.md" title="wikilink">勒布朗·詹姆斯</a></p></td>
<td><p>小前锋</p></td>
<td><p><a href="../Page/克里夫兰骑士.md" title="wikilink">克里夫兰骑士</a>（<a href="../Page/2003-04_NBA赛季.md" title="wikilink">2003-04</a>-<a href="../Page/2009-10_NBA赛季.md" title="wikilink">2009-10</a>，<a href="../Page/2014-15_NBA赛季.md" title="wikilink">2014-15</a>-<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>[6]）<br />
<a href="../Page/迈阿密热.md" title="wikilink">迈阿密热</a>（<a href="../Page/2010-11_NBA赛季.md" title="wikilink">2010-11</a>-<a href="../Page/2013-14_NBA赛季.md" title="wikilink">2013-14</a>）</p></td>
<td><p>1,791</p></td>
<td><p>1.7</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p><a href="../Page/肖恩·馬里昂.md" title="wikilink">肖恩·馬里昂</a></p></td>
<td><p>大前鋒<br />
小前鋒</p></td>
<td><p><a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a>（<a href="../Page/1999-00_NBA賽季.md" title="wikilink">1999-00</a>–<a href="../Page/2007-08_NBA賽季.md" title="wikilink">2007-08</a>）<br />
<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>（<a href="../Page/2007-08_NBA賽季.md" title="wikilink">2007-08</a>–<a href="../Page/2008-09_NBA賽季.md" title="wikilink">2008-09</a>）<br />
<a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a>（<a href="../Page/2008-09_NBA賽季.md" title="wikilink">2008-09</a>）<br />
<a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a>（<a href="../Page/2009-2010_NBA賽季.md" title="wikilink">2009-10</a>–<a href="../Page/2011-12_NBA賽季.md" title="wikilink">2013-14</a>)<br />
<a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a>（<a href="../Page/2014-15_NBA賽季.md" title="wikilink">2014-15</a>）</p></td>
<td><p>1,759</p></td>
<td><p>1.5</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p><a href="../Page/保羅·皮爾斯.md" title="wikilink">保羅·皮爾斯</a></p></td>
<td><p>小前鋒</p></td>
<td><p><a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>（<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>–<a href="../Page/2012-13_NBA賽季.md" title="wikilink">2012-13</a>）<br />
<a href="../Page/布鲁克林篮网.md" title="wikilink">布鲁克林篮网</a>（<a href="../Page/2013-14_NBA赛季.md" title="wikilink">2013-14</a>）<br />
<a href="../Page/华盛顿奇才.md" title="wikilink">华盛顿奇才</a>（<a href="../Page/2014-15_NBA赛季.md" title="wikilink">2014-15</a>）<br />
<a href="../Page/洛杉矶快船.md" title="wikilink">洛杉矶快船</a>（<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>-<a href="../Page/2016-17_NBA赛季.md" title="wikilink">2016-17</a>）</p></td>
<td><p>1,753</p></td>
<td><p>1.4</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p><a href="../Page/魔術強森.md" title="wikilink">魔術強森</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>（<a href="../Page/1979-80_NBA賽季.md" title="wikilink">1979-80</a>–<a href="../Page/1990-91_NBA賽季.md" title="wikilink">1990-91</a>，<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>）</p></td>
<td><p>1,724</p></td>
<td><p>1.9</p></td>
<td><p>2002</p></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p><a href="../Page/羅恩·哈珀.md" title="wikilink">羅恩·哈珀</a></p></td>
<td><p>得分後衛<br />
控球後衛</p></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a>（<a href="../Page/1986-87_NBA賽季.md" title="wikilink">1986-87</a>–<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a>）<br />
<a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a>（<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a>–<a href="../Page/1993-94_NBA賽季.md" title="wikilink">1993-94</a>）<br />
<a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a>（<a href="../Page/1994-95_NBA賽季.md" title="wikilink">1994-95</a>–<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>）<br />
<a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>（<a href="../Page/1999-00_NBA賽季.md" title="wikilink">1999-00</a>–<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>）</p></td>
<td><p>1,716</p></td>
<td><p>1.7</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td><p><a href="../Page/慈善·世界和平.md" title="wikilink">慈善·世界和平</a></p></td>
<td><p>小前鋒</p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a>（<a href="../Page/1999-00_NBA賽季.md" title="wikilink">1999-00</a>–<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>）<br />
<a href="../Page/印第安那溜馬.md" title="wikilink">印第安那溜馬</a>（<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>–<a href="../Page/2005-06_NBA賽季.md" title="wikilink">2005-06</a>）<br />
<a href="../Page/沙加緬度國王.md" title="wikilink">沙加緬度國王</a>（<a href="../Page/2005-06_NBA賽季.md" title="wikilink">2005-06</a>–<a href="../Page/2007-08_NBA賽季.md" title="wikilink">2007-08</a>）<br />
<a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a>（<a href="../Page/2008-09_NBA賽季.md" title="wikilink">2008-09</a>）<br />
<a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>（<a href="../Page/2009-10_NBA賽季.md" title="wikilink">2009-10</a>–<a href="../Page/2012-13_NBA賽季.md" title="wikilink">2012-13</a>，<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>）<br />
<a href="../Page/纽约尼克斯.md" title="wikilink">纽约尼克斯</a>（<a href="../Page/2013-14_NBA赛季.md" title="wikilink">2013-14</a>）</p></td>
<td><p>1,714</p></td>
<td><p>1.8</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td><p><a href="../Page/拉法叶·利夫.md" title="wikilink">拉法叶·利夫</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a>（<a href="../Page/1982-83_NBA賽季.md" title="wikilink">1982-83</a>–<a href="../Page/1983-84_NBA賽季.md" title="wikilink">1983-84</a>）<br />
<a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a>（<a href="../Page/1984-85_NBA賽季.md" title="wikilink">1984-85</a>–<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a>）<br />
<a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a>（<a href="../Page/1990-91_NBA賽季.md" title="wikilink">1990-91</a>–<a href="../Page/1991-92_NBA賽季.md" title="wikilink">1991-92</a>，<a href="../Page/1993-94_NBA賽季.md" title="wikilink">1993-94</a>）</p></td>
<td><p>1,666</p></td>
<td><p>2.2</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td><p><a href="../Page/查爾斯·巴克利.md" title="wikilink">查爾斯·巴克利</a></p></td>
<td><p>大前鋒</p></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a>（<a href="../Page/1984-85_NBA賽季.md" title="wikilink">1984-85</a>–<a href="../Page/1991-92_NBA賽季.md" title="wikilink">1991-92</a>）<br />
<a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a>（<a href="../Page/1992-93_NBA賽季.md" title="wikilink">1992-93</a>–<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>）<br />
<a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a>（<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a>–<a href="../Page/1999-00_NBA賽季.md" title="wikilink">1999-00</a>）</p></td>
<td><p>1,648</p></td>
<td><p>1.5</p></td>
<td><p>2006</p></td>
</tr>
<tr class="even">
<td><p>26</p></td>
<td><p><a href="../Page/盖斯·威廉姆斯.md" title="wikilink">盖斯·威廉姆斯</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a>（<a href="../Page/1975-76_NBA賽季.md" title="wikilink">1975-76</a>–<a href="../Page/1976-77_NBA賽季.md" title="wikilink">1976-77</a>）<br />
<a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a>（<a href="../Page/1977-78_NBA賽季.md" title="wikilink">1977-78</a>–<a href="../Page/1983-84_NBA賽季.md" title="wikilink">1983-84</a>）<br />
<a href="../Page/華盛頓巫師.md" title="wikilink">華盛頓子彈隊</a>（<a href="../Page/1984-85_NBA賽季.md" title="wikilink">1984-85</a>–<a href="../Page/1985-86_NBA賽季.md" title="wikilink">1985-86</a>）<br />
<a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a>（<a href="../Page/1986-87_NBA賽季.md" title="wikilink">1986-87</a>）</p></td>
<td><p>1,638</p></td>
<td><p>2.0</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>27</p></td>
<td><p><a href="../Page/何塞·霍金斯.md" title="wikilink">何塞·霍金斯</a></p></td>
<td><p>得分後衛</p></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a>（<a href="../Page/1988-89_NBA賽季.md" title="wikilink">1988-89</a>–<a href="../Page/1992-93_NBA賽季.md" title="wikilink">1992-93</a>）<br />
<a href="../Page/紐奧良黃蜂.md" title="wikilink">夏洛特黃蜂</a>（<a href="../Page/1993-94_NBA賽季.md" title="wikilink">1993-94</a>–<a href="../Page/1994-95_NBA賽季.md" title="wikilink">1994-95</a>，<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>）<br />
<a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a>（<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>–<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>）<br />
<a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a>（<a href="../Page/1999-2000_NBA賽季.md" title="wikilink">1999-2000</a>）</p></td>
<td><p>1,622</p></td>
<td><p>1.7</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>28</p></td>
<td><p><a href="../Page/埃迪·琼斯.md" title="wikilink">埃迪·琼斯</a></p></td>
<td><p>得分後衛<br />
小前鋒</p></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>（<a href="../Page/1994-95_NBA賽季.md" title="wikilink">1994-95</a>–<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>）<br />
<a href="../Page/紐奧良黃蜂.md" title="wikilink">夏洛特黃蜂</a>（<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>–<a href="../Page/1999-00_NBA賽季.md" title="wikilink">1999-00</a>）<br />
<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>（<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>–<a href="../Page/2004-05_NBA賽季.md" title="wikilink">2004-05</a>）<br />
<a href="../Page/孟菲斯灰熊.md" title="wikilink">孟菲斯灰熊</a>（<a href="../Page/2005-06_NBA賽季.md" title="wikilink">2005-06</a>–<a href="../Page/2006-07_NBA賽季.md" title="wikilink">2006-07</a>）<br />
<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>（<a href="../Page/2006-07_NBA賽季.md" title="wikilink">2006-07</a>）<br />
<a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a>（<a href="../Page/2007-08_NBA賽季.md" title="wikilink">2007-08</a>）</p></td>
<td><p>1,620</p></td>
<td><p>1.7</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>29</p></td>
<td><p><a href="../Page/罗德·斯特里克兰.md" title="wikilink">罗德·斯特里克兰</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>（<a href="../Page/1988-89_NBA賽季.md" title="wikilink">1988-89</a>–<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a>）<br />
<a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a>（<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a>–<a href="../Page/1991-92_NBA賽季.md" title="wikilink">1991-92</a>）<br />
<a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a>（<a href="../Page/1992-93_NBA賽季.md" title="wikilink">1992-93</a>–<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>，<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>）<br />
<a href="../Page/華盛頓巫師.md" title="wikilink">華盛頓巫師</a>（<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a> –<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>）<br />
<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>（<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>）<br />
<a href="../Page/明尼蘇達灰狼.md" title="wikilink">明尼蘇達灰狼</a>（<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>）<br />
<a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a>（<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>）<br />
<a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a>（<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>）<br />
<a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a>（<a href="../Page/2004-05_NBA賽季.md" title="wikilink">2004-05</a>）</p></td>
<td><p>1,616</p></td>
<td><p>1.5</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>30</p></td>
<td><p><a href="../Page/馬克·傑克遜.md" title="wikilink">馬克·傑克遜</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>（<a href="../Page/1987-88_NBA賽季.md" title="wikilink">1987-88</a>–<a href="../Page/1991-92_NBA賽季.md" title="wikilink">1991-92</a>，<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>–<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>）<br />
<a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a>（<a href="../Page/1992-93_NBA賽季.md" title="wikilink">1992-93</a>–<a href="../Page/1993-94_NBA賽季.md" title="wikilink">1993-94</a>）<br />
<a href="../Page/印第安那溜馬.md" title="wikilink">印第安那溜馬</a>（<a href="../Page/1994-95_NBA賽季.md" title="wikilink">1994-95</a>–<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a>，<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a>–<a href="../Page/1999-00_NBA賽季.md" title="wikilink">1999-00</a>）<br />
<a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a>（<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a>）<br />
<a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a>（<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>）<br />
<a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a>（<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>）<br />
<a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a>（<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>）</p></td>
<td><p>1,608</p></td>
<td><p>1.2</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>31</p></td>
<td><p><a href="../Page/泰瑞·波特.md" title="wikilink">泰瑞·波特</a></p></td>
<td><p>控球後衛<br />
得分後衛</p></td>
<td><p><a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a>（<a href="../Page/1985-86_NBA賽季.md" title="wikilink">1985-86</a>–<a href="../Page/1994-95_NBA賽季.md" title="wikilink">1994-95</a>）<br />
<a href="../Page/明尼蘇達灰狼.md" title="wikilink">明尼蘇達灰狼</a>（<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>–<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a>）<br />
<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>（<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>）<br />
<a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a>（<a href="../Page/1999-00_NBA賽季.md" title="wikilink">1999-00</a>–<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>）</p></td>
<td><p>1,583</p></td>
<td><p>1.2</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>32</p></td>
<td><p><a href="../Page/贾森·特里.md" title="wikilink">贾森·特里</a></p></td>
<td><p>控球后卫</p></td>
<td><p><a href="../Page/亚特兰大老鹰.md" title="wikilink">亚特兰大老鹰</a>（<a href="../Page/1999-00_NBA赛季.md" title="wikilink">1999-00</a>-<a href="../Page/2003-04_NBA赛季.md" title="wikilink">2003-04</a>）<br />
<a href="../Page/达拉斯小牛.md" title="wikilink">达拉斯小牛</a>（<a href="../Page/2004-05_NBA赛季.md" title="wikilink">2004-05</a>-<a href="../Page/2011-12_NBA赛季.md" title="wikilink">2011-12</a>）<br />
<a href="../Page/波士顿凯尔特人.md" title="wikilink">波士顿凯尔特人</a>（<a href="../Page/2012-13_NBA赛季.md" title="wikilink">2013-13</a>）<br />
<a href="../Page/布鲁克林篮网.md" title="wikilink">布鲁克林篮网</a>（<a href="../Page/2013-14_NBA赛季.md" title="wikilink">2013-14</a>）<br />
<a href="../Page/休斯顿火箭.md" title="wikilink">休斯顿火箭</a>（<a href="../Page/2014-15_NBA赛季.md" title="wikilink">2014-15</a>-<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>[7]）</p></td>
<td><p>1,568</p></td>
<td><p>1.2</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>33</p></td>
<td><p><a href="../Page/安德烈·伊格达拉.md" title="wikilink">安德烈·伊格达拉</a></p></td>
<td><p>小前锋<br />
得分后卫</p></td>
<td><p><a href="../Page/费城76人.md" title="wikilink">费城76人</a>（<a href="../Page/2004-05_NBA赛季.md" title="wikilink">2004-05</a>-<a href="../Page/2011-12_NBA赛季.md" title="wikilink">2011-12</a>）<br />
<a href="../Page/丹佛掘金.md" title="wikilink">丹佛掘金</a>（<a href="../Page/2012-13_NBA赛季.md" title="wikilink">2012-13</a>）<br />
<a href="../Page/金州勇士.md" title="wikilink">金州勇士</a>（<a href="../Page/2013-14_NBA赛季.md" title="wikilink">2013-14</a>-<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>[8]）</p></td>
<td><p>1,565</p></td>
<td><p>1.7</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>34</p></td>
<td><p><a href="../Page/道格·里弗斯.md" title="wikilink">道格·里弗斯</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a>（<a href="../Page/1983-84_NBA賽季.md" title="wikilink">1983-84</a>–<a href="../Page/1990-91_NBA賽季.md" title="wikilink">1990-91</a>）<br />
<a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a>（<a href="../Page/1991-92_NBA賽季.md" title="wikilink">1991-92</a>）<br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>（<a href="../Page/1992-93_NBA賽季.md" title="wikilink">1992-93</a>–<a href="../Page/1994-95_NBA賽季.md" title="wikilink">1994-95</a>）<br />
<a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a>（<a href="../Page/1994-95_NBA賽季.md" title="wikilink">1994-95</a>，<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>）</p></td>
<td><p>1,563</p></td>
<td><p>1.8</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>35</p></td>
<td><p><a href="../Page/拉里·伯德.md" title="wikilink">拉里·伯德</a></p></td>
<td><p>大前鋒<br />
小前鋒</p></td>
<td><p><a href="../Page/波士頓凱爾特人.md" title="wikilink">波士頓凱爾特人</a>（<a href="../Page/1979-80_NBA賽季.md" title="wikilink">1979-80</a>–<a href="../Page/1991-92_NBA賽季.md" title="wikilink">1991-92</a>）</p></td>
<td><p>1,556</p></td>
<td><p>1.7</p></td>
<td><p>1998</p></td>
</tr>
<tr class="even">
<td><p>36</p></td>
<td><p><a href="../Page/道格·克里斯蒂.md" title="wikilink">道格·克里斯蒂</a></p></td>
<td><p>得分後衛</p></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>（<a href="../Page/1992-93_NBA賽季.md" title="wikilink">1992-93</a>–<a href="../Page/1993-94_NBA賽季.md" title="wikilink">1993-94</a>）<br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>（<a href="../Page/1994-95_NBA賽季.md" title="wikilink">1994-95</a>–<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>）<br />
<a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a>（<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>–<a href="../Page/1999-00_NBA賽季.md" title="wikilink">1999-00</a>）<br />
<a href="../Page/沙加緬度國王.md" title="wikilink">沙加緬度國王</a>（<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>–<a href="../Page/2004-05_NBA賽季.md" title="wikilink">2004-05</a>）<br />
<a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a>（<a href="../Page/2004-05_NBA賽季.md" title="wikilink">2004-05</a>）<br />
<a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a>（<a href="../Page/2005-06_NBA賽季.md" title="wikilink">2005-06</a>）<br />
<a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a>（<a href="../Page/2006-07_NBA賽季.md" title="wikilink">2006-07</a>）</p></td>
<td><p>1,555</p></td>
<td><p>1.9</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>37</p></td>
<td><p><a href="../Page/安德烈·米勒.md" title="wikilink">安德烈·米勒</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a>（<a href="../Page/1999-00_NBA賽季.md" title="wikilink">1999-00</a>－<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>）<br />
<a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a>（<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>）<br />
<a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a>（<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>－<a href="../Page/2006-07_NBA賽季.md" title="wikilink">2006-07</a>）<br />
<a href="../Page/費城76人.md" title="wikilink">費城76人</a>（<a href="../Page/2006-07_NBA賽季.md" title="wikilink">2006-07</a>－<a href="../Page/2008-09_NBA賽季.md" title="wikilink">2008-09</a>）<br />
<a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a>（<a href="../Page/2009-10_NBA賽季.md" title="wikilink">2009-10</a>－<a href="../Page/2010-11_NBA賽季.md" title="wikilink">2010-11</a>）<br />
<a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a>（<a href="../Page/2011-12_NBA賽季.md" title="wikilink">2011-12</a>-<a href="../Page/2013-14_NBA赛季.md" title="wikilink">2013-14</a>）<br />
<a href="../Page/华盛顿奇才.md" title="wikilink">华盛顿奇才</a>（<a href="../Page/2013-14_NBA赛季.md" title="wikilink">2013-14</a>-<a href="../Page/2014-15_NBA赛季.md" title="wikilink">2014-15</a>）<br />
<a href="../Page/萨克拉门托国王.md" title="wikilink">萨克拉门托国王</a>（<a href="../Page/2014-15_NBA赛季.md" title="wikilink">2014-15</a>）<br />
<a href="../Page/明尼苏达森林狼.md" title="wikilink">明尼苏达森林狼</a>（<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>）<br />
<a href="../Page/圣安东尼奥马刺.md" title="wikilink">圣安东尼奥马刺</a>（<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>）</p></td>
<td><p>1,546</p></td>
<td><p>1.2</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>38</p></td>
<td><p><a href="../Page/内特·麦克米兰.md" title="wikilink">内特·麦克米兰</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a>（<a href="../Page/1986-87_NBA賽季.md" title="wikilink">1986-87</a>–<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a>）</p></td>
<td><p>1,544</p></td>
<td><p>1.9</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>39</p></td>
<td><p><a href="../Page/傑夫·霍納塞克.md" title="wikilink">傑夫·霍納塞克</a></p></td>
<td><p>得分後衛</p></td>
<td><p><a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a>（<a href="../Page/1986-87_NBA賽季.md" title="wikilink">1986-87</a>–<a href="../Page/1991-92_NBA賽季.md" title="wikilink">1991-92</a>）<br />
<a href="../Page/費城76人.md" title="wikilink">費城76人</a>（<a href="../Page/1992-93_NBA賽季.md" title="wikilink">1992-93</a>–<a href="../Page/1993-94_NBA賽季.md" title="wikilink">1993-94</a>）<br />
<a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a>（<a href="../Page/1993-94_NBA賽季.md" title="wikilink">1993-94</a>–<a href="../Page/1999-00_NBA賽季.md" title="wikilink">1999-00</a>）</p></td>
<td><p>1,536</p></td>
<td><p>1.4</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>40</p></td>
<td><p><a href="../Page/貝倫·戴維斯.md" title="wikilink">貝倫·戴維斯</a></p></td>
<td><p>得分後衛</p></td>
<td><p><a href="../Page/夏洛特黃蜂.md" title="wikilink">夏洛特黃蜂</a>（<a href="../Page/1999-00_NBA賽季.md" title="wikilink">1999-00</a>－<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>）<br />
<a href="../Page/紐奧良黃蜂.md" title="wikilink">紐奧良黃蜂</a>（<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>－<a href="../Page/2004-05_NBA賽季.md" title="wikilink">2004-05</a>）<br />
<a href="../Page/金州勇士.md" title="wikilink">金州勇士</a>（<a href="../Page/2004-2005_NBA賽季.md" title="wikilink">2004-2005</a>－<a href="../Page/2007-08_NBA賽季.md" title="wikilink">2007-08</a>）<br />
<a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a>（<a href="../Page/2008-09_NBA賽季.md" title="wikilink">2008-09</a>－<a href="../Page/2010-11_NBA賽季.md" title="wikilink">2010-11</a>）<br />
<a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a>（<a href="../Page/2010-11_NBA賽季.md" title="wikilink">2010-11</a>）<br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>（<a href="../Page/2011-12_NBA賽季.md" title="wikilink">2011-12</a>）</p></td>
<td><p>1,530</p></td>
<td><p>1.8</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>41</p></td>
<td><p><a href="../Page/克里斯·穆林.md" title="wikilink">克里斯·穆林</a></p></td>
<td><p>小前鋒</p></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a>（<a href="../Page/1985-86_NBA賽季.md" title="wikilink">1985-86</a>–<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a>，<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>）<br />
<a href="../Page/印第安那溜馬.md" title="wikilink">印第安那溜馬</a>（<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a>–<a href="../Page/1999-00_NBA賽季.md" title="wikilink">1999-00</a>）</p></td>
<td><p>1,530</p></td>
<td><p>1.6</p></td>
<td><p>2011</p></td>
</tr>
<tr class="even">
<td><p>42</p></td>
<td><p><a href="../Page/德怀恩·韦德.md" title="wikilink">德怀恩·韦德</a></p></td>
<td><p>得分後衛</p></td>
<td><p><a href="../Page/迈阿密热.md" title="wikilink">迈阿密热</a>（<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>–<a href="../Page/2015-16_NBA賽季.md" title="wikilink">2015-16</a>[9])</p></td>
<td><p>1,524</p></td>
<td><p>1.7</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>43</p></td>
<td><p><a href="../Page/肯达尔·吉尔.md" title="wikilink">肯达尔·吉尔</a></p></td>
<td><p>得分後衛</p></td>
<td><p><a href="../Page/紐奧良黃蜂.md" title="wikilink">夏洛特黃蜂</a>（<a href="../Page/1990-91_NBA賽季.md" title="wikilink">1990-91</a>–<a href="../Page/1992-93_NBA賽季.md" title="wikilink">1992-93</a>，<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>）<br />
<a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a>（<a href="../Page/1993-94_NBA賽季.md" title="wikilink">1993-94</a>–<a href="../Page/1994-95_NBA賽季.md" title="wikilink">1994-95</a>）<br />
<a href="../Page/紐奧良黃蜂.md" title="wikilink">夏洛特黃蜂</a>（<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>）<br />
<a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a>（<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>–<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>）<br />
<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>（<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>）<br />
<a href="../Page/明尼蘇達灰狼.md" title="wikilink">明尼蘇達灰狼</a>（<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>）<br />
<a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a>（<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>）<br />
<a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a>（<a href="../Page/2004-05_NBA賽季.md" title="wikilink">2004-05</a>）</p></td>
<td><p>1,519</p></td>
<td><p>1.6</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>44</p></td>
<td><p><a href="../Page/朱利葉斯·歐文.md" title="wikilink">朱利葉斯·歐文</a></p></td>
<td><p>小前鋒</p></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a>（<a href="../Page/1976-77_NBA賽季.md" title="wikilink">1976-77</a>–<a href="../Page/1986-87_NBA賽季.md" title="wikilink">1986-87</a>）</p></td>
<td><p>1,508</p></td>
<td><p>1.8</p></td>
<td><p>1993</p></td>
</tr>
<tr class="odd">
<td><p>45</p></td>
<td><p><a href="../Page/雷吉·米勒.md" title="wikilink">雷吉·米勒</a></p></td>
<td><p>得分後衛</p></td>
<td><p><a href="../Page/印第安那溜馬.md" title="wikilink">印第安那溜馬</a>（<a href="../Page/1987-88_NBA賽季.md" title="wikilink">1987-88</a>–<a href="../Page/2004-05_NBA賽季.md" title="wikilink">2004-05</a>）</p></td>
<td><p>1,505</p></td>
<td><p>1.1</p></td>
<td><p>2012</p></td>
</tr>
<tr class="even">
<td><p>46</p></td>
<td><p><a href="../Page/文斯·卡特.md" title="wikilink">文斯·卡特</a></p></td>
<td><p>得分後衛<br />
小前鋒</p></td>
<td><p><a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a>（<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>–<a href="../Page/2004-05_NBA賽季.md" title="wikilink">2004-05</a>）<br />
<a href="../Page/布魯克林籃網.md" title="wikilink">布魯克林籃網</a>（<a href="../Page/2004-05_NBA賽季.md" title="wikilink">2004-05</a>）– <a href="../Page/2008-09_NBA賽季.md" title="wikilink">2008-09</a>）<br />
<a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a>（<a href="../Page/2009-10_NBA賽季.md" title="wikilink">2009-10</a>）–<a href="../Page/2010-11_NBA賽季.md" title="wikilink">2010-11</a>）<br />
<a href="../Page/達拉斯獨行俠.md" title="wikilink">達拉斯獨行俠</a>（<a href="../Page/2011-12_NBA賽季.md" title="wikilink">2011-12</a>）–<a href="../Page/2013-14_NBA賽季.md" title="wikilink">2013-14</a>）<br />
<a href="../Page/孟菲斯灰熊.md" title="wikilink">孟菲斯灰熊</a>（<a href="../Page/2014-15_NBA賽季.md" title="wikilink">2014-15</a>）–<a href="../Page/2016-17_NBA賽季.md" title="wikilink">2016-17</a>）<br />
<a href="../Page/沙加緬度國王.md" title="wikilink">沙加緬度國王</a>（<a href="../Page/2017-18_NBA賽季.md" title="wikilink">2017-18</a>）<br />
<a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a>（<a href="../Page/2018-19_NBA賽季.md" title="wikilink">2018-19</a>）</p></td>
<td><p>1,489</p></td>
<td><p>1.0</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>47</p></td>
<td><p><a href="../Page/特雷沃·阿里扎.md" title="wikilink">特雷沃·阿里扎</a></p></td>
<td><p>小前鋒</p></td>
<td><p><a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>（<a href="../Page/2004-05_NBA賽季.md" title="wikilink">2004-05</a>–<a href="../Page/2005-06_NBA賽季.md" title="wikilink">2005-06</a>）<br />
<a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a>（<a href="../Page/2005-06_NBA賽季.md" title="wikilink">2005-06</a>）– <a href="../Page/2007-08_NBA賽季.md" title="wikilink">2007-08</a>）<br />
<a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>（<a href="../Page/2007-08_NBA賽季.md" title="wikilink">2007-08</a>）–<a href="../Page/2009-10_NBA賽季.md" title="wikilink">2009-10</a>）<br />
<a href="../Page/紐澳良黃蜂.md" title="wikilink">紐澳良黃蜂</a>（<a href="../Page/2010-11_NBA賽季.md" title="wikilink">2010-11</a>）–<a href="../Page/2011-12_NBA賽季.md" title="wikilink">2011-12</a>）<br />
<a href="../Page/華盛頓巫師.md" title="wikilink">華盛頓巫師</a>（<a href="../Page/2012-13_NBA賽季.md" title="wikilink">2012-13</a>）–<a href="../Page/2013-14_NBA賽季.md" title="wikilink">2013-14</a>）<br />
<a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a>（<a href="../Page/2014-15_NBA賽季.md" title="wikilink">2014-15</a>）–<a href="../Page/2017-18_NBA賽季.md" title="wikilink">2017-18</a>）</p></td>
<td><p>1,488</p></td>
<td><p>1.5</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>48</p></td>
<td><p><a href="../Page/丹尼斯·约翰逊.md" title="wikilink">丹尼斯·约翰逊</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a>（<a href="../Page/1976-77_NBA賽季.md" title="wikilink">1976-77</a>–<a href="../Page/1979-80_NBA賽季.md" title="wikilink">1979-80</a>）<br />
<a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a>（<a href="../Page/1980-81_NBA賽季.md" title="wikilink">1980-81</a>–<a href="../Page/1982-83_NBA賽季.md" title="wikilink">1982-83</a>）<br />
<a href="../Page/波士頓凱爾特人.md" title="wikilink">波士頓凱爾特人</a>（<a href="../Page/1983-84_NBA賽季.md" title="wikilink">1983-84</a>–<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a>）</p></td>
<td><p>1,477</p></td>
<td><p>1.3</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>49</p></td>
<td><p><a href="../Page/迈克尔·雷·理查德森.md" title="wikilink">迈克尔·雷·理查德森</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>（<a href="../Page/1978-79_NBA賽季.md" title="wikilink">1978-79</a>–<a href="../Page/1981-82_NBA賽季.md" title="wikilink">1981-82</a>）<br />
<a href="../Page/金州勇士.md" title="wikilink">金州勇士</a>（<a href="../Page/1982-83_NBA賽季.md" title="wikilink">1982-83</a>）<br />
<a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a>（<a href="../Page/1982-83_NBA賽季.md" title="wikilink">1982-83</a>–<a href="../Page/1985-86_NBA賽季.md" title="wikilink">1985-86</a>）</p></td>
<td><p>1,463</p></td>
<td><p>2.6</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>50</p></td>
<td><p><a href="../Page/雷·阿伦.md" title="wikilink">雷·阿伦</a></p></td>
<td><p><a href="../Page/得分后卫.md" title="wikilink">得分后卫</a></p></td>
<td><p><a href="../Page/密尔沃基雄鹿.md" title="wikilink">密尔沃基雄鹿</a>（<a href="../Page/1996-97_NBA赛季.md" title="wikilink">1996-97</a>-<a href="../Page/2002-03_NBA赛季.md" title="wikilink">2002-03</a>）<br />
<a href="../Page/西雅图超音速.md" title="wikilink">西雅图超音速</a>（<a href="../Page/2002-03_NBA赛季.md" title="wikilink">2002-03</a>-<a href="../Page/2006-07_NBA赛季.md" title="wikilink">2006-07</a>）<br />
<a href="../Page/波士顿凯尔特人.md" title="wikilink">波士顿凯尔特人</a>（<a href="../Page/2007-08_NBA赛季.md" title="wikilink">2007-08</a>-<a href="../Page/2011-12_NBA赛季.md" title="wikilink">2011-12</a>）<br />
<a href="../Page/迈阿密热.md" title="wikilink">迈阿密热</a>（<a href="../Page/2012-13_NBA赛季.md" title="wikilink">2012-13</a>-<a href="../Page/2013-14_NBA赛季.md" title="wikilink">2013-14</a>）</p></td>
<td><p>1,451</p></td>
<td><p>1.1</p></td>
<td><p>2018</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 註腳

<references />

[Category:NBA數據統計](../Category/NBA數據統計.md "wikilink")

1.  排名統計至2017年12月17日。

2.  總計統計至2017年12月17日。

3.  每場平均統計至2017年12月17日。

4.
5.
6.
7.
8.
9.