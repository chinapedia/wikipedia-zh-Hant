**NBC偵察車**是[防衛省開發的NBC](../Page/防衛省.md "wikilink")（核・生物・化學）武器處理用輪式[裝甲車](../Page/裝甲車.md "wikilink")。2009年（平成21年）開發完成，2010年（平成22年）開始服役。

陸上自衛隊現在有NC型（[核武](../Page/核子彈.md "wikilink")・[化學](../Page/化學武器.md "wikilink")）處理用的[化學防護車和B型](../Page/化學防護車.md "wikilink")（[細菌戰](../Page/生化武器.md "wikilink")）處理用的[生物偵察車](../Page/生物偵察車.md "wikilink")，所以構想中一體化的NBC處理車輛已經是必然的，預計配備在化學科兵種。

核・生物・化學（NBC）武器可以造成廣範圍地域污染狀況所以探測識別器材和偵察行動是必要的情報收集，以便了解早期狀況、以採取適當措施・抑止被害範圍擴大。化學防護車和生物偵察車一體化的未來輪型戰鬥車；可以使運用彈性加大、減低後續災難成本。

## 相關條目

  - [細菌戰](../Page/生化武器.md "wikilink")
  - [核子彈](../Page/核子彈.md "wikilink")
  - [陸上自衛隊](../Page/陸上自衛隊.md "wikilink")
  - [化學武器](../Page/化學武器.md "wikilink")

## 參考文獻

  - [防衛省・自衛隊：平成16年度 事前の事業評価
    政策評価書一覧](https://web.archive.org/web/20070930205843/http://www.mod.go.jp/j/info/hyouka/16/jizen/)
      - [要旨
        (PDF)](https://web.archive.org/web/20070930231952/http://www.mod.go.jp/j/info/hyouka/16/jizen/youshi/02.pdf)
        - [本文
        (PDF)](https://web.archive.org/web/20070930223841/http://www.mod.go.jp/j/info/hyouka/16/jizen/honbun/02.pdf)
        - [参考
        (PDF)](https://web.archive.org/web/20070930225222/http://www.mod.go.jp/j/info/hyouka/16/jizen/sankou/02.pdf)
  - [TRDI - NBC Reconnaissance Vehicle
    (JPG)](https://web.archive.org/web/20110323220708/http://www.mod.go.jp/trdi/en/programs/ground/nbc.jpg)

[Category:日本裝甲戰鬥車輛](../Category/日本裝甲戰鬥車輛.md "wikilink")