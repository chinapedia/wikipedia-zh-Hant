**NEC紅火箭**（）是一支[日本女子排球隊](../Page/日本.md "wikilink")，隸屬於[日本電氣旗下](../Page/日本電氣.md "wikilink")，總部設於[神奈川縣](../Page/神奈川縣.md "wikilink")[川崎市](../Page/川崎市.md "wikilink")（主場分別設於[東京都](../Page/東京都.md "wikilink")[港區及](../Page/港區.md "wikilink")[神奈川縣](../Page/神奈川縣.md "wikilink")[川崎市](../Page/川崎市.md "wikilink")），目前是[V超級聯賽女子組的球隊之一](../Page/V超級聯賽.md "wikilink")。現任領隊為[山田晃豐](../Page/山田晃豐.md "wikilink")。

## 過往戰績

  - [V超級聯賽](../Page/V超級聯賽.md "wikilink")（日本聯賽女子組）
      - 冠軍（1987-1988球季、1996-1997球季、1999-2000球季、2002-2003球季、2004-2005球季、2014-2015球季、2016-2017球季）
      - 亞軍（1986-1987球季、1995-1996球季、1997-1998球季、2001-2002球季）
  - [黑鷲旗全日本排球錦標賽](../Page/黑鷲旗全日本排球錦標賽.md "wikilink")
      - 冠軍（1997年、2001年）
      - 亞軍（1985年、1987年、1995年、1996年、1999年、2011年、2013年）

## 成員名單

### 選手

2015年1月版\[1\]\[2\]

<table>
<thead>
<tr class="header">
<th><p>編號</p></th>
<th><p>姓名</p></th>
<th><p>衣背姓名</p></th>
<th><p>國籍</p></th>
<th><p>位置</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/八幡美樹.md" title="wikilink">八幡美樹</a></p></td>
<td><p>YAHATA</p></td>
<td></td>
<td><p>主攻手</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p><a href="../Page/Yeliz_Askan_Başa.md" title="wikilink">Yeliz Askan Başa</a></p></td>
<td><p>YELIZ</p></td>
<td></td>
<td><p>主攻手</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/山口かなめ.md" title="wikilink">山口かなめ</a></p></td>
<td><p>YAMAGUCHI</p></td>
<td></td>
<td><p>舉球員</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/近江あかり.md" title="wikilink">近江あかり</a></p></td>
<td><p>OUMI</p></td>
<td></td>
<td><p>主攻手</p></td>
<td><p><strong>副隊長</strong></p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="../Page/秋山美幸.md" title="wikilink">秋山美幸</a></p></td>
<td><p>AKIYAMA</p></td>
<td></td>
<td><p>舉球員</p></td>
<td><p><strong>隊長</strong></p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><a href="../Page/島村春世.md" title="wikilink">島村春世</a></p></td>
<td><p>SHIMAMURA</p></td>
<td></td>
<td><p>副攻手</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p><a href="../Page/白垣里紗.md" title="wikilink">白垣里紗</a></p></td>
<td><p>SHIRAGAKI</p></td>
<td></td>
<td><p>主攻手</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p><a href="../Page/岩崎紗也加.md" title="wikilink">岩崎紗也加</a></p></td>
<td><p>IWASAKI</p></td>
<td></td>
<td><p>自由人</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p><a href="../Page/金子早織.md" title="wikilink">金子早織</a></p></td>
<td><p>KANEKO</p></td>
<td></td>
<td><p>主攻手</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p><a href="../Page/大野果奈.md" title="wikilink">大野果奈</a></p></td>
<td><p>ONO</p></td>
<td></td>
<td><p>副攻手</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p><a href="../Page/鳥越未玖.md" title="wikilink">鳥越未玖</a></p></td>
<td><p>TORIGOE</p></td>
<td></td>
<td><p>自由人</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p><a href="../Page/松崎美歌.md" title="wikilink">松崎美歌</a></p></td>
<td><p>MATSUZAKI</p></td>
<td></td>
<td><p>副攻手</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p><a href="../Page/上野香織.md" title="wikilink">上野香織</a></p></td>
<td><p>UENO</p></td>
<td></td>
<td><p>副攻手</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p><a href="../Page/小山綾菜.md" title="wikilink">小山綾菜</a></p></td>
<td><p>OYAMA</p></td>
<td></td>
<td><p>主攻手</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p><a href="../Page/家高七央子.md" title="wikilink">家高七央子</a></p></td>
<td><p>YATAKA</p></td>
<td></td>
<td><p>副攻手</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p><a href="../Page/佐川奈美.md" title="wikilink">佐川奈美</a></p></td>
<td><p>SAGAWA</p></td>
<td></td>
<td><p>OP</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p><a href="../Page/柳田光綺.md" title="wikilink">柳田光綺</a></p></td>
<td><p>YANAGITA</p></td>
<td></td>
<td><p>主攻手</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p><a href="../Page/奥山優奈.md" title="wikilink">奥山優奈</a></p></td>
<td><p>OKUYAMA</p></td>
<td></td>
<td><p>舉球員</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p><a href="../Page/古賀紗理那.md" title="wikilink">古賀紗理那</a></p></td>
<td><p>KOGA</p></td>
<td></td>
<td><p>主攻手</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p><a href="../Page/篠原沙耶香.md" title="wikilink">篠原沙耶香</a></p></td>
<td><p>SHINOHARA</p></td>
<td></td>
<td><p>舉球員</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 教練及職員

2014年11月版\[3\]\[4\]

<table>
<thead>
<tr class="header">
<th><p>職務</p></th>
<th><p>姓名</p></th>
<th><p>國籍</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>部長</strong></p></td>
<td><p>籔内正三</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>總經理</strong></p></td>
<td><p>中村貴司</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>領隊</strong></p></td>
<td><p><a href="../Page/山田晃豊.md" title="wikilink">山田晃豊</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>主教練</strong></p></td>
<td><p><a href="../Page/大村悟.md" title="wikilink">大村悟</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>教練</strong></p></td>
<td><p>高橋悠</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>教練</strong></p></td>
<td><p>三上晃右</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>トレーニングコーチ</strong></p></td>
<td><p>柴田昌奈</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>訓練員</strong></p></td>
<td><p>小川未央</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>訓練員</strong></p></td>
<td><p>滝田敦子</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>經理</strong></p></td>
<td><p>松田祥子</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>通譯</strong></p></td>
<td><p>長崎節子</p></td>
<td></td>
</tr>
</tbody>
</table>

## 隊服贊助

  - [mizuno](../Page/美津濃.md "wikilink")

## 參考資料

## 外部連結

  - [NEC紅火箭官方網站](http://w-volley.necsports.net/)
  - [NEC紅火箭官方Facebook](https://www.facebook.com/NECRedRockets.VL/?fref=ts)

[Category:日本排球](../Category/日本排球.md "wikilink")

1.
2.
3.
4.