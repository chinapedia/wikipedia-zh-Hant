**nForce
600**系列是[NVIDIA研發的第六代](../Page/NVIDIA.md "wikilink")[主機板](../Page/主機板.md "wikilink")[晶片組](../Page/晶片組.md "wikilink")，首款產品nForce
680在[2006年11月推出](../Page/2006年11月.md "wikilink")\[1\]。晶片組名稱後面的英文字母，代表所支援的處理器平台。例如a代表支援AMD平台，i則是Intel平台。

## 產品規格

### AMD平臺

#### nForce 680a SLI

nForce 680a SLI是為了配合[AMD的](../Page/AMD.md "wikilink")[Quad
FX平臺而開發](../Page/AMD_Quad_FX_平臺.md "wikilink")。由於這個晶元組使用了兩個晶片，所以架構與nForce4
Professional相似。 規格：

  - 支援[AMD的](../Page/AMD.md "wikilink")[Quad
    FX平臺](../Page/AMD_Quad_FX_平臺.md "wikilink")，CPU接口是伺服器平台專用的[Socket
    F](../Page/Socket_F.md "wikilink")
  - 支援两个[HyperTransport通道](../Page/HyperTransport.md "wikilink")
  - 每個晶片支援兩个[PCI Express](../Page/PCI_Express.md "wikilink")
    x16接口，總共支援四个PCI Express x16接口。
  - 支援12個[SATA](../Page/SATA.md "wikilink") 3Gbps介面，可运行两個独立的RAID
    5，每個支援6隻硬碟。

#### GeForce 7050 PV nForce 630a

為了對抗AMD的690G晶片組，NVIDIA將[C61P晶元組易名為MCP](../Page/NVIDIA_C61晶元組.md "wikilink")68PV，推出了nForce
630a整合型晶片組。它支持[AM2接口的AMD處理器](../Page/AM2.md "wikilink")，並整合了GeForce
7050顯示核心，顯核核心頻率是475MHz。 其餘規格：

  - 支援高清音效
  - 擁有10个[USB](../Page/USB.md "wikilink") 2.0接口
  - 擁有4个[SATA-2接口](../Page/SATA-2.md "wikilink")，並支持[RAID技術](../Page/RAID.md "wikilink")
  - 新增支援TV-OUT、[DVI和](../Page/DVI.md "wikilink")[HDMI顯示輸出](../Page/HDMI.md "wikilink")

#### GeForce 7025 nForce 630a

與**GeForce 7050/nForce
630a**相近，差異僅在於顯核核心頻率較低，只有425MHz。它亦不支援[HDMI顯示输出](../Page/HDMI.md "wikilink")，[PureVideo和视频输入功能](../Page/PureVideo.md "wikilink")。

### Intel平臺

#### nForce 680i [SLI](../Page/SLI.md "wikilink")

nVidia代工製造，取代2006年7月推出的 nForce 590 SLI intel Edition，定位於高端。它支持 1333MHz
FSB，改善了記憶體控制器，支援DDR2-1200記憶體，还支持内存的EPP规范。它有两條全速的PCI-E x16 插槽，还有第3条PCIe
x8模式的PCIe
x16插槽，預計用于物理加速。作為高端晶片組，當然支援[LinkBoost技術](../Page/LinkBoost.md "wikilink")。功能與nForce
590 SLI大致相同。

网络連接方面，680i SLI
支援兩個千兆以太网接口、[FirstPacket技術](../Page/FirstPacket.md "wikilink")、[DualNet](../Page/DualNet.md "wikilink")、[TCP/IP加速技术](../Page/TCP/IP加速技术.md "wikilink")，与590
SLI相同。另外，它支援[HD Audio](../Page/HD_Audio.md "wikilink")。存储接口方面，支援6个SATA
3Gbps接口、[MediaShield存储技术](../Page/MediaShield.md "wikilink")，RAID模式有0、1、0+1和5。

由於超頻能力比前作 590 SLI intel Edition 大大提升，680i SLI 迅速受到高端用家的注意。

#### nForce 680i LT SLI

它是nForce 680i
[SLI的廉价精简版](../Page/SLI.md "wikilink")。只支援DDR2-800記憶體，只有一個千兆以太网接口和两條PCIe
x16插槽。此外，它的超频能力亦有所降低。但由於使用公版 PCB 的 680i LT SLI 底板 BIOS 與 680i SLI
共通，實質上分別不大，更使原不能負擔680i SLI 的人願意付鈔購買。

#### nForce 650i SLI

相对680i SLI，它不支援1333MHz总线，只支援两条PCIe x16插槽，並都只以PCIe
x8模式運作。支持雙通道DDR2記憶體，但不支持EPP規範記憶體。存儲接口方面，與680i
SLI相似。

网络連接方面，相对680i SLI，去掉了DualNet和TCP/IP加速技术。

一般認為，nForce 650i SLI的對手是Intel P965。相对Intel P965，nForce 650i
SLI支援[SLI技術](../Page/SLI.md "wikilink")，並擁有兩個[IDE接口](../Page/IDE.md "wikilink")，且支援[RAID技術](../Page/RAID.md "wikilink")。而Intel
P965則不支援[IDE接口](../Page/IDE.md "wikilink")，所以nForce 650i
SLI的擴充性比較好。P965的優勢是支援10個[USB接口](../Page/USB.md "wikilink")，比nForce
650i SLI多兩個。最後，Intel P965擁有6個[PCI-E
x1接口](../Page/PCI-E.md "wikilink")，比nForce 650i SLI多4個。

#### nForce 650i Ultra

只支援單条PCIe
x16插槽，不支援[SLI技術](../Page/SLI.md "wikilink")。支持双通道DDR2記憶體，但不支持EPP規範記憶體。存储接口方面，有4個SATA
3Gbps接口。

#### nForce 630i(MCP73)

於2007年年中，NVIDIA在AMD平台芯片组有六成佔有率，相比之下，在Intel平台只有不足1%的佔有率。由於AMD收購了ATI，變相AMD推出自家的晶片組。NVIDIA與AMD的關係亦出現變數。由原本的合作伙伴，變為晶片組市場的競爭對手。所以NVIDIA須要打入Intel晶片組市場，以維持自己的領先地位。在ATI被AMD收購後，ATI全面退出Intel平台，這倒給了NVIDIA一個機會。當時，ATI的Intel平台佔有率是大約8%。而NVIDIA就推出整合式晶片組，企圖打入Intel整合式晶片組市場。它支持Intel的[Core
2核心處理器](../Page/Intel_Core_2.md "wikilink")，並整合了GeForce
7050顯示核心，首次支持[HDMI接口](../Page/HDMI.md "wikilink")，但只支持單通道記憶體，亦不支援[PureVideo技術](../Page/PureVideo.md "wikilink")。NVIDIA稱這樣做是降低成本，增加競爭力。有外界普遍認為這是來自Intel的限制，因為這樣可避免直接與自家的G33晶片組競爭。

MCP73採用80nm製程製造，集成了[GeForce
7顯示核心](../Page/GeForce_7.md "wikilink")。縱使只支援單通道記憶體，影響集成顯示核心的效能發揮。但數據顯示，由於NVIDIA的驅動程式比較完善，所以立體貼圖錯誤較少，畫質和效能都比對手Intel的G33晶片組高。顯示核心有兩條像素流水線，支持Direct
X 9.0c和Shader Model 3.0。顯示核心頻率介乎500-600
MHz，視乎不同的形號。記憶體方面，與G33不同，只支援最高DDR2
800規格，不支援DDR
3。存儲技術方面，全系列都支援[MediaShield技術](../Page/MediaShield.md "wikilink")，支持RAID
0、RAID 1、RAID 0+1和RAID 5。音效方面，支援[HD
Audio](../Page/HD_Audio.md "wikilink")。网络接口方面，則原生支援Gigabit Ethernet。

它有三款形號：

  - GeForce 7150+nForce 630i -
    顯示核心頻率較高，提供[HDMI或](../Page/HDMI.md "wikilink")[DVI顯示輸出接口](../Page/DVI.md "wikilink")
  - GeForce 7100+nForce 630i - 顯示核心頻率較低
  - GeForce 7050+nForce 610i -
    不支援[HDMI和](../Page/HDMI.md "wikilink")[DVI顯示輸出](../Page/DVI.md "wikilink")，亦不支持[HDCP](../Page/HDCP.md "wikilink")。

## 相關條目

  - [NVIDIA晶片組列表](../Page/NVIDIA晶片組列表.md "wikilink")

## 參考鏈接

## 外部連結

  - [nForce 600 AMD平臺官方網頁](http://www.nvidia.com/page/nforce_600a.html)
  - [nForce 600
    Intel平臺官方網頁](http://www.nvidia.com/page/nforce_600i.html)

[Category:主板](../Category/主板.md "wikilink")
[Category:英伟达](../Category/英伟达.md "wikilink")

1.  [New NVIDIA Products Transform the PC Into the Definitive Gaming
    Platform](http://www.nvidia.com/object/IO_37234.html)