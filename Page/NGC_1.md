**NGC
1**是一個距離[地球](../Page/地球.md "wikilink")1.9億[光年](../Page/光年.md "wikilink")、在[飛馬座的](../Page/飛馬座.md "wikilink")[漩渦星系](../Page/漩渦星系.md "wikilink")。它的视亮度為13等，[赤經為](../Page/赤經.md "wikilink")7秒（[曆元](../Page/曆元.md "wikilink")1860）。
[紅位移約](../Page/紅位移.md "wikilink")4545[km](../Page/公里.md "wikilink")／s。星系[直徑約](../Page/直徑.md "wikilink")90000[光年](../Page/光年.md "wikilink")，只比我們所在的[銀河系小一點](../Page/銀河系.md "wikilink")。它是第一個被收錄在[NGC天體表的星體](../Page/NGC天體表.md "wikilink")。\[1\]

## 参见

  - [NGC天体列表](../Page/NGC天体列表.md "wikilink")

## 參考資料

<div class="references-small">

<references/>

:\*
[<http://seds.org/>](https://web.archive.org/web/20080503094607/http://www.seds.org/~spider/ngc/ngc.cgi?1)

:\*
[<http://backword.me.uk/>](https://web.archive.org/web/20120421003437/http://backword.me.uk/2005/August/galaxies.html)

</div>

## 外部連結

  -
[Category:飞马座NGC天体](../Category/飞马座NGC天体.md "wikilink") [NGC
0001](../Category/螺旋星系.md "wikilink") [NGC
0001](../Category/飛馬座.md "wikilink")
[0001](../Category/NGC天体.md "wikilink")
[00564](../Category/PGC天体.md "wikilink")
[00057](../Category/UGC天体.md "wikilink")

1.