[Galaxy_NGC_1232.jpg](https://zh.wikipedia.org/wiki/File:Galaxy_NGC_1232.jpg "fig:Galaxy_NGC_1232.jpg")

**NGC 1232**
是[波江座的一個](../Page/波江座.md "wikilink")[中間螺旋星系](../Page/中間螺旋星系.md "wikilink")，距離地球約6000萬光年\[1\]。由[弗里德里希·威廉·赫歇爾發現於](../Page/弗里德里希·威廉·赫歇爾.md "wikilink")1784年10月20日。

## 概要

NGC
1232環繞中心的螺旋臂內有大量明亮恆星與暗灰塵。螺旋臂內的[疏散星团內有大量](../Page/疏散星团.md "wikilink")[藍色恆星沿著螺旋臂分佈](../Page/恒星光谱.md "wikilink")，其間有大量的星際塵埃暗區。較暗的恆星和[星際氣體佔了星系質量大部分](../Page/星际物质.md "wikilink")，並且主導了星系內側動力狀態。而星系外側可見物體的動力模式則必須以目前無法被直接觀察的[暗物质的存在解釋](../Page/暗物质.md "wikilink")。

NGC 1232和它的衛星星系，以及另一個大星系[NGC
1300是](../Page/NGC_1300.md "wikilink")[波江座星系團的成員星系](../Page/波江座星系團.md "wikilink")。

## ESO 547-16

**ESO 547-16**，即NGC 1232A是NGC
1232的[衛星星系](../Page/衛星星系.md "wikilink")\[2\]。一般認為它的形成是因為螺旋臂內異常扭曲造成。1988年的觀測結果，NGC
1232A距離地球約6800萬光年\[3\]，而NGC 1232距離地球約6000萬光年\[4\]。

## 參見

  - [NGC 1097](../Page/NGC_1097.md "wikilink")
  - [風車星系](../Page/風車星系.md "wikilink")
  - [渦狀星系](../Page/渦狀星系.md "wikilink")

## 參考資料

[Category:中間螺旋星系](../Category/中間螺旋星系.md "wikilink")
[41](../Category/阿普天體.md "wikilink")
[1232](../Category/波江座NGC天體.md "wikilink")
[11819](../Category/PGC天體.md "wikilink")

1.
2.

3.

4.