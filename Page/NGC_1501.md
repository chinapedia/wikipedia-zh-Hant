{{ Planetary nebula | | image =
[NGC_1501.jpg](https://zh.wikipedia.org/wiki/File:NGC_1501.jpg "fig:NGC_1501.jpg")
| caption = [哈伯太空望遠鏡拍攝](../Page/哈伯太空望遠鏡.md "wikilink") | name = NGC 1501
| type = | epoch = [J2000](../Page/J2000.md "wikilink") | ra = \[1\] |
dec = \[2\] | appmag_v = 13.0\[3\] | size_v = 0.863' （直徑） | dist_ly =
est. \[4\] | constellation = [鹿豹座](../Page/鹿豹座.md "wikilink") | names =
PK 144+6.1, PN G 144.5+06.5, GC 801, CS 14.4, H 4.53 | radius_ly = |
absmag_v = | notes = }}

**NGC 1501**
是[鹿豹座的一個](../Page/鹿豹座.md "wikilink")[行星狀星雲](../Page/行星狀星雲.md "wikilink")。於1787年11月3日由[弗里德里希·威廉·赫歇爾發現](../Page/弗里德里希·威廉·赫歇爾.md "wikilink")\[5\]\[6\]。該星雲因中心恆星相當明亮，以及氣體外層形狀類似牡蠣殼，因此有個非正式暱稱**牡蠣星雲**（Oyster
Nebula）\[7\]。

## 參考資料

[Category:行星狀星雲](../Category/行星狀星雲.md "wikilink")
[1501](../Category/鹿豹座NGC天體.md "wikilink")

1.

2.
3.
4.

5.
6.

7.  [Hubble View of Bubbly
    Nebula](https://www.nasa.gov/content/goddard/hubble-view-of-bubbly-nebula)