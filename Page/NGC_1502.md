**NGC
1502**位於[鹿豹座](../Page/鹿豹座.md "wikilink")，是由大約45顆恆星組成的一個小[疏散星團](../Page/疏散星團.md "wikilink")，是[威廉·赫歇爾在](../Page/威廉·赫歇爾.md "wikilink")1787年11月3日發現\[1\]。[甘伯串珠看似](../Page/甘伯串珠.md "wikilink")"流入"這個星團。

[少衛
(右垣)](../Page/少衛_\(右垣\).md "wikilink")（鹿豹座α）可能是從這個星團拋射出來的[速逃星](../Page/恆星運動學#速逃星.md "wikilink")。

## 參考資料

  -
[1502](../Category/NGC天體.md "wikilink")
[Category:鹿豹座](../Category/鹿豹座.md "wikilink")

1.