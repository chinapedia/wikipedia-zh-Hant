{{ Infobox globular cluster | name = [NGC](../Page/NGC天體表.md "wikilink")
1818 | image =
[Ngc1818_hst_big.jpg](https://zh.wikipedia.org/wiki/File:Ngc1818_hst_big.jpg "fig:Ngc1818_hst_big.jpg")
|caption = [哈伯太空望遠鏡拍攝NGC](../Page/哈伯太空望遠鏡.md "wikilink") 1818影像
Credit:
HST/[NASA](../Page/NASA.md "wikilink")/[ESA](../Page/ESA.md "wikilink").
| epoch = [J2000](../Page/J2000.md "wikilink") | class = | constellation
= [劍魚座](../Page/劍魚座.md "wikilink") | ra = \[1\] | dec = \[2\] | dist_ly
= 164 kly | dist_pc = | appmag_v = 9.7B\[3\] | size_v = | mass_kg =
| mass_msol = | radius_ly = | v_hb = | age = \~25 Myr\[4\] | notes =
少見的年輕球狀星團 | names = }}

**NGC
1818**是位於[大麦哲伦星系內的年輕星團](../Page/大麦哲伦星系.md "wikilink")。該天體被認為是[球狀星團](../Page/球狀星團.md "wikilink")，但年齡僅約2500萬年，而目前已知的球狀星團年齡幾乎至少在數十億年前形成。

NGC 1818於1826年由天文學家[詹姆士·敦洛普發現](../Page/詹姆士·敦洛普.md "wikilink")。

## 參考資料

## 外部連結

  -
  -
  -
  -

</div>

[1818](../Category/劍魚座NGC天體.md "wikilink")
[Category:球狀星團](../Category/球狀星團.md "wikilink") [Category:
大麦哲伦星系](../Category/_大麦哲伦星系.md "wikilink")
[1826](../Category/1826年發現的天體.md "wikilink")

1.

2.
3.
4.