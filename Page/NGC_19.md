**NGC
19**是[仙女座的一個](../Page/仙女座.md "wikilink")[漩渦星系](../Page/漩渦星系.md "wikilink")。[星等為](../Page/星等.md "wikilink")13.3，[赤經為](../Page/赤經.md "wikilink")10分40.8秒，[赤緯為](../Page/赤緯.md "wikilink")+32°58'58"。在1885年9月20日首次被發現。

## 參見

  - [NGC天体列表](../Page/NGC天体列表.md "wikilink")

## 參考資料

[Category:仙女座NGC天体](../Category/仙女座NGC天体.md "wikilink")