**NGC
2040**是距離地球約16萬光年的[疏散星團](../Page/疏散星團.md "wikilink")\[1\]，在天球上位於[劍魚座](../Page/劍魚座.md "wikilink")。NGC
2040是[大麥哲倫星系內部最大範圍之一的](../Page/大麥哲倫星系.md "wikilink")[恆星形成區域中的一群年輕恆星聚集處](../Page/恆星形成.md "wikilink")\[2\]。

## 參考資料

[Category:星協](../Category/星協.md "wikilink")
[2040](../Category/劍魚座NGC天體.md "wikilink")
[Category:大麥哲倫星系](../Category/大麥哲倫星系.md "wikilink")

1.
2.