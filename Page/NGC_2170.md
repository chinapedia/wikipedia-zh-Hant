**NGC 2170**
是[麒麟座的一個面积相当大的](../Page/麒麟座.md "wikilink")[反射星雲](../Page/反射星雲.md "wikilink")。它离地球的距离约为2400光年，整个星云的直径大约有15光年。从地球上看上去这个星云像一幅水彩画：它有蓝色的（比较明亮的）反射部分，也有红色的（比较暗的）放射部分。該星雲由[威廉·赫歇爾發現於](../Page/威廉·赫歇爾.md "wikilink")1784年10月16日。

[Infrared_VISTA_view_of_a_nearby_star_formation_in_Monoceros.jpg](https://zh.wikipedia.org/wiki/File:Infrared_VISTA_view_of_a_nearby_star_formation_in_Monoceros.jpg "fig:Infrared_VISTA_view_of_a_nearby_star_formation_in_Monoceros.jpg")拍攝的NGC
2170與鄰近區域紅外線影像。\]\]

## 參考資料

  - [NASA Astronomy Picture of the Day: NGC 2170: Celestial Still Life
    (14 January 2009)](http://apod.nasa.gov/apod/ap090114.html)

[2170](../Category/麒麟座NGC天體.md "wikilink")
[Category:反射星雲](../Category/反射星雲.md "wikilink")