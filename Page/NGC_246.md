{{ Planetary nebula | image =
[NGC_246.jpg](https://zh.wikipedia.org/wiki/File:NGC_246.jpg "fig:NGC_246.jpg")
|
caption=[史匹哲太空望遠鏡以](../Page/史匹哲太空望遠鏡.md "wikilink")[紅外線拍攝的](../Page/紅外線.md "wikilink")。
創建：[NASA](../Page/NASA.md "wikilink")/[JPL](../Page/JPL.md "wikilink")。
| name = [NGC](../Page/NGC星表.md "wikilink") 246 | type = | epoch =
[J2000](../Page/J2000.md "wikilink") | ra = \[1\] | dec = \[2\] |
dist_ly = 1,600 [光年](../Page/光年.md "wikilink") | appmag_v = 8\[3\] /
11.8 (central star)\[4\] | size_v = 3.8′\[5\] | constellation =
[鯨魚座](../Page/鯨魚座.md "wikilink") | radius_ly = 2-3 光年\[6\] |
absmag_v = | notes = | names
=骷髏星雲\[7\]、小精靈星雲\[8\]、[科德韋爾](../Page/科德韋爾星表.md "wikilink") 56、[HIP](../Page/HIP星表.md "wikilink")
3678、PMN J0047-1152、 2E 178, PN VV 4, [IRAS](../Page/IRAS.md "wikilink")
00445-1207\[9\] }}

**NGC
246**，也稱為**骷髏星雲**\[10\]，是位於[鯨魚座的一個](../Page/鯨魚座.md "wikilink")[行星狀星雲](../Page/行星狀星雲.md "wikilink")。在[SIMBAD匯總的資料庫中](../Page/SIMBAD.md "wikilink")，這個星雲和與它相關的恆星列在好幾個星表內\[11\]。它的距離大約在1,600[光年](../Page/光年.md "wikilink")\[12\]，在星雲的中心有一顆[視星等](../Page/視星等.md "wikilink")12等\[13\]的[白矮星HIP](../Page/白矮星.md "wikilink")
3678。

有一些業餘天文學家因為NGC 246中心恆星和周圍恆星場的排列，而稱它為"小精靈星雲"（"Pac-Man Nebula"）\[14\]。

## 圖集

<File:The> Skull Nebula NGC246 Goran Nilsson & The Liverpool
Telescope.jpg|骷髏星雲（NGC 246）的HaRGB影像。資料來自，總曝光時間為1.1小時，經由Göran
Nilsson處理成像。
[File:n246s.jpg|這是使用美國亞利桑那州萊蒙山頂的0.8米Schulman望遠鏡拍攝的NGC](File:n246s.jpg%7C這是使用美國亞利桑那州萊蒙山頂的0.8米Schulman望遠鏡拍攝的NGC)
246全色（視覺）影像。

## 參考資料

## 外部連結

[Category:行星狀星雲](../Category/行星狀星雲.md "wikilink")
[Category:鯨魚座](../Category/鯨魚座.md "wikilink")
[0246](../Category/NGC天體.md "wikilink")
[056b](../Category/科德韋爾天體.md "wikilink")
[17841127](../Category/1785年發現的天體.md "wikilink")

1.
2.
3.
4.
5.

6.

7.
8.
9.
10. "The Night Sky", *[Astronomy
    Now](../Page/Astronomy_Now.md "wikilink")*, Oct 2008.

11.

12. Stephen James O'Meara, *The Caldwell Objects*, Sky Publishing
    Corporation, , p 223.

13.
14. David H. Levy, *Deep Sky Objects*, Prometheus Books, 2005, , p 129.