**NGC 299**
是[杜鹃座的一個](../Page/杜鹃座.md "wikilink")[疏散星团](../Page/疏散星团.md "wikilink")。該星團位於[小麦哲伦星系內部](../Page/小麦哲伦星系.md "wikilink")，距離地球將近20萬光年\[1\]，於1834年8月12日由[約翰·赫歇爾發現](../Page/約翰·赫歇爾.md "wikilink")\[2\]。

天文學家分析NGC
299的[OGLE-III測光觀測資料](../Page/光學重力透鏡實驗.md "wikilink")，結果在這個年輕疏散星團內未發現低光變幅度的脈動變星，與其他小麥哲倫星系內的年輕疏散星團不同\[3\]。

## 參考資料

[Category:疏散星團](../Category/疏散星團.md "wikilink")
[Category:小麥哲倫星系](../Category/小麥哲倫星系.md "wikilink") [051-SC
005](../Category/ESO天體.md "wikilink")
[299](../Category/杜鵑座NGC天體.md "wikilink")

1.  [Hubble reveals NGC 299 within the Small Magellanic
    Cloud](https://astronomynow.com/2016/10/17/hubble-reveals-ngc-299-within-the-small-magellanic-cloud/)
2.
3.  [Photometric Analysis of Variable Stars in
    NGC 299](https://arxiv.org/abs/1302.6943)