**NGC
36**是[雙魚座的一個](../Page/雙魚座.md "wikilink")[漩渦星系](../Page/漩渦星系.md "wikilink")。[星等為](../Page/星等.md "wikilink")13.3，[赤經為](../Page/赤經.md "wikilink")11分22.4秒，[赤緯為](../Page/赤緯.md "wikilink")+6°23'20"。在1785年10月25日首次被[弗里德里希·威廉·赫歇爾發現](../Page/弗里德里希·威廉·赫歇爾.md "wikilink")。

## 參見

  - [NGC天體列表](../Page/NGC天體列表.md "wikilink")

## 參考資料

## 外部連結

  - [NGC 36](https://web.archive.org/web/20041227020625/http://www.seds.org/~spider/ngc/ngc_fr.cgi?36)

[Category:双子座NGC天体](../Category/双子座NGC天体.md "wikilink")