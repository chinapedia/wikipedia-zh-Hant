**NGC
4**是一個非常黯淡的[星系](../Page/星系.md "wikilink")。它位於[雙魚座](../Page/雙魚座.md "wikilink")，[星等為](../Page/星等.md "wikilink")15.9等，[赤經為](../Page/赤經.md "wikilink")7分24.5秒，[赤緯為](../Page/赤緯.md "wikilink")+8°22'26"。在1864年11月29日被首次發現。

## 參見

  - [NGC天体列表](../Page/NGC天体列表.md "wikilink")

## 參考文獻

  -
[Category:双鱼座NGC天体](../Category/双鱼座NGC天体.md "wikilink")