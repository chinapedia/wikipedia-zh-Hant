{{ Cluster | | image =
[NGC602.jpg](https://zh.wikipedia.org/wiki/File:NGC602.jpg "fig:NGC602.jpg")
<small>A [哈勃太空望远镜](../Page/哈勃太空望远镜.md "wikilink") (HST) image of NGC
602.
Credit:
HST/[NASA](../Page/NASA.md "wikilink")/[ESA](../Page/ESA.md "wikilink").</small>
| name = [NGC](../Page/星云和星团新总表.md "wikilink") 602 | epoch =
[J2000](../Page/J2000.md "wikilink") | class =
[疏散星团](../Page/疏散星团.md "wikilink") | ra = \[1\] | dec =
\[2\] | constellation = [杜鵑座](../Page/杜鵑座.md "wikilink") | dist_ly =
196 [kly](../Page/光年.md "wikilink")\[3\] | dist_pc = 61
[kpc](../Page/秒差距.md "wikilink")\[4\] | appmag_v = - /\> | size_v
= 1′.5 × 0′.7\[5\] | mass_kg = - | mass_msol = - | radius_ly = 90 ly
| v_hb = - | age = 5 My\[6\] | notes = - | names = N90\[7\] }} **NGC
602**
是[水蛇座的一個](../Page/水蛇座.md "wikilink")[彌散星雲](../Page/彌散星雲.md "wikilink")。

## 參考文獻

[Category:水蛇座NGC天体](../Category/水蛇座NGC天体.md "wikilink")
[Category:小麥哲倫星系](../Category/小麥哲倫星系.md "wikilink")

1.

2.
3.

4.
5.

6.

7.