**NGC 88**(**ESO 194-10, AM 0018-485, PGC
1370**)是[鳳凰座一個](../Page/鳳凰座.md "wikilink")[棒旋星系](../Page/棒旋星系.md "wikilink")，[星等為](../Page/星等.md "wikilink")14.1等，[赤經為](../Page/赤經.md "wikilink")21分22秒，[赤緯為](../Page/赤緯.md "wikilink")-48°38'23"。在1834年9月30日首次被[約翰·弗里德里希·威廉·赫歇爾發現](../Page/約翰·弗里德里希·威廉·赫歇爾.md "wikilink")，與[NGC
87](../Page/NGC_87.md "wikilink")、[NGC
89和](../Page/NGC_89.md "wikilink")[NGC
92一起組成](../Page/NGC_92.md "wikilink")[羅伯特四重奏](../Page/羅伯特四重奏.md "wikilink")。

## 參見

  - [NGC天體列表](../Page/NGC天體列表.md "wikilink")

## 外部連結

  - [NGC 88](https://web.archive.org/web/20041025224020/http://www.seds.org/~spider/ngc/ngc_fr.cgi?88)
  - <http://nedwww.ipac.caltech.edu/cgi-bin/nph-objsearch?objname=NGC%2088>

## 參考資料

[Category:凤凰座NGC天体](../Category/凤凰座NGC天体.md "wikilink")
[Category:鳳凰座](../Category/鳳凰座.md "wikilink")
[0088](../Category/NGC天體.md "wikilink")
[01370](../Category/PGC天體.md "wikilink")
[Category:羅伯特四重奏](../Category/羅伯特四重奏.md "wikilink")
[Category:棒旋星系](../Category/棒旋星系.md "wikilink")