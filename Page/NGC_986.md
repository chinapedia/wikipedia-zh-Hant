**NGC
986**是一個距離地球約5600萬光年的[棒旋星系](../Page/棒旋星系.md "wikilink")，在天球上位於[天爐座](../Page/天爐座.md "wikilink")。該星系有兩個各自以中心棒狀結構兩端為起點的巨大且延伸甚遠，並且略微彎曲的旋臂。該星系於1826年由蘇格蘭天文學家[詹姆士·敦洛普發現](../Page/詹姆士·敦洛普.md "wikilink")。

## 參考資料

[Category:棒旋星系](../Category/棒旋星系.md "wikilink")
[0986](../Category/天爐座NGC天體.md "wikilink")
[Category:1826年發現的天體](../Category/1826年發現的天體.md "wikilink")