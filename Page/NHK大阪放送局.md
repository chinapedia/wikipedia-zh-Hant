**NHK大阪放送局**（）是[日本放送協會](../Page/日本放送協會.md "wikilink")（NHK）的放送局之一，為基干局，也是NHK在东京发生重大灾害时的战备台\[1\]。地址位于[大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")[中央區大手前四丁目](../Page/中央區_\(大阪市\).md "wikilink")1番20號。其電視服務開始于1954年3月1日，廣播服務開始于1925年6月1日，呼號為JOBK。其中，[綜合頻道和](../Page/NHK綜合頻道.md "wikilink")[廣播FM頻率的放送範圍為大阪府](../Page/NHK-FM頻率.md "wikilink")，[教育頻道放送範圍為](../Page/NHK教育頻道.md "wikilink")[近畿廣域圈](../Page/近畿廣域圈.md "wikilink")，[廣播第1頻率放送範圍為大阪府](../Page/NHK廣播第1頻率.md "wikilink")、[京都府](../Page/京都府.md "wikilink")、[兵庫縣](../Page/兵庫縣.md "wikilink")、[奈良縣和](../Page/奈良縣.md "wikilink")[和歌山縣](../Page/和歌山縣.md "wikilink")，[廣播第2頻率放送範圍為近畿廣域圈和](../Page/NHK廣播第2頻率.md "wikilink")[德島縣](../Page/德島縣.md "wikilink")。NHK有大量節目在此制作，每年下半年的[晨間小說連續劇也在此制作](../Page/晨間小說連續劇.md "wikilink")。[NHK京都放送局](../Page/NHK京都放送局.md "wikilink")、[NHK神戶放送局](../Page/NHK神戶放送局.md "wikilink")、[NHK姬路支局](../Page/NHK姬路支局.md "wikilink")、[NHK大津放送局](../Page/NHK大津放送局.md "wikilink")、[NHK奈良放送局和](../Page/NHK奈良放送局.md "wikilink")[NHK和歌山放送局皆歸該局管轄](../Page/NHK和歌山放送局.md "wikilink")。

## 参考资料

## 外部連結

  - [NHK大阪放送局](http://www.nhk.or.jp/osaka/)

[分类:1925年日本建立](../Page/分类:1925年日本建立.md "wikilink")

[Category:NHK](../Category/NHK.md "wikilink")

1.