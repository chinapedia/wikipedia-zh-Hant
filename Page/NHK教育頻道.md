**NHK教育頻道**（），又譯為**NHK教育電視台**、**NHK教育台**，為[日本放送協會](../Page/日本放送協會.md "wikilink")（NHK）的[無線電視頻道之一](../Page/無線電視.md "wikilink")，通称**ETV**（[台標顯示為](../Page/台標.md "wikilink")「<font size=-2></font>」）。開播於1959年1月10日，為[日本最早的教育專門](../Page/日本.md "wikilink")[電視臺](../Page/電視臺.md "wikilink")，以及世界上最早的教育电视频道。絕大部分節目均由位於[東京](../Page/東京.md "wikilink")[澀谷的](../Page/澀谷.md "wikilink")[NHK放送中心負責製播](../Page/NHK放送中心.md "wikilink")，再透過NHK在日本各地的放送局向全國播出。身為NHK的一份子，以NHK教育頻道為中心，除了每年製作超過1萬部節目之外，還有節目相關映像[軟體及活動](../Page/軟體.md "wikilink")[企劃製作](../Page/企劃.md "wikilink")、拓展角色人物等，進行多元化的事業發展。

## 概要

自1989年（平成元年）設立以來，以「廣泛且多樣化的教育內容節目製作」為目標，在兒童、教育、美術、教養、語言學習、興趣、實用、科學、健康等領域中，以擁有高水準專業性的製作集團。自2000年開始實行24小時播出。

2006年4月3日起，受到[一連串醜聞導致收視費減少的影響](../Page/NHK的醜聞.md "wikilink")，停止24小時播出。

2009年4月6日，NHK教育頻道3（NHK Eテレ3）開播\[1\]。

目前此頻道將訊號分割為二，分別是數位教育1台（デジタルEテレ1）及數位教育3台（デジタルEテレ3），當兩台播放不同節目時，採一般標準畫質放送，觀眾需擁有[高清晰度电视](../Page/高清晰度电视.md "wikilink")，才可收看數位教育1台和數位教育3台\[2\]。一般情况下，教育1台和教育3台播放不同節目的時間是每周一至周五的下午2時至3時，但如果當[NHK綜合頻道要臨時播放新聞節目時](../Page/NHK綜合頻道.md "wikilink")，綜合頻道原定的節目會改在教育1台播放，而教育3台則會維持播放原定播放的內容。

## 開播日期

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

| 播放地                                         | 類比播放        | 數位播放       |
| ------------------------------------------- | ----------- | ---------- |
| [東京](../Page/NHK放送中心.md "wikilink")         | 1959年1月10日  | 2003年12月1日 |
| [大阪](../Page/NHK大阪放送局.md "wikilink")        | 1959年4月1日   | 2003年12月1日 |
| [函館](../Page/NHK函館放送局.md "wikilink")        | 1960年8月1日   | 2007年10月1日 |
| [室蘭](../Page/NHK室蘭放送局.md "wikilink")        | 1960年9月1日   | 2007年10月1日 |
| [旭川](../Page/NHK旭川放送局.md "wikilink")        | 1960年11月1日  | 2007年10月1日 |
| [福島](../Page/NHK福島放送局.md "wikilink")        | 1960年11月1日  | 2005年12月1日 |
| [仙台](../Page/NHK仙台放送局.md "wikilink")        | 1960年12月1日  | 2005年12月1日 |
| [盛岡](../Page/NHK盛岡放送局.md "wikilink")        | 1960年12月23日 | 2005年12月1日 |
| [広島](../Page/NHK広島放送局.md "wikilink")        | 1961年1月8日   | 2006年10月1日 |
| [富山](../Page/NHK富山放送局.md "wikilink")        | 1961年4月1日   | 2004年10月1日 |
| [高知](../Page/NHK高知放送局.md "wikilink")        | 1961年8月1日   | 2006年10月1日 |
| [青森](../Page/NHK青森放送局.md "wikilink")        | 1961年10月7日  | 2005年12月1日 |
| [静岡](../Page/NHK静岡放送局.md "wikilink")        | 1962年1月8日   | 2005年6月1日  |
| [北九州](../Page/NHK北九州放送局.md "wikilink")\[3\] | 1962年1月8日   | 2006年10月1日 |
| [釧路](../Page/NHK釧路放送局.md "wikilink")        | 1962年2月1日   | 2007年10月1日 |
| [名古屋](../Page/NHK名古屋放送局.md "wikilink")      | 1962年3月27日  | 2003年12月1日 |
| [金沢](../Page/NHK金沢放送局.md "wikilink")        | 1962年4月1日   | 2006年7月1日  |
| [鹿児島](../Page/NHK鹿児島放送局.md "wikilink")      | 1962年4月8日   | 2006年12月1日 |
| [札幌](../Page/NHK札幌放送局.md "wikilink")        | 1962年6月1日   | 2006年6月1日  |
| [松山](../Page/NHK松山放送局.md "wikilink")        | 1962年6月1日   | 2006年10月1日 |
| [山口](../Page/NHK山口放送局.md "wikilink")        | 1962年9月1日   | 2006年10月1日 |
| [福岡](../Page/NHK福岡放送局.md "wikilink")        | 1962年9月1日   | 2006年4月1日  |
| [帯広](../Page/NHK帯広放送局.md "wikilink")        | 1962年10月1日  | 2007年10月1日 |
| [秋田](../Page/NHK秋田放送局.md "wikilink")        | 1962年10月13日 | 2005年12月1日 |
| [山形](../Page/NHK山形放送局.md "wikilink")        | 1962年11月1日  | 2005年12月1日 |
| [新潟](../Page/NHK新潟放送局.md "wikilink")        | 1962年11月1日  | 2006年4月1日  |
| [長野](../Page/NHK長野放送局.md "wikilink")        | 1962年11月1日  | 2006年4月1日  |

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

| 播放地                                    | 類比播放        | 數位播放       |
| -------------------------------------- | ----------- | ---------- |
| [福井](../Page/NHK福井放送局.md "wikilink")   | 1962年11月1日  | 2006年5月1日  |
| [大分](../Page/NHK大分放送局.md "wikilink")   | 1962年12月1日  | 2006年12月1日 |
| [松江](../Page/NHK松江放送局.md "wikilink")   | 1962年12月28日 | 2006年10月1日 |
| [鳥取](../Page/NHK鳥取放送局.md "wikilink")   | 1962年12月28日 | 2006年10月1日 |
| [熊本](../Page/NHK熊本放送局.md "wikilink")   | 1963年2月14日  | 2006年12月1日 |
| [岡山](../Page/NHK岡山放送局.md "wikilink")   | 1963年6月29日  | 2006年12月1日 |
| [北見](../Page/NHK北見放送局.md "wikilink")   | 1963年12月1日  | 2007年10月1日 |
| [甲府](../Page/NHK甲府放送局.md "wikilink")   | 1963年12月1日  | 2006年4月1日  |
| [宮崎](../Page/NHK宮崎放送局.md "wikilink")   | 1963年12月1日  | 2006年12月1日 |
| [長崎](../Page/NHK長崎放送局.md "wikilink")   | 1963年12月1日  | 2006年12月1日 |
| [徳島](../Page/NHK徳島放送局.md "wikilink")   | 1968年2月20日  | 2006年10月1日 |
| [佐賀](../Page/NHK佐賀放送局.md "wikilink")   | 1969年3月15日  | 2006年12月1日 |
| [高松](../Page/NHK高松放送局.md "wikilink")   | 1969年3月22日  | 2006年12月1日 |
| [沖縄](../Page/NHK沖縄放送局.md "wikilink")   | 1972年5月15日  | 2006年4月1日  |
| [京都](../Page/NHK京都放送局.md "wikilink")   | （未開播）\[4\]  | 2010年7月24日 |
| [水戸](../Page/NHK水戸放送局.md "wikilink")   | 規劃中         |            |
| [宇都宮](../Page/NHK宇都宮放送局.md "wikilink") |             |            |
| [前橋](../Page/NHK前橋放送局.md "wikilink")   |             |            |
| [埼玉](../Page/NHK埼玉放送局.md "wikilink")   |             |            |
| [千葉](../Page/NHK千葉放送局.md "wikilink")   |             |            |
| [横浜](../Page/NHK横浜放送局.md "wikilink")   |             |            |
| [岐阜](../Page/NHK岐阜放送局.md "wikilink")   |             |            |
| [津](../Page/NHK津放送局.md "wikilink")     |             |            |
| [大津](../Page/NHK大津放送局.md "wikilink")   |             |            |
| [神戸](../Page/NHK神戸放送局.md "wikilink")   |             |            |
| [奈良](../Page/NHK奈良放送局.md "wikilink")   |             |            |
| [和歌山](../Page/NHK和歌山放送局.md "wikilink") |             |            |

</div>

## 參考

<references/>

## 外部連結

  - [NHK教育頻道](http://www.nhk.or.jp/e-tele)

[Category:日本地面電視](../Category/日本地面電視.md "wikilink")
[Category:NHK电视频道](../Category/NHK电视频道.md "wikilink")

1.  參閱
2.  [不下課的銀幕教室](http://info.pts.org.tw/open/paper/2007/doc2007/1.pdf)
3.  アナログ教育テレビ開局時は｢小倉放送局｣
4.  京都中継局開播教育頻道時，即為「數位訊號」