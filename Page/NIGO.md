**NIGO**（ニゴー，本名：**長尾智明**；），是一位[日本](../Page/日本.md "wikilink")[饒舌歌手及](../Page/饒舌歌手.md "wikilink")[服裝設計師](../Page/服裝設計師.md "wikilink")，[A
Bathing
Ape創辦人](../Page/A_Bathing_Ape.md "wikilink")，[Nowhere](../Page/Nowhere.md "wikilink")[行政總裁](../Page/行政總裁.md "wikilink")。NIGO於1993年在[裏原宿成立](../Page/裏原宿.md "wikilink")[A
Bathing
Ape品牌](../Page/A_Bathing_Ape.md "wikilink")，後來於2011年賣給香港[I.T集團](../Page/I.T.md "wikilink")，現為[HUMAN
MADE創辦人](../Page/HUMAN_MADE.md "wikilink")。

## 簡歷

NIGO在[群馬縣](../Page/群馬縣.md "wikilink")[前橋市出生](../Page/前橋市.md "wikilink")，畢業於[文化服装学院](../Page/文化服装学院.md "wikilink")。在1991年的4月，與Undercover的創辦人高橋盾合作開設NOWHERE，主要以售賣BAPE爲主，每逢有新PRODUCT發售，必定會吸引一班忠實的FANS搶購，
顧名思義，A Bathing Ape是以猿人作爲LOGO及創作元素，而且亦用了電影《Planet of the
Apes》（[决战猩球](../Page/决战猩球.md "wikilink")）的一句經典的口號「Ape shall
never kill
ape」作爲其品牌的標語。BAPE深受明星喜愛，從而帶領搶購熱潮。09年3月NIGO透過日本版時尚雜誌《WWD》宣佈辭任BAPE®母公司NOWHERE社長的消息，及後，他又宣布於同年09年4月1日設立新公司NIGOLD。2008年12月16日，對外發布與女演員[牧瀨里穗的](../Page/牧瀨里穗.md "wikilink")[結婚消息](../Page/結婚.md "wikilink")。於2011年創辦時裝品牌[HUMAN
MADE](../Page/HUMAN_MADE.md "wikilink")。

## 特徵

他從來不會笑，而且跟[藤原浩有幾分相似](../Page/藤原浩.md "wikilink")，因而常有人誤以為他是藤原浩。

时时刻刻都戴着帽子。

## 參見

  - [A Bathing Ape](../Page/A_Bathing_Ape.md "wikilink")
  - [TERIYAKI BOYZ](../Page/TERIYAKI_BOYZ.md "wikilink")

## 參考資料

  - [NIGO®發火非同小可
    BAPE®將軍猿人頭tee](https://web.archive.org/web/20090419161256/http://bee.newmonday.com.hk/shownews.php?id=659)
    @[Bee.WEBZINE](https://web.archive.org/web/20090426163635/http://bee.newmonday.com.hk/)
  - [BAPE® 16週年慶祝產品正式開慛](https://web.archive.org/web/20090416041346/http://bee.newmonday.com.hk/shownews.php?id=643)
    @[Bee.WEBZINE](https://web.archive.org/web/20090426163635/http://bee.newmonday.com.hk/)
  - [NIGO®離任BAPE®
    改組新公司NIGOLD](https://web.archive.org/web/20090512161936/http://bee.newmonday.com.hk/shownews.php?id=627)
    @[Bee.WEBZINE](https://web.archive.org/web/20090426163635/http://bee.newmonday.com.hk/)

## 外部連結

  - [BAPE.COM | A BATHING APE OFFICIAL SITE](https://bape.com/index/)
  - [HUMAN MADE](https://humanmade.jp/)

[Category:日本設計師](../Category/日本設計師.md "wikilink") [Category:TOY'S
FACTORY旗下藝人](../Category/TOY'S_FACTORY旗下藝人.md "wikilink")