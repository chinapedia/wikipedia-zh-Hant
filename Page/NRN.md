****（，英文），即「**全國廣播網**」，是[日本一家](../Page/日本.md "wikilink")[廣播聯播網](../Page/廣播聯播網.md "wikilink")，成立於1965年5月3日，[加盟者為](../Page/加盟.md "wikilink")[AM私營](../Page/調幅廣播.md "wikilink")[廣播電臺](../Page/廣播電臺.md "wikilink")。其[核心局為](../Page/核心局.md "wikilink")[文化放送及](../Page/文化放送_\(日本\).md "wikilink")[日本放送](../Page/日本放送.md "wikilink")。NRN共有40個廣播電台加盟，是日本最大的廣播聯播網。

## 成員列表

本表排列順序為[北海道](../Page/北海道.md "wikilink")-[東北地方](../Page/東北地方.md "wikilink")-[關東地方](../Page/關東地方.md "wikilink")-[甲信越](../Page/甲信越.md "wikilink")-[靜岡](../Page/靜岡.md "wikilink")-[東海地方](../Page/東海地方.md "wikilink")-[北陸地方](../Page/北陸地方.md "wikilink")-[近畿地方](../Page/近畿地方.md "wikilink")-[中四國](../Page/中國、四國地方.md "wikilink")-[九州](../Page/九州.md "wikilink")[沖繩](../Page/沖繩.md "wikilink")。記號欄內使用符號如下：

  - ◆為只加入NRN
  - ◇為同時加入NRN與JRN
  - ☆為有兼營[電視台](../Page/電視台.md "wikilink")
  - ▼為實施AM[立體聲廣播](../Page/立體聲.md "wikilink")
  - ○為未加入任何[電視聯播網](../Page/電視聯播網.md "wikilink")

<table>
<tbody>
<tr class="odd">
<td><p><strong>廣播區域</strong></p></td>
<td><p><strong>簡稱</strong></p></td>
<td><p><strong>名稱</strong></p></td>
<td><p><strong>備考</strong></p></td>
<td><p><strong>記號</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/北海道.md" title="wikilink">北海道</a></p></td>
<td><p><strong>HBC</strong></p></td>
<td><p><a href="../Page/北海道放送.md" title="wikilink">北海道放送</a>（HBCラジオ）</p></td>
<td></td>
<td><p>◇☆</p></td>
</tr>
<tr class="odd">
<td><p><strong>STV</strong></p></td>
<td></td>
<td><p>2005年10月1日繼承母公司<a href="../Page/札幌電視台.md" title="wikilink">札幌電視台的廣播部門</a>。おもに制作・営業関係を子会社に移行（会社設立は同年7月12日）。ただし、電波送信管理やアナウンス業務など制作･営業関係以外のラジオ放送業務は<a href="../Page/受託.md" title="wikilink">受託の形で引き続き札幌テレビ放送が行っている</a>。</p></td>
<td><p>◆</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/青森縣.md" title="wikilink">青森縣</a></p></td>
<td><p><strong>RAB</strong></p></td>
<td><p><a href="../Page/青森放送.md" title="wikilink">青森放送</a></p></td>
<td></td>
<td><p>◇☆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/岩手縣.md" title="wikilink">岩手縣</a></p></td>
<td><p><strong>IBC</strong></p></td>
<td><p><a href="../Page/IBC岩手放送.md" title="wikilink">IBC岩手放送</a></p></td>
<td><p><small>1995年6月22日以前的名稱為<a href="../Page/岩手放送.md" title="wikilink">岩手放送</a></p></td>
<td><p>◇☆</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宮城縣.md" title="wikilink">宮城縣</a></p></td>
<td><p><strong>TBC</strong></p></td>
<td><p><a href="../Page/東北放送.md" title="wikilink">東北放送</a></p></td>
<td><p><small>ネットワークへのライン送出が可能な設備がある</p></td>
<td><p>◇☆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/秋田縣.md" title="wikilink">秋田縣</a></p></td>
<td><p><strong>ABS</strong></p></td>
<td><p><a href="../Page/秋田放送.md" title="wikilink">秋田放送</a></p></td>
<td></td>
<td><p>◇☆</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/山形縣.md" title="wikilink">山形縣</a></p></td>
<td><p><strong>YBC</strong></p></td>
<td><p><a href="../Page/山形放送.md" title="wikilink">山形放送</a></p></td>
<td></td>
<td><p>◇☆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/福島縣.md" title="wikilink">福島縣</a></p></td>
<td><p><strong>RFC</strong></p></td>
<td></td>
<td></td>
<td><p>◇</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/關東廣域圈.md" title="wikilink">關東廣域圈</a></p></td>
<td><p><strong>QR</strong></p></td>
<td><p><a href="../Page/株式會社文化放送.md" title="wikilink">文化放送</a></p></td>
<td><p><small>NRN基幹局，加盟<a href="../Page/富士產經集團.md" title="wikilink">富士產經集團</a>。</p></td>
<td><p>◆▼</p></td>
</tr>
<tr class="odd">
<td><p><strong>LF</strong></p></td>
<td><p><a href="../Page/日本放送.md" title="wikilink">日本放送</a></p></td>
<td><p>◆▼</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/茨城縣.md" title="wikilink">茨城縣</a></p></td>
<td><p><strong>IBS</strong></p></td>
<td><p><a href="../Page/茨城放送.md" title="wikilink">茨城放送</a></p></td>
<td><p><small>2001年4月正式加盟</p></td>
<td><p>◆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/栃木縣.md" title="wikilink">栃木縣</a></p></td>
<td><p><strong>CRT</strong></p></td>
<td><p><a href="../Page/栃木放送.md" title="wikilink">栃木放送</a></p></td>
<td></td>
<td><p>◆</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/山梨縣.md" title="wikilink">山梨縣</a></p></td>
<td><p><strong>YBS</strong></p></td>
<td><p><a href="../Page/山梨放送.md" title="wikilink">山梨放送</a></p></td>
<td></td>
<td><p>◇☆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/長野縣.md" title="wikilink">長野縣</a></p></td>
<td><p><strong>SBC</strong></p></td>
<td><p><a href="../Page/信越放送.md" title="wikilink">信越放送</a></p></td>
<td></td>
<td><p>◇☆</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/新潟縣.md" title="wikilink">新潟縣</a></p></td>
<td><p><strong>BSN</strong></p></td>
<td><p><a href="../Page/新潟放送.md" title="wikilink">新潟放送</a></p></td>
<td></td>
<td><p>◇☆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/靜岡縣.md" title="wikilink">靜岡縣</a></p></td>
<td><p><strong>SBS</strong></p></td>
<td><p><a href="../Page/静岡放送.md" title="wikilink">静岡放送</a></p></td>
<td><p><small>ネットワークへのライン送出が可能な設備がある</p></td>
<td><p>◇☆</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中京廣域圈.md" title="wikilink">中京廣域圈</a></p></td>
<td><p><strong>SF</strong></p></td>
<td><p><a href="../Page/東海ラジオ放送.md" title="wikilink">東海ラジオ放送</a></p></td>
<td><p><small>1965年7月4日に加盟。<a href="../Page/中日新聞社.md" title="wikilink">中日新聞社が筆頭株主</a>。<a href="../Page/東海電視台.md" title="wikilink">東海電視台と関連がある</a>。<br />
ネットワークへのライン送出が可能な設備がある。</p></td>
<td><p>◆▼</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/富山縣.md" title="wikilink">富山縣</a></p></td>
<td><p><strong>KNB</strong></p></td>
<td><p><a href="../Page/北日本放送.md" title="wikilink">北日本放送</a></p></td>
<td></td>
<td><p>◇☆</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/石川縣.md" title="wikilink">石川縣</a></p></td>
<td><p><strong>MRO</strong></p></td>
<td><p><a href="../Page/北陸放送.md" title="wikilink">北陸放送</a></p></td>
<td><p><small>1980年12月1日加盟</p></td>
<td><p>◇☆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/福井縣.md" title="wikilink">福井縣</a></p></td>
<td><p><strong>FBC</strong></p></td>
<td><p><a href="../Page/福井放送.md" title="wikilink">福井放送</a></p></td>
<td></td>
<td><p>◇☆</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/近畿廣域圈.md" title="wikilink">近畿廣域圈</a><br />
<small>（<a href="../Page/大阪府.md" title="wikilink">大阪府</a>）</p></td>
<td><p><strong>ABC</strong></p></td>
<td><p><a href="../Page/朝日放送.md" title="wikilink">朝日放送</a>（ABCラジオ）</p></td>
<td><p><small>近畿廣域圈（大阪府）は3社で加盟。大阪放送のみ<a href="../Page/富士產經集團.md" title="wikilink">富士產經集團に加盟</a>。<br />
ネットワークへのライン送出が可能な設備がある（ラジオ大阪）。</p></td>
<td><p>◇☆</p></td>
</tr>
<tr class="odd">
<td><p><strong>MBS</strong></p></td>
<td><p><a href="../Page/每日放送.md" title="wikilink">每日放送</a>（MBSラジオ）</p></td>
<td><p>◇☆</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>OBC</strong></p></td>
<td><p><a href="../Page/大阪放送.md" title="wikilink">大阪放送</a>（ラジオ大阪）</p></td>
<td><p>◆▼</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/京都府.md" title="wikilink">京都府</a></p></td>
<td><p><strong>KBS</strong></p></td>
<td><p><a href="../Page/京都放送.md" title="wikilink">京都放送</a>（<a href="../Page/KBS京都.md" title="wikilink">KBS京都</a>）</p></td>
<td><p><small>滋賀縣內為<a href="../Page/KBS滋賀.md" title="wikilink">KBS滋賀</a></p></td>
<td><p>◆☆○</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/滋賀縣.md" title="wikilink">滋賀縣</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/和歌山縣.md" title="wikilink">和歌山縣</a></p></td>
<td><p><strong>WBS</strong></p></td>
<td><p><a href="../Page/和歌山放送.md" title="wikilink">和歌山放送</a></p></td>
<td></td>
<td><p>◇▼</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鳥取縣.md" title="wikilink">鳥取縣</a></p></td>
<td><p><strong>BSS</strong></p></td>
<td><p><a href="../Page/山陰放送.md" title="wikilink">山陰放送</a></p></td>
<td></td>
<td><p>◇☆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/島根縣.md" title="wikilink">島根縣</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/岡山縣.md" title="wikilink">岡山縣</a></p></td>
<td><p><strong>RSK</strong></p></td>
<td><p><a href="../Page/山陽放送.md" title="wikilink">山陽放送</a></p></td>
<td><p><small>1997年9月5日加盟</p></td>
<td><p>◇☆▼</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/廣島縣.md" title="wikilink">廣島縣</a></p></td>
<td><p><strong>RCC</strong></p></td>
<td><p><a href="../Page/中國放送.md" title="wikilink">中國放送</a></p></td>
<td><p><small>ネットワークへのライン送出が可能な設備がある</p></td>
<td><p>◇☆▼</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/山口縣.md" title="wikilink">山口縣</a></p></td>
<td><p><strong>KRY</strong></p></td>
<td><p><a href="../Page/山口放送.md" title="wikilink">山口放送</a></p></td>
<td></td>
<td><p>◇☆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/德島縣.md" title="wikilink">德島縣</a></p></td>
<td><p><strong>JRT</strong></p></td>
<td><p><a href="../Page/四國放送.md" title="wikilink">四國放送</a></p></td>
<td></td>
<td><p>◇☆</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/香川縣.md" title="wikilink">香川縣</a></p></td>
<td><p><strong>RNC</strong></p></td>
<td><p><a href="../Page/西日本放送.md" title="wikilink">西日本放送</a></p></td>
<td></td>
<td><p>◇☆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛媛縣.md" title="wikilink">愛媛縣</a></p></td>
<td><p><strong>RNB</strong></p></td>
<td><p><a href="../Page/南海放送.md" title="wikilink">南海放送</a></p></td>
<td><p><small>1979年3月1日加盟</p></td>
<td><p>◇☆</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/高知縣.md" title="wikilink">高知縣</a></p></td>
<td><p><strong>RKC</strong></p></td>
<td><p><a href="../Page/高知放送.md" title="wikilink">高知放送</a></p></td>
<td></td>
<td><p>◇☆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/福岡縣.md" title="wikilink">福岡縣</a></p></td>
<td><p><strong>KBC</strong></p></td>
<td><p><a href="../Page/九州朝日放送.md" title="wikilink">九州朝日放送</a>（KBCラジオ）</p></td>
<td><p><small>ネットワークへのライン送出が可能な設備がある</p></td>
<td><p>◆☆</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/佐賀縣.md" title="wikilink">佐賀縣</a></p></td>
<td><p><strong>NBC</strong></p></td>
<td><p><a href="../Page/長崎放送.md" title="wikilink">長崎放送</a></p></td>
<td><p><small>佐賀縣內為<a href="../Page/NBCラジオ佐賀.md" title="wikilink">NBCラジオ佐賀</a></p></td>
<td><p>◇☆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/長崎縣.md" title="wikilink">長崎縣</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/熊本縣.md" title="wikilink">熊本縣</a></p></td>
<td><p><strong>RKK</strong></p></td>
<td><p><a href="../Page/熊本放送.md" title="wikilink">熊本放送</a></p></td>
<td></td>
<td><p>◇☆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大分縣.md" title="wikilink">大分縣</a></p></td>
<td><p><strong>OBS</strong></p></td>
<td><p><a href="../Page/大分放送.md" title="wikilink">大分放送</a></p></td>
<td></td>
<td><p>◇☆</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宮崎縣.md" title="wikilink">宮崎縣</a></p></td>
<td><p><strong>MRT</strong></p></td>
<td><p><a href="../Page/宮崎放送.md" title="wikilink">宮崎放送</a></p></td>
<td></td>
<td><p>◇☆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鹿兒島縣.md" title="wikilink">鹿兒島縣</a></p></td>
<td><p><strong>MBC</strong></p></td>
<td><p><a href="../Page/南日本放送.md" title="wikilink">南日本放送</a></p></td>
<td></td>
<td><p>◇☆</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沖繩縣.md" title="wikilink">沖繩縣</a></p></td>
<td><p><strong>ROK</strong></p></td>
<td><p><a href="../Page/ラジオ沖繩.md" title="wikilink">ラジオ沖繩</a></p></td>
<td></td>
<td><p>◆</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 注釋

## 外部連結

  - [NRN](http://www.1242.com/network/)

[Category:日本媒體](../Category/日本媒體.md "wikilink")
[Category:電台網](../Category/電台網.md "wikilink")
[Category:1965年建立](../Category/1965年建立.md "wikilink")