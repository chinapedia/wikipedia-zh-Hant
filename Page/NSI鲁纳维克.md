**NSI魯納維克**（**NSÍ
Runavík**）是[法罗群岛的足球俱乐部](../Page/法罗群岛.md "wikilink")，球队创立于1957年。2003年球队首次出现在欧洲赛场。

球队未曾在法罗群岛联赛中赢得过冠军头衔，但是球队在1986年和2002年取得过法罗群岛杯的冠军，并且在1980, 1985,
1988和2004年四度闯入过杯赛的决赛。

俱乐部的球衣赛为黑色和黄色，球队体育场为可以容纳4000人的[鲁纳维克体育场](../Page/鲁纳维克体育场.md "wikilink")。

## 球队荣誉

  - **[法羅群島足球超級聯賽](../Page/法羅群島足球超級聯賽.md "wikilink")：1次**

<!-- end list -->

  -

      -
        2007年

<!-- end list -->

  - **[法羅群島盃](../Page/法羅群島盃.md "wikilink")：2次**

<!-- end list -->

  -

      -
        1986年，2002年

## 外部链接

  - [Homepage](http://www.krea.fo/guliganzfo/)

[NSÍ, Runavík](../Category/法罗群岛足球俱乐部.md "wikilink")