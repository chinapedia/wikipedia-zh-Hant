**Quadro**是由[NVIDIA設計的](../Page/NVIDIA.md "wikilink")[顯示晶片](../Page/顯示晶片.md "wikilink")，發音：，是[西班牙語中](../Page/西班牙語.md "wikilink")[正方形的意思](../Page/正方形.md "wikilink")，又表示四的倍數，意即四倍的效能。首款產品發表於1999年11月\[1\]。Quadro系列定位於專業[繪圖](../Page/繪圖.md "wikilink")[工作站領域](../Page/工作站.md "wikilink")。多數產品的核心實質上與定位於個人領域的[GeForce完全相同](../Page/GeForce.md "wikilink")，但與GeForce相比Quadro強調與行業軟體的兼容性、穩定性以及高效率。其[驅動程式對行業軟體及](../Page/驅動程式.md "wikilink")[編程介面有相應的優化](../Page/編程介面.md "wikilink")\[2\]。幾乎所有Quadro
FX系列都通過了[AutoCAD認證](../Page/AutoCAD.md "wikilink")，可完整支援線框模式的[反鋸齒](../Page/反鋸齒.md "wikilink")，集成一個專為[AutoCAD平滑線條所設計的硬體引擎](../Page/AutoCAD.md "wikilink")，高效能的Gooch[著色器](../Page/著色器.md "wikilink")。Quadro
FX還具備[UMA技術](../Page/UMA.md "wikilink")，高速的硬體[交互作業與雙面光照](../Page/人機互動.md "wikilink")、[3D動態剖切技術](../Page/3D.md "wikilink")。

一般來說Quadro的價錢比GeForce貴，所以有部分使用者軟改[GeForce顯示卡](../Page/GeForce.md "wikilink")，利用改版的驅動，他們可得到一張便宜而等價的Quadro顯示卡。

## 產品系列

  - [Quadro](../Page/Quadro.md "wikilink")：第一代產品，基於[GeForce
    256](../Page/GeForce_256.md "wikilink")，專業級圖形處理器。
  - [Quadro 2](../Page/Quadro_2.md "wikilink")：基於[GeForce
    2](../Page/GeForce_2.md "wikilink")，Quadro改良型專業級圖形處理器。
  - [Quadro DCC](../Page/Quadro_DCC.md "wikilink")：基於[GeForce
    3](../Page/GeForce_3.md "wikilink")，針對[數位內容創作](../Page/數位內容產業.md "wikilink")。
  - [Quadro 4](../Page/Quadro_4.md "wikilink")：基於[GeForce
    4](../Page/GeForce_4.md "wikilink")，專業級圖形處理器。
  - [Quadro
    FX](../Page/Quadro_FX.md "wikilink")：針對[桌上型電腦和行動](../Page/桌上型電腦.md "wikilink")[工作站的專業級圖形處理器](../Page/工作站.md "wikilink")，提供高效能的專業3D應用處理。例如多媒體，視像模擬等。2010年7月27日以後，Quadro
    FX系列重新劃分為Quadro系列。
  - [Quadro
    NVS](../Page/Quadro_NVS.md "wikilink")：專業級商用圖形處理器。提供多顯示功能，例如金融交易等。2010年12月1日以後，Quadro
    NVS系列獨立成為[NVS系列](../Page/NVIDIA_NVS.md "wikilink")。
  - [Quadro CX](../Page/Quadro_CX.md "wikilink")：專為[Adobe Creative
    Suite設計的加速器](../Page/Adobe_Creative_Suite.md "wikilink")。
  - [Quadro
    VX](../Page/Quadro_VX.md "wikilink")：专为满足[中国](../Page/中国.md "wikilink")[AutoCAD专业人士的需求而设计的高性價比圖形處理器](../Page/AutoCAD.md "wikilink")。
  - [Quadro
    Plex](../Page/Quadro_Plex.md "wikilink")：針對最複雜的重度繪圖和運算問題的專業級圖形處理器。例如製造業設計，地球科學，數位內容創建等。
  - [Quadro
    RTX](../Page/Quadro_RTX.md "wikilink")：加入即時光影追蹤[Raytracing技術](../Page/Raytracing.md "wikilink")。而且[Raytracing技術能夠令顯示卡進行即時的運算](../Page/Raytracing.md "wikilink")，渲染與光影追蹤

## 产品销售

早期NVIDIA並沒有專業顯示晶片產品線，[德國](../Page/德國.md "wikilink")[艾爾莎（Elsa）公司自行將NVIDIA的](../Page/艾爾莎科技.md "wikilink")[RIVA
TNT系列改為專業卡](../Page/RIVA_TNT.md "wikilink")，並配以自行研發的專業驅動程式進行銷售，其實力獲得NVIDIA青睞。1999年NVIDIA終於推出專門針對專業級的顯示晶片Quadro，隨後艾爾莎更將[驅動程式編寫團隊賣給NVIDIA](../Page/驅動程式.md "wikilink")，從而換取Quadro顯示卡三年的全球獨家銷售權。所以Quadro4之前的產品都是由艾爾莎推出的。另外[美國](../Page/美國.md "wikilink")[SGI（矽谷圖形）也曾銷售過Quadro顯示卡VPro系列](../Page/矽谷圖形公司.md "wikilink")（含Quadro、Quadro2
MXR、Quadro2 Pro），其同樣是艾爾莎生產。

2002年2月艾爾莎宣佈破產，隨後NVIDIA新授權了兩家公司：[美國的](../Page/美國.md "wikilink")[必恩威（PNY）與](../Page/必恩威科技.md "wikilink")[台灣的](../Page/台灣.md "wikilink")[麗臺（Leadtek）負責歐美地區與亞太地區的銷售](../Page/麗台科技.md "wikilink")。至於艾爾莎，由於其海外子公司財務相對獨立，並沒有隨總公司而破產，其中日本子公司[艾爾莎日本（Elsa
Japan）則繼續保留其在日本獨家銷售的權利](../Page/艾爾莎.md "wikilink")。這就形成了今日三家公司共同銷售Quadro產品的局面，三家公司互不進入對方所在的市場。

## 完整型號列表

## 參考連結

<references />

## 相關條目

  - [NVIDIA GeForce](../Page/GeForce.md "wikilink")
  - [NVIDIA Tesla](../Page/NVIDIA_Tesla.md "wikilink")

## 外部連結

  - [NVIDIA Quadro
    專業級繪圖解決方案](http://www.nvidia.com.tw/page/workstation.html)
  - [NVIDIA Quadro
    商業平臺解決方案](http://www.nvidia.com.tw/page/business_solutions.html)

[Category:英伟达](../Category/英伟达.md "wikilink")

1.  [Quadro誕生十周年，第十代Quadro专业方案解析](http://nvidia.e-works.net.cn/document/200903/article7716.htm)
2.  [主流NVIDIA Quadro
    FX工作站显卡横向评测](http://nvidia.e-works.net.cn/document/200811/article7024.htm)