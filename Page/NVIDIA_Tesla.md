**Tesla**是一個新的[顯示核心系列品牌](../Page/GPU.md "wikilink")，主要用於[伺服器高性能電腦運算](../Page/伺服器.md "wikilink")，用於對抗[AMD的](../Page/AMD.md "wikilink")[FireStream系列](../Page/FireStream.md "wikilink")。這是继[GeForce和](../Page/NVIDIA_GeForce.md "wikilink")[Quadro之后](../Page/NVIDIA_Quadro.md "wikilink")，第三个顯示核心商标。NVIDIA將顯示核心分為三大系列。GeForce用於提供家庭娛樂；Quadro用於專業繪圖設計；Tesla用於大規模的並聯電腦運算。

Tesla以發明家[尼古拉·特斯拉的名字命名](../Page/尼古拉·特斯拉.md "wikilink")。

## 產品系列

[NvidiaTesla.jpg](https://zh.wikipedia.org/wiki/File:NvidiaTesla.jpg "fig:NvidiaTesla.jpg")
目前，Tesla有三個系列：

  - Tesla GPU运算处理器 - 外形與普通顯示卡大致相同，C870採用[GeForce
    8顯示核心](../Page/GeForce_8.md "wikilink")，而C1060採用[GeForce
    200顯示核心](../Page/GeForce_200.md "wikilink")，不設任何顯示輸出。
  - Tesla GPU Deskside Supercomputer -
    桌面平台用，外形與[QuadroPlex相似](../Page/QuadroPlex.md "wikilink")，D870包含兩張C870运算处理器，可透過接線互联多個裝置。Tesla
    10系列中沒有相關產品。
  - Tesla GPU Server -
    [服务器用](../Page/服务器.md "wikilink")，外形與1U伺服器相似，S870包含四張C870运算处理器，而S1070包含四張C1060运算处理器，可透過接線互联多個裝置。

較早的時候，人們已意識到[GPU能運算大量數據](../Page/GPU.md "wikilink")。所以开发者通过图形语言，利用顯示核心，來进行并行计算，亦即是GPGPU（通用繪圖核心）。但开发者需要有一定程度的图形处理知識，才能發揮顯示核心效能。隨後，NVIDIA推出了[CUDA](../Page/CUDA.md "wikilink")。开发者利用[C++语言](../Page/C++语言.md "wikilink")，再通過CUDA编译器，就能利用顯核運算。开发者可忽略图形处理技術，而直接利用熟悉的C++语言。开发者和科学家，就可以利用顯示核心，研究[物理](../Page/物理.md "wikilink")、[生化和](../Page/生化.md "wikilink")[勘探等領域](../Page/勘探.md "wikilink")。

Tesla比較專注於高性能運算，并且C1060以上（G200）系列能支援双精度浮点格式。另一方面，CUDA被所有的NVIDIA顯示核心支援，包括GeForce和Quadro系列。

將來，顯示核心能普及化地，輔助[中央處理器](../Page/中央處理器.md "wikilink")，進行[视频](../Page/视频.md "wikilink")[压缩](../Page/压缩.md "wikilink")、[数据库搜索等工作](../Page/数据库.md "wikilink")。並支援更多程式語言，例如[Fortran](../Page/Fortran.md "wikilink")、[C++](../Page/C++.md "wikilink")、[JAVA和](../Page/JAVA.md "wikilink")[Python等](../Page/Python.md "wikilink")

## 完整型號列表

## 相關條目

  - [NVIDIA Quadro](../Page/NVIDIA_Quadro.md "wikilink")
  - [NVIDIA GeForce](../Page/GeForce.md "wikilink")

## 外部連結

  - [NVIDIA的Tesla主頁](http://www.nvidia.com/object/tesla_computing_solutions.html)

[Category:英伟达](../Category/英伟达.md "wikilink")