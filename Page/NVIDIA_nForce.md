**nForce**是由[NVIDIA開發的](../Page/NVIDIA.md "wikilink")[電腦](../Page/電腦.md "wikilink")[主機板](../Page/主機板.md "wikilink")[晶片組產品](../Page/晶片組.md "wikilink")，Professional系列則是供[伺服器使用的晶片組](../Page/伺服器.md "wikilink")。nForce晶片組擁有強大的效能，曾是[AMD處理器的最佳搭配](../Page/AMD.md "wikilink")。而[Intel平臺則擁有可以組建](../Page/Intel.md "wikilink")[SLI的優勢](../Page/SLI.md "wikilink")，部分高階產品的效能亦超過Intel自有晶片組。其還推出了部分整合[GeForce顯示核心的晶片組](../Page/GeForce.md "wikilink")。

但是，在[AMD收購](../Page/AMD.md "wikilink")[ATi後](../Page/ATi.md "wikilink")，擁有了自有品牌的晶片組業務；Intel推出的全新架構x5x系列晶片組後，原先的授權已不適用，而NVIDIA亦沒有自己的處理器業務，所以其晶片組產品受到排擠，往日的輝煌已不在，處於十分尷尬的境地。在2009年10月NVIDIA宣布放棄[AMD與](../Page/AMD.md "wikilink")[Intel相容晶片組的開發計畫](../Page/Intel.md "wikilink")。不過仍可能會發布新的[行動平臺晶片組以及繼續發表新的](../Page/行動平臺.md "wikilink")[nVIDIA
ION系列整合產品](../Page/nVIDIA_ION.md "wikilink")\[1\]。

## 调控軟體

在[nForce 2的時代時](../Page/nForce_2.md "wikilink")，NVIDIA就曾經推出過System
Utility軟體，它可以在[Windows作業系統中调較](../Page/Windows.md "wikilink")[BIOS中的参數](../Page/BIOS.md "wikilink")。之後從[nForce
3開始](../Page/nForce_3.md "wikilink")，直到[nForce
600](../Page/nForce_600.md "wikilink")，該軟體更名並升級為[nTune](../Page/nTune.md "wikilink")。在[AMD收購](../Page/AMD.md "wikilink")[ATI後](../Page/ATI.md "wikilink")，AMD為其[晶片組開發了OverDrive硬件调控軟體](../Page/晶片組.md "wikilink")。而NVIDIA就研發System
Tools對抗之。新的System Tools支援nForce 780i
SLI晶片組，亦支援NVIDIA最新的[ESA技術](../Page/NVIDIA_ESA.md "wikilink")。該軟體除了可以微调硬體参數外，亦可以即時監察系統的情況，例如散熱扇的轉速\[2\]。新的System
Tools包含三個軟體：

  - NVIDIA Performance Group -
    用作系統微調，例如有關[CPU的](../Page/CPU.md "wikilink")[超頻設置](../Page/超頻.md "wikilink")。
  - NVIDIA System Monitor - 用作系統監察，例如CPU的[溫度](../Page/溫度.md "wikilink")。
  - NVIDIA System Update -
    用作線上偵察新版本的[驅動程式](../Page/驅動程式.md "wikilink")，並將之更新。

## 发展历程

  - nForce
      - [nForce 1](../Page/nForce_1.md "wikilink") -
        首款產品在[2001年6月推出](../Page/2001年6月.md "wikilink")，有四個型號：nForce
        220，nForce 415，nForce 420，nForce
        620，皆為AMD平臺，但[硬體上的缺陷太多](../Page/硬體.md "wikilink")，並未取得成功
      - [nForce 2](../Page/nForce_2.md "wikilink") -
        首款產品在[2002年7月推出](../Page/2002年7月.md "wikilink")，皆為AMD平臺，[威盛電子的王者地位被取代](../Page/威盛電子.md "wikilink")。
      - [nForce 3](../Page/nForce_3.md "wikilink") -
        首款產品在[2003年9月推出](../Page/2003年9月.md "wikilink")，皆為AMD平臺，規格超前。
      - [nForce 4](../Page/nForce_4.md "wikilink") -
        首款產品在[2004年10月推出](../Page/2004年10月.md "wikilink")，其中nForce
        4 SLI Intel
        Edition為nForce首款[Intel平臺晶片組](../Page/Intel.md "wikilink")
      - [nForce 500](../Page/nForce_500.md "wikilink") -
        首款產品在[2006年3月推出](../Page/2006年3月.md "wikilink")，擁有多款效能優化技術
      - [nForce 600](../Page/nForce_600.md "wikilink") -
        首款產品在[2006年11月推出](../Page/2006年11月.md "wikilink")，nForce
        500升級版
      - [nForce 700](../Page/nForce_700.md "wikilink") -
        首款產品在[2007年12月推出](../Page/2007年12月.md "wikilink")，只有nForce
        790i原生支持[DDR3](../Page/DDR3.md "wikilink")
      - [nForce 800](../Page/nForce_800.md "wikilink") - 未推出
      - [nForce 900](../Page/nForce_900.md "wikilink") -
        首款產品在[2009年3月推出](../Page/2009年3月.md "wikilink")，目前只有一款[AMD平臺晶片組nForce](../Page/AMD.md "wikilink")
        980a SLI。

<!-- end list -->

  - nForce Professional
      - nForce Professional 2000
      - nForce Professional 3000

## 相關條目

  - [NVIDIA晶片組列表](../Page/NVIDIA晶片組列表.md "wikilink")

## 參考鏈接

## 外部連結

  - [nForce 官方網頁](http://www.nvidia.com/page/mobo.html)
  - [nForce Professional
    官方網頁](http://www.nvidia.com/page/nforce_pro.html)

[de:Nvidia nForce](../Page/de:Nvidia_nForce.md "wikilink")
[en:NForce](../Page/en:NForce.md "wikilink")
[fi:NForce](../Page/fi:NForce.md "wikilink")
[pl:NForce](../Page/pl:NForce.md "wikilink")

[Category:主板](../Category/主板.md "wikilink")
[Category:英伟达](../Category/英伟达.md "wikilink")

1.  [NVIDIA有关MCP芯片组业务的完整声明](http://news.mydrivers.com/1/146/146246.htm)
2.  [NVIDIA新一代硬件调控工具](http://news.mydrivers.com/1/96/96917.htm)