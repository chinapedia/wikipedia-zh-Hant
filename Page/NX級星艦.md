<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Enterprise_(NX-01).jpg" title="fig:Enterprise_(NX-01).jpg">Enterprise_(NX-01).jpg</a><br />
<small>NX级星舰，首艘能達曲速5級的地球太空航行器</small><br />
<br />
<a href="https://zh.wikipedia.org/wiki/File:Star_Trek_Enterprise.png" title="fig:Star_Trek_Enterprise.png">Star_Trek_Enterprise.png</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>地球星艦-{zh-hans:进取号;zh-hk:企業號;zh-tw:企業號;}- (NX-01)</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>首航：</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>授銜：</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>狀態：</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Columbia_mission_patch.png" title="fig:Columbia_mission_patch.png">Columbia_mission_patch.png</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>地球星艦哥伦比亚号 (NX-02)</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>首航：</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>授銜：</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>狀態：</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>規格</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>甲板：</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>长度：</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>船腹宽度：</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>高度：</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>吨位：</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>推進與動力：</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>護盾：</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>武器：</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>归类：</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>巡航速度：</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>最高速度：</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>工作人员：</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>附属航具：</p></td>
</tr>
</tbody>
</table>

**NX级星舰**是科幻影集《[星际迷航：进取号](../Page/星艦前傳.md "wikilink")》裡虛構的[太空航行器](../Page/星艦.md "wikilink")，在《[星际迷航](../Page/星际迷航.md "wikilink")》的世界中，NX级星舰是第一种由地球人建造，配備可达曲速5级的[曲速引擎的星艦](../Page/曲速引擎_\(星際奇旅\).md "wikilink")。NX级星舰的第一艘命名為[企業號
(NX-01)](../Page/地球星艦企業號_\(NX-01\).md "wikilink")，第二艘命名為[哥伦比亚号
(NX-02)](../Page/地球星舰哥伦比亚号_\(NX-02\).md "wikilink")。

[it:Astronavi di Star Trek\#Classe
NX](../Page/it:Astronavi_di_Star_Trek#Classe_NX.md "wikilink")

[Category:超光速航行](../Category/超光速航行.md "wikilink")
[Category:科幻](../Category/科幻.md "wikilink")
[Category:地球星舰](../Category/地球星舰.md "wikilink")