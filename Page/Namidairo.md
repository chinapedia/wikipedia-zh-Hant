「**Namidairo**」是[日本唱作女](../Page/日本.md "wikilink")[歌手](../Page/歌手.md "wikilink")[YUI於](../Page/YUI.md "wikilink")2008年2月27日推出的第十一張[單曲碟](../Page/單曲.md "wikilink")。由[STUDIOSEVEN
Recordings發行](../Page/STUDIOSEVEN_Recordings.md "wikilink")。YUI在2008年推出的首張單曲「Namidairo」，為朝日電視台連續劇《四姐妹偵探團》的主題曲，曲子是YUI出道初期寫下的作品。單曲也收入電影《Closed
Note》主題曲「LOVE\&TRUTH」的YUI Acoustic Version。

DVD收入「Namidairo」的MV及演唱會DVD《Thank you My teens》的未收錄片段。

## 收錄歌曲

  - 通常版

<!-- end list -->

  - 限量版

通常版 + DVD

## 销售纪录

| 發行         | 排行榜       | 最高位    | 首周銷量    | 總銷量 | 上榜次數 |
| ---------- | --------- | ------ | ------- | --- | ---- |
| 2008年2月27日 | Oricon 日榜 | 3      |         |     |      |
| Oricon 週榜  | 3         | 81,730 | 115,168 | 10  |      |
| Oricon 月榜  | 5         |        |         |     |      |
| Oricon 年榜  | 61        |        |         |     |      |

## 注釋

<div class="references-small">

<references />

</div>

[Category:YUI歌曲](../Category/YUI歌曲.md "wikilink")
[Category:2008年單曲](../Category/2008年單曲.md "wikilink")
[Category:朝日電視台週五晚間九點連續劇主題曲](../Category/朝日電視台週五晚間九點連續劇主題曲.md "wikilink")