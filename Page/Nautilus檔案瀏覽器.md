**Nautilus**是[GNOME桌面預設的](../Page/GNOME.md "wikilink")[檔案瀏覽器](../Page/檔案瀏覽器.md "wikilink")。它被設計為檔案瀏覽器，但同時亦可作為網頁瀏覽器及檔案檢視器。它是用來在GNOME2.0中取代GNOME1.0所使用的[Midnight
Commander](../Page/Midnight_Commander.md "wikilink")。

它曾是已消失的[Eazel公司的旗艦產品](../Page/Eazel.md "wikilink")。

## 歷史

  - 版本1.0出現在Gnome 1.4。這時它的效能仍不能使人滿意。
  - 版本2.0改為使用GTK+ 2.0編寫。
  - 版本2.2作出大幅度改變使得它更符合使用者習慣。
  - 版本2.4將桌面資料夾改為`~/Desktop`（"\~"表示使用者的家目錄）以符合[freedesktop.org標準](../Page/freedesktop.org.md "wikilink")。

在[GNOME](../Page/GNOME.md "wikilink")
2.6中，Nautilus轉變為獨立視窗模式。而傳統的介面亦可由"編輯
-\>偏好設定 -\>運作模式"內選取"總是以瀏覽視窗開啟"打開。

## 特性

Nautilus支持像浏览本地文件系统一样浏览下面的网络服务：[FTP站点](../Page/文件传输协议.md "wikilink")，Windows
[SMB共享](../Page/服务器消息区块.md "wikilink")，[Files transferred over shell
protocol](../Page/Files_transferred_over_shell_protocol.md "wikilink")，[WebDAV服务器和](../Page/WebDAV.md "wikilink")[SFTP](../Page/SFTP.md "wikilink")。这个得益于[GNOME
VFS库的支持](../Page/GNOME_VFS.md "wikilink")。

另外，Nautilus还支持书签，窗口背景，徽标，备忘和扩展脚本，并且用户可以选择是图标视图还是列表视图。它会将访问过的文件夹保存为历史，非常容易再次访问，就像浏览器一样。

Nautilus可以显示文件的预览，如[文本文件](../Page/文本文件.md "wikilink")，图像，声音或视频文件的预览。视频文件通过[Totem取得预览](../Page/Totem.md "wikilink")。声音文件当指针移到它们上面时得到预览。

对于它自己的界面，Nautilus包含了原始的由[Susan
Kare](../Page/Susan_Kare.md "wikilink")\[1\]设计的[vectorized图标](../Page/Vector_graphics.md "wikilink")。

在[Gamin库的支持下](../Page/Gamin.md "wikilink")，Nautilus将实时跟踪本地文件的修改，解决了需要手动刷新的问题。

## 参见

  - [Konqueror](../Page/Konqueror.md "wikilink")
  - [Thunar](../Page/Thunar.md "wikilink")
  - [Dolphin](../Page/Dolphin_\(KDE軟體\).md "wikilink")
  - [檔案瀏覽器列表](../Page/檔案瀏覽器列表.md "wikilink")
  - [檔案瀏覽器比較](../Page/檔案瀏覽器比較.md "wikilink")

## 參考资料

## 外部链接

  -
  - [Introduction to spatial
    Nautilus](http://www.bytebot.net/geekdocs/spatial-nautilus.html)

  - [Nautilus在Debian上的使用指南](http://wiki.debian.org/Nautilus)

[Category:GNOME](../Category/GNOME.md "wikilink")
[Category:自由文件管理器](../Category/自由文件管理器.md "wikilink")
[Category:开放源代码](../Category/开放源代码.md "wikilink")

1.