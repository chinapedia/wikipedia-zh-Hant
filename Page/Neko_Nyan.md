****是一位[日本](../Page/日本.md "wikilink")[原畫家](../Page/原畫家.md "wikilink")、[人物設計師](../Page/人物設計師.md "wikilink")。和[劇本家](../Page/劇本家.md "wikilink")[丸戶史明的組合特別著名](../Page/丸戶史明.md "wikilink")，作品《[Chocolat
～maid cafe
"curio"～](../Page/Chocolat_～maid_cafe_"curio"～.md "wikilink")》、《[Parfait
～Chocolat second
brew～](../Page/Parfait_～Chocolat_second_brew～.md "wikilink")》、《[青空下的約定](../Page/青空下的約定.md "wikilink")》為[戲畫的主要作品](../Page/戲畫_\(遊戲品牌\).md "wikilink")，與同公司的BALDR系列並稱。

## 簡介

並沒有個人網站或網誌，而且較少出現在[同人活動](../Page/同人.md "wikilink")。曾在[C71託人寄賣](../Page/Comic_Market.md "wikilink")[同人誌](../Page/同人誌.md "wikilink")，其中的後記中表示「」其實是三姊妹共用的筆名。但在《》的照片中，卻是男性。

在不同的時間下繪畫同一角色，可以出現完全不同的畫風。

## 作品列表

重製版、移植版除外。

  - 戲畫作品

      - [V.G.
        Re-birth](../Page/V.G._Re-birth.md "wikilink")（2001年9月28日發售）

      - （2002年4月5日發售）

      - [AQUA BLUE](../Page/AQUA_BLUE.md "wikilink")（2002年12月20日發售）

      - [Chocolat ～maid cafe
        "curio"～](../Page/Chocolat_～maid_cafe_"curio"～.md "wikilink")（2003年4月4日發售）

      - （2004年8月27日發售）

      - [Parfait ～Chocolat second
        brew～](../Page/Parfait_～Chocolat_second_brew～.md "wikilink")（2005年3月25日發售）

      - [青空下的約定](../Page/青空下的約定.md "wikilink")（2006年3月31日發售）

      - [Fossette - Cafe au Le Ciel Bleu
        -](../Page/Fossette_-_Cafe_au_Le_Ciel_Bleu_-.md "wikilink")（2006年12月22日發售）

      - [公主大人，請手下留情！](../Page/公主大人，請手下留情！.md "wikilink")（2007年9月28日發售）

      - [OTOME
        CRISIS](../Page/OTOME_CRISIS.md "wikilink")（）（2008年3月28日發售）

      - （2008年11月28日發售）

      - [bitter
        smile.](../Page/bitter_smile..md "wikilink")（預定2010年春季發售）

  - [Compile作品](../Page/Compile.md "wikilink")

      - 吉田學園戦記（[Disc
        Station98](../Page/Disc_Station.md "wikilink")\#9に収録）

      - （1994年12月30日發售、[Game Gear](../Page/Game_Gear.md "wikilink")）

      - （1995年11月24日發售、Game Gear）

      - （遊戲包裝。1996年12月13日、[Game Boy](../Page/Game_Boy.md "wikilink")）

      - （1998年4月2日、[SEGA Saturn](../Page/SEGA_Saturn.md "wikilink")）

      - （1998年11月27日、Game Boy）

      - （遊戲包裝。1999年12月3日、[任天堂64](../Page/任天堂64.md "wikilink")）

      - （2000年9月22日、[Game Boy
        Color](../Page/Game_Boy_Color.md "wikilink")）

  - 作品

      - （2001年4月13日發售）

  - TO BE作品

      - （2000年2月25日發售）

  - 作品

      - [緋之月](../Page/緋之月.md "wikilink")（2003年10月24日發售）

## 參見

  - [戲畫](../Page/戲畫_\(遊戲品牌\).md "wikilink")
  - [丸戶史明](../Page/丸戶史明.md "wikilink")

## 外部連結

  - [](http://nekonan.info/)（官方blog）

[Category:日本插畫家](../Category/日本插畫家.md "wikilink")
[Category:電子遊戲繪圖及原畫家](../Category/電子遊戲繪圖及原畫家.md "wikilink")