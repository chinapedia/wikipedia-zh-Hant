**Nero Burning ROM**一般简称为**Nero**，它是[Nero
AG](../Page/Nero_AG.md "wikilink")（原[Ahead
Software](../Page/Ahead_Software.md "wikilink")）开发的[Microsoft
Windows和](../Page/Microsoft_Windows.md "wikilink")[Linux下的一个流行的](../Page/Linux.md "wikilink")[光盘制作软件](../Page/光盘制作软件.md "wikilink")。\[1\]
现在这个程序通常作为套装软件的一部分，如*Nero 7 Premium Reloaded*，但仍使用Nero名称。

[Nero_Express_5.5.10.38_OEM_20170123.jpg](https://zh.wikipedia.org/wiki/File:Nero_Express_5.5.10.38_OEM_20170123.jpg "fig:Nero_Express_5.5.10.38_OEM_20170123.jpg")
一个功能受限版的Nero Burning ROM被命名为[Nero
Express来发行](../Page/Nero_Express.md "wikilink")，主要面向休闲市场。它具有简单的菜单，主要面向对电脑不太精通的人群。

## 名称

这个程序的名称其实是个[文字游戏](../Page/文字游戏.md "wikilink")。\[2\]
利用[光学](../Page/光学.md "wikilink")[介质来记录訊息通常称为](../Page/介质.md "wikilink")“[烧录](../Page/光盘烧录技术.md "wikilink")”，而压制的[CD通常称作](../Page/CD.md "wikilink")[CD-ROM](../Page/CD-ROM.md "wikilink")。[罗马皇帝](../Page/罗马皇帝.md "wikilink")[尼禄](../Page/尼禄.md "wikilink")（Nero）有时被怀疑为[罗马大火的](../Page/罗马大火.md "wikilink")[纵火者](../Page/纵火.md "wikilink")。这个[双关语在原](../Page/双关语.md "wikilink")[德语中更为明显](../Page/德语.md "wikilink")，德语中“[羅馬](../Page/羅馬.md "wikilink")”（Rome）的[德语名称为](../Page/德语外来语地名.md "wikilink")“Rom”。这个程序的标志就是在大火中的[羅馬競技場](../Page/羅馬競技場.md "wikilink")。

## Linux版本

[Linux版本的程序叫做Nero](../Page/Linux.md "wikilink") Linux，作为[Microsoft
Windows的自由](../Page/Microsoft_Windows.md "wikilink")“升级”版本于2005年3月发佈。\[3\]
这个程序复制了大多数Windows版本的特性，使用[GTK+图形工具包](../Page/GTK+.md "wikilink")。2008年2月26日后可以下载一个试用版本。

## 特性

Nero Burning ROM支持创建\[4\]:

  - [ISO](../Page/ISO_9660.md "wikilink")/[UDF数据光碟](../Page/Universal_Disk_Format.md "wikilink")
  - [Bootable discs](../Page/boot_disk.md "wikilink")
  - [Audio CD光碟](../Page/Red_Book_\(CD_standard\).md "wikilink")
  - [DVD-Video光碟](../Page/DVD-Video.md "wikilink")
  - [Blu-ray Disc](../Page/Blu-ray_Disc.md "wikilink")
  - [AVCHD视频光碟](../Page/AVCHD.md "wikilink")
  - SecurDisc光碟
  - [Disk image](../Page/Disk_image.md "wikilink") files

其他功能包括：

  - [Ripping audio CDs](../Page/CD_ripper.md "wikilink")
  - 转换音频文件到其他音频格式文件
  - 擦除可擦写光碟
  - 使用[LightScribe以及](../Page/LightScribe.md "wikilink")[LabelFlash技术在光碟上打印](../Page/LabelFlash.md "wikilink")
  - 连接到在线音乐数据库[Gracenote](../Page/Gracenote.md "wikilink")

### 映像烧录

Nero支持许多[光盘映像格式](../Page/光盘映像.md "wikilink")，包括常见的[ISO映像以及Nero专有的](../Page/ISO映像.md "wikilink")[NRG](../Page/NRG檔.md "wikilink")
[文件格式](../Page/文件格式.md "wikilink")。根据版本不同，越来越多的格式可能会被原生支持。

要使用更多的格式，特别是无损的[FLAC](../Page/FLAC.md "wikilink")、[Wavpack和](../Page/Wavpack.md "wikilink")[Shorten](../Page/Shorten.md "wikilink")，需要安装附加的程序模块。这些模块叫做[插件或](../Page/插件.md "wikilink")[编解码器](../Page/编解码器.md "wikilink")，通常是可以免费获得的。Nero
AG出售一些视频和专有音频\[5\] 插件。

## 版本历史

### Nero Burning ROM

<table>
<thead>
<tr class="header">
<th><p>版本号</p></th>
<th><p>Nero应用发布日期</p></th>
<th><p>注释</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>colspan = 3 align="center"|<strong>Nero Burning ROM 1</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1.0</p></td>
<td><p>1997</p></td>
<td><p>First release of version 1.</p></td>
</tr>
<tr class="odd">
<td><p>1.0</p></td>
<td><p>1997</p></td>
<td><p>Last release of version 1.</p></td>
</tr>
<tr class="even">
<td><p>colspan = 3 align="center"|<strong>Nero Burning ROM 2</strong>[6]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2.0.0.0</p></td>
<td><p>1997</p></td>
<td><p>First release of version 2.</p></td>
</tr>
<tr class="even">
<td><p>2.0.1.5</p></td>
<td><p>1997</p></td>
<td><p>Last release of version 2.</p></td>
</tr>
<tr class="odd">
<td><p>colspan = 3 align="center"|<strong>Nero Burning ROM 3</strong>[7]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>3.0.0.0</p></td>
<td><p>1997</p></td>
<td><p>First release of version 3.</p></td>
</tr>
<tr class="odd">
<td><p>3.0.7.1</p></td>
<td><p>1998</p></td>
<td><p>Last release of version 3.</p></td>
</tr>
<tr class="even">
<td><p>colspan = 3 align="center"|<strong>Nero Burning ROM 4</strong>[8]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>4.0.0.2</p></td>
<td><p>1999</p></td>
<td><p>First release of version 4.<br />
CD-ROM UDF and UDF/ISO (Bridge) support added. Nero Cover Editor.</p></td>
</tr>
<tr class="even">
<td><p>4.0.3.0</p></td>
<td><p>1999</p></td>
<td><p>Last version for Windows 3.x (requires Win32s).</p></td>
</tr>
<tr class="odd">
<td><p>4.0.9.1</p></td>
<td></td>
<td><p>Last release of version 4.<br />
New features include twin VQ Encoding/Decoding, audio echo filter, and audio drag &amp; drop implemented for Windows 2000.</p></td>
</tr>
<tr class="even">
<td><p>colspan = 3 align="center"|<strong>Nero Burning ROM 5</strong>[9]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5.0.0.3</p></td>
<td><p>2000</p></td>
<td><p>First release of version 5.</p></td>
</tr>
<tr class="even">
<td><p>5.5</p></td>
<td></td>
<td><p>Major update. New features included Nero Wave Editor, Advanced Nero Cover Designer, Nero MPEG1 Video Encoder, VCD/SVCD Menu Creation, Audio Plug-In Interface, Embedded Nero API (Application Program Interface), Nero Toolkit.</p></td>
</tr>
<tr class="odd">
<td><p>5.5.4.0</p></td>
<td></td>
<td><p>First version to support <a href="../Page/DVD.md" title="wikilink">DVD</a> burning.[10]</p></td>
</tr>
<tr class="even">
<td><p>5.5.9.0</p></td>
<td></td>
<td><p>The first version with codec plug-in support (for example, writing FLAC, WavPack, MP4 etc. files to Audio CDs), Windows XP Support.</p></td>
</tr>
<tr class="odd">
<td><p>5.5.10.56</p></td>
<td></td>
<td><p>Last release of version 5.[11] Last version for Windows 95A[12]. Nero 6 requires Windows 95B/98 or later[13].</p></td>
</tr>
<tr class="even">
<td><p><strong>Nero Burning ROM 6</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>6.0.0.9</p></td>
<td></td>
<td><p>First release of version 6.<br />
Early versions of Nero version 6 would burn only data DVDs using the <a href="../Page/ISO_9660.md" title="wikilink">ISO 9660</a> file system. Though DVD drives seemed to have no difficulty reading single-layer discs, compatibility with dual-layer discs was problematic.</p></td>
</tr>
<tr class="even">
<td><p>6.3.1.25</p></td>
<td></td>
<td><p>Last version for Windows NT 4.0.[14]</p></td>
</tr>
<tr class="odd">
<td><p>6.6.0.8</p></td>
<td></td>
<td><p>新增支持<a href="../Page/LightScribe.md" title="wikilink">LightScribe</a>。</p></td>
</tr>
<tr class="even">
<td><p>6.6.1.15c</p></td>
<td></td>
<td><p>Last release of version 6.</p></td>
</tr>
<tr class="odd">
<td><p><strong>Nero Burning ROM 7</strong>[15]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>7.0.1.2</p></td>
<td></td>
<td><p>First release of version 7.<br />
For unknown reasons, Nero 7 Ultra Edition Enhanced for North America has no <a href="../Page/LabelFlash.md" title="wikilink">LabelFlash</a> or <a href="../Page/DiscT@2.md" title="wikilink">DiscT@2</a> support as Nero 7 Premium for Europe does.</p></td>
</tr>
<tr class="odd">
<td><p>7.2.7.0</p></td>
<td></td>
<td><p>Last Version For Windows 98/ME.</p></td>
</tr>
<tr class="even">
<td><p>7.5.1.1</p></td>
<td></td>
<td><p>Introduced support for <a href="../Page/LightScribe.md" title="wikilink">LightScribe</a> and <a href="../Page/Blu-ray_Disc.md" title="wikilink">Blu-ray Disc</a> devices.</p></td>
</tr>
<tr class="odd">
<td><p>7.5.7.0</p></td>
<td></td>
<td><p>支持运行于<a href="../Page/Windows_Vista.md" title="wikilink">Windows Vista上</a>。</p></td>
</tr>
<tr class="even">
<td><p>7.7.5.1</p></td>
<td></td>
<td><p>Officially certified for <a href="../Page/Windows_Vista.md" title="wikilink">Windows Vista</a>.<br />
Fixed all prior reported flaws, updated Booktype support, and added further options for high definition discs.</p></td>
</tr>
<tr class="odd">
<td><p>7.8.5.0</p></td>
<td></td>
<td><p>Bugfix release.</p></td>
</tr>
<tr class="even">
<td><p>7.10.1.1</p></td>
<td></td>
<td><p>Bugfix release.</p></td>
</tr>
<tr class="odd">
<td><p>7.11.10.0b</p></td>
<td></td>
<td><p>Bugfix release.</p></td>
</tr>
<tr class="even">
<td><p>7.11.10.0c</p></td>
<td></td>
<td><p>Windows 7 compatibility update.</p></td>
</tr>
<tr class="odd">
<td><p><strong>Nero Burning ROM 8</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>8.1.1.0</p></td>
<td></td>
<td><p>First release of version 8.</p></td>
</tr>
<tr class="odd">
<td><p>8.2.8.0</p></td>
<td></td>
<td><p>新增支持<a href="../Page/DVD-RW_DL.md" title="wikilink">DualLayer DVD-RW媒体包括</a><a href="../Page/Layer_Jump_Recording_(LJR).md" title="wikilink">layer jump功能</a>。</p></td>
</tr>
<tr class="even">
<td><p>8.3.2.1</p></td>
<td></td>
<td><p>Minor bug fixes. Improved playback compatibility for <a href="../Page/Blu-ray_Disc.md" title="wikilink">Blu-ray</a>.[16]</p></td>
</tr>
<tr class="odd">
<td><p>8.3.6.0</p></td>
<td></td>
<td><p>Minor bug fixes. NeroShowTime gained support for <a href="../Page/DirectX_Video_Acceleration.md" title="wikilink">DXVA</a> 2.0 and ATI <a href="../Page/Unified_Video_Decoder.md" title="wikilink">UVD</a>.</p></td>
</tr>
<tr class="even">
<td><p>8.3.13.0</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>8.3.13.0a</p></td>
<td></td>
<td><p>Prior release of version 8.</p></td>
</tr>
<tr class="even">
<td><p>8.3.20.0</p></td>
<td><p>[17]</p></td>
<td><p>Latest release of version 8. Last version For <a href="../Page/Windows_2000.md" title="wikilink">Windows 2000</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>Nero Burning ROM Express 9</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>3.2.12.1</p>
<p>E</p></td>
<td></td>
<td><p>First release of version 9. First release to support Windows 7.</p></td>
</tr>
<tr class="odd">
<td><p>9.0.9.4d</p></td>
<td></td>
<td><p>Prior release of version 9.</p></td>
</tr>
<tr class="even">
<td><p>9.2.6.0</p></td>
<td></td>
<td><p>Prior release of version 9.</p></td>
</tr>
<tr class="odd">
<td><p>9.4.13.2</p></td>
<td></td>
<td><p>Prior release of version 9.</p></td>
</tr>
<tr class="even">
<td><p>9.4.13.2c</p></td>
<td></td>
<td><p>Prior release of version 9.</p></td>
</tr>
<tr class="odd">
<td><p>9.4.13.2d</p></td>
<td></td>
<td><p>Prior release of version 9.</p></td>
</tr>
<tr class="even">
<td><p>9.4.26.0</p></td>
<td></td>
<td><p>Prior release of version 9. First release officially compatible with Windows 7.</p></td>
</tr>
<tr class="odd">
<td><p>9.4.39.0</p></td>
<td></td>
<td><p>Prior release of version 9.</p></td>
</tr>
<tr class="even">
<td><p>9.4.44.0</p></td>
<td></td>
<td><p>Prior release of version 9.</p></td>
</tr>
<tr class="odd">
<td><p>9.4.44.0b</p></td>
<td></td>
<td><p>Latest release of version 9.</p></td>
</tr>
<tr class="even">
<td><p><strong>Nero Burning ROM 10</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>10.0.10800</p></td>
<td></td>
<td><p>First online release of version 10 - listed in the About Dialog as version 10.0.11100.10.100</p></td>
</tr>
<tr class="even">
<td><p>10.0.13100</p></td>
<td></td>
<td><p>First retail release of version 10</p></td>
</tr>
<tr class="odd">
<td><p>10.5.10300</p></td>
<td></td>
<td><p>First version 10 update - listed in the About Dialog as version 10.2.11000.12.100</p></td>
</tr>
<tr class="even">
<td><p>unknown</p></td>
<td></td>
<td><p>Latest Control Center Online Update - listed in the About Dialog as version 10.2.11600.20.100</p></td>
</tr>
<tr class="odd">
<td><p>unknown</p></td>
<td></td>
<td><p>Latest Control Center Online Update - listed in the About Dialog as version 10.2.12000.25.100</p></td>
</tr>
<tr class="even">
<td><p>10.6.10600</p></td>
<td></td>
<td><p>Latest Control Center Online Update - listed in the About Dialog as version 10.6.10600.3.100</p></td>
</tr>
<tr class="odd">
<td><p>10.6.10700</p></td>
<td></td>
<td><p>Latest Control Center Online Update - listed in the About Dialog as version 10.6.10700.5.100</p></td>
</tr>
<tr class="even">
<td><p><strong>Nero Burning ROM 11</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11.0.10700</p></td>
<td></td>
<td><p>First online release of version 11.</p></td>
</tr>
<tr class="even">
<td><p>11.0.12200</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11.0.10500</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

**Note:** Although Nero AG appears to no longer maintain a history of
older versions on their website, release notes are archived by several
third-party sites.\[18\]\[19\]\[20\]\[21\]

### Nero Linux

<table>
<thead>
<tr class="header">
<th><p>版本号</p></th>
<th><p>发布日期</p></th>
<th><p>注释</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>'''Nero Linux</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1.0</p></td>
<td><p>2005年3月12日[22]</p></td>
<td><p>第一版</p></td>
</tr>
<tr class="odd">
<td><p><strong>Nero Linux 2</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2.1.0.4b</p></td>
<td></td>
<td><p>Nero Linux 第2版的最后一版</p></td>
</tr>
<tr class="odd">
<td><p><strong>Nero Linux 3</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>3.0.0.0</p></td>
<td><p>2007年5月24日</p></td>
<td><p>GTK+ 2接口，支持Unicode，<a href="../Page/蓝光.md" title="wikilink">蓝光烧录</a>。</p></td>
</tr>
<tr class="odd">
<td><p>3.0.2.1</p></td>
<td><p>2007年11月6日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>3.1.1.0</p></td>
<td><p>2008年2月13日</p></td>
<td><p>基于NeroAPI 7.20版，一些命令行参数。</p></td>
</tr>
<tr class="odd">
<td><p>3.5</p></td>
<td><p>2008年3月[23]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>3.5.2.3</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3.5.3.1</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>Nero Linux 4</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>4.0.0.0</p></td>
<td><p>[24]</p></td>
<td><p>Current Linux release.</p></td>
</tr>
<tr class="even">
<td><p>4.0.0.0b</p></td>
<td><p>[25]</p></td>
<td><p>Latest Linux update</p></td>
</tr>
</tbody>
</table>

## 参见

## 注释和参考

## 外部链接

  - [Nero AG official website](http://www.nero.com/)
  - [Archive of Nero
    versions](http://www.afterdawn.com/software/cdr_software/cdr_applications/nero.cfm)
  - [Nero Burning Rom
    Alternatives](http://www.neowin.net/forum/index.php?showtopic=421355)
  - [nrg2iso](http://gregory.kokanosky.free.fr/v4/linux/nrg2iso.en.html)
    – [GPL](../Page/GPL.md "wikilink") Linux utility for converting
    [NRG](../Page/NRG_\(file_format\).md "wikilink") images to [ISO
    images](../Page/ISO_image.md "wikilink")
  - [Open source alternatives to Nero Burning
    ROM](http://www.osalt.com/nero)（see also [CD burning
    software](../Page/CD_burning_software.md "wikilink")）

[Category:光盘创作软件](../Category/光盘创作软件.md "wikilink")
[Category:共享软件](../Category/共享软件.md "wikilink")
[Category:Windows
CD/DVD刻录软件](../Category/Windows_CD/DVD刻录软件.md "wikilink")
[Category:Linux CD/DVD刻录软件](../Category/Linux_CD/DVD刻录软件.md "wikilink")

1.

2.

3.

4.

5.  尤其是多声道的，如[杜比数字
    5.1以及](../Page/杜比数字.md "wikilink")[mp3pro](../Page/mp3pro.md "wikilink")。

6.

7.

8.
9.
10. [Version
    History](https://web.archive.org/web/20020603145307/http://www.nero.com/en/driver.asp)

11. [Nero – Nero 5 -
    Update](http://www.nero.com/eng/downloads-nero5-update.php)

12. [Nero 5 System
    Requirements](http://www.nero.com/eng/support-nero5-system-requirements.html)

13. [Nero 6 System
    Requirements](http://www.nero.com/enu/support-nero6-system-requirements.html)

14. <http://www.nero.com/eng/downloads-nero6-nt.php>

15.

16.

17.

18.
19.
20.

21.

22.

23.

24.

25.