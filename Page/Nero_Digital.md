**Nero
Digital**是一套相容於[MPEG-4的音訊](../Page/MPEG-4.md "wikilink")、視訊壓縮編解碼技術軟體（[codec](../Page/codec.md "wikilink")），這套軟體也整合了一套[DVD的抓截](../Page/DVD.md "wikilink")（）工具軟體：Recode
2。Nero Digital是[德國](../Page/德國.md "wikilink")[Nero
AG公司與](../Page/Nero_AG.md "wikilink")[法國](../Page/法國.md "wikilink")[Ateme公司合作研發而成](../Page/Ateme.md "wikilink")。Recode是Nero公司跨進DVD光碟備份市場的第一套軟體，這套軟體因易學易用易上手而獲得正面評價。

Nero Digital能夠以[MPEG-4 Part
14標準來產生串流媒體檔](../Page/MPEG-4_Part_14.md "wikilink")，副檔名為**.mp4**，檔案內可包含兩種音訊、視訊編解碼：

  - [ASP](../Page/MPEG-4_Part_2.md "wikilink") - 是MPEG-4 Part
    2標準所制訂規範的二十種型態（Profile）中的一種

<!-- end list -->

  - [AVC](../Page/H.264/MPEG-4_AVC.md "wikilink") - 也稱為MPEG-4 Part
    10或H.264

<!-- end list -->

  - LC [AAC](../Page/Advanced_Audio_Coding.md "wikilink") -
    已廣泛使用的編解碼標準，標準由[MPEG-4 Part
    3所定義](../Page/MPEG-4_Part_3.md "wikilink")

<!-- end list -->

  - HE AAC - 亦由MPEG-4 Part 3所定義，有時被誤稱成accPlus

這些編解碼也合乎[ISO](../Page/國際標準化組織.md "wikilink")／[IEC標準](../Page/国际电工委员会.md "wikilink")，不過這標準並不包含字幕、章節等資訊的定義。用Nero
Digital所產出的串流視訊能在軟體的[媒體播放軟體](../Page/媒体播放器.md "wikilink")（如Nero公司自屬的[Nero
ShowTime](../Page/Nero_ShowTime.md "wikilink")），也能在一些獨立使用的硬體播放器中播放使用，不過有許多使用反應表示：用Nero編製成的影劇章節與字幕容易發生問題。

另外，Recode無法抓截加密過的DVD電影內容，然而卻可以匯入[解密過的DVD圖像來進行編製](../Page/DVD_Decrypter.md "wikilink")，Nero
Digital也沒有將其編解碼技術以相同於[DirectShow或](../Page/DirectShow.md "wikilink")[VfW的獨立模組方式釋出](../Page/Video_for_Windows.md "wikilink")，此外為了避免Nero
Digital影響一般用途的視訊編輯軟體的銷售，所以Nero Digital沒有隨附視訊編輯軟體。

Nero Digital的視訊編解碼技術與其他視訊壓縮技術相較下有如下的特點：

  - 在[2003年度Doom9.net網站的編解碼軟體比較](https://web.archive.org/web/20050404152416/http://www.doom9.org/index.html?%2Fcodecs-203-1.htm)中，ASP編解碼是所有編解碼中最快速的，不過也大幅犧牲了播放品質。

<!-- end list -->

  - 在[2004年度Doom9.net網站的編解碼軟體比較](https://web.archive.org/web/20050405073849/http://www.doom9.org/index.html?%2Fcodecs-104-1.htm)中，Nero
    Digital的AVC編解碼被推舉為評比中的贏家，而Nero Digital（簡稱：ND）的ASP編碼則尚未完整。

<!-- end list -->

  - 在[2005年度Doom9.net網站的編解碼軟體比較](https://web.archive.org/web/20160304213912/http://www.doom9.org/index.html?%2Fcodecs-105-1.htm)中，ASP編解碼依舊是最快速的，不過品質已較過往提升，但依舊不夠好。而AVC編解碼則是整體評比中的第二名。

<!-- end list -->

  - 在2006年5月，Nero
    Digital獲得[C't雜誌／視訊編解碼品質評比](https://web.archive.org/web/20050512021422/http://www.heise.de/ct/05/10/146/)的贏家推薦，其他評選的編解碼軟體如VideoSoft的[x264](../Page/x264.md "wikilink")、[MainConcept的](../Page/MainConcept.md "wikilink")[Sorenson等](../Page/Sorenson.md "wikilink")。

<!-- end list -->

  - Nero Digital AVC也贏得[Movie Metric Benchmark
    challenge（電影公制基準挑戰）](http://forum.doom9.org/showthread.php?s=&threadid=90784)，在相同資料率（bitrate）下，Nero
    Digital AVC比[DivX高出](../Page/DivX.md "wikilink")1dB（分貝）的品質增益。

## 參見

  - [多媒體編解碼軟體列表](../Page/編解碼器列表.md "wikilink")

## 外部連結

[Category:視頻編解碼器](../Category/視頻編解碼器.md "wikilink")
[Category:音訊編解碼器](../Category/音訊編解碼器.md "wikilink")