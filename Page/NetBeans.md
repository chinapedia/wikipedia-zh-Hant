**NetBeans**是由[昇陽電腦](../Page/昇陽電腦.md "wikilink")（Sun
Microsystems）建立的開放原始碼的軟體開發工具，是一個開發框架，可擴展的開發平台，可以用於[Java](../Page/Java.md "wikilink")，[C語言](../Page/C語言.md "wikilink")／[C++](../Page/C++.md "wikilink")，[PHP](../Page/PHP.md "wikilink")，[HTML5等程式的開發](../Page/HTML5.md "wikilink")，本身是一個開發平台，可以通過擴展插件來擴展功能。

在NetBeans Platform平台中，應用軟體是用一系列的軟體模組（modular software
components）建構出來。而這些模組是一個jar檔（Java archive
file）它包含了一組[Java程式的類別而它們實作全依據依NetBeans定義了的公開介面以及一系列用來區分不同模組的](../Page/Java.md "wikilink")[定義描述檔（Manifest
file）](../Page/清单文件.md "wikilink")。有賴於模組化帶來的好處，用模組來建構的應用程式可只要加上新的模組就能進一步擴充。由於模組可以獨立地進行開發，所以由NetBeans平台

開發出來的應用程式就能利用著第三方軟體，非常容易及有效率地進行擴充。

## 歷史

NetBeans是一個始於1997年的Xelfi計劃，本身是捷克[布拉格查理大學](../Page/布拉格.md "wikilink")[Charles
University的數學及物理學院的學生計畫](../Page/Charles_University.md "wikilink")。此計劃延伸而成立了一家公司進而發展這個商用版本的NetBeans
IDE，直到1999年昇陽電腦買下此公司。昇陽電腦於次年（2000年）的六月將NetBeans
IDE開放為公開源碼，直到現在NetBeans的社群依然持續增長，而且更多個人及企業使用並開發NetBeans作為程式開發的工具。\[1\]

NetBeans IDE 6.0延伸了原來[Java
EE的特質](../Page/Java_EE.md "wikilink")。NetBeans C/C++
Pack更支援C/C++的編程計畫。現行穩定版本對[PHP](../Page/PHP.md "wikilink")、[Ruby及其它](../Page/Ruby.md "wikilink")[腳本語言的支持已非常成熟](../Page/腳本語言.md "wikilink")。2009年，Sun推出Kenai雲項目，並將其整合到Netbeans中，加強了團隊開發的交互性。

## NetBeans平台

NetBeans平台是一種可重複使用的框架用於簡化其他桌面應用程式的開發。當基於NetBeans平台的應用被執行，平台主要類別的main方法便會被執行。可用的模組會被放置在存儲器中，並且開始執行任務。通常模組會只在被需要時，其代碼才會被裝進內存。

應用程式能動態安裝模組。任何應用程式能包括更新模組，允許用戶申請下載的應用程式升級和加入新功能。這樣安裝，升級以及新發並行不必迫使用戶每次再下載整個應用程式。

整個Netbeans平台提供對桌面應用程式常用的服務，允許開發者集中於僅限於他的應用程式的邏輯設計。其中NetBeans平台的主要特徵是：

  - 用戶界面管理User interface management（例如選單和工具條）
  - 用戶設定管理User settings management
  - 存儲管理Storage management（保留和裝任何種類數據）
  - 視窗管理Window management
  - 精靈框架Wizard framework（一步一步支援對話框）

## 參考文獻

## 外部链接

  - [官方網頁](https://netbeans.org/)
  - Introduction to [NetBeans
    Platform](https://web.archive.org/web/20071022025726/http://www.netbeans.org/products/platform/)
    -- [NetBeans Mobility
    Pack](https://web.archive.org/web/20071021111434/http://www.netbeans.org/products/mobility/)
    -- [NetBeans
    Profiler](https://web.archive.org/web/20071011061405/http://www.netbeans.org/products/profiler/)
    -- [NetBeans Enterprise
    Pack](https://web.archive.org/web/20100306160603/http://netbeans.org/products/enterprise/)
    -- [NetBeans C/C++
    Pack](https://web.archive.org/web/20071023065216/http://www.netbeans.org/products/cplusplus/)
  - [NetBeans Feature
    Demos](https://web.archive.org/web/20090215204457/http://www.netbeans.org/kb/articles/flash.html)（Flash
    movies）
  - [Build Your RCP Application on the NetBeans
    Platform](https://netbeans.org/features/platform/) + [RCP
    Tutorials](http://platform.netbeans.org/tutorials/index.html)
  - [NetBeans Programming
    Tutorials](https://netbeans.org/kb/kb.html)：Swing GUIs, Web and
    Enterprise, Mobility, Profiling...
  - [NetBeans GUI Builder
    "Matisse"](https://web.archive.org/web/20090308114253/http://www.netbeans.org/kb/50/quickstart-gui.html)
  - [Blogs about NetBeans](http://www.planetnetbeans.org/)
  - [NetBeans plugin
    catalogue](https://web.archive.org/web/20080511224500/http://www.netbeans.org/catalogue/)
    and [NB Extras](http://www.nbextras.org) to extend IDE functionality
  - [NetbeansIDA-Interactive
    DisassemblerToDecompiler](http://www.idapython.narod.ru/) Netbeans
    module with [Interactive
    Disassembler](../Page/Interactive_Disassembler.md "wikilink")
  - [Netbeans Archived User
    Forums](https://web.archive.org/web/20061207061427/http://www.techienuggets.com/)

[Category:2000年软件](../Category/2000年软件.md "wikilink")
[Category:自由跨平台軟體](../Category/自由跨平台軟體.md "wikilink")
[Category:自由HTML编辑器](../Category/自由HTML编辑器.md "wikilink")
[Category:自由UML工具](../Category/自由UML工具.md "wikilink")
[Category:自由電腦程式設計工具](../Category/自由電腦程式設計工具.md "wikilink")
[Category:用Java編程的自由軟體](../Category/用Java編程的自由軟體.md "wikilink")
[Category:自由整合開發環境](../Category/自由整合開發環境.md "wikilink")
[Category:Linux集成开发环境](../Category/Linux集成开发环境.md "wikilink")
[Category:Java开发工具](../Category/Java开发工具.md "wikilink")
[Category:Java平台](../Category/Java平台.md "wikilink")
[Category:昇陽電腦軟體](../Category/昇陽電腦軟體.md "wikilink")

1.