**NetHack**是一款最初在1987年發佈的[Roguelike](../Page/Roguelike.md "wikilink")[單人遊戲](../Page/單人遊戲.md "wikilink")。它繼承了Hack（1985年）及更早的Rogue（1980年）。

遊戲名字中的「網路」（Net）是指開發過程通過[互联网合作完成](../Page/互联网.md "wikilink")。\[1\]

而“Hack”是指[角色扮演游戏的特点](../Page/電子角色扮演遊戲.md "wikilink")——战斗和探索（[hack and
slash](../Page/hack_and_slash.md "wikilink")）。

玩家扮演一个地下城探险者寻找Yendor的项链。

## 历史

1987年7月28日，Mike
Stephenson發行了原始NetHack。直到1989年，同樣在7月發行的3.0版本，開始凝聚了一班人組成開發團隊。NetHack是[開源軟件](../Page/開源軟件.md "wikilink")，今時今日，成為了其中一款最古老而仍在活躍開發的遊戲，由於是[開源軟件](../Page/開源軟件.md "wikilink")，所以很多[除錯和新內容開發工作都會有同樣身為玩家的志願者參與](../Page/除錯.md "wikilink")。

## 玩法

玩家需要选择自己所扮演的角色并指定性别、[种族](../Page/种族.md "wikilink")、[职业和](../Page/职业.md "wikilink")[阵营](../Page/阵营.md "wikilink")，或者选择让系统随机产生一个角色。游戏者可以扮演经典奇幻角色，比如[骑士](../Page/骑士.md "wikilink")、[野蛮人](../Page/野蛮人.md "wikilink")、[巫师](../Page/巫师.md "wikilink")、[游侠](../Page/游侠.md "wikilink")、[女武神](../Page/女武神.md "wikilink")、[僧侣和](../Page/僧侣.md "wikilink")[武士](../Page/武士.md "wikilink")，也可以选择一些比较少见的角色，诸如[考古学家](../Page/考古学家.md "wikilink")、[游客和](../Page/游客.md "wikilink")[洞穴人](../Page/洞穴人.md "wikilink")。玩家的角色和阵营决定了其在游戏中需要侍奉的[神灵](../Page/神灵.md "wikilink")。

玩家的目标就是在地下城的最底层取得Yendor的项链并将其供奉给自己的神灵。作为回报，角色会成为[不朽的](../Page/不朽.md "wikilink")[半神](../Page/半神.md "wikilink")。此外，一系列的支线任务也必须完成，其中包括各职业所特有的任务。

游戏开始的时候，玩家通常会有一只[宠物伴随](../Page/宠物.md "wikilink")，一般是[小猫或者](../Page/猫.md "wikilink")[小狗](../Page/小狗.md "wikilink")，但骑士会有一匹[马以及一副](../Page/马.md "wikilink")[马鞍](../Page/马鞍.md "wikilink")。

游戏中的大部分怪物都可以用[魔法或喂食等方式转化为宠物](../Page/魔法.md "wikilink")。

### 地下城等级

NetHack的地下城大约总共有50层，其中大多数都是玩家进入时随机生成的。所有层基本上都有一个向上或向下的通道（包括[楼梯](../Page/楼梯.md "wikilink")、[梯子](../Page/梯子.md "wikilink")、[陷阱或者其他形式](../Page/陷阱.md "wikilink")）、一些用走廊连接起来的房间，房间里面可能有祭坛、商店、[喷泉](../Page/喷泉.md "wikilink")、陷阱甚至水槽。一些比较特别的层有固定的形式。游戏有几个分支路线，包括一个[推箱子游戏和](../Page/推箱子.md "wikilink")[弗拉德之塔](../Page/弗拉德三世.md "wikilink")。

### 物品

NetHack包含各式的物品：近程及遠程[武器](../Page/武器.md "wikilink")、保護角色的[盔甲](../Page/盔甲.md "wikilink")、可供[閱讀的](../Page/閱讀.md "wikilink")[卷軸及](../Page/卷軸.md "wikilink")[魔法書](../Page/魔法書.md "wikilink")、可供[飲用的](../Page/飲.md "wikilink")[藥劑](../Page/藥劑.md "wikilink")，以及多樣化的[工具](../Page/工具.md "wikilink")。

NetHack的其中一個特色是充滿著不明的物品，例如玩家只能在一瓶新發現的藥劑上得到簡單的描述，像是“紫色的藥劑”。玩家能以不同的方式辨別其真正作用，而最簡單但相當危險的方式——就是直接飲用。

## 界面

NetHack原本没有[图形界面](../Page/图形界面.md "wikilink")。

其他图形界面包括使用[等角投影](../Page/等角投影.md "wikilink")（即45度视角）的[Falcon's
Eye和](../Page/Falcon's_Eye.md "wikilink")[Vulture's
Eye](../Page/Vulture's.md "wikilink")，[三维渲染的](../Page/三维渲染.md "wikilink")[noegnud](../Page/noegnud.md "wikilink")；以及其他一些。

## 移植

大部分类似计算机的平台都有这个游戏。[任天堂DS](../Page/任天堂DS.md "wikilink")\[2\]\[3\]、[PlayStation
Portable](../Page/PlayStation_Portable.md "wikilink")\[4\]、[Tapwave
Zodiac](../Page/Tapwave_Zodiac.md "wikilink")\[5\]、[GP2X](../Page/GP2X.md "wikilink")\[6\]、[Windows
Mobile](../Page/Windows_Mobile.md "wikilink")\[7\]、[Nokia
N800](../Page/Nokia_N800.md "wikilink")\[8\]、[Android手机](../Page/Android.md "wikilink")、[苹果公司的](../Page/苹果公司.md "wikilink")[iPhone](../Page/iPhone.md "wikilink")、[iPod
touch和](../Page/iPod_touch.md "wikilink")[iPad](../Page/iPad.md "wikilink")。

[Slash'EM和SporkHack作为稳定版活跃的开发](../Page/Slash'EM.md "wikilink")。

## 参考文献

<references/>

## 外部連結

  - [NetHack首頁](http://www.nethack.org/)
      - [A Guide to the Mazes of Menace (Guidebook for
        NetHack)](http://www.nethack.org/v343/Guidebook.html) by [Eric
        S. Raymond](../Page/Eric_S._Raymond.md "wikilink")
  - [Hall of Fame -
    NetHack](https://web.archive.org/web/20081021070821/http://archive.gamespy.com/legacy/halloffame/nethack_a.shtm)
    位于 [GameSpy](../Page/GameSpy.md "wikilink")
  - [“最佳游戏”"The Best Game
    Ever"](http://www.salon.com/tech/feature/2000/01/27/nethack/) 位于
    [Salon.com](../Page/Salon.com.md "wikilink")
  - [NetHack服务器](http://alt.org/nethack/)
  - [收藏：NetHack
    MS-DOS程序](https://web.archive.org/web/20090122173829/http://se.aminet.net/pub/games/nethack/Msdos/NH_older/)

[Category:1987年电子游戏](../Category/1987年电子游戏.md "wikilink")
[Category:冒险遊戲](../Category/冒险遊戲.md "wikilink")
[Category:自由软件](../Category/自由软件.md "wikilink")
[Category:跨平台软件](../Category/跨平台软件.md "wikilink")
[Category:开源游戏](../Category/开源游戏.md "wikilink")
[Category:Linux游戏](../Category/Linux游戏.md "wikilink") [Category:Mac
OS游戏](../Category/Mac_OS游戏.md "wikilink")
[Category:Windows游戏](../Category/Windows游戏.md "wikilink")
[Category:MacOS遊戲](../Category/MacOS遊戲.md "wikilink")
[Category:Roguelike游戏](../Category/Roguelike游戏.md "wikilink")

1.
2.
3.
4.
5.
6.  [Nethack (graphical) -
    wiki.gp2x.org](http://wiki.gp2x.org/wiki/Nethack_\(graphical\))
7.
8.