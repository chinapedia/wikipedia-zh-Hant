**Netscape
5**（開發代號**Gromit**）是一個於1998年建立的[網路套件](../Page/網路套件.md "wikilink")，作為網景系列第五版上市。原本預定交給Mozilla組織繼續開發但是本身問題過多所以中途被放棄，因此形成缺號。

Netscape
5有兩個釋出的Pre-alpha版（内部测试之前的版本），一个是基于[Mariner](../Page/Mariner_\(排版引擎\).md "wikilink")[排版引擎的版本](../Page/排版引擎.md "wikilink")，一个則是基于[Gecko](../Page/Gecko.md "wikilink")（NGLayout）。

此外，此程式碼後來被稱為「Mozilla Classic」。

## 外部連結

  - [What happened to
    Netscape 5.0?](http://sillydog.org/netscape/kb/communicator5.php)

[en:Netscape (web browser)\#Netscape Communicator 5.0
(canceled)](../Page/en:Netscape_\(web_browser\)#Netscape_Communicator_5.0_\(canceled\).md "wikilink")

[Category:Netscape](../Category/Netscape.md "wikilink")
[Category:1998年軟體](../Category/1998年軟體.md "wikilink")