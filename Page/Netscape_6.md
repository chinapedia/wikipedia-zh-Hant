**Netscape
6**是網景通訊的[跨平台](../Page/跨平台.md "wikilink")[網路套件](../Page/網路套件.md "wikilink")，因為[Netscape
5實質上被廢棄](../Page/Netscape_5.md "wikilink")，所以6是承接之前的[網景通訊家族](../Page/網景通訊家族.md "wikilink")4.8版。此版本和之後的[Netscape
7單純是修改自](../Page/Netscape_7.md "wikilink")[Mozilla
Suite](../Page/Mozilla_Suite.md "wikilink")。

整個套件包含Navigator（[網頁瀏覽器](../Page/網頁瀏覽器.md "wikilink")）、[Netscape Mail &
Newsgroups](../Page/Netscape_Mail_&_Newsgroups.md "wikilink")（[電子郵件客戶端和](../Page/電子郵件客戶端.md "wikilink")[新聞群組軟體](../Page/Usenet客戶端.md "wikilink")）、Address
Book（通訊錄）、[Netscape
Composer](../Page/Netscape_Composer.md "wikilink")（[HTML編輯器](../Page/HTML編輯器.md "wikilink")）、[AOL
Instant
Messenger](../Page/AOL_Instant_Messenger.md "wikilink")（[即時通訊](../Page/即時通訊.md "wikilink")）。

## 歷史和發展

1998年3月，Netscape分離了大部分Communicator程式碼並在開放原始碼授權下發佈\[1\]。該項目被稱為[Mozilla](../Page/Mozilla#開放原始碼計畫.md "wikilink")。據估計，將支離破碎的原始碼（必須移除所有專有內容）轉換為新的瀏覽器版本可能需要一年時間，因此決定下一版本的[Netscape
5.0瀏覽器版本將以此為基礎](../Page/Netscape_5.0.md "wikilink")。Netscape指派瀏覽器開發工程師協助完成該項目。

然而，Mozilla的開發進展並不快，所以Netscape將其部分工程師重新分配到一個新的Communicator
4.5版本。這導致了瀏覽器的部分工作被迫停擺，而Internet Explorer
5.0仍然在發展階段。

在[Internet Explorer
5.0發表了一年半之後](../Page/Internet_Explorer_5.0.md "wikilink")，Netscape
5.0版本被跳過了。原本有計劃發表以4.x為基礎的代碼庫幾乎就緒的5.0版本，但是這個想法被放棄了\[2\]。1998年10月底，Mozilla的工程師們決定放棄Communicator程式碼，使用符合標準的[Gecko排版引擎重新開始](../Page/Gecko.md "wikilink")\[3\]。所有的資源都必須使用Gecko來完成Netscape
6.0，一些Netscape員工仍然認為這是公司歷史上最大的錯誤之一\[4\]。

兩年後（2000年）Netscape 6.0的首次公開發布版本相當令人失望，因速度慢和不穩定而飽受批評。Netscape
6.0於4月\[5\]、8月\[6\]和10月\[7\]發布公開測試版本，Netscape 6.0於2000年11月發布\[8\]。

2001年發布的6.1和6.2版本解決了穩定性問題，儘管得到了一些的關注，但使用者數量仍然相對偏少，Netscape
6正面臨著2001年夏季發布的[Internet Explorer
6.0的新競爭](../Page/Internet_Explorer_6.0.md "wikilink")。

## 版本歷史

  - Netscape 6.0 – 2000年11月14日（基於Mozilla Suite 0.6）
  - Netscape 6.01 – 2001年2月9日（基於Mozilla Suite 0.6）
  - Netscape 6.1 – 2001年8月8日（基於Mozilla Suite 0.9.2.1）
  - Netscape 6.2 – 2001年10月30日（基於Mozilla Suite 0.9.4.1）
  - Netscape 6.2.1 – 2001年11月29日（基於Mozilla Suite 0.9.4.1）
  - Netscape 6.2.2 – 2002年3月19日（基於Mozilla Suite 0.9.4.1）
  - Netscape 6.2.3 – 2002年5月15日（基於Mozilla Suite 0.9.4.1）

## 參考資料

## 外部連結

  - [下載各舊版本的Netscape瀏覽器和網路套件](http://sillydog.org/narchive/full123.php)
  - [由MozTW製作的非官方中文化版本](http://www.moztw.org/dl/ns/)

## 相關文章

  - [Netscape](../Page/Netscape.md "wikilink")
  - [網景 (瀏覽器)](../Page/網景_\(瀏覽器\).md "wikilink")
  - [Mozilla Application
    Suite](../Page/Mozilla_Application_Suite.md "wikilink")
  - [电子邮件客户端列表](../Page/电子邮件客户端列表.md "wikilink")
  - [电子邮件客户端比较](../Page/电子邮件客户端比较.md "wikilink")

[Category:Netscape](../Category/Netscape.md "wikilink")
[Category:2000年軟體](../Category/2000年軟體.md "wikilink")
[Category:網頁瀏覽器](../Category/網頁瀏覽器.md "wikilink")
[Category:電子郵件客戶端](../Category/電子郵件客戶端.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.