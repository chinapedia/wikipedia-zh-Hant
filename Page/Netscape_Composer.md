**Netscape
Composer**是一個[所見即所得的](../Page/所見即所得.md "wikilink")[HTML編輯器](../Page/HTML編輯器.md "wikilink")，最初由[網景通訊公司於](../Page/網景.md "wikilink")1997年發佈，後來成為[Netscape
6和](../Page/Netscape_6.md "wikilink")[Netscape
7](../Page/Netscape_7.md "wikilink")[網路套件的一部份](../Page/網路套件.md "wikilink")。Composer可以查閱和更改HTML碼，並且在領航員中預覽修改結果、[校正拼字和出版網頁](../Page/校對.md "wikilink")，在網景通訊家族中文版中被翻譯為「**設計師**」。

網景最初將Composer開發做為[網景通訊家族](../Page/網景通訊家族.md "wikilink")（Netscape
4）的一部份，但是該公司於1998年被[美國線上買下](../Page/美國線上.md "wikilink")，於是Composer開放原始碼並且交由[Mozilla基金會作進一步的研發](../Page/Mozilla基金會.md "wikilink")，Mozilla所進一步研發的部份被稱為[Mozilla
Composer](../Page/Mozilla_Composer.md "wikilink")，成為其網路套件[Mozilla
Application Suite的一部份](../Page/Mozilla_Application_Suite.md "wikilink")。

最後一版的Netscape
Composer配合Netscape7.2套件上市。在Mozilla專注於研發獨立軟體的同時Composer未受到重視。後來2005年發行的[Netscape
8和](../Page/Netscape_Browser.md "wikilink")2007年發行的[Netscape Navigator
9都是建構於](../Page/Netscape_Navigator_9.md "wikilink")[Firefox的獨立](../Page/Firefox.md "wikilink")[瀏覽器](../Page/瀏覽器.md "wikilink")，皆未包含Composer。

現在的Composer建構於原本的Mozilla
Composer。成為由Mozilla社群維護的網路套件[SeaMonkey的一部份](../Page/SeaMonkey.md "wikilink")（SeaMonkey是之前提到的Mozilla
Application Suite的後繼者）。

## 外部連結

  - [下載各舊版本的Netscape瀏覽器和網路套件](http://sillydog.org/narchive/full123.php)

## 相關文章

  - [網景通訊家族](../Page/網景通訊家族.md "wikilink")
  - [Netscape 6](../Page/Netscape_6.md "wikilink")
  - [Netscape 7](../Page/Netscape_7.md "wikilink")
  - [Mozilla Application
    Suite](../Page/Mozilla_Application_Suite.md "wikilink")
  - [SeaMonkey](../Page/SeaMonkey.md "wikilink")

[Category:Netscape](../Category/Netscape.md "wikilink")
[Category:HTML編輯器](../Category/HTML編輯器.md "wikilink")