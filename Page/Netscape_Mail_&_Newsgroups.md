**Netscape Mail & Newsgroups**一般被稱為Netscape
Mail，是[網景通訊公司設計的](../Page/網景.md "wikilink")[跨平台](../Page/跨平台.md "wikilink")[電子郵件客戶端以及](../Page/電子郵件客戶端.md "wikilink")[新聞群組程式](../Page/新聞群組.md "wikilink")，在[Netscape
4.5到](../Page/網景通訊家族.md "wikilink")[7.2之間成為網景](../Page/Netscape_7.md "wikilink")[網路套件的一部份](../Page/網路套件.md "wikilink")。

這個程式在Netscape 4到4.5之間是兩個程式，一個是郵件程式－「**傳訊者**（Netscape
Messenger）」另一個則是新聞群組程式－「**討論組**（Collabra）」，最後在4.5版的時候合併為Netscape
Mail & Newsgroups。

## 特色

Netscape Mail &
Newsgroups支援數種常見協定如[IMAP](../Page/IMAP.md "wikilink")，[POP3和](../Page/POP3.md "wikilink")[SMTP](../Page/SMTP.md "wikilink")，內建[貝葉斯垃圾郵件過濾器](../Page/貝葉斯垃圾郵件過濾.md "wikilink")，並且在Netscape
6之後的版本中支援多郵件帳號。

最初Netscape Mail and
Newsgroups是網景通訊公司所開發，隨著1998年網景被[美國線上買下](../Page/美國在線.md "wikilink")，他的程式碼被轉移給[Mozilla基金會](../Page/Mozilla基金會.md "wikilink")，並且被改稱為[Mozilla
Mail &
Newsgroups](../Page/Mozilla_Mail_&_Newsgroups.md "wikilink")，成為[開放原始碼的](../Page/開放原始碼.md "wikilink")[Mozilla
Application
Suite的一部份](../Page/Mozilla_Application_Suite.md "wikilink")。接下來Mozilla傾向於開發獨立運作的程式，因此在2004年至2006年期間Mozilla
Application Suite停止開發，同時網景系列的程式暫時停止研發。2005年，網景發佈[Netscape
8的時候亦未包含Netscape](../Page/Netscape_8.md "wikilink") Mail
and Newsgroups，因此7.2版即是最後一版。

目前Mozilla Mail & Newsgroups已經被改稱為[**SeaMonkey Mail &
Newsgroups**並且交由社群繼續研發](../Page/SeaMonkey.md "wikilink")。

## 新世代的Netscape Messenger 9

2007年，網景推出獨立的瀏覽器[Netscape Navigator
9之後](../Page/Netscape_Navigator_9.md "wikilink")，公告還會研發一個獨立的電子郵件客戶端，後來命名為**Netscape
Messenger 9**。這個新版本是修改自Mozilla Mail的後繼者[Mozilla
Thunderbird](../Page/Mozilla_Thunderbird.md "wikilink")。

另外在2006年底在網景社群討論板所做的[民意調查](../Page/輿論調查.md "wikilink")，推測Netscape
7系列的網路套件（包含電子郵件客戶端）將會再被網景內部研發，包含解決其中的重大臭蟲和安全性問題。

## 中文化

[MozTW](http://www.moztw.org/dl/ns/)有提供7.0pr1、7.01和7.2版的非官方中文化版本和語言套件。

## 外部連結

  - [下載各舊版本的Netscape瀏覽器和網路套件](http://sillydog.org/narchive/full123.php)

## 相關文章

  - [網景通訊家族](../Page/網景通訊家族.md "wikilink")
  - [Netscape 6](../Page/Netscape_6.md "wikilink")
  - [Netscape 7](../Page/Netscape_7.md "wikilink")
  - [Mozilla Application
    Suite](../Page/Mozilla_Application_Suite.md "wikilink")
  - [SeaMonkey](../Page/SeaMonkey.md "wikilink")

[Category:Netscape](../Category/Netscape.md "wikilink")
[Category:電子郵件客戶端](../Category/電子郵件客戶端.md "wikilink")