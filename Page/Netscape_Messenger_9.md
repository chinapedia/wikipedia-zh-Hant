**Netscape Messenger
9**是由[網景製作的一個](../Page/網景.md "wikilink")[跨平台的獨立](../Page/跨平台.md "wikilink")[電子郵件客戶端及](../Page/電子郵件客戶端.md "wikilink")[新聞群組程式](../Page/新聞群組.md "wikilink")，建構於[Mozilla
Thunderbird](../Page/Mozilla_Thunderbird.md "wikilink")。最初於2007年6月11日公開的時候以**Netscape
Mercury**之名宣傳，這個郵件程式是設計來搭配網景新公開的瀏覽器[Netscape Navigator
9用的](../Page/Netscape_Navigator_9.md "wikilink")。\[1\]

最初的名稱*Netscape
Mercury*其實是來自於[羅馬神話中的傳遞信息的使者](../Page/羅馬神話.md "wikilink")[墨丘利](../Page/墨丘利.md "wikilink")\[2\]，這取代了之前在版本4到7之間所用的[Netscape
Mail and
Newsgroups的名稱](../Page/Netscape_Mail_and_Newsgroups.md "wikilink")。

在2007年11月15日，網景公佈了[Linux](../Page/Linux.md "wikilink")、[Mac OS
X和](../Page/Mac_OS_X.md "wikilink")[Windows用的Alpha](../Page/Windows.md "wikilink")1測試版本\[3\]。

## 新功能與特色

Netscape Messenger包含了Mozilla Thunderbird
2.0.0.9所有的特色，另外整合了[美國線上](../Page/美國線上.md "wikilink")、[AIM](../Page/AIM.md "wikilink")、Netscape.net的郵件帳號。以及擁有一個和[Netscape
Navigator
9相近的](../Page/Netscape_Navigator_9.md "wikilink")[使用者介面](../Page/使用者介面.md "wikilink")\[4\]。

## 版本歷史

  - Netscape Messenger 9.0a1 – 2007年11月15日（基於Thunderbird2.0.0.9）

## 參考文獻

## 相關文章

  - [Netscape](../Page/Netscape.md "wikilink")
  - [Mozilla Thunderbird](../Page/Mozilla_Thunderbird.md "wikilink")
  - [Netscape Navigator 9](../Page/Netscape_Navigator_9.md "wikilink")
  - [电子邮件客户端比较](../Page/电子邮件客户端比较.md "wikilink")
  - [电子邮件客户端列表](../Page/电子邮件客户端列表.md "wikilink")

[Category:Netscape](../Category/Netscape.md "wikilink")
[Category:電子郵件客戶端](../Category/電子郵件客戶端.md "wikilink")

1.  [Netscape Mercury in progress
    - 2007年6月11日通告](http://community.netscape.com/n/pfx/forum.aspx?tsn=1&nav=messages&webtag=ws-nscpbrowser&tid=8688)
     造訪於 2007年11月21日
2.  [Feature requests in Mercury
    - 2007年6月12日通告](http://community.netscape.com/n/pfx/forum.aspx?msg=8692.4&nav=messages&webtag=ws-nscpbrowser#a4)
    造訪於 2007年11月21日
3.  [Netscape Blog on Netscape Messenger 9.a
    - 2007年11月16日通告](http://blog.netscape.com/2007/11/15/netscape-messenger-9-alpha-1-released/)
    造訪於 2007年11月21日
4.  [Netscape Messenger Release Notes
    - 2007年11月16日通告](http://mailnews.netscape.com/releasenotes/9.0a1/)
    造訪於 2007年11月21日