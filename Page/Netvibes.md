**Netvibes**是一個使用[AJAX的個人化首頁](../Page/AJAX.md "wikilink")，用戶可以依據喜好和需要自訂頁面內的模組、頁籤及外觀佈景，非註冊用戶可以在自己的電腦存取自己的個人化頁面，而註冊用戶則可以在任何電腦存取。Netvibes至今已經被翻譯成65種語言版本。

## 內建模組

[RSS](../Page/RSS.md "wikilink")／[Atom饋送閱讀器](../Page/Atom.md "wikilink")、天氣資訊、支援[iCal的月曆](../Page/iCal.md "wikilink")、書籤、多個網頁搜尋器、支援[POP3](../Page/POP3.md "wikilink")、[IMAP4](../Page/IMAP.md "wikilink")、數種[網頁郵件包括](../Page/Webmail.md "wikilink")[Hotmail](../Page/Hotmail.md "wikilink")、[Yahoo\!
Mail](../Page/Yahoo!_Mail.md "wikilink")、[GMail等的郵件閱讀器](../Page/GMail.md "wikilink")、[Box.net網上儲存](../Page/Box.net.md "wikilink")、[del.icio.us](../Page/del.icio.us.md "wikilink")、[Meebo](../Page/Meebo.md "wikilink")、[Flickr](../Page/Flickr.md "wikilink")、支援[Podcast的媒體播放器等](../Page/Podcast.md "wikilink")。

## 生態系統

用戶可以把個人化的頁籤、饋送、模組經Netvibes的[生態系統（Ecosystem）](http://eco.netvibes.com)與其他用戶分享。生態系統集合各用戶發送的頁籤、饋送、模組、[Podcast](../Page/Podcast.md "wikilink")、事件和「宇宙」。

## 宇宙

Netvibes的「宇宙」是可以讓所有人看見的個人化頁面，每個宇宙為了一個品牌、媒體等建立的，現時有的「宇宙」有[時代雜誌](../Page/時代雜誌.md "wikilink")、[Mandy
Moore等](../Page/Mandy_Moore.md "wikilink")。在將來，每個用戶亦可設計自己的「宇宙」。

## API

早期Netvibes已經提供了一系列[API供用戶設計自己的模組](../Page/API.md "wikilink")；但現時Netvibes推出Universal
Widget API（通用模組API，UWA），用戶使用UWA寫出的模組除了可以用於Netvibes本身，亦可在其他平台。

## 獎項

  - Best International App（The Dutch Web 2.0 Awards，2006）\[1\]
  - Webware 100（2007）\[2\]
  - 50 Best Websites 2007（第8位，時代雜誌，2007）\[3\]

## 參閱

  - [iGoogle](../Page/iGoogle.md "wikilink") - Google與Netvibes性質相似的服務
  - [Live.com](../Page/Live.com.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [Netvibes](http://www.netvibes.com)
  - [Netvibes生態系統](http://eco.netvibes.com/)
  - [NetvibesFAQ](http://faq.netvibes.com/)
  - [Netvibes的部落格](http://blog.netvibes.com/)
  - [Netvibes開發者網站](http://dev.netvibes.com/)
  - [UWA文件](http://dev.netvibes.com/doc/)
  - [對Netvibes的評論](https://web.archive.org/web/20070628183504/http://wiki.netvibes.com/doku.php?id=they_talk_about_us)

[Category:Web 2.0](../Category/Web_2.0.md "wikilink")

1.  <http://www.web20awards.nl/>
2.  <http://www.webware.com/html/ww/100.html>
3.  <http://www.time.com/time/specials/2007/0,28757,1633488,00.html>