[Network_26.jpg](https://zh.wikipedia.org/wiki/File:Network_26.jpg "fig:Network_26.jpg")

**Network 26
新里程**指[香港政府在](../Page/香港政府.md "wikilink")1993年9月1日撤銷[中巴其中](../Page/中華汽車有限公司.md "wikilink")28條路線的專營權給予[城巴營運](../Page/城巴.md "wikilink")。

## 背景

1989年，參與汽車交通運輸業總工會（[工聯會屬會](../Page/工聯會.md "wikilink")）的中巴員工發動大型[罷工行動](../Page/罷工.md "wikilink")，當時城巴臨時開辦非專利的路線往返無[紅色小巴行走的](../Page/香港小巴.md "wikilink")[鴨脷洲](../Page/鴨脷洲.md "wikilink")，而鴨脷洲當時約50,000人口頓時對中巴失去信心，[南區亦然](../Page/南區_\(香港\).md "wikilink")。

其後香港政府因中巴服務質素欠佳，在1992年初宣布，決定將中巴26條路線的專營權收回公開招標。當時城巴、[九巴及](../Page/九巴.md "wikilink")[捷達巴士](../Page/捷達集團.md "wikilink")（Stagecoach）分公司[必達巴士均有向政府遞交標書](../Page/必達巴士.md "wikilink")，而因香港政府不希望香港巴士服務被九巴壟斷，因此在招標初期已勸籲九巴取消投標。因城巴在港島的根基日漸穩固，而必達巴士在本港當時沒有營辦巴士服務，因此港府於1992年10月給予城巴該28條路線的專營權。1993年9月1日起，城巴正式開始營運該28條路線。

## 涉及路線

[CTB8145.jpg](https://zh.wikipedia.org/wiki/File:CTB8145.jpg "fig:CTB8145.jpg")是在Network
26 新里程中城巴接辦的路線之一\]\]

*注：以下起訖點以接辦當時為準*

  - **[1](../Page/城巴1號線.md "wikilink")**
    [中環（林士街）](../Page/中環（林士街）巴士總站.md "wikilink")
    - [跑馬地](../Page/跑馬地.md "wikilink")（上）（註：此線已與5A線合併，並延至摩星嶺 - 跑馬地）
  - **1M** [金鐘（東）](../Page/金鐘（東）巴士總站.md "wikilink") -
    [跑馬地馬場](../Page/跑馬地馬場.md "wikilink") \#
  - **[5](../Page/城巴5號線.md "wikilink")**
    [銅鑼灣](../Page/銅鑼灣.md "wikilink")（威非路道）巴士總站 -
    [西營盤](../Page/西營盤.md "wikilink")（路線取消前延至摩星嶺 - 銅鑼灣（威非路道））\#
  - **[5A](../Page/城巴5A線.md "wikilink")**
    [跑馬地](../Page/跑馬地.md "wikilink")（下）-
    [堅尼地城](../Page/堅尼地城巴士總站.md "wikilink") \#
  - **[5B](../Page/城巴5B線.md "wikilink")**
    [摩星嶺](../Page/摩星嶺.md "wikilink") -
    [銅鑼灣](../Page/銅鑼灣.md "wikilink")（現縮短成：堅尼地城 - 銅鑼灣）
  - **[6](../Page/城巴6系路線.md "wikilink")**
    [中環](../Page/交易廣場.md "wikilink") -
    [赤柱](../Page/赤柱.md "wikilink")
  - **[6A](../Page/城巴6系路線.md "wikilink")** 中環 -
    [赤柱炮台](../Page/赤柱炮台.md "wikilink")（現縮短成：中環 -
    赤柱炮台閘口，及改為繁忙時段服務）
  - **10X** 堅尼地城 - [金鐘（西）](../Page/金鐘（西）巴士總站.md "wikilink") \#
  - **[12](../Page/城巴12線.md "wikilink")** 中環 -
    [羅便臣道](../Page/羅便臣道.md "wikilink")（現時改經擺花街和亞畢諾道上山，並改經雪廠街前往中環碼頭）
  - **[12M](../Page/城巴12M線.md "wikilink")**
    [金鐘（東）](../Page/金鐘（東）巴士總站.md "wikilink")
    - [柏道](../Page/柏道.md "wikilink")
  - **[48](../Page/城巴48線.md "wikilink")**
    [華富北](../Page/華富（北）巴士總站.md "wikilink") -
    [黃竹坑](../Page/黃竹坑.md "wikilink")／[海洋公園](../Page/香港海洋公園.md "wikilink")（現調整成循環線：深灣／海洋公園
    開出往華富北 再返回深灣／海洋公園）
  - **[61](../Page/城巴61線.md "wikilink")** 中環 -
    [淺水灣](../Page/淺水灣.md "wikilink") \#
  - **61M** 金鐘（東）- 淺水灣 \#
  - **[70](../Page/城巴70線.md "wikilink")** 中環 -
    [香港仔](../Page/香港仔.md "wikilink")（現延至中環 - 華貴)
  - **[70M](../Page/城巴70M線.md "wikilink")** 香港仔 - 金鐘（東）\#
  - **[72](../Page/城巴72線.md "wikilink")**
    [華貴](../Page/華貴邨.md "wikilink") - 銅鑼灣
  - **[72A](../Page/城巴72A線.md "wikilink")** 黃竹坑 - 銅鑼灣（摩頓臺）（現延至深灣 -
    銅鑼灣（摩頓臺））
  - **[72B](../Page/城巴72B線.md "wikilink")** 香港仔 - 銅鑼灣（摩頓臺） \#
  - **[75](../Page/城巴75線.md "wikilink")** 中環 - 黃竹坑（現延至中環 - 深灣)
  - **[76](../Page/城巴76線.md "wikilink")**
    [石排灣](../Page/石排灣邨公共運輸交匯處.md "wikilink")
    - 銅鑼灣（摩頓臺）（現改為石排灣至銅鑼灣（邊寧頓街）的循環線）
  - **[90](../Page/城巴90線.md "wikilink")**
    [鴨脷洲](../Page/鴨脷洲邨.md "wikilink") - 中環
  - **[92](../Page/城巴92線.md "wikilink")** 鴨脷洲 - 銅鑼灣（摩頓臺） \#
  - **[96](../Page/城巴96線.md "wikilink")**
    [鴨脷洲利東邨](../Page/利東邨巴士總站.md "wikilink") -
    銅鑼灣（摩頓臺）
  - **[97](../Page/城巴97線.md "wikilink")** 鴨脷洲利東邨 - 中環
  - **[97A](../Page/城巴97A線.md "wikilink")** 鴨脷洲利東邨 - 黃竹坑（現延至 利東／鴨脷洲大街 -
    深灣繁忙時段服務）
  - **[98](../Page/城巴98線.md "wikilink")** 鴨脷洲利東邨 - 香港仔
  - **[107](../Page/過海隧道巴士107線.md "wikilink")**
    [九龍灣企業廣場一期](../Page/九龍灣公共運輸交匯處.md "wikilink")
    - 香港仔（現延至九龍灣 - 華貴）
  - **[170](../Page/過海隧道巴士170線.md "wikilink")**
    [沙田車站](../Page/沙田站.md "wikilink") -
    [華富中](../Page/華富（中）巴士總站.md "wikilink")

**\#**：代表已取消路線（直至2015年5月）

## 事件以後

自城巴接管中巴的26條路線後，該等路線的服務質素得到重大的改善，另一邊廂，中巴的服務質素依然未有任何改善。在1995年9月1日，港府再削減14條中巴路線（[7](../Page/城巴7號線.md "wikilink")、[11](../Page/城巴11線.md "wikilink")、[37](../Page/城巴37線.md "wikilink")、[40](../Page/城巴40線.md "wikilink")、[40M](../Page/城巴40M線.md "wikilink")、[71](../Page/城巴71線.md "wikilink")、[73](../Page/城巴73線.md "wikilink")、[85](../Page/城巴85線.md "wikilink")、[99](../Page/城巴99線.md "wikilink")、[511](../Page/城巴11線.md "wikilink")、[592](../Page/城巴592線.md "wikilink")、[260](../Page/城巴260線.md "wikilink")、[103及](../Page/過海隧道巴士103線.md "wikilink")[182](../Page/過海隧道巴士182線.md "wikilink")），直接交由城巴營運。

在1997年中巴取消多條嚴重虧蝕的路線，分別於3月3日取消45及47線，在4月21日取消41線及在6月2日取消3號線，城巴隨即在上述路線的取消日期開辦[40P](../Page/城巴40P線.md "wikilink")、[47A](../Page/城巴M47線.md "wikilink")（於2005年6月改為M47線，其後於2014年12月因[港鐵](../Page/港鐵.md "wikilink")[西港島綫通車](../Page/港島綫西延.md "wikilink")，由[城巴43M線取代](../Page/城巴43M線.md "wikilink")）、[41A及](../Page/城巴41A線.md "wikilink")[3B](../Page/城巴3B線.md "wikilink")（於2015年5月取消）去取代前述路線，並提升服務，客量得到提升。

1998年2月，政府宣布中巴的專營權在1998年9月1日凌晨零時到期後不再延續，並把路線交由[新巴及城巴營運](../Page/新巴.md "wikilink")，部分[過海聯營路線則改為由九巴獨營](../Page/香港過海隧道巴士路線.md "wikilink")。

## 註釋

## 參考文獻

  - 《Hong Kong Buses Volume 3 - Citybus Limited》，Mike Davis編著，英國DTS
    Publishing出版
  - [中巴皇國之墜落](http://www.chinamotorbus.com/bus/n26/) - 中華巴士紀念館

[Category:1993年香港](../Category/1993年香港.md "wikilink")
[Category:香港交通史](../Category/香港交通史.md "wikilink")
[Category:香港巴士術語](../Category/香港巴士術語.md "wikilink")
[Category:城巴](../Category/城巴.md "wikilink")