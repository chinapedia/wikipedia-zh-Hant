[HK_MegaBox_Level17_Neway_Karaoke_Entrance_201402.jpg](https://zh.wikipedia.org/wiki/File:HK_MegaBox_Level17_Neway_Karaoke_Entrance_201402.jpg "fig:HK_MegaBox_Level17_Neway_Karaoke_Entrance_201402.jpg")
CEO大堂\]\]
[Novotel_Century_Hong_Kong_Neway_2018.jpg](https://zh.wikipedia.org/wiki/File:Novotel_Century_Hong_Kong_Neway_2018.jpg "fig:Novotel_Century_Hong_Kong_Neway_2018.jpg")
[HK_Yuen_Long_元朗_Kau_Yuk_Road_教育路_night_02_Neway_shop.jpg](https://zh.wikipedia.org/wiki/File:HK_Yuen_Long_元朗_Kau_Yuk_Road_教育路_night_02_Neway_shop.jpg "fig:HK_Yuen_Long_元朗_Kau_Yuk_Road_教育路_night_02_Neway_shop.jpg")樂成分店\]\]

**Neway**（前稱**新時代卡拉OK**）是[香港一家](../Page/香港.md "wikilink")[卡拉OK集團](../Page/卡拉OK.md "wikilink")，目前共有18間分店，於1993年開設首間分店\[1\]，現今Neway在[澳門](../Page/澳門.md "wikilink")、[中國大陸和](../Page/中國大陸.md "wikilink")[馬來西亞亦有業務](../Page/馬來西亞.md "wikilink")。

Neway於2000年代成立[Neway
Star](../Page/Neway_Star.md "wikilink")，成為香港首間由卡拉OK開設的[唱片及](../Page/唱片.md "wikilink")[經理人公司](../Page/經理人公司.md "wikilink")，旗下首位藝人為[王浩信](../Page/王浩信.md "wikilink")，後來[HotCha及](../Page/HotCha.md "wikilink")[胡杏兒亦相繼加入](../Page/胡杏兒.md "wikilink")[Neway
Star](../Page/Neway_Star.md "wikilink")。

## 歷史

[卡拉OK是源於](../Page/卡拉OK.md "wikilink")[日本的娛樂文化](../Page/日本.md "wikilink")，1990年代，卡拉OK熱潮席捲香港，當時本地卡拉OK主要以酒廊為經營模式，但普遍不以獨立房間形式經營。Neway看準市場商機，在1993年於人流暢旺的[香港市區](../Page/香港市區.md "wikilink")，如[尖沙咀](../Page/尖沙咀.md "wikilink")、[旺角](../Page/旺角.md "wikilink")、[銅鑼灣和](../Page/銅鑼灣.md "wikilink")[灣仔等地開設首家卡拉OK店](../Page/灣仔.md "wikilink")，其後業務擴充至[新市鎮](../Page/新市鎮.md "wikilink")\[2\]。

Neway曾和[加州紅合伙成立](../Page/加州紅.md "wikilink")[同成發展有限公司](../Page/同成發展有限公司.md "wikilink")，爭取[東亞唱片及](../Page/東亞唱片_\(集團\).md "wikilink")[環球唱片等公司的新歌試唱](../Page/環球唱片_\(香港\).md "wikilink")，後於2008年拆伙。由於同成發展實際運作由[加州紅負責](../Page/加州紅.md "wikilink")，故此Neway曾一度失去[東亞唱片等公司的獨家試唱權](../Page/東亞唱片_\(集團\).md "wikilink")。

2008年底，Neway取得[金牌大風新歌的獨家試唱權](../Page/金牌大風.md "wikilink")。2009年，更先後取得[東亞唱片
(集團)以及](../Page/東亞唱片_\(集團\).md "wikilink")[東亞唱片
(製作)新歌的獨家試唱權](../Page/東亞唱片_\(製作\).md "wikilink")。

除本身的品牌外，Neway亦營運CEO，以較高級的服務去吸引高消費顧客。CEO除以獨立品牌運作外，每個房間亦加設獨立[洗手間](../Page/洗手間.md "wikilink")，大部份房間設有[觸控螢幕點歌功能](../Page/觸控螢幕.md "wikilink")，並且加設多部電視以營造[立體影像](../Page/立體.md "wikilink")，裝潢亦較Neway豪華。至於以[Neway
City營運的店舖則以多](../Page/Neway_City.md "wikilink")[卡拉OK房間運作](../Page/卡拉OK.md "wikilink")，實際上與Neway大同小異。

### 收購同業

1990年代末，隨着香港幾家卡拉OK集團的惡性競爭並相繼倒閉，最終剩下Neway和[加州紅兩個集團](../Page/加州紅.md "wikilink")，形成[寡頭壟斷](../Page/寡頭壟斷.md "wikilink")。

2010年2月22日，有傳Neway將收購加州紅\[3\]，終於3月7日完成收購，初期以兩個品牌繼續運作，外界預料Neway將會壟斷香港卡拉OK行業\[4\]。

Neway於收購後開始將舊有加州紅的品牌轉為Neway名下或CEO。於同年5月31日將最後一間位於[元朗的](../Page/元朗.md "wikilink")[加州紅關閉裝修](../Page/加州紅.md "wikilink")（於2010年重新裝修成為[CEO](../Page/CEO.md "wikilink")，於同年11月開業），後來由於[Red
MR成立](../Page/紅人派對.md "wikilink")，壟斷局面才稍微緩和，後者現在成為Neway的主要競爭對手。

**Neway App**

現時Neway已經推出ios和android手機app, 功能大致一樣,包括：\[5\]\[6\]

Neway K-fun

排行榜

最新資訊

分店及訂房

歌星選曲及類別選曲

●透過《連線到房間》，即可將手機/iPad接駁點歌系統，以手機/iPad進行各項功能：

●透過歌手名稱或歌曲類別點歌

●可隨時暫停及恢復正在播放的K歌

●選擇伴唱或重唱功能編輯歌曲

●可隨時插播或刪除「已選歌曲」

●將心水K歌儲存至「我的最愛」，立即點播已收藏之歌曲

●可隨時設定使用香港、澳門或馬來西亞地區的歌曲資料

●facebook分享

## 重要人物

  - [薛濟傑](../Page/薛濟傑.md "wikilink")：[中星集團前主席兼創辦人](../Page/中星集團.md "wikilink")
  - [薛世恆](../Page/薛世恆.md "wikilink")：負責打理Neway卡拉OK、[星娛樂](../Page/星娛樂.md "wikilink")、[Neway
    Star](../Page/Neway_Star.md "wikilink")。
  - [薛嘉麟](../Page/薛嘉麟.md "wikilink")：薛世恆弟，[中星集團主席](../Page/中星集團.md "wikilink")，妻子為[模特兒](../Page/模特兒.md "wikilink")[諸葛紫岐](../Page/諸葛紫岐.md "wikilink")。

## Neway分店

<span style="font-size:smaller;">註：
<span style="color: #8B00FF">■</span> 為Neway分店；
<span style="color: #E680FF">■</span> 為Neway City分店；
<span style="color: #33005E">■</span> 為CEO分店；
<span style="color: #FF3737">■</span>
為前[加州紅分店](../Page/加州紅.md "wikilink")(已改以Neway名義營業)；
<span style="color: #9F0000">■</span>
為前[加州紅分店](../Page/加州紅.md "wikilink")(已改以CEO名義營業)；
<span style="color: #FF3737">■</span> 為前[Red
Mr分店](../Page/Red_Mr.md "wikilink")(已改以Neway名義營業)；
<span style="color: #A9A9A9">*灰字斜體*</span>為將開業分店；
<span style="color: #A9A9A9"><s>灰字刪除線</s></span>為已結業分店</span></span>

### 香港

  - 港島區（2間）：灣仔英皇集團中心CEO<span style="color: #33005E">■</span>、糖街CEO<span style="color: #33005E">■</span>
    <span style="color: #A9A9A9"><s>灣仔世紀</s></span><span style="color: #FF3737">■</span>、<span style="color: #A9A9A9"><s>銅鑼灣信和</s></span></span><span style="color: #8B00FF">■</span>、<span style="color: #A9A9A9"><s>香港仔中心</s></span><span style="color: #8B00FF">■</span>、<span style="color: #A9A9A9"><s>銅鑼灣廣場1期</s></span><span style="color: #8B00FF">■</span>、<span style="color: #A9A9A9"><s>灣仔天樂里</s></span><span style="color: #8B00FF">■</span>、<span style="color: #A9A9A9"><s>北角雲華</s></span><span style="color: #8B00FF">■</span>
  - 九龍區（10間）：旺角創興<span style="color: #8B00FF">■</span>、旺角中心<span style="color: #8B00FF">■</span>、油麻地富運<span style="color: #8B00FF">■</span>、佐敦<span style="color: #8B00FF">■</span>、尖東港晶<span style="color: #8B00FF">■</span>、安達CEO
    <span style="color: #33005E">■</span>、MegaBox
    CEO<span style="color: #33005E">■、apm<span style="color: #FF3737">■</span>、opc<span style="color: #FF3737">■</span>、深水埗宇宙</span></span><span style="color: #8B00FF">■</span>、九龍城太子匯<span style="color: #8B00FF">■</span>
    <span style="color: #A9A9A9"><s>旺角百老匯</s></span><span style="color: #8B00FF">■</span>、<span style="color: #A9A9A9"><s>佐敦大華</s></span><span style="color: #8B00FF">■</span>、<span style="color: #A9A9A9"><s>旺角九龍行</s></span></span><span style="color: #8B00FF">■</span>、<span style="color: #A9A9A9"><s>觀塘銀都</s></span><span style="color: #8B00FF">■</span>、<span style="color: #A9A9A9"><s>旺角Neway
    City</s></span><span style="color: #E680FF">■</span>、<span style="color: #A9A9A9"><s>九龍城廣場</s></span></span><span style="color: #8B00FF">■、</span><span style="color: #A9A9A9"><s>九龍城百營</s></span></span><span style="color: #8B00FF">■、</span><span style="color: #A9A9A9"><s>尖中寶勒巷寶利</s></span></span><span style="color: #8B00FF">■</span>、</span><span style="color: #A9A9A9"><s>瓊華</s></span></span><span style="color: #8B00FF">■</span><span style="color: #A9A9A9"><s>旺角新世紀</s></span></span><span style="color: #8B00FF">■</span>
  - 新界區（5間）：荃灣CEO<span style="color: #33005E">■</span>、屯門新墟<span style="color: #8B00FF">■</span>、沙田富豪<span style="color: #8B00FF">■</span>、元朗合益<span style="color: #8B00FF">■</span>、粉嶺中心<span style="color: #8B00FF">■</span>
    <span style="color: #A9A9A9"><s>葵芳</s></span><span style="color: #8B00FF">■</span>、<span style="color: #A9A9A9"><s>沙田廣場<span style="font-size:smaller;"></span></s></span><span style="color: #FF3737">■</span>、<span style="color: #A9A9A9"><s>大埔廣場<span style="font-size:smaller;"></span></s></span><span style="color: #8B00FF">■</span>、<span style="color: #A9A9A9"><s>元朗樂成</s></span><span style="color: #8B00FF">■、<span style="color: #A9A9A9"><s>荃灣
    (英皇娛樂廣場5/F
    6/F)</s></span></span><span style="color: #A9A9A9">■、<s>元朗CEO<span style="color: #9F0000">■</s></span></span>

### 澳門

富豪酒店<span style="color: #33005E">■</span>、中福商業中心<span style="color: #8B00FF">■</span>

### 中國大陸

  - [廣州](../Page/廣州.md "wikilink")：花都馨庭<span style="color: #8B00FF">■</span>、北京路<span style="color: #8B00FF">■</span>、康王路<span style="color: #33005E">■</span>
  - [深圳](../Page/深圳.md "wikilink")：<span style="color: #A9A9A9"><s>廬山</s></span><span style="color: #8B00FF">■</span>

### 馬來西亞

  - [吉隆坡](../Page/吉隆坡.md "wikilink")：<span style="color: #8B00FF">■</span>、Fahrenheit
    88<span style="color: #33005E">■</span>、Cheras
    Plaza<span style="color: #8B00FF">■</span>
  - [八打灵再也](../Page/八打灵再也.md "wikilink")：1
    Utama<span style="color: #8B00FF">■</span>、SS2<span style="color: #33005E">■</span>
  - [蒲种](../Page/蒲种.md "wikilink")：Casa
    Square<span style="color: #8B00FF">■</span>
  - [巴生](../Page/巴生.md "wikilink")：Centro
    Mall<span style="color: #8B00FF">■</span>
  - [新山](../Page/新山.md "wikilink")：City
    Square<span style="color: #8B00FF">■</span>
  - [梳邦](../Page/梳邦.md "wikilink")：Jaya
    Square<span style="color: #8B00FF">■</span>
  - [檳城](../Page/檳城.md "wikilink")：Queensbay
    Mall<span style="color: #8B00FF">■</span>

## 目前向Neway提供新歌試唱的唱片公司

  - [Gain Capital](../Page/中星集團.md "wikilink")

      - [Neway Star](../Page/中星集團.md "wikilink")（Neway獨家）
      - [星娛樂](../Page/中星集團.md "wikilink")（Neway獨家）

  - [寰亞音樂](../Page/寰亞唱片.md "wikilink")／[東亞唱片
    (集團)](../Page/東亞唱片_\(集團\).md "wikilink")／[華星唱片](../Page/華星唱片.md "wikilink")（Neway獨家）

  - [太陽娛樂音樂](../Page/太陽娛樂文化.md "wikilink")

  - [TVB](../Page/電視廣播有限公司.md "wikilink")

      - [星夢娛樂](../Page/星夢娛樂.md "wikilink")（Neway獨家）
      - [TVB Music](../Page/正視音樂.md "wikilink")（Neway獨家）

  - [hmv MUSIC](../Page/HMV數碼中國集團.md "wikilink")（Neway獨家）

      - [GME Music](../Page/HMV數碼中國集團.md "wikilink")（Neway獨家）
      - [Starz Track](../Page/HMV數碼中國集團.md "wikilink")（Neway獨家）

  - [A Music](../Page/東亞唱片_\(製作\).md "wikilink")（Neway獨家）

  - [musicNEXT](../Page/環星音樂.md "wikilink")（Neway獨家）

  - [維高文化](../Page/維高文化.md "wikilink")（Neway獨家）

  - [Jam
    Music](../Page/JamCast_Management_\(HK\).md "wikilink")（Neway獨家）

  - [邁亞音樂](../Page/邁亞音樂.md "wikilink")（Neway獨家）

  - [大國文化](../Page/大國文化集團.md "wikilink")

  - [耀榮文化](../Page/耀榮文化.md "wikilink")

  - [Hummingbird Music](../Page/蜂鳥音樂.md "wikilink")

  - [英皇娛樂](../Page/英皇娛樂.md "wikilink")

  - [大名娛樂](../Page/大名娛樂.md "wikilink")

  -   -   - [新藝寶唱片](../Page/新藝寶唱片.md "wikilink")（2013年或之前）
          - [正東唱片](../Page/正東唱片.md "wikilink")（2013年或之前）
          - [上華唱片 (香港)](../Page/上華唱片.md "wikilink")（2013年或之前）
          - [寶麗金唱片](../Page/寶麗金唱片.md "wikilink")（2013年或之前）

      - [百代唱片](../Page/百代唱片_\(香港\).md "wikilink")

      - [華納唱片](../Page/華納唱片_\(香港\).md "wikilink")

          - [金牌大風](../Page/金牌大風.md "wikilink")

      - [索尼音樂娛樂 (香港)](../Page/索尼音樂娛樂_\(香港\).md "wikilink")

          - [杰威爾音樂](../Page/杰威爾音樂.md "wikilink")

<!-- end list -->

  -
## 服務提供

  - 自選卡拉OK
  - 膳食（K-lunch、K-buffet）
  - 免費[無線上網](../Page/無線區域網路.md "wikilink")
  - 房間桌面電視（只限部分分店）
  - 自選電視頻道
  - 獨立洗手間（只限Neway CEO及部分分店）
  - 自選點唱站（只限部分分店）
  - 手機app點唱、cut歌、插播、重播、Shuffle、翻查紀錄、文字訊息、Save歌及Book房
  - X-Box、PlayStaion及Windows PC （只限部分分店）

## 參考來源

## 外部連結

  - [Neway卡拉OK官方網站](http://www.newaykb.com)

  - [Neway卡拉OK Facebook](http://www.facebook.com/newaykaraokebox)

  -
[分類:香港卡拉OK公司](../Page/分類:香港卡拉OK公司.md "wikilink")
[分類:中星集團](../Page/分類:中星集團.md "wikilink")

1.  [Neway-集團簡介](http://www.newaykb.com/newsite/about.php)

2.
3.  [傳Neway收購加州紅獨霸K壇](http://hk.news.yahoo.com/article/100222/3/gou9.html)
    ，《星島日報》，2010年2月23日

4.  [Neway宣布收購加州紅品牌續用](http://hk.news.yahoo.com/article/100307/4/gw53.html)


5.

6.