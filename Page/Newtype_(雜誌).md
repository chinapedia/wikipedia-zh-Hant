[日語版Newtype
2006年11月號](https://zh.wikipedia.org/wiki/File:Newtypejp_200611.jpg "fig:日語版Newtype 2006年11月號")

《**Newtype**》（），由[角川書店出版的](../Page/角川書店.md "wikilink")[日本動畫](../Page/日本動畫.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")，創刊於1985年3月，以月刊形式出版，每月10日發售。Newtype曾有三種語言，分別為日語版、英語版及韓語版，英語版已經於2008年2月廢刊。

杂志的名字来源于动画[机动战士高达和](../Page/机动战士高达.md "wikilink")[机动战士Z高达中的](../Page/机动战士Z高达.md "wikilink")“[Newtype](../Page/Newtype.md "wikilink")”。杂志创刊于Z高达动画播出一周后。

另外，Newtype有一本增刊為[娘TYPE](../Page/娘TYPE.md "wikilink")，於2009年4月30日創刊，美少女角色為中心。

## 內容大要

### NEWTYPE EXPRESS

  - 提供最新動畫及動畫電影的新聞速遞

### Newtype Press

  - 動畫製作人感想及近況報告

### 林原めぐみのぴーひゃら生活

  - [林原めぐみ的個人記事](../Page/林原惠.md "wikilink")

### Switched-on CV

  - 聲優的最新近況

### Newtype DATABASE

  - ANIME LAND—一個月內的動畫節目表
  - monthly sales—一個月內CD，遊戲，書及漫畫的價格表
  - THE OFFICIAL Art of—動畫的設定資料集
  - 全国アニメ＆声優ラジオ番組—全日本動畫及聲優廣播劇節目表

### NEWTYPE FORUM

  - newtype的讀者投稿

### アニメの門

  - 介紹動畫的起源，小道消息

### NEWS\&NEWTYPE

  - TOPIC　
  - DVD＆VIDEO
  - MUSIC
  - HOW to ART
  - EVENT＆VOICE
  - MODEL
  - GAME
  - NEWTYPE SELECTION
  - BOOK
  - NEW RELEASE

各種最新產品的介紹及價格表

### Ranking Review

這是每個月，分開男女角色的十大動畫角色人氣榜，是根據观众反响、杂志读者回馈、网络搜索量、节目的收视率、节目的录话书、DVD销量等数据，综合起来的人氣排名。

## 過往漫畫連載

  - 《[奇蹟少女KOBATO.](../Page/奇蹟少女KOBATO..md "wikilink")》——[CLAMP](../Page/CLAMP.md "wikilink")
  - 《[Lagoon Engine
    Einsatz](../Page/Lagoon_Engine_Einsatz.md "wikilink")》——[杉崎ゆきる](../Page/杉崎ゆきる.md "wikilink")
  - 《[はじまりのグラシュマ](../Page/はじまりのグラシュマ.md "wikilink")》——[スエカネクミコ](../Page/スエカネクミコ.md "wikilink")
  - 《[シャングリ・ラ](../Page/シャングリ・ラ.md "wikilink")》——[池上永一](../Page/池上永一.md "wikilink")
  - 《[PERSPECTIVE](../Page/PERSPECTIVE.md "wikilink")》——[イノセンス](../Page/イノセンス.md "wikilink")
  - 《[五星物語](../Page/五星物語.md "wikilink")》——[永野護](../Page/永野護.md "wikilink")
  - 《[聖獸傳承](../Page/聖獸傳承.md "wikilink")》——[麻宮騎亞](../Page/麻宮騎亞.md "wikilink")

## 關聯條目

  - [娘TYPE](../Page/娘TYPE.md "wikilink")
  - [Megami MAGAZINE](../Page/Megami_MAGAZINE.md "wikilink")

## 外部連結

  - [Newtype日文版官方網站](http://anime.webnt.jp/)
  - [NewType韓文版官方網站](http://blog.naver.com/ntblog)
  - [角川書店](http://www.kadokawa.co.jp/)
  - [過期雜誌](https://web.archive.org/web/20070209005128/http://www.kadokawa.co.jp/mag/bknum.php?tp=newtype)

[Category:角川書店的漫畫雜誌](../Category/角川書店的漫畫雜誌.md "wikilink")
[Category:日本漫畫雜誌](../Category/日本漫畫雜誌.md "wikilink")
[Category:月刊漫畫雜誌](../Category/月刊漫畫雜誌.md "wikilink")
[Category:動畫雜誌](../Category/動畫雜誌.md "wikilink")
[\*](../Category/月刊Newtype.md "wikilink")
[Category:1985年日本建立](../Category/1985年日本建立.md "wikilink")
[Category:1985年建立的出版物](../Category/1985年建立的出版物.md "wikilink")