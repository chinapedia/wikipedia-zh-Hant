[NexGen_Nx596-P90.jpg](https://zh.wikipedia.org/wiki/File:NexGen_Nx596-P90.jpg "fig:NexGen_Nx596-P90.jpg")

**NexGen**是一家私人控股的[美国](../Page/美国.md "wikilink")[半導體公司](../Page/半導體.md "wikilink")，專門設計[x86相容處理器](../Page/x86.md "wikilink")，之後於1996年由[AMD所收併](../Page/AMD.md "wikilink")。NexGen與他的競爭業者[Cyrix相同](../Page/Cyrix.md "wikilink")，都是[無廠半導體公司](../Page/無廠半導體公司.md "wikilink")（[Fabless](../Page/Fabless.md "wikilink")），必須倚賴[晶圓廠代為生產晶片](../Page/晶圓廠.md "wikilink")。NexGen過去是交由[IBM微電子部門代產](../Page/IBM.md "wikilink")。

NexGen成立於1986年，由[Compaq](../Page/Compaq.md "wikilink")、[株式會社ASCII及](../Page/株式會社ASCII.md "wikilink")[Kleiner
Perkins共同創辦](../Page/Kleiner_Perkins.md "wikilink")。第一個設計是期望開發出[80386相同世代等級的處理器](../Page/80386.md "wikilink")，不過設計過於複雜、[電路面積過於龐大](../Page/電路.md "wikilink")，必須用8顆[晶片才能等同於](../Page/晶片.md "wikilink")1顆晶片的功效；等到晶片數已經收斂減少後，整個業界已經轉移、進入[80486世代了](../Page/80486.md "wikilink")。

NexGen的第二顆設計是Nx586
CPU，於1994年推出，並嘗試與[Intel的](../Page/Intel.md "wikilink")[Pentium直接抗衡](../Page/Pentium.md "wikilink")，這也是第一顆向Pentium發出挑戰的處理器。此時Nx586處理器的具體型款為Nx586-P80及Nx586-P90。與其他競爭業者（AMD、Cyrix）不同的，Nx586的接腳配置與Pentium不相容，以致必須搭配特有的晶片組與主機板才行。為此，NexGen推出兩款使用Nx586的[主機板](../Page/主機板.md "wikilink")，一片支援[VLB](../Page/VLB.md "wikilink")（[VESA
Local
Bus](../Page/VESA_Local_Bus.md "wikilink")），另一片支援[PCI](../Page/PCI.md "wikilink")。

與AMD、Cyrix的Pentium級處理器一樣，在相同時脈頻率下，AMD、Cyrix的處理器效能皆超越Pentium；所以Nx586-P80以75MHz運作，Nx586-P90以84MHz運作。然而很不幸的，Nx586的效能勝過搭配82430LX、82430NX晶片組的Pentium，但Intel推出Pentium的新搭配晶片組82430FX（Triton）後就超越了Nx586，使Nx586的優勢難以維持。此外，Nx586不具[浮點運算能力](../Page/浮點運算.md "wikilink")，必須額外搭配Nx587的浮點運算（協力）處理器才行。

不過，後續的Nx586也具有浮點運算能力，這是由IBM使用MCM（Multi-Chip
Module）封裝技術來實現，將Nx586、Nx587兩個裸晶一同封裝，且接腳相容於過去的Nx586；此顆稱為Nx586-PF100，好與之前就已經推出的Nx586-P100（不具浮點運算能力）有所區別。

另外，[Compaq在其產品文宣](../Page/Compaq.md "wikilink")、展示、箱盒上使用586字樣，並以此對抗“Pentium”一詞，不過Compaq並沒有廣泛採用NexGen的處理器。

之後，[AMD
K5處理器的性能與銷售表現都低於預期](../Page/AMD_K5.md "wikilink")，因此AMD買下NexGen，大量取得NexGen的設計團隊，並延續Nx586的設計來開發新處理器，以此成就了在商業上相當成功的[AMD
K6處理器](../Page/AMD_K6.md "wikilink")。

## 外部連結

  - [CPU-INFO: NexGen Nx586, indepth processor
    history](https://web.archive.org/web/20071027090829/http://www.cpu-info.com/index2.php?mainid=html%2Fcpu%2FNx586.php)

<!-- end list -->

  - [AMD: Nx586
    Processor](http://www.amd.com/us-en/Processors/ProductInformation/0,,30_118_1260_1272,00.html)

<!-- end list -->

  - [cpu-world: Nx586 Processor](http://www.cpu-world.com/CPUs/Nx586/)

<!-- end list -->

  - [Nx586](https://web.archive.org/web/20060615180058/http://www.sandpile.org/impl/nx5.htm)

<!-- end list -->

  - [Nx686](https://web.archive.org/web/20060615180714/http://www.sandpile.org/impl/nx6.htm)

[Category:電腦硬件公司](../Category/電腦硬件公司.md "wikilink")
[Category:已結業電腦公司](../Category/已結業電腦公司.md "wikilink")