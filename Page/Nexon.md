**NEXON**（；；中文：**納-{}-克森**）是一家於1994年12月成立於[韓國](../Page/韓國.md "wikilink")\[1\]的[電腦遊戲股控公司](../Page/電腦遊戲.md "wikilink")，Nexon總部曾設於[首爾](../Page/首爾.md "wikilink")，在2011年12月以控股公司方式，搬移營運總部至[東京](../Page/東京.md "wikilink")\[2\]，實際控制權轉移至[濟州島的NXC公司](../Page/濟州島.md "wikilink")。

并擅长开发休闲类的[网络游戏](../Page/网络游戏.md "wikilink")，目前主要的作品有[泡泡堂](../Page/泡泡堂.md "wikilink")、[跑跑卡丁车](../Page/跑跑卡丁车.md "wikilink")、[泡泡战士](../Page/泡泡战士.md "wikilink")、[天翼之鍊](../Page/天翼之鍊.md "wikilink")、[神之領域](../Page/神之領域.md "wikilink")、[新楓之谷](../Page/新楓之谷.md "wikilink")、[瑪奇](../Page/瑪奇.md "wikilink")、[天涯明月刀等](../Page/天涯明月刀.md "wikilink")。在[港](../Page/香港.md "wikilink")[澳](../Page/澳門.md "wikilink")[台地區](../Page/台灣.md "wikilink")，大部分NEXON遊戲均由[遊戲橘子代理運營](../Page/遊戲橘子數位科技.md "wikilink")；而[中國大陸則分別由](../Page/中國大陸.md "wikilink")[盛大網絡及](../Page/盛大網絡.md "wikilink")[世紀天成代理運營](../Page/世紀天成.md "wikilink")。2011年12月14日起在[東京上市](../Page/東京.md "wikilink")，總部移往[日本](../Page/日本.md "wikilink")，原[首爾本部改為](../Page/首爾.md "wikilink")，成為
（Nexon K.K.) 的子公司，Nexon的股份則由位於[濟州島的](../Page/濟州島.md "wikilink")
NXC公司過半持有。

## 營運子公司

  - [Nexon
    Korea](../Page/Nexon_Korea.md "wikilink")（總部設於[韓國](../Page/韓國.md "wikilink")[首爾](../Page/首爾.md "wikilink")）
  - [Nexon
    Europe](../Page/Nexon_Europe.md "wikilink")（總部設於[英國](../Page/英國.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")）
  - [Nexon
    America](../Page/Nexon_America.md "wikilink")（總部設於[美國](../Page/美國.md "wikilink")[加州](../Page/加州.md "wikilink")[洛杉磯](../Page/洛杉磯.md "wikilink")）
  - [Mplay, Inc](../Page/Mplay,_Inc.md "wikilink")
  - [Xeogen](../Page/Xeogen.md "wikilink")
  - [Mobilehands](../Page/Mobilehands.md "wikilink")
  - [Wisekids](../Page/Wisekids.md "wikilink")
  - [Entelligent](../Page/Entelligent.md "wikilink")
  - [Wizet](../Page/Wizet.md "wikilink")
  - [NEOPLE](../Page/NEOPLE.md "wikilink")
  - [devCAT](../Page/devCAT.md "wikilink")
  - [What Studio](../Page/What_Studio.md "wikilink")
  - [Thingsoft](../Page/Thingsoft.md "wikilink")
  - [NDOORS](../Page/NDOORS.md "wikilink")
  - [Boolean Games](../Page/Boolean_Games.md "wikilink")
  - [Big Huge Games](../Page/Big_Huge_Games.md "wikilink")
  - [Fantage](../Page/Fantage.md "wikilink")

## 聯外合作

  - [遊戲橘子](../Page/遊戲橘子.md "wikilink")（獲得[爆爆王](../Page/爆爆王.md "wikilink")、[楓之谷](../Page/楓之谷.md "wikilink")、[泡泡大亂鬥](../Page/泡泡大亂鬥.md "wikilink")……等遊戲台灣代理權）
      - 台灣聯合報指出NEXON從2011年底占遊戲橘子股份不到3%到2012年4月底已高達33%，以外資身分從市場買下超過五萬張遊戲橘子股票。今年三月全球業者齊聚舊金山開遊戲者大會，NEXON就已私下放話將入主遊戲橘子。為了反制遊戲橘子被韓商惡意併購，遊戲產業振興會前會長、樂陞董事長許金龍日前已向公平交易委員會提出檢舉，指稱納克森的行為涉及事業結合，卻違法未事先向公平會申報，已造成不公平競爭，應被禁止結合。根據證交所資料，截至三月底，NEXON在渣打銀行的託管專戶中，共持有遊戲橘子五萬二千六百九十一張，持股比重高達百分之三十三；遠超過橘子董事長劉柏園等經營團隊持股的二成。遊戲橘子六月廿二日將召開股東會，四月底股票截止過戶日才發現，後
        NEXON 因策略考量釋出部分持股，對遊戲橘子的持股比例將從原本的 33% 下降到接近 14％。\[3\]\[4\]
  - [盛大網路](../Page/盛大網路.md "wikilink")（獲得[爆爆王](../Page/爆爆王.md "wikilink")、[楓之谷](../Page/楓之谷.md "wikilink")、[泡泡戰士中國代理權](../Page/泡泡戰士.md "wikilink")）
  - [巨人網路](../Page/巨人網路.md "wikilink")（獲得[艾爾之光中國代理權](../Page/艾爾之光.md "wikilink")）
  - [香港戲谷](../Page/戲谷.md "wikilink")（獲得[拿拿林Online香港代理權](../Page/拿拿林Online.md "wikilink")）
  - [台灣戲谷](../Page/戲谷.md "wikilink")（獲得[拿拿林Online台灣代理權](../Page/拿拿林Online.md "wikilink")）
  - [Garena](../Page/Garena.md "wikilink")（獲得[地下城與勇士台灣代理權](../Page/地下城與勇士.md "wikilink")）
  - [天宇科技](../Page/天宇科技.md "wikilink")（獲得[天翼之鍊](../Page/天翼之鍊.md "wikilink")、[神之領域台灣代理權](../Page/神之領域.md "wikilink")）
  - [紅心辣椒](../Page/紅心辣椒.md "wikilink")（獲得黑Online台灣代理權）
  - [夢工廠](../Page/夢工廠.md "wikilink")（Dream
    Exexution）（授權[獵殺戰場韓國代理權](../Page/獵殺戰場.md "wikilink")）
  - [Valve](../Page/Valve.md "wikilink")（共同製作[CSO系列](../Page/CSO.md "wikilink")）
  - [Ndoors](../Page/Ndoors.md "wikilink")
  - [Eyedentity
    Games](../Page/Eyedentity_Games.md "wikilink")（獲得[龍之谷韓國代理權](../Page/龍之谷.md "wikilink")）
  - [Softmax](../Page/Softmax_\(游戏开发商\).md "wikilink")（授權[天翼之鍊韓國與日本代理權](../Page/天翼之鍊.md "wikilink")）
  - [Toppig](../Page/Toppig.md "wikilink")（共同製作[拿拿林Online](../Page/拿拿林Online.md "wikilink")）
  - [KOG
    Studios](../Page/KOG_Studios.md "wikilink")（授權[永恆冒險韓國代理權](../Page/永恆冒險.md "wikilink")）
  - [Viacom](../Page/Viacom.md "wikilink")（製作[尼奧寵物的新增服飾櫃功能及NC](../Page/尼奧寵物.md "wikilink")
    Mall）
  - [世纪天成](../Page/世纪天成.md "wikilink")
  - [腾讯游戏](../Page/腾讯游戏.md "wikilink")（[冒险岛2](../Page/冒险岛2.md "wikilink")）

### 曾经合作

  - [GameHi](../Page/GameHi.md "wikilink")（共同製作黑Online），后因公司与NEXON子公司[Nextoric](../Page/Nextoric.md "wikilink")（[新天翼之鍊开发商](../Page/新天翼之链.md "wikilink")）合并为NEXON
    GT而不复存在\[5\]
  - [數碼戲胞](../Page/數碼戲胞.md "wikilink")（已倒閉）
  - [CJ E\&M](../Page/CJ_E&M.md "wikilink")（授權突擊風暴韓國代理權）

## 旗下遊戲

### 韓國

由NEXON Korea經營。

  - [2012:首爾](../Page/2012:首爾.md "wikilink")（2012: SEOUL）\[WebGame\]
  - [9Dragons](../Page/9Dragons.md "wikilink")
  - [神之領域](../Page/神之領域.md "wikilink")（Asgard）
  - [Baram](../Page/Baram.md "wikilink")
  - [絕對武力Online](../Page/絕對武力Online.md "wikilink")（Counter Strike
    Online）
  - [泡泡战士](../Page/泡泡战士.md "wikilink")（Crazy Shooting - Bubble Fighter）
  - [爆爆王](../Page/爆爆王.md "wikilink")（Crazy Arcade）
  - [Chronicles of Lunia](../Page/Chronicles_of_Lunia.md "wikilink")
  - [跑跑卡丁車](../Page/跑跑卡丁車.md "wikilink")（CrazyRacing KartRider）
  - [打擊軍火](../Page/打擊軍火.md "wikilink")（Combat Arms）
  - [地下城与勇士](../Page/地下城与勇士.md "wikilink")（Dungeon & Fighter）
  - [Elancia](../Page/Elancia.md "wikilink")
  - [艾尔之光](../Page/艾尔之光.md "wikilink")（[Elsword](../Page/Elsword.md "wikilink")）
  - [Ever Planet](../Page/Ever_Planet.md "wikilink")
  - [哈士奇速递](../Page/哈士奇速递.md "wikilink")（Husky Express）
  - [Legend of Darkness](../Page/Legend_of_Darkness.md "wikilink")
  - [路尼亞戰記](../Page/路尼亞戰記.md "wikilink")（Lunia戰記）
  - [瑪奇](../Page/瑪奇.md "wikilink")（Mabinogi）
  - [瑪奇英雄传](../Page/瑪奇英雄传.md "wikilink")（Mabinogi Heroes）
  - [楓之谷](../Page/楓之谷.md "wikilink")（MapleStory）
  - [拿拿林Online](../Page/拿拿林Online.md "wikilink")（QQ飞行岛）（Nanaimo）
  - [开心星球](../Page/开心星球.md "wikilink")（NEXON Star）
  - [Q-Play](../Page/Q-Play.md "wikilink")
  - [未來啟示錄Online](../Page/未來啟示錄Online.md "wikilink")（Silent Plot 1）
  - [天翼之鍊](../Page/天翼之鍊.md "wikilink")（TalesWeaver）
  - [热血三国](../Page/热血三国.md "wikilink")【网页游戏】（Three Country）\[WebGame\]
  - [War Rock](../Page/War_Rock.md "wikilink")
  - [ZerA: Imperan
    Intrigue](../Page/ZerA:_Imperan_Intrigue.md "wikilink")
  - [第四区](../Page/第四区.md "wikilink")（Zone 4）
  - [突袭](../Page/突击风暴.md "wikilink")（Sudden Attack）
  - [楓之谷 DS](../Page/楓之谷_DS.md "wikilink")（MapleStory DS）(NDSL遊戲)
  - [獵殺戰場](../Page/獵殺戰場.md "wikilink") (War Rock)
  - [暴能特區](../Page/暴能特區.md "wikilink")（Cyphers）
  - [楓之谷2](../Page/楓之谷2.md "wikilink")（MapleStory 2）
  - [CLOSERS](../Page/CLOSERS.md "wikilink")

### 日本

由NEXON Japan經營。

  - [アラド戦記](../Page/アラド戦記.md "wikilink")（Arad/Dungeon & Fighter）
  - [王者世界](../Page/王者世界.md "wikilink")（Atlantica）
  - [Colony of War](../Page/Colony_of_War.md "wikilink")
  - [反恐精英Online](../Page/反恐精英Online.md "wikilink")（Counter Strike
    Online）
  - [闇之傳說](../Page/闇之傳說.md "wikilink")（；Dark Ages）
  - [Dekaron](../Page/Dekaron.md "wikilink")
  - [DRAGONICA](../Page/DRAGONICA.md "wikilink")
  - [EverPlanet](../Page/EverPlanet.md "wikilink")
  - [路尼亞戰記](../Page/路尼亞戰記.md "wikilink")（Lunia）
  - [瑪奇](../Page/瑪奇.md "wikilink")（Mabinogi）
  - [楓之谷](../Page/楓之谷.md "wikilink")（MapleStory）
  - [NTOMO](../Page/NTOMO.md "wikilink")
  - [突袭](../Page/突击风暴.md "wikilink")（Sudden Attack）
  - [天翼之鍊](../Page/天翼之鍊.md "wikilink")（TalesWeaver）
  - [铁鬼](../Page/铁鬼.md "wikilink")（Tekki）
  - [科隆](../Page/科隆.md "wikilink")（Corum Online）

### 北美

由NEXON America經營。

  - [王者世界](../Page/王者世界.md "wikilink")（Atlantica Online）
  - [打擊軍火](../Page/打擊軍火.md "wikilink")（Combat Arms）
  - [龍之谷](../Page/龍之谷.md "wikilink")（Dragon Nest）
  - [地下城与勇士](../Page/地下城与勇士.md "wikilink")（Dungeon Fighter Online）
  - [瑪奇](../Page/瑪奇.md "wikilink")（Mabinogi）
  - [楓之谷](../Page/楓之谷.md "wikilink")（MapleStory）
  - [瑪奇英雄传](../Page/瑪奇英雄传.md "wikilink")（Vindictus/Mabinogi:Heroes）

### 泰國

由NEXON Thailand經營。

  - [楓之谷](../Page/楓之谷.md "wikilink")（MapleStory）
  - [救世者之樹](../Page/救世者之樹.md "wikilink")（Tree of Savior）

### 臺灣

由Nexon Taiwan經營。

  - 天涯明月刀

### 手機遊戲

  - [文明爭戰](../Page/文明爭戰.md "wikilink")
  - [HIT 英雄之戰](../Page/HIT_英雄之戰.md "wikilink")（待停運）
  - [三國志曹操傳](../Page/三國志曹操傳.md "wikilink")
  - [真·三國無雙·斬製作公司](../Page/真·三國無雙·斬.md "wikilink")：[樂陞科技授權商](../Page/樂陞科技.md "wikilink")：日本[光榮](../Page/光榮.md "wikilink")
  - [暗黑復仇者3](../Page/暗黑復仇者3.md "wikilink")
  - [楓之谷M](../Page/楓之谷M.md "wikilink")
  - [楓之谷突擊](../Page/楓之谷突擊.md "wikilink")
  - [無限戰場](../Page/無限戰場.md "wikilink")
  - [爆爆王M](../Page/爆爆王M.md "wikilink")

## 相關電視節目

  - 2018年10月13日：[MBC](../Page/MBC.md "wikilink")《[公司食堂](../Page/公司食堂.md "wikilink")》EP10

## 參考資料

## 参考文献

  - Nexon公司信息：附属公司
    <http://company.nexon.com/english/about/affiliate.htm>
  - 上海邮通科技《洛奇》产品发布暨公测开通发布会董事长致辞：[2](http://home.donews.com/donews/article/7/79993.html)
  - [遊戲橘子](../Page/遊戲橘子.md "wikilink")，NEXON的合作伙伴。

<!-- end list -->

  - 布局东南亚 Nexon收购泰国游戏发行商iDCC
    [3](http://games.qq.com/a/20161115/019666.htm)

## 外部链接

  - [Nexon官方网站（用户向）](http://www.nexon.com/)
  - [Nexon
    公司信息站（韩文](http://company.nexon.com/)／[英文](http://company.nexon.com/english/)）
  - [Nexon韩国官方网站](http://www.nexon.com/)
  - [Nexon日本官方网站](http://www.nexon.co.jp/)
  - [Nexon北美官方网站](http://www.nexon.net/)
  - [Nexon泰国官方网站](https://th.nexon.com/home/th/)
  - [Nexon台湾官方网站](https://tw.nexon.com/main/zh)
  - [nxc韓國官方网站](http://nxc.com/)

[Category:韓國電子遊戲公司](../Category/韓國電子遊戲公司.md "wikilink")
[Category:總部在日本的跨國公司](../Category/總部在日本的跨國公司.md "wikilink")
[Category:1994年開業電子遊戲公司](../Category/1994年開業電子遊戲公司.md "wikilink")
[Category:電子遊戲開發公司](../Category/電子遊戲開發公司.md "wikilink")
[Category:電子遊戲公司](../Category/電子遊戲公司.md "wikilink")
[Category:電子遊戲發行商](../Category/電子遊戲發行商.md "wikilink") [Category:中央區公司
(東京都)](../Category/中央區公司_\(東京都\).md "wikilink")

1.  [1](http://company.nexon.com/Eng/Company/AboutUs/History/History_1990s.aspx)
2.  [納克森執行長崔承祐：從第一天起
    就要走出韓國](http://www.cw.com.tw/article/article.action?id=5044491)
3.  [韓商覬覦遊戲橘子 驚爆惡意併購](http://udn.com/NEWS/FINANCE/FIN7/7090582.shtml)
4.  [NEXON 釋出遊戲橘子部分持股
    遊戲橘子核心營運團隊成為橘子最大股東](https://gnn.gamer.com.tw/1/92151.html)
5.