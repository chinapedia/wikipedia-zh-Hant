**Ng**，或**NG**、**ng**可能指：

## 語言

### 二合字母

  - 在[台灣原住民族語中](../Page/台灣南島語言.md "wikilink")，ng是一個單獨的字母，代表  音。
  - 在[闽南语中](../Page/闽南语.md "wikilink")，ng是一個單獨的字母，代表着韻化輔音[軟顎鼻音](../Page/軟顎鼻音.md "wikilink")。讀作。\[1\]
  - 在[紐西蘭](../Page/紐西蘭.md "wikilink")[毛利語](../Page/毛利語.md "wikilink")，ng是一個單獨的字母。
  - 在[他加祿語](../Page/他加祿語.md "wikilink")，ng亦是一個單獨的字母，讀作"*Nang*"。
  - 在其他南太平洋語言中， 音有時拼作"ng"，亦有時拼作"[G](../Page/G.md "wikilink")"。
  - 在[非洲和](../Page/非洲.md "wikilink")[北美原住民的語言](../Page/北美.md "wikilink")，通常都會用[Ŋ
    ŋ來取代](../Page/Ŋ.md "wikilink")“ng”。
  - 在[阿拉伯字母中](../Page/阿拉伯字母.md "wikilink")，ng作為[ڭ的轉寫](../Page/ڭ.md "wikilink")。
  - 在[西里爾字母中](../Page/西里爾字母.md "wikilink")，ng可能作為以下字母的轉寫：
      - [Ң
        ң](../Page/Ң.md "wikilink")，用於[哈薩克語](../Page/哈薩克語.md "wikilink")、[吉爾吉斯語](../Page/吉爾吉斯語.md "wikilink")、[韃靼語](../Page/韃靼語.md "wikilink")、[巴什基爾語](../Page/巴什基爾語.md "wikilink")、[卡爾梅克語](../Page/卡爾梅克語.md "wikilink")、[圖瓦語](../Page/圖瓦語.md "wikilink")、[哈卡斯語](../Page/哈卡斯語.md "wikilink")、[維吾爾語](../Page/維吾爾語.md "wikilink")、[東干語和](../Page/東干語.md "wikilink")等。
      - [Ӈ
        ӈ](../Page/Ӈ.md "wikilink")，用於[楚科奇語](../Page/楚科奇語.md "wikilink")、[漢特語](../Page/漢特語.md "wikilink")、[曼西語](../Page/曼西語.md "wikilink")、[阿留特語與](../Page/阿留特語.md "wikilink")[基爾丁薩米語等](../Page/基爾丁薩米語.md "wikilink")[楚科奇－堪察加语系與](../Page/楚科奇－堪察加语系.md "wikilink")[薩莫耶德語族中的語言](../Page/薩莫耶德語族.md "wikilink")。
      - [Ҥ
        ҥ](../Page/Ҥ.md "wikilink")，用於[雅庫特語](../Page/雅庫特語.md "wikilink")、[阿爾泰語和](../Page/阿爾泰語.md "wikilink")[東馬里語等](../Page/東馬里語.md "wikilink")。

### 語言名稱

  - [恩敦加語](../Page/恩敦加語.md "wikilink")，的標準方言，其[ISO
    639-1代碼為](../Page/ISO_639-1.md "wikilink")`ng`。

## 人名

  - Ng可能為以下中文姓氏的轉寫：
      - [粤语](../Page/粤语.md "wikilink")、[上海話](../Page/上海話.md "wikilink")、[客家話的](../Page/客家話.md "wikilink")[吳](../Page/吳.md "wikilink")、[伍](../Page/伍姓.md "wikilink")。
      - [客家話](../Page/客家話.md "wikilink")、[潮州話](../Page/潮州話.md "wikilink")、[闽南语的](../Page/闽南语.md "wikilink")[黄](../Page/黄姓.md "wikilink")。
      - [闽南语](../Page/闽南语.md "wikilink")、[潮州話的](../Page/潮州話.md "wikilink")[阮](../Page/阮姓.md "wikilink")。

## 地區

  - [尼日](../Page/尼日.md "wikilink")，其在[聯邦資料處理標準](../Page/聯邦資料處理標準.md "wikilink")（FIPS）中的代碼為`NG`。

  - [奈及利亞](../Page/奈及利亞.md "wikilink")，其[ISO國家代碼為](../Page/ISO_3166-1.md "wikilink")`NG`。

      - [.ng](../Page/.ng.md "wikilink")，為奈及利亞[國家及地區頂級域](../Page/國家及地區頂級域.md "wikilink")（ccTLD）的[域名](../Page/域名.md "wikilink")。

  - [納粹德國](../Page/納粹德國.md "wikilink")，英文Nazi Germany之縮寫。

  - ，為一組在[英國](../Page/英國.md "wikilink")[諾丁漢附近使用的](../Page/諾丁漢.md "wikilink")[郵遞區號](../Page/郵遞區號.md "wikilink")。

## 科學與科技

  - [奈克](../Page/奈克.md "wikilink")，符號ng（），[質量單位之一](../Page/質量單位.md "wikilink")。
  - [天然氣](../Page/天然氣.md "wikilink")，英文natural gas之縮寫。
  - [稀有氣體](../Page/稀有氣體.md "wikilink")，英文noble gas之縮寫。
  - [硝酸甘油](../Page/硝酸甘油.md "wikilink")，英文nitroglycerin之縮寫。
  - 英语Next Generation（下一代）的缩写。常在软件或电子、通信产品的名称中使用。

## 其他

  - [國家地理](../Page/國家地理.md "wikilink")（National Geographic）的縮寫
  - [NG (影視用語)](../Page/NG_\(影視用語\).md "wikilink")，是「**N**o
    **G**ood」的簡稱。

## 參看

  - [нг](../Page/нг.md "wikilink")

## 註釋

{{-}}

[Category:二合字母](../Category/二合字母.md "wikilink")

1.  董峰政,"臺語通用拼音字典",臺南市寧南語言文化協會,[臺南市](../Page/臺南市.md "wikilink"),2006年7月.