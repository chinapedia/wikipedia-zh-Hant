}  **Nine.com.au**
是[澳洲的一家由](../Page/澳洲.md "wikilink")[Microsoft和](../Page/Microsoft.md "wikilink")[PBL
Media各出資一半的合資企業](../Page/PBL_Media.md "wikilink")。它是[澳洲最受歡迎的網站](../Page/澳洲.md "wikilink")，其在澳大利亚的影响力类似于[Nine
Network和](../Page/Nine_Network.md "wikilink")[MSN](../Page/MSN.md "wikilink")。它的流行很大程度上歸因於它是澳洲[Internet
Explorer](../Page/Internet_Explorer.md "wikilink") 6
用戶的默認首頁，同時也是當[澳洲用戶登出](../Page/澳洲.md "wikilink")[Windows
Live Hotmail后自動出現的網站](../Page/Windows_Live_Hotmail.md "wikilink")。

該企業成立于1997年，擁有合資$50，000，000，集資于所有微軟的綫上資產和PBL的媒體資產，包括[Nine
Network](../Page/Nine_Network.md "wikilink")，[Australian Consolidated
Press](../Page/Australian_Consolidated_Press.md "wikilink") (ACP)
和其它PBL資產。

[2006年4月时](https://web.archive.org/web/20080613173443/http://mediacentre.ninemsn.com.au/article.aspx?id=94127)，ninemsn收购了澳大利亚在内容聚合以及移动出版业务上的领导者[HWW
Limited公司](../Page/HWW_Limited.md "wikilink")，并由此开展了電視、電影、音樂、餐廳等业务.

## 外部連結

  - [ninemsn](http://ninemsn.com.au)

[Category:MSN](../Category/MSN.md "wikilink") [Category:Nine
Network](../Category/Nine_Network.md "wikilink") [Category:Web
portals](../Category/Web_portals.md "wikilink")
[Category:澳大利亞網站](../Category/澳大利亞網站.md "wikilink")