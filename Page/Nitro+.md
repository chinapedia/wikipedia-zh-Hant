**株式會社Nitro+**（；）是一間[電腦軟體製作商和](../Page/電腦軟體.md "wikilink")[電腦遊戲](../Page/電腦遊戲.md "wikilink")[角色設計公司](../Page/角色.md "wikilink")。總公司位於[日本](../Page/日本.md "wikilink")[東京都](../Page/東京都.md "wikilink")[千代田區](../Page/千代田區.md "wikilink")。

## 概要

Nitro+與、[0verflow組成](../Page/0verflow.md "wikilink")，代表是。姊妹公司有製作的[Nitro+CHiRAL](../Page/Nitro+CHiRAL.md "wikilink")（ニトロプラスキラル）。

## 主要作品列表

  - 2000年2月25日 - [Phantom -PHANTOM OF
    INFERNO-](../Page/Phantom_-PHANTOM_OF_INFERNO-.md "wikilink")
  - 2001年1月26日 - [吸血殲鬼](../Page/吸血殲鬼.md "wikilink")
  - 2002年3月29日 - [鬼哭街](../Page/鬼哭街.md "wikilink")
  - 2002年9月27日 - ["Hello,
    world."](../Page/"Hello,_world.".md "wikilink")
  - 2003年4月25日 -
  - 2003年12月26日 - [沙耶之歌](../Page/沙耶之歌.md "wikilink")
  - 2004年9月17日 - [Phantom
    INTEGRATION](../Page/Phantom_-PHANTOM_OF_INFERNO-.md "wikilink")
  - 2005年1月28日 -
  - 2005年2月25日 - [咎狗之血](../Page/咎狗之血.md "wikilink")(Nitro+CHiRAL)
  - 2005年6月24日 - [塵骸魔京](../Page/塵骸魔京.md "wikilink")
  - 2005年9月30日 -
  - 2006年2月3日 -
  - 2006年5月26日 -
  - 2006年8月24日 - [字禱子D 妖都最速傳説](../Page/字禱子D_妖都最速傳説.md "wikilink")
  - 2006年10月27日 - [Lamento -BEYOND THE
    VOID-](../Page/Lamento_-BEYOND_THE_VOID-.md "wikilink")（Nitro+CHiRAL）
  - 2007年1月26日 - [月光的嘉年華](../Page/月光的嘉年華.md "wikilink")
  - 2007年7月27日 -
  - 2007年9月28日 - [ニトロ+ロワイヤル
    -ヒロインズデュエル-](../Page/ニトロ+ロワイヤル_-ヒロインズデュエル-.md "wikilink")
  - 2008年4月25日 - [CHAOS;HEAD](../Page/CHAOS;HEAD.md "wikilink")
  - 2008年9月26日 - [STAR MINE GIRL](../Page/STAR_MINE_GIRL.md "wikilink")
  - 2008年12月19日 - [sweet
    pool](../Page/sweet_pool.md "wikilink")（Nitro+CHiRAL）
  - 2009年6月26日 - [SUMAGA SPECIAL](../Page/STAR_MINE_GIRL.md "wikilink")
  - 2009年10月15日 - [Steins;Gate](../Page/Steins;Gate.md "wikilink")
  - 2009年10月30日 - [裝甲惡鬼村正](../Page/裝甲惡鬼村正.md "wikilink")
  - 2010年8月26日 - [装甲悪鬼村正邪念編](../Page/裝甲惡鬼村正.md "wikilink")
  - 2010年12月17日 - [Axanael](../Page/Axanael.md "wikilink")
  - 2011年11月25日 - [ソニコミ](../Page/SUPERSONICO.md "wikilink")
  - 2012年3月23日 - [DRAMAtical
    Murder](../Page/DRAMAtical_Murder.md "wikilink")（Nitro+CHiRAL）
  - 2012年6月28日 - [ROBOTICS;NOTES](../Page/ROBOTICS;NOTES.md "wikilink")
  - 2012年7月26日 - [罪惡王冠：失落的聖誕](../Page/罪惡王冠.md "wikilink")
  - 2012年10月25日 - [Phantom -PHANTOM OF
    INFERNO-](../Page/Phantom_-PHANTOM_OF_INFERNO-.md "wikilink")(Xbox360移植版)
  - 2013年6月28日 - [你和她和她的戀愛。](../Page/你和她和她的戀愛。.md "wikilink")
  - 2014年12月18日 - [CHAOS;CHILD](../Page/CHAOS;CHILD.md "wikilink")
  - 2015年12月10日 - Steins;Gate 0
    (詳見[Steins;Gate](../Page/Steins;Gate.md "wikilink"))
  - 2016年1月29日 -

### 製作中作品

  - 發售日未定 -

### 參與設計作品

  - （動畫（武士團製作）：機械設計担当）

  - [Muv-Luv](../Page/Muv-Luv.md "wikilink")（十八禁遊戲（âge製作）：機械設計協力）

  - [Muv-Luv
    Alternative](../Page/Muv-Luv_Alternative.md "wikilink")（十八禁遊戲（âge製作）：機械設計協力）

  - [夜明前的琉璃色](../Page/夜明前的琉璃色.md "wikilink")（十八禁遊戲（[August製作](../Page/August.md "wikilink")）：3DCG、設計協力）

  - [BLASSREITER-genetic-](../Page/BLASSREITER-genetic-.md "wikilink")（動畫，與（[GONZO共同製作](../Page/GONZO.md "wikilink")）：系列構成、人物設計原案、機械設計原案協力）

  - [Thunderbolt Fantasy
    東離劍遊紀](../Page/Thunderbolt_Fantasy_東離劍遊紀.md "wikilink")(編劇、角色原案)

  - [刀劍亂舞](../Page/刀劍亂舞.md "wikilink")（線上網頁遊戲、與DMM.com共同製作：原作、人物設計）

## 主要工作人員

  - 製作人：
  - 製作指揮：[虛淵玄](../Page/虛淵玄.md "wikilink")
  - 製作進行：
  - 監督：虛淵玄、、
  - 劇本：虛淵玄、、、、
  - 原畫：、、[なまにくATK](../Page/なまにくATK.md "wikilink")
  - 圖像：、津路参太、、、、、、、、、
  - SD角色原畫：
  - 設計：yoshiyuki、Mzk
  - 網站設計：
  - 美術指導：難中亭多樂
  - 3DCG：、、、K、G=2007
  - 程式腳本：徒歩十分
  - 品質管理：、
  - 開發：usao.exe、
  - 廣告：、
  - Nitro+直屬（通販）店長：不動

### 前工作人員

  - 原畫：[中央東口](../Page/中央東口.md "wikilink")、[矢野口君](../Page/矢野口君.md "wikilink")、[Niθ](../Page/Niθ.md "wikilink")
  - 圖像：
  - 廣告、營業：

## 参考文献

  - [作品列表](https://web.archive.org/web/20080327232808/http://www.nitroplus.co.jp/pc/lineup/)

  -
## 外部連結

  - [Nitro+](https://web.archive.org/web/20090110114236/http://www.nitroplus.co.jp/pc/)

{{-}}

[Nitro+](../Category/Nitro+.md "wikilink")
[Category:日本電子遊戲公司](../Category/日本電子遊戲公司.md "wikilink")
[Category:日本成人遊戲公司](../Category/日本成人遊戲公司.md "wikilink")