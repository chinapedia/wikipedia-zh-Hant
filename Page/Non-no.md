《**non-no**》（）是[日本](../Page/日本.md "wikilink")[女性](../Page/女性.md "wikilink")，由[集英社發行](../Page/集英社.md "wikilink")。創刊於1971年5月25日，每[月發行一次](../Page/月.md "wikilink")。

## 概要

雜誌名稱「non-no」源自[阿伊努語中](../Page/阿伊努語.md "wikilink")「花」的意思。其為日本發行時間最久、且引領[潮流的時尚雜誌之一](../Page/潮流.md "wikilink")。長期的競爭對手為《》。其許多受歡迎的專屬模特兒均成功轉型為[演員或](../Page/演員.md "wikilink")[藝人](../Page/藝人.md "wikilink")。原本的編輯方針是針對[大學生](../Page/大學生.md "wikilink")、社會新鮮人等剛[成年的女性族群](../Page/成年.md "wikilink")，自2005年左右，較大學生年長的年齡層也成了目標對象。

其原本一個月發行2回（毎月5日、20日發售），2010年9月18日發售的11月號起改為每月發行1回（[月刊](../Page/月刊.md "wikilink")）。

海外銷售方面，在[香港](../Page/香港.md "wikilink")，除專賣日語書籍的[書店外](../Page/書店.md "wikilink")，在一些[書報攤均可找得到](../Page/報攤.md "wikilink")；[台灣則在日語專門書店](../Page/台灣.md "wikilink")、雜誌專賣店、以及各大連鎖書店均可買到。

## 模特兒

  -
    ※專屬契約者另以「○」標示。

### 現在活躍中

  - [本田翼](../Page/本田翼.md "wikilink")（2010年3月號 - ）○
  - [大政絢](../Page/大政絢.md "wikilink")（2011年1月號 - ）○
  - [岡本梓](../Page/岡本梓.md "wikilink")（2012年8月號 - ）○
  - [鈴木友菜](../Page/鈴木友菜.md "wikilink")（2014年5月號 - ）○
  - [岡本杏理](../Page/岡本杏理.md "wikilink")（2015年6月號 - ）○
  - [上田真央](../Page/上田真央.md "wikilink")（2011年3月號 - ）○
  - [岡田紗佳](../Page/岡田紗佳.md "wikilink")（2012年2月號 - ）○
  - [竹富聖花](../Page/竹富聖花.md "wikilink")（2013年6月號 - ）○
  - [木下日名子](../Page/木下日名子.md "wikilink")（2013年11月號 - ）○
  - [泉春](../Page/泉春.md "wikilink")（2013年11月號 - ）○
  - [佐藤美希](../Page/佐藤美希.md "wikilink")（2014年3月號 - ）○
  - [新木優子](../Page/新木優子.md "wikilink")（2014年3月號 - ）○
  - [久慈暁子](../Page/久慈暁子.md "wikilink")（2014年11月號 - ）○
  - [鈴木優華](../Page/鈴木優華.md "wikilink")（2014年11月號 - ）○
  - [遠藤新菜](../Page/遠藤新菜.md "wikilink")（2014年11月號 - ）○
  - [高田里穗](../Page/高田里穗.md "wikilink")（2014年11月號 - ）○
  - [姜知英](../Page/姜知英.md "wikilink")（2014年12月號 - ）
  - [西野七濑](../Page/西野七濑.md "wikilink")（[乃木坂46](../Page/乃木坂46.md "wikilink")）（2015年6月號-）○
  - [馬場富美香](../Page/馬場富美香.md "wikilink")（2015年6月號 - ）○
  - [佐谷戸ミナ](../Page/佐谷戸ミナ.md "wikilink")（2015年6月號 - ）○
  - [新川優愛](../Page/新川優愛.md "wikilink")（2015年8月號 - ）○
  - [武田玲奈](../Page/武田玲奈.md "wikilink")（2016年6月号 - ）○\[1\]
  - [金城茉奈](../Page/金城茉奈.md "wikilink")（2016年6月号 - ）○\[2\]
  - [佐藤エリ](../Page/佐藤エリ.md "wikilink")（2016年6月号 - ）○\[3\]
  - [松川菜々花](../Page/松川菜々花.md "wikilink")（2017年6月号 - ）○\[4\]
  - [山田愛奈](../Page/山田愛奈.md "wikilink")（2017年6月号 - ）○\[5\]
  - [渡边理佐](../Page/渡边理佐.md "wikilink")（[欅坂46](../Page/欅坂46.md "wikilink")、2017年6月号
    - ）○\[6\]

### 以前活躍的模特兒

  - [栗原景子](../Page/栗原景子.md "wikilink")
  - [涼](../Page/涼_\(演員\).md "wikilink")
  - [Hana](../Page/Hana_\(藝人\).md "wikilink")
  - [加藤幸子](../Page/加藤幸子_\(模特兒\).md "wikilink")
  - [SHIHO（志保）](../Page/SHIHO.md "wikilink")
  - [未希](../Page/未希.md "wikilink")
  - [夏川結衣](../Page/夏川結衣.md "wikilink")
  - [松雪泰子](../Page/松雪泰子.md "wikilink")
  - [西田尚美](../Page/西田尚美.md "wikilink")
  - [小雪](../Page/小雪_\(日本演員\).md "wikilink")
  - [甘糟記子](../Page/甘糟記子.md "wikilink")
  - [山口泉](../Page/山口泉_\(模特兒\).md "wikilink")
  - [尾形幸子](../Page/尾形幸子.md "wikilink")
  - [松島洋子](../Page/松島洋子.md "wikilink")
  - [AYUMI](../Page/AYUMI_\(模特兒\).md "wikilink")
  - [各務沙羅](../Page/各務沙羅.md "wikilink")
  - [EMI](../Page/EMI_\(模特兒\).md "wikilink")
  - [菅井悦子](../Page/菅井悦子.md "wikilink")
  - [五明祐子](../Page/五明祐子.md "wikilink")
  - [佐藤康惠](../Page/佐藤康惠.md "wikilink")
  - [梓](../Page/竹花梓.md "wikilink")
  - [吉野友華梨](../Page/吉野友華梨.md "wikilink")
  - [長谷川ミキ](../Page/長谷川ミキ.md "wikilink")
  - [藤澤恵麻](../Page/藤澤恵麻.md "wikilink")
  - [菊池亜希子](../Page/菊池亜希子.md "wikilink")
  - [松島花](../Page/松島花.md "wikilink")
  - [永野桃子](../Page/黑木桃子.md "wikilink")（現名黑木桃子）　
  - [金月繪里](../Page/金月繪里.md "wikilink")
  - [安座間美優](../Page/安座間美優.md "wikilink")（移籍[CanCam](../Page/CanCam.md "wikilink")）
  - [斎藤恵美](../Page/斎藤恵美.md "wikilink")
  - [津村絵美](../Page/津村絵美.md "wikilink")
  - [ジョジ後藤](../Page/ジョジ後藤.md "wikilink")
  - [松沼リナ](../Page/松沼リナ.md "wikilink")
  - [愛可](../Page/愛可.md "wikilink")
  - [津田真美江](../Page/津田真美江.md "wikilink")
  - [谷口小波](../Page/谷口小波.md "wikilink")
  - [蜂須賀太江子](../Page/蜂須賀太江子.md "wikilink")
  - [三上純子](../Page/三上純子.md "wikilink")
  - [森下紀子](../Page/森下紀子.md "wikilink")
  - [鶴見扶美江](../Page/鶴見扶美江.md "wikilink")
  - [高崎晶子](../Page/高崎晶子.md "wikilink")
  - ナオ
  - [佐藤彌生](../Page/佐藤彌生_\(模特兒\).md "wikilink")
  - [田中伸子](../Page/田中伸子.md "wikilink")
  - [杏](../Page/杏_\(演員\).md "wikilink")
  - [河本麻希](../Page/河本麻希.md "wikilink")
  - [JOSI](../Page/JOSI_\(模特兒\).md "wikilink")
  - [竹下玲奈](../Page/竹下玲奈.md "wikilink")
  - [石井美繪子](../Page/石井美繪子.md "wikilink")
  - [田中愛里](../Page/田中愛里.md "wikilink")
  - [福田明子](../Page/福田明子.md "wikilink")
  - [長井梨紗](../Page/長井梨紗.md "wikilink")
  - [比留川游](../Page/比留川游.md "wikilink")
  - [ikumi](../Page/ikumi.md "wikilink")
  - [森貴美子](../Page/森貴美子.md "wikilink")
  - [山本純子](../Page/山本純子.md "wikilink")
  - [山本佑美](../Page/山本佑美.md "wikilink")
  - [古川美有](../Page/古川美有.md "wikilink")
  - [美優](../Page/美優.md "wikilink")
  - [高橋繪美](../Page/高橋繪美.md "wikilink")
  - [大森美知](../Page/大森美知.md "wikilink")
  - [照屋愛美](../Page/照屋愛美.md "wikilink")
  - [渡邊早織](../Page/渡邊早織.md "wikilink")
  - [菜菜緒](../Page/菜菜緒.md "wikilink")
  - [矢野未希子](../Page/矢野未希子.md "wikilink")（2008年2月號 - 2011年1月號）
  - [田中美保](../Page/田中美保_\(模特兒\).md "wikilink")（2000年 - 2012年5月號）
  - [野崎萌香](../Page/野崎萌香.md "wikilink")（2012年5月號畢業）
  - [赤谷奈緒子](../Page/赤谷奈緒子.md "wikilink")（2012年5月號畢業）
  - [佐藤栞里](../Page/佐藤栞里.md "wikilink")（2010年5/20號 - 2012年8月號）
  - [七菜香](../Page/七菜香.md "wikilink")（2010年4/5號 - 2012年8月號）
  - [佐佐木希](../Page/佐佐木希.md "wikilink")（2010年2/5號 - 2013年5月號）
  - [西田有沙](../Page/西田有沙.md "wikilink")（2011年3月號 - 2013年5月號）
  - [岩本乃蒼](../Page/岩本乃蒼.md "wikilink")（2011年3月號 - 2013年5月號）
  - [大島渚](../Page/大島渚_\(模特兒\).md "wikilink")（2011年3月號 - 2013年5月號）
  - [上間美緒](../Page/上間美緒.md "wikilink")（2012年2月號 - 2013年5月號）
  - [水澤惠麗奈](../Page/水澤惠麗奈.md "wikilink")（2011年6月號 - 2013年6月號）
  - [日南響子](../Page/日南響子.md "wikilink")（2011年6月號　- 2013年10月號）
  - [SAORI](../Page/SAORI_\(1989年生\).md "wikilink")（2012年2月號 - 2014年6月號）
  - [三根梓](../Page/三根梓.md "wikilink")（2012年7月號 - 2014年11月號）
  - [佐藤亞璃紗](../Page/佐藤亞璃紗.md "wikilink")（2010年4/20號 - 2014年9月號）
  - [未來穗香](../Page/未來穗香.md "wikilink")（2013年11月號 - 2014年10月號）
  - [荒井萌](../Page/荒井萌.md "wikilink")（2012年2月號 - 2014年11月號）
  - [岸本聖紫瑠](../Page/岸本聖紫瑠.md "wikilink")（2008年7/20號 - 2015年2月號）
  - [波瑠](../Page/波瑠.md "wikilink")（2012年6月號 - 2015年5月號）
  - [桐谷美玲](../Page/桐谷美玲.md "wikilink")（2012年3月號 - 2015年6月號）
  - [二村祥繪](../Page/二村祥繪.md "wikilink")（2013年6月號 - 2015年6月號）

## 面試會

  - 第32回（2001年）
      - 優勝 - 藤澤惠麻
  - 第33回（2002年）
      - 優勝 - 尾形幸子、松島洋子（上回FRIEND）
  - 第34回（2003年）
      - 優勝 - 河本麻希
  - 第35回（2004年）
      - 優勝 - 沒有相符合者
      - nonno·FRIEND - 古川美有、金月繪里
  - 第36回（2005年）
      - 沒有相符合者
  - 第37回（2005年）
      - 優勝 - 永野桃子（現在為 黒木桃子）
      - ハーバルエッセンシャル賞・Beauty Model - 田中愛里
      - nonno·FRIEND - 真柴有希、鈴木莉紗、岡田茉奈
  - 第38回（2006年）
      - 優勝 - [安座間美優](../Page/安座間美優.md "wikilink")　
      - ハーバルエッセンシャル賞・Beauty Model - 長井梨紗
      - nonno·FRIEND - 吉原亞希子、齋藤菜ツ美、小林瑤
      - 挑戰隊 - 下田美咲、藤田有里、小林恵里、山本智恵
  - 第39回（2007年）
      - 優勝 - 高橋繪美　
      - ハーバルエッセンシャル賞・Beauty Model - 大橋寛子
      - nonno·FRIEND - 小林惠里（上回挑戰隊）、佐藤咲也子、筒井麻未、檜山由貴奈、渡邊早織
      - 挑戰隊 - 三ケ田カレン、佐藤友里、八木菜々花、桑原ゆかり
      - nonno·BOYFRIEND - 鈴木雄貴
  - 第40回（2008年）
      - 優勝 - 沒有相符合者
      - ハーバルエッセンシャル賞・Beauty Model - 森山沙月
      - nonno·FRIEND - 日向千步
      - 挑戰隊 - 高野祐未、松居茉奈（兩名從2010年開始成為nonno·FRIEND）
      - nonno·BOYFRIEND - 笹塚麻朝
  - 第41回（2009年）
      - 優勝 - 照屋愛美
      - ハーバルエッセンシャル賞・Beauty Model - 高瀬リエ
      - nonno·FRIEND - 野崎萌香、山本佑美、堤ゆきみ（3名都從2010年開始成為nonno·Model）
      - 挑戰隊 - 小松愛唯、上木理恵子、猪鼻ちひろ、岡村いずみ、三村有希（5名都從2010年開始成為nonno·FRIEND）
  - 第42回（2010年）
      - 優勝 - 岩本乃蒼
      - 準優勝 - 上田眞央
      - 審査員特別賞 - 西田有沙、大島なぎさ

<!-- end list -->

  - 第43回（2011年）
      - 優勝 - 岡田紗佳
      - 準優勝 - 荒井萌
      - 審査員特別賞 - 上間美緒、SAORI
      - クリニークフレッシュフェイス賞 - 岡田紗佳

## 參見

  - [MEN'S NON-NO](../Page/MEN'S_NON-NO.md "wikilink")（non-no的男性版）
  - [Seventeen (雜誌)](../Page/Seventeen_\(雜誌\).md "wikilink")

## 參考資料

## 外部連結

  - [non-no Web](https://nonno.hpplus.jp/)

[Category:集英社的雜誌](../Category/集英社的雜誌.md "wikilink")
[Category:服裝](../Category/服裝.md "wikilink")
[Category:時尚雜誌](../Category/時尚雜誌.md "wikilink")

1.

2.
3.
4.
5.
6.