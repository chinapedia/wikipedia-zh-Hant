**Notepad2**是发布在[Microsoft
Windows平台下的](../Page/Microsoft_Windows.md "wikilink")[开放源代码的](../Page/开放源代码.md "wikilink")[文本编辑器](../Page/文本编辑器.md "wikilink")。于2004年4月推出第一个版本。作者参照了[微软的](../Page/微软.md "wikilink")[Notepad原则](../Page/Notepad.md "wikilink")：小巧、快速、朴实。自3.0.20版起，Notepad2已經變成完全基於[Unicode的程式](../Page/Unicode.md "wikilink")，因此對各種[字元的處理有很良好的表現](../Page/字元.md "wikilink")。由于Notepad2在资源消耗方面与[微软的](../Page/微软.md "wikilink")[Notepad大体相当](../Page/Notepad.md "wikilink")，而功能更强大，网上亦有人发起用Notepad2或[Notepad++替换Notepad的活动](../Page/Notepad++.md "wikilink")。

Notepad2为以下的程序语言提供[语法高亮标](../Page/语法高亮.md "wikilink")-{注}-：[ASP](../Page/Active_Server_Pages.md "wikilink")、[C语言](../Page/C语言.md "wikilink")、[C++](../Page/C++.md "wikilink")、[C\#](../Page/C♯.md "wikilink")、[CGI](../Page/通用网关接口.md "wikilink")、[CSS](../Page/CSS.md "wikilink")、[HTML](../Page/HTML.md "wikilink")、[Java](../Page/Java.md "wikilink")、[JavaScript](../Page/JavaScript.md "wikilink")、[NSIS](../Page/NSIS.md "wikilink")、[Pascal](../Page/Pascal_\(程式語言\).md "wikilink")、[Perl](../Page/Perl.md "wikilink")、[PHP](../Page/PHP.md "wikilink")、[Python](../Page/Python.md "wikilink")、[SQL](../Page/SQL.md "wikilink")、[VB](../Page/VB.md "wikilink")、[VBScript](../Page/VBScript.md "wikilink")、[XHTML](../Page/XHTML.md "wikilink")、[XML以及](../Page/XML.md "wikilink")[汇编语言](../Page/汇编语言.md "wikilink")。他亦支持部分文本格式的语法高亮标-{注}-：[BAT](../Page/批处理.md "wikilink")、DIFF、[INF](../Page/INF.md "wikilink")、[INI](../Page/INI.md "wikilink")、[REG等](../Page/REG.md "wikilink")。

Notepad2还有其他特性：[常规编辑](../Page/RegEx.md "wikilink")：查找和替换；文字编码转换：在[ASCII](../Page/ASCII.md "wikilink")、[UTF-8和](../Page/UTF-8.md "wikilink")[UTF-16之间互相转换](../Page/UTF-16.md "wikilink")；半透明效果；页面缩放；括弧匹配和自动缩进等。

## 参看

  - [文本编辑器列表](../Page/文本编辑器列表.md "wikilink")
  - [Notepad++](../Page/Notepad++.md "wikilink")

## 外部链接

  -
  - [舊版非官方繁體中文版網站](https://sites.google.com/site/notepad2zhtw/)

  - [新版非官方繁体中文版網站](http://notepad2tw.blogspot.tw/)

[Category:2004年软件](../Category/2004年软件.md "wikilink")
[Category:Windows文本编辑器](../Category/Windows文本编辑器.md "wikilink")
[Category:自由文本编辑器](../Category/自由文本编辑器.md "wikilink")
[Category:Notepad替代](../Category/Notepad替代.md "wikilink")
[Category:Windows独占自由软件](../Category/Windows独占自由软件.md "wikilink")
[Category:用C編程的自由軟體](../Category/用C編程的自由軟體.md "wikilink")
[Category:使用BSD许可证的软件](../Category/使用BSD许可证的软件.md "wikilink")
[Category:使用Scintilla的软件](../Category/使用Scintilla的软件.md "wikilink")