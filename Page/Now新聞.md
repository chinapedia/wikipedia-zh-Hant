[HK_Telecom_House_201612.jpg](https://zh.wikipedia.org/wiki/File:HK_Telecom_House_201612.jpg "fig:HK_Telecom_House_201612.jpg")
**Now新聞**（前稱：Now寬頻電視新聞；）是[Now
TV自製和轉播的](../Page/Now_TV.md "wikilink")[新聞節目](../Page/新聞節目.md "wikilink")，自家建立並擁有[now直播台](../Page/now直播台.md "wikilink")（331頻道）、[now新聞台](../Page/now新聞台.md "wikilink")（332頻道）、[now財經台](../Page/now財經台.md "wikilink")（333頻道）、[now報價台](../Page/now報價台.md "wikilink")（336頻道）、[交通頻道](../Page/now寬頻電視交通頻道.md "wikilink")（375頻道）、[天氣頻道](../Page/now寬頻電視天氣頻道.md "wikilink")（376頻道）及[航班資訊](../Page/now寬頻電視航班資訊.md "wikilink")（377頻道）。

Now新聞早在[now寬頻電視](../Page/now寬頻電視.md "wikilink")（現稱：Now
TV）啟播時已成立，但初時需要依賴[24小時亞視新聞-{台}-及其他國際新聞頻道如](../Page/新闻财经频道.md "wikilink")[CNN](../Page/CNN.md "wikilink")。其自製有關新聞的頻道需要到2006年3月20日才出現，首個自製的頻道是[now直播台](../Page/now直播台.md "wikilink")、[now新聞台](../Page/now新聞台.md "wikilink")、[now財經台和](../Page/now財經台.md "wikilink")[now報價台](../Page/now報價台.md "wikilink")。另外，資訊性頻道，例如：[交通頻道](../Page/now寬頻電視交通頻道.md "wikilink")、[天氣頻道及](../Page/now寬頻電視天氣頻道.md "wikilink")[航班資訊亦在now寬頻電視啟播時已經播放](../Page/now寬頻電視航班資訊.md "wikilink")。

## now新聞總部

now新聞（now直播台、now新聞台及now財經台）的總部處於[灣仔](../Page/灣仔.md "wikilink")[電訊大廈三樓](../Page/電訊大廈.md "wikilink")。

## 自製頻道資料

### now直播台

**now直播台**（），是[香港第三個直播突發新聞現場的頻道](../Page/香港.md "wikilink")。與now直播台相類似的頻道包括[香港有線電視的](../Page/香港有線電視.md "wikilink")[有線直播新聞台和](../Page/有線直播新聞台.md "wikilink")[香港電台的](../Page/香港電台.md "wikilink")[港台電視32](../Page/港台電視32.md "wikilink")。

### now新聞台

**now新聞台**（），是[香港第五個二十四小時報導即時](../Page/香港.md "wikilink")[新聞的頻道](../Page/新聞.md "wikilink")。原本是一個標清製作的新聞頻道，於2018年8月13日起升級為高清頻道。[now新聞台主要競爭對手包括](../Page/now新聞台.md "wikilink")[myTV
SUPER的](../Page/myTV_SUPER.md "wikilink")[無綫新聞台和](../Page/無綫新聞台.md "wikilink")[香港有線電視的](../Page/香港有線電視.md "wikilink")[有線新聞台](../Page/有線新聞台.md "wikilink")。

#### 節目

### now財經台

**now財經台**（、英文簡稱），是[now寬頻電視的其中一個新聞資訊頻道](../Page/now寬頻電視.md "wikilink")。在2006年3月20日開始播放。[now財經台與對手](../Page/now財經台.md "wikilink")[有線電視財經資訊台一樣](../Page/有線電視財經資訊台.md "wikilink")，於平日日間重點直播股市實況、並於[香港交易所設有直播室](../Page/香港交易所.md "wikilink")，而晚間及假日則播放財經節目及與[now新聞台聯播新聞](../Page/now新聞台.md "wikilink")。[now財經台的主要競爭對手為](../Page/now財經台.md "wikilink")[香港有線電視的](../Page/香港有線電視.md "wikilink")[有線財經資訊台及](../Page/有線財經資訊台.md "wikilink")[myTV
SUPER的](../Page/myTV_SUPER.md "wikilink")[無綫財經·資訊台](../Page/無綫財經·資訊台.md "wikilink")。

#### 歷史

  - 2006年3月20日，[now財經台啟播](../Page/now財經台.md "wikilink")，交易日主要播放新聞報道及資訊節目，周末則全日重播周一至周五的資訊節目。並設有全港首創之電視報價服務，可在電視螢幕得到一小時延遲金股匯市免費報價服務。

<!-- end list -->

  - 2006年8月3日，觀眾可付費訂購即時報價服務。

<!-- end list -->

  - 2006年9月6日，now財經台開始提供自選服務，可以隨時收看該台的精選節目《大鳴大放》、《樓市每日睇》及《環球金融快線》。

<!-- end list -->

  - 2006年12月9日，假日全日增設新聞報導，並播放其他資訊節目及[港台節目](../Page/香港電台.md "wikilink")。

<!-- end list -->

  - 2007年10月24日，[now新聞台啟播](../Page/now新聞台.md "wikilink")，[now財經台播放的新聞報道時段改與](../Page/now財經台.md "wikilink")[now新聞台聯播](../Page/now新聞台.md "wikilink")，並開始播放在財經台單獨播放的《now財經新聞》。

<!-- end list -->

  - 2018年8月13日，[now新聞台](../Page/now新聞台.md "wikilink")（332頻道）及[now財經台](../Page/now財經台.md "wikilink")（333頻道）正式由標清轉為高清共享台號頻道，所以節目均以高清製作。

#### 節目

### now報價台

**now報價台**（），是[香港第一個二十四小時不停財經報價的頻道](../Page/香港.md "wikilink")。

## 新聞節目

### [ViuTV新聞時段](../Page/ViuTV.md "wikilink")

ViuTV在2016年4月6日啟播後，會播放以下由now新聞製作之新聞及財經節目：

| 節目                                          | 播放時間          | 播放日期  |
| ------------------------------------------- | ------------- | ----- |
| [早晨新聞](../Page/now早晨.md "wikilink")         | 06:00 - 09:00 | 星期一至五 |
| 06:00 - 08:00                               | 星期六、日         |       |
| [午間新聞](../Page/now午間新聞.md "wikilink")       | 12:00 - 12:20 | 星期二至四 |
| 12:00 - 12:30                               | 星期一、五至日       |       |
| [6點新聞報道](../Page/now新聞報道.md "wikilink")     | 18:00 - 18:30 | 每日    |
| [7點新聞報道](../Page/now新聞報道.md "wikilink")     | 19:00 - 19:30 |       |
| [深宵新聞](../Page/now深宵新聞.md "wikilink")       | 00:30 - 01:00 | 星期二至五 |
| 01:00 - 01:30                               | 星期六           |       |
| [大鳴大放](../Page/大鳴大放_\(電視節目\).md "wikilink") | 18:30 - 19:00 | 星期六、日 |
| [杏林在線](../Page/杏林在線.md "wikilink")          | 19:30 - 20:00 | 星期六   |
| [經緯線](../Page/經緯線_\(電視節目\).md "wikilink")   | 19:30 - 20:00 | 星期日   |

### [now香港台](../Page/now香港台.md "wikilink")

now香港台2008年12月12日啟播後至2016年3月18日，該台每日會播放以下由now新聞製作之新聞及財經節目（下列以停播前為準）：

  - 《[now午間新聞](../Page/now午間新聞.md "wikilink")》（星期一至五13:00-13:30同步錄播）
  - 《收市檢討》（港股交易日16:00-17:00同步直播）
  - 《now財經新聞》（港股交易日19:00-19:20同步直播）
  - 《潮玩科技》（星期四港股交易日19:20-19:30同步播出）
  - 《理財有方》（星期一、二、三、五港股交易日19:20-19:30同步播出）
  - 《[now新聞報道](../Page/now新聞報道.md "wikilink")》（星期一至五19:30-20:00同步錄播）
  - 《一周睇樓團》（逢星期日18:30-19:00錄播）
  - 《[now深宵新聞](../Page/now深宵新聞.md "wikilink")》（每日04:30-05:00同步錄播）

## 採訪事件

### 2000年代

  - 2008年3月25日，正值[西藏騷亂](../Page/西藏騷亂.md "wikilink")，now寬頻電視記者[簡學悕成功突破公安所有阻礙](../Page/簡學悕.md "wikilink")，進入[西藏採訪](../Page/西藏.md "wikilink")；當時，所有境外[記者已被](../Page/記者.md "wikilink")[公安趕離西藏](../Page/公安.md "wikilink")。
  - 2008年7月20日，記者[周月翔採訪一名居於](../Page/周月翔.md "wikilink")[北京老](../Page/北京.md "wikilink")[衚衕的老婆婆時被公安無理中止採訪並且帶走](../Page/衚衕.md "wikilink")；7月24日記者周月翔及攝影師楊楚文採訪[北京市民購買](../Page/北京.md "wikilink")[北京奧運門票時的混亂時](../Page/北京奧運.md "wikilink")，被公安無理押上公安車一段時間；7月25日攝影師楊楚文採訪北京市民購買京奧門票時的混亂時，被公安從梯上拉跌。以上事件引致now新聞對北京發表嚴重抗議，最終導致雙方決裂，日後now新聞在內地的採訪都不時受到阻攔（）。
  - 2009年8月12日，時任now新聞台駐[京記者的](../Page/北京.md "wikilink")[黃嘉瑜與攝影師到](../Page/黃嘉瑜.md "wikilink")[四川](../Page/四川.md "wikilink")[成都採訪](../Page/成都.md "wikilink")，於當日上午6時許準備由酒店出發採訪[譚作人案之際](../Page/譚作人.md "wikilink")，數名公安人士在房間外攔截，指稱接獲市民舉報，懷疑黃等三人入住的房間藏有毒品等違禁品，要搜查房間，擾攘近6小時至下午1時許，該段時間三人一直被軟禁於酒店內，令三人不能正常外出採訪。now新聞台、[香港記者協會及](../Page/香港記者協會.md "wikilink")[香港新聞行政人員協會均發聲明指公安無理執法](../Page/香港新聞行政人員協會.md "wikilink")，妨礙[新聞自由](../Page/新聞自由.md "wikilink")。
  - 2009年9月4日，[now寬頻電視駐京攝影師林振威與記者](../Page/now寬頻電視.md "wikilink")[陳偉利](../Page/陳偉利.md "wikilink")、[無線電視記者](../Page/無線電視.md "wikilink")[林子豪及攝影記者劉永全到](../Page/林子豪.md "wikilink")[新疆](../Page/新疆.md "wikilink")[烏魯木齊採訪時](../Page/烏魯木齊.md "wikilink")，被多名市武警毆打並且被強制扣留，事件在香港造成強烈反響，多間香港新聞媒體及團體在事件發生後提出抗議及參與遊行示威（詳見[2009年新疆武警扣押香港記者事件](../Page/2009年新疆武警扣押香港記者事件.md "wikilink")）。

### 2010年代以後

  - 2011年8月中，時任中國國務院副總理[李克強訪港](../Page/李克強.md "wikilink")，期間警方多次對示威者及記者無理地阻止他們的示威及採訪活動（詳見[李克強訪問香港期間保安爭議](../Page/李克強訪問香港期間保安爭議.md "wikilink")），其中[now寬頻電視記者](../Page/now寬頻電視.md "wikilink")[梁卓麒及攝影師被無穿制服](../Page/梁卓麒.md "wikilink")、無證件及無表明身份之警員阻礙採訪及驅趕。及後，警方不負責任的態度及歪曲事實續令[now寬頻電視不滿](../Page/now寬頻電視.md "wikilink")，故多次發表嚴重抗議聲明，及在now新聞台節目《時事全方位》交代及討論，得到議員及觀眾支持。
  - 2018年5月16日：now新聞台攝影師徐駿銘在早上採訪北京維權律師出席[謝燕益北京律師協會人士聽證會時遭警察和便衣人員阻撓](../Page/謝燕益.md "wikilink")，記者證和回鄉證被取去，未有歸還。攝影師試圖搶回證件但不成功，並被多名便衣人員和公安壓在地上及叉頸，頭部流血，按在地上鎖上手扣，押上警車帶走。攝影師中午前被帶到和平里醫院驗傷，在醫院驗傷時被禁止使用手機與外界聯絡，公安要求徐駿銘寫悔過書才讓他離開。Now新聞台對合法採訪遭到無理及粗暴阻撓表示極度憤怒。署理行政長官張建宗表示，非常關注事件，已馬上接觸[港澳辦](../Page/港澳辦.md "wikilink")。記協發表聲明表示强烈抗議\[1\]\[2\]。特首[林鄭月娥對事件表示遺憾](../Page/林鄭月娥.md "wikilink")\[3\]。

## 報道手法

  - 香港於2008年9月7日舉行[第四屆立法會選舉](../Page/2008年香港立法會選舉.md "wikilink")，新聞台或電視台新聞部傳統上會在選舉前製作一些專題報導略略簡介一下選情，但now新聞台則在6月已經開始了，而且報導得非常深入，甚至加入一些分析元素，更在8月時在[馬鞍山](../Page/馬鞍山_\(香港市鎮\).md "wikilink")[恆安商場](../Page/恆安邨.md "wikilink")、[奧海城](../Page/奧海城.md "wikilink")、[九龍灣展貿中心](../Page/九龍灣展貿中心.md "wikilink")、[愉景新城及](../Page/愉景新城.md "wikilink")[演藝學院舉辦了五場論壇](../Page/香港演藝學院.md "wikilink")（五個地方選區），又舉辦了[功能組別的論壇及其相關專題報導](../Page/功能組別.md "wikilink")，可見其對這次立會選舉的重視。
  - 2008年8月10日，[香港](../Page/香港.md "wikilink")[旺角](../Page/旺角.md "wikilink")[嘉禾大廈發生五級大火](../Page/嘉禾大廈大火.md "wikilink")，now新聞台於中午12:42報導「據消息，有兩名消防員殉職」（指[蕭永方及](../Page/蕭永方.md "wikilink")[陳兆龍](../Page/陳兆龍.md "wikilink")），但到下午2時許其他新聞台才陸續報導有兩名消防員殉職，而不久後官方才正式向記者確認有兩名消防員殉職的事件，

## 音乐

  - [《now新闻报道》主题音乐《Evening
    Drama》](http://links.universalproductionmusic.com/LevhEz)
  - [《now新闻报道》之后天气主题音乐《Morning
    Magazine》](http://links.universalproductionmusic.com/tvoasb)

## 員工

  - 2014年9月起，部分主播間中會被調派到採訪組崗位培訓（此安排與[香港有線新聞相同](../Page/香港有線新聞.md "wikilink")）。

<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p><strong>管理層</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/張志剛_(消支義).md" title="wikilink">張志剛</a><br />
（總監）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>主播</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/徐蕙儀.md" title="wikilink">徐蕙儀</a><br />
（總主播）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/黃浩霖.md" title="wikilink">黃浩霖</a><br />
（首席主播）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/張凱琪.md" title="wikilink">張凱琪</a><br />
（主播）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/徐俊逸.md" title="wikilink">徐俊逸</a><br />
（主播）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>港聞組採訪主任／副採訪主任／助理採訪主任</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/鄭麗矜.md" title="wikilink">鄭麗矜</a><br />
（總採訪主任）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/高志輝.md" title="wikilink">高志輝</a><br />
（副採訪主任）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>港聞組記者</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/簡學悕.md" title="wikilink">簡學悕</a><br />
（首席記者）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/蔡泓杰.md" title="wikilink">蔡泓杰</a><br />
（高級記者）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/李彤茵.md" title="wikilink">李彤茵</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/張倩欣.md" title="wikilink">張倩欣</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/馮文傑.md" title="wikilink">馮文傑</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/嚴彩明.md" title="wikilink">嚴彩明</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>國際新聞組記者／編輯</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/梁宙然.md" title="wikilink">梁宙然</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/宋靜嫻.md" title="wikilink">宋靜嫻</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/梁銘思.md" title="wikilink">梁銘思</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/梁尹齡.md" title="wikilink">梁尹齡</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/廖美兒.md" title="wikilink">廖美兒</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/王晶_(記者).md" title="wikilink">王晶</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>財經組記者</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/潘磊華.md" title="wikilink">潘磊華</a><br />
（首席記者）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/馬子豪.md" title="wikilink">馬子豪</a><br />
（高級記者）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/鄧嘉蔚.md" title="wikilink">鄧嘉蔚</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/黃靜妍.md" title="wikilink">黃靜妍</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>體育組</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/宋貴玲.md" title="wikilink">宋貴玲</a><br />
（首席記者）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>專題組記者／編輯</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/陳偉利.md" title="wikilink">陳偉利</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/陳家俊.md" title="wikilink">陳家俊</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>編導</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/蔡凱珊.md" title="wikilink">蔡凱珊</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/王佩兒.md" title="wikilink">王佩兒</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/歐志誠.md" title="wikilink">歐志誠</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/鄭子韜.md" title="wikilink">鄭子韜</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/童俊雄.md" title="wikilink">童俊雄</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/馬立賢.md" title="wikilink">馬立賢</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/陳毅恒.md" title="wikilink">陳毅恒</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>監製</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/蕭世坤.md" title="wikilink">蕭世坤</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/麥國華.md" title="wikilink">麥國華</a><br />
（港股今日睇）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/杜福良.md" title="wikilink">杜福良</a><br />
（大鳴大放）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 前員工

<sub>以下內容可能並沒有來源根據，請自行判斷以下內容的可信性及真確性。</sub>

<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>'''近況：轉投<a href="../Page/無綫新聞.md" title="wikilink">無綫新聞</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/蔡千紅.md" title="wikilink">蔡千紅</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/周芷菱.md" title="wikilink">周芷菱</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/蘆儀.md" title="wikilink">蘆儀</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>'''近況：轉投<a href="../Page/有線新聞.md" title="wikilink">有線新聞</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/李錦妍.md" title="wikilink">李錦妍</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/邱蔚怡.md" title="wikilink">邱蔚怡</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>'''近況：轉投<a href="../Page/香港電台.md" title="wikilink">香港電台</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/李景鴻.md" title="wikilink">李景鴻</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/程潔明.md" title="wikilink">程潔明</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>'''近況：轉投<a href="../Page/商業電台.md" title="wikilink">商業電台</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/馬韻然.md" title="wikilink">馬韻然</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/朱均偉.md" title="wikilink">朱均偉</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>'''近況：轉投<a href="../Page/鳳凰衛視.md" title="wikilink">鳳凰衛視</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/黎浩然.md" title="wikilink">黎浩然</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>'''近況：轉投其他傳媒機構</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/劉慧芬.md" title="wikilink">劉慧芬</a><br />
（<a href="../Page/資本雜誌.md" title="wikilink">資本雜誌</a>）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/佘曉君.md" title="wikilink">佘曉君</a><br />
（<a href="../Page/新城電台.md" title="wikilink">新城電台</a>）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/林君蓮.md" title="wikilink">林君蓮</a><br />
（<a href="../Page/澳門廣播電視.md" title="wikilink">澳門廣播電視</a>）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/顏慧美.md" title="wikilink">顏慧美</a><br />
（<a href="../Page/蘋果日報.md" title="wikilink">蘋果日報</a>）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/朱穎忻.md" title="wikilink">朱穎忻</a><br />
（財華社－現代電視）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>'''近況：政府、政治及公營機構</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/廖淑明.md" title="wikilink">廖淑明</a><br />
（<a href="../Page/香港證監會.md" title="wikilink">香港證監會</a>）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/何倩華.md" title="wikilink">何倩華</a><br />
（<a href="../Page/澳門政府新聞局.md" title="wikilink">澳門政府新聞局</a>）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/陳詠琳.md" title="wikilink">陳詠琳</a><br />
（<a href="../Page/政府新聞處.md" title="wikilink">政府新聞處</a>）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/蘇婉雯.md" title="wikilink">蘇婉雯</a><br />
（<a href="../Page/民主思路.md" title="wikilink">民主思路</a>）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>'''近況：商業、金融及地產界</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/黃芝蓉.md" title="wikilink">黃芝蓉</a><br />
（銀河娛樂集團總辦事處公共關係副總裁）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/鍾綺君.md" title="wikilink">鍾綺君</a><br />
（恒基兆業地產有限公司公共關係人員）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/潘悅瑤.md" title="wikilink">潘悅瑤</a><br />
（<a href="../Page/意大利國家勞工銀行.md" title="wikilink">意大利國家勞工銀行公關人員</a>）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/陳曉蓉.md" title="wikilink">陳曉蓉</a><br />
（<a href="../Page/中國建設銀行.md" title="wikilink">中國建設銀行</a>）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/張鷺玲.md" title="wikilink">張鷺玲</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/陳正犖.md" title="wikilink">陳正犖</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>'''近況：其他</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/陳宇凰.md" title="wikilink">陳宇凰</a><br />
（市場推廣）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/徐小媚.md" title="wikilink">徐小媚</a><br />
（香港戲院商會公共關係主任）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/方子敏.md" title="wikilink">方子敏</a><br />
（<a href="../Page/科樂美.md" title="wikilink">科樂美公關</a>）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/過智溢.md" title="wikilink">過智溢</a><br />
（移民加拿大<a href="../Page/溫哥華.md" title="wikilink">溫哥華</a> — 任職<a href="../Page/新時代電視.md" title="wikilink">新時代電視</a>）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/陳匡民.md" title="wikilink">陳匡民</a><br />
（<a href="../Page/銀河百度.md" title="wikilink">銀河百度</a>）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/張文欣.md" title="wikilink">張文欣</a><br />
（<a href="../Page/香港賽馬會.md" title="wikilink">香港賽馬會公關人員</a>）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/張兆高.md" title="wikilink">張兆高</a><br />
（「<a href="http://mmcreation.net/">是敢的創作室</a>」創辦人 ）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/容靜怡.md" title="wikilink">容靜怡</a><br />
（升學）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>去向不明或待業</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/游瑋珊.md" title="wikilink">游瑋珊</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/黎卓欣.md" title="wikilink">黎卓欣</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/曾芷倫.md" title="wikilink">曾芷倫</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

## 參考文獻

<references group="註" />

## 外部連結

  - [Now新聞](https://news.now.com/)

[Category:Now TV](../Category/Now_TV.md "wikilink")
[Category:香港電視播放頻道](../Category/香港電視播放頻道.md "wikilink")
[Category:粵語電視頻道](../Category/粵語電視頻道.md "wikilink")
[Category:24小時電視新聞頻道](../Category/24小時電視新聞頻道.md "wikilink")
[Category:2006年成立的電視台或電視頻道](../Category/2006年成立的電視台或電視頻道.md "wikilink")

1.
2.
3.