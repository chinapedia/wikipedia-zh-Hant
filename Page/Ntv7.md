**ntv7**，全称为**NatSeven電視私人有限公司**（），是[馬來西亞私营电视台](../Page/馬來西亞.md "wikilink")，1998年4月7日早上8：00开播。ntv7的目标是为了使马来西亚社会更欢乐又更进步，正式的中文[口號是](../Page/口號.md "wikilink")**“感覺美好”**，当初为一家独立私营电视台，既是与[TV3与](../Page/TV3_\(马来西亚\).md "wikilink")[八度空间竞争](../Page/八度空间_\(电视台\).md "wikilink")。但在2005年，ntv7因财务困难而被[首要媒体收购](../Page/首要媒体.md "wikilink")，因此ntv7与TV3、八度空间与[TV9皆同屬於首要媒體](../Page/TV9.md "wikilink")。

自2017年1月1日起，ntv7的全天播出时间改為06:00-次日凌晨01:50（[除夕](../Page/除夕.md "wikilink")02:00，有時會因播出《CJ
WOW
Shop》而延至02:20）。此外，ntv7也可透过[Astro](../Page/Astro.md "wikilink")107频道，[Unifi
TV](../Page/Unifi_TV.md "wikilink")107频道观看以及在數碼電視[MYTV
Broadcasting](../Page/MYTV_Broadcasting.md "wikilink")107頻道收看。

目前ntv7在馬來西亞為第7頻道，在[巴生谷的超高頻](../Page/巴生河流域.md "wikilink")([UHF](../Page/特高頻.md "wikilink"))頻率為37頻道。

2018年3月5日起，ntv7全面改革为"摩登大马"方向前进，其中华语新闻和香港[无线电视的港剧是否获得保留](../Page/无线电视.md "wikilink")，成为焦点。最后，华语新闻及香港无线电视的港剧被允许继续播放。

ntv7華語新聞自2018年6月1日起，將目前2個時段的新聞時間（下午17：30-18：30、22：30-23：00）合併至17：00-18：00播出。隨著節目時段的調整，港劇《[火線下的江湖大佬](../Page/火線下的江湖大佬.md "wikilink")》也成了ntv7中文劇時段的最后一部戲劇直到2019年1月1日。

自2018年12月31日起，NTV7全天节目安排中大部分内容均为播出CJ WOW SHOP中文版。

## 歷史

### 第一阶段試播（1998年3月7日－1998年3月30日）

1998年3月7日-3月30日，ntv7开始第一阶段的试播，第一晚主要試播的内容為两部MV，分别为[葉玉卿的](../Page/葉玉卿.md "wikilink")《[記憶](../Page/擋不住的風情_\(國語專輯\).md "wikilink")》，和[Toni
Braxton的](../Page/唐妮·布蕾斯頓.md "wikilink")《[You’re Makin’ Me
High](../Page/w:en:You’re_Makin’_Me_High.md "wikilink")》；試

  -
    {| class="wikitable"

\! [馬來西亞時間](../Page/馬來西亞標準時間.md "wikilink") \! 節目名稱 |- | 19:00-20:00 |
[檢驗圖](../Page/檢驗圖.md "wikilink") |- | 20:00-20:01 |
開播、[國歌](../Page/馬來西亞國歌.md "wikilink") |- | 20:01-22:00 |
[音樂影片](../Page/音樂影片.md "wikilink") |- | 22:00-22:01 |
[國歌](../Page/馬來西亞國歌.md "wikilink")、收播 |}

### 第二阶段試播（1998年3月31日－1998年4月6日）

1998年3月31日-4月6日，ntv7开始第二阶段试播，试播内容以电影为主。

**正式开播（1998年4月7日）**

1998年4月7日早上8：00起，NTV7正式开播。

  - ntv7開播當天（1998年4月7日）的節目-{表}-：
    {| class="wikitable"

\! [馬來西亞時間](../Page/馬來西亞標準時間.md "wikilink") \! 節目名稱 |- | 07:00-08:00 |
[檢驗圖](../Page/檢驗圖.md "wikilink") |- | 08:00-08:01 |
開播、[國歌](../Page/馬來西亞國歌.md "wikilink") |- | 08:01-09:00 |
[馬來西亞電視3台傳播](../Page/TV3_\(馬來西亞\).md "wikilink")：「[哈芝節禱告在](../Page/哈芝節.md "wikilink")[馬來西亞](../Page/馬來西亞.md "wikilink")[吉打州](../Page/吉打州.md "wikilink")[亞羅士打](../Page/亞羅士打.md "wikilink")[查希爾清真寺](../Page/查希爾清真寺.md "wikilink")」（[現場直播](../Page/現場直播.md "wikilink")）
|- | 09:00-09:30 | [哈芝節節目](../Page/哈芝節.md "wikilink") -
[紀錄片及](../Page/紀錄片.md "wikilink")[科學](../Page/科學.md "wikilink")[教育](../Page/教育.md "wikilink")：「生活在真主的花園」
|- | 09:30-10:30 |
[舞台](../Page/舞台.md "wikilink")[音樂節](../Page/音樂節.md "wikilink")：「[Sheila
Majid](../Page/w:id:Sheila_Majid.md "wikilink")[演唱會在](../Page/音樂會.md "wikilink")[馬來西亞](../Page/馬來西亞.md "wikilink")[吉隆坡](../Page/吉隆坡.md "wikilink")[佩重納斯大廈](../Page/佩重納斯大廈.md "wikilink")」（[現場直播](../Page/現場直播.md "wikilink")）
|- | 10:30-11:30 | [紀錄片](../Page/紀錄片.md "wikilink")：「Titanic: Breaking
New Ground」 |- | 11:30-12:00 |
[馬來語](../Page/馬來語.md "wikilink")[新聞](../Page/新聞.md "wikilink")
|- | 12:00-13:00 |
[舞台](../Page/舞台.md "wikilink")[音樂節](../Page/音樂節.md "wikilink")：「[邁克爾·傑克遜](../Page/邁克爾·傑克遜.md "wikilink")[HIStory世界演唱會在](../Page/HIStory世界巡迴演唱會.md "wikilink")[馬來西亞](../Page/馬來西亞.md "wikilink")[吉隆坡](../Page/吉隆坡.md "wikilink")[默迪卡體育場](../Page/默迪卡體育場.md "wikilink")」（[現場直播](../Page/現場直播.md "wikilink")）
|- | 13:00-14:00 |
[舞台](../Page/舞台.md "wikilink")[音樂節](../Page/音樂節.md "wikilink")：「[茜蒂·諾哈麗莎](../Page/茜蒂·諾哈麗莎.md "wikilink")[Cindai演唱會在](../Page/w:en:Cindai.md "wikilink")[馬來西亞](../Page/馬來西亞.md "wikilink")[吉隆坡](../Page/吉隆坡.md "wikilink")[國家體育場](../Page/國家體育場.md "wikilink")」（[現場直播](../Page/現場直播.md "wikilink")）
|- | 14:00-16:00 | [哈芝節節目](../Page/哈芝節.md "wikilink") -
[馬來語](../Page/馬來語.md "wikilink")[電視電影](../Page/電視電影.md "wikilink")：「大阿拉呼」（含[馬來語](../Page/馬來語.md "wikilink")[配音和](../Page/配音.md "wikilink")[英語](../Page/英語.md "wikilink")[字幕](../Page/字幕.md "wikilink")）
|- | 16:00-18:00 | [哈芝節節目](../Page/哈芝節.md "wikilink") -
[英語](../Page/英語.md "wikilink")[電影](../Page/電影.md "wikilink")：「[阿拉伯的勞倫斯](../Page/阿拉伯的勞倫斯.md "wikilink")」（含[英語](../Page/英語.md "wikilink")[配音和](../Page/配音.md "wikilink")[馬來語](../Page/馬來語.md "wikilink")[字幕](../Page/字幕.md "wikilink")）
|- | 18:00-19:00 | [香港電視劇](../Page/香港電視劇.md "wikilink") -
[TVB劇場](../Page/無綫電視劇集.md "wikilink")：「[家變](../Page/家變_\(無綫電視劇\).md "wikilink")」（含[粵語](../Page/粵語.md "wikilink")[配音和](../Page/配音.md "wikilink")[馬來語](../Page/馬來語.md "wikilink")[字幕](../Page/字幕.md "wikilink")）
|- | 19:00-20:00 |
[印度](../Page/印度.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")：「[摩訶婆羅多](../Page/w:en:Mahabharat_\(1988_TV_series\).md "wikilink")」（含[泰米爾語](../Page/泰米爾語.md "wikilink")[配音和](../Page/配音.md "wikilink")[馬來語](../Page/馬來語.md "wikilink")[字幕](../Page/字幕.md "wikilink")）
|- | 20:00-20:30 |
[馬來語](../Page/馬來語.md "wikilink")[新聞](../Page/新聞.md "wikilink")：「[Berita
Nasional（國家新聞）](../Page/晚間新聞.md "wikilink")」 |- | 20:30-21:30 |
[舞台](../Page/舞台.md "wikilink")[音樂節](../Page/音樂節.md "wikilink")：「[唐妮·布蕾斯頓](../Page/唐妮·布蕾斯頓.md "wikilink")[You’re
Makin’ Me
High演唱會在](../Page/w:en:You’re_Makin’_Me_High.md "wikilink")[馬來西亞](../Page/馬來西亞.md "wikilink")[吉隆坡](../Page/吉隆坡.md "wikilink")[佩重納斯大廈](../Page/佩重納斯大廈.md "wikilink")」（[現場直播](../Page/現場直播.md "wikilink")）
|- | 21:30-22:00 |
[馬來語](../Page/馬來語.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")：「馬爾斯圖啦」（含[馬來語](../Page/馬來語.md "wikilink")[配音](../Page/配音.md "wikilink")）
|- | 22:30-23:00 |
[英語](../Page/英語.md "wikilink")[新聞](../Page/新聞.md "wikilink")：「[World
News（世界新聞）](../Page/晚間新聞.md "wikilink")」 |- | 23:00-00:00 |
[喜劇及](../Page/喜劇.md "wikilink")[脫口秀](../Page/脫口秀.md "wikilink")：「[大衛·萊特曼深夜秀](../Page/w:en:Late_Night_with_David_Letterman.md "wikilink")」（含[英語](../Page/英語.md "wikilink")[配音](../Page/配音.md "wikilink")）
|- | 00:00-01:00 | [國歌](../Page/馬來西亞國歌.md "wikilink")、收播 |}

## 現時ntv7的播出時段節目

2019年4月15日－2019年4月21日（節目更動以電視台最新公佈為準）

<table style="width:9%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
</colgroup>
<thead>
<tr class="header">
<th></th>
<th><p>星期一（22日）</p></th>
<th><p>星期二（23日）</p></th>
<th><p>星期三（24日）</p></th>
<th><p>星期四（25日）</p></th>
<th><p>星期五（26日）</p></th>
<th><p>星期六（27日）</p></th>
<th><p>星期日（28日）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>6:00a.m</p></td>
<td><p><a href="../Page/龍飛鳳舞_(電視劇).md" title="wikilink">龙飞凤舞</a>^ （第39-48集）</p></td>
<td><p><a href="../Page/龍飛鳳舞_(電視劇).md" title="wikilink">龙飞凤舞</a>^ （第49-54集）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>7:00a.m</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>8:00 a.m</p></td>
<td><p>活力加油站^</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>9:00a.m</p></td>
<td><p>CJ WOW SHOP 中文版</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5:00p.m</p></td>
<td><p>ntv7 华语新闻</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>6:00p.m</p></td>
<td><p><a href="../Page/奇怪的搭档.md" title="wikilink">奇怪的搭档</a># （第17-20集）</p></td>
<td><p><a href="../Page/Oh_My_Baby.md" title="wikilink">Oh My Baby</a>#<br />
</p></td>
<td><p><a href="../Page/Running_Man.md" title="wikilink">Running Man</a>#</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7:00p.m</p></td>
<td><p>CJ WOW SHOP 中文版</p></td>
<td><p>環島8（第2季）^</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>7:30p.m</p></td>
<td><p>ntv7 国语新闻</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>8:00p.m</p></td>
<td><p>ntv7 英语新闻</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>8:30p.m</p></td>
<td><p>CJ WOW SHOP 中文版</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11:00p.m</p></td>
<td></td>
<td><p><a href="../Page/大秦帝国之崛起.md" title="wikilink">大秦帝国之崛起</a>^ （第1-3集）</p></td>
<td><p>CJ WOW SHOP 中文版</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>12:00a.m</p></td>
<td><p>CJ WOW SHOP 中文版</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><p>劇集</p></td>
<td><p>新聞／资讯</p></td>
<td><p>綜藝節目</p></td>
<td><p>^：重播<a href="../Page/八度空间.md" title="wikilink">八度空间的节目</a></p></td>
<td><p>#: 韩语原音，附马来语和中文字幕。</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## ntv7 十大艺人

| 2014年 十大艺人                       |
| -------------------------------- |
| [李洺中](../Page/李洺中.md "wikilink") |
| [王冠逸](../Page/王冠逸.md "wikilink") |
|                                  |

| 2015年 十大艺人                       |
| -------------------------------- |
| [李洺中](../Page/李洺中.md "wikilink") |
| [王冠逸](../Page/王冠逸.md "wikilink") |
|                                  |

| 2016年 十大艺人                       |
| -------------------------------- |
| [李洺中](../Page/李洺中.md "wikilink") |
| [苏盈之](../Page/苏盈之.md "wikilink") |
|                                  |

| 2016/2017年 星10大                  |
| -------------------------------- |
| [庄惟翔](../Page/庄惟翔.md "wikilink") |
| [梁祖仪](../Page/梁祖仪.md "wikilink") |
|                                  |

| 2017/2018年 星10大                  |
| -------------------------------- |
| [梁祖仪](../Page/梁祖仪.md "wikilink") |
| [谢承伟](../Page/谢承伟.md "wikilink") |
|                                  |

## 颁奖典礼

  - [2010金視奬頒獎典禮](../Page/2010金視奬頒獎典禮.md "wikilink") 2010年9月25日
  - [2012金視奬頒獎典禮](../Page/2012金視奬頒獎典禮.md "wikilink") 2012年9月22日
  - [2014金視奬頒獎典禮](../Page/2014金視奬頒獎典禮.md "wikilink") 2014年9月20日
  - [2017金視奬頒獎典禮](../Page/2017金視奬頒獎典禮.md "wikilink") 2017年5月20日

## 新闻节目

新闻时刻，包括以下节目

  - NTV7华语新闻（每日 17.00）
  - Edisi 7（每晚 19.30）
  - 7 Edition（每晚 20.00）

## 马来语

国文节目时刻，包括以下节目：

  - Actorlympics TV
  - Aiyoh\!
  - Ala Carte
  - Asam Garam
  - Aura
  - Elly
  - Hak Anda
  - Magix
  - Mi Casa
  - Sadiq & Co.
  - Seeker
  - Thursday Nite Live
  - Wakenabeb

## 英语

### 本地

  - Who Wants to be a Millionaire（马来西亚马来语与华语版本）
  - The Breakfast Show
  - Life\! Sessions
  - Biz Briefcase
  - Deal or No Deal（马来西亚英语版本）

### 外国

  - [CSI犯罪现场](../Page/CSI犯罪现场.md "wikilink")
  - [CSI犯罪现场: 迈阿密](../Page/CSI犯罪现场:_迈阿密.md "wikilink")
  - [CSI犯罪现场: 纽约](../Page/CSI犯罪现场:_纽约.md "wikilink")
  - Six Degrees
  - Frasier
  - [好汉两个半](../Page/好汉两个半.md "wikilink") Two and a Half Men
  - Pepper Dennis
  - [实习医生格蕾](../Page/实习医生格蕾.md "wikilink") Grey's Anatomy
  - [24](../Page/24_\(美国电视剧\).md "wikilink")
  - [识骨寻踪](../Page/识骨寻踪.md "wikilink") Bones
  - [BASILISK甲贺忍法帖](../Page/BASILISK甲贺忍法帖.md "wikilink")
  - [地狱少女](../Page/地狱少女.md "wikilink")
  - [凉宫春日的忧郁](../Page/凉宫春日的忧郁.md "wikilink")

## Bananana

儿童少年时刻，包括以下节目：

### 西洋

  - Oggy and the Cockroaches
  - Atomic Betty
  - [豆豆先生](../Page/豆豆先生.md "wikilink") Mr. Bean the Animated Series
  - [Hi Hi Puffy AmiYumi](../Page/Hi_Hi_Puffy_AmiYumi.md "wikilink")
  - Martin Mystery
  - [小马宝莉：友情就是魔法My](../Page/小马宝莉：友情就是魔法.md "wikilink") Little
    Pony:Friendship Is Magic Season 3

### 日本

  - [Keroro军曹](../Page/Keroro军曹.md "wikilink")
  - [Tsubasa翼](../Page/Tsubasa翼.md "wikilink")
  - [结界师](../Page/结界师.md "wikilink")
  - [夺还屋](../Page/夺还屋.md "wikilink")
  - [机动战士高达00](../Page/机动战士高达00.md "wikilink")
  - [樱桃小丸子](../Page/樱桃小丸子.md "wikilink")
  - [蜡笔小新](../Page/蜡笔小新.md "wikilink")
  - [哆啦A梦](../Page/哆啦A梦.md "wikilink")
  - [假面骑士系列](../Page/假面骑士系列.md "wikilink")
  - [校園迷糊大王](../Page/校園迷糊大王.md "wikilink")
  - [犬夜叉](../Page/犬夜叉.md "wikilink")
  - [数码宝贝](../Page/数码宝贝.md "wikilink")
  - [妖精的尾巴](../Page/妖精的尾巴.md "wikilink")
  - [七龙珠](../Page/七龙珠.md "wikilink")
  - [浪客剑心](../Page/浪客剑心.md "wikilink")
  - [百变小樱](../Page/百变小樱.md "wikilink")
  - [忍者服部君](../Page/忍者服部君.md "wikilink")
  - [妖怪手表](../Page/妖怪手表.md "wikilink")

## 中文

中文节目时刻，包括以下节目：

### 外购剧

  - [大清后宫](../Page/大清后宫.md "wikilink")
  - [富贵在天](../Page/富贵在天.md "wikilink")
  - [春天后母心](../Page/春天後母心_\(1998年電視劇\).md "wikilink")
  - [世间路](../Page/世间路.md "wikilink")
  - [慾望人生](../Page/慾望人生.md "wikilink")
  - [不了情](../Page/不了情.md "wikilink")
  - [长男的媳妇](../Page/长男的媳妇.md "wikilink")
  - [日正当中](../Page/日正当中.md "wikilink")
  - [我一定要成功](../Page/我一定要成功.md "wikilink")
  - [神机妙算刘伯温](../Page/神机妙算刘伯温.md "wikilink")
  - [台湾龙卷风](../Page/台湾龙卷风.md "wikilink")
  - [金色摩天輪](../Page/金色摩天輪.md "wikilink")
  - [ID精英](../Page/ID精英.md "wikilink")
  - [富贵门](../Page/富贵门.md "wikilink")
  - [东山飘雨西关晴](../Page/东山飘雨西关晴.md "wikilink")
  - [碧血盐枭](../Page/碧血盐枭.md "wikilink")
  - [有营煮妇](../Page/有营煮妇.md "wikilink")
  - [掌上明珠](../Page/掌上明珠.md "wikilink")
  - *[法證先鋒III](../Page/法證先鋒III.md "wikilink")*
  - [蒲松齡](../Page/蒲松齡.md "wikilink")
  - *[刑警](../Page/刑警.md "wikilink")*
  - *[潛行狙擊](../Page/潛行狙擊.md "wikilink")*
  - *[九江十二坊](../Page/九江十二坊.md "wikilink")*
  - *[真相](../Page/真相.md "wikilink")*
  - *[團圓](../Page/團圓.md "wikilink")*

### 本地

#### 综艺节目

  - 我们仨 -
  - 开心就好！<small>*Shout Out\! It's Show Time Se1*</small>
  - 追踪档案 <small>'Edition Investigate Mandarin''</small>
  - 寻找天使 <small>*Finding Angels*</small>
  - 冲峰陷阵 -
  - 悬案<small>*Unsolved Mysterious*</small>
  - e7（娛樂新聞） <small>*e Seven*</small>
  - 强中自有强中手 <small>*Battle of the Best*</small>
  - [一掷千金（马来西亚华语版本）](../Page/一掷千金_\(马来西亚\).md "wikilink") <small>*Deal
    or No Deal*</small>
  - 马来西亚明星偶像 <small>*Star Idol Malaysia*</small>
  - 美食哗啦啦 <small>*Yummy Trail*</small>
  - 檐下温情 <small>*Helping Hands Season*</small>
  - 敢敢挑战II <small>*Feel Good Feel Cook*</small>
  - 女人占尚風 <small>*Women's Zone*</small>
  - 誰來晚餐（[公視](../Page/公共電視文化事業基金會.md "wikilink")）<small>*Guess
    Who*</small>
  - 阳光行动2 <small>*Project Sunshine 2*</small>
  - 開心就好II <small>*Shout Out\! It's Show Time Se2*</small>
  - 女人占尚風2 <small>*Women's Zone Season 2*</small>
  - 食在好玩 <small>*Feel Good Feel Cook*</small>
  - 大人来了 <small>*Kids Ask Big Questions*</small>
  - 檐下温情2 <small>*Helping Hands Season 2*</small>
  - 女人占尚風3 <small>*Women's Zone Season 3*</small>
  - 男女呛反调（1&2） <small>*Mars vs Venus*（1&2）</small>
  - 无敌状元 （2010-2016） <small>*Mandarin Battle Star (2010-2016)*</small>
  - 女人占尚風4 <small>*Women's Zone Season 4*</small>
  - 李白体坛 <small>*All About Sports*</small>
  - Aunty也疯狂 <small>*Aunty Must Go Crazy*</small>
  - 榜中王 <small>*Super Smart Audition*</small>
  - 檐下温情3 <small>*Helping Hands Season 3*</small>
  - *榜中王 2013* <small>*Super Smart Audition 2013*</small>
  - *Aunty也疯狂2* <small>*Aunty Must Go Crazy Season 2*</small>
  - 热血行动<small>*Project CSR*</small>

<!-- end list -->

  - 爱食客

#### 电视電影

<table>
<tbody>
<tr class="odd">
<td><p><strong>首播</strong></p></td>
<td><p><strong>电视電影</strong></p></td>
<td><p><strong>合作演员</strong></p></td>
</tr>
<tr class="even">
<td><p>2011年</p></td>
<td><p><a href="../Page/媒人帮.md" title="wikilink">媒人帮</a><br />
<small><em><a href="../Page/媒人帮.md" title="wikilink">The Superb Matchmakers</a></em></small></p></td>
<td><p><a href="../Page/辛偉廉.md" title="wikilink">辛偉廉</a>、<a href="../Page/陳凱旋.md" title="wikilink">陳凱旋</a>、<a href="../Page/林靜苗.md" title="wikilink">林靜苗</a>、<a href="../Page/李洺中.md" title="wikilink">李洺中</a>、<a href="../Page/楊雁雁.md" title="wikilink">楊雁雁</a>、<a href="../Page/蔡河立.md" title="wikilink">蔡河立</a>、<a href="../Page/谢佳见.md" title="wikilink">谢佳见</a></p></td>
</tr>
<tr class="odd">
<td><p>2012年</p></td>
<td><p><a href="../Page/哈比全家福.md" title="wikilink">哈比全家福</a><br />
<small><em>Happy Family</em></small></p></td>
<td><p><a href="../Page/辛偉廉.md" title="wikilink">辛偉廉</a>、<a href="../Page/陳凱旋.md" title="wikilink">陳凱旋</a>、<a href="../Page/林靜苗.md" title="wikilink">林靜苗</a>、<a href="../Page/李洺中.md" title="wikilink">李洺中</a>、<a href="../Page/楊雁雁.md" title="wikilink">楊雁雁</a>、<a href="../Page/蔡河立.md" title="wikilink">蔡河立</a></p></td>
</tr>
<tr class="even">
<td><p>夺命游戏<br />
<small><em>The Game</em></small></p></td>
<td><p><a href="../Page/蔡河立.md" title="wikilink">蔡河立</a>、<a href="../Page/陳凱旋.md" title="wikilink">陳凱旋</a>、<a href="../Page/陈微欣.md" title="wikilink">陈微欣</a>、<a href="../Page/朱健美.md" title="wikilink">朱健美</a>、<a href="../Page/张顺源.md" title="wikilink">张顺源</a>、<a href="../Page/王淑君.md" title="wikilink">王淑君</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p>聚宝盆·续集<br />
<small><em>Lucky Bowl</em></small></p></td>
<td><p><a href="../Page/王冠逸.md" title="wikilink">王冠逸</a>、<a href="../Page/陳凱旋.md" title="wikilink">陳凱旋</a>、<a href="../Page/林靜苗.md" title="wikilink">林靜苗</a>、<a href="../Page/李洺中.md" title="wikilink">李洺中</a>、<a href="../Page/吳天瑜.md" title="wikilink">吳天瑜</a>、<a href="../Page/王淑君.md" title="wikilink">王淑君</a></p></td>
</tr>
<tr class="even">
<td><p>聚宝盆·前传<br />
<small><em>Lucky Bowl</em></small></p></td>
<td><p><a href="../Page/王冠逸.md" title="wikilink">王冠逸</a>、<a href="../Page/陳凱旋.md" title="wikilink">陳凱旋</a>、<a href="../Page/林靜苗.md" title="wikilink">林靜苗</a>、<a href="../Page/李洺中.md" title="wikilink">李洺中</a>、<a href="../Page/吳天瑜.md" title="wikilink">吳天瑜</a>、<a href="../Page/王淑君.md" title="wikilink">王淑君</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><em>夺命游戏2</em><br />
<small><em>The Game 2</em></small></p></td>
<td><p><a href="../Page/蔡佩璇.md" title="wikilink">蔡佩璇</a>、<a href="../Page/王爱玲.md" title="wikilink">王爱玲</a>、<a href="../Page/庄惟翔.md" title="wikilink">庄惟翔</a>、<a href="../Page/林静苗.md" title="wikilink">林静苗</a>、<a href="../Page/林冰冰.md" title="wikilink">林冰冰</a>、<a href="../Page/盛智伟.md" title="wikilink">盛智伟</a>、<a href="../Page/魏幽兰.md" title="wikilink">魏幽兰</a>、<a href="../Page/吴欣婷.md" title="wikilink">吴欣婷</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>我心向明月<br />
<small><em>Double Trouble Love</em></small></p></td>
<td><p><a href="../Page/王冠逸.md" title="wikilink">王冠逸</a>、<a href="../Page/刘淑敏.md" title="wikilink">刘淑敏</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td><p>|忆起回家<br />
<small><em>Spring Chorus</em></small></p></td>
<td><p><a href="../Page/蔡河立.md" title="wikilink">蔡河立</a>、<a href="../Page/吳天瑜.md" title="wikilink">吳天瑜</a>、<a href="../Page/王爱玲.md" title="wikilink">王爱玲</a>、<a href="../Page/庄仲维.md" title="wikilink">庄仲维</a>、<a href="../Page/陈凯旋.md" title="wikilink">陈凯旋</a>、<a href="../Page/李洺中.md" title="wikilink">李洺中</a>、<a href="../Page/秦雯彬.md" title="wikilink">秦雯彬</a>、<a href="../Page/张惠虹.md" title="wikilink">张惠虹</a>、<a href="../Page/王淑君.md" title="wikilink">王淑君</a>、<a href="../Page/王冠逸.md" title="wikilink">王冠逸</a>、<a href="../Page/梁雅高.md" title="wikilink">梁雅高</a></p></td>
</tr>
<tr class="even">
<td><p>2016年</p></td>
<td><p>|记忆中的菜单</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017年</p></td>
<td><p>|游龍戲鳳</p></td>
<td></td>
</tr>
</tbody>
</table>

#### 电视剧

[Ntv7剧集列表](../Page/Ntv7剧集列表.md "wikilink")

## 歷年標誌

Image:NTV7 New.JPG|被首要媒體收購後的標誌。使用時間為2006年至2012年
Image:Ntv7_2012_logo.jpg|2012年至2018年3月4日 Image:Logo of
newNTV7.png|thumb|new NTV7 logo|2018年3月5日起

## 参考文献

## 外部連結

  - [官方网站](http://extra.tonton.com.my/channel-home/ntv7)
  - [官方YouTube](http://www.youtube.com/user/ntv7malaysia/)
  - [官方面子书](http://www.facebook.com/ntv7malaysia)
  - [2013年電視大戲預告](https://web.archive.org/web/20130505093541/http://lifetv.com.my/node/48199)
  - [ntv7节目表](http://www.mptv360.com.my)

{{-}}

[Category:马来西亚電視台](../Category/马来西亚電視台.md "wikilink")
[Category:马来西亚公司](../Category/马来西亚公司.md "wikilink")