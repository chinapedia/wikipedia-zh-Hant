**Nullsoft腳本安裝系統**（英語：**Nullsoft Scriptable Install
System**，縮寫：NSIS）為一個[開放原始碼](../Page/開放原始碼.md "wikilink")[腳本驅動的封裝安裝檔用工具](../Page/腳本.md "wikilink")。可以用其[腳本語言自定安裝的流程](../Page/腳本語言.md "wikilink")，同時支援多種語系的安裝介面。

## 特色

  - 可腳本控制
  - 多國語系的安裝介面
  - 可用[C](../Page/C.md "wikilink")，[C++](../Page/C++.md "wikilink")，或[Delphi寫出NSIS的外掛](../Page/Delphi.md "wikilink")（Plugins）
  - 支援[ZLib](../Page/ZLib.md "wikilink")，[BZip2](../Page/BZip2.md "wikilink")，[LZMA這三種壓縮方式](../Page/LZMA.md "wikilink")
  - 支援網路安裝模式與檔案Patching
  - 相容於目前的所有主要的Windows版本

## 外部連結

  - [NSIS官網](http://nsis.sourceforge.net/Main_Page)（包含信息、示例、插件及更多内容的wiki站点）
  - [NSIS在SourceForge的项目首页](http://sourceforge.net/projects/nsis)
  - [NSIS用戶手冊](http://www.omega.idv.tw/nsis/Contents.html)
  - [SourceForge.net Project of the
    Month](https://web.archive.org/web/20070227162451/http://sourceforge.net/potm/potm-2006-01.php)
    2006年1月
  - [HM NIS EDIT](http://hmne.sourceforge.net/)（自由NSIS编辑器/IDE）
  - [Venis](https://web.archive.org/web/20070207051133/http://www.spaceblue.com/venis/)（NSIS的虚拟开发环境）
  - [Mihov免费NSIS编辑器](http://www.mihov.com/sw/en/nh.php)
  - [EclipseNSIS](http://eclipsensis.sourceforge.net/)（用[Eclipse编辑](../Page/Eclipse.md "wikilink")、编译和测试NSIS脚本）
  - [ExperienceUI for
    NSIS](http://xpui.sourceforge.net/?sourceid=wikipedia)（完全界面化的用户端）

[Category:安裝軟體](../Category/安裝軟體.md "wikilink")
[Category:自由安裝軟體](../Category/自由安裝軟體.md "wikilink")
[Category:Windows实用程序](../Category/Windows实用程序.md "wikilink")
[Category:用C編程的自由軟體](../Category/用C編程的自由軟體.md "wikilink")
[Category:用C++編程的自由軟體](../Category/用C++編程的自由軟體.md "wikilink")
[Category:綠色軟件](../Category/綠色軟件.md "wikilink")
[Category:SourceForge專案](../Category/SourceForge專案.md "wikilink")