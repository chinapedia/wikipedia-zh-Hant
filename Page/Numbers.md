**Numbers**是一款由[苹果公司开发的](../Page/苹果公司.md "wikilink")[電子試算表](../Page/電子試算表.md "wikilink")[应用程序](../Page/应用程序.md "wikilink")，作为办公软件套装[iWork的一部分](../Page/iWork.md "wikilink")，与[Keynote和](../Page/Keynote.md "wikilink")[Pages捆绑出售](../Page/Pages.md "wikilink")。Numbers
1.0于2007年8月7日发布，只能在苹果电脑[Mac OS X
v10.4](../Page/Mac_OS_X_v10.4.md "wikilink")（"Tiger"，老虎）和[Mac OS X
v10.5](../Page/Mac_OS_X_v10.5.md "wikilink")（"Leopard"，花豹）[操作系统上运行](../Page/操作系统.md "wikilink")。

Numbers的主要竞争对手是[Microsoft
Excel](../Page/Microsoft_Excel.md "wikilink")，但是它兼容Excel的文件格式。在推介的新闻发布会上，苹果CEO[史蒂夫·乔布斯展示了它的一些特色功能](../Page/史蒂夫·乔布斯.md "wikilink")，更友好的界面和更易懂的排版方式。
2013年10月22日发布了完全重新设计改造的新版本。

## 主要特点

  - “表单排版”方式：表单位于一个画板上，可以相对独立于其他表格、图表和图片，这样可以更轻松的在一个表单安放排版不同的表格
  - 表格中心的工作方式，容易通过表头和总结创建表单
  - 下拉式、选项框、拖拉式单元格
  - 从边栏拖放函数到单元格中
  - 打印预览模式下，可以进行函数编辑、实时缩放、自由移动表格以适应页面
  - [XML原生文件格式](../Page/XML.md "wikilink")，使用文件捆绑来实现媒体和关联文件
  - 可以导出到Microsoft Excel，但会缺少某些Excel功能，包括[Pivot
    table和](../Page/Pivot_table.md "wikilink")[Visual Basic for
    Applications](../Page/Visual_Basic_for_Applications.md "wikilink")

## 版本历史

| 版本号 | 发布日期        | 更改                                  |
| --- | ----------- | ----------------------------------- |
| 1.0 | 2007年8月7日   | 首发，作為新iWork 07軟件包的一部分發布             |
| 2.0 | 2009年1月6日   | 功能性和兼容性上增强，作為新iWork 09軟件包的一部分發布     |
| 3.0 | 2013年10月22日 | 完全重新设计改造。                           |
| 5.1 | 2018年6月14日  | 使用Latex或MathML記號加入數學方程式。改進CSV和文字輸入。 |

## 外部链接

  - [Apple - iWork - Numbers](http://www.apple.com/iwork/numbers/)
  - [Apple - iWork -
    Numbers](https://web.archive.org/web/20070928050830/http://www.apple.com.cn/iwork/numbers/)

  - [Apple - iWork -
    Numbers](https://web.archive.org/web/20130902180153/http://www.apple.com/tw/iwork/numbers/)

  - [Apple - iWork -
    Numbers](https://web.archive.org/web/20130808061215/http://www.apple.com/hk/iwork/numbers/)


[ru:IWork\#Numbers](../Page/ru:IWork#Numbers.md "wikilink")

[Category:MacOS軟體](../Category/MacOS軟體.md "wikilink")
[Category:電子製表](../Category/電子製表.md "wikilink")
[Category:蘋果公司軟體](../Category/蘋果公司軟體.md "wikilink")