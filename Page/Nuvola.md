**Nuvola**（名称来自[意大利语的](../Page/意大利语.md "wikilink")「[云](../Page/云.md "wikilink")」，是SKY图标主题的改进版）是一套自由软件图标，并在[GNU](../Page/GNU.md "wikilink")
[LGPL](../Page/LGPL.md "wikilink")
2.1下发布。Nuvola的作者是。这套图标最初是为像[KDE和](../Page/KDE.md "wikilink")[GNOME之类的](../Page/GNOME.md "wikilink")[桌面环境所开发](../Page/桌面环境.md "wikilink")，现在也已经有[Windows和](../Page/Windows.md "wikilink")[Macintosh的版本](../Page/Macintosh.md "wikilink")\[1\]。图标的最终版本1.0包含了将近600个图标。默认的图标为[PNG格式](../Page/PNG.md "wikilink")，但也提供了[SVG版本](../Page/SVG.md "wikilink")。

这套图标用丰富的色彩来表现了大量常用并易于辨认的物体。大部分图标颜色为蓝色，但也有其它颜色。

## 应用

除了[KDE和](../Page/KDE.md "wikilink")[GNOME](../Page/GNOME.md "wikilink")，Nuvola也应用在了[Pidgin即时聊天软件和](../Page/Pidgin.md "wikilink")[Amarok媒体播放器](../Page/Amarok.md "wikilink")。Nuvola是OpenLab
GNU/Linux发行版的默认图标主题。

## 图标示例

<File:Nuvola> apps personal.svg <File:Nuvola> apps package_toys.png
<File:Nuvola> filesystems folder home.svg <File:Nuvola> Spain flag.svg
<File:Gnome-dev-floppy.svg> <File:Gcalctool.svg> <File:Nuvola> apps
core.svg <File:Inkscape.svg> <File:Nuvola> apps important.svg
<File:Nuvola> apps edu miscellaneous.svg <File:Nuvola> mimetypes tar.png
<File:Nuvola> apps edu science.svg <File:Nuvola> mimetypes source.png
<File:Nuvola> apps atlantik.png <File:Nuvola> apps kopete.png
<File:Nuvola> gnome-fs-trash-full.svg <File:Gnome-globe.svg>
<File:Nuvola> devices cdrom unmount.svg <File:Nuvola> filesystems
folder_red_open.png <File:Nuvola> filesystems services.svg
<File:Gnome-dev-ipod.svg> <File:Gnome-dev-mouse.svg> <File:Gamepad.svg>
<File:Gnome-dev-removable-usb.svg> <File:Nuvola> apps konquest.svg
<File:Chess.svg> <File:Nuvola> apps kpovmodeler.svg <File:Nuvola> apps
colors.png <File:Nuvola> mimetypes pdf.png <File:Nuvola> apps kate.png
<File:Nuvola> apps bookcase.svg <File:Newspaper> Cover.svg <File:Stop>
hand nuvola.svg

## 参见

  - [開放美工圖庫](../Page/開放美工圖庫.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [Nuvola主页](https://web.archive.org/web/20080301031757/http://icon-king.com/?p=15)
    David Vignoni的网站，有KDE下的PNG版本。
  - [gnome-themes-extras](http://ftp.gnome.org/pub/GNOME/sources/gnome-themes-extras/0.9/gnome-themes-extras-0.9.0.tar.gz)，一个带有Nuvola在GNOME下的SVG版本的软件包

[Nuvola](../Category/图标.md "wikilink")
[Category:自由軟件](../Category/自由軟件.md "wikilink")

1.