**O**, **o**
是[拉丁字母中的第](../Page/拉丁字母.md "wikilink")15个[字母](../Page/字母.md "wikilink")。

在[希腊语](../Page/希腊语.md "wikilink") [Ο
(Omicron)](../Page/Ο.md "wikilink")、[伊特鲁里亚语和](../Page/伊特鲁里亚语.md "wikilink")[拉丁语中](../Page/拉丁语.md "wikilink")，**O**表示元音／o／。虽然闪族语字母[Ayin在一些字母表中被用来作为转写](../Page/Ayin.md "wikilink")\[o\]，但是它的通常都作为辅音\[/ʕ/\]使用，如[阿拉伯字母ع](../Page/阿拉伯字母.md "wikilink")
'ʿAyn'。

在[北约音标字母中](../Page/北约音标字母.md "wikilink")，O被稱為*Oscar*。

## 字母O的含意

[O](../Page/O.md "wikilink")

## 字符编码

| 字符编码                               | [ASCII](../Page/ASCII.md "wikilink") | [Unicode](../Page/Unicode.md "wikilink") | [EBCDIC](../Page/EBCDIC.md "wikilink") | [摩斯电码](../Page/摩斯电码.md "wikilink") |
| ---------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- | ---------------------------------- |
| [大写](../Page/大写字母.md "wikilink") O | 0x4F (79)                            | 004F                                     | 214                                    | `---`                              |
| [小写](../Page/小写字母.md "wikilink") o | 0x6F (111)                           | 006F                                     | 150                                    |                                    |

## 其他表示方法

## 参看

  - 数字[0](../Page/0.md "wikilink")

  - [°](../Page/°.md "wikilink")（[角度符号](../Page/角度.md "wikilink")）

  - （[希腊字母](../Page/希腊字母.md "wikilink") Omicron）

  - （[西里尔字母](../Page/西里尔字母.md "wikilink") O）

## 外部連結

[Category:拉丁字母](../Category/拉丁字母.md "wikilink")