**OGLE-TR-111**是一顆位於[船底座的](../Page/船底座.md "wikilink")[暗星](../Page/恆星.md "wikilink")，其光譜類型介乎G和K之間，與[太陽相似](../Page/太陽.md "wikilink")。它距離地球大約5000[光年](../Page/光年.md "wikilink")，其[視星等為](../Page/視星等.md "wikilink")15.55。由於該恆星並不起眼，故不少[星表均未有收錄](../Page/星表.md "wikilink")。

2002年，有人偵測到該恆星的光度每隔4天會略微變暗，其後於2004年證實是由[行星](../Page/行星.md "wikilink")[凌日所造成的](../Page/凌日.md "wikilink")，該行星被給予
"[OGLE-TR-111 b](../Page/OGLE-TR-111_b.md "wikilink")" 這個編號。

2005年，有跡象顯示該恆星可能存在另一顆凌日行星，該未知天體被編為[OGLE-TR-111
c](../Page/OGLE-TR-111_c.md "wikilink")。如果發現得以證實，它將成為首顆擁有雙凌日行星的恆星。

## 參見

  - [OGLE-TR-111 b](../Page/OGLE-TR-111_b.md "wikilink")
  - [OGLE-TR-111 c](../Page/OGLE-TR-111_c.md "wikilink")

### 外部連結

  - [The Extrasolar Planets
    Encyclopaedia](https://web.archive.org/web/20050519023444/http://www.obspm.fr/encycl/encycl.html)
    [entry](https://web.archive.org/web/20050310130514/http://www.obspm.fr/encycl/OGLE-TR-111.html)
  - [Extrasolar
    Visions](https://web.archive.org/web/20130902213440/http://www.extrasolar.net/)
    [entry](https://web.archive.org/web/20050310022900/http://www.extrasolar.net/planettour.asp?StarCatID=normal&PlanetID=267)

[Category:黃矮星](../Category/黃矮星.md "wikilink")
[Category:船底座](../Category/船底座.md "wikilink")
[Category:行星凌變星](../Category/行星凌變星.md "wikilink")