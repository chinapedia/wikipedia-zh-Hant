**OGLE-TR-111
b**是一顆[太陽系外行星](../Page/太陽系外行星.md "wikilink")，其母[恆星是](../Page/恆星.md "wikilink")[OGLE-TR-111](../Page/OGLE-TR-111.md "wikilink")。

2002年，[光學重力透鏡實驗小組](../Page/光學重力透鏡實驗.md "wikilink")（OGLE）偵測到該恆星的光度每隔4天會略微變暗，並得知是由一顆類似[行星的天體](../Page/行星.md "wikilink")[凌日造成的](../Page/凌日.md "wikilink")。但當時並沒有對該天體的質量作出量度，因此尚未確定該天體為一行星、[褐矮星或其它](../Page/褐矮星.md "wikilink")。至2004年，透過[徑向速度的量度](../Page/徑向速度.md "wikilink")，得知該凌日天體為一行星。

該行星與不少「[熱木星](../Page/熱木星.md "wikilink")」天體相似，其大小與[木星接近](../Page/木星.md "wikilink")，質量約為木星的一半，與日距離約為0.05[天文單位](../Page/天文單位.md "wikilink")。

## 參見

  - [OGLE-TR-113](../Page/OGLE-TR-113.md "wikilink")

[Category:太陽系外行星](../Category/太陽系外行星.md "wikilink")