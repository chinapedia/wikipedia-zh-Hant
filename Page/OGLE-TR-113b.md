**OGLE-TR-113b**是一個環繞[OGLE-TR-113的](../Page/OGLE-TR-113.md "wikilink")[太陽系外行星](../Page/太陽系外行星.md "wikilink")，比[木星大](../Page/木星.md "wikilink")
1.32倍，屬於[熱木星](../Page/熱木星.md "wikilink")。

## 參見

  - [OGLE-TR-113](../Page/OGLE-TR-113.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:太陽系外行星](../Category/太陽系外行星.md "wikilink")