**OGLE-TR-132b**是一顆環繞[OGLE-TR-132的](../Page/OGLE-TR-132.md "wikilink")[太陽系外行星](../Page/太陽系外行星.md "wikilink")，比木星大1.14倍，它離母恆星非常的近，只有0.03[天文單位](../Page/天文單位.md "wikilink")，是太陽離[水星的十三分之一](../Page/水星.md "wikilink")，另外它的公轉週期也非常的短，只有1.6地球日。

## 參見

  - [OGLE-TR-132](../Page/OGLE-TR-132.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:太陽系外行星](../Category/太陽系外行星.md "wikilink")