**OGLE-TR-56**是一顆位於[人馬座的](../Page/人馬座.md "wikilink")[恆星](../Page/恆星.md "wikilink")\[1\]，於2003年證實有一顆[日外行星環繞其運行](../Page/日外行星.md "wikilink")。該恆星的行星因為有凌星現象，因此在行星發現相關論文中被歸類為食變星\[2\]。

2002年，位於[波蘭的](../Page/波蘭.md "wikilink")[光學重力透鏡實驗小組](../Page/光學重力透鏡實驗.md "wikilink")（OGLE）以凌日觀測的方法找到這顆恆星的行星，編號為**OGLE-TR-56b**，並於2003年以多普勒方式確證\[3\]。這顆行星屬於[熱木星](../Page/熱木星.md "wikilink")，質量比[木星大一點](../Page/木星.md "wikilink")，為木星的1.29
± 0.12倍，其公轉週期為1.21天，截至2006年，這顆行星是所有被人類發現的日外行星當中，公轉週期最短的行星\[4\]。

## 參考資料

## 外部連結

  - [Planet:
    OGLE-TR-56b](https://web.archive.org/web/20070905035618/http://vo.obspm.fr/exoplanetes/encyclo/planet.php?p1=OGLE-TR-56&p2=b)

  - ([web Preprint](http://xxx.lanl.gov/abs/astro-ph/0301052v1))

  -
  -
[Category:人马座](../Category/人马座.md "wikilink")
[Category:黃矮星](../Category/黃矮星.md "wikilink")
[Category:太陽系外行星](../Category/太陽系外行星.md "wikilink")
[Category:行星凌變星](../Category/行星凌變星.md "wikilink")

1.

2.
3.

4.