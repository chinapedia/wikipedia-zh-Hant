**OK超商**（**來來超商股份有限公司**）是一個成立於[臺灣的](../Page/臺灣.md "wikilink")[超商](../Page/超商.md "wikilink")[加盟體系](../Page/加盟.md "wikilink")，原與[美國](../Page/美國.md "wikilink")[Circle
K合作](../Page/OK便利店.md "wikilink")，后变为獨立的台灣[便利商店品牌](../Page/便利商店.md "wikilink")。

## 歷史

1988年9月，[豐群企業集團與美國](../Page/豐群企業集團.md "wikilink")[Circle
K合資成立](../Page/OK便利店.md "wikilink")**眾利超商股份有限公司**，由Circle
K美國連鎖總部提供技術合作、授權以及顧問諮詢，同年改名為**富群超商股份有限公司**。初期除在臺北開店外，還採取在基隆地區開設分店，以發展「鄉村包圍都市」的開店策略，且均為直營店，直到1998年才開始發展加盟事業。1998年、2005年先後推出「委託加盟制度」及「特許加盟制度」兩種[加盟創業的方式](../Page/加盟.md "wikilink")，希望能提供有心創業人士一個「低風險、高保障、獲利穩定」的創業機會，因此目前的經營方式是直營與加盟並行。

2005年由於豐群企業集團與Circle K的技術合作契約到期，再加以富群超商已經發展出一套可以獨立操作的管理模式，因此與Circle
K解約，公司名稱變更為**來來超商股份有限公司**，並將[企業識別標誌](../Page/企業識別標誌.md "wikilink")（CI）改成**OK便利店**\[1\]。2007年加入[聯合信用卡處理中心梅花便利店](../Page/聯合信用卡處理中心.md "wikilink")，成為全臺第一家提供「接觸式」及「感應式」信用卡付款的便利商店。2007年12月底，商標名稱由原來的**OK便利店**改為**OK·MART**（OK超商），相關店舖的招牌也於月底前完成更新；2008年，[統一發票上的商標名稱亦改為OK](../Page/統一發票_\(臺灣\).md "wikilink")·MART。2015年10月1日，與[一卡通公司](../Page/一卡通公司.md "wikilink")、[東森整合行銷合作發行結合](../Page/東森整合行銷.md "wikilink")[得易Ponta會員與超商實體會員集點服務的](../Page/得易Ponta.md "wikilink")[一卡通票證](../Page/一卡通_\(台灣\).md "wikilink")「OK好利聯名卡」\[2\]\[3\]\[4\]\[5\]。

[OK_Mart_head_office_20120713.jpg](https://zh.wikipedia.org/wiki/File:OK_Mart_head_office_20120713.jpg "fig:OK_Mart_head_office_20120713.jpg")
[OK_Mart_Keelung_Chang-an_Store_20150220.jpg](https://zh.wikipedia.org/wiki/File:OK_Mart_Keelung_Chang-an_Store_20150220.jpg "fig:OK_Mart_Keelung_Chang-an_Store_20150220.jpg")[七堵區OK](../Page/七堵區.md "wikilink")·Mart基隆長安店\]\]
[OK超商_楊梅瑞興店_(梅獅路二段)_-_panoramio.jpg](https://zh.wikipedia.org/wiki/File:OK超商_楊梅瑞興店_\(梅獅路二段\)_-_panoramio.jpg "fig:OK超商_楊梅瑞興店_(梅獅路二段)_-_panoramio.jpg")[楊梅區OK](../Page/楊梅區.md "wikilink")·Mart楊梅瑞興店\]\]

至2018年12月為止，OK·MART全臺灣有920家分店，分布在[臺灣本島的西部地區和](../Page/台灣本島.md "wikilink")[宜蘭](../Page/宜蘭縣.md "wikilink")，在[臺北藝術大學](../Page/臺北藝術大學.md "wikilink")、[嘉義大學兩個校區](../Page/嘉義大學.md "wikilink")、[德明財經科技大學](../Page/德明財經科技大學.md "wikilink")、[吳鳳科技大學](../Page/吳鳳科技大學.md "wikilink")、[臺北海洋科技大學淡水校區](../Page/台北海洋科技大學.md "wikilink")、[崇右影藝科技大學](../Page/崇右影藝科技大學.md "wikilink")、[景文科技大學](../Page/景文科技大學.md "wikilink")、[嶺東科技大學](../Page/嶺東科技大學.md "wikilink")、[修平科技大學](../Page/修平科技大學.md "wikilink")、[高雄應用科技大學](../Page/高雄應用科技大學.md "wikilink")、[國防大學](../Page/國防大學.md "wikilink")、[靜宜大學](../Page/靜宜大學.md "wikilink")、[真理大學](../Page/真理大學.md "wikilink")、[高雄師範大學](../Page/高雄師範大學.md "wikilink")、[新北市三民高中](../Page/新北市立三民高級中學.md "wikilink")、[新竹女中](../Page/新竹女中.md "wikilink")、[大甲高中](../Page/大甲高中.md "wikilink")、[桃園市立壽山高級中學](../Page/桃園市立壽山高級中學.md "wikilink")、[臺中家商](../Page/臺中家商.md "wikilink")、[家齊高中](../Page/家齊高中.md "wikilink")、[屏東高中](../Page/屏東高中.md "wikilink")、[南崁高中等學校以及臺鐵](../Page/南崁高中.md "wikilink")[羅東車站](../Page/羅東車站.md "wikilink")、[七堵車站](../Page/七堵車站.md "wikilink")、[瑞芳車站](../Page/瑞芳車站.md "wikilink")、[浮洲車站](../Page/浮洲車站.md "wikilink")、[埔心車站](../Page/埔心車站.md "wikilink")、[新豐車站](../Page/新豐車站.md "wikilink")、[竹北車站](../Page/竹北車站.md "wikilink")、[苗栗車站](../Page/苗栗車站.md "wikilink")、[沙鹿車站](../Page/沙鹿車站.md "wikilink")、[彰化車站第三月臺](../Page/彰化車站.md "wikilink")、[斗南車站](../Page/斗南車站.md "wikilink")、[善化車站](../Page/善化車站.md "wikilink")、[楠梓車站等車站設有分店](../Page/楠梓車站.md "wikilink")。

2018年6月，推出主打全台灣最迷你[便利商店的店型](../Page/便利商店.md "wikilink")「OKmini」，結合[物聯網技術及即時補貨app等](../Page/物聯網.md "wikilink")，用智慧型[自動販賣機的形式](../Page/自動販賣機.md "wikilink")，用最小的空間，快速攻占特殊型的商圈，如：醫院、學校、捷運站、商辦、公園等，滿足消費者的「便利性」\[6\]，力推無現金支付支付，在2018年8月進駐[台北市市政大樓](../Page/台北市市政大樓.md "wikilink")「無現金消費體驗館」\[7\]。

## 参见

  - [便利商店](../Page/便利商店.md "wikilink")
  - [連鎖店](../Page/連鎖店.md "wikilink")
  - [Ponta](../Page/Ponta.md "wikilink")

## 註釋

## 外部連結

  - [OK超商](http://www.okmart.com.tw/)

  -
[Category:台灣便利商店](../Category/台灣便利商店.md "wikilink")
[Category:台灣未上市公司](../Category/台灣未上市公司.md "wikilink")
[Category:1988年成立的公司](../Category/1988年成立的公司.md "wikilink")
[Category:總部位於臺北市信義區的工商業機構](../Category/總部位於臺北市信義區的工商業機構.md "wikilink")

1.  [OK換招牌結束與美合作](http://www.appledaily.com.tw/appledaily/article/finance/20051006/2106879/)
2.  [得易Ponta結盟2勢力
    萊爾富OK大打團體戰](http://www.cardu.com.tw/news/detail.php?nt_pk=8&ns_pk=27483)
3.
4.
5.
6.  劉馥瑜，[四大超商
    科技化時代來臨](http://www.chinatimes.com/newspapers/20180607000360-260204)，中時電子報，2018-06-06
7.  李鴻典，[力推無現金支付　最小的超商OKmini巡迴開跑](https://www.setn.com/News.aspx?NewsID=409567)，三立新聞，2018-07-29