**Office Open XML**（縮寫：**Open
XML**、**OpenXML**或**OOXML**），為由[Microsoft開發的一種以](../Page/Microsoft.md "wikilink")[XML為基礎並以](../Page/XML.md "wikilink")[ZIP格式壓縮的電子文件規範](../Page/ZIP格式.md "wikilink")，支持[文件](../Page/文件.md "wikilink")、[表格](../Page/表格.md "wikilink")、[備忘錄](../Page/備忘錄.md "wikilink")、[幻燈片等檔案格式](../Page/幻燈片.md "wikilink")。

OOXML在[2006年12月成為了](../Page/2006年12月.md "wikilink")[ECMA規範的一部分](../Page/ECMA.md "wikilink")，編號為ECMA-376；並於[2008年4月通過](../Page/2008年4月.md "wikilink")[國際標準化組織的表決](../Page/國際標準化組織.md "wikilink")，在兩個月後公佈為[ISO](../Page/ISO.md "wikilink")／[IEC](../Page/IEC.md "wikilink")
29500國際標準。微軟推出這個格式，很多人認為是出於商業考量。\[1\]許多專家指出，該標準並不是個完整的標準，採用了許多[微軟的獨有規格](../Page/微軟.md "wikilink")，使用上困難重重。\[2\]\[3\]

从Microsoft Office 2007开始，Office Open XML文件格式已经成为Microsoft
Office默认的文件格式。\[4\]\[5\]\[6\]Microsoft Office
2010支持对ECMA-376标准文档的读操作，ISO/IEC 29500 Transitional的读/写，ISO/IEC
29500 Strict的读取。\[7\]Microsoft Office 2013同时支持ISO/IEC 29500
Strict的读写操作。\[8\]

它的競爭對手是[OpenDocument
Format](../Page/OpenDocument_Format.md "wikilink")，後者是被廣泛接受的一種開放的文檔存儲和交換規範。

## 版本

存在以下几个版本的Office Open XML标准。

### ECMA 376

ECMA
376，目前歷經4個版本，第1版（2006年12月）、第2版（2008年12月）、第3版（2011年6月）、第4版（2012年12月）。\[9\]

### ISO/IEC 29500

ISO/IEC 29500目前最新的版本為2012年的版本
ISO/IEC标准的结构分为四部分。第1、2和3是独立的标准，第2部分用于其他文件格式，包括[Design_Web_Format和](../Page/Design_Web_Format.md "wikilink")[XPS格式](../Page/XML纸张规范.md "wikilink")。第4部分作用是读出第1部分的变体。\[10\]

  - 第1部分（基础知识和标记语言参考）
  - 第2部分（解包约定）
  - 第3部分（标记兼容性和可扩展性）
  - 第4部分（过渡期迁移特性）

## 批評

微軟公司發表的Office Open
XML使用許多非標準的規範，造成與其他辦公室軟體(如[LibreOffice](../Page/LibreOffice.md "wikilink"))讀取時發生不相容或內容偏移的情形，目的是讓[Microsoft
Office保持市場優勢](../Page/Microsoft_Office.md "wikilink")。

[ODF](../Page/ODF.md "wikilink")(廣泛接受的開放文檔規範)編碼時會使用其他標準規範(如[ISO
639](../Page/ISO_639.md "wikilink")、[MathML](../Page/MathML.md "wikilink"))來進行儲存，但OOXML使用非標準的編碼進行存取。例如
ODF 裡面的顏色代碼，不管是試算表、文件、簡報等，紅色的代碼都是 \#FF0000，然而在OOXML裏隨不同產品，代碼分別為：
Word：\#FF0000， Excel：\#FFFF0000， Powerpoint：\#FF0000， 。

OOXML設計的目的是將微軟定義的 doc、ppt、xls 二進制格式轉成 XML 格式，並沒有依照 XML
的特性最佳化，而其私有格式內含的額外非標準元件也包含在內，例如
[ActiveX](../Page/ActiveX.md "wikilink") 等等（過時，且容易用於攻擊）；第二個原因是
[比尔·盖茨](../Page/比尔·盖茨.md "wikilink") 在 1998
年留下的備忘錄，提到「不能讓其他瀏覽器可完美顯示 MS
Office 文件，只能讓自家公司的專屬 [IE](../Page/Internet_Explorer.md "wikilink")
可正確顯示」，現在的狀況可以說是微軟延續了比爾·蓋茲的精神，使「現代的 MS Office 存出的 OOXML
檔」刻意做成第三方軟體不能完善地解讀、呈現，顯示OOXML並非如其所聲稱的開放。\[11\]

## 参考资料

[Category:文件格式](../Category/文件格式.md "wikilink") [Category:Microsoft
Office](../Category/Microsoft_Office.md "wikilink")
[Category:IEC標準](../Category/IEC標準.md "wikilink")
[Category:Ecma標準](../Category/Ecma標準.md "wikilink")
[Category:基于XML的标准](../Category/基于XML的标准.md "wikilink")

1.
2.
3.
4.
5.
6.
7.  [Overview of the XML file formats in
    Office 2010](http://technet.microsoft.com/en-us/library/cc179190.aspx)
8.
9.
10.
11. [微軟文書格式為何常跑掉？揭開 OOXML
    格式大祕辛](https://technews.tw/2017/08/16/the-big-secret-of-ooxml/)