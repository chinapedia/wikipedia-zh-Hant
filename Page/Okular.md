**Okular**、Piotr Szymanski由2005年[Google Summer of
Code开发的项目](../Page/Google_Summer_of_Code.md "wikilink")、以[KPDF为基础](../Page/KPDF.md "wikilink")、并代替[KPDF](../Page/KPDF.md "wikilink")、[KGhostView](../Page/KGhostView.md "wikilink")、[KFax](../Page/KFax.md "wikilink")、[KFaxview](../Page/KFaxview.md "wikilink")、与[KDVI](../Page/KDVI.md "wikilink")，成为[KDE
4的文档查看器](../Page/KDE_4.md "wikilink")\[1\]\[2\]。

Okular可以支持下述的文件格式\[3\]：

  - [Portable Document
    Format](../Page/Portable_Document_Format.md "wikilink")
    (PDF)（[Poppler后端](../Page/Poppler.md "wikilink")）
  - [PostScript](../Page/PostScript.md "wikilink")（[libgs后端](../Page/libgs.md "wikilink")）
  - [Tagged Image File
    Format](../Page/Tagged_Image_File_Format.md "wikilink")
    (TIFF)（[libTIFF后端](../Page/libTIFF.md "wikilink")）
  - [HTML Help](../Page/HTML_Help.md "wikilink")
    (CHM)（[libCHM后端](../Page/libCHM.md "wikilink")）
  - [DjVu](../Page/DjVu.md "wikilink")（[DjVuLibre后端](../Page/DjVuLibre.md "wikilink")）
  - [Device independent file
    format](../Page/Device_independent_file_format.md "wikilink") (DVI)
  - [开放XML纸张规范](../Page/XML纸张规范.md "wikilink") (XPS)
  - [OpenDocument](../Page/OpenDocument.md "wikilink") format (ODF)
  - [FictionBook](../Page/FictionBook.md "wikilink")
  - [Comicbook](../Page/Comicbook.md "wikilink")
  - [Plucker](../Page/Plucker.md "wikilink")
  - [EPUB](../Page/EPUB.md "wikilink")
  - [mobi](../Page/mobi.md "wikilink") (amazon kindle的电子书格式)

## 参考文献

## 參見

  - [Evince](../Page/Evince.md "wikilink")
  - [PDF軟體列表](../Page/PDF軟體列表.md "wikilink")

## 外部連結

  - [Okular 主頁](http://okular.kde.org)
  - [通向KDE4之路（七）：文档查看器Okular和Ligature](http://www.kdecn.org/dot/index.php?id=1171453163)

[Category:自由PDF软件](../Category/自由PDF软件.md "wikilink")
[Category:应用软件](../Category/应用软件.md "wikilink")
[Category:PDF閱讀器](../Category/PDF閱讀器.md "wikilink")

1.
2.
3.  <http://okular.kde.org/formats.php>