**一二走航空269號班機**（OG
269）是一班由泰國[曼谷廊曼機場飛往泰國](../Page/曼谷國際機場.md "wikilink")[布吉國際機場的](../Page/布吉國際機場.md "wikilink")[一二走航空國內線定期航班](../Page/一二走航空.md "wikilink")。2007年9月16日下午14:00-15:20，該機由曼谷飛往[布吉途中疑遇上](../Page/布吉.md "wikilink")[風切變](../Page/風切變.md "wikilink")，降落布吉機場時滑出[跑道撞向大樹](../Page/跑道.md "wikilink")，機身斷成兩截，[爆炸](../Page/爆炸.md "wikilink")[着火](../Page/火災.md "wikilink")，至少89人[死亡](../Page/死亡.md "wikilink")。

## 其他

這是涉入MD-80型客機的第18次空難，當中14次有人命傷亡。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

## 相關條目

  - [空難列表](../Page/空難列表.md "wikilink")
  - [風切變](../Page/風切變.md "wikilink")
  - [微下擊暴流](../Page/微下擊暴流.md "wikilink")
  - [地面迫近警告系統](../Page/地面迫近警告系統.md "wikilink")

## 外部連結

  - Aircraft Accident Investigation Committee, Ministry of Transport of
    Thailand:
      - [Final
        report](http://www.skybrary.aero/bookshelf/books/1980.pdf)
        ([Archive](http://www.webcitation.org/6EATBw8JP))
      - "[INTERIM REPORT ONE TWO GO AIRLINES COMPANY LIMITED MCDONNELL
        DOUGLAS DC-9-82 (MD-82) HS-OMG PHUKET INTERNATIONAL AIRPORT
        THAILAND 16
        SEPTEMBER 2007](https://www.webcitation.org/6CJeGxALS?url=http://www.aviation.go.th/doc/Interim%20Report.pdf)."
        AAIC ()
  - [普吉島空難
    泰平價班機88死](http://tw.news.yahoo.com/article/url/d/a/070916/2/ko6r.html)
  - [Airliners.net](http://www.airliners.net/search/photo.search?regsearch=HS-OMG)
    HS-OMG的照片
  - [泰空難死亡人數修定為89人](http://inews.i-cable.com/webapps/news_detail.php?id=247244&category=3)
  - [泰墜機疑因遇上風切變](http://inews.i-cable.com/webapps/news_detail.php?id=247242&category=3)

[Category:2007年航空事故](../Category/2007年航空事故.md "wikilink")
[Category:泰國航空事故](../Category/泰國航空事故.md "wikilink")
[Category:2007年9月](../Category/2007年9月.md "wikilink")
[Category:天氣因素觸發的航空事故](../Category/天氣因素觸發的航空事故.md "wikilink")

1.  [Accident Database: Aircraft Crash Details \> McDonnell Douglas
    MD-8](http://www.airdisaster.com/cgi-bin/aircraft_detail.cgi?aircraft=McDonnell+Douglas+MD-8)