〈**Only You (And You Alone)**〉，常简称为〈**Only
You**〉，是著名[黑人乐队](../Page/黑人.md "wikilink")[派特斯于](../Page/派特斯.md "wikilink")1955年唱红的一首经典流行歌曲。该曲的作者是[巴克·兰姆和安迪](../Page/巴克·兰姆.md "wikilink")·兰德。

派特斯首次演唱该曲并不成功，但是在1955年7月3日再次推出此曲后，获得了极大的成功。该曲在全美[R\&B榜单中连续](../Page/R&B.md "wikilink")7个星期位居榜首。包括[约翰·列侬和](../Page/约翰·列侬.md "wikilink")[林格·斯塔在内的多位歌手曾经翻唱过此曲](../Page/林格·斯塔.md "wikilink")。该曲也出现在许多[好莱坞](../Page/好莱坞.md "wikilink")[电影中](../Page/电影.md "wikilink")，作为背景音乐；也有一些廣告、網路背景音樂喜歡用該首歌。

## 翻唱

在[香港](../Page/香港.md "wikilink")，一個十分有名的[恶搞版是由](../Page/恶搞.md "wikilink")[刘镇伟重新填词](../Page/刘镇伟.md "wikilink")、[卢冠廷编曲](../Page/卢冠廷.md "wikilink")，在电影[-{zh:《西遊記第壹佰零壹回之月光寶盒》;zh-hant:《西遊記第壹佰零壹回之月光寶盒》;zh-hk:《西遊記第壹佰零壹回之月光寶盒》;zh-cn:《大话西游之月光宝盒》;zh-tw:《齊天大聖東遊記》;zh-sg:《西游记第101回之月光宝盒》;zh-mo:《西遊記第壹佰零壹回之月光寶盒》;}-中由](../Page/西游记第壹佰零壹回之月光宝盒.md "wikilink")[羅家英翻唱的](../Page/羅家英.md "wikilink")。

## 訛傳

網路不少有謠傳此曲有「貓王」[-{zh-tw:艾維斯·普里斯萊;zh-hk:埃爾維斯·皮禮士利;
zh-cn:埃尔维斯·普雷斯利;}-的版本](../Page/艾維斯·普里斯萊.md "wikilink")\[1\]，而事實-{zh-tw:艾維斯·普里斯萊;zh-hk:埃爾維斯·皮禮士利;
zh-cn:埃尔维斯·普雷斯利;}-從未演唱過此曲。

## 參考

[Category:1954年單曲](../Category/1954年單曲.md "wikilink")
[Category:美國歌曲](../Category/美國歌曲.md "wikilink")
[Category:1955年單曲](../Category/1955年單曲.md "wikilink")
[Category:1974年單曲](../Category/1974年單曲.md "wikilink")
[Category:1996年單曲](../Category/1996年單曲.md "wikilink")

1.  \[<https://www.google.com.tw/search?client=tablet-android-samsung&ei=xjcAWqSTJ8qu0ATCjor4Bw&q=“Elvis”OR“Elvis+Presley”OR“Elvis+Aaron+Presley”OR“Elvis+Aron+Presley”OR“貓王”OR“埃爾維斯·皮禮士利”OR“埃尔维斯·普雷斯利”+%2B+“Only+You”&oq=“Elvis”OR“Elvis+Presley”OR“Elvis+Aaron+Presley”OR“Elvis+Aron+Presley”OR“貓王”OR“埃爾維斯·皮禮士利”OR“埃尔维斯·普雷斯利”+%2B+“Only+You”&gs_l=psy-ab.3>...10578.13160.0.14055.12.9.0.0.0.0.0.0..0.0....0...1.1.64.psy-ab..12.0.0....0.1D_3rmKCJc4
    Google搜尋結果\]約640,000項 2017年11月6日