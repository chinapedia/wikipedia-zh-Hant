**OpenOffice.org Draw**（又稱**Draw**,**OpenOffice Draw**，**OpenOffice
繪圖**或**OO.o
繪圖**）是一個向量繪圖軟體，它是[OpenOffice.org辦公軟體套裝的一部份](../Page/OpenOffice.org.md "wikilink")。它的特色在於多樣的「接頭」形狀，可以用在多種不同的線條上，方便建築繪圖，以及[流程圖上](../Page/流程圖.md "wikilink")，與[Microsoft
Visio以及](../Page/Microsoft_Visio.md "wikilink")[CorelDRAW等向量繪圖軟體功能類似](../Page/CorelDRAW.md "wikilink")，OpenOffice.org
Draw也有若干[桌上排版](../Page/桌上排版.md "wikilink")（DTP）軟體（如[Microsoft
Publisher以及](../Page/Microsoft_Publisher.md "wikilink")[Adobe
InDesign](../Page/Adobe_InDesign.md "wikilink")）的特性，因此亦可以用來進行簡單的印刷品排版編輯與設計。

OpenOffice.org的使用者也可安裝[開放美工圖庫](../Page/開放美工圖庫.md "wikilink")，這是一個巨大的畫廊，含有旗標、標誌、橫額，可用於一般簡報和製圖。[Linux的](../Page/Linux.md "wikilink")[Debian和](../Page/Debian.md "wikilink")[Ubuntu已內含](../Page/Ubuntu.md "wikilink")[開放美工圖庫](../Page/開放美工圖庫.md "wikilink")。

## SVG支援

對於[SVG格式的崛起](../Page/SVG.md "wikilink")，OpenOffice.org
Draw對於SVG的匯入及匯出已變得日益重要。目前的OpenOffice.org正式支援輸出SVG格式，但仍有一些限制要解決。\[1\]

SVG的匯入濾波器\[2\]仍在深入發展，它需要安裝Java Runtime
Environment。在SVG的匯入濾波器逐漸成熟的同時，使用者將能夠使用OpenOffice.org
Draw編輯大量來自[開放美工圖庫的SVG圖片](../Page/開放美工圖庫.md "wikilink")，而不是只能編輯[點陣圖](../Page/點陣圖.md "wikilink")、或者只能用其他的SVG編輯器（如[Inkscape](../Page/Inkscape.md "wikilink")）而已。

## 參見

  - [開放美工圖庫](../Page/開放美工圖庫.md "wikilink")
  - [OpenOffice.org](../Page/OpenOffice.org.md "wikilink")
  - [Writer](../Page/OpenOffice.org_Writer.md "wikilink")
  - [Calc](../Page/OpenOffice.org_Calc.md "wikilink")
  - [Impress](../Page/OpenOffice.org_Impress.md "wikilink")
  - [Math](../Page/OpenOffice.org_Math.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:办公室自动化软件](../Category/办公室自动化软件.md "wikilink")
[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:向量圖形編輯器](../Category/向量圖形編輯器.md "wikilink")

1.  [1](http://graphics.openoffice.org/svg/svg.htm)
2.  [2](http://wiki.services.openoffice.org/wiki/SVG_Import_Filter)