**OpenVanilla**（簡稱**OV**）計畫在[台灣也常被稱為](../Page/台灣.md "wikilink")「香草輸入法」，是一套跨平台的[輸入法及文字輸出的處理架構](../Page/輸入法.md "wikilink")；由[台灣團隊於](../Page/台灣.md "wikilink")
2004 年開始開發，迄今已有 [Mac OS
X](../Page/Mac_OS_X.md "wikilink")，[Windows](../Page/Windows.md "wikilink")
以及供
[Linux](../Page/Linux.md "wikilink")／[FreeBSD](../Page/FreeBSD.md "wikilink")
各種作業系統使用的版本，可處理簡繁[中文](../Page/中文.md "wikilink")、台語[白話字](../Page/白話字.md "wikilink")、[日文以及](../Page/日文.md "wikilink")[藏文等語言](../Page/藏文.md "wikilink")。OV
也是一套開放原始碼的[自由軟體](../Page/自由軟體.md "wikilink")。

## 簡介

OV 最初設計的目的，是為了解決 [Mac OS X](../Page/Mac_OS_X.md "wikilink")
上[繁體中文輸入法不盡理想的長久問題](../Page/繁體中文.md "wikilink")。OV
延續
[SpaceChewingOSX](http://rt.openfoundry.org/Foundry/Project/Wiki/100/index.html)
與 [香草輸入法](http://rt.openfoundry.org/Foundry/Project/Wiki/170/index.html)
的開放源碼模式，提出一套架構，讓撰寫輸入法變得更加容易。其後因為其框架的設計彈性，加入了文字輸出過濾器（output filter），讓 OV
成為相當適合實驗各種輸入法的環境。

[Mac OS X](../Page/Mac_OS_X.md "wikilink") 上的 OpenVanilla，是 [Universal
Binary](../Page/通用二進位.md "wikilink") 的執行檔，因此無論在使用
[PowerPC](../Page/PowerPC.md "wikilink") 或是
[Intel](../Page/Intel.md "wikilink") 的 [CPU](../Page/CPU.md "wikilink")
的[麥金塔電腦上](../Page/麥金塔.md "wikilink")，均可使用。

OV 的安裝套件中，包含程式核心，以及輸入法模組、文字輸出模組、詞彙管理模組以及偏好設定程式。

## 開發團隊

OV 的開發者包括：劉燈、姜天戩、劉康民、楊維中、陳侃如、洪任諭、張琮翔、陳柏中等人。

## 套件模組內容

OV
團隊本身提供了繁體中文的[注音](../Page/注音.md "wikilink")、智慧型注音輸入法：[酷音](http://chewing.csie.net/)（使用[新酷音輸入法團隊的](../Page/新酷音輸入法.md "wikilink")
libchewing）、[行列](../Page/行列輸入法.md "wikilink")、泛用輸入法、Holo（「[台語](../Page/台語.md "wikilink")」）[白話字以及](../Page/白話字.md "wikilink")[藏文輸入法](../Page/藏文.md "wikilink")。透過泛用輸入法模組，使用者可以自行創建新的輸入法，或者將公開取得的輸入法資料表拷貝至資料目錄中，即可擁有諸如繁體[倉頡](../Page/倉頡輸入法.md "wikilink")、簡易、[大易](../Page/大易輸入法.md "wikilink")、簡體[拼音](../Page/汉语拼音输入法.md "wikilink")、五筆等等輸入法。

在文字輸出上，目前的模組可以做到即時的「打繁出簡」、「打簡出繁」、字根反查，以及全形文字輸出、歐語智慧引號等功能。

## 發展過程

  - 1.1.0 2016年10月20日
  - 1.0.11 2014年10月28日
  - 1.0.2 2012年11月9日
  - 1.0.0 2012年10月23日
  - 0.9.1 2010年8月8日
  - 0.8.0 2007年10月27日
  - 0.7.2 2006年6月25日
  - 0.7.1 2005年8月12日
  - 0.6.3 2005年1月19日
  - 0.6.2 2004年12月24日
  - 0.6.1 2004年12月19日
  - 0.6 2004年12月5日
  - 0.0.4 2004年11月13日
  - 0.0.2 2004年10月31日
  - 0.0.1 2004年10月25日

## 參考資料

  - [我的第一本蘋果書--Mac OS X
    Mavericks](https://books.google.com.tw/books?id=-8gVBQAAQBAJ&pg=PR80&dq=%E9%A6%99%E8%8D%89%E8%BC%B8%E5%85%A5%E6%B3%95&hl=zh-TW&sa=X&ei=L-ExVaCdHoG_mwXw74DgAw&ved=0CCUQ6AEwAQ#v=onepage&q=%E9%A6%99%E8%8D%89%E8%BC%B8%E5%85%A5%E6%B3%95&f=false)

## 外部連結

  - [OpenVanilla 官方網站](http://openvanilla.org/)
  - [OpenVanilla 討論區](http://groups.google.com/group/openvanilla)（Google
    Group）
  - [OpenVanilla
    開發專案網頁](https://github.com/openvanilla/openvanilla/)（GitHub）
  - [論文：OpenVanilla – A Non-Intrusive Plug-In Framework of Text
    Services](http://arxiv.org/pdf/cs/0508041)
  - [新酷音輸入法](http://chewing.csie.net/)
  - [ChewingOSX](http://chewingosx.sourceforge.net/)
      - [SpaceChewingOSX](http://rt.openfoundry.org/Foundry/Project/Wiki/100/index.html)
  - [香草輸入法](http://rt.openfoundry.org/Foundry/Project/Wiki/170/index.html)

[Category:中文輸入法平台](../Category/中文輸入法平台.md "wikilink")
[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:MacOS軟體](../Category/MacOS軟體.md "wikilink")