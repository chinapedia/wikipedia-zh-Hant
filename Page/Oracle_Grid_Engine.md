**Sun Grid
Engine**是[昇陽電腦所研發的](../Page/昇陽.md "wikilink")[自由軟件和](../Page/自由軟件.md "wikilink")[開放原始碼軟件](../Page/開放原始碼軟件.md "wikilink")[計算機集群軟件](../Page/計算機集群.md "wikilink")。

## 作業系統支援

[Xml-qstat.jpg](https://zh.wikipedia.org/wiki/File:Xml-qstat.jpg "fig:Xml-qstat.jpg")

Sun Grid Engine支援以下的[操作系统](../Page/操作系统.md "wikilink")：

  - [AIX](../Page/AIX.md "wikilink")
  - [BSD](../Page/BSD.md "wikilink") -
    [FreeBSD](../Page/FreeBSD.md "wikilink")，[NetBSD](../Page/NetBSD.md "wikilink")，[OpenBSD](../Page/OpenBSD.md "wikilink")
  - [HP-UX](../Page/HP-UX.md "wikilink")
  - [IRIX](../Page/IRIX.md "wikilink")
  - [Linux](../Page/Linux.md "wikilink")
  - [Mac OS X](../Page/Mac_OS_X.md "wikilink")
  - [Solaris](../Page/Solaris.md "wikilink")
  - [SUPER-UX](../Page/SUPER-UX.md "wikilink")
  - [Tru64](../Page/Tru64.md "wikilink")
  - [Windows](../Page/Microsoft_Windows.md "wikilink")
  - IBM [大型計算機](../Page/大型計算機.md "wikilink") (zLinux)

## 開源與商業版本

在Grid Engine 6.0之後，商業版本與開源版本沒有功能上的差別。\[1\]

然而，Grid Engine 6.2 商業版本附加額外的外掛軟件，如*SGE Inspect*、*Grid
Engine-[Hadoop](../Page/Hadoop.md "wikilink")*整合；不過這些額外的軟件也是開源的，但使用者需要由[源代碼自行編譯](../Page/源代碼.md "wikilink")。\[2\]

## 知名用戶

[GridSchedulerQmon.png](https://zh.wikipedia.org/wiki/File:GridSchedulerQmon.png "fig:GridSchedulerQmon.png")

  - 日本的 TSUBAME 超級計算機 - TSUBAME 曾是亞洲最強大的超級計算機 \[3\]
  - 美國[德州大學的](../Page/德州大學.md "wikilink") Ranger 超級計算機 - Ranger 有 3,936
    個節點，每節點有 16 個 Opteron [微處理器核心](../Page/微處理器.md "wikilink")

## 参考文献

## 外部链接

  - [Open Grid Scheduler
    開放原始碼計劃主頁](http://gridscheduler.sourceforge.net/)
    － 開源社群的Grid Engine計劃
  - [Oracle的 Grid
    Engine主頁](https://web.archive.org/web/20100811015359/http://www.oracle.com/us/products/tools/oracle-grid-engine-075549.html)
  - [Grid Engine
    中文技術文檔](https://web.archive.org/web/20060527153505/http://docs.sun.com/app/docs/coll/1192.1?l=zh)

{{-}}

[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:免費軟件](../Category/免費軟件.md "wikilink")
[Category:昇陽電腦](../Category/昇陽電腦.md "wikilink")
[Category:跨平台軟體](../Category/跨平台軟體.md "wikilink")

1.  [Understanding the differences between Grid Engine 5.3, 6.0 and Sun
    N1 Grid Engine 6
    (N1GE 6)](http://bioteam.net/dag/gridengine-6-features.html)
2.  [SGE 6.2u5
    released](http://gridengine.info/2009/12/22/sge-6-2u5-released)
3.  [TSUBAME超級計算機淺析](http://www.ssc.net.cn/paper/paper_pdf/23.pdf)