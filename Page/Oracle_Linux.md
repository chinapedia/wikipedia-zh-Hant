**Oracle Linux**，正式名稱為**甲骨文企業Linux**（Oracle Enterprise
Linux），為[甲骨文公司](../Page/甲骨文公司.md "wikilink")（）所發行的企業級[Linux](../Page/Linux.md "wikilink")，其建基於[Red
Hat Enterprise
Linux](../Page/Red_Hat_Enterprise_Linux.md "wikilink")。首次發佈於2006年10月25日。其免費供應於大眾下載、使用與發布，並為Red
Hat版Linux用戶提供有償支援。 Oracle Linux有两种内核：兼容Red Hat Enterprise
Linux的内核（使用RHEL源代码编译）和Oracle自己的Unbreakable
Enterprise内核。Oracle声明Unbreakable
Enterprise内核兼容RHEL，Oracle中间件和经过RHEL认证的第三方应用程序可以不经过修改的在Unbreakable
Enterprise内核上运行。

## 分支

[Oracle_Unbreakable_Linux.jpg](https://zh.wikipedia.org/wiki/File:Oracle_Unbreakable_Linux.jpg "fig:Oracle_Unbreakable_Linux.jpg")
Oracle Linux提供兩個內核版本：

  - 紅帽相容內核（Red Hat Compatible
    Kernel）：與[紅帽企業Linux](../Page/Red_Hat_Enterprise_Linux.md "wikilink")（RHEL）版相容
  - Unbreakable Enterprise
    Kernel（UEK）：基於Linux-2.6系列內核版本，加上甲骨文公司對於OLTP，InfiniBand，SSD硬碟存取等加強功能。

## 技术支持

甲骨文執行長[賴瑞·艾利森指出](../Page/賴瑞·艾利森.md "wikilink")，Unbreakable
Linux的用戶若要軟體更新支持，則每台伺服器收費99美元，而技術支援則是每台雙重伺服器收取399美元，更大型的系統則年付999美元。

## 參見

  - [Oracle VM](../Page/Oracle_VM.md "wikilink")

## 外部連結

  - [Oracle Unbreakable Linux下載網址](http://edelivery.oracle.com/linux)
  - [Oracle Linux
    中文網站](https://web.archive.org/web/20110415221149/http://www.oracle.com/cn/technologies/linux/index.html)

[Category:Linux發行版](../Category/Linux發行版.md "wikilink")
[Category:甲骨文公司軟體](../Category/甲骨文公司軟體.md "wikilink")