[Aparência_visual_do_'novo_orkut'.png](https://zh.wikipedia.org/wiki/File:Aparência_visual_do_'novo_orkut'.png "fig:Aparência_visual_do_'novo_orkut'.png")

**Orkut**（中文非正式名：我酷，偶酷）是[Google推出的](../Page/Google.md "wikilink")[社会性网络服务](../Page/社会性软件.md "wikilink")。Orkut是以创建它的Google工程师**Orkut
Buyukkokten**的名字命名的，通过这一服务，用户可以在互联网上建立一个虚拟社会关系网。目前大多數使用者來自[巴西及](../Page/巴西.md "wikilink")[印度](../Page/印度.md "wikilink")。2014年6月30日，Google宣布Orkut於同年9月30日關閉服務。\[1\]

## 历史

在2004年1月22日由Google工程师[Orkut
Buyukkokten创建](../Page/Orkut_Buyukkokten.md "wikilink")，2014年9月30日終止服務。

## 帳戶申請

擁有Orkut帳戶，必須首先擁有[Google帳戶](../Page/Google帳戶.md "wikilink")。Orkut的帳戶曾經也通過類似於過去[Gmail帳戶的形式](../Page/Gmail.md "wikilink")，由現有用戶通過[電子郵件發送加入邀請](../Page/電子郵件.md "wikilink")。收到邀請信的用戶，可以通過信中地址啟用自己的Orkut帳戶；後來註冊Orkut已經不再需要邀請。

## 與Gmail、Google帳戶的關係

Google打算讓[Google帳戶作為使用Google一些服務的通用帳戶](../Page/Google帳戶.md "wikilink")，而[Gmail帳戶在註冊時就自動成為一個Google帳戶](../Page/Gmail.md "wikilink")。在Orkut，預設的登錄凭证也是Google帳戶的用戶名和密碼，而新用戶將直接註冊Google帳戶。一旦Orkut帳戶和Google帳戶捆綁以后，Orkut的登入用戶名和密碼就不再出現。這一點類似於[網易](../Page/網易.md "wikilink")、[搜狐或](../Page/搜狐.md "wikilink")[微軟](../Page/微軟.md "wikilink")[Microsoft
Account的通行證](../Page/Microsoft_Account.md "wikilink")。

## 相关条目

  - [社会性软件](../Page/社会性软件.md "wikilink")
  - [Wallop](../Page/Wallop.md "wikilink")

## 参看

  - [Google产品列表](../Page/Google产品列表.md "wikilink")
  - [社会网络站点](../Page/社会网络站点.md "wikilink")

## 参考

  - Meneses, J. (2004).
    [Orkut.com：相对传统虚拟社区的在线交际新探索的反映](http://www.cibersociedad.net/congres2004/grups/fitxacom_publica.php?grup=65&id=516&idioma=en)

<!-- end list -->

  - Butalia, S. (2004).
    \[<http://www.foolonahill.com/mbasns.html‘开拓新兴在线社会网络组潜在市场>’\]

## 参考资料

<references />

[Category:2004年建立的网站](../Category/2004年建立的网站.md "wikilink")
[Category:2014年关闭的网站](../Category/2014年关闭的网站.md "wikilink")
[Category:Web 2.0](../Category/Web_2.0.md "wikilink")
[Category:已終止的Google服務](../Category/已終止的Google服務.md "wikilink")
[Category:电子邮件网站](../Category/电子邮件网站.md "wikilink")
[Category:社会性软件](../Category/社会性软件.md "wikilink")
[Category:社交網路服務](../Category/社交網路服務.md "wikilink")
[Category:Android软件](../Category/Android软件.md "wikilink")

1.