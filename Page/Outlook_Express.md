**Outlook
Express**，简称为**OE**，是[微软公司出品的一款](../Page/微软公司.md "wikilink")[电子邮件客户端](../Page/电子邮件客户端.md "wikilink")，也是一个基于[NNTP协议的](../Page/NNTP.md "wikilink")[Usenet客户端](../Page/Usenet客户端.md "wikilink")。[微软将这个软件与](../Page/微软.md "wikilink")[操作系统以及](../Page/操作系统.md "wikilink")[Internet
Explorer](../Page/Internet_Explorer.md "wikilink")[网页浏览器捆绑在一起](../Page/网页浏览器.md "wikilink")。同时，对于[苹果公司](../Page/苹果公司.md "wikilink")「经典」版的[麦金塔电脑提供该软件的免费下载](../Page/麦金塔.md "wikilink")（微软不对新版本的[Mac
OS X操作系统提供该软件](../Page/Mac_OS_X.md "wikilink")，在OS X上微软对应的软件是[Microsoft
Entourage](../Page/Microsoft_Entourage.md "wikilink")，Microsoft
Entourage是专有商用软件[Microsoft
Office套装的一部分](../Page/Microsoft_Office.md "wikilink")）。

Office软件内的**[Outlook](../Page/Outlook.md "wikilink")**与**Outlook
Express**是两个完全不同的软件平台，他们之间没有共用代码，但是这两个软件的设计理念是共通的。他们之间如此相似的名字使得很多人以为**Outlook
Express**是**Outlook**软件的「精简版」。不過Outlook Express並不像Outlook那樣可以和[Microsoft
Exchange
Server相互搭配而有](../Page/Microsoft_Exchange_Server.md "wikilink")[群組軟體的功能](../Page/群組軟體.md "wikilink")。Outlook
Express目前实际上一直是Internet
Explorer的一部分，将来似乎也不太可能存在剥离版的OE。微软承诺「将来的安全扩展」的结果将会是发布新版本的Internet
Explorer（当然就包括了OE）。这个新版的IE/OE将运行在新的「安全」操作平台－[Windows
Longhorn上](../Page/Windows_Longhorn.md "wikilink")。

在[Windows 95上就已经包括了名为Internet](../Page/Windows_95.md "wikilink") Mail
and News的软件，这是Outlook Express的雏形。Internet Mail and
News只處理纯文本邮件（没有[HTML邮件](../Page/HTML.md "wikilink")），所以该软件没有Outlook已知的安全漏洞。令人惊奇的，微软当年的这个软件没有提供备份地址簿这项基本功能—这让用户在使用中非常不方便。

[OESlash.jpg](https://zh.wikipedia.org/wiki/File:OESlash.jpg "fig:OESlash.jpg")
随着Outlook
Express产品的不断成熟，微软开始整合网络应用于浏览器与电邮客户端中的计划。使两者完全支持[脚本运行](../Page/程序脚本.md "wikilink")。无论如何，这项举动使得良性邮件／可信应用程序与远端网页的界线变得模糊起来。OE软件可以方便的执行[JavaScript脚本并在本地显示远端图片](../Page/JavaScript.md "wikilink")，但与此同时这也为之後爆发的安全和隐私危急埋下了祸根。

在Outlook与Outlook
Express的「欢迎邮件」中，微软承认新的HTML电邮可能存在安全风险。进一步的，微软还描述了它阻止安全问题发生的计划。Outlook
Express以及Internet
Explorer共享相同的安全区域—这是一个不可能也没有必要在其他类似产品上发现的功能。IE的安全区域默认情况下分为局域网、互联网、信任网点和限制站点四类。互联网类别是对不属于其他区域站点的默认分类。信任站点在默认情况下可以做任何事情而不必得到用户允许，这对管理员无值守更新非常有用。[AOL会把其](../Page/AOL.md "wikilink")[free.aol.com](http://free.aol.com)站点加入该区域以便用户在下载其网站上的服务时不会弹出一个是否信任[ActiveX证书的](../Page/ActiveX.md "wikilink")[对话框](../Page/对话框.md "wikilink")。因为这个对话框上使用的严厉措辞有可能把潜在的客户吓跑。安全区域如果像预期设计的那样工作就不会发生Internet
Explorer[骇客行为了](../Page/骇客.md "wikilink")。安全区域应当能够被用户所控制。

[OutlookExpressMacintosh.Png](https://zh.wikipedia.org/wiki/File:OutlookExpressMacintosh.Png "fig:OutlookExpressMacintosh.Png")版Outlook
Express介面\]\]
微软Microsoft为贯彻其计划还另外设计了一个限制区域。但事实上这个「限制」的安全区域并未被很好的限制。比如附件中的脚本有可能会自动执行（因为设计者试图让无害的邮件附件如图像能够自动执行以便在预览中查看，但该特性被恶意[利用](../Page/利用.md "wikilink")）。该漏洞最终被修复，在最新版本的OE中，文件名中字符必须在最後一个「.」号後才能成为[扩展名](../Page/扩展名.md "wikilink")—Windows[文件系统倚靠扩展名决定相应行为](../Page/文件系统.md "wikilink")。没打补丁的OE在打开或预览[电子邮件时有可能在用户不知情的情况下执行代码](../Page/电子邮件.md "wikilink")。目前许多广泛传播的病毒即是利用该弱点。参见Outlook和[可信计算条目了解微软公司对此作出的反应](../Page/可信计算.md "wikilink")。

Outlook
Express目前的使用率使其成为**实际上的**电邮客户端标准。并且。OE也是**实际上的**[蠕虫和](../Page/电脑蠕虫.md "wikilink")[病毒的首选传播目标](../Page/电脑病毒.md "wikilink")。OE的Macintosh版因为已经停止开发，所以较少受到攻击。

最後一点，微软已经停止继续开发Outlook
Express。无论如何，OE已知漏洞的补丁可能需要相比其他微软产品较长的时间才能发布（参见下方**外部链接**中Secunia网站连接）。这使得人们对该产品仍然有[安全担忧](../Page/计算机安全.md "wikilink")。

## 相關产品

  - [Windows Mail](../Page/Windows_Mail.md "wikilink")
  - [Windows Live](../Page/Windows_Live.md "wikilink")
  - [Windows Live Mail](../Page/Windows_Live_Mail.md "wikilink")

## 参考文献

## 外部連結

  - [Microsoft
    Outlook官方網頁](https://www.microsoft.com/zh-tw/outlook-com/?cb=v8ho)

  - [Outlook Express dbx file format by Arne
    Schloh](https://web.archive.org/web/20080112122726/http://oedbx.aroh.de/)
    – “DBX”的文件格式：文档和示例代码。
  - [UnDBX](http://outlookexpress-thunderbird-dbx.7xl.org)在线服务;
    开放源代码工具，Outlook
    Express的“DBX”的文件格式;提取，恢复和电子邮件的恢复删除;源文件和Windows二进制文件在:
    [sourceforge.net](http://sourceforge.net/projects/undbx)
  - [Outlook Express Help - Tips and
    Tricks](http://www.webdevelopersnotes.com/tips/internet/outlook_express_help.php)
    - 如何使用Outlook Express，安装并配置新的电子邮件帐户包括Gmail，备份邮件，邮件规则，电子邮件签名等。

## 参见

  - [电子邮件客户端列表](../Page/电子邮件客户端列表.md "wikilink")
  - [电子邮件客户端比较](../Page/电子邮件客户端比较.md "wikilink")

{{-}}

[Category:微软软件](../Category/微软软件.md "wikilink")
[Category:电子邮件客户端](../Category/电子邮件客户端.md "wikilink")
[Category:Usenet客户端](../Category/Usenet客户端.md "wikilink")