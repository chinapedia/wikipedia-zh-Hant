**P-3AEW\&C Blue
Sentinel**是由[洛克希德公司](../Page/洛克希德.md "wikilink")（LockHeed）以[P-3獵戶（Orion）B式巡邏機為基礎改裝而來的](../Page/P-3獵戶式巡邏機.md "wikilink")[空中預警機](../Page/空中預警機.md "wikilink")（Airborne
Early Warning and Control,
AEW\&C）。[美國國土安全部](../Page/美國國土安全部.md "wikilink")（United
States Department of Homeland
Security，DHS）現有8架，主要用於打擊[毒品走私和國土安全巡邏](../Page/毒品走私.md "wikilink")。

## 發展歷程

1988年6月77日，[美國海關及邊境署](../Page/美國海關及邊境保衛局.md "wikilink")（U.S. Customs and
Border
Protection，CBP，現已併入[美國國土安全部](../Page/美國國土安全部.md "wikilink")）接收第一架[洛克希德公司移交的P](../Page/洛克希德.md "wikilink")-3AEW\&C空中預警管制機，該機被命名為藍色哨兵（Blue
Sentinel），同年7月正式服役。

## 感測系統

本機裝置一組[奇異公司](../Page/奇異公司.md "wikilink")（General
Electric）AN/APS-125超高頻雷達（與[E-2鷹眼（Hawkeye）式空中預警機使用者相同](../Page/E-2空中預警機.md "wikilink")），偵測範圍直徑達350公里。搭配新開發的雷達信號處理處理軟體，可以掌握2,000個雷達追蹤航跡及1,200個敵友識別追蹤航跡，分辨絕對或相關追蹤點的位置、繪出各點航程週期、計算出攔截的方向及位置，透過通訊系統指揮其他單位進行攔截。

運算單元採用AYK-14軍用電腦，內含[漢威聯合公司](../Page/霍尼韋爾.md "wikilink")（Honeywell）1601M陣列高速處理機（Array
Processor）。終端輸出部分配有2具桑德斯（Sanders）公司的Mingraphics終端機組，每具終端機均有1個彩色觸摸式螢幕、1個鍵盤及1個控制球，無論用手觸或搖桿操作，都可以直接進行指令下達。

其次，P-3AEW\&C還配備1具[賀塞亭公司](../Page/賀塞亭公司.md "wikilink")（Hazeltine）AN/TPX-54敵友識別異頻雷達收發機（IFF
Transponder Interrogator）。通訊裝備包括2具AN/ARC-182 VHF／UHF無線電及2具Wulfsberg
VHF／UHF／FM無線電。

## 外銷

洛克希德公司曾在1988年5月遊說[澳洲皇家空軍](../Page/澳大利亞皇家空軍.md "wikilink")，採購4架全新機體的P-3AEW\&C，與服役中的[P-3獵戶式巡邏機共用相同的後勤支援系統](../Page/P-3獵戶式巡邏機.md "wikilink")，並作為[澳洲北部海岸緝私及指揮管制](../Page/澳洲.md "wikilink")[F/A-18戰鬥機作戰](../Page/F/A-18黃蜂式戰鬥攻擊機.md "wikilink")。

也有部分飛機銷往加拿大，由加拿大航空司令部管轄，但是從事海上的偵察任務。

## 外部連結

  - [P-3系列衍生型簡介](http://home.planet.nl/~p3orion/variants.html)
  - [P-3AEW\&C衛星照片](http://wikimapia.org/2435489/P_3AEW_C_Slick)
  - [P-3AEW\&C照片](http://www.planepictures.net/netshow.php?id=375906)

[Category:洛克希德飞机](../Category/洛克希德飞机.md "wikilink")
[Category:空中預警和控制機](../Category/空中預警和控制機.md "wikilink")