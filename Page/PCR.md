**PCR**可能指：

**政党：**

  - [革命共產黨](../Page/革命共產黨.md "wikilink")（*Partido Comunista
    Revolucionario*）
  - [留尼汪共产党](../Page/留尼汪共产党.md "wikilink")（*Parti Communiste
    Réunionnais*）
  - [罗马尼亚共产党](../Page/罗马尼亚共产党.md "wikilink")（*Partidul Communist Român*）

**其他：**

  - [超異域公主連結 Re:Dive](../Page/超異域公主連結_Re:Dive.md "wikilink") （Princess
    Connect\! Re:Dive）

  - , an artist

  - based in Beit Sahour

  - , a radar system exploiting broadcast transmitters

  - , the name of the operating system that runs on Nortel Multiservice
    switches in carrier configurations

  - , an Australian V8 Supercar motor racing team

  - , a source traffic characteristic on ATM networks

  - , a technique used in electrochemistry

  - [Platform Configuration
    Register](../Page/Platform_Configuration_Register.md "wikilink"), a
    Trusted Platform Module register storing platform configuration
    measurements

  - [聚合酶链式反应](../Page/聚合酶链式反应.md "wikilink")（）

  - [Post-conviction
    relief](../Page/Post-conviction_relief.md "wikilink")

  - , a book series designed to teach students Chinese

  - [Pressure Conductive
    Rubber](../Page/Pressure_Conductive_Rubber.md "wikilink")

  - [Primary Charge
    Roller](../Page/Primary_Charge_Roller.md "wikilink"), used in laser
    printers

  - , a statistical technique

  - or Preventative cranial radiotherapy, radiation therapy given to a
    person’s head to prevent or delay the spread of a cancer to the
    brain. Also referred to as "Prophylactic cranial irradiation"

  - [过程能力](../Page/过程能力.md "wikilink") ratio

  - [节目时钟参考](../Page/M2T.md "wikilink"), used in MPEG transport streams

  - , a satellite receiver

  - [Post conviction
    relief](../Page/Post_conviction_relief.md "wikilink")

  - , a technical analysis indicator