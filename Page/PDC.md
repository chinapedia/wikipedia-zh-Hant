**PDC** (**Personal Digital Cellular**)
是一種由[日本開發及使用的](../Page/日本.md "wikilink")[2G](../Page/2G.md "wikilink")[流動電話通訊標準](../Page/流動電話.md "wikilink")。

與[D-AMPS及](../Page/D-AMPS.md "wikilink")[GSM相似](../Page/GSM.md "wikilink")，PDC採用[TDMA技術](../Page/TDMA.md "wikilink")。標準由RCR（其後變成[ARIB](../Page/ARIB.md "wikilink")）在1991年4月制定。而[NTT
DoCoMo在](../Page/NTT_DoCoMo.md "wikilink")1993年3月推出其數碼[MOVA服務](../Page/MOVA.md "wikilink")。PDC採用25
kHz載波、3個時間格、pi/4-[DQPSK](../Page/DQPSK.md "wikilink") 編碼及低速率11.2 kbit/s
及 5.6 kbit/s（半速率）話音[编解码器](../Page/编解码器.md "wikilink")。

PDC使用 800 MHz（下傳 810-888 MHz，上傳 893-958 MHz），及 1.5 GHz（下傳 1477-1501
MHz，上傳 1429-1453 MHz）頻譜。空中介面(air interface) 訂為RCR STD-27，核心網絡地圖（core
network MAP）為
JJ-70.10。[NEC與](../Page/NEC.md "wikilink")[易利信是主要網絡設備製造商](../Page/易利信.md "wikilink")。

提供的服務包括話音（全速及本速），增值服務包括[來電等候](../Page/來電等候.md "wikilink")、[留言信箱](../Page/留言信箱.md "wikilink")、[三人會議](../Page/三人會議.md "wikilink")、[來電轉駁等](../Page/來電轉駁.md "wikilink")，數據服務（最高為
9.6 kbit/s
[CSD](../Page/電路交換數據.md "wikilink")），及封包轉換無線數據（packet-switched
wireless data，最高為28.8 kbit/s [PDC-P](../Page/PDC-P.md "wikilink")）。

與[GSM相比](../Page/GSM.md "wikilink")，PDC的較弱廣播強度讓生產商造出較細小的手機及使用較輕的[電池](../Page/電池.md "wikilink")，但話音質素則低於標準，而維持網絡連接能力亦較為遜色，特別是在密閉環境如[電梯內](../Page/電梯.md "wikilink")。

PDC最高峰時期曾有接近8000萬使用者，2005年12月使用者數字為4585.6萬，2007年3月底的使用者為2621萬人（約佔所有行動電話使用者的27.1%）。逐漸被[3G技術如](../Page/3G.md "wikilink")[CDMA2000或](../Page/CDMA2000.md "wikilink")[WCDMA淘汰](../Page/WCDMA.md "wikilink")。

[Category:移动通信标准](../Category/移动通信标准.md "wikilink")