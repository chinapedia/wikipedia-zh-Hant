**PDF軟體列表**列出[PDF管理軟體](../Page/PDF.md "wikilink")，包含用於製作、閱讀、編輯、開發的軟體等。

## Amiga OS

### 转换器

  - [Antiword](../Page/Antiword.md "wikilink")：一款自由的用于多个操作系统的[Microsoft
    Office Word阅读器](../Page/Microsoft_Office_Word.md "wikilink")；出自Word
    2、6、7、97、2000、2002和2003的转换器的二进制文件用于纯文本或PostScript；适用于[AmigaOS
    4](../Page/AmigaOS_4.md "wikilink")、[MorphOS](../Page/MorphOS.md "wikilink")、[AROS](../Page/AROS.md "wikilink")
    x86
  - [dvipdfm](../Page/dvipdfm.md "wikilink")：带zlib支持的从DVI到PDF的转换器

### 查看器

  - [Xpdf一款跨平台PDF文档阅读器](../Page/Xpdf.md "wikilink")；Amiga版本使用X11引擎[Cygnix](../Page/Cygnix.md "wikilink")。

## Linux/UNIX

### 有注释功能的软件

  - [Okular](../Page/Okular.md "wikilink")：支持一系列注释类型。注释与未修改的PDF分开存储，或者（从0.15版开始与[Poppler](../Page/Poppler.md "wikilink")
    0.20\[1\]）以标准PDF注释存储于文档中。
  - [Xournal](../Page/Xournal.md "wikilink")：允许通过徒手绘制、形状识别、多色及文本字体工具混合及自定义注解。此外有用于直线、高亮及下划线工具。注释可以复制、粘贴及简单的移动。注释也可以通过SCIM输入混合像藏文之类的文本注释与未修改的PDF分开存储，但注释的文档可导入到PDF。
  - [Evince](../Page/Evince.md "wikilink")：支持添加和移除（自3.14版开始\[2\]）基本的文字注解。\[3\]

### 转换器

[CUPS打印系统可以渲染任何页面到PDF文件](../Page/CUPS.md "wikilink")，因此任何含打印功能的UNIX/Linux程序可生成PDF文件。

  - [PageManager](../Page/PageManager.md "wikilink")：可以合并、分割、加水印、加图戳和处理PDF文件。
  - [Pdftk](../Page/Pdftk.md "wikilink")：可以合并、分割、加密、解密、加水印、加图戳和处理PDF文件。
  - [PDFSaM](../Page/PDFSaM.md "wikilink")：可以对PDF文档分割、合并和混合；加密与解密；设置元数据、许可及查看器选项；提取附件；添加封面和封底以及旋转页面。
  - [poppler-utils](../Page/Poppler.md "wikilink")：含从PDF提取图片（pdfimages）及转换PDF到其他格式（pdftohtml、pdftotext、pdftoppm）的命令行工具。
  - [ps2pdf](../Page/ps2pdf.md "wikilink")：转换PostScript文件为PDF。
  - [SWFTools](../Page/SWFTools.md "wikilink")：其pdf2swf组建可转换PDF为SWF。

### 生成器

  - [Xournal](../Page/Xournal.md "wikilink")：开源的创建PDF软件。
  - [Scribus](../Page/Scribus.md "wikilink")：用于专业页面输出的开源软件。

### 开发库

  - [Xpdf](../Page/Xpdf.md "wikilink")：用于查看和处理PDF文件的一款开源多后端库。有同名查看器绑定此库。

### 编辑器

  - [PDF Studio](../Page/PDF_Studio.md "wikilink")：用于查看和编辑PDF文档的专有软件
  - [Inkscape](../Page/Inkscape.md "wikilink")：技术上并非PDF编辑器，但可以逐页编辑
  - [PDFSaM](../Page/PDFSaM.md "wikilink")：用于处理、分割及合并PDF文档的自由（[GPLv2](../Page/GNU_GPL.md "wikilink")）GUI工具
  - [PDFtk](../Page/PDFtk.md "wikilink")：用于处理、分割、合并、加密和解密PDF文档的自由（[GPLv2](../Page/GNU_GPL.md "wikilink")）命令行工具
  - [gv](../Page/Ghostscript.md "wikilink")：Ghostscript的一部份
  - [PDFedit](../Page/PDFedit.md "wikilink")：PDF編輯器

### 查看器

  - [Adobe
    Reader](../Page/Adobe_Reader.md "wikilink")：[GNU](../Page/GNU.md "wikilink")/Linux的免费软件。
  - [Evince](../Page/Evince.md "wikilink")：[GNOME預設PDF檢視器](../Page/GNOME.md "wikilink")；取代[GPdf](../Page/GPdf.md "wikilink")。
  - [Okular](../Page/Okular.md "wikilink")：用于[KDE桌面环境](../Page/KDE_Software_Compilation_4.md "wikilink")；取代[KPDF](../Page/KPDF.md "wikilink")。
  - [Xpdf](../Page/Xpdf.md "wikilink")：[X
    Window](../Page/X_Window_System.md "wikilink") PDF檢視器
  - [Xournal](../Page/Xournal.md "wikilink")：使用[Poppler的GTK](../Page/Poppler.md "wikilink")2
    PDF查看器和加注软件。
  - [Firefox](../Page/Firefox.md "wikilink")：含PDF查看器
  - [Google Chrome](../Page/Google_Chrome.md "wikilink")：含PDF查看器
  - [Foxit Reader](../Page/Foxit_Reader.md "wikilink")：PDF檢視器
  - [PDF Cube](../Page/PDF_Cube.md "wikilink")：PDF檢視器
  - [PDF-XChange
    Viewer](../Page/PDF-XChange_Viewer.md "wikilink")：編輯器，檢視器。
  - [mupdf](../Page/mupdf.md "wikilink"):功能極簡，沒有滑鼠右鍵選單功能，只有[熱鍵操作](https://web.archive.org/web/20161126054022/http://mupdf.com/docs/manual)。效能極好，在開大檔案和翻頁時不會有卡頓的感覺。

## OS X

### 转换器

  - [deskUNPDF](../Page/Docudesk#Products.md "wikilink") for
    Mac：出自[Docudesk的专有软件](../Page/Docudesk.md "wikilink")，用于转换PDF文件到[Microsoft
    Office](../Page/Microsoft_Office.md "wikilink")、[OpenOffice.org](../Page/OpenOffice.org.md "wikilink")、图片及数据文件格式

### 编辑器

  - [Adobe Acrobat](../Page/Adobe_Acrobat.md "wikilink")：专有PDF套件
  - [PDF Studio](../Page/PDF_Studio.md "wikilink")：用于查看和编辑PDF文档的专有软件
  - [PDF
    Signer](../Page/PDF_Signer.md "wikilink")：专有软件；在PDF文档填入表格及嵌入图像签名
  - [PDFSaM](../Page/PDFSaM.md "wikilink")：用于处理、分割、合并、加密及解密PDF文档的自由（[GPLv2](../Page/GNU_GPL.md "wikilink")）命令行及GUI工具

### 生成器

  - [Mac OS X通过打印对话框原生创建PDF文档](../Page/Mac_OS_X.md "wikilink")

### 查看器

  - [Safari](../Page/Safari.md "wikilink")：这个内置的网页浏览器含有内置的阅读PDF文档的支持。
  - [Firefox](../Page/Firefox.md "wikilink")：含PDF查看器
  - [Google Chrome](../Page/Google_Chrome.md "wikilink")：含PDF查看器
  - [Preview](../Page/Preview_\(Mac_OS\).md "wikilink")：[Mac OS
    X默认的PDF查看器](../Page/Mac_OS_X.md "wikilink")（[Mac OS X
    v10.5及更高版本还可以旋转](../Page/Mac_OS_X_v10.5.md "wikilink")、重排、注释和删除页面。还可以合并文件、从已有文件创建新文件，并在已有文件之间转移页面）。
  - [Adobe Reader](../Page/Adobe_Reader.md "wikilink")：[Adobe
    Systems开发的阅读器同样支持Macintosh](../Page/Adobe_Systems.md "wikilink")，可以用Safari插件
  - [Skim](../Page/Skim_\(software\).md "wikilink")：一款用于Mac OS
    X的开源（BSD许可协议）PDF阅读器和注解软件

## Microsoft Windows

### 转换器

  - [Adobe
    Acrobat](../Page/Adobe_Acrobat.md "wikilink")：可转换文件到PDF或从PDF转换到其他格式\[4\]
  - [福昕PDF转Word](../Page/福昕PDF转Word.md "wikilink")：付费PDF转Word转换器，可批量转换PDF到Word（.doc）格式
  - [ABBYY
    FineReader](../Page/ABBYY_FineReader.md "wikilink")：商业PDF转换器，转换PDF到Word（.doc）、Excel（.xls）、PowerPoint（ppt）及更多格式
  - [deskUNPDF](../Page/Docudesk.md "wikilink")：PDF转换器，转换PDF到Word（.doc,
    docx）、Excel（.xls）、（.csv）、（.txt）及更多格式
  - [Gaaiho
    PDF](../Page/Gaaiho_PDF.md "wikilink")：商業軟體，中文品牌名稱為「文電通」，可批次雙向轉換PDF為其他格式(.doc,
    .excel, .ppt)及更多格式
  - [GSview](../Page/GSview.md "wikilink")：File:Convert菜单项转换任意顺序的PDF页面为有序的从位图到TIFF的多种格式的图片，分辨率72到204 × 98（[开源软件](../Page/开源.md "wikilink")）
  - [gDoc
    Fusion](../Page/Global_Graphics#Information_about_gDoc_Fusion.md "wikilink")：专有软件，共享软件；PDF查看、编辑，转换文档为PDF、[XPS或](../Page/开放XML纸张规范.md "wikilink")[DOC](../Page/DOC.md "wikilink")；30天后免费版给文档插入水印。
  - [OmniPage](../Page/OmniPage.md "wikilink")：在PDF及其他文件格式之间转换，带有多个选项
  - [PageManager](../Page/PageManager.md "wikilink")：商业PDF转换器，转换PDF到Word（.doc）、Excel（.xls）、PowerPoint（ppt）及更多格式
  - [PDF-XChange](../Page/PDF-XChange.md "wikilink")：PDF
    Tools及PDF-XChange print
    driver可以转换多种格式为PDF。打印驱动的精简版可以向非商业领域（家庭及学术）免费使用，但会在文档中加水印
  - [Qiqqa](../Page/Qiqqa.md "wikilink")：转换Microsoft Word文档及网页为PDF。
  - [SaveasPDFandXPS](../Page/SaveasPDFandXPS.md "wikilink")：Microsoft
    Office 2007的免费加载项；转换标准Microsoft
    Office格式（如Word、Excel、Powerpoint）为PDF。
  - [SWFTools](../Page/SWFTools.md "wikilink")：pdf2swf组件转换PDF为SWF—带GUI封装的命令行工具

### 生成器

| 名称                                                                                   | 类型及许可            | 描述                                                                                                                                                                                                                                                             |
| ------------------------------------------------------------------------------------ | ---------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Bullzip PDF Printer](../Page/Bullzip_PDF_Printer.md "wikilink")                     | 免费软件             | 虚拟打印机，要求[Ghostscript和PDFPowerTool](../Page/Ghostscript.md "wikilink")。                                                                                                                                                                                         |
| [CC PDF Converter](../Page/CC_PDF_Converter.md "wikilink")                           | GNU GPL          | 从任意软件创建嵌入[创作共用协议的PDF](../Page/创作共用.md "wikilink")。（虚拟打印机）                                                                                                                                                                                                      |
| [CutePDF](../Page/CutePDF.md "wikilink")                                             | 免费软件与专业版         | 虚拟打印机，并带有专有PDF编辑器。会安装Ask Toolbar及Hotspot shield。                                                                                                                                                                                                               |
| [deskPDF](../Page/Docudesk#Products.md "wikilink")                                   | 独立软件             | 客户端及终端服务器都可用。                                                                                                                                                                                                                                                  |
| [doPDF](../Page/doPDF.md "wikilink")                                                 | 免费独立软件           | 虚拟打印机，不用[Ghostscript](../Page/Ghostscript.md "wikilink")。                                                                                                                                                                                                      |
| [eCopy PaperWorks](../Page/Ecopy#Products.md "wikilink")                             | 专有软件             |                                                                                                                                                                                                                                                                |
| [福昕PDF编辑器](../Page/福昕PDF编辑器.md "wikilink")                                           | 商业软件             |                                                                                                                                                                                                                                                                |
| [Gaaiho PDF Driver](../Page/Gaaiho_PDF_Driver.md "wikilink")                         | 商业软件             | 企業級雙向批次轉換器，中文名稱為「文電通」。虚拟打印机，提供[OCR功能](../Page/OCR.md "wikilink")。                                                                                                                                                                                              |
| [gDoc Creator](../Page/Global_Graphics#Information_about_gDoc_Creator.md "wikilink") | 共享软件             | 企业级的生成器，用以创建、复查、编辑、共享或压缩[PDF与](../Page/PDF.md "wikilink")[XPS文档](../Page/开放XML纸张规范.md "wikilink")。30天后在免费版给文档插入水印。                                                                                                                                             |
| [HelpNDoc](../Page/HelpNDoc.md "wikilink")                                           | 免费软件与专业版         | 带原生PDF输出的手动与文档创作工具。对个人使用免费。                                                                                                                                                                                                                                    |
| [Nitro PDF Reader](../Page/Nitro_PDF#Nitro_Reader.md "wikilink")                     | 免费阅读器；商业完整版可用    | 就像[Adobe Acrobat](../Page/Adobe_Acrobat.md "wikilink")，[Nitro PDF Pro的阅读器是免费的](../Page/Nitro_PDF.md "wikilink")，但不同于Adobe Reader，Nitro的阅读器允许创建PDF（通过虚拟打印机、在阅读器输入指定文件名，或者向Nitro PDF Reader的Windows桌面图标拖入文件）；无需[Ghostscript](../Page/Ghostscript.md "wikilink")。 |
| [NovaPDF](../Page/NovaPDF.md "wikilink")                                             | 商业软件             | 虚拟打印机                                                                                                                                                                                                                                                          |
| [PagePlus](../Page/PagePlus.md "wikilink")                                           | 专有软件             | 桌面出版套件，含PDF查看、编辑和创建特性                                                                                                                                                                                                                                          |
| [PaperPort](../Page/PaperPort.md "wikilink")                                         | 商业软件             | 桌面出版套件，含PDF查看、编辑和创建特性                                                                                                                                                                                                                                          |
| [PDFCreator](../Page/PDFCreator.md "wikilink")                                       | FairPlay、GNU GPL | Windows的虚拟打印机，使用称为FairPlay的自定义许可。使用[Ghostscript](../Page/Ghostscript.md "wikilink") GPL并包含[广告软件直至v](../Page/广告软件.md "wikilink")1.7.3。现采用.NET Framework 4。                                                                                                      |
| [PDF-XChange](../Page/PDF-XChange.md "wikilink")                                     | 免费精简版            | PDF Tools可从许多输入来源（图片、扫描等）创建PDF。PDF-XChange打印驱动可直接打印成PDF。打印驱动的精简版可以向非商业领域（家庭及学术）免费使用。                                                                                                                                                                           |
| [PrimoPDF](../Page/PrimoPDF.md "wikilink")                                           | 免费独立软件           | 虚拟打印机，用于Microsoft .NET Framework并使用Ghostscript及[RedMon](../Page/RedMon.md "wikilink")。连接www.primopdf.com。包含Open Candy[广告软件](../Page/广告软件.md "wikilink")。                                                                                                       |
|                                                                                      |                  |                                                                                                                                                                                                                                                                |
| [SaveasPDFandXPS](../Page/SaveasPDFandXPS.md "wikilink")                             | 免费软件             | Microsoft Office 2007的加载项                                                                                                                                                                                                                                      |
| [Solid PDF Creator](../Page/Solid_PDF_Creator.md "wikilink")                         | 专有软件             |                                                                                                                                                                                                                                                                |
| [Xara Photo & Graphic Designer](../Page/Xara_Photo_&_Graphic_Designer.md "wikilink") | 免费软件             | 具有PDF创建，加上导入、输出和有限的编辑的图形设计软件                                                                                                                                                                                                                                   |

### 开发库

  - [HTML2PDF-X](../Page/HTML2PDF-X.md "wikilink")：从导入的HTML生成PDF的专有软件库。

### 编辑器

  - [Adobe Acrobat](../Page/Adobe_Acrobat.md "wikilink")：[Adobe
    Systems的专有PDF创作套件](../Page/Adobe_Systems.md "wikilink")
  - [Adobe Photoshop](../Page/Adobe_Photoshop.md "wikilink")：[Adobe
    Systems的图形设计软件和图像编辑器](../Page/Adobe_Systems.md "wikilink")
  - [福昕PDF编辑器](../Page/福昕PDF编辑器.md "wikilink")：PDF文档全生命周期管理解决方案，提供PDF编辑、页面管理、转换、[OCR文字识别等功能](../Page/OCR.md "wikilink")
  - [Gaaiho PDF
    Suite](../Page/Gaaiho_PDF_Suite.md "wikilink")：中文名稱為「文電通」，可以批次PDF轉換、編輯、審閱、分割、加密、註解，且支持[OCR](../Page/OCR.md "wikilink")
  - [gDoc
    Fusion](../Page/Global_Graphics#Information_about_gDoc_Fusion.md "wikilink")：用以创建、复查、编辑、共享或压缩PDF及XPS文档专有/共享桌面软件。30天后免费版给文档插入水印。
  - [IceCream Split &
    Merge](../Page/IceCream_Split_&_Merge.md "wikilink")：要求多阵列模式以使用户分割及合并PDF文档，包含密码保护及证书加密文件。
  - [LibreOffice](../Page/LibreOffice.md "wikilink")：用于处理、分割、合并、加密及解密PDF文件的自由（[MPLv2](../Page/MPL.md "wikilink")）GUI工具
  - [Nitro PDF
    Pro](../Page/Nitro_PDF.md "wikilink")：专有及商用PDF生成器/编辑器，带有[Microsoft
    Office的界面风格](../Page/Microsoft_Office.md "wikilink")；复制Adobe
    Acrobat完整商业版的多数或全部特性；需要单独的免费阅读器（同样支持PDF创建、注释、协作和签名）；不额外要求[Ghostscript](../Page/Ghostscript.md "wikilink")。
  - [PDF Studio](../Page/PDF_Studio.md "wikilink")：用于查看和编辑PDF文档的专有软件。
  - [pdftk](../Page/pdftk.md "wikilink")：用于处理、编辑和转换文档的自由（GPL）命令行工具；支持用FDF/XFDF数据填充PDF表格
  - [PDFSaM](../Page/PDFSaM.md "wikilink")：自由（[GPLv2](../Page/GNU_GPL.md "wikilink")）命令行及GUI工具，用于用于处理、分割、合并、加密及解密PDF文件
  - [PDF-XChange
    Viewer](../Page/PDF-XChange_Viewer.md "wikilink")：免费PDF阅读器、标注器、编辑器（简版）及转换器（对非商业使用免费）。
  - [Solid PDF
    Tools](../Page/Solid_PDF_Tools.md "wikilink")：转换PDF为可以编辑的文档并从多个文件来源创建PDF。
  - [Microsoft Word](../Page/Microsoft_Word.md "wikilink")
    2013：专有桌面软件，可编辑PDF文件。

### 阅读器

  - [Adobe
    Reader](../Page/Adobe_Reader.md "wikilink")：Adobe的PDF阅读器仅对个人使用免费。对于商业使用，Adobe
    Reader要付费。例如，如果你的软件为了打印之类的事情使用Adobe
    Reader，你和你的客户在取得商业产品资格的情况下要给Adobe
    Reader付费\[5\]\[6\]
  - [BePDF](../Page/BePDF.md "wikilink")：适用于Haiku、BeOS和magnussoft
    ZETA的免费PDF阅读器。
  - [Evince](../Page/Evince.md "wikilink")：自由（GPL）、[开源PDF阅读器](../Page/开源.md "wikilink")。GNOME桌面环境的一部分。Windows移植版自2.28版可用。
  - [Foxit
    Reader](../Page/Foxit_Reader.md "wikilink")：专有/免费PDF阅读器，支持FDF导入/导出、保存已填充表单；其他扩展功能可通过付费版插件使用。
  - [Gaaiho PDF
    Reader](../Page/Gaaiho_PDF_Reader.md "wikilink")：免費PDF閱讀器，支持列印、新增書籤、加入註解或填寫表單等功能。內含有「AddPDF.com」的連結，可提供免費線上PDF轉換服務。
  - [gDoc
    Fusion](../Page/Global_Graphics#Information_about_gDoc_Fusion.md "wikilink")：查看PDF、XPS、Microsoft
    Word文档、Microsoft Excel电子表格、Microsoft
    PowerPoint幻灯片或图片的专有/共享软件。共享版本在30后加上水印。
  - [GSview](../Page/GSview.md "wikilink")：Windows的[开源软件及](../Page/开源软件.md "wikilink")[Ghostscript查看器](../Page/Ghostscript.md "wikilink")
  - [IceCream Split &
    Merge](../Page/IceCream_Split_&_Merge.md "wikilink")：要求多阵列模式以使用户分割及合并PDF文档，包含密码保护及证书加密文件。
  - [Javelin PDF
    Reader](../Page/Javelin_PDF_Reader.md "wikilink")：全功能PDF阅读器，以Drumlin的DRMX和DRMZ格式加密及解密PDF文件支持DRM。
  - [Nitro PDF
    Reader](../Page/Nitro_PDF#Nitro_Reader.md "wikilink")：免费PDF阅读器和生成器。支持三种模式（在阅读器输入指定文件名、向Nitro
    PDF
    Reader的Windows桌面图标拖入文件，或者点击“打印”使用虚拟打印机）创建PDF（不额外要求[Ghostscript](../Page/Ghostscript.md "wikilink")），保存已填入表单（AcroForms），文本输入，标记及签章（文档签名）。
  - [Nuance PDF
    Reader](../Page/Nuance_PDF_Reader.md "wikilink")：免费（带广告）PDF阅读器，支持保存已填入表单，部分支持XFA表单；可以通过Nuance网站转换PDF到MS
    Word或Excel文件。
  - [PDF-XChange
    Viewer](../Page/PDF-XChange_Viewer.md "wikilink")：含免费OCR的免费阅读器，支持FDF/XFDF输入/输出，保存已填入表单，扩展标记及导出为图片。
  - [Qiqqa](../Page/Qiqqa.md "wikilink")：免费PDF阅读器、索引器、标注器及注解器。支持OCR及导出PDF文本及图像。
  - [Sumatra
    PDF](../Page/Sumatra_PDF.md "wikilink")：自由（GPL）、[开源PDF阅读器](../Page/开源.md "wikilink")。还支持DjVu、XPS、CHM、Comic
    Book（CBZ及CBR）与eBook（EPUB及MOBI）格式。支持自动重新载入PDF文件。
  - [STDU
    Viewer](../Page/STDU_Viewer.md "wikilink")：对非商业使用免的PDF阅读器。还支持DjVu、Comic
    Book Archive（CBR或CBZ）、XPS、TIFF、TXT及图片格式。
  - [Windows
    Reader](http://apps.microsoft.com/windows/en-us/app/8a4ae377-a4ab-4260-9b80-f9382360e291)：PDF檢視器

## 行動裝置

  - [Adobe
    Reader](../Page/Adobe_Reader.md "wikilink")：[Symbian](../Page/Symbian.md "wikilink")、[Palm](../Page/Palm.md "wikilink")、[Windows
    Phone](../Page/Windows_Phone.md "wikilink")、[Android](../Page/Android.md "wikilink")
    PDF檢視器
  - [Foxit Reader](../Page/Foxit_Reader.md "wikilink")：[Windows
    CE](../Page/Windows_CE.md "wikilink")、[Windows
    Mobile](../Page/Windows_Mobile.md "wikilink")、[嵌入式Linux](../Page/嵌入式Linux.md "wikilink")、[Android](../Page/Android.md "wikilink")、[iPad](../Page/iPad.md "wikilink")
    PDF檢視器\[7\]
  - [Gaaiho PDF
    Reader](../Page/Gaaiho_PDF_Reader.md "wikilink")：[Android](../Page/Android.md "wikilink")、[iOS](../Page/iOS.md "wikilink")
    PDF檢視器
  - [iBooks](../Page/iBooks.md "wikilink")：[iOS](../Page/iOS.md "wikilink")（iPhone、iPad）PDF檢視器
  - 閱讀器：[HTC](../Page/HTC.md "wikilink") PDF檢視器
  - [Evince移植到](../Page/Evince.md "wikilink")[Maemo平台](../Page/Maemo.md "wikilink")
  - [Google Drive](../Page/Google_Drive.md "wikilink") app for
    Android：可查看PDF\[8\]
  - [Amazon Kindle](../Page/Amazon_Kindle.md "wikilink") app for
    Android：可查看PDF\[9\]
  - [Qiqqa](../Page/Qiqqa.md "wikilink")：Android上的PDF查看器和注解器

## 基于Web

### 转换器

  - [Cometdocs](../Page/Cometdocs.md "wikilink")：在线PDF转换
  - [AddPDF.com](../Page/AddPDF.com.md "wikilink")：中文名稱為「加加PDF」\[10\]，提供免費的線上PDF轉換服務，包括了PDF轉為Word、Word轉PDF等等。
  - [Zamzar](../Page/Zamzar.md "wikilink")：在线文档转换器

### 编辑器

  - [PDFescape](../Page/Pdfescape.md "wikilink")：含广告并付费支持的从浏览器查看、创建表单、填充表单、及编辑PDF文档的网络服务（要求启用JavaScript）
  - [PDFVue](../Page/Pdfvue.md "wikilink")：允许用户从浏览器查看PDF、注释和填充PDF表单免费web应用。会生成水印。

### 阅读器

  - [A.nnotate](../Page/A.nnotate.md "wikilink")：在浏览器中像HTML一样查看PDF文档的网络服务，含注释功能。
  - [DigiSigner](../Page/DigiSigner.md "wikilink")：免费在线查看PDF的java小程序，含有对PDF文档数字签名的附加功能。
  - [Docstoc](../Page/Docstoc.md "wikilink")：可以在线查看PDF文档的网络服务
  - [Issuu](../Page/Issuu.md "wikilink")：可以在线查看PDF文档的网络服务
  - [Google
    Docs](../Page/Google_Docs.md "wikilink")：可以在浏览器中像PNG一样查看PDF文档的网络服务
  - [Scribd](../Page/Scribd.md "wikilink")：可以在浏览器中像HTML5一样渲染PDF文档的网络服务
  - [Pdf.js](../Page/Pdf.js.md "wikilink")：基于Javascript的库，含查看器和浏览器插件。
  - [PDFTron
    Systems](../Page/PDFTron_Systems.md "wikilink")：网页查看器。用于自我寄存的web优化的PDF的一款免费JavaScript查看器及注解器。

## 跨平台

### 转换器

| 名称                                                               | 许可             | 平台                            | 描述                                                                              |
| ---------------------------------------------------------------- | -------------- | ----------------------------- | ------------------------------------------------------------------------------- |
| [GIMP](../Page/GIMP.md "wikilink")                               | GNU GPL        | Linux、Mac、Windows             | 转换PDF到[位图](../Page/位图.md "wikilink")                                            |
| [ImageMagick](../Page/Imagemagick.md "wikilink")                 | Apache\[11\]   | Linux、Mac、Windows             | 转换PDF到[位图](../Page/位图.md "wikilink")，反之亦然                                       |
| [LibreOffice](../Page/LibreOffice.md "wikilink")                 | MPL            | BSD、Linux、Mac、Windows（基于Java） | 从PDF导入（默认包含于扩展），导出为包含[PDF/A的PDF](../Page/PDF/A.md "wikilink")。                  |
| [OpenOffice.org](../Page/OpenOffice.org.md "wikilink")           | GNU LGPL       | Linux、Mac、Windows             | 从PDF导入（带有限制）（安装扩展之后），导出为包含[PDF/A的PDF](../Page/PDF/A.md "wikilink")。             |
| [PDFBox](../Page/Apache_PDFBox.md "wikilink")                    | Apache许可证      | Linux、Unix、Windows            | 转换PDF到其他格式（文本、图片、html……）                                                        |
| [Poppler-utils](../Page/Poppler.md "wikilink")                   | GNU GPL        | Linux、Unix、Windows            | 转换PDF到其他格式（文本、图片、html……）                                                        |
| [pstoedit](../Page/pstoedit.md "wikilink")                       | GNU GPL        | ?                             | 转换PostScript到（其他）[矢量图形文件格式](../Page/矢量图形.md "wikilink")                         |
| [QPDF](../Page/QPDF.md "wikilink")                               | Artistic许可证2.0 | Linux、Windows\[12\]           | 在PDF之间做保留结构、内容的转换                                                               |
| [pdf-parser](../Page/pdf-parser.md "wikilink")                   | 公有领域           | Python脚本                      | 提取和分析工具，处理损坏及恶意的PDF文档                                                           |
| [Solid Converter PDF](../Page/Solid_Converter_PDF.md "wikilink") | 专有             | Windows、Mac OS X              | PDF到Word、Excel、HTML和Text；支持密码、文本编辑和批量转换                                         |
| [SWFTools](../Page/SWFTools.md "wikilink")                       | GNU GPL        | Linux、Windows、Mac             | SWF转换及处理套件，包含独立的PDF到SWF转换器与Python gfx API（要求[Xpdf](../Page/Xpdf.md "wikilink")） |
| [Nitro PDF](../Page/Nitro_PDF.md "wikilink")                     | 专有             | Windows                       | 转换PDF到Word、Excel、PowerPoint、[位图](../Page/位图.md "wikilink")                      |
| [Able2extract](../Page/Able2extract.md "wikilink")               | 专有             | Windows、Mac OS X、Linux        | 转换PDF到Word、Excel、PowerPoint、Publisher、Open Office、AutoCAD、HTML、文本、图像            |
| [Mobipocket Creator](../Page/Mobipocket.md "wikilink")           | 专有             | Windows                       | 从PDF导入并创建HTML及MOBI                                                              |

### 编辑器

| 名称                                                         | 许可        | 平台                                         | 描述                                                                              |
| ---------------------------------------------------------- | --------- | ------------------------------------------ | ------------------------------------------------------------------------------- |
| [LibreOffice Draw](../Page/LibreOffice_Draw.md "wikilink") | MPL       | Windows、Mac OS、Linux                       | PDF查看和编辑，有许多Acrobat所需的特性。                                                       |
| [Foxit PDF Editor](../Page/Foxit_PDF_Editor.md "wikilink") | 专有        | Windows                                    | 文字处理器，诸如编辑、拼写检查、转换到ePub、HTML及RTF，导出表单及文章流                                       |
| [OpenOffice Draw](../Page/OpenOffice_Draw.md "wikilink")   | GNU LGPL  | Windows、Mac OS、Linux                       | 通过[OpenOffice.org软件或扩展导入PDF](../Page/OpenOffice.org.md "wikilink")              |
| [Inkscape](../Page/Inkscape.md "wikilink")                 | GNU GPL   | Windows、Mac OS、Linux                       | 打开、编辑和导出文档，但每次只处理一页（页面稍后可用PDF打印机合并）                                             |
| [PDFedit](../Page/PDFedit.md "wikilink")                   | GNU GPL   | Windows、Linux、BSD                          | 查看或编辑PDF文档内部结构以及合并文档的软件                                                         |
| [Pdftk](../Page/Pdftk.md "wikilink")                       | GNU GPL   | Windows、Mac OS、Linux、FreeBSD、Solaris       | 用于编辑和转换文档的命令行工具；支持用FDF/XFDF数据填充PDF表单。有GUI前端。                                    |
| [PDFSaM](../Page/PDFSaM.md "wikilink")                     | GNU GPLv2 | Windows、Mac OS、Linux、FreeBSD、Solaris       | 用于分割及合并PDF文档的、旋转及重排页面的GUI及命令行工具；支持加密/解密PDF文档、设置权限、添加封面及封底、提取附件、填充元数据            |
| [Nitro PDF](../Page/Nitro_PDF.md "wikilink")               | 专有        | Windows                                    | 富UI，允许合并PDF、转换到其他格式、编辑（添加图像、图形、文本等）                                             |
| [PDF Studio](../Page/PDF_Studio.md "wikilink")             | 专有        | Windows、Mac OS、Linux                       | 完整特性的PDF编辑器                                                                     |
| [Karbon](../Page/Karbon.md "wikilink")                     | LGPL及GPL  | Windows、Mac OS X、Linux、BSD、Solaris/Illumos | 导入如同布局一样带多页的PDF，导出为一页的PDF。完整标准矢量图形编辑器特性。                                        |
| [Serif PagePlus](../Page/PagePlus.md "wikilink")           | 商用        | Windows                                    | 桌面出版（DTP）应用，可以打开的编辑PDF文档。允许兼容保存为PDF 1.3、1.4、1.5和1.7。并且还支持PDF/X1、PDF/X1a及PDF/X-3 |
| [Soda PDF](../Page/Soda_PDF.md "wikilink")                 | 专有        | Windows, Mac OS X                          | 模块化的PDF软件                                                                       |

### 開發程式庫

<table>
<thead>
<tr class="header">
<th><p>名称</p></th>
<th><p>许可</p></th>
<th><p>描述</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/Adobe_PDF_Library.md" title="wikilink">Adobe PDF Library</a></p></td>
<td><p>专有许可</p></td>
<td><p>具有PDF编辑、查看、打印和文本提取支持的C++、.NET、Java API</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Formatting_Objects_Processor.md" title="wikilink">Formatting Objects Processor</a></p></td>
<td><p>Apache许可证</p></td>
<td><p>由<a href="../Page/XSL_Formatting_Objects.md" title="wikilink">XSL Formatting Objects及输出独立格式器驱动的开源打印格式器</a>；主要输出目标为PDF</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/GNU_PDF.md" title="wikilink">GNU PDF</a></p></td>
<td><p>GNU GPL</p></td>
<td><p>开发中的开源C语言库；完成时会提供实现PDF 1.7规范所有特性的功能。（由于Poppler的可用性，2011年10月6日，GNU PDF不再被FSF列为“高优先级计划”。[13]）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/iText.md" title="wikilink">iText</a></p></td>
<td><p><a href="../Page/Affero_General_Public_License.md" title="wikilink">AGPL</a>[14]</p></td>
<td><p>以<a href="../Page/Java.md" title="wikilink">Java</a>、C#及其他.NET语言创建及处理PDF、RTF、HTML文件的开源库</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/JasperReports.md" title="wikilink">JasperReports</a></p></td>
<td><p>GNU LGPL</p></td>
<td><p>屏幕显示，打印或转为PDF、HTML、Microsoft Excel、RTF、ODT、comma-separated values及XML文件的开源Java报告工具</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/JPedal.md" title="wikilink">JPedal</a></p></td>
<td><p>专有、GNU LGPL</p></td>
<td><p>查看、提取、打印PDF文件的Java开发库</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/libHaru.md" title="wikilink">libHaru</a></p></td>
<td><p>[15]</p></td>
<td><p>生成PDF文件的开源、跨平台C语言库</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Apache_PDFBox.md" title="wikilink">PDFBox</a></p></td>
<td><p>Apache许可证</p></td>
<td><p>创建、查看、提取、打印PDF文件的Java开发库</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/PDFTron_Systems.md" title="wikilink">PDFTron Systems</a></p></td>
<td><p>专有许可</p></td>
<td><p>创建、编辑、注解、优化及查看PDF文件的C、C++、C#、.NET、JAVA、Objective-C、Python、Ruby及PHP库。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/podofo.md" title="wikilink">podofo</a></p></td>
<td><p>GNU LGPL</p></td>
<td><p>阅读和编写PDF文件的开源C++库。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Poppler.md" title="wikilink">Poppler</a></p></td>
<td><p>GNU GPL[16]</p></td>
<td><p>取自<a href="../Page/Xpdf.md" title="wikilink">Xpdf的开源多后端C</a>++库；不绑定阅读器；包含pdftohtml转换器</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/PDFium.md" title="wikilink">PDFium</a></p></td>
<td><p>BSD</p></td>
<td><p>用于阅读PDF的库</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/TCPDF.md" title="wikilink">TCPDF</a></p></td>
<td><p>GNU LGPL</p></td>
<td><p>创建PDF文件的开源<a href="../Page/PHP.md" title="wikilink">PHP库</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/XEP.md" title="wikilink">XEP</a></p></td>
<td><p>专有</p></td>
<td><p>Java编写的<a href="../Page/XSL_Formatting_Objects.md" title="wikilink">XSL-FO输出引擎</a>；输出格式为PDF</p></td>
</tr>
</tbody>
</table>

### 生成器

| 名称                                                                    | 许可                                                                                                                                                      | 平台                                                    | 描述                                                                                                         |
| --------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------- | ---------------------------------------------------------------------------------------------------------- |
| [Adobe Acrobat](../Page/Adobe_Acrobat.md "wikilink")                  | 专有                                                                                                                                                      | ?                                                     | 桌面PDF创作套件                                                                                                  |
| [Adobe Illustrator](../Page/Adobe_Illustrator.md "wikilink")          | 专有                                                                                                                                                      | ?                                                     | Illustrator可以保存为PDF，并且可以识别和编辑文本和图形                                                                         |
| [FineReader](../Page/FineReader.md "wikilink")                        | 专有                                                                                                                                                      | ?                                                     | OCR工具；可以保存为PDF，并从PDF识别文本                                                                                   |
| [Ghostscript](../Page/Ghostscript.md "wikilink")                      | GNU GPL                                                                                                                                                 | Linux、Windows、Mac OS X、UNIX                           | 查看器、生成器、转换器；还支持PDF/X-3；为PdfCreator（直到v1.7.3，之后自v2.0.0起用.NET Framwork 4）WinPdf、BullzipPdf、CutePDF免费版及其他软件所用 |
| [Microsoft Office](../Page/Microsoft_Office.md "wikilink")            | 专有                                                                                                                                                      | Windows、Mac OS X                                      | 所有Microsoft Office产品从2007版开始允许保存为PDF                                                                       |
| [OpenOffice.org](../Page/OpenOffice.org.md "wikilink")                | GNU LGPL                                                                                                                                                | Linux、Windows、Mac                                     | 所有OpenOffice.org应用允许PDF输出；也支持PDF/A-1a；自从许多导入格式得到支持（如doc、docx、rtf、xls、ppt）这一转换也是可行的                         |
| [LibreOffice](../Page/LibreOffice.md "wikilink")                      | MPL                                                                                                                                                     | Linux、Windows、Mac                                     | 所有LibreOffice应用允许PDF输出；也支持PDF/A-1a；自从许多导入格式得到支持（如doc、docx、rtf、xls、ppt）这一转换也是可行的                            |
| [Serif PagePlus](../Page/PagePlus.md "wikilink")                      | 商用                                                                                                                                                      | Windows                                               | 桌面出版（DTP）应用。可以兼容性的存储为PDF 1.3、1.4、1.5和1.7，还支持PDF/X1、PDF/X1a和PDF/X-3                                         |
| [PageStream](../Page/PageStream.md "wikilink")                        | 专有                                                                                                                                                      | ?                                                     | 跨平台桌面出版应用，可以打开、编辑、导出                                                                                       |
| [Prince](../Page/Prince_XML.md "wikilink")                            | 商用                                                                                                                                                      | Linux、Mac、Windows、Solaris                             | 通过CSS的方式转换HTML、XML、SVG和MathML为PDF                                                                          |
| [Scribus](../Page/Scribus.md "wikilink")                              | GNU GPL                                                                                                                                                 | Linux/UNIX、Mac OS X、OS/2 Warp 4/eComStation及Windows桌面 | 跨平台桌面出版（DTP）应用；还支持PDF/X-3                                                                                  |
| [XEP](../Page/XEP.md "wikilink")                                      | 专有                                                                                                                                                      | BSD、Linux、Mac、Windows（基于Java）                         | 转换[XML及](../Page/XML.md "wikilink")[XSL-FO为PDF](../Page/XSL_Formatting_Objects.md "wikilink")              |
| [LaTeX](../Page/LaTeX.md "wikilink")、[TeX](../Page/TeX.md "wikilink") | [LaTeX Project Public License](../Page/LaTeX_Project_Public_License.md "wikilink")、[Permissive](../Page/Permissive_free_software_licence.md "wikilink") | ?                                                     | [撰写技术报告](../Page/写作.md "wikilink")、书籍、杂志及几乎任何出版类型的[标记语言及工具](../Page/标记语言.md "wikilink")                    |
| [LuaTeX](../Page/LuaTeX.md "wikilink")                                | GNU GPL                                                                                                                                                 | ?                                                     | 创建PDF文档的[TeX排版系统](../Page/TeX.md "wikilink")                                                               |
| [pdfTeX](../Page/pdfTeX.md "wikilink")                                | GNU GPL                                                                                                                                                 | ?                                                     | 创建PDF文档的[TeX排版系统](../Page/TeX.md "wikilink")                                                               |
| [XeTeX](../Page/XeTeX.md "wikilink")                                  | MIT                                                                                                                                                     | ?                                                     | 创建PDF文档的[TeX排版系统](../Page/TeX.md "wikilink")                                                               |

### 阅读器

<table>
<thead>
<tr class="header">
<th><p>名称</p></th>
<th><p>许可</p></th>
<th><p>描述</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/Adobe_Reader.md" title="wikilink">Adobe Reader</a></p></td>
<td><p>专有、免费软件</p></td>
<td><p>Adobe的PDF阅读器</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/DigiSigner.md" title="wikilink">DigiSigner</a></p></td>
<td><p>专有、免费软件</p></td>
<td><p>附带数码签证PDF文档功能的PDF阅读器</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Evince.md" title="wikilink">Evince</a></p></td>
<td><p><a href="../Page/GNU_GPL.md" title="wikilink">GNU GPL</a></p></td>
<td><p><a href="../Page/GNOME.md" title="wikilink">GNOME的通用阅读器</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Mozilla_Firefox.md" title="wikilink">Mozilla Firefox</a></p></td>
<td><p>MPL</p></td>
<td><p>内置基于JavaScript的阅读器，以<a href="../Page/PDF.js.md" title="wikilink">PDF.js为基础</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Foxit_Reader.md" title="wikilink">Foxit Reader</a></p></td>
<td><p>专有、免费软件</p></td>
<td><p><a href="../Page/台式机.md" title="wikilink">台式机及</a><a href="../Page/移动设备.md" title="wikilink">移动设备的PDF查看器</a>/阅读器。允许用户添加任何元素到PDF（如带箭头的评论框、文本框、链接、书签及图片）。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Ghostview.md" title="wikilink">Ghostview</a></p></td>
<td><p>Aladdin自由公共许可证</p></td>
<td><p>一款Ghostscript的图形界面</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/MuPDF.md" title="wikilink">MuPDF</a></p></td>
<td><p><a href="../Page/Affero通用公共许可证.md" title="wikilink">Affero通用公共许可证</a></p></td>
<td><p>以小巧、快速，有高质量反锯齿图形为目标</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/PDF.js.md" title="wikilink">PDF.js</a></p></td>
<td><p><a href="../Page/Apache_License_2.0.md" title="wikilink">Apache License 2.0</a></p></td>
<td><p>一款转换PDF文件为<a href="../Page/HTML5.md" title="wikilink">HTML5的</a><a href="../Page/JavaScript.md" title="wikilink">JavaScript库</a>，可用作能包含于<a href="../Page/网页浏览器.md" title="wikilink">网页浏览器的基于web的查看器</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Qiqqa.md" title="wikilink">Qiqqa</a></p></td>
<td><p>专有、免费软件</p></td>
<td><p>以缩放、注释、标签、搜索、互相参照多种方式查看PDF</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Utopia_Documents.md" title="wikilink">Utopia Documents</a></p></td>
<td><p>专有、免费软件</p></td>
<td><p>语义学、科学用PDF阅读器（为生命科学及医学优化），允许在PDF加公共注释，在线使用时生成传输过程中的外部链接到科学数据库到资源</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Xpdf.md" title="wikilink">Xpdf</a></p></td>
<td><p><a href="../Page/GNU_GPL.md" title="wikilink">GNU GPL</a> 或专有版本</p></td>
<td><p>、多平台查看器；基于Xpdf库代码</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Google_Chrome.md" title="wikilink">Google Chrome</a></p></td>
<td><p>专有、免费软件</p></td>
<td><p>内置基于PDFium的阅读器</p></td>
</tr>
</tbody>
</table>

## 參考資料

<references/>

## 外部連結

  -
  -
[Category:軟體列表](../Category/軟體列表.md "wikilink")

1.
2.
3.
4.
5.  <http://www.adobe.com/products/reader/rdr_distribution1.html>
6.  <http://www.adobe.com/cn/products/reader/distribution.html>
7.  [Foxit MobilePDF on the Play
    Store](https://play.google.com/store/apps/details?id=com.foxit.mobile.pdf.lite)
8.  Google Drive Blog: [Better ways to find, view, and share in Drive
    for
    Android](http://googledrive.blogspot.de/2014/10/updates-to-drive-for-android.html)
9.  [Kindle for Android Now Supports
    PDF](http://the-digital-reader.com/2011/12/07/kindle-for-android-now-supports-pdf/)
10. <https://www.addpdf.cn/>
11. 对于PDF阅读依赖于GPLv2许可的ghostscript
    [1](http://www.imagemagick.org/script/formats.php)
12. [installing instructions, online QPDF
    manual](http://qpdf.sourceforge.net/files/qpdf-manual.html#ref.building)
13.
14. [iText: licenses](http://itextpdf.com/terms-of-use/) ，iText Software
    Corp.
15.
16.