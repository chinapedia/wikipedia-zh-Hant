**PE2**（IBM Personal Editor
II）為1980年[IBM出產](../Page/IBM.md "wikilink")，於[PC-DOS或](../Page/PC-DOS.md "wikilink")[MS-DOS](../Page/MS-DOS.md "wikilink")[電腦系統適用的個人文書編輯](../Page/電腦系統.md "wikilink")[商業軟體](../Page/商業軟體.md "wikilink")。它的特點是由幕後操作，全螢幕編輯，可同時開啟四個[視窗與](../Page/視窗.md "wikilink")20個文字檔，並在視窗與文字檔間進行切換做文字編輯。

雖該軟體文字輸入上有許多限制與不友善，使用鍵盤操控游標（無法使用[滑鼠](../Page/滑鼠.md "wikilink")）且編輯指令繁多。不過因操作簡易、容易入門且可自定功能鍵，因此於1980年代的DOS環境下，頗受歡迎。事實上，該「個人編輯器」的文書輸入軟體有許多版本，如PE，PE2或目標成為程式設計環境增加內建指令的PE3等等，不過以PE2版本最為著名。

1980年代的[台灣](../Page/台灣.md "wikilink")，因為PE2可以與[正體中文字體相容](../Page/正體中文.md "wikilink")，且可輕易結合[倚天](../Page/倚天中文系統.md "wikilink")、[倉頡等當時的中文系統共同於個人電腦工作](../Page/倉頡系統.md "wikilink")，因此PE2是1980年代台灣個人DOS環境下最為風行的文字編輯軟體。不但如此，之後還出版了各種不同的仿效軟體，功能各有其加強之處，如[慧星一號](../Page/慧星一號.md "wikilink")、[漢書等](../Page/漢書_\(軟體\).md "wikilink")。

在1980年代的中国大陆，PE2是[联想汉卡配套的一个字处理编辑软件](../Page/联想汉卡.md "wikilink")，因联想汉卡实现了在[CGA](../Page/CGA.md "wikilink")（640\*480）的分辨率下显示28行，其中25行为屏幕内容，3行提示行，其他软汉化的软件支持显示不好，而英文软件又不能支持汉字，所以PE2也十分流行，在当年的《[联想世界](../Page/联想世界.md "wikilink")》上还有多篇文章讨论过PE2的应用问题。

一組典型的PE2應包括PE2.EXE（主程式檔）、PE2.HLP（求助文字檔）、PE2.PRO（定義與巨集檔）。當時PE2的巨集寫作成為擴充PE2的必要知識。PE3的巨集檔甚至加入呼叫外部編譯器的能力。部分仿製品如漢書，它的[巨集能力已經可以作為](../Page/巨集.md "wikilink")[Script用途使用](../Page/腳本語言.md "wikilink")。

## 衍生軟體

  - DW2
    約1980年代末期[螢圃公司發行](../Page/螢圃.md "wikilink")，未經原出版者IBM授權，以PE2原本的執行檔，修補其執行檔，使其更適應[BIG5碼的編輯環境](../Page/BIG5.md "wikilink")，並改名為DW2。

## 參考資料

  - 戴建耘、陳詠卿，《國民中學資訊與電腦應用》，1994年，[松崗](../Page/松崗科技.md "wikilink")
  - 羅志明，《國中電腦》，1994年，[仁林](../Page/仁林.md "wikilink")
  - 李宽，《联想世界》1989年第6期，“方便PEII与其它实用程序使用的几点体会”

[Category:文本编辑器](../Category/文本编辑器.md "wikilink")
[Category:DOS軟體](../Category/DOS軟體.md "wikilink")