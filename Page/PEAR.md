**PEAR**（**P**HP **E**xtension and **A**pplication **R**epository）是由Stig
S. Bakken於2000年在[PHP開發者會議](../Page/PHP.md "wikilink")（PHP Developers'
Meeting,
PDM）上提出\[1\]，目的是實做可以重複使用的[函式庫來提供PHP社群使用](../Page/函式庫.md "wikilink")。\[2\]

這個計畫的目標為：

  - 提供有架構的程式碼。
  - 提供社群可重複使用的函式庫。
  - 建立PHP編碼風格標準。

## PEAR函式庫列表

  - Authentication
  - [Benchmarking](../Page/Benchmarking.md "wikilink")
  - Caching
  - Configuration
  - Console
  - [Database](../Page/Database.md "wikilink")
      - DB DataObject
      - DB DataObject FormBuilder
  - Date & Time
  - Encryption
  - Event
  - File Formats
  - File System
  - [Gtk](../Page/Gtk.md "wikilink") Components
  - Gtk2 Components
  - [HTML](../Page/HTML.md "wikilink")
  - [HTTP](../Page/HTTP.md "wikilink")
  - Images
  - Internationalization
  - Logging
  - Mail
  - Math
  - Networking
  - Numbers
  - Payment
  - [PEAR](../Page/PEAR.md "wikilink")
  - [PHP](../Page/PHP.md "wikilink")
  - Processing
  - Science
  - [Semantic Web](../Page/Semantic_Web.md "wikilink")
  - Streams
  - Structures
  - System
  - Text
  - Tools and Utilities
  - Validate
  - [Web Services](../Page/Web_Service.md "wikilink")
  - [XML](../Page/XML.md "wikilink")

## 参考文献

## 外部連結

  - [The PEAR Project](http://pear.php.net)
  - [The PECL Project](http://pecl.php.net)

{{-}}

[Category:PHP软件](../Category/PHP软件.md "wikilink")
[Category:Web应用框架](../Category/Web应用框架.md "wikilink")
[Category:自由软件包管理系统](../Category/自由软件包管理系统.md "wikilink")

1.
2.