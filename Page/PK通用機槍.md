**PK通用機槍**（[俄語](../Page/俄語.md "wikilink")：Пулемет
Калашникова，俄語羅馬化：Pulemyot
Kalashnikova，意為：卡拉什尼科夫機槍）是由[AK-47突击步枪的設計者](../Page/AK-47突击步枪.md "wikilink")[米哈伊尔·季莫费耶维奇·卡拉什尼科夫在](../Page/米哈伊尔·季莫费耶维奇·卡拉什尼科夫.md "wikilink")1961年所設計的[通用機槍](../Page/通用機槍.md "wikilink")，用於取代當時設計老舊的[RPD輕機槍和](../Page/RPD輕機槍.md "wikilink")[SG-43中型機槍](../Page/SG-43中型機槍.md "wikilink")，並大量裝備[蘇聯軍隊](../Page/蘇聯.md "wikilink")，PK通用機槍在蘇軍中的位置等同[北約國家的](../Page/北約.md "wikilink")[FN
MAG](../Page/FN_MAG通用機槍.md "wikilink")（[M240](../Page/M240通用機槍.md "wikilink")）、[M60或](../Page/M60通用機槍.md "wikilink")[MG3](../Page/MG3通用機槍.md "wikilink")。

## 設計

原型的PK通用機槍由[AK-47自動步槍氣動系統及旋轉式槍機閉鎖系統的基礎改進而成](../Page/AK-47突击步枪.md "wikilink")，發射[莫辛-納甘步槍及](../Page/莫辛-納甘步槍.md "wikilink")[SVD狙擊步槍的](../Page/SVD狙擊步槍.md "wikilink")[7.62×54毫米R舊型凸緣彈藥](../Page/7.62×54mmR.md "wikilink")，其後推出的改進版本，將槍枝盡量減重，並將槍管厚度減少，命名為PKM（[俄文](../Page/俄文.md "wikilink")：Пулемет
Калашникова
Модернизированный）。PK通用機槍大量減輕槍身的重量，槍機容納部採鋼板壓鑄成形法製造，[槍托中央也挖空](../Page/槍托.md "wikilink")，並在槍管外圍刻了許多溝紋，以致PK通用機槍只有9公斤，而PKM只有8.4公斤。PK通用機槍發射[7.62×54毫米R](../Page/7.62×54mmR.md "wikilink")[彈藥](../Page/彈藥.md "wikilink")，[彈鏈由](../Page/彈鏈.md "wikilink")[機匣右邊進入](../Page/機匣.md "wikilink")，彈殼在左邊排出。PK通用機槍的設計除可用作一般射擊有生目標外，亦可作防空機槍用途。當時蘇軍的7.62×54R彈鏈主要為25發，士兵多需要人手連接。由於生產當時正值[冷戰時期](../Page/冷戰.md "wikilink")，蘇聯及[華約國家不斷地增加軍備及不小國家都作特許生產](../Page/華約.md "wikilink")，因此PK通用機槍系列數量超過1,000,000把。

## 型号

[Machine_Gun_PKMS.jpg](https://zh.wikipedia.org/wiki/File:Machine_Gun_PKMS.jpg "fig:Machine_Gun_PKMS.jpg")
[Zastava_M84.jpg](https://zh.wikipedia.org/wiki/File:Zastava_M84.jpg "fig:Zastava_M84.jpg")
[Machine_gun_PKT.jpg](https://zh.wikipedia.org/wiki/File:Machine_gun_PKT.jpg "fig:Machine_gun_PKT.jpg")

  - <span id="PKM">**PKM**：（[俄文](../Page/俄文.md "wikilink")：ПКМ）是1969年的PK改進版本，主要改進了槍管及[槍托底板](../Page/槍托.md "wikilink")。PKM獲世界多個國家采用及仿制，包括[塞爾維亞的](../Page/塞爾維亞.md "wikilink")特許生產M84、保加利亞的MG-1及[中國的](../Page/中國.md "wikilink")[80式通用機槍](../Page/80式通用機槍.md "wikilink")\[1\]。
  - <span id="PKS">**PKS /
    PKMS**：（[俄文](../Page/俄文.md "wikilink")：ПКС）是裝在[三腳架的PK通用機槍版本](../Page/三腳架.md "wikilink")，PKMS是用PKM[機匣的改進版本](../Page/機匣.md "wikilink")。
  - <span id="PKB">**PKB /
    PKMB車載機槍**：（俄文：ПКБ），裝在[裝甲車上的PK通用機槍版本](../Page/裝甲車.md "wikilink")，在槍機容納部後端裝上握柄，PKMB是用PKM[機匣的改進版本](../Page/機匣.md "wikilink")。
  - <span id="PKT">**PKT / PKMT同軸機槍**：（俄文：ПК Танковый，解作PK
    Tank）是將PK機槍改裝成[坦克](../Page/坦克.md "wikilink")[砲塔內的](../Page/砲塔.md "wikilink")[同軸機槍](../Page/同軸機槍.md "wikilink")，以取代[SG-43](../Page/SG-43中型機槍.md "wikilink")，PKT移除了槍托、改為長重特殊槍管，加裝抽氣系統、螺線圈及電子控制扳機。PKMT是用PKM[機匣的改進版本](../Page/機匣.md "wikilink")。
  - [80式通用機槍](../Page/80式通用機槍.md "wikilink")：中國生產的PK機槍

## 使用國

[Hungarian_soldier.JPEG](https://zh.wikipedia.org/wiki/File:Hungarian_soldier.JPEG "fig:Hungarian_soldier.JPEG")軍隊使用PK通用機槍\]\]
[PKM_Machine_Gun_Iraq.jpg](https://zh.wikipedia.org/wiki/File:PKM_Machine_Gun_Iraq.jpg "fig:PKM_Machine_Gun_Iraq.jpg")軍隊使用PKS通用機槍\]\]
[Wojsko_Polskie_Irak_DF-SD-04-16534.JPEG](https://zh.wikipedia.org/wiki/File:Wojsko_Polskie_Irak_DF-SD-04-16534.JPEG "fig:Wojsko_Polskie_Irak_DF-SD-04-16534.JPEG")軍隊使用PKM通用機槍\]\]
[U.S.,_Partner_Nations_Train_During_Rapid_Trident_2011_(6007713721).jpg](https://zh.wikipedia.org/wiki/File:U.S.,_Partner_Nations_Train_During_Rapid_Trident_2011_\(6007713721\).jpg "fig:U.S.,_Partner_Nations_Train_During_Rapid_Trident_2011_(6007713721).jpg")士兵與PKM\]\]
[Mongolian_army_assigned_to_the_QRF.jpg](https://zh.wikipedia.org/wiki/File:Mongolian_army_assigned_to_the_QRF.jpg "fig:Mongolian_army_assigned_to_the_QRF.jpg")士兵使用裝上了波蘭製PCS-5瞄準鏡的PKM\]\]
[Northernalliance2002_crop.jpg](https://zh.wikipedia.org/wiki/File:Northernalliance2002_crop.jpg "fig:Northernalliance2002_crop.jpg")戰士與PKM\]\]

包括衍生型及特許生產型：

  -
  -
  -
  -
  -
  -
  -
  -
  - ：Zastava M84通用機槍

  -
  -
  -
  -
  -
  -
  - ：[80式通用機槍](../Page/80式通用機槍.md "wikilink")

  -
  -
  - ：Zastava M84通用機槍

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - ：Zastava M84通用機槍

  -
  -
  -
  -
  -
  -
  -
  -
  - ：Zastava M84通用機槍

  -
  -
  -
  -
  -
  -
  -
  - : Zastava M84通用機槍

  -
  -
  -
  -
  -
  - ：82式通用機槍

  -
  -
  -
  -
  -
  -
  -
  -
  - ：Zastava M84通用機槍

  -
  -
  - ：Zastava M84通用機槍

  -
  - \[2\]

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
### 前使用國

  -
  -
  -
  -
  - : Zastava M84通用機槍

## 流行文化

### 電影

  - 1988年—《》（Rambo
    III）：型號為PKM，被約翰·藍波（[席維斯·史特龍飾演](../Page/席維斯·史特龍.md "wikilink")）從[蘇聯軍隊手上所繳獲使用](../Page/蘇聯軍隊.md "wikilink")。
  - 1997年—《》（Чистилище）：型號為PKM，被俄羅斯士兵所使用。
  - 2001年—《[-{zh-cn:黑鹰坠落; zh-tw:黑鷹計劃;
    zh-hk:黑鷹十五小時;}-](../Page/黑鷹計劃.md "wikilink")》（Black
    Hawk Down）：型號為PKM，在[索馬里的槍店中出現](../Page/索馬里.md "wikilink")。
  - 2002年—《》（Война）：型號為PKM和PKT，被[車臣武裝份子所使用](../Page/車臣人.md "wikilink")。
  - 2005年—《[-{zh-hans:第九连;
    zh-hant:第九突擊隊;}-](../Page/第九突擊隊.md "wikilink")》（9
    рота）：被[蘇聯空降兵及](../Page/俄罗斯空降兵.md "wikilink")[阿富汗](../Page/阿富汗.md "wikilink")[聖戰份子所使用](../Page/聖戰者.md "wikilink")。
  - 2009年—《[-{zh-hans:暴力街区13：终极;
    zh-hant:暴力特區2：極限殺陣;}-](../Page/暴力街区13：终极.md "wikilink")》（Banlieue
    13 - Ultimatum）
  - 2010年—《[-{zh-cn:电锯惊魂3D; zh-tw:奪魂鋸3D;
    zh-hk:恐懼鬥室3D終極審判;}-](../Page/奪魂鋸3D.md "wikilink")》（Saw
    3D）：型號為BHI
    PKM，裝上[EOTech全息瞄準鏡並且被馬克](../Page/全息瞄準鏡.md "wikilink")·荷夫曼（哥斯達斯·文迪拿飾演）作為旋轉掃射機槍的陷阱所使用。
  - 2010年—《[-{zh-cn:生化危机4：来生; zh-tw:惡靈古堡4：陰陽界;
    zh-hk:生化危機3D：戰神再生;}-](../Page/惡靈古堡4：陰陽界.md "wikilink")》（Resident
    Evil:
    Afterlife）：型號為PKM，安置在[保護傘公司於](../Page/安布雷拉.md "wikilink")[東京支部的](../Page/東京.md "wikilink")[悍馬車上](../Page/悍馬_\(民用汽車\).md "wikilink")。
  - 2011年—《》（Forces
    spéciales）：型號為PKM，被[塔利班戰士所使用](../Page/塔利班.md "wikilink")。
  - 2012年—《》（Август. Восьмого）：型號為PKM，被俄羅斯士兵和格魯吉亞士兵所使用。
  - 2012年—《[-{zh-cn:敢死队2; zh-tw:浴血任務2;
    zh-hk:轟天猛將2;}-](../Page/浴血任務2.md "wikilink")》（The
    Expendables 2）：型號為PKM，被故事開頭的尼泊爾叛軍所使用。
  - 2013年—《[-{zh-cn:孤独的生还者; zh-tw:紅翼行動;
    zh-hk:絕地孤軍;}-](../Page/紅翼行動_\(電影\).md "wikilink")》（Lone
    Survivor）：型號為PKM，被[塔利班戰士所使用](../Page/塔利班.md "wikilink")。
  - 2016年—《[-{zh-cn:危机13小时; zh-tw:13小時：班加西的秘密士兵;
    zh-hk:13小時：班加西無名英雄;}-](../Page/13小時：班加西的秘密士兵.md "wikilink")》（13
    Hours: The Secret Soldiers of Benghazi）：型號為PKM，二連裝地安裝於車輛上，被武裝份子所使用。
  - 2016年—《[-{zh-hans:硬核大战; zh-hk:爆機特攻;
    zh-tw:超狂亨利;}-](../Page/硬核大戰.md "wikilink")》（Hardcore
    Henry）：型號為PKM，出現在吉米的武器庫當中，雖有被回收但沒有在電影中使用。
  - 2018年—《[-{zh-cn:12勇士; zh-tw:12猛漢;
    zh-hk:12壯士}-](../Page/12猛漢.md "wikilink")》（12
    Strong）：型號為PKM，被[北方聯盟和](../Page/北方聯盟.md "wikilink")[塔利班戰士所使用](../Page/塔利班.md "wikilink")。

### 電視劇

  - 2009年—《》（Deadliest
    Warrior）：型號為PKM，被索馬里[海盗所使用](../Page/海盗.md "wikilink")。

### 電子遊戲

  - 2001年—《[-{zh-hans:闪点行动：雷霆救兵;
    zh-hant:閃擊點行動：冷戰危機;}-](../Page/闪点行动：雷霆救兵.md "wikilink")》（Operation
    Flashpoint: Cold War Crisis）
  - 2005年—《[-{zh-hans:战地;
    zh-hant:戰地風雲;}-2](../Page/戰地風雲2.md "wikilink")》（Battlefield
    2）：解鎖武器。
  - 2005年—《[真實計劃](../Page/真實計劃.md "wikilink")》（Project Reality）
  - 2007年—《[-{zh-hans:反恐精英;
    zh-hant:絕對武力;}-Online](../Page/絕對武力Online.md "wikilink")》（Counter-Strike
    Online）：為PKM，可裝彈150發，使用黑色槍身、木製、[槍托](../Page/槍托.md "wikilink")、提把柄和橄欖色，並奇怪地有[三發點放功能](../Page/三發點放.md "wikilink")。武器模組採用鏡像佈局（右手持槍時拉機柄在左邊並從左邊供彈）。港台地區於2011年10月18日推出，命名為「橫掃千軍」；中国大陆地區於2011年10月19日推出，命名為「裁决者」。港台地區於2012年4月24日推出透過武器強化系統升級的兩个強化型外觀；中国大陆地區於2012年4月25日推出透過武器強化系統升級的兩个強化型外觀。
  - 2008年—《[-{zh-hans:孤岛惊魂;
    zh-hant:極地戰嚎;}-2](../Page/極地戰嚎2.md "wikilink")》（Far
    Cry 2）
  - 2008年—《[-{zh-hans:战地：叛逆连队;
    zh-hant:戰地風雲：惡名昭彰;}-](../Page/战地：叛逆连队.md "wikilink")》（Battlefield:
    Bad Company）：型號為PKM。
  - 2008年—《[-{zh-hans:合金装备4 爱国者之枪; zh-hant:潛龍諜影4
    愛國者之槍;}-](../Page/潛龍諜影4_愛國者之槍.md "wikilink")》（Metal
    Gear Solid 4: Guns of the Patriots）
  - 2009年—《[-{zh-hans:生化危机5; zh-hk:生化危機5;
    zh-hant:惡靈古堡5;}-](../Page/生化危機5.md "wikilink")》（Resident
    Evil 5）：換上了[RPD輕機槍的護木](../Page/RPD輕機槍.md "wikilink")。
  - 2010年—《[-{zh-hans:战地：叛逆连队;
    zh-hant:戰地風雲：惡名昭彰;}-2](../Page/戰地風雲：惡名昭彰2.md "wikilink")》（Battlefield：Bad
    Company 2）：型號為PKM。
  - 2010年—《[榮譽勳章](../Page/榮譽勳章_\(2010年遊戲\).md "wikilink")》（Medal of
    Honor）：型號為PKM，單人模式中在“Breaking
    Bagram”關卡中被“野免”所使用，其餘被敵軍所使用，有時會安裝於[武裝改裝車上](../Page/武裝改裝車.md "wikilink")。於多人模式當中被[塔利班步槍兵所使用](../Page/塔利班.md "wikilink")。
  - 2012年—《[-{zh-hans:生化危机6; zh-hk:生化危機6;
    zh-hant:惡靈古堡6;}-](../Page/生化危機6.md "wikilink")》（Resident
    Evil 6）：被東歐和中國的J'avo所使用，外型同生化危機5的型號。
  - 2012年—《[-{zh-hans:孤岛惊魂;
    zh-hant:極地戰嚎;}-3](../Page/極地戰嚎3.md "wikilink")》（Far
    Cry 3）
  - 2012年—《[俠盜獵車手V](../Page/俠盜獵車手V.md "wikilink")》（Grand Theft Auto V）
  - 2014年—《[-{zh-hans:孤岛惊魂;
    zh-hant:極地戰嚎;}-4](../Page/極地戰嚎4.md "wikilink")》（Far
    Cry 4）
  - 2016年—《[-{zh-hans:使命召唤：现代战争; zh-hk:決勝時刻：現代戰爭;
    zh-tw:決勝時刻：現代戰爭;}-重製版](../Page/決勝時刻4：現代戰爭.md "wikilink")》（Call
    of Duty: Modern Warfare
    Remastered）：於2017年3月14日添加到遊戲內。型號為PKM，命名为「PKM」，只於聯機模式登場。
  - 2016年—《[少女前線](../Page/少女前線.md "wikilink")》（Girls'
    Frontline），被名為「PK」的戰術人形所使用。
  - 2018年—《叛亂：沙塵暴》（Insurgency:Sandstorm）

### 小說

  - 2014年—《[刀劍神域外傳Gun Gale
    Online](../Page/刀劍神域外傳Gun_Gale_Online.md "wikilink")》：型號為PKM，被“SHINC”小隊的“羅莎”和“蘇菲”所使用。

## 参考文献

<div class="references-small">

<references />

  - [world.guns.ru-PK機槍系列](https://web.archive.org/web/20050801085047/http://world.guns.ru/machine/mg07-e.htm)
  - [milparade.com-俄羅斯軍備（PDF）](https://web.archive.org/web/20080227010804/http://www.milparade.com/Soderzhaniye.pdf)
  - [D boy Gun World-
    PK/PKM系列通用機槍](http://firearmsworld.net/russain/kalash/pk/pkfam.htm)

</div>

## 相關條目

  - [AK-47突击步枪](../Page/AK-47突击步枪.md "wikilink")
  - [RPD輕機槍](../Page/RPD輕機槍.md "wikilink")
  - [FN MAG通用機槍](../Page/FN_MAG通用機槍.md "wikilink")
  - [M60通用機槍](../Page/M60通用機槍.md "wikilink")
  - [UKM-2000通用機槍](../Page/UKM-2000通用機槍.md "wikilink")
  - [73式輕机槍](../Page/73式輕机槍.md "wikilink")

[Category:通用機槍](../Category/通用機槍.md "wikilink")
[Category:7.62×54毫米槍械](../Category/7.62×54毫米槍械.md "wikilink")
[Category:導氣式槍械](../Category/導氣式槍械.md "wikilink")
[Category:俄羅斯槍械](../Category/俄羅斯槍械.md "wikilink")
[Category:蘇聯槍械](../Category/蘇聯槍械.md "wikilink")
[Category:越戰武器](../Category/越戰武器.md "wikilink")
[Category:捷格加廖夫工廠](../Category/捷格加廖夫工廠.md "wikilink")

1.  [中國80式7.62毫米通用機槍](http://mil.news.sina.com.cn/p/2007-09-03/0843462409.html)
2.  <https://sites.google.com/site/worldinventory/wiw_as_koreasouth>