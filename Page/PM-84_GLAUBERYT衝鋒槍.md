**PM-84 GLAUBERYT**（wz.84
GLAUBERYT）是[波蘭在](../Page/波蘭.md "wikilink")1984年製造的[衝鋒槍](../Page/衝鋒槍.md "wikilink")。

## 簡介

PM-84
GLAUBERYT特點是重量輕，外型緊湊，操作穩定，用於個人防衛及150米內的近身戰鬥，發射[9毫米魯格彈](../Page/9毫米魯格彈.md "wikilink")，可選擇全自動或半自動（單發）射擊模式。與[PM-63
Rak用途相同](../Page/PM-63_Rak衝鋒槍.md "wikilink")，PM-84
GLAUBERYT主要提供於重型裝備士兵、[特種部隊](../Page/特種部隊.md "wikilink")、特種反恐部隊及警隊。

## 資料數據

  - [彈藥](../Page/彈藥.md "wikilink")：[9×19毫米魯格彈](../Page/9mm魯格彈.md "wikilink")
  - 槍口初速：360 米／秒
  - 槍口動能：518 [焦耳](../Page/焦耳.md "wikilink")
  - 射速：640 發／分鐘
  - 彈匣：15發（短彈匣）/25發（長彈匣）
  - 彈匣重量（長彈匣）：2.60 千克
  - 射擊模式：半自動或全自動
  - 槍機：自由後坐
  - 照門：可調式照門（75至150米）

## 衍生型

[PM-84_NTW_6_93.jpg](https://zh.wikipedia.org/wiki/File:PM-84_NTW_6_93.jpg "fig:PM-84_NTW_6_93.jpg")
[PM06_Glauberyt_PICT0014.jpg](https://zh.wikipedia.org/wiki/File:PM06_Glauberyt_PICT0014.jpg "fig:PM06_Glauberyt_PICT0014.jpg")
[PM-98_shooting.jpg](https://zh.wikipedia.org/wiki/File:PM-98_shooting.jpg "fig:PM-98_shooting.jpg")

  - PM-84 -9 x 18毫米PM彈版本
  - PM-84P -警用型
  - PM-98 -警用型
  - BRS-99-半自動版本
  - PM-06
  - PM-06S

|                                              |                                              |                                           |                                           |                                           |
| -------------------------------------------- | -------------------------------------------- | ----------------------------------------- | ----------------------------------------- | ----------------------------------------- |
|                                              | PM-84                                        | PM-84P                                    | PM 98                                     | PM-98S                                    |
| 口徑                                           | [9×18毫米PM](../Page/9×18mm馬卡羅夫.md "wikilink") | [9×19毫米](../Page/9×19mm魯格彈.md "wikilink") | [9×19毫米](../Page/9×19mm魯格彈.md "wikilink") | [9×19毫米](../Page/9×19mm魯格彈.md "wikilink") |
| 全長：[槍托摺疊](../Page/槍托.md "wikilink")／槍托展開（毫米） | 354/560                                      | 375/575                                   | 405/605                                   | 405/605                                   |
| 槍管長度（毫米）                                     | 165                                          | 185                                       | 185                                       | 185                                       |
| 空槍重（公斤）                                      | 1.84                                         | 2.17                                      | 2.42                                      | 2.42                                      |
| 連彈匣重：15/25發（公斤）                              | 2.07 / 2.24                                  | 2.46 / 2.62                               | 2.59 / 2.86                               | 2.59 / 2.86                               |
| 槍口初速（米／每秒）                                   | 332                                          | 360                                       | 360                                       | 123                                       |
| 發射速率（發／分鍾）                                   | 600                                          | 640                                       | 640                                       | 770                                       |
| 有效射程（米）                                      | 150                                          | 150                                       | 150                                       | 150                                       |

## 使用國

  -
  -
  -
  -
## 參考

  - [衝鋒槍](../Page/衝鋒槍.md "wikilink")
  - [PM-63 Rak衝鋒槍](../Page/PM-63_Rak衝鋒槍.md "wikilink")

## 外部連結

  - —[Fabryka Broni Łucznik - Radom](http://www.fabrykabroni.pl/)

  - —[Modern
    Firearms](https://web.archive.org/web/20100704184902/http://world.guns.ru/smg/smg68-e.htm)

  - —[D Boy Gun
    World（PM-84冲锋枪）](http://firearmsworld.net/polish/glauberyt/pm84.htm)

[Category:衝鋒槍](../Category/衝鋒槍.md "wikilink")
[Category:波蘭槍械](../Category/波蘭槍械.md "wikilink")
[Category:9毫米魯格彈槍械](../Category/9毫米魯格彈槍械.md "wikilink")