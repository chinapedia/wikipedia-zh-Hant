**Pm**、**pm**或**PM**可以指

## 時制

  - **[下午](../Page/下午.md "wikilink")**，拉丁语的缩写，用于[十二小时制](../Page/十二小时制.md "wikilink")。

## 單位

  - [懸浮粒子濃度](../Page/io懸浮粒子.md "wikilink"): PM
  - 長度:**[皮米](../Page/皮米.md "wikilink")**，英语**picometer**的缩写，一种长度单位，1皮米相当于1[米的一萬億分之一](../Page/米.md "wikilink")。1pm
    = 1 \* 10<sup>-12</sup> m

## 元素

  - **[钷](../Page/钷.md "wikilink")**，一种化学[元素](../Page/元素.md "wikilink")，它的[化学符号是](../Page/化学符号.md "wikilink")**Pm**，它的[原子序数是](../Page/原子序数.md "wikilink")61，属于[镧系元素](../Page/镧系元素.md "wikilink")，也是[稀土元素之一](../Page/稀土元素.md "wikilink")。

## 医疗

  - 家禽霍亂病源（Pasteurella multocida）

## 职业

  - **Prime Minister**
    的缩写，在新闻中经常运用，意指**[总理](../Page/总理.md "wikilink")（首相）**。
  - **Product Marketing** 的缩写，意指**[-{zh-cn:产品市场;
    zh-tw:產品行銷;}-](../Page/產品行銷.md "wikilink")**。
  - **Product Manager**
    的缩写，意指**[產品經理](../Page/產品經理.md "wikilink")**（產品經理人）。產品經理人從事的工作則是產品管理（）
  - **Project Management** 的缩写，意指**[-{zh-cn:项目管理;
    zh-tw:專案管理;}-](../Page/项目管理.md "wikilink")**。
  - **Project Manager** 的缩写，意指**[-{zh-cn:项目经理;
    zh-tw:專案經理;}-](../Page/项目经理.md "wikilink")**。

## 行為

  - **Personal
    Message**的缩写，在[网络语言中](../Page/网络语言.md "wikilink")，pm常作为动词使用，pm某人的意思是给某人发送站内短信息。
  - **Productive
    Maintenance**的缩写，中文译为生产维修，为机械加工性质的制造型企业对生产设备的有计划的维护和系统的整修。可以最大程度地保护设备，延长寿命并节约企业生产成本。

## 軟體

  - **[PageMaker](../Page/PageMaker.md "wikilink")**的缩写，一款為蘋果[麥金塔電腦撰寫的](../Page/麥金塔.md "wikilink")[桌面出版軟體](../Page/桌面出版.md "wikilink")。
  - **[PartitionMagic](../Page/Norton_PartitionMagic.md "wikilink")**的缩写，一款[个人电脑硬盘](../Page/个人电脑.md "wikilink")[分区軟件](../Page/分区.md "wikilink")。

## 娛樂作品

  - **Pocket
    Monster**的缩写，即[精靈寶可夢系列](../Page/精靈寶可夢系列.md "wikilink")，是[任天堂推出的一款掌机游戏](../Page/任天堂.md "wikilink")，亦有同名动画、漫画、书籍、对战卡片等一系列周边产品。

## 游戏

  - **pm**：2012年底KONAMI公司发布的游戏——[流行音乐](../Page/流行音乐_\(游戏\).md "wikilink")（Pop'N
    Music）的另一称呼。
  - **pac-man**:1980由南夢宮公司的岩谷徹設計,遊戲於1980年10月由Midway
    Games公司在美國發行,缺了一角的薄餅是岩谷徹創作此遊戲的靈感來源。

## 通訊技術

  - **[相位調變](../Page/相位调制.md "wikilink")**（**Phase Modulation**）的縮寫。