**PRV引擎**是1974年－1998年由[標緻汽車](../Page/標緻汽車.md "wikilink")、[雷諾汽車及](../Page/雷諾.md "wikilink")[富豪汽車等三家車廠共同研發的V型](../Page/富豪汽車.md "wikilink")6缸引擎，故其名稱各取其字首「P」、「R」、「V」而成；不過1994年之後漸漸被[PSA集團的](../Page/PSA集團.md "wikilink")[雷諾開發的](../Page/雷諾.md "wikilink")[ES引擎取代](../Page/ES引擎.md "wikilink")。

## 規格

這具V6引擎是從V8引擎修改過來的痕跡依稀可見，從兩列氣缸的排列角度可以看的出來，這具引擎不是採用一般V6引擎慣用的60°，而依然是V8引擎慣用的90°。

  - 馬力（DIN）: 136 hp at 5,500 rpm (100 kW at 92 r/s)
  - 馬力（SAE）: 130 hp at 5,500 rpm (97 kW at 92 r/s)
  - 扭力(DIN): 215 Nm at 48 r/s
  - 扭力（SAE）: 208 Nm at 48 r/s (153 ft.lbf at 2,750 rpm)
  - 壓縮比：8.8:1
  - 缸徑： 91 mm
  - 衝程： 73 mm
  - 位移： 2,849 cm ³
  - 點火次序： 1-6-3-5-2-4
  - 重量：約150 kg

## 使用車輛

[Engine_full.JPG](https://zh.wikipedia.org/wiki/File:Engine_full.JPG "fig:Engine_full.JPG")\]\]
使用PVR引擎為基礎動力的車種共有下述：

  - [Alpine A310](../Page/Alpine_A310.md "wikilink") *(October 1976)*
  - [Alpine A610](../Page/Alpine_A610.md "wikilink") *(1991)*
  - [Alpine GT/GTA](../Page/Alpine_GTA.md "wikilink") *(1984)*
  - [Citroën XM](../Page/Citroën_XM.md "wikilink") *(1989)*
  - [De Lorean DMC-12](../Page/De_Lorean_DMC-12.md "wikilink") *(1981)*
  - [Dodge Monaco](../Page/Dodge_Monaco.md "wikilink") *(1990-1992)*
  - [Eagle Premier](../Page/Eagle_Premier.md "wikilink") *(1988-1992)*
  - [Helem V6](../Page/Helem_V6.md "wikilink")
  - [Lancia Thema](../Page/Lancia_Thema.md "wikilink") *(1984)*
  - [Peugeot 504](../Page/Peugeot_504.md "wikilink") coupé/cabriolet
    *(1974/1975)*
  - [Peugeot 505](../Page/Peugeot_505.md "wikilink") *(July 1986)*
  - [Peugeot 604](../Page/Peugeot_604.md "wikilink") *(March 1975)*
  - [Peugeot 605](../Page/Peugeot_605.md "wikilink") *(1990)*
  - [Renault 25](../Page/Renault_25.md "wikilink") *(1984)*
  - [Renault 30](../Page/Renault_20/30.md "wikilink") *(March 1975)*
  - [Renault Espace](../Page/Renault_Espace.md "wikilink")
  - [Renault Laguna](../Page/Renault_Laguna.md "wikilink")
  - [Renault Safrane](../Page/Renault_Safrane.md "wikilink")
  - [Talbot Tagora](../Page/Talbot_Tagora.md "wikilink") *(1980)*
  - [Venturi](../Page/Venturi.md "wikilink") *（全車系）*
  - [Volvo 262/264/265](../Page/Volvo_200_series.md "wikilink")
    *（[October 3](../Page/October_3.md "wikilink")，1974）*
  - [Volvo 760 GLE](../Page/Volvo_760.md "wikilink") *(February 1982)*
  - [Volvo 780](../Page/Volvo_780.md "wikilink") *(1985)*

## 使用賽車

使用這具引擎為基礎動力的賽車共有下述：

  - Alpine A310 V6
  - Fouquet buggies
  - Peugeot 504 V6 Coupé
  - Schlesser Original
  - Venturi 400GTR and 600LM
  - WM Peugeot

[Category:標緻引擎與技術](../Category/標緻引擎與技術.md "wikilink")
[Category:雷諾引擎與技術](../Category/雷諾引擎與技術.md "wikilink")
[Category:富豪引擎與技術](../Category/富豪引擎與技術.md "wikilink")