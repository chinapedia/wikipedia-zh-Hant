**PSX**是[索尼公司生产的一款基于](../Page/索尼.md "wikilink")[PlayStation
2的能进行数字录象](../Page/PlayStation_2.md "wikilink")、DVD录象的多功能媒体设备名称。因为它是作为一种综合影音终端而不单纯是游戏机，所以是由索尼公司本部而非子公司[索尼電腦娛樂](../Page/索尼電腦娛樂.md "wikilink")（[SCEI](../Page/SCEI.md "wikilink")）负责经营（通常SCEI负责索尼游戏相关产品的经营）。PSX最早于2003年12月13日在[日本上市](../Page/日本.md "wikilink")，当时已有计划2004年将陆续在其他地区推出该产品，但因产品销量不佳而终止。

PSX有容量为160GB和250GB[硬盘的两个版本](../Page/硬盘.md "wikilink")，用户可以存储[电视节目](../Page/电视节目.md "wikilink")、家庭录象、数码相片或玩PS2游戏。PSX可以与[PSP连接以传输视频或音乐](../Page/PSP.md "wikilink")，并已有消息宣称PSX可以与[PS3连接](../Page/PS3.md "wikilink")，以使得单纯作为游戏设备的PS3成为一台综合影音终端。

PSX还以索尼独创的“十字媒体键”（[Cross Media
Bar](../Page/Cross_Media_Bar.md "wikilink")，[XMB](../Page/XMB.md "wikilink")）著称。

**PSX**同时也是最早[PlayStation游戏机的一个别称](../Page/PlayStation.md "wikilink")。字母“X”代表索尼最早是想把PlayStation设计成为一台[CD-ROM和任天堂超级游戏机](../Page/CD-ROM.md "wikilink")（[Super
Nintendo Entertainment
System](../Page/Super_Nintendo_Entertainment_System.md "wikilink")，[SNES](../Page/SNES.md "wikilink")）的混合体，然而[任天堂单方停止了该项计划](../Page/任天堂.md "wikilink")，于是索尼开始独立开发自己的游戏设备，被称为“**PlayStation计划**”（**the
PlayStation Experimental**）。

## 功能

### 视频编辑

PSX可以对用户录制的电视节目或导入的家庭录象进行一些基本的[非线性剪辑](../Page/非线性剪辑.md "wikilink")；也可以将图片或视频做成[幻灯片播放形式](../Page/幻灯片.md "wikilink")，加入特效，并可直接烧录成[DVD](../Page/DVD.md "wikilink")。PSX还可以将若干视频内容在以多画面形式播放，或将几个视频合成一个输出。

### 网络

用户可以通过PSX访问[网络](../Page/网络.md "wikilink")，收发[E-mail](../Page/E-mail.md "wikilink")，接受或发送多媒体内容。用户亦可以访问日本“索尼在线”网站以获取更多的视频特效，电影预告片或进行软件升级，这些类似的服务在上市的[PS3上也会有](../Page/PS3.md "wikilink")。PSX也可方便的连接[键盘和](../Page/键盘.md "wikilink")[鼠标](../Page/鼠标.md "wikilink")。

### 游戏

与[PS2一样](../Page/PS2.md "wikilink")，PSX完全向后支持所有[PlayStation](../Page/PlayStation.md "wikilink")（PSone）游戏，它同时也支持所有日文PS2在线游戏。同时PSX巨大的硬盘使得用户可以方便的保存游戏数据以作备份。

### PSP

PSX可以方便的与[PSP相连以传输数据](../Page/PSP.md "wikilink")。同时PSX中存储的电影可以转换为PSP所使用的格式，并通过[USB或](../Page/USB.md "wikilink")[记忆棒传输给PSP](../Page/Memory_Stick.md "wikilink")。

## 技术规格

**DESR-7700**

  - [硬盘容量](../Page/硬盘.md "wikilink")：250GB
  - [CPU及](../Page/CPU.md "wikilink")[GPU](../Page/GPU.md "wikilink")：90[纳米图象引擎](../Page/纳米.md "wikilink")
  - 可录介质：[DVD-R](../Page/DVD-R.md "wikilink")、[DVD-RW](../Page/DVD-RW.md "wikilink")
  - 可复制介质：[DVD-VIDEO](../Page/DVD-VIDEO.md "wikilink")、DVD-R、[DVD-RW](../Page/DVD-RW.md "wikilink")、音乐[CD](../Page/CD.md "wikilink")、[CD-R](../Page/CD-R.md "wikilink")（图片）、记忆棒、PlayStation标准CD-ROM、PlayStation
    2标准CD-ROM或DVD-ROM

**DESR-5700**

  - [硬盘容量](../Page/硬盘.md "wikilink")：160GB
  - [CPU及](../Page/CPU.md "wikilink")[GPU](../Page/GPU.md "wikilink")：90[纳米图象引擎](../Page/纳米.md "wikilink")
  - 可录介质：[DVD-R](../Page/DVD-R.md "wikilink")、[DVD-RW](../Page/DVD-RW.md "wikilink")
  - 可复制介质：[DVD-VIDEO](../Page/DVD-VIDEO.md "wikilink")、DVD-R、[DVD-RW](../Page/DVD-RW.md "wikilink")、音乐[CD](../Page/CD.md "wikilink")、[CD-R](../Page/CD-R.md "wikilink")（图片）、记忆棒、PlayStation标准CD-ROM、PlayStation
    2标准CD-ROM或DVD-ROM

**DESR-7500**

**DESR-5500**

## 外部链接

  - [PSX官方网站（日文）](http://www.psx.sony.co.jp)
  - [媒体报道（英文）](http://www.sony.net/SonyInfo/News/Press_Archive/200310/03-1007E/)

## 相关条目

[Category:索尼遊戲機](../Category/索尼遊戲機.md "wikilink")
[Category:PlayStation](../Category/PlayStation.md "wikilink")
[Category:PlayStation 2](../Category/PlayStation_2.md "wikilink")
[Category:2003年面世的產品](../Category/2003年面世的產品.md "wikilink")