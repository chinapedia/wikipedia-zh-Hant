**PUMA**，是一間[德國體育用品製造商](../Page/德國.md "wikilink")，為[開雲集團旗下品牌](../Page/開雲集團.md "wikilink")。主要對手有[耐吉](../Page/耐吉.md "wikilink")、[愛迪達](../Page/愛迪達.md "wikilink")、[新百倫及](../Page/新百倫.md "wikilink")[安德阿莫等](../Page/安德阿莫.md "wikilink")。PUMA的[鞋與](../Page/鞋.md "wikilink")[服飾在](../Page/服飾.md "wikilink")[嘻哈](../Page/嘻哈.md "wikilink")[塗鴉文化中](../Page/塗鴉.md "wikilink")，無論是[美國內外](../Page/美國.md "wikilink")，都受到極度歡迎。PUMA也與愛迪達同為1970與1980年代[嘻哈文化代表物之一](../Page/嘻哈.md "wikilink")。
\[1\]。

## 簡介

[Rudi_Dassler.jpg](https://zh.wikipedia.org/wiki/File:Rudi_Dassler.jpg "fig:Rudi_Dassler.jpg")
阿迪達斯的前身是由[鲁道夫·達斯勒和](../Page/鲁道夫·達斯勒.md "wikilink")[阿道夫·達斯勒兩兄弟共同建立的](../Page/阿道夫·達斯勒.md "wikilink")「達斯勒兄弟鞋廠」（）\[2\]。[1936年柏林奧運會期間](../Page/1936年柏林奧運會.md "wikilink")，達斯勒兄弟力排眾議向[美國黑人運動員](../Page/美國.md "wikilink")[傑西·歐文斯提供鞋釘](../Page/傑西·歐文斯.md "wikilink")，結果歐文斯連奪四枚奧運金牌，達斯勒兄弟鞋廠也一炮而紅\[3\]。

但是後來的一場誤會引發了兩兄弟的不和：1943年，[二次大戰期間](../Page/二次大戰.md "wikilink")，有一次鲁道夫一家人為了躲避[盟軍的](../Page/盟軍.md "wikilink")[轟炸而躲到了阿道夫一家的](../Page/轟炸.md "wikilink")[防空洞裏面](../Page/防空洞.md "wikilink")，阿道夫不經意地說了一句：「那些死雜種又來了」。儘管阿道夫後來解釋「[死雜種](../Page/死雜種.md "wikilink")」指的是盟軍的[軍機](../Page/軍機.md "wikilink")[飛行員](../Page/飛行員.md "wikilink")，但是魯道夫卻堅持認為「死雜種」指的是自己一家人\[4\]。後來魯道夫被[美軍俘獲](../Page/美軍.md "wikilink")，魯道夫更加相信是阿道夫向美軍出賣了自己\[5\]。二戰結束後，達斯勒兄弟鞋廠復業，有47名員工，並以帆布與[美軍燃油槽提煉出](../Page/美軍.md "wikilink")[合成橡膠](../Page/合成橡膠.md "wikilink")，製成戰後第一款運動鞋。但兄弟二人的不和最終導致他們在1948年分道揚鑣：阿道夫另外建立了自己的品牌「愛迪達」；而魯道夫則在[黑措根奥拉赫河的另一邊建立了自己的品牌](../Page/黑措根奥拉赫河.md "wikilink")，一開始以「鲁道夫·達斯勒」名和姓的首音節命名為「魯達」，後來為了營銷而改名為「Puma」（意思是「美洲獅」）\[6\]。據說兩兄弟直到離世也沒有再往來過，甚至身故後也不願葬在對方身邊\[7\]。直到2009年愛迪達和PUMA兩間企業的員工在黑措根奥拉赫舉行了一場[足球友誼賽](../Page/足球.md "wikilink")，為兩兄弟之間長達一甲子的齟齬，象徵性地劃下了句點\[8\]。

1986年PUMA加入[慕尼黑與](../Page/慕尼黑.md "wikilink")[法蘭克福的](../Page/法蘭克福.md "wikilink")[證券交易所](../Page/證券交易所.md "wikilink")。

2003年公司的資產達到12億[歐元](../Page/歐元.md "wikilink")\[9\]，

2006年公司[利潤](../Page/利潤.md "wikilink")2.63億歐元。

2007年底，PUMA有9,204名員工，並販賣產品至超過80個國家。銷售額23.7億[歐元](../Page/歐元.md "wikilink")，並被[開雲集團併購](../Page/開雲集團.md "wikilink")。

2009年，愛迪達和PUMA兩間企業的員工在黑措根奥拉赫舉行了一場足球友誼賽，為兩兄弟之間長達60年的不和象徵性地劃上了句號\[10\]。

2010年，從手中購入[眼鏡蛇高爾夫](../Page/眼鏡蛇高爾夫.md "wikilink")100%的股份。

## 聯乘

2018年也月，[韓國設計師品牌](../Page/韓國.md "wikilink") Ader Error 與運動品牌Puma聯乘，把Puma
來自八十年代的經典鞋履設計系列
RS-series復刻，特別之處是鞋跟附近的部位有一[USB插頭細節](../Page/USB.md "wikilink")\[11\]。

## 赞助

PUMA赞助不少[足球俱樂部](../Page/足球.md "wikilink")，如有
[阿森纳](../Page/阿森纳.md "wikilink")、[水晶宫](../Page/水晶宫足球俱乐部.md "wikilink")、[纽卡斯尔联](../Page/纽卡斯尔联足球俱乐部.md "wikilink")、[AC米蘭](../Page/AC米蘭足球俱乐部.md "wikilink")、[多特蒙德](../Page/多特蒙德足球俱乐部.md "wikilink")、[马赛](../Page/马赛足球俱乐部.md "wikilink")、[斯图加特](../Page/斯图加特足球俱乐部.md "wikilink")、[波尔多等](../Page/波尔多足球俱乐部.md "wikilink")。

PUMA亦为多队国家足球队制作球衣，例如[意大利](../Page/意大利国家足球队.md "wikilink")、[捷克](../Page/捷克国家足球队.md "wikilink")、[瑞士](../Page/瑞士国家足球队.md "wikilink")、[乌拉圭](../Page/乌拉圭国家足球队.md "wikilink")、[智利](../Page/智利国家足球队.md "wikilink")、[科特迪瓦](../Page/科特迪瓦国家足球队.md "wikilink")、及[加纳等](../Page/加纳国家足球队.md "wikilink")。

PUMA亦赞助了许多足球员，其中以[蒂埃里·亨利](../Page/蒂埃里·亨利.md "wikilink")、[阿奎罗](../Page/塞尔希奥·阿奎罗.md "wikilink")、[埃托奥](../Page/萨缪埃尔·埃托奥.md "wikilink")、[托马什·罗西基](../Page/托马什·罗西基.md "wikilink")、[马尔科·罗伊斯](../Page/马尔科·罗伊斯.md "wikilink")、[巴卡雷·萨尼亚](../Page/巴卡雷·萨尼亚.md "wikilink")、[亚亚·图雷](../Page/亚亚·图雷.md "wikilink")、[布冯](../Page/詹路易吉·布冯.md "wikilink")、[奥利弗·吉鲁](../Page/奥利弗·吉鲁.md "wikilink")、[巴洛特利](../Page/巴洛特利.md "wikilink")、[基耶利尼](../Page/乔吉奥·基耶利尼.md "wikilink")、[米克尔·阿特塔及](../Page/米克尔·阿特塔.md "wikilink")[法布雷加斯等人最为著名](../Page/塞斯克·法布雷加斯.md "wikilink")。

PUMA赞助的[田径选手中](../Page/田径.md "wikilink")，最为人熟悉的为[牙买加飞人](../Page/牙买加.md "wikilink")[保特](../Page/保特.md "wikilink")。

## 代言人

### 美国

  - [蕾哈娜](../Page/蕾哈娜.md "wikilink")
  - [席琳娜·戈梅茲](../Page/席琳娜·戈梅茲.md "wikilink")
  - [魯迪·蓋伊](../Page/魯迪·蓋伊.md "wikilink")
  - [德安德魯·艾頓](../Page/德安德魯·艾頓.md "wikilink")
  - [泰瑞·洛齊爾](../Page/泰瑞·洛齊爾.md "wikilink")
  - [迪馬克斯·卡森斯](../Page/迪馬克斯·卡森斯.md "wikilink")

### 英国

  - [卡拉·迪瓦伊](../Page/卡拉·迪瓦伊.md "wikilink")

### 中國

  - [楊洋](../Page/楊洋.md "wikilink")
  - [刘雯](../Page/刘雯.md "wikilink")
  - [李现](../Page/李现.md "wikilink")

### 韓国

  - [防彈少年團](../Page/防彈少年團.md "wikilink")
  - [BLACKPINK](../Page/BLACKPINK.md "wikilink")

## 市場主要競爭對手

彪馬的市場對手有[Adidas](../Page/Adidas.md "wikilink")、[Asics](../Page/Asics.md "wikilink")、[Mizuno](../Page/Mizuno.md "wikilink")、[New
Balance](../Page/New_Balance.md "wikilink")、[NIKE](../Page/NIKE.md "wikilink")、[Under
Armour等](../Page/Under_Armour.md "wikilink")。

## 參考

<references/>

## 外部連結

  -
[Category:跨國公司](../Category/跨國公司.md "wikilink")
[Category:運動用品製造商](../Category/運動用品製造商.md "wikilink")
[Category:1948年成立的公司](../Category/1948年成立的公司.md "wikilink")
[Category:德國鞋類製造商](../Category/德國鞋類製造商.md "wikilink")
[Category:德國服裝品牌](../Category/德國服裝品牌.md "wikilink")
[Category:歐洲股份公司](../Category/歐洲股份公司.md "wikilink")

1.

2.
3.

4.
5.
6.
7.

8.

9.

10.

11. [如果你走路累了，就穿上這雙球鞋充充電吧｜ ADER ERROR X PUMA 聯乘復刻八十年代經典
    RS-SERIES](https://www.mings-fashion.com/ader-error-puma-%E8%81%AF%E4%B9%98-200825/)