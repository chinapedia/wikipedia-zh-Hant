**Pages**
是一个[文字处理和](../Page/文字处理.md "wikilink")[页面排版应用程序](../Page/页面排版.md "wikilink")。由
[苹果电脑公司开发](../Page/苹果电脑公司.md "wikilink")，包括在
[iWork软件套装中](../Page/iWork.md "wikilink")。Pages 1.0
发布于2005年初，并于当年5月开始上市。和苹果其他大部分软件一样，它只能运行在 [OS
X系统上](../Page/OS_X.md "wikilink")。 Pages
2作为[iWork](../Page/iWork.md "wikilink") '06 的一部分，于2006年发布。

## 历史

OS X系统上的 Pages
是苹果公司多功能办公软件套装[AppleWorks的继承者](../Page/AppleWorks.md "wikilink")。早在2003年就开始在网上有信息流传称苹果公司将出品一款新的文字处理器来替代
AppleWorks，而且新软件包的名称很有可能是 "iWorks" 或者
"iWork"。之后众多的麦金塔电脑用户期望新的程序在2004年问世，可是苹果公司CEO[史蒂夫·乔布斯直到](../Page/史蒂夫·乔布斯.md "wikilink")2005年初才终于发布[iWork](../Page/iWork.md "wikilink")
'05 和[iLife](../Page/iLife.md "wikilink") '05。

Pages
Software电脑公司曾经有个一样名字的软件用于[NeXT电脑](../Page/NeXT.md "wikilink")，包括相似的[所见即所得页面排版性能](../Page/所见即所得.md "wikilink")。后来在1997年苹果公司兼并了NeXT，所以人们认为这两个程序有同样的代码基础。尽管如此，据说Pages
Software公司的[NeXTSTEP](../Page/NeXTSTEP.md "wikilink")
套件一开始只是给一个芝加哥的IT解决方案公司做的。不过OS X
的Pages程序和 [Keynote是同一个团队开发制作的](../Page/Keynote.md "wikilink")。

## 功能

Pages 支持多栏排版，文本段落样式，脚注和其他高级文字处理功能。它为用户提供了强大的模板功能支持。
[AppleWorks](../Page/AppleWorks.md "wikilink")
和微软[Word文件均可被导入](../Page/Word.md "wikilink")，文件可以导出为多种其他格式。在文档中可以直接绘制形状，并直接修改属性。图形可以插入文档，可以环绕文本，并设置阴影，多角度旋转等等。

Pages
能创建列表，专栏，URL链接，日期和时间，页码等对象，并能从[iTunes](../Page/iTunes.md "wikilink")，[iMovie和](../Page/iMovie.md "wikilink")[iPhoto中接受数据](../Page/iPhoto.md "wikilink")。Pages还可用来创建新闻报道，发表，空白文档，论文，文件，请柬，教学材料和其他类型的文档。创建的文档都可以导出为RTF,
HTML, Word, PDF
或者Pages文件格式。它的基本定位是实现一些基本的排版和文字处理功能，而[微软](../Page/微软.md "wikilink")[Word和](../Page/Word.md "wikilink")[Adobe](../Page/Adobe.md "wikilink")
[InDesign更适合于出版大型广告和整本的印刷书籍](../Page/InDesign.md "wikilink")。

Pages的一些功能和受欢迎，如段落样式夹（paragraph style drawer），调板（inspector palette）以及Mac
OS X 系统集成的拼写检查功能等等，但是一些用户也表示不满，它没有语法检查，校正，词典等功能，直到[Mac OS X
10.4版本里的Pages才有改进](../Page/Mac_OS_X_10.4.md "wikilink")。

## 版本历史

| 版本号     | 发布日期        | 更改                                                                                                                                                                |
| ------- | ----------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1.0     | 2005年2月     | 首发                                                                                                                                                                |
| 1.0.1   | 2005年3月17日  | 修正一些错误。可以删除模板页面。                                                                                                                                                  |
| 1.0.2   | 2005年5月25日  | 页面导航和组织的改进。                                                                                                                                                       |
| 2.0     | 2006年1月10日  | 作为 iWork'06的一部分发表。带有新模板，表格计算，照片形状遮罩蒙版，自由曲线工具等等。                                                                                                                   |
| 2.0.1   | 2006年4月26日  | Pages 2.0.1 增加了图表和图像调整。修正了一些小错误。                                                                                                                                  |
| 2.0.1v2 | 2006年5月1日   | 局部改进                                                                                                                                                              |
| 2.0.2   | 2006年9月28日  | Pages 2.0.2 增加了与 [Aperture的兼容](../Page/Aperture.md "wikilink")                                                                                                    |
| 3.0     | 2007年8月7日   | Pages 3.0作为iWork '08的一部分发行。引入了与[Microsoft Office 2007文件的兼容性](../Page/Microsoft_Office_2007.md "wikilink")。引入了改动追踪功能。与Pages 2.0相比，Pages 3.0仅需要三分之一的硬盘空间。           |
| 3.0.1   | 2007年9月27日  | 修复性能及改动追踪功能。                                                                                                                                                      |
| 3.0.2   | 2008年1月29日  | 改进Mac OS X系统的兼容性。                                                                                                                                                 |
| 4.0     | 2009年1月6日   | Pages 4.0作为iWork '09的一部分发行。新功能包括全屏編輯，與[微軟](../Page/微軟.md "wikilink")[Microsoft Office更好的兼容性](../Page/Microsoft_Office.md "wikilink")，並可選擇上傳文件，以及全新的iWork.com服務。   |
| 4.0.1   | 2009年3月26日  | Improves reliability when working with EndNote X2 or MathType 6, or deleting Pages files.                                                                         |
| 4.0.2   | 2009年5月28日  | Improves reliability when saving documents.                                                                                                                       |
| 4.0.3   | 2009年9月28日  | Improves reliability with full-screen mode, applying transparency to images, and [EndNote](../Page/EndNote.md "wikilink") citations.                              |
| 4.0.4   | 2010年8月26日  | Adds support for exporting to the [EPUB](../Page/EPUB.md "wikilink") format (for use with [iBooks](../Page/iBooks.md "wikilink")) and fixes problems with tables. |
| 4.0.5   | 2011年1月5日   | Improves the readability of exported ePub documents.                                                                                                              |
| 4.1     | 2011年7月20日  | 加入支援 Mac OS X Lion, 包括: 全螢幕, Resume, 自動存入, Versions, Character picker. 增進與 Microsoft Office 的相容性                                                                  |
| 4.2     | 2012年7月25日  | 加入支援 Mac OS X Mountain Lion 及 儲存文件於 iCloud.                                                                                                                       |
| 4.3     | 2012年12月4日  | 加入支援 iWork for iOS 1.7 應用程式.                                                                                                                                      |
| 5.0     | 2013年10月22日 | 完全重新设计改造.                                                                                                                                                         |
|         |             |                                                                                                                                                                   |

|}

## 参见

  - [文字处理软件](../Page/文字处理软件.md "wikilink")
  - [Word](../Page/Word.md "wikilink")
  - [AppleWorks](../Page/AppleWorks.md "wikilink")
  - [桌面出版](../Page/桌面出版.md "wikilink")
  - [iWork](../Page/iWork.md "wikilink")

## 外部链接

  - [繁体中文官方网页](https://web.archive.org/web/20061006210243/http://www.apple.com.tw/iwork/pages/)
  - [简体中文官方网页](https://web.archive.org/web/20061105005448/http://www.apple.com.cn/iwork/pages/)
  - [Pages用户网站](http://www.Pagesuser.com/)
  - [iWork Community](http://www.iworkcommunity.com/)
  - [Pages FAQ blog](http://pagesfaq.blogspot.com/)

[ru:IWork\#Pages](../Page/ru:IWork#Pages.md "wikilink")

[Category:蘋果公司軟體](../Category/蘋果公司軟體.md "wikilink")
[Category:MacOS軟體](../Category/MacOS軟體.md "wikilink")
[Category:文書處理器](../Category/文書處理器.md "wikilink")