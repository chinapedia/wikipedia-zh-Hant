[Palmone_pdas.jpg](https://zh.wikipedia.org/wiki/File:Palmone_pdas.jpg "fig:Palmone_pdas.jpg")
**Palm**曾经是著名的[手持设备制造商](../Page/手持设备.md "wikilink")，推出的[掌上电脑和](../Page/掌上电脑.md "wikilink")[手持设备採用名為](../Page/手持设备.md "wikilink")[Palm
OS](../Page/Palm_OS.md "wikilink")、[webOS](../Page/webOS.md "wikilink")、[微软](../Page/微软.md "wikilink")[Windows
CE等](../Page/Windows_CE.md "wikilink")[操作系统](../Page/操作系统.md "wikilink")。[Palm
OS过去拥有](../Page/Palm_OS.md "wikilink")[Handspring](../Page/Handspring.md "wikilink")、[Sony等兼容厂商](../Page/Sony.md "wikilink")，但后来由于全球[智能手机时代兴起](../Page/智能手机.md "wikilink")，[微软在手持设备的介入](../Page/微软.md "wikilink")，以及[Palm
OS](../Page/Palm_OS.md "wikilink")[多媒体功能的相对薄弱和](../Page/多媒体.md "wikilink")[公司治理问题](../Page/公司治理.md "wikilink")，Palm逐渐没落。

## 歷史

**Palm**在1993年由[傑夫·霍金](../Page/傑夫·霍金.md "wikilink")（Jeff Hawkins）和Donna
Dubinsky共同创立。1994年，Jeff
Hawkins发明了一种称为“[fitfitchocolate](../Page/Graffiti_英文手寫輸入法.md "wikilink")”的[手写输入方法](../Page/手写输入方法.md "wikilink")，只需简单的学习，人们就能很快掌握书写规则，在掌上电脑上可以达到几乎和正常手写一样的识别速度和识别正确率，这样便有效的解决了掌上电脑上的手写识别问题（比如[Apple
Newton](../Page/Apple_Newton.md "wikilink")），因此Graffiti在Newton用户中很受欢迎。

同年，Hawkins开始了他第一部掌上电脑的设计构想，其实那个时候他的设想只是一个简单的电子助理－[Organizer](../Page/Organizer.md "wikilink")。在他的构想中，这样一部电子助理应该具备以下几个特点：

1.  体积小到可以放在衬衫的口袋里
2.  可以和个人电脑同步数据资料
3.  操作简单（功能上以[PIM为主](../Page/个人信息管理.md "wikilink")）
4.  价格低廉。

不过，由于掌上电脑产业的不景气，Hawkins没有机会把他的构想变为现实。1995年9月，公司看到了Palm
Computing公司的发展潜力，将其收购。在U.S.
Robotics的资金支持下，1996年4月，第一部[PalmPilot掌上电脑Pilot](../Page/PalmPilot.md "wikilink")
1000问世了，它使用的是Hawkins和Dubinsky自己开发的Palm OS
1.0操作系统，有256K内存。随后，配备了512K内存的Pilot
5000也上市了。同年还获得了[PC
Computing的](../Page/PC_Computing.md "wikilink")“年度易用产品”MVP奖。

1997年，PalmPilot的第二代产品，PalmPilot Personal和Professional问世，这两款产品使用的是Palm OS
2.0，其中Palm Pilot Personal拥有512K内存，增加了背光功能，而Palm Pilot
Professional则具备1M内存。Palm OS
2.0和1.0相比，进一步完善了Palm的功能，增加了[TCP/IP支持](../Page/TCP/IP.md "wikilink")、[电子邮件软件](../Page/电子邮件.md "wikilink")、滚动条、更容易的对[数据库进行分类和查找](../Page/数据库.md "wikilink")、更容易的分类（categories）的操作等功能。

第一代Palm 中文系統由[香港人](../Page/香港人.md "wikilink")（Water
Lou）編寫，解決Palm不能閱讀中文的問題，系統開始在港台、以至中國大陸流行。在中国大陆，Palm的中文系统主要由[杜永涛编写的](../Page/杜永涛.md "wikilink")[CJKOS覆盖](../Page/CJKOS.md "wikilink")。在台湾，则是[掌龙中文](../Page/掌龙中文.md "wikilink")。

同年，[3Com收购了US](../Page/3Com.md "wikilink") Robotics公司。Jeff Hawkins和Donna
Dubinsky感到發展受局限，於是離巢自組公司[HandSpring](../Page/HandSpring.md "wikilink")，開發掌上電腦。

1997年底，Palm开始授权Palm
OS给[IBM](../Page/IBM.md "wikilink")、HandSpring及[Sony等公司](../Page/Sony.md "wikilink")，生產採用Palm
OS的產品，但後期不少廠商，例如IBM及Sony因掌上電腦前景未如理想，放棄繼續授權。

此後數年，掌上電腦經歷了一段高增長期，但由於其他數碼產品，例如[智能手機](../Page/智能手機.md "wikilink")、[多媒體播放器及](../Page/多媒體播放器.md "wikilink")[手提遊戲機的競爭](../Page/手提遊戲機.md "wikilink")，掌上電腦的受歡迎程度開始下降。

隨著掌上電腦市場萎縮，2003年10月28日，Palm
股东批准收購Handspring，並把公司一拆為二，创建两家独立的公司：原公司更名为[PalmOne公司](../Page/PalmOne.md "wikilink")，專注於硬件開發，该公司的普通股开始以股票代号
PLMO
在[纳斯达克股票交易所上进行交易](../Page/纳斯达克.md "wikilink")；新公司[PalmSource則專注於開發及維護掌上電腦操作系统Palm](../Page/PalmSource.md "wikilink")
OS平台。Palm管理層認為Palm
OS平台比较适合独立运作，以吸引各种新的特许制造商的加入，并且可以进一步建立[掌上经济](../Page/掌上经济.md "wikilink")（Palm
Economy）。

Palm一拆為二後，PalmOne在純掌上電腦的產品銷量、市場佔有率與業績皆未見起色，但智能手機市場卻如日方中，其[Treo系列銷量不俗](../Page/Treo.md "wikilink")。不少評論認為PalmOne固步自封，遲遲未採用Palm
OS的最新版本6.0，也有評論認為Palm
OS過份簡單，已未能滿足使用者的需要。相反，採用[微軟](../Page/微軟.md "wikilink")[Windows
Mobile操作系統的掌上電腦卻漸漸受到注意](../Page/Windows_Mobile.md "wikilink")。

2005年7月14日，PalmOne正式把名稱改回Palm,Inc. 並積極發展成一間純硬件生產商，與Palm
OS劃清界線。2005年9月9日，PalmSource被日本軟件公司[愛可信](../Page/愛可信.md "wikilink")（Access）收購。2005年9月26日，Palm與其從前死敵微軟合作，在新的'"Treo700'"智能手機採用微軟的Windows
Mobile操作系統，但Palm聲言不會放棄Palm OS。

2006年12月7日，Palm以4千4百萬美元從[Access購回Palm](../Page/爱可信.md "wikilink") OS 5
之永久使用權及原代碼。

2009年Palm推出新產品[Palm
Pre](../Page/Palm_Pre.md "wikilink")，採用全新作業系統搶攻行動電話市場。所有努力均告失败。

2010年4月28日，[惠普公司宣佈以](../Page/惠普公司.md "wikilink")12億[美元收購](../Page/美元.md "wikilink")**Palm**。\[1\]

2012年8月15日，惠普公司宣布解散Palm公司，將它們併入公司之中。2014年10月，惠普公司将Palm品牌出售给中国的[TCL集团](../Page/TCL集团.md "wikilink")。

2018年10月，[TCL集团将Palm品牌授权给前三星设计副总裁Dennis](../Page/TCL集团.md "wikilink")
Miloseski和Howard
Nuk，在他们的主导下聯合[威瑞森推出搭载Android](../Page/威瑞森.md "wikilink")
8.1，屏幕只有3.3寸的全新Palm智能机，但是新Palm无法擁有独立手機號碼，必须搭配插有[威瑞森電話卡的主手機使用](../Page/威瑞森.md "wikilink")，主手機通過[威瑞森的NumberShare共享服务](../Page/威瑞森.md "wikilink")，將手機號碼分享給新Palm手機，用戶在新Palm
上同步主手机收到的信息、通知\[2\]，但新Palm可以使用[WiFi](../Page/WiFi.md "wikilink")，因此，新Palm定位跟[Apple
Watch](../Page/Apple_Watch.md "wikilink")
[LTE版或其他](../Page/LTE.md "wikilink")[Android
Wear有點類似](../Page/Android_Wear.md "wikilink")\[3\]。

2019年4月4日，独立的Palm手机开始通过[威瑞森网站出售](../Page/威瑞森.md "wikilink")\[4\]。

## 硬件發展

早期Palm硬件十分簡單，顯示屏只有黑白，並全部採用[摩托羅拉龍珠處理器](../Page/摩托羅拉龍珠處理器.md "wikilink")。後來Palm的型號已不再採用龍珠處理器，而是採用與[PPC相同的](../Page/PPC.md "wikilink")[RISC處理器](../Page/RISC.md "wikilink")。採用的處理器廠商包括[德州儀器及](../Page/德州儀器.md "wikilink")[英特爾](../Page/英特爾.md "wikilink")。其擴充能力亦大為增強，[紅外線與](../Page/紅外線.md "wikilink")[藍芽成為基本配備](../Page/藍芽.md "wikilink")，大部分型號採用彩色顯示屏，並設有[SD卡擴充槽](../Page/SD.md "wikilink")，後期推出用[Windows
Mobile作業系統的](../Page/Windows_Mobile.md "wikilink")[智能手機](../Page/智能手機.md "wikilink")[Treo
Pro](../Page/Treo_Pro.md "wikilink")
更有[AGPS及](../Page/AGPS.md "wikilink")[Wifi](../Page/Wifi.md "wikilink")。

2004年11月2日 PalmOne发布Tungsten T5
掌上电脑，拥有256M[快閃記憶體](../Page/快閃記憶體.md "wikilink")，是當時市场上存储量最大的掌上电脑的两倍。

2005年1月17日 PalmOne Tungsten T5
获得业界权威IT门户网站[硅谷动力](../Page/硅谷动力.md "wikilink")“2004
用户最关注IT 产品评选” 铂金奖。

2005年5月 PalmOne收回了Palm商标的使用权同时正式更名回 Palm,Inc.，并且发布了新的徽标。

2005年5月，PalmOne公佈採用微型[硬碟](../Page/硬碟.md "wikilink")，擁有4GB存儲容量的[LifeDrive](../Page/LifeDrive.md "wikilink")，企圖搶佔多媒體播放器市場。

2005年10月12日
Palm公佈兩部新型號：[TX與](../Page/TX.md "wikilink")[Z22](../Page/Z22.md "wikilink")，在未來所有新型號不再使用[Tungsten與](../Page/Tungsten.md "wikilink")[Zire等名稱](../Page/Zire.md "wikilink")。

2009年1月9日
Palm於[CES公佈其新一代作業系統](../Page/CES.md "wikilink")[webOS](../Page/webOS.md "wikilink")（之前代號Nova）及新機[Palm
Pre](../Page/Palm_Pre.md "wikilink")。

2010年4月 Palm智能手机市场在受[iPhone](../Page/iPhone.md "wikilink"),
[Android和](../Page/Android.md "wikilink")[RIM的冲击下极具萎缩](../Page/Research_In_Motion.md "wikilink")，严重缺乏资金，不得已宣布出售其所有公司业务。最终惠普（HP）以12亿美元的巨资购得Palm。

## Palm所推出產品

（Windows Mobile手機多由[HTC代工](../Page/HTC.md "wikilink")）

|             |                                                                                 |                                                   |
| ----------- | ------------------------------------------------------------------------------- | ------------------------------------------------- |
| **發報日期**    | **型號**                                                                          | **作業系統**                                          |
| 1996年3月     | Pilot 1000，Pilot 5000                                                           |                                                   |
| 1997年3月     | PalmPilot Professional、PalmPilot Personal                                       |                                                   |
| 1998年3月     | Palm III                                                                        |                                                   |
| 1999年2月     | Palm IIIx、Palm V                                                                |                                                   |
| 1999年5月     | Palm VII                                                                        |                                                   |
| 1999年7月     | Palm IIIe                                                                       |                                                   |
| 2000年2月     | Palm IIIc                                                                       |                                                   |
| 2000年8月     | Palm VIIx，m100                                                                  |                                                   |
| 2001年3月     | Palm m105，m500，m505                                                             |                                                   |
| 2001年9月     | Palm m125                                                                       |                                                   |
| 2002年1月     | Palm i705                                                                       |                                                   |
| 2002年3月     | Palm m130，m515                                                                  |                                                   |
| 2002年10月    | [Tungsten](../Page/Tungsten.md "wikilink") T，[Zire](../Page/Zire.md "wikilink") |                                                   |
| 2003年2月     | Tungsten W                                                                      |                                                   |
| 2003年4月     | Tungsten C、Zire 71                                                              |                                                   |
| 2003年7月     | Tungsten T2                                                                     |                                                   |
| 2003年10月    | [Tungsten T3](../Page/Tungsten_T3.md "wikilink")、Tungsten E                     |                                                   |
| 2003年11月    | Treo 600                                                                        | Palm OS 5.4                                       |
| 2004年4月     | Zire 72、Zire 31                                                                 | Palm OS 5.4                                       |
| 2004年10月    | Tungsten T5，[Treo 650](../Page/Treo_650.md "wikilink")                          | Palm OS 5.4                                       |
| 2005年4月     | Tungsten E2                                                                     | Palm OS 5.4                                       |
| 2005年5月     | LifeDrive                                                                       | Palm OS 5.4                                       |
| 2005年7月     | Treo 700W                                                                       | Windows Mobile                                    |
| 2005年10月    | TX、Z22                                                                          | Palm OS 5.4                                       |
| 2006年5月     | Treo 700P                                                                       | Palm OS 5.4                                       |
| 2006年9月     | Treo 700Wx                                                                      | Windows Mobile                                    |
| 2006年9月     | Treo 750V                                                                       | Windows Mobile                                    |
| 2006年11月    | Treo 680                                                                        | Palm OS 5.4.9                                     |
| 2007年9月     | Palm Centro                                                                     | Palm OS 5.4.9                                     |
| 2008年7月15日  | Palm Treo800w                                                                   | Windows Mobile CDMA版                              |
| 2008年8月20日  | Palm Treo Pro                                                                   | Windows Mobile                                    |
| 2009年1月9日   | [Palm Pre](../Page/Palm_Pre.md "wikilink")                                      | [webOS](../Page/webOS.md "wikilink")              |
| 2009年9月8日   | [Palm Pixi](../Page/Palm_Pixi.md "wikilink")                                    | [webOS](../Page/webOS.md "wikilink")              |
| 2010年1月8日   | [Palm Pre Plus](../Page/Palm_Pre_Plus.md "wikilink")                            | [webOS](../Page/webOS.md "wikilink")              |
| 2010年1月8日   | [Palm Pixi Plus](../Page/Palm_Pixi_Plus.md "wikilink")                          | [webOS](../Page/webOS.md "wikilink")              |
| 2011年2月9日   | [HP Pre 3](../Page/HP_Pre_3.md "wikilink")                                      | [webOS](../Page/webOS.md "wikilink")              |
| 2011年5月15日  | [HP Veer](../Page/HP_Veer.md "wikilink")                                        | [webOS](../Page/webOS.md "wikilink")              |
| 2018年10月16日 | [Palm](../Page/Palm.md "wikilink")                                              | [Android 8.1](../Page/Android_Oreo.md "wikilink") |

## 參見

  - [Graffiti 英文手寫輸入法](../Page/Graffiti_英文手寫輸入法.md "wikilink")
  - [掌上电脑](../Page/掌上电脑.md "wikilink")
  - [Apple Newton](../Page/Apple_Newton.md "wikilink")
  - [Android](../Page/Android.md "wikilink")
  - [PalmSource](../Page/PalmSource.md "wikilink")，2005年由[日本](../Page/日本.md "wikilink")[愛可信公司以將近](../Page/愛可信公司.md "wikilink")2.243億美元的現金所收併。

## 参考资料

## 外部链接

  -
  - [Palm,Inc.](https://web.archive.org/web/20030926090907/http://www.palm.com/)

  - [PalmSource,Inc.](http://www.palmsource.com)

  - [PalmGear专业的Palm软件共享网站](http://www.palmgear.com)

  - [吹友吧，包括Palm旧机型及webOS机型的大量讨论](http://www.treo8.com/)

  - [MAXPDA，Palm，黑莓论坛](https://web.archive.org/web/20071111140516/http://bbs.maxpda.com/)

[fr:Palm, Inc.](../Page/fr:Palm,_Inc..md "wikilink")

[Category:个人数码助理](../Category/个人数码助理.md "wikilink")
[Category:Palm公司](../Category/Palm公司.md "wikilink")
[Category:惠普](../Category/惠普.md "wikilink")
[Category:行動電話製造商](../Category/行動電話製造商.md "wikilink")
[Category:Palm公司](../Category/Palm公司.md "wikilink")
[P](../Category/森尼韋爾公司.md "wikilink")
[Category:1992年加利福尼亞州建立](../Category/1992年加利福尼亞州建立.md "wikilink")

1.  <http://www.usatoday.com/tech/news/2010-04-28-hp-palm_N.htm>
2.  [Palm
    品牌回归，但首款产品的定位竟是台备用手机](https://cn.engadget.com/2018/10/15/palm-is-back-hands-on/)
3.  [新Palm
    Phone入手前必知4大重點！3.3吋迷你手機可連iPhone](https://ezone.ulifestyle.com.hk/article/2184563/%E6%96%B0+Palm+Phone+%E5%85%A5%E6%89%8B%E5%89%8D%E5%BF%85%E7%9F%A5+4+%E5%A4%A7%E9%87%8D%E9%BB%9E%EF%BC%81+3.3+%E5%90%8B%E8%BF%B7%E4%BD%A0%E6%89%8B%E6%A9%9F%E5%8F%AF%E9%80%A3+iPhone)
4.  [Palm Phone can now be purchased as a standalone Android phone for
    $199](https://9to5google.com/2019/04/04/palm-phone-standalone-android/)