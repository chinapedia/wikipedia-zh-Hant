**Para
Para**（，或Para-Para、ParaPara，注意：r以l音代之）是一種在1980年代開始在[日本發展](../Page/日本.md "wikilink")、並在1990年代時逐漸向其他周邊國家散播的舞蹈形式。這種舞蹈的特徵是以手部動作為主，除了簡單腳步的移動之外，很少有下半身的動作。由於這種特徵與日本在[盂蘭節所跳的傳統](../Page/盂蘭節.md "wikilink")[民族舞蹈](../Page/民族舞蹈.md "wikilink")「[-{盆踊}-](../Page/盆踊.md "wikilink")」相當類似，有人認為Para
Para就有如現代版的-{盆踊}-一般，故又稱之為「-{**平成的盆踊**}-」（起因於此熱潮是在進入[平成時代開始興盛](../Page/平成時代.md "wikilink")）。Para
Para的舞蹈音樂以Techno及Eurobeat等[電子舞曲為主](../Page/電子音樂.md "wikilink")。

## 歷史

Para
Para的舞步起源眾說紛紜，最有力的有一說是源自於1970年代末期日本的流行團體性表演活動[竹之子族](../Page/竹之子族.md "wikilink")；但是一直到1990年代末期才在日本以外的地區（尤其是[亞太地區](../Page/亞太地區.md "wikilink")）流行起來。而Para
Para的名稱由來是手部揮舞動作有時像落葉，因而得名。

隨著舞廳、[俱樂部的發達](../Page/俱樂部.md "wikilink")，Para
Para在[迪斯可全盛期的](../Page/迪斯可.md "wikilink")1980年代蔚為風潮。而1990年代末期，日本偶像[木村拓哉在熱門電視節目](../Page/木村拓哉.md "wikilink")《[SMAP×SMAP](../Page/SMAP×SMAP.md "wikilink")》中跳起Para
Para，帶動了日本全國Para Para的流行，可說是Para Para第三次大流行的濫觴。坊間出現了傳授Para
Para的舞蹈教室，也有許多專為Para
Para而製作的影音產品問世。許多藝人都將這種舞蹈運用在自己的表演之中，甚至有人組成了專門表演Para
Para的團體；連[東京迪士尼樂園的](../Page/東京迪士尼樂園.md "wikilink")[米老鼠也加入Para](../Page/米老鼠.md "wikilink")
Para的行列。

在這一波流行中，電視遊戲公司[KONAMI推出了一系列名為](../Page/KONAMI.md "wikilink")「ParaPara
Paradise」（簡稱PPP）的[Bemani遊戲機](../Page/Bemani.md "wikilink")，更助長了Para
Para的發展；隨著ParaPara Paradise在亞太地區的流行，Para Para也開始在日本以外的地區蔚為風行。在ParaPara
Paradise遊戲機上，在左、右、前、左前及右前等五個位置設有動態偵測感應器，用來感應玩家的手部動作。玩遊戲機時玩家站在八角形平台上，當螢幕上的箭頭到達指定位置時，玩家須對著適當方向揮動手臂，遊戲機會透過感應器判斷玩家的手部位置是否正確並與以評價，達到一定的標準才算過關。此遊戲機在2000年問世，在日本和[香港都相當風靡](../Page/香港.md "wikilink")。

## 香港的Para Para

[HK_North_Point_Piers_北角碼頭_night_街坊_dancing_visitors_June-2012.JPG](https://zh.wikipedia.org/wiki/File:HK_North_Point_Piers_北角碼頭_night_街坊_dancing_visitors_June-2012.JPG "fig:HK_North_Point_Piers_北角碼頭_night_街坊_dancing_visitors_June-2012.JPG")街坊
Para Para 集體運動\]\]

  - 2001年，Para
    Para在[香港興起過一陣熱潮](../Page/香港.md "wikilink")。幾乎每逢週末晚上，也有Para
    Para愛好者在[尖沙咀](../Page/尖沙咀.md "wikilink")[香港文化中心的廣場聚集練習](../Page/香港文化中心.md "wikilink")，最高峰時，練舞者的數目曾多達百人以上。不過，由於無人替此大規模集會提交正式申請，所以警方曾多次到場驅散，終止活動。事實上，藝術愛好者只要循正式途徑作出申請，便能租用該廣場，唯
    Para Para 聚會人數實在太多，故一直無法申請成功。2003年，Para Para在香港的熱潮已過，愛好者的聚會數目亦愈來愈少。
  - 2001年，香港ParaPara舞蹈總會 (Hong Kong Para Para Dance
    Association/HKPPDA)\]成立。稍後亦設立了Facebook專頁。及後每年的11月左右會舉行一年一度的「香港
    ParaPara 教學週年慶祝活動」，此項活動延續至今仍繼續舉行(香港ParaPara教學十二週年活動)。
  - 2011年，LoVin'ToNIte於1月1日成立，並於每月第三個星期六舉辦ParaPara活動「LoVin'ToNIte」。該組織亦於不同的場合舉行ParaPara活動，諸如小學、社區會堂及持牌Disco等。創辦兼主理人DJ
    News熟悉日本ParaPara文化，與日本交流頻繁。於2012年9月29日更衝出香港，於臺灣臺南舉行LoVin'ToNIte@臺南。[LoVin'ToNIte@台南.jpg](https://zh.wikipedia.org/wiki/File:LoVin'ToNIte@台南.jpg "fig:LoVin'ToNIte@台南.jpg")
  - 2011年，12月17日，ParaPara舞蹈組織C4
    SQuare舉辦「快閃@旺角行人專用區」，活動內容是預先公佈歌曲，以表演形式於鬧市表演ParaPara，經過8個月時間完成5次活動。直至2012年的7月止，暫時無再舉辦類似活動。
  - 2012年，5月5日，由Paralist YGT所舉辦的「香港Ori-Para活動」誕生，於活動內教授香港原創ParaPara舞步。
  - 2012年，6月3日，LoVin'ToNIte分支TechPara'ToNIte舉辦第一個以TechPara為主的活動。
  - 2012年，6月27日，集合香港活躍Paralist組成的ParaPara表演團體「Fantastic-P」成立。
  - 2013年, 2月2日，日本著名ParaPara/TechPara團體「SEF
    DELUXE」應LoVin'ToNIte邀請來港舉辦「SEF DX 香港Tour 2013」。

日本SEF DELUXE來賓包括：
DJ：DJ GUN
Dancer：MANAMI まなみ
Dancer：KAIHEI かいへー

## 參见

  - 《[芭拉芭拉樱之花](../Page/芭拉芭拉樱之花.md "wikilink")》：一部关于 Para Para 的电影
  - [ParaParaParadise
    遊戲模擬器](http://www.parapara.org/details/download.html)：一個在電腦上使用的
    [ParaParaParadise](../Page/ParaParaParadise.md "wikilink")
    遊戲[模擬器](../Page/模擬器.md "wikilink")。

## 外部連結

  - [香港PARAPARA舞蹈總會](http://www.parapara.org)
  - [PARAPARA動画](http://parapara-doga.seesaa.net/)

[Category:日本舞蹈](../Category/日本舞蹈.md "wikilink")
[Category:次文化](../Category/次文化.md "wikilink")