**Photo Booth** 是[苹果电脑公司开发的运行在](../Page/苹果电脑公司.md "wikilink")[Mac OS
X与](../Page/Mac_OS_X.md "wikilink")[iOS上的一个小型](../Page/iOS.md "wikilink")[应用程序](../Page/应用程序.md "wikilink")，主要功能是通过摄像头[iSight进行数码拍照](../Page/iSight.md "wikilink")。它有17种内置特效作用于拍摄的照片上。该程序内置于[iMac
G5](../Page/iMac_G5.md "wikilink")，[iMac](../Page/iMac.md "wikilink")，[MacBook](../Page/MacBook.md "wikilink")，[iPad和](../Page/iPad.md "wikilink")[MacBook
Pro](../Page/MacBook_Pro.md "wikilink")
[个人电脑上](../Page/个人电脑.md "wikilink")，这些机器均有内置的iSight摄像头来拍照。
Photo Booth 已成為
[苹果电脑公司的](../Page/苹果电脑公司.md "wikilink")[操作系统](../Page/操作系统.md "wikilink")——[Mac
OS X v10.5](../Page/Mac_OS_X_v10.5.md "wikilink") 花豹(Leopard)
及iOS4以後版本的一部份。

该程序可以与[即时通讯程序联动](../Page/即时通讯.md "wikilink")，如OS
X系统内置的[iMessage软件](../Page/iMessage.md "wikilink")，或[Adium](../Page/Adium.md "wikilink")。

## 技术

Photo Booth启动之后可以输出显示内置iSight摄像头的图像，在“动态输出”模式下主要有以下三种功能。

  - 拍照（输出屏幕下方一个画有照相机的红色按钮）
  - 转入第一组效果
  - 转入第二组效果

当拍 好的照片显示出来后，还有两个新功能：

  - 通过电子邮件发送照片（麦金塔电脑的[Mail程序](../Page/Mail.md "wikilink")）
  - 添加图片到 [iPhoto](../Page/iPhoto.md "wikilink") 照片库中

Photo Booth 拍摄的照片是iSight标准[解析度](../Page/解析度.md "wikilink")，即
1280x720或iPad的640×480[像素](../Page/像素.md "wikilink")，但一般图像大小在100
[KB左右](../Page/KB.md "wikilink")。在iOS5中，图像储存在“相机胶卷”中，而在[Mac OS X
Tiger中](../Page/Mac_OS_X_v10.4.md "wikilink")，图像保存在 `~/Pictures/Photo
Booth/`，其中 \~ 代表登陆中用户的文件夹。

Photo Booth的技术在苹果公司的下一代操作系统[OS X Mountain
Lion中将得到极大改进](../Page/OS_X_Mountain_Lion.md "wikilink")。用户可以在[FaceTime中使用Photo](../Page/FaceTime.md "wikilink")
Booth的效果等等。

## 效果

Photo
Booth拥有两组图像效果，可以在拍照的时候应用。第一组效果包括类似于[Photoshop的图形滤镜](../Page/Adobe_Photoshop.md "wikilink")：

  - 老照片
  - 黑白
  - 光輝
  - 漫画
  - 正常
  - 彩色铅笔
  - 红外热镜头（并不是真的温度分布图像）
  - X光
  - 流行艺术

[Photo_booth_bulge_effect.jpg](https://zh.wikipedia.org/wiki/File:Photo_booth_bulge_effect.jpg "fig:Photo_booth_bulge_effect.jpg")
[Photo_booth_mirror_effect.jpg](https://zh.wikipedia.org/wiki/File:Photo_booth_mirror_effect.jpg "fig:Photo_booth_mirror_effect.jpg")
第二组效果被苹果公司[CEO](../Page/CEO.md "wikilink")
[斯蒂夫·乔布斯](../Page/斯蒂夫·乔布斯.md "wikilink") 笑称为
"少年效果"，不是通过滤镜，而是通过改变图像形状进行变形扭曲等等

  - 膨胀
  - 凹陷
  - 卷曲
  - 挤压
  - 正常
  - 镜像
  - 轻度隧道
  - 鱼眼
  - 延伸

Photo Booth 也有自动左右翻转功能，因为iSight摄像头抓取的是镜像。

## 参见

  - [iMac](../Page/iMac.md "wikilink")
  - [MacBook](../Page/MacBook.md "wikilink")
  - [MacBook Pro](../Page/MacBook_Pro.md "wikilink")
  - [iSight](../Page/iSight.md "wikilink")
  - [iPad 2](../Page/iPad_2.md "wikilink")

## 外部链接

  - [Apple's Description of the iSight and Photo Booth capabilities on
    the
    iMac](https://web.archive.org/web/20061017103420/http://www.apple.com/imac/isight.html)

[Category:蘋果公司軟體](../Category/蘋果公司軟體.md "wikilink")
[Category:MacOS軟體](../Category/MacOS軟體.md "wikilink")
[Category:IOS软件](../Category/IOS软件.md "wikilink")