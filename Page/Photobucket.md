**Photobucket**是一個提供[影像寄存](../Page/影像寄存服務.md "wikilink")、[影片分享](../Page/影片分享網站.md "wikilink")、[幻燈片製作與](../Page/幻燈片.md "wikilink")[照片分享服務網站](../Page/照片.md "wikilink")，於2003年由Alex
Welch與Darren Crystal利用Trinity
Ventures提供的資金創立\[1\]\[2\]。Photobucket一般會用作個人[相册](../Page/相簿.md "wikilink")，寄存[網路論壇上顯示的頭像以及影像的遠端存放空間](../Page/網路論壇.md "wikilink")。

Photobucket上寄存的影像一般會用於[eBay](../Page/eBay.md "wikilink")、[MySpace](../Page/MySpace.md "wikilink")、Bebo與[Facebook帳號](../Page/Facebook.md "wikilink")，以及[LiveJournals或其他](../Page/LiveJournal.md "wikilink")[網誌與留言板](../Page/網誌.md "wikilink")，網站提供用戶500[MB免費儲存容量](../Page/Megabyte.md "wikilink")（付費帳戶為10[GB](../Page/Gigabyte.md "wikilink")）及25GB免費[頻寬](../Page/頻寬.md "wikilink")（付費帳戶無限制）。Photobucket現有的服務條款禁止[裸露內容](../Page/裸體.md "wikilink")\[3\]。Photobucket支援[FTP](../Page/FTP.md "wikilink")，但使用者需為Pro帳號所有人\[4\]。網頁發佈精靈的支援是FTP另一選擇，免費帳號亦可使用\[5\]。

根據Nielsen/Netratings公司的說法，Photobucket被評為2005年發展最迅速的網站，另據ComScore的說法現時其流量位列於首50名之內\[6\]。截至2007年5月10日為止Photobucket聲稱已有超過28億張圖像上傳到其網站。

## 發展

2007年3月Photobucket擁有美國網絡攝影網站瀏覽量市佔率41.4%\[7\]
[財星雜誌於](../Page/財星雜誌.md "wikilink")2007年3月28日報道**Photobucket**擁有3600萬名註册用戶，每日有8萬5000名新用戶加入。財星雜誌聲稱每月瀏覽**Photobucket**的用戶比瀏覽[Facebook的為多](../Page/Facebook.md "wikilink")，其中56%用戶年齡在35歲以下，52%為女性\[8\]。根據[TechCrunch的說法現有](../Page/TechCrunch.md "wikilink")30萬個獨立網站鏈接到**Photobucket**\[9\]。

**Photobucket**的[投資銀行](../Page/投資銀行.md "wikilink")[雷曼兄弟控股公司估計該公司市值約為](../Page/雷曼兄弟控股公司.md "wikilink")3至4億美元或更多\[10\]。

## 被新聞集團收購

根據2007年5月30日的報道[新聞集團已收購了Photobucket](../Page/新聞集團.md "wikilink")，雙方並沒有披露交易細節。\[11\]不過，新聞集團已在2009年把Photobucket出售給Ontela。
\[12\]

## 參見

  - [影像寄存服務](../Page/影像寄存服務.md "wikilink")
  - [網路硬碟](../Page/網路硬碟.md "wikilink")

## 參考

<references/>

## 外部連結

  - [官方網站](http://www.photobucket.com/)
  - [官方網誌](http://blog.photobucket.com/)
  - [Photobucket Leads
    Photosharing](https://web.archive.org/web/20070621195418/http://weblogs.hitwise.com/leeann-prescott/2006/06/photobucket_leads_photo_sharin.html)於網誌Hitwise
  - [Interview with Darren Crystal, CTO and Co-Founder of
    Photobucket](http://www.techrockies.com/fullstory/0004550.html)於techrockies.com
  - [匿名访问PhotoBucket](https://web.archive.org/web/20070715180546/http://www.iwantsurf.com/photo/)

[Category:网络相册](../Category/网络相册.md "wikilink")
[Category:2003年成立的公司](../Category/2003年成立的公司.md "wikilink")

1.

2.

3.

4.

5.

6.

7.  <http://www.pcworld.com/article/id,130488-pg,1/article.html>

8.  [財星雜誌](http://money.cnn.com/2007/03/27/magazines/fortune/fastforward_photobucket.fortune/?postversion=2007032808)


9.  <http://www.techcrunch.com/2007/03/29/how-much-is-photobucket-worth/>
    TechCrunch

10.
11.

12.