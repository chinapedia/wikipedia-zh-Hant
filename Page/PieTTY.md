**PieTTY**是由台灣程式設計師[林弘德](../Page/林弘德.md "wikilink")（PTT BBS
id：piaip）以[PuTTY原始碼為基礎](../Page/PuTTY.md "wikilink")，在[Windows上發展的Telnet](../Page/Windows.md "wikilink")/SSH安全遠端連線程式，修正與完整支援亞洲語系字元，可切換多種[Unicode字元顯示方式](../Page/Unicode.md "wikilink")，提供簡易scp上傳界面，並增加透明視窗、無邊框模式等視覺效果。

PieTTY與PuTTY同樣採用[MIT許可證](../Page/MIT許可證.md "wikilink")，但PieTTY目前並沒有釋出原始碼，如果有非常高度的安全需求，應自行斟酌是否使用PieTTY。

## 外部連結

  - [PieTTY
    project](https://web.archive.org/web/20050617040705/http://ntu.csie.org/~piaip/pietty/)
  - [PuTTY: A Free Telnet/SSH
    Client](http://www.chiark.greenend.org.uk/~sgtatham/putty/)
  - [PuTTY DBCS
    patched](https://web.archive.org/web/20050802091304/http://www.mhsin.org/putty/)
  - [中文的SSH client for
    windows](http://freebsd.sinica.edu.tw/%7Estatue/zh-tut/ssh.html)

## 参见

  - [计算机软件列表](../Page/计算机软件列表.md "wikilink")

[Category:自由软件](../Category/自由软件.md "wikilink")
[Category:网路管理软件](../Category/网路管理软件.md "wikilink")