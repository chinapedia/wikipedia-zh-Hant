**Pike**是一種高階，跨平台的[電腦](../Page/電腦.md "wikilink")[程式語言](../Page/程式語言.md "wikilink")，其語法與[C語言相近](../Page/C語言.md "wikilink")。Pike是屬於[自由軟體](../Page/自由軟體.md "wikilink")，它在[GPL](../Page/GPL.md "wikilink")、[LGPL以及](../Page/LGPL.md "wikilink")[MPL之下發表](../Page/MPL.md "wikilink")。

## 参见

  -
## 参考资料

## 外部链接

  -
  - [社区页面](http://www.gotpike.org/)

[Category:跨平台軟體](../Category/跨平台軟體.md "wikilink")
[Category:自由編譯器與直譯器](../Category/自由編譯器與直譯器.md "wikilink")
[Category:面向对象的编程语言](../Category/面向对象的编程语言.md "wikilink")
[Category:脚本语言](../Category/脚本语言.md "wikilink")
[Category:使用GPL许可证的软件](../Category/使用GPL许可证的软件.md "wikilink")
[Category:使用LGPL许可证的软件](../Category/使用LGPL许可证的软件.md "wikilink")
[Category:使用MPL许可证的软件](../Category/使用MPL许可证的软件.md "wikilink")