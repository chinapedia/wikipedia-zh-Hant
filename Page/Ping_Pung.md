**Ping
Pung**，一支[香港樂隊](../Page/香港.md "wikilink")，於2004年成立，曾簽約[金牌娛樂](../Page/金牌娛樂.md "wikilink")，現已解散。Ping
Pung成員包括[吳雨霏](../Page/吳雨霏.md "wikilink")（Kary
Ng）、[李漢](../Page/李漢文.md "wikilink")（Jerry
Lee）、[李嘉文](../Page/李嘉文.md "wikilink")（Jan Lee）及黃天豪（Tin
Ho）。吳雨霏為Ping
Pung的主音，李漢和同李嘉文主力[作曲](../Page/作曲.md "wikilink")、[編曲和](../Page/編曲.md "wikilink")[監製](../Page/監製.md "wikilink")，而黃天豪則為Ping
Pung摔碟。其中的李嘉文和李漢是親生兄弟，亦是作曲人[雷頌德同母異父的兄弟](../Page/雷頌德.md "wikilink")。Ping
Pung的歌曲主要為自創樂曲，沒有特定的風格。Ping Pung有演出電影《戀愛初歌》

2004年，[金牌娛樂有意把旗下的三位音樂人組樂隊進軍樂壇](../Page/金牌娛樂.md "wikilink")，而主唱則在旗下女子組合[Cookies的四位成員中挑選](../Page/Cookies_\(組合\).md "wikilink")，最終確定了四人當中唱功較穩定的[吳雨霏](../Page/吳雨霏.md "wikilink")。同年，Ping
Pung正式成軍並推出首張大碟《Love &
Hate》，憑大碟上榜歌曲《我話事》、《殺她死》及《愛是最大權利》於[2004年度叱咤樂壇流行榜頒獎典禮奪得](../Page/2004年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink")「叱咤樂壇生力軍組合銀獎」。儘管成績不錯，但吳雨霏表示以後的工作均會以[Cookies為主](../Page/Cookies_\(組合\).md "wikilink")。

2005年，[Cookies暫時解散](../Page/Cookies_\(組合\).md "wikilink")，而[吳雨霏亦開始作個人發展](../Page/吳雨霏.md "wikilink")，Ping
Pung的發展亦隨即暫停。

2006年，有報導指[側田的胞妹會取代Kary成為主音](../Page/側田.md "wikilink")。

近年，[吳雨霏的個人事業穩定下來](../Page/吳雨霏.md "wikilink")，Ping Pung沒有再推出過唱片；

2009年，[吳雨霏表示Ping](../Page/吳雨霏.md "wikilink") Pung有可能會於2010年重組。

2011年1月30日，吳雨霏首次個人紅館演唱會之中，有與Ping Pung另外三位成員（Jan、Jerry、DJ
天豪）一齊表演《殺她死》、《潮爆》歌曲。

而創作《愛是最大權利》《二十世紀少年》等熱作的香港知名音樂人李漢( Jerry Lee
，原名李漢文)，轉戰幕後繼續為香港樂壇作曲，編曲及當唱片監製等多項，是黃柏高金牌大風的旗下創作人之外，還與黎明，陳慧琳，古巨基，鄭秀文，許志安，楊千嬅，容祖兒，譚詠麟，林海峰等等多位知名藝人合作。

## 音樂作品

### 專輯

  - [Love &
    Hate](../Page/Love_&_Hate_\(Ping_Pung專輯\).md "wikilink")（2004年8月20日）

### 其他歌曲

  - 沒有那一天（收錄於《點亮2005》，2005年2月1日）
  - 講清楚（收錄於《Love 05情歌全K集》，2006年1月6日）
  - 戰鬥（收錄於《[戀愛初歌](../Page/戀愛初歌.md "wikilink")》電影原聲大碟，2006年8月15日）

## 獎項

  - [新城勁爆頒獎禮](../Page/新城勁爆頒獎禮.md "wikilink")2004 ── 新人巡禮最佳表現新人組合獎：Ping
    Pung
  - [新城勁爆頒獎禮](../Page/新城勁爆頒獎禮.md "wikilink")2004 ── 新城勁爆新人王（組合）：Ping
    Pung
  - [新城勁爆頒獎禮](../Page/新城勁爆頒獎禮.md "wikilink")2004 ── 新城勁爆卡拉OK歌曲（Ping Pung
    - 愛是最大權利）
  - [叱吒樂壇流行榜頒獎典禮](../Page/叱吒樂壇流行榜頒獎典禮.md "wikilink")2004 ──
    叱咤樂壇生力軍組合銀獎：Ping Pung
  - [第二十七屆十大中文金曲頒獎音樂會](../Page/第二十七屆十大中文金曲得獎名單.md "wikilink")2004 –
    最有前途新人獎（組合）銀獎 ：Ping Pung

## 派台歌曲成績

| **派台歌曲成績**  |
| ----------- |
| 唱片          |
| **2004**    |
| Love & Hate |
| Love & Hate |
| Love & Hate |
| Love & Hate |
| Love 05 情歌集 |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **0**       |

[Category:香港演唱團體](../Category/香港演唱團體.md "wikilink")
[Category:香港搖滾樂團](../Category/香港搖滾樂團.md "wikilink")