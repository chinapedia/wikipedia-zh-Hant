**PlanetMath**是一本自由、協作的網絡[數學](../Page/數學.md "wikilink")[百科全書](../Page/百科全書.md "wikilink")。強調[同行評審](../Page/同行評審.md "wikilink")、嚴密、公開、具教育性、實時內容、內容互連、以及群體協作。PlanetMath的目標是成為一本綜合性的網絡[數學](../Page/數學.md "wikilink")[百科全書](../Page/百科全書.md "wikilink")。該企劃位於[弗吉尼亚理工学院暨州立大学的](../Page/弗吉尼亚理工学院暨州立大学.md "wikilink")[數碼圖書館](../Page/數碼圖書館.md "wikilink")[研究實驗室](../Page/研究實驗室.md "wikilink")。

PlanetMath企劃開始於另一本網絡數學百科全書[MathWorld因](../Page/MathWorld.md "wikilink")[法院禁制令而下線的時候](../Page/法院禁制令.md "wikilink")。其時[CRC
Press控告](../Page/CRC_Press.md "wikilink")[沃夫朗研究公司](../Page/沃夫朗研究公司.md "wikilink")（Wolfram
Research）與其員工[埃立克·魏爾斯史甸](../Page/埃立克·魏爾斯史甸.md "wikilink")（MathWorld創辦人）違反合約。（詳見[MathWorld條目](../Page/MathWorld.md "wikilink")）

PlanetMath使用[知识共享 署名-相同方式共享
协议](../Page/知识共享.md "wikilink")。開始寫作一篇新文章的作者將會成為該文章的擁有者；其後他可以選擇向其他的人或小組發出更改的授權。所有的內容都使用[格式來編寫](../Page/LaTeX.md "wikilink")。[是一個受數學家歡迎的排版系統](../Page/LaTeX.md "wikilink")：它需要使用者重新學習，但能夠產生出符合數學排版需要的高質素輸出。使用者可以明確地設定到其他文章的連結，系統亦會自動將某些字詞轉換成連接到已有文章的連結。每一篇文章都會使用[美國數學學會的分類系統來分門別類](../Page/美國數學學會.md "wikilink")。

使用者可以在文章上貼上[補遺](https://www.moedict.tw/補遺)、[勘誤表](https://www.moedict.tw/勘誤表)[討論](http://www.moedict.tw/討論)。網站亦擁有一個用戶間傳遞私人訊息的系統。

以[同行評審為中心的性質使得PlanetMath的內容比起MathWorld更與眾不同](../Page/同行評審.md "wikilink")、更嚴密及更有學院味道。

運行PlanetMath的軟件使用了[perl來編寫](../Page/perl.md "wikilink")，並運行於[Linux及](../Page/Linux.md "wikilink")[Apache](../Page/Apache.md "wikilink")[網頁伺服器上](../Page/網頁伺服器.md "wikilink")。軟件名稱為Noösphere，使用[自由的](../Page/自由軟件.md "wikilink")[BSD許可證來發行](../Page/BSD許可證.md "wikilink")。

## 外部連結

  - [PlanetMath](http://planetmath.org)
  - [Noösphere軟件](https://web.archive.org/web/20050607002330/http://aux.planetmath.org/noosphere/)

[Category:数学网站](../Category/数学网站.md "wikilink")
[Category:在线百科全书](../Category/在线百科全书.md "wikilink")
[Category:Wiki社群](../Category/Wiki社群.md "wikilink")
[Category:教育网站](../Category/教育网站.md "wikilink")