**Plone**是一种建立在[Zope](../Page/Zope.md "wikilink")[应用服务器之上的](../Page/应用服务器.md "wikilink")[开源](../Page/开源.md "wikilink")[内容管理系统](../Page/内容管理系统.md "wikilink")。Plone是[自由软件并且设计为可扩展的](../Page/自由软件.md "wikilink")。它可以使用于[内网和](../Page/内网.md "wikilink")[外网](../Page/外网.md "wikilink")[服务器](../Page/服务器.md "wikilink")，[文档发布系统](../Page/文档管理系统.md "wikilink")，并且可以作为位置分享的实体协作[软件工具](../Page/协作式软件.md "wikilink")。

其最早由Alan Runyan、Alexander Limi與Vidar
Andersen在1999年开始，之后迅速成为流行而又强大的内容管理系统。

Plone的名字来源于一个来自[英国](../Page/英国.md "wikilink")[伯明翰](../Page/伯明翰.md "wikilink")，叫做Plone的电声乐队。

## 历史

Plone最早由Alan Runyan,Alexander Limi与Vidar Andersen在1999年开始开发。

### 版本时间轴

Plone稳定版本http://plone.org/products/plone

| 稳定版 | ISO日期      | Approx. difference in months | 备注        |
| --- | ---------- | ---------------------------- | --------- |
| 0.1 | 1999       | \-                           | Plone项目开始 |
| 1.0 | 2003-02-06 | \-                           | 第一稳定版     |
| 2.0 | 2004-03-23 | 13                           |           |
| 2.1 | 2005-09-06 | 18                           |           |
| 2.5 | 2006-09-19 | 12                           |           |
| 3.0 | 2007-08-21 | 11                           |           |
| 3.1 | 2008-05-02 | 8                            |           |
| 3.2 | 2009-02-07 | 9                            |           |
| 3.3 | 2009-08-19 | 6                            |           |
| 4.0 | 2010-09-01 | 12                           |           |
| 4.1 | 2011-08-08 | 11                           |           |
| 4.2 | 2012-07-05 | 11                           |           |
| 4.3 | 2013-04-13 | 9                            |           |

## 设计

Plone运行在[Zope应用程序服务器上](../Page/Zope.md "wikilink")，使用[Python编写](../Page/Python.md "wikilink")。

### 语言

Plone主要是用Python开发。但是在该项目中也使用其他的语言。下面是一个表，总结了在Plone中使用的语言，最新的使用情况查看网站：\[1\]

  - Python 55%
  - JavaScript（包括jQuery JavaScript框架）32%
  - XML 11%
  - 其他2%

其他类别包括CSS，XSLT等。

## 社区

### 支持

目前的清单中，在113个国家地区，有360个服务供应商。

### 基金会成员

超过70个基金会成员

### 赞助商

10个以上的赞助商提供资金支持，其中包括谷歌、OpenID基金会和CA公司。

### 典型应用

目前使用Plone的2283个网站\[2\]其中包括知名的：

1.  美国联邦调查局FBI
2.  大赦国际Amnesty International
3.  巴西政府Brazilian Government
4.  发现杂志Discover Magazine
5.  美国国家航空航天局科学NASA Science
6.  诺基亚Nokia
7.  自由软件基金会The Free Software Foundation
8.  耶鲁大学Yale University

### 附加组件

通过各公司的网站，社区的支持和分发数以千计的附加组件，但主要是通过PyPI和www.plone.org。目前在PyPI上，有2149个软件包可用于定制Plone。\[3\]

## 优点和缺点

Plone相比比其他流行的内容管理系统享有良好的安全记录。\[4\]此安全记录已导致了Plone被广泛采用。

### 注重安全

Plone被评为最安全的内容管理系统，根据Mitres CVE's CVE's via Mitre直到2012-12-01:

|           | CVE漏洞数量           |
| --------- | ----------------- |
| 技术        | 到目前为止（2012-12-01） |
| Plone     | 21 \[5\]          |
| Wordpress | 345 \[6\]         |
| Drupal    | 544 \[7\]         |
| Joomla    | 620 \[8\]         |

## 功能

这些是一些在Plone 4中可用的功能\[9\] :

这些是一些在Plone 3.0中可用的功能\[10\]：

## 参见

  - [内容管理系统](../Page/内容管理系统.md "wikilink")
  - [Diazo (software)](../Page/Diazo_\(software\).md "wikilink")
  - [Open source](../Page/Open_source.md "wikilink")
  - [内容管理系统列表](../Page/内容管理系统列表.md "wikilink")
  - [List of applications with iCalendar
    support](../Page/List_of_applications_with_iCalendar_support.md "wikilink")
  - [Zope](../Page/Zope.md "wikilink")

## 参考文献

## 外部链接

  -
  - [Demo Plone
    Now](https://web.archive.org/web/20121231145925/http://demo.plone.org/)

  - ["18 Things I Wish Were True About Plone" an essay by Plone founder
    Alexander
    Limi](http://limi.net/articles/18-things-i-wish-were-true-about-plone/)

  - [Directory of Plone services providers, case studies, and
    Plone-related news items](http://plone.net/)

  - [Introducing Plone, a
    Screencast](http://www.archive.org/details/SeanKellyIntroducingPlone)

  - [Plone demo with simple and quick
    registration](http://plonehostingdemo.nidelven-it.no)

### Reviews

[Category:內容管理系統](../Category/內容管理系統.md "wikilink")

1.
2.
3.
4.  [National Vulnerability Database, 2008-09-20, 9 records for
    Plone, 145 Drupal, 259 Joomla\!, 149 WordPress; none of the Plone
    vulnerabilities were rated
    severe.](http://web.nvd.nist.gov/view/vuln/search-results?query=plone&search_type=all&cves=on)
5.  [1](http://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=plone)
6.  [2](http://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=wordpress)
7.  [3](http://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=drupal)
8.  [4](http://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=joomla)
9.
10. [Plone 3.0功能— Plone
    CMS:开源内容管理系统](http://plone.org/products/plone/features/3.0/)