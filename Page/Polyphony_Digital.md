**Polyphony
Digital**是[索尼互動娛樂內部的](../Page/索尼互動娛樂.md "wikilink")[電子遊戲開發公司](../Page/電子遊戲.md "wikilink")，亦是[索尼全球工作室的一部份](../Page/索尼全球工作室.md "wikilink")，工作室位於[東京都](../Page/東京都.md "wikilink")。該工作室原名為**Polys
Entertainment**，在[跑車浪漫旅的成功獲得更多自主權後便更名為Polyphony](../Page/跑車浪漫旅.md "wikilink")
Digital。

該工作室最著名的是由[山內一典領導的賽車遊戲](../Page/山內一典.md "wikilink")[跑車浪漫旅系列](../Page/跑車浪漫旅.md "wikilink")，该系列已成為[PlayStation與](../Page/PlayStation.md "wikilink")[PlayStation
2上最成功的賽車遊戲系列之一](../Page/PlayStation_2.md "wikilink")，也是SCE第一方拥有的重要品牌。跑車浪漫旅系列是針對提供「[擬真](../Page/擬真.md "wikilink")」感覺的電子賽車遊戲，讓一般人可以體驗操縱及使用不能在現實中駕駛或購買的汽車來競賽。

2006年Polyphony
Digital發佈[摩托浪漫旅](../Page/Tourist_Trophy.md "wikilink")，希望將跑車浪漫旅的真實帶到摩托車比賽。

Polyphony
Digital還參與過實車開發工作，[日產GT-R的行車數據電子顯示屏就是該公司的傑作](../Page/日產GT-R.md "wikilink")，此外亦曾經跟[雪鐵龍合作開發一款概念車](../Page/雪鐵龍.md "wikilink")，稱為[雪鐵龍GT](../Page/雪鐵龍GT.md "wikilink")。

2011年7月，山內一典決定於[福岡縣開設第二間工作室](../Page/福岡縣.md "wikilink")，以分散自然災害的風險，有50名員工包括山內一典在內調往該處，並會聘請更多人手。

## 遊戲開發

  - [PlayStation](../Page/PlayStation.md "wikilink")

<!-- end list -->

  - [跑車浪漫旅](../Page/跑車浪漫旅.md "wikilink")
  - [跑車浪漫旅2](../Page/跑車浪漫旅#Gran_Turismo_2.md "wikilink")
  - [卡通賽車](../Page/卡通賽車.md "wikilink")
  - [卡通賽車2](../Page/卡通賽車2.md "wikilink")
  - [終極推進](../Page/終極推進.md "wikilink")

<!-- end list -->

  - [PlayStation 2](../Page/PlayStation_2.md "wikilink")

<!-- end list -->

  - [跑車浪漫旅3](../Page/跑車浪漫旅#Gran_Turismo_3:_A-Spec.md "wikilink")
  - [跑車浪漫旅4](../Page/GT赛车4.md "wikilink")
  - 跑車浪漫旅4 序章版
  - 跑車浪漫旅4 豐田MTRC版
  - 跑車浪漫旅4 豐田Prius版
  - 跑車浪漫旅4：日產350Z限量版
  - 跑車浪漫旅：概念車賽2001東京
  - 跑車浪漫旅：概念車賽2002東京-漢城
  - 跑車浪漫旅：概念車賽2002東京-日內瓦
  - 跑車浪漫旅For Boys
  - [摩托浪漫旅](../Page/Tourist_Trophy.md "wikilink")

<!-- end list -->

  - [PlayStation 3](../Page/PlayStation_3.md "wikilink")

<!-- end list -->

  - [跑車浪漫旅HD](../Page/跑車浪漫旅#Gran_Turismo_HD_Concept.md "wikilink")
  - [跑車浪漫旅5](../Page/跑車浪漫旅5.md "wikilink")
  - [跑車浪漫旅6](../Page/跑車浪漫旅6.md "wikilink")

<!-- end list -->

  - [PlayStation Portable](../Page/PlayStation_Portable.md "wikilink")

<!-- end list -->

  - [跑車浪漫旅](../Page/跑車浪漫旅#_Gran_Turismo_for_PSP.md "wikilink")

<!-- end list -->

  - [PlayStation 4](../Page/PlayStation_4.md "wikilink")

<!-- end list -->

  - [跑車浪漫旅:競速](../Page/跑車浪漫旅:競速.md "wikilink")

## 相關條目

  - [日產GT-R](../Page/日產GT-R.md "wikilink")
  - [雪鐵龍 GT](../Page/雪鐵龍_GT.md "wikilink")
  - [Red Bull X2010
    Prototype](../Page/Red_Bull_X2010_Prototype.md "wikilink")

## 外部連結

  - [Polyphony Digital](http://www.polyphony.co.jp/)
  - [MobyGames上的](../Page/MobyGames.md "wikilink")[Polyphony
    Digital](http://www.mobygames.com/company/polyphony-digital-inc)簡介
  - [GTChallenge.net](https://web.archive.org/web/20070404164413/http://www.gtchallenge.net/)
    - 愛好者網站
  - [GT Center EU - Gran Turismo
    Europe](https://web.archive.org/web/20070504215115/http://www.gtcenter.eu/)
    - 歐洲跑車浪漫旅網站及社區

[Category:1998年開業電子遊戲公司](../Category/1998年開業電子遊戲公司.md "wikilink")
[Category:索尼互動娛樂遊戲製作工作室](../Category/索尼互動娛樂遊戲製作工作室.md "wikilink")
[Category:日本電子遊戲公司](../Category/日本電子遊戲公司.md "wikilink")
[Category:江東區公司](../Category/江東區公司.md "wikilink")
[Category:福岡縣公司](../Category/福岡縣公司.md "wikilink")