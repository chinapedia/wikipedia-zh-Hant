<div style="font-size:90%;">

<div style="display:block;border-style: solid; border-width: 1px; border-color: #003399;vertical-align: top;width:98%; background-color:#E6F1FB;margin-bottom:10px;margin-top:5px;padding-left:5px;padding-right:4px;">

<h2 style="padding:16px; background:#CCE6FF;;color:#E6F1FB; text-align:center; font-weight:bold; font-size:120%; margin-bottom:5px;margin-top:0;margin-left:-5px;margin-right:-4px;">

<big>[世界遗产首页](../Page/Portal:世界遗产/Welcome.md "wikilink")</big>

</h2>

{{/Welcome}}

</div>

</div>

<div style="display:block;width:100%;float:left">

<div style="width:53%;display:block;float:left;">

<div style="display:block;border-style: solid; border-width: 1px; border-color: #003399;vertical-align: top;width:100%; background-color:#E6F1FB;margin-bottom:10px;padding-bottom:5px;padding-left:5px;padding-right:4px;">

<h2 style="padding:3px; background:#CCE6FF;;color:#E6F1FB; text-align:center; font-weight:bold; font-size:100%; margin-bottom:5px;margin-top:0;margin-left:-5px;margin-right:-4px;">

[相关条目](../Page/Portal:世界遗产/Featured.md "wikilink")

</h2>

{{/Featured}}


</div>

<div style="display:block;border-style: solid; border-width: 1px; border-color: #003399;vertical-align: top;width:100%; background-color:#E6F1FB;margin-bottom:10px;padding-bottom:5px;padding-left:5px;padding-right:4px;">

<h2 style="padding:3px; background:#CCE6FF;;color:#E6F1FB; text-align:center; font-weight:bold; font-size:100%; margin-bottom:5px;margin-top:0;margin-left:-5px;margin-right:-4px;">

[相关图片](../Page/Portal:世界遗产/Picture.md "wikilink")

</h2>

{{/Picture}}

</div>

<div style="display:block;border-style: solid; border-width: 1px; border-color: #003399;vertical-align: top;width:100%; background-color:#E6F1FB;margin-bottom:10px;padding-bottom:5px;padding-left:5px;padding-right:4px;">

<h2 style="padding:3px; background:#CCE6FF;;color:#E6F1FB; text-align:center; font-weight:bold; font-size:100%; margin-bottom:5px;margin-top:0;margin-left:-5px;margin-right:-4px;">

[您知道吗？](../Page/Portal:世界遗产/Dyk.md "wikilink")

</h2>

{{/Dyk}}

</div>

</div>

<div style="width:44%;display:block;float:right;">

<div style="display:block;border-style: solid; border-width: 1px; border-color: #003399;vertical-align: top;width:96%; background-color:#E6F1FB;margin-bottom:10px;padding-bottom:5px;padding-left:5px;padding-right:4px;">

<h2 style="padding:3px; background:#CCE6FF;;color:#E6F1FB; text-align:center; font-weight:bold; font-size:100%; margin-bottom:5px;margin-top:0;margin-left:-5px;margin-right:-4px;">

[热门话题](../Page/Portal:世界遗产/RecentlyFocus.md "wikilink")

</h2 >

{{/RecentlyFocus}}

</div>

<div style="display:block;border-style: solid; border-width: 1px; border-color: #003399;vertical-align: top;width:96%; background-color:#E6F1FB;margin-bottom:10px;padding-bottom:5px;padding-left:5px;padding-right:4px;">

<h2 style="padding:3px; background:#CCE6FF;;color:#E6F1FB; text-align:center; font-weight:bold; font-size:100%; margin-bottom:5px;margin-top:0;margin-left:-5px;margin-right:-4px;">

[您可以……](../Page/Portal:世界遗产/Opentask.md "wikilink")

</h2>

{{/Opentask}}

</div>

<div style="display:block;border-style: solid; border-width: 1px; border-color: #003399;vertical-align: top;width:96%; background-color:#E6F1FB;margin-bottom:10px;padding-bottom:5px;padding-left:5px;padding-right:4px;">

<h2 style="padding:3px; background:#CCE6FF;;color:#E6F1FB; text-align:center; font-weight:bold; font-size:100%; margin-bottom:5px;margin-top:0;margin-left:-5px;margin-right:-4px;">

[如何编写条目](../Page/Portal:世界遗产/Projects.md "wikilink")

</h2>

{{/Projects}}

</div>

<div style="display:block;border-style: solid; border-width: 1px; border-color: #003399;vertical-align: top;width:96%; background-color:#E6F1FB;margin-bottom:10px;padding-bottom:5px;padding-left:5px;padding-right:4px;">

<h2 style="padding:3px; background:#CCE6FF;;color:#E6F1FB; text-align:center; font-weight:bold; font-size:100%; margin-bottom:5px;margin-top:0;margin-left:-5px;margin-right:-4px;">

[世界遗产简明分类](../Page/Portal:世界遗产/Categories.md "wikilink")

</h2>

{{/Categories}}

</div>

</div>

</div>

<div style="display:block;float:left;width:100%;">

<div style="display:block;border-style: solid; border-width: 1px; border-color: #003399;vertical-align: top; background-color:#E6F1FB;margin-bottom:10px;padding-bottom:5px;padding-left:5px;padding-right:4px;width:98%; ">

<h2 style="padding:3px; background:#CCE6FF;;color:#0085FF; text-align:center; font-weight:bold; font-size:100%; margin-bottom:5px;margin-top:0;margin-left:-5px;margin-right:-4px;">

主题首页

</h2>

<center>

</center>

</div>

</div>

</div>

__NOTOC__ __NOEDITSECTION__

[社](../Category/主题首页.md "wikilink") [\*](../Category/世界遗产.md "wikilink")