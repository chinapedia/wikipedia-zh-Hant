__NOTOC__

<div style="display:block;border:1px solid #aaaaaa;vertical-align: top;width:99%; background-color:#f9f9ff;margin-bottom:10px;margin-top:5px;padding-left:5px;padding-right:4px;">

<h2 style="padding:16px; background:#468; color:white; text-align:center; font-weight:bold; font-size:160%; margin-bottom:5px;margin-top:0;margin-left:-5px;margin-right:-4px;">

**中国建筑**

</h2>

**中国建筑首页**是整合维基百科上所有关于[中国建筑文章的一个](../Page/中国建筑.md "wikilink")[主題首頁](../Page/Portal:首頁.md "wikilink")。所有关于中国建筑方面的内容都可以在这里找到。同时欢迎对中国建筑感兴趣的专家和爱好者参与相关条目的编写。您也可以加入我们的[中国建筑兴趣小组](../Page/Wikipedia:中国建筑兴趣小组维基人列表.md "wikilink")。

<h2 style="padding:10px; background:#468; color:white; text-align:center; font-weight:bold; font-size:160%; ">

**中国建筑的特色**

</h2>

东方建筑分为三大系系统，一中国系，二印度系，三回教系。中国系建筑，有数千年历史，其影响东及日本，朝鲜半岛，南及越南。

[中国传统屋顶](../Page/中国传统屋顶.md "wikilink") [斗栱](../Page/斗栱.md "wikilink")
[琉璃瓦](../Page/琉璃瓦.md "wikilink") [彩画](../Page/彩画.md "wikilink")
[和玺彩画](../Page/和玺彩画.md "wikilink")
[椽子彩画](../Page/椽子彩画.md "wikilink")
[旋子彩画](../Page/旋子彩画.md "wikilink")
[福建土楼](../Page/福建土楼.md "wikilink")
[棂星门](../Page/棂星门.md "wikilink")
[牌坊](../Page/牌坊.md "wikilink") [材](../Page/材.md "wikilink")
[大木作](../Page/大木作.md "wikilink") [柱础](../Page/柱础.md "wikilink")
[望柱](../Page/望柱.md "wikilink")

<h2 style="padding:10px; background:#468; color:white; text-align:center; font-weight:bold; font-size:160%; ">

**上古·春秋战国·国秦**

</h2>

《[考工记](../Page/考工记.md "wikilink")》 [阿房宫](../Page/阿房宫.md "wikilink")

<h2 style="padding:10px; background:#4F8; color:white; text-align:center; font-weight:bold; font-size:120%;">

**两汉**

</h2>

[未央宫](../Page/未央宫.md "wikilink") [长乐宫](../Page/长乐宫.md "wikilink")

<h2 style="padding:10px; background:#464; color:white; text-align:center; font-weight:bold; font-size:120%;">

**魏晋南北朝**

</h2>

[China-henan-luoyang-white-horse-temple-entrance-20040506.jpg](https://zh.wikipedia.org/wiki/File:China-henan-luoyang-white-horse-temple-entrance-20040506.jpg "fig:China-henan-luoyang-white-horse-temple-entrance-20040506.jpg")
[80px](../Page/文件:Hanging_Temple.jpg.md "wikilink")
[80px](../Page/文件:Bao_Sheng_Temple.jpg.md "wikilink")
[80px](../Page/文件:Jinshan_temple.JPG.md "wikilink")
[云岗石窟](../Page/云岗石窟.md "wikilink")
[鸿胪寺](../Page/鸿胪寺.md "wikilink")
[白马寺](../Page/白马寺.md "wikilink")
[嵩岳寺塔](../Page/嵩岳寺塔.md "wikilink")
[龙门石窟](../Page/龙门石窟.md "wikilink")
[莫高窟](../Page/莫高窟.md "wikilink")
[江天禅寺](../Page/江天禅寺.md "wikilink")
[保圣寺](../Page/保圣寺.md "wikilink")
[大明寺](../Page/大明寺.md "wikilink")
[玄妙观](../Page/玄妙观.md "wikilink")
[悬空寺](../Page/悬空寺.md "wikilink")

<h2 style="padding:10px; background:#464; color:white; text-align:center; font-weight:bold; font-size:120%;">

**隋唐**

</h2>

[Giant_Wild_Goose_Pagoda.jpg](https://zh.wikipedia.org/wiki/File:Giant_Wild_Goose_Pagoda.jpg "fig:Giant_Wild_Goose_Pagoda.jpg")
[佛光寺大殿](../Page/佛光寺大殿.md "wikilink") [大雁塔](../Page/大雁塔.md "wikilink")
[法门寺](../Page/法门寺.md "wikilink") [安济桥](../Page/安济桥.md "wikilink")
[五龙庙文昌阁](../Page/五龙庙文昌阁.md "wikilink")
[凌烟阁](../Page/凌烟阁.md "wikilink")
[唐昭陵](../Page/唐昭陵.md "wikilink")
[大明宫](../Page/大明宫.md "wikilink")
[临济寺](../Page/临济寺.md "wikilink")
[光孝寺](../Page/光孝寺.md "wikilink")
[大秦塔](../Page/大秦塔.md "wikilink")
[宝光寺](../Page/宝光寺.md "wikilink")
[小雁塔](../Page/小雁塔.md "wikilink")
[崇圣寺三塔](../Page/崇圣寺三塔.md "wikilink")
[风穴寺](../Page/风穴寺.md "wikilink")
[飞英塔](../Page/飞英塔.md "wikilink")
[宁波天宁寺塔](../Page/宁波天宁寺塔.md "wikilink")

<h2 style="padding:10px; background:#464; color:white; text-align:center; font-weight:bold; font-size:120%;">

**五代宋辽金**

</h2>

[The_Fugong_Temple_Wooden_Pagoda.jpg](https://zh.wikipedia.org/wiki/File:The_Fugong_Temple_Wooden_Pagoda.jpg "fig:The_Fugong_Temple_Wooden_Pagoda.jpg")
[80px](../Page/文件:Hall_of_Great_Acomplishement_in_the_Confucious_Temple_of_Pingyao.JPG.md "wikilink")
[Goddess_Temple_Jinsi.JPG](https://zh.wikipedia.org/wiki/File:Goddess_Temple_Jinsi.JPG "fig:Goddess_Temple_Jinsi.JPG")
[宋朝建筑](../Page/宋朝建筑.md "wikilink") [独乐寺](../Page/独乐寺.md "wikilink")
[龙兴寺](../Page/龙兴寺.md "wikilink") [华严寺](../Page/华严寺.md "wikilink")
[开元寺](../Page/开元寺.md "wikilink") [少林寺](../Page/少林寺.md "wikilink")
[玄妙观](../Page/玄妙观.md "wikilink") [三清殿](../Page/三清殿.md "wikilink")
[應縣木塔](../Page/應縣木塔.md "wikilink") [北京天宁寺](../Page/北京天宁寺.md "wikilink")
[天开塔](../Page/天开塔.md "wikilink")
[忏悔正慧大师灵塔](../Page/忏悔正慧大师灵塔.md "wikilink")
[三影塔](../Page/三影塔.md "wikilink") [六和塔](../Page/六和塔.md "wikilink")
[兴圣教寺塔](../Page/兴圣教寺塔.md "wikilink")
[北寺塔](../Page/北寺塔.md "wikilink")
[定县开元寺塔](../Page/定县开元寺塔.md "wikilink")
[慶林寺塔](../Page/慶林寺塔.md "wikilink")
[景州塔](../Page/景州塔.md "wikilink")
[瑞光塔](../Page/瑞光塔.md "wikilink")
[莆田广化寺](../Page/莆田广化寺.md "wikilink")
[雷峰塔](../Page/雷峰塔.md "wikilink")
[晋祠圣母殿](../Page/晋祠.md "wikilink")
[平遥文庙](../Page/平遥文庙.md "wikilink")

<h2 style="padding:10px; background:#471; color:white; text-align:center; font-weight:bold; font-size:160%; ">

**元**

</h2>

[Yuchanglou.JPG](https://zh.wikipedia.org/wiki/File:Yuchanglou.JPG "fig:Yuchanglou.JPG")
[元大都](../Page/元大都.md "wikilink") [普陀山多宝塔](../Page/普陀山多宝塔.md "wikilink")
[裕昌土楼](../Page/裕昌土楼.md "wikilink") [妙应寺](../Page/妙应寺.md "wikilink")
[居庸关云台](../Page/居庸关云台.md "wikilink")
《[梓人遗制](../Page/梓人遗制.md "wikilink")》

<h2 style="padding:10px; background:#471; color:white; text-align:center; font-weight:bold; font-size:160%; ">

**明·清**

</h2>

[TempleofHeaven-HallofPrayer.jpg](https://zh.wikipedia.org/wiki/File:TempleofHeaven-HallofPrayer.jpg "fig:TempleofHeaven-HallofPrayer.jpg")
[80px](../Page/文件:Putuo_Zongcheng_Temple.jpg.md "wikilink")

  -
    《[清工部工程做法则例](../Page/清工部工程做法则例.md "wikilink")》
    [妙应寺](../Page/妙应寺.md "wikilink")
    [居庸关](../Page/居庸关.md "wikilink")
    [万里长城](../Page/万里长城.md "wikilink")
    [紫禁城](../Page/紫禁城.md "wikilink")
    [天坛](../Page/天坛.md "wikilink")
    [社稷坛](../Page/社稷坛.md "wikilink")
    [太庙](../Page/太庙.md "wikilink")
    [智化寺](../Page/智化寺.md "wikilink")
    [法海寺大殿](../Page/法海寺.md "wikilink")
    [历代帝王庙](../Page/历代帝王庙.md "wikilink")
    [明十三陵](../Page/明十三陵.md "wikilink")
    [先农坛](../Page/先农坛.md "wikilink")
    [社稷坛享殿](../Page/社稷坛.md "wikilink")
    [孔庙](../Page/孔庙.md "wikilink")
    [武当山道观](../Page/武当山.md "wikilink")
    [苏州文庙](../Page/苏州文庙.md "wikilink")
    [长陵](../Page/长陵.md "wikilink")
    [普陀宗乘之庙](../Page/普陀宗乘之庙.md "wikilink")
    [文渊阁](../Page/文渊阁.md "wikilink")
    [文津阁](../Page/文津阁.md "wikilink")
    [佛香阁](../Page/佛香阁.md "wikilink")
    [雍和宫](../Page/雍和宫.md "wikilink")
    [正定县文庙](../Page/正定县文庙.md "wikilink")

<h2 style="padding:10px; background:#471; color:white; text-align:center; font-weight:bold; font-size:160%; ">

**现代**

</h2>

[Sun_mausoleum.jpg](https://zh.wikipedia.org/wiki/File:Sun_mausoleum.jpg "fig:Sun_mausoleum.jpg")
[Tiananmen_Square_2.jpg](https://zh.wikipedia.org/wiki/File:Tiananmen_Square_2.jpg "fig:Tiananmen_Square_2.jpg")
[Lujiazui_2016.jpg](https://zh.wikipedia.org/wiki/File:Lujiazui_2016.jpg "fig:Lujiazui_2016.jpg")
[Birds_Nest_at_Night.jpg](https://zh.wikipedia.org/wiki/File:Birds_Nest_at_Night.jpg "fig:Birds_Nest_at_Night.jpg")
[HK_Bank_of_China_Tower_View.jpg](https://zh.wikipedia.org/wiki/File:HK_Bank_of_China_Tower_View.jpg "fig:HK_Bank_of_China_Tower_View.jpg")

  -
    [南京民国建筑](../Page/南京民国建筑.md "wikilink")
    [中山陵](../Page/中山陵.md "wikilink")
    [人民大会堂](../Page/人民大会堂.md "wikilink")
    [人民英雄纪念碑](../Page/人民英雄纪念碑.md "wikilink")
    [毛主席纪念堂](../Page/毛主席纪念堂.md "wikilink")
    [北京展览馆](../Page/北京展览馆.md "wikilink")
    [重庆市人民大礼堂](../Page/重庆市人民大礼堂.md "wikilink")
    [中华世纪坛](../Page/中华世纪坛.md "wikilink")
    [东方明珠广播电视塔](../Page/东方明珠广播电视塔.md "wikilink")
    [上海环球金融中心](../Page/上海环球金融中心.md "wikilink")
    [上海中心大厦](../Page/上海中心大厦.md "wikilink")
    [国家体育场](../Page/国家体育场.md "wikilink")
    [国家游泳中心](../Page/国家游泳中心.md "wikilink")
    [深圳湾体育中心](../Page/深圳湾体育中心.md "wikilink")
    [广州塔](../Page/广州塔.md "wikilink")
    [香港中银大厦](../Page/香港中银大厦.md "wikilink")

<h2 style="padding:10px; background:#473; color:white; text-align:center; font-weight:bold; font-size:120%; ">

**中国建筑家**

</h2>

<h2 style="padding:10px; background:#475; color:white; text-align:center; font-weight:bold; font-size:120%; ">

**中国建筑文献**

</h2>

  -
    《[考工记](../Page/考工记.md "wikilink")》
    《[营造法式](../Page/营造法式.md "wikilink")》
    《[梓人遗制](../Page/梓人遗制.md "wikilink")》
    明午荣 《[鲁班经](../Page/鲁班经.md "wikilink")》
    《[清工部工程做法则例](../Page/清工部工程做法则例.md "wikilink")》
    《[清式營造則例](../Page/清式營造則例.md "wikilink")》
    清[李斗](../Page/李斗.md "wikilink")
    《[工段营造录](../Page/工段营造录.md "wikilink")》
    《[清代匠作则例](../Page/清代匠作则例.md "wikilink")》

\<h2 style="padding:10px; background:\#482; color:white;
text-align:center; font-weight:bold; font-size:120%;
\>**特色条目**[20px](../Page/image:Cscr-featured.svg.md "wikilink")

</h2>

[Goddess_Temple_Jinsi.JPG](https://zh.wikipedia.org/wiki/File:Goddess_Temple_Jinsi.JPG "fig:Goddess_Temple_Jinsi.JPG")
**[宋朝建築](../Page/宋朝建築.md "wikilink")**，泛指在960年至1279年的[宋朝時期](../Page/宋朝.md "wikilink")，於[北宋及](../Page/北宋.md "wikilink")[南宋境內的](../Page/南宋.md "wikilink")[建築](../Page/建築.md "wikilink")，一如以往的朝代，繼承著前朝的建築傳統。宋朝在[經濟](../Page/經濟.md "wikilink")、手工業和科學技術方面都有發展，使得宋代的建築師、木匠、技工、工程師、[斗栱體系](../Page/斗栱.md "wikilink")、建築構造與造型技術達到了很高的水平。建築方式也日漸趨向系統化與模組化，建築物慢慢出現了自由多變的組合，並且綻放出成熟的風格和擁有更專業的外型。宋朝建築物的類型多樣，其中傑出的建築都是[佛塔](../Page/塔.md "wikilink")、石橋、木橋、[園林](../Page/園林.md "wikilink")、[皇陵與](../Page/皇陵.md "wikilink")[宮殿](../Page/宮殿.md "wikilink")。由於注重意境的[園林設計特意追求把自然美與人工美融為一體的意境](../Page/園林.md "wikilink")，所以這一時期的建築，一改唐代雄渾的特點，變得纖巧秀麗，柔弱纖秀，曲線柔和，注重裝飾，華麗而繁細。

\<h2 style="padding:10px; background:\#482; color:white;
text-align:center; font-weight:bold; font-size:120%;
\>**你知道嗎？[<File:02wiki-zn-frontpage-icon.png>](https://zh.wikipedia.org/wiki/File:02wiki-zn-frontpage-icon.png "fig:File:02wiki-zn-frontpage-icon.png")**

</h2>

[Zhaozhou_Bridge.jpg](https://zh.wikipedia.org/wiki/File:Zhaozhou_Bridge.jpg "fig:Zhaozhou_Bridge.jpg")
**安济桥**位于[河北省](../Page/河北.md "wikilink")[赵县城南五里的](../Page/赵县.md "wikilink")[洨河上](../Page/洨河.md "wikilink")，又名**赵州桥**、**大石桥**，设计者是[隋代杰出的工匠](../Page/隋代.md "wikilink")[李春](../Page/李春_\(隋朝\).md "wikilink")，建造于[大业六年](../Page/大业_\(年号\).md "wikilink")([610年](../Page/610年.md "wikilink"))。安济桥是目前世界最古老的现存完好的大跨度单孔敞肩坦弧石拱桥。

[1961年](../Page/1961年.md "wikilink")，安济桥（大石桥）和赵县的另一座著名的圆弧拱桥[永通桥](../Page/永通桥.md "wikilink")（小石桥）、四川[泸定桥同列全国重点文物保护单位](../Page/泸定桥.md "wikilink")，成为中国重点保护的三座古桥。

[1962年中国人民邮政发行安济桥特种纪念](../Page/1962年.md "wikilink")[邮票](../Page/邮票.md "wikilink")

[1991年美国土木工程师学会](../Page/1991年.md "wikilink")（American Society of Civil
Engineers）将安济桥定为[国际土木工程历史古迹](../Page/国际土木工程历史古迹.md "wikilink")

</div>

<h2 style="padding:10px; background:#483; color:white; text-align:center; font-weight:bold; font-size:120%; ">

**主条目**

</h2>

  - [中国建筑史](../Page/中国建筑史.md "wikilink")

[Category:中国主题首页](../Category/中国主题首页.md "wikilink")
[Category:中国建筑](../Category/中国建筑.md "wikilink")
[Category:建筑主题首页](../Category/建筑主题首页.md "wikilink")