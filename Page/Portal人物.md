{{/box-header|人物主題|Portal:人物/簡介|}} {{/簡介}}
{{/box-footer|[關於人物...](../Page/人物.md "wikilink")}}

{{/box-header|分類|Portal:人物/分類|}} {{/分類}} {{/box-footer|}}

<div style="float:left; width:60%;">

{{/box-header|特色條目|Portal:人物/特色条目|}} {{/特色條目}}
{{/box-footer|[更多特色條目…](../Page/維基百科:特色條目/存檔.md "wikilink")}}

{{/box-header|特色圖片|Portal:人物/特色图片|}} {{/特色圖片}} {{/box-footer|}}

{{/box-header|你知道吗 |Portal:人物/你知道吗 |}} {{/你知道吗}} {{/box-footer|}}

</div>

<div style="float:right; width:39%">

{{/box-header|新聞人物|Portal:人物/新聞人物|}} {{/新聞人物}} {{/box-footer|}}

{{/box-header|今日人物（[日](../Page/{{CURRENTMONTHNAME}}{{CURRENTDAY}}日.md "wikilink")）|Portal:人物/日}}
{{/日}} {{/box-footer|}}

{{/box-header|歡迎參與|Portal:人物/歡迎參與|}}  {{/box-footer|}}

{{/box-header|维基工程|Portal:人物/维基工程|}} {{/維基工程}} {{/box-footer|}}

{{/box-header|逝世公告|Portal:人物/逝世公告|}} {{/逝世公告}} {{/box-footer|}}

</div>

<div style="float:right; width:100%">

{{/box-header|維基主題|Portals|}}  {{/box-footer|}}

\[ 刷新\]|[整理](../Page/Portal:人物/整理.md "wikilink")

[社](../Category/主題首頁.md "wikilink") [人物](../Category/人物.md "wikilink")