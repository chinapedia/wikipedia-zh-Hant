__NOTOC__

|                                                    |
| -------------------------------------------------- |
| **[条目提升计划](../Page/Portal:南京/提升计划.md "wikilink")** |

<div>

<table style="width:10%;">
<colgroup>
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Confucius_Temple_river.jpg" title="fig:Confucius_Temple_river.jpg">Confucius_Temple_river.jpg</a></p></td>
<td></td>
<td></td>
<td></td>
<td><div style="font-size:800%; line-height:1; font-family:华文行楷; color:#87CEEB; text-align:left">
<p>南<br />
京</p>
</div>
<div style="text-align:right; margin-right:12px; margin-bottom:1px;">
<p><strong>主题首页</strong></p>
</div></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Nanjing_University.jpg" title="fig:Nanjing_University.jpg">Nanjing_University.jpg</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Shizi_Qiao_1.jpg" title="fig:Shizi_Qiao_1.jpg">Shizi_Qiao_1.jpg</a></p></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><p>欢迎莅临<a href="../Page/南京.md" title="wikilink">南京</a>，一个被誉为六朝古都，十朝都会的历史名城<br />
一个交融了六朝金粉与<a href="../Page/民国.md" title="wikilink">民国气息却又充满活力的现代都市</a></p></td>
</tr>
</tbody>
</table>

</div>

<div>

<table>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:ChinaJiangsuNanjing.png" title="fig:ChinaJiangsuNanjing.png">ChinaJiangsuNanjing.png</a></p></td>
<td><p><a href="../Page/南京.md" title="wikilink">南京</a>，简称宁，别称金陵，“<a href="../Page/中国四大古都.md" title="wikilink">中国四大古都</a>”之一，有“六朝古都”之称。<a href="../Page/南京.md" title="wikilink">南京位于</a><a href="../Page/长江.md" title="wikilink">长江下游沿岸</a>，是长江下游地区重要的产业城市和经济中心，中国重要的文化教育中心之一，也是<a href="../Page/华东地区.md" title="wikilink">华东地区重要的交通枢纽</a>。</p>
<p><a href="../Page/南京.md" title="wikilink">南京大部为低山</a><a href="../Page/丘陵.md" title="wikilink">丘陵地形</a>。长江从西南方向流入南京，在此折向东进入<a href="../Page/镇江.md" title="wikilink">镇江</a>。<a href="../Page/秦淮河.md" title="wikilink">秦淮河</a>、<a href="../Page/滁河.md" title="wikilink">滁河分别从南北岸汇入长江</a>。南京属于北亚热带季风气候，四季分明，冬夏长而春秋短，年平均<a href="../Page/气温.md" title="wikilink">气温</a>16<a href="../Page/摄氏度.md" title="wikilink">摄氏度</a>。夏季盛行<a href="../Page/西南风.md" title="wikilink">西南风</a>，历史最高气温40.7摄氏度；冬季盛行<a href="../Page/东北风.md" title="wikilink">东北风</a>，历史最低气温-14.0摄氏度。南京雨水充沛，年平均降雨117天，一般在6月下旬至7月中旬处于阴雨连绵的梅雨季节。</p>
<p>我们非常欢迎您参与与南京相关条目的编写。您也可以加入我们的<a href="../Page/Portal:南京/提升计划.md" title="wikilink">提升计划</a>。</p>
<div style="margin-right:10px;margin-bottom:4px;">
<p><strong><a href="../Page/南京.md" title="wikilink">進一步了解南京…</a></strong></p>
</div></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Nankin_1912.jpg" title="fig:Nankin_1912.jpg">Nankin_1912.jpg</a></p></td>
</tr>
</tbody>
</table>

</div>

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><strong>推荐条目</strong></p></td>
<td><p><strong>南京新闻</strong></p></td>
</tr>
<tr class="even">
<td><div style="margin-right:4px; margin-bottom:2px;">
</div></td>
<td><div style="margin-right:2%; margin-bottom:2%;">
</div></td>
</tr>
<tr class="odd">
<td><p><strong>推荐图片</strong></p></td>
<td><p><strong>分类</strong></p></td>
</tr>
<tr class="even">
<td><div style="margin-right:2%; margin-bottom:2%;">
</div></td>
<td><div style="margin-right:4px; margin-bottom:2px;">
</div></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table>

</div>

</div>

[Category:中国各地主题首页](../Category/中国各地主题首页.md "wikilink")
[Category:江苏维基资源](../Category/江苏维基资源.md "wikilink")
[Category:南京](../Category/南京.md "wikilink")