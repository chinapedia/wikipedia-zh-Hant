`__NOTOC__ __NOEDITSECTION__`

<table style="width: 100%; -moz-border-radius: 10px 10px 10px 10px;padding: .4em .9em .9em; background:#faf7f0;" border="0" cellspacing="1" cellpadding="3" align="center">

<tr>

<td colspan="2" style="text-align:center">

歡迎來到**藝術小小殿堂**！

**什麼是藝術？**「沒有公認的[定義](../Page/定義.md "wikilink")」。**[藝術](../Page/藝術.md "wikilink")**，廣義來說，是人类透過各種形式及工具以表達其[情感與](../Page/情感.md "wikilink")[意識](../Page/意識.md "wikilink")，因而產生的結果。

**藝術小小殿堂**是維基百科提供廣大藝術愛好者的知識主題頁，從[藝術家到](../Page/藝術家.md "wikilink")[藝術史](../Page/藝術史.md "wikilink")，從[藝術品到](../Page/藝術品.md "wikilink")[藝術形式](../Page/藝術形式.md "wikilink")，從這開始您與藝術的交會吧！

<div style="text-align: center;font-size: 80%;align:middle;padding-top:15px;padding-bottom:15px;">

[Alice's_Adventures_in_Wonderland_-_Carroll,_Robinson_-_S013_-_Contents.png](https://zh.wikipedia.org/wiki/File:Alice's_Adventures_in_Wonderland_-_Carroll,_Robinson_-_S013_-_Contents.png "fig:Alice's_Adventures_in_Wonderland_-_Carroll,_Robinson_-_S013_-_Contents.png")
\[ 刷新頁面：看看不同的特色藝術家與圖片\]

</div>

<h3>

[<File:03wiki-zn-frontpage-icon.gif>](https://zh.wikipedia.org/wiki/File:03wiki-zn-frontpage-icon.gif "fig:File:03wiki-zn-frontpage-icon.gif")
特色專欄

</h3>

<table style="width: 100%; background: #ffffff;" cellspacing="3" cellpadding="3" align="center">

<tr style="vertical-align: middle; width: 11%;">

<td>

[CiceroEpistulaeAdFamiliaresVenice1547page329Detail.jpg](https://zh.wikipedia.org/wiki/File:CiceroEpistulaeAdFamiliaresVenice1547page329Detail.jpg "fig:CiceroEpistulaeAdFamiliaresVenice1547page329Detail.jpg")

</td>

<td>

[Venus,_Amor_en_Mars_Ultor.gif](https://zh.wikipedia.org/wiki/File:Venus,_Amor_en_Mars_Ultor.gif "fig:Venus,_Amor_en_Mars_Ultor.gif")

</td>

<td>

[Soffit_(PSF).png](https://zh.wikipedia.org/wiki/File:Soffit_\(PSF\).png "fig:Soffit_(PSF).png")

</td>

<td>

[Bild_Fahne_Hövel_Ausgrabung.jpg](https://zh.wikipedia.org/wiki/File:Bild_Fahne_Hövel_Ausgrabung.jpg "fig:Bild_Fahne_Hövel_Ausgrabung.jpg")

</td>

<td>

[French-horn.png](https://zh.wikipedia.org/wiki/File:French-horn.png "fig:French-horn.png")

</td>

<td>

[Cinematograf-Camera2.jpg](https://zh.wikipedia.org/wiki/File:Cinematograf-Camera2.jpg "fig:Cinematograf-Camera2.jpg")

</td>

<td>

[Vilhelm_Pedersen_B8,_ubt.jpeg](https://zh.wikipedia.org/wiki/File:Vilhelm_Pedersen_B8,_ubt.jpeg "fig:Vilhelm_Pedersen_B8,_ubt.jpeg")

</td>

<td>

[Die_Gartenlaube_(1871)_b_393.jpg](https://zh.wikipedia.org/wiki/File:Die_Gartenlaube_\(1871\)_b_393.jpg "fig:Die_Gartenlaube_(1871)_b_393.jpg")

</td>

<td>

[PSM_V21_D054_The_binocular_camera.jpg](https://zh.wikipedia.org/wiki/File:PSM_V21_D054_The_binocular_camera.jpg "fig:PSM_V21_D054_The_binocular_camera.jpg")

</td>

<td>

[Illustration_Schwob_moeurs_des_diurnales_page12.jpg](https://zh.wikipedia.org/wiki/File:Illustration_Schwob_moeurs_des_diurnales_page12.jpg "fig:Illustration_Schwob_moeurs_des_diurnales_page12.jpg")

</td>

</tr>

<tr style="vertical-align: top; width: 11%;">

<td>

**[绘画](../Page/绘画.md "wikilink")**
<span style="font-size:70%">（[分类](../Category/绘画.md "wikilink") -
[图](../Page/:Commons:Category:Paintings.md "wikilink")）</span>
<span style="font-size:80%">[油畫](../Page/油畫.md "wikilink") -
[水彩](../Page/水彩.md "wikilink") -
[水墨畫](../Page/水墨畫.md "wikilink")</span>

</td>

<td>

**[雕塑](../Page/雕塑.md "wikilink")**
<span style="font-size:70%">（[分类](../Category/雕塑.md "wikilink") -
[图](../Page/:Commons:Category:Sculptures.md "wikilink")）</span>
<span style="font-size:80%">[木雕](../Page/木雕.md "wikilink") -
[浮雕](../Page/浮雕.md "wikilink") -
[陶藝](../Page/陶藝.md "wikilink")</span>

</td>

<td>

**[建筑](../Page/建筑.md "wikilink")**
<span style="font-size:70%">（[分类](../Category/建筑.md "wikilink") -
[图](../Page/:Commons:Category:Architecture.md "wikilink")</span>）
<span style="font-size:80%">[園林](../Page/園林.md "wikilink") -
[景觀](../Page/景觀設計.md "wikilink") -
[城市规划](../Page/城市规划.md "wikilink")</span>

</td>

<td>

**[工藝](../Page/手工艺.md "wikilink")**
<span style="font-size:70%">（[分类](../Category/手工艺.md "wikilink") -
[图](../Page/:Commons:Category:Crafts.md "wikilink")）</span>
<span style="font-size:80%">[玉器](../Page/玉器.md "wikilink") -
[瓷器](../Page/瓷器.md "wikilink") -
[漆器](../Page/漆器.md "wikilink")</span>

</td>

<td>

**[音乐](../Page/音乐.md "wikilink")**
<span style="font-size:70%">（[分类](../Category/音乐.md "wikilink") -
[图](../Page/:Commons:Category:Music.md "wikilink")）</span>
<span style="font-size:80%">[器樂](../Page/器樂.md "wikilink") -
[聲樂](../Page/聲樂.md "wikilink")</span>

</td>

<td>

**[电影](../Page/电影.md "wikilink")**
<span style="font-size:70%">（[分类](../Category/电影.md "wikilink") -
[图](../Page/:Commons:Category:Cinema.md "wikilink")）</span>
<span style="font-size:80%">[電視](../Page/電視.md "wikilink") -
[廣播](../Page/廣播.md "wikilink")</span>

</td>

<td>

**[舞蹈](../Page/舞蹈.md "wikilink")**
<span style="font-size:70%">（[分类](../Category/舞蹈.md "wikilink") -
[图](../Page/:Commons:Category:Dance.md "wikilink")）</span>
<span style="font-size:80%">[現代舞](../Page/現代舞.md "wikilink") -
[芭蕾](../Page/芭蕾.md "wikilink")</span>

</td>

<td>

**[戏剧](../Page/戏剧.md "wikilink")**
<span style="font-size:70%">（[分类](../Category/戏剧.md "wikilink") -
[图](../Page/:Commons:Category:Theatre.md "wikilink")）</span>
<span style="font-size:80%">[音樂劇](../Page/音樂劇.md "wikilink") -
[歌劇](../Page/歌劇.md "wikilink") -
[電視劇](../Page/電視劇.md "wikilink")</span>

</td>

<td>

'''[攝影](../Page/攝影.md "wikilink") '''
<span style="font-size:70%">（[分类](../Category/攝影.md "wikilink") -
[图](../Page/:Commons:Category:Photography.md "wikilink")）</span>
<span style="font-size:80%">[摄影术](../Page/摄影术.md "wikilink") -
[攝影師](../Page/攝影師.md "wikilink")</span>

</td>

<td>

**其他**
<span style="font-size:80%">[文學](../Page/文學.md "wikilink") -
[設計](../Page/設計.md "wikilink") -
[飲食](../Page/飲食.md "wikilink")</span>

</td>

</tr>

</table>

注意：以上特色專欄僅呈現局部藝術的面貌，不代表完整分類。

</td>

</tr>

<tr>

<td style="vertical-align: top;width: 50%;">

<table style="vertical-align: top;width: 100%;-moz-border-radius: 10px 10px 10px 10px; background: #f8faf0;margin:5px;padding:5px;" border="0" cellspacing="1" cellpadding="3">

<tr>

<td>

</td>

</tr>

</table>

</td>

<td style="vertical-align: top;width: 50%;">

<table style="width: 100%; -moz-border-radius: 10px 10px 10px 10px;background: #eff4dd;margin:5px;padding:5px;" border="0" cellspacing="1" cellpadding="3">

<tr>

<td>

</td>

</tr>

</table>

</td>

</tr>

<tr>

<td style="vertical-align: top;width: 50%;">

<table style="width: 100%; -moz-border-radius: 10px 10px 10px 10px; background: #eff4dd;margin:5px;padding:5px;" border="0" cellspacing="1" cellpadding="3">

<tr>

<td>

</td>

</tr>

</table>

</td>

<td style="vertical-align: top;width: 50%;">

<table style="width: 100%; -moz-border-radius: 10px 10px 10px 10px;background: #f8faf0;margin:5px;padding:5px;" border="0" cellspacing="1" cellpadding="3">

<tr>

<td>

</td>

</tr>

</table>

</td>

</tr>

</table>

<h3>

目錄

</h3>

[艺术](../Category/艺术.md "wikilink") [文](../Category/主题首页.md "wikilink")