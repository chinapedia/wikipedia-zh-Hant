[Lenovo_ideapad_S12.jpg](https://zh.wikipedia.org/wiki/File:Lenovo_ideapad_S12.jpg "fig:Lenovo_ideapad_S12.jpg")s12[上網本內建的PowerDVD](../Page/上網本.md "wikilink")\]\]
**PowerDVD**是由[訊連科技開發的](../Page/訊連科技.md "wikilink")[播放軟件](../Page/播放軟件.md "wikilink")，容許使用者在[個人電腦上觀看](../Page/個人電腦.md "wikilink")[DVD-Video](../Page/DVD-Video.md "wikilink")。Ultra版支援[Blu-ray的播放](../Page/Blu-ray.md "wikilink")。

## 支援播放的光碟與檔案

### 對應光碟格式

  -
    [CD](../Page/CD.md "wikilink")（[CD-ROM](../Page/CD-ROM.md "wikilink"),
    [CD-R](../Page/CD-R.md "wikilink"),
    [CD-RW](../Page/CD-RW.md "wikilink")）
    [DVD](../Page/DVD.md "wikilink")（DVD-ROM, DVD-R, DVD-RW, DVD+R,
    DVD+RW, [DVD-RAM](../Page/DVD-RAM.md "wikilink")）
    [Blu-ray Disc](../Page/Blu-ray_Disc.md "wikilink") (BD-R, BD-RE,
    BD-ROM)
    [HD DVD](../Page/HD_DVD.md "wikilink")（HD DVD-R, HD DVD-ROM） -
    版本8以後省略不支援。
    [CPRM光碟](../Page/CPRM.md "wikilink")

### 檔案

**視訊格式**

  - [ASF](../Page/Advanced_Systems_Format.md "wikilink")
  - [AVCHD](../Page/AVCHD.md "wikilink")
  - [AVCREC](../Page/AVCREC.md "wikilink")
  - [BDAV](../Page/BDAV.md "wikilink")
  - [BDMV](../Page/BDMV.md "wikilink")
  - [DVD+VR](../Page/DVD+VR.md "wikilink")
  - [DVD-VR](../Page/DVD-VR.md "wikilink")
  - [DVD-Video](../Page/DVD-Video.md "wikilink")
  - [H.264](../Page/H.264.md "wikilink")
  - [Mini-DVD](../Page/Mini-DVD.md "wikilink")
  - [MPEG-1](../Page/MPEG-1.md "wikilink")
  - [MPEG-2](../Page/MPEG-2.md "wikilink")
  - [MPEG-2 HD](../Page/MPEG-2.md "wikilink")
  - [MPEG-4
    ASP](../Page/MPEG-4.md "wikilink")（[DivX與](../Page/DivX.md "wikilink")[Xvid等](../Page/Xvid.md "wikilink")）
  - [VC-1](../Page/VC-1.md "wikilink")
  - [WMV-HD](../Page/Windows_Media_Video.md "wikilink")
  - [VCD](../Page/VCD.md "wikilink")

**360 影片**

  - 等距柱狀投影MP4格式 (H.264/265)，2:1長寬比最大支援至4K解析度。

**3D-360 影片**

  - 雙等距柱狀投影MP4格式(H.264/265). 1:1長寬比最大支援至4K解析度。 **HDR 10 影片**: MP4, MKV,
    M2TS的H.265視訊

**音訊格式**

  - [AAC](../Page/AAC.md "wikilink")
  - [DTS 5.1](../Page/デジタル・シアター・システムズ.md "wikilink")
  - [DTS 96/24](../Page/デジタル・シアター・システムズ.md "wikilink")
  - [DTS Neo:6](../Page/デジタル・シアター・システムズ.md "wikilink")
  - [DTS-ES Discrete](../Page/デジタル・シアター・システムズ.md "wikilink")
  - [Dolby Digital EX](../Page/ドルビーデジタル.md "wikilink")
  - [DVD-Audio](../Page/DVD-Audio.md "wikilink") - 版本9以後省略不支援。
  - [MLP Lossless](../Page/MLP_Lossless.md "wikilink")
  - [MPEG Audio Layer-3](../Page/MP3.md "wikilink")
  - [WMA](../Page/Windows_Media_Audio.md "wikilink")（[Windows Media
    Audio](../Page/Windows_Media_Audio.md "wikilink")）
  - [WAV](../Page/WAV.md "wikilink")（[LPCM](../Page/LPCM.md "wikilink")）
  - [DTS](../Page/DTS.md "wikilink")
  - [CDDA](../Page/CDDA.md "wikilink")（音樂CD）
  - [DSD](../Page/DSD.md "wikilink") - 版本16以後支援。 (DFS與DFF)

**相片格式**

  - [BMP](../Page/BMP.md "wikilink")
  - [JPEG](../Page/JPEG.md "wikilink")
  - [JPG](../Page/JPG.md "wikilink")
  - [PNG](../Page/PNG.md "wikilink")
  - [TIF](../Page/TIFF.md "wikilink")
  - [TIFF](../Page/TIFF.md "wikilink")
  - MPO3
  - JPS
  - ARW
  - CR2
  - CRW
  - DCR
  - DNG
  - ERF
  - KDC
  - MEF
  - MRW
  - NEF
  - ORF
  - PEF
  - RAF
  - [RAW](../Page/RAW.md "wikilink")
  - RW2
  - SR2
  - SRF
  - X3F

## 參見

  - [媒體播放器列表](../Page/媒體播放器列表.md "wikilink")
  - [媒體播放器比較](../Page/媒體播放器比較.md "wikilink")

## 外部連結

**官方網站**

  - [官方網站](https://web.archive.org/web/20080408175642/http://tw.cyberlink.com/)
  - [官方網站](http://cn.cyberlink.com/)
  - [官方網站](https://web.archive.org/web/20110307174940/http://www.cyberlink.com/)

[Category:視窗媒體播放器](../Category/視窗媒體播放器.md "wikilink")
[Category:媒体播放器](../Category/媒体播放器.md "wikilink")
[Category:付費軟件](../Category/付費軟件.md "wikilink")
[Category:DVD](../Category/DVD.md "wikilink")