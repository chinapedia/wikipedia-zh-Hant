**PowerDesigner**是[Sybase的企业建模和设计解决方案](../Page/Sybase.md "wikilink")，采用模型驱动方法，将业务与IT结合起来，可帮助部署有效的企业体系架构，并为研发生命周期管理提供强大的分析与设计技术\[1\]。PowerDesigner独具匠心地将多种标准数据建模技术（UML、业务流程建模以及市场领先的[数据建模](../Page/数据建模.md "wikilink")）集成一体，并与
.NET、WorkSpace、PowerBuilder、Java™、Eclipse
等主流开发平台集成起来，从而为传统的软件开发周期管理提供业务分析和规范的数据库设计解决方案。PowerDesigner运行在[Microsoft
Windows平台上](../Page/Microsoft_Windows.md "wikilink")，并提供了[Eclipse插件](../Page/Eclipse.md "wikilink")\[2\]。

## 特性

PowerDesigner支持：

  - [需求分析](../Page/需求分析.md "wikilink")
  - [面向对象建模](../Page/面向对象建模.md "wikilink")（提供[UML](../Page/Unified_Modeling_Language.md "wikilink")
    2.0 所有视图）
  - [数据建模](../Page/数据建模.md "wikilink")（支持主流[關聯式資料庫管理系統](../Page/關聯式資料庫管理系統.md "wikilink")）
  - [业务过程建模](../Page/业务过程建模.md "wikilink")（ProcessAnalyst）支持
    [BPMN](../Page/BPMN.md "wikilink")
  - [XML建模](../Page/XML.md "wikilink")（支持[XML
    Schema](../Page/XML_Schema.md "wikilink") 和
    [DTD](../Page/DTD.md "wikilink") 标准）
  - [数据仓库建模](../Page/数据仓库.md "wikilink")（WarehouseArchitect）
  - [代码生成](../Page/代码生成.md "wikilink")（支持语言及框架包括：Java、C\#、VB
    .NET、Hibernate、EJB3、NHibernate、JSF、WinForm (.NET and .NET
    CF)、[PowerBuilder](../Page/PowerBuilder.md "wikilink")、……）
  - [报表生成](../Page/报表.md "wikilink")
  - [企业知识库](../Page/企业知识库.md "wikilink")
  - [Visual Studio](../Page/Visual_Studio.md "wikilink") 2005 插件
  - [Eclipse](../Page/Eclipse.md "wikilink") 插件

进一步信息： [Sybase
PowerDesigner主页](http://www.sybase.com/products/modelingmetadata/powerdesigner)。

## 历史

PowerDesigner最初由[Xiao-Yun
Wang](../Page/Xiao-Yun_Wang.md "wikilink")（[王晓昀](../Page/王晓昀.md "wikilink")）在[SDP
Technologies公司开发完成](../Page/SDP_Technologies.md "wikilink")。在法国称为AMC\*Designor，在国际市场上称为S-Designor。在这两个产品名字中都包含“or”，它实际上特指“Oracle”，因为在产品开发的最开始是为Oracle数据库设计的，但是很快就发展并支持市场上所有主流的数据库系统。[SDP
Technologies是一个建于](../Page/SDP_Technologies.md "wikilink")1983年的法国公司，1995年，Powersoft公司购买了该公司，而在1994年早期，Sybase已经买下了Powersoft公司。在这些并购之后，为了保持Powersoft的产品商标的一致，改名叫做“PowerDesigner”。
目前Sybase拥有PowerDesigner及其法文版PowerAMC的所有权利。

### 版本发展史

  - 1989年－在法国发布第一个商用版本AMC\*Designor（版本2.0）
  - 1992年－在美国发布第一个商用版本S-Designor
  - 1994年－加入ProcessAnalyst
  - 1995年－S-Designor改名为PowerDesigner，AMC\*Designor改名为[PowerAMC](../Page/PowerAMC.md "wikilink")
  - 1997年－发布PowerDesigner 6.0
  - 1998年－加入WarehouseArchitect
  - 1999年－重写PowerDesigner 7.0，以支持最新的技术并保持与Sybase其他产品一直的界面。
  - 2001年12月－发布PowerDesigner 9.5 的最初版本，并发布升级及维护版本直到2003年。
  - 2004年12月－发布版本PowerDesigner 10.0
  - 2005年－发布PowerDesigner 11.0
  - 2006年1月－发布PowerDesigner 12.0，支持元数据映射以及报表。
  - 2006年8月－发布PowerDesigner 12.1，增强了Microsoft Visual Studio以及SQL
    Server的支持
  - 2007年7月－发布PowerDesigner
    12.5，加入了全新的[ETL](../Page/ETL.md "wikilink")（萃取、轉置、載入）與
    [EII](../Page/EII.md "wikilink")（[Enterprise Information
    Integration](../Page/Enterprise_Information_Integration.md "wikilink")）建模以及完全的[UML](../Page/UML.md "wikilink")2.0支持
  - 2008年10月－发布PowerDesigner 15.0，加入了全新的[Enterprise
    Architecture建模](../Page/Enterprise_Architecture.md "wikilink")、支援自訂框架（[Zachman
    Framework](../Page/Zachman_Framework.md "wikilink")、[FEAF](../Page/FEAF.md "wikilink")、……)、Impact
    and Lineage Analysis Diagram、[Logical Data
    Model](../Page/Logical_Data_Model.md "wikilink")、[Barker
    Notation](../Page/Barker_Notation.md "wikilink")、Project support

## 标准

PowerDesigner支持下面[标准](../Page/标准化.md "wikilink")：

  - [BPEL4WS](../Page/BPEL4WS.md "wikilink")

  - [Business Process Modeling
    Notation](../Page/BPMN.md "wikilink")（BPMN）

  - Document Type Definition（[DTD](../Page/DTD.md "wikilink")）

  -
  - [IDEF](../Page/IDEF.md "wikilink")

  - [RDBMS](../Page/RDBMS.md "wikilink")

  - Rich Text Format（[RTF](../Page/RTF.md "wikilink")）

  - [UML](../Page/Unified_Modeling_Language.md "wikilink") 2.0

  - [XML](../Page/XML.md "wikilink")

  - [XML Schema](../Page/XML_Schema.md "wikilink")

## 主要竞争者

  - [CA公司](../Page/CA公司.md "wikilink")
  - [Embarcadero](../Page/Embarcadero.md "wikilink")
  - Grandite [SILVERRUN](../Page/SILVERRUN.md "wikilink")

## 參見

  - [数据建模](../Page/数据建模.md "wikilink")（）
  - [模型驅動架構](../Page/模型驅動架構.md "wikilink")
  - [UML](../Page/Unified_Modeling_Language.md "wikilink")
  - [RDBMS](../Page/RDBMS.md "wikilink")
  - [XML](../Page/XML.md "wikilink")
  - [XML Schema](../Page/XML_Schema.md "wikilink")
  - [資料倉儲](../Page/資料倉儲.md "wikilink")

## 参考

[Category:数据建模工具](../Category/数据建模工具.md "wikilink")
[Category:UML工具](../Category/UML工具.md "wikilink")

1.
2.