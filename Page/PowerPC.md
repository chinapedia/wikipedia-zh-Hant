[IBM_PowerPC601_PPC601FD-080-2_top.jpg](https://zh.wikipedia.org/wiki/File:IBM_PowerPC601_PPC601FD-080-2_top.jpg "fig:IBM_PowerPC601_PPC601FD-080-2_top.jpg")
**PowerPC**（，有時簡稱**PPC**）是一種[精簡指令集](../Page/精簡指令集.md "wikilink")（[RISC](../Page/RISC.md "wikilink")）架構的[中央處理器](../Page/中央處理器.md "wikilink")（[CPU](../Page/CPU.md "wikilink")），其基本的設計源自[IBM的](../Page/IBM.md "wikilink")[POWER](../Page/IBM_POWER.md "wikilink")（Performance
Optimized With Enhanced RISC；《IBM
Connect電子報》2007年8月號譯為「增強RISC性能優化」）架構。POWER是1991年，[Apple](../Page/蘋果電腦.md "wikilink")、IBM、[Motorola組成的AIM联盟所發展出的](../Page/Motorola.md "wikilink")[微處理器架構](../Page/微處理器.md "wikilink")。PowerPC是整个[AIM联盟平台的一部分](../Page/AIM联盟.md "wikilink")，并且是到目前为止唯一的一部分。但蘋果電腦自2005年起，將旗下電腦產品轉用[Intel
CPU](../Page/Intel_CPU.md "wikilink")。

PowerPC的历史可以追溯到早在1990年随RISC System/6000一起被介绍的IBM
POWER架構。该设计是从早期的RISC架构（比如[IBM
801](../Page/IBM_801.md "wikilink")）与[MIPS架构的处理器得到灵感的](../Page/MIPS架构.md "wikilink")。

1990年代，IBM、Apple和Motorola开发PowerPC晶片成功，并制造出基于PowerPC的多处理器计算机。PowerPC架构的特点是可伸缩性好、方便灵活。第一代PowerPC采用[0.6微米製程](../Page/0.6微米製程.md "wikilink")，[電晶體达到单芯片](../Page/電晶體.md "wikilink")300万个。

1998年，铜芯片问世，开创了一个新的历史纪元。

2000年，IBM开始大批推出采用铜芯片的产品，如RS/6000的X80系列产品。[铜製程取代了已经沿用了](../Page/铜製程.md "wikilink")30年的[铝製程](../Page/铝製程.md "wikilink")，使矽芯片多CPU的生产工艺达到了[0.2微米的水平](../Page/0.2微米製程.md "wikilink")，单芯片整合了2亿个電晶体，大大提高了运算性能；而1.8V的低[电压操作](../Page/电压.md "wikilink")（原为2.5V）大大降低了[芯片的耗能](../Page/芯片.md "wikilink")，容易散热，从而大大提高了系统的稳定性。

2005年10月，IBM发布System
p5产品线，采用基于POWER5处理器的增强版——POWER5+处理器，提供一系列更优化功能。产品一经推出，就打破15项计算领域的世界纪录。新的POWER5+处理器被称为“片上服务器”（server
on a
chip），它包括2个处理器，一个高带宽系统交换器，一个更大高速缓存和I/O界面。最新的POWER5+有1.5和1.9GHz两个主频选择，最大72MB板上高速缓存，支持逻辑分区技术，可使System
p5为用户提供更强大性能，而占用面积更小。

## 產品應用

較廣為人知的產品應用包含：

  - [蘋果公司](../Page/蘋果公司.md "wikilink")：[Power
    Macintosh系列](../Page/Power_Macintosh.md "wikilink")、PowerPC
    [PowerBook系列](../Page/PowerBook.md "wikilink")（1995年以後的產品）、[iBook系列](../Page/iBook.md "wikilink")、[iMac系列](../Page/iMac.md "wikilink")（2005年以前的產品）、[eMac系列產品](../Page/eMac.md "wikilink")。
  - [任天堂](../Page/任天堂.md "wikilink")：[GameCube](../Page/GameCube.md "wikilink")\[1\]、[Wii和](../Page/Wii.md "wikilink")[Wii
    U](../Page/Wii_U.md "wikilink")。
  - [微软](../Page/微软.md "wikilink")：[Xbox
    360](../Page/Xbox_360.md "wikilink")\[2\]
  - [索尼](../Page/索尼.md "wikilink")：[PlayStation
    3](../Page/PlayStation_3.md "wikilink")\[3\]

## 相關條目

  - [RTEMS](../Page/RTEMS.md "wikilink")

## 参考文献

[Category:IBM处理器](../Category/IBM处理器.md "wikilink")
[Category:微處理器](../Category/微處理器.md "wikilink")
[Category:POWER架構](../Category/POWER架構.md "wikilink")
[Category:1991年面世](../Category/1991年面世.md "wikilink")
[Category:指令集架構](../Category/指令集架構.md "wikilink")

1.
2.

3.