***Precious*** 是[宇多田光在正式出道前以](../Page/宇多田光.md "wikilink")「Cubic
U」之名發表的英文專輯，於1996年在紐約Sony
Music工作室錄製。本作首次於1998年1月28日在日本發行。原定於日本美國同步發行，但因[EMI的內部重組問題](../Page/EMI.md "wikilink")，本專輯未在美國發行，只有在日本發行。之後1999年3月31日在日本重新發行。

## 曲目

  - 作詞・作曲：Cubic U and Charlene Harrison（1.,4.,6.,8.）
  - 作詞・作曲：cubic U and Charlene Harrison（2.,11.）
  - 作詞・作曲：Cubic U, Michael C.Warner and Charlene Harrison（3.,5.,7.,12.）
  - 作詞・作曲：Cubic U and Michael C.Warner（9.）
  - 作詞・作曲：Hal David, Burt Bacharach（10.）

### 美版曲目

### 日版曲目

*Note*：其中"Here and There and Back Again" 一曲未於日版收錄。

## 收錄單曲

| Date       | Title                                                            | Peak position | Weeks | Sales |
| :--------- | :--------------------------------------------------------------- | :------------ | :---- | :---- |
| 1997年1月28日 | "[Close to You](../Page/Close_to_You_\(Cubic_U\).md "wikilink")" | \---          | \---  | \---  |
|            |                                                                  |               |       |       |

## 銷售

***Precious*** - **[Oricon](../Page/Oricon.md "wikilink")** Sales Chart
(Japan)

| Release          | Chart                      | Peak Position | Sales Total | Chart Run |
| :--------------- | :------------------------- | :------------ | :---------- | :-------- |
| January 28, 1998 | Oricon Daily Albums Chart  |               |             |           |
| January 28, 1998 | Oricon Weekly Albums Chart | \#4           | 702,060     | 17 weeks  |
| January 28, 1998 | Oricon Yearly Albums Chart |               |             |           |
|                  |                            |               |             |           |

[Category:宇多田光專輯](../Category/宇多田光專輯.md "wikilink")
[Category:1998年音樂專輯](../Category/1998年音樂專輯.md "wikilink")
[Category:EMI日本音樂專輯](../Category/EMI日本音樂專輯.md "wikilink")