**Primer Banco del Istmo**或稱**Banistmo**及**Banco Del
Istmo**，並成為巴拿馬及中美洲最大銀行，由[Grupo
Banistmo擁有](../Page/Grupo_Banistmo.md "wikilink")，直至2006年11月收購Grupo
Banistmo，2007年Grupo
Banistmo完成合併，成為[巴拿馬滙豐銀行](../Page/巴拿馬滙豐銀行.md "wikilink")，但**Primer
Banco del Istmo**仍保持獨立運作。

**Banistmo**透過42透過個支點提供全面的個人，商業和投資銀行服務及本由Grupo Banistmo持有的[Compania
Nacional de Seguros](../Page/Compania_Nacional_de_Seguros.md "wikilink")
('Conase')提供保險服務。已成為旗下子公司。

## 相關

  - [墨西哥滙豐](../Page/墨西哥滙豐.md "wikilink")
  - [巴拿馬滙豐銀行](../Page/巴拿馬滙豐銀行.md "wikilink")

## 外部連結

  - [Banistmo.com](http://www.banistmo.com)

[Category:滙豐](../Category/滙豐.md "wikilink")