**Princess
Rose**是[日本](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")[田村由香里的第](../Page/田村由香里.md "wikilink")11張單曲，由[科樂美數位娛樂於](../Page/科樂美數位娛樂.md "wikilink")2006年12月20日發行（由[KING
RECORDS分銷](../Page/KING_RECORDS.md "wikilink")），商品編號為GBCM-15。

## 概要

  - 初回限定版採用[Digipak形式的包裝](../Page/Digipak.md "wikilink")
  - 這是田村由香里的唱片合約轉往[KING
    RECORDS前最後一張發行的單曲](../Page/KING_RECORDS.md "wikilink")。
  - 在[Oricon唱片銷量排行榜的最高排名為第](../Page/Oricon.md "wikilink")6位（2007年第1週），是田村當時獲得的最高排名。

## 收錄曲目

1.  Princess Rose

      - 作詞：，作曲、編曲：[橋本由香利](../Page/橋本由香利.md "wikilink")
      - 動畫「[童話槍手小紅帽](../Page/童話槍手小紅帽.md "wikilink")」後期片頭曲（即[無線動畫](../Page/電視廣播有限公司.md "wikilink")《俏皮劍俠小紅帽》部分集數主題歌曲）
      - [文化放送系電台節目](../Page/文化放送.md "wikilink")「」開首曲（2006年11月－2007年4月）
      - [db-FM網上電台節目](../Page/db-FM.md "wikilink")「」開首曲（\#183〜\#205）

2.    - 作曲、作詞：[大津美紀](../Page/大津美紀.md "wikilink")，編曲：[河野伸](../Page/河野伸.md "wikilink")

3.    - 作詞：，作曲、編曲：[太田雅友](../Page/太田雅友.md "wikilink")
      - 文化放送系電台節目「」開首曲（2006年11月〜2007年4月）
      - db-FM網上電台節目「」結尾曲（\#183〜\#205）

## 參考資料

<div class="references-small">

<references />

</div>

[Category:2006年單曲](../Category/2006年單曲.md "wikilink")
[Category:東京電視台動畫主題曲](../Category/東京電視台動畫主題曲.md "wikilink")
[Category:田村由香里單曲](../Category/田村由香里單曲.md "wikilink")