**Pwn**
()，是一個[駭客語法的](../Page/leet.md "wikilink")[俚語詞](../Page/俚語.md "wikilink")，自"own"這個字引申出來的\[1\]\[2\]，這個詞的含意在於，玩家在整個遊戲對戰中處在勝利的優勢，或是說明競爭對手處在完全慘敗的情形下，這個詞習慣上在[網路](../Page/Internet.md "wikilink")[遊戲](../Page/電子遊戲.md "wikilink")[文化主要用於嘲笑競爭對手在整個遊戲對戰中已經完全被擊敗](../Page/文化.md "wikilink")（例如："You
just got pwned\!"）。過去式的拼寫可以拼成**pwnd**，**pwn3d**，**pwnt**（讀成 *t*
這個音）或是**powned**（讀成 *d* 這個音）。

在[駭客](../Page/駭客_\(電腦安全\).md "wikilink")[行話裡](../Page/行話.md "wikilink")，尤其在另外一種電腦技術方面，包括電腦（[伺服器或](../Page/伺服器.md "wikilink")[個人電腦](../Page/個人電腦.md "wikilink")）、網站、閘道裝置、或是應用程式，"pwn"在這一方面的意思是攻破（"to
compromise"，危及、損害）或是控制（"to
control"）。在這一方面的意義上，它與[駭客入侵與破解是相同意思的](../Page/駭客_\(電腦安全\).md "wikilink")。例如某一個外部團體已經取得未經公家許可的系統管理員控制權限，並利用這個權限駭入並入侵（"owned"
或是 "pwned"）這個系統。

## 詞的起源

"pwn"這個詞的源起以及它被廣泛地普遍使用的原因，源自於[魔獸爭霸某段訊息上設計師](../Page/魔獸爭霸.md "wikilink")[打字時拼錯而造成的](../Page/誤植.md "wikilink")，原先的字詞應該是"own"這個字，因為
'p' 與 'o' 在[標準英文鍵盤上的位置是相鄰的](../Page/QWERTY.md "wikilink")。\[3\]

## 單字發音

雖然"pwn"這個單字早在公元1989年就出現在網際網路上\[4\]，但是至今還是以拼寫的方式表現出來，目前為止沒有公認的[發音標準](../Page/讀音.md "wikilink")。大多人普遍認定的字彙發音可以用以下幾個字來表達："own"，"pone"，"pawn"與"poon"（可以參考[威爾斯語的](../Page/威爾斯語.md "wikilink")**，意為[谷地（valley）](../Page/谷地.md "wikilink")）；雖然上述第一個與第二個單字的發音被大多數人公認為是最正確的發音方式，但是"pwen"這個字也是被認定為可以接受的發音。\[5\]

[Electronic Gaming Monthly
電子遊戲月刊的編輯](../Page/Electronic_Gaming_Monthly.md "wikilink")
 也是把 pwn
這個字「動詞化（verbalize）」的其中一人，念"owned"之前會發"puh-"這個音，就像是經過一陣笑聲之後說某個行話或特定詞一樣。在成人卡通動畫*[South
Park（南方公園，南方四賤客）](../Page/南方四賤客.md "wikilink")*較晚期播出的片段也可以發現到類似的發音方式（例如"[Make
Love, Not Warcraft（要愛，不要魔獸）](../Page/要愛，不要魔獸世界.md "wikilink")，" ""）。

## 錯誤拼寫

有時會發生意外或是其他原因造成拼字錯誤或是發音錯誤，而寫（讀）成 Prawned 這個字（Prawn
是[蝦子](../Page/蝦.md "wikilink")，當動詞加 'ed'
意為**捕蝦子**）。儘管在遊戲中與其他玩家的對話過程中，會出現並產生幽默的效果，還會讓某些玩家聯想到與甲殼類動物相關的幽默笑話，這是由於使用者在臨時想到要使用某個字彙，卻愚笨無知地拼錯原先想要使用的字彙，造成整個字句在意義上出現重大的偏差。

## 相關條目

  - [Leet](../Page/Leet.md "wikilink")

## 參考資料

[Category:互联网用语](../Category/互联网用语.md "wikilink")
[Category:網路迷因](../Category/網路迷因.md "wikilink")
[Category:電子遊戲迷因](../Category/電子遊戲迷因.md "wikilink")

1.  ["Pwned - 10 Tales of Appropriation in Video
    Games"](http://publik.tuwien.ac.at/files/pub-inf_4395.pdf) (Martin
    Pichlmair)
2.  ["Computer Slang"](http://books.ifmo.ru/book/vip/196.pdf)
3.  About.com, [pwn -
    own](http://internetgames.about.com/library/glossary/bldef-pwn.htm),
    accessed [1 January](../Page/1_January.md "wikilink"), 2006
4.  [Urban Dictionary:
    pwn](http://www.urbandictionary.com/define.php?term=pwn) (see
    images)
5.  [Pwn](http://www.pcmag.com/encyclopedia_term/0,2542,t=pwn&i=56903,00.asp),
    PCMagazine Experts Help Encyclopedia, accessed [2
    March](../Page/2_March.md "wikilink") 2008