**菲麗絲·桃樂絲·詹姆斯，荷蘭公園的詹姆斯女爵**，[OBE](../Page/不列顛帝國勳章.md "wikilink")，[FRSA](../Page/皇家藝術學會.md "wikilink")，[FRSL](../Page/皇家文學學會.md "wikilink")（**Phyllis
Dorothy James, Baroness James of Holland
Park**，）是一位[英國](../Page/英國.md "wikilink")[犯罪小說的女作家](../Page/犯罪小說.md "wikilink")，1991年封[終身貴族進入](../Page/終身貴族.md "wikilink")[上議院](../Page/英國上議院.md "wikilink")。

## 作品

[戴立許](../Page/戴立許.md "wikilink")（Adam Dalgliesh）系列（Adam Dalgliesh）

1.  [掩上她的臉Cover her face](../Page/掩上她的臉Cover_her_face.md "wikilink")
    (1962)([中文版-聯經出版(2007)](https://www.linkingbooks.com.tw/LNB/book/Book.aspx?ID=184110))
2.  [謀殺之心 A Mind to Murder](../Page/謀殺之心_A_Mind_to_Murder.md "wikilink")
    (1963)([中文版-聯經出版(2008)](https://www.linkingbooks.com.tw/LNB/book/Book.aspx?ID=184118))
3.  [Unnatural Causes](../Page/Unnatural_Causes.md "wikilink") (1967)
4.  [Shroud for a
    Nightingale](../Page/Shroud_for_a_Nightingale.md "wikilink") (1971)
5.  [黑塔](../Page/黑塔_\(P.D._James\).md "wikilink") (1975)
6.  [Death of an Expert
    Witness](../Page/Death_of_an_Expert_Witness.md "wikilink") (1977)
7.  [死亡的滋味 A Taste for
    Death](../Page/死亡的滋味_A_Taste_for_Death.md "wikilink")
    (1986)([中文版-聯經出版(2008)](https://www.linkingbooks.com.tw/LNB/book/Book.aspx?ID=184127))
8.  [Devices and Desires](../Page/Devices_and_Desires.md "wikilink")
    (1989)
9.  [Original Sin](../Page/Original_Sin_\(novel\).md "wikilink") (1994)
10. [A Certain Justice](../Page/A_Certain_Justice.md "wikilink") (1997)
11. [Death in Holy Orders](../Page/Death_in_Holy_Orders.md "wikilink")
    (2001)
12. [謀殺展覽室 The Murder Room](../Page/謀殺展覽室_The_Murder_Room.md "wikilink")
    (2003)([中文版-聯經出版(2011)](https://www.linkingbooks.com.tw/LNB/book/Book.aspx?ID=184196))
13. [燈塔 The Lighthouse](../Page/燈塔_The_Lighthouse.md "wikilink")
    (2005)([中文版-聯經出版(2010)](https://www.linkingbooks.com.tw/LNB/book/Book.aspx?ID=184166))
14. [私家病人The Private
    Patient](../Page/私家病人The_Private_Patient.md "wikilink")(2008)([中文版-聯經出版(2010)](https://www.linkingbooks.com.tw/LNB/book/Book.aspx?ID=184167))

[寇蒂莉雅·葛雷](../Page/寇蒂莉雅·葛雷.md "wikilink")（Cordelia Gray）系列：

1.  [不適合女性的職業](../Page/不適合女性的職業.md "wikilink") (1972)
2.  [The Skull Beneath the
    Skin](../Page/The_Skull_Beneath_the_Skin.md "wikilink") (1982)

### 非小說類

1.  [推理小說這樣讀：謀殺天后詹姆絲告訴你Talking About Detective
    Fiction](../Page/推理小說這樣讀：謀殺天后詹姆絲告訴你Talking_About_Detective_Fiction.md "wikilink")
    (2009)([中文版-聯經出版(2011)](https://www.linkingbooks.com.tw/LNB/book/Book.aspx?ID=184191))

### 改編成電視劇或電影

  - Death of an Expert Witness (1983)
  - Shroud for a Nightingale (1984)
  - Cover Her Face (1985)
  - The Black Tower (1985)
  - Unnatural Causes
  - Original Sin
  - A Taste For Death
  - Devices and Desires
  - A Mind to Murder
  - A Certain Justice
  - An Unsuitable Job for a Woman
  - Death in Holy Orders (2003)
  - The Murder Room (2004)
  - Children of Men (劇情片，2006)
  - Death Comes to Pemberley (電視連續劇，2013)
  - [彭伯利谋杀案](../Page/彭伯利谋杀案.md "wikilink") (電視連續劇，2013)

## 參考資料

[J](../Category/英國小說家.md "wikilink")
[J](../Category/獲授終身貴族者.md "wikilink")
[J](../Category/OBE勳銜.md "wikilink")
[Category:英国女性作家](../Category/英国女性作家.md "wikilink")
[Category:20世纪作家](../Category/20世纪作家.md "wikilink")
[Category:英格蘭聖公宗教徒](../Category/英格蘭聖公宗教徒.md "wikilink")
[Category:牛津人](../Category/牛津人.md "wikilink")