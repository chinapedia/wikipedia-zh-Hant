**小科利尔·布朗**（，），昵称“P·J”，[美国职业](../Page/美国.md "wikilink")[篮球运动员](../Page/篮球.md "wikilink")，司职[中锋或](../Page/中锋.md "wikilink")[大前锋](../Page/大前锋.md "wikilink")。

布朗出生于[底特律](../Page/底特律.md "wikilink")，大学就读于[路易斯安那理工大學](../Page/路易斯安那理工大學.md "wikilink")，在[1992年NBA选秀中于第二轮总第](../Page/1992年NBA选秀.md "wikilink")29位被[新泽西网队选中](../Page/新泽西网队.md "wikilink")。但是布朗当年并未立即加入NBA，而是远赴[希腊打了一年球](../Page/希腊.md "wikilink")。1993年，布朗加入网队，效力四年后，转入[邁阿密熱火](../Page/邁阿密熱火.md "wikilink")，成为一位防守高手。

2000年，布朗被交换去[新奥尔良黄蜂队](../Page/新奥尔良黄蜂队.md "wikilink")，2006年他又被交换去[芝加哥公牛队](../Page/芝加哥公牛队.md "wikilink")。2007年他在合同结束后并未与公牛队续约，2008年3月转与波士顿凯尔特人队签约，并于赛季末拿到总冠军退役结束其16年的职业生涯。

## 参考资料

## 外部链接

  - [ Player Info](http://www.nba.com/playerfile/pj_brown) at
    [NBA.com](../Page/NBA.com.md "wikilink")

  -
  - [](http://sports.espn.go.com/nba/players/profile?statsId=814) at
    [ESPN.com](../Page/ESPN.com.md "wikilink")

  - [](https://web.archive.org/web/20080329040355/http://media.cnnsi.com/basketball/nba/players/814/)
    at [SI.com](../Page/Sports_Illustrated.md "wikilink")

[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:新泽西网队球员](../Category/新泽西网队球员.md "wikilink")
[Category:底特律活塞队球员](../Category/底特律活塞队球员.md "wikilink")
[Category:新奥尔良黄蜂队球员](../Category/新奥尔良黄蜂队球员.md "wikilink")
[Category:芝加哥公牛队球员](../Category/芝加哥公牛队球员.md "wikilink")
[Category:波士顿凯尔特人队球员](../Category/波士顿凯尔特人队球员.md "wikilink")