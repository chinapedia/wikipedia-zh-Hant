**Q** , **q**
是[拉丁字母中的第](../Page/拉丁字母.md "wikilink")17个[字母](../Page/字母.md "wikilink")。

在[闪族语中](../Page/闪含语系.md "wikilink")，字母Qôp表示。[希腊语中](../Page/希腊语.md "wikilink")（称为[Qoppa](../Page/Ϙ.md "wikilink")）可能是包括和的唇化塞音，后各变成和。所以Qoppa变成两个字母：Qoppa只表数字，Φι（Phi）表送气音，而后在[现代希腊语中变为](../Page/希腊语.md "wikilink")。[伊特鲁里亚语Q只与V连用](../Page/伊特鲁里亚语.md "wikilink")，表示和V。\[1\]有学者表示Q和Phi无关\[2\]\[3\]

很多现代歐洲语言中Q显多余。在[罗曼语族和](../Page/罗曼语族.md "wikilink")[日耳曼语族中](../Page/日耳曼语族.md "wikilink")，几乎均缀u。[英语最常为复辅音](../Page/英语.md "wikilink")（同[意大利语](../Page/意大利语.md "wikilink")），在[德语中为](../Page/德语.md "wikilink")，在[法语](../Page/法语.md "wikilink")、[西班牙语和](../Page/西班牙语.md "wikilink")[加泰罗尼亚语中表示](../Page/加泰罗尼亚语.md "wikilink")（在西班牙语中，“qu”在i和e前代替c表示，因为ce、ci中c为摩擦音。在[阿塞拜疆语](../Page/阿塞拜疆语.md "wikilink")、[乌兹别克语](../Page/乌兹别克语.md "wikilink")、[维吾尔语](../Page/维吾尔语.md "wikilink")、[鞑靼语](../Page/鞑靼语.md "wikilink")、[哈萨克语中](../Page/哈萨克语.md "wikilink")，Q发音同闪族语q。而在后者中，q一般为[清小舌塞音](../Page/清小舌塞音.md "wikilink")。[國際音標之](../Page/國際音標.md "wikilink")为[清小舌塞音](../Page/清小舌塞音.md "wikilink")。

## 字母Q的含意

## 字符编码

| 字符编码                               | [ASCII](../Page/ASCII.md "wikilink") | [Unicode](../Page/Unicode.md "wikilink") | [EBCDIC](../Page/EBCDIC.md "wikilink") | [摩斯电码](../Page/摩斯电码.md "wikilink") |
| ---------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- | ---------------------------------- |
| [大写](../Page/大写字母.md "wikilink") Q | 81                                   | 0051                                     | 216                                    | `--·-`                             |
| [小写](../Page/小写字母.md "wikilink") q | 113                                  | 0071                                     | 152                                    |                                    |

## 計算編碼

  -
    <sup>1</sup>

## 其他表示方法

## 其他字母中的相近字母

  - （[希腊字母](../Page/希腊字母.md "wikilink") Qoppa）

  - （[西里尔字母](../Page/西里尔字母.md "wikilink") Koppa）

## 參見

  - [Q後面不接U的英文單字列表](../Page/Q後面不接U的英文單字列表.md "wikilink")

## 注释

## 外部連結

  -
  -
  -
[Category:拉丁字母](../Category/拉丁字母.md "wikilink")

1.  《伟大的字母：从A到Z，字母表的辉煌历史》，第252页。作者：（美）大卫·萨克斯，翻译：康慨
2.  Q来自腓尼基字母Qosh，后来变成希腊字母Ϙ(Qoppa)，再演化为今天的Q。而字母Φ(phi)则不是来自腓尼基字母。
3.  《伟大的字母：从A到Z，字母表的辉煌历史》，第253页下脚注。