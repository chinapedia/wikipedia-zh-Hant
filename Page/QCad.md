[Qcad.png](https://zh.wikipedia.org/wiki/File:Qcad.png "fig:Qcad.png")

**QCad**是一个用于二维设计及绘图的[计算机辅助设计](../Page/计算机辅助设计.md "wikilink")（computer-aided
design, CAD）软件，支持[Linux](../Page/Linux.md "wikilink")、[Mac OS
X](../Page/Mac_OS_X.md "wikilink")、[Unix及](../Page/Unix.md "wikilink")[Microsoft
Windows操作系统](../Page/Microsoft_Windows.md "wikilink")。Qcad的社区版功能要比完全版稍少\[1\]，但它是使用[GPL协议发布](../Page/GPL协议.md "wikilink")。对于一些Linux平台也有预编译的包可以使用，如Debian。

QCad由[RibbonSoft从](../Page/RibbonSoft.md "wikilink")1999年10月开始开发，代码基础是[CAM
Expert](../Page/CAM_Expert.md "wikilink")。而QCad 2的开发则始于2002年5月。

QCad使用了很多和[AutoCAD相似的接口及概念](../Page/AutoCAD.md "wikilink")。QCad支持[AutoCAD
DXF文件格式的读写](../Page/AutoCAD_DXF.md "wikilink")，并可以导入或导出位图\[2\]。

QCad在[KDE平台有一个修改版本](../Page/KDE.md "wikilink")，称为KDE辅助设计（KDE aided
design, KAD）\[3\]。

## 参考文献

## 参见

  - [计算机辅助设计](../Page/计算机辅助设计.md "wikilink")

## 外部链接

  - [QCad主页](http://www.qcad.org)

  - [QCad视频教程](https://web.archive.org/web/20110202113054/http://thecadtrain.com/)

  - [QCad教程](http://linuxfocus.org/English/January2002/article132.shtml)

[Category:三维图像软件](../Category/三维图像软件.md "wikilink")
[Category:使用Qt的軟體](../Category/使用Qt的軟體.md "wikilink")

1.
2.
3.