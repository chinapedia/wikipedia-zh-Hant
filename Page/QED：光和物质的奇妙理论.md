《**QED：光和物质的奇异性**》（）是[美国](../Page/美国.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")[理查德·费曼编著的一本关于](../Page/理查德·费曼.md "wikilink")[量子电动力学](../Page/量子电动力学.md "wikilink")（QED）的通俗读物。它由费曼所作的4个讲座合集而成。最早由[普林斯顿大学出版社于](../Page/普林斯顿大学.md "wikilink")1985年出版。

## 内容

全书的4个讲座分别是：

1.  引言（Introduction），1
2.  光子：光的粒子（Photons: Particles of Light），39
3.  电子和它们的相互作用（Electrons and Their Interactions），85
4.  松散的结尾（Loose Ends），139

<!-- end list -->

  -

      -
        <small>注：其中后注数字为页码，共172页。</small>

## 主要版本

  -   - 汉译版1：
      - 汉译版2：

## 参见

  - [奇异数](../Page/奇异数.md "wikilink")

## 参考资料

## 外部链接

  -
  -
  -
  -
[Category:物理书籍](../Category/物理书籍.md "wikilink")