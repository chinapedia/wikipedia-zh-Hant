[Qwerty.svg](https://zh.wikipedia.org/wiki/File:Qwerty.svg "fig:Qwerty.svg")
[QWERTY_keyboard.jpg](https://zh.wikipedia.org/wiki/File:QWERTY_keyboard.jpg "fig:QWERTY_keyboard.jpg")
**QWERTY**是各国使用的、基于[拉丁字母的标准](../Page/拉丁字母.md "wikilink")[打字机和](../Page/打字机.md "wikilink")[-{zh-hans:计算机;
zh-hant:電腦;}-键盘布局](../Page/電腦鍵盘.md "wikilink")。“QWERTY”是该键盘布局字母区第一行的前六个字母。键的安排顺序由[克里斯托弗·肖尔斯](../Page/克里斯托弗·肖尔斯.md "wikilink")（Christopher
Sholes）设计。\[1\]

## 歷史

### 概說

使用QWERTY排列的[打字机在](../Page/打字机.md "wikilink")1874年投入批量生产。從此成為应用最广泛的人机接口，大部分的-{zh-hans:计算机;
zh-hant:電腦;}-都是使用QWERTY键盘。\[2\]

QWERTY鍵盤安排次序的原則，是减少打字機在打字時連動杆之間的擠壓及故障發生率的狀況，因而要把常用字母隔開（不過像“E”、“R”就在一起）。其他種類的鍵盤，如1932年[奧古斯特·德沃夏克發明的](../Page/奧古斯特·德沃夏克.md "wikilink")[德沃夏克鍵盤](../Page/德沃夏克鍵盤.md "wikilink")，為希望在已經不需要避免連動杆的擠壓後，重新排列鍵位以提高打字速度，因此把[元音及](../Page/元音.md "wikilink")5個最常用的[輔音安排在中間一行](../Page/輔音.md "wikilink")，以便交換左右手打字的頻率，同時也有設計左手或右手為重的鍵位。但是因故無法量產，以至於[德沃夏克鍵盤鍵盤的普及程度不如QWERTY鍵盤](../Page/德沃夏克鍵盤.md "wikilink")。

### 現代的替代品

如今绝大多数[移動操作系統均配备虚拟QWERTY键盘](../Page/移動操作系統.md "wikilink")。也有大量的[个人数码助理及非智能](../Page/个人数码助理.md "wikilink")[手机配备实体的QWERTY键盘](../Page/手机.md "wikilink")，如[TREO650](../Page/TREO650.md "wikilink")、[TREO600](../Page/TREO600.md "wikilink")、[黑莓及](../Page/黑莓手機.md "wikilink")[诺基亚E系列手机](../Page/諾基亞產品列表#E系列.md "wikilink")。而用于以上设备的QWERTY键盘亦被称为**全键盘**。

## HQ键盘

[Nokia_E55_01.jpg](https://zh.wikipedia.org/wiki/File:Nokia_E55_01.jpg "fig:Nokia_E55_01.jpg")
E55上的HQ鍵盤\]\]
HQ键盘是**H**alf-**Q**WERTY的缩写，是介于“[T9键盘](../Page/T9键盘.md "wikilink")”与“QWERTY键盘”之间，兼有数字键盘的简便且有QWERTY的快速输入功能的键盘。它的出现代表[互联网时代](../Page/互联网时代.md "wikilink")[手机的特色](../Page/手机.md "wikilink")。

## 參考資料

## 參見

  - [鍵盤佈局](../Page/鍵盤佈局.md "wikilink")
  - [德沃夏克鍵盤](../Page/德沃夏克鍵盤.md "wikilink")

## 外部連結

  - [Article on QWERTY and Path Dependence from EH.NET's
    Encyclopedia](https://web.archive.org/web/20060113120201/http://www.eh.net/encyclopedia/article/puffert.path.dependence)
  - [QWERTY Keyboard
    History](http://www.ideafinder.com/history/inventions/qwerty.htm)
  - [QWERTY Keyboard in
    Mobiles](https://web.archive.org/web/20110708055946/http://www.bakwaash.com/2011/07/05/mobile-phone-termonologies/)
  - [Android phones with QWERTY
    keyboards](http://merelinc.com/art-and-design/qwerty-android-phones-amazing-buttons/)

[it:Tastiera
(informatica)\#QWERTY](../Page/it:Tastiera_\(informatica\)#QWERTY.md "wikilink")

[Category:鍵盤配置](../Category/鍵盤配置.md "wikilink")

1.
2.