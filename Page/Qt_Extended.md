**Qt
Extended**（2008年9月30日前稱**Qtopia**）\[1\]是一個軟體平台，主要用於採用[嵌入式](../Page/嵌入式系統.md "wikilink")[Linux系統](../Page/Linux.md "wikilink")（embedded
Linux-based
system）的[電子手帳或](../Page/電子手帳.md "wikilink")[移動電話](../Page/移動電話.md "wikilink")。Qt
Extended的主要用途，在於提供一個跨平台的軟體平台，以便軟體開發者可以為系統提供更多的軟體。Qt
Extended是由[Nokia的子公司](../Page/Nokia.md "wikilink")[Qt
Software](../Page/Qt_Software.md "wikilink")（前稱Trolltech）所開發。

## 開發中止

2009年3月3日，Qt Software宣佈Qt Extended不再繼續作為獨立產品而開發，部份功能整合進Qt
Framework\[2\]。[OpenMoko以](../Page/OpenMoko.md "wikilink")[Qt Extended
Improved作為它的分支](../Page/Qt_Extended_Improved.md "wikilink")，繼續開發。

## 參考資訊

## 外部連結

  - [名詞解釋：Qtopia](https://web.archive.org/web/20071030222356/http://www.emb-kb.com/doku.php/%E8%A9%9E%E8%A7%A3/qtopia)（嵌入式知識館）
  - [Qtopia中文邮件列表](http://groups.google.com/group/qtopia-china)（Qtopia中文邮件列表）
  - [Qt/Qtopia中文论坛](https://web.archive.org/web/20070428054021/http://www.qtopia.org.cn/phpBB2/)（Qt/Qtopia中文论坛）

[Category:个人数码助理](../Category/个人数码助理.md "wikilink")
[Category:智能手機](../Category/智能手機.md "wikilink")

1.  [Trolltech and Qtopia rev'd and
    renamed](https://archive.is/20130427001806/www.linuxdevices.com/news/NS3988803998.html)
2.