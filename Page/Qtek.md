**Qtek
Corporation**，由智慧型手機廠商[宏達電與無線通訊產品通路商](../Page/宏達電.md "wikilink")[亮點於](../Page/亮點.md "wikilink")2002年合資成立。曾為宏達電在[歐洲](../Page/歐洲.md "wikilink")，[香港與](../Page/香港.md "wikilink")[拉丁美洲的自有品牌](../Page/拉丁美洲.md "wikilink")。宏達電收購[多普達國際後](../Page/多普達.md "wikilink")，採取淡化Qtek品牌的策略，現Qtek公司已全面采用HTC品牌來銷售產品。2008年公司已結業，但亮點會繼續提供產品保固，用戶仍可登錄其網站獲取軟體更新。

## 相關連結

  - [宏達國際電子](../Page/宏達國際電子.md "wikilink")

## 外部連結

  - [宏達電（HTC）](http://www.htc.com/)

<!-- end list -->

  - [Qtek
    歐洲](https://web.archive.org/web/20080707182953/http://www.myqtek.com/)

<!-- end list -->

  - [Qtek
    法國](https://web.archive.org/web/20080220081617/http://www.qtek.fr/)

<!-- end list -->

  - [Qtek 荷蘭](http://www.qtek.nl/)

<!-- end list -->

  - [Brightpoint](https://web.archive.org/web/20061110032857/http://www.brightpoint.com/)

<!-- end list -->

  - [宏达电品牌策略再出招
    既有Qtek将换上HTC](http://it.sohu.com/20060619/n243809794.shtml)

<!-- end list -->

  - [Qtek成为过去，HTC发布自有品牌手机](http://www.8080.net/html/200606/m16141058all.html)

[Category:品牌](../Category/品牌.md "wikilink")
[Category:電腦硬件公司](../Category/電腦硬件公司.md "wikilink")
[Category:行動電話製造商](../Category/行動電話製造商.md "wikilink")
[Category:已結業電腦公司](../Category/已結業電腦公司.md "wikilink")
[Category:跨国公司](../Category/跨国公司.md "wikilink")
[Category:行動電話](../Category/行動電話.md "wikilink")
[Category:智能手機](../Category/智能手機.md "wikilink")
[Category:个人数码助理](../Category/个人数码助理.md "wikilink")
[Category:超级移动电脑](../Category/超级移动电脑.md "wikilink")