[Rapid_neutron_capture.svg](https://zh.wikipedia.org/wiki/File:Rapid_neutron_capture.svg "fig:Rapid_neutron_capture.svg")

**R-過程**，或稱為[快中子捕獲過程](../Page/中子捕獲.md "wikilink")，是在核心發生塌縮的超新星（參考[超新星核合成](../Page/超新星核合成.md "wikilink")）中創造富含[中子且](../Page/中子.md "wikilink")[比鐵重的元素的程序](../Page/重金屬.md "wikilink")，並創造了大約一半的數量。R-過程需要以[鐵為](../Page/鐵.md "wikilink")*種核*進行連續的*快*[中子捕獲](../Page/中子捕獲.md "wikilink")，或是短程的**R-過程**。另一種居主導地位產生重元素的機制為[S-過程](../Page/S-過程.md "wikilink")，也就是通過*慢中子*捕獲進行核合成，主要發生在[AGB星](../Page/漸近巨星分支.md "wikilink")，而這兩種過程在產生比鐵重的元素的[星系化學演化中占了很重的分量](../Page/化學演化.md "wikilink")。

## 歷史

**R-過程**似乎必須從重元素的同位素相對豐度和在1956年由和[哈羅德·尤里重新印製的](../Page/哈羅德·尤里.md "wikilink")[化學元素豐度表來觀察](../Page/化學元素豐度表.md "wikilink")，尤其是[鍺](../Page/鍺.md "wikilink")、[氙](../Page/氙.md "wikilink")、和[铂這三種元素豐度的峰值](../Page/铂.md "wikilink")。根據[量子力學和](../Page/量子力學.md "wikilink")[殼層模型](../Page/殼層模型.md "wikilink")，原子核經[放射性衰變成為同位素時](../Page/放射性.md "wikilink")，會在接近[中子滴線處關閉中子殼層](../Page/中子滴線.md "wikilink")。這暗示了有些含量豐富的核子必須經由快[中子捕獲來創造](../Page/中子捕獲.md "wikilink")，並且也只能估算哪些核子可以經歷這樣的過程。在1957年，有一篇[著名的論文提出了](../Page/B2FH.md "wikilink")[S-過程和R](../Page/S-過程.md "wikilink")-過程的分攤表\[1\]，也提出了[恆星核合成的理論和設置了當代的](../Page/恆星核合成.md "wikilink")[核天體物理學的框架](../Page/核天體物理學.md "wikilink")。

## 核子物理

緊接在核塌縮超新星之後，有高溫和一股強大的[中子通量](../Page/中子通量.md "wikilink")（大約有10<font size= "-1"><sup>22</sup></font>中子每公分<font size= "-1">²</font>每秒鐘），因此[中子捕獲不僅進行的速率遠比](../Page/中子捕獲.md "wikilink")[β衰變為快](../Page/β衰變.md "wikilink")，並且穩定；這意味著**r-過程**
"沿著"[中子滴線進行](../Page/中子滴線.md "wikilink")。只有兩件事情可以阻止這個過程超越中子滴線，一是著名的中子捕獲[截面積因為中子殼層關閉而減小](../Page/核子結面積.md "wikilink")；另一則是重元素的的同位素穩定區域，當這樣的核變得不穩定時，便會自發性的產生分裂，使r-過程終止（目前相信中子的豐富數可以達到*A*
=
270，這是在[核種圖上的原子量](../Page/同位素表.md "wikilink")。）。在中子通量減少之後，這些極度不穩定的[放射性元素迅速的形成穩定](../Page/放射性衰變.md "wikilink")、中子豐富的原子核。所以，當[s-過程創造穩定的原子核和封閉中子殼層時](../Page/s-過程.md "wikilink")，r-過程創造的核子豐頂大約比[s-過程的峰頂低](../Page/s-過程.md "wikilink")10個[原子質量單位](../Page/原子質量單位.md "wikilink")，r-過程的核子衰變會退回而穩定在核種圖上原子數接近*A*的線。
[Nucleosynthesis_periodic_table.svg](https://zh.wikipedia.org/wiki/File:Nucleosynthesis_periodic_table.svg "fig:Nucleosynthesis_periodic_table.svg")

## 天文物理的場所

**r-過程**進行的場所相信是在核塌縮[超新星](../Page/超新星.md "wikilink")（光譜為[Ib和Ic超新星](../Page/Ib和Ic超新星.md "wikilink")、[II型超新星](../Page/II型超新星.md "wikilink")），因為能提供r-過程需要的物理條件（狀況）。無論如何，r-過程[核子的豐度不是只有一小部分的超新星拋出r](../Page/原子核.md "wikilink")-過程的核子至[星際物質中](../Page/星際物質.md "wikilink")，就是所有的超新星都只拋出極少量的r-過程核子。新近提出二擇一的解答是[中子星併吞](../Page/中子星.md "wikilink")（在由兩顆中子星組成的[聯星系統](../Page/聯星.md "wikilink")）可能在r-過程中也扮演著一個角色，但是這還需要[觀測來證實](../Page/觀測天文學.md "wikilink")。

## 參考資料

[Category:中子](../Category/中子.md "wikilink")
[Category:原子核物理学](../Category/原子核物理学.md "wikilink")
[Category:核合成](../Category/核合成.md "wikilink")
[Category:天体物理学](../Category/天体物理学.md "wikilink")

1.  E. M. Burbidge, G. R. Burbidge, W. A. Fowler, and F. Hoyle. [Reviews
    of Modern Physics](../Page/Reviews_of_Modern_Physics.md "wikilink"),
    **29** (1957) 547.