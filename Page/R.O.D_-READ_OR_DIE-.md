《**R.O.D -READ OR
DIE-**》（）是2001年到2002年期間[Aniplex發售的](../Page/Aniplex.md "wikilink")[OVA作品](../Page/OVA.md "wikilink")。全3集。由[STUDIO
DEEN動畫製作公司的監督](../Page/STUDIO_DEEN.md "wikilink")[舛成孝二負責](../Page/舛成孝二.md "wikilink")。[倉田英之的輕小說](../Page/倉田英之.md "wikilink")《R.O.D
READ OR DIE YOMIKO READMAN "THE PAPER"》為原作。描寫「操紙術」能自在操縱紙的能力的秘密探員讀子．利德曼。

## 劇情簡介

## 登場人物

### 大英圖書館特殊工作部

  - 讀子・列特文（読子・リードマン）[配音員](../Page/配音員.md "wikilink")：[三浦理惠子](../Page/三浦理惠子.md "wikilink")

紙術士，能夠把紙變成紙牆壁等等的多樣化武器，菫川奈奈奈的好朋友。\[詳見[R.O.D -THE
TV-](../Page/R.O.D_-THE_TV-.md "wikilink")\]

  - 蘭西．幕張（ナンシー・幕張）配音員：[根谷美智子](../Page/根谷美智子.md "wikilink")

為臥底，偉人軍團中的一員，有穿透物件的能力，事件之後,記憶倒退/大英圖書館特殊工作部/，後跟讀子一起生活。

  - 多雷克．安德森（ドレイク・アンダーソン）配音員：[岩崎征實](../Page/岩崎征實.md "wikilink")

讀子的同伴，很在乎自己的女兒

  - （ジェントルメン）配音員：[外波山文明](../Page/外波山文明.md "wikilink")
  - 丑角（ジョーカー）配音員：[鄉田穗積](../Page/鄉田穗積.md "wikilink")

大英圖書館負責人

  - 溫蒂（ウェンディ）配音員：[鮭延未可](../Page/鮭延未可.md "wikilink")

秘書，丑角得力助手，因為是大英博物館，所以常泡英式紅茶給同伴

### 世界偉人軍團

**企圖以貝多芬的自殺交響曲，清除多餘的人類**

  - [一休宗純](../Page/一休宗純.md "wikilink")（一休宗純）配音員：[KONTA](../Page/KONTA.md "wikilink")
  - [平賀源内](../Page/平賀源内.md "wikilink")（平賀源内）配音員：[大谷亮介](../Page/大谷亮介.md "wikilink")
  - [讓-昂利·法布爾](../Page/讓-昂利·法布爾.md "wikilink")（ジャン・ヘンリー＝ファーブル）配音員：[竹内順子](../Page/竹内順子.md "wikilink")、[高荷邦彦](../Page/高荷邦彦.md "wikilink")
  - [奧托·李林塔爾](../Page/奧托·李林塔爾.md "wikilink")（オットー・リリエンタール）配音員：[金子はりい](../Page/金子はりい.md "wikilink")
  - [玄奘三蔵](../Page/玄奘.md "wikilink")（玄奘三蔵）配音員：[高橋廣樹](../Page/高橋廣樹.md "wikilink")
  - [喬治·史蒂芬生](../Page/喬治·史蒂芬生.md "wikilink")（ジョージ・スチーブンソン）
  - [貝多芬](../Page/貝多芬.md "wikilink")（ルートヴィヒ・ヴァン・ベートーヴェン）
  - [瑪塔·哈里](../Page/瑪塔·哈里.md "wikilink")(蘭西．幕張)（マタ・ハリ）

## 製作團隊

  - 原作、系列構成、腳本 - [倉田英之](../Page/倉田英之.md "wikilink")（集英社「Ultra Jump
    Comics」刊）
  - 執行製作人 - [北川直樹](../Page/北川直樹.md "wikilink")
  - 企劃製作人 - 白川隆三
  - 人物原案 - [羽音たらく](../Page/羽音たらく.md "wikilink")
  - 人物設計 - [石浜真史](../Page/石浜真史.md "wikilink")
  - Visual work enhancer - [中嶋敦子](../Page/中嶋敦子.md "wikilink")
  - 機械設計 - [菅沼栄治](../Page/菅沼栄治.md "wikilink")
  - 項目設計 - 山形厚史
  - 製作設計 - [神宮司訓之](../Page/神宮司訓之.md "wikilink")
  - 美術監督 - 小倉一男
  - 美術設定 - 成田偉保
  - 色彩設計 - 松本真司
  - 攝影監督 - 近藤慎与
  - 編輯 - 宮本逸雄
  - 音樂 - [岩崎琢](../Page/岩崎琢.md "wikilink")
  - 音樂監督 - 児玉隆
  - 動作錄音演出 - [平光琢也](../Page/平光琢也.md "wikilink")
  - 音響設計 - 山田稔
  - 錄音製作 - [神南工作室](../Page/神南工作室.md "wikilink")
  - 製作人 - 藤本昌俊、松田桂一
  - 監督、分鏡、演出 - [舛成孝二](../Page/舛成孝二.md "wikilink")
  - 動畫製作 - [STUDIO DEEN](../Page/STUDIO_DEEN.md "wikilink")
  - 製作 - [SME・Visual works](../Page/Aniplex.md "wikilink")

## 各話列表

<table>
<thead>
<tr class="header">
<th><p>話數</p></th>
<th><p>日文標題</p></th>
<th><p>中文標題</p></th>
<th><p>作畫監督</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第1話</p></td>
<td></td>
<td><p>讀子小姐、事件喲。</p></td>
<td><p><a href="../Page/石濱真史.md" title="wikilink">石濱真史</a>（人物）<br />
<a href="../Page/菅沼榮治.md" title="wikilink">菅沼榮治</a>、<a href="../Page/鈴木博文_(動畫師).md" title="wikilink">鈴木博文</a>（機械）</p></td>
</tr>
<tr class="even">
<td><p>第2話</p></td>
<td></td>
<td><p>讀子小姐、印度喲。</p></td>
<td><p>石濱真史（人物）<br />
菅沼榮治（機械）<br />
橘秀樹（道具）</p></td>
</tr>
<tr class="odd">
<td><p>第3話</p></td>
<td></td>
<td><p>讀子小姐、危機喲。</p></td>
<td><p>石濱真史（人物）<br />
菅沼榮治（機械、特效）</p></td>
</tr>
</tbody>
</table>

## 關連項目

  - [R.O.D](../Page/R.O.D.md "wikilink")
  - [R.O.D -THE TV-](../Page/R.O.D_-THE_TV-.md "wikilink")

## 外部連結

  - [官方網站](http://www.sonymusic.co.jp/Animation/ROD/media/ovaindex.html)
  - [萬代頻道](http://www.b-ch.com/ttl/index.php?ttl_c=351)

[Category:圖書館題材作品](../Category/圖書館題材作品.md "wikilink")
[Category:2001年日本OVA動畫](../Category/2001年日本OVA動畫.md "wikilink")
[Category:Aniplex](../Category/Aniplex.md "wikilink")
[Category:历史人物题材作品](../Category/历史人物题材作品.md "wikilink")
[Category:輕小說改編動畫](../Category/輕小說改編動畫.md "wikilink")