[Half_r.png](https://zh.wikipedia.org/wiki/File:Half_r.png "fig:Half_r.png")\]\]
**R**，**r**是[拉丁字母中的第](../Page/拉丁字母.md "wikilink")18个[字母](../Page/字母.md "wikilink")。

R通常在語言中的發音屬於[流音](../Page/流音.md "wikilink")[輔音](../Page/輔音.md "wikilink")，不過在歐洲，不同的語言之間存在着許多不一樣的發音方式。隨著[印刷術和書寫方式的演變](../Page/印刷術.md "wikilink")，曾經有一段時間R存在著一個變體叫做[半r](../Page/半r.md "wikilink")，今日已經隨著時間與[長s一同消失在語言體系了](../Page/長s.md "wikilink")。

## R的意義

## R的變體

  - [摩斯電碼用短長短](../Page/摩斯電碼.md "wikilink")「· - ·」表示。
  - [北約音標字母以Romeo表示](../Page/北約音標字母.md "wikilink")。
  - [德文字母表以Richard表示](../Page/德文.md "wikilink")。
  - [商業的](../Page/商業.md "wikilink")「」代表[註冊商標](../Page/註冊商標.md "wikilink")。
  - [貨幣的](../Page/貨幣.md "wikilink")「₨」表示[盧比](../Page/盧比.md "wikilink")。
  - [醫學的](../Page/醫學.md "wikilink")「℞」表示處方箋，也可寫作[Rx](../Page/Rx.md "wikilink")。

## 各语言中r的发音方式

|                                           |                                                                                                                                                                                                                                                                     |
| ----------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 发音方式                                      | 语言                                                                                                                                                                                                                                                                  |
| [卷舌近音](../Page/卷舌近音.md "wikilink")\[ɻ\]   | [普通话](../Page/普通话.md "wikilink")、[美国英语](../Page/美国英语.md "wikilink")                                                                                                                                                                                                 |
| [齿龈颤音](../Page/齿龈颤音.md "wikilink")\[r\]   | [阿拉伯语](../Page/阿拉伯语.md "wikilink")、[亚美尼亚语](../Page/亚美尼亚语.md "wikilink")、[波兰语](../Page/波兰语.md "wikilink")、[葡萄牙语](../Page/葡萄牙语.md "wikilink")（一些方言）、[芬兰语](../Page/芬兰语.md "wikilink")、[意大利语](../Page/意大利语.md "wikilink")、[西班牙语](../Page/西班牙语.md "wikilink")（rr / r作開首） |
| [齿龈近音](../Page/齿龈近音.md "wikilink")\[ɹ\]   | [英语](../Page/英语.md "wikilink")                                                                                                                                                                                                                                      |
| [齿龈闪音](../Page/齿龈闪音.md "wikilink")\[ɾ\]   | [希腊语](../Page/希腊语.md "wikilink")、[葡萄牙语](../Page/葡萄牙语.md "wikilink")、[西班牙语](../Page/西班牙语.md "wikilink")'r'、[印地语](../Page/印地语.md "wikilink")'र'、[韓語](../Page/韓語.md "wikilink")'ㄹ'                                                                                     |
| [齿龈边闪音](../Page/齿龈边闪音.md "wikilink")\[ɺ\] | [日语](../Page/日语.md "wikilink")                                                                                                                                                                                                                                      |
| [卷舌闪音](../Page/卷舌闪音.md "wikilink")\[ɽ\]   | [挪威语](../Page/挪威语.md "wikilink")、[印地语](../Page/印地语.md "wikilink")'ड़'、苏格兰英语（有时）                                                                                                                                                                                     |
| [浊小舌擦音](../Page/浊小舌擦音.md "wikilink")\[ʁ\] | [丹麦语](../Page/丹麦语.md "wikilink")、[荷兰语](../Page/荷兰语.md "wikilink")、[法语](../Page/法语.md "wikilink")、[德语](../Page/德语.md "wikilink")、[希伯来语](../Page/希伯来语.md "wikilink")、[葡萄牙语](../Page/葡萄牙语.md "wikilink")                                                               |
| [小舌顫音](../Page/小舌顫音.md "wikilink")\[ʀ\]   | [德语](../Page/德语.md "wikilink")                                                                                                                                                                                                                                      |

## 字符编码

| 字符编码                              | [ASCII](../Page/ASCII.md "wikilink") | [Unicode](../Page/Unicode.md "wikilink") | [EBCDIC](../Page/EBCDIC.md "wikilink") | [摩斯电码](../Page/摩斯电码.md "wikilink") |
| --------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- | ---------------------------------- |
| [大写R](../Page/大写字母.md "wikilink") | 82                                   | 0052                                     | 217                                    | `·-·`                              |
| [小写r](../Page/小写字母.md "wikilink") | 114                                  | 0072                                     | 153                                    |                                    |

## 其他表示方法

## 参看

### r的变体

  - [半r](../Page/半r.md "wikilink")

  - [小舌颤音](../Page/小舌颤音.md "wikilink")

  - [齒齦邊閃音](../Page/齒齦邊閃音.md "wikilink")

### 其他字母中的相近字母

  - （[希腊字母Rho](../Page/希腊字母.md "wikilink")）

  - （[西里尔字母Er](../Page/西里尔字母.md "wikilink")）

### 与R相似但无任何关系的字母

  - （西里尔字母Ya）

[Category:拉丁字母](../Category/拉丁字母.md "wikilink")