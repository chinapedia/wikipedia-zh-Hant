**Rad51**是[真核生物體內的一種](../Page/真核生物.md "wikilink")[蛋白質](../Page/蛋白質.md "wikilink")，与原核生物的[RecA](../Page/RecA.md "wikilink")[同源](../Page/同源.md "wikilink")，是一种高度保守的蛋白，從[酵母菌到](../Page/酵母菌.md "wikilink")[人類之間的變異不大](../Page/人類.md "wikilink")。人類的Rad51含有339個[氨基酸](../Page/氨基酸.md "wikilink")，於[同源重組中扮演主要角色](../Page/同源重組.md "wikilink")，參與搜尋同源部位與DNA的配對過程。

人類的Rad51的基因位於[第15號染色體](../Page/15號染色體_\(人類\).md "wikilink")。包括此基因在內，[哺乳類共有七種類似recA的基因](../Page/哺乳類.md "wikilink")，包括Rad51、Rad51L1/B、Rad51L2/C、Rad51L3/D、XRCC2、XRCC3以及DMC1\[1\]。

## 參考文獻

  - Galkin,V .E. *et al*. The Rad51/RadA N-Terminal domain activates
    nucleoprotein filament ATPase activity. Structure 14, pp.
    983-992(2006)

<!-- end list -->

  - Pellegrini, L. *et al*. Insights into DNA recombination from the
    structure of a Rad51-BRCA2 complex. Nature 420,287-293(2002)

## 外部連結

  -
[Category:蛋白質](../Category/蛋白質.md "wikilink")

1.  Kawabata,M., Kawabata, T. and Nishibori, M. Role of recA/Rad51
    family proteins in mammals. *Acta Med.Okayama* **v59**, No.1,
    pp.1-9(2005)