《**RAINBOW**》（濱紛彩虹）為[日本歌手](../Page/日本.md "wikilink")[濱崎步發行的第五張專輯](../Page/濱崎步.md "wikilink")，專輯名稱以及同名歌曲中的「A」字正確標示為她自創的符號（見右方）。本作於2002年12月18日於日本發售，距離前張專輯作品的發行不過一年，銷售的首日銷量達到了45萬張，並連續三週登上了[公信榜週榜單的第一名](../Page/公信榜.md "wikilink")。至今專輯的銷售量超過180萬，被[日本唱片協會認證為兩百萬專輯](../Page/日本唱片協會.md "wikilink")。

## 背景與主題

在《I
am...》的發行之後，濱崎步開始接納在[日本以外的地區進行表演活動](../Page/日本.md "wikilink")。在2002年[MTV亞洲大獎的表演後](../Page/MTV.md "wikilink")，濱崎步認為以[日文填寫歌詞已經不足以傳遞她的訊息給國外的聽眾](../Page/日文.md "wikilink")。她認知[英語是世界最為通用的](../Page/英語.md "wikilink")[語言](../Page/語言.md "wikilink")，於是她開始在歌曲中加入英文的填詞\[1\]\[2\]。

在這張專輯中，濱崎步還是以[CREA的筆名與D](../Page/CREA.md "wikilink")･A･I共同進行創作。但是在風格上又有了轉變，它包含了搖滾、Trip-hop、黑人靈歌風格的音樂。填詞亦與前作不同，自由地追求愛、女性的迷惘等富有[女性主義色彩的填詞已經漸漸融入歌詞中](../Page/女性主義.md "wikilink")，另外「悲情之夏」也是這張專輯的一個特色。

Sputnikmusic給了專輯一個正面的評論：「《RAINBOW》與其他日本流行音樂對比起來，擁有更西洋的感覺，這對於其他專輯來說可能是一大困難點。」

## 收錄歌曲

## 統計

| 榜單                                       | 最高排名 | 銷量        | 在榜時數 |
| ---------------------------------------- | ---- | --------- | ---- |
| Oricon日間專輯榜 <small>(2002年12月18日)</small> | 1    | —         | 22週  |
| Oricon週間專輯榜 <small>(2002年12月第3週)</small> | 1    | 1,016,482 |      |
| Oricon月間專輯榜 <small>(2003年1月)</small>     | 2    | —         |      |
| Oricon年間專輯榜 <small>(2003年)</small>       | 2    | 1,857,870 |      |
| Oricon累積銷量                               | —    | 1,857,870 |      |

## 參考資料

<references/>

[Category:濱崎步專輯](../Category/濱崎步專輯.md "wikilink")
[Category:2002年音樂專輯](../Category/2002年音樂專輯.md "wikilink")
[Category:RIAJ百萬認證專輯](../Category/RIAJ百萬認證專輯.md "wikilink")
[Category:Oricon百萬銷量達成專輯](../Category/Oricon百萬銷量達成專輯.md "wikilink")
[Category:2002年Oricon專輯週榜冠軍作品](../Category/2002年Oricon專輯週榜冠軍作品.md "wikilink")
[Category:2003年Oricon專輯週榜冠軍作品](../Category/2003年Oricon專輯週榜冠軍作品.md "wikilink")

1.
2.  Original text from *Cawaii*:
    "英語を解禁にしようと思ったのは、アルバムの「Rainbow」からなんだけど、あのころMTVなどで賞をいただいてアジアでパフォーマンスするって
    い
    うようをことか何回か続いたの。そのときに、日本語だとやっぱけ傳れってをいかもっていう気がしちゃって、単純な少女はやっぱけ世界共通語だよな\~っで
    思ったんだよね。"