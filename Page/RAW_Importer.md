**StepOK RAW 导入器**（**RAW Importer**）是一个由[Step
OK图形工作室出品的具备导入](../Page/Step_OK.md "wikilink")、转换[RAW图形格式文件的](../Page/RAW.md "wikilink")[免费软件](../Page/免费软件.md "wikilink")。

最新版本为1.2。

## 用途

[RAW格式在提供相比于传统格式](../Page/RAW.md "wikilink")，如[JPEG等更完整的摄影信息的同时](../Page/JPEG.md "wikilink")，却由于并不是一个唯一确定的图形格式标准，从而每家相机制造厂商都可以对RAW格式进行自己的修改而得到自己的标准，例如，[佳能拥有自己的CRW](../Page/佳能.md "wikilink")，[尼康有NEF](../Page/尼康.md "wikilink")，这些标准在广义上都属于RAW，但是彼此并不相同，在最糟糕的情况下，只能由相机厂商自己的软件打开并转换成为以往的图形格式，如[JPEG](../Page/JPEG.md "wikilink")，再输出到一些图形处理软件，如[PhotoShop等进行后期的润饰](../Page/Adobe_Photoshop.md "wikilink")。

在这样的情况下，这款软件诞生了。其目的即在于兼容大部分的RAW格式，而能进行统一的调整和前期修饰，或转换为一个较为通用的格式，如DNG（Adobe
Digital Negative）等。

## RAW格式兼容一览

  - [Adobe](../Page/Adobe.md "wikilink")：Digital Negative（DNG）
  - [佳能](../Page/佳能.md "wikilink")：CR2, CRW
  - [卡西欧](../Page/卡西欧.md "wikilink") RAW
  - [康泰时](../Page/康泰时.md "wikilink") RAW
  - [富士](../Page/富士.md "wikilink")：RAF
  - [柯尼卡美能达](../Page/柯尼卡美能达.md "wikilink")：MRW
  - [徕卡](../Page/徕卡.md "wikilink") RAW
  - [尼康](../Page/尼康.md "wikilink")：NEF
  - [奥林巴斯](../Page/奥林巴斯.md "wikilink")：ORF
  - [索尼](../Page/索尼.md "wikilink")：ARW, SR2, SRF

## 参见

  - [RAW](../Page/RAW.md "wikilink")
  - [Step OK](../Page/Step_OK.md "wikilink")
  - [Picasa](../Page/Picasa.md "wikilink")

## 外部链接

  - [Raw导入器 介绍](http://www.stepok.com/chs/raw_importer.htm) — StepOK
  - [深入相机RAW文档](https://web.archive.org/web/20090518081313/http://www.52hutu.com/show.aspx?ID=63302818557093750020)
    — 52hutu.com

[Category:图像软件](../Category/图像软件.md "wikilink")