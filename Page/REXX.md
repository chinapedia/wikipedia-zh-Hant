**REXX**（Restructured Extended
Executor）是[IBM在](../Page/IBM.md "wikilink")1980年代发明的一种[程序设计语言](../Page/程序设计语言.md "wikilink")。主要用在IBM的[大型计算机](../Page/大型计算机.md "wikilink")（Mainframe
Computer）上，但在大部份其他的平台上也可以找到它的[解译器或](../Page/解译器.md "wikilink")[编译器](../Page/编译器.md "wikilink")。另有[面向对象的版本](../Page/面向对象.md "wikilink")，称为ObjectRexx。

## 語言特色

\*;[十進位](../Page/十进制.md "wikilink")[浮点数算術](../Page/浮点数.md "wikilink")：能準確計算表示和計算0.1之類的數字。

## 外部链接

  - [REXX語言官方網站](http://www.ibm.com/rexx/)
  - [人人可用的Rexx](http://www-128.ibm.com/developerworks/cn/linux/l-rexx.html)使用自由软件Rexx的实现来编写脚本
  - [Regina](http://regina-rexx.sourceforge.net)：其中一個Open
    Source的REXX语言实现。