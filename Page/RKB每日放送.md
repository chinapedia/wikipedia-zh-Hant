**RKB每日放送株式會社**（，英語譯名：）是[日本的](../Page/日本.md "wikilink")[廣播公司之一](../Page/廣播.md "wikilink")，在[福岡縣經營](../Page/福岡縣.md "wikilink")[無線](../Page/地面電視.md "wikilink")[電視台及](../Page/電視台.md "wikilink")[AM](../Page/調幅廣播.md "wikilink")[廣播電台](../Page/廣播電台.md "wikilink")，為一\[1\]。總部位於[福岡的](../Page/福岡市.md "wikilink")[百道濱](../Page/百道濱.md "wikilink")。其名稱中的「RKB」是舊公司名**九州廣播電台**（**R**adio
**K**yushu
**B**roadcasting）的縮寫。1951年12月1日時先以廣播電台（Radio九州）的身份成立，而電視台的部分則是在7年後的1958年3月1日開台。

RKB每日放送電視台是[日本新聞網](../Page/日本新聞網_\(TBS\).md "wikilink")（JNN，一般也通稱為[TBS系](../Page/TBS系.md "wikilink")）旗下電視台之一，[呼號為](../Page/呼號.md "wikilink")**JOFR-DTV**。至於台號**JOFR**的（）是以（AM
stereo）方式播放，主要頻道AM 1278[千赫](../Page/千赫.md "wikilink")。

除了主要播出範圍福岡縣之外，RKB每日放送在隔鄰的[佐賀縣也擁有不少的](../Page/佐賀縣.md "wikilink")[觀眾群](../Page/觀眾.md "wikilink")。

2016年4月1日，RKB每日放送導入制度，成為[西日本首家導入此制度的廣播公司](../Page/西日本.md "wikilink")。RKB每日放送原本的公司法人「RKB每日放送株式會社」更名為「」，大部分業務則轉移給新成立的「RKB每日放送株式會社」，為前者的全資子公司。

## 參考資料

## 外部連結

  - [RKB每日放送](https://rkb.jp/)

[Category:日本電視台](../Category/日本電視台.md "wikilink")
[Category:日本廣播電台](../Category/日本廣播電台.md "wikilink")
[Category:日本新聞網 (TBS)](../Category/日本新聞網_\(TBS\).md "wikilink")
[9407](../Category/福岡證券交易所上市公司.md "wikilink")
[Category:福岡縣公司](../Category/福岡縣公司.md "wikilink")

1.  日本對兼營電台與電視台的廣播業者之稱呼。