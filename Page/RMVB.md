**RealMedia Variable
Bitrate**，縮寫RMVB，是由[RealNetworks开发的](../Page/RealNetworks.md "wikilink")[RealMedia多媒体封装格式的一种动态](../Page/RealMedia.md "wikilink")[比特率扩展](../Page/比特率.md "wikilink")。采用此格式文件的[后缀名为](../Page/后缀名.md "wikilink")“.rmvb”。

## 與rm的差異

相較於一般使用了固定位元率的RealMedia視訊文件格式「.rm」，RMVB副檔名多出的「VB」指的是可變動的位元率（Variable
Bitrate）。也由於變動位元率的緣故，RMVB可以在畫面變動不大或是簡單的影片片段，降低位元率；反之，在畫面變動大或是動態的影片片段，提高位元率，藉以在檔案大小以及畫質之間取得一個平衡。一般而言，在較低的平均位元率下，RMVB格式的影像畫質會較原先固定位元率的RealMedia檔案來得要好。

RealMedia用了許多與[MPEG-4 Part
10編解碼器](../Page/MPEG-4_Part_10.md "wikilink")（像是[H.264](../Page/H.264.md "wikilink")）相似的壓縮技術。

## 使用

RMVB在傳遞亞洲地區的影片，尤其是[動畫和中文的影集](../Page/動畫.md "wikilink")，非常的流行。因此，在檔案分享平台上，例如[BitTorrent](../Page/BitTorrent.md "wikilink")、[eDonkey和](../Page/eDonkey.md "wikilink")[Gnutella上](../Page/Gnutella.md "wikilink")，RMVB非常流行。

主要的媒體播放程式對於RMVB的支援有限，一般是在[Windows平台上](../Page/Windows.md "wikilink")。包括商業軟體[RealPlayer
10](../Page/RealPlayer.md "wikilink")，和在使用了適當的[DirectShow過濾器](../Page/DirectShow.md "wikilink")、或是[Real
Alternative的開放原始碼軟體](../Page/Real_Alternative.md "wikilink")[Media
Player
Classic](../Page/Media_Player_Classic.md "wikilink")。[MPlayer和](../Page/MPlayer.md "wikilink")[totem等程序](../Page/totem.md "wikilink")，可以在[Linux或](../Page/Linux.md "wikilink")[UNIX為基礎的](../Page/UNIX.md "wikilink")[x86電腦上](../Page/x86.md "wikilink")，藉由使用[WIN32的](../Page/WIN32.md "wikilink")[動態連接庫](../Page/動態連接庫.md "wikilink")（[DLL](../Page/DLL.md "wikilink")），或是原生的封閉原始碼函式庫播放。

## 檔案識別

  - 在\*.rmvb檔的最前面4個[bytes是](../Page/byte.md "wikilink")"`.RMF`"（標準的RealMedia格式），或是[16進制的](../Page/16進制.md "wikilink")`2E 52 4D 46`，這可以讓電腦系統更容易的識別檔案。

## 相關條目

  - [RealVideo](../Page/RealVideo.md "wikilink")
  - [RealPlayer](../Page/RealPlayer.md "wikilink")

## 外部連結

  - [RealNetworks](http://www.real.com)
  - [RealMedia](http://wiki.multimedia.cx/index.php?title=RealMedia)
    File Format
  - [CREATION為香港首台支援播放RMVB的IDTV](https://web.archive.org/web/20070320214115/http://www.creation-digital.jp/)
    CREATION為香港首台支援播放RMVB的IDTV

[Category:扩展名](../Category/扩展名.md "wikilink")
[Category:文件格式](../Category/文件格式.md "wikilink")