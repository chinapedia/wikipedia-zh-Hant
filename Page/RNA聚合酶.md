[5iy8.jpg](https://zh.wikipedia.org/wiki/File:5iy8.jpg "fig:5iy8.jpg")

**RNA聚合酶**（、、、，[EC](../Page/EC編號.md "wikilink")2.7.7.6）或稱**核糖核酸聚合酶**，是一種負責從[DNA或](../Page/DNA.md "wikilink")[RNA模板製造RNA的](../Page/RNA.md "wikilink")[酶](../Page/酶.md "wikilink")。RNA聚合酶是通過稱為[轉錄的過程來建立RNA鏈](../Page/轉錄.md "wikilink")，以完成這個工程。在[科學上](../Page/科學.md "wikilink")，RNA聚合酶是一個在RNA轉錄本3'端[聚合](../Page/聚合.md "wikilink")[核糖核甘酸的](../Page/核糖核甘酸.md "wikilink")[核苷轉移酶](../Page/核苷轉移酶.md "wikilink")。RNA聚合酶是一種非常重要的酶，且可在所有[生物](../Page/生物.md "wikilink")、[細胞及多種](../Page/細胞.md "wikilink")[病毒中可見](../Page/病毒.md "wikilink")。

RNA聚合酶是於1960年分別由[山姆·懷斯及](../Page/山姆·懷斯.md "wikilink")[霍維茲同時發現](../Page/霍維茲.md "wikilink")。但在此之前，於1959年，[諾貝爾獎頒發給了](../Page/諾貝爾獎.md "wikilink")[塞韋羅·奧喬亞](../Page/塞韋羅·奧喬亞.md "wikilink")，因為他的發現當時認為是RNA聚合酶，但其實是[核糖核酸酶](../Page/核糖核酸酶.md "wikilink")。

## 控制轉錄

[1QKL_Essential_Subunit_Of_Human_Rna_Polymerases_I_IiAnd_Iii_01.png](https://zh.wikipedia.org/wiki/File:1QKL_Essential_Subunit_Of_Human_Rna_Polymerases_I_IiAnd_Iii_01.png "fig:1QKL_Essential_Subunit_Of_Human_Rna_Polymerases_I_IiAnd_Iii_01.png")
控制[轉錄過程會影響](../Page/轉錄.md "wikilink")[基因表現的模式](../Page/基因表現.md "wikilink")，並從而容許[細胞適應不同的環境](../Page/細胞.md "wikilink")、執行[生物內獨特的角色及維持生存所需的](../Page/生物.md "wikilink")[代謝過程](../Page/代謝.md "wikilink")。所以，RNA聚合酶是活動不單是複雜，而且是有高度規律的。在[大腸桿菌中](../Page/大腸桿菌.md "wikilink")，已確認超過100個因子可以修飾RNA聚合酶的活動。

RNA聚合酶可以在特定的[DNA序列](../Page/DNA.md "wikilink")，稱為[啟動子發動](../Page/啟動子.md "wikilink")[轉錄](../Page/轉錄.md "wikilink")。它繼而產生[RNA鏈以補足DNA的](../Page/RNA.md "wikilink")[模板股](../Page/模板股.md "wikilink")。並會加入[核苷酸至RNA股](../Page/核苷酸.md "wikilink")，這個過程稱為「延伸」。在[真核生物的RNA聚合酶可以建立一條長達](../Page/真核生物.md "wikilink")240萬個[核苷的鏈](../Page/核苷.md "wikilink")（等同於[肌萎縮蛋白](../Page/肌萎縮蛋白.md "wikilink")[基因的總長度](../Page/基因.md "wikilink")）。RNA聚合酶會優先在基因末端已編碼的DNA序列（稱為[終止子](../Page/終止子.md "wikilink")）釋放它的RNA轉錄本。

[核糖體會把一些RNA分子會作為](../Page/核糖體.md "wikilink")[蛋白質生物合成的模板](../Page/蛋白質生物合成.md "wikilink")。其他會摺疊成[核酶或](../Page/核酶.md "wikilink")[轉運RNA](../Page/轉運RNA.md "wikilink")（tRNA）分子。第三種可能性是RNA分子會單純地用作控制調節將來的基因表現。（參考[小干擾性RNA](../Page/小干擾性RNA.md "wikilink")）

RNA聚合酶完成一個全新的合成。它能夠這樣造是因為它與起始的核苷酸獨特的相互作用，能把它牢牢地抓住，方便對進入的核苷酸進行化學攻擊。這種獨特的相互作用解釋了為何RNA聚合酶較喜歡以[三磷酸腺苷](../Page/三磷酸腺苷.md "wikilink")（ATP）作為轉錄的開始，依次其次是[三磷酸鳥苷](../Page/三磷酸鳥苷.md "wikilink")（GTP）、[三磷酸尿苷](../Page/三磷酸尿苷.md "wikilink")（UTP）及[三磷酸胞苷](../Page/三磷酸胞苷.md "wikilink")（CTP）。與[DNA聚合酶相反](../Page/DNA聚合酶.md "wikilink")，RNA聚合酶包含了[解旋酶的活動](../Page/解旋酶.md "wikilink")，所以無須另外的[酶來捲開DNA](../Page/酶.md "wikilink")。

## 細菌的RNA聚合酶

在[細菌中](../Page/細菌.md "wikilink")，相同的[酶催化三種](../Page/酶.md "wikilink")[RNA的合成](../Page/RNA.md "wikilink")：[信使RNA](../Page/信使RNA.md "wikilink")（mRNA）、[核糖體RNA](../Page/核糖體RNA.md "wikilink")（rRNA）及[轉運RNA](../Page/轉運RNA.md "wikilink")（tRNA）。

RNA聚合酶是相對大的分子。核心酶有5個亞基（\~400 kDa）：

  - α<sub>2</sub>：這兩個α亞基組合成酶及辨認調節因子。每個亞基有兩個區，αC末端區及αN末端區，分別與[啟動子結合及與聚合酶的其他部份結合](../Page/啟動子.md "wikilink")。
  - β：有著聚合酶的活動，負責催化RNA的合成。
  - β'：與[DNA結合](../Page/DNA.md "wikilink")。
  - ω：還未清楚它的功能。但是它在[恥垢分枝桿菌中似乎是提供保護功能予β](../Page/恥垢分枝桿菌.md "wikilink")'亞基''。

為著與啟動子的特定區域結合，核心酶須有其他亞基，稱為σ。[σ因子大大減低RNA聚合酶與非特定的DNA的關係](../Page/σ因子.md "wikilink")，視乎σ因子本身而增加對某些啟動子區域的獨特性。所以完整的[全酶有著](../Page/全酶.md "wikilink")6個亞基：α<sub>2</sub>、β、β'、σ及ω（\~480
kDa）。RNA聚合酶的結構就有一個長約55Å（即5.5納米）的溝道及直徑為25Å（2.5納米）。這個溝道正好適合20Å（2納米）的DNA雙股。55Å的長度可以接受16[核苷酸](../Page/核苷酸.md "wikilink")。

當不使用時，RNA聚合酶會與[弱結合部位結合](../Page/弱結合部位.md "wikilink")，等待活性啟動子的位點開啟並快速轉換。RNA聚合全酶所以在不使用時不是在[細胞內自由浮動的](../Page/細胞.md "wikilink")。

## 真核生物的RNA聚合酶

[真核生物有著幾種RNA聚合酶](../Page/真核生物.md "wikilink")：

  - [RNA聚合酶Ｉ合成](../Page/RNA聚合酶I.md "wikilink")[核糖體RNA](../Page/核糖體RNA.md "wikilink")（rRNA）前體45S，當成熟後會成為28S、18S及5.8S核糖體RNA，是將來[核糖體的主要](../Page/核糖體.md "wikilink")[RNA部份](../Page/RNA.md "wikilink")。
  - [RNA聚合酶Ⅱ合成](../Page/RNA聚合酶II.md "wikilink")[信使RNA](../Page/信使RNA.md "wikilink")（mRNA）的前體及大部份[小核RNA](../Page/小核RNA.md "wikilink")（snRNA）以及[微型RNA](../Page/微型RNA.md "wikilink")（microRNA）。因為它在[轉錄過程中需要多種](../Page/轉錄.md "wikilink")[轉錄因子才能與](../Page/轉錄因子.md "wikilink")[啟動子結合](../Page/啟動子.md "wikilink")，所以這是現時最多研究的種類。
  - [RNA聚合酶Ⅲ合成](../Page/RNA聚合酶III.md "wikilink")[轉運RNA](../Page/轉運RNA.md "wikilink")（tRNAs）、rRNA
    5S及其他可以在[細胞核及](../Page/細胞核.md "wikilink")[原生質找到的細小的RNA](../Page/原生質.md "wikilink")。
  - 其他在[粒線體及](../Page/粒線體.md "wikilink")[葉綠體的RNA聚合酶種類](../Page/葉綠體.md "wikilink")。

## 古菌的RNA聚合酶

[古菌的RNA聚合酶只有一种](../Page/古菌.md "wikilink")(據推測可能多於一種)，负责所有RNA的合成。古菌RNA聚合酶在结构和催化机理上与都与细菌、真核生物的聚合酶类似，尤其类似于真核生物的[RNA聚合酶Ⅱ](../Page/RNA聚合酶Ⅱ.md "wikilink")\[1\]\[2\]。

古菌RNA聚合酶的研究开展较晚，第一项成果发表于1971年，當時從極端*Halobacterium
cutirubrum*獲得的RNAP是被分離且純化的。
\[3\]從硫磺礦硫化葉菌與芝田硫化葉菌取得的RNAP的晶體結構使得已確認古亞基總數達到13個。\[4\]\[5\]

## 病毒的RNA聚合酶

很多[病毒都有為RNA聚合酶編碼](../Page/病毒.md "wikilink")。相信最多研究的病毒RNA聚合酶是[噬菌體T](../Page/噬菌體.md "wikilink")7。牠的RNA聚合酶是單一亞基的，與在[粒線體及](../Page/粒線體.md "wikilink")[葉綠體所找到的RNA聚合酶相關](../Page/葉綠體.md "wikilink")，並且與[DNA聚合酶](../Page/DNA聚合酶.md "wikilink")[同源](../Page/同源.md "wikilink")。因此很多人相信大部份的病毒聚合酶是從DNA聚合酶演化而來，並不是直接與上述的多亞基聚合酶有所關聯。

病毒聚合酶是繁雜的，且包括一些形態可以使用[RNA](../Page/RNA.md "wikilink")（而非[DNA](../Page/DNA.md "wikilink")）作為模板。[反鏈核糖核酸病毒及](../Page/反鏈核糖核酸病毒.md "wikilink")[雙鏈核糖核酸病毒都是以雙股RNA形式生存](../Page/雙鏈核糖核酸病毒.md "wikilink")。但是，有些[正鏈核糖核酸病毒](../Page/正鏈核糖核酸病毒.md "wikilink")，如[小兒麻痺病毒](../Page/小兒麻痺.md "wikilink")，亦包含這些[RNA依賴性RNA聚合酶](../Page/RNA依賴性RNA聚合酶.md "wikilink")。

## 轉錄輔助因子

有部份[蛋白質可以與RNA聚合酶結合](../Page/蛋白質.md "wikilink")，並修飾其活動。例如[大腸桿菌的greA及greB可以促進RNA聚合酶劈開接近鏈末端](../Page/大腸桿菌.md "wikilink")[RNA模板的能力](../Page/RNA.md "wikilink")。這可以奪回陷入了的聚合酶分子，並且可以校對RNA聚合酶偶然的錯誤。另一種輔助因子Mfd涉及在[轉錄合併修復中](../Page/轉錄合併修復.md "wikilink")，而其他輔助因子則都是負責調節作用，即幫助RNA聚合酶選擇是否表現某些[基因](../Page/基因.md "wikilink")。

## 纯化

RNA聚合酶可以用以下方式纯化：

  - [磷酸纖維素柱層析](../Page/磷酸纖維素柱層析.md "wikilink")
  - [甘油梯度離心](../Page/甘油梯度離心.md "wikilink")
  - [DNA柱層析](../Page/DNA柱層析.md "wikilink")
  - [離子交換柱](../Page/離子交換柱.md "wikilink")
  - 一併使用磷酸纖維素柱層析加上DNA柱層析

## 內部連結

  - [DNA聚合酶](../Page/DNA聚合酶.md "wikilink")
  - [T7核糖核酸聚合酶](../Page/T7核糖核酸聚合酶.md "wikilink")

## 外部連結

  - [DNAi](http://www.dnai.org)

  -
  -
  - [RNA Polymerase - Synthesis RNA from DNA
    Template](https://web.archive.org/web/20101123181000/http://bioteachnology.com/enzyme/rna-polymerase-synthesis-rna-from-dna-template)

  - [3D macromolecular structures of RNA Polymerase from the EM Data
    Bank(EMDB)](http://www.pdbe.org/emsearch/rna\\%20polymerase)

[Category:基因表現](../Category/基因表現.md "wikilink")
[Category:RNA](../Category/RNA.md "wikilink")
[Category:酶](../Category/酶.md "wikilink") [Category:EC
2.7.7](../Category/EC_2.7.7.md "wikilink")

1.  Korkhin, Y., U. Unligil, O. Littlefield, P. Nelson, D. Stuart, P.
    Sigler, S. Bell and N. Abrescia (2009). "Evolution of Complex RNA
    Polymerases: The Complete Archaeal RNA Polymerase Structure." PLoS
    biology 7(5): e102.
2.  Werner, F. (2007). "Structure and function of archaeal RNA
    polymerases." Molecular microbiology 65(6): 1395-1404.
3.  Louis, B. G. and P. S. Fitt (1971). "Nucleic acid enzymology of
    extremely halophilic bacteria. Halobacterium cutirubrum
    deoxyribonucleic acid-dependent ribonucleic acid polymerase." The
    Biochemical journal 121(4): 621-627.
4.  Korkhin, Y., U. Unligil, O. Littlefield, P. Nelson, D. Stuart, P.
    Sigler, S. Bell and N. Abrescia (2009). "Evolution of Complex RNA
    Polymerases: The Complete Archaeal RNA Polymerase Structure." PLoS
    biology 7(5): e102.
5.  Hirata, A., B. Klein and K. Murakami (2008). "The X-ray crystal
    structure of RNA polymerase from Archaea." Nature 451(7180):
    851-854.