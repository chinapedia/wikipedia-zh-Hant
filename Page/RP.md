**RP**、**Rp**或**rp**可能指：

## 科学与技术

  - [原型速成](../Page/原型速成.md "wikilink")（），一种制造与工程过程
  - [背面投影](../Page/背面投影效果.md "wikilink")（），将图像从背面投影到投影幕上
  - [红磷](../Page/红磷.md "wikilink")（），[磷的一种同素异形体](../Page/磷.md "wikilink")
  - [反相色谱法](../Page/反相色谱法.md "wikilink")（），一种色谱技术
  - [RP-3](../Page/RP-3.md "wikilink")，一种火箭弹
  - [RP (复杂度)](../Page/RP_\(复杂度\).md "wikilink")（），随机多项式时间，计算复杂度理论中的一类
  - 数学
      - [实射影直线](../Page/实射影直线.md "wikilink")（）
      - [实射影平面](../Page/实射影平面.md "wikilink")（）
      - [实射影空间](../Page/实射影空间.md "wikilink")（）
  - 製圖
      - [參考面](../Page/參考面.md "wikilink")（），一种色谱技术

## 医学与生物学

  - [根治性前列腺切除术](../Page/根治性前列腺切除术.md "wikilink")（）
  - [視網膜色素變性](../Page/視網膜色素變性.md "wikilink")（）

## 体育运动

  - [后援投手](../Page/后援投手.md "wikilink")（），一个棒球术语
  - **RP**，一种用于攀岩的小岩石塞，因Rowland Pauligk得名

## 商业与金融

  - [回购协议](../Page/回购协议.md "wikilink")（）
  - [保留价格](../Page/保留价格.md "wikilink")
  - [盧比](../Page/盧比.md "wikilink")（），若干国家所使用的货币的常用名称
  - [印尼盾](../Page/印尼盾.md "wikilink")（），印度尼西亚的官方货币

## 人名

  - [羅伯·派汀森](../Page/羅伯·派汀森.md "wikilink")（）
  - [罗恩·保罗](../Page/罗恩·保罗.md "wikilink")（）

## 地点

  - [菲律宾共和国](../Page/菲律宾共和国.md "wikilink")（）
  - [波兰共和国](../Page/波兰共和国.md "wikilink")（）
  - [莱茵兰-普法尔茨](../Page/莱茵兰-普法尔茨.md "wikilink")（）

## 宗教信仰

  - 改革长老会（），[改革长老教会中的一支](../Page/改革长老教会.md "wikilink")

## 语言学

  - [公认发音](../Page/公认发音.md "wikilink")（）
  - [Rioplatense Spanish](../Page/Rioplatense_Spanish.md "wikilink")

## 其他

  - [人品](../Page/人品.md "wikilink")，網路用語。為漢語拼音**R**en
    **P**in的縮寫，一般指運氣的含義，在網路遊戲中經常說"我RP太差了"，指的就是我運氣太差了，太背了的意思。