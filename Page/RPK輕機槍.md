**RPK**（[俄語](../Page/俄語.md "wikilink")：****，****）是[蘇聯在](../Page/蘇聯.md "wikilink")1959年為[蘇軍裝備以替換](../Page/蘇軍.md "wikilink")[RPD的](../Page/RPD輕機槍.md "wikilink")[輕機槍](../Page/輕機槍.md "wikilink")，發射[7.62×39毫米口徑M](../Page/7.62×39mm.md "wikilink")1943[中間型威力槍彈](../Page/中間型威力槍彈.md "wikilink")，屬於蘇聯的第二代班支援武器。

## 歷史

RPK由[卡拉什尼科夫從](../Page/米哈伊爾·季莫費耶維奇·卡拉什尼科夫.md "wikilink")[AK-47改良型](../Page/AK-47突擊步槍.md "wikilink")[AKM型步槍的基礎上改進而成](../Page/AKM.md "wikilink")，並保持著[AK-47的良好效能及可靠性](../Page/AK-47突擊步槍.md "wikilink")。

初期在十人步兵班中可配備一把RPK作[班用機槍](../Page/輕機槍.md "wikilink")，直至1970年代後期，小口徑的[AK-74及](../Page/AK-74突擊步槍.md "wikilink")[RPK-74開始裝備蘇軍](../Page/RPK-74輕機槍.md "wikilink")。儘管如此，大量RPK仍舊裝備蘇軍，直至現在。

RPK現今在[北韓](../Page/北韓.md "wikilink")、[羅馬尼亞](../Page/羅馬尼亞.md "wikilink")、[芬蘭](../Page/芬蘭.md "wikilink")、[越南及](../Page/越南.md "wikilink")[塞爾維亞等部分國家都獲授權生產或私自仿製及使用](../Page/塞爾維亞.md "wikilink")。

## 設計

RPK採用長、重槍管，有效射程及槍口初速比AK-47高，槍口裝有新型制退器以降低連續射擊時的後坐力，備有可提高射擊精確度及方便伏姿射擊的鋼板壓鑄成型式摺疊[兩腳架](../Page/兩腳架.md "wikilink")。

RPK的照門重新設計並增加了風偏調整令遠程射擊精確度有所提高，改用適合機槍使用的改進型大型木製固定[槍托](../Page/槍托.md "wikilink")（類似[RPD](../Page/RPD輕機槍.md "wikilink")）以保持槍枝穩定性。

RPK通用40發香蕉型[彈匣](../Page/彈匣.md "wikilink")、75發專用[彈鼓](../Page/彈鼓.md "wikilink")（俄制／中國製）及[AK-47的](../Page/AK-47突擊步槍.md "wikilink")30發香蕉型7.62×39毫米彈匣，令火力持續性提高。然而，由於它使用固定槍管，無法長時間連續射擊，實際上只屬於重槍管[自動步槍](../Page/自動步槍.md "wikilink")。

## 使用國

[2011._День_защиты_детей_в_Донецке_089.jpg](https://zh.wikipedia.org/wiki/File:2011._День_защиты_детей_в_Донецке_089.jpg "fig:2011._День_защиты_детей_в_Донецке_089.jpg")
[Soviet_RPK.JPEG](https://zh.wikipedia.org/wiki/File:Soviet_RPK.JPEG "fig:Soviet_RPK.JPEG")
[Russian_RPKs_DM-SD-04-07062.JPEG](https://zh.wikipedia.org/wiki/File:Russian_RPKs_DM-SD-04-07062.JPEG "fig:Russian_RPKs_DM-SD-04-07062.JPEG")
[Iraq_RPK_Machine_Gun.jpg](https://zh.wikipedia.org/wiki/File:Iraq_RPK_Machine_Gun.jpg "fig:Iraq_RPK_Machine_Gun.jpg")
[Iraqi_soldier_with_RPK.jpg](https://zh.wikipedia.org/wiki/File:Iraqi_soldier_with_RPK.jpg "fig:Iraqi_soldier_with_RPK.jpg")
[Familiarization_with_a_RPK_during_Exercise_Rescue_Eagle_2000.jpg](https://zh.wikipedia.org/wiki/File:Familiarization_with_a_RPK_during_Exercise_Rescue_Eagle_2000.jpg "fig:Familiarization_with_a_RPK_during_Exercise_Rescue_Eagle_2000.jpg")
[Mongolian_soldiers_in_2007.jpg](https://zh.wikipedia.org/wiki/File:Mongolian_soldiers_in_2007.jpg "fig:Mongolian_soldiers_in_2007.jpg")
[Georgian_Army_soldiers_on_firing_range_DF-SD-04-11509.JPG](https://zh.wikipedia.org/wiki/File:Georgian_Army_soldiers_on_firing_range_DF-SD-04-11509.JPG "fig:Georgian_Army_soldiers_on_firing_range_DF-SD-04-11509.JPG")及RPK作射擊訓練的格魯吉亞士兵\]\]

  -
  -
  -
  -
  -
  -
  -
  - —採用[Zastava M72及其衍生型](../Page/扎斯塔瓦M72輕機槍.md "wikilink")。

  -
  -
  -
  -
  -
  -
  -
  -
  -
  - —採用Zastava M72及其衍生型。

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - —採用Zastava M72及其衍生型。

  -
  -
  -
  -
  -
  -
  - —採用Zastava M72及其衍生型。

  -
  -
  -
  -
  -
  - —採用Zastava M72及其衍生型。

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - —採用Zastava M72及其衍生型。

  -
  -
  - —採用Zastava M72及其衍生型。

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
### 前使用國

  -
  -
  - —使用本土生產的Zastava M72及其衍生型。

## 型號

### 俄羅斯／蘇聯

  - **RPKS**—傘兵部隊版本（改為木製摺疊[槍托](../Page/槍托.md "wikilink")）
  - **RPKS-N**—配有夜視瞄準具的 RPKS
  - **RPKM**—採用玻璃纖維塑料護木及摺疊槍托的現代化版本，口徑與RPK相同
  - **[RPK-74](../Page/RPK-74輕機槍.md "wikilink")**－1974年[5.45×39毫米版本](../Page/5.45×39mm.md "wikilink")

### 其他

  - **Valmet M78**—[芬蘭仿製版本](../Page/芬蘭.md "wikilink")
  - **TUL 1**—[越南仿製版本](../Page/越南.md "wikilink")

<!-- end list -->

  - **扎斯塔瓦M65**及**[M72](../Page/扎斯塔瓦M72輕機槍.md "wikilink")**—[塞爾維亞仿製版本](../Page/塞爾維亞.md "wikilink")，衍生型包括**M72
    B1/AB1**、**M70
    B1**（摺疊槍托）、**[M77](../Page/扎斯塔瓦M77輕機槍.md "wikilink")**（[7.62×51毫米](../Page/7.62×51mm_NATO.md "wikilink")）及**M90**（[5.56×45毫米](../Page/5.56×45mm_NATO.md "wikilink")）

## 參考

  - [AK-47](../Page/AK-47突擊步槍.md "wikilink")
  - [RPK-74](../Page/RPK-74輕機槍.md "wikilink")
  - [Zastava M72](../Page/扎斯塔瓦M72輕機槍.md "wikilink")
  - [FN Minimi](../Page/FN_Minimi輕機槍.md "wikilink")
  - [M249](../Page/M249班用自動武器.md "wikilink")
  - [HK23](../Page/HK23輕機槍.md "wikilink")
  - [PK/PKM](../Page/PK通用機槍.md "wikilink")
  - [PSL](../Page/PSL狙擊步槍.md "wikilink")

## 流行文化

### [電子遊戲](../Page/電子遊戲.md "wikilink")

  - 2016年—《[逃離塔科夫](../Page/逃離塔科夫.md "wikilink")》:命名為「RPK-16」可更改槍口、槍管、槍托、握把、彈夾、瞄準器等等的配件。

## 註釋

## 參考文獻

  - [Modern
    Firearms](https://web.archive.org/web/20070701112816/http://world.guns.ru/machine/mg15-e.htm)

## 外部連結

  - —[D Boy's
    GunWorld（RPK輕機槍）](http://firearmsworld.net/russain/kalash/ak/rpk.htm)

[Category:輕機槍](../Category/輕機槍.md "wikilink")
[Category:7.62×39毫米槍械](../Category/7.62×39毫米槍械.md "wikilink")
[Category:俄羅斯槍械](../Category/俄羅斯槍械.md "wikilink")
[Category:蘇聯槍械](../Category/蘇聯槍械.md "wikilink")
[Category:越戰武器](../Category/越戰武器.md "wikilink")