**RTFM**，是一個[英文](../Page/英文.md "wikilink")[縮寫](../Page/縮寫.md "wikilink")，意思是：「去讀那些他媽的手冊」（**R**ead
**T**he **F**ucking
**M**anual），這句話通常用在回覆那些只要查閱文件就可以解決，拿出來提問只是浪費別人時間的問題。而為了避免這個縮寫單詞，因為用了「fuck」（[他媽的](../Page/他媽的.md "wikilink")）這個單詞而攻擊性、火藥味太重，RTFM也被解釋成「去讀那些愚蠢的手冊」（Read
The Foolish Manual）；有的時候也解釋成「去讀那些友善的手冊」（Read The Friendly
Manual）或「去讀那些寫得不錯的手冊」（Read The Fine
Manual）。另外，有時候就乾脆把「F」拿掉，直接寫成RTM（去讀手冊，Read The
Manual）。

## 起源

最早的起源被认为是出自[LINPACK用户手册目录的一个章节](../Page/LINPACK.md "wikilink")“R.T.F.M.”。

## 衍生

在某些加入[大英國協的國家中](../Page/大英國協.md "wikilink")，許多[黑客則傾向使用](../Page/黑客.md "wikilink")**RTBM**，同樣意指「去讀那些天殺的手冊」（**R**ead
**T**he **B**loody
**M**anual），原因是在這些國家中，[bloody](../Page/wikt:bloody.md "wikilink")
與 fuck 一字同義，不過在[美國則較少有這種用法](../Page/美國.md "wikilink")。

與這個字類似的黑客用語還包括： **RTFS** ：「去讀該死的原始碼」（Read The Fucking Source） **RTFB**
：「去讀該死的已編譯程式」（Read The Fucking
Binary），常常也以[星際大戰中的台詞](../Page/星際大戰.md "wikilink")「用原力啊，[路克](../Page/路克·天行者.md "wikilink")」（Use
The Force, Luke）改寫成「用原始碼啊，路克」（Use The Source, Luke），而縮寫成
**UTSL**，這樣的寫法因為具有幽默的趣味，以及假設程式開發者並沒有提供完善的手冊，而降低使用 RTFM
一字時的攻擊性，特別是，在應用 RTFB
的場合，往往是因為程式執行檔案已經過於老舊，而且欠缺文件，唯一能夠瞭解程式的方法就是直接閱讀[機器碼](../Page/機器碼.md "wikilink")，使用太過攻擊性的詞彙，實在不妥。
**RTFE**:「去读该死的错误」（"Read The Fucking Error") **RTDA**：“去看该死的文章”("Read
The Damn
Article")。另外，在[Slashdot網站上](../Page/Slashdot.md "wikilink")，也可以看到類似的**RTFA**（去讀天殺的文章，Read
The Fucking Article），這個字用在回應某些沒有看清原文內容，就發表沒有關係的留言的人。
**STFW**：“搜一下网络不行吗？”("Search The Fucking
Web")，在1996年開始在[Usenet上也可以看到](../Page/Usenet.md "wikilink")**STFW**（去搜尋天殺的網站吧，Search
The Fucking
Web）這類的字，另外一個版本是**UTFG**（去用天殺的[Google搜尋一番吧](../Page/Google.md "wikilink")，Use
the Fucking
Google）。中国大陆网络上的“**操不百**”（或写作“**艹不百**”，即“**操**你妈，你**不**会**百**度吗？”的缩写）含义也与“RTFM”类似。

## 参见

  - [网络语言](../Page/网络语言.md "wikilink")

## 参考文献

[Category:互联网用语](../Category/互联网用语.md "wikilink")