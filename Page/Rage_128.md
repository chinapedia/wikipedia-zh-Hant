**Rage 128**顯示卡由[ATI推出](../Page/ATI.md "wikilink")，在1999年发布，是針對[Voodoo
II而推出](../Page/Voodoo_II.md "wikilink")。该顯示核心的扩展型号非常多。性能上，**Rage
128**并不顶尖，但各方面的性能都非常平均，尤其是比較出色的[DVD播放](../Page/DVD.md "wikilink")。隨後更推出支持[VIVO和](../Page/VIVO.md "wikilink")[ALL-IN-WONDER版本](../Page/ALL-IN-WONDER.md "wikilink")。

Rage 128支援[AGP](../Page/AGP.md "wikilink")
4X，硬體[MPEG-2加速](../Page/MPEG-2.md "wikilink")，各向异性过滤。顯核架構是128bit。作為第三代顯示卡，Rage
128的性能比[RIVA TNT和Voodoo](../Page/RIVA_TNT.md "wikilink") 2高。Rage
128核心是专为32bit渲染而设计的，當對比16bit渲染時，它的32bit渲染的性能损失不到5%。而其他顯示卡方面，例如nVidia
TNT和Matrox G200都會有不同程度的20%～50%的性能下降。它的强項是其2D和3D的性能。Rage 128也完善地支援OpenGL
ICD，亦获得了SGI的认证。

但由於推迟了上市，當正式上市時，對手已變成性能强大的Voodoo Ⅲ、[RIVA
TNT2和](../Page/RIVA_TNT2.md "wikilink")[G400](../Page/Matrox_G400.md "wikilink")。

## 形号列表

  - Rage 128 VR - 低端型号
  - Rage 128 GL - 高端型号

## Rage 128 Pro

**Rage 128 Pro**在1999年4月发布，是針對[RIVA
TNT2而推出](../Page/RIVA_TNT2.md "wikilink")。對比Rage
128，它支持[非线性过滤](../Page/非线性过滤.md "wikilink")，[各向异性过滤](../Page/各向异性过滤.md "wikilink")，[纹理压缩](../Page/纹理压缩.md "wikilink")。其各项异性过滤性能比三线性过滤還好。Rage
128 Pro亦升级了三角生成引擎，三角形率是每秒800万个。以上改善令Rage 128 Pro的3D性能比Rage 128高了50%。

它支持LCD显示器，設有DVI输出。亦繼承了Rage 128的優良的2D和視頻質素。

## Rage Fury MAXX

為了提高顯示卡性能，对抗GeForce256,ATI推出了Rage Fury MAXX，亦稱為**曙光女神**計劃，將兩顆 Rage 128
Pro顯核集成在同一塊PCB板上，类似Voodoo
II的[SLI](../Page/SLI.md "wikilink")。它擁有64MB顯存，每个顯核使用32MB
。它的像素填充率是500 Mpixel/sec。它支援最高AGP 4X。但由於性能不佳，價格高昂，這計劃並不成功 。

## All-in-Wonder 128

All-in-Wonder 128於1999年8月发布，它是建于Rage 128核心，是一張多媒体显卡。它的繪圖技術与Rage
128相同，支援16或32MB显存。它内置MPEG-2解码器，能向个人电腦或电視播放DVD。它內建了电視調諧器，能播放电視訊号。亦支援即時视频压缩，令儲存影像檔案大小大幅降低。它可以截屏所有影像，包括输入，动態视频和静止图像。

[en:ATI Rage\#RAGE 128](../Page/en:ATI_Rage#RAGE_128.md "wikilink")

[Category:顯示卡](../Category/顯示卡.md "wikilink")
[Category:冶天科技](../Category/冶天科技.md "wikilink")