[3dragepro.jpg](https://zh.wikipedia.org/wiki/File:3dragepro.jpg "fig:3dragepro.jpg")
**Rage Pro**由ATI推出，在1997年4月發佈。3D Rage
Pro晶片是原生支援Intel的[AGP接口](../Page/AGP.md "wikilink")，有利於材質執行模式、指揮流水線、邊帶尋址和完全2x-模式協定。ATI為這款晶片新加了[三角形設定引擎](../Page/三角形設定引擎.md "wikilink")；亦改善了[透視法修正](../Page/透視法修正.md "wikilink")，[距離模糊和透明化效果](../Page/距離模糊.md "wikilink")。新增了[反射高光技術和增強了影像播放功能](../Page/反射高光.md "wikilink")，尤其是[DVD播放](../Page/MPEG2.md "wikilink")。3D
Rage
Pro支援最高8MB[SGRAM或](../Page/SGRAM.md "wikilink")16MB[WRAM](../Page/WRAM.md "wikilink")，取決於針對的市場。由於效果不錯，所以ATI得到大沘OEM订单，使ATI在1998年的收入提高了一倍。

Rage Pro支援矢量纹理压缩，像素填充率是45M
Pixels/s，每秒能生成120万个三角形，支援单周期的三线性过滤。但初期的驱动严重限制了它的效能，不但無法超越[Voodoo](../Page/Voodoo.md "wikilink")，更落后于[RIVA
128](../Page/RIVA_128.md "wikilink")。原因就是缺乏對[OpenGL支援](../Page/OpenGL.md "wikilink")，難以成為一個堅固的遊戲解決方案。1998年2月，ATI企圖以新瓶舊酒的方式，重新發佈Rage
Pro顯示卡。他們將Rage Pro易名為"Rage Pro
Turbo"，並配合新的驅動程式（4.10.2312版本），希望效能有40%的增長。實際上，這個驅動程式只對[標準檢查程式有效](../Page/標準檢查程式.md "wikilink")，例如[Ziff-Davis的](../Page/Ziff-Davis.md "wikilink")[3D
Winbench](../Page/3D_Winbench.md "wikilink") 98和[Final
Reality](../Page/Final_Reality.md "wikilink")。到真正的遊戲程式時，效能實際上減少了。

3D Rage Pro主要售賣成Xpert@Work或Xpert@Play，他們的主要分別是Xpert@Play版本擁有TV-out接口。

## Rage LT

[ATI_Rage_LT_Pro_AGP.png](https://zh.wikipedia.org/wiki/File:ATI_Rage_LT_Pro_AGP.png "fig:ATI_Rage_LT_Pro_AGP.png")
Rage LT通常裝備在主機版或筆記型電腦上。這顆晶片與Rage
Pro非常相似，它們都支援相同的應用。它為筆記型電腦的[液晶顯示器整合了LVDS發送器和高級電源管理](../Page/液晶顯示器.md "wikilink")。Rage
LT PRO亦提供"Filtered Ratiometric
Expansion"，可以自動將圖像縮放至全螢幕尺寸。ATI的ImpacTV2+亦被整合到Rage
LT PRO晶片中，這樣就可以令顯示卡支援多螢幕顯示；或將訊號同時輸出到TV、CRT和LCD。另外，Rage LT
PRO可以同時驅動兩個不同解析度／刷新率的顯示器，因為它採用了双獨立[CRT控制器](../Page/CRT.md "wikilink")。

*Rage LT Pro*晶片即是稍微較快的版本。

## Rage XL

[3dragexl.jpg](https://zh.wikipedia.org/wiki/File:3dragexl.jpg "fig:3dragexl.jpg")
Rage XL是以*Rage
Pro*為基礎的低成本顯示解決方案。這晶片用於很多低端的顯示卡。它亦見於[Intel的主機版上](../Page/Intel.md "wikilink")，最近在2004年使用過，到2006年亦用於伺服器主機版上。它的電源消耗量很低，但仍有能力提供2D加速。

根本上，這款晶片是*Rage Pro*的晶粒縮小版，優化成價錢低廉的解決方案，所以只提供基本的顯示輸出。

[en:ATI Rage](../Page/en:ATI_Rage.md "wikilink")

[Category:顯示卡](../Category/顯示卡.md "wikilink")
[Category:冶天科技](../Category/冶天科技.md "wikilink")