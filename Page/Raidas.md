**Raidas**，1986年成立的[香港](../Page/香港.md "wikilink")[流行音樂二人組合](../Page/流行音樂.md "wikilink")，成員包括[黃耀光及](../Page/黃耀光.md "wikilink")[陳德彰](../Page/陳德彰.md "wikilink")，1988年解散。

## 成員

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出生日期</p></th>
<th><p>出生地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/黃耀光.md" title="wikilink">黃耀光</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳德彰.md" title="wikilink">陳德彰</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 團名由來

在2003年的[香港電台的](../Page/香港電台.md "wikilink")《真音樂》節目當中，陳德彰提及其實當時因爲要參加比賽，所以要給組合取一個名字。在於比賽希望奪獎的前提下，選擇了當時電影《[奪寶奇兵](../Page/奪寶奇兵.md "wikilink")》（）的英文名的第一個字，再改變了串法而成。\[1\]

## 發展歷程

**Raidas**成立於1986年6月，以原創作品《[吸煙的女人](../Page/吸煙的女人.md "wikilink")》参加「[亞太流行歌曲創作大賽香港區決賽](../Page/亞太流行歌曲創作大賽.md "wikilink")」，獲得亞軍而成名並加入樂壇。

Raidas的兩位成員，[黄耀光負責作曲和編曲](../Page/黄耀光.md "wikilink")，[陳德彰則負責主唱](../Page/陳德彰.md "wikilink")。而自《吸煙的女人》開始，他們的歌詞大多數都由同時出道的填詞人[林夕所包辦](../Page/林夕.md "wikilink")，不少人都視他為組合的第三位成員。現時無線電視藝員[蔣志光當時是Raidas的隱形成員](../Page/蔣志光_\(藝員\).md "wikilink")，當年[黃耀光參加創作比賽](../Page/黃耀光.md "wikilink")，原先打算找蔣志光作伙伴，但[蔣志光跟](../Page/蔣志光.md "wikilink")[寶麗金唱片公司簽了約](../Page/寶麗金.md "wikilink")，黃耀光才找陳德彰搭檔，Raidas推出的唱片中，蔣志光全部有參與製作，但沒有出名（僅以筆名小光的名義）。

Raidas的作品，深受80年代英倫[電子音樂的影响](../Page/電子音樂.md "wikilink")，著重旋律的節奏感。當中較流行的有《[傳說](../Page/傳說_\(Raidas專輯\).md "wikilink")》、《[傾心](../Page/傳說_\(Raidas專輯\).md "wikilink")》、《[別人的歌](../Page/傳說_\(Raidas專輯\).md "wikilink")》、《[某月某日](../Page/傳說_\(Raidas專輯\).md "wikilink")》、《危險遊戲》和《沒有路人的都市》等多首。

《傾心》入選[第十屆十大中文金曲](../Page/第十屆十大中文金曲得獎名單.md "wikilink")（1987年度）的「十大中文金曲」之一。\[2\]

Raidas出版了一張[EP和兩張大碟後](../Page/EP.md "wikilink")，黄耀光跟唱片公司就Raidas的音樂風格路向上有分歧，最後黄耀光決定離開Raidas，Raidas就宣佈解散。\[3\]九十年代初，[樂隊熱潮開始式微](../Page/樂隊.md "wikilink")。陳德彰曾以個人歌手身份推出專輯，但反應遠遠不及Raidas時的作品，其後陳德彰退出娛樂圈從事配音工作，近年才間中客串演出；黃耀光則專注宣揚[佛法](../Page/佛法.md "wikilink")，製作[佛教音樂及教授](../Page/佛教.md "wikilink")[瑜伽](../Page/瑜伽.md "wikilink")。

## 唱片

| 專輯 \# | 專輯名稱                                                     | 發行日期       |
| :---- | :------------------------------------------------------- | :--------- |
| 1st   | [吸煙的女人](../Page/吸煙的女人.md "wikilink")                     | 1986年6月    |
| 2nd   | [傳說](../Page/傳說_\(Raidas專輯\).md "wikilink")              | 1987年3月16日 |
| 3rd   | [Raidas 87](../Page/Raidas_87.md "wikilink")             | 1987年12月   |
| 4th   | [陳德彰．Raidas新曲+精選](../Page/陳德彰．Raidas新曲+精選.md "wikilink") | 1989年9月    |
| 5th   | 傳說（HQCD）                                                 | 2010年      |

### 派台歌曲成績

| **派台歌曲四台上榜最高位置**                                         |
| -------------------------------------------------------- |
| 唱片                                                       |
| **1986年**                                                |
| 吸煙的女人                                                    |
| **1987年**                                                |
| 傳說                                                       |
| 傳說                                                       |
| 傳說                                                       |
| 傳說                                                       |
| Raidas 87                                                |
| **1988年**                                                |
| Raidas 87                                                |
| [香港電台六十週年紀念（雜錦碟）](../Page/香港電台六十週年紀念（雜錦碟）.md "wikilink") |
| Raidas 87                                                |
|                                                          |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **0**       |

## 雜錦唱片

  - 《[地球大合唱](../Page/地球大合唱.md "wikilink")》（1987年）
  - 《[大話西遊](../Page/大話西遊.md "wikilink")》（1988年）

## 資料來源

[Category:香港男子演唱團體](../Category/香港男子演唱團體.md "wikilink")
[Category:電子音樂團體](../Category/電子音樂團體.md "wikilink")
[Category:1986年成立的音樂團體](../Category/1986年成立的音樂團體.md "wikilink")
[Category:已解散的男子演唱團體](../Category/已解散的男子演唱團體.md "wikilink")
[Category:香港音樂](../Category/香港音樂.md "wikilink")

1.  [《真音樂》](http://www.rthk.org.hk/classicschannel/realmusic.htm)
    香港電台文教組及香港作曲家及作詞家協會聯合製作 (2003年)

2.  [歷年十大中文金曲頒獎音樂會 第十屆
    (1987)](http://www.rthk.org.hk/classicschannel/goldsong10.htm)

3.