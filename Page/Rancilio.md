**Rancilio**是一個[濃縮咖啡機製造商](../Page/濃縮咖啡.md "wikilink")，由Roberto
Rancilio於1927年在[義大利](../Page/義大利.md "wikilink")[米蘭Parabiago創立](../Page/米蘭.md "wikilink")。Rancilio因其創新的分銷網絡、[工業設計與高度可靠性而著名](../Page/工業設計.md "wikilink")。

## 歷史

Rancilio初期最為人所知的機器為La
Regina直立咖啡機，與[Bezzera及](../Page/Bezzera.md "wikilink")[Victoria
Arduino早期的機器功能以及它們](../Page/Victoria_Arduino.md "wikilink")「[美麗年代](../Page/美麗年代.md "wikilink")」的樣式相似。

  - 1950年－Rancilio發售直立蒸煮機器 Invicta
    Horizzontal，証明戰後改為朝向簡約的[現代樣式發展](../Page/現代性.md "wikilink")，
    可是該機器仍使用二十世紀早期的蒸煮方式。
  - 1953年－Rancilio采用由[Gaggia發明的杠杆活塞技術](../Page/Gaggia.md "wikilink")。
  - 1957年－擁有自動杠杆處理的H/L Automatica型號發售。
  - 1961年－Rancilio跟隨新科技的趋勢使用由[Faema發明的連續傳送蒸煮技術](../Page/Faema.md "wikilink")。
  - 1965年－Rancilio妥托工業設計師Marco Zanuso設計Rancilio Z8。
  - 1974年－Marco Zanuso設計了Z9，Rancilio持續製作數個 Z系列機器至Z11為止。
  - 1990年代 - S系列發售，在北美市場非常流行。
  - 2001年－Rancilio重整產品線，引進了基本系列Epoca 與Classe系列，以及生產全自動咖啡機Classe 12。

Silvia家用濃縮咖啡機與Rocky咖啡磨豆機令Rancilio在北美非常有名，因該家用機器很多部件都來自商業機器而降低兩者間的差別。

  - 2009年－北京佰特莱正式代理rancilio、egro品牌机器，作为大陆中国区总代理。

## 參考

  - [Rancilio公司歷史](https://web.archive.org/web/20061211083058/http://www.rancilio.it/rancilio/azienda_storia.jsp?id_azienda=2&id_language=2)
  - [北京佰特莱国际贸易有限公司 中国大陆区总代理。www.cafevip.com](http://www.rancilio.com.cn)

## 外部連結

  - [Rancilio官方網站](http://www.rancilio.com)

[Category:咖啡](../Category/咖啡.md "wikilink")
[R](../Category/米兰公司.md "wikilink")
[R](../Category/意大利製造公司.md "wikilink")