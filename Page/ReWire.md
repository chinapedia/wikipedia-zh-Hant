ReWire是一個軟體-{zh-hans:协议; zh-hant:協定}-，由 Propellerhead 公司和
Steinberg公司聯合開發出來的，允許遠程控制（remote control）並傳輸-{zh-hans:数字;
zh-hant:數位}-音頻編輯（digital audio editing），已是音頻／MIDI程式間通訊的一種標準。

ReWire總線技術上為單聲道形式，目前廣泛使用於麥金塔電腦與微軟的作業系統，在系統內建立虚拟的音频通路，传输最多64个通道（channel）的音频流。

## Rewire 伺服器 (混音程式)

  - [Ableton Live](../Page/Ableton_Live.md "wikilink")
  - [Adobe Audition](../Page/Adobe_Audition.md "wikilink")
  - [Logic Audio](../Page/Logic_Audio.md "wikilink")
  - [Arturia Storm](../Page/Arturia_Storm.md "wikilink")
  - [FL Studio](../Page/FL_Studio.md "wikilink")
  - [Cakewalk Sonar](../Page/Cakewalk_Sonar.md "wikilink")
  - [Cockos](../Page/Cockos.md "wikilink")
    [REAPER](../Page/REAPER.md "wikilink")
  - [Companion E\&D Intuem
    RW](../Page/Companion_E&D_Intuem_RW.md "wikilink")
  - [Cycling '74 Max/MSP](../Page/Max_\(software\).md "wikilink")
  - [Digidesign](../Page/Digidesign.md "wikilink") [Pro
    Tools](../Page/Pro_Tools.md "wikilink")
  - [GarageBand](../Page/GarageBand.md "wikilink")
  - [Granted Software
    ReVision](../Page/Granted_Software_ReVision.md "wikilink")
  - [Line 6](../Page/Line_6.md "wikilink")
    [GuitarPort](../Page/GuitarPort.md "wikilink")
  - [Line 6](../Page/Line_6.md "wikilink")
    [RiffTracker](../Page/RiffTracker.md "wikilink")
  - [MOTU](../Page/MOTU.md "wikilink") [Digital
    Performer](../Page/Digital_Performer.md "wikilink")
  - [Opcode Vision DSP](../Page/Opcode_Vision_DSP.md "wikilink")
  - [Opcode Studio Vision](../Page/Opcode_Studio_Vision.md "wikilink")
  - Plogue [Bidule](../Page/Bidule.md "wikilink")
  - [Raw Material](../Page/Raw_Material.md "wikilink")
    [Tracktion](../Page/Tracktion.md "wikilink")
  - Pro Tools on OS9 using [reFuse](../Page/reFuse.md "wikilink")
  - [Samplitude 10](../Page/Samplitude_10.md "wikilink")
  - [Sonoma Wire Works](../Page/Sonoma_Wire_Works.md "wikilink")
    [RiffWorks](../Page/RiffWorks.md "wikilink")
  - [Sony ACID Pro](../Page/Sony_ACID_Pro.md "wikilink")
  - [Steinberg](../Page/Steinberg.md "wikilink")
    [Cubase](../Page/Cubase.md "wikilink")
  - [Steinberg](../Page/Steinberg.md "wikilink")
    [Nuendo](../Page/Nuendo.md "wikilink")
  - [Synapse Audio](../Page/Synapse_Audio.md "wikilink") Orion

## Rewire 終端 (音樂產生程式)

  - [Ableton Live](../Page/Ableton_Live.md "wikilink")
  - [ArKaos VJ](../Page/ArKaos_VJ.md "wikilink")
  - [Arturia](../Page/Arturia.md "wikilink") Storm
  - [Audionaut](../Page/Audionaut.md "wikilink") Obsession
  - [Bitheadz](../Page/Bitheadz.md "wikilink") Retro AS-1
  - [Bitheadz](../Page/Bitheadz.md "wikilink") Unity DS-1
  - [Cockos](../Page/Cockos.md "wikilink")
    [REAPER](../Page/REAPER.md "wikilink")
  - [Digital Salade Toki
    Shot](../Page/Digital_Salade_Toki_Shot.md "wikilink")
  - [FL Studio](../Page/FL_Studio.md "wikilink")
  - [Cakewalk](../Page/Cakewalk.md "wikilink") Project 5
  - [MadTracker](../Page/MadTracker.md "wikilink")
  - [Cycling '74 Max/MSP](../Page/Max_\(software\).md "wikilink")
  - [Plogue Bidule](../Page/Plogue_Bidule.md "wikilink")
  - [Propellerheads](../Page/Propellerheads.md "wikilink")
    [Reason](../Page/Reason.md "wikilink")
  - [Propellerheads](../Page/Propellerheads.md "wikilink")
    [ReBirth](../Page/ReBirth.md "wikilink") RB-338
  - [Sony ACID Pro](../Page/Sony_ACID_Pro.md "wikilink")
  - [Speedsoft VSampler](../Page/Speedsoft_VSampler.md "wikilink")
  - [Tascam](../Page/Tascam.md "wikilink") GigaStudio
  - [Toontrack dfh
    SUPERIOR](../Page/Toontrack_dfh_SUPERIOR.md "wikilink")
  - [Vocaloid](../Page/Vocaloid.md "wikilink")

## 外部連結

  - [Propellerheads' description of
    ReWire](http://www.propellerheads.se/technologies/rewire/)

[Category:音樂軟件](../Category/音樂軟件.md "wikilink")