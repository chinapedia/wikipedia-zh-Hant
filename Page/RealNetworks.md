**RealNetworks**（）是著名的[RealPlayer播放器的開發商](../Page/RealPlayer.md "wikilink")，創立於1995年，公司总部设在[美国](../Page/美国.md "wikilink")[华盛顿州的](../Page/华盛顿州.md "wikilink")[西雅图](../Page/西雅图.md "wikilink")。该公司还制作一些其他的视频播放、编辑软件。RealNetworks也擁有購自listen.com的網路音樂Rhapsody，網路遊戲RealArcade和GameHouse。

## 註釋

## 外部連結

  - [RealNetworks.com](http://www.realnetworks.com)

[Category:美國電腦公司](../Category/美國電腦公司.md "wikilink")
[Category:软件公司](../Category/软件公司.md "wikilink")