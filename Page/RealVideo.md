**RealVideo**是由[RealNetworks於](../Page/RealNetworks.md "wikilink")1997年所開發的一种专用视频压缩格式
——
具体格式随版本而变化，截止到2018年RealVideo的最新版本为RV60。它从开发伊始就被定位为应用于网络视频播放的格式（即[串流媒体](../Page/串流媒体.md "wikilink")）。RealVideo支援各種播放平台，包含[Windows](../Page/Windows.md "wikilink")、[Mac](../Page/Mac.md "wikilink")、[Linux](../Page/Linux.md "wikilink")、[Solaris以及某些](../Page/Solaris.md "wikilink")[移动电话](../Page/移动电话.md "wikilink")。相較於其他的视频编解码器，**RealVideo**通常可以將视频資料壓縮得更小，因此它可以在用[56Kbps
MODEM拨号上网的条件实现不间断的视频播放](../Page/數據機.md "wikilink")。

## 扩展名

RealVideo最早的文件扩展名一般为`.rm`或`.rvm`

现在广泛流行的是[RMVB格式](../Page/RMVB.md "wikilink")，即使用动态比特率的RealVideo，扩展名一般为`.rmvb`

RealNetworks于2018年4月发布的最新的RealVideo HD格式扩展名为`.rmhd`

## 发展前景

随着[网络带宽的提升和](../Page/带宽_\(计算机\).md "wikilink")[存储设备的发展](../Page/存储设备.md "wikilink")，RealVideo在网络传输上不再有体积优势，充裕的网络带宽和更廉价、空间更大的存储设备使人们开始追求更高的画质，因此目前RealVideo已经逐渐被[高级视频编码](../Page/AVC.md "wikilink")、[VP9等新一代视频格式所取代](../Page/VP9.md "wikilink")，但最新的RealVideo
HD格式将有希望与[高效率视频编码等新兴格式抗衡](../Page/HEVC.md "wikilink")

## 视频压缩格式与编解码器版本

RealVideo文件使用多种不同的视频压缩格式进行压缩。 每种视频压缩格式都使用四字符代码标识。
下面是视频压缩格式及编解码器、播放器版本列表：

<table>
<thead>
<tr class="header">
<th><p>编解码器</p></th>
<th><p>四字符表示</p></th>
<th><p>视频压缩格式</p></th>
<th><p>播放器</p></th>
<th><p>其他</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>RealVideo 1</p></td>
<td><p>RV10<br />
RV13</p></td>
<td><p><a href="../Page/H.263.md" title="wikilink">H.263</a></p></td>
<td><p>RealPlayer 5</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>RealVideo G2<br />
或<br />
RealVideo G2+SVT</p></td>
<td><p>RV20</p></td>
<td><p>H.263</p></td>
<td><p>RealPlayer 6</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>RealVideo 8</p></td>
<td><p>RV30</p></td>
<td><p>H.264早期草案[1]</p></td>
<td><p>RealPlayer 8</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>RealVideo 9</p></td>
<td><p>RV40</p></td>
<td><p>H.264[2]</p></td>
<td><p>RealPlayer 9</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>RealVideo 10<br />
或<br />
RealVideo 9 EHQ</p></td>
<td><p>RV40</p></td>
<td><p>H.264[3]</p></td>
<td><p>RealPlayer 10</p></td>
<td><p>这是RV9的改进编码器，它完全向前支持RealPlayer 9，格式和解码器都没有改变，只对编码器进行了改进，因此它与RV9使用相同的四字符表示</p></td>
</tr>
<tr class="even">
<td><p>RealVideo 11<br />
或<br />
RealVideo HD</p></td>
<td><p>RV60</p></td>
<td><p>H.265[4]</p></td>
<td><p>RealPlayer 16</p></td>
<td><p>2018年4月，RV60编码器速度和压缩效率的测试结果在网上发布。 该测试将RealMedia HD与<a href="../Page/HEVC.md" title="wikilink">高效率视频编码</a>，高级视频编码和VP9进行了比较。 结果显示RealMedia HD可以在更高的视觉质量下提供比高效率视频编码更高的压缩率。 此外，RealMedia HD在相似的复杂度设置下比高效率视频编码和VP9更快</p></td>
</tr>
</tbody>
</table>

最新版本的RealPlayer可以播放任何RealVideo文件，使用FFmpeg的程序也可以正常播放除RealVideo
HD之外的RealVideo文件（截止FFmpeg 4.0.3）

其他程序可能不支持所有RealVideo格式，尤其是最新的RealVideo HD格式

## 注释

## 外部連結

  - [RealVideo 10 codec
    description](https://web.archive.org/web/20040804142359/http://www.realnetworks.com/products/codecs/realvideo.html)
  - [Helix Community the open source project of all media framework of
    RealNetworks](https://helixcommunity.org/)
  - [RealMedia
    Splitter](http://sourceforge.net/project/showfiles.php?group_id=82303&package_id=87719)
    [DirectShow](../Page/DirectShow.md "wikilink") filter by
    [Gabest](../Page/Gabest.md "wikilink")（guliverkli,
    sourceforge.net）to split RV40 etc. in .rmvb, .mkv, .dsm, etc.
    Decoder is not included.
  - [Filter for playing RealNetworks audio/video files using Windows
    Media
    Player](http://www.free-codecs.com/download/RealMedia_Splitter.htm)

[Category:视频编解码器](../Category/视频编解码器.md "wikilink")

1.  H.264和AVC都是[高级视频编码的格式名](../Page/AVC.md "wikilink")

2.
3.
4.  H.265是[高效率视频编码（HEVC）的格式名](../Page/HEVC.md "wikilink")