**Real Alternative**是一個用於[Microsoft
Windows作業系統播放](../Page/Microsoft_Windows.md "wikilink")[RealMedia檔案而不需安裝](../Page/RealMedia.md "wikilink")[RealPlayer的](../Page/RealPlayer.md "wikilink")[編解碼器安裝包](../Page/編解碼器.md "wikilink")。該編解碼器可用於任何可使用[DirectShow的媒體播放器](../Page/DirectShow.md "wikilink")，如包含在安裝包內的[Media
Player Classic](../Page/Media_Player_Classic.md "wikilink")。該安裝包由[KL
Software設計](../Page/KL_Software.md "wikilink")，使用了開源視訊濾光器[Real Media
Splitter及RealMedia](../Page/Real_Media_Splitter.md "wikilink")[視訊编解码器](../Page/視訊编解码器.md "wikilink")。

Real Alternative與許多免費的編解碼器安裝包同梱。有些人較喜歡使用Real
Alternative，因RealPlayer令用戶感到不滿\[1\]。

## 合法性

據[RealNetworks所說該編解碼器安裝包並不合法](../Page/RealNetworks.md "wikilink")，因RealNetworks擁有[RealMedia編解碼器的版權](../Page/RealMedia.md "wikilink")，而RealNetworks並沒有授權KL
Software使用\[2\]

## 功能限制

Real Alternative功能上有所限制，它不能播放如RealVideo 10與RealAudio
5.0等較新的RealMedia檔案，.smi及.smil檔案只能播放片段的第一部份，[RealTime
Stream
Protocol](../Page/RealTime_Stream_Protocol.md "wikilink")（rtsp://）不能於除Media
Player Classic外其他DirectShow媒體放器中播放。\[3\]

## 另見

  - [RealPlayer](../Page/RealPlayer.md "wikilink")
  - [Helix](../Page/Helix_project.md "wikilink")
  - [QuickTime Alternative](../Page/QuickTime_Alternative.md "wikilink")
  - [Winamp Alternative](../Page/Winamp_Alternative.md "wikilink")
  - [Media Player Classic](../Page/Media_Player_Classic.md "wikilink")
  - [K-Lite](../Page/K-Lite.md "wikilink")

## 參考資料

## 外部連結

[en:RealPlayer\#Real
Alternative](../Page/en:RealPlayer#Real_Alternative.md "wikilink")

[Category:免費軟體](../Category/免費軟體.md "wikilink")
[Category:视频编解码器](../Category/视频编解码器.md "wikilink")

1.
2.
3.  Limitations to Real Alternative