《**Ribon**》（）是[日本](../Page/日本.md "wikilink")[集英社發行的月刊](../Page/集英社.md "wikilink")[少女漫畫](../Page/少女漫画.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")。1955年8月3日創刊；2015年8月創刊60週年。

《Ribon》和[講談社的](../Page/講談社.md "wikilink")《[Nakayoshi](../Page/Nakayoshi.md "wikilink")》和[小學館的](../Page/小學館.md "wikilink")《[Ciao](../Page/Ciao.md "wikilink")》並列為日本三大少女漫畫雜誌。漫畫的內容在三大少女漫畫雜誌中最接近成人化。相對於《Ciao》，讀者的平均年齡較高。

## 發行

《Ribon》讀者對象以[小學中年級至](../Page/小學.md "wikilink")[中學的女生為主](../Page/初级中学.md "wikilink")，近年來更偏向於小學高年級至中學的讀者群。

1980年代後半至1990年代中期，《Ribon》曾經創下少女漫畫雜誌歷史上最高的發行量記錄（250萬冊），當時有[櫻桃子的連載](../Page/櫻桃子.md "wikilink")[櫻桃小丸子造成爆紅](../Page/櫻桃小丸子.md "wikilink")。1994年以後發行數量逐漸減少。

自從2000年時將[矢澤愛等實力派漫畫家從RIBON分出成](../Page/矢澤愛.md "wikilink")《[Cookie](../Page/Cookie.md "wikilink")》，造成讀者群的分裂，雜誌銷售量再度下降，使得《[Ciao](../Page/Ciao.md "wikilink")》躍升成為第一大少女漫畫雜誌，讀者轉而只去買單行本，但其發行的單行本仍屬Ribon
Mascot Comic系列。

2006年發行量只有40萬冊，大約是顛峰期的1/6，2008年發行量剩下376,666冊，為三大少女漫畫雜誌的最後一名。

## 授權

《Ribon》在台灣正式授權的雜誌是[尖端出版的](../Page/尖端出版.md "wikilink")《[夢夢少女漫畫月刊](../Page/夢夢少女漫畫月刊.md "wikilink")》。

## 雜誌種類

  - 一般：每月3日出刊，每月一本，內容為連載的作品。
  - 增刊號：偶數月出1次

## 目前連載作品

  - HIGH SCORE（[津山千奈美](../Page/津山千奈美.md "wikilink")）
  - [動物小町](../Page/動物小町.md "wikilink")（[前川涼](../Page/前川涼.md "wikilink")）
  - 緒子美美（[園田小波](../Page/園田小波.md "wikilink")）
  - [六月的情書](../Page/六月的情書.md "wikilink")（）（[春田菜菜](../Page/春田菜菜.md "wikilink")）
  - [絕叫學級
    轉生](../Page/絕叫學級_轉生.md "wikilink")（）（[石川惠美](../Page/石川惠美.md "wikilink")）
  - [群青色般炫目的你](../Page/群青色般炫目的你.md "wikilink")（）（[酒井真由](../Page/酒井真由.md "wikilink")）
  - [青春特調檸檬蜂蜜蘇打](../Page/青春特調檸檬蜂蜜蘇打.md "wikilink")（）（[村田真優](../Page/村田真優.md "wikilink")）
  - [在我眼中閃耀的他](../Page/在我眼中閃耀的他.md "wikilink")（）
    ([槙陽子](../Page/槙陽子.md "wikilink"))
  - [古屋先生は杏ちゃんのモノ](../Page/古屋先生は杏ちゃんのモノ.md "wikilink")
    ([香純裕子](../Page/香純裕子.md "wikilink"))

## 過去主要連載作品

  - （[種村有菜](../Page/種村有菜.md "wikilink")）

  - （[半澤香織](../Page/半澤香織.md "wikilink")）

  - [勝利的惡魔](../Page/勝利的惡魔.md "wikilink")（[槙陽子](../Page/槙陽子.md "wikilink")）

  - （小櫻池夏海）

  -
  -
  - （[福米友美](../Page/福米友美.md "wikilink")）

  - （[真城雛](../Page/真城雛.md "wikilink")）

  -
  - [愛你寶貝★★](../Page/愛你寶貝.md "wikilink")（槙陽子）

  - [浪漫時鐘](../Page/浪漫時鐘.md "wikilink")（[槙陽子](../Page/槙陽子.md "wikilink")）

  - [戀愛天](../Page/戀愛天.md "wikilink")（[谷川史子](../Page/谷川史子.md "wikilink")）

  - [小紅帽恰恰](../Page/小紅帽恰恰.md "wikilink")（[彩花珉](../Page/彩花珉.md "wikilink")）

  -
  - [彩蝶100%](../Page/彩蝶100%.md "wikilink")（[武內梢](../Page/武內梢.md "wikilink")）

  - （第1部）（[山岸-{凉}-子](../Page/山岸凉子.md "wikilink")）

  -
  -
  - [戀愛小魔女](../Page/戀愛小魔女.md "wikilink")（[吉住涉](../Page/吉住涉.md "wikilink")）

  -
  -
  - [神風怪盗貞德](../Page/神風怪盗貞德.md "wikilink")（種村有菜）

  -
  - [辣妹當家](../Page/辣妹當家.md "wikilink")（[藤井三穗南](../Page/藤井三穗南.md "wikilink")）

  - [魔法糖果](../Page/魔法糖果.md "wikilink")（[原田妙子](../Page/原田妙子.md "wikilink")）

  - （[萩岩睦美](../Page/萩岩睦美.md "wikilink")）

  - [Good Morning
    Call](../Page/Good_Morning_Call.md "wikilink")（[高須賀由枝](../Page/高須賀由枝.md "wikilink")）

  - （[片桐澪](../Page/片桐澪.md "wikilink")）

  - [呱呱響叮噹](../Page/呱呱響叮噹.md "wikilink")（[藤田鮪](../Page/藤田鮪.md "wikilink")）

  - [近所物語](../Page/近所物語.md "wikilink")（[矢澤愛](../Page/矢澤愛.md "wikilink")）

  - [小孩子的玩具](../Page/小孩子的玩具.md "wikilink")（[小花美穗](../Page/小花美穗.md "wikilink")）

  - （[小椋冬美](../Page/小椋冬美.md "wikilink")）

  - [黑色星期五？](../Page/黑色星期五？.md "wikilink")（[榎本千鶴](../Page/榎本千鶴.md "wikilink")）

  - [櫻姬華傳](../Page/櫻姬華傳.md "wikilink")（[種村有菜](../Page/種村有菜.md "wikilink")）

  - [砂之城](../Page/砂之城.md "wikilink")（[一条由香莉](../Page/一条由香莉.md "wikilink")）

  - [聖龍小公主](../Page/聖龍小公主.md "wikilink")（[松本夏實](../Page/松本夏實.md "wikilink")）

  - [生物彗星WoO](../Page/生物彗星WoO.md "wikilink")（[加加見繪里](../Page/加加見繪里.md "wikilink")）

  - （[小田空](../Page/小田空.md "wikilink")）

  - （[香村陽子](../Page/香村陽子.md "wikilink")）

  - （藤原友佳）

  - [櫻桃小丸子](../Page/櫻桃小丸子.md "wikilink")（[櫻桃子](../Page/櫻桃子.md "wikilink")）

  - [啾♥啾♥啾](../Page/啾♥啾♥啾.md "wikilink")（[中島椿](../Page/中島椿.md "wikilink")）

  -
  - （[本田惠子](../Page/本田惠子.md "wikilink")）

  -
  - [飛天小女警Z](../Page/飛天小女警Z.md "wikilink")（[込由野志穗](../Page/込由野志穗.md "wikilink")）

  - [心跳今夜](../Page/心跳今夜.md "wikilink")（[池野恋](../Page/池野恋.md "wikilink")）

  - [守護天使莉莉佳](../Page/守護天使莉莉佳.md "wikilink")（[秋元康](../Page/秋元康.md "wikilink")、池野戀）

  - （[弓月光](../Page/弓月光.md "wikilink")）

  -
  - （[松乃美佳](../Page/松乃美佳.md "wikilink")）

  - [貓・貓・幻想曲](../Page/貓・貓・幻想曲.md "wikilink")（[高田繪美](../Page/高田繪美.md "wikilink")）

  - [PARTNER](../Page/PARTNER.md "wikilink")（小花美穗）

  -
  - （[太刀掛秀子](../Page/太刀掛秀子.md "wikilink")）

  - （[水野英子](../Page/水野英子.md "wikilink")）

  -
  -
  -
  - （[赤塚不二夫](../Page/赤塚不二夫.md "wikilink")）

  - [百變小姬子](../Page/百變小姬子.md "wikilink")（[水澤惠](../Page/水澤惠.md "wikilink")）

  - [百變小姬子
    彩色](../Page/百變小姬子#百變小姬子_彩色.md "wikilink")（[込由野志穗](../Page/込由野志穗.md "wikilink")）

  -
  - （[高橋由佳利](../Page/高橋由佳利.md "wikilink")）

  - [尋找滿月](../Page/尋找滿月.md "wikilink")（種村有菜）

  - [娃娃★愛你](../Page/娃娃★愛你.md "wikilink")（[椎名愛弓](../Page/椎名愛弓.md "wikilink")）

  -
  -
  - [前進保育園](../Page/前進保育園.md "wikilink")（[樫之木小香](../Page/樫之木小香.md "wikilink")）

  - [星之瞳的剪影](../Page/星之瞳的剪影.md "wikilink")（[柊青](../Page/柊青.md "wikilink")）

  - （[佐藤真樹](../Page/佐藤真樹.md "wikilink")）

  - （[牧美也子](../Page/牧美也子.md "wikilink")）

  - [戀愛無敵\!](../Page/戀愛無敵!.md "wikilink")（[倉橋繪里花](../Page/倉橋繪里花.md "wikilink")）

  - [魔女莎莉](../Page/魔女莎莉.md "wikilink")（[橫山光輝](../Page/橫山光輝.md "wikilink")）

  -
  - [橘子醬男孩](../Page/橘子醬男孩.md "wikilink")（吉住涉）

  - [真由美まゆみ\!](../Page/真由美まゆみ!.md "wikilink")（[田邊真由美](../Page/田邊真由美.md "wikilink")）

  - [真夜中的Kiss](../Page/真夜中的Kiss.md "wikilink")（[持田秋](../Page/持田秋.md "wikilink")）

  - （[山本優子](../Page/山本優子.md "wikilink")）

  - [水之館](../Page/水之館.md "wikilink")（小花美穗）

  - [心之谷](../Page/心之谷.md "wikilink")（[柊青](../Page/柊青.md "wikilink")）

  -
  - （[北澤薰](../Page/北澤薰.md "wikilink")）

  -
  - [愛的木莓寮](../Page/愛的木莓寮.md "wikilink")（春田菜菜）

  - [仙人掌的秘密](../Page/仙人掌的秘密.md "wikilink")（春田菜菜）

  - [甜心汪汪](../Page/甜心汪汪.md "wikilink")（[亞月亮](../Page/亞月亮.md "wikilink")）

  -
  - [紳士同盟](../Page/紳士同盟.md "wikilink")（種村有菜）

  - [青空Pop](../Page/青空Pop.md "wikilink")（小櫻池夏海）

  - [巧克力波斯菊](../Page/巧克力波斯菊.md "wikilink")（春田菜菜）

  - [搖滾天堂](../Page/搖滾天堂.md "wikilink")（酒井真由）

  - [絕對覺醒天使](../Page/絕對覺醒天使.md "wikilink")（種村有菜）

  - [LoveCotton株式會社](../Page/LoveCotton株式會社.md "wikilink")（樫之木小香）

  - MOMO （ 酒井真由 ）

  - [夢色蛋糕師](../Page/夢色蛋糕師.md "wikilink")（松本夏實）

  - [薔薇♥戀愛預感](../Page/薔薇♥戀愛預感.md "wikilink")（[小櫻池夏海](../Page/小櫻池夏海.md "wikilink")）

  - 狼少年的誘惑（[優木那智](../Page/優木那智.md "wikilink")）

  - [蜜糖女孩＊大作戰](../Page/蜜糖女孩＊大作戰.md "wikilink")（[酒井真由](../Page/酒井真由.md "wikilink")）

  - [小雞之戀](../Page/小雞之戀.md "wikilink")（[雪丸萌](../Page/雪丸萌.md "wikilink")）

  - [星塵★眨眼](../Page/星塵★眨眼.md "wikilink")（[春田菜菜](../Page/春田菜菜.md "wikilink")）

  - [青春贊歌哈利路亞](../Page/青春贊歌哈利路亞.md "wikilink")
    ([藤原友佳](../Page/藤原友佳.md "wikilink"))

  - [月夜の王冠](../Page/月夜の王冠.md "wikilink")
    ([藤原友佳](../Page/藤原友佳.md "wikilink"))

  - [人家怎麼可能不在意貓田。](../Page/人家怎麼可能不在意貓田。.md "wikilink")（）（[大詩理惠](../Page/大詩理惠.md "wikilink")）

  - （[酒井真由](../Page/酒井真由.md "wikilink")）

## 外部連結

  - [官方網站](http://ribon.shueisha.co.jp/index.html)

[\*](../Category/Ribon.md "wikilink")
[Category:日本漫畫雜誌](../Category/日本漫畫雜誌.md "wikilink")
[Category:少女漫畫雜誌](../Category/少女漫畫雜誌.md "wikilink")
[Category:月刊漫畫雜誌](../Category/月刊漫畫雜誌.md "wikilink")
[Category:集英社的漫畫雜誌](../Category/集英社的漫畫雜誌.md "wikilink")