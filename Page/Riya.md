**riya**（、）是[福岡縣出身的](../Page/福岡縣.md "wikilink")[女性](../Page/女性.md "wikilink")[歌手](../Page/歌手.md "wikilink")。血型是AB型。

## 概要．簡介

在短大的音樂科畢業後，因為傾慕**[新居昭乃](../Page/新居昭乃.md "wikilink")**，一邊就職音樂鍵盤的講師，一邊與曾是同窗的**[myu](../Page/myu.md "wikilink")**組成「[refio](../Page/refio.md "wikilink")」。與共同製作同人（M3）為中心的音樂。之後，[菊地創聽到她上載在自己網站的曲子後](../Page/菊地創.md "wikilink")，二人便組成「[eufonius](../Page/eufonius.md "wikilink")」。使用乍聽之下以為是英語的「造詞」來作詞。此外，作詞被稱為「」，第一人稱是「」，第二人稱為「」的手法為多。

## 作品

### 單曲

  - 時の向こう側（c/w：空の果てへ）

<!-- end list -->

  -
    2005年3月24日發售（[Lantis](../Page/Lantis.md "wikilink")）
    PSP遊戲『[英雄伝説ガガーブトリロジー 白き魔女](../Page/英雄傳說.md "wikilink")』開頭、結尾主題歌

### 專輯

  - Love Song

<!-- end list -->

  -
    2005年8月31日發售（[Key Sounds
    Label](../Page/Key_Sounds_Label.md "wikilink")）
    始まりの坂／蒼の夢／星なる石／走る／百年の夏／僕らの恋／灰色の羽根／グラモフォン／神話／氷時計／折れない翼／そして物語が終わる／Love
    Song

### 其他樂曲

  - 少女の幻想／オーバー／海鳴り／遠い旅の記憶／一万の軌跡／空に光る／桜抒曲／同じ高み／風の少女／ひとひらの桜／木漏れ日（電腦遊戲「[CLANNAD](../Page/CLANNAD.md "wikilink")」
    印象曲）
  - 廻る世界で＜duet with [霜月はるか](../Page/霜月はるか.md "wikilink")＞（[PlayStation
    2遊戲](../Page/PlayStation_2.md "wikilink")「アカイイト」開頭主題歌）
  - 旅路の果て＜duet with 霜月はるか＞（PlayStation 2遊戲「アカイイト」結尾主題歌）
  - ディアノイア（電腦遊戲「[最終試験くじら](../Page/最終試験くじら.md "wikilink")」開頭主題歌）
  - CRESCENT MOON（PlayStation
    2＆[Dreamcast遊戲](../Page/Dreamcast.md "wikilink")「[水月](../Page/水月.md "wikilink")～迷心～」開頭主題歌）
  - メグメル（電腦遊戲「[CLANNAD](../Page/CLANNAD.md "wikilink")」開頭主題歌）
  - －影二つ－／小さなてのひら（電腦遊戲「CLANNAD」結尾主題歌）
  - 光のほうへ～明日へのJYUMON～（PlayStation 2遊戲「マビノ×スタイル」開頭主題歌）
  - 比翼の羽根（電視動畫「[緣之空](../Page/緣之空.md "wikilink")」開頭主題歌）
  - Traveler's tale ([PlayStation
    4遊戲](../Page/PlayStation_4.md "wikilink")「[ISLAND](../Page/:ISLAND_\(遊戲\)#%E4%B8%BB%E9%A1%8C%E6%9B%B2.md "wikilink")」開頭主題歌)

[Category:日本女歌手](../Category/日本女歌手.md "wikilink")
[Category:動畫歌手](../Category/動畫歌手.md "wikilink")
[Category:福岡縣出身人物](../Category/福岡縣出身人物.md "wikilink")
[Category:同人歌手](../Category/同人歌手.md "wikilink")