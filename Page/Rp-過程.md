**rp-過程**（快質子捕獲過程）包括一連串的質子被[種核捕獲形成重元素](../Page/種核.md "wikilink")。\[1\]。這是結合[S-過程和](../Page/S-過程.md "wikilink")[R-過程的](../Page/R-過程.md "wikilink")[核合成過程](../Page/核合成.md "wikilink")，也許要對當前宇宙許多世代的重元素形成負起責任。然而，他有與其他被提及的過程有所不同而值得特別注意，因為它發生在穩定而富含質子的一側，相對的另一邊則是穩定但富含中子。Rp-過程的終點（他能製造的最重元素）雖然還不能確定，但是目前的研究顯示在[中子星內不可能比](../Page/中子星.md "wikilink")[碲更重](../Page/碲.md "wikilink")。\[2\]雖然更輕的，而且穩定的碲同位素也可以經由α衰變形成，但Rp-過程受到[α衰變的抑制](../Page/α衰變.md "wikilink")，使得終點被限制在[<sup>105</sup>Te](../Page/碲.md "wikilink")，這是被觀測到能進行α衰變的最輕原子核\[3\]。

## 條件

這種過程必須很高的溫度（大約1 x 10<sup>9</sup>
[k或是](../Page/熱力學溫標.md "wikilink")1億K），所以質子可以克服帶電粒子間巨大的[庫侖障壁而作用](../Page/庫侖障壁.md "wikilink")。由於需要巨大的質子流，所以富含氫的環境是必要的先決條件。這個過程的種核被認為可以在熱的[碳氮氧循環形成的期間中產生](../Page/碳氮氧循環.md "wikilink")。在Rp-過程中獨特的質子捕獲必須和(α,p)競爭，因為富含氫流量的環境中通常也有豐富的氦流量。Rp-過程的時間尺度是由β<sup>+</sup>衰變或是[質子滴線設定的](../Page/質子滴線.md "wikilink")，因為[弱交互作用比](../Page/弱交互作用.md "wikilink")[強交互作用和](../Page/強交互作用.md "wikilink")[電磁力慢是眾所周知的事實](../Page/電磁力.md "wikilink")。

## 可能的場所

Rp-過程可能進行的場所被認為是有緻密伴星，即使是低質量的[黑洞或](../Page/黑洞.md "wikilink")[中子星](../Page/中子星.md "wikilink")，的雙星系統。在這些系統中的另一顆恆星（通常是[紅巨星](../Page/紅巨星.md "wikilink")）[供應緻密恆星所需要的物質](../Page/吸積_\(天體物理學\).md "wikilink")。由於這些物質來自共生恆星的表面，因此富含氫與氦，而因為緻密恆星的強[重力場](../Page/重力場.md "wikilink")，物質會以高[速度落向這顆伴星](../Page/速度.md "wikilink")，而通常在路途上會與其他的物質碰撞而形成[吸積盤](../Page/吸積盤.md "wikilink")。在這樣的情況下，中子星的吸積作用，會使物質在表面緩慢的累積，並有著極高的溫度，典型的溫度是1×10<sup>8</sup>
K，同時成為電子[簡併物質](../Page/簡併物質.md "wikilink")。最後，因為物質的電子簡併，他被相信在高熱的大氣層中會出現熱不穩定的狀態。溫度的增加不會導致壓力的增大，因此溫度將持續的上昇，直到引發逃離的[熱核爆炸](../Page/熱核爆炸.md "wikilink")，這就是我們所謂的Rp-過程。在觀測上，中子星雙星的Rp-過程被認為就是[X射線爆發](../Page/X射線爆發.md "wikilink")。

## 參考資料

[R](../Category/原子核物理学.md "wikilink") [R](../Category/核合成.md "wikilink")
[R](../Category/雙星.md "wikilink") [R](../Category/天体物理学.md "wikilink")

1.  Lars Bildsten, ["Thermonuclear Burning on Rapidly Accreting Neutron
    Stars"](http://arxiv.org/abs/astro-ph/9709094v1) in The Many Faces
    of Neutron Stars, ed. R. Buccheri, J. van Paradijs, & M. A. Alpar
    (Kluwer), 419 (1998)
2.
3.