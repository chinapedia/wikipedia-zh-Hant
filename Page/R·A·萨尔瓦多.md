**R·A·萨尔瓦多**（**R. A. Salvatore**，全名**Robert Anthony
Salvatore**，）是美国的一个[小说家](../Page/小说家.md "wikilink")，以撰写[奇幻小说著名](../Page/奇幻小说.md "wikilink")。其笔下最受欢迎的角色是《[黑暗精灵三部曲](../Page/黑暗精灵三部曲.md "wikilink")》中的主人公[黑暗精灵](../Page/黑暗精灵.md "wikilink")[崔斯特](../Page/崔斯特.md "wikilink")。

在大学期间，他在[约翰·罗纳德·瑞尔·托尔金小说](../Page/约翰·罗纳德·瑞尔·托尔金.md "wikilink")《[魔戒](../Page/魔戒.md "wikilink")》的影响下把专业从[信息科学转为](../Page/信息科学.md "wikilink")[新闻](../Page/新闻.md "wikilink")。1981年在[费区堡州立学院获得](../Page/费区堡州立学院.md "wikilink")[传播学学士学位](../Page/传播学.md "wikilink")，随后又获得[英文学士学位](../Page/英文.md "wikilink")。1982年开始写作，后来出版的《[第四魔法之回音](../Page/第四魔法之回音.md "wikilink")》（*Echoes
of the Fourth Magic*）就是從這時开始撰写的。

萨尔瓦多的处女作是[TSR在](../Page/TSR_\(公司\).md "wikilink")1988年推出的《[碎魔晶](../Page/碎魔晶.md "wikilink")》（*The
Crystal
Shard*）。1990年萨尔瓦多正式成为专职[作家](../Page/作家.md "wikilink")。之后他撰写了许多奇幻小说。

1997年，他的信件、剧本和小说都被收藏在母校的萨尔瓦多图书馆。

## 兴趣爱好

萨尔瓦多非常喜欢玩[纸上角色扮演游戏](../Page/纸上角色扮演游戏.md "wikilink")，每周日都会和朋友玩这些游戏。甚至成立了自己的公司七剑客。1999年TSR为此推出了他们为[冰风谷所撰写的](../Page/冰风谷.md "wikilink")[冒险游戏模组](../Page/冒险游戏.md "wikilink")《[诅咒之塔](../Page/诅咒之塔.md "wikilink")》。

## 著作

  - 《[黑暗精靈三部曲](../Page/黑暗精靈三部曲.md "wikilink")》(*The Dark Elf Trilogy*)

<!-- end list -->

  -
    《[故土](../Page/故土.md "wikilink")》（*Homeland*)
    《[流亡](../Page/流亡.md "wikilink")》（*Exile*）
    《[旅居](../Page/旅居.md "wikilink")》（*Sojourn*）

<!-- end list -->

  - 《[血脈系列](../Page/血脈系列.md "wikilink")》（*Legacy of the Drow*）-
    這是他為TSR所撰寫的第一本精裝版小說

<!-- end list -->

  -
    《[血脈](../Page/血脈.md "wikilink")》（*The legacy*）
    《[無星之夜](../Page/無星之夜.md "wikilink")》（*Starless Night*）
    《[暗軍突襲](../Page/暗軍突襲.md "wikilink")》（*Siege of Darkness*）
    《[破曉之路](../Page/破曉之路.md "wikilink")》（*Passage to Dawn*）

<!-- end list -->

  - 《[冰風谷三部曲](../Page/冰風谷三部曲.md "wikilink")》(*The Icewind Dale Trilogy*)

<!-- end list -->

  -
    《[碎魔晶](../Page/碎魔晶.md "wikilink")》（*The Crystal Shard*）
    《[白銀溪流](../Page/白銀溪流.md "wikilink")》（*Streams of Silver*）
    《[半身人的魔墜](../Page/半身人的魔墜.md "wikilink")》（*The Halfling's Gem*）

<!-- end list -->

  - 《[牧師五部曲](../Page/牧師五部曲.md "wikilink")》（*The Cleric Quintet*）

<!-- end list -->

  -
    《[黑暗頌歌](../Page/黑暗頌歌.md "wikilink")》（*Canticle*)
    《[蔭林暗影](../Page/蔭林暗影.md "wikilink")》（*In Sylvan Shadows*）
    《[影夜假面](../Page/影夜假面.md "wikilink")》（*Night Masks*）
    《[淪城要塞](../Page/淪城要塞.md "wikilink")》（*The Fallen Fortress*）
    《[渾沌詛咒](../Page/渾沌詛咒.md "wikilink")》（*The Chaos Curse*）

<!-- end list -->

  - 《[黑暗之途系列](../Page/黑暗之途系列.md "wikilink")》（*Paths Of Darkness*）

<!-- end list -->

  -
    《[無聲之刃](../Page/無聲之刃.md "wikilink")》（*The Silent Blade*）
    《[世界之脊](../Page/世界之脊.md "wikilink")》（*The Spine of the World*）
    《[劍刃之海](../Page/劍刃之海.md "wikilink")》（*Sea of Swords*）

<!-- end list -->

  - 《[獵人之刃三部曲](../Page/獵人之刃三部曲.md "wikilink")》（*The Hunter's Blades
    Trilogy*）

<!-- end list -->

  -
    《[千獸人](../Page/千獸人.md "wikilink")》（*The Thousand Orcs*）
    《[孤身卓爾](../Page/孤身卓爾.md "wikilink")》（*The Lone Drow*）
    《[雙刃](../Page/雙刃.md "wikilink")》（*he Two Swords*）

<!-- end list -->

  - 《[變遷三部曲](../Page/變遷三部曲.md "wikilink")》（*Transitions*）

<!-- end list -->

  -
    《[獸人王](../Page/獸人王.md "wikilink")》（*The Orc King*）
    《[海盜王](../Page/海盜王.md "wikilink")》（*The Pirate King*）
    《[鬼魂王](../Page/鬼魂王.md "wikilink")》（*The Ghost King*）

<!-- end list -->

  - 《[絕冬城系列](../Page/無冬城_\(小说\).md "wikilink")》（*The Neverwinter Saga*）

[Category:美国奇幻小說家](../Category/美国奇幻小說家.md "wikilink")
[Category:義大利裔美國人](../Category/義大利裔美國人.md "wikilink")