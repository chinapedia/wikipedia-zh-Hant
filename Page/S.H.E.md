**H**ebe（田馥甄）{{·}}**E**lla（陳嘉樺） | 國籍 =  | 居住地 =
[臺灣](../Page/臺灣.md "wikilink") | 職業 =
[歌手](../Page/歌手.md "wikilink")、[演員](../Page/演員.md "wikilink")、[主持人](../Page/主持人.md "wikilink")
| 語言 =
[國語](../Page/中華民國國語.md "wikilink")、[閩南語](../Page/台灣閩南語.md "wikilink")
| 音樂類型 = [華語流行音樂](../Page/華語流行音樂.md "wikilink") | 演奏樂器 =
[歌唱](../Page/歌唱.md "wikilink") | 出身地 =  | 出道日期 =  | 出道作 =
專輯《[女生宿舍](../Page/女生宿舍_\(S.H.E\).md "wikilink")》 | 代表作 =
〈戀人未滿〉
〈Remember〉
〈熱帶雨林〉
〈美麗新世界〉
〈花都開好了〉
〈遠方〉
〈Super Star〉
〈波斯貓〉
〈痛快〉
〈不想長大〉
〈天灰〉
〈怎麼辦〉
〈中國話〉
〈老婆〉
〈宇宙小姐〉
〈安靜了〉
〈SHERO〉
〈愛上你〉
〈我愛雨夜花〉
〈花又開好了〉
〈心還是熱的〉
〈你曾是少年〉
〈永遠都在〉
〈十七〉 | 活躍年代 = 2001年－至今 | 唱片公司 =
[華研國際音樂](../Page/華研國際音樂.md "wikilink")（2000年－至今）
| 經紀公司 = [華研國際音樂](../Page/華研國際音樂.md "wikilink")（2000年－2018年9月30日）
任家萱：[任真美好](../Page/任真美好有限公司.md "wikilink")（2018年10月－至今）
田馥甄：[樂來樂好](../Page/樂來樂好有限公司.md "wikilink")（2018年10月－至今）
陳嘉樺：[勁樺娛樂](../Page/勁樺娛樂有限公司.md "wikilink")（2018年10月－至今） | 網站 =
[華研國際音樂官方網站](http://www.him.com.tw/artistscon2.php?tid=9)
| 現任成員 =
[任家萱](../Page/任家萱.md "wikilink")(**S**elina)、[田馥甄](../Page/田馥甄.md "wikilink")(**H**ebe)、[陳嘉樺](../Page/陳嘉樺.md "wikilink")(**E**lla)
| goldenmelodyawards =
**[第14屆金曲獎](../Page/第14屆金曲獎.md "wikilink")[最佳重唱組合獎](../Page/最佳演唱組合獎_\(金曲獎\).md "wikilink")**
**2003年《[美麗新世界](../Page/美麗新世界_\(專輯\).md "wikilink")》**
[第45屆金鐘獎](../Page/第45屆金鐘獎.md "wikilink")[電臺臺呼獎](../Page/金鐘獎電臺臺呼獎.md "wikilink")
2010年《S.H.E－917在一起》 | awards = [音樂風雲榜](../Page/音樂風雲榜.md "wikilink")
2010年 港臺十年最具影響力音樂人物
[全球流行音樂金榜](../Page/全球流行音樂金榜.md "wikilink")
2011年 年度最佳組合獎
2013年 年度最佳組合獎
[HITO流行音樂獎](../Page/HITO流行音樂獎.md "wikilink")
2013年 年度HITO最佳團體
(更多於得獎與提名) | imdb = 1997035 }}

**S.H.E**，為[臺灣知名](../Page/臺灣.md "wikilink")[女子演唱團體](../Page/女子演唱團體.md "wikilink")，其團名取自三位成員[英文名字的首個](../Page/英文名字.md "wikilink")[字母](../Page/英文字母.md "wikilink")，是由[**S**elina](../Page/任家萱.md "wikilink")（任家萱）、[**H**ebe](../Page/田馥甄.md "wikilink")（田馥甄）和[**E**lla](../Page/陳嘉樺.md "wikilink")（陳嘉樺）三位成員組成。三人在2000年參加《宇宙2000實力美少女爭霸戰》選秀比賽，之後被[宇宙唱片](../Page/華研國際音樂.md "wikilink")（現名[華研國際音樂](../Page/華研國際音樂.md "wikilink")）簽下培訓後，於2001年9月11日正式出道，發行首張專輯《[女生宿舍](../Page/女生宿舍_\(專輯\).md "wikilink")》。

S.H.E自出道至今，專輯總銷售量在全球累積已超過1600萬張\[1\]，亦是臺灣最長壽及唱片銷售量最高的女子團體\[2\]\[3\]，也是現今亞洲五大暢銷與全球十大暢銷女子演唱組合之一\[4\]\[5\]，並有「華人第一女子天團」之美譽\[6\]\[7\]\[8\]\[9\]。

## 團名由來

S.H.E所屬公司[宇宙唱片](../Page/華研國際音樂.md "wikilink")（[華研國際音樂前身](../Page/華研國際音樂.md "wikilink")），在當時分別為三人的風格，取各自具有代表性意義的英文名字：[Selina等同於](../Page/任家萱.md "wikilink")「[月亮女神](../Page/月亮女神.md "wikilink")」，代表著「溫柔」，代表顏色為**粉紅色**；[Hebe等同於](../Page/田馥甄.md "wikilink")「青春女神」，代表著「自信」，代表顏色為**綠色**；代表著「勇氣」的[Ella](../Page/陳嘉樺.md "wikilink")，隱含[古希臘神話中](../Page/古希臘神話.md "wikilink")「[火炬](../Page/火炬.md "wikilink")」的意義，代表顏色為**藍色**。S.H.E三人並標榜其組合擁有高、中、低三種聲線，三頻美聲、三種個性，可以是「男生的女朋友，女生的好朋友」。以上即為唱片公司最初賦予S.H.E的定位與方向\[10\]\[11\]。

## 成員列表

  - 註：S.H.E沒有隊長亦無主唱副唱之分\[12\]\[13\]\[14\]。早期S.H.E演唱歌曲時，多會分配各自擅長的[音域](../Page/音域.md "wikilink")，例如[高音主唱是](../Page/高音.md "wikilink")[田馥甄](../Page/田馥甄.md "wikilink")（**H**ebe），[中](../Page/中音.md "wikilink")、[高音主唱是](../Page/高音.md "wikilink")[任家萱](../Page/任家萱.md "wikilink")（**S**elina），[中](../Page/中音.md "wikilink")、[低音主唱是](../Page/低音.md "wikilink")[陳嘉樺](../Page/陳嘉樺.md "wikilink")（**E**lla），而在沒演唱[主音的時候也會參與歌曲](../Page/主音.md "wikilink")[和聲部分的演唱](../Page/和聲.md "wikilink")，但隨著時間的推移此區分已漸不明顯，現在更是以[主音與](../Page/主音.md "wikilink")[和聲兩個部分來回穿插為主](../Page/和聲.md "wikilink")\[15\]。

<table>
<tbody>
<tr class="odd">
<td><p><strong>本名</strong></p></td>
<td><p>=1 style="width:14%"|<strong>藝名</strong></p></td>
<td><p><strong>出生日期 / 出生地</strong></p></td>
<td><p><strong>官方應援色</strong> |- style="background: linear-gradient(to bottom, #FFC0CB, #FF8383)</p></td>
<td style="text-align: center;"><p><strong><a href="../Page/任家萱.md" title="wikilink">任家萱</a></strong></p></td>
<td style="text-align: center;"><p><strong>Selina</strong></p></td>
<td><p><br />
<a href="../Page/台北市.md" title="wikilink">台北市</a><a href="../Page/士林區.md" title="wikilink">士林區</a></p></td>
<td style="text-align: center;"><p><strong>粉红：温柔</strong> |- style="background: linear-gradient(to bottom, #E9FFDB, #BEDF41)</p></td>
<td style="text-align: center;"><p><strong><a href="../Page/田馥甄.md" title="wikilink">田馥甄</a></strong></p></td>
<td style="text-align: center;"><p><strong>Hebe</strong></p></td>
<td><p><br />
<a href="../Page/新竹縣.md" title="wikilink">新竹縣</a><a href="../Page/新豐鄉_(台灣).md" title="wikilink">新豐鄉</a></p></td>
<td style="text-align: center;"><p><strong>綠色：自信</strong> |- style="background: linear-gradient(to bottom, #E7FEFF, #98F5FF)</p></td>
<td style="text-align: center;"><p><strong><a href="../Page/陳嘉樺.md" title="wikilink">陳嘉樺</a></strong></p></td>
<td style="text-align: center;"><p><strong>Ella</strong></p></td>
<td><p><br />
<a href="../Page/屏東縣.md" title="wikilink">屏東縣</a><a href="../Page/麟洛鄉.md" title="wikilink">麟洛鄉</a></p></td>
<td style="text-align: center;"><p><strong>藍色：勇氣</strong></p></td>
</tr>
</tbody>
</table>

## 出道歷程

2000年，三人各自參加由宇宙唱片（為[華研唱片前身](../Page/華研國際音樂.md "wikilink")）與[中視節目](../Page/中視.md "wikilink")《電視大國民》殘酷舞台單元合辦的《宇宙2000實力美少女爭霸戰》選秀比賽。決賽共六位參加，最後[Selina獲得冠軍](../Page/任家萱.md "wikilink")，[Hebe進入決賽](../Page/田馥甄.md "wikilink")，[Ella則在決賽時被淘汰](../Page/陳嘉樺.md "wikilink")，原先計劃只選冠軍，賽後唱片公司再找回七名參賽者試音，在經過唱片公司內部協商討論之後，決定簽下現在S.H.E的三位成員並組成團體。S.H.E正式組團後，而當時仍就讀[大學的Selina](../Page/大學.md "wikilink")、[高中的Hebe和](../Page/高中.md "wikilink")[五專的Ella](../Page/五專.md "wikilink")，利用假日空檔北上[台北進行錄音和受訓](../Page/台北.md "wikilink")，之後Hebe和Ella畢業後不久之後S.H.E就即將出道，而S.H.E三人住在宿舍一起共同生活、工作、上通告和唱片宣傳。

## 演藝歷程

### 2001年至2006年

  - 《女生宿舍》、《青春株式會社》、《美麗新世界》時期

第1張專輯《[女生宿舍](../Page/女生宿舍_\(S.H.E\).md "wikilink")》（首張錄音室專輯）於2001年9月11日發行，而主打歌〈戀人未滿〉是首[R\&B風格的歌曲](../Page/R&B.md "wikilink")（翻唱自[天命真女的](../Page/天命真女.md "wikilink")〈Brown
Eyes〉），之後此首歌曲也令三人一炮而紅，所以〈戀人未滿〉除了是S.H.E的出道作品，也是令三人一炮而紅並讓人所熟識且廣為人知的歌曲，S.H.E也以新人之姿首張專輯在臺灣的銷量約17萬張，全亞洲的銷量約75萬張。隔年2002年3月更以此張專輯入圍台灣《[第十三屆金曲獎](../Page/第13屆金曲獎.md "wikilink")》「[最佳新人獎](../Page/最佳新人獎_\(金曲獎\).md "wikilink")」。

第2張專輯《[青春株式會社](../Page/青春株式會社.md "wikilink")》（第2張錄音室專輯）在與上一張專輯《[女生宿舍](../Page/女生宿舍_\(S.H.E\).md "wikilink")》相隔約四個多月後的2002年1月29日發行。臺灣銷量約25萬張，全亞洲銷量約125萬張，此張專輯裡有一半歌曲都是改編作品；第3張專輯《[美麗新世界](../Page/美麗新世界_\(專輯\).md "wikilink")》（第3張錄音室專輯）於2002年8月5日發行，同月24日在[臺南市政府前廣場舉辦出道以來首次演唱會](../Page/臺南市政府.md "wikilink")，也是出道以來首次舉辦免費非售票演唱會。在次年2003年8月獲[第十四屆金曲獎](../Page/第14屆金曲獎.md "wikilink")「[最佳重唱組合獎](../Page/最佳演唱組合獎_\(金曲獎\).md "wikilink")」\[16\]。臺灣專輯銷量約30萬張，全亞洲銷量約176萬張。

  - 《Together新歌+精選》、《Super Star》、《奇幻旅程》、《奇幻樂園大型巡演》、《Encore！》時期

2003年，唱片公司聲言S.H.E將「轉型」告別「半熟卵」時期，進入一個全新的階段，第4張專輯（首張新歌加精選）《[Together
新歌+精選](../Page/Together_\(S.H.E專輯\).md "wikilink")》便為告別舊形象之作，專輯於2003年1月23日發行，台灣銷量約28萬張。

第5張專輯《[Super
Star](../Page/Super_Star_\(S.H.E\).md "wikilink")》（第4張錄音室專輯）於2003年8月22日發行，Sweetbox主動促成這場臺灣和[德國的首度跨國合作](../Page/德國.md "wikilink")，並量聲打造製作專輯首波同名主打歌〈Super
Star〉\[17\]。之後亦有[韓國女歌手](../Page/韓國.md "wikilink")[ASH和](../Page/ASH.md "wikilink")[加拿大女歌手](../Page/加拿大.md "wikilink")[Skye
Sweetnam分別前後將](../Page/斯凱·威楠.md "wikilink")〈Super
Star〉翻唱成[韓文版和](../Page/韓文.md "wikilink")[英文版](../Page/英文.md "wikilink")，歌名也皆稱為〈Super
Star〉\[18\]。台灣專輯銷量約32萬張，亞洲銷量約275萬張。

在《[Super
Star](../Page/Super_Star_\(S.H.E\).md "wikilink")》專輯發行前一個月，成員之一的[Ella於](../Page/陳嘉樺.md "wikilink")2003年7月29日，因為主持《[快樂星期天](../Page/快樂星期天.md "wikilink")》節目出外景而出意外，使背脊第一腰椎受傷在醫院休息20多天，需穿著背架三至六個月，暫時休養半年，因此不能參與拍攝此專輯裡的MV，只有專輯同名主打歌〈Super
Star〉是她受傷前真正上場拍攝的MV，在[Ella休養期間](../Page/Ella.md "wikilink")，MV拍攝以[Selina](../Page/任家萱.md "wikilink")、[Hebe為主](../Page/田馥甄.md "wikilink")，其它MV的Ella部分都是後來獨立拍攝再剪接後製上去的，或是使用替身代替Ella拍攝其背影來完成MV的拍攝，而專輯宣傳活動與廣告代言，皆由
[Selina](../Page/任家萱.md "wikilink")、[Hebe兩人出席](../Page/田馥甄.md "wikilink")\[19\]，因此專輯如期發行，並沒有影響到發片計畫。

第6張專輯《[奇幻旅程](../Page/奇幻旅程.md "wikilink")》（第5張錄音室專輯）於2004年2月6日發行，是受傷的[Ella傷癒歸隊後發行的專輯](../Page/陳嘉樺.md "wikilink")。此張專輯走奇幻風格，並標榜前九首歌曲代表九個國家，有如經歷過這九個國家的旅程，便如同其專輯名稱「奇幻旅程」。巡迴演唱會自2004年9月4日，於臺灣[臺北市立體育場拉開序幕](../Page/臺北市立體育場.md "wikilink")，是S.H.E首次舉辦大型巡迴演唱會，亦是出道以來第一次舉辦大型售票巡迴演唱會\[20\]。

第7張專輯《[Encore安可](../Page/Encore_\(S.H.E專輯\).md "wikilink")》（第6張錄音室專輯）於2004年11月12日發行，而這次是繼第五張專輯《[Super
Star](../Page/Super_Star_\(S.H.E\).md "wikilink")》專輯中為S.H.E量聲打造專輯同名主打歌〈Super
Star〉之後再度合作\[21\]，之後於同年Sweetbox亦有再翻唱這首歌，英文版歌名為〈More Then
Love〉，後收錄於《》專輯。台灣專輯銷量約19萬張，全亞洲銷量約200萬張\[22\]。

  - 《不想長大》、《Forever新歌+精選》、《移動城堡大型巡演》時期

2005年6月21日，S.H.E拍攝電視劇《[真命天女](../Page/真命天女.md "wikilink")》，[Ella不慎遭火吻](../Page/陳嘉樺.md "wikilink")，耳朵灼傷、兩側頭髮燒焦，經送臺大醫院急救住院，幸無大礙，只需要休息兩天\[23\]。

2005年9月28日，推出由S.H.E共同擔任女主角的電視劇《[真命天女](../Page/真命天女.md "wikilink")》的《[真命天女電視原聲帶](../Page/真命天女電視原聲帶.md "wikilink")》，原聲帶除了收錄S.H.E所演唱的主題曲〈星光〉外，也收錄了團員個人演唱的獨唱曲兼戲劇插曲；同年10月16日，為S.H.E量身打造的電視劇《[真命天女](../Page/真命天女.md "wikilink")》於[華視首播](../Page/華視.md "wikilink")，隔年Ella也以此劇入圍[第41屆金鐘獎](../Page/第41屆金鐘獎.md "wikilink")[戲劇類最佳女主角獎](../Page/金鐘獎戲劇節目女主角獎得獎列表.md "wikilink")。11月25日，發行第8張專輯《[不想長大](../Page/不想長大_\(專輯\).md "wikilink")》，S.H.E首次為專輯中歌曲〈神槍手〉擔任配唱製作人，Selina則首次為專輯中歌曲〈星星之火〉填寫英文詞，而此張專輯大量的採用新人的詞曲創作，整張專輯收錄的全是原創歌曲，台灣銷量約15萬張，亞洲銷量最終也突破200萬張，並且也讓S.H.E連續五年入圍[台灣金曲獎](../Page/台灣金曲獎.md "wikilink")\[24\]，其中並獲得2003年度[第14屆金曲獎的最佳重唱組合獎](../Page/第14屆金曲獎.md "wikilink")。而《不想長大》專輯亦被「[中華音樂人交流協會](../Page/中華音樂人交流協會.md "wikilink")」選為2005年推薦專輯。

第9張專輯《[Forever
新歌+精選](../Page/Forever_\(S.H.E專輯\).md "wikilink")》於2006年7月21日發行，台灣銷量約7萬5千張。在此張精選專輯發行前便已又展開新一輪的大型巡演《[移動城堡世界巡迴演唱會](../Page/移動城堡世界巡迴演唱會.md "wikilink")》，7月初的7月8日自[上海八萬人體育場揭開序幕](../Page/上海八萬人體育場.md "wikilink")\[25\]，並再次刷新女歌手上海演唱會票房紀錄\[26\]。2006年9月10日，在9月11日S.H.E成軍出道滿五週年的前一天，提前舉辦了《當我們同在一起》五週年同樂會。同年12月16日回到臺灣舉辦《S.H.E移動城堡世界巡迴演唱會》，初次登上[臺北小巨蛋舞臺開唱](../Page/臺北小巨蛋.md "wikilink")，成為第一組登上台北小巨蛋開唱的流行女子音樂演唱團體，亦是S.H.E首次在小巨蛋舉辦大型售票演唱會\[27\]，此次巡演自上海場到台灣場在6個月内一共累計了大約20萬人次的觀眾\[28\]。

[SHE_perfect3hk.jpg](https://zh.wikipedia.org/wiki/File:SHE_perfect3hk.jpg "fig:SHE_perfect3hk.jpg")》香港站\]\]

### 2007年至2010年

  - 《Play》、《我的電臺FM S.H.E》時期

2007年5月11日，發行第10張專輯《[Play](../Page/Play_\(S.H.E專輯\).md "wikilink")》（第8張錄音室專輯），此張專輯如同專輯名稱一樣，是以「玩」、「玩樂」的概念為出發點製作而成的專輯
，在首波主打歌〈[中國話](../Page/中國話_\(歌曲\).md "wikilink")〉首次挑戰[繞口令](../Page/繞口令.md "wikilink")，並融合[電子](../Page/電子音樂.md "wikilink")、[舞曲](../Page/舞曲.md "wikilink")、[嘻哈和](../Page/嘻哈.md "wikilink")[Rap元素](../Page/Rap.md "wikilink")。但歌曲卻在台灣引起喧然大波，引發爭議，網友批評這首歌有諂媚[中国大陆嫌疑](../Page/中国大陆.md "wikilink")，而《[自由時報](../Page/自由時報.md "wikilink")》與旗下刊物刊登也大篇幅報導[炒作此事](../Page/炒作.md "wikilink")\[29\]。而對於新歌變成政治事件，S.H.E三人均表示無奈\[30\]（中國話相關爭議請參閱「[中國話](../Page/中國話_\(歌曲\).md "wikilink")」條目）</small>。另外，S.H.E，在這張專輯終於發表自己創作的作品，Selina和Ella更進一步首次嘗試詞曲創作，在專輯中寫出描述S.H.E彼此感情的〈老婆〉，為Selina填詞、Ella作曲，其後Ella也陸續發表一些詞曲創作（以譜曲為主）；Hebe在專輯中亦替〈說你愛我〉一曲首次填詞。專輯台灣銷量超過13萬張，亞洲銷量也突破160萬張。

2008年受[中國中央電視台之邀於](../Page/中國中央電視台.md "wikilink")2月6日[農曆](../Page/農曆.md "wikilink")[除夕夜首次於](../Page/除夕.md "wikilink")《[中國中央電視台春節聯歡晚會](../Page/2008年中國中央電視台春節聯歡晚會.md "wikilink")》演出，演唱《中國話》。S.H.E也在[2008年中國北京奧林匹克運動會開幕典禮](../Page/2008年夏季奧林匹克運動會.md "wikilink")、開幕前的火炬傳遞城市慶典和閉幕典禮擔任表演嘉賓，演唱奧運歌曲《紅遍全球》\[31\]\[32\]\[33\]\[34\]。

第11張專輯《[我的電臺FM
S.H.E](../Page/我的電台FM_S.H.E.md "wikilink")》（第9張錄音室專輯）於2008年9月23日發行，此張專輯的概念是從「電臺」出發，延續上一張專輯《[Play](../Page/Play_\(S.H.E專輯\).md "wikilink")》玩音樂的精神，並且S.H.E在专辑中首次担任电台DJ，專輯中加入三個電臺主播的聲音片段穿插其中作為連接整張專輯中的歌曲。同年2008年10月19日於Selina母校[台北市](../Page/台北市.md "wikilink")[國立臺灣師範大學校本部體育館舉辦Top](../Page/國立臺灣師範大學.md "wikilink")
Girl《[我的電台 FM
S.H.E](../Page/我的電台_FM_S.H.E.md "wikilink")》新歌演唱會，而台灣專輯銷量超過9萬張，亞洲銷量也突破120萬張。

  - 《愛的地圖》、《愛而為一大型巡演》、《SHERO》時期

2009年，與2007年12月1日舉辦的《[移動城堡世界巡迴演唱會](../Page/移動城堡世界巡迴演唱會.md "wikilink")》[馬來西亞](../Page/馬來西亞.md "wikilink")[吉隆坡最終場](../Page/吉隆坡.md "wikilink")，相隔一年半後的2009年6月20日，《[移動城堡世界巡迴演唱會](../Page/移動城堡世界巡迴演唱會.md "wikilink")》再加開一場「特別場」場次的演出，並在[澳門舉辦](../Page/澳門.md "wikilink")。

2009年6月22日發行首張數位專輯《[愛的地圖](../Page/愛的地圖.md "wikilink")》，2009年未發行實體專輯，但推出寫真圖文書《愛的3溫暖》並收錄翻唱曲〈夢田〉及新歌曲〈鎖住時間〉。同年10月16日、10月17日，展開第三次的世界巡迴演唱會《[愛而為一世界巡迴演唱會](../Page/愛而為一世界巡迴演唱會.md "wikilink")》而首站在[香港紅磡體育館舉行](../Page/香港紅磡體育館.md "wikilink")，並且是首次在紅館連續兩天舉辦兩場的演唱會，兩場演唱會因內部門票供不應求\[35\]，兩場演出票房收入約3000萬元\[36\]，此次巡迴演唱會香港站與上一次巡演《[移動城堡世界巡迴演唱會](../Page/移動城堡世界巡迴演唱會.md "wikilink")》香港站，共三次站上[紅磡體育館開唱](../Page/紅磡體育館.md "wikilink")，這也是此次巡演香港站與上一次巡演香港站睽違三年多之後，再次於紅館開唱\[37\]。2010年3月26日，發行第12張專輯《[SHERO](../Page/SHERO.md "wikilink")》（第10張錄音室專輯），此外也擔任[2010年臺北國際花卉博覽會代言親善大使](../Page/2010年臺北國際花卉博覽會.md "wikilink")（2010年11月6日至2011年4月25日）\[38\]。除了〈SHERO〉是繼第五張專輯《[Super
Star](../Page/Super_Star_\(S.H.E\).md "wikilink")》中的主打歌曲〈Super
Star〉之後久違的首波搖滾主打歌之外，還特別收錄了S.H.E出道以來第一首臺語歌曲《[我愛雨夜花](../Page/我愛雨夜花.md "wikilink")》，此首歌曲是取樣自經典傳統[臺語歌謠](../Page/臺語.md "wikilink")《[雨夜花](../Page/雨夜花.md "wikilink")》。台灣專輯銷售量共6萬8千張\[39\]。

  - 單飛不解散階段：

2010年3月30日，Hebe在臺北[The Wall舉行名為](../Page/The_Wall.md "wikilink")「330音樂
田」的慈善性質生日音樂會，也被視為是Hebe發行首張個人專輯的熱身，唱片公司也透露Hebe將發行醞釀已久的個人專輯，而Ella和Selina分別朝電影、主持和電視劇方面發展。2010年下半年，S.H.E三人除了演唱會、廣告、商演外將以「單飛不解散」的模式分進合擊，「單飛不解散」\[40\]\[41\]\[42\]。9月3日，Hebe以「單飛不解散」的模式發行專輯，使用本名「田馥甄」以新人之姿單飛，發行首張個人專輯《[To
Hebe](../Page/To_Hebe.md "wikilink")》，唱片銷售成績亮眼\[43\]，也於10月8、9、10日順利在臺北Legacy連續開了三場個人名義的「Love！田馥甄To
Hebe音樂會」，及11月初以個人名義在香港舉行的音樂會，均獲得熱烈迴響。專輯總銷售量為5萬張。10月22日，Selina於上海拍攝《[我和春天有個約會](../Page/愛在春天.md "wikilink")》不幸遭到爆破效果灼傷，全身54%灼傷其中有41%三度灼傷、13%二度灼傷，對未來投下不確定的震撼彈\[44\]\[45\]。2010年10月24日，Selina搭乘國際SOS醫療專機於10點30分左右降落[臺北市](../Page/臺北市.md "wikilink")[松山機場](../Page/松山機場.md "wikilink")，直接送往[林口](../Page/林口區.md "wikilink")[長庚醫院接受治療](../Page/長庚醫院.md "wikilink")。
<small>（後續賠償相關文章請參閱《[我和春天有個約會](../Page/我和春天有個約會_\(2010年電視劇\).md "wikilink")》條目）、（Selina後續情況請參閱「[Selina](../Page/任家萱.md "wikilink")」條目）</small>。

### 2011年至2014年

  - 《花又開好了》時期

2011年1月19日，因上海拍戲灼傷的Selina在[林口](../Page/林口區.md "wikilink")[長庚醫院住院兩個多月後](../Page/長庚醫院.md "wikilink")，出院回家休養，而唱片公司則於當日舉行出院記者會，而Selina則由Hebe和Ella陪同出席記者會\[46\]。因Selina燒傷需要長期休養，導致2011年原訂於2、3月分別在[陝西和](../Page/陝西.md "wikilink")[杭州的兩場S](../Page/杭州.md "wikilink").H.E《[愛而為一世界巡迴演唱會](../Page/愛而為一世界巡迴演唱會.md "wikilink")》，因Selina灼傷意外而取消；S.H.E三人共同代言的廣告在續約和出席代言活動時，則由Hebe和Ella暫時代表出席，而S.H.E原先預定七、八月發行的新專輯，也因為Selina來不及復原錄音而延期\[47\]。Selina原訂於4月結婚，為避免Selina壓力過大，造成身體的負擔，婚禮亦延期舉行\[48\]。

2011年3月4日發行《[愛而為一演唱會影音館](../Page/愛而為一演唱會影音館.md "wikilink")》，內容收錄2010年5月29日於[台北小巨蛋舉辦的](../Page/台北小巨蛋.md "wikilink")《[S.H.E
IS THE ONE 愛而為一世界巡迴演唱會](../Page/愛而為一世界巡迴演唱會.md "wikilink")》TOP
GIRL台北「旗艦場」的演唱會實錄，之後並獲得[IFPI香港唱片銷量大獎頒奬禮2011](../Page/IFPI香港唱片銷量大獎頒奬禮2011.md "wikilink")「最高銷量國語唱片獎」。4月，Ella投入首部主演的電影《[女孩壞壞](../Page/女孩壞壞.md "wikilink")》的拍攝工作，接著5月參與主演的電影《[新天生一對](../Page/新天生一對.md "wikilink")》的拍攝工作。5月，《[SHERO](../Page/SHERO.md "wikilink")》專輯入圍[第22屆金曲獎](../Page/第22屆金曲獎.md "wikilink")[最佳演唱組合獎項](../Page/最佳演唱組合獎_\(金曲獎\).md "wikilink")；而田馥甄的單飛作品《[To
Hebe](../Page/To_Hebe.md "wikilink")》，入圍[第22屆金曲獎四項獎項](../Page/第22屆金曲獎.md "wikilink")。同年9月2日，田馥甄推出第二張個人專輯《[My
Love](../Page/My_Love_\(田馥甄專輯\).md "wikilink")》獲得熱烈迴響，並在12月3日、4日於[臺大體育館舉行連續兩場](../Page/臺灣大學綜合體育館.md "wikilink")「
To My Love
慶功音樂會」。專輯台灣銷售量為6萬2千張。12月16日，Selina發行個人迷你專輯《[重作一個夢](../Page/重作一個夢.md "wikilink")》，其中歌曲〈夢〉由Selina填詞\[49\]。專輯台灣銷售量約3萬張。

2012年1月及4月，Ella主演電影《[新天生一對](../Page/新天生一對.md "wikilink")》和《[女孩壞壞](../Page/女孩壞壞.md "wikilink")》陸續上映，而同年3月30日，已先發行個人首張迷你專輯《[我就是...](../Page/我就是....md "wikilink")》，並收錄搭配Ella所主演的電影《女孩壞壞》的電影主題曲、片尾曲及插曲，台灣上半年度銷售量為銷售1萬8千張，最終台灣總銷售量約為2萬張。同年Ella與赖斯翔於4月15日訂婚\[50\]、5月5日完婚\[51\]。6月23日，S.H.E擔任[第23屆金曲獎表演節目](../Page/第23屆金曲獎.md "wikilink")「Happy
Together」的演出嘉賓，這是自2010年10月Selina在[中國](../Page/中國.md "wikilink")[上海拍戲](../Page/上海.md "wikilink")[燒傷以來的首次合體演出](../Page/燒傷.md "wikilink")，並宣告回歸，同時也宣告Selina正式復出\[52\]。而田馥甄的個人作品《[My
Love](../Page/My_Love_\(田馥甄專輯\).md "wikilink")》，入圍金曲獎七項，亦為該屆金曲獎入圍最多項目的專輯之一。9月11日，在[汽車旅館舉辦慶祝成軍出道](../Page/汽車旅館.md "wikilink")11週年的慶生派對，而此場慶生派對也在[Youtube上有](../Page/Youtube.md "wikilink")[現場直播](../Page/現場直播.md "wikilink")，並且S.H.E也在現場接受歌迷點歌並現場演唱歌迷所點的曲目\[53\]。10月11日，宣布與[華研國際音樂續約](../Page/華研國際音樂.md "wikilink")\[54\]\[55\]。

S.H.E繼2012年6月23日，擔任[第23屆金曲獎演出嘉賓宣告回歸之後](../Page/第23屆金曲獎.md "wikilink")，第13張專輯《[花又開好了](../Page/花又開好了.md "wikilink")》（第11張錄音室專輯）於2012年11月16日發行，這是距離上次《[SHERO](../Page/SHERO.md "wikilink")》專輯發片後睽違2年8個月再次發行新專輯\[56\]，發片前夕於2012年11月4日，提前在[台大體育館舉辦](../Page/台大體育館.md "wikilink")「不說再見同學會」\[57\]。唱片公司也在此張唱片的宣傳期間再度安排S.H.E三人住進「[女生宿舍](../Page/女生宿舍.md "wikilink")」，重溫S.H.E組團後剛出道時合宿的生活模式\[58\]。截至2012年12月，專輯台灣銷售量約4萬6千多張，2012年度專輯最終台灣銷售量共6萬2千多張。

  - 《2gether 4ever大型巡演》、《2gether 4ever大型巡演 Encore》時期

2013年受[中國中央電視台之邀二度於](../Page/中國中央電視台.md "wikilink")《[中國中央電視台春節聯歡晚會](../Page/2013年中國中央電視台春節聯歡晚會.md "wikilink")》合體演出，演唱《SHERO》，同時也是S.H.E宣告回歸後正式在中國大陸的演出。2月24日，於前一年2012年11月16日發行的第13張專輯《[花又開好了](../Page/花又開好了.md "wikilink")》中的單曲〈迫不及待〉被選為該年度2013年度[新竹](../Page/新竹.md "wikilink")[台灣颩燈會形象主題曲](../Page/台灣燈會.md "wikilink")，也擔任該年度[台灣燈會代言人](../Page/台灣燈會.md "wikilink")。3月28日，在2013年6月舉行《[Together
Forever世界巡迴演唱會](../Page/Together_Forever世界巡迴演唱會.md "wikilink")》，成為第一組三度在台北小巨蛋開唱的台灣團體，也是第一組連續兩天連唱兩場的台灣團體，此次巡演亦是Selina於[上海拍戲燒傷意外後休養並復出後的首個巡迴演唱會](../Page/上海.md "wikilink")\[59\]，台北場兩場演唱會門票約三小時完售，總票房約6200萬元\[60\]，此為其第4次大型巡迴演唱會。此次巡演香港站時，宣布2014年將加開第二階段的《Together
Forever世界巡迴演唱會》安可場\[61\]。4月，Selina接下《[小宇宙33號](../Page/小宇宙33號.md "wikilink")》主持棒，2013年4月，Ella接下《[王子的約會](../Page/王子的約會.md "wikilink")》擔任主持人，2013年7月，Ella主演戲劇《[謊言遊戲](../Page/謊言遊戲.md "wikilink")》，Selina於同年9月，憑著《[小宇宙33號](../Page/小宇宙33號.md "wikilink")》與[納豆入圍](../Page/林郁智.md "wikilink")《[第48屆金鐘獎](../Page/第48屆金鐘獎.md "wikilink")》[最佳綜藝節目主持人獎](../Page/金鐘獎綜藝節目主持人獎得獎列表.md "wikilink")，Hebe於同年11月28日發行個人第三張國語專輯《[渺小](../Page/渺小.md "wikilink")》獲4萬張銷量，並於隔年5月以此張專輯入圍[第25屆金曲獎兩個獎項](../Page/第25屆金曲獎.md "wikilink")。

2014年7月和8月《[Together
Forever世界巡迴演唱會](../Page/Together_Forever世界巡迴演唱會.md "wikilink")》再次唱回家鄉台灣，台北安可場兩場總票房收入約新台幣6300萬元\[62\]。其中，此次巡演台灣場次共舉辦台北4場、高雄1場，創下台灣團體單次巡演舉辦最多場之記錄，也是第一組最早四度在台北小巨蛋開唱的女子演唱團體。8月8日發行《[2GETHER
4EVER演唱會影音館](../Page/2GETHER_4EVER演唱會影音館.md "wikilink")》，內容收錄《[2GETHER
4EVER 世界巡迴演唱會](../Page/Together_Forever世界巡迴演唱會.md "wikilink")》的實錄和相關曲目。

2014年團體巡迴演唱會結束後，三人再度回到各自「單飛不解散」的模式。Ella主演的電視劇《[謊言遊戲](../Page/謊言遊戲.md "wikilink")》在10月上檔，並於同月以《[王子的約會](../Page/王子的約會.md "wikilink")》與[庾澄慶一同入圍](../Page/庾澄慶.md "wikilink")[第49屆金鐘獎](../Page/第49屆金鐘獎.md "wikilink")[綜藝節目主持人獎](../Page/金鐘獎綜藝節目主持人獎得獎列表.md "wikilink")。Selina與Ella分別於10月和11月分別擔任[第49屆金鐘獎及](../Page/第49屆金鐘獎.md "wikilink")[第51屆金馬獎主持人](../Page/第51屆金馬獎.md "wikilink")，且兩人於2014年底一同接下於2014年12月31日舉辦的2014-2015《[2015年臺北最HIGH新年城](../Page/臺北最HIGH新年城.md "wikilink")》跨年晚會的主持棒；Hebe則於12月6日、7日首次以個人身分展開首場個人大型巡演《[如果世界巡迴演唱會](../Page/如果世界巡迴演唱會.md "wikilink")》於[台北小巨蛋揭開序幕](../Page/台北小巨蛋.md "wikilink")。

### 2015年至2018年

  - 《永遠都在》時期

2015年1月9日，Selina推出個人首張專輯《[3.1415](../Page/3.1415_\(任家萱專輯\).md "wikilink")》，並於2月15日在台北舉辦《致，有圓人》個人唱談會。Ella主演的電影《[吉星高照2015](../Page/吉星高照2015.md "wikilink")》在3月上映，接著4月17日推出首張個人專輯《[Why
Not](../Page/Why_Not_\(陳嘉樺專輯\).md "wikilink")》，並於6月7日在台北舉辦《你正常嗎》個人唱演會，主演的電影《[缺角一族](../Page/缺角一族.md "wikilink")》亦在5月上映。7月，Hebe為電影《[我的少女時代](../Page/我的少女時代.md "wikilink")》獻唱電影主題曲《[小幸運](../Page/小幸運.md "wikilink")》（《[我的少女時代電影原聲帶](../Page/我的少女時代電影原聲帶.md "wikilink")》於2015年10月21日發行）。同年成軍滿14週年的前夕，於[台大體育館舉辦](../Page/台大體育館.md "wikilink")《S.H.E人健人愛14週年趣味競賽》。

2016年5月23日，推出合體新單曲《殊途》，並在2016年受[杜莎夫人蠟像館之邀](../Page/杜莎夫人蠟像館.md "wikilink")，於5月29日，正式宣布進駐[上海杜莎夫人蠟像館留下三人蠟像](../Page/上海杜莎夫人蠟像館.md "wikilink")\[63\]\[64\]\[65\]\[66\]。8月10日，推出紀念S.H.E成軍15週年的合體新單曲〈永遠都在〉，此單曲〈永遠都在〉融入了S.H.E歷年來的歌名與歌詞，此單曲收錄於S.H.E首張同名迷你專輯《[永遠都在](../Page/永遠都在.md "wikilink")》，發行日當天也因適逢S.H.E成軍出道15週年前夕，擴大舉辦S.H.E首場《團圓one
in
one》15週年紀念特展，自8月26日至9月19日於[台北市](../Page/台北市.md "wikilink")[松山文創園區展開為期](../Page/松山文創園區.md "wikilink")25天的展覽\[67\]\[68\]\[69\]。

  - 16週年線上直播「宵夜晚眠生日趴」及《小時帶 S.H.E's in
    style》、17週年音樂會「17th音樂會」及17週年新單曲《十七》時期

2017年9月4日至9月10日，每晚「9點11分」在S.H.E[Facebook粉絲專頁](../Page/Facebook.md "wikilink")，上傳7段一小段以S.H.E的歌曲作為[背景音樂的神秘小短片](../Page/背景音樂.md "wikilink")，紀念出道16週年活動的舖陳前導短片\[70\]\[71\]\[72\]\[73\]。9月11日晚上「9點11分」，Facebook粉絲專頁上[直播名為](../Page/直播.md "wikilink")「宵夜晚眠生日趴」的紀念出道16週年線上直播\[74\]。除此之外，S.H.E也在直播中宣布推出收錄第1張專輯至第13張專輯的S.H.E16週年復刻紀念全套卡帶組《小時帶
S.H.E's in style》卡帶紀念作品\[75\]\[76\]\[77\]，而最終台灣銷售量為年度第9名。

2018年9月11日，在[台北](../Page/台北市.md "wikilink")[兩廳院藝文廣場舉辦紀念出道](../Page/國家兩廳院.md "wikilink")17年週年演唱活動\[78\]。音樂會開演前的一個月開放免費索取後，總共1萬2千張門票在1分鐘之內被索取一空\[79\]\[80\]\[81\]。演出前舉辦名為「我的S.H.E愛歌」的人氣票選活動，而票選活動內容則是選出S.H.E在音樂會當天會演唱的部分歌曲曲目\[82\]。9月11日音樂會當天，場內共有12000名觀眾入場，而線上直播為中國大陸的[騰訊視頻和](../Page/騰訊視頻.md "wikilink")[咪咕音樂](../Page/咪咕音樂.md "wikilink")，分別有100萬人和105萬人線上觀看，另外[LINE
Today以及S](../Page/LINE_Today.md "wikilink").H.E
[YouTube官方頻道則分別有](../Page/YouTube.md "wikilink")50萬人和9萬人的觀看人次，估計共高達264萬人同時在線上觀看\[83\]\[84\]，最終達到線上超過500萬人的總觀看人數\[85\]\[86\]\[87\]。

  - 三人分別成立個人公司，確立單飛不解散：

2018年9月30日，華研音樂宣布與S.H.E三人合約正式到期。2018年10月1日，與華研唱片結束長達17年的合作關係後，三人各自成立新公司。Selina成立了「任真美好有限公司」、Hebe成立「樂來樂好有限公司」、Ella成立「勁樺娛樂有限公司」，至於目前「S.H.E」的團名屬於華研音樂，未來三人合體計畫和活動仍須由華研唱片統籌，而未來與華研的合作方式仍是未知數\[88\]\[89\]。

## 音樂作品

  - **註：成員的個人部分請參閱成員個人「音樂作品列表」條目**

****

:\***錄音室專輯：**

1.  《[女生宿舍](../Page/女生宿舍_\(S.H.E\).md "wikilink")》（2001年）
2.  《[青春株式會社](../Page/青春株式會社.md "wikilink")》（2002年）
3.  《[美麗新世界](../Page/美麗新世界_\(專輯\).md "wikilink")》（2002年）
4.  《[Super Star](../Page/Super_Star_\(S.H.E\).md "wikilink")》（2003年）
5.  《[奇幻旅程](../Page/奇幻旅程.md "wikilink")》（2004年）
6.  《[Encore](../Page/Encore_\(S.H.E專輯\).md "wikilink")》（2004年）
7.  《[不想長大](../Page/不想長大.md "wikilink")》（2005年）
8.  《[Play](../Page/Play_\(S.H.E專輯\).md "wikilink")》（2007年）
9.  《[我的电台 FM S.H.E](../Page/我的电台_FM_S.H.E.md "wikilink")》（2008年）
10. 《[SHERO](../Page/SHERO.md "wikilink")》（2010年）
11. 《[花又開好了](../Page/花又開好了.md "wikilink")》（2012年）

:\***精選專輯：**

1.  《[Together
    新歌+精選](../Page/Together_\(S.H.E專輯\).md "wikilink")》（2003年）
2.  《[Forever 新歌+精選](../Page/Forever_\(S.H.E專輯\).md "wikilink")》（2006年）

:\***迷你專輯：**

1.  《[永遠都在](../Page/永遠都在.md "wikilink")》（2016年）

:\***復刻版卡帶：**

1.  《[小時帶 S.H.E's in
    style](../Page/小時帶_S.H.E's_in_style.md "wikilink")》復刻卡帶紀念組（2017年）

:\***數位單曲：**

1.  《[你曾是少年](../Page/你曾是少年.md "wikilink")》數位單曲（2015年）
2.  《[十七](../Page/十七_\(S.H.E歌曲\).md "wikilink")》數位單曲（2018年）

## 演唱會

  - **註：成員的個人部分請參閱成員個人條目**

****

1.  2004年～2006年《[奇幻樂園世界巡迴演唱會](../Page/奇幻樂園世界巡迴演唱會.md "wikilink")》（9場）
2.  2006年～2009年《[移動城堡世界巡迴演唱會](../Page/移動城堡世界巡迴演唱會.md "wikilink")》（12場）
3.  2009年～2010年《[愛而為一世界巡迴演唱會](../Page/愛而為一世界巡迴演唱會.md "wikilink")》（12場）
4.  2013年～2014年《[2gether
    4ever世界巡迴演唱會](../Page/Together_Forever世界巡迴演唱會.md "wikilink")》（22場）

## 影視作品

  - **註：成員的個人部分請參閱個人條目**

****

## 廣告代言

<div style="width: 100%; height:30em; overflow:auto; border: 2px solid lightgray; font-size:10pt">

  - 2001年

<!-- end list -->

  - [勵馨基金會](../Page/勵馨社會福利事業基金會.md "wikilink")[紅羽毛志工大使](../Page/紅羽毛.md "wikilink")
  - [新竹旅遊節代言](../Page/新竹市.md "wikilink")
  - [佐丹奴手表發表秀](../Page/佐丹奴.md "wikilink")
  - [華歌爾聲音代言人](../Page/華歌爾.md "wikilink")
  - [清風紙巾](../Page/清風.md "wikilink")
  - [Digimaster數碼相機](../Page/Digimaster.md "wikilink")—拍奇精靈
  - [魔力寶貝Online](../Page/魔力寶貝.md "wikilink")—PC 電玩
  - [阿爾卡特手機活動代言](../Page/阿爾卡特.md "wikilink")
  - [新加坡Fuzion](../Page/新加坡.md "wikilink") Smoothic 飲料

<!-- end list -->

  - 2002年

<!-- end list -->

  - [臺北國際馬拉松比賽代言](../Page/臺北國際馬拉松.md "wikilink")
  - [統一多果汁飲料](../Page/統一企業.md "wikilink")
  - [華歌爾聲音代言人](../Page/華歌爾.md "wikilink")
  - [Digimaster](../Page/Digimaster.md "wikilink") 數碼相機—DG-2100 綿羊機
  - [N-age美麗新世界Online](../Page/N-age.md "wikilink")
  - [Bobson牛仔褲](../Page/Bobson.md "wikilink")
  - [博士倫隱形眼鏡](../Page/博士倫.md "wikilink")

<!-- end list -->

  - 2003年

<!-- end list -->

  - [Yamaha](../Page/Yamaha.md "wikilink") 機車
  - [7-11便利商店](../Page/7-11.md "wikilink")
  - [味全每日](../Page/味全公司.md "wikilink") C 系列飲料
  - [博士倫舒服能每日拋隱形眼鏡](../Page/博士倫.md "wikilink")
  - [愛戀金飾](../Page/愛戀.md "wikilink")
  - [康師傅](../Page/康師傅.md "wikilink") 3 + 2 餅乾
  - [求質運動鞋](../Page/求質.md "wikilink")
  - [唐獅服飾](../Page/唐獅.md "wikilink")

<!-- end list -->

  - 2004年

<!-- end list -->

  - [KIA](../Page/起亞汽車.md "wikilink") Eurostar 汽車
  - 臺灣[全中運動會代言](../Page/全國中等學校運動會.md "wikilink")
  - [臺灣兒福聯盟](../Page/台灣兒福聯盟.md "wikilink")—搶救生命棄兒不舍
  - 臺灣[TVBS](../Page/TVBS.md "wikilink")—寶貝我們的希望-讓夢想起飛
  - [博士倫舒服能系列隱形眼鏡](../Page/博士倫.md "wikilink")
  - 愛戀金飾
  - Heme護膚系列
  - [可口可樂](../Page/可口可樂.md "wikilink")—中國與香港地區
  - [英華達](../Page/英華達.md "wikilink")
    [Okwap手機](../Page/OKWAP千里.md "wikilink")
  - [7-11便利商店](../Page/7-11.md "wikilink")
  - [S\&K服飾](../Page/S&K.md "wikilink")
  - [新絕代雙驕 Online](../Page/新絕代雙驕系列.md "wikilink")
  - [家樂福月餅](../Page/家樂福.md "wikilink")

<!-- end list -->

  - 2005年

<!-- end list -->

  - [Digimaster candy](../Page/Digimaster_candy.md "wikilink") MP3
  - 愛戀金飾
  - [康師傅餅乾](../Page/康師傅.md "wikilink")
  - [KIA](../Page/起亞汽車.md "wikilink") Eurostar汽車
  - [7-11便利商店](../Page/7-11.md "wikilink")
  - [雅客益牙木糖醇](../Page/雅客益.md "wikilink")
  - [魔獸世界](../Page/魔獸世界.md "wikilink") Online
  - [博士倫舒服能系列隱形眼鏡](../Page/博士倫.md "wikilink")
  - [Heme護膚系列](../Page/Heme.md "wikilink")
  - [達芙妮](../Page/達芙妮.md "wikilink") Daphne女鞋
  - 新絕代雙驕Online
  - [可口可樂](../Page/可口可樂.md "wikilink")—中國大陸與香港地區
  - S\&K 服飾
  - [台灣兒福聯盟](../Page/兒童福利聯盟.md "wikilink")—搶救生命棄兒不舍

<!-- end list -->

  - 2006年

<!-- end list -->

  - [Daphne](../Page/Daphne.md "wikilink")[達芙妮女鞋](../Page/達芙妮.md "wikilink")
  - [雅迪電動車](../Page/雅迪.md "wikilink")
  - Nanolife奈米生活
  - [7-11便利商店](../Page/7-11.md "wikilink")
  - [中國移動](../Page/中國移動.md "wikilink")
    M-zone[動感地帶](../Page/動感地帶.md "wikilink")
  - [Casio](../Page/Casio.md "wikilink") 手錶
  - [海昌隐形眼镜](../Page/海昌.md "wikilink")
  - [可口可樂](../Page/可口可樂.md "wikilink")—中國與香港地區
  - [博士倫舒服能系列隱形眼鏡](../Page/博士倫.md "wikilink")
  - [OKWAP](../Page/OKWAP.md "wikilink")[英華達手機](../Page/英華達.md "wikilink")
  - [Samuel & KEVIN](../Page/Samuel_&_KEVIN.md "wikilink") 服飾
  - 兒童福利聯盟文教基金會
  - 臺灣農業委員會—國產牛乳
  - 雅客木糖醇
  - [康师傅餅乾](../Page/康师傅.md "wikilink")

<!-- end list -->

  - 2007年

<!-- end list -->

  - [7-11](../Page/7-11.md "wikilink") 便利商店 超商年度形象代言人
      - [7-11](../Page/7-11.md "wikilink") Always Open
        租房子篇（與[飛輪海](../Page/飛輪海.md "wikilink")）
      - [7-11](../Page/7-11.md "wikilink") Always Open
        米飯篇（[Ella](../Page/陳嘉樺.md "wikilink")、[吳尊](../Page/吳尊.md "wikilink")、[炎亞綸](../Page/炎亞綸.md "wikilink")）
      - [7-11](../Page/7-11.md "wikilink") Always
        Open-海洋音樂祭音浪來襲篇（[Hebe](../Page/田馥甄.md "wikilink")、[汪東城](../Page/汪東城.md "wikilink")、[炎亞綸](../Page/炎亞綸.md "wikilink")）
      - [7-11](../Page/7-11.md "wikilink") Always Open
        夏日輕功秘笈篇（[Selina](../Page/任家萱.md "wikilink")、[汪東城](../Page/汪東城.md "wikilink")、[辰亦儒](../Page/辰亦儒.md "wikilink")）
  - Samuel & KEVIN 服飾
  - [博士倫舒服能系列隱形眼鏡](../Page/博士倫.md "wikilink")
  - [可口可樂](../Page/可口可樂.md "wikilink")—中國與香港地區
  - [海昌隱形眼鏡](../Page/海昌.md "wikilink")
  - [兒童福利聯盟文教基金會](../Page/兒童福利聯盟.md "wikilink")
  - 公益－鲜乳公益大使
  - [世界展望會](../Page/世界展望會.md "wikilink")
  - [中國移動](../Page/中國移動.md "wikilink")
    M-zone[動感地帶](../Page/動感地帶.md "wikilink")
  - [Casio手錶](../Page/Casio.md "wikilink")
  - [達芙妮Daphne女鞋](../Page/達芙妮.md "wikilink")
  - [光泉茉莉茶園](../Page/光泉.md "wikilink")
  - C\&B 詩恩碧彩妝
  - [康乃馨](../Page/康乃馨.md "wikilink")—舞動天使衛生棉
  - [英華達](../Page/英華達.md "wikilink")[OKWAP手機代言](../Page/OKWAP千里.md "wikilink")
  - 雅客木糖醇
  - 東方駱駝服飾
  - Nanolife

<!-- end list -->

  - 2008年

<!-- end list -->

  - [蒙牛酸酸乳](../Page/蒙牛集團.md "wikilink")
      - 蒙牛酸酸乳 道歉篇（與[飛輪海](../Page/飛輪海.md "wikilink")）
      - 蒙牛酸酸乳 做主篇（與[飛輪海](../Page/飛輪海.md "wikilink")）
      - 蒙牛酸酸乳 吸管篇（與[飛輪海](../Page/飛輪海.md "wikilink")）
  - [英華達](../Page/英華達.md "wikilink")[OKWAP](../Page/OKWAP千里.md "wikilink")
    A136手機代言
  - 雅迪電動車
  - 海昌隱形眼鏡
  - C\&B 詩恩碧彩妝
  - Top Girl系列女性時裝
  - [ViVa TV](../Page/ViVa_TV.md "wikilink") 花舞春天Top Girl系列女性時裝
  - [卡西歐Baby](../Page/Casio.md "wikilink")-G狗狗寵物錶
  - [麥當勞勁辣雞腿堡](../Page/麥當勞.md "wikilink")
  - [達芙妮妮女鞋](../Page/達芙妮.md "wikilink")
  - [中國移動M](../Page/中國移動.md "wikilink")-zone[動感地帶](../Page/動感地帶.md "wikilink")
  - [可口可樂](../Page/可口可樂.md "wikilink")
  - [康乃馨](../Page/康乃馨.md "wikilink")—舞動天使衛生棉
  - [7-11便利商店](../Page/7-11.md "wikilink")
  - [光泉茉莉蜜茶](../Page/光泉.md "wikilink")
  - 有聲小說-神秘谷
  - [海爾手機](../Page/海爾集團.md "wikilink")
  - 七色花7-magic
  - 月月舒女性用品

<!-- end list -->

  - 2009年

<!-- end list -->

  - 2009 無菸臺灣反菸大使
  - [台灣世界展望會助學行動](../Page/台灣世界展望會.md "wikilink")
  - [蒙牛酸酸乳](../Page/蒙牛集團.md "wikilink")
  - [蒙牛果蔬酸酸乳](../Page/蒙牛集團.md "wikilink")
  - Muzee 網路廣播電臺代言人
  - [中国移动](../Page/中国移动.md "wikilink")[动感地带](../Page/动感地带.md "wikilink")
  - 月月舒
  - 碧修堂
  - 牽手 這樣紫阿
  - [麥當勞勁辣雞腿堡](../Page/麥當勞.md "wikilink")
  - [光泉茉莉茶園](../Page/光泉.md "wikilink")
  - [TOP GIRL服飾](../Page/TOP_GIRL.md "wikilink")
  - [達芙妮女鞋](../Page/達芙妮.md "wikilink")
  - 東方駱駝服飾
  - [Luna](../Page/Luna.md "wikilink") Online—PC電玩
  - heme保養品
  - 動作類型線上遊戲 [Dungeon &
    Fighter](../Page/地下城與勇士.md "wikilink")（[Selina](../Page/任家萱.md "wikilink")
    + [Hebe](../Page/田馥甄.md "wikilink")）
  - [Neway](../Page/Neway.md "wikilink")（香港）
  - 香约奶茶
  - [Osim](../Page/Osim.md "wikilink") uKimono

<!-- end list -->

  - 2010年

<!-- end list -->

  - [2010年臺北國際花卉博覽會](../Page/2010年臺北國際花卉博覽會.md "wikilink")**熱力花博**親善大使（2010年11月6日－2011年4月25日）

<!-- end list -->

  -
    《[SHERO](../Page/SHERO.md "wikilink")》專輯同名主打歌〈[SHERO](../Page/SHERO_\(歌曲\).md "wikilink")〉為**熱力花博**指定主題曲

<!-- end list -->

  - [達芙妮女鞋](../Page/達芙妮.md "wikilink")
  - [TOP GIRL](../Page/TOP_GIRL.md "wikilink") 服飾
  - 海俐恩隱形眼鏡
  - [Osim](../Page/Osim.md "wikilink") uKimono
  - 碧修堂
  - heme保養品
  - MUZEE網路電臺[USB](../Page/USB.md "wikilink")
  - [中国移動M](../Page/中国移動.md "wikilink")-zone[動感地帶](../Page/動感地帶.md "wikilink")　
  - 哎呀呀女生飾品
  - 東方駱駝
  - 蒙牛酸酸乳
  - 蒙牛果蔬酸酸乳
  - 2010 捷運盃亞洲街舞大賽代言人
  - 香約奶茶
  - 麗濤洗髮水

<!-- end list -->

  - 2011年

<!-- end list -->

  - [2010年臺北國際花卉博覽會](../Page/2010年臺北國際花卉博覽會.md "wikilink")**熱力花博**親善大使
  - [達芙妮女鞋](../Page/達芙妮.md "wikilink")
  - [TOP GIRL服飾](../Page/TOP_GIRL.md "wikilink")
  - MUZEE網路電臺USB
  - Fexata

<!-- end list -->

  - 2012年

<!-- end list -->

  - Fexata
  - [TOP GIRL服飾](../Page/TOP_GIRL.md "wikilink")
  - [達芙妮女鞋](../Page/達芙妮.md "wikilink")
  - 2013[台灣颩燈會](../Page/台灣燈會.md "wikilink")（[新竹](../Page/新竹.md "wikilink")[台灣燈會](../Page/台灣燈會.md "wikilink")）代言人
  - 香約奶茶
  - [7-11便利商店](../Page/7-11.md "wikilink")（心熱園）
  - [7-11便利商店](../Page/7-11.md "wikilink")（So Hot Eat）
  - [任天堂3DS](../Page/任天堂3DS.md "wikilink")
  - [M+ Messenger智慧型手機即時通訊軟體](../Page/M+_Messenger.md "wikilink")
  - 哎呀呀女生飾品

<!-- end list -->

  - 2013年

<!-- end list -->

  - 2013[台灣颩燈會](../Page/台灣燈會.md "wikilink")（[新竹](../Page/新竹.md "wikilink")[台灣燈會](../Page/台灣燈會.md "wikilink")）代言人（形象主題曲《迫不及待》收錄於S.H.E《[花又開好了](../Page/花又開好了.md "wikilink")》專輯）
  - [7-11便利商店](../Page/7-11.md "wikilink")（Hello Kitty印章）
  - 碧修堂
  - 温碧泉
  - [7-11便利商店](../Page/7-11.md "wikilink")（心熱園）

<!-- end list -->

  - 2014年

<!-- end list -->

  - 溫碧泉
  - [教育部體育署及](../Page/教育部體育署.md "wikilink")[董氏基金會](../Page/董氏基金會.md "wikilink")「SH150方案」代言人

<!-- end list -->

  - 2016年

<!-- end list -->

  -
<!-- end list -->

  - 2017年

<!-- end list -->

  - World Gym世界健身中心

</div>

## 得獎與提名

  - *' 註：成員的個人部分請參閱成員個人條目*'

****

## 相關連結

  -
## 參考文獻

## 外部連結

  -
  - [華研國際音樂官方網站](https://www.him.com.tw/artistscon2.php?tid=9)

  -
  -
  -
  -
[\*](../Category/S.H.E.md "wikilink")
[Category:2001年成立的音樂團體](../Category/2001年成立的音樂團體.md "wikilink")
[Category:華語流行音樂團體](../Category/華語流行音樂團體.md "wikilink")
[Category:台灣流行音樂團體](../Category/台灣流行音樂團體.md "wikilink")
[Category:台灣演唱團體](../Category/台灣演唱團體.md "wikilink")
[Category:臺灣女子演唱團體](../Category/臺灣女子演唱團體.md "wikilink")
[Category:台灣華語流行音樂歌手](../Category/台灣華語流行音樂歌手.md "wikilink")
[Category:台灣女歌手](../Category/台灣女歌手.md "wikilink")
[Category:台灣電視女演員](../Category/台灣電視女演員.md "wikilink")
[Category:台灣電影女演員](../Category/台灣電影女演員.md "wikilink")
[Category:台灣電視主持人](../Category/台灣電視主持人.md "wikilink")
[Category:台灣綜藝節目主持人](../Category/台灣綜藝節目主持人.md "wikilink")
[Category:音樂風雲榜十年盛典十年最具影響力音樂人物（港台）](../Category/音樂風雲榜十年盛典十年最具影響力音樂人物（港台）.md "wikilink")
[Category:音乐风云榜最受欢迎女歌手得主](../Category/音乐风云榜最受欢迎女歌手得主.md "wikilink")
[Category:金曲獎最佳演唱組合獎獲得者](../Category/金曲獎最佳演唱組合獎獲得者.md "wikilink")
[Category:單飛不解散團體](../Category/單飛不解散團體.md "wikilink")
[Category:三人組](../Category/三人組.md "wikilink")
[Category:2001年台灣建立](../Category/2001年台灣建立.md "wikilink")
[Category:台湾之最](../Category/台湾之最.md "wikilink")

1.

2.

3.
4.

5.

6.

7.

8.

9.

10.

11.

12.

13.

14.

15.
16.
17.

18.
19. [SHE體驗消防員　ELLA高樓跳下受傷](http://www.tvbs.com.tw/news/news_list.asp?no=jean20030729170755)

20.

21.

22.

23. [陳嘉樺不慎遭火吻](http://www.libertytimes.com.tw/2005/new/jun/23/today-show7.htm)


24. [奇幻樂園玩不夠，S.H.E再建『移動城堡』開唱！.iNEWS娛樂新聞.2010-11-17](http://inews.iwant-radio.com/archives/11914)

25.
26.
27. [完全娛樂S.H.E 14週年專題報導](https://www.youtube.com/watch?v=xZnhre0CjxE)

28. [答謝歌迷多年支持，"S.H.E社長"簽唱比照尾牙送出365個獎項！](http://inews.iwant-radio.com/archives/13908)

29. [S.H.E唱中國話
    被譙捧老中LP](http://www.libertytimes.com.tw/2007/new/may/2/today-show1.htm)


30. [「中國話」風波發生後，S.H.E首度公開說明](http://www.taipeitoday.com.tw/modules/news/article.php?storyid=12026)


31.

32.

33.

34.

35. [S.H.E香港開唱
    粉絲安可20分鐘不走-中天新聞](http://www.youtube.com/watch?v=8ihtqVTjhQ8&feature=fvwrel)

36.

37.

38. [S.H.E任熱力花博親善大使
    演唱花博指定歌曲「SHERO」婦女節首度曝光](http://www.wowonews.com/2010/03/she-shero.html)

39. [年度唱片銷量 小豬15.5萬張 首度踹下雙J
    登基唱銷王](https://www.youtube.com/watch?v=hWxi5oRbFV8)

40. [S.H.E宣佈下半年將單飛
    稱刻意不玩性感](http://news.shm.com.cn/2010-04/24/content_2931944.htm)

41. [SHE永遠不分開](http://www.merit-times.com.tw/NewsPage.aspx?Unid=179659)

42. [S.H.E就算結婚也不解散
    笑言要一起唱到80歲](http://entertainment.dbw.cn/system/2010/04/26/052472377.shtml)

43. [田馥甄預購成績嚇嚇叫！　Selina、Ella「分身」力挺](http://www.nownews.com/2010/09/11/37-2645453.htm)(2010/09/11
    10:14)

44. [Selina痊癒以年算
    俞灝明深二度灼傷沒有毀容](http://news.sina.com.tw/article/20101026/3890135.html)
     2010/10/26 北京新浪網

45. [Selina受傷的模擬視](http://ent.kankanews.com//vods/ff8080812a9521a7012aa3d595457267/ff8080812bd512bf012bda059267042a/)


46. [亮相8分鐘S.H.E再合體Selina戰勝灼傷出院](http://tw.nextmedia.com/applenews/article/art_id/33127068/IssueID/20110120)
    2011-01-20

47. [S.H.E三缺一 演唱喊卡、專輯再等等](http://www.shcaoan.com/wy/caesar/70010.html)

48. [為免Selina壓力過大
    阿中宣佈婚禮延期](http://stars.udn.com/newstars/collect/CollectPage.do?cid=6978)


49. [Selina「重作一個夢」12/16正式發行\!\!\!\!](http://www.him.com.tw/forum_1.asp?fid=53797&sid=2)
    2011-12-9華研官網

50. [图文：Ella订婚仪式现场-坐花轿的Ella](http://ent.sina.com.cn/s/p/2012-04-16/14113606778.shtml)
    2012-04-16

51. [即／Ella開心邀帥尪結婚去　Hebe、大S興奮早起盼婚禮.](http://www.ettoday.net/news/20120505/44107.htm)
    2012-05-05

52. [S.H.E.金曲強勢回歸！Selina足蹬5公分高跟鞋復出](http://www.nownews.com/2012/06/25/11836-2827646.htm)
    2012-6-25

53.

54.

55.

56. [S.H.E合體錄音
    彩帶紅毯迎接](http://tw.news.yahoo.com/she%E5%90%88%E9%AB%94%E9%8C%84%E9%9F%B3-%E5%BD%A9%E5%B8%B6%E7%B4%85%E6%AF%AF%E8%BF%8E%E6%8E%A5-124816075.html)
    2012-07-12中央社

57.

58. [S.H.E宿舍規定「男賓禁留宿」](http://www.nexttv.com.tw/news/realtime/entertainment/10490979/privacy)

59. [S.H.E巡演限量15場‧大馬場次720舉行](http://ent.sinchew.com.my/node/38907)

60.

61.

62. [S.H.E攻蛋Selina忙作媒
    Hebe黃腔逗尤秋興](http://ent.ltn.com.tw/news/paper/803459)

63.

64.

65.

66.

67.

68.

69.

70.

71.

72.

73.

74.

75.

76.

77.

78.

79.

80.

81.

82.
83.

84.

85.

86.

87.

88. [S.H.E「今正式離開」！華研聲明公開3人收入](https://star.ettoday.net/news/1270309#ixzz5SdiLMObQ)

89. [SHE自立門戶！公司名稱曝光竟藏姊妹情…粉絲：我笑了](https://www.setn.com/e/News.aspx?NewsID=436373)