**S**, **s** 是[拉丁字母中的第](../Page/拉丁字母.md "wikilink")19个英文字母。

[闪族语的](../Page/闪族语.md "wikilink")
（弓）的发音与现代[英语中的连音SH的发音相同](../Page/英语.md "wikilink")，都发作。在[希腊语中](../Page/希腊语.md "wikilink")，只用一个[音位](../Page/音位.md "wikilink")而没有，所以希腊语中的（sigma）用来表示希腊语中的发音。*sigma*这个名字可能来源于闪族语字母"Sâmek"而不是"Šîn"。在[伊特鲁里亚语和](../Page/伊特鲁里亚语.md "wikilink")[拉丁语中](../Page/拉丁语.md "wikilink")，的音值仍然保留着，只是到了现代，S开始表达其他的发音，像在[匈牙利语中的](../Page/匈牙利语.md "wikilink")或英语、[法语和](../Page/法语.md "wikilink")[德语中的](../Page/德语.md "wikilink")（在英语中的*rise*；法语中的*lisez*、“读！（命令式，复数）”；德语中的*lesen*，“读”）。

*s*的另一个古语形式 ſ 称为[长s](../Page/长s.md "wikilink")（long
s）或中间音*s*。它用于词语的开始或中间；现代形式的短音s或结尾*s*用于单词的结尾。ſs和在一起后来变成德语中的*[ess-tsett](../Page/ß.md "wikilink")*（ß）。

在[北约音标字母中](../Page/北约音标字母.md "wikilink")，使用*Sierra*表示字母S。

## 字母S的含意

## 字符编码

| 字符编码                               | [ASCII](../Page/ASCII.md "wikilink") | [Unicode](../Page/Unicode.md "wikilink") | [EBCDIC](../Page/EBCDIC.md "wikilink") | [摩斯电码](../Page/摩斯电码.md "wikilink") |
| ---------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- | ---------------------------------- |
| [大写](../Page/大写字母.md "wikilink") S | 83                                   | 0053                                     | 226                                    | `···`                              |
| [小写](../Page/小写字母.md "wikilink") s | 115                                  | 0073                                     | 162                                    |                                    |

## 其他表示法

## 发音

[清齿龈擦音](../Page/清齿龈擦音.md "wikilink")

## 在大眾文化中

  - S身材表示身材好。
  - S代表[SM之中的施虐傾向者](../Page/虐戀.md "wikilink")（Sadism）
  - 在[日渡早紀的](../Page/日渡早紀.md "wikilink")《[地球守護靈](../Page/地球守護靈.md "wikilink")》，S為主角小林輪所用的匿名

## 参看

### s的变体

  - [ß](../Page/ß.md "wikilink")（有时写成ss）

  - （长s）

  - [ʃ](../Page/ʃ.md "wikilink") — Esh

  - ∫ — [积分符号](../Page/积分.md "wikilink")

  - $ — [錢的符號](../Page/錢.md "wikilink")

### 其他字母中的相近字母

  - （[希腊字母](../Page/希腊字母.md "wikilink") Sigma）

  - （[西里尔字母](../Page/西里尔字母.md "wikilink") Es）

### 与S相似但无任何关系的字母

  - （西里尔字母 Dze，只在[马其顿语使用](../Page/马其顿语.md "wikilink")）

[Category:拉丁字母](../Category/拉丁字母.md "wikilink")