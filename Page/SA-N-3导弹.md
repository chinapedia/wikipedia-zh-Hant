**M-11**«Шторм»，（M-11
Shtorm，英文:Storm）[火箭炮兵裝備總局命名為](../Page/俄羅斯國防部火箭炮兵裝備總局.md "wikilink")4K60。[北約代號SA](../Page/北約代號.md "wikilink")-N-3「高腳杯」（Goblet）。此型導彈1969年服役，部署於[航空母艦上](../Page/航空母艦.md "wikilink")。

4K60，是一種全天候中、低空艦對空導彈系統，通常置於雙聯發射架上。該導彈長6.1米，重845公斤，彈頭80公斤。射程可達55公里，飛行速度2-3[馬赫](../Page/馬赫.md "wikilink")。

4K60主要用於攻擊戰機，也具有某種反艦能力。該系統主要由導彈、雙聯裝發射架和「前燈」（Head
Lights）半主動制導雷達（semi-active radar
homing）組成。4K60從1969年開始服役，歷史上裝備過蘇聯海軍軍艦22艘，包括三艘[基輔級航空母艦](../Page/基輔級.md "wikilink")、兩艘[莫斯科級](../Page/莫斯科級直升機航空母艦.md "wikilink")[直升機母艦](../Page/直升機母艦.md "wikilink")，七艘[卡拉級導彈](../Page/卡拉级巡洋舰.md "wikilink")[巡洋艦和](../Page/巡洋艦.md "wikilink")10艘克列斯塔Ⅱ級導彈巡洋艦。航空母艦裝有兩套4K60（四發射架），載彈24枚。巡洋艦則可多至八套（16發射架），載彈80枚以上，形成強大防空力量。

4K60尚無實戰記錄。

最初版本的4K60 M-11 "Shtorm"系統裝備了V-611型飛彈，北約代號為SA-N-3，另有4K60 M-11
"Shtorm-M"使用СУО Гром-М的版本，升級版的4K65 M-11
"Shtorm-N"系統裝備了V-611M型飛彈,北約代號為SA-N-3B。

## 外部連結

  - [SA-N-3發射情況](https://web.archive.org/web/20070929152846/http://www.ndcnc.gov.cn/datalib/2003/Weapon/DL/DL-65477/Weapon/Pic/0/imageView/__end_key__)

[Category:蘇聯面對空飛彈](../Category/蘇聯面對空飛彈.md "wikilink")
[Category:區域防空飛彈](../Category/區域防空飛彈.md "wikilink")
[Category:艦載防空飛彈](../Category/艦載防空飛彈.md "wikilink")
[Category:俄羅斯飛彈](../Category/俄羅斯飛彈.md "wikilink")
[Category:反艦飛彈](../Category/反艦飛彈.md "wikilink")