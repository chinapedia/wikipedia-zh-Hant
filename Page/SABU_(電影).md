《**SABU**》是由[日本](../Page/日本.md "wikilink")[作家](../Page/作家.md "wikilink")[山本周五郎的同名原作改編的](../Page/山本周五郎.md "wikilink")[時代劇](../Page/時代劇.md "wikilink")。

為紀念[名古屋電視台開台](../Page/名古屋電視台.md "wikilink")40週年，最初於[朝日電視台](../Page/朝日電視台.md "wikilink")2002年5月14日播放。因為反響不錯，由[電影旬報社出品](../Page/電影旬報社.md "wikilink")，在日本全國4個劇場公映。10月5日以[新宿武藏野館為開端](../Page/新宿.md "wikilink")，11月9日大阪，11月30日[名古屋](../Page/名古屋.md "wikilink")·[札幌](../Page/札幌.md "wikilink")。[電影版增加了電視版未播放的內容](../Page/電影.md "wikilink")，延長至121分鐘。2003年2月22日DVD發售。

[劇情透過](../Page/劇情.md "wikilink")[女主角的](../Page/女主角.md "wikilink")[回憶描寫](../Page/回憶.md "wikilink")[江戶時代的](../Page/江戶時代.md "wikilink")[生活](../Page/生活.md "wikilink")，榮二和SABU之间真挚的[友情以及與](../Page/友情.md "wikilink")[女主角的](../Page/女主角.md "wikilink")[愛情](../Page/愛情.md "wikilink")。此片以較為[詩情的方式描寫](../Page/詩情.md "wikilink")，與[導演](../Page/導演.md "wikilink")[三池崇史大多數影片的](../Page/三池崇史.md "wikilink")[恐怖](../Page/恐怖.md "wikilink")[血腥有所不同](../Page/血腥.md "wikilink")。

## 演員

  - [藤原龍也](../Page/藤原龍也.md "wikilink") - 榮二
  - [妻夫木聰](../Page/妻夫木聰.md "wikilink") - SABU
  - [田畑智子](../Page/田畑智子.md "wikilink") - おのぶ
  - [吹石一恵](../Page/吹石一恵.md "wikilink") - おすえ
  - [澤田研二](../Page/澤田研二.md "wikilink") - 岡安喜兵衛
  - [六平直政](../Page/六平直政.md "wikilink") - 松田櫨蔵
  - [山田辰夫](../Page/山田辰夫.md "wikilink") - 小島良二郎
  - [有薗芳記](../Page/有薗芳記.md "wikilink") - 与平
  - [堀部圭亮](../Page/堀部圭亮.md "wikilink") - 女街の六
  - [遠藤憲一](../Page/遠藤憲一.md "wikilink") - 義一
  - [山口祥行](../Page/山口祥行.md "wikilink") - 才次
  - [玉木宏](../Page/玉木宏.md "wikilink") - 金太
  - [合田雅吏](../Page/合田雅吏.md "wikilink") - 万吉
  - [八木小織](../Page/八木小織.md "wikilink") - おかめ
  - [西山繭子](../Page/西山繭子.md "wikilink") - おその
  - [田中要次](../Page/田中要次.md "wikilink") - 徳
  - [林知花](../Page/林知花.md "wikilink") - おせえ
  - [石丸謙二郎](../Page/石丸謙二郎.md "wikilink") - 儀兵衛
  - [大杉漣](../Page/大杉漣.md "wikilink") - 平藏

[Category:2002年电影](../Category/2002年电影.md "wikilink")
[Category:日本电影作品](../Category/日本电影作品.md "wikilink")