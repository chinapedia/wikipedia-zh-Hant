**音標字母評估法**（****）是一種電腦可讀的音標符號，它是以[國際音標為基礎](../Page/國際音標.md "wikilink")，並且只使用7位元中的可列印符號。

這種轉寫法是源自[歐洲共同體為六種歐洲語言](../Page/欧洲共同体.md "wikilink")，於1980年代展開的[歐洲資訊科技研究策略計劃中的資訊科技研究及發展項目](../Page/歐洲資訊科技研究策略計劃.md "wikilink")。如果國際音標的寫法和ASCII中的字母相同，就直接取用，否則就使用其他ASCII符號代替，例如\[`@`\]用來代表[schwa](../Page/schwa.md "wikilink")（國際音標：），\[`2`\]則代表法語「deux」中的元音（國際音標：），而\[`9`\]便代表法語「neuf」中的元音（國際音標：）。

下列語言現在都已經發展了正式的SAMPA表，每張表中都包含了該語言裡的所有語音：

<table style="width:99%;">
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/阿拉伯語.md" title="wikilink">阿拉伯語</a></li>
<li><a href="../Page/保加利亞語.md" title="wikilink">保加利亞語</a></li>
<li><a href="../Page/廣東話.md" title="wikilink">廣東話</a></li>
<li><a href="../Page/捷克語.md" title="wikilink">捷克語</a></li>
<li><a href="../Page/丹麥語.md" title="wikilink">丹麥語</a></li>
<li><a href="../Page/荷蘭語.md" title="wikilink">荷蘭語</a></li>
<li><a href="../Page/英語.md" title="wikilink">英語</a></li>
<li><a href="../Page/愛沙尼亞語.md" title="wikilink">愛沙尼亞語</a></li>
<li><a href="../Page/法語.md" title="wikilink">法語</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/德語.md" title="wikilink">德語</a></li>
<li><a href="../Page/希臘語.md" title="wikilink">希臘語</a></li>
<li><a href="../Page/希伯來語.md" title="wikilink">希伯來語</a></li>
<li><a href="../Page/匈牙利語.md" title="wikilink">匈牙利語</a></li>
<li><a href="../Page/意大利語.md" title="wikilink">意大利語</a></li>
<li><a href="../Page/挪威語.md" title="wikilink">挪威語</a></li>
<li><a href="../Page/波蘭語.md" title="wikilink">波蘭語</a></li>
<li><a href="../Page/葡萄牙語.md" title="wikilink">葡萄牙語</a></li>
<li><a href="../Page/羅馬尼亞語.md" title="wikilink">羅馬尼亞語</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/俄語.md" title="wikilink">俄語</a></li>
<li><a href="../Page/蘇格蘭語.md" title="wikilink">蘇格蘭語</a></li>
<li><a href="../Page/塞爾維亞-克羅埃西亞語.md" title="wikilink">塞爾維亞-克羅埃西亞語</a></li>
<li><a href="../Page/斯洛伐克語.md" title="wikilink">斯洛伐克語</a></li>
<li><a href="../Page/西班牙語.md" title="wikilink">西班牙語</a></li>
<li><a href="../Page/瑞典語.md" title="wikilink">瑞典語</a></li>
<li><a href="../Page/泰語.md" title="wikilink">泰語</a></li>
<li><a href="../Page/土耳其語.md" title="wikilink">土耳其語</a></li>
</ul></td>
</tr>
</tbody>
</table>

\[**`"s{mp@`**\]這串符號代表了「SAMPA」這詞在英語的發音。和國際音標一樣，SAMPA通常放在[方括號或](../Page/括号.md "wikilink")[斜線符號裡](../Page/斜線符號.md "wikilink")，以表示它並非字詞的一部分，而是用來標示讀音的音標。

## SAMPA的問題

SAMPA表有多種語言版本，每種版本設計時都只針對該語言所用到的音標，因此不同版本的SAMPA表並不兼容而產生衝突，所以SAMPA並不適合代表全部的國際音標。由於SAMPA表並不能全面地把整張[IPA表轉寫成ASCII符號](../Page/國際音標.md "wikilink")，於是便設計了[X-SAMPA這種可以代表所有國際音標的單一代碼表](../Page/X-SAMPA.md "wikilink")，不用再考慮不同語言的問題。

## 參見

  - 簡明的[英語SAMPA表](../Page/英語SAMPA表.md "wikilink")。
  - 一張較完整、能代表歐洲大部分語音的[SAMPA表](../Page/SAMPA表.md "wikilink")。
  - [基爾斯漢邦](../Page/基爾斯漢邦.md "wikilink")（Kirshenbaum）有時也稱作ASCII-IPA，是另一種[ASCII音標字母](../Page/ASCII.md "wikilink")。
  - [國際音標](../Page/國際音標.md "wikilink")
  - [X-SAMPA和SAMPA類似](../Page/X-SAMPA.md "wikilink")，但沒有不同語言版本之分，包含了整張國際音標表中的所有音標。

## 外部連結

  - [SAMPA電腦可讀音標](http://www.phon.ucl.ac.uk/home/sampa/home.htm)
  - [英語的SAMPA](http://www.phon.ucl.ac.uk/home/sampa/english.htm)
  - [蘇格蘭語的SAMPA](http://www.scots-online.org/airticles/phonetics.htm)
  - [SAMPA與IPA的比較](https://web.archive.org/web/20070307055106/http://mmc.sinica.edu.tw/sampa.htm)

[Category:音標](../Category/音標.md "wikilink")