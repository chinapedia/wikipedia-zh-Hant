本表列出在[SARS爆发期間](../Page/SARS事件.md "wikilink")[殉職的醫療工作者及醫療相關的人士](../Page/殉職.md "wikilink")，並依照[國籍與](../Page/國籍.md "wikilink")[逝世](../Page/逝世.md "wikilink")[日期英文排序](../Page/日期.md "wikilink")。

##

| 姓名                                                 | 職位                                                                   | 殉職日期             |
| -------------------------------------------------- | -------------------------------------------------------------------- | ---------------- |
| [范信德](../Page/范信德.md "wikilink")                   | 广州[中山大学附属第二医院司机](../Page/中山大学附属第二医院.md "wikilink")                   | 2003年2月23日\[1\]  |
| [叶欣](../Page/叶欣.md "wikilink")（1956-2003）          | [广东省中医院二沙分院急诊科护士长](../Page/广东省中医院.md "wikilink")\[2\]\[3\]           | 2003年3月25日       |
| [李晓红](../Page/李晓红_\(医生\).md "wikilink")（1974-2003） | [武警北京总队医院内二科主治医师](../Page/武警北京总队医院.md "wikilink")\[4\]               | 2003年4月16日       |
| [段力军](../Page/段力军.md "wikilink")                   | [北京中医药大学东直门医院急诊科医生](../Page/北京中医药大学.md "wikilink")                   | 2003年4月19日\[5\]  |
| [鄧練賢](../Page/鄧練賢.md "wikilink")( 1949 -2003)      | 廣州[中山大學附屬第三醫院傳染病科黨支部書記](../Page/中山大學附屬第三醫院.md "wikilink")、主任醫師）\[6\] | 2003年4月21日       |
| [梁世奎](../Page/梁世奎.md "wikilink")（1946-2003）        | [山西省人民医院急诊科副主任](../Page/山西省人民医院.md "wikilink")\[7\]                  | 2003年4月24日       |
| [王晶](../Page/王晶_\(护士\).md "wikilink")（1971-2003）   | [北京大学人民医院急诊科护士](../Page/北京大学人民医院.md "wikilink")                      | 2003年4月27日       |
| [王建华](../Page/王建华_\(护士\).md "wikilink")            | 北京市通州区[潞河医院血液科护士长](../Page/潞河医院.md "wikilink")                       | 2003年5月4日\[8\]   |
| [杨涛](../Page/杨涛_\(医生\).md "wikilink")(1960-2003)   | [北京市通州區潞河医院放射科医生](../Page/北京市通州區潞河医院.md "wikilink")\[9\]             | 2003年5月6日        |
| [陈洪光](../Page/陈洪光.md "wikilink")                   | [广州市胸科医院重症监护室主任](../Page/广州市胸科医院.md "wikilink")                      | 2003年5月7日\[10\]  |
| [丁秀兰](../Page/丁秀兰.md "wikilink")（1954-2003）        | [北京大学人民医院主任医师](../Page/北京大学人民医院.md "wikilink")，急诊科副主任、急诊科党支部书记\[11\] | 2003年5月13日       |
| [李采尧](../Page/李采尧.md "wikilink")                   | 武警北京总队医院副主任医师                                                        | 2003年5月29日\[12\] |
| [张林国](../Page/张林国.md "wikilink")                   | [中国中医科学院西苑医院副主任医师](../Page/中国中医科学院.md "wikilink")                    | 2003年6月9日\[13\]  |

##

[HK_Park_Memorial_of_Fighting_SARS_Heros_bronze_statue_6_CHENG_Ha_Yan_b.JPG](https://zh.wikipedia.org/wiki/File:HK_Park_Memorial_of_Fighting_SARS_Heros_bronze_statue_6_CHENG_Ha_Yan_b.JPG "fig:HK_Park_Memorial_of_Fighting_SARS_Heros_bronze_statue_6_CHENG_Ha_Yan_b.JPG")

| 姓名                                            | 職位                                                   | 殉職日期       |
| --------------------------------------------- | ---------------------------------------------------- | ---------- |
| [劉大鈞](../Page/劉大鈞\(醫生\).md "wikilink")（-2003） | 香港兒科私人執業醫生\[14\]                                     | 2003年4月3日  |
| [劉永佳](../Page/劉永佳.md "wikilink")（1965-2003）   | [屯門醫院胸肺科内科護士](../Page/屯門醫院.md "wikilink")\[15\]      | 2003年4月26日 |
| [謝婉雯](../Page/謝婉雯.md "wikilink")（1968-2003）   | 屯門醫院胸肺科內科醫生\[16\]                                    | 2003年5月13日 |
| [鄧香美](../Page/鄧香美.md "wikilink")（1967-2003）   | [基督教聯合醫院健康服務助理](../Page/基督教聯合醫院.md "wikilink")\[17\] | 2003年5月16日 |
| [劉錦蓉](../Page/劉錦蓉.md "wikilink")（1956-2003）   | 基督教聯合醫院健康服務助理\[18\]                                  | 2003年5月27日 |
| [王庚娣](../Page/王庚娣.md "wikilink")（1950-2003）   | [威爾斯親王醫院健康服務助理](../Page/威爾斯親王醫院.md "wikilink")\[19\] | 2003年5月31日 |
| [張錫憲](../Page/張錫憲.md "wikilink")（1945-2003）   | 香港耳鼻喉科私人執業醫生\[20\]                                   | 2003年5月31日 |
| [鄭夏恩](../Page/鄭夏恩.md "wikilink")（1973-2003）   | [大埔醫院醫生](../Page/大埔醫院.md "wikilink")\[21\]           | 2003年6月1日  |

##

  - [卡爾婁·武爾班尼](../Page/卡爾婁·武爾班尼.md "wikilink")（2003年3月14日，義大利籍[世界卫生组织派驻](../Page/世界卫生组织.md "wikilink")[越南](../Page/越南.md "wikilink")[河内医生](../Page/河内.md "wikilink")，亦是首位發現[SARS的醫生](../Page/SARS.md "wikilink")）\[22\]

##

| 姓名                               | 職位                                            | 殉職日期             |
| -------------------------------- | --------------------------------------------- | ---------------- |
| [王复赐](../Page/王复赐.md "wikilink") | [陈笃生医院医生](../Page/陈笃生医院.md "wikilink")        | 2003年4月9日\[23\]  |
| [赵光灏](../Page/赵光灏.md "wikilink") | [中央医院血管外科顾问医生](../Page/新加坡中央医院.md "wikilink") | 2003年4月22日\[24\] |

##

| 姓名                                 | 職位                                                    | 殉職日期        |
| ---------------------------------- | ----------------------------------------------------- | ----------- |
| [陳靜秋](../Page/陳靜秋.md "wikilink")   | 台北[和平醫院護理長](../Page/和平醫院.md "wikilink")\[25\]         | 2003年5月1日   |
| [陳呂麗玉](../Page/陳呂麗玉.md "wikilink") | 台北和平醫院清潔環保員\[26\]                                     | 2003年5月3日   |
| [胡貴芳](../Page/胡貴芳.md "wikilink")   | 台北[仁濟醫院護理師](../Page/仁濟醫院_\(台北市\).md "wikilink")\[27\] | 2003年5月7日   |
| [林佳鈴](../Page/林佳鈴.md "wikilink")   | 台北和平醫院護理師\[28\]                                       | 2003年5月11日  |
| [郭國展](../Page/郭國展.md "wikilink")   | 台北市政府消防局第三救災救護大隊延平分隊救護隊員\[29\]                        | 2003年5月15日  |
| [林重威](../Page/林重威.md "wikilink")   | 台北和平醫院醫生\[30\]                                        | 2003年5月15日  |
| [林永祥](../Page/林永祥.md "wikilink")   | 高雄[長庚醫院内科医生](../Page/長庚醫院.md "wikilink")\[31\]        | 2003年5月16日  |
| [鄭雪慧](../Page/鄭雪慧.md "wikilink")   | 台北和平醫院護理部副主任\[32\]                                    | 2003年5月18日  |
| [楊淑媜](../Page/楊淑媜.md "wikilink")   | 台北和平醫院護理書記\[33\]                                      | 2003年5月28日  |
| [蔡巧妙](../Page/蔡巧妙.md "wikilink")   | 台北和平醫院醫檢師\[34\]                                       | 2003年6月13日  |
| [簡惠珍](../Page/簡惠珍.md "wikilink")   | 台北榮民總醫院護理長\[35\]                                      | 2003年10月13日 |

## 相关作品

  - 电视剧《亞洲英雄》（2003）
  - 《社会记录·医生日记》（2003，[中国中央电视台](../Page/中国中央电视台.md "wikilink")）
  - 电影《[天作之盒](../Page/天作之盒.md "wikilink")》（2004，香港粤语电影）
  - 纪录片《走過和平》（台湾[公共電視台](../Page/公共電視台.md "wikilink")）
  - 《社会记录·那年春天》（2007，中国中央电视台）
  - 《非典十年祭》（2013，[凤凰卫视](../Page/凤凰卫视.md "wikilink")）
  - 香港1:99電影行動︰《2003年春天的回憶》

## 參考文献

## 外部链接

  - [部分在抗击“非典”战争中以身殉职的医务工作者](http://news.xinhuanet.com/zhengfu/2003-05/08/content_862038.htm),
    新华网

## 参见

  - [嚴重急性呼吸系統綜合症](../Page/嚴重急性呼吸系統綜合症.md "wikilink")（SARS）
  - [SARS事件](../Page/SARS事件.md "wikilink")

[Category:2003年逝世](../Category/2003年逝世.md "wikilink")
[Category:SARS](../Category/SARS.md "wikilink")
[Category:死者列表](../Category/死者列表.md "wikilink")
[Category:殉職者](../Category/殉職者.md "wikilink")
[Category:死于传染病的人](../Category/死于传染病的人.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14. [1](http://www.rotary3450.org/district/pdg/peterwan/files/Memorialize%20Rotarian%20Lau%20Tai%20Kun.doc)
15. [2](http://www.people.com.cn/BIG5/shizheng/18/21/20030507/986142.html)
16.
17.
18.
19.
20.
21.
22. Donald G. McNeil Jr.: *[Disease's Pioneer Is Mourned as a
    Victim](http://www.nytimes.com/2003/04/08/science/sciencespecial/08PROF.html)*,
    The New York Times, April 8, 2003
23.
24.
25. [3](http://www.nownews.com/2003/05/01/10969-1448140.htm)
26.
27. [4](http://www.nownews.com/2003/05/07/319-1451237.htm)
28.
29. [5](http://enews.nfa.gov.tw/V4one-news.asp?NewsNo=2330)
30. [6](http://www.nownews.com/2003/05/15/23-1455030.htm)
31. [7](http://www.nownews.com/2003/09/04/10997-1507979.htm)
32.
33. [8](http://www.nownews.com/2003/05/28/23-1461166.htm)
34.
35.