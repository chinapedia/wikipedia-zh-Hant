**SBD無畏式**（）為[道格拉斯公司開發的](../Page/道格拉斯飛行器公司.md "wikilink")[艦上](../Page/艦載機.md "wikilink")[俯衝轟炸機](../Page/俯衝轟炸機.md "wikilink")，與[格魯門](../Page/格魯門公司.md "wikilink")[F4F野貓式](../Page/F4F戰鬥機.md "wikilink")[戰鬥機及](../Page/戰鬥機.md "wikilink")[TBD蹂躪者式魚雷轟炸機](../Page/TBD蹂躪者式魚雷轟炸機.md "wikilink")[魚雷](../Page/魚雷.md "wikilink")[攻擊機為二次大戰開戰時](../Page/攻擊機.md "wikilink")[美國三大主力](../Page/美國.md "wikilink")[艦載機](../Page/艦載機.md "wikilink")。主要於[第二次世界大戰時期活躍於](../Page/第二次世界大戰.md "wikilink")[太平洋戰場上](../Page/太平洋戰爭.md "wikilink")。

## 開發

無畏式最初誕生在繪圖板的時間約在1932年，當時稱為，而當時的諾斯洛普公司尚為道格拉斯飛機公司旗下之子公司。諾斯羅普BT-1設計是由[約翰·諾斯洛普與](../Page/約翰·諾斯洛普.md "wikilink")[艾德·海茵曼一同合作設計的低單翼](../Page/艾德·海茵曼.md "wikilink")、全金屬製艦載[俯衝轟炸機](../Page/俯衝轟炸機.md "wikilink")，誕生肇因在美國海軍1934年提出之最新艦載偵查轟炸機規格需求。當時的美國海軍希望新的偵查轟炸機與艦載轟炸機，新機型均需以全金屬結構製造、全收納設計起落架設計，偵查轟炸機機體起飛總重不超過5,000磅（2,268公斤）的同時具有500磅（226公斤）酬載；如果是艦載轟炸機則放寬到6,000磅（2,722公斤）起飛重量下有1,000磅（455公斤）酬載。同時新飛機還需要有可承擔9G重力狀態的俯衝用[減速板](../Page/空氣制動器.md "wikilink")。除了重量限制，為了符合航艦操作標準還提出了低速操作限制；美軍希望飛機可在25節逆風起飛、最低失速速度不得高於每小時60海浬速度（111公里/小時）。

1934年的需求案共計有6家飛機公司競爭，艦載轟炸機需求最後由設計的與諾斯洛普XBT-1出線、轟炸偵察機則是沃特SB2U得標；諾斯羅普在1934年11月20日和美軍簽約，XBT-1在1935年8月19日首次試飛，並得到美國海軍54架訂單，訂單在1938年4月交付完成；在諾斯洛普BT-1出廠之後，1936年11月美軍在精進BT-1設計的要求下簽署了XBT-2的合約。相較於BT-1，XBT-2使用發動機直徑較大，但輸出功率及重量皆較優秀的XR-1820-22發動機取代了[普惠R-1535發動機](../Page/普惠R-1535發動機.md "wikilink")，除此之外XBT-2設計了全新的起落架及尾翼，取消了複雜度較高的折疊式機翼。
[Northrop_XBT-1_and_XBT-2_comparison.jpg](https://zh.wikipedia.org/wiki/File:Northrop_XBT-1_and_XBT-2_comparison.jpg "fig:Northrop_XBT-1_and_XBT-2_comparison.jpg")
XBT-2設計時，諾斯洛普領導的團隊營運逐漸上軌道，諾斯洛普公司在1939年公司正式獨立；而道格拉斯公司則從1937年起逐漸接手諾斯羅普正執行的設計案。諾斯洛普BT-2後續設計由艾德·海茵曼所屬團隊在1938年完成，編號也改名為道格拉斯XSBD-1。

1939年2月，美國海軍接收第一架XSBD-1原型機，該機通過[美國國家航空諮詢委員會一系列的測試](../Page/美國國家航空諮詢委員會.md "wikilink")，因此2個月後，1939年4月8日美國海軍和道格拉斯簽約製造第一批SBD；分別為美國海軍陸戰隊下訂的57架SBD-1、美國海軍下訂的87架SBD-2，SBD-2搭載了較完整的防護設備與油箱。

比起TBD破壞者的開發，SBD的[金屬蒙皮技術更為成熟](../Page/金屬.md "wikilink")，使用了與[SBC式相同的穿孔式](../Page/SBC俯衝轟炸機.md "wikilink")[空氣](../Page/空氣.md "wikilink")[煞車](../Page/煞車.md "wikilink")[襟翼兼顧了結構強度與俯衝時機身穩定性](../Page/襟翼.md "wikilink")，不像[德國的](../Page/納粹德國.md "wikilink")[容克斯JU-87與](../Page/Ju_87俯衝轟炸機.md "wikilink")[日本的](../Page/大日本帝國.md "wikilink")[九九艦爆必須額外加裝維持穩定的副翼與固定式起落架維持穩定](../Page/九九式艦上爆擊機.md "wikilink")，收藏式的[起落架比起前兩者減低了更多的風阻](../Page/起落架.md "wikilink")，雖然飛機重量高於國外同級產品，但仍能維持相同速度與更好的飛行性能。

1940年5月1日，SBD-1首飛，隨後美軍單位接收到第一批戰機。雖然性能不俗，但歐洲戰場JU-87的戰果豐碩衝擊美軍；由美國國會議員所組成的杜魯門委員會認為SBD性能不足，不應繼續量產，但美國海軍決定繼續替SBD提升性能；1941年服役的SBD-3改換出力1000匹馬力的R-1820-52[發動機](../Page/發動機.md "wikilink")、與防彈裝甲以及更大的[炸彈掛載重量](../Page/空用炸彈.md "wikilink")，增加的出力彌補新裝備的重量，因此基本性能沒有下降，SBD-3也成為太平洋戰爭爆發後美國海軍最佳的空中攻擊利器，伴隨美軍走過最糟糕的一段時間。1942年推出的SBD-4是以增加[無線電導航裝置](../Page/無線電導航.md "wikilink")、改善電系為主，SBD-4將飛機電系輸出功率自12[伏特提升到](../Page/伏特.md "wikilink")24[伏特](../Page/伏特.md "wikilink")，雖然因為調整電系讓機上配備全盤翻整，延遲了配發時間，但使用較高電壓的電力燃油泵和漢密爾頓製可變螺距螺旋槳為飛機提供較佳的操縱反映表現，使飛機在飛行設定調節時更加靈活，SBD-4在1943年4月停產。

由於SBD接班人, [SB2C](../Page/SB2C.md "wikilink"),
性能普通飛行表現卻又糟糕透頂的結果，無畏式在二戰中持續改良以符合前線飛官的需求，並提供一定數量作為戰力必要補充；SBD家族產量最大者為SBD-5，在1943年2月服役，產線持續到1944年4月，SBD-5使用了輸出標準為1,200匹馬力的的R-1820-60發動機；1944年為美國海軍陸戰隊製造的450架SBD-6則是SBD系列的最終版本，換裝了岀力1350匹馬力的R-1820-66發動機及可以掛載副油箱的強化機翼提升航程，同時增加航速。

在1942年10月起，美國海軍開始替新生產的SBD-4上測試移植[雷達](../Page/雷達.md "wikilink")；到戰爭中後期SBD逐漸退出第一線攻擊任務後，有數百架的SBD家族裝上了雷達提供偵查與巡邏用途，包括對潛偵蒐的英國製ASV雷達、對海偵蒐用的西屋ASB-3雷達等。

## 戰歷

[Aircraft_spotted_on_the_forward_flight_deck_of_USS_Enterprise_(CV-6)_in_April_1942.jpg](https://zh.wikipedia.org/wiki/File:Aircraft_spotted_on_the_forward_flight_deck_of_USS_Enterprise_\(CV-6\)_in_April_1942.jpg "fig:Aircraft_spotted_on_the_forward_flight_deck_of_USS_Enterprise_(CV-6)_in_April_1942.jpg")上準備起飛的SBD無畏式，攝於1942年\]\]
[Planes_after_attacking_Kaga.jpg](https://zh.wikipedia.org/wiki/File:Planes_after_attacking_Kaga.jpg "fig:Planes_after_attacking_Kaga.jpg")號之後返航的SBD無畏式，圖中可見機身上受創的彈孔，攝於[中途島海戰](../Page/中途島海戰.md "wikilink")\]\]
正如其名，希望駕駛它的[飛行員可以不畏懼的勇往直前](../Page/飛行員.md "wikilink")，而確實SBD也獲得飛行員極高的評價，但是在二戰爆發時SBD也渡過了一段難過的日子。

1941年12月日軍偷襲珍珠港之際，美國海軍編制裡有584架SBD-3，美軍主力大型航艦包括薩拉托加、列克星頓、約克鎮、企業號等艦上至少都搭載了2支配備SBD之轟炸、偵查中隊，每中隊編制18架戰機。同時美國海軍陸戰隊也已有完整中隊換裝，因此在珍珠港時有不少SBD在地面上遭摧毀。

SBD首次攻擊任務則不是艦載機型執行，是由美國陸軍航空隊版本A-24實施；1942年2月17日，7架A-24轟炸[峇里島](../Page/峇里島.md "wikilink")，2架遭擊落、3架受創無法修復，在多國聯合艦隊敗退下A-24中隊很快地就在1942年3月初撤出[印尼](../Page/印尼.md "wikilink")。

1942年5月[珊瑚海戰役時](../Page/珊瑚海戰役.md "wikilink")，SBD中隊開始立下顯赫戰功，該役擊傷了[翔鶴號航空母艦](../Page/翔鶴號航空母艦.md "wikilink")、擊沉了[祥鳳號航空母艦](../Page/祥鳳號航空母艦.md "wikilink")，阻止了日本帝國海軍在新幾內亞海域擴張制海之企圖；1942年6月份的[中途島海戰時](../Page/中途島海戰.md "wikilink")，SBD中隊在TBF中隊的犧牲代價下創下空前戰績勝利，6分鐘內擊沉了日本引以為傲的海上主力：[赤城](../Page/赤城號航空母艦.md "wikilink")、[加賀](../Page/加賀號航空母艦.md "wikilink")、[蒼龍](../Page/蒼龍號航空母艦.md "wikilink")，並在後續炸毀了[飛龍](../Page/飛龍號航空母艦.md "wikilink")，計四艘[航空母艦](../Page/航空母艦.md "wikilink")。至1944年由於後繼機種[SB2C俯衝轟炸機的服役](../Page/SB2C俯衝轟炸機.md "wikilink")，才慢慢退居第二線。
而1944年SBD也加入了[英國皇家海軍的行列](../Page/英國皇家海軍.md "wikilink")，在北海對抗德軍的[U型潛艇](../Page/U-潛艇.md "wikilink")，同時SBD也以A-24之名加入[美國陸軍航空隊](../Page/美國陸軍航空隊.md "wikilink")，在地中海戰場上打擊[德國與](../Page/納粹德國.md "wikilink")[義大利的](../Page/義大利王國.md "wikilink")[裝甲部隊](../Page/裝甲車.md "wikilink")。

## 规格 (SBD-5)

[SBD-5_BuAer_3_view_drawing.jpg](https://zh.wikipedia.org/wiki/File:SBD-5_BuAer_3_view_drawing.jpg "fig:SBD-5_BuAer_3_view_drawing.jpg")

## 使用國家

  - [Flag_of_the_United_States.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_States.svg "fig:Flag_of_the_United_States.svg")－[美國海軍](../Page/美國海軍.md "wikilink")，[美國陸軍](../Page/美國陸軍.md "wikilink")
  - [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg")－[法國陸軍](../Page/法國陸軍.md "wikilink")，法國海軍
  - [Flag_of_Free_France_(1940-1944).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Free_France_\(1940-1944\).svg "fig:Flag_of_Free_France_(1940-1944).svg")－自由法國陸軍
  - [Flag_of_the_United_Kingdom.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_Kingdom.svg "fig:Flag_of_the_United_Kingdom.svg")－[英國皇家海軍](../Page/英國皇家海軍.md "wikilink")，[英國皇家空軍](../Page/英國皇家空軍.md "wikilink")
  - [Flag_of_Mexico.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Mexico.svg "fig:Flag_of_Mexico.svg")－墨西哥空軍
  - [Flag_of_New_Zealand.svg](https://zh.wikipedia.org/wiki/File:Flag_of_New_Zealand.svg "fig:Flag_of_New_Zealand.svg")－紐西蘭皇家空軍
  - [Flag_of_Chile.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Chile.svg "fig:Flag_of_Chile.svg")－智利空軍
  - [Flag_of_Morocco.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Morocco.svg "fig:Flag_of_Morocco.svg")－摩洛哥警備隊

## 相關條目

  - [俯衝轟炸機](../Page/俯衝轟炸機.md "wikilink")
  - [第二次世界大戰](../Page/第二次世界大戰.md "wikilink")
  - [太平洋戰爭](../Page/太平洋戰爭.md "wikilink")
  - [中途島戰役](../Page/中途島戰役.md "wikilink")
  - [珊瑚海戰役](../Page/珊瑚海戰役.md "wikilink")

## 参考资料

[Category:道格拉斯](../Category/道格拉斯.md "wikilink")
[Category:美國轟炸機](../Category/美國轟炸機.md "wikilink")
[Category:艦載機](../Category/艦載機.md "wikilink")
[Category:二戰俯衝轟炸機](../Category/二戰俯衝轟炸機.md "wikilink")