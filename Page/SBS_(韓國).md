**SBS**（），前稱**首爾放送**（，；於2000年3月改為目前的名稱），是[韓國四大全國無線電視及電台網絡中仅有的私營業者](../Page/韓國.md "wikilink")。SBS的經營口號是「共同制造欢乐」（，[英语](../Page/英语.md "wikilink")：Together,we
make Delight），這個口號從2015年起使用到現在。

## 沿革

  - 1990年11月14日：泰榮地產建築公司和31家公司共同成立「株式會社漢城放送」，[尹世荣代表理事](../Page/尹世荣.md "wikilink")。
  - 1991年3月20日：SBS廣播電台開播，頻率為792 kHz（呼號：HLSQ，後改為「SBS Love FM」）。
  - 1991年12月9日：無線電視（[SBS
    TV](../Page/SBS_TV.md "wikilink")）於上午10:00开播，在[首尔及其周边地区播出](../Page/首尔.md "wikilink")。
  - 1992年5月：SBS制作公司得以成立。
  - 1992年12月：SBS一山演播室竣工。
  - 1994年3月：[尹世荣会长](../Page/尹世荣.md "wikilink")、[尹赫基社长得以就任](../Page/尹赫基.md "wikilink")
  - 1994年8月8日：台徽变更为第二代台标（全大写SBS）。
  - 1994年10月24日，星期六、星期日的《[SBS
    8点新闻](../Page/SBS_8点新闻.md "wikilink")》更名為《SBS
    新聞 2000》（SBS 뉴스 2000）。
  - 1995年5月14日，SBS結合其它地方電視台，成立電視聯播網（播放範圍不含[江原道](../Page/江原道_\(韓國\).md "wikilink")、[忠清北道](../Page/忠清北道.md "wikilink")、[濟州](../Page/濟州特別自治道.md "wikilink")）。
  - 1996年10月26日，《SBS 新聞 2000》更名為《News Q》。
  - 1996年10月30日：開始試播。
  - 1996年11月14日：SBS Power FM開播，頻率為107.7 MHz。
  - 1998年8月：[韩国首次进行](../Page/韩国.md "wikilink")[朝鲜正式采访](../Page/朝鲜.md "wikilink")。
  - 1998年11月：制定新的司训（以人为本、文化创造、走向未来）
  - 1998年12月：SBS ArtTech, SBS NewsTech得以成立
  - 1999年1月：[SBS LOVE FM](../Page/SBS_LOVE_FM.md "wikilink")（103.5
    MHz）开播
  - 1999年5月：SBS在[KOSDAQ上市](../Page/科斯达克.md "wikilink")
  - 1999年6月:[SBS 高爾夫頻道](../Page/SBS_高爾夫頻道.md "wikilink")([SBS
    Golf](../Page/SBS_Golf.md "wikilink"))正式開播
  - 1999年8月：SBS互联网（SBSi）成立
  - 2000年3月：公司名称由汉城广播更名為SBS株式會社
  - 2000年10月：韩国首次成功在[平壤直播新闻报道](../Page/平壤.md "wikilink")
  - 2000年11月14日：SBS慶祝10週年，並更改台徽（即為現在使用的第三代台徽）
  - 2001年4月6日：SBS首次在[韓國進行](../Page/大韩民国.md "wikilink")-{zh-cn:高清晰度电视;zh-hk:高清電視;zh-tw:高畫質電視;zh-hans:高清晰度电视;zh-hant:高解析度電視;zh-mo:高清電視;}-直播。
  - 2001年10月26日：無線數位電視（HLSQ-DTV）開播，採用-{zh-cn:高清晰度电视;zh-hk:高清電視;zh-tw:高畫質電視;zh-hans:高清晰度电视;zh-hant:高解析度電視;zh-mo:高清電視;}-廣播，是韓國第一個-{zh-cn:高清晰度电视;zh-hk:高清電視;zh-tw:高畫質電視;zh-hans:高清晰度电视;zh-hant:高解析度電視;zh-mo:高清電視;}-頻道，也是亞洲第一個無線數位電視頻道\[1\]\[2\]，但當時僅一部分節目以-{zh-cn:高清晰度;zh-hk:高清;zh-tw:高畫質;zh-hans:高清晰度;zh-hant:高解析度;zh-mo:高清;}-形式播放。
  - 2002年2月21日：24小時戲劇和娛樂節目頻道開播。
  - 2002年5月31日：[濟州國際自由都市廣播開播](../Page/濟州國際自由都市廣播.md "wikilink")，至此SBS完成全國電視聯播網。
  - 2003年10月10日：G1 Fresh FM開播（春川 105.1㎒，江陵 106.1㎒），至此完成SBS Power
    FM全國聯播網。
  - 2004年5月：第1次召开[首尔数码论坛](../Page/首尔数码论坛.md "wikilink")
  - 2005年8月：迎来解放60周年之际，进行了[SBS
    Special](../Page/SBS_Special.md "wikilink")《[趙容弼](../Page/赵容弼.md "wikilink")[平壤](../Page/平壤.md "wikilink")2005》表演
  - 2005年12月：地面波DMB“SBSⓤ”得以成立
  - 2006年6月：获得2010\~2016年度[奥运会转播权合同及南北韩同时转播权](../Page/奥林匹克运动会.md "wikilink")、韩国首次实施数字数据广播模式
  - 2006年8月：获得了2010年、2014年的[世界杯转播权](../Page/國際足協世界盃.md "wikilink")
  - 2007年7月：韩国首次实施了[数字电视](../Page/数字电视.md "wikilink")(DTV)带有字幕的正式广播
  - 2008年3月：[SBS媒体控股得以成立](../Page/SBS媒体控股.md "wikilink")
  - 2010年1月1日：[SBS-CNBC开播](../Page/SBS-CNBC.md "wikilink")
  - 2010年11月14日：SBS庆祝成立20周年
  - 2011年7月：获得了2016\~2024年度[奥运会的转播权](../Page/奥林匹克运动会.md "wikilink")
  - 2011年8月20日：SBS所有節目皆改為-{zh-cn:高清晰度;zh-hk:高清;zh-tw:高畫質;zh-hans:高清晰度;zh-hant:高解析度;zh-mo:高清;}-播放\[3\]。
  - 2011年9月：SBS成為維亞康姆的韓國官方合作夥伴，並在同年11月將MTV Korea更名為「SBS MTV」。
  - 2012年7月：开放了地面波联盟N屏幕服务平台“pooq”；SBS Prism Tower竣工。
  - 2012年11月：开放式电台播音室“The Studio樂”得以成立。
  - 2013年1月：世界首次成功水下LTE直播
  - 2014年5月：将SBS NewsTech和SBS ArtTech合并为SBS A\&T
  - 2014年6月：设立子公司Smart Media Representative
    (SMR)；世界首次成功[超高清直播世界杯](../Page/超高畫質電視.md "wikilink")
  - 2014年9月：与[美国广播公司签署了](../Page/美国广播公司.md "wikilink")《[来自星星的你](../Page/来自星星的你.md "wikilink")》改编合同
  - 2015年11月14日：SBS宣布创社25周年标语 – “共同制造欢乐”（）
  - 2016年1月：成功完成全世界最早的[UHDTV](../Page/UHDTV.md "wikilink") ATSC3.0播送系统试验
  - 2016年3月：[尹世荣会长](../Page/尹世荣.md "wikilink")（SBS媒体控股理事会会长）和[尹硕敏副会长](../Page/尹硕敏.md "wikilink")（SBS理事会会长）就任
  - 2016年10月10日：因[SBS
    TV直播](../Page/SBS_TV.md "wikilink")《2016年[韓國職棒聯賽季後賽](../Page/韓國職棒聯賽.md "wikilink")》決賽，《SBS
    8點新聞》順延至22:00播出。
  - 2016年12月9日：因[韓国總統](../Page/大韩民国总统.md "wikilink")[朴槿惠遭](../Page/朴槿惠.md "wikilink")[彈劾停職](../Page/彈劾.md "wikilink")，《SBS
    8點新聞》提前至19:28播出，並延長播出2個半小時。
  - 2017年3月12日：《SBS
    8點新聞》提前至18:57播出（原於19:57播出的新聞提要直接跳過不播），聯播網分台的自製新聞順延至20:50播出（[G1為](../Page/G1有限公司.md "wikilink")21:00）。
  - 2017年5月31日：SBS提供UHD電視訊號。

## 頻道

  - 無線電視
      - [SBS TV](../Page/SBS_TV.md "wikilink")（6頻道，高清频道，識別訊號：HLSQ-DTV）

<!-- end list -->

  - 電臺
      - (首爾：FM 107.7㎒，[東豆川市](../Page/東豆川市.md "wikilink")：FM
        100.3㎒)：播放[韓國流行音樂的電台](../Page/韓國流行音樂.md "wikilink")

      - (FM 103.5㎒、792
        ㎑)：播放新聞、韓國流行音樂、[韓國演歌的電台](../Page/韓國演歌.md "wikilink")

      - （[DTMB](../Page/DTMB.md "wikilink") CH 12C）

<!-- end list -->

  - 有線電視／衛星電視
      - ：24小時播放戲劇和娛樂節目的頻道

      - SBS FunE (2014年1月1日前稱**E\!TV**)

      - SBS FOX Sports (2014年1月1日前稱**SBS ESPN**)

      - [SBS GOLF](../Page/SBS_GOLF.md "wikilink")

      - [SBS-CNBC](../Page/SBS-CNBC.md "wikilink")

      - SBS MTV

      - Nickelodeon Korea
  - [ONE TV
    ASIA](../Page/ONE_TV_ASIA.md "wikilink")(由索尼電視娛樂代理,僅在新加坡,馬來西亞和印尼播放)

## 聯播網成員

  - [KNN](../Page/KNN.md "wikilink")（Korea New
    Network）：原[釜山放送](../Page/KNN.md "wikilink")（PSB）。
  - [大邱放送](../Page/大邱放送.md "wikilink")（TBC）
  - [光州放送](../Page/光州放送.md "wikilink")（KBC）
  - [G1](../Page/G1有限公司.md "wikilink")（Gangwon No.1
    Broadcasting）：原名**江原民放**（GTB）。
  - [大田放送](../Page/大田放送.md "wikilink")（TJB）
  - [清州放送](../Page/清州放送.md "wikilink")（CJB）
  - [全州放送](../Page/全州放送.md "wikilink")（JTV）
  - [蔚山放送](../Page/蔚山放送.md "wikilink")（UBC）
  - [济州国际自由都市放送](../Page/济州国际自由都市放送.md "wikilink")（JIBS）

## 標誌

<File:SBS> CI 1991 - 1994.png|SBS第一代台徽 (1990年11月14日\~1994年8月7日)
<File:SBS> 1994 - 2000.png|SBS第二代台徽 (1994年8月8日\~2000年11月13日)
<File:Seoul> Broadcasting System logo.svg|SBS現時台徽 (2000年11月14日至今)

## 節目

### 2018年世界巡迴《[Super Concert](../Page/Super_Concert.md "wikilink")》

  - SBS Super Concert in Taipei（時間：7月7日）

### 綜藝節目

#### 播出中

  - [同床异梦2 - 你是我的命运](../Page/同床异梦2_-_你是我的命运.md "wikilink")

  - [深夜正式演艺](../Page/深夜TV演藝.md "wikilink")

  -
  - [Style Follow](../Page/Style_Follow.md "wikilink")

  - [亲爱的百年客人](../Page/亲爱的百年客人.md "wikilink")

  - [叢林的法則](../Page/叢林的法則.md "wikilink")

  - [白种元的胡同餐馆](../Page/白种元的胡同餐馆.md "wikilink")

  - [Game show遊戲樂樂](../Page/Game_show遊戲樂樂.md "wikilink")

  - [Master Key](../Page/Master_Key.md "wikilink")

  - [Running Man](../Page/Running_Man.md "wikilink")

  - [家師父一體](../Page/家師父一體.md "wikilink")

  - [我家的熊孩子](../Page/我家的熊孩子.md "wikilink")

#### 已結束

  - [Fantastic Duo 2](../Page/Fantastic_Duo_2.md "wikilink")

### 音樂節目

  - [人气歌谣](../Page/人气歌谣.md "wikilink")
  - [THE SHOW](../Page/THE_SHOW_\(SBS_MTV節目\).md "wikilink")（）

### 電視劇

#### 尚在播映

  - [SBS晨間連續劇](../Page/SBS晨間連續劇.md "wikilink")
  - [SBS月火連續劇](../Page/SBS月火連續劇.md "wikilink")
  - [SBS特別劇](../Page/SBS特別劇.md "wikilink")
  - [SBS週末連續劇](../Page/SBS週末連續劇.md "wikilink")

#### 過去時段

  - [SBS日日連續劇](../Page/SBS日日連續劇.md "wikilink")
  - [SBS金曜連續劇](../Page/SBS金曜連續劇.md "wikilink")
  - [SBS週末劇場](../Page/SBS週末劇場.md "wikilink")
  - [SBS週末特別計劃劇](../Page/SBS週末特別計劃劇.md "wikilink")

#### 不特定時段

  - [SBS特輯電視劇](../Page/SBS特輯電視劇.md "wikilink")

## 收播時間

現僅[SBS TV](../Page/SBS_TV.md "wikilink")（無線電視頻道）有收播時段，若當日直播運動賽事則不收播

  - 週一：02:16\~04:56
  - 週二至週五：03:00\~05:00

## 股東

  - [泰榮地產建築](http://www.taeyoung.com/english/index.asp) 30.0%
  - [瑰都啦咪鍋爐](http://www.krb.co.kr/chinese/main/main.html) 13.2%
  - [大韓投資信託運用](https://web.archive.org/web/20080103023248/http://www.dimco.co.kr/)
    9.5%
      - （[韓亞金控](http://www.hanafn.com/)
        49%、[瑞銀](../Page/UBS.md "wikilink")51%）
  - 政府公股（韓國國民退休金管理公團）6.5%
  - [大韓製粉](https://web.archive.org/web/20080118130202/http://www.dhflour.co.kr/eng/company.htm)
    5.6%
  - Lucy娱乐集团 4%

## 主播

  - （[SBS 8點新聞](../Page/SBS_8點新聞.md "wikilink")（平日）主播，2016年12月19日起）

  - （[SBS 8點新聞](../Page/SBS_8點新聞.md "wikilink")（平日）主播，2016年12月19日起）

  - 申東旭（，曾任《SBS8點新聞》平日主播）

  -
  - [朴善英](../Page/朴善英_\(新聞主播\).md "wikilink")

  -
## 其他友好電視台(姊妹台)

  - ：[中國中央电视台](../Page/中國中央电视台.md "wikilink")（CCTV）、[北京广播電視台](../Page/北京广播電視台.md "wikilink")（BTV）、[上海广播电视台](../Page/上海广播电视台.md "wikilink")（SMG）、[浙江广播电视集团](../Page/浙江广播电视集团.md "wikilink")（ZRTG）、[安徽广播电视台](../Page/安徽广播电视台.md "wikilink")（AHTV）

  - ：[電視廣播有限公司](../Page/電視廣播有限公司.md "wikilink")（TVB）

  - ：[中華電視公司](../Page/中華電視公司.md "wikilink")（CTS）、[八大電視](../Page/八大電視.md "wikilink")（GTV）

  - ：[日本電視台](../Page/日本電視台.md "wikilink")（NTV）

  - ：[新傳媒私人有限公司](../Page/新傳媒私人有限公司.md "wikilink")（MediaCorp）、[ONE TV
    ASIA](../Page/ONE_TV_ASIA.md "wikilink")

  - ：（Hanoi TV）

  - ：[法國電視一台](../Page/法國電視一台.md "wikilink")（TF1）

  - ：[首要媒体集团](../Page/首要媒体集团.md "wikilink")（Media Prima Berhad）、[ONE TV
    ASIA](../Page/ONE_TV_ASIA.md "wikilink")

  - ：（Televicentro）

  - ：（Megavisión）

  - ：[全俄国家电视广播公司](../Page/全俄国家电视广播公司.md "wikilink")（RTR）

  - ：[美國廣播公司](../Page/美國廣播公司.md "wikilink")（ABC）、[全國廣播公司](../Page/全國廣播公司.md "wikilink")（NBC）

  - ：[卡拉科爾電視台](../Page/卡拉科爾電視台.md "wikilink")（Caracol Televisión）

  - ：[環球電視](../Page/環球電視.md "wikilink")（Global Television Network）

## 歷年標語口號

《[SBS8點新聞](../Page/SBS8點新聞.md "wikilink")》曾在開場和結尾時，新聞棚的電視牆會出現SBS的標語口號，2016年12月19日起因《SBS8點新聞》改版，新聞棚電視牆改為不出現標語。

|              |                                                    |
| ------------ | -------------------------------------------------- |
| **年份**       | **口號**                                             |
| 2010年\~2015年 | 내일을 봅니다（，看到美好的明天）                                  |
| 2015年\~      | 함께 만드는 기쁨（英语：Together,we make Delight，共同制造欢乐\[4\]） |

## 爭議事件

### 2008年[北京奥运开幕式彩排泄密事件](../Page/北京奥运.md "wikilink")

2008年7月底，SBS涉嫌於[北京奧運開幕式綵排時拍攝及轉播相關片段](../Page/2008年夏季奧林匹克運動會開幕式.md "wikilink")\[5\]，被[國際奧林匹克委員會與](../Page/國際奧林匹克委員會.md "wikilink")[北京奧組委嚴厲抗議與指責](../Page/北京奧組委.md "wikilink")。奧運會雖允許轉播商攜帶器材進行測試，但奧運會的慣例和其與轉播商間的約束向來是不得提前洩露開幕式內容。

在7月30日晚上的彩排开始前，一段来自SBS的包括有文艺表演、代表团入场、点火台等内容的开幕式视频在网络流传。

网友对SBS的行为表示谴责，并认为SBS获得开幕式彩排片段的手法需要有关方面继续关注，SBS应该掌握了大量有关开幕式的素材信息，此次披露的内容只是其中一部分。SBS是分批次披露还是就此止步，尚待观察。另有网友指出，SBS的行为有违反新闻业界行规之嫌。\[6\]

韩国记者对SBS的行为表示“无奈和痛心”，据称SBS在[雅典奥运上就曾因违规行为被国际奥委会警告](../Page/雅典奥运.md "wikilink")\[7\]。8月1日，北京奥组委新闻发言人孙伟德表示，北京奥组委已经就SBS未经授权播放北京奥运会开幕式部分片段的做法提出严正交涉，对SBS的做法表示强烈不满，要求SBS从中吸取教训、严格遵守奥运报道有关惯例，不要做有悖於新闻职业道德的事情。\[8\]

SBS在8月1日称，当天已派专人赴[北京](../Page/北京.md "wikilink")，就曝光北京奥运会开幕式彩排事件向[国际奥委会和](../Page/国际奥委会.md "wikilink")[北京奥组委的相关人士进行解释](../Page/第29届奥林匹克运动会组织委员会.md "wikilink")。SBS宣传组组长[朴载晚接受](../Page/朴载晚.md "wikilink")[新华社记者采访时表示](../Page/新华社.md "wikilink")，SBS体育部部长与国际部副部长已于当天出发前往北京，希望能当面向国际奥委会和北京奥组委的相关人员进行解释说明；如果国际奥委会和北京奥组委要求，他们将代表SBS表示道歉。\[9\]

### 2009年綜藝節目抄襲事件

2009年7月22日，由於SBS星期六綜藝節目《驚人的大會紅星》在本月18日播出的單元〈3分鐘上班法〉被[日本](../Page/日本.md "wikilink")[網友揭發](../Page/網友.md "wikilink")[抄襲](../Page/抄襲.md "wikilink")[TBS電視生活情報綜藝節目](../Page/TBS電視.md "wikilink")《時短生活情報秀》（，台湾[纬来日本台翻译为](../Page/纬来日本台.md "wikilink")《[超省时生活](../Page/超省时生活.md "wikilink")》）在2009年3月播出的單元〈5分鐘上班法〉（），SBS公開道歉。\[10\]

### 2010年亞運跆拳道爭議事件中 楊淑君對韓道歉報導

SBS在2010年11月27日的新聞中報導，特派記者鄭由美到台灣採訪[2010年亞洲運動會跆拳道爭議事件當事人](../Page/2010年亞洲運動會跆拳道爭議事件.md "wikilink")[楊淑君](../Page/楊淑君.md "wikilink")：這名[中華台北](../Page/中華台北.md "wikilink")[跆拳道代表隊女將對於因她而引發的台灣](../Page/跆拳道.md "wikilink")[反韓情緒](../Page/反韓情緒.md "wikilink")，向韓國民眾表達了歉意。報導並配上楊淑君在[廣州亞運會與](../Page/廣州亞運會.md "wikilink")[越南選手比賽的新聞畫面](../Page/越南.md "wikilink")，指出比賽快結束之前，楊淑君因腳後跟貼有不合規定的感應片，被裁定失格而淚灑賽場；但她已成為台灣的「超級明星」，所到之處湧來記者和粉絲，不僅是廣告、甚至演藝圈的邀約也不斷。\[11\]

楊淑君：「我是針對蛋襲國民小學跟燒人家國旗這部分，跟他們說一聲道歉。他們應該是[斷章取義最後一段](../Page/斷章取義.md "wikilink")。韓媒有先講說那邊有因為我的事情而造成一陣話題、一陣的騷動。那也許他們也是想說，利用這種方式，來平復他們國民平民的心。」

## 参考文献

## 外部連結

  - [SBS官方网页](http://www.sbs.co.kr/)

  -
  -
  - [Korea New Network](http://www.knn.co.kr/)

  - [大田放送](http://www.tjb.co.kr/)

  - [大邱放送](http://www.tbc.co.kr/)

  - [光州放送](http://www.ikbc.co.kr/)

  - [蔚山放送](https://web.archive.org/web/20130511002914/http://ubc.co.kr/)

  - [全州放送](http://www.jtv.co.kr)

  - [清州放送](https://web.archive.org/web/20120728144405/http://www.cjb.co.kr/)

  - [江原民放](https://web.archive.org/web/20090213145103/http://igtb.co.kr/)

  - [濟州放送](https://web.archive.org/web/20120910181856/http://www.jibstv.com/)

## 参见

  - [媒體列表](../Page/媒體列表.md "wikilink")
  - [韩流](../Page/韩流.md "wikilink")
  - [韩国电视剧](../Page/韩国电视剧.md "wikilink")

{{-}}

[Category:韩国电视台](../Category/韩国电视台.md "wikilink")
[Category:韓國廣播電台](../Category/韓國廣播電台.md "wikilink")
[SBS](../Category/SBS.md "wikilink")
[Category:1990年成立的公司](../Category/1990年成立的公司.md "wikilink")

1.  [SBS 10월 26일에 HDTV 본방송
    개시](http://dvdprime.donga.com/g5/bbs/board.php?bo_table=display&wr_id=497)
2.  [韓國DTT HDTV政策](http://web.pts.org.tw/~rnd/p9/2009/01/K9.pdf)
3.  [SBS,20일부터 HD방송으로 전면
    전환](http://sports.khan.co.kr/news/sk_index.html?art_id=201108181709543&sec_id=540201)
4.
5.  [“泄密”、撞车、降雨－开幕式彩排遭遇突发事件](http://chinanews.com.cn/olympic/news/2008/07-30/1330213.shtml)
6.  [韩媒曝北京奥运开幕式细节
    中外网友谴责其泄密行为](http://sports.sina.com.cn/o/2008-07-31/01023820848.shtml)
7.  [韩媒曝光京奥运开幕彩排
    韩记者：无奈和痛心](http://hk.crntt.com/doc/1007/0/8/7/100708700.html)
8.  [北京奥组委提出严正交涉
    SBS专人转交总裁道歉信](http://politics.people.com.cn/GB/1026/7598104.html)
9.  [奥组委严正交涉　SBS专人转交总裁道歉信](http://cn.chinareviewnews.com/doc/1007/0/9/3/100709359.html?coluid=105&kindid=3294&docid=100709359)
10. 《[中央日報
    (韓國)](../Page/中央日報_\(韓國\).md "wikilink")》：[〈SBS週末バラエティー番組「スターキング」　日本の映像を盗用〉](http://japanese.joins.com/article/article.php?aid=118248&servcode=700&sectcode=700)，2009年7月22日
11.