**SC**是一对[拉丁字母](../Page/拉丁字母.md "wikilink")，一般用于缩写。

**SC**、**sc**或**Sc**可能指：

## 地理

  - [南卡罗来纳州](../Page/南卡罗来纳州.md "wikilink")，[美国州份邮政缩写列表](../Page/美国州份邮政缩写列表.md "wikilink")
  - [圣基茨和尼维斯](../Page/圣基茨和尼维斯.md "wikilink")，[北约缩写](../Page/北约.md "wikilink")
  - [塞舌尔](../Page/塞舌尔.md "wikilink")，[ISO
    3166-1缩写](../Page/ISO_3166-1.md "wikilink")
  - 巴西的[圣卡塔琳娜州](../Page/圣卡塔琳娜州.md "wikilink")，[ISO
    3166-2](../Page/ISO_3166-2.md "wikilink")

## 电子游戏

  - [星際爭霸](../Page/星際爭霸.md "wikilink") (**S**tar**C**raft)
  - [模拟城市](../Page/模拟城市.md "wikilink") (**S**im**C**ity)
  - [剑魂](../Page/剑魂.md "wikilink") (**S**oul **C**alibur)
  - [分裂細胞](../Page/分裂細胞.md "wikilink")(**S**plinter **C**ell)
  - [最高指挥官](../Page/最高指挥官.md "wikilink")(**S**upreme **C**ommander)
  - [英雄傳說VI「空之軌跡SC」](../Page/英雄傳說VI「空之軌跡」.md "wikilink")（The Legend of
    Heroes VI "SORA NO KISEKI" **S**econd **C**hapter）
  - [方格連線](../Page/方格連線.md "wikilink")(**S**quare.**C**onnect)
  - [Sven Co-op](../Page/Sven_Co-op.md "wikilink")(**S**ven **C**o-op)

## 職稱

  - [資深大律師](../Page/資深大律師.md "wikilink")（**S**enior **C**ounsel）

## 其它

  - [**S**nap**c**hat](../Page/'''S'''nap'''c'''hat.md "wikilink")
  - [钪](../Page/钪.md "wikilink")（Sc），[化学符号](../Page/化学符号.md "wikilink")
  - [山东航空公司](../Page/山东航空公司.md "wikilink")，[IATA航空公司代码](../Page/IATA航空公司代码.md "wikilink")
  - [萨丁尼亚语](../Page/萨丁尼亚语.md "wikilink")，[ISO
    639](../Page/ISO_639.md "wikilink")
  - [凌志SC](../Page/凌志SC.md "wikilink")，轎跑車
  - [守護甜心\!](../Page/守護甜心!.md "wikilink")，英文簡稱（**S**hugo **C**hara\!）
  - [科學計數機](../Page/計算器#進階電子計算器.md "wikilink")：如[信利科學計數機](../Page/信利.md "wikilink")**SC**-185
  - [賽車比賽中途出動的](../Page/賽車.md "wikilink")[安全車](../Page/安全車.md "wikilink")（**S**afety
    **C**ar）
  - [鋼結構](../Page/鋼結構.md "wikilink")（**S**teel **C**onstruction）
  - 一種[光纖接頭](../Page/光纖接頭.md "wikilink")
  - [渣打銀行](../Page/渣打銀行.md "wikilink")（**S**tandard **C**hartered）
  - [上海商業銀行](../Page/上海商業銀行.md "wikilink")（**S**hanghai **C**ommercial）
  - [空中英語教室](../Page/空中英語教室.md "wikilink")（**S**tudio
    **C**lassroom），英語教學雜誌
  - [中學畢業證書](../Page/中學畢業證書.md "wikilink")（**S**chool
    **C**ertificate），英國中學教育資歷，1951年被[GCE](../Page/GCE.md "wikilink")
    O-Level取代
  - [香港中文大學](../Page/香港中文大學.md "wikilink")[逸夫書院](../Page/逸夫書院.md "wikilink")（**S**haw
    **C**ollege）
  - sc是流动电流的意思，英文全称 streaming current。
  - sc在国内外通常也作为水处理行业产品型号或型号一部分来使用
  - 海大商船学院的简称。
  - 皮下注射(subcutaneous)
  - 四川 Sichuan