**SD戰國傳**是[SD
GUNDAM系列的衍生故事](../Page/SD_GUNDAM系列.md "wikilink")。原指披上日本鎧甲風的SD
GUNDAM，發展至今，衍生出現代風格的○傳、三國武將風格的[三國傳等](../Page/三國傳.md "wikilink")，但一概歸納為同一系列。自1988年誕生後，一直受廣大FANS支持，《SD戰國傳》已成為BB戰士的主力產品，還衍生各類週邊[漫畫](../Page/漫畫.md "wikilink")、[OVA](../Page/OVA.md "wikilink")、[劇場版](../Page/劇場版.md "wikilink")、-{zh-hk:手辦;zh-tw:GK;}-[模型等](../Page/模型.md "wikilink")。截至2009年10月，正在推出的系列分別為[BB戰士三國傳
戰神決鬥編](../Page/BB戰士三國傳_戰神決鬥編.md "wikilink")，[BB戰士 三國傳外傳
武勇激鬥錄以及](../Page/BB戰士_三國傳外傳_武勇激鬥錄.md "wikilink")[SD戰國傳
武神降臨篇](../Page/SD戰國傳_武神降臨篇.md "wikilink")。

## 誕生

1988年，[BANDAI把漫畫作品](../Page/BANDAI.md "wikilink")《[模型狂四郎](../Page/模型狂四郎.md "wikilink")》中登場的武者GUNDAM（Musha
-{Gundam}-）SD化，並推出塑料模型，被視為SD戰國傳的開端。武者GUNDAM原為BB戰士的一部分，所以模型漫畫悖跟其他BB戰士一樣，採一集完的方式；但因SD武者造型大受歡迎，BANDAI
開始了以SD 武者武士造型BB戰士的長篇模型系列，並在推出精太頑駄無時，正式把系列命名為《SD戰國傳》，開展了SD戰國傳長達20年的模型系列。

## 系列

  - SD戰國傳

<!-- end list -->

  - [SD戰國傳 武者七人眾篇](../Page/SD戰國傳_武者七人眾篇.md "wikilink")
  - [SD戰國傳 風林火山篇](../Page/SD戰國傳_風林火山篇.md "wikilink")
  - [SD戰國傳 天下統一篇](../Page/SD戰國傳_天下統一篇.md "wikilink")

<!-- end list -->

  - 新SD戰國傳

<!-- end list -->

  - [新SD戰國傳 地上最強篇](../Page/新SD戰國傳_地上最強篇.md "wikilink")
  - [新SD戰國傳 傳說之大將軍篇](../Page/新SD戰國傳_傳說之大將軍篇.md "wikilink")
  - [新SD戰國傳 七人之超將軍篇](../Page/新SD戰國傳_七人之超將軍篇.md "wikilink")
  - [新SD戰國傳 超機動大將軍篇](../Page/新SD戰國傳_超機動大將軍篇.md "wikilink")

<!-- end list -->

  - 超SD戰國傳

<!-- end list -->

  - [超SD戰國傳 武神輝羅鋼篇](../Page/超SD戰國傳_武神輝羅鋼篇.md "wikilink")
  - [超SD戰國傳 刕霸大將軍篇](../Page/超SD戰國傳_刀霸大將軍篇.md "wikilink")
  - [新SD戰國傳 天星七人眾篇](../Page/新SD戰國傳_天星七人眾篇.md "wikilink")

<!-- end list -->

  - [武者戰記 光之變幻篇](../Page/武者戰記_光之變幻篇.md "wikilink")

<!-- end list -->

  - [SD高達 武者G世紀](../Page/SD高達_武者G世紀.md "wikilink")

<!-- end list -->

  - SD頑駄無

<!-- end list -->

  - [SD頑駄無 武者O傳](../Page/SD頑駄無_武者O傳.md "wikilink")
  - [SD頑駄無 武者02傳](../Page/SD頑駄無_武者02傳.md "wikilink")
  - [SD頑駄無 武者03傳](../Page/SD頑駄無_武者03傳.md "wikilink")

<!-- end list -->

  - SDGUNDAMFORCE

<!-- end list -->

  - [武者烈傳·零](../Page/武者烈傳·零.md "wikilink")
  - [武者烈傳 舞化武可篇](../Page/武者烈傳_舞化武可篇.md "wikilink")
  - [武者番長風雲錄](../Page/武者番長風雲錄.md "wikilink")

<!-- end list -->

  - BB戰士

<!-- end list -->

  - [SD高達聖獸戰記 獸魂爭霸戰](../Page/SD高達聖獸戰記_獸魂爭霸戰.md "wikilink")
  - [SD戰國傳 武神降臨編](../Page/SD戰國傳_武神降臨編.md "wikilink")

:;BB戰士三國傳

::\*[BB戰士三國傳 風雲豪傑編](../Page/BB戰士三國傳_風雲豪傑編.md "wikilink")

::\*[BB戰士三國傳 英雄激突編](../Page/BB戰士三國傳_英雄激突編.md "wikilink")

::\*[BB戰士三國傳 戰神決鬥編](../Page/BB戰士三國傳_戰神決鬥編.md "wikilink")

::\*[BB戰士三國傳外傳 武勇激鬥錄](../Page/BB戰士三國傳外傳_武勇激鬥錄.md "wikilink")

## 製作人員

  - [橫井孝二](../Page/橫井孝二.md "wikilink")
  - [今石進](../Page/今石進.md "wikilink")
  - [寺島慎也](../Page/寺島慎也.md "wikilink")
  - [濱田一紀](../Page/濱田一紀.md "wikilink")
  - [神田正宏](../Page/神田正宏.md "wikilink")
  - [一式正人](../Page/一式正人.md "wikilink")
  - [時田洸一](../Page/時田洸一.md "wikilink")
  - [矢野健太郎](../Page/矢野健太郎.md "wikilink")

## 週邊作品

### 漫畫

  - 《SD武者GUNDAM風雲錄》（全套9卷），[大和虹一著](../Page/大和虹一.md "wikilink")（日：[講談社](../Page/講談社.md "wikilink")；台：[青文出版社](../Page/青文出版社.md "wikilink")；港：[正文社](../Page/正文社.md "wikilink")）
  - 《新SD戰國傳·七人之超將軍篇》（全套2卷），[神田正宏著](../Page/神田正宏.md "wikilink")（日：講談社；台：青文出版社；港：正文社）
  - 《新SD戰國傳·超機動大將軍篇》（全套3卷），神田正宏著（日：講談社；台：青文出版社；港：正文社）
  - 《新SD戰國傳·武神輝羅鋼篇》（全套2卷），神田正宏著（日：講談社；台：青文出版社；港：正文社）
  - 《新SD戰國傳·刀霸大將軍篇》（全套2卷），神田正宏著（日：講談社；台：青文出版社；港：正文社）
  - 《新SD戰國傳·天星七人眾篇》（全套2卷），神田正宏著（日：講談社；台：青文出版社；港：正文社）
  - 《新SD戰國傳·光之變幻篇篇》（全套2卷），神田正宏著（日：講談社；台：青文出版社；港：正文社）
  - 《SD頑駄無
    武者○伝》（全套2卷），[一式正人著](../Page/一式正人.md "wikilink")（日：講談社；台：青文出版社；港：正文社）
  - 《SD頑駄無 武者○伝２》（全套2卷），一式正人著（日：講談社；台：青文出版社；港：正文社）
  - 《SD頑駄無 武者○伝３》（全套3卷），一式正人著（日：講談社；台：青文出版社；港：正文社）
  - 《SD高達軍團畫卷 武者烈傳 舞化武可篇》，（全套3卷），一式正人著（日：講談社；台：青文出版社；港：正文社）
  - 《武者番長風雲錄》，（全套4卷）一式正人著（日：講談社；港：正文社）
  - 《BB戰士三國傳·風雲豪傑篇》（全套2卷），[鴇田洸一著](../Page/鴇田洸一.md "wikilink")（日：講談社；台：青文出版社；港：正文社）
  - 《BB戰士三國傳·英雄激突編
    》（全套3卷），[矢野健太郎著](../Page/矢野健太郎.md "wikilink")（日：角川；台：台灣角川）

### [動畫](../Page/動畫.md "wikilink")

  - 《SD戰國傳　暴終空城之章》
  - 《SD戰國傳・頭虫邸的忍者合戦》
  - 《SD戰國傳・天之巻》
  - 《SD戰國傳・地之巻》
  - 《SD戰國傳・真之巻》
  - 《SD戰國傳・頑駄無五人衆的妖怪退治》
  - 《SD戰國傳　天下泰平編》

### [遊戲](../Page/遊戲.md "wikilink")

  - 《SD戰國傳 國盜物語》（[GameBoy](../Page/GameBoy.md "wikilink")）
  - 《SD戰國傳2 天下統一編》（[GameBoy](../Page/GameBoy.md "wikilink")）
  - 《SD戰國傳3 地上最強編》（[GameBoy](../Page/GameBoy.md "wikilink")）
  - 《新SD戰國傳 大將軍列傳》（[超級任天堂](../Page/超級任天堂.md "wikilink")）
  - 《新SD戰國傳 機動武者大戰》（[PlayStation](../Page/PlayStation.md "wikilink")）

## 設定資料書籍

1.  Hobby Japan編集部。《SDGUNDAM目錄 SD世界編》。日本：Hobby Japan，2006年。
2.  COMIC BOMBOM編集部。《SDGUNDAM編年史 SD戰國傳武者英雄譚》。日本：講談社，2007年。

## 相關連結

  - [GUNDAM PERFECT WEB BB戰士](http://www.gundam.channel.or.jp/)
  - [（香港網頁）無國界SD高-{}-達大聯盟](http://www.sdgundamorg.com/)

[Category:SD GUNDAM](../Category/SD_GUNDAM.md "wikilink")
[SD戰國傳](../Category/SD戰國傳.md "wikilink") [Category:Comic
BomBom](../Category/Comic_BomBom.md "wikilink")