**SECOM株式會社**（[中文譯為](../Page/中文.md "wikilink")：**西科姆**，）成立於1962年，是[日本第一家](../Page/日本.md "wikilink")[保全公司](../Page/保全公司.md "wikilink")，現為日本最大的保全公司，同時也是[全球布局的保全集團](../Page/全球.md "wikilink")。

## 歷史

[SECOM_(headquarters_1).jpg](https://zh.wikipedia.org/wiki/File:SECOM_\(headquarters_1\).jpg "fig:SECOM_(headquarters_1).jpg")
1962年7月，飯田亮與戶田壽一成立**[日本警備保障株式會社](../Page/日本警備保障.md "wikilink")**，是日本第一家保全公司。1974年4月，日本警備保障在[東京證券交易所市場第二部](../Page/東京證券交易所.md "wikilink")[股票上市](../Page/股票上市.md "wikilink")。1983年12月，日本警備保障改名**SECOM株式會社**。1986年6月，SECOM在[大阪證券交易所市場第一部股票上市](../Page/大阪證券交易所.md "wikilink")。1992年12月，SECOM成立「西科姆（中國）有限公司」。

## 海外事業

  - [中興保全](../Page/中興保全.md "wikilink")：與[國產實業建設合資成立](../Page/國產實業建設.md "wikilink")。\[1\]

## 注釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [SECOM株式會社](https://www.secom.co.jp)

  - [中興保全](http://www.secom.com.tw)

[Category:澀谷區公司](../Category/澀谷區公司.md "wikilink")
[Category:總部在日本的跨國公司](../Category/總部在日本的跨國公司.md "wikilink")
[Category:保全公司](../Category/保全公司.md "wikilink")
[Category:保安](../Category/保安.md "wikilink")
[Category:日本机器人学](../Category/日本机器人学.md "wikilink")
[Category:大阪证券交易所上市公司](../Category/大阪证券交易所上市公司.md "wikilink")

1.  [西科姆海外事業聯結網頁](http://www.secom.co.jp/corporate/group/world.html)