**SECRET
AMBITION**，是[水树奈奈第](../Page/水树奈奈.md "wikilink")15张单曲，2007年4月18日发售，商品番号KICM-1199。

## 作品概要

2007年4月30日的[Oricon公信榜周排行榜上](../Page/Oricon.md "wikilink")，初登场即获得第二位的成绩，这平了之前在2005年10月25日本人单曲[ETERNAL
BLAZE在同一排行榜上获得的排名的记录](../Page/ETERNAL_BLAZE.md "wikilink")。两首单曲都来自[魔法少女奈叶系列的主题曲](../Page/魔法少女奈叶系列.md "wikilink")（OP）。

在首日的销售记录上，打破了之前由「ETERNAL
BLAZE」创造的记录。累计单曲、专辑一起算，是销售量最高的记录。Oricon排名：最高周排名第二位，2007年上半年单曲排名第61位。

## 收錄曲

1.  SECRET AMBITION
      - 作词：水樹奈奈、作曲：[志倉千代丸](../Page/志倉千代丸.md "wikilink")、编曲：[藤間仁](../Page/藤間仁.md "wikilink")（[Elements
        Garden](../Page/Elements_Garden.md "wikilink")）
      - 电视动画《[魔法少女奈叶StrikerS](../Page/魔法少女奈叶StrikerS.md "wikilink")》第一期主题曲（OP1）
2.  Heart-shaped chant
      - 作词：水樹奈奈、作曲・编曲：[上松範康](../Page/上松範康.md "wikilink")（Elements
        Garden）
      - [PS2游戏](../Page/PS2.md "wikilink")[光明之风主題歌](../Page/光明之风.md "wikilink")
      - 特别邀请上松範康之妹、[藤間仁之妻](../Page/藤間仁.md "wikilink")，日本[竖琴演奏家](../Page/竖琴.md "wikilink")[上松美香参与演奏](../Page/上松美香.md "wikilink")。
3.  Level Hi\!
      - 作词：、作曲・编曲：[斎藤真也](../Page/斎藤真也.md "wikilink")
      - [TBS节目](../Page/TBS.md "wikilink")4～6月片尾曲（ED）
4.  SECRET AMBITION (without NANA)
5.  Heart-shaped chant (without NANA)
6.  Level Hi！ (without NANA)

[Category:2007年单曲](../Category/2007年单曲.md "wikilink")
[Category:水樹奈奈單曲](../Category/水樹奈奈單曲.md "wikilink")
[Category:魔法少女奈葉歌曲](../Category/魔法少女奈葉歌曲.md "wikilink")
[Category:電視動畫主題曲](../Category/電視動畫主題曲.md "wikilink")
[Category:遊戲主題曲](../Category/遊戲主題曲.md "wikilink")
[Category:Oricon動畫單曲年榜冠軍歌曲](../Category/Oricon動畫單曲年榜冠軍歌曲.md "wikilink")