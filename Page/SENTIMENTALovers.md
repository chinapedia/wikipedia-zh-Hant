**SENTIMENTALovers**（中譯：情深意堅）是[日本男歌手](../Page/日本.md "wikilink")[平井堅的第六張原創專輯](../Page/平井堅.md "wikilink")，日本地區於2004年11月24日發行。發行首周空降[Oricon日本公信榜冠軍](../Page/Oricon.md "wikilink")，登場回數52周，總銷量超過160萬張，獲得[日本唱片協會百萬唱片認證](../Page/日本唱片協會.md "wikilink")。

## 解説

  - 距離上一張原創專輯『[LIFE
    is...](../Page/LIFE_is....md "wikilink")』約一年十個月的第六張原創專輯。
  - 収録了上一張原創專輯『LIFE
    is...』後發行的所有單曲「[style](../Page/Style_\(平井堅單曲\).md "wikilink")」、「[瞳をとじて](../Page/輕閉雙眼.md "wikilink")」、「[キミはともだち](../Page/朋友_\(平井堅單曲\).md "wikilink")」、「[思いがかさなるその前に・・・](../Page/在思緒重疊之前….md "wikilink")」，以及兩首c/w曲「signal」、「jealousy」。
  - 平井堅第三張百萬專輯，收錄的單曲總銷量為所有專輯最多。
  - 本專輯所有作詞皆由平井堅擔當。

## 單曲銷量

  - [style](../Page/Style_\(平井堅單曲\).md "wikilink")：ORICON最高第12名。銷量約4.4萬張
  - [輕閉雙眼](../Page/輕閉雙眼.md "wikilink")：ORICON最高第2名。銷量約89.3萬張
  - [朋友](../Page/朋友_\(平井堅單曲\).md "wikilink")：ORICON最高第5名。銷量約16.1萬張
  - [在思緒重疊之前…](../Page/在思緒重疊之前….md "wikilink")：ORICON最高第1名。銷量約27.2萬張

## 収録曲

1.  **[思いがかさなるその前に・・・](../Page/在思緒重疊之前….md "wikilink")**
      - 作詞・作曲：平井堅／編曲：亀田誠治
      - 22nd單曲。[TOYOTA](../Page/TOYOTA.md "wikilink")「COROLLA
        FIELDER」電視廣告曲※本人出演
      - 富士電視台連續劇「積木崩塌的真相」主題歌
2.  **[jealousy](../Page/朋友_\(平井堅單曲\).md "wikilink")**
      - 作詞：平井堅／作曲：YOSHIKA／編曲：OCTOPUSSY
      - 21st單曲「キミはともだち」c/w曲。
3.  **言わない関係**
      - 作詞・作曲：平井堅／編曲：中西康晴
4.  **君が僕に憑依した\!\!**
      - 作詞・作曲：平井堅／編曲：AKIRA
5.  **[瞳をとじて](../Page/輕閉雙眼.md "wikilink")**
      - 作詞・作曲：平井堅／編曲：亀田誠治
      - 20th單曲。電影「[世界の中心で、愛をさけぶ](../Page/在世界的中心呼喊愛情.md "wikilink")」主題歌
6.  **青春デイズ**
      - 作詞・作曲：平井堅／編曲：田中直
7.  **[style](../Page/Style_\(平井堅單曲\).md "wikilink")**
      - 作詞：平井堅／作曲：南ヤスヒロ／編曲：OCTOPUSSY
      - 19th單曲。
8.  **[signal](../Page/Style_\(平井堅單曲\).md "wikilink")**
      - 作詞・作曲：平井堅／編曲：FILUR
      - 19th單曲「style」c/w曲。
9.  **鍵穴**
      - 作詞・作曲：平井堅／編曲：URU
10. **nostalgia**
      - 作詞：平井堅／作曲：大沢伸一
11. **[キミはともだち](../Page/朋友_\(平井堅單曲\).md "wikilink")**
      - 作詞・作曲：平井堅／編曲：松浦晃久
      - 21st單曲。富士電視台連續劇「ワンダフルライフ」主題歌
12. **センチメンタル**
      - 作詞・作曲：平井堅／編曲：塩谷哲
      - 日本電視台「第25屆[洲際盃足球賽](../Page/洲際盃足球賽.md "wikilink") 總決賽」中継主題曲

## 参考資料

[Sony Music的作品紹介](../Page/索尼音樂娛樂.md "wikilink")

  - [通常盤](http://www.sonymusic.co.jp/Music/Arch/DF/KenHirai/DFCL-1170/index.html)

[Category:2004年音樂專輯](../Category/2004年音樂專輯.md "wikilink")
[Category:平井堅音樂專輯](../Category/平井堅音樂專輯.md "wikilink")
[Category:RIAJ百萬認證專輯](../Category/RIAJ百萬認證專輯.md "wikilink")
[Category:Oricon百萬銷量達成專輯](../Category/Oricon百萬銷量達成專輯.md "wikilink")
[Category:2004年Oricon專輯週榜冠軍作品](../Category/2004年Oricon專輯週榜冠軍作品.md "wikilink")