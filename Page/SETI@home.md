[Setiathomeversion3point07.JPG](https://zh.wikipedia.org/wiki/File:Setiathomeversion3point07.JPG "fig:Setiathomeversion3point07.JPG")

****（，“在家[搜寻外星智慧](../Page/搜尋地外文明.md "wikilink")”），是一个通过[互联网利用](../Page/互联网.md "wikilink")[个人计算机处理](../Page/个人计算机.md "wikilink")[天文数据的](../Page/天文.md "wikilink")[分布式计算项目](../Page/分布式计算.md "wikilink")。该项目试图通过分析[阿雷西博射电望远镜采集的](../Page/阿雷西博射电望远镜.md "wikilink")[无线电信号](../Page/无线电.md "wikilink")，搜寻能够证实[地外智慧生物存在的证据](../Page/地外生物.md "wikilink")。该项目由[美国](../Page/美国.md "wikilink")[加州大学伯克利分校的](../Page/加州大学伯克利分校.md "wikilink")[伯克利空间科学实验室](../Page/伯克利空间科学实验室.md "wikilink")（SSL）主办。

它主要会在接受到的射电数据中搜寻以下三种讯号：

  - 讯号强度的[高斯曲线形升降](../Page/高斯曲线.md "wikilink")；
  - 可能代表[窄频数码讯息的脉冲讯号](../Page/窄频.md "wikilink")；
  - 三连波讯号，即三个等间距的突波。

程序在用户的个人计算机上，通常在屏幕保护模式下或以后台模式运行。它利用的是多余的处理器资源，不影响用户正常使用计算机。

项目自1999年5月17日开始正式运行。至2004年5月，该项目在世界各地拥有近500万参与者，积累了近200万年的[CPU运行时间](../Page/CPU.md "wikilink")，进行了近5×10<sup>21</sup>次[浮点运算](../Page/浮点数.md "wikilink")，处理了超过13亿个数据单元，无疑是非常成功的分布式计算试验项目。不过到目前为止，该项目的分析结果中还没有足以证明外星智能生命存在的证据。

官方2005年3月中旬发布消息，逐渐停止（即旧平台）的计算，全面转入[BOINC计算平台](../Page/BOINC.md "wikilink")，数据转换预计在2个月之内完成。但是由于全球的计算者抵制新的平台，所以仍运行了数月，只是关闭新帐户注册。2005年12月15日美国伯克利大学官方已关闭。

项目计划在将来增加处理来自[澳大利亚](../Page/澳大利亚.md "wikilink")[帕克斯天文台](../Page/帕克斯天文台.md "wikilink")（Parkes
Observatory）的数据，以便同时分析南半球的天空。在有更多的射电望远镜加入这个项目后，2008年1月的新闻稿说，项目每天增加的新数据为300G，需要更多的志愿者加入才能处理如此多的数据\[1\]。

## 参考文献

## 外部链接

  - [SETI@home 主页](http://setiathome.berkeley.edu/)
  - [SETI@home 正體中文主頁](http://setitaiwan.tripod.com/MIRROR/)
  - [SETI@home Classic紀念文章](http://setiathome.berkeley.edu/classic.php)

## 参见

  - [搜尋地外文明](../Page/搜尋地外文明.md "wikilink")
  - [分布式计算](../Page/分布式计算.md "wikilink")
  - [网格计算](../Page/网格计算.md "wikilink")
  - [BOINC](../Page/BOINC.md "wikilink")

{{-}}

[Category:伯克利开放式网络计算平台](../Category/伯克利开放式网络计算平台.md "wikilink")
[Category:宇宙](../Category/宇宙.md "wikilink")
[Category:公众科学](../Category/公众科学.md "wikilink")

1.