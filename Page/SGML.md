**标准通用标记语言**（，）是现时常用的[超文本格式的最高层次标准](../Page/超文本.md "wikilink")，是可以定义标记语言的[元语言](../Page/元语言.md "wikilink")，甚至可以定义不必采用\<
\>的常规方式。由于它的复杂，因而难以普及。

这是它的一個例子：

<QUOTE TYPE="example">` `
`typically something like `<ITALICS>`this`</ITALICS>` `
</QUOTE>

同时它也是一个ISO标准："ISO 8879:1986 Information processing -- Text and office
systems -- Standard Generalized Markup Language (SGML)"

它有非常强大的适应性，也正是因为同样的原因，导致在小型的应用中难以普及。

[HTML和](../Page/HTML.md "wikilink")[XML同样衍生于它](../Page/XML.md "wikilink")：XML可以被认为是它的一个子集，而HTML是它的一个应用。

XML的产生就是为了简化它，以便用于更加通用的目的，比如[语义Web](../Page/语义Web.md "wikilink")。它已经应用于大量的场合，比较著名的有[XHTML](../Page/XHTML.md "wikilink")、[RSS](../Page/RSS.md "wikilink")、[XML-RPC和](../Page/XML-RPC.md "wikilink")[SOAP](../Page/SOAP.md "wikilink")。

## 参见

  - [S-表达式](../Page/S-表达式.md "wikilink")
  - [LaTeX](../Page/LaTeX.md "wikilink")
  - [XML](../Page/XML.md "wikilink")

[Category:标记语言](../Category/标记语言.md "wikilink")