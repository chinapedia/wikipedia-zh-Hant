『**SHOCK**』是由日本[傑尼斯事務所旗下藝人](../Page/傑尼斯事務所.md "wikilink")**[堂本光一](../Page/堂本光一.md "wikilink")**擔任座長，負責企劃、構思、監製、音樂及主演，並由事務所社長[Johnny喜多川從旁輔導的](../Page/Johnny喜多川.md "wikilink")[音樂劇系列總稱](../Page/音樂劇.md "wikilink")。該劇最初是以1991年[少年隊主演的音樂劇](../Page/少年隊.md "wikilink")『PLAYZONE
'91 SHOCK』為基礎改編，但自2000年11月，『MILLENNIUM
SHOCK』在「[帝國劇場](../Page/帝國劇場.md "wikilink")」首次公演之後，為追求更好的舞台效果，每年都會針對名稱、表演內容及演員陣容進行調整，現已呈現完全不同的風貌，迄今固定在帝國劇場公演，成為一年一度的盛事。2005年演出該系列第五個版本時，更宣布定名為『Endless
SHOCK』，內容也作了大幅更新。由於該劇內容精彩、佳評如潮，經常造成一票難求的盛況，被日本媒體譽為『最難買到票的舞台劇』。

2008年4月，第33屆『菊田一夫演劇賞』頒發最高榮譽『演劇大賞』給『Endless
SHOCK』\[1\]，以表揚該劇的高度舞台成果。這是傑尼斯藝人主演的舞台劇中，首次獲此殊榮。

2009年3月，該劇花費8年又5個月的時間，刷新帝國劇場個人主演舞台劇「最多公演場次」的紀錄\[2\]，目前仍不斷累積，成為帝國劇場百年歷史中最具代表性的作品之一。

2013年3月21日，費時12年又5個月，達成1000場公演的紀錄，成為日本劇場史上最快達成千場的單人主演舞台\[3\]。

2014年10月31日，於福岡博多座完成終場演出後，累計達成「1214場公演」，刷新日本單人主演舞台劇的場次紀錄，成為日本第一名紀錄保持人\[4\]。

## 概要

『**SHOCK**』是以「劇場」內幾位演員發展出的故事為主軸，其中再穿插經典戲劇場面的「劇中劇」，透過交互演出的方式進行。全系列圍繞著『**Show
must go
on**』的核心主題，堅持『無論如何也要完成演出』的舞台精神，故事以「光一」為主人翁，透過他與其他角色之間從共同合作、意見紛歧、衝突，到最後大和解、團結一心的互動過程，描繪出一段「用生命演出」的舞台人生。雖然主演的[堂本光一曾說](../Page/堂本光一.md "wikilink")「光一（劇中主角）跟我是完全不同的人」，但他多次於公演期間受傷仍持續演出，對舞台工作毫不妥協的認真態度卻如出一轍。他也曾表示希望藉由演出這齣劇，來培育自己有同樣的精神與態度。

該劇大量使用原創樂曲，再結合知名音樂劇曲目，以華麗的歌舞與場景，打造出媲美紐約百老匯的主舞台。另外，劇中劇常以日本經典的傳奇軼事、武士殺陣為主題，為全劇增添不少東洋風情，也曾設計過大規模的奇幻魔術秀，風格多變。

除了豐富的表演題材之外，[堂本光一展現了許多高危險的驚人演出](../Page/堂本光一.md "wikilink")，包括用單臂繞住彩帶飛越觀眾席、整個人綁腳倒吊在空中360°旋轉、從高達8米傾斜40度的階梯跌落下來等，精彩程度曾被比喻為『每五分鐘就帶給觀眾一次驚喜』。極具舞台魅力的個人表現，不停進化的表演內容，每每造成話題\[5\]。

原本預定在2006年公演完畢即宣告結束的『Endless
SHOCK』，因為反應熱烈、向隅者眾，大約14個人中只有1個人能買到票的懸殊比例，堂本光一也說過就連他自己想替親友買票也買不到，一票難求的程度，讓觀眾要求加演的呼聲不斷，故隔年（2007年）決定再演\[6\]。『Endless
SHOCK』也因此被公認是「日本最難買到票的舞台劇」以及「傑尼斯偶像演出的舞台中最難買到票的表演」。

## MILLENNIUM SHOCK（2000年）

### 公演期間

  - 2000年11月2日－11月26日（共38場）

### 劇情大綱

正在巡迴公演的劇團領導人光一，因不顧成員翼的傷勢逕行演出，而與團員間產生爭執。此時，與光一3年前亡故的哥哥頗有因緣的美國百老匯劇場，傳來了演出的邀約。不顧眾人反對前往百老匯的光一，出現在他面前的卻是哥哥生前的友人，一位像謎一樣的人物...。

### 演員

  - [堂本光一](../Page/堂本光一.md "wikilink")（[KinKi
    Kids](../Page/KinKi_Kids.md "wikilink")）
  - [東山紀之](../Page/東山紀之.md "wikilink")（[少年隊](../Page/少年隊.md "wikilink")）
  - [今井翼](../Page/今井翼.md "wikilink")（[瀧與翼](../Page/瀧與翼.md "wikilink")）
  - [赤坂晃](../Page/赤坂晃.md "wikilink")（前[光GENJI](../Page/光GENJI.md "wikilink")）
  - [鈴木ほのか](../Page/鈴木ほのか.md "wikilink")
  - [篠井英介](../Page/篠井英介.md "wikilink")
  - [秋山純](../Page/秋山純.md "wikilink")（[Musical
    Academy](../Page/Musical_Academy.md "wikilink")）
  - [町田慎吾](../Page/町田慎吾.md "wikilink")（Musical Academy）
  - [米花剛史](../Page/米花剛史.md "wikilink")（Musical Academy）
  - [屋良朝幸](../Page/屋良朝幸.md "wikilink")（Musical Academy）
  - [上田龍也](../Page/上田龍也.md "wikilink")（[KAT-TUN](../Page/KAT-TUN.md "wikilink")）
  - [中丸雄一](../Page/中丸雄一.md "wikilink")（KAT-TUN）
  - [赤西仁](../Page/赤西仁.md "wikilink")（KAT-TUN）

## SHOW劇・SHOCK（2001年、2002年）

SHOCK取其「衝擊」的意思。日文劇名寫作「ショー劇・SHOCK」。

### 公演期間

  - 2001年12月1日－2002年1月27日（共76場）
  - 2002年6月4日－6月28日（共38場）

### 劇情大綱

繼承已故哥哥的遺志，謹記著『Show must go on』每天持續演出的光一。因為公演獲得好評，接到百老匯Imperial
Garden劇院的演出邀約。由於那邊正是哥哥去世的地方，加上翼在表演中發生事故，甚至可能斷送舞者生涯，此時不顧成員前往美國，將受到周遭強烈的反對。但光一仍秉持『Show
must go on』的舞台精神，放下嫂嫂、嫂嫂現任丈夫與失意的翼前往紐約，而那裡卻也有各種麻煩在等著他...。

### 演員

  - [堂本光一](../Page/堂本光一.md "wikilink")([KinKi
    Kids](../Page/KinKi_Kids.md "wikilink"))
  - [樹里咲穂](../Page/樹里咲穂.md "wikilink")
  - [今井翼](../Page/今井翼.md "wikilink")([瀧與翼](../Page/瀧與翼.md "wikilink"))
  - [錦戸亮](../Page/錦戸亮.md "wikilink")([関ジャニ∞](../Page/関ジャニ∞.md "wikilink"))
  - [今拓哉](../Page/今拓哉.md "wikilink")
  - [秋山純](../Page/秋山純.md "wikilink")
  - [町田慎吾](../Page/町田慎吾.md "wikilink")
  - [米花剛史](../Page/米花剛史.md "wikilink")
  - [屋良朝幸](../Page/屋良朝幸.md "wikilink")
  - [龜梨和也](../Page/龜梨和也.md "wikilink")([KAT-TUN](../Page/KAT-TUN.md "wikilink")）
  - [赤西仁](../Page/赤西仁.md "wikilink")
  - [田中聖](../Page/田中聖.md "wikilink")
  - [田口淳之介](../Page/田口淳之介.md "wikilink")
  - [長谷川純](../Page/長谷川純.md "wikilink")
  - [戸塚祥太](../Page/戸塚祥太.md "wikilink")（[A.B.C-Z](../Page/A.B.C-Z.md "wikilink")）
  - [東新良和](../Page/東新良和.md "wikilink")
  - [辰巳雄大](../Page/辰巳雄大.md "wikilink")（[4U](../Page/4U.md "wikilink")）
  - [小山慶一郎](../Page/小山慶一郎.md "wikilink")（[NEWS](../Page/NEWS.md "wikilink")）

## SHOCK is Real Shock（2003年）

### 公演期間

  - 2003年1月8日－2月25日（共76場）

### 劇情大綱

大致上與『SHOW劇・SHOCK』相同，唯角色名字隨著演員不同有所改變（基本上是以演員本人的名字來當作角色名字）。此外，黑幕人物也有變更。

### 演員

  - 堂本光一（KinKi Kids）
  - [未唯](../Page/未唯.md "wikilink")
  - [生田斗真](../Page/生田斗真.md "wikilink")
  - [井上順](../Page/井上順.md "wikilink")
  - 秋山純（Musical Academy）
  - 町田慎吾（Musical Academy）
  - 米花剛史（Musical Academy）
  - 屋良朝幸（Musical Academy）
  - 東新良和
  - [風間俊介](../Page/風間俊介.md "wikilink")
  - KAT-TUN
  - [A.B.C](../Page/A.B.C-Z.md "wikilink")

## Shocking SHOCK（2004年）

### 公演期間

  - 2004年2月6日－2月29日（共38場）

### 劇情大綱

大致與『SHOW劇・SHOCK』、『SHOCK is Real Shock』相同。（演出者不同）

### 演員

  - 堂本光一（KinKi Kids）
  - 今井翼（瀧與翼）
  - [伊織直加](../Page/伊織直加.md "wikilink")
  - [尾藤イサオ](../Page/尾藤イサオ.md "wikilink")
  - 東新良和
  - 秋山純（Musical Academy）
  - 町田慎吾（Musical Academy）
  - 米花剛史（Musical Academy）
  - 屋良朝幸（Musical Academy）
  - 風間俊介
  - 戶塚祥太

## Endless SHOCK（2005年－）

由堂本光一參與腳本、演出，是歷年變更劇情與登場人物幅度最大的一次。2013年起，劇團老闆一角改由女演員擔任，並在不影響劇情的前提下，新增或重新編排歌曲、劇目，讓這齣作品持續創新。

### 公演期間

  - 2005年1月8日－2月28日（共76場）
  - 2006年2月6日－3月29日（共76場）
  - 2007年1月6日－2月28日（共81場）
  - 2008年1月6日－2月26日（共76場）
  - 2009年2月5日－3月30日（共76場）
  - 2010年2月14日 ‐ 3月30日、7月4日 - 7月31日（共100場） ※Endless SHOCK 初次夏季公演
  - 2011年2月5日 - 3月10日（共48場）
  - 2012年1月7日 - 31日（博多座 共32場）※SHOCK初次地方公演
  - 2012年2月7日 - 4月30日（共105場）
  - 2013年2月4日 - 3月31日
  - 2013年4月8日 - 30日（博多座）
  - 2013年9月2日 - 29日（梅田藝術劇場）※SHOCK初次大阪公演
  - 2014年2月4日 - 3月31日
  - 2014年9月8日 - 30日（梅田藝術劇場）
  - 2014年10月8日 - 31日（博多座）

### 劇情大綱

光一在童年好友秋山繼承的業餘小劇場裡演出，獲得輿論報導一致的好評。劇團裡，希望以此為開端邁向百老匯，朝著目標努力不懈的光一；慘淡經營，卻拼命想守護小劇場的秋山（大倉）；把光一當成競爭對手，逐漸燃起火花的翼（亮、斗真、屋良）以及盲目愛慕著光一的Rika，每個人都有不同的目標與考量，使得這群原本和睦的好友之間，隨著日子過去，漸漸交織出複雜的情感...。某天，與光一屢次意見不合的翼（亮、斗真、屋良），為了測試光一是否真能秉持『Show
must go on』的信念，竟在舞台演出時，把原本要砍向光一的道具換成了真的劍...。

### 演員

  - [堂本光一](../Page/堂本光一.md "wikilink")（KinKi Kids）
  - 今井翼（瀧與翼）－2005、6年 飾演 翼（與 錦戸亮 分飾一角）
  - [錦戸亮](../Page/錦戸亮.md "wikilink")（NEWS、[關西傑尼斯8](../Page/關西傑尼斯8.md "wikilink")）－2005、6年
    飾演 亮（與 今井翼 分飾一角）
  - 生田斗真－2007年 飾演 斗真
  - 屋良朝幸（Musical Academy）－2008、9年 飾演 屋良
  - [黑木美沙](../Page/黑木美沙.md "wikilink")－2005年 飾演 Rika（與 小宮山實花 分飾一角）
  - [小宮山實花](../Page/小宮山實花.md "wikilink")－2005年 飾演 Rika（與 黑木美沙 分飾一角）
  - [田畑亞彌](../Page/田畑亞彌.md "wikilink")－2006年 飾演 Rika
  - [松本真理香](../Page/松本真理香.md "wikilink")－2007年 飾演 Rika
  - [RiRiKA](../Page/RiRiKA.md "wikilink")－2008年 飾演 Rika
  - [佐藤めぐみ](../Page/佐藤めぐみ.md "wikilink")－2009、10年 飾演 Rika
  - 原田夏希 - 2010年 飾演 Rika
  - 神田沙也加 - 2012年 飾演 Rika
  - [梅田彩佳](../Page/梅田彩佳.md "wikilink") - 2019年 飾演 Rika
  - [石川直](../Page/石川直.md "wikilink")
  - 秋山純（原Musical Academy）－2005、6、7年 飾演 秋山
  - [大倉忠義](../Page/大倉忠義.md "wikilink")（關西傑尼斯8）－2008年 飾演 大倉
  - [植草克秀](../Page/植草克秀.md "wikilink")（[少年隊](../Page/少年隊.md "wikilink")）－2009年
    飾演 劇團老闆（Rika的父親）
  - 町田慎吾（Musical Academy）
  - 米花剛史（Musical Academy）
  - 屋良朝幸（Musical Academy）
  - [福田悠太](../Page/福田悠太.md "wikilink")（M.A.D.）
  - [松崎祐介](../Page/松崎祐介.md "wikilink")（M.A.D.）
  - 辰巳雄大（M.A.D.）
  - [越岡裕貴](../Page/越岡裕貴.md "wikilink")（M.A.D.）
  - [A.B.C](../Page/A.B.C-Z.md "wikilink")

## 劇中劇

在主要的劇情之間，穿插了國內外經典戲劇知名場面的重新演繹。

  - [哈姆雷特](../Page/哈姆雷特.md "wikilink")
  - 理查三世（莎士比亞劇 King Richard III）
  - [羅密歐與茱麗葉](../Page/羅密歐與茱麗葉.md "wikilink")
  - Elektra（希臘神話）
  - 曾根崎心中
  - [新選組](../Page/新選組.md "wikilink")
  - [鐘樓怪人](../Page/巴黎聖母院_\(小說\).md "wikilink")
  - 東海道四谷怪談
  - 忠臣藏
  - [白鯨記](../Page/白鯨記.md "wikilink")
  - [西城故事](../Page/西城故事.md "wikilink")　等

## 使用樂曲

  - Take the 'A' Train
  - [布蘭詩歌](../Page/布蘭詩歌.md "wikilink")
  - BROADWAY MELODY　等，其餘大部分使用原創歌曲。

## 相關商品

全部以堂本光一名義發行。唱片公司為[傑尼斯娛樂](../Page/傑尼斯娛樂.md "wikilink")。

### DVD

  - KOICHI DOHMOTO SHOCK DIGEST（2002年6月19日）
  - Koichi Domoto SHOCK（2003年1月16日）
  - Endless SHOCK（2006年2月15日）
  - Endless SHOCK 2008（2008年10月29日）

### CD

  - [KOICHI DOMOTO Endless SHOCK Original Sound
    Track](../Page/KOICHI_DOMOTO_Endless_SHOCK_Original_Sound_Track.md "wikilink")（2006年1月11日）

## 参考文献

<div class="references-small">

<references />

</div>

[Category:日本音樂劇作品](../Category/日本音樂劇作品.md "wikilink")

1.  2008/4/4 菊田一夫演劇賞大賞に「Ｅｎｄｌｅｓｓ　ＳＨＯＣＫ」《產經新聞》
2.  2009/3/13 堂本光一、帝国劇場単独主演記録を626回に更新　森繁久彌を抜いて1位に
3.  [2013/3/21 堂本光一、『SHOCK』1000回公演達成 剛とヒガシも祝福《Oricon
    News》](http://www.oricon.co.jp/news/2022814/full/)
4.  [2014/11/1 堂本光一“日本一の舞”　ミュージカル単独主演記録達成＆更新１２１４回《YAHOO
    News》](http://headlines.yahoo.co.jp/hl?a=20141101-00000004-dal-ent)

5.  2007/10/25 限界超えてやる！堂本光一が史上最高“２６段階段落ち”に挑戦
6.  [2006/2/13
    堂本光一「ＳＨＯＣＫ」異例の再上演《日刊體育報》](http://www.nikkansports.com/ns/entertainment/p-et-tp0-060213-0012.html)