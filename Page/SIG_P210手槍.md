**SIG
P210**是[瑞士](../Page/瑞士.md "wikilink")[SIG](../Page/SIG.md "wikilink")（現為[Swiss
Arms](../Page/Swiss_Arms.md "wikilink")）在1949年推出的[半自動手槍](../Page/半自動手槍.md "wikilink")。

## 設計

SIG
P210是一把由瑞士著名廠商SIG生產的單動手槍，可發射[9×19毫米](../Page/9×19mm鲁格弹.md "wikilink")、[7.65毫米Luger及](../Page/7.65mm_Luger.md "wikilink")[.22
LR口徑彈藥](../Page/.22_LR.md "wikilink")，單排彈匣可放8發子彈，[機匣裝有可強制封鎖扳機的手動保險及彈匣退出時自動扳機扳機的自動保險系統](../Page/機匣.md "wikilink")。其獨特之處是它的主要鋼制部件由人手車削，其套筒及骨架配套制成，採用高質量的120毫米槍管，加上嚴格的品質監控，因此其可靠性、準確度、耐用性都比一般手槍為高，在50米射靶時可打出在5至10發保持5厘米內的成绩。

P210雖然有高命中精度和可靠的優點，但早期版本沒有握把式彈匣釋放鈕，不及其他手槍般操作方便，且由於手工裝配及高質量部件令價格比其他手槍為高，因此當時沒有太多國家採用，現在的美國民用市場上只有少數當時的P210發售，售價更超過3000[美元](../Page/美元.md "wikilink")。

## 採用

SIG
P210在1949年推出後便成為了[瑞士陸軍的制式手槍](../Page/瑞士陸軍.md "wikilink")，當時命名為**Pistole
49**\[1\]，並一直採用至1975年被[SIG
P75手槍取代](../Page/SIG_P220手槍.md "wikilink")，並曾被[德國聯邦警察採用](../Page/德國聯邦警察.md "wikilink")。

一些[瑞士警察部隊](../Page/瑞士.md "wikilink")、[丹麥及](../Page/丹麥.md "wikilink")[拉脫維亞的軍隊和](../Page/拉脫維亞.md "wikilink")[摩納哥的大公槍騎兵中隊至今仍然採用P](../Page/摩納哥.md "wikilink")210為制式手槍。

## 型號

[Sig210-6.jpg](https://zh.wikipedia.org/wiki/File:Sig210-6.jpg "fig:Sig210-6.jpg")

SIG P210有多種不同版本：
（註：[機匣上號碼前印有](../Page/機匣.md "wikilink")「A」字是瑞士陸軍版本，「P」字是瑞士軍方送給退役士兵時印上的標記，解為私人擁有，丹麥及德國版本印有特別標記。）

  - **SIG P210-1**：普通版本，有9×19毫米、7.65毫米Luger及.22
    LR三種口徑，裝有木制握把片、固定缺口式照門及槍身處理。

<!-- end list -->

  - **SIG P210-2**：瑞士陸軍版本，9×19毫米口徑，裝有塑料制握把片、固定缺口式照門及槍身磨沙處理。

<!-- end list -->

  - **SIG P210-3**：瑞士警察版本，有9×19毫米及7.65毫米Luger兩種口徑，裝有木制握把片、固定缺口式照門及槍身處理。

<!-- end list -->

  - **SIG P210-4**：德國聯邦警察版本，9×19毫米口徑，以P210-2的設計改進而成，加裝供彈指示器。

<!-- end list -->

  - **SIG
    P210-5**：加長槍管（150毫米）版本，有9×19毫米及7.65毫米Luger兩種口徑，裝有木制握把片、可調式扳機、可調及可拆式射靶用照門、槍身磨沙處理。

<!-- end list -->

  - **SIG
    P210-6**：運動型版本，有9×19毫米及7.65毫米Luger兩種口徑，裝有木制或塑料制握把片、可調式扳機、照門可選、槍身磨沙處理。

<!-- end list -->

  - **SIG P210-7**：[.22
    LR口徑版本](../Page/.22_LR.md "wikilink")，裝有木制或塑料制握把片，後期型改用特制擊鎚，通過轉換部件可以把所有型號的P210改為P210-7。

<!-- end list -->

  - **SIG P210-8**：高級運動型版本，9×19毫米口徑，裝有握把式彈匣釋放鈕、木制握把片、可調式扳機、可調式照門、槍身磨沙處理。

<!-- end list -->

  - **SIG
    P210-5LS**：2003年版本，9×19毫米口徑，裝有握把式彈匣釋放鈕、加長槍管及套筒、木制握把片、可調式扳機、可調式照門、槍身磨沙處理。

<!-- end list -->

  - **SIG
    P210-6S**：2003年版本，9×19毫米口徑，裝有握把式彈匣釋放鈕、標準槍管及套筒、木制握把片、可調式扳機、可調式照門、槍身磨沙處理。

<!-- end list -->

  - **SIG m/49**：又名**Neuhausen**，丹麥皇家陸軍版本的P210-1或P210-2，9×19毫米口徑，槍身印有標記。

## 使用國

  -
  -
  -
  -
  -
  -
## 資料來源

<div class="references-small">

<references />

  - [P210.com-SIG P210歷史](http://www.p210.com/history/)
  - [D boy gun world-SIG
    P210](http://firearmsworld.net/sigsauer/p210/P210.htm)

</div>

## 相關條目

  - [SIG P220手槍](../Page/SIG_P220手槍.md "wikilink")（Pistole 75）
  - [SIG P230手槍](../Page/SIG_P230手槍.md "wikilink")

## 外部链接

  - \-[SIG
    P210的設計](https://web.archive.org/web/20071119204325/http://larvatus.livejournal.com/33732.html)

  - \-[waffenhq.de-P210](https://web.archive.org/web/20030118065331/http://www.waffenhq.de/infanterie/sig210.html)

[Category:半自動手槍](../Category/半自動手槍.md "wikilink")
[Category:瑞士槍械](../Category/瑞士槍械.md "wikilink")
[Category:9毫米魯格彈槍械](../Category/9毫米魯格彈槍械.md "wikilink")
[Category:.22 LR口徑槍械](../Category/.22_LR口徑槍械.md "wikilink")

1.  [official Swiss Army
    webpage](http://www.lba.admin.ch/internet/lba/de/home/logistikleistung/ausrue/pers0/bewaffnung/pist49.html)