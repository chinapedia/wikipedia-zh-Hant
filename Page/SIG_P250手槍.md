**SIG P250**是[Swiss
Arms生產的](../Page/Swiss_Arms.md "wikilink")[9×19毫米口徑緊湊型](../Page/9×19mm鲁格弹.md "wikilink")[手槍](../Page/手槍.md "wikilink")。

## 設計

P250裝有3.86寸槍管及15發彈匣，空槍只重約1.5磅，扣力為8.6磅，有以下的特點：

  - 採用一般手槍常見的缺口式照門
  - 底部配有戰術導軌
  - 採用與其他SIG手槍相同的短後座作用原理、、純雙動扳機系統設計
  - 由於採用簡化部件設計，分解P250時無須專門工具\[1\]
  - 裝有防意外[擊針保險裝置](../Page/撞針.md "wikilink")，所以不設手動保險制

P250在2007年11月推出了[.45 ACP口徑版本](../Page/.45_ACP.md "wikilink")，而[.357
SIG及](../Page/.357_SIG.md "wikilink")[.40
S\&W於](../Page/.40_S&W.md "wikilink")2009年推出。

## 型號

[SIG-SAUER_PPNL.jpg](https://zh.wikipedia.org/wiki/File:SIG-SAUER_PPNL.jpg "fig:SIG-SAUER_PPNL.jpg")
ACTION 4 NP 的彈藥的SIG Sauer PPNL 手槍。\]\]
[SIG_Sauer_P250_9mm.jpg](https://zh.wikipedia.org/wiki/File:SIG_Sauer_P250_9mm.jpg "fig:SIG_Sauer_P250_9mm.jpg")

  - 原廠型號：

<!-- end list -->

  - 普通型
  - 緊湊型
  - 袖珍型
  - PPNL (Politie Pistool Nederland - Police Pistol
    Netherlands)：採用[RUAG](../Page/RUAG.md "wikilink") ACTION 4
    NP的彈藥的P250，已經停產。

## 採用

  - ： [警務處刑事部絕大部份部門](../Page/香港警務處.md "wikilink")

      - [有組織罪案及三合會調查科](../Page/有組織罪案及三合會調查科.md "wikilink")
      - [重案組](../Page/重案組_\(香港\).md "wikilink")
      - [刑事情報科](../Page/刑事情報科.md "wikilink")
      - [商業罪案調查科](../Page/商業罪案調查科.md "wikilink")
      - [毒品調查科](../Page/毒品調查科.md "wikilink")\[2\]\[3\]\[4\]\[5\]

  - ：警察單位\[6\]

### 已取消訂單

  - ：警察，特製的PPNL型號以取代[瓦爾特P5及](../Page/瓦爾特P5手槍.md "wikilink")[格洛克17](../Page/格洛克17.md "wikilink")；最後選擇[P99Q作為新佩槍](../Page/瓦爾特P99手槍.md "wikilink")

  - ：[內政部警政署](../Page/內政部警政署.md "wikilink"),改採用[瓦爾特PPQ手槍](../Page/瓦爾特PPQ手槍.md "wikilink")。

  - ：[聯邦航空警察](../Page/:en:_Federal_Air_Marshal_Service.md "wikilink")，以取代[SIG
    P229手槍](../Page/SIG_P229手槍.md "wikilink")

## 流行文化

### 電影

  - 2012年—《[寒戰](../Page/寒戰.md "wikilink")》（Cold War）：型號為P250
    Dcc，被劉傑輝（[郭富城飾演](../Page/郭富城.md "wikilink")）、徐永基（[錢嘉樂飾演](../Page/錢嘉樂.md "wikilink")）和李文彬（[梁家輝飾演](../Page/梁家輝.md "wikilink")）所使用。
  - 2017年—《[寒戰II](../Page/寒戰II.md "wikilink")》（Cold War II）：型號為P250
    Dcc，被劉傑輝（[郭富城飾演](../Page/郭富城.md "wikilink")）和李文彬（[梁家輝飾演](../Page/梁家輝.md "wikilink")）所使用。

### 電子遊戲

  - 2012年—《[-{zh-hans:反恐精英：全球攻势;
    zh-hant:絕對武力：全球攻勢;}-](../Page/反恐精英：全球攻勢.md "wikilink")》（Counter-Strike:
    Global Offensive）：為CS:GO新加入的武器，用以替代過往作品中的[SIG
    P228手槍](../Page/SIG_P228手槍.md "wikilink")。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - —[sigarms.com](https://web.archive.org/web/20061015170824/http://www.sigarms.com/)

  - —[world.guns.ru-P250 DCc](http://world.guns.ru/handguns/hg119-e.htm)

  - —[Sig-Arms Advertising
    Video](http://www.youtube.com/watch?v=_MqnZqVdiLs)

  - —[D Boy Gun World（槍炮世界）—SIG Sauer
    P250](http://firearmsworld.net/sigsauer/p250/p250.htm)

[Category:半自動手槍](../Category/半自動手槍.md "wikilink")
[Category:9毫米魯格彈槍械](../Category/9毫米魯格彈槍械.md "wikilink")
[Category:.40 S\&W口徑槍械](../Category/.40_S&W口徑槍械.md "wikilink")
[Category:.357 SIG口徑槍械](../Category/.357_SIG口徑槍械.md "wikilink")
[Category:.45 ACP口徑槍械](../Category/.45_ACP口徑槍械.md "wikilink")
[Category:美國槍械](../Category/美國槍械.md "wikilink")

1.
2.  [police.gov.hk-為刑偵人員提供新手槍訓練](http://www.police.gov.hk/offbeat/844/chi/bottom.htm)
3.  [頭條網-香港CID採用SIG P250
    DCc](http://www.hkheadline.com/news/headline_news_detail.asp?id=21604)
4.
5.
6.  <https://sites.google.com/site/worldinventory/wiw_eu_unitedkingdom>