[1GB_DDR2_SO-DIMM.png](https://zh.wikipedia.org/wiki/File:1GB_DDR2_SO-DIMM.png "fig:1GB_DDR2_SO-DIMM.png")
**SK海力士半導體公司**（SK Hynix Semiconductor
Inc.，[諺文](../Page/諺文.md "wikilink")：，）是一家[韓國的](../Page/韓國.md "wikilink")[電子公司](../Page/電子.md "wikilink")，[全球二十大半導體廠商之一](../Page/全球二十大半導體廠商.md "wikilink")。海力士於1983年以**現代電子產業有限公司**的名字創立。在80及90年代
他們專注於銷售[DRAM](../Page/DRAM.md "wikilink")，後來是[SDRAM](../Page/SDRAM.md "wikilink")。2001年他們以6億5000萬美元的價格出售[TFT
LCD業務](../Page/薄膜電晶體液晶顯示器.md "wikilink")，同年他們開發出世界第一顆128MB圖形DDR
SDRAM。2012年初，韩国第三大财阀[SK集团宣布收购海力士](../Page/SK集团.md "wikilink")21.05%的股份从而入主这家記憶體大厂。

## 價格操控與處分

2004－2005年一個調查顯示出1999－2002年期間有一個共謀破壞競爭與提高個人電腦價格的全球性DRAM價格操控。結果導致[三星電子需繳付](../Page/三星電子.md "wikilink")3億美元罰金，[英飛淩於](../Page/英飛淩.md "wikilink")2004年繳付1億6000萬美元罰金，海力士於2005年繳付1億8500萬美元罰金。[美光因與檢察官合作而不需繳交罰金](../Page/美光.md "wikilink")。

## 参见

  - [全球二十大半导体厂商](../Page/全球二十大半导体厂商.md "wikilink")

## 外部連結

  - [海力士半導體官方網站](https://web.archive.org/web/20170606000402/http://skhynix.com/)

## 参考文献

<div class="references-small">

<references>

</references>

</div>

[Category:韓國電子公司](../Category/韓國電子公司.md "wikilink")
[Category:韩国半导体公司](../Category/韩国半导体公司.md "wikilink")
[Category:電腦儲存媒體公司](../Category/電腦儲存媒體公司.md "wikilink")