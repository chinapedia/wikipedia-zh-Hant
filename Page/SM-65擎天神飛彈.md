[Atlas_missile_launch.jpg](https://zh.wikipedia.org/wiki/File:Atlas_missile_launch.jpg "fig:Atlas_missile_launch.jpg")
**擎天神**（Atlas）是[美國第一種服役的](../Page/美國.md "wikilink")[洲際彈道飛彈](../Page/洲際彈道飛彈.md "wikilink")，美軍編號**SM-65/CGM-16**，於1957首次发射，1959年開始部署，1965年退役。

## 歷史

擎天神飛彈的發展歷史可以回溯到1947年[美國陸軍航空軍因為經費不夠而取消的MX](../Page/美國陸軍航空軍.md "wikilink")-774飛彈計畫，當時由康維爾飛機公司進行的設計方案已經有相當的進展，因此，在當年9月正式獨立的[美國空軍允許該公司在剩餘的經費下繼續各項設計與測試工作](../Page/美國空軍.md "wikilink")。

1948年與1949年在[新墨西哥州白沙飛彈試驗場測試的結果相當良好](../Page/新墨西哥州.md "wikilink")，但是依據無法說服空軍繼續支持，因此康維爾公司決定以自有的經費繼續研發的工作，並於其後兩年投資約3百萬[美元](../Page/美元.md "wikilink")。

1951年1月美國空軍重新恢復對洲際彈道飛彈的支持態度，同時研究經費也因為[韓戰的關係而大幅成長](../Page/韓戰.md "wikilink")，此時空軍將飛彈計畫命名為MX-1593，交給康維爾公司同時進行吸氣式發動機或是彈道飛彈，可以攜帶8000磅彈頭，具備5750[英里射程與圓周平均誤差](../Page/英里.md "wikilink")（CEP）1500英尺的設計。同年6月康維爾公司正式提出擎天神飛彈設計案。

1957年試驗用的擎天神A型飛彈進行首次試射，直至1958年6月的8次試射中，有6次飛彈在發射台上或者是升空後不久爆炸，剩下兩枚飛彈成功飛行600英里。

擎天神B型是很接近量產型的第二種測試飛彈，1958年7月第一次發射後不久就發生飛彈爆炸，到了11月飛彈成功飛行6000英里的距離。第8顆飛彈編號擎天神10-B成功將**SCORE**計畫的籌載物在1958年12月發射到[地球](../Page/地球.md "wikilink")[軌道](../Page/軌道.md "wikilink")，成為全世界第一顆通訊[人造衛星](../Page/人造衛星.md "wikilink")。

直到擎天神D型於1959年宣布在范登堡空軍基地進入服役。一共有三種服役的擎天神彈道飛彈：擎天神D型、擎天神E型和擎天神F型。

## 性能諸元

|          |
| -------- |
| SM-65擎天神 |
| 生產廠商：    |
| 火箭發動機：   |
| 燃料：      |
| 彈頭：      |
| 長：       |
| 直徑：      |
| 重量：      |
| 射程：      |

## 参考文献

## 外部链接

  -
{{-}}

[da:Atlasraket](../Page/da:Atlasraket.md "wikilink") [de:Atlas
(Rakete)](../Page/de:Atlas_\(Rakete\).md "wikilink") [es:Atlas
(cohete)](../Page/es:Atlas_\(cohete\).md "wikilink") [fi:Atlas
(raketti)](../Page/fi:Atlas_\(raketti\).md "wikilink") [hu:Atlas
rakéta](../Page/hu:Atlas_rakéta.md "wikilink") [id:Atlas
(roket)](../Page/id:Atlas_\(roket\).md "wikilink") [nl:Atlas
(raket)](../Page/nl:Atlas_\(raket\).md "wikilink") [simple:Atlas
(missile)](../Page/simple:Atlas_\(missile\).md "wikilink") [sk:Atlas
(raketa)](../Page/sk:Atlas_\(raketa\).md "wikilink")

[Category:美國彈道飛彈](../Category/美國彈道飛彈.md "wikilink")