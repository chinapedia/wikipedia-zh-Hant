**超新星1604**（編號： **SN
1604**，或稱**克卜勒超新星**）是位於[銀河系內的一顆](../Page/銀河系.md "wikilink")[超新星](../Page/超新星.md "wikilink")，位置在[蛇夫座內](../Page/蛇夫座.md "wikilink")。到目前為止，SN
1604是[銀河系內最後一顆肉眼可见的](../Page/銀河系.md "wikilink")[超新星](../Page/超新星.md "wikilink")，距[地球僅](../Page/地球.md "wikilink")4,000[秒差距](../Page/秒差距.md "wikilink")（約13,000[光年](../Page/光年.md "wikilink")）。高峰時曾成為全天最亮的[恒星](../Page/恒星.md "wikilink")，也比[金星外的其他](../Page/金星.md "wikilink")[行星亮](../Page/行星.md "wikilink")，[視星等為](../Page/視星等.md "wikilink")−2.5。

[Kepler_Drawing_of_SN_1604.png](https://zh.wikipedia.org/wiki/File:Kepler_Drawing_of_SN_1604.png "fig:Kepler_Drawing_of_SN_1604.png")所繪的星圖，記錄了[超新星的位置](../Page/超新星.md "wikilink")，以字母"N"標示。（由下數起第四行，左起第四格，[蛇夫座的右腳跟](../Page/蛇夫座.md "wikilink")）\]\]

此[超新星在](../Page/超新星.md "wikilink")1604年10月9日首次被人發現\[1\]。到10月17日，[德國天文學家](../Page/德國.md "wikilink")[克卜勒發現是次現象](../Page/約翰內斯·開普勒.md "wikilink")，他深入研究後寫了一本書鉅細無遺地記錄此事，書名為《De
Stella nova in pede Serpentarii》（[蛇夫座足部的新星](../Page/蛇夫座.md "wikilink")）。

而在《[明史](../Page/明史.md "wikilink")》中對此次[超新星爆發也有記載](../Page/超新星爆發.md "wikilink")：

超新星1604是當代看見的第二次[超新星爆發](../Page/超新星爆發.md "wikilink")，前一次發生在1572年（[第谷新星](../Page/第谷新星.md "wikilink")）。此後，再無觀測到[銀河系內的](../Page/銀河系.md "wikilink")[超新星爆發](../Page/超新星爆發.md "wikilink")，但是河外的[超新星則屢見不鮮](../Page/超新星.md "wikilink")，最矚目的有[超新星1987A](../Page/超新星1987A.md "wikilink")。

超新星1604爆發後的[遺骸成為了往後發現的同類物體的原型](../Page/超新星爆炸遺骸.md "wikilink")，在四百年後的今天仍然是常常被深入研究的[天體](../Page/天體.md "wikilink")。

## 相关条目

  - [超新星列表](../Page/超新星列表.md "wikilink")
  - [SN 185](../Page/SN_185.md "wikilink")
  - [SN 1006](../Page/SN_1006.md "wikilink")
  - [SN 1054](../Page/SN_1054.md "wikilink")
  - [SN 1572](../Page/SN_1572.md "wikilink")

## 參考資料

<references />

## 外部連結

  - [SEDS有關SN1604的介紹](https://web.archive.org/web/20131017120708/http://spider.seds.org/spider/Vars/sn1604.html)

[Category:蛇夫座](../Category/蛇夫座.md "wikilink")
[Category:超新星](../Category/超新星.md "wikilink")

1.