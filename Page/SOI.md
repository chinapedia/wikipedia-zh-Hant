[Athlon_64_X2_E6_3800.jpg](https://zh.wikipedia.org/wiki/File:Athlon_64_X2_E6_3800.jpg "fig:Athlon_64_X2_E6_3800.jpg")

**SOI**全名為**Silicon On
Insulator**，是指[矽](../Page/矽.md "wikilink")[電晶體結構在](../Page/電晶體.md "wikilink")[絕緣體之上的意思](../Page/絕緣體.md "wikilink")，原理就是在矽電晶體之間，加入絕緣體物質，可使兩者之間的寄生[電容比原來的少上一倍](../Page/電容.md "wikilink")。優點是可以較易提升[時脈](../Page/時脈.md "wikilink")，並減少[電流](../Page/電流.md "wikilink")成為省電的IC，在製程上還可以省略部分[光罩以節省成本](../Page/光罩.md "wikilink")，因此不論在製程上或是電路上都有其優勢。此外，在SOI晶圓（SOI
wafer）本身基板的[阻抗值的部分也會影響到元件的表現](../Page/阻抗.md "wikilink")，因此後來也有公司在基板上進行阻抗值的調整，達到射頻元件（Radio
frequency component、RF
component）特性的提升。原本應通過交換器的[電子](../Page/電子.md "wikilink")，有些會鑽入矽中造成浪費；SOI可防止電子流失，與補強一些原本Bulk
wafer中CMOS元件的缺點。[摩托罗拉宣稱](../Page/摩托罗拉.md "wikilink")[中央處理器可因此提升時脈](../Page/中央處理器.md "wikilink")20%，並減低耗電30%。除此之外，還可以減少一些有害的電氣效應。還有一點，可以說是很多[超頻玩家所感興趣的](../Page/超頻.md "wikilink")，那就是它的工作溫度可高達300°C，減少過熱的問題。

SOI一開始是由美商[IBM公司的晶片部門投入開發](../Page/IBM.md "wikilink")，最早用於[Macintosh電腦](../Page/Macintosh.md "wikilink")（MAC）的[PowerPC
G4處理器](../Page/PowerPC_G4.md "wikilink")，除了IBM外，還有[Motorola](../Page/Motorola.md "wikilink")、[德州儀器](../Page/德州儀器.md "wikilink")（[TI](../Page/TI.md "wikilink")）、[NEC等公司投入SOI技術的開發工作](../Page/NEC.md "wikilink")，但是[Intel公司拒絕在其處理器產品中使用SOI技術](../Page/Intel.md "wikilink")，因為其認為SOI技術容易影響[晶圓品質與減低電晶體交換速度](../Page/晶圓.md "wikilink")，並且SOI上接合點也會減少，也就是一般製程中「」的缺點所煩惱。

SOI行业联盟是負責推廣SOI技術，成員包括SOI技術的發明者IBM及一些[半導體公司](../Page/半導體.md "wikilink")，例如[AMD和](../Page/AMD.md "wikilink")[NVIDIA](../Page/NVIDIA.md "wikilink")，而Intel並未加入該組織\[1\]。

## 使用SOI技術的產品

### IBM

  - Power 4
  - Power 5
  - Power 6
  - PowerPC G3
  - PowerPC 7400
  - [Power PC](../Page/PowerPC_G4.md "wikilink")

### Sony

  - [Cell](../Page/Cell.md "wikilink")

### AMD

  - [Athlon 64](../Page/Athlon_64.md "wikilink")
  - [Athlon X2](../Page/Athlon_X2.md "wikilink")
  - [Athlon 64 FX](../Page/Athlon_64_FX.md "wikilink")
  - [Opteron](../Page/Opteron.md "wikilink")
  - [Sempron](../Page/Sempron.md "wikilink")
  - [Phenom](../Page/Phenom.md "wikilink")
  - [Phenom II](../Page/Phenom_II.md "wikilink")
  - [Turion 64](../Page/Turion_64.md "wikilink")
  - [Turion 64 X2](../Page/Turion_64_X2.md "wikilink")
  - [Turion Ultra](../Page/Turion_Ultra.md "wikilink")
  - [AMD Bulldozer](../Page/AMD_Bulldozer.md "wikilink")

### VIA

  - [C7](../Page/VIAC7.md "wikilink")
  - [C7-M](../Page/C7-M.md "wikilink")
  - [VIA C7-D](../Page/VIA_C7-D.md "wikilink")

## 參考文獻

  - 腳註

<!-- end list -->

  - 引用

[S](../Category/半导体器件制造.md "wikilink")
[Category:半導體結構](../Category/半導體結構.md "wikilink")
[Category:微技術](../Category/微技術.md "wikilink")

1.  [NVIDIA加入SOI绝缘硅技术行业联盟](http://news.mydrivers.com/1/111/111417.htm)