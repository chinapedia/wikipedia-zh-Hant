**STAR
M-{o}-vies**（[臺灣作](../Page/臺灣.md "wikilink")「-{zh;zh-hant;zh-hans|衛視西片台}-」，[香港](../Page/香港.md "wikilink")、[大陸作](../Page/大陸.md "wikilink")「-{zh;zh-hant;zh-hans|衛視國際電影台}-」\[1\]）是[星空傳媒及](../Page/星空傳媒.md "wikilink")[华特迪士尼公司](../Page/华特迪士尼公司.md "wikilink")）的[亞洲](../Page/亞洲.md "wikilink")[電影頻道](../Page/電影.md "wikilink")，於[亞洲](../Page/亞洲.md "wikilink")[地區播放](../Page/地區.md "wikilink")。STAR
M-{o}-vies於2012年1月1日在印度、中國大陸、越南、中東及台灣版本之外更名为[FOX
Movies](../Page/FOX_Movies.md "wikilink")（于2019年全数转至[福斯公司](../Page/福斯公司.md "wikilink")）。《[英文中國郵報](../Page/英文中國郵報.md "wikilink")》的[電視](../Page/電視.md "wikilink")[節目表標示的](../Page/節目表.md "wikilink")[頻道名稱為](../Page/頻道.md "wikilink")「Star
international」\[2\]。

## 版本

## 台徽變遷

## 參考來源

## 外部連結

  -
  - [STAR M-{o}-vies（印度）](http://www.starmovies.in)

  - [STAR M-{o}-vies-STAR
    Select](http://www.starselect.com/channels/movies.html)

[Category:1998年成立的电视台或电视频道](../Category/1998年成立的电视台或电视频道.md "wikilink")
[Category:東南亞電視台](../Category/東南亞電視台.md "wikilink")
[Category:台灣電視播放頻道](../Category/台灣電視播放頻道.md "wikilink")
[Category:星空傳媒](../Category/星空傳媒.md "wikilink")
[Category:電影頻道](../Category/電影頻道.md "wikilink")

1.  [卫视-{}-国际电影台 | Star Movies International | 福斯国际电视网 | Fox
    International Channels](http://www.ficchina.cn/smi.aspx)
2.  《[英文中國郵報](../Page/英文中國郵報.md "wikilink")》，2013年6月10日 TV Guide版