****是历史上第九十五次航天飞机任务，也是[发现号航天飞机的第二十七次太空飞行](../Page/發現號太空梭.md "wikilink")。

## 任务成员

  - **[柯提斯·布朗](../Page/柯提斯·布朗.md "wikilink")**（，曾执行、、、、以及任务），指令长
  - **[斯科特·凯利](../Page/斯科特·凯利.md "wikilink")**（，曾执行以及任务），飞行员
  - **[斯蒂文·史密斯](../Page/斯蒂文·史密斯.md "wikilink")**（，曾执行、、以及任务），任务专家
  - **[迈克尔·福奥勒](../Page/迈克尔·福奥勒.md "wikilink")**（，曾执行、、、、以及[远征8号任务](../Page/远征8号.md "wikilink")），任务专家
  - **[约翰·格伦斯菲尔德](../Page/约翰·格伦斯菲尔德.md "wikilink")**（，曾执行、、以及任务），任务专家
  - **[克劳德·尼克列](../Page/克劳德·尼克列.md "wikilink")**（，[瑞士宇航员](../Page/瑞士.md "wikilink")，曾执行、、以及任务），任务专家
  - **[让-弗朗西斯·格列福瑞](../Page/让-弗朗西斯·格列福瑞.md "wikilink")**（，[法国宇航员](../Page/法国.md "wikilink")，曾执行、以及任务），任务专家

[Category:1999年美国](../Category/1999年美国.md "wikilink")
[Category:1999年科學](../Category/1999年科學.md "wikilink")
[Category:发现号航天飞机任务](../Category/发现号航天飞机任务.md "wikilink")
[Category:1999年12月](../Category/1999年12月.md "wikilink")