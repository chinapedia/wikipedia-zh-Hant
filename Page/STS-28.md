****是历史上第三十次航天飞机任务，也是[哥伦比亚号航天飞机的第八次太空飞行](../Page/哥伦比亚号航天飞机.md "wikilink")。

## 任务成员

  - **[布鲁斯特·肖](../Page/布鲁斯特·肖.md "wikilink")**（，曾执行、以及任务），指令长
  - **[理查德·理查兹](../Page/理查德·理查兹.md "wikilink")**（，曾执行、、以及任务），飞行员
  - **[詹姆斯·亚当森](../Page/詹姆斯·亚当森.md "wikilink")**（，曾执行以及任务），任务专家
  - **[大卫·里茨马](../Page/大卫·里茨马.md "wikilink")**（，曾执行、以及任务），任务专家
  - **[马克·布朗](../Page/马克·布朗.md "wikilink")**（，曾执行以及任务），任务专家

[STS28ByPhilKonstantin.jpg](https://zh.wikipedia.org/wiki/File:STS28ByPhilKonstantin.jpg "fig:STS28ByPhilKonstantin.jpg")

[Category:1989年佛罗里达州](../Category/1989年佛罗里达州.md "wikilink")
[Category:哥伦比亚号航天飞机任务](../Category/哥伦比亚号航天飞机任务.md "wikilink")
[Category:1989年科学](../Category/1989年科学.md "wikilink")
[Category:1989年8月](../Category/1989年8月.md "wikilink")