****是历史上第三次航天飞机任务，也是[哥伦比亚号航天飞机的第三次太空飞行](../Page/哥伦比亚号航天飞机.md "wikilink")。

## 任务成员

  - **[杰克·洛斯马](../Page/杰克·洛斯马.md "wikilink")**（，曾执行[天空实验室3号以及](../Page/天空实验室3号.md "wikilink")任务），指令长
  - **[戈尔登·福勒顿](../Page/戈尔登·福勒顿.md "wikilink")**（，曾执行以及任务），飞行员

### 替补成员

<small>替补成员同样接受任务训练，在主力成员因各种原因无法执行任务时接替。</small>
<small>的替补团队执行了任务。</small>

  - **[肯·马丁利](../Page/肯·马丁利.md "wikilink")**（，曾执行[阿波罗16号](../Page/阿波罗16号.md "wikilink")、以及任务），指令长
  - **[亨利·哈特斯菲尔德](../Page/亨利·哈特斯菲尔德.md "wikilink")**（，曾执行、以及任务），飞行员

[Category:1982年佛罗里达州](../Category/1982年佛罗里达州.md "wikilink")
[Category:1982年科学](../Category/1982年科学.md "wikilink")
[Category:哥伦比亚号航天飞机任务](../Category/哥伦比亚号航天飞机任务.md "wikilink")
[Category:1982年3月](../Category/1982年3月.md "wikilink")