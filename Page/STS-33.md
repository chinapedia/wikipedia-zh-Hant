****是历史上第三十二次航天飞机任务，也是[发现号航天飞机的第九次太空飞行](../Page/發現號太空梭.md "wikilink")。

## 任务成员

  - **[弗雷德里克·格里高利](../Page/弗雷德里克·格里高利.md "wikilink")**（，曾执行、以及任务），指令长
  - **[约翰·布拉哈](../Page/约翰·布拉哈.md "wikilink")**（，曾执行、、、以及任务），飞行员
  - **[斯多里·马斯格雷夫](../Page/斯多里·马斯格雷夫.md "wikilink")**（，曾执行、、、、以及任务），任务专家
  - **[索尼·卡特](../Page/索尼·卡特.md "wikilink")**（，曾执行任务），任务专家
  - **[凯瑟琳·索恩顿](../Page/凯瑟琳·索恩顿.md "wikilink")**（，曾执行、、以及任务），任务专家

[STS33ByPhilKonstantin.jpg](https://zh.wikipedia.org/wiki/File:STS33ByPhilKonstantin.jpg "fig:STS33ByPhilKonstantin.jpg")

[Category:1989年科学](../Category/1989年科学.md "wikilink")
[Category:1989年佛罗里达州](../Category/1989年佛罗里达州.md "wikilink")
[Category:发现号航天飞机任务](../Category/发现号航天飞机任务.md "wikilink")
[Category:1989年11月](../Category/1989年11月.md "wikilink")