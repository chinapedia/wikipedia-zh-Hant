****是历史上第四十一次航天飞机任务，也是[哥伦比亚号航天飞机的第十一次太空飞行](../Page/哥伦比亚号航天飞机.md "wikilink")。

## 任务成员

  - **[布莱恩·欧康诺尔](../Page/布莱恩·欧康诺尔.md "wikilink")**（，曾执行以及任务），指令长
  - **[西德尼·古蒂雷兹](../Page/西德尼·古蒂雷兹.md "wikilink")**（，曾执行以及任务），飞行员
  - **[詹姆斯·巴吉安](../Page/詹姆斯·巴吉安.md "wikilink")**（，曾执行以及任务），任务专家
  - **[塔玛拉·杰尼根](../Page/塔玛拉·杰尼根.md "wikilink")**（，曾执行、、、以及任务），任务专家
  - **[里娅·塞顿](../Page/里娅·塞顿.md "wikilink")**（，曾执行、以及任务），任务专家
  - **[德鲁·加弗尼](../Page/德鲁·加弗尼.md "wikilink")**（，曾执行任务），任务专家
  - **[米莉·休斯-弗尔福德](../Page/米莉·休斯-弗尔福德.md "wikilink")**（，曾执行任务），任务专家

[Category:1991年佛罗里达州](../Category/1991年佛罗里达州.md "wikilink")
[Category:1991年科學](../Category/1991年科學.md "wikilink")
[Category:哥伦比亚号航天飞机任务](../Category/哥伦比亚号航天飞机任务.md "wikilink")
[Category:1991年6月](../Category/1991年6月.md "wikilink")