****是历史上第四十三次航天飞机任务，也是[发现号航天飞机的第十三次太空飞行](../Page/發現號太空梭.md "wikilink")。此次任务的主要载重是[高层大气研究卫星](../Page/高层大气研究卫星.md "wikilink")（UARS）。

## 任务成员

  - **[约翰·克雷顿](../Page/约翰·克雷顿.md "wikilink")**（，曾执行、以及任务），指令长
  - **[肯内斯·雷特勒](../Page/肯内斯·雷特勒.md "wikilink")**（，曾执行以及任务），飞行员
  - **[詹姆斯·布克利](../Page/詹姆斯·布克利.md "wikilink")**（，曾执行、、以及任务），任务专家
  - **[查尔斯·格马](../Page/查尔斯·格马.md "wikilink")**（，曾执行、以及任务），任务专家
  - **[马克·布朗](../Page/马克·布朗.md "wikilink")**（，曾执行以及任务），任务专家

[Upperatmosphereresearchsatellite.jpg](https://zh.wikipedia.org/wiki/File:Upperatmosphereresearchsatellite.jpg "fig:Upperatmosphereresearchsatellite.jpg")

[Category:1991年佛罗里达州](../Category/1991年佛罗里达州.md "wikilink")
[Category:1991年科學](../Category/1991年科學.md "wikilink")
[Category:发现号航天飞机任务](../Category/发现号航天飞机任务.md "wikilink")
[Category:1991年9月](../Category/1991年9月.md "wikilink")