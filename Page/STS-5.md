****是历史上第五次航天飞机任务，也是[哥伦比亚号航天飞机的第五次太空飞行](../Page/哥伦比亚号航天飞机.md "wikilink")。

## 任务成员

  - **[文斯·布兰德](../Page/文斯·布兰德.md "wikilink")**（，曾执行[阿波罗-联盟测试计划](../Page/阿波罗-联盟测试计划.md "wikilink")、、以及任务），指令长
  - **[罗伯特·欧沃米尔](../Page/罗伯特·欧沃米尔.md "wikilink")**（，曾执行以及任务），飞行员
  - **[约瑟夫·艾伦](../Page/约瑟夫·艾伦.md "wikilink")**（，曾执行以及任务），任务专家
  - **[威廉·勒诺](../Page/威廉·勒诺.md "wikilink")**（，曾执行任务），任务专家

[Category:1982年佛罗里达州](../Category/1982年佛罗里达州.md "wikilink")
[Category:1982年科学](../Category/1982年科学.md "wikilink")
[Category:哥伦比亚号航天飞机任务](../Category/哥伦比亚号航天飞机任务.md "wikilink")
[Category:1982年11月](../Category/1982年11月.md "wikilink")