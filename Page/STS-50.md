****是历史上第四十八次航天飞机任务，也是[哥伦比亚号航天飞机的第十二次太空飞行](../Page/哥倫比亞號太空梭.md "wikilink")。

## 任务成员

  - **[理查德·理查兹](../Page/理查德·理查兹.md "wikilink")**（，曾执行、、以及任务），指令长
  - **[肯内斯·鲍威索克斯](../Page/肯内斯·鲍威索克斯.md "wikilink")**（，曾执行、、、、、以及任务），飞行员
  - **[波妮·唐巴尔](../Page/波妮·唐巴尔.md "wikilink")**（，曾执行、、、以及任务），任务专家
  - **[埃伦·贝克](../Page/埃伦·贝克.md "wikilink")**（，曾执行、以及任务），任务专家
  - **[卡尔·米德](../Page/卡尔·米德.md "wikilink")**（，曾执行、以及任务），任务专家
  - **[劳伦斯·德卢卡斯](../Page/劳伦斯·德卢卡斯.md "wikilink")**（，曾执行任务），有效载荷专家
  - **[尤金·郑](../Page/尤金·郑.md "wikilink")**（，曾执行任务），有效载荷专家

### 替补有效载荷专家

  - **[约瑟夫·普拉](../Page/约瑟夫·普拉.md "wikilink")**（）
  - **[迈克尔·兰普顿](../Page/迈克尔·兰普顿.md "wikilink")**（，曾执行任务）

[Category:1992年佛罗里达州](../Category/1992年佛罗里达州.md "wikilink")
[Category:哥伦比亚号航天飞机任务](../Category/哥伦比亚号航天飞机任务.md "wikilink")
[Category:1992年科學](../Category/1992年科學.md "wikilink")
[Category:1992年6月](../Category/1992年6月.md "wikilink")
[Category:1992年7月](../Category/1992年7月.md "wikilink")