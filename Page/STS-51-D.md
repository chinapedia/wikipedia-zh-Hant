****是历史上第十六次航天飞机任务，也是[发现号的第四次太空飞行](../Page/發現號太空梭.md "wikilink")。

## 任务成员

  - **[卡罗尔·鲍勃科](../Page/卡罗尔·鲍勃科.md "wikilink")**（，曾执行、以及任务），指令长
  - **[唐纳德·威廉姆斯](../Page/唐纳德·威廉姆斯.md "wikilink")**（，曾执行以及任务），飞行员
  - **[里娅·塞顿](../Page/里娅·塞顿.md "wikilink")**（，曾执行、以及任务），任务专家
  - **[杰弗利·霍夫曼](../Page/杰弗利·霍夫曼.md "wikilink")**（，曾执行、、、以及任务），任务专家
  - **[大卫·格里格斯](../Page/大卫·格里格斯.md "wikilink")**（，曾执行任务），任务专家
  - **[查尔斯·沃克](../Page/查尔斯·沃克.md "wikilink")**（，曾执行、以及任务），有效载荷专家
  - **[杰克·加恩](../Page/杰克·加恩.md "wikilink")**（，曾执行任务），有效载荷专家

[Category:1985年科學](../Category/1985年科學.md "wikilink")
[Category:1985年佛罗里达州](../Category/1985年佛罗里达州.md "wikilink")
[Category:发现号航天飞机任务](../Category/发现号航天飞机任务.md "wikilink")
[Category:1985年4月](../Category/1985年4月.md "wikilink")