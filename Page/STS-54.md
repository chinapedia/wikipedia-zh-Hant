****是历史上第五十二次航天飞机任务，也是[奋进号航天飞机的第三次太空飞行](../Page/奮進號太空梭.md "wikilink")。

## 任务成员

  - ，曾执行[STS-36](../Page/STS-36.md "wikilink")、、[STS-62以及](../Page/STS-62.md "wikilink")[STS-77任务](../Page/STS-77.md "wikilink")），指令长

  - ，曾执行[STS-39](../Page/STS-39.md "wikilink")、以及[STS-66任务](../Page/STS-66.md "wikilink")），飞行员

  - ，曾执行[STS-44](../Page/STS-44.md "wikilink")、以及[STS-77任务](../Page/STS-77.md "wikilink")），任务专家

  - ，曾执行[STS-39](../Page/STS-39.md "wikilink")、、[STS-71以及](../Page/STS-71.md "wikilink")[STS-82任务](../Page/STS-82.md "wikilink")），任务专家

  - ，曾执行、[STS-64](../Page/STS-64.md "wikilink")、[STS-78](../Page/STS-78.md "wikilink")、[STS-101](../Page/STS-101.md "wikilink")、、以及任务），任务专家

[Category:1993年佛罗里达州](../Category/1993年佛罗里达州.md "wikilink")
[Category:奋进号航天飞机任务](../Category/奋进号航天飞机任务.md "wikilink")
[Category:1993年科學](../Category/1993年科學.md "wikilink")
[Category:1993年1月](../Category/1993年1月.md "wikilink")