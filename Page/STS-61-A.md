****是历史上第二十二次航天飞机任务，也是[-{zh-hans:挑战者号航天飞机;
zh-hant:挑戰者號太空梭;}-的第九次太空飞行](../Page/挑戰者號太空梭.md "wikilink")。

## 任务成员

  - **[亨利·哈特斯菲尔德](../Page/亨利·哈特斯菲尔德.md "wikilink")**（，曾执行、、任务），指令长
  - **[斯蒂芬·纳格尔](../Page/斯蒂芬·纳格尔.md "wikilink")**（，曾执行、、以及任务），飞行员
  - **[波妮·唐巴尔](../Page/波妮·唐巴尔.md "wikilink")**（，曾执行、、、以及任务），任务专家
  - **[詹姆斯·布克利](../Page/詹姆斯·布克利.md "wikilink")**（，曾执行、、以及任务），任务专家
  - **[圭恩·布鲁福德](../Page/圭恩·布鲁福德.md "wikilink")**（，曾执行、、以及任务），任务专家
  - **[雷恩哈德·弗瑞尔](../Page/雷恩哈德·弗瑞尔.md "wikilink")**（，[德国宇航员](../Page/德国.md "wikilink")，曾执行任务），有效载荷专家
  - **[恩斯特·梅瑟施米德](../Page/恩斯特·梅瑟施米德.md "wikilink")**（，[德国宇航员](../Page/德国.md "wikilink")，曾执行任务），有效载荷专家
  - **[乌波·欧克斯](../Page/乌波·欧克斯.md "wikilink")**（，[荷兰宇航员](../Page/荷兰.md "wikilink")，曾执行任务），有效载荷专家

## 替补有效载荷专家

  - **[乌尔夫·默博尔德](../Page/乌尔夫·默博尔德.md "wikilink")**（，[西德宇航员](../Page/西德.md "wikilink")，曾执行、以及任务）

[Category:1985年佛罗里达州](../Category/1985年佛罗里达州.md "wikilink")
[Category:1985年科學](../Category/1985年科學.md "wikilink")
[Category:挑战者号航天飞机任务](../Category/挑战者号航天飞机任务.md "wikilink")
[Category:1985年10月](../Category/1985年10月.md "wikilink")
[Category:1985年11月](../Category/1985年11月.md "wikilink")