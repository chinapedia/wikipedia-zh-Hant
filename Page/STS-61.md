****是历史上第五十八次航天飞机任务，也是[奋进号航天飞机的第五次太空飞行](../Page/奮進號太空梭.md "wikilink")。

## 任务成员

  - **[理查德·科维](../Page/理查德·科维.md "wikilink")**（，曾执行、、以及任务），指令长
  - **[肯内斯·鲍威索克斯](../Page/肯内斯·鲍威索克斯.md "wikilink")**（，曾执行、、、、、以及任务），飞行员
  - **[斯多里·马斯格雷夫](../Page/斯多里·马斯格雷夫.md "wikilink")**（，曾执行、、、、以及任务），有效载荷指令长
  - **[凯瑟琳·索恩顿](../Page/凯瑟琳·索恩顿.md "wikilink")**（，曾执行、、以及任务），任务专家
  - **[克劳德·尼克列](../Page/克劳德·尼克列.md "wikilink")**（，[瑞士宇航员](../Page/瑞士.md "wikilink")，曾执行、、以及任务），任务专家
  - **[杰弗利·霍夫曼](../Page/杰弗利·霍夫曼.md "wikilink")**（，曾执行、、、以及任务），任务专家
  - **[托马斯·埃克斯](../Page/托马斯·埃克斯.md "wikilink")**（，曾执行、、以及任务），任务专家

[Category:1993年佛罗里达州](../Category/1993年佛罗里达州.md "wikilink")
[Category:奋进号航天飞机任务](../Category/奋进号航天飞机任务.md "wikilink")
[Category:1993年科學](../Category/1993年科學.md "wikilink")
[Category:1993年12月](../Category/1993年12月.md "wikilink")