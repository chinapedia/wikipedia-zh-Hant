****是历史上第六十六次航天飞机任务，也是[发现号航天飞机的第二十次太空飞行](../Page/發現號太空梭.md "wikilink")。

## 任务成员

  - **[吉姆·威瑟比](../Page/吉姆·威瑟比.md "wikilink")**（，曾执行、、、、以及任务），指令长
  - **[艾琳·科林斯](../Page/艾琳·科林斯.md "wikilink")**（，曾执行、、以及任务），飞行员
  - **[迈克尔·福奥勒](../Page/迈克尔·福奥勒.md "wikilink")**（，曾执行、、、、以及[远征8号任务](../Page/远征8号.md "wikilink")），任务专家
  - **[简妮丝·沃斯](../Page/简妮丝·沃斯.md "wikilink")**（，曾执行、、、以及任务），任务专家
  - **[伯纳德·哈里斯](../Page/伯纳德·哈里斯.md "wikilink")**（，曾执行以及任务），任务专家
  - **[弗拉基米尔·蒂托夫](../Page/弗拉基米尔·蒂托夫.md "wikilink")**（，[俄罗斯宇航员](../Page/俄罗斯.md "wikilink")，曾执行、、、[和平号空间站](../Page/和平號太空站.md "wikilink")、、以及任务）

[Category:1995年科学](../Category/1995年科学.md "wikilink")
[Category:发现号航天飞机任务](../Category/发现号航天飞机任务.md "wikilink")
[Category:1995年美国](../Category/1995年美国.md "wikilink")
[Category:1995年2月](../Category/1995年2月.md "wikilink")