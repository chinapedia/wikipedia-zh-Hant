****是历史上第六十四次航天飞机任务，也是[奋进号航天飞机的第七次太空飞行](../Page/奮進號太空梭.md "wikilink")。

## 任务成员

  - **[迈克尔·贝克](../Page/迈克尔·贝克.md "wikilink")**（，曾执行、、以及任务），指令长
  - **[泰伦斯·威尔卡特](../Page/泰伦斯·威尔卡特.md "wikilink")**（，曾执行、、以及任务），飞行员
  - **[托马斯·琼斯](../Page/托马斯·琼斯.md "wikilink")**（，曾执行、、以及任务），有效载荷指令长
  - **[斯蒂文·史密斯](../Page/斯蒂文·史密斯.md "wikilink")**（，曾执行、、以及任务），任务专家
  - **[丹尼尔·博斯奇](../Page/丹尼尔·博斯奇.md "wikilink")**（，曾执行、、、、以及任务），任务专家
  - **[彼得·维索夫](../Page/彼得·维索夫.md "wikilink")**（，曾执行、、以及任务），任务专家

[Category:1994年佛罗里达州](../Category/1994年佛罗里达州.md "wikilink")
[Category:奋进号航天飞机任务](../Category/奋进号航天飞机任务.md "wikilink")
[Category:1994年科学](../Category/1994年科学.md "wikilink")
[Category:1994年9月](../Category/1994年9月.md "wikilink")
[Category:1994年10月](../Category/1994年10月.md "wikilink")