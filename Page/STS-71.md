****是历史上第六十九次[航天飞机任务](../Page/航天飞机.md "wikilink")，也是[亚特兰蒂斯号航天飞机的第十四次太空飞行](../Page/亞特蘭提斯號太空梭.md "wikilink")。

## 任务成员

  - **[罗伯特·吉布森](../Page/罗伯特·吉布森.md "wikilink")**（，曾执行、、、以及任务），指令长
  - **[查尔斯·普里克特](../Page/查尔斯·普里克特.md "wikilink")**（，曾执行、、以及任务），飞行员
  - **[埃伦·贝克](../Page/埃伦·贝克.md "wikilink")**（，曾执行、以及任务），任务专家
  - **[波妮·唐巴尔](../Page/波妮·唐巴尔.md "wikilink")**（，曾执行、、、以及任务），任务专家
  - **[格里高利·哈巴](../Page/格里高利·哈巴.md "wikilink")**（，曾执行、、以及任务），任务专家

### 发射：和平号19成员

  - **[阿纳托利·索罗耶夫](../Page/阿纳托利·索罗耶夫.md "wikilink")**（）
  - **[尼古拉·布达林](../Page/尼古拉·布达林.md "wikilink")**（）

### 返回：和平号18成员

  - **[诺曼·萨加德](../Page/诺曼·萨加德.md "wikilink")**（，曾执行、、、、以及任务）
  - **[弗拉基米尔·德朱罗夫](../Page/弗拉基米尔·德朱罗夫.md "wikilink")**（Vladimir
    Dezhurov）
  - **[杰纳迪·斯特雷卡罗夫](../Page/杰纳迪·斯特雷卡罗夫.md "wikilink")**（Gennady
    Strekalov）

[Category:载人航天](../Category/载人航天.md "wikilink")
[Category:航天飞机任务](../Category/航天飞机任务.md "wikilink")
[Category:1995年6月](../Category/1995年6月.md "wikilink")
[Category:1995年7月](../Category/1995年7月.md "wikilink")