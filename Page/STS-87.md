****是历史上第87次航天飞机任务，也是[哥伦比亚号航天飞机的第](../Page/哥倫比亞號太空梭.md "wikilink")21次太空飞行。

## 任务成员

  - **[凯文·克雷格](../Page/凯文·克雷格.md "wikilink")**（，曾执行、、、任务），指令长
  - **[斯蒂文·林赛](../Page/斯蒂文·林赛.md "wikilink")**（，曾执行、、以及任务），飞行员
  - **[温斯顿·斯科特](../Page/温斯顿·斯科特.md "wikilink")**（，曾执行以及任务），任务专家
  - **[卡尔帕娜·乔拉](../Page/卡尔帕娜·乔拉.md "wikilink")**（，曾执行以及任务），任务专家
  - **[土井隆雄](../Page/土井隆雄.md "wikilink")**（日本宇航员，曾执行以及任务），任务专家
  - **[里奥尼德·卡德纽科](../Page/里奥尼德·卡德纽科.md "wikilink")**（，[乌克兰宇航员](../Page/乌克兰.md "wikilink")，曾执行任务），有效载荷专家

[Category:1997年佛罗里达州](../Category/1997年佛罗里达州.md "wikilink")
[Category:1997年科學](../Category/1997年科學.md "wikilink")
[Category:哥伦比亚号航天飞机任务](../Category/哥伦比亚号航天飞机任务.md "wikilink")
[Category:1997年11月](../Category/1997年11月.md "wikilink")
[Category:1997年12月](../Category/1997年12月.md "wikilink")