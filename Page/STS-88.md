****是历史上第九十二次航天飞机任务，也是[奋进号航天飞机的第十三次太空飞行](../Page/奮進號太空梭.md "wikilink")。

## 任务成员

  - **[罗伯特·卡巴纳](../Page/罗伯特·卡巴纳.md "wikilink")**（，曾执行、、以及任务），指令长
  - **[弗雷德里克·斯托考](../Page/弗雷德里克·斯托考.md "wikilink")**（，曾执行、以及任务），飞行员
  - **[南希·居里](../Page/南希·居里.md "wikilink")**（，曾执行、、以及任务），任务专家
  - **[杰瑞·罗斯](../Page/杰瑞·罗斯.md "wikilink")**（，曾执行、、、、、以及任务），任务专家
  - **[詹姆斯·纽曼](../Page/詹姆斯·纽曼.md "wikilink")**（，曾执行、、以及任务），任务专家
  - **[谢尔盖·克里卡列夫](../Page/谢尔盖·克里卡列夫.md "wikilink")**（，[俄罗斯宇航员](../Page/俄罗斯.md "wikilink")，曾执行[联盟TM-7](../Page/联盟TM-7.md "wikilink")、[和平号EO-4](../Page/和平號太空站.md "wikilink")、[联盟TM-12](../Page/联盟TM-12.md "wikilink")、[和平号LD-3](../Page/和平號太空站.md "wikilink")、、、[联盟TM-31](../Page/联盟TM-31.md "wikilink")、[远征1号](../Page/远征1号.md "wikilink")、、[联盟TMA-6以及](../Page/联盟TMA-6.md "wikilink")[远征11号任务](../Page/远征11号.md "wikilink")），任务专家

[Category:1998年佛罗里达州](../Category/1998年佛罗里达州.md "wikilink")
[Category:奋进号航天飞机任务](../Category/奋进号航天飞机任务.md "wikilink")
[Category:1998年科學](../Category/1998年科學.md "wikilink")
[Category:1998年12月](../Category/1998年12月.md "wikilink")