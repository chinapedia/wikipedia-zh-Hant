****是历史上第九次航天飞机任务，也是[哥伦比亚号的第六次太空飞行](../Page/哥伦比亚号航天飞机.md "wikilink")。

## 任务成员

  - **[约翰·杨](../Page/约翰·杨.md "wikilink")**（，曾执行[双子星3号](../Page/双子星3号.md "wikilink")、[双子星10号](../Page/双子星10号.md "wikilink")、[阿波罗10号](../Page/阿波罗10号.md "wikilink")、[阿波罗16号](../Page/阿波罗16号.md "wikilink")、以及任务），指令长
  - **[布鲁斯特·肖](../Page/布鲁斯特·肖.md "wikilink")**（，曾执行、以及任务），飞行员
  - **[欧文·加里欧特](../Page/欧文·加里欧特.md "wikilink")**（，曾执行[天空实验室3号以及](../Page/天空实验室3号.md "wikilink")任务），任务专家
  - **[罗伯特·帕克](../Page/罗伯特·帕克.md "wikilink")**（，曾执行以及任务），任务专家
  - **[乌尔夫·默博尔德](../Page/乌尔夫·默博尔德.md "wikilink")**（，[西德宇航员](../Page/西德.md "wikilink")，曾执行、以及任务），有效载荷专家
  - **[拜伦·利希滕贝格](../Page/拜伦·利希滕贝格.md "wikilink")**（，曾执行以及任务），有效载荷专家

### 替补有效载荷专家

  - **[乌波·欧克斯](../Page/乌波·欧克斯.md "wikilink")**（，[荷兰宇航员](../Page/荷兰.md "wikilink")，曾执行任务）
  - **[迈克尔·兰普顿](../Page/迈克尔·兰普顿.md "wikilink")**（，曾执行任务）

[Category:1983年佛罗里达州](../Category/1983年佛罗里达州.md "wikilink")
[Category:哥伦比亚号航天飞机任务](../Category/哥伦比亚号航天飞机任务.md "wikilink")
[Category:1983年科学](../Category/1983年科学.md "wikilink")
[Category:1983年11月](../Category/1983年11月.md "wikilink")
[Category:1983年12月](../Category/1983年12月.md "wikilink")