****是历史上第九十一次航天飞机任务，也是[发现号航天飞机的第二十五次太空飞行](../Page/發現號太空梭.md "wikilink")。

## 任务成员

  - **[柯提斯·布朗](../Page/柯提斯·布朗.md "wikilink")**（，曾执行、、、、以及任务），指令长
  - **[斯蒂文·林赛](../Page/斯蒂文·林赛.md "wikilink")**（，曾执行、、以及任务），飞行员
  - **[斯科特·帕拉金斯基](../Page/斯科特·帕拉金斯基.md "wikilink")**（，曾执行、、以及任务），任务专家
  - **[斯蒂芬·罗宾逊](../Page/斯蒂芬·罗宾逊.md "wikilink")**（，曾执行、以及任务），任务专家
  - **[佩德罗·杜克](../Page/佩德罗·杜克.md "wikilink")**（，[西班牙宇航员](../Page/西班牙.md "wikilink")，曾执行、[联盟TMA-3以及](../Page/联盟TMA-3.md "wikilink")[联盟TMA-2任务](../Page/联盟TMA-2.md "wikilink")），任务专家
  - **[向井千秋](../Page/向井千秋.md "wikilink")**（，日本宇航员，曾执行以及任务），有效载荷专家
  - **[约翰·格伦](../Page/约翰·格伦.md "wikilink")**（，曾执行[水星-大力神6号以及](../Page/水星-大力神6号.md "wikilink")任务），有效载荷专家

[Category:1998年美国](../Category/1998年美国.md "wikilink")
[Category:1998年科學](../Category/1998年科學.md "wikilink")
[Category:发现号航天飞机任务](../Category/发现号航天飞机任务.md "wikilink")
[Category:1998年10月](../Category/1998年10月.md "wikilink")
[Category:1998年11月](../Category/1998年11月.md "wikilink")