**SUNDAY**（1997年－2007年，[港交所前上市代號](../Page/香港聯合交易所.md "wikilink")：0866，[納斯達克前上市代號](../Page/納斯達克.md "wikilink")：SDAY）為[香港一間](../Page/香港.md "wikilink")[電訊公司](../Page/電訊.md "wikilink")，成立於1997年，是香港提供[2G及](../Page/2G.md "wikilink")[3G服務的電訊公司之一](../Page/3G.md "wikilink")。公司在[開曼群島註冊](../Page/開曼群島.md "wikilink")，總部設於[鰂魚涌](../Page/鰂魚涌.md "wikilink")[太古坊](../Page/太古坊.md "wikilink")[和域大廈](../Page/和域大廈.md "wikilink")，2000年3月16日在香港聯合交易所及3月15日在美國納斯達克市場上市。

2006年12月15日起，公司在香港交易所和納斯達克停止交易，並把所有資產賣給[電訊盈科](../Page/電訊盈科.md "wikilink")。2006年12月19日SUNDAY由[開曼群島遷冊](../Page/開曼群島.md "wikilink")[英屬處女群島](../Page/英屬處女群島.md "wikilink")，並在2007年1月被清盤。清盤前SUNDAY的主要股東是電訊盈科（79.35%）及[華為技術投資有限公司](../Page/華為公司.md "wikilink")（9.91%）。SUNDAY品牌已於2007年3月26日正式易名為[PCCW
Mobile](../Page/PCCW_Mobile.md "wikilink") 2G。

## 背景

SUNDAY創辦人、前聯席主席[衞斯文在香港從事電訊業多年](../Page/衞斯文.md "wikilink")。1984年，他與[和記黃埔及](../Page/和記黃埔.md "wikilink")[摩托羅拉合夥成立和記電話](../Page/摩托羅拉.md "wikilink")，即今天的[3](../Page/3_\(電訊\).md "wikilink")；他出任該合營公司的董事總經理。1993年，衛斯文離開和記，並與同年辭職的和黃前董事總經理[馬世民創辦私人流動電訊組合公司](../Page/馬世民.md "wikilink")**Distacom**。隨後引入[麗新集團](../Page/麗新集團.md "wikilink")[林建岳](../Page/林建岳.md "wikilink")、[富聯國際](../Page/富聯國際.md "wikilink")[鄭維新及](../Page/鄭維新.md "wikilink")[中旅國際等為股東](../Page/香港中旅國際投資.md "wikilink")，成立**匯亞電訊**，1996年取得香港六個[個人通訊服務牌照之一](../Page/個人通訊服務.md "wikilink")\[1\]；1997年匯亞易名為**SUNDAY**，推出流動電話服務。衞斯文與[鄭維新共同擔任SUNDAY聯席主席](../Page/鄭維新.md "wikilink")。

## 發展

SUNDAY於1997年開始營運，初期經營[GSM](../Page/GSM.md "wikilink")1800流動電話業務，其後推出[IDD](../Page/IDD.md "wikilink")、[ISP](../Page/ISP.md "wikilink")、[WAP服務](../Page/WAP.md "wikilink")。2000年初，該公司採用WAP和[GPRS技術](../Page/GPRS.md "wikilink")，推出sunday.com及SO
WAP。2001年，SUNDAY以最低價$10,000.01投得3G牌照，但缺乏資金發展，未能推出服務。在2005年5月SUNDAY宣布推出3G服务的一周内，電盈宣布入主SUNDAY。由于電盈的介入，SUNDAY实力大增，其整合后的PCCW
Mobile于2006年1月推出爲期六個月的“3G優先試”活動，进行3G业务推广。在2006年年中正式開始3G用戶商業推廣活動。

### 上市

SUNDAY在[香港電訊市場份額一向不大](../Page/香港電訊.md "wikilink")。1999年末、2000年初，[TOM.COM在港掀起](../Page/TOM.COM.md "wikilink")[科網股熱潮](../Page/科網股.md "wikilink")，同樣具新科技概念的SUNDAY雖然未符合三年盈利的上市規定，但它以基建公司類別向聯交所申請豁免獲批，成功借科網股之勢上市集資。當時，SUNDAY的主要股東包括Distacom、[麗新發展](../Page/麗新發展.md "wikilink")、富聯國際、[僑福建設企業及中旅](../Page/僑福建設企業.md "wikilink")。

2000年3月16日，SUNDAY以招股上限3.78港元掛牌，集資淨額約21億港元。但隨着科網股熱潮爆破，加上香港電訊競爭激烈，SUNDAY股價逐年下跌，2002年曾挫至0.092元的歷史新低，較招股價跌了97%。

2003年3月底，SUNDAY引入[深圳電訊設備商](../Page/深圳.md "wikilink")[華為科技](../Page/華為公司.md "wikilink")，借款16億元以償還債務及興建網絡；經過2004年兩次增加投資份額\[2\]\[3\]，華為最後持股增至9.91%。

### 收購

2005年6月13日，電訊盈科宣佈以每股0.65元，收購Distacom（46%）及富聯（13%）所持SUNDAY股份，作價11.6億元，並計劃以19.4億進行全面收購。這是繼電盈於2002年向澳洲電訊（[Telstra](../Page/Telstra.md "wikilink")）沽清[CSL權益後](../Page/CSL_\(電訊\).md "wikilink")，重返香港流動電話市場。

2005年12月16日，SUNDAY召開特別股東大會，表決電盈[私有化方案](../Page/私有化.md "wikilink")。由於收購價較當初招股價少逾八成，不少小股東對方案表示反對。結果華為在內的3.16億股（82.8%）股份贊成、小股東為主的6500萬股（17.2%）股份反對，多於10%股東反對，私有化遭否決，電盈須回復SUNDAY公眾持股量達25%。

2006年11月30日，SUNDAY舉行特別股東大會，以99.5%通過把所有資產賣給[電訊盈科](../Page/電訊盈科.md "wikilink")、把所有現金派發特別[股息及撤銷上市地位](../Page/股息.md "wikilink")。SUNDAY在12月15日停牌，並在2007年1月[清盤](../Page/清盤.md "wikilink")，每股分得0.65港元。如果在2000年[IPO時認購了SUNDAY股份](../Page/IPO.md "wikilink")，一直持有直到清盤的話，投資者帳面損失82.8%（未計手續費和[機會成本](../Page/機會成本.md "wikilink")）。

SUNDAY為電訊盈科併購後的2G流動通訊服務品牌。但電盈於2007年3月1日宣佈，SUNDAY流動通訊服務將採用[電訊盈科流動通訊品牌](../Page/電訊盈科流動通訊.md "wikilink")，已於2007年3月26日正式易名為[PCCW
Mobile](../Page/電訊盈科流動通訊.md "wikilink")
2G。而Sunday門市亦於同日起改為電訊盈科專門店。為客戶提供賬單繳費、更改服務及其他售後服務；手機銷售及上台、淨手機及配件銷售及儲值咭銷售。

## 廣告宣傳

為提高市場佔有率，SUNDAY曾大量利用[電視廣告作為宣傳工具](../Page/電視廣告.md "wikilink")，建立品牌形象，因手法誇張出眾，故在香港引起廣泛關注。1999年3月1日，[流動電話號碼可攜正式實施](../Page/電話號碼可攜服務.md "wikilink")，客戶可以攜號轉台，SUNDAY利用電視廣告吸引客戶轉台。當年2月1日，該公司以「獨立日」為主題的廣告開始播放，片中一班青年在街上示威，宣揚「SUNDAY獨立日」，暗示不滿本身電訊公司的客戶終可以轉換其他供應商。

在1999年-2001年推出名為「SUNDAY話你知」的天氣預報，預報後，出現10秒的創意片段。 例如：

1.  天氣晴朗：[羊頭](../Page/羊.md "wikilink")[比堅尼女郎](../Page/比堅尼.md "wikilink")，黑面雪人（日間），蝦仔你乖乖瞓落床（晚間）
2.  天陰：雲吃日，[吸血鬼](../Page/吸血鬼.md "wikilink")（日間），雲揪月（晚間）
3.  有雨：有隻[青蛙跌落水](../Page/青蛙.md "wikilink")，恭祝你福壽雨天齊
4.  [寒冷天氣警告](../Page/寒冷天氣警告.md "wikilink")：[眼睛吃](../Page/眼睛.md "wikilink")[冰淇淋](../Page/冰淇淋.md "wikilink")
5.  [熱帶氣旋警告](../Page/熱帶氣旋警告.md "wikilink")：甩毛雀
6.  外面有霧：[蒼蠅死亡](../Page/蒼蠅.md "wikilink")
7.  [雷暴警告](../Page/雷暴警告.md "wikilink")：比武論劍
8.  暴雨警告系統及山泥傾瀉警告：大雨點，開槍掃射，[小貓墮下畫像](../Page/小貓.md "wikilink")

其後SUNDAY的「夜班的士司機撞鬼—平到你驚」及「食麵送牛眼—儲值卡送全套增值服務」廣告，由於有驚嚇場面，曾被觀眾投訴。

## 主要服務

  - 提供[2G服務](../Page/2G.md "wikilink")
  - 以[電訊盈科流動通訊的名義提供](../Page/電訊盈科流動通訊.md "wikilink")3G服務

## 參閱

  - [香港通訊](../Page/香港通訊.md "wikilink")

## 參考

<references />

  - 獨立日廣告 <https://www.youtube.com/watch?v=_7PA4d5732E>
  - SUNDAY年報（1999年、2005年）
  - 《獲豁免三年盈利規定
    SUNDAY全球招股集資22億》，原載《[明報](../Page/明報.md "wikilink")》，2000年3月6日，經濟要聞，B01
  - 《電盈科水
    李澤楷幫SUNDAY填氹》，原載《[壹週刊](../Page/壹週刊.md "wikilink")》，2005年6月16日，時事
  - 《電盈私化SUNDAY夢碎》，原載《[香港商報](../Page/香港商報.md "wikilink")》，2005年12月16日，香港產經，A07
  - 電訊盈科新聞稿，《SUNDAY流動通訊服務將採用電訊盈科流動通訊品牌》，2007年3月1日

## 外部連結

  - [SUNDAY](https://web.archive.org/web/20071011192823/http://www.sunday.com/?lang=ch)
  - [電訊盈科](https://web.archive.org/web/20071011000941/http://www.pccwmobile.com/?lang=ch)

[Category:1997年成立的公司](../Category/1997年成立的公司.md "wikilink")
[Category:2007年結業公司](../Category/2007年結業公司.md "wikilink")
[Category:香港已結業電訊公司](../Category/香港已結業電訊公司.md "wikilink")
[Category:香港交易所已除牌公司](../Category/香港交易所已除牌公司.md "wikilink")
[Category:納斯達克已除牌公司](../Category/納斯達克已除牌公司.md "wikilink")
[Category:香港移動通訊](../Category/香港移動通訊.md "wikilink")

1.
2.  [2004年2月11日 21世紀經濟報導 1億港幣入股Sunday
    力挺香港3G](http://www.cnetnews.com.cn/2004/0211/106485.shtml)
3.  [2004年11月26日 賽迪網 Sunday與華為加大財務合作
    確保明年推出3G](http://tech.sina.com.cn/t/2004-11-26/1125465568.shtml)