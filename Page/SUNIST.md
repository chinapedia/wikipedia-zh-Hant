**SUNIST**，全称Sino-UNIted Spherical
Tokamak，也即“中国联合球形[托卡马克](../Page/托卡马克.md "wikilink")”，是一个由[北京清华大学工程物理系与](../Page/清华大学_\(北京\).md "wikilink")[中国科学院物理研究所联合建造的球形](../Page/中国科学院物理研究所.md "wikilink")[托卡马克](../Page/托卡马克.md "wikilink")，坐落于[北京的清华大学工程物理系](../Page/北京.md "wikilink")，于2002年建成并投入实验。

SUNIST主要用于研究低环径比托卡马克[等离子体的物理特性](../Page/等离子体.md "wikilink")。从2002年11月产生第一炮[等离子体起](../Page/等离子体.md "wikilink")，SUNIST先后进行了欧姆放电、[等离子体边界湍流](../Page/等离子.md "wikilink")、垂直场改造、[微波驱动非感应电流启动](../Page/微波.md "wikilink")、电极辅助电流启动、[欧姆加热反向放电](../Page/欧姆.md "wikilink")，等等诸多研究。在有限的预算下，取得了较为不错的实验结果。

## 實驗室

SUNIST联合实验室是围绕SUNIST装置而成立的一个专业实验室。成员包括4\~6名[教授](../Page/教授.md "wikilink")、[副教授和大约](../Page/副教授.md "wikilink")10名[研究生](../Page/研究生.md "wikilink")。真空、[电子学](../Page/电子学.md "wikilink")、电力电子、微波、计算等高温[等离子体相关实验室设备较为完备](../Page/等离子体.md "wikilink")。

## 主要參數

| 參數        | 代號            | 數值         |
| --------- | ------------- | ---------- |
| 外徑        | R             | 30 cm      |
| 內徑        | a             | 23 cm      |
| 延伸率       | κ             | 1.6        |
| 軸線磁場      | B<sub>0</sub> | 0.15 Tesla |
| 典型的等离子体电流 | I<sub>P</sub> | 50 kA      |

## 參考文獻

  - [A Research Program of Spherical Tokamak in
    China](http://www.iop.org/EJ/article/1009-0630/4/4/003/pst_4_4_003.pdf)
  - [Initial Plasma Startup Test on SUNIST Spherical
    Tokamak](http://www.iop.org/EJ/article/1009-0630/5/6/001/pst_5_6_001.pdf)
  - [News about the construction of the SUNIST shperical tokamak (in
    Chinese)](http://wwww.cas.cn/html/Dir/2006/01/17/13/70/52.htm)

## 外在链接

  - [SUNIST联合实验室(www.sunist.org )](http://www.sunist.org)

[Category:核反应堆](../Category/核反应堆.md "wikilink")
[Category:原子核物理学](../Category/原子核物理学.md "wikilink")