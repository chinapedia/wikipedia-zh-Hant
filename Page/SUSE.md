**SUSE**（發音/ˈsuːsə/\[1\]）是[Linux作業系統的](../Page/Linux作業系統.md "wikilink")[發行版之一](../Page/Linux發行版.md "wikilink")，也是[德國的一個發行版](../Page/德國.md "wikilink")。SUSE屬於[Novell旗下的業務](../Page/Novell.md "wikilink")，它同時亦是Desktop
Linux Consortium的發起成員之一。

## 歷史

[SUSELinuxcon2016.jpg](https://zh.wikipedia.org/wiki/File:SUSELinuxcon2016.jpg "fig:SUSELinuxcon2016.jpg")
SUSE [Linux原是以](../Page/Linux.md "wikilink")[Slackware
Linux為基礎](../Page/Slackware_Linux.md "wikilink")，並提供完整[德文使用界面的產品](../Page/德文.md "wikilink")。1992年
[Peter McDonald成立了](../Page/Peter_McDonald.md "wikilink")[Softlanding
Linux
System](../Page/Softlanding_Linux_System.md "wikilink")（[SLS](../Page/SLS.md "wikilink")）這個發行版。這套發行版包含的軟件非常多，更首次收錄了[X
Window及](../Page/X_Window.md "wikilink")[TCP/IP等套件](../Page/TCP/IP.md "wikilink")。Slackware就是一個基於SLS的發行版。

SuSE於1992年末創辦，目的是成為[UNIX技術公司](../Page/UNIX.md "wikilink")，專門製為[德国人推出量身訂作的SLS](../Page/德国.md "wikilink")/Slackware[軟件及](../Page/軟件.md "wikilink")[UNIX](../Page/UNIX.md "wikilink")/Linux說明文件。1994年，他們首次推出了SLS/[Slackware的安裝光碟](../Page/Slackware.md "wikilink")，命名為S.u.S.E.
Linux 1.0。其後它綜合了[Florian La
Roche的](../Page/Florian_La_Roche.md "wikilink")[Jurix
distribution](../Page/Jurix_distribution.md "wikilink")（也是一個基於Slackware的發行版），於1996年推出一個完全自家打造的發行版
- S.u.S.E. Linux 4.2。其後SUSE Linux採用了不少[Red
Hat](../Page/Red_Hat.md "wikilink")
Linux的特質。（使用[RPM及](../Page/RPM.md "wikilink")/etc/sysconfig）

"S.u.S.E."後來改稱/簡短為"SuSE"，意思為"Software-und
System-Entwicklung"，那是一句德文，英文為"Software and system
development"。現在這家公司的名字再度更改成SUSE
Linux，"SUSE"一字已經不包含甚麼意義了。有非官方的謠言說"SUSE"同時是指德國電腦先驅[Konrad
Zuse的一個](../Page/Konrad_Zuse.md "wikilink")[雙關語](../Page/雙關語.md "wikilink")。

2003年11月4日，[Novell表示將會對SUSE提出](../Page/Novell.md "wikilink")[收購](../Page/收購.md "wikilink")。收購的工作於2004年1月完成。Novell也向大家保證SUSE的開發工作仍會繼續下去，Novell更把公司內全線電腦的系統換成SUSE
LINUX，並同時表示將會把SUSE特有而優秀的系統管理程式 -
[YaST](../Page/YaST.md "wikilink")2以[GPL授權釋出](../Page/GPL.md "wikilink")。

2005年8月4日，Novell公共關係科的領導及代言人 － Bruce Lowry表示，SUSE Linux
Professional系列的開發將變得更開放以及讓社群參與當中的工作。新的開發計劃名為openSUSE，目的是為了吸引更多的使用者及開發人員。相比以往，現在所有的開發人員及使用者能夠測試SUSE的產品並一起開發新版本的SUSE。在以往，SUSE的開發工作都是於內部進行的。SUSE
10.0是第一個給予公眾測試的版本。為了配合這個轉變，用戶除了能夠購買盒裝版本的SUSE外，也可以從網絡上免費下載。一系列的改變讓於2005年10月6日推出的SUSE
Linux有三個版本 － "OSS版"（完全地開放原始碼）、"試用版"（同時包含開放原始碼的程序及專屬程序如Adobe Reader、Real
Player等，其實就是盒裝零售版，也可以免費下載，可以安裝在硬碟上，並且沒有使用限制或限期，但不含說明手冊及Novell提供的技術支援）及盒裝零售版。

2013年11月9日，发布内核版本openSUSE 13.1，它是SUSE Linux
Enterprise的产品基础。该内核版本更新了所有的应用程序的版本，包括服务器的应用程序和桌面的应用程序，引入一些新的特性，吸纳了1000多个的开源项目，比方引进了OpenStack
Havana将近400个新的功能点等\[2\]。

2011年4月Attachmate集團購併了[網威](../Page/Novell.md "wikilink")，連帶購併了SUSE，但該集團將SUSE分立成一個獨立的事業單位，之後2014年10月Micro
Focus國際購併了Attachmate集團，連帶擁有網威與SUSE，但SUSE依然維持獨立事業單位。

## 功能

SUSE包含一個安裝及系統管理工具YaST2。它能夠進行磁碟分割、系統安裝、線上更新、網絡及防火牆組態設定、用戶管理和其他更多的工作。它為原來複雜的設定工作提供了方便的組合界面。

SUSE支持在安裝的時候調小NTFS硬碟的大小，令把Linux安裝到一台已經安裝了Windows
2000或XP的電腦的工作進行得更順利。此外，SUSE亦會自動偵測很多常見的Windows數據機並為它們安裝驅動程式。

SUSE也收錄了Linux下的多個桌面環境如KDE和GNOME及一些視窗管理員，比如是Window
Maker、Blackbox等。YaST2安裝程式也會讓使用者選擇使用GNOME、KDE或者不安裝圖形界面。SUSE已經為使用者提供了一系列多媒體程式如K3B（CD/DVD燒錄）、amaroK（音樂播放器）和Kaffeine（影片播放器）。它也收錄了OpenOffice.org，以及其他的文字閱讀／處理格式，如PDF等。

## 版本

SUSE Linux 11 分別有零售版本及自由、開放源碼的版本，叫作SUSE Linux
OSS。在軟體收錄方面，兩者幾乎一模一樣。它們最大的分別是，零售版含有一些專屬軟體如Macromedia
Flash Player。此版本的售價為$59.95美元，包括一本手冊以及有限度的技術支援。SUSE Linux
OSS則可以於它的網站上免費下載。零售版及可於網絡上免費取得試用版有1片DVD及5片CD，SUSE
Linux OSS則只提供5片CD，但DVD所包含的就是那5片CD的軟體，它只是為用戶提供一個更方便的安裝媒介。

SUSE Linux也提供了一個企業伺服器版本，名為SUSE Linux Enterprise
Server。它可以免費取得，但如果不付款，它只提供30天的更新服務。

## 發行方式

[Crystal_128_yast_bootmode.png](https://zh.wikipedia.org/wiki/File:Crystal_128_yast_bootmode.png "fig:Crystal_128_yast_bootmode.png")

過往，SUSE有別於其他的Linux般發行版提供立即為他們的新版本提供免費下載。SUSE首先釋出盒裝，包含說明手冊的個人版（Personal）及專業版（Professional），然後才於幾個月後提供FTP網絡安裝。近年SUSE有提供特別的專業版本，該版本缺少一些商業軟體，但此版本沒有提供一般的iso影像檔，而是要使用者下載一片小型的開機碟（[磁片或光碟](../Page/磁片.md "wikilink")）並使用上面所提及的網絡安裝。從9.2版開始，一片LiveDVD試用光碟可以免費取得。

現在，由於[openSUSE計劃](../Page/openSUSE.md "wikilink")，無論是SUSE Linux
OSS，還是試用版光碟，都可以於SUSE Linux釋出新版本後馬上取得。

## 参考文献

## 外部連結

  - [Novell SUSE官方網站](http://www.novell.com/linux/)
  - [openSUSE官方網站](http://www.opensuse.org/)
  - [openSUSE中文維基網站](https://zh.opensuse.org/)

## 参见

  - [openSUSE](../Page/openSUSE.md "wikilink")
  - [Novell Desktop](../Page/Novell_Desktop.md "wikilink")
  - [Linux套件列表](../Page/Linux套件列表.md "wikilink")

{{-}}

[ru:Дистрибутивы SUSE
Linux](../Page/ru:Дистрибутивы_SUSE_Linux.md "wikilink")

[Category:Linux发行版](../Category/Linux发行版.md "wikilink")
[Category:Linux公司](../Category/Linux公司.md "wikilink")
[Category:Novell軟件](../Category/Novell軟件.md "wikilink")
[Category:自由作業系統](../Category/自由作業系統.md "wikilink")

1.
2.   openSUSE官方发行说明