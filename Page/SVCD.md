[SVCD_logo.svg](https://zh.wikipedia.org/wiki/File:SVCD_logo.svg "fig:SVCD_logo.svg")

**Super Video
CD**（简称**SVCD**，也称**超级VCD**）是一種在標準[CD媒體上儲存視訊的格式](../Page/CD.md "wikilink")。以技術能力和影像品質來說，它的水準在[VCD和](../Page/VCD.md "wikilink")[DVD之間](../Page/DVD.md "wikilink")。

## 規格

SVCD的解析度是DVD的三分之二，並且是VCD的2.7倍以上。一張標準的CD-R燒錄片可以容納60分鐘的一般品質SVCD視訊。因為在規格中並未限制SVCD的影片長度，假如要放入更長的影片，就必須降低[位元率](../Page/位元率.md "wikilink")，影像的品質也會因此而降低。要放入超過100分鐘的影片而沒有明顯品質的損失是非常困難的，況且許多硬體播放器也無法播放位元率低於300-600kbps的影像。

### 視訊

  - [编解码器](../Page/编解码器.md "wikilink")：[MPEG-2](../Page/MPEG-2.md "wikilink")
  - [解析度](../Page/解析度.md "wikilink")：
      - [NTSC](../Page/NTSC.md "wikilink")：480x480
      - [PAL](../Page/PAL.md "wikilink")／[SECAM](../Page/SECAM.md "wikilink")：480x576
  - [縱橫比](../Page/縱橫比_\(圖像\).md "wikilink")：4:3
  - [幀率](../Page/幀率.md "wikilink")：
      - [NTSC](../Page/NTSC.md "wikilink")：每秒29.97*或*23.976楨
      - [PAL](../Page/PAL.md "wikilink")／[SECAM](../Page/SECAM.md "wikilink")：每秒25楨
  - [位元率](../Page/位元率.md "wikilink")：最高每秒2.6Mbps（2,600kbps）
      - 可使用[穩定位元率](../Page/固定码率.md "wikilink") (CBR)
        或[可變位元率](../Page/可变码率.md "wikilink")（VBR）

SVCD支援[隔行掃描的影像](../Page/隔行掃描.md "wikilink")，不過在楨率為23.976fps時則一定要使用[3:2
pulldown](../Page/3:2_pulldown.md "wikilink")。

不像其他種同樣以CD做儲存的視訊媒體，如China Video
Disc和[VCD](../Page/VCD.md "wikilink")，SVCD因為解析度的緣故和DVD-Video並不相容。不過[HD
DVD支援](../Page/HD_DVD.md "wikilink")480x480/576的解析度。

视频与音频的总码率不得超过2756[kilobits](../Page/kilobit.md "wikilink")。设置此速率上限的原因，在某种程度上是，确保与转速较慢但价格较便宜的“二倍速（2×
speed）”CD驱动器的兼容性。

### 音效

  - 编解码器：[MPEG-1 Audio Layer
    II](../Page/MPEG-1_Audio_Layer_II.md "wikilink")
  - 取樣頻率：44,100赫茲
  - 輸出：[單聲道](../Page/單聲道.md "wikilink")、雙聲道或[立體聲](../Page/立體聲.md "wikilink")
  - 位元率：固定數字
      - 穩定位元率編碼（CBR）

大多數的CD視訊媒體使用44.1kHz的取樣頻率，SVCD也不例外，但DVD則使用48kHz，所以兩者不相容。

### 額外功能

SVCD支援一些其他的功能，包括菜單、[超連結](../Page/超連結.md "wikilink")、卡啦OK歌詞提示、[overlay字幕](../Page/overlay.md "wikilink")、DVD品質的幻燈片秀（最高解析度NTSC
704x480或PAL
704x576）。SVCD可以有兩種不同的立體聲音效（使用於幕後評論或其他語言），而音效本身可以多至八軌道（以7.1聲道方式排列），環繞音效則使用[MPEG
Multichannel格式](../Page/MPEG_Multichannel.md "wikilink")。即使如此，受制於CD的容量和粗劣的回放硬體，這樣的音效規格並不實用。

### 回放問題

許多播放器呈現的SVCD影像天生就有缺陷，因為它的規格幾乎要違反了取樣理論。它的「三分之二」解析度幾乎很少透過完全的播放器內建電路來實現，理由是硬體播放器本身雖然支援許多不同格式（VCD、SVCD、DVD等等），它們的水平解析度並不相同（360、480、540、720像素），但卻只提供一個類比low
pass濾鏡，也就是說三種格式中有兩種會產生[混疊失真](../Page/混疊.md "wikilink")（aliasing）。通常這個濾鏡以最好的畫質媒體做最佳化，所以DVD受惠而SVCD則因為
[foldover損失畫質](../Page/混疊.md "wikilink")。如果顯示不遵循正確的理論，這種令人不快的混疊失真，會使影像被來自其他地方的噪訊所埋沒，例如攝影機、量化和MPEG的瑕疵（artifact）。

## 開發歷史

SVCD起初是在[中華人民共和國信息產業部的指導下](../Page/中華人民共和國信息產業部.md "wikilink")，由China
Recording Standards
Commmitee所開發的增強VCD格式的標準。它的動機之一是，中國需要獨立於DVD、不會受科技忠誠所影響的替代性格式。[中國政府擔心DVD格式被外國公司高度掌控](../Page/中國政府.md "wikilink")，因此迫切地開發可以在國內散佈而不受限制的格式。另外一個原因是預期SVCD的出現能夠降低成本，諸如中國國內的消費DVD
播放器和DVD授權費。

SVCD有兩個競爭對象，分別是[C-Cube微系統開發的](../Page/C-Cube.md "wikilink")**China Video
Disc**（CVD）和飛利浦、Sony、[松下](../Page/松下.md "wikilink")、[JVC集團的](../Page/JVC.md "wikilink")[HQ-VCD](../Page/HQ-VCD.md "wikilink")（VCD格式就是他們開發的）。CVD是最早出台的格式，其他兩方仍在草創階段時，它的規格書就已經完成。因此信息產業部和VCD集團同意合力開發，將HQ-VCD的功能整合入SVCD，但在1998年7月他們的規格書完成之時，CVD早已經被許多播放器的主力製造商所支援。為了維持相容性，CVD也被帶入SVCD的統一格式中，形成了Chaoji
Video
CD（規格書1998年11月），這與現存的SVCD幾乎完全相同。一台支援所謂SVCD的播放器，意味著它支援許多不同格式，包括SVCD、CVD、VCD
2.0、VCD 1.1 和[CDDA碟片](../Page/CDDA.md "wikilink")。

SVCD已經於2000年7月15日加入[IEC標準](../Page/IEC.md "wikilink")，即IEC
62107；這代表它如同CDDA或VCD一樣，成為國際認定的CD格式之一。飛利浦已經將SVCD字樣加入該公司的經典Compact
Disc圖示中。SVCD格式的電影已經在[两岸三地和數個亞洲國家中販售](../Page/两岸三地.md "wikilink")，但在亞洲國家以外，是否成功則仍需商榷。在西方世界，SVCD通常是用來儲存從DVD或[LD上拷貝下來的家庭錄影或電影](../Page/LD.md "wikilink")。

## 參見

  - [DVD](../Page/DVD.md "wikilink")
  - [VCD](../Page/VCD.md "wikilink")
  - [China Video Disc](../Page/China_Video_Disc.md "wikilink")
  - [miniDVD](../Page/miniDVD.md "wikilink")：將DVD視訊存於CD媒體上
  - [Enhanced Versatile
    Disc](../Page/Enhanced_Versatile_Disc.md "wikilink")

## 外部連結

  - [Technical Explanation of
    SVCD](https://web.archive.org/web/20070820130625/http://www.xp-media.com/svcd.pdf)
  - [AfterDawn's SVCD guides](http://www.afterdawn.com/guides/)
  - [VideoHelp description of SVCD](http://www.videohelp.com/svcd)

[Category:影像儲存](../Category/影像儲存.md "wikilink")
[Category:電腦儲存媒體](../Category/電腦儲存媒體.md "wikilink")