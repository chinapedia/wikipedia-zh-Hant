**SVK**（也寫作**svk**）是一個以[Perl寫成的](../Page/Perl.md "wikilink")[分散式](../Page/分散式.md "wikilink")[版本控制系統](../Page/版本控制.md "wikilink")（與其它的版本控制系統比較起來，像是[BitKeeper和](../Page/BitKeeper.md "wikilink")[GNU
arch](../Page/GNU_arch.md "wikilink")）。

SVK的主要作者是[高嘉良](../Page/高嘉良.md "wikilink")，使用[Artistic
License和](../Page/Artistic_License.md "wikilink")[GPL](../Page/GPL.md "wikilink")[雙重授權的](../Page/雙重授權.md "wikilink")[自由軟體](../Page/自由軟體.md "wikilink")。

2006年6月5日，高嘉良加入了[Best
Practical](../Page/Best_Practical.md "wikilink")（也就是[Request
Tracker的開發公司](../Page/Request_Tracker.md "wikilink")，同時也是SVK的重度使用者）。而SVK也變成了Best
Practical的一項產品\[1\]\[2\]。

2009年5月28日，高嘉良在邮件列表中宣布SVK将[停止开发](http://lists.bestpractical.com/pipermail/svk-users/2009-May/000425.html)。

## 特色

SVK 使用 [Subversion](../Page/Subversion.md "wikilink") 的檔案系統，但是提供了更多的特色：

  - 离线操作，比如 “checkin”，“log”，“merge”。
  - 分布式分支。
  - Lightweight checkout copy management (no *.svn* directories).
  - Advanced merge algorithms, like *star-merge* and *[cherry
    picking](../Page/cherry_picking.md "wikilink")*.
  - Changeset signing and verification.
  - 能对[Subversion](../Page/Subversion.md "wikilink")，[Perforce和](../Page/Perforce.md "wikilink")[CVS的版本库进行镜像和操作](../Page/CVS.md "wikilink")。

## 參見

  - [List of revision control
    software](../Page/List_of_revision_control_software.md "wikilink")
  - [Comparison of revision control
    software](../Page/Comparison_of_revision_control_software.md "wikilink")
  - [Subversion](../Page/Subversion.md "wikilink")
  - [GNU arch](../Page/GNU_arch.md "wikilink")，the source of inspiration
    for svk's star-merge feature
  - [Monotone](../Page/Monotone_\(software\).md "wikilink")，another
    open-source distributed
    [SCM](../Page/Source_Control_Management.md "wikilink") tool
  - [Mercurial](../Page/Mercurial_\(software\).md "wikilink")，another
    open-source distributed
    [SCM](../Page/Source_Control_Management.md "wikilink") tool

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [SVK Homepage](http://svk.bestpractical.com/)
  - [Best Practical](http://bestpractical.com/)
  - [Perl.com article on
    SVK](http://www.perl.com/pub/a/2004/03/03/svk.html)
  - [SVK Tutorials](http://www.bieberlabs.com/wordpress/svk-tutorials)

{{-}}

[Category:自由版本控制软件](../Category/自由版本控制软件.md "wikilink")
[Category:Perl软件](../Category/Perl软件.md "wikilink")

1.   [Best Practical Solutions Announces SVK
    Acquisition](http://use.perl.org/~jesse/journal/29812)
2.   [Best Practical Solutions Announces SVK Acquisition - Total World
    Domination Plan Proceeding
    Apace](http://bestpractical.com/news/svk.html)