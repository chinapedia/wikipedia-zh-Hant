**Segoe**（发音//）是一款西文[无衬线体](../Page/无衬线体.md "wikilink")，是由[微软公司开发的并且广泛使用的字体](../Page/微软.md "wikilink")。Segoe
UI是Segoe[商标下一系列](../Page/商标.md "wikilink")（至少有27种）字体的一种，这个系列的其他字体包括品牌印刷字体的拓展，\[1\]
在微软内部、广告代理商使用。另外[Windows
Vista中的手写字体](../Page/Windows_Vista.md "wikilink")[Segoe
Script和](../Page/Segoe_Script.md "wikilink")[Segoe
Print](../Page/Segoe_Print.md "wikilink")，一些[Segoe Media
Center字体](../Page/Segoe_Media_Center.md "wikilink")，还有内置于[MSNTV的](../Page/MSNTV.md "wikilink")[Segoe
TV都是这个家族](../Page/Segoe_TV.md "wikilink")。

## 争论

关于Segoe
UI字体和*[Frutiger](../Page/Frutiger.md "wikilink")*字体家族的相似性一直存在争论。[Frutiger是](../Page/Frutiger.md "wikilink")[德国](../Page/德国.md "wikilink")[Linotype公司的产品](../Page/Linotype.md "wikilink")。在2004年，[微软把一些Segoe和Segoe斜体字体作为其自创字体设计在](../Page/微软.md "wikilink")[欧盟进行商标和设计室注册](../Page/欧盟.md "wikilink")。Linotype公司对此表示抗议，2006年2月，欧盟撤销了微软的注册。\[2\]

实际上，Segoe体的很多字符的确和Frutiger非常相似，但是相似性还不及其他“类Frutiger字体”，如[Adobe公司的](../Page/Adobe.md "wikilink")[Myriad和](../Page/Myriad.md "wikilink")[苹果电脑公司的](../Page/苹果电脑公司.md "wikilink")[Podium
Sans](../Page/Podium_Sans.md "wikilink")。

## 发布

[Segoe_UI_Revision_Differences.png](https://zh.wikipedia.org/wiki/File:Segoe_UI_Revision_Differences.png "fig:Segoe_UI_Revision_Differences.png")
Segoe UI字体家族通过微软公司的产品一同发布。使用[Microsoft Office
2007](../Page/Microsoft_Office_2007.md "wikilink")、[Microsoft Office
2010](../Page/Microsoft_Office_2010.md "wikilink")、[Windows
Vista和](../Page/Windows_Vista.md "wikilink")[Windows
7的用户都可以获得这个字体](../Page/Windows_7.md "wikilink")。[Windows
8中的Segoe](../Page/Windows_8.md "wikilink") UI使用了新的字形设计。

## Segoe Print

**Segoe Print**是一款手写体打印字体，在非Windows7/Windows
Vista环境下均可[下载](../Page/下载.md "wikilink")，由数字、字母及符号构成。

### 首尔（English）字符中的构成

于2008年正式使用，目前还没有此字体。参数设置的字体中，选择「Segoe Print Hanja」。

### 汉字输入

参数设置中的字体中，选择「Segoe Print Chinese Characters」。

### 东京（English）字符中的构成

于2008年正式使用，目前还没有此字体。参数设置的字体中，选择「Segoe Print Kanji」或「Segoe Print Kana」。

## Segoe Script

[Segoe_Script_sample-cat_article_Wikipedia.png](https://zh.wikipedia.org/wiki/File:Segoe_Script_sample-cat_article_Wikipedia.png "fig:Segoe_Script_sample-cat_article_Wikipedia.png")
**Segoe
Script**是一套手写体，此字体包含字形有[常规体](../Page/常规体.md "wikilink")、[粗体](../Page/粗体.md "wikilink")、[粗斜体](../Page/粗斜体.md "wikilink")、[伪斜体和](../Page/伪斜体.md "wikilink")[希腊文](../Page/希腊文.md "wikilink")&[俄文](../Page/俄文.md "wikilink")。

### [阿拉伯数字和](../Page/阿拉伯数字.md "wikilink")[字母的各种变化](../Page/字母.md "wikilink")

#### [阿拉伯数字](../Page/阿拉伯数字.md "wikilink")

| [变形](../Page/变形.md "wikilink")\[3\] | [字体](../Page/字体.md "wikilink")[说明](../Page/说明.md "wikilink")                              | [使用](../Page/使用.md "wikilink")[字符](../Page/字符.md "wikilink")                        |
| ----------------------------------- | ----------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------- |
| 2个**[1](../Page/1.md "wikilink")**  | 若使用头尾相接的**[1](../Page/1.md "wikilink")**确实存在。即：由一笔相接的**[1](../Page/1.md "wikilink")**连成。  | **[1](../Page/1.md "wikilink")**                                                    |
| 2个**[2](../Page/2.md "wikilink")**  | 为**[22](../Page/22.md "wikilink")**。                                                      | **[2](../Page/2.md "wikilink")**                                                    |
| 2个**[3](../Page/3.md "wikilink")**  | 为**[33](../Page/33.md "wikilink")**。                                                      | **[3](../Page/3.md "wikilink")**                                                    |
| 2个**[4](../Page/4.md "wikilink")**  | 为**[44](../Page/44.md "wikilink")**。 详见[小学数学/自然数](http://zh.wikibooks.org/wiki/小学数学/自然数)。 | **[4](../Page/4.md "wikilink")**。如果有**[示例](../Page/示例.md "wikilink")**，也可以加粗**示例**。 |
| 2个**[5](../Page/5.md "wikilink")**  | 也有**[55](../Page/55.md "wikilink")**。                                                     | **[5](../Page/5.md "wikilink")**                                                    |
| 2个**[6](../Page/6.md "wikilink")**  | 也有**[66](../Page/66.md "wikilink")**。                                                     | **[6](../Page/6.md "wikilink")**                                                    |
| 2个**[7](../Page/7.md "wikilink")**  | 也有**[77](../Page/77.md "wikilink")**。                                                     | **[7](../Page/7.md "wikilink")**                                                    |
| 2个**[8](../Page/8.md "wikilink")**  | 也有**[88](../Page/88.md "wikilink")**。                                                     | **[8](../Page/8.md "wikilink")**                                                    |
| 2个**[9](../Page/9.md "wikilink")**  | 最后有**[99](../Page/99.md "wikilink")**。                                                    | '''[9](../Page/9.md "wikilink")"                                                    |
| 2个**[0](../Page/0.md "wikilink")**  | 在实际情况下，[0不能做头](../Page/0.md "wikilink")，实际为[100](../Page/100.md "wikilink")。              | **[0](../Page/0.md "wikilink")**                                                    |

## 请参阅

  - [ClearType](../Page/ClearType.md "wikilink")
  - [微软雅黑](../Page/微软雅黑.md "wikilink")
  - [微軟正黑體](../Page/微軟正黑體.md "wikilink")
  - [Malgun Gothic](../Page/Malgun_Gothic.md "wikilink")

## 外部链接

  - [Legal Background of the Segoe
    Case](http://www.sanskritweb.net/forgers/segoe.pdf)，an anti-Segoe
    perspective
  - [Is Microsoft's Vista Font Just a
    Copy?](https://web.archive.org/web/20060716112123/http://itmanagement.earthweb.com/columns/executive_tech/article.php/3599861)，Brian
    Livingston, 2006年4月18日

## 注释

<references />

## 外部链接

  - Microsoft Typography：[Segoe
    Chess](http://www.microsoft.com/typography/fonts/family.aspx?FID=339)、[Segoe
    Print](http://www.microsoft.com/typography/fonts/family.aspx?FID=352)、[Segoe
    Script](http://www.microsoft.com/typography/fonts/family.aspx?FID=353)、[Segoe
    UI](http://www.microsoft.com/typography/fonts/family.aspx?FID=331)、[Segoe
    UI
    Book](http://www.microsoft.com/typography/fonts/family.aspx?FID=332)
  - [Cleartype图库](https://web.archive.org/web/20100103094458/http://neosmart.net/gallery/v/apps/fonts)
  - [LSegoe Case的法律背景](http://www.sanskritweb.net/forgers/segoe.pdf)

[Category:Windows Vista字体](../Category/Windows_Vista字体.md "wikilink")

1.  [The Two Faces of the Microsoft
    Brand](http://www.mcpmag.com/features/article.asp?EditorialsID=602)
    (Doug Barney, July 2006)
2.
3.  变形字形中，小写数字的变形确定目标的基础。