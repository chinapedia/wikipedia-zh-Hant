**Skyrail Internatinal Tourism
Management**（簡稱：**Skyrail-ITM**或**Skyrail**），是一間以[旅遊](../Page/旅遊.md "wikilink")[纜車](../Page/纜車.md "wikilink")[管理](../Page/管理.md "wikilink")[顧問及](../Page/顧問.md "wikilink")[經營為](../Page/經營.md "wikilink")[業務的](../Page/業務.md "wikilink")[跨國公司](../Page/跨國公司.md "wikilink")，[總部設於](../Page/總部.md "wikilink")[澳洲](../Page/澳洲.md "wikilink")[昆士蘭](../Page/昆士蘭.md "wikilink")，於1995年創辦。

## 服務景點

  - [澳大利亞](../Page/澳大利亞.md "wikilink")[雪梨](../Page/雪梨.md "wikilink")[塔隆加動物園](../Page/塔朗加動物園.md "wikilink")\[1\]
  - [澳大利亞](../Page/澳大利亞.md "wikilink")[昆士蘭省](../Page/昆士蘭省.md "wikilink")[凱恩斯](../Page/凱恩斯_\(昆士蘭州\).md "wikilink")
    \[2\]

## 香港Skyrail

[Ngong_Ping_360.gif](https://zh.wikipedia.org/wiki/File:Ngong_Ping_360.gif "fig:Ngong_Ping_360.gif")於Skyrail營運時期的標誌\]\]

Skyrail在[香港成立](../Page/香港.md "wikilink")[子公司](../Page/子公司.md "wikilink")──**Skyrail-ITM（Hong
Kong）Ltd**，[主席為](../Page/主席.md "wikilink")[簡卓民](../Page/簡卓民.md "wikilink")（Ken
Chapman）(於2005年9月入職為總經理，其後轉任為香港Skyrail[主席](../Page/主席.md "wikilink"))，常務[董事是](../Page/董事.md "wikilink")[高德偉](../Page/高德偉.md "wikilink")（William
A.
Calderwood）(曾經出任[澳洲旅遊局副執行總監及](../Page/澳洲旅遊局.md "wikilink")[安捷航空旗下的旅行社組織Traveland](../Page/安捷航空.md "wikilink"))。[地鐵公司與Skyrail](../Page/地鐵公司.md "wikilink")-ITM簽署為期20年的[合約](../Page/合約.md "wikilink")，合作組成聯合營運公司，於2002年6月投得[昂坪360為期](../Page/昂坪360.md "wikilink")30年的專利營運權利。然後在其管理下，[昂坪360多次發生事故](../Page/昂坪360.md "wikilink")，其硬件質素及管理手法受到公眾關注。昂坪360於啟用初期，曾經多次發生事故，其[硬件質素及管理手法多次引起](../Page/硬件.md "wikilink")[香港傳媒及](../Page/香港傳媒.md "wikilink")[立法會議員的關注](../Page/香港立法會議員.md "wikilink")。於2007年[昂坪纜車墮下事故發生後](../Page/昂坪纜車墮下事故.md "wikilink")，[香港政府勒令即時停駛纜車](../Page/香港政府.md "wikilink")，並且進行徹底調查。地鐵公司其後以收購纜車公司的方式，終止與Skyrail-ITM[合作](../Page/合作.md "wikilink")。兩名前任高層人員因為纜車墜毀事故被[律政司以](../Page/律政司.md "wikilink")《架空纜車（安全）條例》的[疏忽處理架空纜車而為他人構成危險罪起訴](../Page/疏忽處理架空纜車而為他人構成危險罪.md "wikilink")，前常務董事[高德偉涉嫌疏忽](../Page/高德偉.md "wikilink")，沒有向該公司[員工提供必要的煞車](../Page/員工.md "wikilink")[測試](../Page/測試.md "wikilink")[程序及](../Page/程序.md "wikilink")[工作指引](../Page/工作.md "wikilink")，使轄下的架空纜車可能對附近的人構成不[安全](../Page/安全.md "wikilink")。案件於2008年1月10日正式提堂。Skyrail前助理維修經理[李杰來](../Page/李杰來.md "wikilink")，被控於事發當日疏忽地進行煞車測試\[3\]，控辯雙方同意合併與Skyrail前助理維修經理的傳票處理\[4\]。2009年3月5日，律政司基於原營運商Skyrail已經認罪及判處罰款5,000元，因而承擔了全部責任，若果再次進行審訊則不符合公眾利益，最後決定不起訴高德偉及李杰來兩人\[5\]。

## 參見

  - [2007年昂坪纜車墮下事故](../Page/2007年昂坪纜車墮下事故.md "wikilink")

## 参考文献

## 外部連結

  - [Skyrail ITM官方網站](http://www.skyrail-itm.com)

[Category:跨國公司](../Category/跨國公司.md "wikilink")
[Category:旅游公司](../Category/旅游公司.md "wikilink")
[Category:索道](../Category/索道.md "wikilink")
[Category:澳大利亚公司](../Category/澳大利亚公司.md "wikilink")

1.  [The Sky Safari](http://www.zoo.nsw.gov.au/content/view.asp?id=199)
2.  [Skyrail Rainforest Cableway](http://www.skyrail.com.au)
3.  [360前高層否認疏忽
    涉沒向員工提供測試指引](https://web.archive.org/web/20080116064543/http://hk.news.yahoo.com/080110/12/2mtms.html)《明報》，2008年1月11日
4.  [360前高層否認疏忽
    涉沒向員工提供測試指引](https://web.archive.org/web/20080116064543/http://hk.news.yahoo.com/080110/12/2mtms.html)《明報》，2008年1月11日
5.  [昂坪360兩前高層獲撤控
    墜毁車廂原因報告今公開](http://hk.news.yahoo.com/article/090305/4/b0hl.html)
    《明報》，2009年3月5日