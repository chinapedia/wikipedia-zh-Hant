**Skytrax**是一家以[英國為基地的顧問公司](../Page/英國.md "wikilink")，是Inflight Research
Services的附屬公司，
大部份股權則由利氏家族擁有\[1\]，其主要業務是為[航空公司的服務進行意見調查](../Page/航空公司.md "wikilink")。透過對國際旅行的問卷調查進行統計分析，從而找出現有服務提供者中擁有最佳[空中服務員](../Page/空中服務員.md "wikilink")、最佳航空公司、最佳航空公司酒廊、最佳機上娛樂、最佳膳食及其他與航空公司相關的服務意見調查\[2\]。除了上述的意見調查以外，Skytrax的網頁亦設有航空公司討論區，讓旅客分享其飛行經驗，以供其他旅客參考。另外，他們亦有進行服務評估、機艙檢查及滿意度調查。而他們最廣為人所知的，是每年一度舉辦的[年度全球最佳航空公司獎及](../Page/年度全球最佳航空公司獎.md "wikilink")[年度全球最佳機場獎](../Page/年度全球最佳機場獎.md "wikilink")。Skytrax还设立了機場和航空公司評等制度。

該公司的研究主要讓英國政府作參考，以便制定航空交通政策。例如：他們曾向[英國上議院提交一份有關飛行與健康的報告](../Page/英國上議院.md "wikilink")\[3\]。

## 獎項

### 年度全球最佳航空公司獎

<table>
<thead>
<tr class="header">
<th><p>年次</p></th>
<th><p>第一名</p></th>
<th><p>第二名</p></th>
<th><p>第三名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2001[4]</p></td>
<td><p><a href="../Page/阿聯酋航空.md" title="wikilink">阿聯酋航空</a></p></td>
<td><p><a href="../Page/新加坡航空.md" title="wikilink">新加坡航空</a></p></td>
<td><p><a href="../Page/國泰航空.md" title="wikilink">國泰航空</a></p></td>
</tr>
<tr class="even">
<td><p>2002[5]</p></td>
<td><p><a href="../Page/阿聯酋航空.md" title="wikilink">阿聯酋航空</a></p></td>
<td><p><a href="../Page/國泰航空.md" title="wikilink">國泰航空</a></p></td>
<td><p><a href="../Page/新加坡航空.md" title="wikilink">新加坡航空</a></p></td>
</tr>
<tr class="odd">
<td><p>2003[6]</p></td>
<td><p><a href="../Page/國泰航空.md" title="wikilink">國泰航空</a> [7]</p></td>
<td><p><a href="../Page/阿聯酋航空.md" title="wikilink">阿聯酋航空</a></p></td>
<td><p><a href="../Page/新加坡航空.md" title="wikilink">新加坡航空</a></p></td>
</tr>
<tr class="even">
<td><p>2004[8]</p></td>
<td><p><a href="../Page/新加坡航空.md" title="wikilink">新加坡航空</a></p></td>
<td><p><a href="../Page/阿聯酋航空.md" title="wikilink">阿聯酋航空</a></p></td>
<td><p><a href="../Page/國泰航空.md" title="wikilink">國泰航空</a></p></td>
</tr>
<tr class="odd">
<td><p>2005[9]</p></td>
<td><p><a href="../Page/國泰航空.md" title="wikilink">國泰航空</a></p></td>
<td><p><a href="../Page/澳洲航空.md" title="wikilink">澳洲航空</a></p></td>
<td><p><a href="../Page/阿聯酋航空.md" title="wikilink">阿聯酋航空</a></p></td>
</tr>
<tr class="even">
<td><p>2006[10]</p></td>
<td><p><a href="../Page/英國航空.md" title="wikilink">英國航空</a></p></td>
<td><p><a href="../Page/澳洲航空.md" title="wikilink">澳洲航空</a></p></td>
<td><p><a href="../Page/國泰航空.md" title="wikilink">國泰航空</a></p></td>
</tr>
<tr class="odd">
<td><p>2007[11]</p></td>
<td><p><a href="../Page/新加坡航空.md" title="wikilink">新加坡航空</a></p></td>
<td><p><a href="../Page/泰國國際航空.md" title="wikilink">泰國國際航空</a></p></td>
<td><p><a href="../Page/國泰航空.md" title="wikilink">國泰航空</a></p></td>
</tr>
<tr class="even">
<td><p>2008[12]</p></td>
<td><p><a href="../Page/新加坡航空.md" title="wikilink">新加坡航空</a></p></td>
<td><p><a href="../Page/國泰航空.md" title="wikilink">國泰航空</a></p></td>
<td><p><a href="../Page/澳洲航空.md" title="wikilink">澳洲航空</a></p></td>
</tr>
<tr class="odd">
<td><p>2009[13]</p></td>
<td><p><a href="../Page/國泰航空.md" title="wikilink">國泰航空</a></p></td>
<td><p><a href="../Page/新加坡航空.md" title="wikilink">新加坡航空</a></p></td>
<td><p><a href="../Page/韓亞航空.md" title="wikilink">韓亞航空</a></p></td>
</tr>
<tr class="even">
<td><p>2010[14]</p></td>
<td><p><a href="../Page/韓亞航空.md" title="wikilink">韓亞航空</a></p></td>
<td><p><a href="../Page/新加坡航空.md" title="wikilink">新加坡航空</a></p></td>
<td><p><a href="../Page/卡塔爾航空.md" title="wikilink">卡塔爾航空</a></p></td>
</tr>
<tr class="odd">
<td><p>2011[15]</p></td>
<td><p><a href="../Page/卡塔爾航空.md" title="wikilink">卡塔爾航空</a></p></td>
<td><p><a href="../Page/新加坡航空.md" title="wikilink">新加坡航空</a></p></td>
<td><p><a href="../Page/韓亞航空.md" title="wikilink">韓亞航空</a></p></td>
</tr>
<tr class="even">
<td><p>2012[16]</p></td>
<td><p><a href="../Page/卡塔爾航空.md" title="wikilink">卡塔爾航空</a></p></td>
<td><p><a href="../Page/韓亞航空.md" title="wikilink">韓亞航空</a></p></td>
<td><p><a href="../Page/新加坡航空.md" title="wikilink">新加坡航空</a></p></td>
</tr>
<tr class="odd">
<td><p>2013[17]</p></td>
<td><p><a href="../Page/阿聯酋航空.md" title="wikilink">阿聯酋航空</a></p></td>
<td><p><a href="../Page/卡塔爾航空.md" title="wikilink">卡塔爾航空</a></p></td>
<td><p><a href="../Page/新加坡航空.md" title="wikilink">新加坡航空</a></p></td>
</tr>
<tr class="even">
<td><p>2014[18]</p></td>
<td><p><a href="../Page/國泰航空.md" title="wikilink">國泰航空</a></p></td>
<td><p><a href="../Page/卡塔爾航空.md" title="wikilink">卡塔爾航空</a></p></td>
<td><p><a href="../Page/新加坡航空.md" title="wikilink">新加坡航空</a></p></td>
</tr>
<tr class="odd">
<td><p>2015[19]</p></td>
<td><p><a href="../Page/卡塔爾航空.md" title="wikilink">卡塔爾航空</a></p></td>
<td><p><a href="../Page/新加坡航空.md" title="wikilink">新加坡航空</a></p></td>
<td><p><a href="../Page/國泰航空.md" title="wikilink">國泰航空</a></p></td>
</tr>
<tr class="even">
<td><p>2016[20]</p></td>
<td><p><a href="../Page/阿聯酋航空.md" title="wikilink">阿聯酋航空</a></p></td>
<td><p><a href="../Page/卡塔爾航空.md" title="wikilink">卡塔爾航空</a></p></td>
<td><p><a href="../Page/新加坡航空.md" title="wikilink">新加坡航空</a></p></td>
</tr>
<tr class="odd">
<td><p>2017[21]</p></td>
<td><p><a href="../Page/卡塔爾航空.md" title="wikilink">卡塔爾航空</a></p></td>
<td><p><a href="../Page/新加坡航空.md" title="wikilink">新加坡航空</a></p></td>
<td><p><a href="../Page/全日本空輸.md" title="wikilink">全日空</a></p></td>
</tr>
<tr class="even">
<td><p>2018[22]</p></td>
<td><p><a href="../Page/新加坡航空.md" title="wikilink">新加坡航空</a></p></td>
<td><p><a href="../Page/卡塔爾航空.md" title="wikilink">卡塔爾航空</a></p></td>
<td><p><a href="../Page/全日本空輸.md" title="wikilink">全日空</a></p></td>
</tr>
</tbody>
</table>

### 五星级航空公司\[23\]

  - [全日空](../Page/全日本空輸.md "wikilink")（[星空聯盟](../Page/星空聯盟.md "wikilink")）

  - [日本航空](../Page/日本航空.md "wikilink")（[寰宇一家](../Page/寰宇一家.md "wikilink")）

  - [韓亞航空](../Page/韓亞航空.md "wikilink")（[星空聯盟](../Page/星空聯盟.md "wikilink")）

  - [國泰航空](../Page/國泰航空.md "wikilink")（[寰宇一家](../Page/寰宇一家.md "wikilink")）

  - [阿提哈德航空](../Page/阿提哈德航空.md "wikilink")

  - [長榮航空](../Page/長榮航空.md "wikilink")（[星空聯盟](../Page/星空聯盟.md "wikilink")）

  - [加魯達印尼航空](../Page/加魯達印尼航空.md "wikilink")（[天合聯盟](../Page/天合聯盟.md "wikilink")）

  - [海南航空](../Page/海南航空.md "wikilink")

  - [漢莎航空](../Page/漢莎航空.md "wikilink")（[星空聯盟](../Page/星空聯盟.md "wikilink")）

  - [卡塔尔航空](../Page/卡塔尔航空.md "wikilink")（[寰宇一家](../Page/寰宇一家.md "wikilink")）

  - [新加坡航空](../Page/新加坡航空.md "wikilink")（[星空聯盟](../Page/星空聯盟.md "wikilink")）

### 年度全球最佳機場獎

<table>
<thead>
<tr class="header">
<th><p>年分</p></th>
<th><p>第一名</p></th>
<th><p>第二名</p></th>
<th><p>第三名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1998</p></td>
<td><p><a href="../Page/吉隆坡国际机场.md" title="wikilink">吉隆坡国际机场</a>[24]</p></td>
<td><p>无</p></td>
<td><p>无</p></td>
</tr>
<tr class="even">
<td><p>1999</p></td>
<td><p><a href="../Page/阿姆斯特丹史基浦機場.md" title="wikilink">阿姆斯特丹史基浦機場</a>[25]</p></td>
<td><p>無</p></td>
<td><p>無</p></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td><p><a href="../Page/新加坡樟宜機場.md" title="wikilink">新加坡樟宜機場</a>[26]</p></td>
<td><p>無</p></td>
<td><p>無</p></td>
</tr>
<tr class="even">
<td><p>2001[27]</p></td>
<td><p><a href="../Page/香港國際機場.md" title="wikilink">香港國際機場</a></p></td>
<td><p><a href="../Page/吉隆坡國際機場.md" title="wikilink">吉隆坡國際機場</a></p></td>
<td><p><a href="../Page/新加坡樟宜機場.md" title="wikilink">新加坡樟宜機場</a></p></td>
</tr>
<tr class="odd">
<td><p>2002[28]</p></td>
<td><p><a href="../Page/香港國際機場.md" title="wikilink">香港國際機場</a></p></td>
<td><p><a href="../Page/新加坡樟宜機場.md" title="wikilink">新加坡樟宜機場</a></p></td>
<td><p><a href="../Page/雪梨機場.md" title="wikilink">雪梨機場</a></p></td>
</tr>
<tr class="even">
<td><p>2003[29]</p></td>
<td><p><a href="../Page/香港國際機場.md" title="wikilink">香港國際機場</a>[30]</p></td>
<td><p><a href="../Page/新加坡樟宜機場.md" title="wikilink">新加坡樟宜機場</a></p></td>
<td><p><a href="../Page/杜拜國際機場.md" title="wikilink">杜拜國際機場</a></p></td>
</tr>
<tr class="odd">
<td><p>2004[31]</p></td>
<td><p><a href="../Page/香港國際機場.md" title="wikilink">香港國際機場</a></p></td>
<td><p><a href="../Page/新加坡樟宜機場.md" title="wikilink">新加坡樟宜機場</a></p></td>
<td><p><a href="../Page/阿姆斯特丹史基浦機場.md" title="wikilink">阿姆斯特丹史基浦機場</a></p></td>
</tr>
<tr class="even">
<td><p>2005[32]</p></td>
<td><p><a href="../Page/香港國際機場.md" title="wikilink">香港國際機場</a></p></td>
<td><p><a href="../Page/新加坡樟宜機場.md" title="wikilink">新加坡樟宜機場</a></p></td>
<td><p><a href="../Page/仁川國際機場.md" title="wikilink">仁川國際機場</a></p></td>
</tr>
<tr class="odd">
<td><p>2006[33]</p></td>
<td><p><a href="../Page/香港國際機場.md" title="wikilink">香港國際機場</a></p></td>
<td><p><a href="../Page/新加坡樟宜機場.md" title="wikilink">新加坡樟宜機場</a> [34]</p></td>
<td><p><a href="../Page/慕尼黑機場.md" title="wikilink">慕尼黑機場</a></p></td>
</tr>
<tr class="even">
<td><p>2007[35]</p></td>
<td><p><a href="../Page/香港國際機場.md" title="wikilink">香港國際機場</a></p></td>
<td><p><a href="../Page/仁川國際機場.md" title="wikilink">仁川國際機場</a></p></td>
<td><p><a href="../Page/新加坡樟宜機場.md" title="wikilink">新加坡樟宜機場</a></p></td>
</tr>
<tr class="odd">
<td><p>2008[36]</p></td>
<td><p><a href="../Page/香港國際機場.md" title="wikilink">香港國際機場</a></p></td>
<td><p><a href="../Page/新加坡樟宜機場.md" title="wikilink">新加坡樟宜機場</a></p></td>
<td><p><a href="../Page/仁川國際機場.md" title="wikilink">仁川國際機場</a></p></td>
</tr>
<tr class="even">
<td><p>2009[37]</p></td>
<td><p><a href="../Page/仁川國際機場.md" title="wikilink">仁川國際機場</a></p></td>
<td><p><a href="../Page/香港國際機場.md" title="wikilink">香港國際機場</a></p></td>
<td><p><a href="../Page/新加坡樟宜機場.md" title="wikilink">新加坡樟宜機場</a></p></td>
</tr>
<tr class="odd">
<td><p>2010[38]</p></td>
<td><p><a href="../Page/新加坡樟宜機場.md" title="wikilink">新加坡樟宜機場</a></p></td>
<td><p><a href="../Page/仁川國際機場.md" title="wikilink">仁川國際機場</a></p></td>
<td><p><a href="../Page/香港國際機場.md" title="wikilink">香港國際機場</a></p></td>
</tr>
<tr class="even">
<td><p>2011[39]</p></td>
<td><p><a href="../Page/香港國際機場.md" title="wikilink">香港國際機場</a></p></td>
<td><p><a href="../Page/新加坡樟宜機場.md" title="wikilink">新加坡樟宜機場</a></p></td>
<td><p><a href="../Page/仁川國際機場.md" title="wikilink">仁川國際機場</a></p></td>
</tr>
<tr class="odd">
<td><p>2012[40]</p></td>
<td><p><a href="../Page/仁川國際機場.md" title="wikilink">仁川國際機場</a></p></td>
<td><p><a href="../Page/新加坡樟宜機場.md" title="wikilink">新加坡樟宜機場</a></p></td>
<td><p><a href="../Page/香港國際機場.md" title="wikilink">香港國際機場</a></p></td>
</tr>
<tr class="even">
<td><p>2013[41]</p></td>
<td><p><a href="../Page/新加坡樟宜機場.md" title="wikilink">新加坡樟宜機場</a></p></td>
<td><p><a href="../Page/仁川國際機場.md" title="wikilink">仁川國際機場</a></p></td>
<td><p><a href="../Page/阿姆斯特丹史基浦機場.md" title="wikilink">阿姆斯特丹史基浦機場</a></p></td>
</tr>
<tr class="odd">
<td><p>2014[42]</p></td>
<td><p><a href="../Page/新加坡樟宜機場.md" title="wikilink">新加坡樟宜機場</a></p></td>
<td><p><a href="../Page/仁川國際機場.md" title="wikilink">仁川國際機場</a></p></td>
<td><p><a href="../Page/慕尼黑機場.md" title="wikilink">慕尼黑機場</a></p></td>
</tr>
<tr class="even">
<td><p>2015[43]</p></td>
<td><p><a href="../Page/新加坡樟宜機場.md" title="wikilink">新加坡樟宜機場</a></p></td>
<td><p><a href="../Page/仁川國際機場.md" title="wikilink">仁川國際機場</a></p></td>
<td><p><a href="../Page/慕尼黑機場.md" title="wikilink">慕尼黑機場</a></p></td>
</tr>
<tr class="odd">
<td><p>2016[44]</p></td>
<td><p><a href="../Page/新加坡樟宜機場.md" title="wikilink">新加坡樟宜機場</a></p></td>
<td><p><a href="../Page/仁川國際機場.md" title="wikilink">仁川國際機場</a></p></td>
<td><p><a href="../Page/慕尼黑機場.md" title="wikilink">慕尼黑機場</a></p></td>
</tr>
<tr class="even">
<td><p>2017[45]</p></td>
<td><p><a href="../Page/新加坡樟宜機場.md" title="wikilink">新加坡樟宜機場</a></p></td>
<td><p><a href="../Page/東京國際機場.md" title="wikilink">東京國際機場</a></p></td>
<td><p><a href="../Page/仁川國際機場.md" title="wikilink">仁川國際機場</a></p></td>
</tr>
<tr class="odd">
<td><p>2018[46]</p></td>
<td><p><a href="../Page/新加坡樟宜機場.md" title="wikilink">新加坡樟宜機場</a></p></td>
<td><p><a href="../Page/仁川國際機場.md" title="wikilink">仁川國際機場</a></p></td>
<td><p><a href="../Page/東京國際機場.md" title="wikilink">東京國際機場</a></p></td>
</tr>
</tbody>
</table>

### 五星级機場

至2018年，Skytrax 共給予下列9座機場五星級評價。\[47\]

#### 亞洲

  - [香港國際機場](../Page/香港國際機場.md "wikilink")

  - [新加坡樟宜機場](../Page/新加坡樟宜機場.md "wikilink")

  - [仁川國際機場](../Page/仁川國際機場.md "wikilink")

  - [海口美蘭國際機場](../Page/海口美蘭國際機場.md "wikilink")

  - [東京國際機場](../Page/東京國際機場.md "wikilink")

  - [中部國際機場](../Page/中部國際機場.md "wikilink")

  - [哈馬德國際機場](../Page/哈馬德國際機場.md "wikilink")

  - [蓋達爾·阿利耶夫國際機場](../Page/蓋達爾·阿利耶夫國際機場.md "wikilink")

#### 歐洲

  - [慕尼黑國際機場](../Page/慕尼黑國際機場.md "wikilink")

## 外部連結

  - [Skytrax官方網站](http://www.airlinequality.com)

## 備注

[Category:民用航空](../Category/民用航空.md "wikilink")
[Category:英國公司](../Category/英國公司.md "wikilink")

1.

2.

3.

4.

5.

6.

7.
8.

9.  [Airline of the
    Year 2005](http://www.airlinequality.com/2005/airline-05-ent.htm)

10.

11. [Airline of the
    Year 2007](http://www.worldairlineawards.com/Awards_2007/AirlineYear-2007.htm)


12. [Airline of the
    Year 2008](http://www.worldairlineawards.com/Awards_2008/AirlineYear-2008.htm)


13. [Airline of the
    Year 2009](http://www.worldairlineawards.com/Awards_2009/AirlineYear-2009.htm)


14. [Airline of the
    Year 2010](http://www.worldairlineawards.com/Awards-2010/Airline2010.htm)


15. [Airline of the
    Year 2011](http://www.worldairlineawards.com/Awards_2011/Airline2011.htm)


16.

17.

18. [1](http://www.worldairlineawards.com/Awards/world_airline_rating_2014.html)

19.

20.

21.

22.

23.

24. [Profil Korporat - Malaysia Airport Holdings
    Berhad](http://www.klia.com.my/index.php?m=corp_info&c=brand/)

25. [Previous Winners : Airport Of The
    Year](http://www.worldairportawards.com/main/winner_history.htm)

26.
27.

28.

29.

30.
31. [HKIA wins top
    accolades](http://www.hktrader.net/200405/trade/product-bestairport200405.htm)

32. [Airport of the
    Year 2005](http://www.airlinequality.com/2005/airport-05-ent.htm)

33. [Airport of the
    Year 2006](http://www.worldairportawards.com/Awards-2006/AirportYear-2006.htm)


34.
35. [Airport of the
    Year 2007](http://www.worldairportawards.com/Awards_2007/Airport2007.htm)


36. [Airport of the
    Year 2008](http://www.worldairportawards.com/Awards_2008/Airport2008.htm)


37. [Airport of the
    Year 2009](http://www.worldairportawards.com/Awards_2009/Airport2009.htm)


38. [Airport of the
    Year 2010](http://www.worldairportawards.com/main/Press-APT2010.htm)


39. [Airport of the
    Year 2011](http://www.worldairportawards.com/Awards_2011/Airport2011.htm)


40. [Airport of the
    Year 2012](http://www.worldairportawards.com/Awards_2012/Airport2012.htm)


41. [Airport of the
    Year 2013](http://www.worldairportawards.com/Awards_2013/Airport2013.htm)


42. [Airport of the
    Year 2014](http://www.worldairportawards.com/Awards_2014/worlds_best_airport.htm)


43. [2](http://www.worldairportawards.com/Awards/world_airport_rating.html)

44. [3](http://www.worldairportawards.com/Awards/worlds_best_airport.html)

45. [4](http://www.worldairportawards.com/Awards/worlds_best_airport.html)

46. [5](http://www.worldairportawards.com/Awards/world_airport_rating.html)

47.