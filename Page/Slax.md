**SLAX**是一個基於[Debian的作業系統](../Page/Debian.md "wikilink")，預設[KDE桌面環境](../Page/KDE.md "wikilink")，由[捷克人Tomáš](../Page/捷克人.md "wikilink")
Matějíček研發。
Slax是一個方便、便攜的Linux作業系統。它的界面富有時尚感，可基於不同模組的功能運作。儘管它的體積較小，Slax依然預載了不少日常應用的軟件。除此之外，它還擁有精簡而齊整的圖形用戶界面和一些有用的修復工具，方便系統管理員修復系統。\[1\]

## 特性

模組化方式使Slax容易加入其他軟件，这使得定制Slax成为一件很容易的事情。在它的usb或者liveCD版本安装或者卸载软件，使用者只需下載包含該軟件的[Slackware組件](../Page/Slackware.md "wikilink")，並將它拷貝到Slax的/slax/modules目录下面即可。Slax的软件管理是[Slackware的管理方式](../Page/Slackware.md "wikilink")，依赖关系需要用户自行解决。此外，Slackware的软件包可通过**tgz2lzm**命令转化成Slax的模块，当然借助**deb2lzm**用户也可以将强大的[debian包转化成Slax的模块](../Page/debian.md "wikilink")。

Slax的主页为上传和下载用户定制组件提供便利。事实上，Slax提供了一个相当易用的包管理方式。用户下载的lzm包在live系统中双击后就能发挥作用。这于其他发行版的live系统是很不一样的。

## 版本

在Slax
3之前，Slax发行版被认为是"Slackware-Live"（Slackware的[Live版本](../Page/自生系统.md "wikilink")）。\[2\]\[3\]

### Slax 5

Slax 5有5种版本:

  - **Slax Standard**：为普通用户提供的标准版本。
  - **Slax KillBill**
    ：包含[Wine](../Page/Wine.md "wikilink")、[DOSBox以及](../Page/DOSBox.md "wikilink")[QEMU](../Page/QEMU.md "wikilink")，旨在运行[DOS与](../Page/DOS.md "wikilink")[Microsoft
    Windows程序](../Page/Microsoft_Windows.md "wikilink")。
  - **Slax
    Server**：提供额外的网络功能，以及预先配置的[DNS](../Page/Domain_Name_System.md "wikilink")、[DHCP](../Page/Dynamic_Host_Configuration_Protocol.md "wikilink")、[Samba](../Page/Samba.md "wikilink")、[HTTP](../Page/Hypertext_Transfer_Protocol.md "wikilink")、[FTP](../Page/File_Transfer_Protocol.md "wikilink")、[MySQL](../Page/MySQL.md "wikilink")、[SMTP](../Page/SMTP.md "wikilink")、[POP3](../Page/Post_Office_Protocol.md "wikilink")、[IMAP以及](../Page/Internet_Message_Access_Protocol.md "wikilink")[SSH等服务器](../Page/Secure_Shell.md "wikilink")。
  - **Slax Popcorn**：为浏览网络和多媒体功能定制的极简版本。使用[Mozilla
    Firefox作为默认](../Page/Mozilla_Firefox.md "wikilink")[网页浏览器](../Page/网页浏览器.md "wikilink")；以轻量的[Xfce作为其](../Page/Xfce.md "wikilink")[桌面环境](../Page/桌面环境.md "wikilink")，而非[KDE](../Page/KDE.md "wikilink")。
  - **Slax Frodo**：只有字符环境的版本，可供内存小的计算机使用。

[Fluxbox](../Page/Fluxbox.md "wikilink")[窗口管理器是Frodo之外版本的可选项](../Page/窗口管理器.md "wikilink")。

### Slax 6

Slax
6的定制完全依赖于模块系统。从该版本开始，模块基于[LZMA算法进行压缩](../Page/LZMA.md "wikilink")。Slax
5所使用的.mo模块以及Slax
6的.lzm模块之间在初始时可以通用。后来，由于在6的子版本中，[Linux内核版本有变](../Page/Linux内核.md "wikilink")，.mo模块被废弃了，每个模块必须依照新的内核版本进行编译才可使用。

### Slax 7

Slax
7支持[64位与](../Page/64位.md "wikilink")[32位的](../Page/32位.md "wikilink")[计算机架构](../Page/计算机架构.md "wikilink")。在官网的下载页面上，它提供了超过50种语言的版本。Slax
7使用精简版的KDE 4桌面，一张新的壁纸，以及新的模块系统。

## 延伸发行版

[SLAX有很多延伸发行版](../Page/SLAX.md "wikilink")。在[DistroWatch和](../Page/DistroWatch.md "wikilink")[Live
Developers](https://web.archive.org/web/20071020150519/http://live-developers.org/CategoryDistributions)的wiki社区上能够找到更多的信息。

## 外部連結

  - [Slax官方網站](http://www.slax.org/)
  - [Slax官方網站](https://www.slax.org/zhTW/)
  - [Slax官方网站](http://www.slax.org/?lang=cn)
  - [Slax官方网站（旧版）](https://web.archive.org/web/20130831134655/http://old.slax.org/)
  - [非官方SLAX Wiki](http://66.246.76.162/slax/wiki/)
  - [非官方SLAX 6.x.x模块 列表](http://66.246.76.162/slax/wiki/Module_List)
  - [自定义SLAX设置](http://www.ab9il.net/slax/slax-customization1.html)

[Category:Linux發行版](../Category/Linux發行版.md "wikilink")
[Category:LiveCD](../Category/LiveCD.md "wikilink")

1.
2.
3.