**Smart
Query**是一套功能強大的Web及Mobile查詢報表工具，自2002年上市至今已讓上千家中大型企業成功上線，容易學習及高度的開發效率，解決了企業在大量資訊報表呈現及快速WEB化的需求，提供方便的Drill-down、顏色管理、統計圖表、燈號、趨勢及報表主動派送等進階功能，讓應用的角色也從單純報表開發工具，擴展到企業資訊整合服務平台的地位。從作業層次、管理層次，以致決策分析到平衡計分卡﹝BSC﹞等應用，都可以看到客戶成功的應用案例。顯見Smart
Query不只是一套報表工具，除了報表製作外，更可以做為未來佈局商業智慧的前哨站。

開發人員不用學習Web程式設計，只需瞭解[SQL指令及使用](../Page/SQL.md "wikilink")[Excel](../Page/Excel.md "wikilink")，即可完成多種查詢及報表功能，搭配多種實用的設計，使用單位可以立即在不同的裝置、作業系統及瀏覽器(Chrome、Safari、Firefox、Edge、IE11)上即時得到查詢結果與報表列印，遇有功能變更，也僅須對Server做上傳，彈指間輕鬆完成更新，同時提升設計及維護效率3～10倍。
對於進階使用者而言，更可以在被授權的範圍，自行對資料加以運用，包括樞紐分析及報表設計等，也能夠將查詢的結果及報表匯出成不同的格式，如Excel、文字檔、XML、PDF及網頁等，支援套表及條碼列印。

## 此軟體可使用的資料來源

  - OLTP
    只要是[ADO和](../Page/ADO.md "wikilink")[ODBC相容的資料來源皆可使用](../Page/ODBC.md "wikilink")。

<!-- end list -->

  - OLAP
    限定[Microsoft SQL Server](../Page/Microsoft_SQL_Server.md "wikilink")
    2005 SP2 以上的版本，並曾經安裝過 MS-SQL Server的Analysis Services。

## 相關軟體

  - Smart eVision

<!-- end list -->

  -
    同一家公司的另一項產品，以**Smart Query**的WebAP為基礎，加上更多關於Web展示資料的方式。
    （魚骨圖、長條圖、折線圖、雷達圖、……等）

## 競爭對手

  - [水晶報表](../Page/水晶報表.md "wikilink")（Crystal Reports）
  - [FineReport](../Page/FineReport.md "wikilink")
  - [Smartbi](../Page/Smartbi.md "wikilink")

## 外部連結

  - 官網（臺灣）

<!-- end list -->

  - [Smart Query-查詢/報表開發及智慧管理工具](http://www.lcnet.com.tw/Product/query)
  - [Smart
    eVision-新世代資訊展現平台，快速整合多種資訊工具](http://www.lcnet.com.tw/Product/evision)
  - [Smart
    eBuilder-規格產生輸入/查詢/儀表板工具](http://www.lcnet.com.tw/Product/ebuilder)

<!-- end list -->

  - Smart eVision聯銓資訊科技

<!-- end list -->

  - [Smart eVision聯銓資訊科技官網](http://www.lcnet.com.tw)

<!-- end list -->

  - 其他

<!-- end list -->

  - [Smart Query -
    ProgWiki](http://www.player.idv.tw/prog/index.php?title=Smart_Query)（部分的FAQ）

[Category:報告軟件](../Category/報告軟件.md "wikilink")