[Logo_sobe.gif](https://zh.wikipedia.org/wiki/File:Logo_sobe.gif "fig:Logo_sobe.gif")
**SoBe**是一個由[百事公司生產與銷售的](../Page/百事公司.md "wikilink")[茶](../Page/茶.md "wikilink")、[elixir與](../Page/elixir.md "wikilink")[能量飲料品牌](../Page/能量飲料.md "wikilink")。SoBe的正確讀法為"so-bee"（[IPA](../Page/國際音標.md "wikilink")：\[sobi\]），全寫為"South
Beach Beverage Co.（南海灘飲料公司）"。

## 公司歷史

南海灘飲料公司是一家總部位於美國[康乃狄克州](../Page/康乃狄克州.md "wikilink")[諾沃克市的飲料生產商](../Page/諾沃克.md "wikilink")，在1996年組成，首個產品*SoBe
Black Tea
3G*([人參](../Page/人參.md "wikilink")（ginseng）、[瓜拿納](../Page/瓜拿納.md "wikilink")（guarana）與[銀杏](../Page/銀杏.md "wikilink")（ginkgo））因在發售後非常受歡迎而推出更多口味，2000年10月被北美[百事公司以](../Page/百事公司.md "wikilink")3億7000萬[美元收購](../Page/美元.md "wikilink")。

## 贊助

  - [騎師Eddie](../Page/騎師.md "wikilink") Castro
  - 由SoBe No Fear贊助的[NASCAR Nextel
    Cup車手](../Page/NASCAR_Nextel_Cup.md "wikilink")[Boris
    Said於](../Page/Boris_Said.md "wikilink")2006年Pepsi 400賽事中取得第四名

## 瓶蓋

因SoBe的每個瓶蓋都印有模仿諷刺流行文化或流行文化相關的語錄而非常有名，其中包括：Sobe Wan
Kenobi（[電影](../Page/電影.md "wikilink")[星球大戰](../Page/星球大戰.md "wikilink")）、Apocalizard
Now、Wasssssobe?、With Liberty and the Pursuit of Lizards、Go Herbal、Team
Ebos、Sobe Dooby
Doo（[動畫](../Page/動畫.md "wikilink")[你在哪？史酷比](../Page/你在哪？史酷比.md "wikilink")）、Say
Hello To My Little Lizard（電影[疤面煞星](../Page/疤面煞星.md "wikilink")）、Excuse
Me, I Believe You Have My
Stapler?（電影[上班一條蟲](../Page/上班一條蟲.md "wikilink")）、Professor
Chaos and General
Disarray（動畫[南方公園](../Page/南方公園.md "wikilink")[第606集Professor
Chaos](http://en.wikipedia.org/wiki/Professor_Chaos)）、Do the Lizards
Have Large Talons?（電影[拿破崙炸藥](../Page/拿破崙炸藥.md "wikilink")）、L to the
Izzo、Stiffler's Mom（電影[美國派](../Page/美國派.md "wikilink")）、1st place for
sweet Canadian Mullet與Mahogany Slide等，現時已有超過200種不同的瓶蓋標語產品\[1\]。

## 產品

**Exotic Teas and Fruit Juice Blends**

  - SoBe Black Tea 3G
  - SoBe Green Tea 3G
  - SoBe Oolong Tea 3G
  - SoBe Zen Tea 3G
  - SoBe Dragon
  - SoBe Nirvana

**SoBe Elixirs 3C**

  - SoBe Elixir 3C Pomegranate-Cranberry
  - SoBe Elixir 3C Cranberry-Grapefruit
  - SoBe Elixir 3C Orange-Carrot
  - SoBe Elixir 3C Grape

**Power Line**

  - SoBe Courage
  - SoBe Energy
  - SoBe Fuerte
  - SoBe Power

**Lizard Line**

  - Liz Blizz
  - Lizard Lava
  - Lizard Lightning
  - Strawberry and Banana
  - SoBe Lizard Fuel
  - SoBe Tsunami
  - SoBe Black & Blue Berry Brew
  - Long John Lizard's Grape Grog

**SoBe Lean**

  - SoBe Lean Cranberry-Grapefruit
  - SoBe Lean Energy
  - SoBe Lean Green Tea
  - SoBe Lean Mango-Melon
  - SoBe Lean Peach Tea

**SoBe Adrenaline Rush**

  - SoBe Adrenaline Rush
      - Sugar Free SoBe Adrenaline Rush

**SoBe No Fear**

  - SoBe No Fear
      - Sugar Free SoBe No Fear
  - SoBe No Fear Gold

**SoBe Synergy**

  - Fruit Punch
  - Grape
  - Kiwi Strawberry
  - Lemonade
  - Mango Orange

**SoBe Special Recipes**

  - MacLizard's Special Recipe Lemonade

**其他產品**

  - Superman SoBe

## 中止產品

  - SoBe Essentials（使用較小型的瓶子，但售價與大型瓶子相同）
  - SoBe Love Bus Brew
  - SoBe Mr. Green

## 瑣事

  - [電子遊戲](../Page/電子遊戲.md "wikilink")*Oddworld: Munch's
    Oddysee*內擁有可回復角色的[生命值的SoBe自動販賣機](../Page/生命值.md "wikilink")，該機器在後期關卡被相同效果"Health
    Up"機器取代。

## 參考及記事

<references />

## 外部連結

  - [公司官方網站](http://www.sobebev.com)
  - [SoBe Adrenaline Rush](http://www.sobeadrenalinerush.com/)

[Category:机能性饮料](../Category/机能性饮料.md "wikilink")
[Category:茶企業](../Category/茶企業.md "wikilink")
[Category:1996年成立的公司](../Category/1996年成立的公司.md "wikilink")

1.