**Socket
939**是[AMD](../Page/AMD.md "wikilink")[處理器插座的一種](../Page/CPU插座.md "wikilink")，於2004年6月推出，用作取代[Athlon
64所使用的](../Page/Athlon_64.md "wikilink")[Socket
754](../Page/Socket_754.md "wikilink")，是支持[64位元平台](../Page/64位元.md "wikilink")[AMD64的插座](../Page/AMD64.md "wikilink")，並支持[雙通道](../Page/雙通道.md "wikilink")（Dual
Channel）。而支持[DDR2 SDRAM的](../Page/DDR2_SDRAM.md "wikilink")[Socket
AM2已取代Socket](../Page/Socket_AM2.md "wikilink") 939。

Socket 939與[Socket 754主要不同的地方為](../Page/Socket_754.md "wikilink")：

  - 支持內含有雙通道記憶體控制器的Athlon 64處理器（64位元）
  - [HyperTransport能支持](../Page/HyperTransport.md "wikilink")1000[MHz速率](../Page/MHz.md "wikilink")

## 可用性

Socket 939於2004年6月推出，在2006年5月被AM2取代，但需求仍然很高。目前雙核只有[Athlon 64
X2](../Page/Athlon_64_X2.md "wikilink") 4200+ 這型號（90納米製程，2x512 KB
L2[快取](../Page/快取.md "wikilink")，89W，E4和E6步進）仍然有售\[1\]。

用於這插座的單核和雙核處理器包括[Athlon 64](../Page/Athlon_64.md "wikilink")、[Athlon 64
FX](../Page/Athlon_64_FX.md "wikilink")、[Athlon 64
X2](../Page/Athlon_64_X2.md "wikilink")、[Opteron和](../Page/Opteron.md "wikilink")[Sempron](../Page/Sempron.md "wikilink")。其中Opteron
185和Athlon 64 FX-60，具2.6 [GHz的時脈和](../Page/GHz.md "wikilink")1MB
L2快取，是在這插座中最快的處理器。

## 技術

Socket 939支持[双通道](../Page/Dual_Channel.md "wikilink")[DDR
SDRAM記憶體](../Page/DDR_SDRAM.md "wikilink")，具6.4 GB/s的記憶體頻寬。Socket
939的處理器均支持[3DNow\!](../Page/3DNow!.md "wikilink")、[SSE和](../Page/SSE.md "wikilink")[SSE2](../Page/SSE2.md "wikilink")[指令集](../Page/指令集.md "wikilink")，而Revision
E之後的更支持[SSE3指令集](../Page/SSE3.md "wikilink")。它有一個16位元的[HyperTransport](../Page/HyperTransport.md "wikilink")，能夠以2000
MT/s運行。用於這插座的處理器均有每個64KB的L1指令和數據快取，與512KB或1MB的L2快取。

## 參考

## 外部連結

  - [AMD Product
    Information](http://www.amd.com/us-en/Processors/ProductInformation/)
  - [AMD Technical Details for Athlon 64 and Athlon 64
    FX](http://www.amd.com/us-en/Processors/TechnicalResources/0,,30_182_739_7203,00.html)
  - [Socket 939 Motherboard Roundup
    \#2](https://web.archive.org/web/20110607211929/http://www.extremetech.com/article2/0%2C1558%2C1647599%2C00.asp)

[Category:CPU插座](../Category/CPU插座.md "wikilink")

1.  [AMD August Price
    list](http://www.amd.com/us-en/Processors/ProductInformation/0,,30_118_609,00.html),
    於2007年8月20日檢閱