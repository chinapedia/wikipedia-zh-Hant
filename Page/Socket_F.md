**Socket F**是第二代 [AMD Opteron](../Page/AMD_Opteron.md "wikilink")
處理器（型號：22xx、82xx）所使用的[插座之一](../Page/CPU插座.md "wikilink")，擁有雙核心、1207個腳位，採用與[LGA
775相似的接觸點式設計](../Page/LGA_775.md "wikilink")，並已於2006年8月15日推出。目前推出的標準版本消耗功率為98W，另外還推出了68W的低溫、省電、低消耗功率
HE 系列，產品型號為22xxHE、82xxHE。

這款插座會主攻[伺服器市場](../Page/伺服器.md "wikilink")，並會與[Socket
AM2及](../Page/Socket_AM2.md "wikilink")[Socket
S1共被視為同一世代的插座](../Page/Socket_S1.md "wikilink")，前者主要供家用的[Phenom](../Page/Phenom.md "wikilink")、[Athlon
X2](../Page/Athlon_X2.md "wikilink")、[Athlon
64及](../Page/Athlon_64.md "wikilink")[Sempron使用](../Page/Sempron.md "wikilink")。後者則為行動電腦的[Turion
64及雙核](../Page/Turion_64.md "wikilink")[Turion 64
X2所使用](../Page/Turion_64_X2.md "wikilink")，全數均支援[DDR2記憶體](../Page/DDR2_SDRAM.md "wikilink")。

除此之外，Socket
F還支援[FB-DIMM](../Page/FB-DIMM.md "wikilink")，而這些新處理器也或會支援多種新技術，包括[DDR3記憶體](../Page/DDR3_SDRAM.md "wikilink")、XD-RAM等。由於這些記憶體皆使用相同規格的FB-DIMM，因此可在無需更換處理器的情況下更換新記憶體，免卻早期Hammer架構的Opteron處理器需要整顆更換般麻煩。

另外，為了對抗[英特爾四核心處理器的推出](../Page/英特爾.md "wikilink")，AMD推出4x4平台，使用兩枚雙核心Athlon
64 FX-7X處理器，而插座則是Socket F而非Socket AM2。但這些處理器因為使用DDR2而非DDR2
FB-DIMM，所以不與Opteron處理器相容。

[Category:CPU插座](../Category/CPU插座.md "wikilink")