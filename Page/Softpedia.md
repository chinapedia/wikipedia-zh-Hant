**Softpedia**是一個收錄大量[遊戲](../Page/電子遊戲.md "wikilink")、[驅動程式](../Page/驅動程式.md "wikilink")、手機電話與[Windows](../Page/Microsoft_Windows.md "wikilink")、[Macintosh與](../Page/Macintosh.md "wikilink")[Linux平台軟體資訊並提供](../Page/Linux.md "wikilink")[下載的](../Page/下載.md "wikilink")[網站](../Page/網站.md "wikilink")，該網站同時收錄[科技](../Page/科技新聞.md "wikilink")、[科學](../Page/科學新聞.md "wikilink")、[健康及](../Page/健康新聞.md "wikilink")[娛樂新聞](../Page/娛樂新聞.md "wikilink")。[2006年10月後期](../Page/2006年10月.md "wikilink")[Alexa之流量排名指其為](../Page/Alexa.md "wikilink")[互聯網](../Page/互聯網.md "wikilink")600個最高流量網站之一\[1\]。該網站原名為Softnews.ro，於2003年後期改名為Softpedia。

Softpedia聲明會使用多種知名[防毒及](../Page/防毒軟體.md "wikilink")[反間諜軟體徹底檢查每一個他們提供的軟體及遊戲是否含有](../Page/反間諜軟體.md "wikilink")[病毒](../Page/電腦病毒.md "wikilink")、[惡意軟體](../Page/惡意軟體.md "wikilink")、[廣告軟體或](../Page/廣告軟體.md "wikilink")[間諜軟體](../Page/間諜軟體.md "wikilink")。Softpedia會給予未發現病毒、惡意軟體、廣告軟體及間諜軟體的程式"100%
Clean"[認證](../Page/認證.md "wikilink")，同時對免費程式給予"100% Free"認證。

軟體分類以[階層排序並模仿](../Page/階層.md "wikilink")[类Unix](../Page/类Unix.md "wikilink")[檔案系統](../Page/檔案系統.md "wikilink")[路徑方式表示](../Page/路徑.md "wikilink")，如"Home
/ Linux
Home"。大型的分類會分割成數頁。使用者可以自訂排序方式如最近更新[日期](../Page/日期.md "wikilink")、下載人數或[分數](../Page/分數.md "wikilink")。

## 外部連結

  - [Softpedia網站](http://www.softpedia.com)

## 注釋

<references/>

[Category:下載網站](../Category/下載網站.md "wikilink")
[Category:網站](../Category/網站.md "wikilink")

1.