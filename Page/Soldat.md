（[俄语](../Page/俄语.md "wikilink")“軍人、勇士”）是[波兰人](../Page/波兰.md "wikilink")在16歲時設計之多人[橫向捲軸射擊遊戲](../Page/捲軸射擊遊戲.md "wikilink")，受[liero](../Page/liero.md "wikilink")、[Scorched
Earth啟發](../Page/Scorched_Earth.md "wikilink")，結合[Counter-Strike和](../Page/Counter-Strike.md "wikilink")[百战天虫一些特徵](../Page/百战天虫.md "wikilink")。支持更改圖片、武器數值或是設計地圖。

## 遊戲戰鬥模式

### Death Match—死鬥模式

勝利條件（符合以下其中一項）：

1.  最先殺人數達到殺人上限
2.  限時內殺人數最多

### Point Match—持旗死鬥

死鬥模式的進化版。不同的是，持旗殺人分數兩倍，即得2分。
勝利條件（符合以下其中一項）：

1.  最先殺人數達到殺人上限
2.  限時內殺人數最多

### Rambo Match（Rambo）—第一滴血

同樣是死鬥模式的進化版。場地上會放置一把[藍波弓](../Page/藍波弓.md "wikilink")，得到的人會變身為藍波（有自動補血之效果）。
玩家則必須殺死擁有藍波弓者或持藍波弓殺人才能得分
勝利條件（符合以下其中一項）：

1.  最先殺人數達到殺人上限
2.  限時內殺人數最多

### Team Match—隊伍死鬥模式

最多可分為 紅、藍、黃和綠（Alpha、Bravo、Charlie、Delta）四隊來進行死鬥。
勝利條件（符合以下其中一項）：

1.  最先殺人數達到殺人上限
2.  限時內殺人數最多

### Catch The Flag—搶旗模式

只分為 紅和藍（Alpha、Bravo）兩隊，取得對方旗幟去觸碰己方旗幟即可得1分。
勝利條件（符合以下其中一項）：

1.  最快得到搶旗分數上限
2.  限時內得分最高隊

### Infiltration—守旗模式

搶旗模式的進化版， 紅隊（Alpha）為攻方和藍隊（Bravo）為守方。
紅隊需取得黑旗去觸碰白旗處才能得分（快速得25分，慢則得30分）
藍隊只要旗幟不被搶走，每5秒就會加1分
勝利條件：

1.  分數到達分數上限

### Hold The Flag—隊伍持旗戰鬥模式

只分為 紅和藍（Alpha、Bravo）兩隊，持旗隊每5秒加1分。
勝利條件：

1.  分數到達分數上限

## 遊戲進行模式

### Survival mode—生存模式

開始時會有5秒的暖身時間，玩家死亡後即不能重生，必須等到其中一隊全滅。

### Realistic mode—真實模式

無法看到視線以外的人（身後、被遮蔽）、槍枝後座力明顯增加、有重力限制（從高處摔落會受傷、武器重量影響移動速度）。

### Advance mode—進階模式

剛開始只有副武器可供選擇，要達到開場者設定的殺人數才會隨機增加1樣武器，反之，被殺即會減少武器。

## 武器

  - 主要武器（括號內的是鍵盤選擇鍵）
      - （1）[Desert Eagles](../Page/沙漠之鷹.md "wikilink")
        [以色列](../Page/以色列.md "wikilink")[IMI製](../Page/以色列軍事工業.md "wikilink")[半自動手槍](../Page/半自動手槍.md "wikilink")（[雙槍](../Page/雙持.md "wikilink")，在0.94c版中為單槍）
      - （2）[HK MP5](../Page/HK_MP5冲锋枪.md "wikilink")
        [德国](../Page/德国.md "wikilink")[黑克勒-科赫製](../Page/黑克勒-科赫.md "wikilink")[冲锋枪](../Page/冲锋枪.md "wikilink")
      - （3）[AK-74](../Page/AK-74突击步枪.md "wikilink")
        [苏联](../Page/苏联.md "wikilink")／[俄罗斯](../Page/俄罗斯.md "wikilink")[米哈伊尔·季莫费耶维奇·卡拉什尼科夫](../Page/米哈伊尔·季莫费耶维奇·卡拉什尼科夫.md "wikilink")[突击步枪](../Page/突击步枪.md "wikilink")1974年型
      - （4）[Steyr AUG](../Page/斯泰爾AUG突擊步槍.md "wikilink")
        [奥地利](../Page/奥地利.md "wikilink")[陆军通用](../Page/陆军.md "wikilink")[犢牛式](../Page/犢牛式_\(槍械\).md "wikilink")（Bullpup）[突击步枪](../Page/突击步枪.md "wikilink")
      - （5）[Spas-12](../Page/弗蘭基SPAS-12戰鬥霰彈槍.md "wikilink")
        [意大利](../Page/意大利.md "wikilink")製[戰鬥](../Page/戰鬥霰彈槍.md "wikilink")[霰彈槍](../Page/霰彈槍.md "wikilink")
      - （6）[Ruger-77](../Page/儒格M77步槍.md "wikilink")
        [美國](../Page/美國.md "wikilink")[儒格公司製](../Page/儒格-斯特姆.md "wikilink")[手動步槍](../Page/手動槍機.md "wikilink")
      - （7）[M79](../Page/M79榴彈發射器.md "wikilink")
        [美國](../Page/美國.md "wikilink")[春田兵工廠製](../Page/春田兵工廠.md "wikilink")[榴彈發射器](../Page/榴彈發射器.md "wikilink")
      - （8）[Barrett M82A1](../Page/巴雷特M82狙擊步槍.md "wikilink")
        [美國](../Page/美國.md "wikilink")[巴雷特製](../Page/巴雷特槍械公司.md "wikilink")[狙击步枪](../Page/狙击步枪.md "wikilink")（[反器材步槍](../Page/反器材步槍.md "wikilink")）
      - （9）[FN Minimi](../Page/FN_Minimi輕機槍.md "wikilink")
        [比利时](../Page/比利时.md "wikilink")[FN製](../Page/Fabrique_Nationale.md "wikilink")[轻机枪](../Page/轻机枪.md "wikilink")（[M249班用機槍](../Page/M249班用自動武器.md "wikilink")）
      - （0）[XM214 Mini gun](../Page/XM214微型砲機槍.md "wikilink")
        [美國](../Page/美國.md "wikilink")[-{zh-cn:通用电气; zh-tw:奇異;
        zh-hk:通用電氣;}-製](../Page/通用电气.md "wikilink")[加特林机枪](../Page/加特林机枪.md "wikilink")
  - 附屬武器（無法使用鍵盤選擇鍵選擇，但可在Player中的Secondary Weapon預設）
      - [USSCOM](../Page/HK_Mk_23_Mod_0手槍.md "wikilink")（HK MK23 Mod
        0）[德国](../Page/德国.md "wikilink")[黑克勒-科赫](../Page/黑克勒-科赫.md "wikilink")[手枪](../Page/手枪.md "wikilink")

      - [鏈鋸](../Page/鋸.md "wikilink")

      - [戰術小刀](../Page/小刀.md "wikilink")，可按拋棄鍵[投擲](../Page/飛刀.md "wikilink")。

      - [M72 LAW](../Page/M72輕型反裝甲武器.md "wikilink")
        [美國](../Page/美國.md "wikilink")[輕型反裝甲武器](../Page/反坦克武器.md "wikilink")
  - 特殊武器（無法一開始就取得，但是[手榴弹除外](../Page/手榴弹.md "wikilink")）
      - [Grenade](../Page/手榴弹.md "wikilink")
        [手榴弹](../Page/手榴弹.md "wikilink")，一開始有2個，可在場上的[綠色盒取得](../Page/綠色.md "wikilink")，可在Options的Maximum
        Grenade 調整取得數量（1—5個）。

      - Cluster Grenade
        破片[手榴弹](../Page/手榴弹.md "wikilink")，可在場上的[黄色盒取得](../Page/黄色.md "wikilink")，無法調整取得的數量，每盒取得的手榴弹數是3個。

      - [藍波弓](../Page/弓.md "wikilink")，只限Rambo
        Match（[第一滴血](../Page/第一滴血.md "wikilink")）使用。射出的[箭是一擊必殺](../Page/箭.md "wikilink")。

      - Flame Arrow
        藍波弓的另一種型態，射出的[箭會](../Page/箭.md "wikilink")[爆炸](../Page/爆炸.md "wikilink")。

      - [Flamer](../Page/火焰喷射器.md "wikilink")
        [火焰放射器](../Page/火焰喷射器.md "wikilink")。

      - M2 Stationary Gun
        [白朗寧M2重機槍](../Page/白朗寧M2重機槍.md "wikilink")，屬於[固定式機槍](../Page/固定式機槍.md "wikilink")。

      - Punch [拳頭](../Page/拳頭.md "wikilink")，可以使敵人繳械。
  - 被取消的武器
      - [M4 Carbine](../Page/M4卡賓槍.md "wikilink")
        [美國柯爾特製](../Page/美國.md "wikilink")[卡賓槍](../Page/卡賓槍.md "wikilink")，在最早的0.94c中出現，在0.97b版被[Steyr
        AUG所取代](../Page/斯泰爾AUG突擊步槍.md "wikilink")。
      - [Colt M1911](../Page/M1911手槍.md "wikilink")
        [美國柯爾特製](../Page/美國.md "wikilink")[半自動手槍](../Page/半自動手槍.md "wikilink")1911年型，在1.1.0版被[USSCOM](../Page/HK_Mk_23_Mod_0手槍.md "wikilink")（HK
        MK23 Mod 0）取代。

## 關於註冊

Soldat中的某些額外功能如更改噴射器顏色、更改面板等等，都是需要註冊後才能使用。
註冊的動作是在官方網站的“註冊”區執行。

## 外部連結

  - [Soldat 官方網站](http://www.soldat.pl/)

  - [官方論壇](http://forums.soldat.pl)

  - [Official
    Soldat指引／Wiki](https://web.archive.org/web/20070522183838/http://wiki.soldat.nl/Index)

  - [*SoldatHQ Maps*](http://soldathq.com/maps/) -
    最大和最快的升級Soldat的網上地圖資料庫。

## 其他

  - [liero](../Page/liero.md "wikilink")
  - [Counter-Strike](../Page/Counter-Strike.md "wikilink")

[Category:共享软件](../Category/共享软件.md "wikilink")
[Category:免费游戏](../Category/免费游戏.md "wikilink")
[Category:電腦遊戲](../Category/電腦遊戲.md "wikilink")
[Category:動作射擊遊戲](../Category/動作射擊遊戲.md "wikilink")
[Category:第三人称射击游戏](../Category/第三人称射击游戏.md "wikilink")