**Solidot**是[CNET中国旗下的一个中国的科技资讯网站和](../Page/CNET.md "wikilink")[奇客交流社区](../Page/奇客.md "wikilink")，其口号是“奇客的资讯，重要的东西”。Solidot创建于2005年4月，先运行在Geeklog系统上，后在2006年采用了知名网站“[Slashdot](../Page/Slashdot.md "wikilink")”的后台系统Slashcode。Solidot的主要是面对[开源](../Page/开源.md "wikilink")[自由软件和关心科技资讯读者群](../Page/自由.md "wikilink")，包括众多中国开源软件的开发者、爱好者和布道者。

## 历史

2005年4月，效仿Slashdot，李国伟（网名CdrPlum，或Jesse_Lee）创建了Solidot的前身Slashdotcn。运行于基于Geeklog的[内容管理系统](../Page/内容管理系统.md "wikilink")。2006年5月，Jesse_Lee组织人汉化了基于[Perl的](../Page/Perl.md "wikilink")[Slashdot的大部分](../Page/Slashdot.md "wikilink")[源代码](../Page/源代码.md "wikilink")[Slashcode](../Page/Slashcode.md "wikilink")。Solidot运行在新的Slashcode上\[1\]。

部分由于Slashcode的资源开销庞大，难于维护，由上海微睦主机赞助的虚拟主机不堪重负。2006年9月，Jesse_Lee宣布Solidot即将停止服務\[2\]并放到ebay上拍卖\[3\]。其间受到了媒体\[4\]和网友的关注\[5\]
\[6\] \[7\]

2007年1月，Solidot正式成为[CNET旗下网站](../Page/CNET.md "wikilink")，2007年媒体公司CNET旗下[CNET
NEWS的英文页面](../Page/CNET_NEWS.md "wikilink")"About
us"介绍说solidot是访问量最大的科技资讯网站之一，吸引了多达15000以上的[RSS订阅](../Page/RSS.md "wikilink")\[8\]。到2010年5月Solidot的RSS订阅量已经超过了25万。

## 影响

Solidot以IT技术爱好者提交的原创新闻或直接采编国外新闻而闻名。其新闻常被各大网络新闻媒体和个人博客转载。Solidot对[Ubuntu创始人Mark的采访](../Page/Ubuntu.md "wikilink")，被[新浪科技](../Page/新浪.md "wikilink")\[9\]、[网易科技](../Page/网易.md "wikilink")\[10\]等大量科技资讯网站和[博客转载](../Page/博客.md "wikilink")。

Solidot网友的评论和观点也经常成为各网站的话题和论点\[11\]\[12\]\[13\]。

## 社区

Solidot除了IT科学新闻外，常常对很多基于网络有争议的热点问题发表新闻或者评论\[14\]。

## 参见

  - [Slashdot](../Page/Slashdot.md "wikilink")

## 外部链接

  - [Solidot](http://www.solidot.org/)

## 参考文献

[Category:网站](../Category/网站.md "wikilink")
[Category:網誌](../Category/網誌.md "wikilink")
[Category:社群網站](../Category/社群網站.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10. [网易](http://tech.163.com/07/0706/10/3INAG9FK000915BD.html)
11.
12.
13.
14. [Solidot名人堂](http://solidot.org/hof.shtml)