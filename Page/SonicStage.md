**SonicStage**是[索尼推出的](../Page/索尼.md "wikilink")[隨身聽](../Page/隨身聽.md "wikilink")[Walkman用的音樂管理程式](../Page/Walkman.md "wikilink")，它適用於[微軟](../Page/微軟.md "wikilink")[Windows](../Page/Windows.md "wikilink")[作業系統](../Page/作業系統.md "wikilink")。最初版本為1999年推出OpenMG
Jukebox（初時並未使用SonicStage這個名字），它可以用於[筆記型電腦](../Page/筆記型電腦.md "wikilink")[VAIO和](../Page/VAIO.md "wikilink")[Net
MD隨身聽上](../Page/Net_MD.md "wikilink")，其後OpenMG Jukebox被納入SonicStage
2.0。在2005年，[新力公司除發表了SonicStage](../Page/新力.md "wikilink")
3.0外，也另外推出了音樂管理程式「CONNECT Player」。CONNECT
Player是由美國新力公司分部[Sony
Connect於](../Page/Sony_Connect.md "wikilink")2005年開發，在2005年11月與索尼的新型硬碟數位音樂播放器「NW-A
Walkman」系列一同發佈到歐洲與日本市場。2006年1月，歐洲索尼因接到過千宗關於CONNECT
Player的問題報告，建議使用者在等待進一步更新時改用SonicStage。2006年5月25日，Sony
Connect發佈SonicStage CP 4.0，為SonicStage加入如智能隨機播放與智慧搜尋連結（Artist
Link）等功能。2009年6月11日發佈的SonicStage V
Ver.5.2是SonicStage的最終版本。2009年8月11日，索尼結束SonicStage
3.3以後版本的自動取得音樂資訊功能。2009年10月7日，索尼發佈[x-アプリ](../Page/x-アプリ.md "wikilink")，取代SonicStage。2012年10月1日，[數位音樂下載服務](../Page/數位音樂下載.md "wikilink")「[mora](../Page/mora.md "wikilink")」全面改版，不再提供SonicStage使用者購買及下載音樂。

## 问题

直至今日 SonicStage CP 也以對多國語系支援不足而為人詬病。另外，此软件的文件版权限制受到部分用户的批评。

{{-}}

[Category:媒体播放器](../Category/媒体播放器.md "wikilink")
[Category:索尼](../Category/索尼.md "wikilink")