**索尼爱立信K750i**是[索尼爱立信一款在](../Page/索尼爱立信.md "wikilink")2005年推出的手提電話，而W800i為K750i的Walkman版本，W800i與K750i只是外型不同，硬體則完全相同。它是[Sony
Ericsson
K700i的延續](../Page/Sony_Ericsson_K700i.md "wikilink")，改良的版本為在2006年推出的[Sony
Ericsson
K800i](../Page/Sony_Ericsson_K800i.md "wikilink")。K750i配有二百萬[像素鏡頭](../Page/像素.md "wikilink")，位於手機背面，由一塊可移動的蓋掩保護。

## 規格

[K750icam.jpg](https://zh.wikipedia.org/wiki/File:K750icam.jpg "fig:K750icam.jpg")

### 數位功能

  - 相機功能：200 萬像素 CMOS 感光元件
  - 補光燈
  - 數位變焦：4X 數碼變焦、自動對焦、圖片編輯器
  - 30秒、無限時影片拍攝
  - 影片播放：[3GP](../Page/3GP.md "wikilink")、[MP4](../Page/MP4.md "wikilink")
  - 音樂播放：[MP3](../Page/MP3.md "wikilink")
  - 影音補充資料：[Mega Bass等化器](../Page/Mega_Bass.md "wikilink")、
    音樂播放、音樂編輯器／[MIDI](../Page/MIDI.md "wikilink")（音樂DJ）功能
  - [記憶體](../Page/記憶體.md "wikilink")：34 MB
  - 記憶卡插槽：Memory Stick Pro Duo（可擴充記憶至8GB，建議擴充至2GB以下）

### 傳輸功能

  - [藍芽](../Page/藍芽.md "wikilink")、[紅外線](../Page/紅外線.md "wikilink")、[USB傳輸](../Page/USB.md "wikilink")
  - 上網功能：GPRS Class 10（WAP 2.0）
  - 多媒體訊息：MMS、EMS、SMS
  - Email收發

## 參考條目

  - [索尼爱立信](../Page/索尼爱立信.md "wikilink")

## 外部連結

  - [索尼爱立信K750i官方網站](https://web.archive.org/web/20060228193300/http://www.sonyericsson.com/k750i/)

### 軟件

  - [K750軟件共享](https://web.archive.org/web/20060813225100/http://www.jail.se/sony_ericsson_k750i.html)

### 遊戲下載網站

  - [Lasyk
    Network](https://web.archive.org/web/20060813140108/http://www.myk750.lasyk.net/K750i/Games/)
  - [Zedge.no](http://zedge.no/?side=javagames)
  - [4se.ru](http://4se.ru/index.php?go=Files)
  - [mobileplayground.co.uk](https://web.archive.org/web/20060830081514/http://www.mobileplayground.co.uk/uploads/SonyEricsson/)
  - [penguin.kurgan.ru](http://penguin.kurgan.ru/cgi-bin/mob_games.pl?dir=//)
  - [by.ru](https://web.archive.org/web/20060113043431/http://ww.by.ru/k750/)
  - [kilogr-box.narod.ru](http://kilgor-box.narod.ru/javagames.html)
  - [bob385.com](https://web.archive.org/web/20040708152817/http://java.mob385.com/en/)
  - [java2phone.ru](http://www.java2phone.ru/eng/)
  - [midlet.org](http://www.midlet.org/category.jsp;jsessionid=2anijmpigng8?parentLevel=1)
  - [tagtag.com](https://web.archive.org/web/20060811165826/http://tagtag.com/site/javalist.php3)

[K750i](../Category/索尼愛立信手機.md "wikilink")
[Category:2005年面世的手機](../Category/2005年面世的手機.md "wikilink")