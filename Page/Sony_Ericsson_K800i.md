**Sony Ericsson
K800i**為[索尼爱立信於](../Page/索尼爱立信.md "wikilink")2006年7月推出的首部[Cyber-Shot](../Page/Cyber-Shot.md "wikilink")[3G手提電話](../Page/3G.md "wikilink")。此外更備有320萬[像素](../Page/像素.md "wikilink")[自動對焦相機功能](../Page/自動對焦.md "wikilink")。中国大陆版本为K790c。

## 外型

K800i的尺寸為105x47x22mm，SIZE比起2G機K750i的（100x46x20.5mm）沒有太大的分別。由於背部的相機鏡頭保護蓋是突出的，所以手機不能完全平放於桌面上。鍵盤的設計與K750i的近似，方正而整齊。相機鏡頭保護蓋採用推蓋的設計，推開機蓋即可進入相機模式。

## 音樂／鈴聲播放

聽歌方面，K800i支援WMA、MP3、AAC、AAC+及eAAC+的音樂檔案，但由於K800i並非主打Walkman音樂手機，跟機只附送HPM-62耳機（即與K750i跟機附送的耳機一樣），認為低音的效果比預期的差，建議玩家可自行購買其他系列耳機

雖然 K800i 並不支援 Mega Bass，但仍然設置有EQ等化器，於多媒體播放器內的目錄點播聽歌亦是十分簡單。 同時 K800i
亦支援飛行模式，即不須啟動手機通話功能，仍可開啟音樂播放器。另外，K800i 亦支援 Stereo RDS FM
收音機功能。

支援72和弦鈴聲的K800i，於鈴聲表現方面聲音還是非常小，比起聲音較大的K750i、W800、W900i、W550i來說，K800i的聲音大小還相差得遠。

## 拍攝功能

使用Sony的Cyber-shot品牌為賣點的K800i擁有320萬像素[CMOS感光元件](../Page/CMOS.md "wikilink")，同樣使用了Sony數碼相機的介面，率先採用先進的BestPic拍攝功能，內置[氙氣閃光燈及具備防震功能](../Page/氙氣.md "wikilink")。K800i亦支援自動對焦、微距拍攝、防紅眼功能及16倍數碼變焦。

採用了最新BestPic技術的K800i，用家只須按下快門，手機便會連續拍下9張相片，讓您挑選最滿意的影像作儲存。配合K800i的內置64MB記憶體及外加M2記憶卡（容量高達8GB），相片儲存方面毫無問題。

K800i亦支援[PictBridge列印標準](../Page/PictBridge.md "wikilink")，只需透過[USB接線將手機直接支援PictBridge列印標準的相片打印機](../Page/USB.md "wikilink")，毋須經過電腦處理，便可即時將手機內的數碼影像列印成相片。

## 特殊技術

  - BestPic（[Sony Ericsson的專利影像技術](../Page/Sony_Ericsson.md "wikilink")）
  - 內建320萬畫素自動對焦相機
  - 內建氙氣閃光燈
  - 支援動態與靜態影像[防手震功能](../Page/防手震.md "wikilink")
  - VGA 攝影鏡頭可進行影像電話
  - 音樂播放程式（支援[MP3](../Page/MP3.md "wikilink")、[AAC](../Page/AAC.md "wikilink")、[AAC+](../Page/AAC+.md "wikilink")、[eAAC+](../Page/eAAC+.md "wikilink")）
  - 相片[部落格](../Page/部落格.md "wikilink")
  - 自動防紅眼功能
  - 內建動態錄影／播放
  - 支援[藍芽](../Page/藍芽.md "wikilink")、[紅外線](../Page/紅外線.md "wikilink")、[USB傳輸](../Page/USB.md "wikilink")
  - RDS FM收音機
  - 可擴充[Memory Stick Micro記憶卡](../Page/Memory_Stick_Micro.md "wikilink")
  - 飛航模式

## 參考條目

  - [索尼爱立信](../Page/索尼爱立信.md "wikilink")

[K800i](../Category/索尼愛立信手機.md "wikilink")
[Category:2006年面世的手機](../Category/2006年面世的手機.md "wikilink")