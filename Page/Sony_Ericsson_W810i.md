在2006年1月14日，[索尼愛立信宣佈推出W](../Page/索尼愛立信.md "wikilink")810型號[Walkman手機](../Page/Walkman.md "wikilink")。

其他值得注意的特色包括 full function 網路瀏覽器，兩百萬畫素自動對焦鏡頭，[Memory Stick Pro
Duo](../Page/Memory_Stick.md "wikilink")
插槽（最高4GB），以及音樂播放模式。當使用音樂播放模式時手機的功能會完全關閉，以便在不能使用手機通訊的地點時也能享受音樂。此款手機支援[MP3以及](../Page/MP3.md "wikilink")
[AAC音樂格式以及](../Page/Advanced_Audio_Coding.md "wikilink")[MPEG4和](../Page/MPEG4.md "wikilink")[3GPP影片格式](../Page/3GPP.md "wikilink")。

W810i中有不少改變是來自[Sony Ericsson
W800i](../Page/Sony_Ericsson_W800i.md "wikilink")，但是最明顯的改變是[Sony
Ericsson手機常見的搖桿式方向鍵](../Page/Sony_Ericsson.md "wikilink")（joystick）被
D-pad 所取代。D-pad 增加了在聽音樂時的方便性。

W810i在2006年四月推出，現在已經全球發行。

## 型號差異

  - W810i - 在歐洲，非洲，美洲，亞洲和澳洲使用
  - W810c - 在中國內地使用

## 外部連結

  - [Official W810 press
    release](https://web.archive.org/web/20060513182917/http://www.sonyericsson.com/spg.jsp?cc=global&lc=en&ver=4001&template=pc3_1_1&zone=pc&lm=pc3_1&prid=4459)
  - [Official W810
    page](https://web.archive.org/web/20060428080328/http://www.sonyericsson.com/spg.jsp?cc=us&lc=en&ver=4000&template=pp1_loader&php=PHP1_10376&zone=pp&lm=pp1&pid=10376)
  - [Official W810
    specifications](https://web.archive.org/web/20060703022537/http://www.sonyericsson.com/spg.jsp?cc=us&lc=en&ver=4000&template=pp1_1_1&zone=pp&lm=pp1&pid=10376)
  - [Sony Ericsson W810i Review -
    Mobiledia](http://www.mobiledia.com/reviews/sonyericsson/w810i/page1.html)
  - [Sony Ericsson W810i Features, Specs, and User Reviews -
    Mobiledia](http://www.mobiledia.com/phones/sonyericsson/w810i.html)
  - [W810i high resolution
    pictures](http://scr3.golem.de/?d=0601/SonyEricsson_W810i&a=42543)

[W810i](../Category/索尼愛立信手機.md "wikilink")