**XPERIA
X1**是[索尼愛立信旗下的一款](../Page/索尼愛立信.md "wikilink")[智能手機](../Page/智能手機.md "wikilink")，為該公司第一款搭载
[Windows Mobile](../Page/Windows_Mobile.md "wikilink")
操作系统的智能机。由台灣[宏達電](../Page/宏達電.md "wikilink")[設計及製造](../Page/ODM.md "wikilink")，產品代號[HTC
Kovsky](../Page/HTC_Kovsky.md "wikilink")（KOVS100）。2008年2月於[巴塞隆那舉辦的](../Page/巴塞隆那.md "wikilink")[移動電訊展](../Page/世界行動通訊大會.md "wikilink")（Mobile
World Congress 2008）上發表，同年10月27日在歐洲推出。

其主要特色包括3吋輕觸式[WVGA屏幕](../Page/WVGA.md "wikilink")、[弧面滑蓋設計](../Page/弧面滑蓋設計.md "wikilink")（Arc-Slider）、使用Windows
Mobile6.1版本、[Xperia
Panel操作介面](../Page/Xperia_Panel.md "wikilink")，支援多種[流動通訊網絡](../Page/流動通訊網絡.md "wikilink")（包括[GSM](../Page/GSM.md "wikilink")／[GPRS](../Page/GPRS.md "wikilink")／[EDGE](../Page/EDGE.md "wikilink")
四頻、[UMTS](../Page/UMTS.md "wikilink")／[HSDPA](../Page/HSDPA.md "wikilink")／[HSUPA](../Page/HSUPA.md "wikilink")
四頻）等。

## 手機規格

  - 大小：110x17x53毫米

<!-- end list -->

  - 重量：145克

<!-- end list -->

  - 顏色：黑色、銀色

<!-- end list -->

  - 支援網絡：[GSM](../Page/GSM.md "wikilink")（850|900|1800|1900）,[EDGE](../Page/EDGE.md "wikilink")，[HSDPA](../Page/HSDPA.md "wikilink")，[HSUPA](../Page/HSUPA.md "wikilink")，[UMTS](../Page/UMTS.md "wikilink")（850|900|1700|1900|2100）屏幕：800x480（[WVGA](../Page/WVGA.md "wikilink")）、3吋、65536色[TFT](../Page/TFT.md "wikilink")、輕觸式感應（电阻式觸控面板）

<!-- end list -->

  - 記憶容量：內置400MB、支援[MicroSD](../Page/MicroSD.md "wikilink")

<!-- end list -->

  - 輸入：[光學搖捍](../Page/光學搖捍.md "wikilink")（Optical
    Joystick）、[QWERTY鍵盤](../Page/QWERTY鍵盤.md "wikilink")、手寫輸入

<!-- end list -->

  - 傳輸功能：[USB
    2.0](../Page/USB_2.0.md "wikilink")、[aGPS](../Page/aGPS.md "wikilink")、[藍芽](../Page/藍芽.md "wikilink")、[Wi-Fi](../Page/Wi-Fi.md "wikilink")

<!-- end list -->

  - 鏡頭：320萬像素[CMOS](../Page/CMOS.md "wikilink")、副鏡頭30萬畫素[CMOS](../Page/CMOS.md "wikilink")、自動對焦、影片拍攝

<!-- end list -->

  - 多媒體格式：[mp3](../Page/mp3.md "wikilink")、[AAC](../Page/AAC.md "wikilink")、[MPEG4](../Page/MPEG4.md "wikilink")、[3GP](../Page/3GP.md "wikilink")

<!-- end list -->

  - 程式支援：[JAVA](../Page/JAVA.md "wikilink")、[CAB](../Page/CAB.md "wikilink")

<!-- end list -->

  - 訊息功能：[SMS](../Page/SMS.md "wikilink")、[MMS](../Page/MMS.md "wikilink")、[E-Mail](../Page/E-Mail.md "wikilink")（[POP3](../Page/POP3.md "wikilink")、[IMAP4](../Page/IMAP4.md "wikilink")、[SMTP](../Page/SMTP.md "wikilink")）

<!-- end list -->

  - 預載軟件：[Internet Explorer](../Page/Internet_Explorer.md "wikilink")
    Mobile 、[Microsoft Office](../Page/Microsoft_Office.md "wikilink")
    Mobile、[Windows Media](../Page/Windows_Media.md "wikilink")™ Player
    Mobile、[Windows
    Live](../Page/Windows_Live.md "wikilink")™、[Microsoft
    Outlook](../Page/Microsoft_Outlook.md "wikilink") Mobile:
    email、Exchange ActiveSync®
    應用軟件、[RSS閱讀器](../Page/RSS.md "wikilink")、[Adobe](../Page/Adobe.md "wikilink")
    Reader LE、[Opera Browser](../Page/Opera_Browser.md "wikilink")
    Mobile

<!-- end list -->

  - 瀏覽器：[Internet Explorer](../Page/Internet_Explorer.md "wikilink")
    Mobile、[Opera Browser](../Page/Opera_Browser.md "wikilink") Mobile

<!-- end list -->

  - 其他功能：FM收音機、行事曆、計算機、電話簿、飛航模式、鬧鐘、免持擴音器、多和弦鈴聲、視像電話、震動提示、設有3.5mm耳機插頭、[A2DP等](../Page/A2DP.md "wikilink")

## 外部連結

[XPERIA X1
官方網頁](https://web.archive.org/web/20080409212947/http://www.sonyericsson.com/x1/)

[XPERIA X1
手機技術規格](http://www.sonyericsson.com/cws/products/mobilephones/specifications/x1?cc=hk&lc=zh)

[Category:智能手機](../Category/智能手機.md "wikilink")
[Category:索尼愛立信手機](../Category/索尼愛立信手機.md "wikilink")