**Spiritual
Garden**是[日本](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")[田村由香里的第](../Page/田村由香里.md "wikilink")9張單曲，由[科樂美於](../Page/科樂美.md "wikilink")2005年10月26日發行，並由[KING
RECORDS負責分銷](../Page/KING_RECORDS.md "wikilink")。商品編號為GBCM-7。

## 概要

  - 初回限定版採用Digipak形式的包裝，是首次採用的包裝。
  - 在Oricon唱片銷量排行榜的最高排名為第10位（2005年11月第一週\[1\]），是田村首張進入Oricon10大排行榜的單曲\[2\]。

## 收錄曲目

1.  Spiritual Garden
      - 作詞：，作曲、編曲：[太田雅友](../Page/太田雅友.md "wikilink")
      - 動畫「[魔法少女奈葉A's](../Page/魔法少女奈葉A's.md "wikilink")」片尾曲
2.  Cutie ♥ Cutie
      - 作詞：，作曲、編曲：[橋本由香利](../Page/橋本由香利.md "wikilink")
      - 田村由香里主持的電台節目「」開首曲（2005年10月－2006年4月）
3.  Traveling with a sheep
      - 作詞：ucio，作曲、編曲：柘植敏道
      - 田村由香里主持的電台節目「」結尾曲（2005年10月－2006年4月）

## 參考資料

<div class="references-small">

<references />

</div>

[Category:2005年單曲](../Category/2005年單曲.md "wikilink")
[Category:田村由香里單曲](../Category/田村由香里單曲.md "wikilink")
[Category:魔法少女奈葉歌曲](../Category/魔法少女奈葉歌曲.md "wikilink")
[Category:電視動畫主題曲](../Category/電視動畫主題曲.md "wikilink")

1.
2.  [Oricon的有關報導](http://www.oricon.co.jp/news/ranking/1050/)