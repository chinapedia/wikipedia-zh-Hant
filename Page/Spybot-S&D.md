是一款[反間諜軟件](../Page/反間諜軟件.md "wikilink")。

## 簡介

在許多網站上時常介紹為「免費軟體」，但正確上來說，只限個人及非商業用途時能以[捐贈軟體的方式利用](../Page/捐贈軟體.md "wikilink")。

它能掃瞄及移除[間諜程式和](../Page/間諜程式.md "wikilink")[廣告軟體](../Page/廣告軟體.md "wikilink")，並提供眾多工具予用戶保護電腦系統及[瀏覽器安全](../Page/瀏覽器.md "wikilink")。像大多其他的反間諜軟件，這軟件利用文件和內存搜索來尋找。而且，這軟件還可以利用預防禁止一些間諜軟件的安裝。

## 特點

能夠消除很多流氓軟件造成的問題，如消除含有個人信息的[Cookie](../Page/Cookie.md "wikilink")「小餅乾」、[登錄](../Page/登錄.md "wikilink")（）、操作系統的網絡平台、[對象連接與嵌入程序](../Page/對象連接與嵌入.md "wikilink")（OLE）、瀏覽器劫持程序、BHO插件程序、等等。還可以利用於預防或禁止一些間諜軟件的安裝。由於這軟件的特製不是預防，早期出版的版本有一提示，推薦用戶同時使用[SpywareBlaster](../Page/SpywareBlaster.md "wikilink")。不是一個殺毒軟件，但是它可以清理一些常見的[木馬病毒和](../Page/特洛伊木馬_\(電腦\).md "wikilink")[擊鍵信息收集器](../Page/擊鍵信息收集器.md "wikilink")。
因為許多免費商業程序的經濟來源是間諜軟件提供商，如果程序發現捆綁的間諜軟件被消除，許多這些正式的程序會停止運行。因此在某些情況下，不刪除間諜軟體而改用文件代替的方式覆蓋間諜軟體。

由於流氓軟體的變化很快，程序幾乎每星期都被更新。這些更新是從軟件內部的更新功能而更新的。

支持從之後的所有操作系統，已被翻譯於十幾種語言。程序也支持版面的改制。軟體的[論壇也有技術支持](../Page/論壇.md "wikilink")。

## 評價

## 相關條目

  - [間諜軟體](../Page/間諜軟體.md "wikilink")
  - [反間諜軟體](../Page/反間諜軟體.md "wikilink")
  - [廣告軟體](../Page/廣告軟體.md "wikilink")
  - [Ad-Aware](../Page/Ad-Aware.md "wikilink")

## 外部連結

  - [Spybot-S\&D官方主頁](http://www.spybot.info/)
  - [Spybot Search and
    Destroy中文官方網頁](https://web.archive.org/web/20070426203909/http://www.safer-networking.org/cs/index.html)
  - [Spybot Search and
    Destroy繁體中文官方網頁](https://web.archive.org/web/20080517061246/http://safer-networking.org/ct/index.html)

[Category:系統軟件](../Category/系統軟件.md "wikilink")
[Category:反間諜軟件](../Category/反間諜軟件.md "wikilink")