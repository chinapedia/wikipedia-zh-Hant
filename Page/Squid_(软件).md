[LAMP_software_bundle.svg](https://zh.wikipedia.org/wiki/File:LAMP_software_bundle.svg "fig:LAMP_software_bundle.svg")
software bundle (here additionally with **Squid**).\]\] **Squid
Cache**（簡稱為**Squid**）是[HTTP](../Page/HTTP.md "wikilink")[代理服务器软件](../Page/代理服务器.md "wikilink")。Squid用途广泛，可以作为[缓存服务器](../Page/缓存服务器.md "wikilink")，可以过滤流量帮助网络安全，也可以作为代理服务器链中的一环，向上级代理转发数据或直接连接互联网。Squid程序在[Unix一类系统运行](../Page/Unix.md "wikilink")。由于它是开源软件，有网站修改Squid的源代码，编译为原生Windows版\[1\]；用户也可在Windows里安装[Cygwin](../Page/Cygwin.md "wikilink")，然后在Cygwin里编译Squid。

Squid歷史悠久，功能完善。除了[HTTP外](../Page/HTTP.md "wikilink")，對[FTP與](../Page/FTP.md "wikilink")[HTTPS的支援也相當好](../Page/HTTPS.md "wikilink")，在3.0測試版中也支援了[IPv6](../Page/IPv6.md "wikilink")。但是Squid的上级代理不能使用[SOCKS协议](../Page/SOCKS.md "wikilink")。

## 支持平台

Squid能在以下[操作系统中运行](../Page/操作系统.md "wikilink")：

  - [AIX](../Page/AIX.md "wikilink")
  - [BSDI](../Page/BSD/OS.md "wikilink")
  - [Digital Unix](../Page/Digital_Unix.md "wikilink")
  - [FreeBSD](../Page/FreeBSD.md "wikilink")
  - [HP-UX](../Page/HP-UX.md "wikilink")
  - [IRIX](../Page/IRIX.md "wikilink")
  - [Linux](../Page/Linux.md "wikilink")
  - [Mac OS X](../Page/Mac_OS_X.md "wikilink")
  - [NetBSD](../Page/NetBSD.md "wikilink")
  - [NeXTStep](../Page/NeXTStep.md "wikilink")
  - [OpenBSD](../Page/OpenBSD.md "wikilink")
  - [SCO OpenServer](../Page/SCO_OpenServer.md "wikilink")
  - [Solaris](../Page/Solaris.md "wikilink")
  - [UnixWare](../Page/UnixWare.md "wikilink")
  - [Windows](../Page/Microsoft_Windows.md "wikilink")

## 參見

  - [Traffic Server](../Page/Traffic_Server.md "wikilink")

## 参考资料

[Category:自由代理服务器](../Category/自由代理服务器.md "wikilink")
[Category:反向代理](../Category/反向代理.md "wikilink")
[Category:Linux代理服务器软件](../Category/Linux代理服务器软件.md "wikilink")
[Category:Unix网络相关软件](../Category/Unix网络相关软件.md "wikilink")
[Category:Gopher客户端](../Category/Gopher客户端.md "wikilink")
[Category:自由跨平台軟體](../Category/自由跨平台軟體.md "wikilink")

1.