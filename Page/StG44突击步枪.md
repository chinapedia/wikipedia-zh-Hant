**StG44突击步枪**（，定型投产时名為**MP43**），是现代步兵史上划时代的成就之一。它是第一支使用了短药筒的[中间型威力枪弹并大规模装备以及真正意義上的](../Page/中间型威力枪弹.md "wikilink")[突擊步枪](../Page/突擊步枪.md "wikilink")。\[1\]

## 設計及歷史

[Haenel_Mkb_42(H).jpg](https://zh.wikipedia.org/wiki/File:Haenel_Mkb_42\(H\).jpg "fig:Haenel_Mkb_42(H).jpg")
[Bundesarchiv_Bild_146-1979-118-55,_Infanterist_mit_Sturmgewehr_44.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_146-1979-118-55,_Infanterist_mit_Sturmgewehr_44.jpg "fig:Bundesarchiv_Bild_146-1979-118-55,_Infanterist_mit_Sturmgewehr_44.jpg")
實驗證明二十世紀初的標準的步槍彈藥對自動步槍來說威力過大，當時的自動步槍像機槍般採用全威力彈，導致連發射擊時難以控制精度，其重量亦不方便單兵攜帶。到30年代後期，[德国陸軍開始研究威力小一些的短藥筒彈藥](../Page/德国.md "wikilink")，將[冲锋枪的特點結合到](../Page/冲锋枪.md "wikilink")[全自動步槍上](../Page/自动步枪.md "wikilink")，雖然射程不如步槍，但可保持比步槍輕便的特點並改善連發射擊精度。

黑內爾公司以槍械設計師[路易斯·施邁瑟擔任方案設計師](../Page/路易斯·施邁瑟.md "wikilink")，開始設計使用短藥筒彈藥的自動步槍。1941年，經過反覆實驗後德國成功研制一種[7.92×33毫米短彈](../Page/7.92×33mm_Kurz.md "wikilink")。它的長度比當時的德軍[7.92×57毫米標準步槍子彈短](../Page/7.92×57mm毛瑟.md "wikilink")，彈頭更輕，發射火藥也較少，使單兵能攜帶更多發彈藥；單發彈頭在有效射程內的威力足以癱瘓一名敵軍，全自動火力密度直逼衝鋒槍，後座力亦在可控制的範圍內。這類短彈被稱為[中间型威力枪弹](../Page/中间型威力枪弹.md "wikilink")，隨後能使用這種短槍彈的新型自動步槍很快被研制出來。

1942年黑內爾公司設計使用7.92×33毫米短槍彈的原型槍**MKb42**（MKb是德語Maschinenkarabiner的縮寫，意為：自動[卡賓槍](../Page/卡賓槍.md "wikilink")），經測試後被德國軍方選中。

  - 該槍採用氣導式自動原理，以及槍機偏轉式閉鎖方式，在擊發槍彈後，少部分氣體順著槍管上的小孔，經過導氣管導入機匣，用以推動槍機向後，完成拋殼、重新上膛和再擊發的過程。其單發閉鎖系統使槍手在單發射擊模式時，能在有效射程內保持合理的精確度，並有[全自動射擊模式以應付突發的近距離作戰](../Page/自動火器.md "wikilink")。
  - 此外，該槍可有效壓制衝鋒槍射程外的敵方火力據點，在數把突擊步槍密集開火時能發射大量子彈呼嘯劃過敵方及擊中敵人四周，對敵產生心理恐懼，將其壓制在掩體後方；密集的火力也大幅提高擊中敵方的機率，在笨重的班用輕機槍或者更多把的手動、半自動步槍以外提供了更方便的選擇，在當時同類輕武器中屬於劃時代的設計。
  - 槍械的弧形彈匣能裝入30發子彈，比所有現役步槍載彈量更高，減少戰時士兵更換彈夾的次數；前置的瞻孔能讓士兵於光線不足的環境中大致上瞄準目標，雖然如此會犧牲掉部分遠距打擊的精確度，但畢竟此武器原初設計上並非是作為精確射擊之用，而為了彌補這項缺憾，早期版本亦可加裝光學瞄準鏡。
  - 全槍大量採用沖壓工藝，生產成本較機械加工低和有效率，適合大規模生產。

原型槍進行了小批量生產，藉由空投方式火速送往東線戰場上一支正遭到紅軍圍困的德國部隊手上，使部隊成功突圍而出。其前衛的設計概念震撼紅軍，也因此激發俄國研製相似概念的武器，催生出舉世聞名的[AK-47](../Page/AK-47.md "wikilink")。

但是該槍的生產過程並非順遂，[納粹德國元首](../Page/納粹德國.md "wikilink")[希特勒早先因為尚未意識到當時戰爭型態已由一戰遠距壕溝戰演變至近距離城鎮戰](../Page/阿道夫·希特勒.md "wikilink")，而以該武器既無法打擊四百米以外的敵人，近距離火力密集度又不如現役衝鋒槍為理由，禁止生產這種不倫不類的武器。為了繞過[希特勒下令停止該槍研制的干擾](../Page/阿道夫·希特勒.md "wikilink")，順利投入生產，它借用了[冲锋枪的命名方式](../Page/冲锋枪.md "wikilink")，命名為**MP43**（Maschinenpistole
43）以暫時瞞過元首，但此槍的彈藥和威力實際上都是步槍的規格，而非手槍子彈等級。MP43隨後做了些許利於生產的設計變更後，定名為MP44，少量發配到部隊之中。

[MP44.jpg](https://zh.wikipedia.org/wiki/File:MP44.jpg "fig:MP44.jpg")
但紙總包不住火，1944年，[希特勒赴東線視察戰況](../Page/阿道夫·希特勒.md "wikilink")，當他問及德軍將士需要什麼時，將士們異口同聲回答:「更多的MP44！」希特勒很好奇這是什麼武器，一看之下發現是自己早已禁止生產的武器。希特勒一開始大為震怒，直到看過該槍火力展示後了解當初的看法有所偏差，於是宣佈把MP44定名為**Sturmgewehr
44**（意指突擊步槍1944年型），簡稱StG44，並優先生產該槍，這也是「突擊步槍」一名的由來。由於納粹德國瀕臨戰敗，到戰爭結束時該槍僅生產了40萬支而無法全面裝備德軍，但該槍在1944年曾普遍裝備於裝甲旅中，在相關戰役中對西線同盟軍帶來不小震撼。

可是，當盟軍的分析員研究過該槍後，認為它「笨重」和「不順手」，一旦故障就要丟棄；當它和堅硬的地面衝擊過後會導致槍身彎曲和槍栓卡死，而且因為它的反作用力彈弓藏在槍托裡，只要槍托被破壞的話整把槍就不能使用。不過有些負面評論實際上出於盟軍對它的厭惡而非客觀評價，亦也無法蓋過它在戰場上優秀的表現。

該槍的設計概念影響深遠，從最早的[AK-47](../Page/AK-47.md "wikilink")，到比利時原先構想[FAL時計劃採用的概念都受其影響](../Page/FN_FAL自動步槍.md "wikilink")；美軍直到越戰時遭到手持AK-47的越共士兵痛擊後，才由國內眾多步槍設計方案中選出概念最接近的[AR-15](../Page/AR-15自動步槍.md "wikilink")，命名為[M16並裝備美軍部隊](../Page/M16突擊步槍.md "wikilink")。從此該設計理念被廣泛運用至親蘇和親美國家的武器中。

除了[納粹德國有採用之外](../Page/納粹德國.md "wikilink")，[阿根廷也曾少量採購StG](../Page/阿根廷.md "wikilink")44用作測試。

## 二戰後的採用

[Bundesarchiv_Bild_183-33349-0002,_Neustrelitz,_Jahrestag_der_DDR,_Volkspolizei.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_183-33349-0002,_Neustrelitz,_Jahrestag_der_DDR,_Volkspolizei.jpg "fig:Bundesarchiv_Bild_183-33349-0002,_Neustrelitz,_Jahrestag_der_DDR,_Volkspolizei.jpg")\]\]

[東德](../Page/東德.md "wikilink")[國家人民軍及](../Page/國家人民軍_\(東德\).md "wikilink")[警察在戰後一段短時間內以](../Page/东德人民警察.md "wikilink")「MPi.44」的名義使用過StG44，其後在列裝[AK步槍後便把它們完全淘汰](../Page/AK-47突擊步槍.md "wikilink")。另外，[捷克斯洛伐克也曾裝備過StG](../Page/捷克斯洛伐克.md "wikilink")44。[南斯拉夫亦有把納粹德軍遺留的StG](../Page/南斯拉夫.md "wikilink")44配發給[南斯拉夫人民軍傘兵部隊使用](../Page/南斯拉夫人民軍.md "wikilink")，直到它們在1983年完全被[Zastava
M70取代為止](../Page/Zastava_M70突擊步槍.md "wikilink")；之後南斯拉夫把部份退役的StG44賣給[英國的收藏家及其他友好的](../Page/英國.md "wikilink")[中東和](../Page/中東.md "wikilink")[非洲國家](../Page/非洲.md "wikilink")。

[阿根廷曾在](../Page/阿根廷.md "wikilink")1940年代末至1950年代初於當地生產StG44的實驗版本，然而他們最後還是採用了[FN
FAL](../Page/FN_FAL自動步槍.md "wikilink")，這是考慮到[7.62×51毫米北約子彈在當時的普及程度和火力更強等因素](../Page/7.62×51mm_NATO.md "wikilink")，而且更能撇清該國跟納粹德國的聯繫。

在[冷戰期間蘇聯和其他](../Page/冷戰.md "wikilink")[東方陣營國家向一些友好國家和反政府游擊隊提供了大量在二戰期間繳獲的德軍槍械和彈藥](../Page/東方陣營.md "wikilink")，當中亦包括了StG44，其中法軍在[阿爾及利亞發現了大量StG](../Page/阿爾及利亞.md "wikilink")44並確認它們來自捷克斯洛伐克。[越南南方民族解放陣線在](../Page/越南南方民族解放陣線.md "wikilink")[越戰期間亦被發現使用該種步槍](../Page/越戰.md "wikilink")。目前一些在[中東和](../Page/中東.md "wikilink")[非洲之角的民兵和游擊隊仍然有限度使用StG](../Page/非洲之角.md "wikilink")44\[2\]，[美軍亦曾於](../Page/美軍.md "wikilink")[伊拉克叛軍及](../Page/伊拉克.md "wikilink")[民兵手上繳獲該槍](../Page/民兵.md "wikilink")。在[敘利亞內戰期間](../Page/敘利亞內戰.md "wikilink")，敘利亞叛軍也曾從敘利亞政府軍的軍火庫內繳獲五千多枝StG44並用於實戰，包括把它們安裝在[遙控武器系統上](../Page/遙控武器系統.md "wikilink")。\[3\]

## 當成轉角槍使用

轉角槍的意念最早源自於一戰。當時的澳洲軍隊曾製作過有關的組件，二戰時期的StG
44也可以在槍口內加裝轉角裝置以更有效地攻擊死角位的敵人\[4\]，而[以色列所研製的](../Page/以色列.md "wikilink")[墙角枪](../Page/墙角枪.md "wikilink")（Corner
Shot）則是21世紀的現代化產品。

## 現代重製型

目前[德國的](../Page/德國.md "wikilink")[HZA
Kulmbach公司推出了Mkb](../Page/HZA_Kulmbach公司.md "wikilink")42、MP43和StG44的半自動現代重製版，分別命名為「BD
42(H)」 、「BD 43/1」和「BD 44」。另外也有一些廠商宣佈將要生產多種現代口徑的StG44復刻槍。

## 使用國

[Bundesarchiv_Bild_101I-676-7996-13,_Infanterist_mit_Sturmgewehr_44.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_101I-676-7996-13,_Infanterist_mit_Sturmgewehr_44.jpg "fig:Bundesarchiv_Bild_101I-676-7996-13,_Infanterist_mit_Sturmgewehr_44.jpg")士兵\]\]
[Bundesarchiv_Bild_183-1985-0104-501,_Ardennenoffensive,_Grenadiere_in_Luxemburg.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_183-1985-0104-501,_Ardennenoffensive,_Grenadiere_in_Luxemburg.jpg "fig:Bundesarchiv_Bild_183-1985-0104-501,_Ardennenoffensive,_Grenadiere_in_Luxemburg.jpg")作戰的德軍士兵\]\]
[G3_and_StG44.jpg](https://zh.wikipedia.org/wiki/File:G3_and_StG44.jpg "fig:G3_and_StG44.jpg")與附有轉角裝置的StG44\]\]

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
### 非國家團體

  - [越南南方民族解放陣線](../Page/越南南方民族解放陣線.md "wikilink")
  - [索馬里](../Page/索馬里.md "wikilink")[民兵](../Page/民兵.md "wikilink")
  - [伊拉克叛軍](../Page/伊拉克.md "wikilink")
  - [敘利亞反對派和革命力量全國聯盟](../Page/敘利亞反對派和革命力量全國聯盟.md "wikilink")
  - [伊斯兰国](../Page/伊斯兰国.md "wikilink")

## 登場作品

### [電影](../Page/電影.md "wikilink")

  - 2004年—《[-{zh-cn:帝国的毁灭; zh-hk:希特拉的最後十二夜 ; zh-tw:帝國毀滅
    ;}-](../Page/帝國毀滅.md "wikilink")》（Der Untergang）：
    被[德意志國防軍士兵所使用](../Page/德意志國防軍.md "wikilink")。
  - 2014年—《[-{zh-cn:狂怒; zh-hk:戰逆豪情;
    zh-tw:怒火特攻隊;}-](../Page/怒火特攻隊.md "wikilink")》（Fury）：
    被唐恩·「戰爸」·柯利爾上士（[布萊德·彼特飾演](../Page/布萊德·彼特.md "wikilink")）和[德意志國防軍士兵所使用](../Page/德意志國防軍.md "wikilink")。

### [電視劇](../Page/電視劇.md "wikilink")

  - 2001年—《[-{zh-cn:兄弟连; zh-hk:雷霆傘兵; zh-tw:諾曼第大空降
    ;}-](../Page/兄弟連.md "wikilink")》（Band of
    Brothers）：被[德意志國防軍士兵所使用](../Page/德意志國防軍.md "wikilink")。

### [電子遊戲](../Page/電子遊戲.md "wikilink")

  - 2000年—《》（Medal of Honor: Underground）：命名為“MP44”。
  - 2002年—《[-{zh-hans:荣誉勋章：联合袭击;
    zh-hant:榮譽勳章：反攻諾曼第;}-](../Page/榮譽勳章：聯合襲擊.md "wikilink")》（Medal
    of Honor: Allied Assault）：命名為“StG-44”。
  - 2002年—《[-{zh-hans:战地1942;
    zh-hant:戰地風雲1942;}-](../Page/戰地風雲1942.md "wikilink")》（Battlefield:
    1942）：命名為“StG-44”。
  - 2003年—《[-{zh-hans:胜利之日;
    zh-hant:決勝之日;}-](../Page/決勝之日.md "wikilink")》（Day
    of Defeat）：命名為“StG-44”，[德意志國防軍陣營武器](../Page/德意志國防軍.md "wikilink")。
  - 2003年—《[-{zh-hans:使命召唤;
    zh-hant:決勝時刻;}-](../Page/使命召喚_\(遊戲\).md "wikilink")》（Call
    of
    Duty）：命名為“MP44”，奇怪的被描繪為[開放式槍機設計](../Page/開放式槍機.md "wikilink")，被[德意志國防軍所使用](../Page/德意志國防軍.md "wikilink")。
  - 2004年—《[-{zh-hans:使命召唤：联合进攻;
    zh-hant:決勝時刻：聯合行動;}-](../Page/決勝時刻：聯合行動.md "wikilink")》（Call
    of Duty: United
    Offensive）：命名為“MP44”，奇怪的被描繪為[開放式槍機設計](../Page/開放式槍機.md "wikilink")，被[德意志國防軍所使用](../Page/德意志國防軍.md "wikilink")。
  - 2004年—《[榮譽勳章：前線](../Page/榮譽勳章：前線.md "wikilink")》（Medal of Honor:
    Frontline）：命名為“StG-44”。
  - 2004年—《[-{zh-hans:胜利之日：起源;
    zh-hant:決勝之日：次世代;}-](../Page/決勝之日：次世代.md "wikilink")》（Day
    of Defeat:
    Source）：命名為“StG-44”，[德意志國防軍陣營武器](../Page/德意志國防軍.md "wikilink")。
  - 2005年—《[-{zh-hans:使命召唤;
    zh-hant:決勝時刻;}-2](../Page/決勝時刻2.md "wikilink")》（Call
    of Duty
    2）：命名為“MP44”，奇怪的被描繪為[開放式槍機設計](../Page/開放式槍機.md "wikilink")，被[德意志國防軍所使用](../Page/德意志國防軍.md "wikilink")。
  - 2006年—《[-{zh-hans:使命召唤;
    zh-hant:決勝時刻;}-3](../Page/決勝時刻3.md "wikilink")》（Call
    of Duty
    3）：命名為“MP44”，奇怪的被描繪為[開放式槍機設計](../Page/開放式槍機.md "wikilink")，被[德意志國防軍所使用](../Page/德意志國防軍.md "wikilink")。
  - 2006年—《》（Red Orchestra: Ostfront
    41-45）：命名為“Stg44”，被[德意志國防軍所使用](../Page/德意志國防軍.md "wikilink")。
  - 2007年—《》（Medal of Honor: Vanguard）：命名為“StG-44”。
  - 2007年—《[-{zh-hans:荣誉勋章：神兵天降;
    zh-hant:榮譽勳章：空降神兵;}-](../Page/榮譽勳章：空降神兵.md "wikilink")》（Medal
    of Honor: Airborne）：命名為“StG-44”。
  - 2007年—《[戰地之王](../Page/戰地之王.md "wikilink")》（Alliance of Valiant Arms）
  - 2007年—《[-{zh-hans:反恐精英;
    zh-hant:絕對武力;}-Online](../Page/絕對武力Online.md "wikilink")》（Counter-Strike
    Online）：命名為“StG44”。
  - 2007年—《[-{zh-hans:使命召唤4：现代战争;
    zh-hant:決勝時刻4：現代戰爭;}-](../Page/決勝時刻4：現代戰爭.md "wikilink")》（Call
    of Duty 4: Modern Warfare）：命名為“MP44”，只於聯機模式登場，無法進行任何改裝。
  - 2008年—《[-{zh-hans:使命召唤：世界战争;
    zh-hant:決勝時刻：戰爭世界;}-](../Page/決勝時刻：戰爭世界.md "wikilink")》（Call
    of Duty: World at
    War）：命名為“StG-44”，奇怪的有空倉掛機。單人模式中被[德意志國防軍和](../Page/德意志國防軍.md "wikilink")[武裝黨衛軍所使用](../Page/武裝黨衛軍.md "wikilink")。聯機模式可被所有陣營所使用，並可使用消焰器、光圈照準器、光學瞄準鏡。
  - 2009年—《[重返德軍總部](../Page/重返德軍總部.md "wikilink")》（Return to Castle
    Wolfenstein）
  - 2010年—《[-{zh-hans:使命召唤：黑色行动;
    zh-hant:決勝時刻：黑色行動;}-](../Page/決勝時刻：黑色行動.md "wikilink")》（Call
    of Duty: Black Ops）：命名為“StG-44”，奇怪的有空倉掛機。故事模式關卡“Project
    Nova”當中被[德意志國防軍所使用](../Page/德意志國防軍.md "wikilink")。
  - 2012年—《[-{zh-hans:使命召唤：黑色行动II;
    zh-hant:決勝時刻：黑色行動II;}-](../Page/決勝時刻：黑色行動II.md "wikilink")》（Call
    of Duty: Black Ops II）：只於殭屍模式中登場。
  - 2012年—《[战争前线](../Page/战争前线.md "wikilink")》（Warface）：命名为“StG44”，为步枪手专用武器，在二战纪念活动期间限时可购买时限版本。30发弹匣，可以改装前期生产型号光学瞄准镜，除此外无任何可改装部件。
  - 2014年—《[-{zh-cn:使命召唤：高级战争;
    zh-tw:決勝時刻：先進戰爭;}-](../Page/使命召唤：高级战争.md "wikilink")》（Call
    of Duty: Advanced Warfare）：於2015年8月的一次更新中添加到遊戲內，命名為“STG44”。
  - 2015年—《[-{zh-hans:使命召唤：黑色行动III;
    zh-hant:決勝時刻：黑色行動III;}-](../Page/決勝時刻：黑色行動III.md "wikilink")》（Call
    of Duty: Black Ops III）：只於殭屍模式中登場。
  - 2016年—《[-{zh-hans:使命召唤：现代战争;
    zh-hant:決勝時刻：現代戰爭;}-重製版](../Page/決勝時刻4：現代戰爭.md "wikilink")》（Call
    of Duty: Modern Warfare Remastered）：外觀比原作更精緻。
  - 2016年—《[英雄與將軍](../Page/英雄與將軍.md "wikilink")》（Heroes &
    Generals）：為德國步兵武器，可改装ZF-4光學瞄準鏡。
  - 2017年—《》
  - 2017年—《[-{zh-hans:使命召唤：二战;
    zh-hant:決勝時刻：二戰;}-](../Page/決勝時刻：二戰.md "wikilink")》（Call
    of Duty:
    WWII）：命名為“StG44”，使用“延長彈匣”時會改以延長的直形彈匣供彈。單人模式中被[德意志國防軍所使用](../Page/德意志國防軍.md "wikilink")。聯機模式時於等級15解鎖，可使用改裝包括：鏡片瞄具、快速出槍、[握把](../Page/輔助握把.md "wikilink")、反射式瞄具、大口徑、穩定瞄準、全金屬披甲彈、4倍瞄準鏡、速射、延長彈匣、高級[膛線](../Page/膛線.md "wikilink")、[刺刀](../Page/刺刀.md "wikilink")。
  - 2018年—《[-{zh-hans:战地5;
    zh-hant:戰地風雲5;}-](../Page/戰地風雲5.md "wikilink")》（Battlefield
    V）：命名為“StG
    44”，單機模式中被[德意志國防軍所使用](../Page/德意志國防軍.md "wikilink")。聯機模式中為突擊兵專用武器。

## 參見

  - [StG45突击步枪](../Page/StG45突击步枪.md "wikilink")
  - [FG42伞兵步枪](../Page/FG42伞兵步枪.md "wikilink")
  - [AK-47突击步枪](../Page/AK-47突击步枪.md "wikilink")
  - [Wimmersperg Spz-kr](../Page/Wimmersperg_Spz-kr.md "wikilink")
  - [費德洛夫M1916自動步槍](../Page/費德洛夫M1916自動步槍.md "wikilink")
  - [湯普森輕型步槍](../Page/湯普森輕型步槍.md "wikilink")

## 註釋

## 参考文献

  - [Machine Carbine Promoted from Tactical and Technical
    Trends](http://www.lonesentry.com/articles/ttt07/stg44-assault-rifle.html)
  - Shore, C. (Capt.), With British Snipers to the Reich, Samworth
    Press, 1948年
  - Weeks, John, World War II Small Arms, Galahad Books, 1979年
  - [Argentinian
    Military-MP44](http://www.ww2incolor.com/forum/showthread.php?p=89172)

## 外部連結

  - [1945年生產的MP44圖片](http://forums.accuratereloading.com/eve/forums/a/tpc/f/7811043/m/750108067)
  - [Modern Firearms
    StG44突击步枪](https://web.archive.org/web/20080913150058/http://world.guns.ru/assault/as51-e.htm)

[Category:自動步槍](../Category/自動步槍.md "wikilink")
[Category:突擊步槍](../Category/突擊步槍.md "wikilink")
[Category:7.92×33毫米槍械](../Category/7.92×33毫米槍械.md "wikilink")
[Category:導氣式槍械](../Category/導氣式槍械.md "wikilink")
[Category:德國槍械](../Category/德國槍械.md "wikilink")
[Category:德國二戰武器](../Category/德國二戰武器.md "wikilink")
[Category:德国发明](../Category/德国发明.md "wikilink")
[Category:越戰武器](../Category/越戰武器.md "wikilink")

1.  Bishop, Chris (1998), The Encyclopedia of Weapons of World War II,
    New York: Orbis Publiishing Ltd, ISBN 0-7607-1022-8
2.  <http://www.zweiterweltkrieg.org/phpBB2/viewtopic.php?p=88722>
3.  <http://www.thefirearmblog.com/blog/2012/08/22/sturmgewehr-44-used-by-syrian-rebels/>
4.  <http://www.youtube.com/watch?v=Z56SNHHL60U>