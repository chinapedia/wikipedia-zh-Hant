[Strrynight.png](https://zh.wikipedia.org/wiki/File:Strrynight.png "fig:Strrynight.png")
****是一款世界知名的[天文](../Page/天文.md "wikilink")[应用软件](../Page/应用软件.md "wikilink")。目前的最新版本是Pro
Plus 6.4.3版。

## 系统要求

[Windows](../Page/Windows.md "wikilink")：[Windows
XP](../Page/Windows_XP.md "wikilink")，800
MHz或更高[处理器](../Page/CPU.md "wikilink")，256 MB内存（推荐512 MB）3.0
GB硬盘空间。[OpenGL支持需要](../Page/OpenGL.md "wikilink")64
MBOpenGL®兼容[显卡](../Page/显卡.md "wikilink")。最小[显示器](../Page/显示器.md "wikilink")[分辨率为](../Page/分辨率.md "wikilink")1024×768。[DVD](../Page/DVD.md "wikilink")[光驱](../Page/光驱.md "wikilink")。

[Macintosh](../Page/Macintosh.md "wikilink")：[OS
X](../Page/OS_X.md "wikilink") 10.3或更高版本，G4 800 MHz或更高处理器，256 MB内存（推荐512
MB）3.0 GB硬盘空间。不支援OS
9.x以及之前的[操作系统](../Page/操作系统.md "wikilink")。OpenGL支持需要64
MB的OpenGL兼容[显卡](../Page/显卡.md "wikilink")。最小[显示器](../Page/显示器.md "wikilink")[分辨率为](../Page/分辨率.md "wikilink")1024×768。[DVD](../Page/DVD.md "wikilink")[光驱](../Page/光驱.md "wikilink")。

[Starry_Night_screenshot.png](https://zh.wikipedia.org/wiki/File:Starry_Night_screenshot.png "fig:Starry_Night_screenshot.png")下使用Starry
Night
Pro模拟前1096年9月19日的[天象](../Page/天象.md "wikilink")，并记录了之前的[火星轨道轨迹](../Page/火星.md "wikilink")\]\]

## 特色

  - Starry Night
    Pro提供超过20000张[天体全彩照片](../Page/天体.md "wikilink")。并且可以从网站上下载更多最新照片。这些图片皆可以缩放
  - 显示超过5500万颗星（如果在线可以查看5亿颗[恒星](../Page/恒星.md "wikilink")）
  - PGC目录包括100万个[天体](../Page/天体.md "wikilink")、[星系](../Page/星系.md "wikilink")
  - 自动上網更新最新天象
  - 导出兩萬[年内某一时刻的天体数据](../Page/年.md "wikilink")
  - 跟踪任意时刻[人造卫星的位置](../Page/人造卫星.md "wikilink")
  - 可以自定义选择地面样式
  - 提供来自[哈勃天文望远镜](../Page/哈勃天文望远镜.md "wikilink")、[钱德拉X射线天文台](../Page/钱德拉X射线天文台.md "wikilink")、[斯皮策空间望远镜的高解析度的](../Page/斯皮策空间望远镜.md "wikilink")[星座星体图像](../Page/星座.md "wikilink")
  - 可以通过飞行控制板游览[宇宙](../Page/宇宙.md "wikilink")
  - 可以接驳大多数电脑天文望远镜，使得寻星更为容易。输入星名即可自动定位。包括Orion SkyQuest
    Intelliscope、Meade LX-200、AutoStar和Celestron®
    Nexstar系列望远镜（需數據線連接上望遠鏡）
  - 自定义目镜设置
  - 可以对每个天体存储附注和上载自定义图像
  - 可以列印全天任意区域（180[度](../Page/度.md "wikilink")）的星名列表
  - 自动更新修正[彗星和](../Page/彗星.md "wikilink")[小行星運行參數以計算其位置](../Page/小行星.md "wikilink")

## 参见

  - [天文软件](../Page/天文软件.md "wikilink")
  - [KStars](../Page/KStars.md "wikilink")
  - [Cartes du Ciel](../Page/Cartes_du_Ciel.md "wikilink")
  - [Stellarium](../Page/Stellarium.md "wikilink")
  - [星光飞扬](../Page/星光飞扬.md "wikilink")

## 外部链接

  - [Starry Night官方网站](http://www.starrynight.com/)

[Category:应用软件](../Category/应用软件.md "wikilink")
[Category:天文软件](../Category/天文软件.md "wikilink")