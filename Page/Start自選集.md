《**Start自選集**》為[孫燕姿的專輯](../Page/孫燕姿.md "wikilink")，於2002年1月5日發行。除最後兩首歌曲為廣告代言，與創作歌曲《Someone》外，其他歌曲皆為自選翻唱。《Start自選集》發片僅一個月，台灣本地銷量便突破25萬張，亦獲得了2002年上半年專輯銷售總冠軍。

## 曲目

## 參考資料

[Category:孫燕姿音樂專輯](../Category/孫燕姿音樂專輯.md "wikilink")
[Category:台灣流行音樂專輯](../Category/台灣流行音樂專輯.md "wikilink")
[Category:新加坡音樂專輯](../Category/新加坡音樂專輯.md "wikilink")
[Category:2002年音樂專輯](../Category/2002年音樂專輯.md "wikilink")
[Category:翻唱專輯](../Category/翻唱專輯.md "wikilink")