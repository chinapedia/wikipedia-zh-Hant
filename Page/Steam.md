**Steam**是美國電子遊戲商[维尔福](../Page/维尔福.md "wikilink")（Valve）於2003年9月12日推出的[數位發行平台](../Page/數位發行.md "wikilink")，提供[数字版权管理](../Page/数字版权管理.md "wikilink")、[多人游戏](../Page/多人电子游戏.md "wikilink")、[流媒体和](../Page/流媒体.md "wikilink")[社交网络服务等功能](../Page/社交網路服務.md "wikilink")。借助Steam，用户能安装并自动更新游戏，也可以使用包括好友列表和组在内的社区功能，还能使用[云存储](../Page/云存储.md "wikilink")、游戏内语音和聊天功能。Steam软件免费提供了一个[应用程序接口](../Page/应用程序接口.md "wikilink")，称为Steamworks，开发商可以用来整合Steam的功能到自己的产品中，例如网络、在线对战、成就、微交易，并通过Steam创意工坊分享用户创作的内容。最初Steam只在[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")[操作系统发布](../Page/操作系统.md "wikilink")，但后来也发布了[OS
X和](../Page/MacOS.md "wikilink")[Linux版本](../Page/Linux.md "wikilink")。2010年以来，Steam推出了为[iOS](../Page/iOS.md "wikilink")、[Android和](../Page/Android.md "wikilink")[Windows
Phone设计的移动应用](../Page/Windows_Phone.md "wikilink")，与电脑版软件实现互联。

Steam被认为是[电脑游戏界最大的数碼发行平台](../Page/电脑游戏.md "wikilink")。2013年10月，Screen
Digest估计Steam的市场份额有75%。\[1\]2015年，跟踪网站Steam
Spy估计，用户从Steam商店或第三方供应商购买的Steam游戏总额约为35亿美元，占当年PC游戏销售总额的15%。\[2\]\[3\]2017年末，跟踪网站Steam
Spy的数据表示Steam已有2.81亿注册账户，同时在线用户1750万。\[4\]\[5\]Steam平台的成功也让维尔福开始研发[Steam主机和](../Page/Steam_Machine.md "wikilink")[SteamOS](../Page/SteamOS.md "wikilink")。

## 历史

### 早期

在Steam推出之前，维尔福公司在为[絕對武力等](../Page/絕對武力.md "wikilink")[電子遊戲更新時遇到了難題](../Page/電子遊戲.md "wikilink")，发布一个补丁可能导致大部分玩家断线数天。此後，维尔福團隊決定要製作一個能自动更新游戏的平台，并整合更严格的反盜版及反作弊系统。2002年公布开发计划时的用户调查显示，75%的Valve用户使用高速互联网连接，并且一定会在未来的几年逐步增长，而Valve也认识到通过數碼發行的渠道能更快速地为用户提供游戏更新。\[6\]在正式製作前，Valve公司曾接觸多個公司以合作開發平台，當中有[微軟](../Page/微軟.md "wikilink")、[雅虎和](../Page/雅虎.md "wikilink")[RealNetworks](../Page/RealNetworks.md "wikilink")，但被拒絕。\[7\]

Steam的開發工作于2002年开始，計劃為代號「Grid」（網格）及「Gazelle」（羚羊）。\[8\]\[9\]Steam的首次公開亮相是在2002年3月22日的[遊戲開發者大會中](../Page/遊戲開發者大會.md "wikilink")，當時完全是以分布式网络的概念来设计的。\[10\]為了展示Steam平台的整合性，[Relic
Entertainment設計了一特別版本的](../Page/遺迹娛樂.md "wikilink")[不可思议的生物](../Page/不可思议的生物.md "wikilink")，\[11\]不过直到2015年该游戏才在Steam上发售。Valve公司在此計劃中曾和多個公司合作，包括[AT\&T](../Page/AT&T.md "wikilink")、[宏碁及](../Page/宏碁.md "wikilink")[GameSpy](../Page/GameSpy.md "wikilink")。第一個在Steam发布的[遊戲模組是](../Page/遊戲模組.md "wikilink")[決勝之日](../Page/決勝之日.md "wikilink")。\[12\]

Steam客户端的第一个测试版本于2003年1月[CS
1.6公测时发布](../Page/反恐精英.md "wikilink")，当时作为必须安装和使用的组件。当时，Steam的主要功能是简化在线游戏中常见的补丁过程。对于其他所有游戏，Steam是可选安装的组件。测试期间大约有8至30万名玩家测试了Steam系统。\[13\]\[14\]而Steam系统和网站因為數千用户同时下载絕對武力的更新版本而不勝負荷。\[15\]2004年，[世界對抗網絡系統](../Page/世界對抗網絡系統.md "wikilink")（WON）关闭并由Steam平台取代，以前需要WON來登入連機模式的遊戲从此以后必须通过Steam联机。\[16\]

經過一段時間的試驗，Valve開始聯絡不同的[遊戲發行商和](../Page/電子遊戲發行商.md "wikilink")[獨立遊戲開發者簽署合約並為他們提供Steam平台的遊戲銷售服務](../Page/游戏开发者#独立开发者.md "wikilink")。兩個最早期在Steam平台上推出的獨立遊戲是[Rag
Doll Kung
Fu和](../Page/Rag_Doll_Kung_Fu.md "wikilink")[Darwinia](../Page/Darwinia.md "wikilink")。\[17\]2005年12月，加拿大遊戲發行商[Strategy
First亦和Valve合作](../Page/Strategy_First.md "wikilink")，在Steam发行自己现有和未来的游戏。\[18\]2002年，Valve总经理[加布·纽维尔宣布](../Page/加布·纽维尔.md "wikilink")[遊戲模組制作组可以花费](../Page/遊戲模組.md "wikilink")获得游戏引擎授权并通过Steam发行模组。\[19\]Valve的[半条命2是第一款需要Steam才能运行的游戏](../Page/半条命2.md "wikilink")，包括零售版。这一决定引发玩家对于软件所有权、软件需求以及在之前反恐精英测试时发生的服务器拥塞问题的关注。\[20\]在此期间用户在游戏过程遇到诸多问题。\[21\]\[22\]\[23\]

### 盈利

从2005年10月起，第三方遊戲開始在Steam平台上提供给用户购买和下载，最早的一个是Rag Doll Kung
Fu，\[24\]同时Valve亦表示Steam由于一些自家推出的成功遊戲而獲得盈利。雖然当时[數碼發行的銷售模式的售出量仍不及零售發行](../Page/數碼發行.md "wikilink")，但Valve及游戏開發商在Steam的盈利還是远高於零售方式。\[25\]\[26\]

2007年，[id Software](../Page/id_Software.md "wikilink")\[27\]、[Eidos
Interactive](../Page/Eidos.md "wikilink")\[28\]和[卡普空](../Page/卡普空.md "wikilink")\[29\]等規模較大的遊戲開發發行商開始在Steam銷售他們的遊戲。同年五月，Steam平台上已經有一千三百萬個帳號被建立，共有150個遊戲在Steam上售賣。\[30\]\[31\]及後十月，Steam平台上亦陸續推出《[橘盒](../Page/橘盒.md "wikilink")》、《[生化奇兵](../Page/生化奇兵.md "wikilink")》、《[決勝時刻4：現代戰爭](../Page/決勝時刻4：現代戰爭.md "wikilink")》和《[浩劫殺陣：車諾比之影](../Page/浩劫殺陣：車諾比之影.md "wikilink")》等等的高知名度遊戲。

### 近況

  - 2010年3月，Valve開放Steam翻譯伺服器，服務可供Steam用户幫助遊戲的翻譯工作。該項服務一直測試至同年10月。
  - 2011年1至2月，發生「俄區大封號」或稱「清洗俄區」。中華人民共和國用戶最受影響，在不良代購及盜版聞名的時代下，Steam進行大量封鎖帳號的活動。結合受影響的用戶經驗，封號與違法序列号、跨區購物有關。
  - 2011年4月下旬，Steam帳户与[PS3帳户连接](../Page/PS3.md "wikilink")，实现跨平台游戏配对、对战、好友系统、聊天和成就等。PS3上的Steam也可以把合作模式游戏进度和单人游戏存档保存在Steam云端，使玩家在任意的PS3主机上继续他们的游戏进度。
  - 2011年5月初，Steam推出視訊分享功能，藉助全球最大的視訊分享網站[Youtube實現Steam用戶間的視訊分享](../Page/Youtube.md "wikilink")，付費用戶可以在社群介面進行Steam帳戶與Youtube帳號的連線，從而在Steam社群中實現方便快捷的玩家間的視訊分享。
  - 2011年6月上旬，Steam帳戶可與Facebook帳戶進行聯結，從而可以更加方便快捷的尋找好友，這使得Steam的交友功能變得更加完善。
  - 2011年11月6日，黑客入侵Steam，盜取用戶個人資料，並使社群論壇關閉。11月10日，Valve表示此次事件中，有一個保存顧客資料的[數據庫被入侵](../Page/數據庫.md "wikilink")，黑客可存取玩家的資料例如：加密[密碼和](../Page/密碼.md "wikilink")[信用卡資料](../Page/信用卡.md "wikilink")。\[32\]而當時的[Valve根本還未意識到用戶資料有被入侵的風險](../Page/維爾福.md "wikilink")，只有警告玩家自我提防。
  - 2012年9月，Valve启动Steam青睐之光，旨在征集社区帮助，挑选新游戏在Steam上发布。游戏开发者可以先上传游戏信息，随后玩家根据游戏信息进行投票，Valve根据投票结果决定游戏是否在Steam上架。
  - 2013年9月，官方宣布親友分享功能，並開放公開測試。此功能讓限定的親友，在單一的Steam客戶端上（即同一台機器上），選擇性地分享各自擁有的遊戲。
  - 2014年3月，官方宣布正式開放親友分享功能。
  - 2014年5月，公布家中串流功能。此功能使家中兩台在同區域網路的電腦登入Steam後能互相溝通，並能在一端玩另一端安裝的遊戲。例如在筆記型電腦上玩只安裝在桌上型電腦內的遊戲，所有遊戲的計算都由桌上型電腦提供，筆記型電腦只提供使用者介面。
  - 2014年10月25日前，得到測試版硬體測試候選資格的300個人，可免費得到史上第一台Steam遊戲主機。
  - 2014年12月，宣布實況直播功能並公開測試。此功能讓玩家選擇性地開放給其他人即時觀看該玩家遊戲的進行。
  - 2015年3月27日，Steam表示可以在同年11月開始訂購Steam Controller（Steam的遊戲手柄）。
  - 2015年6月，正式公布遊戲退款方案與受理退款請求。
  - 2015年10月30日，Steam表示在11月3日特賣活動結束後，將支援人民幣，港元，新台幣，印度盧比結算，各有關區域用戶之前的美元資金，屆時將按照匯率轉換成相應貨幣，存入用戶帳戶中。\[33\]
  - 2016年4月26日，Steam新增租借線上串流電影功能。\[34\]
  - 2016年10月，Steam支援[微信支付](../Page/微信.md "wikilink")。
  - 2016年11月24日，Steam支援[财付通支付](../Page/财付通.md "wikilink")，并且重新恢复[支付宝支付](../Page/支付宝.md "wikilink")。
  - 2017年6月13日，Steam青睐之光正式关闭，被SteamDirect取代。
  - 2017年12月16日起，Steam社区域名在中国大陆地区疑似遭到[防火长城关键字屏蔽和DNS污染](../Page/防火长城.md "wikilink")，原因未知。\[35\]但根据[百度贴吧一名吧友的说法](../Page/百度贴吧.md "wikilink")，steam社区功能被屏蔽之前，steam官方在2017年6月强制解散了一个名叫“[中国共产党](../Page/中国共产党.md "wikilink")”的群组。有可能是被屏蔽的原因之一。
  - 2018年6月12日，中国[完美世界股份有限公司获得](../Page/完美世界股份有限公司.md "wikilink")[維爾福公司的授权](../Page/維爾福公司.md "wikilink")，代理Steam中国。\[36\]
  - 2018年11月28日，Steam中国正式落户上海浦东。\[37\]

## 地區內容限制

Steam上提供的內容存在地區限制，多個遊戲或軟體只在特定的地區進行數位發售，當訪問存在地區限制的內容會進行提示。Steam會檢查[IP地址](../Page/IP地址.md "wikilink")，核對帳單地址是否真實及配合支付歷史記錄等來避免用戶繞過地區購買限制，當用戶多次繞過地區限制進行購買，根據Steam用戶合約，Valve公司有權封禁用戶\[38\]。

## Steam平台的功能

### 客户端安全性

Steam平台的用戶必須先安裝Steam客戶端軟體，并經註冊後才能享有Steam平台的使用權；而平台上的付費遊戲，必須透過代理廠商購買產品，或是透過平台上的線上訂購，取得付費遊戲序號後，才可擁有下載以及安裝權。而每個Steam帳戶除了帳號、密碼和安全問題外，亦有一組獨一無二的Steam識別碼。

現在，Steam平台的客户端有Steam Guard，這個功能旨在保护Steam用戶帳戶的安全。Steam
Guard首先需要验证Steam帳戶与一个電子郵箱關聯，當Steam用戶使用一台未知電腦登入Steam帳戶的同時，發送一个包含驗證碼的電子郵件到關聯郵箱中，然后通過在驗證介面輸入驗證碼來確認登入。由此可見Steam
Guard加強了Steam帳戶的安全性能。

### 線上購物系統

Steam採用[Global
Collect公司的支付系統進行電子支付](../Page/Global_Collect.md "wikilink")，幣種為[美元](../Page/美元.md "wikilink")，[俄羅斯](../Page/俄羅斯.md "wikilink")、[巴西和](../Page/巴西.md "wikilink")[香港等本土化地区採用本地幣種進行支付](../Page/香港.md "wikilink")，採用區域定價策略。俄羅斯與[獨聯體國家普遍定價較低](../Page/獨立國家聯合體.md "wikilink")，有部份遊戲限為該區的版本就是「俄區遊戲」。Steam目前支持包括[PayPal](../Page/PayPal.md "wikilink")、[VISA](../Page/VISA.md "wikilink")、[MasterCard](../Page/MasterCard.md "wikilink")、[American
Express](../Page/American_Express.md "wikilink")、[Discover](../Page/发现卡.md "wikilink")、[JCB](../Page/JCB.md "wikilink")、[银联](../Page/银联.md "wikilink")、[微信](../Page/微信.md "wikilink")、[支付宝](../Page/支付宝.md "wikilink")（中国大陆）、ClickandBuy、[MoneyBookers](../Page/MoneyBookers.md "wikilink")、PaySafeCard、、、[Carte
Bleue](../Page/Carte_Bleue.md "wikilink")、DIRECTebanking、Boleto
Bancário、Hipercard等在內的多個國際或地區支付方式。

2016年4月，威爾烏開始接受[比特幣進行電子支付](../Page/比特幣.md "wikilink")，但在2017年12月宣佈不再接受比特幣，理由是手續費暴增且幣值波動劇烈。\[39\]

### 反作弊反盜版

維爾福公司在Steam的網頁論壇上宣佈了新的[維爾福反作弊系統](../Page/維爾福反作弊系統.md "wikilink")(VAC2)的問世，並進入了最後的測試階段。VAC2支持所有由維爾福出品的遊戲及第三方模組。這個反作弊系統會禁止作弊人士的帳號連接使用VAC的伺服器。VAC2將會掃描電腦的記憶體，確保玩家對遊戲中的畫面、速度等沒有任何改變。

VAC不僅在反作弊中有用，同時也會阻止盜版使用者進入多人遊戲內。到現在VAC也還沒有傳出被人成功破解的消息。

不過，有玩家批評，VAC會誤判，其原因可能是電腦裡有疑似作弊的程式。并且在CSGO游戏中外挂成风，VAC并没有起到什么关键性的作用。然而，VAC是根據使用者帳號的封鎖，不波及其他共用電腦的帳號。

### 自動更新系統

能夠自動進行更新，而不需玩家自己手動更新，但在初期時常發生在更新時突然當機的情況。隨著Valve公司不斷改善，這個問題已獲得解決，整個系統也變得越來越實用。而除了當機的問題外，初期也發生過頻寬不足的問題，原因在於更新時系統會自動選擇最快速的伺服器來進行更新檔下载，這問題則直到Valve公司尋求外來資助後才逐漸獲得解決。

### 好友社區系統

好友系統使得玩家們有了新的交流方式。就如[MSN一樣](../Page/MSN.md "wikilink")，好友系統也可以儲存好友於自己的名單中，且顯示出好友是否上線或在忙碌中等狀態，並了解其在於全球網路的哪個伺服器上。同時，Steam平台上亦有玩家群組和討論區。(須充值5美元於STEAM錢包內或累计5美元購買遊戲/軟件解锁完整的好友功能)

从2008年起，Steam支持了Steam云功能，玩家可以上传游戏截图到社区分享，也可以使用云存档功能实现游戏进度的同步。\[40\]但是Valve要求游戏制作者合理使用云功能，玩家可以选择禁用某一特定游戏的Steam云功能。\[41\]2012年五月，Steam更新了远程控制功能，可以在电脑网页端和手机移动端通过云服务控制游戏的更新和下载。\[42\]

### 伺服器控制台

這使得玩家不需要開啟遊戲就能知道伺服器的狀況，包括伺服器的名稱、玩家數量、連線狀態等，也可使用滑鼠右鍵將自己常用的伺服器加入我的最愛。還有可從歷史列表知道玩家之前去過的伺服器，以及知道好友目前在哪個伺服器中。

### 遊戲收藏列表

遊戲列表可使玩家選擇戰慄時空系列的其他模組遊玩，或者是藉此下載其他的模組，以節省玩家的時間；玩家只需要在列表前面的圖示上點击一下便可下載遊戲或開啟遊戲進行更新。

### SteamOS

SteamOS包括了Linux的架构以及为大屏幕模式设计的游戏体验。SteamOS四项主要功能：⑴家用流媒体、⑵音乐，电视，电影、⑶家庭分享、⑷家长控制。

## 中国大陆的封锁

2017年12月16日开始，
因疑似被[防火长城](../Page/防火长城.md "wikilink")[封锁steamcommunity](../Page/中华人民共和国被封锁网站列表.md "wikilink").com域名下的steam社区内容，导致在中国大陆的用户除了商店外无法访问其他任何功能，需要修改[hosts或使用加速器等相关工具才能绕过封锁](../Page/hosts.md "wikilink")。\[43\]

## 默认启用https链接

2018年4月28日起，steam在全球除中国大陆地区以外启用默认https链接代替之前的默认http链接，此举被认为可以降低用户数据被ISP劫持分析的可能性。\[44\]中国大陆地区此时只有商店采用默认https链接，社区仍然是默认http链接。

2018年6月2日，steam在中国大陆地区采用社区默认https链接，由此提供了一个非常稳定的hosts（103.86.70.254
store.steampowered.com、103.86.70.254
steamcommunity.com），在hosts文件里添加这两行代码后即可直连社区。但该hosts在稳定使用三个多月后失效，玩家仍需要不断修改hosts或使用加速器等工具才能访问社区。\[45\]

## 参考资料

## 注释

## 外部链接

  -
  - [Steam社區](http://steamcommunity.com/)

  - [維爾福公司](http://www.valvesoftware.com/)

[no:Valve
Corporation\#Steam](../Page/no:Valve_Corporation#Steam.md "wikilink")

[S](../Category/2003年軟體.md "wikilink")
[Steam](../Category/Steam.md "wikilink")
[S](../Category/Steam商店遊戲.md "wikilink")
[S](../Category/Windows軟體.md "wikilink")
[S](../Category/MacOS軟體.md "wikilink")
[S](../Category/雲端運算.md "wikilink")
[S](../Category/電子遊戲線上零售商.md "wikilink")
[S](../Category/威爾烏.md "wikilink") [S](../Category/虛擬經濟.md "wikilink")
[S](../Category/網站.md "wikilink") [S](../Category/電子商務.md "wikilink")
[S](../Category/數位版權管理.md "wikilink")
[S](../Category/多人遊戲服務.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.

12.

13.
14.

15.

16.

17.

18.

19.
20.

21.
22.

23.

24.

25.

26.

27.

28.

29.

30.

31.

32.

33.

34.

35.

36.

37. [Steam中国落户上海浦东](http://tech.ifeng.com/a/20181129/45240723_0.shtml)

38. Steam 用戶合約:
    [英文版](http://store.steampowered.com/subscriber_agreement/?l=english)

39. [Steam 不再支援
    Bitcoin](https://steamcommunity.com/games/593110/announcements/detail/1464096684955433613)

40.

41.

42.

43. [Steam社区被关键字屏蔽](http://www.williamlong.info/archives/5191.html?utm_medium=website)

44. [Steam 开始默认启用加密，但中国区除外](https://www.solidot.org/story?sid=56316)

45.