在[数学中](../Page/数学.md "wikilink")，**斯通氏布尔代数表示定理**声称所有[布尔代数都](../Page/布尔代数.md "wikilink")[同构于](../Page/同构.md "wikilink")[集合域](../Page/集合域.md "wikilink")。这个定理是深入理解在二十世纪上半叶所拓展的[布尔代数的基础](../Page/布尔代数.md "wikilink")。这个定理首先由[斯通氏](../Page/马歇尔·哈维·斯通.md "wikilink")（1936年）证明，并以他的姓氏命名。斯通氏通过他对[希尔伯特空间上的](../Page/希尔伯特空间.md "wikilink")[算子的](../Page/线性算子.md "wikilink")[谱理论的研究而得出了它](../Page/谱理论.md "wikilink")。

## 定理

斯通氏表示定理断言[布尔代数](../Page/布尔代数.md "wikilink")[同构于如下形式的它的那些](../Page/同构.md "wikilink")[超滤子的集合的所有](../Page/超滤子.md "wikilink")[子集的代数](../Page/子集.md "wikilink")， 对布尔代数的某个元素 。

可能令人惊奇，它的证明要求[选择公理](../Page/选择公理.md "wikilink")。这个定理等价于声称所有布尔代数都有素理想的[布尔素理想定理](../Page/布尔素理想定理.md "wikilink")，它的证明也要求[选择公理](../Page/选择公理.md "wikilink")。然而斯通氏表示定理要严格弱于选择公理。

对非[布尔代数的其他特定](../Page/布尔代数.md "wikilink")[代数结构也存在类似的定理](../Page/代数结构.md "wikilink")。例如，所有[群都同构于](../Page/群.md "wikilink")[变换群](../Page/变换群.md "wikilink")，这里的[函数复合解释](../Page/函数复合.md "wikilink")[群乘积](../Page/群子集的乘积.md "wikilink")。

## 与拓扑学和范畴论的联系

这个定理可以用[拓扑学和](../Page/拓扑学.md "wikilink")[范畴论的语言来重述如下](../Page/范畴论.md "wikilink")。斯通氏表示定理断言在[布尔代数](../Page/布尔代数.md "wikilink")[范畴和](../Page/范畴论.md "wikilink")**[斯通氏空间](../Page/斯通氏空间.md "wikilink")**，也就是[完全不连通](../Page/完全不连通空间.md "wikilink")[紧致](../Page/紧致空间.md "wikilink")[豪斯多夫](../Page/豪斯多夫空间.md "wikilink")[拓扑空间](../Page/拓扑空间.md "wikilink")（也叫做**布尔空间**）范畴之间的[对偶](../Page/范畴对偶性.md "wikilink")。

这个定理是[斯通氏对偶性的特殊情况](../Page/斯通氏对偶性.md "wikilink")，它是在[拓扑空间和](../Page/拓扑空间.md "wikilink")[偏序集合之间的对偶性的一般性框架](../Page/偏序集合.md "wikilink")。在布尔代数的范畴内，[态射是](../Page/态射.md "wikilink")[布尔同态](../Page/布尔代数#同态和同构.md "wikilink")。在斯通氏空间的范畴内，态射是[连续函数](../Page/连续函数_\(拓扑学\).md "wikilink")。斯通氏对偶性把利用[真值表特征化有限布尔代数推广到了命题的无限集合](../Page/真值表.md "wikilink")。它系统性的利用了[两元素布尔代数](../Page/两元素布尔代数.md "wikilink")**2**作为[同态的目标](../Page/同态.md "wikilink")，它的载体是{0,1}或[真值](../Page/真值.md "wikilink"){F,T}。

布尔代数  的[斯通氏空间是在](../Page/斯通氏空间.md "wikilink")  上的所有二值同态的集合，带有这种同态的[网](../Page/网_\(数学\).md "wikilink")[逐点收敛的拓扑](../Page/逐点收敛.md "wikilink")。（构造  的斯通氏空间的可替代和等价的方式是作为  中所有[超滤子的集合](../Page/超滤子.md "wikilink")，带有对每个  中的  的集合  都是这个拓扑的[基](../Page/基_\(拓扑学\).md "wikilink")。我们使用了下面的同态方式。）

从布尔代数  到布尔代数  同态以自然方式对应于从斯通氏空间  到斯通氏空间  的连续函数。换句话说，这种对偶性是[逆变函子](../Page/逆变函子.md "wikilink")。

所有布尔代数都[同构与它的斯通氏空间的](../Page/同构.md "wikilink")[闭开](../Page/闭开集.md "wikilink")（就是说同时是闭集和开集）[子集的代数](../Page/子集.md "wikilink")。这个同构把任何  的元素  映射到把  映射到 1 的那些同态的集合。

所有完全不连通[紧致](../Page/紧致空间.md "wikilink")[豪斯多夫空间都](../Page/豪斯多夫空间.md "wikilink")[同胚于所有它的](../Page/同胚.md "wikilink")[闭开子集的布尔代数的斯通氏空间](../Page/闭开集.md "wikilink")。这个同胚把每个点  映射到 2-值同态 ，它依据  或  给出 
1或0}}。

## 引用

  - [集合域](../Page/集合域.md "wikilink")
  - [布尔代数主题列表](../Page/布尔代数主题列表.md "wikilink")
  - [斯通氏空间](../Page/斯通氏空间.md "wikilink")

## 引用

  - [Paul Halmos](../Page/Paul_Halmos.md "wikilink")，and Givant, Steven
    (1998) *Logic as Algebra*. Dolciani Mathematical Expositions No. 21.
    [The Mathematical Association of
    America](../Page/The_Mathematical_Association_of_America.md "wikilink")。
  - Johnstone, Peter T. (1982) *Stone Spaces*. Cambridge University
    Press. ISBN 0-521-23893-5.
  - [Marshall H. Stone](../Page/Marshall_H._Stone.md "wikilink") (1936)
    "[The Theory of Representations of Boolean
    Algebras,](http://links.jstor.org/sici?sici=0002-9947%28193607%2940%3A1%3C37%3ATTORFB%3E2.0.CO%3B2-8)"
    *Transactions of the American Mathematical Society 40*: 37-111.

A monograph available free online:

  - Burris, Stanley N., and H.P. Sankappanavar, H. P.(1981) *[A Course
    in Universal
    Algebra.](http://www.thoralf.uwaterloo.ca/htdocs/ualg.html)*
    Springer-Verlag. ISBN 3-540-90578-2.

[Category:点集拓扑学](../Category/点集拓扑学.md "wikilink")
[Category:布尔代数](../Category/布尔代数.md "wikilink")
[Category:代数定理](../Category/代数定理.md "wikilink")