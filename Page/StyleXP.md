**StyleXP**是一個設計用來修改[Windows
XP用戶界面的](../Page/Windows_XP.md "wikilink")[電腦程式](../Page/電腦程式.md "wikilink")，到版本3.19為止可修改主題、瀏覽器列、背景、登入畫面、圖示、開機畫面、透明度、指標與螢幕保護程式。

## 歷史

由TGTSoft創作，StyleXP是其中一個像[Stardock的](../Page/Stardock.md "wikilink")[WindowBlinds與](../Page/WindowBlinds.md "wikilink")[Object
Desktop等可供選擇的更改外殼程式之一](../Page/Object_Desktop.md "wikilink")。

## 如何運作

StyleXP會修改一個名為uxtheme.dll的[DLL檔案](../Page/動態連結庫.md "wikilink")，Uxtheme.dll預設禁止用戶安裝未經[微軟](../Page/微軟.md "wikilink")[數位簽署的主題](../Page/數位簽署.md "wikilink")，透過修改這個DLL檔StyleXP可以安裝未經數位簽署主題。該程序的早期版本會修改硬盤上的uxtheme.dll檔，而新版會在[記憶體上實行](../Page/記憶體.md "wikilink")。

## 流行

該程式的流行度在近幾年有所提升，同時有數個提供免費外殼予公眾的網站設立。該些外殼在一個讓用戶不需購買StyleXP來使用它們，經修改過的uxtheme[動態連結庫發佈後更加流行](../Page/動態連結庫.md "wikilink")。

## 外部連結

  - [StyleXP官方網頁](https://web.archive.org/web/20070405191802/http://www.tgtsoft.com/prod_sxp.php)
  - [deviantART上的](../Page/deviantART.md "wikilink")[Visual
    Style存庫](http://browse.deviantart.com/?catpath=customization/skins/windows/visualstyle/&offset=24&order=9&alltime=yes)
  - [Belchfire.net的主題庫](https://web.archive.org/web/20070405091553/http://themes.belchfire.net/)

[Category:Windows軟體](../Category/Windows軟體.md "wikilink")
[Category:圖形用戶界面](../Category/圖形用戶界面.md "wikilink")