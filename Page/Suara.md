**Suara**（），[日本女性歌手](../Page/日本.md "wikilink")，出生且出身於[大阪府](../Page/大阪府.md "wikilink")，母親出生於台灣基隆\[1\]。隷屬於[FIX
Records](../Page/FIX_Records.md "wikilink")，由[Lantis製作及發行](../Page/Lantis.md "wikilink")。作詞及作曲時會使用本名**巽明子**。

在她的官网上提到，Suara是印度尼西亚语“声音”的意思。

## 概要

  - 曾就讀[大阪外國語大學](../Page/大阪外國語大學.md "wikilink")[印尼语學系](../Page/印尼语.md "wikilink")。
  - 2009年9月19日時在個人的網誌上公開與[Elements
    Garden所屬的作曲家](../Page/Elements_Garden.md "wikilink")[藤田淳平結婚的消息](../Page/藤田淳平.md "wikilink")\[2\]
    。
  - 2017年2月來台舉辦演唱會\[3\]。

## 唱片目錄

### 單曲

1.  **夢想歌**，2006年4月26日發售

    1.  夢想歌
          - 电视动画《[传颂之物](../Page/传颂之物.md "wikilink")》片头曲
    2.  星想夜曲
          - 电视动画《[传颂之物](../Page/传颂之物.md "wikilink")》印象曲
    3.  夢想歌（Instrumental）
    4.  星想夜曲（Instrumental）

2.  ，2006年10月25日發售

    1.    - 电视动画《[后天的方向](../Page/后天的方向.md "wikilink")》片头曲

    2.  傘

          - 电视动画《[后天的方向](../Page/后天的方向.md "wikilink")》插曲

    3.
    4.  傘（Instrumental）

3.  **一番星**，2007年2月28日發售

    1.  一番星
          - OVA《[ToHeart2](../Page/ToHeart2.md "wikilink")》片头曲
    2.  I am
    3.  一番星（Instrumental）
    4.  I am（instrumental）

4.  **BLUE／蕾-blue dreams-**，2007年10月24日發售

    1.  BLUE
          - 电视动画《[BLUE DROP](../Page/BLUE_DROP.md "wikilink")》片头曲
    2.  蕾-blue dreams-
          - 电视动画《[BLUE DROP](../Page/BLUE_DROP.md "wikilink")》片尾曲
    3.  BLUE（Instrumental）
    4.  蕾-blue dreams-（Instrumental）

5.  ，2008年1月23日发售

    1.    - 电视动画《[君吻](../Page/君吻.md "wikilink")》新片尾曲

    2.
    3.
    4.
6.  ，2009年1月28日发售

    1.    - 电视动画《[白色相簿](../Page/白色相簿.md "wikilink")》片尾曲

    2.
    3.
    4.
7.  ，2009年4月22日发售

    1.    - 电视动画《[花冠之淚](../Page/花冠之淚.md "wikilink")》片头曲

    2.
    3.
    4.
8.  ，2009年6月24日发售

    1.    - OVA《[传颂之物](../Page/传颂之物.md "wikilink")》片头曲

    2.
    3.
    4.
9.  ，2009年10月28日发售

    1.    - 电视动画《[白色相簿](../Page/白色相簿.md "wikilink")》后期片尾曲

    2.
    3.
    4.
10. ，2014年1月15日發售

    1.
    2.
    3.
    4.
11. ，2015年8月26日發售

    1.    - 遊戲《[傳頌之物 虛偽的假面](../Page/傳頌之物_虛偽的假面.md "wikilink")》片头曲

12. ，2015年11月4日發售

    1.    - 电视动画《[傳頌之物 虛偽的假面](../Page/傳頌之物_虛偽的假面.md "wikilink")》片头曲

    2.    - 电视动画《[傳頌之物 虛偽的假面](../Page/傳頌之物_虛偽的假面.md "wikilink")》片尾曲

    3.
13. ，2016年1月27日發售

    1.    - 電視動畫《[傳頌之物 虛偽的假面](../Page/傳頌之物_虛偽的假面.md "wikilink")》後期片頭曲

    2.    - 電視動畫《[傳頌之物 虛偽的假面](../Page/傳頌之物_虛偽的假面.md "wikilink")》後期片尾曲

    3.
14. ，2016年9月21日發售

    1.    - 遊戲《[傳頌之物 二人的白皇](../Page/傳頌之物_二人的白皇.md "wikilink")》主題曲

### 專輯

1.  ，2006年1月25日發售

    1.
    2.
    3.
    4.
    5.
    6.
    7.
2.  ，2006年9月27日發售

    1.
    2.
    3.
    4.
    5.
    6.
    7.
    8.
    9.
    10.
    11.
    12. (「[傳頌之物](../Page/傳頌之物.md "wikilink")」最終ED)

3.  ，2008年8月27日發售

    1.
    2.
    3.
    4.
    5.
    6.
    7.
    8.
    9.
    10.
    11.
    12.
    13.
    14.
4.  ，2009年8月19日發售

    1.
    2.
    3.
    4.
    5.
    6.
    7.
    8.
    9.
    10.
    11.
    12.
5.  ，2011年10月26日發售

    1.
    2.
    3.
    4.
    5.
    6.
    7.
    8.
    9.
    10.
    11.
    12.
    13.
6.  ，2015年10月14日發售

      -
        **Disc 1**

    <!-- end list -->

    1.  　(「[傳頌之物 虛偽的假面](../Page/傳頌之物_虛偽的假面.md "wikilink")」OP)

    2.
    3.
    4.
    5.
    6.  　(「[傳頌之物 虛偽的假面](../Page/傳頌之物_虛偽的假面.md "wikilink")」)

    7.
    8.
    9.
    10.
    11.
    12.
    13.
    <!-- end list -->

      -
        **Disc 2**

    <!-- end list -->

    1.
    2.
    3.
    4.
    5.
### 精选集

  - ，2012年9月26日发售
    ;Disc1

<!-- end list -->

1.
2.
3.
4.
5.
6.
7.
8.
9.
10. I am

11. BLUE

12.
13.
14. haunting melody

15. memory

<!-- end list -->

  - ;Disc2

<!-- end list -->

1.
2.  Free and Dream

3.  adamant faith

4.
5.
6.
7.  MOON PHASE

8.
9.
10. POWDER SNOW

11.
12.
13. Future World

## 註解

## 外部連結

  - [Suara官方網站](http://www.fixrecords.com/suara/)
  - [Anime News
    Network上的*Suara*介紹](http://www.animenewsnetwork.com/encyclopedia/people.php?id=53325)

[Category:日本女歌手](../Category/日本女歌手.md "wikilink")
[Category:大阪府出身人物](../Category/大阪府出身人物.md "wikilink")
[Category:日本女性流行音樂歌手](../Category/日本女性流行音樂歌手.md "wikilink")
[Category:動畫歌手](../Category/動畫歌手.md "wikilink")
[Category:動畫音樂作詞家](../Category/動畫音樂作詞家.md "wikilink")
[Category:動畫音樂作曲家](../Category/動畫音樂作曲家.md "wikilink")
[Category:電子遊戲音樂家](../Category/電子遊戲音樂家.md "wikilink")
[Category:Lantis旗下歌手](../Category/Lantis旗下歌手.md "wikilink")
[Category:台灣裔日本人](../Category/台灣裔日本人.md "wikilink")
[Category:大阪外國語大學校友](../Category/大阪外國語大學校友.md "wikilink")

1.
2.
3.