**Suica**（），中文譯為**企鵝卡**、**西瓜卡**\[1\]，是[日本一種可再充值](../Page/日本.md "wikilink")、非接觸式的[智能卡](../Page/智能卡.md "wikilink")（IC卡），兼有[儲值車票及](../Page/儲值車票.md "wikilink")[電子錢包功能](../Page/電子錢包.md "wikilink")，最初由[JR东日本发起](../Page/JR东日本.md "wikilink")，现在由JR东日本、[東京單軌電車以及](../Page/東京單軌電車.md "wikilink")[東京臨海高速鐵道三家铁道公司共同發行](../Page/東京臨海高速鐵道.md "wikilink")。Suica採用了[索尼的](../Page/索尼.md "wikilink")[FeliCa技術](../Page/FeliCa.md "wikilink")，至2011年3月底已發行了約3534萬張，并与[PASMO](../Page/PASMO.md "wikilink")、[Kitaca](../Page/Kitaca.md "wikilink")、[TOICA](../Page/TOICA.md "wikilink")、[ICOCA](../Page/ICOCA.md "wikilink")、[SUGOCA](../Page/SUGOCA.md "wikilink")、[manaca](../Page/manaca.md "wikilink")、[PiTaPa](../Page/PiTaPa.md "wikilink")、[nimoca以及](../Page/nimoca.md "wikilink")[快捷卡在乘車功能上](../Page/快捷卡.md "wikilink")。

「Suica」是JR東日本的註冊商標。

## 簡介

[Suica_wiki2.jpg](https://zh.wikipedia.org/wiki/File:Suica_wiki2.jpg "fig:Suica_wiki2.jpg")
[Iccard.gif](https://zh.wikipedia.org/wiki/File:Iccard.gif "fig:Iccard.gif")
Suica于2001年11月18日開始运营。目前，系统的覆盖范围包括[JR东日本铁道线路中的](../Page/JR东日本.md "wikilink")[首都圈铁道区](../Page/首都圈.md "wikilink")、[仙台铁道区和](../Page/仙台.md "wikilink")[新潟](../Page/新潟.md "wikilink")、[埼玉新都市交通](../Page/埼玉新都市交通.md "wikilink")、[仙台機場鐵道和](../Page/仙台機場鐵道.md "wikilink")[伊豆急行铁道公司的全部车站和](../Page/伊豆急行.md "wikilink")[JR巴士关东的部分路线](../Page/JR巴士关东.md "wikilink")。同时，Suica也能與[PASMO](../Page/PASMO.md "wikilink")、[Kitaca](../Page/Kitaca.md "wikilink")、[TOICA](../Page/TOICA.md "wikilink")、[ICOCA](../Page/ICOCA.md "wikilink")、[SUGOCA](../Page/SUGOCA.md "wikilink")、[manaca](../Page/manaca.md "wikilink")、[PiTaPa](../Page/PiTaPa.md "wikilink")、[nimoca以及](../Page/nimoca.md "wikilink")[快捷卡在乘車功能上互相通用](../Page/快捷卡.md "wikilink")。

Suica是JR東日本公司開發，用以取代原本在[自動售票機發行的](../Page/自動售票機.md "wikilink")[儲值磁卡](../Page/儲值卡.md "wikilink")（2005年3月31日停止發售）的一款非接觸式智能卡乘車票證。Suica與過去的儲值磁卡一樣，可以在大部分的售票機購買，也可利用[自動補票機](../Page/自動補票機.md "wikilink")（自動精算機）補票、加值，具定期車票的功能；亦能夠在車站商店街中的部份[商店及](../Page/商店.md "wikilink")[自動販賣機購買](../Page/自動販賣機.md "wikilink")[商品](../Page/商品.md "wikilink")。

Suica是非接觸式的智能卡，因此不需要從皮夾中取出，只要揮動或碰觸就感應使用，感應的範圍在沒有障礙物的情況下為半徑10公分。但是，為了加快Suica通過检票口的速度，因此建議使用「觸碰即走」（、）的方式，也就是將放有Suica的車票夾或皮夾直接觸碰剪票口的感應區。

Suica的名稱源於***S**uper **U**rban **I**ntelligent
**CA**rd*（超級都會智能卡）的簡稱，在日語中有「順暢通行（スイスイ行）的智慧卡」的語意，也和西瓜（）的發音相同，讓人感到親切。Suica的[標誌](../Page/標誌.md "wikilink")（Logo）以JR東日本的代表色－綠色為主，將鐵道路線以西瓜表現。Suica字樣的部份在「ic」兩字使用反白的顏色，表示為使用IC卡的智慧型票證。[吉祥物是一隻](../Page/吉祥物.md "wikilink")[企鵝](../Page/企鵝.md "wikilink")，由[插畫家](../Page/插畫家.md "wikilink")[坂崎千春設計](../Page/坂崎千春.md "wikilink")。

FeliCa是使用13.56MHz左右周波數的非接觸式無線電[RFID技術](../Page/RFID.md "wikilink")，在感應範圍內同時可以有複數的FeliCa裝置互相通信。與日本[Edy](../Page/Edy.md "wikilink")[電子貨幣](../Page/電子貨幣.md "wikilink")，[香港](../Page/香港.md "wikilink")[八達通及](../Page/八達通.md "wikilink")[新加坡](../Page/新加坡.md "wikilink")[ezlink卡採用同一技術](../Page/易通卡.md "wikilink")。

### 基本使用方式

購買Suica時必須支付500[日圓的](../Page/日圓.md "wikilink")[保證金](../Page/押金.md "wikilink")，如Suica
IO
Card（Suicaイオカード）售價2,000日圓中能實際使用的金額為1,500日圓。保證金無法折抵車資。不使用Suica卡可辦理退費，退費時會自卡片可用餘額扣除220日圓（2014年4月1日之前是210日圓\[2\]）手續費後，將所餘金額連同保證金退還，惟因遺失或有車資計算不符情形者保證金不予退回。如果可用餘額不足220日圓，僅會退還保證金。Suica卡可以在有「Suica」標誌的「自動售票機（自動券売機）」「卡片發售機（カード発売機）」「自動補票機（のりこし精算機）」現金儲值。每次可儲值金額的單位是1,000日圓、2,000日圓、3,000日圓、4,000日圓、5,000日圓、10,000日圓等6種，卡片可儲值的上限是20,000日圓。

[KuzuokaEkiKaisatsuguchi2005-5.jpg](https://zh.wikipedia.org/wiki/File:KuzuokaEkiKaisatsuguchi2005-5.jpg "fig:KuzuokaEkiKaisatsuguchi2005-5.jpg")[葛岡站](../Page/葛岡站.md "wikilink")）\]\]

## 與私鐵、地下鐵、公共汽車整合

2007年3月18日起，Suica可通行於東京都會區的私鐵、地下鐵及[公共汽車](../Page/公共汽車.md "wikilink")。[參考說明](https://web.archive.org/web/20070406172145/http://www.jreast.co.jp/suica-co/index.html)（日文）
並針對外國旅客於2007年3月28日發行Suica+N'EX的套票。[參考說明](https://web.archive.org/web/20070328093330/http://www.jreast.co.jp/tc/suica-nex/index.html)

### 可以使用的區間

  - 鐵道

<!-- end list -->

  - [東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")（JR東日本）
      - 在來線
          - （首）[東京近郊區間](../Page/大都市近郊區間_\(JR\)#東京近郊區間.md "wikilink")\[3\]
              -
                ※以東京站為中心超出韮崎站、澀川站、水戶站的路段（水上方向、磐城方向除外）只顯示「一部分對應站」（[長坂站](../Page/長坂站.md "wikilink")、[小淵澤站](../Page/小淵澤站.md "wikilink")、[富士見站](../Page/富士見站.md "wikilink")、[茅野站](../Page/茅野站.md "wikilink")、[上諏訪站](../Page/上諏訪站.md "wikilink")、[下諏訪站](../Page/下諏訪站.md "wikilink")、[岡谷站](../Page/岡谷站.md "wikilink")、[鹽尻站](../Page/鹽尻站.md "wikilink")、[松本站](../Page/松本站.md "wikilink")、[清里站](../Page/清里站.md "wikilink")、[野邊山站](../Page/野邊山站.md "wikilink")、、、、[常陸太田站](../Page/常陸太田站.md "wikilink")、、、）
                ※[烏山線](../Page/烏山線.md "wikilink")、[鹿島線](../Page/鹿島線.md "wikilink")、[久留里線除外](../Page/久留里線.md "wikilink")
          - （仙）[仙台近郊區間](../Page/大都市近郊區間_\(JR\)#仙台近郊區間.md "wikilink")\[4\]
              -
                ※磐越西線（喜久田站以遠）、奧羽本線（起點站除外）、仙山線（陸前白澤站以遠）、陸羽東線（塚目站以遠）、東北本線（田尻站以遠）、石巻線（起點站與石卷站除外）只顯示「一部分對應站」（[一之關站](../Page/一之關站.md "wikilink")、[平泉站](../Page/平泉車站_\(日本\).md "wikilink")、[磐梯熱海站](../Page/磐梯熱海站.md "wikilink")、、[會津若松站](../Page/會津若松站.md "wikilink")、、[山形站](../Page/山形站.md "wikilink")、、、[鳴子溫泉站](../Page/鳴子溫泉站.md "wikilink")）
                ※、[石卷線](../Page/石卷線.md "wikilink")（[石卷站](../Page/石卷站.md "wikilink")－[女川站](../Page/女川站.md "wikilink")）除外
          - （新）[新潟近郊區間](../Page/大都市近郊區間_\(JR\)#新潟近郊區間.md "wikilink")\[5\]
              -
                ※以新潟站為中心超出長岡站、吉田站、新發田站的路段（彌彥線除外）只顯示「一部分對應站」（、[柏崎站](../Page/柏崎站.md "wikilink")、[直江津站](../Page/直江津站.md "wikilink")、、、、）
        <!-- end list -->
          -
            ※特急列車等可購入別途特急券等。但首都圈地域的[東武線直通特急](../Page/日光號列車.md "wikilink")，不可用於車費和特急料金。
      - 新幹線（下述只限Suica定期券與）
          - （首）[東京站](../Page/東京站.md "wikilink")－[那須鹽原站](../Page/那須鹽原站.md "wikilink")、[上毛高原站](../Page/上毛高原站.md "wikilink")、[上越妙高站](../Page/上越妙高站.md "wikilink")\[6\]
          - （仙）郡山站－[古川站](../Page/古川站.md "wikilink")\[7\]
          - （新）[長岡站](../Page/長岡站.md "wikilink")－[新潟站](../Page/新潟站.md "wikilink")\[8\]
        <!-- end list -->
          -
            ※Suica
            FREX定期券、FREXパル定期券只限用於訂定的券面區間內。Suica定期券只限可用於券面區間內的2個或以上新幹線停車站。
            ※只限乘座普通車自由席。普通車指定席船[Gran
            Class](../Page/Gran_Class.md "wikilink")、Green車不可乘座。
            ※Mobile Suica特急券可用於JR東日本新幹線全線的所有車站互相（包括在來線直通區間）使用。
            ※上記以外的Suica及其他IC卡情況，購入別途特急券亦不可用驗票閘門進行補票。
  - （首）[東京單軌電車全線](../Page/東京單軌電車.md "wikilink")\[9\]
  - （首）[東京臨海高速鐵道全線](../Page/東京臨海高速鐵道.md "wikilink")\[10\]
  - （首）[埼玉新都市交通全線](../Page/埼玉新都市交通.md "wikilink")\[11\]
  - （首）[伊豆急行全線](../Page/伊豆急行.md "wikilink")\[12\]
  - （首）[富士急行全線](../Page/富士急行.md "wikilink")\[13\]
  - （仙）[仙台機場鐵道全線](../Page/仙台機場鐵道.md "wikilink")\[14\]

<!-- end list -->

  - 巴士

<!-- end list -->

  - （首）[JR巴士關東](../Page/JR巴士關東.md "wikilink")
      - 一般路線\[15\]
          -
          -
          -
          -
          -
          -
        <!-- end list -->
          -
            ※不可用於並行、共同運行的其他公司
      - 高速路線\[16\]\[17\]
          - 新宿、東京－佐野、鹿沼線（）

          - 新宿－本-{庄}-、伊勢崎線

          -
          - 東京－鹿島線（）

          - 東京－神栖、波崎線（）

          - 東京－筑波線（）

          - 東京站－東京晴空塔城線（）
        <!-- end list -->
          -
            ※也可用於共同運行的其他公司（一部分臨時便除外）

只能在從上車到下車為止，都在上述的同一區域内移動的情況下才能使用
。但如果兩個事業者的區間有互相銜接，就可以跨區間使用，東京臨海高速鐵道、PASMO區域與JR東日本的首都圈區域，仙台機場鐵道與JR東日本的仙台區域，各自都能跨區間使用。另外，在從區域外前往區域内時，可以在下車站的有人窗口向站務員申告自己的上車站，這樣就可以運用卡片內剩餘的金額來進行精算。

### 鐵道、公共汽車間的彼此通用

[ICCard_Connection.svg](https://zh.wikipedia.org/wiki/File:ICCard_Connection.svg "fig:ICCard_Connection.svg")

Suica可與以下IC卡的可用區間內彼此通用。但依照區域不同，可能會有一些使用上的限制。

  - **[ICOCA](../Page/ICOCA.md "wikilink")**（[西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本））
      - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）[大阪近郊區間内](../Page/大都市近郊區間#大阪近郊區間.md "wikilink")（除了一部分區間之外）所使用的IC卡**[ICOCA](../Page/ICOCA.md "wikilink")**，在2004年8月1日開始，彼此間可以互相通用。2007年9月1日開始，岡山廣島區域也可以使用。
  - **[PASMO](../Page/PASMO.md "wikilink")**（首都圈的民間鐵路、公共汽車業者）
      - 關東的[私鐵](../Page/私鐵.md "wikilink")、[地下鐵](../Page/地下鐵.md "wikilink")22社局共通的[公共汽車網路與](../Page/公共汽車網路.md "wikilink")1都3縣的[公共汽車各社所發行的](../Page/公共汽車.md "wikilink")[公共汽車共通卡合而為一的非接觸式IC卡車票](../Page/公共汽車共通卡.md "wikilink")**[PASMO](../Page/PASMO.md "wikilink")**，從2007年3月18日一開始提供服務時，就可以和Suica彼此間互相通用。詳細的使用法請參照****。
  - **[TOICA](../Page/TOICA.md "wikilink")**（[東海旅客鐵道](../Page/東海旅客鐵道.md "wikilink")（JR東海））
      - 2008年3月29日開始彼此間可以互相通用。
  - **[Kitaca](../Page/Kitaca.md "wikilink")**（[北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")（JR北海道））
      - 2009年3月14日開始彼此間可以互相通用。
  - **[SUGOCA](../Page/SUGOCA.md "wikilink")**（[九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")（JR九州））、**[nimoca](../Page/nimoca.md "wikilink")**（[西日本鐵道](../Page/西日本鐵道.md "wikilink")（西鐵））、**[はやかけん](../Page/はやかけん.md "wikilink")**（[福岡市交通局](../Page/福岡市交通局.md "wikilink")）
      - 2010年3月13日開始彼此間可以互相通用\[18\]。
  - **[manaca](../Page/manaca.md "wikilink")**（[名古屋鐵道](../Page/名古屋鐵道.md "wikilink")（名鐵）、[名古屋市交通局等](../Page/名古屋市交通局.md "wikilink")）
      - 2013年3月23日開始彼此間可以互相通用。

[PASMO_Automatic_Ticket_Gate.jpg](https://zh.wikipedia.org/wiki/File:PASMO_Automatic_Ticket_Gate.jpg "fig:PASMO_Automatic_Ticket_Gate.jpg")-Suica専用自動[驗票閘門](../Page/驗票閘門.md "wikilink")（[小田急江之島線](../Page/小田急江之島線.md "wikilink")[藤澤站](../Page/藤澤站.md "wikilink")）\]\]

## 非交通用途的應用

[Keyless_locker_001.JPG](https://zh.wikipedia.org/wiki/File:Keyless_locker_001.JPG "fig:Keyless_locker_001.JPG")適用的[收費儲物櫃](../Page/儲物櫃.md "wikilink")\]\]
[Suica_drink_vendor1.jpg](https://zh.wikipedia.org/wiki/File:Suica_drink_vendor1.jpg "fig:Suica_drink_vendor1.jpg")

  - 便利商店
  - 餐廳
  - 部分車站之收費[儲物櫃](../Page/置物櫃.md "wikilink")
  - 車內購物
  - 自動販賣機\[19\]

## 歷史沿革

  - 2002年4月21日及12月1日分別推出了單軌Suica卡 (Monorail Suica)及臨海Suica卡 (Rinkai
    Suica)，用於[東京單軌電車及](../Page/東京單軌電車.md "wikilink")[東京臨海高速鐵道](../Page/東京臨海高速鐵道.md "wikilink")。
  - 2003年11月，JR西日本在[關西地方推出了類似的智能卡系統](../Page/關西地方.md "wikilink")[ICOCA](../Page/ICOCA.md "wikilink")，和Suica於部分地區互通。
  - 2007年3月18日起，Suica可通行於東京都會區的私鐵及地下鐵。
  - 2008年3月29日，Suica和JR東海的TOICA可以互通。
  - 2010年3月13日，Suica和JR九州的SUGOCA、西日本鐵道的nimoca與福岡市交通局的はやかけん可以互通，同日通行於伊豆急行全線。\[20\]
  - 2014年3月14日，與[臺灣](../Page/臺灣.md "wikilink")[一卡通簽署合作備忘錄](../Page/一卡通_\(台灣\).md "wikilink")，期許兩種票卡能於臺灣及日本互通使用。\[21\]
  - 2016年9月7日，[苹果公司宣布在日本发售的](../Page/苹果公司.md "wikilink")[iPhone
    7](../Page/iPhone_7.md "wikilink")、7 Plus及[Apple Watch Series
    2将支持FeliCa技术并可于](../Page/Apple_Watch_Series_2.md "wikilink")[Apple
    Pay添加Suica卡](../Page/Apple_Pay.md "wikilink")。\[22\]
  - 2017年9月12日，[苹果公司发布](../Page/苹果公司.md "wikilink")[iPhone
    8](../Page/iPhone_8.md "wikilink")、8 Plus、[iPhone
    X及](../Page/iPhone_X.md "wikilink")[Apple Watch Series
    3](../Page/Apple_Watch_Series_3.md "wikilink")，全球所有地区发售版本均支持FeliCa。\[23\]
  - 2018年5月29日，[Google宣布](../Page/Google.md "wikilink")[Google
    Pay開始支援](../Page/Google_Pay.md "wikilink")[Suica卡片綁定](../Page/Suica.md "wikilink")，但僅限[日本地區使用](../Page/日本.md "wikilink")。\[24\]

## 參考

## 外部链接

  - [Suica：JR東日本](https://www.jreast.co.jp/suica/)
  - [Suica | 简体中文](https://www.jreast.co.jp/sc/pass/suica.html)
  - [Suica | 繁体中文](https://www.jreast.co.jp/tc/pass/suica.html)
  - [東京單軌電車](http://www.tokyo-monorail.co.jp/suica/)
  - [Suica 東京臨海高速鐵道](http://www.twr.co.jp/fare/suica.html)
  - [SFCard
    Viewer](http://www.sony.co.jp/Products/felica/consumer/download/sfcardviewer2.html)
    Sony的智慧卡記錄檢視軟體
  - [參考說明](https://web.archive.org/web/20060716204057/http://www.jreast.co.jp/suica/use/charge/index.html#haraimodoshi)

[Category:東日本旅客鐵道](../Category/東日本旅客鐵道.md "wikilink")
[Category:東京單軌電車](../Category/東京單軌電車.md "wikilink")
[Category:東京臨海高速鐵道](../Category/東京臨海高速鐵道.md "wikilink")
[Category:日本智能卡](../Category/日本智能卡.md "wikilink")
[Category:電子貨幣](../Category/電子貨幣.md "wikilink")

1.  [关于单轨电车西瓜卡(Suica)](http://www.tokyo-monorail.co.jp/sc/tickets/suica/index.html)

2.

3.  [Suica首都圏エリア](http://www.jreast.co.jp/suica/area/tokyo/index.html) -
    JR東日本

4.  [Suica仙台エリア](http://www.jreast.co.jp/suica/area/sendai/index.html) -
    JR東日本

5.  [Suica新潟エリア](http://www.jreast.co.jp/suica/area/niigata/index.html)
    - JR東日本

6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17. [高速バス時刻表](http://www.jrbuskanto.co.jp/timebook/)  - JRバス関東

18. [詳細](http://www.jreast.co.jp/press/2008/20080409.pdf)

19. [詳細](http://www.jreast.co.jp/tc/pass/suica.html)

20. [詳細](http://www.izukyu.co.jp/ir/newsletter/210114.pdf)

21. [台日電子票證
    簽署合作備忘錄](http://www.epochtimes.com/b5/14/3/14/n4106480.htm%E5%8F%B0%E6%97%A5%E9%9B%BB%E5%AD%90%E7%A5%A8%E8%AD%89%E6%A5%AD%E5%90%88%E4%BD%9C-%E5%85%B1%E5%89%B5%E6%96%B0%E5%A5%91%E6%A9%9F.html?photo=2)

22.

23.

24.