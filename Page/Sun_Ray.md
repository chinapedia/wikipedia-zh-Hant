[Sunray.jpg](https://zh.wikipedia.org/wiki/File:Sunray.jpg "fig:Sunray.jpg")
**Sun Ray**是[昇陽電腦公司](../Page/昇陽電腦.md "wikilink")（Sun
Microsystems）於1999年所發表的[瘦客户端](../Page/瘦客户端.md "wikilink")（Thin
Client），是針對企業資訊環境而推出。

Sun Ray的組成包括一個[智慧卡](../Page/智慧卡.md "wikilink")（Smart
Card）讀卡機，且多數機款與[平面顯示器](../Page/平面顯示器.md "wikilink")（Flat
Panel Display，FPD）合為一體。

## 型款

| 型號          | 簡述                                                                            | 附註                    |
| ----------- | ----------------------------------------------------------------------------- | --------------------- |
| Sun Ray 1   | 不具顯示功能                                                                        | 供援停止（End-Of-Life，EOL） |
| Sun Ray 100 | 與17英吋CRT顯示器一體化設計                                                              | 供援停止（End-Of-Life，EOL） |
| Sun Ray 150 | 與15英吋LCD顯示器一體化設計                                                              | 供援停止（End-Of-Life，EOL） |
| Sun Ray 1g  | 不具顯示功能、強化[解析度](../Page/解析度.md "wikilink")、增加[USB埠](../Page/USB.md "wikilink") | \--                   |
| Sun Ray 170 | 與17英吋LCD顯示器一體化設計                                                              | \--                   |
| Sun Ray 2   | 不具顯示功能                                                                        | \--                   |
| Sun Ray 2FS | 不具顯示功能，雙接頭輸出                                                                  | \--                   |

## 附註

1.  \- Thin
    Client也稱為瘦身型、苗條型的用戶端電腦（**瘦身型用戶端**），相對的[個人電腦](../Page/個人電腦.md "wikilink")（Personal
    Computer，PC）被支持與主張Thin Client的業者譏諷為Fat
    Client（**肥胖型用戶端**），然PC陣營的業者則辯稱PC為Rich
    Client（**豐富型用戶端**），此外比較持中立立場者則稱PC為Thick Client（**厚實型用戶端**）。

## 外部連結

  - [昇陽電腦官方網站](http://www.sun.com/)

  - [昇陽電腦精簡型電腦及伺服器型集中式運算團體的部落格（網誌）](https://web.archive.org/web/20060630202554/http://blogs.sun.com/ThinkThin)

  - [Sun
    Ray用戶團體](https://web.archive.org/web/20060518194225/http://sun-rays.org/)

  - [Sun
    Ray用戶的電子報訂閱](https://web.archive.org/web/20060816050012/http://filibeto.org/sun/sunray-users/mailing-list.html)

[Category:昇陽電腦](../Category/昇陽電腦.md "wikilink")