**Sunday
Girls**是由[佐藤麻衣](../Page/佐藤麻衣.md "wikilink")、[五島千佳](../Page/五島千佳.md "wikilink")、[千田愛紗](../Page/千田愛紗.md "wikilink")、[高垣朋子四名成員所組成的女子偶像團體](../Page/高垣朋子.md "wikilink")。

## 成員列表

<table>
<tbody>
<tr class="odd">
<td><p><strong>本名</strong></p></td>
<td><p><strong>藝名</strong></p></td>
<td><p><strong>出生日期</strong></p></td>
<td><p><strong>國籍</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/佐藤麻衣.md" title="wikilink">佐藤麻衣</a></p></td>
<td><p>麻衣</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/千田愛紗.md" title="wikilink">千田愛紗</a></p></td>
<td><p>愛紗</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/五島千佳.md" title="wikilink">五島千佳</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/高垣朋子.md" title="wikilink">高垣朋子</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 專輯

<table>
<colgroup>
<col style="width: 15%" />
<col style="width: 15%" />
<col style="width: 15%" />
<col style="width: 55%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>專輯</p></th>
<th><p>唱片</p></th>
<th><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2001年</p></td>
<td><p>《喜歡你》</p></td>
<td><p><a href="../Page/豐華唱片.md" title="wikilink">豐華唱片</a></p></td>
<td><ol>
<li>喜歡你</li>
<li>努力向上吧</li>
<li>Fresh</li>
<li>Flower</li>
<li>First Love</li>
<li>快樂在一起</li>
<li>丸子三兄弟</li>
<li>好想嫁給他</li>
<li>好久不見</li>
<li>Love Machine</li>
</ol></td>
</tr>
</tbody>
</table>

## 徵選

Sunday
Girls是2001年[華視](../Page/華視.md "wikilink")[綜藝節目](../Page/綜藝節目.md "wikilink")《[超級星期天](../Page/超級星期天.md "wikilink")》中，由[豐華唱片](../Page/豐華唱片.md "wikilink")、[東風文化傳播](../Page/東風文化傳播.md "wikilink")（當時《超級星期天》製作單位）、[吉本興業聯合舉辦的](../Page/吉本興業.md "wikilink")「超級動員令之日本美少女」活動徵選而出的。

四名Sunday
Girls的成員是由2000多封從[日本各地寄來的報名表中](../Page/日本.md "wikilink")，選出130名合格的參賽者，最後經現場選拔而出六名成員，分別為[遠藤舞](../Page/遠藤舞.md "wikilink")、[安藤裕子](../Page/安藤裕子.md "wikilink")、[佐藤麻衣](../Page/佐藤麻衣.md "wikilink")、[高垣朋子](../Page/高垣朋子.md "wikilink")、[五島千佳和](../Page/五島千佳.md "wikilink")[佐藤梢](../Page/佐藤梢.md "wikilink")（[千田愛紗為候補](../Page/千田愛紗.md "wikilink")）。她們都是至台灣訓練三個月，經過磨練比賽而選出的。

## 現況

目前[佐藤麻衣與](../Page/佐藤麻衣.md "wikilink")[千田愛紗仍以藝人身份在](../Page/千田愛紗.md "wikilink")[台灣發展](../Page/台灣.md "wikilink")。[安藤裕子則在日本以創作型女歌手的身份出道發行專輯](../Page/安藤裕子.md "wikilink")，並且曾在電影《[催眠](../Page/催眠.md "wikilink")》與電視劇《[池袋西口公園](../Page/池袋西口公園_\(小說\).md "wikilink")》中參與演出。

[Category:日本女子演唱團體](../Category/日本女子演唱團體.md "wikilink")
[Category:臺灣女子演唱團體](../Category/臺灣女子演唱團體.md "wikilink")