**Super
G**是[Atheros公司的專利](../Page/Atheros.md "wikilink")[幀](../Page/幀.md "wikilink")[突發](../Page/突發技術.md "wikilink")、[壓縮與](../Page/資料壓縮.md "wikilink")[通道捆合](../Page/通道捆合技術.md "wikilink")[技術](../Page/技術.md "wikilink")，用以加強[IEEE
802.11g](../Page/IEEE_802.md "wikilink")[無線區域網路的效能](../Page/無線區域網路.md "wikilink")。使用Super
G時的[吞吐](../Page/吞吐量.md "wikilink")[傳輸速度限制在透過結合兩條](../Page/比特率.md "wikilink")54Mbit/s的802.11g頻道後實現的108Mbit/s[訊號速率時聲稱可達到](../Page/訊號速率.md "wikilink")40[Mbit/s至](../Page/Mbit/s.md "wikilink")60Mbit/s。

其他銷售商曾以名稱108G Technology、108Mbit/s 802.11g與Xtreme G來銷售Super
G產品，取得[Atheros授權的生產商包括](../Page/Atheros.md "wikilink")[奇勝](../Page/奇勝.md "wikilink")、[友訊科技](../Page/友訊科技.md "wikilink")、[Netgear](../Page/Netgear.md "wikilink")、[北電網絡](../Page/北電網絡.md "wikilink")、[久森](../Page/久森.md "wikilink")、[SMC
Networks](../Page/SMC_Networks.md "wikilink")、[索尼](../Page/索尼.md "wikilink")、[速連通訊與](../Page/速連通訊.md "wikilink")[東芝](../Page/東芝.md "wikilink")。一般來說所有其他銷售商的Super
G產品均可在Super G模式下[互通](../Page/可互通性.md "wikilink")。

像Super
G等非標準的802.11g通道捆合擴展被批評會對所有[Wi-Fi頻道做成](../Page/Wi-Fi.md "wikilink")[干擾](../Page/干擾.md "wikilink")，潛在性的引發其他同一[波段](../Page/波段_\(無線電\).md "wikilink")[無線裝置的問題](../Page/無線.md "wikilink")，如鄰近的[無線網絡](../Page/無線網絡.md "wikilink")、[無繩電話](../Page/無繩電話.md "wikilink")、[嬰兒監察器與](../Page/嬰兒監察器.md "wikilink")[藍芽等裝置](../Page/藍芽.md "wikilink")。不過Atheros聲稱由於現實環境中的物理分隔與牆壁近端網絡將不會受到來自Super
G網絡的干擾。

Super
G是數個開發用來加強802.11g裝置效能的競爭性專屬方式之一，其他包括了[Broadcom的](../Page/Broadcom.md "wikilink")[125
High Speed Mode](../Page/125_High_Speed_Mode.md "wikilink")、[Airgo
Networks基於](../Page/Airgo_Networks.md "wikilink")[MIMO的擴展與](../Page/MIMO.md "wikilink")[科勝訊公司的](../Page/科勝訊公司.md "wikilink")[Nitro等](../Page/Nitro.md "wikilink")。

Atheros亦有在他們的802.11a/g晶片中採用這個技術，並以Super AG的名稱銷售。

## 外部連結

  - [Super
    G](https://web.archive.org/web/20050112104743/http://www.super-g.com/)網站

[Category:IEEE 802](../Category/IEEE_802.md "wikilink")
[Category:Wi-Fi](../Category/Wi-Fi.md "wikilink")
[Category:無線網絡](../Category/無線網絡.md "wikilink")