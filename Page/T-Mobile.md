**T-Mobile**是一家跨国[電信公司](../Page/電信公司.md "wikilink")，是[德国电信的子公司](../Page/德国电信.md "wikilink")，属于[Freemove联盟](../Page/Freemove.md "wikilink")。T-Mobile在[西欧和](../Page/西欧.md "wikilink")[美国营运](../Page/美国.md "wikilink")[GSM网络并通过金融手段参与](../Page/GSM.md "wikilink")[东欧和](../Page/东欧.md "wikilink")[东南亚的网络营运](../Page/东南亚.md "wikilink")。该公司拥有1.09亿用户，是世界上[最大的移动电话公司之一](../Page/最大的移动电话公司.md "wikilink")。

## 市场状况

[德国是T](../Page/德国.md "wikilink")-Mobile的老家，作为次大的移动电话营运商它拥有2670万用户（2004年3月），紧跟竞争对手[沃达丰](../Page/沃达丰.md "wikilink")。为了把德国高利润的GSM网络升级到[UMTS第三代移动通讯标准](../Page/UMTS.md "wikilink")，满足手机上网的需求，2000年8月德国电信取得了德国使用3G的授权，T-Mobile花了160亿德国马克（76亿美金）。

T-Mobile USA前身是VoiceStream（已收购无线营运商PowerTel,
Aerial和Omnipoint）。VoiceStream在2001年5月被德国电信以240亿美元收购。总部设在华盛顿Bellevue。T-Mobile
USA是美国市场上第三大营运商，也是继Verizon
Wireless之后的超过160万用户的的第二大成长最快的公司，平均每季度增长1百万用户。它也是唯一一家在欧洲和美国使用统一品牌的移动电话公司。通过国际漫游协定连接到德国电信并与其他GSM网络兼容，T－Mobile
USA比任何一家美国无线营运商提供更多的全球范围内更多的覆盖面积。2004年，T-Mobile
USA被授予了J.D.Power和Associates的几个奖项，包括“2004无线用户服务最高评价奖”和“2004无线零售服务海外用户最满意奖”。

在美国T-Mobile为[Wi-Fi无线Internet访问在包括机场](../Page/Wi-Fi.md "wikilink")，机场俱乐部，[星巴克咖啡屋](../Page/星巴克.md "wikilink")，[Kinkos](../Page/Kinkos.md "wikilink")，[Borders
Books and
Music在内的很多地方部署了超过](../Page/Borders_Books_and_Music.md "wikilink")4500个热区。Wi-Fi基础设施是T-Mobile通过收购[WISP](../Page/WISP.md "wikilink")
[MobileStar获得的](../Page/MobileStar.md "wikilink")。

T-Mobile的全球代言人是女演员[凯瑟琳·泽塔-琼斯](../Page/凯瑟琳·泽塔-琼斯.md "wikilink")。（被T-Mobile收购之前），北美的VoiceStream
Wireless公司的代言人是[Jamie Lee
Curtis](../Page/Jamie_Lee_Curtis.md "wikilink")。在泽塔琼斯作为全球代言人的期间，T-Mobile美国2004年末开始启用[说唱音乐歌手](../Page/说唱音乐.md "wikilink")[Snoop
Dogg作为公司产品](../Page/Snoop_Dogg.md "wikilink")[T-Mobile
Sidekick广告系列片的代言人](../Page/危险Hiptop.md "wikilink")。

T-Mobile还是好几个以他的名字冠名体育运动队的赞助商，
例如它拥有自己的自行车运动队－[T-Mobile队](../Page/T-Mobile队.md "wikilink")，或者给奥地利足球[联赛冠名](../Page/联赛.md "wikilink")。
2009年9月9日德意志電信將旗下T-Mobile英國業務，與[法國電信旗下Orange英國業務整合](../Page/法國電信.md "wikilink")，組建各佔50%股權的合營公司Everything
Everywhere。合併後，手機用戶達到2,840萬，占英國逾37%的手機市場，成為英國最大手機營運商。

2011年3月20日，德意志電信計劃將旗下T-Mobile美國業務，以390億美元的現金與股票的價格出售予[AT\&T](../Page/AT&T.md "wikilink")。\[1\]不过此收购案后来被[美国司法部否决](../Page/美国司法部.md "wikilink")。因为监管机构担心影响美国通信市场竞争为由失败，AT\&T为此支付30亿美元违约金。

2018年4月30日，T-Mobile US
再次公布預計以0.10256股[T-Mobile股票兌換一股Sprint股票](../Page/T-Mobile.md "wikilink")，每股價值6.62美元，斥資265億美元併購同業[Sprint](../Page/Sprint.md "wikilink")，合併後的新公司將沿用T-Mobile，[德國電信將持有新公司](../Page/德國電信.md "wikilink")42%股權，擁有[Sprint的](../Page/Sprint.md "wikilink")[Softbank將持有](../Page/Softbank.md "wikilink")27%股權，本交易尚未完成，也尚未獲得[聯邦通信委員會批准](../Page/聯邦通信委員會.md "wikilink")
。

## T-Mobile商标

在以下国家使用"T-Mobile"商标运营GSM网络 总计用户数2004年七月（2004年3月）

  - [奥地利](../Page/奥地利.md "wikilink")200万（200万）
  - [捷克共和国](../Page/捷克共和国.md "wikilink")410万（400万）
  - [德国](../Page/德国.md "wikilink")2710万（2670万）
  - [克罗地亚](../Page/克罗地亚.md "wikilink")170万（160万）
  - [匈牙利](../Page/匈牙利.md "wikilink")400万（380万）
  - [荷兰](../Page/荷兰.md "wikilink")220万（210万）
  - [英国](../Page/英国.md "wikilink")1490万（1430万）
  - [美国](../Page/美国.md "wikilink")1540万（1430万）

## 商业合作

在以下国家T-Mobile通过金融股份形式参与移动电话服务：

  - [白俄罗斯](../Page/白俄罗斯.md "wikilink")
  - [波斯尼亚黑塞格维纳](../Page/波斯尼亚和黑塞哥维那.md "wikilink")
  - [加拿大](../Page/加拿大.md "wikilink")
  - [印度尼西亚](../Page/印度尼西亚.md "wikilink")
  - [马来西亚](../Page/马来西亚.md "wikilink")
  - [菲律宾](../Page/菲律宾.md "wikilink")
  - [波兰](../Page/波兰.md "wikilink")
  - [马其顿共和国](../Page/马其顿共和国.md "wikilink")
  - [俄罗斯](../Page/俄罗斯.md "wikilink")
  - [斯洛伐克](../Page/斯洛伐克.md "wikilink")
  - [乌克兰](../Page/乌克兰.md "wikilink")

## 参见

  - [2009年Sidekick數據丟失事件](../Page/2009年Sidekick數據丟失事件.md "wikilink")

## 参考资料

## 外部链接

各个国家的T-Mobile站点：

  - [T-Mobile国际](https://web.archive.org/web/20060425005415/http://www.t-mobile.net/)
  - [T-Mobile奥地利](http://www.t-mobile.at/)
  - [T-Mobile捷克共和国](http://www.t-mobile.cz/)
  - [T-Mobile德国](http://www.t-mobile.de/)
  - [T-Mobile荷兰](http://www.t-mobile.nl/)
  - [T-Mobile波兰](http://www.era.pl/)
  - [T-Mobile俄罗斯](http://www.mtsgsm.com/)
  - [T-Mobile英国](http://www.t-mobile.co.uk/)
  - [T-Mobile美国](http://www.t-mobile.com/)
  - [T-Mobile匈牙利](http://www.t-mobile.hu/)
  - [T-Mobile克罗地亚](http://webarchive.loc.gov/all/20090213081936/http%3A//www.t%2Dmobile.hr/)

[Category:德國流動電話運營商](../Category/德國流動電話運營商.md "wikilink")
[Category:德国电信](../Category/德国电信.md "wikilink")
[Category:德國品牌](../Category/德國品牌.md "wikilink")
[Category:英国通信公司](../Category/英国通信公司.md "wikilink")
[Category:奥地利通信公司](../Category/奥地利通信公司.md "wikilink")
[Category:1990年成立的公司](../Category/1990年成立的公司.md "wikilink")

1.