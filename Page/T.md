**T**, **t**
是[拉丁字母中的第](../Page/拉丁字母.md "wikilink")20个[字母](../Page/字母.md "wikilink")。

是西方[闪族语字母表和](../Page/闪族语.md "wikilink")[希伯来字母的最后一个字母](../Page/希伯来字母.md "wikilink")。闪族语的Taw的音值、[希腊字母](../Page/希腊字母.md "wikilink")（Tau）以及[伊特鲁里亚字母和](../Page/伊特鲁里亚字母.md "wikilink")[拉丁字母的T都曾发作](../Page/拉丁字母.md "wikilink")/t/。

[北约音标字母以](../Page/北约音标字母.md "wikilink")*Tango*来表示字母T。

## 字母T的含意

## 字符编码

| 字符编码                               | [ASCII](../Page/ASCII.md "wikilink") | [Unicode](../Page/Unicode.md "wikilink") | [EBCDIC](../Page/EBCDIC.md "wikilink") | [摩斯电码](../Page/摩斯电码.md "wikilink") |
| ---------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- | ---------------------------------- |
| [大写](../Page/大写字母.md "wikilink") T | 84                                   | 0054                                     | 227                                    | `-`                                |
| [小写](../Page/小写字母.md "wikilink") t | 116                                  | 0074                                     | 163                                    |                                    |

## 在其他字母系統中的相似字母

  - ⲧ̅: [古努比亞語](../Page/古努比亞語.md "wikilink")(Old Nubian
    language)裡所使用的[科普特字母](../Page/科普特字母.md "wikilink")。

## 其他表示方法

## 参看

  - （[希腊字母](../Page/希腊字母.md "wikilink") Tau）

  - （[西里尔字母](../Page/西里尔字母.md "wikilink") Te）

## 外部連結

  -
  -
  -
[Category:拉丁字母](../Category/拉丁字母.md "wikilink")