[Tap.a330-200.cs-toe.arp.jpg](https://zh.wikipedia.org/wiki/File:Tap.a330-200.cs-toe.arp.jpg "fig:Tap.a330-200.cs-toe.arp.jpg")

**TAP葡萄牙航空**（；全稱「」，简称「」，）是[葡萄牙的](../Page/葡萄牙.md "wikilink")[國家航空公司](../Page/國家航空公司.md "wikilink")。该公司為[星空聯盟的成員之一](../Page/星空聯盟.md "wikilink")，以-{[里斯本](../Page/里斯本.md "wikilink")}-為該公司基地，葡萄牙航空的主要業務是經營[歐洲](../Page/歐洲.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、[美洲的業務](../Page/美洲.md "wikilink")。

## 歷史

TAP
Portugal成立于1945年3月14日，在1946年9月19日開展由里斯本至[馬德里的商業航線](../Page/馬德里.md "wikilink")。同年12月31日擴展至12個地區，包括[安哥拉](../Page/安哥拉.md "wikilink")、[莫桑比克等](../Page/莫桑比克.md "wikilink")。

1947年開展里斯本到[波爾圖的內陸航線](../Page/波爾圖.md "wikilink")；2年後開展[倫敦至](../Page/倫敦.md "wikilink")[聖多美普林西比](../Page/聖多美普林西比.md "wikilink")。

1964年迎來了開業以來的第100萬乘客。

1969年開拓以[紐約](../Page/紐約.md "wikilink")、[波士頓為主的](../Page/波士頓.md "wikilink")[美洲的業務](../Page/美洲.md "wikilink")。

1996年開始至[澳門及](../Page/澳門.md "wikilink")[曼谷的航線](../Page/曼谷.md "wikilink")，但因載客量少，因此在翌年取消相關的業務；在2006年底，由於澳門的經濟強勁，因此有意重開至澳門的航線（由於澳門機場的使用率方面已幾近飽和，至今仍未落實，可能會轉移至香港）。

2005年加入[星空联盟](../Page/星空联盟.md "wikilink")。

TAP葡萄牙航空曾持有下列航空公司的股份：

  - [聖多美和普林西比航空](../Page/聖多美和普林西比航空.md "wikilink")
    （曾持有其40%股份，该公司于2006年在其唯一一架且是由TAP葡萄牙航空提供的飞机坠毁后终止了运营）
  - [澳門航空](../Page/澳門航空.md "wikilink")（15%）。

## 機隊

[tap.a320-200.cs-tnj.arp.jpg](https://zh.wikipedia.org/wiki/File:tap.a320-200.cs-tnj.arp.jpg "fig:tap.a320-200.cs-tnj.arp.jpg")
截至2018年9月，TAP葡萄牙航空擁有以下飛機\[1\] \[2\]：

<center>

| 機型                                                   | 數量                         | 已訂購 | 載客量 | 備註  |
| ---------------------------------------------------- | -------------------------- | --- | --- | --- |
| <abbr title="行政艙">C</abbr>                           | <abbr title="經濟艙">Y</abbr> | 總數  |     |     |
| [空中巴士A319-111](../Page/空中客车A320.md "wikilink")       | 21                         | －   | 60  | 84  |
| [空中巴士A320-214](../Page/空中巴士A320.md "wikilink")       | 21                         | －   | 42  | 132 |
| [空中巴士A320neo](../Page/空中巴士A320neo系列.md "wikilink")   | 1                          | 18  | 42  | 132 |
| [空中巴士空中巴士A321-211](../Page/空中巴士A320系列.md "wikilink") | 4                          | －   | 12  | 204 |
| [空中巴士A321neo](../Page/空中巴士A320neo系列.md "wikilink")   | 3                          | 16  | 12  | 204 |
| [空中巴士A321neoLR](../Page/空中巴士A320neo系列.md "wikilink") | －                          | 14  | 16  | 152 |
| [空中巴士A330-202／-223](../Page/空中客车A330.md "wikilink")  | 12                         | －   | 25  | 244 |
| [空中巴士A330-300](../Page/空中客车A330.md "wikilink")       | 4                          | －   | 30  | 255 |
| [空中巴士A330-900neo](../Page/空中客车A330neo.md "wikilink") | 2                          | 19  | 34  | 264 |
| [空中巴士A340-300](../Page/空中客车A340.md "wikilink")       | 4                          | －   | 36  | 232 |
| 總數                                                   | 71                         | 68  |     |     |

**現役機隊**

</center>

### 已退役機隊

  - [卡拉維爾客機](../Page/卡拉維爾客機.md "wikilink")(服役期:1962-1969)
  - [空中巴士A310-300](../Page/空中客车A310.md "wikilink") (服役期:1988-2008)
  - [波音707-320B](../Page/波音707.md "wikilink")(服役期:1965-1990)
  - [波音727-100](../Page/波音727-100.md "wikilink")(服役期:1967-1989)
  - [波音727-200](../Page/波音727-200.md "wikilink")(服役期:1975-1991)
  - [波音737-200](../Page/波音737-200.md "wikilink")(服役期:1983-1999)
  - [波音737-300](../Page/波音737-300.md "wikilink")(服役期:1988-2001)
  - [波音747-200](../Page/波音747-200.md "wikilink")(服役期:1972-1984)
  - [L-1011](../Page/L-1011.md "wikilink")(服役期:1983-1997)
  - [L-1049](../Page/L-1049.md "wikilink")(服役期:1953-1967)
  - [德哈維蘭加拿大 DHC-6
    雙水獺](../Page/德哈維蘭加拿大_DHC-6_雙水獺.md "wikilink")(服役期:1979-未知)
  - [道格拉斯DC-3](../Page/道格拉斯DC-3.md "wikilink")(服役期:1949-1959)
  - [道格拉斯DC-4](../Page/道格拉斯DC-4.md "wikilink")(服役期:1947-1960)

## 参考文献

<div class="references-small">

<references />

</div>

## 相關資料

  - [1](https://web.archive.org/web/20070221003849/http://www.flytap.com/USA/en/Company/Fleet/)

[Category:葡萄牙交通](../Category/葡萄牙交通.md "wikilink")
[Category:葡萄牙公司](../Category/葡萄牙公司.md "wikilink")
[Category:葡萄牙航空公司](../Category/葡萄牙航空公司.md "wikilink")
[Category:1945年成立的航空公司](../Category/1945年成立的航空公司.md "wikilink")

1.  [Air Portugal Fleet Details and
    History](http://www.airfleets.net/flottecie/TAP%20Air%20Portugal.htm)
2.  [Fleet TAP Air
    Portugal](http://www.airfleets.net/flottecie/TAP%20Air%20Portugal.htm)