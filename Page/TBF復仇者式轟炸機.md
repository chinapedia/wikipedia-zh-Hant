**TBF復仇者式**（；如果是[通用汽車所代工版本則稱為](../Page/通用汽車.md "wikilink")**TBM
Avenger**）為[格魯門公司開發的](../Page/格魯門公司.md "wikilink")[-{zh:艦上;zh-tw:海基;zh-hk:艦上;zh-hans:舰载}-](../Page/艦載機.md "wikilink")[魚雷轟炸機](../Page/魚雷轟炸機.md "wikilink")，主要於[第二次世界大戰的太平洋戰場上運用](../Page/第二次世界大戰.md "wikilink")。

## 開發

在1941年底美國正式對[軸心國參戰時](../Page/軸心國.md "wikilink")，[美國海軍的魚雷攻擊機仍是以](../Page/美國海軍.md "wikilink")[道格拉斯飛行器公司](../Page/道格拉斯飛行器公司.md "wikilink")1935年研製的[TBD毀滅者式為主](../Page/TBD毀滅者式.md "wikilink")，其性能落後自然不在話下。不過早在1939年海軍便向各航空公司徵求新一代艦上轟炸機，在1940年4月海軍向格魯曼訂購2架它們所設計的TBF，當時稱為XTBF-1；原型機於1941年8月7日首飛，雖然其中1架原型機在測試時發生意外在紐約的布倫特伍德墜毀，但測試及量產仍緊鑼密鼓的執行。復仇者式正式向外界公開是1941年12月7日下午，地點則是在格魯曼未來將量產該機型的工廠；無巧不巧的當天美國受到日軍攻擊，因此剛展示的新機隨即受嚴密保護，工廠也提高戒備以免遭到破壞。隨後TBF宣稱冠上復仇者的名諱做為對日作戰的象徵。不過實際上此名稱早在1941年10月便決定，公開的時間點與名字純屬巧合。

比起TBD，TBF的性能有著明顯大幅的提升，也是美國軍隊在二戰中最重的單引擎飛機。

TBF配有輸出功率達1700匹馬力的活塞發動機提供充沛動力，在量產型上更換了F6F原型機運用的R-2600-20發動機，動力輸出在二戰魚雷攻擊機中堪稱頂尖；新設計的[流線型座艙配備防彈玻璃](../Page/流線型.md "wikilink")，機身的防彈裝甲也前所未有的堅固。[機翼設計延續同廠F](../Page/機翼.md "wikilink")4F戰鬥機以來經典的折疊式主翼，讓她在[航空母艦操作彈性上優於其他國家的攻擊機](../Page/航空母艦.md "wikilink")；TBF的載彈量比起[日本的](../Page/大日本帝國.md "wikilink")[九七艦攻還更強悍許多](../Page/九七式艦上攻擊機.md "wikilink")，受惠於內置彈艙降低空氣阻力，TBF在兼顧了酬載量同時並沒有犧牲太多的飛行性能；除了搭載一枚[Mark
13航空魚雷之外](../Page/Mk13型鱼雷.md "wikilink")，內置彈艙可酬載一枚2000磅或四枚500磅[炸彈](../Page/炸彈.md "wikilink")，而[襟翼配備減速板設計加上](../Page/襟翼.md "wikilink")[煞車](../Page/煞車.md "wikilink")[減速板](../Page/減速板.md "wikilink")，更讓TBF可以擁有和[俯衝轟炸機一樣的俯衝攻擊能力](../Page/俯衝轟炸機.md "wikilink")，在戰場上成為[日本軍艦的頭號殺手](../Page/大日本帝國.md "wikilink")。

除了一開始設計的對艦／對地攻擊任務，美國海軍與麻省理工學院工程師合作，利用TBF機體容積酬載量充足之優勢將她裝上使用[真空管零件操作的對海搜索雷達](../Page/真空管.md "wikilink")，用以進行反潛與預警任務。

1942年3月開始，因格魯曼必須同時生產[F4F野貓式與](../Page/F4F戰鬥機.md "wikilink")[F6F地獄貓式](../Page/F6F戰鬥機.md "wikilink")[戰鬥機](../Page/戰鬥機.md "wikilink")，為了減低生產壓力，便授權[通用製造TBF](../Page/通用汽車公司.md "wikilink")，而通用生產的TBF則被稱為TBM。
[George_H.W._Bush_seated_in_a_Grumman_TBM_Avenger,_circa_1944_(H069-13).jpg](https://zh.wikipedia.org/wiki/File:George_H.W._Bush_seated_in_a_Grumman_TBM_Avenger,_circa_1944_\(H069-13\).jpg "fig:George_H.W._Bush_seated_in_a_Grumman_TBM_Avenger,_circa_1944_(H069-13).jpg")於TBF復仇者機艙內的留影，攝於1944年航空母艦[聖哈辛托號上](../Page/聖哈辛托號航空母艦.md "wikilink")\]\]
[TBF_(Avengers)_flying_in_formation.jpg](https://zh.wikipedia.org/wiki/File:TBF_\(Avengers\)_flying_in_formation.jpg "fig:TBF_(Avengers)_flying_in_formation.jpg")上空的飛行編隊，攝於1942年\]\]

## 戰歷

TBF在對日宣戰後便緊鑼密鼓的量產，在1942年6月完成首批100架TBF-1型交付海軍，但運往[珍珠港時卻正好與](../Page/珍珠港.md "wikilink")3艘航艦錯身而過，因此大部分TBF沒在[中途島海戰出戰](../Page/中途島海戰.md "wikilink")。只有6架編入大黃蜂號航空母艦的第8魚雷攻擊機中隊（VT-8），由於人員素質及與TBD混用的因素，這6架TBF首次參戰便遭受毀滅性損失-其中5架遭到日軍擊墜，剩下1架則是砲手1人戰死，剩下兩人重傷的情況下歸艦；不過魚雷中隊的損失讓剩下的俯衝轟炸機取得攻擊日軍航艦的最佳機會。由於TBD在此戰役的低落表現加速美軍汰換進度，在中途島戰役過後，海軍便全面將所有的航艦換裝上TBF。

於同年8月的[第二次所羅門海戰時](../Page/東所羅門海戰.md "wikilink")，兩艘美國航艦：薩拉托加號與企業號上共配備24架的TBF，該戰役以損失7架TBF的代價擊沉了日本的[龍驤號航空母艦](../Page/龍驤號航空母艦.md "wikilink")，緊接著11月份的[瓜達爾卡納爾海戰也重創](../Page/瓜達爾卡納爾海戰.md "wikilink")[比叡號戰艦](../Page/比叡號戰艦.md "wikilink")。

1943年，新型的TBF-3服役，換裝威力更強的新型[Mark
15航空魚雷](../Page/Mark15型魚雷.md "wikilink")，也加大了炸彈掛載量與出力更強的發動機，同時也搭載了對水面及反潛[雷達](../Page/雷達.md "wikilink")，開始支援[大西洋戰場的獵殺](../Page/大西洋.md "wikilink")[德國](../Page/納粹德國.md "wikilink")[U型潛艇任務](../Page/U-潛艇.md "wikilink")；而同時搭載對空及對海搜索[雷達的機型也成為可於夜間或雨天出擊的全天候攻擊機](../Page/雷達.md "wikilink")。1944年6月於[菲律賓海海戰擊沉了日本的航空母艦](../Page/菲律賓海海戰.md "wikilink")[飛鷹號](../Page/飛鷹號航空母艦.md "wikilink")，隨後於[雷伊泰灣海戰及隔年的](../Page/雷伊泰灣海戰.md "wikilink")[坊之岬海海戰先後擊沉了大型航空母艦](../Page/坊之岬海海戰.md "wikilink")[瑞鶴](../Page/瑞鶴號航空母艦.md "wikilink")，以及象徵帝國海軍[聯合艦隊的](../Page/聯合艦隊.md "wikilink")[大和與](../Page/大和號戰艦.md "wikilink")[武藏兩艘](../Page/武藏號戰艦.md "wikilink")[戰艦](../Page/戰艦.md "wikilink")，可謂戰功彪炳。

## 规格

[Grumman_TBF-1_Avenger_BuAer_drawing.jpg](https://zh.wikipedia.org/wiki/File:Grumman_TBF-1_Avenger_BuAer_drawing.jpg "fig:Grumman_TBF-1_Avenger_BuAer_drawing.jpg")

## 使用國家

[JMSDF_TBM-3W.jpg](https://zh.wikipedia.org/wiki/File:JMSDF_TBM-3W.jpg "fig:JMSDF_TBM-3W.jpg")的TBM-3W\]\]

  - [Flag_of_the_United_States.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_States.svg "fig:Flag_of_the_United_States.svg")美國海軍航空隊
  - [Flag_of_the_United_Kingdom.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_Kingdom.svg "fig:Flag_of_the_United_Kingdom.svg")英國皇家海軍航空隊
  - [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg")法國海軍
  - [Flag_of_Canada_1921.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Canada_1921.svg "fig:Flag_of_Canada_1921.svg")加拿大皇家海軍
  - [Flag_of_New_Zealand.svg](https://zh.wikipedia.org/wiki/File:Flag_of_New_Zealand.svg "fig:Flag_of_New_Zealand.svg")紐西蘭皇家空軍
  - [Flag_of_the_Netherlands.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Netherlands.svg "fig:Flag_of_the_Netherlands.svg")荷蘭皇家海軍
  - [Flag_of_Brazil.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Brazil.svg "fig:Flag_of_Brazil.svg")巴西海軍
  - [Flag_of_Uruguay.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Uruguay.svg "fig:Flag_of_Uruguay.svg")烏拉圭海軍
  - [Flag_of_Japan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Japan.svg "fig:Flag_of_Japan.svg")日本海上自衛隊

## 相關條目

  - [第二次世界大戰](../Page/第二次世界大戰.md "wikilink")
  - [太平洋戰爭](../Page/太平洋戰爭.md "wikilink")
  - [TBD毀滅者式](../Page/TBD毀滅者式.md "wikilink")
  - [九七式艦上攻擊機](../Page/九七式艦上攻擊機.md "wikilink")
  - [天山艦上攻擊機](../Page/天山艦上攻擊機.md "wikilink")
  - [劍魚式魚雷轟炸機](../Page/劍魚式魚雷轟炸機.md "wikilink")

## 参考文献

<div class="references-small">

<references>

</references>

</div>

[Category:美國轟炸機](../Category/美國轟炸機.md "wikilink")
[Category:艦載機](../Category/艦載機.md "wikilink")