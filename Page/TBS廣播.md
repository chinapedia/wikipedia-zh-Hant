**TBS廣播公司**（，TBS Radio,
Inc.），簡稱**TBS電台**（），是[日本一家以](../Page/日本.md "wikilink")[關東廣域圈為播出範圍的](../Page/關東廣域圈.md "wikilink")[廣播電台](../Page/廣播電台.md "wikilink")，隸屬於[東京放送控股旗下](../Page/東京放送控股.md "wikilink")，為[廣播聯播網](../Page/廣播聯播網.md "wikilink")「[JRN](../Page/JRN.md "wikilink")」的[核心台](../Page/核心局.md "wikilink")，播出頻率為[AM](../Page/調幅廣播.md "wikilink")954，另擁有一[FM](../Page/調頻廣播.md "wikilink")（頻率90.5）。1951年12月25日開播，現有之公司法人則於2000年3月21日成立。

## 沿革

  - 1951年12月25日，東京電台（）開播，為日本第6家、東日本第1家開播的[商業廣播電台](../Page/商業廣播.md "wikilink")。
  - 1960年11月29日，東京電台更名為[東京放送](../Page/東京放送.md "wikilink")（；今[東京放送控股前身](../Page/東京放送控股.md "wikilink")），簡稱「TBS」。[電台廣播部門則更名為](../Page/電台.md "wikilink")「TBS電台」（）。
  - 2000年3月21日，東京放送的電台廣播部門獨立為[子公司](../Page/子公司.md "wikilink")「TBS廣播與信息公司」（）。
  - 2001年10月1日，東京放送的電台廣播業務改由TBS廣播與信息公司繼承。
  - 2009年4月1日，東京放送更名為[東京放送控股](../Page/東京放送控股.md "wikilink")，數位電台廣播業務由TBS廣播與信息公司繼承。
  - 2016年4月1日，TBS廣播與信息公司更名为「TBS广播公司」（）。

## 外部連結

  - [TBS電台官方網站](https://www.tbsradio.jp)

  -
[Category:日本广播电台](../Category/日本广播电台.md "wikilink")
[\*](../Category/東京放送控股.md "wikilink") [Category:港區公司
(東京都)](../Category/港區公司_\(東京都\).md "wikilink")
[Category:赤坂](../Category/赤坂.md "wikilink")
[Category:1951年建立](../Category/1951年建立.md "wikilink")