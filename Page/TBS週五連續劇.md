**週五連續劇**（），簡稱「Kindora」（），是[日本](../Page/日本.md "wikilink")[TBS電視台从](../Page/TBS電視台.md "wikilink")1972年起每逢星期五22:00起播放的[電視連續劇時段](../Page/电视剧.md "wikilink")。推出作品多以年輕題材為主，並以較受矚目或受歡迎的年輕[演員來擔綱演出](../Page/演員.md "wikilink")，是TBS重要的電視劇節目時段。

隨著電視劇的低迷化，加上抵不過新聞報導強化的潮流，1987年TBS電視台一度讓新聞節目進駐原週五連續劇之時段，一直到1989年才以《[比雨更溫柔](../Page/比雨更溫柔.md "wikilink")》恢復週五連續劇的播出。

## 概要

## 作品列表

### 1980年代

#### 1987年

  - 1月-3月：「[親子萬歲](../Page/親子萬歲.md "wikilink")」　主演：[大原麗子](../Page/大原麗子.md "wikilink")
  - 4月-6月：「[男人們請多指教](../Page/男人們請多指教.md "wikilink")」　主演：[田村正和](../Page/田村正和.md "wikilink")
  - 7月-9月：「[蒙娜麗莎們的冒險](../Page/蒙娜麗莎們的冒險.md "wikilink")」　主演：[大竹忍](../Page/大竹忍.md "wikilink")

#### 1989年

  - 10月-12月：「[比雨更溫柔](../Page/比雨更溫柔.md "wikilink")」　主演：[淺野優子](../Page/淺野優子.md "wikilink")

### 1990年代

#### 1990年

  - 1月-3月：「[危險男女](../Page/危險男女.md "wikilink")」　主演：[今井美樹](../Page/今井美樹.md "wikilink")
  - 4月-6月：「[誘惑](../Page/誘惑.md "wikilink")」　主演：
  - 7月-9月：「[都會之森](../Page/都會之森.md "wikilink")」　主演：[高嶋政伸](../Page/高嶋政伸.md "wikilink")
  - 10月-12月：「」　主演：

#### 1991年

  - 1月-3月：「[長不齊的蘋果們
    III](../Page/長不齊的蘋果們_III.md "wikilink")」　主演：[中井貴一](../Page/中井貴一.md "wikilink")
  - 4月-6月：「」　主演：[三上博史](../Page/三上博史.md "wikilink")
  - 7月-9月：「[男大不中留](../Page/男大不中留.md "wikilink")」　主演：
  - 10月-12月：「[愛有明天](../Page/愛有明天.md "wikilink")」　主演：[今井美樹](../Page/今井美樹.md "wikilink")

#### 1992年

  - 1月-3月：「[成人的選擇](../Page/成人的選擇.md "wikilink")」　主演：
  - 4月-6月：「[愛又怎麼樣](../Page/愛又怎麼樣.md "wikilink")」　主演：[緒形拳](../Page/緒形拳.md "wikilink")
  - 7月-9月：「[一直都愛你](../Page/一直都愛你.md "wikilink")」　主演：
  - 10月-12月：「[十年愛](../Page/十年愛.md "wikilink")」　主演：[田中美佐子](../Page/田中美佐子.md "wikilink")、[浜田雅功](../Page/浜田雅功.md "wikilink")

#### 1993年

  - 1月-3月：「[高校教師](../Page/高校教師.md "wikilink")」　主演：[真田廣之](../Page/真田廣之.md "wikilink")、
  - 4月-6月：「[个体户恋曲](../Page/个体户恋曲.md "wikilink")」　主演：[松田聖子](../Page/松田聖子.md "wikilink")
  - 7月-9月：「」　主演：
  - 10月-12月：「[全心的愛](../Page/全心的愛.md "wikilink")」　主演：、

#### 1994年

  - 1月-3月：「[心中有陽光](../Page/心中有陽光.md "wikilink")」　主演：[西田敏行](../Page/西田敏行.md "wikilink")
  - 4月-6月：「[適齢期](../Page/適齢期.md "wikilink")」　主演：[三上博史](../Page/三上博史.md "wikilink")
  - 7月-9月：「[人間失格](../Page/人間失格.md "wikilink")」　主演：
  - 10月-12月：「[援助愛情](../Page/援助愛情.md "wikilink")」　主演：[真田廣之](../Page/真田廣之.md "wikilink")、[小泉今日子](../Page/小泉今日子.md "wikilink")

#### 1995年

  - 1月-3月：「[揺れる想い](../Page/搖擺的想念.md "wikilink")」　主演：
  - 4月-6月：「[六月新娘](../Page/六月新娘.md "wikilink")」　主演：[財前直見](../Page/財前直見.md "wikilink")
  - 7月-9月：「[跟我說愛我](../Page/跟我說愛我.md "wikilink")」　主演：[豊川悦司](../Page/豐川悅司.md "wikilink")、[常盤貴子](../Page/常盤貴子.md "wikilink")
  - 10月-12月：「[未成年](../Page/未成年.md "wikilink")」　主演：

#### 1996年

  - 1月-3月：「[無悔的愛](../Page/無悔的愛.md "wikilink")」　主演：[緒形直人](../Page/緒形直人.md "wikilink")
  - 4月-6月：「[與你相遇](../Page/與你相遇.md "wikilink")」　主演：[本木雅弘](../Page/本木雅弘.md "wikilink")
  - 7月-9月：「[玻璃碎片](../Page/玻璃碎片.md "wikilink")」　主演：、[松雪泰子](../Page/松雪泰子.md "wikilink")
  - 10月-12月：「[協奏曲](../Page/協奏曲.md "wikilink")」　主演：[田村正和](../Page/田村正和.md "wikilink")、[木村拓哉](../Page/木村拓哉.md "wikilink")、[宮澤理惠](../Page/宮澤理惠.md "wikilink")

#### 1997年

  - 1月-3月：「[人生戀曲](../Page/人生戀曲.md "wikilink")」　主演：[高嶋政伸](../Page/高嶋政伸.md "wikilink")
  - 4月-6月：「[長不齊的蘋果們
    IV](../Page/長不齊的蘋果們_IV.md "wikilink")」　主演：[中井貴一](../Page/中井貴一.md "wikilink")
  - 7月-9月：「[最後之戀](../Page/最後之戀.md "wikilink")」　主演：[中居正広](../Page/中居正広.md "wikilink")、[常盤貴子](../Page/常盤貴子.md "wikilink")
  - 10月-12月：「[青鳥](../Page/青鳥.md "wikilink")」　主演：[豊川悦司](../Page/豐川悅司.md "wikilink")

#### 1998年

  - 1月-3月：「[聖者行進](../Page/聖者行進.md "wikilink")」　主演：
  - 4月-6月：「[邂逅](../Page/邂逅.md "wikilink")」　主演：[常盤貴子](../Page/常盤貴子.md "wikilink")、[福山雅治](../Page/福山雅治.md "wikilink")
  - 7月-9月：「[夏日幽會](../Page/夏日幽會.md "wikilink")」　主演：[田中美佐子](../Page/田中美佐子.md "wikilink")、[桃井薰](../Page/桃井薰.md "wikilink")
  - 10月-12月：「[繼母庸人妙管家](../Page/繼母庸人妙管家.md "wikilink")」　主演：

#### 1999年

  - 1月-3月：「[繼續](../Page/繼續.md "wikilink")」　主演：[中谷美紀](../Page/中谷美紀.md "wikilink")、[渡部篤郎](../Page/渡部篤郎.md "wikilink")
  - 4月-6月：「[週末婚](../Page/週末婚.md "wikilink")」　主演：[永作博美](../Page/永作博美.md "wikilink")
  - 7月-9月：「[獨身生活](../Page/獨身生活.md "wikilink")」　主演：[江角真紀子](../Page/江角真紀子.md "wikilink")
  - 10月-12月：「[美人](../Page/美人.md "wikilink")」　主演：[田村正和](../Page/田村正和.md "wikilink")、[常盤貴子](../Page/常盤貴子.md "wikilink")

### 2000年代

#### 2000年

  - 01月14日-03月17日：「[星期五的戀人](../Page/星期五的戀人.md "wikilink")」　主演：[藤原纪香](../Page/藤原纪香.md "wikilink")
  - 04月14日-06月23日：「[電子情謎](../Page/電子情謎.md "wikilink")」　主演：[財前直見](../Page/財前直見.md "wikilink")
  - 07月7日-09月15日：「[Friends](../Page/Friends.md "wikilink")」　主演：[浜田雅功](../Page/浜田雅功.md "wikilink")
  - 10月13日-12月15日：「[真夏的聖誕節](../Page/真夏的聖誕節.md "wikilink")」　主演：[竹野内豊](../Page/竹野内豊.md "wikilink")、[中谷美紀](../Page/中谷美紀.md "wikilink")

#### 2001年

  - 01月12日-03月16日：「[蛋糕上的草莓](../Page/蛋糕上的草莓.md "wikilink")」　主演：[瀧澤秀明](../Page/瀧澤秀明.md "wikilink")、[深田恭子](../Page/深田恭子.md "wikilink")
  - 04月13日-06月29日：「[再見舊情人](../Page/再見舊情人.md "wikilink")」　主演：[藤原紀香](../Page/藤原紀香.md "wikilink")、[大澤隆夫](../Page/大澤隆夫.md "wikilink")
  - 07月6日-09月14日：「」　主演：[岸谷五朗](../Page/岸谷五朗.md "wikilink")
  - 10月19日-12月21日：「[好久沒戀愛](../Page/好久沒戀愛.md "wikilink")」　主演：[小泉今日子](../Page/小泉今日子.md "wikilink")、[飯島直子](../Page/飯島直子.md "wikilink")、[黑木瞳](../Page/黑木瞳.md "wikilink")

#### 2002年

  - 01月18日-03月15日：「[木更津貓眼](../Page/木更津貓眼.md "wikilink")」　主演：[岡田准一](../Page/岡田准一.md "wikilink")
  - 04月12日-06月28日：「[夢想加州](../Page/夢想加州.md "wikilink")」　主演：[堂本剛](../Page/堂本剛.md "wikilink")
  - 07月12日-09月13日：「[不需要愛情的夏天](../Page/不需要愛情的夏天.md "wikilink")」　主演：[廣末涼子](../Page/廣末涼子.md "wikilink")、[渡部篤郎](../Page/渡部篤郎.md "wikilink")
  - 10月11日-12月13日：「[媽媽的遺傳因子](../Page/媽媽的遺傳因子.md "wikilink")」　主演：[藥師丸博子](../Page/藥師丸博子.md "wikilink")

#### 2003年

  - 01月10日-03月21日：「[高校教師](../Page/高校教師.md "wikilink")」　主演：[藤木直人](../Page/藤木直人.md "wikilink")、[上戶彩](../Page/上戶彩.md "wikilink")
  - 04月11日-06月20日：「[帥哥醫生](../Page/帥哥醫生.md "wikilink")」　主演：[妻夫木聰](../Page/妻夫木聰.md "wikilink")
  - 07月4日-09月12日：「[Stand
    UP\!\!](../Page/Stand_UP!!.md "wikilink")」　主演：[二宮和也](../Page/二宮和也.md "wikilink")
  - 10月10日-12月12日：「[熱血校園](../Page/熱血校園.md "wikilink")」　主演：[竹野內豊](../Page/竹野內豊.md "wikilink")

#### 2004年

  - 01月16日-03月26日：「[魔法嬌妻](../Page/魔法嬌妻.md "wikilink")」　主演：[篠原涼子](../Page/篠原涼子.md "wikilink")
  - 04月16日-06月25日：「[HOME
    DRAMA\!](../Page/HOME_DRAMA!.md "wikilink")」　主演：[堂本剛](../Page/堂本剛.md "wikilink")
  - 07月2日-09月10日：「[在世界中心呼喚愛](../Page/在世界中心呼喚愛.md "wikilink")」　主演：[山田孝之](../Page/山田孝之.md "wikilink")、[綾瀨遙](../Page/綾瀨遙.md "wikilink")
  - 10月15日－2005年3月25日：「[3年B組金八先生](../Page/3年B組金八先生.md "wikilink")
    」　主演：[武田鐵矢](../Page/武田鐵矢.md "wikilink")

#### 2005年

  - 04月15日-06月24日：「[虎與龍](../Page/虎與龍.md "wikilink")」　主演：[長瀬智也](../Page/長瀬智也.md "wikilink")、[岡田准一](../Page/岡田准一.md "wikilink")
  - 07月8日-09月16日：「[東大特訓班](../Page/東大特訓班.md "wikilink")」　主演：[阿部寬](../Page/阿部寬.md "wikilink")
  - 10月21日-12月16日：「[流星花園](../Page/流星花園.md "wikilink")」　主演：[井上真央](../Page/井上真央.md "wikilink")

#### 2006年

  - 01月13日-03月24日：「[夜王](../Page/夜王.md "wikilink")」　主演：[松岡昌宏](../Page/松岡昌宏.md "wikilink")
  - 04月14日-06月23日：「[詐欺獵人](../Page/詐欺獵人.md "wikilink")」　主演：[山下智久](../Page/山下智久.md "wikilink")
  - 07月14日-09月15日：「[太陽之歌](../Page/太陽之歌.md "wikilink")」　主演：[山田孝之](../Page/山田孝之.md "wikilink")、[澤尻英龍華](../Page/澤尻英龍華.md "wikilink")
  - 10月13日-11月24日：「[水手服與機關槍](../Page/水手服與機關槍.md "wikilink")」　主演：[長澤雅美](../Page/長澤雅美.md "wikilink")
  - 12月1日-12月15日：「[不想談可笑的戀愛](../Page/不想談可笑的戀愛.md "wikilink")」　主演：[山崎静代](../Page/山崎静代.md "wikilink")、[河本準一](../Page/河本準一.md "wikilink")

#### 2007年

  - 1月5日－3月16日：「[流星花園2](../Page/流星花園.md "wikilink")」　主演：[井上真央](../Page/井上真央.md "wikilink")
  - 4月13日－6月22日：「」　主演：[田中聖](../Page/田中聖.md "wikilink")
  - 7月6日－9月14日：「[貧窮貴公子](../Page/貧窮貴公子.md "wikilink")」　主演：[二宮和也](../Page/二宮和也.md "wikilink")
  - 10月12日－12月21日：「[歌姫](../Page/歌姫_\(戲劇\).md "wikilink")」
  - 主演：[長瀨智也](../Page/長瀨智也.md "wikilink")
  - 劇本：[宅間孝行](../Page/宅間孝行.md "wikilink")

#### 2008年

  - 1月11日－3月14日：[愛迪生之母](../Page/愛迪生之母.md "wikilink")

<!-- end list -->

  -
    主演：[伊東美咲](../Page/伊東美咲.md "wikilink")
    劇本：[大森美香](../Page/大森美香.md "wikilink")

<!-- end list -->

  - 4月11日－6月20日：[Around40](../Page/Around40.md "wikilink")

<!-- end list -->

  -
    主演：[天海祐希](../Page/天海祐希.md "wikilink")
    劇本：[橋部敦子](../Page/橋部敦子.md "wikilink")

<!-- end list -->

  - 7月4日－9月12日：[魔王](../Page/魔王_\(日本電視劇\).md "wikilink")

<!-- end list -->

  -
    主演：[大野智](../Page/大野智.md "wikilink")、[生田斗真](../Page/生田斗真.md "wikilink")
    劇本：[前川洋一](../Page/前川洋一.md "wikilink")、[西田征史](../Page/西田征史.md "wikilink")

<!-- end list -->

  - 10月17日－12月19日：[流星之絆](../Page/流星之絆.md "wikilink")

<!-- end list -->

  -
    主演：[二宮和也](../Page/二宮和也.md "wikilink")
    原作：[東野圭吾](../Page/東野圭吾.md "wikilink")《流星之絆》
    劇本：[宮藤官九郎](../Page/宮藤官九郎.md "wikilink")

#### 2009年

  - 1月16日-3月20日：**[Love
    Shuffle](../Page/Love_Shuffle.md "wikilink")**（ラブシャッフル）

<!-- end list -->

  -
    主演：[玉木宏](../Page/玉木宏.md "wikilink")
    劇本：[野島伸司](../Page/野島伸司.md "wikilink")

<!-- end list -->

  - 4月17日-6月26日：**[Smile](../Page/Smile_\(日本電視劇\).md "wikilink")**（スマイル）

<!-- end list -->

  -
    主演：[松本潤](../Page/松本潤.md "wikilink")
    劇本：、[篠崎繪里子](../Page/篠崎繪里子.md "wikilink")

<!-- end list -->

  - 7月24日-9月25日：****（オルトロスの犬）

<!-- end list -->

  -
    主演：[瀧澤秀明](../Page/瀧澤秀明.md "wikilink")
    劇本：[青木万央等](../Page/青木万央.md "wikilink")

<!-- end list -->

  - 10月16日-12月18日：**[單身情歌](../Page/單身情歌.md "wikilink")**（おひとりさま）

<!-- end list -->

  -
    主演：[觀月亞里莎](../Page/觀月亞里莎.md "wikilink")
    劇本：[尾崎將也](../Page/尾崎將也.md "wikilink")

### 2010年代

#### 2010年

  - 1月15日－3月19日：**[完美小姐進化論](../Page/完美小姐進化論#電視連續劇.md "wikilink")**（）

<!-- end list -->

  -
    主演：[龜梨和也](../Page/龜梨和也.md "wikilink")
    原作：[早川智子](../Page/早川智子.md "wikilink")《完美小姐進化論》
    劇本：[篠崎繪里子](../Page/篠崎繪里子.md "wikilink")

<!-- end list -->

  - 4月23日－6月25日：**[不良仔與眼鏡妹](../Page/不良仔與眼鏡妹.md "wikilink")**（）

<!-- end list -->

  -
    主演：[成宮寬貴](../Page/成宮寬貴.md "wikilink")
    原作：[吉河美希](../Page/吉河美希.md "wikilink")《不良仔與眼鏡妹》
    劇本：[永田優子](../Page/永田優子.md "wikilink")

<!-- end list -->

  - 7月9日－9月17日：**[自戀刑警](../Page/うぬぼれ刑事.md "wikilink")**（）

<!-- end list -->

  -
    主演：[長瀬智也](../Page/長瀬智也.md "wikilink")
    劇本：[宮藤官九郎](../Page/宮藤官九郎.md "wikilink")

<!-- end list -->

  - 10月8日－12月17日：**[SPEC〜警視廳公安部公安第五課
    未詳事件特別對策係事件簿〜](../Page/SPEC〜警視廳公安部公安第五課_未詳事件特別對策係事件簿〜.md "wikilink")**（）

<!-- end list -->

  -
    主演：[戶田惠梨香](../Page/戶田惠梨香.md "wikilink")、[加瀬亮](../Page/加瀬亮.md "wikilink")
    劇本：[西荻弓繪](../Page/西荻弓繪.md "wikilink")

#### 2011年

  - 1月7日－3月25日：**[LADY](../Page/LADY.md "wikilink")**（）

<!-- end list -->

  -
    主演：[北川景子](../Page/北川景子.md "wikilink")
    劇本：[荒井修子](../Page/荒井修子.md "wikilink")、[渡邊雄介](../Page/渡邊雄介.md "wikilink")

<!-- end list -->

  - 4月22日－6月24日：**[出生](../Page/出生_\(電視劇\).md "wikilink")**（）

<!-- end list -->

  -
    主演：[堀北真希](../Page/堀北真希.md "wikilink")
    劇本：[鈴木收](../Page/鈴木收.md "wikilink")

<!-- end list -->

  - 7月15日－9月23日：**[原來是美男](../Page/原來是美男_\(日本電視劇\).md "wikilink")**（）

<!-- end list -->

  -
    主演：[瀧本美織](../Page/瀧本美織.md "wikilink")、[玉森裕太](../Page/玉森裕太.md "wikilink")、[藤藤谷太輔](../Page/藤藤谷太輔.md "wikilink")、[八乙女光](../Page/八乙女光.md "wikilink")
    原作：韓劇
    [洪貞恩](../Page/洪貞恩.md "wikilink")、[洪美蘭](../Page/洪美蘭.md "wikilink")《[原來是美男](../Page/原來是美男.md "wikilink")》
    劇本：[高橋麻紀](../Page/高橋麻紀.md "wikilink")

<!-- end list -->

  - 10月21日－12月16日：**[專業主婦偵探～我是影子](../Page/我是影子#電視劇.md "wikilink")**（）

<!-- end list -->

  -
    主演：[深田恭子](../Page/深田恭子.md "wikilink")
    原作：粕谷紀子《[我是影子](../Page/我是影子.md "wikilink")》
    劇本：[中園美保](../Page/中園美保.md "wikilink")、[山岡真介](../Page/山岡真介.md "wikilink")

#### 2012年

  - 1月20日－3月23日：**[戀愛Neet～忘記了的戀愛開始方法](../Page/戀愛Neet～忘記了的戀愛開始方法.md "wikilink")**（）

<!-- end list -->

  -
    主演：[仲間由紀惠](../Page/仲間由紀惠.md "wikilink")
    劇本：[永田優子](../Page/永田優子.md "wikilink")

<!-- end list -->

  - 4月20日－6月22日：**[再一次對你求婚](../Page/再一次對你求婚.md "wikilink")**（）

<!-- end list -->

  -
    主演：[竹野内豊](../Page/竹野内豊.md "wikilink")
    劇本：[桐野世樹](../Page/桐野世樹.md "wikilink")

<!-- end list -->

  - 7月20日－9月21日：**[黑之女教師](../Page/黑之女教師.md "wikilink")**（）

<!-- end list -->

  -
    主演：[榮倉奈奈](../Page/榮倉奈奈.md "wikilink")
    劇本：[大林利江子](../Page/大林利江子.md "wikilink")、[吉澤智子](../Page/吉澤智子.md "wikilink")、[池田奈津子](../Page/池田奈津子.md "wikilink")

<!-- end list -->

  - 10月12日－12月14日：**[大奧
    ～誕生【有功·家光篇】](../Page/大奧_～誕生【有功·家光篇】.md "wikilink")**（）

<!-- end list -->

  -
    主演：[堺雅人](../Page/堺雅人.md "wikilink")
    原作：[吉永史](../Page/吉永史.md "wikilink")《大奧》
    劇本：[神山由美子](../Page/神山由美子.md "wikilink")

#### 2013年

  - 1月18日－3月22日：**[夜行觀覽車](../Page/夜行觀覽車.md "wikilink")**（夜行観覧車）

<!-- end list -->

  -
    主演：[鈴木京香](../Page/鈴木京香.md "wikilink")
    原作：[湊佳苗](../Page/湊佳苗.md "wikilink")《[夜行觀覽車](../Page/夜行觀覽車.md "wikilink")》
    劇本：[奥寺佐渡子](../Page/奥寺佐渡子.md "wikilink")、[清水友佳子](../Page/清水友佳子.md "wikilink")

<!-- end list -->

  - 4月19日－6月21日：**[TAKE
    FIVE～我們能盜取愛嗎～](../Page/TAKE_FIVE.md "wikilink")**（TAKE
    FIVE〜俺たちは愛を盗めるか〜）

<!-- end list -->

  -
    主演：[唐澤壽明](../Page/唐澤壽明.md "wikilink")
    劇本：[櫻井武晴](../Page/櫻井武晴.md "wikilink")、[藤井清美](../Page/藤井清美.md "wikilink")、[-{丑}-尾健太郎](../Page/丑尾健太郎.md "wikilink")

<!-- end list -->

  - 7月12日－9月13日：**[總會有辦法的](../Page/總會有辦法的.md "wikilink")**（なるようになるさ。）

<!-- end list -->

  -
    主演：[館廣](../Page/館廣.md "wikilink")
    劇本：[橋田壽賀子](../Page/橋田壽賀子.md "wikilink")

<!-- end list -->

  - 10月11日－12月13日：**[黑河內](../Page/黑河內.md "wikilink")**（クロコーチ）

<!-- end list -->

  -
    主演：[長瀨智也](../Page/長瀨智也.md "wikilink")
    原作：[長崎尚志](../Page/長崎尚志.md "wikilink")《黑德指導》
    劇本：[泉吉紘](../Page/泉吉紘.md "wikilink")

#### 2014年

  - 1月17日－3月21日：****（）

<!-- end list -->

  -
    主演：[觀月亞里沙](../Page/觀月亞里沙.md "wikilink")
    劇本：[林宏司](../Page/林宏司.md "wikilink")

<!-- end list -->

  - 4月11日－6月13日：**[愛麗絲之棘](../Page/愛麗絲之棘.md "wikilink")**（）

<!-- end list -->

  -
    主演：[上野樹里](../Page/上野樹里.md "wikilink")
    劇本：[高橋麻紀](../Page/高橋麻紀.md "wikilink")、[池田奈津子](../Page/池田奈津子.md "wikilink")

<!-- end list -->

  - 7月4日－9月5日：**[家族狩獵](../Page/家族狩獵.md "wikilink")**（）

<!-- end list -->

  -
    原作：[天童荒太](../Page/天童荒太.md "wikilink")《[家族狩獵](../Page/家族狩獵.md "wikilink")》
    主演：[松雪泰子](../Page/松雪泰子.md "wikilink")
    劇本：[大石靜](../Page/大石靜.md "wikilink")、[泉澤陽子](../Page/泉澤陽子.md "wikilink")

<!-- end list -->

  - 10月17日－12月：**[為了N](../Page/為了N#電視劇.md "wikilink")**（）

<!-- end list -->

  -
    原作：[湊佳苗](../Page/湊佳苗.md "wikilink")《[為了N](../Page/為了N.md "wikilink")》
    主演：[榮倉奈奈](../Page/榮倉奈奈.md "wikilink")
    劇本：[奥寺佐渡子](../Page/奥寺佐渡子.md "wikilink")

#### 2015年

  - 1月－3月：**[無間雙龍](../Page/無間雙龍.md "wikilink")**（）

<!-- end list -->

  -
    原作：《[無間雙龍](../Page/無間雙龍.md "wikilink")》
    主演：[生田斗真](../Page/生田斗真.md "wikilink")
    劇本：

<!-- end list -->

  - 4月－6月：**[獻給阿爾吉儂的花束](../Page/獻給阿爾吉儂的花束_\(2015年電視劇\).md "wikilink")**（）

<!-- end list -->

  -
    原作：[丹尼爾·凱斯](../Page/丹尼爾·凱斯.md "wikilink")《[獻給阿爾吉儂的花束](../Page/獻給阿爾吉儂的花束.md "wikilink")》（Flowers
    for Algernon，1966年）
    主演：[山下智久](../Page/山下智久.md "wikilink")
    劇本：[池田奈津子](../Page/池田奈津子.md "wikilink")

<!-- end list -->

  - 7月－9月：**[表參道高校合唱部！](../Page/表參道高校合唱部！.md "wikilink")**（）

<!-- end list -->

  -
    主演：[芳根京子](../Page/芳根京子.md "wikilink")
    劇本：[櫻井剛](../Page/櫻井剛.md "wikilink")

<!-- end list -->

  - 10月－12月：**[產科醫鴻鳥](../Page/產科醫鴻鳥.md "wikilink")**（）

<!-- end list -->

  -
    原作：[鈴之木祐](../Page/鈴之木祐.md "wikilink")《產科醫鴻鳥》
    主演：[綾野剛](../Page/綾野剛.md "wikilink")
    劇本：[山本睦](../Page/山本睦.md "wikilink")、[坪田文](../Page/坪田文.md "wikilink")

#### 2016年

  - 1月－3月：**[別讓我走](../Page/別讓我走_\(電視劇\).md "wikilink")**（）

<!-- end list -->

  -
    原作：[石黑一雄](../Page/石黑一雄.md "wikilink")《[別讓我走](../Page/別讓我走.md "wikilink")》
    主演：[綾瀨遙](../Page/綾瀨遙.md "wikilink")
    劇本：[森下佳子](../Page/森下佳子.md "wikilink")

<!-- end list -->

  - 4月－6月：**[我不是無法結婚，是不婚](../Page/我不是無法結婚，是不婚.md "wikilink")**（）

<!-- end list -->

  -
    原作：《スパルタ婚活塾》
    主演：[中谷美紀](../Page/中谷美紀.md "wikilink")
    劇本：

<!-- end list -->

  - 7月－9月：**[擁有神之舌的男人](../Page/擁有神之舌的男人.md "wikilink")**（）

<!-- end list -->

  -
    主演：[向井理](../Page/向井理.md "wikilink")
    劇本：

<!-- end list -->

  - 10月－12月：**[砂之塔〜知道太多事情的鄰居](../Page/砂之塔〜知道太多事情的鄰居.md "wikilink")**（）

<!-- end list -->

  -
    主演：[菅野美穗](../Page/菅野美穗.md "wikilink")
    劇本：

#### 2017年

  - 1月－3月：****（）

<!-- end list -->

  -
    原作：[櫻井信一](../Page/櫻井信一.md "wikilink")《下剋上考試》
    主演：[阿部貞夫](../Page/阿部貞夫.md "wikilink")
    劇本：[兩澤和幸](../Page/兩澤和幸.md "wikilink")

<!-- end list -->

  - 4月－6月：**[反轉](../Page/反轉.md "wikilink")**（）

<!-- end list -->

  -
    原作：[湊佳苗](../Page/湊佳苗.md "wikilink")《反轉》
    主演：[藤原龍也](../Page/藤原龍也.md "wikilink")
    劇本：[奧寺佐渡子](../Page/奧寺佐渡子.md "wikilink")、[清水友佳子](../Page/清水友佳子.md "wikilink")

<!-- end list -->

  - 7月－9月：**[偵探物語](../Page/偵探物語.md "wikilink")**（）

<!-- end list -->

  -
    原作：[弘兼憲史](../Page/弘兼憲史.md "wikilink")《偵探物語》
    主演：[瑛太](../Page/瑛太.md "wikilink")
    劇本：[大根仁](../Page/大根仁.md "wikilink")

<!-- end list -->

  - 10月－12月：**[產科醫鴻鳥](../Page/產科醫鴻鳥.md "wikilink")2**（）

<!-- end list -->

  -
    原作：[鈴之木祐](../Page/鈴之木祐.md "wikilink")《產科醫鴻鳥》
    主演：[綾野剛](../Page/綾野剛.md "wikilink")
    劇本：[坪田文](../Page/坪田文.md "wikilink")、[矢島弘一](../Page/矢島弘一.md "wikilink")、[吉田康弘](../Page/吉田康弘.md "wikilink")

#### 2018年

  - 1月－3月：**[UNNATURAL](../Page/UNNATURAL.md "wikilink")**（）

<!-- end list -->

  -
    主演：[石原聰美](../Page/石原聰美.md "wikilink")
    劇本：[野木亞紀子](../Page/野木亞紀子.md "wikilink")

<!-- end list -->

  - 4月－6月：**[有家可歸的戀人們](../Page/有家可歸的戀人們.md "wikilink")**（）

<!-- end list -->

  -
    原作：[山本文緒](../Page/山本文緒.md "wikilink")《有家可歸的戀人們》
    主演：[中谷美紀](../Page/中谷美紀.md "wikilink")
    劇本：[大島里美](../Page/大島里美.md "wikilink")

<!-- end list -->

  - 7月－9月：**[啦啦隊之舞](../Page/啦啦隊之舞.md "wikilink")**（）

<!-- end list -->

  -
    原作：電影《[青春後空翻](../Page/青春後空翻.md "wikilink")》
    主演：[土屋太鳳](../Page/土屋太鳳.md "wikilink")
    劇本：[後藤法子](../Page/後藤法子.md "wikilink")、[德尾浩司](../Page/德尾浩司.md "wikilink")

<!-- end list -->

  - 10月－12月：**[大戀愛～和忘記我的你](../Page/大戀愛～和忘記我的你.md "wikilink")**（）

<!-- end list -->

  -
    主演：[戶田惠梨香](../Page/戶田惠梨香.md "wikilink")
    劇本：[大石靜](../Page/大石靜.md "wikilink")

#### 2019年

  - 1月－3月：**[警察之家](../Page/警察之家.md "wikilink")**（）

<!-- end list -->

  -
    原作：[加藤實秋](../Page/加藤實秋.md "wikilink")《警察之家》
    主演：[高畑充希](../Page/高畑充希.md "wikilink")
    劇本：[黑岩勉](../Page/黑岩勉.md "wikilink")

<!-- end list -->

  - 4月－6月：**[破案神手](../Page/破案神手.md "wikilink")**（）

<!-- end list -->

  -
    原作：[朱戸青](../Page/朱戸青.md "wikilink")《破案神手》
    主演：[山下智久](../Page/山下智久.md "wikilink")
    劇本：[吉田康弘](../Page/吉田康弘.md "wikilink")

## 放送局

### 現在放送局

<table>
<thead>
<tr class="header">
<th><p>放送對象地域</p></th>
<th><p>放送局</p></th>
<th><p>系列</p></th>
<th><p>放送時間</p></th>
<th><p>備考</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/東京放送控股.md" title="wikilink">TBS電視台</a></p></td>
<td><p><a href="../Page/日本新聞網_(TBS).md" title="wikilink">TBS系列</a></p></td>
<td><p>週五22:00-22:54</p></td>
<td><p><strong>制作局</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/北海道.md" title="wikilink">北海道</a></p></td>
<td></td>
<td><p>同步播出</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/青森縣.md" title="wikilink">青森縣</a></p></td>
<td><p><a href="../Page/青森電視台.md" title="wikilink">青森電視台</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/岩手县.md" title="wikilink">岩手縣</a></p></td>
<td><p><a href="../Page/IBC岩手放送.md" title="wikilink">IBC岩手放送</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/宮城縣.md" title="wikilink">宮城縣</a></p></td>
<td><p><a href="../Page/東北放送.md" title="wikilink">東北放送</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/山形縣.md" title="wikilink">山形縣</a></p></td>
<td><p><a href="../Page/TVU山形.md" title="wikilink">TVU山形</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/福島縣.md" title="wikilink">福島縣</a></p></td>
<td><p><a href="../Page/TVU福島.md" title="wikilink">TVU福島</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/山梨縣.md" title="wikilink">山梨縣</a></p></td>
<td><p><a href="../Page/山梨電視台.md" title="wikilink">山梨電視台</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新潟縣.md" title="wikilink">新潟縣</a></p></td>
<td><p><a href="../Page/新潟放送.md" title="wikilink">新潟放送</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/長野縣.md" title="wikilink">長野縣</a></p></td>
<td><p><a href="../Page/信越放送.md" title="wikilink">信越放送</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/静岡県.md" title="wikilink">静岡県</a></p></td>
<td><p><a href="../Page/靜岡放送.md" title="wikilink">靜岡放送</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/富山縣.md" title="wikilink">富山縣</a></p></td>
<td><p><a href="../Page/鬱金香電視台.md" title="wikilink">鬱金香電視台</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/石川縣.md" title="wikilink">石川縣</a></p></td>
<td><p><a href="../Page/北陸放送.md" title="wikilink">北陸放送</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/CBC电视台_(日本).md" title="wikilink">中部日本放送</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/MBS电视台.md" title="wikilink">每日放送</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鳥取県.md" title="wikilink">鳥取県</a>・<a href="../Page/岛根县.md" title="wikilink">島根縣</a></p></td>
<td><p><a href="../Page/山陰放送.md" title="wikilink">山陰放送</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/山陽放送.md" title="wikilink">山陽放送</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/広島県.md" title="wikilink">広島県</a></p></td>
<td><p><a href="../Page/中國放送.md" title="wikilink">中國放送</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/山口縣.md" title="wikilink">山口縣</a></p></td>
<td><p><a href="../Page/山口電視台.md" title="wikilink">山口電視台</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/愛媛縣.md" title="wikilink">愛媛縣</a></p></td>
<td><p><a href="../Page/愛電視台.md" title="wikilink">愛電視台</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/高知縣.md" title="wikilink">高知縣</a></p></td>
<td><p><a href="../Page/高知電視台.md" title="wikilink">高知電視台</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/福岡縣.md" title="wikilink">福岡縣</a></p></td>
<td><p><a href="../Page/RKB每日放送.md" title="wikilink">RKB每日放送</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/長崎縣.md" title="wikilink">長崎縣</a></p></td>
<td><p><a href="../Page/長崎放送.md" title="wikilink">長崎放送</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/熊本縣.md" title="wikilink">熊本縣</a></p></td>
<td><p><a href="../Page/熊本放送.md" title="wikilink">熊本放送</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大分縣.md" title="wikilink">大分縣</a></p></td>
<td><p><a href="../Page/大分放送.md" title="wikilink">大分放送</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宮崎縣.md" title="wikilink">宮崎縣</a></p></td>
<td><p><a href="../Page/宮崎放送.md" title="wikilink">宮崎放送</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鹿儿岛县.md" title="wikilink">鹿兒島縣</a></p></td>
<td><p><a href="../Page/南日本放送.md" title="wikilink">南日本放送</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沖縄県.md" title="wikilink">沖縄県</a></p></td>
<td><p><a href="../Page/琉球放送.md" title="wikilink">琉球放送</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 海外相關放送

  - [美國](../Page/美國.md "wikilink")
      - [KIKU-TV](../Page/KIKU-TV.md "wikilink")（[夏威夷州](../Page/夏威夷州.md "wikilink")）
      - NGN（[夏威夷州](../Page/夏威夷州.md "wikilink")）
      - [TV
        Japan](../Page/TV_Japan.md "wikilink")（[美國](../Page/美國.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[波多黎各](../Page/波多黎各.md "wikilink")）
  - [台灣](../Page/臺灣.md "wikilink")
      - [緯來日本台早期以](../Page/緯來日本台.md "wikilink")「一番偶像劇」，後來將劇集各自打散，是為週一至週五時段，另外有同季播出-{《[大奧【男女‧逆轉】](../Page/大奧_～誕生【有功·家光篇】.md "wikilink")》}-於週六播出。
      - [TVBS歡樂台](../Page/TVBS歡樂台.md "wikilink")
      - [八大戲劇台開闢](../Page/八大戲劇台.md "wikilink")「TBS劇場」同季播出，是為2014年4月11日起週五時段，僅引進-{《[愛麗絲的復仇](../Page/愛麗絲的復仇.md "wikilink")》}-
        。
      - [國興衛視](../Page/國興衛視.md "wikilink")
  - [香港](../Page/香港.md "wikilink")
      - [電視廣播有限公司](../Page/電視廣播有限公司.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")、[J2](../Page/J2.md "wikilink")、[煲劇1台](../Page/煲劇1台.md "wikilink")、[煲劇2台](../Page/煲劇2台.md "wikilink")、[美亞電視劇台](../Page/美亞電視劇台.md "wikilink")。

## 參見

  - [週一懸疑劇院](../Page/週一懸疑劇院.md "wikilink")
  - [週四連續劇9](../Page/週四連續劇9.md "wikilink")
  - [TBS週六晚間八點連續劇](../Page/TBS週六晚間八點連續劇.md "wikilink")
  - [周日劇場](../Page/周日劇場.md "wikilink")

## 外部連結

  - [TBS官方網站](http://www.tbs.co.jp/)

[Category:日本電視台電視劇列表](../Category/日本電視台電視劇列表.md "wikilink")
[TBS電視劇](../Category/TBS電視劇.md "wikilink")
[\*](../Category/TBS週五連續劇.md "wikilink")
[Category:日本電視劇播放時段](../Category/日本電視劇播放時段.md "wikilink")