**TBS週四9時連續劇**是[TBS電視台每逢星期四的](../Page/TBS電視台.md "wikilink")21:00到21:54播出的[電視連續劇時段](../Page/:ja:ドラマ.md "wikilink")。[電視連續劇時段於](../Page/:ja:ドラマ.md "wikilink")2009年3月26日[冷暖人間第九季的大結局播出後](../Page/:ja:渡る世間は鬼ばかり.md "wikilink")，正式廢止。此時段共播放了35個TBS制作的連續劇。

## 概要

## 作品列表

### 1970年代

### 1980年代

### 1990年代

#### 1990年

「[冷暖人間](../Page/冷暖人間.md "wikilink")」 第一季（10月－91年9月）

#### 1991年

#### 1992年

#### 1993年

  - 「[家栽之人](../Page/家栽之人.md "wikilink")」（1月～3月）出演：[片岡鶴太郎](../Page/:ja:片岡鶴太郎.md "wikilink")、[仙道敦子](../Page/:ja:仙道敦子.md "wikilink")、[風間トオル](../Page/:ja:風間トオル.md "wikilink")、[柄本明](../Page/:ja:柄本明.md "wikilink")
  - 「[冷暖人間](../Page/冷暖人間.md "wikilink")」 第二季（4月－94年3月）

#### 1994年

#### 1995年

  - 「[おかみ三代女の戦い](../Page/おかみ三代女の戦い.md "wikilink")」（1月～3月）出演：高橋由美子、山岡久乃、萬田久子、高橋恵子、峰竜太ほか
    制作：テレパック
  - 「[HOTEL](../Page/:ja:ホテル.md "wikilink")」
    第4シリーズ（4月～9月）出演：松方弘樹、高嶋政伸、赤坂晃、紺野美沙子、丹波哲郎ほか
    制作：東映、近藤照男プロダクション
  - 「[3年B組金八先生](../Page/3年B組金八先生.md "wikilink")」
    第4シリーズ（10月－96年3月）出演：武田鉄矢、星野真里、森田順平、茅島成美、金田明夫ほか

#### 1996年

  - 「[冷暖人間](../Page/冷暖人間.md "wikilink")」 第三季（4月－97年3月）

#### 1997年

  - 「[新幹線'97戀物語](../Page/新幹線'97戀物語.md "wikilink")」（4月～6月）出演：[高島政伸](../Page/:ja:高嶋政伸.md "wikilink")、[渡辺えり](../Page/:ja:渡辺えり.md "wikilink")、[南野陽子](../Page/:ja:南野陽子.md "wikilink")、[赤坂晃](../Page/赤坂晃.md "wikilink")、[柳澤慎吾](../Page/柳澤慎吾.md "wikilink")
    制作：大映電視
  - 「[寶貝兒子](../Page/寶貝兒子.md "wikilink")」（7月～9月）出演：[淺野優子](../Page/:ja:浅野ゆう子.md "wikilink")、[片岡鶴太郎](../Page/:ja:片岡鶴太郎.md "wikilink")、[永作博美](../Page/永作博美.md "wikilink")、[国分太一](../Page/:ja:国分太一.md "wikilink")、[山岡久乃](../Page/山岡久乃.md "wikilink")
  - 「番茶も出花」（10月－98年3月）出演：[泉萍子](../Page/:ja:泉ピン子.md "wikilink")、[一路真輝](../Page/:ja:一路真輝.md "wikilink")、[山口達也](../Page/山口達也.md "wikilink")、[小林桂樹](../Page/小林桂樹.md "wikilink")

#### 1998年

  - 「[飯店一族](../Page/飯店一族.md "wikilink")」 第五部（4月～6月）制作：東映、近藤照男製作公司
  - 「[你不孤單](../Page/你不孤單.md "wikilink")」（7月～9月）出演：[浜田雅功](../Page/:ja:浜田雅功.md "wikilink")、[永作博美](../Page/永作博美.md "wikilink")、[濱田岳](../Page/:ja:濱田岳.md "wikilink")、[純名里沙](../Page/純名里沙.md "wikilink")、[加賀真理子](../Page/:ja:加賀まりこ.md "wikilink")
  - 「[冷暖人間](../Page/冷暖人間.md "wikilink")」 第四季（10月－99年9月）

#### 1999年

  - 「[3年B組金八先生](../Page/3年B組金八先生.md "wikilink")」 第五部（10月－00年3月）

### 2000年代

#### 2000年

  - 「[讓愛說出來](../Page/讓愛說出來.md "wikilink")」（4月～6月）出演：[ともさかりえ](../Page/:ja:ともさかりえ.md "wikilink")、[上川隆也](../Page/:ja:上川隆也.md "wikilink")、[藤原竜也](../Page/:ja:藤原竜也.md "wikilink")、[真行寺君枝](../Page/:ja:真行寺君枝.md "wikilink")、[加藤茶](../Page/:ja:加藤茶.md "wikilink")
  - 「[20歲的結婚](../Page/20歲的結婚.md "wikilink")」（7月～9月）出演：[優香](../Page/優香.md "wikilink")、[米倉涼子](../Page/米倉涼子.md "wikilink")、[押尾学](../Page/:ja:押尾学.md "wikilink")、[宮崎美子](../Page/:ja:宮崎美子.md "wikilink")、[中尾彬](../Page/:ja:中尾彬.md "wikilink")
    制作：AVEC
  - 「[冷暖人間](../Page/冷暖人間.md "wikilink")」 第五季（10月－01年9月）

#### 2001年

  - 「[3年B組金八先生](../Page/3年B組金八先生.md "wikilink")」 第六部（10月－02年3月）

#### 2002年

  - 「[冷暖人間](../Page/冷暖人間.md "wikilink")」 第六季（4月－03年3月）

#### 2003年

  - 「[爸氣十足](../Page/爸氣十足.md "wikilink")」（4月～6月）出演：[反町隆史](../Page/:ja:反町隆史.md "wikilink")、[矢田亜希子](../Page/:ja:矢田亜希子.md "wikilink")、[小西真奈美](../Page/:ja:小西真奈美.md "wikilink")
    制作：AVEC
  - 「[為了愛人想被愛](../Page/為了愛人想被愛.md "wikilink")」（7月～9月）出演：[坂口憲二](../Page/:ja:坂口憲二.md "wikilink")、[黒木瞳](../Page/:ja:黒木瞳.md "wikilink")、[菊川怜](../Page/:ja:菊川玲.md "wikilink")、[柳葉敏郎](../Page/:ja:柳葉敏郎.md "wikilink")
    制作：共同電視
  - 「エ・アロール」（10月～12月）出演：[豊川悦司](../Page/:ja:豊川悦司.md "wikilink")、[木村佳乃](../Page/:ja:木村佳乃.md "wikilink")、[草笛光子](../Page/:ja:草笛光子.md "wikilink")
    制作：ドリマックス・テレビジョン

#### 2004年

  - 「[上班族金太郎4](../Page/上班族金太郎.md "wikilink")」（1月～3月）出演：[高橋克典](../Page/:ja:高橋克典.md "wikilink")、[羽田美智子](../Page/:ja:羽田美智子.md "wikilink")、[長嶋一茂](../Page/長嶋一茂.md "wikilink")、[保坂尚希](../Page/:ja:保阪尚希.md "wikilink")、[內藤剛志](../Page/:ja:内藤剛志.md "wikilink")、[野際陽子](../Page/野際陽子.md "wikilink")、[高橋英樹](../Page/:ja:高橋英樹_\(俳優\).md "wikilink")
    制作：ドリマックス・テレビジョン
  - 「[冷暖人間](../Page/冷暖人間.md "wikilink")」 第七季（4月－05年3月）

#### 2005年

  - 「[在夢中相見](../Page/在夢中相見.md "wikilink")」（4月～6月）出演：[矢田亞希子](../Page/矢田亞希子.md "wikilink")、[長塚京三](../Page/:ja:長塚京三.md "wikilink")、押尾学、[永井大](../Page/:ja:永井大.md "wikilink")、[野際陽子](../Page/野際陽子.md "wikilink")
    制作：ドリマックス・テレビジョン
  - 「[神啊請給我幸福](../Page/神啊請給我幸福.md "wikilink")」（7月～9月）出演：[深田恭子](../Page/深田恭子.md "wikilink")、[松下由樹](../Page/松下由樹.md "wikilink")、[伊原剛志](../Page/:ja:伊原剛志.md "wikilink")、[谷原章介](../Page/:ja:谷原章介.md "wikilink")、[津川雅彦](../Page/:ja:津川雅彦.md "wikilink")
    制作：ドリマックス・テレビジョン
  - 「[求愛三兄弟](../Page/求愛三兄弟.md "wikilink")」（10月～12月）出演：[田中美佐子](../Page/:ja:田中美佐子.md "wikilink")、[玉山鉄二](../Page/:ja:玉山鉄二.md "wikilink")、[国仲涼子](../Page/:ja:国仲涼子.md "wikilink")、[速水もこみち](../Page/:ja:速水もこみち.md "wikilink")、[中尾明慶](../Page/:ja:中尾明慶.md "wikilink")、[生瀬勝久](../Page/:ja:生瀬勝久.md "wikilink")

#### 2006年

  - 1月-3月：「[白夜行](../Page/白夜行.md "wikilink")」　主演：[山田孝之](../Page/:ja:山田孝之.md "wikilink")、[綾瀬はるか](../Page/:ja:綾瀬はるか.md "wikilink")、[武田鉄矢](../Page/:ja:武田鉄矢.md "wikilink")、[渡部篤郎](../Page/:ja:渡部篤郎.md "wikilink")、[柏原崇](../Page/:ja:柏原崇.md "wikilink")、[小出恵介](../Page/:ja:小出恵介.md "wikilink")
  - 4月－2007年3月：「[冷暖人間](../Page/冷暖人間.md "wikilink")」第八季

#### 2007年

  - 4月-6月：「[夫婦道](../Page/夫婦道.md "wikilink")」　主演：[武田鉄矢](../Page/:ja:武田鉄矢.md "wikilink")、[高畑淳子](../Page/:ja:高畑淳子.md "wikilink")、[山崎静代](../Page/:ja:山崎静代.md "wikilink")
  - 7月-9月：「[婆媳大戰](../Page/婆媳大戰.md "wikilink")」　主演：[江角マキコ](../Page/:ja:江角マキコ.md "wikilink")、[野際陽子](../Page/:ja:野際陽子.md "wikilink")、[沢村一樹](../Page/:ja:沢村一樹.md "wikilink")、[伊東四朗](../Page/:ja:伊東四朗.md "wikilink")
  - 10月－2008年3月：「[3年B組金八先生](../Page/3年B組金八先生.md "wikilink")」第八部

#### 2008年

  - 4月－2009年3月：「[冷暖人間](../Page/冷暖人間.md "wikilink")」第九季

#### 2010年

  - 10月－2011年9月：「[冷暖人間](../Page/冷暖人間.md "wikilink")」第十季

[Category:日本電視台電視劇列表](../Category/日本電視台電視劇列表.md "wikilink")
[TBS電視劇](../Category/TBS電視劇.md "wikilink")
[\*](../Category/TBS週四晚間九點連續劇.md "wikilink")
[日本電視劇播放時段](../Category/日本電視劇播放時段.md "wikilink")