《**TENNIS
Magazine**》是一本每月發行的[網球](../Page/網球.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")，介紹世界上最重要的網球[新聞](../Page/新聞.md "wikilink")。

## 網球史上最偉大的40位球員

《TENNIS Magazine》在2005年评选出网球史上最伟大的40名球员名单，用以庆祝其创刊40周年。

  - 名字后带星号者为入选该名单时尚未退役的球员。

|      |                                                      |                                           |
| ---- | ---------------------------------------------------- | ----------------------------------------- |
| 1\.  | [皮特·桑普拉斯](../Page/皮特·桑普拉斯.md "wikilink")             | [美国](../Page/美国.md "wikilink")            |
| 2\.  | [玛蒂娜·纳芙拉蒂洛娃](../Page/玛蒂娜·纳芙拉蒂洛娃.md "wikilink")\*     | [捷克斯洛伐克](../Page/捷克斯洛伐克.md "wikilink")／美国 |
| 3\.  | [施特菲·格拉芙](../Page/施特菲·格拉芙.md "wikilink")             | [德国](../Page/德国.md "wikilink")            |
| 4\.  | [克里斯·埃弗特](../Page/克里斯·埃弗特.md "wikilink")             | 美国                                        |
| 5\.  | [比约·博格](../Page/比约·博格.md "wikilink")                 | [瑞典](../Page/瑞典.md "wikilink")            |
| 6\.  | [玛格丽特·考特](../Page/玛格丽特·考特.md "wikilink")             | [澳大利亚](../Page/澳大利亚.md "wikilink")        |
| 7\.  | [吉米·康诺尔斯](../Page/吉米·康诺尔斯.md "wikilink")             | 美国                                        |
| 8\.  | [罗德·拉沃](../Page/罗德·拉沃.md "wikilink")                 | 澳大利亚                                      |
| 9\.  | [比莉·珍·金](../Page/比莉·珍·金.md "wikilink")               | 美国                                        |
| 10\. | [伊万·伦德尔](../Page/伊万·伦德尔.md "wikilink")               | 捷克斯洛伐克／美国                                 |
| 11\. | [约翰·麦肯罗](../Page/约翰·麦肯罗.md "wikilink")               | 美国                                        |
| 12\. | [安德烈·阿加西](../Page/安德烈·阿加西.md "wikilink")\*           | 美国                                        |
| 13\. | [莫尼卡·塞莱斯](../Page/莫尼卡·塞莱斯.md "wikilink")\*           | [南斯拉夫](../Page/南斯拉夫.md "wikilink")／美国     |
| 14\. | [斯特凡·埃德博格](../Page/斯特凡·埃德博格.md "wikilink")           | 瑞典                                        |
| 15\. | [马茨·维兰德](../Page/马茨·维兰德.md "wikilink")               | 瑞典                                        |
| 16\. | [约翰·纽康姆](../Page/紐康姆.md "wikilink")                  | 澳大利亚                                      |
| 17\. | [塞莱娜·威廉姆斯](../Page/塞莱娜·威廉姆斯.md "wikilink")\*         | 美国                                        |
| 18\. | [鲍里斯·贝克尔](../Page/鲍里斯·贝克尔.md "wikilink")             | 德国                                        |
| 19\. | [罗杰·费德勒](../Page/罗杰·费德勒.md "wikilink")\*             | [瑞士](../Page/瑞士.md "wikilink")            |
| 20\. | [肯·罗斯威尔](../Page/肯·罗斯威尔.md "wikilink")               | 澳大利亚                                      |
| 21\. | [罗伊·爱默森](../Page/罗伊·爱默森.md "wikilink")               | 澳大利亚                                      |
| 22\. | [玛蒂娜·辛吉斯](../Page/玛蒂娜·辛吉斯.md "wikilink")\*           | 瑞士                                        |
| 23\. | [伊芳·谷拉恭](../Page/伊芳·谷拉恭.md "wikilink")               | 澳大利亚                                      |
| 24\. | [吉尔勒莫·维拉斯](../Page/吉尔勒莫·维拉斯.md "wikilink")           | [阿根廷](../Page/阿根廷.md "wikilink")          |
| 25\. | [维纳斯·威廉姆斯](../Page/维纳斯·威廉姆斯.md "wikilink")\*         | 美国                                        |
| 26\. | [吉姆·考瑞尔](../Page/吉姆·考瑞尔.md "wikilink")               | 美国                                        |
| 27\. | [阿兰特萨·桑切斯·比卡里奥](../Page/阿兰特萨·桑切斯·比卡里奥.md "wikilink") | [西班牙](../Page/西班牙.md "wikilink")          |
| 28\. | [伊利耶·纳斯塔塞](../Page/伊利耶·纳斯塔塞.md "wikilink")           | [罗马尼亚](../Page/罗马尼亚.md "wikilink")        |
| 29\. | [林达塞·达文波特](../Page/林达塞·达文波特.md "wikilink")\*         | 美国                                        |
| 30\. | [阿瑟·阿什](../Page/阿瑟·阿什.md "wikilink")                 | 美国                                        |
| 31\. | [贾斯汀·海宁·哈德恩](../Page/贾斯汀·海宁·哈德恩.md "wikilink")\*     | [比利时](../Page/比利时.md "wikilink")          |
| 32\. | [特雷西·奥斯丁](../Page/特雷西·奥斯丁.md "wikilink")             | 美国                                        |
| 33\. | [哈纳·曼德利科娃](../Page/哈纳·曼德利科娃.md "wikilink")           | 捷克斯洛伐克／澳大利亚                               |
| 34\. | [雷登·休伊特](../Page/雷登·休伊特.md "wikilink")\*             | 澳大利亚                                      |
| 35\. | [斯坦·史密斯](../Page/斯坦·史密斯.md "wikilink")               | 美国                                        |
| 36\. | [詹尼弗·卡普里亚蒂](../Page/詹尼弗·卡普里亚蒂.md "wikilink")\*       | 美国                                        |
| 37\. | [古斯塔沃·库尔滕](../Page/古斯塔沃·库尔滕.md "wikilink")\*         | [巴西](../Page/巴西.md "wikilink")            |
| 38\. | [弗吉尼亚·瓦德](../Page/弗吉尼亚·瓦德.md "wikilink")             | [英国](../Page/英国.md "wikilink")            |
| 39\. | [帕特里克·拉夫特](../Page/帕特里克·拉夫特.md "wikilink")           | 澳大利亚                                      |
| 40\. | [加布莱拉·萨巴蒂尼](../Page/加布莱拉·萨巴蒂尼.md "wikilink")         | 阿根廷                                       |

## 外部連結

  - [Official website](http://www.tennis.com/)
  - [中文《网球》杂志](http://www.taimo.cn/category-225.html)

[Category:網球](../Category/網球.md "wikilink")
[Category:月刊](../Category/月刊.md "wikilink")