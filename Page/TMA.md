**TMA**（）是[日本一家](../Page/日本.md "wikilink")[成人视频制造商](../Page/成人视频制造商.md "wikilink")。

## 公司简介

TMA于1990年10月成立于[东京都](../Page/东京都.md "wikilink")[丰岛区](../Page/丰岛区.md "wikilink")，主业为拍摄[角色扮演色情影片](../Page/角色扮演色情影片.md "wikilink")\[1\]；其作品大多基于时下流行的漫画或动画\[2\]，如《[幸運☆星](../Page/幸運☆星.md "wikilink")》、《[新世纪福音战士](../Page/新世纪福音战士.md "wikilink")》以及《[轻音少女](../Page/轻音少女.md "wikilink")》。

TMA亦是探索成人色情影片[新媒体的先锋](../Page/新媒体.md "wikilink")。2005年8月12日，TMA发布了适用于[PlayStation
Portable的以](../Page/PlayStation_Portable.md "wikilink")[通用媒体光盘作为](../Page/通用媒体光盘.md "wikilink")[存储介质的影片](../Page/存储介质.md "wikilink")\[3\]；两年后，当[Blu-ray格式还在与](../Page/Blu-ray.md "wikilink")[HD
DVD竞争时](../Page/HD_DVD.md "wikilink")，TMA发售了成人色情影片业界第一张[蓝光光碟作品](../Page/蓝光光碟.md "wikilink")，成为日本第一家以蓝光光碟作为存储介质进行成人色情影片发售的公司。\[4\]

TMA现在产量为每个月16部影片。

## 主要作品

<table>
<thead>
<tr class="header">
<th><p>作品名称</p></th>
<th><p>来源作品</p></th>
<th><p>作品译名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>{{lang|ja|Pureキャロットへようこそ</p></td>
<td><p>}}</p></td>
<td><p>{{lang|ja|[[Piaキャロットへようこそ</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>圣母在上</p></td>
</tr>
<tr class="odd">
<td><p>Faith/stay knight</p></td>
<td><p><a href="../Page/Fate/stay_night.md" title="wikilink">Fate/stay night</a></p></td>
<td><p>Faith/stay knight</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>那就是我的主人</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>凉宫Hahiru的忧郁</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>Rozen Maiden</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>闪亮☆星</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>Cosplay无双</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>CODE EROSU 淫虐的Cosplayer</p></td>
</tr>
<tr class="even">
<td><p>EVER RE TAKE</p></td>
<td><p>的同人作品「RE TAKE」</p></td>
<td><p>EVER RE TAKE</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/CLANNAD.md" title="wikilink">クラナド</a></p></td>
<td><p>CLONNAD</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>Cosplayer Frontier</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>Cosplay例大祭</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>Cosplayer魔女</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>轻音部！</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>我的义妹哪有这么好色！</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>魔法性女Erosu☆Magika</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>下流伪装少女</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/日常.md" title="wikilink">日常</a></p></td>
<td><p>非日常</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/Steins;Gate.md" title="wikilink">Steins;Gate</a></p></td>
<td><p>LAYERS;GATE</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>GACHIYURI</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/Fate/Zero.md" title="wikilink">Fate/Zero</a></p></td>
<td><p>Faith/ero</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/ONE_PIECE.md" title="wikilink">ONE PIECE</a></p></td>
<td><p>ONE PENIS</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/光之美少女.md" title="wikilink">ふたりはプリキュア</a></p></td>
<td><p>Pretty战士</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>LoveAibu!</p></td>
</tr>
<tr class="even">
<td><p>{{lang|ja|ラブアイブ! サンシャイン</p></td>
<td><p>}}</p></td>
<td><p>{{lang|ja|[[LoveLive!_Sunshine</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>请问您今天要来点姑娘吗？</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>兽耳Cosplayers</p></td>
</tr>
<tr class="odd">
<td><p>Faith/Grand Orgasm</p></td>
<td><p><a href="../Page/Fate/Grand_Order.md" title="wikilink">Fate/Grand Order</a></p></td>
<td><p>Faith/Grand Orgasm</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>色情露营△</p></td>
</tr>
</tbody>
</table>

## 来源

## 外部链接

  - [TMA官网](http://www.tma.co.jp/)

[Category:日本色情片公司](../Category/日本色情片公司.md "wikilink")
[Category:丰岛区公司](../Category/丰岛区公司.md "wikilink")
[Category:1990年成立的公司](../Category/1990年成立的公司.md "wikilink")

1.  ["日映審系AVメーカー（た～わ行）](http://allabout.co.jp/gm/gl/13144/) Otsubo,
    Kemuta 2010-06-30
2.
3.
4.