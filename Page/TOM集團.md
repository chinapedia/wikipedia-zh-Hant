**TOM集團有限公司**（股份代號：2383）為[香港聯合交易所主板上市的](../Page/香港聯合交易所.md "wikilink")[媒體與](../Page/媒體.md "wikilink")[科技公司](../Page/科技.md "wikilink")。除了經營[出版和廣告的媒體業務外](../Page/出版.md "wikilink")，TOM集團也是一個科技平台，營運業務包括[電子商貿](../Page/電子商貿.md "wikilink")、[社交網絡](../Page/社交網絡.md "wikilink")、移動互聯網；其策略投資領域則涵蓋金融科技和先進數據分析。TOM集團為[長江和記實業有限公司成員](../Page/長江和記實業.md "wikilink")。公司在[開曼群島註冊](../Page/開曼群島.md "wikilink")，主席為[陸法蘭](../Page/陸法蘭.md "wikilink")。

TOM集團（前稱**TOM.COM**）2000年3月1日在[香港創業板上市](../Page/香港創業板.md "wikilink")，當時上市編號為8001.HK。TOM.COM當時上市招股價為1.78元，上市首日收報7.75元，較招股價上升3.35倍。\[1\]

TOM集團2001年底收購[台灣](../Page/台灣.md "wikilink")[出版集團](../Page/出版.md "wikilink")[城邦文化](../Page/城邦文化.md "wikilink")。2006年1月25日集團宣佈：首席執行官[王兟以希望投入更多時間於私人股权投資為由向董事局請辭](../Page/王兟.md "wikilink")，并于当年1月27日生效，財務總監[湯美娟繼任](../Page/湯美娟.md "wikilink")。王兟在辞去TOM集团首席执行官后，随即加入美国[私人股权投资公司](../Page/私人股权投资.md "wikilink")[德州太平洋集团](../Page/德州太平洋集团.md "wikilink")，并担任投资合伙人。TOM集團現任首席執行官兼執行董事為楊國猛，首席財務官兼執行董事為麥淑芬。

TOM集團於2004年分拆[TOM
Online在香港創業板上市](../Page/TOM_Online.md "wikilink")，然後再在2007年[私有化TOM](../Page/私有化.md "wikilink")
Online。

2006年12月20日TOM和[eBay同时宣布建立合资公司](../Page/eBay.md "wikilink")，这也成为Tom集团的一个里程碑。\[2\]

2010年8月10日，[中国邮政集团公司与TOM集团有限公司合资建设的](../Page/中国邮政集团公司.md "wikilink")[B2C](../Page/B2C.md "wikilink")[网上购物平台](../Page/网上购物.md "wikilink")《[郵樂網](../Page/郵樂網.md "wikilink")》正式上线，由中国邮政和TOM集团分别持股51%和49%的合资公司“北京邮乐电子商务有限公司”负责运营。

現時TOM集團的主要股東是[周凱旋](../Page/周凱旋.md "wikilink")。\[3\]

## 主要资产

  - [郵樂控股](../Page/郵樂控股.md "wikilink") 44.24%
  - [华娱卫视](../Page/华娱卫视.md "wikilink") 99.99%
  - [城邦媒體控股集團](../Page/城邦媒體控股集團.md "wikilink") -
    [城邦文化](../Page/城邦文化.md "wikilink")
  - [TOM在線](../Page/TOM在線.md "wikilink")
  - TOM戶外傳媒集團
  - [易趣](../Page/易趣.md "wikilink")
  - [電腦報](../Page/電腦報.md "wikilink") 49%
  - [幻劍書盟網站](../Page/幻劍書盟網站.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [TOM集團](https://web.archive.org/web/20090422174951/http://www.tomgroup.com/)
  - [大陆子公司TOM.COM](http://www.tom.com/)
  - [郵樂網](http://www.ule.com/)
  - [幻劍書盟](http://hjsm.tom.com/)

[Category:香港交易所上市公司](../Category/香港交易所上市公司.md "wikilink")
[Category:香港上市綜合企業公司](../Category/香港上市綜合企業公司.md "wikilink")
[Category:長和集團](../Category/長和集團.md "wikilink")
[Category:香港網站](../Category/香港網站.md "wikilink")
[Category:香港出版社](../Category/香港出版社.md "wikilink")
[Category:2000年成立的公司](../Category/2000年成立的公司.md "wikilink")

1.
2.  详讯：eBay与TOM在线宣布建合资公司，http://tech.sina.com.cn/i/2006-12-20/08441297000.shtml
3.