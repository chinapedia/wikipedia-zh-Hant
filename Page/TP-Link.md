**TP-Link**（中文全称：**普联技术有限公司**）是[中国的](../Page/中国.md "wikilink")[网络通讯设备研制企业](../Page/网络通讯设备.md "wikilink")，成立于1996年，其总部位于[深圳](../Page/深圳.md "wikilink")。

## 產品線

[TP_Link_2.jpg](https://zh.wikipedia.org/wiki/File:TP_Link_2.jpg "fig:TP_Link_2.jpg")
TP-Link的產品線包括[路由器](../Page/路由器.md "wikilink")、[交換器](../Page/網路交換器.md "wikilink")、[網路中繼器（包括家用無線區域網信號延伸器）](../Page/中继器.md "wikilink")、[ADSL裝置](../Page/ADSL.md "wikilink")、乙太網路/無線區域網適配器、[手機](../Page/手機.md "wikilink")、[網路監控攝影機](../Page/網路監控攝影機.md "wikilink")、[電力線通信裝置](../Page/電力線通信.md "wikilink")、、外掛電源等。除此以外也有代工產品，例如[Google的](../Page/Google.md "wikilink")。\[1\]2016年為旗下的手機部門另立新品牌Neffos。\[2\]除此以外TP-Link也以Kasa品牌涉足智慧型家居裝置領域。\[3\]

其主要競爭廠商有[Netgear](../Page/Netgear.md "wikilink")、、[貝爾金](../Page/貝爾金.md "wikilink")、[Linksys](../Page/Linksys.md "wikilink")、[D-Link以及](../Page/D-Link.md "wikilink")[華碩](../Page/華碩.md "wikilink")。

### 手机

  - 2015年，TP-Link发布[Neffos手机品牌](../Page/Neffos.md "wikilink")。

## 参考文献

## 外部链接

  - [TP-Link中国大陆](http://www.tp-link.com.cn/)

  - [TP-Link台湾](http://www.tp-link.tw/)

  - [TP-Link美国](http://www.tp-link.com/us/)

[Category:中国品牌](../Category/中国品牌.md "wikilink")
[Category:深圳公司](../Category/深圳公司.md "wikilink")
[Category:中国电子公司](../Category/中国电子公司.md "wikilink")
[Category:中国民营企业](../Category/中国民营企业.md "wikilink")
[Category:网络硬件公司](../Category/网络硬件公司.md "wikilink")
[Category:1996年成立的公司](../Category/1996年成立的公司.md "wikilink")

1.
2.
3.
    TP-Link|website=www.tp-link.com|language=en-us|access-date=2017-09-16}}