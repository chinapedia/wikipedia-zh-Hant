**TPG网络**（，），可被理解为“完全外围设备小组”，通常称TPG或TPG网络。是[澳大利亚的一家](../Page/澳大利亚.md "wikilink")[因特网服务提供商以及](../Page/因特网服务提供者.md "wikilink")[IT公司](../Page/信息技术.md "wikilink")。隶属于[SP
Telemedia](../Page/SP_Telemedia.md "wikilink")（同为澳洲的通讯公司）。在过去的18年以来，TPG号称是[澳大利亚最大的](../Page/澳大利亚.md "wikilink")10家[因特网服务提供商之一](../Page/因特网服务提供者.md "wikilink")。最近，TPG公司着手于在[澳大利亚范围内建设自己的](../Page/澳大利亚.md "wikilink")[ADSL2+宽带网络](../Page/ADSL2+.md "wikilink")，采用的是线路共享技术。

TPG公司的服务范围，包括因特网接入服务、组建网络、OEM服务、会计软件等等。

2018年8月28日TPG網絡和沃達豐和記澳洲合併，新公司會易名為TPG
Telecom，及在澳洲交易所上市。沃達豐和記澳洲將持有合併後公司50.1%股權，TPG持有餘下49.1%股權。

## 部门分类

### TPG Internet

TPG宽带出售多种[因特网服务](../Page/因特网.md "wikilink")，例如[拨号上网](../Page/拨号上网.md "wikilink")、[ADSL](../Page/ADSL.md "wikilink")、[ADSL2+](../Page/ADSL2+.md "wikilink")、[VDSL2](../Page/VDSL2.md "wikilink")（依然处于测试阶段）、[SHDSL等等互联网接入服务](../Page/SHDSL.md "wikilink")，以及电子邮件、[网站以及](../Page/网站.md "wikilink")[域名主持](../Page/域名.md "wikilink")、[IPTV](../Page/IPTV.md "wikilink")、[VOIP解决方案及](../Page/VOIP.md "wikilink")[VOIP虚拟电话卡](../Page/VOIP.md "wikilink")。

#### IPTV

经过了短暂的测试，TPG于2007年6月12日正式开始提供IPTV服务。[1](http://forums.whirlpool.net.au/forum-replies.cfm?t=760306).
IPTV面向ADSL2+客户推出，适用于启用了IPTV支持的电话交换机。早先推出的电视频道列表如下：

  - [Net 25](../Page/Net_25.md "wikilink")
  - [DW-TV](../Page/DW-TV.md "wikilink")
  - [Bloomberg Television](../Page/Bloomberg_Television.md "wikilink")
  - [Al Jazeera English](../Page/Al_Jazeera_English.md "wikilink")
  - [TV5MONDE](../Page/TV5MONDE.md "wikilink")
  - [Eurosport](../Page/Eurosport.md "wikilink")
  - [Eurosport News](../Page/Eurosport_News.md "wikilink")
  - [凤凰卫视资讯台](../Page/凤凰卫视资讯台.md "wikilink")
  - [Channel NewsAsia](../Page/Channel_NewsAsia.md "wikilink")
  - [Russia Today TV](../Page/Russia_Today_TV.md "wikilink")
  - [TRT
    International](../Page/Turkish_Radio_and_Television_Corporation.md "wikilink")
  - [Playboy Channel](../Page/Playboy_Channel.md "wikilink")
  - [Adult 2](../Page/Adult_2.md "wikilink")

目前的频道几乎全是新闻台，完全免费，未来将会有需要订阅的频道推出。

### TPG Network

TPG
Network向社团提供[WAN服务](../Page/广域网.md "wikilink")。本部门同时也出售因特网[VPN以及其他的企业网络连接服务](../Page/VPN.md "wikilink")。TPG
Network也面向中小型商务客户提供IP PABX。

### TPG Systems

TPG
Systems是[PC](../Page/个人电脑.md "wikilink")、[笔记本电脑](../Page/笔记本电脑.md "wikilink")、[服务器等的](../Page/服务器.md "wikilink")[原始设备制造商](../Page/OEM.md "wikilink")。此部门在2004年初已停止营业。

### TPG Software

TPG Software，采用[Catsoft商标](../Page/Catsoft.md "wikilink")，向商业客户出售经算软件。

### TPG Boomerang TV

TPG Boomerang TV是通过[Intelsat
8卫星播放收费电视的服务](../Page/Intelsat_8.md "wikilink")，但目前已停止。频道阵容：

  - [Cartoon Network](../Page/Cartoon_Network_Australia.md "wikilink")
  - [CNNfn](../Page/CNNfn.md "wikilink")
  - [Animal
    Planet](../Page/Animal_Planet_Australia/New_Zealand.md "wikilink")
  - [有线新闻网](../Page/有线新闻网.md "wikilink")
  - [ESPN](../Page/ESPN_Australia.md "wikilink")
  - [Turner Classic
    Movies](../Page/Turner_Classic_Movies_\(Asia\).md "wikilink")

## 外部链接

  - [TPG公司的官方网站（英文）](http://www.tpg.com.au)
  - [TPG System的官方网站（英文）](http://www.pc.tpg.com.au)
  - [Alexa网站上的TPG公司信息（英文）](http://www.alexa.com/data/details/?url=tpg.com.au)
  - [Catsoft（英文）](http://www.catsoft.tpg.com.au)

[Category:澳大利亚公司](../Category/澳大利亚公司.md "wikilink")
[Category:ISP](../Category/ISP.md "wikilink")