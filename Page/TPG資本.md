**TPG資本**（TPG Capital），原名**德州太平洋集團**（Texas Pacific
Group），是美国的最大的[私人股权投资公司之一](../Page/私人股权投资.md "wikilink")，由、和於1992年創立。德州太平洋集團在[沃斯堡](../Page/沃斯堡.md "wikilink")、[門洛帕克](../Page/門洛帕克_\(加利福尼亞州\).md "wikilink")、[三藩市](../Page/三藩市.md "wikilink")、[香港](../Page/香港.md "wikilink")、[莫斯科](../Page/莫斯科.md "wikilink")、[上海](../Page/上海.md "wikilink")、[倫敦](../Page/倫敦.md "wikilink")、[孟買](../Page/孟買.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[盧森堡](../Page/盧森堡.md "wikilink")、[紐約](../Page/紐約市.md "wikilink")、[東京](../Page/東京.md "wikilink")、[墨爾本](../Page/墨爾本.md "wikilink")、[巴黎](../Page/巴黎.md "wikilink")、[華盛頓均設有分部](../Page/華盛頓哥倫比亞特區.md "wikilink")。\[1\]

德州太平洋集團的业务主要是为公司转型、管理層收購和资本重组提供资金支持。集團所投资的行业包括[媒體與](../Page/媒體.md "wikilink")[電訊](../Page/電訊.md "wikilink")、[工業](../Page/工業.md "wikilink")[股票](../Page/股票.md "wikilink")、[科技](../Page/科技.md "wikilink")，以及[醫療護理](../Page/醫療.md "wikilink")。集團現正在使用第四批資金──TPG
Partners IV,
L.P.进行投资。該批資金於2003年3月募集完成，擁有53億[美元的資本额度](../Page/美元.md "wikilink")。

## 重大投資

多年來，德州太平洋集團提議或進行了多項重大投資和交易：\[2\]\[3\]

  - 1993年，[大衛·波德曼收購宣佈破產的](../Page/大衛·波德曼.md "wikilink")[美國](../Page/美國.md "wikilink")[大陸航空](../Page/大陸航空.md "wikilink")，隨即創立德州太平洋集團。
  - 2002年7月，德州太平洋集團對[漢堡王](../Page/漢堡王.md "wikilink")、[貝恩資本和](../Page/貝恩資本.md "wikilink")[高盛資本夥伴進行了](../Page/高盛.md "wikilink")[管理層收購](../Page/管理層收購.md "wikilink")。2005年，集團與[新力公司和其他](../Page/新力公司.md "wikilink")[私人公司合資獲得了](../Page/私人公司.md "wikilink")[米高梅公司](../Page/米高梅公司.md "wikilink")，而高端零售商[內曼·馬庫斯亦收歸其下](../Page/內曼·馬庫斯.md "wikilink")。德州太平洋集團亦持有，並擁有和[J.
    Crew的股份](../Page/J._Crew.md "wikilink")。\[4\]
  - 2003年11月17日，[安隆提議購買](../Page/安隆.md "wikilink")。但是，各界對負債的關注、猜測以至已發行的文件，均指出德州太平洋集團很可能擴大短期盈利，最終使公用事業的消費者蒙受損失。[俄勒岡州的](../Page/俄勒岡州.md "wikilink")的監控員因而在2005年3月10日否決有關的購買事宜。
  - 2005年10月，德州太平洋集團以2,150萬[美元購入](../Page/美元.md "wikilink")[越南的](../Page/越南.md "wikilink")約6%[股權](../Page/股權.md "wikilink")。该公司的主营业务是[移动通讯服务以及移动电话的销售](../Page/移动通讯.md "wikilink")，值得一提的是该笔交易是[TOM集团前首席执行官](../Page/TOM集团.md "wikilink")[王兟成為德太合伙人後首宗參與的投資](../Page/王兟.md "wikilink")。\[5\]
  - 2006年6月，德州太平洋集團藉助[美國兒童電視巨富](../Page/美國.md "wikilink")，赢得了美國最大的[西班牙語媒體](../Page/西班牙語.md "wikilink")──[Univision的投標](../Page/Univision.md "wikilink")。但是，公司[股東向有關交易提出控訴](../Page/股東.md "wikilink")。\[6\]
  - 2006年6月23日，德州太平洋集團和新橋投資集團確認，已致函[香港的](../Page/香港.md "wikilink")[電訊盈科有限公司](../Page/電訊盈科有限公司.md "wikilink")[董事會](../Page/董事會.md "wikilink")，提交針對其[電訊傳媒資產的收購建議書](../Page/電訊.md "wikilink")。\[7\]
  - 2006年11月，據報，德州太平洋集團為了[收購](../Page/收購.md "wikilink")[澳洲航空公司](../Page/澳洲航空公司.md "wikilink")，與[澳洲的](../Page/澳洲.md "wikilink")[麥格理銀行合組名為](../Page/麥格理銀行.md "wikilink")「的國際財團。\[8\]
  - 2006年12月1日，據報，德州太平洋集團和[科爾伯格－克拉維斯－羅伯茨均著手研究可否對](../Page/科爾伯格－克拉維斯－羅伯茨.md "wikilink")[美國第二大零售商](../Page/美國.md "wikilink")[家得寶提出](../Page/家得寶.md "wikilink")1,000憶[美元的融資合併](../Page/美元.md "wikilink")。
  - 2006年12月19日，宣佈，董事局投票接納和德州太平洋集團以17億[美元或每股](../Page/美元.md "wikilink")90美元提出的收購。\[9\]
  - 2007年，德州太平洋集團正與[松下電器產業商談收購](../Page/松下電器產業.md "wikilink")[JVC](../Page/JVC.md "wikilink")。
  - 2014年6月23日TPG資本以12 .15億澳元(約合70 .75億元人民幣)收購戴德梁行
  - 2014年11月17日德州太平洋集團以3.04億英鎊收購英國連鎖餐廳Prezzo
  - 2015年5月11日**TPG資本**以20億美元收購[高緯環球](../Page/高緯環球.md "wikilink")，其中包括義大利阿涅利家族（Agnelli
    Family）旗下的控股公司Exo持有的75%股權。高緯環球與該公司旗下的戴德粱行合併成全球最大的房地產服務公司，合併後將會以[高緯環球作為新公司的品牌](../Page/高緯環球.md "wikilink")

## 殊榮

2005-2006年間，德州太平洋集團的表現獲媒體各界認同。該公司先後打入《[收購雜誌](../Page/湯森路透.md "wikilink")》的「年度公司」、《》雜誌的「年度最佳跨國公司」和《》的「年度北美大型私營上市公司」。而[新橋資本](../Page/新橋資本.md "wikilink")（TPG-Newbridge）獲[亞洲風險投資年會](../Page/亞洲風險投資年會.md "wikilink")（Asia
Venture Forum）評為「年度最佳公司」。

## 參考

## 外部連結

  - [德州太平洋集團](https://archive.is/20130104205627/http://www.texaspacificgroup.com/)
  - [德州太平洋集團風險投資](http://www.tpgventures.com)（風險投資附屬公司）

[Category:私人股权投资公司](../Category/私人股权投资公司.md "wikilink")
[Category:美国金融公司](../Category/美国金融公司.md "wikilink")
[Category:1992年成立的公司](../Category/1992年成立的公司.md "wikilink")

1.  [德州太平洋集團：聯絡](http://www.texaspacificgroup.com/contact/index.html)
2.  [TPG概況](http://biz.yahoo.com/ic/51/51545.html)，[雅虎](../Page/雅虎.md "wikilink")
3.  <http://www.fundinguniverse.com/company-histories/Texas-Pacific-Group-Inc-Company-History.html>

4.  [德州太平洋集團公司概況](http://biz.yahoo.com/ic/51/51545.html)，[雅虎](../Page/雅虎.md "wikilink")
5.  [德太1.67億購越南電訊軟件商](http://hk.news.yahoo.com/061221/12/1yq1d.html)
    ，《[明報](../Page/明報.md "wikilink")》/雅虎香港
6.  [「Univision股東控告董事會公司交易管理」](http://www.mercurynews.com/mld/mercurynews/news/local/states/california/northern_california/14932584.htm)，《[聖荷西信使報](../Page/聖荷西信使報.md "wikilink")》，2006年6月29日
7.  [新橋確認曾洽購電盈電訊及媒體資產](http://www.metroradio.com.hk/news/fnews/20060623125338.htm)，《[都市日報](../Page/都市日報.md "wikilink")》
8.  [「澳洲關注澳航投標」](http://www.theage.com.au/news/Business/Public-wont-fly-Qantas-if-broken-up/2006/11/22/1163871462015.html)，《[世紀報](../Page/世紀報.md "wikilink")》，2006年11月22日
9.  [「哈拉斯娛樂董事會同意每股$90競投」](http://www.lasvegassun.com/sunbin/stories/nevada/2006/dec/19/121910040.html)
    ，《[拉斯維加斯太陽報](../Page/拉斯維加斯太陽報.md "wikilink")》，2006年12月19日