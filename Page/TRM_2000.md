**2噸運載型全輪驅動式戰術卡車**（[法文](../Page/法文.md "wikilink")：Toutes Roues Motrices
2000，**TRM
2000**），為[法國](../Page/法國.md "wikilink")[雷諾汽車開發的軍用](../Page/雷諾汽車.md "wikilink")[卡車](../Page/卡車.md "wikilink")，被[法國等國家軍隊使用](../Page/法國.md "wikilink")。在不整地行進的速度上，遠超過過去的軍用[卡車](../Page/卡車.md "wikilink")。

## 概要

為了取代由1950年代開始使用的[GBC
180](../Page/GBC_180.md "wikilink")[卡車](../Page/卡車.md "wikilink")，[雷諾汽車在](../Page/雷諾汽車.md "wikilink")1970年代開始開發TRM（全輪驅動車）系列，至1981年TRM2000開始量產使用。

目前共計生產12,000台。

## 參考文獻

<div class="references-small">

  - [military-today.com Renault
    TRM 2000](http://www.military-today.com/trucks/renault_trm_2000.htm)

<references />

</div>

[Category:軍用卡車](../Category/軍用卡車.md "wikilink")