****（缩写****）是一種[自由又簡單的實時无损音频](../Page/自由软件.md "wikilink")[编解码器](../Page/编解码器.md "wikilink")。TTA是一种基于自适应预测过滤的无损音频压缩，\[1\]

## TTA压缩器

  - 可将数据压缩至30%的无损[音频数据压缩](../Page/音频数据压缩.md "wikilink")
  - 實時編碼／解碼[算法](../Page/算法.md "wikilink")
  - 操作快捷、对系统要求低
  - 支持多平台
  - [自由软件和](../Page/自由软件.md "wikilink")[开放源代码](../Page/开放源代码.md "wikilink")
  - 硬件支持

TTA是用于对多声道8、16、24
bits整型和32bitsIEEE浮点型的音訊WAV格式的无损压缩，压缩的大小范围是原文件大小的30%—70%。TTA格式主要目标不是追求最大的压缩率，而是对于硬件执行的编码算法最优化，同时支持ID3v1和ID3v2两种标签信息。

使用True
Audio编码，您可以将20张收藏的音频[CD存储到一张](../Page/CD.md "wikilink")[DVD-R盘上并播放](../Page/DVD-R.md "wikilink")。还能用流行的[ID3标签存储所有曲目的信息](../Page/ID3.md "wikilink")。

## TTA项目

  - 自由和简单的数据格式
  - 为所有流行媒体播放器提供插件
  - TTA [DirectShow滤波器](../Page/DirectShow.md "wikilink")
  - Tau Producer -
    用于[Windows的图形界面压缩器](../Page/Microsoft_Windows.md "wikilink")
  - 各种TTA的 C/C++开发库

## 參见

  - [无损数据压缩](../Page/无损数据压缩.md "wikilink")
  - [有损数据压缩](../Page/有损数据压缩.md "wikilink")
  - [音频数据压缩](../Page/音频数据压缩.md "wikilink")
  - [音频编码格式的比较](../Page/音频编码格式的比较.md "wikilink")

## 外部链接

  - [True Audio Software Project](http://www.true-audio.com)
  - [压缩理论](http://www.true-audio.com/codec.theory)
  - [TTA格式描述](http://www.true-audio.com/codec.format)
  - [True Audio编解码器比较](http://www.true-audio.com/codec.comparison)
  - [硬件支持](http://www.true-audio.com/codec.hardware)
  - [Tau
    Producer（用于Windows的图形界面TTA压缩器）](http://www.true-audio.com/ftp/WinTTA-setup.exe)
  - [Tau Producer Screen Shoots](http://www.true-audio.com/codec.images)
  - [Project @
    Corecodec.Org](https://web.archive.org/web/20050205013416/http://tta.corecodec.org/)
  - [Project @ Sourceforge.Net](http://tta.sourceforge.net/)

[Category:自由音频编解码器](../Category/自由音频编解码器.md "wikilink")
[Category:跨平台軟體](../Category/跨平台軟體.md "wikilink")
[Category:无损音频编解码器](../Category/无损音频编解码器.md "wikilink")

1.