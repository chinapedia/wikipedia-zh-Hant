**托卡列夫手槍**（[俄語](../Page/俄語.md "wikilink")：，意為：托卡列夫7.62毫米半自動手槍1930年型）是前[蘇聯製造的](../Page/蘇聯.md "wikilink")[半自動手槍之一](../Page/半自動手槍.md "wikilink")，於1930年定型，由[費德爾·華西列維奇·托卡列夫設計](../Page/費德爾·華西列維奇·托卡列夫.md "wikilink")，開發目的是用以取代[沙俄時期的](../Page/沙俄.md "wikilink")[納甘M1895轉輪手槍](../Page/納甘M1895轉輪手槍.md "wikilink")，有時又會被稱為**TT**（，意為：圖拉托卡列夫）。

## 設計

TT手槍在設計時明顯是深受[約翰·白朗寧的](../Page/約翰·白朗寧.md "wikilink")[M1903及](../Page/FN_M1903手槍.md "wikilink")[M1911等手槍的影響](../Page/M1911.md "wikilink")。

此槍採用[7.62×25毫米口徑手槍子彈](../Page/7.62×25mm托卡列夫手槍彈.md "wikilink")，在外觀及機械結構方面均與[FN
M1903相似](../Page/FN_M1903手槍.md "wikilink")，但發射時的槍機後座距離較短。

TT-30在開始投產後簡化了一些設計，如槍管、[套筒釋放鈕](../Page/手槍套筒.md "wikilink")、扳機及[底把等](../Page/机匣.md "wikilink")，以便更易於生產。\[1\]

這種改進型名為TT-33。為了降低生產成本，蘇聯在1946年再度簡化了TT-33的設計。

和同期一般自動手槍不同，TT手槍明顯是沒有額外保險裝置，也沒有手動擊錘保險，但仍然可以透把擊錘拉開一半後固定，此時仍然不能開槍但可以盡快縮短完全打開的時間，以實現[半待擊狀態](../Page/半待擊狀態.md "wikilink")，做到事實仍然安全狀態的另類保險方式。作為軍用手槍來說安全性是合格的，但並不適合用作警用手槍，因為除了安全問題以外，7.62毫米托卡列夫手槍彈的貫穿力被認為是過強。

## 历史

1930年，蘇聯革命議會要求以新型制代手槍取代[納甘M1895左輪手槍](../Page/納甘M1895左輪手槍.md "wikilink")\[2\]。在1920年，蘇聯軍方購入了大量的[德國製](../Page/德國.md "wikilink")[毛瑟C96手槍](../Page/毛瑟C96手槍.md "wikilink")。其後因其採用火力強大的7.63×25亳米槍彈而深受[蘇聯紅軍青睞](../Page/蘇聯紅軍.md "wikilink")，這令蘇聯決定自行研製自己的手槍。

不久，蘇聯在毛瑟公司的生產許可下仿製了7.63×25亳米槍彈並加以改良，命名為「7.62毫米手槍彈」（即是7.62×25亳米托卡列夫槍彈）。

經過一場競爭後，於1931年1月7日，由費德爾·華西列維奇·托卡列夫設計的TT-30被蘇軍選中並成為了新型制式手槍，數星期後，1000把TT-30開始投產並裝備蘇聯紅軍。

## 採用

TT-33成為了[二戰中廣為蘇軍使用的手槍](../Page/二戰.md "wikilink")，但直至二戰終結時也沒有完全取代納甘M1895。[二戰开始初年](../Page/二戰.md "wikilink")，TT-33开始被大量投入生產並裝備部隊。在1941年6月22日，蘇聯紅軍已收到大約六百萬支TT-33，在戰爭期間，該槍的生產量再度增加。[納粹德軍在二戰時並有使用部份繳獲的手槍](../Page/納粹德軍.md "wikilink")，並把這些戰利品命名為「Pistole
615（r）」。

在1951年，當蘇軍列裝9毫米口徑的[馬卡洛夫PM以後](../Page/馬卡洛夫手槍.md "wikilink")，TT-33便漸漸地退出蘇軍前線裝備。儘管如此，直到1970年代，一些當地警察部隊仍然有裝備。

中國自參與韓戰的1951年開始引進TT手槍，之後還仿製成[51式手槍](../Page/51式手槍.md "wikilink")，51式再作改良為[54式手槍](../Page/54式手槍.md "wikilink")。由於在其手柄有一顆星，54式被俗稱為黑星手槍。

## 現況

蘇聯於1954年停止生產TT-33後，便把机器賣給多个[東方陣營國家](../Page/東方陣營.md "wikilink")，並允許他們進行仿製，有些國家至今仍有生產及採用仿製品。在[蘇聯解體後](../Page/蘇聯解體.md "wikilink")，TT-33仍在多個繼承國的軍警中服役或用作儲備（包括[俄羅斯及](../Page/俄羅斯.md "wikilink")[烏克蘭](../Page/烏克蘭.md "wikilink")）。

除了一些[第三世界國家的部隊外](../Page/第三世界國家.md "wikilink")，TT-33已成為一款廣被[犯罪組織及](../Page/犯罪組織.md "wikilink")[恐怖組織使用的槍支](../Page/恐怖組織.md "wikilink")。主要原因是其低廉的成本並使不法份子容易從[黑市中購買](../Page/黑市.md "wikilink")（在黑市中TT-33佔了不少數目，當中不少是從前蘇聯的[軍火庫中盗取的](../Page/軍火庫.md "wikilink")），另外也可能與蘇聯在[冷戰期間大量對外輸出武器有關](../Page/冷戰.md "wikilink")。一些美國和加拿大的居民也購買用作自衛武器的用途。

## 衍生型

仿製過TT-33的國家包括[中國](../Page/中華人民共和國.md "wikilink")、[北韓](../Page/北韓.md "wikilink")、[匈牙利](../Page/匈牙利.md "wikilink")、[波蘭](../Page/波蘭.md "wikilink")、[罗馬尼亞及](../Page/罗馬尼亞.md "wikilink")[南斯拉夫等](../Page/南斯拉夫.md "wikilink")，包括：

  - [中國](../Page/中國.md "wikilink")：[51式](../Page/51式手槍.md "wikilink")、[54式](../Page/54式手枪.md "wikilink")
  - [南斯拉夫](../Page/南斯拉夫.md "wikilink")：[M57](../Page/M57手槍.md "wikilink")
  - [朝鲜民主主义人民共和国](../Page/朝鲜民主主义人民共和国.md "wikilink")：[68式](../Page/68式手枪.md "wikilink")
  - [埃及](../Page/埃及.md "wikilink")：[Tokagypt](../Page/Tokagypt手槍.md "wikilink")
  - [羅馬尼亞](../Page/羅馬尼亞.md "wikilink")：[Carpaţi](../Page/Carpaţi手槍.md "wikilink")
  - [波蘭](../Page/波蘭.md "wikilink")：[Wz.33](../Page/Wz.33手槍.md "wikilink")
  - [匈牙利](../Page/匈牙利.md "wikilink")：[M48](../Page/M48手槍.md "wikilink")

[USSROfficerTT33.JPG](https://zh.wikipedia.org/wiki/File:USSROfficerTT33.JPG "fig:USSROfficerTT33.JPG")

## 優点及缺点

### 优点

  - 所用的7.62×25毫米子彈貫穿力強大
  - 射程遠
  - 體積小
  - 重量輕
  - 成本低廉
  - 握持及攜帶方便
  - 易於裝配和拆卸
  - 可靠性優異

### 缺点

  - 子彈數少
  - 偶爾會出現卡彈或走火現象
  - 性能落後現代的新型手槍
  - 沒有保險裝置\[3\]
  - 7.62×25毫米子彈不足

## 使用國

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
### 前使用國

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 登場作品

### [電影](../Page/電影.md "wikilink")

  - 廣泛地出現在[蘇聯電影](../Page/蘇聯電影.md "wikilink")
  - 2001年—《[-{zh-cn:兵临城下; zh-hk:敵對邊緣;
    zh-tw:大敵當前;}-](../Page/大敵當前.md "wikilink")》（Enemy At
    The
    Gates）：型號為TT-33，被[蘇聯紅軍士兵和政治委員所使用](../Page/蘇聯紅軍.md "wikilink")，當中[尼基塔·謝爾蓋耶維奇·赫魯雪夫](../Page/尼基塔·謝爾蓋耶維奇·赫魯雪夫.md "wikilink")（[鮑伯·霍金斯飾演](../Page/鮑伯·霍金斯.md "wikilink")）曾把他的配槍留下給一名蘇聯紅軍軍官用以[自殺謝罪](../Page/自殺.md "wikilink")。
  - 2001年—《[-{zh-cn:黑鹰坠落; zh-hk:黑鷹十五小時;
    zh-tw:黑鷹計劃;}-](../Page/黑鷹計劃.md "wikilink")》（Black
    Hawk
    Down）：型號為TT-33，被一名[索馬里](../Page/索馬里.md "wikilink")[民兵射殺蘭迪](../Page/民兵.md "wikilink")‧舒嘉特三等士官長時所使用。
  - 2011年—《[登陸之日](../Page/登陸之日.md "wikilink")》（My
    Way）：型號為TT-33，被蘇聯紅軍、監獄守衛和政治委員所使用，其中一枝亦被長谷川辰雄大佐（[小田切讓飾演](../Page/小田切讓.md "wikilink")）用以射殺撤退的日軍士兵。
  - 2013年—《[-{zh-cn:斯大林格勒; zh-hk:史太林格勒保衛戰;
    zh-tw:史達林格勒;}-](../Page/斯大林格勒_\(2013年電影\).md "wikilink")》（Сталинград）：型號為TT-33，被格羅莫夫上尉（彼得·費奧多羅夫飾演）和其他蘇聯紅軍士兵所使用。
  - 2015年—《[-{zh-cn:王牌特工：特工学院; zh-hk:皇家特工：間諜密令;
    zh-tw:金牌特務;}-](../Page/皇家特工：間諜密令.md "wikilink")》（Kingsman:
    The Secret Service）：電影中「金士曼」特工們所使用的「金士曼手槍」（Kingsman
    Pistol）是以TT-30改裝而成，並把彈匣容量增加至10發，槍管下亦裝上了一套特殊的[霰彈槍套件](../Page/霰彈槍.md "wikilink")。
  - 2016年—《[-{zh-hans:硬核大战; zh-hk:爆機特攻;
    zh-tw:超狂亨利;}-](../Page/硬核大戰.md "wikilink")》（Hardcore
    Henry）：型號為TT-33，會裝上[消音器](../Page/抑制器.md "wikilink")，被亨利所使用。
  - 2017年—《[-{zh-cn:王牌特工2：黄金圈; zh-hk:皇家特工：金圈子;
    zh-tw:金牌特務：機密對決;}-](../Page/金牌特務：機密對決.md "wikilink")》（Kingsman:
    The Golden
    Circle）：為[上一部電影的](../Page/皇家特工：間諜密令.md "wikilink")「金士曼手槍」（Kingsman
    Pistol），被蓋瑞·「伊格西」·安文（[泰隆·艾格頓飾演](../Page/泰隆·艾格頓.md "wikilink")）和哈利·哈特（[柯林·佛斯飾演](../Page/柯林·佛斯.md "wikilink")）所使用。
  - 2017年ㄧ《[-{zh-hans:极寒之城; zh-hk:原子殺姬;
    zh-tw:極凍之城;}-](../Page/極凍之城.md "wikilink")》：型號為TT-33，被[英國](../Page/英國.md "wikilink")[秘密情報局特工蘿琳](../Page/秘密情報局.md "wikilink")·布勞頓（[莎莉·賽隆飾演](../Page/莎莉·賽隆.md "wikilink")）與部分[蘇聯國家安全委員會特工所使用](../Page/蘇聯國家安全委員會.md "wikilink")。

### [電視劇](../Page/電視劇.md "wikilink")

  - 廣泛地出現在俄羅斯的電視劇
  - 《[-{zh-hans:反击; zh-hk:絕地反擊;
    zh-tw:勇者逆襲;}-](../Page/勇者逆襲.md "wikilink")》系列
      - [第一季](../Page/勇者逆襲_\(第一季\).md "wikilink")（Strike Back: Project
        Dawn）：型號為TT-33，裝上[消聲器](../Page/抑制器.md "wikilink")，在第7集被[科索沃](../Page/科索沃.md "wikilink")[恐怖份子綁架歐盟代表團時所使用](../Page/恐怖份子.md "wikilink")。
      - [第二季](../Page/勇者逆襲_\(第二季\).md "wikilink")（Strike Back: Shadow
        Warfare）：型號為TT-33，被[俄羅斯黑手黨首領阿爾卡季](../Page/俄羅斯黑手黨.md "wikilink")·烏里揚諾夫利及亞姆·巴克斯特中士的綁架者所使用。
      - [第四季](../Page/勇者逆襲_\(第四季\).md "wikilink")（Strike Back:
        Legacy）：型號為TT-33，被[朝鮮人民軍士兵](../Page/朝鮮人民軍.md "wikilink")、[朝鮮勞動黨黨員](../Page/朝鮮勞動黨.md "wikilink")、麗娜和俄羅斯黑手黨成員所使用，亦曾被達米思·斯科特中士（[蘇利文·斯坦布萊頓飾演](../Page/蘇利文·斯坦布萊頓.md "wikilink")）和[聯邦安全局特工納迪亞](../Page/聯邦安全局.md "wikilink")·丹斯基所繳獲。

### 電子遊戲

  - 1997年—《[黃金眼007](../Page/黃金眼007.md "wikilink")》（GoldenEye 007）
  - 2003年—《》（Vietcong）：型號為TT-33，被[越南人民軍及](../Page/越南人民軍.md "wikilink")[越南南方民族解放陣線所使用](../Page/越南南方民族解放陣線.md "wikilink")。
  - 2004年—《[-{zh-cn:使命召唤：联合进攻;
    zh-tw:決勝時刻：聯合行動;}-](../Page/決勝時刻：聯合行動.md "wikilink")》（Call
    of Duty: United Offensive）：型號為TT-33，為蘇聯紅軍制式手槍。
  - 2004年—《[-{zh-cn:使命召唤2;
    zh-tw:決勝時刻2;}-](../Page/決勝時刻2.md "wikilink")》（Call
    of Duty 2）：型號為TT-33，為蘇聯紅軍制式手槍。
  - 2005年—《[真實計劃](../Page/真實計劃.md "wikilink")》（Project
    Reality）：型號為TT-33，為[塔利班](../Page/塔利班.md "wikilink")、[哈瑪斯](../Page/哈瑪斯.md "wikilink")、伊拉克叛軍、[敘利亞自由軍](../Page/敘利亞自由軍.md "wikilink")、非洲抵抗軍和民兵陣營專用手槍。
  - 2007年—《[穿越火线](../Page/穿越火线.md "wikilink")》（Crossfire）：型号为TT-33，最早由中国大陆服务器于2017年9月9日推出，命名为“TT-33”，8发弹匣，只能在2017年9月9日\~19日期间在活动页面领取永久。
  - 2008年—《[-{zh-cn:使命召唤：战争世界;
    zh-tw:決勝時刻：戰爭世界;}-](../Page/決勝時刻：戰爭世界.md "wikilink")》（Call
    of Duty: World at
    War）：型號為TT-33，為蘇聯紅軍制式手槍並被迪米特里·彼得連科列兵所使用。聯機模式時可被所有陣營使用。
  - 2010年—《[-{zh-cn:使命召唤：黑色行动;
    zh-tw:決勝時刻：黑色行動;}-](../Page/決勝時刻：黑色行動.md "wikilink")》（Call
    of Duty: Black Ops）：型號為TT-33，於「Project
    Nova」關卡當中被蘇聯紅軍士兵維克多·雷澤諾夫、列夫·克拉夫錢科及[英國突擊隊所使用](../Page/英國突擊隊.md "wikilink")。
  - 2010年—《[-{zh-hans:战地：叛逆连队2;
    zh-hant:戰地風雲：惡名昭彰2;}-](../Page/战地：叛逆连队.md "wikilink")》（Battlefield:
    Bad Company 2）：只於越戰資料片中登場，命名為“TT-33”。
  - 2014年—《合同戰爭》（Contract Wars）
  - 2016年—《[英雄與將軍](../Page/英雄與將軍.md "wikilink")》（Heroes and Generals）
  - 2016年—《[女神異聞錄5](../Page/女神異聞錄5.md "wikilink")》(Persona 5)
    ：主人公Joker在異世界中默認的遠程武器就是TT手槍（托卡列夫）。
  - 2016年—《[少女前線](../Page/少女前線.md "wikilink")》（Girls'
    Frontline）：以托卡列夫的名稱登場。
  - 2017年—《Rising Storm 2: Vietnam》：為越共陣營使用的手槍。

### 動畫

  - 2006年—《[BLOOD+](../Page/BLOOD+.md "wikilink")》：型號為TT-33。
  - 2006年—《[暮蟬悲鳴時](../Page/暮蟬悲鳴時_\(動畫\).md "wikilink")》：型號為TT-33。
  - 2006年—《[-{zh-cn:黑礁;
    zh-tw:企業傭兵;}-](../Page/企業傭兵.md "wikilink")》（BLACK
    LAGOON）：型號為TT-33，被莫斯科酒店成員、[真主黨成員伊布拉哈等多個陣營和角色所使用](../Page/真主黨.md "wikilink")，也被萊薇在日本活動期間短暫地用作配槍。
  - 2007年—《[DARKER THAN BLACK
    -黑之契約者-](../Page/DARKER_THAN_BLACK.md "wikilink")》：型號為TT-33，被黑手黨成員所使用。
  - 2009年—《[Phantom -PHANTOM OF
    INFERNO-](../Page/Phantom_-PHANTOM_OF_INFERNO-.md "wikilink")》：型號為TT-33，被洛梅羅、多個黑幫和中國黑手黨成員徐進發（可能是[54式手槍](../Page/54式手槍.md "wikilink")）所使用。
  - 2009年—《》（[英語](../Page/英語.md "wikilink")：First Squad: The Moment of
    Truth；[俄語](../Page/俄語.md "wikilink")：Пе́рвый
    отря́д；[日語](../Page/日語.md "wikilink")：
    ファーストスクワッド）：型號為TT-33，白銀裝飾，出現在軍火庫中，後來被澤娜所使用。
  - 2011年—《[Fate/Zero](../Page/Fate/Zero.md "wikilink")》：型號為TT-33，被衛宮切嗣殺害其父親時所使用。

### [漫畫](../Page/漫畫.md "wikilink")

  - 2010年—《》：為TT-33，娜佳和部份[蘇聯紅軍士兵及](../Page/蘇聯紅軍.md "wikilink")[內務人民委員會委員的配槍](../Page/內務人民委員會.md "wikilink")。

## 資料來源

<div class="references-small">

<references />

  - [weaponplace.ru-TT](http://www.weaponplace.ru/tt.php)
  - [tokarev.com](http://www.tokarev.com/eng/)

</div>

## 注释

<div class="references-small">

<references group="注"/>

</div>

## 外部链接

  - \-[Modern
    Firearms-TT-30/33](https://web.archive.org/web/20080405183318/http://world.guns.ru/handguns/hg20-e.htm)

[Category:半自動手槍](../Category/半自動手槍.md "wikilink")
[Category:俄羅斯槍械](../Category/俄羅斯槍械.md "wikilink")
[Category:蘇聯槍械](../Category/蘇聯槍械.md "wikilink")
[Category:7.62×25毫米托卡列夫手槍彈槍械](../Category/7.62×25毫米托卡列夫手槍彈槍械.md "wikilink")
[Category:蘇聯二戰武器](../Category/蘇聯二戰武器.md "wikilink")
[Category:越戰武器](../Category/越戰武器.md "wikilink")
[Category:韓戰武器](../Category/韓戰武器.md "wikilink")

1.
2.
3.  TT手槍雖然無保險裝置，但可以先把打開到中間位置，即處於「半待擊狀態」便可以防止無意中扣動發射了。這個設計是大部分現代手槍都有的。