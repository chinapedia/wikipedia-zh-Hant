**TVB8**是[香港](../Page/香港.md "wikilink")[電視廣播(國際)有限公司擁有的一條以](../Page/電視廣播\(國際\)有限公司.md "wikilink")[國語廣播為主而經營的衛星頻道](../Page/國語.md "wikilink")，該頻道主要以每日播放娛樂、資訊、音樂及戲劇節目。

於1998年12月7日，**電視廣播(衛星)有限公司**成立，是唯一一個為進軍海外中文衛星電視市場，以國語廣播為主的**頻道**開播，該頻道及同屬的[星河頻道獲](../Page/TVB星河頻道.md "wikilink")[中華人民共和國國家廣播電影電視總局批准在中國大陸境内落地收看](../Page/国家广播电影电视总局.md "wikilink")。

**TVB8**同時正式在[亞洲](../Page/亞洲.md "wikilink")、[歐洲](../Page/歐洲.md "wikilink")、[非洲及](../Page/非洲.md "wikilink")[澳洲等地區播放](../Page/澳洲.md "wikilink")。

該頻道曾舉辦的活動有：[全球華人新秀歌唱大賽及](../Page/全球華人新秀歌唱大賽.md "wikilink")[TVB8金曲榜頒獎典禮等](../Page/TVB8金曲榜頒獎典禮.md "wikilink")。

該頻道已於2018年9月1日完全停播。因此本頻道廣播了約20年。

## 歷史與發展

  - 2006年9月8日，正式落地香港，觀眾可以透過[無綫收費電視及](../Page/無綫收費電視.md "wikilink")[now寬頻電視分別使用](../Page/now寬頻電視.md "wikilink")15頻道及815頻道收看。

<!-- end list -->

  - 2013年4月1日，[無綫網絡電視部分頻道號碼正式更改](../Page/無綫網絡電視.md "wikilink")，其中該頻道將使用原有的第15頻道更改為現有的第20頻道。

<!-- end list -->

  - 2017年6月1日，本頻道因[無綫網絡電視交還收費牌照而停播](../Page/無綫網絡電視.md "wikilink")，惟未安排[myTV
    SUPER繼續播放本頻道](../Page/myTV_SUPER.md "wikilink")，而[now
    TV及衛星電視客戶就不受影響](../Page/now_TV.md "wikilink")。於[無綫網絡電視停播前最後一個節目為](../Page/無綫網絡電視.md "wikilink")「[黃承暉謝敏瑜安息禮拜](../Page/黃承暉謝敏瑜安息禮拜.md "wikilink")」。

<!-- end list -->

  - 2018年7月，隨TVB裁減TVBI資源，[now寬頻電視將於](../Page/now寬頻電視.md "wikilink")8月15日正式停播此頻道，同時TVB
    Anywhere以及亞太6號衛星亦會於9月1日停播此頻道。

<!-- end list -->

  - 2018年8月15日0時0分，[now寬頻電視正式停播](../Page/now寬頻電視.md "wikilink")[TVB8](../Page/TVB8.md "wikilink")，於[now寬頻電視停播前最後一個節目為](../Page/now寬頻電視.md "wikilink")「[女婿們的戰爭](../Page/女婿們的戰爭.md "wikilink")\#49」。同时TVB
    Anywhere以及亞太6號衛星版本在播出画面左侧固定显示：“TVB8频道将于2018年9月1日零时零分起停止广播。感謝各位觀眾多年來對TVB8的支持”\[1\]

<!-- end list -->

  - 2018年9月1日，TVB8全面停播，TVB8官方網站於2018年7月31日停止服務，TVB8的亞太地區停播前最後一個節目為「隨意煮」，告别卡为“TVB8頻道已於2018年9月1日起停止廣播
    感謝各位觀眾多年來對TVB8的支持 祝 生活愉快！”畫面。而美國版則停播前最後一個節目為「文化中國\#1040」。

## 節目

以下所有節目均以亞太版本為準：

### 自製節目（由製作）

  - [娛樂最前線](../Page/娛樂最前線.md "wikilink")（與[TVB星河頻道同步](../Page/TVB星河頻道.md "wikilink")）（已停止同步播放）

  - 氣象報告（與[TVB星河頻道同步](../Page/TVB星河頻道.md "wikilink")）（已停止同步播放）

  - [超級樂壇全接觸](../Page/超級樂壇全接觸.md "wikilink")（已停止播放）

  - [超級樂壇推薦歌](../Page/超級樂壇推薦歌.md "wikilink")（已停止播放）

  - [TVB8金曲榜](../Page/TVB8金曲榜.md "wikilink")（已停止播放, 2018年1月2日为最后一期）

  - [情迷TVB戲劇之王](../Page/情迷TVB戲劇之王.md "wikilink")（已停止播放）

### 由香港無綫電視製播的節目

以下所有由[香港無綫電視製播的節目](../Page/香港無綫電視.md "wikilink")，除特別註明外，均已配上國語發音。

  - [水之源](../Page/水之源.md "wikilink")
  - [向世界出發](../Page/向世界出發.md "wikilink")
  - [同事三分親](../Page/同事三分親.md "wikilink")
  - [樂遊約克郡](../Page/樂遊約克郡.md "wikilink")
  - [甜姐兒](../Page/甜姐兒_\(電視節目\).md "wikilink")
  - [廉遊韓國](../Page/廉遊韓國.md "wikilink")
  - [吾湯吾水](../Page/吾湯吾水.md "wikilink")
  - [浪遊愛爾蘭](../Page/浪遊愛爾蘭.md "wikilink")
  - [回鄉](../Page/回鄉.md "wikilink")
  - [台灣闖蕩](../Page/台灣闖蕩.md "wikilink")
  - [廣州闖蕩](../Page/廣州闖蕩.md "wikilink")
  - [上海闖蕩](../Page/上海闖蕩.md "wikilink")
  - [最佳男主角](../Page/最佳男主角.md "wikilink")
  - [最佳女主角](../Page/最佳女主角.md "wikilink")

### 外購節目

<table style="width:40%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 30%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>節目名稱</p></th>
<th style="text-align: center;"><p>節目類型 / 備-{注}-</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/歡喜來逗陣.md" title="wikilink">歡喜來逗陣</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/型男大主廚.md" title="wikilink">型男大主廚</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/世界那麼大.md" title="wikilink">世界那麼大</a></p></td>
<td style="text-align: center;"><p><strong>旅遊節目</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/水果冰淇淋.md" title="wikilink">水果冰淇淋</a></p></td>
<td style="text-align: center;"><p><a href="../Page/公共電視文化事業基金會.md" title="wikilink">公共電視節目</a>，於2003年播出[2]</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/文化中國.md" title="wikilink">文化中國</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/薰衣草_(台灣偶像劇).md" title="wikilink">薰衣草</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/韓國勁歌傳真.md" title="wikilink">韓國音樂流行榜</a><br />
Pops in Seoul</p></td>
<td style="text-align: center;"><p><strong>音樂節目</strong><br />
本節目僅以英語原音廣播並提供繁體中文字幕</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/泰好聽•泰好玩.md" title="wikilink">泰好聽•泰好玩</a><br />
Sawasdee Station</p></td>
<td style="text-align: center;"><p><strong>音樂節目</strong><br />
本節目僅以英語原音廣播並提供簡體中文字幕</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/魔女18號.md" title="wikilink">魔女18號</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/1001個真相.md" title="wikilink">1001個真相</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/金太狼的幸福生活.md" title="wikilink">金太狼的幸福生活</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/左右時尚.md" title="wikilink">左右時尚</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/愛情兩好三壞.md" title="wikilink">愛情兩好三壞</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/愛玩客.md" title="wikilink">愛玩客</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/我要上春晚.md" title="wikilink">我要上春晚</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/你是我的命運_(電視劇).md" title="wikilink">你是我的命運</a></p></td>
<td style="text-align: center;"><p><a href="../Page/亞洲電視.md" title="wikilink">亞洲電視率先首播</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/寶石拌飯.md" title="wikilink">寶石拌飯</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/風吹好日子.md" title="wikilink">風吹好日子</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/逆轉女王.md" title="wikilink">逆轉女王</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/過得有滋有味.md" title="wikilink">享受人生</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>創藝.遊</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>浩克慢遊</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/惡作劇之吻_(2016年電視劇).md" title="wikilink">惡作劇之吻</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>帶你去吃風</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>小爸媽</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/为她而战.md" title="wikilink">為她而戰</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>大貓兒追愛記</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/不得不爱.md" title="wikilink">不得不愛</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/I_do_I_do.md" title="wikilink">仙履奇緣</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>Go Go 捷運</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/愛玩客.md" title="wikilink">愛玩客之移動廚房</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>開心揹包遊</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>Go Go 玩樂王</p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

## 各地版本播放

****在兩個地區播放三個不同版本的節目，三個版本分別為亞太標清版、亞太高清版、美國版，曾在台灣播放[台灣版](../Page/TVB8_\(台灣版\).md "wikilink")。

亞太標清版除了在[香港透過](../Page/香港.md "wikilink")[無綫網絡電視](../Page/無綫網絡電視.md "wikilink")、[now寬頻電視收看之外](../Page/now寬頻電視.md "wikilink")，同時在[澳洲透過](../Page/澳洲.md "wikilink")[翡翠互動電視](../Page/翡翠互動電視.md "wikilink")、[中國大陸](../Page/中國大陸.md "wikilink")、[新加坡透過](../Page/新加坡.md "wikilink")[星和視界](../Page/新加坡電視頻道列表.md "wikilink")、[馬來西亞透過](../Page/馬來西亞.md "wikilink")[UniFi](../Page/w:en:UniFi.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[越南和](../Page/越南.md "wikilink")[澳門透過](../Page/澳門.md "wikilink")[澳門有線電視收看](../Page/澳門有線電視.md "wikilink")，該版本從啟播至2010年6月30日在[馬來西亞可透過](../Page/馬來西亞.md "wikilink")[Astro使用第](../Page/Astro.md "wikilink")313頻道（啟播初期曾使用第33頻道更改）收看，後可[馬來西亞透過](../Page/馬來西亞.md "wikilink")[UniFi](../Page/w:en:UniFi.md "wikilink")
IPTV在第154頻道(後為208和234頻道頻道)
收看，[台灣天外天有線電視曾在](../Page/台灣.md "wikilink")2001年上架亞太標清版\[3\]；亞太高清版目前只在亞太六號衛星上作試播，尚無任何地區落地；美國版從啟播至2012年11月18日在[美國曾透過](../Page/美國.md "wikilink")[DIRECTV使用第](../Page/DirecTV.md "wikilink")2058頻道收看，同日在[美國透過](../Page/美國.md "wikilink")[Dish
Network使用第](../Page/Dish_Network.md "wikilink")9938頻道收看\[4\]。台灣版從啟播之日起透過[中華電信MOD使用第](../Page/中華電信MOD.md "wikilink")40頻道收看。

亞太標清版的節目編排時間以[香港時間為準](../Page/香港時間.md "wikilink")，播放地區亦同時在[香港](../Page/香港.md "wikilink")、[澳門](../Page/澳門.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[中國大陸](../Page/中國大陸.md "wikilink")、[越南和](../Page/越南.md "wikilink")[澳洲收看](../Page/澳洲.md "wikilink")，該版本與美國版本所播放的節目內容不同，亞太版本除了部分時間播放自製節目之外，其餘時間播放[香港無綫電視所製播的旅遊](../Page/香港無綫電視.md "wikilink")、飲食、生活消閒、紀實和資訊節目及[處境劇集](../Page/處境劇集.md "wikilink")，以及外購[劇集及綜藝娛樂](../Page/劇集.md "wikilink")、旅遊、飲食、音樂、生活消閒、時尚、紀實和資訊節目。

目前正在試播的亞太高清版於2013年4月12日開始在[亞太6號衛星上斷斷續續的進行試播](../Page/亞太6號.md "wikilink")，具體的開播時間、播出方式以及覆蓋地區未知。此頻道由[翡翠東方](../Page/翡翠東方.md "wikilink")（）運營。

美國版的節目編排時間以[西岸為準](../Page/美國西岸.md "wikilink")，播放地區亦同時僅在美國收看。與亞太版本所播放的節目內容不同，美國版本除了部分時間播放自製節目之外，其餘時間播放[香港無綫電視所製播的](../Page/香港無綫電視.md "wikilink")[劇集及旅遊](../Page/劇集.md "wikilink")、飲食、生活消閒、紀實和資訊節目、以《明珠新聞》作為節目名稱而播放的[明珠台節目](../Page/明珠台.md "wikilink")《[七點半新聞報道](../Page/七點半新聞報道.md "wikilink")》(本節目僅以英語原音廣播並提供英文字幕)、由[myTV
SUPER製作而以國語廣播播放的](../Page/myTV_SUPER.md "wikilink")《[娛樂新聞報道](../Page/TVB娛樂新聞報道.md "wikilink")》以及由[神州新聞台所提供的中國大陸各地電視台新聞](../Page/神州新聞台.md "wikilink")（以《神州新聞》作為節目名稱）和節目《[文化中國](../Page/文化中國.md "wikilink")》，還有在美國當地製作而播放的《美國新聞》和《財經新聞》。

台灣版由[TVBS營運](../Page/TVBS.md "wikilink")，屬於联意制作股份有限公司（[TVBS](../Page/TVBS.md "wikilink")）的家族頻道之一，亦是TVBS的第一个高清綜合頻道。原定2013年6月17日開播，後延到2013年7月8日中午12點在[中華電信MOD第](../Page/中華電信MOD.md "wikilink")40頻道開播。該版本以播放TVBS自制的綜藝節目與[香港無綫電視的港劇為主](../Page/香港無綫電視.md "wikilink")，亦會少量播放來自香港TVB8的自制節目以及來自香港TVB的非劇集類節目。台灣版於2017年8月3日停播，並變更為[TVBS精采台](../Page/TVBS精采台.md "wikilink")。

## 參考資料

<references />

## 參見

  - [TVB8 (台灣版)](../Page/TVB8_\(台灣版\).md "wikilink")
  - [普通話翡翠台](../Page/普通话翡翠台.md "wikilink")
  - [TVB星河頻道](../Page/TVB星河頻道.md "wikilink")

## 外部連結

  - [電視廣播(美國)有限公司 -
    TVB(USA)](http://www.tvbusa.com/index.php?option=com_channel&Itemid=118&channel=J8)
  - [TVB8 - TVB I](http://b.tvb.com/tvbi/?p=2982&preview=true)
  - [舊標誌](http://imgur.com/KjFRLyZ)

[Category:1998年成立的电视台或电视频道](../Category/1998年成立的电视台或电视频道.md "wikilink")
[Category:電視廣播有限公司電視頻道](../Category/電視廣播有限公司電視頻道.md "wikilink")
[Category:香港已停播的電視頻道](../Category/香港已停播的電視頻道.md "wikilink")

1.
2.  [TVB8](https://web.archive.org/web/20030212104619/http://www.tvb.com/tvbi/channels/tvb8_p3_chi.htm)（存檔於[網際網路檔案館](../Page/網際網路檔案館.md "wikilink")）
3.  天外天有線電視90年7月頻道表()
4.  [2012年11月18日起，TVB節目將在DISH衛星平台繼續播放。](http://www.tvbusa.com/index.php?option=com_channel&Itemid=140&id=3686)
    電視廣播(美國)有限公司網站