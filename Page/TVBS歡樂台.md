**TVBS歡樂台**，是台灣TVBS旗下的娛樂節目頻道。

## 歷史

  - 1994年9月12日開播，原名「**TVBS-G
    黃金娛樂台**」為[TVBS四個家族頻道中](../Page/TVBS.md "wikilink")，第二個開播的頻道。
  - 2009年6月1日零時更名「**TVBS歡樂台**」，簡稱「**歡樂台**」。
  - 專門播放[音樂](../Page/音樂.md "wikilink")、流行、戲劇、娛樂、體育等精選內容，尤以播放母公司[香港無綫電視（TVB）的](../Page/電視廣播有限公司.md "wikilink")[港劇著名](../Page/港劇.md "wikilink")（SD訊號為國語配音，HD訊號為國粵雙語）。除外購節目也製作不少青春偶像劇。
  - 最早TVBS節目部，在「[八德大樓](../Page/TVBS企業總部.md "wikilink")」內。
  - 2000年遷入「[南港大樓](../Page/TVBS企業總部.md "wikilink")」一樓。
  - 2009年同TVBS部門集中「[內湖大樓](../Page/TVBS企業總部.md "wikilink")」，目前位於「[內湖大樓](../Page/TVBS企業總部.md "wikilink")」四樓。
  - 2014年2月27日起，TVBS歡樂台的HD高畫質版本開始播放，並陸續在各大有線電視系統上架。
  - 2014年9月29日起，同家族頻道[TVBS新聞台](../Page/TVBS新聞台.md "wikilink")、[TVBS一樣](../Page/TVBS_\(頻道\).md "wikilink")，TVBS歡樂台的標清版本更改視頻比例為16:9。
  - 2016年12月21日，更換全新台標。\[1\]

## TVBS節目部人員

  - 邵正祥（總監）
  - 唐千代（副理）

## 參考資料

## 外部連結

  - [TVBS官方網站](http://www.tvbs.com.tw/)

[Category:聯利媒體電視頻道](../Category/聯利媒體電視頻道.md "wikilink")
[Category:台灣電視播放頻道](../Category/台灣電視播放頻道.md "wikilink")

1.  [TVBS新風貌！　全新logo亮相「新舊媒體融合」](http://news.tvbs.com.tw/news/detail/inter-news/694839).TVBS新聞網.2016-12-21