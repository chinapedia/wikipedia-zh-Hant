**TVB大富**（）是[香港](../Page/香港.md "wikilink")[電視廣播(國際)有限公司與](../Page/電視廣播\(國際\)有限公司.md "wikilink")[日本](../Page/日本.md "wikilink")[大富株式會社合作開辦而擁有的一條綜合娛樂頻道](../Page/大富株式會社.md "wikilink")。[電視廣播有限公司並非直接經營此頻道](../Page/電視廣播有限公司.md "wikilink")，而是[授權大富株式会社設立](../Page/授權.md "wikilink")。

## 記事

  - 因日本政府规定，境外电视频道在日本有线电视不得直接落地，需要与日本的商营有线电视台合作控股\[1\]。1998年2月，大仓商事與[富士电视台合资成立大富株式会社](../Page/富士电视台.md "wikilink")，由张丽玲上任董事局主席。

<!-- end list -->

  - 2002年1月，香港[电视广播（国际）有限公司与大富株式会社合作](../Page/电视广播（国际）有限公司.md "wikilink")，将TVB主力频道以及台湾[TVBS的代表性娱乐节目整合在一起](../Page/TVBS.md "wikilink")，在日本落地播出，落地后的频道名定为「[TVB大富](../Page/TVB大富.md "wikilink")」，节目播出语种为普通话和粤语相糅合，播出内容以[TVB8](../Page/TVB8.md "wikilink")、[TVBS](../Page/TVBS.md "wikilink")、[翡翠台和](../Page/翡翠台.md "wikilink")[明珠台节目为主](../Page/明珠台.md "wikilink")。如果原节目是粤语发音且拥有普通话配音声道，会替换成普通话配音。该频道同时亦有重播大富公司自办的在[CCTV大富首播的当天的](../Page/CCTV大富.md "wikilink")《日本新闻》节目。

<!-- end list -->

  - 2011年9月30日，香港电视广播（国际）有限公司与大富公司合约终止，TVB大富停播。

## 節目

TVB大富主要播放[香港無綫電視和](../Page/香港無綫電視.md "wikilink")[台灣](../Page/台灣.md "wikilink")[TVBS旗下各頻道的](../Page/TVBS.md "wikilink")[娛樂](../Page/娛樂.md "wikilink")[新聞](../Page/新聞.md "wikilink")、[音樂節目和](../Page/音樂.md "wikilink")[電視劇等不同類型的節目](../Page/電視劇.md "wikilink")。

## 參考資料

  - [香港与日本合开华语电视](http://news.bbc.co.uk/chinese/trad/hi/newsid_1660000/newsid_1665700/1665780.stm)
    2001年11月20日，BBC中文网

## 外部連結

  - [大富株式会社](http://www.cctvdf.com/c/)
  - [無綫電視相關網頁(TVBI) - TVB大富](http://b.tvb.com/tvbi/?p=1229&preview=true)

[日](../Category/電視廣播有限公司電視頻道.md "wikilink")
[Category:2001年成立的電視台或電視頻道](../Category/2001年成立的電視台或電視頻道.md "wikilink")
[Category:日本衛星電視頻道](../Category/日本衛星電視頻道.md "wikilink")

1.  如[CNN同样在日本打造](../Page/CNN.md "wikilink")[CNNj频道](../Page/CNNj.md "wikilink")，英语和日语双语播出；韩国[KBS
    World在日本播放版本台标呈毛玻璃透明状](../Page/KBS_World.md "wikilink")，字幕亦是日语的。而日本的华侨华人若使用长城平台卫星，依然照常看到原版CCTV-4亚洲版节目及广告。