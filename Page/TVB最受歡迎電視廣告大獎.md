**《TVB最受歡迎電視廣告大獎》**（[英文](../Page/英文.md "wikilink")：），由[電視廣播有限公司主辦](../Page/電視廣播有限公司.md "wikilink")，首屆於2007年舉行，當時名稱為**《TVB最受歡迎廣告頒獎典禮》**，2008年起改為現在的名稱。頒獎禮在[翡翠台及](../Page/翡翠台.md "wikilink")[高清翡翠台](../Page/高清翡翠台.md "wikilink")（2008年至2015年）播出，在[電視廣播城舉行](../Page/電視廣播城.md "wikilink")(除2011、2015年外)，2016年起，因應[高清翡翠台改名為](../Page/高清翡翠台.md "wikilink")[J5](../Page/J5.md "wikilink")，頒獎禮改為僅於[翡翠台播出](../Page/翡翠台.md "wikilink")。這是香港地區第三個專為廣告而設的頒獎禮，2017年開始停辦。

## 投票

首先評判團會在數百個[廣告中因應性質選出每個界別五個候選名單](../Page/廣告.md "wikilink")，再由[香港市民以網上投票或郵寄選票選出得出獲獎單位](../Page/香港.md "wikilink")，最後經[黃龍德會計師事務所點算及核實結果](../Page/黃龍德會計師事務所.md "wikilink")。

## 播出、司儀及演出

<table style="width:95%;">
<colgroup>
<col style="width: 15%" />
<col style="width: 20%" />
<col style="width: 30%" />
<col style="width: 30%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>播出</p></th>
<th><p>司儀</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2007（第一屆）</p></td>
<td><p>8月5日晚上8時30分</p></td>
<td><p><a href="../Page/鄭丹瑞.md" title="wikilink">鄭丹瑞</a>、<a href="../Page/鄧健泓.md" title="wikilink">鄧健泓</a></p></td>
<td><p>口號為「全港首創·100%全民直選」、「300%創意破格，TVB最受歡迎廣告即將誕生。」<br />
<a href="../Page/王祖藍.md" title="wikilink">王祖藍主持</a>《TVB最受歡迎廣告頒獎典禮2007 前奏》</p></td>
</tr>
<tr class="even">
<td><p>2008（第二屆）</p></td>
<td><p>7月26日晚上8時30分</p></td>
<td><p>鄭丹瑞、<a href="../Page/李浩林.md" title="wikilink">李浩林</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009（第三屆）</p></td>
<td><p>4月25日晚上9時</p></td>
<td><p><a href="../Page/李思捷.md" title="wikilink">李思捷</a>、<a href="../Page/錢嘉樂.md" title="wikilink">錢嘉樂</a>、<a href="../Page/周汶錡.md" title="wikilink">周汶錡</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010（第四屆）</p></td>
<td><p>5月15日晚上9時30分</p></td>
<td><p><a href="../Page/曾志偉.md" title="wikilink">曾志偉</a>、<a href="../Page/鄧梓峰.md" title="wikilink">鄧梓峰</a>、<a href="../Page/丘凱敏.md" title="wikilink">丘凱敏</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011（第五屆）</p></td>
<td><p>7月31日晚上22:00-23:30</p></td>
<td><p><a href="../Page/森美.md" title="wikilink">森美</a>、<a href="../Page/陳庭欣.md" title="wikilink">陳庭欣</a></p></td>
<td><p>7月21日晚上假<a href="../Page/九龍香格里拉酒店.md" title="wikilink">九龍香格里拉酒店B</a>1粉嶺廳舉行，名為《魔法擂台-2011 TVB最受歡迎廣告頒獎典禮》</p></td>
</tr>
<tr class="even">
<td><p>2012（第六屆）</p></td>
<td><p>6月30日晚上9時55分</p></td>
<td><p><a href="../Page/麥長青.md" title="wikilink">麥長青</a>、陳庭欣、<a href="../Page/金剛_(藝人).md" title="wikilink">金　剛</a>、<a href="../Page/呂慧儀.md" title="wikilink">呂慧儀</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013（第七屆）</p></td>
<td><p>6月22日晚上9時00分</p></td>
<td><p>鄭丹瑞、<a href="../Page/林盛斌.md" title="wikilink">林盛斌</a>、<a href="../Page/黃山怡.md" title="wikilink">黃山怡</a></p></td>
<td><p>演出：<a href="../Page/C_AllStar.md" title="wikilink">C AllStar</a>、<a href="../Page/Mr..md" title="wikilink">Mr.</a>、<a href="../Page/梁漢文.md" title="wikilink">梁漢文</a></p></td>
</tr>
<tr class="even">
<td><p>2014（第八屆）</p></td>
<td><p>6月28日晚上8時30分</p></td>
<td><p><a href="../Page/曾華倩.md" title="wikilink">曾華倩</a>、<a href="../Page/崔建邦.md" title="wikilink">崔建邦</a>、<a href="../Page/王貽興.md" title="wikilink">王貽興</a></p></td>
<td><p>嘉賓：<a href="../Page/王敏奕.md" title="wikilink">王敏奕</a>、<a href="../Page/王梓軒.md" title="wikilink">王梓軒</a>、<a href="../Page/江欣燕.md" title="wikilink">江欣燕</a>、<a href="../Page/羽翹.md" title="wikilink">羽翹</a>、<a href="../Page/吳若希.md" title="wikilink">吳若希</a>、<a href="../Page/吳嘉熙.md" title="wikilink">吳嘉熙</a>、<a href="../Page/吳燕菁.md" title="wikilink">吳燕菁</a>、<a href="../Page/李克勤.md" title="wikilink">李克勤</a>、<a href="../Page/狄易達.md" title="wikilink">狄易達</a>、<a href="../Page/周志文.md" title="wikilink">周志文</a>、<a href="../Page/周志康.md" title="wikilink">周志康</a>、<a href="../Page/張慧儀.md" title="wikilink">張慧儀</a>、<a href="../Page/陳嘉寶.md" title="wikilink">陳嘉寶</a>、<a href="../Page/彭永琛.md" title="wikilink">彭永琛</a>、<a href="../Page/蔚雨芯.md" title="wikilink">蔚雨芯</a>、<a href="../Page/鄭世豪.md" title="wikilink">鄭世豪</a>、<a href="../Page/賴慰玲.md" title="wikilink">賴慰玲</a><br />
旁白：<a href="../Page/陳廷軒.md" title="wikilink">陳廷軒</a></p></td>
</tr>
<tr class="odd">
<td><p>2015（第九屆）</p></td>
<td><p>7月4日晚上8時30分</p></td>
<td><p><a href="../Page/黃智賢_(香港).md" title="wikilink">黃智賢</a>、<a href="../Page/姚子羚.md" title="wikilink">姚子羚</a>、<a href="../Page/陳凱琳.md" title="wikilink">陳凱琳</a></p></td>
<td><p>6月26日晚上7時30分假<a href="../Page/九龍東皇冠假日酒店.md" title="wikilink">九龍東皇冠假日酒店一樓宴會廳舉行</a><br />
頒獎禮後為《實至名歸好廣告》，主持為<a href="../Page/宋熙年.md" title="wikilink">宋熙年</a>、<a href="../Page/鄭敬基.md" title="wikilink">鄭敬基</a><br />
演出：<a href="../Page/阮兆祥.md" title="wikilink">阮兆祥</a>、<a href="../Page/謝安琪.md" title="wikilink">謝安琪</a>、<a href="../Page/蔣志光.md" title="wikilink">蔣志光</a>、<a href="../Page/鄭俊弘.md" title="wikilink">鄭俊弘</a>、<a href="../Page/劉雅麗.md" title="wikilink">劉雅麗</a>、<a href="../Page/韋綺姍.md" title="wikilink">韋綺姍</a>、<a href="../Page/C_All_Star.md" title="wikilink">C All Star</a>、<a href="../Page/梁烈唯.md" title="wikilink">梁烈唯</a><br />
旁白：<a href="../Page/陳廷軒.md" title="wikilink">陳廷軒</a></p></td>
</tr>
<tr class="even">
<td><p>2016（第十屆）</p></td>
<td><p>7月23日晚上8時30分</p></td>
<td><p><a href="../Page/陸浩明.md" title="wikilink">陸浩明</a>、<a href="../Page/區永權.md" title="wikilink">區永權</a>、<a href="../Page/黃心穎.md" title="wikilink">黃心穎</a>、<a href="../Page/麥美恩.md" title="wikilink">麥美恩</a>、<a href="../Page/朱智賢.md" title="wikilink">朱智賢</a></p></td>
<td><p>同時在場的廣告商亦會參與<a href="../Page/香港小姐.md" title="wikilink">香港小姐的投票</a><br />
頒獎禮後為《全城Like爆好廣告》，主持為<a href="../Page/張曦雯.md" title="wikilink">張曦雯</a>、<a href="../Page/莫家淦.md" title="wikilink">莫家淦</a><br />
演出：<a href="../Page/何雁詩.md" title="wikilink">何雁詩</a>、<a href="../Page/農夫.md" title="wikilink">農夫</a>、<a href="../Page/狄易達.md" title="wikilink">狄易達</a>、<a href="../Page/甄澤權.md" title="wikilink">甄澤權</a></p></td>
</tr>
</tbody>
</table>

## 得獎名單

分為多個界別獎項，另外設有個人獎項及大獎，分別是：

### 2007年

| 獎項              | 獲奬                                                                                                 |
| --------------- | -------------------------------------------------------------------------------------------------- |
| 最受歡迎食品及餐飲廣告     | [果汁先生](../Page/果汁先生.md "wikilink")-先生話篇                                                            |
| 最受歡迎潮流產品及零售廣告   | [愛迪達](../Page/愛迪達.md "wikilink")[碧咸Impossible](../Page/碧咸.md "wikilink") is Nothing故事              |
| 最受歡迎美客產品及服務廣告   | [OTO](../Page/OTO.md "wikilink") FLABeLOS 多功能搖擺健身機                                                 |
| 最受歡迎金融及投資廣告     | [安信兄弟情義篇](../Page/安信.md "wikilink")                                                                |
| 最受歡迎地產廣告        | [嘉亨灣](../Page/嘉亨灣.md "wikilink")-生活代表作                                                             |
| 最受歡迎家居用品廣告      | [TEMPO](../Page/TEMPO.md "wikilink") 當堂醒晒                                                          |
| 最受歡迎生活及消閒廣告     | [海洋公園水母萬花筒](../Page/海洋公園.md "wikilink")                                                            |
| 最受歡迎通訊運輸及公共設施廣告 | [國泰航空手足情深](../Page/國泰航空.md "wikilink")                                                             |
| 最受歡迎綜合類別廣告      | [得果定](../Page/得果定.md "wikilink") COOL唱山歌                                                           |
| 最受歡迎公益廣告        | [康樂及文化事務署](../Page/康樂及文化事務署.md "wikilink")：有豬腩肉游多幾游                                                |
| 最受歡迎資訊系列        | [高鈣健](../Page/高鈣健.md "wikilink")-醫神教你呢件事                                                           |
| 最受歡迎男主角         | [得果定](../Page/得果定.md "wikilink") COOL唱山歌 ——[陳奕迅](../Page/陳奕迅.md "wikilink")                        |
| 最受歡迎女主角         | [護舒寶超柔軟瞬潔](../Page/護舒寶.md "wikilink") ——[林嘉欣](../Page/林嘉欣.md "wikilink")                           |
| 最受歡迎電視廣告歌曲      | [麥當勞](../Page/麥當勞.md "wikilink")24小時營業 ——為午夜喝采（[陳奕迅](../Page/陳奕迅.md "wikilink")）                   |
| 最受歡迎動物明星        | [海洋公園聖誕熊貓篇](../Page/海洋公園.md "wikilink")                                                            |
| 最受歡迎童星          | [果汁先生](../Page/果汁先生.md "wikilink")-先生話篇——[蘇文峰](../Page/蘇文峰.md "wikilink")                          |
| 電視廣告創意投放獎       | [OSIM](../Page/OSIM.md "wikilink")[香港](../Page/香港.md "wikilink")[有限公司](../Page/有限公司.md "wikilink") |
| 榮譽大獎            | [嘉亨灣](../Page/嘉亨灣.md "wikilink")-生活代表作                                                             |

### 2008年

| 獎項                  | 獲奬                                                                                   |
| ------------------- | ------------------------------------------------------------------------------------ |
| 最受歡迎電視廣告－飲食餐宴       | [益力多](../Page/益力多.md "wikilink")-重逢篇                                                 |
| 最受歡迎電視廣告－潮流服飾及美容    | [Rejoice](../Page/Rejoice.md "wikilink")-出奇順滑絲綢篇                                     |
| 最受歡迎電視廣告－財富投資       | [EPS](../Page/EPS.md "wikilink") Easy Cash-軟硬合拍篇                                     |
| 最受歡迎電視廣告－樓宇地產       | [星匯居](../Page/星匯居.md "wikilink")-回家篇                                                 |
| 最受歡迎電視廣告－家品生活       | [Tempo](../Page/Tempo.md "wikilink")-婚外情篇                                            |
| 最受歡迎電視廣告－消閒、時尚及品味   | [海洋公園](../Page/香港海洋公園.md "wikilink")[十月全城哈囉喂](../Page/萬聖夜.md "wikilink")-鬼叫你篇        |
| 最受歡迎電視廣告－電訊、運輸及公共設施 | [聯邦快遞](../Page/聯邦快遞.md "wikilink")-中國方言篇                                             |
| 最受歡迎電視廣告－綜合類別       | 1-Day ACUVUE 散光鏡-認錯男朋友篇                                                              |
| 最受歡迎電視廣告－資訊系列       | [九巴](../Page/九龍巴士.md "wikilink")-乘客之道                                                |
| 最受歡迎電視廣告－系列廣告       | [香港寬頻](../Page/香港寬頻.md "wikilink")-擠塞篇/貝爾篇/蟻哥篇                                       |
| 最受歡迎男主角－明星及名人       | [道地](../Page/道地.md "wikilink")-極品煎茶-[劉德華](../Page/劉德華.md "wikilink")                 |
| 最受歡迎男主角－廣告演員        | [麥當勞](../Page/麥當勞.md "wikilink")-員工篇-[黃文禹](../Page/黃文禹.md "wikilink")                |
| 最受歡迎女主角－明星及名人       | [麥當勞Shake](../Page/麥當勞.md "wikilink") Shake 薯條-[容祖兒](../Page/容祖兒.md "wikilink")      |
| 最受歡迎女主角－廣告演員        | 1-Day ACUVUE 散光鏡 認錯男朋友篇-[李慧思](../Page/李慧思.md "wikilink")                             |
| 最受歡迎電視廣告歌曲          | [香港鐵路有限公司](../Page/香港鐵路有限公司.md "wikilink") [兩鐵合併](../Page/兩鐵合併.md "wikilink") 一路帶動生活 |
| 最受歡迎電視廣告標語          | [麥當勞](../Page/麥當勞.md "wikilink") 這一刻更加親「麥」I'm lovin it                               |
| 726票王大獎             | [海洋公園](../Page/香港海洋公園.md "wikilink")[十月全城哈囉喂](../Page/萬聖夜.md "wikilink")-鬼叫你篇        |
| 電視廣告創意投放獎           | [雅居樂](../Page/雅居樂.md "wikilink")                                                     |
| 最具創意大獎              | [香港寬頻](../Page/香港寬頻.md "wikilink")-擠塞篇/貝爾篇/蟻哥篇                                       |
| 榮譽大獎                | [益力多](../Page/益力多.md "wikilink")-重逢篇                                                 |

### 2009年

| 獎項                  | 獲奬                                                                             |
| ------------------- | ------------------------------------------------------------------------------ |
| 最受歡迎電視廣告－飲食餐宴       | [可口可樂](../Page/可口可樂.md "wikilink") - 奧運鳥巢                                      |
| 最受歡迎電視廣告－潮流服飾及美容    | [Rejoice飄柔](../Page/Rejoice.md "wikilink") - 巴士篇                               |
| 最受歡迎電視廣告－財富投資       | [VISA](../Page/VISA.md "wikilink") - 逍遙遊世界                                     |
| 最受歡迎電視廣告－樓宇地產       | [中原地產](../Page/中原地產.md "wikilink") - 憑創見，走到更前                                  |
| 最受歡迎電視廣告－家品生活       | [Tempo](../Page/Tempo.md "wikilink") - 洗衣機篇                                    |
| 最受歡迎電視廣告－消閒、時尚及品味   | [海洋公園](../Page/香港海洋公園.md "wikilink")[十月全城哈囉喂](../Page/萬聖夜.md "wikilink") 揀性電梯篇 |
| 最受歡迎電視廣告－電訊、運輸及公共設施 | [FedEx](../Page/FedEx.md "wikilink") -靈犬篇                                      |
| 最受歡迎電視廣告－綜合類別       | 香港吸煙與健康委員會-「贏」篇                                                                |
| 最受歡迎男主角             | [Tempo](../Page/Tempo.md "wikilink") - 洗衣機篇 [蔡瀚億](../Page/蔡瀚億.md "wikilink")   |
| 最受歡迎女主角             | [惠康超級市場](../Page/惠康.md "wikilink") - 父女篇 [林卓恩](../Page/林卓恩.md "wikilink")      |
| 最受歡迎電視廣告歌曲          | 道地極品無糖系列-白茶篇／紅烏龍篇（一晚長大）                                                        |
| 最佳導演                | [Tempo](../Page/Tempo.md "wikilink") - 洗衣機篇 [陳敏聰](../Page/陳敏聰.md "wikilink")   |
| 最具創意大獎              | [惠康超級市場](../Page/惠康.md "wikilink") - 父女篇                                       |
| 榮譽大獎                | [可口可樂](../Page/可口可樂.md "wikilink") - 奧運鳥巢                                      |

### 2010年

| 獎項                  | 獲奬                                                                                                                  |
| ------------------- | ------------------------------------------------------------------------------------------------------------------- |
| 最受歡迎電視廣告－飲食餐宴       | [維他奶](../Page/維他奶.md "wikilink") - Stand By Me                                                                      |
| 最受歡迎電視廣告－潮流服飾及美容    | [REJOICE](../Page/REJOICE.md "wikilink") - 柔順新婚篇                                                                    |
| 最受歡迎電視廣告－財富投資       | [中國建設銀行（亞洲）](../Page/中國建設銀行（亞洲）.md "wikilink") - 回家篇                                                                |
| 最受歡迎電視廣告－樓宇地產       | [領都](../Page/領都.md "wikilink") - BB熊篇                                                                               |
| 最受歡迎電視廣告－家品生活       | [Tempo盒裝紙巾](../Page/Tempo.md "wikilink") - 1 Take 搞掂晒                                                               |
| 最受歡迎電視廣告－消閒、時尚及品味   | [海洋公園](../Page/海洋公園.md "wikilink") - 亞洲動物天地                                                                         |
| 最受歡迎電視廣告－電訊、運輸及公共設施 | [FedEx](../Page/FedEx.md "wikilink") - 層疊篇                                                                          |
| 最受歡迎電視廣告－綜合類別       | [萬寧](../Page/萬寧.md "wikilink") - 我有少少鍾意咗佢                                                                           |
| 最受歡迎男主角             | [Tempo盒裝紙巾](../Page/Tempo.md "wikilink") - 1 Take 搞掂晒（[Napat E-Thongchai](../Page/Napat_E-Thongchai.md "wikilink")） |
| 最受歡迎女主角             | [萬寧](../Page/萬寧.md "wikilink") - 我有少少鍾意咗佢（[王允祈](../Page/王允祈.md "wikilink")）                                         |
| 最受歡迎電視廣告歌曲          | [維他奶](../Page/維他奶.md "wikilink") - Stand By Me                                                                      |
| 最受歡迎電視廣告－資訊系列       | [九巴乘客之道](../Page/九巴.md "wikilink") - 自然好乘客                                                                          |
| 最受歡迎電視廣告－系列廣告       | [益力多四十週年](../Page/益力多.md "wikilink") - 闔家篇, 舞會篇, 士多篇                                                                |
| 榮譽大獎                | [萬寧](../Page/萬寧.md "wikilink") - 我有少少鍾意咗佢（[王允祈](../Page/王允祈.md "wikilink")）                                         |
| 最具創意大獎              | [吉百利牛奶朱古力](../Page/吉百利.md "wikilink") - 眉飛色舞篇                                                                       |
| 專業評審榮譽大獎            | [吉百利牛奶朱古力](../Page/吉百利.md "wikilink") - 眉飛色舞篇                                                                       |
| 最佳導演                | [吉百利牛奶朱古力](../Page/吉百利.md "wikilink") - 眉飛色舞篇                                                                       |

### 2011年

| 獎項                   | 獲奬                                                                              |
| -------------------- | ------------------------------------------------------------------------------- |
| 最受歡迎電視廣告－飲食餐宴        | [陽光檸檬茶](../Page/陽光檸檬茶.md "wikilink") - 初戀                                       |
| 最受歡迎電視廣告－潮流服飾及美容     | [adidas](../Page/adidas.md "wikilink") 2010 世界杯 - F50極速对决                       |
| 最受歡迎電視廣告－財富投資        | [EPS](../Page/EPS.md "wikilink") 雙生兒                                            |
| 最受歡迎電視廣告－最佳導演        | [中原地產](../Page/中原地產.md "wikilink") - 緣分篇 [陳敏聰](../Page/陳敏聰.md "wikilink")       |
| 最受歡迎電視廣告－樓宇地產        | [中原地產](../Page/中原地產.md "wikilink") - 緣分篇                                        |
| 最受歡迎電視廣告－家品生活        | [百佳幫你日日賺](../Page/百佳.md "wikilink") - 父子篇                                       |
| 最受歡迎電視廣告－消閒、時尚及品味    | [海洋公園十月全城哈囉喂](../Page/海洋公園.md "wikilink") - 十週年                                 |
| 最受歡迎電視廣告－電訊、運輸及公共設施  | [港鐵](../Page/港鐵.md "wikilink") - 唔好意思                                           |
| 最受歡迎電視廣告歌曲           | [陽光檸檬茶](../Page/陽光檸檬茶.md "wikilink") - 初戀（如果......陽光）                           |
| 最受歡迎電視廣告－綜合類別        | 13價疫苗 - 護士媽媽篇                                                                   |
| 最受歡迎電視廣告－資訊系列        | [九巴乘客之道](../Page/九巴.md "wikilink") - 與你同心同路                                     |
| 最受歡迎電視廣告－系列廣告        | [麥當勞](../Page/麥當勞.md "wikilink") - 「愛.延續」電視廣告系列                                 |
| 最受歡迎男主角              | [百佳幫你日日賺](../Page/百佳.md "wikilink")（[黎耀祥](../Page/黎耀祥.md "wikilink")）           |
| 最受歡迎女主角              | [海洋公園十月全城哈囉喂](../Page/海洋公園.md "wikilink") - 十週年（[羅蘭](../Page/羅蘭.md "wikilink")） |
| 最受歡迎電視廣告－最具創意大獎      | [中原地產](../Page/中原地產.md "wikilink") - 緣分篇                                        |
| 最受歡迎電視廣告－專業評審榮譽大獎    | [中原地產](../Page/中原地產.md "wikilink") - 緣分篇                                        |
| 最受歡迎電視廣告－最具創造力廣告公司大獎 | Metta Communications Limited                                                    |
| 最受歡迎電視廣告－榮譽大獎        | [利嘉閣地產](../Page/利嘉閣.md "wikilink") - 仲唔返屋企                                      |
|                      |                                                                                 |

### 2012年

| 獎項                  | 獲奬                                                                                                            |
| ------------------- | ------------------------------------------------------------------------------------------------------------- |
| 最受歡迎電視廣告－飲食餐宴       | [雅培](../Page/雅培.md "wikilink") - 便便面試                                                                         |
| 最受歡迎電視廣告－潮流服飾及美容    | [Dermes](../Page/Dermes.md "wikilink") - 一個承諾 十萬見證                                                            |
| 最受歡迎電視廣告－財富投資       | [中銀集團人壽](../Page/中銀集團人壽.md "wikilink") - 母子篇                                                                  |
| 最受歡迎電視廣告－樓宇地產       | [利嘉閣地產](../Page/利嘉閣.md "wikilink") - It's so High                                                             |
| 最受歡迎電視廣告－家品生活       | [Tempo盒裝紙巾](../Page/Tempo.md "wikilink") - 夠韌就有驚喜篇                                                            |
| 最受歡迎電視廣告－消閒、時尚及品味   | [三星GALAXY Tab–Help Koo](../Page/三星GALAXY_Tab–Help_Koo.md "wikilink") - 韓語篇                                    |
| 最受歡迎電視廣告－電訊、運輸及公共設施 | [FedEx](../Page/FedEx.md "wikilink") - 喪屍危機                                                                   |
| 最受歡迎電視廣告－綜合類別       | [萬寧](../Page/萬寧.md "wikilink") - 萬寧貓咪篇（上集）                                                                    |
| 最受歡迎男主角             | [三星GALAXY Tab–Help Koo](../Page/三星GALAXY_Tab–Help_Koo.md "wikilink") - 韓語篇 （[古天樂](../Page/古天樂.md "wikilink")） |
| 最受歡迎女主角             | [萬寧](../Page/萬寧.md "wikilink") - 宇宙最強爸爸（[黃學盈](../Page/黃學盈.md "wikilink")）                                     |
| 最受歡迎電視廣告歌曲          | [道地極品玄米茶](../Page/道地極品玄米茶.md "wikilink") - 企鵝篇 (無心快意)                                                         |
| 最受歡迎電視廣告－資訊系列       | [藍十字至醒貼士](../Page/藍十字.md "wikilink")                                                                          |
| 最受歡迎電視廣告－系列廣告       | [萬寧](../Page/萬寧.md "wikilink") · [萬寧貓咪篇](../Page/萬寧.md "wikilink")                                            |
| 榮譽大獎                | [利嘉閣地產](../Page/利嘉閣.md "wikilink") - It's so High                                                             |
| 最具創意大獎              | [益力多](../Page/益力多.md "wikilink")2011電視廣告系列 - 足球篇                                                              |
| 專業評審榮譽大獎            | [益力多](../Page/益力多.md "wikilink")2011電視廣告系列 - 足球篇                                                              |
| 最佳視覺效果大獎            | [益力多](../Page/益力多.md "wikilink")2011電視廣告系列 - 足球篇                                                              |
| 最具創造力廣告公司大獎         | Metta Communications                                                                                          |

### 2013年

| 獎項                  | 獲奬                                                                                  |
| ------------------- | ----------------------------------------------------------------------------------- |
| 最受歡迎電視廣告－飲食餐宴       | [麥當勞](../Page/麥當勞.md "wikilink") - 開心篇                                              |
| 最受歡迎電視廣告－潮流服飾及美容    | [Dermes](../Page/Dermes.md "wikilink") - 展露女生最細緻的美                                  |
| 最受歡迎電視廣告－財富地產       | [EPS EasyCash](../Page/EPS_EasyCash.md "wikilink") - 幾多點                            |
| 最受歡迎電視廣告－家品生活       | [Tempo卷紙](../Page/Tempo.md "wikilink") - 完美男人                                       |
| 最受歡迎電視廣告－消閒、時尚及品味   | [鐵達時](../Page/鐵達時.md "wikilink") - 100年之約                                           |
| 最受歡迎電視廣告－電訊、運輸及公共設施 | [港鐵](../Page/港鐵.md "wikilink") - 維修先鋒                                               |
| 最受歡迎電視廣告－綜合類別       | [萬寧](../Page/萬寧.md "wikilink") - 紅粉貓貓Cat Cat 笑                                      |
| 最受歡迎男主角             | [萬寧](../Page/萬寧.md "wikilink") - Mannings Beauty （[劉青雲](../Page/劉青雲.md "wikilink")） |
| 最受歡迎女主角             | [海洋公園](../Page/海洋公園.md "wikilink") - 十月全城哈囉喂 2012 （[羅蘭](../Page/羅蘭.md "wikilink")）  |
| 最受歡迎電視廣告歌曲          | [PCCW-HKT 服務無極](../Page/PCCW-HKT_服務無極.md "wikilink") - （天機密語 PCCW-HKT 電視廣告主題曲）      |
| 最受歡迎電視廣告－系列廣告       | [維他蒸餾水](../Page/維他蒸餾水.md "wikilink") · 7百萬人的點滴 2012                                 |
| 至尊榮譽大獎              | [鐵達時](../Page/鐵達時.md "wikilink") - 100年之約                                           |
| 最具創意大獎              | [鐵達時](../Page/鐵達時.md "wikilink") - 100年之約                                           |
| 專業評審榮譽大獎            | [鐵達時](../Page/鐵達時.md "wikilink") - 100年之約                                           |
| 最佳視覺效果大獎            | [鐵達時](../Page/鐵達時.md "wikilink") - 100年之約                                           |
| 最具創造力廣告公司大獎         | Metta Communications                                                                |

### 2014年

| 獎項                  | 獲奬                                                                                |
| ------------------- | --------------------------------------------------------------------------------- |
| 最受歡迎電視廣告－飲食餐宴       | [太興](../Page/太興燒味.md "wikilink") - My Hero 太好味道                                   |
| 最受歡迎電視廣告－潮流服飾及美容    | [Salad](../Page/Salad.md "wikilink") - Carry Me Lite                              |
| 最受歡迎電視廣告－財富地產       | 正確使用[八達通](../Page/八達通.md "wikilink")                                              |
| 最受歡迎電視廣告－家品生活       | [惠康](../Page/惠康.md "wikilink") - 六十八週年，好多獻給你                                      |
| 最受歡迎電視廣告－消閒、時尚及品味   | [海洋公園](../Page/海洋公園.md "wikilink") - 哈囉喂 全日祭 2013                                 |
| 最受歡迎電視廣告－電訊、運輸及公共設施 | [港鐵](../Page/港鐵.md "wikilink") - 新生代 鐵路網 (沙中綫)                                    |
| 最受歡迎電視廣告－綜合類別       | [萬寧](../Page/萬寧.md "wikilink") - 萬寧貓遇上[鄭多燕](../Page/鄭多燕.md "wikilink")            |
| 最受歡迎男主角             | [太興](../Page/太興燒味.md "wikilink") - My Hero 太好味道（[郭偉亮](../Page/郭偉亮.md "wikilink")） |
| 最受歡迎女主角             | [益達](../Page/益達.md "wikilink") - 度出好笑容（[黃翠如](../Page/黃翠如.md "wikilink")）          |
| 最受歡迎電視廣告歌曲          | [道地極品十八茶](../Page/道地.md "wikilink") (餘生一起過)                                       |
| 最受歡迎電視廣告－系列廣告       | [萬寧](../Page/萬寧.md "wikilink") - 萬寧操FIT香港                                         |
| 至尊榮譽大獎              | [鐵達時](../Page/鐵達時.md "wikilink") - 時間樹篇                                           |
| 最具創意大獎              | [益力多LT](../Page/益力多.md "wikilink")2013 - 法庭篇                                      |
| 最受觀眾喜愛大獎            | [太興](../Page/太興燒味.md "wikilink") - My Hero 太好味道                                   |
| 最佳視覺效果大獎            | [維珍航空](../Page/維珍航空.md "wikilink") - 遨遊天際　飛躍不凡                                    |
| 最具創造力廣告公司大獎         | Metta Communications Limited                                                      |

### 2015年

<table style="width:80%;">
<colgroup>
<col style="width: 40%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獎項</p></th>
<th><p>獲奬</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>專業評審獎</p></td>
<td><p><a href="../Page/鐵達時.md" title="wikilink">鐵達時</a> - 企鵝篇<br />
<a href="../Page/鐵達時.md" title="wikilink">鐵達時</a> - 那一年篇<br />
<a href="../Page/香港吸煙與健康委員會.md" title="wikilink">香港吸煙與健康委員會</a> - 煙、槍一樣，可以殺人。小心！<br />
<a href="../Page/益力多.md" title="wikilink">益力多LT</a>2014 - 舞獅篇<br />
<a href="../Page/益力多.md" title="wikilink">益力多LT</a>2014 - 打劫篇<br />
<a href="../Page/港鐵.md" title="wikilink">港鐵港島綫西延</a><br />
<a href="../Page/Sony.md" title="wikilink">Sony</a> A6000 - IT'S THE MOMENT STILL<br />
<a href="../Page/幸福醫藥.md" title="wikilink">幸福醫藥</a>2015 - 獅子山精神<br />
<a href="../Page/萬寧_(零售商).md" title="wikilink">Mannings</a> - 萬寧強得幾多得幾多<br />
<a href="../Page/海洋公園.md" title="wikilink">海洋公園</a> - 哈囉喂雙倍全日祭<br />
<a href="../Page/佳能.md" title="wikilink">佳能保鮮紙</a> - 失散篇<br />
<a href="../Page/惠康.md" title="wikilink">惠康</a> - 多少年至Like依然</p></td>
</tr>
<tr class="even">
<td><p>最受歡迎電視廣告</p></td>
<td><p><a href="../Page/M&amp;M&#39;s.md" title="wikilink">M&amp;M'S</a> 擄粒事件<br />
<a href="../Page/鐵達時.md" title="wikilink">鐵達時</a> - 企鵝篇<br />
<a href="../Page/美素佳兒.md" title="wikilink">美素佳兒</a> - 盡情體驗童年時<br />
<a href="../Page/實惠家居.md" title="wikilink">實惠家居</a> - 小小空間 大大宇宙<br />
<a href="../Page/TEMPO.md" title="wikilink">TEMPO</a> - 愛人篇<br />
<a href="../Page/萬寧_(零售商).md" title="wikilink">Mannings</a> - 萬寧書院校歌篇<br />
<a href="../Page/萬寧_(零售商).md" title="wikilink">Mannings</a> - 萬寧強得幾多得幾多<br />
<a href="../Page/海洋公園.md" title="wikilink">海洋公園</a> - 哈囉喂雙倍全日祭<br />
<a href="../Page/維他.md" title="wikilink">維他港式奶茶</a> - 港式俚語篇<br />
<a href="../Page/惠康.md" title="wikilink">惠康</a> - 多少年至Like依然</p></td>
</tr>
<tr class="odd">
<td><p>最受歡迎男主角</p></td>
<td><p><a href="../Page/萬寧_(零售商).md" title="wikilink">Mannings</a> - 萬寧強得幾多得幾多（<a href="../Page/古天樂.md" title="wikilink">古天樂</a>）</p></td>
</tr>
<tr class="even">
<td><p>最受歡迎女主角</p></td>
<td><p><a href="../Page/惠康.md" title="wikilink">惠康</a> - 多少年至Like依然（<a href="../Page/羅蘭_(香港).md" title="wikilink">羅蘭</a>）</p></td>
</tr>
<tr class="odd">
<td><p>至尊榮譽大獎</p></td>
<td><p><a href="../Page/幸福醫藥.md" title="wikilink">幸福醫藥</a>2015 - 獅子山精神</p></td>
</tr>
<tr class="even">
<td><p>最具創意大獎</p></td>
<td><p><a href="../Page/幸福醫藥.md" title="wikilink">幸福醫藥</a>2015 - 獅子山精神</p></td>
</tr>
<tr class="odd">
<td><p>最受觀眾喜愛大獎</p></td>
<td><p><a href="../Page/惠康.md" title="wikilink">惠康</a> - 多少年 至Like依然</p></td>
</tr>
<tr class="even">
<td><p>最佳視覺效果大獎</p></td>
<td><p><a href="../Page/幸福醫藥.md" title="wikilink">幸福醫藥</a>2015 - 獅子山精神</p></td>
</tr>
<tr class="odd">
<td><p>最具創造力廣告公司大獎</p></td>
<td><p>Metta Communications Limited</p></td>
</tr>
</tbody>
</table>

### 2016年

<table style="width:80%;">
<colgroup>
<col style="width: 40%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獎項</p></th>
<th><p>獲奬</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>最受歡迎電視廣告</p></td>
<td><p><a href="../Page/SAMSUNG.md" title="wikilink">SAMSUNG</a> <a href="../Page/Galaxy_Note_5.md" title="wikilink">Galaxy Note 5</a> - LOUIS 分身篇<br />
<a href="../Page/維他奶.md" title="wikilink">維他奶</a> - 2015 Stand By Me<br />
<a href="../Page/太興.md" title="wikilink">太興</a> My Hero II<br />
<a href="../Page/道地.md" title="wikilink">道地極品解茶</a><br />
<a href="../Page/海洋公園.md" title="wikilink">海洋公園</a><a href="../Page/哈囉喂.md" title="wikilink">哈囉喂</a> - 攞你命FIFTEEN<br />
<a href="../Page/麥當勞.md" title="wikilink">麥當勞Big</a> Mac - 相逢何必曾BIG MAC<br />
<a href="../Page/惠康.md" title="wikilink">惠康</a> - 同行70年 至Like依然<br />
AIA <a href="../Page/友邦保險.md" title="wikilink">友邦保險</a>2015 - 愛在當下之父親篇<br />
<a href="../Page/EPS.md" title="wikilink">EPS</a> EasyCash - 唔使急最緊要快<br />
<a href="../Page/CSL.md" title="wikilink">CSL</a> - 大網絡 大時代</p></td>
</tr>
<tr class="even">
<td><p>最受歡迎男主角</p></td>
<td><p><a href="../Page/麥當勞.md" title="wikilink">麥當勞Big</a> Mac - 相逢何必曾BIG MAC （<a href="../Page/蔣志光.md" title="wikilink">蔣志光</a>）</p></td>
</tr>
<tr class="odd">
<td><p>最受歡迎女主角</p></td>
<td><p><a href="../Page/海洋公園.md" title="wikilink">海洋公園</a><a href="../Page/哈囉喂.md" title="wikilink">哈囉喂</a> - 攞你命FIFTEEN （<a href="../Page/羅蘭.md" title="wikilink">羅蘭</a>）</p></td>
</tr>
<tr class="even">
<td><p>最受觀眾喜愛大獎</p></td>
<td><p><a href="../Page/惠康.md" title="wikilink">惠康</a> - 同行70年 至Like依然</p></td>
</tr>
<tr class="odd">
<td><p>最具效益廣告優異獎</p></td>
<td><p><a href="../Page/中原地產.md" title="wikilink">中原地產</a>2015 - Big 爆樓盤資料褲<br />
<a href="../Page/幸福醫藥.md" title="wikilink">幸福醫藥</a>2015 - What the咳<br />
<a href="../Page/PPS.md" title="wikilink">PPS</a> 2015 繳費靈 碟仙篇</p></td>
</tr>
<tr class="even">
<td><p>最具創意大獎</p></td>
<td><p>AIA <a href="../Page/友邦保險.md" title="wikilink">友邦保險</a>2015 - 愛在當下之父親篇</p></td>
</tr>
<tr class="odd">
<td><p>至尊榮譽大獎</p></td>
<td><p><a href="../Page/海洋公園.md" title="wikilink">海洋公園</a><a href="../Page/哈囉喂.md" title="wikilink">哈囉喂</a> - 攞你命FIFTEEN</p></td>
</tr>
<tr class="even">
<td><p>最具創造力廣告公司大獎</p></td>
<td><p>Metta Communications Limited</p></td>
</tr>
</tbody>
</table>

## 最受歡迎男女主角記錄

|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |

## 參考

  - [友邦《愛在當下》廣告榮獲
    TVB最受歡迎電視廣告大獎兩項殊榮](http://paper.wenweipo.com/2016/08/02/zt1608020003.htm)
  - [郭偉亮Chok爆拉票競逐最受歡迎電視廣告](https://hk.on.cc/hk/bkn/cnt/entertainment/20160616/bkn-20160616183518243-0616_00862_001.html)

## 外部連結

  - [TVB最受歡迎電視廣告大獎官方網站](http://event.tvb.com/tvcawards/)
  - [TVB最受歡迎廣告頒獎典禮2007
    官方網頁](http://jade.tvb.com/special/tvcawards2007/index2.html)
  - [TVB最受歡迎電視廣告大獎2008 官方網頁](http://ad.tvb.com/tvcawards/index.html)
  - [TVB最受歡迎電視廣告大獎2009 官方網頁](http://event.tvb.com/tvcawards/index.php)
  - [TVB最受歡迎電視廣告大獎2010 官方網頁](http://event.tvb.com/tvcawards/)
  - [TVB最受歡迎電視廣告大獎2011 官方網頁](http://event.tvb.com/tvcawards/)
  - [TVB最受歡迎電視廣告大獎2012 官方網頁](http://event.tvb.com/tvcawards/)
  - [TVB最受歡迎電視廣告大獎2013 官方網頁](http://event.tvb.com/tvcawards/)
  - [TVB最受歡迎電視廣告大獎2014 官方網頁](http://event.tvb.com/tvcawards/)
  - [TVB最受歡迎電視廣告大獎2015 官方網頁](http://event.tvb.com/tvcawards)
  - [TVB最受歡迎電視廣告大獎2016 官方網頁](http://event.tvb.com/tvcawards)

## 電視節目的變遷

[Category:無綫電視特備節目](../Category/無綫電視特備節目.md "wikilink")
[Category:2007年电视奖项](../Category/2007年电视奖项.md "wikilink")
[Category:2008年电视奖项](../Category/2008年电视奖项.md "wikilink")
[Category:2009年电视奖项](../Category/2009年电视奖项.md "wikilink")
[Category:2010年电视奖项](../Category/2010年电视奖项.md "wikilink")
[Category:2011年电视奖项](../Category/2011年电视奖项.md "wikilink")
[Category:2012年电视奖项](../Category/2012年电视奖项.md "wikilink")
[Category:2013年电视奖项](../Category/2013年电视奖项.md "wikilink")
[Category:2014年电视奖项](../Category/2014年电视奖项.md "wikilink")