**道Taew**（[英文](../Page/英文.md "wikilink")：Natapohn
Tameeruks，[泰语](../Page/泰语.md "wikilink")： ณฐพร
เตมีรักษ์，），是一個[泰國](../Page/泰國.md "wikilink")[藝人](../Page/藝人.md "wikilink")。

## 簡介

[泰国三台女演员](../Page/泰國第3電視台.md "wikilink")，1989年2月6日出生，毕业于泰国第一學府[朱拉隆功大学建筑系](../Page/朱拉隆功大学.md "wikilink")。她出演了多部热门电视剧，凭借著日趋成熟的演技获得了电视剧最佳女主角奖项。

## 電視劇

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>電視台</p></th>
<th><p>名稱</p></th>
<th><p>角色</p></th>
<th><p>合作者</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2006</p></td>
<td><p>泰國3台</p></td>
<td><p><a href="../Page/可爱.md" title="wikilink">可爱</a><br />
Narak<br />
</p></td>
<td><p>Narak</p></td>
<td><p><a href="../Page/Man_Karin.md" title="wikilink">Man Karin</a></p></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p>泰國3台</p></td>
<td><p><a href="../Page/心的抉擇.md" title="wikilink">心的抉擇</a><br />
Sood Tae Jai Ja Kwai Kwa<br />
</p></td>
<td><p>Sawang</p></td>
<td><p><a href="../Page/亞歷克斯·倫德爾.md" title="wikilink">亞歷克斯·倫德爾</a></p></td>
</tr>
<tr class="odd">
<td><p>泰國3台</p></td>
<td><p><a href="../Page/胡椒与稻米葉.md" title="wikilink">胡椒与稻米葉</a><br />
Prik Tai Gub BaiKao<br />
</p></td>
<td></td>
<td><p><a href="../Page/Nattarat.md" title="wikilink">Nattarat</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><p>泰國3台</p></td>
<td><p><a href="../Page/浮華世家.md" title="wikilink">浮華世家</a><br />
Dong Poo Dee<br />
</p></td>
<td><p>Kom</p></td>
<td><p><a href="../Page/Smart_Krissada_Pornweroj(Mart).md" title="wikilink">Smart Krissada Pornweroj(Mart)</a></p></td>
</tr>
<tr class="odd">
<td><p>泰國3台</p></td>
<td><p><a href="../Page/离心.md" title="wikilink">离心</a><br />
Hua Jai Song Park<br />
</p></td>
<td><p>Tawanchai</p></td>
<td><p><a href="../Page/Por.md" title="wikilink">Por</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td><p>泰國3台</p></td>
<td><p><a href="../Page/爱在宋干.md" title="wikilink">爱在宋干</a><br />
Songkran Hang Kwarm Ruk<br />
</p></td>
<td><p>Nam</p></td>
<td><p><a href="../Page/Art_Pasut_Banyam.md" title="wikilink">Art Pasut Banyam</a></p></td>
</tr>
<tr class="odd">
<td><p>泰國3台</p></td>
<td><p><a href="../Page/心的约束.md" title="wikilink">心的约束</a><br />
Pieng Jai Tee Pook Pun<br />
</p></td>
<td><p>Muk</p></td>
<td><p><a href="../Page/阿提查·春那侬.md" title="wikilink">阿提查·春那侬</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td><p>泰國3台</p></td>
<td><p><a href="../Page/骄傲的火焰.md" title="wikilink">骄傲的火焰</a><br />
Plerng Torranong<br />
</p></td>
<td><p>Namfon</p></td>
<td><p><a href="../Page/马里奥·毛瑞尔.md" title="wikilink">馬力歐·莫瑞爾</a></p></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td><p>泰國3台</p></td>
<td><p><a href="../Page/撒旦保镖.md" title="wikilink">撒旦保镖</a><br />
Phu Pha Prae Mai<br />
</p></td>
<td><p>Paremai</p></td>
<td><p><a href="../Page/Por_Thrisadee_Sahawong.md" title="wikilink">Por Thrisadee Sahawong</a></p></td>
</tr>
<tr class="even">
<td><p>泰國3台</p></td>
<td><p><a href="../Page/日耀云帘.md" title="wikilink">日耀云帘</a><br />
Tawan Chai Nai Marn Mek<br />
</p></td>
<td><p>Tawanchai</p></td>
<td><p><a href="../Page/貝·柏光.md" title="wikilink">貝·柏光</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p>泰國3台</p></td>
<td><p><a href="../Page/名门绅士五部曲之缘定芳林.md" title="wikilink">名门绅士五部曲之缘定芳林</a><br />
Supapburus JuthaThep Khunchai Rachonon<br />
</p></td>
<td><p>Soifah</p></td>
<td><p><a href="../Page/博·坦尼.md" title="wikilink">博·坦尼</a></p></td>
</tr>
<tr class="even">
<td><p>2014</p></td>
<td><p>泰國3台</p></td>
<td><p><a href="../Page/星阁怨.md" title="wikilink">星阁怨</a><br />
Wiang Roy Dao<br />
</p></td>
<td><p>Roydao</p></td>
<td><p><a href="../Page/Smart_Krissada_Pornweroj(Mart).md" title="wikilink">Smart Krissada Pornweroj(Mart)</a></p></td>
</tr>
<tr class="odd">
<td><p>泰國3台</p></td>
<td><p><a href="../Page/日冕之恋.md" title="wikilink">日冕之恋</a><br />
Roy Ruk Hak Liam Tawan<br />
</p></td>
<td><p>Praedao</p></td>
<td><p><a href="../Page/马里奥·毛瑞尔.md" title="wikilink">馬力歐·莫瑞爾</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015</p></td>
<td><p>泰國3台</p></td>
<td><p><a href="../Page/风之戀.md" title="wikilink">风之戀</a><br />
Lom Son Ruk<br />
</p></td>
<td><p>Pattarin</p></td>
<td><p><a href="../Page/纳得克·库吉米亚.md" title="wikilink">纳得克·库吉米亚</a></p></td>
</tr>
<tr class="odd">
<td><p>泰國3台</p></td>
<td><p><a href="../Page/龙裔黑帮五部曲之犀牛.md" title="wikilink">龙裔黑帮五部曲之犀牛</a><br />
Luadmungkorn Rhino<br />
</p></td>
<td><p>Panthera</p></td>
<td><p><a href="../Page/Andrew_Gregson.md" title="wikilink">Andrew Gregson</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td><p>泰國3台</p></td>
<td></td>
<td><p>娜迦女神</p></td>
<td><p>Kenp</p></td>
</tr>
<tr class="odd">
<td><p>2018</p></td>
<td><p>泰國3台</p></td>
<td><p><a href="../Page/熾愛遊戲.md" title="wikilink">熾愛遊戲</a><br />
nok<br />
</p></td>
<td><p>James Ji</p></td>
<td></td>
</tr>
</tbody>
</table>

## 電影

  - 2006:《Dek Hor/鬼宿舍》飾演 Namtan/纳坦

## 外部链接

  - [道Taew中文论坛](https://web.archive.org/web/20091106141905/http://www.taewcn.com/bbs/index.php)

  - [泰國第3電視臺官方網站](http://www.thaitv3.com)

  -
[Category:泰國女演員](../Category/泰國女演員.md "wikilink")
[Category:泰國華人](../Category/泰國華人.md "wikilink")
[Category:朱拉隆功大學校友](../Category/朱拉隆功大學校友.md "wikilink")