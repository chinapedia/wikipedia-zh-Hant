[Start-here.svg](https://zh.wikipedia.org/wiki/File:Start-here.svg "fig:Start-here.svg")
**Tango桌面計劃**（）針對為不同的[自由與](../Page/自由軟體.md "wikilink")[開放原始碼](../Page/開放原始碼.md "wikilink")[桌面環境上的應用程式提供一個協調的使用者體驗](../Page/桌面環境.md "wikilink")，該計劃的主要目標為讓開發者可輕易地將它們的軟體與桌面在外觀方面結合起來。由不同的桌面環境（[KDE](../Page/KDE.md "wikilink")、[GNOME](../Page/GNOME.md "wikilink")、[XFCE](../Page/XFCE.md "wikilink")...）與自訂品牌的分發版所引起的視覺不協調令第三方難以面向[Linux](../Page/Linux.md "wikilink")。對Tango計劃的一個共通誤解為其目的是提供一個可於主要桌面環境上使用的圖示主題。

其樣式的目的並非以視覺上的獨特來區分自己。次要的目的為讓應用程式在操作系統上同時運行時可外觀相稱。[獨立軟體銷售商若提供遵循Tango樣式的圖示會發現它們的應用程式不會與Windows](../Page/獨立軟體銷售商.md "wikilink")
XP、Mac OS X、KDE、GNOME或XFCE出現不相稱。該樣式從可適當地配搭該些環境的Firefox應用軟件圖示集中得到大量靈感。

除提供視覺指引外該計劃致力為圖示提供一組共通的象徵，Tango遵循[Freedesktop.org的](../Page/Freedesktop.org.md "wikilink")[Standard
Icon Theming
Specification](http://freedesktop.org/wiki/Standards/icon-theme-spec)及積極地發展Freedesktop.org的[Standard
Icon Naming
Specification](http://standards.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html)來定義最常見圖示的名稱與其使用象徵。

許多自由軟體計劃，如[GIMP](../Page/GIMP.md "wikilink")、[Scribus與](../Page/Scribus.md "wikilink")[GNOME已開始在它們的圖示上遵循](../Page/GNOME.md "wikilink")[Tango樣式的指示](http://tango.freedesktop.org/Tango_Icon_Theme_Guidelines)。

## 外部連結

  - [Tango計劃](http://tango-project.org)官方主頁
  - [Tango project aims to clean up the
    desktop](http://www.linux.com/article.pl?sid=05/11/23/1952204)於Linux.com

[分類:傳達設計](../Page/分類:傳達設計.md "wikilink")

[Category:自由桌面环境](../Category/自由桌面环境.md "wikilink")
[Category:图形设计](../Category/图形设计.md "wikilink")