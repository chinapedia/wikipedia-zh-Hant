[Taste_in_Festival_Walk_201903.jpg](https://zh.wikipedia.org/wiki/File:Taste_in_Festival_Walk_201903.jpg "fig:Taste_in_Festival_Walk_201903.jpg")[又一城分店](../Page/又一城.md "wikilink")\]\]
[Taste_Supermarket_in_Shun_Tak_Centre_2015.jpg](https://zh.wikipedia.org/wiki/File:Taste_Supermarket_in_Shun_Tak_Centre_2015.jpg "fig:Taste_Supermarket_in_Shun_Tak_Centre_2015.jpg")[信德中心分店](../Page/信德中心.md "wikilink")\]\]

[Taste_in_KOLOUR_Yuen_Long_interior_201508.jpg](https://zh.wikipedia.org/wiki/File:Taste_in_KOLOUR_Yuen_Long_interior_201508.jpg "fig:Taste_in_KOLOUR_Yuen_Long_interior_201508.jpg")分店\]\]
[TasteStore_Maritime_Square.jpg](https://zh.wikipedia.org/wiki/File:TasteStore_Maritime_Square.jpg "fig:TasteStore_Maritime_Square.jpg")[青衣城分店](../Page/青衣城.md "wikilink")\]\]
[TasteStore_Huafa_Mall.JPG](https://zh.wikipedia.org/wiki/File:TasteStore_Huafa_Mall.JPG "fig:TasteStore_Huafa_Mall.JPG")華發商都分店\]\]
[HK_East_Point_City_Taste_Store_Interior.jpg](https://zh.wikipedia.org/wiki/File:HK_East_Point_City_Taste_Store_Interior.jpg "fig:HK_East_Point_City_Taste_Store_Interior.jpg")[東港城分店](../Page/東港城.md "wikilink")\]\]
[Sandwich_bread_(Bakers_Choice).JPG](https://zh.wikipedia.org/wiki/File:Sandwich_bread_\(Bakers_Choice\).JPG "fig:Sandwich_bread_(Bakers_Choice).JPG")**Taste**是[長江和記實業旗下](../Page/長江和記實業.md "wikilink")[屈臣氏集團於](../Page/屈臣氏集團.md "wikilink")[香港設立的](../Page/香港.md "wikilink")[連鎖](../Page/連鎖店.md "wikilink")[超級市場](../Page/超級市場.md "wikilink")，主要對象是[中產階級的](../Page/中產階級.md "wikilink")[消費者](../Page/消費者.md "wikilink")，[市場定位在](../Page/市場定位.md "wikilink")[百佳與](../Page/百佳.md "wikilink")[Great之間](../Page/Great.md "wikilink")，售賣源於世界各地的產品。首間分店於2004年11月在香港[九龍塘](../Page/九龍塘.md "wikilink")[又一城開幕](../Page/又一城.md "wikilink")，2010年起拓展中國内地市場，陆续在[廣州](../Page/廣州.md "wikilink")、[深圳](../Page/深圳.md "wikilink")、[佛山](../Page/佛山.md "wikilink")、[珠海](../Page/珠海.md "wikilink")、[上海](../Page/上海.md "wikilink")、[成都等地开设分店](../Page/成都.md "wikilink")。2015年12月在香港[鑽石山](../Page/鑽石山.md "wikilink")[荷里活廣場開設第](../Page/荷里活廣場.md "wikilink")15間分店，率先引入全新槪念保健產品及藥物專櫃。

Taste部份分店是翻新同一集團旗下百佳超級廣場後，改以新模式經營，部份店內出售之產品比百佳為之高檔，主要競爭對手為[牛奶國際旗下的](../Page/牛奶國際.md "wikilink")[3hreeSixty](../Page/3hreeSixty.md "wikilink")、[Market
Place by Jasons](../Page/Market_Place_by_Jasons.md "wikilink")、Jasons
Ichiba。

## 分店

香港區共設有14間分店。3間為全新店舖，4間已翻新為新裝修店舖，其他以舊裝修([百佳超級廣場](../Page/百佳超級廣場.md "wikilink"))式店舖。除特別註明外，是翻新[屈臣氏集團旗下](../Page/屈臣氏集團.md "wikilink")[百佳超級廣場](../Page/百佳超級廣場.md "wikilink")。

  - [香港島](../Page/香港島.md "wikilink")

| 分店地址                                                                                               | 營業時間            | 店舖設計                |
| -------------------------------------------------------------------------------------------------- | --------------- | ------------------- |
| [灣仔](../Page/灣仔.md "wikilink")[合和中心地下及地庫全層](../Page/合和中心.md "wikilink")                            | 08:00am–10:30pm | 舊裝修                 |
| [北角](../Page/北角.md "wikilink")[港運城](../Page/港運城.md "wikilink")[成坤廣場地庫](../Page/成坤廣場.md "wikilink") |                 | 全新店舖，（原百佳超級市場在商場二樓） |

  - [九龍](../Page/九龍.md "wikilink")

| 分店地址                                                                              | 營業時間            | 店舖設計                  |
| --------------------------------------------------------------------------------- | --------------- | --------------------- |
| [紅磡](../Page/紅磡.md "wikilink")[黃埔花園家居庭](../Page/黃埔花園.md "wikilink")               | 08:00am–10:30pm | 新裝修 （原百佳超級廣場）         |
| [九龍塘](../Page/九龍塘.md "wikilink")[又一城MTR層](../Page/又一城.md "wikilink")5號舖           | 07:30am–11:30pm | 新裝修                   |
| [土瓜灣](../Page/土瓜灣.md "wikilink")[半山壹號](../Page/半山壹號.md "wikilink")1樓12號舖          | 08:00am–10:00pm | 舊裝修                   |
| [牛頭角](../Page/牛頭角.md "wikilink")[淘大商場第](../Page/淘大商場.md "wikilink")3期1樓F188-204號舖 | 08:00am–10:30pm | 全新店舖（原百佳超級廣場在商場第1期1樓） |
| [鑽石山](../Page/鑽石山.md "wikilink")[荷里活廣場](../Page/荷里活廣場.md "wikilink")4樓401號舖       | 08:00am–10:30pm | 全新店舖（原百佳超級廣場在商場二樓）    |

  - [新界](../Page/新界.md "wikilink")

| 分店地址                                                                                           | 營業時間            | 店舖設計                             |
| ---------------------------------------------------------------------------------------------- | --------------- | -------------------------------- |
| [屯門](../Page/屯門.md "wikilink")[屯門市廣場](../Page/屯門市廣場.md "wikilink")2期2樓全層                       | 08:00am–10:30pm | 新裝修                              |
| [青衣](../Page/青衣.md "wikilink")[青衣城](../Page/青衣城.md "wikilink")3樓313號舖                          | 08:00am–10:30pm | 舊裝修                              |
| [荃灣](../Page/荃灣.md "wikilink")[荃灣站](../Page/荃灣站.md "wikilink")[綠楊坊](../Page/綠楊坊.md "wikilink") | 08:00am–10:30pm | 新裝修（原International by PARKnSHOP） |
| [馬鞍山](../Page/馬鞍山.md "wikilink")[馬鞍山廣場](../Page/馬鞍山廣場.md "wikilink")3樓343-363號舖                | 08:00am–10:00pm | 新裝修                              |
| [馬鞍山](../Page/馬鞍山.md "wikilink")[WeGoMALL](../Page/WeGoMALL.md "wikilink")301室                 | 08:00am–10:00pm | 2018年5月11日開幕，全新店舖                |
| [元朗千色匯地庫](../Page/元朗千色匯.md "wikilink")1樓全層                                                     | 08:00am–10:00pm | 新裝修                              |

  - [離島及](../Page/離島.md "wikilink")[大嶼山](../Page/大嶼山.md "wikilink")

| 分店地址                                                                   | 營業時間            | 店舖設計 |
| ---------------------------------------------------------------------- | --------------- | ---- |
| [東涌](../Page/東涌.md "wikilink")[東薈城地庫](../Page/東薈城.md "wikilink")1-10號舖 | 08:00am–10:30pm | 翻新中  |

  - [荃灣](../Page/荃灣.md "wikilink")[綠楊坊的店舖前身為International](../Page/綠楊坊.md "wikilink")
    by ParkNshop
  - [馬鞍山](../Page/馬鞍山.md "wikilink")[馬鞍山廣場的店舖前身為](../Page/馬鞍山廣場.md "wikilink")[八佰伴](../Page/八佰伴.md "wikilink")
  - [元朗千色匯的店舖前身為](../Page/元朗千色匯.md "wikilink")[百佳超級市場及同層其他店舖](../Page/百佳超級市場.md "wikilink")

中國

  - [廣州](../Page/廣州.md "wikilink")
      - [越秀區](../Page/越秀區.md "wikilink")[中華廣場三樓](../Page/中華廣場_\(廣州\).md "wikilink")（營業時間：8:30am–10:00pm，店舖前身為[吉之島](../Page/吉之島.md "wikilink")）
      - [番禺區](../Page/番禺區.md "wikilink")[洛溪新城如意中心首層](../Page/洛溪新城.md "wikilink")（營業時間：8:00am–11:00pm，店舖前身為[百佳超級市場](../Page/百佳超級市場.md "wikilink")）
      - [天河區](../Page/天河區.md "wikilink")[正佳廣場負一樓](../Page/正佳廣場.md "wikilink")（營業時間：平日8:30am-10:30pm/周五及六11:00pm，店舖前身為[百佳超級市場](../Page/百佳超級市場.md "wikilink")）
  - [深圳](../Page/深圳.md "wikilink")
      - [罗湖區金光華廣場負二樓](../Page/罗湖區.md "wikilink")（營業時間：平日8:30am-10:00pm/周五及六10:30pm）
      - [南山區](../Page/南山區.md "wikilink")[深圳來福士廣場負一樓](../Page/深圳來福士廣場.md "wikilink")（新店舖，營業時間：每日9:00am-10:00pm）
  - [珠海](../Page/珠海.md "wikilink")
      - [香洲區](../Page/香洲區.md "wikilink")[珠海大道](../Page/珠海大道.md "wikilink")8號[華發商都二樓](../Page/華發商都.md "wikilink")（營業時間：平日10:00am–10:00pm/周五及六10:30pm）
      - 香洲區[吉大](../Page/吉大街道.md "wikilink")[景山路](../Page/景山路.md "wikilink")220号[免税商场二楼](../Page/珠海免税商场.md "wikilink")（營業時間：9:30am–10:00pm，店舖前身為[百佳超級市場](../Page/百佳超級市場.md "wikilink")）
  - [佛山](../Page/佛山.md "wikilink")
      - [南海區](../Page/南海區.md "wikilink")[萬科廣場負一樓](../Page/萬科廣場.md "wikilink")（營業時間
        平日10:00am-10:00pm/周五至日10:30pm）
  - [成都](../Page/成都.md "wikilink")
      - [武侯區來福士廣場負二樓](../Page/武侯區.md "wikilink")（營業時間：9:00am–10:00pm）

已結業分店

<table>
<thead>
<tr class="header">
<th><p>分店地址</p></th>
<th><p>營業時間</p></th>
<th><p>店舖設計</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/赤柱.md" title="wikilink">赤柱</a><a href="../Page/赤柱廣場.md" title="wikilink">赤柱廣場</a>2樓201-203號舖</p></td>
<td><p>08:00am–10:00pm</p></td>
<td><p>舊裝修<br />
2017年5月19日結業，原址由<a href="../Page/牛奶國際.md" title="wikilink">牛奶國際旗下</a><a href="../Page/3hreeSixty.md" title="wikilink">3hreeSixty超市取代</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大角咀.md" title="wikilink">大角咀</a><a href="../Page/奧海城.md" title="wikilink">奧海城</a>2期地下1-10號舖</p></td>
<td><p>08:00am–10:30pm</p></td>
<td><p>舊裝修<br />
2017年7月24日結業，原址由<a href="../Page/牛奶國際.md" title="wikilink">牛奶國際旗下</a><a href="../Page/Market_Place_by_Jasons.md" title="wikilink">Market Place by Jasons超市取代</a> ，而奧海城三期 International by PARKNSHOP 亦於7月30日結業。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/葵涌.md" title="wikilink">葵涌</a><a href="../Page/葵芳.md" title="wikilink">葵芳</a><a href="../Page/新都會廣場.md" title="wikilink">新都會廣場</a>4樓429-432號舖</p></td>
<td><p>08:00am–10:30pm</p></td>
<td><p>舊裝修<br />
2017年7月4日結業，被商場發展商旗下的<a href="../Page/一田超級市場.md" title="wikilink">一田超級市場取代</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/將軍澳.md" title="wikilink">將軍澳</a><a href="../Page/坑口.md" title="wikilink">坑口</a><a href="../Page/東港城.md" title="wikilink">東港城</a>2樓228及228A號舖</p></td>
<td><p>08:00am–10:30pm</p></td>
<td><p>舊裝修<br />
2016年8月15日結業，被商場發展商旗下的<a href="../Page/一田超級市場.md" title="wikilink">一田超級市場取代</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/上環.md" title="wikilink">上環</a><a href="../Page/信德中心.md" title="wikilink">信德中心</a>4 樓 402A及402B 號鋪</p></td>
<td><p>08:00am–10:00pm</p></td>
<td><p>全新店舖<br />
2018年4月22日結業</p></td>
</tr>
</tbody>
</table>

## 出售產品

**Taste**出售來自全球的產品，包括：

  - 來自全球的[雜貨](../Page/雜貨.md "wikilink")
  - 新鮮[海產](../Page/海產.md "wikilink")
  - 來自[亞洲及](../Page/亞洲.md "wikilink")[日本的蔬果及產品](../Page/日本.md "wikilink")
  - [TaSTe麵包店的新鮮](../Page/Taste.md "wikilink")[麵包](../Page/麵包.md "wikilink")
  - 即叫即造的[三文治及沙律](../Page/&.md "wikilink")
    （只限[九龍塘](../Page/九龍塘.md "wikilink")[又一城](../Page/又一城.md "wikilink")、[灣仔](../Page/灣仔.md "wikilink")[合和中心及](../Page/合和中心.md "wikilink")[東涌](../Page/東涌_\(香港\).md "wikilink")[東薈城分店](../Page/東薈城.md "wikilink")）
  - 環球[熟食](../Page/熟食.md "wikilink")
    （[鑽石山](../Page/鑽石山.md "wikilink")[荷里活廣場及](../Page/荷里活廣場.md "wikilink")[上環](../Page/上環.md "wikilink")[信德中心分店除外](../Page/信德中心.md "wikilink")）
  - Watson's Wine Cellar 酒窖出售的[酒](../Page/酒.md "wikilink")
    （只限[紅磡](../Page/紅磡.md "wikilink")[黃埔花園家居庭](../Page/黃埔花園.md "wikilink")、[九龍塘](../Page/九龍塘.md "wikilink")[又一城及](../Page/又一城.md "wikilink")[東涌](../Page/東涌_\(香港\).md "wikilink")[東薈城分店](../Page/東薈城.md "wikilink")）
  - 家居用品（只限[馬鞍山廣場](../Page/馬鞍山廣場.md "wikilink")、[紅磡](../Page/紅磡.md "wikilink")[黃埔花園家居庭](../Page/黃埔花園.md "wikilink")、[牛頭角](../Page/牛頭角.md "wikilink")[淘大商場及](../Page/淘大商場.md "wikilink")[東涌](../Page/東涌.md "wikilink")[東薈城共](../Page/東薈城.md "wikilink")4間分店）
  - 保健產品及藥物專櫃（只限[鑽石山](../Page/鑽石山.md "wikilink")[荷里活廣場分店](../Page/荷里活廣場.md "wikilink")）
  - Gourmet Cuts
    牛肉專門店（只限[屯門市廣場](../Page/屯門市廣場.md "wikilink")、[馬鞍山廣場及](../Page/馬鞍山廣場.md "wikilink")[東涌](../Page/東涌.md "wikilink")[東薈城分店](../Page/東薈城.md "wikilink"))
  - Italian Tomato
    （只限[九龍塘](../Page/九龍塘.md "wikilink")[又一城分店](../Page/又一城.md "wikilink")）
  - Harts
    （只限[九龍塘](../Page/九龍塘.md "wikilink")[又一城分店](../Page/又一城.md "wikilink")）
  - ComeBuy 台式飲品店（只限[屯門市廣場分店](../Page/屯門市廣場.md "wikilink")）

## 全新裝修

位於[九龍塘](../Page/九龍塘.md "wikilink")[又一城是全港首間Taste](../Page/又一城.md "wikilink")，佔地35,000平方尺，於2013年12月斥3000萬大裝修後，以略帶鄉村風情的朱紅色調襯木材為主題設計，並引入多項全新及獨家發售的產品，如可持續養殖的荷蘭熏魚和福岡「甜王」士多啤梨，更有多達50種日本和韓國蔬果，又增設全人手主理的Pizza
Pala羅馬薄餅專櫃、新西蘭著名麵包烘烤品牌Loaf和刺身吧。集團亦會將續翻新其他門市。

到2018年11月，又一城分店再次進行翻新，斥資達4500萬元。3月1日重新開業後加入了不少創新科技元素，如為顧客介紹貨品的機械人「Pepper」，「訂購易」和自助收銀機等。同時增加健康食品數量。\[1\]

Taste Supermarket Festival Walk Interior
2014.jpg|又一城分店在2013年至2018年以朱紅色調襯木材為主題設計
Taste Supermarket Festival Walk Interior 2014-1.jpg|新裝修分店內貌（2014年） Taste
Supermarket Festival Walk Interior 201903.jpg|2019年3月又一城分店翻新後，劃分五大專區
Taste Supermarket in Tuen Mun Town Plaza during renovation
201412.jpg|正進行翻新的[屯門市廣場分店](../Page/屯門市廣場.md "wikilink")（2014年12月）

## 批評

有批評指[百佳超級市場](../Page/百佳.md "wikilink")／廣場及當時旗下的[九龍塘](../Page/九龍塘.md "wikilink")[又一城](../Page/又一城.md "wikilink")**Taste**分店於2005年4月共解僱525名員工，大部分員工也於2006年[農曆新年前被解僱](../Page/農曆新年.md "wikilink")，但九成被解僱的員工被重新聘用。並指職位與被解僱前一樣，薪金被降而工時卻加長。

## 參考資料

  - [Taste](https://web.archive.org/web/20070314024724/http://www.aswatson.com/c_retail_fegm_221_taste.htm)

[Category:2004年成立的公司](../Category/2004年成立的公司.md "wikilink")
[Category:香港超級市場](../Category/香港超級市場.md "wikilink")
[Category:屈臣氏集團](../Category/屈臣氏集團.md "wikilink")

1.