**Technōs
Japan株式会社**（，1981年12月－1996年，通常簡稱「Technos（）」）是[日本一家已破產的](../Page/日本.md "wikilink")[電子遊戲開發公司](../Page/電子遊戲開發商.md "wikilink")，擅長於[格鬥遊戲及](../Page/格鬥遊戲.md "wikilink")[運動遊戲](../Page/運動遊戲.md "wikilink")，以開發出[熱血系列和](../Page/熱血系列.md "wikilink")[雙截龍系列而盛極一時](../Page/雙截龍系列.md "wikilink")。

Technos是在1981年12月由自[Data
East出走的以瀧邦夫為中心的開發人員所創設的](../Page/Data_East.md "wikilink")，第一代總部設於[東京都](../Page/東京都.md "wikilink")[新宿區](../Page/新宿區.md "wikilink")[西新宿](../Page/西新宿.md "wikilink")，其後搬遷到[歌舞伎町](../Page/歌舞伎町.md "wikilink")。其崛起的契機是在1986年5月於[街機上推出的橫向格鬥動作遊戲](../Page/街機.md "wikilink")《[熱血硬派](../Page/熱血硬派.md "wikilink")》；隔年1987年11月，又在街機上推出的球類運動遊戲《[熱血高校](../Page/熱血高校.md "wikilink")》（又譯「[熱血躲避球](../Page/熱血躲避球.md "wikilink")」），兩款也是日後同類遊戲的元祖作品。由於這兩款作品都博得極大的人氣，成功地打響了「熱血」的名號，所以Technos
Japan後來更專注於續作的開發，陸續推出多款遊戲，形成了所謂的「[熱血系列](../Page/熱血系列.md "wikilink")」。1987年，Technos除了推出《熱血高校》之外，還延續上年《熱血硬派》的熱潮推出了另一款格鬥遊戲《[雙截龍](../Page/雙截龍_\(遊戲\).md "wikilink")》，同樣大受好評，之後也陸續推出續作，形成「[雙截龍系列](../Page/雙截龍系列.md "wikilink")」。

雖然Technos靠著兩大人氣遊戲系列熱血系列及雙截龍系列而得到大量營收，但也由於過度依賴這兩條產品線，以致後來創意枯竭，未能有人氣新作繼起；再加上Technos於1991年用了大量資金在東京都[中野區](../Page/中野區.md "wikilink")[新井建造了](../Page/新井.md "wikilink")「Technos中野大廈（）」（大廈於1992年11月落成入伙，並且作為其新總部），以致於[泡沫經濟崩壞時終於被沈重的利息負擔所壓垮](../Page/泡沫經濟.md "wikilink")，1995年12月15日停止營業，1996年宣告破產倒閉。Technos中野大廈直至2013年10月1日才更名為「草莓中野北大樓（）」。

Technos的[版權自破產後由當年的開發團隊新成立的](../Page/版權.md "wikilink")[Million](../Page/Million.md "wikilink")（）所擁有，以前由[Atlus發行](../Page/Atlus.md "wikilink")[移植版](../Page/移植版.md "wikilink")、[復刻版等](../Page/復刻版.md "wikilink")，現在由[Arc
System
Works發行](../Page/Arc_System_Works.md "wikilink")。2015年6月12日Million所擁有的Technos的版權全部轉讓給Arc
System Works。\[1\]

## 註腳

[Technōs_Japan](../Category/Technōs_Japan.md "wikilink")
[Category:日本已結業電子遊戲公司](../Category/日本已結業電子遊戲公司.md "wikilink")
[Category:1981年開業電子遊戲公司](../Category/1981年開業電子遊戲公司.md "wikilink")
[Category:1996年結業電子遊戲公司](../Category/1996年結業電子遊戲公司.md "wikilink")
[Category:中野區歷史](../Category/中野區歷史.md "wikilink")

1.