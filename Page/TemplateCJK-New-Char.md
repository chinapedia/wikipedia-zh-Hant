<div class="notice CJKNewChar" id="spoiler">

<small>**注意**：本页面含有[Unihan](../Page/中日韓統一表意文字.md "wikilink"){{\#switch:}}}|A=[-{zh-cn:扩展;
zh-tw:擴充;}-A区](../Page/中日韓統一表意文字擴展區A.md "wikilink")|B=[-{zh-cn:扩展;
zh-tw:擴充;}-B区](../Page/中日韓統一表意文字擴展區B.md "wikilink")|C=[-{zh-cn:扩展;
zh-tw:擴充;}-C区](../Page/中日韓統一表意文字擴展區C.md "wikilink")|D=[-{zh-cn:扩展;
zh-tw:擴充;}-D区](../Page/中日韓統一表意文字擴展區D.md "wikilink")||E=[-{zh-cn:扩展;
zh-tw:擴充;}-E区](../Page/中日韓統一表意文字擴展區E.md "wikilink")|F=[-{zh-cn:扩展;
zh-tw:擴充;}-F区](../Page/中日韓統一表意文字擴展區F.md "wikilink")|3.0=[-{zh-cn:扩展;
zh-tw:擴充;}-A区](../Page/中日韓統一表意文字擴展區A.md "wikilink")|3.1=[-{zh-cn:扩展;
zh-tw:擴充;}-B区](../Page/中日韓統一表意文字擴展區B.md "wikilink")|4.1=4.1版|5.1=5.1版|5.2=5.2版|\#Default=新版}}用字{{\#if:|：「\<span
{{\#if:  |id="glyphwebfont"
class=""|}}\>{{\#if:|、{{\#if:|、{{\#if:|、{{\#if:|、{{\#if:|、{{\#if:|、{{\#if:|、{{\#if:|、{{\#if:|、{{\#if:|、{{\#if:|、{{\#if:|、{{\#if:|、{{\#if:|、{{\#if:|、{{\#if:|、{{\#if:|、{{\#if:|、{{\#if:|、}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}</span>」}}。有关-{zh-cn:字符;
zh-tw:字元;}-可能會[错误显示](../Page/亂碼.md "wikilink")，詳见[Unicode-{zh-cn:扩展;
zh-tw:擴充;}-汉字](../Page/Wikipedia:Unicode扩展汉字.md "wikilink")。</small>

</div>

<includeonly>{{\#ifeq:|yes||}}{{\#if:|}}{{\#if:
|}}</includeonly><noinclude></noinclude>

[](../Category/使用Unicode扩展汉字的条目.md "wikilink")
[](../Category/使用21个以上Unicode扩展汉字的条目.md "wikilink")
[Category:使用webfont的页面](../Category/使用webfont的页面.md "wikilink")