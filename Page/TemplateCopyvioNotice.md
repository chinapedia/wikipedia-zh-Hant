<noinclude></noinclude>{{<includeonly>subst:</includeonly>\#if:|

## 您創建的條目[:可能侵犯版權](:{{{1}}}.md "wikilink")

}} ![Copyright-problem.svg](Copyright-problem.svg
"Copyright-problem.svg")您好，您先前创建或编辑的页面「[:](:{{{1}}}.md "wikilink")」被认为[与他人的文字雷同](Wikipedia:CV.md "wikilink")。
維基百科非常歡迎您的編輯，但請先看看**[版權常見問題解答](WP:CRFAQ.md "wikilink")**和[何為侵犯著作權](WP:COPYVIO.md "wikilink")，以免犯了[常見的錯誤](WP:ACM.md "wikilink")。
**請不要修改疑似侵權頁面，或再重複創建相同內容的頁面**，版權驗證皆因維基百科十分重視版權。請前往[Example}}}了解情况](:{{{1.md "wikilink")。您亦可以與提出檢查的維基人進行溝通。维基百科只能接受[公有领域或兼容](公有领域.md "wikilink")[-{zh-hans:知识共享;
zh-hant:創作共用; zh-cn:知识共享; zh-tw:創用CC; zh-hk:共享創意; zh-mo:共享創意;}-
署名-相同方式共享3.0协议的内容](知识共享.md "wikilink")，若要繼續保留该內容在维基百科，您可：

  - [捐献版权](Wikipedia:捐赠版权材料/捐赠流程.md "wikilink") —— 适用于内容原创者或版权持有者；
  - [请求版权许可](Wikipedia:请求版权许可.md "wikilink") —— 适用于复制他人文字，且希望继续使用者；
  - **\[ 重写\]** —— 用您自己撰写的文字重新建立该条目；
  - [申诉](Wikipedia:CV.md "wikilink") —— 请在此页面对应条目下说明。

谢谢合作！
<small>幫助：[互助客棧](維基百科:互助客棧.md "wikilink") ·
[IRC聊天頻道](維基百科:IRC聊天頻道.md "wikilink") ·
[版权常见问题解答](Wikipedia:版权常见问题解答.md "wikilink") ·
[何為侵犯著作權](維基百科:侵犯著作權.md "wikilink")</small>
--\~\~<noinclude></noinclude>\~\~<noinclude>

## 模板重定向

\=

</noinclude>

[](../Category/用戶對話頁模板.md "wikilink")