{{\#ifeq:|G8||__NOINDEX__
<span id="delete-reason" style="display:none">}}</span>

</div>

\!</span>}}
{{\#if:|请对照以下删除理由判断本页是否具备执行快速删除的理据：|标记者没有提供理由，请对照以下删除说明补上合适的参数：

  -
    `{{Delete|'''快速删除理由'''}}`

快速删除的理由可以填寫左邊代碼，模板会自动显示相应的理由：}} {{\#invoke:Template:Delete|reasons}}
<strong>{{\#if:|[认为](../Page/User:{{{bot}}}.md "wikilink")}}本页可能符合[快速删除的標準而需删除](../Page/Wikipedia:快速刪除#準則.md "wikilink")，理由：</strong>
|2=

  - <strong>{{\#invoke:Template:Delete|input|F7|}}}}</strong>
  - <strong>本文件已於[維基共享資源提供](../Page/Commons:.md "wikilink")，链接為：[}}}](../Page/Commons:File:{{{1.md "wikilink")。</strong><includeonly>}}}}}}</includeonly>

|3=
<includeonly>{{\#invoke:Template:Delete|input|parent=}}</includeonly> }}

**請勿移除本模板**。如有異議，請在**本模板下方加入**，並儘快。

<small>|2=

  - 請[管理员注意将](../Page/Wikipedia:管理员.md "wikilink")[使用该图像的所有条目修改为维基共享资源上的图像](../Page/Special:Whatlinkshere/{{FULLPAGENAME}}.md "wikilink")，然后才能<span class="autotrigger">\[}}}}
    删除\]。</span>
  - |3=}}**其他**編者若認為本頁明顯不符合快速删除的标准，{{\#switch:{{\#invoke:Template:Delete|input|parent=|reasoncode=}}|G10|O1=|\#default=或者您已修正存在的問題，}}可去除此模板。</small>

<div id="speedy-delete" class="template-delete">

</div>

}}}}<noinclude>  </noinclude>