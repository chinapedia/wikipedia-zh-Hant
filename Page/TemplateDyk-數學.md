\<div font-size: small;"\>

  - **[哪一種三角形](../Page/直角三角形.md "wikilink")**和勾股定理有密切的關係？

<!-- end list -->

  - **[哪一個幾何定理](../Page/驴桥定理.md "wikilink")**和等腰三角形有關．也是幾何原本第一卷命題五的內容？

<!-- end list -->

  - **[哪一種反三角函數](../Page/反正切.md "wikilink")**可以用來計算直角坐标平面上已知斜率的直線與x軸的夾角？

<!-- end list -->

  - **[哪種多面體](../Page/正四面体.md "wikilink")**是面數最少的多面體？

<!-- end list -->

  - **[哪位數學家](../Page/婆羅摩笈多.md "wikilink")**首先提出有關0的計算規則？

<!-- end list -->

  - **[什么定理](../Page/柯西积分定理.md "wikilink")**说明复平面上的全纯函数沿着闭合曲线的积分等于0？

<noinclude>

  - **[什么公式](../Page/柯西积分公式.md "wikilink")**说明，复平面中任一个闭合区域上的全纯函数在区域内部的值完全取决于它在区域边界上的值？
  - **[什么函数](../Page/双周期函数.md "wikilink")**有两种周期？
  - **[为什么](../Page/三等分角.md "wikilink")**尺规作图中无法将60度角三等分？
  - **[哪一個數學理論](../Page/集合论.md "wikilink")**探討集合、成員關係等最基本數學概念？

<!-- end list -->

  - **[哪种性质](../Page/条件收敛.md "wikilink")**被用来形容收敛但不绝对收敛的无穷级数？
  - **[为什么](../Page/黎曼级数定理.md "wikilink")**把\(1 - \frac{1}{2} + \frac{1}{3} - \frac{1}{4} + \cdots\,\!\)里面的数重新排列一番再加起来，就能得到任何一个数，比如10000？
  - **[哪个数学](../Page/度分布.md "wikilink")**概念可以描述维基百科中随机条目的内链数量？
  - **[哪種多面體](../Page/正二十面体.md "wikilink")**是面數最多的正多面體？
  - **[哪一種數](../Page/光滑数.md "wikilink")**專指其質因數均小於某特定數值的正整數？

<!-- end list -->

  - **[哪種函數](../Page/模稜函數.md "wikilink")**為菲力浦·伍德沃德在1953年所提出，並且被廣泛地使用在時頻分析、訊號處理等領域上？
  - 测度论中，**[什么测度](../Page/σ-有限测度.md "wikilink")**使得全集能够表示为可数个有限测度的子集的并集？
  - **[什么公式](../Page/费曼-卡茨公式.md "wikilink")**将[抛物偏微分方程的解和](../Page/抛物偏微分方程.md "wikilink")[伊藤随机过程的条件期望联系起来](../Page/伊藤过程.md "wikilink")？
  - **[什么定理](../Page/四色定理.md "wikilink")**是首个借助计算机证明的著名数学定理？
  - 四人各写一张贺年卡互相赠送，有多少种赠送方法，这是一个**[什么数学问题](../Page/错排问题.md "wikilink")**？

<!-- end list -->

  - 数学中，平均曲率为零的曲面称为**[什么](../Page/极小曲面.md "wikilink")**？
  - **[什么原理](../Page/狄利克雷原理.md "wikilink")**将[帕松方程式的解刻画为某个势能函数的最小解](../Page/帕松方程式.md "wikilink")？
  - **[哪一種整數數列](../Page/幂数.md "wikilink")**的英文名稱恰好可看成「強大有力的數」？\*概率论中，**[如何刻画](../Page/随机变量的收敛.md "wikilink")**[随机变量序列的渐进变化趋势](../Page/随机变量.md "wikilink")？
  - “1+1”是**[哪个著名数学猜想](../Page/哥德巴赫猜想.md "wikilink")**的别称？
  - **[哪种吸引子](../Page/洛伦茨吸引子.md "wikilink")**是以美国数学、气象学家爱德华·诺顿·洛伦茨的名字命名的？

<!-- end list -->

  - 如果不定義[連續函數](../Page/連續函數.md "wikilink")，怎樣說明一個**集合**的**[連續性](../Page/連續統.md "wikilink")**？
  - [线性算子的](../Page/线性算子.md "wikilink")**[哪种运算](../Page/埃尔米特伴随.md "wikilink")**相当于[复数共轭](../Page/复数共轭.md "wikilink")？
  - 若兩個[三角形其中兩個](../Page/三角形.md "wikilink")[角及一條](../Page/角.md "wikilink")[邊都對應地相等的話](../Page/邊.md "wikilink")，這兩個[三角形稱為](../Page/三角形.md "wikilink")**[甚麼](../Page/全等三角形.md "wikilink")**？
    \***[哪個引理](../Page/舒爾引理.md "wikilink")**奠定了[有限群表示論的基石](../Page/有限群表示論.md "wikilink")？
  - [拓撲學中如何處理一個](../Page/拓撲學.md "wikilink")[空間中](../Page/拓撲空間.md "wikilink")**[從一點到另一點的曲線](../Page/道路_\(拓撲學\).md "wikilink")**？
  - [**为什么**](../Page/巴拿赫不动点定理.md "wikilink")[完备度量空间上的](../Page/完备度量空间.md "wikilink")[压缩映射有且只有一个](../Page/压缩映射.md "wikilink")[不动点](../Page/不动点.md "wikilink")？

<!-- end list -->

  - 自然形成的[肥皂膜](../Page/肥皂泡#数学性质.md "wikilink")**[哪个曲率](../Page/平均曲率.md "wikilink")**恰好为零？
  - **[哪一個](../Page/立方和.md "wikilink")**[恆等式可以計算出兩個](../Page/恆等式.md "wikilink")[立方體的總和](../Page/立方體.md "wikilink")？
  - [**为什么**](../Page/单调收敛定理.md "wikilink")[单调](../Page/单调函数.md "wikilink")、[有界的](../Page/有界函数.md "wikilink")[数列一定有](../Page/数列.md "wikilink")[极限](../Page/极限.md "wikilink")？
  - [**哪一个**以](../Page/维塔利集合.md "wikilink")[朱塞佩·维塔利命名的](../Page/朱塞佩·维塔利.md "wikilink")[集合是不可测的](../Page/集合.md "wikilink")？
  - [**为什么**闭区间上的](../Page/极值定理.md "wikilink")[连续函数一定有](../Page/连续函数.md "wikilink")[最大值和](../Page/最大值.md "wikilink")[最小值](../Page/最小值.md "wikilink")？

<!-- end list -->

  - **[三角矩阵](../Page/三角矩阵.md "wikilink")**是[三角形的吗](../Page/三角形.md "wikilink")？
  - [矩阵可以进行](../Page/矩阵.md "wikilink")**[指数运算](../Page/矩阵指数.md "wikilink")**吗？
  - [数学家](../Page/数学家.md "wikilink")[G·H·哈代](../Page/G·H·哈代.md "wikilink")**[於1940年写成的自传](../Page/一个数学家的辩白.md "wikilink")**的书名是什么？
  - 在计算时常被用到的**四舍五入**依据的是**[什么规则](../Page/数字修约规则.md "wikilink")**？
  - [微分几何的中心概念是](../Page/微分几何.md "wikilink")**[什么](../Page/流形.md "wikilink")**?</noinclude>

*' <span class="noprint plainlinksneverexpand">\[
<span style="color: #002bb8;" title="Edit">編輯</span>\]</span> |
[數學Dyk存档](../Page/Template:dyk-數學.md "wikilink") |
[创建新条目](../Page/Help:如何创建新页面.md "wikilink") |
[更多新条目...](../Page/Special:Newpages.md "wikilink")*'

<noinclude></noinclude>

[Category:辅助模板](../Category/辅助模板.md "wikilink")