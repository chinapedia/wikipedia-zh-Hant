<div id='column-dyk' class='knoblock {{{class|}}}'>

{{ Dyk/auto

`| 0 = `**[`哪一種偶氮染料`](../Page/氮紅.md "wikilink")**`在乾燥狀態下呈紅色至栗色，其`[`E编码為E`](../Page/E编码.md "wikilink")`129？`
`| p0 = Azorubine.svg`
`| t0 = chemistry`
`| 1 = `**[`哪一场学生运动`](../Page/清毒运动.md "wikilink")**`发生于1943年至1944年，由`[`中共南京工委领导学生团体开展的一项清除鸦片`](../Page/中国共产党南京市委员会.md "wikilink")`、捣毁砸烟馆、焚烧烟土的运动？`
`| p1 = 1944年《中国周报》贩毒巨犯曹玉成枪决.png`
`| t1 = history`
`| 2 = `**[`哪場戰役`](../Page/青島戰役.md "wikilink")**`是`[`第一次世界大戰發生在`](../Page/第一次世界大戰.md "wikilink")[`中國的唯一戰役`](../Page/北洋政府.md "wikilink")`？`
`| p2 = Bundesarchiv Bild 134-C1315, Tsingtau, Seesoldaten in Deckung.jpg`
`| t2 = war`
`| 3 = `[`台灣`](../Page/台灣.md "wikilink")**[`哪一位籃球教練`](../Page/劉孟竹_\(體育\).md "wikilink")**`曾於`[`2018`](../Page/2018年.md "wikilink")`-`[`19年間`](../Page/2019年.md "wikilink")`，透過承辦`[`大專校院籃球運動聯賽的八強賽事`](../Page/大專校院籃球運動聯賽.md "wikilink")`，公開呼籲國人改變`[`桃園市對於運動賽會的第一印象`](../Page/桃園市.md "wikilink")`？`
`| p3 = `
`| t3 = sport`
`| 4 = `**[`哪一位女性導演`](../Page/陳小娟.md "wikilink")**`曾在`[`第38屆香港電影金像獎中獲得`](../Page/第38屆香港電影金像獎.md "wikilink")`「`[`新晉導演`](../Page/香港電影金像獎新晉導演獎.md "wikilink")`」獎？`
`| p4 = `
`| t4 = Biography`
`| 5 = `[`南非於`](../Page/南非.md "wikilink")`1983年舉行了`**[`哪一場公投`](../Page/1983年南非憲法改革公民投票.md "wikilink")**`？`
`| p5 = South Africa 1983 and 1992 referendum results by province.svg`
`| t5 = political`
`| allimages = `

}}

</div>

<noinclude></noinclude>