<table border=0 cellpadding=5>

<tr bgcolor=#F0F0F0>

<td>

**TimeAxis =**

</td>

<td colspan=3 align=center>

**orientation:horizontal**

</td>

<td colspan=3 align=center>

**orientation:vertical**

</td>

</tr>

<tr bgcolor=#F0F0F0>

<td>

**Alignbars =**

</td>

<td>

<timeline> ImageSize = width:90 height:120 PlotArea = width:65 height:80
left:20 bottom:20 AlignBars = early

DateFormat = yyyy Period = from:1 till:5 TimeAxis =
orientation:horizontal ScaleMajor = unit:year increment:1 start:1

PlotData=

` color:red width:10`
` bar:A from:start till:end`
` bar:B from:start till:end`
` bar:C from:start till:end`
` bar:D from:start till:end`

TextData=

` pos:(20,110) fontsize:M text:Early`

</timeline>

</td>

<td>

<timeline> ImageSize = width:90 height:120 PlotArea = width:65 height:80
left:20 bottom:20 AlignBars = late

DateFormat = yyyy Period = from:1 till:5 TimeAxis =
orientation:horizontal ScaleMajor = unit:year increment:1 start:1

PlotData=

` color:red width:10`
` bar:A from:start till:end`
` bar:B from:start till:end`
` bar:C from:start till:end`
` bar:D from:start till:end`

TextData=

` pos:(20,110) fontsize:M text:Late`

</timeline>

</td>

<td>

<timeline> ImageSize = width:90 height:120 PlotArea = width:65 height:80
left:20 bottom:20 AlignBars = justify

DateFormat = yyyy Period = from:1 till:5 TimeAxis =
orientation:horizontal ScaleMajor = unit:year increment:1 start:1

PlotData=

` color:red width:10`
` bar:A from:start till:end`
` bar:B from:start till:end`
` bar:C from:start till:end`
` bar:D from:start till:end`

TextData=

` pos:(20,110) fontsize:M text:Justify`

</timeline>

<td>

<timeline> ImageSize = width:90 height:120 PlotArea = width:65 height:80
left:20 bottom:20 AlignBars = early

DateFormat = yyyy Period = from:1 till:5 TimeAxis = orientation:vertical
ScaleMajor = unit:year increment:1 start:1

PlotData=

` color:red width:10`
` bar:A from:start till:end`
` bar:B from:start till:end`
` bar:C from:start till:end`
` bar:D from:start till:end`

TextData=

` pos:(20,110) fontsize:M text:Early`

</timeline>

</td>

<td>

<timeline> ImageSize = width:90 height:120 PlotArea = width:65 height:80
left:20 bottom:20 AlignBars = late

DateFormat = yyyy Period = from:1 till:5 TimeAxis = orientation:vertical
ScaleMajor = unit:year increment:1 start:1

PlotData=

` color:red width:10`
` bar:A from:start till:end`
` bar:B from:start till:end`
` bar:C from:start till:end`
` bar:D from:start till:end`

TextData=

` pos:(20,110) fontsize:M text:Late`

</timeline>

</td>

<td>

<timeline> ImageSize = width:90 height:120 PlotArea = width:65 height:80
left:20 bottom:20 AlignBars = justify

DateFormat = yyyy Period = from:1 till:5 TimeAxis = orientation:vertical
ScaleMajor = unit:year increment:1 start:1

PlotData=

` color:red width:10`
` bar:A from:start till:end`
` bar:B from:start till:end`
` bar:C from:start till:end`
` bar:D from:start till:end`

TextData=

` pos:(20,110) fontsize:M text:Justify`

</timeline>

</td>

<tr>

</table>

<noinclude> </noinclude>

[Category:辅助模板](../Category/辅助模板.md "wikilink")
[Category:使用簡易時間線語法的模板](../Category/使用簡易時間線語法的模板.md "wikilink")