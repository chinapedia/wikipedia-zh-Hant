}|} }}}} }}} | subheader = }}}

| image =
{{\#invoke:InfoboxImage|InfoboxImage|image=}}}|size=}}}|sizedefault=frameless}}

| headerstyle = background-color:\#99ccff

| header1 = {{\#if:}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}|比賽資訊}}

| label2 = 圈數 | data2 = }}}

| label3 = 賽道長度 | data3 = {{\#if:}}}

` | ``}}} 公里 {{#if:``}}}|（``}}} 英里）}}`
` | {{#if:``}}}|``}}} 英里}}`
` }}`

| label4 = 比賽長度 | data4 = {{\#if:}}}

` | ``}}} 公里 {{#if:``}}} |（``}}} 英里）}}`
` | {{#if:``}}} | ``}}} 英里 }}`
` }}`

| label5 = 舉辦次數 | data5 = }}}

| label6 = 首次舉辦 | data6 = {{\#if:}}}|{{\#ifexist:}}} }}}}}|\[\[}}}
}}}}}}}

| label7 = 最後舉辦 | data7 = {{\#if:}}} |{{\#ifexist:}}} }}}}}|\[\[}}}
}}}}}}}

| label8 =  | data8 = {{\#if:}}} |}}} }}

| label9 =  | data9 = {{\#if:}}} |}}} }}

| header10 = {{\#if:}}}

` |上次紀錄（{{#ifexist:``}}} ``}}}}}|[[``}}} `` }}}}}年）  ``} | ``}}} }}`
`    `
`    | header3 = 頒獎台`
`    | data4 = `

<div class="plainlist" style="text-align:left;">

  - {{\#if:}}} | } | }}} | }}} }}}}
  - {{\#if:}}} | } | }}} | }}} }}}}
  - {{\#if:}}} | } | }}} | }}} }}}}

</div>

`    | header5 = 最快圈速`
`    | data6 = ``} | ``}}} | ``}}} }}`

`}} }}`

}}<noinclude>  </noinclude>

[Category:一級方程式模板](../Category/一級方程式模板.md "wikilink")
[赛](../Category/体育信息框模板.md "wikilink")