}||持续时间|}} <u></u><u></u>{{\#if:|（）|}}。'''
當這個-{zh:訊息;zh-hans:消息;zh-hant:訊息}-框出現時，请暂时勿编辑此条目，加入這個-{zh:訊息;zh-hans:消息;zh-hant:訊息}-框的使用者會被記錄在编辑\[
历史\]（使用完畢或暂时不再编辑时，请刪除这个標籤。）

<span style="font-size: 90%">此-{zh:訊息;zh-hans:消息;zh-hant:訊息}-框是為了避免[编辑冲突](../Page/Help:编辑冲突.md "wikilink")，此次编辑期間結束後**請移除此-{zh:訊息;zh-hans:消息;zh-hant:訊息}-框**，以便讓其他使用者繼續編輯維基百科內容。</span>

{{\#ifeq:}}}||<span style="font-size: 90%">致其他編輯者：如<span title="{{TimeSinceLastEdit}}"><u>条目最后版本提交时间</u>（前，）</span>距今超逾<u></u>，則可**自行移除此-{zh:訊息;zh-hans:消息;zh-hant:訊息}-框**。</span>}}
}}<includeonly>{{\#ifeq:|true||}\>7200|}}
}}}}}}}</includeonly><noinclude></noinclude>

[Category:被擱置的條目](../Category/被擱置的條目.md "wikilink")