|              |
| ------------ |
|              |
| ****         |
|              |
|              |
|              |
| **黨魁**       |
|              |
| **創黨時間**     |
| **總部**       |
|              |
| **政治信仰**     |
| **政治路線**     |
| **國際聯繫**     |
| **歐洲聯繫**     |
| **歐洲議會所屬派系** |
| **象徵顏色**     |
|              |
| **網址**       |
|              |
| **請參見**      |

<noinclude> [cy:Nodyn:Gwybodlen Plaid Wleidyddol
Brydeinig](cy:Nodyn:Gwybodlen_Plaid_Wleidyddol_Brydeinig.md "wikilink")
[en:Template:Infobox political
party](en:Template:Infobox_political_party.md "wikilink") </noinclude>

[欧](../Category/政黨信息框模板.md "wikilink")