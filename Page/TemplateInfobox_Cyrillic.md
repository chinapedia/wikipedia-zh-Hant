{{\#if:}}}|}}}}}</includeonly> |headerstyle = background:\#ccf;
|header1style = font-size:120%;

|header1 = 西里尔字母}}}} |label2 =  |data2 =
<span style="font-family:FreeSerif,Georgia,'Times New Roman','Nimbus Roman No9 L','Century Schoolbook L','Trebuchet MS','URW Bookman L','URW Chancery L','URW Palladio L',Teams,serif; font-size:large;">}}}}</span>
|label3 = [小寫字母](小寫字母.md "wikilink") |data3 =
<span style="font-family:FreeSerif,Georgia,'Times New Roman','Nimbus Roman No9 L','Century Schoolbook L','Trebuchet MS','URW Bookman L','URW Chancery L','URW Palladio L',Teams,serif; font-size:large;">}}}}</span>
|label4 =  |data4 = {{\#switch:}}}}| |а |ӑ |ӓ |ӕ |в |г |ѓ |ӷ |ӻ |д |и |й
|ѝ |ӣ |ҋ |ӥ |п |ԥ |ҧ |т |ҭ |ԏ |ѣ
|ҵ=<span style="font-family:FreeSerif,Georgia,'Times New Roman','Nimbus Roman No9 L','Century Schoolbook L','Trebuchet MS','URW Bookman L','URW Chancery L','URW Palladio L',Teams,serif; font-size:large;"><i>}}}}</i></span>
}} |label5 = {{\#if:|<small>[Unicode編碼](Unicode.md "wikilink")</small>}}
|data5 =
{{\#if:|<span style="font-size:90%;text-align:left;vertical-align:middle;">大寫：\[[http://www.fileformat.info/info/unicode/char/](http://www.fileformat.info/info/unicode/char/.md)/index.htm
U+\]
小寫：\[[http://www.fileformat.info/info/unicode/char/](http://www.fileformat.info/info/unicode/char/.md)/index.htm
U+\]}}</span> |label6 =  |data6 =
{{\#if:|<span style="font-size:90%;">代表</span>}} |header7 =
[西里尔字母](西里尔字母.md "wikilink") |header8 =

<div style="font-weight:normal;background:#f0f0f0;">

[斯拉夫语族字母](斯拉夫语族.md "wikilink")

</div>

|data9 =

<div lang="ru" xml:lang="ru">

</div>

|header10 =

<div style="font-weight:normal;background:#f0f0f0;">

非[斯拉夫语族字母](斯拉夫语族.md "wikilink")

</div>

|data11 =

<div lang="ru" xml:lang="ru">

</div>

|header12 =

<div style="font-weight:normal;background:#f0f0f0;">

[曾经使用的字母](早期西里尔字母.md "wikilink")

</div>

|data13 =

<div lang="ru" xml:lang="ru">

<noinclude>

</noinclude>

[Category:西里尔字母模板](../Category/西里尔字母模板.md "wikilink")
[语言](../Category/语言信息框模板.md "wikilink")