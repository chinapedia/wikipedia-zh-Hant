| subbox =  | bodyclass = biography vcard | bodystyle = width:24em |
title = }}} | titleclass = fn | image =
{{\#invoke:InfoboxImage|InfoboxImage|image=}}}|size=|sizedefault=frameless|alt=}}
| caption =  | headerstyle = background-color: gainsboro

| label1 = 出生 | data1 = }|}}} }} | label2 = 逝世 | data2 =  | header3 =
{{\#if:|世界[一級方程式錦標賽職業生涯](一級方程式.md "wikilink")}}

| label4 = [國籍](FIA超級駕駛執照#車手國籍.md "wikilink") | data4 = }}} | label5 =
活躍年代 | data5 =  | label6 = 車隊 | data6 = }}} | label10 = 2019年車隊 |
class10 = org | data10 =  | label11 = 2019年車號 | data11 =  | label14 = 車號
| data14 =

| label15 = 出賽次數 | data15 =  | label16 =
[世界冠軍](F1世界車手冠軍列表.md "wikilink") | data16 =  |
label17 = [分站冠軍](一級方程式賽車分站賽冠軍列表.md "wikilink") | data17 =  | label18 =
頒獎台 | data18 =  | label19 = 生涯積分 | data19 =  | label20 =
[桿位](桿位.md "wikilink") | data20 =  | label21 =
[最快圈速](最快圈速.md "wikilink") | data21 =  | label22 = 首次出賽
| data22 =  | label23 = 首次分站冠軍 | data23 =  | label24 = 最後分站冠軍 | data24 =
 | label25 = {{\#if:|最近出賽|最後出賽}} | data25 =  | label26 =
{{\#if:|[2011}}}年世界一級方程式錦標賽排名]({{{Last_season.md "wikilink")}} | data26
=

| header27 = {{\#if:|[英國一級方程式系列賽職業生涯](英國一級方程式系列賽.md "wikilink")}} |
label28 = 活躍年代 | data28 =  | label29 = 出賽次數 | data29 =  | label30 = 世界冠軍
| data30 =  | label31 = 分站冠軍 | data31 =  | label32 = 頒獎台 | data32 =  |
label33 = 生涯積分 | data33 =  | label34 = [桿位](桿位.md "wikilink") | data34 =
 | label35 = [最快圈速](最快圈速.md "wikilink") | data35 =

| data36 = }}}}}}}}} | data37 = }}}}}} | data38 = }}}}}} | data39 =
}}}}}} | data40 = }}}}}}

| below = {{\#if:}}}|最後更新於}}}.}} }}<noinclude>  </noinclude>