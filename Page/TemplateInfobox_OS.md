{{ \#invoke:Unsubst||date=__DATE__|$B=
}|}}}|<includeonly></includeonly>}} | titleclass = summary | subheader =
{{\#if:}}}|}}}操作系统的一个版本}} | image =
{{\#invoke:InfoboxImage|InfoboxImage|image=|size=|sizedefault=250px|alt=}}
| caption =  | image2 =
{{\#if:|}}{{\#invoke:InfoboxImage|InfoboxImage|image=|size=|sizedefault=300px|alt=}}
| caption2= {{\#if:|}} | labelstyle =  | datastyle =  | label1 =
[开发者](软件设计师.md "wikilink") | data1 = }} | label2 =
[编程语言](编程语言.md "wikilink") | data2 = }}}}}}}}}}} |
label3 = 作業系統家族 | data3 =  | label4 = 運作狀態 | data4 = }}} | label5 = 源码模式
| data5 = }}} | label6 = 初始版本 | data6 = {{\#if:}}}}}}}}}}}}||}} | label7
= [發放給生產商](軟件版本週期#RTM.md "wikilink") | data7 =
}}}}}}}}}{{\#if:}}}|{{\#tag:ref|}}}}}}} | label8 =
[正式发售日期](軟件版本週期#General_availability.md "wikilink")
| data8 = }}}{{\#if:}}}|{{\#tag:ref|}}}}}}} | label9 =
\[\[軟件出版週期|{{\#if:}}}}}}}}}|}}}}}}}}}{{\#if:}}}}}}}}}|<small>/
}}}}}}}}}</small>}}{{\#if:}}}|{{\#tag:ref|}}}}}}}|{{\#ifexist:Template:Latest
stable software release/}}}}}|}}}}}}} }} | label10 =
\[\[軟件出版週期|{{\#if:}}}}}}}}}|}}}}}}}}}{{\#if:}}}}}}}}}|<small>/
}}}}}}}}}</small>}}{{\#if:}}}|{{\#tag:ref|}}}}}}}|{{\#ifexist:Template:Latest
preview software release/}}}}}|}}}}}}} }} | label11 = 市場取向 | data11 =
}}} | label12 = 支持的[语言](自然语言.md "wikilink") | data12 =
{{\#if:|种|}}}}}}}} | data13 = {{\#if:|{{\#if:}}}}}}|}}}} | label14 =
更新方式 | data14 = }}}}}} | label15 = [软件包管理系统](软件包管理系统.md "wikilink")
| data15 = }}} | label16 = 支援平台 | data16 = }}} | label17 =
[内核类别](内核.md "wikilink") | data17 = }}} | label18 =
[使用者空間](使用者空間.md "wikilink") | data18 =  | label19 = 施影響於 |
data19 =  | label20 = 受影響於 | data20 = }}} | label21 =
默认[用户界面](用户界面.md "wikilink") | data21 =  | label22 =
[许可证](软件许可证.md "wikilink") | data22 = }} | label23 = 前一代 | data23 = }}}
| label24 = 后一代 | data24 = }}} | label25 = 官方網站 | data25 = {{\#if:

`                 |{{#ifeq:``|hide||`` }}`
`                 |{{#if:{{#property:P856}}`
`                    |`
`                  }}`
`              }}`

| header26 = {{\#if:}}}|支援状态}} | data27 = }}} | header28 =
{{\#if:}}}|有关这系列的条目}} | below = }}} | belowstyle = text-align:
left }} }}<noinclude> </noinclude>