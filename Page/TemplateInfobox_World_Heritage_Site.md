|aboveclass=fn org |abovestyle=background-color:\#5b92e5;
color:\#fff;font-size: 125%; padding: 2px; height: 36px; line-height:
36px; |above= |subheaderclass=category
|subheaderstyle=font-weight:bolder;
|subheader=[联合国教科文组织认定的](联合国教育、科学及文化组织.md "wikilink")[世界遗产](世界遗产.md "wikilink")

|image={{\#invoke:InfoboxImage|InfoboxImage|image=}}}}}}|size=}}}}}}}}}|sizedefault=frameless|alt=}}}}}
|caption=}}}

|headerstyle=background-color: \#B5D8EF;

|header1={{\#if:|正式名稱}} |label2= |data2={{\#if:|}} |label3=
|data3={{\#if:|}} |label4= |data4={{\#if:|}} |label5= |data5={{\#if:|}}
|label6= |data6={{\#if:|}} |header7=基本資料
|label8=[國家](各國世界遺產數列表.md "wikilink")
|class8=label |data8=}}}
|label9=[地区\*\*](http://whc.unesco.org/en/list/?search=&search_by_country=&type=&media=&region=&order=region.md)
|class9=label |data9={{\#if:|[}}}世界遗产列表]({{{Region.md "wikilink")}}
|label10=编号
|data10={{\#if:|\[[http://whc.unesco.org/en/list/](http://whc.unesco.org/en/list/.md)
\]}} |label11= |class11=category |data11={{\#if:|{{\#if:|複合遺產 |文化遺產 }}
|自然遺產 }} |label12=[評定標準](世界遗产#世界遗产评定標準.md "wikilink")
|class12=category |data12={{\#if:|{{\#if:|文化遺產
自然遺產 |文化遺產 }} |自然遺產 }} |label13=備考 |data13= |header14=註冊歷史 |label15=註冊年份
|data15=}}}}}}{{\#if:|（）}} |label16=拓展年份 |data16= |label17=濒危年份 |data17=
|header18={{\#if: |其他}} |label19=[IUCN編號](國際自然保護聯盟.md "wikilink")
|data19= |header20={{\#if:}}}|地圖}}
|data21={{\#invoke:InfoboxImage|InfoboxImage|image=|size=|sizedefault=200px|alt=的位置}}
|data22
={{\#if:|{{\#if:}}}}}}||width=|border=infobox|caption={{\#if:||}}在{{\#invoke:Location
map|data||name}}的位置 }} }} }} |}} |label23=[经纬度](经纬度.md "wikilink")
|data23=

|header30={{\#if: |\[ UNESCO的记录\]}} |below=<includeonly>\*
[名稱依據世界遺產名錄註冊。](http://whc.unesco.org/en/list.md)
\*\*
[地區以聯合國教科文組織所劃分为准。](http://whc.unesco.org/en/list/?search=&search_by_country=&type=&media=&region=&order=region.md)</includeonly>
|belowstyle=line-height:150%; padding-top:0.8em; border-top: 1px \#aaa
solid; text-align:left; font-weight:lighter;color:\#555;margin:0.5em;
}}<noinclude>  </noinclude>