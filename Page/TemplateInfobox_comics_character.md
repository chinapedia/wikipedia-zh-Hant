}|alt=|sizedefault=250px| maxsize=250px}}

| caption = <small>{{\#if:||{{\#if:|| }}}}</small>

| header1 = {{\#if:    |出版-{zh-tw:資訊;zh-cn:信息;}-}}

| label2 = [出版商](:分类:漫画出版社.md "wikilink") | data2 =  | label3 =
[首次登場](首次登場.md "wikilink") | data3 =  | label4 =
[创作者](:分类:漫画创作者.md "wikilink") | data4 =

| header5 = {{\#if:              |故事-{zh-tw:資訊;zh-cn:信息;}-}}

| label6 = {{\#if:|

` 全名|`
` {{#if:``|`
`   真名`
` }}`

}} | data6 =  | label7 = 种族 | data7 =  | label8 = 原居地 | data8 =  |
label9 = 所属团队 | data9 = {{\#if:   |

` ``{{#if: `` |`
`   , `
` }}|`
` `

}} | label10 = 伙伴关系 | data10 =  | label11 = 配角 | data11 =  | label12 =
重要别名 | data12 =  | label13 = 能力 | data13 =

| header14= {{\#if:|改编到其他媒体中的故事信息}}

| label15 = {{\#if:|

` 全名|`
` {{#if:``|`
`   真名`
` }}`

}} | data15 =  | label16 = 所属团队 | data16 =  | label17 = 伙伴关系 | data17 =
| label18 = 重要别名 | data18 =  | label19 = 能力 | data19 =

}} {{\#if:|

`    {{#if:``|`
`      {{#if:``|`
`        {{#ifeq:``|超级|`
`          {{#if:``|`
`            `[`}}}超级英雄`](分类:{{{subcat.md "wikilink")`|}}`
`          {{#if:``|`
`            `[`}}}超级反派`](分类:{{{subcat.md "wikilink")`|}}|`
`          `[`}}}``   ``角色`](分类:{{{subcat.md "wikilink")
`      }}|`
`      {{#if:``|`
`        `[`}}}`](分类:{{{altcat.md "wikilink")`|`
`        {{#if:``||`
`          [[分类:漫畫角色|``|}} {{#if:``|}} {{#if:``|}} {{#if:``|}}
{{#if:``|}} {{#if:``|}} }}`<noinclude>

</noinclude>

[}}}](../Category/{{{addcharcat1.md "wikilink")
[}}}](../Category/{{{addcharcat2.md "wikilink")
[}}}](../Category/{{{addcharcat3.md "wikilink")
[}}}](../Category/{{{addcharcat4.md "wikilink")
[}}}](../Category/{{{addcharcat5.md "wikilink")
[}}}](../Category/{{{addcharcat6.md "wikilink")