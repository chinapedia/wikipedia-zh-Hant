}|}}}|<includeonly></includeonly>}} | titleclass = summary | labelstyle
= white-space: nowrap; | image = {{\#if:

` | {{#invoke:InfoboxImage | InfoboxImage | image=`` | title=`` | size=``}}} | sizedefault=300x64px | maxsize=300x64px | alt=``}}} }} `
` | {{#invoke:InfoboxImage | InfoboxImage | image=`` | title=`` | size=``}}} | alt=``}}} }}`

}} | caption =  | image2 = {{\#if:|{{\#if:|}}}}{{\#invoke:InfoboxImage |
InfoboxImage | image= | size=}}} | sizedefault=300x0px | maxsize=300x0px
| alt=}}} | title= }} | caption2 = {{\#if:|{{\#if:|}}}} | label1 = 其他名称
| data1 =  | label2 = [原作者](软件设计.md "wikilink") | data2 =  | label3 =
[開發者](軟體開發.md "wikilink") | data3 =  | label4 = 初始版本 | data4 =  |
rowstyle5 = display: none; | data5 = {{\#ifeq:|stacked

`|`
` |`
` |``}}}`
` |``}}}`
` |``}}}`
` |``}}}`
` }}`
`|`
` |`
` |``}}}`
` |``}}}`
` |``}}}`
` |``}}}`
` }}`
`}}`

| label6 = [源代码库](仓库_\(版本控制\).md "wikilink") | data6 = {{\#if:

`                 |`
`                 |{{#if: {{#property:P1324}} | {{#ifeq: `` | none | `` | `` }} }}`
`              }}`

| label7 = [编程语言](编程语言.md "wikilink") | data7 = }}} | label8 =
[操作系统](操作系统.md "wikilink") | data8 = }}} | label9 =
[系統平台](系統平台.md "wikilink") | data9 =  | label10 = 内置于 | data10 =
}}} | label11 = 引擎 | data11 = }}} | label12 = 取代 | data12 =  | label13 =
被取代 | data13 =  | label14 = 服务名称 | data14 =  | label15 =
[文件大小](文件大小.md "wikilink") | data15 =  | label16 =
[标准](标准.md "wikilink") | data16 =  | label17 = 语言 | data17 =
{{\#if:|种语言|}} | data18 = {{\#if:|{{\#if:|}}}} | label19 = 类型 | data19 =
 | label20 = [许可协议](软件许可证.md "wikilink") | data20 = }}} | label21 =
[Alexa排名](Alexa_Internet.md "wikilink") | data21 = {{\#if:|}} | label22
= 网站 | data22 = {{\#if:

`                 |{{#ifeq:``|hide||`` }}`
`                 |{{#if:{{#property:P856}}`
`                    |``}}|`
`                  }}`
`              }}`

| label23 = 数据截至 | data23 =  }}{{\#invoke:Check for unknown parameters |
check | showblankpositional=1 | unknown =  | preview =
页面使用了[Template:Infobox
software不存在的参数](Template:Infobox_software.md "wikilink")"_VALUE_"
| title | name | logo | logo size | logo_size | logo alt | logo_alt |
logo caption | screenshot | screenshot size | screenshot_size |
screenshot alt | screenshot_alt | caption | collapsible | author |
developer | engine | engines | released | discontinued | ver layout |
latest release version | latest_release_version | latest release date
| latest_release_date | latest preview version |
latest_preview_version | latest preview date | latest_preview_date |
repo | programming language | programming_language | operating system |
operating_system | other_names | platform | included_with | included
with | replaces | replaced_by | service_name | size | language |
language count | language footnote | genre | license | licence | alexa |
website | standard | AsOf | bodystyle }}<noinclude>  </noinclude>