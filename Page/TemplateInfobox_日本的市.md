| native_name_lang = ja | settlement_type = \#if:||}} |
translit_lang1 = \#if:}}}|日文}} | translit_lang1_type =
\#if:|[日文](日文.md "wikilink")}} | translit_lang1_info = }} |
translit_lang1_type1 = \#if:|[平假名](平假名.md "wikilink")}} |
translit_lang1_info1 = }} | translit_lang1_type2 =
\#if:}}}|[罗马字](日语罗马字.md "wikilink")}} |
translit_lang1_info2 = }}} | image_skyline =  | imagesize =  |
image_caption =  | image_flag = }} | flag_link = 市町村旗 | flag_type =
市旗 | image_shield = }} | shield_type = {{\#if:||市章}} | shield_size
= 70px | nickname = | motto = | image_map = }} | map_caption =
\#if:|在的位置}} | image_map1 = | map_caption1 = | pushpin_map =
Japan | pushpin_relief = | pushpin_label_position = |
pushpin_map_alt = | pushpin_map_caption = 在日本的位置 | coordinates =
{{\#if:|{{\#invoke:Coordinates|coordinsert||region:JP|type:city}}}} |
coordinates_footnotes = | subdivision_type =
[国家](世界政區索引.md "wikilink") | subdivision_name =  |
subdivision_type1 = [地方](日本地理分区.md "wikilink") | subdivision_name1 =
| subdivision_type2 = [都道府縣](日本行政區劃.md "wikilink") | subdivision_name2
= {{\#if:|[]({{{都道府縣}}}.md "wikilink")}}{{\#if:|
[}}}]({{_#if:_{{{支廳連結.md "wikilink") }} | subdivision_type3 =
[郡](郡_\(日本\).md "wikilink") | subdivision_name3 =  |
subdivision_type4 =  | subdivision_name4 =  | established_title = |
established_date = | extinct_title = | extinct_date = | founder = |
named_for = | government_footnotes = | leader_party = | leader_title
= 市長 | leader_name = {{\#if:|
<small>（現任任期至：）</small>}} | leader_title1 = | leader_name1 = |
total_type = | unit_pref = metric_only | area_magnitude = |
area_footnotes =  | area_total_km2 =  | area_land_km2 = |
area_note = {{\#switch:|有=<span style="font-size:80%;">（部份邊界未確定）</span>
| }} | population_footnotes = | population_total =  |
population_as_of =  | population_density_km2 =  | population_note =
| timezone1 = [日本標準時間](日本標準時間.md "wikilink") | utc_offset1 = +9 |
area_code_type = [地方公共團體編號](日本地方公共團體編號.md "wikilink") | area_code =
| demographics_type1 = \#if:|象徵}} | demographics1_title1 = 市樹 |
demographics1_info1 =  | demographics1_title2 = 市花 |
demographics1_info2 =  | demographics1_title3 = 市鳥 |
demographics1_info3 =  | demographics1_title4 = 市鱼 |
demographics1_info4 =  | demographics1_title5 = 市歌 |
demographics1_info5 =  | demographics1_title6 =  |
demographics1_info6 =  | blank_name_sec1 = \#if:|[-{zh-cn:邮政编码;
zh-tw:郵遞區號; zh-hk:郵區編號; zh-sg:邮递区号;}-](郵遞區號.md "wikilink")}} |
blank_info_sec1 =  | blank1_name_sec1 = 市役所地址 | blank1_info_sec1 =
 | blank2_name_sec1 = 電話號碼 | blank2_info_sec1 = +81- |
blank3_name_sec1 = 市議員數 | blank3_info_sec1 =  | blank4_name_sec1 =
 | blank4_info_sec1 =
{{\#if:{{\#property:P3225}}|\[[http://www.houjin-bangou.nta.go.jp/henkorireki-johoto.html?selHouzinNo=](http://www.houjin-bangou.nta.go.jp/henkorireki-johoto.html?selHouzinNo=.md){{\#property:P3225}}
{{\#property:P3225}}\]}} | website =  | footnotes =
}}<includeonly>|Category:{{\!}}\]\]}} </includeonly> {{\#invoke:Check
for unknown parameters|check|unknown=|preview =
页面使用了[Template:Infobox_日本的市不存在的参数](Template:Infobox_日本的市.md "wikilink")"_VALUE_"|ignoreblank=y|自治體名|日文原名|類型|羅馬字拼音|羅馬拼音|平假名|圖像|圖像大小|圖像說明|市旗|市章|圖章類型|地圖|都道府縣|座標|coordinates|地方|支廳|支廳連結|郡|自治體|市長|卸任日期|面積|邊界未定|人口|統計時間|人口密度|郵遞區號|編號|樹|花|鳥|魚|歌|其他象徵|其他象徵物|所在地|議員數|電話號碼|外部連結|註記}}<noinclude>
 </noinclude>