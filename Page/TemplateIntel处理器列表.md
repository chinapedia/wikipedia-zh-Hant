{{ navbox | name = Intel处理器列表 | title =
[Intel處理器](英特爾微處理器列表.md "wikilink")

| listclass = hlist | group1 = 已停產 | list1 = \*  | group2 = 現有產品 | list2
= \*

| group3 = 未來產品 | list3 =

  -
  - [Moorestown](Moorestown.md "wikilink")

| group4 = 平台 | list4 =

  - [Centrino](迅馳.md "wikilink")
  - [Centrino2](迅馳.md "wikilink")
  - [Centrino Duo](迅馳#Napa.E5.B9.B3.E5.8F.B0.md "wikilink")
  - [Centrino Pro](迅馳#Santa_Rosa.E5.B9.B3.E5.8F.B0.md "wikilink")
  - [vPro](博锐.md "wikilink")
  - [Viiv](欢悦.md "wikilink")
  - [MID](行動聯網裝置.md "wikilink")

| group5 = 微架構 | list5 = \*

| group6 = 列表 | list6 =

  - [處理器插座](CPU插座.md "wikilink")
  - [處理器](英特爾微處理器列表.md "wikilink")
  - [處理器代號](英特爾處理器代號列表.md "wikilink")
  - [PCH](PCH.md "wikilink")</br>[Atom](Intel_Atom處理器列表.md "wikilink")
  - [Celeron](Intel_Celeron處理器列表.md "wikilink")
  - [Core](Intel_Core#處理器列表.md "wikilink")
      - [2](Intel_Core_2處理器列表.md "wikilink")
      - [i3](Intel_Core_i3處理器列表.md "wikilink")
      - [i5](Intel_Core_i5處理器列表.md "wikilink")
      - [i7](Intel_Core_i7處理器列表.md "wikilink")
      - [i9](Intel_Core_i9#處理器列表.md "wikilink")
      - [M](Intel_Core_M處理器列表.md "wikilink")
  - [Itanium](Intel_Itanium處理器列表.md "wikilink")
  - [Pentium](Intel_Pentium處理器列表.md "wikilink")
      - [II](Intel_Pentium_II處理器列表.md "wikilink")
      - [III](Intel_Pentium_III處理器列表.md "wikilink")
      - [4](Intel_Pentium_4處理器列表.md "wikilink")
      - [D](Intel_Pentium_D處理器列表.md "wikilink")
      - [Dual-Core](Pentium_Dual-Core#處理器列表.md "wikilink")
      - [M](Intel_Pentium_M#處理器列表.md "wikilink")
  - [Xeon](Intel_Xeon處理器列表.md "wikilink")
      - [Phi](Xeon_Phi.md "wikilink")

|group7 = 相關 |list7 =

  - [晶片組](英特爾晶片組列表.md "wikilink")

  - [PCH](平台路徑控制器.md "wikilink")

  -
  - [ICH](I/O路徑控制器.md "wikilink")

  -
  -
  -
  - [GMA](Intel_GMA.md "wikilink")

  - [HD Graphics](Intel_HD_Graphics.md "wikilink")

}}<noinclude> </noinclude>

[Category:硬件模板](../Category/硬件模板.md "wikilink")
[Category:科技导航模板](../Category/科技导航模板.md "wikilink")
[Category:電腦模板](../Category/電腦模板.md "wikilink")