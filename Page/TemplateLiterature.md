|                                                                                                                     |
| ------------------------------------------------------------------------------------------------------------------- |
| **[文學](文学.md "wikilink")**                                                                                          |
| ![Liji2_no_bg.png](Liji2_no_bg.png "Liji2_no_bg.png")                                                             |
| <small>[散文](散文.md "wikilink") - [韻文](韻文.md "wikilink") - [駢文](駢文.md "wikilink")</small>                             |
| <small>[詩](诗歌.md "wikilink") - [詞](词_\(文学\).md "wikilink") - [曲](元曲.md "wikilink") - [歌詞](歌詞.md "wikilink")</small> |
| <small>[小說](小说.md "wikilink")（[長篇小說](長篇小說.md "wikilink")） - [戲劇](戏剧.md "wikilink") - [傳記](傳記.md "wikilink")</small> |
| <small>[兒童文學](儿童文学.md "wikilink") - [文學流派](文学流派.md "wikilink")</small>                                              |
| <small>[西方文學理論](西方文學理論.md "wikilink") - [文學史](文学史.md "wikilink")</small>                                            |
| **地域文學**                                                                                                            |
| <small>[古希腊文学](古希腊文学.md "wikilink") - [古罗马文学](古罗马文学.md "wikilink")</small>                                          |
| <small>[古埃及文學](古埃及文學.md "wikilink") - [爱尔兰文学](爱尔兰文学.md "wikilink")</small>                                          |
| <small>[美国文学](美国文学.md "wikilink") - [英國文學](英國文學.md "wikilink")</small>                                              |
| <small>[義大利文學](義大利文學.md "wikilink") - [非洲文學](非洲文學.md "wikilink")</small>                                            |
| <small>[西班牙文學](西班牙文学.md "wikilink") - [印度文學](印度文學.md "wikilink")</small>                                            |
| <small>[中文文学](中国文学.md "wikilink") - [臺灣文學](台灣文學.md "wikilink")</small>                                              |
| <small>[香港文學](香港文學.md "wikilink") - [朝鲜文学](朝鲜文学.md "wikilink")</small>                                              |
| <small>[-{zh-hans:朝鲜;zh-hant:北韓}-文学](朝鮮民主主義人民共和國文學.md "wikilink") - [韓國文學](韓國文學.md "wikilink")</small>              |
| <small>[德國文學](德國文學.md "wikilink") - [伊朗文学](波斯文学.md "wikilink")</small>                                              |
| <small>[日本文學](日本文學.md "wikilink") - [法國文學](法语文学.md "wikilink")</small>                                              |
| <small>[拉美文学](拉丁美洲文学爆炸.md "wikilink") - [俄国文学](俄国文学.md "wikilink")</small>                                          |
| **作家**                                                                                                              |
| <small>[小說家](小說家.md "wikilink") - [隨筆家](隨筆家.md "wikilink")</small>                                                  |
| <small>[剧作家](剧作家.md "wikilink") - [評論家](評論家一覧.md "wikilink")</small>                                                |
| <small>[詩人](詩人.md "wikilink") - [詞人](詞人.md "wikilink")</small>                                                      |
| <small>[作曲家](作曲家.md "wikilink") - [填詞人](填詞人.md "wikilink")<s/mall>                                                  |
| <small>[散文家](散文家.md "wikilink") - [網路作家](网络作家.md "wikilink")</small>                                                |
| **分類**                                                                                                              |
| <small>[文學](:../Category/文學.md "wikilink") - [各國文學](:../Category/各國文學.md "wikilink")</small>                        |
| <small>[文學類型](:../Category/文學類型.md "wikilink") - [文學體裁](:../Category/文學體裁.md "wikilink")</small>                    |
| <small>[作家](:../Category/作家.md "wikilink") - [登場人物](:../Category/文学角色.md "wikilink")</small>                        |
| <small>[文学流派](:../Category/文学流派.md "wikilink") </small>                                                             |

<noinclude>

[ja:Template:文学](ja:Template:文学.md "wikilink") </noinclude>

[\*](../Category/文学.md "wikilink")
[文学导航模板](../Category/文学导航模板.md "wikilink")
[](../Category/藝術和文化側面模板.md "wikilink")