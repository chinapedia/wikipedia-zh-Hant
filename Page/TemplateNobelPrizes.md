<noinclude>

## 参见

  - [Template:諾貝爾獎](../Page/Template:諾貝爾獎.md "wikilink")
  - [Template:诺贝尔化学奖获得者](../Page/Template:诺贝尔化学奖获得者.md "wikilink")
  - [Template:诺贝尔文学奖获得者](../Page/Template:诺贝尔文学奖获得者.md "wikilink")
  - [Template:诺贝尔和平奖获得者](../Page/Template:诺贝尔和平奖获得者.md "wikilink")
  - [Template:诺贝尔物理学奖获得者](../Page/Template:诺贝尔物理学奖获得者.md "wikilink")
  - [Template:诺贝尔生理学或医学奖获得者](../Page/Template:诺贝尔生理学或医学奖获得者.md "wikilink")
  - [Template:诺贝尔经济学奖获得者](../Page/Template:诺贝尔经济学奖获得者.md "wikilink")

</noinclude>

[诺贝尔奖模板](../Category/诺贝尔奖模板.md "wikilink")