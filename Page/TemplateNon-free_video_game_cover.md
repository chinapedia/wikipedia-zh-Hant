}|yes| {{\#ifeq:|File|}} |
**致巡查員與管理員**：如果本檔案擁有一份合適的合理使用依據，請在本授權模板增加`|image
has rationale=yes`參數語法。}}
{{\#ifeq:|File|{{\#ifeq:|yes|}}}}}}}}<includeonly>{{\#ifeq:||}}}}</includeonly><noinclude>
 </noinclude>

[Category:合理使用理據檢查完成影像](../Category/合理使用理據檢查完成影像.md "wikilink")
[Category:合理使用理據待檢查影像](../Category/合理使用理據待檢查影像.md "wikilink")
[Category:合理使用理據待檢查影像
(機器人標註)](../Category/合理使用理據待檢查影像_\(機器人標註\).md "wikilink")