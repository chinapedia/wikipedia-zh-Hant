<noinclude></noinclude> |- \!style="background:silver;"|   |-
style="text-align:center;" | <span style="font-size:larger;">**冠軍**
**[曼联](曼联足球俱乐部.md "wikilink")**</span>

<div style="font-size:smaller;">

**亞軍**
[-{A](切尔西足球俱乐部.md "wikilink")

**4強出局**
[-{A](巴塞罗那足球俱乐部.md "wikilink") • [利物浦](利物浦足球俱乐部.md "wikilink")

**8強出局**
[-{A](沙尔克04足球俱乐部.md "wikilink") • [羅馬](羅馬足球俱樂部.md "wikilink") •
[-{A](阿森纳足球俱乐部.md "wikilink") • [-{A](費倫巴治體育會.md "wikilink")

**16強出局**
[AC米兰](米兰足球俱乐部.md "wikilink") • [-{A](凯尔特人足球俱乐部.md "wikilink") •
[-{A](塞維利亞足球俱樂部.md "wikilink") • [里昂](里昂足球俱樂部.md "wikilink") •
[-{A](奥林匹亚克斯足球俱乐部.md "wikilink") • [皇家马德里](皇家马德里足球俱乐部.md "wikilink") •
[-{A](波尔图足球俱乐部.md "wikilink") • [国际米兰](国际米兰足球俱乐部.md "wikilink")

**分組賽轉戰足協杯**
[-{A](葡萄牙体育俱乐部.md "wikilink") • [-{A](本菲卡.md "wikilink") •
[布拉格斯拉維亞](布拉格斯拉維亞.md "wikilink") •
[-{A](洛辛堡.md "wikilink") • [云达不来梅](不来梅沙洲体育俱乐部.md "wikilink") •
[格拉斯哥流浪者](流浪者足球俱乐部.md "wikilink") • [-{A](PSV燕豪芬.md "wikilink") •
[马赛](马赛足球俱乐部.md "wikilink")

**分組賽出局**
[莫斯科中央陸軍](莫斯科中央陸軍.md "wikilink") • [-{A](基輔戴拿模.md "wikilink") •
[-{A](薩克達.md "wikilink") • [布加勒斯特星](布加勒斯特星队.md "wikilink") •
[-{A](斯图加特体育俱乐部.md "wikilink") • [-{A](贝西克塔什体操俱乐部.md "wikilink") •
[-{A](拉素足球會.md "wikilink") • [-{A](巴伦西亚足球俱乐部.md "wikilink")

**第三圈外圍賽出局**
[圖盧茲](圖盧茲足球俱樂部.md "wikilink") • [-{A](阿積士.md "wikilink") •
[AEK雅典](AEK雅典.md "wikilink")• [莫斯科斯巴達](莫斯科斯巴達.md "wikilink") •
[-{A](布加勒斯特戴拿模.md "wikilink") • [-{A](皇家安德列治體育會.md "wikilink") •
[布拉格斯巴達](布拉格斯巴達.md "wikilink") •
[蘇黎世](蘇黎世足球俱樂部.md "wikilink")
[貝爾格萊德紅星](貝爾格萊德紅星.md "wikilink") • [-{A](IF埃尔夫斯堡.md "wikilink") •
[萨拉热窝](FK薩拉熱窩.md "wikilink") • [-{A](巴迪.md "wikilink") •
[薩爾茨堡紅牛](薩爾茨堡紅牛.md "wikilink") •
[-{A](坦佩雷联足球俱乐部.md "wikilink") •
[哥本哈根](哥本哈根足球會.md "wikilink") •
[-{A](薩格勒布戴拿模.md "wikilink")

**第二圈外圍賽出局**
[佩歷克](佩歷克.md "wikilink") • [利維迪亞](利維迪亞.md "wikilink") •
[沙達](沙達.md "wikilink") • [迪比辛尼](迪比辛尼.md "wikilink") •
[路賓](路賓.md "wikilink")• [亨克](亨克.md "wikilink") •
[雲特史比斯](雲特史比斯.md "wikilink") •
[阿斯塔納](阿斯塔納足球會.md "wikilink") •
[夏拿佐杜亞](夏拿佐杜亞.md "wikilink")
[貝塔耶路撒冷](貝塔耶路撒冷.md "wikilink") • [斯利納](斯利納.md "wikilink") •
[索菲亞利夫斯基](索菲亞利夫斯基.md "wikilink") •
[當姆薩尼](當姆薩尼.md "wikilink") • [舒列夫](舒列夫.md "wikilink")

</div>

<noinclude>  </noinclude>