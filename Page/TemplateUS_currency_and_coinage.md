[联邦准备银行券](../Page/联邦准备银行券.md "wikilink"){{·}}[美元](../Page/美元.md "wikilink"){{·}}[美国财政部](../Page/美国财政部.md "wikilink"){{·}}[美国铸币局](../Page/美国铸币局.md "wikilink"){{·}}[美国雕刻印刷局](../Page/美国雕刻印刷局.md "wikilink"){{·}}[残损货币](../Page/残损货币.md "wikilink")
|group2=[流通硬币](../Page/美元硬币.md "wikilink") |list2 =[1美分
(Cent)](../Page/1美分硬币.md "wikilink"){{·}} [5美分
(Nickel)](../Page/5美分硬币.md "wikilink"){{·}} [10美分
(Dime)](../Page/10美分硬币.md "wikilink") {{·}} [25美分
(Quarter)](../Page/25美分硬币.md "wikilink"){{·}} [50美分 (Half
Dollar)](../Page/50美分.md "wikilink") {{·}} [1美元
(Dollar)](../Page/1美元硬币.md "wikilink") |group3=特别版本硬币 |list3 =
[1943年钢制1美分硬币](../Page/1943年钢制1美分硬币.md "wikilink"){{·}}[1955年双压印1美分硬币](../Page/1955年双压印1美分硬币.md "wikilink"){{·}}[1974年铝制1美分硬币](../Page/1974年铝制1美分硬币.md "wikilink")
|group4=纪念硬币 |list4
=[美国建国二百周年纪念币](../Page/美国建国二百周年纪念币.md "wikilink"){{·}}[苏珊·安东尼纪念币](../Page/苏珊·安东尼1美元纪念币.md "wikilink"){{·}}[萨卡加维亚纪念币](../Page/萨卡加维亚1美元纪念币.md "wikilink"){{·}}[美国西部探险5美分纪念币](../Page/美国西部探险5美分纪念币.md "wikilink"){{·}}[50州纪念币](../Page/美国50州25美分纪念币.md "wikilink"){{·}}
[哥伦比亚特区及自治领地纪念币](../Page/美国哥伦比亚特区及自治领地25美分纪念币.md "wikilink"){{·}}[美国总统纪念币](../Page/美国总统1美元纪念币.md "wikilink"){{·}}[林肯200周年纪念币](../Page/1美分林肯200周年纪念币.md "wikilink"){{·}}[国家公园纪念币](../Page/美国美丽国家公园25美分纪念币.md "wikilink")
|group5=贵金属币 |list5
=[美国美丽国家公园纪念银币](../Page/国家公园纪念银币.md "wikilink"){{·}}[美国水牛金币](../Page/美国水牛金币.md "wikilink"){{·}}[美国金鹰硬币](../Page/美国金鹰硬币.md "wikilink"){{·}}[美国铂金鹰硬币](../Page/美国铂金鹰硬币.md "wikilink"){{·}}[美国银鹰硬币](../Page/美国银鹰硬币.md "wikilink")
|group6=流通纸币 |list6 =
[$1](../Page/一美元纸币.md "wikilink"){{·}}[$2](../Page/2美元纸币.md "wikilink"){{·}}[$5](../Page/5美元纸币.md "wikilink"){{·}}[$10](../Page/10美元纸币.md "wikilink"){{·}}[$20](../Page/20美元纸币.md "wikilink"){{·}}[$50](../Page/50美元纸币.md "wikilink"){{·}}[$100](../Page/100美元纸币.md "wikilink"){{·}}[大额纸币](../Page/大额美国纸币.md "wikilink")''
|group7=参见 |list7
=[美国建国二百周年纪念币计划](../Page/美国建国二百周年纪念币计划.md "wikilink"){{·}}[美国纪念币](../Page/美国纪念币.md "wikilink"){{·}}[早期纪念币](../Page/美国早期纪念硬币.md "wikilink"){{·}}[当代纪念币](../Page/美国当代纪念硬币.md "wikilink"){{·}}[联盟元](../Page/美利坚联盟国元.md "wikilink"){{·}}[假面值美元纸币](../Page/假面值美元纸币.md "wikilink")（[$200](../Page/200美元纸币.md "wikilink")）{{·}}[美国已停用的纸币面值](../Page/曾经流通的面值.md "wikilink"){{·}}[厘](../Page/厘\(货币\).md "wikilink"){{·}}[硬币制造](../Page/美国硬币年发行量列表.md "wikilink"){{·}}*[In
God We Trust](../Page/我们信仰上帝.md "wikilink")*{{·}}*[E pluribus
unum](../Page/合众为一.md "wikilink")*{{·}}[别称](../Page/:en:Federal_Reserve_Note#Nicknames.md "wikilink")
}}<noinclude> </noinclude>

[Category:美国货币](../Category/美国货币.md "wikilink")
[Category:钱币学导航模板](../Category/钱币学导航模板.md "wikilink")
[Category:货币模板](../Category/货币模板.md "wikilink")