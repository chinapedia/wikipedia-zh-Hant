[宿雾语](宿雾语维基百科.md "wikilink"):[ceb](:ceb:.md "wikilink")

</div>

|group2 = 超過200万篇條目 |abbr2 = 200 |list2 =

<div>

[瑞典语](瑞典语维基百科.md "wikilink"):[sv](:sv:.md "wikilink") {{\!}}
[德語](德語維基百科.md "wikilink"):[de](:de:.md "wikilink")
{{\!}} [法語](法语维基百科.md "wikilink"):[fr](:fr:.md "wikilink")

</div>

|group3 = 超過150万篇條目 |abbr3 = 150 |list3 =

<div>

[荷蘭語](荷蘭語維基百科.md "wikilink"):[nl](:nl:.md "wikilink") {{\!}}
[俄语](俄语维基百科.md "wikilink"):[ru](:ru:.md "wikilink")
{{\!}} [意大利语](意大利语维基百科.md "wikilink"):[it](:it:.md "wikilink") {{\!}}
[西班牙语](西班牙语维基百科.md "wikilink"):[es](:es:.md "wikilink")

</div>

|group4 = 超過100万篇條目 |abbr4 = 100 |list4 =

<div>

[波兰语](波兰语维基百科.md "wikilink"):[pl](:pl:.md "wikilink") {{\!}}
[瓦瑞语](瓦瑞语维基百科.md "wikilink"):[war](:war:.md "wikilink")
{{\!}} [越南语](越南语维基百科.md "wikilink"):[vi](:vi:.md "wikilink") {{\!}}
[日语](日语维基百科.md "wikilink"):[ja](:ja:.md "wikilink") {{\!}}
**[中文](中文维基百科.md "wikilink"):[zh](Wikipedia:首页.md "wikilink")**
{{\!}} [葡萄牙语](葡萄牙语维基百科.md "wikilink"):[pt](:pt:.md "wikilink")

</div>

|group5 =超過50万篇條目 |abbr5 =50 |list5 =

<div>

[乌克兰语](乌克兰语维基百科.md "wikilink"):[uk](:uk:.md "wikilink") {{\!}}
[阿拉伯语](阿拉伯语维基百科.md "wikilink"):[ar](:ar:.md "wikilink")
{{\!}} [波斯语](波斯语维基百科.md "wikilink"):[fa](:fa:.md "wikilink") {{\!}}
[塞尔维亚语](塞尔维亚语维基百科.md "wikilink"):[sr](:sr:.md "wikilink") {{\!}}
[加泰罗尼亚语](加泰罗尼亚语维基百科.md "wikilink"):[ca](:ca:.md "wikilink") {{\!}}
[书面挪威语](挪威语维基百科.md "wikilink"):[no](:no:.md "wikilink")

</div>

|group6 =超過20万篇條目 |abbr6 =20 |list6 =

<div>

[印尼語](印尼語維基百科.md "wikilink"):[id](:id:.md "wikilink") {{\!}}
[芬兰语](芬兰语维基百科.md "wikilink"):[fi](:fi:.md "wikilink")
{{\!}} [韓語](韓語維基百科.md "wikilink"):[ko](:ko:.md "wikilink") {{\!}}
[塞爾維亞-克羅埃西亞語](塞爾維亞-克羅埃西亞語維基百科.md "wikilink"):[sh](:sh:.md "wikilink")
{{\!}} [匈牙利语](匈牙利语维基百科.md "wikilink"):[hu](:hu:.md "wikilink") {{\!}}
[捷克语](捷克语维基百科.md "wikilink"):[cs](:cs:.md "wikilink") {{\!}}
[罗马尼亚语](罗马尼亚语维基百科.md "wikilink"):[ro](:ro:.md "wikilink")
{{\!}} [巴斯克語](巴斯克語維基百科.md "wikilink"):[eu](:eu:.md "wikilink") {{\!}}
[馬來語](馬來語維基百科.md "wikilink"):[ms](:ms:.md "wikilink") {{\!}}
[土耳其語](土耳其語維基百科.md "wikilink"):[tr](:tr:.md "wikilink") {{\!}}
[世界語](世界語維基百科.md "wikilink"):[eo](:eo:.md "wikilink") {{\!}}
[亞美尼亞語](亞美尼亞語維基百科.md "wikilink"):[hy](:hy:.md "wikilink")
{{\!}} [保加利亞語](保加利亞語維基百科.md "wikilink"):[bg](:bg:.md "wikilink") {{\!}}
[丹麦语](丹麦语维基百科.md "wikilink"):[da](:da:.md "wikilink") {{\!}}
[希伯來語](希伯來語維基百科.md "wikilink"):[he](:he:.md "wikilink")
{{\!}} [斯洛伐克語](斯洛伐克語維基百科.md "wikilink"):[sk](:sk:.md "wikilink") {{\!}}
[閩南語](閩南語維基百科.md "wikilink"):[zh-min-nan](:zh-min-nan:.md "wikilink")
{{\!}} [哈萨克语](哈萨克语维基百科.md "wikilink"):[kk](:kk:.md "wikilink") {{\!}}
[米南佳保語](米南佳保語維基百科.md "wikilink"):[min](:min:.md "wikilink") {{\!}}
[車臣語](車臣語維基百科.md "wikilink"):[ce](:ce:.md "wikilink") {{\!}}
[克罗地亚语](克罗地亚语维基百科.md "wikilink"):[hr](:hr:.md "wikilink")

</div>

|group7 =超過10万篇條目 |abbr7 =10 |list7 =

<div>

[立陶宛語](立陶宛語維基百科.md "wikilink"):[lt](:lt:.md "wikilink") {{\!}}
[愛沙尼亞語](愛沙尼亞語維基百科.md "wikilink"):[et](:et:.md "wikilink")
{{\!}} [白俄羅斯語](白俄羅斯語維基百科.md "wikilink"):[be](:be:.md "wikilink") {{\!}}
[斯洛文尼亞語](斯洛文尼亞語維基百科.md "wikilink"):[sl](:sl:.md "wikilink") {{\!}}
[希臘語](希臘語維基百科.md "wikilink"):[el](:el:.md "wikilink") {{\!}}
[加利西亚语](Wikipedia:加里西亞語維基百科.md "wikilink"):[gl](:gl:.md "wikilink")
{{\!}} [阿塞拜疆語](阿塞拜疆語維基百科.md "wikilink"):[az](:az:.md "wikilink") {{\!}}
[烏爾都語](烏爾都語維基百科.md "wikilink"):[ur](:ur:.md "wikilink") {{\!}}
[简单英语](简单英语维基百科.md "wikilink"):[simple](:simple:.md "wikilink")
{{\!}} [新挪威語](新挪威語維基百科.md "wikilink"):[nn](:nn:.md "wikilink") {{\!}}
[azb](:azb:.md "wikilink") {{\!}}
[乌兹别克语](乌兹别克语维基百科.md "wikilink"):[uz](:uz:.md "wikilink")
{{\!}} [泰語](泰語維基百科.md "wikilink"):[th](:th:.md "wikilink") {{\!}}
[拉丁語](拉丁語維基百科.md "wikilink"):[la](:la:.md "wikilink") {{\!}}
[印地語](印地語維基百科.md "wikilink"):[hi](:hi:.md "wikilink") {{\!}}
[格魯吉亞語](Wikipedia:格魯吉亞語維基百科.md "wikilink"):[ka](:ka:.md "wikilink")
{{\!}}
[沃拉普克語](Wikipedia:沃拉普克語維基百科.md "wikilink"):[vo](:vo:.md "wikilink")
{{\!}} [泰米爾語](泰米爾語維基百科.md "wikilink"):[ta](:ta:.md "wikilink") {{\!}}
[威爾士語](Wikipedia:威爾士語維基百科.md "wikilink"):[cy](:cy:.md "wikilink")

</div>

|group8 = 超過5万篇條目 |abbr8 = 5 |list8 =
[海地語](Wikipedia:海地語維基百科.md "wikilink"):[ht](:ht:.md "wikilink")
{{\!}} [馬其頓語](馬其頓語維基百科.md "wikilink"):[mk](:mk:.md "wikilink") {{\!}}
[尼瓦爾語](Wikipedia:尼瓦爾語維基百科.md "wikilink"):[new](:new:.md "wikilink")
{{\!}} [奧克語](Wikipedia:奧克語維基百科.md "wikilink"):[oc](:oc:.md "wikilink")
{{\!}}
[皮德蒙語](Wikipedia:皮德蒙語維基百科.md "wikilink"):[pms](:pms:.md "wikilink")
{{\!}}
[阿羅馬尼亞語](Wikipedia:阿羅馬尼亞語維基百科.md "wikilink"):[roa-rup](:roa-rup:.md "wikilink")
{{\!}} [泰盧固語](Wikipedia:泰盧固語維基百科.md "wikilink"):[te](:te:.md "wikilink")
{{\!}} [塔加洛語](Wikipedia:塔加洛語維基百科.md "wikilink"):[tl](:tl:.md "wikilink")
{{\!}} [粵文](粵文維基百科.md "wikilink"):[zh-yue](:zh-yue:.md "wikilink")

|group9 = 超過1万篇條目 |abbr9 = 1 |list9 =
[南非語](Wikipedia:南非語維基百科.md "wikilink"):[af](:af:.md "wikilink")
{{\!}} [阿勒曼尼語](阿勒曼尼語維基百科.md "wikilink"):[als](:als:.md "wikilink")
{{\!}} [阿姆哈拉語](阿姆哈拉語維基百科.md "wikilink"):[am](:am:.md "wikilink") {{\!}}
[亞拉岡語](亞拉岡語維基百科.md "wikilink"):[an](:an:.md "wikilink") {{\!}}
[埃及阿拉伯语](Wikipedia:埃及阿拉伯语维基百科.md "wikilink"):[arz](:arz:.md "wikilink")
{{\!}}
[阿斯圖里亞語](Wikipedia:阿斯圖里亞語維基百科.md "wikilink"):[ast](:ast:.md "wikilink")
{{\!}} [巴什基爾語](巴什基爾語維基百科.md "wikilink"):[ba](:ba:.md "wikilink") {{\!}}
[薩莫吉希亞語](薩莫吉希亞語維基百科.md "wikilink"):[bat-smg](:bat-smg:.md "wikilink")
{{\!}}
[舊白俄羅斯語](白俄羅斯語維基百科.md "wikilink"):[be-tarask](:be-tarask:.md "wikilink")
{{\!}} [孟加拉語](孟加拉語維基百科.md "wikilink"):[bn](:bn:.md "wikilink") {{\!}}
[比什奴普萊利亞-曼尼浦爾語](Wikipedia:比什奴普萊利亞-曼尼浦爾語維基百科.md "wikilink"):[bpy](:bpy:.md "wikilink")
{{\!}}
[布列塔尼語](Wikipedia:布列塔尼語維基百科.md "wikilink"):[br](:br:.md "wikilink")
{{\!}} [波斯尼亞語](波斯尼亞語維基百科.md "wikilink"):[bs](:bs:.md "wikilink") {{\!}}
[布吉語](布吉語維基百科.md "wikilink"):[bug](:bug:.md "wikilink") {{\!}}
[中庫爾德語](中庫爾德語維基百科.md "wikilink"):[ckb](:ckb:.md "wikilink"){{\!}}
[楚瓦什語](楚瓦什語維基百科.md "wikilink"):[cv](:cv:.md "wikilink") {{\!}}
[扎扎其語](扎扎其語維基百科.md "wikilink"):[diq](:diq:.md "wikilink")
{{\!}} [西弗里斯語](西弗里斯語維基百科.md "wikilink"):[fy](:fy:.md "wikilink") {{\!}}
[愛爾蘭語](愛爾蘭語維基百科.md "wikilink"):[ga](:ga:.md "wikilink") {{\!}}
[蘇格蘭蓋爾語](蘇格蘭蓋爾語維基百科.md "wikilink"):[gd](:gd:.md "wikilink")
{{\!}} [古加拉特語](古加拉特語維基百科.md "wikilink"):[gu](:gu:.md "wikilink") {{\!}}
[拉丁國際語](拉丁國際語維基百科.md "wikilink"):[ia](:ia:.md "wikilink") {{\!}}
[伊多語](伊多語維基百科.md "wikilink"):[io](:io:.md "wikilink") {{\!}}
[冰島語](冰島語維基百科.md "wikilink"):[is](:is:.md "wikilink") {{\!}}
[爪哇語](Wikipedia:爪哇語維基百科.md "wikilink"):[jv](:jv:.md "wikilink")
{{\!}} [卡纳達語](卡纳達語維基百科.md "wikilink"):[kn](:kn:.md "wikilink") {{\!}}
[庫爾德語](Wikipedia:庫爾德語維基百科.md "wikilink"):[ku](:ku:.md "wikilink")
{{\!}} [吉爾吉斯語](吉爾吉斯語維基百科.md "wikilink"):[ky](:ky:.md "wikilink") {{\!}}
[盧森堡語](盧森堡語維基百科.md "wikilink"):[lb](:lb:.md "wikilink") {{\!}}
[倫巴底語](Wikipedia:倫巴底語維基百科.md "wikilink"):[lmo](:lmo:.md "wikilink")
{{\!}} [拉脫維亞語](拉脫維亞語維基百科.md "wikilink"):[lv](:lv:.md "wikilink") {{\!}}
[班尤馬山語](班尤馬山語維基百科.md "wikilink"):[map-bms](:map-bms:.md "wikilink")
{{\!}} [馬拉加什語](馬拉加什語維基百科.md "wikilink"):[mg](:mg:.md "wikilink") {{\!}}
[馬拉亞姆語](馬拉亞姆語維基百科.md "wikilink"):[ml](:ml:.md "wikilink") {{\!}}
[馬拉地語](Wikipedia:馬拉地語維基百科.md "wikilink"):[mr](:mr:.md "wikilink")
{{\!}} [緬甸語](Wikipedia:緬甸語維基百科.md "wikilink"):[my](:my:.md "wikilink")
{{\!}} [馬桑達拉尼語](馬桑達拉尼語維基百科.md "wikilink"):[mzn](:mzn:.md "wikilink")
{{\!}}
[那不勒斯語](Wikipedia:那不勒斯語維基百科.md "wikilink"):[nap](:nap:.md "wikilink")
{{\!}}
[低地德語](Wikipedia:低地德語維基百科.md "wikilink"):[nds](:nds:.md "wikilink")
{{\!}} [尼泊爾語](尼泊爾語維基百科.md "wikilink"):[ne](:ne:.md "wikilink") {{\!}}
[西旁遮普語](西旁遮普語維基百科.md "wikilink"):[pnb](:pnb:.md "wikilink") {{\!}}
[蓋丘亞語](蓋丘亞語維基百科.md "wikilink"):[qu](:qu:.md "wikilink") {{\!}}
[西西里語](Wikipedia:西西里語維基百科.md "wikilink"):[scn](:scn:.md "wikilink")
{{\!}} [蘇格蘭語](蘇格蘭語維基百科.md "wikilink"):[sco](:sco:.md "wikilink") {{\!}}
[阿爾巴尼亞語](阿爾巴尼亞語維基百科.md "wikilink"):[sq](:sq:.md "wikilink") {{\!}}
[巽他語](Wikipedia:巽他語維基百科.md "wikilink"):[su](:su:.md "wikilink")
{{\!}} [斯瓦希里語](斯瓦希里語維基百科.md "wikilink"):[sw](:sw:.md "wikilink") {{\!}}
[塔吉克語](塔吉克語維基百科.md "wikilink"):[tg](:tg:.md "wikilink") {{\!}}
[塔塔爾語](塔塔爾語維基百科.md "wikilink"):[tt](:tt:.md "wikilink")
{{\!}} [瓦隆語](瓦隆語維基百科.md "wikilink"):[wa](:wa:.md "wikilink") {{\!}}
[約魯巴語](約魯巴語維基百科.md "wikilink"):[yo](:yo:.md "wikilink")

|group10= [汉語維基百科](中文维基百科.md "wikilink") |abbr10= Chiwp |list10=
[中文维基百科](中文维基百科.md "wikilink"):[zh](:zh:.md "wikilink")
{{\!}}
[閩南語維基百科](閩南語維基百科.md "wikilink"):[zh-min-nan](:zh-min-nan:.md "wikilink")
{{\!}} [粵文維基百科](粵文維基百科.md "wikilink"):[zh-yue](:zh-yue:.md "wikilink")
{{\!}}
[吳語維基百科](Wikipedia:吳語維基百科.md "wikilink"):[wuu](:wuu:.md "wikilink")
{{\!}}[閩東語維基百科](Wikipedia:閩東語維基百科.md "wikilink"):[cdo](:cdo:.md "wikilink")
{{\!}}
[客家語維基百科](Wikipedia:客家語維基百科.md "wikilink"):[hak](:hak:.md "wikilink")
{{\!}}
[文言文維基百科](Wikipedia:文言文維基百科.md "wikilink"):[zh-classical](:zh-classical:.md "wikilink")
{{\!}}
[贛語維基百科](Wikipedia:贛語維基百科.md "wikilink"):[gan](:gan:.md "wikilink")
{{\!}}

|group11= 其他 |abbr11= other |list11=
[维吾尔语维基百科](维吾尔语维基百科.md "wikilink"):[ug](:ug:.md "wikilink")
{{\!}} [藏语维基百科](Wikipedia:藏语维基百科.md "wikilink"):[bo](:bo:.md "wikilink")
{{\!}} [蒙古语维基百科](蒙古语维基百科.md "wikilink"):[mn](:mn:.md "wikilink") {{\!}}
[壮语维基百科](Wikipedia:壮语维基百科.md "wikilink"):[za](:za:.md "wikilink") {{\!}}
|below = [完整列表](Wikipedia:维基百科语言列表.md "wikilink") }}<noinclude>

  - 折叠与展开参数

整个模板：  分组：

`{{Wikipedia Editions|`*`该组缩写`*`}}`。从上至下各组的缩写为：400，150，100，50，10，5，1。
</noinclude>

[Category:姊妹计划链接模板](../Category/姊妹计划链接模板.md "wikilink")