<div id="lang" class="nowraplinks nourlexpansion plainlinks">

{{\#ifeq:  | Special:Statistics | |
[中文维基百科目前有](../Page/中文维基百科.md "wikilink")[篇条目](../Page/Special:Statistics.md "wikilink")。}}
以下列出一些条目数量最多的各语言版本维基百科。

  - 条目数量在**5,000,000**以上的语言版本：
    <div class="hlist inline">
      -
    </div>
  - 条目数量在**1,000,000**以上的语言版本：
    <div class="hlist inline">
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
    </div>
  - 条目数量在**250,000**以上的语言版本：
    <div class="hlist inline">
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
    </div>
  - 条目数量在**50,000**以上的语言版本：
    <div class="hlist inline">
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
    </div>

</div>

  - **[完整列表](../Page/Wikipedia:维基百科语言列表.md "wikilink")**
  - **[跨语言整合](../Page/:en:Wikipedia:Multilingual_coordination.md "wikilink")**
  - **[创建新语言版本](../Page/m:Help:How_to_start_a_new_Wikipedia.md "wikilink")**

<noinclude>

</noinclude>