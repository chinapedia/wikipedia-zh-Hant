**维基百科**由非营利组织[维基媒体基金会运作](../Page/:wikimedia:首页.md "wikilink")。基金会旗下尚有其他数个多语言、内容开放的维基计划：

<table>
<tbody>
<tr class="odd">
<td><figure>
<img src="https://zh.wikipedia.org/wiki/File:Wiktprintable_without_text_zh.svg" title="Wiktprintable_without_text_zh.svg" alt="Wiktprintable_without_text_zh.svg" /><figcaption>Wiktprintable_without_text_zh.svg</figcaption>
</figure></td>
<td><p><a href="../Page/wikt:.md" title="wikilink"><strong>维基词典</strong></a><br />
<span style="font-size:small;">多语言字词典</span></p></td>
<td><figure>
<img src="https://zh.wikipedia.org/wiki/File:Wikisource-logo.svg" title="Wikisource-logo.svg" alt="Wikisource-logo.svg" /><figcaption>Wikisource-logo.svg</figcaption>
</figure></td>
<td><p><a href="../Page/s:.md" title="wikilink"><strong>维基文库</strong></a><br />
<span style="font-size:small;">自由内容的图书馆</span></p></td>
<td><figure>
<img src="https://zh.wikipedia.org/wiki/File:Wikiquote-logo.svg" title="Wikiquote-logo.svg" alt="Wikiquote-logo.svg" /><figcaption>Wikiquote-logo.svg</figcaption>
</figure></td>
<td><p><a href="../Page/q:.md" title="wikilink"><strong>维基语录</strong></a><br />
<span style="font-size:small;">名人名言的集锦</span></p></td>
<td><figure>
<img src="https://zh.wikipedia.org/wiki/File:Wikibooks-logo.svg" title="Wikibooks-logo.svg" alt="Wikibooks-logo.svg" /><figcaption>Wikibooks-logo.svg</figcaption>
</figure></td>
<td><p><a href="../Page/b:.md" title="wikilink"><strong>维基教科书</strong></a><br />
<span style="font-size:small;">自由的教科书和手册</span></p></td>
</tr>
<tr class="even">
<td><figure>
<img src="https://zh.wikipedia.org/wiki/File:Wikinews-logo.svg" title="Wikinews-logo.svg" alt="Wikinews-logo.svg" /><figcaption>Wikinews-logo.svg</figcaption>
</figure></td>
<td><p><a href="../Page/n:.md" title="wikilink"><strong>维基新闻</strong></a><br />
<span style="font-size:small;">自由内容的新闻</span></p></td>
<td><figure>
<img src="https://zh.wikipedia.org/wiki/File:Wikispecies-logo.svg" title="Wikispecies-logo.svg" alt="Wikispecies-logo.svg" /><figcaption>Wikispecies-logo.svg</figcaption>
</figure></td>
<td><p><a href="../Page/Wikispecies:首页.md" title="wikilink"><strong>维基物种</strong></a><br />
<span style="font-size:small;">物种目录</span></p></td>
<td><figure>
<img src="https://zh.wikipedia.org/wiki/File:Wikivoyage-Logo-v3-icon.svg" title="Wikivoyage-Logo-v3-icon.svg" alt="Wikivoyage-Logo-v3-icon.svg" width="35" /><figcaption>Wikivoyage-Logo-v3-icon.svg</figcaption>
</figure></td>
<td><p><a href="../Page/voy:.md" title="wikilink"><strong>维基导游</strong></a><br />
<span style="font-size:small;">自由的旅行指南</span></p></td>
<td><figure>
<img src="https://zh.wikipedia.org/wiki/File:Wikiversity-logo.svg" title="Wikiversity-logo.svg" alt="Wikiversity-logo.svg" /><figcaption>Wikiversity-logo.svg</figcaption>
</figure></td>
<td><p><a href="../Page/v:.md" title="wikilink"><strong>维基学院</strong></a><br />
<span style="font-size:small;">自由的研习社群</span></p></td>
</tr>
<tr class="odd">
<td><figure>
<img src="https://zh.wikipedia.org/wiki/File:Commons-logo.svg" title="Commons-logo.svg" alt="Commons-logo.svg" /><figcaption>Commons-logo.svg</figcaption>
</figure></td>
<td><p>-{zh-hans:<a href="../Page/commons:首页.md" title="wikilink"><strong>维基共享资源</strong></a>; zh-hant:<a href="../Page/commons:首頁.md" title="wikilink"><strong>維基共享資源</strong></a>;}-<br />
<span style="font-size:small;">自由的多媒体资料库</span></p></td>
<td><figure>
<img src="https://zh.wikipedia.org/wiki/File:Wikidata-logo.svg" title="Wikidata-logo.svg" alt="Wikidata-logo.svg" width="47" /><figcaption>Wikidata-logo.svg</figcaption>
</figure></td>
<td><p>-{zh-hans:<a href="../Page/d:Wikidata:首页.md" title="wikilink"><strong>维基数据</strong></a>; zh-hant:<a href="../Page/d:Wikidata:首頁.md" title="wikilink"><strong>維基數據</strong></a>;}-<br />
<span style="font-size:small;">自由的知识库</span></p></td>
<td><figure>
<img src="https://zh.wikipedia.org/wiki/File:MediaWiki-notext.svg" title="MediaWiki-notext.svg" alt="MediaWiki-notext.svg" /><figcaption>MediaWiki-notext.svg</figcaption>
</figure></td>
<td><p><a href="../Page/mw:MediaWiki/zh.md" title="wikilink"><strong>MediaWiki</strong></a><br />
<span style="font-size:small;">Wiki软件开发</span></p></td>
<td><figure>
<img src="https://zh.wikipedia.org/wiki/File:Wikimedia_Community_Logo.svg" title="Wikimedia_Community_Logo.svg" alt="Wikimedia_Community_Logo.svg" /><figcaption>Wikimedia_Community_Logo.svg</figcaption>
</figure></td>
<td><p><a href="../Page/m:首页.md" title="wikilink"><strong>元维基</strong></a><br />
<span style="font-size:small;">协调各维基计划</span></p></td>
</tr>
</tbody>
</table>

<noinclude></noinclude>