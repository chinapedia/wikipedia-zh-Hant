|- \! colspan=2 width="100%" bgcolor=lightsteelblue {{\!}} 經濟 |-
{{\#if:| {{\!}}
**[GDP總量](国内生产总值.md "wikilink")**<sup>[年]({{{GDP_as_of}}}年.md "wikilink")</sup>
{{\!}} <font size="2">[￥](人民幣.md "wikilink")  億元</font> }} |- {{\#if:|
{{\!}} <font size="2">-GDP位次</font> {{\!}}
<font size="2"><span style="color:gary;"></span></font> }} |- {{\#if:|
{{\!}}**人均GDP**<sup>[年]({{{GDPpCa_as_of}}}年.md "wikilink")</sup> {{\!}}
<font size="2"> }} |- {{\#if:| {{\!}} <font size="2">-人均GDP位次</font>
{{\!}} <font size="2"></font> }} |- {{\#if:|
{{\!}}**[GNP總量](國民生產總值.md "wikilink")** {{\!}}
<font size="2"></font> }} |- {{\#if:| {{\!}}
<font size="2">-GNP位次</font> {{\!}} <font size="2"></font> }} |-
{{\#if:| {{\!}}**人均GNP** {{\!}} <font size="2"></font> }} |- {{\#if:|
{{\!}} <font size="2">-人均GNP位次</font> {{\!}} <font size="2"></font> }}
<noinclude>

## 示例一

    {{一覽表 經濟
    |GDP        = GDP数值（本币，常以百万、亿、万亿为单位）
    |GDPRank    = 第x位（省内比较）
    |GDPpCa     = 人均GDP数值（本币，以元为单位）
    |GDPpCaRank = 第x位（省内比较）
    |GNP        = GNP数值（本币，常以百万、亿、万亿为单位）
    |GNPRank    = 第x位（省内比较）
    |GNPpCa     = 人均GNP数值（本币，以元为单位）
    |GNPpCaRank = 第x位（省内比较）
    }}

## 示例二

    {{一覽表 經濟
    |GDP        = GDP数值（本币，常以百万、亿、万亿为单位） <br> 折合xxx(美元单位计算)
    |GDPRank    = 第x位（省内比较）
    |GDPpCa     = 人均GDP数值（本币，以元为单位） <br> 折合xxx(美元单位计算)
    |GDPpCaRank = 第x位（省内比较）
    |GNP        = GNP数值（本币，常以百万、亿、万亿为单位） <br> 折合xxx(美元单位计算)
    |GNPRank    = 第x位（省内比较）
    |GNPpCa     = 人均GNP数值（本币，以元为单位） <br> 折合xxx(美元单位计算)
    |GNPpCaRank = 第x位（省内比较）
    }}

</noinclude>

[](../Category/国家模板.md "wikilink")
[亚](../Category/行政区划信息框模板.md "wikilink")