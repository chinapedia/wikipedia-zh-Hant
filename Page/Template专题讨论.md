<div style="float: right; width: 200px; font-size:80%; background: #f0f0f0; margin: 2.8em 0 0 3.2em; padding: 1em 0.3em 1em 0.5em">

**[专题讨论](../Page/Wikipedia:专题讨论.md "wikilink")**（[聊天](../Page/Wikipedia:聊天.md "wikilink")、[互助客栈中有价值的讨论将被不定期复制到下列专题及其討論頁中](../Page/Wikipedia:互助客栈.md "wikilink")）

技巧交流

1.  [原创编写](../Page/Wikipedia_talk:原创编写.md "wikilink")（[操作建议](../Page/Wikipedia:原创编写.md "wikilink")）
2.  [文章翻译](../Page/Wikipedia_talk:文章翻译.md "wikilink")（[操作建议](../Page/Wikipedia:文章翻译.md "wikilink")）
3.  [管理研究](../Page/Wikipedia_talk:管理研究.md "wikilink")（[操作建议](../Page/Wikipedia:管理研究.md "wikilink")）

撰写体例

1.  [主题首页](../Page/Portal_talk:首頁.md "wikilink")（[操作建议](../Page/Portal:首頁.md "wikilink")）
2.  [分类强化](../Page/Wikipedia_talk:分类强化.md "wikilink")（[操作建议](../Page/Wikipedia:分类强化.md "wikilink")）

分工协作

1.  [维基规划](../Page/Wikipedia_talk:维基规划.md "wikilink")（[操作建议](../Page/Wikipedia:维基规划.md "wikilink")）
2.  [维基推广](../Page/Wikipedia_talk:维基推广.md "wikilink")（[操作建议](../Page/Wikipedia:维基推广.md "wikilink")）
3.  [成员分工](../Page/Wikipedia_talk:成员分工.md "wikilink")（[操作建议](../Page/Wikipedia:成员分工.md "wikilink")）
4.  [垃圾清理](../Page/Wikipedia_talk:垃圾清理.md "wikilink")（[操作建议](../Page/Wikipedia:垃圾清理.md "wikilink")）

争议仲裁

1.  [投票讨论](../Page/Wikipedia_talk:投票.md "wikilink")（[投票操作](../Page/Wikipedia:投票.md "wikilink")）

意见感想

1.  [发展建议](../Page/Wikipedia_talk:发展建议.md "wikilink")（[操作建议](../Page/Wikipedia:发展建议.md "wikilink")）

目标规范

1.  [条目标准](../Page/Wikipedia_talk:条目标准.md "wikilink")（[操作建议](../Page/Wikipedia:条目标准.md "wikilink")）

技术问题

1.  [繁简处理](../Page/Wikipedia_talk:繁简处理/技术方案.md "wikilink")（[技术方案](../Page/Wikipedia:繁简处理/技术方案.md "wikilink")）

</div>

<noinclude> </noinclude>

[Category:專題模板](../Category/專題模板.md "wikilink")