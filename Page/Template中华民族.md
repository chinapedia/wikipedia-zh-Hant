**[白族](../Page/白族.md "wikilink")**{{.w}}[瓦鄉人](../Page/瓦鄉族.md "wikilink"){{.w}}[穿青人](../Page/穿青人.md "wikilink"){{.w}}[蔡家人](../Page/蔡家人.md "wikilink"){{.w}}[羿人](../Page/羿人.md "wikilink"){{.w}}[者来寨人](../Page/者来寨人.md "wikilink"){{.w}}[阿尔巴津人](../Page/阿尔巴津人.md "wikilink")

`|group2 = `[`藏緬語族`](../Page/藏緬語族.md "wikilink")
`|list2 = `**[`藏族`](../Page/藏族.md "wikilink")**`{{.w}}`**[`门巴族`](../Page/门巴族.md "wikilink")**`{{.w}}`**[`彝族`](../Page/彝族.md "wikilink")**`{{.w}}`**[`哈尼族`](../Page/哈尼族.md "wikilink")**`{{.w}}`**[`傈僳族`](../Page/傈僳族.md "wikilink")**`{{.w}}`**[`拉祜族`](../Page/拉祜族.md "wikilink")**`{{.w}}`**[`纳西族`](../Page/纳西族.md "wikilink")**`{{.w}}`**[`基诺族`](../Page/基诺族.md "wikilink")**`{{.w}}`**[`怒族`](../Page/怒族.md "wikilink")**`{{.w}}`**[`羌族`](../Page/羌族.md "wikilink")**`{{.w}}`**[`土家族`](../Page/土家族.md "wikilink")**`{{.w}}`**[`普米族`](../Page/普米族.md "wikilink")**`{{.w}}`[`普米藏人`](../Page/普米藏族.md "wikilink")`{{.w}}`**[`景颇族`](../Page/景颇族.md "wikilink")**`{{.w}}`**[`阿昌族`](../Page/阿昌族.md "wikilink")**`{{.w}}`**[`独龙族`](../Page/独龙族.md "wikilink")**`{{.w}}`**[`珞巴族`](../Page/珞巴族.md "wikilink")**`{{.w}}`[`嘉絨人`](../Page/嘉絨人.md "wikilink")`{{.w}}`[`白馬人`](../Page/白馬人.md "wikilink")`{{.w}}`[`僜人`](../Page/僜人.md "wikilink")`{{.w}}`[`夏尔巴人`](../Page/夏尔巴人.md "wikilink")`{{.w}}`[`木雅人`](../Page/木雅人.md "wikilink")`{{.w}}`[`毕苏人`](../Page/毕苏人.md "wikilink")`{{.w}}`[`阿侬人`](../Page/阿侬人.md "wikilink")`{{.w}}`[`载瓦人`](../Page/载瓦人.md "wikilink")`{{.w}}`[`勒墨人`](../Page/勒墨人.md "wikilink")`{{.w}}`[`尔苏人`](../Page/尔苏人.md "wikilink")`{{.w}}`[`纳木依人`](../Page/纳木依人.md "wikilink")
`|group3 = `[`壯侗語族`](../Page/壯侗語族.md "wikilink")

|list3 =
**[壮族](../Page/壮族.md "wikilink")**{{.w}}**[傣族](../Page/傣族.md "wikilink")**{{.w}}**[布依族](../Page/布依族.md "wikilink")**{{.w}}**[侗族](../Page/侗族.md "wikilink")**{{.w}}**[水族](../Page/水族.md "wikilink")**{{.w}}**[仫佬族](../Page/仫佬族.md "wikilink")**{{.w}}**[毛南族](../Page/毛南族.md "wikilink")**{{.w}}**[黎族](../Page/黎族.md "wikilink")**{{.w}}**[仡佬族](../Page/仡佬族.md "wikilink")**{{.w}}[撣族](../Page/撣族.md "wikilink"){{.w}}[拉基族](../Page/拉基族.md "wikilink"){{.w}}[布标族](../Page/布标族.md "wikilink"){{.w}}[臨高人](../Page/臨高人.md "wikilink"){{.w}}[木佬人](../Page/木佬人.md "wikilink"){{.w}}[布央人](../Page/布央人.md "wikilink"){{.w}}[莫家人](../Page/莫家人.md "wikilink"){{.w}}[标人](../Page/标人.md "wikilink"){{.w}}[茶洞人](../Page/茶洞人.md "wikilink"){{.w}}[仡隆人](../Page/仡隆人.md "wikilink")

`|group4 = `[`苗瑤語族`](../Page/苗瑤語族.md "wikilink")

|list4 =
**[苗族](../Page/苗族.md "wikilink")**{{.w}}**[瑤族](../Page/瑶族.md "wikilink")**{{.w}}**[畲族](../Page/畲族.md "wikilink")**{{.w}}[巴天族](../Page/巴天族.md "wikilink"){{.w}}[绕家人](../Page/绕家人.md "wikilink"){{.w}}[布努人](../Page/布努人.md "wikilink"){{.w}}[包瑙人](../Page/包瑙人.md "wikilink")
}} |group2 = [阿尔泰语系](../Page/阿尔泰语系.md "wikilink") |list2 =
**[东乡族](../Page/东乡族.md "wikilink")**{{.w}}**[土族](../Page/土族.md "wikilink")**{{.w}}**[达斡尔族](../Page/达斡尔族.md "wikilink")**{{.w}}**[保安族](../Page/保安族.md "wikilink")**{{.w}}**[裕固族](../Page/裕固族.md "wikilink")**（東部）{{.w}}[蒙古回回](../Page/蒙古回回.md "wikilink")（[蒙古族穆斯林](../Page/蒙古族穆斯林.md "wikilink"){{.w}}[托茂人](../Page/托茂人.md "wikilink")）{{.w}}[康家人](../Page/康家人.md "wikilink")

`|group2 = `[`滿－通古斯語族`](../Page/滿－通古斯語族.md "wikilink")

|list2 =
**[满族](../Page/满族.md "wikilink")**{{.w}}**[锡伯族](../Page/锡伯族.md "wikilink")**{{.w}}**[鄂温克族](../Page/鄂温克族.md "wikilink")**{{.w}}**[赫哲族](../Page/赫哲族.md "wikilink")**{{.w}}**[鄂伦春族](../Page/鄂伦春族.md "wikilink")**{{.w}}[翁阔人](../Page/新疆索伦人.md "wikilink")

`|group3 = `[`突厥語族`](../Page/突厥語族.md "wikilink")

|list3 =
**[维吾尔族](../Page/维吾尔族.md "wikilink")**{{.w}}**[哈薩克族](../Page/中國哈薩克族.md "wikilink")**{{.w}}**[柯尔克孜族](../Page/柯尔克孜族.md "wikilink")**{{.w}}**[撒拉族](../Page/撒拉族.md "wikilink")**{{.w}}**[乌孜别克族](../Page/乌孜别克族.md "wikilink")**{{.w}}**[塔塔尔族](../Page/塔塔尔族.md "wikilink")**{{.w}}**[裕固族](../Page/裕固族.md "wikilink")**（西部）{{.w}}[圖瓦人](../Page/圖瓦人.md "wikilink"){{.w}}[富裕柯尔克孜人](../Page/乌裕尔河畔五姓柯尔克孜.md "wikilink")（[哈卡斯人](../Page/哈卡斯人.md "wikilink")）{{.w}}[艾努人](../Page/艾努人.md "wikilink"){{.w}}[克里雅人](../Page/克里雅人.md "wikilink"){{.w}}[雅庫特人](../Page/雅庫特人.md "wikilink")
}} |group3 = [南亞語系](../Page/南亞語系.md "wikilink") |list3 =
**[佤族](../Page/佤族.md "wikilink")**{{.w}}**[布朗族](../Page/布朗族.md "wikilink")**{{.w}}**[京族](../Page/京族.md "wikilink")**{{.w}}**[德昂族](../Page/德昂族.md "wikilink")**{{.w}}[克木族](../Page/克木族.md "wikilink"){{.w}}[莽族](../Page/莽族.md "wikilink"){{.w}}[曼咪人](../Page/曼咪人.md "wikilink")
|group4 = [印歐語系](../Page/印歐語系.md "wikilink") |list4 =
**[俄羅斯族](../Page/中国俄罗斯族.md "wikilink")**{{.w}}**[塔吉克族](../Page/中國塔吉克族.md "wikilink")**{{.w}}[土生葡人](../Page/澳門土生葡人.md "wikilink"){{.w}}[达曼人](../Page/在华尼泊尔人.md "wikilink"){{.w}}（[中印混血儿](../Page/中印混血儿.md "wikilink")）
|group5 = [南島語系](../Page/南島語系.md "wikilink") |list5 =
[占族](../Page/占族.md "wikilink")（[回輝人](../Page/回輝人.md "wikilink")）{{.w}}[菲律宾人](../Page/在华菲律宾人.md "wikilink")
|group6 = 其他 |list6 =
**[朝鮮族](../Page/中國朝鮮族.md "wikilink")**{{.w}}[和族](../Page/日裔中国人.md "wikilink")（[遗华日侨](../Page/遗华日侨.md "wikilink")）{{.w}}[犹太人](../Page/中国犹太人.md "wikilink"){{.w}}[未识别民族](../Page/中国未识别民族.md "wikilink")
|below =
粗體字為中華人民共和國國務院認定的56民族，其中[高山族和](../Page/高山族.md "wikilink")[臺灣原住民族有分類部分重疊](../Page/臺灣原住民族.md "wikilink")、爭議處。
參見：[中华民族](../Page/中华民族.md "wikilink"){{.w}}[中国各民族生育率表](../Page/中国各民族生育率表.md "wikilink"){{.w}}[中國境內的民族和族群
(按區域分類)](../Page/Template:中华民族分类.md "wikilink"){{.w}}[中华民族分类索引](../Category/中华民族.md "wikilink")
}}<noinclude>

</noinclude>

[\*](../Category/中华民族.md "wikilink")
[Category:中国民族导航模板](../Category/中国民族导航模板.md "wikilink")