<noinclude></noinclude><noinclude></noinclude>[3](福爾摩沙高速公路.md "wikilink"){{.w}}[5](蔣渭水高速公路.md "wikilink"){{.w}}[6](水沙連高速公路.md "wikilink")
|group2 = 支線、環線 |list2 =
[2](國道二號_\(中華民國\).md "wikilink"){{.w}}[3甲](國道三號甲線.md "wikilink"){{.w}}[4](國道四號_\(中華民國\).md "wikilink"){{.w}}[8](國道八號_\(中華民國\).md "wikilink"){{.w}}[10](國道十號_\(中華民國\).md "wikilink")
|group3 = 計劃路線 |list3 =
[7](國道七號_\(中華民國\).md "wikilink"){{.w}}[1甲](桃園航空城北側聯外高速公路.md "wikilink"){{.w}}[2甲](國道二號甲線.md "wikilink")
|group4 = 幹線拓寬線 |list4 =
[汐五高架](汐止五股高架道路.md "wikilink"){{.w}}[五楊高架](五股楊梅高架道路.md "wikilink")
|group5 = 道路設施 |list5 =
[交流道](w:zh:../Category/台灣高速公路交流道.md "wikilink")（[國1](中山高速公路交流道列表.md "wikilink"){{.w}}[國3](福爾摩沙高速公路交流道列表.md "wikilink"){{.w}}[國5](蔣渭水高速公路交流道列表.md "wikilink")）{{.w}}[電子收費系統](高速公路電子收費系統_\(臺灣\).md "wikilink"){{.w}}<s>[收費站](中華民國國道收費站列表.md "wikilink")</s>{{.w}}[服務區及休息站](中華民國國道服務區列表.md "wikilink")
|group6 = 業務機關 |list6 =
[交通部](中華民國交通部.md "wikilink"){{.w}}[高速公路局](交通部高速公路局.md "wikilink"){{.w}}<s>[國道工程局](交通部臺灣區國道新建工程局.md "wikilink")</s>{{.w}}[國道公路警察局](內政部警政署國道公路警察局.md "wikilink")
|belowstyle = background:\#99cc99;
|below=[中華民國公路分級](臺灣公路.md "wikilink")：[國道](中華民國國道.md "wikilink")
{{\!}} [省道](臺灣省道.md "wikilink") {{\!}}
[縣道](中華民國縣道.md "wikilink")（[市道](中華民國市道.md "wikilink")）
{{\!}} [鄉道](台灣鄉道.md "wikilink")（[區道](台灣區道.md "wikilink")） {{\!}}
[專用公路](台灣專用公路.md "wikilink") }}<noinclude> </noinclude>

[Category:各国高速公路模板](../Category/各国高速公路模板.md "wikilink")
[台灣公路模板](../Category/台灣公路模板.md "wikilink")