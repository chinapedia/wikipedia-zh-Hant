[富邦悍將](富邦悍將.md "wikilink"){{.w}}[中信兄弟](中信兄弟.md "wikilink")

`  |group2=二軍球隊`
`  | list2=`[`統一二軍`](統一二軍.md "wikilink")`{{.w}}`[`Lamigo二軍`](Lamigo二軍.md "wikilink")`{{.w}}`[`富邦二軍`](富邦二軍.md "wikilink")`{{.w}}`[`中信兄弟二軍`](中信兄弟二軍.md "wikilink")`{{.w}}`
`   |group4=歷史球隊`
`  | list4=`[`味全龍`](味全龍.md "wikilink")`{{.w}}`[`三商虎`](三商虎.md "wikilink")`{{.w}}`[`時報鷹`](時報鷹.md "wikilink")`{{.w}}`[`俊國熊`](俊國熊.md "wikilink")`{{.w}}`[`興農熊`](興農熊.md "wikilink")`{{.w}}`[`那魯灣太陽`](誠泰太陽.md "wikilink")`{{.w}}`[`誠泰太陽`](誠泰太陽.md "wikilink")`{{.w}}`[`第一金剛`](第一金剛.md "wikilink")`{{.w}}`[`和信鯨`](和信鯨.md "wikilink")`{{.w}}`[`誠泰COBRAS`](誠泰COBRAS.md "wikilink")`{{.w}}`[`米迪亞暴龍`](米迪亞暴龍.md "wikilink")`{{.w}}`[`中信鯨`](中信鯨.md "wikilink")`{{.w}}`[`La``
 ``New熊`](Lamigo桃猿.md "wikilink")`{{.w}}`[`興農牛`](興農牛.md "wikilink")`{{.w}}`[`兄弟象`](兄弟象.md "wikilink")`{{.w}}`[`義大犀牛`](義大犀牛.md "wikilink")
`  |group5=歷史二軍球隊`
`  | list5=`[`代訓紅隊`](代訓紅隊.md "wikilink")`{{.w}}`[`代訓藍隊`](代訓藍隊.md "wikilink")`{{.w}}`[`興農二軍`](興農二軍.md "wikilink")`{{.w}}`[`兄弟二軍`](兄弟二軍.md "wikilink")`{{.w}}`[`義大二軍`](義大二軍.md "wikilink")
`  |group6=未成立球隊`
`  | list6=`[`聲寶巨人`](台灣大聯盟.md "wikilink")`{{.w}}`[`九禾龍`](九禾龍.md "wikilink")
` }}`

| group2 = 賽事 | list2 =

` `

| group3 = 球季 | list3 =

` `[`1991`](中華職棒2年.md "wikilink")`{{!wrap}}`[`1992`](中華職棒3年.md "wikilink")`{{!wrap}}`[`1993`](中華職棒4年.md "wikilink")`{{!wrap}}`[`1994`](中華職棒5年.md "wikilink")`{{!wrap}}`[`1995`](中華職棒6年.md "wikilink")`{{!wrap}}`[`1996`](中華職棒7年.md "wikilink")`{{!wrap}}`[`1997`](中華職棒8年.md "wikilink")`{{!wrap}}`[`1998`](中華職棒9年.md "wikilink")`{{!wrap}}`[`1999`](中華職棒10年.md "wikilink")
`  | list2=`[`2000`](中華職棒11年.md "wikilink")`{{!wrap}}`[`2001`](中華職棒12年.md "wikilink")`{{!wrap}}`[`2002`](中華職棒13年.md "wikilink")`{{!wrap}}`[`2003`](中華職棒14年.md "wikilink")`{{!wrap}}`[`2004`](中華職棒15年.md "wikilink")`{{!wrap}}`[`2005`](中華職棒16年.md "wikilink")`{{!wrap}}`[`2006`](中華職棒17年.md "wikilink")`{{!wrap}}`[`2007`](中華職棒18年.md "wikilink")`{{!wrap}}`[`2008`](中華職棒19年.md "wikilink")`{{!wrap}}`[`2009`](中華職棒20年.md "wikilink")
`  | list3=`[`2010`](中華職棒21年.md "wikilink")`{{!wrap}}`[`2011`](中華職棒22年.md "wikilink")`{{!wrap}}`[`2012`](中華職棒23年.md "wikilink")`{{!wrap}}`[`2013`](中華職棒24年.md "wikilink")`{{!wrap}}`[`2014`](中華職棒25年.md "wikilink")`{{!wrap}}`[`2015`](中華職棒26年.md "wikilink")`{{!wrap}}`[`2016`](中華職棒27年.md "wikilink")`{{!wrap}}`[`2017`](中華職棒28年.md "wikilink")`{{!wrap}}`[`2018`](中華職棒29年.md "wikilink")`{{!wrap}}`[`2019`](中華職棒30年.md "wikilink")
` }}`

| group4 = 獎項 | list4 = [勝投王](中華職棒勝投王.md "wikilink") -
[三振王](中華職棒三振王.md "wikilink") -
[中繼王](中華職棒中繼王.md "wikilink") -
[救援王](中華職棒救援王.md "wikilink") -
[防禦率王](中華職棒防禦率王.md "wikilink") -
[全壘打王](中華職棒全壘打王.md "wikilink") -
[安打王](中華職棒安打王.md "wikilink") -
[打點王](中華職棒打點王.md "wikilink") -
[打擊王](中華職棒打擊王.md "wikilink") -
[盜壘王](中華職棒盜壘王.md "wikilink") -
[最佳十人](中華職棒最佳十人獎.md "wikilink") -
[金手套](中華職棒金手套獎.md "wikilink") -
[最佳進步獎](中華職棒最佳進步獎.md "wikilink") -
[新人王](中華職棒年度新人王.md "wikilink") -
[年度MVP](中華職棒年度最有價值球員.md "wikilink") -
[最佳總教練](中華職棒最佳總教練.md "wikilink") -
[台灣大賽最有價值球員](中華職棒總冠軍賽最有價值球員獎.md "wikilink")
- [台灣大賽優秀球員](中華職棒總冠軍賽優秀球員獎.md "wikilink") -
[單月MVP](中華職棒單月最有價值球員獎.md "wikilink") -
[明星賽MVP](中華職棒明星賽最有價值球員.md "wikilink") | below =
相關條目：[中華職棒年度口號](中華職棒年度口號.md "wikilink"){{.w}}[中華職棒歷年選秀會](中華職棒歷年選秀會.md "wikilink"){{.w}}[中華職棒會長](中華職棒會長.md "wikilink"){{.w}}[台灣棒球史](台灣棒球史.md "wikilink"){{.w}}[中華職棒重要紀錄](中華職棒重要紀錄.md "wikilink")
}}<noinclude> </noinclude>

[](../Category/台灣棒球模板.md "wikilink")