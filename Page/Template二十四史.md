<table>
<caption><a href="二十四史.md" title="wikilink">二十四史</a></caption>
<thead>
<tr class="header">
<th><p>序</p></th>
<th><p>书名</p></th>
<th><p>作者</p></th>
<th><p>今本<br />
卷數</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="史记.md" title="wikilink">史记</a></p></td>
<td><p>［<a href="西汉.md" title="wikilink">西汉</a>］<a href="司馬遷.md" title="wikilink">司馬遷</a></p></td>
<td><p>130</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="汉书.md" title="wikilink">汉书</a></p></td>
<td><p>［<a href="东汉.md" title="wikilink">东汉</a>］<a href="班固.md" title="wikilink">班固</a></p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="后汉书.md" title="wikilink">后汉书</a></p></td>
<td><p>［<a href="刘宋.md" title="wikilink">刘宋</a>］<a href="范曄_(史家).md" title="wikilink">范曄</a></p></td>
<td><p>120</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="三國志.md" title="wikilink">三國志</a></p></td>
<td><p>［<a href="西晋.md" title="wikilink">西晋</a>］<a href="陈寿.md" title="wikilink">陈寿</a></p></td>
<td><p>65</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="晋书.md" title="wikilink">晋书</a></p></td>
<td><p>［<a href="唐朝.md" title="wikilink">唐</a>］<a href="房玄龄.md" title="wikilink">房玄龄</a> 等</p></td>
<td><p>130</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="宋书.md" title="wikilink">宋书</a></p></td>
<td><p>［<a href="梁_(南朝).md" title="wikilink">梁</a>］<a href="沈約.md" title="wikilink">沈約</a></p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="南齐书.md" title="wikilink">南齐书</a></p></td>
<td><p>［梁］<a href="蕭子顯.md" title="wikilink">蕭子顯</a></p></td>
<td><p>59</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><a href="梁书.md" title="wikilink">梁书</a></p></td>
<td><p>［唐］<a href="姚思廉.md" title="wikilink">姚思廉</a></p></td>
<td><p>56</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p><a href="陈书.md" title="wikilink">陈书</a></p></td>
<td><p>36</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p><a href="魏书.md" title="wikilink">魏书</a></p></td>
<td><p>［<a href="北齐.md" title="wikilink">北齐</a>］<a href="魏收.md" title="wikilink">魏收</a></p></td>
<td><p>114</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p><a href="北齐书.md" title="wikilink">北齐书</a></p></td>
<td><p>［唐］<a href="李百藥.md" title="wikilink">李百藥</a></p></td>
<td><p>50</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p><a href="周书.md" title="wikilink">周书</a></p></td>
<td><p>［唐］<a href="令狐德棻.md" title="wikilink">令狐德棻</a> 等</p></td>
<td><p>50</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p><a href="隋书.md" title="wikilink">隋书</a></p></td>
<td><p>［唐］<a href="魏徵.md" title="wikilink">魏-{徵}-</a> 等</p></td>
<td><p>85</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p><a href="南史.md" title="wikilink">南史</a></p></td>
<td><p>［唐］<a href="李延壽.md" title="wikilink">李延壽</a></p></td>
<td><p>80</p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p><a href="北史.md" title="wikilink">北史</a></p></td>
<td><p>100</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p><a href="旧唐书.md" title="wikilink">旧唐书</a></p></td>
<td><p>［<a href="后晋.md" title="wikilink">后晋</a>］<a href="劉昫.md" title="wikilink">劉昫</a> 等</p></td>
<td><p>200</p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p><a href="新唐书.md" title="wikilink">新唐書</a></p></td>
<td><p>［<a href="北宋.md" title="wikilink">北宋</a>］<a href="欧阳修.md" title="wikilink">欧阳修</a> 等</p></td>
<td><p>225</p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p><a href="旧五代史.md" title="wikilink">旧五代史</a></p></td>
<td><p>［北宋］<a href="薛居正.md" title="wikilink">薛居正</a> 等</p></td>
<td><p>150</p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p><a href="新五代史.md" title="wikilink">新五代史</a></p></td>
<td><p>［北宋］<a href="欧阳修.md" title="wikilink">欧阳修</a></p></td>
<td><p>74</p></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p><a href="宋史.md" title="wikilink">宋史</a></p></td>
<td><p>［<a href="元朝.md" title="wikilink">元</a>］<a href="脱脱.md" title="wikilink">脱脱</a> 等</p></td>
<td><p>496</p></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p><a href="辽史.md" title="wikilink">辽史</a></p></td>
<td><p>116</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p><a href="金史.md" title="wikilink">金史</a></p></td>
<td><p>135</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td><p><a href="元史.md" title="wikilink">元史</a></p></td>
<td><p>［<a href="明朝.md" title="wikilink">明</a>］<a href="宋濂.md" title="wikilink">宋濂</a> 等</p></td>
<td><p>210</p></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td><p><a href="明史.md" title="wikilink">明史</a></p></td>
<td><p>［<a href="清朝.md" title="wikilink">清</a>］<a href="张廷玉.md" title="wikilink">张廷玉</a> <a href="明史館.md" title="wikilink">等</a></p></td>
<td><p>332</p></td>
</tr>
<tr class="odd">
<td><p>相關</p></td>
<td><p><a href="東觀漢記.md" title="wikilink">東觀漢記</a></p></td>
<td><p>［<a href="東漢.md" title="wikilink">東漢</a>］<a href="劉珍_(東漢).md" title="wikilink">劉珍</a> 等</p></td>
<td><p>22</p></td>
</tr>
<tr class="even">
<td><p>相關</p></td>
<td><p><a href="新元史.md" title="wikilink">新元史</a></p></td>
<td><p>［<a href="北洋政府.md" title="wikilink">民國</a>］<a href="柯劭忞.md" title="wikilink">柯劭忞</a></p></td>
<td><p>257</p></td>
</tr>
<tr class="odd">
<td><p>相關</p></td>
<td><p><a href="清史稿.md" title="wikilink">清史稿</a></p></td>
<td><p>［<a href="北洋政府.md" title="wikilink">民國</a>］<a href="赵尔巽.md" title="wikilink">赵尔巽</a> <a href="清史館.md" title="wikilink">等</a></p></td>
<td><p>529</p></td>
</tr>
</tbody>
</table>

<noinclude>

</noinclude>

[二十四史](../Category/二十四史.md "wikilink")
[Category:中国历史导航模板](../Category/中国历史导航模板.md "wikilink")
[Category:中国系列书籍导航模板](../Category/中国系列书籍导航模板.md "wikilink")