[盘龙区](../Page/盘龙区.md "wikilink"){{.w}}[官渡区](../Page/官渡区.md "wikilink"){{.w}}[西山区](../Page/西山区_\(昆明市\).md "wikilink"){{.w}}[东川区](../Page/东川区.md "wikilink"){{.w}}[晋宁区](../Page/晋宁区.md "wikilink"){{.w}}[安宁市](../Page/安宁市.md "wikilink"){{.w}}[富民县](../Page/富民县.md "wikilink"){{.w}}[宜良县](../Page/宜良县.md "wikilink"){{.w}}[嵩明县](../Page/嵩明县.md "wikilink"){{.w}}[石林彝族自治县](../Page/石林彝族自治县.md "wikilink"){{.w}}[禄劝彝族苗族自治县](../Page/禄劝彝族苗族自治县.md "wikilink"){{.w}}[寻甸回族彝族自治县](../Page/寻甸回族彝族自治县.md "wikilink")

|group3=[曲靖市](../Page/曲靖市.md "wikilink")
|list3=[麒麟区](../Page/麒麟区.md "wikilink"){{.w}}[沾益区](../Page/沾益区.md "wikilink"){{.w}}[马龙区](../Page/马龙区.md "wikilink"){{.w}}[宣威市](../Page/宣威市.md "wikilink"){{.w}}[陆良县](../Page/陆良县.md "wikilink"){{.w}}[师宗县](../Page/师宗县.md "wikilink"){{.w}}[罗平县](../Page/罗平县.md "wikilink"){{.w}}[富源县](../Page/富源县.md "wikilink"){{.w}}[会泽县](../Page/会泽县.md "wikilink")

|group4=[玉溪市](../Page/玉溪市.md "wikilink")
|list4=[红塔区](../Page/红塔区.md "wikilink"){{.w}}[江川区](../Page/江川区.md "wikilink"){{.w}}[澄江县](../Page/澄江县.md "wikilink"){{.w}}[通海县](../Page/通海县.md "wikilink"){{.w}}[华宁县](../Page/华宁县.md "wikilink"){{.w}}[易门县](../Page/易门县.md "wikilink"){{.w}}[峨山彝族自治县](../Page/峨山彝族自治县.md "wikilink"){{.w}}[新平彝族傣族自治县](../Page/新平彝族傣族自治县.md "wikilink"){{.w}}[元江哈尼族彝族傣族自治县](../Page/元江哈尼族彝族傣族自治县.md "wikilink")

|group5=[保山市](../Page/保山市.md "wikilink")
|list5=[隆阳区](../Page/隆阳区.md "wikilink"){{.w}}[腾冲市](../Page/腾冲市.md "wikilink"){{.w}}[施甸县](../Page/施甸县.md "wikilink"){{.w}}[龙陵县](../Page/龙陵县.md "wikilink"){{.w}}[昌宁县](../Page/昌宁县.md "wikilink")

|group6=[昭通市](../Page/昭通市.md "wikilink")
|list6=[昭阳区](../Page/昭阳区.md "wikilink"){{.w}}[水富市](../Page/水富市.md "wikilink"){{.w}}[鲁甸县](../Page/鲁甸县.md "wikilink"){{.w}}[巧家县](../Page/巧家县.md "wikilink"){{.w}}[盐津县](../Page/盐津县.md "wikilink"){{.w}}[大关县](../Page/大关县.md "wikilink"){{.w}}[永善县](../Page/永善县.md "wikilink"){{.w}}[绥江县](../Page/绥江县.md "wikilink"){{.w}}[镇雄县](../Page/镇雄县.md "wikilink"){{.w}}[彝良县](../Page/彝良县.md "wikilink"){{.w}}[威信县](../Page/威信县.md "wikilink")

|group7=[丽江市](../Page/丽江市.md "wikilink")
|list7=[古城区](../Page/古城区.md "wikilink"){{.w}}[永胜县](../Page/永胜县.md "wikilink"){{.w}}[华坪县](../Page/华坪县.md "wikilink"){{.w}}[玉龙纳西族自治县](../Page/玉龙纳西族自治县.md "wikilink"){{.w}}[宁蒗彝族自治县](../Page/宁蒗彝族自治县.md "wikilink")

|group8=[普洱市](../Page/普洱市.md "wikilink")
|list8=[思茅区](../Page/思茅区.md "wikilink"){{.w}}[宁洱哈尼族彝族自治县](../Page/宁洱哈尼族彝族自治县.md "wikilink"){{.w}}[墨江哈尼族自治县](../Page/墨江哈尼族自治县.md "wikilink"){{.w}}[景东彝族自治县](../Page/景东彝族自治县.md "wikilink"){{.w}}[景谷傣族彝族自治县](../Page/景谷傣族彝族自治县.md "wikilink"){{.w}}[镇沅彝族哈尼族拉祜族自治县](../Page/镇沅彝族哈尼族拉祜族自治县.md "wikilink"){{.w}}[江城哈尼族彝族自治县](../Page/江城哈尼族彝族自治县.md "wikilink"){{.w}}[孟连傣族拉祜族佤族自治县](../Page/孟连傣族拉祜族佤族自治县.md "wikilink"){{.w}}[澜沧拉祜族自治县](../Page/澜沧拉祜族自治县.md "wikilink"){{.w}}[西盟佤族自治县](../Page/西盟佤族自治县.md "wikilink")

|group9=[临沧市](../Page/临沧市.md "wikilink")
|list9=[临翔区](../Page/临翔区.md "wikilink"){{.w}}[凤庆县](../Page/凤庆县.md "wikilink"){{.w}}[云县](../Page/云县.md "wikilink"){{.w}}[永德县](../Page/永德县.md "wikilink"){{.w}}[镇康县](../Page/镇康县.md "wikilink"){{.w}}[双江拉祜族佤族布朗族傣族自治县](../Page/双江拉祜族佤族布朗族傣族自治县.md "wikilink"){{.w}}[耿马傣族佤族自治县](../Page/耿马傣族佤族自治县.md "wikilink"){{.w}}[沧源佤族自治县](../Page/沧源佤族自治县.md "wikilink")
}} |group3style = text-align: center; |group3 =
[自治州](../Page/自治州.md "wikilink") |list3 =
[双柏县](../Page/双柏县.md "wikilink"){{.w}}[牟定县](../Page/牟定县.md "wikilink"){{.w}}[南华县](../Page/南华县.md "wikilink"){{.w}}[姚安县](../Page/姚安县.md "wikilink"){{.w}}[大姚县](../Page/大姚县.md "wikilink"){{.w}}[永仁县](../Page/永仁县.md "wikilink"){{.w}}[元谋县](../Page/元谋县.md "wikilink"){{.w}}[武定县](../Page/武定县.md "wikilink"){{.w}}[禄丰县](../Page/禄丰县.md "wikilink")

|group11=[红河哈尼族彝族自治州](../Page/红河哈尼族彝族自治州.md "wikilink")
|list11=[蒙自市](../Page/蒙自市.md "wikilink"){{.w}}[个旧市](../Page/个旧市.md "wikilink"){{.w}}[开远市](../Page/开远市.md "wikilink"){{.w}}[弥勒市](../Page/弥勒市.md "wikilink"){{.w}}[建水县](../Page/建水县.md "wikilink"){{.w}}[石屏县](../Page/石屏县.md "wikilink"){{.w}}[泸西县](../Page/泸西县.md "wikilink"){{.w}}[元阳县](../Page/元阳县.md "wikilink"){{.w}}[红河县](../Page/红河县.md "wikilink"){{.w}}[绿春县](../Page/绿春县.md "wikilink"){{.w}}[屏边苗族自治县](../Page/屏边苗族自治县.md "wikilink"){{.w}}[金平苗族瑶族傣族自治县](../Page/金平苗族瑶族傣族自治县.md "wikilink"){{.w}}[河口瑶族自治县](../Page/河口瑶族自治县.md "wikilink")

|group12=[文山壮族苗族自治州](../Page/文山壮族苗族自治州.md "wikilink")
|list12=[文山市](../Page/文山市.md "wikilink"){{.w}}[砚山县](../Page/砚山县.md "wikilink"){{.w}}[西畴县](../Page/西畴县.md "wikilink"){{.w}}[麻栗坡县](../Page/麻栗坡县.md "wikilink"){{.w}}[马关县](../Page/马关县.md "wikilink"){{.w}}[丘北县](../Page/丘北县.md "wikilink"){{.w}}[广南县](../Page/广南县.md "wikilink"){{.w}}[富宁县](../Page/富宁县.md "wikilink")

|group13=[西双版纳傣族自治州](../Page/西双版纳傣族自治州.md "wikilink")
|list13=[景洪市](../Page/景洪市.md "wikilink"){{.w}}[勐海县](../Page/勐海县.md "wikilink"){{.w}}[勐腊县](../Page/勐腊县.md "wikilink")

|group14=[大理白族自治州](../Page/大理白族自治州.md "wikilink")
|list14=[大理市](../Page/大理市.md "wikilink"){{.w}}[祥云县](../Page/祥云县.md "wikilink"){{.w}}[宾川县](../Page/宾川县.md "wikilink"){{.w}}[弥渡县](../Page/弥渡县.md "wikilink"){{.w}}[永平县](../Page/永平县.md "wikilink"){{.w}}[云龙县](../Page/云龙县.md "wikilink"){{.w}}[洱源县](../Page/洱源县.md "wikilink"){{.w}}[剑川县](../Page/剑川县.md "wikilink"){{.w}}[鹤庆县](../Page/鹤庆县.md "wikilink"){{.w}}[漾濞彝族自治县](../Page/漾濞彝族自治县.md "wikilink"){{.w}}[南涧彝族自治县](../Page/南涧彝族自治县.md "wikilink"){{.w}}[巍山彝族回族自治县](../Page/巍山彝族回族自治县.md "wikilink")

|group15=[德宏傣族景颇族自治州](../Page/德宏傣族景颇族自治州.md "wikilink")
|list15=[芒市](../Page/芒市.md "wikilink"){{.w}}[瑞丽市](../Page/瑞丽市.md "wikilink"){{.w}}[梁河县](../Page/梁河县.md "wikilink"){{.w}}[盈江县](../Page/盈江县.md "wikilink"){{.w}}[陇川县](../Page/陇川县.md "wikilink")

|group16=[怒江傈僳族自治州](../Page/怒江傈僳族自治州.md "wikilink")
|list16=[泸水市](../Page/泸水市.md "wikilink"){{.w}}[福贡县](../Page/福贡县.md "wikilink"){{.w}}[贡山独龙族怒族自治县](../Page/贡山独龙族怒族自治县.md "wikilink"){{.w}}[兰坪白族普米族自治县](../Page/兰坪白族普米族自治县.md "wikilink")

|group17=[迪庆藏族自治州](../Page/迪庆藏族自治州.md "wikilink")
|list17=[香格里拉市](../Page/香格里拉市.md "wikilink"){{.w}}[德钦县](../Page/德钦县.md "wikilink"){{.w}}[维西傈僳族自治县](../Page/维西傈僳族自治县.md "wikilink")
}}

|belowstyle = text-align: left; font-size: 80%; |below =
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[云南省乡级以上行政区列表](../Page/云南省乡级以上行政区列表.md "wikilink")。
}}<noinclude>

</noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[Category:中华人民共和国二级行政区划导航模板](../Category/中华人民共和国二级行政区划导航模板.md "wikilink")
[\*](../Category/云南行政区划.md "wikilink")
[云南行政区划模板](../Category/云南行政区划模板.md "wikilink")