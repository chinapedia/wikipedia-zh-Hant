<noinclude>  </noinclude> [1996年 哈爾濱](1996年亞洲冬季運動會.md "wikilink"){{.w}}
[1999年 江原道](1999年亞洲冬季運動會.md "wikilink"){{.w}} [2003年
青森](2003年亞洲冬季運動會.md "wikilink"){{.w}} [2007年
長春](2007年亞洲冬季運動會.md "wikilink"){{.w}} [2011年
阿斯塔納－阿拉木圖](2011年亞洲冬季運動會.md "wikilink"){{.w}}
[2017年 札幌](2017年亞洲冬季運動會.md "wikilink") }}

|group2=[亞洲室內暨武藝運動會](亞洲室內暨武藝運動會.md "wikilink") |list2= [2007年
澳門](2007年亞洲室內運動會.md "wikilink"){{.w}} [2009年
河內](2009年亞洲室內運動會.md "wikilink")

`  |group2=`[`武藝運動會`](亞洲武藝運動會.md "wikilink")
`  |list2=`` `[`2009年``   ``曼谷`](2009年亞洲武藝運動會.md "wikilink")
`  |group3=合併`
`  |list3=`` `[`2013年``
 ``仁川`](2013年亞洲室內暨武藝運動會.md "wikilink")`{{.w}} `` `[`2017年``
 ``阿什哈巴德`](2017年亞洲室內暨武藝運動會.md "wikilink")

}}

|group3=[亞洲沙灘運動會](亞洲沙灘運動會.md "wikilink") |list3= [2008年
峇里](2008年亞洲沙灘運動會.md "wikilink"){{.w}} [2010年
馬斯喀特](2010年亞洲沙灘運動會.md "wikilink"){{.w}} [2012年
海陽](2012年亞洲沙灘運動會.md "wikilink"){{.w}} [2014年
普吉](2014年亞洲沙灘運動會.md "wikilink"){{.w}} [2016年
岘港](2016年亞洲沙灘運動會.md "wikilink"){{.w}}* [2020年
三亞](2020年亞洲沙灘運動會.md "wikilink")*
|group4=[亞洲青年運動會](亞洲青年運動會.md "wikilink")
|list4= [2009年 新加坡](2009年亞洲青年運動會.md "wikilink"){{.w}} [2013年
南京](2013年亞洲青年運動會.md "wikilink"){{.w}} <s>[2017年
雅加達](2017年亞洲青年運動會.md "wikilink")</s>（取消）{{,}} * [2021年
汕头](2021年亞洲青年運動會.md "wikilink")* |group5=其他區域性賽會
|list5=[東亞運動會](東亞運動會.md "wikilink")→[東亞青年運動會](東亞青年運動會.md "wikilink"){{.w}}[中亞運動會](中亞運動會.md "wikilink"){{.w}}[南亞運動會](南亞運動會.md "wikilink"){{.w}}[東南亞運動會](東南亞運動會.md "wikilink"){{.w}}[西亞運動會](西亞運動會.md "wikilink"){{.w}}[南亞冬季運動會](南亞冬季運動會.md "wikilink"){{.w}}[南亞沙灘運動會](南亞沙灘運動會.md "wikilink")

|belowclass = hlist |below=

  - [分类](:../Category/亚洲运动会.md "wikilink")

  - [主题](Portal:亚运会.md "wikilink")

  - [专题](Wikipedia:WikiProject_Multi-sport_events.md "wikilink")

}}<noinclude> </noinclude>

[Category:亞洲運動會模板](../Category/亞洲運動會模板.md "wikilink")