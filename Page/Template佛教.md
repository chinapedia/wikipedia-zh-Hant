{{ sidebar with collapsible lists | name = 佛教 | class = hlist
nounderlines | pretitle = | title = [佛教](佛教.md "wikilink") | image =
![Dharma_Wheel.svg](Dharma_Wheel.svg "Dharma_Wheel.svg") | style =
float: right; clear: both; border: 2px solid \#af4630; background:
\#ffffe8; text-align:center;width:200px; | liststyle = text-align: left;
| listtitlestyle = background: \#ffeec2;text-align: center; | list1title
= 基本教義 | list1name = Dharma | list1 =  | list2title =
[修行位階](修行位階.md "wikilink") | list2name = Concepts |
list2 =  | list3title =  | list3name = Peoples | list3 =  | list4title =
[宗派](佛教宗派.md "wikilink")、國家和地區 | list4name = Traditions | list4 =  |
list5title = [歷史](佛教歷史.md "wikilink") | list5name = Traditions | list5 =
 | list6title = [經籍舉要](佛经.md "wikilink") | list6name = Canons | list6 =
| list7title = [聖地](佛教聖地.md "wikilink") | list7name = Pilgrimages |
list7 =  | list8title = 相關主題 | list8name = Others | list8 =  |
belowclass = plainlist | below =
[18px](file:DharmaWheelGIF.gif.md "wikilink")
[佛教主題](portal:佛教.md "wikilink") | tnavbar = yes
}}<noinclude>

### 用法

如要使用本模板，请添加**{{佛教}}**，或**{{Buddhism}}**、**{{Buddhism2}}**。

</noinclude>

[佛教模板](../Category/佛教模板.md "wikilink")
[Category:宗教侧面模板](../Category/宗教侧面模板.md "wikilink")