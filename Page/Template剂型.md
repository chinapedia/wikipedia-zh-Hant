[滲透控釋口服給藥系統](../Page/滲透控釋口服給藥系統.md "wikilink")

`           | group2 = `[`液體`](../Page/液體.md "wikilink")
`           | list2 = `[`溶液`](../Page/溶液.md "wikilink")`{{•}}`[`軟膠囊`](../Page/軟膠囊.md "wikilink")`{{•}}`[`混懸劑`](../Page/懸浮液.md "wikilink")`{{•}}`[`乳劑`](../Page/乳劑.md "wikilink")`{{•}}`[`糖漿`](../Page/糖漿.md "wikilink")`{{•}}`[`酏劑`](../Page/酏劑.md "wikilink")`{{•}}`[`酊劑`](../Page/酊劑.md "wikilink")`{{•}}`[`水凝膠`](../Page/水凝膠.md "wikilink")

}}

`    | group2  = `[`頰間隙`](../Page/頰間隙.md "wikilink")`/`[`唇下`](../Page/唇下給藥.md "wikilink")`/`[`舌下`](../Page/舌下給藥.md "wikilink")` `
`    | list2  = `[`薄膜藥劑`](../Page/薄膜藥劑.md "wikilink")`{{•}}`[`棒棒糖`](../Page/棒棒糖.md "wikilink")`{{•}}`[`喉片`](../Page/喉片.md "wikilink")`{{•}}`[`口香糖`](../Page/口香糖.md "wikilink")
`           | group2 = `[`液體`](../Page/液體.md "wikilink")
`           | list2 = `[`漱口藥水`](../Page/漱口水.md "wikilink")`{{•}}`[`牙膏`](../Page/牙膏.md "wikilink")`{{•}}`[`外用軟膏`](../Page/外用.md "wikilink")`{{•}}`[`口噴霧劑`](../Page/口噴霧劑.md "wikilink")

}}

`    | group3 = `[`呼吸道`](../Page/呼吸道.md "wikilink")
`    | list3  = `[`乾粉吸入劑`](../Page/乾粉吸入劑.md "wikilink")
`           | group2 = `[`液体`](../Page/液体.md "wikilink")
`           | list2 = `[`計量吸入劑`](../Page/計量吸入劑.md "wikilink")`{{·}}`[`噴霧器`](../Page/噴霧器.md "wikilink")`{{·}}`[`麻醉汽化劑`](../Page/麻醉汽化劑.md "wikilink")
`           | group3 = `[`氣體`](../Page/氣體.md "wikilink")
`           | list3 = `[`氧氣面罩`](../Page/氧氣面罩.md "wikilink")`{{·}}`[`製氧機`](../Page/製氧機.md "wikilink")`{{·}}`[`麻醉機`](../Page/麻醉機.md "wikilink")`{{·}}`[`相對鎮痛機`](../Page/相對鎮痛機.md "wikilink")

}} }}

| group2 = [眼](../Page/眼.md "wikilink") / [耳](../Page/耳.md "wikilink") /
[鼻](../Page/鼻.md "wikilink") | list2 =
[鼻噴劑](../Page/鼻腔给药.md "wikilink"){{·}}[耳藥水](../Page/耳藥水.md "wikilink"){{·}}[眼藥水](../Page/眼藥水.md "wikilink"){{·}}[外用軟膏](../Page/外用.md "wikilink"){{·}}[水凝膠](../Page/水凝膠.md "wikilink"){{·}}[微球藥劑](../Page/微球藥劑.md "wikilink")

| group3 = [泌尿生殖系统](../Page/泌尿生殖系统.md "wikilink") | list3 =
[外用軟膏](../Page/外用.md "wikilink"){{·}}[陰道栓劑](../Page/陰道栓劑.md "wikilink"){{·}}[陰道環](../Page/陰道環.md "wikilink"){{·}}[陰道沖洗](../Page/陰道沖洗.md "wikilink"){{·}}[子宮環](../Page/子宮環.md "wikilink"){{·}}
[Extra-amniotic
infusion](../Page/Extra-amniotic_administration.md "wikilink"){{·}}[膀胱內灌注](../Page/膀胱.md "wikilink")

| group4 = [直腸給藥](../Page/直腸給藥.md "wikilink")
([腸給藥](../Page/腸給藥.md "wikilink")) | list4 =
[外用藥膏](../Page/外用.md "wikilink"){{·}}[栓剂](../Page/栓剂.md "wikilink"){{·}}[灌腸](../Page/灌肠_\(医学\).md "wikilink")
([溶液](../Page/溶液.md "wikilink"){{·}}[凝胶](../Page/凝胶.md "wikilink")){{·}}[Murphy
drip](../Page/Murphy_drip.md "wikilink"){{·}}[Nutrient
enema](../Page/Nutrient_enema.md "wikilink")

| group5 = [真皮](../Page/真皮.md "wikilink") | list5 =
[Ointment](../Page/Topical.md "wikilink"){{·}}[Liniment](../Page/Liniment.md "wikilink"){{·}}[Paste](../Page/Paste_\(rheology\).md "wikilink"){{·}}[Film](../Page/Thin_film_drug_delivery.md "wikilink"){{·}}[Hydrogel](../Page/Hydrogel.md "wikilink"){{·}}[脂质体](../Page/脂质体.md "wikilink"){{·}}[Transfersome
vesicals](../Page/Transfersome.md "wikilink"){{·}}[霜](../Page/霜_\(医药\).md "wikilink"){{·}}[露](../Page/露_\(医药\).md "wikilink"){{·}}[潤唇膏](../Page/潤唇膏.md "wikilink"){{·}}[藥用洗髮水](../Page/洗髮精.md "wikilink"){{·}}[Dermal
patch](../Page/Dermal_patch.md "wikilink"){{·}}[Transdermal
patch](../Page/Transdermal_patch.md "wikilink"){{·}}[Transdermal
spray](../Page/Metered_dose_transdermal_spray.md "wikilink"){{·}}[Jet
injector](../Page/Jet_injector.md "wikilink")

| group6 = [注射](../Page/注射.md "wikilink") /
[輸液](../Page/輸液泵.md "wikilink")
*(進入組織/血液)* | list6 =
[皮下注射](../Page/皮下注射.md "wikilink"){{·}}[Transdermal
implant](../Page/Transdermal_implant.md "wikilink")

`    | group2 = `[`器官`](../Page/人体.md "wikilink")
`    | list2  = `[`Intracavernous`](../Page/Intracavernous_injection.md "wikilink")`{{·}}`[`Intravitreal`](../Page/Intravitreal_administration.md "wikilink")`{{·}}`[`關節內注射液`](../Page/關節內注射液.md "wikilink")`{{·}}Transscleral`

`    | group3 = `[`中樞神經系統`](../Page/中樞神經系統.md "wikilink")` `
`    | list3  = Intracerebral{{·}}`[`鞘内注射`](../Page/鞘内.md "wikilink")`{{·}}`[`硬脊膜外麻醉`](../Page/硬脊膜外麻醉.md "wikilink")

`    | group4 = `[`循环系统`](../Page/循环系统.md "wikilink")` / `[`人體肌肉骨骼系統`](../Page/人體肌肉骨骼系統.md "wikilink")` `
`    | list4  = `[`靜脈注射`](../Page/靜脈注射.md "wikilink")`{{·}}`[`Intracardiac`](../Page/Intracardiac_injection.md "wikilink")`{{·}}`[`肌肉注射`](../Page/肌肉注射.md "wikilink")`{{·}}`[`Intraosseous`](../Page/Intraosseous_infusion.md "wikilink")`{{·}}`[`Intraperitoneal`](../Page/Intraperitoneal_injection.md "wikilink")`{{·}}`[`Nanocell``
 ``injection`](../Page/Nanocell.md "wikilink")

}}

| group7 = 附加說明: | list7 =

}}<noinclude> </noinclude>

[](../Category/药物导航模板.md "wikilink") [](../Category/给药途径.md "wikilink")
[剂型](../Category/剂型.md "wikilink")