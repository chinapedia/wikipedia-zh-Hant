[西什库天主堂](西什库天主堂.md "wikilink")（原主教座堂）{{.w}}[西直门天主堂](西直门天主堂.md "wikilink"){{.w}}[王府井天主堂](王府井天主堂.md "wikilink"){{.w}}[东交民巷天主堂](东交民巷天主堂.md "wikilink"){{.w}}[南岗子天主堂](南岗子天主堂.md "wikilink"){{.w}}[马尾沟教堂](马尾沟教堂.md "wikilink"){{.w}}[朝阳门天主堂](朝阳门内大街81号.md "wikilink"){{.w}}[正福寺天主堂](正福寺天主堂.md "wikilink"){{.w}}[平房天主堂](平房天主堂.md "wikilink"){{.w}}[西三旗天主堂](西三旗天主堂.md "wikilink"){{.w}}[西北旺天主堂](西北旺天主堂.md "wikilink"){{.w}}[白家疃天主堂](白家疃天主堂.md "wikilink"){{.w}}[后八家天主堂](北京天主教神哲学院.md "wikilink"){{.w}}[张家铺天主堂](张家铺天主堂.md "wikilink"){{.w}}[后桑峪天主堂](后桑峪天主堂.md "wikilink"){{.w}}[曹各庄天主堂](曹各庄天主堂.md "wikilink"){{.w}}[永宁天主堂](永宁天主堂.md "wikilink"){{.w}}[长辛店天主堂](长辛店天主堂.md "wikilink"){{.w}}[东管头天主堂](东管头天主堂.md "wikilink"){{.w}}[牛坊天主堂](牛坊天主堂.md "wikilink"){{.w}}[亦庄天主堂](亦庄天主堂.md "wikilink"){{.w}}[西胡林天主堂](西胡林天主堂.md "wikilink"){{.w}}[求贤天主堂](求贤天主堂.md "wikilink"){{.w}}[贾后疃天主堂](贾后疃天主堂.md "wikilink"){{.w}}[龙庄天主堂](龙庄天主堂.md "wikilink"){{.w}}[牛牧屯天主堂](牛牧屯天主堂.md "wikilink"){{.w}}[通州天主堂](通州天主堂.md "wikilink"){{.w}}[燕子口天主堂](燕子口天主堂.md "wikilink"){{.w}}[十三陵天主堂](十三陵天主堂.md "wikilink"){{.w}}[天通苑天主堂](天通苑天主堂.md "wikilink"){{.w}}[立教天主堂](立教天主堂.md "wikilink"){{.w}}[太和庄天主堂](太和庄天主堂.md "wikilink"){{.w}}[二站天主堂](二站天主堂.md "wikilink"){{.w}}[石景山天主堂](石景山天主堂.md "wikilink"){{.w}}[顺义天主堂](顺义天主堂.md "wikilink")

|group2 = [东正教](东正教北京传道团.md "wikilink") |list2 =
[圣母安息主教座堂、教众致命堂和圣英诺肯提十字堂](北馆.md "wikilink"){{.w}}[奉献节教堂](俄国使馆旧址.md "wikilink"){{.w}}[圣母堂](圣母堂_\(北京\).md "wikilink")

|group3 = [新　教](北京基督教新教.md "wikilink") |list3 =
[卫理公会亚斯立堂](亚斯立堂.md "wikilink"){{.w}}[卫理公会珠市口教堂](珠市口堂.md "wikilink"){{.w}}[中华圣公会救主堂](中华圣公会救主堂.md "wikilink"){{.w}}[八面槽救世军中央堂](八面槽救世军中央堂.md "wikilink"){{.w}}[北京协和礼拜堂](北京协和礼拜堂.md "wikilink"){{.w}}[中华基督教会缸瓦市教堂](中华基督教会缸瓦市教堂.md "wikilink"){{.w}}[宽街基督徒聚会处](宽街基督徒聚会处.md "wikilink"){{.w}}[公理会灯市口教堂](公理会灯市口教堂.md "wikilink"){{.w}}[海淀堂](海淀堂.md "wikilink"){{.w}}[伦敦会米市教堂](伦敦会米市教堂.md "wikilink"){{.w}}[基督徒会堂](基督徒会堂.md "wikilink"){{.w}}[长老会交道口教堂](长老会交道口教堂.md "wikilink"){{.w}}[长老会鼓楼西堂](长老会鼓楼西堂.md "wikilink"){{.w}}[永定门教堂](永定门教堂.md "wikilink"){{.w}}[西兴隆街救世军南队](西兴隆街救世军南队.md "wikilink"){{.w}}[救世军北队](救世军北队.md "wikilink"){{.w}}[救世军西北队](救世军西北队.md "wikilink"){{.w}}[花市教堂](花市教堂.md "wikilink"){{.w}}[广安门教堂](广安门教堂.md "wikilink"){{.w}}[大兴教堂](大兴教堂.md "wikilink"){{.w}}[南口教堂](南口教堂.md "wikilink"){{.w}}[通州教堂](通州教堂.md "wikilink"){{.w}}[朝阳堂](朝阳堂.md "wikilink")

}}<noinclude>

</noinclude>

[Category:中国教堂导航模板](../Category/中国教堂导航模板.md "wikilink")
[\*](../Category/北京市教堂.md "wikilink")
[Category:北京市建筑模板](../Category/北京市建筑模板.md "wikilink")