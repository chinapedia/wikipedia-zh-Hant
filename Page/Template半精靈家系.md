

人類和精靈結合，托爾金作品中還暗示有第四對人類和精靈的結合，不過不在此家譜中。 <noinclude>

## 原模版

### 原模版B

  -
    {|

|-align=center |[芬威](芬威.md "wikilink")|| || || || || || ||
[伊露維塔](伊露維塔.md "wikilink") |-align=center |||| || || ||
|| || ||||| |-align=center |[芬國昐](芬國昐.md "wikilink")||
||[高多](高多.md "wikilink")|| ||
||[埃盧·庭葛](埃盧·庭葛.md "wikilink")||===||[美麗安](美麗安.md "wikilink")
|-align=center |||| ||||| || ||(1)||||| |-align=center
|[特剛](特剛.md "wikilink")|| ||[胡爾](胡爾.md "wikilink")||
||[貝倫](貝倫.md "wikilink")||===||[露西安](露西安.md "wikilink")||
|-align=center ||||(2)||||| || ||||| || |-align=center
|[伊綴爾·凱勒布琳朵](伊綴爾·凱勒布琳朵.md "wikilink")||===||[圖爾](圖爾_\(小說人物\).md "wikilink")||
|| ||[迪歐](迪歐.md "wikilink")||===||[寧羅絲](寧羅絲.md "wikilink")
|-align=center | ||||| || || || ||||| |-align=center | ||||| || ||
||---||---||--- |-align=center | ||||| || || ||||||||| |-align=center |
||[埃蘭迪爾](埃蘭迪爾.md "wikilink")||===||===||===||[愛爾溫](愛爾溫.md "wikilink")||[埃盧瑞](埃盧瑞.md "wikilink")||[埃盧林](埃盧林.md "wikilink")
|-align=center | || || ||||| || || || |-align=center | ||
||---||---||---|| || || |-align=center | || ||||| ||||| || ||
|-align=center | || ||[愛洛斯](愛洛斯.md "wikilink")||
||[愛隆](愛隆.md "wikilink")||===||[凱勒布理安](凱勒布理安.md "wikilink")||
|-align=center | || ||||| || ||||| || |-align=center | ||
||[努曼諾爾帝國](努曼諾爾帝國.md "wikilink")[皇帝](努曼諾爾帝國皇帝.md "wikilink")||
|| ||||| || |-align=center | || ||:|| || ||||| || |-align=center | ||
||[安督奈伊领主](安督奈伊领主.md "wikilink")|| || ||||| || |-align=center | || ||:||
|| ||||| || |-align=center | || ||[伊蘭迪爾](伊蘭迪爾.md "wikilink")|| || |||||
|| |-align=center | || ||:|| || ||||| || |-align=center | ||
||[剛鐸](剛鐸.md "wikilink")[國王及](剛鐸國王.md "wikilink")[亞爾諾](亞爾諾.md "wikilink")[國王](亞爾諾國王.md "wikilink")||
|| ||||| || |-align=center | || ||:|| || ||||| || |-align=center | ||
||[登丹人酋長](登丹人酋長.md "wikilink")|| || ||||| || |-align=center | || ||:||
||---||---||---|| |-align=center | || ||:||(3)||||||||||| |-align=center
| ||
||[亞拉岡](亞拉岡_\(魔戒\).md "wikilink")||===||[亞玟](亞玟.md "wikilink")||[伊萊丹](伊萊丹.md "wikilink")||[伊羅何](伊羅何.md "wikilink")||
|-align=center | || || ||||| || || || |-align=center | || ||
||[艾達瑞安](艾達瑞安.md "wikilink")|| || || || |}

(1)(2)(3)人類和精靈結合，托爾金作品中還暗示有第四對人類和精靈的結合，不過不在此家譜中。

### 原模版A

`            `[`芬國昐`](芬國昐.md "wikilink")`           `[`高多`](高多.md "wikilink")`  `[`埃盧·庭葛`](埃盧·庭葛.md "wikilink")` = `[`美麗安`](美麗安.md "wikilink")
`                |             |            |`
`              |             |        (1) |`
`             `[`特剛`](特剛.md "wikilink")`           `[`胡爾`](胡爾.md "wikilink")`     `[`貝倫`](貝倫.md "wikilink")` = `[`露西安`](露西安.md "wikilink")
`                |     (2)     |         |`
` `[`伊綴爾·凱勒布琳朵`](伊綴爾·凱勒布琳朵.md "wikilink")` ======= `[`圖爾`](圖爾_\(小說人物\).md "wikilink")`       `[`迪歐`](迪歐.md "wikilink")` = `[`寧羅絲`](寧羅絲.md "wikilink")
`                       |                   |`
`                    |                -----------------`
`                    |                |       |       |`
`                  `[`埃蘭迪爾`](埃蘭迪爾.md "wikilink")` ======== `[`愛爾溫`](愛爾溫.md "wikilink")`  `[`埃盧瑞`](埃盧瑞.md "wikilink")`  `[`埃盧林`](埃盧林.md "wikilink")
`                                  |`
`                       -------------------`
`                       |                 |`
`                    `[`愛洛斯`](愛洛斯.md "wikilink")`                `[`愛隆`](愛隆.md "wikilink")` = `[`凱勒布理安`](凱勒布理安.md "wikilink")
`                          |                    |`
`                `[`努曼諾爾帝國皇帝`](努曼諾爾帝國.md "wikilink")`                |`
`                       :                     |`
`                    `[`伊蘭迪爾`](伊蘭迪爾.md "wikilink")`                    |`
`                       :                     |`
`               `[`剛鐸及`](剛鐸.md "wikilink")[`亞爾諾的國王`](亞爾諾.md "wikilink")`                |    `
`                       :              ---------------`
`                       :      (3)     |     |       |`
`                     `[`亞拉岡`](亞拉岡.md "wikilink")` ========= `[`亞玟`](亞玟.md "wikilink")`  `[`伊萊丹`](伊萊丹.md "wikilink")`  `[`伊羅何`](伊羅何.md "wikilink")` `
`                                     |`
`                             `[`艾達瑞安`](艾達瑞安.md "wikilink")

</noinclude>

[Category:虚构角色模板](../Category/虚构角色模板.md "wikilink")