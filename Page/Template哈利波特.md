衛斯理\]\] {{\!}} [妙麗{{·}}格蘭傑](妙麗·格蘭傑.md "wikilink") {{\!}}
[阿不思{{·}}鄧不利多](阿不思·鄧不利多.md "wikilink") {{\!}}
[伏地魔](伏地魔.md "wikilink") {{\!}}
[賽佛勒斯{{·}}石內卜](賽佛勒斯·石內卜.md "wikilink")
{{\!}} [-{zh-hans:鲁伯{{·}}海格;zh-hant:魯霸{{·}}海格}-](魯霸·海格.md "wikilink")
{{\!}} [天狼星{{·}}布萊克](天狼星·布萊克.md "wikilink") {{\!}}
[雷木思{{·}}路平](雷木思·路平.md "wikilink") {{\!}}
[跩哥{{·}}馬份](跩哥·馬份.md "wikilink") | group2 = 配角 | list2 =
[弗雷和喬治·衛斯理](弗雷和喬治·衛斯理.md "wikilink") {{\!}}
[金妮{{·}}衛斯理](金妮·衛斯理.md "wikilink") {{\!}}
[亞瑟·衛斯理](亞瑟·衛斯理.md "wikilink") {{\!}}
[茉莉·衛斯理](茉莉·衛斯理.md "wikilink") {{\!}}
[比爾·衛斯理](比爾·衛斯理.md "wikilink") {{\!}}
[花兒·戴樂古](花兒·戴樂古.md "wikilink") {{\!}}
[查理·衛斯理](查理·衛斯理_\(哈利波特\).md "wikilink") {{\!}}
[派西·衛斯理](派西·伊格內修斯·衛斯理.md "wikilink") {{\!}}
[-{zh-cn:纳威{{·}}隆巴顿;zh-tw:奈威{{·}}隆巴頓}-](纳威·隆巴顿.md "wikilink")
{{\!}} [露娜{{·}}羅古德](露娜·羅古德.md "wikilink") {{\!}} [張秋](張秋.md "wikilink")
{{\!}} [西追·迪哥里](西追·迪哥里.md "wikilink") {{\!}} [麥米奈娃](米奈娃·麥.md "wikilink")
{{\!}} [菲力·孚立維](菲力·孚立維.md "wikilink") {{\!}} [瘋眼穆敵](瘋眼穆敵.md "wikilink")
{{\!}} [小仙女·東施](小仙女·東施.md "wikilink") {{\!}}
[西碧·崔老妮](西碧·崔老妮.md "wikilink") {{\!}}
[赫瑞司·史拉轟](斯拉格霍恩.md "wikilink") {{\!}}
[桃樂絲·恩不里居](桃樂絲·恩不里居.md "wikilink") {{\!}}
[康尼留斯·夫子](康尼留斯·夫子.md "wikilink") {{\!}}
[盧夫·昆爵](盧夫·昆爵.md "wikilink") {{\!}}
[多比](多比.md "wikilink") {{\!}} [彼得·佩迪魯](彼得·佩迪魯.md "wikilink")
{{\!}} [盧休思·馬份](卢修斯·马尔福.md "wikilink") {{\!}}
[水仙·馬份](水仙·馬份.md "wikilink") {{\!}}
[貝拉·雷斯壯](貝拉·雷斯壯.md "wikilink") {{\!}}
[巴提·柯羅奇](巴提·柯羅奇.md "wikilink") {{\!}}
[小巴堤·柯羅奇](小巴堤·柯羅奇.md "wikilink") {{\!}}
[迪安·托马斯](迪安·托马斯.md "wikilink") {{\!}}
[柯林·克利維](柯林·克利維.md "wikilink") {{\!}}
[詹姆·波特](詹姆·波特.md "wikilink") {{\!}}
[莉莉·波特](莉莉·波特.md "wikilink") {{\!}}
[德思禮一家](德思禮一家.md "wikilink") {{\!}}
[麗塔·史譏](麗塔·史譏.md "wikilink") {{\!}}
[阿各·飛七](阿各·飛七.md "wikilink") {{\!}}
[吉德羅·洛哈](吉德羅·洛哈.md "wikilink") {{\!}}
[愛哭鬼麥朵](桃金娘_\(哈利波特\).md "wikilink") {{\!}}
[蓋瑞·葛林戴華德](蓋瑞·葛林戴華德.md "wikilink") </font> | group3
= 生物 | list3 = [阿辣哥](阿辣哥.md "wikilink") {{\!}} [佛客使](佛客使.md "wikilink")
{{\!}} [嘿美](嘿美.md "wikilink") {{\!}} [歪腿](歪腿.md "wikilink") {{\!}}
[巴嘴](巴嘴.md "wikilink") {{\!}} [拿樂絲太太](拿樂絲太太.md "wikilink") |
group4 = 組織和家庭 | list4 =[霍格華茲的教職員](霍格華茲的教職員.md "wikilink") {{\!}}
[鳳凰會](鳳凰會.md "wikilink") {{\!}} [食死徒](食死徒.md "wikilink") {{\!}}
[鄧不利多的軍隊](鄧不利多的軍隊.md "wikilink") {{\!}} [衛斯理家族](衛斯理家族.md "wikilink") }}

|group2 = [魔法世界](哈利波特的虛構世界.md "wikilink") |list2 = |group3 = 相關作品 |list3
=

  - 《[怪獸與牠們的產地](怪獸與牠們的產地.md "wikilink")》
      - [紐特·斯卡曼德](紐特·斯卡曼德.md "wikilink")
  - 《[穿越歷史的魁地奇](穿越歷史的魁地奇.md "wikilink")》
  - 《[吟遊詩人皮陀故事集](吟遊詩人皮陀故事集.md "wikilink")》
  - 《[前傳](哈利波特前傳.md "wikilink")》
  - [Pottermore](Pottermore.md "wikilink")
  - 《[被诅咒的孩子](哈利波特：被诅咒的孩子.md "wikilink")》
  - 《[霍格華茲不完全不靠譜指南](霍格華茲不完全不靠譜指南.md "wikilink")》
  - 《[霍格華茲的權力、政治與搗蛋鬼的短篇故事](霍格華茲的權力、政治與搗蛋鬼的短篇故事.md "wikilink")》
  - 《》

|group4 = [電影](哈利波特系列電影.md "wikilink") |list4 = |group5 =  |list5 =

  - 《[哈利波特：魁地奇世界盃](哈利波特：魁地奇世界盃.md "wikilink")》
  - 《》
  - 《》
  - 《》
  - 《[哈利波特：巫師聯盟](哈利波特：巫師聯盟.md "wikilink")》

|group6 = 景點 |list6 =

  - [哈利波特的魔法世界](哈利波特的魔法世界.md "wikilink")

      - [奧蘭多](哈利波特的魔法世界_\(奧蘭多環球影城\).md "wikilink")

      -
      -
  -   -
      -
      -
      -
      -
|group7 = 展覽 |list7 =

  - [哈利波特魔法世界](哈利波特魔法世界_\(展覽\).md "wikilink")

  - [哈利波特：魔法史](哈利波特：魔法史.md "wikilink")

  -
|group8 = 其他相關 |list8 =

  - [创作来源及相似作品](哈利·波特的创作灵感来源及相似作品.md "wikilink")

  - [宗教爭論](哈利波特系列的宗教爭論.md "wikilink")

  - [The Harry Potter Lexicon](The_Harry_Potter_Lexicon.md "wikilink")

  - [中文翻譯](Wikipedia:哈利·波特中文翻譯詞彙對照表.md "wikilink")

  -
  - 《[佛地魔：傳人的起源](佛地魔：傳人的起源.md "wikilink")》

}}

| below =

  - **[圖書](:en:Book:Harry_Potter.md "wikilink")**

  - **[分類](:../Category/哈利波特.md "wikilink")**

  - **[共享資源](commons:../Category/Harry_Potter.md "wikilink")**

  - **[主題](Portal:哈利波特.md "wikilink")**

}}<noinclude>

</noinclude>

[Category:系列小說導航模板](../Category/系列小說導航模板.md "wikilink")
[\*](../Category/哈利波特.md "wikilink")