<includeonly>{{\#invoke:sidebar|collapsible | basestyle =
background:lavender; | title = [基督教](基督教.md "wikilink")
<font size="-1">[**系列條目**](:../Category/基督教.md "wikilink")</font> |
pretitle = | width = 180px | image =
![Golden_Christian_Cross.svg](Golden_Christian_Cross.svg
"Golden_Christian_Cross.svg")</span> | headingstyle = border-bottom:5px
solid \#efefef; | listtitlestyle =
background:lavender;padding:0.2em;text-align:center; | listclass =
plainlist | expanded = }}}}}}}}}

| list1name = | list1title =  | list1 =

  - [基督教中的耶稣](基督教中的耶稣.md "wikilink")
  - [降生](耶稣诞生.md "wikilink")
  - [事工](耶稣的事工.md "wikilink")
  - [受难](耶穌受難.md "wikilink")
  - [复活](耶稣复活.md "wikilink")

| list2name = | list2title =  | list2 =

  - [旧约](旧约圣经.md "wikilink")

  - [新约](新約聖經.md "wikilink")

  - [福音](福音_\(耶稣\).md "wikilink")

  - [正典](正典.md "wikilink")

  -
  - [教会](教会.md "wikilink")

  - [信經](信經.md "wikilink")

  -
| list3name = | list3title = [神学](基督教神學.md "wikilink") | list3 =

  - [神](基督教的神.md "wikilink")
  - [三位一體](三位一體.md "wikilink")
      - [聖父](聖父.md "wikilink")
      - [聖子](神之子.md "wikilink")
      - [聖靈](聖靈.md "wikilink")

<!-- end list -->

  - [辨惑学](基督教辨惑学.md "wikilink")

  - [洗礼](洗禮.md "wikilink")

  - [基督论](基督论.md "wikilink")

  -
  - [传教](基督教传教活动.md "wikilink")

  - [救赎](基督教救贖論.md "wikilink")

| list4name = | list4title =  | list4 =

  - [马利亚](馬利亞_\(耶穌的母親\).md "wikilink")
  - [使徒](使徒_\(基督教\).md "wikilink")
  - [彼得](彼得_\(使徒\).md "wikilink")
  - [保罗](保罗_\(使徒\).md "wikilink")
  - [教父](教父_\(基督教歷史\).md "wikilink")
  - [早期基督教](早期基督教.md "wikilink")
  - [君士坦丁](君士坦丁大帝.md "wikilink")
  - [大公會議](大公會議.md "wikilink")
  - [奥古斯丁](希波的奥古斯丁.md "wikilink")
  - [东西教会大分裂](東西教會大分裂.md "wikilink")
  - [十字军东征](十字军东征.md "wikilink")
  - [阿奎那](托马斯·阿奎那.md "wikilink")
  - [亞維儂分裂](天主教會大分裂.md "wikilink")
  - [威克里夫](約翰·威克里夫.md "wikilink")
  - [揚胡斯](扬·胡斯.md "wikilink")
  - [宗教改革](宗教改革.md "wikilink")
  - [路德](马丁·路德.md "wikilink")
  - [喀爾文](讓·喀爾文.md "wikilink")
  - [慈運理](烏利希·慈運理.md "wikilink")
  - [亨利八世](亨利八世.md "wikilink")

| list5name = | list5title = 相关专题 | list5 =

  - [艺术](基督教藝術.md "wikilink")

  - [节日](教会年历.md "wikilink")

  - [批评](对基督教的批评.md "wikilink")

  - [合一运动](普世教会合一运动.md "wikilink")

  - [礼拜仪式](基督教礼拜仪式.md "wikilink")

  - [音乐](基督教音乐.md "wikilink")

  -
  -
  - [布道](布道.md "wikilink")

  -
| list6name = groupings | list6title =  | list6 =

| below = {{\#ifexpr:({{\#ifeq:|ichthys |1 |0}} or )

`          | `![`Ichthus.svg`](Ichthus.svg "Ichthus.svg")
`          | `![`P_christianity.svg`](P_christianity.svg
"P_christianity.svg")
`         }} `[`Portal:基督教`](Portal:基督教.md "wikilink")

}}</includeonly><noinclude>  </noinclude>