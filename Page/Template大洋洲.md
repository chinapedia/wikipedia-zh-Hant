<noinclude>
</noinclude>[非洲](../Page/非洲.md "wikilink"){{.w}}[美洲](../Page/美洲.md "wikilink"){{.w}}[南极洲](../Page/南极洲.md "wikilink"){{.w}}[亞洲](../Page/亞洲.md "wikilink"){{.w}}[歐洲](../Page/歐洲.md "wikilink"){{.w}}[各国首都列表](../Page/各国首都列表.md "wikilink")

` |group1    = `[`澳大拉西亞`](../Page/澳大拉西亞.md "wikilink")
` | list1    = `[`澳大利亚`](../Page/澳大利亚.md "wikilink")`{{.w}}`[`科科斯（基林）群島`](../Page/科科斯（基林）群島.md "wikilink")<small>`（澳）`</small>`{{.w}}`[`聖誕島`](../Page/圣诞岛.md "wikilink")<small>`（澳）`</small>`{{.w}}`[`-{zh-hans:阿什莫尔和卡捷岛;``
 ``zh-hant:亞什摩及卡地爾群島}-`](../Page/阿什莫尔和卡捷群岛.md "wikilink")<small>`（澳）`</small>`{{.w}}`[`珊瑚海群島`](../Page/珊瑚海群岛.md "wikilink")<small>`（澳）`</small>
` |group2    = `[`密克羅尼西亞`](../Page/密克罗尼西亚群岛.md "wikilink")
` | list2    = `[`帛琉`](../Page/帛琉.md "wikilink")`{{.w}}`[`密克罗尼西亚联邦`](../Page/密克罗尼西亚联邦.md "wikilink")`{{.w}}`[`馬紹爾群島`](../Page/馬紹爾群島.md "wikilink")`{{.w}}`[`基里巴斯`](../Page/基里巴斯.md "wikilink")`{{.w}}`[`諾魯`](../Page/諾魯.md "wikilink")`{{.w}}`[`威克島`](../Page/威克島.md "wikilink")<small>`（美）`</small>`{{.w}}`[`北马里亚纳群岛`](../Page/北马里亚纳群岛.md "wikilink")<small>`（美）`</small>`{{.w}}`[`關島`](../Page/關島.md "wikilink")<small>`（美）`</small>
` |group3    = `[`美拉尼西亚`](../Page/美拉尼西亚.md "wikilink")
` | list3    = `[`巴布亚新几内亚`](../Page/巴布亚新几内亚.md "wikilink")`{{.w}}`[`所罗门群岛`](../Page/所罗门群岛.md "wikilink")`{{.w}}`[`瓦努阿图`](../Page/瓦努阿图.md "wikilink")`{{.w}}`[`斐濟`](../Page/斐濟.md "wikilink")`{{.w}}`[`新喀里多尼亞`](../Page/新喀里多尼亞.md "wikilink")<small>`（法）`</small>
` |group4    = `[`玻里尼西亞`](../Page/玻里尼西亞.md "wikilink")
` | list4    = `[`圖瓦盧`](../Page/圖瓦盧.md "wikilink")`{{.w}}`[`萨摩亚`](../Page/萨摩亚.md "wikilink")`{{.w}}`[`-{zh-hans:汤加;``
 ``zh-hant:湯加;``
 ``zh-tw:東加}-`](../Page/東加.md "wikilink")`{{.w}}`[`紐埃`](../Page/紐埃.md "wikilink")`{{.w}}`[`库克群岛`](../Page/库克群岛.md "wikilink")`{{.w}}`[`新西兰`](../Page/新西兰.md "wikilink")` ``{{.w}}`[`夏威夷`](../Page/夏威夷州.md "wikilink")<small>`（美）`</small>`{{.w}}`[`中途島`](../Page/中途島.md "wikilink")<small>`（美）`</small>`{{.w}}`[`约翰斯顿环礁`](../Page/约翰斯顿环礁.md "wikilink")<small>`（美）`</small>`{{.w}}`[`鳳凰群島北部的`](../Page/菲尼克斯群岛.md "wikilink")[`豪蘭島和`](../Page/豪蘭島.md "wikilink")[`貝克島`](../Page/貝克島.md "wikilink")<small>`（美）`</small>`{{.w}}`[`莱恩群岛北部的`](../Page/莱恩群岛.md "wikilink")[`金曼礁`](../Page/金曼礁.md "wikilink")`、`[`帕邁拉環礁和中部的`](../Page/帕邁拉環礁.md "wikilink")[`賈維斯島`](../Page/賈維斯島.md "wikilink")<small>`（美）`</small>`{{.w}}`[`托克劳群岛`](../Page/托克劳.md "wikilink")<small>`（新西兰）`</small>`{{.w}}`[`瓦利斯和富圖納`](../Page/瓦利斯和富圖納.md "wikilink")<small>`（法）`</small>`{{.w}}`[`美屬薩摩亞`](../Page/美屬薩摩亞.md "wikilink")<small>`（美）`</small>`{{.w}}`[`法屬玻里尼西亞`](../Page/法屬玻里尼西亞.md "wikilink")<small>`（法）`</small>`{{.w}}`[`皮特凯恩群岛`](../Page/皮特凯恩群岛.md "wikilink")<small>`（英）`</small>`{{.w}}`[`復活節島`](../Page/復活節島.md "wikilink")<small>`（智利）`</small>
` |below     = `<small>

[印度尼西亚的部份地區和](../Page/印度尼西亚.md "wikilink")[东帝汶有時被視為大洋洲](../Page/东帝汶.md "wikilink")。
[新西兰與](../Page/新西兰.md "wikilink")[巴布亚新几内亚有時被認為屬於澳大拉西亞地區](../Page/巴布亚新几内亚.md "wikilink")。
[新西兰與](../Page/新西兰.md "wikilink")[紐埃](../Page/紐埃.md "wikilink")、[库克群岛組成](../Page/库克群岛.md "wikilink")[联系邦](../Page/联系邦.md "wikilink")。
[美國與](../Page/美國.md "wikilink")[帛琉](../Page/帛琉.md "wikilink")、[密克罗尼西亚联邦](../Page/密克罗尼西亚联邦.md "wikilink")、[馬紹爾群島組成](../Page/馬紹爾群島.md "wikilink")[联系邦](../Page/联系邦.md "wikilink")。
</small> }}<noinclude> </noinclude>

[-](../Category/大洋洲国家模板.md "wikilink")
[-](../Category/大洋洲地理.md "wikilink")