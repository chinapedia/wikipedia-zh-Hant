} |

`   | {{#ifeq: ``  }}} | `
`                | {{#ifeq: ``|`
`                              |  `![`PD-icon.svg`](PD-icon.svg
"PD-icon.svg")` `
`                              |
 `![`Wikisource-logo.svg`](Wikisource-logo.svg
"Wikisource-logo.svg")` `
`                           }}`
`                 |  `![`Wikisource-logo.svg`](Wikisource-logo.svg
"Wikisource-logo.svg")` `
`               }}`
`  }}`

}} {{\#ifeq:  }}} |

`   | {{#ifeq: ``  }}} | `
`                                    | 本條目`
`                                    | 本條目部分或全部内容`
`                                   }}
出自`[`公有领域`](公有领域.md "wikilink")`：`
`   }}  ``=`
`   |{{#if:`` |first1 |HIDE_PARAMETER2}}=`
`   |{{#if:`` |first2 |HIDE_PARAMETER3}}=`
`   |{{#if:`` |first3 |HIDE_PARAMETER4}}=`

`   |{{#if:``   |last   |HIDE_PARAMETER5}}=`
`   |{{#if:``  |last1  |HIDE_PARAMETER6}}=`
`   |{{#if:``  |last2  |HIDE_PARAMETER7}}=`
`   |{{#if:``  |last3  |HIDE_PARAMETER8}}=`

`   |{{#if:`` |author  |HIDE_PARAMETER9}}=`
`   |{{#if:`` |author1 |HIDE_PARAMETERa}}=`
`   |{{#if:`` |author2 |HIDE_PARAMETERb}}=`
`   |{{#if:`` |author3 |HIDE_PARAMETERc}}=`
`   |{{#if:`` |authors |HIDE_PARAMETERd}}=`
`   |{{#if:`` |authorlink |HIDE_PARAMETERe1}}=`
`   |{{#if:`` |author2link |HIDE_PARAMETERe2}}=`
`   |{{#if:`` |author3link |HIDE_PARAMETERe3}}=`
`   |{{#if:``  |coauthors  |HIDE_PARAMETERf}}=`

`   |{{#if:``  |accessdate  |HIDE_PARAMETERg}}=`
`   |{{#if:`` |display |HIDE_PARAMETERh}}=`
`   | wstitle =  `` }}} }}}`
`   | title = `` }}}`
`   |{{#if:`` |edition |HIDE_PARAMETERi}}=`
`   |{{#if:``    |year    |HIDE_PARAMETERj}}=`
`   |{{#if:``     |url     |HIDE_PARAMETERk}}=`
`   |{{#if:``  |volume  |HIDE_PARAMETERl}}=`
`   |{{#if:``    |page    |HIDE_PARAMETERm}}=`
`   |{{#if:``   |pages   |HIDE_PARAMETERn}}=`
`   |{{#if:`` |postscript  |HIDE_PARAMETERp}}=`
`   |{{#if:`` |separator  |HIDE_PARAMETERq}}=`
`   |{{#if:`` |mode  |HIDE_PARAMETERr}}=`
`   | ref = `
`  }} {{#ifeq: `` |`
`   | {{#if:``|}} {{#if:``|}} {{#if:``|}}`
`    {{#if:``|}}`
`    {{#if:``|}}`
`   |`
`   }}  `<noinclude>

</noinclude> <includeonly>

[Category:含有1911年版大英百科全书内容的维基百科条目](../Category/含有1911年版大英百科全书内容的维基百科条目.md "wikilink")
[Category:Wikipedia articles incorporating text from the 1911
Encyclopædia Britannica with an unnamed
parameter](../Category/Wikipedia_articles_incorporating_text_from_the_1911_Encyclopædia_Britannica_with_an_unnamed_parameter.md "wikilink")
[Category:Wikipedia articles incorporating text from the 1911
Encyclopædia Britannica with an article
parameter](../Category/Wikipedia_articles_incorporating_text_from_the_1911_Encyclopædia_Britannica_with_an_article_parameter.md "wikilink")
[Category:Wikipedia articles incorporating text from the 1911
Encyclopædia Britannica with a wikisource
parameter](../Category/Wikipedia_articles_incorporating_text_from_the_1911_Encyclopædia_Britannica_with_a_wikisource_parameter.md "wikilink")
[Category:Wikipedia articles incorporating a citation from the 1911
Encyclopaedia Britannica that includes an obsolete
parameter](../Category/Wikipedia_articles_incorporating_a_citation_from_the_1911_Encyclopaedia_Britannica_that_includes_an_obsolete_parameter.md "wikilink")
[Category:Wikipedia articles incorporating a citation from the 1911
Encyclopaedia Britannica that includes an obsolete
parameter](../Category/Wikipedia_articles_incorporating_a_citation_from_the_1911_Encyclopaedia_Britannica_that_includes_an_obsolete_parameter.md "wikilink")
[](../Category/引據模板.md "wikilink")