[庐阳区](../Page/庐阳区.md "wikilink"){{.w}}[包河区](../Page/包河区.md "wikilink"){{.w}}[巢湖市](../Page/巢湖市.md "wikilink"){{.w}}[长丰县](../Page/长丰县.md "wikilink"){{.w}}[肥东县](../Page/肥东县.md "wikilink"){{.w}}[肥西县](../Page/肥西县.md "wikilink"){{.w}}[庐江县](../Page/庐江县.md "wikilink")

|group3 = [芜湖市](../Page/芜湖市.md "wikilink") |list3 =
[鸠江区](../Page/鸠江区.md "wikilink"){{.w}}[镜湖区](../Page/镜湖区.md "wikilink"){{.w}}[弋江区](../Page/弋江区.md "wikilink"){{.w}}[三山区](../Page/三山区.md "wikilink"){{.w}}[芜湖县](../Page/芜湖县.md "wikilink"){{.w}}[繁昌县](../Page/繁昌县.md "wikilink"){{.w}}[南陵县](../Page/南陵县.md "wikilink"){{.w}}[无为县](../Page/无为县.md "wikilink")

|group4 = [蚌埠市](../Page/蚌埠市.md "wikilink") |list4 =
[蚌山区](../Page/蚌山区.md "wikilink"){{.w}}[龙子湖区](../Page/龙子湖区.md "wikilink"){{.w}}[禹会区](../Page/禹会区.md "wikilink"){{.w}}[淮上区](../Page/淮上区.md "wikilink"){{.w}}[怀远县](../Page/怀远县.md "wikilink"){{.w}}[五河县](../Page/五河县.md "wikilink"){{.w}}[固镇县](../Page/固镇县.md "wikilink")

|group5 = [淮南市](../Page/淮南市.md "wikilink") |list5 =
[田家庵区](../Page/田家庵区.md "wikilink"){{.w}}[大通区](../Page/大通区.md "wikilink"){{.w}}[谢家集区](../Page/谢家集区.md "wikilink"){{.w}}[八公山区](../Page/八公山区.md "wikilink"){{.w}}[潘集区](../Page/潘集区.md "wikilink"){{.w}}[寿县](../Page/寿县.md "wikilink"){{.w}}[凤台县](../Page/凤台县.md "wikilink")

|group6 = [马鞍山市](../Page/马鞍山市.md "wikilink") |list6 =
[雨山区](../Page/雨山区.md "wikilink"){{.w}}[花山区](../Page/花山区.md "wikilink"){{.w}}[博望区](../Page/博望区.md "wikilink"){{.w}}[当涂县](../Page/当涂县.md "wikilink"){{.w}}[含山县](../Page/含山县.md "wikilink"){{.w}}[和县](../Page/和县.md "wikilink")

|group7 = [淮北市](../Page/淮北市.md "wikilink") |list7 =
[相山区](../Page/相山区.md "wikilink"){{.w}}[杜集区](../Page/杜集区.md "wikilink"){{.w}}[烈山区](../Page/烈山区.md "wikilink"){{.w}}[濉溪县](../Page/濉溪县.md "wikilink")

|group8 = [铜陵市](../Page/铜陵市.md "wikilink") |list8 =
[铜官区](../Page/铜官区.md "wikilink"){{.w}}[义安区](../Page/义安区.md "wikilink"){{.w}}[郊区](../Page/郊区_\(铜陵市\).md "wikilink"){{.w}}[枞阳县](../Page/枞阳县.md "wikilink")

|group9 = [安庆市](../Page/安庆市.md "wikilink") |list9 =
[大观区](../Page/大观区.md "wikilink"){{.w}}[迎江区](../Page/迎江区.md "wikilink"){{.w}}[宜秀区](../Page/宜秀区.md "wikilink"){{.w}}[桐城市](../Page/桐城市.md "wikilink"){{.w}}[潜山市](../Page/潜山市.md "wikilink"){{.w}}[怀宁县](../Page/怀宁县.md "wikilink"){{.w}}[太湖县](../Page/太湖县.md "wikilink"){{.w}}[宿松县](../Page/宿松县.md "wikilink"){{.w}}[望江县](../Page/望江县.md "wikilink"){{.w}}[岳西县](../Page/岳西县.md "wikilink")

|group10 = [黄山市](../Page/黄山市.md "wikilink") |list10 =
[屯溪区](../Page/屯溪区.md "wikilink"){{.w}}[黄山区](../Page/黄山区.md "wikilink"){{.w}}[徽州区](../Page/徽州区.md "wikilink"){{.w}}[歙县](../Page/歙县.md "wikilink"){{.w}}[休宁县](../Page/休宁县.md "wikilink"){{.w}}[黟县](../Page/黟县.md "wikilink"){{.w}}[祁门县](../Page/祁门县.md "wikilink")

|group11 = [滁州市](../Page/滁州市.md "wikilink") |list11 =
[琅琊区](../Page/琅琊区.md "wikilink"){{.w}}[南谯区](../Page/南谯区.md "wikilink"){{.w}}[天长市](../Page/天长市.md "wikilink"){{.w}}[明光市](../Page/明光市.md "wikilink"){{.w}}[来安县](../Page/来安县.md "wikilink"){{.w}}[全椒县](../Page/全椒县.md "wikilink"){{.w}}[定远县](../Page/定远县.md "wikilink"){{.w}}[凤阳县](../Page/凤阳县.md "wikilink")

|group12 = [阜阳市](../Page/阜阳市.md "wikilink") |list12 =
[颍州区](../Page/颍州区.md "wikilink"){{.w}}[颍东区](../Page/颍东区.md "wikilink"){{.w}}[颍泉区](../Page/颍泉区.md "wikilink"){{.w}}[界首市](../Page/界首市.md "wikilink"){{.w}}[临泉县](../Page/临泉县.md "wikilink"){{.w}}[太和县](../Page/太和县.md "wikilink"){{.w}}[阜南县](../Page/阜南县.md "wikilink"){{.w}}[颍上县](../Page/颍上县.md "wikilink")

|group13 = [宿州市](../Page/宿州市.md "wikilink") |list13 =
[埇桥区](../Page/埇桥区.md "wikilink"){{.w}}[砀山县](../Page/砀山县.md "wikilink"){{.w}}[萧县](../Page/萧县.md "wikilink"){{.w}}[灵璧县](../Page/灵璧县.md "wikilink"){{.w}}[泗县](../Page/泗县.md "wikilink")

|group14 = [六安市](../Page/六安市.md "wikilink") |list14 =
[金安区](../Page/金安区.md "wikilink"){{.w}}[裕安区](../Page/裕安区.md "wikilink"){{.w}}[叶集区](../Page/叶集区.md "wikilink"){{.w}}[霍邱县](../Page/霍邱县.md "wikilink"){{.w}}[舒城县](../Page/舒城县.md "wikilink"){{.w}}[金寨县](../Page/金寨县.md "wikilink"){{.w}}[霍山县](../Page/霍山县.md "wikilink")

|group15 = [亳州市](../Page/亳州市.md "wikilink") |list15 =
[谯城区](../Page/谯城区.md "wikilink"){{.w}}[涡阳县](../Page/涡阳县.md "wikilink"){{.w}}[蒙城县](../Page/蒙城县.md "wikilink"){{.w}}[利辛县](../Page/利辛县.md "wikilink")

|group16 = [池州市](../Page/池州市.md "wikilink") |list16 =
[贵池区](../Page/贵池区.md "wikilink"){{.w}}[东至县](../Page/东至县.md "wikilink"){{.w}}[石台县](../Page/石台县.md "wikilink"){{.w}}[青阳县](../Page/青阳县.md "wikilink")

|group17 = [宣城市](../Page/宣城市.md "wikilink") |list17 =
[宣州区](../Page/宣州区.md "wikilink"){{.w}}[宁国市](../Page/宁国市.md "wikilink"){{.w}}[郎溪县](../Page/郎溪县.md "wikilink"){{.w}}[广德县](../Page/广德县.md "wikilink"){{.w}}[泾县](../Page/泾县.md "wikilink"){{.w}}[绩溪县](../Page/绩溪县.md "wikilink"){{.w}}[旌德县](../Page/旌德县.md "wikilink")
}}

|belowstyle = text-align: left; font-size: 80%; |below =
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[安徽省乡级以上行政区列表](../Page/安徽省乡级以上行政区列表.md "wikilink")。
}}<noinclude> </noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[\*](../Category/安徽行政区划.md "wikilink")
[安徽行政区划模板](../Category/安徽行政区划模板.md "wikilink")