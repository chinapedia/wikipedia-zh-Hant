[槐荫区](../Page/槐荫区.md "wikilink"){{.w}}[天桥区](../Page/天桥区.md "wikilink"){{.w}}[历城区](../Page/历城区.md "wikilink"){{.w}}[长清区](../Page/长清区.md "wikilink"){{.w}}[章丘区](../Page/章丘区.md "wikilink"){{.w}}[济阳区](../Page/济阳区.md "wikilink"){{.w}}[莱芜区](../Page/莱芜区.md "wikilink"){{.w}}[钢城区](../Page/钢城区.md "wikilink"){{.w}}[平阴县](../Page/平阴县.md "wikilink"){{.w}}[商河县](../Page/商河县.md "wikilink")

|group2 = [青岛市](../Page/青岛市.md "wikilink") |list2 =
[市南区](../Page/市南区.md "wikilink"){{.w}}[市北区](../Page/市北区.md "wikilink"){{.w}}[李沧区](../Page/李沧区.md "wikilink"){{.w}}[崂山区](../Page/崂山区.md "wikilink"){{.w}}[城阳区](../Page/城阳区.md "wikilink"){{.w}}[黄岛区](../Page/黄岛区.md "wikilink"){{.w}}[即墨区](../Page/即墨区.md "wikilink"){{.w}}[胶州市](../Page/胶州市.md "wikilink"){{.w}}[平度市](../Page/平度市.md "wikilink"){{.w}}[莱西市](../Page/莱西市.md "wikilink")
}}

|group3 = [地级市](../Page/地级市.md "wikilink") |list3 =
[淄川区](../Page/淄川区.md "wikilink"){{.w}}[博山区](../Page/博山区.md "wikilink"){{.w}}[临淄区](../Page/临淄区.md "wikilink"){{.w}}[周村区](../Page/周村区.md "wikilink"){{.w}}[桓台县](../Page/桓台县.md "wikilink"){{.w}}[高青县](../Page/高青县.md "wikilink"){{.w}}[沂源县](../Page/沂源县.md "wikilink")

|group5 = [枣庄市](../Page/枣庄市.md "wikilink") |list5 =
[薛城区](../Page/薛城区.md "wikilink"){{.w}}[市中区](../Page/市中区_\(枣庄市\).md "wikilink"){{.w}}[峄城区](../Page/峄城区.md "wikilink"){{.w}}[台儿庄区](../Page/台儿庄区.md "wikilink"){{.w}}[山亭区](../Page/山亭区.md "wikilink"){{.w}}[滕州市](../Page/滕州市.md "wikilink")

|group6 = [东营市](../Page/东营市.md "wikilink") |list6 =
[东营区](../Page/东营区.md "wikilink"){{.w}}[河口区](../Page/河口区.md "wikilink"){{.w}}[垦利区](../Page/垦利区.md "wikilink"){{.w}}[利津县](../Page/利津县.md "wikilink"){{.w}}[广饶县](../Page/广饶县.md "wikilink")

|group7 = [烟台市](../Page/烟台市.md "wikilink") |list7 =
[莱山区](../Page/莱山区.md "wikilink"){{.w}}[芝罘区](../Page/芝罘区.md "wikilink"){{.w}}[福山区](../Page/福山区.md "wikilink"){{.w}}[牟平区](../Page/牟平区.md "wikilink"){{.w}}[龙口市](../Page/龙口市.md "wikilink"){{.w}}[莱阳市](../Page/莱阳市.md "wikilink"){{.w}}[莱州市](../Page/莱州市.md "wikilink"){{.w}}[蓬莱市](../Page/蓬莱市.md "wikilink"){{.w}}[招远市](../Page/招远市.md "wikilink"){{.w}}[栖霞市](../Page/栖霞市.md "wikilink"){{.w}}[海阳市](../Page/海阳市.md "wikilink"){{.w}}[长岛县](../Page/长岛县.md "wikilink")

|group8 = [潍坊市](../Page/潍坊市.md "wikilink") |list8 =
[奎文区](../Page/奎文区.md "wikilink"){{.w}}[潍城区](../Page/潍城区.md "wikilink"){{.w}}[寒亭区](../Page/寒亭区.md "wikilink"){{.w}}[坊子区](../Page/坊子区.md "wikilink"){{.w}}[青州市](../Page/青州市.md "wikilink"){{.w}}[诸城市](../Page/诸城市.md "wikilink"){{.w}}[寿光市](../Page/寿光市.md "wikilink"){{.w}}[安丘市](../Page/安丘市.md "wikilink"){{.w}}[高密市](../Page/高密市.md "wikilink"){{.w}}[昌邑市](../Page/昌邑市.md "wikilink"){{.w}}[临朐县](../Page/临朐县.md "wikilink"){{.w}}[昌乐县](../Page/昌乐县.md "wikilink")

|group9 = [济宁市](../Page/济宁市.md "wikilink") |list9 =
[任城区](../Page/任城区.md "wikilink"){{.w}}[兖州区](../Page/兖州区.md "wikilink"){{.w}}[曲阜市](../Page/曲阜市.md "wikilink"){{.w}}[邹城市](../Page/邹城市.md "wikilink"){{.w}}[微山县](../Page/微山县.md "wikilink"){{.w}}[鱼台县](../Page/鱼台县.md "wikilink"){{.w}}[金乡县](../Page/金乡县.md "wikilink"){{.w}}[嘉祥县](../Page/嘉祥县.md "wikilink"){{.w}}[汶上县](../Page/汶上县.md "wikilink"){{.w}}[泗水县](../Page/泗水县.md "wikilink"){{.w}}[梁山县](../Page/梁山县.md "wikilink")

|group10 = [泰安市](../Page/泰安市.md "wikilink") |list10 =
[泰山区](../Page/泰山区_\(泰安市\).md "wikilink"){{.w}}[岱岳区](../Page/岱岳区.md "wikilink"){{.w}}[新泰市](../Page/新泰市.md "wikilink"){{.w}}[肥城市](../Page/肥城市.md "wikilink"){{.w}}[宁阳县](../Page/宁阳县.md "wikilink"){{.w}}[东平县](../Page/东平县.md "wikilink")

|group11 = [威海市](../Page/威海市.md "wikilink") |list11 =
[环翠区](../Page/环翠区.md "wikilink"){{.w}}[文登区](../Page/文登区.md "wikilink"){{.w}}[荣成市](../Page/荣成市.md "wikilink"){{.w}}[乳山市](../Page/乳山市.md "wikilink")

|group12 = [日照市](../Page/日照市.md "wikilink") |list12 =
[东港区](../Page/东港区.md "wikilink"){{.w}}[岚山区](../Page/岚山区.md "wikilink"){{.w}}[五莲县](../Page/五莲县.md "wikilink"){{.w}}[莒县](../Page/莒县.md "wikilink")

|group13 = [临沂市](../Page/临沂市.md "wikilink") |list13 =
[兰山区](../Page/兰山区.md "wikilink"){{.w}}[罗庄区](../Page/罗庄区.md "wikilink"){{.w}}[河东区](../Page/河东区_\(临沂市\).md "wikilink"){{.w}}[沂南县](../Page/沂南县.md "wikilink"){{.w}}[郯城县](../Page/郯城县.md "wikilink"){{.w}}[沂水县](../Page/沂水县.md "wikilink"){{.w}}[兰陵县](../Page/兰陵县.md "wikilink"){{.w}}[费县](../Page/费县.md "wikilink"){{.w}}[平邑县](../Page/平邑县.md "wikilink"){{.w}}[莒南县](../Page/莒南县.md "wikilink"){{.w}}[蒙阴县](../Page/蒙阴县.md "wikilink"){{.w}}[临沭县](../Page/临沭县.md "wikilink")

|group14 = [德州市](../Page/德州市.md "wikilink") |list14 =
[德城区](../Page/德城区.md "wikilink"){{.w}}[陵城区](../Page/陵城区.md "wikilink"){{.w}}[乐陵市](../Page/乐陵市.md "wikilink"){{.w}}[禹城市](../Page/禹城市.md "wikilink"){{.w}}[宁津县](../Page/宁津县.md "wikilink"){{.w}}[庆云县](../Page/庆云县.md "wikilink"){{.w}}[临邑县](../Page/临邑县.md "wikilink"){{.w}}[齐河县](../Page/齐河县.md "wikilink"){{.w}}[平原县](../Page/平原县.md "wikilink"){{.w}}[夏津县](../Page/夏津县.md "wikilink"){{.w}}[武城县](../Page/武城县.md "wikilink")

|group15 = [聊城市](../Page/聊城市.md "wikilink") |list15 =
[东昌府区](../Page/东昌府区.md "wikilink"){{.w}}[临清市](../Page/临清市.md "wikilink"){{.w}}[阳谷县](../Page/阳谷县.md "wikilink"){{.w}}[莘县](../Page/莘县.md "wikilink"){{.w}}[茌平县](../Page/茌平县.md "wikilink"){{.w}}[东阿县](../Page/东阿县.md "wikilink"){{.w}}[冠县](../Page/冠县.md "wikilink"){{.w}}[高唐县](../Page/高唐县.md "wikilink")

|group16 = [滨州市](../Page/滨州市.md "wikilink") |list16 =
[滨城区](../Page/滨城区.md "wikilink"){{.w}}[沾化区](../Page/沾化区.md "wikilink"){{.w}}[邹平市](../Page/邹平市.md "wikilink"){{.w}}[惠民县](../Page/惠民县.md "wikilink"){{.w}}[阳信县](../Page/阳信县.md "wikilink"){{.w}}[无棣县](../Page/无棣县.md "wikilink"){{.w}}[博兴县](../Page/博兴县.md "wikilink")

|group17 = [菏泽市](../Page/菏泽市.md "wikilink") |list17 =
[牡丹区](../Page/牡丹区.md "wikilink"){{.w}}[定陶区](../Page/定陶区.md "wikilink"){{.w}}[曹县](../Page/曹县.md "wikilink"){{.w}}[单县](../Page/单县.md "wikilink"){{.w}}[成武县](../Page/成武县.md "wikilink"){{.w}}[巨野县](../Page/巨野县.md "wikilink"){{.w}}[郓城县](../Page/郓城县.md "wikilink"){{.w}}[鄄城县](../Page/鄄城县.md "wikilink"){{.w}}[东明县](../Page/东明县.md "wikilink")
}}

|belowstyle = text-align: left; font-size: 80%; |below=
注：[济南市](../Page/济南市.md "wikilink")、[青岛市为](../Page/青岛市.md "wikilink")[副省级市](../Page/副省级市.md "wikilink")。
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[山东省乡级以上行政区列表](../Page/山东省乡级以上行政区列表.md "wikilink")。
}}<noinclude>

</noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[\*](../Category/山东行政区划.md "wikilink")
[山东行政区划模板](../Category/山东行政区划模板.md "wikilink")