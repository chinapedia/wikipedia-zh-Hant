[迎泽区](../Page/迎泽区.md "wikilink"){{.w}}[尖草坪区](../Page/尖草坪区.md "wikilink"){{.w}}[万柏林区](../Page/万柏林区.md "wikilink"){{.w}}[晋源区](../Page/晋源区.md "wikilink"){{.w}}[古交市](../Page/古交市.md "wikilink"){{.w}}[清徐县](../Page/清徐县.md "wikilink"){{.w}}[阳曲县](../Page/阳曲县.md "wikilink"){{.w}}[娄烦县](../Page/娄烦县.md "wikilink")

|group3 = [大同市](../Page/大同市.md "wikilink") |list3 =
[平城区](../Page/平城区.md "wikilink"){{.w}}[云冈区](../Page/云冈区.md "wikilink"){{.w}}[新荣区](../Page/新荣区.md "wikilink"){{.w}}[云州区](../Page/云州区.md "wikilink"){{.w}}[阳高县](../Page/阳高县.md "wikilink"){{.w}}[天镇县](../Page/天镇县.md "wikilink"){{.w}}[广灵县](../Page/广灵县.md "wikilink"){{.w}}[灵丘县](../Page/灵丘县.md "wikilink"){{.w}}[左云县](../Page/左云县.md "wikilink"){{.w}}[浑源县](../Page/浑源县.md "wikilink")

|group4 = [阳泉市](../Page/阳泉市.md "wikilink") |list4 =
[城区](../Page/城区_\(阳泉市\).md "wikilink"){{.w}}[矿区](../Page/矿区_\(阳泉市\).md "wikilink"){{.w}}[郊区](../Page/郊区_\(阳泉市\).md "wikilink"){{.w}}[平定县](../Page/平定县.md "wikilink"){{.w}}[盂县](../Page/盂县.md "wikilink")

|group5 = [长治市](../Page/长治市.md "wikilink") |list5 =
[潞州区](../Page/潞州区.md "wikilink"){{.w}}[上党区](../Page/上党区.md "wikilink"){{.w}}[屯留区](../Page/屯留区.md "wikilink"){{.w}}[潞城区](../Page/潞城区.md "wikilink"){{.w}}[襄垣县](../Page/襄垣县.md "wikilink"){{.w}}[平顺县](../Page/平顺县.md "wikilink"){{.w}}[黎城县](../Page/黎城县.md "wikilink"){{.w}}[壶关县](../Page/壶关县.md "wikilink"){{.w}}[长子县](../Page/长子县.md "wikilink"){{.w}}[武乡县](../Page/武乡县.md "wikilink"){{.w}}[沁县](../Page/沁县.md "wikilink"){{.w}}[沁源县](../Page/沁源县.md "wikilink")

|group6 = [晋城市](../Page/晋城市.md "wikilink") |list6 =
[城区](../Page/城区_\(晋城市\).md "wikilink"){{.w}}[高平市](../Page/高平市.md "wikilink"){{.w}}[沁水县](../Page/沁水县.md "wikilink"){{.w}}[阳城县](../Page/阳城县.md "wikilink"){{.w}}[陵川县](../Page/陵川县.md "wikilink"){{.w}}[泽州县](../Page/泽州县.md "wikilink")

|group7 = [朔州市](../Page/朔州市.md "wikilink") |list7 =
[朔城区](../Page/朔城区.md "wikilink"){{.w}}[平鲁区](../Page/平鲁区.md "wikilink"){{.w}}[怀仁市](../Page/怀仁市.md "wikilink")
{{.w}}[山阴县](../Page/山阴县.md "wikilink"){{.w}}[应县](../Page/应县.md "wikilink"){{.w}}[右玉县](../Page/右玉县.md "wikilink")

|group8 = [晋中市](../Page/晋中市.md "wikilink") |list8 =
[榆次区](../Page/榆次区.md "wikilink"){{.w}}[介休市](../Page/介休市.md "wikilink"){{.w}}[榆社县](../Page/榆社县.md "wikilink"){{.w}}[左权县](../Page/左权县.md "wikilink"){{.w}}[和顺县](../Page/和顺县.md "wikilink"){{.w}}[昔阳县](../Page/昔阳县.md "wikilink"){{.w}}[寿阳县](../Page/寿阳县.md "wikilink"){{.w}}[太谷县](../Page/太谷县.md "wikilink"){{.w}}[祁县](../Page/祁县.md "wikilink"){{.w}}[平遥县](../Page/平遥县.md "wikilink"){{.w}}[灵石县](../Page/灵石县.md "wikilink")

|group9 = [运城市](../Page/运城市.md "wikilink") |list9 =
[盐湖区](../Page/盐湖区.md "wikilink"){{.w}}[永济市](../Page/永济市.md "wikilink"){{.w}}[河津市](../Page/河津市.md "wikilink"){{.w}}[临猗县](../Page/临猗县.md "wikilink"){{.w}}[万荣县](../Page/万荣县.md "wikilink"){{.w}}[闻喜县](../Page/闻喜县.md "wikilink"){{.w}}[夏县](../Page/夏县.md "wikilink"){{.w}}[稷山县](../Page/稷山县.md "wikilink"){{.w}}[新绛县](../Page/新绛县.md "wikilink"){{.w}}[绛县](../Page/绛县.md "wikilink"){{.w}}[垣曲县](../Page/垣曲县.md "wikilink"){{.w}}[平陆县](../Page/平陆县.md "wikilink"){{.w}}[芮城县](../Page/芮城县.md "wikilink")

|group10 = [忻州市](../Page/忻州市.md "wikilink") |list10 =
[忻府区](../Page/忻府区.md "wikilink"){{.w}}[原平市](../Page/原平市.md "wikilink"){{.w}}[定襄县](../Page/定襄县.md "wikilink"){{.w}}[五台县](../Page/五台县.md "wikilink"){{.w}}[代县](../Page/代县.md "wikilink"){{.w}}[繁峙县](../Page/繁峙县.md "wikilink"){{.w}}[宁武县](../Page/宁武县.md "wikilink"){{.w}}[静乐县](../Page/静乐县.md "wikilink"){{.w}}[神池县](../Page/神池县.md "wikilink"){{.w}}[五寨县](../Page/五寨县.md "wikilink"){{.w}}[岢岚县](../Page/岢岚县.md "wikilink"){{.w}}[河曲县](../Page/河曲县.md "wikilink"){{.w}}[保德县](../Page/保德县.md "wikilink"){{.w}}[偏关县](../Page/偏关县.md "wikilink")

|group11 = [临汾市](../Page/临汾市.md "wikilink") |list11 =
[尧都区](../Page/尧都区.md "wikilink"){{.w}}[侯马市](../Page/侯马市.md "wikilink"){{.w}}[霍州市](../Page/霍州市.md "wikilink"){{.w}}[曲沃县](../Page/曲沃县.md "wikilink"){{.w}}[翼城县](../Page/翼城县.md "wikilink"){{.w}}[襄汾县](../Page/襄汾县.md "wikilink"){{.w}}[洪洞县](../Page/洪洞县.md "wikilink"){{.w}}[古县](../Page/古县.md "wikilink"){{.w}}[安泽县](../Page/安泽县.md "wikilink"){{.w}}[浮山县](../Page/浮山县.md "wikilink"){{.w}}[吉县](../Page/吉县.md "wikilink"){{.w}}[乡宁县](../Page/乡宁县.md "wikilink"){{.w}}[大宁县](../Page/大宁县.md "wikilink"){{.w}}[隰县](../Page/隰县.md "wikilink"){{.w}}[永和县](../Page/永和县.md "wikilink"){{.w}}[蒲县](../Page/蒲县.md "wikilink")
{{.w}}[汾西县](../Page/汾西县.md "wikilink")

|group12 = [吕梁市](../Page/吕梁市.md "wikilink") |list12 =
[离石区](../Page/离石区.md "wikilink"){{.w}}[孝义市](../Page/孝义市.md "wikilink"){{.w}}[汾阳市](../Page/汾阳市.md "wikilink"){{.w}}[文水县](../Page/文水县.md "wikilink"){{.w}}[交城县](../Page/交城县.md "wikilink"){{.w}}[兴县](../Page/兴县.md "wikilink"){{.w}}[临县](../Page/临县.md "wikilink"){{.w}}[柳林县](../Page/柳林县.md "wikilink"){{.w}}[石楼县](../Page/石楼县.md "wikilink"){{.w}}[岚县](../Page/岚县.md "wikilink"){{.w}}[方山县](../Page/方山县.md "wikilink"){{.w}}[中阳县](../Page/中阳县.md "wikilink"){{.w}}[交口县](../Page/交口县.md "wikilink")
}}

|belowstyle = text-align: left; font-size: 85%; |below =
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[山西省乡级以上行政区列表](../Page/山西省乡级以上行政区列表.md "wikilink")
}}<noinclude>

</noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[\*](../Category/山西行政区划.md "wikilink")
[中华人民共和国山西省行政区划导航模板](../Category/中华人民共和国山西省行政区划导航模板.md "wikilink")