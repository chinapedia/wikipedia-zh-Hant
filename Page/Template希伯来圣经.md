<table>
<thead>
<tr class="header">
<th><div style="padding:0.4em 0 0 <!--necessary to center heading:-->0em; line-height:1.25em;">
</div>
<div style="padding-bottom:0.2em;padding-top:0.33em; font-size:200%;">
<p><strong><a href="塔纳赫.md" title="wikilink">塔纳赫</a> / <a href="旧约.md" title="wikilink">旧约</a></strong></p>
<p>{{#if:|<img src="Köln-Tora-und-Innenansicht-Synagoge-Glockengasse-040.JPG" title="fig:Köln-Tora-und-Innenansicht-Synagoge-Glockengasse-040.JPG" alt="Köln-Tora-und-Innenansicht-Synagoge-Glockengasse-040.JPG" width="25" /><br />
|<img src="Köln-Tora-und-Innenansicht-Synagoge-Glockengasse-040.JPG" title="fig:Köln-Tora-und-Innenansicht-Synagoge-Glockengasse-040.JPG" alt="Köln-Tora-und-Innenansicht-Synagoge-Glockengasse-040.JPG" width="60" /><br />
}}</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="犹太教.md" title="wikilink">犹太教和</a><a href="基督教.md" title="wikilink">基督教都接受为</a><a href="正典.md" title="wikilink">正典的书卷</a><br />
</p></td>
</tr>
<tr class="odd">
<td><p><a href="创世记.md" title="wikilink">创世记</a>{{.w}}<a href="出埃及记.md" title="wikilink">出埃及记</a>{{.w}}<a href="利未记.md" title="wikilink">利未记</a>{{.w}}<a href="民数记.md" title="wikilink">民数记</a>{{.w}}<a href="申命记.md" title="wikilink">申命记</a>{{.w}}<a href="约书亚记.md" title="wikilink">约书亚记</a>{{.w}}<a href="士师记.md" title="wikilink">士师记</a>{{.w}}<a href="路得记.md" title="wikilink">路得记</a>{{.w}}<a href="撒母耳記.md" title="wikilink">撒母耳記</a>{{.w}}<a href="列王紀_(聖經).md" title="wikilink">列王纪</a>{{.w}}<a href="历代志.md" title="wikilink">历代志</a>{{.w}}<a href="以斯拉记.md" title="wikilink">以斯拉记</a>{{.w}}<a href="尼希米记.md" title="wikilink">尼希米记</a>{{.w}}<a href="以斯帖记.md" title="wikilink">以斯帖记</a>{{.w}}<a href="约伯记.md" title="wikilink">约伯记</a>{{.w}}<a href="诗篇.md" title="wikilink">诗篇</a>{{.w}}<a href="箴言.md" title="wikilink">箴言</a>{{.w}}<a href="传道书.md" title="wikilink">传道书</a>{{.w}}<a href="雅歌.md" title="wikilink">雅歌</a>{{.w}}<a href="以赛亚书.md" title="wikilink">以赛亚书</a>{{.w}}<a href="耶利米书.md" title="wikilink">耶利米书</a>{{.w}}<a href="耶利米哀歌.md" title="wikilink">耶利米哀歌</a>{{.w}}<a href="以西结书.md" title="wikilink">以西结书</a>{{.w}}<a href="但以理书.md" title="wikilink">但以理书</a>{{.w}}<a href="小先知书.md" title="wikilink">小先知书</a>（<a href="何西阿书.md" title="wikilink">何西阿书</a>{{.w}}<a href="约珥书.md" title="wikilink">约珥书</a>{{.w}}<a href="阿摩司书.md" title="wikilink">阿摩司书</a>{{.w}}<a href="俄巴底亚书.md" title="wikilink">俄巴底亚书</a>{{.w}}<a href="约拿书.md" title="wikilink">约拿书</a>{{.w}}<a href="弥迦书.md" title="wikilink">弥迦书</a>{{.w}}<a href="那鸿书.md" title="wikilink">那鸿书</a>{{.w}}<a href="哈巴谷书.md" title="wikilink">哈巴谷书</a>{{.w}}<a href="西番雅书.md" title="wikilink">西番雅书</a>{{.w}}<a href="哈该书.md" title="wikilink">哈该书</a>{{.w}}<a href="撒迦利亚书.md" title="wikilink">撒迦利亚书</a>{{.w}}<a href="玛拉基书.md" title="wikilink">玛拉基书</a>）</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="天主教.md" title="wikilink">天主教与</a><a href="东正教.md" title="wikilink">东正教都接受的</a><a href="次经.md" title="wikilink">次经</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="多俾亚传.md" title="wikilink">多俾亚传</a>{{.w}}<a href="友弟德传.md" title="wikilink">犹滴传</a>{{.w}}<a href="馬加比一书.md" title="wikilink">馬加比一书</a>{{.w}}<a href="馬加比二书.md" title="wikilink">馬加比二书</a>{{.w}}<a href="所罗门智训.md" title="wikilink">所罗门智训</a>{{.w}}<a href="便西拉智训.md" title="wikilink">便西拉智训</a>{{.w}}<a href="巴錄書.md" title="wikilink">巴錄書</a>（<a href="耶利米书信.md" title="wikilink">耶利米书信</a>）{{.w}}<a href="但以理书补编.md" title="wikilink">但以理书补编</a>（即比新教的但以理书多出的3个段落）{{.w}}<a href="以斯帖记补编.md" title="wikilink">以斯帖记补编</a></p></td>
</tr>
<tr class="even">
<td><p><strong>此外<a href="东正教.md" title="wikilink">东正教还接受的次经</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="以斯拉續篇上卷.md" title="wikilink">以斯拉續篇上卷</a>{{.w}}<a href="诗篇.md" title="wikilink">诗篇续编</a>（即第<a href="151篇.md" title="wikilink">151篇和</a><a href="玛拿西祷词.md" title="wikilink">玛拿西祷词</a>）{{.w}}<a href="馬加比三书.md" title="wikilink">馬加比三书</a>{{.w}}<a href="马加比四书.md" title="wikilink">马加比四书</a>（附录）</p></td>
</tr>
<tr class="even">
<td><p><img src="Bible.malmesbury.arp.jpg" title="fig:Bible.malmesbury.arp.jpg" alt="Bible.malmesbury.arp.jpg" width="30" /> <strong><a href="Portal:圣经.md" title="wikilink">主题:圣经</a></strong></p></td>
</tr>
<tr class="odd">
<td><hr/></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

<noinclude>

</noinclude>

[希伯来圣经导航模板](../Category/希伯来圣经导航模板.md "wikilink")
[\*](../Category/希伯来圣经.md "wikilink")
[\*](../Category/旧约圣经.md "wikilink")