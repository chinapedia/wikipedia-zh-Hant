[江南区](../Page/江南区_\(南宁市\).md "wikilink"){{.w}}[西乡塘区](../Page/西乡塘区.md "wikilink"){{.w}}[良庆区](../Page/良庆区.md "wikilink"){{.w}}[邕宁区](../Page/邕宁区.md "wikilink"){{.w}}[武鸣区](../Page/武鸣区.md "wikilink"){{.w}}[隆安县](../Page/隆安县.md "wikilink"){{.w}}[马山县](../Page/马山县.md "wikilink"){{.w}}[上林县](../Page/上林县.md "wikilink"){{.w}}[宾阳县](../Page/宾阳县.md "wikilink"){{.w}}[横县](../Page/横县.md "wikilink")

|group3 = [柳州市](../Page/柳州市.md "wikilink") |list3 =
[城中区](../Page/城中区_\(柳州市\).md "wikilink"){{.w}}[鱼峰区](../Page/鱼峰区.md "wikilink"){{.w}}[柳南区](../Page/柳南区.md "wikilink"){{.w}}[柳北区](../Page/柳北区.md "wikilink"){{.w}}[柳江区](../Page/柳江区.md "wikilink"){{.w}}[柳城县](../Page/柳城县.md "wikilink"){{.w}}[鹿寨县](../Page/鹿寨县.md "wikilink"){{.w}}[融安县](../Page/融安县.md "wikilink"){{.w}}[融水苗族自治县](../Page/融水苗族自治县.md "wikilink"){{.w}}[三江侗族自治县](../Page/三江侗族自治县.md "wikilink")

|group4 = [桂林市](../Page/桂林市.md "wikilink") |list4 =
[临桂区](../Page/临桂区.md "wikilink"){{.w}}[秀峰区](../Page/秀峰区.md "wikilink"){{.w}}[叠彩区](../Page/叠彩区.md "wikilink"){{.w}}[象山区](../Page/象山区.md "wikilink"){{.w}}[七星区](../Page/七星区.md "wikilink"){{.w}}[雁山区](../Page/雁山区.md "wikilink"){{.w}}[荔浦市](../Page/荔浦市.md "wikilink"){{.w}}[阳朔县](../Page/阳朔县.md "wikilink"){{.w}}[灵川县](../Page/灵川县.md "wikilink"){{.w}}[全州县](../Page/全州县.md "wikilink"){{.w}}[兴安县](../Page/兴安县.md "wikilink"){{.w}}[永福县](../Page/永福县.md "wikilink"){{.w}}[灌阳县](../Page/灌阳县.md "wikilink"){{.w}}[资源县](../Page/资源县.md "wikilink"){{.w}}[平乐县](../Page/平乐县.md "wikilink"){{.w}}[龙胜各族自治县](../Page/龙胜各族自治县.md "wikilink"){{.w}}[恭城瑶族自治县](../Page/恭城瑶族自治县.md "wikilink")

|group5 = [梧州市](../Page/梧州市.md "wikilink") |list5 =
[长洲区](../Page/长洲区.md "wikilink"){{.w}}[万秀区](../Page/万秀区.md "wikilink"){{.w}}[龙圩区](../Page/龙圩区.md "wikilink"){{.w}}[岑溪市](../Page/岑溪市.md "wikilink"){{.w}}[苍梧县](../Page/苍梧县.md "wikilink"){{.w}}[藤县](../Page/藤县.md "wikilink"){{.w}}[蒙山县](../Page/蒙山县.md "wikilink")

|group6 = [北海市](../Page/北海市.md "wikilink") |list6 =
[海城区](../Page/海城区.md "wikilink"){{.w}}[银海区](../Page/银海区.md "wikilink"){{.w}}[铁山港区](../Page/铁山港区.md "wikilink"){{.w}}[合浦县](../Page/合浦县.md "wikilink")

|group7 = [防城港市](../Page/防城港市.md "wikilink") |list7 =
[港口区](../Page/港口区.md "wikilink"){{.w}}[防城区](../Page/防城区.md "wikilink"){{.w}}[东兴市](../Page/东兴市.md "wikilink"){{.w}}[上思县](../Page/上思县.md "wikilink")

|group8 = [钦州市](../Page/钦州市.md "wikilink") |list8 =
[钦北区](../Page/钦北区.md "wikilink"){{.w}}[钦南区](../Page/钦南区.md "wikilink"){{.w}}[灵山县](../Page/灵山县.md "wikilink"){{.w}}[浦北县](../Page/浦北县.md "wikilink")

|group9 = [贵港市](../Page/贵港市.md "wikilink") |list9 =
[港北区](../Page/港北区_\(贵港市\).md "wikilink"){{.w}}[港南区](../Page/港南區_\(廣西\).md "wikilink"){{.w}}[覃塘区](../Page/覃塘区.md "wikilink"){{.w}}[桂平市](../Page/桂平市.md "wikilink"){{.w}}[平南县](../Page/平南县.md "wikilink")

|group10 = [玉林市](../Page/玉林市.md "wikilink") |list10 =
[玉州区](../Page/玉州区.md "wikilink"){{.w}}[福绵区](../Page/福绵区.md "wikilink"){{.w}}[北流市](../Page/北流市.md "wikilink"){{.w}}[容县](../Page/容县.md "wikilink"){{.w}}[陆川县](../Page/陆川县.md "wikilink"){{.w}}[博白县](../Page/博白县.md "wikilink"){{.w}}[兴业县](../Page/兴业县.md "wikilink")

|group11 = [百色市](../Page/百色市.md "wikilink") |list11 =
[右江区](../Page/右江区.md "wikilink"){{.w}}[靖西市](../Page/靖西市.md "wikilink"){{.w}}[田阳县](../Page/田阳县.md "wikilink"){{.w}}[田东县](../Page/田东县.md "wikilink"){{.w}}[平果县](../Page/平果县.md "wikilink"){{.w}}[德保县](../Page/德保县.md "wikilink"){{.w}}[那坡县](../Page/那坡县.md "wikilink"){{.w}}[凌云县](../Page/凌云县.md "wikilink"){{.w}}[乐业县](../Page/乐业县.md "wikilink"){{.w}}[田林县](../Page/田林县.md "wikilink"){{.w}}[西林县](../Page/西林县.md "wikilink"){{.w}}[隆林各族自治县](../Page/隆林各族自治县.md "wikilink")

|group12 = [贺州市](../Page/贺州市.md "wikilink") |list12 =
[八步区](../Page/八步区.md "wikilink"){{.w}}[平桂区](../Page/平桂区.md "wikilink"){{.w}}[昭平县](../Page/昭平县.md "wikilink"){{.w}}[-{A](../Page/钟山县.md "wikilink"){{.w}}[富川瑶族自治县](../Page/富川瑶族自治县.md "wikilink")

|group13 = [河池市](../Page/河池市.md "wikilink") |list13 =
[宜州区](../Page/宜州区.md "wikilink"){{.w}}[金城江区](../Page/金城江区.md "wikilink"){{.w}}[南丹县](../Page/南丹县.md "wikilink"){{.w}}[天峨县](../Page/天峨县.md "wikilink"){{.w}}[凤山县](../Page/凤山县.md "wikilink"){{.w}}[东兰县](../Page/东兰县.md "wikilink"){{.w}}[罗城仫佬族自治县](../Page/罗城仫佬族自治县.md "wikilink"){{.w}}[环江毛南族自治县](../Page/环江毛南族自治县.md "wikilink"){{.w}}[巴马瑶族自治县](../Page/巴马瑶族自治县.md "wikilink"){{.w}}[都安瑶族自治县](../Page/都安瑶族自治县.md "wikilink"){{.w}}[大化瑶族自治县](../Page/大化瑶族自治县.md "wikilink")

|group14 = [来宾市](../Page/来宾市.md "wikilink") |list14 =
[兴宾区](../Page/兴宾区.md "wikilink"){{.w}}[合山市](../Page/合山市.md "wikilink"){{.w}}[忻城县](../Page/忻城县.md "wikilink"){{.w}}[象州县](../Page/象州县.md "wikilink"){{.w}}[武宣县](../Page/武宣县.md "wikilink"){{.w}}[金秀瑶族自治县](../Page/金秀瑶族自治县.md "wikilink")

|group15 = [崇左市](../Page/崇左市.md "wikilink") |list15 =
[江州区](../Page/江州区.md "wikilink"){{.w}}[凭祥市](../Page/凭祥市.md "wikilink"){{.w}}[扶绥县](../Page/扶绥县.md "wikilink"){{.w}}[宁明县](../Page/宁明县.md "wikilink"){{.w}}[龙州县](../Page/龙州县.md "wikilink"){{.w}}[大新县](../Page/大新县.md "wikilink"){{.w}}[天等县](../Page/天等县.md "wikilink")
}}

|belowstyle = text-align: left; font-size: 80%; |below =
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[广西壮族自治区乡级以上行政区列表](../Page/广西壮族自治区乡级以上行政区列表.md "wikilink")。
}}<noinclude>

</noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[\*](../Category/广西行政区划.md "wikilink")
[广西行政区划模板](../Category/广西行政区划模板.md "wikilink")