{{ infobox | title = }} | subheader =
[J·R·R·托爾金作品中的人物](J·R·R·托爾金.md "wikilink")
| image = {{\#if:|![{{{image_character}}}]({{{image_character}}}
"{{{image_character}}}")}} | caption =

| label1 = [別名](別名.md "wikilink") | data1 =  | label2 =
[头衔](头衔.md "wikilink") | data2 =  | label3 =
[種族](種族.md "wikilink") | data3 =  | label4 =
[族群](族群.md "wikilink") | data4 =  | label5 =
[家族](家族.md "wikilink") | data5 =  | label6 = 出生日期 | data6 =  |
label7 = 前往[阿門洲日期](阿門洲.md "wikilink") | data7 =  | label8 = 逝世日期 | data8
=  | label9 = [領土](領土.md "wikilink") | data9 =  | label10 =
[性別](性別.md "wikilink") | data10 =  | label11 =
[壽命](壽命.md "wikilink") | data11 =  | label12 =
[武器](武器.md "wikilink") | data12 =  | label13 =
[演員](演員.md "wikilink") | data13 =  | label14 =
[聲音](聲音.md "wikilink") | data14 =  | label15 =
[書目](:../Category/中土大陸作品.md "wikilink") | data15 =

}}<noinclude>

[en:Template:Infobox Tolkien
character](en:Template:Infobox_Tolkien_character.md "wikilink")
[pt:Predefinição:Tolkienperso](pt:Predefinição:Tolkienperso.md "wikilink")
[tr:Şablon:Orta Dünya
Karakteri](tr:Şablon:Orta_Dünya_Karakteri.md "wikilink") </noinclude>

[Category:虚构人物信息框模板](../Category/虚构人物信息框模板.md "wikilink")