[新莊區](../Page/新莊區.md "wikilink"){{-w}}[中和區](../Page/中和區.md "wikilink"){{-w}}[永和區](../Page/永和區.md "wikilink"){{-w}}[土城區](../Page/土城區.md "wikilink"){{-w}}[樹林區](../Page/樹林區.md "wikilink"){{-w}}[三峽區](../Page/三峽區.md "wikilink"){{-w}}[鶯歌區](../Page/鶯歌區.md "wikilink"){{-w}}[三重區](../Page/三重區.md "wikilink"){{-w}}[蘆洲區](../Page/蘆洲區.md "wikilink"){{-w}}[五股區](../Page/五股區.md "wikilink"){{-w}}[泰山區](../Page/泰山區_\(台灣\).md "wikilink"){{-w}}[林口區](../Page/林口區.md "wikilink"){{-w}}[八里區](../Page/八里區.md "wikilink"){{-w}}[淡水區](../Page/淡水區.md "wikilink"){{-w}}[三芝區](../Page/三芝區.md "wikilink"){{-w}}[石門區](../Page/石門區.md "wikilink"){{-w}}[金山區](../Page/金山區_\(台灣\).md "wikilink"){{-w}}[萬里區](../Page/萬里區.md "wikilink"){{-w}}[汐止區](../Page/汐止區.md "wikilink"){{-w}}[瑞芳區](../Page/瑞芳區.md "wikilink"){{-w}}[貢寮區](../Page/貢寮區.md "wikilink"){{-w}}[平溪區](../Page/平溪區.md "wikilink"){{-w}}[雙溪區](../Page/雙溪區.md "wikilink"){{-w}}[新店區](../Page/新店區.md "wikilink"){{-w}}[深坑區](../Page/深坑區.md "wikilink"){{-w}}[石碇區](../Page/石碇區.md "wikilink"){{-w}}[坪林區](../Page/坪林區.md "wikilink")

` |group2 = `[`原住民區`](../Page/直轄市山地原住民區.md "wikilink")
` | list2 = `[`烏來區`](../Page/烏來區.md "wikilink")
` |group3 = 歷史沿革`
` | list3 = `[`天興州`](../Page/天興州.md "wikilink")
`   |group2 = 清治`
`   | list2 = `[`諸羅縣`](../Page/諸羅縣.md "wikilink")`{{→}}`[`淡水廳`](../Page/淡水廳.md "wikilink")`{{→}}`[`臺北府`](../Page/臺北府.md "wikilink")`（`[`淡水縣`](../Page/淡水縣.md "wikilink")`{{.w}}`[`基隆廳`](../Page/基隆廳.md "wikilink")`{{.w}}`[`南雅廳`](../Page/南雅廳.md "wikilink")`）`
`   |group3 = 日治`
`   | list3 =  `[`臺北縣`](../Page/臺北縣_\(日治時期\).md "wikilink")`{{→}}`[`臺北廳`](../Page/臺北廳.md "wikilink")`{{.w}}`[`基隆廳`](../Page/基隆廳.md "wikilink")`{{.w}}`[`深坑廳`](../Page/深坑廳.md "wikilink")`{{→}}`[`臺北廳`](../Page/臺北廳.md "wikilink")`{{→}}`[`臺北州`](../Page/臺北州.md "wikilink")`（`[`淡水郡`](../Page/淡水郡.md "wikilink")`{{.w}}`[`七星郡`](../Page/七星郡.md "wikilink")`{{.w}}`[`新莊郡`](../Page/新莊郡.md "wikilink")`{{.w}}`[`海山郡`](../Page/海山郡.md "wikilink")`{{.w}}`[`基隆郡`](../Page/基隆郡.md "wikilink")`{{.w}}`[`文山郡`](../Page/文山郡.md "wikilink")`）`
`   |group4 = 民國`
`   | list4 = 臺北縣``（淡水區{{.w}}七星區{{.w}}新莊區{{.w}}海山區{{.w}}`[`基隆區`](../Page/基隆北海岸.md "wikilink")`{{.w}}`[`文山區`](../Page/大文山區.md "wikilink")`）{{→}}`[`臺北縣`](../Page/臺北縣.md "wikilink")
` }}`
` | below = `[`新北市地理`](../Category/新北市地理.md "wikilink")`{{.w}}`[`新北市交通`](../Category/新北市交通.md "wikilink")

}}<includeonly>{{\#IFEQ:|no||}}</includeonly><noinclude></noinclude>

[Category:新北市行政區劃](../Category/新北市行政區劃.md "wikilink")