<noinclude></noinclude>  |- rowspan="2" | valign="top" |
![Flag_of_Japan.svg](Flag_of_Japan.svg "Flag_of_Japan.svg")
|align="center" style="background:\#BFD7FF;" width="100%" |  |
![Flag_of_Japan.svg](Flag_of_Japan.svg "Flag_of_Japan.svg")   |-
style="text-align:center;" \! [甲組聯賽](日本甲組職業足球聯賽.md "wikilink")（J1） |-
align=center | [仙台維加泰](仙台維加泰.md "wikilink"){{·}}
[北海道札幌岡薩多](北海道札幌岡薩多.md "wikilink"){{·}}
[鹿島鹿角](鹿島鹿角.md "wikilink"){{·}} [浦和紅鑽](浦和紅鑽.md "wikilink"){{·}}
[長崎成功丸](長崎成功丸.md "wikilink"){{·}}
[柏雷素爾](柏雷素爾.md "wikilink"){{·}}
[FC東京](東京足球會.md "wikilink"){{·}}
[川崎前鋒](川崎前鋒.md "wikilink"){{·}}
[橫濱水手](橫濱水手.md "wikilink")
[湘南比馬](湘南比馬.md "wikilink"){{·}} [名古屋鯨魚](名古屋鯨魚.md "wikilink"){{·}}
[磐田-{zh-cn:山葉; zh-tw:喜悅; zh-hk:山葉}-](磐田喜悅.md "wikilink"){{·}}
[大阪飛腳](大阪飛腳.md "wikilink"){{·}}
[神戶勝利船](神戶勝利船.md "wikilink"){{·}}
[廣島三箭](廣島三箭.md "wikilink"){{·}}
[大阪櫻花](大阪櫻花.md "wikilink"){{·}}
[鳥栖砂岩](鳥栖砂岩.md "wikilink"){{·}}
[清水心跳](清水心跳.md "wikilink")

|- \! [乙組聯賽](日本乙組職業足球聯賽.md "wikilink")（J2） |- align=center |
[甲府風林](甲府風林.md "wikilink"){{·}}
[福岡黃蜂](福岡黃蜂.md "wikilink"){{·}}
[山形山神](山形山神.md "wikilink"){{·}}
[水戶蜀葵](水戶蜀葵.md "wikilink"){{·}} [-{zh-cn:千叶市原;
zh-tw:市原千葉; zh-hk:千葉市原}-](市原千葉JEF聯.md "wikilink"){{·}}
[東京綠茵](東京綠茵.md "wikilink"){{·}}
[町田澤維亞](町田澤維亞足球會.md "wikilink"){{·}}
[橫濱FC](橫濱FC.md "wikilink"){{·}}
[松本山雅](松本山雅足球會.md "wikilink"){{·}}
[FC岐阜](岐阜足球會.md "wikilink"){{·}}
[金澤薩維根](金澤薩維根.md "wikilink")
[京都不死鳥](京都不死鳥.md "wikilink"){{·}} [岡山綠雉](岡山綠雉.md "wikilink"){{·}}
[山口雷法](山口雷法足球會.md "wikilink"){{·}}
[-{zh-hant:讚岐釜玉海;zh-hans:赞岐釜玉海;zh-hk:讚岐卡馬達馬尼;}-](讚岐釜玉海.md "wikilink"){{·}}
[德島漩渦](德島漩渦.md "wikilink"){{·}} [FC愛媛](愛媛足球會.md "wikilink"){{·}}
[大宮松鼠](大宮松鼠.md "wikilink"){{·}}
[大分三神](大分三神.md "wikilink"){{·}}
[栃木SC](栃木足球會.md "wikilink"){{·}}
[熊本羅亞素](熊本羅亞素.md "wikilink"){{·}}
[新潟天鵝](新潟天鵝.md "wikilink")

|- \! [丙組聯賽](日本丙組職業足球聯賽.md "wikilink")（J3） |- align=center |
[盛岡仙鶴](盛岡仙鶴.md "wikilink"){{·}}
[秋田藍閃電](秋田藍閃電.md "wikilink"){{·}}
[福島聯](福島聯足球會.md "wikilink"){{·}}
[群馬草津溫泉](群馬草津溫泉.md "wikilink"){{·}}
[橫濱SCC](橫濱體育及文化會.md "wikilink"){{·}}
[SC相模原](相模原體育會.md "wikilink"){{·}}
[長野帕塞羅](長野帕塞羅體育會.md "wikilink"){{·}}
[富山勝利](富山勝利.md "wikilink"){{·}}
[藤枝MYFC](藤枝MYFC.md "wikilink"){{·}}
[鳥取飛翔](鳥取飛翔.md "wikilink"){{·}} [鹿兒島聯](鹿兒島聯足球會.md "wikilink"){{·}}
[FC琉球](琉球足球會.md "wikilink"){{·}} [沼津青藍](沼津青藍.md "wikilink"){{·}}
[北九州向日葵](北九州向日葵.md "wikilink"){{·}}
[FC東京U](東京足球會.md "wikilink")-23{{·}}
[大阪飛腳U](大阪飛腳.md "wikilink")-23{{·}}
[大阪櫻花U](大阪櫻花.md "wikilink")-23 |- \!
[百年計劃](日職聯百年計劃.md "wikilink")[準加盟球隊](日職聯百年計劃加盟球會.md "wikilink")
|- align=center | [八戶雲羅里](八戶雲羅里.md "wikilink"){{·}}
[栃木葡萄](栃木葡萄足球會.md "wikilink"){{·}}
[前橋圖南](前橋圖南.md "wikilink"){{·}}
[東京武藏野城](東京武藏野城足球會.md "wikilink"){{·}}
[奈良俱樂部](奈良俱樂部.md "wikilink"){{·}} [FC今治](今治足球會.md "wikilink") |-
\! 已解散球隊 |- align=center
|[橫濱飛翼](橫濱飛翼.md "wikilink"){{·}}[日職聯U22](日職聯U22選拔隊.md "wikilink")
|- \! 日聯主要賽事 |- align=center
|[聯賽盃](日本聯賽盃.md "wikilink"){{·}}[超級盃](日本超級盃.md "wikilink"){{·}}[銀行錦標](骏河银行锦标赛.md "wikilink"){{·}}[J1資格季後賽](J1資格季後賽.md "wikilink"){{·}}[新年盃](日本新年盃.md "wikilink"){{·}}[亞洲挑戰盃](日職聯亞洲挑戰盃.md "wikilink"){{·}}[世界挑戰盃](日職聯世界挑戰盃.md "wikilink"){{·}}[育成比賽日](日職聯育成比賽日.md "wikilink"){{·}}[青年盃](日本青年盃.md "wikilink")
|- \! 相關條目 |- align=center
|[天皇盃](日本天皇杯.md "wikilink"){{·}}[明星選舉](日職聯足球明星選舉.md "wikilink"){{·}}[最佳11人](日職聯最佳11人.md "wikilink"){{·}}[月間MVP](日職聯月間MVP.md "wikilink"){{·}}[史上最佳](日職史上最佳球員.md "wikilink"){{·}}[日職聯PR部](日職聯PR部.md "wikilink"){{·}}[百年計劃](日職聯百年計劃.md "wikilink"){{·}}[發牌制度](日職聯發牌制度.md "wikilink"){{·}}[日職聯準會員制度](日職聯準會員制度.md "wikilink"){{·}}[日職聯準加盟球會](日職聯準加盟球會.md "wikilink"){{·}}[豐田超級盃](豐田超級盃.md "wikilink")
 <noinclude>

</noinclude>

[](../Category/體育導航模板.md "wikilink")
[](../Category/日本導航模板.md "wikilink")
[](../Category/日本足球模板.md "wikilink")