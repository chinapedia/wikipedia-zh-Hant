{{Navbox |name = 日本都道府縣 |title = [都道府縣](../Page/日本行政區劃.md "wikilink")
|image =
![Regions_and_Prefectures_of_Japan_-_blank.svg](https://zh.wikipedia.org/wiki/File:Regions_and_Prefectures_of_Japan_-_blank.svg
"Regions_and_Prefectures_of_Japan_-_blank.svg")

|group1style = background: \#ececec; text-align: center; |group1 =
[{{Color](../Page/日本地理分区.md "wikilink") |list1style = background:
\#ececec; text-align: center; |list1 =
47[一级行政区](../Page/一级行政区.md "wikilink")（1[都](../Page/首都.md "wikilink")、1[道](../Page/道_\(行政区划\).md "wikilink")、2[府](../Page/府_\(行政区划\).md "wikilink")、43[县](../Page/县.md "wikilink")）

|group2style = background: \#FF8585; |group2 =
[{{Color](../Page/北海道.md "wikilink") |list2 =
[北海道](../Page/北海道.md "wikilink")

|group3style = background: \#FFFB91; |group3 =
[{{Color](../Page/东北地方.md "wikilink") |list3 =
[青森縣](../Page/青森縣.md "wikilink")・
[岩手县](../Page/岩手县.md "wikilink")・
[宮城縣](../Page/宮城縣.md "wikilink")・
[秋田县](../Page/秋田县.md "wikilink")・
[山形县](../Page/山形县.md "wikilink")・
[福岛县](../Page/福岛县.md "wikilink")

|group4style = background: \#79FF76; |group4 =
[{{Color](../Page/關東地方.md "wikilink") |list4 =
[茨城縣](../Page/茨城縣.md "wikilink")・
[栃木縣](../Page/栃木縣.md "wikilink")・
[群馬縣](../Page/群馬縣.md "wikilink")・
[埼玉縣](../Page/埼玉縣.md "wikilink")・
[千葉縣](../Page/千葉縣.md "wikilink")・
[東京都](../Page/東京都.md "wikilink")・
[神奈川縣](../Page/神奈川縣.md "wikilink")

|group5style = background: \#8BFFE8; |group5 =
[{{Color](../Page/中部地方.md "wikilink") |list5 =
[新潟县](../Page/新潟县.md "wikilink")・
[富山縣](../Page/富山縣.md "wikilink")・
[石川縣](../Page/石川縣.md "wikilink")・
[福井縣](../Page/福井縣.md "wikilink")・
[山梨县](../Page/山梨县.md "wikilink")・
[长野县](../Page/长野县.md "wikilink")・
[岐阜县](../Page/岐阜县.md "wikilink")・
[靜岡縣](../Page/靜岡縣.md "wikilink")・
[愛知縣](../Page/愛知縣.md "wikilink")

|group6style = background: \#7272FF; |group6 =
[{{Color](../Page/近畿地方.md "wikilink") |list6 =
[三重县](../Page/三重县.md "wikilink")・
[滋贺县](../Page/滋贺县.md "wikilink")・
[京都府](../Page/京都府.md "wikilink")・
[大阪府](../Page/大阪府.md "wikilink")・
[兵库县](../Page/兵库县.md "wikilink")・
[奈良县](../Page/奈良县.md "wikilink")・
[和歌山县](../Page/和歌山县.md "wikilink")

|group7style = background: \#FF9148; |group7 =
[{{Color](../Page/中国地方.md "wikilink") |list7 =
[鳥取縣](../Page/鳥取縣.md "wikilink")・
[岛根县](../Page/岛根县.md "wikilink")・
[岡山縣](../Page/岡山縣.md "wikilink")・
[廣島縣](../Page/廣島縣.md "wikilink")・
[山口县](../Page/山口县.md "wikilink")

|group8style = background: \#D16EFF; |group8 =
[{{Color](../Page/四國.md "wikilink") |list8 =
[德岛县](../Page/德岛县.md "wikilink")・
[香川县](../Page/香川县.md "wikilink")・
[爱媛县](../Page/爱媛县.md "wikilink")・
[高知县](../Page/高知县.md "wikilink")

|group9style = background: \#C5C5C5; |group9 = \[\[九州_(日本)|<noinclude>

## 相关模板

  -
</noinclude>

[\*](../Category/日本都道府縣.md "wikilink")
[日本行政區劃模板](../Category/日本行政區劃模板.md "wikilink")
[Japan](../Category/各国一级行政区划导航模板.md "wikilink")