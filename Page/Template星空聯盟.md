[愛琴海航空](愛琴海航空.md "wikilink"){{.w}}[加拿大航空](加拿大航空.md "wikilink"){{.w}}[中国国际航空](中国国际航空.md "wikilink"){{.w}}[印度航空](印度航空.md "wikilink"){{.w}}[長榮航空](長榮航空.md "wikilink"){{.w}}[新西蘭航空](新西蘭航空.md "wikilink"){{.w}}[全日空](全日本空輸.md "wikilink"){{.w}}[韓亞航空](韓亞航空.md "wikilink"){{.w}}[奧地利航空](奧地利航空.md "wikilink"){{.w}}[哥倫比亞航空](哥倫比亞航空.md "wikilink"){{.w}}[布鲁塞尔航空](布鲁塞尔航空.md "wikilink"){{.w}}[巴拿馬航空](巴拿馬航空.md "wikilink"){{.w}}[克罗地亚航空](克罗地亚航空.md "wikilink"){{.w}}[埃及航空](埃及航空.md "wikilink"){{.w}}[埃塞俄比亞航空](埃塞俄比亞航空.md "wikilink"){{.w}}[波蘭航空](波蘭航空.md "wikilink"){{.w}}[汉莎航空](汉莎航空.md "wikilink"){{.w}}[北歐航空](北歐航空.md "wikilink"){{.w}}[深圳航空](深圳航空.md "wikilink"){{.w}}[新加坡航空](新加坡航空.md "wikilink"){{.w}}[南非航空](南非航空.md "wikilink"){{.w}}[瑞士國際航空](瑞士國際航空.md "wikilink"){{.w}}[葡萄牙航空](葡萄牙航空.md "wikilink"){{.w}}[泰國國際航空](泰國國際航空.md "wikilink"){{.w}}[土耳其航空](土耳其航空.md "wikilink"){{.w}}[聯合航空](聯合航空.md "wikilink")

|group2 = 子成員 |list2 =
[加拿大快运航空](加拿大快运航空.md "wikilink"){{.w}}[紐西蘭支線航空](紐西蘭支線航空.md "wikilink"){{.w}}[全日空日本航空](全日空日本航空.md "wikilink"){{.w}}[全日空之翼航空](全日空之翼航空.md "wikilink"){{.w}}[Helicol](Helicol.md "wikilink"){{.w}}[薩爾瓦多哥倫比亞航空](薩爾瓦多哥倫比亞航空.md "wikilink"){{.w}}[巴西哥倫比亞航空](巴西哥倫比亞航空.md "wikilink"){{.w}}[哥斯大黎加哥倫比亞航空](哥斯大黎加哥倫比亞航空.md "wikilink"){{.w}}[秘魯哥倫比亞航空](秘魯哥倫比亞航空.md "wikilink"){{.w}}[哥倫比亞巴拿馬航空](哥倫比亞巴拿馬航空.md "wikilink"){{.w}}[埃及快運航空](埃及快運航空.md "wikilink"){{.w}}[汉莎区域航空](汉莎区域航空.md "wikilink"){{.w}}[南非连接航空](南非连接航空.md "wikilink"){{.w}}[南非快運航空](南非快運航空.md "wikilink"){{.w}}[泰國微笑航空](泰國微笑航空.md "wikilink"){{.w}}[聯航快運](聯航快運.md "wikilink"){{.w}}[TAP快運航空](TAP快運航空.md "wikilink")

|group3 = 优连伙伴 |list3 = [吉祥航空](吉祥航空.md "wikilink")

|group4 = 即將加入成員 |list4 =

|group5 = 已經退出成員 |list5 =
[澳洲安捷航空](澳洲安捷航空.md "wikilink"){{.w}}[蓝天航空](蓝天航空.md "wikilink"){{.w}}[英倫航空](英倫航空.md "wikilink"){{.w}}[美國大陸航空](美國大陸航空.md "wikilink"){{.w}}[墨西哥航空](墨西哥航空.md "wikilink"){{.w}}[上海航空](上海航空.md "wikilink"){{.w}}[西班牙航空](西班牙航空.md "wikilink"){{.w}}[巴西天马航空](巴西南美航空.md "wikilink"){{.w}}[全美航空](全美航空.md "wikilink"){{.w}}[巴西航空](里约格朗德航空.md "wikilink")

|group6=已結業
|list6=[勞達航空](勞達航空.md "wikilink"){{.w}}[瑞士歐洲航空](瑞士歐洲航空.md "wikilink"){{.w}}[PGA
Express](PGA_Express.md "wikilink"){{.w}}[Portugália](Portugália.md "wikilink")
}}

|below = }}

<noinclude> </noinclude>

[Category:星空聯盟](../Category/星空聯盟.md "wikilink")
[\*](../Category/星空聯盟.md "wikilink")
[Category:航空模板](../Category/航空模板.md "wikilink")