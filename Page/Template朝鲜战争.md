<noinclude>
</noinclude>[朝鲜半岛统一问题](朝鲜半岛统一问题.md "wikilink"){{.w}}[空战](朝鲜战争空战.md "wikilink")（[米格走廊](米格走廊.md "wikilink")）{{.w}}[朝-{}-鮮停戰談判](朝鲜停战谈判.md "wikilink")（[战俘遣返问题](朝鲜战争战俘遣返问题.md "wikilink")）{{.w}}[紀念建築](Template:韓戰紀念建築.md "wikilink")

|group1=[朝鮮人民軍攻势](朝鮮人民軍.md "wikilink")
|list1=[甕津](甕津戰役.md "wikilink"){{·w}}[開城-汶山](開城-汶山戰役.md "wikilink"){{·w}}[高浪浦](高浪浦戰役.md "wikilink"){{·w}}[抱川](抱川戰役.md "wikilink"){{·w}}[東豆川](東豆川戰役.md "wikilink"){{·w}}[春川](春川戰役.md "wikilink"){{·w}}[議政府](議政府戰役.md "wikilink"){{·w}}[第一次漢城](第一次漢城戰役.md "wikilink"){{·w}}[江陵](江陵戰役.md "wikilink"){{·w}}[彌阿里](彌阿里戰役.md "wikilink"){{·w}}[漢江](漢江戰役_\(1950年\).md "wikilink"){{·w}}{{·w}}[始興-安養-水原](始興-安養-水原戰役.md "wikilink"){{·w}}

|group2=[聯合國軍介入](聯合國軍.md "wikilink")
|list2={{·w}}[烏山](烏山戰役.md "wikilink"){{·w}}{{·w}}{{·w}}[同乐里](同樂里戰役.md "wikilink"){{·w}}[丹陽郡](丹陽戰役.md "wikilink"){{·w}}[鎭川郡](鎮川戰役.md "wikilink"){{·w}}[忠州](忠州戰役.md "wikilink"){{·w}}[梨花嶺](梨花嶺戰役.md "wikilink"){{·w}}[大田](大田戰役.md "wikilink"){{·w}}[化寧場](化寧場.md "wikilink"){{·w}}{{·w}}[安東](安東戰役.md "wikilink"){{·w}}[浦項](浦項戰役.md "wikilink"){{·w}}{{·w}}{{·w}}**[釜山](釜山環形防禦圈戰役.md "wikilink")**{{·w}}[慶州](慶州戰役.md "wikilink"){{·w}}[海州](海州海戰.md "wikilink"){{·w}}**[仁川](仁川登陸.md "wikilink")**{{·w}}[第二次漢城](第二次漢城戰役.md "wikilink"){{·w}}{{·w}}[沙里院](沙里院戰役.md "wikilink"){{·w}}[榮州](榮州戰役.md "wikilink"){{·w}}[靈興島](靈興島戰役.md "wikilink"){{·w}}[金川郡](金川郡戰役.md "wikilink"){{·w}}[平壤-元山](平壤-元山戰役.md "wikilink")（[平壤](平壤戰役_\(1950年\).md "wikilink"){{·w}}[元山](元山戰役.md "wikilink")）{{·w}}[順川-肅川](順川-肅川戰役.md "wikilink"){{·w}}[熙川](熙川戰役.md "wikilink"){{·w}}[楚川郡](楚川郡.md "wikilink"){{·w}}

|group3=[中國人民志願軍參戰](中國人民志願軍.md "wikilink") |list3=
**[云山](云山战斗.md "wikilink")**{{·w}}[博川](博川戰鬥.md "wikilink"){{·w}}[第一次長津湖](長津湖阻擊戰.md "wikilink")
|group2=[第二次戰役](抗美援朝第二次战役.md "wikilink")
|list2=**[清川江](清川江戰役.md "wikilink")**{{·w}}{{·w}}**[第二次長津湖](長津湖戰役.md "wikilink")**{{·w}}[第31团级战鬥队](北極熊團.md "wikilink")
|group3=[第三次戰役](抗美援朝第三次战役.md "wikilink")
|list3=**[第三次汉城](第三次汉城战役.md "wikilink")**{{·w}}
|group4=[第四次戰役](抗美援朝第四次战役.md "wikilink")
|list4=**[汉江南岸](漢江南岸防禦戰.md "wikilink")**{{·w}}{{·w}}[圍捕](圍捕作戰.md "wikilink"){{·w}}
**[橫城](橫城反擊戰.md "wikilink")**{{·w}}**[砥平里](砥平里戰鬥.md "wikilink")**{{·w}}[第三次原州](第三次原州戰役.md "wikilink"){{·w}}[屠夫](屠夫行動.md "wikilink"){{·w}}[撕裂者](撕裂者行動.md "wikilink"){{·w}}**[第四次汉城](第四次汉城战役.md "wikilink")**
|group5=[第五次戰役](抗美援朝第五次战役.md "wikilink")
|list5=****{{·w}}[加平](加平戰鬥.md "wikilink"){{·w}}[美羅洞](美羅洞戰役.md "wikilink"){{·w}}**[縣里](縣里戰役.md "wikilink")**{{·w}}**[铁原](鐵原戰役.md "wikilink")**{{·w}}**[朝-{}-鲜停战谈判](朝鲜停战谈判.md "wikilink")**
}}

|group4=僵　　持 |list4=
{{·w}}**[文登里](文登里戰役.md "wikilink")**{{·w}}{{·w}}[馬良山](馬良山戰役.md "wikilink"){{·w}}{{·w}}[智陵洞](智陵洞战斗.md "wikilink")
|group2=1952年
|list2=[1952年春夏季](朝鲜战争1952年春夏季战役.md "wikilink"){{·w}}{{·w}}[1952年平壤－江西地震](1952年平壤－江西地震.md "wikilink"){{·w}}{{·w}}[第一次上浦防东山](上浦防东山战役.md "wikilink"){{·w}}[哈德森港](哈德森港行動.md "wikilink"){{·w}}[巨济岛事件](巨济岛事件.md "wikilink"){{·w}}[1952年秋季](朝鲜战争1952年秋季战役.md "wikilink")（[高阳岱西山](高阳岱西山戰鬥.md "wikilink"){{·w}}[白馬山](白馬山戰役.md "wikilink")）{{·w}}**[上甘嶺](上甘岭战役.md "wikilink")**{{·w}}[杰克逊高地](杰克逊高地戰役.md "wikilink")
|group3=1953年
|list3=[丁字山](丁字山戰鬥.md "wikilink"){{·w}}[第二次上浦防东山](上浦防東山戰鬥.md "wikilink"){{·w}}[马踏里西山](马踏里西山战斗.md "wikilink"){{·w}}[381东北无名高地（哈里哨）](381东北无名高地战斗.md "wikilink"){{·w}}[第一次猪排山](石峴洞北山戰役.md "wikilink"){{·w}}[坪村南山](坪村南山戰鬥.md "wikilink"){{·w}}[石峴洞北山（豬排山）](石峴洞北山戰鬥.md "wikilink"){{·w}}[1953年夏季](朝鲜战争1953年夏季战役.md "wikilink")（[马踏里西山—梅砚里东南山](马踏里西山—梅砚里东南山战斗.md "wikilink"){{·w}}[马踏里东山](马踏里东山战斗.md "wikilink"){{·w}}[座首洞南山](座首洞南山戰鬥.md "wikilink"){{·w}}[第二次猪排山](石峴洞北山戰鬥.md "wikilink"){{·w}}{{·w}}[金城](金城戰役.md "wikilink")）{{·w}}**[朝-{}-鲜停战协定](朝鲜停战协定.md "wikilink")**{{·w}}[**战俘遣返**](朝鲜战争战俘遣返问题.md "wikilink")
}}

|group5=战争罪行 |list5=
[信川郡大屠杀](信川郡大屠杀.md "wikilink"){{·w}}[云川里屠杀](云川里屠杀.md "wikilink"){{·w}}[朝鲜战争细菌战](朝鲜战争细菌战.md "wikilink")
|group2=大韓民國國軍
|list2=[濟州四·三事件](濟州四·三事件.md "wikilink"){{·w}}[漢江大橋爆破事件](漢江大橋爆破事件.md "wikilink"){{·w}}[保導聯盟事件](保導聯盟事件.md "wikilink"){{·w}}[國民防衛軍事件](國民防衛軍事件.md "wikilink"){{·w}}[金井窟屠杀事件](金井窟屠杀事件.md "wikilink"){{·w}}[山清·咸陽屠殺事件](山清·咸陽屠殺事件.md "wikilink"){{·w}}[居昌屠杀事件](居昌屠杀事件.md "wikilink"){{·w}}[江華島屠殺事件](江華島屠殺事件.md "wikilink"){{·w}}[南杨州屠杀事件](南杨州屠杀事件.md "wikilink")
|group3=朝鮮人民軍
|list3=[漢城國立大學附屬醫院屠殺事件](漢城國立大學附屬醫院屠殺事件.md "wikilink"){{·w}}[303高地屠杀](303高地屠杀.md "wikilink")
}}

|group6=后　　续
|list6=[普韋布洛號通用環境研究艦](普韋布洛號通用環境研究艦.md "wikilink"){{·w}}[EC-121击落事件](EC-121击落事件.md "wikilink"){{·w}}[板门店事件](板门店事件.md "wikilink"){{·w}}[仰光爆炸事件](仰光爆炸事件.md "wikilink"){{·w}}[青瓦台事件](青瓦台事件.md "wikilink"){{·w}}[實尾島事件](實尾島事件.md "wikilink"){{·w}}[文世光事件](文世光事件.md "wikilink"){{·w}}[大韓航空858號班機空難](大韓航空858號班機空難.md "wikilink"){{·w}}[麗水](麗水海戰.md "wikilink"){{·w}}[江陵潜艇渗透事件](江陵潜艇渗透事件.md "wikilink"){{·w}}[能登半島沿海不明船事件](能登半島沿海不明船事件.md "wikilink"){{·w}}[第一次延坪海戰](第一次延坪海戰.md "wikilink"){{·w}}[奄美大岛海战](奄美大岛海战.md "wikilink"){{·w}}[第二次延坪海戰](第二次延坪海戰.md "wikilink"){{·w}}[大青島海戰](大青島海戰.md "wikilink"){{·w}}[天安号沉没事件](天安号沉没事件.md "wikilink"){{·w}}[2010年韩朝边境冲突](2010年韩朝边境冲突.md "wikilink"){{·w}}[延坪岛炮击事件](延坪岛炮击事件.md "wikilink"){{·w}}[韓國士兵誤向客機開火事件](韓國士兵誤向客機開火事件.md "wikilink"){{·w}}[2013年朝鮮半島危機](2013年朝鮮半島危機.md "wikilink"){{·w}}[2017-18年朝鲜危机](2017-18年朝鲜危机.md "wikilink"){{·w}}[2018年4月朝韓首腦會晤](2018年4月朝韓首腦會晤.md "wikilink"){{·w}}[2018年5月朝韓首腦會晤](2018年5月朝韓首腦會晤.md "wikilink"){{·w}}[2018年朝美首脑会议](2018年朝美首脑会议.md "wikilink"){{·w}}[2018年9月南北韓首腦會談](2018年9月南北韓首腦會談.md "wikilink"){{·w}}[2019年朝美首腦會談](2019年朝美首腦會談.md "wikilink")

|below = 各阶段主要交战及事件**加粗**显示 }}<noinclude>

</noinclude>

[\*](../Category/朝鲜战争.md "wikilink")
[Category:亚洲战争模板](../Category/亚洲战争模板.md "wikilink")
[Category:朝鲜半岛历史模板](../Category/朝鲜半岛历史模板.md "wikilink")