|group2 = [叠彩区](叠彩区.md "wikilink") |list2 =
[北门街道](北门街道_\(桂林市\).md "wikilink") |group2 = 1乡
|list2 = [大河乡](大河乡_\(桂林市\).md "wikilink") }}

|group3 = [象山区](象山区.md "wikilink") |list3 =
[象山街道](象山街道_\(桂林市\).md "wikilink"){{.w}}[平山街道](平山街道_\(桂林市\).md "wikilink")
|group2 = 1乡 |list2 = [二塘乡](二塘乡_\(桂林市\).md "wikilink") }}

|group4 = [七星区](七星区.md "wikilink") |list4 =
[穿山街道](穿山街道.md "wikilink"){{.w}}[漓东街道](漓东街道.md "wikilink")
|group2 = 1乡 |list2 = [朝阳乡](朝阳乡_\(桂林市\).md "wikilink") }} }}

|group5 = [雁山区](雁山区.md "wikilink") |list5 = [柘木镇](柘木镇.md "wikilink")
|group3 = 1乡 |list3 = [大埠乡](大埠乡_\(桂林市\).md "wikilink") |group4 = 1民族乡
|list4 = [草坪回族乡](草坪回族乡.md "wikilink") }}

|group6 = [临桂区](临桂区.md "wikilink") |list6 =
[六塘镇](六塘镇_\(桂林市\).md "wikilink"){{.w}}[会仙镇](会仙镇.md "wikilink"){{.w}}[两江镇](两江镇_\(桂林市\).md "wikilink"){{.w}}[五通镇](五通镇_\(桂林市\).md "wikilink"){{.w}}[四塘镇](四塘镇_\(桂林市\).md "wikilink"){{.w}}[南边山镇](南边山镇.md "wikilink"){{.w}}[中庸镇](中庸镇.md "wikilink")
|group2 = 1乡 |list2 = [茶洞乡](茶洞乡.md "wikilink") |group3 = 2民族乡 |list3 =
[宛田瑶族乡](宛田瑶族乡.md "wikilink"){{.w}}[黄沙瑶族乡](黄沙瑶族乡.md "wikilink") }} }}

|group3 = [县级市](县级市.md "wikilink") |list3 =
[新坪镇](新坪镇.md "wikilink"){{.w}}[杜莫镇](杜莫镇.md "wikilink"){{.w}}[青山镇](青山镇_\(荔浦县\).md "wikilink"){{.w}}[修仁镇](修仁镇.md "wikilink"){{.w}}[大塘镇](大塘镇_\(荔浦县\).md "wikilink"){{.w}}[花箦镇](花箦镇.md "wikilink"){{.w}}[双江镇](双江镇_\(荔浦县\).md "wikilink"){{.w}}[马岭镇](马岭镇_\(荔浦县\).md "wikilink")
|group2 = 2乡 |list2 =
[龙怀乡](龙怀乡.md "wikilink"){{.w}}[茶城乡](茶城乡.md "wikilink")
|group3 = 1民族乡 |list3 = [蒲芦瑶族乡](蒲芦瑶族乡.md "wikilink") }} }}

|group4 = [县](县_\(中华人民共和国\).md "wikilink") |list4 =
[福利镇](福利镇_\(阳朔县\).md "wikilink"){{.w}}[兴坪镇](兴坪镇.md "wikilink"){{.w}}[葡萄镇](葡萄镇.md "wikilink"){{.w}}[高田镇](高田镇_\(阳朔县\).md "wikilink")
|group2 = 3乡 |list2 =
[金宝乡](金宝乡.md "wikilink"){{.w}}[普益乡](普益乡.md "wikilink"){{.w}}[杨堤乡](杨堤乡.md "wikilink")
}}

|group2 = [灵川县](灵川县.md "wikilink") |list2 =
[大圩镇](大圩镇_\(灵川县\).md "wikilink"){{.w}}[定江镇](定江镇.md "wikilink"){{.w}}[三街镇](三街镇_\(灵川县\).md "wikilink"){{.w}}[潭下镇](潭下镇_\(灵川县\).md "wikilink"){{.w}}[九屋镇](九屋镇.md "wikilink"){{.w}}[灵田镇](灵田镇.md "wikilink")
|group2 = 3乡 |list2 =
[潮田乡](潮田乡.md "wikilink"){{.w}}[海洋乡](海洋乡_\(灵川县\).md "wikilink"){{.w}}[公平乡](公平乡.md "wikilink")
|group3 = 2民族乡 |list3 =
[大境瑶族乡](大境瑶族乡.md "wikilink"){{.w}}[兰田瑶族乡](兰田瑶族乡.md "wikilink")
}}

|group3 = [全州县](全州县.md "wikilink") |list3 =
[黄沙河镇](黄沙河镇.md "wikilink"){{.w}}[庙头镇](庙头镇_\(全州县\).md "wikilink"){{.w}}[文桥镇](文桥镇_\(全州县\).md "wikilink"){{.w}}[大西江镇](大西江镇.md "wikilink"){{.w}}[龙水镇](龙水镇_\(全州县\).md "wikilink"){{.w}}[才湾镇](才湾镇.md "wikilink"){{.w}}[绍水镇](绍水镇.md "wikilink"){{.w}}[石塘镇](石塘镇_\(全州县\).md "wikilink"){{.w}}[安和镇](安和镇.md "wikilink"){{.w}}[两河镇](两河镇_\(全州县\).md "wikilink"){{.w}}[凤凰镇](凤凰镇_\(全州县\).md "wikilink"){{.w}}[咸水镇](咸水镇.md "wikilink"){{.w}}[枧塘镇](枧塘镇.md "wikilink")
|group2 = 2乡 |list2 =
[永岁乡](永岁乡.md "wikilink"){{.w}}[白宝乡](白宝乡.md "wikilink")
|group3 = 2民族乡 |list3 =
[蕉江瑶族乡](蕉江瑶族乡.md "wikilink"){{.w}}[东山瑶族乡](东山瑶族乡.md "wikilink")
}}

|group4 = [兴安县](兴安县.md "wikilink") |list4 =
[湘漓镇](湘漓镇.md "wikilink"){{.w}}[界首镇](界首镇_\(兴安县\).md "wikilink"){{.w}}[高尚镇](高尚镇.md "wikilink"){{.w}}[严关镇](严关镇.md "wikilink"){{.w}}[溶江镇](溶江镇.md "wikilink")
|group2 = 3乡 |list2 =
[漠川乡](漠川乡.md "wikilink"){{.w}}[白石乡](白石乡_\(兴安县\).md "wikilink"){{.w}}[崔家乡](崔家乡.md "wikilink")
|group3 = 1民族乡 |list3 = [华江瑶族乡](华江瑶族乡.md "wikilink") }}

|group5 = [永福县](永福县.md "wikilink") |list5 =
[罗锦镇](罗锦镇.md "wikilink"){{.w}}[百寿镇](百寿镇.md "wikilink"){{.w}}[苏桥镇](苏桥镇_\(永福县\).md "wikilink"){{.w}}[三皇镇](三皇镇.md "wikilink"){{.w}}[堡里镇](堡里镇.md "wikilink")
|group2 = 3乡 |list2 =
[广福乡](广福乡.md "wikilink"){{.w}}[永安乡](永安乡_\(永福县\).md "wikilink"){{.w}}[龙江乡](龙江乡_\(永福县\).md "wikilink")
}}

|group6 = [灌阳县](灌阳县.md "wikilink") |list6 =
[黄关镇](黄关镇.md "wikilink"){{.w}}[文市镇](文市镇.md "wikilink"){{.w}}[新街镇](新街镇_\(灌阳县\).md "wikilink"){{.w}}[新圩镇](新圩镇_\(灌阳县\).md "wikilink")
|group2 = 2乡 |list2 =
[观音阁乡](观音阁乡.md "wikilink"){{.w}}[水车乡](水车乡.md "wikilink")
|group3 = 2民族乡 |list3 =
[洞井瑶族乡](洞井瑶族乡.md "wikilink"){{.w}}[西山瑶族乡](西山瑶族乡_\(灌阳县\).md "wikilink")
}}

|group7 = [资源县](资源县.md "wikilink") |list7 =
[梅溪镇](梅溪镇_\(资源县\).md "wikilink"){{.w}}[中峰镇](中峰镇_\(资源县\).md "wikilink")
|group2 = 1乡 |list2 = [瓜里乡](瓜里乡.md "wikilink") |group3 = 3民族乡 |list3 =
[车田苗族乡](车田苗族乡.md "wikilink"){{.w}}[两水苗族乡](两水苗族乡.md "wikilink"){{.w}}[河口瑶族乡](河口瑶族乡.md "wikilink")
}}

|group8 = [平乐县](平乐县.md "wikilink") |list8 =
[二塘镇](二塘镇_\(平乐县\).md "wikilink"){{.w}}[沙子镇](沙子镇_\(平乐县\).md "wikilink"){{.w}}[同安镇](同安镇_\(平乐县\).md "wikilink"){{.w}}[张家镇](张家镇_\(平乐县\).md "wikilink"){{.w}}[源头镇](源头镇.md "wikilink")
|group2 = 3乡 |list2 =
[阳安乡](阳安乡.md "wikilink"){{.w}}[青龙乡](青龙乡_\(平乐县\).md "wikilink"){{.w}}[桥亭乡](桥亭乡_\(平乐县\).md "wikilink")
|group3 = 1民族乡 |list3 = [大发瑶族乡](大发瑶族乡.md "wikilink") }} }}

|group5 = [自治县](自治县.md "wikilink") |list5 =
[三门镇](三门镇_\(龙胜县\).md "wikilink"){{.w}}[平等镇](平等镇.md "wikilink"){{.w}}[龙脊镇](龙脊镇.md "wikilink")
|group2 = 5乡 |list2 =
[泗水乡](泗水乡.md "wikilink"){{.w}}[江底乡](江底乡_\(龙胜县\).md "wikilink"){{.w}}[马堤乡](马堤乡.md "wikilink"){{.w}}[伟江乡](伟江乡.md "wikilink"){{.w}}[乐江乡](乐江乡.md "wikilink")
}}

|group2 = [恭城瑶族自治县](恭城瑶族自治县.md "wikilink") |list2 =
[栗木镇](栗木镇.md "wikilink"){{.w}}[莲花镇](莲花镇_\(恭城县\).md "wikilink"){{.w}}[嘉会镇](嘉会镇.md "wikilink"){{.w}}[西岭镇](西岭镇_\(恭城县\).md "wikilink")
|group2 = 4乡 |list2 =
[平安乡](平安乡_\(恭城县\).md "wikilink"){{.w}}[三江乡](三江乡_\(恭城县\).md "wikilink"){{.w}}[观音乡](观音乡_\(恭城县\).md "wikilink"){{.w}}[龙虎乡](龙虎乡_\(恭城县\).md "wikilink")
}} }} }} <noinclude> </noinclude>

[\*](../Category/桂林行政区划.md "wikilink")
[Category:广西行政区划模板](../Category/广西行政区划模板.md "wikilink")
[Category:中华人民共和国各地级行政区行政区划模板](../Category/中华人民共和国各地级行政区行政区划模板.md "wikilink")