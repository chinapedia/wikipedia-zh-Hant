[东北官话](../Page/东北官话.md "wikilink"){{.w}}[胶辽官话](../Page/胶辽官话.md "wikilink"){{.w}}[冀鲁官话](../Page/冀鲁官话.md "wikilink"){{.w}}[中原官话](../Page/中原官话.md "wikilink"){{.w}}[兰银官话](../Page/兰银官话.md "wikilink"){{.w}}[江淮官话](../Page/江淮官话.md "wikilink"){{.w}}[西南官话](../Page/西南官话.md "wikilink"){{.w}}[桂柳官话](../Page/桂柳官话.md "wikilink"){{.w}}[岷江话](../Page/岷江话.md "wikilink"){{.w}}[天津话](../Page/天津话.md "wikilink"){{.w}}[贛榆话](../Page/贛榆话.md "wikilink"){{.w}}[南通话](../Page/南通话.md "wikilink"){{.w}}[漢中話](../Page/漢中話.md "wikilink")}}

`|group2 = `[`吴语`](../Page/吴语.md "wikilink")
`|list2 = `[`吳語方言`](../Page/吳語方言.md "wikilink")`{{.w}}`[`太湖片`](../Page/太湖片.md "wikilink")`{{.w}}`[`台州话`](../Page/台州话.md "wikilink")`{{.w}}`[`温州话`](../Page/温州话.md "wikilink")`{{.w}}`[`金衢片`](../Page/金衢片.md "wikilink")`{{.w}}`[`上麗片`](../Page/上麗片.md "wikilink")`{{.w}}`[`宣州片`](../Page/宣州片.md "wikilink")
`|group3 = `[`闽语`](../Page/闽语.md "wikilink")
`|list3 = `[`臺灣話`](../Page/臺灣話.md "wikilink")`{{.w}}`[`潮州话`](../Page/潮州话.md "wikilink")`{{.w}}`[`浙南閩語`](../Page/浙南閩語.md "wikilink")`{{.w}}`[`前路话`](../Page/前路话.md "wikilink")`{{.w}}`[`中山閩語`](../Page/中山閩語.md "wikilink")
`|group2 = `[`閩東語`](../Page/閩東語.md "wikilink")
`|list2 = `[`侯官片`](../Page/侯官片.md "wikilink")`{{.w}}`[`福宁片`](../Page/福宁片.md "wikilink")`{{.w}}`[`蛮讲`](../Page/蛮讲.md "wikilink")
`|group3 = `[`閩北語`](../Page/閩北語.md "wikilink")
`|list3 = `[`松溪话`](../Page/松溪话.md "wikilink")`{{.w}}`[`建阳话`](../Page/建阳话.md "wikilink")
`|group4 = `[`閩中語`](../Page/閩中語.md "wikilink")
`|list4 = `[`三明話`](../Page/三明話.md "wikilink")`{{.w}}`[`永安話`](../Page/永安話.md "wikilink")`{{.w}}`[`沙縣話`](../Page/沙縣話.md "wikilink")
`|group5 = `[`莆仙語`](../Page/莆仙語.md "wikilink")
`|list5 = `[`莆田話`](../Page/莆田話.md "wikilink")`{{.w}}`[`仙遊話`](../Page/仙遊話.md "wikilink")`{{.w}}`[`烏坵話`](../Page/烏坵話.md "wikilink")
`|group6 = `[`邵将语`](../Page/邵将语.md "wikilink")
`|list6 = `[`邵武話`](../Page/邵武話.md "wikilink")`{{.w}}`[`將樂話`](../Page/將樂話.md "wikilink")
`|group7 = `[`瓊雷語`](../Page/瓊雷語.md "wikilink")
`|list7 = `[`雷州話`](../Page/雷州話.md "wikilink")`{{.w}}`[`海南話`](../Page/海南話.md "wikilink")` }}`
`|group4 = `[`粤语`](../Page/粤语.md "wikilink")
`|list4 = `[`粵語方言`](../Page/粵語方言.md "wikilink")`{{.w}}`[`广州话`](../Page/广州话.md "wikilink")`{{.w}}`[`香港粵語`](../Page/香港粵語.md "wikilink")`{{.w}}`[`莞寶方言`](../Page/莞寶方言.md "wikilink")`{{.w}}`[`四邑方言`](../Page/四邑方言.md "wikilink")`{{.w}}`[`勾漏片粤语`](../Page/勾漏片粤语.md "wikilink")`{{·w}}`[`东江本地话`](../Page/水源音.md "wikilink")
`|group5 = `[`客家语`](../Page/客家语.md "wikilink")
`|list5 = `[`客語方言`](../Page/客語方言.md "wikilink")`{{·w}}`[`梅縣話`](../Page/梅縣話.md "wikilink")`{{·w}}`[`畲话`](../Page/畲话.md "wikilink")
`|group6 = 其他分支`
`|list6 =`[`贛語`](../Page/贛語.md "wikilink")`{{·w}}`[`湘语`](../Page/湘语.md "wikilink")`{{·w}}`[`晋语`](../Page/晋语.md "wikilink")`{{·w}}`[`徽语`](../Page/徽语.md "wikilink")`{{·w}} 平話土話（`[`湘南土话`](../Page/湘南土话.md "wikilink")`{{·w}}`[`粤北土话`](../Page/粤北土话.md "wikilink")`{{·w}}`[`廣西平話`](../Page/廣西平話.md "wikilink")`）{{·w}}`[`瓦乡话`](../Page/瓦乡话.md "wikilink")`{{·w}}`[`迈话`](../Page/迈话.md "wikilink")`{{·w}}`[`军家话`](../Page/军家话.md "wikilink")`（`[`海南军话`](../Page/海南军话.md "wikilink")`）{{·w}}`[`富馬話`](../Page/富馬話.md "wikilink")`}}`

}} |group2 = [書寫系統](../Page/中文書面語.md "wikilink") |state2 = uncollapsed
|abbr2 = 書寫 |list2 = [方言字](../Page/方言字.md "wikilink")）</small>

`|group2 = 文體`
`|list2 = `[`中州韻白話文`](../Page/中州韻白話文.md "wikilink")`（韻白）{{·w}} `[`吳語白話文`](../Page/吳語白話文.md "wikilink")`（蘇白） {{·w}}`[`粵語白話文`](../Page/粵語白話文.md "wikilink")`（廣白）{{·w}}`[`臺語白話文`](../Page/臺語白話文.md "wikilink")`{{·w}}`[`客家語白話文`](../Page/客家語白話文.md "wikilink")

}}}}

`|group2 = `[`表音文字`](../Page/表音文字.md "wikilink")
`|list2 = `[`西里尔字母`](../Page/汉语西里尔字母转写系统.md "wikilink")`{{·w}}`[`阿拉伯字母`](../Page/阿拉伯字母.md "wikilink")
`|group2 = 文體 `
`|list2 = `[`教會羅馬字`](../Page/教會羅馬字.md "wikilink")`{{·w}}`[`東干文`](../Page/东干语.md "wikilink")`{{·w}}`[`小儿经`](../Page/小儿经.md "wikilink")

}}

`|group3 = 半音節文字`
`|list3 = `[`盲文/點字`](../Page/盲文.md "wikilink")`{{·w}}`[`三推成字法`](../Page/三推成字法.md "wikilink")`{{·w}}`[`中国切音新字`](../Page/中国切音新字.md "wikilink")`{{·w}}`[`臺灣語假名`](../Page/臺灣語假名.md "wikilink")`{{·w}}`[`臺語諺文`](../Page/臺語諺文.md "wikilink")
`|group4 = 其他`
`|list4 = `[`女书`](../Page/女书.md "wikilink")`{{·w}}`[`漢羅台文`](../Page/漢羅台文.md "wikilink")

}} }}

|group3 = [語言研究](../Page/语文学.md "wikilink") |state3 = uncollapsed |abbr3
= 學術 |list3 =
[原始汉语](../Page/原始汉语.md "wikilink"){{.w}}[古代汉语](../Page/古代汉语.md "wikilink")（[上古](../Page/上古汉语.md "wikilink"){{·w}}
[中古](../Page/中古汉语.md "wikilink"){{·w}}
[近代](../Page/近代汉语.md "wikilink")）{{·w}}[老國音](../Page/老國音.md "wikilink"){{·w}}[現代標準漢語](../Page/現代標準漢語.md "wikilink")
|group2 = [汉语音韵学](../Page/汉语音韵学.md "wikilink") |list2 =
[先秦](../Page/先秦音系.md "wikilink"){{·w}}
[漢代](../Page/漢代音系.md "wikilink"){{·w}}
[晉代](../Page/晉代音系.md "wikilink"){{·w}}
[隋唐](../Page/隋唐音系.md "wikilink"){{·w}}
[五代](../Page/五代音系.md "wikilink"){{·w}}
[宋代](../Page/宋代音系.md "wikilink"){{·w}}
[元代](../Page/元代音系.md "wikilink"){{·w}}
[明初](../Page/明初官话音系.md "wikilink"){{·w}}
[明清](../Page/明清音系.md "wikilink"){{·w}}
[現代](../Page/現代標準漢語音系.md "wikilink")

|group3 = 語言研究 |list3 = [文字](../Page/文字学.md "wikilink"){{·w}}
[詞彙](../Page/汉语语法.md "wikilink"){{·w}}
[語法](../Page/汉语语法.md "wikilink"){{·w}}
[訓詁](../Page/训诂学.md "wikilink"){{·w}}
[方言](../Page/汉语方言学.md "wikilink") }}

|group4 = 管理机构 |state4 = uncollapsed |abbr4 = 管理 |list4 =

  - [教育部](../Page/中华人民共和国教育部.md "wikilink")[国家语言文字工作委员会](../Page/国家语言文字工作委员会.md "wikilink")

  - [語文教育及研究常務委員會](../Page/語文教育及研究常務委員會.md "wikilink")

  - [教育部](../Page/中華民國教育部.md "wikilink")[終身教育司第四科](../Page/教育部國語推行委員會.md "wikilink")

  - [推广华语理事会](../Page/新加坡推廣華語理事會.md "wikilink")

  - [华语规范理事会](../Page/馬來西亞華語規範理事會.md "wikilink")

|below = [汉语方言列表](../Page/汉语方言列表.md "wikilink") }}<noinclude>

</noinclude>

[\*](../Category/汉语.md "wikilink")
[汉语导航模板](../Category/汉语导航模板.md "wikilink")