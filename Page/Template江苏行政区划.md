[建邺区](../Page/建邺区.md "wikilink"){{.w}}[鼓楼区](../Page/鼓楼区_\(南京市\).md "wikilink"){{.w}}[浦口区](../Page/浦口区.md "wikilink"){{.w}}[栖霞区](../Page/栖霞区.md "wikilink"){{.w}}[雨花台区](../Page/雨花台区.md "wikilink"){{.w}}[江宁区](../Page/江宁区_\(南京市\).md "wikilink"){{.w}}[六合区](../Page/六合区.md "wikilink"){{.w}}[溧水区](../Page/溧水区.md "wikilink"){{.w}}[高淳区](../Page/高淳区.md "wikilink")
}}

|group3style =text-align: center; |group3 =
[地级市](../Page/地级市.md "wikilink") |list3 =
[锡山区](../Page/锡山区.md "wikilink"){{.w}}[惠山区](../Page/惠山区.md "wikilink"){{.w}}[滨湖区](../Page/滨湖区.md "wikilink"){{.w}}[新吴区](../Page/新吴区.md "wikilink"){{.w}}[江阴市](../Page/江阴市.md "wikilink"){{.w}}[宜兴市](../Page/宜兴市.md "wikilink")

|group4 = [徐州市](../Page/徐州市.md "wikilink") |list4 =
[云龙区](../Page/云龙区.md "wikilink"){{.w}}[鼓楼区](../Page/鼓楼区_\(徐州市\).md "wikilink"){{.w}}[贾汪区](../Page/贾汪区.md "wikilink"){{.w}}[泉山区](../Page/泉山区.md "wikilink"){{.w}}[铜山区](../Page/铜山区.md "wikilink"){{.w}}[新沂市](../Page/新沂市.md "wikilink"){{.w}}[邳州市](../Page/邳州市.md "wikilink"){{.w}}[丰县](../Page/丰县.md "wikilink"){{.w}}[沛县](../Page/沛县.md "wikilink"){{.w}}[睢宁县](../Page/睢宁县.md "wikilink")

|group5 = [常州市](../Page/常州市.md "wikilink") |list5 =
[新北区](../Page/新北區_\(常州市\).md "wikilink"){{.w}}[天宁区](../Page/天宁区.md "wikilink"){{.w}}[钟楼区](../Page/钟楼区.md "wikilink"){{.w}}[武进区](../Page/武进区.md "wikilink"){{.w}}[金坛区](../Page/金坛区.md "wikilink"){{.w}}[溧阳市](../Page/溧阳市.md "wikilink")

|group6 = [苏州市](../Page/苏州市.md "wikilink") |list6 =
[姑苏区](../Page/姑苏区.md "wikilink"){{.w}}[虎丘区](../Page/虎丘区.md "wikilink"){{.w}}[吴中区](../Page/吴中区.md "wikilink"){{.w}}[相城区](../Page/相城区.md "wikilink"){{.w}}[吴江区](../Page/吴江区.md "wikilink"){{.w}}[常熟市](../Page/常熟市.md "wikilink"){{.w}}[张家港市](../Page/张家港市.md "wikilink"){{.w}}[昆山市](../Page/昆山市.md "wikilink"){{.w}}[太仓市](../Page/太仓市.md "wikilink")

|group7 = [南通市](../Page/南通市.md "wikilink") |list7 =
[崇川区](../Page/崇川区.md "wikilink"){{.w}}[港闸区](../Page/港闸区.md "wikilink"){{.w}}[通州区](../Page/通州区_\(南通市\).md "wikilink"){{.w}}[启东市](../Page/启东市.md "wikilink"){{.w}}[如皋市](../Page/如皋市.md "wikilink"){{.w}}[海门市](../Page/海门市.md "wikilink"){{.w}}[海安市](../Page/海安市.md "wikilink"){{.w}}[如东县](../Page/如东县.md "wikilink")

|group8 = [连云港市](../Page/连云港市.md "wikilink") |list8 =
[海州区](../Page/海州区_\(连云港市\).md "wikilink"){{.w}}[连云区](../Page/连云区.md "wikilink"){{.w}}[赣榆区](../Page/赣榆区.md "wikilink"){{.w}}[东海县](../Page/东海县.md "wikilink"){{.w}}[灌云县](../Page/灌云县.md "wikilink"){{.w}}[灌南县](../Page/灌南县.md "wikilink")

|group9 = [淮安市](../Page/淮安市.md "wikilink") |list9 =
[淮安区](../Page/淮安区.md "wikilink"){{.w}}[淮阴区](../Page/淮阴区.md "wikilink"){{.w}}[清江浦区](../Page/清江浦区.md "wikilink"){{.w}}[洪泽区](../Page/洪泽区.md "wikilink"){{.w}}[涟水县](../Page/涟水县.md "wikilink"){{.w}}[盱眙县](../Page/盱眙县.md "wikilink"){{.w}}[金湖县](../Page/金湖县.md "wikilink")

|group10 = [盐城市](../Page/盐城市.md "wikilink") |list10 =
[亭湖区](../Page/亭湖区.md "wikilink"){{.w}}[盐都区](../Page/盐都区.md "wikilink"){{.w}}[大丰区](../Page/大丰区.md "wikilink"){{.w}}[东台市](../Page/东台市.md "wikilink"){{.w}}[响水县](../Page/响水县.md "wikilink"){{.w}}[滨海县](../Page/滨海县.md "wikilink"){{.w}}[阜宁县](../Page/阜宁县.md "wikilink"){{.w}}[射阳县](../Page/射阳县.md "wikilink"){{.w}}[建湖县](../Page/建湖县.md "wikilink")

|group11 = [扬州市](../Page/扬州市.md "wikilink") |list11 =
[邗江区](../Page/邗江区.md "wikilink"){{.w}}[广陵区](../Page/广陵区.md "wikilink"){{.w}}[江都区](../Page/江都区.md "wikilink"){{.w}}[仪征市](../Page/仪征市.md "wikilink"){{.w}}[高邮市](../Page/高邮市.md "wikilink"){{.w}}[宝应县](../Page/宝应县.md "wikilink")

|group12 = [镇江市](../Page/镇江市.md "wikilink") |list12 =
[京口区](../Page/京口区.md "wikilink"){{.w}}[润州区](../Page/润州区.md "wikilink"){{.w}}[丹徒区](../Page/丹徒区.md "wikilink"){{.w}}[丹阳市](../Page/丹阳市.md "wikilink"){{.w}}[扬中市](../Page/扬中市.md "wikilink"){{.w}}[句容市](../Page/句容市.md "wikilink")

|group13 = [泰州市](../Page/泰州市.md "wikilink") |list13 =
[海陵区](../Page/海陵区.md "wikilink"){{.w}}[高港区](../Page/高港区.md "wikilink"){{.w}}[姜堰区](../Page/姜堰区.md "wikilink"){{.w}}[兴化市](../Page/兴化市.md "wikilink"){{.w}}[靖江市](../Page/靖江市.md "wikilink"){{.w}}[泰兴市](../Page/泰兴市.md "wikilink")

|group14 = [宿迁市](../Page/宿迁市.md "wikilink") |list14 =
[宿豫区](../Page/宿豫区.md "wikilink"){{.w}}[宿城区](../Page/宿城区.md "wikilink"){{.w}}[沭阳县](../Page/沭阳县.md "wikilink"){{.w}}[泗阳县](../Page/泗阳县.md "wikilink"){{.w}}[泗洪县](../Page/泗洪县.md "wikilink")
}}

|belowstyle = text-align: left; font-size: 80%; |below =
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[江苏省乡级以上行政区列表](../Page/江苏省乡级以上行政区列表.md "wikilink")。
}}<noinclude>

</noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[\*](../Category/江苏行政区划.md "wikilink")
[Category:江苏行政区划导航模板](../Category/江苏行政区划导航模板.md "wikilink")
[Category:中华人民共和国江苏省行政区划模板](../Category/中华人民共和国江苏省行政区划模板.md "wikilink")