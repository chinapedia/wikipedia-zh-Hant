[管城回族区](../Page/管城回族区.md "wikilink"){{.w}}[金水区](../Page/金水区.md "wikilink"){{.w}}[上街区](../Page/上街区.md "wikilink"){{.w}}[惠济区](../Page/惠济区.md "wikilink"){{.w}}[巩义市](../Page/巩义市.md "wikilink"){{.w}}[荥阳市](../Page/荥阳市.md "wikilink"){{.w}}[新密市](../Page/新密市.md "wikilink"){{.w}}[新郑市](../Page/新郑市.md "wikilink"){{.w}}[登封市](../Page/登封市.md "wikilink"){{.w}}[中牟县](../Page/中牟县.md "wikilink")

|group3 = [开封市](../Page/开封市.md "wikilink") |list3 =
[鼓楼区](../Page/鼓楼区_\(开封市\).md "wikilink"){{.w}}[龙亭区](../Page/龙亭区.md "wikilink"){{.w}}[顺河回族区](../Page/顺河回族区.md "wikilink"){{.w}}[禹王台区](../Page/禹王台区.md "wikilink"){{.w}}[祥符区](../Page/祥符区.md "wikilink"){{.w}}[杞县](../Page/杞县.md "wikilink"){{.w}}[通许县](../Page/通许县.md "wikilink"){{.w}}[尉氏县](../Page/尉氏县.md "wikilink"){{.w}}[兰考县](../Page/兰考县.md "wikilink")

|group4 = [洛阳市](../Page/洛阳市.md "wikilink") |list4 =
[洛龙区](../Page/洛龙区.md "wikilink"){{.w}}[老城区](../Page/老城区.md "wikilink"){{.w}}[西工区](../Page/西工区.md "wikilink"){{.w}}[瀍河回族区](../Page/瀍河回族区.md "wikilink"){{.w}}[涧西区](../Page/涧西区.md "wikilink"){{.w}}[吉利区](../Page/吉利区.md "wikilink"){{.w}}[偃师市](../Page/偃师市.md "wikilink"){{.w}}[孟津县](../Page/孟津县.md "wikilink"){{.w}}[新安县](../Page/新安县.md "wikilink"){{.w}}[栾川县](../Page/栾川县.md "wikilink"){{.w}}[嵩县](../Page/嵩县.md "wikilink"){{.w}}[汝阳县](../Page/汝阳县.md "wikilink"){{.w}}[宜阳县](../Page/宜阳县.md "wikilink"){{.w}}[洛宁县](../Page/洛宁县.md "wikilink"){{.w}}[伊川县](../Page/伊川县.md "wikilink")

|group5 = [平顶山市](../Page/平顶山市.md "wikilink") |list5 =
[新华区](../Page/新华区_\(平顶山市\).md "wikilink"){{.w}}[卫东区](../Page/卫东区.md "wikilink"){{.w}}[石龙区](../Page/石龙区.md "wikilink"){{.w}}[湛河区](../Page/湛河区.md "wikilink"){{.w}}[舞钢市](../Page/舞钢市.md "wikilink"){{.w}}[汝州市](../Page/汝州市.md "wikilink"){{.w}}[宝丰县](../Page/宝丰县.md "wikilink"){{.w}}[叶县](../Page/叶县.md "wikilink"){{.w}}[鲁山县](../Page/鲁山县.md "wikilink"){{.w}}[郏县](../Page/郏县.md "wikilink")

|group6 = [安阳市](../Page/安阳市.md "wikilink") |list6 =
[北关区](../Page/北关区.md "wikilink"){{.w}}[文峰区](../Page/文峰区.md "wikilink"){{.w}}[殷都区](../Page/殷都区.md "wikilink"){{.w}}[龙安区](../Page/龙安区.md "wikilink"){{.w}}[林州市](../Page/林州市.md "wikilink"){{.w}}[安阳县](../Page/安阳县.md "wikilink"){{.w}}[汤阴县](../Page/汤阴县.md "wikilink"){{.w}}[滑县](../Page/滑县.md "wikilink"){{.w}}[内黄县](../Page/内黄县.md "wikilink")

|group7 = [鹤壁市](../Page/鹤壁市.md "wikilink") |list7 =
[淇滨区](../Page/淇滨区.md "wikilink"){{.w}}[鹤山区](../Page/鹤山区.md "wikilink"){{.w}}[山城区](../Page/山城区.md "wikilink"){{.w}}[浚县](../Page/浚县.md "wikilink"){{.w}}[淇县](../Page/淇县.md "wikilink")

|group8 = [新乡市](../Page/新乡市.md "wikilink") |list8 =
[卫滨区](../Page/卫滨区.md "wikilink"){{.w}}[红旗区](../Page/红旗区.md "wikilink"){{.w}}[凤泉区](../Page/凤泉区.md "wikilink"){{.w}}[牧野区](../Page/牧野区.md "wikilink"){{.w}}[卫辉市](../Page/卫辉市.md "wikilink"){{.w}}[辉县市](../Page/辉县市.md "wikilink"){{.w}}[新乡县](../Page/新乡县.md "wikilink"){{.w}}[获嘉县](../Page/获嘉县.md "wikilink"){{.w}}[原阳县](../Page/原阳县.md "wikilink"){{.w}}[延津县](../Page/延津县.md "wikilink"){{.w}}[封丘县](../Page/封丘县.md "wikilink"){{.w}}[长垣县](../Page/长垣县.md "wikilink")

|group9 = [焦作市](../Page/焦作市.md "wikilink") |list9 =
[解放区](../Page/解放区_\(焦作市\).md "wikilink"){{.w}}[中站区](../Page/中站区.md "wikilink"){{.w}}[马村区](../Page/马村区.md "wikilink"){{.w}}[山阳区](../Page/山阳区.md "wikilink"){{.w}}[沁阳市](../Page/沁阳市.md "wikilink"){{.w}}[孟州市](../Page/孟州市.md "wikilink"){{.w}}[修武县](../Page/修武县.md "wikilink"){{.w}}[博爱县](../Page/博爱县.md "wikilink"){{.w}}[武陟县](../Page/武陟县.md "wikilink"){{.w}}[温县](../Page/温县.md "wikilink")

|group10 = [濮阳市](../Page/濮阳市.md "wikilink") |list10 =
[华龙区](../Page/华龙区.md "wikilink"){{.w}}[清丰县](../Page/清丰县.md "wikilink"){{.w}}[南乐县](../Page/南乐县.md "wikilink"){{.w}}[范县](../Page/范县.md "wikilink"){{.w}}[台前县](../Page/台前县.md "wikilink"){{.w}}[濮阳县](../Page/濮阳县.md "wikilink")

|group11 = [许昌市](../Page/许昌市.md "wikilink") |list11 =
[魏都区](../Page/魏都区.md "wikilink"){{.w}}[建安区](../Page/建安区.md "wikilink"){{.w}}[禹州市](../Page/禹州市.md "wikilink"){{.w}}[长葛市](../Page/长葛市.md "wikilink"){{.w}}[鄢陵县](../Page/鄢陵县.md "wikilink"){{.w}}[襄城县](../Page/襄城县.md "wikilink")

|group12 = [漯河市](../Page/漯河市.md "wikilink") |list12 =
[郾城区](../Page/郾城区.md "wikilink"){{.w}}[源汇区](../Page/源汇区.md "wikilink"){{.w}}[召陵区](../Page/召陵区.md "wikilink"){{.w}}[舞阳县](../Page/舞阳县.md "wikilink"){{.w}}[临颍县](../Page/临颍县.md "wikilink")

|group13 = [三门峡市](../Page/三门峡市.md "wikilink") |list13 =
[湖滨区](../Page/湖滨区.md "wikilink"){{.w}}[陕州区](../Page/陕州区.md "wikilink"){{.w}}[义马市](../Page/义马市.md "wikilink"){{.w}}[灵宝市](../Page/灵宝市.md "wikilink"){{.w}}[渑池县](../Page/渑池县.md "wikilink"){{.w}}[卢氏县](../Page/卢氏县.md "wikilink")

|group14 = [南阳市](../Page/南阳市.md "wikilink") |list14 =
[卧龙区](../Page/卧龙区.md "wikilink"){{.w}}[宛城区](../Page/宛城区.md "wikilink"){{.w}}[邓州市](../Page/邓州市.md "wikilink"){{.w}}[南召县](../Page/南召县.md "wikilink"){{.w}}[方城县](../Page/方城县.md "wikilink"){{.w}}[西峡县](../Page/西峡县.md "wikilink"){{.w}}[镇平县](../Page/镇平县.md "wikilink"){{.w}}[内乡县](../Page/内乡县.md "wikilink"){{.w}}[淅川县](../Page/淅川县.md "wikilink"){{.w}}[社旗县](../Page/社旗县.md "wikilink"){{.w}}[唐河县](../Page/唐河县.md "wikilink"){{.w}}[新野县](../Page/新野县.md "wikilink"){{.w}}[桐柏县](../Page/桐柏县.md "wikilink")

|group15 = [商丘市](../Page/商丘市.md "wikilink") |list15 =
[梁园区](../Page/梁园区.md "wikilink"){{.w}}[睢阳区](../Page/睢阳区.md "wikilink"){{.w}}[永城市](../Page/永城市.md "wikilink"){{.w}}[民权县](../Page/民权县.md "wikilink"){{.w}}[睢县](../Page/睢县.md "wikilink"){{.w}}[宁陵县](../Page/宁陵县.md "wikilink"){{.w}}[柘城县](../Page/柘城县.md "wikilink"){{.w}}[虞城县](../Page/虞城县.md "wikilink"){{.w}}[夏邑县](../Page/夏邑县.md "wikilink")

|group16 = [信阳市](../Page/信阳市.md "wikilink") |list16 =
[平桥区](../Page/平桥区.md "wikilink"){{.w}}[浉河区](../Page/浉河区.md "wikilink"){{.w}}[罗山县](../Page/罗山县.md "wikilink"){{.w}}[光山县](../Page/光山县.md "wikilink"){{.w}}[新县](../Page/新县.md "wikilink"){{.w}}[商城县](../Page/商城县.md "wikilink"){{.w}}[固始县](../Page/固始县.md "wikilink"){{.w}}[潢川县](../Page/潢川县.md "wikilink"){{.w}}[淮滨县](../Page/淮滨县.md "wikilink"){{.w}}[息县](../Page/息县.md "wikilink")

|group17 = [周口市](../Page/周口市.md "wikilink") |list17 =
[川汇区](../Page/川汇区.md "wikilink"){{.w}}[项城市](../Page/项城市.md "wikilink"){{.w}}[扶沟县](../Page/扶沟县.md "wikilink"){{.w}}[西华县](../Page/西华县.md "wikilink"){{.w}}[商水县](../Page/商水县.md "wikilink"){{.w}}[沈丘县](../Page/沈丘县.md "wikilink"){{.w}}[郸城县](../Page/郸城县.md "wikilink"){{.w}}[淮阳县](../Page/淮阳县.md "wikilink"){{.w}}[太康县](../Page/太康县.md "wikilink"){{.w}}[鹿邑县](../Page/鹿邑县.md "wikilink")

|group18 = [驻马店市](../Page/驻马店市.md "wikilink") |list18 =
[驿城区](../Page/驿城区.md "wikilink"){{.w}}[西平县](../Page/西平县.md "wikilink"){{.w}}[上蔡县](../Page/上蔡县.md "wikilink"){{.w}}[平舆县](../Page/平舆县.md "wikilink"){{.w}}[正阳县](../Page/正阳县.md "wikilink"){{.w}}[确山县](../Page/确山县.md "wikilink"){{.w}}[泌阳县](../Page/泌阳县.md "wikilink"){{.w}}[汝南县](../Page/汝南县.md "wikilink"){{.w}}[遂平县](../Page/遂平县.md "wikilink"){{.w}}[新蔡县](../Page/新蔡县.md "wikilink")
}}

|group3style = text-align: center; |group3 = [省直辖
县级行政区](../Page/省直辖县级行政区.md "wikilink") |list3 =

|belowstyle = text-align: left; font-size: 80%; |below =
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[河南省乡级以上行政区列表](../Page/河南省乡级以上行政区列表.md "wikilink")。
}}<noinclude>

</noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[\*](../Category/河南行政区划.md "wikilink")
[河南行政区划模板](../Category/河南行政区划模板.md "wikilink")