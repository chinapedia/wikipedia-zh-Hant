[督政府](督政府.md "wikilink") | 1795年–1799年

`   | {{*}} `[`执政府`](执政府.md "wikilink")` | 1799年–1804年`
`   | `[`第一帝国`](法兰西第一帝国.md "wikilink")` | 1804年–1814年`
`   | `[`波旁复辟`](波旁复辟.md "wikilink")` | 1814年–1830年`
`   | `[`七月革命`](法国七月革命.md "wikilink")` | 1830年`
`   | `[`七月王朝`](七月王朝.md "wikilink")` | 1830年–1848年`
`   | `[`二月革命`](法国二月革命.md "wikilink")` | 1848年`
`   | `[`第二共和国`](法兰西第二共和国.md "wikilink")` | 1848年–1852年`
`   | `[`第二帝国`](法蘭西第二帝國.md "wikilink")` | 1852年–1870年`
`   | `[`第三共和国`](法兰西第三共和国.md "wikilink")` | 1870年–1940年`
`   | `[`巴黎公社`](巴黎公社.md "wikilink")` | 1871年`
`  }}`

|list7title =  |list7 =

|below =

}}<noinclude>

## 相关模板

</noinclude>

[Category:系列條目側面模板](../Category/系列條目側面模板.md "wikilink")
[法国历史模板](../Category/法国历史模板.md "wikilink")
[France](../Category/历史导航模板.md "wikilink")