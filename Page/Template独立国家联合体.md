[-{A](../Page/阿塞拜疆.md "wikilink"){{.w}}[白俄罗斯](../Page/白俄罗斯.md "wikilink"){{.w}}[哈萨克斯坦](../Page/哈萨克斯坦.md "wikilink"){{.w}}[-{zh-hans:吉尔吉斯斯坦;zh-hant:吉爾吉斯}-](../Page/吉尔吉斯斯坦.md "wikilink"){{.w}}[摩尔多瓦](../Page/摩尔多瓦.md "wikilink"){{.w}}[俄罗斯](../Page/俄罗斯.md "wikilink"){{.w}}[-{zh-hans:塔吉克斯坦;zh-hk:塔吉克;zh-tw:塔吉克;}-](../Page/塔吉克斯坦.md "wikilink"){{.w}}[乌兹别克斯坦](../Page/乌兹别克斯坦.md "wikilink")

|group2 = 附屬成員國 |list2 = [土库曼斯坦](../Page/土库曼斯坦.md "wikilink")

|group3 = 已退出 |list3 =
[格鲁吉亚](../Page/格鲁吉亚.md "wikilink"){{.w}}[乌克兰](../Page/乌克兰.md "wikilink")
| group4 = 體育 | list4 =
[奥林匹克运动会独联体代表团](../Page/奥林匹克运动会独联体代表团.md "wikilink"){{.w}}[帕拉林匹克運動會獨立國協代表團](../Page/帕拉林匹克運動會獨立國協代表團.md "wikilink"){{.w}}[獨立國協班迪球代表隊](../Page/Commonwealth_of_Independent_States_national_bandy_team.md "wikilink"){{.w}}[獨聯體國家足球隊](../Page/獨聯體國家足球隊.md "wikilink"){{.w}}[獨立國協男子冰球球代表隊](../Page/CIS_men's_national_ice_hockey_team.md "wikilink"){{.w}}[獨立國協橄欖球隊](../Page/CIS_\(rugby\).md "wikilink"){{.w}}[足球独联体杯](../Page/独联体杯.md "wikilink")

| group5 = 軍事 | list5 =
[集体安全条约组织](../Page/集体安全条约组织.md "wikilink"){{.w}}[集体快速反应部队](../Page/集体快速反应部队.md "wikilink"){{.w}}[独联体联合防空体系](../Page/独联体联合防空体系.md "wikilink")

| group6 = 經濟 | list6 =
[经济法院](../Page/独联体经济法院.md "wikilink"){{.w}}[独联体自贸区](../Page/独联体自贸区.md "wikilink"){{.w}}[歐亞經濟共同體](../Page/歐亞經濟共同體.md "wikilink"){{.w}}[欧亚专利公约](../Page/欧亚专利公约.md "wikilink"){{.w}}[欧亚专利组织](../Page/欧亚专利组织.md "wikilink"){{.w}}[欧洲技术援助](../Page/对独联体技术援助.md "wikilink")

| group7 = 協會 | list7 = [州際航空委員會](../Page/州際航空委員會.md "wikilink")

| below = [分類](../Page/:分類:独立国家联合体.md "wikilink")

}}<noinclude>

</noinclude>

[\*](../Category/独立国家联合体.md "wikilink")
[C](../Category/国际组织导航模板.md "wikilink")