<noinclude>  </noinclude>{{ sidebar | name = 用于维护与合作的资源 | title =
當前的[维护与合作任務](../Page/Wikipedia:維基百科維護.md "wikilink") |
titlestyle = background-color:\#d9ceb8; font-size: 120% | style =
border: 1px solid \#bfb1a3 | class = hlist | headingstyle = border:1px
\#bfb1a3 solid; background-color: \#f2e6ce | heading1 =
[清理](../Page/Wikipedia:清理.md "wikilink") | content1 =

  - [需要关注的页面](../Category/需要关注的页面.md "wikilink")
  - [需要清理的条目](../Category/需要清理的条目.md "wikilink")
  - [清理模板](../Page/Wikipedia:模板消息/清理.md "wikilink")
  - [關注度不足](../Category/主題關注度不足的條目.md "wikilink")
  - [校对](../Category/需要校对的页面.md "wikilink")
  - [维基化](../Category/需要维基化的页面.md "wikilink")
  - [缺少鏈入頁面的條目](../Category/缺少鏈入頁面的條目.md "wikilink")
  - [更新条目](../Category/待更新条目.md "wikilink")

| heading2 = [分類](../Page/Wikipedia:頁面分類.md "wikilink") | content2 =

  - [未分类条目](../Page/Special:Uncategorizedpages.md "wikilink")
  - [標示缺少分類的條目](../Category/缺少頁面分類的頁面.md "wikilink")
  - [未分类的页面分类](../Page/Special:Uncategorizedcategories.md "wikilink")
  - [条目太少的](../Category/需要補充條目的分類.md "wikilink")
  - [重定向](../Category/重定向.md "wikilink")

| heading3 = [-{zh-cn:创建;
zh-tw:建立}-条目](../Page/Wikipedia:您的第一篇文章.md "wikilink")
| content3 =

  - [暂缺傳統百科條目](../Page/Wikipedia:传统百科全书条目专题.md "wikilink")
  - [首页缺失条目](../Page/维基百科:首页的缺失条目专题.md "wikilink")
  - [最多需求的](../Page/Special:Wantedpages.md "wikilink")
  - [缺少的最多語言版本條目](../Page/Wikipedia:最多語言版本的待撰條目.md "wikilink")
  - [其他語言的典範條目](../Page/Wikipedia:模仿专题.md "wikilink")
  - [被请求的条目](../Page/Wikipedia:条目请求.md "wikilink")
  - [短页面](../Page/Special:短页面.md "wikilink")

| heading4 = 参考 | content4 =

  - [缺少来源的条目](../Category/缺少来源的条目.md "wikilink")
  - [事实审核](../Page/Wikipedia:來源專題.md "wikilink")
  - [需更多来源](../Category/需补充来源的条目.md "wikilink")
  - [原创研究](../Category/可能带有原创研究的条目.md "wikilink")
  - [參考資料格式錯誤](../Category/参考资料格式错误的页面.md "wikilink")

| heading5 = [小作品](../Page/Wikipedia:小作品.md "wikilink") | content5 =

  - [指引](../Page/Wikipedia:小作品.md "wikilink")
  - [短页面](../Page/Special:Shortpages.md "wikilink")
  - [按主题察看](../Page/Wikipedia:小作品类别列表.md "wikilink")
  - [小小作品](../Page/Wikipedia:小小作品.md "wikilink")
  - [未完成列表](../Category/未完成列表.md "wikilink")
  - [双周合作](../Page/Wikipedia:条目质量提升计划.md "wikilink")

| heading6 = [删除](../Page/Wikipedia:管理员删除指导.md "wikilink") | content6 =

  - [頁面](../Page/Wikipedia:頁面存廢討論.md "wikilink")
  - [文件](../Page/Wikipedia:檔案存廢討論.md "wikilink")
  - [無版權來源](../Page/Wikipedia:檔案存廢討論/無版權訊息或檔案來源.md "wikilink")
  - [日志](../Page/Special:Log/delete.md "wikilink")
  - [讨论存档](../Page/Wikipedia:删除投票/档案.md "wikilink")
  - [存廢复审](../Page/Wikipedia:存廢覆核請求.md "wikilink")
  - [修订版本删除](../Page/Wikipedia:修订版本删除请求.md "wikilink")
  - [快速](../Category/快速删除候选.md "wikilink")

| heading7 = [質量提升](../Page/Wikipedia:條目質量提升計劃.md "wikilink") | content7
=

  - [扩充请求](../Page/Wikipedia:扩充请求.md "wikilink")
  - [優良條目評選](../Page/Wikipedia:優良條目評選.md "wikilink")
  - [典範條目評選](../Page/Wikipedia:典范条目评选.md "wikilink")
  - [特色圖片評選](../Page/Wikipedia:特色圖片評選.md "wikilink")
  - [特色列表評選](../Page/Wikipedia:特色列表评选.md "wikilink")
  - [同行評審](../Page/Wikipedia:同行评审.md "wikilink")

| heading8 = 翻译成中文 | content8 =

  - [从外语维基百科](../Page/Wikipedia:翻译请求.md "wikilink")
  - [大量外文的页面](../Category/需要翻译的文章.md "wikilink")
  - [條目翻譯](../Category/正在翻譯的條目.md "wikilink")
  - [計劃翻譯](../Category/正在翻译的维基百科页面.md "wikilink")
  - [校對翻譯](../Category/需要校對的頁面.md "wikilink")
  - [维基百科:翻译](../Page/Wikipedia:翻译守则.md "wikilink")

| heading9 = [图像](../Page/Wikipedia:图像.md "wikilink") | content9 =

  - [题注复审](../Page/Wikipedia:题注.md "wikilink")
  - [图片请求](../Page/Wikipedia:图片请求.md "wikilink")

| heading10 = 争议 | content10 =

  - [条目准确性](../Page/Wikipedia:准确性争议.md "wikilink")
  - [准确性争议条目](../Category/准确性有争议的作品.md "wikilink")
  - [中立性](../Page/Wikipedia:中立观点争议.md "wikilink")
  - [中立性争议条目](../Category/中立性有争议的作品.md "wikilink")
  - [争议性陈述](../Page/Wikipedia:争议性陈述.md "wikilink")

| heading11 = [需進行工作列表](../Page/Wikipedia:需進行工作列表.md "wikilink") |
content11 =

  - [条目](../Page/Wikipedia:需進行工作列表.md "wikilink")
  - [专题](../Page/Wikipedia:专题.md "wikilink")

| heading12 = 標題 | content12 =

  - [命名常規](../Page/維基百科:命名常規.md "wikilink")
  - [移动请求](../Page/Wikipedia:移動請求.md "wikilink")

| heading13 = 消歧义 | content13 =

  - [消歧义](../Category/维基百科积压工作.md "wikilink")
  - [頂註](../Page/Wikipedia:頂註.md "wikilink")
  - [消歧義頁格式手冊](../Page/Wikipedia:格式手冊/消歧義頁.md "wikilink")
  - [模板](../Page/Wikipedia:模板消息/一般.md "wikilink")

| heading14 = 更多 | content14 =

  - [要合并的文章](../Page/Wikipedia:合并请求.md "wikilink")
  - [要分离的文章](../Category/需要分割的条目.md "wikilink")
  - [积压工作](../Category/维基百科积压工作.md "wikilink")
  - [版权侵害](../Page/Wikipedia:頁面存廢討論/疑似侵權.md "wikilink")
  - [活跃任务](../Page/Wikipedia:社区主页/活跃任务.md "wikilink")
  - [請求](../Page/Wikipedia:請求.md "wikilink")
  - [请求保护页面](../Page/Wikipedia:请求保护页面.md "wikilink")
  - [专家关注](../Category/需要专业人士关注的页面.md "wikilink")

}}<noinclude></noinclude>