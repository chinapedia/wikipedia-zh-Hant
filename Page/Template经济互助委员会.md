[波兰人民共和国](波兰人民共和国.md "wikilink"){{.w}}
[罗马尼亚社会主义共和国](罗马尼亚社会主义共和国.md "wikilink"){{.w}}
[捷克斯洛伐克社会主义共和国](捷克斯洛伐克社会主义共和国.md "wikilink"){{.w}}
[保加利亚人民共和国](保加利亞人民共和國.md "wikilink"){{.w}}
[蒙古人民共和国](蒙古人民共和国.md "wikilink"){{.w}}
[德意志民主共和国](德意志民主共和国.md "wikilink"){{.w}}
[越南社会主义共和国](越南社会主义共和国.md "wikilink"){{.w}}
[匈牙利人民共和国](匈牙利人民共和国.md "wikilink"){{.w}}
[古巴共和国](古巴共和国.md "wikilink")

|group2 = 咨询国 |list2 =  [南斯拉夫社会主义联邦共和国](南斯拉夫社会主义联邦共和国.md "wikilink")

|group3 = 退出的国家 |list3 =  [阿尔巴尼亚社会主义人民共和国](阿尔巴尼亚社会主义人民共和国.md "wikilink")

|group4 = 观察员国 |list4 =  [老挝人民民主共和国](老挝人民民主共和国.md "wikilink"){{.w}}
[阿富汗民主共和国](阿富汗民主共和国.md "wikilink"){{.w}}
[安哥拉人民共和国](安哥拉人民共和国.md "wikilink"){{.w}}
[埃塞俄比亚人民民主共和国](埃塞俄比亚人民民主共和国.md "wikilink"){{.w}}
[莫桑比克人民共和国](莫桑比克人民共和国.md "wikilink"){{.w}}
[也门民主人民共和国](也门民主人民共和国.md "wikilink"){{.w}}
[-{zh-hans:朝鲜民主主义人民共和国;zh-hant:朝鮮民主主義人民共和國}-](朝鲜民主主义人民共和国.md "wikilink"){{.w}}
[尼加拉瓜共和国](尼加拉瓜共和国.md "wikilink")

|below = |belowstyle =

|titlegroup = |titlegroupstyle = |innerstyle = }}<noinclude>
</noinclude>

[Category:国际组织导航模板](../Category/国际组织导航模板.md "wikilink")