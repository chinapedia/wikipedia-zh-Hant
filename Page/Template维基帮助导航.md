{{Navbox | name = 维基帮助导航 | title = [維基百科-{zh-cn:帮助; zh-tw:說明;
zh-hk:求助}-页面](Help:目录.md "wikilink") | image = | state =  |
listclass = hlist plainlinks

| group1 = [閱讀](Help:讀者手冊.md "wikilink") | list1 =

  - [-{zh-cn:链接; zh-tw:連結;}-](Help:链接.md "wikilink")
  - [-{zh-cn:搜索; zh-tw:搜尋;}-](Help:搜索.md "wikilink")
  - [分类](Help:分类.md "wikilink")
  - [章节](Help:章节.md "wikilink")
  - [-{zh-cn:名字空间; zh-tw:命名空間;}-](Wikipedia:名字空间.md "wikilink")
  - [URL](Help:URL.md "wikilink")
  - [跨语言-{zh-cn:链接; zh-tw:連結;}-](Help:跨语言链接.md "wikilink")
  - [打印页面](Help:打印页面.md "wikilink")
  - [随机页面](Help:随机页面.md "wikilink")
  - [特殊页面](Help:特殊页面.md "wikilink")
  - [讨论页](H:讨论页.md "wikilink")
  - [閱讀生物分類框](Help:如何閲讀生物分類框.md "wikilink")

| group2 = 账户与设定 | list2 =

  - [访-{}-问](Help:如何访问维基百科.md "wikilink")
  - [-{zh-cn:登录; zh-tw:登入;}-](Help:登录.md "wikilink")
  - [-{zh-cn:参数设置; zh-hk:參數設置; zh-tw:偏好設定;}-](Help:参数设置.md "wikilink")
  - [用户样式](Help:自定义设置.md "wikilink")

<!-- end list -->

  - [-{zh-cn:用户页; zh-hk:用戶頁; zh-tw:使用者頁面}-](Help:用户页.md "wikilink")
  - [电子邮件确认](Help:电子邮件确认.md "wikilink")

| group3 = 跟踪更改 | list3 =

  - [页面历史](Help:页面历史.md "wikilink")
  - [差异](Help:差异.md "wikilink")
  - [-{zh-cn:链; zh-hk:鏈; zh-mo:鏈; zh-tw:連}-入页面](Help:链入页面.md "wikilink")
  - [-{zh-cn:链; zh-hk:鏈; zh-mo:鏈; zh-tw:連}-出更改](Help:链出更改.md "wikilink")
  - [最近更改](Help:最近更改.md "wikilink")
  - [监视列表](Help:监视列表.md "wikilink")
  - [编辑摘要](Help:编辑摘要.md "wikilink")
  - [-{zh-cn:用户; zh-hk:用戶; zh-tw:使用者}-贡献](Help:用户贡献页面.md "wikilink")
  - [小修改](Help:小修改.md "wikilink")

| group4 = [编辑](Help:编辑手册.md "wikilink") | list4 =

  - [-{zh-cn:创建; zh-tw:建立;}-新頁面](Help:创建新页面.md "wikilink")
  - [编辑页面](Help:编辑页面.md "wikilink")
  - [链接颜色](Help:链接颜色.md "wikilink")
  - [列表](Help:列表.md "wikilink")
  - [表格](Help:表格.md "wikilink")
  - [图-{}-像](Help:图像.md "wikilink")
  - [模板](Help:模板.md "wikilink")
  - [HTML](Help:HTML.md "wikilink")
  - [页面更名](Help:页面重命名.md "wikilink")
  - [特殊-{zh-cn:字符; zh-tw:字元;}-](Help:特殊字母與符號.md "wikilink")
  - [回退](Wikipedia:回退.md "wikilink")
  - [-{zh-cn:脚注; zh-tw:註腳;}-](Help:脚注.md "wikilink")
      - [如何引用来源](帮助:如何引用来源.md "wikilink")
      - [相关工具](H:参考文献工具.md "wikilink")
  - [國際標準書號](Help:國際標準書號.md "wikilink")
  - [签名](Wikipedia:签名.md "wikilink")
  - [繁簡處理](Help:進階字詞轉換處理.md "wikilink")
  - [-{重定向}-](Help:重定向.md "wikilink")
  - [-{zh-cn:可视化; zh-tw:視覺化;}-编辑器](Wikipedia:可视化编辑器/用户指南.md "wikilink")
  - [翻译](H:翻译.md "wikilink")
  - [如何介绍自己的公司](帮助:如何介绍自己的公司.md "wikilink")

| group5 = -{zh-cn:高级; zh-tw:進階}- | list5 =

  - [魔术字](Help:魔术字.md "wikilink")
  - [-{zh-cn:默认; zh-tw:預設;}-参数](Help:默认参数.md "wikilink")
  - [解析器-{zh-cn:函数; zh-tw:函式; zh-hk:函數;}-](Help:解析器函数.md "wikilink")
      - [時間序號](Help:時間序號.md "wikilink")
      - [隨機功能](Help:隨機顯示.md "wikilink")
  - [替换引用](Help:替换引用.md "wikilink")
  - [乐谱](Help:乐谱.md "wikilink")
  - [计算](Help:计算.md "wikilink")
  - [数学公式](Help:数学公式.md "wikilink")
  - [简易时间线语法](Help:简易时间线语法.md "wikilink")
  - [输入框](Help:输入框.md "wikilink")
  - [-{zh-cn:自定义; zh-tw:自訂;}-首頁](幫助:自訂首頁.md "wikilink")
  - [小測](幫助:小測.md "wikilink")
  - [擴展](Help:擴展.md "wikilink")
  - [{{color](Help:LiquidThreads.md "wikilink")
  - [层叠样式表（CSS）](help:层叠样式表.md "wikilink")
  - [模板-{zh-cn:数据; zh-hk:數據; zh-tw:資料;}-](Help:模板數據.md "wikilink")
  - [手工字词转换](Help:手工字词转换.md "wikilink")
  - [中文维基百科的繁简、地区词处理](Help:中文维基百科的繁简、地区词处理.md "wikilink")
  - [折叠显示](Help:折叠显示.md "wikilink")

| group6 = 姊妹計劃-{zh-cn:帮助; zh-tw:說明; zh-hk:求助;}-页面 | list6 =

  - [zh-hans=目录](m:Help:{{lan.md "wikilink")
  - [維基新聞](n:Wikinews:帮助.md "wikilink")
  - [維基語錄](q:Help:帮助.md "wikilink")
  - [維基詞典](wikt:Wiktionary:帮助.md "wikilink")
  - [維基教科書](b:Wikibooks:帮助.md "wikilink")
  - [維基文庫](s:Help:目录.md "wikilink")
  - [zh-hans=目录](commons:Help:{{lan.md "wikilink")
      - [維基物種](wikispecies:Help:Contents.md "wikilink")
      - [維基學院](wikiversity:Help:Contents.md "wikilink")
      - [維基導遊](voy:Wikivoyage:帮助.md "wikilink")
      - [维基数据](wikidata:Help:目录.md "wikilink")
      - [MediaWiki](mw:Help:Contents/zh.md "wikilink")
    }}<noinclude>

</noinclude>

[](../Category/帮助模板.md "wikilink")
[Category:維基百科導航模板](../Category/維基百科導航模板.md "wikilink")