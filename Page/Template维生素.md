[四烯甲萘醌（K<sub>2</sub>）](../Page/四烯甲萘醌.md "wikilink"){{·}}[甲萘醌（K<sub>3</sub>）](../Page/甲萘醌.md "wikilink"){{·}}[甲萘二酚（K<sub>4</sub>）](../Page/甲萘二酚.md "wikilink")

}}

`| group2 = 水溶性`
`| list2 = `**`B`<sub>`2`</sub>**`（`[`核黄素`](../Page/核黄素.md "wikilink")`）{{·}}`**`B`<sub>`3`</sub>**`（`[`烟酸`](../Page/烟酸.md "wikilink")`、`[`烟酰胺`](../Page/烟酰胺.md "wikilink")`）{{·}}`**`B`<sub>`5`</sub>**`（`[`泛酸`](../Page/泛酸.md "wikilink")`、``、``）{{·}}`**[`B`<sub>`6`</sub>](../Page/维生素B6.md "wikilink")**`（`[`吡哆醇`](../Page/吡哆醇.md "wikilink")`、`[`磷酸吡哆醛`](../Page/磷酸吡哆醛.md "wikilink")`、`[`吡哆胺`](../Page/吡哆胺.md "wikilink")`、`[`吡硫醇`](../Page/吡硫醇.md "wikilink")`）{{·}}`**`B`<sub>`7`</sub>**`（`[`生物素`](../Page/生物素.md "wikilink")`）{{·}}`**`B`<sub>`9`</sub>**`（`[`叶酸`](../Page/叶酸.md "wikilink")`、`[`二氢叶酸`](../Page/二氢叶酸.md "wikilink")`、`[`亚叶酸`](../Page/亚叶酸.md "wikilink")`、`[`MTHF`](../Page/5,10-亚甲基四氢叶酸.md "wikilink")`）{{·}}`**[`B`<sub>`12`</sub>](../Page/维生素B12.md "wikilink")**`（`[`氰钴胺`](../Page/氰钴胺.md "wikilink")`、`[`羟钴胺素`](../Page/羟钴胺素.md "wikilink")`、`[`甲钴胺`](../Page/甲钴胺.md "wikilink")`、`[`腺苷钴胺`](../Page/腺苷钴胺.md "wikilink")`）{{·}}`[`胆碱`](../Page/胆碱.md "wikilink")

|group2=[C](../Page/维生素C.md "wikilink") |list2=

  - [抗坏血酸](../Page/抗坏血酸.md "wikilink")
  - [脱氢抗坏血酸](../Page/脱氢抗坏血酸.md "wikilink")

}}

|group3=组合 |list3=

  -
|belowstyle=background:transparent;padding:0px; |below=

}}<noinclude>  </noinclude>

[](../Category/酶模板.md "wikilink") [](../Category/化学模板.md "wikilink")
[A11](../Category/採用ATC碼的藥物模板.md "wikilink")