{{\#invoke:sidebar|sidebar | name = 网络协议 | title =
[網際網路-{zh-tw:協定;zh-cn:协议;}-套組](TCP/IP协议族.md "wikilink") |
bodyclass = hlist

| heading1 = [應用層](應用層.md "wikilink") | content1 =

  - [BGP](Border_Gateway_Protocol.md "wikilink")

  - [DHCP](Dynamic_Host_Configuration_Protocol.md "wikilink")

  - [DNS](Domain_Name_System.md "wikilink")

  - [FTP](檔案傳輸協定.md "wikilink")

  - [HTTP](超文本传输协议.md "wikilink")

  - [IMAP](Internet_Message_Access_Protocol.md "wikilink")

  - [LDAP](Lightweight_Directory_Access_Protocol.md "wikilink")

  -
  - [NNTP](Network_News_Transfer_Protocol.md "wikilink")

  - [NTP](Network_Time_Protocol.md "wikilink")

  - [POP](Post_Office_Protocol.md "wikilink")

  - [ONC/RPC](Open_Network_Computing_Remote_Procedure_Call.md "wikilink")

  - [RTP](Real-time_Transport_Protocol.md "wikilink")

  - [RTSP](Real_Time_Streaming_Protocol.md "wikilink")

  - [RIP](Routing_Information_Protocol.md "wikilink")

  - [SIP](Session_Initiation_Protocol.md "wikilink")

  - [SMTP](简单邮件传输协议.md "wikilink")

  - [SNMP](Simple_Network_Management_Protocol.md "wikilink")

  - [SSH](Secure_Shell.md "wikilink")

  - [Telnet](Telnet.md "wikilink")

  - [TLS／SSL](傳輸層安全性協定.md "wikilink")

  - [XMPP](可扩展消息与存在协议.md "wikilink")

  - [*更多...*](:../Category/应用层协议.md "wikilink")

| heading2 = [傳輸層](傳輸層.md "wikilink") | content2 =

  - [TCP](Transmission_Control_Protocol.md "wikilink")
  - [UDP](User_Datagram_Protocol.md "wikilink")
  - [DCCP](Datagram_Congestion_Control_Protocol.md "wikilink")
  - [SCTP](Stream_Control_Transmission_Protocol.md "wikilink")
  - [RSVP](Resource_Reservation_Protocol.md "wikilink")
  - [*更多...*](:../Category/传输层协议.md "wikilink")

| heading3 = [網路層](網路層.md "wikilink") | content3 =

  - [IP](Internet_Protocol.md "wikilink")
      - [IPv4](IPv4.md "wikilink")
      - [IPv6](IPv6.md "wikilink")
  - [ICMP](Internet_Control_Message_Protocol.md "wikilink")
  - [ICMPv6](ICMPv6.md "wikilink")
  - [ECN](明確擁塞通知.md "wikilink")
  - [IGMP](Internet_Group_Management_Protocol.md "wikilink")
  - [OSPF](Open_Shortest_Path_First.md "wikilink")
  - [IPsec](IPsec.md "wikilink")
  - [*更多...*](:../Category/网络层协议.md "wikilink")

| heading4 =  | content4 =

  - [ARP](Address_Resolution_Protocol.md "wikilink")
  - [NDP](Neighbor_Discovery_Protocol.md "wikilink")
  - [Tunnels](Tunneling_protocol.md "wikilink")
      - [L2TP](Layer_2_Tunneling_Protocol.md "wikilink")
  - [PPP](Point-to-Point_Protocol.md "wikilink")
  - [MAC](Media_access_control.md "wikilink")
      - [Ethernet](Ethernet.md "wikilink")
      - [DSL](Digital_subscriber_line.md "wikilink")
      - [ISDN](Integrated_Services_Digital_Network.md "wikilink")
      - [FDDI](Fiber_Distributed_Data_Interface.md "wikilink")
  - [*更多...*](:../Category/链路协议.md "wikilink")

}}<noinclude>

</noinclude>

[](../Category/计算机技术模板.md "wikilink")