
{{ nowrap|**`AZ`**  [亞利桑那](../Page/亞利桑那州.md "wikilink")}}
{{ nowrap|**`AR`**  [阿肯色](../Page/阿肯色州.md "wikilink")}}
{{ nowrap|**`CA`**  [加利福尼亞](../Page/加利福尼亚州.md "wikilink")}}
{{ nowrap|**`CO`**  [科羅拉多](../Page/科羅拉多州.md "wikilink")}}
{{ nowrap|**`CT`**
[康-{zh-hans:涅狄格;zh-hk:涅狄格;zh-tw:乃狄克;}-](../Page/康乃狄克州.md "wikilink")}}
{{ nowrap|**`DE`**
[-{zh-hans:特拉华;zh-hk:特拉華;zh-tw:德拉瓦;}-](../Page/特拉华州.md "wikilink")}}
{{ nowrap|**`FL`**  [佛罗里达](../Page/佛罗里达州.md "wikilink")}}
{{ nowrap|**`GA`**
[-{zh-hans:佐;zh-hk:佐;zh-tw:喬;}-治亞](../Page/喬治亞州.md "wikilink")}}
|col2 = {{ nowrap|**`HI`**  [夏威夷](../Page/夏威夷州.md "wikilink")}}
{{ nowrap|**`ID`**
[-{zh-hans:爱达;zh-hk:愛達;zh-tw:愛達;}-荷](../Page/爱达荷州.md "wikilink")}}
{{ nowrap|**`IL`**
[-{zh-hans:伊利诺伊;zh-hk:伊利諾伊;zh-tw:伊利諾;}-](../Page/伊利诺伊州.md "wikilink")}}
{{ nowrap|**`IN`**  [印第安纳](../Page/印第安纳州.md "wikilink")}}
{{ nowrap|**`IA`**
[-{zh-hans:艾奥瓦;zh-hk:艾奧瓦;zh-tw:愛荷華;}-](../Page/艾奥瓦州.md "wikilink")}}
{{ nowrap|**`KS`**  [堪薩斯](../Page/堪薩斯州.md "wikilink")}}
{{ nowrap|**`KY`**  [肯塔基](../Page/肯塔基州.md "wikilink")}}
{{ nowrap|**`LA`**  [路易斯安那](../Page/路易斯安那州.md "wikilink")}}
{{ nowrap|**`ME`**  [缅因](../Page/缅因州.md "wikilink")}}
{{ nowrap|**`MD`**  [马里兰](../Page/马里兰州.md "wikilink")}}
|col3 = {{ nowrap|**`MA`**
[-{zh-hans:马萨诸塞;zh-hk:麻省;zh-tw:麻薩諸塞;}-](../Page/麻薩諸塞州.md "wikilink")}}
{{ nowrap|**`MI`**
[密-{zh-hans:歇;zh-hk:芝;zh-tw:西;}-根](../Page/密歇根州.md "wikilink")}}
{{ nowrap|**`MN`**  [明尼蘇達](../Page/明尼蘇達州.md "wikilink")}}
{{ nowrap|**`MS`**  [密西西比](../Page/密西西比州.md "wikilink")}}
{{ nowrap|**`MO`**  [密蘇里](../Page/密蘇里州.md "wikilink")}}
{{ nowrap|**`MT`**  [蒙大拿](../Page/蒙大拿州.md "wikilink")}}
{{ nowrap|**`NE`**  [內布拉斯加](../Page/內布拉斯加州.md "wikilink")}}
{{ nowrap|**`NV`**  [内华达](../Page/内华达州.md "wikilink")}}
{{ nowrap|**`NH`**
[-{zh-hans:新罕布什尔;zh-tw:新罕布夏;zh-hk:新罕布什爾;}-](../Page/新罕布什尔州.md "wikilink")}}
{{ nowrap|**`NJ`**
[-{zh-hans:新泽西;zh-hk:紐澤西;zh-tw:新澤西;}-](../Page/新泽西州.md "wikilink")}}
|col4 = {{ nowrap|**`NM`**  [新墨西哥](../Page/新墨西哥州.md "wikilink")}}
{{ nowrap|**`NY`**  [纽约](../Page/纽约州.md "wikilink")}}
{{ nowrap|**`NC`**
[北卡羅-{zh-hans:来纳;zh-hk:萊納;zh-tw:來納;}-](../Page/北卡罗来纳州.md "wikilink")}}
{{ nowrap|**`ND`**  [北达科他](../Page/北达科他州.md "wikilink")}}
{{ nowrap|**`OH`**  [俄亥俄](../Page/俄亥俄州.md "wikilink")}}
{{ nowrap|**`OK`**
[-{zh-hans:俄克拉何;zh-hk:奧克拉荷;zh-tw:奧克拉荷;}-馬](../Page/奧克拉荷馬州.md "wikilink")}}
{{ nowrap|**`OR`**  [俄勒冈](../Page/俄勒冈州.md "wikilink")}}
{{ nowrap|**`PA`**
[賓夕-{zh-hans:法;zh-hk:凡;zh-tw:法;}-尼亞](../Page/宾夕法尼亚州.md "wikilink")}}
{{ nowrap|**`RI`**
[羅-{zh-hans:得;zh-hk:德;zh-tw:德;}-島](../Page/羅德島州.md "wikilink")}}
{{ nowrap|**`SC`**
[南卡羅-{zh-hans:来纳;zh-hk:萊納;zh-tw:來納;}-](../Page/南卡罗来纳州.md "wikilink")}}
|col5 = {{ nowrap|**`SD`**  [南达科他](../Page/南达科他州.md "wikilink")}}
{{ nowrap|**`TN`**  [田纳西](../Page/田纳西州.md "wikilink")}}
{{ nowrap|**`TX`**
[-{zh-hans:得克;zh-hk:德克;zh-tw:德克;}-薩斯](../Page/德克萨斯州.md "wikilink")}}
{{ nowrap|**`UT`**  [犹他](../Page/犹他州.md "wikilink")}}
{{ nowrap|**`VT`**  [佛蒙特](../Page/佛蒙特州.md "wikilink")}}
{{ nowrap|**`VA`**
[-{zh-hans:弗吉;zh-hk:維珍;zh-tw:維吉;}-尼亞](../Page/弗吉尼亚州.md "wikilink")}}
{{ nowrap|**`WA`**  [华盛顿](../Page/华盛顿州.md "wikilink")}}
{{ nowrap|**`WV`**
[西-{zh-hans:弗吉;zh-hk:維珍;zh-tw:維吉;}-尼亞](../Page/西維吉尼亞州.md "wikilink")}}
{{ nowrap|**`WI`**
[威斯康-{zh-hans:星;zh-hk:辛;zh-tw:辛;}-](../Page/威斯康辛州.md "wikilink")}}
{{ nowrap|**`WY`**  [怀俄明](../Page/怀俄明州.md "wikilink")}}
}} |group2 = [聯邦地區](../Page/聯邦地區.md "wikilink") |list2 =  |group3 =
[島嶼地區](../Page/島嶼地區.md "wikilink") |list3 =  |group4 = 其他 |list4
=  }}<noinclude>  </noinclude>

[\*](../Category/美国行政区划.md "wikilink")
[美国行政区划导航模板](../Category/美国行政区划导航模板.md "wikilink")
[USA](../Category/各国一级行政区划导航模板.md "wikilink")