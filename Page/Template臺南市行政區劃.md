[南區](../Page/南區_\(臺南市\).md "wikilink"){{.w}}[北區](../Page/北區_\(臺南市\).md "wikilink"){{.w}}[安平區](../Page/安平區.md "wikilink"){{.w}}[安南區](../Page/安南區.md "wikilink")

` |group2 = 新營地區`
` |list2= `[`新營區`](../Page/新營區.md "wikilink")`{{.w}}`[`後壁區`](../Page/後壁區.md "wikilink")`{{.w}}`[`白河區`](../Page/白河區.md "wikilink")`{{.w}}`[`東山區`](../Page/東山區_\(臺南市\).md "wikilink")`{{.w}}`[`柳營區`](../Page/柳營區.md "wikilink")`{{.w}}`[`鹽水區`](../Page/鹽水區.md "wikilink")
` |group3 = 曾文地區`
` |list3= `[`麻豆區`](../Page/麻豆區.md "wikilink")`{{.w}}`[`官田區`](../Page/官田區.md "wikilink")`{{.w}}`[`六甲區`](../Page/六甲區.md "wikilink")`{{.w}}`[`大內區`](../Page/大內區.md "wikilink")`{{.w}}`[`下營區`](../Page/下營區.md "wikilink")

|group4 = 北門地區

` |list4= `[`佳里區`](../Page/佳里區.md "wikilink")`{{.w}}`[`學甲區`](../Page/學甲區.md "wikilink")`{{.w}}`[`北門區`](../Page/北門區.md "wikilink")`{{.w}}`[`將軍區`](../Page/將軍區.md "wikilink")`{{.w}}`[`七股區`](../Page/七股區.md "wikilink")`{{.w}}`[`西港區`](../Page/西港區.md "wikilink")

|group5 = 新化地區

` |list5= `[`新化區`](../Page/新化區.md "wikilink")`{{.w}}`[`新市區`](../Page/新市區_\(台灣\).md "wikilink")`{{.w}}`[`善化區`](../Page/善化區.md "wikilink")`{{.w}}`[`安定區`](../Page/安定區_\(台灣\).md "wikilink")`{{.w}}`[`山上區`](../Page/山上區.md "wikilink")`{{.w}}`[`玉井區`](../Page/玉井區.md "wikilink")`{{.w}}`[`楠西區`](../Page/楠西區.md "wikilink")`{{.w}}`[`左鎮區`](../Page/左鎮區.md "wikilink")`{{.w}}`[`南化區`](../Page/南化區.md "wikilink")

|group6 = 新豐地區

` |list6= `[`永康區`](../Page/永康區.md "wikilink")`{{.w}}`[`仁德區`](../Page/仁德區.md "wikilink")`{{.w}}`[`歸仁區`](../Page/歸仁區.md "wikilink")`{{.w}}`[`關廟區`](../Page/關廟區.md "wikilink")`{{.w}}`[`龍崎區`](../Page/龍崎區.md "wikilink")
`   |group7= 已合併`
`   |list7 = `[`中區`](../Page/中區_\(臺南市\).md "wikilink")`{{.w}}`[`西區`](../Page/西區_\(臺南市\).md "wikilink")
`   }}`
` |group2 = 歷史沿革`
` | list2 = `[`天興縣`](../Page/天興縣.md "wikilink")`）`
`   |group3 = 清治`
`   | list3 = `[`臺灣府`](../Page/臺灣府.md "wikilink")`（`[`臺灣縣`](../Page/臺灣縣_\(1684年-1887年\).md "wikilink")`{{.w}}`[`諸羅縣`](../Page/諸羅縣.md "wikilink")`）{{→}}`[`臺南府`](../Page/臺南府.md "wikilink")`（`[`安平縣`](../Page/安平縣_\(台灣\).md "wikilink")`{{.w}}`[`嘉義縣`](../Page/嘉義縣_\(清朝\).md "wikilink")`）`
`   |group4 = 日治`
`   | list4 = `[`臺南縣`](../Page/臺南縣_\(日治時期\).md "wikilink")`（`[`嘉義縣`](../Page/嘉義縣_\(日治時期\).md "wikilink")`）{{→}}`[`臺南廳`](../Page/臺南廳.md "wikilink")`{{.w}}`[`鹽水港廳`](../Page/鹽水港廳.md "wikilink")`{{→}}`[`臺南廳`](../Page/臺南廳.md "wikilink")`{{.w}}`[`嘉義廳`](../Page/嘉義廳.md "wikilink")`{{→}}`
[`臺南州`](../Page/臺南州.md "wikilink")`（`[`臺南市`](../Page/臺南市_\(州轄市\).md "wikilink")`{{.w}}`[`新豐郡`](../Page/新豐郡.md "wikilink")`{{.w}}`[`新化郡`](../Page/新化郡.md "wikilink")`{{.w}}`[`曾文郡`](../Page/曾文郡.md "wikilink")`{{.w}}`[`北門郡`](../Page/北門郡.md "wikilink")`{{.w}}`[`新營郡`](../Page/新營郡.md "wikilink")`）`
`   |group5 = 民國`
`   | list5 = `[`臺南市`](../Page/臺南市_\(省轄市\).md "wikilink")`{{.w}}`[`臺南縣`](../Page/臺南縣.md "wikilink")`（新營區{{.w}}新化區{{.w}}曾文區{{.w}}北門區{{.w}}新豐區）{{→}}`[`臺南縣`](../Page/臺南縣.md "wikilink")[`市`](../Page/臺南市_\(省轄市\).md "wikilink")
`}}`
` | below  = `[`大員`](../Page/大員.md "wikilink")`{{.w}}`[`臺灣府城`](../Page/臺灣府城.md "wikilink")`{{.w}}`[`臺南都會區`](../Page/臺南都會區.md "wikilink")`{{.w}}`[`臺灣首都`](../Page/臺灣首都.md "wikilink")

}}<includeonly></includeonly> <noinclude> </noinclude>

[Category:臺南市行政區劃](../Category/臺南市行政區劃.md "wikilink")
[](../Category/台灣行政區劃模板.md "wikilink")