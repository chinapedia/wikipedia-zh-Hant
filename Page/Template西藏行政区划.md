[达孜区](../Page/达孜区.md "wikilink"){{.w}}[林周县](../Page/林周县.md "wikilink"){{.w}}[当雄县](../Page/当雄县.md "wikilink"){{.w}}[尼木县](../Page/尼木县.md "wikilink"){{.w}}[曲水县](../Page/曲水县.md "wikilink"){{.w}}[墨竹工卡县](../Page/墨竹工卡县.md "wikilink")

|group3 = [日喀则市](../Page/日喀则市.md "wikilink") |list3 =
[桑珠孜区](../Page/桑珠孜区.md "wikilink"){{.w}}[南木林县](../Page/南木林县.md "wikilink"){{.w}}[江孜县](../Page/江孜县.md "wikilink"){{.w}}[定日县](../Page/定日县.md "wikilink"){{.w}}[萨迦县](../Page/萨迦县.md "wikilink"){{.w}}[拉孜县](../Page/拉孜县.md "wikilink"){{.w}}[昂仁县](../Page/昂仁县.md "wikilink"){{.w}}[谢通门县](../Page/谢通门县.md "wikilink"){{.w}}[白朗县](../Page/白朗县.md "wikilink"){{.w}}[仁布县](../Page/仁布县.md "wikilink"){{.w}}[康马县](../Page/康马县.md "wikilink"){{.w}}[定结县](../Page/定结县.md "wikilink"){{.w}}[仲巴县](../Page/仲巴县.md "wikilink"){{.w}}[亚东县](../Page/亚东县.md "wikilink"){{.w}}[吉隆县](../Page/吉隆县.md "wikilink"){{.w}}[聂拉木县](../Page/聂拉木县.md "wikilink"){{.w}}[萨嘎县](../Page/萨嘎县.md "wikilink"){{.w}}[岗巴县](../Page/岗巴县.md "wikilink")

|group4 = [昌都市](../Page/昌都市.md "wikilink") |list4 =
[卡若区](../Page/卡若区.md "wikilink"){{.w}}[江达县](../Page/江达县.md "wikilink"){{.w}}[贡觉县](../Page/贡觉县.md "wikilink"){{.w}}[类乌齐县](../Page/类乌齐县.md "wikilink"){{.w}}[丁青县](../Page/丁青县.md "wikilink"){{.w}}[察雅县](../Page/察雅县.md "wikilink"){{.w}}[八宿县](../Page/八宿县.md "wikilink"){{.w}}[左贡县](../Page/左贡县.md "wikilink"){{.w}}[芒康县](../Page/芒康县.md "wikilink"){{.w}}[洛隆县](../Page/洛隆县.md "wikilink"){{.w}}[边坝县](../Page/边坝县.md "wikilink")

|group5 = [林芝市](../Page/林芝市.md "wikilink") |list5 =
[巴宜区](../Page/巴宜区.md "wikilink"){{.w}}[工布江达县](../Page/工布江达县.md "wikilink"){{.w}}[米林县](../Page/米林县.md "wikilink"){{.w}}[墨脱县](../Page/墨脱县.md "wikilink"){{.w}}[波密县](../Page/波密县.md "wikilink"){{.w}}[察隅县](../Page/察隅县.md "wikilink"){{.w}}[朗县](../Page/朗县.md "wikilink")

|group6 = [山南市](../Page/山南市.md "wikilink") |list6 =
[乃東區](../Page/乃東區.md "wikilink"){{.w}}[扎囊县](../Page/扎囊县.md "wikilink"){{.w}}[贡嘎县](../Page/贡嘎县.md "wikilink"){{.w}}[桑日县](../Page/桑日县.md "wikilink"){{.w}}[琼结县](../Page/琼结县.md "wikilink"){{.w}}[曲松县](../Page/曲松县.md "wikilink"){{.w}}[措美县](../Page/措美县.md "wikilink"){{.w}}[洛扎县](../Page/洛扎县.md "wikilink"){{.w}}[加查县](../Page/加查县.md "wikilink"){{.w}}[隆子县](../Page/隆子县.md "wikilink"){{.w}}[错那县](../Page/错那县.md "wikilink"){{.w}}[浪卡子县](../Page/浪卡子县.md "wikilink")

|group7 = [那曲市](../Page/那曲市.md "wikilink") |list7 =
[色尼区](../Page/色尼区.md "wikilink"){{.w}}[嘉黎县](../Page/嘉黎县.md "wikilink"){{.w}}[比如县](../Page/比如县.md "wikilink"){{.w}}[聂荣县](../Page/聂荣县.md "wikilink"){{.w}}[安多县](../Page/安多县.md "wikilink"){{.w}}[申扎县](../Page/申扎县.md "wikilink"){{.w}}[索县](../Page/索县.md "wikilink"){{.w}}[班戈县](../Page/班戈县.md "wikilink"){{.w}}[巴青县](../Page/巴青县.md "wikilink"){{.w}}[尼玛县](../Page/尼玛县.md "wikilink"){{.w}}[双湖县](../Page/双湖县.md "wikilink")
}} |group3style =text-align: center; |group3 =
[地区](../Page/地区_\(中国行政区划\).md "wikilink") |list3 =
[普兰县](../Page/普兰县.md "wikilink"){{.w}}[札达县](../Page/札达县.md "wikilink"){{.w}}[日土县](../Page/日土县.md "wikilink"){{.w}}[革吉县](../Page/革吉县.md "wikilink"){{.w}}[改则县](../Page/改则县.md "wikilink"){{.w}}[措勤县](../Page/措勤县.md "wikilink")
}}

|belowstyle = text-align:left; font-size:80%; |below =
注：[错那县](../Page/错那县.md "wikilink")、[隆子县](../Page/隆子县.md "wikilink")、[朗县](../Page/朗县.md "wikilink")、[米林县](../Page/米林县.md "wikilink")、[墨脱县和](../Page/墨脱县.md "wikilink")[察隅县的南面土地](../Page/察隅县.md "wikilink")，统称[藏南地區](../Page/藏南地區.md "wikilink")，未实际統治，參见[中印边界问题](../Page/中印边界问题.md "wikilink")。
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[西藏自治区乡级以上行政区列表](../Page/西藏自治区乡级以上行政区列表.md "wikilink")。
}}<noinclude>

</noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[西藏行政区划模板](../Category/西藏行政区划模板.md "wikilink")
[Category:西藏自治区导航模板](../Category/西藏自治区导航模板.md "wikilink")