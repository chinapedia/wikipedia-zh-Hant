{{\#invoke:sidebar|sidebar |wraplinks = true |style = width: auto;
border-collapse: collapse; |above = [调音方法](调音方法.md "wikilink")
|aboveclass = navbox-title |headingclass = navbox-group |headingstyle =
text-align: center; |contentclass = plainlist |contentstyle =
text-align: left; padding: .5em; |navbarstyle = padding-right: 0.2em;

|heading1 = |content1 =

  - [阻礙音](阻礙音.md "wikilink")
      - [塞音](塞音.md "wikilink")

      - [塞擦音](塞擦音.md "wikilink")

      - [擦音](擦音.md "wikilink")

          - [噝音](噝音.md "wikilink")/有噝擦音
  - [响音](響音.md "wikilink")
      - [鼻音](鼻音_\(辅音\).md "wikilink")

      - [闪音](闪音.md "wikilink")/彈音

      - [近音](近音.md "wikilink")

          - [半元音](半元音.md "wikilink")

      - [元音](元音.md "wikilink")

      -   - [彈音](彈音.md "wikilink")

          - [顫音](颤音_\(语音学\).md "wikilink")
  - [流音](流音.md "wikilink")
      -
      - [边音](边音.md "wikilink")
  - [閉合音](閉合音.md "wikilink")
  - [持續音](持續音.md "wikilink")

| heading2 = [氣流機制](氣流機制.md "wikilink") | content2 =

  -
  -
  - [擠喉音](擠喉音.md "wikilink")

  - [內爆音](內爆音.md "wikilink")

  -
  - [搭嘴音](搭嘴音.md "wikilink")/舌音

  -
  -
  -
| heading3 = 參見 | content3 =

  - [调音](调音.md "wikilink")

  - [调音语音学](调音语音学.md "wikilink")

  - [送氣](送氣.md "wikilink")

  - [無聲除阻](無聲除阻.md "wikilink")

  -
  - [清音](清音.md "wikilink")

  - [濁音](濁音.md "wikilink")

  - [调音部位](调音部位.md "wikilink")

}}<noinclude>

</noinclude>

[](../Category/语言模板.md "wikilink")