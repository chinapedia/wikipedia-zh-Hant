[云岩区](../Page/云岩区.md "wikilink"){{.w}}[花溪区](../Page/花溪区.md "wikilink"){{.w}}[乌当区](../Page/乌当区.md "wikilink"){{.w}}[白云区](../Page/白云区_\(贵阳市\).md "wikilink"){{.w}}[清镇市](../Page/清镇市.md "wikilink"){{.w}}[开阳县](../Page/开阳县.md "wikilink"){{.w}}[息烽县](../Page/息烽县.md "wikilink"){{.w}}[修文县](../Page/修文县.md "wikilink")

|group3 = [六盘水市](../Page/六盘水市.md "wikilink") |list3 =
[钟山区](../Page/钟山区.md "wikilink"){{.w}}[六枝特区](../Page/六枝特区.md "wikilink"){{.w}}[盘州市](../Page/盘州市.md "wikilink"){{.w}}[水城县](../Page/水城县.md "wikilink")

|group4 = [遵义市](../Page/遵义市.md "wikilink") |list4 =
[汇川区](../Page/汇川区.md "wikilink"){{.w}}[红花岗区](../Page/红花岗区.md "wikilink"){{.w}}[播州区](../Page/播州区.md "wikilink"){{.w}}[赤水市](../Page/赤水市.md "wikilink"){{.w}}[仁怀市](../Page/仁怀市.md "wikilink"){{.w}}[桐梓县](../Page/桐梓县.md "wikilink"){{.w}}[绥阳县](../Page/绥阳县.md "wikilink"){{.w}}[正安县](../Page/正安县.md "wikilink"){{.w}}[凤冈县](../Page/凤冈县.md "wikilink"){{.w}}[湄潭县](../Page/湄潭县.md "wikilink"){{.w}}[余庆县](../Page/余庆县.md "wikilink"){{.w}}[习水县](../Page/习水县.md "wikilink"){{.w}}[道真仡佬族苗族自治县](../Page/道真仡佬族苗族自治县.md "wikilink"){{.w}}[务川仡佬族苗族自治县](../Page/务川仡佬族苗族自治县.md "wikilink")

|group5 = [安顺市](../Page/安顺市.md "wikilink") |list5 =
[西秀区](../Page/西秀区.md "wikilink"){{.w}}[平坝区](../Page/平坝区.md "wikilink"){{.w}}[普定县](../Page/普定县.md "wikilink"){{.w}}[镇宁布依族苗族自治县](../Page/镇宁布依族苗族自治县.md "wikilink"){{.w}}[关岭布依族苗族自治县](../Page/关岭布依族苗族自治县.md "wikilink"){{.w}}[紫云苗族布依族自治县](../Page/紫云苗族布依族自治县.md "wikilink")

|group6 = [毕节市](../Page/毕节市.md "wikilink") |list6 =
[七星关区](../Page/七星关区.md "wikilink"){{.w}}[大方县](../Page/大方县.md "wikilink"){{.w}}[黔西县](../Page/黔西县.md "wikilink"){{.w}}[金沙县](../Page/金沙县.md "wikilink"){{.w}}[织金县](../Page/织金县.md "wikilink"){{.w}}[纳雍县](../Page/纳雍县.md "wikilink"){{.w}}[赫章县](../Page/赫章县.md "wikilink"){{.w}}[威宁彝族回族苗族自治县](../Page/威宁彝族回族苗族自治县.md "wikilink")

|group7 = [铜仁市](../Page/铜仁市.md "wikilink") |list7 =
[碧江区](../Page/碧江区.md "wikilink"){{.w}}[万山区](../Page/万山区.md "wikilink"){{.w}}[江口县](../Page/江口县.md "wikilink"){{.w}}[石阡县](../Page/石阡县.md "wikilink"){{.w}}[思南县](../Page/思南县.md "wikilink"){{.w}}[德江县](../Page/德江县.md "wikilink"){{.w}}[玉屏侗族自治县](../Page/玉屏侗族自治县.md "wikilink"){{.w}}[印江土家族苗族自治县](../Page/印江土家族苗族自治县.md "wikilink"){{.w}}[沿河土家族自治县](../Page/沿河土家族自治县.md "wikilink"){{.w}}[松桃苗族自治县](../Page/松桃苗族自治县.md "wikilink")
}}

|group3style =text-align: center; |group3 =
[自治州](../Page/自治州.md "wikilink") |list3 =
[兴仁市](../Page/兴仁市.md "wikilink"){{.w}}[普安县](../Page/普安县.md "wikilink"){{.w}}[晴隆县](../Page/晴隆县.md "wikilink"){{.w}}[贞丰县](../Page/贞丰县.md "wikilink"){{.w}}[望谟县](../Page/望谟县.md "wikilink"){{.w}}[册亨县](../Page/册亨县.md "wikilink"){{.w}}[安龙县](../Page/安龙县.md "wikilink")

|group9 = [黔东南苗族侗族自治州](../Page/黔东南苗族侗族自治州.md "wikilink") |list9 =
[凯里市](../Page/凯里市.md "wikilink"){{.w}}[黄平县](../Page/黄平县.md "wikilink"){{.w}}[施秉县](../Page/施秉县.md "wikilink"){{.w}}[三穗县](../Page/三穗县.md "wikilink"){{.w}}[镇远县](../Page/镇远县.md "wikilink"){{.w}}[岑巩县](../Page/岑巩县.md "wikilink"){{.w}}[天柱县](../Page/天柱县.md "wikilink"){{.w}}[锦屏县](../Page/锦屏县.md "wikilink"){{.w}}[剑河县](../Page/剑河县.md "wikilink"){{.w}}[台江县](../Page/台江县.md "wikilink"){{.w}}[黎平县](../Page/黎平县.md "wikilink"){{.w}}[榕江县](../Page/榕江县.md "wikilink"){{.w}}[从江县](../Page/从江县.md "wikilink"){{.w}}[雷山县](../Page/雷山县.md "wikilink"){{.w}}[麻江县](../Page/麻江县.md "wikilink"){{.w}}[丹寨县](../Page/丹寨县.md "wikilink")

|group10 = [黔南布依族苗族自治州](../Page/黔南布依族苗族自治州.md "wikilink") |list10 =
[都匀市](../Page/都匀市.md "wikilink"){{.w}}[福泉市](../Page/福泉市.md "wikilink"){{.w}}[荔波县](../Page/荔波县.md "wikilink"){{.w}}[贵定县](../Page/贵定县.md "wikilink"){{.w}}[-{zh:瓮安县;zh-hant:甕安縣;zh-hans:瓮安县}-](../Page/瓮安县.md "wikilink"){{.w}}[独山县](../Page/独山县.md "wikilink"){{.w}}[平塘县](../Page/平塘县.md "wikilink"){{.w}}[罗甸县](../Page/罗甸县.md "wikilink"){{.w}}[长顺县](../Page/长顺县.md "wikilink"){{.w}}[龙里县](../Page/龙里县.md "wikilink"){{.w}}[惠水县](../Page/惠水县.md "wikilink"){{.w}}[三都水族自治县](../Page/三都水族自治县.md "wikilink")
}} |belowstyle = text-align: left; font-size: 80%;
|below=参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[贵州省乡级以上行政区列表](../Page/贵州省乡级以上行政区列表.md "wikilink")。
}}<noinclude>

</noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[\*](../Category/贵州行政区划模板.md "wikilink")