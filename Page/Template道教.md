<noinclude>

## 使用

### 使用说明

添加{{道教}}、{{Taoism}}、{{dao}}使用模板。

### 参见

  - [{{道教全国重点宫观}}](Template:道教全国重点宫观.md "wikilink")
  - [{{中国道教协会会长}}](Template:中国道教协会会长.md "wikilink")
  - [{{全真道}}](Template:全真道.md "wikilink")
  - [{{四大道教名山}}](Template:四大道教名山.md "wikilink")
  - [{{道藏}}](Template:道藏.md "wikilink")
  - [{{中国民间信仰}}](Template:中国民间信仰.md "wikilink")
  - [{{八仙}}](Template:八仙.md "wikilink")
  - [{{四海龍王}}](Template:四海龍王.md "wikilink")
  - [{{南瀛三大代天府}}](Template:南瀛三大代天府.md "wikilink")

</noinclude>

[\*](../Category/道教.md "wikilink")
[道教导航模板](../Category/道教导航模板.md "wikilink")
[Category:宗教侧面模板](../Category/宗教侧面模板.md "wikilink")