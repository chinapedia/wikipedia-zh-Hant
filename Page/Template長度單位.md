<noinclude></noinclude>[-{zh-hans:尧米;zh-hk:堯米;zh-tw:佑米;}-](../Page/堯米.md "wikilink")（Ym）{{·w}}[-{zh-hans:泽米;zh-hk:澤米;zh-tw:皆米;}-](../Page/澤米_\(單位\).md "wikilink")（Zm）{{·w}}
[艾米](../Page/艾米.md "wikilink")（Em）{{·w}}[拍米](../Page/拍米.md "wikilink")（Pm）{{·w}}[-{zh-hans:太米;zh-hk:太米;zh-tw:兆米;}-](../Page/垓米.md "wikilink")（Tm）{{·w}}[吉米](../Page/京米.md "wikilink")（Gm）{{·w}}[-{zh-hans:兆米;zh-hk:兆米;zh-tw:百萬米;}-](../Page/百万米.md "wikilink")（Mm）{{·w}}
[-{zh-hans:千米;zh-hk:公里;zh-tw:公里;}-](../Page/千米.md "wikilink")（km）{{·w}}
[-{zh-hans:百米;zh-hk:百米;zh-tw:公引;}-](../Page/百米.md "wikilink")（hm）{{·w}}
[-{zh-hans:十米;zh-hk:十米;zh-tw:公丈;}-](../Page/十米.md "wikilink")（dam）{{·w}}[-{zh-hans:分米;zh-hk:分米;zh-tw:公寸;}-](../Page/分米.md "wikilink")（dm）{{·w}}[-{zh-hans:厘米;zh-hk:厘米;zh-tw:公分;}-](../Page/厘米.md "wikilink")（cm）{{·w}}
[-{zh-hans:毫米;zh-hk:毫米;zh-tw:公釐;}-](../Page/毫米.md "wikilink")（mm）{{·w}}[絲米](../Page/絲米.md "wikilink")（dmm）{{·w}}[忽米](../Page/忽米.md "wikilink")（cmm）{{·w}}[微米](../Page/微米.md "wikilink")（μm）{{·w}}
[-{zh-hans:纳米;zh-hk:納米;zh-tw:奈米;}-](../Page/纳米.md "wikilink")（nm）{{·w}}[皮米（微微米）](../Page/皮米.md "wikilink")（pm）{{·w}}[飞米（费米）](../Page/飛米.md "wikilink")（fm）{{·w}}
[阿米](../Page/阿米.md "wikilink")（am）{{·w}}
[-{zh-hans:仄米;zh-hk:仄米;zh-tw:介米;}-](../Page/介米.md "wikilink")（zm）{{·w}}
[-{zh-hans:幺米;zh-hk:么米;zh-tw:攸米;}-](../Page/攸米.md "wikilink")（ym）

|group2 = [市制](../Page/市制.md "wikilink") |list2 =
[里{{·w}}引{{·w}}丈{{·w}}尺{{·w}}寸{{·w}}分{{·w}}-{zh-cn:厘;zh-tw:釐;}-{{·w}}毫](../Page/市制#长度.md "wikilink")

|group3 = [英制](../Page/英制单位.md "wikilink") |list3 =
[-{zh-hans:英里;zh-hk:英哩;zh-tw:英哩;}-（哩、咪）](../Page/英里.md "wikilink")（mi）{{·w}}-{[-{zh-hans:浪;zh-hant:化朗;}-](../Page/浪_\(量度單位\).md "wikilink")}-（furlong）{{·w}}[鏈](../Page/鏈_\(量度單位\).md "wikilink")（chain）{{·w}}[桿](../Page/桿.md "wikilink")（rod）{{·w}}[英寻（噚）](../Page/英寻.md "wikilink")（fathom）{{·w}}[码](../Page/码.md "wikilink")（yd）{{·w}}[-{zh-hans:英尺;zh-hk:英呎;zh-tw:英呎;}-（呎）](../Page/英尺.md "wikilink")（ft）{{·w}}[-{zh-hans:英寸;zh-hk:英吋;zh-tw:英吋;}-（吋）](../Page/英寸.md "wikilink")（in）

|group4 = [东亚传统](../Page/东亚传统计量体系.md "wikilink") |list4 =
[堂](../Page/堂_\(長度單位\).md "wikilink"){{·w}}[里](../Page/里.md "wikilink"){{·w}}[引](../Page/引.md "wikilink"){{·w}}[丈](../Page/丈.md "wikilink"){{·w}}[仞](../Page/仞.md "wikilink"){{·w}}[尺、咫](../Page/尺.md "wikilink"){{·w}}[寸](../Page/寸.md "wikilink"){{·w}}[分](../Page/分_\(尺貫法\).md "wikilink"){{·w}}[-{zh-cn:厘;zh-tw:釐;}-](../Page/厘_\(长度单位\).md "wikilink")（換算另見[度量衡](../Page/度量衡.md "wikilink")）

|group5 = [天文學](../Page/天文學.md "wikilink") |list5 =
[秒差距](../Page/秒差距.md "wikilink")（pc）{{·w}}[光年](../Page/光年.md "wikilink")（ly）{{·w}}[天文單位](../Page/天文單位.md "wikilink")（AU）{{·w}}[月球距離](../Page/月球距離.md "wikilink")（LD）

|group6 = [台制](../Page/台制.md "wikilink") |list6 =
[尺{{·w}}寸{{·w}}分{{·w}}厘](../Page/台制#長度.md "wikilink")

|group7 = [專屬單位](../Page/專屬單位.md "wikilink") |list7 =
[海里（浬）](../Page/海里.md "wikilink")（NM, nm, nmi, n
mile）{{·w}}[-{埃}-](../Page/埃格斯特朗_\(单位\).md "wikilink")（Å）{{·w}}[点](../Page/点_\(印刷\).md "wikilink")（pt）{{·w}}[派卡](../Page/派卡.md "wikilink")（pc）{{·w}}[條](../Page/條.md "wikilink")

|group8 = [自然單位](../Page/自然單位.md "wikilink") |list8
=[普朗克長度](../Page/普朗克長度.md "wikilink")

}}<noinclude> </noinclude>

[Category:單位模板](../Category/單位模板.md "wikilink")
[Category:科學與自然模板](../Category/科學與自然模板.md "wikilink")