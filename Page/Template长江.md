[当曲](当曲.md "wikilink")（南源） {{\!}} [楚玛尔河](楚玛尔河.md "wikilink")（北源）

|group2 = [金沙江](金沙江.md "wikilink")[水系](Template:金沙江水系.md "wikilink")
|list2 =

` `` `[`九龙河`](九龙河.md "wikilink")` {{!}} `[`理塘河`](理塘河.md "wikilink")` {{!}} `[`鲜水河`](鲜水河.md "wikilink")
` |group2 = 其他支流`
` |list2 = `[`普渡河`](普渡河.md "wikilink")` {{!}} `[`以礼河`](以礼河.md "wikilink")` {{!}} `[`牛栏江`](牛栏江.md "wikilink")` {{!}} `[`横江`](横江_\(金沙江支流\).md "wikilink")` {{!}} `[`松麦河`](松麦河.md "wikilink")
` }}`

|group3 = [长江上游水系](Template:长江上游水系.md "wikilink") |list3 =

` `` `[`麻尔柯河`](麻尔柯河.md "wikilink")` {{!}} `[`绰斯甲河`](绰斯甲河.md "wikilink")
`   |group2 = 其他支流`
`   |list2 = `[`黑水河`](黑水河_\(岷江支流\).md "wikilink")` {{!}} `[`马边河`](马边河.md "wikilink")
`   }}`
` |group2 = `[`乌江水系`](乌江.md "wikilink")
` |list2 =  `[`六冲河`](六冲河.md "wikilink")` {{!}} `[`芙蓉江`](芙蓉江.md "wikilink")` {{!}} `[`濯河`](濯河.md "wikilink")` {{!}} `[`郁江`](郁江_\(乌江支流\).md "wikilink")` {{!}} `[`清水江`](清水江.md "wikilink")`{{!}} `[`洪渡河`](洪渡河.md "wikilink")

` |group3 = `[`嘉陵江水系`](嘉陵江.md "wikilink")
` |list3 =  `[`白龙江`](白龙江.md "wikilink")` - `[`白水江`](白水江.md "wikilink")` {{!}} `[`渠江`](渠江_\(嘉陵江支流\).md "wikilink")` - `[`大通江`](大通江.md "wikilink")` {{!}} `[`东河`](东河_\(嘉陵江支流\).md "wikilink")` {{!}} `[`涪江`](涪江.md "wikilink")` {{!}} `
` |group4 = `[`沱江水系`](沱江.md "wikilink")
` |list4 = `[`绵远河`](绵远河.md "wikilink")` {{!}} `[`湔江`](湔江_\(沱江西源\).md "wikilink")`-`[`鸭子河`](鸭子河.md "wikilink")` {{!}}  `[`石亭江`](石亭江.md "wikilink")` `
` |group5 = 其他支流`
` |list5 =  `[`小江水系`](小江.md "wikilink")` {{!}}  `[`赤水河`](赤水河.md "wikilink")` {{!}} `[`綦江`](綦江.md "wikilink")` {{!}} `[`南广河`](南广河.md "wikilink")` {{!}} `[`御臨河`](御臨河.md "wikilink")` `

` }}`

|group4 = [长江中游水系](Template:长江中游水系.md "wikilink") |list4 =

` `

|group5 = [长江下游水系](Template:长江下游水系.md "wikilink") |list5 =

` `

}} <noinclude>

</noinclude>

[Category:长江水系模板](../Category/长江水系模板.md "wikilink")