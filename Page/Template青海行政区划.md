[城西区](../Page/城西区.md "wikilink"){{.w}}[城北区](../Page/城北区_\(西宁市\).md "wikilink"){{.w}}[湟中县](../Page/湟中县.md "wikilink"){{.w}}[湟源县](../Page/湟源县.md "wikilink"){{.w}}[大通回族土族自治县](../Page/大通回族土族自治县.md "wikilink")

|group3 = [海东市](../Page/海东市.md "wikilink") |list3 =
[乐都区](../Page/乐都区.md "wikilink"){{.w}}[平安区](../Page/平安区.md "wikilink"){{.w}}[民和回族土族自治县](../Page/民和回族土族自治县.md "wikilink"){{.w}}[互助土族自治县](../Page/互助土族自治县.md "wikilink"){{.w}}[化隆回族自治县](../Page/化隆回族自治县.md "wikilink"){{.w}}[循化撒拉族自治县](../Page/循化撒拉族自治县.md "wikilink")
}}

|group3style = text-align: center; |group3 =
[自治州](../Page/自治州.md "wikilink") |list3 =
[祁连县](../Page/祁连县.md "wikilink"){{.w}}[刚察县](../Page/刚察县.md "wikilink"){{.w}}[门源回族自治县](../Page/门源回族自治县.md "wikilink")

|group2 = [黄南藏族自治州](../Page/黄南藏族自治州.md "wikilink") |list2 =
[同仁县](../Page/同仁县.md "wikilink"){{.w}}[尖扎县](../Page/尖扎县.md "wikilink"){{.w}}[泽库县](../Page/泽库县.md "wikilink"){{.w}}[河南蒙古族自治县](../Page/河南蒙古族自治县.md "wikilink")

|group3 = [海南藏族自治州](../Page/海南藏族自治州.md "wikilink") |list3 =
[共和县](../Page/共和县.md "wikilink"){{.w}}[同德县](../Page/同德县.md "wikilink"){{.w}}[贵德县](../Page/贵德县.md "wikilink"){{.w}}[兴海县](../Page/兴海县.md "wikilink"){{.w}}[贵南县](../Page/贵南县.md "wikilink")

|group4 = [果洛藏族自治州](../Page/果洛藏族自治州.md "wikilink") |list4 =
[玛沁县](../Page/玛沁县.md "wikilink"){{.w}}[班玛县](../Page/班玛县.md "wikilink"){{.w}}[甘德县](../Page/甘德县.md "wikilink"){{.w}}[达日县](../Page/达日县.md "wikilink"){{.w}}[久治县](../Page/久治县.md "wikilink"){{.w}}[玛多县](../Page/玛多县.md "wikilink")

|group5 = [玉树藏族自治州](../Page/玉树藏族自治州.md "wikilink") |list5 =
[玉树市](../Page/玉树市.md "wikilink"){{.w}}[杂多县](../Page/杂多县.md "wikilink"){{.w}}[称多县](../Page/称多县.md "wikilink"){{.w}}[治多县](../Page/治多县.md "wikilink"){{.w}}[囊谦县](../Page/囊谦县.md "wikilink"){{.w}}[曲麻莱县](../Page/曲麻莱县.md "wikilink")

|group6 = [海西蒙古族藏族自治州](../Page/海西蒙古族藏族自治州.md "wikilink") |list6 =
[德令哈市](../Page/德令哈市.md "wikilink"){{.w}}[格尔木市](../Page/格尔木市.md "wikilink"){{.w}}[茫崖市](../Page/茫崖市.md "wikilink"){{.w}}[乌兰县](../Page/乌兰县.md "wikilink"){{.w}}[都兰县](../Page/都兰县.md "wikilink"){{.w}}[天峻县](../Page/天峻县.md "wikilink"){{.w}}[大柴旦行政委员会](../Page/大柴旦行政委员会.md "wikilink")\*
}}

|belowstyle = text-align: left; font-size: 80%; |below =
注：带“\*”属于地方设立的县级[行政管理区](../Page/行政管理区.md "wikilink")，并非[中华人民共和国民政部在册的](../Page/中华人民共和国民政部.md "wikilink")[行政区](../Page/中华人民共和国行政区划.md "wikilink")。
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[青海省乡级以上行政区列表](../Page/青海省乡级以上行政区列表.md "wikilink")。
}}<noinclude>

</noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[\*](../Category/青海行政区划.md "wikilink")
[青海行政区划模板](../Category/青海行政区划模板.md "wikilink")