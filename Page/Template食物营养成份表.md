<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p><strong>柿子</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>营养成份表</strong><br />
<sub><strong>每100<a href="../Page/克.md" title="wikilink">克食物中</a></strong></sub></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/总热量.md" title="wikilink">总热量</a><br />
 - <a href="../Page/脂肪.md" title="wikilink">脂肪</a><br />
 - <a href="../Page/碳水化合物.md" title="wikilink">碳水化合物</a><br />
 - <a href="../Page/蛋白质.md" title="wikilink">蛋白质</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/蛋白质.md" title="wikilink">蛋白质</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/总脂肪.md" title="wikilink">总脂肪</a><br />
 - <a href="../Page/饱和脂肪酸.md" title="wikilink">饱和脂肪酸</a><br />
 - <a href="../Page/多不饱和脂肪酸.md" title="wikilink">多不饱和脂肪酸</a><br />
 - <a href="../Page/单不饱和脂肪酸.md" title="wikilink">单不饱和脂肪酸</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/碳水化合物.md" title="wikilink">碳水化合物</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/无机盐.md" title="wikilink">无机盐</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/常量元素.md" title="wikilink">常量元素</a><br />
 - <a href="../Page/鈣營養.md" title="wikilink">鈣質</a><br />
 - <a href="../Page/磷營養.md" title="wikilink">磷質</a><br />
 - <a href="../Page/镁.md" title="wikilink">镁</a><br />
 - <a href="../Page/钠.md" title="wikilink">钠</a><br />
 - <a href="../Page/钾.md" title="wikilink">钾</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/微量元素.md" title="wikilink">微量元素</a><br />
 - <a href="../Page/铁.md" title="wikilink">铁</a><br />
 - <a href="../Page/锌.md" title="wikilink">锌</a><br />
 - <a href="../Page/碘.md" title="wikilink">碘</a><br />
 - <a href="../Page/硒.md" title="wikilink">硒</a><br />
 - <a href="../Page/钾.md" title="wikilink">钾</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/维生素A.md" title="wikilink">维生素A</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/维生素B.md" title="wikilink">维生素B</a><br />
 - <a href="../Page/维生素B1.md" title="wikilink">维生素B1</a><br />
 - <a href="../Page/维生素B2.md" title="wikilink">维生素B2</a><br />
 - <a href="../Page/维生素B3.md" title="wikilink">维生素B3</a><br />
 - <a href="../Page/维生素B12.md" title="wikilink">维生素B12</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/维生素C.md" title="wikilink">维生素C</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/维生素D.md" title="wikilink">维生素D</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/维生素E.md" title="wikilink">维生素E</a></p></td>
</tr>
</tbody>
</table>

<noinclude></noinclude>

[Category:生活模板](../Category/生活模板.md "wikilink")