{{ sidebar | name = 餐 | pretitle = 餐食系列 | title =
[餐](../Page/餐.md "wikilink") | titlestyle = font-size: 200% | image
=
![Floris_Claesz._van_Dyck_001.jpg](https://zh.wikipedia.org/wiki/File:Floris_Claesz._van_Dyck_001.jpg
"Floris_Claesz._van_Dyck_001.jpg") | imagestyle = padding-left:1em;
padding-right:1em | headingstyle = background-color: \#f4eea1;
padding-top: 0 | contentclass = hlist | width = 200px | heading1 = 餐 |
content1 =

  - [早餐](../Page/早餐.md "wikilink")

  -
  - [上午茶](../Page/上午茶.md "wikilink")

  - [早午餐](../Page/早午餐.md "wikilink")

  - [午餐](../Page/午餐.md "wikilink")

  -
  - [下午茶](../Page/下午茶.md "wikilink")

  - [晚餐](../Page/晚餐.md "wikilink")

  -
  -
  -
  - [零食](../Page/零食.md "wikilink")

  -
| heading2 = 組成及菜 | content2 =

  - **

  -
  - *[前菜](../Page/前菜.md "wikilink")*

  - [甜品](../Page/甜品.md "wikilink")

  -
  -
  - [主菜](../Page/主菜.md "wikilink")

  -
  - [小菜](../Page/小菜.md "wikilink")

| heading3 = 相關條目 | content3 =

  - **

  - [宴會](../Page/宴會.md "wikilink")

  -
  - [自助餐](../Page/自助餐.md "wikilink")

  - [料理](../Page/料理.md "wikilink")

      -
  - [饮料](../Page/饮料.md "wikilink")

  - [進食](../Page/進食.md "wikilink")

  - [食物](../Page/食物.md "wikilink")

  -
  -
  - *[定食](../Page/定食.md "wikilink")*

  - [餐桌禮儀](../Page/餐桌禮儀.md "wikilink")

  - [小吃](../Page/小吃.md "wikilink")

}}<includeonly>{{\#ifeq:|nocat||}}</includeonly><noinclude>

## 使用方法

本模板会自动把引用它的页面加入分类。引用本模板时使用下列语法，可以避免加入分类：<small>（注意nocat必须为英文小写）</small>

{{|nocat}}

</noinclude>

[Category:餐](../Category/餐.md "wikilink")
[\*](../Category/餐.md "wikilink")
[Category:飲食導航模板](../Category/飲食導航模板.md "wikilink")