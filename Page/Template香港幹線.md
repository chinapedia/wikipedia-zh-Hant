{{navbox |name = 香港幹線 |title = [香港幹線](香港幹線編號系統.md "wikilink")

|bodyclass = hlist |state =  |image = |group1 = 南北走廊 |list1 =

  - ['''\<span style="color:\#{{hkroutecolor](香港1號幹線.md "wikilink")
  - ['''\<span style="color:\#{{hkroutecolor](香港2號幹線.md "wikilink")
  - ['''\<span style="color:\#{{hkroutecolor](香港3號幹線.md "wikilink")
  - ['''\<span style="color:\#{{hkroutecolor](香港10號幹線.md "wikilink")

|group2 = 東西走廊 |list2 =

  - ['''\<span style="color:\#{{hkroutecolor](香港4號幹線.md "wikilink")
  - ['''\<span style="color:\#{{hkroutecolor](香港5號幹線.md "wikilink")
  - ['''\<span style="color:\#{{hkroutecolor](香港7號幹線.md "wikilink")
  - ['''\<span style="color:\#{{hkroutecolor](香港8號幹線.md "wikilink")

|group3 = 新界環迴幹線 |list3 =

  - ['''\<span style="color:\#{{hkroutecolor](香港9號幹線.md "wikilink")

|group4 = 興建中 |list4 =

  - [\<span style="color:\#{{hkroutecolor](香港6號幹線.md "wikilink")

|group5 = 計劃中 |list5 = \* \[\[香港11號幹線|\<span
style="color:\#<includeonly></includeonly> <noinclude></noinclude>

[Category:香港幹線](../Category/香港幹線.md "wikilink")
[Category:香港幹線模板](../Category/香港幹線模板.md "wikilink")