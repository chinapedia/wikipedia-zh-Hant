[前香港總督府](前香港總督府.md "wikilink")（今[香港禮賓府](香港禮賓府.md "wikilink")）{{.w}}[英皇書院](英皇書院.md "wikilink"){{.w}}[香港大學本部大樓外部](香港大學本部大樓.md "wikilink"){{.w}}[香港大學鄧志昂樓外部](香港大學鄧志昂樓.md "wikilink")（今[香港大學亞洲研究中心](香港大學亞洲研究中心.md "wikilink")）{{.w}}[香港大學孔慶熒樓外部](香港大學孔慶熒樓.md "wikilink"){{.w}}[前域多利監獄](前域多利監獄.md "wikilink"){{.w}}[堅巷舊病理學院](堅巷舊病理學院.md "wikilink")（今[香港醫學博物館](香港醫學博物館.md "wikilink")）{{.w}}[梅夫人婦女會主樓外部](梅夫人婦女會主樓.md "wikilink"){{.w}}[都爹利街石階及煤氣路燈](都爹利街.md "wikilink"){{.w}}[聖士提反女子中學主樓](聖士提反女子中學.md "wikilink"){{.w}}[聖約翰座堂](聖約翰座堂_\(香港\).md "wikilink"){{.w}}[聖若瑟書院北座及西座](聖若瑟書院.md "wikilink"){{.w}}[舊三軍司令官邸](舊三軍司令官邸.md "wikilink")（今[茶具文物館](茶具文物館.md "wikilink")）{{.w}}[舊上環街市](舊上環街市.md "wikilink")（今[西港城](西港城.md "wikilink")）{{.w}}[舊中區警署](舊中區警署.md "wikilink"){{.w}}[舊最高法院外部](舊最高法院.md "wikilink")（今[終審法院大樓](香港終審法院.md "wikilink")）{{.w}}[舊總督山頂別墅守衛室](舊總督山頂別墅守衛室.md "wikilink"){{.w}}[青洲燈塔建築群](青洲燈塔.md "wikilink"){{.w}}[甘棠第](孫中山紀念館_\(香港\).md "wikilink"){{.w}}[和平紀念碑](和平紀念碑_\(香港\).md "wikilink"){{.w}}[文武廟](東華三院文武廟.md "wikilink"){{.w}}[舊精神病院正立面](西營盤社區綜合大樓.md "wikilink")

|group2 = **[東區](東區_\(香港\).md "wikilink")** |list2 =
[柴灣羅屋](柴灣羅屋.md "wikilink")（今[羅屋民俗館](羅屋民俗館.md "wikilink")）{{.w}}[銅鑼灣天后廟](銅鑼灣天后廟.md "wikilink"){{.w}}[鯉魚門軍營第](鯉魚門軍營.md "wikilink")7、10及25座

|group3 = **[灣仔區](灣仔區.md "wikilink")** |list3 =
[舊灣仔郵政局](舊灣仔郵政局.md "wikilink"){{.w}}[景賢里](景賢里.md "wikilink"){{.w}}[東蓮覺苑](東蓮覺苑.md "wikilink"){{.w}}[蓮花宮](蓮花宮.md "wikilink"){{.w}}[馬場先難友紀念碑](馬棚先難友紀念碑.md "wikilink")

|group4 = **[南區](南區_\(香港\).md "wikilink")** |list4 =
[大浪灣石刻](大浪灣石刻.md "wikilink"){{.w}}[香港大學大學堂外部](香港大學大學堂.md "wikilink"){{.w}}[黃竹坑石刻](黃竹坑石刻.md "wikilink"){{.w}}[舊赤柱警署](舊赤柱警署.md "wikilink"){{.w}}[鶴咀燈塔](鶴咀燈塔.md "wikilink"){{.w}}[聖士提反書院書院大樓](聖士提反書院.md "wikilink"){{.w}}[薄扶林水塘](薄扶林水塘.md "wikilink")6項歷史構築物{{.w}}[大潭水塘](大潭水塘.md "wikilink")22項歷史構築物{{.w}}[黃泥涌水塘](黃泥涌水塘.md "wikilink")3項歷史構築物{{.w}}[香港仔水塘](香港仔水塘.md "wikilink")4項歷史構築物{{.w}}[伯大尼修院](伯大尼修院.md "wikilink"){{.w}}[鴨脷洲洪聖廟](鴨脷洲洪聖廟.md "wikilink")

}}

|list2 =
[前九龍英童學校](前九龍英童學校.md "wikilink"){{.w}}[九龍佑寧堂](九龍佑寧堂.md "wikilink"){{.w}}[前九廣鐵路鐘樓](尖沙咀鐘樓.md "wikilink"){{.w}}[香港天文台總部](香港天文台總部.md "wikilink"){{.w}}[東華三院文物館](東華三院文物館.md "wikilink"){{.w}}[大包米訊號塔](訊號山.md "wikilink")

|group2 = **[深水埗區](深水埗區.md "wikilink")** |list2 =
[李鄭屋漢墓](李鄭屋漢墓.md "wikilink")（今[李鄭屋漢墓博物館](李鄭屋漢墓博物館.md "wikilink")）

|group3 = **[九龍城區](九龍城區.md "wikilink")** |list3 =
[九龍寨城南門遺蹟](九龍寨城.md "wikilink"){{.w}}前[九龍寨城衙門](九龍寨城.md "wikilink"){{.w}}[瑪利諾修院學校](瑪利諾修院學校.md "wikilink"){{.w}}[侯王古廟](九龍城侯王廟.md "wikilink")

|group4 = **[黃大仙區](黃大仙區.md "wikilink")** |list4 = -

|group5 = **[觀塘區](觀塘區.md "wikilink")** |list5 = -

}}

|list3 = [九龍水塘](九龍水塘.md "wikilink")5項歷史構築物

|group2 = **[西貢區](西貢區.md "wikilink")** |list2 =
[大廟灣刻石](大廟灣刻石.md "wikilink"){{.w}}[上窰村](上窰村.md "wikilink"){{.w}}[滘西洲洪聖古廟](滘西洲洪聖古廟.md "wikilink"){{.w}}[佛頭洲稅關遺址](佛頭洲稅關遺址.md "wikilink"){{.w}}[東龍洲炮台](東龍洲炮台.md "wikilink"){{.w}}[東龍洲石刻](東龍洲石刻.md "wikilink"){{.w}}[滘西洲石刻](滘西洲石刻.md "wikilink"){{.w}}[龍蝦灣石刻](龍蝦灣石刻.md "wikilink")

|group3 = **[元朗區](元朗區.md "wikilink")** |list3 =
[錦田](錦田.md "wikilink")[二帝書院](二帝書院.md "wikilink"){{.w}}[山廈村張氏宗祠](山廈村張氏宗祠.md "wikilink"){{.w}}[屏山](屏山_\(香港\).md "wikilink")[愈喬二公祠](愈喬二公祠.md "wikilink"){{.w}}屏山[聚星樓](聚星樓.md "wikilink"){{.w}}[屏山鄧氏宗祠](屏山鄧氏宗祠.md "wikilink"){{.w}}[廈村楊侯宮](廈村楊侯宮.md "wikilink"){{.w}}[新田](新田_\(香港\).md "wikilink")[大夫第](新田大夫第.md "wikilink"){{.w}}新田[麟峯文公祠](麟峯文公祠.md "wikilink"){{.w}}[橫洲](橫洲_\(元朗\).md "wikilink")[二聖宮](橫洲二聖宮.md "wikilink"){{.w}}[八鄉](八鄉.md "wikilink")[梁氏宗祠](元崗村梁氏宗祠.md "wikilink"){{.w}}八鄉[植桂書室](植桂書室.md "wikilink"){{.w}}[廈村鄧氏宗祠](廈村鄧氏宗祠.md "wikilink"){{.w}}[仁敦岡書室](仁敦岡書室.md "wikilink"){{.w}}[錦田](錦田.md "wikilink")[廣瑜鄧公祠](廣瑜鄧公祠.md "wikilink"){{.w}}[浪濯村39號](浪濯村39號.md "wikilink")[白泥碉堡](白泥碉堡.md "wikilink"){{.w}}[達德公所](達德公所.md "wikilink")

|group4 = **[屯門區](屯門區.md "wikilink")** |list4 =
[何福堂會所馬禮遜樓](何福堂會所.md "wikilink")（前[香港達德學院本部大樓](香港達德學院.md "wikilink")）

|group5 = **[荃灣區](荃灣區.md "wikilink")** |list5 =
[三棟屋村](三棟屋村.md "wikilink")（今[三棟屋博物館](三棟屋博物館.md "wikilink")）{{.w}}[汲水門](汲水門.md "wikilink")[燈籠洲燈塔](燈籠洲燈塔.md "wikilink"){{.w}}[海壩村民宅](賽馬會德華公園.md "wikilink")

|group6 = **[葵青區](葵青區.md "wikilink")** |list6 =
[城門水塘紀念碑](城門水塘.md "wikilink")

|group7 = **[大埔區](大埔區.md "wikilink")** |list7 =
[文武二帝廟](文武二帝廟.md "wikilink"){{.w}}[上碗窰樊仙宮](上碗窰樊仙宮.md "wikilink"){{.w}}[碗窰村碗窰窰址](大埔碗窰窰址.md "wikilink"){{.w}}[舊北區理民府](舊北區理民府.md "wikilink"){{.w}}[前政務司官邸](前政務司官邸.md "wikilink"){{.w}}[敬羅家塾](敬羅家塾.md "wikilink"){{.w}}[舊大埔墟火車站](舊大埔墟火車站.md "wikilink")（今[香港鐵路博物館](香港鐵路博物館.md "wikilink")）

|group8 = **[北區](北區_\(香港\).md "wikilink")** |list8 =
[上水](上水.md "wikilink")[河上鄉](河上鄉.md "wikilink")[居石侯公祠](居石侯公祠.md "wikilink"){{.w}}[上水](上水.md "wikilink")[上水鄉](上水鄉.md "wikilink")[廖萬石堂](廖萬石堂.md "wikilink"){{.w}}[粉嶺龍躍頭天后宮](粉嶺龍躍頭天后宮.md "wikilink"){{.w}}[打鼓嶺](打鼓嶺.md "wikilink")[坪輋](坪輋.md "wikilink")[長山古寺](長山古寺.md "wikilink"){{.w}}[龍躍頭](龍躍頭.md "wikilink")[老圍門樓及圍牆](老圍_\(龍躍頭\).md "wikilink"){{.w}}龍躍頭[松嶺鄧公祠](松嶺鄧公祠.md "wikilink"){{.w}}龍躍頭[麻笏圍門樓](麻笏圍.md "wikilink"){{.w}}龍躍頭[覲龍圍圍牆及更樓](新圍_\(龍躍頭\).md "wikilink"){{.w}}龍躍頭覲龍圍門樓{{.w}}[沙頭角](沙頭角.md "wikilink")[鏡蓉書屋](鏡蓉書屋.md "wikilink"){{.w}}[葉定仕故居](葉定仕故居.md "wikilink"){{.w}}[發達堂](發達堂.md "wikilink")

|group9 = **[離島區](離島區.md "wikilink")** |list9 =
[大嶼山](大嶼山.md "wikilink")[分流石圓環](分流石圓環.md "wikilink"){{.w}}大嶼山[分流炮台](分流炮台.md "wikilink"){{.w}}大嶼山[石壁石刻](石壁石刻.md "wikilink"){{.w}}大嶼山[東涌小炮台](東涌小炮台.md "wikilink"){{.w}}大嶼山[東涌炮台](東涌炮台.md "wikilink"){{.w}}[大澳楊侯古廟](大澳楊侯古廟.md "wikilink"){{.w}}[長洲石刻](長洲石刻.md "wikilink"){{.w}}[蒲台島石刻](蒲台島石刻.md "wikilink"){{.w}}[橫瀾島](橫瀾島.md "wikilink")[橫瀾燈塔](橫瀾燈塔.md "wikilink")

}}

|below = （截至2017年10月13日共**117**項）
以上並不包括「暫定古蹟」及即將成為法定古蹟但未刊憲作實的歷史建築。
參見：[香港一級歷史建築](香港一級歷史建築.md "wikilink")、[香港二級歷史建築](香港二級歷史建築.md "wikilink")、[香港三級歷史建築](香港三級歷史建築.md "wikilink")

}}<includeonly></includeonly><noinclude> </noinclude>

[Category:香港法定古蹟](../Category/香港法定古蹟.md "wikilink")
[\*](../Category/香港法定古蹟.md "wikilink")
[法](../Category/香港建築模板.md "wikilink")
[Category:香港文物模板](../Category/香港文物模板.md "wikilink")