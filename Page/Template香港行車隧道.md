[中環灣仔繞道](中環灣仔繞道.md "wikilink")<sup>1</sup> | group2 = 香港島至九龍 | list2 =
[紅磡海底隧道](海底隧道_\(香港\).md "wikilink")<sup>3</sup>{{·w}}[東區海底隧道](東區海底隧道.md "wikilink")<sup>3</sup>{{·w}}[西區海底隧道](西區海底隧道.md "wikilink")<sup>2</sup>
| group3 = [九龍](九龍.md "wikilink") | list3 =
[啟德隧道](啟德隧道.md "wikilink")<sup>1</sup>{{·w}}*[中九龍幹線](中九龍幹線.md "wikilink")*<small>（興建中）</small><sup>1</sup>{{·w}}*[東南九龍T2主幹道](東南九龍T2主幹道.md "wikilink")*<small>（興建中）</small>
| group4 = 九龍至新界 | list4 =
[獅子山隧道](獅子山隧道.md "wikilink"){{·w}}[大老山隧道](大老山隧道.md "wikilink")<sup>3</sup>{{·w}}[將軍澳隧道](將軍澳隧道.md "wikilink"){{·w}}[尖山隧道](尖山隧道及沙田嶺隧道.md "wikilink")<sup>7</sup>{{·w}}*[將軍澳－藍田隧道](將軍澳－藍田隧道.md "wikilink")*<small>（興建中）</small>
| group5 = [新界](新界.md "wikilink") | list5 =
[長青隧道](長青隧道.md "wikilink")<sup>5</sup>{{·w}}[大欖隧道](大欖隧道.md "wikilink")<sup>2</sup>{{·w}}[城門隧道](城門隧道.md "wikilink"){{·w}}[愉景灣隧道](愉景灣隧道.md "wikilink")<sup>8</sup>{{·w}}[大圍隧道](大圍隧道.md "wikilink")<sup>7</sup>{{·w}}[沙田嶺隧道](尖山隧道及沙田嶺隧道.md "wikilink")<sup>7</sup>{{·w}}[南灣隧道](南灣隧道.md "wikilink")<sup>6</sup>{{·w}}*[屯門至赤鱲角連接路](屯門至赤鱲角連接路.md "wikilink")*<small>（興建中）</small>{{·w}}[港珠澳大橋海底隧道段](港珠澳大橋.md "wikilink")<sup>4、9</sup>{{·w}}[機場隧道](機場隧道.md "wikilink")<sup>1</sup>{{·w}}[觀景山隧道](觀景山隧道.md "wikilink")<sup>9</sup>{{·w}}*[屯門西繞道](屯門西繞道.md "wikilink")*<small>（籌劃中）</small>{{·w}}*[龍山隧道](龍山隧道.md "wikilink")*<sup>1</sup><small>（興建中）</small>{{·w}}*[長山隧道](長山隧道.md "wikilink")*<sup>1</sup><small>（興建中）</small>
| below = 備註：
1：免費隧道　2：[民間興建營運後轉移模式](民間興建營運後轉移模式.md "wikilink")（BOT）　3：BOT專營權已經屆滿，擁有及管理權已經移交[政府](香港特別行政區政府.md "wikilink")
4：在香港境外水域，香港特區政府有份投資興建　5：屬[青馬管制區管理之免費隧道](青馬管制區.md "wikilink")　6：屬[青沙管制區管理之免費隧道](青沙管制區.md "wikilink")
7：屬[青沙管制區管理之收費隧道](青沙管制區.md "wikilink")　8：私人收費行車隧道　9：跨境行車隧道，邊境禁區範圍 }}
<noinclude> </noinclude>

[Category:香港建築模板](../Category/香港建築模板.md "wikilink")
[隧](../Category/香港交通導航模板.md "wikilink")