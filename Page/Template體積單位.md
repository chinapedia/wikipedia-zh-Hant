[市斗](市制#容积.md "wikilink"){{·}} [市升](市制#容积.md "wikilink"){{·}}
[市合](市制#容积.md "wikilink") - [勺](市制#容积.md "wikilink"){{·}}
[撮](市制#容积.md "wikilink")

| group2 = [東亞古制](东亚传统计量体系.md "wikilink") | list2 =
[石](石_\(容量單位\).md "wikilink"){{·}} [斛](斛.md "wikilink"){{·}}
[斗](斗.md "wikilink"){{·}}
-{[升](升.md "wikilink")}-（換算見[中国度量衡](中国度量衡.md "wikilink")）

| group3 = [英-{制}-](英制单位.md "wikilink")、[美-{制}-](美制单位.md "wikilink") |
list3 = （mi<sup>3</sup>）{{·}} （acre·ft）{{·}} （yd<sup>3</sup>）{{·}}
[立方英尺](立方英尺.md "wikilink")（ft<sup>3</sup>）{{·}}
[立方英寸](立方英寸.md "wikilink")（in<sup>3</sup>）
[桶](桶_\(單位\).md "wikilink")（bbl）{{·}} [蒲式耳](蒲式耳.md "wikilink")（bsh）{{·}}
（kenning）{{·}} [配克](配克.md "wikilink")（peck）{{·}}
[-{夸}-脫](夸脫.md "wikilink")（qt）{{·}}
[加仑](加仑.md "wikilink")（gal）{{·}}
[品脫](品脫.md "wikilink")（pt）{{·}}
[及耳](及耳.md "wikilink")（gill）{{·}}
[液量盎司](液量盎司.md "wikilink")（fl oz）{{·}}
[液量打兰](液量打兰.md "wikilink")（fl dr）{{·}}
[液量吩](液量吩.md "wikilink")（fl scruple）{{·}} （minim）

| list5 =

| list6=

|list8 =

}}<noinclude>

</noinclude>

[Category:單位模板](../Category/單位模板.md "wikilink")
[Category:科學與自然模板](../Category/科學與自然模板.md "wikilink")