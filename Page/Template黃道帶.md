<table>
<thead>
<tr class="header">
<th><p>|-</p></th>
<th><p><a href="占星術.md" title="wikilink">占星術</a> | <a href="黃道十二宮.md" title="wikilink">黃道十二宮</a> |-</p></th>
<th><p><a href="白羊宮.md" title="wikilink">白羊宮</a></p></th>
<th><p><a href="金牛宮.md" title="wikilink">金牛宮</a></p></th>
<th><p><a href="雙子宮.md" title="wikilink">雙子宮</a></p></th>
<th><p><a href="巨蟹宮.md" title="wikilink">巨蟹宮</a></p></th>
<th><p><a href="獅子宮.md" title="wikilink">獅子宮</a></p></th>
<th><p><a href="室女宮.md" title="wikilink">室女宮</a></p></th>
<th><p><a href="天秤宮.md" title="wikilink">天秤宮</a></p></th>
<th><p><a href="天蠍宮.md" title="wikilink">天蠍宮</a></p></th>
<th><p><a href="人馬宮.md" title="wikilink">人馬宮</a></p></th>
<th><p><a href="摩羯宮.md" title="wikilink">摩羯宮</a></p></th>
<th><p><a href="寶瓶宮.md" title="wikilink">寶瓶宮</a></p></th>
<th><p><a href="雙魚宮.md" title="wikilink">雙魚宮</a> |- colspan="10"</p></th>
<th><p><a href="圖像:Aries.svg.md" title="wikilink">20px</a></p></th>
<th><p><a href="圖像:Taurus.svg.md" title="wikilink">20px</a></p></th>
<th><p><a href="圖像:Gemini.svg.md" title="wikilink">20px</a></p></th>
<th><p><a href="圖像:Cancer.svg.md" title="wikilink">20px</a></p></th>
<th><p><a href="圖像:Leo.svg.md" title="wikilink">20px</a></p></th>
<th><p><a href="圖像:Virgo.svg.md" title="wikilink">20px</a></p></th>
<th><p><a href="圖像:Libra.svg.md" title="wikilink">20px</a></p></th>
<th><p><a href="圖像:Scorpio.svg.md" title="wikilink">20px</a></p></th>
<th><p><a href="圖像:Sagittarius.svg.md" title="wikilink">20px</a></p></th>
<th><p><a href="圖像:Capricorn.svg.md" title="wikilink">20px</a></p></th>
<th><p><a href="圖像:Aquarius.svg.md" title="wikilink">20px</a></p></th>
<th><p><a href="圖像:Pisces.svg.md" title="wikilink">20px</a> |-</p></th>
<th><p><a href="天文學.md" title="wikilink">天文學</a> | <a href="黄道.md" title="wikilink">黄道</a><a href="星座.md" title="wikilink">星座</a> |-</p></th>
<th><p><a href="白羊座.md" title="wikilink">白羊座</a></p></th>
<th><p><a href="金牛座.md" title="wikilink">金牛座</a></p></th>
<th><p><a href="雙子座.md" title="wikilink">雙子座</a></p></th>
<th><p><a href="巨蟹座.md" title="wikilink">巨蟹座</a></p></th>
<th><p><a href="獅子座.md" title="wikilink">獅子座</a></p></th>
<th><p><a href="室女座.md" title="wikilink">室女座</a></p></th>
<th><p><a href="天秤座.md" title="wikilink">天秤座</a></p></th>
<th><p><a href="天蝎座.md" title="wikilink">天蠍座</a></p></th>
<th><p><a href="人马座.md" title="wikilink">人马座</a></p></th>
<th><p><a href="摩羯座.md" title="wikilink">摩羯座</a></p></th>
<th><p><a href="寶瓶座.md" title="wikilink">寶瓶座</a></p></th>
<th><p><a href="雙魚座.md" title="wikilink">雙魚座</a> |-</p></th>
<th><div class="navbox-abovebelow">
<p>相關條目：<a href="黃道光.md" title="wikilink">黃道光</a>{{•}} <a href="天宮圖.md" title="wikilink">天宮圖</a> </span></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<noinclude>

</noinclude>

[Category:天文学模板](../Category/天文学模板.md "wikilink")