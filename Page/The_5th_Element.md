《**The 5th
Element**》為香港女歌手[關心妍的第五張個人音樂大碟](../Page/關心妍.md "wikilink")。大碟原定於2004年12月推出，但因印刷上出現錯誤而押後至2005年1月推出。

## 專輯曲目

**Disc 1**

1.  高竇（粵）
2.  終點（國）
3.  慘得過我（粵）
4.  太陽雨（國）
5.  JK25（粵）
6.  思念（國）
7.  來者不拒（粵）
8.  同桌的你（國）
9.  突然遺失（粵）
10. 貓咪（國）
11. 算（粵）

**Disc 2 (VCD)**

1.  慘得過我 MV
2.  好得過我 MV

## 參見

[關心妍](../Page/關心妍.md "wikilink")

## 相關網站

  - [關心妍所屬唱片公司 BMA Records 官方網站](http://www.bma.com.hk)

[Category:關心妍音樂專輯](../Category/關心妍音樂專輯.md "wikilink")
[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")
[Category:2005年音樂專輯](../Category/2005年音樂專輯.md "wikilink")
[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")