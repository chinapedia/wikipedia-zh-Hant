**The Code
Project**，是一個免費公開來源碼的程式設計網站，主要的使用者是Windows平台上的電腦程式設計人員。每一篇文章幾乎都附有來源碼（src）和例子（demo）下載。

## 語言

The Code Project 包含下列語言的文章與程式碼：

  - [C](../Page/C语言.md "wikilink")／[C++](../Page/C++.md "wikilink")（特別是
    [MFC](../Page/MFC.md "wikilink")）
  - [C\#](../Page/C_Sharp.md "wikilink")
  - [Visual Basic](../Page/Visual_Basic.md "wikilink")
  - [ASP](../Page/ASP.md "wikilink")
  - [AJAX](../Page/AJAX.md "wikilink")
  - [SQL](../Page/SQL.md "wikilink")

## 類似的網站

  - [Bytes(曾用名TheScripts)](../Page/Bytes\(曾用名TheScripts\).md "wikilink")
  - [CodeGuru](../Page/CodeGuru.md "wikilink")
  - [DevX](../Page/DevX.md "wikilink")
  - [Experts-Exchange](../Page/Experts-Exchange.md "wikilink")
  - [CodeComments](../Page/CodeComments.md "wikilink")
  - [Planet Source Code](../Page/Planet_Source_Code.md "wikilink")

## 外部連結

  - [The Code Project](http://www.codeproject.com)

[Category:網站](../Category/網站.md "wikilink")
[Category:社群網站](../Category/社群網站.md "wikilink")
[Category:计算机技术](../Category/计算机技术.md "wikilink")