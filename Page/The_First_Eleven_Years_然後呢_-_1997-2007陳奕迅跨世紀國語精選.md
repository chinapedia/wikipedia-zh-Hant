《**The First Eleven Years 然後呢 -
1997-2007陳奕迅跨世紀國語精選**》是[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")[陳奕迅](../Page/陳奕迅.md "wikilink")2008年[國語](../Page/國語.md "wikilink")[精選輯](../Page/精選輯.md "wikilink")，於2008年3月31日發行。

## 專輯簡介

這是陳奕迅加入[環球唱片後第一張](../Page/環球唱片.md "wikilink")[國語](../Page/國語.md "wikilink")[精選輯](../Page/精選輯.md "wikilink")，但當時陳奕迅經環球唱片出版的國語專輯只有兩張（《[怎麼樣](../Page/怎麼樣.md "wikilink")》及《[認了吧](../Page/認了吧.md "wikilink")》），連帶[粵語專輯](../Page/粵語.md "wikilink")《U87》附送的兩首國語歌曲，只有合共20首國語歌曲，推出國語精選輯根本是不夠數量。於是環球唱片特地購下了陳奕迅在[台灣的舊公司](../Page/台灣.md "wikilink")[艾迴及](../Page/艾迴.md "wikilink")[立得推出的國語歌曲的版權](../Page/立得.md "wikilink")，連同公司現有的國語歌曲，選出30首陳奕迅在10年內最令人難忘的國語歌曲，造成了這張「跨世紀」的國語精選輯。精選輯在台灣推出後反應非常理想，進佔台灣[G-music台灣風雲榜長達](../Page/G-music.md "wikilink")16週（12-27週），最高位置達第二位。

## 曲目

### Disc 1

1.  K歌之王
2.  十年
3.  淘汰
4.  愛是懷疑
5.  想哭
6.  聖誕結
7.  婚禮的祝福
8.  煙味
9.  對不起 謝謝
10. 好久不見
11. 低等動物
12. 我們都寂寞
13. 謝謝
14. 阿怪
15. 狂人日記

### Disc 2

1.  愛情轉移
2.  你的背包
3.  預感
4.  謝謝儂
5.  兄妹
6.  全世界失眠
7.  不如這樣
8.  要你的
9.  不睡
10. 紅玫瑰
11. 拔河
12. 不然你要我怎麼樣
13. 世界
14. 人造衛星
15. 醞釀

## 相關條目

[G-music](../Page/G-music.md "wikilink")

[Category:陳奕迅音樂專輯](../Category/陳奕迅音樂專輯.md "wikilink")
[Category:陳奕迅國語專輯](../Category/陳奕迅國語專輯.md "wikilink")
[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:2008年音樂專輯](../Category/2008年音樂專輯.md "wikilink")