**The
Iconfactory**是一家小型[軟體與圖像設計公司](../Page/軟體.md "wikilink")，專於製作[圖標與用來製作及使用圖標的軟體](../Page/圖標.md "wikilink")。The
Iconfactory由Corey Marion、Talos Tsui與Gedeon Maheux於1996年4月創立，首席工程師Craig
Hockenberry與藝術家Dave Brasgalla分別於1997年及1999年1月加入，並於2000年1月組成公司。The
Iconfactory透過提供免費下載的圖標套裝作品而獲得知名度，並迅速地發展為商業圖標設計的領導公司之一。The
Iconfactory亦有發行製作、組織與使用圖標的軟體以及一般的圖形用戶界面應用程式。

由1997年起至2004年為止The
Iconfactory每年會舉行一個名為Pixelpalooza的[麥金塔圖標設計比賽](../Page/麥金塔.md "wikilink")，讓全球熱心的藝術家設計與製作原創的圖標作品來贏取軟體或硬體的獎品。Pixelpalooza於2005年中止，但The
Iconfactory表示仍有再次舉辦的可能。

The
Iconfactory到現時為止最著名的委託專案為替[微軟製作超過](../Page/微軟.md "wikilink")100個包括在[Windows
XP操作系統的圖標](../Page/Windows_XP.md "wikilink")\[1\]，它們亦為[Xbox
360的](../Page/Xbox_360.md "wikilink")[圖形用戶界面與網站製作了超過](../Page/圖形用戶界面.md "wikilink")100個圖標\[2\]。

## 軟體

  - IconBuilder — 用於製作[圖標的](../Page/圖標.md "wikilink")[Adobe
    Photoshop](../Page/Adobe_Photoshop.md "wikilink")[插件](../Page/插件.md "wikilink")
  - CandyBar — 用來更改系統圖標，iControl的後繼者
  - Pixadex —
    與[iPhoto相似的圖標管理工具](../Page/iPhoto.md "wikilink")，IconDropper的後繼者
  - iPulse — 顯示系統效能的公用程式
  - xScope — 用來測量屏幕上任何要素的工具
  - Dine-O-Matic — [Dashboard小工具](../Page/Dashboard.md "wikilink")
  - DownloadCheck —
    靈感來自[木馬程式](../Page/木馬程式.md "wikilink")[MP3Concept的簡易公用程式](../Page/MP3Concept.md "wikilink")
  - [Twitterrific](../Page/Twitterrific.md "wikilink") —
    [社會性網路網站](../Page/社會性網路.md "wikilink")[Twitter的客户端](../Page/Twitter.md "wikilink")
  - IconDropper — [System 7](../Page/System_7.md "wikilink")、[Mac OS
    8與](../Page/Mac_OS_8.md "wikilink")[Mac OS
    9使用的圖標管理公用程式](../Page/Mac_OS_9.md "wikilink")
  - iControl — 用來更改任何[Mac OS 8與](../Page/Mac_OS_8.md "wikilink")[Mac OS
    9系統圖標的公用程式](../Page/Mac_OS_9.md "wikilink")

CandyBar與Pixadex由Panic公司維護，xScope由[ARTIS
Software維護](../Page/ARTIS_Software.md "wikilink")

## 参见

  - [圖標](../Page/圖標.md "wikilink")
  - [IconBuilder](../Page/IconBuilder.md "wikilink")
  - [Icon
    Composer](../Page/Apple_Developer_Tools#Icon_Composer.md "wikilink")

## 參考

## 外部連結

  - [The Iconfactory](http://www.iconfactory.com/)
  - [The Iconfactory 的 Twitter](http://twitter.com/iconfactory)

[Category:圖形用戶界面](../Category/圖形用戶界面.md "wikilink")
[Category:圖標](../Category/圖標.md "wikilink")
[Category:麥金塔軟體公司](../Category/麥金塔軟體公司.md "wikilink")
[Category:麥金塔網站](../Category/麥金塔網站.md "wikilink")
[Category:網站](../Category/網站.md "wikilink")

1.
2.