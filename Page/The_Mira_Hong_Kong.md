[Hotel_Miramar_Hong_Kong.JPG](https://zh.wikipedia.org/wiki/File:Hotel_Miramar_Hong_Kong.JPG "fig:Hotel_Miramar_Hong_Kong.JPG")
[The_Mira_Hong_Kong_Enterance_2010.jpg](https://zh.wikipedia.org/wiki/File:The_Mira_Hong_Kong_Enterance_2010.jpg "fig:The_Mira_Hong_Kong_Enterance_2010.jpg")
[The_Mira_Hong_Kong_Hotel_Lobby.jpg](https://zh.wikipedia.org/wiki/File:The_Mira_Hong_Kong_Hotel_Lobby.jpg "fig:The_Mira_Hong_Kong_Hotel_Lobby.jpg")

**The Mira Hong Kong**，舊稱**美麗華酒店**（ 及
），位處於[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[尖沙咀](../Page/尖沙咀.md "wikilink")[彌敦道](../Page/彌敦道.md "wikilink")118至130號，為[香港的一間](../Page/香港.md "wikilink")[酒店](../Page/酒店.md "wikilink")，1957年開幕，共提供492間客房，包括56間套房及8間特色套房。建築師為[朱彬](../Page/朱彬_\(香港人\).md "wikilink")。酒店由[美麗華酒店企業管理](../Page/美麗華酒店企業.md "wikilink")。美麗華酒店鄰近[港鐵](../Page/港鐵.md "wikilink")[荃灣綫](../Page/荃灣綫.md "wikilink")[尖沙咀站](../Page/尖沙咀站.md "wikilink")、[西鐵綫](../Page/西鐵綫.md "wikilink")[尖東站及](../Page/尖東站.md "wikilink")[中港碼頭](../Page/中港碼頭.md "wikilink")，往來[港鐵](../Page/港鐵.md "wikilink")[東鐵綫](../Page/東鐵綫.md "wikilink")／[西鐵綫](../Page/西鐵綫.md "wikilink")[紅磡站只需要](../Page/紅磡站.md "wikilink")15分鐘。酒店曾經有一座舊翼，於1988年拆卸，於1996年至1997年重建成今日的[美麗華商場](../Page/美麗華商場.md "wikilink")\[1\]。

## 歷史

美麗華酒店由已故香港富商[楊志雲等人於](../Page/楊志雲.md "wikilink")1957年創辦，為當時首間由香港人經營的高級酒店。1979年6月第二層地庫開設[楊氏蠟像院](../Page/楊氏蠟像院.md "wikilink")，不過蠟像被歹徒惡意破壞，該院在1979年12月停業。

到了1980年代，楊氏家族下一代無意繼續經營，便將股權賣給[李兆基](../Page/李兆基.md "wikilink")。到了2006年起，李家誠為上市公司美麗華酒店的董事總經理。在2007年酒店大翻新前，酒店地庫為Shooters
52美國餐廳；地下為公主吧 Princess Bar 及The
Atrium餐廳；一樓為東宮中菜館、三樓為[商務印書館](../Page/商務印書館.md "wikilink")。

## 翻新工程

2007年年中，逾50年歷史的尖沙咀美麗華酒店，斥資逾4億元翻新大革新，酒店重新命名為「The Mira」，走時尚型格路綫，全新首批120
間客房於2008年年中翻新完成。除配備電腦及娛樂二合一組合，更是首間酒店提供免費本地手提電話服務，一個電話接通酒店房間及戶外使用。

酒店內492
間客房主要分4種色調，有綠色、紅色和銀色，貴賓樓層用深紫色，套房更有白色選擇。每間房間都有名師ArenJacobsen設計的蛋形座椅，更配備500G藍光碟個人電腦及娛樂組合，透過40吋高清電視，可免費WiFi上網，看新聞、查詢航班資訊等。

酒店內的餐廳也進行大革新，地下大堂中庭The Atrium餐廳結業後，於2008年5月改建為Yamm日本菜餐廳，餐廳由Colin
Cowin設計。與一般酒店餐廳不同，自助餐設有點菜服務。大堂也設有Room One Bar &
Lounge酒吧及售賣自家製朱古力和咖啡專門店COCO專門店。

到了2009年，三樓[商務印書館原址改建為多個宴會室及國金軒](../Page/商務印書館.md "wikilink")。國金軒更於2010年列入[2011米芝蓮香港澳門指南新上榜的二星餐廳](../Page/2011米芝蓮香港澳門指南.md "wikilink")。

5樓設WHISK餐廳，並且可直接通往酒店5樓的露天花園酒廊Vibes。

<File:The> Mira Hong Kong Enterance.jpg|翻新前美麗華酒店入口 <File:Hotel> Miramar
Yamm Restaurant.jpg|Yamm日本菜餐廳 <File:The> Mira Hong Kong
COCO.jpg|COCO朱古力和咖啡專門店 (2008年) <File:The> Mira Hong Kong Room
One Bar & Lounge.jpg|Room One Bar & Lounge酒吧 <File:The> Mira Hong Kong
3rd-floor-Function-Rooms Enterance.jpg|三樓宴會室 <File:Cuisine> Cuisine at
the Mira.jpg|國金軒

## 美麗華廣場二期

[Mira_mall_Level_1_201205.jpg](https://zh.wikipedia.org/wiki/File:Mira_mall_Level_1_201205.jpg "fig:Mira_mall_Level_1_201205.jpg")
[Mira_mall_basement_shops_201210.jpg](https://zh.wikipedia.org/wiki/File:Mira_mall_basement_shops_201210.jpg "fig:Mira_mall_basement_shops_201210.jpg")
2010年，業主將地下舖位，物業地舖、地庫及1樓進行大翻新，並分間成3個大型舖位，吸引大品牌進駐，以提高租金收入，並命名為**mira
mall**。商場樓高四層，共10萬平方呎，於2012年4月對外開放，同年10月17日正式開幕。2017年5月再更名為**美麗華廣場二期**，英文為**Mira
Place
Two**。場內有約30間店舖，其中5間為旗艦店，佔地兩層或以上，每間面積介乎2,300至22,000平方呎不等。租戶包括約5,000平方呎的名牌皮具[Coach](../Page/Coach.md "wikilink")、
Le Sean Seasons Florist、佔地6,449平方呎的美國名牌時裝[Tommy
Hilfiger](../Page/Tommy_Hilfiger.md "wikilink")、逾1.5萬平方呎，景福珠寶旗下的Masterplece
by king fook、TWIST 的旗艦店及美容店等。其中名牌皮具Coach，月租280萬元，呎租約560元。

地庫面積約1.5萬平方呎，開設有日本年青服飾品牌[Collect
Point的概念店及集團旗下的niko](../Page/Collect_Point.md "wikilink")
and精品店，另設日式西餐廳wired
cafe，到2018年12月約滿結業。到2019年1月，日本連鎖百貨「[激安之殿堂](../Page/激安之殿堂.md "wikilink")」以每月約100萬元承租，呎租約67元。\[2\]

## 軼聞

### 斯諾登入住酒店

2013年5月20日，前[美國中央情報局](../Page/美國中央情報局.md "wikilink")（CIA）職員[爱德华·斯诺登滯留香港入住此酒店](../Page/爱德华·斯诺登.md "wikilink")，在此將秘密文件披露給《[衛報](../Page/衛報.md "wikilink")》並接受該報採訪。到6月10日，《衛報》的
Even MacAskill 稱，由於香港酒店昂貴，「他的信用卡很快就會超出使用限額」。斯諾登在當日已經退房。\[3\]\[4\]

### 鋁窗墜街 女清潔工被捕被指有爭議

2019年1月21日上午10時25分，酒店女清潔工在酒店內清潔窗戶時，鋁窗突然墮下，擊中由[佛山來港的一對男女遊客](../Page/佛山.md "wikilink")，其中24歲的女遊客頭部大量流血並且昏迷，被送往[伊利沙伯醫院搶救](../Page/伊利沙伯醫院.md "wikilink")，\[5\]女遊客經搶救後，於中午12時16分傷重不治。事後警方拘捕一名酒店39歲尼泊爾籍女清潔工，涉嫌容許物件從高處墮下。\[6\]\[7\][屋宇署根據](../Page/屋宇署.md "wikilink")《建築物條例》向酒店發出勘測命令，限令對方在一個月內委託合資格人員檢查所有窗戶，並提交報告及改善建議。\[8\]

飲食及酒店業職工總會認為責任不在女工，批評警方拘捕行動極不合理，對此感到驚訝。指出女清潔工只是按照上級指示去做，責任應在美麗華酒店。\[9\]而酒店發出聲明，對事件感到非常難過，向死者家屬致以深切慰問，表示會盡力提供協助。酒店正積極配合警方調查。\[10\]

## 圖集

<File:The> Mira Hong Kong at night.jpg|晚上的The Mira，右方建築物為[The
ONE](../Page/The_ONE.md "wikilink")

## 鄰近景點

  - [美麗華商場](../Page/美麗華商場.md "wikilink")
  - [香檳大廈](../Page/香檳大廈.md "wikilink")
  - [The ONE](../Page/The_ONE.md "wikilink")
  - [九龍公園](../Page/九龍公園.md "wikilink")
  - [聖安德烈堂](../Page/聖安德烈堂.md "wikilink")
  - 尖沙咀[清真寺](../Page/清真寺.md "wikilink")

## 途經之公共交通服務

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")：[尖沙咀站](../Page/尖沙咀站.md "wikilink")
  - <font color="{{西鐵綫色彩}}">█</font>[西鐵綫](../Page/西鐵綫.md "wikilink")：[尖東站](../Page/尖東站.md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

</div>

</div>

## 參看

  - [香港酒店列表](../Page/香港酒店列表.md "wikilink")

## 註釋

## 參考資料

## 外部連結

  - [The Mira Hong Kong 官方網站](http://www.themirahotel.com/)
  - [香港酒店網 The Mira Hong
    Kong](https://web.archive.org/web/20080914064550/http://www.hotel.com.hk/hotel.php?id=10056)

[Category:尖沙咀酒店](../Category/尖沙咀酒店.md "wikilink")
[Category:恒基兆業](../Category/恒基兆業.md "wikilink")
[Category:彌敦道](../Category/彌敦道.md "wikilink")

1.  [美麗華酒店企業里程碑](http://www.miramar-group.com/tc/The-Miramar-Group/Milestones.aspx)

2.
3.
4.
5.
6.
7.
8.
9.
10.