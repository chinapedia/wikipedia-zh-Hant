[MetalTypeZoomIn.JPG](https://zh.wikipedia.org/wiki/File:MetalTypeZoomIn.JPG "fig:MetalTypeZoomIn.JPG")
[The_quick_brown_fox_jumps_over_the_lazy_dog.png](https://zh.wikipedia.org/wiki/File:The_quick_brown_fox_jumps_over_the_lazy_dog.png "fig:The_quick_brown_fox_jumps_over_the_lazy_dog.png")
****（相对应的中文可以简短地翻译为“快狐跨懒狗”，完整翻译则是“敏捷的棕色狐狸跨过懒狗”）是一個著名的[英語](../Page/英語.md "wikilink")[全字母句](../Page/全字母句.md "wikilink")，常被用於測試[字體的顯示效果和](../Page/字體.md "wikilink")[鍵盤有沒有故障](../Page/鍵盤.md "wikilink")。此句也常以「」做為指代簡稱。

此句共35個[字母](../Page/字母.md "wikilink")，其中e重複3次，h重複2次，o重複4次，r重複2次，t重複2次（包含T），u重複2次，不過使用的都是常用的單詞，沒有使用[專有名詞](../Page/專有名詞.md "wikilink")。

在[Microsoft Office Word](../Page/Microsoft_Office_Word.md "wikilink")
2003中輸入「=rand()」命令，再按Enter鍵，會出現此句\[1\]，而Office
2007及更高版本則是「=rand.old()」命令\[2\]。

在[Microsoft Office Word](../Page/Microsoft_Office_Word.md "wikilink")
2016中輸入「=rand.old(1)」命令，再按Enter鍵，會出現「機會稍縱即逝。機會稍縱即逝。機會稍縱即逝。」

## 參考文獻

## 参见

  - [全字母句](../Page/全字母句.md "wikilink")
  - [英语](../Page/英语.md "wikilink")

[Category:印刷](../Category/印刷.md "wikilink")
[Category:语言](../Category/语言.md "wikilink")
[Category:長條目名](../Category/長條目名.md "wikilink")

1.
2.