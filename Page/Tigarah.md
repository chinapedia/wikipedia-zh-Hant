**Tigarah**（，出生於[日本](../Page/日本.md "wikilink")[東京](../Page/東京.md "wikilink")），是一隊來自日本的baile
funk司儀，在她們的音樂的元素中包含了grime、crunk和Baltimore
club。她能以[英語和](../Page/英語.md "wikilink")[日語來表演](../Page/日語.md "wikilink")，同時也在[美國和](../Page/美國.md "wikilink")[日本都有為數正在增長的追隨者](../Page/日本.md "wikilink")。她現在把時間分配在來往[洛杉磯和其家中](../Page/洛杉磯.md "wikilink")。*Tigarah*這個字，按照她的解釋為「很像老虎的女生」。

Tigarah從她在[慶應義塾大學的](../Page/慶應義塾大學.md "wikilink")[巴西同學中通曉baile](../Page/巴西.md "wikilink")
funk，一種巴西版本的Miami
Bass。主修[政治科學的她被這些音樂吸引了](../Page/政治學.md "wikilink")，所以她到訪[聖保羅和](../Page/聖保羅.md "wikilink")[里約熱內盧](../Page/里約熱內盧.md "wikilink")，學習多些有關這方面的知識，和融入巴西人的文化。留在里約熱內盧期間，她還遇上了Mr.
D，一個擁有[瑞士和](../Page/瑞士.md "wikilink")[德國血統的](../Page/德國.md "wikilink")[唱片騎師](../Page/唱片騎師.md "wikilink")／製作人，他經常來往當地和洛杉磯。Mr.
D現在是Tigarah的主要夥伴，他們創作了一張有8首歌的EP，現在於她的官方網站中發售。

雖然她沒有受到大型唱片公司的招倈，但是她已經在[美國國家公共廣播](../Page/美國國家公共廣播.md "wikilink")，和多本音樂雜誌中亮相，包括*[Blender
Magazine](../Page/Blender_Magazine.md "wikilink")*,
*[URB](../Page/URB.md "wikilink")*,
"[XLR8R](../Page/XLR8R.md "wikilink")", "[Dazed &
Confused](../Page/Dazed_&_Confused.md "wikilink")"和*[Los Angeles
Times](../Page/Los_Angeles_Times.md "wikilink")*。而她的音樂在電視節目"Numbers"中可以聽到，而遊戲[FIFA
2007和](../Page/FIFA_2007.md "wikilink")[極速快感10中也在其音樂箱中收錄她的作品](../Page/極速快感.md "wikilink")，還有商業用途的[手提電話鈴聲供應商Mobizzo](../Page/手提電話.md "wikilink")。她現在的知名度，已能和同樣靠網絡走紅的genre-bending司儀[M.I.A.相比](../Page/M.I.A..md "wikilink")。

## 作品列表

**EP**

歌曲列表：

1.  "Girl Fight\!"
2.  "Fake Out"
3.  "Japanese Queen"
4.  "Roppongi-Dori"
5.  "The Game In Rio"
6.  "Money"
7.  "Everything Is In Your Hand"
8.  "Fake Out" ([Mr. D](../Page/Mr._D_\(DJ\).md "wikilink") Rio Disco
    Mix)
9.  "Girl Fight\!" (Mr. D Hyphy Mix)

## 參考

  - Ely, Suzanne. [Tigarah: The politics of
    pop](http://www.urb.com/online/features/137_bsides.shtml)。URB
    Magazine。2006年6月19日出版，於2006年7月19日獲取
  - Inoue, Todd. [Ghetto
    Jam](http://www.metroactive.com/metro/04.12.06/tigarah-0615.html)（Tigarah專訪）。2006年4月12日出版，於2006年6月19日獲取

## 外部連結

  - [官方網站](https://web.archive.org/web/20170902195336/http://www.tigarah.net/)
  - [Tigarah在Myspace的頁面](http://www.myspace.com/tigarah)
  - [Youtube連結](http://www.youtube.com/user/tigarah)

[Category:日本音樂人](../Category/日本音樂人.md "wikilink")
[Category:慶應義塾大學校友](../Category/慶應義塾大學校友.md "wikilink")