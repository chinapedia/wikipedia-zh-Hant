『**Tomorrow's
way**』是日本唱作女歌手[YUI](../Page/YUI.md "wikilink")，於2005年6月22日所推出的單曲碟，並是在[Sony
Music Records旗下的第二張單曲碟](../Page/日本新力音樂.md "wikilink")。

## 收錄歌曲

1.  **Tomorrow's way**
      -
        作詞・作曲：YUI　編曲：　[弦樂團編曲](../Page/弦樂團.md "wikilink")：
        [松竹系電影](../Page/松竹.md "wikilink")「[HINOKIO](../Page/我愛奇諾奧.md "wikilink")（我愛奇諾奧）」的主題歌。後來[香港歌手](../Page/香港.md "wikilink")[薛凱琪翻唱了此曲](../Page/薛凱琪.md "wikilink")，歌名為「Dear
        Fiona」。
2.  **Last Train**
      -
        作詞・作曲：YUI　編曲：鈴木Daichi秀行
3.  **feel my soul～YUI Acoustic Version～**
      -
        每張新單曲會收錄前一單曲的Acoustic Version。
4.  **Tomorrow's way～Instrumental～**

## 銷售紀錄

  - 累積發售量：32,391
  - [Oricon最高排名](../Page/Oricon.md "wikilink")：第15名
  - [Oricon上榜次數](../Page/Oricon.md "wikilink")：10次

| 發行         | 排行榜       | 最高位 | 總銷量    |
| ---------- | --------- | --- | ------ |
| 2005年6月22日 | Oricon 日榜 | 15  | 32,391 |

[Category:YUI歌曲](../Category/YUI歌曲.md "wikilink")
[Category:電影主題曲](../Category/電影主題曲.md "wikilink")
[Category:2005年單曲](../Category/2005年單曲.md "wikilink")