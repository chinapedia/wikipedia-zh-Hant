**Tony**，本名**田中貴之**，毕业于[宫城县](../Page/宫城县.md "wikilink")[仙台市的美术专门学校](../Page/仙台市.md "wikilink")，[日本](../Page/日本.md "wikilink")[插畫家](../Page/插畫家.md "wikilink")、[原畫家](../Page/原畫家.md "wikilink")、[人物設計師](../Page/人物設計師.md "wikilink")。現任[rpm](../Page/rpm_\(公司\).md "wikilink")[有限公司](../Page/有限公司.md "wikilink")[執行董事](../Page/執行董事.md "wikilink")。

原為[廣告設計員](../Page/廣告設計.md "wikilink")，後來轉職為插畫家。因[十八禁遊戲的繪畫原畫的工作較多而越趨知名](../Page/十八禁遊戲.md "wikilink")。

## 作品一覽

### 一般遊戲

  - [西風狂詩曲2：暴風雨](../Page/西風狂詩曲2：暴風雨.md "wikilink")（TEMPEST）（[Softmax](../Page/Softmax_\(游戏开发商\).md "wikilink")1998年12月17日發售，[旭力亞](../Page/旭力亞.md "wikilink")2000年發售）\[1\]
  - [小秘書Lina](../Page/小秘書Lina.md "wikilink")（Room with Lina）（株式會社[Visual
    Lab](../Page/Visual_Lab.md "wikilink")1999年6月24日發售，[遊戲橘子](../Page/遊戲橘子.md "wikilink")2000年9月26日發售）\[2\]
  - [光明之淚](../Page/光明之淚.md "wikilink")
  - [光明之風](../Page/光明之風.md "wikilink")
  - [光明之心](../Page/光明之心.md "wikilink")
  - [光明之刃](../Page/光明之刃.md "wikilink")
  - [光明之舟](../Page/光明之舟.md "wikilink")
  - [光明之響](../Page/光明之響.md "wikilink")
  - [Exstetra](../Page/Exstetra.md "wikilink")

### 十八禁遊戲

  -
  -
  -
  -
  - [Ciel Limited Collector's Box ～Tony Illustration
    Games～](../Page/Ciel_Limited_Collector's_Box_～Tony_Illustration_Games～.md "wikilink")

  - [After… -Story
    Edition-](../Page/After…_-Story_Edition-.md "wikilink")

  - [After…](../Page/After….md "wikilink")

  -
  -
  -
  -
  -
  -
  -
  -
### 十八禁動畫

  - （全2集）

  - （全2集）

  - （全3集）

### 设定资料和原画集（含十八禁）

  -
  -
  -
  -
  -
### 個人画集

  - Tony's Art Works Graph I〜IV（[台湾限定販售](../Page/台湾.md "wikilink")）

  - COLLECT1 Tony同人畫集總集篇（C76販售）

  - Tony線画集 Tony's Line ART works Vol.1
    （C77由[世嘉販售](../Page/世嘉.md "wikilink")，光明系列線稿本）

  - Tony's ARTworks from Shining Wind

  - （十八禁，ISBN 978-4-86379-107-7）

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [T2 ART WORKS](http://www.bekkoame.ne.jp/i/taka_tony/)

  -
  - [](https://web.archive.org/web/20071014015128/http://shinings.blog116.fc2.com/)

  - [](http://www.sp-janis.com/)

  - [光明之淚](http://shining-tears.jp/)

  - [光明之風](http://shining-wind.jp/)

  - [Shining World](http://shining-world.jp/)

  - [仏蘭西少女～Une fille
    blanche～](https://web.archive.org/web/20090830024645/http://www.franceshojo.com/top.html)

[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:日本插畫家](../Category/日本插畫家.md "wikilink")
[Category:電子遊戲繪圖及原畫家](../Category/電子遊戲繪圖及原畫家.md "wikilink")
[Category:仙台市出身人物](../Category/仙台市出身人物.md "wikilink")

1.  [Tony早期出道参与人物设定的游戏《TEMPEST》](http://ibaiba.sakura.ne.jp/blog/archives/1998_12_17_224.html)

2.  [](http://ascii.jp/elem/000/000/315/315860/)