**TopLink**是一套專供[Java程式師運用的](../Page/Java.md "wikilink")**物件關連映射**（[object-relational
mapping](../Page/object-relational_mapping.md "wikilink")）套件（Package，Java程式語言的Package是專門用來對「[類別](../Page/類別.md "wikilink")」進行群化整理之用）。它提供一個強效且彈性的框架（Framework），可讓Java物件存放到關連性資料庫內，或提供Java物件與[XML文件的轉換功效](../Page/XML.md "wikilink")。
[EntityLifeCycle.png](https://zh.wikipedia.org/wiki/File:EntityLifeCycle.png "fig:EntityLifeCycle.png")
**TopLink
Essentials**\[[https://web.archive.org/web/20060707013121/http://www.oracle.com/technology/products/ias/toplink/jpa/index.html\]是](https://web.archive.org/web/20060707013121/http://www.oracle.com/technology/products/ias/toplink/jpa/index.html%5D是)[EJB](../Page/EJB.md "wikilink")
3.0 Java Persistence
API（簡稱：[JPA](../Page/JPA.md "wikilink")）的一個參考實現（Reference
Implementation，簡稱：RI），更簡單說是即是一個合乎EJB 3.0規範的JPA RI。此外TopLink
Essentials也是Oracle公司TopLink軟體的開放原碼社群版（open-source community
edition）。

## 歷史

TopLink最初是在1990年代由The Object
People公司以[Smalltalk程式語言所撰寫成](../Page/Smalltalk.md "wikilink")，而"TOP"一字其實是縮寫，全寫即是"The
Object
People"。到了1995年、1996年左右這套軟體產品被人用[Java程式語言加以完整改寫](../Page/Java.md "wikilink")，等於用Java程式語言重新再詮釋一遍此項產品，並重新命名為「TopLink
for Java」。

到了2000年The Object
People公司被拆分，同時TopLink軟體也被[WebGain公司買去](../Page/WebGain公司.md "wikilink")。至2002年時又由甲骨文公司（[Oracle](../Page/Oracle.md "wikilink")）買走TopLink，並由其接手後續的研發改版，如今TopLink這套軟體已經成為Oracle
Fusion Middleware（融合中介軟體）軟體家族中的一員。

有關TopLink的更細節歷史請見：[TopLink軟體的歷史](https://web.archive.org/web/20080908025120/http://www.oracle.com/technology/tech/java/newsletter/articles/toplink/history_of_toplink.html)

TopLink依然是該領域的領導軟體，它有多項獲獎特點包括：

  - 經由**Java程式師期刊**（**[Java Developer's
    Journal](../Page/Java_Developer's_Journal.md "wikilink")**）的讀者投票，TopLink被票選為**最佳Java貫徹架構獎**（**Best
    Java Persistence Architecture**）。

<!-- end list -->

  - **Java世界**（**[JavaWorld](../Page/JavaWorld.md "wikilink")**）期刊的編輯，評選TopLink為2003年的**最佳Java資料存取工具獎**（**Best
    Java Data Access Tool**）。

<!-- end list -->

  - **[Java
    Pro](../Page/Java_Pro.md "wikilink")**期刊的**讀者評選獎**（**Readers'
    Choice Award**）中，TopLink獲得**最佳Java資料存取工具或驅動程式**（**Best Java Data
    Access Tool or Driver**）的獎項。

## 特點

雖然多數人都已知TopLink是一套[物件關連映射](../Page/物件關連映射.md "wikilink")（或稱：對應）的工具程式，但其仍有幾項關鍵特點值得強調：

  - 豐富的「查詢框架，query framework」，該框架支援：物件導向的表現框架、範例式查詢「Query by
    Example，簡稱：QBE」、EJB
    QL、SQL以及[預存程序](../Page/預存程序.md "wikilink")（[stored
    procedure](../Page/stored_procedure.md "wikilink")）。

<!-- end list -->

  - 一個物件層級、層次的交易框架。

<!-- end list -->

  - 先進的快取能力，確保物件能被一致性的辨識。

<!-- end list -->

  - 完整具備了直接映射（對應）與關連映射（對應）。

<!-- end list -->

  - Object-to-XML的映射、對應，此外也支援[JAXB](../Page/JAXB.md "wikilink")。

<!-- end list -->

  - 支援[EIS](../Page/EIS.md "wikilink")／[JCA等非關連性的資料來源](../Page/JCA.md "wikilink")。

<!-- end list -->

  - 視覺化的映射編輯軟體：Mapping Workbench。

## 外部連結

  - [Oracle公司TopLink軟體的官方網站](https://web.archive.org/web/20050910162737/http://www.oracle.com/technology/products/ias/toplink/index.html)-**（英文）**

<!-- end list -->

  - [Oracle TopLink Quick
    Tour](https://web.archive.org/web/20060515073905/http://www.oracle.com/technology/products/ias/toplink/quicktour/index.htm)

<!-- end list -->

  - [TopLink軟體的Wiki](https://web.archive.org/web/20060513072101/http://toplink.waldura.com/Main)-**（英文）**

[Category:Java平台軟體](../Category/Java平台軟體.md "wikilink")
[Category:Oracle公司](../Category/Oracle公司.md "wikilink")