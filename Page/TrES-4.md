**TrES-4**是一個環繞著[GSC
02620-00648的](../Page/GSC_02620-00648.md "wikilink")[太陽系外行星](../Page/太陽系外行星.md "wikilink")，位在[武仙座](../Page/武仙座.md "wikilink")，距離地球1400[光年](../Page/光年.md "wikilink")，它不尋常的一點是，它的密度極為的低，只有0.2g/cm<sup>3</sup>，但是它的表面溫度才1300℃，這還不夠熱到讓它膨脹到這種地步，或許行星本生的[內熱可以解決這問題](../Page/內熱.md "wikilink")。

## 參見

  - [GSC 02620-00648](../Page/GSC_02620-00648.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:太陽系外行星](../Category/太陽系外行星.md "wikilink")