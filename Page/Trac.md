**Trac**是公司开发并维护的[开放源码網頁界面](../Page/开放源码.md "wikilink")[專案管理](../Page/專案管理.md "wikilink")、[缺陷追踪軟體](../Page/缺陷追踪.md "wikilink")。Trac的灵感来自于CVSTrac，因为能够与[Subversion接口](../Page/Subversion.md "wikilink")，所以最初叫做svntrac。

Trac使用[Python](../Page/Python.md "wikilink")[程式語言開發](../Page/程式語言.md "wikilink")。在2005年中以前，Trac以[GPL發行](../Page/GNU通用公共許可證.md "wikilink")；直到0.9版開始使用[修改過的BSD許可證釋出](../Page/BSD許可證.md "wikilink")\[1\]。基本上都是屬於[自由軟體的](../Page/自由軟體.md "wikilink")[許可證](../Page/软件许可证.md "wikilink")。

## 主要功能

Trac使用[超链接方式把](../Page/超链接.md "wikilink")[软件缺陷数据库](../Page/软件缺陷.md "wikilink")、[版本控制系统和](../Page/版本控制.md "wikilink")[wiki内容结合起来](../Page/wiki.md "wikilink")，并作为[版本控制系统的](../Page/版本控制系统.md "wikilink")[web接口](../Page/World_Wide_Web.md "wikilink")，支持的版本控制系统包括[Subversion](../Page/Subversion.md "wikilink")、[Git](../Page/Git.md "wikilink")、[Mercurial](../Page/Mercurial.md "wikilink")、、、。在0.11版本之前，Trac的web展示前端由
web模板系统提供，自0.11开始，由其自行开发的模板系统提供，但保持了对ClearSilver及其插件的兼容。

其他功能：

  - [项目管理](../Page/项目管理.md "wikilink")（Roadmap、Milestones，等等...）
  - 追踪系统（缺陷追踪、任务等）
  - 细粒度权限支持（自0.11版本开始）
  - 最近活动的时间轴
  - [Wiki](../Page/Wiki.md "wikilink")
  - 可定制的报告
  - [版本控制系统的web接口](../Page/版本控制系统.md "wikilink")
  - [RSS Feeds](../Page/RSS.md "wikilink")
  - 多项目支持
  - 环境扩展（通过Python插件支持）
  - [iCal输出](../Page/iCal.md "wikilink")

## 參考資料

## 外部链接

  - [Trac官方網站](http://trac.edgewall.org/)
  - [Trac-Hacks](http://www.trac-hacks.org/)
  - [Edgewall Software Home](http://www.edgewall.org/)

[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:项目管理](../Category/项目管理.md "wikilink")

1.