[TrebuchetMS.svg](https://zh.wikipedia.org/wiki/File:TrebuchetMS.svg "fig:TrebuchetMS.svg")
**Trebuchet
MS**是一種[無襯線字體](../Page/無襯線字體.md "wikilink")，1996年由替[微軟開發](../Page/微軟.md "wikilink")。

微軟稱Trebuchet
MS為良好的[網頁設計字體](../Page/網頁設計.md "wikilink")，並將其納入[網頁核心字型的其中之一](../Page/網頁核心字型.md "wikilink")，同時也附載在一些微軟的產品中，包含[Microsoft
Windows操作系統](../Page/Microsoft_Windows.md "wikilink")、[Microsoft
Office辦公室軟體以及](../Page/Microsoft_Office.md "wikilink")[Internet
Explorer瀏覽器](../Page/Internet_Explorer.md "wikilink")。此外，英文版的[Windows
XP更將此字型設為標題列預設字型](../Page/Windows_XP.md "wikilink")。

## 特色

Trebuchet MS與其他常見的無襯線字體大不相同，其顯著的特色如下：（請參照圖示）

  - 大寫字母「M」的兩端與垂直線呈10度角
  - 大寫字母「Q」的尾巴
  - 大寫字母「A」中的橫槓較低
  - 小寫字母「e」和數字「6、9」的尾巴較短
  - 小寫字母「g」的下圓帶有開口
  - 小寫字母「i、j」上面的點為圓點
  - 小寫字母「l」的尾巴呈彎曲狀
  - 金錢符號「$」的直豎筆畫不貫穿S字母，只出現在上端和下端
  - 「[&](../Page/&.md "wikilink")」符號是英文「Et」的合寫
  - [感嘆號](../Page/感嘆號.md "wikilink")「\!」下的點為圓點
  - [斜體不單只是把原本字體傾斜](../Page/斜體.md "wikilink")，而是另外設計一套斜體的字母

## 外部連結

  - [Trebuchet
    MS字體相關資訊](http://www.microsoft.com/typography/fonts/font.aspx?FID=2&FNAME=Trebuchet%20MS)
  - [Trebuchet
    Nation](http://www.microsoft.com/typography/web/fonts/trebuche/default.htm)
    - 講述Trebuchet MS字體演變的短文
  - [Trebuchet
    MS下載點](http://sourceforge.net/project/showfiles.php?group_id=34153)

[Category:无衬线字体](../Category/无衬线字体.md "wikilink")
[Category:微软字体](../Category/微软字体.md "wikilink")
[Category:網頁核心字型](../Category/網頁核心字型.md "wikilink")
[Category:Windows XP字體](../Category/Windows_XP字體.md "wikilink")