[Treo_300.jpg](https://zh.wikipedia.org/wiki/File:Treo_300.jpg "fig:Treo_300.jpg")

**Treo**是[美國](../Page/美國.md "wikilink")[Palm公司生產的](../Page/Palm.md "wikilink")[智能手機系列](../Page/智能手機.md "wikilink")，原本由[Handspring開發](../Page/Handspring.md "wikilink")，在Palm[收購Handspring公司後](../Page/收購.md "wikilink")，全線改由Palm開發及生產。Treo是其中一個最受歡迎的[PDA](../Page/PDA.md "wikilink")[手提電話系列](../Page/手提電話.md "wikilink")，功能包括個人數碼助理、[話音通訊](../Page/話音.md "wikilink")、[電子郵件](../Page/電子郵件.md "wikilink")、[網頁瀏覽等](../Page/網頁.md "wikilink")，近年的型號亦包括[數碼相機](../Page/數碼相機.md "wikilink")。

至2006年9月，共有14款Treo型號，至[Treo 680止使用](../Page/Treo_680.md "wikilink")[Palm
OS](../Page/Palm_OS.md "wikilink")[操作系統](../Page/操作系統.md "wikilink")，自700系列開始，Treo型號分別使用Palm
OS及[Windows Mobile](../Page/Windows_Mobile.md "wikilink")。Treo
700w是首部使用Windows Mobile操作系統的型號。

Palm已和[RIM達成協議](../Page/Research_In_Motion.md "wikilink")，讓旗下Treo產品連接及收發流行的[BlackBerry推動式電子郵件](../Page/BlackBerry.md "wikilink")，採用BlackBerry
Connect軟件連接。

2006年9月，Palm與[Vodafone合作在](../Page/Vodafone.md "wikilink")[歐洲推出首部](../Page/歐洲.md "wikilink")[3G手機](../Page/3G.md "wikilink")，採用Windows
Mobile操作系統的Treo 750v。

## Treo產品

  - Treo 90 PDA
  - Treo 180 [GSM](../Page/GSM.md "wikilink")
  - Treo 180g GSM
  - Treo 270 GSM
  - Treo 300 [CDMA](../Page/CDMA.md "wikilink")
  - Treo 500 GSM / WCDMA (Windows Mobile)
  - Treo 600 GSM / CDMA
  - Treo 600（无摄像头）CDMA
  - [Treo 650](../Page/Treo_650.md "wikilink") GSM / CDMA
  - Treo 680 GSM
  - Treo 700w CDMA (Windows Mobile)
  - Treo 700p CDMA / [EVDO](../Page/EVDO.md "wikilink")
  - Treo 700wx CDMA (Windows Mobile)
  - Treo 750 GSM (Windows Mobile)
  - Treo 750v GSM (Windows Mobile)
  - Treo 755p CDMA
  - Treo Pro (Windows Mobile)

## 外部連結

  - [Palm](https://web.archive.org/web/20030926090907/http://www.palm.com/)
  - [吹友吧 TREO中文用户专门站](http://www.treo8.com/)
  - [TreoCentral](http://www.treocentral.com/)
  - [MAXPDA
    中文Palm社区](https://web.archive.org/web/20071111140516/http://bbs.maxpda.com/)
  - [Everything Treo](http://www.everythingtreo.com/)
  - [mytreo.net](http://mytreo.net/)
  - [Treonauts.com](http://blog.treonauts.com/)
  - [treo Addicts](http://www.treoaddicts.com/)
  - [Treo Mobile](http://www.treomobile.com/)

[Category:智能手機](../Category/智能手機.md "wikilink")
[Category:个人数码助理](../Category/个人数码助理.md "wikilink")
[Category:Palm公司](../Category/Palm公司.md "wikilink") [Category:Palm
OS](../Category/Palm_OS.md "wikilink")