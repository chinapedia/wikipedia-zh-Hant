[Treo.jpg](https://zh.wikipedia.org/wiki/File:Treo.jpg "fig:Treo.jpg")
**Treo 650**是[Palm公司推出的一款採用](../Page/Palm.md "wikilink")[Palm
OS的](../Page/Palm_OS.md "wikilink")[智能手機](../Page/智能手機.md "wikilink")。擁有[移动电话及](../Page/移动电话.md "wikilink")[PDA功能](../Page/PDA.md "wikilink")，曾在国际上獲得獎項。

## 基本情况

Treo 650预装的系统为Palm OS 5.4.8。用户也可以自行定制ROM文件并更换预装的ROM文件\[1\]。

Treo 650使用了[QWERTY键盘](../Page/QWERTY.md "wikilink")，并配以PALM上常用的五向导航键。

Treo 650使用了[TFT](../Page/TFT.md "wikilink") 触控式屏幕（320 x
320分辨率，16位色彩）。屏幕的生产商有[索尼和](../Page/索尼.md "wikilink")[三星等](../Page/三星電子.md "wikilink")\[2\]。

Treo
650的机身[内存仅为](../Page/内存.md "wikilink")32MB，用户可用内存也只有21MB。这也使得后来推出的[Treo
680虽然同样使用了Intel](../Page/Treo_680.md "wikilink") XScale主频为312
MHz的处理器，但在内存增加后性能却得到大大提升\[3\]。

Treo 650的尺寸（11.3 x 5.9 x 2.3 cm）和重量（178克）相对于后来推出的Treo 680及其它同时期的手机都相对较大。

在扩展性方面，Treo 650支持
[SD](../Page/SD.md "wikilink")，[SDIO及](../Page/SDIO.md "wikilink")
[MMC](../Page/MMC.md "wikilink")。另外，Treo
650也支持[SDHC卡](../Page/SDHC.md "wikilink")。但在使用时可能会出现一些问题，如不能拍摄视频\[4\]。

在多媒体性能方面，Treo 650使用2.5 mm 耳机插孔。Treo 650自带了30万像素的摄像头，并搭载了自拍镜。

数据传输方面，Treo配备了[红外和](../Page/红外.md "wikilink")[蓝牙传输的功能](../Page/蓝牙.md "wikilink")，也支持
GPRS 网络连接，但是目前没有证据表示 Treo 650 支持 EDGE 网络。

Palm公司并没有提供Treo
650在[Linux下的同步方案](../Page/Linux.md "wikilink")，但通过一系列设置后，Treo
650也可以通过[Jpilot](../Page/Jpilot.md "wikilink")、[Gnome-pilot](../Page/Gnome-pilot.md "wikilink")、[KPilot等软件进行同步](../Page/KPilot.md "wikilink")\[5\]\[6\]。

## 特色功能

[Treo_650_thread_sms.png](https://zh.wikipedia.org/wiki/File:Treo_650_thread_sms.png "fig:Treo_650_thread_sms.png")
Treo 650有很多人性化的设计，但部分设计在后来的Treo 680中取消了。Treo
650提供外置铃声开关，使用都可以不开机就将手机在铃声和振动模式间进行转换。

Treo
650的[SIM卡插槽放置在手机顶端](../Page/SIM卡.md "wikilink")，可以在不拆电池的情况下进行插拔\[7\]。

Treo
650另外一个特色功能就是采用了聊天模式的短信管理，这使得用户可以像使用[即时通讯软件一样进行短信通讯](../Page/即时通讯软件.md "wikilink")。

## 缺陷

由于Treo
650使用了全新的系统结构，在使用过程中有时会出现不稳定的情况。\[8\]，但大部分都是由于软件冲突造成的，后期的treo680等机型有很大改善。

此外， 部分Treo 650使用一段时间后常会出现耳机插孔接触不良以及温度低的时候睡死的情况。这是由于不同批次的模块质量不同造成的。\[9\]。

## 相关文化

[Treo_prison_break.jpg](https://zh.wikipedia.org/wiki/File:Treo_prison_break.jpg "fig:Treo_prison_break.jpg")
在不少影视作品中也可以找到Treo
650的身影\[10\]，如《[越狱](../Page/越狱.md "wikilink")》第三季第一集末尾，林肯所使用的手机就是Treo
650\[11\]。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [Palm公司提供的Treo 650拆机示意图](http://www.palm.com/us/support/contact/environment/disassem_inst_Treo650.pdf)

[Category:智能手機](../Category/智能手機.md "wikilink")
[Category:Palm公司](../Category/Palm公司.md "wikilink") [Category:Palm
OS](../Category/Palm_OS.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.