**TrigML** 是一種[XML導向的使用者介面描述語言](../Page/XML.md "wikilink")（XML-derived
UI mark-up
language），由英國Trigenix公司發明，功能類似[HTML和](../Page/HTML.md "wikilink")[JavaScript](../Page/JavaScript.md "wikilink")。可提供美國[Qualcomm公司的BREW](../Page/Qualcomm.md "wikilink")
[uiOne的Trig模式的撰寫](../Page/uiOne.md "wikilink")，2004年10月12日Qualcomm宣布以3600萬美金併購Trigenix公司，将提升BREW使用者介面技术开发推进到更高的境界\[1\]。

收购后，TrigML及其相关的集成开发环境（IDE）被[高通重新命名为](../Page/高通.md "wikilink")[uiOne](../Page/uiOne.md "wikilink")\[2\]。

## 注釋

1.  [PDF document on uiOne and
    TrigML](https://web.archive.org/web/20070927183723/http://brew.qualcomm.com/bnry_brew/pdf/brew_2005/t502_nijdam_qualcomm.pdf)
2.  [Qualcomm related
    patent](http://www.freshpatents.com/Automatic-updating-of-variables-in-a-data-language-dt20061130ptan20060271845.php?type=description)

[Category:動畫軟件](../Category/動畫軟件.md "wikilink")
[Category:图形文件格式](../Category/图形文件格式.md "wikilink")
[Category:高通](../Category/高通.md "wikilink")

1.  [Qualcomm dials Cambridge to enter UI
    business](http://www.theregister.co.uk/2004/10/13/qualcomm_buys_trigenix/)
2.  [Qualcomm Announces BREW® uiOne User Interface
    Offering](https://www.qualcomm.com/news/releases/2005/02/14/qualcomm-announces-brew-uione-user-interface-offering)