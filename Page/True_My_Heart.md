**True My
Heart**是2007年9月5日發售的[單曲](../Page/單曲.md "wikilink")，歌手為「[ave;new](../Page/ave;new.md "wikilink")
feat. [佐倉紗織](../Page/佐倉紗織.md "wikilink")」。

## 簡介

  - 收錄2005年發售的[十八禁遊戲](../Page/十八禁遊戲.md "wikilink")《[Nursery
    Rhyme](../Page/Nursery_Rhyme.md "wikilink")》的主題曲「True My
    Heart」混音版和B-side的「kiss my lips -Lovers EDIT-」。
  - 2007年春天，「True My
    Heart」因[空耳歌詞而在](../Page/幻聽歌詞.md "wikilink")[NICONICO動畫](../Page/NICONICO動畫.md "wikilink")、[YouTube等影片網站引起話題](../Page/YouTube.md "wikilink")。歌詞中的「」和「」聽起來像「」（一種料理，[名古屋特產](../Page/名古屋.md "wikilink")），因此「True
    My Heart」亦有「」的別稱\[1\]\[2\]
  - 2007年9月9日，a.k.a.dRESS以[角色主唱系列的](../Page/角色主唱系列.md "wikilink")翻唱「True
    My Heart」\[3\]\[4\]，再次於NICONICO動畫引起話題。
  - 其後「true my heart -Lovable
    mix-」再收錄於專輯「Lovable」，於2007年12月29日至31日的[C73先行發售](../Page/Comic_Market.md "wikilink")，翌年1月25日正式發售。
  - 2008年7月9日，「true my heart -ave;new NICCO
    Remix-」收录於NICONICO動畫歌曲集「」\[5\]。

## 曲目

1.  true my heart -Lovable mix-
      -
        歌：ave;new feat. 佐倉紗織，作詞：a.k.a.dRESS、佐倉紗織，作曲、編曲：a.k.a.dRESS
2.  kiss my lips -Lovers EDIT-
      -
        歌：ave;new feat. 佐倉紗織，作詞、作曲、編曲：a.k.a.dRESS
3.  true my heart -premium-
      -
        歌：ave;new feat. 佐倉紗織，作詞：a.k.a.dRESS、佐倉紗織，作曲、編曲：a.k.a.dRESS
4.  True My Heart -HARD dRESS STYLE-
      -
        歌：ave;new feat. 佐倉紗織，作詞：a.k.a.dRESS、佐倉紗織，作曲、編曲：a.k.a.dRESS
5.  true my heart -Lovable mix-（karaoke）
6.  kiss my lips -Lovers EDIT-（karaoke）

## 参考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [ave;new
    Works.](https://web.archive.org/web/20070906192040/http://www.avenew.jp/works.html)

  - [「True My
    Heart」空耳歌詞的一例](https://archive.is/20130501002858/ameblo.jp/un-man-chin/entry-10046197753.html)

[Category:2005年音樂](../Category/2005年音樂.md "wikilink")
[Category:2007年單曲](../Category/2007年單曲.md "wikilink")
[Category:電波歌曲](../Category/電波歌曲.md "wikilink")
[Category:遊戲主題曲](../Category/遊戲主題曲.md "wikilink")

1.  [](http://www.new-akiba.com/archives/2007/02/nursery_rhyme.html)
2.  《NICONICO動畫Fan Book》 ISBN 4-88380-663-4
3.  [今週的ave;new | ♯19 ave;new
    feat.初音未來（內含試聽）](http://blog.avenew.jp/archives/76)

4.  [a.k.a.dRESS presents dRESS-room - ave;new
    feat.初音未來](http://akadress.com/?p=34)
5.