**Turbo C++**是一個[Borland](../Page/Borland.md "wikilink")
[C++編譯器與](../Page/C++.md "wikilink")[IDE](../Page/集成开发环境.md "wikilink")。最早的Turbo
C++ 產品系列出現在1993年以後，以及於2006年重新發行，具有一個互動的IDE，本質上源自他們的旗艦產品[C++
Builder的降級版本](../Page/C++_Builder.md "wikilink")。Turbo C++
2006則發佈於2006年9月5日，以及它區分為「Explorer」與「Professional」兩個版本。Explorer版可以自由下載與散佈，Professional版則是銷售的商品。

## 外部連結

  - [Official website](http://www.turboexplorer.com)
  - [Turbo C++ v1.01 free download from
    Borland](https://web.archive.org/web/20060111143737/http://community.borland.com/article/0%2C1410%2C21751%2C00.html)

[Category:C++編譯器](../Category/C++編譯器.md "wikilink")