**Turbo
Pascal**是[Borland公司一个很有代表性的软件开发产品](../Page/Borland.md "wikilink")。从语言角度说Turbo
Pascal是[Pascal语言的一个扩展](../Page/Pascal_\(编程语言\).md "wikilink")。它扩展了标准Pascal的功能特性。在语法上，Turbo
Pascal吸取了其他语言如[C等的特性](../Page/C编程语言.md "wikilink")，简化了标准Pascal的语法；在功能上，Turbo
Pascal提供了包括屏幕控制、图形处理、系统调用等有用的函数库（Turbo Pascal中称为**单元**）。

Turbo
Pascal包括[CP/M](../Page/CP/M.md "wikilink")、[MS-DOS](../Page/MS-DOS.md "wikilink")、[Windows等不同](../Page/Windows.md "wikilink")[操作系统上的版本](../Page/操作系统.md "wikilink")。

1983年，[Borland公司发布了Turbo](../Page/Borland.md "wikilink")
Pascal的第一个版本。该版本的[编译器核心部分由](../Page/编译器.md "wikilink")[安德斯·海尔斯伯格授权给](../Page/安德斯·海尔斯伯格.md "wikilink")[Borland公司](../Page/Borland.md "wikilink")。同时，安德斯·海尔斯伯格也作为雇员加入了Borland公司，并且是后来所有Turbo
Pascal版本与Delphi前3个版本的架构师。在第一个版本的Turbo Pascal中，Borland公司的创始人[Philippe
Kahn为该版本的Turbo](../Page/Philippe_Kahn.md "wikilink")
Pascal添加了用户界面与编辑器。第一版本Turbo
Pascal是个人电脑上编译器发展的一个里程碑。

1989年发布的Turbo Pascal
5.5版是重要的版本，从这一版本起，Borland公司把[面向对象程序设计](../Page/面向对象程序设计.md "wikilink")（OOP）引入Pascal语言，这就是大家所知道的Object
Pascal的开端。

Turbo Pascal
6.0推出用OOP思想封装的DOS应用程序框架（Framework），叫做TurboVision，这个版本的IDE据信即是使用该Framework开发的。

Turbo Pascal最后一个版本叫做Borland Pascal 7，包含增强的DOS
[IDE和编译器](../Page/IDE.md "wikilink")，可以创建DOS和Windows3.x程序，后来为[Delphi](../Page/Delphi.md "wikilink")1.0代替。

  - 1983年11月20日 Turbo Pascal version 1.0发布
  - 1986年9月17日 Turbo Pascal version 3.02 发布
  - 1989年5月2日 Turbo Pascal version 5.5 发布

## 参考文献

## 外部链接

  - [Turbo-Pascal Programming Language](http://www.turbo-pascal.com/)

## 参见

  - [Delphi](../Page/Delphi.md "wikilink")
  - [Object Pascal](../Page/Object_Pascal.md "wikilink")
  - [Free Pascal](../Page/Free_Pascal.md "wikilink")

{{-}}

[Category:1983年软件](../Category/1983年软件.md "wikilink")
[Category:Pascal](../Category/Pascal.md "wikilink")
[Category:程序设计语言](../Category/程序设计语言.md "wikilink")
[Category:Borland軟體](../Category/Borland軟體.md "wikilink")