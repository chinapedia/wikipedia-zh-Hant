**Typhoon Games (HK)
Ltd.**，成立於2001年8月，主要業務是代理[歐美](../Page/歐美.md "wikilink")、[日本製作的](../Page/日本.md "wikilink")[電腦遊戲](../Page/電腦遊戲.md "wikilink")，以及推出這些遊戲的本地化版本（如繁體中文版）。

## 代理遊戲

  - [Onimusha 中文版](../Page/Onimusha_中文版.md "wikilink")
  - [咕噜小天使 中文版](../Page/咕噜小天使.md "wikilink")
  - [美少女夢工場4 中文版](../Page/美少女夢工場.md "wikilink")
  - [英雄傳說VI「空之軌跡FC」中文版](../Page/英雄傳說VI「空之軌跡」#FC.md "wikilink")
  - [英雄傳說VI「空之軌跡SC」中文版](../Page/英雄傳說VI「空之軌跡」#SC.md "wikilink")
  - [英雄傳說VI「空之軌跡3rd」中文版](../Page/英雄傳說VI「空之軌跡」#3rd.md "wikilink")
  - [英雄傳說「零之軌跡」中文版](../Page/英雄傳說_零之軌跡.md "wikilink")
  - [英雄傳說「碧之軌跡」中文版](../Page/英雄傳說_碧之軌跡.md "wikilink")
  - [生化危機4 PC\~中文及英文版biohazard４](../Page/生化危機4.md "wikilink")
  - [鬼武者3 中文版](../Page/鬼武者3.md "wikilink")
  - [Lost Planet : Extreme Condition
    PC](../Page/Lost_Planet_:_Extreme_Condition_PC.md "wikilink")
  - [Lost Planet : Colonies
    PC](../Page/Lost_Planet_:_Colonies_PC.md "wikilink")
  - [Ys VI 伊蘇VI 中文版](../Page/伊蘇VI.md "wikilink")
  - [Ys Origin 伊蘇始源 中文版](../Page/伊蘇始源.md "wikilink")
  - [新世紀福音戰士之鋼鐵戀人十週年特別篇 中文版](../Page/新世紀福音戰士之鋼鐵戀人.md "wikilink")
  - [Heroes of Annihilated Empires
    中文版](../Page/Heroes_of_Annihilated_Empires_中文版.md "wikilink")
  - [Xanadu Next 迷城國度 中文版](../Page/迷城國度.md "wikilink")
  - [Devil May Cry 3 中文版](../Page/惡魔獵人3.md "wikilink")
  - [Devil May Cry 4 英文版](../Page/惡魔獵人4.md "wikilink")
  - [Winning Eleven 8 PC](../Page/Winning_Eleven.md "wikilink")
  - [Winning Eleven 9 PC](../Page/Winning_Eleven.md "wikilink")
  - [Winning Eleven Pro Evolution Soccer 2007
    PC](../Page/Winning_Eleven.md "wikilink")
  - [Pro Evolution Soccer 2008
    PC](../Page/Pro_Evolution_Soccer_2008_PC.md "wikilink")
  - [Soldner: Secret Wars](../Page/Soldner:_Secret_Wars.md "wikilink")
  - [Jowood](../Page/Jowood.md "wikilink")
  - [Deep Sea Tycoon](../Page/Deep_Sea_Tycoon.md "wikilink")
  - [RPG Maker XP 中文版](../Page/RPG_Maker_XP.md "wikilink")
  - [GADGET TRIAL機甲蘿莉 中文版](../Page/機甲蘿莉.md "wikilink")
  - [Chaos Legion 中文版](../Page/Chaos_Legion_中文版.md "wikilink")
  - [遊戲王online](../Page/遊戲王online.md "wikilink")
  - [A-7列車 中文版](../Page/A-7列車.md "wikilink")
  - [SEGA Rally REVO](../Page/SEGA_Rally_REVO.md "wikilink")
  - [The House Of The Dead 3](../Page/死亡鬼屋.md "wikilink")
  - [紅樓夢繁体中文版](../Page/紅樓夢.md "wikilink")
  - [雙星物語2繁体中文版](../Page/雙星物語2.md "wikilink")
  - [雙星物語2 PLUS繁体中文版](../Page/雙星物語2.md "wikilink")
  - [新天魔界‧渾沌慾望繁体中文版](../Page/新天魔界‧渾沌慾望.md "wikilink")
  - [古剑奇谭繁体中文版](../Page/古剑奇谭.md "wikilink")
  - [伊蘇7繁体中文版](../Page/伊蘇7.md "wikilink")
  - [Pro Evolution Soccer 2009
    PC](../Page/Pro_Evolution_Soccer_2009_PC.md "wikilink")
  - [Pro Evolution Soccer 2010
    PC](../Page/Pro_Evolution_Soccer_2010_PC.md "wikilink")
  - [Pro Evolution Soccer 2011
    PC](../Page/Pro_Evolution_Soccer_2011_PC.md "wikilink")
  - [Pro Evolution Soccer 2012
    PC](../Page/Pro_Evolution_Soccer_2012_PC.md "wikilink")
  - [Football Manager 2010](../Page/Football_Manager_2010.md "wikilink")
  - [Football Manager 2011](../Page/Football_Manager_2011.md "wikilink")
  - [Football Manager 2012](../Page/Football_Manager_2012.md "wikilink")
  - [Aliens VS Predator](../Page/Aliens_VS_Predator.md "wikilink")
  - [Total War Empire](../Page/Total_War_Empire.md "wikilink")
  - [Total War Napoleon](../Page/Total_War_Napoleon.md "wikilink")
  - [Total War SHOGUN 2](../Page/Total_War_SHOGUN_2.md "wikilink")

## 開發遊戲

  - [Hello Kitty Football Cup
    PC](../Page/Hello_Kitty_Football_Cup_PC.md "wikilink")
  - [Hello Kitty Mission Rescue
    (Xbox)](../Page/Hello_Kitty_Mission_Rescue_\(Xbox\).md "wikilink")
  - [Adidas Impossible Team Online
    Game](../Page/Adidas_Impossible_Team_Online_Game.md "wikilink")
  - [Hello Kitty Online
    Game](../Page/Hello_Kitty_Online_Game.md "wikilink")

## 外部链接

  - [Typhoon Games 官方網頁](http://www.typhoongames.com)

[Category:香港電子遊戲公司](../Category/香港電子遊戲公司.md "wikilink")
[Category:電子遊戲發行商](../Category/電子遊戲發行商.md "wikilink")
[Category:2001年開業電子遊戲公司](../Category/2001年開業電子遊戲公司.md "wikilink")