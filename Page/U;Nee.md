**U;Nee**（，），本名**許允**（**Heo
Yoon**，），[韩国女演员兼歌手](../Page/韩国.md "wikilink")；曾用艺名李慧蓮（**Lee
Hye-Ryeon**，）。

## 生平

她毕业于[庆熙大学戏剧电影系](../Page/庆熙大学.md "wikilink")，1996年以出演KBS连续剧《[大人们不懂年轻人](../Page/大人们不懂年轻人.md "wikilink")》（New
Generation Report-Adults Don't
Know）出道，曾出演电影《[Seventeen](../Page/Seventeen_\(電影\).md "wikilink")》
。2003年开始以U;Nee为艺名踏入歌坛。2003年6月12日发行首张专辑《1st u;nee
code》（DreamBeat），2005年2月18日发行第二张专辑《Vol.2 - Passion &
Pure》（Synnara Music）所屬經理人公司為IDPlus。

在2006年8月起暫停工作，遷至[仁川](../Page/仁川.md "wikilink")[西區與外婆](../Page/西區_\(仁川\).md "wikilink")、媽媽和舅舅一起生活。

## 電視劇

  - 《[大人們不懂年輕人](../Page/大人們不懂年輕人.md "wikilink")》

## 電影

  - 《[Seventeen](../Page/Seventeen_\(電影\).md "wikilink")》（1998年7月17日）

## 專輯

  - 《[1st U;Nee Code](../Page/1st_U;Nee_Code.md "wikilink")》（2003年6月12日）
  - 《[Vol.2 - Passion &
    Pure](../Page/Vol.2_-_Passion_&_Pure.md "wikilink")》（2005年2月18日）
  - 《[Solo Fantasy](../Page/Solo_Fantasy.md "wikilink")》（2007年1月26日）

## 逝世

2007年1月21日約中午一時被其71歲的外婆發現在仁川西區[麻田洞的某寓所](../Page/麻田洞.md "wikilink")22樓的家中房間門框內，以白色浴袍的腰帶在衣櫃掛衣棒吊頸身亡，據其家人稱U;Nee近期有抑鬱徵狀。適逄翌日就是她第三張專輯《Solo
fantasy》推出之際，並擬以此重返歌壇，故警方推測自殺還有其他原因。U;Nee於翌日下午2時出殯，遺體在仁川富平火葬場火化後，骨灰安厝在京畿道安城Utopia骨灰堂裡。

因U;Nee自杀而推迟发行的第三张专辑《Solo fantasy》于2007年1月26日正式发行。

## 其它

2006年，由[马来西亚](../Page/马来西亚.md "wikilink")[雪兰莪的阿旦杜亚](../Page/雪兰莪.md "wikilink")·沙丽布（Altantuya
Shaariibuu）的谋杀案件被本地警方对媒体显露后，[新马两国的媒体就在本案件的发展中宣传几张被误为阿旦杜亚的照片](../Page/新加坡.md "wikilink")。媒体后来发现这件丑闻的真相，而照片里的人是U;Nee。\[1\]\[2\]\[3\]\[4\]

## 参考文件

<references/>

## 外部链接

  - [U;Nee 的 Daum 网站](http://cafe.daum.net/love1244)

  - [U;Nee自杀的消息](http://news.naver.com/news/read.php?mode=LSD&office_id=003&article_id=0000293703&section_id=102&menu_id=102)

  - [U;Nee自杀的消息](http://ent.sina.com.cn/y/2007-01-21/17001419781.html)，新浪娛樂2007年1月21日

  - [UNEE自殺身亡眾星奔赴靈堂弔唁](https://web.archive.org/web/20160305061216/http://cnnews.chosun.com/site/data/html_dir/2007/01/22/20070122000000.html)
    組圖，朝鮮日報2007年1月22日

[Category:韓國女歌手](../Category/韓國女歌手.md "wikilink")
[Category:韓國流行音樂歌手](../Category/韓國流行音樂歌手.md "wikilink")
[Category:韓語流行音樂歌手](../Category/韓語流行音樂歌手.md "wikilink")
[Category:韓國電影演員](../Category/韓國電影演員.md "wikilink")
[Category:韓國電視演員](../Category/韓國電視演員.md "wikilink")
[Category:韓國自殺藝人](../Category/韓國自殺藝人.md "wikilink")
[Category:自杀歌手](../Category/自杀歌手.md "wikilink")
[Category:自杀演员](../Category/自杀演员.md "wikilink")
[Category:女性自殺者](../Category/女性自殺者.md "wikilink")
[Category:上吊自杀者](../Category/上吊自杀者.md "wikilink")
[Category:慶熙大學校友](../Category/慶熙大學校友.md "wikilink")
[Category:首爾特別市出身人物](../Category/首爾特別市出身人物.md "wikilink")
[Category:李姓](../Category/李姓.md "wikilink")
[Category:許姓](../Category/許姓.md "wikilink")

1.  [Confusion over which Altantuya was
    murdered](http://thestar.com.my/news/story.asp?file=/2006/11/14/nation/16013566&sec=nation)

2.  [Two Malaysian police officers charged with murder of Mongolian
    model](http://www.channelnewsasia.com/stories/southeastasia/view/241471/1/.html)
3.  [Altantuya Modelling
    Pictures](http://www.slackernetwork.com/out.php?id=59700)
4.  [这是阿旦杜亚吗?](http://thestar.com.my/news/story.asp?file=/2006/11/11/nation/15995457&sec=nation)