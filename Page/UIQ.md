**UIQ**（原名User Interface
Quartz，水晶用户界面）是所有的基于[Symbian操作系统的软件平台](../Page/Symbian_OS.md "wikilink")。UIQ的本质是一种[用户界面层](../Page/用户界面.md "wikilink")，为核心[操作系统提供附加的组件](../Page/操作系统.md "wikilink")，使开发第三方应用软件来扩充手机的功能成为可能。

## 各版本首发

  - UIQ 2.0通过[索尼爱立信P](../Page/索尼爱立信.md "wikilink")800首发。
  - UIQ 3.0通过索尼爱立信P990首发。
  - UIQ 3.1通过[摩托罗拉RIZR](../Page/摩托罗拉.md "wikilink") Z8首发。
  - UIQ 3.2通过摩托罗拉RIZR Z10首发。

## 收购史

索尼爱立信于2007年2月完成了对UIQ平台的收购。

摩托罗拉于2007年10月从索尼爱立信手中购得50%的UIQ平台股份。

## 采用UIQ 2的手机列表

  - [索尼爱立信](../Page/索尼爱立信.md "wikilink")

<!-- end list -->

  - 索尼爱立信 P800 (UIQ 2.0)
  - 索尼爱立信 P900 (UIQ 2.1)
  - 索尼爱立信 P910 (UIQ 2.1)

<!-- end list -->

  - [摩托罗拉](../Page/摩托罗拉.md "wikilink")

<!-- end list -->

  - 摩托罗拉 A920 (UIQ 2.0)
  - 摩托罗拉 A925 (UIQ 2.0)
  - 摩托罗拉 A1000 (UIQ 2.1)
  - 摩托罗拉 M1000 (UIQ 2.1)

<!-- end list -->

  - [明基](../Page/明基.md "wikilink")

<!-- end list -->

  - 明基 P30 (UIQ 2.0)
  - 明基 P31 (UIQ 2.1)

<!-- end list -->

  - [诺基亚](../Page/诺基亚.md "wikilink")

<!-- end list -->

  - 诺基亚 6708 (UIQ 2.1)

## 采用UIQ 3的手机列表

  - 掌上電腦式的設計（類似[掌上電腦](../Page/掌上電腦.md "wikilink")）

<!-- end list -->

  - [Sony Ericsson P1](../Page/Sony_Ericsson_P1.md "wikilink")/Sony
    Ericsson P1i/P1c (UIQ 3.0)
  - [Sony Ericsson M600](../Page/Sony_Ericsson_M600.md "wikilink")/Sony
    Ericsson M600i/M608c (UIQ 3.0)
  - [Sony Ericsson P990](../Page/Sony_Ericsson_P990.md "wikilink")/Sony
    Ericsson P990i/P990c (UIQ 3.0)
  - [Sony Ericsson W950](../Page/Sony_Ericsson_W950.md "wikilink")/Sony
    Ericsson W950i/W958c (UIQ 3.0)
  - [Sony Ericsson W960](../Page/Sony_Ericsson_W960.md "wikilink")/Sony
    Ericsson W960i/W960c (UIQ 3.0)

<!-- end list -->

  - 混合型[PDA](../Page/PDA.md "wikilink")/手機設計（直板手機可觸屏幕）

<!-- end list -->

  - [Sony Ericsson G700](../Page/Sony_Ericsson_G700.md "wikilink") (UIQ
    3.0)
  - [Sony Ericsson G900](../Page/Sony_Ericsson_G900.md "wikilink") (UIQ
    3.0)

<!-- end list -->

  - [滑蓋設計](../Page/滑蓋.md "wikilink")

<!-- end list -->

  - [Motorola RIZR Z8](../Page/Motorola_RIZR_Z8.md "wikilink")/Motorola
    Nahpohos Z8 (UIQ 3.1)
  - [Motorola RIZR Z10](../Page/Motorola_RIZR_Z10.md "wikilink") (UIQ
    3.2)

## 终止

根據Symbian基金會的決定，UIQ用戶平台已停止開發。

## 参考资料

<references />

[UIQ](../Category/諾基亞.md "wikilink")
[Category:操作系统](../Category/操作系统.md "wikilink")
[Category:智能手機](../Category/智能手機.md "wikilink")