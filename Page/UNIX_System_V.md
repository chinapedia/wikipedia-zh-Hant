[Unix_history-simple.svg](https://zh.wikipedia.org/wiki/File:Unix_history-simple.svg "fig:Unix_history-simple.svg")
**UNIX System
V**是[Unix](../Page/Unix.md "wikilink")[操作系统众多版本中的一支](../Page/操作系统.md "wikilink")。它最初由[AT\&T开发](../Page/AT&T.md "wikilink")，在1983年第一次发布，因此也被稱為**AT\&T
System V**。一共发行了4个System V的主要版本：版本1、2、3和4。System V Release
4，或者称为SVR4，是最成功的版本，成为一些UNIX共同特性的源头，例如“SysV
[初始化脚本](../Page/初始化.md "wikilink")”（`/etc/init.d`），用来控制系统启动和关闭，*System
V Interface Definition*（SVID）是一个System V如何工作的标准定义。

AT\&T出售运行System
V的专有硬件，但许多（或许是大多数）客户在其上运行一个转售的版本，这个版本基于AT\&T的[实现说明](../Page/实现说明.md "wikilink")。流行的SysV衍生版本包括Dell
SVR4和Bull SVR4。当今广泛使用的System V版本是[SCO](../Page/SCO.md "wikilink")
[OpenServer](../Page/OpenServer.md "wikilink")，基于System V Release
3，以及[SUN](../Page/SUN.md "wikilink")
[Solaris和SCO](../Page/Solaris.md "wikilink")
[UnixWare](../Page/UnixWare.md "wikilink")，都基于System V Release 4。

System V是AT\&T的第一个商业UNIX版本（[UNIX System
III](../Page/UNIX_System_III.md "wikilink")）的加强。传统上，System
V被看作是两种UNIX“风味”之一（另一个是[BSD](../Page/BSD.md "wikilink")）。然而，随着一些并不基于这两者代码的类UNIX实现的出现，例如[Linux和](../Page/Linux.md "wikilink")[QNX](../Page/QNX.md "wikilink")，这一归纳不再准确，但不论如何，像[POSIX这样的标准化努力一直在试图减少各种实现之间的不同](../Page/POSIX.md "wikilink")。

## SVR1

System
V的第一个版本，发布于1983年。它引进了一些特性，例如[vi编辑器和](../Page/vi.md "wikilink")[curses库](../Page/curses.md "wikilink")（这是从[加州大学伯克利分校开发的](../Page/加州大学伯克利分校.md "wikilink")[BSD中引进的](../Page/BSD.md "wikilink")）。其中也包括了对DEC
[VAX机器的支持](../Page/VAX.md "wikilink")。同时也支持使用消息进行[进程间通讯](../Page/进程间通讯.md "wikilink")，[信号量](../Page/信号标.md "wikilink")，和[共享内存](../Page/共享内存.md "wikilink")。

## SVR2

System V Release
2在1984年发布。其中添加了[shell功能和SVID](../Page/shell.md "wikilink")。

## SVR3

System V Release 3在1987年。它包括STREAMS，远程文件共享（remote file
sharing，RFS），[共享库](../Page/库.md "wikilink")，以及[Transport
Layer Interface](../Page/Transport_Layer_Interface.md "wikilink")（TLI）。

## SVR4

System V Release 4.0在1989年11月1日公开，并于1990年发布。是[UNIX Systems
Laboratories和Sun联合进行的项目](../Page/UNIX_Systems_Laboratories.md "wikilink")，它融合了来自Release
3，[4.3BSD](../Page/BSD.md "wikilink")，[Xenix](../Page/Xenix.md "wikilink")，以及[SunOS的技术](../Page/SunOS.md "wikilink")：

  - 来自BSD:
    [TCP/IP支持](../Page/TCP/IP.md "wikilink")，[csh](../Page/C_shell.md "wikilink")
  - 来自SunOS:
    [网络文件系统](../Page/网络文件系统.md "wikilink")，[内存映射文件](../Page/mmap.md "wikilink")，以及一个新的共享库系统
  - 其他的实现：
      - [ksh](../Page/Korn_shell.md "wikilink")
      - [ANSI C兼容](../Page/C编程语言.md "wikilink")
      - 更好的[国际化和本地化支持](../Page/本地化.md "wikilink")
      - 一个[应用二进制接口](../Page/应用二进制接口.md "wikilink")
      - 支持[POSIX](../Page/POSIX.md "wikilink")、[X/Open和](../Page/X/Open.md "wikilink")[SVID](../Page/SVID.md "wikilink")3标准

### SVR4.1

Release 4.1添加了异步I/O。

### SVR4.2

Release 4.2添加了Veritas文件系统的支持，[存取控制列表（access control
list）](../Page/ACL.md "wikilink")，以及动态可加载内核[模块](../Page/模块.md "wikilink")。

## SVR5

Release 5由[SCO Group制作](../Page/SCO_Group.md "wikilink")

## 外部链接

  - [Unix FAQ - history](http://www.faqs.org/faqs/unix-faq/faq/part6/)

[Category:1983年软件](../Category/1983年软件.md "wikilink") [Category:System
V](../Category/System_V.md "wikilink")
[Category:Unix](../Category/Unix.md "wikilink")