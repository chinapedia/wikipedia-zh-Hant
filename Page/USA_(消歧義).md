**USA**最常被用以指代[美國](../Page/美國.md "wikilink")，亦为美国的[ISO
3166-1三位字母国家代碼](../Page/ISO_3166-1.md "wikilink")。

此外，**USA**还可以指：

## 国家、地点与政府

  - [南非聯邦](../Page/南非聯邦.md "wikilink")（Union of South
    Africa，1910年－1961年）
  - [非洲合众国](../Page/非洲合众国.md "wikilink")（United States of Africa）
  - [宇佐町](../Page/宇佐町.md "wikilink")（Usa-chou），位於[日本](../Page/日本.md "wikilink")[高知縣](../Page/高知縣.md "wikilink")
  - [宇佐市](../Page/宇佐市.md "wikilink")（Usa-shi），位於日本[大分縣](../Page/大分縣.md "wikilink")
  - [宇佐郡](../Page/宇佐郡.md "wikilink")（Usa-gun），位於日本大分縣

## 地理

  - Usa河（源於[德國](../Page/德國.md "wikilink")[黑森](../Page/黑森.md "wikilink")）
  - Usa河（源於東非國家[坦桑尼亞](../Page/坦桑尼亞.md "wikilink")）

## 人物

  - [宇佐美吉啟](../Page/宇佐美吉啟.md "wikilink")（USA），日本的藝人，日本歌舞團體[EXILE的表演者](../Page/EXILE.md "wikilink")。

## 教育

  - [南阿拉巴馬州大學](../Page/南阿拉巴馬州大學.md "wikilink")（University of South
    Alabama）
  - [南澳大學](../Page/南澳大學.md "wikilink")（University of South Australia）

## 公司

  - [美國航空公司的](../Page/美國航空公司.md "wikilink")[ICAO航空公司代碼](../Page/ICAO航空公司代碼#U.md "wikilink")
  - [United Space
    Alliance](../Page/United_Space_Alliance.md "wikilink")（位於美國[德克薩斯州](../Page/德克薩斯州.md "wikilink")[侯斯頓的太空研發公司](../Page/侯斯頓.md "wikilink")）

## 音樂

  - 《USA》，歌唱組合King Crimson在1975年發售的[專輯](../Page/專輯.md "wikilink")。
  - 《U.S.A.》，[二重唱Ying](../Page/二重唱.md "wikilink") Yang
    Twins在2005年發售的[唱片](../Page/唱片.md "wikilink")。
  - 《[U.S.A. (DA
    PUMP单曲)](../Page/U.S.A._\(DA_PUMP单曲\).md "wikilink")》，日本组合[DA
    PUMP在](../Page/DA_PUMP.md "wikilink")2018年發售的[单曲](../Page/单曲.md "wikilink")。

## 體育

  - [美國和加拿大的足球協會](../Page/美國和加拿大的足球協會.md "wikilink")（United Soccer
    Association，已不存在）
  - United Spirit Arena（位於美國德克薩斯州拉伯克縣的一個大型舞台）
  - Unicycling Society of America（美國的[單輪車組織](../Page/單輪車.md "wikilink")）

## 其他

  - [美國聯邦檢察官](../Page/美國聯邦檢察官.md "wikilink")（United States Attorney）
  - [美國陸軍](../Page/美國陸軍.md "wikilink")（United States Army）
  - Underwriting Service Assistant（[保險業相關職位](../Page/保險.md "wikilink")）