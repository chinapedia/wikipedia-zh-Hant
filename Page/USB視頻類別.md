**USB video class**（又稱為**USB video device class** or
**UVC**）就是[USB](../Page/通用串行總線.md "wikilink") [device
class影像產品在不需要安裝任何的驅動程式下隨插即用](../Page/USB.md "wikilink")，包括[網路攝影機](../Page/網路攝影機.md "wikilink")、數位[攝影機](../Page/攝影機.md "wikilink")、類比影像轉換器、及[靜態影像相機](../Page/靜態影像相機.md "wikilink")。

最新的UVC版本為UVC
1.5，由[USB-IF](../Page/USB-IF.md "wikilink")（[USB開發者論壇](../Page/USB開發者論壇.md "wikilink")）定義包括基本協定及負載格式
\[1\] \[2\].

*請參考[USB視頻類別列表](../Page/USB視頻類別列表.md "wikilink")*

## 配件

### Webcams

Webcams是第一個支援UVC標準而且也是最大量的UVC設備，目前，作業系統只要是Windows XP
SP2之後的版本都可以支援UVC。使用UVC的好處USB在Video這塊也成為一項標準了之後，硬體在各個程式之間彼此運用會更加順利。

### 采集卡

UVC v1.1 支持压缩视频流，包括[MPEG-2
TS](../Page/MPEG_transport_stream.md "wikilink")，[H.264](../Page/H.264/MPEG-4_AVC.md "wikilink")，[MPEG-4
SL](../Page/MPEG-4_SL.md "wikilink") [SMPTE
VC1和](../Page/VC-1.md "wikilink")[MJPEG](../Page/Motion_JPEG.md "wikilink")。\[3\]

## 作業系統支援

### Linux

UVC在[Linux上的支援可參考](../Page/Linux.md "wikilink")[Linux UVC
driver](http://www.ideasonboard.org/uvc/)。

### Mac OS X

Mac OS X載有UVC
driver，其版本為10.4.3,至10.4.9版時，可與[iChat溝通](../Page/iChat.md "wikilink")。

### PlayStation 3

[PlayStation
3於](../Page/PlayStation_3.md "wikilink")1.54版時支援UVC相容webcams.

### Solaris

[Solaris支援UVC](../Page/Solaris.md "wikilink") webcams於[usbvc driver for
OpenSolaris](https://web.archive.org/web/20070823203924/http://www.opensolaris.org/os/community/device_drivers/projects/usb/uvc/)。其驅動程式可至[Solaris
Express下載](../Page/Solaris.md "wikilink")。

### Windows

Windows XP自Service Pack 2以後至Windows8支持如下表\[4\].

| UVC Version         | Windows Vista/XP | Windows 7     | Windows 8 |
| ------------------- | ---------------- | ------------- | --------- |
| USB Video Class 1.5 | Not supported    | Not supported | Supported |
| USB Video Class 1.1 | Not supported    | Supported     | Supported |
| USB Video Class 1.0 | Supported        | Supported     | Supported |

## 參考資料

<references/>

[Category:電腦硬體](../Category/電腦硬體.md "wikilink")

1.  [USB Device Class Definition for Video Devices,
    Revision 1.5](http://www.usb.org/developers/devclass_docs/USB_Video_Class_1_5.zip)
    , August 2012
2.  [USB Device Class Definition for Video Devices,
    Revision 1.1](http://www.usb.org/developers/devclass_docs/USB_Video_Class_1_1.zip)
    , June 2005
3.  [USB Device Class Definition for Video Devices,
    Revision 1.5](http://www.usb.org/developers/docs/devclass_docs/USB_Video_Class_1_5.zip)
    , June 2012
4.  \[<http://msdn.microsoft.com/en-us/library/windows/hardware/ff568651(v=vs.85>).aspx
    Microsoft USB Video Class Driver Overview\]