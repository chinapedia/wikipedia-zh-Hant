**U
Magazine**，是香港的一本消閒週刊，是香港[經濟日報集團出版旗下刊物](../Page/經濟日報.md "wikilink")，於2005年12月創刊，一書三冊，逢星期五出版。

主要內容包括[旅遊](../Page/旅遊.md "wikilink")、[飲食](../Page/飲食.md "wikilink")、優質生活、時裝潮流、人物及專欄，例如食肆介紹及特色飲食，世界各地旅遊專題等。

## 歷史

2018年10月，U
magazine發文指，香港永久居民到臺灣旅遊有新安排，網上填寫入境卡，毋須列印，[臺灣觀光局駐香港辦事處澄清](../Page/臺灣觀光局.md "wikilink")，持[BNO或港澳特區護照的旅客](../Page/BNO.md "wikilink")，需要辦理並列印[入台證](../Page/入台證.md "wikilink")。\[1\]

同年11月，U
magazine在[facebook呼籲網民留言以免費獲取](../Page/facebook.md "wikilink")[香港消委會](../Page/香港消委會.md "wikilink")[選擇月刊測試報告](../Page/選擇月刊.md "wikilink")，網民留言後會在私訊收到一份8頁之[PDF](../Page/PDF.md "wikilink")，在文件上有U
Magazine商標。\[2\]

## 主要內容

  - U Travel
  - U Food
  - U life
  - U People
  - U Style

## 獎項

  - 2011年 The Asian Publishing Awards
      - 最佳社交網絡應用大獎
  - 2011年 Magazine of the Year
      - 最佳旅遊雜誌第一位
      - 年度十大最佳雜誌
  - 2011年 Visit Britain Media Awards
      - 最佳旅遊報導獎
  - 2009年 Asia Travel & Tourism Creative Awards
      - 最佳旅遊攝影獎-風景組銅獎
  - 2008年 IFRA 7th Asia Media Awards
      - 最佳專題報導金獎（雜誌組）
      - 最佳封面設計金獎（雜誌組）
  - 2007年 The Asian Publishing Management Awards
      - 亞洲最佳新雜誌獎

## 同類競爭者

  - 《[飲食男女](../Page/飲食男女.md "wikilink")》
  - 《[新假期](../Page/新假期.md "wikilink")》

## 外部連結

  - [U Magazine Facebook專頁](http://www.facebook.com/umagazinehk)
  - [U magazine](http://www.umagazine.com.hk/)
  - [U Forum](http://www.umagazine.com.hk/forum.php)

[Category:旅遊指南](../Category/旅遊指南.md "wikilink")
[Category:香港雜誌](../Category/香港雜誌.md "wikilink")
[Category:香港經濟日報](../Category/香港經濟日報.md "wikilink")

1.
2.