**Ulead
PhotoImpact**（中文名：**Photo硬派**）是一个由[Ulead推出的](../Page/Ulead.md "wikilink")[图像处理](../Page/图像处理.md "wikilink")[软件](../Page/软件.md "wikilink")，由[友立資訊於](../Page/友立資訊.md "wikilink")1996年2月開發完成上市，运行于[Windows](../Page/Windows.md "wikilink")[操作系统下](../Page/操作系统.md "wikilink")，以家庭用户為主。

## 描述

用户可以在PhotoImpact上处理数字化图像。通过所谓[TWAIN接口](../Page/TWAIN.md "wikilink")，它可以直接从扫描仪或[数码相机调用照片](../Page/数码相机.md "wikilink")。在PhotoImpact的帮助下，用户可以制作自己的[网页](../Page/网页.md "wikilink")。

PhotoImpact支持友立資訊自己的.[UFO格式存储](../Page/UFO_\(消歧义\).md "wikilink")。

PhotoImpact与一些图像处理软件如[Corel PaintShop
Photo定位于同一消费群](../Page/Corel_PaintShop_Photo.md "wikilink")，在一些测试中常进行比较。

友立資訊在2006年底由[Corel控股](../Page/Corel.md "wikilink")，PhotoImpact也成为Corel旗下产品，完善了其产品线。

2008年，Corel推出PhotoImpact
X3，為PhotoImpact最終版本。2009年，PhotoImpact停止開發，其地位被Corel
PaintShop Pro取代。

## 版本歷史

| 版本                   | 發佈日期  |
| -------------------- | ----- |
| PhotoImpact 3        | 1996年 |
| PhotoImpact 4        | 1997年 |
| PhotoImpact 5        | 1999年 |
| PhotoImpact 6        | 2000年 |
| PhotoImpact 7        | 2001年 |
| PhotoImpact 8        | 2002年 |
| PhotoImpact XL（8.5）  | 2003年 |
| PhotoImpact 10       | 2004年 |
| PhotoImpact 11       | 2005年 |
| PhotoImpact 12       | 2006年 |
| PhotoImpact X3（或稱13） | 2008年 |

## 参见

  - [Corel](../Page/Corel.md "wikilink")
  - [友立资讯](../Page/友立资讯.md "wikilink")

## 外部链接

  - [PhotoImpact产品主页](http://www.corel.com/servlet/Satellite/tw/ct/Product/1251059137269)

[Category:图像软件](../Category/图像软件.md "wikilink")