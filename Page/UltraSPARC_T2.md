**UltraSPARC T2**（研發代號：**Niagara
II**）是[昇陽電腦所研發的](../Page/昇陽電腦.md "wikilink")[微處理器](../Page/微處理器.md "wikilink")，是一顆[多核心](../Page/多核心.md "wikilink")（執行核）、[多线程](../Page/多线程.md "wikilink")（多執行緒）的[CPU](../Page/CPU.md "wikilink")，采用[开放架构](../Page/开放架构.md "wikilink")。UltraSPARC
T2的前一款處理器為[UltraSPARC T1](../Page/UltraSPARC_T1.md "wikilink")。

UltraSPARC
T2衍生自[UltraSPARC系列微處理器](../Page/SPARC.md "wikilink")，這顆處理器內可以有8個CPU核心，且每個核心最多可同時執掌、處理8個執行緒，如此UltraSPARC
T2處理器在最理想狀態下可同時執行64個執行緒，而且，每個核心也有自屬的[浮點運算單元](../Page/浮點運算單元.md "wikilink")（[FPU](../Page/FPU.md "wikilink")）。同時[加密](../Page/加密.md "wikilink")、解密相關的執行處理單元也將由單核提升成多核。T2將使用65奈米製程，而且L2[快取記憶體增到](../Page/快取記憶體.md "wikilink")4MB.

2006年4月12日，[昇陽電腦宣佈完整的UltraSPARC](../Page/昇陽電腦.md "wikilink")
T2處理器已經進行試產（tape-out），且功率用電方面的特性表現將與現有UltraSPARC
T1相同，在效能方面，倘若執行已依據UltraSPARC T1架構而重新編譯、轉化的軟體，則UltraSPARC
T2的執行效率將會是UltraSPARC T1的兩倍；在浮点运算能力方面，UltraSPARC
T2每个核心均有一个[浮点运算单元](../Page/浮点运算单元.md "wikilink")（[FPU](../Page/FPU.md "wikilink")），相比UltraSPARC
T1八个核心共享一个浮点运算单元，其性能提升将是根本性的。

2006年5月26日，UltraSPARC
T2[處理器進入整機性](../Page/處理器.md "wikilink")（指將處理器裝入[伺服器內](../Page/伺服器.md "wikilink")）的系統測試，比原訂的8月底、9月底進行測試提前三個月，並預計在2007年能正式推出使用UltraSPARC
T2[處理器的](../Page/處理器.md "wikilink")[伺服器](../Page/伺服器.md "wikilink")。此外也已經得知UltraSPARC
T2將內建二個10Gbps的[乙太網路界面](../Page/乙太網路.md "wikilink")（簡稱：[10GbE](../Page/10GbE.md "wikilink")）。

2007年12月11日，Sun公司通过[OpenSPARC计划将UltraSPARC](../Page/OpenSPARC.md "wikilink")
T2处理器以[GPL开源协议发布](../Page/GPL.md "wikilink")\[1\]。

## 操作系统支持

  - [Solaris](../Page/Solaris.md "wikilink")
  - [Ubuntu](../Page/Ubuntu.md "wikilink")
  - [Debian](../Page/Debian.md "wikilink")
  - [OpenBSD](../Page/OpenBSD.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [opensparc](http://www.opensparc.net/)

[Category:Sun微处理器](../Category/Sun微处理器.md "wikilink")
[Category:开源微处理器](../Category/开源微处理器.md "wikilink")
[Category:SPARC微处理器架构](../Category/SPARC微处理器架构.md "wikilink")

1.