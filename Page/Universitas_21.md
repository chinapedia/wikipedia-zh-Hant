[Universitas_21_logo.png](https://zh.wikipedia.org/wiki/File:Universitas_21_logo.png "fig:Universitas_21_logo.png")
**Universitas
21，**簡稱**U21**，是一个由全球21所优秀的研究型[大学所组成的大學联盟](../Page/大学.md "wikilink")。这一联盟團體没有中文译名，仅以Universitas
21称呼之。这个联盟的宗旨是促進这些学校的教學、科學研究和学术水平；加强成员大學的世界交際能力；并且在成员之間建立国际性的共同标准和共识。

Universitias
21創立于1997年，至今成員分布於12个不同的国家暨地區，由21所大學所共同组成。最新加入的大學是印度[德里大學](../Page/德里大學.md "wikilink")，該校於2007年正式加入。这21所成员大學约有500,000名在校学生，40,000研究人员，并有约2百万名畢業校友。

## 成员列表

### 东亚

  -
    ****：[复旦大学](../Page/复旦大学.md "wikilink")、[上海交通大学](../Page/上海交通大学.md "wikilink")

<!-- end list -->

  -
    ****：[香港大學](../Page/香港大學.md "wikilink")

<!-- end list -->

  -
    ****：[新加坡国立大学](../Page/新加坡国立大学.md "wikilink")

<!-- end list -->

  -
    ****：[早稻田大学](../Page/早稻田大学.md "wikilink")

<!-- end list -->

  -
    ****：[高丽大学](../Page/高丽大学.md "wikilink")

### 南亚

  -
    ****：[德里大学](../Page/德里大学.md "wikilink")

### 欧洲

  -
    ****：[伯明翰大学](../Page/伯明翰大学.md "wikilink")、[爱丁堡大学](../Page/爱丁堡大学.md "wikilink")、[格拉斯哥大学](../Page/格拉斯哥大学.md "wikilink")、[诺丁漢大学](../Page/诺丁漢大学.md "wikilink")

<!-- end list -->

  -
    ****：[阿姆斯特丹大學](../Page/阿姆斯特丹大學.md "wikilink")

<!-- end list -->

  -
    ****：[都柏林大學](../Page/都柏林大學.md "wikilink")

<!-- end list -->

  -
    ****：[隆德大学](../Page/隆德大学.md "wikilink")

### 北美

  -
    ****：[不列颠哥伦比亚大学](../Page/不列颠哥伦比亚大学.md "wikilink")、[麦吉尔大学](../Page/麦吉尔大学.md "wikilink")

<!-- end list -->

  -
    ****：[马里兰大学](../Page/马里兰大学.md "wikilink")、[康涅狄格大学](../Page/康涅狄格大学.md "wikilink")、[俄亥俄州立大学](../Page/俄亥俄州立大学.md "wikilink")

<!-- end list -->

  -
    ****：[蒙特雷理工大学](../Page/蒙特雷理工大学.md "wikilink")

### 南美

  -
    ****：[智利天主教大学](../Page/智利天主教大学.md "wikilink")

### 大洋洲

  -
    ****：[墨尔本大学](../Page/墨尔本大学.md "wikilink")、[昆士兰大学](../Page/昆士兰大学.md "wikilink")、[新南威尔士大学](../Page/新南威尔士大学.md "wikilink")

<!-- end list -->

  -
    ****：[奥克兰大学](../Page/奥克兰大学.md "wikilink")

### 非洲

  -
    ****：[约翰内斯堡大学](../Page/约翰内斯堡大学.md "wikilink")

## 外部連結

  - [Universitas 21 網站](http://www.universitas21.com/)

[Category:大学](../Category/大学.md "wikilink")