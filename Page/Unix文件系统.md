**Unix文件系统**（，縮寫為UFS），是一种[文件系统](../Page/文件系统.md "wikilink")，為许多[UNIX和](../Page/UNIX.md "wikilink")[类Unix操作系统所使用](../Page/类Unix.md "wikilink")。它也被称为**[伯克利快速文件系统](../Page/加州大學柏克萊分校.md "wikilink")**（Berkeley
Fast File System），**[BSD快速文件系统](../Page/BSD.md "wikilink")**（BSD Fast
File System），縮寫為**FFS**。

FFS对Unix System
V文件系统“FS”有所继承。FFS在Unix文件系统（1或2）之上，提供目录结构的信息，以及各种磁盘访问的优化。UFS（以及UFS2）定义了on-disk数据规格。

## 参见

  - [文件系统](../Page/文件系统.md "wikilink")

## 外部链接

  - Marshall Kirk McKusick、William N. Joy、Samuel J. Leffler以及Robert S.
    Fabry，[A Fast File System for
    UNIX](http://citeseer.ist.psu.edu/mckusick84fast.html) *Transactions
    on Computer Systems*, vol. 2, num. 3, Aug. 1984, pp. 181-197.
  - Linux文档计划（The Linux Documentation Project），[Filesystems HOWTO:
    FFS](http://www.tldp.org/HOWTO/Filesystems-HOWTO-9.html#ffs).
  - [Little UFS2 FAQ:
    UFS与FFS的区别是什么？](https://web.archive.org/web/20040113105449/http://sixshooter.v6.thrupoint.net/jeroen/faq.html#UFS-DIFF-FFS)

[Category:磁盘文件系统](../Category/磁盘文件系统.md "wikilink")