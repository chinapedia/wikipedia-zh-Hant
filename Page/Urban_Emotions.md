《**Urban Emotions**》
是[張敬軒的第八張大碟](../Page/張敬軒.md "wikilink")，於2008年7月11日推出。在9月4日推出內地版時加上中文名稱《**他的故事**》，並刪去《狐》及《鬧鬼愛情》兩首歌曲。第二版於2008年9月12日推出。

## 曲目

<div class="references-small">

</div>

## 相關網站

  - [大碟曲目及封面](http://www.hmv.com.hk/product/canto.asp?sku=161164)

[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:張敬軒音樂專輯](../Category/張敬軒音樂專輯.md "wikilink")
[Category:2008年音樂專輯](../Category/2008年音樂專輯.md "wikilink")