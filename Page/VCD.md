**影音光碟**（），又稱影音壓縮光碟，是一種在[光碟](../Page/CD.md "wikilink")（Compact
Disc）上存儲[視訊信息的標準](../Page/視訊.md "wikilink")。VCD可以在[個人電腦和](../Page/個人電腦.md "wikilink")[VCD播放器](../Page/VCD播放器.md "wikilink")\[1\]以及大部分[DVD播放機中播放](../Page/DVD播放機.md "wikilink")，但大部分[藍光光碟播放機已不支援VCD](../Page/藍光光碟.md "wikilink")。

VCD標準由[索尼](../Page/索尼公司.md "wikilink")、[飛利浦](../Page/飛利浦.md "wikilink")、[JVC](../Page/JVC.md "wikilink")、[松下電器等電器生產廠商聯合於](../Page/松下電器.md "wikilink")1993年制定，屬於數位光碟的[白皮書標準](../Page/CD行業標準.md "wikilink")。

## 技術規範

VCD是全動態、全屏播放的視頻標準。格式可分為：

  - 分辨率為352×240[像素](../Page/像素.md "wikilink")，每秒29.97幅畫面（適合[NTSC制式電視播放](../Page/NTSC制式.md "wikilink")）
  - 分辨率為352×240像素，每秒23.976幅畫面（適合電影每秒24格的影片）
  - 分辨率為352×288像素，每秒25幅畫面（適合[PAL制式電視播放](../Page/PAL制式.md "wikilink")）

整體來說，分辨率大約是對應電視制式分辨率的四分之一。VCD的視頻採用[MPEG-1壓縮編碼](../Page/MPEG-1.md "wikilink")，音頻採用MPEG
1/2 Layer
2（[MP2](../Page/MPEG-1_Audio_Layer_II.md "wikilink")）編碼，碼率分別為視頻1150[kbit/s](../Page/比特率.md "wikilink")、音頻224kbit/s。

VCD的[比特率和普通](../Page/比特率.md "wikilink")[音樂CD相當](../Page/音樂CD.md "wikilink")，因此一張標準的74分鐘的CD可以存放大約74分鐘的VCD格式影片。

## 標準

[Return_button_on_vcd_player.JPG](https://zh.wikipedia.org/wiki/File:Return_button_on_vcd_player.JPG "fig:Return_button_on_vcd_player.JPG")
 VCD有三個正式標準：1.0、1.1和2.0：

  - 1.0：原始版本。
  - 1.1：修正版。
  - 2.0：增加播放選單功能（**P**lay **B**ack **C**ontrol,
    **PBC**）、清晰靜止畫面播放功能（704x480或704x576像素）和支援352x288/25fps格式。

## 應用

VCD僅在[亞洲地區被廣泛使用](../Page/亞洲.md "wikilink")。在部份國家或地區，VCD和[DVD在發行市場上已完全取代](../Page/DVD.md "wikilink")[錄影帶和](../Page/錄影帶.md "wikilink")[鐳射影碟的地位](../Page/鐳射影碟.md "wikilink")。

VCD因為除了[東南亞和](../Page/東南亞.md "wikilink")[大中華地區外](../Page/大中華地區.md "wikilink")，從未被普及過便直接從錄影帶過渡向DVD，所以被認為會終將在全球被DVD所取代。但是由於它的一些特點，還將保持一段時間的市場。2010年代中才漸逐停產。

  - VCD沒有像[DVD區域碼一樣的](../Page/DVD區域碼.md "wikilink")[區域限制](../Page/區域限制.md "wikilink")，這意味著它可以在任何兼容機器上觀看。
  - 有些節目因為成本問題，不會製作[DVD或VHS](../Page/DVD.md "wikilink")[錄像帶版本](../Page/錄像帶.md "wikilink")，購買者只能購買VCD版本。
  - VCD比DVD廉價，當然VCD缺乏DVD提供的很多額外特性，如[字幕](../Page/字幕.md "wikilink")、選單功能（用作「製作花絮」等）等（VCD
    2.0 加入簡單的選單功能（Play Back Control, PBC），但功能不及DVD豐富）。
  - 畫面比例是預設的，所以在[電腦上會自動調節成正確比例](../Page/電腦.md "wikilink")。

另外，一些厂商也推出了“游戏VCD”，可以运行带有[任天堂](../Page/任天堂.md "wikilink")[红白机或](../Page/红白机.md "wikilink")[世嘉](../Page/世嘉.md "wikilink")[MD游戏的VCD光盘](../Page/Mega_Drive.md "wikilink")，只要接上手柄就可以进行游戏。

## SVCD

SVCD是VCD的改進標準，它採用[MPEG-2壓縮](../Page/MPEG-2.md "wikilink")，採用可變壓縮率來獲得較好的影片品質。

## 参见

  - [VCD-BOX](../Page/VCD-BOX.md "wikilink")

## 參考來源

## 外部連結

  - [什麼是VCD，From Videohelp.com](http://www.videohelp.com/vcd)

[Category:電腦儲存媒體](../Category/電腦儲存媒體.md "wikilink")
[Category:影像儲存](../Category/影像儲存.md "wikilink")

1.  [80后记忆犹新
    盘点那些逝去的家电品牌-中关村在线](http://m.zol.com.cn/article/5206786.html)