<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p>VF-14吸血鬼戰鬥機／Fz-109</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Fz-109A.gif" title="fig:Fz-109A.gif">Fz-109A.gif</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>——VF-14諸元——<br />
（F：戰鬥機G：半人形B：機器人）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>功能</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>乘員</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>戰鬥機</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>長</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>翼展</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>高</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>空機重量</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>性能</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>動力：</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>極速（10000公尺）</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>極速（30000公尺）</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>爬升率：</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>：機體強度：</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>武裝</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>固定武裝</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>附加武裝</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>————Fz-109諸元————[1]</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>長、翼展、高、性能</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>武裝</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>固定武裝</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>附加武裝</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

[Lockheed_SR-71_Blackbird.jpg](https://zh.wikipedia.org/wiki/File:Lockheed_SR-71_Blackbird.jpg "fig:Lockheed_SR-71_Blackbird.jpg")

**VF-14吸血鬼戰鬥機**是日本[超時空要塞系列動畫動畫中與](../Page/超時空要塞.md "wikilink")[VF-11競標](../Page/VF-11雷霆式戰鬥機.md "wikilink")2030年代主力戰鬥機的失敗者。其設計與[VF-4閃電式戰鬥機同樣修改自](../Page/VF-4閃電三式.md "wikilink")[洛克希德公司的](../Page/洛克希德公司.md "wikilink")[黑鳥式偵察機](../Page/SR-71黑鳥式偵察機.md "wikilink")，命名則是援引自英國迪海維蘭公司於第二次世界大戰末期生產的[吸血鬼戰鬥機](../Page/吸血鬼戰鬥機.md "wikilink")。本機首次登場於[Macross
Plus](../Page/Macross_Plus.md "wikilink")，僅作為背景停放於新愛德華航空基地的停機坪上。之後，在Macross7中，則是以Fz-109的編號作為反派勢力的主力戰鬥機登場。由於在各系列作品中皆非主流機種，故甚少為一般觀眾所知悉。

## 概要

21世紀20年代，做為統合軍新一代主力的[VF-4與](../Page/VF-4閃電三式.md "wikilink")[VF-5000已逐漸汰換老舊的](../Page/VF-5000.md "wikilink")[VF-1女武神陸續成軍](../Page/VF-1女武神.md "wikilink")。然而，由於VF-4的大氣圈內性能不盡人意，而VF-5000對宇宙作戰適應不良，故統合政府開始對新一代的通用機種展開招標，以取代以上兩者的任務。由[通用銀河公司的新星重工的VF](../Page/通用电气.md "wikilink")-X-11及YF-14吸血鬼競標，最後由前者勝出。

儘管VF-X-11贏得了競標的勝利，然而，卻並不代表其性能超越YF-14。除購置價格、生產性與維修性方面外。在最高速度及加速性能及航程等……各項性能方面，皆由YF-14領先。也就是看中了這一點，統合政府在選擇了VF-X-11之後，仍然購買了少量VF-14並做有限的配備，並配置於部分以[傑特拉帝人為主的宇宙調查船團及超長距離移民船團](../Page/傑特拉帝人.md "wikilink")；於2030年停產後仍授權予部分殖民地自行生產運用。

2045年的邊境衝突中，本機充分展現了其優秀的性能。原始惡魔藉由擄獲Meagroad-13上的VF-14進行改裝後大量生產，並賦予Fz-109之編號，與當年在競標中的優勝者(VF-11系列)對抗，展現出了不凡的性能，並使後者陷於苦戰。此外，VF-14亦有接受增加裝甲的改裝。改裝後的VF-14重新編號為VA-14並作為[攻击机使用](../Page/攻击机.md "wikilink")。

## 釋注

<div class="references-small">

<references />

</div>

## 參考資料

マクロス７アニメーション資料集\[M\]。小学館，1995年。ISBN 978-4-09-101583-9

## 登場作品

  - [Macross Plus](../Page/Macross_Plus.md "wikilink")
  - [Macross 7](../Page/超時空要塞_7.md "wikilink")
  - [Macross M3](../Page/Macross_M3.md "wikilink")

## 相關機種

  - [VF-11雷霆式戰鬥機](../Page/VF-11雷霆式戰鬥機.md "wikilink")
  - [YF-19/VF-19王者之劍](../Page/YF-19/VF-19王者之劍.md "wikilink")
  - [YF-21/VF-22雨燕](../Page/YF-21/VF-22雨燕.md "wikilink")
  - [SR-71黑鳥式偵察機](../Page/SR-71黑鳥式偵察機.md "wikilink")

## 外部連結

  - [HAHQ上關於VF-14的資訊](https://web.archive.org/web/20070706015607/http://www.mahq.net/mecha/macross/m3/vf-14.htm)
  - [HAHQ上關於Fz-109的資訊](http://www.mahq.net/mecha/macross/macross7/fz-109a.htm)
  - [UN
    Database上關於VF-14的資訊](https://web.archive.org/web/20070926234116/http://unsd.macrossroleplay.org/vf-14.html)
  - [大陸的Fz-109性能資料](https://web.archive.org/web/20070928225329/http://maccity7.com/mech/fz-109.asp)
  - [Macross Compedium的機械設定解說](http://macross.anime.net/mecha/)

[Category:超時空要塞兵器](../Category/超時空要塞兵器.md "wikilink")

1.  擄獲機體資料