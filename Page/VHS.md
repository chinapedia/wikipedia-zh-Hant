[JVC-VHS_Cassette001.JPG](https://zh.wikipedia.org/wiki/File:JVC-VHS_Cassette001.JPG "fig:JVC-VHS_Cassette001.JPG")

[VHS_Internal.jpg](https://zh.wikipedia.org/wiki/File:VHS_Internal.jpg "fig:VHS_Internal.jpg")
[VHSTapeOpen.jpg](https://zh.wikipedia.org/wiki/File:VHSTapeOpen.jpg "fig:VHSTapeOpen.jpg")

**VHS**是**Video Home
System**的缩写，意为**家用录像系统**。它是由日本[JVC公司在](../Page/JVC.md "wikilink")1976年开发的一种家用[录像机录制和播放标准](../Page/录像机.md "wikilink")。

## 概要

虽然VHS的官方翻译就是家用录像系统，但是最初VHS代表Vertical Helical
Scan（垂直螺旋扫描）的意思，因为它采用了磁头／磁带垂直扫描的技术。有些早期的报道声称，VHS是Victor
Helical Scan（胜利螺旋扫描）系统的意思，因为JVC的V就是Victor（胜利）的意思。

1980年代，在经历了和[索尼公司的](../Page/索尼公司.md "wikilink")[Betamax格式以及](../Page/Betamax.md "wikilink")[飞利浦的](../Page/飞利浦.md "wikilink")[Video
2000格式的竞争之后](../Page/Video_2000.md "wikilink")，VHS成为家用录像机的标准格式。VHS提供了比Betamax格式更长的播放时间，同时磁带传送机构又没有Betamax那么复杂。VHS比Betamax的快进和后退速度要快很多，因为在磁带高速卷动之前，播放磁头已经离开了磁带。畫質方面则是Betamax格式比較好。

VHS盒式[录影带裡的](../Page/录影带.md "wikilink")[磁带宽](../Page/磁带.md "wikilink")12.65毫米（通常被称为大½英寸格式），磁带在播放的时候会经过录像磁头或者播放磁头。VHS格式的[带宽大约为](../Page/带宽.md "wikilink")3[MHz](../Page/MHz.md "wikilink")，水平分辨率大约240线。VHS的垂直分辨率由电视的制式所决定，[NTSC制式下为](../Page/NTSC制式.md "wikilink")486线，[PAL制式下为](../Page/PAL制式.md "wikilink")576线。

VHS有一些改进的标准，例如改进的模拟标准S-VHS和用VHS磁带记录数字视频的D-VHS。也有可以将[个人电脑和VHS录像机相连接用来备份数据的设备](../Page/个人电脑.md "wikilink")。

还有一种小型的VHS录影带格式VHS-C（C指的是Compact），它和标准的VHS使用同样宽度的磁带，可以用过适配器在普通录像机上观看，但是它的体积-{只}-有92毫米×69毫米×23毫米，比标准的VHS录影带又减小了很多，可以用在手持式摄像机等设备上。这个格式在某些方面阻碍了Betamax格式的销售，因为Betamax格式的磁带的尺寸很难做小。

VHS在美国还有较多的市场，还有很多录影带租赁企业提供VHS录影带的租借。在亚洲，VHS已经被[VCD](../Page/VCD.md "wikilink")、[DVD甚至](../Page/DVD.md "wikilink")[Blu-ray等所淘汰](../Page/Blu-ray.md "wikilink")。在[摄像机格式方面](../Page/摄像机.md "wikilink")，[DV数字视频已取代了VHS](../Page/DV.md "wikilink")-C格式，近年來更流行內建硬碟或記憶體而不需要磁帶的攝影機。

## 改良

在美國和日本VHS繼續流行，是因為開發了能夠兼容傳統VHS錄影帶的改良型，即S-VHS和D-VHS。D-VHS更可以錄播[高清電視節目](../Page/高清電視.md "wikilink")，並有極長的錄影時間（超過二十四小時）。亦有適應高清電視畫質的正版D-VHS錄影帶租售。但這些機器和磁帶價格高昂，又不能直接兼容其他國家版本的高清電視，所以在兩國外並未普及。

後來因為BD燒錄器和BD錄影機出現，D-VHS亦被冷落了，但仍然受D-VHS愛好者青睞。

## 容量

| 錄影帶標籤 | 錄影帶長度             | 錄影時間（[NTSC模式](../Page/NTSC.md "wikilink")） | 錄影時間（[PAL模式](../Page/PAL.md "wikilink")）   |
| ----- | ----------------- | ------------------------------------------ | ------------------------------------------ |
| SP    | LP                | EP/SLP                                     | SP                                         |
| T-120 | 812 ft (247.5 m)  | 2:00                                       | 4:00                                       |
| T-160 | 1075 ft (327.7 m) | 2:40                                       | 5:20                                       |
| T-180 | 1210 ft (368.8 m) | 3:00                                       | 6:00                                       |
| T-210 | 1421 ft (433.1 m) | 3:30                                       | 7:00                                       |
| 錄影帶標籤 | 錄影帶長度             | 錄影時間（[PAL模式](../Page/PAL.md "wikilink")）   | 錄影時間（[NTSC模式](../Page/NTSC.md "wikilink")） |
| SP    | ——                | LP                                         | SP                                         |
| E-120 | 570 ft (173.7 m)  | 2:00                                       |                                            |
| E-180 | 851 ft (259.4 m)  | 3:00                                       |                                            |
| E-240 | 1142 ft (348.1 m) | 4:00                                       |                                            |
|       |                   |                                            |                                            |

## 相關條目

  - [IEEE里程碑列表](../Page/IEEE里程碑列表.md "wikilink")

[Category:影像儲存](../Category/影像儲存.md "wikilink")
[Category:IEEE里程碑](../Category/IEEE里程碑.md "wikilink")
[Category:1976年面世的產品](../Category/1976年面世的產品.md "wikilink")