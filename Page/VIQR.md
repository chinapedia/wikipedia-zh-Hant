**可讀越語引述法**（英：***VI**etnamese
**Q**uoted-**R**eadable*，**VIQR**）是一套使用[ASCII字符来书写](../Page/ASCII.md "wikilink")[越南语的做法](../Page/越南语.md "wikilink")。越南语包含大量不在ASCII的字母；VIQR则提供一个方法，在打完本来没有[附加符号的字母後](../Page/附加符号.md "wikilink")，加上1或2个符号去代表附加符号。

## VIQR的附加符號

<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p>VIQR的附加符号</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>附加符号</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>（<a href="../Page/短音符.md" title="wikilink">短音符</a>，breve）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>（<a href="../Page/抑揚符.md" title="wikilink">抑揚符</a>，circumflex）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>(horn)</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>玄聲 （<a href="../Page/重音符.md" title="wikilink">重音符</a>，grave）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>銳聲 （<a href="../Page/尖音符.md" title="wikilink">尖音符</a>，acute）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>問聲 （<a href="../Page/鉤號.md" title="wikilink">鉤號</a>，hook）</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>跌聲 （<a href="../Page/波浪號.md" title="wikilink">波浪號</a>，tilde）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>重聲 （<a href="../Page/下句點.md" title="wikilink">下句點</a>，dot below）</p></td>
</tr>
</tbody>
</table>

VIQR 以 `DD` 或 `Dd` 代表越南语字母  和以 `dd` 代表
。如要输入部分[标点符号](../Page/标点符号.md "wikilink")（句点、问号、撇號、斜号、开括号和引号），需要在该标点符号之前先加一个反斜号
(`\`) ，以避免被当成为附加符号字母。例子：

  -

      -
        *您叫什麼名字？我叫 Teddy Thuy。*

与[越南資訊交換標準代碼不同的是](../Page/越南資訊交換標準代碼.md "wikilink")，VIQR不是[字符集](../Page/字符集.md "wikilink")。相反，VIQR是以ASCII来打出越南语的方法。VIQR相比于越南資訊交換標準代碼等字符集的好处是，它无须安装特别的键盘或软件。但是，当[統一碼在全球的电脑使用变得越益流行时](../Page/統一碼.md "wikilink")，VIQR便演变成为一些软件的越南语输入法，软件接收了键盤的指令後再把它转为統一碼。

## 历史

在20世纪90年代初，一种被称为Vietnet的助记符专用系统在越南网络[邮件列表和](../Page/邮件列表.md "wikilink")[soc.culture.vietnamese](news://soc.culture.vietnamese/)组中被使用\[1\]\[2\]。

1992年，位于[加利福尼亚州TriChlor软件集团的越南标准化组织](../Page/加利福尼亚州.md "wikilink")（Viet-Std，''Nhóm
Nghiên Cứu Tiêu Chuẩn Tiếng Việt）正式确定了VIQR公约。它在RFC 1456中明文描述了它。

## 註釋

## 参看

  - [越南資訊交換標準代碼](../Page/越南資訊交換標準代碼.md "wikilink")
  - [VNI](../Page/VNI.md "wikilink")

## 外部链接

  - RFC 1456 – Conventions for Encoding the Vietnamese Language

  - [VIQR 慣例](http://vietunicode.sourceforge.net/charset/viqr.html)

  - [AnGiang
    軟體](https://web.archive.org/web/20060213114637/http://xmlunify.com/angiang_ss2.htm)

  -   - [Vietnamese Character Encoding Standardization Report – VISCII
        and VIQR 1.1 Character Encoding
        Specifications](http://vietstd.sourceforge.net/report/rep92.htm)
        (English and Vietnamese)
      - [The VIQR
        Convention](http://vietstd.sourceforge.net/document/viqr.htm)

  - [Free Online VIQR to Unicode
    Converter](http://www.enderminh.com/minh/vnconversions.aspx)

  - [Help page on inputting Vietnamese text in Vietnamese
    Wikipedia](../Page/:vi:Wikipedia:Gõ_tiếng_Việt.md "wikilink")

[Category:越南語輸入法](../Category/越南語輸入法.md "wikilink")

1.
2.