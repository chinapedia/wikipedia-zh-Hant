**Verdana**，是一套[無襯線字體](../Page/無襯線字體.md "wikilink")，由於它在小字上仍有結構清晰端整、閱讀辨識容易等高品質的表現，因而在1996年推出後即迅速成為許多領域所愛用的標準字型之一。

此字體的設計源起於[微軟字型設計小組的](../Page/微軟.md "wikilink")[維吉尼亞·惠烈](../Page/維吉尼亞·惠烈.md "wikilink")（Virginia
Howlett）希望設計一套具有高辨識性、易讀性的新字型以供[螢幕顯示之用](../Page/螢幕.md "wikilink")，於是她邀請了世界頂級字型設計師之一的[馬修·卡特](../Page/馬修·卡特.md "wikilink")（Matthew
Carter）操刀，以[Frutiger字體及](../Page/Frutiger.md "wikilink")[愛德華·約翰斯頓](../Page/愛德華·約翰斯頓.md "wikilink")（Edward
Johnston）為[倫敦地鐵所設計的字體為藍本](../Page/倫敦地鐵.md "wikilink")，並由[Monotype公司的字型微調](../Page/Monotype.md "wikilink")（Hint）專家[湯姆·瑞克納](../Page/湯姆·瑞克納.md "wikilink")（Tom
Rickner）擔任手工微調，字體結構與[Tahoma](../Page/Tahoma.md "wikilink")（同為馬修·卡特所設計）很相似，微軟將Verdana納入[網頁核心字體之一](../Page/網頁核心字體.md "wikilink")。

「Verdana」一名是由「verdant」和「Ana」兩字所組成的。「verdant」意為「蒼翠」，象徵著「[翡翠之城](../Page/翡翠之城.md "wikilink")」[西雅圖及有](../Page/西雅圖.md "wikilink")「[常青州](../Page/常青州.md "wikilink")」之稱的[華盛頓州](../Page/華盛頓州.md "wikilink")。「Ana」則來自於維吉尼亞·惠烈[大女兒的名字](../Page/女兒.md "wikilink")。

## 外部連結

  - [與馬修·卡特的訪談](https://web.archive.org/web/20130828210842/http://www.will-harris.com/verdana-georgia.htm)
  - [與維吉尼亞·惠烈的訪談](http://www.dmxzone.com/go?6669)
  - [Verdana字體資訊](http://www.microsoft.com/typography/fonts/font.aspx?FID=1&FNAME=Verdana)
  - [Channel
    Verdana](http://www.microsoft.com/typography/web/fonts/verdana/default.htm)
  - [下載Verdana](http://prdownloads.sourceforge.net/corefonts/verdan32.exe?download)

[Category:无衬线字体](../Category/无衬线字体.md "wikilink")
[Category:微软字体](../Category/微软字体.md "wikilink")
[Category:網頁核心字型](../Category/網頁核心字型.md "wikilink")
[Category:Windows XP字體](../Category/Windows_XP字體.md "wikilink")