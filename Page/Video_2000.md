**Video 2000**（或称作**V2000**、**Video Compact
Cassette**、**VCC**）是一种由[飞利浦与](../Page/飞利浦.md "wikilink")[Grundig公司开发出的一种与](../Page/Grundig.md "wikilink")[JVC的](../Page/JVC.md "wikilink")[VHS格式和](../Page/VHS.md "wikilink")[索尼的](../Page/索尼.md "wikilink")[Betamax格式竞争的一种家用录像带格式标准](../Page/Betamax.md "wikilink")。其上市贩售始于1979年，终于1988年。它们-{只}-有在[欧洲与](../Page/欧洲.md "wikilink")[阿根廷上市贩售](../Page/阿根廷.md "wikilink")。

飞利浦公司原先将其命名为VCC(Video Compact
Cassette)以延续他们在1963年注册的另一[商标](../Page/商标.md "wikilink")：ACC（[Audio
Compact
Cassette](../Page/Audio_Compact_Cassette.md "wikilink")）。然而，两个商标皆被证明无法成名，且飞利浦公司主要以**Video
2000**这个注册商标销售此一录像系统，而Grundig一开始使用2x4这个名字，表示其录制时长可达2x4个小时。VCC/V2000/2x4接续并取代了飞利浦公司先前研发的[VCR](../Page/VCR.md "wikilink")、[VCR-LP以及Grundig的](../Page/VCR-LP.md "wikilink")[SVR格式](../Page/SVR.md "wikilink")。

## 技术上的革新

Video 2000提供了几种在竟争对手－VHS与Betamax－格式中所没有的创新技术：

  - 所有的VCC影带皆将视频与音频信号记录在[磁带的一侧](../Page/磁带.md "wikilink")。V2000以[螺旋扫描方式扫过一半的磁带](../Page/螺旋扫描.md "wikilink")。经由手动翻面，它可以扫描另一半，因此使得可录时长倍增。
  - 其特有的动态寻轨（DTF - Dynamic Track
    Following）技术（有一个在前、可动的磁头）使得V2000不需要视频轨控制。请注意：某些V2000机种并没有此功能。
  - 所有的V2000机种皆支持自动回带（后来VHS与Betamax亦跟进）。
  - 优秀的动态降噪（[磁带噪声](../Page/磁带噪声.md "wikilink")）功能。
  - 视频轨旁包含一数据轨。

由于DTF的关系，V2000可以在停格播放时同时播放两个[视频场](../Page/视频场.md "wikilink")，并保持完整的垂直分辨率。而VHS与Betamax-{只}-能播放一个视频场及保持一半的垂直[分辨率](../Page/分辨率.md "wikilink")。实际上，这项功能弊多于利，因为非影片的场是同一时间内隔开，在没有新式的数码纠错下，同时播放它们会造成屏幕[闪烁](../Page/闪烁.md "wikilink")。DTF的一个真正好处是可以在屏幕上不出现错轨线的情况下进行图像寻找。这个功能没有任何家用VHS或Betamax机种可以完全做到。

值得注意地，飞利浦与Grundig采用了一种共同的影带格式，但机械设计上却有着根本上的不同。第一台Grundig生产的机器采用了类似Betamax的进带环将磁带环绕在磁头旁，而飞利浦则采用类似VHS的M型进带方式。

在Video 2000停止生产前不久，飞利浦推出了一款长磁带－V2000
XL。它每面有8个小时的录制时长。飞利浦另外创造了一种可以透过完整尺寸的转接器在已有的播放机上播放的更小型的V2000影带[原型](../Page/原型.md "wikilink")（类似于[VHS-C](../Page/VHS-C.md "wikilink")）。然而飞利浦在它开发至可以上市之前中止了Video
2000的生产。

虽然部份机种支持线性[立体声](../Page/立体声.md "wikilink")，Hi-Fi立体声机种却从未上市。而在1980年代中期，VHS与Betamax皆提供了接近CD音质的立体双声道。

## VCC影带的构造

尽管名叫Video Compact
Cassette，它或多或少地比VHS大。尺寸上比VHS窄5公厘，但高出一毫米且深6公厘。\[1\]\[2\]这种影带有2卷半英寸（12.5公厘）宽的[二氧化铬](../Page/二氧化铬.md "wikilink")[磁带](../Page/磁带.md "wikilink")。这种格式每*单面*只使用四分之一英寸（6.25公厘），所以它有时也被称为四分之一英寸磁带，尽管它实际宽度是二分之一英寸。

VHS与Betamax影带上有一个扳片。用户可以扳断它以防止已录好的节目被洗掉或覆盖。若扳片已被扳断，在下一次使用该录像带录影前必须填满或覆盖该处空隙。VCC的设计则较有弹性，它有一个开关可以指示一次录影需保留的长度。这项技术在后来的[Video8](../Page/Video8.md "wikilink")、[MiniDV以及](../Page/MiniDV.md "wikilink")[MicroMV录像带上亦被应用](../Page/MicroMV.md "wikilink")。

## Video 2000与录像带格式战

飞利浦在1979年推出了第一款Video 2000的录像机－VR2000。一些其他的机种接着由飞利浦、Grundig与[Bang &
Olufsen三家公司生产](../Page/Bang_&_Olufsen.md "wikilink")。但最终于1988年全部停产。Video
2000最终输了这场[录像带格式战](../Page/录像带格式战.md "wikilink")，Betamax也在不久后步上它的后尘。
[Vr2020.jpg](https://zh.wikipedia.org/wiki/File:Vr2020.jpg "fig:Vr2020.jpg")

V2000的失败有一部份可归因于其较慢进入市场（由于DTF系统开发时碰到的问题而耽误）。另外，虽然它在部份技术上胜过它的竟争对手，但它仍不能与VHS和Betamax的主要优势相比：

  - VHS与Betamax已经瓜分了大部份市场并有大量预录好的影像库
  - VHS与Betamax的[显示分辨率较高](../Page/显示分辨率.md "wikilink")。
  - VHS与Betamax录像机被认为更可靠。
  - Betamax[摄像机早已上市](../Page/摄像机.md "wikilink")
  - VHS与Betamax皆在全球销售。

V2000格式一个最主要的目的－特别是包含DTF功能的－即是"影带兼容性"：在任何机器上录制的影带应该要能在任何其他机器上正常播放。不幸的是，当飞利浦的第一种机型－VR2020上市时，它被发现与一年前上市的Grundig
2x4录像机相比，它的音频磁头偏离了2.5公厘。两家公司后来的机种将音频磁头各移动了1.25公厘至一个相同的位置，但这使得旧型机器留下的兼容性问题依然存在。\[3\]尤有甚者，DTF系统需要的接近偏差与脆弱性造成严重的机器内部兼容性问题，而此问题从未被完善解决。

在1980年代下半叶，飞利浦公司已经开始生产他们的VHS兼容机种了。

## 附注

<references/>

## 外部链接

  - [Mikey's Vintage VTR Page -
    Video 2000](http://www.oldtechnology.net/v2000.html)
  - [Video 2000 page at Total Rewind - The Virtual Museum of Vintage
    VCRs](http://www.totalrewind.org/v2000.htm)
  - [V2000 PALsite](http://v2000.palsite.com/) - Information about the
    V2000 video format
  - [Interview of Rudolf
    Drabek](http://www.ieee.org/portal/cms_docs_iportals/iportals/aboutus/history_center/oral_history/pdfs/Drabek290.pdf)，V2000
    developer, July 1996, Retrieved on March 26，2007
  - [V2000 WebSite Only](http://steelbull.byethost18.com/) - Dedicated
    site on V2000 format (Foreign-language)
  - [VHS Community](http://www.vhs-std.com/)

[Category:飞利浦](../Category/飞利浦.md "wikilink")
[Category:影像儲存](../Category/影像儲存.md "wikilink")
[Category:1979年面世的產品](../Category/1979年面世的產品.md "wikilink")

1.  ["V2000 PALsite"](http://v2000.palsite.com/) accessed January 3,
    2007, lists the VCC dimensions: 183 mm x 26 mm x 110 mm
2.  [VHS_e.htm "VHS Community:
    VHS 1976"](http://www.vhs-std.com/english/VHS_E/p1) accessed January
    3, 2007, lists the VHS cassette dimensions: 188 mm x 25 mm x 104 mm
3.  Dean, Richard. *Home Video* (Newnes Technical Books, 1982), page 18