**Vii**，又名**威力棒**，是[中國](../Page/中國.md "wikilink")[广州](../Page/广州.md "wikilink")[京仕敦电子科技有限公司生產製造的](../Page/京仕敦电子科技有限公司.md "wikilink")[電子遊戲](../Page/電子遊戲.md "wikilink")[遊戲機](../Page/遊戲機.md "wikilink")，外观酷似[任天堂的](../Page/任天堂.md "wikilink")[Wii](../Page/Wii.md "wikilink")，主要针对[儿童用户](../Page/儿童.md "wikilink")。於2007年12月发售。由于其夸张的宣传和实质不符而广受诟病。\[1\]

## 基本情况

Vii的主机和手柄的外观都与Wii高度相似，主机尺寸190\*150\*48毫米，手柄尺寸140\*35\*34毫米，都略小于wii，主机和手柄的按键布局也与wii基本相同。\[2\]

Vii的最初零售定价为1280[人民币](../Page/人民币.md "wikilink")，亦有优惠价799元人民币的版本，主要通过[电视购物销售](../Page/电视购物.md "wikilink")。在[广告中](../Page/广告.md "wikilink")，商家主要宣传了Vii是所谓的有独立研发生产的动作感应系统，可以捕捉玩家的身体动作，而且其手柄具有震动功能。而事实上，Vii的手柄只是一只普通手柄，而且也并不具有动作感应功能（這就是它不需要紅外線感應的原因）。另外，Vii是沒有雙打功能的。

有玩家通过拆卸Vii主机发现，其[集成电路](../Page/集成电路.md "wikilink")[面积不到](../Page/面积.md "wikilink")[体积的](../Page/体积.md "wikilink")1/5，且电路设计十分简单，基座部分还有厂商为了增加主机重量而放入的铁块。\[3\]

Vii内置12款國際運動游戏，但縱然如此，Vii的12款遊戲除了「[網球](../Page/網球.md "wikilink")」、「[保齡球](../Page/保齡球.md "wikilink")」、「[高爾夫](../Page/高爾夫.md "wikilink")」、「[乒乓球](../Page/乒乓球.md "wikilink")」、「射飛鏢」和「[棒球](../Page/棒球.md "wikilink")」這6項為真正的「國際運動」外，其他的都只是玩樂性質。游戏的画面则是16位色深，声音为单音道。设计者虽然比较明显地优化了画面，但主机的输出画面质量依然十分低劣。许多游戏设计非常简单：如游戏“保龄球”，玩家不能控制發球的角度方向，只需要控制发球的力度，就可以赢得赛局。

Vii
2代内置11款游戏（取消了「骰子」遊戲）。游戏32bit，画面也提升至800\*600分辨率，声音为左右声道。赠送的游戏卡也从7合1提升至25合1。

由於Vii自2007年9月推出以來，一直被批評指為日本任天堂出產之電視遊戲機Wii的仿製品，縱然Vii的製造廠家[樂視購一直不肯承認此事](../Page/樂視購.md "wikilink")，並多次作出有關Vii的版權聲明，但終未能得取電子遊戲玩家的信任，而且Vii現時暫只能透過電視購物廣告才能發售，銷量一直不多。其後，Vii推出第二代電視遊戲機，也被視為又再成一新仿製之作（仿製[PS3](../Page/PS3.md "wikilink")），而遊戲的種類也相當地少，一直未有動漫發行公司願意推出其作品給Vii，同樣也是只能透過電視購物廣告才能購買（在中國大陸、[台灣](../Page/台灣.md "wikilink")、[新加坡和](../Page/新加坡.md "wikilink")[日本的部份電子遊戲機店也有發售](../Page/日本.md "wikilink")，但全部都是[二手貨](../Page/二手貨.md "wikilink")），自然不被重視。

## 评价

玩家普遍对这种抄袭行为表示抗议。[网易在测试Vii后](../Page/网易.md "wikilink")，给出的评价是“拙劣的造工、笨拙的控制、惨不忍睹的画面、弱智的游戏”。\[4\]Vii发售后，。日本民间还将Vii戏称为“[Chintendo](../Page/支天堂.md "wikilink")
Vii”。\[5\]

## 参考文献

<div class="references-small">

<references />

</div>

## 参见

  - [Vii2](../Page/Vii2.md "wikilink")，第二代Vii，外观酷似[PS3](../Page/PS3.md "wikilink")。
  - [Polystation](../Page/Polystation.md "wikilink")，仿冒Playstation遊戲機。
  - [Pop station](../Page/Pop_station.md "wikilink")，仿冒PSP遊戲機。

## 外部链接

  - [京仕敦電子科技有限公司](http://www.kst1388.com/)：Vii遊戲機生產商，內有有關Vii的介紹**(已失效)**

  -
  - [Engadget Vlog大家討厭的 Vii
    又來囉！](http://chinese.engadget.com/2007/11/20/engadget-vlog-vii/)

[Category:Wii](../Category/Wii.md "wikilink")
[Category:中华人民共和国侵犯版权事件](../Category/中华人民共和国侵犯版权事件.md "wikilink")
[Category:第七世代遊戲機](../Category/第七世代遊戲機.md "wikilink")
[Category:2007年面世的產品](../Category/2007年面世的產品.md "wikilink")

1.  [国产威力棒游戏机试用手记](http://digi.163.com/07/1123/14/3U07FPG8001624J7_2.html)
    网易 2007-11-26

2.  [瘾科技Engadget独家报道：Vii火拼Wii](http://cn.engadget.com/2007/10/13/engadget-vii-vs-wii-first-shot/)

3.  [毫无威力——国产游戏机威力棒Vii评测](http://www.yyjoy.com/html/62/n-8462.html)

4.
5.  取自 China(中國) 和 Nintendo(任天堂) 的合成詞彙