**VirtualDub**是一个[开放源代码的视频捕捉及线性处理软件](../Page/开放源代码.md "wikilink")。它由Avery
Lee编写，遵循[GPL协议](../Page/GPL.md "wikilink")。

## 介绍

VirtualDub有很多高级功能，并且可以通过插件来添加不同的视频处理方法。

VirtualDub的早期版本可以打开微软的\*.asf文件，后来由于软件专利问题，在1.3d版中去除了这个功能。

附加项目提供Avery
Lee的版本中不存在的额外功能，如支持[MPEG-2和](../Page/MPEG-2.md "wikilink")[Ogg
Theora](../Page/Ogg_Theora.md "wikilink")。

## 重構檔案功能

[VirtualDub-AVI-Index-Reconstruction.png](https://zh.wikipedia.org/wiki/File:VirtualDub-AVI-Index-Reconstruction.png "fig:VirtualDub-AVI-Index-Reconstruction.png")
VirtualDub的其中一個功能，在於它能夠修復在擷取錄像的過程中因為種種原因而失敗的檔案。它可以重新讀入整個[AVI檔](../Page/AVI.md "wikilink")，並重新構建它的索引模塊。

## 开发

VirtualDub可以通过读取[DirectShow文件](../Page/DirectShow.md "wikilink")，也可以通过插件支持以下输入格式：
**32位**

  - [MPEG2 /
    DVD](https://web.archive.org/web/20090220124714/http://fcchandler.home.comcast.net/~fcchandler/Plugins/MPEG2/)（by
    fcchandler）
  - [WMV /
    ASF](https://web.archive.org/web/20090220004326/http://fcchandler.home.comcast.net/~fcchandler/Plugins/WMV/)（by
    fcchandler）
  - [AC3](https://web.archive.org/web/20090220162112/http://fcchandler.home.comcast.net/~fcchandler/Plugins/AC3/)（by
    fcchandler）
  - [QuickTime](http://www.tateu.net/software/)（by tateu）
  - [Flash Video](http://www.moitah.net/)（by Moitah）
  - [MP4
    / 3GP](https://web.archive.org/web/20131210034009/http://forums.virtualdub.org/index.php?act=ST&f=7&t=15356)（by
    SEt）
  - [FLIC](https://web.archive.org/web/20090220164307/http://fcchandler.home.comcast.net/~fcchandler/Plugins/FLIC/)（by
    fcchandler）
  - [PVN](https://web.archive.org/web/20131210024935/http://forums.virtualdub.org/index.php?act=ST&f=7&t=15580)（by
    DJStealth）
  - [R3D](https://web.archive.org/web/20090301042252/http://arenafilm.hu/alsog/vdr3d/)（[Redcode
    RAW文件](../Page/Red_Digital_Cinema_Camera_Company.md "wikilink")，by
    Gábor Kertai）
  - [DirectShow](https://web.archive.org/web/20131210032658/http://forums.virtualdub.org/index.php?act=ST&f=7&t=15093)（by
    phaeron）

**64位**

  - [MPEG2 / DVD
    x64](https://web.archive.org/web/20131210032932/http://forums.virtualdub.org/index.php?act=ST&f=7&t=16673)（by
    fcchandler）
  - [DirectShow
    x64](https://web.archive.org/web/20131210035830/http://forums.virtualdub.org/index.php?act=ST&f=7&t=13064)（by
    phaeron）

## 参见

  - [Avidemux](../Page/Avidemux.md "wikilink")

  -
## 外部链接

  - [VirtualDub官方网站](http://www.virtualdub.org/)
  - [Avery为什么编写VirtualDub](http://www.virtualdub.org/virtualdub_history)
  - [SourceForge项目页](http://sourceforge.net/projects/virtualdub)
  - [免安裝版主頁](http://portableapps.com/apps/music_video/virtualdub_portable)

[Category:SourceForge專案](../Category/SourceForge專案.md "wikilink")
[Category:視頻編輯軟體](../Category/視頻編輯軟體.md "wikilink")