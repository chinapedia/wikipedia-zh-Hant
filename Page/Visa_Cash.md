**Visa Cash**
是由[VISA公司推出的接觸式](../Page/VISA.md "wikilink")[智能卡](../Page/智能卡.md "wikilink")[電子貨幣](../Page/電子貨幣.md "wikilink")，主要用於小額交易。

Visa Cash
曾在不同地區推出，包括中國大陸、台灣、香港\[1\]\[2\]、[泰國及](../Page/泰國.md "wikilink")[英國](../Page/英國.md "wikilink")[列斯](../Page/列斯.md "wikilink")。

卡的尺寸與普通[銀行卡或](../Page/銀行卡.md "wikilink")[信用卡無異](../Page/信用卡.md "wikilink")，上裝有智能卡晶片。使用前須先在特定的[自動櫃員機充值](../Page/自動櫃員機.md "wikilink")；使用時只須插入零售商的讀卡機及按鍵確定，毋須輸入密碼。

另外 VISA 公司的對手 [MasterCard](../Page/MasterCard.md "wikilink") 亦有類似的產品
[Mondex](../Page/Mondex.md "wikilink")，同樣使用接觸式智能卡技術。

在香港，由於同時期已有交易時間較短，使用亦較方便的[八達通](../Page/八達通.md "wikilink")，Visa Cash
出現不久就消聲匿跡。[香港運輸署亦曾試驗以](../Page/香港運輸署.md "wikilink") Visa Cash
和 Mondex 代替[易泊卡](../Page/易泊卡.md "wikilink")，但現亦已改用八達通。

在泰國，7-eleven便利店發售和接納帶有接觸式+非接觸式智能卡晶片及磁帶的Visa Cash卡，惟磁帶並無法直接在任何ATM或POS使用。

## 另見

  - [Visa payWave](../Page/Visa_payWave.md "wikilink")

## 注釋

<references />

## 外部連結

  - [VisaCash.org](http://www.visacash.org/) 有關 Visa Cash 歷史的網站

[Category:付費系統](../Category/付費系統.md "wikilink")
[Category:支付系統](../Category/支付系統.md "wikilink")

1.  <http://www.feb.gov.tw/ct.asp?xItem=31720&ctNode=1369&mp=3>
2.