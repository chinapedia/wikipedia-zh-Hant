**Visa公司**（英語：**Visa
Inc.**，标识为**VISA**；）是一家总部位于[美国](../Page/美国.md "wikilink")[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[福斯特市的跨国金融服务公司](../Page/福斯特市.md "wikilink")。\[1\]Visa国际组织通过Visa品牌的[信用卡](../Page/信用卡.md "wikilink")（Credit
Card）和[借记卡](../Page/借记卡.md "wikilink")（Debit
Card）促进全球的电子资金转账。\[2\]VISA不会为消费者发行信用卡，延长信贷或设定费率和收费；相反，VISA为全球各地的金融机构提供Visa品牌的支付产品，并让它们向客户提供信用卡，借记卡，预付费和现金服务。2015年尼尔森报告发现，Visa的全球网络（俗称「**VisaNet**」）处理金额突破6.8兆美元或1,000亿的交易数。\[3\]

VISA在2015年後是全世界規模第二大，也是去除中國大陸地區之後世界規模最大的信用卡与借记卡支付机构，而[万事达卡](../Page/万事达卡.md "wikilink")（）则紧追其后。\[4\]

2015年11月3日，VISA斥資約212億歐元（233億美元）收購VISA Europe。

## VISA标志

<File:Old> Visa Logo.svg|第一代VISA标志：1992年7月1日至2000年 <File:Former> Visa
(company) logo.svg|第二代VISA标志：1998年8月至2006年 <File:Visa> Inc.
logo.svg|第三代VISA标志：2005年至2015年5月 <File:Visa> 2014 logo
detail.svg|第四代VISA标志：2014年1月起至今
[File:Visa.svg|現今VISA验证标志：2015年起](File:Visa.svg%7C現今VISA验证标志：2015年起)

## VISA卡

VISA卡是一個[信用卡和](../Page/信用卡.md "wikilink")[借记卡品牌](../Page/借记卡.md "wikilink")。VISA卡於1976年開始發行，它的前身是由[美国銀行所發行的BankAmericard](../Page/美国銀行.md "wikilink")。

在國際上，BankAmericard在稱為Visa之前也有其他一些名稱，BankAmericard的「藍-白-金」圖形也在這些卡上使用。在[英國](../Page/英國.md "wikilink")，它被稱作巴克萊卡（BarclayCard），由巴克萊銀行（Barclays
Bank）發行。在[加拿大](../Page/加拿大.md "wikilink")，一個包括[道明加拿大信托银行](../Page/道明加拿大信托银行.md "wikilink")，[加拿大帝國商業銀行](../Page/加拿大帝國商業銀行.md "wikilink")，[加拿大皇家銀行和](../Page/加拿大皇家銀行.md "wikilink")[加拿大丰业银行在內的銀行聯盟發行名為Chargex的銀行卡](../Page/加拿大丰业银行.md "wikilink")。在[法國](../Page/法國.md "wikilink")，它被稱為藍卡（Carte
Bleue）。

### 卡等

  - VISA普通卡（Visa Classic）
  - VISA金卡（Visa Gold）
  - VISA白金卡（Visa Platinum）
  - VISA御璽卡（Visa Signature）
  - VISA無限卡（Visa Infinite）
  - VISA無限殊榮卡（Visa Infinite Privilege）

## 非接触式付费

[EMVCoContactlessIndicator.svg](https://zh.wikipedia.org/wiki/File:EMVCoContactlessIndicator.svg "fig:EMVCoContactlessIndicator.svg")
 2007年9月，VISA国际组织推介了新的非接觸支付技術——「Visa
payWave」，其允許持持卡人用VISA借记卡或信用卡在非接觸式支付終端前揮動，不需要物理刷卡或將卡插入一個銷售點設備付费。\[5\]

## 发行地区

VISA國際組織本身並不發行VISA卡，而是由VISA國際組織的會員銀行發行。幾乎全球所有的[商業銀行都發行VISA卡](../Page/商業銀行.md "wikilink")。如今[摩根大通銀行是全球最大的VISA卡發行銀行](../Page/摩根大通.md "wikilink")。VISA目前在全球147个国家与地区发行。\[6\]


  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
<!-- end list -->

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
<!-- end list -->

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
<!-- end list -->

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## Visa与银联矛盾

双标卡是是中国大陆的银行为了解决中国境外的卡组织无法进行人民币清算，而银联在中国境外普及率不足而推出的产品。卡面上印有中国境外的卡组织与银联两个发卡机构标志。2010年，Visa要求在中国境外渠道结算卡号为4开头（Visa卡号）的双标卡时，必须使用Visa通道进行结算。\[7\]其后，银联阻止包括Visa在内的境外卡组织结算人民币交易，亦即中国境內发行的Visa卡不得于中国境內进行交易，仅限包含中国境外及港澳地区消费使用。中国境內的Visa刷卡机只可以结算境外发行的Visa卡。

## 奧林匹克贊助商

[Olympic_rings_without_rims.svg](https://zh.wikipedia.org/wiki/File:Olympic_rings_without_rims.svg "fig:Olympic_rings_without_rims.svg")
自從1988年冬運會開始，VISA就一直作為[奧運會各種收入和與奧運會相關的交易的電子支付方式](../Page/奧運會.md "wikilink")。VISA目前與[國際奧委會](../Page/國際奧委會.md "wikilink")（IOC）所簽訂的Visa作為唯一支付卡的合約將持續到2020年。

## [國際足協世界盃](../Page/國際足協世界盃.md "wikilink")

2007年，[國際足協和VISA簽約](../Page/國際足協.md "wikilink")，擔任2010年,
2014年和2018年世界盃贊助。

## 參見

  - [萬事達卡](../Page/萬事達卡.md "wikilink")
  - [中國銀聯](../Page/中國銀聯.md "wikilink")
  - [美國運通](../Page/美國運通.md "wikilink")
  - [JCB](../Page/JCB.md "wikilink")
  - [大來卡](../Page/大來卡.md "wikilink")

## 注释

## 参考文献

## 外部連結

  - [Visa Inc.](https://www.visa.com)

  -
  -
  -
[Category:信用卡发卡组织](../Category/信用卡发卡组织.md "wikilink")
[V](../Category/標準普爾500指數成分股.md "wikilink")
[Category:总部在加利福尼亚州的跨国公司](../Category/总部在加利福尼亚州的跨国公司.md "wikilink")
[Category:1958年成立的公司](../Category/1958年成立的公司.md "wikilink")
[Category:1958年加利福尼亞州建立](../Category/1958年加利福尼亞州建立.md "wikilink")
[Category:2008年IPO](../Category/2008年IPO.md "wikilink")

1.
2.  [Visa](http://www.corporate.visa.com). Retrieved March 26, 2010.
3.
4.
5.
6.  [VISA Inc.](https://www.visa.com/)
7.