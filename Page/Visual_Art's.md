**Visual
Art's**（ビジュアルアーツ）是一家[日本](../Page/日本.md "wikilink")[遊戲公司](../Page/遊戲.md "wikilink")，[總部位於](../Page/總部.md "wikilink")[日本](../Page/日本.md "wikilink")[大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")，在1991年3月26日成立。\[1\]擁有[Key及ZERO等遊戲品牌](../Page/Key_\(公司\).md "wikilink")。旗下各品牌之中，有的專注於發行[男性向](../Page/男性向.md "wikilink")，有的則專注製作[女性向戀愛冒險遊戲](../Page/女性向.md "wikilink")。

目前該公司主要以發行利用自主開發的遊戲引擎（AVG32、RealLive、SiglusEngine）所製作的遊戲為主要業務，除此之外，也販售自遊戲所衍生的周邊商品及音樂CD。

與[I've的合作](../Page/I've.md "wikilink")，更是創下遊戲業界首次登上日本武道館的紀錄。

[2008年10月與](../Page/2008年10月.md "wikilink")[PARADIGM出版社合作](../Page/PARADIGM.md "wikilink")[VA文庫創刊](../Page/VA文庫.md "wikilink")，開始進入出版業。

## 合作品牌一覽

※排名不分先後，★者為未發售作品的品牌

<table>
<tbody>
<tr class="odd">
<td><h3 id="游戏">游戏</h3>
<ul>
<li><a href="../Page/アブノーマル・ソフトウェア・スタジオ.md" title="wikilink">A.S.S.</a></li>
<li><a href="../Page/AMEDEO.md" title="wikilink">AMEDEO</a></li>
<li>b works</li>
<li><a href="../Page/Bonbee!.md" title="wikilink">Bonbee!</a>（ボンびぃボンボン）</li>
<li>catwalk（catwalk NERO）</li>
<li><a href="../Page/CONCEPT.md" title="wikilink">CONCEPT</a></li>
<li>Dress</li>
<li>Frill</li>
<li>Garden ★</li>
<li>g-clef</li>
<li><a href="../Page/IMAGE_CRAFT.md" title="wikilink">IMAGE CRAFT</a>（Oz PROJECT, ぷち蔵）</li>
<li>Iris</li>
<li>issue</li>
<li><a href="../Page/Key_(公司).md" title="wikilink">Key</a></li>
<li>Kur-Mar-Ter</li>
<li><a href="../Page/Lapis_lazuli.md" title="wikilink">Lapis lazuli</a></li>
<li>LimeLight</li>
<li>mana</li>
<li><a href="../Page/ocelot.md" title="wikilink">ocelot</a></li>
<li>OPTiM</li>
<li>pekoe</li>
<li>Radi</li>
<li>riffraff</li>
<li>Rio</li>
<li><a href="../Page/SAGA_PLANETS.md" title="wikilink">SAGA PLANETS</a></li>
<li></li>
<li><a href="../Page/Spray.md" title="wikilink">Spray</a></li>
<li><a href="../Page/Studio_Mebius.md" title="wikilink">Studio Mebius</a>（Studio Ring）</li>
<li>tone work's</li>
<li>ZION</li>
<li><a href="../Page/イージーオー.md" title="wikilink">イージーオー</a>（E.G.O.）</li>
<li>ななせんち</li>
<li>人形遊戯舎 ★</li>
<li>のると</li>
<li><a href="../Page/裸足少女.md" title="wikilink">裸足少女</a></li>
<li><a href="../Page/はむはむソフト.md" title="wikilink">はむはむソフト</a></li>
</ul></td>
<td><h3 id="音乐">音乐</h3>
<ul>
<li><a href="../Page/Blasterhead.md" title="wikilink">Blasterhead</a></li>
<li>CURE RECORDS</li>
<li><a href="../Page/fripSide.md" title="wikilink">fripSide</a>{{#tag:ref|<a href="http://www.product.co.jp/">VA公式サイト</a>ではパートナーブランドと表記 2012-06-23閲覧}}</li>
<li><a href="../Page/I&#39;ve.md" title="wikilink">I've</a></li>
<li><a href="../Page/Key_Sounds_Label.md" title="wikilink">Key Sounds Label</a></li>
<li>KuroCo</li>
<li><a href="../Page/Lia.md" title="wikilink">queens label</a></li>
<li>voix</li>
</ul>
<h3 id="活动终止解散品牌">活动终止・解散品牌</h3>
<ul>
<li><a href="../Page/13cm.md" title="wikilink">13cm</a>（13cc, 130cm, アーヴォリオ）</li>
<li>AKIKO ★</li>
<li><a href="../Page/CRAFTWORK.md" title="wikilink">CRAFTWORK</a></li>
<li>D-XX</li>
<li>FLADY</li>
<li>HERVEST</li>
<li><a href="../Page/otherwise.md" title="wikilink">otherwise</a></li>
<li>Otsu</li>
<li>PASS GUARD</li>
<li><a href="../Page/プレイム.md" title="wikilink">PLAYM</a></li>
<li><a href="../Page/RAM_(遊戲品牌).md" title="wikilink">RAM</a></li>
<li>REALDEAL</li>
<li>REX</li>
<li>Words</li>
<li></li>
<li>悪美</li>
<li>アンフィニ</li>
<li></li>
<li><a href="../Page/ベースユニット.md" title="wikilink">仮面商会</a></li>
<li>きゅろっと</li>
<li>たまちゅ堂</li>
<li>時代屋 ★</li>
<li>ハーベスト</li>
<li><a href="../Page/林組.md" title="wikilink">林組</a></li>
<li>まんぼうソフト</li>
<li>ミスチフ</li>
<li>雅（みやび）</li>
<li>レックス</li>
<li>萌。（萌♂）</li>
</ul></td>
</tr>
</tbody>
</table>

## 参考资料

## 外部連結

  - [Visual Art's](http://visual-arts.jp/index.html)

  - [最速VisualAntena\!](https://web.archive.org/web/20070401045759/http://visualarts.product.co.jp/)

  -
[\*](../Category/Visual_Art's.md "wikilink")
[Category:日本電子遊戲公司](../Category/日本電子遊戲公司.md "wikilink")
[Category:大阪府公司](../Category/大阪府公司.md "wikilink")
[Category:1991年開業電子遊戲公司](../Category/1991年開業電子遊戲公司.md "wikilink")

1.  [会社概要](http://visual-arts.jp/outline.html)Visual Art's