**維沃移動通-{}-信有限公司**（通称：**vivo**）中国科技公司，為一行動裝置開發公司，隸屬於[廣東](../Page/廣東.md "wikilink")[步步高電子集團](../Page/步步高電子.md "wikilink")\[1\]。vivo原是步步高電子的開發行動裝置部門，負責手機業務。

## 事件

  - 2009年，Vivo於全球註冊。
  - 2011年，Vivo進軍智能手機領域。
  - 2012年11月 ，Vivo發布智能手機Vivo X1。
  - 2013年5月，Vivo發布智能手機Vivo Xplay。
  - 2013年4月，Vivo推出基於[Android深度定制的系統](../Page/Android.md "wikilink")[Funtouch
    OS](../Page/Funtouch_OS.md "wikilink")。
  - 2013年12月，Vivo發布智能手機Vivo Xplay3S。
  - 2014年5月，Vivo發布智能手機Vivo Xshot，同時進軍海外市場。
  - 2014年12月，Vivo發布智能手機Vivo X5Max。
  - 2015年11月，Vivo發布智能手機Vivo X6。
  - 2016年7月，Vivo發布智能手機vivo
    X7，同時成為2016-2017賽季[NBA中國官方合作夥伴](../Page/NBA.md "wikilink")。
  - 2016年11月15日，Vivo发布智能手機V5（面向海外市场），翌日發布智能手機X9。
  - 2016年12月，Vivo發布智能手機X9 Plus。
  - 2017年6月，Vivo发布智能手机X9s/X9s Plus。
  - 2017年9月，Vivo在印度发布智能手机V7/V7+，同月21日在中国发布智能手机X20。
  - 2018年1月，Vivo在美国[CES消費電子展上展出了与](../Page/消費電子展.md "wikilink")[Synaptics合作的全球第一款](../Page/Synaptics.md "wikilink")“屏幕下指纹识别”手机Vivo
    X20 Plus\[2\]。
  - 2018年3月19日，Vivo在中国发布智能手机X21。
  - 2018年6月12日，Vivo在中国发布智能手机[vivo NEX](../Page/vivo_NEX.md "wikilink")。
  - 2018年9月6日，vivo在中国发布智能手机X23.
  - 2018年12月11日，vivo在中国发布智能手机NEX 双屏版。
  - 2019年3月1日，Vivo旗下子品牌[iQOO发布智能手机Vivo](../Page/Vivo_iQOO.md "wikilink")
    iQOO。

## 全球市场

2014年，vivo开始全面进军海外市场。

2014年1月17日，vivo国际官方网站正式上线。\[3\]

2018年與[國際足球協會簽約](../Page/國際足球協會.md "wikilink")，成為[2018年FIFA世界盃官方指定智能手機](../Page/2018年FIFA世界盃.md "wikilink")\[4\]。

### 泰国

2014年8月，vivo在泰国曼谷塔拉世贸中心大酒店举行vivo的首场国际发布会，此举意味着vivo正式进军泰国。\[5\]
2015年7月，vivo在泰国举行了第二次新品发布会。截止2015年7月，vivo在泰国已经拥有2000多家合作客户和1000多名销售人员，并在泰国全境建设了20多家专卖店，构成了可以为全泰国范围内的客户提供销售服务的网络，同时还建设了15个售后服务中心。

在与泰国用户的沟通中，vivo在泰国电视台、音乐电台、MajorCineplex院线、大型高档商场、线下音乐会、高校校园等地大规模投放广告，其中，泰国主流社交平台Line的粉丝已超过1000多万。

### 缅甸

2015年3月，vivo在缅甸发布vivo X5Max，正式进入缅甸市场。vivo
X5Max机身厚度为4.75mm，为当时世界上最薄的智能手机。\[6\]

### 印度尼西亚

2015年6月，vivo正式进军印尼市场，在雅加达举办了手机vivo
X5Pro手机发布会。在发布会上，vivo副总裁冯磊表示最早在2017年前，将在印尼投资建设vivo自己的本土化工厂。\[7\]

### 印度

2014年12月，vivo通过X5Max发布正式进入印度市场。

2016年，vivo与印度板球协会BCCI共同宣布，vivo将总冠名印度板球超级联赛（IPL）2016年和2017年两个赛季，加强vivo与印度消费者的沟通。\[8\]

### 马来西亚

2015年6月，vivo携X5Pro召开在马来西亚的首场发布会，正式进入马来西亚市场。

vivo已经在马来西亚建成接近2000家销售点，售后服务中心也达10家，员工超过千人，覆盖马来西亚全境。

### 越南

2015年底，vivo继续拓展东南亚市场，继续进入越南市场。

### 菲律宾

2015年底，vivo继续拓展东南亚市场，陆续进入了菲律宾市场。

### 香港

2018年，vivo进入香港市场，于各大电器店发售。

## 產品列表

  - Y系列
      - [vivo Y51](../Page/vivo_Y51.md "wikilink")
      - [vivo Y53](../Page/vivo_Y53.md "wikilink")
      - [vivo Y55](../Page/vivo_Y55.md "wikilink")
      - [vivo Y67](../Page/vivo_Y67.md "wikilink")
      - [vivo Y66](../Page/vivo_Y66.md "wikilink")
      - [vivo Y71](../Page/vivo_Y71.md "wikilink")
      - [vivo Y79](../Page/vivo_Y79.md "wikilink")
      - [vivo Y75](../Page/vivo_Y75.md "wikilink")
      - [vivo Y81s](../Page/vivo_Y81s.md "wikilink")
      - [vivo Y83](../Page/vivo_Y83.md "wikilink")
      - [vivo Y85](../Page/vivo_Y85.md "wikilink")
      - [vivo Y91](../Page/vivo_Y91.md "wikilink")
      - [vivo Y93](../Page/vivo_Y93.md "wikilink")
      - [vivo Y95](../Page/vivo_Y95.md "wikilink")
  - V系列
      - [vivo V7](../Page/vivo_V7.md "wikilink")
      - [vivo V9](../Page/vivo_V9.md "wikilink")
      - [vivo V11](../Page/vivo_V11.md "wikilink")
      - [vivo V15](../Page/vivo_V15.md "wikilink")
  - X系列
      - [vivo X1](../Page/vivo_X1.md "wikilink")
      - [vivo X3](../Page/vivo_X3.md "wikilink")
      - [vivo X5](../Page/vivo_X5.md "wikilink")
      - [vivo X6](../Page/vivo_X6.md "wikilink")
      - [vivo X7](../Page/vivo_X7.md "wikilink")
      - [vivo X9](../Page/vivo_X9.md "wikilink")
      - [vivo X20](../Page/vivo_X20.md "wikilink")
      - [vivo X21](../Page/vivo_X21.md "wikilink")
      - [vivo X23](../Page/vivo_X23.md "wikilink")
      - [vivo X27](../Page/vivo_X27.md "wikilink")
  - Z系列
      - [vivo Z1](../Page/vivo_Z1.md "wikilink")
      - [vivo Z3](../Page/vivo_Z3.md "wikilink")
  - S系列
      - [vivo S1](../Page/vivo_S1.md "wikilink")
  - NEX系列
      - [vivo NEX](../Page/vivo_NEX.md "wikilink")
      - [vivo NEX 雙屏版](../Page/vivo_NEX_雙屏版.md "wikilink")
  - iQOO
      - [vivo iQOO](../Page/vivo_iQOO.md "wikilink")

## 參考文獻

## 外部連結

  - [Vivo Global](http://www.vivo.com/en/)
  - [Vivo中國](https://www.vivo.com.cn)

[Vivo](../Category/Vivo.md "wikilink")
[Category:東莞公司](../Category/東莞公司.md "wikilink")
[Category:中國電子公司](../Category/中國電子公司.md "wikilink")
[Category:中國民營企業](../Category/中國民營企業.md "wikilink")
[Category:行動電話製造商](../Category/行動電話製造商.md "wikilink")
[Category:2011年成立的公司](../Category/2011年成立的公司.md "wikilink")

1.  [人民日報 -
    步步高是去年國產手機最大贏家](http://finance.people.com.cn/BIG5/n1/2016/0226/c1004-28151555.html)

2.
3.
4.  [Chinese smartphone brand Vivo counts on World Cup to boost its
    slowing
    sales](https://www.scmp.com/tech/china-tech/article/2147387/chinese-smartphone-brand-vivo-counts-world-cup-give-its-slowing)
5.
6.
7.
8.