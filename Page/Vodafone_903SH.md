**Vodafone 903SH**是[日本](../Page/日本.md "wikilink")[Vodefone
JP](../Page/Vodafone.md "wikilink")（現為[軟銀](../Page/Softbank.md "wikilink")）發售的[3G](../Page/3G.md "wikilink")[手提電話](../Page/手提電話.md "wikilink")，支援Vodefone
JP（現為軟銀）[3G網絡服務](../Page/3G.md "wikilink")。Vodafone
903SH於2005年8月12日開始在日本發售，並且擁有當時屬最高階規的320萬像素[CCD鏡頭](../Page/CCD.md "wikilink")，由[SHARP製造](../Page/夏普.md "wikilink")。

[香港版本由](../Page/香港.md "wikilink")[Smartone-Vodafone於](../Page/數碼通.md "wikilink")2005年9月獨家發售，型號為SX833，當時售價為港幣5680\[1\]。[台灣方面則由](../Page/台灣.md "wikilink")[夏普與](../Page/夏普.md "wikilink")[遠傳電信合作推出](../Page/遠傳電信.md "wikilink")，型號為WX-T91，售價為台幣14699\[2\]。

## 手機規格

  - 大小：闊約50×長約109×高約29mm
  - 重量：148克（連電池）
  - 顏色：黑色、紅色、白色
  - 支援網絡：[W-CDMA](../Page/W-CDMA.md "wikilink")、[GSM](../Page/GSM.md "wikilink")（900/1800/1900）
  - 通話時間：[W-CDMA](../Page/W-CDMA.md "wikilink")：150分鐘；[GSM](../Page/GSM.md "wikilink")；240分鐘
  - 待機時間：[W-CDMA](../Page/W-CDMA.md "wikilink")：300小時：[GSM](../Page/GSM.md "wikilink")：290小時
  - 顯示屏幕：2.4吋[QVGA液晶顯示](../Page/QVGA.md "wikilink")、26萬色、160度可視角度
  - 鏡頭：320萬像素[CCD](../Page/CCD.md "wikilink")、2倍光學變焦鏡頭、自動對焦、影片拍攝
  - 副鏡頭：11萬像素[CMOS](../Page/CMOS.md "wikilink")
  - 連接性：[藍芽](../Page/藍芽.md "wikilink")、[紅外線傳輸](../Page/紅外線.md "wikilink")、支援[miniSD](../Page/miniSD.md "wikilink")（最大1GB）
  - 多媒體：[MP3](../Page/MP3.md "wikilink")、[AAC](../Page/AAC.md "wikilink")、[AAC+](../Page/AAC+.md "wikilink")、[MPEG4](../Page/MPEG4.md "wikilink")、[Macromedia
    Flash](../Page/Macromedia_Flash.md "wikilink")、[JAVA](../Page/JAVA.md "wikilink")
  - 內置記憶體：8MB
  - 其他功能：128和弦鈴聲、QR code條碼辨識功能、語音記事、免持擴音功能、匯率換算、電話薄等

## 外部連結

  - [夏普官方網頁介紹903SH（日語）](http://www.sharp.co.jp/products/903sh/)
  - [法林岳之評Vodafone 903SH（日語）](http://ad.impress.co.jp/special/903sh/)
  - [Vodafone 903SH之中文介紹
    (香港)](http://hk.phonedaily.com/mobile/phone/?prod_id=1840)
  - [Vodafone 903SH之中文介紹
    (台灣)](https://web.archive.org/web/20080329232427/http://www.sogi.com.tw/product/productInfo.aspx?pno=1942&page=specification&bname=Sharp)
  - [香港Smatone-Vodafone網頁](http://www.smartone-vodafone.com/jsp/tchinese/index.jsp)
  - [台灣遠傳電訊](http://www.fetnet.net/)

## 參考文獻

[Category:移动电话](../Category/移动电话.md "wikilink")

1.
2.