**Volari V8
Duo**是一種[AGP双核心](../Page/AGP.md "wikilink")[顯示卡](../Page/顯示卡.md "wikilink")，[XGI設計](../Page/XGI.md "wikilink")，於2003年12月發佈，支援[DirectX](../Page/DirectX.md "wikilink")
9.0和[OpenGL](../Page/OpenGL.md "wikilink") 1.4\[1\]。有報導稱Volari V8
Duo曾在[日本](../Page/日本.md "wikilink")[秋叶原电脑市场上市销售](../Page/秋叶原.md "wikilink")\[2\]。效能大約等於[GeForce](../Page/GeForce.md "wikilink")
5700 Ultra 或 [Radeon](../Page/Radeon.md "wikilink") 9600XT
。XGI將兩顆中階核心合併成一張顯示卡，对抗GeForce 5900 或 Radeon
9800。由於驅动問題，效能一般。

*Volari*這個名字，源自於*Velocity*的谐音。後者的意思是速度，或力量。Volari V8
Duo是Volari系列產品線的旗艦級產品。Duo的意思，是將兩顆中等級的顯示核心，集成在同一張顯示卡中，作協同計算。有點像[SLI和](../Page/SLI.md "wikilink")[CrossFire技術](../Page/CrossFire.md "wikilink")。Volari
V8
Duo是繼[3dfx的](../Page/3dfx.md "wikilink")[Voodoo後](../Page/Voodoo.md "wikilink")，再有廠商以單卡雙核心形式推出顯示卡\[3\]。可是，這種產品的驅動程式設計複雜，[各向异性过滤等級和效能比較低](../Page/各向异性过滤.md "wikilink")(只支援4X，當時的[ATI](../Page/ATI.md "wikilink")
Radeon 9800XT 可以支援16X)，而且容易造成不穩定\[4\]。計算方面有兩個模式，分別是 Frame Interleave 和
Line Interleave
。前者是一個顯示核心負責一個页框，後者是將一個页框分割成多個扫描线，一個顯示核心負責單數扫描线，另一個則負責偶數。由於是[AGP的限制](../Page/AGP.md "wikilink")，實際上只有一個顯示核心是連接主機板。兩個顯示核心是主從關係，透過BitFluent技術連接。主顯示核心透過AGP連接主機板，也連接從顯示核心。而從顯示核心只會跟主顯示核心溝通

顯示核心沒有集成TV Encoder 编码单元，需要透過 SiS301
编码芯片作电视输出。影音解碼方面，可以硬體支援[MPEG-2和](../Page/MPEG-2.md "wikilink")[H.264](../Page/H.264.md "wikilink")\[5\]。

## 核心配置

以下列表以單一個顯示核心表示

| 核心時脈   | 制程                                       | 象素流水線 | 顯示記憶體       | 容量    | 記憶體頻寬  | 記憶體時脈  |
| ------ | ---------------------------------------- | ----- | ----------- | ----- | ------ | ------ |
| 350Mhz | [0.13微米](../Page/0.13微米製程.md "wikilink") | 8條    | DDRI或DDR II | 256MB | 128bit | 900Mhz |

由於XGI Volair Duo V8 Ultra是雙核心顯示卡，所以此卡配置是：

| 核心時脈   | 制程     | 象素流水線 | 顯示記憶體       | 容量    | 記憶體頻寬  | 記憶體時脈  |
| ------ | ------ | ----- | ----------- | ----- | ------ | ------ |
| 350Mhz | 0.13微米 | 16條   | DDRI或DDR II | 512MB | 256bit | 900Mhz |

## 優点

  - 設計新穎
  - 性能在GeForce FX 5700 Ultra 到 GeForce FX 5900 XT 之间，或 Radeon 9600 XT 到
    Radeon 9800 Pro 之间，但價格更便宜。

## 缺点

  - 耗電量驚人，需要两个外接电源接口。
  - 散热装置的噪音不小。
  - Reactor驅動程式不成熟。

## 參考資料

<references/>

[Category:顯示卡](../Category/顯示卡.md "wikilink")

1.  [3D效能新势力 撼讯Volari Duo
    V8（图）](http://tech.icxo.com/htmlnews/2003/12/09/42128.htm)
2.  [XGI Volari Duo V8
    Ultra零售版实物抢先看](http://www.pc6.com/infoview/Article_7590.html)
3.  [双GPU－－Volari Duo V8
    Ultra性能大公開](http://bak1.beareyes.com.cn/2/lib/200401/23/20040123003.htm)
4.  [XGI Volari V8 Duo显示卡抢先测试](http://news.mydrivers.com/1/17/17307.htm)
5.  [圖誠科技 Volari G6 H264
    播放能力](http://www.youtube.com/watch?v=rqv5oOPtzgE)