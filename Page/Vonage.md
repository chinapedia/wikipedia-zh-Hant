**Vonage**（，[國際音標](../Page/國際音標.md "wikilink")）是在美国[纽约证券交易所上市的](../Page/纽约证券交易所.md "wikilink")[IP电话和](../Page/IP电话.md "wikilink")[会话发起协议的网络公司](../Page/会话发起协议.md "wikilink")，主要提供基于宽带的电话服务。2003年在美國成立。（公司名称来自于他们的座右铭“Voice-Over-Net-AGE”，意思是网络电话时代）

Vonage在美国将自己宣传为“Vonage，宽带电话公司®”\[1\]和“领导互联网电话变革”。Vonage目前具有最多的用户，当前有220万用户线，\[2\]完成了50亿次呼叫。
Vonage带领着宽带语音（VoBB），或宽带电话产业穿过迅速成长的消费者市场，包括美国，英国，加拿大和其他国家。

要使用Vonage提供的语音服务，用户需要购买一个Vonage品牌的“VOIP[路由器](../Page/路由器.md "wikilink")”或电话适配器，并将其连入主路由器或宽带[调制解调器](../Page/调制解调器.md "wikilink")。除此以外，用户还需要上行速度达到30-90K[比特率](../Page/比特率.md "wikilink")，且按可靠性[Qos优化的连接](../Page/Qos.md "wikilink")，这样在会话时才不会有严重的[时延和](../Page/时延.md "wikilink")[抖动](../Page/抖动.md "wikilink")。

Vonage最初位于[新泽西州](../Page/新泽西州.md "wikilink")[爱迪生](../Page/爱迪生_\(新泽西州\).md "wikilink")，目前位于[新泽西州](../Page/新泽西州.md "wikilink")[含德市一栋保德信金融集团曾经使用的办公楼](../Page/含德市.md "wikilink")。\[3\]
Vonage在整个[美国提供服务](../Page/美国.md "wikilink")，2004年4月业务扩展到[加拿大](../Page/加拿大.md "wikilink")，2005年1月业务扩展到[英国](../Page/英国.md "wikilink")。

2007年4月12日，Vonage的[首席执行官Michael](../Page/首席执行官.md "wikilink")
Snyder同意辞去首席执行官和公司董事会的职务。董事会主席和[首席战略执行官](../Page/首席战略执行官.md "wikilink")[Jeffrey
A.
Citron将取而代之](../Page/Jeffrey_A._Citron.md "wikilink")，出任临时首席执行官\[4\]。公司同时宣布10%（180人）的裁员计划。\[5\].

2018年9月21日Vonage以3.5億美元現金收購雲端呼叫中心NewVoiceMedia

## 参见

  - [电话](../Page/电话.md "wikilink")
  - [IP电话](../Page/IP电话.md "wikilink")
  - [Skype](../Page/Skype.md "wikilink")
  - [Vbuzzer](../Page/Vbuzzer.md "wikilink")

## 参考文献

## 外部链接

  - [官方网站](http://www.vonage.com/)
  - [加拿大官方网站](http://www.vonage.ca/)
  - [英国官方网站](http://www.vonage.co.uk/)
  - [Vonage 9-1-1
    dialing](https://web.archive.org/web/20070616181332/http://www.vonage.com/features.php?feature=911)
  - [Vonage Forum](http://www.vonage-forum.com/)—An Independent Support
    Site
  - [Commentary on the Vonage (VG)
    IPO](http://seekingalpha.com/by/symbol/vg)
  - [Sky News Technofile](http://www.voipfone.co.uk/Sky_News.wmv)
    Interview Kerry Ritz (Vonage) & Colin
    Duffy（[Voipfone](../Page/Voipfone.md "wikilink")）
  - [Skype vs
    Vonage](https://web.archive.org/web/20070604152713/http://www.voiceoverip-guide.com/Skype_vs_Vonage_The_30_second_VoIP_comparison.php)
    Short comparison of Skype and Vonage

[Category:Companies based in New
Jersey](../Category/Companies_based_in_New_Jersey.md "wikilink")
[Category:VoIP companies](../Category/VoIP_companies.md "wikilink")

1.  [USPTO Latest Status
    Info](http://tarr.uspto.gov/servlet/tarr?regser=serial&entry=78213936)
2.  [Vonage FAQs](http://ir.vonage.com/faq.cfm?faqid=2)
3.  [Vonage Announces Plans To Move Headquarters To
    Holmdel](http://www.vonage-forum.com/article1835.html) Vonage Press
    Release publ. Holmdel Journal. Joan Colella, May 12 2005. Also
    [archive.](https://web.archive.org/web/20051127002325/http://www.vonage.com/corporate/press_news.php?PR=2005_05_12_0)
4.  [Vonage CEO resigns, Company Moves to Cut
    Costs](http://www.computerworld.com/action/article.do?command=viewArticleBasic&taxonomyName=networking_and_internet&articleId=9016340&taxonomyId=16)
    . computerworld.com, [April 12](../Page/April_12.md "wikilink"),
    2007.
5.  [Vonage Prepares To Cut
    Workforce 10%](http://www.informationweek.com/hardware/showArticle.jhtml?articleID=199000539&subSection=Telecom)
    Information Week. W. David Gardner. 2007-4-12.