**W**, **w**
是[拉丁字母中的第](../Page/拉丁字母.md "wikilink")23个[字母](../Page/字母.md "wikilink")。

W由[盎格鲁撒克逊人在](../Page/盎格鲁撒克逊人.md "wikilink")7世纪发明，起初用来表示两個
V，但后来[拉丁语中的](../Page/拉丁语.md "wikilink")/w/发展成为[罗曼语中的](../Page/罗曼语.md "wikilink")/v/，因此V不再完全表示[日尔曼语族中的](../Page/日尔曼语族.md "wikilink")/w/
，在法語，w 稱為 ，除外來語外，多數表示/v/音。在[西班牙語](../Page/西班牙語.md "wikilink")，此字母稱為  或
。[德语中的](../Page/德语.md "wikilink") W
也像在罗曼语系中一样丢失了，这就是为什么德语的W表示/v/而不是/w/。在[荷兰语中](../Page/荷兰语.md "wikilink")，W是[唇齿近音/ʋ](../Page/唇齒近音.md "wikilink")/（除了带有eeuw的词语，发作/-e:β/）。

在[瑞典语和](../Page/瑞典语.md "wikilink")[芬兰语字母表中](../Page/芬兰语.md "wikilink")，*W*被看作*V*的变体而不是一个独立的字母。但是仍然保持在名字中并获得承认，例如“William”。

在[北约音标字母中使用](../Page/北约音标字母.md "wikilink")*Whiskey*表示字母W。

## 字母W的含意

## 字符编码

| 字符编码                               | [ASCII](../Page/ASCII.md "wikilink") | [Unicode](../Page/Unicode.md "wikilink") | [EBCDIC](../Page/EBCDIC.md "wikilink") | [摩斯电码](../Page/摩斯电码.md "wikilink") |
| ---------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- | ---------------------------------- |
| [大写](../Page/大写字母.md "wikilink") W | 87                                   | 0057                                     | 230                                    | `·--`                              |
| [小写](../Page/小写字母.md "wikilink") w | 119                                  | 0077                                     | 166                                    |                                    |

## 其他表示方法

## 参看

### W的變體

  - [清唇軟顎擦音](../Page/清唇軟顎擦音.md "wikilink")

### 其他字母中的相近字母

  - （[希腊字母](../Page/希腊字母.md "wikilink") Upsilon）

  - （[西里尔字母](../Page/西里尔字母.md "wikilink")，在[白俄罗斯语使用](../Page/白俄罗斯语.md "wikilink")）

[Category:拉丁字母](../Category/拉丁字母.md "wikilink")