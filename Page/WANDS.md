**WANDS**（1991年－2000年），知名日本搖滾樂團。由**上杉昇**、**大島康祐**、**柴崎浩**所組成。出道曲為「**寂しさは秋の色**」，成員經過三次更替後，在2000年宣告解散。

## 簡介

### 成員

  - 大島康祐（[日本](../Page/日本.md "wikilink")[福岡縣人](../Page/福岡縣.md "wikilink")），WANDS第一期鍵盤。
  - 上杉昇（[日本](../Page/日本.md "wikilink")[神奈川縣人](../Page/神奈川縣.md "wikilink")），WANDS第一、二期主唱。
  - 柴崎浩（[日本](../Page/日本.md "wikilink")[東京都人](../Page/東京都.md "wikilink")），WANDS第一、二期吉他。
  - 木村真也（[日本](../Page/日本.md "wikilink")[青森縣人](../Page/青森縣.md "wikilink")），WANDS第二、三期鍵盤。
  - 和久二郎（[日本](../Page/日本.md "wikilink")[東京都人](../Page/東京都.md "wikilink")），WANDS第三期主唱。
  - 杉元一生（[日本](../Page/日本.md "wikilink")[富山縣人](../Page/富山縣.md "wikilink")），WANDS第三期吉他。

### 取名由來

日本音樂大廠**Being Music
Fantasy**在1991年，由[長戶大幸先生一手策劃](../Page/長戶大幸.md "wikilink")，促生的搖滾樂團。WANDS之名來源是取自[塔羅牌的卡片](../Page/塔羅牌.md "wikilink")[權杖](../Page/權杖.md "wikilink")，取其理想、熱情、追求之意。在鍵盤手大島康祐脫團後，也有了取當時的靈魂人物上杉昇（**W**esugi
Show）和柴崎浩（**S**hibasaki
Hiroshi）的「W」和「S」組成『**W**AND**S**』的說法，更讓這個名字有了更深一層的意涵。

## 歷史

\[第1期\]

  - 1991年12月，以單曲「寂しさは秋の色」出道。

<!-- end list -->

  - 1992年5月發行第二張單曲「ふりむいて抱きしめて」。
  - 同年6月發行首張專輯「WANDS」。
  - 同年7月第三張單曲「もっと強く抱きしめたなら」發行，這是WANDS首張登上日本ORICON公信榜冠軍的單曲，進榜時間更長達44週，也是WANDS銷售量最高之單曲。
  - 同年9月大島康祐退出WANDS，由柴崎浩的好友木村真也遞補，進入WANDS第二期。即使大島康祐退出WANDS，但之後的單曲「時の扉」、「戀せよ乙女」都還有他的影響力存在。

\[第2期\]

  - 1992年10月與**[中山美穂](../Page/中山美穂.md "wikilink")**發行單曲「[世界中の誰よりきっと](../Page/世界中の誰よりきっと.md "wikilink")」，是張大賣近兩百萬的單曲。

<!-- end list -->

  - 1993年2月發行第四張單曲「時の扉」。單曲「時の扉」為WANDS銷售量第二高的單曲。
  - 同年4月發行第五張單曲「愛を語るより口づけをかわそう」及第二張專輯「時の扉」。專輯「時の扉」大賣近兩百萬張，是WANDS銷售量最高之專輯。
  - 同年7月發行第六張單曲「戀せよ乙女」。
  - 同年10月發行第三張專輯「Little Bit…」。
  - 同年11月發行第七張單曲「Jumpin' Jack Boy」。

<!-- end list -->

  - 1994年6月發行第八張單曲「[世界が終るまでは…](../Page/直到世界的盡頭….md "wikilink")」。此單曲為朝日電視台動畫[灌籃高手片尾曲](../Page/灌籃高手.md "wikilink")。

<!-- end list -->

  - 1995年2月發行第九張單曲「Secret Night \~It's My Treat\~」。
  - 同年4月發行第四張專輯「PIECE OF MY SOUL」。
  - 同年12月發行第十張單曲「Same Side」。

<!-- end list -->

  - 1996年2月發行第十一張單曲「WORST CRIME \~About a rock star who was a
    swindler\~」。
  - 同年3月發行首張精選輯「SINGLES COLLECTION+6」。
  - 同年上杉昇、柴崎浩不滿Being公司而退出WANDS並離開此公司，由和久二郎、杉元一生遞補。原始WANDS成員正式全部退出本團。

\[第3期\]

  - 1997年9月發行第十二張單曲「錆びついたマシンガソで今を擊ち拔こう」。此單曲為富士電視台動畫[七龍珠GT片尾曲](../Page/七龍珠.md "wikilink")。
  - 同年11月發行第二張精選輯「WANDS BEST ～HISTORIAL BEST ALBUM～」。

<!-- end list -->

  - 1998年2月發行第十三張單曲「Brand New Love」。
  - 同年6月發行第十四張單曲「明日もし君が壞れても」。此單曲為朝日電視台動畫[遊戲王片尾曲](../Page/遊戲王.md "wikilink")

<!-- end list -->

  - 1999年3月發行第十三張單曲「今日、ナニカノハズミデ生きている」。
  - 同年10月發行第五張專輯「AWAKE」。

<!-- end list -->

  - 2000年WANDS宣佈解散。

\[解散後\]

  - 2000年6月發行第三張精選輯「BEST OF WANDS HISTORY」。

<!-- end list -->

  - 2002年8月發行第四張精選輯「complete of WANDS at the BEING studio」。

## 外部連結

  - [WANDS Official Web Site](http://www.being.co.jp/wands/)官方網站（日文）

[Category:日本摇滚樂團](../Category/日本摇滚樂團.md "wikilink")
[Category:Being旗下艺人](../Category/Being旗下艺人.md "wikilink")
[Category:日本金唱片大獎邦樂部門獲獎者](../Category/日本金唱片大獎邦樂部門獲獎者.md "wikilink")