**WASP-1b**是一刻環繞著恆星[WASP-1的](../Page/WASP-1.md "wikilink")[太陽系外行星](../Page/太陽系外行星.md "wikilink")，距離[地球一千的](../Page/地球.md "wikilink")[光年](../Page/光年.md "wikilink")，大小跟[木星差不多](../Page/木星.md "wikilink")，但是和離母恆星的距離很近，因此被歸類於[熱木星](../Page/熱木星.md "wikilink")。
[木星和WASP](../Page/木星.md "wikilink")-1b的大小比較：
{| class="wikitable" align="right" |- \! WASP-1b \! 木星
|-bgcolor="\#000000" |
[Circle_-_black_simple.svg](https://zh.wikipedia.org/wiki/File:Circle_-_black_simple.svg "fig:Circle_-_black_simple.svg")
|
[Jupiter.jpg](https://zh.wikipedia.org/wiki/File:Jupiter.jpg "fig:Jupiter.jpg")
|}









\== 參見 ==

  - [WASP-1](../Page/WASP-1.md "wikilink")

[Category:太陽系外行星](../Category/太陽系外行星.md "wikilink")
[Category:2006年发现的系外行星](../Category/2006年发现的系外行星.md "wikilink")