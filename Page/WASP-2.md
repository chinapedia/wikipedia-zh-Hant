**WASP-2**是位於[海豚座](../Page/海豚座.md "wikilink")，距離469光年，[視星等](../Page/視星等.md "wikilink")12等的一顆[橘色矮星](../Page/橘色矮星.md "wikilink")
\[1\]
。

## 行星系統

在2006年，[SuperWASP專案計畫發現這顆恆星有一顆](../Page/SuperWASP.md "wikilink")[系外行星](../Page/系外行星.md "wikilink")[WASP-2b](../Page/WASP-2b.md "wikilink")\[2\]。

## 聯星

在2008年，一個研究小組以[凌日法研究](../Page/系外行星偵測法#凌日法.md "wikilink")14顆原本使用相對較小的望遠鏡已經發現有行星的恆星。這些系統以[西班牙](../Page/西班牙.md "wikilink")[卡拉上天文台](../Page/卡拉上天文台.md "wikilink")

[反射望遠鏡重新審視](../Page/反射望遠鏡.md "wikilink")。這個系統，和另外兩個系統，被確認為有過去未知的[聯星系統](../Page/聯星.md "wikilink")。以前不知道的伴星是一顆暗淡的[M-型恆星](../Page/恆星類型.md "wikilink")，[視星等](../Page/視星等.md "wikilink")15等，與主星鄉巨大約111[AU](../Page/天文單位.md "wikilink")，在影像中與主星的角距離為1角秒。此一發現導致重新計算主星和行星的軌道參數\[3\]。

## 註解

  - <cite id="fn_a">[Note b:](../Page/#fn_a_back.md "wikilink")</cite>
    伴星被標示為大寫字母"C" ，以免與小寫的行星標示"b"混淆。\[4\]

  -
## 相關條目

  - [SuperWASP](../Page/SuperWASP.md "wikilink")
  - [WASP-1](../Page/WASP-1.md "wikilink")
  - [WASP-2b](../Page/WASP-2b.md "wikilink")

## 參考資料

## 外部連結

  -
[Category:海豚座](../Category/海豚座.md "wikilink")
[Category:行星凌變星](../Category/行星凌變星.md "wikilink")
[Category:太陽系外行星](../Category/太陽系外行星.md "wikilink")
[Category:聯星](../Category/聯星.md "wikilink")
[Category:橙矮星](../Category/橙矮星.md "wikilink")

1.

2.   ([web Preprint](http://xxx.lanl.gov/abs/astro-ph?papernum=0609688))

3.

4.