[ColdSpot.jpg](https://zh.wikipedia.org/wiki/File:ColdSpot.jpg "fig:ColdSpot.jpg")
**WMAP冷斑點**是2004年由[WMAP在](../Page/WMAP.md "wikilink")[波江座檢測出的一個區域](../Page/波江座.md "wikilink")，該處的[宇宙微波背景輻射](../Page/宇宙微波背景輻射.md "wikilink")（CMB）溫度比周圍要低\[1\]。如此大和冷的區域在原始的[CMB中發生的機率估計只有大約](../Page/宇宙微波背景輻射.md "wikilink")0.2%
\[2\]。

## 位置

它的中心位置在[銀河座標系統](../Page/銀河座標系統.md "wikilink") l<sub>*II*</sub> = 207.8°,
b<sub>*II*</sub> =
−56.3°（[赤道座標系統](../Page/赤道座標系統.md "wikilink")：[*α*](../Page/赤經.md "wikilink")
=
03<sup>h</sup>15<sup>m</sup>05<sup>s</sup>，[*δ*](../Page/赤緯.md "wikilink")
=
−19<sup>d</sup>35<sup>m</sup>02<sup>s</sup>），在[天球的](../Page/天球.md "wikilink")[北半球內](../Page/北半球.md "wikilink")。

## 可能的天體

### 超級空洞

一個居於主導地位的解釋是：冷斑點是在原始的[CMB和我們之間的巨大空洞](../Page/宇宙微波背景輻射.md "wikilink")。空洞可以經由[完全薩克斯-瓦福效應](../Page/薩克斯-瓦福效應.md "wikilink")（Integrated
Sachs-Wolfe
effect，ISW效應）或"[利斯-夏瑪效應](../Page/利斯-夏瑪效應.md "wikilink")（）"造成一個比較低溫的區域\[3\]。如果[暗能量沒有像](../Page/暗能量.md "wikilink")[光子穿過一樣的舒展空洞](../Page/光子.md "wikilink")，這種效應將會非常小
\[4\]。

在2007年8月，有兩篇論文在同一天內先後出現在[*astro-ph*](../Page/arXiv.md "wikilink")，兩者都引用外星系巡天（[NVSS](../Page/NVSS.md "wikilink")）的資料，證明來自靠近冷斑點方向的電波源密度一如預測的是異常的低。首先\[5\]，使用對整個涵蓋的天空區域進行子波測量的分析，之後再\[6\]檢查來源的數量和對冷斑點的亮度分布單獨進行平滑處理。。

雖然大的空洞在宇宙中早就被發現，但要解釋冷斑點的空洞必須是特別的大，或許千倍於典型空洞的大小。他可能相距60至100億光年的距離，並有10億光年的直徑，並且在宇宙中可能比在原始的[CMB中的WMAP冷斑點更為罕見](../Page/宇宙微波背景輻射.md "wikilink")\[7\]
\[8\] \[9\]。

### 宇宙結構

在2007年後期，克魯斯等人\[10\]提出冷斑點可能是早期宇宙發展所殘留下來的[結構](../Page/宇宙結構.md "wikilink")。這雖然是一個外來的解釋，但是值得考濾，因為超級空洞可能就是如此大的冷斑點造成的。

### 平行宇宙

另一種有爭議的論點，由[Laura
Mersini-Houghton提出](../Page/Laura_Mersini.md "wikilink")，可能是在[宇宙暴脹之前](../Page/宇宙暴脹.md "wikilink")，宇宙之間的[量子纏結](../Page/量子纏結.md "wikilink")，造成在我們之外的[另一個宇宙](../Page/多重宇宙.md "wikilink")
\[11\]。Laura
Mersini-Houghton認為："標準的宇宙論不能解釋如此巨大的空洞"，而對WMAP冷斑點可能的假設是"…其他宇宙在我們宇宙的邊緣標示的不可能造成誤解的標記"。果真如此，這將是[平行宇宙的第一個](../Page/平行宇宙.md "wikilink")[經驗證據](../Page/經驗證據.md "wikilink")（在理論的模型之前先證明平行宇宙）。這也支持聲稱這個項目在理論上是可以[測試的](../Page/可試性.md "wikilink")
[弦論如果平行宇宙的理論是真實的](../Page/弦論.md "wikilink")，在[天球的](../Page/天球.md "wikilink")[南半球應該也有相似的空洞](../Page/南半球.md "wikilink")。[1](https://web.archive.org/web/20080126153010/http://www.paternitytestinglabs.com/evidence-for-a-parallel-universe/)

### 搜尋方法的靈敏度

密西根大學的研究人員指出，冷斑點的異常是因為有一圈相對較熱的氣體環繞著的緣故，如果只是考慮它的大小和低溫的呈度並不罕見\[12\]。就技術而言，它的檢測和發現取決於篩選用的過濾器，就好像是找到[墨西哥帽小波](../Page/墨西哥帽小波.md "wikilink")
()一樣。

## 相關條目

  - [史隆長城](../Page/史隆長城.md "wikilink")
  - [CfA2長城](../Page/CfA2長城.md "wikilink")
  - [宇宙暗流](../Page/宇宙暗流.md "wikilink")
  - [巨引源](../Page/巨引源.md "wikilink")
  - [空洞 (天文學)](../Page/空洞_\(天文學\).md "wikilink")
  - [宇宙微波背景輻射](../Page/宇宙微波背景輻射.md "wikilink")
  - [威爾金森微波各向異性探測器](../Page/威爾金森微波各向異性探測器.md "wikilink")

## 參考資料

## 外部連結

  - <https://web.archive.org/web/20070930032951/http://www.dailytech.com/Gaping+Hole+Found+in+Universe/article8598.htm>
  - <http://www.space.com/scienceastronomy/070823_huge_hole.html>
  - <http://news.nationalgeographic.com/news/2007/08/070824-hole-sky.html>
  - [BBC News: Great 'cosmic nothingness'
    found](http://news.bbc.co.uk/1/hi/sci/tech/6962185.stm)

[Category:空洞](../Category/空洞.md "wikilink")
[Category:波江座](../Category/波江座.md "wikilink")

1.  Vielva, Martínez-González, Barreiro, Sanz & Cayon, 2004, ["Detection
    of Non-Gaussianity in the Wilkinson Microwave Anisotropy Probe
    First-Year Data Using Spherical
    Wavelets"](http://arxiv.org/abs/astro-ph/0310273), ApJ 609 22-34
2.  Cruz, Martínez-González, Vielva & Cayón, 2005, ["Detection of a
    non-Gaussian spot in WMAP"](http://arxiv.org/abs/astro-ph/0405341),
    MNRAS 356 29-40
3.  Inoue & Silk, 2006, ["Local Voids as the Origin of Large-Angle
    Cosmic Microwave Background Anomalies
    I"](http://arxiv.org/abs/astro-ph/0602478), ApJ 648 23-30
4.  [Centauri Dreams » Blog Archive » Dark Energy Paints the
    Void](http://www.centauri-dreams.org/?p=1427)
5.  McEwen et al., 2007, ["Probing dark energy with steerable wavelets
    through correlation of WMAP and NVSS local morphological
    measures"](http://arxiv.org/abs/0704.0626), MNRAS, in press
6.  Rudnick, Brown & Williams, 2007, ["Extragalactic Radio Sources and
    the WMAP Cold Spot"](http://arxiv.org/abs/0704.0908), ApJ, 671, pp.
    40-44
7.  [BBC NEWS | Science/Nature | Great 'cosmic nothingness'
    found](http://news.bbc.co.uk/2/hi/science/nature/6962185.stm)
8.  [Astronomers Find Enormous Hole in the Universe
    (August 2007)](http://www.nrao.edu/pr/2007/coldspot/)
9.  [graphics](http://www.nrao.edu/pr/2007/coldspot/graphics.shtml)
10.
11. [The void: Imprint of another
    universe?](http://space.newscientist.com/article/mg19626311.400-the-void-imprint-of-another-universe.html)
    , [New Scientist](../Page/New_Scientist.md "wikilink"), 2007-11-24
12. Zhang & Huterer (2009), ["Disks in the sky: A reassessment of the
    WMAP "cold spot"](http://arxiv.org/abs/0908.3988),
    <http://arxiv.org/abs/0908.3988>