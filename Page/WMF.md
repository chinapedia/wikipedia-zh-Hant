**WMF**（）是一种[Microsoft
Windows的](../Page/Microsoft_Windows.md "wikilink")
[图形文件格式](../Page/图形文件格式.md "wikilink")。它是一个[矢量圖格式](../Page/矢量圖.md "wikilink")，但是也允许包含[位图](../Page/位图.md "wikilink")。本质上，一个WMF文件保存一系列可以用来重建图片的Windows
[GDI命令](../Page/图形设备接口.md "wikilink")。在某种程度上，它类似于印刷业广泛使用的[PostScript格式](../Page/PostScript.md "wikilink")。可以用[Microsoft
Office相关软件编辑](../Page/Microsoft_Office.md "wikilink")，或是用[Adobe开发的Flash和](../Page/Adobe.md "wikilink")[Illustrator等矢量图编辑器](../Page/Illustrator.md "wikilink")。

## 歷史

WMF格式在[Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")
3.0開始引入，是一個16位元的標準。[Windows
NT推出時](../Page/Windows_NT.md "wikilink")，微軟又推出一個32位元的加強版標準，稱之為**Enhanced
Metafile**（簡稱**[EMF](../Page/EMF.md "wikilink")**）。這個加強版除了原有的命令以外，亦新加入一批新的指令。有某幾個牌子的[打印機支援列印EMF格式的檔案](../Page/打印機.md "wikilink")。

## WMF文件格式漏洞

2005年末，一些人发现利用WMF文件格式的漏洞可以在Windows程序查看图片时执行代码。[McAfee在](../Page/McAfee.md "wikilink")2005年12月31日报告其6%的用户受到了攻击\[1\]。2006年1月6日，微软针对此问题发布了安全性更新。

## 參看

  - [SVG](../Page/SVG.md "wikilink")
  - [PostScript](../Page/PostScript.md "wikilink")

## 注釋

<references />

## 外部連結

  - [Windows
    GDI](http://msdn.microsoft.com/library/default.asp?url=/library/en-us/gdi/metafile_0hmb.asp)
  - [File Format Summary at
    fileformat.info](http://www.fileformat.info/format/wmf/)
  - [libWMF](http://wvware.sourceforge.net/libwmf.html)
  - [libEMF](http://libemf.sourceforge.net/)
  - [Transcoder](http://xml.apache.org/batik/javadoc/org/apache/batik/transcoder/wmf/tosvg/package-summary.html)
  - [wmf2svg](http://www.blackdirt.com/graphics/svg/)
  - [FAQ about Windows
    Metafile](http://www.companionsoftware.com/PR/WMRC/WindowsMetafileFaq.html)

[Category:图形文件格式](../Category/图形文件格式.md "wikilink")
[Category:矢量图形](../Category/矢量图形.md "wikilink")

1.  [1](http://isc.sans.org/diary.php?storyid=992)