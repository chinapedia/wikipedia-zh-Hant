**國家女子籃球協會**（[英語](../Page/英語.md "wikilink")：****，簡稱****），是美國[NBA主辦的職業女子籃球聯賽](../Page/NBA.md "wikilink")，成立於1996年。這與[國家籃球協會](../Page/NBA.md "wikilink")（NBA）十分相似。WNBA於1997年舉辦第一屆，每年5月至8月期間進行常規賽，9月進行季後賽。

大部分WNBA的球隊都有一支同屬城市的NBA球隊，並與同市的NBA球隊共用同一場館，只有[康乃狄克太陽並沒有任何一隊NBA球隊處於同一城市](../Page/康乃狄克太陽.md "wikilink")，截止2008賽季，[芝加哥天空及](../Page/芝加哥天空.md "wikilink")[休斯頓彗星是WNBA球隊中少數沒有與任何NBA球隊共用同一場館的隊伍](../Page/休斯頓彗星.md "wikilink")。另外，[亞特蘭大美夢](../Page/亞特蘭大美夢.md "wikilink")、[芝加哥天空](../Page/芝加哥天空.md "wikilink")、[康乃狄克太陽](../Page/康乃狄克太陽.md "wikilink")、[休斯頓彗星](../Page/休斯頓彗星.md "wikilink")、[洛杉磯火花](../Page/洛杉磯火花.md "wikilink")、[鳳凰城水星](../Page/鳳凰城水星.md "wikilink")、[西雅圖風暴及](../Page/西雅圖風暴.md "wikilink")[華盛頓神秘是獨立營運的](../Page/華盛頓神秘.md "wikilink")。

## 賽事

### 常規賽

WNBA一共分開為兩個聯盟（Conference）：[東部聯盟和](../Page/東部聯盟_\(WNBA\).md "wikilink")[西部聯盟](../Page/西部聯盟_\(WNBA\).md "wikilink")。2008賽季，東部聯盟一同有7支球隊，而西部聯盟也有7支球隊。在每年5月至8月其間，每支球隊都要完成34場比賽，當中每隊規定與同屬聯盟的其中4隊對賽3次，餘下的2隊則對賽4次（共20場）。而每隊亦需與另一聯盟的7隊，每隊對賽2次（以主客形式進行，共14場）。各聯盟前4名進入九月舉行的季後賽。

如果該賽季剛巧是舉行夏季奧運會，WNBA將會休戰一個月，以便讓球員能夠為其所屬的國家隊作賽。2008賽季中，八月大部分時間時處於休戰時間，目的是為了遷就[2008北京奧運會的舉行](../Page/2008年夏季奧林匹克運動會.md "wikilink")。而2008常規賽會在2008年5月17日至2008年9月14日舉行（7月28日-8月27日為奧運會舉行時間），而季後賽亦會順延至10月開始。

### 全明星賽

於每年七月中，常規賽將會暫停數天，以便舉行[WNBA全明星賽](../Page/WNBA全明星賽.md "wikilink")。每年舉辦全明星賽的場館，都會經過挑選而得出，而且必定是WNBA中其中一的主場館。與NBA一樣，全明星賽是由東部聯盟面對西部聯盟，球迷是可以經過投票而選出他們心中的出賽球員。在2006季的全明星賽中，球員都穿上了WNBA成立十週年紀念球衣作賽。2008賽季，由於有奧運會舉行的關係，全明星賽將會取消，並會在2009賽季中重新舉行。

### 季後賽

WNBA季後賽（WNBA
Playoffs）在每年大約9月上旬開始，東部聯盟與西部聯盟各前四位的球隊可獲得季後賽資格。兩個聯盟中，第一位會與第四位對賽，第二位的則會與第三位對賽，勝出的球隊可晉身-{A|zh-cn:分岸決赛;
zh-hk:分岸決賽; zh-tw:分區決賽}-。而在分岸決賽勝出的兩隊，將進入最後的總決賽。

WNBA季後賽的第一-{A|zh-cn:圈; zh-hk:圈;
zh-tw:輪}-（準決賽）和第二圈（分岸決賽），是以三場兩勝制進行。第一場比賽是由常規賽排名較低的一隊有主場優勢，而第二場及第三場則輪到排名較高的一隊有主場優勢。而在WNBA總決賽中，是以五場三勝制方式進行，通常是在九月舉行。

## 規則

WNBA規則是根據NBA的標準籃球規則，但也有少數規有差別的，以下是些較主要的規則：

  - WNBA的三分線是20尺6.25寸（6.25公尺）長，這是與[國際籃球總會](../Page/國際籃球總會.md "wikilink")（International
    Basketball Federation）規定的是相同。
  - WNBA所採用的籃球，圓周最少是28.5寸（72.4公分）長，與NBA所用的球少1寸（2.54公分）。在2004年，這個大小的球被廣泛應用在世界各地女子籃球比賽中。
  - WNBA每節的時間是10分鐘，與NBA的12分鐘有所不同。

在2004賽季，WNBA的每場比場將為4節，每節10分鐘進行，取代了以往分為2節，每節20分鐘的形式。

## 球隊

在歷史上，WNBA至今曾出現過18支球隊，當中有4隊已經不存在：包括**克里夫蘭搖滾者**（Cleveland
Rockers）、**邁阿密太陽神**（Miami Sol）、**夏洛特螫針**（Charlotte
Sting）及**波特蘭火燄**（Portland Fire）。

另外**奧蘭多奇蹟**（Orlando
Miracle）於2003年遷移至[康乃狄](../Page/康乃狄克州.md "wikilink")，改名為**康乃狄陽光**（Connecticut
Sun）；**[底特律震動](../Page/底特律震動.md "wikilink")**於2010年遷移至[土爾沙改名為](../Page/土爾沙.md "wikilink")**土爾沙震動**，再於2016年遷移至[達拉斯改名為](../Page/達拉斯.md "wikilink")**達拉斯飛馬**；**猶他巨星**（Utah
Starzz）於2003年遷移至[聖安東尼奧改名為](../Page/聖安東尼奧.md "wikilink")**聖安東尼奧銀星**（San
Antonio Silver
Stars），再於2018年遷移到[拉斯維加斯改名為](../Page/拉斯維加斯.md "wikilink")**拉斯維加斯王牌**（Las
Vegas Aces）。

WNBA球隊大部分與NBA的都十分相似，因為他們都有同一個市場，例如：

  - [華盛頓巫師](../Page/華盛頓巫師.md "wikilink")（Washington
    Wizards）與華盛頓神秘（Washington Mystics）、
  - [-{A](../Page/薩克拉門托帝王.md "wikilink")（Sacramento
    Kings）與沙加緬度國女王（Sacramento Monarchs）、
  - [鳳凰城太陽](../Page/鳳凰城太陽.md "wikilink")（Phoenix Suns）與鳳凰城水星（Phoenix
    Mercury）、
  - [-{A](../Page/明尼蘇達灰狼.md "wikilink")（Minnesota
    Timberwolves）與明尼蘇達山貓(Minnesota Lynx)
  - [休士頓火箭](../Page/休士頓火箭.md "wikilink")（Houston Rockets）與休斯頓彗星（Houston
    Comets）
  - [夏洛特黃蜂](../Page/夏洛特黃蜂.md "wikilink")（Charlotte
    Hornets）與夏洛特螫針（Charlotte Sting）（但前者曾經搬遷，後者更已不存在。）

### 東部聯盟

<table style="width:90%;">
<colgroup>
<col style="width: 25%" />
<col style="width: 20%" />
<col style="width: 15%" />
<col style="width: 20%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>球隊</p></th>
<th><p>城市</p></th>
<th><p>顏色</p></th>
<th><p>主場館</p></th>
<th><p>成立年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/亞特蘭大美夢.md" title="wikilink">亞特蘭大美夢</a></strong><br />
Atlanta Dream</p></td>
<td><p><a href="../Page/喬治亞州.md" title="wikilink">喬治亞州</a><a href="../Page/亞特蘭大.md" title="wikilink">亞特蘭大</a></p></td>
<td><p>天藍、紅、白</p></td>
<td><p><a href="../Page/州立農業球館.md" title="wikilink">州立農業球館</a></p></td>
<td><p>2008</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/芝加哥天空.md" title="wikilink">芝加哥天空</a></strong><br />
Chicago Sky</p></td>
<td><p><a href="../Page/伊利諾伊州.md" title="wikilink">伊利諾伊州</a><a href="../Page/芝加哥.md" title="wikilink">芝加哥</a></p></td>
<td><p>天藍、金</p></td>
<td><p><a href="../Page/Wintrust_Arena.md" title="wikilink">Wintrust Arena</a></p></td>
<td><p>2006</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/康乃狄克太陽.md" title="wikilink">康乃狄克太陽</a></strong><br />
Connecticut Sun</p></td>
<td><p><a href="../Page/康涅狄格州.md" title="wikilink">康涅狄格州</a></p></td>
<td><p>深藍、紅、金</p></td>
<td></td>
<td><p>1999</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/印第安納狂熱.md" title="wikilink">印第安納狂熱</a></strong><br />
Indiana Fever</p></td>
<td><p><a href="../Page/印第安納州.md" title="wikilink">印第安納州</a><a href="../Page/印第安納波利斯.md" title="wikilink">印第安納波利斯</a></p></td>
<td><p>深藍、金、紅</p></td>
<td><p><a href="../Page/班克斯人壽球館.md" title="wikilink">班克斯人壽球館</a></p></td>
<td><p>2000</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/紐約自由人.md" title="wikilink">紐約自由人</a></strong><br />
New York Liberty</p></td>
<td><p><a href="../Page/紐約州.md" title="wikilink">紐約州</a><a href="../Page/白原市_(紐約州).md" title="wikilink">白原市</a></p></td>
<td><p>藍、綠、橙</p></td>
<td></td>
<td><p>1997</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/華盛頓神秘.md" title="wikilink">華盛頓神秘</a></strong><br />
Washington Mystics</p></td>
<td><p><a href="../Page/華盛頓哥倫比亞特區.md" title="wikilink">華盛頓哥倫比亞特區</a></p></td>
<td><p>藍、黑、青銅</p></td>
<td></td>
<td><p>1998</p></td>
</tr>
</tbody>
</table>

### 西部聯盟

<table style="width:90%;">
<colgroup>
<col style="width: 25%" />
<col style="width: 20%" />
<col style="width: 15%" />
<col style="width: 20%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>球隊</p></th>
<th><p>城市</p></th>
<th><p>顏色</p></th>
<th><p>主場館</p></th>
<th><p>成立年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/達拉斯飛馬.md" title="wikilink">達拉斯飛馬</a></strong><br />
Dallas Wings</p></td>
<td><p><a href="../Page/德克薩斯州.md" title="wikilink">德克薩斯州</a><a href="../Page/阿靈頓_(德克薩斯州).md" title="wikilink">阿靈頓</a></p></td>
<td><p>黃綠、藍</p></td>
<td></td>
<td><p>1998</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/拉斯維加斯王牌.md" title="wikilink">拉斯維加斯王牌</a></strong><br />
Las Vegas Aces</p></td>
<td><p><a href="../Page/內華達州.md" title="wikilink">內華達州</a><a href="../Page/天堂市.md" title="wikilink">天堂市</a></p></td>
<td><p>紅、白</p></td>
<td></td>
<td><p>1997</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/洛杉磯火花.md" title="wikilink">洛杉磯火花</a></strong><br />
Los Angeles Sparks</p></td>
<td><p><a href="../Page/加利福尼亞州.md" title="wikilink">加利福尼亞州</a><a href="../Page/洛杉磯.md" title="wikilink">洛杉磯</a></p></td>
<td><p>紅、金</p></td>
<td><p><a href="../Page/史達普斯中心.md" title="wikilink">史達普斯中心</a></p></td>
<td><p>1997</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/明尼蘇達山貓.md" title="wikilink">明尼蘇達山貓</a></strong><br />
Minnesota Lynx</p></td>
<td><p><a href="../Page/明尼蘇達州.md" title="wikilink">明尼蘇達州</a><a href="../Page/明尼阿波利斯.md" title="wikilink">明尼阿波利斯</a></p></td>
<td><p>藍、綠、銀</p></td>
<td><p><a href="../Page/標靶中心.md" title="wikilink">標靶中心</a></p></td>
<td><p>1999</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/鳳凰城水星.md" title="wikilink">鳳凰城水星</a></strong><br />
Phoenix Mercury</p></td>
<td><p><a href="../Page/亞利桑那州.md" title="wikilink">亞利桑那州</a><a href="../Page/鳳凰城.md" title="wikilink">鳳凰城</a></p></td>
<td><p>紫、紅、黃綠色</p></td>
<td><p><a href="../Page/托金斯迪克度假酒店球館.md" title="wikilink">托金斯迪克度假酒店球館</a></p></td>
<td><p>1997</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/西雅圖風暴.md" title="wikilink">西雅圖風暴</a></strong><br />
Seattle Storm</p></td>
<td><p><a href="../Page/華盛頓州.md" title="wikilink">華盛頓州</a><a href="../Page/西雅圖.md" title="wikilink">西雅圖</a></p></td>
<td><p>綠、紅、金</p></td>
<td></td>
<td><p>2000</p></td>
</tr>
</tbody>
</table>

### 已經不存在球隊

<table style="width:70%;">
<colgroup>
<col style="width: 30%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<thead>
<tr class="header">
<th><p>球隊</p></th>
<th><p>存在年份</p></th>
<th><p>原因</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>猶他巨星</strong><br />
Utah Starzz</p></td>
<td><p>1997-2002</p></td>
<td><p>改名為聖安東尼奧銀星</p></td>
</tr>
<tr class="even">
<td><p><strong>奧蘭多奇蹟</strong><br />
Orlando Miracle</p></td>
<td><p>1999-2002</p></td>
<td><p>改名為康乃狄陽光</p></td>
</tr>
<tr class="odd">
<td><p><strong>邁阿密太陽神</strong><br />
Miami Sol</p></td>
<td><p>2000-2002</p></td>
<td><p>解散</p></td>
</tr>
<tr class="even">
<td><p><strong>波特蘭火燄</strong><br />
Portland Fire</p></td>
<td><p>2000-2002</p></td>
<td><p>解散</p></td>
</tr>
<tr class="odd">
<td><p><strong>克里夫蘭搖滾者</strong><br />
Cleveland Rockers</p></td>
<td><p>1997-2003</p></td>
<td><p>解散</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/夏洛特螫針.md" title="wikilink">夏洛特螫針</a></strong><br />
Charlotte Sting</p></td>
<td><p>1997-2006</p></td>
<td><p>解散</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/休斯頓彗星.md" title="wikilink">休斯頓彗星</a></strong><br />
Houston Comets</p></td>
<td><p>1997-2008</p></td>
<td><p>解散</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/沙加緬度女皇.md" title="wikilink">沙加緬度女皇</a></strong><br />
Sacramento Monarchs</p></td>
<td><p>1997-2009</p></td>
<td><p>解散</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/底特律震動.md" title="wikilink">底特律震動</a></strong><br />
Detroit Shock</p></td>
<td><p>1998-2009</p></td>
<td><p>改名為土爾沙震動</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/土爾沙震動.md" title="wikilink">土爾沙震動</a></strong><br />
Tulsa Shock</p></td>
<td><p>2010-2015</p></td>
<td><p>改名為達拉斯飛馬</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/聖安東尼奧銀星.md" title="wikilink">聖安東尼奧銀星</a></strong><br />
San Antonio Stars</p></td>
<td><p>2003-2017</p></td>
<td><p>改名為拉斯維加斯王牌</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 未來球隊

2007年，投資者創建了一隊名為[科羅拉多寒冷](../Page/科羅拉多寒冷.md "wikilink")（Colorado
Chill）的球隊。但同年9月，球隊的贊助者宣佈並未籌到足夠的金錢，因此未能參加2008年的WNBA。\[1\]

## 營業管理

### WNBA主席

  - （Val Ackerman） － 1997-2005

  - （Donna Orender） － 2005-現在

### 財政

到目前為止，WNBA並未能在財政上達到成功地步，儘管在2007賽季當中獲得了收益，但還需NBA一直提供每年大約1200萬美元的補貼。WNBA的平均每場入座率僅有NBA的一半。

在薪金方面，大部分WNBA的新秀每年只有大約35,190美元的收入，而最高薪俸的球員亦僅有101,500美元。所以有不少球員會選擇在休季期間，到[歐洲或](../Page/歐洲.md "wikilink")[澳洲一些女子籃球聯賽比賽](../Page/澳洲.md "wikilink")，以作為她們人工上的補給。

### 大眾傳媒

2007賽季中，WNBA的賽事是由[美國廣播公司](../Page/美國廣播公司.md "wikilink")（ABC）、[ESPN及](../Page/ESPN.md "wikilink")三家傳媒轉播，但收視率一直偏低。在2009賽季中，聯盟將會與美國廣播公司／ESPN引入廣播協定，經濟上的交易期限會直到2016年。

許多球隊其實都有當地的電視轉播，所有賽事會在當地的[收音機播出](../Page/收音機.md "wikilink")。

## 歷屆冠軍

| 賽季        | 冠軍                                         | 比數    | 亞軍                                       |
| --------- | ------------------------------------------ | ----- | ---------------------------------------- |
| **1997年** | **[休斯頓彗星](../Page/休斯頓彗星.md "wikilink")**   | 65-51 | [紐約自由人](../Page/紐約自由人.md "wikilink")     |
| **1998年** | **[休斯頓彗星](../Page/休斯頓彗星.md "wikilink")**   | 2-1   | [鳳凰城水星](../Page/鳳凰城水星.md "wikilink")     |
| **1999年** | **[休斯頓彗星](../Page/休斯頓彗星.md "wikilink")**   | 2-1   | [紐約自由人](../Page/紐約自由人.md "wikilink")     |
| **2000年** | **[休斯頓彗星](../Page/休斯頓彗星.md "wikilink")**   | 2-0   | [紐約自由人](../Page/紐約自由人.md "wikilink")     |
| **2001年** | **[洛杉磯火花](../Page/洛杉磯火花.md "wikilink")**   | 2-0   | [夏洛特螫針](../Page/夏洛特螫針.md "wikilink")     |
| **2002年** | **[洛杉磯火花](../Page/洛杉磯火花.md "wikilink")**   | 2-0   | [紐約自由人](../Page/紐約自由人.md "wikilink")     |
| **2003年** | **[底特律震動](../Page/達拉斯飛馬.md "wikilink")**   | 2-1   | [洛杉磯火花](../Page/洛杉磯火花.md "wikilink")     |
| **2004年** | **[西雅圖風暴](../Page/西雅圖風暴.md "wikilink")**   | 2-1   | [康乃狄克太陽](../Page/康乃狄克太陽.md "wikilink")   |
| **2005年** | **[沙加緬度女皇](../Page/沙加緬度女皇.md "wikilink")** | 3-1   | [康乃狄克太陽](../Page/康乃狄克太陽.md "wikilink")   |
| **2006年** | **[底特律震動](../Page/達拉斯飛馬.md "wikilink")**   | 3-2   | [沙加緬度女皇](../Page/沙加緬度女皇.md "wikilink")   |
| **2007年** | **[鳳凰城水星](../Page/鳳凰城水星.md "wikilink")**   | 3-2   | **[底特律震動](../Page/達拉斯飛馬.md "wikilink")** |
| **2008年** | **[底特律震動](../Page/達拉斯飛馬.md "wikilink")**   | 3-0   | [聖安東尼奧銀星](../Page/聖安東尼奧銀星.md "wikilink") |
| **2009年** | **[鳳凰城水星](../Page/鳳凰城水星.md "wikilink")**   | 3-2   | [印第安納狂熱](../Page/印第安納狂熱.md "wikilink")   |
| **2010年** | **[西雅圖風暴](../Page/西雅圖風暴.md "wikilink")**   | 3-0   | [亞特蘭大美夢](../Page/亞特蘭大美夢.md "wikilink")   |
| **2011年** | **[明尼蘇達山貓](../Page/明尼蘇達山貓.md "wikilink")** | 3-0   | [亞特蘭大美夢](../Page/亞特蘭大美夢.md "wikilink")   |
| **2012年** | **[印地安那狂熱](../Page/印地安那狂熱.md "wikilink")** | 3-1   | [明尼蘇達山貓](../Page/明尼蘇達山貓.md "wikilink")   |
| **2013年** | **[明尼蘇達山貓](../Page/明尼蘇達山貓.md "wikilink")** | 3-0   | [亞特蘭大美夢](../Page/亞特蘭大美夢.md "wikilink")   |
| **2014年** | **[鳳凰城水星](../Page/鳳凰城水星.md "wikilink")**   | 3-0   | [芝加哥天空](../Page/芝加哥天空.md "wikilink")     |
| **2015年** | **[明尼蘇達山貓](../Page/明尼蘇達山貓.md "wikilink")** | 3-2   | [印第安納狂熱](../Page/印第安納狂熱.md "wikilink")   |
| **2016年** | **[洛杉磯火花](../Page/洛杉磯火花.md "wikilink")**   | 3-2   | [明尼蘇達山貓](../Page/明尼蘇達山貓.md "wikilink")   |
| **2017年** | **[明尼蘇達山貓](../Page/明尼蘇達山貓.md "wikilink")** | 3-2   | [洛杉磯火花](../Page/洛杉磯火花.md "wikilink")     |
| **2018年** | **[西雅圖風暴](../Page/西雅圖風暴.md "wikilink")**   | 3-0   | [華盛頓神秘](../Page/華盛頓神秘.md "wikilink")     |

## WNBA獎項

當一個賽季結束後，將會頒發以下獎項給領隊及球員：

  -
  -
  -
  -
  -
  -
  -
  -
  -
## 參看

  - [NBA](../Page/NBA.md "wikilink")
  - [國際籃球聯合會](../Page/國際籃球聯合會.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

  -
  -
  -
  -
  -
  -
  -
## 外部链接

  -
  - [WNBA statistics at
    Basketball-Reference.com](http://www.basketball-reference.com/wnba/)

[WNBA](../Category/WNBA.md "wikilink")
[Category:1996年美國建立](../Category/1996年美國建立.md "wikilink")
[Category:美國職業運動聯盟](../Category/美國職業運動聯盟.md "wikilink")
[Category:1996年建立的體育聯賽](../Category/1996年建立的體育聯賽.md "wikilink")

1.  [TorontoSun.com - Basketball - WNBA coming
    north?](http://www.torontosun.com/Sports/Basketball/2008/03/09/4952141-sun.html)