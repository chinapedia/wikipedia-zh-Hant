**WPS
Office**是由[金山軟件股份有限公司發佈的一款辦公軟體](../Page/金山軟件.md "wikilink")，可以應用於辦公軟體最常用的文字編輯、試算表、簡報等功能。覆蓋Windows、[Linux](../Page/Linux.md "wikilink")、[Android](../Page/Android.md "wikilink")、[iOS等平臺](../Page/iOS.md "wikilink")。

## 版本发展

WPS视窗版1.0为金山公司支援微软视窗操作系统的产品，操作界面效仿香港金山电脑的[Super-WPS的风格](../Page/Super-WPS.md "wikilink")。

1997年9月7日珠海金山公司，Kingsoft发布第二个16位视窗版的WPS
97。采用全新微软视窗风格的设计手法，对DOS时代的WPS文档给予一定级别的支持。但未保留DOS时代WPS的操作风格。图文排版成为了这个版本的亮点，随同文本模式存在于这套系统内。专业版安装介质中包含着众多的金山字库可在安装时有选择性的安装。

1999年，WPS
2000发布，纯32位，开始集成文字办公、表格处理、多媒体演示等多种功能。部分DOS时代WPS的操作方式被重新引回，可用于纯文本模式。兼容DOS时代的文档格式。安装光盘内附带了多种方正中文字库以及86和98版的五笔字型输入法。

从WPS 2000后，WPS正式更名为WPS
Office以效仿微软Office，软件中不仅包括了昔日辉煌的WPS文字处理系统，而且也包含像Excel和PowerPoint这样的流行软件，在兼容微软文档的同时保留着对早期WPS文档的支持。\[1\]

2001年5月，新版本以WPS
Office“金山办公组合”为新名称发布，开始从专职文字处理软件转向桌面办公软件，包含了类似于Excel和PowerPoint的表格和幻灯片软件，并尝试兼容微软Office的文档格式。

2002年6月18日，WPS Office 2002发布，操作方式很大程度上借鉴微软Office。

2003年8月30日，WPS Office 2003发布。它的开发初衷是尽量与微软Office靠齐。然而实际性能却无法与当年的Office
2003相比。如果是在Windows
Vista/7上安装这个软件，它常常会将[TrueType字体的功能关闭](../Page/TrueType.md "wikilink")，导致中文字体显示异常。

2004年，WPS Office
Storm发布，与之前的WPS不同，事实上它是基于与WPS完全无关的[OpenOffice.org](../Page/OpenOffice.org.md "wikilink")，支持于视窗和Linux平台。

WPS Office Storm昙花一现后，WPS
Office的名称被重新使用于新产品之上。然而从这一时代起所有的产品已经不再支持早年DOS时代的文档格式，无论操作方式、界面风格加之文档支持能力都尽可能的与微软Office靠拢，已经全然丧失WPS原本之特色。全新的设计、更小的尺寸成为了它向海外市场运转的动力，海外版起初官名为Kingsoft
Office。

2005年，内部代号为“V6”的WPS Office
2005发布，这是一款全新的中文办公软件，全新的32位内核，轻小的体积。一眼看上去仿佛与微软的Office无异，但功能和操作方式上还是有差异的。

2007年，WPS Office
2007发布，个人版成为免费版本。从这一版本开始，金山开辟了海外市场，先后发布了繁体中文版、日本版以及英文版的Kingsoft
Office 2007，与简体中文版不同，这些版本均只有一个专业版，没有免费使用的个人版。

2008年11月18日，“WPS Office 2009体验版”发布。

2009年5月1日，“WPS Office
2009正式版”发布。这是第六代WPS的第三个产品，在这一版本中，又出现了面向于学生免费使用的学生版，面向于教育人员使用的教育版，以及面向于中小企业可供二次开发的中小企业版。英文版的WPS
Office 2009 Professional也再次受到东南亚英语使用者的青睐。

2010年5月4日，“WPS Office
2010正式版”发布，从这一时代起海外版产品日渐成熟，版本并不只局限于专业版，家庭版和标准版也随之引入。语言版本，不仅渗透于台湾、香港、日本以及英语区，对于德语、法语以及意大利语的尝试也开始了。

2011年9月18日，“WPS Office 2012正式版”发布，与WPS Office
2010不同，内部版本号直接跳到了8，类似于Office
2007中的Ribbon的引入，以及与网云资源的集成，使得这款中文办公软件变得更加小巧，界面更加美观。然而它只能读取[.docx的](../Page/.docx.md "wikilink")[文档](../Page/文档.md "wikilink")，不能将文档保存为这种格式。海外版中正版出现德语、法语以及意大利语版，但繁体中文版却停滞于2010版。

2012年3月28日，“WPS for
Linux”发布了Alpha测试版，基于[Qt](../Page/Qt.md "wikilink")，在小范围中进行内测\[2\]，宣告WPS正式开始向Linux桌面办公领域进军，平台的透明度成为了WPS
Office的设计目标，意味着WPS Office欲摆脱微软视窗束缚的初衷。同年WPS进军移动办公市场，发布了Android平台上的版本。

2013年，WPS Office 2013正式版发布。

2018年，WPS for Mac正式版发布。

## 参考文献

## 外部链接

  - [WPS](http://www.wps.cn/)
  - [WPS海外](http://www.wps.com/)
  - [台灣繁體中文版官方網站（無敵科技）](http://tw.wpsoffice.com/)

## 參見

  - [辦公室軟體](../Page/辦公室軟體.md "wikilink")
  - [OpenOffice](../Page/OpenOffice.md "wikilink")
  - [LibreOffice](../Page/LibreOffice.md "wikilink")

{{-}}

[Category:办公室自动化软件](../Category/办公室自动化软件.md "wikilink")
[Category:金山软件](../Category/金山软件.md "wikilink")
[Category:使用Qt的軟體](../Category/使用Qt的軟體.md "wikilink")
[Category:专有软件](../Category/专有软件.md "wikilink")

1.
2.  [关于最近金山WPS For
    Linux的一些评测和信息](http://www.lupaworld.com/article-216722-1.html)