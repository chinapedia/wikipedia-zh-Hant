**WaT**，[日本男子演唱二人組](../Page/日本.md "wikilink")，**WaT**意為“**W**entz（[Wentz瑛士](../Page/Wentz瑛士.md "wikilink")）**a**nd
**T**eppei（[小池徹平](../Page/小池徹平.md "wikilink")）”，為[環球唱片旗下歌手](../Page/環球唱片.md "wikilink")。兩人的經紀公司同為[Burning
Production](../Page/Burning_Production.md "wikilink")。除了音樂創作及演唱，成員更個別參與電影及電視演出。

出道時，因成員已以個人身分分別進行演藝工作，故已有不少支持者。2005年首次登上第56屆[紅白歌合戰](../Page/紅白歌合戰.md "wikilink")，打破在日本歌手自出道至紅白歌合戰登場的最短時程。2015年12月6日，在東京舉行的演唱會上，宣布2016年2月11日解散\[1\]。

## 經歷

  - 2002年
      - [Wentz瑛士與](../Page/Wentz瑛士.md "wikilink")[小池徹平結成組合](../Page/小池徹平.md "wikilink")（當時並沒有組合名稱），並定期在[代代木公園附近定期舉行路邊演唱](../Page/代代木公園.md "wikilink")。

<!-- end list -->

  - 2003年
      - 12月的路邊演唱吸引了超過一千名觀眾，因造成混亂所以終止了以後的路邊演唱活動。

<!-- end list -->

  - 2004年
      - 2月18日，由獨立唱片公司發行首張單曲「卒業TIME」（畢業時刻）。
      - 2月20日，在[池袋Sunshine
        City噴水廣場舉行的獨立唱片出道紀念活動](../Page/太陽城_\(東京\).md "wikilink")，吸引了超過５千名歌迷到場。
      - 同年開始了官方歌迷會「So WaT?」以及舉行「WaT Entertainment Show
        Vol.1」（之後仍繼續著Vol.2，Vol.3…）。

<!-- end list -->

  - 2005年
      - 有11間唱片公司爭奪其唱片合約，最後決定了於[環球唱片正式出道](../Page/環球唱片.md "wikilink")。
      - 11月2日，發行正式出道單曲『僕のキモチ』（我的心意），首週打進ORICON細碟榜第二位。
      - 首次演出「[第56回NHK紅白歌合戰](../Page/第56回NHK紅白歌合戰.md "wikilink")」，打破了當時在日本『由出道到參加[NHK紅白歌合戰](../Page/NHK紅白歌合戰.md "wikilink")』的最短時距。

<!-- end list -->

  - 2006年
      - 3月1日，發行第一張的專輯『卒業TIME〜僕らのはじまり〜』（畢業時刻～我們的開始～）。
      - 4月26日，初次發行DVD「WaT ENTERTAINMENT SHOW 2006 ACT "do" LIVE Vol.4」。
      - 第2年連續出場「[第57回NHK紅白歌合戰](../Page/第57回NHK紅白歌合戰.md "wikilink")」。
      - 同年成為「2006 世界排球錦標賽」的官方應援者。
      - 同年得到的獎項有
          - 『第43回 2005年度 ゴールデン・アロー賞』 新人賞受賞。
          - 『第20回 2005年度 [日本金唱片大賞](../Page/日本金唱片大賞.md "wikilink")』
            ニュー・アーティスト・オブ・ザ・イヤー受賞。
          - 『第39回 [BEST HIT歌謠祭](../Page/BEST_HIT歌謠祭.md "wikilink")』
            最優秀新人賞受賞。
          - 『第39回 [日本有線大賞](../Page/日本有線大賞.md "wikilink")』 特別賞受賞。
          - 『第48回 [日本唱片大賞](../Page/日本唱片大賞.md "wikilink")』新人賞受賞。

<!-- end list -->

  - 2007年
      - 2月14日，小池徹平以『君に贈る歌』（獻給妳的歌）初次正式單獨表演。
      - 4月25日，瑛士以『Awaking Emotion 8/5』初次正式單獨表演。
      - 10月7日，在開演奏會。
      - 第3年連續出場「[第58回NHK紅白歌合戰](../Page/第58回NHK紅白歌合戰.md "wikilink")」。

<!-- end list -->

  - 2008年
      - 5月，WaT LIVE TOUR2008
        “凶×小吉＝大吉TOUR”在日本全國6個城市有10場公演。最後一天在日比谷野外大音楽堂，一邊下雨一邊進行演出。
      - 第4年連續出場「[第59回NHK紅白歌合戰](../Page/第59回NHK紅白歌合戰.md "wikilink")」。

<!-- end list -->

  - 2009年
      - 由2005年初次到上一年為止，連續出場的「[第60回NHK紅白歌合戰](../Page/第60回NHK紅白歌合戰.md "wikilink")」落選了，連續出場的形式被間斷。
      - [小池徹平亦作個人的音樂發展](../Page/小池徹平.md "wikilink")，發行了個人單曲『キミだけ』（只有你）及專輯『Jack
        In The Box』。

<!-- end list -->

  - 2010年
      - 5月，宣佈隔了2年，終於再以WaT為單位在7月28日發行單曲「君が僕にKissをした」（你的KISS），亦預定於9月8日發行單曲「24/7～もう一度～」。
  - 2015年
      - 2月14日，時隔五年，兩人宣布將參加2月25日的「SKYPerfecTV\!音樂節」以及3月12日的「Folk Days
        第92章」並在夏季參與許多音樂活動，今年11月也將發售久違5年的新曲，12月在都內舉行睽違7年半的演唱會。

## 成員資料

  - [小池徹平](../Page/小池徹平.md "wikilink")（Koike Teppei）
      - 出身地：[大阪府](../Page/大阪府.md "wikilink")[大阪狹山市](../Page/大阪狹山市.md "wikilink")
      - 生日：1986年1月5日出生
      - 血型：B型
      - 身高：167 cm
      - 体重：54 kg
      - 興趣：彈吉他、籃球
      - 兄弟姐妹：一位弟弟
      - 喜歡的食物：[壽司](../Page/壽司.md "wikilink")、[拉麵](../Page/拉麵.md "wikilink")
      - 討厭的食物：[哈密瓜](../Page/哈密瓜.md "wikilink")、[西瓜](../Page/西瓜.md "wikilink")、[鮮奶油](../Page/鮮奶油.md "wikilink")、[冰淇淋](../Page/冰淇淋.md "wikilink")（不喜歡白色的食物）
      - 出道過程：2001年第14屆[JUNON超級男孩金獎得主](../Page/JUNON.md "wikilink")，2002年到東京發展，出道處女作[富士電視臺日劇](../Page/富士電視臺.md "wikilink")「[天體觀測](../Page/天體觀測.md "wikilink")」。

<!-- end list -->

  - [Wentz瑛士](../Page/Wentz瑛士.md "wikilink")（Wentz Eiji）
      - 1985年10月8日生
      - 出身地：[東京都](../Page/東京都.md "wikilink")[三鷹市](../Page/三鷹市.md "wikilink")
      - 血型：O型
      - 身高：170 cm
      - 体重：55 kg
      - 興趣：聽音樂、看電影、打撞球
      - 特殊才藝：籃球、滑雪板
      - 兄弟姐妹：一位哥哥
      - 喜歡的食物：[玄米飯](../Page/玄米.md "wikilink")、[明太子](../Page/明太子.md "wikilink")、[納豆](../Page/納豆.md "wikilink")、[烏龍麵](../Page/烏龍麵.md "wikilink")（烏冬）
      - 討厭的食物：[扇貝](../Page/扇貝.md "wikilink")
      - 出道過程：德裔美国人的父親＋日本人的母親之混血兒，4歲開始擔任模特兒，並在四季劇團演出「美女與野獸」。

## 作品

**粗體**為成員個人名義的音樂作品。 （*※有關成員個人名義之戲劇、綜藝節目等演出，請參見個別項目。*）

### 單曲

  - 2004年2月18日 「卒業TIME」（畢業時刻）發行自獨立唱片／現在是廃盤
  - 2005年11月2日 「僕のキモチ」（我的心意）
  - 2006年1月25日 「5センチ。」（5厘米／5公分）
  - 2006年8月2日 「Hava Rava」
  - 2006年11月1日 「Ready Go！」
  - 2006年12月6日 「ボクラノLove Story」（我們的愛情故事）
  - 2007年2月14日 「君に贈る歌」（獻給妳的歌）**小池徹平**／「ラッキーでハッピー」**Wentz瑛士與Gachapin &
    Mukku**（二人個人solo單曲第一彈：同一張單曲，但封面及曲序兩個版本）
  - 2007年4月25日 「my brand new way」（嶄新的一天）**小池徹平**／「Awaking Emotion
    8/5」**Wentz瑛士**（二人個人solo單曲第二彈；同一張單曲，但封面及曲序兩個版本）
  - 2008年1月16日 「夢の途中 (WaT)／TOKIMEKI☆DooBeeDoo (e2)」（夢想途中）
  - 2008年4月23日 「時を越えて-Fantastic World-」
  - 2008年10月29日 「36℃」
  - 2009年6月24日 「キミだけ」（只有你）**小池徹平**
  - 2010年7月28日 「君が僕にKissをした」（你的KISS）
  - 2010年9月8日 「24/7～もう一度～」

### 專輯

  - 2006年3月1日 「卒業TIME～僕らのはじまり～」（畢業時刻～我們的開始～）
  - 2007年6月27日 「小池徹平 ファースト・ソロアルバム」（pieces）**小池徹平**
  - 2007年11月28日 「WaT Collection」
  - 2009年7月15日 「Jack In The Box」**小池徹平**
  - 2016年2月10日 「卒業BEST」

### DVD

  - 2006年4月26日 「WaT Entertainment Show 2006 ACT “do”LIVE Vol.4」
  - 2006年12月20日 「My Favorite Girl -The Movie-」
  - 「WaT Music Video Collection」
  - 「WaT Entertainment Show 2008」

## 外部連結

  - [WaT官方網站](https://web.archive.org/web/20150909120254/https://wat.fanmo.jp/)（日文）
  - [WaT環球唱片官方網站](http://www.universal-music.co.jp/wat/)（日文）
  - [musicJAPANplus艺人资料库 -
    WaT](http://www.musicjapanplus.jp/artistdb/?artist_id=212)

[Category:2002年成立的音樂團體](../Category/2002年成立的音樂團體.md "wikilink")
[Category:日本樂團](../Category/日本樂團.md "wikilink")
[Category:日本男子演唱團體](../Category/日本男子演唱團體.md "wikilink")
[Category:日本男子偶像團體](../Category/日本男子偶像團體.md "wikilink")
[Category:Burning
Production系列所屬藝人](../Category/Burning_Production系列所屬藝人.md "wikilink")
[Category:BEST
HIT歌謠祭最優秀新人獎獲獎者](../Category/BEST_HIT歌謠祭最優秀新人獎獲獎者.md "wikilink")
[Category:二人组](../Category/二人组.md "wikilink")
[Category:2016年解散的音樂團體](../Category/2016年解散的音樂團體.md "wikilink")

1.