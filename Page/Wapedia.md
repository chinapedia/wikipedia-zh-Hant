[Wapedia_-_Strona_Główna.JPG](https://zh.wikipedia.org/wiki/File:Wapedia_-_Strona_Główna.JPG "fig:Wapedia_-_Strona_Główna.JPG")[手机上的显示的Wapedia首页](../Page/手机.md "wikilink")\]\]
**Wapedia**是[弗洛利安·阿墨罕](../Page/弗洛利安·阿墨罕.md "wikilink")（）利用[Taptu平台建立的非官方的維基百科](../Page/Taptu.md "wikilink")[WAP行動版镜像网站](../Page/WAP.md "wikilink")，旨於把[維基百科內容帶進像](../Page/維基百科.md "wikilink")[行動電話與](../Page/行動電話.md "wikilink")[個人數位助理的行動裝置](../Page/個人數位助理.md "wikilink")。\[1\]\[2\]。

Wapedia於2004年8月建立，在很長時間內扮演了第一移动版维基百科的角色，但隨著2007年6月維基百科官方WAP版的出現，Wapedia的使用有所減少。2013年11月4日Wapedia宣-{}-告关闭。

Wapedia提供了每個文章最新的版本，這是透過類似[網頁](../Page/網頁.md "wikilink")[代理行為與本地端文章](../Page/代理伺服器.md "wikilink")[資料庫組合達成](../Page/資料庫.md "wikilink")。這種組合一方面提供快速的最新文章，另一方面對維基伺服器低有著負載低頻寬要求的特性\[3\]。

Wapedia可透過[行動電話或](../Page/行動電話.md "wikilink")[個人數位助理裡的WAP或者行動網路瀏覽器至網站存取](../Page/個人數位助理.md "wikilink")。

標準的維基百科網頁在這些行動裝置上顯示通常太長，故它會被切成幾個較小的區塊以塞進較小的顯示幕上。為降低頻寬要求，圖像都被縮小至符合行動裝置的解析度。另外，Wapedia亦包括快速搜尋引擎，它是獨立於維基百科的伺服器群之外單一機器。行動版也支援[WML以及現代](../Page/WML.md "wikilink")[XHTML行動格式](../Page/XHTML.md "wikilink")，以及符合每個裝置最佳格式的自動偵測。正文是透過獨立的文章資料庫傳送\[4\]。

<File:Wapedia> en start.jpg <File:Wapedia_en_cologne.jpg>
<File:Wapedia_en_pictures.jpg> <File:Wapedia_en_star.jpg> <File:Wapedia>
screenshot on BlackBerry 9300.jpg|黑莓9300上面的Wapedia首页

## 參見

  - [Wikipedia:WAP存取](../Page/Wikipedia:WAP存取.md "wikilink")（介紹维基百科官方WAP版网站）
  - [维基百科移动版](../Page/维基百科移动版.md "wikilink")

## 外部連結

  - [Wapedia](https://web.archive.org/web/20101006133019/http://wapedia.mobi/)

  - \[//zh.m.wikipedia.org/ 维基百科官方移动版首页\]

## 腳註參考

[Category:維基百科](../Category/維基百科.md "wikilink")

1.

2.

3.
4.