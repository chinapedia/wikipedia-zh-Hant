《**What a beautiful
moment**》是[ZARD出道](../Page/ZARD.md "wikilink")13年來第一次，也是唯一一次巡迴演唱會的DVD作品。發行日期為2005年6月8日。

## 内容

  - 收錄了ZARD首次的實況演唱會影像。主要使用在[東京國際論壇Hall](../Page/東京國際論壇.md "wikilink")
    A的Live映像被構成。
  - [坂井泉水歌唱生涯唯一現場演唱會DVD作品](../Page/坂井泉水.md "wikilink")。

## 場次

|               |                                              |
| ------------- | -------------------------------------------- |
| 公演日           | 会场                                           |
| 2004年3月2日（二）  | 大阪Festival Hall                              |
| 2004年3月8日（一）  | [東京國際論壇Hall](../Page/東京國際論壇.md "wikilink") A |
| 2004年3月9日（二）  | 東京國際論壇Hall A                                 |
| 2004年4月5日（一）  | 神户国际会馆                                       |
| 2004年4月8日（四）  | Pacifco横浜国立大Hall                             |
| 2004年4月30日（五） | 大阪Festival Hall                              |
| 2004年5月8日（六）  | 青森市文化会馆                                      |
| 2004年5月10日（一） | 仙台Sunplaza Hall                              |
| 2004年6月2日（三）  | 名古屋Century Hall                              |
| 2004年6月11日（五） | 福岡Sunpalace                                  |
| 2004年7月23日（五） | [日本武道馆](../Page/日本武道馆.md "wikilink")         |

## 曲目

(Opening：永遠 (What a beautiful moment Tour Opening Ver.)（Instrumental）)

  - 1.搖擺的思念
  - 2.悠游在愛中疲憊不堪
  - 3.再多些 再多一些
  - 4.別忘記那個微笑
  - 5.世界一定在未來之中
  - 6.You and me（and…）
  - 7.想更近看你的側臉
  - 8.夢見明日
  - 9.閉上雙眼
  - 10.敞開心胸
  - 11.好想感受你的存在
  - 12.看不見愛
  - 13.Today is another day
  - 14.明年夏天也
  - 15.My Baby Grand ～期待那溫存～
  - 16.你不在
  - 17.My Friend
  - 18.別認輸
  - 樂手介紹(Instrumental)\~Good-bye My Loneliness\~想見你時\~一定不會忘記\~
  - 19.pray
  - 20.停擺的時鐘此刻開始運轉
  - 21.Don't you see\!

## 樂手

  - vocal：[坂井泉水](../Page/坂井泉水.md "wikilink")
  - Electric
    Guitar：[大賀好修](../Page/大賀好修.md "wikilink")、[綿貫正顕](../Page/綿貫正顕.md "wikilink")、[岡本仁志](../Page/岡本仁志.md "wikilink")
  - Bass：[麻井寛史](../Page/麻井寛史.md "wikilink")
  - Keyboards：[古井弘人](../Page/古井弘人.md "wikilink")、[大楠雄藏](../Page/大楠雄藏.md "wikilink")
  - Acoustic Guitar &
    Chorus：[岩井勇一郎](../Page/岩井勇一郎.md "wikilink")、[大田紳一郎](../Page/大田紳一郎.md "wikilink")
  - Chorus：[岡崎雪](../Page/岡崎雪.md "wikilink")、[SATIN
    DOLL](../Page/SATIN_DOLL.md "wikilink")
  - Violin：[高久知也](../Page/高久知也.md "wikilink")
  - Saxophone：[鈴木央紹](../Page/鈴木央紹.md "wikilink")、[勝田一樹](../Page/勝田一樹.md "wikilink")
  - Percussion：[車谷啓介](../Page/車谷啓介.md "wikilink")
  - Drums：[David C. Brown](../Page/David_C._Brown.md "wikilink")
  - Manipulator：[大藪拓](../Page/大藪拓.md "wikilink")

## 関連項目

  - [ZARD](../Page/ZARD.md "wikilink")
  - [坂井泉水](../Page/坂井泉水.md "wikilink")

[Category:ZARD](../Category/ZARD.md "wikilink")
[Category:巡迴音樂會](../Category/巡迴音樂會.md "wikilink")
[Category:2004年音樂](../Category/2004年音樂.md "wikilink")