**白狼遊戲**（White Wolf,
Inc.）是一家美國遊戲公司，靠出版[角色扮演遊戲規則書起家](../Page/角色扮演遊戲.md "wikilink")。

白狼遊戲的成名作為VTM，之後出了一系列[黑暗世界](../Page/黑暗世界.md "wikilink")（*The World of
Darkness*）的遊戲，早期作品特色為重視社交性和超自然社群，近年來多角化經營，集卡式遊戲、電腦遊戲都有涉獵，並出版黑暗世界的系列小說（VTM、WTA、HTR），除此之外還有出[魔獸爭霸的規則](../Page/魔獸爭霸.md "wikilink")、sword
&
sorcery系列等。另外還有拿到ravenloft和pendragon的規則代理權，還另開[新黑暗世界](../Page/新黑暗世界.md "wikilink")(The
New World of Darkness)的規則。

## 出版品

### RPG

  - [黑暗世界](../Page/黑暗世界.md "wikilink")(The World of Darkness)
  - Exalted
  - [劍與魔法](../Page/劍與魔法.md "wikilink")(Sword & Sorcery)
  - Arthaus Games
  - Orpheus

### [卡片游戏](../Page/卡片游戏.md "wikilink")

  - [Jyhad: The Eternal
    Struggle(V:TES)](../Page/Jyhad:_The_Eternal_Struggle\(V:TES\).md "wikilink")
  - [Pimp: The Backhanding](../Page/Pimp:_The_Backhanding.md "wikilink")

### 电子游戏

  - [吸血鬼之避世血族](../Page/吸血鬼之避世血族.md "wikilink")
  - [吸血鬼之避世救赎](../Page/吸血鬼之避世救赎.md "wikilink")

## 連結

[白狼官網](http://www.white-wolf.com/)

[Category:遊戲製造商](../Category/遊戲製造商.md "wikilink")
[Category:電子遊戲發行商](../Category/電子遊戲發行商.md "wikilink")
[Category:美国公司](../Category/美国公司.md "wikilink")