**Wikia**（中文：**維基亞**），原名（中文：**維基城**），是一個[Wiki農場](../Page/Wiki農場.md "wikilink")，創立於2004年10月18日，創立人為[吉米·威爾士和](../Page/吉米·威爾士.md "wikilink")[安琪拉·貝絲蕾](../Page/安琪拉·貝絲蕾.md "wikilink")，主要提供[wiki服務](../Page/Wiki.md "wikilink")。Wikia股份有限公司集結了許多不同族群的wiki站，免費提供使用者參與閱讀和編輯，以[廣告作為收入來源](../Page/廣告.md "wikilink")。2016年9月所屬的網路頁面以及手機應用程式正名為Fandom
powered by Wikia。

## 网站说明

使用者可向Wikia的站方申請一個專屬的wiki站，早期獲准成立的wiki站可能要提出該站點的具體計畫。[藥品](../Page/藥品.md "wikilink")、[煙](../Page/煙.md "wikilink")、[賭博等主題的宣傳網站是禁止開立的主題](../Page/赌博.md "wikilink")。之後在2009年5月開始，放寬簡化了申請站台的程序，站台迅速增加，由2009年年初1萬多個站台，至2010年1月為止wikia底下已有超過8萬個站台，但無人管理或無內容站台的比例也因此相對增加。許多站台雖延續了[維基百科的模式](../Page/中文维基百科.md "wikilink")，但是在細節上已超越了被公認為合適的普通百科全書內容的範疇。比方說在《[星際大戰](../Page/星際大戰.md "wikilink")》電影中出現的一個次要角色可能在[Wookieepedia上擁有一篇獨立的條目](../Page/Wookieepedia.md "wikilink")\[1\]。Wikia和維基百科的另一個不同之處在於它允許含有非中立性觀點或惡搞的內容存在。

早期多數wikia站台與維基百科一樣以[GFDL為主要授權方式](../Page/GNU自由文档许可证.md "wikilink")，在[維基百科更改相關版權之後](../Page/中文维基百科.md "wikilink")，也於[2009年8月開始改為cc](../Page/2009年8月.md "wikilink")-by-sa
3.0授權。（但有仍部分站台各自採用該社群所選擇的授權方式）

除了在wikia網域之下的站台之外，wikia底下有幾個非屬於該網域，但後來加入wikia的網站，包括[阿爾法記憶](../Page/阿爾法記憶.md "wikilink")、[wowwiki](../Page/WoWWiki.md "wikilink")（魔獸世界百科）等。部分網站如英文[偽基百科在加入之後](../Page/偽基百科.md "wikilink")，曾因被迫改為wikia網域而引發社群的不滿。

该网站曾经被[中华人民共和国](../Page/中华人民共和国.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[沙特阿拉伯](../Page/沙特阿拉伯.md "wikilink")、[伊朗等国政府封锁](../Page/伊朗.md "wikilink")。[2009年7月后](../Page/2009年7月.md "wikilink")[中华人民共和国已将该站的大部分wiki站解禁](../Page/中华人民共和国.md "wikilink")，然而在该站上运行的[简体中文版](../Page/简化字.md "wikilink")[伪基百科目前在中国大陆仍不能正常访问](../Page/伪基百科.md "wikilink")。之后wikia随即开始了大幅的创新与整改。

## 负面评价

Wikia 曾經強逼用戶更改版面，引發社群的強烈反對及不滿。\[2\]

2010年10月，Wikia 默认的 Monaco 模版将被新修改的 Oasis 版面所取代。11月，将只有new wikia
skin和monobook这两种模版可以选择。为此使得不少参与的网民与部分规模较大的站台都紛紛申请撤离
Wikia。除外，網站上含有過多、煩人和不適當的廣告，\[3\]固定的網名（必須包含.wikia.com）\[4\]也是致使用戶撤離
Wikia 的一大因素。

Wikia 亦會對提出撤出 Wikia
的用戶進行報復，通常會以撤銷其管理員權限或是封禁，有時也會刪除相關信息。類似中國大陸的[河蟹行為](../Page/河蟹_\(网络用语\).md "wikilink")。\[5\]部分用戶就算向
Wikia 申請關閉其站台時，一般都會遭到其拒絕。

2014年12月，Wikia 宣佈將會把現有的 Oasis 版面更換成 Venus
版面，引起極大反響。許多用戶要求保留原有的版面。\[6\]後來
Venus 版面被終止開發及移除，Oasis 版面則保留下來。

2018年5月25日， Monobook 版面被完全從 Wikia 中刪除，所有使用 Monobook 版面的 Wiki 和用戶都被迫切換到
Wikia 默認的 Oasis 版面，引起社區中許多用戶的反感。
該公司列舉了保持兩個皮膚符合[歐盟](../Page/歐盟.md "wikilink")[一般資料保護規範的技術問題](../Page/歐盟一般資料保護規範.md "wikilink")。\[7\]

## Wikia知名站台

  - [Wowwiki](http://wowwiki.wikia.com/wiki/)
  - [LyricWiki](http://lyrics.wikia.com/wiki/)
  - [阿爾法記憶](http://memory-alpha.wikia.com/wiki)
  - [偽基百科](../Page/偽基百科.md "wikilink")（約半數版本設在wikia）
  - [Wookieepedia](http://starwars.wikia.com/wiki/)
  - [Lostpedia](http://lostpedia.wikia.com/wiki/)
  - [ArmchairGM](http://armchairgm.wikia.com/wiki/)
  - [香港網絡大典](http://evchk.wikia.com/wiki/)
  - [香港巴士大典](http://hkbus.wikia.com/wiki/)
  - [香港鐵路大典](http://hkrail.wikia.com/wiki/)

## 參見

  - [wiki](../Page/Wiki.md "wikilink")
  - [wiki農場](../Page/wiki農場.md "wikilink")
  - [Referata](../Page/Wiki農場.md "wikilink")
  - [wikkii](../Page/wikkii.md "wikilink")
  - [editthis.info](../Page/editthis.info.md "wikilink")
  - [WikiHoster](../Page/WikiHoster.md "wikilink")

## 参考文献

## 外部連結

  -
  - [Wikia簡體中文首頁](../Page/wikia:zh:Wikia中文.md "wikilink")

  - [Wikia繁體中文首頁](../Page/wikia:zh-tw:Wikia中文.md "wikilink")

{{-}}

[Category:2004年建立的网站](../Category/2004年建立的网站.md "wikilink")
[Category:Wiki](../Category/Wiki.md "wikilink")
[Wikia](../Category/Wikia.md "wikilink")
[Category:MediaWiki網站](../Category/MediaWiki網站.md "wikilink")
[Category:維基農場](../Category/維基農場.md "wikilink")
[Category:2004年成立的公司](../Category/2004年成立的公司.md "wikilink")
[Category:美国互联网公司](../Category/美国互联网公司.md "wikilink")

1.
2.  [香港網絡大典\#香港網絡大典撤離Wikia討論](http://evchk.wikia.com/wiki/%E9%A6%99%E6%B8%AF%E7%B6%B2%E7%B5%A1%E5%A4%A7%E5%85%B8#.E9.A6.99.E6.B8.AF.E7.B6.B2.E7.B5.A1.E5.A4.A7.E5.85.B8.E6.92.A4.E9.9B.A2Wikia.E8.A8.8E.E8.AB.96)
3.
4.
5.
6.  Rupert Giles, [Prototype for a new article
    layout](http://community.wikia.com/wiki/User_blog:Rupert_Giles/Prototype_for_a_new_article_layout)
    Wikia Community Central, December 2014 \[2014-12-11\]
7.