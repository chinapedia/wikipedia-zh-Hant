**WildFly**，原名**JBoss
AS**或者**JBoss**，是一套[应用程序服务器](../Page/应用程序服务器.md "wikilink")，属于[开源的企业级](../Page/开源软件.md "wikilink")[Java](../Page/Java.md "wikilink")[中间件软件](../Page/中间件.md "wikilink")，用于实现基于[SOA架构的web应用和服务](../Page/SOA.md "wikilink")。

WildFly包含一组可独立运行的软件。

2006年4月10日，[Redhat宣布斥资](../Page/Redhat.md "wikilink")3.5亿美元收购JBoss。

2014年11月20日，JBoss更名为WildFly。

## 參見

  - [HornetQ](../Page/HornetQ.md "wikilink") － JBoss
    社區所研發的[消息中間件](../Page/消息中間件.md "wikilink")

## 参考资料

## 外部链接

  - [官方网站](http://www.jboss.com)
  - [官方jboss社区](http://www.jboss.org)
  - [Jboss集群配置](http://jijian91.com/blog20071010/jboss-cluster.html)

[Category:Java](../Category/Java.md "wikilink") [Category:Red
Hat](../Category/Red_Hat.md "wikilink")
[Category:Java企业平台](../Category/Java企业平台.md "wikilink")
[Category:跨平台軟體](../Category/跨平台軟體.md "wikilink")
[Category:用Java編程的自由軟體](../Category/用Java編程的自由軟體.md "wikilink")