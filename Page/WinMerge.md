**WinMerge**是一款运行于Windows系统下的免费的文件比较／合并工具。它非常适用于确定不同版本的文件间的改变以及合并这些改变，尤其是在有多人编辑同一文件的情况下。

## 特性

优点

  - 对文本文档的可视化编辑以及合并
  - 语法高亮功能
  - 可以处理DOS、UNIX和MAC中的[文本文件](../Page/文本文件.md "wikilink")
  - 支持[Unicode](../Page/Unicode.md "wikilink")
  - 在不同的栏中显示当前文件的差异
  - 高亮差异
  - 在目录比较中使用文件过滤器
  - 在文件比较中检测移动过的段落
  - 在外壳扩展中集成
  - 通过[7-Zip支持压缩文件](../Page/7-Zip.md "wikilink")
  - 支持插件功能
  - 通过DLL支持本地化界面

缺点

  - 不支持手动对齐文本

## 外部链接

  - [官方网站](http://winmerge.org/)
  - [SourceForge上的](../Page/SourceForge.md "wikilink")[WinMerge
    项目页面](http://sourceforge.net/projects/winmerge/)

[Category:SourceForge专案](../Category/SourceForge专案.md "wikilink")
[Category:檔案比較工具](../Category/檔案比較工具.md "wikilink")