**WinRAR**是一款用于管理[压缩包文件的](../Page/压缩软件比较.md "wikilink")[共享软件](../Page/共享软件.md "wikilink")。RAR文件格式及其[算法由作者](../Page/算法.md "wikilink")[Eugene
Roshal研发](../Page/Eugene_Roshal.md "wikilink")，享有原创[专利](../Page/专利.md "wikilink")。

## 功能

  - 完全支援[RAR與](../Page/RAR.md "wikilink")[ZIP壓縮檔案](../Page/ZIP_\(檔案格式\).md "wikilink")，並且能解壓縮[CAB](../Page/CAB.md "wikilink")、[ARJ](../Page/ARJ.md "wikilink")、[LZH](../Page/LZH.md "wikilink")、[TAR](../Page/tar_\(檔案格式\).md "wikilink")，[GZ](../Page/Gzip.md "wikilink")、[ACE](../Page/ACE_\(文件格式\).md "wikilink")、[UUE](../Page/uuencode.md "wikilink")、[BZ2](../Page/bzip2.md "wikilink")、[JAR](../Page/Jar_\(檔案格式\).md "wikilink")，[ISO](../Page/ISO_9660.md "wikilink")、[7z](../Page/7z.md "wikilink")
    和 [ZIP壓縮檔案](../Page/ZIP_\(檔案格式\).md "wikilink")。
  - 可使用[AES进行](../Page/高级加密标准.md "wikilink")128位[加密](../Page/加密.md "wikilink")。
  - 文件最高支持8,589,000,000TB的大小。
  - 可以制作[-{zh-hans:自解压;zh-tw:自解壓縮檔;}-文件](../Page/自解压.md "wikilink")。
  - 可进行数据修复。
  - WinRAR支持[NTFS系统和](../Page/NTFS.md "wikilink")[Unicode文件名](../Page/Unicode.md "wikilink")。
  - WinRAR可以试用40天，试用期结束后會跳出提醒視窗，但仍然可以壓縮與解压缩。
  - 試用版無法使用憑證驗證。

## 沿革

  - 从3.50 beta 1开始，WinRAR支持面板（Skin）和主题自选。
  - 从3.50开始，WinRAR支持[Windows](../Page/Windows.md "wikilink") 64位系统。
  - 從3.60開始，WinRAR支援多[线程壓縮](../Page/线程.md "wikilink")／解壓縮
  - 從4.01開始，新增支援[TAR壓縮檔中以二進位型式記錄的檔案大小](../Page/TAR.md "wikilink")。有些[TAR壓縮檔處理大於](../Page/TAR.md "wikilink")
    8 GB 檔案時改用二進位大小格式。
  - 从5.00 beta 1开始，加入了新的RAR 5.0归档格式，旧版本的WinRAR无法解压缩RAR 5.0的文件
  - 從5.10開始，加入[7z分割檔解壓縮支援](../Page/7z.md "wikilink")，如 (.7z.001,
    .7z.002, ...)。
  - 從5.11開始，WinRAR 能解壓縮資料夾含有 pax
    延伸檔頭的[TAR壓縮檔](../Page/TAR.md "wikilink")、對[7-Zip壓縮檔支援](../Page/7-Zip.md "wikilink")
    \[保留毀損的檔案\] 解壓縮選項。
  - 從5.20 beta 1開始，\[保留毀損檔案\] 解壓縮選項支援 bzip2 壓縮檔、可以使用 WinRAR.ini 檔案來儲存
    WinRAR 設定，而不使用機碼，方便安裝 WinRAR 在移動式媒體中。
  - 從5.20 beta 2開始，\[轉換壓縮檔\] 對話方塊中的壓縮設定之前按 \[儲存\] 按鈕也不會儲存。
  - 從5.20開始，採用[xz壓縮演算法支援](../Page/xz.md "wikilink")[ZIP及](../Page/ZIP格式.md "wikilink")[ZIPX壓縮檔](../Page/ZIPX.md "wikilink")。
  - 從5.70 beta
    1開始，不再支援ACE格式。因2019年2月，WinRAR裡面處理ACE格式解壓縮的`UNACEV2.DLL`[函式庫被发现存在](../Page/函式庫.md "wikilink")[漏洞](../Page/漏洞.md "wikilink")。由于ACE格式已在2007年11月后停止更新，成為[废弃软件](../Page/废弃软件.md "wikilink")，在無法修補漏洞的情形下只能删去`UNACEV2.DLL`文件，而不再支持
    ACE 存档格式。\[1\]\[2\]

## 非 Windows 系统

RARlab 也发布其它操作系统下的 RAR
软件，比如[GNU/Linux下的](../Page/GNU/Linux.md "wikilink")
rarlinux、[Mac OS X下的](../Page/Mac_OS_X.md "wikilink")
rarosx，它们都是在命令行下使用，但可以跟其他图形归档软件配合使用。

## WinRAR 受青睐的原因

由于 ZIP 不支持 Unicode
编码、可能出現亂碼现象，同时也不支持分卷壓縮，[壓縮率稍低](../Page/壓縮率.md "wikilink")，因此發展初期
RAR 更受青睞。\[3\]

## WinRAR 在华人区

WinRAR
的“官方简体中文版”有两个版本。其中，官方英文网站和上海代理商网站上所链接的版本为“上海代理商合作定制版”（即所谓的“非商业个人免费版”）。该版本无论用户是否已付费注册，都会强制弹出上海代理商预先指定的广告展示窗口。另一个正常的“干净”（不会强制弹出广告）的版本，则需要已注册用户通过电子邮件的形式，向
[WinRAR 官方总部技术支持团队](http://www.rarlab.com/feedback.htm)发邮件索取才能获取到。

WinRAR 的“官方繁体中文版”则没有以上问题，用户直接从官方英文网站下载安装并注册后即可让弹窗广告消失。

起初，WinRAR
在[大中华区的总代理为台湾的淺藍科技有限公司](../Page/大中华区.md "wikilink")。其间，WinRAR的“官方国际中文版”（繁体中文版）先后由台湾网友“Jazz”和“浅蓝科技”的“张逸昕（即“一抹浅蓝”），英文名“Sam
Chang”）先生负责翻译和校对。不久后，经中國大陆地区的热心网友提议，由张逸昕先生带领JAZZ、苏清朗、醉狐（QFox）、李柏均、周明波等海峡两岸间的热心网友，完成首个“官方简体中文版”的翻译和校对工作。由于该版本的翻译质量嚴謹，一经发布便赢得广大中文用户的好评。

一段时间之后，上海的软众信息科技有限公司（原名“上海比特瑞旺电脑有限公司”）取得了代理权，RARLAB 将简体中文版的翻译工作移交给了这家公司。

现如今，WinRAR
在[上海与](../Page/上海市.md "wikilink")[台湾分别由上海软众信息科技有限公司以及淺藍科技有限公司代理](../Page/台湾.md "wikilink")，而且通常[繁体中文版会先于](../Page/繁体中文.md "wikilink")[简体中文版发布](../Page/简体中文.md "wikilink")。　

5.21版发布时，上海软众信息科技有限公司宣布向所有使用简体中文版的个人用户推出完全免费的“非商业个人版”（即“上海代理商定制版”），该版本无论用户是否已付费注册，都会强制弹出广告展示窗口。

## 参见

  - [压缩软件列表](../Page/压缩软件列表.md "wikilink")
  - [壓縮軟體比較](../Page/壓縮軟體比較.md "wikilink")

## 参考

## 外部链接

  - [WinRAR 官方網站](http://www.rarlab.com/)
  - [WinRAR 中国大陆地区注册代理商网站](http://www.winrar.com.cn/)
  - [WinRAR 台湾地区代理商网站](http://rar.tw/)
  - [WinRAR 香港地区代理商网站](http://www.rarasia.com/ts/index.html)

[Category:数据压缩软件](../Category/数据压缩软件.md "wikilink")
[Category:专有软件](../Category/专有软件.md "wikilink")
[Category:共享软件](../Category/共享软件.md "wikilink")

1.
2.
3.