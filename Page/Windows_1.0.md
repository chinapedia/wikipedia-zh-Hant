**Microsoft Windows
1.0**是[微软第一次对](../Page/微软.md "wikilink")[个人电脑](../Page/个人电脑.md "wikilink")[操作系统进行](../Page/操作系统.md "wikilink")[用户图形界面的尝试](../Page/用户图形界面.md "wikilink")。Windows
1.0基于[MS-DOS](../Page/MS-DOS.md "wikilink")[操作系统](../Page/操作系统.md "wikilink")。

[Microsoft](../Page/Microsoft.md "wikilink") Windows
1.0是[Windows系列的第一个产品](../Page/Windows.md "wikilink")，於1985年11月20日开始发行。

当时很多人认为Microsoft Windows
1.0是一个低劣的产品。当时最好的[GUI电脑平台是G](../Page/GUI.md "wikilink").E.M.，另外一个选择是。Microsoft
Windows 1.0在1987年由新发行的[Windows
2.0取代](../Page/Windows_2.0.md "wikilink")，但此版本的支援週期長達16年，由1985年支援到2001年最後一天為止。

Windows 1.0中鼠标作用得到特别的重视，用户可以通过点击鼠标完成大部分的操作。Windows 1.0
自带一些简单的应用程序，包括日历、记事本、计算器等等。总之，刚诞生的Windows
1.0，总会让人感到它像是一个[PDA](../Page/PDA.md "wikilink")，甚至可能功能还赶不上PDA，不过这在Windows
1.0诞生时已经相当吸引人了。Windows
1.0的另外一个显著特点就是允许用户同时执行多个程序，并在各个程序之间进行切换，这对于DOS来说是不可想象的。

Windows 1.0
可以显示256种颜色，窗口可以任意缩放，当窗口最小化的时候桌面上会有专门的空间放置这些窗口（其实就是现在的任务栏）。在Windows
1.0中已经出现控制面板（Control
Panel），对[驱动程序](../Page/驱动程序.md "wikilink")、[虚拟内存有明确的定义](../Page/虚拟内存.md "wikilink")，不过功能非常有限。

## 参見

  - [Microsoft Windows的历史](../Page/Microsoft_Windows的历史.md "wikilink")
  - [微軟](../Page/微軟.md "wikilink")
  - [操作系統](../Page/操作系統.md "wikilink")
  - [微軟操作系統列表](../Page/微軟操作系統列表.md "wikilink")
  - [操作系統列表](../Page/操作系統列表.md "wikilink")

[Category:磁盘操作系统](../Category/磁盘操作系统.md "wikilink") [Category:Microsoft
Windows](../Category/Microsoft_Windows.md "wikilink")
[Category:已停止開發的作業系統](../Category/已停止開發的作業系統.md "wikilink")