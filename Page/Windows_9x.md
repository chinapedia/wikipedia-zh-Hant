**Windows 9x**是以[Windows
95](../Page/Windows_95.md "wikilink")[内核作为蓝本的](../Page/内核.md "wikilink")[微软](../Page/微软.md "wikilink")[操作系统的通称](../Page/操作系统.md "wikilink")。这包括了[Windows
95](../Page/Windows_95.md "wikilink")、[Windows
98的所有版本](../Page/Windows_98.md "wikilink")，以及2000年发布的[Windows
Me](../Page/Windows_Me.md "wikilink")。\[1\]

Windows
9x所使用的内部发行版本号为“4.x”，而之前基于[MS-DOS的Windows版本则使用等于或小于](../Page/MS-DOS.md "wikilink")3.9的版本号。[Windows
NT使用类似但独立的](../Page/Windows_NT.md "wikilink")、介于3.5与4.0的版本号。Windows
9x的第一继承人[Windows
XP使用的版本号为](../Page/Windows_XP.md "wikilink")5.1。\[2\]

Windows
9x闻名于它在[桌面的使用](../Page/桌面.md "wikilink")；在1998年，Windows占有了操作系统市场份额的82%。\[3\]

基于Windows 9x产品线的操作系统的大多数功能集与兼容性都随着Windows XP的发布，合并到Windows NT中。

为了能与[Windows
3.x](../Page/Windows_3.x.md "wikilink")、[MS-DOS兼容](../Page/MS-DOS.md "wikilink")，Windows
9x的[内核裡混杂着](../Page/内核.md "wikilink")[16位元和](../Page/16位元.md "wikilink")[32位元的](../Page/32位元.md "wikilink")[程序码](../Page/程序码.md "wikilink")。

## 历史

### 95之前的Windows

[微软第一个独立的Windows版本](../Page/微软.md "wikilink")1.0发布于1985年11月20号，颇受欢迎。它最初定名为“Interface
Manager”，但Rowland Hanson，微软的销售经理，说服公司，认为名称“Windows”更为引人。Windows
1.0并非完整的操作系统，而是被扩展的[MS-DOS](../Page/MS-DOS.md "wikilink")“操作环境”，并拥有后者固有的缺陷与问题。

Windows
2.0现身于1987年12月9日。它使用[真實模式的](../Page/真實模式.md "wikilink")[内存模型](../Page/内存.md "wikilink")，受限于为1兆字节的最大[內存](../Page/內存.md "wikilink")。于此模式下，它可运行在使用[286](../Page/286.md "wikilink")[保护模式的另一个多任务管理器下](../Page/保护模式.md "wikilink")，如[DESKQView](../Page/DESKQView.md "wikilink")。

发布于1990年的[Windows
3.0让Windows获得了巨大的商业成功](../Page/Windows_3.0.md "wikilink")。除了给予本机应用程序更好的功能外，由于引用[虚拟内存机制](../Page/虚拟内存.md "wikilink")，Windows也让用户（相对于Windows/386）享有更好的[MS-DOS程序](../Page/MS-DOS.md "wikilink")[多任务处理](../Page/多任务处理.md "wikilink")。

微软开发了[Windows 3.1](../Page/Windows_3.1.md "wikilink")，对Windows
3.0做了些小改进，主要在错误修正及对多媒体的支持上。它也剔除了[实模式支持](../Page/实模式.md "wikilink")，只可在[80286或更高的](../Page/80286.md "wikilink")[处理器上运行](../Page/处理器.md "wikilink")。尔后，微软又发布[Windows
3.11](../Page/Windows_3.11.md "wikilink")，对1992年的Windows
3.1进行了修饰并加入Windows 3.1发布之后的所有补丁与更新。

同时，微软继续研发[Windows NT](../Page/Windows_NT.md "wikilink")。其主要的系统架构师为Dave
Cutler，曾在[Digital Equipment
Corporation](../Page/Digital_Equipment_Corporation.md "wikilink")（后被[康柏收购](../Page/康柏.md "wikilink")，现为[惠普的一部分](../Page/惠普.md "wikilink")）担任VMS的架构师。微软于1988年8月聘请他以开发[OS/2的后继操作系统](../Page/OS/2.md "wikilink")，但他却创建了一个全新的操作系统。

微软在会上宣布了将会为Windows NT及Windows 3.1的替换者（[Windows
95](../Page/Windows_95.md "wikilink")，代号为Chicago）开发一个可统一两者的操作系统。这个计划的代号为Cairo。事后看来，Cairo是一个远比微软预期中更为艰难的项目。因此，NT及Chicago直到[Windows
XP才统一成功](../Page/Windows_XP.md "wikilink")。

### Windows 95

[Windows
3.11之后](../Page/Windows_3.11.md "wikilink")，微软开始研发一款以用户为导向的、代号为Chicago的操作系统。Chicago的主要设计目标为支援像是[OS/2和](../Page/OS/2.md "wikilink")[Windows
NT那样的](../Page/Windows_NT.md "wikilink")32位[抢占式多任务处理](../Page/抢占式多任务处理.md "wikilink")，尽管16位[内核将会因向后兼容而被保留](../Page/内核.md "wikilink")。首次推出于Windows
NT中的[Win32](../Page/Win32.md "wikilink")
[API被选定为标准](../Page/API.md "wikilink")32位的程序接口，并通过一种被称为“thunking”的技术对Win16兼容。设计全新的[GUI虽然没有在计划之中](../Page/GUI.md "wikilink")，但很多Cairo的用户界面元素都被借鉴并用在后来的发布之中（特别是即插即用-Plug
and Play）。

微软并没有把所有的Windows代码更换到32位；部分代码因兼容、性能、开发时间等问题而保留一些16位代码（尽管没直接使用[实模式](../Page/实模式.md "wikilink")）。再说，它必须满足早期开发Windows的设计决策，以便向后兼容，即使这些设计决策不再适用于更现代的计算环境。这些因素最终开始影响操作系统的稳定与效率。

当Chicago发布于1995年8月24日时，微软销售部采用“Windows 95”作为Chicago的产品名字。

微软接着发布五个不同版本的[Windows 95](../Page/Windows_95.md "wikilink")：

  - Windows 95 - 原始版本
  - Windows 95 A - Windows 95 OSR1汇集到安装程序中。
  - Windows 95 B -（OSR2）包含了几个重大的改进、[Internet
    Explorer](../Page/Internet_Explorer.md "wikilink")（IE）3.0和完全的[FAT32文件系统支援](../Page/FAT32.md "wikilink")。
  - Windows 95 B USB -（OSR2.1）包含了基本的USB支持。
  - Windows 95 C -（OSR2.5）包含了上述所有功能，再加上IE 4.0。这是最后一个Windows 95版本。

OSR2，OSR2.1及OSR2.5没有对外发布；相反的，它们仅被售卖给那些会预装系统在电脑中的[OEM厂商](../Page/OEM.md "wikilink")。一些公司销售预装OSR2新的[硬盘驱动器](../Page/硬盘驱动器.md "wikilink")。

第一个[Microsoft
Plus\!附加包是为了Windows](../Page/Microsoft_Plus!.md "wikilink")
95而出售的。

### Windows 98

1998年6月25日，微软发布了[Windows
98](../Page/Windows_98.md "wikilink")。它包含了新的硬件驱动，并提供较完善的[FAT32文件系统支援](../Page/FAT32.md "wikilink")，允许硬盘分区大于在[Windows
95可接受的](../Page/Windows_95.md "wikilink")2GB上限。Windows
98对[USB的支援也远优于仅提供基本USB功能的Windows](../Page/USB.md "wikilink")
95的[OEM版本](../Page/OEM.md "wikilink")。它还受争议地集成了[Internet
Explorer](../Page/Internet_Explorer.md "wikilink")[浏览器到Windows](../Page/浏览器.md "wikilink")
GUI及Windows Explorer（Windows 资源管理器）中。

1999年，微软发布了Windows 98的第二版，一个过渡性的发布。其值得注意的新特性有[Internet Connection
Sharing和较完善的WDM音频及调制解调器支援](../Page/Internet_Connection_Sharing.md "wikilink")。Internet
Connection
Sharing是一种网络地址转换器，允许多个机器在同一个[LAN上共享唯一的互联网连接](../Page/LAN.md "wikilink")。Windows
98的第二版改进了原版的一些问题，支援了更多的硬件驱动。由于很多存于原版中的小问题得到了修复，Windows
98的第二版成为众人眼中最稳定的Windows
9x发布——以致众多评论员常说[Windows
98的测试版本比](../Page/Windows_98.md "wikilink")[Windows
Me的最终版本稳定](../Page/Windows_Me.md "wikilink")。\[4\]

### Windows ME

于2000年9月，微软推出了[Windows
Me](../Page/Windows_Me.md "wikilink")（千禧年版本），一个拥有增强的多媒体及互联网功能的[Windows
98升级版](../Page/Windows_98.md "wikilink")。它也引入了第一版的[系统还原机制](../Page/系统还原.md "wikilink")，使用户能在系统故障的情况下还原系统到之前“已知良好”的状态。[Windows
Movie Maker的第一版也在这时推出了](../Page/Windows_Movie_Maker.md "wikilink")。

Windows Me被认为是一个在Windows 98与[Windows
XP之间临时过渡性的](../Page/Windows_XP.md "wikilink")、为期一年的短期项目。[Windows
Update网站为旧版Windows提供了很多相关新特性的更新下载](../Page/Windows_Update.md "wikilink")。因此，Windows
Me不被认为是一个在[Windows 95或](../Page/Windows_95.md "wikilink")98产品线上的独立操作系统。

因为频繁的死机和崩溃，Windows Me的稳定性与可靠性常被人诟病。一篇《PC World》所载的文章为Windows
Me取名为“Mistake Edition”（错误版本），并把它列为“Worst Tech Products of All
Time”（史上最差的科技产品）的第四名。\[5\]

由于用户无法像之前的Windows
9x版本那样容易启动进入[MS-DOS模式](../Page/MS-DOS.md "wikilink")，造成大量用户快速学会破解Windows
Me安装程序以制定出所需服务。

### 衰落

[Windows 2000的发布标志着介于Windows](../Page/Windows_2000.md "wikilink")
9x系列与[Windows
NT系列的用户体验转换](../Page/Windows_NT.md "wikilink")。NT受困于缺少USB及即插即用的支持，且披着陈旧过时、难以操作的界面。而Windows
2000则有着比Windows 9x更炫酷的界面，和较完善的即插即用及USB支持。

[Windows
XP的发布显示了微软方向的转变](../Page/Windows_XP.md "wikilink")，把个人版及企业版操作系统调和得更为统一。

慢慢地，微软结束了对Windows
9x的支持，并先后对终端用户及[OEM厂商停止售卖相关软件](../Page/OEM.md "wikilink")。到了2004年的3月，任何Windows
9x系列的软件拷贝已无法在市场上购买到。

### 服务终结

微软在2006年7月11日终止了对Windows
9x系列的支持。[DirectX系列在](../Page/DirectX.md "wikilink")8.0a版停止对[Windows
95的更新](../Page/Windows_95.md "wikilink")、9.0c版停止对[Windows
98与Me的更新](../Page/Windows_98.md "wikilink")；[Visual
Studio](../Page/Visual_Studio.md "wikilink") 系列最后可将 Windows 98
作为目标平台的版本是 Visual Studio
2008。如今，即使是[开源项目的](../Page/开源.md "wikilink")[Mozilla浏览器](../Page/Mozilla浏览器.md "wikilink")，在不加工修改的情况下，也无法在Windows
9x上运行。\[6\]

## 设计

### 内核

Windows 9x是一种混合16/32位的操作系统。

像大多数操作系统一样，Windows
9x由[内核及](../Page/内核.md "wikilink")[用户空间组成](../Page/用户空间.md "wikilink")。

尽管Windows
9x拥有[内存保护机制](../Page/内存保护.md "wikilink")，但它并没有保护内存的首个兆字节以防止来自[用户空间应用程序的侵害](../Page/用户空间.md "wikilink")。这个内存区含有让操作系统正常运作的关键码。写入这些内存区将可能造成系统崩溃或死机。错误的应用程序可能意外写入此内存区并使操作系统停止运行。这是系统不稳定的原因之一。\[7\]

#### 用户模式

Windows 9x的用户模式由三个子系统组成：Win16子系统、Win32子系统及MS-DOS。

Windows
9x/Me为[GDI及](../Page/圖形設備接口.md "wikilink")[堆资源保留两块](../Page/堆.md "wikilink")64KB的内存。长时间运行多个应用程序，又或运行多个拥有大量GDI元素的应用程序将可能耗尽这些内存。若空闲资源少于10%，Windows将变得不稳定并可能崩溃。

#### 内核模式

内核模式由虚拟机管理器（VMM）、Installable File
System（[IFSHLP](../Page/IFSHLP.md "wikilink")）及配置管理器所组成。在Windows
Me及之后的版本中，又加入了[WDM驱动管理器](../Page/WDM.md "wikilink")（NTKERN）。身为32位操作系统，Windows
9x中的每一个进程都有4GB的虚拟内存空间：前2GB给内核而后2GB给应用程序。

### 注册表

像Windows NT一样，Windows
9x把用户和配置特定的设定储存在一个大型的、被称为Windows[注册表的信息数据库](../Page/注册表.md "wikilink")。硬件特定的设定也存于注册表中，且很多设备驱动器都使用注册表来加载配置数据。此前的Windows版本使用如[AUTOEXEC.BAT](../Page/AUTOEXEC.BAT.md "wikilink")、[CONFIG.SYS](../Page/CONFIG.SYS.md "wikilink")、[WIN.INI](../Page/WIN.INI.md "wikilink")、[SYSTEM.INI的文件及其它带有](../Page/SYSTEM.INI.md "wikilink").INI后缀的文件来维持配置设定。当Windows日趋复杂并纳入更多的特性时，.INI文件们对当时的FAT文件系统无疑显得笨重至极。对.INI文件的兼容维持直到Windows
XP的出现。

尽管微软不鼓励使用.INI文件以代替注册表的使用，但仍有大量的应用程序（尤其是基于16位Windows的程序）使用.INI文件。Windows
9x支援.INI文件仅仅是为了兼容这些程序及相关工具（譬如安装程序）。AUTOEXEC.BAT及CONFIG.SYS文件也继续存在以兼容实模式系统组件并允许用户更换默认系统设定，譬如PATH环境变量。

注册表由User.dat及System.dat文件组成。在Windows Me中，又加入了Classes.dat。

### 虚拟机管理器

虚拟机管理器（VMM）是个在Windows
9x核心中的32位[保护模式内核](../Page/保护模式.md "wikilink")。它的主要任务是创建、执行、监管及终止[虚拟机](../Page/虚拟机.md "wikilink")。VMM提供可管理内存、进程、中断及错误保护机制的服务。VMM作用于虚拟设备（可加载的内核模块，主要由32位环-0或内核模式码组成，但也包了括其它类型的代码，譬如16位实模式初始化段）以允许这些虚拟设备拦截中断或故障来控制一个应用程序对硬件及软件的访问。VMM及虚拟设备都运行在一个单一的、0权限级（也称做环-0）的32位平面内存模型地址空间。VMM提供多线程[抢占式多任务处理机制](../Page/抢占式多任务处理.md "wikilink")。为了同时执行多个应用程序，它把[CPU时间分配给在这些应用程序及虚拟机中的线程们](../Page/CPU.md "wikilink")。

VMM也负责为系统进程及还需运行在MS-DOS模式中的Windows程序创建MS-DOS环境。它是Win386（在Windows
3.x中）的替代品。文件vmm32.vxd是一个压缩档案，含有大部分VxD的核心程序码，包括了VMM.vxd及ifsmgr.vxd（利于文件系统的访问，无需调用DOS内核实模式的系统码）。

### 软件支持

#### 文件系统

Windows
9x本身不支持[NTFS或](../Page/NTFS.md "wikilink")[HPFS](../Page/HPFS.md "wikilink")，但第三方解决了问题，允许Windows
9x对NTFS卷进行只读访问。

Windows 95的早期版本不支持[FAT32](../Page/FAT32.md "wikilink")。

同[Windows for Workgroups
3.11一样](../Page/Windows_for_Workgroups_3.11.md "wikilink")，Windows
9x支持[32位文件访问](../Page/32位文件访问.md "wikilink")。不同于Windows 3.x，Windows
9x支持VFAT文件系统，允许文件名字可达最多255个字母以取代[8.3文件命名法](../Page/8.3文件命名法.md "wikilink")。

#### 事件日志及追踪

Windows 9x不支持Windows
NT家族所拥有的[事件日志及追踪或](../Page/事件日志及追踪.md "wikilink")[出错报表机制](../Page/出错报表.md "wikilink")，尽管使用像[Norton
CrashGuard的软件可在Windows](../Page/Norton_CrashGuard.md "wikilink")
9x上实现相似的功能。

#### 安全机制

Windows 9x乃单用户系统。所以，它的安全机制不如Windows NT有效。其原因之一是Windows
9x只正式支持[FAT](../Page/FAT.md "wikilink")[文件系统](../Page/文件系统.md "wikilink")（包括FAT12/16/32），尽管Windows
NT也支持FAT 12及16，且Windows 9x也可通过第三方的[Installable File
System驱动器来实现对](../Page/Installable_File_System.md "wikilink")[NTFS卷的读写](../Page/NTFS.md "wikilink")。FAT系统的安全机制不彰，每个拥有FAT驱动器访问权限的用户都可访问其内的所有文件。不像NTFS，FAT文件系统没有[访问控制列表和文件系统加密机制](../Page/访问控制列表.md "wikilink")。\[8\]

一些与Windows
9x同时期的操作系统（不管是[多用户的或是拥有多个不同访问权限的用户户口](../Page/多用户.md "wikilink")）的重要系统文件（譬如内核映像）在大多数用户户口底下都是不可更动的。相反的，Windows
95及其后的操作系统为多个用户提供简档选项，它们没有访问权限的概念，使它们大致相当于单用户的、单户口的操作系统，意味着所有[进程都可修改所有未打开的文件及引导扇区](../Page/进程.md "wikilink")，或对硬盘进行修改。这使病毒及其它被秘密安装的软件更紧密地与操作系统结合在一起，使用户难以对它们进行侦查及撤消。在Windows
9x中的用户简档支持仅是为了方便而已。除非修改一些注册表项，在登录时可按下“取消”以访问系统，即使所有的简档都拥有口令保护。Windows
95的默认登录对话框允许在不登录的情况下创建新用户的简档。

用户或应用程序可通过删除或覆写重要的系统文件来使操作系统无法运作。用户或应用程序也可自由更改配置文件以致操作系统无法启动或正常运作。

安装软件常常覆盖或删除系统文件，但没有正确检查被覆盖或删除的文件是否为更新的版本或正在使用着。这就是[DLL地狱的由来](../Page/DLL地狱.md "wikilink")。

Windows
Me引入[系统文件保护机制及](../Page/系统文件保护.md "wikilink")[系统还原来解决此问题](../Page/系统还原.md "wikilink")。

而被Windows
9x从[DOS继承的](../Page/DOS.md "wikilink")[FAT及](../Page/FAT.md "wikilink")[FAT32](../Page/FAT32.md "wikilink")[文件系统则没有文件保护机制](../Page/文件系统.md "wikilink")，也没有类似[NTFS的](../Page/NTFS.md "wikilink")[文件权限机制](../Page/文件权限.md "wikilink")。

### 硬件支持

#### 驱动程序

Windows
9x的[设备驱动程序可以是虚拟设备驱动程序或WDM驱动程序](../Page/设备驱动程序.md "wikilink")（始于Windows
98）。VxD通常都有文件名字后缀.vxd或.386，而WDM兼容驱动程序则使用[扩展名](../Page/扩展名.md "wikilink")[.sys](../Page/.sys.md "wikilink")。32位VxD消息服务器（msgsrv32）乃一程序，能在启动时加载虚拟设备驱动程序（VxD）并尔后处理与驱动程序的通信。消息服务器也可执行一些背景功能，包括加载Windows
shell（譬如Explorer.exe或Program.exe）。

其它类型的驱动程序有.DRV驱动程序。这些驱动程序在用户模式中被加载，通常被使用来控制设备，譬如多媒体设备。为了访问这些设备，[动态链接库](../Page/动态链接库.md "wikilink")（如MMAYARWM.DLL）是必需的。

写给Windows 9x/Windows
Me的驱动程序被加载到与内核同样的地址空间，说明驱动程序可能有意或无意地覆写操作系统的关键内存区。这将导致系统崩溃，死机及硬盘损坏。故障的操作系统驱动程序乃操作系统不稳定的其一原因。[单内核或](../Page/单内核.md "wikilink")[混合内核之流](../Page/混合内核.md "wikilink")，如[Linux及](../Page/Linux.md "wikilink")[Windows
NT者也易受失常的驱动程序扰乱内核运作](../Page/Windows_NT.md "wikilink")。

驱动及应用程序开发者常常因缺乏为“新”系统编程的经验而造成了许多被用户误认为系统错误的错误，即使错误的发生并不缘由于Windows或DOS本身之故。为此，微软自Windows
95以后，不断地重新设计Windows驱动程序架构。

#### CPU及总线技术

Windows
9x不支持[超线程](../Page/超线程.md "wikilink")、[数据执行保护](../Page/数据执行保护.md "wikilink")、[对称多处理或](../Page/对称多处理.md "wikilink")[多核处理器](../Page/多核处理器.md "wikilink")。

Windows 9x不支持[SATA](../Page/SATA.md "wikilink")
[AHCI](../Page/AHCI.md "wikilink")（Windows 2000及Windows XP預設也不支援SATA
AHCI，需要安裝驅動程式，較為麻煩），或[USB隨身碟](../Page/USB隨身碟.md "wikilink")（Windows
Me除外，Windows 98需要安裝USB隨身碟的驅動程式），但出现为数众多针对Windows
98/Me驱动程序的SATA-I控制器。对Windows
98/Me的硬件驱动程序支持自2005年逐渐减少，特别是主板芯片组和显卡的驱动程序。

早期的Windows
95不支持[USB和](../Page/USB.md "wikilink")[AGP](../Page/AGP.md "wikilink")。

Windows 98是第一款支援[ACPI的Windows](../Page/ACPI.md "wikilink")。

### MS-DOS

[Windows 95远远比](../Page/Windows_95.md "wikilink")[Windows
3.1x及其前的作品更加削弱](../Page/Windows_3.1x.md "wikilink")[MS-DOS在Windows中的影响](../Page/MS-DOS.md "wikilink")。微软称，MS-DOS在Windows
95中有两个角色：一，为引导加载器；二，为16位旧式设备驱动层。

当Windows
95启动时，MS-DOS加载、处理CONFIG.SYS、启动COMMAND.COM、执行AUTOEXEC.BAT，最后执行WIN.COM。WIN.COM程序利用MS-DOS来加载虚拟机管理器、读取SYSTEM.INI、加载虚拟设备驱动程序、关闭任何运行的EMM386副本并切换到保护模式。一旦在保护模式下，虚拟设备驱动程序（[VxD](../Page/VxD.md "wikilink")）将传送所有MS-DOS的状态信息到32位文件系统管理器，并关闭MS-DOS。这些VxD允许Windows
9x直接与硬件资源交互，就像提供了底层功能，譬如32位硬盘访问及内存管理。接下来的文件系统操作将会转送到32位文件系统管理器。

在[Windows
Me中](../Page/Windows_Me.md "wikilink")，WIN.COM不再在启动过程中被执行了，而直接从IO.SYS执行VMM32.VXD。MS-DOS（作为16位旧式设备驱动层）的第二角色是用来在Windows中执行DOS程序的向后兼容工具。许多MS-DOS程序及设备驱动器都以底层的方式与DOS交互，例如：修补低级别BIOS中断，譬如[int
13h](../Page/int_13h.md "wikilink")（底层硬盘IO中断）。当一程序发出一int
21h呼叫以访问MS-DOS，这呼叫将先转到32位文件系统管理器。管理器将试图侦查这一类的修补。若查到此程序尝试挂接到DOS，管理器将跳转到16位代码以让此挂接得以进行。一个被称做IFSMGR.SYS的16位驱动程序将事前被CONFIG.SYS加载以在其它驱动及应用程序之前被挂接到MS-DOS；尔后从16位代码跳转到32位代码，当此DOS程序运行完毕，以让32位文件系统管理器继续工作。据一Windows开发者Raymond
Chen，“此MS-DOS乃一精心圈套。任何16位驱动及应用程序将修补或挂接到一个它们以为是真正MS-DOS的东东，但实乃一圈套尔。当32位文件系统管理器侦查到有人上钩，它必让此圈套呱呱大叫。（MS-DOS
was just an extremely elaborate decoy. Any 16-bit drivers and programs
would patch or hook what they thought was the real MS-DOS, but which was
in reality just a decoy. If the 32-bit file system manager detected that
somebody bought the decoy, it told the decoy to quack.）”\[9\]

#### MS-DOS虚拟机制

在Windows 9x中，MS-DOS应用程序运行在“虚拟DOS机”（Virtual DOS Machine）中。

#### MS-DOS模式

Windows 95及Windows
98也提供对DOS应用程序的回归支持以能够启动进入原始“DOS模式”（MS-DOS可以在不启动Windows引导，且不使用保护模式）。通过Windows
9x的内存管理器及其它的“后DOS”改进，整体系统的性能及功能得到改善。这异于在基于Windows
NT操作系统中所使的仿真手段。一些非常老旧的应用程序或硬件都需要“DOS模式”。

使用命令行模式，在不进入GUI时，能够修复特定的系统错误。例如，如果一病毒活跃于GUI模式中并锁住自身的文件，那么它通常可在DOS模式中通过删除病毒文件被安全去除。

同样的，损坏的注册表文件、系统文件或启动文件都可用命令行来恢复。在命令提示符中键入可从DOS模式中启动Windows 95及Windows
98。但[Windows
2000中的](../Page/Windows_2000.md "wikilink")[恢复控制台](../Page/恢复控制台.md "wikilink")（Recovery
Console）也扮演着类似的角色。

由于DOS没有多任务处理机制，基于DOS的Windows版本（如9x）缺少文件系统安全机制，譬如文件权限。再者，若用户使用16位DOS驱动程序，Windows可能变得不稳定。在Windows
9x系列中就频繁发生硬盘错误。

### 用户界面

用户可通过[命令行界面](../Page/命令行界面.md "wikilink")（CLI）或[图形用户界面](../Page/图形用户界面.md "wikilink")（GUI）来控制一个基于Windows
9x的系统。对于桌面系统，默认模式通常为GUI，而[MS-DOS](../Page/MS-DOS.md "wikilink")
Windows则使用CLI。

Windows 9x的GUI运行在一个基于DOS的层。

不同于加载到[内核空间的Windows](../Page/内核空间.md "wikilink") NT
[GDI](../Page/圖形設備接口.md "wikilink")，身为Win32及Win16子系统的Windows
9x GDI被加载到[用户空间](../Page/用户空间.md "wikilink")。

[Alpha通道或透明特效](../Page/Alpha通道.md "wikilink")，譬如菜单的渐变效果，都不被Windows 9x
[GDI支持](../Page/圖形設備接口.md "wikilink")。

[Windows
Explorer乃台式机的默认用户界面](../Page/Windows_Explorer.md "wikilink")，尽管出现了各式各样的[Windows
Shell更换软件](../Page/Windows_Shell.md "wikilink")。

其它种类的GUI有[LiteStep](../Page/LiteStep.md "wikilink"),
[bbLean及](../Page/bbLean.md "wikilink")[Program
Manager](../Page/Program_Manager.md "wikilink")。GUI让用户更方便地控制个别应用程序的位置与外观，及更好地与Windows系统交互。

## 注释

## 关联条目

  - [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")
  - [Windows XP](../Page/Windows_XP.md "wikilink")
  - [Windows 3.x](../Page/Windows_3.x.md "wikilink")
  - [MS-DOS](../Page/MS-DOS.md "wikilink")

[Category:Microsoft
Windows](../Category/Microsoft_Windows.md "wikilink")

1.
2.  [Windows OS Version
    Numbers](http://www.nirmaltv.com/2009/08/17/windows-os-version-numbers/)
3.  [Chronology of Microsoft Windows Operating
    Systems](http://www.islandnet.com/~kpolsson/windows/win1997.htm)
4.  [Windows 98: Stable and fast, as well as "new and
    improved"](http://aroundcny.com/Technofile/texts/tec032998.html)
5.
6.  [No support for Windows 98 in Mozilla
    Firefox](http://support.mozilla.com/tiki-view_forum_thread.php?locale=et&comments_parentId=74873&forumId=1)

7.
8.
9.