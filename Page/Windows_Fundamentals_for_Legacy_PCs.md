**Windows Fundamentals for Legacy
PCs**（WinFLP）是[微软公司於](../Page/微软.md "wikilink")2006年7月8日發表的[精簡客戶端](../Page/精簡客戶端.md "wikilink")（Thin
client）版本Windows XP。它在2005年中開發時用了**Eiger**和**Mönch** 這兩個開發代號。

微軟開發這個精簡客戶端的目的，主要是協助企業把舊有的電腦過渡至可執行只適用於Windows
XP的新軟件，例如由[香港政府推出的](../Page/香港政府.md "wikilink")2001年版[HKSCS支援](../Page/HKSCS.md "wikilink")。作為Win9x與WinXP的過渡系統，WinFLP可於比較舊的硬體上運行，但由於其核心仍然與[Windows
XP](../Page/Windows_XP.md "wikilink") SP2相同，所以所有適用於Windows
XP的軟體都可以在WinFLP上運行。不過，其功能只維持最基本的能力，例如[Windows
防火牆](../Page/Windows_防火牆.md "wikilink")、[群組原則](../Page/群組原則.md "wikilink")（Group
Policy）、[Windows
Update及其他管理功能之類](../Page/Windows_Update.md "wikilink")。一般日常使用的應用軟體，都要透過執行[遠端桌面協定來執行安裝於伺服器的軟件](../Page/遠端桌面協定.md "wikilink")。所以，其實這套作業系統只是把企業內的舊電腦變成一台以WinXP為基礎的[無碟工作站](../Page/無碟工作站.md "wikilink")。
WinFLP不會作公開發售，只是純粹為企業用戶訂購而發送。

## 特性

  - 去除了[控制台环境下的第一步](../Page/命令行界面.md "wikilink")（蓝屏）安装过程，并基本上一直用[图形界面引导用户](../Page/图形界面.md "wikilink")。
  - 采用镜像復原安装，速度大大提高，也减少了电脑的负担（與[Windows
    Vista相似](../Page/Windows_Vista.md "wikilink")）。
  - 安裝模式，有 最小化／典型／全部／自定義 的自行選擇之選項。
  - 安裝组件，有 Additional Driver Support、[Internet
    Explorer](../Page/Internet_Explorer.md "wikilink")、Language
    Support、Local Management Support、Windows Help Files、[Windows Media
    Player](../Page/Windows_Media_Player.md "wikilink")、[Windows
    Messenger](../Page/Windows_Messenger.md "wikilink")
    的自行選擇之選項。部份组件比WinXP专业版没有任何减少（如果全选的话），當然遊戲的附件就減去了。
  - 對管理員密碼默認啟用了複雜度要求，要求至少6個字符的長度，必須包括以下四項的三項：大寫字符、小寫字符、數字、符號，而且不能包含用戶的名字。
  - 由於發行较晚，不但没有缩水，反而有了很多改进。WinXP中的許多错误已得到修正，驱动程序也是更新过的，死机和无法挽救性错误少了很多。
  - 预设置更为合理，计算机会得到令人惊喜的启动速度和运行效率。
  - 视觉效果、壁纸、屏幕保护等等，就几乎完全被精简掉了。
  - 登錄模式與2003方式相同，需使用
    [Ctrl+Alt+Del](../Page/Ctrl+Alt+Del.md "wikilink")。
  - [Windows
    Vista的痕迹](../Page/Windows_Vista.md "wikilink")（WinFLP里面“有Vista的影子”）。例如采用Windows
    Vista的[WIM安装方式](../Page/WIM.md "wikilink")，以及一些不可捉摸的改进。
  - 部份軟件（如[Microsoft Office
    XP](../Page/Microsoft_Office_XP.md "wikilink")、[Windows Live
    Messenger](../Page/Windows_Live_Messenger.md "wikilink") 8.x）有兼容問題。
  - 不支持[拨号上网](../Page/拨号上网.md "wikilink")。可以安装Modem驱动和创建连接，但拨号时出现[错误633](../Page/错误633.md "wikilink")。

## 最低系統要求

  - 64MB的RAM（建議128MB）
  - [Pentium等級的中央處理器](../Page/Pentium.md "wikilink")
  - 500MB的硬碟空間（建議1GB）
  - 800×600或更高的螢幕解析度
  - 網路卡

## 外部連結

  - [Microsoft Delivers 'Eiger' Lean
    Client](https://archive.is/20130427035038/http://www.microsoft-watch.com/article2/0,2180,1987951,00.asp)
  - [Windows Fundamentals for Legacy PCs for Software
    Assurance](https://web.archive.org/web/20060718203106/http://www.microsoft.com/licensing/programs/sa/benefits/fundamentals.mspx)
    - Information from Microsoft on this release
  - [Terminal Server Progress and
    Commitment](https://web.archive.org/web/20060904174214/http://www.digicomp.ch/misc/documents/citrixday/Microsoft_Terminal_Server_Progress_and_Commitment.pdf)
    - PDF presentation; WinFLP is described at the end
  - [Exclusive: Microsoft Windows XP Codenames: "Eiger" and
    "Mönch"](http://bink.nu/Article3812.bink) - partial specs on Eiger
    and Mōnch.
  - [Test of Microsoft's Windows Fundamentals for Legacy
    PCs](https://web.archive.org/web/20121023080228/http://bink.nu/Article7745.bink)
    at Bink.nu
  - [Screenshot
    gallery](https://web.archive.org/web/20060901045542/http://bink.nu/photos/news_article_images/category1021.aspx)
    at Bink.nu

[Category:Windows XP](../Category/Windows_XP.md "wikilink")