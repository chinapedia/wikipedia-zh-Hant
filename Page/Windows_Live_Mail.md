**Windows Live Mail**（以前稱為**Windows Live Mail
Desktop**）是一個的[電子郵件](../Page/電子郵件.md "wikilink")[用戶端](../Page/用戶端.md "wikilink")，屬於微軟[Windows
Live系列的產品之一](../Page/Windows_Live.md "wikilink")，是[Windows
XP上的](../Page/Windows_XP.md "wikilink")[Outlook
Express和](../Page/Outlook_Express.md "wikilink")[Windows
Vista上的](../Page/Windows_Vista.md "wikilink")[Windows
Mail後繼版本](../Page/Windows_Mail.md "wikilink")。它包含了Windows
Mail上原有的功能和特性，並整合了存取Windows Live Hotmail網路Web介面的功能。

它包含於[Windows Live程式集內](../Page/Windows_Live程式集.md "wikilink")。

## 相關條目

  - [Windows Live](../Page/Windows_Live.md "wikilink")
  - [Outlook.com](../Page/Outlook.com.md "wikilink")
  - [Windows Mail](../Page/Windows_Mail.md "wikilink")

## 外部連結

  - [Windows Live Mail](http://explore.live.com/windows-live-mail)

[Category:Windows Live](../Category/Windows_Live.md "wikilink")