**WMA**（**Windows Media
Audio**）是[微软公司开发的一系列](../Page/微软.md "wikilink")[音频编解码器](../Page/音频编解码器.md "wikilink")，也指相应的[數位](../Page/數位.md "wikilink")[音频编码格式](../Page/音频.md "wikilink")。WMA包括四种不同的编解码器：（1）
*WMA*，原始的WMA编解码器，作为[MP3和](../Page/MP3.md "wikilink")[RealAudio编解码器的竞争者](../Page/RealAudio.md "wikilink")\[1\]\[2\]；（2）
*WMA Pro*，支持更多声道和更高质量的音频\[3\]；（3） *WMA
Lossless*，[无损编解码器](../Page/无损压缩.md "wikilink")；（4）*WMA
Voice*，用于储存语音，使用的是低码率压缩\[4\]。一些使用Windows Media
Audio编码格式编码其所有内容的纯音频[ASF文件也使用WMA作为扩展名](../Page/ASF.md "wikilink")。

WMA格式最初为微软公司所开发，但是随着众多播放器对它的支持，这个格式正在成为[MP3格式的竞争对手之一](../Page/MP3.md "wikilink")。它兼容MP3的ID3元数据标签，同时支持额外的标签。另外，一般情况下相同音质的WMA和MP3音频，前者文件体积较小。

WMA可以用于多种格式的编码文件中。应用程序可以使用Windows Media Format
SDK进行WMA格式的编码和解码。一些常见的支持WMA的应用程序包括[Windows
Media Player](../Page/Windows_Media_Player.md "wikilink")、[Windows Media
Encoder](../Page/Windows_Media_Encoder.md "wikilink")、[RealPlayer](../Page/RealPlayer.md "wikilink")、[Winamp等等](../Page/Winamp.md "wikilink")。其它一些平台，例如[Linux和移动设备中的软硬件也支持此格式](../Page/Linux.md "wikilink")。

## 特色

### 优点

WMA
7之后的WMA支持证书加密，未经许可（即未获得许可证书），即使是非法拷贝到本地，也是无法收听的。同时，微软公司开始时宣称的：同文件比MP3体积小一倍而音质不变，也得到了兑现。事实上，这个说法，仅仅适用于低[位元率的情况](../Page/位元率.md "wikilink")，另外，微软公司在WMA
9大幅改进了其引擎，实际上64Kbps的WMA音樂就可以達到与128Kbps的MP3音樂接近的音質，比MP3體積少1/3左右\[5\]，意味在相同的空間下，你可以多儲存1倍的音樂，但卻可以擁有一樣的音質，因此非常適合用於網路[串流媒體及行動裝置](../Page/串流媒體.md "wikilink")。但是2003年9月的一次公众听力测试显示，听众认为128Kbps的Lame
MP3音频的质量显著优于64Kbps的WMA音频，甚至相近码率的MP3pro音频也普遍优于WMA音频。此外，参与测试的所有格式（包括Real，HE
AAC，QT AAC，Vorbis）都没能在一半码率的条件下胜过MP3音频的质量。\[6\]

### 缺点

與MP3相同，通常的WMA也是有損數據壓縮的檔案格式，特別在高位元率的音質渲染力明顯不足，對於有更高要求的用戶來說WMA並不是一個適合的格式。此外WMA也與MP3一樣同為有專利版權的檔案格式。支援的裝置需要購買使用版權。

## 参考资料

<references/>

## 参见

  - [音訊檔案格式](../Page/音訊檔案格式.md "wikilink")
  - [WMV](../Page/WMV.md "wikilink")
  - [Windows Media Player](../Page/Windows_Media_Player.md "wikilink")
  - [ASF](../Page/ASF.md "wikilink")
  - [MP3](../Page/MP3.md "wikilink")

## 外部链接

  - [微软的媒体服务主页](http://www.microsoft.com/windows/windowsmedia/default.aspx)
  - [Windows Media
    文件扩展名指南](http://msdn.microsoft.com/library/default.asp?url=/library/en-us/wmform95/htm/sampleapplications.asp)

[Category:數位音訊](../Category/數位音訊.md "wikilink")
[Category:音頻格式](../Category/音頻格式.md "wikilink")
[Category:音频编解码器](../Category/音频编解码器.md "wikilink")
[Category:Windows多媒体](../Category/Windows多媒体.md "wikilink")
[Category:有損壓縮演算法](../Category/有損壓縮演算法.md "wikilink")
[Category:無損壓縮演算法](../Category/無損壓縮演算法.md "wikilink")

1.

2.

3.

4.
5.  [Audio 8 及
    Video 8](http://www.microsoft.com/windows/windowsmedia/tw/format/audvid8.aspx)

6.