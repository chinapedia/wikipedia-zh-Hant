**Windows Movie Maker**（包括前稱**Windows Live Movie
Maker**的版本；開發代碼為***Sundance***）[微軟](../Page/微軟.md "wikilink")[Windows
Live開發的影片編輯軟體](../Page/Windows_Live.md "wikilink")，可以製作、編輯及分享家庭[影片](../Page/影片.md "wikilink")，並新增特殊效果、[音樂及](../Page/音樂.md "wikilink")[旁白](../Page/旁白.md "wikilink")，然後透過[網路](../Page/網路.md "wikilink")、[電子郵件或](../Page/電子郵件.md "wikilink")[CD分享您製作的電影](../Page/CD.md "wikilink")，對家庭用戶來說，這個入門級是簡單易學的。

曾一度推出改名**Windows Live Movie Maker**的版本\[1\]。“Windows Live Movie
Maker”於2008年9月17日在 Windows Live 系列 Wave 3 測試版發布。正式版本于2009年8月19正式推出。

但在2012年8月7日新版本又改稱“**Windows Movie Maker**”。它包含於
[Windows程式集內](../Page/Windows程式集.md "wikilink")。Windows Movie
Maker 2012無法在[Windows
Vista或](../Page/Windows_Vista.md "wikilink")[Windows
XP安裝](../Page/Windows_XP.md "wikilink")\[2\]。

## 概要

Windows Movie
Maker可以將[音訊和](../Page/音訊.md "wikilink")[視訊從](../Page/視訊.md "wikilink")[數位攝影機擷取到電腦](../Page/數位攝影機.md "wikilink")，然後將所擷取的內容運用在電影中。除此之外，亦可以將現有的音訊、視訊或[圖片匯入至Windows](../Page/圖片.md "wikilink")
Movie Maker，以用於所建立的電影中。

在Windows Movie
Maker中可以編輯音訊及視訊，內容經[設計及編輯之後](../Page/設計.md "wikilink")（包括新增特殊效果、新增標題、視訊轉換或效果），就可以把電影儲存，並將它與朋友或家人分享。

## 介面的改變

相對於舊有的Movie Maker原本的介面，全新的Windows Movie Maker介面更偏向於Windows 7系列與Office
2010包含程式使用的一致性介面，然而，取消了原本舊有的Movie
Maker中的集合窗格，卻使得其不再容易整理匯入的影片，且由於不能直接拖拉，在編輯上也變得更加麻煩。

## 歷史

[Windows_Movie_Maker_on_Vista.png](https://zh.wikipedia.org/wiki/File:Windows_Movie_Maker_on_Vista.png "fig:Windows_Movie_Maker_on_Vista.png")中的Windows
Movie Maker\]\] Windows Movie Maker最初內置於[Windows
ME](../Page/Windows_ME.md "wikilink")。其後，版本1.1內置在[Windows
XP](../Page/Windows_XP.md "wikilink")。2002年11月，Windows Movie
Maker2.0版本發布並且增加了一些新的特點。2.1版本，包含一些小更新，內置於Windows XP Service Pack
2。[Windows XP Media Center
Edition](../Page/Windows_XP_Media_Center_Edition.md "wikilink")
2005內置了2.5版本。[Windows
Vista中](../Page/Windows_Vista.md "wikilink")，Windows Movie
Maker內置2.6版。

|            |                                                                                                                                                       |                                                                                                                                            |
| ---------- | ----------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------ |
| 2000年      |                                                                                                                                                       | Windows Movie Maker 1.0內置[Windows Me](../Page/Windows_Me.md "wikilink")                                                                    |
| 2001年      |                                                                                                                                                       | Windows Movie Maker 1.1內置[Windows XP](../Page/Windows_XP.md "wikilink")                                                                    |
| 2002年      |                                                                                                                                                       | Windows Movie Maker 2.0執行於[Windows XP](../Page/Windows_XP.md "wikilink")                                                                   |
| 2004年      |                                                                                                                                                       | Windows Movie Maker 2.1內置[Windows XP](../Page/Windows_XP.md "wikilink") SP2                                                                |
| 2004年      |                                                                                                                                                       | Windows Movie Maker 2.5內置[Windows XP Media Center Edition](../Page/Windows_XP_Media_Center_Edition.md "wikilink") 2005                     |
| 2006年      |                                                                                                                                                       | Windows Movie Maker 2.6執行於[Windows Vista](../Page/Windows_Vista.md "wikilink")                                                             |
| 2007年      | [Windows_Movie_Maker_icon.png](https://zh.wikipedia.org/wiki/File:Windows_Movie_Maker_icon.png "fig:Windows_Movie_Maker_icon.png")                 | Windows Movie Maker內置於[Windows Vista](../Page/Windows_Vista.md "wikilink")                                                                 |
| 2008年9月17日 | [Windows_Live_Movie_Maker_logo.png](https://zh.wikipedia.org/wiki/File:Windows_Live_Movie_Maker_logo.png "fig:Windows_Live_Movie_Maker_logo.png") | Windows Live Movie Maker執行於[Windows Vista](../Page/Windows_Vista.md "wikilink")                                                            |
| 2010年6月20日 | [Windows_Live_Movie_Maker_logo.png](https://zh.wikipedia.org/wiki/File:Windows_Live_Movie_Maker_logo.png "fig:Windows_Live_Movie_Maker_logo.png") | Windows Live Movie Maker 2011 (Wave 4)執行於[Windows Vista及](../Page/Windows_Vista.md "wikilink")[Windows 7](../Page/Windows_7.md "wikilink") |
| 2012年8月7日  | [Windows_Live_Movie_Maker_logo.png](https://zh.wikipedia.org/wiki/File:Windows_Live_Movie_Maker_logo.png "fig:Windows_Live_Movie_Maker_logo.png") | Windows Movie Maker 2012 (Wave 5)執行於[Windows 7及](../Page/Windows_7.md "wikilink")[Windows 8](../Page/Windows_8.md "wikilink")              |
|            |                                                                                                                                                       |                                                                                                                                            |

## 偽造木馬軟體

2017年1月17日，微软停止对Windows Live Movie Maker支持。但是谷歌上搜索“Windows Movie
Maker”，会跳出一个名为www.windows-movie-maker.org的网站。点击链接之后，页面的布局和设计看上去像是正规网站，并且提供了Windows
7/8/10/XP/Vista系统版本的下载。然而，当你尝试下载Windows Movie
Maker的时候用户需要支付29.95美元才能享受“完整版本”。但事实上真正的Windows
Movie Maker是完全免费的。

除了谷歌搜索网站之外，有一陣子在用户在Microsoft
Store上搜索这款视频编辑应用也會找到，虽然名字相同，但是它完全不具备微软官方应用的编辑功能。\[3\]另外，即使是微软自家的[bing](../Page/bing.md "wikilink")，冒牌网站也堂而皇之的现身结果第一页。安全公司[ESET在最新的](../Page/ESET.md "wikilink")2017年11月防病毒威胁报告中，将山寨Windows
Movie
Maker列为全球TOP3威胁之一（Win32/Hoax.MovieMaker），其中以色列地区受损最严重（第一位），另外，从2017年11月6日开始，菲律宾、丹麦、芬兰等地也开始出现大面积中招反馈。\[4\]

## 註釋

## 參見

  - [Windows Live](../Page/Windows_Live.md "wikilink")

## 外部链接

  - [Windows Movie
    Maker](http://windows.microsoft.com/zh-TW/windows-live/movie-maker-get-started)
  - [1](https://web.archive.org/web/20080424045330/http://www.microsoft.com/windowsxp/using/moviemaker/default.mspx)
  - [2](http://www.download.com/Windows-Movie-Maker-Windows-XP-/3000-13631_4-10165075.html)

[Category:微软软件](../Category/微软软件.md "wikilink")
[Category:视频编辑软件](../Category/视频编辑软件.md "wikilink")
[Category:Windows组件](../Category/Windows组件.md "wikilink") [Movie
Maker](../Category/Windows_Live.md "wikilink")

1.  類似[Windows Live 影像中心取代](../Page/Windows_Live_影像中心.md "wikilink")
    [Windows 相片圖庫](../Page/Windows_相片圖庫.md "wikilink")；如同[Windows Live
    Mail來取代](../Page/Windows_Live_Mail.md "wikilink") [Windows
    Mail的名稱](../Page/Windows_Mail.md "wikilink")。但在2012年8月7日“Windows
    Live 影像中心”改稱“Windows 影像中心”，而“Windows Live Movie Maker”也改稱“**Windows
    Movie Maker**”。
2.  參見 [Windows Live
    程式集系統需求](http://windows.microsoft.com/zh-TW/windows/Windows-Essentials-2012-system-requirements)
3.  [虚假微软应用借助Google搜索引擎传播勒索病毒](http://tech.sina.com.cn/i/2017-11-10/doc-ifynsait6883823.shtml).[新浪科技](../Page/新浪科技.md "wikilink").\[2017-11-10\]
4.  [神伪装！冒牌Windows Movie
    Maker网络肆虐：密集中招](http://tech.qianlong.com/2017/1110/2163465.shtml).[千龙网](../Page/千龙网.md "wikilink").\[2017-11-10\]