**Windows NT 3.1**是[微软的](../Page/微软.md "wikilink")[Windows
NT产品线的第一代产品](../Page/Windows_NT.md "wikilink")，用于[服务器和商业桌面](../Page/服务器.md "wikilink")[操作系统](../Page/操作系统.md "wikilink")，于1993年7月27日发表。版本号的选择是为了匹配[Windows
3.1](../Page/Windows_3.1.md "wikilink")，[微软当时最新版的图形用户界面](../Page/微软.md "wikilink")，以表明它们拥有非常类似的用户界面方面的视觉效果。有两个版本的NT
3.1可供选择，Windows NT 3.1和**Windows NT Advanced Server**。

它可以运行在Intel [x86](../Page/x86.md "wikilink")，[DEC
Alpha和](../Page/DEC_Alpha.md "wikilink")[MIPS
R4000的](../Page/MIPS_architecture.md "wikilink")[CPU上](../Page/CPU.md "wikilink")。

## 對Windows NT 3.1的一般看法

Windows
NT的开发工作始于1988年11月，当时微软雇用了一组开发人员，他们来自[DEC公司](../Page/Digital_Equipment_Corporation.md "wikilink")，由[Dave
Cutler领导](../Page/Dave_Cutler.md "wikilink")。许多设计元素反映了DEC在[VMS和](../Page/Virtual_Memory_System.md "wikilink")[RSX-11上的前期经验](../Page/RSX-11.md "wikilink")。这一操作系统设计以运行于多种[指令集结构和每种结构里的多种硬件平台之上](../Page/指令集结构.md "wikilink")。这种平台无关性通过一个叫做[HAL的核心模式模块最大程度地隐藏在系统其余部分之外](../Page/硬件抽象层.md "wikilink")。

Windows NT最开始计划用于[OS/2](../Page/OS/2.md "wikilink")
3.0，由微软和[IBM联合开发的操作系统的第三版](../Page/International_Business_Machines.md "wikilink")。但当[Windows
3.0于](../Page/Windows_3.0.md "wikilink")1990年5月发表后，取得了异常的成功，于是微软决定修改当时尚未发表的NT
OS/2（它当时叫这个名字）的主要[应用程序接口](../Page/应用程序接口.md "wikilink")，由对OS/2
API的扩展改为对[Windows
API的扩展](../Page/Windows_API.md "wikilink")。这一决定导致了微软和IBM之间关系的紧张，他们的合作最终破裂。IBM独自继续对OS/2的开发，而微软则继续在新命名的Windows
NT上的开发工作。

Windows NT的首次公开演示，当时它叫作“Windows Advanced Server for Lan
Manager”，是在1991年8月的一个开发人员大会上\[1\]，并于1993年在[佐治亚州](../Page/佐治亚州.md "wikilink")[亚特兰大市召开的Comdex春季展会上正式宣布](../Page/亚特兰大市.md "wikilink")。

## N-Ten

起初这一操作系统的定位是[Intel i860](../Page/Intel_i860.md "wikilink")
CPU，代号为N10（英文读作"**N-T**en"），NT操作系统家族便由此得名。\[2\]
但i860“严重落后于计划”，于是NT小组在i860原型硬件（代号为*Dazzle*）交付之前使用了一个模拟器。对其它平台的支持将随后提供。起初定位于i860的原因是为了提高可移植性，避免制造一个以x86为中心的设计方案。\[3\]

## 類似Windows 3.1

Windows NT 3.1的用户界面与Windows 3.1的用户界面非常相似，以致于人们有时候会把它与Windows
3.1相混淆。这使得那些在Windows 3.x上有使用经验的人们用起来非常容易。

## 參考資料

<references />

## 外部連結

  - [GUIdebook: Windows NT 3.1
    Gallery](http://www.guidebookgallery.org/guis/windows/winnt31) - A
    website dedicated to preserving and showcasing Graphical User
    Interfaces

[Category:Windows NT](../Category/Windows_NT.md "wikilink")
[Category:Discontinued Microsoft
software](../Category/Discontinued_Microsoft_software.md "wikilink")

1.
2.
3.  <http://www.usenix.org/events/usenix-win2000/invitedtalks/lucovsky_html/>