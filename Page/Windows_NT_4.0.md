**Windows NT 4.0**是[微軟](../Page/微軟.md "wikilink")[Windows
NT家族的第四套產品](../Page/Windows_NT.md "wikilink")，代號Cairo，於1996年7月29日發行給製造商。為一個[32位元的作業系統](../Page/32位元.md "wikilink")，分為工作站以及伺服器版本。而其圖形操作界面類似於[Windows
95](../Page/Windows_95.md "wikilink")。根據[比爾蓋茲所言](../Page/比爾蓋茲.md "wikilink")，產品名稱中的"NT"為"New
Technology（新技術）"的意思，然而時至今日，該名稱已經沒有其他特殊意義存在。

雖然穩定性高於Windows
95，然而從桌上型電腦的觀點來說，操作界面稍微欠缺了彈性。系統穩定性大部分要歸功於將硬體資源虛擬化，軟體必須藉由系統的[API以使用硬體資源](../Page/API.md "wikilink")，而不像[DOS以及Windows](../Page/DOS.md "wikilink")
95（包括稍後的版本）時期直接由軟體進行控制的緣故。但穩定的代價就是利用API進行操作所需要的步驟遠比直接操作硬體資源多，因此造成硬體需求廣泛的程式（如：遊戲）執行上緩慢許多。許多以[Win32](../Page/Win32.md "wikilink")
API開發的程式可以在Windows 95以及Windows NT上執行，但當時的主流3D遊戲則因為NT
4.0對[DirectX的支援有限而無法在NT](../Page/DirectX.md "wikilink")4上面運作。

Windows NT 4.0在進行維護管理工作的時候，使用者界面比起Windows 95較為不友善，舉例而言，對於電腦的硬體沒有device
management overview.

NT與"9x"的分界線直到[Windows
XP的推出之後才消失](../Page/Windows_XP.md "wikilink")，原因包括遊戲用的APIs—諸如[OpenGL以及](../Page/OpenGL.md "wikilink")[DirectX](../Page/DirectX.md "wikilink")—已經成熟到有夠高的運作效率，並且加上硬體本身也有夠高的效能，才能夠以可接受的速度運行[API](../Page/application_programming_interface.md "wikilink")。

Windows NT 4.0在[Windows
2000推出之後](../Page/Windows_2000.md "wikilink")，微軟已經終止所有關於NT4的服務，然而到2007年為止，儘管微軟希望他們的客戶可以換成新的版本，依然有許多的公司企業配合舊硬體持續穩定的運作該系統中。

## 特色

最值得注意的特點是不論工作站或者伺服器版本的Windows NT 4.0都使用[Windows
95的使用者介面](../Page/Windows_95.md "wikilink")，包括[Windows
Shell](../Page/Windows_Shell.md "wikilink")、[Windows
Explorer](../Page/Windows_Explorer.md "wikilink")（被稱為Windows NT
Explorer）、以及使用"我的"命名法（舉例：[我的文件](../Page/我的文件.md "wikilink")、[我的電腦](../Page/我的電腦.md "wikilink")）。

伺服器版本的Windows NT
4.0內建了[網頁伺服器](../Page/網頁伺服器.md "wikilink")，[IIS](../Page/網際網路資訊服務.md "wikilink")
2.0。並且直接支援[Microsoft
FrontPage](../Page/Microsoft_FrontPage.md "wikilink")（撰寫以及管理網站的一支應用程式）的外掛插件（plugins）以及延伸資源（extensions）。

此版本另一個重要的特色為針對網路應用程式提供了[Microsoft Transaction
Server](../Page/Microsoft_Transaction_Server.md "wikilink")，以及[Microsoft
Message Queuing](../Page/Microsoft_Message_Queuing.md "wikilink")
(MSMQ)，提高了通訊能力。

而跟之前版本的明顯差異，即為Windows NT將[Graphics Device
Interface](../Page/Graphics_Device_Interface.md "wikilink")
(GDI)整合進系統核心\[1\]以提升[圖形使用者介面](../Page/圖形使用者介面.md "wikilink")（GUI）的效率，使得系統效能跟[Windows
NT
3.51相比有長足的進步](../Page/Windows_NT_3.51.md "wikilink")，不過也使得圖形驅動程式也必須放在核心之中，造成潛在的穩定性問題。

Windows NT
4.0的其中一項缺點為欠缺對[Direct3D的支援](../Page/Direct3D.md "wikilink")。不過這個問題在之後版本的NT家族中（例如[Windows
2000](../Page/Windows_2000.md "wikilink")）獲得了解決。Windows NT
4.0也不支援[USB](../Page/USB.md "wikilink")，這項問題也伴隨著[Windows
2000的問世而消除](../Page/Windows_2000.md "wikilink")。也有第三方廠商開發的公用程式提供了Windows
NT 4.0對於DirectX以及USB的支援。

## Service Packs

為了修正[程序错误](../Page/程序错误.md "wikilink")，微軟在Windows NT
4.0的產品生命週期中，曾經釋出數個Service
Pack，包括服務更新包跟選擇更新包。最後完整的Service Pack為Service
Pack 6a (SP6a)。

| 軟體版本                                        | 發行日         |
| ------------------------------------------- | ----------- |
| 釋出給製造商（[RTM](../Page/软件版本周期.md "wikilink")） | 1996年7月29日  |
| 一般釋出                                        | 1996年8月24日  |
| Service Pack 1                              | 1996年10月16日 |
| Service Pack 2                              | 1996年12月14日 |
| Service Pack 3                              | 1997年5月15日  |
| Service Pack 4                              | 1998年10月25日 |
| Service Pack 5                              | 1999年5月4日   |
| Service Pack 6                              | 1999年11月22日 |
| Service Pack 6a                             | 1999年11月30日 |
| Service Pack 6a後安全性更新                       | 2001年7月26日  |

SP7原本預定在2001年初期釋出，但是卻成為**SP6a後安全更新**，而不是一個完整的Service Pack。

釋出的Service packs以及option
pack也被用來增加新的功能。包括新版本的[IIS](../Page/網際網路資訊管理員.md "wikilink")
3.0和4.0，增加[Active Server
Pages](../Page/Active_Server_Pages.md "wikilink")、公鑰以及憑證授權功能、智慧卡（Smart
Card）支援、強化[對稱多處理機](../Page/對稱多處理機.md "wikilink")（**SMP** **S**ymmetric
**M**ulti
**P**rocessor）的彈性，叢集支援、以及[組件對象模型](../Page/組件對象模型.md "wikilink")（COM）支援、以及其他等等。

## 發行版本

### 伺服器

  - **Windows NT 4.0伺服器**，發行於1996年，以小規模企業的需要為設計原則的伺服器系統。
  - **Windows NT
    4.0伺服器，企業版**，發行於1997年，是首次在Windows伺服器版本的作業系統中冠上**企業版**名稱的系統。企業版伺服器被設計在高使用率，高流量的網路中運作。
  - **Windows NT 4.0終端伺服器**，發行於1998年，允許使用者遠端登入。在[Windows
    2000中](../Page/Windows_2000.md "wikilink")，同樣的功能被稱為**[終端機服務](../Page/終端機服務.md "wikilink")**，而在[Windows
    XP以及](../Page/Windows_XP.md "wikilink")[Windows Server
    2003中](../Page/Windows_Server_2003.md "wikilink")，被稱為**遠端桌面**.

Windows NT 4.0伺服器被包含於[BackOffice Small Business
Server](../Page/Microsoft_Small_Business_Server.md "wikilink")4.0以及4.5套裝軟體中。

### 其他

  - **Windows NT 4.0工作站**
    的設計目標為針對一般商業使用的桌上型作業系統。以穩定的純32位元作業環境為宣傳，快速的在目標市場中獲得成功地位。
  - **Windows NT 4.0嵌入式系統**
    為針對特定裝置，例如[ATM而設計的小型系統](../Page/自動櫃員機.md "wikilink")。

## 安全性

Windows
NT4.0對於安全性問題[MS03-010](https://web.archive.org/web/20070417193930/http://www.microsoft.com/technet/security/Bulletin/MS03-010.mspx)並沒有任何更新。
沒有更新的原因為，微軟宣稱「基於Windows NT4.0和Windows 2000一些基本上的差異，為了修正此錯誤而重新編譯Windows
NT4.0是不可能的，因為這需要重新建置大部分Windows NT
4.0的作業系統，而不是僅僅修改受影響的的[RPC元件](../Page/Remote_Procedure_Control，遠端程序控制.md "wikilink")，這種規模的系統更新將不能保證原本為了Windows
NT 4.0設計的程式能夠繼續在更新過的系統上運作。」

作為替代方案，微軟建議Windows NT 4.0使用者以安裝防火牆阻擋連接埠135以保護他們的NT 4.0系統。

於2004年12月31日，微軟終止了Windows NT 4.0，包括安全更新的所有技術支援。因此，微軟建議目前的Windows
NT客戶升級為更新、更安全的作業系統，例如[Windows
XP或者](../Page/Windows_XP.md "wikilink")[Windows Server
2003](../Page/Windows_Server_2003.md "wikilink")。

## 外部連結

  - [Windows NT Workstation 4.0 Official Product
    Page](https://web.archive.org/web/20070310075951/http://www.microsoft.com/ntworkstation/)
  - [Windows NT Server 4.0 Official Product
    Page](https://web.archive.org/web/20060424083337/http://www.microsoft.com/ntserver/)
  - [Screen shots of Windows NT 4.0
    Workstation](http://toastytech.com/guis/nt4.html)
  - [Guidebook: Windows NT 4.0
    Gallery](http://www.guidebookgallery.org/guis/windows/winnt40) – A
    website dedicated to preserving and showcasing Graphical User
    Interfaces
  - [HPC:Factor Windows NT 4.0 Workstation Patches & Updates
    Guide](http://www.hpcfactor.com/qlink/?linkID=16)
  - [HPC:Factor Windows NT 4.0 Server Patches & Updates
    Guide](http://www.hpcfactor.com/qlink/?linkID=149)
  - [Windows NT 4.0 Reference Material](http://nt4ref.zcm.com.au/)
  - [Windows NT 4 server轉移到Windows 2008 server R2
    (Migration)](http://www.hkitblog.com/?p=5257)

## 參考資料

<references/>

  -
[Category:Windows NT](../Category/Windows_NT.md "wikilink")
[Category:Windows服务器](../Category/Windows服务器.md "wikilink")

1.  [Windows IT Pro - Windows NT 4.0,
    April 1996](http://www.windowsitpro.com/Windows/Articles/ArticleID/2469/pg/2/2.html)