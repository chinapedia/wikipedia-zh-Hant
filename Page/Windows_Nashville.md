**Windows Nashville**是一个被取消的操作系统的开发代号。此操作系统原定于1996年上市，作为[Windows
95的一个升级版本](../Page/Windows_95.md "wikilink")。对于Windows
95用户来说，Nashville是一个介于Windows
95与下一个主要版本的Windows——Memphis之间的升级\[1\]；对于[Windows
NT用户](../Page/Windows_NT.md "wikilink")，Nashville是一个介于[NT4.0与Cairo之间的版本](../Page/Windows_NT_4.0.md "wikilink")。Cairo的开发週期刚好与Memphis重叠，加上一系列的延迟之后，Cairo最后以[Windows
NT
4.0的名称被推出](../Page/Windows_NT_4.0.md "wikilink")，作为之后的一个NT内核的主要操作系统\[2\]。因为推出时间介于Windows
95与[Windows
97/98之间](../Page/Windows_98.md "wikilink")，Nashville有时也被称为Windows
96\[3\]。

微软曾声称会在Nashville的桌面添加基于[Internet Explorer
3.0浏览器](../Page/Internet_Explorer_3.md "wikilink")（在Nashville之前几个月推出）的Internet整合功能。引人注目的功能包括资源管理器与网页浏览器的结合、以[Internet
Explorer的](../Page/Internet_Explorer.md "wikilink")[ActiveX技术正确地打开微软](../Page/ActiveX.md "wikilink")[Office文件以及直接使用动态网页作为桌面背景从而取代静态图片桌面背景的功能](../Page/Microsoft_Office.md "wikilink")。

在Nashville的计划取消之后，许多被计划在该版本添加的功能改在较后期版本的Internet
Explorer及Windows中出现，特别是许多Internet整合功能，包括资源管理器与网页浏览器的结合，可以在安装[Windows桌面更新后添加到Windows](../Page/Windows桌面更新.md "wikilink")
95以及Windows NT4.0中，而这个升级包已经包含在Internet Explorer
4.0中（开发代号也是Nashville，在1997年推出）。它可以被单独安装，而它也被整合在Windows
95 OSR 2.5（Windows 95 4.00.950 C）与Windows 98中。

## 参见

  - [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")

  -
## 注釋

## 外部链接

  - [The Road to Cairo Goes Through
    Nashville](https://web.archive.org/web/20070227231952/http://www.windowsitpro.com/Windows/Article/ArticleID/2553/2553.html)（June
    1996 preview）
  - [Beyond
    Windows 95](http://www.guidebookgallery.org/articles/beyondwindows95)

[Category:Microsoft
Windows](../Category/Microsoft_Windows.md "wikilink")

1.  Memphis是[Windows
    98的开发代号](../Page/Windows_98.md "wikilink")。此处英文维基之注解误植为Windows
    97，而实际上Windows 97的开发代号是Detroit。Windows 97的功能随后被整合到Windows
    98中，而没有被作为单一版本推出。
2.  此处有严重疑义。其他消息指出：Cairo应为NT4.0的开发代号，而Windows 2000的开发代号是NT5.0。
3.  实际上在Nashville的安装程序中，有一部分确实是显示Windows 96而不是Windows 95。