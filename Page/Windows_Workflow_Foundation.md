**Windows Workflow
Foundation**（WF），是一項微軟技術，用於定義、執行、管理工作流程（workflows）。目前已原生於[Windows
vista作業系統](../Page/Windows_vista.md "wikilink")，並可以安裝在 [Windows XP
sp2](../Page/Windows_XP.md "wikilink") 和 [Windows Server
2003](../Page/Windows_Server_2003.md "wikilink") 作業系統。

## 創作流程

工作流程包括活動。開發人員可以編寫自己的特定域（domain-specific）的活動，然後利用他們在工作流程。WF
也提供了一般用途活動，涵蓋若干控制流結構。

## 書籍

  - *Essential Windows Workflow Foundation*, Dharma Shukla/Bob Schmidt,
    Addison-Wesley Professional, [13
    October](../Page/13_October.md "wikilink") 2006。ISBN 0-321-39983-8
  - *Foundations of WF ISBN 1-59059-718-4*, Brian R. Myers, Apress, [23
    October](../Page/23_October.md "wikilink") 2006。ISBN 1-59059-718-4
  - *Pro WF: Windows Workflow in .NET 3.0*, Bruce Bukovics, Apress, [19
    February](../Page/19_February.md "wikilink") 2007。ISBN 1-59059-778-8
  - *Professional Windows Workflow Foundation ISBN 0-470-05386-0*, Todd
    Kitta, Wrox, [12 March](../Page/12_March.md "wikilink") 2007。ISBN
    0-470-05386-0
  - *Microsoft Windows Workflow Foundation Step by Step*, Kenn Scribner,
    Microsoft Press, [28 February](../Page/28_February.md "wikilink")
    2007。ISBN 0-7356-2335-X

## 外部連結

  - [MSDN Library: Windows Workflow
    Foundation](http://msdn2.microsoft.com/en-us/library/ms735967.aspx)
  - [En blogg om workflow foundation på
    Svenska](http://workflowfoundation.blogspot.com)，瑞典網站

[Workflow Foundation](../Category/Windows_Vista.md "wikilink")