**Windows
XP**（开发代号：）是[微软公司推出供](../Page/微软公司.md "wikilink")[个人电脑使用的](../Page/个人电脑.md "wikilink")[操作系统](../Page/操作系统.md "wikilink")，包括商用及家用的[桌上型电脑](../Page/桌上型电脑.md "wikilink")、[笔记本电脑](../Page/笔记本电脑.md "wikilink")、和[平板电脑等](../Page/平板电脑.md "wikilink")。其[RTM版于](../Page/软件版本周期#RTM.md "wikilink")2001年8月24日发布；零售版于2001年10月25日上市\[1\]。其名字「****」的意思是[英文中的](../Page/英文.md "wikilink")「体验」（）\[2\]。Windows
XP是继[Windows 2000及](../Page/Windows_2000.md "wikilink")[Windows
Me之后的下一代](../Page/Windows_Me.md "wikilink")操作系统，也是微软首个面向消费者且使用Windows
NT架构的操作系统。Windows XP的外部版本是2002，內部版本是5.1（即Windows NT
5.1），正式版的版本号是5.1.2600。

Windows XP OEM及零售版本已经在2008年6月30日停止销售，但用户仍可在购买Windows
Vista旗舰版（Ultimate）或企业版（Enterprise）之后降级到Windows
XP。\[3\]\[4\]

微软最先发行专业版和家庭版两个版本，之后又发行过平板电脑版、嵌入版、客户端版等多個版本及64位元Windows XP操作系统。Windows
XP也有几个只在特定地区销售的版本，如Windows XP Starter Edition等。

基于[NT的操作系统比](../Page/Windows_NT.md "wikilink")[9x系列有更佳的稳定性及效能](../Page/Windows_9x.md "wikilink")。Windows
XP中出现一个新的[图形使用者界面](../Page/图形使用者界面.md "wikilink")，因为微软想提供一个比过去Windows版本[易用性更好的系統](../Page/易用性.md "wikilink")。Windows
XP亦是首个使用[产品启用与](../Page/产品启用.md "wikilink")[盜版竞争的Windows](../Page/盜版.md "wikilink")，然而Windows
XP亦被部分用户批评其安全漏洞、与应用程序（如[Internet Explorer
6及](../Page/Internet_Explorer_6.md "wikilink")[Windows Media
Player](../Page/Windows_Media_Player.md "wikilink")）需紧密结合和预设使用者界面等。在之后的SP2、SP3和[Internet
Explorer 8的版本解決其中部分问题](../Page/Internet_Explorer_8.md "wikilink")。

2011年9月底前，Windows XP是世界上使用人数最多的操作系统，市场占有率达42%；在2007年1月，Windows
XP的市场占有率达历史最高水平，超过76%。\[5\]根据Netmarketshare公司对全球互联网用户的统计数据显示，2012年8月份，统治操作系统市场长达11年之久的Windows
XP最终被Windows 7超越\[6\]。

2013年12月30日，微軟宣布，99天後，也就是2014年4月8日，終止對Windows XP的支援服務，Windows
XP因此正式退役。\[7\]但2014年1月16日，為了防止電腦病毒擴散的危害，微软宣布将会为Windows
XP的用户提供病毒定義檔([Microsoft Security
Essentials](../Page/Microsoft_Security_Essentials.md "wikilink"))更新方面的支持，直到2015年7月14日。\[8\]

## 開發

微軟Windows XP嘗試將[Windows 9x系列和](../Page/Windows_9x.md "wikilink")[Windows
NT系列融合](../Page/Windows_NT.md "wikilink")。Windows XP是[Windows
NT系列作業系統](../Page/Windows_NT.md "wikilink")（Windows NT
5.1），它包含Windows 2000所有相對高效率及安全穩定的性質，但亦有Windows
Me的多媒體功能。然而，它不再支持某些[DOS程序](../Page/DOS.md "wikilink")。\[9\]\[10\]

在Windows XP之前，微軟有兩個相互獨立的作業系統系列，一个是[Windows
9x系列](../Page/Windows_9x.md "wikilink")，包括[Windows
95](../Page/Windows_95.md "wikilink")、[Windows
98](../Page/Windows_98.md "wikilink")、Windows 98 SE以及[Windows
Me](../Page/Windows_Me.md "wikilink")。Windows
9x的系統基層主要程式是16位的DOS源代碼，它是一種16位/32位混合源代碼的准32位作業系統，不穩定而且安全性不高的缺点随着用户的增多暴露出来，但因為當時硬體对此类系统支援較[Windows
NT佳](../Page/Windows_NT.md "wikilink")、再加上微軟將其定位成家用作業系統（價格較低），所以在XP出現之前，為大部分家庭所使用。主要面向桌面電腦的系列。\[11\]还有一个是[Windows
NT系列](../Page/Windows_NT.md "wikilink")，包括Windows NT
3.1/3.5/3.51，[Windows NT
4.0以及](../Page/Windows_NT_4.0.md "wikilink")[Windows
2000](../Page/Windows_2000.md "wikilink")。Windows
NT是純32位作業系統，使用較先進的NT核心技術，相對穩定。分為面向工作站和高級筆記本的Workstation版本（以及後來的Professional版），以及面向伺服器的Server版。\[12\]

## 版本

Windows
XP最主要的兩個版本分別是為家庭用戶設計的XP家庭版，及為商業用戶和高級用戶設計的XP專業版。專業版包含一些進階的功能，這些功能是普遍家庭用戶很少機會用到的。這兩個版本均可在電腦軟件銷售店中買到，亦會預先安裝在大部份電腦生產商銷售的電腦上。直至2008年中，這兩個版本均會繼續銷售。第三個版本名為Windows
XP Media Center Edition（媒体中心版），於2002年引入，並且每年更新直至2006年加入新的數碼媒體、廣播電視和Media
Center
Entender功能。這版本與家用版及專業版不同的地方是這個版本並不能以零售方式購買，只可能通過[OEM途徑或預先安裝在市場上一些稱為](../Page/OEM.md "wikilink")「個人電腦媒體中心」（media
center PCs）的電腦。

再者，微軟還推出兩個64位元的版本，其中一個版本是特別針對以[安騰為基礎的工作站推出](../Page/安騰.md "wikilink")，這個版本於2001年時推出，與家用版及專業版兩個版本的推出時間相近。但是在幾年後，隨著Itanium硬體的硬體販售商因為工作站等級的機器銷售量不高而將其停售後也跟著停止發售。另一個版本，被稱為Windows
XP 64位元專業版，支援Intel IA-32擴充的x86-64處理器。x86-64在AMD被稱為「AMD
64」，主要用在AMD的[Opteron和](../Page/Opteron.md "wikilink")[Athlon
64處理器](../Page/Athlon_64.md "wikilink")，而在Intel被稱為「Intel
64」（過去被稱為IA-32e和EM64T），主要用在Intel的[Pentium
4及之後的處理器上](../Page/Pentium_4.md "wikilink")。
[HP_Tablet_PC_running_Windows_XP_(Tablet_PC_edition)_(2006).jpg](https://zh.wikipedia.org/wiki/File:HP_Tablet_PC_running_Windows_XP_\(Tablet_PC_edition\)_\(2006\).jpg "fig:HP_Tablet_PC_running_Windows_XP_(Tablet_PC_edition)_(2006).jpg")
Windows XP Tablet PC
Edition生產給一種特別設計的手提電腦──[平板电脑](../Page/平板电脑.md "wikilink")（tablet
PC）。這個版本相容輕觸式螢幕，並支持手寫記事及直向屏幕。

另外，微軟也推出[Windows XP
Embedded](../Page/Windows_XP_Embedded.md "wikilink")，這是一款針對消費性電子產品、[機上盒](../Page/機上盒.md "wikilink")、[自動櫃員機](../Page/自動櫃員機.md "wikilink")、醫療器材、遊戲裝置、[銷售點終端機](../Page/銷售時點情報系統.md "wikilink")（point-of-sale
terminals）、[VoIP裝置所推出的版本](../Page/IP電話.md "wikilink")。在2006年7月，微軟又推出[Windows
Fundamentals for Legacy
PCs](../Page/Windows_Fundamentals_for_Legacy_PCs.md "wikilink")，此為精簡版的Windows
XP Embedded，其主要目標是針對那些老舊的機器而設計。而这个版本只有對微軟軟體保證（[Software
Assurance](../Page/Microsoft_Software_Assurance.md "wikilink")）的客戶推出。主要是針對那些想要升級Windows
XP以獲取其安全上及管理能力上的優勢，但又無力負擔購買新機器的公司而推出。

### 特定市场的版本

Windows XP Starter Edition是Windows
XP一个在某些国家可用的低廉版本，包括[泰国](../Page/泰国.md "wikilink")、[土耳其](../Page/土耳其.md "wikilink")、[马来西亚](../Page/马来西亚.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[俄罗斯](../Page/俄罗斯.md "wikilink")、[印度](../Page/印度.md "wikilink")、[哥伦比亚](../Page/哥伦比亚.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[阿根廷](../Page/阿根廷.md "wikilink")、[秘鲁](../Page/秘鲁.md "wikilink")、[玻利维亚](../Page/玻利维亚.md "wikilink")、[智利](../Page/智利.md "wikilink")、[墨西哥](../Page/墨西哥.md "wikilink")、[厄瓜多尔](../Page/厄瓜多尔.md "wikilink")、[乌拉圭和](../Page/乌拉圭.md "wikilink")[委内瑞拉](../Page/委内瑞拉.md "wikilink")。这点类似于Windows
XP家庭版，但Starter
Edition仅限于低端的硬件，一次只能运行3个程序，而且默认去除或者禁止某些功能。每个国家的版本针对该国而定制，包括风景区的桌面背景，对于那些可能不说英语的国家提供[本地化帮助功能](../Page/软件本地化.md "wikilink")，以及为了使得比传统Windows
XP安装更容易而设计的其他默认配置。例如马来西亚的版本就包含吉隆坡天际线的桌面背景，但该版本不设繁体与简体中文版本。\[13\]

在2004年3月，[歐洲委員會罰款微軟](../Page/歐洲委員會.md "wikilink")4.97億歐元並規定該公司提供一款沒有[Windows
Media
Player的Windows版本](../Page/Windows_Media_Player.md "wikilink")。委員會斷定微軟「違反歐盟的競爭法，利用其在個人電腦作業系統市場上近乎[壟斷的市場](../Page/壟斷.md "wikilink")，壟斷工作組[伺服器作業系統和媒體播放器的市場](../Page/伺服器.md "wikilink")。在2004年至2005年的上訴失敗後，微軟與歐盟達成協議，會釋出一個順從法院的版本──Windows
XP Edition N。這個版本沒有內置該公司出產的Windows Media
Player，但改為鼓勵用戶下載及使用他們公司的媒體播放器。微軟希望把這個Windows版本命名為「Reduced
Media Edition」，但歐盟管理者反對，並建議以「Edition N」作為Windows XP的家用版及專業版的命名，「N」代表「not
with Windows Media Player」（不包含Windows Media
Player）\[14\]。事實上，由於這版本的價錢與內置Windows
Media
Player版本的價錢一樣，[戴爾](../Page/戴爾.md "wikilink")、[惠普](../Page/惠普.md "wikilink")、[聯想及](../Page/聯想.md "wikilink")[富士通-西門子電腦公司選擇不進貨該產品](../Page/富士通-西門子電腦公司.md "wikilink")。但是，戴爾亦曾提供此作業系統一段短時間。消費者對此產品的興趣大減，概略地估算，僅1,500套產品出貨給OEM廠商，且沒有任何消費者的消費報告。\[15\]\[16\]\[17\]\[18\]\[19\]在2005年12月，韓國公平交易委員會命令微軟提供不包含[Windows
Media Player或者](../Page/Windows_Media_Player.md "wikilink")[Windows
Messenger的Windows](../Page/Windows_Messenger.md "wikilink") XP及[Windows
Server
2003版本](../Page/Windows_Server_2003.md "wikilink")。\[20\]與歐洲委員會的判決相似，這個判決基於微軟在濫用其佔有優勢的市場，把其公司的其他產品推到消費者。與那個判決不同，但是，微軟也被迫在南韓的市場撤回不符合規定Windows版本。這次判決的結果是微軟在2006年8月釋出「K」及「KN」改變的家用版及專業版。

同年，微软公司还发布针对「subscription-based」和「pay-as-you-go」之定价模式的另外两个版本的Windows
XP家庭版。这些版本被作为微软倡议的一部分而发布，是结合使用的硬件以强制时间限制使用的Windows操作系统。其目标市场是新兴经济体，如[巴西](../Page/巴西.md "wikilink")、[匈牙利和](../Page/匈牙利.md "wikilink")[越南](../Page/越南.md "wikilink")。\[21\]\[22\]

### 語言

Windows
XP可用于很多语言。\[23\]对某些可用的语言，[MUI](../Page/Multilingual_User_Interface.md "wikilink")（多語言使用者界面）包和语言-{界面}-包可以翻译用户界面。\[24\]\[25\]

## 特色簡介

Windows XP引入數個新特色到Windows產品線，包括：

  - 更快的啟動與休眠過程\[26\]
  - 提供驅動程序回復功能以應對由於更新或升級設備驅動程序可能造成的問題。\[27\]
  - 提供更加友好的用戶界面，以及為桌面環境開發主題的架構。
  - [快速切換使用者](../Page/快速切換使用者.md "wikilink")，允許一個使用者儲存當前狀態及已開啟的程式，同時允許另一使用者在不影響該等資訊的情況下登入。
  - [ClearType字體渲染機制](../Page/ClearType.md "wikilink")，用以提高[液晶顯示器上的文字可讀性](../Page/液晶顯示器.md "wikilink")。\[28\]
  - 遠端桌面功能允許使用者通過網絡遠程連接一台運行Windows XP的機器操作應用程序、文件、打印機和設備。\[29\]
  - 支持多數[DSL調制解調器以及](../Page/DSL.md "wikilink")[無線網絡連接](../Page/IEEE_802.11.md "wikilink")，以及通過[火線和](../Page/IEEE_1394.md "wikilink")[藍牙的網絡連接](../Page/藍牙.md "wikilink")。

## 用戶界面

Windows XP擁有一個名为「月神」（Luna）的豪華亮麗的用戶圖形界面，將傳統的「8位元」色系界面帶到「塑膠」色系的圖形界面。

Windows
XP的特色之一是它拥有一個基於任務的新[图形用户界面](../Page/图形用户界面.md "wikilink")。除了開始採用新的視窗標誌外，亦開始使用新式開始功能表\[30\]，搜尋性能亦被重新設計\[31\]，並加入许多視覺效果，包括：

  - 於檔案總管中，有一個半透明藍色選取矩形。\[32\]
  - 在桌面上，圖示標籤带有陰影。
  - 在檔案總管視窗左侧显示常用操作任务和文件（文件夹）信息。
  - 鎖定工作列及其他工具列，以防止意外修改。\[33\]
  - “開始”功能表中，對最近安裝的程式反白顯示。
  - 功能表下的陰影（Windows 2000在游標上有陰影，而在功能表沒有陰影）

Windows
XP會分析視覺效果對效能的影響，並根据分析结果決定是否開啟这些效果，以避免這些新功能消耗過多的處理資源。用戶亦可以更進一步的個人化這些設定。\[34\]某些效果如[Alpha
blending](../Page/Alpha_blending.md "wikilink")（透明及淡出），很多新的顯示卡都能完全處理。但是，如果顯示卡並不支援透明及淡出，效能會大大下降，微軟推薦手動關閉這個功能。\[35\]當[顯存達到](../Page/顯存.md "wikilink")64
[MiB以上時](../Page/MiB.md "wikilink")，以上特效預設為開啟。「Luna」僅表示一個特定的視覺化樣式，並非Windows
XP整體的所有新使用者界面特色。通過某些工具可以破解`uxtheme.dll`文件使Windows XP可以使用第三方主題。

除了內置的Windows XP主題之外，还有一個之前未曾發佈的主題，名為「[Royale
Noir](../Page/Windows_XP主题.md "wikilink")」。它有一個與Windows
Vista十分相似的深藍色[任务栏](../Page/任务栏.md "wikilink")
，以**非官方**的形式下載。\[36\]之后微軟官方又正式發佈一款名為「[Zune](../Page/Windows_XP可视化主题.md "wikilink")」的主題，是上述「Royale
Noir」主題的修改版本，以慶祝Zune可攜式多媒體播放器於2006年11月的發佈。兩個主題的分別僅在視覺上：黑色的任务栏取代深藍色的任务栏，同时橙色的「開始」按鍵取代深藍色的「開始」按鍵。\[37\]此外，內置在媒體中心版本中的Media
Center「Royale」主題，亦提供下載到所有版本的Windows XP中使用。\[38\]

預設桌面背景牆紙[Bliss](../Page/Bliss.md "wikilink")——一張[BMP格式的照片](../Page/BMP.md "wikilink")——是一幅由美國攝影師[查克·歐里爾](../Page/查克·歐里爾.md "wikilink")（Chuck
O'Rear）拍攝的[加州](../Page/加州.md "wikilink")[索诺马县](../Page/索诺马县_\(加利福尼亚州\).md "wikilink")（Sonoma
County）郊外的風景照，当中展现高低起伏的綠山以及飘着[層積雲和](../Page/層積雲.md "wikilink")[卷雲的藍天](../Page/卷雲.md "wikilink")\[39\]。

使用者也可依據喜好使用經典主题界面。

## 配備需求

[DesignedforWinXPlogo.png](https://zh.wikipedia.org/wiki/File:DesignedforWinXPlogo.png "fig:DesignedforWinXPlogo.png")標誌\]\]

<center>

|                                    | 最低配備                                   | 建議配備                                     |
| ---------------------------------- | -------------------------------------- | ---------------------------------------- |
| [中央處理器](../Page/CPU.md "wikilink") | 233[MHz](../Page/MHz.md "wikilink")    | 300MHz或更高                                |
| [-{A](../Page/記憶體.md "wikilink")   | 64[MB](../Page/Mebibyte.md "wikilink") | 128MB                                    |
| [顯卡](../Page/顯卡.md "wikilink")     | 640 x 480                              | Super VGA (800 x 600)或更高分辨率              |
| [硬碟剩餘空間](../Page/硬碟.md "wikilink") | 1.5[GB](../Page/GB.md "wikilink")      | 1.5GB以上                                  |
| 其他設備                               | [CD-ROM](../Page/CD-ROM.md "wikilink") | [CD-ROM以上](../Page/CD-ROM.md "wikilink") |

**Windows XP系統需求**：\[40\]
{{-}}

</center>

备注：

  - 虽然这是微软宣布的最低处理器频率，但是仍然可以在具有早期的[IA-32处理器](../Page/IA-32.md "wikilink")（例如[P5](../Page/P5.md "wikilink")
    [Pentium](../Page/Pentium.md "wikilink")）的计算机上安装并运行Windows
    XP操作系统。\[41\] Windows
    XP与早于[Pentium](../Page/Pentium.md "wikilink")（例如：486）的处理器不兼容，因为它需要CMPXCHG8B指令集。

  - 对于涉及Web浏览的许多操作，例如电子邮件等活动，64MB内存可以提供给用户在同等硬件条件下差不多甚至优于[Windows
    Me的用户体验](../Page/Windows_Me.md "wikilink")。\[42\]

  - 安裝SP2時需要額外1.8
    GB空間\[43\]，安裝SP3時需要額外900[MB空間](../Page/MB.md "wikilink")\[44\]。

Windows XP
32位版只支持到4GB的内存，而且在實際使用時，只能夠使用到3\~3.5GB的内存（因為一些内存容量被電腦其他硬體占用）\[45\]。實際上32位操作系统可以透過[PAE模式規避此上限](../Page/PAE.md "wikilink")，因此擁有4GB以上的内存的電腦使用Windows
XP
32位版還是有辦法不造成浪費，只要使用特殊程序就可以將多餘的内存轉換成虛擬硬盘，只是這可能会出現兼容性問題，故此解決方案是**非官方方案**。

## Service Pack（更新包）

微軟偶爾會為其Windows作業系統發佈[更新包](../Page/更新包.md "wikilink")（Service
Packs）以修正問題和增加特色。每個更新包（及其最新修訂版）都是之前所有更新包和修補程式的合集，所以只需安裝最新的更新包。\[46\]安裝最新版本前亦無需移除舊版本的更新包。

### Service Pack 1

#### Windows XP Service Pack 1

[Windows_XP_-_Program_Access_and_Defaults.png_.png](https://zh.wikipedia.org/wiki/File:Windows_XP_-_Program_Access_and_Defaults.png_.png "fig:Windows_XP_-_Program_Access_and_Defaults.png_.png")Windows
XP
更新包1於2002年9月9日發行，這更新包包含RTM之後的多個安全性[更新](../Page/更新.md "wikilink")、相容性更新、修正程序，選擇性的[.NET
Framework支援](../Page/.NET_Framework.md "wikilink")，支援新科技裝置，例如[平板電腦](../Page/平板電腦.md "wikilink")，附新的[Windows
Messenger](../Page/Windows_Messenger.md "wikilink")
4.7版本。在RTM中没有的Microsoft Java虛擬機器（Microsoft Java Virtual
Machine，MSJVM）也新增在此更新包。最值得注意的新功能是對[USB](../Page/USB.md "wikilink")2.0的支援，以及一個「設定程序存取和預設程序」工具，目標是隱藏各種的中間產物。使用者可以依據習慣控制一個事件預設使用什么程式，例如設定用哪个網頁瀏覽器與即時通訊服務，以及隱藏微軟一些內建程序控制的。這工具首次出現在較舊的作業系統—Windows
2000的SP3中。\[47\]

#### Windows XP Service Pack 1a

在2003年2月3日，微軟釋出更新包1a。因為較早前與[昇陽電腦的訴訟](../Page/昇陽電腦.md "wikilink")，這個更新包移除Microsoft
Java虛擬機器。\[48\]

### Service Pack 2

#### Windows XP Service Pack 2

[Windows_Security_Center_xp.JPG](https://zh.wikipedia.org/wiki/File:Windows_Security_Center_xp.JPG "fig:Windows_Security_Center_xp.JPG")

**更新包2（Service Pack
2）**（簡稱SP2，代號「Springboard」）經過數日耽擱後，在2004年8月25日推出。\[49\]與之前的更新包不同，SP2特別強調改善安全問題，亦為Windows
XP中加入新的功能，包括一個加強的[防火牆](../Page/防火牆.md "wikilink")、改良對[Wi-Fi的支援](../Page/Wi-Fi.md "wikilink")（例如[WPA加密相容性](../Page/WPA.md "wikilink")，和一個精靈設定工具）、在Internet
Explorer
6中加入一個[彈出式廣告攔截功能](../Page/彈出式廣告.md "wikilink")、以及對[藍牙的支援](../Page/藍牙.md "wikilink")。更新包2也引進一個值得注目的核心啟動，可以更快載入作業系統。在核心啟動的時候，新歡迎畫面移除「Professional」及「Home
Edition」字樣，因為微軟在SP2發佈前，引進新的Windows
XP版本。在家用版中，綠色的載入條被在專業版及其他版本中可見的標準藍色取代，使得作業系統的類別彼此相似。

Service Pack 2增加一些安全功能，包括一個防火牆，在Windows下的名稱是[Windows
防火牆](../Page/Windows_防火牆.md "wikilink")，在預設的情況下它是啟動的。另外，利用較新型號處理器的[NX
bit功能](../Page/NX_bit.md "wikilink")，新增一個系統記憶體保護功能（[資料執行防止](../Page/資料執行防止.md "wikilink")，DEP），它可以防止一些[快取溢位攻擊](../Page/快取.md "wikilink")。\[50\]
此外，有關的防護改良還包括[電子郵件和](../Page/電子郵件.md "wikilink")[瀏覽器](../Page/瀏覽器.md "wikilink")。Windows
XP Service Pack
2內建一個[Windows資訊安全中心](../Page/Windows資訊安全中心.md "wikilink")，為使用者提供一個關於系統安全防護的概觀，例如[防毒軟體的狀態](../Page/防毒軟體.md "wikilink")，[Windows
Update和新的Windows防火牆](../Page/Windows_Update.md "wikilink")。第三方的防毒軟體和[防火牆亦可以配合安全中心](../Page/防火牆.md "wikilink")，使用者更易瞭解電腦的安全狀況，並提供改善建議。\[51\]

#### Windows XP Service Pack 2c

在2007年8月10日，微軟為了解決「密鑰荒」而發布一個SP2次要的升級，稱為Service Pack
2c（SP2c）\[52\]。這個更新在增加新密鑰的同時，也降低部分可用密鑰的數量。這個更新只針對Windows
XP Professional(不包含簡體中文、俄文和韓文版)和Windows XP Professional
N作業系統。SP2c已於2007年9月推出\[53\]。此更新包並沒有包含任何重大更新或者是系統修補程式\[54\]。

由於SP2整合許多新功能和重大的更新，媒體有時把Windows XP SP2當作一個不同於Windows XP的新作業系統\[55\]。

### Service Pack 3

Windows XP 更新包3 build
5512已於2008年4月21日向電腦製造廠商發放，並於2008年5月6日經由Microsoft下載中心及Windows
Update對外公開發佈。\[56\]\[57\]\[58\]

這更新包已於2008年7月10日開始經由自動更新向使用者發送。\[59\]微軟亦發佈一個功能群組的概觀，該概觀詳述一些新功能可從獨立的更新包取得及安裝到Windows
XP。\[60\]更新包3總共包含1,174項修正。\[61\]更新包3能夠安裝在使用Internet
Explorer版本6、7或8的系統上。\[62\] Internet Explorer
7和8並無內置在更新包3中。\[63\]

現行發佈的更新包3僅提供給32-bit版本作業系統。64-bit與[Windows Server
2003共用部分核心](../Page/Windows_Server_2003.md "wikilink")，並且它們的更新包也是同時開發和發佈的。\[64\]

#### 新功能

  - 在核心部分，增加微軟核心模式加密模組（Microsoft Kernel Mode Cryptographic
    Module），是一種適用于核心層面的軟體加密方法，系统會利用這項加密功能来控制核心的驅動程序和服務，限制可疑軟體控制系统服務，以免對系统造成破壞。

  - 針對無線網路，新增黑洞[路由器探測](../Page/路由器.md "wikilink")（Black Hole Router
    Detection）功能\[65\]，能够自動偵測發送破壞性數據的路由器並自動進行過濾，避免電腦不小心連線至惡意路由器導致個人資料被盗取。

  - [-{zh-hans:网络访问保护;zh-hant:網路存取保護}-](../Page/網路存取保護技術.md "wikilink")（NAP）用戶端

  - \[66\]

  - 認證[-{zh-hans:安全服务提供程序;zh-hant:安全性服務提供者}-](../Page/安全性服務提供者.md "wikilink")（CredSSD）\[67\]

  - 描述安全性選項在[群組原則](../Page/群組原則.md "wikilink")／本機安全性原則使用者介面中。

  - 在Microsoft核心模式密碼編譯模組的[FIPS
    140-2的更新版本認證](../Page/FIPS_140-2.md "wikilink")。\[68\]

  - 在安裝零售版及OEM版時不用輸入產品金鑰。

整合SP3的Windows
XP零售版及OEM版能夠在不輸入產品金鑰的情況下，安裝及完全的運行30日；過了30日後，用戶將需輸入有效的產品金鑰，並需[啟用產品](../Page/產品啟用.md "wikilink")。大量授權（VLK）版本仍需要在安裝前輸入產品金鑰。\[69\]

雖然SP3中已經累積至目前為止的更新包，但要在現有安裝的Windows XP中安裝SP3仍要求電腦中最少必須已經安裝SP1。\[70\]
不過，整合SP3到「包含任何Service Pack版本或原來的RTM版本的Windows
XP安裝檔」中，並不會有任何錯誤或問題。\[71\]整合SP3到Windows
XP Media Center Edition 2005是不支援的。\[72\]

Service Pack 3包含Windows XP Media Center Edition（MCE）和Windows XP Tablet
PC Edition的作業系統元件，以及.NET Framework 1.0版的安全性更新程式，這些更新程式隨附於Windows XP
[SKU](../Page/最小存貨單位.md "wikilink")。然而，SP3並沒有包括Windows XP MCE
2005中Windows Media Center的更新套件（Update rolls）。\[73\] SP3亦忽略Windows
Media Player 10的安全性更新，雖然這播放機內置在Windows XP MCE 2005。\[74\]

## 支援週期

尚未安裝[服務包的Windows](../Page/服務包.md "wikilink")
XP支援已於2005年12月30日結束。而Windows XP
SP1和SP1a的支援亦於2006年10月10日結束。\[75\]Windows XP
SP2（不包括x64）在2010年7月13號終止服務，幾乎比其原始的使用期多6年，SP3 (x86)及SP2
(x64)延伸支援已於2014年4月8日終止服務。\[76\]在Windows Vista
上市17個月之後，微軟公司於2008年6月30日停止開放Windows
XP的公開許可證給[原始設備製造商與終端作業系統零售商](../Page/原始設備製造商.md "wikilink")。然而，微軟在2008年4月3日向生產[小筆電的原始設備製造商宣布了一項例外直到一年後](../Page/小筆電.md "wikilink")
Windows 7上市（2010年4月3日）。分析家指出此舉主要是為了與基於Linux的小筆電競爭，雖然微軟的Kevin Hutz
表示此決定是因為Windows低階電腦的相近市場。

在2009年4月14日，Windows
XP作業系統的各種版本按照微軟產品技術支援週期原則，由主流支援轉換為延伸支援階段。在延伸支援階段，微軟會繼續提供每月安全性更新給Windows
XP，但是免費的技術支援、保固支援、設計變更等將不再提供。

2013年10月18日，微软中国捐赠Windows
XP于[中国国家博物馆永久收藏](../Page/中国国家博物馆.md "wikilink")，正式作为藏品。\[77\]

[缩略图](https://zh.wikipedia.org/wiki/File:Dialog_box_displaying_Microsoft's_stopping_support_for_XP.PNG "fig:缩略图")

所有的Windows
XP（包括x86及x64）支援，包括安全性更新及安全性相關的hotfixes已于2014年4月8日終止。\[78\]\[79\]

由于[中国大陆还有很多用户仍在使用Windows](../Page/中国大陆.md "wikilink") XP，因此此次Windows
XP支持停止的消息引发诸多热议。2014年3月2日，微软中国宣布，已经采取特别行动，与多家互联网安全及防病毒厂商密切合作，为中国大陆已安装XP电脑的用户继续提供安全保护。\[80\]

2014年5月1日，因为IE的秘狐漏洞，微软破例为Windows
XP提供扩展支持结束后的第一个补丁。\[81\]2017年5月12日，因为[WannaCry病毒的泛滥](../Page/WannaCry.md "wikilink")，微软为Windows
XP提供扩展支持结束后的第二个补丁。\[82\]

## 批評

### 產品啟動及驗證

微軟為了抑制盜版而增添[產品啟動機制](../Page/產品啟動.md "wikilink")，但受到強烈批評的原因是因為它的驗證方式。這種驗證方式使得主機硬體的部份遭受到控管，並在軟體可以永久使用前（每30天一個啟動週期）在微軟的記錄上增加一個唯一的參考[序號](../Page/序號.md "wikilink")（reference
number）。在其它電腦上安裝系統，將因為硬體的不同而無法啟動。如果是在同一台電腦上更換太多硬體（包括新增數個或以上的虛擬光碟機，尤其是**主機板**），則會因為硬體改變太大而導致需要重新啟動。\[83\]\[84\]

### Windows Genuine Advantage

**Windows Genuine
Advantage**（簡稱**WGA**）是一套由[微軟設計的反](../Page/微軟.md "wikilink")[盜版系統](../Page/盜版.md "wikilink")，當使用者存取某些微軟線上服務時，例如[Windows
Update（微軟更新）](../Page/Windows_Update.md "wikilink")、以及從Microsoft
Download
Center（微軟下載中心）下載系統元件，就會強制要求使用者進行驗證[系統是否為正版](../Page/作業系統.md "wikilink")。

### 安全性問題

基於Windows作業系統龐大的市場佔有率，成為電腦病毒創作者的誘人目標，再加上作業系統本身的一些缺陷（安全漏洞），使作業系統容易受到[電腦病毒](../Page/電腦病毒.md "wikilink")、[木馬程式](../Page/木馬程式.md "wikilink")、[蠕蟲](../Page/蠕蟲.md "wikilink")、[惡意軟件的感染](../Page/惡意軟件.md "wikilink")。\[85\]這些經常出現的電腦安全性問題使Windows
XP受到批評。

### 產品壟斷

由於微軟把很多以前是本应由第三方提供的軟體整合到作業系統中，使XP遭受到猛烈的批評。這些軟件包括防火牆、媒體播放器（[Windows Media
Player](../Page/Windows_Media_Player.md "wikilink")），即時通訊軟體（[Windows
Messenger](../Page/Windows_Messenger.md "wikilink")），以及它與Microsoft
Passport網絡服務的緊密結合，這都被很多電腦專家認為會增加安全風險，威脅個人隱私。另外，這些特性的增加被認為是微軟繼續其傳統的[壟斷行為](../Page/壟斷.md "wikilink")。\[86\]

### 用戶介面及性能表現

Windows
XP的新介面及視覺效果會使用較多的系統處理資源，同時會減慢電腦的速度及效能，對於一些硬體規格較低的電腦會有一定的影響。\[87\]\[88\]

### 安裝

在某些情況下，安裝Windows
XP的時候可能需要使用磁碟機。例如安裝XP在一個SATA硬碟時，如果該電腦的BIOS沒有IDE相容模式，電腦有可能辨認不到硬碟。因為XP的安裝光碟中並沒有通用的驅動程式。\[89\]而近年來絕大部份的新電腦都沒有磁碟機，這些電腦將需要使用自定義安裝光碟或者購買一個磁碟機\[90\]
。在32GB或以上的硬碟分割區安裝Windows
XP只能支援[NTFS格式](../Page/NTFS.md "wikilink")，Windows
XP是最後一版支援在[FAT32硬碟分割區上安裝Windows系統的Windows作業系統](../Page/FAT32.md "wikilink")。

## 授權方式

Windows XP有三種主要的授權方式，包括零售版、OEM版以及大量授權版。

### 零售版

Windows XP Professional的香港建議標準零售版價格是港幣2330元，升級版是港幣1550元；\[91\]Windows XP
Home Edition的香港建議完全零售版價格是港幣1550元，升級版是港幣770元。\[92\]Windows XP
Professional的中国建议标准零售版价格是1998元人民币，升级版是1629元人民币；Windows XP Home
Edition的中国建议完全零售版价格是1498元人民币，升级版是798元人民币。完全零售版可以全新安裝，也可以升級安裝；升級版只做升級安裝。這種版本的Windows
XP產品有精美的包裝盒，一本較厚較精美的說明書，一張授權證明以及精美帶有全息影像變化防偽的[光碟](../Page/光碟.md "wikilink")。完全版的光碟標卷有FPP字樣（如英文專業版WXPFPP_EN），升級版的光碟標卷有CCP字樣（如英文專業版WXPCCP_EN）。這種授權的Windows
XP需要激活才可使用\[93\]。若原本安裝的電腦不再使用，授權可移轉至其他電腦使用，但仍需激活。

### OEM版

微軟也通過[OEM授權方式讓Windows](../Page/OEM.md "wikilink")
XP（包括專業版與家庭版）與電腦一起搭售，這種授權的價格較零售版便宜，但規定必須搭配新電腦或電腦零件一起才可獲得，若所搭配的電腦因任何原因停用，授權也不能移至其他電腦續用。這種版本的Windows
XP產品只有一本說明書，一張授權證明以及帶有全息影像變化防偽的光碟。OEM版的Windows
XP只能用來全新安裝，光碟標卷有OEM字樣（如英文專業版WXPOEM_EN）。這種授權的Windows
XP需要激活才可使用。一些品牌的電腦廠商所提供的是Windows XP系統恢復光碟（Recovery
CD），因其電腦廠商在BIOS中加入啟動資訊，也因此該產品只能配備相關產品品牌的電腦才能使用，用戶也無需啟動。\[94\]

### 大量授權版（VOL）

在面向企業時，微軟通過大量啟動（Volume License）的授權方式讓企業以優惠的價格購買大量的Windows XP
Professional。這-{只}-對於企業、政府或教育機構，並且只有專業版有批量許可的授權方式，家庭版沒有。這種版本的Windows
XP產品必需是有相關企業機構與微軟簽訂的軟體大量授權合約，大量許可序號（Volume License
Key，VLK）以及相關的大量授權光碟。大量授權版與完全零售版一樣，可以全新安裝，也可以升級安裝。光碟標卷有VOL字樣（如英文專業版WXPVOL_EN）為了在企業上的效率，故這種授權的Windows
XP無啟動的概念，但也正因如此，大量授權版的序號也成為許多[盜版使用者在使用Windows作業系統時](../Page/盜版.md "wikilink")，可以幾乎不費吹灰之力就可避開微軟盜版驗證的方式之一。\[95\]

## 盗版

Windows XP在中国被大量盗版。由于Windows
XP具有产品激活这一性质，许多盗版提供各种方式帮助用户绕过激活程序或者通过欺骗达到激活的目的，有的大量授权版被泄露到市场，也成为盗版的一大源头。著名的有早期的“俄罗斯破解版”、“上海市政府VOL版”、“[番茄花园版](../Page/番茄花园.md "wikilink")”等，这些盗版集成相关软件，方便用户安装，不过存在[木马等潜在的威胁](../Page/特洛伊木马_\(电脑\).md "wikilink")。尽管如此，这些盗版仍有大量用户。

微软大力打击盗版，曾经通过在安全补丁里包含正版验证程序的方式，使部分盗版用户的屏幕显示黑屏并提示“您是盗版软件的受害者”。此举一度引起争议，而且即使是这个功能本身也旋即被破解。

另外，“番茄花园版XP”的制作人员[洪磊](../Page/洪磊_\(中國罪犯\).md "wikilink")，因犯侵犯著作权罪于2008年被判有期徒刑三年六个月并处罚金人民币1,000,000元。同案的[成都共软网络科技有限公司因犯侵犯著作权罪](../Page/成都共软网络科技有限公司.md "wikilink")，其违法所得计人民币2,924,289.09元予以没收，并判处罚金人民币8,772,861.27元；[孙显忠因侵犯著作权罪](../Page/孙显忠.md "wikilink")，判处有期徒刑三年六个月，并处罚金人民币1,000,000元。[张天平因侵犯著作权罪](../Page/张天平.md "wikilink")，判处有期徒刑二年，并处罚金人民币100,000元；[梁焯勇因侵犯著作权罪](../Page/梁焯勇.md "wikilink")，判处有期徒刑二年，并处罚金人民币100,000元。\[96\]这也是微软首次在中国就盗版提出法律诉讼。根据相关的报道，洪磊已于2011年年底出狱，现重新开放“番茄花园”网站，不过其内容转向对[Android系统的研究](../Page/Android.md "wikilink")。

## 退役以後與現狀

### 微軟官方專供地區延續支援

[愛爾蘭政府以及](../Page/愛爾蘭政府.md "wikilink")[美國海軍等政府及軍方單位](../Page/美國海軍.md "wikilink")，因為仍有為數不少的舊款軟硬體仍需在Windows
XP才可使用而尚未更新，在微軟終止對Windows XP提供支援後，仍編列預算付費購買後繼支援服務\[97\]\[98\]

### 市場佔有率

市場研究機構的數據顯示，已經退役的微軟「Windows
XP」系統2014年8月市佔率仍有23.89%，僅比7月份降低0.93個百分點，比4月份該系統終止技術支援時僅降低2.4個百分點，「[Windows
8](../Page/Windows_8.md "wikilink")/[8.1](../Page/Windows_8.1.md "wikilink")」合計市佔率僅有13.37%，「[Windows
7](../Page/Windows_7.md "wikilink")」市佔率基本與上一個月持平，保持在51.2%。\[99\]

### 使用現狀

Windows
XP的遊樂場網際網路西式拱豬、網際網路西洋棋、網際網路西洋骰子棋、網際網路黑白棋、網際網路傷心小棧等遊戲目前還可以連線與其他玩家遊玩，而且一般應用程式發生錯誤仍然可以回報錯誤傳回給微軟伺服器，這代表微軟官方並沒有完全淘汰Windows
XP系統。目前在台灣的光南大批發以及家樂福等賣場上仍有販售往日低配備需求國產電腦遊戲，這些遊戲可以在Windows
XP作業系統上執行，Windows XP系統可向下相容Windows 95、Windows 98、Windows
me、Windows NT、Windows 2000以及3.5磁片相關軟體，現今一般使用者大多在最新作業系統例如Windows
7、Windows 8、Windows 8.1、Windows 10桌面上安裝虛擬Windows XP執行許多懷舊遊戲軟體。

## 註釋

## 參考資料

## 參见

  - [作業系統列表](../Page/作業系統列表.md "wikilink")
  - [作業系統歷史](../Page/作業系統歷史.md "wikilink")
  - [Microsoft Windows的歷史](../Page/Microsoft_Windows的歷史.md "wikilink")
  - [作業系統](../Page/作業系統.md "wikilink")

## 外部連結

  - [Windows XP
    的支援已終止](https://support.microsoft.com/zh-tw/help/14223/windows-xp-end-of-support)
  - [Windows XP
    終止支援說明網頁](https://www.microsoft.com/zh-tw/WindowsForBusiness/end-of-xp-support)

[Category:Microsoft
Windows](../Category/Microsoft_Windows.md "wikilink") [Category:Windows
NT](../Category/Windows_NT.md "wikilink") [Category:Windows
XP](../Category/Windows_XP.md "wikilink")
[Category:已停止開發的作業系統](../Category/已停止開發的作業系統.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.

12.

13.

14.

15.
16.

17.

18.

19.

20.

21.

22.

23.

24.

25.

26.

27.

28.

29.

30.

31.
32.

33.
34.

35.
36.

37. <http://go.microsoft.com/fwlink/?LinkID=75078>

38.

39.

40.

41.

42.

43.

44.
45.

46.

47.

48.

49.

50.

51.

52.

53.
54.
55.

56.

57.

58.

59.

60.

61.

62.

63.
64.

65.
66.

67.

68.
69.

70.

71.

72.

73.

74.
75.

76.
77.

78. Windows XP SP3與Office
    2003支援將在2014年4月8日終止[英文](http://www.microsoft.com/en-us/windows/enterprise/endofsupport.aspx)
    [繁体中文](http://windows.microsoft.com/zh-TW/windows/end-support-help)

79.

80.

81.

82.

83.

84. [蓋棺定論！Windows
    XP的是非與功過](http://big5.pconline.com.cn/b5/pcedu.pconline.com.cn/softnews/gdpl/0803/1255985_3.html)-太平洋電腦網

85.

86.

87.

88.

89.

90.
91.

92.
93.

94.  關於Windows XP產品啟動的技術性細節

95.
96.

97.

98.

99. [-{zh-cn:互联网档案馆;zh-tw:網際網路檔案館;zh-hk:互聯網檔案館;zh-sg:互联网档案馆;}-](../Page/互联网档案馆.md "wikilink")