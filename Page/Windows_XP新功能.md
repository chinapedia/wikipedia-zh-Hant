[Windows XP有很多在之前Windows版本中找不到的功能](../Page/Windows_XP.md "wikilink")。

## 用戶界面

### 改良的界面

[Windows_XP_SP3_desktop.png](https://zh.wikipedia.org/wiki/File:Windows_XP_SP3_desktop.png "fig:Windows_XP_SP3_desktop.png")\]\]
Windows
XP包括了一組新的[視覺主題](../Page/視覺.md "wikilink")，眾所周知，其代號為「[Luna](../Page/Luna_\(系統主題\).md "wikilink")」。用户还可以从[网络上下载其他个性化的](../Page/网络.md "wikilink")[主题](../Page/主题.md "wikilink")[文件使用](../Page/文件.md "wikilink")。

### 快速切換使用者

快速切換使用者允許另一使用者在之前的使用者不登出及結束其使用中的程式的情況下登入使用系統。\[1\]以前（包括Windows
Me及Windows
2000）在同一時間-{只}-能夠有一位使用者登入（除了通過远程桌面连接），這對於多用戶活動來說是十分嚴重的問題。快速切換使用者與Terminal
Services相似，都比在同一時間僅一名使用者登入需要更多系統資源。另外，雖然多於一名使用者能夠同時保持登入，但一次仍然-{只}-有一名使用者能操作[電腦](../Page/電腦.md "wikilink")。

### 遠端協助

遠端協助允許一個Windows XP用戶臨時地接管一部遠端的Windows
XP電腦經由網路或網際網路來解決問題。\[2\]由於要[系統管理員親身視察受到影響的電腦有可能是一種麻煩](../Page/系統管理員.md "wikilink")，遠端協助允許他們診斷，甚至能在不親身視察的情況下修復電腦問題。

### [CD燒錄](../Page/CD.md "wikilink")

[Windows
XP包括了](../Page/Windows_XP.md "wikilink")[Roxio的技術](../Page/Roxio.md "wikilink")，從而讓用戶經由[Windows
Explorer直接燒錄檔案到一隻](../Page/Windows_Explorer.md "wikilink")[compact
disc](../Page/compact_disc.md "wikilink")。在以前，用戶需要[安裝](../Page/安裝.md "wikilink")[光碟](../Page/光碟.md "wikilink")[燒錄](../Page/燒錄.md "wikilink")[軟體](../Page/軟體.md "wikilink")，例如[Nero
Burning
ROM](../Page/Nero_Burning_ROM.md "wikilink")。現在，[CD及](../Page/CD.md "wikilink")[DVD-RAM燒錄已經被直接地整合到Windows的界面裡](../Page/DVD-RAM.md "wikilink")；用戶燒錄檔案到一隻CD與他們寫入檔案到磁碟或硬碟均採用同樣方式。

### ClearType

[ClearType技術](../Page/ClearType.md "wikilink")；針對[LCD](../Page/LCD.md "wikilink")[液晶](../Page/液晶.md "wikilink")[顯示器設計](../Page/顯示器.md "wikilink")，可提高[文字的清晰度](../Page/文字.md "wikilink")，使[字型更清晰邊緣更平滑](../Page/字型.md "wikilink")。

## 應用軟件的相容性

當[Windows
XP合併了](../Page/Windows_XP.md "wikilink")[家庭版本及](../Page/家庭.md "wikilink")[企業版本的Windows到同一個](../Page/企業.md "wikilink")[作業系統時](../Page/作業系統.md "wikilink")，它混合了[Windows
Me的](../Page/Windows_Me.md "wikilink")[user-friendly介面到](../Page/user-friendly.md "wikilink")[Windows
2000的內核上](../Page/Windows_2000.md "wikilink")。但這造成一個缺點──一些較舊的、設計給之前[Windows版本的](../Page/Windows.md "wikilink")[軟體可能不能運作](../Page/軟體.md "wikilink")。\[3\]不过，Windows
XP在程序属性中提供了对兼容性的设置，在一定程度上允许旧的程序以兼容性模式运行。

## 請參看

  - [微软](../Page/微软.md "wikilink")
  - [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")
  - [Microsoft Windows的歷史](../Page/Microsoft_Windows的歷史.md "wikilink")

## 參考

[Category:Windows XP](../Category/Windows_XP.md "wikilink")

1.  <http://support.microsoft.com/kb/279765/zh-tw>
2.
3.