**WonderSwan**是日本[万代公司发行](../Page/万代.md "wikilink")，[横井军平的Koto](../Page/横井军平.md "wikilink")
Laboratory公司和万代开发的[掌上游戏机](../Page/掌上游戏机.md "wikilink")。这款[第五世代游戏机于](../Page/游戏机历史_\(第五世代\).md "wikilink")1999年推出，WonderSwan有**WonderSwan
Color**和**SwanCrystal**两款后继型号，万代在2003年正式停止支持三款主机。WonderSwan系列主机市场等原因，未在日本外推出。

WonderSwan采用16位元[中央处理器](../Page/中央处理器.md "wikilink")（CPU），和竞争对手——[任天堂的](../Page/任天堂.md "wikilink")[Game
Boy
Color与](../Page/Game_Boy_Color.md "wikilink")[SNK的](../Page/SNK_Playmore.md "wikilink")[Neo
Geo Pocket
Color](../Page/Neo_Geo_Pocket_Color.md "wikilink")——相比，价格低廉且电池使用时间更长。后继型号除延续优势外，还改用彩色显示屏。WonderSwan可以横向或竖向游玩。掌机有一批独占游戏，除改编自第一方动画资产的游戏外，还有[史克威尔](../Page/史克威尔.md "wikilink")、[南梦宫](../Page/南梦宫.md "wikilink")、[太东等知名第三方的游戏](../Page/太东.md "wikilink")。WonderSwan各型号累计销量约350万，在受任天堂[Game
Boy
Advance边缘化前](../Page/Game_Boy_Advance.md "wikilink")，一度占据日本掌机市场份额的8%。虽然WonderSwan在和任天堂的短暂竞争中销量弱势，但回顾评论称赞了掌机的潜力。

## 历史

[万代是山科直治在](../Page/万代.md "wikilink")1950年创立的公司，以玩具车与可塑模型生产起家。公司从1963年掌握《[铁臂阿童木](../Page/铁臂阿童木.md "wikilink")》角色版权起，以热门动画角色授权经营，成为玩具业巨头。万代在1970年代中，生产了电视节目和专用游戏机改编的[LCD游戏](../Page/LCD.md "wikilink")。公司于1982年在日本发行[Intellivision](../Page/Intellivision.md "wikilink")，于1985年成为首批第三方[红白机厂商之一](../Page/红白机.md "wikilink")。但公司在电子游戏界的最大成功，是1996年首发的电子宠物[他媽哥池](../Page/他媽哥池.md "wikilink")\[1\]。然而万代在产品成功后，突然取消了1997年和[世嘉合并的计划](../Page/世嘉.md "wikilink")。万代董事会不到一周就通过反对合并决议，世嘉在决议当晚的紧急董事会见中，也接受了万代的行动。对未获合并支持一事，万代总裁山科诚表示负责\[2\]。于是在万代没有外部支持的情况下，进入电子游戏市场\[3\]。

工程设计师[横井军平在](../Page/横井军平.md "wikilink")[任天堂时](../Page/任天堂.md "wikilink")，设计了著名的掌上游戏机[Game
Boy](../Page/Game_Boy.md "wikilink")。他在[Virtual
Boy失败后](../Page/Virtual_Boy.md "wikilink")，于1996年离开任天堂，自创工程设计公司Koto
Laboratory。万代之后请横井军平制作WondoerSwan，和Game
Boy竞争\[4\]。横井参与了新掌机的开发，但1997年因车祸先于产品推出前离世\[5\]。

1998年10月8日，WonderSwan在东京正式公开\[6\]。WonderSwan其名有凸显美学和技术性能之意：水面上看到的是优雅的天鹅（[Swan](../Page/wikt:swan.md "wikilink")），水面下则脚掌在拼命的打水\[7\]。公司宣称，电池使用时间为30小时，价格低廉，首发游戏约50款\[8\]。

[WonderSwan-WonderWave-PocketStation.jpg](https://zh.wikipedia.org/wiki/File:WonderSwan-WonderWave-PocketStation.jpg "fig:WonderSwan-WonderWave-PocketStation.jpg")（右）上的部分万代游戏\]\]

WonderSwan于1999年3月4日首发\[9\]，外壳颜色有9种：珍珠白、镂空绿、金属银、镂空红、金属蓝、镂空蓝、镂空黑、迷彩、金色。此外还有冰薄荷、冰霜瓜和苏打蓝，三种限量双色版主机。万代在官方网站发起推荐颜色投票，而为金属版和珍珠白版，为给特别版腾出位置，栏位在7月22日取消\[10\]。虽然任天堂在5个月前发行[Game
Boy
Color](../Page/Game_Boy_Color.md "wikilink")，但万代相信黑白屏的WonderSwan表现也会出色：因黑白屏节电、游戏库质量高，Game
Boy最终战胜彩屏的[世嘉Game
Gear和](../Page/Game_Gear.md "wikilink")[雅达利Lynx](../Page/雅达利Lynx.md "wikilink")。WonderSwan零售价4,800日圆也比对手便宜\[11\]。万代曾在2000年和Mattel签定协议，由后者代理WonderSwan的北美销售，但掌机最终未能上市\[12\]。取消发售的确切原因不明，但掌上游戏机市场拥挤或为一原因\[13\]。

万代在翌年公开了WonderSwan Color，该游戏机显示屏为彩色，同时向下兼容WonderSwan\[14\]。WonderSwan
Color于2000年9月在日本发行，外壳颜色有珍珠蓝、珍珠粉、水晶黑、水晶蓝和水晶橙\[15\]\[16\]。主机首发获一般成功，不到一个月间售出27万台\[17\]。任天堂在WonderSwan能发售之前，宣布了硬件配置更优的[Game
Boy
Advance](../Page/Game_Boy_Advance.md "wikilink")。相较于Advance的9,800日圆，WonderSwan
Color的6,800日圆仍更廉价；WonderSwan Color的日本掌机市场占有率虽一度达8%，但自2001年3月Game Boy
Advance上架后，WonderSwan的销量就再未上去\[18\]。

SwanCrystal是WonderSwan
Color的重新设计版，2002年7月12日在日本上市，售价7,800日圆\[19\]\[20\]，比当时的Game
Boy
Advance便宜1,000日圆\[21\]。万代网站再次举办外壳色彩投票，选项为紫罗兰、暗红、水晶蓝恶化水晶黑\[22\]\[23\]。SwanCrystal虽优化LCD屏幕并降价，却仍无竞争力，万代因此宣布，在2003年停产低销量的WonderSwan家族，并彻底退出游戏硬件制造业\[24\]。WonderSwan家族累计销量350万台\[25\]\[26\]，其中WonderSwan销量155万，WonderSwan
Color销量超过110万\[27\]\[28\]。

## 技术规格

[WonderSwan-Color-Back-wBattery.jpg](https://zh.wikipedia.org/wiki/File:WonderSwan-Color-Back-wBattery.jpg "fig:WonderSwan-Color-Back-wBattery.jpg")供电\]\]

WonderSwan的主[CPU为](../Page/中央处理器.md "wikilink")16位的[NEC
V20](../Page/NEC_V20.md "wikilink")。原版WonderSwan显示屏灰度为8位，主要对手Game
Boy的则为4。主机和雅达利Lynx类似，有一组附加按键，可供玩家不同角度游玩；有了这些按键，玩家横向或竖向操作皆可。WonderSwan系列皆由单节AA电池供电，每节电池可为黑白屏原版供电40小时。掌机内有储存器记录存档，玩家无需抄写密码\[29\]。游戏机LCD屏幕为，分辨率为224
× 144\[30\]。游戏机音源为4
[PCM声道](../Page/脈衝編碼調變.md "wikilink")，各声道音频皆32采样、4位元，可调节音量和音高\[31\]。

WonderSwan有专门开发的产品和外设。[WonderWitch是](../Page/WonderWitch.md "wikilink")[Qute开发的官方](../Page/Qute.md "wikilink")[软件开发工具](../Page/软件开发工具.md "wikilink")，面向业余程序员\[32\]。工具包售价11,800日圆，用户可以[C语言开发游戏](../Page/C语言.md "wikilink")。WonderSwan没有耳机接口，故公司推出了耳机－掌机转接器。WonderSwan还有遥控机器人WonderBorg。此外WonderWave连接掌机和[PlayStation
2](../Page/PlayStation_2.md "wikilink")，惟该功能很罕用。WonderSwan及后续机种可用移动电话网上网\[33\]。

WonderSwan Color尺寸略大于WonderSwan，为12.8 × 7.43 × 24.3厘米，其重96克。WonderSwan
Color的CPU为3.072 MHz的NEC V20，[RAM为](../Page/随机存取存储器.md "wikilink")512
kB，由[显示RAM和工作RAM共用](../Page/VRAM.md "wikilink")。游戏机调色板为4,096色，最大同屏发色数为241，每行至多支持28个精灵\[34\]。游戏机LCD屏幕尺寸为\[35\]，亦大于原版WonderSwan\[36\]。WonderSwan
Color完全兼容WonderSwan作品\[37\]。SwanCrystal采用[TFT
LCD显示器](../Page/TFT_LCD.md "wikilink")，比Color的[FSTN显示器响应更快](../Page/覆膜型STN.md "wikilink")，减弱了画面[動態模糊](../Page/動態模糊.md "wikilink")。重新设计的外壳也更耐用。每节电池可使用约15小时\[38\]。

## 游戏库

Koto
Laboratories称WonderSwan游戏卡带共售出1,000万张\[39\]。万代在开发WonderSwan时，获得数间厂商的支持。[萬普](../Page/萬普.md "wikilink")——万代当时部分持有——在动画授权和作品许可上提供支持，[南梦宫和](../Page/南梦宫.md "wikilink")[卡普空为掌机开发了多款作品](../Page/卡普空.md "wikilink")\[40\]。[史克威尔为游戏机重制了](../Page/史克威尔.md "wikilink")《[最终幻想](../Page/最终幻想_\(游戏\).md "wikilink")》、《[最终幻想II](../Page/最终幻想II.md "wikilink")》和《[最终幻想IV](../Page/最终幻想IV.md "wikilink")》，这些游戏后亦登陆Game
Boy
Advance\[41\]\[42\]。[太东移植了](../Page/太东.md "wikilink")[太空侵略者和](../Page/太空侵略者.md "wikilink")[電車GO\!系列](../Page/電車GO!系列.md "wikilink")，这些移植游戏获得好评。万代也发行了自己的作品，包括[数码宝贝和](../Page/数码宝贝.md "wikilink")[GUNDAM系列的独占作品](../Page/GUNDAM.md "wikilink")\[43\]。横井军平为抗衡《[俄罗斯方块](../Page/俄罗斯方块.md "wikilink")》，自己为系统开发了一款益智游戏，游戏最终以自己的名字命名为“Gunpey”\[44\]。其续作《Gunpey
EX》是WonserSwan Color的首发游戏\[45\]。一些游戏由WonderWitch工具制作，其中的《Judgment
Silversword》评价出色\[46\]\[47\]。

第三方开发者对WonderSwan的支持不够热烈。WonderSwan虽由部分知名厂商支持，但多数发行商依然全力支持任天堂掌机。史克威尔放弃WonderSwan，转而为任天堂开发游戏，是为掌机晚期销量下跌的原因之一\[48\]。WonderSwan在2003年停产后，几家开发商将WonderSwan游戏移植到Game
Boy Advance\[49\]。

## 评价

WonderSwan系列售出350万台\[50\]\[51\]，一度占据日本掌机商场份额的8%，但最终被Game Boy
Advance击败\[52\]。任天堂因Game Boy
Advance鲜艳的屏幕和丰富的游戏库，几乎将日本掌机市场垄断，直到索尼2004年发行的[PlayStation
Portable将局面打破](../Page/PlayStation_Portable.md "wikilink")\[53\]。

西方怀旧评论称赞了掌机的成就，但称其只“定位于”特定玩家。杰里米·帕里什认为，WonderSwan极致展现了横井军平的设计哲学。他还将掌机不温不火归罪于万代：“人们最后想起WonderSwan，就是掌机史上的一现昙花，这非系统设计缺陷之过，而是万代莫名软弱的战略，保持低调而恶化时机”
\[54\]。帕里什提出WonderSwan在未上市北美的假设，称“在美国零售店，很难看到Neo Geo
Pocket系统和游戏，很难想象，会有人强烈要求另一小众日本掌机”\[55\]。[Kotaku的卢克](../Page/Kotaku.md "wikilink")·布朗克称赞WonderSwan挑战任天堂，“它尝试了些非常独特有趣的东西，和过往大多掌机经营者相比，他打了一场更坚决的战斗”\[56\]。《[Retro
Gamer](../Page/Retro_Gamer.md "wikilink")》的金·王尔德多处批评掌机，如无耳机和交流电接口、左撇子操作方案差、接耳机适配器时不能多人连线游戏。但王尔德也有所称赞，“万代的WonderSwan经营竞争令人称奇。今天看来依然低廉的售价，使其更值得考虑。”\[57\]

## 参考文献

## 外部链接

  - [官方网站](https://web.archive.org/web/20000816000422/http://www.swan.channel.or.jp/)（已存档）


[Category:萬代遊戲機](../Category/萬代遊戲機.md "wikilink")
[Category:掌上遊戲機](../Category/掌上遊戲機.md "wikilink")
[Category:第五世代遊戲機](../Category/第五世代遊戲機.md "wikilink")

1.

2.

3.

4.
5.

6.

7.

8.
9.
10.
11.
12.
13.
14.
15.
16.

17.

18.
19.

20.

21.

22.
23.

24.
25.
26.
27.
28.
29.

30.
31.

32.

33.
34.
35.
36.

37.
38.
39.
40.
41.
42.
43.
44.
45.
46.
47.
48.
49.
50.
51.

52.
53.
54.
55.
56.
57.