**-{WordPad}-**是一個簡單的[文本编辑器](../Page/文本编辑器.md "wikilink")，在简体中文版称作“**-{写字板}-**”。自從[Windows
95開始](../Page/Windows_95.md "wikilink")，[Microsoft
Windows大部分的版本都內建了這個軟體](../Page/Microsoft_Windows.md "wikilink")。它的功能比[記事本](../Page/Windows記事本.md "wikilink")（一個更基本的文字處理軟體）強，但比[Microsoft
Word弱](../Page/Microsoft_Word.md "wikilink")。WordPad發源自[Windows
1.0x的時候](../Page/Windows_1.0.md "wikilink")，一個名為[Write的軟體](../Page/Windows_Write.md "wikilink")。

WordPad的功能包括處理文字格式，或者將文字列印出來。但它缺少了一些中等程度的功能，例如拼字檢查，[類語辭典](../Page/類語辭典.md "wikilink")，表格的支援等等。就其本身而論，它適合書寫一封信件，或者一個短的文章。它不太適合書寫一個長的報告（因為這類文件通常有大量的圖片），或者一個長的文章，例如[图书或者](../Page/图书.md "wikilink")[手抄本](../Page/手抄本.md "wikilink")。

WordPad原生支援[RTF](../Page/RTF.md "wikilink")，亦利用到微軟的[RichEdit控制](../Page/RichEdit.md "wikilink")。從[Windows
XP](../Page/Windows_XP.md "wikilink") SP1之後的作業系統，版本是4.1，\[1\]包括[Windows
Vista](../Page/Windows_Vista.md "wikilink")。之前的作業系統亦支援“Word for Windows
6.0”格式，與Microsoft Word的檔案格式相容。

Windows 95首次內建了WordPad，用以取代[Windows
Write](../Page/Windows_Write.md "wikilink")。Windows3.1版本和之前的作業系統，都捆了[Windows
Write](../Page/Windows_Write.md "wikilink")。而WordPad的源代碼，微軟已在[MFC类別库版本](../Page/MFC.md "wikilink")3.2後公開，成為一個範例軟體。公開的時候在Windows
95發佈前。現在仍可以在[MSDN的網站下載](../Page/MSDN.md "wikilink")\[<http://msdn2.microsoft.com/en-us/library/51y8h3tk(vs.71>).aspx
Web site\]。

直到現在，WordPad和Microsoft Word仍然是微軟唯一支援讀寫Windows Write建立的WRI檔案的軟體。

Windows XP的WordPad增加了多語言文字編輯。它可以開啟Microsoft
Word（版本6.0-2003）的檔案，雖然仍然有機會出現格式的錯誤。但是，它不可以儲存[.doc格式的檔案](../Page/DOC.md "wikilink")（只支援[.txt或者](../Page/.txt.md "wikilink")[.rtf](../Page/RTF.md "wikilink")），與之前的WordPad版本不同。由於安全問題，Windows
XP Service Pack 2禁止了WordPad對.wri檔案的支援。在Windows
Vista中，亦移除了對Word檔案的支援，原因是格式錯誤顯示的問題還未解決。最後，它新增支援[语音识别](../Page/语音识别.md "wikilink")，以及RichEdit的*[Text
Services
Framework](../Page/Text_Services_Framework.md "wikilink")*（TSF）。對於Word檔案，以至[Open
XML的格式錯誤顯示問題](../Page/Open_XML.md "wikilink")，微軟發佈了免費的Word Viewer。

## 参见

  - [记事本](../Page/记事本.md "wikilink")
  - [Windows Write](../Page/Windows_Write.md "wikilink")

## 參考

[Category:Windows组件](../Category/Windows组件.md "wikilink")
[Category:文書處理器](../Category/文書處理器.md "wikilink")

1.  [RichEdit
    versions](http://blogs.msdn.com/murrays/archive/2006/10/14/richedit-versions.aspx)