《**Words of
Silence**》為[香港歌手](../Page/香港歌手.md "wikilink")[麥浚龍的](../Page/麥浚龍.md "wikilink")[粵語](../Page/粵語.md "wikilink")[音樂專輯](../Page/音樂.md "wikilink")，於2008年1月29日正式發行。

## 專輯簡介

## 曲目

| 次序  | 歌名      | 作曲                                                                    | 填詞                               | 編曲                                 | 監製                                     |
| --- | ------- | --------------------------------------------------------------------- | -------------------------------- | ---------------------------------- | -------------------------------------- |
| 1\. | 人牆      | [Jerald](../Page/Jerald.md "wikilink")                                | [潘源良](../Page/潘源良.md "wikilink") | Jerald's Dad & His Son             | [Jerald](../Page/Jerald.md "wikilink") |
| 2\. | 借火      | [馮穎琪](../Page/馮穎琪.md "wikilink")                                      | [周耀輝](../Page/周耀輝.md "wikilink") | [王雙駿](../Page/王雙駿.md "wikilink")   | [王雙駿](../Page/王雙駿.md "wikilink")       |
| 3\. | 可不可以撐下去 | [張佳添](../Page/張佳添.md "wikilink")@[宇宙大爆炸](../Page/宇宙大爆炸.md "wikilink") | [周耀輝](../Page/周耀輝.md "wikilink") | [王雙駿](../Page/王雙駿.md "wikilink")   | [王雙駿](../Page/王雙駿.md "wikilink")       |
| 4\. | 燈塔      | [鍾達欣](../Page/鍾達欣.md "wikilink")                                      | [周耀輝](../Page/周耀輝.md "wikilink") | [王雙駿](../Page/王雙駿.md "wikilink")   | [王雙駿](../Page/王雙駿.md "wikilink")       |
| 5\. | 酷兒      | [徐繼宗](../Page/徐繼宗.md "wikilink")                                      | [周耀輝](../Page/周耀輝.md "wikilink") | [王雙駿](../Page/王雙駿.md "wikilink")   | [王雙駿](../Page/王雙駿.md "wikilink")       |
| 6\. | 鐵甲人     | [徐繼宗](../Page/徐繼宗.md "wikilink")                                      | [林若寧](../Page/林若寧.md "wikilink") | [at17](../Page/at17.md "wikilink") | [王雙駿](../Page/王雙駿.md "wikilink")       |
| 7\. | 也       | [王雙駿](../Page/王雙駿.md "wikilink")                                      | [周耀輝](../Page/周耀輝.md "wikilink") | [王雙駿](../Page/王雙駿.md "wikilink")   | [王雙駿](../Page/王雙駿.md "wikilink")       |
| 8\. | 吃鯨魚的人   | [馮穎琪](../Page/馮穎琪.md "wikilink")                                      | [黃偉文](../Page/黃偉文.md "wikilink") | [王雙駿](../Page/王雙駿.md "wikilink")   | [王雙駿](../Page/王雙駿.md "wikilink")       |
| 9\. | 舌尖紋了瑪利亞 | [徐繼宗](../Page/徐繼宗.md "wikilink")                                      | [周耀輝](../Page/周耀輝.md "wikilink") | [王雙駿](../Page/王雙駿.md "wikilink")   | [王雙駿](../Page/王雙駿.md "wikilink")       |

## 派台歌曲及成績

  - 1.借火
  - 2.吃鯨魚的人
  - 3.酷兒
  - 4.舌尖紋了瑪利亞

## 相關獎項或提名

[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:麥浚龍音樂專輯](../Category/麥浚龍音樂專輯.md "wikilink")