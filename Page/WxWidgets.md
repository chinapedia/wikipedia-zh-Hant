**wxWidgets**（，原名**wxWindows**\[1\]）是一個[開放原始碼且](../Page/開放原始碼.md "wikilink")[跨平台的](../Page/跨平台.md "wikilink")[物件工具集](../Page/部件工具箱.md "wikilink")（widget
toolkit），其[函式庫可用來建立基本的](../Page/函式庫.md "wikilink")[圖形使用者介面](../Page/圖形使用者介面.md "wikilink")（GUI）。wxWidgets由Julian
Smart於1992年首先開發。

wxWidgets標榜使用其函式庫所開發的[軟體只需要對](../Page/軟體.md "wikilink")[原始碼做少量更改](../Page/原始碼.md "wikilink")（或者完全不用更改），就能在各種不同的作業平台上編譯並執行。目前可支援[Windows](../Page/Windows.md "wikilink")、[Apple
Macintosh](../Page/Apple_Macintosh.md "wikilink")、[Linux](../Page/Linux.md "wikilink")／[Unix](../Page/Unix.md "wikilink")（轉譯成[X11](../Page/X11.md "wikilink")、[GTK+](../Page/GTK+.md "wikilink")、[Motif等函式庫](../Page/Motif.md "wikilink")）、[OpenVMS](../Page/OpenVMS.md "wikilink")、以及[OS/2](../Page/OS/2.md "wikilink")。[嵌入式的版本也正在開發中](../Page/嵌入式系統.md "wikilink")\[2\]。

函式庫本身使用[C++語言開發](../Page/C++.md "wikilink")，但也有其它不同[程式語言的](../Page/程式語言.md "wikilink")[綁紮](../Page/綁紮.md "wikilink")，例如：[Python](../Page/Python.md "wikilink")（wxPython）、[Lua](../Page/Lua.md "wikilink")(wxlua)、[Perl](../Page/Perl.md "wikilink")（wxPerl）、[Ruby](../Page/Ruby.md "wikilink")（wxRuby）、[Smalltalk](../Page/Smalltalk.md "wikilink")（wxSmalltalk）、[Java](../Page/Java.md "wikilink")（wx4j）、甚至是[JavaScript](../Page/JavaScript.md "wikilink")（wxjs）等。

使用wxWidgets開發的軟體不需經過諸如[虛擬機器的技術就能執行](../Page/虛擬機器.md "wikilink")，雖然在不同平台可使用相近甚至相同的原始碼，但其最終轉譯並生成的執行檔是完全基於作業平台的。

另外，wxWidgets不只可以用來建立GUI，它也內建了基於[ODBC的](../Page/ODBC.md "wikilink")[資料庫函式](../Page/資料庫.md "wikilink")、[行程間通訊以及網路socket函式等的支援](../Page/行程間通訊.md "wikilink")。

wxWidgets的授權許可證是經過[開放原始碼促進會認證](../Page/開放原始碼促進會.md "wikilink")，其本質等同於[GNU宽通用公共许可证](../Page/GNU宽通用公共许可证.md "wikilink")（LGPL）。然而一個例外是wxWidgets授權允許修改者以自己的許可證發佈。

## 使用wxWidgets開發的軟體

  - [Amaya網頁編輯工具](../Page/Amaya.md "wikilink")
  - [aMule](../Page/aMule.md "wikilink")
    基於[eMule的跨平台](../Page/eMule.md "wikilink")[P2P軟體](../Page/P2P.md "wikilink")。
  - [Audacity](../Page/Audacity.md "wikilink") 跨平台且開放原始碼的聲音編輯器。
  - [BitTorrent點對點檔案分享peer](../Page/BitTorrent.md "wikilink")-to-peer
    file sharing application
  - [FileZilla](../Page/FileZilla.md "wikilink") -
    跨平台且開放原始碼的[FTP軟體](../Page/FTP.md "wikilink")。
  - [Code::Blocks](../Page/Code::Blocks.md "wikilink") C/C++ IDE
  - [CodeLite](../Page/CodeLite.md "wikilink") C/C++ IDE
  - [ionCube](../Page/ionCube.md "wikilink") PHP編碼器
  - [PTGui](../Page/PTGui.md "wikilink") 照片拼接軟件
  - [RapidSVN](../Page/RapidSVN.md "wikilink") Subversion用戶端
  - [TortoiseCVS](../Page/TortoiseCVS.md "wikilink") CVS用戶端
  - [wxDownload Fast](../Page/wxDownload_Fast.md "wikilink") 下載管理員
  - [wxMaxima](../Page/Maxima.md "wikilink") 電腦代數系統
  - [MadEdit](../Page/MadEdit.md "wikilink") 文本/十六进制编辑器
  - [FreeFileSync](../Page/FreeFileSync.md "wikilink") 文件同步工具
  - [smartCOM](../Page/smartCOM.md "wikilink") 串口调试工具，支持LINUX与WINDOWS
  - [HeeksCAD](../Page/HeeksCAD.md "wikilink") 三维CAD软件
  - [wxMP3gain](../Page/wxMP3gain.md "wikilink") MP3gain的图形界面前端程序

## 參見

  - [Microsoft Foundation Class
    Library](../Page/Microsoft_Foundation_Class_Library.md "wikilink")
  - [Qt](../Page/Qt.md "wikilink")
  - [GTK+](../Page/GTK+.md "wikilink")

## 外部連結

  - [官方網站](http://wxwidgets.org/)
  - [官方教學文件](http://wxwidgets.org/docs/tutorials.htm)
  - [wxForum，非官方討論區](https://web.archive.org/web/20170116151401/http://wxforum.shadonet.com/)
  - [wxSnippets，wxWidgets原始碼片段蒐集](https://web.archive.org/web/20160304233541/http://wxsnippets.com/)
  - [wxCode，wxWidgets擴充元件](http://wxcode.sourceforge.net)
  - [wyoGuide，跨平台教學指南](http://wyoguide.sourceforge.net)
  - [wxWidgets中国爱好者邮件列表（wxChinese）](https://groups.google.com/group/wxchinese)

### 其它程式語言支援

  - [wxBasic](http://wxbasic.sourceforge.net/)
  - [wxCaml](https://web.archive.org/web/20070814040120/http://plus.kaist.ac.kr/~shoh/ocaml/wxcaml/doc/)
  - [wxCL](http://www.wxcl-project.org/)
  - [wxD](http://wxd.sourceforge.net/)
  - [wxEiffel](http://elj.sourceforge.net/projects/gui/ewxw/)
  - [wxErlang](https://web.archive.org/web/20070812010111/http://wxerlang.sourceforge.net/)
  - [wxHaskell](http://wxhaskell.sourceforge.net/)
  - [wx4j（wxWidgets for Java）](http://www.wx4j.org/)
  - [wxJavaScript](http://www.wxjavascript.net)
  - [wx.NET](http://wxnet.sourceforge.net/)
  - [wxLua](http://wxlua.sourceforge.net/)
  - [wxPerl](http://wxperl.sourceforge.net/)
  - [wxPython](http://www.wxpython.org/)
  - [wxRuby](http://webarchive.loc.gov/all/20090306104451/http%3A//wxruby.rubyforge.org/)
  - [wxSqueak](http://www.wxsqueak.org/)

### 整合開發環境（IDE）與快速開發工具（RAD tools）

  - [Boa
    Constructor](http://sourceforge.net/projects/boa-constructor/)，使用wxPython的GUI開發環境。
  - [wxWidgets
    RAD工具比較](http://wiki.codeblocks.org/index.php?title=Comparison_of_wxSmith_features)
  - [wxDesigner](http://www.roebling.de/)，對話盒編輯器、RAD工具。
  - [DialogBlocks](http://www.anthemion.co.uk/dialogblocks/)，產生C++
    程式碼與XRC資源檔的對話盒編輯器。
  - [wxGlade](http://wxglade.sourceforge.net/)，使用wxWidgets的GUI設計工具。
  - [CodeBlocks](http://www.codeblocks.org/)，使用wxWidgets的IDE程式開發工具。
  - [wxDev-C++](http://wxdsgn.sourceforge.net/)，基於[Dev-C++開發環境](../Page/Dev-C++.md "wikilink")，並加入wxWidgets的GUI設計工具。
  - [wxFormBuilder](http://www.wxformbuilder.org)，由C++
    寫成、開放原始碼的wxWidgets GUI設計工具。
  - [wxVS2008Integration](http://priyank.co.in/wxVS2008Integration.php)，[Visual
    Studio .NET](../Page/Visual_Studio_.NET.md "wikilink")
    2008的wxWidgets專案與類別精靈。
  - [PythonCard](http://sourceforge.net/project/showfiles.php?group_id=19015)，跨平台GUI建立工具。

## 參考文獻

<div class="references-small">

<references />

</div>

  - [*Cross-Platform GUI Programming with
    wxWidgets*](http://wxwidgets.org/book/index.htm) -
    第一本關於wxWidgets的書籍。
  - [*Cross-Platform GUI Programming with wxWidgets
    pdf*](http://www.phptr.com/content/images/0131473816/downloads/0131473816_book.pdf)
    - 電子書版本
  - [*wxTutorial, comprehensive guide to
    wxWidgets*](http://ghostdev85.googlepages.com/intro.html) - 教學文件。
  - [Drawing and Printing in C++ with
    wxWidgets](http://www.informit.com/articles/article.asp?p=405047&rl=1)
    - 關於wxWidgets繪圖功能的教學。
  - [wxWidgets-related articles and tutorials](http://wxwidgets.info) -
    教學文件。
  - [Introduction to
    wxWidgets](https://web.archive.org/web/20070928164931/http://priyank.co.in/articles.php?cat_id=1)
    - 給初學者的wxWidgets跨平台（Windows/Linux）開發教學。
  - [wx-sdl](http://code.technoplaza.net/wx-sdl/) -
    wxWidgets與[SDL整合教學](../Page/SDL.md "wikilink")。
  - [The sbVB wxWidgets
    course](https://web.archive.org/web/20071007065226/http://www.sbvb.com.br/cgi-bin/index.cgi?p=3)
    - 教學文件。

[Category:SourceForge专案](../Category/SourceForge专案.md "wikilink")
[Category:计算机编程](../Category/计算机编程.md "wikilink")
[Category:计算机程序库](../Category/计算机程序库.md "wikilink")

1.
2.