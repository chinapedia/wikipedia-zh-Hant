**wz. 2005 Jantar**是波蘭製的[5.56×45
NATO口徑](../Page/5.56×45mm_NATO.md "wikilink")[突擊步槍原型槍](../Page/突擊步槍.md "wikilink")，採用[犢牛式設計佈局](../Page/犢牛式.md "wikilink")，設計基礎來自[Wz.
1996鈹式突擊步槍](../Page/Wz._1996鈹式突擊步槍.md "wikilink")。由波蘭本國的Wojskowa
Akademia科技廠所設計，有來自[AK-47槍族的血統](../Page/AK-47突击步枪.md "wikilink")。

## 外部链接

  - [militaryphotos.net Jantar-M, a Beryl in
    bullpup](https://web.archive.org/web/20080410204533/http://www.militaryphotos.net/forums/showthread.php?t=61598)

[Category:自動步槍](../Category/自動步槍.md "wikilink")
[Category:突擊步槍](../Category/突擊步槍.md "wikilink")
[Category:5.56×45毫米槍械](../Category/5.56×45毫米槍械.md "wikilink")
[Category:波蘭槍械](../Category/波蘭槍械.md "wikilink")
[Category:試驗和研究槍械](../Category/試驗和研究槍械.md "wikilink")