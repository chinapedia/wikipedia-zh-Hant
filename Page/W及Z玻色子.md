在[物理學中](../Page/物理學.md "wikilink")，**W及Z玻色子**（boson）是負責傳遞[弱核力的](../Page/弱核力.md "wikilink")[基本粒子](../Page/基本粒子.md "wikilink")。它們是1983年在[歐洲核子研究組織發現的](../Page/歐洲核子研究組織.md "wikilink")，被認為是[粒子物理](../Page/粒子物理.md "wikilink")[標準模型的一大勝利](../Page/標準模型.md "wikilink")。

**W玻色子**是因弱核力的“弱”（**W**eak）字而命名的。而**Z玻色子**則半幽默地因是“最後一個要發現的粒子”而名。另一個說法是因Z玻色子有零（**Z**ero）[電荷而得名](../Page/電荷.md "wikilink")。

## 基本性質

W玻色子有兩種，分別有
+1（W<sup>+</sup>）和−1（W<sup>−</sup>）單位電荷。W<sup>+</sup>是W<sup>−</sup>的[反粒子](../Page/反粒子.md "wikilink")。而Z玻色子（Z<sup>0</sup>）則為電中性的，且為自身的反粒子。這三種粒子皆十分短命，其[半衰期約為](../Page/半衰期.md "wikilink")\(3 \times 10^{-25}\)秒。

這些[玻色子在各種基本粒子之中屬重型的一類](../Page/玻色子.md "wikilink")。W的[質量為](../Page/質量.md "wikilink")80.399
± 0.023 GeV，而Z則為91.1876 ± 0.0021
GeV。它們差不多是[質子質量的一百倍](../Page/質子.md "wikilink")——比[鐵](../Page/鐵.md "wikilink")[原子還要重](../Page/原子.md "wikilink")。玻色子的質量是十分重要的，因其限制了弱核力的相用範圍。相對地，[電磁力的相用範圍無限遠因為](../Page/電磁力.md "wikilink")[光子無質量](../Page/光子.md "wikilink")。

## 弱相互作用

[Beta_Negative_Decay.svg](https://zh.wikipedia.org/wiki/File:Beta_Negative_Decay.svg "fig:Beta_Negative_Decay.svg")衰变成的[电子和](../Page/电子.md "wikilink")[反電中微子](../Page/反電中微子.md "wikilink")\]\]
W和Z玻色子是传递[弱相互作用的媒介粒子](../Page/弱相互作用.md "wikilink")，就像[光子是传递](../Page/光子.md "wikilink")[电磁相互作用的媒介粒子一樣](../Page/电磁相互作用.md "wikilink")。W玻色子在[核衰變過程中擔任一個重要的角色](../Page/核衰變.md "wikilink")。以[鈷](../Page/鈷.md "wikilink")-60的[β衰變為例](../Page/β衰變.md "wikilink")，

\[{}^{60}_{27}\hbox{Co}\to{}^{60}_{28}\hbox{Ni}+\hbox{e}^-+\overline{\nu}_e\]

此反應在[超新星和](../Page/超新星.md "wikilink")[中子彈爆炸時是非常重要的](../Page/中子彈.md "wikilink")。可是它並不需牽涉到整個[鈷](../Page/鈷.md "wikilink")[核子](../Page/核子.md "wikilink")，而只是它33個[中子其中之一](../Page/中子.md "wikilink")。此[中子在](../Page/中子.md "wikilink")[衰變期間轉變成一個](../Page/衰變.md "wikilink")[質子](../Page/質子.md "wikilink")、[電子](../Page/電子.md "wikilink")（又叫[β粒子](../Page/β粒子.md "wikilink")）和[反電中微子](../Page/反電中微子.md "wikilink")：

  -
    \(\hbox{n}\to \hbox{p}+\hbox{e}^-+\overline{\nu}_e\)

但中子和質子都只是[夸克的組合](../Page/夸克.md "wikilink")（中子是“上下下”，質子是“上上下”）。中子的一粒下夸克在β衰變中受弱相互作用的影响而變成上夸克：

  -
    \(\hbox{d}\to\hbox{u}+\hbox{W}^-\)，

故弱相互作用可改變夸克的“味道”（參閱[費米子](../Page/費米子.md "wikilink")）。而所發出的W<sup>−</sup>粒子迅速衰變成電子和反電中微子：

  -
    \(\hbox{W}^-\to\hbox{e}^-+\overline{\nu}_e\)

因Z玻色子是自己的[反粒子](../Page/反粒子.md "wikilink")，故它的所有[量子數皆為零](../Page/量子數.md "wikilink")。交換Z玻色子是一個[中性流作用](../Page/中性流.md "wikilink")（Neutral
current
interaction），而接收和發出玻色子的粒子除[動量外甚麼也沒變](../Page/動量.md "wikilink")。要觀測中性流作用需要在[粒子加速器和](../Page/粒子加速器.md "wikilink")[粒子偵察器上作很大的投資](../Page/粒子偵察器.md "wikilink")，故目前世界上只有幾所[高能物理實驗室擁有這些儀器](../Page/高能物理.md "wikilink")。

## W和Z玻色子的預測

[Kaon-box-diagram.svg](https://zh.wikipedia.org/wiki/File:Kaon-box-diagram.svg "fig:Kaon-box-diagram.svg")\]\]
於1950年代[量子電動力學的空前成功後](../Page/量子電動力學.md "wikilink")，科學家希望為弱核力建立相似的理論。於1968年，這個論調在統一電磁力和弱核力後達到高潮。提出弱電統一的[谢尔登·格拉肖](../Page/谢尔登·格拉肖.md "wikilink")、[史蒂文·温伯格和](../Page/史蒂文·温伯格.md "wikilink")[阿卜杜勒·萨拉姆因此得到](../Page/阿卜杜勒·萨拉姆.md "wikilink")1979年的[諾貝爾物理學獎](../Page/諾貝爾獎.md "wikilink")\[1\]。他們的[弱電理論不止假設了W玻色子的存在來解釋β衰變](../Page/弱電理論.md "wikilink")，還預測有一種未被發現的Z玻色子。

W和Z玻色子有質量，而光子卻沒有——這是弱電理論發展的一大障礙。這些粒子現時以一個SU(2)
[规范理論來精確描述](../Page/规范理論.md "wikilink")，但理論中玻色子必定無質量。譬如，光子無質量是因為電磁力能以一個U(1)规范理論解釋。某些機制必須破壞SU(2)的對稱來給予W和Z玻色子的質量。其中一個解釋是由[彼得·希格斯於](../Page/彼得·希格斯.md "wikilink")1960年代晚期提出的[希格斯機制](../Page/希格斯機制.md "wikilink")。它預言了一種新粒子——[希格斯玻色子](../Page/希格斯玻色子.md "wikilink")(現今此粒子已被證實存在了)。

SU(2)測量儀理論、電磁力和希格斯機制三者的組合稱為[格拉肖-温伯格-萨拉姆模型](../Page/電弱交互作用.md "wikilink")。它是目前廣泛接受為標準模型的一大支柱。

## W和Z玻色子的發現

W和Z粒子的發現是[歐洲核子研究組織的主要成就之一](../Page/歐洲核子研究組織.md "wikilink")。首先，於1973年，實驗觀察到了弱電理論預測的中性流作用；那時[加尔加梅勒的](../Page/加尔加梅勒.md "wikilink")[氣泡室拍攝到有一些電子突然自行移動的軌跡](../Page/氣泡室.md "wikilink")。這些觀測結果被詮釋為[中微子藉由交換沒有軌跡的Z玻色子與電子互相作用](../Page/中微子.md "wikilink")。由於中微子是偵測不到的，因此實驗中-{只}-能看到電子因著交互作用而造成的動量改變。

W和Z粒子要到能量夠高的[粒子加速器建立後才正式被發現](../Page/粒子加速器.md "wikilink")。第一部這樣的加速器是[超級質子同步加速器](../Page/超級質子同步加速器.md "wikilink")，其中[卡洛·鲁比亚和](../Page/卡洛·鲁比亚.md "wikilink")[西蒙·范德梅尔在](../Page/西蒙·范德梅尔.md "wikilink")1983年一月進行的一連串實驗給出了明顯的W粒子證據。這些實驗稱作“UA1”（由鲁比亚主導）和“UA2”，且為眾多人合作的努力成果。范德梅尔是加速器方面的驅策者（[隨機冷卻](../Page/隨機冷卻.md "wikilink")）。UA1和UA2在幾個月後（1983年五月）找到Z粒子。很快地鲁比亚和范德梅尔因而得到1984年的[諾貝爾物理學獎](../Page/諾貝爾物理學獎.md "wikilink")\[2\]，這可算是保守的諾貝爾獎基金會自成立以來相當不尋常迅速的一次。

## 参考资料

<references />

## 外部連結

  - [粒子物理學回顧（The Review of Particle
    Physics）](http://pdg.lbl.gov/)：粒子性質資訊的首要來源。
  - [CERN的W和Z粒子網頁](https://web.archive.org/web/20050416075202/http://intranet.cern.ch/Chronological/Announcements/CERNAnnouncements/2003/09-16WZSymposium/Courier/HeavyLight/Heavylight.html)

[Category:规范玻色子](../Category/规范玻色子.md "wikilink")
[Category:基本粒子](../Category/基本粒子.md "wikilink")
[Category:电弱理论](../Category/电弱理论.md "wikilink")

1.  [1](http://www.nobel.se/physics/laureates/1979/)
2.  [2](http://www.nobel.se/physics/laureates/1984/)