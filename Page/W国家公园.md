**W国家公园**（[法语](../Page/法语.md "wikilink")：）主要位于[尼日尔境内](../Page/尼日尔.md "wikilink")，在[布基纳法索和](../Page/布基纳法索.md "wikilink")[贝宁也有部分](../Page/贝宁.md "wikilink")。公园因[尼日尔河在这里河道弯曲呈](../Page/尼日尔河.md "wikilink")“W”状而得名。公园的10000平方公里土地基本无人居住。公园于1954年8月4日根据一项法令成立。

公园以公园内数目巨大的[哺乳动物而知名](../Page/哺乳动物.md "wikilink")，包括[土豚](../Page/土豚.md "wikilink")、[狒狒](../Page/狒狒.md "wikilink")、[水牛](../Page/水牛.md "wikilink")、[猞猁](../Page/猞猁.md "wikilink")、[猎豹](../Page/猎豹.md "wikilink")、[大象](../Page/大象.md "wikilink")、[河马](../Page/河马.md "wikilink")、[狮子](../Page/狮子.md "wikilink")、[薮猫和](../Page/薮猫.md "wikilink")[疣猪等](../Page/疣猪.md "wikilink")。

公园于1996年被[联合国教科文组织列入](../Page/联合国教科文组织.md "wikilink")[世界遗产名录](../Page/世界遗产名录.md "wikilink")。

## 气候

这个地区是典型的[热带稀树草原气候](../Page/熱帶乾濕季氣候.md "wikilink")，在夏季有[季风带来的雨水](../Page/季风.md "wikilink")。最冷月（一月）的气温在31.2 °C到10.7 °C之间，最热月（五月）的气温在44 °C到26 °C之间。降雨量较不稳定，一年大约有30—50天下雨，平均500—800毫米。

## 植被

植被的类型主要是苏丹类型的热带稀树草原。到1983年记录到总共454种植物，最近估计大约有500种或更多。公园内有6种主要的生境型，包括[灌木丛](../Page/灌木.md "wikilink")（生长在含有红土和沙的土壤上），稀树草原，[落叶林](../Page/落叶林.md "wikilink")（沿季节性水道分布），半落叶林（在湿润的谷地到干燥的高低的过渡带），[常绿林](../Page/常绿林.md "wikilink")（生长在深的土壤上），尼日尔河[泛滥平原](../Page/泛滥平原.md "wikilink")。[次生林主要在低海拔地区](../Page/次生林.md "wikilink")，大约公园的70%。

## 动物

W国家公园有典型的苏丹类型的热带稀树草原的动物。公园内有尼日尔唯一的[非洲象](../Page/非洲象.md "wikilink")、[非洲水牛和](../Page/非洲水牛.md "wikilink")[肯尼亚水羚](../Page/肯尼亚水羚.md "wikilink")（*Kobus
kob*）种群。有超过70种日行哺乳动物被记述，包括许多[食肉动物](../Page/食肉动物.md "wikilink")，有[斑鬣狗](../Page/斑鬣狗.md "wikilink")（*Crocuta
crocuta*）、[亚洲胡狼](../Page/亚洲胡狼.md "wikilink")（*Canis
aureus*）、[薮猫](../Page/薮猫.md "wikilink")（*Felis
serval*）、[狞猫](../Page/狞猫.md "wikilink")（*F.
caracal*）、[狮](../Page/狮.md "wikilink")、[猎豹](../Page/猎豹.md "wikilink")、[猎狗](../Page/猎狗.md "wikilink")（*Lycaon
pictus*）。还有[狒狒](../Page/狒狒.md "wikilink")（*Papio
anubis*）、[赤猴](../Page/赤猴.md "wikilink")（*Erythrocebus
patas*）。[有蹄类有](../Page/有蹄类.md "wikilink")[疣猪](../Page/疣猪.md "wikilink")（*Phacochoerus
aethiopicus*）、[河马](../Page/河马.md "wikilink")、[薮羚](../Page/薮羚.md "wikilink")（*Tragelaphus
scriptus*）、[红肋小羚羊](../Page/红肋小羚羊.md "wikilink")（*Cephalophus
rufilatus*）、[灰小羚羊](../Page/灰小羚羊.md "wikilink")（*Sylvicapra
grimmia*）、[水羚](../Page/水羚.md "wikilink")、[塞内加尔小苇羚](../Page/塞内加尔小苇羚.md "wikilink")（*Redunca
redunca*）、[马羚](../Page/马羚.md "wikilink")（*Hippotragus
equinus*）、[麋羚](../Page/麋羚.md "wikilink")（*Alcelaphus
buselaphus*）、[南非大羚羊](../Page/南非大羚羊.md "wikilink")、[侏羚](../Page/侏羚.md "wikilink")（*Ourebia
ourebi*）、[红额羚](../Page/红额羚.md "wikilink")（*Gazella
rufifrons*）。大约有150种鸟类被找到。[候鸟在](../Page/候鸟.md "wikilink")2月至5月间来到。[珍珠鸡](../Page/珍珠鸡.md "wikilink")、[大鸨](../Page/大鸨.md "wikilink")、[犀鸟](../Page/犀鸟.md "wikilink")、[凤头鹧鸪](../Page/凤头鹧鸪.md "wikilink")（*Francolinus
sp.*）在公园各处都能找到。[猛禽中的](../Page/猛禽.md "wikilink")[秃鹰](../Page/秃鹰.md "wikilink")、[吼海鵰](../Page/吼海鵰.md "wikilink")（*Haliaeetus
vocifer*）、[猛鵰](../Page/猛鵰.md "wikilink")（*Polemaetus
bellicosus*）、[红脸歌鹰](../Page/红脸歌鹰.md "wikilink")（*Melierax
gabar*）也是常见的。[水禽常见的有](../Page/水禽.md "wikilink")[鸭科](../Page/鸭科.md "wikilink")、[涉禽类](../Page/涉禽.md "wikilink")、[鹮科](../Page/鹮科.md "wikilink")、[鹳科](../Page/鹳科.md "wikilink")、[鹭科](../Page/鹭科.md "wikilink")。陆生爬行动物类包括[苏卡达象龟](../Page/苏卡达象龟.md "wikilink")（*Geochelone
sulcata*）、[尼罗河巨蜥](../Page/尼罗河巨蜥.md "wikilink")（*Varanus
niloticus*）、[非洲蟒](../Page/非洲蟒.md "wikilink")（*Python
sebae*）、[球蟒](../Page/球蟒.md "wikilink")（*Python
regius*）。水中的爬行动物包括[尼罗鳄](../Page/尼罗鳄.md "wikilink")（*Crocodylus
niloticus*）。鱼类主要是生活在尼日尔境内的尼日尔河中的鱼类。

## 参考资料

  - Benoit M (1998) Statut et usage du sol en périphérie du parc
    national du "W" du Niger. Tome 1 : Contribution à l’étude du milieu
    naturel et des ressources végétales du canton de Tamou et du Parc du
    "W". ORSTOM, Niamey, Niger, 41 p.
    [1](http://horizon.documentation.ird.fr/exl-doc/pleins_textes/pleins_textes_6/griseli1/010016709.pdf)
  - Doussa S (2004) Les impacts de la culture cotonnière sur la gestion
    des ressources naturelles du Parc W. Maitrise, Université de
    Ouagadougou.
  - Grégoire JM, Fournier A, Eva H & Sawadogo L (2003) Caractérisation
    de la dynamique des feux et de l’évolution du couvert dans le Parc
    du W: Burkina Faso, Bénin et Niger. 64 S.
    [2](https://web.archive.org/web/20060515160415/http://www-tem.jrc.it/PDF_publis/2003/Gregoire_EUR_ParcW_2003.pdf)
  - Hogan C.Michael (2009) [*Painted Hunting Dog: Lycaon pictus*,
    GlobalTwitcher.com, ed. N.
    Stromberg](https://web.archive.org/web/20101209234758/http://globaltwitcher.auderis.se/artspec_information.asp?thingid=35993)
  - Koster S & Grettenberger J (1983) A preliminary survey of birds in
    Park W Niger. Malimbus, 5: 62-72
  - Poche R (1976) A checklist of National Park W, Niger. Africa Mig.
    Field. 41(3): 113- 115.
  - Poche R (1973) Niger's threatened park 'W'. Oryx 12(2): 216-222 .
  - Rabeil T (2003) Distribution potentielles des grands mammifères dans
    le Parc du W au Niger. Doctoral Thesis, Univ. Paris VII. 463 S.
    [3](http://tel.ccsd.cnrs.fr/docs/00/04/71/22/PDF/tel-00006931.pdf)
  - Price et al. (2003) The “W” Regional Park of Benin, Burkina Faso and
    Niger - Building on a Process of Regional Integration to Address
    both Local Interests and Transboundary Challenges. World Parks
    Congress 2003, Durban, RSA. In: Pansky, Diane (ed.). 2005.
    Governance Stream of the Vth World Parks Congress. Ottawa, Canada:
    Parks Canada and IUCN/WCPA. ISBN: R62-375/2003E-MRC 0-662-40433-5.
    [4](https://web.archive.org/web/20110706175402/http://www.earthlore.ca/clients/WPC/English/grfx/sessions/PDFs/session_1/Price.pdf)
  - W National Park of Niger. 2009.
    [5](http://www.wwfus.org/bsp/publications/africa/108/190/chap2.htm)

## 参见

  - [非洲世界遗产](../Page/非洲世界遗产.md "wikilink")

## 外部链接

  -
  - [Detailed Nigerien government tourist
    map](https://web.archive.org/web/20080420115109/http://www.air-voyages-niger.com/Air%20pages/Niamey-Parc-W-Carte.html).

  - [WCMC World Heritage Site Data
    Sheet](https://archive.is/19970710091410/http://www.wcmc.org.uk/protected_areas/data/wh/'w'-np.htm)

  - [联合国教科文组织上的登录页面](http://whc.unesco.org/pg.cfm?cid=31&id_site=749)

  - [BirdLife IBA Factsheet 'W' National
    Park](http://www.birdlife.org/datazone/sites/index.html?action=SitHTMDetails.asp&sid=6719&m=0).

  - [Le Parc du W du Niger (Niger, Burkina Faso, Bénin): Aires protégées
    Burkina Faso - Niger -
    Bénin](https://web.archive.org/web/20070627095327/http://www.orleans.ird.fr/UR_US/ur136/site/activites/wniger.htm).
    Centre IRD d'Orléans Research Summary, University of Orleans
    (France)

[Category:尼日尔](../Category/尼日尔.md "wikilink")
[W](../Category/尼日世界遺產.md "wikilink")