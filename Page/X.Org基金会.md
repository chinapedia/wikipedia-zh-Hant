[X.Org_Logo.svg](https://zh.wikipedia.org/wiki/File:X.Org_Logo.svg "fig:X.Org_Logo.svg")

**X.Org基金会**是负责[X Window
System开发的社团](../Page/X_Window_System.md "wikilink")，2004年1月22日在[Open
Group的X](../Page/Open_Group.md "wikilink").org网站基础上建立。

Foundation的建立在X的管理上标志着根本性的转变。
从1988年其X的*监护人*（包括过去的X.Org）是厂商组织，而基金会由软件开发者领导，用集市模式的社区开发，依赖外界参与。成员身份向个人开放，而公司做为赞助者参与。

[X.Org
Server是X的](../Page/X.Org_Server.md "wikilink")[参考实现](../Page/参考实现.md "wikilink")，当前版本是X11R7.7，发布于2012年6月6日。由[freedesktop.org提供主机服务](../Page/freedesktop.org.md "wikilink")。

## 外部链接

  -

  -

[fr:X.Org\#Fondation
X.Org](../Page/fr:X.Org#Fondation_X.Org.md "wikilink")

[Category:自由软件](../Category/自由软件.md "wikilink") [Category:X
Window系统](../Category/X_Window系统.md "wikilink")
[Category:基金会](../Category/基金会.md "wikilink")
[Category:自由軟件組織](../Category/自由軟件組織.md "wikilink")