**X**, **x**
（[英文](../Page/英语.md "wikilink")：ex；中文音译：亞克斯）是[拉丁字母中的第](../Page/拉丁字母.md "wikilink")24个[字母](../Page/字母.md "wikilink")。在[北约音标字母中使用](../Page/北约音标字母.md "wikilink")*X-Ray*来表示X。

/ks/音在古代的西部希腊语中用“Χ”（Chi）字母来表示；而在古代的东部希腊语中用“Ξ”（Xi）字母来表示，“Χ”（Chi）字母只表示/x/。

结果，现代[希腊语采用了东方式的写法](../Page/希腊语.md "wikilink")，但西方式的写法却传入了意大利半岛的伊特鲁里亚。因此它在伊特鲁里亚语中表示/ks/，在拉丁语中表示/ks/和/gs/；相反的，源自东希腊语的斯拉夫字母却继承了东希腊语的读法，以致现时东方的[阿塞拜疆语](../Page/阿塞拜疆语.md "wikilink")，[乌兹别克语](../Page/乌兹别克语.md "wikilink")、[鞑靼语](../Page/鞑靼语.md "wikilink")、[维吾尔语都把X来标记](../Page/维吾尔语.md "wikilink")/x/音。不过，有学者表明拉丁字母的X并不等同于希腊字母Χ。葡萄牙语中x表示。

但是对于字母[Ψ (Psi)](../Page/Ψ.md "wikilink")，[Χ (Chi,
Khi)](../Page/Χ.md "wikilink") 和 [Ξ (Xi)](../Page/Ξ.md "wikilink")
是来源于希腊语还是闪族语则仍有争论。

## 字母X的含意

## 字符编码

| 字符编码                               | [ASCII](../Page/ASCII.md "wikilink") | [Unicode](../Page/Unicode.md "wikilink") | [EBCDIC](../Page/EBCDIC.md "wikilink") | [摩斯电码](../Page/摩斯电码.md "wikilink") |
| ---------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- | ---------------------------------- |
| [大写](../Page/大寫字母.md "wikilink") X | 88                                   | 0058                                     | 231                                    | `-··-`                             |
| [小写](../Page/小寫字母.md "wikilink") x | 120                                  | 0078                                     | 167                                    |                                    |

## 其他表示法

## 参看

  - （[希腊字母Chi](../Page/希腊字母.md "wikilink")）

  - （[西里尔字母Cha](../Page/西里尔字母.md "wikilink")）

[Category:拉丁字母](../Category/拉丁字母.md "wikilink")