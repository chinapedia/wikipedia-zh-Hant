**XChat**原本是為Unix-like系統所開發的[IRC通訊協定軟體](../Page/IRC.md "wikilink")。由俄國人（Zed）從1998年開發至2010年8月29日釋放2.8.9後再無活動。XChat使用[GTK+開發](../Page/GTK+.md "wikilink")，是跨平台的應用程式，運行在[Linux](../Page/Linux.md "wikilink")、[FreeBSD](../Page/FreeBSD.md "wikilink")、到[Windows和](../Page/Windows.md "wikilink")[Mac的](../Page/Mac.md "wikilink")[OSX](../Page/OSX.md "wikilink")。作者使用[GNU通用公共许可证授權將軟體](../Page/GNU通用公共许可证.md "wikilink")[原始碼釋出](../Page/原始碼.md "wikilink")，這意謂這X-Chat是[自由軟體的一員](../Page/自由軟體.md "wikilink")。

## 特性

Xchat使用标签页界面，可以同时连接多个服务器，定制性较高。拥有[命令行界面和](../Page/命令行界面.md "wikilink")[图形用户界面两种版本](../Page/图形用户界面.md "wikilink")，主程序代码使用[GNU通用公共许可证授权](../Page/GNU通用公共许可证.md "wikilink")，但Windows的官方版本是[共享软件](../Page/共享软件.md "wikilink")，使用[GTK+生成界面](../Page/GTK+.md "wikilink")。
Xchat具有大多数IRC客户端全部的基本功能，包括[CTCP](../Page/CTCP.md "wikilink"),
[DCC文件传输和聊天](../Page/DCC.md "wikilink")，XChat支持使用其它语言编写的脚本或插件来扩展功能，可用[C语言](../Page/C语言.md "wikilink")、[C++](../Page/C++.md "wikilink")、[Perl](../Page/Perl.md "wikilink")、[Python](../Page/Python.md "wikilink")、[Tcl](../Page/Tcl.md "wikilink")、[Ruby](../Page/Ruby.md "wikilink")\[1\]、[Lua](../Page/Lua.md "wikilink")\[2\]、[CLISP](../Page/CLISP.md "wikilink")、[D语言](../Page/D语言.md "wikilink")、[DMDScript](../Page/DMDScript.md "wikilink")\[3\]等程序设计语言编写。
此外，XChat支援多種語系的轉換，對於程式顯示的樣式也有著詳細的內建設定。

XChat可以在下列[操作系统下运行](../Page/操作系统.md "wikilink"):
GNU/[Linux](../Page/Linux.md "wikilink"),
[FreeBSD](../Page/FreeBSD.md "wikilink"),
[NetBSD](../Page/NetBSD.md "wikilink"),
[OpenBSD](../Page/OpenBSD.md "wikilink"),
[Solaris](../Page/Solaris.md "wikilink"),
[AIX](../Page/IBM_AIX_\(operating_system\).md "wikilink"),
[IRIX](../Page/IRIX.md "wikilink"), [Mac OS
X](../Page/Mac_OS_X.md "wikilink"),
[Windows](../Page/Microsoft_Windows.md "wikilink") 98/ME/NT/2000/XP and
others. 官方已移除对98/ME的支持\[4\]，但通过派生版本或从Windows版源码编译，XChat依旧可以运行在这种平台。

## 授权方式的改变

在2004年8月23日，作者Zed將XChat的Windows可執行版本改為[共享软件並且開始收費](../Page/共享软件.md "wikilink")，有30天的试用期，并将之前的Windows免费版本从网站上移出。作者给出的理由是编制Windows的版本需要大量的时间和精力\[5\]。

但原始碼仍然能夠相當自由的取得，只要使用者有能力，可以自己下載原始碼並加以編譯。從此開始，其他Power-user開始釋出各種不同的X-Chat
Build，其中有名的就是SilverX所建立的版本。這些版本通常幾乎完全依照Zed所寫的原始碼所編，依照編譯者的意願，這些可執行版本是免費取得的。官方的版本和其他使用者所釋出的版本，差異在於內建函式庫以及語言支援的部分。

  - [HexChat](../Page/HexChat.md "wikilink")

## 参考资料

## 外部链接

[XChat官方网站](http://www.xchat.org/)

### 派生版本

  - [近期更新的各Windows版本](http://xchatdata.net/Using/BuildLineup)
  - [XChat
    Aqua](https://web.archive.org/web/20080516051831/http://xchataqua.sourceforge.net/)
    for [Mac OS X](../Page/Mac_OS_X.md "wikilink")

### 官方频道

  - [freenode](../Page/freenode.md "wikilink")：[\#xchat](irc://irc.freenode.net/xchat)
  - [EFnet](../Page/EFnet.md "wikilink")：[\#xchat](irc://irc.efnet.org/xchat)

[Category:自由软件](../Category/自由软件.md "wikilink")
[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:IRC客户端](../Category/IRC客户端.md "wikilink")

1.
2.
3.  [xcdscript home page](http://unborn.ludost.net/xcdscript/)
4.  [XChat Forum - Support for Windows 98/ME
    dropped](http://forum.xchat.org/viewtopic.php?t=3989)
5.  [XChat for Windows](http://www.xchat.org/windows/) – Quote: "Q. Why
    can't XChat for Windows be free? A. \[...\] Building XChat for
    Windows is a difficult process, it requires quite some skill and
    expertise to accomplish. It takes time, and is by no means
    automated. \[...\]"