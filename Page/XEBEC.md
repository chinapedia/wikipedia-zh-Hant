**XEBEC**（日文：****）是[日本一家](../Page/日本.md "wikilink")[動畫製作公司](../Page/動畫.md "wikilink")。

## 概要

公司原由[Production
I.G於](../Page/Production_I.G.md "wikilink")1995年完全持股成立，後來跟Production
I.G、[Mag Garden](../Page/Mag_Garden.md "wikilink")、[WIT
STUDIO](../Page/WIT_STUDIO.md "wikilink")、[SIGNAL.MD等一同納入](../Page/SIGNAL.MD.md "wikilink")集團，成為上市集團一分子。公司早年曾因CG技術先驅拿下不少主要由[小學館企劃的玩具相關動畫](../Page/小學館.md "wikilink")，並於2000年早期有足夠資金設立Triple
A、XEBEC M2及XEBEC
Zwei等分社。至2005年，基於傳統玩具市場萎縮及CG技術普及化，管理層變賣了部分分社，並開始把業務核心轉為無法透過Production
I.G名義推出之萌系及肉番動畫作品，但仍保留少數較大型機器人系作品，以方便規劃集團公司間的稅務事宜。

2018年11月，[日昇動畫收購公司後期製作以外的業務](../Page/日昇動畫.md "wikilink")，包括其過往原創作品之知識產權，並於2019年4月正式將之轉移至新設子公司\[1\]\[2\]\[3\]。Production
I.G同時吸納部分未被收購的作品版權，於4月5日完成所有事宜後，正式對外宣佈解散公司。

## 作品列表

### 電視系列

#### 1990年代

  - [爆走獵人](../Page/爆走獵人.md "wikilink")（1995年）
  - [機動戰艦 Martian Successor NADESICO](../Page/機動戰艦.md "wikilink")（1996年）
  - [-{zh-hans:四驱兄弟;zh-hant:爆走兄弟}-系列](../Page/爆走兄弟.md "wikilink")（1996年）
  - [星方武俠](../Page/星方武俠.md "wikilink")（製作公司：[日昇動畫](../Page/日昇動畫.md "wikilink")，各話製作協力，1999年）
  - [快傑蒸汽偵探團](../Page/快傑蒸汽偵探團.md "wikilink")（1998年）
  - [超速YOYO](../Page/超速YOYO.md "wikilink")（1998年）
  - [亞克傳承2](../Page/亞克傳承系列.md "wikilink")（製作公司：BEE TRAIN，各話製作協力，1999年）
  - [爆球連發！！超級彈珠人](../Page/爆球連發！！超級彈珠人.md "wikilink")（1999年）
  - [地球防禦企業](../Page/地球防禦企業.md "wikilink")（1999年）
  - [淘氣二人組](../Page/淘氣二人組.md "wikilink")（1999年）
  - [∀GUNDAM](../Page/∀GUNDAM.md "wikilink")（製作公司：[日昇動畫](../Page/日昇動畫.md "wikilink")，各話製作協力，1999年）
  - [機獸新世紀ZOIDS](../Page/機獸新世紀ZOIDS.md "wikilink")（1999年）

#### 2000年-2004年

  - [女神候補生](../Page/女神候補生.md "wikilink")（2000年）
  - [純情房東俏房客](../Page/纯情房东俏房客.md "wikilink")（2000年）
  - [暗之末裔](../Page/暗之末裔.md "wikilink")（製作公司：[J.C.STAFF](../Page/J.C.STAFF.md "wikilink")，各話製作協力，2000年）
  - [袖珍女侍小梅](../Page/袖珍女侍小梅.md "wikilink")（製作公司：TNK，各話製作協力，2000年）
  - [機獸新世紀ZERO](../Page/機獸新世紀.md "wikilink")（2001年）
  - [永恆傳說THE ANIMATION](../Page/永恒传说.md "wikilink")（2001年）
  - [通靈王](../Page/通靈王.md "wikilink")（2001年－2002年）
  - [陸上防衛隊](../Page/陸上防衛隊.md "wikilink")（2002年）
  - [洛克人EXE](../Page/洛克人EXE.md "wikilink")（2002年）
  - [尋找滿月](../Page/尋找滿月.md "wikilink")（製作公司：[STUDIO
    DEEN](../Page/STUDIO_DEEN.md "wikilink")，各話製作協力，2002年）
  - [攻殻機動隊 STAND ALONE
    COMPLEX](../Page/攻殻機動隊_STAND_ALONE_COMPLEX.md "wikilink")（製作公司：[Production
    I.G](../Page/Production_I.G.md "wikilink")，各話製作協力，2002年）
  - [D・N・ANGEL](../Page/天使怪盜.md "wikilink")（2003年）
  - [宇宙星路](../Page/宇宙星路.md "wikilink")（2003年）
  - [瓶詰妖精](../Page/瓶詰妖精.md "wikilink")（2003年）
  - [洛克人EXE AXESS](../Page/洛克人EXE_AXESS.md "wikilink")（2003年）
  - [幻龍記](../Page/幻龍記.md "wikilink")（2004年）
  - [蒼穹之戰神](../Page/蒼穹之戰神.md "wikilink")（2004年）
  - [鋼之鍊金術師](../Page/鋼之鍊金術師_\(動畫\).md "wikilink")（製作公司：[BONES](../Page/BONES.md "wikilink")，各話製作協力，2005年）
  - [洛克人EXE Stream](../Page/洛克人EXE_Stream.md "wikilink")（2004年）

#### 2005年-2009年

  - [魔法老师](../Page/魔法老师.md "wikilink")（2005年）
  - [武器種族傳說](../Page/武器種族傳說.md "wikilink")（2005年）
  - [機獸創世紀](../Page/機獸創世紀.md "wikilink")（製作公司：[小學館](../Page/小學館.md "wikilink")，各話製作協力，2005年）
  - [SHUFFLE\!](../Page/SHUFFLE!.md "wikilink")（製作公司：[Asread](../Page/Asread.md "wikilink")，各話製作協力，2005年）
  - [BLOOD+](../Page/BLOOD+.md "wikilink")（製作公司：[Production
    I.G](../Page/Production_I.G.md "wikilink")，各話製作協力，2005年）
  - [洛克人EXE BEAST](../Page/洛克人EXE_BEAST.md "wikilink")（2005年）
  - [蒼穹之戰神－Right of Left](../Page/蒼穹之戰神.md "wikilink")（2005年）
  - [蒼之瞳的少女](../Page/蒼之瞳的少女.md "wikilink")（2006年）
  - [洛克人EXE BEAST+](../Page/洛克人EXE_BEAST+.md "wikilink")（2006年）
  - [流星的洛克人](../Page/洛克人#流星之\(的\)洛克人_系列.md "wikilink")（2006年）
  - [捉猴啦](../Page/捉猴啦.md "wikilink")（[小學館共同製作](../Page/小學館.md "wikilink")，2006年）
  - [武裝鍊金](../Page/武裝鍊金.md "wikilink")（2006年）
  - [鐵馬少年](../Page/鐵馬少年.md "wikilink")（2007年）
  - [英雄時代](../Page/英雄時代.md "wikilink")（2007年）
  - [天元突破
    紅蓮螺巖](../Page/天元突破_紅蓮螺巖.md "wikilink")（製作公司：[GAINAX](../Page/GAINAX.md "wikilink")，各話製作協力，2007年）
  - [暗夜魔法使](../Page/暗夜魔法使.md "wikilink")（製作公司：[HAL FILM
    MAKER](../Page/HAL_FILM_MAKER.md "wikilink")，各話製作協力，2007年）
  - [棋靈少女](../Page/棋靈少女.md "wikilink")（製作公司：[STUDIO
    DEEN](../Page/STUDIO_DEEN.md "wikilink")，各話製作協力，2007年）
  - [流星洛克人2 TRIBE](../Page/流星洛克人.md "wikilink")（2007年）
  - [記憶女神的女兒們](../Page/記憶女神的女兒們.md "wikilink")（2008年）
  - [我的狐仙女友](../Page/我的狐仙女友.md "wikilink")（2008年）
  - [出包王女](../Page/出包王女.md "wikilink")（2008年）
  - [我們這一家](../Page/我們這一家.md "wikilink")（製作公司：シンエイ動画，各話製作協力，2008年）
  - [今天的五年二班](../Page/今天的五年二班.md "wikilink")（2008年）
  - [道子與哈金](../Page/道子與哈金.md "wikilink")（製作公司：[manglobe](../Page/manglobe.md "wikilink")，各話製作協力，2008年）
  - [時空幻境深淵傳奇（Tales of the
    Abyss）](../Page/深淵傳奇.md "wikilink")（製作公司：[日昇動畫](../Page/日昇動畫.md "wikilink")，各話製作協力，2008年）
  - [食靈-零](../Page/食靈.md "wikilink")（製作公司：Aslead ＆ AIC
    Spirits，各話製作協力，2008年）
  - [續 夏目友人帳](../Page/續_夏目友人帳.md "wikilink")（製作公司：[Brain's
    Base](../Page/Brain's_Base.md "wikilink")，各話製作協力，2009年）
  - [南家三姊妹歡迎回來](../Page/南家三姊妹.md "wikilink")（製作公司：[Asread](../Page/Asread.md "wikilink")，各話製作協力，2009年）
  - [潘朵拉之心](../Page/潘朵拉之心.md "wikilink")（2009年）
  - [魯邦三世VS名偵探柯南](../Page/魯邦三世VS名偵探柯南.md "wikilink")（製作公司：東京電影，各話製作協力，2009年）
  - [機巧魔神](../Page/機巧魔神.md "wikilink")（製作公司：[Seven
    Arcs](../Page/Seven_Arcs.md "wikilink")，各話製作協力，2009年）

#### 2010年－2014年

  - [大小姐×执事！](../Page/大小姐×执事！.md "wikilink")（2010年）
  - [戀愛班長](../Page/戀愛班長.md "wikilink")（製作公司：[小學館](../Page/小學館.md "wikilink")
    Music\&Digital Entertainment，各話製作協力，2010年）
  - [HEROMAN](../Page/HEROMAN.md "wikilink")（製作公司：[BONES](../Page/BONES.md "wikilink")，各話製作協力，2010年）
  - [世紀末超自然學院](../Page/世紀末超自然學院.md "wikilink")（製作公司：[A-1
    Pictures](../Page/A-1_Pictures.md "wikilink")，各話製作協力，2010年）
  - [花樣河童](../Page/花樣河童.md "wikilink")（OLM共同製作，2010年）
  - [MM一族](../Page/MM一族.md "wikilink")（2010年）
  - [To LOVE 出包王女 二期](../Page/出包王女.md "wikilink")（2011年）
  - [Rio RainbowGate\!](../Page/Rio_RainbowGate!.md "wikilink")（2011年）
  - [迷糊軟網社](../Page/迷糊軟網社.md "wikilink")（2011年）
  - [變研會](../Page/變研會.md "wikilink")（2011年）
  - [輪迴的拉格朗日](../Page/輪迴的拉格朗日.md "wikilink")（2012年）
  - [襲來！美少女邪神](../Page/襲來！美少女邪神.md "wikilink")（2012年）
  - [輪迴的拉格朗日 season2](../Page/輪迴的拉格朗日.md "wikilink")（2012年）
  - [槍械少女！！](../Page/槍械少女！！.md "wikilink")（2012年）
  - [To LOVE 出包王女（DARKNESS）](../Page/出包王女.md "wikilink")（2012年）
  - [宇宙戰艦大和號2199](../Page/宇宙戰艦大和號2199.md "wikilink")（1～10話、劇场版一至四章与[AIC共同製作](../Page/AIC.md "wikilink")，後由XEBEC獨立製作，2013年）
  - [襲來！美少女邪神W](../Page/襲來！美少女邪神.md "wikilink")（2013年）
  - [神奇寶貝THE ORIGIN](../Page/神奇寶貝系列.md "wikilink")（製作公司：[Production
    I.G](../Page/Production_I.G.md "wikilink")、[OLM](../Page/OLM.md "wikilink")，各話製作協力，2013年）
  - [當不成勇者的我，只好認真找工作了。](../Page/當不成勇者的我，只好認真找工作了。.md "wikilink")（製作公司：[Asread](../Page/Asread.md "wikilink")，各話製作協力，2013年）
  - [至高指令](../Page/至高指令.md "wikilink")（製作公司：Asread，各話製作協力，2013年）
  - [未來卡片 戰鬥夥伴](../Page/未來卡片_戰鬥夥伴.md "wikilink")（OLM共同製作，2014年－2015年）
  - [魔劍姬！通](../Page/魔劍姬！.md "wikilink")（2014年）
  - [白銀的意志 ARGEVOLLEN](../Page/白銀的意志_ARGEVOLLEN.md "wikilink")（2014年）
  - [東京ESP](../Page/東京ESP.md "wikilink")（2014年）

#### 2015年－2019年

  - [絕命制裁X](../Page/絕命制裁X.md "wikilink")（2015年）
  - [To LOVE 出包王女（DARKNESS）2nd](../Page/出包王女.md "wikilink")（2015年）
  - [未來卡片 戰鬥夥伴](../Page/未來卡片_戰鬥夥伴.md "wikilink")（OLM共同製作，2014年）
  - 未來卡片 戰鬥夥伴100（OLM共同製作，2015年－2016年）
  - 未來卡片 戰鬥夥伴DDD（OLM共同製作，2016年－）
  - [競女\!\!\!\!\!\!\!\!](../Page/競女!!!!!!!!.md "wikilink")（2016年10月）
  - [BanG Dream\!](../Page/BanG_Dream!.md "wikilink")（OLM共同製作，2017年）
  - [時鐘機關之星](../Page/時鐘機關之星.md "wikilink")（2017年）
  - [驚爆危機 Invisible Victory](../Page/驚爆危機.md "wikilink")（2018年）
  - [搖曳莊的幽奈小姐](../Page/搖曳莊的幽奈小姐.md "wikilink")（2018年）

### OVA

  - [爆走獵人](../Page/爆走獵人.md "wikilink")（全3卷、1996年－1997年）
  - [碧奇魂2](../Page/碧奇魂.md "wikilink")（第3卷、1996年－1997年）
  - [激鋼牙3](../Page/激鋼牙3.md "wikilink") 熱血大決戰\!\!（1998年）
  - [純情房東俏房客Again](../Page/純情房東俏房客_\(動畫\).md "wikilink")（2002年）
  - [魔法少女朱可奈](../Page/魔法少女朱可奈.md "wikilink")（全3卷、2005年－2006年）
  - [出包王女](../Page/出包王女.md "wikilink")（2009年）
  - [我的狐仙女友](../Page/我的狐仙女友.md "wikilink")（2009年）
  - [變研會](../Page/變研會.md "wikilink")（2010年）
  - [丹特麗安的書架](../Page/丹特麗安的書架.md "wikilink")（製作公司：[GAINAX](../Page/GAINAX.md "wikilink")，製作協力，2012年）
  - [輪迴的拉格朗日－鴨川Days](../Page/輪迴的拉格朗日.md "wikilink")（2013年）
  - [出包王女 DARKNESS](../Page/出包王女.md "wikilink")（2016年）

### 劇場作品

  - [爆走兄弟Let's & Go\!\! WGP
    暴走迷你四驅大追跡\!](../Page/爆走兄弟Let's_&_Go!!_WGP_暴走迷你四驅大追跡!.md "wikilink")（1997年）
  - [機動戰艦 -The prince of darkness-](../Page/機動戰艦.md "wikilink")（1998年）
  - [秋葉原電腦組 2011年的暑假](../Page/秋葉原電腦組.md "wikilink")（1999年）
  - [洛克人劇場版-光與黑暗的遺產](../Page/洛克人exe系列.md "wikilink")（2005年）
  - [棒球大聯盟劇場版 友情的一球](../Page/棒球大聯盟劇場版_友情的一球.md "wikilink")（2008年）
  - [福音戰士新劇場版：破](../Page/福音戰士新劇場版：破.md "wikilink")（製作公司：[Khara](../Page/Khara.md "wikilink")，製作協力，2009年）
  - [破刃之劍劇場版](../Page/破刃之劍.md "wikilink")（[Production
    I.G共同制作](../Page/Production_I.G.md "wikilink")，2010年）
  - [神奇寶貝劇場版：比克提尼與黑英雄 捷克羅姆/白英雄
    雷希拉姆](../Page/神奇寶貝劇場版：比克提尼與黑英雄_捷克羅姆/白英雄_雷希拉姆.md "wikilink")（[Production
    I.G](../Page/Production_I.G.md "wikilink")、OLM 共同制作，2011年）
  - [劇場版 魔法老師！ ANIME
    FINAL](../Page/劇場版_魔法老師！_ANIME_FINAL.md "wikilink")（製作公司：Studio
    Pastoral、[SHAFT](../Page/SHAFT.md "wikilink")，製作協力，2011年）
  - [花樣河童劇場版：開花！蝶之國的大冒險](../Page/花樣河童劇場版：開花！蝶之國的大冒險.md "wikilink")（[OLM](../Page/OLM.md "wikilink")
    共同制作，2013年）

## 相關條目

  - [日本動畫工作室列表](../Page/日本動畫工作室列表.md "wikilink")

## 外部連結

  - [XEBEC公司網站](http://www.xebec-inc.co.jp)

[Category:1995年成立的公司](../Category/1995年成立的公司.md "wikilink")
[\*](../Category/XEBEC.md "wikilink")
[Category:日本動畫工作室](../Category/日本動畫工作室.md "wikilink")
[Category:東京都公司](../Category/東京都公司.md "wikilink")
[Category:國分寺市](../Category/國分寺市.md "wikilink")

1.
2.
3.