[XMMS.png](https://zh.wikipedia.org/wiki/File:XMMS.png "fig:XMMS.png")

**X Multimedia
System**（**XMMS**）是一套自由的音訊播放器，其外表極像[Winamp](../Page/Winamp.md "wikilink")，但是執行在[Unix-like](../Page/Unix-like.md "wikilink")[作業系統](../Page/作業系統.md "wikilink")。

## 歷史

XMMS原來的名字是"X11Amp"，是由Peter and Mikael
Alm在1997年11月所編寫的，原因是「Linux沒有好的mp3播放器」。[1](http://www.slashnet.org/forums/xmms-19990613.html)
這個播放器是特意模仿[Winamp的](../Page/Winamp.md "wikilink")，其第一個版本是在翌年5月發行的。自其發行後，XMMS皆支援Winamp的面板。雖然原來的授權是有版權的，但現已成為開放原始碼，採用[GNU
General Public
License](../Page/GNU_General_Public_License.md "wikilink")。

在1999年6月10日，[4Front
Technologies決定贊助X](../Page/4Front_Technologies.md "wikilink")11Amp開發並將計劃名稱改成"XMMS"[2](http://www.opensound.com/press/1999/xmms.txt)
- 這個名稱解為"X MultiMedia
System"。大部份XMMS使用者將X解為[X11或](../Page/X11.md "wikilink")[X
Window
System](../Page/X_Window_System.md "wikilink")，但實際上X是解為"跨平台"。[3](http://www.slashnet.org/forums/xmms-19990613.html)

### 批評

XMMS一直堅持使用古老的[GTK+函式庫來編寫](../Page/GTK.md "wikilink")，最基本的不升級原因是因為大部份插件皆依賴於舊的GTK環境。很多程式設計師也認為XMMS的原始碼設計得太差，很難維護。基於以上原因，網路上出現了很多相關的改進計劃：

  - [Beep Media Player](../Page/Beep_Media_Player.md "wikilink")，是以[GTK+
    2來重新編寫XMMS的音訊播放器](../Page/GTK2.md "wikilink")，在2003年開始計劃。

<!-- end list -->

  - 而有一個並不著名的以[GTK+
    2重新編寫XMMS的計劃](../Page/GTK2.md "wikilink")，稱為XMMS2，是由Mohammed
    Sameer主持的，。計劃，是打算將XMMS的程式碼全部重新編寫的，由Peter
    Alm主持，在2002冬季開始。[4](https://web.archive.org/web/20051031042955/http://wiki.xmms2.xmms.se/index.php/History#.28Late_2002.29)

## 特色

XMMS現在支援以下的音訊和視訊檔案：

  - [音樂光碟](../Page/音樂光碟.md "wikilink")，包含經由[FreeDB支持](../Page/FreeDB.md "wikilink")[CDDB](../Page/CDDB.md "wikilink")
  - [libmikmod所支援的格式](../Page/libmikmod.md "wikilink")（including .XM,
    .MOD, .IT）
  - MPEG Layer 1,2 and
    3（即是[MP3](../Page/MP3.md "wikilink")），經由[mpg123函式庫支援](../Page/mpg123.md "wikilink")
  - [Vorbis](../Page/Vorbis.md "wikilink")
  - [WAV](../Page/WAV.md "wikilink")
  - [TTA](../Page/TTA.md "wikilink") - 經由第三方插件支援
  - [WavPack](../Page/WavPack.md "wikilink") - 經由第三方插件支援
  - [speex經由第三方插件支援高质及高比率壓縮](../Page/speex.md "wikilink")
  - [FLAC經由FLAC插件支援](../Page/FLAC.md "wikilink")
  - [AAC經由faad](../Page/Advanced_Audio_Coding.md "wikilink")2函式庫插件支援，支援m4a檔案
  - [WMA經由第三方插件提供有限度支援](../Page/WMA.md "wikilink")

參見：<https://web.archive.org/web/20130715120646/http://mcmcc.bat.ru/xmms-wma/>

它支援[Icecast與](../Page/Icecast.md "wikilink")[SHOUTcast串流](../Page/SHOUTcast.md "wikilink")，而且相容[Winamp](../Page/Winamp.md "wikilink")
2的面板。

## XMMS 2發展

  - XMMS2 0.2 DrCox釋出（2006年2月21日）。

## 參考

  - [4Front
    Technologies](../Page/4Front_Technologies.md "wikilink")（2005年8月20日）[關於XMMS支援贊助的新聞稿](http://www.opensound.com/press/1999/xmms.txt)。
  - [SlashNET](../Page/SlashNET.md "wikilink")（2005年8月20日），XMMS開發者們的聊天紀錄，[1999-06-13](http://www.slashnet.org/forums/xmms-19990613.html)。
  - XMMS2（2005年8月20日），[計畫歷史](https://web.archive.org/web/20051031042955/http://wiki.xmms2.xmms.se/index.php/History#.28Late_2002.29)。
  - Planet XMMS,
    [5](https://web.archive.org/web/20060309122226/http://planet.xmms.se/)。

## 外部連結

  - [官方網站](http://www.xmms.org/)
  - [XMMS截圖集](http://www.lynucs.org/?xmms)
  - [ATER XMMS Skins官方网站](http://www.illusions.hu/)

[Category:开放源代码](../Category/开放源代码.md "wikilink")