**XO-1b**是一個環繞著[XO-1的](../Page/XO-1_\(恆星\).md "wikilink")[太陽系外行星](../Page/太陽系外行星.md "wikilink")，他是一個[氣體行星](../Page/氣體行星.md "wikilink")，代表它是由[氫和](../Page/氫.md "wikilink")[氦之類的氣體所組成](../Page/氦.md "wikilink")，另外XO-1b距離他的母恆星很近，表面溫度很高，因此它也被歸類於[熱木星](../Page/熱木星.md "wikilink")。

## 參見

  - [XO-1](../Page/XO-1_\(恆星\).md "wikilink")

[Category:太陽系外行星](../Category/太陽系外行星.md "wikilink")