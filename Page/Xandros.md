**Xandros**系统是[GNU/Linux](../Page/GNU/Linux.md "wikilink")[操作系统的一种发行版本](../Page/操作系统.md "wikilink")，由[Xandros公司发行](../Page/Xandros公司.md "wikilink")，采用的是商业发行模式。Xandros这个名称由Xandros系统采用的[X
Window系统](../Page/X_Window系统.md "wikilink")，与[希腊的](../Page/希腊.md "wikilink")[安德罗斯岛](../Page/安德羅斯島_\(希臘\).md "wikilink")（[Andros](../Page/安德羅斯島_\(希臘\).md "wikilink")）组合而来。\[1\]

[Xandros](https://web.archive.org/web/20080207112729/http://www.xandros.com/)的发行版本主要有：桌面专业版、桌面家庭版以及服务器标准版。\[2\]

[华硕的](../Page/华硕.md "wikilink")[EeePC初期预装的操作系统就是Xandros](../Page/EeePC.md "wikilink")。

## 参考资料

[category:KDE](../Page/category:KDE.md "wikilink")

[Category:2001年紐約州建立](../Category/2001年紐約州建立.md "wikilink")

1.
2.  [Xandros的下载页面](http://www.xandros.com/about/downloads.html?utm_source=xandroshomepage&utm_medium=text_link)