**Xcode**是[蘋果公司向開發人員提供的](../Page/蘋果公司.md "wikilink")[集成開發環境](../Page/集成開發環境.md "wikilink")，用於開發[macOS](../Page/macOS.md "wikilink")、[iOS](../Page/iOS.md "wikilink")、[WatchOS和](../Page/WatchOS.md "wikilink")[tvOS的應用程序](../Page/tvOS.md "wikilink")。

## 概述

Xcode前身是繼承自[NeXT的](../Page/NeXT.md "wikilink")[Project
Builder](../Page/Project_Builder.md "wikilink")。

The Xcode suite包含有[GNU Compiler
Collection自由軟體](../Page/GNU_Compiler_Collection.md "wikilink")（GCC、apple-darwin9-gcc-4.0.1以及apple-darwin9-gcc-4.2.1，默認的是後者），並支援[C語言](../Page/C語言.md "wikilink")、[C++](../Page/C++.md "wikilink")、[Fortran](../Page/Fortran.md "wikilink")、[Objective-C](../Page/Objective-C.md "wikilink")、[Objective-C++](../Page/Objective-C++.md "wikilink")、[Java](../Page/Java.md "wikilink")、[AppleScript](../Page/AppleScript.md "wikilink")、[Python](../Page/Python.md "wikilink")、[Ruby和](../Page/Ruby.md "wikilink")[Swift](../Page/Swift_\(程式語言\).md "wikilink")，還提供[Cocoa](../Page/Cocoa.md "wikilink")、[Carbon以及Java等編程模式](../Page/Carbon_\(API\).md "wikilink")。協力廠商更提供了[GNU
Pascal](../Page/GNU_Pascal.md "wikilink")，\[1\][Free
Pascal](../Page/Free_Pascal.md "wikilink")\[2\],
[Ada](../Page/Ada.md "wikilink")\[3\],
[C\#](../Page/C_Sharp.md "wikilink")\[4\],
[Perl](../Page/Perl.md "wikilink")\[5\],
[Haskell](../Page/Haskell.md "wikilink")\[6\]和[D語言](../Page/D語言.md "wikilink")。Xcode套件使用[GDB作為其後台](../Page/GDB.md "wikilink")[調試工具](../Page/調試工具.md "wikilink")。

從Xcode 3.1開始附帶iOS SDK，作為[iOS的開發環境](../Page/iOS.md "wikilink")。

Xcode 4.0於2011 年 3 月 9
日正式發行。該版本非[Apple開發者註冊會員亦能從](../Page/Apple開發者.md "wikilink")[Mac
App Store中付費下載](../Page/Mac_App_Store.md "wikilink")，收取US$4.99的費用。

從Xcode 4.1開始，[Mac OS X v10.6和](../Page/Mac_OS_X_v10.6.md "wikilink")[Mac
OS X v10.7的使用者可以從](../Page/Mac_OS_X_v10.7.md "wikilink")[Mac App
Store中免費下載](../Page/Mac_App_Store.md "wikilink")。

Xcode最新的版本是Xcode 10（2018 年 WWDC發布），支持[iOS](../Page/iOS.md "wikilink")
12和[WatchOS](../Page/WatchOS.md "wikilink") 3的開發。同樣，可在[Mac App
Store上免費下載](../Page/Mac_App_Store.md "wikilink")。

## 版本歷史

### Xcode 1.0 - Xcode 2.x（支援 iOS 之前）

| 版本歷程                 |
| -------------------- |
| 版本                   |
| <big>**1.0**</big>   |
| <big>**1.1**</big>   |
| <big>**1.2**</big>   |
| <big>**1.5**</big>   |
| <big>**2.0**</big>   |
| <big>**2.1**</big>   |
| <big>**2.2**</big>   |
| <big>**2.2.1**</big> |
| <big>**2.3**</big>   |
| <big>**2.4**</big>   |
| <big>**2.4.1**</big> |
| <big>**2.5**</big>   |
| 版本                   |

### Xcode 3.0 - Xcode 4.x

| 版本歷程                 |
| -------------------- |
| 版本                   |
| <big>**3.0**</big>   |
| <big>**3.1**</big>   |
| <big>**3.1.1**</big> |
| <big>**3.1.2**</big> |
| <big>**3.1.3**</big> |
| <big>**3.1.4**</big> |
| <big>**3.2**</big>   |
| <big>**3.2.1**</big> |
| <big>**3.2.2**</big> |
| <big>**3.2.3**</big> |
| <big>**3.2.4**</big> |
| <big>**3.2.5**</big> |
| <big>**3.2.6**</big> |
| <big>**4.0**</big>   |
| <big>**4.0.1**</big> |
| <big>**4.0.2**</big> |
| <big>**4.1**</big>   |
| <big>**4.1.1**</big> |
| <big>**4.2**</big>   |
| <big>**4.2.1**</big> |
| <big>**4.3**</big>   |
| <big>**4.3.1**</big> |
| <big>**4.3.2**</big> |
| <big>**4.3.3**</big> |
| <big>**4.4**</big>   |
| <big>**4.4.1**</big> |
| <big>**4.5**</big>   |
| <big>**4.5.1**</big> |
| <big>**4.5.2**</big> |
| <big>**4.6**</big>   |
| <big>**4.6.1**</big> |
| <big>**4.6.2**</big> |
| <big>**4.6.3**</big> |
| 版本                   |

### Xcode 5.0 - 6.x (支援 arm64 )

| 版本歷程                 |
| -------------------- |
| 版本                   |
| <big>**5.0**</big>   |
| <big>**5.0.1**</big> |
| <big>**5.0.2**</big> |
| <big>**5.1**</big>   |
| <big>**5.1.1**</big> |
| <big>**6.0.1**</big> |
| <big>**6.1**</big>   |
| <big>**6.1.1**</big> |
| <big>**6.2**</big>   |
| <big>**6.3**</big>   |
| <big>**6.3.1**</big> |
| <big>**6.3.2**</big> |
| <big>**6.4**</big>   |
| 版本                   |

### Xcode 7.0 - 9.x (開放免費部署至實體裝置)

| 版本歷程                    |
| ----------------------- |
| 版本                      |
| <big>**7.0**</big>      |
| <big>**7.0.1**</big>    |
| <big>**7.1**</big>      |
| <big>**7.1.1**</big>    |
| <big>**7.2**</big>      |
| <big>**7.2.1**</big>    |
| <big>**7.3**</big>      |
| <big>**7.3.1**</big>    |
| <big>**8.0**</big>      |
| <big>**8.1**</big>      |
| <big>**8.2**</big>      |
| <big>**8.2.1**</big>    |
| <big>**8.3**</big>      |
| <big>**8.3.1**</big>    |
| <big>**8.3.2**</big>    |
| <big>**8.3.3**</big>    |
| <big>**9.0**</big>      |
| <big>**9.0.1**</big>    |
| <big>**9.1**</big>      |
| <big>**9.2**</big>      |
| <big>**9.3**</big>      |
| <big>**9.4 beta**</big> |
| 版本                      |

|  |                         |  |                          |  |                              |
|  | ----------------------- |  | ------------------------ |  | ---------------------------- |
|  | <abbr title=>已停產</abbr> |  | <abbr title=>目前版本</abbr> |  | <abbr title=>Beta測試版本</abbr> |

## 事故

### XcodeGhost

在2015 年 9 月 18
日，有人發佈消息稱發現由某第三方提供的Xcode（即XcodeGhost）包含惡意後門，會使其編譯的程序被注入相應的後門用於收集相關使用信息，多個中國大陸發佈的軟件被該編譯器感染。\[7\]\[8\]有人推測傳染方式可能是其作者通過多種社交途徑推廣其發佈的第三方Xcode；\[9\]也有人分析通過污染迅雷的離線下載服務使其緩存了染毒版本文件，使只要通過迅雷下載，即使是從官方途徑下載也會因為迅雷的CDN機制而染毒。\[10\]

之後所感染的軟件開發者和其他蘋果應用開發者相繼排查並重新發佈無污染版本。迅雷表示其CDN系統的緩存文件無被污染。\[11\]其事件作者也發表聲明稱此次為一次錯誤的實驗。\[12\]\[13\]

根據[史諾登](../Page/史諾登.md "wikilink")（Edward
Snowden）揭露文件，[美國](../Page/美國.md "wikilink")[中情局](../Page/中情局.md "wikilink")（CIA）在2012
年
已有類似攻擊手法，即可以透過偽冒Xcode，用來監控所有使用該偽冒開發工具所開發的App及後續的修改版本，而這套偽冒開發工具所開發的App，可以在[蘋果公司的官方App](../Page/蘋果公司.md "wikilink")
Store上架並販售，且不會被任何人員發覺有異常之處。\[14\]

## 參考文獻

## 外部連結

  - [Xcode - 新增特性 - Apple
    Developer](https://developer.apple.com/cn/xcode/)

{{-}}

[Category:MacOS 軟體](../Category/MacOS_軟體.md "wikilink") [Category:iOS
開發](../Category/iOS_開發.md "wikilink")
[Category:軟件史](../Category/軟件史.md "wikilink")
[Category:蘋果公司軟體](../Category/蘋果公司軟體.md "wikilink")
[Category:文本編輯器](../Category/文本編輯器.md "wikilink")
[Category:集成開發環境](../Category/集成開發環境.md "wikilink")

1.  [GNU Pascal and Xcode](http://www.microbizz.nl/gpcxcode.html)

2.  [Using Free Pascal with Xcode](http://pascal-central.com/fp-xcode/)

3.  [Tools](http://www.adapower.net/macos/tools.html)

4.  [CSharpPlugin - cocoa-sharp-dev - Google
    Code](http://code.google.com/p/cocoa-sharp-dev/wiki/CSharpPlugin)

5.  [CamelBones, an Objective-C/Perl bridge for Mac OS X & GNUStep -
    Home](http://camelbones.sourceforge.net/index.html)

6.  [Haskell Xcode Plugin](http://hoovy.org/HaskellXcodePlugin/)

7.

8.

9.
10.
11.

12.

13.

14.