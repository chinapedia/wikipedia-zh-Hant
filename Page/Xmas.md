**Xmas**（亦寫作**X-mas**），是[耶誕節英文](../Page/耶誕節.md "wikilink")**Christmas**的意思。

最正統的寫法是Xmas，而X'mas乃東亞地區誤用詞。

Xmas是代寫，並非一般人所認為的缩写。X翻译自[希臘文的](../Page/希臘文.md "wikilink")<span lang="el">ΧΡΙΣΤΟΣ</span>，（译为：*<span lang="el">Χριστoς</span>*或*Christ*，[基督](../Page/基督.md "wikilink")），[新约圣经原文是用希臘文写成的](../Page/新约圣经.md "wikilink")。

## 意思

Xmas的X是擷取自希臘文<span lang="el">Χριστoς</span>的第首字母X，<span lang="el">Χριστoς</span>則是Christos，解作[基督](../Page/基督.md "wikilink")；而mas就是取自mass，意則是[彌撒](../Page/彌撒.md "wikilink")（[天主教會中的聖餐](../Page/天主教會.md "wikilink")）的意思，因此Xmas的意思就是「[基督的](../Page/基督.md "wikilink")[彌撒](../Page/彌撒.md "wikilink")」。

*Xmas*的首字母*X*代表了[希腊字母](../Page/希腊字母.md "wikilink")<span lang="el">[Χ](../Page/Χ.md "wikilink")</span>，这是基督的希腊名字的首字母。

有人認為X是代表基督的十字架，但這是沒有根據的，其實只有[聖安德魯的十字是X型的](../Page/聖安德魯.md "wikilink")，基督的十字是T或†型的。

古代基督教标志<span lang="el">χ</span>和<span lang="el">χρ</span>（Chi
Rho——希腊语基督徒的首两个字母）是基督名字的缩写。在很多新约原稿中，基督（<span lang="el">Χριστoς</span>）缩写为X。牛津英语字典里这种缩略用法可以追溯到1551年，这比英国殖民美洲早50年、比[钦定圣经的完成早](../Page/钦定圣经.md "wikilink")60年。

同时，*Xian*和*Xianity*也是[基督徒和](../Page/基督徒.md "wikilink")[基督教的缩写](../Page/基督教.md "wikilink")，但不及Xmas使用普遍。

## 由來

Xmas一字傳到東亞地區（[台灣](../Page/台灣.md "wikilink")、[日本](../Page/日本.md "wikilink")、[南韓以及](../Page/南韓.md "wikilink")[東南亞地區](../Page/東南亞.md "wikilink")）。

大概是受到現代字母[X字的影響](../Page/X.md "wikilink")，這些地區的許多人誤以為X是Christ的缩写。

因為誤讀為缩写的關係，開始有人習慣性地在X後加上一撇。

跟著大家看習慣了，X'mas這個寫法反而愈來愈普及，東亞地區大街小巷都是這麼寫。

約定俗成下X'mas這個寫法變被人們接受了，很多人到現在也還不知道X'mas中的X原意是英文Christ的缩写。

另一個說法是可能因為某些[字典會標上](../Page/字典.md "wikilink")[重音符號或音節符號](../Page/重音.md "wikilink")，而標誌重音的地方正正就是X之後，字典裏便會寫成X'mas。

東亞地區的人們誤以為字典的寫法是X'mas，以訛傳訛後大家也是這樣寫了。

## 軼事

美國电视动画《[乃出個未來](../Page/乃出個未來.md "wikilink")》中，在31世纪，Xmas是从前叫做Christmas（圣诞节）的那个日子的官方名字。

## 批評

部分[基督教人士批評](../Page/基督教.md "wikilink")，使用Xmas用意實際是省去耶穌基督的名稱，以淡化[聖誕節原本的宗教意義](../Page/聖誕節.md "wikilink")，涉及[政治正確的用語](../Page/政治正確.md "wikilink")。但與其他英文的節日問候用語，如節日快樂（Happy
Holidays）及季節問侯（Seasons' greetings）相比，Xmas包括了聖誕節的意思。

## 参考书目

## 外部連結

[Category:圣诞节](../Category/圣诞节.md "wikilink")