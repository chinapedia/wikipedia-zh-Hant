[XASEdges.svg](https://zh.wikipedia.org/wiki/File:XASEdges.svg "fig:XASEdges.svg")
[XASFig.jpg](https://zh.wikipedia.org/wiki/File:XASFig.jpg "fig:XASFig.jpg")

**X光吸收光譜**（，缩写：XAS）是目前廣泛應用於取得[氣態](../Page/氣態.md "wikilink")、[分子](../Page/分子.md "wikilink")、及[凝體](../Page/凝體.md "wikilink")（例如，[固體](../Page/固體.md "wikilink")）中，目標原子之區域（原子尺度）結構資訊及電子狀態的一種技術。

## 光源

[X光](../Page/X光.md "wikilink")[吸收光譜可藉由調變X光](../Page/吸收光譜.md "wikilink")[光子能量](../Page/光子.md "wikilink")，於目標原子束縛[電子之](../Page/電子.md "wikilink")[激發能量範圍內進行掃瞄而得](../Page/激發.md "wikilink")。通常需使用[同步輻射設施以提供高強度並可調變波長之X光光束](../Page/同步輻射.md "wikilink")。

## 光譜區域

與[X光](../Page/X光.md "wikilink")[吸收光譜相關的技術](../Page/吸收光譜.md "wikilink")，有稱之為[X光吸收細微結構](../Page/X光吸收細微結構.md "wikilink")（X-ray
Absorption Fine Structure,
XAFS），或[延伸X光吸收細微結構](../Page/延伸X光吸收細微結構.md "wikilink")（Extended
X-ray Absorption Fine Structure,
EXAFS）。另外，X光吸數光譜區段在接近目標原子之殼層電子激發處，目標原子之殼層電子吸收光子，會有一陡直的上升，常稱之為[X光吸收近邊緣結構](../Page/X光吸收近邊緣結構.md "wikilink")（X-ray
Absorption Near-Edge
Structures，XANES）或[X光吸收近邊緣結構](../Page/X光吸收近邊緣結構.md "wikilink")（Near-Edge
X-ray Absorption Fine Structure，NEXAFS）。

## 應用及樣品型態

由於X光穿透能力強，光譜各種目標原子的吸收區域重疊性也不高，在分子、[材料科學](../Page/材料科學.md "wikilink")、[生物學](../Page/生物學.md "wikilink")、[化學](../Page/化學.md "wikilink")、[地球科學](../Page/地球科學.md "wikilink")、[環境科學等各領域都有相當廣泛的應用](../Page/環境科學.md "wikilink")。其受測樣品可以是[粉末](../Page/粉末.md "wikilink")、[液體和](../Page/液體.md "wikilink")[氣態樣品](../Page/氣態.md "wikilink")。依實驗方法及實驗站設計，可進行高、低壓、高、低溫、原位（*in-situ*）實驗。

## 實驗方法概述

### 穿透法

最常用且容易的方法，以[台灣](../Page/台灣.md "wikilink")[NSRRC同步輻射的X光吸收光譜實驗站為例](../Page/NSRRC.md "wikilink")，X光光源經單光器選擇光源能量後，引導至實驗站，以一氣體離子腔偵測入射光源強度*I<sub>0</sub>*，光束通過離子腔之後，打在樣品，再由樣品後的離子腔偵測穿過樣品後光束剩餘強度*I<sub>t</sub>*。在It離子腔後，可選擇性加置一參考標準品及另一離子腔*I<sub>r</sub>*，以做為能量校正。實驗時，改變光源之[單光器來掃描目標原子的能量範圍](../Page/單光器.md "wikilink")，即可以\(\ln {\dfrac{I_0}{I_t}}\)求取各能量位置的吸收度。穿透法的限制在於樣品的[濃度](../Page/濃度.md "wikilink")，過濃會有自吸收的效應，太稀[訊雜比不好也無法分析](../Page/訊雜比.md "wikilink")。

### 螢光法

螢光法利用目標原子在吸收光源後躍遷回基態所放出的螢光強度，來判定被吸收光子的量。可用於高濃度粉末或液體形態的樣品。常用的偵檢器為Lytle
detector，並以特定濾光片去除光源的影響。測得的螢光光源\(I_f\)正比於吸收強度，可以用\(I_f/I_0\)得到吸收光譜。

### 電子產率

## 参考文献

## 外部連結

  - [International XAFS Society, official organization promoting
    XAS](http://www.i-x-s.org/)
  - [FEFF Project, University of Washington,
    Seattle](http://leonardo.phys.washington.edu/feff/)
  - [GNXAS project and XAS laboratory, Università di
    Camerino](http://gnxas.unicam.it)

## 参见

  - [X射線晶體學](../Page/X射線晶體學.md "wikilink")
  - [同步輻射](../Page/同步輻射.md "wikilink")
  - [X射線](../Page/X射線.md "wikilink")

[Category:材料科學](../Category/材料科學.md "wikilink")
[Category:光谱学](../Category/光谱学.md "wikilink")