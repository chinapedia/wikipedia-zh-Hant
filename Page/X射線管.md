[Roentgen-Roehre.svg](https://zh.wikipedia.org/wiki/File:Roentgen-Roehre.svg "fig:Roentgen-Roehre.svg")，W<sub>in</sub>/W<sub>out</sub>=冷却水的出／入口\]\]

**X射线管**是能够发生[X射线的一个设备或设备的部件](../Page/X射线.md "wikilink")。

## 結構

X射線管都包含陰極和陽極組件，其皆位于[真空的材料密封罩內亦即為](../Page/真空.md "wikilink")[真空管](../Page/真空管.md "wikilink")。

  - 陰極組件

其是由[鎢絲繞成線圈的形式裝在一個淺的](../Page/鎢.md "wikilink")[聚焦杯](../Page/聚焦杯.md "wikilink")（focusing
cup）中，當[鎢絲通過足夠的](../Page/鎢絲.md "wikilink")[電流時](../Page/電流.md "wikilink")，使其產生白熱現象時[電子會從](../Page/電子.md "wikilink")[鎢的表面逸出形成](../Page/鎢.md "wikilink")[電子雲](../Page/電子雲.md "wikilink")。

  - 陽極組件

主要為[鎢靶](../Page/鎢.md "wikilink")，通常是由[鎢或](../Page/鎢.md "wikilink")[鎢錸合金所製成](../Page/鎢錸合金.md "wikilink")，其作用在於使陰極來的高速電子停止運動。

## X射綫管分類

1\. 根據密封材質不同，可分为玻璃管、陶瓷管和金屬陶瓷管。 2. 根據用途不同，可分為醫療X射綫管和工業X射綫管。 3.
根據密封方式不同，可分為開放式X射綫管：在使用過程中需要不斷抽真空。密閉式X射綫管：生産X射綫管時抽真空到一定程度後立即密封，使用過程中無需再次抽真空。

## 產生方式

X线是由高速运行的电子群撞击物质突然被阻时产生的。因此，它的产生，必须具备以下3个条件：

1.  自由活动的电子群。
2.  电子群以高速运行。
3.  电子群在高速运行时突然受阻。

<!-- end list -->

  - [制動輻射](../Page/制動輻射.md "wikilink")（Bremsstrahlung）
    高速電子突然減速後，其動能轉變成能量釋放齣來，此能量即为X-ray，且此能量會隨減速之程度爾而有所不同。
  - [特性輻射](../Page/特性輻射.md "wikilink")（Characteristic）
    高速電子撞擊原子和外圍軌道上電子，使之遊離且釋放之能量，即为X-ray。

## 運作

當[鎢絲通過足夠的](../Page/鎢.md "wikilink")[電流使其產生電子雲](../Page/電流.md "wikilink")，且有足夠的[電壓](../Page/電壓.md "wikilink")（千伏等級）加在陽極和陰極間，使得電子雲被拉往陽極。此時[電子以高能高速的狀態撞擊](../Page/電子.md "wikilink")[鎢靶](../Page/鎢.md "wikilink")，其部分能量約1%將轉換成[X射線](../Page/X射線.md "wikilink")，剩下的能量轉換成熱能。

## 醫療用途

1.  診斷用X射線管
2.  治療用X射線管

## x射线荧光

x射线可使以下物质发出荧光：钨酸钙CaWO4、氟化钙CaF、硫化锌ZnS、铂氰化钾K2Pt(CN)6、铂氰化钙CaPt(CN)6、铂氰化钡BaPt(CN)6

## 參看

  - [X射線](../Page/X射線.md "wikilink")

[Category:放射線學](../Category/放射線學.md "wikilink")
[Category:德国发明](../Category/德国发明.md "wikilink")