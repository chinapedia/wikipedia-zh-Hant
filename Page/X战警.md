**X戰警**（）是[漫威漫畫的](../Page/漫威漫畫.md "wikilink")[超級英雄團體](../Page/超級英雄.md "wikilink")，由[斯坦·李和](../Page/斯坦·李.md "wikilink")[杰克·科比創造](../Page/杰克·科比.md "wikilink")，初次登場於1963年9月漫畫X-Men
\#1中。與其相關的書在當今美國主流漫畫界十分賣座，並於2000年推出同名電影，全球票房收入達3億美元，其系列電影迄今已拍攝10部。

影片講述一群懷有[異能的](../Page/超能力.md "wikilink")[變種人](../Page/變種人.md "wikilink")，在X教授的號召下，成立了保護世界的特攻队。但人類對變種人恐惧而展開了爭鬥。與其他英雄人物不同的是，《X戰警》告別「個人英雄主義」，充分詮釋了「團結就是力量」的真義，每位X戰警都有绝技，但也有弱點，只有合作才是致勝法寶。

[台灣於](../Page/台灣.md "wikilink")1994年首次以[卡通引進](../Page/卡通.md "wikilink")[台視](../Page/台視.md "wikilink")，稱為《**特異功能組**》。

## 出場人物

### X戰警

#### 正派

  - [X教授](../Page/X教授.md "wikilink")（Professor X）
    原名：查爾斯·泽维爾（Charles Xavier）
    定位：变种學校創辦校长，万磁王好友，擁有世上最强精神力，下肢瘫痪。

<!-- end list -->

  - [镭射眼](../Page/镭射眼.md "wikilink")（Cyclops）
    原名：史考特·萨默斯（Scott Summers）
    異能：眼睛射出強力鐳射
    定位： X战警队长

<!-- end list -->

  - [火凤凰](../Page/琴·葛雷.md "wikilink")（Phoenix）
    原名：琴·葛雷（Jean Grey）
    異能：精神力及念力、鳳凰之力
    定位：最強变种人，有[鳳凰之力](../Page/鳳凰之力.md "wikilink")，現已復活。
    漫畫：少女時期琴被野獸帶回現代與復活的成年琴相見。

<!-- end list -->

  - [暴風女](../Page/暴風女.md "wikilink")（Storm）
    原名：奧蘿洛·摩洛（Ororo Munroe）
    異能：控制天文現象
    定位：家族傳承異能數千年；於『[烏托邦](../Page/烏托邦.md "wikilink")』任領導層之一，目前已加入了非凡X特攻隊。

<!-- end list -->

  - [小淘氣](../Page/小淘氣.md "wikilink")（Rogue）
    原名：瑪莉（Marie）
    異能：与他人接觸時，能吸收生命力及超能力
    定位：現加入聯合小隊(非凡復仇者)。

<!-- end list -->

  - [鋼人](../Page/鋼人_\(漫威漫畫\).md "wikilink")（Colossus）
    原名：彼得·拉斯普廷（Peter Rasputin）
    異能：全身钢化而獲得力量。
    漫畫：是秘客（Magik）的哥哥，加入了機堡帶領的隊伍。

<!-- end list -->

  - [野獸](../Page/野獸_\(漫畫\).md "wikilink")（Beast）
    原名：亨利"漢克"·麥考伊（Henry "Hank" McCoy）
    職業：國防部变种管理部長及聯合國大使
    定位：科學顧問，與鐳射眼一言不合離開烏托邦，加入秘密復仇者。而從過去把原始的五位X戰警成員（史考特、琴、野獸他自己、冰人和天使）帶回。

<!-- end list -->

  - [白皇后](../Page/艾瑪·佛斯特.md "wikilink")（White Queen）
    原名：艾瑪·弗洛斯特（Emma Frost）
    異能：鑽石化，強大精神力
    原设：地狱火俱乐部的白皇后，后加入X战警。

<!-- end list -->

  - [天使](../Page/天使_\(漫威漫畫\).md "wikilink")（Angel）
    原名：華倫·沃辛頓三世（Warren Worthington III）
    異能：有翅膀飛行、適應高空和超強視覺及自癒能力
    定位：繼承了父亲的富有家業;曾為天啟四騎士「死亡」騎士，體內被天啟植入一個自己的人格，自稱「大天使」，變成此狀態時會長出金屬翅膀及藍色皮膚

<!-- end list -->

  - [機堡](../Page/機堡_\(漫威漫畫\).md "wikilink")（Cable）
    原名：納森·克里斯多福·桑瑪斯（Nathan Christopher Summers）
    異能：精神力及念力
    定位：一名神秘的未來戰士，史考特與玛德琳(琴的複製人)之子，左半身被感染為生化機械，撫養了 M 日後首位出生的變種女孩霍普。

<!-- end list -->

  - [秘客](../Page/秘客.md "wikilink")（Magik）
    原名：伊琳雅娜·拉斯普廷（Illyana Rasputin）
    異能：穿越、魔法。
    定位：地獄边境女王。加入了『[烏托邦](../Page/烏托邦.md "wikilink")』。

<!-- end list -->

  - [靈蝶](../Page/靈蝶.md "wikilink")（Psylocke）
    原名：伊莉莎白·布拉多克（Elizabeth Braddock）
    異能：具有心靈感應和精神力凝聚成刀的能力、忍者身體素質及武術。
    定位：英國人，在一次事件中與日本女忍者互換身;哥哥是著名英雄[英國隊長](../Page/英國隊長.md "wikilink")

<!-- end list -->

  - [彌賽亞](../Page/霍普·桑瑪斯.md "wikilink")（Messiah）
    原名:霍普·桑瑪斯（Hope Summers）
    異能：能力複製、鳳凰之力、崑崙武術
    定位：M日後首個出生的變種女孩，复苏变种人异能，由"機堡"带到现代。

<!-- end list -->

  - [快銀](../Page/快銀.md "wikilink")（Quicksilver）
    原名:皮特罗·馬克西莫夫（Pietro Maximoff）
    異能:高速移動。
    定位：萬磁王之子,與姐姐[緋紅女巫一起加入復仇者](../Page/緋紅女巫.md "wikilink")。

<!-- end list -->

  - [萬磁王](../Page/萬磁王.md "wikilink")（Magneto）
    原名：艾瑞克·馬格斯·藍歇爾（Erik Magnus Lehnsherr）
    異能：控制磁场，高级体能。
    定位：加入『[烏托邦](../Page/烏托邦.md "wikilink")』，成為戰鬥主力之一，任領導層。

#### 反派

  - [天啟](../Page/天啟.md "wikilink")（Apocalypse）
    原名：恩·沙巴·努（En Sabah Nur）
    異能：強大改造力，超级体能，瞬移，飛行及操控能量

## 漫畫

| 名稱／刊數                         | 出版日期        | 集數          | 備註                    |
| ----------------------------- | ----------- | ----------- | --------------------- |
| Uncanny X-Men Vol.1           | 1963年～2011年 | 全544集       |                       |
| Uncanny X-Men Vol.2           | 2012年       | 全20集        | Regenesis             |
| Uncanny X-Men Vol.3           | 2013年～2015年 | 全35集        | MARVEL NOW\!\[1\]     |
| Uncanny X-Men Vol.4           | 2016年～2017年 | 全19集        | ALL NEW ALL DIFFERENT |
| X-Men Gold                    | 2017年～      | 連載中         | ResurrXion \[2\]      |
| X-Men Blue                    | 2017年～      | 連載中         | ResurrXion            |
| X-Men Red                     | 2018年～      | 連載中         | Marvel Legacy         |
| Wolverine and the X-Men Vol.1 | 2011年～2014年 | 全42集        | Regenesis             |
| Wolverine and the X-Men Vol.2 | 2014年       | 全12集        | ALL NEW MARVEL NOW\!  |
| Extraordinary X-Men           | 2015年～2017年 | 全20集        | ALL NEW ALL DIFFERENT |
| All-New X-Men Vol.1           | 2012年～2015年 | 全41集        | MARVEL NOW\!          |
| All-New X-Men Vol.2           | 2015年～2017年 | 全19集        | ALL NEW ALL DIFFERENT |
| X-Force Vol.1                 | 1991年～2002年 | 全129集       |                       |
| X-Force Vol.2                 | 2004年～2005年 | 全6集         |                       |
| X-Force Vol.3                 | 2008年～2010年 | 全28集        |                       |
| Uncanny X-Force Vol.1         | 2010年～2012年 | 全35集        |                       |
| Uncanny X-Force Vol.2         | 2013年～2014年 | 全17集        | MARVEL NOW\!          |
| Cable and X-Force             | 2012年～2014年 | 全19集        | MARVEL NOW\!          |
| X-Force Vol.4                 | 2014年～2015年 | 全15集        | ALL NEW MARVEL NOW\!  |
| Weapon X                      | 2017年～      | 連載中         | ResurrXion            |
| X-Men Vol.1                   | 1991年～2001年 | \#1～\#113   |                       |
| New X-Men                     | 2001年～2004年 | \#114～\#156 |                       |
| X-Men Vol.2                   | 2004年～2008年 | \#157～\#207 |                       |
| X-Men: Legacy Vol.1           | 2008年～2012年 | \#208～\#275 |                       |
| X-Men Vol.3                   | 2011年～2013年 | 全41集        |                       |
| X-Men: Legacy Vol.2           | 2012年～2014年 | 全24集        |                       |
| X-Men Vol.4                   | 2013年～2015年 | 全26集        | MARVEL NOW\!          |
| Astonishing X-Men Vol.1       | 1995年       | 全4集         |                       |
| Astonishing X-Men Vol.2       | 1999年       | 全3集         |                       |
| Astonishing X-Men Vol.3       | 2004年～2013年 | 全68集        |                       |
| Astonishing X-Men Vol.4       | 2017年～      | 連載中         | ResurrXion            |
| Amazing X-Men Vol.1           | 1995年       | 全4集         |                       |
| Amazing X-Men Vol.2           | 2013年～2015年 | 全19集        |                       |
| New Mutants Vol.1             | 1983年～1991年 | 全100集       |                       |
| New Mutants Vol.2             | 2003年～2004年 | 全13集        |                       |
| New X-Men: Academy X          | 2004年～2008年 | 全46集        |                       |
| Young X-Men                   | 2008年～2009年 | 全12集        |                       |
| New Mutants Vol.3             | 2009年～2012年 | 全50集        |                       |
| X-Factor Vol.1                | 1986年～1998年 | 全149集       |                       |
| X-Factor Vol.2                | 2002年       | 全4集         |                       |
| X-Factor Vol.3                | 2006年～2013年 | 全94集        |                       |
| All-New X-Factor              | 2014年～2015年 | 全20集        | ALL NEW MARVEL NOW\!  |
| X-Treme X-Men Vol.1           | 2001年～2004年 | 全46集        |                       |
| X-Treme X-Men Vol.2           | 2012年～2013年 | 全13集        |                       |
| Generation X Vol.1            | 1994年～2001年 | 全75集        |                       |
| Generation X Vol.2            | 2017年～      | 連載中         | ResurrXion            |
| X-Statix                      | 2002年～2004年 | 全26集        |                       |

## 其他媒體

### 電影

  - 《[X戰警](../Page/X戰警_\(電影\).md "wikilink")》（2000年）
  - 《[X戰警2](../Page/X戰警2.md "wikilink")》（2003年）
  - 《[X戰警：最後戰役](../Page/X戰警：最後戰役.md "wikilink")》（2006年）
  - 《[X戰警：第一戰](../Page/X戰警：第一戰.md "wikilink")》（2011年）
  - 《[X戰警：未來昔日](../Page/X戰警：未來昔日.md "wikilink")》（2014年）
  - 《[X戰警：天啟](../Page/X戰警：天啟.md "wikilink")》（2016年）
  - 《[X戰警：黑鳳凰](../Page/X戰警：黑鳳凰.md "wikilink")》（2019年）

#### 回歸[漫威電影宇宙](../Page/漫威電影宇宙.md "wikilink")

2017年12月，華特迪士尼公司以524億美元（約1.57兆元台幣）收購福斯集團旗下部分產權，包括由21世紀福斯所擁有的X戰警、驚奇4超人等系列版權，也一併收回華特迪士尼公司旗下漫威影業。美國時間2017年12月14日，華特迪士尼公司宣布X戰警、[死侍](../Page/死侍.md "wikilink")、[驚奇4超人等角色](../Page/驚奇4超人.md "wikilink")，將正式回歸[漫威電影宇宙](../Page/漫威電影宇宙.md "wikilink")。

### 電視劇

  - 《[變種軍團](../Page/變種軍團.md "wikilink")》（2017年2月開播）
  - 《[變種天賦](../Page/變種天賦.md "wikilink")》（2017年10月開播）

### 電子遊戲

以 X 戰警為主題的電子遊戲：

  - Marvel's X-Men (Acclaim)
  - X-Men (1992, Konami)
  - X-Men: Mutant Apocalypse (Capcom)
  - [X戰警：磁場原子人](../Page/X戰警：磁場原子人.md "wikilink") (1994, Capcom)
  - Marvel Super-Heroes (1995, Capcom)
  - X-Men vs. Street Fighter (1996, Capcom)
  - Marvel Super Heroes vs. Street Fighter (1997, Capcom)
  - Marvel vs. Capcom: Clash of Super Heroes (1998, Capcom)
  - Marvel vs. Capcom 2: New Age of Heroes (2000, Capcom)
  - [X戰警](../Page/X戰警_\(1993年遊戲\).md "wikilink") (Sega)
  - [X戰警2：複製人大戰](../Page/X戰警2：複製人大戰.md "wikilink") (Sega)
  - X-Men: Mutant Wars
  - X-men: Wolverine's Rage
  - X2: Wolverine's Revenge (2003)
  - X-Men: Mutant Academy (2000)
  - X-Men: Mutant Academy 2 (2001)
  - X-Men: Next Dimension (2002)
  - X-Men Legends (2004, Activision)
  - X-Men Legends 2 (2005, Activision)
  - X-Men 3: The Last Stand (The Official Game)(2006, Activision)

含有X戰警角色的電子遊戲：

  - 《[漫威英雄 Online](../Page/漫威英雄_Online.md "wikilink")》：有多位X戰警角色可供遊玩

## 参考资料

## 外部連結

  - [官方主頁](http://www.marvel.com/publishing/showcomic.htm?id=4)
  - [Chronology.Net](http://www.chronology.net/)（X戰警劇本）
  - [*Children of The
    Atom*](http://www.redjacketpress.com/books/children_of_the_atom.html)
  - [GameFAQ's Comic Book FAQ:
    X-Men](https://web.archive.org/web/20050905212941/http://www.knightmare6.com/faq/x-men/)
  - [UncannyXmen.net](http://uncannyxmen.net/)
  - [DRG4's X-Men the Animated Series
    Page](http://members.aol.com/drg4/x.html)
  - [mutanthigh.com](http://mutanthigh.com)
  - [X戰警中心](https://web.archive.org/web/20061129051745/http://www.freewebs.com/thexmencentral/)

[X戰警](../Category/X戰警.md "wikilink")
[Category:漫威漫畫超級英雄戰隊](../Category/漫威漫畫超級英雄戰隊.md "wikilink")
[Category:虚构组织](../Category/虚构组织.md "wikilink")
[Category:漫威漫畫突變型](../Category/漫威漫畫突變型.md "wikilink")
[Category:漫威漫畫作品](../Category/漫威漫畫作品.md "wikilink")
[Category:台視外購動畫](../Category/台視外購動畫.md "wikilink")
[Category:亞洲電視外購動畫](../Category/亞洲電視外購動畫.md "wikilink")
[Category:1963年首次发行的漫画](../Category/1963年首次发行的漫画.md "wikilink")

1.  [1](http://marvel.com/comics/events/331/marvel_now_2016) ，In the
    aftermath of Civil War II, the Marvel Universe explodes with new
    heroes, new villains, new teams, and more\! The greatest creators in
    comics provide a fresh take on the best characters ever beginning in
    October 2016\!。
2.  [2](http://marvel.com/comics/events/335/resurrxion)，In the wake of
    war with the Inhumans, the X-Men regroup for a new era of action and
    excitement\! Experience the next bold chapter in the saga of
    Marvel's mutant heroes\!。