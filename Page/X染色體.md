**X染色體**是部分[動物決定性別的](../Page/動物.md "wikilink")[染色體之一](../Page/染色體.md "wikilink")。它出現在X0和XY[性別決定系統中](../Page/性別決定系統.md "wikilink")。對一般[人類來說](../Page/人類.md "wikilink")，[女性有兩條X染色體](../Page/女性.md "wikilink")，[男性X](../Page/男性.md "wikilink")、[Y染色體各有一條](../Page/Y染色體.md "wikilink")。

## 人類

在人類約20000至25000個[基因之中](../Page/基因.md "wikilink")，X染色體約有2000個基因。

一般女性有兩條**X染色體**，有一條會惰性化，變成[巴爾體](../Page/巴爾體.md "wikilink")（Barr
body），這個作用稱為[X去活性](../Page/X去活性.md "wikilink")（X
inactivation）或[里昂化作用](../Page/里昂化作用.md "wikilink")（lyonization，以發現者Mary
F. Lyon命名）。（參見：[三色貓](../Page/三色貓.md "wikilink")）
不論生物遺傳了幾個X染色體，都只有一個X染色體會活性化，其餘皆惰性化，使生物能正常發揮功能，並具有修復功能。

## 疾病

有些人的性染色體數目可能異常：

  - [X-三體](../Page/X-三體.md "wikilink")：女性有三條X染色體。她們發展較遲緩（但可由後天多加培養而糾正），平均智力為90，較正常的100為低。她們能正常生育。\[[https://web.archive.org/web/20070205222117/http://www.aaa.dk/TURNER/ENGELSK/TRIEN.HTM\]較罕見的情況如XXXX或XXXXX都有](https://web.archive.org/web/20070205222117/http://www.aaa.dk/TURNER/ENGELSK/TRIEN.HTM%5D較罕見的情況如XXXX或XXXXX都有)。
  - [克氏综合症](../Page/克氏综合症.md "wikilink")：男性有兩條X染色體。他們的[睪酮水平可能較低](../Page/睪酮.md "wikilink")，胸部發育接近女性。較罕見的情況如XXXY或XXXXY都有。
  - [透納氏症](../Page/透納氏症.md "wikilink")（Turner
    syndrome）：女性只有一條X染色體。她們身材較矮小，性功能不能正常發展。
  - [XYY-三体](../Page/XYY-三体.md "wikilink")：男性多出一条Y染色体。患者身材高大，智力一般与正常人没有过大的差别。

### 遺傳病

[X-linked_recessive_zh.svg](https://zh.wikipedia.org/wiki/File:X-linked_recessive_zh.svg "fig:X-linked_recessive_zh.svg")

X性聯[隱性](../Page/隱性基因.md "wikilink")[遺傳病是指只有一個人所有的X染色體都有帶病基因時才會發生的疾病](../Page/遺傳病.md "wikilink")。男性患上這些疾病的機會都比女性高，因為男性只有一個X染色體，而女性有兩條。若女性其中一條有帶病基因，另一條可以發揮作用，但可能也會有較輕微的症狀出現，因為在每個細胞內去活性化的X染色體都是隨機決定的，若在負責帶病基因相關功能的細胞內去活性化的為帶正常基因的染色體，就會有輕微的症狀出現，例如帶有色盲致病基因的女性，若在視網膜組織內有較多的帶正常基因X染色體不活化，則該女性的辨色力會比基因型完全正常的女性要差。

若以X<sup>B</sup>表示正常基因，以X<sup>b</sup>表示帶病基因，則男性病者表示為X<sup>b</sup>Y，辨色能力正常者為X<sup>B</sup>Y；女性病者表示為X<sup>b</sup>X<sup>b</sup>，[帶因者為X](../Page/帶原者.md "wikilink")<sup>B</sup>X<sup>b</sup>，功能正常亦非帶因者為X<sup>B</sup>X<sup>B</sup>。排除子代的基因發生突變的因素的情況下，子代與父母辨色能力的關係如下：

  - 父母均為病者（X<sup>b</sup>Y，X<sup>b</sup>X<sup>b</sup>）
      -
        兒子為X<sup>b</sup>Y，女兒為X<sup>b</sup>X<sup>b</sup>，即子代都是病者。
  - 父親正常，母親為病者（X<sup>B</sup>Y，X<sup>b</sup>X<sup>b</sup>）
      -
        兒子為X<sup>b</sup>Y，女兒為X<sup>B</sup>X<sup>b</sup>，即，兒子都是病者，女兒為帶因者。
  - 父親為病者，母親正常（兩種情況）
      - 父親為X<sup>b</sup>Y，母親為X<sup>B</sup>X<sup>B</sup>
          -
            兒子為X<sup>B</sup>Y，女兒為X<sup>b</sup>X<sup>B</sup>，即兒子正常，女兒為帶因者。
      - 父親為X<sup>b</sup>Y，母親為X<sup>B</sup>X<sup>b</sup>
          -
            兒子X<sup>B</sup>Y或X<sup>b</sup>Y，女兒為X<sup>B</sup>X<sup>b</sup>或X<sup>b</sup>X<sup>b</sup>，即兒子有50%的概率為病者，女兒必為帶因者，而其中有50%的機率為病者。
  - 父母均正常（兩種情況）
      - 父親為X<sup>B</sup>Y，母親為X<sup>B</sup>X<sup>B</sup>
          -
            兒子為X<sup>B</sup>Y，女兒為X<sup>B</sup>X<sup>B</sup>，即子代都正常。
      - 父親為X<sup>B</sup>Y，母親為X<sup>B</sup>X<sup>b</sup>
          -
            兒子為X<sup>B</sup>Y或X<sup>b</sup>Y，女兒為X<sup>B</sup>X<sup>B</sup>或X<sup>B</sup>X<sup>b</sup>，即兒子有50%的概率為病者，女兒有50%的概率為帶因者。

常見的X性聯隱性遺傳病有：

  - [重症肌無力](../Page/重症肌無力.md "wikilink")
  - [血小板減少症](../Page/血小板減少症.md "wikilink")
  - [痛風](../Page/痛風.md "wikilink")
  - [SCID](../Page/SCID.md "wikilink")([先天性免疫缺乏症](../Page/先天性免疫缺乏症.md "wikilink")、[泡泡兒](../Page/泡泡兒.md "wikilink"))
  - [眼球白化症](../Page/眼球白化症.md "wikilink")
  - [裂顎](../Page/裂顎.md "wikilink")
  - [肌肉萎縮症](../Page/肌肉萎縮症.md "wikilink")
  - [血友病](../Page/血友病.md "wikilink")
  - [色盲](../Page/色盲.md "wikilink")
  - [ALD](../Page/ALD.md "wikilink")（[腎上腺腦白質退化症](../Page/腎上腺腦白質退化症.md "wikilink")）
  - [G6PD](../Page/G6PD.md "wikilink")（[蠶豆症](../Page/蠶豆症.md "wikilink")）
  - [XLSA](../Page/XLSA.md "wikilink")（[含鐵母細胞性貧血的一種](../Page/含鐵母細胞性貧血.md "wikilink")）
  - [X染色體易裂症](../Page/X染色體易裂症.md "wikilink")
  - [法布瑞氏症](../Page/法布瑞氏症.md "wikilink")
  - [愛伯症候群](../Page/愛伯症候群.md "wikilink")

此外，亦有[顯性遺傳病](../Page/顯性基因.md "wikilink")，已知文獻中，此遺傳類型非常稀少。（父母任何一方為病患者便有可能傳到後代）如：

  - [色素失調症](../Page/色素失調症.md "wikilink")
  - [維生素D型無效性](../Page/維生素D.md "wikilink")[佝僂病](../Page/佝僂病.md "wikilink")
  - [CMTX](../Page/CMTX.md "wikilink")（[腓骨肌萎縮症的一種](../Page/腓骨肌萎縮症.md "wikilink")）
  - [蕾特氏症](../Page/蕾特氏症.md "wikilink")([Rett
    Syndrome](../Page/Rett_Syndrome.md "wikilink"))

## 外部連結

  - [什麼是里昂化作用？，黃仲嘉（科學月刊0259期）](http://book.tngs.tn.edu.tw/database/scientieic/content/1991/00070259/0011.htm)
  - [Sciscape
    新聞Dec 20, 2002：愛斷裂的染色體](https://web.archive.org/web/20070522213127/http://www.sciscape.org/news_detail.php?news_id=907)
  - [Sciscape
    新聞Feb 12, 2004：逃離X染色體](https://web.archive.org/web/20070927173119/http://www.sciscape.org/news_detail.php?news_id=1385)

{{-}}

[Category:染色体](../Category/染色体.md "wikilink")
[Category:细胞遗传学](../Category/细胞遗传学.md "wikilink")