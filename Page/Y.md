**Y**, **y**
是[拉丁字母中的第](../Page/拉丁字母.md "wikilink")25个[字母](../Page/字母.md "wikilink")。

在[希腊语中](../Page/希腊语.md "wikilink")，（Upsilon）曾经发作/[u](../Page/闭后圆唇元音.md "wikilink")/（后来变成/[y](../Page/闭前圆唇元音.md "wikilink")/，现在发作/[i](../Page/闭前不圆唇元音.md "wikilink")/）。[罗马人直接借用了](../Page/罗马人.md "wikilink")[希腊语的Y](../Page/希腊.md "wikilink")，因为他们感到V不再完全相当于希腊语的/y/，並稱之為"I
graeca"("希臘的I")，造就出現代拉丁語系中"Y"的名字:[西班牙语](../Page/西班牙语.md "wikilink")"I
griega"，[加泰罗尼亚语](../Page/加泰罗尼亚语.md "wikilink")*I
grega*，[法语](../Page/法语.md "wikilink")*I
grec*（都表示“希腊语的I”）。而[英语中Y的名称](../Page/英语.md "wikilink")/waɪ/，則是因為早期將"Y"認為是上面"V"/w/、下面I/i/的組合，所以發音為/wi:/，而其長母音/i:/經歷[母音大推移後成了](../Page/母音大推移.md "wikilink")/aɪ/，從此Y在英語中變為/waɪ/。在很多其他[欧洲](../Page/欧洲.md "wikilink")[语言中](../Page/语言.md "wikilink")，这个希腊字母的名称仍然保留着。字母Y起初是用来表示[元音的](../Page/元音.md "wikilink")，现在则同时表示元音和[辅音](../Page/辅音.md "wikilink")。

在[北约音标字母中](../Page/北约音标字母.md "wikilink")，用*Yankee*表示字母Y。

## 字母Y的含意

## 字符编码

| 字符编码                               | [ASCII](../Page/ASCII.md "wikilink") | [Unicode](../Page/Unicode.md "wikilink") | [EBCDIC](../Page/EBCDIC.md "wikilink") | [摩斯电码](../Page/摩斯电码.md "wikilink") |
| ---------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- | ---------------------------------- |
| [大写](../Page/大写字母.md "wikilink") Y | 89                                   | 0059                                     | 232                                    | `-·--`                             |
| [小写](../Page/小写字母.md "wikilink") y | 121                                  | 0079                                     | 168                                    |                                    |

## 其他表示法

## 参看

### Y的變體

  - [次閉次前圓唇元音](../Page/次閉次前圓唇元音.md "wikilink")

  - [硬顎邊音](../Page/硬顎邊音.md "wikilink")

### 其他字母中的相近字母

  - （[希腊字母](../Page/希腊字母.md "wikilink") Upsilon）

  - （[西里尔字母](../Page/西里尔字母.md "wikilink") U）

[Category:拉丁字母](../Category/拉丁字母.md "wikilink")