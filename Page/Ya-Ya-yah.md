**Ya-Ya-yah**（）是[傑尼斯事務所旗下的男子偶像團體](../Page/傑尼斯事務所.md "wikilink")，由四名小傑尼斯組成。

Ya-Ya-yah的兄組為[KAT-TUN](../Page/KAT-TUN.md "wikilink")，而弟組則為。

## 簡介

  - 於2002年之前，組合的主唱是[薮宏太和](../Page/薮宏太.md "wikilink")；2003年之後，由於赤間直哉進入變聲期，主唱改由藪宏太和[八乙女光擔任](../Page/八乙女光.md "wikilink")。

<!-- end list -->

  - 八乙女光、藪宏太、[山下翔央](../Page/山下翔央.md "wikilink")、[鮎川太陽均畢業自](../Page/鮎川太陽.md "wikilink")[堀越高等學校](../Page/堀越高等學校.md "wikilink")。

### 組合名的由來

組合成立時，組合初代成員薮宏太（）、山下翔央（）、赤間直哉（）等人的姓氏用羅馬音標記后都有“Ya”，故而得名。隨後，隨著其中四人的退團，組合名被解釋為與[披頭四的歌曲有關](../Page/披頭四.md "wikilink")。

最初組合名為YA-YA，后改為YA-YA-Yah，最後被定為Ya-Ya-yah。

組合的正式名稱為“Ya-Ya-yah”，但有時也會被其他媒體稱之為“ヤーヤーヤー”和“YA-YA-yah”。

## 履歷

## 成員

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>片假名</p></th>
<th><p>生年月日</p></th>
<th><p>血型</p></th>
<th><p>出身地</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/八乙女光.md" title="wikilink">八乙女光</a></strong></p></td>
<td></td>
<td></td>
<td><p>O型</p></td>
<td><p><a href="../Page/宮城縣.md" title="wikilink">宮城縣</a></p></td>
<td><p>加入<a href="../Page/Hey!_Say!_JUMP.md" title="wikilink">Hey! Say! JUMP出道</a>。</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/薮宏太.md" title="wikilink">薮宏太</a></strong></p></td>
<td></td>
<td></td>
<td><p>A型</p></td>
<td><p><a href="../Page/神奈川縣.md" title="wikilink">神奈川縣</a></p></td>
<td><p>加入<a href="../Page/Hey!_Say!_JUMP.md" title="wikilink">Hey! Say! JUMP出道</a>。</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/山下翔央.md" title="wikilink">山下翔央</a></strong></p></td>
<td></td>
<td></td>
<td><p>A型</p></td>
<td><p><a href="../Page/東京都.md" title="wikilink">東京都</a></p></td>
<td><p>2010年退出傑尼斯事務所，與弟弟山下玲央組成“山下兄弟”。現隸屬。</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/鮎川太陽.md" title="wikilink">鮎川太陽</a></strong></p></td>
<td></td>
<td></td>
<td><p>A型</p></td>
<td><p><a href="../Page/東京都.md" title="wikilink">東京都</a></p></td>
<td><p>2007年退出傑尼斯事務所。現隸屬<a href="../Page/尾木製作.md" title="wikilink">尾木製作</a>。</p></td>
</tr>
</tbody>
</table>

### 中途退出的成員

  - （，）：團隊內最年輕的成員，2004年退出[傑尼斯事務所](../Page/傑尼斯事務所.md "wikilink")。2011年7月以“熊野直哉”為名重返娛樂圈，現隸屬

<!-- end list -->

  - 星野正樹（，）：2004年退出傑尼斯事務所。2007年曾在[匡威公司的](../Page/匡威.md "wikilink")“”中登場。

<!-- end list -->

  - 安藤靖浩（，）：2002年退出傑尼斯事務所。

<!-- end list -->

  - 吉田雄基（，）：2002年退出傑尼斯事務所。

## 作品J

### CD

  - 《[勇氣100%／直到世界大同](../Page/勇氣100%／直到世界大同.md "wikilink")》（勇気100%／世界がひとつになるまで）：出版日期：2002年5月15日。

### 未正式出版的歌曲

### 組合演唱會

## 演出

## 復出傳聞

[Hey\! Say\! JUMP](../Page/Hey!_Say!_JUMP.md "wikilink")
成員[薮宏太以及](../Page/薮宏太.md "wikilink")[八乙女光於](../Page/八乙女光.md "wikilink")2017年1月在帝國劇場
- 「[Johnny's ALLSTAR
IsLAND](../Page/Johnny's_ALLSTAR_IsLAND.md "wikilink")」節目以Ya-Ya-yah身份登場，並演出Ya-Ya-yah兩首代表作:「Just
wanna lovin' you」和「愛しのプレイガール」，被喻為奇蹟復活。

## 相關詞條

  - [傑尼斯事務所](../Page/傑尼斯事務所.md "wikilink")
  - [波麗佳音](../Page/波麗佳音.md "wikilink")
  - [Hey\! Say\!
    JUMP](../Page/Hey!_Say!_JUMP.md "wikilink")<small>（團隊中[薮宏太與](../Page/薮宏太.md "wikilink")[八乙女光最終出道的團體](../Page/八乙女光.md "wikilink")）</small>

[Ya-Ya-yah](../Category/Ya-Ya-yah.md "wikilink")
[Y](../Category/前小傑尼斯團體.md "wikilink")
[Y](../Category/日本男子演唱團體.md "wikilink")
[Y](../Category/日本男子偶像團體.md "wikilink")
[Y](../Category/波麗佳音.md "wikilink")