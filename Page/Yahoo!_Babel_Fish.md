**Yahoo\! Babel Fish**（[Yahoo\!
奇摩稱作](../Page/Yahoo!_奇摩.md "wikilink")**Yahoo\!
奇摩翻譯**；又譯**寶貝魚**、**巴別魚**）是一個[互聯網上一個自動化的免費](../Page/互聯網.md "wikilink")[機械翻譯系統服務](../Page/機械翻譯.md "wikilink")，由[門戶網站](../Page/門戶網站.md "wikilink")[AltaVista](../Page/AltaVista.md "wikilink")（已被[雅虎收購](../Page/雅虎.md "wikilink")）提供。寶貝魚可以為用戶翻譯一段文字，或指定的網頁內容。早期的寶貝魚服務只提供有五種最常見的歐洲語言，分別是：英語、法語、德語、西班牙語及意大利語。現時系統已能作12種語言（繁簡中文算作一種）的翻譯。从2012年6月1日起，Yahoo\!Babel
Fish已经关停，如果访问Yahoo\! Babel Fish则会自动转向[Bing
Translator](../Page/Bing翻译.md "wikilink")。\[1\]

## 取名

「寶貝魚」的名稱源於[英國廣播公司的連續劇](../Page/英國廣播公司.md "wikilink")、改編自[道格拉斯·亞當斯的小說](../Page/道格拉斯·亞當斯.md "wikilink")《[-{zh-hans:银河系漫游指南;
zh-hant:銀河便車指南;}-](../Page/银河系漫游指南.md "wikilink")》系列的虛構魚形機械生物[巴別魚](../Page/巴別魚.md "wikilink")，而「巴別魚」的名稱又來自《[聖經](../Page/聖經.md "wikilink")·[創世紀](../Page/創世紀.md "wikilink")》有關人類在被上帝搞亂了他們的語言時所興建的建築物[巴別塔](../Page/巴別塔.md "wikilink")。

## 翻譯功能

寶貝魚的翻譯功能其實是由另外一家公司所提供。這一家公司名叫[SYSTRAN](../Page/SYSTRAN.md "wikilink")，是一家專長於機器翻譯的公司。SYSTRAN的主力是研究供一般桌面電腦用戶使用的翻譯系統。而隨着SYSTRANS可提供自動翻譯的語言增加，寶貝魚所能夠翻譯的語言數目亦同時增加。而SYSTRANS除了為AltaVista及雅虎提供自動翻譯服務以外，亦有與其他網站合作，例如：2007年之前的[Google翻译](../Page/Google翻译.md "wikilink")。

## 支援語言

寶貝魚支援以下12種語言：

1.  [英語](../Page/英語.md "wikilink")
2.  [中文](../Page/中文.md "wikilink")（簡繁體）
3.  [荷蘭語](../Page/荷蘭語.md "wikilink")
4.  [法語](../Page/法語.md "wikilink")
5.  [德語](../Page/德語.md "wikilink")
6.  [希臘語](../Page/希臘語.md "wikilink")
7.  [意大利語](../Page/意大利語.md "wikilink")
8.  [日語](../Page/日語.md "wikilink")
9.  [韓語](../Page/韓語.md "wikilink")
10. [葡萄牙語](../Page/葡萄牙語.md "wikilink")
11. [俄語](../Page/俄語.md "wikilink")
12. [西班牙語](../Page/西班牙語.md "wikilink")

## 翻译结果

大致上來說，寶貝魚服務對各個歐洲語言的翻譯水平算是不錯。不過對於三種東方語言來說卻甚為糟糕：它的詞庫很小，使很多常用詞都會譯錯，而且對於拉丁化了的文字的辨識更是糟糕。所以有些網站特地把用寶貝魚翻出來的垃圾來製作笑話網頁（見外部連結第三條）。舉例說：

1.  原句：英語
      -
        Oh, Christmas Tree\!
2.  譯成意大利語
      -
3.  再譯回英語
      -
        Good tree of Been born them\!
4.  譯成葡萄牙語
      -
5.  再譯回英語
      -
        Good tree of loaded them\!
6.  譯成西班牙語
      -
7.  再譯回英語
      -
        Good tree of loaded them\!

## 参考资料

## 外部連結

  - [Babel Fish Translation
    Service](https://web.archive.org/web/19990421124540/http://babelfish.altavista.com/)
  - [翻譯新寵寶貝魚](https://web.archive.org/web/20070930180730/http://babelfish.yahoo.com.cn/)
  - [Lost in Translation (round-trip
    translation)](https://web.archive.org/web/20070701111927/http://www.tashian.com/multibabel/)
  - [Yahoo\!奇摩翻譯](https://web.archive.org/web/20090227012157/http://tw.babelfish.yahoo.com/)
  - [Bad Translator](http://ackuna.com/badtranslator)

[Category:機器翻譯](../Category/機器翻譯.md "wikilink")
[Category:2012年关闭的网站](../Category/2012年关闭的网站.md "wikilink")
[Category:1997年面世的產品](../Category/1997年面世的產品.md "wikilink")

1.  [Yahoo\! Babel Fish 翻译正式由 Bing Translator
    取代](http://www.yseeker.com/archives/7263.html)