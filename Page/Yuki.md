**Yuki**（）是一個常見的[日語字詞](../Page/日語.md "wikilink")，通常用來作為[人名](../Page/姓名.md "wikilink")。

  - **[YUKI
    (歌手)](../Page/YUKI_\(歌手\).md "wikilink")**：[日本女](../Page/日本.md "wikilink")[歌手](../Page/歌手.md "wikilink")，日本已解散[樂團](../Page/樂團.md "wikilink")[JUDY
    AND MARY主唱](../Page/JUDY_AND_MARY.md "wikilink")。
  - **[櫻井有紀](../Page/櫻井有紀.md "wikilink")**：日本男性歌手，日本樂隊Rice主唱。
  - **[徐懷鈺](../Page/徐懷鈺.md "wikilink")**：[台灣女性歌手](../Page/台灣.md "wikilink")。
  - **[增山裕紀](../Page/增山裕紀.md "wikilink")**：[台灣](../Page/台灣.md "wikilink")[男性歌手](../Page/男性.md "wikilink")，擁有[日本人血統](../Page/日本人.md "wikilink")。
  - **[石井優希](../Page/石井優希.md "wikilink")**：[日本](../Page/日本.md "wikilink")[女性排球員](../Page/女性.md "wikilink")。
  - **[长门有希](../Page/长门有希.md "wikilink")**：[日本](../Page/日本.md "wikilink")[轻小说](../Page/輕小說.md "wikilink")《[凉宫春日系列](../Page/涼宮春日系列.md "wikilink")》中的人物。