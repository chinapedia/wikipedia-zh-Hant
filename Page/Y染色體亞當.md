**Y染色體亞當**（Y-chromosomal
Adam），或稱**Y-MRCA**，在[遺傳學上](../Page/遺傳學.md "wikilink")，由[人類Y染色體DNA單倍型類群推測出的所有現存](../Page/人類Y染色體DNA單倍型類群.md "wikilink")[男性在](../Page/男性.md "wikilink")[父系上的](../Page/父系.md "wikilink")[最近共同祖先](../Page/最近共同祖先.md "wikilink")，從他遺傳[Y染色體](../Page/Y染色體.md "wikilink")。Y染色體亞當相對於[粒線體夏娃](../Page/粒線體夏娃.md "wikilink")。

從[世界上所有地區的人類](../Page/世界.md "wikilink")[DNA分析中](../Page/DNA.md "wikilink")，[遺傳學家](../Page/遺傳學家.md "wikilink")[史賓賽·韋爾斯](../Page/史賓賽·韋爾斯.md "wikilink")（Spencer
Wells）結論出所有今天的人類都是生存於6萬年前[非洲大陆的男人的後裔](../Page/非洲大陆.md "wikilink")\[1\]。

## 命名

Y染色體亞當虽然是以《[創世記](../Page/創世記.md "wikilink")》中的[亞當來命名](../Page/亞當.md "wikilink")，但這個名稱并不是指Y染色體亞當是當時唯一的男性，而是指他的[Y染色體流传至今](../Page/Y染色體.md "wikilink")、是现代男性的父系最近共同祖先。事實上，其他同期男性的其他基因也流传至今，只是他们的Y染色體没有留传到现代，也就是他们纯粹的直系男嗣已经中断，但通过女性后人繁衍下来的其他后代依然存在。

## 時間框

根據[分子鐘及](../Page/分子鐘.md "wikilink")[遺傳標記的研究](../Page/遺傳標記.md "wikilink")，Y染色體亞當可能生存於6萬至9萬年前。雖然Y染色體亞當與[粒線體夏娃的後裔變得親密](../Page/粒線體夏娃.md "wikilink")，但他們卻相隔了最少3萬年([粒線體夏娃較早](../Page/粒線體夏娃.md "wikilink"))，或可能是1000代。這是從男性及女性的[生殖方法差異而發現的](../Page/生殖.md "wikilink")。

Y染色體亞當較粒線體夏娃為近期，對應[舊石器時代有後代的男人比當時有後代的女人的](../Page/舊石器時代.md "wikilink")[概率分佈有更大的差異](../Page/概率分佈.md "wikilink")。正常生育女人有接近相等的機會產下一定數量能正常生育的後代，而正常生育男人則較參差，有些沒有兒子，或有些則有很多。

Y染色體亞當在[人類歷史中的亞當並非同一人](../Page/人類.md "wikilink")，所有現今人類的Y染色體亞當有可能會與過去或未來人類的Y染色體亞當有所不同。如果其中一支较远的Y染色体单倍群在后来失传，一個較近期的人就會成為新的Y染色體亞當。在迅速人口增長的時代，父系分支對比[人口瓶頸時較難消失](../Page/人口瓶頸.md "wikilink")。

## 參考文献

### 引用

### 来源

  - 刊物文章

<!-- end list -->

  -
  -
## 外部連結

  - [Mitochondrial Eve and Y-chromosomal Adam
    Diagrams](http://www.chartsgraphsdiagrams.com/evolution/mitochondrial-eve.html)
  - [Y-Chromosome Biallelic
    Haplogroups](http://www.roperld.com/YBiallelicHaplogroups.htm)

{{-}}

[Category:演化生物學](../Category/演化生物學.md "wikilink")
[Category:遺傳學](../Category/遺傳學.md "wikilink")
[Category:人類演化](../Category/人類演化.md "wikilink")
[Category:亚当与夏娃](../Category/亚当与夏娃.md "wikilink")

1.