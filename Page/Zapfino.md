**Zapfino**是一个西文[手写体字体](../Page/手写体.md "wikilink")，由德国字体设计师[赫尔曼·察普夫在](../Page/赫尔曼·察普夫.md "wikilink")1998年为[Linotype公司设计](../Page/Linotype.md "wikilink")，共有6种字重，基本按照1944年察普夫的手写字母表制作而成。作为一个字体，它极大丰富拓展了[连字和](../Page/连字.md "wikilink")[字符变体](../Page/字符变体.md "wikilink")（比如，小写字母d有9种变体）。
为了丰富手写字体，[苹果公司在其](../Page/苹果公司.md "wikilink")[Mac OS
X操作系统中收入了一个字重的Zapfino](../Page/Mac_OS_X.md "wikilink")。这个字体含有4整套字母，字形超过1400种。

## 历史

1983年，Zapf、[高德纳和](../Page/高德纳.md "wikilink")[斯坦福大学的David](../Page/斯坦福大学.md "wikilink")
Siegel一同为[美国数学会完成了](../Page/美国数学会.md "wikilink")[AMS
Euler字体](../Page/AMS_Euler.md "wikilink")，包含了[德文尖角体和](../Page/德文尖角体.md "wikilink")[希腊字母](../Page/希腊字母.md "wikilink")，用于制作数学公式。彼时David
Siegel刚刚完成在斯坦福的学业，希望能够进入[字体排印领域](../Page/字体排印.md "wikilink")。他向Zapf透露了自己制作一款包含大量字符变体的字体的想法，希望从Zapf在芝加哥字体艺术协会（the
Society of Typographical Arts in Chicago）发表的手写体字样开始。

Zapf不大认同这个想法，故而对这个新项目没什么兴趣。不过，Zapf想到了自己1944年的一张手写体作品。在1948年，他曾经尝试为Stempel制作一款手写字体，受制于铁水创作的局限性没有成功。现代的数码技术已能轻易完成这项任务，于是Zapf和Siegel开始编写所需的复杂软件。Siegel雇佣了波士顿的程序员Gino
Lee一同完成这个项目。

然而，就在项目即将完成之际，Siegel给Zapf去信说自己被女朋友甩了，已无心任何事情。于是Siegel中断了项目，开始了自己的新生涯——为[苹果电脑带来色彩](../Page/苹果电脑.md "wikilink")。他现在是一名互联网设计师。

Zapfino的开发被严重推迟，直到Zapf向[Linotype展示这一项目](../Page/Linotype.md "wikilink")。Linotype决定重新组织并完成这一项目，最终，Zapfino作为一个[Type
1字体于](../Page/Type_1.md "wikilink")1998年发布。

## 应用情况

[苹果公司在](../Page/苹果公司.md "wikilink")[Mac OS
X中包含了Zapfino](../Page/Mac_OS_X.md "wikilink")，籍此展示其高级的字体排印功能。Linotype在零售渠道销售含有更多字符的字体版本，并在2003年推出的*Zapfino
Extra*中包含了额外的字重（苹果并未获得该字体的授权）。

## Zapfino Extra

在2003年，Zapf和小林章（Akira Kobayashi）共同重新设计了Zapfino，推出了Zapfino Extra。Zapfino
Extra含有更丰富的字形，支持[OpenType格式](../Page/OpenType.md "wikilink")。

## 外部链接

  - [Typowiki: Zapfino](http://typophile.com/wiki/zapfino)
  - [Linotype公司关于Zapfino的描述](https://archive.is/20130128024216/http://www.linotype.com/en/1175/zapfino-family.html)

[Category:字体](../Category/字体.md "wikilink")
[Category:蘋果公司字體](../Category/蘋果公司字體.md "wikilink")
[Category:Linotype字体](../Category/Linotype字体.md "wikilink")