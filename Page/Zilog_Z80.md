\-{H|zh-cn:位;zh-tw:位元}-  **Zilog Z80
CPU**，簡稱**Z80**，是一款由[zilog公司製造的](../Page/zilog.md "wikilink")8位元[微處理器](../Page/微處理器.md "wikilink")，與[英特爾公司出產的](../Page/英特爾公司.md "wikilink")[8080](../Page/Intel_8080.md "wikilink")[微處理器的代碼兼容](../Page/微處理器.md "wikilink")。Z80可執行為8080所寫的[CP/M](../Page/CP/M.md "wikilink")[作業系統](../Page/作業系統.md "wikilink")，所以過去在[apple
II兼容機盛行的年代](../Page/apple_II.md "wikilink")，很多人都愛在電腦內加裝[z80擴充卡](../Page/z80擴充卡.md "wikilink")，並透過它來運行[WordStar](../Page/WordStar.md "wikilink")、[VisiCalc等商業軟體](../Page/VisiCalc.md "wikilink")。

Z80也廣泛用在一些家用電腦（當時還未使用[個人電腦這一名詞](../Page/個人電腦.md "wikilink")）中，其中較知名的例如Tandy
/ Radio Shack的TRS-80。
[Okona-GfhR-TRS-80.jpg](https://zh.wikipedia.org/wiki/File:Okona-GfhR-TRS-80.jpg "fig:Okona-GfhR-TRS-80.jpg")
[Game-Gear-Handheld.jpg](https://zh.wikipedia.org/wiki/File:Game-Gear-Handheld.jpg "fig:Game-Gear-Handheld.jpg")遊戲機使用Zilog
Z80\]\]
Z80也大量用於微電腦學習機，例如[宏碁公司的第一款微電腦產品](../Page/宏碁.md "wikilink")：[小教授一號](../Page/小教授一號.md "wikilink")（MPF
I）。

原始Z80的最高時脈是2.5 MHz，Z80A則可以使用到4 MHz時脈，後來推出的Z80B最高可以使用6
MHz時脈。之後還有8MHz、10MHz的版本。

Z80原使用NMOS製程，後來也有生產CMOS製程的Z80。後期使用的編號，NMOS者為Z8400，CMOS者為Z84C00。

## 第二來源廠商

[KL_NEC_uPD780C.jpg](https://zh.wikipedia.org/wiki/File:KL_NEC_uPD780C.jpg "fig:KL_NEC_uPD780C.jpg")
除了Zilog公司外，當時製造與Z80（A）相同CPU的第二來源廠商還有Sharp與NEC，其廠牌型號對應如下：

| 廠牌＼最高時脈速度 | 2.5 MHz | 4 MHz     | 6 MHz     |
| --------- | ------- | --------- | --------- |
| ZiLog     | Z80 CPU | Z80A CPU  | Z80B CPU  |
| NEC       | µPD780C | µPD780C-1 | µPD780C-2 |
| Sharp     | LH0080  | LH0080A   | LH0080B   |

## Z80週邊元件

Z80系列除了有Z80 CPU（後來又稱Z8400）之外，Zilog公司也推出一系列週邊IC，可搭配Z80 CPU使用。

  - Z80 DMA (Z8410) :
    [直接記憶體存取控制器](../Page/直接記憶體存取.md "wikilink")。（功能類似Intel
    8257）
  - Z80 PIO (Z8420) :平行輸入輸出埠介面。（功能類似Intel 8255）
  - Z80 CTC (Z8430) :計數器、計時器。（功能類似Intel 8253）
  - Z80 SIO (Z8440) :序列輸入輸出埠介面，支援同步與非同步傳輸。（功能類似Intel 8251）

## 參見

  - [Z8000](../Page/Z8000.md "wikilink") :
    Zilog公司在Z80之後所推出的16位元微處理器。同期的競爭產品有Intel的8086與Motorola的68000。

[Category:微處理器](../Category/微處理器.md "wikilink")