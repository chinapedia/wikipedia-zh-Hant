[Human-gnome-dev-zipdisk.svg](https://zh.wikipedia.org/wiki/File:Human-gnome-dev-zipdisk.svg "fig:Human-gnome-dev-zipdisk.svg")

**zip
Drive**（**極碟**）是[美國](../Page/美國.md "wikilink")[埃美加](../Page/埃美加.md "wikilink")（Iomega）[公司所發明的一種高容量軟式磁碟機](../Page/公司.md "wikilink")，使用具有較堅固外殼的特製高容量軟碟片，並利用部分[硬碟中使用的技術](../Page/硬碟.md "wikilink")，製成的[個人電腦](../Page/個人電腦.md "wikilink")[儲存裝置](../Page/儲存裝置.md "wikilink")。

## 發展

zip最初推出時只有50MB；到推出100MB容量的-{zh:磁碟;zh-hans:软盘;zh-hk:磁碟;zh-tw:磁碟片;}-（zip100）後，開始大受歡迎；數年後，更推出250MB容量的-{zh:磁碟;zh-hans:软盘;zh-hk:磁碟;zh-tw:磁碟片;}-（zip250）；最後，推出750MB容量的-{zh:磁碟;zh-hans:软盘;zh-hk:磁碟;zh-tw:磁碟片;}-（zip750）。ZIP驱动器具有向下兼容性，即較高容量的zip磁碟機可以讀取相同容量或較低容量的zip磁碟片，但写入速度有不同：例如ZIP250驱动器可以写入ZIP100的盘片，但速度不如原生ZIP100驱动器的写入速度快。較低容量的zip磁碟機不能讀取較高容量的zip磁碟片。

zip磁碟片的製造商僅有Iomega與[Fujifilm而已](../Page/Fujifilm.md "wikilink")。

## 競爭

後來以[3M為首的公司推出可兼容傳統](../Page/3M.md "wikilink")3.5吋[軟碟的](../Page/軟碟.md "wikilink")120MB-{zh:磁碟;zh-hans:软盘;zh-hk:磁碟;zh-tw:磁碟片;}-，稱為[Super
Disk](../Page/Super_Disk.md "wikilink")（又名[LS-120](../Page/LS-120.md "wikilink")）；但由於速度超慢，加上zip
Drive已拿下市場領導地位，所以未有對其構成威脅。
[ZipDiskNDrive_unitsales_1998to2003.svg](https://zh.wikipedia.org/wiki/File:ZipDiskNDrive_unitsales_1998to2003.svg "fig:ZipDiskNDrive_unitsales_1998to2003.svg")

## 衰落

不久，隨着[CD-R價格不斷下降及](../Page/CD-R.md "wikilink")[外接硬碟的普及](../Page/外接硬碟.md "wikilink")，zip-{zh:磁碟;zh-hans:软盘;zh-hk:磁碟;zh-tw:磁碟片;}-不再受歡迎；但在[香港不少](../Page/香港.md "wikilink")[大專院校的電腦中心](../Page/大專院校.md "wikilink")，他們的電腦仍然有zip
Drive。除此之外，Iomega亦從生產zip-{zh:磁碟;zh-hans:软盘;zh-hk:磁碟;zh-tw:磁碟片;}-轉為生產CD[燒錄機](../Page/燒錄機.md "wikilink")。

## 評價

  - 2006年5月26日，zip榮獲[PCWorld.com網站選為](../Page/PCWorld.com.md "wikilink")
    [「史上25大科技爛貨」第15名](http://www.pcworld.com/reviews/article/0,aid,125772,pg,4,00.asp#zip)。

[File:Zip-100a.jpg|zip100-{zh:磁碟;zh-hans:软盘;zh-hk:磁碟;zh-tw:磁碟片](File:Zip-100a.jpg%7Czip100-%7Bzh:磁碟;zh-hans:软盘;zh-hk:磁碟;zh-tw:磁碟片);}-正面
[File:Zip-100b.JPG|zip100-{zh:磁碟;zh-hans:软盘;zh-hk:磁碟;zh-tw:磁碟片](File:Zip-100b.JPG%7Czip100-%7Bzh:磁碟;zh-hans:软盘;zh-hk:磁碟;zh-tw:磁碟片);}-背面
<File:ZIP> 100 750
discos.png|zip100與zip750-{zh:磁碟;zh-hans:软盘;zh-hk:磁碟;zh-tw:磁碟片;}-正面
<File:NEC> FZ110A
20070416.jpg|[日本電氣製造的內接式zip](../Page/日本電氣.md "wikilink")100磁碟機FZ110A，可讀取zip100磁碟片
[File:Internal_zip_drive_inside_computer.jpg|上：內接式zip250磁碟機，可讀取zip100與zip250磁碟片](File:Internal_zip_drive_inside_computer.jpg%7C上：內接式zip250磁碟機，可讀取zip100與zip250磁碟片)
[File:ZipDrives.jpeg|左起](File:ZipDrives.jpeg%7C左起)：[USB介面的zip](../Page/USB.md "wikilink")250免插電外接式磁碟機、[並行端口介面的zip](../Page/並行端口.md "wikilink")100外接式磁碟機及其[變壓器](../Page/變壓器.md "wikilink")

## 參看

  - [爵士可擴充硬碟](../Page/爵士可擴充硬碟.md "wikilink")（Jaz drive）
  - [磁光碟](../Page/磁光碟.md "wikilink")（MO）

## 外部連結

  - [Iomega全球首頁](https://web.archive.org/web/20080317205814/http://www.iomega.com/global/index.jsp)

[Category:電腦貯存裝置](../Category/電腦貯存裝置.md "wikilink")