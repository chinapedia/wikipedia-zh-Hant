[ZOHO.svg](https://zh.wikipedia.org/wiki/File:ZOHO.svg "fig:ZOHO.svg")**Zoho**（又名：**Zoho
Office Suite**，以及
**Zoho.com**）为在线办公室[网站](../Page/网站.md "wikilink")，Zoho是ZOHOCORP(原名AdventNet)基于云计算技术推出的一系列办公应用，包含在线Office、电子邮箱、客户关系管理（CRM）、项目管理（PM）、客户服务管理、商业智能、建站工具等。

## 关于

Zoho成立于1996年，致力于在线办公的研究，是全球第一大在线软件提供商，由印度人Sridhar Vembu创立。其主要产品线为：[Zoho
Writer](../Page/Zoho_Writer.md "wikilink")、[Zoho
Sheet](../Page/Zoho_Sheet.md "wikilink") 、[Zoho
Show](../Page/Zoho_Show.md "wikilink") 、[Zoho
Projects](../Page/Zoho_Projects.md "wikilink") 、[Zoho
CRM](../Page/Zoho_CRM.md "wikilink") 、[Zoho
Creator](../Page/Zoho_Creator.md "wikilink") 、[Zoho
Wiki](../Page/Zoho_Wiki.md "wikilink") 、[Zoho
Planner](../Page/Zoho_Planner.md "wikilink") 、[Zoho
Notebook](../Page/Zoho_Notebook.md "wikilink") 、[Zoho
Chat](../Page/Zoho_Chat.md "wikilink") 、[Zoho Virtual
Office](../Page/Zoho_Virtual_Office.md "wikilink")、[Zoho
Challenge以及](../Page/Zoho_Challenge.md "wikilink")[Zoho
Polls](../Page/Zoho_Polls.md "wikilink")
。由于在线网站的特殊性，因此Zoho适用于各种操作系统以及大多数浏览器（例如[IE](../Page/Internet_Explorer.md "wikilink")、[Firefox等](../Page/Firefox.md "wikilink")）

## Zoho Creator

**Zoho
Creator**为**Zoho**旗下的[表单处理系统](../Page/表单处理.md "wikilink")，用户可以使用zoho
id来增加／阅读反馈表单。同大多数zoho产品一样，Zoho
Creator拥有非常多的功能，用户可以指定表单阅读方式，例如发送邮件、制成表格、在线阅读等。

## Zoho Mail

**Zoho Mail**（原名：**Zoho Virtual
Office**）为**Zoho**旗下几乎所有产品的整合版本，目前为正式版。Zoho
Mail目前为收费产品。由于为在线系统，因此支持几乎所有的操作系统，支持的浏览器有[IE以及](../Page/Internet_Explorer.md "wikilink")[Firefox](../Page/Firefox.md "wikilink")。并且，Zoho
Mail应用了大量的[AJAX特效](../Page/AJAX.md "wikilink")。

Zoho Mail的前身为Zoho Virtual Office，Zoho Virtual Office的最后版本为测试版。Zoho
mail包括邮件系统、写作系统、电子表格系统以及日历系统等。

## Zoho Projects

**Zoho Projects**（Zoho项目管理）为**Zoho**面对企业级用户的产品

## Zoho Sheet

**Zoho
Sheet**（**Zoho表格**）为**Zoho**旗下的[在线](../Page/在线.md "wikilink")[电子表格系统](../Page/电子表格.md "wikilink")。由于在线表格的特殊性，因此可以在各种平台的计算机上使用。Zoho
Sheet支持的浏览器有：[IE](../Page/Internet_Explorer.md "wikilink")、[Firefox以及运行于](../Page/Firefox.md "wikilink")[MAC上的](../Page/Macintosh.md "wikilink")[Sarfri](../Page/Sarfri.md "wikilink")。

Zoho Sheet创建于2004年，目的是增加Zoho的产品数量以及在未来对抗例如微软的[Microsoft
Office等](../Page/Microsoft_Office.md "wikilink")。由于Zoho
Sheet为在线系统，因此支持所有有浏览器情况下的计算机。

## Zoho Show

**Zoho
Show**（**Zoho演示文档**）为**Zoho**旗下的[在线](../Page/在线.md "wikilink")[演示系统](../Page/演示系统.md "wikilink")，使用[Flash创建](../Page/Flash.md "wikilink")。Zoho
Show同其他Zoho产品一样，拥有丰富的[API](../Page/API.md "wikilink")。

Zoho Show诞生于2006年，基于Flash技术。此外，Zoho
Show拥有大量的API，使用者可以通过调用其所提供的API来刊登其演示系统于自己的网站上。Zoho
Show的诞生对互连网有着不小的冲击，其最大竞争对手为微软Microsoft Office
Powerpoint以及尚未发布仍然在开发中的Google系列套件中的演示。

## Zoho Wiki

**Zoho
Wiki**（**Zoho维基**）为**Zoho**旗下的[维基提供](../Page/维基.md "wikilink")[网站](../Page/网站.md "wikilink")，用户可以无限量的创建、编辑自己的Zoho
Wiki。 Zoho Wiki创建于2006年，目的为扩大Zoho的产品线以及未来与相关网站对抗。

## Zoho Writer

**Zoho
Writer**（**Zoho文字编辑**）为**Zoho**旗下的[WYSIWYG](../Page/WYSIWYG.md "wikilink")[在线编辑文字系统](../Page/在线编辑文字.md "wikilink")，由于为在线编辑，因此除了[浏览器外](../Page/浏览器.md "wikilink")，支持所有的[操作系统](../Page/操作系统.md "wikilink")。支持的浏览器包括：[IE](../Page/Internet_Explorer.md "wikilink")、[Firefox等](../Page/Firefox.md "wikilink")。Zoho
Writer应用了大量的AJAX代码，并且其基本功能不劣于[微软的Microsoft](../Page/微软.md "wikilink")
Office，因此被称之为[Microsoft
Office杀手](../Page/Microsoft_Office.md "wikilink")。目前Zoho
Writer的最大竞争对手是同样在线编辑系统[Google Docs &
Sheets](../Page/Google_Docs_&_Sheets.md "wikilink")。

## 参见

  - [Microsoft Office](../Page/Microsoft_Office.md "wikilink")
  - [Google文件](../Page/Google文件.md "wikilink")

## 外部链接

  - [Zoho首页](http://www.zoho.com)

  - [Zoho Creator首页](http://creator.zoho.com)

  - [Zoho Mail首页](http://mail.zoho.com/)

  - [Zoho Projects首页](http://projects.zoho.com)

  - [Zoho Sheet首页](http://zohosheet.com)

  - [Zoho Wiki首页](http://wiki.zoho.com)

  - [Zoho
    Writer首页](https://web.archive.org/web/20081006200848/http://zohowriter.com/)

  - [Zoho官方博客](http://blogs.zoho.com/)

  - [Zoho中文论坛](https://web.archive.org/web/20080406054611/http://www.zohowriter.cn/)

[Category:网站](../Category/网站.md "wikilink")