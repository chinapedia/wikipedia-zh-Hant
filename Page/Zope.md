**Zope**是一个以[Python编写的](../Page/Python.md "wikilink")[开源](../Page/开源.md "wikilink")、[面向对象的](../Page/面向对象的程序设计.md "wikilink")[Web应用服务器](../Page/万维网.md "wikilink")。Zope代表着“Z对象发布环境（*Z
Object Publishing
Environment*）”，并且是第一个系统使用了新的通用对象发布方法的框架。\[1\]\[2\]Zope已经被认为是一个[Python的杀手级应用](../Page/Python.md "wikilink")，一个帮助Python走到聚光灯下的应用。\[3\]

在过去的几年中，Zope社区发布了几个不同类型的[Web框架](../Page/Web框架.md "wikilink")，但是他们应用相同的哲学、人生和源代码。Zope
2仍是应用最广泛的框架，这主要得益于[Plone](../Page/Plone.md "wikilink")[内容管理系统运行于Zope](../Page/内容管理系统.md "wikilink")
2之上。[BlueBream](../Page/BlueBream.md "wikilink")（早先被称作Zope 3）应用较Zope
2少，但也有许多网站运行于其上，包括[Launchpad](../Page/Launchpad.md "wikilink")。[Grok以对程序员更加友好的框架的目标开发](../Page/Grok.md "wikilink")。在2009年[BFG以Zope](../Page/BFG.md "wikilink")
principles为基础的minimalistic framework受到Zope社区的注意。

它是一个事务型的[对象数据库平台](../Page/对象数据库.md "wikilink")。Zope除了能储存内容，数据外，还能存放动态的[HTML模板](../Page/HTML.md "wikilink")、脚本、搜索引擎、[关系数据库管理系统](../Page/关系数据库管理系统.md "wikilink")（RDBMS）接口和代码。Zope裡的一切都是物件。它有一个强大的基于web的在线开发模板，使你能在世界上任何地方，任何时间方便地更新你的网站。作为对以上功能的安全保障，Zope提供了一个集成的安全模型，能方便对数据库的内容进行分层授权管理。可以把个人的内容分配给个人用户管理，部门的内容分配给部门管理员管理，整个网站的内容由系统管理员管理。现在基于Zope平台已开发出了大量的产品，能方便地嵌入Zope中以扩展它的基本功能。

Zope3是一个全新设计的Web开发架构，其中采用组件和接口技术，以实现Web应用的快速开发和布署。

## 参考来源

## 外部链接

  -
  - [Zope
    Foundation](https://web.archive.org/web/20100106083240/http://foundation.zope.org/)

  - [Zope 2](https://archive.is/20130113124015/http://zope2.zope.org/)

  - [BlueBream](http://bluebream.zope.org/) (AKA Zope 3)

      - [Zope 3 on launchpad.net](https://launchpad.net/zope3)
      - [Zope 3 demos](http://code.google.com/p/zope3demos)

  - [The Zope2 Book](http://docs.zope.org/zope2/zope2book/)

  - [The Zope3 Book](https://en.wikibooks.org/wiki/The_Zope_3_Book)

  - [Zope
    Corporation](https://web.archive.org/web/20080509071756/http://zope.com/)

  - [1](http://old.zope.org/index.html) Zope.org

      - [2](http://old.zope.org/Documentation/Books/ZopeBook/2_6Edition.11)
        The Zope Book 2.6

[Category:自由跨平台軟體](../Category/自由跨平台軟體.md "wikilink")
[Category:自由内容管理系统](../Category/自由内容管理系统.md "wikilink")
[Category:用Python編程的自由軟體](../Category/用Python編程的自由軟體.md "wikilink")

1.
2.
3.