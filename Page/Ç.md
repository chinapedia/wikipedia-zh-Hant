[Visigothic_Z-C_cedille.svg](https://zh.wikipedia.org/wiki/File:Visigothic_Z-C_cedille.svg "fig:Visigothic_Z-C_cedille.svg")
（带[软音符的](../Page/软音符.md "wikilink")[c](../Page/c.md "wikilink")）是[阿尔巴尼亚语](../Page/阿尔巴尼亚语.md "wikilink")、[土耳其语](../Page/土耳其语.md "wikilink")、[阿塞拜疆语](../Page/阿塞拜疆语.md "wikilink")、[土库曼语](../Page/土库曼语.md "wikilink")、[鞑靼语和](../Page/鞑靼语.md "wikilink")[北库尔德语的一个](../Page/北库尔德语.md "wikilink")[字母](../Page/字母.md "wikilink")。这个字母在[英语](../Page/英语.md "wikilink")、[法语](../Page/法语.md "wikilink")、[葡萄牙语](../Page/葡萄牙语.md "wikilink")、[奥克语](../Page/奥克语.md "wikilink")、[加泰罗尼亚语和一些](../Page/加泰罗尼亚语.md "wikilink")[弗留利语方言](../Page/弗留利语.md "wikilink")，也作[变音字母使用](../Page/变音符号.md "wikilink")。

Cedilla 原本来自[西班牙语](../Page/西班牙语.md "wikilink")，意即是“小的 z”，因为  下的一画原本是小的 z
字，而西班牙语的  字表示  音。但在18世纪的拼写改革之後，已完全被 z 字母所取代。

  - 在阿尔巴尼亚语、土耳其语和阿塞拜疆语，这个字母都是排在字母表的第 4 位，表示  音。
  - 在土库曼语，这个字母排在字母表的第 4 位，表示  音。
  - 在鞑靼语，这个字母排在字母表的第 5 位，表示  音。

在法语、葡萄牙语、欧西坦语、加泰罗尼亚语中，“” 是软音符号。原本 c 字在 a、o、u 字前发  音。如果要把 c 字改发  音，在 c
下面就要加上这个符号。例如法语的
*[français](../Page/wikt:fr:français.md "wikilink")*,
*[façade](../Page/wikt:fr:façade.md "wikilink")*,
*[soupçon](../Page/wikt:fr:soupçon.md "wikilink")*,
*[garçon](../Page/wikt:fr:garçon.md "wikilink")*（法语的 *façade*
后来也引进[英语使用](../Page/英语.md "wikilink")），葡萄牙语的
*[poça](../Page/wikt:pt:poça.md "wikilink")*,
*[moço](../Page/wikt:pt:moço.md "wikilink")*,
*[açúcar](../Page/wikt:pt:açúcar.md "wikilink")*，加泰罗尼亚语中，[巴塞罗那足球俱乐部的别名](../Page/巴塞罗那足球俱乐部.md "wikilink")
*[Barça](../Page/wikt:ca:Barça.md "wikilink")* 等。

在[國際音標中](../Page/國際音標.md "wikilink")，
音表示[清硬顎擦音](../Page/清硬顎擦音.md "wikilink")。

也是[非洲参考字母之一](../Page/非洲参考字母.md "wikilink")。

## 字符编码

<table>
<thead>
<tr class="header">
<th><p>字符编码</p></th>
<th><p><a href="../Page/Unicode.md" title="wikilink">Unicode</a></p></th>
<th><p><a href="../Page/ISO/IEC_8859.md" title="wikilink">ISO 8859</a>-<a href="../Page/ISO/IEC_8859-1.md" title="wikilink">1</a>，<a href="../Page/ISO/IEC_8859-2.md" title="wikilink">2</a>，<a href="../Page/ISO/IEC_8859-3.md" title="wikilink">3</a>，<br />
<a href="../Page/ISO/IEC_8859-9.md" title="wikilink">9</a>，<a href="../Page/ISO/IEC_8859-14.md" title="wikilink">14</a>，<a href="../Page/ISO/IEC_8859-15.md" title="wikilink">15</a>，<a href="../Page/ISO/IEC_8859-16.md" title="wikilink">16</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/大寫字母.md" title="wikilink">大写</a> </p></td>
<td><p>U+00C7</p></td>
<td><p>C7</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小寫字母.md" title="wikilink">小写</a> </p></td>
<td><p>U+00E7</p></td>
<td><p>E7</p></td>
</tr>
</tbody>
</table>

[CÇ](../Category/衍生拉丁字母.md "wikilink")
[ç](../Category/音標符號.md "wikilink")