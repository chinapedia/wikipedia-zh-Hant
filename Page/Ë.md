（带[分音符的](../Page/分音符.md "wikilink")[e](../Page/e.md "wikilink")）是[阿尔巴尼亚语](../Page/阿尔巴尼亚语.md "wikilink")、[荷兰语和](../Page/荷兰语.md "wikilink")[卡舒比语的一个](../Page/卡舒比语.md "wikilink")[字母](../Page/字母.md "wikilink")。这个字母在[法语中](../Page/法语.md "wikilink")，也作[变音字母使用](../Page/变音符号.md "wikilink")。

  - 在阿尔巴尼亚语中，这个字母排在字母表的第 8 位，表示  音。
  - 在卡舒比语中，这个字母排在字母表的第 9 位，表示  音。
  - 在[法语](../Page/法语.md "wikilink")，使用
    的字有：*[noël](../Page/wikt:en:noël.md "wikilink")*.
  - 在[英語中](../Page/英語.md "wikilink")，此字母代表該e需獨自發音，而非屬於雙字母的一部份。

此字母也可以做为
<span style="font-size:120%" lang="ru">[Ё](../Page/Е.md "wikilink")
</span>的罗马字母化

## 字符编码

<table>
<thead>
<tr class="header">
<th><p>字符编码</p></th>
<th><p><a href="../Page/Unicode.md" title="wikilink">Unicode</a></p></th>
<th><p><a href="../Page/ISO/IEC_8859.md" title="wikilink">ISO 8859</a>-<a href="../Page/ISO/IEC_8859-1.md" title="wikilink">1</a>，<a href="../Page/ISO/IEC_8859-2.md" title="wikilink">2</a>，<a href="../Page/ISO/IEC_8859-3.md" title="wikilink">3</a>，<a href="../Page/ISO/IEC_8859-4.md" title="wikilink">4</a>，<br />
<a href="../Page/ISO/IEC_8859-9.md" title="wikilink">9</a>，<a href="../Page/ISO/IEC_8859-10.md" title="wikilink">10</a>，<a href="../Page/ISO/IEC_8859-14.md" title="wikilink">14</a>，<a href="../Page/ISO/IEC_8859-15.md" title="wikilink">15</a>，<a href="../Page/ISO/IEC_8859-16.md" title="wikilink">16</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/大寫字母.md" title="wikilink">大写</a> </p></td>
<td><p>U+00CB</p></td>
<td><p>CB</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小寫字母.md" title="wikilink">小写</a> </p></td>
<td><p>U+00EB</p></td>
<td><p>EB</p></td>
</tr>
</tbody>
</table>

## 参看

  - <span style="font-size:120%" lang="ru">[Ё
    ё](../Page/Ё.md "wikilink")</span>（[西里尔字母](../Page/西里尔字母.md "wikilink")）
  - <span style="font-size:120%" lang="ru">[Ӭ
    ӭ](../Page/Ӭ.md "wikilink")</span>（西里尔字母）

[EË](../Category/衍生拉丁字母.md "wikilink")