（带[尖音符的](../Page/尖音符.md "wikilink")[i](../Page/i.md "wikilink")）是[法罗语](../Page/法罗语.md "wikilink")、[匈牙利语](../Page/匈牙利语.md "wikilink")、[冰岛语](../Page/冰岛语.md "wikilink")、[捷克语](../Page/捷克语.md "wikilink")、[斯洛伐克语和](../Page/斯洛伐克语.md "wikilink")[鞑靼语的一个](../Page/鞑靼语.md "wikilink")[字母](../Page/字母.md "wikilink")。这个字母在[加泰罗尼亚语](../Page/加泰罗尼亚语.md "wikilink")、[爱尔兰语](../Page/爱尔兰语.md "wikilink")、[葡萄牙语](../Page/葡萄牙语.md "wikilink")、[西班牙语](../Page/西班牙语.md "wikilink")、[越南语中](../Page/越南语.md "wikilink")，也作[变音字母使用](../Page/变音符号.md "wikilink")。

  - 在法罗语中，这个字母排在字母表的第 11 位，表示  音。
  - 在匈牙利语中，这个字母排在字母表的第 16 位，表示  音（长／i／音）。
  - 在冰岛语中，这个字母排在字母表的第 12 位，表示  音。
  - 在捷克语中，这个字母排在字母表的第 16 位，表示  音。
  - 在斯洛伐克语中，这个字母排在字母表的第 18 位，表示  音。
  - 在鞑靼语中，这个字母排在字母表的第 14 位，表示  音。
  - 在[越南语音系中](../Page/越南语音系.md "wikilink")， 是 i 的锐声（阴去声）。
  - 在[汉语拼音中](../Page/汉语拼音.md "wikilink")，
    作为[韵母](../Page/韵母.md "wikilink") i 的阳平声。

## 字符编码

<table>
<thead>
<tr class="header">
<th><p>字符编码</p></th>
<th><p><a href="../Page/Unicode.md" title="wikilink">Unicode</a></p></th>
<th><p><a href="../Page/ISO/IEC_8859.md" title="wikilink">ISO 8859</a>-<a href="../Page/ISO/IEC_8859-1.md" title="wikilink">1</a>、<a href="../Page/ISO/IEC_8859-2.md" title="wikilink">2</a>、<a href="../Page/ISO/IEC_8859-3.md" title="wikilink">3</a>、<a href="../Page/ISO/IEC_8859-4.md" title="wikilink">4</a>、<br />
<a href="../Page/ISO/IEC_8859-9.md" title="wikilink">9</a>、<a href="../Page/ISO/IEC_8859-10.md" title="wikilink">10</a>、<a href="../Page/ISO/IEC_8859-14.md" title="wikilink">14</a>、<a href="../Page/ISO/IEC_8859-15.md" title="wikilink">15</a>、<a href="../Page/ISO/IEC_8859-16.md" title="wikilink">16</a></p></th>
<th><p><a href="../Page/VISCII.md" title="wikilink">VISCII</a></p></th>
<th><p><a href="../Page/GB_2312.md" title="wikilink">GB 2312</a></p></th>
<th><p><a href="../Page/香港增補字符集.md" title="wikilink">HKSCS</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/大寫字母.md" title="wikilink">大写</a> </p></td>
<td><p>U+00CD</p></td>
<td><p>CD</p></td>
<td><p>CD</p></td>
<td><p>/</p></td>
<td><p>/</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小寫字母.md" title="wikilink">小写</a> </p></td>
<td><p>U+00ED</p></td>
<td><p>ED</p></td>
<td><p>ED</p></td>
<td><p>A8AA</p></td>
<td><p>8871</p></td>
</tr>
</tbody>
</table>

[IÍ](../Category/衍生拉丁字母.md "wikilink")