（[u](../Page/u.md "wikilink")-[macron](../Page/長音符號.md "wikilink")）是[拉脱维亚语和](../Page/拉脱维亚语.md "wikilink")[立陶宛语的一个](../Page/立陶宛语.md "wikilink")[字母](../Page/字母.md "wikilink")。

  - 在拉脱维亚语中，这个字母排在字母表的第 30 位，表示  音。
  - 在立陶宛语中，这个字母排在字母表的第 28 位，表示  音。
  - 在[汉语拼音中](../Page/汉语拼音.md "wikilink")，
    作为[韵母](../Page/韵母.md "wikilink") u 的阴平声。

在一些语言中， 代表长的 u
音，包括[毛利语](../Page/毛利语.md "wikilink")、[拉丁语和](../Page/拉丁语.md "wikilink")[日语罗马字等](../Page/日语罗马字.md "wikilink")。

## 字符编码

| 字符编码                             | [Unicode](../Page/Unicode.md "wikilink") | [ISO 8859](../Page/ISO/IEC_8859.md "wikilink")-[4](../Page/ISO/IEC_8859-4.md "wikilink") | ISO 8859-[10](../Page/ISO/IEC_8859-10.md "wikilink") | ISO 8859-[13](../Page/ISO/IEC_8859-13.md "wikilink") | [GB 2312](../Page/GB_2312.md "wikilink") | [HKSCS](../Page/香港增補字符集.md "wikilink") |
| -------------------------------- | ---------------------------------------- | ---------------------------------------------------------------------------------------- | ---------------------------------------------------- | ---------------------------------------------------- | ---------------------------------------- | -------------------------------------- |
| [大写](../Page/大寫字母.md "wikilink") | U+016A                                   | DE                                                                                       | AE                                                   | DB                                                   | /                                        | /                                      |
| [小写](../Page/小寫字母.md "wikilink") | U+016B                                   | FE                                                                                       | BE                                                   | FB                                                   | A8B1                                     | 8878                                   |

## 参看

  - <span lang="ru" style="font-size:120%;">[Ӯ
    ӯ](../Page/Ӯ.md "wikilink")</span>（[西里尔字母](../Page/西里尔字母.md "wikilink")）

[UŪ](../Category/衍生拉丁字母.md "wikilink")