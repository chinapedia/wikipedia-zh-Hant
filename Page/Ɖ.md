****,
****（[d](../Page/d.md "wikilink")-tail）是扩展[拉丁字母之一](../Page/拉丁字母.md "wikilink")，小写字母在[國際音標中代表](../Page/國際音標.md "wikilink")[濁捲舌塞音](../Page/濁捲舌塞音.md "wikilink")。它也是[非洲参考字母之一](../Page/非洲参考字母.md "wikilink")，在[埃维语](../Page/埃维语.md "wikilink")（Ewe
/ Eʋegbe）等非洲语言中，使用了这个字母。
值得注意的是，[Ðð](../Page/Ð.md "wikilink")、[Đđ](../Page/Đ.md "wikilink")、三組字母的大寫[寫法完全一樣](../Page/Đ_\(消歧義\).md "wikilink")。

## 編碼

<table>
<caption>Retroflex D on computers</caption>
<thead>
<tr class="header">
<th><p>字母</p></th>
<th><p><a href="../Page/Unicode.md" title="wikilink">Unicode</a></p></th>
<th><p>名稱</p></th>
<th><p><a href="../Page/字元值參照.md" title="wikilink">字元值參照</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><em>Latin Extended-B</em>[1]</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Ɖ</p></td>
<td><p>U+0189</p></td>
<td><p>LATIN CAPITAL LETTER AFRICAN D</p></td>
<td><p>Ɖ<br />
Ɖ</p></td>
</tr>
<tr class="odd">
<td><p><em>IPA Extensions</em>[2]</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>ɖ</p></td>
<td><p>U+0256</p></td>
<td><p>LATIN SMALL LETTER D WITH TAIL (retroflex hook)</p></td>
<td><p>ɖ<br />
ɖ</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參見

  - [ð](../Page/ð.md "wikilink")
  - [đ](../Page/đ.md "wikilink")
  - [D](../Page/D.md "wikilink")

## 參考資料

## 外部链接

  - <http://sumale.vjf.cnrs.fr/phono/Reclangues1N.php?ChoixCellule=C12NL>

[DƉ](../Category/衍生拉丁字母.md "wikilink")
[ɖ](../Category/音標符號.md "wikilink")

1.
2.