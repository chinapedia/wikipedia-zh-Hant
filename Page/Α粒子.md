[Alphaparticlemagnetic.svg](https://zh.wikipedia.org/wiki/File:Alphaparticlemagnetic.svg "fig:Alphaparticlemagnetic.svg")
[Alpha_Decay.svg](https://zh.wikipedia.org/wiki/File:Alpha_Decay.svg "fig:Alpha_Decay.svg")
[Alfa_beta_gamma_radiation.svg](https://zh.wikipedia.org/wiki/File:Alfa_beta_gamma_radiation.svg "fig:Alfa_beta_gamma_radiation.svg")的原子核可被紙所阻擋，β粒子相當於[電子可被鋁箔所阻擋](../Page/電子.md "wikilink")，γ射線則具有高穿透性。\]\]

**α粒子**是一種放射性粒子，由兩個[質子及兩個](../Page/質子.md "wikilink")[中子組成](../Page/中子.md "wikilink")，並不帶任何電子，亦即等同于**[氦-4](../Page/氦-4.md "wikilink")**的原子核，或電離化後的氦-4，He<sup>2+</sup>。

通常具有[放射性而](../Page/放射性.md "wikilink")[原子量较大的化学](../Page/原子量.md "wikilink")[元素](../Page/元素.md "wikilink")，会透過[α衰变放射出α粒子](../Page/α衰变.md "wikilink")，從而變成較輕的元素，直至該元素穩定為止。由於α粒子的[體積比較大](../Page/體積.md "wikilink")，又帶兩個正電荷，很容易就可以[電離其他物質](../Page/電離.md "wikilink")。因此，它的能量亦散失得較快，穿透能力在眾多電離輻射中是最弱的，人類的[皮膚或一張紙已能隔阻α粒子](../Page/皮膚.md "wikilink")。

## 危害

α粒子釋放出的[放射性同位素在人體外部不構成危險](../Page/放射性同位素.md "wikilink")。然而，釋放α粒子的物質（[鐳](../Page/鐳.md "wikilink")、[鈾等](../Page/鈾.md "wikilink")）一旦被吸入或注入人體內，那将會十分危险，α粒子能直接破壞內臟的[細胞](../Page/細胞.md "wikilink")。它的穿透力虽然弱，但由于它的[电离能力很强](../Page/电离.md "wikilink")，所以它对生物所造成的危害并不亞于其他辐射。快速衰變的重金屬型[毒藥就是利用α粒子的毒性作下毒基礎](../Page/毒藥.md "wikilink")\[1\]。

## 著名实验

1911年，[漢斯·蓋革和恩內斯特](../Page/漢斯·蓋革.md "wikilink")·馬斯登在[盧瑟福指導下](../Page/欧内斯特·卢瑟福.md "wikilink")，使用α粒子轰击金箔，发现大多数α粒子穿过金箔后仍保持原来运动方向，但有极少数α粒子发生较大角度的偏转。由此卢瑟福确定了原子核的存在。\[2\]

## 參見

  - [電子](../Page/電子.md "wikilink")
  - [氦-4](../Page/氦-4.md "wikilink")

## 參考資料

### 注釋

<references group="注" />

### 參考文獻

[Category:放射線學](../Category/放射線學.md "wikilink")
[Category:突變原](../Category/突變原.md "wikilink")
[Category:氦的同位素](../Category/氦的同位素.md "wikilink")
[Category:氦](../Category/氦.md "wikilink")

1.  參見[亞歷山大·瓦爾杰洛維奇·利特維年科被下毒案](../Page/亞歷山大·瓦爾杰洛維奇·利特維年科.md "wikilink")
2.