__NOTOC__  **М**, **м**（称呼为 эм,
*em*）是一个广泛用在斯拉夫语言的[西里尔字母](../Page/西里尔字母.md "wikilink")，由希腊字母
[Μ](../Page/Μ.md "wikilink") 演变而成。

此字母的小写 м 是缩小了的大写 М ，留意
<span style="font-size:large;font-family:Times New Roman">**</span>
是斜体的小写 [Т](../Page/Т.md "wikilink")。

## 字母的次序

在[俄语](../Page/俄语.md "wikilink")、[白俄罗斯语字母中排第](../Page/白俄罗斯语.md "wikilink")14位，在[乌克兰语字母中排第](../Page/乌克兰语.md "wikilink")17位，在[保加利亚语字母中排第](../Page/保加利亚语.md "wikilink")13位，在[塞尔维亚语字母中排第](../Page/塞尔维亚语.md "wikilink")15位，在[马其顿语字母中排第](../Page/马其顿语.md "wikilink")16位。

## 音值

通常为 ，在[腭化](../Page/顎音化.md "wikilink")[元音前代表](../Page/元音.md "wikilink") 。

## 字符编码

| 字符编码                               | [Unicode](../Page/Unicode.md "wikilink") | [ISO 8859-5](../Page/ISO/IEC_8859-5.md "wikilink") | {{tsl|en|KOI-8 | KOI-8}} | [GB 2312](../Page/GB_2312.md "wikilink") | [HKSCS](../Page/香港增補字符集.md "wikilink") |
| ---------------------------------- | ---------------------------------------- | -------------------------------------------------- | -------------- | ------- | ---------------------------------------- | -------------------------------------- |
| [大写](../Page/大寫字母.md "wikilink") М | U+041C                                   | BC                                                 | ED             | A7AE    | C841                                     |                                        |
| [小写](../Page/小寫字母.md "wikilink") м | U+043C                                   | DC                                                 | CD             | A7DE    | C862                                     |                                        |

## 參考文獻

  -
  -
## 参看

  - <span lang="el" style="font-size:120%;">[Μ
    μ](../Page/Μ.md "wikilink")</span>（[希腊字母](../Page/希腊字母.md "wikilink")）
  - <span lang="en" style="font-size:120%;">[M
    m](../Page/M.md "wikilink")</span>（[拉丁字母](../Page/拉丁字母.md "wikilink")）

## 外部連結

  -
  -
  - [Буква М](http://graphemica.com/М) на сайте graphemica.com

  - [Буква м](http://graphemica.com/м) на сайте graphemica.com

[Category:西里尔字母](../Category/西里尔字母.md "wikilink")