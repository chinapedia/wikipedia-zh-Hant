****,
****（）是一个[西里尔字母](../Page/西里尔字母.md "wikilink")，它源于与的[连字Ꙑ](../Page/连字.md "wikilink")。在[俄语和](../Page/俄语.md "wikilink")[白俄罗斯语中放在非](../Page/白俄罗斯语.md "wikilink")[顎音化](../Page/顎音化.md "wikilink")（硬音）辅音之後的。因为发音的缘故，在硬音辅音之後的被紧缩成。

俄语中不会出现在字首，但有一些例外，如动词（去发Ы的音），以及数个专有名词或外来词，如：粤语拼音[吴姓Ын](../Page/吴姓.md "wikilink")，[萨哈共和国的村落](../Page/萨哈共和国.md "wikilink")（Ytyk-Kiuiol）、萨哈共和国的河流（Ygyatta）、隋朝时[高句丽將軍](../Page/高句丽.md "wikilink")
（[乙支文德](../Page/乙支文德.md "wikilink")，Eulchi
Mundok）。而在[古西伯利亞語言之](../Page/古西伯利亞語言.md "wikilink")[楚科奇语](../Page/楚科奇语.md "wikilink")、[科里亚克语等語言用](../Page/科里亚克语.md "wikilink")
Ы 來表示 [Ә](../Page/Ә.md "wikilink")。

## 字母的次序

在[俄语字母中排第](../Page/俄语.md "wikilink")29位，在[白俄罗斯语字母中排第](../Page/白俄罗斯语.md "wikilink")28位。

## 音值

在教会斯拉夫语通常为，在俄语及白俄罗斯语为，在[唇音](../Page/唇音.md "wikilink")之後作。在一些突厥语族和蒙古语族语言表示或。

## 转写

在[转写成拉丁字母时](../Page/转写.md "wikilink")，通常转写作*y*。

## 字符编码

## 参看

  - （西里尔字母）

## 外部連結

  -
  -
[Category:西里尔字母](../Category/西里尔字母.md "wikilink")