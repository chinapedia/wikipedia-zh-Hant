**ㄅ**（[bpmf-b.svg](https://zh.wikipedia.org/wiki/File:bpmf-b.svg "fig:bpmf-b.svg")）是[注音符號中的](../Page/注音符號.md "wikilink")[聲母之一](../Page/聲母.md "wikilink")，字形取自「包」的古字「[勹](../Page/勹.md "wikilink")」字，發音則取「包」字的聲母。《[說文解字](../Page/說文解字.md "wikilink")》：「勹，裹也，象人曲行，有所包裹。」

「ㄅ」的發音為[不送氣清雙唇塞音](../Page/清雙唇塞音.md "wikilink")//，不過單講「ㄅ」這個符號時通常會發為「ㄅㄛ」或「ㄅㄜ」。

在[漢語拼音](../Page/漢語拼音.md "wikilink")、[通用拼音](../Page/通用拼音.md "wikilink")、[粵拼中對應的為](../Page/粵拼.md "wikilink")「b」，[威妥瑪拼音](../Page/威妥瑪拼音.md "wikilink")、[臺羅拼音等則為](../Page/臺羅拼音.md "wikilink")「p」。

## 相關變體

在注音符號的擴充版本[閏音符號中](../Page/閏音符號.md "wikilink")，增加了「ㆠ」（[bpmf-bb.svg](https://zh.wikipedia.org/wiki/File:bpmf-bb.svg "fig:bpmf-bb.svg")）作為「ㄅ」的濁音對應，寫法為最末筆收個圓但不突出。[臺灣方音符號亦從之](../Page/臺灣方音符號.md "wikilink")。「ㆠ」發音為[濁雙唇塞音](../Page/濁雙唇塞音.md "wikilink")//，可用於[蘇州話](../Page/蘇州話.md "wikilink")、[廈門話](../Page/廈門話.md "wikilink")、[松江話](../Page/松江話.md "wikilink")、[浦東話](../Page/浦東話.md "wikilink")、[粵語](../Page/粵語.md "wikilink")[勾漏片和](../Page/勾漏片.md "wikilink")[封開話](../Page/封開話.md "wikilink")、臺灣話、[中古漢語等有濁雙脣塞音的漢語](../Page/中古漢語.md "wikilink")，在臺羅拼音中對應的即為「b」。[方音注音符號則改作](../Page/方音注音符號.md "wikilink")「[bpmf-b.svg](https://zh.wikipedia.org/wiki/File:bpmf-b.svg "fig:bpmf-b.svg")[bpmf-voiced.svg](https://zh.wikipedia.org/wiki/File:bpmf-voiced.svg "fig:bpmf-voiced.svg")」形。

此外，方音注音符號及臺灣方音符號還有「ㆴ」（[bpmf-_b.svg](https://zh.wikipedia.org/wiki/File:bpmf-_b.svg "fig:bpmf-_b.svg")），為縮小版的「ㄅ」，寫在韻母元音後，用於雙脣[入聲](../Page/入聲.md "wikilink")（//）。在臺灣方音符號裏，其上多一點表示陽入，無點則為陰入。它在粵拼、臺羅拼音對應為寫在字末的\[p\]，如「立」的臺語發音為//，寫為「ㄌㄧㆴ‧」、\[li̍p\]。此符號在閏音符號中寫作「[bpmf-_p.svg](https://zh.wikipedia.org/wiki/File:bpmf-_p.svg "fig:bpmf-_p.svg")」，見[ㄆ條目](../Page/ㄆ.md "wikilink")。

閏音符號還有「[bpmf-bf.svg](https://zh.wikipedia.org/wiki/File:bpmf-bf.svg "fig:bpmf-bf.svg")」號，為「ㄅㄈ」的合音、[不送氣清唇齒塞擦音](../Page/清唇齒塞擦音.md "wikilink")//，用於[西安話](../Page/西安話.md "wikilink")「朱」之聲母。

## 現代用法

在官話青少年的用語中，「ㄅ」也指「吧」、「不」、「掰」的諧音。例如「好ㄅ」等同於「好吧」，「ㄅㄅ」即是「掰掰」。

## 編碼與拼音

  - 以下漢語拼音包含擴充版本[閩南方言拼音方案](../Page/閩南方言拼音方案.md "wikilink")。

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
<col style="width: 0%" />
<col style="width: 0%" />
<col style="width: 0%" />
<col style="width: 0%" />
<col style="width: 0%" />
<col style="width: 0%" />
<col style="width: 0%" />
<col style="width: 0%" />
</colgroup>
<thead>
<tr class="header">
<th><p>字元</p></th>
<th><p>圖檔</p></th>
<th><p>編碼</p></th>
<th><p><a href="../Page/國際音標.md" title="wikilink">國際音標</a></p></th>
<th><p><a href="../Page/漢語拼音.md" title="wikilink">漢語拼音</a></p></th>
<th><p><a href="../Page/威妥瑪拼音.md" title="wikilink">威妥瑪拼音</a></p></th>
<th><p><a href="../Page/臺羅拼音.md" title="wikilink">臺羅拼音</a></p></th>
<th><p><a href="../Page/通用拼音.md" title="wikilink">通用拼音</a></p></th>
<th><p><a href="../Page/粵拼.md" title="wikilink">粵拼</a></p></th>
<th><p><a href="../Page/黔东苗文.md" title="wikilink">拉丁苗文</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/Unicode.md" title="wikilink">Unicode</a></p></td>
<td><p><a href="../Page/Big5.md" title="wikilink">Big5</a></p></td>
<td><p><a href="../Page/GB_2312.md" title="wikilink">GB 2312</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>ㄅ</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:bpmf-b.svg" title="fig:bpmf-b.svg">bpmf-b.svg</a></p></td>
<td><p>U+3105</p></td>
<td><p>A374</p></td>
<td><p>A8C5</p></td>
<td></td>
<td><p>b</p></td>
<td><p>p</p></td>
<td><p>p</p></td>
<td><p>b</p></td>
</tr>
<tr class="odd">
<td><p>ㆠ</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:bpmf-bb.svg" title="fig:bpmf-bb.svg">bpmf-bb.svg</a></p></td>
<td><p>U+31A0</p></td>
<td><p>未收錄</p></td>
<td><p>未收錄</p></td>
<td></td>
<td><p>bb</p></td>
<td></td>
<td><p>b</p></td>
<td><p>bh/bb[1]</p></td>
</tr>
<tr class="even">
<td><p>ㄅﾞ</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:bpmf-b.svg" title="fig:bpmf-b.svg">bpmf-b.svg</a><a href="https://zh.wikipedia.org/wiki/File:bpmf-voiced.svg" title="fig:bpmf-voiced.svg">bpmf-voiced.svg</a></p></td>
<td><p>未收錄</p></td>
<td><p>未收錄</p></td>
<td><p>未收錄</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>ㆴ</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:bpmf-_b.svg" title="fig:bpmf-_b.svg">bpmf-_b.svg</a></p></td>
<td><p>U+31B4</p></td>
<td><p>未收錄</p></td>
<td><p>未收錄</p></td>
<td></td>
<td><p>-p</p></td>
<td></td>
<td><p>-p</p></td>
<td><p>-p</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:bpmf-bf.svg" title="fig:bpmf-bf.svg">bpmf-bf.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:bpmf-bf.svg" title="fig:bpmf-bf.svg">bpmf-bf.svg</a></p></td>
<td><p>未收錄</p></td>
<td><p>未收錄</p></td>
<td><p>未收錄</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 註釋

## 參考資料

  - 國語統一籌備委員會：《注音符號總表》，北平：國語統一籌備委員會，1932年4月。
  - 中国文字改革研究委员会秘书处拼音方案工作组：《全国主要方言区方音对照表》，北京：中华书局，1954年12月。
  - 吳守禮：《國臺對照活用辭典》，臺北：遠流，2000年8月。

[Category:注音符號](../Category/注音符號.md "wikilink")

1.  在臺語通用拼音裡寫作\[bh\]，在客語通用拼音裡寫作\[bb\]。