《**一個都不能少**》是一部1999年上映的[劇情片](../Page/劇情片.md "wikilink")，由[張藝謀执导](../Page/張藝謀.md "wikilink")，改编自[施祥生](../Page/施祥生.md "wikilink")1997年的小说《天上有个太阳》\[1\]\[2\]。该片使用一班非专业演員製作一齣像[纪录片的電影](../Page/纪录片.md "wikilink")，故事主題是關於[農村](../Page/農村.md "wikilink")、[貧窮及](../Page/貧窮.md "wikilink")[文盲的問題](../Page/文盲.md "wikilink")，在该片中張藝謀保留了演員本身的名字。本片獲得十項國際電影獎項，包括[金鸡奖](../Page/金鸡奖.md "wikilink")、[圣保罗国际电影节和](../Page/圣保罗国际电影节.md "wikilink")[威尼斯電影節的金狮奖](../Page/威尼斯電影節.md "wikilink")。

## 剧情

故事圍繞一名十三歲女生—魏敏芝展开，她被任命到中國大陆河北山區一間偏远學校去做代课老師。前任老师临走前吩咐魏敏芝不要失去任何一個學生，这些学生“一个也不能少”。但其中一個男生張慧科因为家境的问题而辍学到大城市裡尋找工作，所以魏敏芝便到城市里找他，可是却得知张慧科在车站走失，后来便描述敏芝千辛万苦寻人的感人过程，最后在电视台的帮助下顺利寻回，并在各界的捐助下，这所偏远小学也因此得以重生。

## 演员

其中的演员魏敏芝和張慧科并不是专业的电影演员，而是真实的农村孩子，他們来自于[河北省](../Page/河北省.md "wikilink")[张家口市](../Page/张家口市.md "wikilink")[赤城县](../Page/赤城县.md "wikilink")[镇宁堡乡水泉村](../Page/镇宁堡乡.md "wikilink")，后来电影名声大噪，魏敏芝的母校“镇宁堡学校”和电影拍摄地“水泉小学”也因此为人熟知。

此后，这两人离开了她（他）们从小生活的小山区，到河北省[石家庄市上学](../Page/石家庄.md "wikilink")。魏敏芝曾经考过[北京电影学院](../Page/北京电影学院.md "wikilink")，大學[高考后](../Page/高考.md "wikilink")，进入了由[西部电影集团和](../Page/西部电影集团.md "wikilink")[西安外国语大学联合创办的](../Page/西安外国语大学.md "wikilink")[西外西影影视传媒学院](../Page/西外西影影视传媒学院.md "wikilink")。

## 参考资料

### 脚注

### 参考书目

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 外部連結

  - [一個都不能少（新浪）](http://eladies.sina.com.cn/yige)
  - [魏敏芝的网志](http://blog.sina.com.cn/m/weiminzhi)
  - [西影影视传媒学院](https://web.archive.org/web/20090407065736/http://xiying.xisu.edu.cn/)

[分类:小学背景电影](../Page/分类:小学背景电影.md "wikilink")
[分类:新画面电影](../Page/分类:新画面电影.md "wikilink")

[Category:张艺谋电影](../Category/张艺谋电影.md "wikilink")
[Category:1990年代剧情片](../Category/1990年代剧情片.md "wikilink")
[Category:中国电影作品](../Category/中国电影作品.md "wikilink")
[Category:1999年电影](../Category/1999年电影.md "wikilink")
[Category:河北背景电影](../Category/河北背景电影.md "wikilink")
[Category:河北取景电影](../Category/河北取景电影.md "wikilink")
[Category:金獅獎獲獎電影](../Category/金獅獎獲獎電影.md "wikilink")
[Category:金鸡奖最佳导演获奖电影](../Category/金鸡奖最佳导演获奖电影.md "wikilink")
[Category:百花奖最佳故事片](../Category/百花奖最佳故事片.md "wikilink")
[Category:北京大学生电影节最佳影片](../Category/北京大学生电影节最佳影片.md "wikilink")
[Category:贫穷题材电影](../Category/贫穷题材电影.md "wikilink")
[Category:中国农村题材作品](../Category/中国农村题材作品.md "wikilink")
[Category:社会现实主义](../Category/社会现实主义.md "wikilink")

1.
2.  .