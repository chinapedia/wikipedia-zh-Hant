**一國一城令**是日本在[元和元年](../Page/元和_\(後水尾天皇\).md "wikilink")（1615年）閏6月13日由[江戶幕府所發佈的](../Page/江戶幕府.md "wikilink")[命令](../Page/命令.md "wikilink")。在一國（此處的「國」是指[令制國](../Page/令制國.md "wikilink")，或大名的領國（之後的[藩](../Page/藩_\(日本\).md "wikilink")））中，由大名所居住作為政廳所在的[城只能保留一個](../Page/城_\(日本\).md "wikilink")，其餘的城必須全部廢除。

## 具體例子

在一個令制國由複數的大名分割領有的情形下，各大名各有一城（例：[伊予國的](../Page/伊予國.md "wikilink")[大洲城](../Page/大洲城.md "wikilink")、[松山城](../Page/松山城_\(伊予國\).md "wikilink")、[宇和島城等](../Page/宇和島城.md "wikilink")）、一個大名家領有複数的令制國時，領有的各令制国各有一城。（例：[藤堂家的](../Page/津藩.md "wikilink")[津城](../Page/津城.md "wikilink")（[伊勢國](../Page/伊勢國.md "wikilink")）、[上野城](../Page/上野城.md "wikilink")（[伊賀國](../Page/伊賀國.md "wikilink")）)

## 例外

此一法制並非畫一的實施，而是有彈性的運用。

### 毛利家的例子

[毛利家擁有](../Page/毛利家.md "wikilink")[周防國](../Page/周防國.md "wikilink")、[長門國兩令制國](../Page/長門國.md "wikilink")，但只有一城。根據[山本博文所著](../Page/山本博文.md "wikilink")「」記載，毛利家向幕府報告保留長門國的[萩城](../Page/萩城.md "wikilink")，[岩國城受到廢除](../Page/岩國城.md "wikilink")，而幕府的反應是「毛利家有周防國與長門國兩國，因此周防國的岩國城應該沒有廢除的必要。（）」。毛利家内部基於[支藩統制上必要](../Page/支藩.md "wikilink")，因而先行廢除。

### 其他例外

[仙台藩](../Page/仙台藩.md "wikilink")[白石城與](../Page/白石城.md "wikilink")[熊本藩](../Page/熊本藩.md "wikilink")[八代城等](../Page/八代城.md "wikilink")，雖為大名的家臣，但因對幕府有功績，其子孫的居城例外不屬於廢城的對象。

[秋田藩的](../Page/秋田藩.md "wikilink")[大館城與](../Page/大館城.md "wikilink")[横手城](../Page/横手城.md "wikilink")（於[戊辰戰爭中燒毀](../Page/戊辰戰爭.md "wikilink")）屬於對象外，包含[久保田城在內](../Page/久保田城.md "wikilink")，被允許擁有3城。這是屬於大大名的佐竹氏[轉封至秋田的抵押](../Page/轉封.md "wikilink")，亦有一說為政情不安定地區被允許擁有城池。

## 實施的意義

此後原先在[安土桃山時代數量約](../Page/安土桃山時代.md "wikilink")3000的城遽減至300，結果家臣團與領民進一步聚集於城下町。
其目的是德川家為了強化支配全國而加強統制[大名的勢力](../Page/大名.md "wikilink")，特別是在[外樣大名較多的西國實施較徹底](../Page/外樣大名.md "wikilink")。

## 關連項目

  - [武家諸法度](../Page/武家諸法度.md "wikilink")

[Category:江户时代政治](../Category/江户时代政治.md "wikilink")
[Category:日本城堡](../Category/日本城堡.md "wikilink")
[Category:江戶幕府](../Category/江戶幕府.md "wikilink")