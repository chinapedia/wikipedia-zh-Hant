**一心路（Yisin
Rd.）**為[高雄市的東西向重要道路](../Page/高雄市.md "wikilink")，本道路共分為二個部份。東起[凱旋路接](../Page/凱旋路_\(高雄市\).md "wikilink")[瑞隆路](../Page/瑞隆路.md "wikilink")，西至[中山二路與](../Page/中山路_\(高雄市\).md "wikilink")[三多三路口](../Page/三多路_\(高雄市\).md "wikilink")，僅經[前鎮一區](../Page/前鎮區.md "wikilink")。

## 行經行政區域

  - [前鎮區](../Page/前鎮區.md "wikilink")

## 分段

一心路共分為二個部份：

  - 一心一路；東起於[凱旋三路口接](../Page/凱旋路_\(高雄市\).md "wikilink")[瑞隆路](../Page/瑞隆路.md "wikilink")，西於[民權二路口接一心二路](../Page/民權路_\(高雄市\).md "wikilink")
  - 一心二路：東於民權二路口接一心一路，西止於[中山二路與](../Page/中山路_\(高雄市\).md "wikilink")[三多三路口](../Page/三多路_\(高雄市\).md "wikilink")。

## 歷史

語出自《[書經](../Page/書經.md "wikilink")‧秦誓》記載：「予臣三千惟一心。」、《[管子](../Page/管子.md "wikilink")‧法禁》：「武王有臣三千而一心」。取「萬眾一心」之意。[周武王朝臣雖然多達數千名](../Page/周武王.md "wikilink")，但是都能團結一條心，所以國家必然昌盛。

2014年7月31日深夜因發生[高雄氣爆事故的影響](../Page/2014年高雄氣爆事故.md "wikilink")，造成整條一心一路（從[凱旋路口至](../Page/凱旋路_\(高雄市\).md "wikilink")[光華路口](../Page/光華路_\(高雄市\).md "wikilink")）無法通行。歷經3個月重建的日夜趕工之後，於同年11月20日午夜12時完工同時開放通車\[1\]。

## 沿線設施

（由東至西）

  - 一心一路
      - [高雄市政府警察局前鎮分局](../Page/高雄市政府警察局.md "wikilink")
          - 一心路派出所
      - 新正薪醫院
      - [高雄市前鎮區民權國小](http://www.mchps.kh.edu.tw/)
      - [麥當勞高雄一心店](../Page/麥當勞.md "wikilink")
      - 光華公園
      - [土地銀行前鎮分行](../Page/土地銀行.md "wikilink")
      - [台灣中油加油站民權路站](../Page/台灣中油.md "wikilink")
      - [車之輪汽車百貨民權門市](../Page/車之輪汽車百貨.md "wikilink")
      - [籬仔內站](../Page/籬仔內站.md "wikilink")（[高雄捷運環狀輕軌](../Page/高雄捷運環狀輕軌.md "wikilink")）
      - [高雄市公共腳踏車租賃系統](../Page/高雄市公共腳踏車租賃系統.md "wikilink")
          - 光華公園站：[前鎮區民裕街與一心一路口](../Page/前鎮區.md "wikilink")
      - [日產汽車裕昌汽車一心服務廠](../Page/日產汽車.md "wikilink")
      - [英菲尼迪一心展示中心](../Page/英菲尼迪.md "wikilink")
      - [三菱汽車順益汽車一心服務廠](../Page/三菱汽車.md "wikilink")
      - [馬自達右達汽車一心所](../Page/馬自達.md "wikilink")
      - 竹南里里辦公室

<!-- end list -->

  - 一心二路
      - [中國信託南高雄分行](../Page/中國信託.md "wikilink")
      - [華南銀行前鎮分行](../Page/華南銀行.md "wikilink")
      - [高雄市第三信用合作社一心分社](../Page/高雄市第三信用合作社.md "wikilink")
      - [屈臣氏一心店](../Page/屈臣氏.md "wikilink")
      - [全聯福利中心一心店](../Page/全聯福利中心.md "wikilink")
      - [合作金庫銀行一心路分行](../Page/合作金庫銀行.md "wikilink")
      - 捷運[三多商圈站](../Page/三多商圈站.md "wikilink")

## 公車資訊

### [高雄市市區公車](../Page/高雄市市區公車.md "wikilink")

站牌名為《一心一路》

  - [漢程客運](../Page/漢程客運.md "wikilink")：
      - **<font color="red">82A</font>**
        [瑞豐站](../Page/瑞豐站.md "wikilink")－三山國王廟
      - **<font color="red">82B</font>**
        [瑞豐站](../Page/瑞豐站.md "wikilink")－捷運西子灣站(輕軌哈瑪星站)
  - [統聯客運](../Page/統聯客運.md "wikilink")：
      - **<font color="red">83</font>**
        瑞豐站－[高雄火車站](../Page/高雄火車站.md "wikilink")
  - [東南客運](../Page/東南客運.md "wikilink")：
      - **<font color="red">紅18 一心幹線</font>** 中崙社區－
        捷運[三多商圈站](../Page/三多商圈站.md "wikilink")－[實踐大學高雄城區中心](../Page/實踐大學.md "wikilink")

站牌名為《一心二路》

  - [漢程客運](../Page/漢程客運.md "wikilink")：
      - **<font color="red">72A</font>**
        [金獅湖站](../Page/金獅湖站.md "wikilink")－[中正高工](../Page/高雄市立中正高級工業職業學校.md "wikilink")
      - **<font color="red">72B</font>**
        [金獅湖站](../Page/金獅湖站.md "wikilink")－正勤社區
  - [統聯客運](../Page/統聯客運.md "wikilink")：
      - **<font color="red">83</font>**
        [瑞豐站](../Page/瑞豐站.md "wikilink")－[高雄火車站](../Page/高雄火車站.md "wikilink")
  - [東南客運](../Page/東南客運.md "wikilink")：
      - **<font color="red">紅18 一心幹線</font>** 中崙社區－
        捷運[三多商圈站](../Page/三多商圈站.md "wikilink")－[實踐大學高雄城區中心](../Page/實踐大學.md "wikilink")

## 相鄰道路

<table>
<thead>
<tr class="header">
<th><p><strong><a href="../Page/高雄市.md" title="wikilink">高雄市</a><a href="../Page/高雄市主要道路列表.md" title="wikilink">主要幹道</a></strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>西行道路</strong></p></td>
</tr>
<tr class="even">
<td><p><em>終端</em><br />
<small><a href="../Page/苓雅區.md" title="wikilink">苓雅區</a></small></p></td>
</tr>
</tbody>
</table>

## 內部連結

  - [高雄市主要道路列表](../Page/高雄市主要道路列表.md "wikilink")

[Category:前鎮區](../Category/前鎮區.md "wikilink")
[Category:高雄市街道](../Category/高雄市街道.md "wikilink")

1.  [沒有慶祝！高雄氣爆道路修復
    凌晨開放通車](http://news.ltn.com.tw/index.php/news/life/breakingnews/1161793).
    [自由時報](../Page/自由時報.md "wikilink"). \[2014-11-21\].