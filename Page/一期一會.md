**一期一會**是一個源於[日本茶道的](../Page/日本茶道.md "wikilink")[成語](../Page/成語.md "wikilink")，意思是在茶會時領悟到這次相會無法重來，是一輩子只有一次的相會，故賓主須各盡其誠意。

在茶道以外，這個意義可推而廣之，指一生一次的機會，當下的時光不會再來，須珍重之。

“一期一會”這個用語也常在日本[武道中出現](../Page/武道.md "wikilink")，警惕學武道的人不要因以為有「再試一次」的機會而掉以輕心或疏於練習，若是面臨生死關頭就沒機會「再試一次」了。此外，雖然武道的技巧（如[柔道的招式](../Page/柔道.md "wikilink")）可以多次重複使出，但每次比試、較量都是獨特的。

## 詞源和精神

原為[千利休的話](../Page/千利休.md "wikilink")。利休本人沒有留下著作，但其弟子[山上宗二所著](../Page/山上宗二.md "wikilink")《[山上宗二記](../Page/山上宗二記.md "wikilink")》中的〈茶湯者覺悟十躰〉記載了千利休的話：

「一期」為佛語，指人的一生；「一會」則意味著僅有一次相會。「一期一會」勸勉人們應珍惜身邊的人，珍惜每一次的茶会。

一期一會與[日本禪當中的](../Page/日本禪宗.md "wikilink")「瞬間」概念有關，而日本茶道也體現了日本禪的精神，所以這句話經常會寫在掛於[茶室的](../Page/茶室.md "wikilink")[字畫上](../Page/字畫.md "wikilink")，意味著每次的茶聚都是獨一無二的。

## 相關流行文化

  - 2009年日本知名電視劇《[仁醫](../Page/仁醫.md "wikilink")》、2011年《仁醫2》，皆有提及，並搭配劇中人事物向觀眾解釋其含意。
  - 2010\~2012，日本演員[大澤隆夫共](../Page/大澤隆夫.md "wikilink")5場朗讀活劇，皆與其有關。
  - 日本殿堂級女歌手[中島美雪於](../Page/中島美雪.md "wikilink")2007年發行的單曲《》。
  - [美國電影](../Page/美國電影.md "wikilink")《[阿甘正傳](../Page/阿甘正傳.md "wikilink")》在日上映時，以“一期一會”作為日語版片名的副標。
  - 日本漫畫《[植木的法則
    PLUS](../Page/植木的法則_PLUS.md "wikilink")》曾提及朋友是“一期一會”的；同樣是日本漫畫的《[花樣男子](../Page/花樣男子.md "wikilink")》、《[校園漫畫大王](../Page/校園漫畫大王.md "wikilink")》其中一個單元亦常提到；《[-{zh-hans:黑子的篮球;
    zh-hk:幻影籃球王;
    zh-tw:影子籃球員;}-](../Page/黑子的籃球.md "wikilink")》中主角黑子哲也的座右銘亦是「一期一會」；《[火影忍者](../Page/火影忍者.md "wikilink")》在火影房間的走廊上，四字格言的其中一幅。
  - 韓國劇集《[流星花園](../Page/流星花園_\(韓國電視劇\).md "wikilink")》中，“一期一會”是蘇易正與其初戀女友的聯繫。
  - 美國電視劇《[Heroes](../Page/Heroes.md "wikilink")》裡的[日本人角色](../Page/日本人.md "wikilink")[中村廣最喜歡](../Page/中村廣.md "wikilink")“一期一會”這句話\[1\]。
  - [香港無綫電視劇集](../Page/香港無綫電視.md "wikilink")《[和味濃情](../Page/和味濃情.md "wikilink")》中，男主角鍾禮和工作的[日本料理店名為](../Page/日本料理.md "wikilink")「一期一會」。
  - 香港獨立唱作人[林一峰與](../Page/林一峰.md "wikilink")[W創作社曾合作音樂劇](../Page/W創作社.md "wikilink")《[一期一會](../Page/一期一會_\(音樂劇\).md "wikilink")》。
  - 日本电视剧《[日本人不知道的日语](../Page/日本人不知道的日语.md "wikilink")》第8集中也有所提及。
  - 日本晨间电视剧《[多谢款待](../Page/多谢款待.md "wikilink")》中，第一周的名字叫做一期一会。

## 參考資料

  - 戶倉恆信
    \[<https://web.archive.org/web/20141017170837/http://news.ltn.com.tw/news/opinion/paper/789653>　生魚片與「無垢箸」\]，台灣自由時報，2014年6月22日

## 外部連結

  - [To Blossom and
    Scatter](https://web.archive.org/web/20081014190816/http://www.furyu.com/wayne/Dave%27sF/S2Flowers3.html)

[Category:日本傳統](../Category/日本傳統.md "wikilink")
[Category:日本茶道](../Category/日本茶道.md "wikilink")
[Category:武道](../Category/武道.md "wikilink")
[Category:日語熟語](../Category/日語熟語.md "wikilink")
[Category:成語](../Category/成語.md "wikilink")

1.