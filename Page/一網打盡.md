《**一網打盡**》是[香港](../Page/香港.md "wikilink")[無綫電視的一個](../Page/無綫電視.md "wikilink")[節目](../Page/無綫電視節目列表.md "wikilink")，由[王貽興及](../Page/王貽興.md "wikilink")[梁慧思主持](../Page/梁慧思.md "wikilink")，自2007年
起逢星期六晚7:35分播出至2007年5月26日播畢。

## 演員表

### 主持

  - [王貽興](../Page/王貽興.md "wikilink")
  - [梁慧思](../Page/梁慧思.md "wikilink")

### 演出

  - [陳丹丹](../Page/陳丹丹.md "wikilink")
  - [黃淑怡](../Page/黃淑怡.md "wikilink")
  - [陳宙希](../Page/陳宙希.md "wikilink")

## 抄襲爭議

《[明報](../Page/明報.md "wikilink")》及其他傳媒引述網絡的討論指[TVB節目](../Page/TVB.md "wikilink")《一網打盡》抄襲《[第一手真相](../Page/第一手真相.md "wikilink")》，亞視管理層葉家寶暗示[TVB製造](../Page/TVB.md "wikilink")「[A貨](../Page/A貨.md "wikilink")」，觀眾收看時要應驗明「正貨」。《[明報](../Page/明報.md "wikilink")》舉例，2007年3月《一網打盡》報道[元朗賽馬會廣場公園有以艱澀](../Page/元朗賽馬會廣場公園.md "wikilink")[英文](../Page/英文.md "wikilink")「[Paraphernalia](../Page/:en:Paraphernalia.md "wikilink")」寫成告示牌，而《第一手真相》在《一網打盡》播出前數日已以相同形式報導過\[1\]，但此內容在個多月前，香港《[太陽報](../Page/太陽報_\(香港\).md "wikilink")》已有報道\[2\]，另外在形式上，《一網打盡》不會像《第一手真相》大量播出有關非[公眾人物行為的片段](../Page/名人.md "wikilink")，並不會有[隱私權的問題](../Page/隱私權.md "wikilink")。

而《一網打盡》和另一個被指涉嫌被抄襲的亞洲電視節目《[冷知識衝動](../Page/冷知識衝動.md "wikilink")》相似，《冷知識衝動》主要介紹[生辟知識亦並非亞洲電視原創](../Page/冷知識.md "wikilink")（實際原創者是[日本](../Page/日本.md "wikilink")[富士電視台](../Page/富士電視台.md "wikilink")，而[台灣](../Page/台灣.md "wikilink")[東風衛視亦曾在亞視未推出](../Page/東風衛視.md "wikilink")《冷知識衝動》前製作過同類節目《[冷知識轟趴](../Page/冷知識轟趴.md "wikilink")》，並曾經在[香港播放](../Page/香港.md "wikilink")），《一網打盡》則集中討論[網上傳聞的真確性](../Page/網上傳聞.md "wikilink")，而且由註冊營養師擔任主持，側重報道食物安全資訊，但兩者都會邀請專家講解。

## 參考資料

<div style="font-size: 85%">

<references />

</div>

## 參見

  - [第一手真相](../Page/第一手真相.md "wikilink")
  - [冷知識衝動](../Page/冷知識衝動.md "wikilink")

[Category:2007年無綫電視節目](../Category/2007年無綫電視節目.md "wikilink")
[Category:無綫電視資訊節目](../Category/無綫電視資訊節目.md "wikilink")

1.  [《一網打盡》被指抄襲亞視《第一手真相》](http://hk.news.yahoo.com/070318/12/23vvk.html)
    ，明報，2007年3月19日
2.  [康文署考起人？](http://the-sun.on.cc/channels/news/20070221/20070221015325_0000.html)，太陽報，2007年2月21日