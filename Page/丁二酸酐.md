**丁二酸酐**，别名**琥珀酸酐**，是[琥珀酸自身](../Page/琥珀酸.md "wikilink")[脱水](../Page/脱水.md "wikilink")[缩合形成的环状](../Page/缩合.md "wikilink")[酸酐](../Page/酸酐.md "wikilink")，分子式为C<sub>4</sub>H<sub>4</sub>O<sub>3</sub>。它与热水反应生成[丁二酸](../Page/丁二酸.md "wikilink")，与[氨反应生成](../Page/氨.md "wikilink")[琥珀酰亚胺](../Page/琥珀酰亚胺.md "wikilink")，是[有机合成重要的中间体](../Page/有机合成.md "wikilink")，在医药工业上主要用于合成[氯霉素琥珀酸酯钠](../Page/氯霉素.md "wikilink")、[羟孕酮琥珀酸酯钠和](../Page/羟孕酮.md "wikilink")[氢化可的松琥珀酸酯钠等](../Page/氢化可的松.md "wikilink")。

## 参见

  - [顺丁烯二酸酐](../Page/顺丁烯二酸酐.md "wikilink")

## 外部链接

  - [化学数据](http://www.chemicalland21.com/specialtychem/perchem/SUCCINIC%20ANHYDRIDE.htm)

[Category:酸酐](../Category/酸酐.md "wikilink")
[Category:四氢呋喃](../Category/四氢呋喃.md "wikilink")