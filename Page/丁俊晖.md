**丁俊晖**（），[中国](../Page/中国.md "wikilink")[江苏](../Page/江苏.md "wikilink")[宜兴人](../Page/宜兴.md "wikilink")，[斯诺克](../Page/斯诺克.md "wikilink")[台球運動員](../Page/台球.md "wikilink")，画家[丁天缺的侄孙](../Page/丁天缺.md "wikilink")。斯諾克三大賽事裡，最重要的[斯諾克世界錦標賽的史上最佳亞洲選手](../Page/斯諾克世界錦標賽.md "wikilink")。2011年四強敗於[朱德·特魯姆普](../Page/朱德·特魯姆普.md "wikilink")，2016年決賽14－18敗於[馬克·塞爾比獲亞軍](../Page/馬克·塞爾比.md "wikilink")，2017年四強再敗於塞爾比。其餘兩大賽事，[斯诺克英国锦标赛在](../Page/斯诺克英国锦标赛.md "wikilink")2005和2009年兩獲冠軍，[斯诺克大師賽在](../Page/斯诺克大師賽.md "wikilink")2011年奪冠。職業賽上六度[單杆擊出147分滿分](../Page/斯诺克单杆最高得分.md "wikilink")。

丁俊晖長居[英格兰](../Page/英格兰.md "wikilink")[谢菲尔德打球](../Page/谢菲尔德.md "wikilink")。

## 早期

丁俊晖的父母曾是从事[副食品生意的普通](../Page/副食品.md "wikilink")[个体户](../Page/个体户.md "wikilink")，父亲丁文钧爱好[台球](../Page/台球.md "wikilink")。8岁时丁俊晖就开始接触[台球](../Page/台球.md "wikilink")，不过他真正走上[斯诺克台球的道路缘于一次偶然经历](../Page/斯诺克台球.md "wikilink")。他在小学三年级暑假时，他父亲与一位当地的台球高手切磋球技。在其他人的怂恿下，丁俊晖在父亲上厕所的间隙替父亲打了几杆球，竟出人意料地替父亲战胜了对手。自此，丁父亲便开始有意识地培养儿子的[台球兴趣](../Page/台球.md "wikilink")。望子成龙的父亲为了保证丁俊晖的训练，甚至顶着家庭和社会的各种压力要求丁俊晖就读的学校[东莞市](../Page/东莞市.md "wikilink")[莞城二中允许让丁俊晖只修](../Page/莞城二中.md "wikilink")[语文和](../Page/语文.md "wikilink")[数学](../Page/数学.md "wikilink")，半天学习，半天练球。另外还放弃原先的生意，自己开了一家球房。假期里，还送丁俊晖到斯诺克台球环境相对较好的[上海接受系统的斯诺克专业训练](../Page/上海.md "wikilink")。而凭着自己的努力和天赋，丁俊晖很快就在[江苏省内崭露头角](../Page/江苏省.md "wikilink")。

1998年底，家人为了丁俊晖的进一步发展，更是举家迁往[广东](../Page/广东.md "wikilink")[东莞](../Page/东莞.md "wikilink")。小学毕业后，[台球和学业之间](../Page/台球.md "wikilink")，丁俊晖选择了完全放弃学业，为东莞的一家台球城打球。对台球的热情使得他常常是每天训练超过八个小时。同时那段时间也是丁家生活最为拮据的时期，父母甚至把老家[宜兴的房子变卖](../Page/宜兴.md "wikilink")。

## 职业生涯

丁俊晖真正引起[斯诺克台球界的注意是在](../Page/斯诺克台球.md "wikilink")2002年。在5月份举行的21岁以下[亚洲青年锦标赛上](../Page/亚洲.md "wikilink")，年仅15岁的丁俊晖成为最年轻的亚洲青年冠军，紧接着又获得了当年的亚洲锦标赛冠军。同年8月，在[拉脱维亚首都](../Page/拉脱维亚.md "wikilink")[里加举行的国际台球和](../Page/里加.md "wikilink")[斯诺克联合会](../Page/斯诺克联合会.md "wikilink")（the
International Billiards and Snooker Federation,
IBSF）21岁以下世界青年锦标赛上，又成为中国第一个-{zh-hans:台球;zh-hk:桌球;zh-tw:撞球;}-世界冠军。而在10月的[釜山](../Page/釜山.md "wikilink")[亚运会上](../Page/亚洲运动会.md "wikilink")，又在[斯诺克单打决赛中战胜](../Page/斯诺克.md "wikilink")[泰国选手夺得](../Page/泰国.md "wikilink")[金牌](../Page/金牌.md "wikilink")。

受[非典的影响](../Page/非典.md "wikilink")，2003年的亚洲锦标赛和青年锦标赛均取消了。不过丁俊晖再次闯入了21岁以下世界青年锦标赛的4强。
当年8月，他还曾两度战胜当时[世界职业排名第一的](../Page/斯诺克职业选手世界排名.md "wikilink")[马克·威廉姆斯](../Page/马克·威廉姆斯.md "wikilink")（Mark
Williams）。鉴于其出色表现，他被[世界斯诺克协会](../Page/世界斯诺克协会.md "wikilink")（the World
Snooker Association）特许参加职业巡回赛（awarded a Main Tour
concession）并于当年9月正式成为职业斯诺克选手。

2003年初，丁俊晖开始到[英国](../Page/英国.md "wikilink")[威林布勒](../Page/威林布勒.md "wikilink")（Wellingborough）的一家斯诺克俱乐部训练。平时与2002年[世锦赛冠军](../Page/斯诺克世界锦标赛.md "wikilink")[彼得·艾伯顿](../Page/彼得·艾伯顿.md "wikilink")（Peter
Ebdon）等练球。艾伯顿对其在训练中表现出的技术和耐心赞誉有佳。

2004年2月，持外卡参赛的丁俊晖在[温布利大师赛上首轮即以](../Page/斯诺克大师赛.md "wikilink")6-3淘汰了当时世界排名第16位的[喬·佩里](../Page/喬·佩里_\(士碌架運動員\).md "wikilink")（Joe
Perry）。不过第二轮在5-2领先的情况下最后以5-6的微弱劣势输给了[史蒂芬·李](../Page/史蒂芬·李.md "wikilink")（Stephen
Lee）。

而进入2005年，丁俊晖日渐成熟，比赛发挥更趋稳定。先是在温布利大师赛上更进一步打进八强。
4月，刚刚度过自己18岁生日的丁俊晖更是以一名持[外卡](../Page/外卡.md "wikilink")（wild
card）球员的身份在[中国公开赛上一路击败](../Page/斯诺克中国公开赛.md "wikilink")[彼得·艾伯顿](../Page/彼得·艾伯顿.md "wikilink")、[傅家俊](../Page/傅家俊.md "wikilink")、[肯·达赫蒂和](../Page/肯·达赫蒂.md "wikilink")[史蒂芬·亨得利等世界排名前](../Page/史蒂芬·亨得利.md "wikilink")16位的名将，夺得自己首个职业排名赛的冠军。特别是后两场比赛，先是6-0横扫达赫蒂进入决赛。决赛中，又是在1-4落后的情况下奋起直追，最终以9-5战胜了7次[世界锦标赛冠军得主亨得利](../Page/斯诺克世界锦标赛.md "wikilink")。

2005年12月18日，在[英国斯诺克锦标赛决赛中](../Page/英国斯诺克锦标赛.md "wikilink")，丁俊晖经过16局苦战击败英国老将[斯蒂夫·戴维斯](../Page/斯蒂夫·戴维斯.md "wikilink")，成为该锦标赛27年来第一位来自[英国](../Page/英国.md "wikilink")、[爱尔兰以外的冠军](../Page/爱尔兰.md "wikilink")。丁俊晖最新的世界排名也因此一跃上升到了31位，创造了个人的历史新高。\[1\]\[2\]

2006年，他勇奪第3個積分賽冠軍 - 北愛爾蘭冠軍賽。

2006年12月，他经历了一天四场的魔鬼赛程，实现了[多哈亚运会上夺取斯诺克团体](../Page/多哈亚运会.md "wikilink")、双打、个人三项冠军的“大满贯”。

2007年1月2日，他夺得2006年中国斯诺克全国锦标赛冠军。

### 2007年温布利大师赛

2007年1月14日晚，2007年[温布利大师赛拉开序幕](../Page/斯诺克大师赛.md "wikilink")，丁俊晖持外卡参赛，在揭幕战6比3击败对手汉密尔顿，顺利挺进大师赛16强。在第七局比赛中，丁俊晖打出了职业生涯正式比赛首个单杆147分，是温布利大师赛历史上第二次出现单杆满分的现象。这也是斯诺克有史以来出现的第55次单杆147满分，也是第三位单杆147满分的[亚洲选手](../Page/亚洲.md "wikilink")，之前两位是[泰国选手詹姆斯](../Page/泰国.md "wikilink")-瓦塔纳和[香港选手](../Page/香港.md "wikilink")[傅家俊](../Page/傅家俊.md "wikilink")。

赛后，[路透社的标题则直接指出](../Page/路透社.md "wikilink")《丁俊晖成为最年轻的147创造者》。英国《[泰晤士报](../Page/泰晤士报.md "wikilink")》网站体育板块也用大篇幅对丁俊晖的惊人表现进行了赞扬，“丁俊晖的庆祝显得非常的简洁。首先是笑容慢慢在丁俊晖脸上蔓延，随后全场观众起立为丁俊晖鼓掌喝彩，但是仅仅在几秒钟之内，中国天才的脸就恢复到了通常那个冷漠的表情。”

而对手汉密尔顿则评价：“和他往常一样，（打出147）几分钟之后，我在洗手间再度见到丁俊晖，他看起来就像任何事情都没有发生一样。”\[3\]

2007年1月21日，温布利大师赛决赛拉开战幕。丁俊晖对阵号称超级火箭的[罗尼·奥沙利文](../Page/罗尼·奥沙利文.md "wikilink")。[罗尼·奥沙利文发挥出色](../Page/罗尼·奥沙利文.md "wikilink")，但丁俊晖在第8局因未中一个粉球导致未能扳平总比分而显得很沮丧，又在场外粗鲁观众的一片嘘声中明显的乱了阵脚，甚至后来都止不住流下了流泪。虽然[罗尼·奥沙利文试图安慰年轻的丁俊晖](../Page/罗尼·奥沙利文.md "wikilink")，最终丁俊晖仍以3比10不敌对手，获得亚军。\[4\]

### 2007/08赛季

2007/08赛季丁俊晖没有突出亮点，各项排名赛事均没能进入8强。

[2008年斯诺克世界锦标赛](../Page/2008年斯诺克世界锦标赛.md "wikilink")，丁俊晖和[傅家俊上演了一场精彩的](../Page/傅家俊.md "wikilink")“中国德比”，也是世锦赛历史上首次有两名中国选手在正赛相遇，最终丁俊晖在决胜局战胜了[傅家俊](../Page/傅家俊.md "wikilink")。\[5\]
第二轮，丁俊晖与[史蒂芬·亨得利相遇](../Page/史蒂芬·亨得利.md "wikilink")，第一场就以2：6大比分落后，最终以7：13败北，未能进入8强。

### 2009/10赛季

2009年12月14日，丁俊晖在[英國桌球錦標賽決賽中](../Page/英國桌球錦標賽.md "wikilink")，以10比8战胜世界临时排名第一的[希金斯](../Page/希金斯.md "wikilink")，继2005年之后第二次在英锦赛封王，同時他的临时世界排名也飙升至第6位，而他在今次比賽之中亦連續殺敗世界排名頭5位的當中4人，分別有马奎尔（排名第2）、墨菲（排名第3）、希金斯（排名第4）及卡特（排名第5）。

### 2010/11赛季

丁俊晖决赛击败傅家俊首夺温布利大师赛冠军，同年世锦赛打入半决赛，负于特鲁姆普。

### 2011/12赛季

2012年2012年2月20日，斯诺克威尔士公开赛决赛上丁俊晖以9-6力擒排名世界第一的塞尔比，结束了长达26个月的排名赛冠军荒，夺得自己职业第五个排名赛冠军。

[Ding_Junhui_at_Snooker_German_Masters_(DerHexer)_2013-01-30_07.jpg](https://zh.wikipedia.org/wiki/File:Ding_Junhui_at_Snooker_German_Masters_\(DerHexer\)_2013-01-30_07.jpg "fig:Ding_Junhui_at_Snooker_German_Masters_(DerHexer)_2013-01-30_07.jpg")

### 2012/13赛季

2013年2013年3月18日，斯诺克PTC（斯诺克球员巡回赛）总决赛上，丁俊晖在0-3落后的不利局面下连胜4局成功逆转尼尔·罗伯逊，夺得个人第六座排名赛冠军。

### 2013/14赛季

  - 2013年9月22日，斯诺克上海大师赛上，丁俊晖10-6战胜同胞肖国栋，夺得个人第七座排名赛冠军。
  - 2013年10月18日，首届斯诺克印度公开赛决赛，丁俊晖5-0战胜东道主选手梅塔，获得个人第八个排名赛冠军。第一次“背靠背”获得连续两站比赛的胜利。
  - 2013年11月4日，丁俊晖在斯诺克成都国际锦标赛决赛中10-9战胜傅家俊，实现大型排名赛三连冠。
  - 2014年2月4日，丁俊晖在德国大师赛决赛中9-5战胜[Judd
    Trump](http://en.wikipedia.org/wiki/Judd_Trump)，收获赛季第四冠和职业生涯第10冠。
  - 2014年3月1日，斯诺克威尔士公开赛上，丁俊晖分别战胜了柯普、沃拉斯顿、唐纳德森和沃克尔，晋级四强。半决赛打败Joe
    Perry。决赛负于奥沙利文。
  - 2014年中国公开赛，丁俊晖决赛战胜尼尔·罗伯逊，获得第11个排名赛头衔。
  - 2014年世界斯诺克锦标赛，丁俊晖首轮9-10负于韦斯利。

### 2014/15赛季

  - 赛季初，丁俊晖获得了APTC宜兴站的冠军。这使他早早获得了参加PCGF的资格，所以丁放弃了之后整个赛季的欧巡赛和亚巡赛。
  - 上海大师赛打入4强。
  - 丁俊晖没有打进成都国际锦标赛的正赛。并且先后在大师赛、英锦赛、德国公开赛、威尔士公开赛、印度公开赛、世界大奖赛和PCGF首轮出局。
  - 2014年12月，丁俊晖成为世界排名第一。只维持了10天就被尼尔·罗伯逊代替。
  - 2015年中国公开赛，丁俊晖先后战胜坎贝尔，马克·威廉姆斯，约翰·希金斯，在半决赛负于加里·威尔森。
  - 2015年斯诺克世界锦标赛中，丁俊晖10-7战胜马克·戴维斯、13-9战胜约翰·希金斯、4-13负于TRUMP，止步8强。

### 2015/16赛季

2016年4月底，丁俊晖在世锦赛首场半决赛中以17:11击败苏格兰45岁老将[麦克马努斯](../Page/麦克马努斯.md "wikilink")，成为首位打进[斯诺克世锦赛决赛的亚洲球手](../Page/斯诺克世锦赛.md "wikilink")。\[6\]在决赛中以14-18负于[马克·塞尔比居亚军](../Page/马克·塞尔比.md "wikilink")，塞尔比继2014年后再次获得世锦赛冠军。\[7\]

### 2016/17赛季

  - 2016年9月，丁俊晖在决赛中以8-7险胜宾汉姆，首次获得斯诺克6红球世锦赛冠军。
  - 2016年10月的斯诺克上海大师赛，丁俊晖发挥出色，决赛以10-6战胜马克-塞尔比夺取冠军，成为了上海大师赛十年来唯一一位获得了两次冠军的球员。这也是他时隔29个月再夺排名赛冠军。

## 历史战绩与排名

<table>
<thead>
<tr class="header">
<th><p><strong>Tournaments</strong></p></th>
<th><p><a href="../Page/2003/2004_snooker_season.md" title="wikilink">2003/<br />
04</a></p></th>
<th><p><a href="../Page/2004/2005_snooker_season.md" title="wikilink">2004/<br />
05</a></p></th>
<th><p><a href="../Page/2005/2006_snooker_season.md" title="wikilink">2005/<br />
06</a></p></th>
<th><p><a href="../Page/2006/2007_snooker_season.md" title="wikilink">2006/<br />
07</a></p></th>
<th><p><a href="../Page/2007/2008_snooker_season.md" title="wikilink">2007/<br />
08</a></p></th>
<th><p><a href="../Page/Snooker_season_2008/2009.md" title="wikilink">2008/<br />
09</a></p></th>
<th><p><a href="../Page/2009/2010_snooker_season.md" title="wikilink">2009/<br />
10</a></p></th>
<th><p><a href="../Page/2010/2011_snooker_season.md" title="wikilink">2010/<br />
11</a></p></th>
<th><p><a href="../Page/Snooker_season_2011/2012.md" title="wikilink">2011/<br />
12</a></p></th>
<th><p><a href="../Page/Snooker_season_2012/2013.md" title="wikilink">2012/<br />
13</a></p></th>
<th><p><a href="../Page/Snooker_season_2013/2014.md" title="wikilink">2013/<br />
14</a></p></th>
<th><p><a href="../Page/Snooker_season_2014/2015.md" title="wikilink">2014/<br />
15</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/Snooker_world_rankings.md" title="wikilink">Rankings</a></strong>[8][9]</p></td>
<td><p><a href="../Page/Snooker_world_rankings_2003/2004.md" title="wikilink">UR</a>[10]</p></td>
<td><p><a href="../Page/Snooker_world_rankings_2004/2005.md" title="wikilink">76</a></p></td>
<td><p><a href="../Page/Snooker_world_rankings_2005/2006.md" title="wikilink">62</a></p></td>
<td><p><a href="../Page/Snooker_world_rankings_2006/2007.md" title="wikilink">27</a></p></td>
<td><p><a href="../Page/Snooker_world_rankings_2007/2008.md" title="wikilink">9</a></p></td>
<td><p><a href="../Page/Snooker_world_rankings_2008/2009.md" title="wikilink">11</a></p></td>
<td><p><a href="../Page/Snooker_world_rankings_2009/2010.md" title="wikilink">13</a></p></td>
<td><p><a href="../Page/Snooker_world_rankings_2010/2011.md" title="wikilink">5</a></p></td>
<td><p><a href="../Page/Snooker_world_rankings_2011/2012.md" title="wikilink">4</a></p></td>
<td><p><a href="../Page/Snooker_world_rankings_2012/2013.md" title="wikilink">11</a></p></td>
<td><p><a href="../Page/Snooker_world_rankings_2013/2014.md" title="wikilink">10</a></p></td>
<td><p><a href="../Page/Snooker_world_rankings_2014/2015.md" title="wikilink">2</a></p></td>
</tr>
<tr class="even">
<td><p><strong>Ranking tournaments</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Wuxi_Classic.md" title="wikilink">Wuxi Classic</a>[11]</p></td>
<td><p>Tournament Not Held</p></td>
<td><p>Non-ranking</p></td>
<td><p><a href="../Page/2012_Wuxi_Classic.md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2013_Wuxi_Classic.md" title="wikilink">2R</a></p></td>
<td><p>LQ</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Australian_Goldfields_Open.md" title="wikilink">Australian Goldfields Open</a></p></td>
<td><p>Tournament Not Held</p></td>
<td><p><a href="../Page/2011_Australian_Goldfields_Open.md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2012_Australian_Goldfields_Open.md" title="wikilink">2R</a></p></td>
<td><p>WD</p></td>
<td><p>A</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Shanghai_Masters_(snooker).md" title="wikilink">Shanghai Masters</a></p></td>
<td><p>Tournament Not Held</p></td>
<td><p><a href="../Page/2007_Shanghai_Masters.md" title="wikilink">2R</a></p></td>
<td><p><a href="../Page/2008_Shanghai_Masters.md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2009_Shanghai_Masters.md" title="wikilink">QF</a></p></td>
<td><p><a href="../Page/2010_Shanghai_Masters.md" title="wikilink">2R</a></p></td>
<td><p><a href="../Page/2011_Shanghai_Masters.md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2012_Shanghai_Masters.md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2013_Shanghai_Masters.md" title="wikilink"><strong>W</strong></a></p></td>
<td><p><a href="../Page/2014_Shanghai_Masters.md" title="wikilink">SF</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/International_Championship.md" title="wikilink">International Championship</a></p></td>
<td><p>Tournament Not Held</p></td>
<td><p><a href="../Page/2012_International_Championship.md" title="wikilink">2R</a></p></td>
<td><p><a href="../Page/2013_International_Championship.md" title="wikilink"><strong>W</strong></a></p></td>
<td><p>LQ</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/UK_Championship_(snooker).md" title="wikilink">UK Championship</a></p></td>
<td><p>LQ</p></td>
<td><p><a href="../Page/2004_UK_Championship_(snooker).md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2005_UK_Championship_(snooker).md" title="wikilink"><strong>W</strong></a></p></td>
<td><p><a href="../Page/2006_UK_Championship_(snooker).md" title="wikilink">QF</a></p></td>
<td><p><a href="../Page/2007_UK_Championship_(snooker).md" title="wikilink">QF</a></p></td>
<td><p><a href="../Page/2008_UK_Championship_(snooker).md" title="wikilink">2R</a></p></td>
<td><p><a href="../Page/2009_UK_Championship_(snooker).md" title="wikilink"><strong>W</strong></a></p></td>
<td><p><a href="../Page/2010_UK_Championship_(snooker).md" title="wikilink">2R</a></p></td>
<td><p><a href="../Page/2011_UK_Championship_(snooker).md" title="wikilink">QF</a></p></td>
<td><p><a href="../Page/2012_UK_Championship_(snooker).md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2013_UK_Championship_(snooker).md" title="wikilink">4R</a></p></td>
<td><p><a href="../Page/2014_UK_Championship_(snooker).md" title="wikilink">3R</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/German_Masters.md" title="wikilink">German Masters</a></p></td>
<td><p>Tournament Not Held</p></td>
<td><p><a href="../Page/2011_German_Masters.md" title="wikilink">QF</a></p></td>
<td><p><a href="../Page/2012_German_Masters.md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2013_German_Masters.md" title="wikilink">2R</a></p></td>
<td><p><a href="../Page/2014_German_Masters.md" title="wikilink"><strong>W</strong></a></p></td>
<td><p><a href="../Page/2015_German_Masters.md" title="wikilink">1R</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Welsh_Open_(snooker).md" title="wikilink">Welsh Open</a></p></td>
<td><p>LQ</p></td>
<td><p><a href="../Page/2005_Welsh_Open_(snooker).md" title="wikilink">1R</a></p></td>
<td><p>LQ</p></td>
<td><p><a href="../Page/2007_Welsh_Open_(snooker).md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2008_Welsh_Open_(snooker).md" title="wikilink">3R</a></p></td>
<td><p><a href="../Page/2009_Welsh_Open_(snooker).md" title="wikilink">2R</a></p></td>
<td><p><a href="../Page/2010_Welsh_Open_(snooker).md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2011_Welsh_Open_(snooker).md" title="wikilink">QF</a></p></td>
<td><p><a href="../Page/2012_Welsh_Open_(snooker).md" title="wikilink"><strong>W</strong></a></p></td>
<td><p><a href="../Page/2013_Welsh_Open_(snooker).md" title="wikilink">SF</a></p></td>
<td><p><a href="../Page/2014_Welsh_Open_(snooker).md" title="wikilink">F</a></p></td>
<td><p><a href="../Page/2015_Welsh_Open_(snooker).md" title="wikilink">1R</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Indian_Open_(snooker).md" title="wikilink">Indian Open</a></p></td>
<td><p>Tournament Not Held</p></td>
<td><p><a href="../Page/2013_Indian_Open.md" title="wikilink"><strong>W</strong></a></p></td>
<td><p><a href="../Page/2015_Indian_Open.md" title="wikilink">1R</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Players_Championship_Grand_Final.md" title="wikilink">Players Championship Grand Final</a>[12]</p></td>
<td><p>Tournament Not Held</p></td>
<td><p>DNQ</p></td>
<td><p><a href="../Page/Players_Tour_Championship_2011/2012_–_Finals.md" title="wikilink">2R</a></p></td>
<td><p><a href="../Page/Players_Tour_Championship_2012/2013_–_Finals.md" title="wikilink"><strong>W</strong></a></p></td>
<td><p><a href="../Page/Players_Tour_Championship_2013/2014_–_Finals.md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/Players_Tour_Championship_2014/2015_–_Finals.md" title="wikilink">1R</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/China_Open_(snooker).md" title="wikilink">China Open</a></p></td>
<td><p>NH</p></td>
<td><p><a href="../Page/2005_China_Open_(snooker).md" title="wikilink"><strong>W</strong></a></p></td>
<td><p><a href="../Page/2006_China_Open_(snooker).md" title="wikilink">SF</a></p></td>
<td><p><a href="../Page/2007_China_Open_(snooker).md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2008_China_Open_(snooker).md" title="wikilink">2R</a></p></td>
<td><p><a href="../Page/2009_China_Open_(snooker).md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2010_China_Open_(snooker).md" title="wikilink">F</a></p></td>
<td><p><a href="../Page/2011_China_Open_(snooker).md" title="wikilink">SF</a></p></td>
<td><p><a href="../Page/2012_China_Open_(snooker).md" title="wikilink">SF</a></p></td>
<td><p><a href="../Page/2013_China_Open_(snooker).md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2014_China_Open_(snooker).md" title="wikilink"><strong>W</strong></a></p></td>
<td><p><a href="../Page/2015_China_Open_(snooker).md" title="wikilink">SF</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/World_Snooker_Championship.md" title="wikilink">World Championship</a></p></td>
<td><p>LQ</p></td>
<td><p>LQ</p></td>
<td><p>LQ</p></td>
<td><p><a href="../Page/2007_World_Snooker_Championship.md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2008_World_Snooker_Championship.md" title="wikilink">2R</a></p></td>
<td><p><a href="../Page/2009_World_Snooker_Championship.md" title="wikilink">2R</a></p></td>
<td><p><a href="../Page/2010_World_Snooker_Championship.md" title="wikilink">2R</a></p></td>
<td><p><a href="../Page/2011_World_Snooker_Championship.md" title="wikilink">SF</a></p></td>
<td><p><a href="../Page/2012_World_Snooker_Championship.md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2013_World_Snooker_Championship.md" title="wikilink">QF</a></p></td>
<td><p><a href="../Page/2014_World_Snooker_Championship.md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2015_World_Snooker_Championship.md" title="wikilink">QF</a></p></td>
</tr>
<tr class="even">
<td><p><strong>Non-ranking tournaments</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Champion_of_Champions_(snooker).md" title="wikilink">Champion of Champions</a></p></td>
<td><p>Tournament Not Held</p></td>
<td><p><a href="../Page/2013_Champion_of_Champions.md" title="wikilink">QF</a></p></td>
<td><p><a href="../Page/2014_Champion_of_Champions.md" title="wikilink">SF</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Masters_(snooker).md" title="wikilink">The Masters</a></p></td>
<td><p><a href="../Page/2004_Masters_(snooker).md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2005_Masters_(snooker).md" title="wikilink">QF</a></p></td>
<td><p>LQ</p></td>
<td><p><a href="../Page/2007_Masters_(snooker).md" title="wikilink">F</a></p></td>
<td><p><a href="../Page/2008_Masters_(snooker).md" title="wikilink">QF</a></p></td>
<td><p><a href="../Page/2009_Masters_(snooker).md" title="wikilink">QF</a></p></td>
<td><p><a href="../Page/2010_Masters_(snooker).md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2011_Masters_(snooker).md" title="wikilink"><strong>W</strong></a></p></td>
<td><p><a href="../Page/2012_Masters_(snooker).md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2013_Masters_(snooker).md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2014_Masters_(snooker).md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2015_Masters_(snooker).md" title="wikilink">1R</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Championship_League.md" title="wikilink">Championship League</a></p></td>
<td><p>Tournament Not Held</p></td>
<td><p>A</p></td>
<td><p><a href="../Page/2009_Championship_League.md" title="wikilink">RR</a></p></td>
<td><p><a href="../Page/2010_Championship_League.md" title="wikilink">RR</a></p></td>
<td><p><a href="../Page/2011_Championship_League.md" title="wikilink">RR</a></p></td>
<td><p><a href="../Page/2012_Championship_League.md" title="wikilink"><strong>W</strong></a></p></td>
<td><p><a href="../Page/2013_Championship_League.md" title="wikilink">SF</a></p></td>
<td><p>A</p></td>
<td><p>A</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2015_World_Grand_Prix.md" title="wikilink">World Grand Prix</a></p></td>
<td><p>Tournament Not Held</p></td>
<td><p><a href="../Page/2015_World_Grand_Prix.md" title="wikilink">1R</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>Variant format tournaments</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Snooker_Shoot-Out.md" title="wikilink">Shoot-Out</a></p></td>
<td><p>Tournament Not Held</p></td>
<td><p><a href="../Page/2011_Snooker_Shoot-Out.md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2012_Snooker_Shoot-Out.md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2013_Snooker_Shoot-Out.md" title="wikilink">1R</a></p></td>
<td><p>A</p></td>
<td><p>A</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>Former ranking tournaments</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Scottish_Open_(snooker).md" title="wikilink">Scottish Open</a>[13]</p></td>
<td><p><a href="../Page/2004_Players_Championship_(snooker).md" title="wikilink">2R</a></p></td>
<td><p>Tournament Not Held</p></td>
<td><p>MR</p></td>
<td><p>Not Held</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/British_Open_(snooker).md" title="wikilink">British Open</a></p></td>
<td><p>LQ</p></td>
<td><p><a href="../Page/2004_British_Open_(snooker).md" title="wikilink">3R</a></p></td>
<td><p>Tournament Not Held</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Irish_Masters.md" title="wikilink">Irish Masters</a></p></td>
<td><p>LQ</p></td>
<td><p>LQ</p></td>
<td><p>NH</p></td>
<td><p>NR</p></td>
<td><p>Tournament Not Held</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Malta_Cup.md" title="wikilink">Malta Cup</a>[14]</p></td>
<td><p>LQ</p></td>
<td><p>LQ</p></td>
<td><p>LQ</p></td>
<td><p><a href="../Page/2007_Malta_Cup.md" title="wikilink">1R</a></p></td>
<td><p>NR</p></td>
<td><p>Tournament Not Held</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Northern_Ireland_Trophy.md" title="wikilink">Northern Ireland Trophy</a></p></td>
<td><p>Not Held</p></td>
<td><p>NR</p></td>
<td><p><a href="../Page/2006_Northern_Ireland_Trophy.md" title="wikilink"><strong>W</strong></a></p></td>
<td><p><a href="../Page/2007_Northern_Ireland_Trophy.md" title="wikilink">3R</a></p></td>
<td><p><a href="../Page/2008_Northern_Ireland_Trophy.md" title="wikilink">2R</a></p></td>
<td><p>Tournament Not Held</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/World_Open_(snooker).md" title="wikilink">World Open</a>[15]</p></td>
<td><p>LQ</p></td>
<td><p>LQ</p></td>
<td><p>LQ</p></td>
<td><p><a href="../Page/2006_Grand_Prix_(snooker).md" title="wikilink">RR</a></p></td>
<td><p><a href="../Page/2007_Grand_Prix_(snooker).md" title="wikilink">RR</a></p></td>
<td><p><a href="../Page/2008_Grand_Prix_(snooker).md" title="wikilink">QF</a></p></td>
<td><p><a href="../Page/2009_Grand_Prix_(snooker).md" title="wikilink">F</a></p></td>
<td><p><a href="../Page/2010_World_Open_(snooker).md" title="wikilink">QF</a></p></td>
<td><p><a href="../Page/2012_World_Open_(snooker).md" title="wikilink">1R</a></p></td>
<td><p><a href="../Page/2013_World_Open_(snooker).md" title="wikilink">QF</a></p></td>
<td><p><a href="../Page/2014_World_Open_(snooker).md" title="wikilink">3R</a></p></td>
<td><p>NH</p></td>
</tr>
<tr class="even">
<td><p><strong>Former Non-ranking tournaments</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Northern_Ireland_Trophy.md" title="wikilink">Northern Ireland Trophy</a></p></td>
<td><p>Not Held</p></td>
<td><p><a href="../Page/2005_Northern_Ireland_Trophy.md" title="wikilink">1R</a></p></td>
<td><p>Ranking Event</p></td>
<td><p>Tournament Not Held</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Malta_Cup.md" title="wikilink">Malta Cup</a>[16]</p></td>
<td><p>Ranking Event</p></td>
<td><p><a href="../Page/2008_Malta_Cup.md" title="wikilink">SF</a></p></td>
<td><p>Tournament Not Held</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Wuxi_Classic.md" title="wikilink">Wuxi Classic</a>[17]</p></td>
<td><p>Tournament Not Held</p></td>
<td><p><a href="../Page/2008_Jiangsu_Classic.md" title="wikilink"><strong>W</strong></a></p></td>
<td><p><a href="../Page/2009_Jiangsu_Classic.md" title="wikilink">F</a></p></td>
<td><p><a href="../Page/2010_Wuxi_Classic.md" title="wikilink">F</a></p></td>
<td><p><a href="../Page/2011_Wuxi_Classic.md" title="wikilink">SF</a></p></td>
<td><p>Ranking Event</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Premier_League_Snooker.md" title="wikilink">Premier League Snooker</a></p></td>
<td><p>A</p></td>
<td><p>A</p></td>
<td><p><a href="../Page/2005_Premier_League_Snooker_(2005/06).md" title="wikilink">SF</a></p></td>
<td><p><a href="../Page/2006_Premier_League_Snooker.md" title="wikilink">RR</a></p></td>
<td><p><a href="../Page/2007_Premier_League_Snooker.md" title="wikilink">SF</a></p></td>
<td><p><a href="../Page/2008_Premier_League_Snooker.md" title="wikilink">RR</a></p></td>
<td><p>A</p></td>
<td><p><a href="../Page/2010_Premier_League_Snooker.md" title="wikilink">RR</a></p></td>
<td><p><a href="../Page/2011_Premier_League_Snooker.md" title="wikilink">F</a></p></td>
<td><p><a href="../Page/2012_Premier_League_Snooker.md" title="wikilink">RR</a></p></td>
<td><p>Not Held</p></td>
<td></td>
</tr>
</tbody>
</table>

| Performance Table Legend |
| ------------------------ |
| LQ                       |
| SF                       |
| DNQ                      |

|                          |                                                  |
| ------------------------ | ------------------------------------------------ |
| NH / Not Held            | means an event was not held.                     |
| NR / Non-Ranking Event   | means an event is/was no longer a ranking event. |
| R / Ranking Event        | means an event is/was a ranking event.           |
| MR / Minor-Ranking Event | means an event is/was a minor-ranking event.     |

## 个人生活

丁俊晖现居住在英国谢菲尔德。尽管丁俊晖打球擅长进攻，在生活中却是个外刚内柔的人。他还爱好[电子游戏和](../Page/电子游戏.md "wikilink")[乒乓球](../Page/乒乓球.md "wikilink")，甚至还喜欢抱着布娃娃睡觉。丁俊暉得到世界桌球冠軍後，[上海交通大學](../Page/上海交通大學.md "wikilink")、[復旦大學及](../Page/復旦大學.md "wikilink")[西南交通大學三所高校分別主動邀請他唸大學](../Page/西南交通大學.md "wikilink")。最終丁俊暉選擇上海交大，並獲取錄為經濟與管理學院[工商管理類專業的學生](../Page/工商管理.md "wikilink")。

## 主要榮譽

  - 2002年

<!-- end list -->

  - 亚洲斯诺克台球錦標賽 冠軍
  - 世界U-21斯诺克錦標賽 冠軍
  - [釜山亞運會](../Page/2002年亞洲運動會.md "wikilink") 斯诺克男子單打 冠軍

<!-- end list -->

  - 2005年

<!-- end list -->

  - [中国公开赛](../Page/斯诺克中国公开赛.md "wikilink")
    冠軍（2004/2005年球季，勝[蘇格蘭名將](../Page/蘇格蘭.md "wikilink")[史蒂芬·亨得利](../Page/史蒂芬·亨得利.md "wikilink")9-5）

  - 冠軍（2005/2006年球季，勝[英格蘭名將](../Page/英格蘭.md "wikilink")[斯蒂夫·戴维斯](../Page/斯蒂夫·戴维斯.md "wikilink")10-6）

<!-- end list -->

  - 2006年

<!-- end list -->

  - 冠軍（2006/2007年球季，勝[英格蘭名將](../Page/英格蘭.md "wikilink")[罗尼·奥沙利文](../Page/罗尼·奥沙利文.md "wikilink")9-6）

  - [多哈亞運會](../Page/2006年亞洲運動會.md "wikilink") 斯诺克男子雙打/單打/團體 冠軍

<!-- end list -->

  - 2007年

<!-- end list -->

  - 2007年1月14日
    [温布利大师赛亚军](../Page/斯诺克大师赛.md "wikilink")，其间打出了职业生涯正式比赛首个单杆147分，成为斯诺克历史上在电视转播的情况下打出单杆满分的最年轻的球手。\[18\]

<!-- end list -->

  - 2009年

<!-- end list -->

  - 冠軍（2009年12月14日，丁俊晖在英国台球锦标赛决赛中10比8战胜希金斯，第二次在英锦赛封王，也是他的职业第四个排名赛冠军。）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [廣州亞運會](../Page/2010年亞洲運動會.md "wikilink") 斯诺克男子雙打 亞軍

<!-- end list -->

  - 2011年

<!-- end list -->

  - [温布利大师赛](../Page/斯诺克大师赛.md "wikilink")
    冠军（2011年1月16日，勝[傅家俊](../Page/傅家俊.md "wikilink")10-4）
    \[19\]
  - 斯諾克世界盃 冠軍（和梁文博組中國隊）

<!-- end list -->

  - 2012年

<!-- end list -->

  - 威尔士公开赛
    冠軍（2012年2月20日，斯诺克威尔士公开赛决赛上丁俊晖以9-6力擒排名世界第一的塞尔比，结束了长达26个月的排名赛冠军荒，夺得自己职业第五个排名赛冠军。）

<!-- end list -->

  - 2013年

<!-- end list -->

  - 2013年3月18日，斯诺克PTC(斯诺克球员巡回赛)总决赛上，丁俊晖在0-3落后的不利局面下连胜4局成功逆转尼尔.罗伯逊，夺得个人第六座排名赛冠军。
  - 2013年9月22日，斯诺克上海大师赛上，丁俊晖10-6战胜同胞肖国栋，夺得个人第七座排名赛冠军。
  - 2013年10月18日，首届斯诺克印度公开赛决赛，丁俊晖5-0战胜东道主选手梅塔，获得个人第八个排名赛冠军。第一次“背靠背”获得连续两站比赛的胜利。
  - 2013年11月4日，丁俊晖在斯诺克成都国际锦标赛决赛中10-9战胜傅家俊，实现大型排名赛三连冠。

<!-- end list -->

  - 2014年

<!-- end list -->

  - [德國大師賽](../Page/德國大師賽.md "wikilink") 冠軍。冠军头衔达到10个。
  - [中国公开赛](../Page/斯诺克中国公开赛.md "wikilink")
    冠軍（2014年4月6日，丁俊晖在斯诺克中国公开赛中10-5战胜卫冕冠军罗伯逊，获得个人第十一个排名赛冠军。成為繼亨德利之后史上第二人達成单赛季五冠。）

<!-- end list -->

  - 2016年

<!-- end list -->

  - 2016年世界斯诺克锦标赛 亞軍
  - 2016年9月，6红球世锦赛 冠军
  - 2016[斯诺克上海大师赛](../Page/斯诺克上海大师赛.md "wikilink") 冠军

## 注释

## 外部链接

  -
[Category:中国斯诺克运动员](../Category/中国斯诺克运动员.md "wikilink")
[Category:2006年亚洲运动会金牌得主](../Category/2006年亚洲运动会金牌得主.md "wikilink")
[Category:上海交通大學校友](../Category/上海交通大學校友.md "wikilink")
[Category:宜興人](../Category/宜興人.md "wikilink")
[J俊](../Category/丁姓.md "wikilink")

1.

2.

3.

4.

5.  [《中国德比丁俊晖险胜傅家俊》](http://eurosport.sports.sohu.com/snooker/world-championship/2007-2008/sport_sto1550315.shtml)，eurosport2008年4月23日发布，2008年4月26日查阅。

6.

7.

8.

9.  From the 2010/2011 season it shows the ranking at the beginning of
    the season.

10. New players on the Main Tour don't have a ranking.

11. The event ran under the name Jiangsu Classic (2008/2009–2009/2010)

12. The event was called the Players Tour Championship Grand Finals
    (2010/2011–2012/2013)

13. The event was called the Players Championship (2003/2004)

14. The event was called the European Open (2003/2004)

15. The event was called the LG Cup (2003/2004) and the Grand Prix
    (2004/2005–2009/2010)

16.
17.
18.

19.