**丁偉傑**，[香港](../Page/香港.md "wikilink")[now寬頻電視及](../Page/now寬頻電視.md "wikilink")[广州电视竞赛台足球評述員](../Page/广州电视竞赛台.md "wikilink")。丁偉傑旁述風格資訊性甚強，習慣以詳細資料搜集作分析足球賽事，他並將以往評述過的比賽、經年收集得來的資料儲存成為資料庫。早年已攜帶手提電腦進行直播以向觀眾提供足球數據，其手提電腦並非用作上網，而是以自己的資料庫輔助旁述\[1\]。

## 職業生涯

丁偉傑[香港培正中學校友](../Page/香港培正中學.md "wikilink")，[珠海學院新聞系畢業](../Page/珠海學院.md "wikilink")，大專期間已經為[無綫電視體育組擔任助導](../Page/無綫電視.md "wikilink")，主要負責[球迷世界](../Page/球迷世界.md "wikilink")、英超精華等節目的幕後製作，另參與歐洲國家盃及巴塞隆拿奧運製作。畢業後加入新報及太陽報當體育版記者，負責報導本地及國際足球消息。

他於1994年加入[香港有線電視](../Page/香港有線電視.md "wikilink")，擔任資料搜集及職業體育評述員。當時有線[體育台是香港第一個以體育為主的頻道](../Page/香港有線電視體育台.md "wikilink")，購入[ESPN的节目](../Page/ESPN.md "wikilink")，当时ESPN没有自己的评述员，有线就用ESPN的节目加入自己的评述，他可算是香港第一批全職的職業體育評述員。他职业讲波生涯的第一场球賽是荷甲[阿積士對](../Page/阿積士.md "wikilink")[飛燕諾](../Page/飛燕諾.md "wikilink")\[2\]。

1995年是丁偉傑职业生涯的里程碑，他加入總部位於[新加坡的](../Page/新加坡.md "wikilink")[ESPN STAR
Sports](../Page/ESPN_STAR_Sports.md "wikilink")（ESS），於[ESPN及](../Page/ESPN.md "wikilink")[STAR
Sports兩個體育頻道擔任足球評述工作](../Page/衛視體育台.md "wikilink")，效力8年間，主力擔任[NBA及](../Page/NBA.md "wikilink")[英超](../Page/英超.md "wikilink")、[西甲等足球賽事評述](../Page/西甲.md "wikilink")，他與[何輝](../Page/何輝.md "wikilink")、[江忠德及](../Page/江忠德.md "wikilink")[黃興桂的旁述廣獲好評](../Page/黃興桂.md "wikilink")。同時亦兼任[亞洲電視評述員](../Page/亞洲電視.md "wikilink")。

2003年，丁偉傑離開[ESPN STAR
Sports](../Page/ESPN_STAR_Sports.md "wikilink")，加入[廣州電視台擔任評述員](../Page/廣州電視台.md "wikilink")，負責[英超](../Page/英超.md "wikilink")、[意甲](../Page/意甲.md "wikilink")、[世界盃外圍賽等賽事直播](../Page/世界盃.md "wikilink")。同時亦成為[亞洲電視合約評述員](../Page/亞洲電視.md "wikilink")，主要負責[歐洲聯賽冠軍盃及](../Page/歐洲聯賽冠軍盃.md "wikilink")[西甲](../Page/西甲.md "wikilink")。當時他在香港生活，星期六、日則在[廣州擔當評述工作](../Page/廣州.md "wikilink")。

2007年初，因有線另一位旁述員[潘源良請假](../Page/潘源良.md "wikilink")，曾短暫回歸[香港有線電視客串](../Page/香港有線電視.md "wikilink")，旁述[歐洲足協盃賽事](../Page/歐洲足協盃.md "wikilink")。

2007年[now寬頻電視重金奪得香港](../Page/now寬頻電視.md "wikilink")[英超播映權](../Page/英超.md "wikilink")，丁偉傑回到香港發展，轉投[now寬頻電視旗下](../Page/now寬頻電視.md "wikilink")，主力旁述[英超及](../Page/英超.md "wikilink")[意甲四強的賽事](../Page/意甲.md "wikilink")。[英格蘭](../Page/英格蘭.md "wikilink")[足總杯及](../Page/足總杯.md "wikilink")[歐洲聯賽冠軍杯賽事舉行期間](../Page/歐洲聯賽冠軍杯.md "wikilink")，會回到[廣州電視台作為嘉賓旁述賽事](../Page/廣州電視台.md "wikilink")。

現時他亦在香港報章體育版及[雅虎香港撰寫足球專欄](../Page/雅虎.md "wikilink")。

2007年開始他亦在廣東佛山電臺FM92.4頻道，主持電臺節目《同步足球世界》。2013年末約滿後離開該節目。

2015年加盟樂視評述內地粵語英超。[1](http://sports.163.com/15/0607/14/ARGUM61N00051C8U.html)

## 曾旁述重要賽事

  - [1998年世界盃](../Page/1998年世界盃.md "wikilink")（現場旁述）
  - [歐洲聯賽冠軍盃](../Page/歐洲聯賽冠軍盃.md "wikilink")
  - [歐洲足協盃](../Page/歐洲足協盃.md "wikilink")
  - [英格蘭超級足球聯賽](../Page/英格蘭超級足球聯賽.md "wikilink")
  - [西班牙甲組足球聯賽](../Page/西班牙甲組足球聯賽.md "wikilink")
  - [荷蘭足球甲級聯賽](../Page/荷蘭足球甲級聯賽.md "wikilink")
  - [德國足球甲級聯賽](../Page/德國足球甲級聯賽.md "wikilink")

## 個人生活

丁偉傑在[2004年歐洲國家盃後結婚](../Page/2004年歐洲國家盃.md "wikilink")。並於廣州经营的一家高级西餐廳"丁煮場"(荔湾区中山七路富邦中心1楼129-131铺)

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [廣州足球網丁偉傑專訪](https://archive.is/20060829171826/http://news.gzfootball.net/?q=node/174)
  - 雅虎香港 丁偉傑專欄 <http://hk.sports.yahoo.com/stephen/>
  - 丁偉傑博客 <http://dwjworkshop.blog.163.com/>

[丁](../Category/香港主持人.md "wikilink")
[Category:香港廣播主持人](../Category/香港廣播主持人.md "wikilink")
[Category:前香港有線電視足球評述員](../Category/前香港有線電視足球評述員.md "wikilink")
[Category:前香港ESPN足球評述員](../Category/前香港ESPN足球評述員.md "wikilink")
[Category:Now寬頻電視足球評述員](../Category/Now寬頻電視足球評述員.md "wikilink")
[Category:香港培正中學校友](../Category/香港培正中學校友.md "wikilink")
[Category:丁姓](../Category/丁姓.md "wikilink")

1.  [丁偉傑專欄「從史泰路斯的行為　看究竟有多少人希望車路士仍威脅到曼聯」2007年5月6日](http://hk.sports.yahoo.com/070506/308/26ubl.html)
2.  [丁伟杰专访
    丁伟杰官方博客](http://blog.sports268.com/oblog/user1/shewlee/archives/2007/1021180.html)