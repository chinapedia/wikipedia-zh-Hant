[丁日昌肖像.jpg](https://zh.wikipedia.org/wiki/File:丁日昌肖像.jpg "fig:丁日昌肖像.jpg")
[連舉人厝中丁日昌所贈匾額.jpg](https://zh.wikipedia.org/wiki/File:連舉人厝中丁日昌所贈匾額.jpg "fig:連舉人厝中丁日昌所贈匾額.jpg")中[福建巡撫丁日昌所贈](../Page/福建巡撫.md "wikilink")[匾額](../Page/匾額.md "wikilink")，1876年。\]\]

**丁日昌**（），[清朝](../Page/清朝.md "wikilink")[洋务运动主要人物](../Page/洋务运动.md "wikilink")。[字](../Page/表字.md "wikilink")**禹生**\[1\]，又作**雨生**\[2\]，[室名](../Page/室名.md "wikilink")**持静齋**、**清節堂**、**得恩堂**、**百蘭山館**、**蕉雨書屋**。[广东](../Page/广东.md "wikilink")[丰顺人](../Page/丰顺.md "wikilink")，出身[秀才](../Page/秀才.md "wikilink")，曾为[曾国藩](../Page/曾国藩.md "wikilink")、[李鴻章等幕僚](../Page/李鴻章.md "wikilink")，帮助筹办[水师](../Page/水师.md "wikilink")。在任[福州船政期间提出创建](../Page/福州船政.md "wikilink")[北洋](../Page/北洋.md "wikilink")、[东洋](../Page/东洋.md "wikilink")、[南洋三支水师](../Page/南洋.md "wikilink")，分区设防的主张；还提出革新船政，延聘外国人教习技术。1876年到[台湾](../Page/台湾.md "wikilink")，开办[煤矿](../Page/煤矿.md "wikilink")，架起中国第一条自建电报线。制订《[海难救护章程](../Page/海难救护章程.md "wikilink")》，1877年8月，因病回籍休养。1879年[清政府命他专制](../Page/清政府.md "wikilink")[南洋事宜](../Page/南洋.md "wikilink")，节度沿海水师官兵。1881年向[总理衙门建议派人巡抚广西](../Page/总理衙门.md "wikilink")，加强对西南边疆的控制。他在政务之余，悉心读书，尤酷爱搜聚典籍，是清代三大藏书家之一，辑有《[持静斋书目](../Page/持静斋书目.md "wikilink")》五卷。

光緒八年（1882年2月27日），逝世于[广东](../Page/广东.md "wikilink")[揭阳家中](../Page/揭阳.md "wikilink")。卹如制。

## 生平

丁日昌二十歲中[秀才之后](../Page/秀才.md "wikilink")，補為[廪生](../Page/廪生.md "wikilink")，屡考[舉人不中](../Page/舉人.md "wikilink")，[惠潮嘉道](../Page/惠潮嘉道.md "wikilink")[李璋煜见到他文章后](../Page/李璋煜.md "wikilink")，称赞为“不世之才”，聘为[幕僚](../Page/幕僚.md "wikilink")，1854年，[太平军攻打](../Page/太平军.md "wikilink")[潮州城](../Page/潮州.md "wikilink")，丁日昌以[廩貢生治](../Page/廩貢.md "wikilink")[鄉團](../Page/鄉團.md "wikilink")，數卻潮州寇。咸豐六年以军功選[瓊州府府學](../Page/瓊州府.md "wikilink")[訓導](../Page/訓導.md "wikilink")。咸豐九年任[江西](../Page/江西.md "wikilink")[萬安縣](../Page/萬安縣.md "wikilink")[知縣](../Page/知縣.md "wikilink")、
後任[庐陵知县](../Page/吉安縣.md "wikilink")，[髮逆入](../Page/髮逆.md "wikilink")，[罷免](../Page/罷免.md "wikilink")。

1861年为[曾国藩](../Page/曾国藩.md "wikilink")[幕僚](../Page/幕僚.md "wikilink")，1862年5月被派往广东督办[釐金和](../Page/釐金.md "wikilink")[軍火](../Page/軍火.md "wikilink")，復官。[江蘇巡撫](../Page/江蘇巡撫.md "wikilink")[李鴻章治軍](../Page/李鴻章.md "wikilink")[上海](../Page/上海.md "wikilink")，主[機器局](../Page/機器局.md "wikilink")，同治二年積勛[同知銜](../Page/同知.md "wikilink")，升任直隸州[知州](../Page/知州.md "wikilink")，同治四年擢[江蘇](../Page/江苏省_\(清\).md "wikilink")[蘇松太道](../Page/蘇松太道.md "wikilink")[道員](../Page/道員.md "wikilink")，幫李鴻章辦理外交事務，後調兩淮鹽運使\[3\]。同治六年（1867年）升[江蘇布政使](../Page/江蘇布政使.md "wikilink")，同治六年到同治九年為[江蘇巡撫](../Page/江蘇巡撫.md "wikilink")\[4\]，後坐[兵部侍郎銜](../Page/兵部侍郎.md "wikilink")、都察院[右副都御史銜](../Page/右副都御史.md "wikilink")\[5\]。

同治七年（1868年），丁日昌下令严禁“淫词小说”，查禁小说戏曲书目122种，“淫词”唱片114种，续查“淫书”34种，分2批共计268种，含《[紅樓夢](../Page/紅樓夢.md "wikilink")》、《[金瓶梅](../Page/金瓶梅.md "wikilink")》、《[巫山艳史](../Page/巫山艳史.md "wikilink")》、《[杏花天](../Page/杏花天.md "wikilink")》、《[蟫史](../Page/蟫史.md "wikilink")》、《[女仙外史](../Page/女仙外史.md "wikilink")》等。有清一代，丁日昌查禁小说最为彻底\[6\]。

光緒元年到光緒四年任[福建巡撫](../Page/福建巡撫.md "wikilink")\[7\]兼[福建船政大臣](../Page/福建船政大臣.md "wikilink")\[8\]，光緒三年再兼臺灣[學政](../Page/學政.md "wikilink")，光緒五年理[烏石山教案](../Page/烏石山教案.md "wikilink")，詔加[總督銜](../Page/總督.md "wikilink")、兼理各國事務大臣\[9\]。

## 政治主张

  - 发展实业方面：首创轮船航运事业；创设[江南制造总局](../Page/江南制造总局.md "wikilink")；倡办[开平煤矿和](../Page/开平煤矿.md "wikilink")[轮船招商局](../Page/轮船招商局.md "wikilink")；在台湾开矿藏、筑铁路、架电线、造船械、办农垦等。
  - 军事国防方面：提议创建[北洋](../Page/北洋水师.md "wikilink")、东洋、南洋三支水师；建言建策不计资历，从实践中选拔各方面的人才。
  - 外交方面：力主维护主权和收回利权等。
  - 内政方面：提倡清政廉洁，严惩贪官污吏；平反冤狱，清理积案；剔除陋规积弊，蠲减苛捐杂税；注意治水促耕，抢险救灾等。
  - 华侨华工方面：建议禁止外人在沿海诱骗华工出国；建议设市舶司，管理在外国的华侨和华工等。
  - 文化教育方面：建议变八股为八科，改革科举制度；推动和促成派遣第一批留美学童；挑选船政学堂优等生赴欧留学；组织翻译出版西方科技书籍和编撰府志政书；主张在通商口岸创办报馆；倡导广设社学和义学。

## 家世

  - 曾祖父丁世美
  - 祖父丁捷華
  - 父丁賢拔（[丁賢拔墓園](../Page/丁賢拔墓園.md "wikilink")）
  - 子丁惠衡 ; 丁惠馨; 丁惠吉 ; 丁惠康 ; 丁惠宣
  - 孫丁寶元; 丁寶泰 ; 丁寶光

## 著述

  - 《[丁禹生政书](../Page/丁禹生政书.md "wikilink")》
  - 《[抚吴公犊](../Page/抚吴公犊.md "wikilink")》
  - 《[百兰山馆诗集](../Page/百兰山馆诗集.md "wikilink")》
  - 《[百兰山馆政书](../Page/百兰山馆政书.md "wikilink")》

## 注釋

## 参考文献

## 外部链接

{{-}}

[Category:洋務運動領袖](../Category/洋務運動領袖.md "wikilink")
[Category:湘军人物](../Category/湘军人物.md "wikilink")
[Category:清朝福建巡抚](../Category/清朝福建巡抚.md "wikilink")
[Category:清朝江蘇巡撫](../Category/清朝江蘇巡撫.md "wikilink")
[Category:清朝江蘇布政使](../Category/清朝江蘇布政使.md "wikilink")
[Category:清朝萬安縣知縣](../Category/清朝萬安縣知縣.md "wikilink")
[Category:清朝盧陵縣知縣](../Category/清朝盧陵縣知縣.md "wikilink")
[Category:豐順人](../Category/豐順人.md "wikilink")
[Category:揭阳人](../Category/揭阳人.md "wikilink")
[R日](../Category/丁姓.md "wikilink")

1.  國立故宮博物院圖書文獻處清史館傳稿, 701005940號.
2.  《碑傳集》三編, 1冊14卷, p. 855.
3.  國立故宮博物院圖書文獻處清國史館傳稿, 70100588號
4.  中央研究院歷史語言研究所內閣大庫檔案, 015687號
5.  中央研究院歷史語言研究所內閣大庫檔案,1 40618號
6.  《[鲁迅全集](../Page/鲁迅全集.md "wikilink")》卷9, p. 197.
7.  國立故宮博物院圖書文獻處清國史館傳稿, 70100588號
8.  《清代職官年表》, 2冊, p. 1716.
9.  國立故宮博物院圖書文獻處清國史館傳稿, 70100588號.