[Ding_Sheng.jpg](https://zh.wikipedia.org/wiki/File:Ding_Sheng.jpg "fig:Ding_Sheng.jpg")
**丁盛**（），外號**丁大膽**\[1\]，[中華人民共和國](../Page/中華人民共和國.md "wikilink")[江西省](../Page/江西省.md "wikilink")[于都县人](../Page/于都县.md "wikilink")，[中国工农红军](../Page/中国工农红军.md "wikilink")、[八路军](../Page/八路军.md "wikilink")、[中国人民解放军高级将领](../Page/中国人民解放军.md "wikilink")，1955年被授予[中国人民解放军少将军衔](../Page/中国人民解放军少将.md "wikilink")，曾任[广州军区和](../Page/广州军区.md "wikilink")[南京军区](../Page/南京军区.md "wikilink")[司令员](../Page/司令员.md "wikilink")。

## 生平

丁盛生於民國二年的农历十月初十，14歲時就已經參與當時被稱為「紅小鬼」的工作\[2\]。1930年参加[中国工农红军](../Page/中国工农红军.md "wikilink")，當時只有17歲\[3\]。1932年加入[中国共产党](../Page/中国共产党.md "wikilink")。历任班长、连指导员，[红二十八军组织科科长](../Page/红二十八军.md "wikilink")、二团政治委员，参加了[长征](../Page/长征.md "wikilink")。

### 抗日戰爭

[抗日战争时期](../Page/抗日战争.md "wikilink")，他任八路军一二O师三五八旅政治部助理员、科长，挺进军七团政治委员，晋察冀教导二旅一团政治委员。1938年参加[黄土岭围歼战](../Page/黄土岭围歼战.md "wikilink")，击毙包括号称“名将之花”的日军中将[阿部规秀在内共一千四百馀名日军](../Page/阿部规秀.md "wikilink")\[4\]；又曾率領部下參加[百团大战](../Page/百团大战.md "wikilink")，還被日軍称为“守路钉”\[5\]。1945年8月后他从政工干部转为军事主官，任第二十七旅旅长。1947年4月冀察热辽军区部队改编为[东北民主联军第八纵队](../Page/东北民主联军.md "wikilink")，[黄永胜任司令](../Page/黄永胜.md "wikilink")，丁盛任二十四师师长，之后参加了[辽沈战役](../Page/辽沈战役.md "wikilink")。辽沈战役结束后，他任人民解放军四十五军一三五师师长。参加了[平津战役](../Page/平津战役.md "wikilink")，率部攻占天津。1949年9月－10月率领第四野战军45军下的135师参加[衡宝战役](../Page/衡宝战役.md "wikilink")，堵击[白崇禧所部第](../Page/白崇禧.md "wikilink")7军，因此闻名第四野战军。

### 中共建國

[中华人民共和国建立后](../Page/中华人民共和国.md "wikilink")，1953年6月，丁盛加入了中國的「[抗美援朝作战](../Page/韓戰.md "wikilink")」，任中国人民志愿军第54军军长，参加了包括[金城战役的大大小小戰役](../Page/金城战役.md "wikilink")\[6\]。1955年丁盛被授少将衔。

1959年－1961年负责平定[西康省平的](../Page/西康省.md "wikilink")「叛亂工作」\[7\]。1962年10月在[中印边界战争中](../Page/中印边界战争.md "wikilink")，丁盛负责统一指挥[瓦弄地区前线作战](../Page/瓦弄.md "wikilink")。11月对瓦弄地区印军发动攻击，夺回瓦弄地区。1964年5月调任新疆军区生产建设兵团第一副司令员，8月兼[新疆军区副司令员](../Page/新疆军区.md "wikilink")\[8\]。

1968年丁盛任[广州军区副司令员](../Page/广州军区.md "wikilink")\[9\]，颇受[林彪](../Page/林彪.md "wikilink")、[黄永胜看重](../Page/黄永胜.md "wikilink")。1969年任广州军区司令员。1971年[林彪出逃](../Page/林彪.md "wikilink")。1972年的调查结果认为林彪有计划逃往[广州](../Page/广州.md "wikilink")，另立中央。当时丁盛也向[中共中央表示他坚决服从中央的命令](../Page/中共中央.md "wikilink")。1973年[毛泽东决定将八大](../Page/毛泽东.md "wikilink")[军区的司令员互相调换](../Page/军区.md "wikilink")。丁盛与[许世友调换](../Page/许世友.md "wikilink")，来到[南京军区](../Page/南京军区.md "wikilink")。

### 退役及之後

1976年8月8日晚，丁盛同江青集团重要成员[马天水](../Page/马天水.md "wikilink")、[徐景贤](../Page/徐景贤.md "wikilink")、[王秀珍谈话](../Page/王秀珍_\(中共中央委员\).md "wikilink")。对这一夜所谈内容，各方众说纷纭。官方说法是“谈了在毛泽东逝世后可能打内战，并分析了驻[上海附近部队的情况](../Page/上海.md "wikilink")”，徐景贤的证词也类似，但丁盛自己和近来的研究者均认为这一内容不符合当时的情形和会谈者的关系。1977年丁盛因这一谈话遭到批判，事件被稱為「913事件」\[10\]。1982年被命令退出现役，10月开除党籍。后被广州军区按照师级离休待遇接回。闲居之时，常四处游览，昔日战友部下听说他来，纷纷拜访，络绎不绝。1999年9月25日在广州去世。

## 影視形象

  - 电视剧《[亮剑](../Page/亮剑.md "wikilink")》裡的 丁偉
    被認為是從丁盛及[鍾偉二人的經歷為藍本而創作](../Page/鍾偉.md "wikilink")。\[11\]

## 参考资料

  -
  -
{{-}}

[Category:中國人民解放軍少將](../Category/中國人民解放軍少將.md "wikilink")
[Category:南京軍區司令員](../Category/南京軍區司令員.md "wikilink")
[Category:廣州軍區司令員](../Category/廣州軍區司令員.md "wikilink")
[Category:中華人民共和國廣東省省長](../Category/中華人民共和國廣東省省長.md "wikilink")
[Category:中共广东省委书记](../Category/中共广东省委书记.md "wikilink")
[Category:八路軍將領](../Category/八路軍將領.md "wikilink")
[Category:中國工農紅軍將領](../Category/中國工農紅軍將領.md "wikilink")
[Category:朝鮮戰爭人物](../Category/朝鮮戰爭人物.md "wikilink")
[Category:國共戰爭人物](../Category/國共戰爭人物.md "wikilink")
[Category:中國第二次世界大戰人物](../Category/中國第二次世界大戰人物.md "wikilink")
[Category:江西客家人](../Category/江西客家人.md "wikilink")
[Category:于都人](../Category/于都人.md "wikilink")
[S](../Category/丁姓.md "wikilink")
[Category:中国人民解放军开国少将](../Category/中国人民解放军开国少将.md "wikilink")
[Category:被开除中国共产党党籍者](../Category/被开除中国共产党党籍者.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.