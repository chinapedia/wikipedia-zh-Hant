### 七公主

[香港](../Page/香港.md "wikilink")[粵語片時代的其中七位著名](../Page/粵語片.md "wikilink")[上契](../Page/上契.md "wikilink")[結拜女演員的稱號](../Page/結拜.md "wikilink")。當中成員依次序為：
:[馮素波](../Page/馮素波.md "wikilink")（馮寶寶親姊）、[沈芝華](../Page/沈芝華.md "wikilink")、[陳寶珠](../Page/陳寶珠.md "wikilink")、[蕭芳芳](../Page/蕭芳芳.md "wikilink")、[薛家燕](../Page/薛家燕.md "wikilink")、[王愛明及](../Page/王愛明.md "wikilink")[馮寶寶](../Page/馮寶寶.md "wikilink")\[1\]。

## 其他影壇結義

另外同時期[結誼的演員](../Page/結誼.md "wikilink")／導演還包括：

### [七兄弟](../Page/七兄弟.md "wikilink")

  -
    [麥先聲](../Page/麥先聲.md "wikilink")、[尤聲普](../Page/尤聲普.md "wikilink")、[陳玉郎](../Page/陳玉郎.md "wikilink")、[雲展鵬](../Page/雲展鵬.md "wikilink")、[潘有聲](../Page/潘有聲.md "wikilink")、[黎家寶及](../Page/黎家寶.md "wikilink")[文千歲](../Page/文千歲.md "wikilink")。

### [八牡丹](../Page/八牡丹.md "wikilink")

[鳳凰女](../Page/鳳凰女.md "wikilink")（紅牡丹）、[鄧碧雲](../Page/鄧碧雲.md "wikilink")（藍牡丹）、[羅艷卿](../Page/羅艷卿.md "wikilink")（銀牡丹）、[余麗珍](../Page/余麗珍.md "wikilink")（紫牡丹）、[吳君麗](../Page/吳君麗.md "wikilink")（白牡丹）、[于素秋](../Page/于素秋.md "wikilink")（黑牡丹）、[南紅](../Page/南紅.md "wikilink")（綠牡丹）、[林鳳](../Page/林鳳.md "wikilink")（黃牡丹）\[2\]

### [九大姐](../Page/九大姐.md "wikilink")

  -
    [任冰兒](../Page/任冰兒.md "wikilink")、[朱日紅](../Page/朱日紅.md "wikilink")、[金影憐](../Page/金影憐.md "wikilink")、[許卿卿](../Page/許卿卿.md "wikilink")、[黎坤蓮](../Page/黎坤蓮.md "wikilink")、[英麗梨](../Page/英麗梨.md "wikilink")、[李香琴](../Page/李香琴.md "wikilink")、[梁素琴](../Page/梁素琴.md "wikilink")、[譚倩紅](../Page/譚倩紅.md "wikilink")\[3\]

### [十大導](../Page/十大導.md "wikilink")

  -
    [黃岱](../Page/黃岱.md "wikilink")、[莫康時](../Page/莫康時.md "wikilink")、[龍圖](../Page/龍圖\(香港導演\).md "wikilink")、[胡鵬](../Page/胡鵬.md "wikilink")、[馮志剛](../Page/馮志剛.md "wikilink")、[李鐵](../Page/李鐵\(香港導演\).md "wikilink")、[吳回](../Page/吳回_\(香港\).md "wikilink")、[黃鶴聲](../Page/黃鶴聲.md "wikilink")、[盧雨岐](../Page/盧雨岐.md "wikilink")、[珠璣](../Page/珠璣.md "wikilink")\[4\]

### [十二金釵](../Page/十二金釵_\(香港電影\).md "wikilink")

受[1962年](../Page/1962年.md "wikilink")[國語片](../Page/國語片.md "wikilink")《[紅樓夢](../Page/紅樓夢.md "wikilink")》啟發，十二位早期已參演的[中國電影女明星](../Page/中國.md "wikilink")，在[1963年於](../Page/1963年.md "wikilink")[香港結義金蘭](../Page/香港.md "wikilink")，被[香港報章輿論稱為](../Page/香港.md "wikilink")『十二金釵』\[5\]，她們訂下每年每月[農曆初九日舉行一次聚舊](../Page/農曆.md "wikilink")\[6\]，依年齡最大排列：

  - （一）[黎灼灼](../Page/黎灼灼.md "wikilink")，[上海](../Page/上海.md "wikilink")[默片艷星](../Page/默片.md "wikilink")，排行最大、
  - （二）[陳皮梅](../Page/陳皮梅.md "wikilink")，[薛覺先](../Page/薛覺先.md "wikilink")[徒弟](../Page/徒弟.md "wikilink")，[粵劇坤班文武生](../Page/粵劇.md "wikilink")，現在[美國定居](../Page/美國.md "wikilink")，她排行第二、
  - （三）[新加坡](../Page/新加坡.md "wikilink")[粵劇花旦](../Page/粵劇.md "wikilink")[馬笑英排行第三](../Page/馬笑英.md "wikilink")、
  - （四）[李倩顰排行第四](../Page/李倩顰.md "wikilink")、
  - （五）[黃曼梨](../Page/黃曼梨.md "wikilink")，出身於[上海](../Page/上海.md "wikilink")『暨南影片公司』，專拍武俠默片，後到[香港拍](../Page/香港.md "wikilink")[粵語片](../Page/粵語片.md "wikilink")，被譽為『苦情皇后』，她排行第五、
  - （六）[高偉蘭](../Page/高偉蘭.md "wikilink")，1933年[廣州](../Page/廣州.md "wikilink")[默片](../Page/默片.md "wikilink")《[愛的教育](../Page/愛的教育.md "wikilink")》的[女主角](../Page/女主角.md "wikilink")，她的[丈夫是](../Page/丈夫.md "wikilink")[導演](../Page/導演.md "wikilink")[盧敦](../Page/盧敦.md "wikilink")、
  - （七）[李月清第七](../Page/李月清.md "wikilink")、
  - （八）[容玉意](../Page/容玉意.md "wikilink")，出身[上海的艷舞演員](../Page/上海.md "wikilink")，已定居[溫哥華](../Page/溫哥華.md "wikilink")，她排行第八、
  - （九）[黎雯](../Page/黎雯.md "wikilink")，[廣西省](../Page/廣西省.md "wikilink")[梧州](../Page/梧州.md "wikilink")[話劇](../Page/話劇.md "wikilink")[演員](../Page/演員.md "wikilink")，她排行第九、
  - （十）[徐意排行第十](../Page/徐意.md "wikilink")、
  - （十）[甘露](../Page/甘露.md "wikilink")，[粵劇坤班武生](../Page/粵劇.md "wikilink")，她排行第十一、
  - （十一）[鄭雯霞](../Page/鄭雯霞.md "wikilink")，排行第十二，[香港](../Page/香港.md "wikilink")[麗的電視](../Page/麗的電視.md "wikilink")[藝員](../Page/藝員.md "wikilink")。

## 參考

[C](../Category/香港電影女演員.md "wikilink")
[C](../Category/香港粵語片演員.md "wikilink")
[Category:人物並稱](../Category/人物並稱.md "wikilink")
[Category:名數7](../Category/名數7.md "wikilink")

1.  [銀壇「七公主」代表作《七公主》](http://esee99.com/tw/%E8%AB%87%E5%A8%9B%E8%AA%AA%E8%97%9D/941-%E9%8A%80%E5%A3%87%E3%80%8C%E4%B8%83%E5%85%AC%E4%B8%BB%E3%80%8D%E4%BB%A3%E8%A1%A8%E4%BD%9C%E3%80%8A%E4%B8%83%E5%85%AC%E4%B8%BB%E3%80%8B)
2.  [八牡丹粒粒星個個獨當一面](http://orientaldaily.on.cc/cnt/entertainment/20170517/00282_008.html)
3.  [【獨家專訪】譚倩紅丈夫離世消沉一年　與「九大姐」定期聚會自尋快樂人生](https://bka.mpweekly.com/interview/%E5%A8%9B%E6%A8%82123/20180101-96094)
4.  [從十大導作品窺粵片概況](http://news.takungpao.com.hk/paper/q/2013/0919/1914067.html)
5.  [星光大道──五六十年代香港影壇風貌
    P. 214](https://books.google.com.hk/books?id=fLzsDQAAQBAJ&pg=PT214&lpg=PT214&dq=%E5%8D%81%E4%BA%8C%E9%87%91%E9%87%B5+%E9%BB%83%E6%9B%BC%E6%A2%A8&source=bl&ots=90QbIr20nX&sig=c34IO6tgwS-0aOSJhMvOEn5qAsg&hl=en&sa=X&ved=0ahUKEwizouTm4PLaAhUCWbwKHdPSAogQ6AEIfjAL#v=onepage&q=%E5%8D%81%E4%BA%8C%E9%87%91%E9%87%B5%20%E9%BB%83%E6%9B%BC%E6%A2%A8&f=false)
6.  1978年2月15日，星期三，香港《工商晚報》，第六頁。