**七叶树亚科**（Hippocastanoideae）為[無患子科下之一亞科](../Page/無患子科.md "wikilink")。該亞科於植物分類表上歸於[被子植物門](../Page/被子植物門.md "wikilink")（Angiospermae）[雙子葉植物綱](../Page/雙子葉植物綱.md "wikilink")（Magnoliopsida）[無患子目](../Page/無患子目.md "wikilink")（Sapindales）[無患子科](../Page/無患子科.md "wikilink")（Sapindaceae）。\[1\]

## 分类

本亚科包含五个属：

  - [枫属](../Page/枫属.md "wikilink") *Acer* [L.](../Page/L..md "wikilink")
  - [七叶树属](../Page/七叶树属.md "wikilink") *Aesculus* L.
  - [三叶树属](../Page/三叶树属.md "wikilink") *Billia*
    [Peyr.](../Page/Peyr..md "wikilink")
  - [金钱槭属](../Page/金钱槭属.md "wikilink") *Dipteronia*
    [Oliv.](../Page/Oliv..md "wikilink")
  - [掌叶木属](../Page/掌叶木属.md "wikilink") *Handeliodendron*
    [Rehder.](../Page/Rehder..md "wikilink")

## 参考文献

## 外部連結

  -
[七葉樹亞科](../Category/七葉樹亞科.md "wikilink")
[Category:无患子科](../Category/无患子科.md "wikilink")

1.