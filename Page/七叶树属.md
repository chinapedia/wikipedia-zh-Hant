**七叶树属**包括20-25种生长在北半球[温带地区的落叶](../Page/温带.md "wikilink")[树或](../Page/树.md "wikilink")[灌木](../Page/灌木.md "wikilink")。其中有7-10种是原生于[北美洲的](../Page/北美洲.md "wikilink")，有13-15种是原生于[欧](../Page/欧洲.md "wikilink")[亚大陆的](../Page/亚洲.md "wikilink")。七叶树属原来是单独的一个[科](../Page/科.md "wikilink")，后来根据[基因判断](../Page/基因.md "wikilink")，和[槭树科一起合并到](../Page/槭树科.md "wikilink")[无患子科内](../Page/无患子科.md "wikilink")，但有的学者认为它们都是独立起源的，应该分成单独的[科](../Page/科.md "wikilink")。

在[美洲](../Page/美洲.md "wikilink")，七叶树一般被称为“鹿瞳”，因为其果实是棕[黄色](../Page/黄色.md "wikilink")，类似[鹿眼睛的](../Page/鹿.md "wikilink")[颜色](../Page/颜色.md "wikilink")，在[欧洲被称为](../Page/欧洲.md "wikilink")“马栗”，其中“[马](../Page/马.md "wikilink")”的意思并不是供马吃的，而是“强有力”的意思，指其坚硬难以食用。[中国称为](../Page/中国.md "wikilink")“天师栗”或“猴板栗”。

七叶树属的[植物都是](../Page/植物.md "wikilink")[乔木](../Page/乔木.md "wikilink")，高达4-35[米](../Page/米.md "wikilink")，树根含树脂；[叶子分为七瓣掌状](../Page/叶.md "wikilink")，最大可达65厘米宽；[花为](../Page/花.md "wikilink")4-5瓣，花期长达80-110天；果实[直径为](../Page/直径.md "wikilink")2-5厘米，坚果类似[栗子](../Page/栗.md "wikilink")，但果壳中只有一个或两个种子，有的种类果壳有软刺，有的没有，成熟时果壳裂成三瓣，放出种子。

[Aesculus_carnea_BotGartenMuenster_PurpurKastanie_6685.jpg](https://zh.wikipedia.org/wiki/File:Aesculus_carnea_BotGartenMuenster_PurpurKastanie_6685.jpg "fig:Aesculus_carnea_BotGartenMuenster_PurpurKastanie_6685.jpg")
有许多种七叶树因为其花色美丽，是作为观赏植物栽培的。

七叶树一般没有病虫害，但也能感染一些[真菌或被某些](../Page/真菌.md "wikilink")[昆虫作为](../Page/昆虫.md "wikilink")[食物](../Page/食物.md "wikilink")，可是不会造成重大的灾害。

七叶树的果实含有大量的皂角苷，叫做七叶树素，是破坏[红血球的有毒物质](../Page/红血球.md "wikilink")，但有的[动物例如](../Page/动物.md "wikilink")[鹿和](../Page/鹿.md "wikilink")[松鼠可以抵御这种毒素食用七叶树的果实](../Page/松鼠.md "wikilink")。有人用它们的果实磨粉毒[鱼](../Page/鱼.md "wikilink")。
加州七叶树由于其[花蜜中也含有毒素可以造成某些种类的](../Page/花蜜.md "wikilink")[蜜蜂中毒](../Page/蜜蜂.md "wikilink")，但当地土生的蜜蜂可以抵御这种毒素。这种毒素不耐高温，经蒸煮后种子中的[淀粉可以被食用](../Page/淀粉.md "wikilink")。中国七叶树的种子是一种[中药](../Page/中药.md "wikilink")，名为娑罗子，所以有时中国七叶树也被称为[娑羅樹](../Page/娑羅樹.md "wikilink")。天师栗的种子也可以作为娑罗子入药。

[Paardekastanje_wit_uitgelopen_knop.jpg](https://zh.wikipedia.org/wiki/File:Paardekastanje_wit_uitgelopen_knop.jpg "fig:Paardekastanje_wit_uitgelopen_knop.jpg")

## 外部链接

  - [美国药典](http://www.henriettesherbal.com/eclectic/kings/aesculus.html)
  - [药用价值](http://www.homeoint.org/books3/kentmm/aesc-hip.htm)
  - [七叶树属](https://web.archive.org/web/20160304093317/http://herbstopic.com/?p=365&lang=zh-cn)
  - [七葉樹屬,
    Aesculus](http://libproject.hkbu.edu.hk/was40/search?lang=ch&channelid=1288&searchword=parent_latin=Aesculus)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[\*](../Category/七叶树属.md "wikilink")
[Category:中药](../Category/中药.md "wikilink")
[Category:藥用植物](../Category/藥用植物.md "wikilink")