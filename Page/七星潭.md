[Taiwan_2009_HuaLien_City_CiSingTan_Bay_FRD_8343.jpg](https://zh.wikipedia.org/wiki/File:Taiwan_2009_HuaLien_City_CiSingTan_Bay_FRD_8343.jpg "fig:Taiwan_2009_HuaLien_City_CiSingTan_Bay_FRD_8343.jpg")

**七星潭**，又稱**月牙灣**，位於[臺灣](../Page/臺灣.md "wikilink")[花蓮縣](../Page/花蓮縣.md "wikilink")[新城鄉大漢村](../Page/新城鄉_\(臺灣\).md "wikilink")、[花蓮市北郊](../Page/花蓮市.md "wikilink")、[空軍花蓮基地東側](../Page/花蓮機場.md "wikilink")。七星潭是花蓮縣唯一的縣級風景區，連接[太魯閣國家公園](../Page/太魯閣國家公園.md "wikilink")、[東海岸和](../Page/東部海岸國家風景區.md "wikilink")[花東縱谷國家風景區](../Page/花東縱谷國家風景區.md "wikilink")。

## 概述

七星潭（Malongayangay）
原本位於今日[花蓮機場東側位置](../Page/花蓮機場.md "wikilink")，為南北縱向長條湖泊，北端為L型勺口，南端為勺柄，整體約略類似北斗七星，故名七星潭。1936年[日據時期](../Page/台灣日治時期.md "wikilink")，日本當局為了建設花蓮港北飛行場（現今的[花蓮機場](../Page/花蓮機場.md "wikilink")）就填平了七星潭，現今只剩約四個零星湖泊。七星潭殘湖主要是[國立東華大學美崙校區的涵翠湖](../Page/國立東華大學美崙校區.md "wikilink")，和[空軍花蓮基地內的兩個小池塘](../Page/花蓮機場.md "wikilink")。

月牙灣（Cikatingan）
，是原七星潭居民被日本當局遷居的地方，由於他們自認為七星潭人並懷念七星潭，就將新遷居之地稱為七星潭。現今七星潭為瀕臨太平洋的弧形[海灣](../Page/海灣.md "wikilink")，由大大小小的礫石所鋪陳。地質上屬陡降型沙灘，[瘋狗浪出沒](../Page/瘋狗浪.md "wikilink")，為全台十大危險海域之一\[1\]\[2\]。此地昔日是小漁村，同時也是花蓮定置漁業發達興盛之地，原因係當地海域缺乏天然灣澳，然而因[黑潮流過](../Page/黑潮.md "wikilink")[台灣東部](../Page/台灣東部.md "wikilink")，帶來各種迴游的魚類資源，加上[中央山脈的山腳緊迫於海濱](../Page/中央山脈.md "wikilink")，沿岸的流水深且急，近海魚群在此匯集成天然漁場之故。

近年來，[花蓮縣政府於七星潭興建石雕園區](../Page/花蓮縣政府.md "wikilink")、賞星廣場、觀日樓、兒童遊樂場、港濱自行車道等休憩設施、在漁場附近設有海岸生態的解說牌，並且利用防風林區闢建德燕海濱植物園區，加上附近動植物生態非常豐富，已成為花蓮縣境內著名的休憩景點

## 交通

  - [縣道193號](../Page/縣道193號.md "wikilink")

## 週遭附近

[Taiwan_2009_HuaLien_City_YuanYeh_Resort_Overlooking_CiSingTan_Bay_FRD_8295.jpg](https://zh.wikipedia.org/wiki/File:Taiwan_2009_HuaLien_City_YuanYeh_Resort_Overlooking_CiSingTan_Bay_FRD_8295.jpg "fig:Taiwan_2009_HuaLien_City_YuanYeh_Resort_Overlooking_CiSingTan_Bay_FRD_8295.jpg")

  - 原野牧場
  - 港濱自行車道
  - 環保公園
  - [七星柴魚博物館](../Page/七星柴魚博物館.md "wikilink")
  - [花蓮機場](../Page/花蓮機場.md "wikilink")
  - [國立東華大學美崙校區](../Page/國立東華大學.md "wikilink")
  - [天主教海星高級中學](../Page/花蓮縣私立海星高級中學.md "wikilink")

## 圖片

Image:Taiwan 2009 HuaLien City CiSingTan Left FRD 8202 Pano Extracted
Left.jpg Image:Taiwan 2009 HuaLien City CiSingTan Right FRD 8193 Pano
Extracted Right.jpg Image:Taiwan 2009 HuaLien City CiSingTan Bay Biking
FRD 8365.jpg Image:Taiwan 2009 HuaLien City CiSingTan Bay First Light
FRD 8235.jpg Image:Taiwan 2009 HuaLien City CiSingTan Bay FRD 8326 Book
Front Cover.jpg Image:Taiwan 2009 HuaLien City CiSingTan Bay FRD
8387.jpg Image:Taiwan 2009 HuaLien City CiSingTan FRD 8248.jpg
Image:Chihsingtan_Beach_1.JPG Image:七星潭海邊.jpg Image:Cisingtan in
Hualien (5453202297).jpg Image:Cisingtan in Hualien (5453195141).jpg
Image:Cisingtan in Hualien (5453809838).jpg <File:Cisingtan> in Hualien
(5453817166).jpg <File:Cisingtan> in Hualien (5453209433).jpg
<File:2004.02.01.七星潭> - panoramio.jpg

## 參考資料

## 外部連結

  - [花蓮縣觀光資訊網](http://tour-hualien.hl.gov.tw/)
  - [花蓮黃頁網路電話簿 旅遊景點 七星潭](http://www.tel038.com.tw/news_po1.php?no=21)
  - [花蓮旅人誌](http://www.hl-net.com.tw/blog/kview.php?kd=%E4%B8%83%E6%98%9F%E6%BD%AD)
  - [愛玩地圖：花蓮七星潭周遭地區始末](http://theericel.blogspot.tw/2014/05/blog-post.html)

[Category:台灣沙灘](../Category/台灣沙灘.md "wikilink")
[Category:花蓮縣旅遊景點](../Category/花蓮縣旅遊景點.md "wikilink")
[Category:東部海岸國家風景區](../Category/東部海岸國家風景區.md "wikilink") [Category:新城鄉
(台灣)](../Category/新城鄉_\(台灣\).md "wikilink")
[Category:臺灣危險海域](../Category/臺灣危險海域.md "wikilink")

1.
2.  。