**万全区**是[中国](../Page/中华人民共和国.md "wikilink")[河北省](../Page/河北省.md "wikilink")[张家口市下辖的一个区](../Page/张家口市.md "wikilink")。地处河北省西北部，西、北以明长城为界与尚义、张北两县接壤，南隔[洋河与](../Page/洋河.md "wikilink")[怀安县相望](../Page/怀安县.md "wikilink")，东临[张家口市桥西区](../Page/张家口市.md "wikilink")，总面积
1161平方公里，东西长37公里，南北宽35公里。属东亚大陆性季风气候，年均气温6.9℃，年降水量464毫米，年均积温2788℃。

## 历史

万全区在河北省张家口市西部，[外长城内侧](../Page/长城.md "wikilink")。[京包铁路经过境内南部](../Page/京包铁路.md "wikilink")。[清朝时置万全县](../Page/清朝.md "wikilink")。1958年曾撤消并入[怀安县](../Page/怀安县.md "wikilink")，1961年恢复设置，县城一直为万全城（今万全镇），1983
年 7 月 1
日县城由万全城迁到孔家庄镇。2016年行政区划调整，撤销万全县，设置万全区。农产品以[高粱](../Page/高粱.md "wikilink")、[谷子](../Page/谷子.md "wikilink")、[玉米](../Page/玉米.md "wikilink")、[马铃薯](../Page/马铃薯.md "wikilink")、豆类为主。畜牧业发达。矿产有[煤](../Page/煤.md "wikilink")。[工业有](../Page/工业.md "wikilink")[化学](../Page/化学.md "wikilink")、轻纺、机械、建材等。名胜古迹有[万全右卫城](../Page/万全右卫城.md "wikilink")、野狐嶺古戰場（1211年[成吉思汗](../Page/成吉思汗.md "wikilink")[蒙古軍大敗金軍](../Page/蒙古.md "wikilink")40萬於此）。

## 行政区划

下辖4个[镇](../Page/行政建制镇.md "wikilink")、7个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 经济

2010年GDP总产值365578万元

## 少数民族

全县少数民族人口426人，其中蒙古族87人；回族35人；藏族2人；维吾尔族3人；苗族14人；彝族16人；壮族1人；布依族6人；朝鲜族2人；满族240人；瑶族4人；土家族5人；哈尼族4人；傈僳族1人；仫佬族2人；毛南族1人；锡伯族3人。

## 参考文献

[万全县](../Page/category:万全县.md "wikilink")
[县](../Page/category:张家口区县.md "wikilink")
[张家口](../Page/category:河北省县份.md "wikilink")
[冀](../Page/category:国家级贫困县.md "wikilink")