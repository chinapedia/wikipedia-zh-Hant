**万江街道**是[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[东莞市下辖的一个](../Page/东莞市.md "wikilink")[街道](../Page/街道_\(行政区划\).md "wikilink")\[1\]。面积50.5平方公里，常住人口6.6万。境内[107国道](../Page/107国道.md "wikilink")、[莞深高速公路贯穿全境](../Page/莞深高速公路.md "wikilink")。

全世界最大的购物广场[华南摩尔坐落于此](../Page/华南摩尔.md "wikilink")。东莞市汽车客运总站于此。

## 行政区划

万江街道下辖以下地区：\[2\]

。

## 参考资料

## 外部链接

  - [万江概况](http://www.dgpage.com/today_dongguan/wangjian.asp)，东莞在线。
  - [东莞市万江区办事处网站](https://web.archive.org/web/20061106210455/http://www.wanjiang.dg.gov.cn/)

[Category:东莞市街道](../Category/东莞市街道.md "wikilink")

1.
2.