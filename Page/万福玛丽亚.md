《**万福玛丽亚**》（[英语](../Page/英语.md "wikilink")：******；[西班牙语](../Page/西班牙语.md "wikilink")：******）是[哥伦比亚和](../Page/哥伦比亚.md "wikilink")[美国合作拍摄的一部电影](../Page/美国.md "wikilink")，由[乔舒亚·马斯顿编剧并导演](../Page/乔舒亚·马斯顿.md "wikilink")。扮演电影主角玛丽亚的演员[卡塔利娜·桑迪诺·莫雷诺因此在](../Page/卡塔利娜·桑迪诺·莫雷诺.md "wikilink")[柏林国际电影节赢得](../Page/柏林国际电影节.md "wikilink")[最佳女演员银熊奖](../Page/最佳女演员银熊奖.md "wikilink")，并在[第77届奥斯卡金像奖获提名为](../Page/第77届奥斯卡金像奖.md "wikilink")[最佳女主角](../Page/奥斯卡最佳女主角奖.md "wikilink")。

## 内容简介

影片讲述一个名叫玛丽亚·阿尔瓦雷斯（卡塔利娜·桑迪诺·莫雷诺饰演）的17岁哥[伦比亚少女](../Page/伦比亚.md "wikilink")[走私毒](../Page/走私.md "wikilink")[品到美国的经过](../Page/品.md "wikilink")。她吞下数十个毒品胶囊从[波哥大搭乘飞机到](../Page/波哥大.md "wikilink")[纽约市](../Page/纽约市.md "wikilink")，到达后美国海关怀疑她藏有毒品，遂要求她进行X光检查，但在发现她[怀孕后予以放行](../Page/怀孕.md "wikilink")。在一家宾馆里，毒贩剖开了另一名运毒者露西的腹部，以取得她藏有的毒品。玛丽亚发现后逃离宾馆，前往露西的姐姐家中寄居。

## 外部链接

  - [官方网站](http://www.mariafullofgrace.com/main.html)

  -
[Category:2004年电影](../Category/2004年电影.md "wikilink")
[Category:美國電影作品](../Category/美國電影作品.md "wikilink")
[Category:哥倫比亞影業電影](../Category/哥倫比亞影業電影.md "wikilink")
[Category:導演處女作](../Category/導演處女作.md "wikilink")
[Category:美國劇情犯罪片](../Category/美國劇情犯罪片.md "wikilink")
[Category:美國犯罪驚悚片](../Category/美國犯罪驚悚片.md "wikilink")
[Category:2000年代劇情犯罪片](../Category/2000年代劇情犯罪片.md "wikilink")
[Category:2000年代犯罪驚悚片](../Category/2000年代犯罪驚悚片.md "wikilink")
[Category:毒品相關電影](../Category/毒品相關電影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:西班牙语电影](../Category/西班牙语电影.md "wikilink")
[Category:聖丹斯影展獲獎者](../Category/聖丹斯影展獲獎者.md "wikilink")
[Category:2000年代独立电影](../Category/2000年代独立电影.md "wikilink")
[Category:美国独立电影](../Category/美国独立电影.md "wikilink")
[Category:哥倫比亞背景電影](../Category/哥倫比亞背景電影.md "wikilink")
[Category:哥倫比亞取景電影](../Category/哥倫比亞取景電影.md "wikilink")
[Category:紐約市背景電影](../Category/紐約市背景電影.md "wikilink")
[Category:未成年懷孕題材電影](../Category/未成年懷孕題材電影.md "wikilink")