**三个世界理论**指[毛泽东在](../Page/毛泽东.md "wikilink")[冷战背景下](../Page/冷战.md "wikilink")，根据[第二次世界大战后](../Page/第二次世界大战.md "wikilink")[国际关系的新格局把](../Page/国际关系.md "wikilink")[国际社会划分为](../Page/国际社会.md "wikilink")3个部分的[国际关系理论](../Page/国际关系理论.md "wikilink")。

毛泽东这一思想萌芽于1940年代的[中间地带论思想](../Page/中间地带论.md "wikilink")，雏形于1960年代的中间地带论外交战略，形成于1970年代。

## 提出

1973年6月22日，毛泽东在会见[马里国家元首](../Page/马里.md "wikilink")[特拉奥雷时提到](../Page/特拉奥雷.md "wikilink")：“我们都是叫做第三世界，就是发展中国家”。1974年2月22日，毛泽东在会见[赞比亚总统](../Page/赞比亚总统.md "wikilink")[卡翁达时首次公开提出这一思想](../Page/卡翁达.md "wikilink")，“我看美国、苏联是第一世界。中间派，日本、欧洲、澳大利亚、加拿大，是第二世界。咱们是第三世界。”“亚洲除了日本，都是第三世界，整个非洲都是第三世界，拉丁美洲也是第三世界。”

1974年4月，邓小平在联合国第六届特别会议上发言，第一次向世界全面阐述了划分“三个世界”的战略思想。

## 内容

三个世界理论中的“三个世界”的涵义是：

  - [第一世界是指](../Page/第一世界.md "wikilink")[美国和](../Page/美国.md "wikilink")[苏联两个超级大国](../Page/苏联.md "wikilink")，在世界范围内推行[霸权主义的](../Page/霸权主义.md "wikilink")[超级大国](../Page/超级大国.md "wikilink")。
  - [第二世界是指处于这两者之间的](../Page/第二世界.md "wikilink")[发达国家](../Page/发达国家.md "wikilink")，如[英国](../Page/英国.md "wikilink")、[德国](../Page/德国.md "wikilink")、[日本](../Page/日本.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[澳大利亚等国](../Page/澳大利亚.md "wikilink")。
  - [第三世界指](../Page/第三世界.md "wikilink")[亚洲](../Page/亚洲.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、[拉丁美洲和其他地区的](../Page/拉丁美洲.md "wikilink")[发展中国家和](../Page/发展中国家.md "wikilink")[未開發國家](../Page/未開發國家.md "wikilink")。

其中以第三世界國家最多，而且彼此大有差別（除最貧窮的國家外，還包括盛產石油的國家），因此無法給出適當的定義。有些人（包括聯合國）還承認存在一個包括25個最貧窮國家的第四世界。

第三世界國家往往採取中立的立場，因而從政治上把世界一分為三。

第三世界是反对[帝国主义](../Page/帝国主义.md "wikilink")、[殖民主义](../Page/殖民主义.md "wikilink")、[霸权主义的主要力量](../Page/霸权主义.md "wikilink")。

## 批评

  - [阿尔巴尼亚领导人](../Page/阿尔巴尼亚.md "wikilink")[恩维尔·霍查强烈反对三个世界理论](../Page/恩维尔·霍查.md "wikilink")，他认为三个世界理论掩盖了时代的基本特征，即[帝国主义和](../Page/帝国主义.md "wikilink")[无产阶级革命时代的基本矛盾](../Page/无产阶级革命.md "wikilink")，鼓吹阶级合作，从本质上说是[阶级斗争熄灭论的变种](../Page/阶级斗争.md "wikilink")。三个世界理论和“不结盟世界”理论、“南北世界”理论、“[发展中国家](../Page/发展中国家.md "wikilink")”理论一样，都不是以[阶级为标准来划分的](../Page/阶级.md "wikilink")，因此不是[马列主义的理论](../Page/马列主义.md "wikilink")\[1\]。霍查这一批评得到了很多[反修正主义政党和组织的支持](../Page/反修正主义.md "wikilink")，它们因此放弃[毛主义](../Page/毛主义.md "wikilink")，转而追随[阿尔巴尼亚劳动党的政治路线](../Page/阿尔巴尼亚劳动党.md "wikilink")，形成了[国际共产主义运动中的](../Page/国际共产主义运动.md "wikilink")[霍查派](../Page/霍查主义.md "wikilink")。\[2\]
  - 美国革命共产党刊物《革命》1978年发表文章认为，中国修正主义者将三个世界理论称为“面向国际无产阶级和被压迫人民的全球战略”，这绝非毛泽东的本意，将三个世界理论归于毛泽东是一种伪造，三个世界理论是反革命路线。\[3\]

## 参考文献

## 參見

  - [第一世界](../Page/第一世界.md "wikilink")、[第二世界](../Page/第二世界.md "wikilink")、[第三世界](../Page/第三世界.md "wikilink")
  - [毛泽东思想](../Page/毛泽东思想.md "wikilink")
  - [霍查主义](../Page/霍查主义.md "wikilink")

<!-- end list -->

  - 类似的[国际关系理论](../Page/国际关系理论.md "wikilink")

<!-- end list -->

  - [三个世界模式](../Page/三个世界模式.md "wikilink")

{{-}}

[三个世界](../Category/三个世界.md "wikilink")
[Category:毛泽东思想](../Category/毛泽东思想.md "wikilink")
[Category:国际关系理论](../Category/国际关系理论.md "wikilink")
[Category:中华人民共和国外交术语](../Category/中华人民共和国外交术语.md "wikilink")

1.
2.  [当代国际共运史的主线：毛主义和现代修正主义的斗争](http://www.wyzxwk.com/Article/guoji/2009/09/14526.html)
3.