[Mitsui_Main_Building_2009.jpg](https://zh.wikipedia.org/wiki/File:Mitsui_Main_Building_2009.jpg "fig:Mitsui_Main_Building_2009.jpg")。東京都中央區日本橋室町\]\]
**三井财阀**（日语假名：****），是[日本三大](../Page/日本.md "wikilink")[财阀之一](../Page/财阀.md "wikilink")。其最初的企业为[三井高俊创立的](../Page/三井高俊.md "wikilink")[三井越后屋](../Page/三井越後屋.md "wikilink")。目前包括[银行](../Page/银行.md "wikilink")、零售等多个产业。

## 歷史

### 江户时代

三井越后屋（即今日的[三越](../Page/三越.md "wikilink")）是由[伊势国](../Page/伊势国.md "wikilink")[松阪市的商人](../Page/松阪市.md "wikilink")[三井高利所创办的](../Page/三井高利.md "wikilink")。当时他采用了小量商品贩卖以及薄利多销等创造性的商业手法，获得了巨大的成功。此后，财团为[江戶幕府提供金融](../Page/江戶幕府.md "wikilink")[汇兑服务等](../Page/汇兑.md "wikilink")，渐渐成为江户幕府的御用商人，发展成为日本首屈一指的富豪。

在[江户时代末期](../Page/江户时代.md "wikilink")，三井财团也和其他商团一样受到政治动荡的影响，但在进入[明治时期后依然保持了发展的趋势](../Page/明治.md "wikilink")。

### 明治以后

财团聘用了[三野村利左卫门](../Page/三野村利左卫门.md "wikilink")、[益田孝](../Page/益田孝.md "wikilink")、[中上川彦次郎等经营人才](../Page/中上川彦次郎.md "wikilink")，逐渐走上了近代化经营的道路。

1876年，[三井银行和](../Page/三井银行.md "wikilink")[三井物产创立](../Page/三井物产.md "wikilink")。此后，财团不断扩大产业领域，进入了[纺织业](../Page/纺织.md "wikilink")、[采矿业和](../Page/采矿.md "wikilink")[机械制造业等行业](../Page/机械.md "wikilink")。1909年，成立[三井合名会社](../Page/三井合名会社.md "wikilink")。当时已经成为日本规模最大的财阀。

[二次世界大战结束以后](../Page/二次世界大战.md "wikilink")，以[美軍為主](../Page/美軍.md "wikilink")，对日本实行[軍事佔領的](../Page/軍事佔領.md "wikilink")[駐日盟軍總司令部](../Page/駐日盟軍總司令部.md "wikilink")（[GHQ](../Page/GHQ.md "wikilink")）发布了[财阀解体的命令](../Page/财阀解体.md "wikilink")，因此，三井财阀也被迫解体。

### 当代（二战后）

三井财阀与日本其他大财团一样，建立庞大的产业集团。但是相对于[三菱集团](../Page/三菱集团.md "wikilink")、[住友集团](../Page/住友集团.md "wikilink")，[三井集團较为松散](../Page/三井集團.md "wikilink")，也包含如[丰田汽车](../Page/丰田汽车.md "wikilink")、[东芝电器等独立性较强的公司](../Page/东芝电器.md "wikilink")。

2001年，三井集团的核心企业[樱花银行与住友集团的核心银行](../Page/樱花银行.md "wikilink")[住友银行合并](../Page/住友银行.md "wikilink")，成立了[三井住友银行](../Page/三井住友银行.md "wikilink")。从此在金融行业诞生了[三井住友金融集团](../Page/三井住友金融集团.md "wikilink")。在其他一些领域，原三井财阀的公司也与住友集团的公司积极展开了合并或者合作的活动，如日本最大的海运公司[商船三井就是由原住友集团的大阪商船与三井财阀的三井船舶于](../Page/商船三井.md "wikilink")1964年合并而成。

## 系統圖

-----

## 註解

## 关联条目

  - [三井家](../Page/三井家.md "wikilink")
  - [团琢磨](../Page/团琢磨.md "wikilink")

[Category:日本財閥](../Category/日本財閥.md "wikilink")
[Category:三井集團](../Category/三井集團.md "wikilink")