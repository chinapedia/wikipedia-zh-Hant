**[道家三十六小洞天](../Page/道家.md "wikilink")**，相对于[十大洞天而言](../Page/十大洞天.md "wikilink")，其“亦上仙所统治之处也”\[1\]。

## 分类

  - 霍桐山洞——周回三千里，名曰霍林洞天，在福州[长溪县](../Page/长溪.md "wikilink")，属仙人王纬玄治之。
  - 东岳[泰山洞](../Page/泰山.md "wikilink")——周回一千里，名曰蓬玄洞天，在衮州乾封县，属山图公子治之。
  - 南岳[衡山洞](../Page/衡山.md "wikilink")——周回七百里，名曰朱陵洞天，在衡州衡山县，仙人石长生治之。
  - 西岳[华山洞](../Page/华山.md "wikilink")——周回三百里，名曰惣仙洞天，在华州[华阴县](../Page/华阴.md "wikilink")，真人惠车子主之。
  - 北岳[恒山洞](../Page/恒山.md "wikilink")——周回三千里，名曰惣玄洞天，在恒州常山曲阳县，真人郑子真治之。
  - 中岳[嵩山洞](../Page/嵩山.md "wikilink")——周回三千里，名曰司马洞天，在东都[登封县](../Page/登封.md "wikilink")，仙人邓云山治之。
  - [峨眉山洞](../Page/峨眉山.md "wikilink")——周回三百里，名曰虚陵洞天，在嘉州[峨嵋县](../Page/峨嵋.md "wikilink")，真人唐览治之。
  - [庐山洞](../Page/庐山.md "wikilink")——周回一百八十里，名曰洞灵真天，在江州[德安县](../Page/德安.md "wikilink")，真人周正时治之。
  - [四明山洞](../Page/四明山.md "wikilink")——周回一百八十里，名曰山赤水天，在越州[上虞县](../Page/上虞.md "wikilink")，真人刁道林治之。
  - [会稽山洞](../Page/会稽山.md "wikilink")——周回三百五十里，名曰极玄大亢天，在越州[山阴县镜湖中](../Page/山阴.md "wikilink")，仙人郭华治之。
  - [太白山洞](../Page/太白山.md "wikilink")——周回五百里，名曰玄德洞天，在京北府[长安县连终南山](../Page/长安.md "wikilink")，仙人张季连治之。
  - 西山洞——周回三百里，名曰天柱宝极玄天，在[洪州](../Page/洪州.md "wikilink")[南昌县](../Page/南昌.md "wikilink")，真人唐公成治之。
  - 小沩山洞——周回三百里，名曰好生玄上天，在[潭州](../Page/潭州.md "wikilink")[澧陵县](../Page/醴陵.md "wikilink")，仙人花丘林治之。
  - 潜山洞——周回八十里，名曰天柱司玄天，在[舒州](../Page/舒州.md "wikilink")[怀宁县](../Page/怀宁.md "wikilink")，仙人稷丘子治之。
  - 鬼谷山洞——周回七十里，名曰贵玄司真天，在信州[贵溪县](../Page/贵溪.md "wikilink")，真人崔文子治之。
  - [武夷山洞](../Page/武夷山.md "wikilink")——周回一百二十里，名曰真升化玄天，在建州[建阳县](../Page/建阳.md "wikilink")，真人刘少公治之。
  - 笥山洞——周回一百二十里，名曰太玄法乐天，在[吉州](../Page/吉州.md "wikilink")[永新县](../Page/永新.md "wikilink")，真人梁伯鸾主之。
  - 华盖山洞——周回四十里，名曰容成大玉天，在[温州](../Page/温州.md "wikilink")[永嘉县](../Page/永嘉.md "wikilink")，仙人平公修治之。
  - 盖竹山洞——周回八十里，名曰长耀宝光天，在[台州](../Page/台州.md "wikilink")[黄岩县](../Page/黄岩.md "wikilink")，属仙人商丘子治之。
  - 都峤山洞——周回一百八十里，名曰宝玄洞天，在容州[普宁县](../Page/普宁.md "wikilink")，仙人刘根治之。
  - 白石山洞——周回七十里，名曰秀乐长真天，在云和州[含山县](../Page/含山.md "wikilink")，白真人治之。
  - 岣漏山洞——周回四十里，名曰玉阙宝圭天，在客州[北流县](../Page/北流.md "wikilink")，属仙人饯真人治之。
  - 疑山洞——周回三千里，名曰朝真太虚天，在[道州](../Page/道州.md "wikilink")[延唐县](../Page/延唐.md "wikilink")，仙人严真青治之。
  - [洞阳山洞](../Page/洞阳山.md "wikilink")——周回一百五十里，名曰洞阳、隐观天，在[潭州](../Page/潭州.md "wikilink")[长沙县](../Page/长沙县.md "wikilink")，刘真人治之。
  - [幕阜山洞](../Page/幕阜山.md "wikilink")——周回一百八十里，名曰玄真太元天，在[鄂州唐年县](../Page/鄂州.md "wikilink")，属陈真治之。
  - [大酉山洞](../Page/大酉山.md "wikilink")——周回一百里，名曰大酉华妙天，去[辰州七十里](../Page/辰州.md "wikilink")，尹真人治之。
  - 金庭山洞——周回三百里，名曰金庭崇妙天，在越州[剡县](../Page/剡县.md "wikilink")，属赵仙伯治之。
  - 麻姑山洞——周回一百五十里，名曰丹霞天，在[抚州南城县](../Page/抚州.md "wikilink")，属王真人治之。
  - [仙都山洞](../Page/仙都.md "wikilink")——周回三百里，名曰仙都祈仙天，在[处州](../Page/处州.md "wikilink")[缙云县](../Page/缙云.md "wikilink")，属赵真人治之。
  - [青田山洞](../Page/青田.md "wikilink")——周回四十五里，名曰青田大鹤天，在[处州](../Page/处州.md "wikilink")[青田县](../Page/青田.md "wikilink")，属付真人治之。
  - [钟山洞](../Page/钟山.md "wikilink")——周回一百里，名曰朱日太生天，在[润州上元县](../Page/润州.md "wikilink")，属龚真人治之。
  - 良常山洞——周回三十里，名曰良常放命洞天，在润州[句容县](../Page/句容.md "wikilink")，属李治之。
  - [紫盖山洞](../Page/紫盖山.md "wikilink")——周回八十里，名曰紫玄洞照天，在[荆州](../Page/荆州.md "wikilink")[常阳县](../Page/常阳.md "wikilink")，属公羽真人治之。
  - 目山洞——周回一百里，名曰天盖涤玄天，在[杭州](../Page/杭州.md "wikilink")[余杭县](../Page/余杭.md "wikilink")，属姜真人治之。
  - 桃源山洞——周回七十里，名曰白马玄光天，在玄州[武陵县](../Page/武陵.md "wikilink")，属谢真人治之。
  - [金华山洞](../Page/金华.md "wikilink")——周回五十里，名曰金华洞元天，在[婺州](../Page/婺州.md "wikilink")[金华县](../Page/金华.md "wikilink")，属戴真人治之。

## 参见

  - [道教](../Page/道教.md "wikilink")
  - [道观](../Page/道观.md "wikilink")
  - [洞天](../Page/洞天.md "wikilink")
  - [洞天福地](../Page/洞天福地.md "wikilink")

## 參考文獻

[洞天福地](../Category/洞天福地.md "wikilink")

1.  《[洞天福地岳渎名山记](../Page/洞天福地岳渎名山记.md "wikilink")》，为[五代道士](../Page/五代.md "wikilink")[杜光庭编录](../Page/杜光庭.md "wikilink")。