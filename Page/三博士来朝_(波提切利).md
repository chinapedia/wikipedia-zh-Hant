**三博士来朝**是[意大利](../Page/意大利.md "wikilink")[画家](../Page/画家.md "wikilink")[桑德罗·波提切利创作于](../Page/桑德罗·波提切利.md "wikilink")1475年－1476年间的名画。画面显示《[圣经](../Page/圣经.md "wikilink")》中[东方三博士朝拜](../Page/东方三博士.md "wikilink")[耶稣](../Page/耶稣.md "wikilink")[基督的故事](../Page/基督.md "wikilink")。本作品是为了圣新玛利亚教堂绘制的圣餐台油画。现藏于意大利[佛罗伦萨的](../Page/佛罗伦萨.md "wikilink")[乌菲兹美术馆](../Page/乌菲兹美术馆.md "wikilink")。

画中画家非常巧妙的展现了自己以及其主要资助人[美第奇家族的主要人物](../Page/美第奇家族.md "wikilink")，根据[乔尔乔·瓦萨里解释](../Page/乔尔乔·瓦萨里.md "wikilink")，画面上那个接触圣婴的脚的年老贤士，正是被誉为佛罗伦萨国父的[科西莫·德·美第奇](../Page/科西莫·德·美第奇.md "wikilink")（即老科西莫）；穿白色袍子跪着的人是老科西莫的孙子[朱利亚诺·德·美第奇](../Page/朱利亚诺·德·美第奇.md "wikilink")（也是“豪华者”[洛伦佐·德·美第奇的兄弟](../Page/洛伦佐·德·美第奇.md "wikilink")）；在他后面对那个孩子表现出感激崇拜的人，是老科西莫的次子乔瓦尼·德·美第奇；在他膝下画面中央前景出的人被认为是老柯西莫的长子[皮耶罗一世·德·美第奇](../Page/皮耶罗一世·德·美第奇.md "wikilink")（即[朱利亚诺·德·美第奇与](../Page/朱利亚诺·德·美第奇.md "wikilink")[洛伦佐·德·美第奇之父](../Page/洛伦佐·德·美第奇.md "wikilink")）；穿着黑底肩上有红色条纹长袍者可能是理想化了的洛伦佐·伊·玛尼菲科。最右面看向观者的黄衣青年正是画家本人。

## 同名作品

  - [三博士来朝 (达芬奇)](../Page/三博士来朝_\(达芬奇\).md "wikilink")
  - [三博士来朝 (利皮)](../Page/三博士来朝_\(利皮\).md "wikilink")

## 外部链接

  - <https://web.archive.org/web/20071030201717/http://www.polomuseale.firenze.it/english/benvenuto.asp>

## 参见

  - [桑德罗·波提切利](../Page/桑德罗·波提切利.md "wikilink")
  - [乌菲兹美术馆](../Page/乌菲兹美术馆.md "wikilink")

[Category:桑德罗·波提切利的绘画作品](../Category/桑德罗·波提切利的绘画作品.md "wikilink")
[Category:博士来朝主题绘画作品](../Category/博士来朝主题绘画作品.md "wikilink")