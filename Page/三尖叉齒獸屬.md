[Thrinaxodon_liorhinus.jpg](https://zh.wikipedia.org/wiki/File:Thrinaxodon_liorhinus.jpg "fig:Thrinaxodon_liorhinus.jpg")
[Thrinaxodon_liorhinus_AMNH_9516_cast.jpg](https://zh.wikipedia.org/wiki/File:Thrinaxodon_liorhinus_AMNH_9516_cast.jpg "fig:Thrinaxodon_liorhinus_AMNH_9516_cast.jpg")的脊椎與肋骨化石（編號AMNH
9516）\]\]
**三尖叉齒獸屬**（[学名](../Page/学名.md "wikilink")：*Thrinaxodon*）又譯**三叉棕櫚龍**，是種[獸孔目](../Page/獸孔目.md "wikilink")[犬齒獸類動物](../Page/犬齒獸類.md "wikilink")，生存於[三疊紀早期的](../Page/三疊紀.md "wikilink")[南非與](../Page/南非.md "wikilink")[南極洲](../Page/南極洲.md "wikilink")，約2億4800萬到2億4500萬年前。三尖叉齒獸非常類似[哺乳動物](../Page/哺乳動物.md "wikilink")，是種著名的[過渡化石](../Page/過渡化石.md "wikilink")\[1\]\[2\]。

## 發現與命名

[Thrinaxodon_Lionhinus.jpg](https://zh.wikipedia.org/wiki/File:Thrinaxodon_Lionhinus.jpg "fig:Thrinaxodon_Lionhinus.jpg")
[Thrinaxodon_liorhinus_AMNH_5630.jpg](https://zh.wikipedia.org/wiki/File:Thrinaxodon_liorhinus_AMNH_5630.jpg "fig:Thrinaxodon_liorhinus_AMNH_5630.jpg")的頭顱骨（編號AMNH
5630）\]\]三尖叉齒獸的化石發現於[南非](../Page/南非.md "wikilink")、[南極洲](../Page/南極洲.md "wikilink")，地質年代為[三疊紀早期](../Page/三疊紀.md "wikilink")，約2億4800萬到2億4500萬年前。三尖叉齒獸的化石地理分布，顯示這兩個大陸在當時彼此相接。

三尖叉齒獸的屬名在[古希臘文意為](../Page/古希臘文.md "wikilink")「三叉的牙齒」，意指其頰齒形狀。在某些文獻裡，其屬名曾被誤植為「*Thrina**c**odon*」。

## 敘述

根據已知的兩個種，三尖叉齒獸的體型約從30到50公分之間\[3\]。許多科學家認為三尖叉齒獸頭骨上的洞孔，顯示牠們擁有鬍鬚，但不確定牠們是否覆蓋者皮毛\[4\]。有科學家推測說，[三疊紀早期的氣溫差異大](../Page/三疊紀.md "wikilink")，而三尖叉齒獸是[廣溫性動物](../Page/廣溫性.md "wikilink")，顯示牠們的體溫調節能力已接近[哺乳動物](../Page/哺乳動物.md "wikilink")\[5\]。三尖叉齒獸的身體骨骼仍有許多[爬行動物的特徵](../Page/爬行動物.md "wikilink")，仍是種卵生動物\[6\]。

根據2003的一份研究，某些早期犬齒獸類可能是穴居動物，例如三尖叉齒獸\[7\]。三尖叉齒獸是種[肉食性動物](../Page/肉食性.md "wikilink")，擁有銳利牙齒、身體低矮，可能以小型動物維生。根據化石顯示，三尖叉齒獸比牠們的[合弓綱祖先更像](../Page/合弓綱.md "wikilink")[哺乳類](../Page/哺乳類.md "wikilink")。牠們擁有相當大的頭骨，而且前端有洞，顯示牠們可能有鬍鬚，身體也可能覆蓋者皮毛；但是。[雙領蜥的口鼻部也具有類似的洞孔](../Page/雙領蜥.md "wikilink")，而沒有鬍鬚。下頜有大型[齒骨](../Page/齒骨.md "wikilink")，牙齒位於齒槽中。三尖叉齒獸的胸部與腹部可能有[橫膈膜分隔開](../Page/橫膈膜.md "wikilink")，橫膈膜是一種接觸到肺的肌肉，可讓三尖叉齒獸呼吸時比牠們的祖先更有效率。

## 種系發生學

三尖叉齒獸屬於[犬齒獸類](../Page/犬齒獸類.md "wikilink")[演化支](../Page/演化支.md "wikilink")，是種原始犬齒獸類，不屬於更衍化的[真犬齒獸類](../Page/真犬齒獸類.md "wikilink")。以下[演化樹來自於](../Page/演化樹.md "wikilink")2007年的研究\[8\]：

## 大眾文化

  - 在[電視節目](../Page/電視節目.md "wikilink")《[與恐龍共舞](../Page/與恐龍共舞.md "wikilink")》（*Walking
    with
    Dinosaurs*）第一集，曾出現一隻[犬齒獸類](../Page/犬齒獸類.md "wikilink")，是參考三尖叉齒獸而重建的，但是採取有鱗片版本。
  - 在[科普作家](../Page/科普作家.md "wikilink")[理察·道金斯的著作](../Page/理察·道金斯.md "wikilink")《祖先的故事》（*The
    Ancestor's Tale*）第16章，也出現三尖叉齒獸。
  - 三尖叉齒獸還出現在電視節目《[動物末日](../Page/動物末日.md "wikilink")》（*Animal
    Armageddon*）第5集。

## 參考資料

<div class="references-small">

<references>

</references>

  - 202-203.

  - 69\.

</div>

[Category:三疊紀合弓類](../Category/三疊紀合弓類.md "wikilink")
[Category:犬齒獸亞目](../Category/犬齒獸亞目.md "wikilink")
[Category:過渡化石](../Category/過渡化石.md "wikilink")

1.
2.  Kazlev, M. Alan (April 19, 2011). [Glossary entry for "Transitional
    fossil"](http://palaeos.com/evolution/glossary.html#transitional) in
    palaeos.com. Retrieved November 10, 2012.
3.
4.
5.
6.
7.
8.  Abdala, F. (2007). "Redescription of Platycraniellus Elegans
    (Therapsida, Cynodontia) from the Lower Triassic of South Africa,
    and the Cladistic Relationships of Eutheriodonts". Palaeontology 50
    (3): 591–618.