[Sanxia_River_Viewed_from_Changfu_Bridge.jpg](https://zh.wikipedia.org/wiki/File:Sanxia_River_Viewed_from_Changfu_Bridge.jpg "fig:Sanxia_River_Viewed_from_Changfu_Bridge.jpg")
[Dahan_Riverside_of_Sansia.jpg](https://zh.wikipedia.org/wiki/File:Dahan_Riverside_of_Sansia.jpg "fig:Dahan_Riverside_of_Sansia.jpg")
**三峽河**，又名**三峽溪**，位於[台灣北部](../Page/北台灣.md "wikilink")，是[大漢溪的支流](../Page/大漢溪.md "wikilink")，全長約28.5公里\[1\]，流域面積約200平方公里\[2\]，流域幾乎涵蓋[新北市](../Page/新北市.md "wikilink")[三峽區全境](../Page/三峽區.md "wikilink")，以及[土城區](../Page/土城區.md "wikilink")、[樹林區](../Page/樹林區.md "wikilink")、[桃園市](../Page/桃園市.md "wikilink")[大溪區的一小部分](../Page/大溪區.md "wikilink")。另外，三峽河是台灣少數以「河」為名的河川。

## 源流

其源流為[熊空溪](../Page/熊空溪.md "wikilink")，發源於江南腳山東南方約1.5公里之三峽、烏來邊境山區\[3\]，先向西轉西南流至[樂樂谷](../Page/樂樂谷.md "wikilink")，匯集另一支流[蚋仔溪後改稱](../Page/蚋仔溪.md "wikilink")[大豹溪](../Page/大豹溪.md "wikilink")，轉向西北流經有木、插角、湊合，轉向北流，進入平原後流至三峽街區，轉向東北流，最終於柑園注入大漢溪。

上游地區有[滿月圓森林遊樂區](../Page/滿月圓森林遊樂區.md "wikilink")、樂樂谷、[鴛鴦谷等景點](../Page/鴛鴦谷.md "wikilink")，三峽市區則有著名的[老街](../Page/三峽老街.md "wikilink")、[祖師廟](../Page/三峽祖師廟.md "wikilink")，均為大台北西南區著名的旅遊勝地。

## 三峽河主要支流

  - **三峽河**（三峽溪）：[新北市](../Page/新北市.md "wikilink")[土城區](../Page/土城區.md "wikilink")、[樹林區](../Page/樹林區.md "wikilink")、[三峽區](../Page/三峽區.md "wikilink")、[桃園市](../Page/桃園市.md "wikilink")[大溪區](../Page/大溪區.md "wikilink")
      - [橫溪](../Page/橫溪.md "wikilink")（竹坑溪）：新北市三峽區
          - [竹崙溪](../Page/竹崙溪.md "wikilink")
      - 中埔溪：新北市三峽區
          - 土地公坑溪：新北市三峽區
      - 麻園溪：新北市三峽區
      - 五寮溪：新北市三峽區、桃園市大溪區
      - **[大豹溪](../Page/大豹溪.md "wikilink")**：新北市三峽區
          - [東眼溪](../Page/東眼溪.md "wikilink")
          - [蚋仔溪](../Page/蚋仔溪.md "wikilink")
              - 中坑溪
          - **[熊空溪](../Page/熊空溪.md "wikilink")**

## 相關條目

  - [三峽祖師廟](../Page/三峽祖師廟.md "wikilink")
  - [滿月圓森林遊樂區](../Page/滿月圓森林遊樂區.md "wikilink")

## 註釋

<div class="references-small">

<references />

</div>

## 外部連結

[Category:大漢溪水系](../Category/大漢溪水系.md "wikilink")
[Category:新北市河川](../Category/新北市河川.md "wikilink")
[Category:三峽區](../Category/三峽區.md "wikilink")

1.  依據[三峽鎮志第一章《地理》](http://szt3d.ntpu.edu.tw/taipei/d/c/c_1/c_1_001.html)：總計三峽溪幹流長約二八·五公里，較日治時期二三公里增長四公里餘，蓋河道變遷，注入口北移所致
2.
3.  依據[三峽鎮志第一章《地理》](http://szt3d.ntpu.edu.tw/taipei/d/c/c_1/c_1_001.html)：三峽溪，發源自鎮境之極東南地帶；上源熊空溪發源於熊空山以西
    (應為東南) 之三峽、烏來區界分水嶺，源頭地海拔約八四○公尺