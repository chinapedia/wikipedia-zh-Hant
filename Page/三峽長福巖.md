**三峽長福巖**，古稱**三角湧長福巖**，人稱**三峽祖師廟**，素有「**東方藝術殿堂**」之稱。為[中華民國](../Page/中華民國.md "wikilink")[新北市](../Page/新北市.md "wikilink")[三峽區的信仰中心](../Page/三峽區.md "wikilink")，主奉來自[福建](../Page/福建.md "wikilink")[泉州](../Page/泉州.md "wikilink")[安溪縣的高僧](../Page/安溪縣.md "wikilink")[清水祖師](../Page/清水祖師.md "wikilink")，為一座受[道教與](../Page/道教.md "wikilink")[臺灣民間信仰影響極多的](../Page/臺灣民間信仰.md "wikilink")[佛教](../Page/佛教.md "wikilink")[祖師廟](../Page/祖師廟.md "wikilink")，本廟與[艋舺祖師廟](../Page/艋舺祖師廟.md "wikilink")、[淡水祖師廟](../Page/淡水祖師廟.md "wikilink")、[瑞芳祖師廟](../Page/瑞芳祖師廟.md "wikilink")，合稱為「[大臺北四大祖師廟](../Page/大臺北.md "wikilink")」\[1\]
\[2\]，每年[農曆](../Page/農曆.md "wikilink")[正月初六也就是祖師聖誕日](../Page/正月初六.md "wikilink")，三峽祖師廟都會舉行盛大的[神豬祭典比賽](../Page/神豬.md "wikilink")，常有媒體報導，是著名的祖師廟，現為[新北市政府公告之直轄市市定古蹟](../Page/新北市政府.md "wikilink")，亦屬於「[新北市文化資產](../Page/新北市文化資產.md "wikilink")」\[3\]。

## 祀神

[福建](../Page/福建.md "wikilink")[安溪高僧](../Page/安溪.md "wikilink")[清水祖師](../Page/清水祖師.md "wikilink")（屬神：四大將）、[太陽星君](../Page/太陽星君.md "wikilink")、[太陰星君](../Page/太陰星君.md "wikilink")、[文昌帝君](../Page/文昌帝君.md "wikilink")、[太歲星君](../Page/太歲星君.md "wikilink")。

## 歷史

1767年，來自今日的[三峽](../Page/三峽.md "wikilink")、[土城](../Page/土城.md "wikilink")、[鶯歌](../Page/鶯歌.md "wikilink")、[大溪等的](../Page/大溪.md "wikilink")[鄉民](../Page/鄉民.md "wikilink")，分為劉姓、陳姓、林姓、李姓、王姓、大雜姓、中庄雜姓等七組人馬，動工建造[祖師廟](../Page/祖師廟.md "wikilink")，供奉[安溪清水巖迎來的](../Page/安溪清水巖.md "wikilink")[清水祖師](../Page/清水祖師.md "wikilink")。1813年廟內更開設[私塾](../Page/私塾.md "wikilink")，由名士陳川負責教育當地孩童。

該廟因地震有部份毀損，1833年進行第一次重建。

1895年[馬關條約](../Page/馬關條約.md "wikilink")，[臺灣割讓](../Page/臺灣割讓.md "wikilink")，[日本接收臺灣時](../Page/日本.md "wikilink")，[乙未戰爭爆發](../Page/乙未戰爭.md "wikilink")，[日軍在三角湧失利](../Page/日軍.md "wikilink")，事後採掃蕩方式南下挺進，本廟被焚，1899年進行第二次重建。

1945年[二次大戰結束](../Page/二次大戰.md "wikilink")，[日本投降](../Page/日本投降.md "wikilink")，[國民政府接管臺灣](../Page/國民政府接管臺灣.md "wikilink")，本廟歸[公家所有](../Page/公家.md "wikilink")，由當時代理三峽街長的藝術家[李梅樹教授管理](../Page/李梅樹.md "wikilink")，李梅樹篤信清水祖師，愛好藝術，極其用心地進行了第三次的重建計劃，從募款、設計、成立委員會、工程進行，費時費力，在1947年動土，由前殿開始進行全面整修，還買了[台灣神社的](../Page/台灣神社.md "wikilink")[鳥居改作龍柱](../Page/鳥居.md "wikilink")。1963年正殿作樑；1975年鼓樓施工。

1983年，主持重建三十多年的李梅樹去世，重建委員會改選時一波三折，1995年因石欄桿的裝設引起爭議，1996年三峽祖師廟的重建告停。

## 建築

長福巖歷經三次改建，今為三進九開間的殿堂式廟宇，由前殿、大殿（也就是[中殿](../Page/中殿.md "wikilink")）以及後殿組成，加上左右兩殿將廟宇圍成「回」字封閉空間；九開間則是廟的寬幅，三進九開間的特色就是寬幅大（30米），進深小（40米）\[4\]。

前殿採三個獨立屋頂，三川五門與兩廂相連，東側龍門，西側虎門。正殿立於中央，香爐置於與正殿與前殿間之丹墀；中殿則為[重簷歇山迴廊式](../Page/歇山顶.md "wikilink")，三川有三門，兩側門不供出入，外側圍以劍門，天公爐置於入口階梯下。[廂房為兩層樓式](../Page/厢房.md "wikilink")，二樓做側殿及[鐘鼓樓](../Page/钟鼓楼.md "wikilink")，外型朝垂直方向延伸\[5\]\[6\]。

各殿屋頂皆為重簷，每根脊上有剪黏裝飾，剪花匠按[正脊](../Page/正脊.md "wikilink")、[垂脊及](../Page/垂脊.md "wikilink")[戧脊等三條脊的不同設計作裝飾處理](../Page/戗脊.md "wikilink")；內部則以[藻井做成暗厝型式](../Page/藻井.md "wikilink")，以往只有殿堂式廟宇之中殿及三川殿才用此種做法\[7\]\[8\]。

長福巖是一座以石為基、以木為頂，同時也是臺灣第一座以銅雕藝術為飾的廟宇。壁體全用石作，大部份使用深灰色的觀音山石及[花崗石](../Page/花崗岩.md "wikilink")。木材選用[檜](../Page/扁柏屬.md "wikilink")、[樟木](../Page/樟树.md "wikilink")，以防蟲蛀。

除了基座的造型較簡潔外，廟體的其他部份都有繁美的石雕，最吸引人的是大殿20根步柱的石雕，其中的「三層雙龍柱」、「花鳥柱」與「百鳥朝梅柱」可說是廟中最重要的石柱雕刻。石雕圖樣除了由李梅樹設計外，也有出自當代畫家，如[林玉山](../Page/林玉山.md "wikilink")、[歐豪年](../Page/歐豪年.md "wikilink")、王逸雲、林松等人的手筆；木雕則由木匠[黃龜理](../Page/黃龜理.md "wikilink")、李松林等人操刀，以歷史典故、民間故事為主；銅雕部分，包括五門、前殿正門[哼哈二將](../Page/哼哈二将.md "wikilink")、[四大天王及](../Page/四大天王.md "wikilink")[四大金剛](../Page/四大天王.md "wikilink")，以及部份牆堵，由[李梅樹指導藝專學生完成](../Page/李梅樹.md "wikilink")。樑為栝形，且不彩繪，在雕成後貼以金箔。石、銅柱可分為雙龍、單龍、花鳥柱及附有對聯的圓柱，柱頭雕刻特殊的倒捲樹葉形式圖案，稱為垂花。中殿四對花崗石大柱則以陰文鐫刻[于右任](../Page/于右任.md "wikilink")、[賈景德](../Page/賈景德.md "wikilink")、[閻錫山](../Page/閻錫山.md "wikilink")、[高拜石等名人或書法家所寫的對聯](../Page/高拜石.md "wikilink")\[9\]\[10\]。

## 祭典

每逢[舊曆](../Page/舊曆.md "wikilink")[正月初六祖師](../Page/正月初六.md "wikilink")[生日](../Page/生日.md "wikilink")，三峽祖師廟以[神豬祭拜](../Page/神豬.md "wikilink")**清水祖師**，以示崇敬，敬考清水祖師一生，為[漢傳佛教](../Page/漢傳佛教.md "wikilink")[禪宗得道高僧](../Page/禪宗.md "wikilink")，且其在世時未見有食肉之舉，故有人認為以祭祀素果、糕餅為宜，不適合以[肉類](../Page/肉類.md "wikilink")、[葷菜奉敬](../Page/葷菜.md "wikilink")。

三峽祖師廟廟方解釋：神豬祭典原本不是祭祀清水祖師，原是因為三峽地區的[福建](../Page/福建.md "wikilink")[安溪移民在](../Page/安溪.md "wikilink")[三峽](../Page/三峽.md "wikilink")、[鶯歌與](../Page/鶯歌.md "wikilink")[桃園](../Page/桃園.md "wikilink")[大溪開墾之際](../Page/大溪.md "wikilink")，常遭[野生動物及當地](../Page/野生動物.md "wikilink")[原住民](../Page/原住民.md "wikilink")[出草攻擊](../Page/出草.md "wikilink")，因此產生[除夕殺神豬拜](../Page/除夕.md "wikilink")[山靈](../Page/山靈.md "wikilink")（或[山神](../Page/山神.md "wikilink")）以求平安的習俗，後來因大年初六是祖師誕辰，便將二者合併祭祀，此習俗實際上與祖師本人無關。三峽祖師廟總務組長劉金達說：「祖師公喫素，不喫豬肉。」\[11\]\[12\]\[13\]\[14\]也有不少人相信，山神亦屬於清水祖師廟的[護法神](../Page/護法神.md "wikilink")。另外民間相信祖師本身[茹素](../Page/素食.md "wikilink")，但祖師部下的[五營神將與天兵天將則不一定為素食者](../Page/五營神將.md "wikilink")。\[15\]\[16\]\[17\]

## 文化

作曲家[劉學軒所譜之大型管絃樂曲](../Page/劉學軒.md "wikilink")《三峽祖師廟的石獅》曾排入[匈牙利](../Page/匈牙利.md "wikilink")[布達佩斯交響樂團](../Page/布達佩斯交響樂團.md "wikilink")（Budapest
Symphony Orchestra）2004年臺灣巡迴演出曲目\[18\]。

## 相關條目

  - [清水祖師](../Page/清水祖師.md "wikilink")
  - [李梅樹](../Page/李梅樹.md "wikilink")
  - [清水巖](../Page/清水巖.md "wikilink")
  - [安溪清水巖](../Page/安溪清水巖.md "wikilink")
  - [部岩功德院](../Page/部岩功德院.md "wikilink")
  - [艋舺祖師廟](../Page/艋舺祖師廟.md "wikilink")
  - [淡水祖師廟](../Page/淡水祖師廟.md "wikilink")
  - [瑞芳祖師廟](../Page/瑞芳祖師廟.md "wikilink")
  - [景美祖師廟](../Page/景美祖師廟.md "wikilink")
  - [萬隆祖師廟](../Page/萬隆祖師廟.md "wikilink")
  - [土城祖師廟](../Page/土城祖師廟.md "wikilink")
  - [五甲龍成宮](../Page/五甲龍成宮.md "wikilink")
  - [臺灣民間信仰](../Page/臺灣民間信仰.md "wikilink")
  - [高僧信仰](../Page/高僧信仰.md "wikilink")

## 腳注

## 外部連結

  - [財團法人台北縣三峽長福巖清水祖師公](http://www.longfuyan.org.tw/)
  - [三峽祖師廟建築藝術之美](https://web.archive.org/web/20051223075310/http://home.educities.edu.tw/kjt/sansha/)
  - 國立交通大學建築研究所：李梅樹三峽祖師廟全套建築圖
  - [在Flickr上看三峽祖師廟的相關攝影照片](http://www.flickr.com/search/?q=%E4%B8%89%E5%B3%BD%E7%A5%96%E5%B8%AB%E5%BB%9F&m=text)
  - [李梅樹紀念館](https://web.archive.org/web/20091129161847/http://www.limeishu.org/)
  - [三峽傷心廟](http://www.geocities.jp/skytenky/1/soshi.html)
  - [三峽長福巖清水祖師廟—文化部文化資產局](https://nchdb.boch.gov.tw/assets/advanceSearch/monument/20180426000001)

[S](../Category/新北市廟宇.md "wikilink")
[Category:三峽區](../Category/三峽區.md "wikilink")
[Category:清水祖師廟](../Category/清水祖師廟.md "wikilink")

1.

2.  [公民新聞 淡水清水巖祖師廟](https://www.peopo.org/news/368243)

3.  [市定古蹟─三峽長福巖清水祖師廟](http://my.ntcag.ctu.com.tw/content/%E5%B8%82%E5%AE%9A%E5%8F%A4%E8%B9%9F%E2%94%80%E4%B8%89%E5%B3%BD%E9%95%B7%E7%A6%8F%E5%B7%96%E6%B8%85%E6%B0%B4%E7%A5%96%E5%B8%AB%E5%BB%9F)

4.

5.
6.
7.

8.

9.
10.
11. [祖師廟神豬祭
    特等獎1487斤](http://www.chinatimes.com/newspapers/20180207000784-260107)

12. [清水祖師誕辰
    三峽賽神豬](http://www.chinatimes.com/realtimenews/20140205005078-260402)

13. [三峽祖師廟：神豬文化源自祭拜山靈](http://news.ltn.com.tw/news/life/paper/654337)

14. [「好大」1061公斤神豬亮相好胃口嚼檳榔年初六就要獻祭](http://tw.nextmedia.com/applenews/article/art_id/33980492/IssueID/20120124)

15. 《臺灣各地「祖師廟」神豬祭祀現況訪查報告》

16. 《臺北縣三峽長福岩沿革志》

17. 《三峽長福岩藝文探微》

18.