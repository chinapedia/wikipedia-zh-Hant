[Northwest.arp.750pix.jpg](https://zh.wikipedia.org/wiki/File:Northwest.arp.750pix.jpg "fig:Northwest.arp.750pix.jpg")\]\]
**三引擎噴射機**是有三個[噴射引擎的](../Page/噴射引擎.md "wikilink")[飛機](../Page/飛機.md "wikilink")，三引擎噴射機曾是世界上最廣泛使用的飛機種類之一，但隨著引擎的推力越來越大，許多雙引擎飛機的性能能與三引擎媲美，使三引擎噴射機逐漸沒落。

除了三引擎喷射机外，还有三引擎固定翼螺旋桨飞机（如[三引擎Ju-52（Ju
52/3m）](../Page/Ju_52.md "wikilink")）和三引擎直升机（如[西科斯基](../Page/西科斯基.md "wikilink")[CH-53E直升机](../Page/CH-53E直升机.md "wikilink")）。

## 结构

和双引擎飞机相比，三引擎噴射機的第三个引擎裝配在機尾垂直尾舵和机身之间，位于机身中线上。这样可以使动力平衡，并获得相对好的空气动力学效果。

## 历史

直到20世纪中期，喷射引擎的可靠性不高，因此双引擎飞机被禁止进行长距离飞行，直到[双发延程飞行定立前](../Page/双发延程飞行.md "wikilink")，双引擎飞机都不能执行超过两个小时的长程航班。而三引擎飞机没有此限制，且成本低于四引擎飞机，因此占据了不少长程航班的市场。

第一款三引擎噴射機是[霍克薛利](../Page/霍克薛利.md "wikilink")[三叉戟](../Page/三叉戟.md "wikilink")。1970年代大量三引擎飛機問世，包括DC-10和L-1011，這些客機都是寬體客機，能裝載250-300人，在營運方面花費比四引擎飛機少，更適合越洋航線。三引擎噴射機在1970年－1980年代的[美國是十分受歡迎的](../Page/美國.md "wikilink")，在1970年－2000年間，[美國航空曾營運超過](../Page/美國航空.md "wikilink")125架三引擎飛機，[聯合航空曾營運超過](../Page/聯合航空.md "wikilink")177架三引擎飛機。\[1\]\[2\]

三引擎客機比四引擎客機省油，看似少了一個引擎的維修需求，但實際上若要正確的拆裝及保養在機尾的引擎，很費工時，很容易造成維修不確實，許多航空公司曾引入簡單的拆裝方法，但也會破壞結構；當機尾引擎故障、脫離或爆炸時，會破壞控制系統；許多[墜機及死傷可以歸咎於此問題](../Page/墜機.md "wikilink")。

[MD-11承接](../Page/MD-11.md "wikilink")[DC-10的設計](../Page/DC-10.md "wikilink")，於1990年問世，可是MD-11的表現未如理想，加上不少同廠及其他同類產品的競爭下，諸如[波音777及](../Page/波音777.md "wikilink")[A330](../Page/A330.md "wikilink")／[A340](../Page/A340.md "wikilink")，且雙引擎客機及四引擎客機的營運成本都比三引擎的低，使得MD-11的銷售量大減，於2001年宣佈停產。[MD-11是最後一款三引擎噴射機](../Page/MD-11.md "wikilink")，它的停產標誌著三引擎噴射機的歷史使命完成。

現今所有三引擎噴射機的生產及研發已經全部終止了，由更具效益的雙引擎飛機取代。當今的飛機引擎故障率比以前的較低，更具燃油效益，基本上雙引擎能使一台大型飛機提供所需的動力，但一些巨大飛機如[波音747及](../Page/波音747.md "wikilink")[A380則選擇由四引擎來推動](../Page/A380.md "wikilink")（四引擎比三引擎耗油些但保養成本可以省更多）。隨著引擎推力增加，[雙發延程飛行的認證飛機也隨之增加](../Page/雙發延程飛行.md "wikilink")，如[波音777的雙發延程飛行便增加至](../Page/波音777.md "wikilink")207分鐘，三引擎飛機已經沒有競爭力了。

## 优缺点

### 优点

长距离飞行较双引擎飞机更安全，比四引擎更省油，在发动机可靠性不高的時代效益更是明顯。

### 缺点

第三个引擎安装在机尾，设计和维修较双引擎及四引擎飞机复杂，而且空气动力学效率低，油耗高于双引擎飞机。其可靠性優點必須依靠確實維修保養才能實現，但此時增加的維修成本會讓三引擎客機比略微耗油的四引擎客機更昂貴；如果沒有確實維修保養，其可靠性會低於雙引擎客機。

## 著名的三引擎飞机

[Dassault.falcon900.cs-dfh.arp.jpg](https://zh.wikipedia.org/wiki/File:Dassault.falcon900.cs-dfh.arp.jpg "fig:Dassault.falcon900.cs-dfh.arp.jpg")

### 三引擎运输機

  - [三引擎Ju-52（Ju 52/3m）](../Page/Ju_52.md "wikilink")

### 三引擎公務機

  - [达梭隼50](../Page/达梭隼50.md "wikilink")
  - [达梭隼900](../Page/达梭隼900.md "wikilink")
  - [雅克-40](../Page/雅克-40.md "wikilink")

### 三引擎窄體商業客機

  - [波音727](../Page/波音727.md "wikilink")
  - [圖波列夫Tu-154](../Page/Tu-154.md "wikilink")
  - [Yakovlev Yak-42](../Page/Yak-42.md "wikilink")
  - [霍克薛利三叉戟型](../Page/霍克薛利三叉戟型.md "wikilink")

### 三引擎寬體商業客機

  - [洛克希德L-1011三星機](../Page/洛歇L-1011.md "wikilink")
  - [道格拉斯DC-10](../Page/道格拉斯DC-10.md "wikilink")
  - [麦道11](../Page/麦道11.md "wikilink")

## 終止研發的三引擎飛機

  - 波音[747-300三引擎型](../Page/波音747.md "wikilink")：曾一度構想為三引擎飛機，可是因上層甲板會影響二號引擎運作而終止。\[3\]
  - Dassault超音波公務機\[4\]
  - 閃鋅礦寬體三引擎噴射機（Blended Wing Body Trijet）\[5\]

## 其他設計提議

  - 有三引擎[翼身融合飛機的提案](../Page/翼身融合.md "wikilink")，例如波音X-48
  - [Airbus有提議使用對稱式垂直尾翼的三引擎客機](../Page/Airbus.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:飛機](../Category/飛機.md "wikilink")
[Category:航空航天工程](../Category/航空航天工程.md "wikilink")
[Category:飞行器布局](../Category/飞行器布局.md "wikilink")

1.  [Airfleets](http://www.airfleets.net/home/)
2.  [ch-aviation](http://www.ch-aviation.ch/aircraft.php)
3.  [Boeing 747
    Trimotor](http://rides.webshots.com/photo/2162379980048918155wRzLzH)
4.  [Dassault超音波公務機圖片](http://www.aeroworld.net/photos/falconsst.jpg)
5.  [Blended Wide Body](http://www.twitt.org/BWBBowers.html)