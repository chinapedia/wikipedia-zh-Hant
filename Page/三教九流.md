**三教九流**（亦作**九流三教**）泛指[古代中國的](../Page/古代中國.md "wikilink")[宗教與各種](../Page/宗教.md "wikilink")[學術流派](../Page/學術.md "wikilink")，是古代中國對人的地位和[職業名稱劃分的等級](../Page/職業.md "wikilink")。在古代[白話](../Page/白話.md "wikilink")[小說中](../Page/小說.md "wikilink")，往往含有貶義。

## 概述

[三教](../Page/三教.md "wikilink")，指中國三大傳統宗教：[儒](../Page/儒教_\(宗教\).md "wikilink")、[释](../Page/佛教.md "wikilink")、[道](../Page/道教.md "wikilink")。

九流，指尊卑不同的九种身份。由尊至卑依次是：[帝王](../Page/君主.md "wikilink")、[文士](../Page/文人.md "wikilink")、[官吏](../Page/官吏.md "wikilink")、[医](../Page/医生.md "wikilink")[卜](../Page/占卜.md "wikilink")、[僧](../Page/僧伽.md "wikilink")[道](../Page/道士.md "wikilink")、[士兵](../Page/军人.md "wikilink")、[农民](../Page/農夫.md "wikilink")、[工匠](../Page/手工藝人.md "wikilink")、[商贾](../Page/商人.md "wikilink")。

## 九流十家

在《[漢書](../Page/漢書.md "wikilink")·[藝文志](../Page/藝文志.md "wikilink")》分別指：[儒家](../Page/儒家.md "wikilink")、[道家](../Page/道家.md "wikilink")、[陰陽家](../Page/陰陽家.md "wikilink")、[法家](../Page/法家.md "wikilink")、[名家](../Page/名家.md "wikilink")、[墨家](../Page/墨家.md "wikilink")、[縱橫家](../Page/縱橫家.md "wikilink")、[雜家](../Page/雜家.md "wikilink")、[農家](../Page/農家.md "wikilink")。九流十家，是九流以外加上[小說家](../Page/小说家_\(诸子百家\).md "wikilink")。

## 社會九流

社會地位上的九流可細分爲**上九流**、**中九流**和**下九流**，但卻有不一的說法。（此九流為社會地位，與九流十家的學說無直接關係）

### 上九流

[帝王](../Page/帝王.md "wikilink")、[聖賢](../Page/聖賢.md "wikilink")、[隐士](../Page/隐士.md "wikilink")、[童仙](../Page/童仙.md "wikilink")、[文人](../Page/文人.md "wikilink")、[武士](../Page/武士.md "wikilink")、[農](../Page/農.md "wikilink")、[工](../Page/工.md "wikilink")、[商](../Page/商.md "wikilink")。

### 中九流

[舉子](../Page/舉人.md "wikilink")(科舉中省試被取錄者)、[醫生](../Page/醫生.md "wikilink")、[相命](../Page/算命.md "wikilink")、[丹青](../Page/画家.md "wikilink")(畫匠、買畫人)、[書生](../Page/書生.md "wikilink")、[琴](../Page/琴.md "wikilink")[棋](../Page/棋.md "wikilink")、[僧](../Page/和尚.md "wikilink")、[道](../Page/道士.md "wikilink")、[尼](../Page/尼姑.md "wikilink")。

### 下九流

[師爺](../Page/師爺.md "wikilink")、[衙差](../Page/衙差.md "wikilink")、[升秤](../Page/升秤.md "wikilink")(秤手)、[媒婆](../Page/媒婆.md "wikilink")、[走卒](../Page/走卒.md "wikilink")、[時妖](../Page/巫婆.md "wikilink")(拐騙及巫婆)、[盜](../Page/盜.md "wikilink")、[竊](../Page/竊.md "wikilink")、[娼](../Page/娼妓.md "wikilink")。

別註1：[蒋月泉在](../Page/蒋月泉.md "wikilink")[评弹](../Page/评弹.md "wikilink")《[大生堂](../Page/大生堂.md "wikilink")》里提到末九流（其余未提及）：一流举子二流医，三星四卜五堪舆，唯有相家称第六，七书八画九琴棋。

別註2：过去社会还有其他说法和[三姑六婆](../Page/三姑六婆.md "wikilink")、[五花八门](../Page/五花八门.md "wikilink")\[1\]和十等\[2\]

## 參考文獻

[de:Drei Lehren](../Page/de:Drei_Lehren.md "wikilink")

[Category:中國傳統職業](../Category/中國傳統職業.md "wikilink")
[Category:中国思想史](../Category/中国思想史.md "wikilink")

1.  [“三教九流”与“五花八门”](http://www.ht88.com/article/article_8644_1.html)
2.  历史随笔 帝国如风－－元朝的另类历史