**三木真一郎**（），[日本的男性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")，出身於[東京都](../Page/東京都.md "wikilink")，血型為AB型，身高180cm。目前隸屬於[81
Produce](../Page/81_Produce.md "wikilink")。

## 人物介紹

  - 2005年10月，長子出生（2006年6月24日，在自己網站中發表）。
  - 2014年10月25日以嘉賓身份參與同樣以愛車出名的聲優[諏訪部順一所主持的UNLIMITED](../Page/諏訪部順一.md "wikilink")
    MOTOR WORKS，兩人試駕並大聊高級超跑。

## 經歷

  - 2010年：第4回[聲優獎Award](../Page/聲優獎.md "wikilink")「助演男優賞」受賞\[1\]\[2\]。

<!-- end list -->

  - 2014年：第8回聲優獎Award「富山敬賞」受賞\[3\]。

## 演出作品

※**粗體字**表示說明飾演的主要角色。

### 電視動畫

**1995年**

  - [愛天使傳說](../Page/愛天使傳說.md "wikilink") - 柳葉和也／大天使利摩那
  - [VR快打](../Page/VR快打.md "wikilink") - **結城晶**
  - [夢幻遊戲](../Page/夢幻遊戲.md "wikilink") - 夕城奎介
  - [小紅豆](../Page/小紅豆.md "wikilink") - 藤卷拓

**1996年**

  - [聖天空戰記](../Page/聖天空戰記.md "wikilink") - 天野進／**亞連・謝薩爾**
  - [奧運高手](../Page/奧運高手.md "wikilink") - **真田俊彥**
  - [超者雷丁](../Page/超者雷丁.md "wikilink") - **南條一夜**

**1997年**

  - [HARELUYA II BØY](../Page/BØY_\(漫畫\).md "wikilink") - **日日野晴矢**
  - [吸血姬美夕](../Page/吸血姬美夕.md "wikilink") - **拉法**
  - [神奇寶貝](../Page/神奇寶貝.md "wikilink") -
    **小次郎**、[神奇寶貝圖鑑](../Page/神奇寶貝圖鑑.md "wikilink")

**1998年**

  - [白色十字架](../Page/白色十字架.md "wikilink") - **Yohji／工藤耀爾**
  - [頭文字D First Stage](../Page/頭文字D.md "wikilink") -
    **[藤原拓海](../Page/藤原拓海.md "wikilink")**
  - [超速YOYO](../Page/超速YOYO.md "wikilink") - **中村名人**
  - [機動戰艦 The prince of
    darkness](../Page/機動戰艦_The_prince_of_darkness.md "wikilink")
    - 高杉三郎太

**1999年**

  - [快感指令](../Page/快感指令.md "wikilink") - **藤堂雪文**
  - [GTO](../Page/麻辣教師GTO.md "wikilink") - 大澤秀郎
  - [頭文字D Second Stage](../Page/頭文字D.md "wikilink") -
    **[藤原拓海](../Page/藤原拓海.md "wikilink")**

**2000年**

  - [萬有引力](../Page/萬有引力_\(漫畫\).md "wikilink") - 相澤瀧
  - [暗之末裔](../Page/暗之末裔.md "wikilink") - **都築麻斗**
  - [機巧奇傳](../Page/機巧奇傳.md "wikilink") - 阿拉希
  - [夢幻天女](../Page/夢幻天女.md "wikilink") - 御鍵（始祖）

**2001年**

  - [頭文字D Third Stage -INITIAL D THE MOVIE](../Page/頭文字D.md "wikilink")
    - **[藤原拓海](../Page/藤原拓海.md "wikilink")**

**2002年**

  - [駭客時空](../Page/.hack/SIGN.md "wikilink") - 克利姆
  - [驚爆危機](../Page/驚爆危機.md "wikilink") - **克魯茲・威巴**
  - [閃靈二人組](../Page/閃靈二人組.md "wikilink") - 天子峰猛
  - [東京地底奇兵](../Page/東京地底奇兵.md "wikilink") - 赤
  - [奇鋼仙女](../Page/奇鋼仙女.md "wikilink") - **建智鐵也**
  - [火影忍者](../Page/火影忍者.md "wikilink") - 水木
  - [盜賊王JING](../Page/盜賊王JING.md "wikilink") - Postino
  - [名偵探柯南](../Page/名偵探柯南.md "wikilink") - 萩原研二（※第304話）

**2003年**

  - [最後流亡](../Page/最後流亡.md "wikilink") - 莫藍・謝蘭度
  - [魔偵探洛基RAGNAROK](../Page/魔偵探洛基.md "wikilink") - **闇野龍介（米德加爾茲歐姆）**
  - [廢棄公主](../Page/廢棄公主.md "wikilink") - **夏儂・卡蘇魯**
  - [偵探學園Q](../Page/偵探學園Q.md "wikilink") - 七海光太郎
  - 驚爆危機？校園篇 -**克魯茲・威巴**
  - [神奇寶貝超世代](../Page/神奇寶貝超世代.md "wikilink") - **小次郎**
  - [聖鬥士星矢冥王哈帝斯冥界篇](../Page/聖鬥士星矢冥王哈帝斯冥界篇.md "wikilink") - **天雄星神鷲
    艾亞哥斯**
  - [R.O.D -THE TV-](../Page/R.O.D_-THE_TV-.md "wikilink") - 阿李

**2004年**

  - [遙遠時空 八葉抄](../Page/遙遠時空.md "wikilink") - **源賴久**
  - [學園愛麗絲](../Page/學園愛麗絲.md "wikilink") - 派爾索那
  - [今日大魔王\!](../Page/魔之系列#電視動畫.md "wikilink") - 真王
  - [天上天下](../Page/天上天下.md "wikilink") - **卜比牧原**
  - [舞-HiME](../Page/舞-HiME.md "wikilink") - 石上亙
  - [BLEACH](../Page/BLEACH.md "wikilink") - **浦原喜助**
  - [御伽草子](../Page/御伽草子.md "wikilink") - 源 賴光／**萬歲樂**
  - [頭文字D Fourth Stage](../Page/頭文字D.md "wikilink") -
    **[藤原拓海](../Page/藤原拓海.md "wikilink")**
  - [七武士](../Page/七武士_\(動畫\).md "wikilink") - 久藏

**2005年**

  - [植木的法則](../Page/植木的法則.md "wikilink") - 馬修
  - [機動新撰組 萌之劍](../Page/機動新撰組_萌之劍.md "wikilink") -
    **[平賀源內](../Page/平賀源內.md "wikilink")**
  - [驚爆危機\!The Second Raid](../Page/驚爆危機.md "wikilink") - **克魯茲・威巴**
  - [IGPX](../Page/IGPX.md "wikilink") - アレックス・カニンガム
  - [黑貓](../Page/黑貓.md "wikilink") - **庫裡德・迪思肯特**
  - [天堂之吻](../Page/天堂之吻.md "wikilink") - 如月星次
  - [怪醫黑傑克](../Page/怪醫黑傑克.md "wikilink") - 鯨岡的兒子、大和屋暁
  - [喜歡就是喜歡](../Page/喜歡就是喜歡.md "wikilink") - 水都真一朗（水都先生）
  - [ドラゴンブースター](../Page/ドラゴンブースター.md "wikilink") - '''モードリッド・ペイン '''
  - [Xenosaga the
    Animation](../Page/Xenosaga_the_Animation.md "wikilink") - **ヘルマン**

**2006年**

  - [地獄少女](../Page/地獄少女.md "wikilink") - エスパー☆ワタナベ (第20話)
  - [Tsubasa翼](../Page/Tsubasa翼.md "wikilink") - 桃矢
  - [Fate/stay night](../Page/Fate/stay_night.md "wikilink") -
    佐佐木小次郎（Assassin）
  - [學園天堂](../Page/學園天堂.md "wikilink") - **成瀨由紀彥**
  - [牙 -KIBA](../Page/牙_-KIBA.md "wikilink") - ロベス
  - [銀魂](../Page/銀魂_\(動畫\).md "wikilink") - 坂本辰馬、渡辺篤之助
  - [.hack//Roots](../Page/駭客時空.md "wikilink") - クーン
  - [變身公主](../Page/變身公主.md "wikilink") - 坂本英季
  - [神奇寶貝鑽石&珍珠](../Page/神奇寶貝鑽石&珍珠.md "wikilink") -
    **小次郎**、小智的[草苗龜](../Page/草苗龜.md "wikilink")
  - [非戰特攻隊：帝國陸軍情報部第3課](../Page/南瓜剪刀.md "wikilink") - 利奧尼爾・泰拉
  - [超級機器人大戰OG](../Page/超級機器人大戰OG.md "wikilink")（動畫）- **伊達隆聖**
  - [天保異聞 妖奇士](../Page/天保異聞_妖奇士.md "wikilink") - **江戸元閥**
  - [TOKYO TRIBE2](../Page/TOKYO_TRIBE2.md "wikilink") - ンコイ

**2007年**

  - [REIDEEN](../Page/REIDEEN.md "wikilink") - **前田崎太郎**
  - [魔女獵人](../Page/魔女獵人.md "wikilink") - 海因茨・修奈達
  - [金田一少年之事件簿 (動畫)](../Page/金田一少年之事件簿_\(動畫\).md "wikilink") - 城龍也
  - [鋼鐵三國志](../Page/鋼鐵三國志.md "wikilink") - **周瑜公瑾**
  - [黑之契約者](../Page/黑之契約者.md "wikilink") - 艾瑞克・西島
  - [藍龍](../Page/藍龍.md "wikilink") - ルメール（魯梅魯）
  - [惡魔獵人](../Page/惡魔獵人.md "wikilink") - モデウス
  - [灣岸Midnight](../Page/灣岸Midnight.md "wikilink") - **島達也**
  - [意外 (漫畫)](../Page/意外_\(漫畫\).md "wikilink") - 亞季的哥哥
  - [機動戰士GUNDAM 00](../Page/機動戰士GUNDAM_00.md "wikilink") -
    **洛克昂・史特拉托斯**（**尼爾‧狄蘭迪**）
  - [驅魔少年](../Page/驅魔少年.md "wikilink") - 白燦
  - [遙久時空３](../Page/遙久時空.md "wikilink") - **有川將臣**
  - [名偵探柯南](../Page/名偵探柯南.md "wikilink") - 時津潤哉
  - [擁抱春天的羅曼史](../Page/擁抱春天的羅曼史.md "wikilink") - **香藤洋二**

**2008年**

  - [機動戰士GUNDAM 00第二季](../Page/機動戰士GUNDAM_00.md "wikilink") -
    **洛克昂・史特拉托斯**（**萊爾‧狄蘭迪**）
  - [今日大魔王\!第三季](../Page/魔之系列#電視動畫.md "wikilink") - 真王
  - [水晶之焰](../Page/水晶之焰.md "wikilink") - **修**
  - [出包王女（動畫）](../Page/出包王女.md "wikilink") - ピカリー
  - [西洋骨董洋菓子店](../Page/西洋骨董洋菓子店_\(動畫\).md "wikilink") - **小野裕介**
  - [破天荒遊戲](../Page/破天荒遊戲.md "wikilink") - **巴羅庫希特**
  - [無限之住人](../Page/無限之住人.md "wikilink") - 屍良
  - [幻影少年](../Page/幻影少年.md "wikilink") - 石野鏡
  - [魍魎之匣](../Page/魍魎之匣.md "wikilink") - 增岡則之
  - [黒塚 KUROZUKA](../Page/黒塚_KUROZUKA.md "wikilink") - 嵐山

**2009年**

  - [RIDEBACK](../Page/RIDEBACK.md "wikilink") - 羅曼諾夫‧卡連巴克
  - [鋼之鍊金術師 FULLMETAL ALCHEMIST](../Page/鋼之鍊金術師.md "wikilink") -
    **羅伊・馬斯坦古**
  - [奇蹟少女KOBATO.](../Page/奇蹟少女KOBATO..md "wikilink") - 沖浦和斗
  - [空中鞦韆](../Page/空中鞦韆.md "wikilink") - 星山純一
  - [天降之物](../Page/天降之物.md "wikilink") - 天上的主人/米諾斯
  - [名偵探柯南](../Page/名偵探柯南.md "wikilink") - 相園修 \#549、550

**2010年**

  - [遙遠時空3 永無止境的命運](../Page/遙遠時空3.md "wikilink") - **有川將臣**
  - [無法掙脫的背叛](../Page/無法掙脫的背叛.md "wikilink") - 祇王橘
  - [薄櫻鬼\~新選組奇譚\~](../Page/薄櫻鬼.md "wikilink") - **土方歳三**
  - [薄櫻鬼 碧血錄](../Page/薄櫻鬼.md "wikilink") - **土方歳三**
  - [王牌投手 振臂高揮 ～夏季大會篇](../Page/王牌投手_振臂高揮.md "wikilink") - 三橋玲一
  - [神奇寶貝超級願望](../Page/神奇寶貝超級願望.md "wikilink") - **小次郎**
  - [STAR DRIVER 閃亮的塔科特](../Page/STAR_DRIVER_閃亮的塔科特.md "wikilink") -
    **形代龍介／委員長**
  - [天降之物Forte](../Page/天降之物.md "wikilink") - 天上的主人/米諾斯
  - [超級機械人大戰OG -The
    Inspector-](../Page/超級機械人大戰OG_-The_Inspector-.md "wikilink")
    - **伊達隆聖**
  - [天使禁獵區](../Page/天使禁獵區.md "wikilink") - 肯達

**2011年**

  - [銀魂'](../Page/銀魂_\(動畫\).md "wikilink") - 坂本辰馬
  - [戰國鬼才傳](../Page/戰國鬼才傳.md "wikilink") -
    [高山右近](../Page/高山右近.md "wikilink")
  - [NO.6](../Page/NO.6.md "wikilink") - 揚眠
  - [魔法少女小圓](../Page/魔法少女小圓.md "wikilink") - 公關B（第8集）
  - [UN-GO](../Page/UN-GO.md "wikilink") - **海勝麟六**
  - [火影忍者疾風傳](../Page/火影忍者疾風傳.md "wikilink") - 水木
  - [美食獵人TORIKO](../Page/美食獵人TORIKO.md "wikilink") - **史塔俊**

**2012年**

  - [足球騎士](../Page/足球騎士.md "wikilink") - **岩城鐵平**
  - [偽物語](../Page/偽物語.md "wikilink") - **貝木泥舟**
  - [BRAVE10](../Page/BRAVE10.md "wikilink") - 直江兼續
  - [夏色奇蹟](../Page/夏色奇蹟.md "wikilink") - 紗季的父親
  - [黑子的籃球](../Page/黑子的籃球.md "wikilink") - 相田景虎、旁白
  - [少年同盟2](../Page/少年同盟.md "wikilink") - 店長
  - [CØDE:BREAKER](../Page/CØDE:BREAKER.md "wikilink") - **人見**
  - [紙箱戰機W](../Page/紙箱戰機.md "wikilink") - ビリー・スタリオン
  - [薄櫻鬼 黎明錄](../Page/薄櫻鬼.md "wikilink") - **土方歳三**
  - [頭文字D Fifth Stage](../Page/頭文字D.md "wikilink") -
    **[藤原拓海](../Page/藤原拓海.md "wikilink")**
  - [ROBOTICS;NOTES](../Page/ROBOTICS;NOTES.md "wikilink") - 澤田敏行

**2013年**

  - [花牌情緣2](../Page/花牌情緣.md "wikilink")（安東尼·里夫）
  - [八犬傳－東方八犬異聞－](../Page/八犬傳－東方八犬異聞－.md "wikilink")（**犬山道節**）
  - [八犬傳－東方八犬異聞－第二期](../Page/八犬傳－東方八犬異聞－.md "wikilink")（**犬山道節**）
  - [魔奇少年](../Page/魔奇少年.md "wikilink")（伊蘇南）
  - [Hunter × Hunter](../Page/Hunter_×_Hunter.md "wikilink")（諾布）
  - [萌萌侵略者 OUTBREAK
    COMPANY](../Page/萌萌侵略者_OUTBREAK_COMPANY.md "wikilink")（迦流士·恩·克德巴爾）
  - [KILL la KILL](../Page/KILL_la_KILL.md "wikilink")（**美木杉愛九郎**）
  - [黑子的籃球 第2期](../Page/黑子的籃球.md "wikilink")（相田景虎、旁白）
  - [聲優戰隊VoiceStorm7](../Page/聲優戰隊VoiceStorm7.md "wikilink")（三廻部洋）
  - [銀狐](../Page/銀狐_\(漫畫\).md "wikilink")（**銀太郎**）
  - [物語系列 第二季](../Page/物語系列_第二季.md "wikilink")（**貝木泥舟**）
  - [神奇寶貝XY](../Page/神奇寶貝XY.md "wikilink")（**小次郎**）

**2014年**

  - [排球少年！！](../Page/排球少年！！.md "wikilink")（追分拓朗）
  - [黑色子彈](../Page/黑色子彈.md "wikilink")（**薙澤彰磨**）
  - [頭文字D Final
    Stage](../Page/頭文字D.md "wikilink")（**[藤原拓海](../Page/藤原拓海.md "wikilink")**）
  - [花物語](../Page/花物語.md "wikilink")（貝木泥舟）
  - [笑傲曇天](../Page/笑傲曇天.md "wikilink")（嘉神直人）
  - [Fate/stay
    night（ufotable版）](../Page/Fate/stay_night.md "wikilink")（Assassin）
  - [TRINITY
    SEVEN](../Page/TRINITY_SEVEN_魔道書7使者.md "wikilink")（王立書冊學園校長）
  - [我，要成為雙馬尾](../Page/我，要成為雙馬尾.md "wikilink")（罪惡魔蝶）

**2015年**

  - [银魂°](../Page/銀魂_\(動畫\).md "wikilink")（坂本辰馬）
  - [絕命制裁X](../Page/絕命制裁X.md "wikilink")（犬鳴慎一郎）
  - [潮與虎](../Page/潮與虎.md "wikilink")（雷信）
  - [Classroom☆Crisis](../Page/Classroom☆Crisis.md "wikilink")（誘拐犯首領、伊布拉·阿靈頓）
  - [Concrete
    Revolutio～超人幻想～](../Page/Concrete_Revolutio～超人幻想～.md "wikilink")（人吉孫竹）
  - [一拳超人](../Page/一拳超人.md "wikilink")（史內克）
  - [對魔導學園35試驗小隊](../Page/對魔導學園35試驗小隊.md "wikilink")（**鳳颯月**）

**2016年**

  - [ONE PIECE](../Page/ONE_PIECE.md "wikilink")（佩特羅）
  - [龙珠超](../Page/龙珠超.md "wikilink")（撒马斯、合体撒马斯、孙悟空 \<在撒马斯的身体\>）
  - [排球少年！！第二季](../Page/排球少年！！.md "wikilink")（追分拓朗）
  - [紅殼的潘朵拉](../Page/紅殼的潘朵拉.md "wikilink")（ジヨー・ライト伍長）
  - [潮與虎](../Page/潮與虎.md "wikilink") 第2期（雷信）
  - [薄櫻鬼 〜御伽草子〜](../Page/薄櫻鬼.md "wikilink")（**土方歲三**）
  - [Concrete
    Revolutio～超人幻想～](../Page/Concrete_Revolutio～超人幻想～.md "wikilink")
    THE LAST SONG（人吉孫竹）
  - [ReLIFE 重返17歲](../Page/ReLIFE.md "wikilink")（黑心企業上司②）
  - [時間旅行少女～真理、和花與8名科學家～](../Page/時間旅行少女～真理、和花與8名科學家～.md "wikilink")（班傑明·富蘭克林）
  - [路人超能100](../Page/靈能百分百.md "wikilink")（誇山）
  - [七大罪 聖戰的預兆](../Page/七大罪_\(漫畫\).md "wikilink")（史雷達）
  - [精靈寶可夢 太陽&月亮](../Page/精靈寶可夢_太陽&月亮.md "wikilink")（**小次郎**）

**2017年**

  - [3月的獅子](../Page/3月的獅子.md "wikilink")（島田開）
  - 精靈寶可夢 太陽&月亮（哈拉的鐵掌力士\[4\]、格拉吉歐的鬃岩狼人\[5\]）
  - [超·少年偵探團NEO](../Page/超·少年偵探團NEO.md "wikilink")（旁白）
  - [清戀](../Page/清戀.md "wikilink")（湯木尚武）
  - [幼女戰記](../Page/幼女戰記.md "wikilink")（**埃里希·馮·雷魯根**）
  - [鬼平](../Page/鬼平犯科帳.md "wikilink")（近藤勘四郎）
  - [黑色推銷員NEW](../Page/黑色推銷員.md "wikilink")（道原驅）
  - [戰姬絕唱SYMPHOGEAR AXZ](../Page/戰姬絕唱SYMPHOGEAR.md "wikilink")（亞當）
  - [潔癖男子青山！](../Page/潔癖男子青山！.md "wikilink")（伊吹誠吾）
  - [THE REFLECTION](../Page/THE_REFLECTION.md "wikilink")（**埃克森**）
  - [側車搭檔](../Page/側車搭檔.md "wikilink")（**棚橋教練**）

**2018年**

  - [BASILISK～櫻花忍法帖～](../Page/BASILISK～櫻花忍法帖～.md "wikilink")（根來轉寢）
  - [HUG！光之美少女](../Page/HUG！光之美少女.md "wikilink")（利斯托爾）
  - [銀河英雄傳說 Die Neue These 邂逅](../Page/銀河英雄傳說.md "wikilink")（華爾特·馮·先寇布）
  - 驚爆危機！Invisible Victory（**克魯茲·威巴**）
  - [深夜！天才傻鵬](../Page/深夜！天才傻鵬.md "wikilink")（YOSHIKI）
  - [DOUBLE
    DECKER！道格&基里爾](../Page/DOUBLE_DECKER！道格&基里爾.md "wikilink")（英俊的喬）
  - [學園BASARA](../Page/戰國BASARA系列#學園BASARA.md "wikilink")（後藤又兵衛）
  - [BAKUMATSU](../Page/戀愛幕末男友～在時空的彼方盛開之戀～.md "wikilink")（**坂本龍馬**\[6\]）
  - [書店裡的骷髏店員本田](../Page/書店裡的骷髏店員本田.md "wikilink")（天蓋）

**2019年**

  - [我的英雄學院](../Page/我的英雄學院.md "wikilink")（夜目爵士）

### 網絡動畫

**2018年**

  - [超級七龍珠英雄](../Page/七龍珠英雄.md "wikilink")（合体札玛斯）

### OVA

  - [Level C](../Page/Level_C.md "wikilink")（本條一臣）
  - [擁抱春天的羅曼史](../Page/擁抱春天的羅曼史.md "wikilink")（香藤洋二）
  - [冬之蟬](../Page/冬之蟬.md "wikilink")（草加十馬）
  - [パパと KISS IN THE
    DARK](../Page/パパと_KISS_IN_THE_DARK.md "wikilink")（宗方鏡介）
  - [聖鬥士星矢 THE LOST CANVAS
    冥王神話](../Page/THE_LOST_CANVAS_冥王神話_\(動畫\).md "wikilink")（**白羊座史昂**）
  - [Carnival Phantasm](../Page/Carnival_Phantasm.md "wikilink")（佐佐木小次郎）
  - [暗殺教室](../Page/暗殺教室.md "wikilink")（赤红之眼）
  - [銀魂第](../Page/銀魂_\(動畫\).md "wikilink")58卷同捆OAD（坂本辰馬）

**2010年**

  - 腐女子的品格（セム）

**2015年**

  - [絕命制裁X](../Page/絕命制裁X.md "wikilink")（犬鳴慎一郎）

**2018年**

  - [ReLIFE"完結篇"](../Page/ReLIFE.md "wikilink")（上司）

### 劇場版動畫

  - [名偵探柯南劇場版](../Page/名偵探柯南.md "wikilink") 銀翼的奇術師 - 新莊 功
  - [精灵宝可梦剧场版](../Page/精灵宝可梦剧场版.md "wikilink") - **小次郎**
  - [頭文字D Third Stage](../Page/頭文字D.md "wikilink") - **藤原拓海**
  - [銀魂劇場版 新譯紅櫻篇](../Page/銀魂劇場版_新譯紅櫻篇.md "wikilink") - 坂本辰馬（客串）
  - [劇場版　遙遠時空　舞一夜](../Page/遙遠時空.md "wikilink") - **源 賴久**
  - [劇場版 Fate／stay night UNLIMITED BLADE
    WORKS](../Page/Fate/stay_night.md "wikilink") - 佐佐木小次郎 (從者-刺客)
  - [劇場版 機動戰士鋼彈00 -A wakening of the
    Trailblazer-](../Page/劇場版_機動戰士鋼彈00_-A_wakening_of_the_Trailblazer-.md "wikilink")
    - 洛克昂·史特拉托斯
  - [鋼之鍊金術師 嘆息之丘的聖星](../Page/鋼之鍊金術師_嘆息之丘的聖星.md "wikilink") -
    [羅伊·馬斯坦古](../Page/羅伊·馬斯坦古.md "wikilink")
  - [永遠之久遠](../Page/永遠之久遠.md "wikilink") - 上代源治
  - [天降之物 計時的悲傷女神](../Page/天降之物.md "wikilink") - 米諾斯
  - [薄櫻鬼劇場版-第一章 ～京都亂舞～](../Page/薄櫻鬼.md "wikilink")-**土方歲三**
  - [銀魂完結篇 永遠的萬事屋](../Page/銀魂劇場版_完結篇_永遠的萬事屋.md "wikilink") - 坂本辰馬（客串）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [樂園追放 -Expelled from
    Paradise-](../Page/樂園追放_-Expelled_from_Paradise-.md "wikilink")
    - **扎里克·卡吉瓦拉**
  - [天降之物 Final 我永遠的鳥籠](../Page/天降之物.md "wikilink") - 米諾斯

<!-- end list -->

  - 2015年

<!-- end list -->

  - [屍者的帝國](../Page/屍者的帝國.md "wikilink") - 阿列克塞·卡拉馬佐夫

<!-- end list -->

  - 2016年

<!-- end list -->

  - [KING OF PRISM by
    PrettyRhythm](../Page/KING_OF_PRISM_by_PrettyRhythm.md "wikilink") -
    **法月仁**
  - [頭文字D新劇場版：夢現](../Page/頭文字D新劇場版：夢現.md "wikilink") - 駕駛GT86的司機

<!-- end list -->

  - 2017年

<!-- end list -->

  - [劇場版 TRINITY SEVEN
    -悠久圖書館與鍊金術少女-](../Page/TRINITY_SEVEN_魔道書7使者.md "wikilink")
    - 王立書冊學園校長

<!-- end list -->

  - 2018年

<!-- end list -->

  - [K SEVEN STORIES](../Page/K_SEVEN_STORIES.md "wikilink") - 伏見仁希

<!-- end list -->

  - 2019年

<!-- end list -->

  - [劇場版 TRINITY SEVEN
    -天空圖書館與緋紅的魔王-](../Page/TRINITY_SEVEN_魔道書7使者.md "wikilink")
    - 王立書冊學園校長

### 廣播節目

  - 悪魔城ドラキュラ ラジオクロニクル（KONAMI STATION：2008年8月27日 - 2008年11月21日）
  - アニキンFREEDOM （東海ラジオ放送：1994年4月 - 1998年10月）
  - エンペラー黒田のゲーム業界裏の裏〜ゲームクリエイター・オフレコミーティング〜
  - 眞一郎と綾子のミッドナイトキーパーズ（[文化放送](../Page/文化放送.md "wikilink")）
  - ファイナルファンタジータクティクス アドバンス・ラジオエディション
  - フリーダム探偵局 大沼探偵部
  - ネオロマンス Paradise（アール・エフ・ラジオ日本、ラジオ関西：2001年10月 - 2002年4月）
  - ネオロマンス Paradise Cure\!（ラジオ大阪：2003年7月3日 -
    2004年3月25日|[文化放送](../Page/文化放送.md "wikilink")：2003年7月2日
    - 2004年3月24日）
  - [BLEACH](../Page/BLEACH.md "wikilink") “B” STATION（2006年5月度节目主持）
  - オー\!NARUTOニッポン（2005年9月度节目主持）
  - 韓風ラジオ
  - 破天荒Radio（アニメイトTV： 2007年12月6日 - 2008年3月27日）
  - 薄桜鬼WEBラジオ 新選組通信（アニメイトTV： 2009年7月9日–2010年12月27日）
  - 薄桜鬼集会 放送録（不定期节目主持）（アニメイトTV：2011年5月12日– ）
  - Pokemon Radio Show\! ロケット団ひみつ帝国（エフエムインターウェーブ|InterFM、2012年7月1日 - ）
  - [頭文字D](../Page/頭文字D.md "wikilink") 網絡廣播節目|頭文字D Radio
    Stage（[音泉](../Page/音泉.md "wikilink")、HiBiKi Radio
    Station：2012年11月10日 - 2013年5年26日）
  - [银狐](../Page/銀狐_\(漫畫\).md "wikilink") 網絡廣播節目|TVアニメぎんぎつね
    銀とハルの稲荷通信（アニメイトTV：2013年10月4日 - ）
  - [Kill La Kill](../Page/Kill_La_Kill.md "wikilink")
    網絡廣播節目|キルラキルラジオ（[音泉](../Page/音泉.md "wikilink")：2013年10月15日
    - ）

<!-- end list -->

  - Radio Drama

<!-- end list -->

  - 悪魔城ドラキュラX 月下の夜想曲|悪魔城ドラキュラX 追憶の夜想曲（**リヒター・ベルモンド**）
  - VOMIC [NARUTO -ナルト-](../Page/NARUTO_-ナルト-.md "wikilink")（ミズキ）

### 特攝

  - [-{zh:假面騎士;zh-hans:假面骑士;zh-hk:幪面超人;zh-tw:假面騎士;}-電王](../Page/幪面超人電王.md "wikilink")
    - 異魔神 齊格／電王 Wing Form
  - [假面骑士铠武](../Page/假面骑士铠武.md "wikilink") - 创世纪驱动器（声效）

### 遊戲

  - [刺客教條：大革命](../Page/刺客教條：大革命.md "wikilink")（**亞諾·多里安**）
  - [仁王](../Page/仁王_\(遊戲\).md "wikilink")（**[真田幸村](../Page/真田幸村.md "wikilink")**）
  - [Virtua Fighter](../Page/Virtua_Fighter.md "wikilink")（**結城晶**）
  - [BLEACH](../Page/BLEACH.md "wikilink")（**浦原喜助**）
  - [頭文字D 攜帶版](../Page/頭文字D_攜帶版.md "wikilink")（**藤原拓海**）
  - [超級機器人大戰系列](../Page/超級機器人大戰系列.md "wikilink") - 伊達隆聖
  - [英雄傳說 軌跡系列](../Page/英雄傳說_軌跡系列.md "wikilink")（**蘭迪·奧蘭多**）
      - \[PSP\][英雄傳說 零之軌跡](../Page/英雄傳說_零之軌跡.md "wikilink")
      - \[PSP\][英雄傳說 碧之軌跡](../Page/英雄傳說_碧之軌跡.md "wikilink")
      - \[PS VITA\] [英雄傳說 零之軌跡
        Evolution](../Page/英雄傳說_零之軌跡.md "wikilink")
      - \[PS VITA\] [英雄傳說 碧之軌跡
        Evolution](../Page/英雄傳說_碧之軌跡.md "wikilink")
  - [七武士](../Page/七武士_\(動畫\).md "wikilink") - 久藏
  - [召喚夜響曲4](../Page/召喚夜響曲4.md "wikilink") - 謝隆
  - [心跳回憶 Girl's Side](../Page/心跳回憶.md "wikilink") - （**三原色**）
  - [遙久時空](../Page/遙久時空.md "wikilink") - 源 賴久（第一代），源
    賴忠（第二代），有川將臣（第三代），柊（第四代）
  - [Fate/unlimited codes](../Page/Fate/unlimited_codes.md "wikilink") -
    Assassin（佐佐木小次郎）
  - [Fate/tiger colosseum](../Page/Fate/tiger_colosseum.md "wikilink") -
    Assassin（佐佐木小次郎）
  - [Fate/Grand Order](../Page/Fate/Grand_Order.md "wikilink") -
    Assassin（佐佐木小次郎）
  - [Fate/Grand Order](../Page/Fate/Grand_Order.md "wikilink") -
    caster（[帕拉塞爾蘇斯](../Page/帕拉塞爾蘇斯.md "wikilink")·馮·霍恩海姆)
  - [薄櫻鬼](../Page/薄櫻鬼.md "wikilink")（**[土方歲三](../Page/土方歲三.md "wikilink")**）
  - [三国恋戦記～オトメの兵法！](../Page/三国恋戦記～オトメの兵法！.md "wikilink")（玄德）
  - [.hack//G.U.](../Page/.hack/G.U..md "wikilink")（**庫恩**）
  - [El Shaddai](../Page/El_Shaddai.md "wikilink")（以諾）
  - [花帰葬](../Page/花帰葬.md "wikilink")（玄冬）
  - [鬼武者Soul(Web)](../Page/鬼武者Soul\(Web\).md "wikilink")
  - [絕對絕望少女 槍彈辯駁Another
    Episode](../Page/絕對絕望少女_槍彈辯駁Another_Episode.md "wikilink")（塔和灰慈）
  - [戰國BASARA4](../Page/戰國BASARA4.md "wikilink")(後藤又兵衛)
  - [戰國BASARA4 皇](../Page/戰國BASARA4_皇.md "wikilink")(後藤又兵衛)
  - [魔女王](../Page/魔女王.md "wikilink")(**瑪利安**)
  - [文豪與鍊金術師](../Page/文豪與鍊金術師.md "wikilink")（菊池寬）
  - [在茜色的世界與君詠唱](../Page/在茜色的世界與君詠唱.md "wikilink")（坂本龍馬）
  - [7'scarlet](../Page/7'scarlet.md "wikilink")(叢雲 結月)
  - [七龙珠系列](../Page/七龙珠.md "wikilink")（撒马斯、合体撒马斯）
  - [偶像夢幻祭](../Page/偶像夢幻祭.md "wikilink")（冰鷹誠矢）

### 吹替

  - [吳彥祖](../Page/吳彥祖.md "wikilink")
      - [新警察故事](../Page/新警察故事.md "wikilink")
      - [寶貝計劃](../Page/寶貝計劃.md "wikilink")
      - [精武家庭](../Page/精武家庭.md "wikilink")
      - [大佬愛美麗](../Page/大佬愛美麗.md "wikilink")
      - [天堂口](../Page/天堂口.md "wikilink")
      - [新宿事件](../Page/新宿事件.md "wikilink")
      - [太極](../Page/太極.md "wikilink")
  - [黃曉明](../Page/黃曉明.md "wikilink")
      - [神鵰俠侶](../Page/神鵰俠侶.md "wikilink")
      - [葉問2](../Page/葉問2.md "wikilink")
  - [余文樂](../Page/余文樂.md "wikilink")
      - [無間道](../Page/無間道.md "wikilink")（電視放送版）
      - [無間道II](../Page/無間道II.md "wikilink")（電視放送版）
  - [麥克·法斯賓達](../Page/麥克·法斯賓達.md "wikilink")
      - [X戰警：第一戰](../Page/X戰警：第一戰.md "wikilink")
      - [X戰警：未來昔日](../Page/X戰警：未來昔日.md "wikilink")
      - [簡愛](../Page/簡愛.md "wikilink")
  - [張家輝](../Page/張家輝.md "wikilink")
      - [決戰紫禁之巔](../Page/決戰紫禁之巔.md "wikilink")（龍龍九）
      - [黑社會](../Page/黑社會.md "wikilink")（飛機）

### BLCD

  - パパとKISS IN THE DARK（宗方鏡介）

<!-- end list -->

  -
    　パパとLoving All Night
    　パパとDeep in the Forest
    　パパとUnder the Moonlight
    　パパと愛だけ足りない
    　パパと番外 Love Trial

<!-- end list -->

  - 二重螺旋シリーズ（篠宮雅紀）

<!-- end list -->

  -
    　二重螺旋
    　二重螺旋2 -愛情鎖縛-
    　二重螺旋3 -攣哀感情-
    　情愛のベクトル（全員サービスドラマCD）

<!-- end list -->

  - 110番は恋の始まり（劍持薫）

<!-- end list -->

  -
    　110番は甘い鼓動

<!-- end list -->

  - 花絳樓系列

<!-- end list -->

  -
    　媚笑の閨に侍る夜（上杉）

<!-- end list -->

  - 春を抱いていた系列（香藤洋二）
  - よくある話（**袴田俊樹**）

## 腳註

## 外部連結

  - [官方網站－三木派](http://www.miki-ha.com/)

[Category:日本男性配音員](../Category/日本男性配音員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:聲優獎助演男優獎得主](../Category/聲優獎助演男優獎得主.md "wikilink")

1.
2.
3.
4.
5.
6.