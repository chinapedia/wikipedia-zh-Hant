**三枝夕夏IN
db**為[日本](../Page/日本.md "wikilink")[樂團](../Page/樂團.md "wikilink")。隸屬於日本[Being
Media Factory旗下的](../Page/Being_Media_Factory.md "wikilink")[GIZA
Studio](../Page/GIZA_Studio.md "wikilink")。[台灣由](../Page/台灣.md "wikilink")[日商美音亞洲股份有限公司負責代理](../Page/日商美音亞洲股份有限公司.md "wikilink")、發行。

## 簡介

2002年以[音樂製作專案db為背景](../Page/音樂.md "wikilink")，開始了個人的活動，6月終於以「Whenever I
think
you」[單曲出道](../Page/單曲.md "wikilink")。2003年10月的第六張單曲「」一度進入日本[公信榜第](../Page/公信榜.md "wikilink")8名而收到矚目。亦由此[單曲開始](../Page/單曲.md "wikilink")，開始有[岩井勇一郎](../Page/岩井勇一郎.md "wikilink")（吉他）、[大藪拓](../Page/大藪拓.md "wikilink")（貝斯）、[車谷啟介](../Page/車谷啟介.md "wikilink")（鼓手）的加入，開始了以4人所組成的[樂團活動](../Page/樂團.md "wikilink")。同時，三枝夕夏也提供歌詞給其他的[藝人](../Page/藝人.md "wikilink")，作為[作曲家的她亦獲得相當高的評價](../Page/作曲家.md "wikilink")。

2006年6月6日，三枝夕夏IN db發行精選集，作為他們5週年紀念。

2009年10月三枝夕夏IN db在官網宣布於2010年1月的巡迴演出後解散。

2010年1月 完成東京、名古屋及大阪的演唱會後三枝夕夏IN db
正式解散，三枝夕夏以歌手名義作個人發展，不過事實是三枝夕夏正式從藝能界引退，db
成員則繼續於藝能界活動。

## 成員

  - [三枝夕夏](../Page/三枝夕夏.md "wikilink")- 主唱
  - [岩井勇一郎](../Page/岩井勇一郎.md "wikilink")- 吉他
  - [大藪拓](../Page/大藪拓.md "wikilink")- 貝斯
  - [車谷啟介](../Page/車谷啟介.md "wikilink")- 鼓手

## 成員資料

  - **[三枝夕夏](../Page/三枝夕夏.md "wikilink")**（）

出身地：愛知縣名古屋市

生日：1980年6月9日

擔任：主音、作曲、作詞

  - **[岩井勇一郎](../Page/岩井勇一郎.md "wikilink")**（）

生日：6月6日

擔任：吉他

1998年夏天組成\[New Cinema 蜥蜴\]。擔任作曲
於吉他手。推出了11張單曲與2張專輯之後，於2002年夏天宣布解散。現在除了身為db的吉他手外，也以作曲家的身份活動著，主要提供曲是ZARD「」「」等。另外錄音室的工作中，他的合音能力也獲得好評。

  - **[大藪拓](../Page/大藪拓.md "wikilink")**（）

生日：1月30日

擔任：貝斯

除了貝斯手之外，他也擔任音樂工程師，替多位歌手做錄音工作。另外，他也在倉木麻衣、愛內里菜等多數歌手的LIVE演唱中，操作電子樂相關機器。

  - **[車谷啓介](../Page/車谷啓介.md "wikilink")**（）

生日：1月19日

擔任：鼓手

1998年夏天參加 \[New Cinema
蜥蜴\]。擔任鼓手。推出了11張單曲與2張專輯之後，於2002年夏天宣布解散。現在除了身為db的鼓手外，也為其他的藝人進行指導。

## 發行作品

### 單曲

| 順序 | 作品                   | 發行日期        | 銷售數量  |
| -- | -------------------- | ----------- | ----- |
| 1  | 君と約束した優しいあの場所まで      | 2003年10月29日 | 3萬4千張 |
| 2  | 眠る君の横顔に微笑みを          | 2004年3月3日   | 2萬5千張 |
| 3  | ジューンブライド 〜あなたしか見えない〜 | 2005年6月15日  | 2萬2千張 |
| 4  | へこんだ気持ち 溶かすキミ        | 2004年8月11日  | 1萬6千張 |
| 5  | It's for you         | 2002年8月28日  | 1萬5千張 |
| 6  | 笑顔でいようよ              | 2004年9月15日  | 1萬4千張 |
|    |                      |             |       |

<table>
<thead>
<tr class="header">
<th><p>序号</p></th>
<th><p>发售日期</p></th>
<th><p>标题</p></th>
<th><p>规格编号</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>2002年6月12日</p></td>
<td><p>''' <a href="../Page/Whenever_I_think_of_you.md" title="wikilink">Whenever I think of you</a> '''</p></td>
<td><p>GZCA-2041</p></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p>2002年8月28日</p></td>
<td><p>''' <a href="../Page/It&#39;s_for_you.md" title="wikilink">It's for you</a> '''</p></td>
<td><p>GZCA-2049</p></td>
</tr>
<tr class="odd">
<td><p>3rd</p></td>
<td><p>2002年11月6日</p></td>
<td><p>''' <a href="../Page/Tears_Go_By.md" title="wikilink">Tears Go By</a> '''</p></td>
<td><p>GZCA-7001</p></td>
</tr>
<tr class="even">
<td><p>4th</p></td>
<td><p>2003年6月18日</p></td>
<td><p>''' <a href="../Page/CHU☆TRUE_LOVE.md" title="wikilink">CHU☆TRUE LOVE</a> '''</p></td>
<td><p>GZCA-7020</p></td>
</tr>
<tr class="odd">
<td><p>5th</p></td>
<td><p>2003年8月20日</p></td>
<td><p>''' <a href="../Page/I_can&#39;t_see,_I_can&#39;t_feel.md" title="wikilink">I can't see, I can't feel</a> '''</p></td>
<td><p>GZCA-7029</p></td>
</tr>
<tr class="even">
<td><p>6th</p></td>
<td><p>2003年10月29日</p></td>
<td><p>''' <a href="../Page/君と約束した優しいあの場所まで.md" title="wikilink">君と約束した優しいあの場所まで</a> '''</p></td>
<td><p>GZCA-7034</p></td>
</tr>
<tr class="odd">
<td><p>7th</p></td>
<td><p>2004年3月3日</p></td>
<td><p>''' <a href="../Page/眠る君の横顔に微笑みを.md" title="wikilink">眠る君の横顔に微笑みを</a> '''</p></td>
<td><p>GZCA-7042＜初回盤＞<br />
GZCA-7046＜通常盤＞</p></td>
</tr>
<tr class="even">
<td><p>8th</p></td>
<td><p>2004年8月11日</p></td>
<td><p>''' <a href="../Page/へこんだ気持ち_溶かすキミ.md" title="wikilink">へこんだ気持ち 溶かすキミ</a> '''</p></td>
<td><p>GZCA-4008＜初回盤＞<br />
GZCA-4009＜通常盤＞</p></td>
</tr>
<tr class="odd">
<td><p>9th</p></td>
<td><p>2004年9月15日</p></td>
<td><p>''' <a href="../Page/笑顔でいようよ.md" title="wikilink">笑顔でいようよ</a> '''</p></td>
<td><p>GZCA-4013＜初回盤＞<br />
GZCA-4014＜通常盤＞</p></td>
</tr>
<tr class="even">
<td><p>10th</p></td>
<td><p>2004年10月13日</p></td>
<td><p>''' <a href="../Page/いつも心に太陽を.md" title="wikilink">いつも心に太陽を</a> '''</p></td>
<td><p>GZCA-4024</p></td>
</tr>
<tr class="odd">
<td><p>11th</p></td>
<td><p>2005年2月16日</p></td>
<td><p>''' <a href="../Page/飛び立てない私にあなたが翼をくれた.md" title="wikilink">飛び立てない私にあなたが翼をくれた</a> '''</p></td>
<td><p>GZCA-4033</p></td>
</tr>
<tr class="even">
<td><p>12th</p></td>
<td><p>2005年6月15日</p></td>
<td><p>''' <a href="../Page/ジューンブライド_〜あなたしか見えない〜.md" title="wikilink">ジューンブライド 〜あなたしか見えない〜</a> '''</p></td>
<td><p>GZCA-4043</p></td>
</tr>
<tr class="odd">
<td><p>13th</p></td>
<td><p>2005年11月16日</p></td>
<td><p>''' <a href="../Page/君の愛に包まれて痛い.md" title="wikilink">君の愛に包まれて痛い</a> '''</p></td>
<td><p>GZCA-4052</p></td>
</tr>
<tr class="even">
<td><p>14th</p></td>
<td><p>2006年2月15日</p></td>
<td><p>''' <a href="../Page/愛のワナ.md" title="wikilink">愛のワナ</a> '''</p></td>
<td><p>GZCA-4060</p></td>
</tr>
<tr class="odd">
<td><p>15th</p></td>
<td><p>2006年5月24日</p></td>
<td><p>''' <a href="../Page/Fall_in_Love.md" title="wikilink">Fall in Love</a> '''</p></td>
<td><p>GZCA-4069</p></td>
</tr>
<tr class="even">
<td><p>16th</p></td>
<td><p>2006年7月12日</p></td>
<td><p>''' <a href="../Page/Everybody_Jump.md" title="wikilink">Everybody Jump</a> '''</p></td>
<td><p>GZCA-4074</p></td>
</tr>
<tr class="odd">
<td><p>17th</p></td>
<td><p>2006年9月20日</p></td>
<td><p>''' <a href="../Page/太陽(单曲).md" title="wikilink">太陽</a> '''</p></td>
<td><p>GZCA-7078＜初回盤＞<br />
GZCA-7079＜通常盤＞</p></td>
</tr>
<tr class="even">
<td><p>18th</p></td>
<td><p>2007年1月31日</p></td>
<td><p>''' <a href="../Page/乘着白云.md" title="wikilink">雲に乗って</a> '''</p></td>
<td><p>GZCA-4086<初回盤><br />
GZCA-7085<通常盤></p></td>
</tr>
<tr class="odd">
<td><p>19th</p></td>
<td><p>2007年10月31日</p></td>
<td><p>''' <a href="../Page/明天在明天的风中.....梦中/新的自己的转变.md" title="wikilink">明日は明日の風の中.....夢の中／新しい自分へ変わるスイッチ</a> '''</p></td>
<td><p>GZCA-7098</p></td>
</tr>
<tr class="even">
<td><p>20th</p></td>
<td><p>2008年2月27日</p></td>
<td><p>''' <a href="../Page/雪どけのあの川の流れのように.md" title="wikilink">雪どけのあの川の流れのように</a> '''</p></td>
<td><p>GZCA-7104＜初回盤＞<br />
GZCA-7105＜通常盤＞</p></td>
</tr>
<tr class="odd">
<td><p>21st</p></td>
<td><p>2008年12月10日</p></td>
<td><p>''' <a href="../Page/誰もがきっと誰かのサンタクロース.md" title="wikilink">誰もがきっと誰かのサンタクロース</a> '''</p></td>
<td><p>GZCA-7130</p></td>
</tr>
<tr class="even">
<td><p>22nd</p></td>
<td><p>2009年2月25日</p></td>
<td><p>''' <a href="../Page/もう君をひとりにさせない.md" title="wikilink">もう君をひとりにさせない</a> '''</p></td>
<td><p>GZCA-7137</p></td>
</tr>
<tr class="odd">
<td><p>23rd</p></td>
<td><p>2009年6月3日</p></td>
<td><p>''' <a href="../Page/いつも素顔の私でいたい.md" title="wikilink">いつも素顔の私でいたい</a> '''</p></td>
<td><p>GZCA-7144</p></td>
</tr>
<tr class="even">
<td><p>24th</p></td>
<td><p>2009年8月26日</p></td>
<td><p>''' <a href="../Page/夏の終りにあなたへの手紙書きとめています.md" title="wikilink">夏の終りにあなたへの手紙書きとめています</a> '''</p></td>
<td><p>GZCA-7148</p></td>
</tr>
</tbody>
</table>

### 專輯

### Original Album

<table>
<thead>
<tr class="header">
<th><p>序号</p></th>
<th><p>发售日</p></th>
<th><p>标题</p></th>
<th><p>规格编号</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>2003年11月19日</p></td>
<td><p>''' <a href="../Page/三枝夕夏_IN_db_1st_〜君と約束した優しいあの場所まで〜.md" title="wikilink">三枝夕夏 IN db 1st 〜君と約束した優しいあの場所まで〜</a> '''</p></td>
<td><p>GZCA-5042</p></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p>2004年11月17日</p></td>
<td><p>''' <a href="../Page/U-ka_saegusa_IN_db_II.md" title="wikilink">U-ka saegusa IN db II</a> '''</p></td>
<td><p>GZCA-5055</p></td>
</tr>
<tr class="odd">
<td><p>3rd</p></td>
<td><p>2006年9月20日</p></td>
<td><p>''' <a href="../Page/U-ka_saegusa_IN_db_III.md" title="wikilink">U-ka saegusa IN db III</a> '''</p></td>
<td><p>GZCA-5088＜初回限定盤＞<br />
GZCA-5089＜通常盤＞</p></td>
</tr>
<tr class="even">
<td><p>4th</p></td>
<td><p>2009年11月25日</p></td>
<td><p>''' <a href="../Page/U-ka_saegusa_IN_db_IV_〜クリスタルな季節に魅せられて〜.md" title="wikilink">U-ka saegusa IN db IV 〜クリスタルな季節に魅せられて〜</a> '''</p></td>
<td><p>GZCA-5201＜初回限定盤＞<br />
GZCA-5202＜通常盤＞</p></td>
</tr>
</tbody>
</table>

### Best Album

<table>
<thead>
<tr class="header">
<th><p>序号</p></th>
<th><p>发售日</p></th>
<th><p>标题</p></th>
<th><p>规格编号</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>2007年6月6日</p></td>
<td><p><strong><a href="../Page/三枝夕夏_IN_d-best_〜Smile&amp;Tears〜.md" title="wikilink">三枝夕夏 IN d-best 〜Smile&amp;Tears〜</a></strong></p></td>
<td><p>GZCA-5104＜初回盤A＞<br />
GZCA-5105＜初回盤B＞<br />
GZCA-5106＜通常盤B＞</p></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p>2010年1月13日</p></td>
<td><p><strong><a href="../Page/U-ka_saegusa_IN_db_Final_Best.md" title="wikilink">U-ka saegusa IN db Final Best</a></strong></p></td>
<td><p>GZCA-5211<br />
GZCA-5212</p></td>
</tr>
</tbody>
</table>

### Mini Album

| 序号  | 发售日       | 标题                                                    | 规格编号      |
| --- | --------- | ----------------------------------------------------- | --------- |
| 1st | 2003年2月5日 | **[Secret\&Lies](../Page/Secret&Lies.md "wikilink")** | GZCA-5028 |

### DVD

| 序号  | 发售日         | 标题                                                                                                                                       |
| --- | ----------- | ---------------------------------------------------------------------------------------------------------------------------------------- |
| 1st | 2003年11月19日 | [U-ka saegusa IN db FILM COLLECTION VOL.1-SHOCKING BLUE-](../Page/U-ka_saegusa_IN_db_FILM_COLLECTION_VOL.1-SHOCKING_BLUE-.md "wikilink") |
| 2nd | 2005年2月16日  | [U-ka saegusa IN db〔one 1 Live〕](../Page/U-ka_saegusa_IN_db〔one_1_Live〕.md "wikilink")                                                   |
| 3rd | 2005年9月21日  | [U-ka saegusa IN db FILM COLLECTION VOL.2](../Page/U-ka_saegusa_IN_db_FILM_COLLECTION_VOL.2.md "wikilink")                               |
| 4th | 2006年11月1日  | [U-ka saegusa IN db“CHOCO II とLIVE”](../Page/U-ka_saegusa_IN_db“CHOCO_II_とLIVE”.md "wikilink")                                           |
| 5th | 2008年2月27日  | [三枝夕夏 IN d-best LIVE 〜Smile & Tears〜](../Page/三枝夕夏_IN_d-best_LIVE_〜Smile_&_Tears〜.md "wikilink")                                         |
| 6th | 2010年4月21日  | [U-ka saegusa IN db -FINAL LIVE TOUR 2010-](../Page/U-ka_saegusa_IN_db_-FINAL_LIVE_TOUR_2010-.md "wikilink")                             |

## 外部連結

  - [三枝夕夏IN db 日本官方網站](http://www.uka-saegusa.com/)
  - [三枝夕夏IN db 官方部落格](http://blog.livedoor.jp/uka_db/)

[Category:日本樂團](../Category/日本樂團.md "wikilink")
[Category:Being旗下藝人](../Category/Being旗下藝人.md "wikilink")
[Category:2002年成立的音樂團體](../Category/2002年成立的音樂團體.md "wikilink")
[Category:2010年解散的音樂團體](../Category/2010年解散的音樂團體.md "wikilink")