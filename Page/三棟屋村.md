[HK_SamTungUkResiteVillage_Gateway.JPG](https://zh.wikipedia.org/wiki/File:HK_SamTungUkResiteVillage_Gateway.JPG "fig:HK_SamTungUkResiteVillage_Gateway.JPG")
[HK_SamTungUkResiteVillage_NearSamTungUkRoad.JPG](https://zh.wikipedia.org/wiki/File:HK_SamTungUkResiteVillage_NearSamTungUkRoad.JPG "fig:HK_SamTungUkResiteVillage_NearSamTungUkRoad.JPG")
**三棟屋村**是[香港](../Page/香港.md "wikilink")[新界一個重置舊](../Page/新界.md "wikilink")[客家](../Page/客家.md "wikilink")[圍村](../Page/圍村.md "wikilink")，本來位於[荃灣近現時](../Page/荃灣.md "wikilink")[荃灣站一帶](../Page/荃灣站.md "wikilink")（土名[牛牯墩](../Page/牛牯墩.md "wikilink")），後來因[地鐵工程遷至](../Page/香港地鐵.md "wikilink")[象山邨以北](../Page/象山邨.md "wikilink")，[大帽山南面山腰](../Page/大帽山.md "wikilink")。舊有村落因保存良好，被政府闢作[三棟屋博物館](../Page/三棟屋博物館.md "wikilink")，更成為[香港法定古蹟](../Page/香港法定古蹟.md "wikilink")。

## 歷史

三棟屋村由[陳氏人士所建](../Page/陳姓.md "wikilink")，原本居住於[福建省](../Page/福建省.md "wikilink")[汀州府](../Page/汀州府.md "wikilink")[寧化縣](../Page/寧化縣.md "wikilink")，後來有一支搬到去[廣東省](../Page/廣東省.md "wikilink")[廣州府](../Page/廣州.md "wikilink")[新安縣](../Page/深圳市.md "wikilink")[羅芳](../Page/羅芳.md "wikilink")。到了第十三代，當中的村民陳任盛搬到荃灣鹹田附近居住，曾於[大窩口填海開地種田](../Page/大窩口.md "wikilink")，並興建一座草屋。其長子陳健常其後看中牛牯墩西部一地，發現風水極佳，便在該處立村，稱為三棟屋村。

三棟屋村於1786年建立，本來只有一座三進口的房舍，[棟是](../Page/棟.md "wikilink")[主樑的意思](../Page/主樑.md "wikilink")，三棟屋因此而得名，可解作「三條棟樑的房子」。陳氏後人後來先後在三棟屋村的兩旁及後面加建房子，遂漸成為今貌，佈局左右對稱如棋盤，中軸線上分別建有前廳、中廳及祠堂，在正門石楣上刻有「陳氏家祠」四字，而兩旁分別築有四間居室，左右及後排的橫屋將整個三棟屋圍攏成圍村。

1979年首季，由於[香港地鐵](../Page/香港地鐵.md "wikilink")[荃灣綫的工程](../Page/荃灣綫.md "wikilink")，三棟屋村村民放棄原址他遷。1987年，三棟屋村遺址經當時的[區域市政局修葺後](../Page/區域市政局.md "wikilink")，改為[三棟屋博物館開放讓公眾人士參觀](../Page/三棟屋博物館.md "wikilink")，現由[康樂及文化事務署管理](../Page/康樂及文化事務署.md "wikilink")。现存的三棟屋村位于[三棟屋路近](../Page/三棟屋路.md "wikilink")[和宜合交汇处一带](../Page/和宜合交汇处.md "wikilink")。

## 公共交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

</div>

</div>

## 外部連結

  - [康樂及文化事務署 - 三棟屋村](http://www.amo.gov.hk/b5/monuments_10.php)

[Category:香港客家鄉村](../Category/香港客家鄉村.md "wikilink")
[Category:荃灣區鄉村](../Category/荃灣區鄉村.md "wikilink")