**三氧化钨**（[化学式](../Page/化学式.md "wikilink")：[W](../Page/钨.md "wikilink")[O](../Page/氧.md "wikilink")<sub>3</sub>）是[钨](../Page/钨.md "wikilink")([VI](../Page/氧化态.md "wikilink"))的[氧化物](../Page/氧化物.md "wikilink")，是从钨矿制取单质钨工业的重要中间体。\[1\]该冶炼过程涉及两步：第一步用[碱处理钨矿](../Page/碱.md "wikilink")，制得WO<sub>3</sub>，然后用[碳或](../Page/碳.md "wikilink")[氢气还原三氧化钨](../Page/氢气.md "wikilink")，得到金属钨：

  -
    WO<sub>3</sub> + 3H<sub>2</sub> → W + 3H<sub>2</sub>O
    2WO<sub>3</sub> + 3C → 2W + 3CO<sub>2</sub>

## 制备

三氧化钨可由很多方法制备：

  - 先用[钨酸钙与](../Page/钨酸钙.md "wikilink")[盐酸反应生成](../Page/盐酸.md "wikilink")[钨酸沉淀](../Page/钨酸.md "wikilink")，然后钨酸高温分解成为三氧化钨和水。\[2\]

<!-- end list -->

  -

      -
        CaWO<sub>4</sub> + 2HCl → CaCl<sub>2</sub> +
        H<sub>2</sub>WO<sub>4</sub>
        H<sub>2</sub>WO<sub>4</sub> → H<sub>2</sub>O + WO<sub>3</sub>

<!-- end list -->

  - 氧化剂存在下，[仲钨酸铵热分解](../Page/仲钨酸铵.md "wikilink")：\[3\]

<!-- end list -->

  -

      -
        (NH<sub>4</sub>)<sub>10</sub>\[H<sub>2</sub>W<sub>12</sub>O<sub>42</sub>\]·4H<sub>2</sub>O
        → 12 WO<sub>3</sub> + 10NH<sub>3</sub> + 11H<sub>2</sub>O

## 结构

三氧化钨的结构取决于温度：它在740 °C以上为[四方晶系](../Page/四方晶系.md "wikilink")、330-740 °C为[正交晶系](../Page/正交晶系.md "wikilink")、17-330 °C为[单斜晶系](../Page/单斜晶系.md "wikilink")、-50-17 °C为[三斜晶系](../Page/三斜晶系.md "wikilink")。单斜的结构最常见，其空间群为P2<sub>1</sub>/n。\[4\]

## 化学性质

三氧化钨的性质随制备条件的不同（速率和温度）而不同：低温下制得的三氧化钨较活泼，易溶于水；\[5\]高温制得的三氧化钨则不溶于水。此外，若仲钨酸铵热分解时为还原性气氛，则产物为蓝色氧化钨（[钨蓝](../Page/钨蓝.md "wikilink")，WO<sub>3-x</sub>），\[6\]组分不定，主要是三氧化钨、[铵盐和](../Page/铵.md "wikilink")[二氧化钨](../Page/二氧化钨.md "wikilink")。

用[还原剂](../Page/还原剂.md "wikilink")，如[锡还原](../Page/锡.md "wikilink")[钨酸盐溶液也可得到](../Page/钨酸盐.md "wikilink")“钨蓝”，使整个溶液呈蓝色。

## 用途

除制取金属钨外，黄色的三氧化钨也可作为颜料，用在陶瓷和涂料中。\[7\]使用红外线的非接触式车窗控制系统（[Smart
windows](../Page/:en:Smart_windows.md "wikilink")）中，也应用了三氧化钨。\[8\]

## 参考资料

[Category:钨化合物](../Category/钨化合物.md "wikilink")
[Category:氧化物](../Category/氧化物.md "wikilink")

1.  Patnaik, Pradyot. Handbook of Inorganic Chemicals. New York:
    McGraw-Hill, 2003.

2.
3.
4.  Lassner, Erik and Wolf-Dieter Schubert. Tungsten: Properties,
    Chemistry, Technology of the Element, Alloys, and Chemical
    Compounds. New York: Kluwer Academic, 1999.

5.
6.  "Tungsten Oxides & Acids" *International Tungsten Industry
    Association*. 2003\<<http://www.itia.org.uk/Default.asp?Page=44>\>.

7.
8.  Lee, W.J.; Fang, Y.K.; Ho, J.; Hsieh, W.T.; Ting, S.F. J. Electron.
    Mater. 2000, 29 (2), 183.