**三氯化锑**是一个[无机化合物](../Page/无机化合物.md "wikilink")，[化学式为SbCl](../Page/化学式.md "wikilink")<sub>3</sub>。

## 制备

由[銻與](../Page/銻.md "wikilink")[氯作用或將](../Page/氯.md "wikilink")[三硫化二锑溶解於](../Page/三硫化二锑.md "wikilink")[鹽酸中製得](../Page/鹽酸.md "wikilink")。用於製[藥物](../Page/藥物.md "wikilink")、色澱及銻鹽等，也用作媒染劑、有機合成中的[氣化劑和](../Page/氣化劑.md "wikilink")[催化劑以及化學試劑等](../Page/催化.md "wikilink")。

## 物理性质

三氯化锑溶於[稀鹽酸](../Page/稀鹽酸.md "wikilink")、[醇](../Page/醇.md "wikilink")、[苯](../Page/苯.md "wikilink")、[二硫化碳](../Page/二硫化碳.md "wikilink")、[氯仿](../Page/氯仿.md "wikilink")、[四氯化碳](../Page/四氯化碳.md "wikilink")、[乙醚](../Page/乙醚.md "wikilink")、[丙酮等中](../Page/丙酮.md "wikilink")。也溶于液体二氧化硫、液体硫化氢以及酒石酸等各种无机液体。

三氯化锑本身也是一种溶剂，可以溶解一些物质，如[碘等](../Page/碘.md "wikilink")。

## 化学性质

三氯化锑遇到水发生水解，生成[氧氯化銻](../Page/氧氯化銻.md "wikilink")（SbOCl），因此，在配制三氯化锑水溶液时，需要先用浓盐酸溶解来抑制水解，再稀释。

三氯化锑可以和[维生素A在氯仿中形成蓝色的加合物](../Page/维生素A.md "wikilink")\[1\]：

  -
    SbCl<sub>3</sub> + C<sub>20</sub>H<sub>29</sub>OH →
    C<sub>20</sub>H<sub>29</sub>OH·SbCl<sub>3</sub>

和[维生素D在氯仿中形成橙色化合物](../Page/维生素D.md "wikilink")。\[2\]

和二氧杂环己烷在甲醇中反应，可以得到配合物SbCl<sub>3</sub>·\[(CH<sub>2</sub>)<sub>4</sub>O<sub>2</sub>\]<sub>1.5</sub>（白色）：\[3\]

  -
    SbCl<sub>3</sub> + 1.5 (CH<sub>2</sub>)<sub>4</sub>O<sub>2</sub> →
    SbCl<sub>3</sub>·\[(CH<sub>2</sub>)<sub>4</sub>O<sub>2</sub>\]<sub>1.5</sub>

和[视黄醇在氯仿中反应](../Page/视黄醇.md "wikilink")，形成蓝色可溶物，并在数秒内稳定。\[4\]

## 安全

  - 危害：
    會經由吸入、食入以及[皮膚吸收](../Page/皮膚.md "wikilink")，造成人體健康危害，且高濃度的三氯化銻對眼睛、皮膚甚至呼吸道等，有強烈的刺激性，也可能因此而引發[支氣管炎](../Page/支氣管炎.md "wikilink")、[肺水腫等呼吸方面疾病](../Page/肺水腫.md "wikilink")。
    严重时可导致死亡\[5\]。

<!-- end list -->

  - 特性：當遇到受熱或者水分解，則會釋放出有毒的腐蝕性煙霧。若經燃燒分解，會產生「[氯化物](../Page/氯化物.md "wikilink")」。

## 處理方式

  - 隔離污染區，周圍設置警告標語，處理人員需配戴呼吸器及穿著化學防護衣。避免直接接觸。
  - 不慎[皮膚接觸](../Page/皮膚.md "wikilink")，須立即用大量清水沖洗，若有灼傷，則必須送醫治療。若是[眼睛接觸](../Page/眼睛.md "wikilink")，需用清水沖洗，或者使用少許[碳酸氫鈉溶液沖洗](../Page/碳酸氫鈉.md "wikilink")。
  - 不慎吸食，則必要時對患者使用[人工呼吸](../Page/人工呼吸.md "wikilink")，然後盡速就醫。若是食入，患者清醒的情況下可給予漱口，然後送醫。

## 用途

利用三氯化锑的水解可以用来制备[三氧化二锑](../Page/三氧化二锑.md "wikilink")：\[6\]

  -
    SbCl<sub>3</sub> + H<sub>2</sub>O → Sb(OH)Cl<sub>2</sub> + HCl
    Sb(OH)Cl<sub>2</sub> + H<sub>2</sub>O → Sb(OH)<sub>2</sub>Cl + HCl
    Sb(OH)<sub>2</sub>Cl → SbOCl + HCl
    4 SbOCl + H<sub>2</sub>O → Sb<sub>2</sub>O<sub>3</sub>·2SbOCl + 2
    HCl

生成的Sb<sub>2</sub>O<sub>3</sub>·2SbOCl和Na<sub>2</sub>CO<sub>3</sub>溶液反应，可脱去氯：

  -
    Sb<sub>2</sub>O<sub>3</sub>·2SbOCl + Na<sub>2</sub>CO<sub>3</sub> →
    2Sb<sub>2</sub>O<sub>3</sub> + 2NaCl + CO<sub>2</sub>↑

此外，三氯化锑还是一些反应中的催化剂。

## 参见

  - [苦艾酒](../Page/苦艾酒.md "wikilink")
  - [聚碳酸酯](../Page/聚碳酸酯.md "wikilink")

## 参考资料

## 外部連結

  - [Jmol
    三維圖像](http://chemapps.stolaf.edu/jmol/jmol.php?model=Cl%5BSb%5D%28Cl%29Cl)

[Category:氯化物](../Category/氯化物.md "wikilink")
[Category:金属卤化物](../Category/金属卤化物.md "wikilink")
[Category:锑化合物](../Category/锑化合物.md "wikilink")
[Category:炼金术物质](../Category/炼金术物质.md "wikilink")

1.  《无机化学丛书》.第四卷 氮 磷 砷分族.科学出版社. 3.4 锑的卤化物. P<sub>342</sub>
2.  EDGAR M. SHANTZ. Antimony Trichloride Reaction of Vitamin D\[J\].
    *Ind. Eng. Chem. Anal. Ed.*, **1944**, *16* (3), pp 179–180 DOI:
    10.1021/i560127a010
3.  臧祥生 等.
    三氯化锑和二氧杂环己烷配合物\[SbCl<sub>3</sub>·{(CH<sub>2</sub>)<sub>4</sub>O<sub>2</sub>}<sub>1.5</sub>\]的合成与晶体结构\[J\].
    无机化学学报. 2011.11 Vol.6 pp.901-904
4.  张应年. 三氯化锑比色法测定牛肝视黄醇\[J\]. 甘肃农业大学学报. 1997.9. Vol.32 pp267-270
5.  李建伟 等. 三氯化锑小面积烫伤死亡一例\[J\]. 中华烧伤杂志. 2004.2 Vol.20. P<sub>53</sub>
6.  郑文裕 等. 锑白生产新工艺——用三氯化锑水解法制取三氧化二锑\[J\]. 湿法冶金. 1991(3).
    P<sub>13-18</sub>