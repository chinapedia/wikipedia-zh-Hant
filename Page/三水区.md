**三水**（官方外文名称为，传统粤语外文名称为或\[1\]）是[中国](../Page/中华人民共和国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[佛山市的一个](../Page/佛山市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")，位于广东省的中部偏西、佛山市境西北部，地處珠三角的腹地，因[西江](../Page/西江.md "wikilink")、[北江](../Page/北江.md "wikilink")、[綏江彙流境內而得名](../Page/绥江_\(广东河流\).md "wikilink")。總面積874.22平方公里，2015年全區常住人口為64萬人。三水是著名[華人的僑鄉](../Page/華人.md "wikilink")，現居港澳台及海外的三水籍人士有20多萬人。

## 歷史

  - 三水区[白坭镇银洲贝丘遗址证明](../Page/白坭镇.md "wikilink")，早在4000多年前的[新石器时代](../Page/新石器时代.md "wikilink")，三水已經有人类居住活动。
  - 秦代至明朝中期（公元前221年至公元1525年）分別屬[高要縣](../Page/高要縣.md "wikilink")、[番禺縣和](../Page/番禺縣.md "wikilink")[南海縣管轄](../Page/南海縣.md "wikilink")。
  - 公元1526年6月10日（[明](../Page/明.md "wikilink")[嘉靖五年](../Page/嘉靖.md "wikilink")[五月癸未](../Page/五月癸未.md "wikilink")）始置[三水縣](../Page/三水縣.md "wikilink")，因位置處於西江、北江和綏江三江匯流縣境，取「三水合流」之意而得名。
  - 1959年3月2日，三水縣被併入[南海縣](../Page/南海縣.md "wikilink")。1960年9月30日，恢復三水縣建制。1993年3月29日，撤縣建市（縣級）。2002年12月8日，[中華人民共和國](../Page/中華人民共和國.md "wikilink")[國務院批准調整佛山市行政區劃](../Page/中华人民共和国国务院.md "wikilink")：撤銷縣級[三水市](../Page/三水市.md "wikilink")，設立佛山市三水區。2003年1月8日，三水區正式掛牌成立，成為佛山市五個轄區之一。

## 行政区划

下辖2個[街道](../Page/街道.md "wikilink")：[西南街道](../Page/西南街道_\(佛山市\).md "wikilink")，[云东海街道](../Page/云东海街道.md "wikilink")。5个[镇](../Page/镇.md "wikilink")：[蘆苞鎮](../Page/蘆苞鎮.md "wikilink")，[大塘鎮](../Page/大塘鎮_\(佛山市\).md "wikilink")，[樂平鎮](../Page/樂平鎮_\(佛山市\).md "wikilink")，[白坭鎮](../Page/白坭鎮.md "wikilink")，[南山鎮](../Page/南山鎮_\(佛山市\).md "wikilink")。

## 地理

三水區地處[珠江三角洲西北角](../Page/珠江三角洲.md "wikilink")，是珠三角通往粵西、粵西北和大西南地區的咽喉要衝，東鄰[廣州市](../Page/廣州市.md "wikilink")[花都區](../Page/花都區.md "wikilink")，東南與佛山市[南海區相連](../Page/南海區.md "wikilink")，西北與[四會市交界](../Page/四會市.md "wikilink")，北接[清新縣](../Page/清新縣.md "wikilink")，西南與[高要市](../Page/高要市.md "wikilink")、佛山市[高明區隔](../Page/高明區.md "wikilink")[西江相望](../Page/西江.md "wikilink")。毗鄰珠三角大中城市群及港、澳地區，距[廣州](../Page/廣州.md "wikilink")30公里、[香港](../Page/香港.md "wikilink")230公里、[澳門](../Page/澳門.md "wikilink")195公里。

三水区的區域形狀狹長，南北最長為68公里，東西最寬為30.1公里，地勢自西北向東南傾斜，主要為[丘陵](../Page/丘陵.md "wikilink")、[台地](../Page/台地.md "wikilink")、平原區，且多積水洼地，西北多高丘，最高峰西平嶺海拔591米，東南多衝積平原及低丘。北江和西江在[河口交匯](../Page/河口.md "wikilink")。

## 交通

三水水陸空交通便利。

  - 铁路：[广茂铁路](../Page/广茂铁路.md "wikilink")、[南廣高鐵、貴廣高鐵](../Page/南廣鐵路.md "wikilink")、广佛肇城际轻轨
  - 公路：[321國道](../Page/321國道.md "wikilink")、[324國道](../Page/324國道.md "wikilink")，[廣三高速公路](../Page/廣三高速公路.md "wikilink")、[廣肇高速公路及](../Page/廣肇高速公路.md "wikilink")[肇花高速公路橫貫全境](../Page/肇花高速公路.md "wikilink")。
  - 水运：擁有黃金水道[西江](../Page/西江.md "wikilink")、[北江](../Page/北江.md "wikilink")，有可營造300～5000噸位碼頭的岸線近100公里，設有三水海關和可靠泊3000噸級船舶的大型港口[三水港](../Page/三水港.md "wikilink")，每日有貨輪往返港澳。
  - 航空：[广州白云国际机场僅](../Page/广州白云国际机场.md "wikilink")25分鐘車程。
  - 地鐵：[佛山地铁](../Page/佛山地铁.md "wikilink")8号线由[佛山市铁路投资建设集团有限公司负责建设](../Page/佛山市铁路投资建设集团有限公司.md "wikilink")，由[佛山西站至三水](../Page/佛山西站.md "wikilink")[西南街道](../Page/西南街道_\(佛山市\).md "wikilink")，全长为18.9km，将途经[佛山西站](../Page/佛山西站.md "wikilink")、[小塘](../Page/小塘.md "wikilink")、广海大道、西南街道组团联系，具体规划尚在研究中。
  - 区级公交：设有59条路线，可通往三水区内各镇街及[南海区内部分镇街](../Page/南海区.md "wikilink")。西南街道内（不包含北江新区、三水新城、金本、南岸）上车投币2元，超出西南街道实行分段收费，营运时间：06:00——22:30。
  - 镇街公交：设有24条路线，可通往镇街内各自然村，西南街道、[白坭镇](../Page/白坭镇.md "wikilink")、[乐平镇](../Page/乐平镇_\(佛山市\).md "wikilink")、[大塘镇](../Page/大塘镇_\(佛山市\).md "wikilink")、[芦苞镇](../Page/芦苞镇.md "wikilink")、[南山镇均设有镇巴](../Page/南山镇_\(佛山市\).md "wikilink")，实行分段收费，营运时间：06:30——22:30。
  - 佛山城巴：设有10条路线，可通往[禅城区](../Page/禅城区.md "wikilink")、[南海区](../Page/南海区.md "wikilink")、[顺德区](../Page/顺德区.md "wikilink")、[高明区](../Page/高明区.md "wikilink")，实行分段收费，营运时间：06:00——21:00。
  - 广佛城巴：设有3条路线，可通往[广州市](../Page/广州.md "wikilink")[荔湾区](../Page/荔湾区.md "wikilink")、[白云区](../Page/白云区_\(广州市\).md "wikilink")，实行分段收费，营运时间：05:20——19:10。

## 氣候

1月份均溫12.7℃，7月均溫28℃。年降水量1687毫米。

## 自然資源

三水區自然資源有[石油](../Page/石油.md "wikilink")、[天然氣](../Page/天然氣.md "wikilink")、[煤](../Page/煤.md "wikilink")、[硫鐵](../Page/硫鐵.md "wikilink")、[油母頁岩](../Page/油母頁岩.md "wikilink")、[石膏](../Page/石膏.md "wikilink")、[石灰石](../Page/石灰石.md "wikilink")、[泥炭土](../Page/泥炭土.md "wikilink")、[陶瓷土](../Page/陶瓷土.md "wikilink")、[玻璃砂](../Page/玻璃砂.md "wikilink")、[岩鹽等礦藏](../Page/岩鹽.md "wikilink")；野生動物有[野豬](../Page/野豬.md "wikilink")、[豪豬](../Page/豪豬.md "wikilink")、[果子狸](../Page/果子狸.md "wikilink")、[穿山甲](../Page/穿山甲.md "wikilink")、[禾花雀](../Page/黃胸鵐.md "wikilink")、[山雞](../Page/山雞.md "wikilink")、[貓頭鷹](../Page/貓頭鷹.md "wikilink")、[水鴨等](../Page/水鴨.md "wikilink")；林木則有[木棉](../Page/木棉.md "wikilink")、[榕](../Page/榕.md "wikilink")、[樟](../Page/樟.md "wikilink")、[格木](../Page/格木.md "wikilink")、[柚木](../Page/柚木.md "wikilink")、[苦楝](../Page/苦楝樹.md "wikilink")、[馬尾松等](../Page/馬尾松.md "wikilink")；另有優質[礦泉水](../Page/礦泉水.md "wikilink")。

## 人口

2008年全区总户数為124082户，总人口390254人；全区百岁以上寿星99人。2011年全区常住人口為625375人，其中户籍人口397150人，户籍人口全部为非农业人口。\[2\]

居民绝大部分为[汉族](../Page/汉族.md "wikilink")，占总人口99.7%以上；其余为[壮](../Page/壮族.md "wikilink")、[瑶](../Page/瑶族.md "wikilink")、[满](../Page/满族.md "wikilink")、[土家](../Page/土家族.md "wikilink")、[回](../Page/回族.md "wikilink")、[侗](../Page/侗族.md "wikilink")、[藏](../Page/藏族.md "wikilink")、[苗](../Page/苗族.md "wikilink")、[土](../Page/土族.md "wikilink")、[黎](../Page/黎族.md "wikilink")、[布衣](../Page/布衣族.md "wikilink")、[蒙古](../Page/蒙古族.md "wikilink")、[朝鲜](../Page/朝鲜族.md "wikilink")、[仡佬](../Page/仡佬族.md "wikilink")、[彝](../Page/彝族.md "wikilink")、[白](../Page/白族.md "wikilink")、[傣](../Page/傣族.md "wikilink")、[毛南](../Page/毛南族.md "wikilink")、[京等民族](../Page/京族.md "wikilink")。

大部分地区讲[粵語](../Page/粵語.md "wikilink")[广州話](../Page/广州話.md "wikilink")，大塘镇六和片多数村落讲[客家方言](../Page/客家方言.md "wikilink")，约占总人口的3%。\[3\]

## 經濟

三水區是廣東省主要產糧區之一，主種[稻](../Page/稻.md "wikilink")，其次為[玉米](../Page/玉米.md "wikilink")、[甘薯](../Page/甘薯_\(薯蕷屬\).md "wikilink")、[甘蔗](../Page/甘蔗.md "wikilink")、[花生](../Page/花生.md "wikilink")、水果、[蔬菜](../Page/蔬菜.md "wikilink")、中藥材等，盛產塘魚及河鮮。工業有建材、[紡織](../Page/紡織.md "wikilink")、食品、化工、機電、飲料等，「[健力寶](../Page/健力寶.md "wikilink")」是著名飲料產品。

2016年[國內生產總值](../Page/國內生產總值.md "wikilink")1092億。

三水區在2016年度在全國中小城市綜合實力100強評比中，排名第36位，5個鎮（街道）全部入圍全國小城鎮綜合發展指數測評排名中的千強鎮。

## 特產

特產有[西南醬油](../Page/西南醬油.md "wikilink")、[貓兒坑西瓜](../Page/貓兒坑西瓜.md "wikilink")、[三紅馬蹄](../Page/三紅馬蹄.md "wikilink")、[大塘三黃雞](../Page/大塘三黃雞.md "wikilink")、[南邊白雞](../Page/南邊白雞.md "wikilink")、[白坭黑皮冬瓜](../Page/白坭黑皮冬瓜.md "wikilink")、[大路竇明蝦](../Page/大路竇明蝦.md "wikilink")、[彭灶豆腐等](../Page/彭灶豆腐.md "wikilink")。

## 著名品牌

李宁服装厂：1988年汉城奥运会后，著名运动员李宁退役，在健力宝董事长李经纬的支持下，健力宝与李宁及一家新加坡企业成立健力宝运动服装公司，李宁出任总经理。1996年初，李宁将公司总部从广东迁到北京，并更名为李宁运动服装公司，彻底告别了健力宝。在随后的十几年间，“李宁牌服装”壮大成为中国屈指可数的运动品牌之一。

强力啤酒：1987年三水县啤酒厂更名强力啤酒厂，年产逾50000吨。

金嗓可乐：1986年三水县食品厂研制的保健饮料，1988年销量近1200吨。

暖美威牌羊毛衫：三水县羊毛衫厂名牌产品，曾获省优、部优称号。

佳力牌镉镍电池：出口量曾在全国同行业中居於首位。

三虎牌、三鹿牌水泥：曾获得省优、部优等称号。

芦苞从心泉腊味：历史已超过70年，制作的腊肠、腊肉和腊鸭，兴盛时曾远销新加坡等地。

西南酱油厂：三和酱园、三兴酱园、矩隆酱园、信昌酱园、又成酱园是三水规模较大的酱园，其后五家酱园合并成西南酱油厂。

## 文化

由三水區文化廣電新聞出版局於2007年1月31日公佈「三水區第一批[非物質文化遺產代表作名錄](../Page/非物質文化遺產.md "wikilink")」，包括[粵曲星腔](../Page/粵曲星腔.md "wikilink")、[水上婚禮](../Page/水上婚禮.md "wikilink")、[黃塘狗肉宴](../Page/黃塘狗肉宴.md "wikilink")、紅頭巾、[上塘竹編](../Page/上塘竹編.md "wikilink")、[搶炮](../Page/搶炮.md "wikilink")、[龍形拳](../Page/龍形拳.md "wikilink")、[華僑民間風情舞](../Page/華僑民間風情舞.md "wikilink")、[賽龍舟](../Page/賽龍舟.md "wikilink")、[八音鑼鼓](../Page/八音鑼鼓.md "wikilink")。
粵語地區流行一些取笑三水人的歇後語，例如三水佬走馬燈，三水佬考狀元（ 得個少個），三水佬食麪， 等等 （ 詳見：
<https://hk.answers.yahoo.com/question/index?qid=20080529000051KK02753>
）

## 教育

全区现有[大学](../Page/大学.md "wikilink")1所（即[广东财经大学三水校区](../Page/广东财经大学三水校区.md "wikilink")）、[电大](../Page/电大.md "wikilink")1所、[中等职业技术学校](../Page/中等职业技术学校.md "wikilink")2所、业余[体校](../Page/体校.md "wikilink")1所、[特殊教育学校](../Page/特殊教育.md "wikilink")1所、公办中小学校57所（普通[高中](../Page/高中.md "wikilink")3所、[初级中学](../Page/初级中学.md "wikilink")14所、[小学](../Page/小学.md "wikilink")40所）、民办学校15所（其中完全中学1所、职业学校2所、义务教育阶段学校12所）。\[4\]

### 三水同鄉會

又，三水同鄉會在香港開辦多所學交，包括：

  - [三水同鄉會劉本章學校](../Page/三水同鄉會劉本章學校.md "wikilink")
  - [三水同鄉會禤景榮學校](../Page/三水同鄉會禤景榮學校.md "wikilink")
  - [星願小王子](../Page/星願小王子.md "wikilink")：有關機構學校開發的教學軟體

## 旅遊景點

  - [宋代](../Page/宋代.md "wikilink")[蘆苞祖廟](../Page/蘆苞祖廟.md "wikilink")
  - [明代](../Page/明代.md "wikilink")[魁崗文塔](../Page/魁崗文塔.md "wikilink")
  - [三水荷花世界](../Page/三水荷花世界.md "wikilink")
  - [三水森林公園](../Page/三水森林公園.md "wikilink")，內有大臥佛、孔聖園、小型賽車場等景區。

## 著名人物

  - [何維栢](../Page/何維栢.md "wikilink")：明代大理寺少卿
  - [汪精衛](../Page/汪精衛.md "wikilink")、
  - [陈心陶](../Page/陈心陶.md "wikilink")：寄生蟲學家。
  - [凤凰女](../Page/凤凰女.md "wikilink")、
  - [邓碧云](../Page/邓碧云.md "wikilink")、
  - [韦基舜](../Page/韦基舜.md "wikilink")、

<!-- end list -->

  - [鄧曼薇](../Page/鄧曼薇.md "wikilink")、
  - [鄧國光](../Page/鄧國光.md "wikilink")、
  - [鄧立光](../Page/鄧立光.md "wikilink")、
  - [李乐诗](../Page/李乐诗_\(探险家\).md "wikilink")、
  - [蔡楓華](../Page/蔡楓華.md "wikilink")、
  - [劉青雲](../Page/劉青雲.md "wikilink")
  - [李寧](../Page/李寧.md "wikilink")： 廣西柳州人，壯族 在廣東佛山市三水區創立健力寶集團（
    生產健力寶健康飲料）
  - [黃子華](../Page/黃子華.md "wikilink")
  - 明居正一國立臺灣大學政治系教授，硏究中共派系鬥爭，祖籍廣東三水，在臺北市出生長大
  - [林穎嫻](../Page/林穎嫻.md "wikilink")（1987年[香港小姐季軍](../Page/香港小姐.md "wikilink")）、

<!-- end list -->

  - [钱小豪](../Page/钱小豪.md "wikilink")、
  - [錢嘉樂](../Page/錢嘉樂.md "wikilink")、
  - [李蕙敏](../Page/李蕙敏.md "wikilink")（香港著名歌星）
  - [叶世雄](../Page/叶世雄.md "wikilink")：[香港电台编导](../Page/香港电台.md "wikilink")，负责香港电台的广播剧如《开心family》（1995年广播剧），《不回家吃饭的人》（大约是1995年广播剧），此外，叶世雄也是香港电台第五台节目《戏曲天地》的制作人。（参考：https://www.art-mate.net/doc/8895?name=%E8%91%89%E4%B8%96%E9%9B%84）
  - [劉聖雪](../Page/劉聖雪.md "wikilink")（[新民黨成員](../Page/新民黨_\(香港\).md "wikilink")）

## 参考文献

## 外部链接

  - [佛山市三水区人民政府](http://www.ss.gov.cn/)

[三水区](../Category/三水区.md "wikilink")
[Category:佛山市辖区](../Category/佛山市辖区.md "wikilink")

1.  中华民国《1947年广东政区及邮政式广东地名一览》，大部分根据粤语标准音广州话拼写，其他部分由潮汕、客家、官话或其他方音拼写而成。
2.  [城市人口](http://www.ss.gov.cn/main/zjgy/qqjs/csrk/)
3.  [民族宗教](http://www.ss.gov.cn/main/zjgy/qqjs/mzzj/)
4.  [教育事业](http://www.ss.gov.cn/main/zjgy/qqjs/jysy/)