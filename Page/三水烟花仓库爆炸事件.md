**三水烟花倉庫爆炸事件**是指发生在2008年2月14日[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[佛山市](../Page/佛山市.md "wikilink")[三水区](../Page/三水区.md "wikilink")[彭坑村](../Page/彭坑村.md "wikilink")[佛山粤通仓储运输有限公司下属](../Page/佛山粤通仓储运输有限公司.md "wikilink")[仓库所储藏的](../Page/仓库.md "wikilink")[烟花](../Page/烟花.md "wikilink")[爆竹发生猛烈爆炸](../Page/爆竹.md "wikilink")，事故造成两人受伤，爆炸产生的[冲击波遍及方圆](../Page/冲击波.md "wikilink")4公里。

## 事发经过

2008年2月14日凌晨3時25分（[北京时间](../Page/UTC+8.md "wikilink")），位于[广东省](../Page/广东省.md "wikilink")[佛山市](../Page/佛山市.md "wikilink")[三水区西南街道彭坑村佛山粤通仓储运输有限公司下属烟花爆竹仓库](../Page/三水区.md "wikilink")7号仓库发生爆炸，随即引燃其余19个仓库内储藏的15000箱烟花爆炸，所有20个仓库被炸毁\[1\]。
[sanshuiaccident.jpg](https://zh.wikipedia.org/wiki/File:sanshuiaccident.jpg "fig:sanshuiaccident.jpg")
爆炸范围两公里内村庄所有楼房窗户皆被震毁，[江根](../Page/江根.md "wikilink")、[洲边](../Page/洲边.md "wikilink")、[五顶岗和](../Page/五顶岗.md "wikilink")[南岸镇合共约](../Page/南岸镇.md "wikilink")4000楼房受到不同程度受损。部分仓库碎片被弹出数公里外，1公里外的[广肇高速公路一盏路灯被一条](../Page/广肇高速公路.md "wikilink")8米钢梁砸毁，并造成路面不同程度损坏。附近多个农场家禽共有18222只被震死，其中[鸡](../Page/鸡.md "wikilink")7840只、[鹅](../Page/鹅.md "wikilink")418只、[鸭](../Page/鸭.md "wikilink")9964只、[猪](../Page/猪.md "wikilink")85头；损坏禽畜窝棚13453平方米；农作物毁坏133亩\[2\]。[佛山市及](../Page/佛山市.md "wikilink")[广州市部分地区有明显震感](../Page/广州市.md "wikilink")，[广东省地震局](../Page/广东省地震局.md "wikilink")2008年2月15日称，爆炸产生的震动相当于[里氏](../Page/里氏.md "wikilink")1.1级。官方消息称，事故共造成一人被玻璃割伤，两名儿童受惊吓送院治疗，多名村民声称[脑震荡](../Page/脑震荡.md "wikilink")，附近160名村民疏散。

## 扑救

事发后，有关当局调派[公安](../Page/公安.md "wikilink")[消防合共](../Page/消防.md "wikilink")650人前往救灾，利用[坦克改装的](../Page/坦克.md "wikilink")[消防坦克首次出动](../Page/消防坦克.md "wikilink")\[3\]
。靠近事发现场的三水二桥随后被封锁检查，最后认定并无损毁。同时环保监测部门检测周围水源，最后判定并无受到污染。2008年2月14日下午2时，爆炸的火花引发两个山头大火，随后被扑灭。有关当局为保障消防救护人员安全，并不调派消防员靠近现场救火，2008年2月15日上午8时，现场大火熄灭。

## 损失

这次事故所在的佛山粤通仓储运输有限公司下属[仓库占地](../Page/仓库.md "wikilink")173亩，承担全国爆竹出口量60%以上的装运业务，所储藏的15000箱烟花来自[湖南](../Page/湖南.md "wikilink")、[江西等地](../Page/江西.md "wikilink")，准备通过[三水港中转出口至](../Page/三水港.md "wikilink")[欧洲和](../Page/欧洲.md "wikilink")[美国等地](../Page/美国.md "wikilink")。事发后导致三水口岸关停不能履行外贸合同和按时交付货物，数千家爆竹企业损失近两亿美元。2008年2月15日，国内唯一烟花爆竹[上市公司浏阳花炮股价大幅受挫](../Page/上市公司.md "wikilink")，开盘价17.2元[人民幣](../Page/人民幣.md "wikilink")，跌至收盘价16.49元[人民幣](../Page/人民幣.md "wikilink")，跌幅达4.68%\[4\]。2008年2月17日，粤通仓储运输有限公司拨款300万元人民币用于赔偿因爆炸影响的部分村民。\[5\]

## 事后调查

初步调查，粤通仓储运输有限公司仓库外墙并没有严重缺陷，从现场残留下的最大规格为12号礼花弹外壳和成的爆炸威力分析，该公司存在使用部分C级仓库违规超量储存A级产品的现象\[6\]。该仓库在2005年8月7日被三水区政府安全生产委员会作为重大危险源且列入关停名单，而相反的该仓库却通过了2005年广东省法定评估机构的安全评估，确定为非重大危险源而并未关闭。2006年9月22日，三水区政府决定暂停三水港的烟花爆竹运输作业，并停止发放烟花爆竹的运输证。同年11月1日，三水口岸的烟花鞭炮出口业务完全正式停止，而2007年2月25日恢复对烟花爆竹的运输，并且[佛山市政府与](../Page/佛山市.md "wikilink")[长沙市政府](../Page/长沙市.md "wikilink")、[株洲市政府签订安全协议](../Page/株洲市.md "wikilink")\[7\]。

## 参考资料

<div class="references-small">

<references />

</div>

## 外部链接

  - [金羊网专题](https://web.archive.org/web/20080217154716/http://www.ycwb.com/special/node_5110.htm)
  - [佛山三水烟花仓库爆炸：未致人伤亡
    原因未明·南方新闻网](http://www.southcn.com/today/hotpicnews/content/2008-02/15/content_4328011.htm)
  - [三水烟花仓库爆炸昨天8时结束](https://web.archive.org/web/20080302010131/http://www.gd.xinhuanet.com/newscenter/photo/2008-02/16/content_12465110.htm)

[Category:2008年2月](../Category/2008年2月.md "wikilink")
[Category:2008年广东](../Category/2008年广东.md "wikilink")
[Category:2008年中国灾难](../Category/2008年中国灾难.md "wikilink")
[Category:2008年爆炸案](../Category/2008年爆炸案.md "wikilink")
[Category:中华人民共和国广东省事故](../Category/中华人民共和国广东省事故.md "wikilink")
[Category:佛山历史](../Category/佛山历史.md "wikilink")
[Category:三水区](../Category/三水区.md "wikilink")
[Category:中国重大化学爆炸事故](../Category/中国重大化学爆炸事故.md "wikilink")

1.  [广东佛山烟花仓库爆炸追踪,20个仓库尽数炸毁](http://www.news365.com.cn/gdxww/758324.htm)
2.  [三水烟花仓库爆炸4千房屋受损1.8万禽畜死](http://www.jbzyw.com/cms/html/1/2/20080219/32301.html)

3.  [三水烟花仓库连环爆炸
    首辆消防坦克上阵](http://news.dg.soufun.com/2008-02-15/1513559.htm)
4.  [广东三水烟花仓库爆炸使行业损失15亿](http://www.xici.net/b920638/d65569860.htm)
5.  [香港商报：广东三水烟花仓库爆炸肇事公司已预支补偿款三百万元](http://www.gd.chinanews.com.cn/2008/2008-02-28/8/63133.shtml)
6.  [国务院通报广东三水烟花仓库爆炸事故
    新华网](http://news.xinhuanet.com/video/2008-02/17/content_7617825.htm)
7.  [广东三水烟花仓库爆炸
    百余村民房屋被震裂](http://news.zj.com/china/detail/2008-02-15/928485.html)