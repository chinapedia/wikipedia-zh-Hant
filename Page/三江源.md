**三江源**地处[青藏高原的](../Page/青藏高原.md "wikilink")[青海省](../Page/青海.md "wikilink")，因属[长江](../Page/长江.md "wikilink")、[黄河](../Page/黄河.md "wikilink")、[澜沧江](../Page/澜沧江.md "wikilink")（[湄公河](../Page/湄公河.md "wikilink")）三大[水系发源地而得名](../Page/水系.md "wikilink")。该地区平均海拔4000米以上；区域范围涉及16个县和1个乡；总面积36.3万平方公里，约占青海全省面积的50.4%。2000年5月青海省成立“[三江源自然保护区](../Page/三江源自然保护区.md "wikilink")”，2003年1月[国务院批准为](../Page/中华人民共和国国务院.md "wikilink")[国家级自然保护区](../Page/国家级自然保护区.md "wikilink")。2016年，该地成为[中华人民共和国首个国家公园](../Page/中华人民共和国国家公园.md "wikilink")。

## 地域范围

依照行政区划划分，地域范围为[玉树藏族自治州和](../Page/玉树藏族自治州.md "wikilink")[果洛藏族自治州全境](../Page/果洛藏族自治州.md "wikilink")，所属[海南藏族自治州的](../Page/海南藏族自治州.md "wikilink")2县、所属[黄南藏族自治州的](../Page/黄南藏族自治州.md "wikilink")2县和所属[海西蒙古族藏族自治州的](../Page/海西蒙古族藏族自治州.md "wikilink")[格尔木市](../Page/格尔木市.md "wikilink")[唐古拉山乡](../Page/唐古拉山乡.md "wikilink")。

  - [玉树藏族自治州](../Page/玉树.md "wikilink")1市5县：
      - [玉树市](../Page/玉树市.md "wikilink")
      - [杂多县](../Page/杂多县.md "wikilink")
      - [称多县](../Page/称多县.md "wikilink")
      - [治多县](../Page/治多县.md "wikilink")
      - [囊谦县](../Page/囊谦县.md "wikilink")
      - [曲麻莱县](../Page/曲麻莱县.md "wikilink")
  - [果洛藏族自治州](../Page/果洛藏族自治州.md "wikilink")6个县：
      - [玛沁县](../Page/玛沁县.md "wikilink")
      - [班玛县](../Page/班玛县.md "wikilink")
      - [甘德县](../Page/甘德县.md "wikilink")
      - [达日县](../Page/达日县.md "wikilink")
      - [久治县](../Page/久治县.md "wikilink")
      - [玛多县](../Page/玛多县.md "wikilink")
  - [海南藏族自治州](../Page/海南藏族自治州.md "wikilink")2个县：
      - [兴海县](../Page/兴海县.md "wikilink")
      - [同德县](../Page/同德县.md "wikilink")
  - [黄南藏族自治州](../Page/黄南藏族自治州.md "wikilink")2个县：
      - [泽库县](../Page/泽库县.md "wikilink")
      - [河南蒙古族自治县](../Page/河南蒙古族自治县.md "wikilink")
  - [海西蒙古族藏族自治州的](../Page/海西蒙古族藏族自治州.md "wikilink")[格尔木市所属](../Page/格尔木市.md "wikilink")[唐古拉山乡](../Page/唐古拉山乡.md "wikilink")

## 地理

三江源地区，地处青藏高原腹地，区内或临近[可可西里](../Page/可可西里自然保护区.md "wikilink")、[阿尔金山和](../Page/阿尔金山自然保护区.md "wikilink")[羌塘自然保护区](../Page/羌塘自然保护区.md "wikilink")。地理上，北有[昆仑山脉](../Page/昆仑山脉.md "wikilink")，西为[可可西里](../Page/可可西里.md "wikilink")，东有[巴颜喀拉山脉](../Page/巴颜喀拉山脉.md "wikilink")，南为[唐古拉山脉](../Page/唐古拉山脉.md "wikilink")；境内有2000余座山峰，多为[雪山和](../Page/雪山.md "wikilink")[冰川](../Page/冰川.md "wikilink")。冰川总面积超过5000平方公里，储水量约4000亿立方米；湖泊总面积超过5000平方公里，0.5平方公里以上的湖泊188个。境内分布[高原](../Page/高原.md "wikilink")[湿地](../Page/湿地.md "wikilink")、高原[沼泽和](../Page/沼泽.md "wikilink")[湖泊](../Page/湖泊.md "wikilink")、高原[草甸以及种类繁多的野生](../Page/草甸.md "wikilink")[动物和](../Page/动物.md "wikilink")[植物资源](../Page/植物.md "wikilink")。

## 人口

三江源地区人口约59万人（2002年），以牧业人口为主，为40.89万人；民族以[藏族为主](../Page/藏族.md "wikilink")，约占90％左右，其他民族为[汉族](../Page/汉族.md "wikilink")、[回族](../Page/回族.md "wikilink")、[撒拉族](../Page/撒拉族.md "wikilink")、[土族和](../Page/土族.md "wikilink")[蒙古族等民族](../Page/蒙古族.md "wikilink")。

## 动物资源

动物以兽类、鸟类多，两栖类和爬行类物种组成简单，种群数量相对较小。

  - [兽类](../Page/兽类.md "wikilink")，共有85种兽类，重要种群有[白唇鹿](../Page/白唇鹿.md "wikilink")、[马鹿](../Page/加拿大马鹿.md "wikilink")、[马麝](../Page/马麝.md "wikilink")、[岩羊](../Page/岩羊.md "wikilink")、[猕猴](../Page/猕猴.md "wikilink")、[毛冠鹿](../Page/毛冠鹿.md "wikilink")、[豹](../Page/豹.md "wikilink")、[小熊猫](../Page/小熊猫.md "wikilink")、[野猪](../Page/野猪.md "wikilink")、[豺](../Page/豺.md "wikilink")、[狼](../Page/狼.md "wikilink")、[黄鼬等](../Page/黄鼬.md "wikilink")；
  - [鸟类](../Page/鸟类.md "wikilink")，16目41科237种，其中珍稀物种有[白马鸡](../Page/白马鸡.md "wikilink")、[蓝马鸡](../Page/蓝马鸡.md "wikilink")、[血雉](../Page/血雉.md "wikilink")、[雉鹑](../Page/雉鹑.md "wikilink")、[斑尾臻鸡](../Page/斑尾臻鸡.md "wikilink")、[绿尾虹雉](../Page/绿尾虹雉.md "wikilink")、[高原山鹑](../Page/高原山鹑.md "wikilink")、[环颈雉](../Page/环颈雉.md "wikilink")、[藏雪鸡](../Page/藏雪鸡.md "wikilink")、[岩鸽](../Page/岩鸽.md "wikilink")、[红隼](../Page/红隼.md "wikilink")、[鸳](../Page/鸳.md "wikilink")、[胡兀鹫等](../Page/胡兀鹫.md "wikilink")；
  - [两栖类](../Page/两栖类.md "wikilink")7种，珍稀物种有[西藏蟾蜍](../Page/西藏蟾蜍.md "wikilink")、[大蟾蜍](../Page/大蟾蜍.md "wikilink")、[花背蟾蜍](../Page/花背蟾蜍.md "wikilink")、[中国林蛙](../Page/中国林蛙.md "wikilink")、[西藏山溪鲵等](../Page/西藏山溪鲵.md "wikilink")；
  - [爬行类](../Page/爬行类.md "wikilink")8种，主要为[青海沙蜥](../Page/青海沙蜥.md "wikilink")、[秦岭滑蜥](../Page/秦岭滑蜥.md "wikilink")、[枕纹锦蛇和](../Page/枕纹锦蛇.md "wikilink")[蝮蛇等](../Page/蝮蛇.md "wikilink")。

## 植物资源

植物种类以寒温性的针叶林为主，包括针叶林、阔叶林、针阔混交林、灌丛、草甸、草原、沼泽及水生植物、垫状和稀疏植物九个类型；以北温带植物群落为主。区内有油麦吊云杉、红花绿绒蒿、[虫草](../Page/虫草.md "wikilink")3种国家二级保护植物、列入[国际贸易公约附录](../Page/国际贸易公约.md "wikilink")Ⅱ的兰科植物31种、青海省级重点保护植物34种。

  - [森林树类](../Page/森林.md "wikilink")，有川西云杉、紫果云杉、红杉、祁连圆柏、大果圆柏、塔枝圆柏、密枝圆柏、白桦、红桦和糙皮桦等；
  - 灌丛植被，有杜鹃、山柳、沙棘、金露梅、锦鸡儿、锈线菊和水荀子等。

## 旅游

  - [鄂陵湖](../Page/鄂陵湖.md "wikilink")（淡水湖，果洛州）
  - [托索湖](../Page/托索湖.md "wikilink")（果洛州）
  - [星宿海](../Page/星宿海.md "wikilink")（玉树州）
  - [扎陵湖](../Page/扎陵湖.md "wikilink")（淡水湖，果洛州）

<!-- end list -->

  - [莫格德哇遗址](../Page/莫格德哇遗址.md "wikilink")（果洛州）
  - [格萨尔王狮龙宫殿](../Page/格萨尔王狮龙宫殿.md "wikilink")（果洛州）
  - [勒巴沟岩画](../Page/勒巴沟岩画.md "wikilink")（玉树州）
  - [玛玉文化中心](../Page/玛玉文化中心.md "wikilink")（果洛州）
  - [牛头碑](../Page/牛头碑.md "wikilink")（果洛州）
  - [新寨嘛呢城](../Page/新寨嘛呢城.md "wikilink")（玉树州，博物馆）
  - [勒巴沟嘛呢山](../Page/勒巴沟嘛呢山.md "wikilink")（玉树州）
  - [唐蕃古道](../Page/唐蕃古道.md "wikilink")
  - [文成公主庙](../Page/文成公主庙.md "wikilink")（玉树州，[禅古寺](../Page/禅古寺.md "wikilink")）

<!-- end list -->

  - [白玉寺](../Page/白玉寺.md "wikilink")（果洛州）
  - [查朗寺](../Page/查朗寺.md "wikilink")（果洛州）
  - [多卡寺](../Page/多卡寺.md "wikilink")（果洛州）
  - [结古寺](../Page/结古寺.md "wikilink")（玉树州）
  - [拉加寺](../Page/拉加寺.md "wikilink")（果洛州）

<!-- end list -->

  - [可可西里自然保护区](../Page/可可西里自然保护区.md "wikilink")
  - [坎布拉国家森林公园](../Page/坎布拉国家森林公园.md "wikilink")
  - [隆宝滩自然保护区](../Page/隆宝滩自然保护区.md "wikilink")（玉树州）
  - [仁玉原始森林](../Page/仁玉原始森林.md "wikilink")（果洛州）
  - [玛可河原始森林](../Page/玛可河原始森林.md "wikilink")（果洛州）

<!-- end list -->

  - [巴颜喀拉山](../Page/巴颜喀拉山.md "wikilink")（玉树州）

<!-- end list -->

  - [阿尼玛卿峰](../Page/阿尼玛卿峰.md "wikilink")（雪山，海拔6282米，果洛州）
  - [年保玉则峰](../Page/年保玉则峰.md "wikilink")（果洛州）

### 探险

  - [长江源](../Page/长江源.md "wikilink")
  - [黄河源](../Page/黄河源.md "wikilink")
  - [澜沧江源](../Page/扎曲_\(澜沧江\).md "wikilink")
  - [可可西里无人区](../Page/可可西里无人区.md "wikilink")

## 参考文献

### 引用

### 来源

  - 主要依据 [青海新闻网](http://www.qhnews.com) 有关资料整理

{{-}}

[三江源](../Category/三江源.md "wikilink")
[Category:中华人民共和国国家公园](../Category/中华人民共和国国家公园.md "wikilink")
[Category:青海地理](../Category/青海地理.md "wikilink")
[Category:青海旅游景点](../Category/青海旅游景点.md "wikilink")