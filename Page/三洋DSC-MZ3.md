**三洋
DSC-MZ3**乃[三洋](../Page/三洋.md "wikilink")（[Sanyo](../Page/Sanyo.md "wikilink")）公司於2002年推出的數碼相機。

## 規格

  - [像素](../Page/像素.md "wikilink")：211萬，[有效像素](../Page/有效像素.md "wikilink")195萬
  - 感光元件：[CCD](../Page/CCD.md "wikilink")（1/1.8[寸](../Page/寸.md "wikilink")）
  - [LCD螢幕](../Page/LCD.md "wikilink")：1.5[寸](../Page/寸.md "wikilink")，11萬[像素](../Page/像素.md "wikilink")
  - 記錄媒體：[Compact Flash](../Page/CF卡.md "wikilink") Type I/II（包括[Micro
    Drive](../Page/Micro_Drive.md "wikilink")）
  - [拍攝模式](../Page/拍攝.md "wikilink")：圖像，連拍，影像
  - 記錄格式（圖像）：[JPEG](../Page/JPEG.md "wikilink")（True Color v1.1 壓縮率Super
    Fine, Fine, Normal）, [TIFF](../Page/TIFF.md "wikilink")（非壓縮）
  - 記錄格式（影像）：影像[Quick Time](../Page/Quick_Time.md "wikilink")
    Movie（[Photo-JPEG](../Page/Photo-JPEG.md "wikilink")）聲音[WAVE](../Page/WAVE.md "wikilink")
  - 記錄檔案名稱：SANY\#\#\#\#
  - [Exif版本](../Page/Exif.md "wikilink")：2.2
  - 支援[解析度](../Page/解析度.md "wikilink")（圖像）：2000x1496（軟件支援）, 1600x1200,
    640x480
  - 支援[解析度](../Page/解析度.md "wikilink")（影像）：640x480, 320x240, 160x120
  - [光圈大小](../Page/光圈.md "wikilink")：F2.7-F4.9
  - [焦距](../Page/焦距.md "wikilink")：7.7mm-23.1mm（等效[焦距](../Page/焦距.md "wikilink")37mm-111mm）
  - [對焦範圍](../Page/對焦範圍.md "wikilink")：標準50cm-無限，微距10cm-50cm
  - [快門速度](../Page/快門.md "wikilink")：16秒-1/2500秒，連拍1/10秒-1/10000秒，影像1/15秒-1/10000秒
  - [測光模式](../Page/測光.md "wikilink")：多重分割，中央重點，點測光
  - [ISO感光度](../Page/ISO.md "wikilink")：自動，100, 200, 400
  - [白平衡模式](../Page/白平衡.md "wikilink")：自動，晴天，陰天，鎢絲燈，螢光燈，自訂
  - [曝光補償](../Page/曝光補償.md "wikilink")：±1.8EV（每0.3EV逐級調整）
  - [閃燈模式](../Page/閃燈.md "wikilink")：自動，防紅眼，強制閃光，禁止閃光
  - [變焦](../Page/變焦.md "wikilink")：3x光學，5x數碼
  - [連接埠](../Page/連接埠.md "wikilink")：[USB
    1.1](../Page/USB_1.1.md "wikilink"),
    [NTSC制式](../Page/NTSC制式.md "wikilink")
  - 使用[電池](../Page/電池.md "wikilink")：DB-L10
  - 尺寸大小：長99mm x 高55mm x 厚32.5mm
  - [重量](../Page/重量.md "wikilink")：210[克](../Page/克.md "wikilink")
  - 運作環境：[溫度](../Page/溫度.md "wikilink")0℃-40℃，[濕度](../Page/濕度.md "wikilink")30%-90%

## 參见

  - [三洋數碼相機網站](http://www.sanyo-dsc.com/)
  - [DSC-MZ3官方網站](https://web.archive.org/web/20051225104209/http://www.sanyo-dsc.com/support/archive/series/mz3/index.html)
  - [DSC-MZ3圖像樣本](https://web.archive.org/web/20051123223737/http://www.sanyo-dsc.com/products/lineup/sample/mz3.html)

[Category:数码照相机](../Category/数码照相机.md "wikilink")