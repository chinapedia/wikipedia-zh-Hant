**三清山**位于中国[江西省](../Page/江西省.md "wikilink")[上饶市](../Page/上饶市.md "wikilink")[玉山县与](../Page/玉山县.md "wikilink")[德兴市交界处](../Page/德兴市.md "wikilink")，距玉山县城50公里，距上饶市78公里。三清山为[怀玉山脉主峰](../Page/怀玉山脉.md "wikilink")，因**玉京、玉虚、玉华**“三峰峻拔、如三清列坐其巅”而得其名，三峰中以**玉京峰**为最高，海拔1816.9米，是江西第五高峰，也是[信江的源头之一](../Page/信江.md "wikilink")\[1\]。

三清山风景秀美，同时又是[道教名山](../Page/道教.md "wikilink")。1988年被列为[国家重点风景名胜区](../Page/国家重点风景名胜区.md "wikilink")，2005年被列为[国家地质公园](../Page/中国国家地质公园.md "wikilink")，2008年被列为[世界自然遗产](../Page/世界自然遗产.md "wikilink")，2011年被列[国家AAAAA级旅游区](../Page/中国国家5A级旅游景区.md "wikilink")，2012年被列为[世界地质公园](../Page/世界地质公园.md "wikilink")。

## 道教名山

[SanQingShan6.jpg](https://zh.wikipedia.org/wiki/File:SanQingShan6.jpg "fig:SanQingShan6.jpg")
三清山为历代道家修炼场所，自[晋朝](../Page/晋朝.md "wikilink")[葛云](../Page/葛云.md "wikilink")、[葛洪来山以后](../Page/葛洪.md "wikilink")，便渐为信奉道学的名家所向往。

最先在三清山修建道观的为[唐朝](../Page/唐朝.md "wikilink")[信州太守](../Page/信州_\(唐朝行政区划\).md "wikilink")[王鉴的后裔](../Page/王鉴.md "wikilink")。[唐僖宗时](../Page/唐僖宗.md "wikilink")（873－888年）信州太守王鉴奉旨抚民，到达三清山北麓，见到此山风光秀丽，景色清幽，卸任后即携家归隐在此。到[宋朝时](../Page/宋朝.md "wikilink")，王鉴后裔[王霖捐资兴建道观](../Page/王霖.md "wikilink")，三清山成为道家[洞天福地](../Page/洞天福地.md "wikilink")。

至[明](../Page/明.md "wikilink")[景泰年间](../Page/景泰.md "wikilink")（1450－1456年），王霖后裔[王祜又重建](../Page/王祜.md "wikilink")**三清宫**。从登山处步云桥直至天门三清福地，共兴建宫观、亭阁、石刻、石雕、山门、桥梁等200余处，使道教建筑遍布全山，其规模与气势，可与[青城山](../Page/青城山.md "wikilink")、[武当山](../Page/武当山.md "wikilink")、[龙虎山媲美](../Page/龙虎山.md "wikilink")。因此，三清山有“露天道教博物馆”之称。

2013年，[三清山古建筑群被列为第七批全国重点文物保护单位](../Page/三清山古建筑群.md "wikilink")。

## 生物宝库

[SanQingShan9.jpg](https://zh.wikipedia.org/wiki/File:SanQingShan9.jpg "fig:SanQingShan9.jpg")
三清山是野生动植物的宝库。

植被以[常绿阔叶林](../Page/常绿阔叶林.md "wikilink")、常绿-落叶混交林、[山地矮曲林](../Page/山地矮曲林.md "wikilink")、[针叶林](../Page/针叶林.md "wikilink")、竹林等为主。[被子植物共有](../Page/被子植物.md "wikilink")148科691属1602种(包括变种、变型)，其中[双子叶植物](../Page/双子叶植物.md "wikilink")1300种544属122科，[单子叶植物](../Page/单子叶植物.md "wikilink")302种147属26科\[2\]。[蕨类植物共有](../Page/蕨类植物.md "wikilink")28科52属86种\[3\]。有国家保护植物33种，隶属30属22科，其中蕨类植物1科1属1种，[裸子植物](../Page/裸子植物.md "wikilink")6科12属12种；被子植物15科17属20种\[4\]。

野生[动物有](../Page/动物.md "wikilink")800多种。

## 地质公园

三清山位于[扬子地块和](../Page/扬子地块.md "wikilink")[华夏地块结合带](../Page/华夏地块.md "wikilink")，已知有9亿多年的[地质构造演化发展历史](../Page/地质构造.md "wikilink")。三清山花岗岩出露面积约98平方千米，可划分为3期：第1期为中粗粒黑云母碱[长花岗岩](../Page/长花岗岩.md "wikilink")；第2期为中粒黑云母碱长花岗岩；第3期为细粒黑云母碱长花岗岩，其中以第2期的规模最大，最重要的景点即分布于该期花岗岩内\[5\]。花岗岩受北东、北西和北东东向三条断裂控制，形成典型的三角形断块山，地貌处于幼年晚期至壮年早期发育阶段\[6\]。

三清山是世界花岗岩地质、花岗岩地貌、花岗岩生态完美结合的一个突出代表，是研究[西太平洋地区](../Page/西太平洋地区.md "wikilink")[中生代](../Page/中生代.md "wikilink")[陆内俯冲型花岗岩的最好园地](../Page/陆内俯冲型花岗岩.md "wikilink")。三清山花岗岩微地貌类型多样，以锥状峰峦与密集峰柱组合型花岗岩峰林为特征的地貌景观被称“三清山式”花岗岩景观。这一景观记录和保存了地球中新生代以来[地壳形成演化的历史](../Page/地壳.md "wikilink")。\[7\]

三清山与[黄山](../Page/黄山.md "wikilink")、[华山](../Page/华山.md "wikilink")、[泰山](../Page/泰山.md "wikilink")、[普陀山和](../Page/普陀山.md "wikilink")[克什克腾是中国最具代表性的花岗岩景观](../Page/克什克腾.md "wikilink")，其中的“三清山式”花岗岩景观，是具有世界对比意义的花岗岩景观型式之一。\[8\]

三清山和黄山地域相近，同为花岗岩地貌，但又有其独特之处：首先是地质结构不同，黄山古时地处[华夏板块中间](../Page/华夏板块.md "wikilink")，没有经过大的地壳运动；但是三清山处在板块的碰撞带上，经历了大的地壳运动。因此三清山周边伴生着许多矿，比如西侧的[德兴铜矿](../Page/德兴铜矿.md "wikilink")。其次三清山微地貌景观发育比黄山丰富。另外黄山经过了第四纪冰川，而三清山没有经过，属于第四纪冰川的避难所，所以三清山的山峰很尖，且植物没有被破坏。\[9\]

三清山因其独特的[花岗岩地貌于](../Page/花岗岩地貌.md "wikilink")2005年9月被[国土资源部列为](../Page/国土资源部.md "wikilink")[国家地质公园](../Page/中国国家地质公园.md "wikilink")\[10\]，2012年9月21日更被[联合国教科文组织列为](../Page/联合国教科文组织.md "wikilink")[世界地质公园](../Page/世界地质公园.md "wikilink")\[11\]。

## 世界遗产

2008年7月8日，第32届[世界遗产大会将三清山列入](../Page/世界遗产大会.md "wikilink")《[世界遗产名录](../Page/世界遗产.md "wikilink")》，三清山成为中国第十一个[世界自然遗产](../Page/世界自然遗产.md "wikilink")。

世界遗产委员会对三清山的遗产价值给予了高度评价。会议认为：三清山在一个相对较小的区域内展示了独特[花岗岩石柱与山峰](../Page/花岗岩.md "wikilink")，它们栩栩如生。三清山丰富的花岗岩造型石与多种植被、远近变化的景观及震撼人心的气象奇观相结合，创造了独一无二的景观效果，呈现了引人入胜的自然美。

## 旅游胜地

三清山是著名的旅游胜地，1988年8月被列为第二批[国家重点风景名胜区](../Page/国家重点风景名胜区.md "wikilink")，2011年9月6日被列为[国家5A级旅游景区](../Page/国家5A级旅游景区.md "wikilink")\[12\]。

三清山景区总面积756.6[平方公里](../Page/平方公里.md "wikilink")，其中核心区面积229.5平方公里\[13\]。主要景区有南清园、万寿园、西海岸、阳光海岸、玉京峰、三清宫、西华台、三洞口、石鼓岭、玉灵观等，著名景点有司春女神、巨蟒出山、猴王献宝、玉女开怀、老道拜月、观音赏曲、葛洪献丹、神龙戏松、三龙出海、蒲牢鸣天等。

近年来，三清山旅游事业发展迅速。2003年游客仅逾10万人次，2008年游客131万人次，旅游综合收入12.9亿元\[14\]。2008年申遗成功后，更是实现了跨越式发展。2009年游客227.3万人次\[15\]，2010年游客297.5万人次\[16\]，2011年游客达460.8万人次，旅游总收入35.9亿元\[17\]。2012年游客611.3万人次，旅游总收入47.8亿元\[18\]。2013年上半年，风景区共接待境内外游客327.63万人次，同比增长17.8%。旅游综合收入22.02亿元，同比增长22.74%\[19\]。

现建有两个索道：南部的外双溪索道，东部的金沙索道。

目前山下的四[星级](../Page/星级.md "wikilink")[宾馆有](../Page/宾馆.md "wikilink")3个，三星级宾馆有4个，正在建设的五星级宾馆有3个。\[20\]

[File:SanQingShan1.jpg|景区大门](File:SanQingShan1.jpg%7C景区大门)
[File:SanQingShan2.jpg|三清山索道售票處](File:SanQingShan2.jpg%7C三清山索道售票處)
[File:SanQingShan6.jpg|三清宮](File:SanQingShan6.jpg%7C三清宮)
[File:SanQingShan7.jpg|巨蟒出山打霧](File:SanQingShan7.jpg%7C巨蟒出山打霧)
[File:SanQingShan8.jpg|巨蟒出山](File:SanQingShan8.jpg%7C巨蟒出山)
[File:SanQingShan9.jpg|三清山風景](File:SanQingShan9.jpg%7C三清山風景)
[File:SanQingShan11.jpg|透明台](File:SanQingShan11.jpg%7C透明台)
[File:SanQingShan13.jpg|三清山風景](File:SanQingShan13.jpg%7C三清山風景)
[File:SanQingShan3.jpg|地圖](File:SanQingShan3.jpg%7C地圖)
[File:SanQingShan4.jpg|南部的外双溪索道](File:SanQingShan4.jpg%7C南部的外双溪索道)
<File:SanQingShan5.jpg>|[黄山松](../Page/黄山松.md "wikilink")
[File:SanQingShan10.jpg|绝壁栈道](File:SanQingShan10.jpg%7C绝壁栈道)
[File:SanQingShan12.jpg|栈道](File:SanQingShan12.jpg%7C栈道)
[File:SanQingShan14.jpg|三清山風景](File:SanQingShan14.jpg%7C三清山風景)
[File:SanQingShan15.jpg|铁索桥](File:SanQingShan15.jpg%7C铁索桥)
[File:Sanqingshan1518.jpg|司春女神](File:Sanqingshan1518.jpg%7C司春女神)

## 參考文獻

## 外部链接

  - [三清山官方网站](http://www.sqs.gov.cn/)
  - [三清旅游网](https://web.archive.org/web/20060127194101/http://www.sanqing.com/)

[Category:中國國家地質公園](../Category/中國國家地質公園.md "wikilink")
[赣](../Category/中国世界遗产.md "wikilink")
[Category:花岗岩地貌](../Category/花岗岩地貌.md "wikilink")
[Category:江西山峰](../Category/江西山峰.md "wikilink")
[Category:上饶地理](../Category/上饶地理.md "wikilink")
[Category:上饶旅游](../Category/上饶旅游.md "wikilink")
[Category:玉山县](../Category/玉山县.md "wikilink")
[Category:国家5A级旅游景区](../Category/国家5A级旅游景区.md "wikilink")
[Category:世界地质公园](../Category/世界地质公园.md "wikilink")

1.

2.
3.

4.

5.

6.

7.

8.
9.  [风景区党委书记刘书宗畅谈三清山未来发展](http://www.sqs373.com/article/1648.html)

10.

11.

12.

13.
14. [三清山08年旅游门票收入过亿 综合收入12.9亿](http://srzc.com/news/ksr/0941714918.html)


15. [江西省人大检查组调研三清山考察旅游产业建设](http://www.jxcn.cn/1669/2010-7-16/30132@730245.htm)

16.

17.

18.

19.

20. [三清山管委会谢华杰：三清山旅游一路飘红](http://www.china.com.cn/travel/txt/2010-04/14/content_19813609.htm)