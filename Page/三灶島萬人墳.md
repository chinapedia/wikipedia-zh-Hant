**三灶島萬人墳**是一處位在廣東[珠海市三灶島的](../Page/珠海市.md "wikilink")[萬人塚](../Page/萬人塚.md "wikilink")，其佔地約4千平方米，為[廣東省文物保護單位之一](../Page/广东省文物保护单位.md "wikilink")。

## 歷史

1938年農曆[正月十七](../Page/正月十七.md "wikilink")，規模約六千餘人的[日軍部隊於今日](../Page/日本军.md "wikilink")[珠海市三灶島蓮塘灣附近](../Page/珠海市.md "wikilink")[登陸](../Page/登陸作戰.md "wikilink")，並於隨後在島上展開[屠殺行動](../Page/屠杀.md "wikilink")，許多當地民眾不幸在該行動中遇害或餓死。此外，日軍亦將許多徵自[朝鮮](../Page/朝鮮.md "wikilink")、[臺灣和](../Page/臺灣.md "wikilink")[橫琴興建機場之](../Page/橫琴.md "wikilink")[民工秘密殺害](../Page/民工.md "wikilink")。

1945年[日本投降後](../Page/日本投降.md "wikilink")，當地居民開始返回島上並將死難者之遺體投入[亂葬崗之中](../Page/萬人塚.md "wikilink")。

1948年，三灶島華僑，香港和澳門居民籌款修建完成三灶島萬人墳。

2000年代，為紀念[抗日戰爭结束](../Page/抗日戰爭.md "wikilink")60週年，珠海市當局計劃重新擴建三灶島萬人墳，並預定以[人民幣](../Page/人民幣.md "wikilink")800萬之資金建造**三灶島侵華日軍暴行及罪証陳列館**；該計劃預定將原萬人墳之佔地面積擴增至逾4萬平方米。\[1\]\[2\]\[3\]

## 參見

  - [三灶日本文字摩崖](../Page/三灶日本文字摩崖.md "wikilink")

## 參考

## 連結

  -
[Category:日军在第二次中日战争中的战争罪行](../Category/日军在第二次中日战争中的战争罪行.md "wikilink")
[Category:中国屠杀事件与各类血案](../Category/中国屠杀事件与各类血案.md "wikilink")
[Category:珠海抗日战争相关建筑](../Category/珠海抗日战争相关建筑.md "wikilink")
[Category:珠海墓葬](../Category/珠海墓葬.md "wikilink")
[Category:萬人塚](../Category/萬人塚.md "wikilink")

1.
2.  [三灶岛“万人坟”遗址——广东省文物保护单位](https://www.gdarts.com/gdich/heritage_detail_203.html)
3.  [珠海：三灶將建萬人墳紀念公園 -
    本館動態](http://www.sunyat-sen.org:1980/b5/sunyat-sen.org/zxdt/show.php?id=3159)