**三焦**是[中醫學範疇中](../Page/中醫學.md "wikilink")[六腑之一](../Page/六腑.md "wikilink")，又名「決瀆之官」，為[上焦](../Page/上焦.md "wikilink")、[中焦](../Page/中焦.md "wikilink")、[下焦三者的統稱](../Page/下焦.md "wikilink")；對應的臟為[心包](../Page/心包絡.md "wikilink")。「焦」古作「[膲](../Page/:wiktionary:膲.md "wikilink")」，為皮下、肌間紋理之意。過去英文意譯成「Three
warmer」、「Triple warmer」、「Triple
burner」，理由是與身體[代謝有關](../Page/代謝.md "wikilink")。

根據《[素問](../Page/素問.md "wikilink")·靈蘭秘典》：「三焦者、決瀆之官、水道出焉。」遍佈在人體[胸腔及](../Page/胸腔.md "wikilink")[腹腔](../Page/腹腔.md "wikilink")，是[血氣](../Page/血氣.md "wikilink")、[津液運行至](../Page/津液.md "wikilink")[五臟](../Page/五臟.md "wikilink")[六腑的途徑](../Page/六腑.md "wikilink")，故此，三焦與其他腑器不同，並無實體，明確位置亦有不同的說法及見解；亦有認為三焦對應解剖學的[內分泌系統](../Page/內分泌系統.md "wikilink")。然而，三焦與個別臟腑之間的關係極為密切，可以用來調整及輔助臟腑的機能，所以三焦亦可說是臟腑的「[外府](../Page/外府.md "wikilink")」與「[外衛](../Page/外衛.md "wikilink")」。

三焦的狀態各有不同，按《[靈樞](../Page/靈樞.md "wikilink")·營衛生會》：「上焦如霧，中焦如漚、下焦如瀆。」可見三焦狀態之異。

三焦通暢，則水液及氣機運行暢順無阻；相反便會引致氣化功能失調，影響各個臟腑間的週節機能，導致各個相關臟腑的病變。

三焦如同其他臟腑，具有相應的正經——[手少陽三焦經](../Page/手少陽三焦經.md "wikilink")。

## 位置

  - 上焦:位於[橫膈膜以上的部份](../Page/橫膈膜.md "wikilink")，包括[心](../Page/心_\(臟腑\).md "wikilink")、[肺](../Page/肺_\(臟腑\).md "wikilink")。所謂「上焦如霧」，指的是上焦的宣發功能，令血氣及津液如霧氣般散發全身。
    中焦:位於橫膈膜以下，肚臍以上的位置，包括[脾](../Page/脾_\(臟腑\).md "wikilink")、[胃](../Page/胃_\(臟腑\).md "wikilink")。當脾胃運化及腐熟食物時，水穀會被分解消化，如化為泡沫的過程，故稱「中焦如漚」，其中「漚」是指中焦的消化功能。
    下焦:位於[肚臍對下](../Page/肚臍.md "wikilink")，包括[肝](../Page/肝_\(臟腑\).md "wikilink")、[腎](../Page/腎_\(臟腑\).md "wikilink")、[小腸](../Page/小腸_\(臟腑\).md "wikilink")、[大腸及](../Page/大腸_\(臟腑\).md "wikilink")[膀胱](../Page/膀胱_\(臟腑\).md "wikilink")。「下焦如瀆」，指的是下焦排泄濁物的功能。

## 功用

[李時珍於](../Page/李時珍.md "wikilink")《[本草綱目](../Page/本草綱目.md "wikilink")》中曾說：「上焦主納，中焦主化，下焦主出」。可見三焦關係到水穀的整個消化過程，支配水穀的受納、吸收及排泄。三焦各部份的功用如下：

  - 上焦:根據《難經·三十一難》，上焦「主納而不出」，進食的食物經胃化生成精氣，通過脾轉化為精，再上輸至心肺，再把精氣散於全身。《素問·營衛生會》：「上焦如霧」，指的正是精氣如霧氣般瀰漫全身的情況。

<!-- end list -->

  - 中焦:

<!-- end list -->

  - 下焦:

## 相關病症

由於三焦負責提供通道，三焦的疾病會引致體內的水液循環出現阻礙，引致水腫或[小便不利](../Page/小便.md "wikilink")。

## 現代醫學

最新發現的間質器官 (Interstitium)的功能與中醫學的三焦類似

## 參看

廖育群：《重構秦漢醫學圖像》（上海：上海交通大學出版社，2012）。

[Structure and Distribution of an Unrecognized Interstitium in Human
Tissues](https://www.nature.com/articles/s41598-018-23062-6)

[S](../Category/臟象學說.md "wikilink")