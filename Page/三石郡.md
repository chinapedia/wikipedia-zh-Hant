日高郡|日高郡 (北海道){{\!}}日高郡\]\] | 現在郡=日高郡_(北海道){{\!}}日高郡 | 都道府縣=北海道 |
支廳=日高支廳 | 町=1 | 村= | 經度= | 緯度= |
地圖=[Hokkaido_Mitsuishi-gun.png](https://zh.wikipedia.org/wiki/File:Hokkaido_Mitsuishi-gun.png "fig:Hokkaido_Mitsuishi-gun.png")
}}
**三石郡**（）為過去位於[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[日高支廳中部的郡](../Page/日高支廳.md "wikilink")，已於2006年3月31日因轄下唯一的[三石町與](../Page/三石町.md "wikilink")[靜內郡](../Page/靜內郡.md "wikilink")[靜內町合併而廢除](../Page/靜內町.md "wikilink")。

## 轄區

轄區包含1町：

  - [三石町](../Page/三石町.md "wikilink")

## 沿革

  - 1869年8月15日：北海道設置11國86郡，[日高國三石郡成立](../Page/日高國.md "wikilink")。
  - 1897年11月：北海道實施支廳制，三石郡隸屬浦河支廳之管轄，此時三石郡下轄姨布村、邊訪村、幌毛村、本桐村、鳧舞村、歌笛村。\[1\]（6村）
  - 1906年4月1日：姨布村、邊訪村、幌毛村、本桐村、鳧舞村、歌笛村合併為三石村，並成為北海道二級村。（1村）
  - 1938年4月1日：三石村改為北海道一級村。
  - 1943年6月1日：北海道一級二級町村制廢止。
  - 1951年4月1日：三石村改制為[三石町](../Page/三石町.md "wikilink")。
  - 2006年3月31日：三石町和[靜內郡](../Page/靜內郡.md "wikilink")[靜內町](../Page/靜內町.md "wikilink")[合併為新成立的](../Page/市町村合併.md "wikilink")[新日高町](../Page/新日高町.md "wikilink")，並隸屬新設立的[日高郡](../Page/日高郡_\(北海道\).md "wikilink")。三石郡因已無管轄之町村而同時廢除。

[Urakawa-shicho.png](https://zh.wikipedia.org/wiki/File:Urakawa-shicho.png "fig:Urakawa-shicho.png")

## 參考資料

[Category:日高國](../Category/日高國.md "wikilink")

1.