**三立都會台**，是[三立電視旗下的頻道之一](../Page/三立電視.md "wikilink")，以年輕觀眾為目標收視群，1995年9月1日開播\[1\]。1996年12月4日由「三立CITY都會台」改名為三立都會台\[2\]。2014年11月1日，啟用高畫質版本「**三立都會台HD**」。

## 頻道簡介

  - 三立都會台於1995年9月1日開播，當時名為三立2台，最初定頻在第24頻道\[3\]。2005年1月，因[行政院新聞局規劃有線電視頻道](../Page/行政院新聞局.md "wikilink")，定頻在30頻道。頻道代表色塊為。
  - 2015年2月16日，三立都會台畫面比例全面更改為16:9。並陸續在各大數位有線電視系統升級為HD畫質。
  - 2015年8月1日，三立都會台正式以HD高畫質播出，並更換鏡面和標示HD字樣的台標。（類比訊號原先也有標示HD字樣，但於10月7日取消標示﹚
  - 2016年3月10日起，三立都會台正式於三立匯流影音平台『SET Vidol TV』上提供Live聯播服務和官方APP。
  - 2018年4月16日，三立都會台所有節目標誌全新改版播出。

## 頻道標誌變遷

  - 三立city台

## 節目

### 戲劇節目

#### 已播畢

  - 週日偶像劇

<!-- end list -->

  - [薰衣草](../Page/薰衣草_\(台灣偶像劇\).md "wikilink")
  - [MVP情人](../Page/MVP情人.md "wikilink")
  - [海豚灣戀人](../Page/海豚灣戀人.md "wikilink")
  - [西街少年](../Page/西街少年.md "wikilink")
  - [雪天使](../Page/雪天使.md "wikilink")
  - [紫禁之巔](../Page/紫禁之巔.md "wikilink")
  - [天國的嫁衣](../Page/天國的嫁衣.md "wikilink")
  - [格鬥天王](../Page/格鬥天王.md "wikilink")
  - [王子變青蛙](../Page/王子變青蛙.md "wikilink")
  - [綠光森林](../Page/綠光森林.md "wikilink")
  - [愛情魔髮師](../Page/愛情魔髮師.md "wikilink")
  - [微笑Pasta](../Page/微笑Pasta.md "wikilink")
  - [愛情經紀約](../Page/愛情經紀約.md "wikilink")
  - [放羊的星星](../Page/放羊的星星.md "wikilink")
  - [櫻野三加一](../Page/櫻野三加一.md "wikilink")
  - [鬥牛，要不要](../Page/鬥牛，要不要.md "wikilink")
  - [命中注定我愛你](../Page/命中注定我愛你_\(台灣電視劇\).md "wikilink")
  - [無敵珊寶妹](../Page/無敵珊寶妹.md "wikilink")
  - [敗犬女王](../Page/敗犬女王.md "wikilink")
  - [福氣又安康](../Page/福氣又安康.md "wikilink")
  - [下一站，幸福](../Page/下一站，幸福.md "wikilink")
  - [偷心大聖PS男](../Page/偷心大聖PS男.md "wikilink")
  - [鍾無艷](../Page/鍾無艷_\(2010年電視劇\).md "wikilink")
  - [國民英雄](../Page/國民英雄.md "wikilink")
  - [醉後決定愛上你](../Page/醉後決定愛上你.md "wikilink")
  - [小資女孩向前衝](../Page/小資女孩向前衝.md "wikilink")
  - [向前走向愛走](../Page/向前走向愛走.md "wikilink")
  - [螺絲小姐要出嫁](../Page/螺絲小姐要出嫁.md "wikilink")
  - [金大花的華麗冒險](../Page/金大花的華麗冒險.md "wikilink")
  - [真愛黑白配](../Page/真愛黑白配.md "wikilink")
  - [回到愛以前](../Page/回到愛以前.md "wikilink")
  - [愛上兩個我](../Page/愛上兩個我.md "wikilink")
  - [再說一次我願意](../Page/再說一次我願意_\(電視劇\).md "wikilink")
  - [聽見幸福](../Page/聽見幸福.md "wikilink")
  - [他看她的第2眼](../Page/他看她的第2眼.md "wikilink")
  - [愛上哥們](../Page/愛上哥們.md "wikilink")
  - [後菜鳥的燦爛時代](../Page/後菜鳥的燦爛時代.md "wikilink")
  - [狼王子](../Page/狼王子.md "wikilink")
  - [浮士德的微笑](../Page/浮士德的微笑.md "wikilink")
  - [我的愛情不平凡](../Page/我的愛情不平凡.md "wikilink")
  - [噗通噗通我愛你](../Page/噗通噗通我愛你.md "wikilink")
  - [已讀不回的戀人](../Page/已讀不回的戀人.md "wikilink")
  - [三明治女孩的逆襲](../Page/三明治女孩的逆襲.md "wikilink")
  - [高校英雄傳](../Page/高校英雄傳.md "wikilink")

<!-- end list -->

  - 週六優質戲劇

<!-- end list -->

  - [波麗士大人](../Page/波麗士大人.md "wikilink")
  - [比賽開始](../Page/比賽開始.md "wikilink")
  - [那一年的幸福時光](../Page/那一年的幸福時光.md "wikilink")
  - [第二回合我愛你](../Page/第二回合我愛你.md "wikilink")
  - [倪亞達](../Page/倪亞達.md "wikilink")
  - [犀利人妻](../Page/犀利人妻.md "wikilink")
  - [勇士們](../Page/勇士們_\(電視劇\).md "wikilink")

<!-- end list -->

  - 華人電視劇

<!-- end list -->

  - 八點檔
      - 平日
          - [真愛找麻煩](../Page/真愛找麻煩.md "wikilink")
          - [愛上巧克力](../Page/愛上巧克力.md "wikilink")
          - [剩女保鏢](../Page/剩女保鏢.md "wikilink")
          - [愛情女僕](../Page/愛情女僕.md "wikilink")
          - [兩個爸爸](../Page/兩個爸爸.md "wikilink")
          - [幸福選擇題](../Page/幸福選擇題.md "wikilink")
          - [有愛一家人](../Page/有愛一家人.md "wikilink")
          - [女人30情定水舞間](../Page/女人30情定水舞間.md "wikilink")
          - [媽咪的男朋友](../Page/媽咪的男朋友.md "wikilink")
          - [幸福兌換券](../Page/幸福兌換券.md "wikilink")
          - [我的寶貝四千金](../Page/我的寶貝四千金.md "wikilink")
          - [好想談戀愛](../Page/好想談戀愛.md "wikilink")
          - [軍官·情人](../Page/軍官·情人.md "wikilink")
          - [戀愛鄰距離](../Page/戀愛鄰距離.md "wikilink")
          - [大人情歌](../Page/大人情歌_\(電視劇\).md "wikilink")
          - [我的極品男友](../Page/我的極品男友.md "wikilink")
          - [獨家保鑣](../Page/獨家保鑣.md "wikilink")
          - [只為你停留](../Page/只為你停留.md "wikilink")
          - [真情之家](../Page/真情之家.md "wikilink")
          - [必勝大丈夫](../Page/必勝大丈夫.md "wikilink")
      - 週五
          - [我租了一個情人](../Page/我租了一個情人.md "wikilink")
          - [大紅帽與小野狼](../Page/大紅帽與小野狼.md "wikilink")
  - 九點檔
      - 平日
          - [我們發財了](../Page/我們發財了.md "wikilink")
          - [真愛趁現在](../Page/真愛趁現在.md "wikilink")
          - [美味的想念](../Page/美味的想念.md "wikilink")
  - 十點檔
      - 週五
          - [就是要你愛上我](../Page/就是要你愛上我.md "wikilink")
          - [我的自由年代](../Page/我的自由年代.md "wikilink")
          - [喜歡·一個人](../Page/喜歡·一個人.md "wikilink")
          - [22K夢想高飛](../Page/22K夢想高飛.md "wikilink")
          - [莫非，這就是愛情](../Page/莫非，這就是愛情.md "wikilink")
          - [料理高校生](../Page/料理高校生.md "wikilink")
          - [1989一念間](../Page/1989一念間.md "wikilink")
          - [飛魚高校生](../Page/飛魚高校生.md "wikilink")
          - [極品絕配](../Page/極品絕配.md "wikilink")
          - [姊的時代](../Page/姊的時代.md "wikilink")

<!-- end list -->

  - 其他時段

<!-- end list -->

  - [住左邊住右邊](../Page/住左邊住右邊.md "wikilink")
      - ◇第一季、第二季
      - 第三季《幸福小套房》
      - 第四季《全民拼幸福》
  - ◇[別再叫我外籍新娘](../Page/別再叫我外籍新娘.md "wikilink")
  - [千金百分百](../Page/千金百分百.md "wikilink")
  - ◇[孽子](../Page/孽子_\(電視劇\).md "wikilink")
  - ◇[米可，GO！](../Page/米可，GO！.md "wikilink")
  - ◇[大醫院小醫師](../Page/大醫院小醫師.md "wikilink")
  - ◇[45°C天空下](../Page/45°C天空下.md "wikilink")
  - ◇[孤戀花](../Page/孤戀花_\(2005年電視劇\).md "wikilink")
  - ◇[危險心靈](../Page/危險心靈_\(電視劇\).md "wikilink")
  - [大熊醫師家](../Page/大熊醫師家.md "wikilink")
  - [老王同學會](../Page/老王同學會.md "wikilink")
  - ◇[玻璃鞋](../Page/玻璃鞋_\(電視劇\).md "wikilink")
  - ◇
  - ◇
  - ◇[夏日香氣](../Page/夏日香氣.md "wikilink")
  - ◇[烈愛無間道](../Page/烈愛無間道.md "wikilink")
  - ◇[一起來看流星雨](../Page/一起來看流星雨.md "wikilink")
  - ◇[乾隆下江南](../Page/乾隆下江南_\(電視劇\).md "wikilink")
  - ◇[把愛搶回來](../Page/夏家三千金.md "wikilink")
  - [PM10-AM03](../Page/PM10-AM03.md "wikilink")
  - [舞吧舞吧在一起](../Page/舞吧舞吧在一起.md "wikilink")
  - ◇[京華煙雲](../Page/京華煙雲_\(1988年電視劇\).md "wikilink")
  - [你好，幸福](../Page/你好，幸福.md "wikilink")

### 綜藝節目

  - 播出中

<!-- end list -->

  - [完全娛樂](../Page/完全娛樂.md "wikilink")
  - [綜藝大熱門](../Page/綜藝大熱門.md "wikilink")
  - [型男大主廚](../Page/型男大主廚.md "wikilink")
  - [國光幫幫忙](../Page/國光幫幫忙.md "wikilink")
  - [綜藝玩很大](../Page/綜藝玩很大.md "wikilink")
  - [拜託了！女神](../Page/拜託了！女神.md "wikilink")

<!-- end list -->

  - 已停播

<!-- end list -->

  - [黃金七秒半](../Page/黃金七秒半.md "wikilink")
  - [王牌大眼睛](../Page/王牌大眼睛.md "wikilink")
  - [硬是要鬥牛](../Page/硬是要鬥牛.md "wikilink")
  - [愛上陶花園](../Page/愛上陶花園.md "wikilink")
  - [封面人物](../Page/封面人物.md "wikilink")
  - [冒險奇兵](../Page/冒險奇兵.md "wikilink")
  - [王牌大賤諜](../Page/王牌大賤諜_\(綜藝節目\).md "wikilink")
  - [至尊美食王](../Page/至尊美食王.md "wikilink")
  - [女人好犀利](../Page/女人好犀利.md "wikilink")
  - [超級接班人](../Page/超級接班人.md "wikilink")
  - [超級偶像](../Page/超級偶像.md "wikilink")
  - [超愛美小姐](../Page/超愛美小姐.md "wikilink")

### 旅遊節目（原世界地理雜誌）

  - 播出中

<!-- end list -->

  - [愛玩客](../Page/愛玩客.md "wikilink")
  - [玩客瘋高雄](../Page/玩客瘋高雄.md "wikilink")(最新一季)
  - [台三愛玩客](../Page/台三愛玩客.md "wikilink")

<!-- end list -->

  - 已停播

<!-- end list -->

  - [在中國的故事](../Page/在中國的故事.md "wikilink")
  - [美食大三通](../Page/美食大三通.md "wikilink")
  - [冒險王](../Page/冒險王_\(旅遊節目\).md "wikilink")
  - [台灣全記錄](../Page/台灣全記錄.md "wikilink")
  - [世界那麼大](../Page/世界那麼大.md "wikilink")
  - [玩客瘋德島](../Page/玩客瘋德島.md "wikilink")
  - SpeXial 的日本二本松之旅
  - 熊嫚日本趴趴GO！
  - [勇闖中台灣](../Page/勇闖中台灣.md "wikilink")

### 資訊節目

  - 播出中

<!-- end list -->

  - [婆媳當家](../Page/婆媳當家.md "wikilink")

<!-- end list -->

  - 已停播

<!-- end list -->

  - [學校沒教的事](../Page/學校沒教的事.md "wikilink")
  - [魔法生活王](../Page/魔法生活王.md "wikilink")
  - [淘氣過生活](../Page/淘氣過生活.md "wikilink")
  - [創意過生活](../Page/創意過生活.md "wikilink")
  - [Life樂生活](../Page/Life樂生活.md "wikilink")
  - [超級糾察隊](../Page/超級糾察隊.md "wikilink")
  - [音樂無雙](../Page/音樂無雙.md "wikilink")
  - [街角遇到WOW](../Page/街角遇到WOW.md "wikilink")
  - [爆米花電影院](../Page/爆米花電影院.md "wikilink")

### 卡通

※自從華人電視劇開始播出時，其重播時間取代全部的動畫節目區塊。

  - 播出中

<!-- end list -->

  - 已停播

<!-- end list -->

  - ◇[家庭教師里包恩](../Page/家庭教師HITMAN_REBORN!_\(動畫\).md "wikilink")
  - ◇[通靈王](../Page/通靈王.md "wikilink")
  - ◇
  - ◇[徽章戰士](../Page/徽章戰士.md "wikilink")
  - ◇[神奇寶貝](../Page/神奇寶貝_\(1997-2002年動畫\).md "wikilink")
  - ◇[尋找滿月](../Page/尋找滿月.md "wikilink")
  - ◇[章魚燒超人](../Page/章魚燒超人.md "wikilink")
  - ◇[忍者亂太郎](../Page/忍者亂太郎.md "wikilink")
  - ◇[遊戲王](../Page/遊戲王.md "wikilink")
  - ◇[遊戲王 怪獸之決鬥](../Page/遊戲王.md "wikilink")
  - ◇[名偵探柯南](../Page/名偵探柯南.md "wikilink")
  - ◇[足球風雲](../Page/足球風雲.md "wikilink")
  - ◇[中華一番](../Page/中華一番.md "wikilink")
  - ◇[靈異教師神眉](../Page/靈異教師神眉.md "wikilink")
  - ◇[妙廚老爹](../Page/妙廚老爹.md "wikilink")
  - ◇[超激力戰鬥車](../Page/激鬥戰車.md "wikilink")
  - ◇[甜心戰士](../Page/甜心戰士.md "wikilink")
  - ◇[慕敏家族](../Page/慕敏家族.md "wikilink")
  - ◇[幽遊白書](../Page/幽遊白書.md "wikilink")
  - ◇[少年偵探 ～推理之絆～](../Page/推理之絆.md "wikilink")
  - ◇[貓眼三姊妹](../Page/貓眼三姊妹.md "wikilink")
  - ◇[爆走兄弟](../Page/爆走兄弟.md "wikilink")
  - ◇[電腦冒險記](../Page/電腦冒險記.md "wikilink")
  - ◇[天地無用GXP戰警](../Page/天地無用!.md "wikilink")
  - ◇[秀逗博士](../Page/秀逗博士.md "wikilink")
  - ◇爆走城市

### 跨年特別節目製播

  - ◇勁爆港都98 三立情（1998年跨年節目）\[4\]
  - 《高雄夢時代跨年晚會》（2008、2009，2011-2013）
  - 《2014新北市歡樂耶誕城》
  - 《2016臺北最HIGH新年城》(2015/12/31)
  - 《2017臺北最HIGH新年城》(2016/12/31)
  - 《2018臺北最HIGH新年城》(2017/12/31)
  - 《麗寶樂園2019花現台中跨年晚會》(2018/12/31)

### 頒獎典禮

  - 金曲獎

<!-- end list -->

  - [第24屆金曲獎](../Page/第24屆金曲獎.md "wikilink")

<!-- end list -->

  - 金鐘獎

<!-- end list -->

  - [第51屆金鐘獎](../Page/第51屆金鐘獎.md "wikilink")
  - [第52屆金鐘獎](../Page/第52屆金鐘獎.md "wikilink")
  - [第53屆金鐘獎](../Page/第53屆金鐘獎.md "wikilink")

<!-- end list -->

  - 台北電影獎

<!-- end list -->

  - 第15屆台北電影獎
  - 第16屆台北電影獎
  - 第17屆台北電影獎

<!-- end list -->

  - 華劇大賞

<!-- end list -->

  - 2012華劇大賞
  - 2013華劇大賞
  - 2014華劇大賞
  - 2015華劇大賞
  - 2016華劇大賞

<!-- end list -->

  - 金音創作獎

<!-- end list -->

  - [第5屆金音創作獎](../Page/第5屆金音創作獎.md "wikilink")
  - [第9屆金音創作獎](../Page/第9屆金音創作獎.md "wikilink")

## 参考文献

## 外部連結

  - [三立電視官方網站](http://www.iset.com.tw/portal/metro.php)

  - [三立都會台第一代標誌](http://i.imgur.com/Wec9W.jpg)

  -
  -
  -
{{-}}

[三立都會台](../Category/三立都會台.md "wikilink")
[Category:三立電視旗下頻道](../Category/三立電視旗下頻道.md "wikilink")
[Category:台灣電視播放頻道](../Category/台灣電視播放頻道.md "wikilink")
[Category:1995年成立的电视台或电视频道](../Category/1995年成立的电视台或电视频道.md "wikilink")

1.  [聯合晚報](../Page/聯合晚報.md "wikilink")10版.1995-09-01
2.  江聰明.三立改台呼.[聯合報](../Page/聯合報.md "wikilink").1996-12-05
3.  青春明星熱情演出為選手加油.民生報.1998-05-01
4.  辦晚會 兩頻道南北車拚.聯合晚報.1997-12-23