[Hypersphere_coord.gif](https://zh.wikipedia.org/wiki/File:Hypersphere_coord.gif "fig:Hypersphere_coord.gif")（meridians）（藍色）以及超子午線（hypermeridians）（綠色）的[立體投影法](../Page/立體投影.md "wikilink")（Stereographic
projection）。
因為立體投影法的[共形特性](../Page/共形.md "wikilink")，這些曲線彼此在交點上彼此正交（圖中黃色點），如同在四維空間中一樣。所有曲線都是圓；交會在\<0,0,0,1\>的曲線具有無限大的半徑（亦即：直線）。\]\]

[數學中](../Page/數學.md "wikilink")，**三維球面**（英文常寫作**3-sphere**）是[球面在高維空間中的類比客體](../Page/球面.md "wikilink")。它由四維[歐幾里得空間中與一固定中心點等距離的所有點所組成](../Page/歐幾里得空間.md "wikilink")。尋常的球面（或者說[二維球面](../Page/二維球面.md "wikilink")）是一個二維[表面](../Page/表面.md "wikilink")，而三維球面是一個具有三個[維度的幾何客體](../Page/維度.md "wikilink")，這樣的幾何客體都可以歸類為[三維流形](../Page/三維流形.md "wikilink")（3-manifold）。

三維球面也稱作[超球面](../Page/超球面.md "wikilink")（hypersphere），雖然這個辭彙可以更廣義地代表任何*n*維球面，而*n*
≥ 3。

## 定義

以[座標表示](../Page/座標.md "wikilink")，三維球面具有中心（*C*<sub>0</sub>, *C*<sub>1</sub>, *C*<sub>2</sub>, *C*<sub>3</sub>）及半徑*r*
乃在**R**<sup>4</sup>符合條件

\[\sum_{i=0}^3(x_i - C_i)^2 = ( x_0 - C_0 )^2 + ( x_1 - C_1 )^2 + ( x_2 - C_2 )^2+ ( x_3 - C_3 )^2 = r^2 \,\]
的所有點的[集合](../Page/集合.md "wikilink")：
（*x*<sub>0</sub>, *x*<sub>1</sub>, *x*<sub>2</sub>, *x*<sub>3</sub>）。

三維球面球心在原點，而半徑是1的稱為**單位三維球面**(unit 3-sphere)，常寫作*S*<sup>3</sup>：

\[S^3 = \left\{(x_0,x_1,x_2,x_3)\in\mathbb{R}^4 : x_0^2 + x_1^2 + x_2^2 + x_3^2 = 1\right\}\]。

方便性上，常將**R**<sup>4</sup>另外以[複數](../Page/複數.md "wikilink")**C**<sup>2</sup>或[四元數](../Page/四元數.md "wikilink")(quaternions)**H**等價表示。單位三維球面則可寫為

\[S^3 = \left\{(z_1,z_2)\in\mathbb{C}^2 : |z_1|^2 + |z_2|^2 = 1\right\}\]
或

\[S^3 = \left\{q\in\mathbb{H} : |q| = 1\right\}\]。

最後一個表示法常是最有用的。其將三維球面描述為所有**單位四元數**——[絕對值為](../Page/絕對值.md "wikilink")1的四元數——的集合。正如同所有單位複數的集合在[複數幾何是重要的](../Page/複數幾何.md "wikilink")，所有單位四元數的集合在四元數幾何中也是重要的。

## 外部連結

  -
<!-- end list -->

  -
    注意：此篇文章使用了*n*維空間的球面，稱作*n*維球面（*n*-sphere）。

[S](../Category/代数拓扑.md "wikilink") [S](../Category/几何拓扑学.md "wikilink")
[S](../Category/四元數.md "wikilink") [S](../Category/解析几何.md "wikilink")