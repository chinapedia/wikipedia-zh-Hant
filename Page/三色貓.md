[Calico_cat_-_Phoebe.jpg](https://zh.wikipedia.org/wiki/File:Calico_cat_-_Phoebe.jpg "fig:Calico_cat_-_Phoebe.jpg")
**三色貓**，又叫**三毛貓**、**玳瑁貓**，也有的人稱呼它三花貓，指[黑色](../Page/黑色.md "wikilink")、[橘色與](../Page/橘色.md "wikilink")[白色共存在身上的](../Page/白色.md "wikilink")[貓](../Page/貓.md "wikilink")，亦稱為[玳瑁色](../Page/玳瑁.md "wikilink")。

## 成因

因決定黑色和橘色的[基因是](../Page/基因.md "wikilink")[性聯遺傳位於](../Page/性聯遺傳.md "wikilink")[X染色體上](../Page/X染色體.md "wikilink")（白色基因在常染色體上），若是貓的基因在分化的時候隨機去活性化，可能毛色細胞內黑色基因被去活性而表現出橘色，反之則呈現黑色，因此絕大部分三色貓都是[雌性](../Page/雌性.md "wikilink")。

在少數的例子裡，因多出一個以上的X染色體（[克蘭費爾特氏症候群](../Page/克蘭費爾特氏症候群.md "wikilink")）或[嵌合體的情況會出現](../Page/嵌合體.md "wikilink")[雄性三色貓](../Page/雄性.md "wikilink")，一般没有生育能力；在[日本亦因為雄性三色貓稀少的緣故](../Page/日本.md "wikilink")，將其視為一種[幸運的象徵](../Page/幸運.md "wikilink")。

## 文學

  - 日本[推理小說家](../Page/推理小說.md "wikilink")[赤川次郎的一個系列就以此為名](../Page/赤川次郎.md "wikilink")（）。
  - 日本[輕小說家](../Page/輕小說.md "wikilink")[谷川流的作品](../Page/谷川流.md "wikilink")「[涼宮春日系列](../Page/涼宮春日系列.md "wikilink")」中登場的公貓[三味線就是這種類型的公貓](../Page/涼宮春日系列角色列表#三味線.md "wikilink")。
  - 日本[漫畫家](../Page/漫畫.md "wikilink")[衫作的作品為什麼貓都要當老大中的三色公貓](../Page/衫作.md "wikilink")[信長也是這類型的貓](../Page/信長.md "wikilink")。
  - 日本[小說家](../Page/小說.md "wikilink")[夏目漱石的作品](../Page/夏目漱石.md "wikilink")[我是貓中作者自比](../Page/我是貓.md "wikilink")，用來諷刺社會的無名貓也是這種類型的貓

## ACG

动画《[猫愿三角恋](../Page/猫愿三角恋.md "wikilink")》中的一只雄性三色猫，名字是与现实中的日本猫猫站长相同的[小玉](../Page/小玉_\(猫\).md "wikilink")（Tama）。
动画《[問題兒童都來自異世界](../Page/問題兒童都來自異世界.md "wikilink")》中，和春日部耀一同抵達箱庭的貓。

## 音樂

K-POP組合[BTS](../Page/BTS.md "wikilink")（[防彈少年團](../Page/防彈少年團.md "wikilink")）于2017年9月5日公開的新專輯LOVE
YOURSELF 承 Her "Serendipity" Comeback
Trailer中曾引用到“三色貓”的概念。擔任作詞的隊長[RM](../Page/RM.md "wikilink")（[金南俊](../Page/金南俊.md "wikilink")）在歌詞中寫道"난
네 삼색 고양이, 널 만나러
온."（“我是你的三色貓，為遇見你而來”）表達了少年們對傾慕之人的悸動，以罕有的雄性三色貓，引出“我是你生命中，那罕有、獨特的人”。

## 三色貓的顏色分布

<center>

|典型玳瑁貓 [File:中長毛玳瑁貓.jpg|介於短及長毛間玳瑁貓](File:中長毛玳瑁貓.jpg%7C介於短及長毛間玳瑁貓)
<File:Long-haired> tortoiseshell DSCF0193.JPG|長毛玳瑁貓 <File:Calico> Cat on
a red cushion.jpg|典型混白玳瑁貓 <File:Gato> 070330 2.jpg|典型三色貓
[File:Three_color_cat.jpg|波斯三色貓](File:Three_color_cat.jpg%7C波斯三色貓)

</center>

## 参看

  - [小玉](../Page/小玉_\(貓\).md "wikilink")
  - Joni

{{-}}

[S](../Category/貓皮毛種類.md "wikilink")