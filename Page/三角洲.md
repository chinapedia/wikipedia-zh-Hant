[Mississippi_Delta_Lobes.jpg](https://zh.wikipedia.org/wiki/File:Mississippi_Delta_Lobes.jpg "fig:Mississippi_Delta_Lobes.jpg")
**三角洲**，及河口沖積平原（不等於沖積扇），是一种常见的地表形貌。江河奔流中所裹挟的泥沙等杂质，在[入海口处遇到含盐量较淡水高得多的海水](../Page/入海口.md "wikilink")，凝絮淤积，逐渐成为河口岸边新的[湿地](../Page/湿地.md "wikilink")，继而形成三角洲平原。三角洲的顶部指向[河流](../Page/河流.md "wikilink")[上游](../Page/上游.md "wikilink")，外缘面向大海，可以看作是三角形的“底边”。

「三角洲」這名詞翻译自英文delta。英文delta即希腊文[Δ的转写](../Page/Δ.md "wikilink")。希腊文含义源自三角洲的形狀像“Δ”（三角形），有人认为这就是字母“Δ”的象形起源。

## 地貌形体

### 三角洲形体类型

三角洲的形态可以分为：

  - **尖头形三角洲**（如[长江三角洲](../Page/长江三角洲.md "wikilink")），[波浪显著地控制着三角洲前缘的形态](../Page/波浪.md "wikilink")，其前缘沉积物大多经[波浪改造](../Page/波浪.md "wikilink")、移动和重新分配，形成大体平行于[海岸线分布的](../Page/海岸线.md "wikilink")[沙坝或](../Page/沙坝.md "wikilink")[沙滩](../Page/沙滩.md "wikilink")，其中[河口附近沙体堆积较多](../Page/河口.md "wikilink")。
  - **扇形三角洲**（如[尼罗河三角洲](../Page/尼罗河三角洲.md "wikilink")），以河流为主的**单一三角洲体**，河流呈**放射状**分汊，汊道众多，各汊道多次改道、摆动，使整个三角洲岸线向海推进，其河流沙坝又经过波浪改造，彼此连接，造成形体成蛇形或扇形的三角洲，称扇形三角洲。
  - **鸟足型三角洲**（如[密西西比河三角洲](../Page/密西西比河三角洲.md "wikilink")），**河流作用占优势**的**河流型三角洲**，平面形态常呈鸟足状。
  - **指形三角洲**（如[巴布亚湾巴布亚河口三角洲](../Page/巴布亚湾巴布亚河口三角洲.md "wikilink")），发育以潮流作用为主一般在近岸部分有涨潮形成的泥滩；向海部分在落潮的作用下，在分流的河口内及出海口地段上，形成一系列与潮流方向平行的带状或指形沙垅（坝）。

### 形成

#### 基本条件

  - 丰富的泥沙来源
  - 海洋的侵蚀运移能力较小
  - 口外海滨区地势平缓，水深较浅

#### 发育过程

\-下游的沉積地貌 -細小的沉積物，因河流流速減慢，而在河口沉積 -冬季時，沉積物在河口沉積和阻塞 -夏季時，高流量的河水會貫穿沉積物令分流出現
-當河水流到海洋時，會出現沉積 -持續阻礙和貫穿會形成一片泥地，形成出三角洲

### 河口及其分段

  - 近口段
  - 河口段
  - 口外海滨段

#### 河口水理化特征和沉积作用

### 与人类的关系

三角洲因为地势平坦，土地肥沃，依河临海，生物[资源非常丰富](../Page/资源.md "wikilink")，为农业、渔业、[养殖业的开发提供了相当有利的条件](../Page/养殖业.md "wikilink")，故气候适宜地区的三角洲通常是当地[人口稠密](../Page/人口.md "wikilink")、[经济发达的地区](../Page/经济.md "wikilink")。此外，三角洲的地质演变往往利于形成现代社会迫切需要的[石油和](../Page/石油.md "wikilink")[天然气](../Page/天然气.md "wikilink")。

另外一方面，由于人类过度开發带来的[生态破坏与过早的开发造成的](../Page/生态.md "wikilink")[污染积累](../Page/污染.md "wikilink")，以及来自河流、海洋等方面迁移性污染的汇聚，三角洲也当属世界上生态最为脆弱，生态危机甚为深重的地区之一。

## 著名三角洲列表

[NileDelta-EO.JPG](https://zh.wikipedia.org/wiki/File:NileDelta-EO.JPG "fig:NileDelta-EO.JPG")

  - [尼罗河三角洲](../Page/尼罗河三角洲.md "wikilink")
  - [密西西比河三角洲](../Page/密西西比河三角洲.md "wikilink")
  - [多瑙河三角洲](../Page/多瑙河三角洲.md "wikilink")
  - [湄公河三角洲](../Page/湄公河三角洲.md "wikilink")
  - [恆河三角洲](../Page/恆河三角洲.md "wikilink")
  - [紅河三角洲](../Page/紅河三角洲.md "wikilink")
  - [印度河三角洲](../Page/印度河三角洲.md "wikilink")
  - [尼日尔河三角洲](../Page/尼日尔河三角洲.md "wikilink")
  - [伏尔加河三角洲](../Page/伏尔加河三角洲.md "wikilink")
  - [长江三角洲](../Page/长江三角洲.md "wikilink")
  - [黄河三角洲](../Page/黄河三角洲.md "wikilink")
  - [珠江三角洲](../Page/珠江三角洲.md "wikilink")
  - [奥卡万戈三角洲](../Page/奥卡万戈三角洲.md "wikilink")
  - [韓江三角洲](../Page/韓江三角洲.md "wikilink")

## 参见

  - [冲积平原](../Page/冲积平原.md "wikilink")
  - [入海口](../Page/入海口.md "wikilink")
  - [湿地](../Page/湿地.md "wikilink")
  - [外动力地貌](../Page/外动力地貌.md "wikilink")
  - [流水地貌](../Page/流水地貌.md "wikilink")
  - [地貌形体](../Page/地貌形体.md "wikilink")

## 参考

  - 《中国少年儿童百科全书》自然·环境 183-184页 浙江教育出版社 1994年12月第2版

## 外部链接

  - [Louisiana State University
    Geology](http://www.geol.lsu.edu/WDD/DELTA_LISTS/continents.htm) -
    World Deltas

[三角洲](../Category/三角洲.md "wikilink")
[Category:平原](../Category/平原.md "wikilink")
[Category:河口](../Category/河口.md "wikilink")