[HK_Sai_Ying_Pun_Triangle_Piers_Ullambana_Flag_near_Western_Park_Sport_Centre_2.JPG](https://zh.wikipedia.org/wiki/File:HK_Sai_Ying_Pun_Triangle_Piers_Ullambana_Flag_near_Western_Park_Sport_Centre_2.JPG "fig:HK_Sai_Ying_Pun_Triangle_Piers_Ullambana_Flag_near_Western_Park_Sport_Centre_2.JPG")以三角碼頭命名\[<http://www.ird.gov.hk/cgi-bin/ird/ach/search.cgi?lang=e&id=91/05911>,\]
\]\]
[HK_Sai_Ying_Pun_Des_Voeux_Road_West_三角碼頭孟蘭勝會_Association_Limited_Dec-2009.JPG](https://zh.wikipedia.org/wiki/File:HK_Sai_Ying_Pun_Des_Voeux_Road_West_三角碼頭孟蘭勝會_Association_Limited_Dec-2009.JPG "fig:HK_Sai_Ying_Pun_Des_Voeux_Road_West_三角碼頭孟蘭勝會_Association_Limited_Dec-2009.JPG")
**三角碼頭**是[香港歷史上的一個](../Page/香港歷史.md "wikilink")[碼頭](../Page/碼頭.md "wikilink")，位於現在[上環消防局和](../Page/上環消防局.md "wikilink")[皇后街之間](../Page/皇后街.md "wikilink")。在[香港](../Page/香港.md "wikilink")18世紀尾至19世紀中，香港已經是一個商貿[運輸繁忙的](../Page/運輸.md "wikilink")[轉口港](../Page/轉口港.md "wikilink")，連接[珠三角](../Page/珠三角.md "wikilink")、[廣州](../Page/廣州.md "wikilink")、[福建](../Page/福建.md "wikilink")、[潮州](../Page/潮州.md "wikilink")、[東南亞等](../Page/東南亞.md "wikilink")[港口](../Page/港口.md "wikilink")，尤以[香港島北岸為首](../Page/香港島.md "wikilink")，附近有[南北行等](../Page/南北行.md "wikilink")，沿岸有不少[散貨碼頭](../Page/散貨.md "wikilink")。聘任[碼頭工人](../Page/碼頭工人.md "wikilink")（俗稱碼頭苦力，又稱「咕喱」）搬運貨物，更不少是[潮州人](../Page/潮州人.md "wikilink")\[1\]
\[2\]。

三角碼頭，又名**永樂街碼頭**，[永樂街以西](../Page/永樂街.md "wikilink")，該處有塊三角位的[地皮](../Page/地皮.md "wikilink")，現在建築物名為[德輔道西9號](../Page/德輔道西9號.md "wikilink")\[3\]，以北就是永樂街碼頭，所以人稱「三角碼頭」\[4\]
\[5\] \[6\]。

在[香港淪陷之時](../Page/香港淪陷.md "wikilink")，[日軍在三角碼頭殺死不少](../Page/日本军.md "wikilink")[香港人](../Page/香港人.md "wikilink")，引起香港見證歷史的居民有心理陰影，所以有不少於五個[盂蘭勝會的](../Page/盂蘭勝會.md "wikilink")[社會團體出現](../Page/社會團體.md "wikilink")，包括有[三角碼頭盂蘭勝會](../Page/三角碼頭盂蘭勝會.md "wikilink")，發展至今。
\[7\]

## 鄰近

  - [干諾道西](../Page/干諾道西.md "wikilink")
  - [永樂街](../Page/永樂街.md "wikilink")
  - [高陞街](../Page/高陞街.md "wikilink")
  - [皇后大道西](../Page/皇后大道西.md "wikilink")
  - [德輔道西](../Page/德輔道西.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:香港歷史](../Category/香港歷史.md "wikilink")
[Category:上環](../Category/上環.md "wikilink")
[Category:香港貨運碼頭](../Category/香港貨運碼頭.md "wikilink")

1.  [香港潮商溯源:
    從前初到香港的潮州人，多数住在三角码头至西营盘一带。](http://www.chaorenwang.com/chaosh/showdontai.asp?nos=1416)
2.  [《華僑新聞報》:
    三角碼頭潮州人](http://www.sa-cnet.com/home/writings/02goodwork/gw2000/57.html)

3.  [香港地圖](http://www.centamap.com/gc/centamaplocation.aspx?x=833226&y=816569&sx=833226.512&sy=816569.064&z=3)
4.  [三角碼頭，又名 永樂街碼頭](http://hk.epochtimes.com/7/7/13/47982.htm)
5.  [香港掌故: 三角碼頭](http://hk.epochtimes.com/7/6/1/45690.htm)
6.  [香港地方: 三角碼頭](http://www.hk-place.com/db.php?post=d001015)
7.  [三角碼頭盂蘭勝會的由來](http://www.hku.hk/sociodep/oralhistory/2/2.2.1.4.html)