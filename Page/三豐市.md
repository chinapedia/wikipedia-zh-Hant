**三豐市**（）是位於[日本](../Page/日本.md "wikilink")[香川縣](../Page/香川縣.md "wikilink")[西讚地方的](../Page/西讚.md "wikilink")[城市](../Page/城市.md "wikilink")。以[紫雲出山及](../Page/紫雲出山.md "wikilink")[浦島太郎傳說而聞名](../Page/浦島太郎.md "wikilink")。在香川縣內為人口第三多的城市，次于[高松市和](../Page/高松市.md "wikilink")[丸龜市](../Page/丸龜市.md "wikilink")。

轄區西北側為[莊內半島](../Page/莊內半島.md "wikilink")，西側臨[燧灘](../Page/燧灘.md "wikilink")，北側為[詫間灣](../Page/詫間灣.md "wikilink")，南側以[讚岐山脈與](../Page/讚岐山脈.md "wikilink")[德島縣相鄰](../Page/德島縣.md "wikilink")。

過去[製鹽為主要產業](../Page/製鹽.md "wikilink")，現在沿海地區則已開發為工業區。

## 歷史

在[令制國時代屬於](../Page/令制國.md "wikilink")[三野郡勝間鄉](../Page/三野郡.md "wikilink")、高瀨鄉、詫間鄉、熊岡鄉、高野鄉、大野鄉、本山鄉）和[豐田郡](../Page/豐田郡_\(香川縣\).md "wikilink")（刈田郡）山本鄉。

### 年表

  - 1890年2月15日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：
      - [三野郡](../Page/三野郡.md "wikilink")：仁尾村、莊內村、粟島村、詫間村、大見村、吉津村、下高瀨村、上高瀨村、勝間村、笠田村、比地二村、比地大村、桑山村、本山村、上高野村、二之宮村、麻村、神田村、財田村、財田大野村。
      - [豐田郡](../Page/豐田郡_\(香川縣\).md "wikilink")：辻村、河內村。
  - 1899年4月1日：三野郡和豐田郡合併為[三豐郡](../Page/三豐郡.md "wikilink")。
  - 1924年4月1日：仁尾村改制為[仁尾町](../Page/仁尾町.md "wikilink")。
  - 1942年4月15日：詫間村改制為[詫間町](../Page/詫間町.md "wikilink")。
  - 1955年3月31日：
      - 比地二村、二之宮村、麻村、勝間村、上高瀨村[合併為](../Page/市町村合併.md "wikilink")[高瀨町](../Page/高瀨町_\(香川縣\).md "wikilink")。
      - 本山村、上高野村、笠田村、比地大村、桑山村合併為豐中村。
  - 1955年4月1日：
      - 辻村、河內村、神田村、財田大野村合併為山本村。
      - 大見村、下高瀨村、吉津村合併為三野村。
      - 詫間町、粟島村、莊內村合併為新設置的詫間町。
  - 1957年1月1日：豐中村改制為[豐中町](../Page/豐中町_\(香川縣\).md "wikilink")。
  - 1957年11月3日：山本村改制為[山本町](../Page/山本町_\(香川縣\).md "wikilink")。
  - 1961年9月1日：三野村改制為[三野町](../Page/三野町_\(香川縣\).md "wikilink")。
  - 1970年2月15日：財田村改制為[財田町](../Page/財田町_\(香川縣\).md "wikilink")，為香川縣最後一個村級行政區劃。
  - 2006年1月1日：仁尾町、高瀨町、豐中町、山本町、財田町、詫間町、三野町合併為**三豐市**。\[1\]

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1890年2月15日</p></th>
<th><p>1890年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1988年</p></th>
<th><p>1989年至今</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>仁尾村</p></td>
<td><p>1924年4月1日改制為仁尾町</p></td>
<td><p>2006年1月1日<br />
合併為三豐市</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>莊內村</p></td>
<td><p>1955年4月1日<br />
合併為詫間町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>粟島村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>詫間村</p></td>
<td><p>1942年4月15日改制為詫間町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大見村</p></td>
<td><p>1955年4月1日<br />
合併為三野村</p></td>
<td><p>1961年9月1日<br />
改制為三野町</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>吉津村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>下高瀨村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>上高瀨村</p></td>
<td><p>1955年3月31日<br />
合併為高瀨町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>勝間村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>比地二村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>二之宮村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>麻村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>笠田村</p></td>
<td><p>1955年3月31日<br />
合併為豐中村</p></td>
<td><p>1957年1月1日<br />
改制為豐中町</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>比地大村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>桑山村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>本山村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>上高野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>財田村</p></td>
<td><p>1970年2月15日改制為財田町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>財田大野村</p></td>
<td><p>1955年4月1日<br />
合併為山本村</p></td>
<td><p>1957年11月3日<br />
改制為山本町</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>神田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>辻村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>河內村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [四國旅客鐵道](../Page/四國旅客鐵道.md "wikilink")
      - [予讚線](../Page/予讚線.md "wikilink")：（多度津町） -
        [津島之宮站](../Page/津島之宮站.md "wikilink")（臨時站） -
        [詫間站](../Page/詫間站.md "wikilink") -
        [三野站](../Page/三野站.md "wikilink") -
        [高瀨站](../Page/高瀨站_\(香川縣\).md "wikilink")　-
        [比地大站](../Page/比地大站.md "wikilink") -
        [本山站](../Page/本山站_\(香川縣\).md "wikilink") - （觀音寺市）
      - [土讚線](../Page/土讚線.md "wikilink")：（滿濃町） -
        [讚岐財田站](../Page/讚岐財田站.md "wikilink") - （三好市）

|                                                                                                                                                                  |                                                                                                |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------- |
| [20170804_Tsushimanomiya_Station_1.jpg](https://zh.wikipedia.org/wiki/File:20170804_Tsushimanomiya_Station_1.jpg "fig:20170804_Tsushimanomiya_Station_1.jpg") | [Mino-station.JPG](https://zh.wikipedia.org/wiki/File:Mino-station.JPG "fig:Mino-station.JPG") |
| [Sanuki-saida_Station.jpg](https://zh.wikipedia.org/wiki/File:Sanuki-saida_Station.jpg "fig:Sanuki-saida_Station.jpg")                                          |                                                                                                |

其中津島之宮站為每年津嶋神社在夏之大祭時的（8月4日和8月5日）兩天開放的臨時車站。

### 道路

  - 高速道路

<!-- end list -->

  - [高松自動車道](../Page/高松自動車道.md "wikilink")：[三豐鳥坂交流道](../Page/三豐鳥坂交流道.md "wikilink")
    - [高瀬休息區](../Page/高瀬休息區.md "wikilink") -
    [讚岐豐中交流道](../Page/讚岐豐中交流道.md "wikilink")

### 渡輪

  - 粟島汽船：[詫間港](../Page/詫間港.md "wikilink") - 粟島、志志島
  - 蔦島渡船：[仁尾港](../Page/仁尾港.md "wikilink") - 蔦島

### 公車

  - 三豐市社區巴士
      - 高瀨線
      - 高瀨仁尾線
      - 山本線
      - 三野線
      - 財田高瀨線
      - 高瀨觀音寺線
      - 豐中仁尾線
      - 詫間線
      - 莊內線
      - 詫間三野線
      - 仁尾線
      - 財田觀音寺線

## 觀光資源

[Awashima_Kagawa.jpg](https://zh.wikipedia.org/wiki/File:Awashima_Kagawa.jpg "fig:Awashima_Kagawa.jpg")
[Daikoji_10.JPG](https://zh.wikipedia.org/wiki/File:Daikoji_10.JPG "fig:Daikoji_10.JPG")

  - 紫雲出山遺跡館
  - 浦島花卉公園
  - [粟島海洋記念公園](../Page/粟島海洋記念公園.md "wikilink")
  - [粟島海洋記念館](../Page/粟島海洋記念館.md "wikilink")
  - [津嶋神社](../Page/津嶋神社_\(三豐市\).md "wikilink")
  - [大水上神社](../Page/大水上神社.md "wikilink")（讚岐國二宮）
  - [四國八十八箇所靈場](../Page/四國八十八箇所.md "wikilink")
      - 第67號札所 [大興寺](../Page/大興寺_\(三豊市\).md "wikilink")
      - 第70號札所 [本山寺](../Page/本山寺_\(三豊市\).md "wikilink")
      - 第71號札所 [彌谷寺](../Page/彌谷寺.md "wikilink")

[MotoyamaJi,No.70_本山寺_(三豊市)_四国八十八箇所霊場第七十番札所.JPG](https://zh.wikipedia.org/wiki/File:MotoyamaJi,No.70_本山寺_\(三豊市\)_四国八十八箇所霊場第七十番札所.JPG "fig:MotoyamaJi,No.70_本山寺_(三豊市)_四国八十八箇所霊場第七十番札所.JPG")

## 教育

[SetouchiTankiDaigaku.jpg](https://zh.wikipedia.org/wiki/File:SetouchiTankiDaigaku.jpg "fig:SetouchiTankiDaigaku.jpg")

  - 大學、短期大學

<!-- end list -->

  - [瀨戶內短期大學](../Page/瀨戶內短期大學.md "wikilink")

<!-- end list -->

  - 專修學校

<!-- end list -->

  - [瀨戶內綜合學院](../Page/瀨戶內綜合學院.md "wikilink")

<!-- end list -->

  - 高等專門學校

<!-- end list -->

  - [詫間電波工業高等專門學校](../Page/詫間電波工業高等專門學校.md "wikilink")

<!-- end list -->

  - 高等學校

<!-- end list -->

  - [香川縣立高瀨高等學校](../Page/香川縣立高瀨高等學校.md "wikilink")
  - [香川縣立笠田高等學校](../Page/香川縣立笠田高等學校.md "wikilink")
  - [香川西高等學校](../Page/香川西高等學校.md "wikilink")

<!-- end list -->

  - 中學校

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="http://www.niji.or.jp/school/nio-tyu/">三豐市立仁尾中學校</a></li>
<li><a href="https://web.archive.org/web/20070517230248/http://www.takuchu.town.takuma.kagawa.jp/">三豐市立詫間中學校</a></li>
<li><a href="https://web.archive.org/web/20070630130729/http://www.takasecho-kgw.ed.jp/~takase/index.html">三豐市立高瀨中學校</a></li>
<li>三豐市立豐中中學校</li>
</ul></td>
<td><ul>
<li>三豐市立和光中學校</li>
<li><a href="http://edu.city.mitoyo.lg.jp/minotsujh/">三豐市立三野津中學校</a></li>
<li><a href="https://web.archive.org/web/20071007185614/http://www.niji.jp/home/mitoyojh/">三豐市觀音寺市學校組合立三豐中學校</a></li>
</ul></td>
</tr>
</tbody>
</table>

  - 小學校

<table>
<tbody>
<tr class="odd">
<td><ul>
<li>三豐市立仁尾小學校</li>
<li>三豐市立曾保小學校</li>
<li>三豐市立大濱小學校</li>
<li><a href="https://web.archive.org/web/20070205120121/http://www.matsusyo.town.takuma.kagawa.jp/">三豐市立松崎小學校</a></li>
<li><a href="https://web.archive.org/web/20060902141519/http://www.hakosyo.town.takuma.kagawa.jp/">三豐市立箱浦小學校</a></li>
<li>三豐市立詫間小學校</li>
<li><a href="https://web.archive.org/web/20060517032552/http://www.takasecho-kgw.ed.jp/~katsuma/">三豐市立勝間小學校</a></li>
<li><a href="https://web.archive.org/web/20060517032540/http://www.takasecho-kgw.ed.jp/~kamitakase/">三豐市立上高瀨小學校</a></li>
</ul></td>
<td><ul>
<li><a href="https://web.archive.org/web/20070602115918/http://www.takasecho-kgw.ed.jp/~ninomiya/">三豐市立二之宮小學校</a></li>
<li><a href="https://web.archive.org/web/20070601043409/http://www.takasecho-kgw.ed.jp/~asa/">三豐市立麻小學校</a></li>
<li><a href="https://web.archive.org/web/20060517032702/http://www.takasecho-kgw.ed.jp/~hiji/">三豐市立比地小學校</a></li>
<li><a href="https://web.archive.org/web/20071008175328/http://www.niji.or.jp/home/kasada/">三豐市立笠田小學校</a></li>
<li>三豐市立上高野小學校</li>
<li><a href="https://web.archive.org/web/20061007084355/http://www.niji.or.jp/school/kuwayama/">三豐市立桑山小學校</a></li>
<li><a href="https://web.archive.org/web/20070927192456/http://www.niji.or.jp/school/hizidai/">三豐市立比地大小學校</a></li>
<li>三豐市立本山小學校</li>
<li>三豐市立辻小學校</li>
</ul></td>
<td><ul>
<li><a href="http://www.niji.or.jp/school/kouti/">三豐市立河內小學校</a></li>
<li>三豐市立大野小學校</li>
<li>三豐市立神田小學校</li>
<li>三豐市立財田上小學校</li>
<li>三豐市立財田中小學校</li>
<li>三豐市立下高瀨小學校</li>
<li>三豐市立吉津小學校</li>
<li><a href="https://web.archive.org/web/20051231032029/http://www.main.or.jp/omi-s/">三豐市立大見小學校</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 姊妹、友好都市

### 日本

  - [美波町](../Page/美波町.md "wikilink")（[德島縣](../Page/德島縣.md "wikilink")[海部郡](../Page/海部郡_\(德島縣\).md "wikilink")）
      -
        於2007年7月21日締結為友好都市
  - [洞爺湖町](../Page/洞爺湖町.md "wikilink")（[北海道](../Page/北海道.md "wikilink")[虻田郡](../Page/虻田郡.md "wikilink")）
      - 於2007年7月1日地結為友好都市\[2\]

### 海外

  - [陜川郡](../Page/陜川郡.md "wikilink")（[韓國](../Page/韓國.md "wikilink")[慶尚南道](../Page/慶尚南道.md "wikilink")）

      -
        於2007年7月13日地結為友好都市

  - （[美國](../Page/美國.md "wikilink")[威斯康辛州](../Page/威斯康辛州.md "wikilink")[沃帕卡縣](../Page/沃帕卡縣_\(威斯康辛州\).md "wikilink")）

  - [三原縣](../Page/三原縣.md "wikilink")（[中華人民共和國](../Page/中華人民共和國.md "wikilink")[陝西省](../Page/陝西省.md "wikilink")[咸陽市](../Page/咸陽市.md "wikilink")）

      -
        於2009年1月13日地結為友好都市

## 本地出身之名人

[Ookubo-Jinnojyou.jpg](https://zh.wikipedia.org/wiki/File:Ookubo-Jinnojyou.jpg "fig:Ookubo-Jinnojyou.jpg")

  - [石井絹治郎](../Page/石井絹治郎.md "wikilink")：[大正製藥創辦人](../Page/大正製藥.md "wikilink")。
  - [大久保諶之丞](../Page/大久保諶之丞.md "wikilink")：明治時代政治家、曾任愛媛縣議會議員、香川縣議會議員，致力於建設四國地區的交通建設。
  - [矢野庄太郎](../Page/矢野庄太郎.md "wikilink")：政治家、曾任[台湾總督府書記官](../Page/台湾總督府.md "wikilink")、前[眾議院議員](../Page/眾議院_\(日本\).md "wikilink")、前[大藏大臣](../Page/大藏大臣.md "wikilink")。
  - [真鍋賢二](../Page/真鍋賢二.md "wikilink")：前[參議院議員](../Page/參議院_\(日本\).md "wikilink")、曾任環境廳長官。
  - [前川忠夫](../Page/前川忠夫.md "wikilink")：前[香川大學校長](../Page/香川大學.md "wikilink")、曾任香川縣知事。
  - [栗本キミ代](../Page/栗本キミ代.md "wikilink")（松崎キミ代）：前桌球選手、生涯累積[世界桌球錦標賽七面金牌](../Page/世界桌球錦標賽.md "wikilink")。
  - [田村實造](../Page/田村實造.md "wikilink")：[亞洲史學者](../Page/亞洲史.md "wikilink")、前[京都女子大學校長](../Page/京都女子大學.md "wikilink")。
  - [要潤](../Page/要潤.md "wikilink")：演員和模特兒
  - [馬渕英俚可](../Page/馬渕英俚可.md "wikilink")：女演員

## 参考资料

## 外部連結

  - [三豐市觀光交流局](http://www.mitoyo-kanko.com/)

  - [三豐市中小企業振興協議會](http://mitoyo-kigyo.org/)

  - [三豐合併協議會](http://www.city.mitoyo.lg.jp/hf/mitoyo-gappei/index.htm)

<!-- end list -->

1.
2.