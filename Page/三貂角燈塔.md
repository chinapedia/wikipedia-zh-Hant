**三貂角燈塔**，位於[臺灣](../Page/臺灣.md "wikilink")[新北市](../Page/新北市.md "wikilink")[貢寮區](../Page/貢寮區.md "wikilink")[三貂角岬角處](../Page/三貂角.md "wikilink")、地址為：22841新北市貢寮區福連村馬崗街38號。為典型頂層外有環繞[陽台之燈塔造型](../Page/陽台.md "wikilink")，臺灣最東邊之[燈塔](../Page/燈塔.md "wikilink")。燈塔旁設有[交通部民用航空局飛航服務總臺三貂角雷達臺](../Page/交通部民用航空局.md "wikilink")。
[ROC-MOTC-CAA-ANWS_Sandiaojiao_Radar_Site_20170404.jpg](https://zh.wikipedia.org/wiki/File:ROC-MOTC-CAA-ANWS_Sandiaojiao_Radar_Site_20170404.jpg "fig:ROC-MOTC-CAA-ANWS_Sandiaojiao_Radar_Site_20170404.jpg")

## 歷史

公元1935年（[昭和](../Page/昭和.md "wikilink")10年）燈塔完工。最早燈器為三等煤油白熱燈，1942年改裝為三等旋轉透鏡電燈。西元1929年及1931年，[日本船舶](../Page/日本.md "wikilink")[撫順丸](../Page/撫順丸.md "wikilink")（Bujun
Maru）及[華南丸](../Page/華南丸.md "wikilink")（Kwanan
Maru）\[1\]，先後於[三貂角附近海域遭遇](../Page/三貂角.md "wikilink")[海難沉沒](../Page/海難.md "wikilink")，[台灣總督府因此於此地籌建此](../Page/台灣總督府.md "wikilink")[燈塔](../Page/燈塔.md "wikilink")。[二戰時遭盟軍戰機炸射毀損](../Page/第二次世界大战.md "wikilink")，1946年修復運作。

### 三貂角教堂

三貂角教堂、原是三貂角燈塔之燈塔燃油的堆放倉庫，位於燈塔旁。由於燈塔已不燒煤油了，2010年始，[東北角暨宜蘭海岸國家風景區管理處將閒置多時的倉庫以教堂型的圓拱](../Page/東北角暨宜蘭海岸國家風景區.md "wikilink")、加以彩繪玻璃，打造成類似歐式教堂風格的建築，別名「三貂角教堂」，吸引大多數的婚紗攝影業者造訪；目前內部尚未開放，外部可供遊客取景拍照。\[2\]

## 燈塔結構諸元

  - 塔高：16.5公尺。
  - 塔身塗色・構造：混凝土圓塔、外觀漆[白](../Page/白色.md "wikilink")，頂層外有環繞[陽台](../Page/陽台.md "wikilink")。
  - 燈高：100.6公尺{{〈}}以高潮面至燈火中心計{{〉}}。
  - 燈器：3等旋轉透鏡電燈。
  - 燈質（頻率）：每28秒旋轉1週（連閃白、紅光2次）
  - [光力](../Page/發光強度.md "wikilink")：白光650,000支[燭光](../Page/燭光.md "wikilink")、紅光110,000支[燭光](../Page/燭光.md "wikilink")。
  - 公稱[光程](../Page/光程.md "wikilink")：白光24.5[浬](../Page/浬.md "wikilink")、紅光20[浬](../Page/浬.md "wikilink")。\[3\]
  - [明弧](../Page/明弧.md "wikilink")：紅光011度-021度，白光-011度。

### 燈塔行政諸元

  - 管轄機關：燈塔原為[中華民國](../Page/中華民國.md "wikilink")[財政部關務署管理](../Page/財政部關務署.md "wikilink")，已於2013年1月1日轉交由中華民國[交通部航港局管轄](../Page/交通部航港局.md "wikilink")。
  - 人員編制：有看守人。
  - 開放參觀與否：可（1992年1月起），免費參觀。上午九時至下午六時（夏令時間4月1日至10月31日），上午九時至下午五時（冬令時間11月1日至3月31日），每逢星期一內部整理不開放。
  - 展覽室：位於燈塔主體下。展示臺灣燈塔的史料、珍貴圖片與分布圖，及郵局發行的燈塔郵票及航港局的主要業務。並陳列不同的燈具零件及模型。
  - 電話：02-24991300。

<center>

<File:Sandiaojiao> Lighthouse from the gate.jpg|三貂角燈塔正門
[File:三貂角燈塔仰景.JPG|三貂角燈塔](File:三貂角燈塔仰景.JPG%7C三貂角燈塔)
<File:Cape> Santiago Lighthouse2.jpg|三貂角燈塔 <File:Shandiaojiao>
Lighthouse 1.jpg|三貂角燈塔
[File:三貂角雷達站.jpg|三貂角雷達站](File:三貂角雷達站.jpg%7C三貂角雷達站)

</center>

## 交通路線

客運路線：

1.  由台北北站搭往宜蘭、羅東的[國光客運濱海線](../Page/國光客運.md "wikilink")，於三貂角站下車。

自行開車：

1.  台北 → 重慶北路交流道 → ２乙省道（大度路）→ 淡水 → ２號省道。
2.  中山高速公路 → 八堵交流道 → ２號省道 。
3.  三重 → 五股交流道 → 關渡大橋 → 竹圍 → 登輝大道 → ２號省道。
4.  台北 → [陽明山國家公園](../Page/陽明山國家公園.md "wikilink") → 陽金公路（２甲省道）→ 金山
    →２號省道。

自行車路線：

1.  由[福隆車站前租](../Page/福隆車站.md "wikilink")[自行車沿濱海](../Page/自行車.md "wikilink")2號省道前往，約40分鐘可抵達。

## 關連項目

  - [台灣燈塔列表](../Page/台灣燈塔列表.md "wikilink")
  - [美國燈塔列表](../Page/美國燈塔列表.md "wikilink")（Lighthouses in the United
    States）
  - [世界燈塔列表](../Page/世界燈塔列表.md "wikilink")（List of lighthouses and
    lightvessels）
  - [日本燈塔50選](../Page/日本燈塔50選.md "wikilink")
  - [萊萊山](../Page/萊萊山.md "wikilink")
  - [隆隆山](../Page/隆隆山.md "wikilink")
  - [隆林山](../Page/隆林山.md "wikilink")
  - [卯里山](../Page/卯里山.md "wikilink")（荖蘭山）

## 註釋

## 參考文獻

1.  李素芳
    編著，《台灣的燈塔》，遠足文化事業，[臺北縣](../Page/新北市.md "wikilink")，2002年12月，pp. 88–91。ISBN
    957-28031-2-3
2.  葉倫會
    著，《海洋領航者－台灣燈塔展》，[高雄市立歷史博物館](../Page/高雄市立歷史博物館.md "wikilink")，[高雄市](../Page/高雄市.md "wikilink")，2000年12月，pp. 40–41。ISBN
    957-02-7455-7
3.  財政部關稅總局編撰，《中華民國海關簡史》，[台北市](../Page/台北市.md "wikilink")，1998年7月。ISBN
    9570048611
4.  文、圖／Tony，"台北／三貂角燈塔
    享受浪漫異國風情"[3](http://travel.udn.com/mag/travel/storypage.jsp?f_ART_ID=31001)，[聯合新聞網](../Page/聯合新聞網.md "wikilink")，2009/10/02.

## 外部連結

  - [三貂角燈塔](http://wshnt.kuas.edu.tw/sundayasp2/g4/new_page_27.htm)
  - [交通部航港局-燈塔相關資訊-三貂角燈塔](https://web.archive.org/web/20141230082956/http://www.motcmpb.gov.tw/MOTCMPBWeb/wSite/ct?xItem=4310&ctNode=260&mp=1)
  - [【台灣，你好！】沙發環島空拍鷹眼系列 - 2015/7/24 台北貢寮三貂角燈塔
    卷一](https://www.youtube.com/watch?v=0DqxJMMS_G4&index=20&list=PLOEs_R7Am6v_OgFukirZQUDsf2mpKuF3K/)

[Category:台灣燈塔](../Category/台灣燈塔.md "wikilink")
[Category:新北市旅遊景點](../Category/新北市旅遊景點.md "wikilink")
[Category:貢寮區](../Category/貢寮區.md "wikilink")
[Category:1935年台灣建立](../Category/1935年台灣建立.md "wikilink")

1.
2.  記者王以瑾／台北報導．攝影,"名為教堂但不是教堂　三貂角教堂只是感覺"[1](http://www.nownews.com/2011/05/18/153-2713343.htm),[NOWnews](../Page/NOWnews.md "wikilink")/今日新聞網,2011年5月18日
    09:26.
3.  [2](http://www.pse100i.idv.tw/n/sntencnn/sntencnn008.jpg)