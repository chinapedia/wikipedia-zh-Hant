**三越**（）是株式會社[三越伊勢丹控股旗下的](../Page/三越伊勢丹控股.md "wikilink")**株式會社[三越伊勢丹](../Page/三越伊勢丹.md "wikilink")**所營運的連鎖[日本百貨店](../Page/日本百貨公司.md "wikilink")。原始營運公司為**株式會社三越**。

1.  初代（1928年 - 2003年8月31日）
2.  第2代（2003年9月1日 - 2011年3月31日） -
    由1與株式會社名古屋三越、株式會社千葉三越、株式會社鹿兒島三越、株式會社福岡三越合併成立。現在株式會社三越伊勢丹的前身。

[商號的](../Page/商號.md "wikilink")「三越」是取自的「**三**井」與創業時的名稱「**越**後屋」，並在1904年將「合名會社三井吳服店」改為「株式會社三越吳服店」時正式成為商號。1935年竣工的[日本橋本店是國家](../Page/日本橋_\(東京都中央區\).md "wikilink")[重要文化財](../Page/重要文化財.md "wikilink")\[1\]\[2\]。[東京地下鐵](../Page/東京地下鐵.md "wikilink")[銀座線的車站](../Page/東京地下鐵銀座線.md "wikilink")[三越前便是以日本橋本店命名](../Page/三越前站.md "wikilink")。\[3\]

## 概要

[江戶時代的](../Page/江戶時代.md "wikilink")1673年（[延寶元年](../Page/延寶.md "wikilink")），以「店前現金交易」（）、「現金交易不二價」（）、「無論布長皆可銷售」（）等創新商法打出名號的吳服店「越後屋」（ゑちごや）創業。越後屋推出的標價出售使得進一般市民能開始接觸到原來只在富裕階層流行的吳服。1928年，成立「株式會社三越」。

「三越」在更名時同時提出「[百貨公司宣言](../Page/百貨公司.md "wikilink")」。這項宣言常被視為是日本百貨店的起源\[4\]。

2003年（平成15年）9月1日，當時的「株式會社三越」與其子公司「株式會社名古屋三越」、「株式會社千葉三越」、「株式會社鹿兒島三越」、「株式會社福岡三越」合併成立新的「株式會社三越」。

當時在營業利益率方面，集團連結僅**1.09%**，百貨店事業僅**0.799%**，加上百貨店業界持續低迷，2008年9月宣布結束四間百貨店與兩間零售店的營運。
之後根據經營整合後的重整方針，2010年4月1日將關東以外的店舖獨立分出。

剩餘店鋪於2011年4月1日與[伊勢丹合併成立](../Page/伊勢丹.md "wikilink")「**株式會社三越伊勢丹**」。同日，與兩公司合併成立「**株式會社**」。

## 沿革

[えちご屋広告チラシ.JPG](https://zh.wikipedia.org/wiki/File:えちご屋広告チラシ.JPG "fig:えちご屋広告チラシ.JPG")
[Echigo-Ya_Signboard.JPG](https://zh.wikipedia.org/wiki/File:Echigo-Ya_Signboard.JPG "fig:Echigo-Ya_Signboard.JPG")
[Hiroshige,_Sugura_street.jpg](https://zh.wikipedia.org/wiki/File:Hiroshige,_Sugura_street.jpg "fig:Hiroshige,_Sugura_street.jpg")）
可看到越後屋的暖簾。\]\]
[Ousuke_Hibi.jpg](https://zh.wikipedia.org/wiki/File:Ousuke_Hibi.jpg "fig:Ousuke_Hibi.jpg")

三越始於[江戶時代](../Page/江戶時代.md "wikilink")，一間本著「不會抬高售價」的營商手法而馳名於世的日式和服商店「越後屋」。1904年成為了當時日本第一間百貨店，之後便開始以百貨店的形式繼續經營。位於日本橋的總店建於1935年，被視為一所古舊建築。不過雖則以百貨店來說是日本的第一家，但若以前身開始算起，則應以[松坂屋最為古老](../Page/松坂屋.md "wikilink")。

### 越後屋、三井吳服店時代

  - 1673年 -
    由、[三井家的](../Page/三井家.md "wikilink")於當時的[江戶本町一丁目](../Page/日本橋本石町.md "wikilink")（現在的[日本銀行旁](../Page/日本銀行.md "wikilink")）設立「越後屋」日式和服（吳服）店。
  - 1683年 -
    因大火將店鋪從本町遷移到[駿河町](../Page/日本橋室町.md "wikilink")，同時設立一所換幣店（現在的[三井住友銀行](../Page/三井住友銀行.md "wikilink")）。
  - 1691年 - 位於[大阪府](../Page/大阪府.md "wikilink")一丁目的越後屋大阪店與換幣店開幕。
  - 1837年 - 大阪店因[大鹽平八郎之亂而遭受襲擊](../Page/大鹽平八郎之亂.md "wikilink")，全店焚毀。
  - 1872年 - 正式從三井大元方（統籌三井家族事業的機構）分離，以新創立的「**三越家**」名義繼續經營\[5\]。
  - 1875年 - 大阪分店遷移到高麗橋三丁目，規模縮小。
  - 1888年 - 駿河町的「**三越洋服店**」開店。
  - 1893年 - 「越後屋」改稱「**三井吳服店聯名公司**」（）
  - 1894年 - 大阪分店於高麗橋一丁目的堺筋重新開幕。
  - 1895年 - 就任理事。開始進行經營改革。11月廢止帳房櫃檯式物品銷售。
  - 1900年 - 櫃檯式物品銷售全部廢除。原先的銷售櫃檯改為陳列場，全館開幕。開店後因來客太多須暫時關閉大門。錄用女子社員。

### 株式会社三越吳服店時代

  - 1904年 - 「**株式會社三越吳服店**」成立。****就任初代專務。
  - 1905年 - 主要新聞與雜誌等刊載「百貨公司宣言」。
      - 日本首次導入（交貨用）汽車。
      - 店頭設置大型點燈裝飾。
      - 開始販售化妝品、帽子。
      - 舉行「光琳遺品展覧會」。
  - 1907年 - 日本橋本店內開設食堂與相片室。
      - 增加包、鞋、洋傘等商品。
      - 在越後屋舊址開設大阪店
      - 開設「新美術部」（大阪店9/15、日本橋店12/1）。
  - 1909年 - 組成少年音樂隊、Messenger Boy。
  - 1911年 - 公開招募海報圖案。
      - 照片室使用彩色照片。
      - 開始接受電話訂單。
      - 擁有木造30公尺裝飾窗的大阪店兩層樓新館落成。
  - 1913年 -
    [帝國劇場的宣傳冊廣告](../Page/帝國劇場.md "wikilink")「今天去帝劇，明天去三越」（）成為流行句。
  - 1914年 -
    日本橋本店文藝復興式新館落成。鋼筋地上5層、地下1層的新館被稱為「[蘇伊士運河以東最大建築](../Page/蘇伊士運河.md "wikilink")」。
      - 店內裝設日本首座電扶梯，與電梯、自動灑水設備、全館暖氣等最新設備。
      - 屋頂庭園、茶室、音樂堂、正面玄關設置「獅子像」等。
      - 舉行「第一回再興院展」。
  - 1916年 - 金字塔插上天気預報信號旗。設置望遠鏡。
  - 1917年 - 大阪店新館開店，地下1層地上7層的新館成為大阪最大的文藝復興式建物。
      - 日本橋本店中央大廳設置巨大風扇。
  - 1920年 - 大阪店東館完成，全館開店。
  - 1921年 - 大阪店成為西日本首間廢止脫鞋的百貨店。
  - 1923年8月5日 - 日本橋店推出日本首次百貨店大減價特賣。
      - [關東大地震造成日本橋本店與丸之內別館火災](../Page/關東大地震.md "wikilink")。
  - 1925年 -
    大阪店天台的大阪放送局（現[NHK大阪放送局](../Page/NHK大阪放送局.md "wikilink")）開始播出。
  - 1927年 - 日本橋本店開設三越會館（現三越劇場）。
      - 舉行日本首場時裝秀。設置美容室。

### （初代）株式會社三越時代

[View_of_Ginza_in_1930s.jpg](https://zh.wikipedia.org/wiki/File:View_of_Ginza_in_1930s.jpg "fig:View_of_Ginza_in_1930s.jpg")）與三越銀座店（1933年10月）\]\]

  - 1928年 - 「三越吳服店」改稱「**三越**」。
  - 1929年 - 新宿店開店。
  - 1930年 - 銀座店開店。
      - 本店食堂推出「兒童洋食」。
      - 京城支店開店。
  - 1932年 -
    由三越負責建設資金的[東京地鐵](../Page/東京地鐵.md "wikilink")[三越前站開幕](../Page/三越前站.md "wikilink")。同年，位於[北海道的](../Page/北海道.md "wikilink")[札幌分店亦宣佈開幕](../Page/札幌.md "wikilink")。
  - 1933年 - 仙台店開店。
  - 1935年 - 日本橋總店的擴建工程完工，改為樓高七層及設有兩層地庫，中央大堂裡更放置了一座管風琴。
  - 1937年 - 大阪店改建工程完工，新設新式冷氣。
  - 1944年 - 本店開設結婚式場。
  - 1946年 - 戰後首間新店松山店開店。
  - 1947年 - 財團法人三越診療所（現三越厚生事業團）成立。
  - 1950年 - 日本橋本店舉行戰後首次時裝秀。同年舉行《麥克阿瑟元帥胸像》揭幕式。
  - 1951年 -
    全部分店均開始使用全新由所繪畫的「花開」（華ひらく）包裝紙。包裝紙上的「mitsukoshi」字樣則出自當時宣傳部[柳瀨嵩的手筆](../Page/柳瀨嵩.md "wikilink")。
      - 12月18日 - 日本百貨店首次[罷工](../Page/罷工.md "wikilink")\[6\]\[7\]。
  - 1954年 - 舉行「第一回無形文化財日本傳統工藝展」。
  - 1956年 - 日本橋本店舉行巴黎展。
  - 1958年 - 日本橋本店第2期增改建工程完成，店面規模為全國第一。
  - 1960年4月19日 - 三越創立50周年紀念，日本橋本店中央大廳設置製作的「天女像（まごころ）」。
  - 1967年 - 高松店改建，暫時關閉。
  - 1968年 -
    銀座店新開店。位於[大阪府東北部的](../Page/大阪府.md "wikilink")[枚方分店開幕](../Page/枚方市.md "wikilink")。高松店恢復營業。
  - 1970年 - 入主「大越百貨店」集團，公司名改為「」。
  - 1971年 - 成為日本零售業首間營收突破1,000億日圓的公司。
      - 海外第一間分店巴黎三越開店。
      - 銀座店內開設[麥當勞日本](../Page/麥當勞.md "wikilink")1號店。
      - 札幌店新開幕。
      - 與「奈良屋」（千葉）合資成立「新奈良屋」（ニューナラヤ）。
      - 與「丸屋」（鹿兒島）業務合作。
  - 1973年 - 廣島店開店。
  - 1974年 - 大阪分店新館落成。
  - 1975年 - 與[名古屋市的](../Page/名古屋市.md "wikilink")「東方中村百貨店」（1954年開業）業務合作。
      - 羅馬三越開店。
  - 1976年 - 大阪店全館改建重新開幕。
  - 1978年 - 與「小林百貨店」（新潟）業務合作。
  - 1979年 - 倫敦三越、杜塞爾多夫三越、紐約三越、夏威夷三越開店。
  - 1980年 - 倉敷店開店。「東方中村百貨店」改稱「名古屋三越」，「小林百貨店」改稱「新潟三越」。（及後跟名古屋三越合併）。
  - 1981年 - 香港三越開店、法蘭克福三越、陽光城三越開店。
  - 1982年6月17日 -
    爆發，要求供應商在商品及電影發售或上映前購入契約（強制發售）、提供資助、要求派遺員工、以及支付各種應酬娛樂費用等等，被裁定觸犯禁止壟斷法第19條的不公正營商手法（濫用優越地位）。
      - 同年8月28日 - 被發現在「古代波斯寶物展」中的展品超過一半均為贋品。
      - 同年9月22日 -
        當時的主席涉嫌向其情婦竹久美智所經營的「竹久飾物」給予不當利益，及動用公司的資金去支付其家中的裝修費用等等，被董事會動議撤消其主席及董事職務。董事會結果以16比0裁定撤消岡田的職務，岡田被撤消職務後的一句「為什麼？」成為了當時的流行語。後天，岡田與竹久二人因涉疑持有19億日圓的不明來歷資產而被拘捕。雖則分別向地方法院及高等法院提出上訴，但最終依然被裁定有罪。岡田的審訊因岡田於1995年7月20日死亡而終結。至於竹久的審訊則繼續，直至她於1997年10月放棄提出上訴為止。最終竹久被判入獄兩年六個月，以及罰款六千萬日圓。
  - 1984年2月 -
    [神戶分店關閉](../Page/神戶.md "wikilink")，取而代之的是位於神戶元町的小型商店。（已經於2004年1月關閉。）
      - 同年10月 - 鹿兒島縣的「丸屋」改稱「鹿兒島三越」。
  - 1995年 - 大阪分店因[阪神大地震而損毀](../Page/阪神大地震.md "wikilink")，舊館倒塌。
  - 1997年 -
    隨著再開發[西鐵的](../Page/西日本鐵道.md "wikilink")[福岡站](../Page/西鐵福岡（天神）車站.md "wikilink")，福岡三越開幕。
  - 1999年 -
    將位於[新宿區的](../Page/新宿區.md "wikilink")「新宿三越南館」售予[大塚家具](../Page/大塚家具.md "wikilink")。該分店在開業初期因最頂層設有一個[美術館曾一時聲名大噪](../Page/美術館.md "wikilink")，但熱潮過後營業額持續不振。再加上並沒有和總店有連繫，結果結束其八年間的營業。
  - 2000年 -
    位於東京都[多摩市的多摩中心分店開幕](../Page/多摩市.md "wikilink")。（原址前身為[崇光百貨](../Page/崇光百貨.md "wikilink")）
  - 2001年 -
    東京吉祥寺分店開幕。（原址前身為[近鐵百貨](../Page/近鐵百貨.md "wikilink")，但只使用1,2及B1層。）

### （第2代）株式會社三越時代

[Mitsukoshi_Nihonbashi_Atrium_201505.jpg](https://zh.wikipedia.org/wiki/File:Mitsukoshi_Nihonbashi_Atrium_201505.jpg "fig:Mitsukoshi_Nihonbashi_Atrium_201505.jpg")
[Mitsukoshi_Nihonbashi_main_store.jpg](https://zh.wikipedia.org/wiki/File:Mitsukoshi_Nihonbashi_main_store.jpg "fig:Mitsukoshi_Nihonbashi_main_store.jpg")

  - 2003年 -
    三越、名古屋三越、千葉三越、福岡三越、鹿兒島三越合併，成為「**三越株式會社**」（<font lang=ja>株式-{会}-社三越</font>）
  - 2004年10月 - 「百貨公司宣言」100週年。同時在日本橋總店旁的新．新館落成。
  - 2005年2月 -
    位於[函館](../Page/函館.md "wikilink")、[羽田機場](../Page/羽田機場.md "wikilink")、[洗足](../Page/洗足.md "wikilink")、[三田](../Page/三田.md "wikilink")、[小豆島](../Page/小豆島.md "wikilink")、[枕崎的分店關閉](../Page/枕崎.md "wikilink")。
      - 同年3月 -
        設於名古屋榮本分店的專門店館「LACHIC」開幕。另外設於新宿三越，專門售賣雜貨為主「ALCOTT」亦同時開幕。（重新裝修於2004年完成）
      - 同年5月 - 大阪分店、橫濱分店、倉敷分店、枚方分店（小型商店）關閉。
  - 2006年5月7日 - 吉祥寺分店關閉。
  - 2008年（平成20年）4月1日新公司[三越伊勢丹控股成立](../Page/三越伊勢丹控股.md "wikilink")，按一股[伊勢丹換一股三越伊勢丹控股](../Page/伊勢丹.md "wikilink")；一股三越可換三股三越伊勢丹控股，並使三越及伊勢丹成為三越伊勢丹控股全資附屬公司。\[8\]
  - 2010年4月1日 - 地方直營店大半獨立成立分公司。
  - 2011年4月1日 - 營運公司株式會社三越與株式會社伊勢丹合併成立「**株式會社三越伊勢丹**」。
  - 2016年7月25日 -
    三越日本橋本店建物指定為國家[重要文化財](../Page/重要文化財.md "wikilink")\[9\]。
  - 2017年3月20日 - 三越千葉店、多摩中心店關店\[10\]\[11\]\[12\]\[13\]。

## 現時國內分店

  - 日本橋總店（東京都中央區日本橋室町）
  - [銀座分店](../Page/銀座.md "wikilink")（東京都中央區）
  - [惠比壽
    (東京都)分店](../Page/惠比壽_\(東京都\).md "wikilink")（東京都[澀谷區](../Page/澀谷區.md "wikilink")）
  - [多摩中心分店](../Page/多摩中心.md "wikilink")（東京都[多摩市](../Page/多摩市.md "wikilink")）
  - 札幌分店（[札幌市中央區](../Page/札幌市.md "wikilink")）
  - 仙台分店（[仙台市青葉區](../Page/仙台市.md "wikilink")）
  - 新潟分店（[新潟市西堀通](../Page/新潟市.md "wikilink")）
  - 名古屋榮本分店（[名古屋市中區](../Page/名古屋市.md "wikilink")）
      - LACHIC（名古屋市中區）
  - 星丘分店（名古屋市千種區）
  - 廣島分店（[廣島市中區](../Page/廣島市.md "wikilink")）
  - 高松分店（[高松市](../Page/高松市.md "wikilink")）
  - 松山分店（[松山市](../Page/松山市.md "wikilink")）
  - 福岡分店（[福岡市中央區](../Page/福岡市.md "wikilink")）

### 小型商店

多於中小型都市或機場營業，主要售賣衣服及禮品等商品。

  - [北海道](../Page/北海道.md "wikilink") -
    苫小牧、新千歲機場、[帶廣](../Page/帶廣市.md "wikilink")、釧路
  - [岩手縣](../Page/岩手縣.md "wikilink") - 盛岡
  - [秋田縣](../Page/秋田縣.md "wikilink") - 秋田
  - [宮城縣](../Page/宮城縣.md "wikilink") - 石卷、大河原
  - [福島縣](../Page/福島縣.md "wikilink") - 岩木
  - [新潟縣](../Page/新潟縣.md "wikilink") - 柏崎
  - [茨城縣](../Page/茨城縣.md "wikilink") - 牛久、古河、水戶、石岡、鹿島（神栖市）
  - [櫪木縣](../Page/枥木县.md "wikilink") - 櫪木、小山
  - [群馬縣](../Page/群馬縣.md "wikilink") - 高崎、伊勢崎、館林、桐生
  - [埼玉縣](../Page/埼玉縣.md "wikilink") - 川越、新所澤、飯能、熊谷、春日部、深谷、浦和（埼玉）、久喜
  - [千葉縣](../Page/千葉縣.md "wikilink") -
    成田、茂原、旭、東金、五井（市原）、臼井（佐倉）、四街道、館山、尤嘉利丘（佐倉）
  - [東京都](../Page/東京都.md "wikilink") -
    馬事公苑（[世田谷區](../Page/世田谷區.md "wikilink")）、昭島
  - [神奈川縣](../Page/神奈川縣.md "wikilink") - 厚木、秦野、鎌倉、小田原
  - [岐阜縣](../Page/岐阜縣.md "wikilink") - 高山
  - [兵庫縣](../Page/兵庫縣.md "wikilink") - 神戶元町、三田
  - [廣島縣](../Page/廣島縣.md "wikilink") - 吳、東廣島
  - [香川縣](../Page/香川縣.md "wikilink") - 高松機場、坂出、丸龜、觀音寺、三本松（東香川）
  - [愛媛縣](../Page/愛媛縣.md "wikilink") -
    西條、今治、宇和島、伊予三島（[四國中央](../Page/四國.md "wikilink")）
  - [福岡縣](../Page/福岡縣.md "wikilink") - 福岡

## 海外分店

  - [罗马](../Page/罗马.md "wikilink")
  - [上海](../Page/上海.md "wikilink")
  - [台灣](../Page/台灣.md "wikilink")
  - [奥兰多](../Page/奥兰多_\(佛罗里达州\).md "wikilink")\[14\]

此外，在[二戰期間](../Page/二戰.md "wikilink")，三越百貨也曾經在中國[大連和](../Page/大連.md "wikilink")[韓國](../Page/韓國.md "wikilink")[首爾開設過分店](../Page/首爾.md "wikilink")，但隨後隨著日本戰敗而相繼結業或轉手。

### 香港

三越百貨曾在[香港](../Page/香港.md "wikilink")[銅鑼灣和](../Page/銅鑼灣.md "wikilink")[尖沙嘴開設分店](../Page/尖沙嘴.md "wikilink")，於2006年撤出香港。\[15\]

### 臺灣

[ShinKongMitsukoshi_TaipeiStation.jpg](https://zh.wikipedia.org/wiki/File:ShinKongMitsukoshi_TaipeiStation.jpg "fig:ShinKongMitsukoshi_TaipeiStation.jpg")

臺灣的[新光三越百貨是由](../Page/新光三越百貨.md "wikilink")[新光集團與三越百貨在](../Page/新光集團.md "wikilink")1989年共同合資成立，旗下分店有：[臺北南西店](../Page/新光三越百貨#現有分店.md "wikilink")、[臺北站前店](../Page/新光三越百貨#現有分店.md "wikilink")、[臺北信義新天地](../Page/新光三越百貨#現有分店.md "wikilink")、[臺北天母店](../Page/新光三越百貨#現有分店.md "wikilink")、[桃園大有店](../Page/新光三越百貨#現有分店.md "wikilink")、[桃園站前店](../Page/新光三越百貨#現有分店.md "wikilink")、[新竹店](../Page/新光三越百貨#現有分店.md "wikilink")、[臺中店](../Page/新光三越百貨#現有分店.md "wikilink")、[嘉義店](../Page/新光三越百貨#現有分店.md "wikilink")、[臺南中山店](../Page/新光三越百貨#現有分店.md "wikilink")、[臺南新天地](../Page/新光三越百貨#現有分店.md "wikilink")、[高雄三多店以及](../Page/新光三越百貨#現有分店.md "wikilink")[高雄左營店等](../Page/新光三越百貨#現有分店.md "wikilink")。

### 意大利罗马

罗马三越（Mitsukoshi Roma）位于意大利首都罗马，地址：Via Nazionale,259 Rome Italy 邮编：00184。
商店于1975年6月10号开业至今，目前是三越百货在欧洲的唯一分店。
官网（www.mitsukoshi.it）目前有意大利语，中文，英文和日语四种语言。

### 美國奧蘭多

[奧蘭多三越位於美國](../Page/奧蘭多.md "wikilink")[華特迪士尼世界度假區](../Page/華特迪士尼世界度假區.md "wikilink")[Epcot園區當中的日本館](../Page/Epcot.md "wikilink")，目前為三越在美洲的唯一分店。

### 中國

#### 上海

上海三越位于茂名南路58号，由日本三越直接经营，于1989年10月15日软开业并在1990年3月20日正式开业其后进行了多次扩建。

## 資料來源

<references/>

## 外部連結

  - [日本三越](http://www.mitsukoshi.co.jp/)
  - [意大利罗马三越](http://www.mitsukoshi.it/)
  - [美國奧蘭多迪士尼三越](http://www.mitsukoshi-orlando.com/)
  - [上海三越](http://www.mitsukoshi.cn/)
  - [台灣新光三越](http://www.skm.com.tw/)

[Category:日本百貨公司](../Category/日本百貨公司.md "wikilink")
[Category:中國百貨公司](../Category/中國百貨公司.md "wikilink")
[Category:總部在日本的跨國公司](../Category/總部在日本的跨國公司.md "wikilink")
[Category:日本橋 (東京都中央區)](../Category/日本橋_\(東京都中央區\).md "wikilink")

1.  平成28年7月25日文部科学省告示第102号。
2.  [重要文化財（建造物）の指定について](http://www.bunka.go.jp/koho_hodo_oshirase/hodohappyo/2016052002.html)（文化庁サイト）
3.
4.  1904年12月20日顧客らに送った書状。のちに「デパートメントストア宣言」と呼ばれるこの文書「米国に行はるるデパートメント・ストーアの一部を実現致すべく候」翌日の12月21日、三越呉服店は日本初のデパートとして営業を開始した。
5.  [小学館
    日本大百科全書(ニッポニカ)-越後屋](https://kotobank.jp/word/%E8%B6%8A%E5%BE%8C%E5%B1%8B-36710)
6.  [日本労働年鑑第25集「三越の争議」](http://oohara.mt.tama.hosei.ac.jp/rn/25/rn1953-275.html)
     [法政大学大原社会問題研究所](../Page/法政大学大原社会問題研究所.md "wikilink")
7.  [昭和26年12月18日　三越でスト　百貨店で初](http://showa.mainichi.jp/comeback/2009/12/post-b21d.html)
    - 昭和毎日
8.
9.  平成28年7月25日文部科学省告示第102号。
10.
11.
12.
13. [三越伊勢丹　千葉、多摩店を閉店　労組と対話重視　改革難航も](http://www.sankeibiz.jp/business/news/170321/bsd1703210500001-n1.htm)
    SANKEI Biz（2017年3月21日）2017年3月21日閲覧
14.
15.