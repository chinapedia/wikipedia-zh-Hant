**上元**（674年八月－676年十一月）是[唐高宗李治的年号](../Page/唐高宗.md "wikilink")。[唐朝使用这个年号共](../Page/唐朝.md "wikilink")2年餘。

## 大事记

## 出生

## 逝世

  - [上元二年](../Page/675年.md "wikilink")——[李弘](../Page/李弘.md "wikilink")，皇太子
  - [上元三年](../Page/676年.md "wikilink")——[王勃](../Page/王勃.md "wikilink")，唐朝诗人

## 纪年

| 上元                               | 元年                             | 二年                             | 三年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 674年                           | 675年                           | 676年                           |
| [干支](../Page/干支纪年.md "wikilink") | [甲戌](../Page/甲戌.md "wikilink") | [乙亥](../Page/乙亥.md "wikilink") | [丙子](../Page/丙子.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他使用[上元年號的政權](../Page/上元.md "wikilink")
  - 同期存在的其他政权年号

## 參考文獻

  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7-101-02512-9

[Category:唐朝年号](../Category/唐朝年号.md "wikilink")
[Category:7世纪中国年号](../Category/7世纪中国年号.md "wikilink")
[Category:670年代中国政治](../Category/670年代中国政治.md "wikilink")