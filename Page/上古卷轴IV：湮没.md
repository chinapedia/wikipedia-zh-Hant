是一款[奇幻类](../Page/奇幻.md "wikilink")[开放世界型](../Page/开放世界.md "wikilink")[动作角色扮演游戏](../Page/动作角色扮演游戏.md "wikilink")。它由[贝塞斯达软件公司开发](../Page/贝塞斯达软件公司.md "wikilink")、贝塞斯达和[2K
Games联合发行于](../Page/2K_Games.md "wikilink")2006年，是[上古卷轴系列的第四代作品](../Page/上古卷轴系列.md "wikilink")。游戏有[Windows](../Page/Windows.md "wikilink")、[Xbox
360](../Page/Xbox_360.md "wikilink")、[PlayStation
3等多个平台的版本](../Page/PlayStation_3.md "wikilink")。\[1\]游戏的资料片《上古卷轴IV：战栗孤岛》以及一系列升级包随之发布。游戏赢得了众多评论员的好评，获得许多奖项。\[2\]并在[Metacritic得到了](../Page/Metacritic.md "wikilink")94分的高分。\[3\]截至到2006年4月10日，《湮没》已销售出170万份，\[4\]2007年1月18日，销量超过300万份。\[5\]《上古卷轴IV：年度版》（含“战栗孤岛”和升级包“九骑士”）于2007年9月10日发布。\[6\]\[7\]

## 遊戲性

本作保留了上古卷軸系列的無縫接軌的遊戲方式，可以以第一人稱視角或第三人稱視角來進行遊戲，自由的遊戲方式可以使玩家不僅可以騎馬來代步，甚至已探索的地點都可以藉由快速旅行來快速移動到目的地。此外前作沒有的任務追蹤功能以及任務日誌也在本作中出現，降低玩家在搜索任務地的困難與整理資訊的麻煩。
而本遊戲具有創新的AI系統“Radiant
AI”，這種系統可以使NPC們具有根據周圍事物做出智慧判斷的能力，他們可以決定自己在哪里尋得食物，與誰談話，談話的內容是什麼。他們還可以睡覺，去教堂禮拜，甚至偷東西，這都由他們獨特的性格決定。遊戲的AI系統在角色扮演遊戲中也可以說是一個突破。

與前作相同，玩家在一開始就要決定你的種族與姓名等基本特徵，而最初的遊戲任務劇情中引導玩家的遊戲教學後可以決定你的主要技能與屬性點，當提昇主要技能時可以有效提昇你的等級。
有別於其他類似的遊戲，玩家可以對大部分的友善NPC對話，可以藉由成功的口才表演來增加NPC的友好度以及口才等級，進行交易買賣提昇商業技巧，成功撬開鎖來提昇開鎖等級，跳躍提昇雜技等級或是跑步游泳提昇運動等級等...並不一定要進行戰鬥才能提昇自己的技能，而這些其中一項若是主要技能更是能快速提昇角色等級。

## 劇情概要

故事依然發生在泰姆瑞尔（Tamriel）世界，這次是在帝國首都所在的赛洛迪尔（Cyrodiil）行省。玩家一開始是因不明原因入獄的囚犯，偶然捲入了皇帝尤里爾·賽普丁七世（Uriel
Septim
VII）被追殺的事件，玩家成為了皇帝在逃亡期間最信任的人，並將皇權的象徵：帝王護符交給玩家，但皇帝隨即就被不知身份的刺客給刺殺。由於沒有了皇帝，通往湮沒世界（Oblivion，等同於Tamriel世界的地獄）的大門被開啟，湮沒世界的魔鬼開始入侵赛洛迪尔，屠殺人民。因此玩家必須找到王位繼承人來拯救世界。

## 参考文献

## 外部链接

  - [湮没概述](https://web.archive.org/web/20071119075432/http://www.elderscrolls.com/games/oblivion_overview.htm)
    — 官方网站
  - [OblivioWiki](http://oblivion.gamewikis.org/wiki/Main_Page) —
    一个湮没的[wiki](../Page/wiki.md "wikilink")。
  - [UESPWiki](http://www.uesp.net/wiki/Main_Page) - 一個獨立的湮没wiki。

[Category:2006年电子游戏](../Category/2006年电子游戏.md "wikilink")
[Category:PlayStation 3游戏](../Category/PlayStation_3游戏.md "wikilink")
[Category:電子角色扮演遊戲](../Category/電子角色扮演遊戲.md "wikilink")
[4](../Category/上古卷轴系列.md "wikilink")
[Category:Windows游戏](../Category/Windows游戏.md "wikilink")
[Category:Xbox 360游戏](../Category/Xbox_360游戏.md "wikilink")
[Category:Gamebryo引擎游戏](../Category/Gamebryo引擎游戏.md "wikilink")
[Category:使用Havok的電子遊戲](../Category/使用Havok的電子遊戲.md "wikilink")
[Category:僅有單人模式的電子遊戲](../Category/僅有單人模式的電子遊戲.md "wikilink")
[Category:可選主角性別遊戲](../Category/可選主角性別遊戲.md "wikilink")

1.
2.
3.
4.
5.
6.
7.