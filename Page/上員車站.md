**上員車站**位於[台灣](../Page/台灣.md "wikilink")[新竹縣](../Page/新竹縣.md "wikilink")[竹東鎮](../Page/竹東鎮.md "wikilink")，為[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[內灣線的](../Page/內灣線.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。

## 車站構造

  - [岸式月台一座](../Page/岸式月台.md "wikilink")。

## 營運狀況

  - 目前為[招呼站](../Page/臺灣鐵路管理局車站等級#招呼站.md "wikilink")。

<!-- end list -->

  - 內灣線在2007年3月1日因[新竹](../Page/新竹車站.md "wikilink")－[竹東段配合](../Page/竹東車站.md "wikilink")[六家線興建與內灣線改善工程而暫時停駛](../Page/六家線.md "wikilink")，於2011年11月11日復駛。
  - 本站開放使用[悠遊卡](../Page/悠遊卡.md "wikilink")、[一卡通及](../Page/一卡通_\(台灣\).md "wikilink")[icash
    2.0付費](../Page/icash.md "wikilink")。

| 年度   | 日均進站旅次（人） |
| ---- | --------- |
| 1998 | 12        |
| 1999 | 15        |
| 2000 | 20        |
| 2001 | 69        |
| 2002 | 99        |
| 2003 | 104       |
| 2004 | 120       |
| 2005 | 126       |
| 2006 | 124       |
| 2007 | 121       |
| 2008 | 暫停營運      |
| 2009 | 暫停營運      |
| 2010 | 暫停營運      |
| 2011 | 43        |
| 2012 | 62        |
| 2013 | 64        |
| 2014 | 70        |
| 2015 | 68        |
| 2016 | 62        |

歷年旅客人次紀錄

## 歷史

  - 1970年2月6日：設站。
  - 2007年3月1日：隨著內灣線新竹－竹東段停駛而暫停營運。
  - 2011年11月11日：隨著工程完工而復駛。
  - 2013年9月30日：啟用多卡通刷卡機。

## 車站週邊

  - [工業技術研究院](../Page/工業技術研究院.md "wikilink")（中興院區）

  -
## 公車資訊

  - [新竹客運](../Page/新竹客運.md "wikilink")：
      - **<font color="red">5670</font>** 竹東－員山路－竹中口

## 圖片

<File:TRA> Shangyuan Station.jpg|上員車站
[File:Taiwan_ShangYuan_Railway_Station.JPG|上員車站現今外觀](File:Taiwan_ShangYuan_Railway_Station.JPG%7C上員車站現今外觀)
[File:Taiwan_ShangYuan_Railway_Station_3.JPG|上員車站近景](File:Taiwan_ShangYuan_Railway_Station_3.JPG%7C上員車站近景)
<File:Taiwan_ShangYuan_Railway_Station_2.JPG>|[冷氣柴客停靠上員車站](../Page/冷氣柴客.md "wikilink")

## 鄰近車站

## 參考文獻

## 外部連結

  - [內灣線站牌
    榮華、竹中方向搞錯](https://web.archive.org/web/20160316214009/https://tw.news.yahoo.com/%E5%85%A7%E7%81%A3%E7%B7%9A%E7%AB%99%E7%89%8C-%E6%A6%AE%E8%8F%AF-%E7%AB%B9%E4%B8%AD%E6%96%B9%E5%90%91%E6%90%9E%E9%8C%AF-203238859.html%3B_ylt%3DAuCnJmalzG8cySkzEFxpv8rVBdF_%3B_ylu%3DX3oDMTQ2amsyaTVoBG1pdANUb3BTdG9yeSBMaWZlc3R5bGVTRiBMaWZlU1NGBHBrZwM0ZmExODMyOS03N2MxLTMwM2ItYjI1Zi00NTc5ZjRiZDcwZmIEcG9zAzcEc2VjA3RvcF9zdG9yeQR2ZXIDY2RkNGFlZDAtMDU5MS0xMWUxLTlmYmYtYjdhMWEzNWU5OGVi%3B_ylg%3DX3oDMTIwN2hybzQ2BGludGwDdHcEbGFuZwN6aC1oYW50LXR3BHBzdGFpZAMEcHN0Y2F0A.eUn.a0u3zntpzlkIgEcHQDc2VjdGlvbnM-%3B_ylv%3D3)

[Category:1970年启用的铁路车站](../Category/1970年启用的铁路车站.md "wikilink")
[Category:竹東鎮鐵路車站](../Category/竹東鎮鐵路車站.md "wikilink")
[Category:內灣線車站](../Category/內灣線車站.md "wikilink")