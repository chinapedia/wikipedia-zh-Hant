**上官小強**（），原名**陳國賢**。[香港著名](../Page/香港.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")。16歲當漫畫學徒，在六十年代時已開始編繪[漫畫](../Page/漫畫.md "wikilink")，而1973年創刊的「[壽星仔](../Page/壽星仔.md "wikilink")」，就令他廣受讀者們的接受和歡迎。「今日留一線，他日好相見。」這個就是上官小強處世的座右銘。如果說[黃國興和](../Page/黃國興.md "wikilink")[甘小文是漫畫笑匠](../Page/甘小文.md "wikilink")，那麼比他倆更早出道的上官小強就是惹笑漫畫的一代宗師。

上官小強於六十年代時已開始編繪惹笑漫畫，其中以「地球保衛戰」、「小木偶」和「足球小將」較為人所熟識。當時他所用的筆名确實不少，包括「陳小強」、「凌霄」和「狄龍」。

七十年代初，他客串為上官小寶的「[73漫畫](../Page/73漫畫.md "wikilink")」寫散稿，「壽星仔」就是在這段時期誕生的。上官小強曾承認「壽星仔」是深受西洋漫畫「[史諾比](../Page/史諾比.md "wikilink")」(Snoopy)的影響，內容風趣幽默兼且夠通俗；後來黃國興的「[蟋蟀仔](../Page/蟋蟀仔.md "wikilink")」也深受它的風格所影響。

1975年，上官小強結束與上官家族的合作，跳到「[玉郎機構](../Page/玉郎機構.md "wikilink")」旗下幹活；而「壽星仔」的受歡迎程度卻是有增無減。

十三年後的1989年中，上官小強在「玉郎機構」壟斷崩潰時脫離公司，另立山頭。他順利地成立了「[小強出版社](../Page/小強出版社.md "wikilink")」和「[好老友製作室](../Page/好老友製作室.md "wikilink")」。早年受過上官小強提拔的漫畫人，例如[邱福龍等](../Page/邱福龍.md "wikilink")，在他自立門戶時都有拔刀相助；「[小強畫集](../Page/小強畫集.md "wikilink")」和「[四格漫畫大全](../Page/四格漫畫大全.md "wikilink")」都是這段期間的傑作。近年，上官小強因厭倦了漫畫行業的運作手法和人事鬥爭而把公司結束，專心打理他那位於[旺角](../Page/旺角.md "wikilink")[信和中心的](../Page/信和中心.md "wikilink")「[壽星專門店](../Page/壽星專門店.md "wikilink")」的零售生意。

2017年1月13日，上官小強因病逝世，享年66歲，由遺孀於社交網站宣佈死訊。\[1\]

2017年1月27日，其好友漫畫家月巴氏在專欄中懷念他︰「在那個漫畫界光芒耀眼到好多人盲目的時候，上官小強為很多有志於漫畫的新人提供了機會，而他這樣做，並不因為要做甚麼漫畫大亨，或甚麼漫畫之神，而只因為他是一個好Nice的人。」

## 漫畫作品

  - 《[地球保衛戰](../Page/地球保衛戰.md "wikilink")》
  - 《[小木偶](../Page/小木偶.md "wikilink")》
  - 《[足球小將](../Page/足球小將_\(上官小強\).md "wikilink")》(又稱上官小將)
  - 《[壽星仔](../Page/壽星仔.md "wikilink")》
  - 《[金裝壽星](../Page/金裝壽星.md "wikilink")》
  - 《[小強漫畫集](../Page/小強漫畫集.md "wikilink")》
  - 《[小強畫集](../Page/小強畫集.md "wikilink")》
  - 《[四格漫畫大全](../Page/四格漫畫大全.md "wikilink")》

## 參考

<references/>

[Category:香港漫畫家](../Category/香港漫畫家.md "wikilink")
[Category:陳姓](../Category/陳姓.md "wikilink")

1.