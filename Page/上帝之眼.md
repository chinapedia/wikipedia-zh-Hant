[Catedral_De_Salta_Ojo_de_la_Providencia_-_Salta_Cathedral_All_Seeing_Eye.jpg](https://zh.wikipedia.org/wiki/File:Catedral_De_Salta_Ojo_de_la_Providencia_-_Salta_Cathedral_All_Seeing_Eye.jpg "fig:Catedral_De_Salta_Ojo_de_la_Providencia_-_Salta_Cathedral_All_Seeing_Eye.jpg")
[United_States_one_dollar_bill,_reverse.jpg](https://zh.wikipedia.org/wiki/File:United_States_one_dollar_bill,_reverse.jpg "fig:United_States_one_dollar_bill,_reverse.jpg")背面的上帝之眼\]\]

**上帝之眼**（，音译**普羅維登斯之眼**、**普洛維頓斯之眼**），又稱**全視之眼**（All-seeing
Eye），代表「[上帝](../Page/上帝.md "wikilink")」監視[人類的法眼](../Page/人類.md "wikilink")，常見的形式為一顆被[三角形及萬丈光芒所環繞的](../Page/三角形.md "wikilink")[眼睛](../Page/眼睛.md "wikilink")，以出現在[美國國徽及](../Page/美國國徽.md "wikilink")[一美元纸币的背面而廣為人知](../Page/一美元纸币.md "wikilink")。

## 宗教用途

上帝之眼的概念有人認為是源自[古埃及的](../Page/古埃及.md "wikilink")[荷魯斯之眼](../Page/荷魯斯之眼.md "wikilink")。在[中世紀和](../Page/中世紀.md "wikilink")[文藝復興時期的肖像畫法中](../Page/文藝復興.md "wikilink")，眼睛圖案（通常包圍在三角形內）被部分人猜測象徵著[基督教的](../Page/基督教.md "wikilink")[三位一體](../Page/三位一體.md "wikilink")，但基督教[聖經中並無有關論述](../Page/聖經.md "wikilink")。和現在常見形式較接近的上帝之眼則可追溯到17、18世紀的[歐洲](../Page/歐洲.md "wikilink")，當時的形式為一顆飄浮在空中的眼睛，有時會有雲霧或光芒環繞。

## 美國用途

1782年，上帝之眼被加入美国国徽的背面，人们通常认为这个建议来自艺术顾问[皮埃尔-尤金·迪西默蒂埃](../Page/皮埃尔-尤金·迪西默蒂埃.md "wikilink")\[1\]。国徽上的上帝之眼位于未完成的[金字塔上](../Page/金字塔.md "wikilink")。这在2004年[迪士尼的电影](../Page/迪士尼.md "wikilink")[国家宝藏中被编为剧本](../Page/驚天奪寶.md "wikilink")。

## 共濟会

现在，上帝之眼通常会与[共济会关联](../Page/共济会.md "wikilink")。眼睛的图案最早是在1797年作为共济会标准意象的一部分出现的\[2\]。这个图案代表上帝的全视之眼，警示着共济会的所思所行都被上帝观察着。在共济会中，上帝被称为宇宙的伟大建筑师（*Great
Architect of the
Universe*）。共濟會早期的標誌也是上帝之眼，其上飾以雲霧，其外為半圓形的光芒所包圍，眼睛图案有时会被三角形包围，已相當接近美國國徽中出現的上帝之眼。也因此一直有人盛傳美國國徽之所以放置上帝之眼正是由於當時共濟會成員的野心與陰謀。然而，共济会中眼睛图案的普遍使用是在国徽创造后的14年以后。并且在国徽设计委员会的会员中，只有[本杰明·富兰克林一人是共济会会员](../Page/本杰明·富兰克林.md "wikilink")（他的设计方案并未被采纳）。许多共济会组织已明确拒绝和国徽的创造发生任何关联。\[3\]\[4\]

## 各種上帝之眼的概念與表現形式

<File:US> Great Seal
Reverse.svg|[美國國徽背面的上帝之眼](../Page/美國國徽.md "wikilink")
[File:Eyeofra.png|古埃及的](File:Eyeofra.png%7C古埃及的)[荷魯斯之眼](../Page/荷魯斯之眼.md "wikilink")
[File:QuoModoDeum.gif|17世紀的上帝之眼](File:QuoModoDeum.gif%7C17世紀的上帝之眼)
<File:ChristianEyeOfProvidence.svg>|[基督教](../Page/基督教.md "wikilink")[三位一體的上帝之眼](../Page/三位一體.md "wikilink")
[File:MasonicEyeOfProvidence.gif|共濟會早期的](File:MasonicEyeOfProvidence.gif%7C共濟會早期的)[徽章](../Page/徽章.md "wikilink")
<File:Great> Seal of United
States.jpg|[一美元紙幣中的](../Page/一美元紙幣.md "wikilink")[美国国徽](../Page/美国国徽.md "wikilink")
[File:IAO-logo.png|美国](File:IAO-logo.png%7C美国)[國防高等研究計劃署的旧机构](../Page/國防高等研究計劃署.md "wikilink")的徽章
<File:OlympicStadiumLondon2012.jpg>|[倫敦奧林匹克體育場環繞會場頂部的照明燈象徵上帝之眼](../Page/倫敦奧林匹克體育場.md "wikilink")
<File:Elyon.JPG>|[以色列最高法院大樓中頂部有圓洞的金字塔](../Page/以色列最高法院.md "wikilink")
<File:Thiên> nhãn Cao Đài.jpg|越南[高台教中的天眼](../Page/高台教.md "wikilink")

## 注释

## 参考文献

## 外部链接

## 参见

  - [荷魯斯之眼](../Page/荷魯斯之眼.md "wikilink")
  - [美國國徽](../Page/美國國徽.md "wikilink")

[P](../Category/眼睛.md "wikilink") [P](../Category/宗教象徵.md "wikilink")
[P](../Category/共济会.md "wikilink")

1.  *[The Great Seal of the United
    States](http://www.state.gov/documents/organization/27807.pdf)*
    booklet, published by [US Dept. of
    State](../Page/United_States_Department_of_State.md "wikilink")
2.  [Masonic Service Association](http://www.msana.com/eyeinpyramid.asp)
     webpage on the *Eye in the Pyramid*
3.  [Grand Lodge of British Columbia and
    Yukon](http://freemasonry.bcy.ca/anti-masonry/anti-masonry02.html#eye_pyramid),
    *Anti-masonry Frequently Asked Questions*
4.  [Pietre-Stones Review of
    Freemasonry](http://www.freemasons-freemasonry.com/masonic_dollar.html),
    *The "Masonic" One Dollar: Fact or Fiction?* by W.Bro. David Barrett