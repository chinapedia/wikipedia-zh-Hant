「**上帝已死**」（）是[德國哲學家](../Page/德國.md "wikilink")[尼采的一句名言](../Page/尼采.md "wikilink")，此句在尼采《[快樂的科學](../Page/快樂的科學.md "wikilink")》一書中出現了三次\[1\]，後來又在其名作《[查拉圖斯特拉如是說](../Page/查拉圖斯特拉如是說.md "wikilink")》中出現。「上帝已死」是尼采常被誤解的名言。

## 解釋

「上帝已死」並非是字面上解讀的尼采相信一開始實際存在一名[上帝](../Page/上帝.md "wikilink")，而後來死了。相對地他想表達的是上帝已不再是生命意義的來源或是[道德圭臬](../Page/道德.md "wikilink")。尼采認為上帝的死所帶來的崩解即是現存的道德假設：「當一個人放棄[基督信仰時](../Page/基督.md "wikilink")，他就把[基督教的道德觀從自己腳下抽出來了](../Page/基督教.md "wikilink")。這種道德觀絕非不證自明......當一個人打破了信仰上帝這項基督教化，就等於打破了一切：一個人的手中必然空虛。」這也是為何在「The
Madman」中，有一個段落主要致予[非有神論](../Page/非有神論.md "wikilink")（特別是無神論）者，其問題在於要保留任何非神性的[價值觀體系](../Page/價值觀.md "wikilink")。

上帝之死是說明人類再不能相信這種[宇宙秩序的方法](../Page/宇宙.md "wikilink")，因為他們無法識別這種秩序是否真正存在。尼采認為「上帝已死」，不單對人對宇宙或物質秩序失去信心，更令人否定絕對[價值](../Page/價值.md "wikilink")——不再相信一種[客觀而且普世地存在的道德法律](../Page/客觀.md "wikilink")，把每個個體都包括在內。這種絕對道德觀的失去，就是[虛無主義的開端](../Page/虛無主義.md "wikilink")。這種虛無主義令尼采盡其努力去找出重估人類基本價值的方法，即比基督教價值更深入的[宇宙觀](../Page/宇宙觀.md "wikilink")。

尼采相信，大部份人都不認同（或拒絕認識）「上帝已死」這種觀念，因為他們內心深處都有深層的恐懼或憤怒。所以，當這種死亡被廣泛認識之時，他們會覺得十分痛苦，然後虛無主義變得猖獗，而且[相對主義會在人類社會中成為法律](../Page/相對主義.md "wikilink")——所有事物都是被許可的。這是尼采認為基督教相當虛無的部份原因。對尼采而言，虛無主義是所有理想化了的哲學體系的必然後果，因為所有[理想主義都有像基督教道德](../Page/理想主義.md "wikilink")。

### 新的可能

尼采相信，人類沒有了上帝，依然可以找到正面的可能性。放棄了對上帝的信仰為人類發展自己的創作能力開了第一道門戶。基督教的神常有隨意的命令和禁令，但衪已經無法左右人類，所以人可以放棄向[超自然的力量尋求協助](../Page/超自然.md "wikilink")，而去認識這個世界的新一套價值。承認「上帝已死」就像一塊空的大帆布那樣。這是成為新的，不一樣的，更創新的東西的自由——這種自由並不包括接受過去的包袱。

尼采用了茫茫大海作比喻。那些學會為自己創造新生的人代表了人類的新階段，就是[超人](../Page/超人說.md "wikilink")——它指某些通過駕馭自己的虛無主義而成為傳奇英雄的人。

## 参考文献

  - Heidegger, Martin. *Nietzsches Wort 'Gott ist tot* (1943) translated
    as "The Word of Nietzsche: 'God Is Dead,'" in *Holzwege*, edited and
    translated by Julian Young and Kenneth Haynes. Cambridge University
    Press, 2002.
  - Kaufmann, Walter. *Nietzsche: Philosopher, Psychologist,
    Antichrist*. Princeton: Princeton University Press, 1974.
  - Roberts, Tyler T. *Contesting Spirit: Nietzsche, Affirmation,
    Religion.* Princeton: Princeton University Press, 1998.
  - [Thomas J. J. Altizer](../Page/Thomas_J._J._Altizer.md "wikilink"),
    *The Gospel of Christian Atheism* (Philadelphia: Westminster, 1966).
  - Thomas J. J. Altizer and William Hamilton, *Radical Theology and the
    Death of God* (Indianapolis: Bobbs-Merrill, 1966).
  - Bernard Murchland, ed., *The Meaning of the Death of God* (New York:
    Random House, 1967).
  - [Gabriel Vahanian](../Page/Gabriel_Vahanian.md "wikilink"), *The
    Death of God* (New York: George Braziller, 1961).
  - [John D. Caputo](../Page/John_D._Caputo.md "wikilink"), [Gianni
    Vattimo](../Page/Gianni_Vattimo.md "wikilink"), *After the Death of
    God*, edited by [Jeffrey W.
    Robbins](../Page/Jeffrey_W._Robbins.md "wikilink") (New York:
    Columbia University Press, 2007).

## 相关条目

  - [人之死](../Page/人之死.md "wikilink")
  - [虛無主義](../Page/虛無主義.md "wikilink")
  - [道德相對主義](../Page/道德相對主義.md "wikilink")

## 註釋

{{-}}

[ca:Friedrich Wilhelm Nietzsche\#El diagnòstic: el
nihilisme](../Page/ca:Friedrich_Wilhelm_Nietzsche#El_diagnòstic:_el_nihilisme.md "wikilink")

[G](../Category/弗里德里希·尼采.md "wikilink")
[D](../Category/無神論.md "wikilink")
[G](../Category/倫理學.md "wikilink")
[D](../Category/上帝.md "wikilink") [G](../Category/死亡.md "wikilink")
[Category:虚无主义](../Category/虚无主义.md "wikilink")
[Category:德语词汇或短语](../Category/德语词汇或短语.md "wikilink")

1.  in sections 108 (New Struggles), 125 (The Madman), and for a third
    time in section 343 (The Meaning of our Cheerfulness).