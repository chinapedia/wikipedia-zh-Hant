**上校**是[軍隊中](../Page/軍隊.md "wikilink")[军官的職銜](../Page/军官.md "wikilink")，在多数國家中是校官中的最高級別，在[中校之上](../Page/中校.md "wikilink")、[少將或](../Page/少將.md "wikilink")[准將之下](../Page/准將.md "wikilink")。但在某些军事组织中，則还设有高上校一級的[大校軍銜](../Page/大校.md "wikilink")。在某些较小的军事组织中，上校也有可能作为最高级别的军衔。

在多数英语国家中，[陸軍](../Page/陸軍.md "wikilink")、[空軍及](../Page/空軍.md "wikilink")[海军陸戰隊的上校稱為](../Page/海军陸戰隊.md "wikilink")****（缩写为Col.），[海軍上校則稱作](../Page/海軍上校.md "wikilink")****。[英国空军上校的稱呼则為](../Page/英国空军.md "wikilink")****（[聯隊長](../Page/空軍聯隊.md "wikilink")）。在東亞各國軍隊的上校階級中，[舊日本軍的漢字为](../Page/日本軍.md "wikilink")**大佐**，[自衛隊則依軍種分稱為一等陸佐](../Page/自衛隊.md "wikilink")、一等海佐和一等空佐（簡稱**一佐**），[越南人民軍上校](../Page/越南人民軍.md "wikilink")（）和[朝鮮人民軍上校](../Page/朝鮮人民軍.md "wikilink")（）的對應漢字為**上佐**、[韓國國軍上校](../Page/韓國國軍.md "wikilink")（）的對應漢字則為**大領**。

## 執掌

上校在[陸軍中主要擔任](../Page/陸軍.md "wikilink")[團長或副](../Page/團長.md "wikilink")[旅長等職務](../Page/旅長.md "wikilink")，也可能擔任旅長；在[海軍一般為](../Page/海軍.md "wikilink")[巡洋艦或](../Page/巡洋艦.md "wikilink")[航空母艦的艦長](../Page/航空母艦.md "wikilink")；在[空軍則擔任](../Page/空軍.md "wikilink")[大隊長或類似層級的職務](../Page/大队_\(军事航空\).md "wikilink")，但在[美國等國空軍中可為](../Page/美國.md "wikilink")[聯隊長](../Page/聯隊_\(軍事航空\).md "wikilink")。

[中华人民共和国上校主要擔任副师职](../Page/中华人民共和国.md "wikilink")／正旅职，正团职／副旅职。\[1\]

[中華民國因軍隊組織重組及將官數量縮小](../Page/中華民國.md "wikilink")，上校階級均承襲以往將軍的任務，擔任指揮官、訓練中心（兵科學校）之校長、海軍大型艦艇艦長，陸海空軍之高司指揮官等

## 特殊用法

在2011年，[利比亚领导者](../Page/利比亚.md "wikilink")[卡扎菲的统治被推翻之前](../Page/卡扎菲.md "wikilink")，其同僚以其在军方的职位称呼其为“上校”。

## 参考文献

<div class="references-small">

<references />

</div>

[\*](../Category/上校.md "wikilink")

1.