**上海儿童医学中心站**位于[上海](../Page/上海.md "wikilink")[浦东新区](../Page/浦东新区.md "wikilink")[东方路](../Page/东方路.md "wikilink")[北园路](../Page/北园路.md "wikilink")[上海儿童医学中心附近](../Page/上海儿童医学中心.md "wikilink")，为[上海轨道交通6号线的地下](../Page/上海轨道交通6号线.md "wikilink")[岛式车站](../Page/岛式站台.md "wikilink")，于2007年12月29日启用。车站以[橙色作為佈置的主要色彩](../Page/橙色.md "wikilink")。站名为上海地铁全线网中最长，因此报站中以“儿童医学中心”代替。

## 车站结构

[Shanghai_Childrens_Medical_Center_Station_Exit_2.jpg](https://zh.wikipedia.org/wiki/File:Shanghai_Childrens_Medical_Center_Station_Exit_2.jpg "fig:Shanghai_Childrens_Medical_Center_Station_Exit_2.jpg")
本站为地下二层岛式车站。

<table>
<tbody>
<tr class="odd">
<td><p><strong>地面</strong></p></td>
<td><p>出入口</p></td>
<td><p>1—3出入口</p></td>
</tr>
<tr class="even">
<td><p><strong>地下一层</strong></p></td>
<td><p>站厅</p></td>
<td><p>服务中心、进出站闸机</p></td>
</tr>
<tr class="odd">
<td><p><strong>地下二层</strong></p></td>
<td><p>北行</p></td>
<td><p>列车往方向 <small>（）</small></p></td>
</tr>
<tr class="even">
<td><center>
<p><small><a href="../Page/岛式站台.md" title="wikilink">岛式站台</a>，左边车门将会开启</small></p>
</center></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>南行</p></td>
<td><p>　列车往方向 <small>（）</small> </p></td>
<td></td>
</tr>
</tbody>
</table>

## 公交换乘

170、219、614、795、819、871、980、984、隧道九线

## 车站出口

  - 1、2号口：东方路东侧，北园路南
  - 3号口：东方路西侧，北园路南

## 临近车站

[Category:浦东新区地铁车站](../Category/浦东新区地铁车站.md "wikilink")
[Category:2007年启用的铁路车站](../Category/2007年启用的铁路车站.md "wikilink")
[Category:以建築物命名的鐵路車站](../Category/以建築物命名的鐵路車站.md "wikilink")