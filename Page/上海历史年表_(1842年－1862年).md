<table style="width:99%;">
<colgroup>
<col style="width: 14%" />
<col style="width: 35%" />
<col style="width: 48%" />
</colgroup>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong><font color="#FFFFFF">日期</font></strong></p></td>
<td style="text-align: center;"><p><strong><font color="#FFFFFF">事件</font></strong></p></td>
<td style="text-align: center;"><p><strong><font color="#FFFFFF">简述</font></strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1842年8月29日</p></td>
<td style="text-align: center;"><p>《<a href="../Page/南京条约.md" title="wikilink">南京条约</a>》签订</p></td>
<td style="text-align: center;"><p>中英《南京条约》规定<a href="../Page/上海.md" title="wikilink">上海</a>、<a href="../Page/宁波.md" title="wikilink">宁波</a>、<a href="../Page/福州.md" title="wikilink">福州</a>、<a href="../Page/厦门.md" title="wikilink">厦门以及广州这五处开辟为对外通商口岸</a>（简称“五口”），英国人可以在这五处居住和经商。</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1843年11月17日</p></td>
<td style="text-align: center;"><p>首任<a href="../Page/英国.md" title="wikilink">英国驻沪</a><a href="../Page/领事.md" title="wikilink">领事</a><a href="../Page/巴富尔.md" title="wikilink">巴富尔抵达上海</a></p></td>
<td style="text-align: center;"><p>英国驻沪领事的到任意味着上海自此正式开埠。</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1844年7月13日</p></td>
<td style="text-align: center;"><p>《<a href="../Page/望厦条约.md" title="wikilink">望厦条约</a>》签订</p></td>
<td style="text-align: center;"><p>中美订立的这份合约的内容参照了中英《南京条约》。</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1844年10月24日</p></td>
<td style="text-align: center;"><p>《<a href="../Page/黄埔条约.md" title="wikilink">黄埔条约</a>》签订</p></td>
<td style="text-align: center;"><p>中法订立的这份合约的内容同样也参照了中英《南京条约》。</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1845年11月29日</p></td>
<td style="text-align: center;"><p><a href="../Page/上海第一次土地章程.md" title="wikilink">第一次土地章程</a>（也称“地皮章程”）出台</p></td>
<td style="text-align: center;"><p>这份土地章程被视为上海租界存在和运作的依据法理，由<a href="../Page/上海道台.md" title="wikilink">上海道台和英国驻沪领事共同商定</a>，其要旨在于华洋隔离居住的政策，并定出了英国人居住地的范围。</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1846年</p></td>
<td style="text-align: center;"><p><a href="../Page/上海道路码头委员会.md" title="wikilink">道路码头委员会成立</a></p></td>
<td style="text-align: center;"><p>外国人在上海成立的第一个组织，主要解决租界内部道路建设等问题，是<a href="../Page/上海工部局.md" title="wikilink">工部局的雏形</a>。</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1848年11月27日</p></td>
<td style="text-align: center;"><p><a href="../Page/上海英租界.md" title="wikilink">英租界第一次扩充</a></p></td>
<td style="text-align: center;"><p>北界扩充至<a href="../Page/苏州河.md" title="wikilink">苏州河</a>，西界扩充至周泾浜。（今西藏路）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1849年4月6日</p></td>
<td style="text-align: center;"><p><a href="../Page/上海法租界.md" title="wikilink">法租界成立</a></p></td>
<td style="text-align: center;"><p>　</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1853年4月12日</p></td>
<td style="text-align: center;"><p><a href="../Page/上海租地西人大会.md" title="wikilink">上海租地西人大会通过组织</a><a href="../Page/义勇队.md" title="wikilink">义勇队的决议</a></p></td>
<td style="text-align: center;"><p><a href="../Page/太平天国运动.md" title="wikilink">太平天国运动的影响波及上海</a>，为了维护租界的安全，租界内的外国人在英、法、美三国领事的倡导下自发组织了义勇队，这是外国人在上海的第一支武装队伍。</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1853年9月7日</p></td>
<td style="text-align: center;"><p><a href="../Page/小刀会.md" title="wikilink">小刀会占领上海县城</a></p></td>
<td style="text-align: center;"><p>烧毁了上海县衙和海关等处，杀了上海知县，囚禁了上海道台。</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1854年4月4日</p></td>
<td style="text-align: center;"><p><a href="../Page/泥城浜之战.md" title="wikilink">泥城浜之战</a></p></td>
<td style="text-align: center;"><p>清军企图通过租界进攻占领县城的小刀会起义军，保持中立的租界方在英、美海军的协助下以武力强行阻止清军过界。</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1854年7月11日</p></td>
<td style="text-align: center;"><p><a href="../Page/上海第二次土地章程.md" title="wikilink">第二次土地章程出台</a></p>
<p><a href="../Page/上海工部局.md" title="wikilink">工部局成立</a></p></td>
<td style="text-align: center;"><p>　</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1855年2月17日</p></td>
<td style="text-align: center;"><p>小刀会退出上海县城</p></td>
<td style="text-align: center;"><p>　</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1858年1月26日</p></td>
<td style="text-align: center;"><p>《<a href="../Page/天津条约.md" title="wikilink">天津条约</a>》签订</p></td>
<td style="text-align: center;"><p>至此<a href="../Page/鸦片.md" title="wikilink">鸦片买卖合法化</a>，上海逐渐成为了当时中国最主要的鸦片输入港。</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1860年8月17日</p></td>
<td style="text-align: center;"><p>太平天国起义军首次攻打上海</p></td>
<td style="text-align: center;"><p>　</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1862年1月11日</p></td>
<td style="text-align: center;"><p>太平天国起义军第二次攻打上海</p></td>
<td style="text-align: center;"><p>　</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1862年5月1日</p></td>
<td style="text-align: center;"><p>上海法租界公董局成立</p></td>
<td style="text-align: center;"><p>为了保全上海法租界的独立性，法国驻沪领事<a href="../Page/爱棠.md" title="wikilink">爱棠宣布法租界退出工部局</a>，代之以上海法租界公董局。</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1862年8月</p></td>
<td style="text-align: center;"><p>太平天国起义军第三次攻打上海</p></td>
<td style="text-align: center;"><p>　</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1862年11月</p></td>
<td style="text-align: center;"><p><a href="../Page/上海自由市计划.md" title="wikilink">上海自由市计划</a></p></td>
<td style="text-align: center;"><p>由在沪外国商人提出，主要内容是将上海划为自由的贸易性都市，不受制于任何国家和个人。此项计划由各国领事报呈各国驻中国的公使，最后被各国公使以不得干涉中国内政为由驳回。</p></td>
</tr>
</tbody>
</table>

[Category:19世纪上海](../Category/19世纪上海.md "wikilink")
[Category:1840年代中国](../Category/1840年代中国.md "wikilink")
[Category:1850年代中国](../Category/1850年代中国.md "wikilink")
[Category:1860年代中国](../Category/1860年代中国.md "wikilink")
[Category:年表](../Category/年表.md "wikilink")