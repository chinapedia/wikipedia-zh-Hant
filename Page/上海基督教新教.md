[基督教新教自](../Page/基督教新教.md "wikilink")1840年代进入上海后，成为[上海的主流宗教之一](../Page/上海宗教.md "wikilink")。

## 历史

1843年，上海开埠的同一年，第一位基督教传教士，英国[伦敦会的](../Page/伦敦会.md "wikilink")[麦都思来到上海](../Page/麦都思.md "wikilink")。

此后，来自英美各教派的差会陆续进入上海：

  - [伦敦会](../Page/伦敦会.md "wikilink")：1843年来沪，在城内建堂（城中堂）。以后在租界中心的山东路设立[墨海书馆](../Page/墨海书馆.md "wikilink")、[仁济医院和](../Page/仁济医院.md "wikilink")[天安堂](../Page/天安堂.md "wikilink")，在虹口兆丰路（今高阳路）设立麦伦中学和天乐堂。

  - [英国圣公会](../Page/英国圣公会.md "wikilink")

  - [美国圣公会](../Page/美国圣公会.md "wikilink")：1842年来华，最终在1845年选择在上海立足，初期除了在南市设堂外，该会在虹口设立的[救主堂是所谓美租界乃至苏州河以北市区发展的起点](../Page/救主堂.md "wikilink")（1937年迁往法租界赵主教路，今徐汇区五原路，20世纪末曾作为华东神学院院址），不远处设立了同仁医院。1879年，该会在西郊苏州河岸边兴建圣约翰书院，20世纪初发展成[圣约翰大学](../Page/圣约翰大学.md "wikilink")。后来，该会又陆续设立圣彼得堂、[上海诸圣堂等](../Page/诸圣堂_\(上海\).md "wikilink")。

  - [美南浸信会](../Page/美南浸信会.md "wikilink")：1847年来沪，在老北门外西侧护城河边设立了第一浸会堂。20世纪初发展到虹口北四川路，1920年代又在北郊购置浸会庄，包括明强中学和[晏摩氏女中](../Page/晏摩氏女中.md "wikilink")。虹口则另设[上海怀恩堂](../Page/上海怀恩堂.md "wikilink")，抗战期间迁往西摩路（陕西北路）。

  - [美南监理会](../Page/美南监理会.md "wikilink")：1848年来华，即进入上海，设立[慕尔堂](../Page/慕尔堂.md "wikilink")（沐恩堂）、[景林堂](../Page/景林堂.md "wikilink")、闸北堂（大统路、天目西路附近，已拆除）等教堂，[林乐知创办](../Page/林乐知.md "wikilink")[中西书院](../Page/中西书院.md "wikilink")（后并入苏州[东吴大学](../Page/东吴大学.md "wikilink")，办理法学院和附中），[海淑德创办](../Page/海淑德.md "wikilink")[中西女中](../Page/中西女中.md "wikilink")。

  - [美北长老会](../Page/美北长老会.md "wikilink")：1850年来沪，先在大南门外设立清心中学、清心女中和[清心堂](../Page/清心堂.md "wikilink")，20世纪初在虹口设立[美华书馆和](../Page/美华书馆.md "wikilink")[鸿德堂](../Page/鸿德堂.md "wikilink")（今多伦路），不远处的宝通路[闸北堂则是由华人牧师创立](../Page/闸北堂.md "wikilink")，主要服务于在附近[商务印书馆任职的长老会信徒](../Page/商务印书馆.md "wikilink")。

  - 安息浸礼会：1847年来沪，1902年在斜桥设立惠中堂。

  - [宣道会](../Page/宣道会.md "wikilink")：1900年义和团事变中，[吴伯莱牧师从天津迁来](../Page/吴伯莱.md "wikilink")，在虹口北四川路设立[守真堂](../Page/守真堂.md "wikilink")。

  - [基督复临安息日会](../Page/基督复临安息日会.md "wikilink")：1910年以后，在杨树浦宁国路设立中华总会（[沪东堂](../Page/沪东堂.md "wikilink")），在虹口武进路设立[沪北会堂](../Page/沪北会堂.md "wikilink")。

  - [中国内地会](../Page/中国内地会.md "wikilink")：1892年在今虹口区吴淞路设立总部，1931年迁往今静安区北京西路与新闸路之间（今儿童医院），该会在上海没有设立面向华人的教堂，只在[地丰路](../Page/地丰路.md "wikilink")（乌鲁木齐北路）设立供西方传教士聚会的公共礼拜堂（今[新恩堂](../Page/新恩堂.md "wikilink")）。

  -
20世纪上半叶，上海又出现不少华人创立的独立团体：

  - [伯特利教会](../Page/伯特利教会.md "wikilink")：1920年，[石美玉医生脱离](../Page/石美玉.md "wikilink")[美以美会](../Page/美以美会.md "wikilink")，从江西[九江迁到上海南市制造局路](../Page/九江.md "wikilink")，兴建伯特利医院（今第九医院）及教堂、学校等。1930年代，该会发起全国巡回布道运动，聘[计志文](../Page/计志文.md "wikilink")、[宋尚节讲道](../Page/宋尚节.md "wikilink")，影响很大。

  - [地方教会](../Page/地方教会.md "wikilink")（基督徒聚会处）：1926年在新闸路[汪佩真家中开始](../Page/汪佩真.md "wikilink")[擘饼聚会](../Page/擘饼聚会.md "wikilink")，次年[倪柝声到上海](../Page/倪柝声.md "wikilink")，于1928年初在哈同路（今铜仁路）240弄文德里（已拆除）聚会，并开设[上海福音书房](../Page/上海福音书房.md "wikilink")。1948年，地方教会举行数次大规模福音游行，信徒人数增加甚多，于是在不远处的南阳路123号和145号兴建大型聚会所（[南阳路聚会所](../Page/南阳路聚会所.md "wikilink")）。

  - [真耶稣教会](../Page/真耶稣教会.md "wikilink")：

  - [灵粮堂](../Page/灵粮堂.md "wikilink")：

  -
## 现状

2002年，上海现有可统计信徒18万多人。开放的基督教堂点164处。

## 著名教堂

  - [慕尔堂](../Page/慕尔堂.md "wikilink")（沐恩堂）
  - [景林堂](../Page/景林堂.md "wikilink")
  - [上海怀恩堂](../Page/上海怀恩堂.md "wikilink")
  - [新恩堂](../Page/新恩堂.md "wikilink")
  - [上海国际礼拜堂](../Page/上海国际礼拜堂.md "wikilink")
  - [沪东堂](../Page/沪东堂.md "wikilink")
  - [鸿德堂](../Page/鸿德堂.md "wikilink")
  - [清心堂](../Page/清心堂.md "wikilink")
  - [上海诸圣堂](../Page/诸圣堂_\(上海\).md "wikilink")
  - [上海圣三一堂](../Page/上海圣三一堂.md "wikilink")
  - [闸北堂](../Page/闸北堂.md "wikilink")
  - [惠中堂](../Page/惠中堂.md "wikilink")
  - [沪西礼拜堂](../Page/沪西礼拜堂.md "wikilink")
  - [普安堂](../Page/普安堂.md "wikilink")
  - [救恩堂](../Page/救恩堂.md "wikilink")

## 参考文献

## 外部链接

  - [上海宗教志 \>\>
    第五编基督教](http://www.shtong.gov.cn/newsite/node2/node2245/node75195/node75204/index.html)
  - [上海基督教青年会、女青年会](http://www.shymca.org.cn/)

## 参见

  - [中国基督教新教](../Page/中国基督教新教.md "wikilink")
  - [天主教上海教区](../Page/天主教上海教区.md "wikilink")
  - [上海东正教](../Page/上海东正教.md "wikilink")

{{-}}

[SH](../Category/中国各省基督教新教.md "wikilink")
[上海基督教新教](../Category/上海基督教新教.md "wikilink")