**恒隆广场**（**Plaza
66**）是[上海第](../Page/上海市.md "wikilink")5高的[摩天大樓](../Page/摩天大樓.md "wikilink")（截至2013年11月），位于[静安区](../Page/静安区.md "wikilink")[南京西路北侧的](../Page/南京西路.md "wikilink")1266号，[陕西北路与](../Page/陕西北路_\(上海\).md "wikilink")[西康路之间](../Page/西康路_\(上海\).md "wikilink")。地上66层，288米，地下3层，裙房5层，占地30788平方米。设计师为[KPF建筑事务所](../Page/KPF建筑事务所.md "wikilink")，流线型玻璃主体建筑，建成于2001年建成时是中国上海[浦西地区的第一高楼](../Page/浦西.md "wikilink")。包括写字楼和购物中心。

## 购物中心

购物中心于2001年6月20日开始营业、7月14日正式开幕。商场于2015年开始逐步进行翻新工程，至2017年年初完工\[1\]。购物中心有5层楼面，面积为5.5万平方米，集中了一批世界知名时尚品牌的旗舰店，包括[Bvlgari](../Page/Bvlgari.md "wikilink")、[Cartier](../Page/Cartier.md "wikilink")、[Chanel](../Page/Chanel.md "wikilink")、[Dior](../Page/Dior.md "wikilink")、[Escada](../Page/Escada.md "wikilink")、[Fendi](../Page/Fendi.md "wikilink")、[Hermes](../Page/Hermes.md "wikilink")、[Louis
Vuitton](../Page/Louis_Vuitton.md "wikilink")、[Prada](../Page/Prada.md "wikilink")、[Versace](../Page/Versace.md "wikilink")、等。恒隆广场西侧的[上海商城](../Page/上海商城.md "wikilink")、对面的[上海展览中心](../Page/上海展览中心.md "wikilink")，都属于上海的标志性建筑。

## 交通

  - [上海轨道交通](../Page/上海轨道交通.md "wikilink")
      - [二号线](../Page/上海轨道交通二号线.md "wikilink")︰[南京西路站](../Page/南京西路站.md "wikilink")
      - [二号线](../Page/上海轨道交通二号线.md "wikilink")︰[静安寺站](../Page/静安寺站.md "wikilink")
      - [七号线](../Page/上海轨道交通七号线.md "wikilink")︰[静安寺站](../Page/静安寺站.md "wikilink")

## 画廊

<File:Plaza> 66.jpg|2006年的恒隆广场 <File:Plaza66>
Interior.jpg|商場匯集不少世界知名时尚品牌商店 <File:Plaza66>
Interior1.jpg|商場內部 <File:Plaza66> Void.jpg|购物中心中庭

[Category:上海摩天大楼](../Category/上海摩天大楼.md "wikilink")
[Category:恒隆地產物業](../Category/恒隆地產物業.md "wikilink")
[Category:2001年完工建築物](../Category/2001年完工建築物.md "wikilink")
[Category:2006年完工建築物](../Category/2006年完工建築物.md "wikilink")
[Category:上海商场](../Category/上海商场.md "wikilink")
[Category:250米至299米高的摩天大樓](../Category/250米至299米高的摩天大樓.md "wikilink")
[Category:静安区建筑物](../Category/静安区建筑物.md "wikilink")
[Category:上海写字楼](../Category/上海写字楼.md "wikilink")
[Category:KPF建筑事务所设计的建筑](../Category/KPF建筑事务所设计的建筑.md "wikilink")

1.