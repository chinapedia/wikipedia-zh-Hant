[1962-03_1962年_上海天马电影制品厂拍摄_魔术家的奇遇.jpg](https://zh.wikipedia.org/wiki/File:1962-03_1962年_上海天马电影制品厂拍摄_魔术家的奇遇.jpg "fig:1962-03_1962年_上海天马电影制品厂拍摄_魔术家的奇遇.jpg")
**上海电影制片厂**位于[上海](../Page/上海.md "wikilink")，是[中华人民共和国主要的](../Page/中华人民共和国.md "wikilink")[电影制片厂之一](../Page/电影制片厂.md "wikilink")。

## 组织沿革\[1\]\[2\]

  - 1949年11月16日，上海电影制片厂成立。[于伶任厂长](../Page/于伶.md "wikilink")，[钟敬之任副厂长](../Page/钟敬之.md "wikilink")。
  - 1953年2月，上海8家私营电影厂并入上海电影制片厂。于伶任厂长，[叶以群](../Page/叶以群.md "wikilink")、[蔡贲任副厂长](../Page/蔡贲.md "wikilink")。
  - 1957年**上海电影制片厂**经过改组，成为一个联合企业性质的公司——[上海电影制片公司](../Page/上海电影制片公司.md "wikilink")。[上海电影制片公司由](../Page/上海电影制片公司.md "wikilink")[江南电影制片厂](../Page/江南电影制片厂.md "wikilink")、[海燕电影制片厂和](../Page/海燕电影制片厂.md "wikilink")[天马电影制片厂构成](../Page/天马电影制片厂.md "wikilink")。
  - 1985年上海电影制片公司经过合并改组为[上海电影总公司](../Page/上海电影总公司.md "wikilink")，但是厂名依然叫作**上海电影制片厂**。
  - 1996年7月11日，以上海电影制片厂为母体，组建成立[上海电影电视（集团）公司](../Page/上海电影（集团）公司.md "wikilink")。
  - 1996年10月16日与1998年3月16日，上海电影制片厂播出摄制了22集电视连续剧
    《[三毛流浪记](../Page/三毛流浪记_\(1996年电视剧\).md "wikilink")》。上影大顺影视制作有限公司与上海电影制片厂又拍摄了24集电视连续剧
    《[三毛流浪记续集](../Page/三毛流浪记续集.md "wikilink")》。
  - 2001年8月以上海电影制片厂为母体，集合[上海美术电影制片厂](../Page/上海美术电影制片厂.md "wikilink")、[上海电影译制厂](../Page/上海电影译制厂.md "wikilink")、[上海电影技术厂](../Page/上海电影技术厂.md "wikilink")、上海电影发展总公司、上海东方影视发行公司、上海新光影艺苑及上海银星皇冠酒店等，组建成了[上海电影（集团）公司](../Page/上海电影（集团）公司.md "wikilink")，总资产近[人民幣](../Page/人民幣.md "wikilink")21亿元。上海电影（集团）公司目前为[上海文化广播影视集团九个组成单位之一](../Page/上海文化广播影视集团.md "wikilink")。

## 代表作品

  - 《[南征北战](../Page/南征北战.md "wikilink")》
  - 《[鸡毛信](../Page/鸡毛信.md "wikilink")》
  - 《[渡江侦察记](../Page/渡江侦察记.md "wikilink")》
  - 《[女篮五号](../Page/女篮五号.md "wikilink")》
  - 《[庐山恋](../Page/庐山恋.md "wikilink")》
  - 《[林则徐](../Page/林則徐_\(電影\).md "wikilink")》
  - 《[聂耳](../Page/聂耳.md "wikilink")》
  - 《[红色娘子军](../Page/红色娘子军.md "wikilink")》
  - 《[城南旧事](../Page/城南旧事.md "wikilink")》
  - 《[铁道游击队](../Page/铁道游击队.md "wikilink")》

## 知名人物

  - [谢晋](../Page/谢晋.md "wikilink")
  - [吴贻弓](../Page/吴贻弓.md "wikilink")
  - [程之](../Page/程之.md "wikilink")
  - [陈述](../Page/陈述_\(演员\).md "wikilink")
  - [仲星火](../Page/仲星火.md "wikilink")
  - [孙道临](../Page/孙道临.md "wikilink")
  - [金焰](../Page/金焰.md "wikilink")
  - [秦怡](../Page/秦怡.md "wikilink")
  - [潘虹](../Page/潘虹.md "wikilink")
  - [陈冲](../Page/陈冲.md "wikilink")
  - [张瑜](../Page/张瑜.md "wikilink")
  - [龚雪](../Page/龚雪.md "wikilink")

## 参考文献

<div class='references-small'>

<references/>

</div>

## 外部链接

  - [上海电影（集团）公司](http://www.sfs-cn.com/)

[上海电影制片厂电影](../Category/上海电影制片厂电影.md "wikilink")
[Category:上海电影（集团）公司](../Category/上海电影（集团）公司.md "wikilink")
[Category:中国影视制作机构](../Category/中国影视制作机构.md "wikilink")
[Category:1949年成立的公司](../Category/1949年成立的公司.md "wikilink")
[Category:总部位于上海的中华人民共和国国有企业](../Category/总部位于上海的中华人民共和国国有企业.md "wikilink")

1.
2.