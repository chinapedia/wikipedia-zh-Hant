**上海鲁迅纪念馆**是为纪念[鲁迅而辟的主题性博物馆](../Page/鲁迅.md "wikilink")。纪念馆开放于1951年1月7日，是[中华人民共和国的第一座人物性纪念馆](../Page/中华人民共和国.md "wikilink")。纪念馆收藏有文物、文献资料二十余万件，其中有鲁迅的文稿、诗稿、译稿、书信，以及鲁迅收藏的木刻作品和生活用品等。纪念馆位于[四川北路](../Page/四川北路.md "wikilink")2288号，采纳了[江南民居的风格特点](../Page/江南民居.md "wikilink")，是一座中西合璧的二层庭院式建筑。纪念馆的馆额由[周恩来题写](../Page/周恩来.md "wikilink")。纪念馆除了陈列展览相关的文物，还编辑出版鲁迅研究书籍，开展学术交流活动，并负责[鲁迅墓](../Page/鲁迅墓.md "wikilink")、[鲁迅故居的保护管理](../Page/鲁迅故居.md "wikilink")。

## 沿革

[1963-02_1963年_上海鲁迅纪念馆.jpg](https://zh.wikipedia.org/wiki/File:1963-02_1963年_上海鲁迅纪念馆.jpg "fig:1963-02_1963年_上海鲁迅纪念馆.jpg")
纪念馆初设于[山阴路](../Page/山阴路.md "wikilink")[大陆新村](../Page/大陆新村.md "wikilink")9号和10号。9号是[上海鲁迅故居](../Page/上海鲁迅故居.md "wikilink")，10号为辅助陈列室。

1956年10月[鲁迅墓由](../Page/鲁迅墓.md "wikilink")[上海万国公墓迁建于](../Page/上海万国公墓.md "wikilink")[鲁迅公园（原虹口公园）](../Page/鲁迅公园_\(上海\).md "wikilink")，纪念馆也在公园建造了新的馆舍。鲁迅墓由花岗石构筑而成，墓碑由[毛泽东题字](../Page/毛泽东.md "wikilink")。墓前草坪上有鲁迅先生铜像。

1998年8月1日纪念馆改建后重新开放。

公布了第一批[上海市文物保护单位](../Page/上海市文物保护单位.md "wikilink")，鲁迅故居位列其中。1977年，又被列入重新公布的名单中。

## 展厅

[Status_of_Luxun.JPG](https://zh.wikipedia.org/wiki/File:Status_of_Luxun.JPG "fig:Status_of_Luxun.JPG")

**鲁迅先生生平陈列厅**：该展厅有1000多平方米，通过鲁迅的照片、手稿、遗物、文献以及雕塑、蜡像、场景、影视等一千余件展品，介绍了鲁迅的思想发展、文学创作和社会活动。

**朝华文库**：收藏与鲁迅有过直接接触的文化名人的遗物。现设有[陈望道](../Page/陈望道.md "wikilink")、[许广平](../Page/许广平.md "wikilink")、[曹靖华](../Page/曹靖华.md "wikilink")、[曹聚仁](../Page/曹聚仁.md "wikilink")、[巴人](../Page/巴人.md "wikilink")、[汪静之](../Page/汪静之.md "wikilink")、[李霁野](../Page/李霁野.md "wikilink")、[黄源](../Page/黄源.md "wikilink")、[陈学昭](../Page/陈学昭.md "wikilink")、[赵家璧](../Page/赵家璧.md "wikilink")、[吴朗西](../Page/吴朗西.md "wikilink")、[唐弢](../Page/唐弢.md "wikilink")、[杜宣](../Page/杜宣.md "wikilink")、[钱君匋](../Page/钱君匋.md "wikilink")、[张望](../Page/张望.md "wikilink")、[李桦](../Page/李桦.md "wikilink")、[力群](../Page/力群.md "wikilink")、[杨可扬](../Page/杨可扬.md "wikilink")、[邵克萍](../Page/邵克萍.md "wikilink")、[史莽](../Page/史莽.md "wikilink")、[裘沙](../Page/裘沙.md "wikilink")、[王伟君](../Page/王伟君.md "wikilink")、[杨涵](../Page/杨涵.md "wikilink")、[丁景唐](../Page/丁景唐.md "wikilink")16个专库，收藏有藏书、手稿、书信等文物、文献三万余件。库名“朝华文库”由[巴金先生题写](../Page/巴金.md "wikilink")，各专库分别由[夏征农](../Page/夏征农.md "wikilink")、[赵朴初](../Page/赵朴初.md "wikilink")、[雷洁琼](../Page/雷洁琼.md "wikilink")、[张光年](../Page/张光年.md "wikilink")、[黄源](../Page/黄源.md "wikilink")、[张爱萍](../Page/张爱萍.md "wikilink")、[于若木](../Page/于若木.md "wikilink")、[钱君甸](../Page/钱君甸.md "wikilink")、[赖少其](../Page/赖少其.md "wikilink")、[顾廷龙](../Page/顾廷龙.md "wikilink")、[王光英](../Page/王光英.md "wikilink")、[乔石](../Page/乔石.md "wikilink")、[力群](../Page/力群.md "wikilink")、[王琦](../Page/王琦.md "wikilink")、[程思远等题写](../Page/程思远.md "wikilink")。

**奔流艺苑**：350平方米的专题展厅。

**树人堂**：学术报告厅，可容纳150人，配有影视放映和同声翻译设备。

## 外部链接

  - [鲁迅纪念馆网站](https://web.archive.org/web/20050824172822/http://www.shcrm.com.cn/SHCRM/luxun/default.htm)

## 参见

  - [鲁迅](../Page/鲁迅.md "wikilink")
  - [鲁迅墓](../Page/鲁迅墓.md "wikilink")
  - [鲁迅公园](../Page/鲁迅公园.md "wikilink")
  - [鲁迅故居](../Page/鲁迅故居.md "wikilink")
  - [鲁迅纪念馆](../Page/鲁迅纪念馆.md "wikilink")

{{-}}

[沪](../Category/国家一级博物馆.md "wikilink")
[Category:鲁迅纪念馆](../Category/鲁迅纪念馆.md "wikilink")
[Category:上海各博物馆](../Category/上海各博物馆.md "wikilink")
[Category:虹口区建筑物](../Category/虹口区建筑物.md "wikilink")
[沪](../Category/全国爱国主义教育示范基地.md "wikilink")