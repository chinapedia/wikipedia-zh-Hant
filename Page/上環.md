[Overlook_Sheung_Wan.jpg](https://zh.wikipedia.org/wiki/File:Overlook_Sheung_Wan.jpg "fig:Overlook_Sheung_Wan.jpg")
[Millennium_Plaza_Open_Space_201008.jpg](https://zh.wikipedia.org/wiki/File:Millennium_Plaza_Open_Space_201008.jpg "fig:Millennium_Plaza_Open_Space_201008.jpg")（左）及[中遠大廈](../Page/中遠大廈.md "wikilink")（右）之間\]\]
[HK_上環_Sheung_Wan_Wah_Lane_shop_art_gallery_June_2018_IX2_(1).jpg](https://zh.wikipedia.org/wiki/File:HK_上環_Sheung_Wan_Wah_Lane_shop_art_gallery_June_2018_IX2_\(1\).jpg "fig:HK_上環_Sheung_Wan_Wah_Lane_shop_art_gallery_June_2018_IX2_(1).jpg")
**上環**（），舊稱**西區**（，簡稱**Western**），位於[香港島](../Page/香港島.md "wikilink")[中西區的北部](../Page/中西區_\(香港\).md "wikilink")，東起[鴨巴甸街](../Page/鴨巴甸街.md "wikilink")、[永吉街](../Page/永吉街.md "wikilink")，西至[威利麻街](../Page/威利麻街.md "wikilink")、南至[堅道及](../Page/堅道.md "wikilink")[般咸道](../Page/般咸道.md "wikilink")。

該區[區議會](../Page/區議會.md "wikilink")[議員有兩席](../Page/議員.md "wikilink")，分別有[民主黨籍的](../Page/香港民主黨.md "wikilink")[甘乃威及](../Page/甘乃威.md "wikilink")[伍凱欣](../Page/伍凱欣.md "wikilink")。

## 歷史

上環原是[四環九約裏其中一環](../Page/四環九約.md "wikilink")。自1843年以來，上環一直是[華人聚居的地方](../Page/華人.md "wikilink")，[英國人及其他外國人則住在](../Page/英國.md "wikilink")[中環](../Page/中環.md "wikilink")。1874年桂文燦編纂的《廣東圖說》[同治刊本](../Page/同治.md "wikilink")，當中以[群帶路標誌為上環](../Page/阿群帶路圖.md "wikilink")、[中環](../Page/中環.md "wikilink")、[下環](../Page/灣仔.md "wikilink")。而1866年的《新安縣全圖》，當時的[群帶路所標示的位置亦在上環現址相近](../Page/阿群帶路圖.md "wikilink")。

由1850年代開始，隨著[太平天國之亂中逃難華人帶來資金和營商經驗](../Page/太平天國.md "wikilink")，上環發展成華人主要商貿區。[蘇杭街和](../Page/蘇杭街.md "wikilink")[文咸西街由於接近當時海邊的](../Page/文咸西街.md "wikilink")[三角碼頭](../Page/三角碼頭.md "wikilink")，故成為了香港早期[轉口貿易集中地](../Page/轉口.md "wikilink")。[干諾道西為當時的食](../Page/干諾道.md "wikilink")[米批發中心](../Page/稻.md "wikilink")；[德輔道西一帶則是](../Page/德輔道.md "wikilink")[海味和](../Page/海味.md "wikilink")[鹹魚的集散地](../Page/鹹魚.md "wikilink")。此後，上環一直隨著[香港經濟而發展](../Page/香港經濟.md "wikilink")。

## 特色

上環是香港的商業區，不少中資企業的辦事處設在該區。在今日的[香港交易所的前身](../Page/香港交易所.md "wikilink")「[聯合交易所](../Page/聯合交易所.md "wikilink")」成立之前，香港的股票及期貨主要由4個交易場所負責。當中的「[金銀業貿易場](../Page/香港金銀業貿易場.md "wikilink")」及其他2家交易所就在上環的[蘇杭街](../Page/蘇杭街.md "wikilink")。

上環也是香港的[海味及](../Page/海味.md "wikilink")[中藥的集散地](../Page/中藥.md "wikilink")。中藥店林立的[文咸東街與](../Page/文咸東街.md "wikilink")[文咸西街](../Page/文咸西街.md "wikilink")，有「[南北行](../Page/南北行.md "wikilink")」之稱
；海味店則集中在[德輔道西一帶](../Page/德輔道.md "wikilink")，老一輩稱呼該區為「[三角碼頭](../Page/三角碼頭.md "wikilink")」。

上環-{荷里活}-道及[摩羅街一帶](../Page/摩羅街.md "wikilink")，以[古玩店著名](../Page/古玩.md "wikilink")，吸引不少中外遊客在此尋寶。

## 著名地點、街道及建築物

### 地點及街道

  - [摩利臣街](../Page/摩利臣街.md "wikilink")
  - [蘇杭街](../Page/蘇杭街.md "wikilink")
  - [永樂街](../Page/永樂街.md "wikilink")
  - [高陞街](../Page/高陞街.md "wikilink")（藥材街）
  - [文咸西街](../Page/文咸西街.md "wikilink")
  - [蘇豪區](../Page/蘇豪區.md "wikilink")
  - [文咸西街](../Page/文咸西街.md "wikilink")

### 建築物

  - [西港城](../Page/西港城.md "wikilink")
  - [信德中心](../Page/信德中心.md "wikilink")
  - [新紀元廣場](../Page/新紀元廣場.md "wikilink")
  - [中遠大廈](../Page/中遠大廈.md "wikilink")
  - [無限極廣場](../Page/無限極廣場.md "wikilink")
  - [永安中心](../Page/永安中心.md "wikilink")
  - [上環文娛中心](../Page/上環文娛中心.md "wikilink")
  - [荷李活商業中心](../Page/荷李活商業中心_\(上環\).md "wikilink")
  - [文武廟](../Page/文武廟.md "wikilink")
  - [帝-{后}-華庭](../Page/帝后華庭.md "wikilink")
  - [中環麗柏酒店](../Page/中環麗柏酒店.md "wikilink")

<File:Western> Market Overview
201008.jpg|上環的地標——[西港城](../Page/西港城.md "wikilink")|alt=愛德華時代風格的紅磚造建築物，正門上方寫了“WESTERN
MARKET” <File:Shun> Tak Centre Overview
201105.jpg|[信德中心和](../Page/信德中心.md "wikilink")[港澳碼頭是香港居民前住](../Page/港澳碼頭_\(香港\).md "wikilink")[澳門的主要途徑](../Page/澳門.md "wikilink")
<File:HK> SW Tram Station 60421 FX
street.jpg|[急庇利街附近有不少找換店](../Page/急庇利街.md "wikilink")|alt=外幣找換店
<File:HK> SW Tram Station
60421.jpg|[上環電車總站](../Page/上環電車總站.md "wikilink")|alt=電車站
<File:Upper> Lascar Row view
201705.jpg|[摩羅街](../Page/摩羅街.md "wikilink")|alt=摩羅街
<File:HK> R10 Sheung Wan Wing Lok Street Hoi Fung Hong
Evening.jpg|[永樂街開設各類海味商店](../Page/永樂街.md "wikilink")|alt=海產乾貨

## 交通

### 來往澳門客運服務

過去香港和[澳門的海上交通就只靠位於上環的](../Page/澳門.md "wikilink")[港澳碼頭](../Page/港澳客輪碼頭.md "wikilink")。但隨着[尖沙咀](../Page/尖沙咀.md "wikilink")[中港碼頭陸續提供來往澳門的航運服務](../Page/中國客運碼頭.md "wikilink")，信德中心的港澳碼頭就不再享有這個專利。

此外，上環[信德中心的天台設有直升機場](../Page/信德中心.md "wikilink")，提供來往香港至澳門的直升機客運航線。

### 主要交通幹道

  - [干諾道中](../Page/干諾道.md "wikilink")、西
  - [皇后大道中](../Page/皇后大道.md "wikilink")、西
  - [德輔道中](../Page/德輔道.md "wikilink")、西
  - [堅道](../Page/堅道.md "wikilink")
  - [中環灣仔繞道](../Page/中環灣仔繞道.md "wikilink")
  - [羅便臣道](../Page/羅便臣道.md "wikilink")

### 公共交通

<div class="NavFrame collapsed" style="background-color: #FFFF00;clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

交通路線列表

</div>

<div class="NavContent" style="background-color: #FFFF00;margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[上環站](../Page/上環站.md "wikilink")、[西營盤站](../Page/西營盤站.md "wikilink")

<!-- end list -->

  - [電車](../Page/香港電車.md "wikilink")
    [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

<!-- end list -->

  - 紅色小巴

<!-- end list -->

  - 西環－銅鑼灣
  - 荃灣－灣仔
  - 荃灣－上環

</div>

</div>

## 區議會議席分佈

為方便比較，以下列表會以東至[鴨巴甸街](../Page/鴨巴甸街.md "wikilink")、[永吉街](../Page/永吉街.md "wikilink")、[德輔道](../Page/德輔道.md "wikilink")、[永和街](../Page/永和街.md "wikilink")，西至[威利麻街](../Page/威利麻街.md "wikilink")、南至[堅道](../Page/堅道.md "wikilink")、[般咸道](../Page/般咸道.md "wikilink")(包括[太平山區](../Page/太平山區.md "wikilink"))為範圍。

<table>
<thead>
<tr class="header">
<th><p>年度/範圍</p></th>
<th><p>2000-2003</p></th>
<th><p>2004-2007</p></th>
<th><p>2008-2011</p></th>
<th><p>2012-2015</p></th>
<th><p>2016-2019</p></th>
<th><p>2020-2023</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/鴨巴甸街.md" title="wikilink">鴨巴甸街</a>、<a href="../Page/永吉街.md" title="wikilink">永吉街</a>、<a href="../Page/永和街.md" title="wikilink">永和街以西至</a><a href="../Page/急庇利街.md" title="wikilink">急庇利街以東</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/急庇利街.md" title="wikilink">急庇利街以西至</a><a href="../Page/威利麻街.md" title="wikilink">威利麻街以東</a>，<a href="../Page/荷李活道.md" title="wikilink">荷李活道以北</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/荷李活道.md" title="wikilink">荷李活道以南</a>、<a href="../Page/鴨巴甸街.md" title="wikilink">鴨巴甸街以西</a>、<a href="../Page/般咸道.md" title="wikilink">般咸道</a>、<a href="../Page/堅道.md" title="wikilink">堅道以北</a>、<a href="../Page/東邊街.md" title="wikilink">東邊街以東</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

註：以上主要範圍尚有其他細微調整（包括編號），請參閱有關區議會選舉選區分界地圖及條目。

## 註釋

## 參看

  - [中西區](../Page/中西區_\(香港\).md "wikilink")
  - [中環](../Page/中環.md "wikilink")
  - [太平山區](../Page/太平山區.md "wikilink")
  - [西營盤](../Page/西營盤.md "wikilink")
  - [半山區](../Page/半山區.md "wikilink")

{{-}}

{{-}}

[上環](../Category/上環.md "wikilink") [Category:中西區
(香港)](../Category/中西區_\(香港\).md "wikilink")
[Category:香港商業區](../Category/香港商業區.md "wikilink")