**上莱茵省**（Haut-Rhin）是[法国的第](../Page/法国.md "wikilink")68[省](../Page/省.md "wikilink")。

上莱茵省是[法国大革命期间](../Page/法国大革命.md "wikilink")，根据1789年12月22日的法律和1790年1月8日的条例，于1790年3月4日建立的。它北与[下莱茵省](../Page/下莱茵省.md "wikilink")，西与[孚日省](../Page/孚日省.md "wikilink")，以及西南与[贝尔福地区交界](../Page/贝尔福地区.md "wikilink")。同时，它的东侧沿[莱茵河与](../Page/莱茵河.md "wikilink")[德国](../Page/德国.md "wikilink")，南侧与[瑞士接壤](../Page/瑞士.md "wikilink")。历史上，[阿尔萨斯大部分地区曾经于](../Page/阿尔萨斯.md "wikilink")1871年根据[法兰克福条约被划归德国](../Page/法兰克福条约.md "wikilink")，留在法国的部分形成了贝尔福地区；1919年[凡尔赛条约将其重新划归法国](../Page/凡尔赛条约.md "wikilink")，但仍与贝尔福地区分离。

该省是法国最富裕的省份之一，[失业率很低](../Page/失业率.md "wikilink")。它的工业、商业、教育活动集中于经济中心[米卢斯](../Page/米卢斯.md "wikilink")（Mulhouse，人口270
000）周边，比如标致汽车的一个生产基地位于米卢斯。[米卢斯](../Page/米卢斯.md "wikilink")，[盖布维莱尔和](../Page/盖布维莱尔.md "wikilink")[坦恩所形成的三角地带构成了经济最活跃的地区](../Page/坦恩_\(上莱茵省\).md "wikilink")，特别是其北端，商业网点的建立带动了就业机会的增加以及商业区的扩张。许多当地居民在瑞士境内工作，特别是服务于[巴塞尔的](../Page/巴塞尔.md "wikilink")[化学工业](../Page/化学工业.md "wikilink")，同时享受本地相对低廉的物价。

当地的方言为[阿尔萨斯语](../Page/阿尔萨斯语.md "wikilink")。

[Category:法国省份](../Category/法国省份.md "wikilink")
[H](../Category/大東部大區.md "wikilink")