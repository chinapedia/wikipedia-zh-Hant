<div style="float: right; margin: 0 0 1em 2em; width: 24em; text-align: right; font-size: 0.86em; line-height: normal;">

<div style="border: 1px solid #ccd2d9; background: #f0f6fa; text-align: left; padding: 0.5em 1em; text-align: center;">

<div style="text-align: center;">

<big>**上越教育大学**</big>
[Joetsu_University_of_Education.jpg](https://zh.wikipedia.org/wiki/File:Joetsu_University_of_Education.jpg "fig:Joetsu_University_of_Education.jpg")

</div>

<table>
<thead>
<tr class="header">
<th><p>大学設置</p></th>
<th><p>1978年</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>創立</p></td>
<td><p>1978年</p></td>
</tr>
<tr class="even">
<td><p>学校種別</p></td>
<td><p><a href="../Page/国立大学.md" title="wikilink">国立</a></p></td>
</tr>
<tr class="odd">
<td><p>設置者</p></td>
<td><p><a href="../Page/国立大学法人上越教育大学.md" title="wikilink">国立大学法人上越教育大学</a></p></td>
</tr>
<tr class="even">
<td><p>本部所在地</p></td>
<td><p>新潟県上越市山屋敷町1番地</p></td>
</tr>
<tr class="odd">
<td><p>校區</p></td>
<td><p>山屋敷地区、西城地区、本城地区</p></td>
</tr>
<tr class="even">
<td><p>学部</p></td>
<td><p><a href="../Page/学校教育学部.md" title="wikilink">学校教育学部</a></p></td>
</tr>
<tr class="odd">
<td><p>研究科</p></td>
<td><p>学校教育研究科<br />
連合学校教育学研究科</p></td>
</tr>
<tr class="even">
<td><p>網站</p></td>
<td><p><a href="http://www.juen.ac.jp/">上越教育大学官方網站</a></p></td>
</tr>
</tbody>
</table>

</div>

</div>

**上越教育大学**（[日文](../Page/日文.md "wikilink")：、Joetsu University of
Education），位于[新潟县](../Page/新潟县.md "wikilink")[上越市山屋敷町的](../Page/上越市.md "wikilink")[日本国立大学](../Page/日本.md "wikilink")。

设立宗旨是向初级中等教育教师提供研究钻研的机会，并培养初级教育教师，于1978年设立。

大学院硕士課程由具有10多年教育经验的中坚教员，重视实践并且重视与本科生的交流，是本大学的一大特征。

校内设有为单身的本科生和硕士生准备的单身宿舍，也有适合家族居住的家族宿舍。

## 学部・学科

  - 学校教育学部

## 大学院

  - 学校教育研究科（硕士課程）
  - 联合学校教育学研究科（博士課程）

## 外部链接

  - [上越教育大学主页](http://www.juen.ac.jp/)

[Category:新潟縣的大學](../Category/新潟縣的大學.md "wikilink")
[Category:日本的國立大學](../Category/日本的國立大學.md "wikilink")
[Category:1978年創建的教育機構](../Category/1978年創建的教育機構.md "wikilink")
[Category:日本師範院校](../Category/日本師範院校.md "wikilink")
[Category:上越市](../Category/上越市.md "wikilink")