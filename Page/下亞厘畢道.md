[HK_Lower_Albert_Road_18_br.JPG](https://zh.wikipedia.org/wiki/File:HK_Lower_Albert_Road_18_br.JPG "fig:HK_Lower_Albert_Road_18_br.JPG")
**下亞厘畢道**（[英語](../Page/英語.md "wikilink")：**Lower Albert
Road**）是[香港](../Page/香港.md "wikilink")[香港島](../Page/香港島.md "wikilink")[中環一處有名的街道](../Page/中環.md "wikilink")，因為[香港政府總部曾設於該處](../Page/香港政府總部.md "wikilink")，常有遊行示威引致交通問題，而經常受到[傳媒關注](../Page/香港傳媒.md "wikilink")。

該處的[中區政府合署有五十多年歷史](../Page/中區政府合署.md "wikilink")，是一座以簡約主義風格的行政大廈建築物，於政府總部搬遷時，也備受[環保及](../Page/環保.md "wikilink")[政治愛好者關注](../Page/政治.md "wikilink")。政府總部遷往[金鐘](../Page/金鐘.md "wikilink")[添馬後](../Page/添馬.md "wikilink")，中區政府合署改為律政中心。

下亞厘畢道連同[上亞厘畢道是以](../Page/上亞厘畢道.md "wikilink")[維多利亞女皇夫婿](../Page/維多利亞女皇.md "wikilink")[亞厘畢親王](../Page/亞厘畢親王.md "wikilink")（Prince
Albert of Saxe-Coburg and Gotha）命名。

## 位置

[Lower_Albert_Road_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Lower_Albert_Road_\(Hong_Kong\).jpg "fig:Lower_Albert_Road_(Hong_Kong).jpg")
下亞厘畢道一條大弓字形的街道西東開建在[香港禮賓府與中區政府合署之間](../Page/香港禮賓府.md "wikilink")，西接[中環](../Page/中環.md "wikilink")[雲咸街](../Page/雲咸街.md "wikilink")，起自[藝穗會](../Page/藝穗會.md "wikilink")，經過港中醫院，見[上亞厘畢道靠左](../Page/上亞厘畢道.md "wikilink")，分別經中區政府合署西座及中區政府合署東座，經[美國駐香港總領事館](../Page/美國駐香港總領事館.md "wikilink")，入[花園道止](../Page/花園道.md "wikilink")。

## 相關

  - [上亞厘畢道](../Page/上亞厘畢道.md "wikilink")
  - [香港禮賓府](../Page/香港禮賓府.md "wikilink")
  - 香港聖公會福利協會，在下亞厘畢道1A

## 外部参考

  - [下亞厘畢道中原地圖](http://www.centamap.com/scripts/centamapgif.asp?lg=B5&tp=2&sx=&sy=&sl=&ss=0&mx=834155&my=815619&vm=&ly=&lb=&ms=2&ca=0&x=834260&y=815619&z=2)
  - [下亞厘畢道古樹名冊](http://www.lcsd.gov.hk/green/tree/dbh.php?sid=225&page=2&lang=b5)
  - [上亞厘畢道防空隧道網絡](http://www.hk-place.com/view.php?id=248)
  - [藝穗會所在的建築物是建於1890年](https://web.archive.org/web/20060426234629/http://www.info.gov.hk/yearbook/2003/tc_chi/chapter19/19_06.html)

[Category:中環街道](../Category/中環街道.md "wikilink")
[Category:冠以人名的香港道路](../Category/冠以人名的香港道路.md "wikilink")