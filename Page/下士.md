[Taiwan-army-OR-4.svg](https://zh.wikipedia.org/wiki/File:Taiwan-army-OR-4.svg "fig:Taiwan-army-OR-4.svg")**下士**是一種[軍銜](../Page/軍銜.md "wikilink")，在[中華民國國軍](../Page/中華民國國軍.md "wikilink")[軍銜制度中](../Page/中華民國國軍軍銜.md "wikilink")，下士為[士官的最低階](../Page/士官.md "wikilink")，下士之下的階級為[上等兵](../Page/上等兵.md "wikilink")，下士之上的階級為[中士](../Page/中士.md "wikilink")，在[軍隊中通常擔任](../Page/軍隊.md "wikilink")[班長或副班長職務](../Page/班長.md "wikilink")。

中華民國-{的}-士官（包含士兵）共分九級，下士相當於[美國陸軍的Corporal](../Page/美國陸軍.md "wikilink")，[美國空軍的Senior](../Page/美國空軍.md "wikilink")
Airman，或[美國海軍的Petty](../Page/美國海軍.md "wikilink") Officer 3rd Class;

[日本的](../Page/日本.md "wikilink")「下士官」是[中文的士官階級的通稱](../Page/中文.md "wikilink")，包含[軍曹](../Page/軍曹.md "wikilink")、[曹長等職務](../Page/曹長.md "wikilink")。而[日語中的](../Page/日語.md "wikilink")「士官」實際上等於[漢語的](../Page/漢語.md "wikilink")[軍官](../Page/軍官.md "wikilink")，俗稱[將校](../Page/將校.md "wikilink")。

##

下士，是[中華民國](../Page/中華民國.md "wikilink")[義務役役男經過](../Page/義務役.md "wikilink")[預官預士考試後](../Page/預官預士考試.md "wikilink")，所擔任的[軍階之一](../Page/軍階.md "wikilink")。具有[專科學校以上學歷的役男](../Page/專科.md "wikilink")，經由考試取得預備士官資格者，於入伍訓及專長訓後，授予下士士官官階。

如役男未能參加[預官預士考試者](../Page/預官預士考試.md "wikilink")，也可於服役期間申請，或是經營挑選參加各種士官訓練班隊，如士官班、士訓隊、幹訓班、專業士官班（送達各個專業學校受訓預備士官）等，結訓後即可成為下士。

##

[PLA_Corporal.svg](https://zh.wikipedia.org/wiki/File:PLA_Corporal.svg "fig:PLA_Corporal.svg")

[中国人民解放军](../Page/中国人民解放军.md "wikilink")1955年军衔制中设立有下士军衔，属于义务兵军衔，可以由[上等兵自然晋升](../Page/上等兵.md "wikilink")。1988年军衔制继续设立下士，同1955年军衔制。1999年解放军实行士官制度，义务兵服役年限仅为两年，取消下士、[中士和](../Page/中士.md "wikilink")[上士军衔](../Page/上士.md "wikilink")，改设1-6级士官，对应下士的为一级士官。2009年将6个士官级别改为7个级别，并改变称谓，将一级士官改为下士，恢复了下士这一军衔。现中国人民解放军下士属于志愿兵军衔，上等兵需和部队订立志愿兵服役协议转为士官后，方可获下士军衔。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[Category:军衔](../Category/军衔.md "wikilink")

1.