[2013_09_26_SL_Foot_Patrol_F.jpg_(9977471684).jpg](https://zh.wikipedia.org/wiki/File:2013_09_26_SL_Foot_Patrol_F.jpg_\(9977471684\).jpg "fig:2013_09_26_SL_Foot_Patrol_F.jpg_(9977471684).jpg")
**下朱巴州**（[索馬里語](../Page/索馬里語.md "wikilink")：）是[索馬里最南部的一個州](../Page/索馬里.md "wikilink")，西鄰[肯尼亞](../Page/肯尼亞.md "wikilink")，東臨[印度洋](../Page/印度洋.md "wikilink")。[朱巴河在本省入海](../Page/朱巴河.md "wikilink")。面積42,876平方公里。首府[基斯馬尤](../Page/基斯馬尤.md "wikilink")。

[J](../Category/索馬里行政區劃.md "wikilink")
[\*](../Category/下朱巴州.md "wikilink")