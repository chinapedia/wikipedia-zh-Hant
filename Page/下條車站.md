**下條車站**（）是一個位於[日本](../Page/日本.md "wikilink")[新潟縣](../Page/新潟縣.md "wikilink")[十日町市下条四丁目](../Page/十日町市.md "wikilink")，由[東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")（JR東日本）所經營的[鐵路車站](../Page/鐵路車站.md "wikilink")。下条車站是JR東日本所屬的[地方交通線鐵路](../Page/地方交通線.md "wikilink")、[飯山線沿線的一個](../Page/飯山線.md "wikilink")[無人小站](../Page/無人車站.md "wikilink")，在營運上是由[JR東日本長野支社負責管理](../Page/JR東日本長野支社.md "wikilink")。

## 車站結構

[側式月台](../Page/側式月台.md "wikilink")1面1線的[地面車站](../Page/地面車站.md "wikilink")\[1\]。曾經為[對向式月台](../Page/對向式月台.md "wikilink")2面2線\[2\]。軌道的西側為月台，再西側設有站舍。

## 相鄰車站

  - 東日本旅客鐵道

    飯山線

      -

        －**下條**－

## 外部連結

  - [下条車站（JR東日本）](http://www.jreast.co.jp/estation/station/info.aspx?StationCd=651)

[Jou](../Category/日本鐵路車站_Ge.md "wikilink")
[Category:新潟縣鐵路車站](../Category/新潟縣鐵路車站.md "wikilink")
[Category:飯山線車站](../Category/飯山線車站.md "wikilink")
[Category:1927年启用的铁路车站](../Category/1927年启用的铁路车站.md "wikilink")
[Category:十日町市](../Category/十日町市.md "wikilink")
[Category:1927年日本建立](../Category/1927年日本建立.md "wikilink")

1.
2.