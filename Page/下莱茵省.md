**下莱茵省**（）是[法国的一个省](../Page/法国.md "wikilink")，属于[大东部大区](../Page/大东部大区.md "wikilink")，编号为67。

## 历史

下莱茵省是法国大革命期间，根据1789年12月22日的法律，于1790年3月4日建立的。它南与[上莱茵省](../Page/上莱茵省.md "wikilink")，西南与[孚日省](../Page/孚日省.md "wikilink")、[默尔特-摩泽尔省](../Page/默尔特-摩泽尔省.md "wikilink")，西与[摩泽尔省接壤](../Page/摩泽尔省.md "wikilink")。它的东侧沿着[莱茵河](../Page/莱茵河.md "wikilink")，
以及北侧与[德国接壤](../Page/德国.md "wikilink")。

1871年，根据[法兰克福条约](../Page/法兰克福条约.md "wikilink")，该省全境被划归德国；1919年[凡尔赛条约将其重新划归法国](../Page/凡尔赛条约.md "wikilink")。

## 名称

有趣的是，下莱茵省是法国仅存的含“下”（Bas）字修饰的省份。由于该字隐含的贬义，其余省份都改用更中性的命名，比如：下比利牛斯省于1969年更名为[比利牛斯-大西洋省](../Page/比利牛斯-大西洋省.md "wikilink")，下阿尔卑斯省于1970年更名为[上普罗旺斯阿尔卑斯省](../Page/上普罗旺斯阿尔卑斯省.md "wikilink")。同样的现象也出现在原来名字含有“内”（Inférieur）字的省份。

[\*](../Category/下莱茵省.md "wikilink")
[Category:法国省份](../Category/法国省份.md "wikilink")
[Category:大东部大区行政区划](../Category/大东部大区行政区划.md "wikilink")