**不浪·尤幹**（漢名：**黃建福**，），[台灣原住民](../Page/台灣原住民.md "wikilink")[泰雅族人](../Page/泰雅族.md "wikilink")\[1\]，[台灣](../Page/台灣.md "wikilink")[歌手與](../Page/歌手.md "wikilink")[音樂製作人](../Page/音樂製作人.md "wikilink")。曾為[張惠妹](../Page/張惠妹.md "wikilink")、[任賢齊及](../Page/任賢齊.md "wikilink")[王力宏等歌手製作音樂](../Page/王力宏.md "wikilink")，並於[第39屆金馬獎中以電影](../Page/第39屆金馬獎.md "wikilink")《[夢幻部落](../Page/夢幻部落.md "wikilink")》獲得最佳原創配樂獎。2005年12月31日晚間22時55分左右，他在[北京](../Page/北京.md "wikilink")[首都體育館於歌手](../Page/首都體育館.md "wikilink")[齊秦的](../Page/齊秦.md "wikilink")2006年[跨年](../Page/跨年.md "wikilink")[演唱會上表演時發生意外](../Page/演唱會.md "wikilink")，掉落於3公尺深的舞台底下，送往醫院急救後仍於次日凌晨1時20分左右證實死亡，得年37歲。\[2\]

## 作品

### 專輯

  - 黑皮伍肆叁《Happy 543》

### 歌曲

  - Jealous Guy

### 譜曲

  - [張惠妹](../Page/張惠妹.md "wikilink")〈好想見你〉
  - [動力火車](../Page/動力火車.md "wikilink")〈彩虹〉
  - 〈風中小米田〉
  - 〈瑪雅的彩虹〉
  - 〈我遺忘已久的泰雅〉

## 獎項

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>項目</p></th>
<th><p>類別</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2001</p></td>
<td><p>瑪雅的彩虹</p></td>
<td><p><a href="../Page/第36屆金鐘獎.md" title="wikilink">第36屆金鐘獎音效獎</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td><p>夢幻部落</p></td>
<td><p><a href="../Page/第39屆金馬獎.md" title="wikilink">第39屆金馬獎最佳原創電影音樂</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  -
  -
[Bulang](../Category/台灣原住民歌手.md "wikilink")
[Bulang](../Category/臺灣電影配樂家.md "wikilink")
[Bulang](../Category/台灣音樂製作人.md "wikilink")
[Bulang](../Category/金馬獎最佳電影音樂獲獎者.md "wikilink")
[Category:金鐘獎音效得主](../Category/金鐘獎音效得主.md "wikilink")
[Category:新北市私立南強高級工商職業學校校友](../Category/新北市私立南強高級工商職業學校校友.md "wikilink")
[Bulang](../Category/臺灣意外身亡者.md "wikilink")
[Category:臺灣新教徒](../Category/臺灣新教徒.md "wikilink")
[Bulang](../Category/泰雅族人.md "wikilink")
[Bulang](../Category/烏來人.md "wikilink")

1.  [gCache](https://webcache.googleusercontent.com/search?q=cache:http://tour.ntpc.gov.tw/page.aspx%3Fwtp=1%26wnd=163%26ntp=1%26nid=3952)
2.