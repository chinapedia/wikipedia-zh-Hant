[MAI_KOFXIII_stance.gif](https://zh.wikipedia.org/wiki/File:MAI_KOFXIII_stance.gif "fig:MAI_KOFXIII_stance.gif")，在《[拳皇XIII](../Page/拳皇.md "wikilink")》裡的戰鬥姿態\]\]
**不知火舞**（）是[电视游戏](../Page/电视游戏.md "wikilink")《[餓狼傳說](../Page/餓狼傳說.md "wikilink")》、《[拳皇](../Page/拳皇.md "wikilink")》系列中的女性角色之一。为当代忍者，其祖父[不知火半藏为](../Page/不知火半藏.md "wikilink")[不知火流](../Page/不知火.md "wikilink")[忍术的大师](../Page/忍术.md "wikilink")。不知火舞最先登場於《餓狼傳說2》，以大扇子和火焰作為武器。她身材纖細胸部卻非常豐滿，身穿露出度極高的红色忍者裝束。

她愛上了在其祖父門下修行的[安迪·柏格](../Page/安迪·柏格.md "wikilink")（アンディ・ボガードAndy
Bogard），而在追蹤安迪時投入了戰鬥中。最初的[拳皇](../Page/拳皇.md "wikilink")、拳皇'99、拳皇EX及EX
2中和安迪組隊，其他情況下都是女性格鬪家隊的隊員。並無在XI中出場。是為[春麗之後第二位對戰型格鬪遊戲的女性角色](../Page/春麗.md "wikilink")。因此在[CAPCOM](../Page/CAPCOM.md "wikilink")
VS. [SNK中和春麗對戰前有專用片段存在](../Page/SNK.md "wikilink")。

## 其他

  - 使用大[扇子伴隨多種類別的攻擊](../Page/扇子.md "wikilink")，不過主要還是以[忍術為主](../Page/忍術.md "wikilink")。
  - 在[侍魂系列中出場的角色](../Page/侍魂.md "wikilink")『不知火幻庵』和外觀衣著都很相似的女忍者『不知火麻衣』（日语发音同不知火舞）是完全無關的人物。
  - 對於KOF94時就將她撇下的安迪非常不滿，卻也因此結識阪崎百合和金並成為朋友（但也常常向2人抱怨安迪的不是），金所開設的酒吧常客。
  - 雖然對安迪年年拋下她組隊參與KOF感到耿耿於懷（除99年外），但在女性格鬥家隊很照顧其他女性成員。
  - 在跟安迪的感情上，她主要處於比較主動的地位，一有空馬上對安迪逼婚（而且有越來越嚴重的趨勢，KOF99如果餓狼隊由不知火舞打倒其魔王，就會出現逼婚的結局）。
  - 安迪新收的徒弟北斗丸似乎也很怕她，因為有時見不到安迪的舞會在北斗丸面前發火所以成了他唯一害怕的人物。
  - 擁有傳統日本女性溫柔的一面，同時不論[茶道](../Page/茶道.md "wikilink")[插花和](../Page/插花.md "wikilink")[料理都非常的得心應手](../Page/料理.md "wikilink")，同時也有彈出一手好琴的本領。
  - 似乎再也無法忍受每次被安迪拋下，於XI'強制安迪和自己放假兼約會，於是首度在KOF系列缺席。
  - XIII'復歸參賽，並於賽後前往參加女性格鬥家隊的聚會，因雛子無心的一句話而和眾女格鬥家之間發生混戰。
  - 曾視布魯・瑪莉為自己的假想情敵（因為跟安迪一樣是外國人）。另外也視東丈為安迪的豬朋狗友，因此有時兩人一見面也會吵架。
  - 私下穿著和普通女性沒有兩樣。
  - 與安迪的特殊開場有穿[婚紗禮服和變出幼兒叫安迪爸爸的場景](../Page/婚紗.md "wikilink")。通常安迪都會被嚇傻而必須打自己一拳才能開始格鬥比賽。
  - 在GBA版｢慾望之血EX｣中如果由餓狼隊打贏最終BOSS(而且是舞打倒時)，結局場景會在金的酒吧。舞試圖逼婚，但被東丈和獅鷲假面給阻止，因此遭到舞的不滿。

## 聲優

  - [曾木亞古彌](../Page/曾木亞古彌.md "wikilink")（各種遊戲及動畫「The King of
    Fighters:Another Day」）
  - [三石琴乃](../Page/三石琴乃.md "wikilink")（動畫「Battle Fighters餓狼伝說」、「餓狼伝說
    -THE MOTION PICTURE-」）
  - [小清水亞美](../Page/小清水亞美.md "wikilink")（KOF SKY STAGE、KOF XIII、KOF XIV）
  - [林原惠](../Page/林原惠.md "wikilink")（廣播CD『餓狼伝説 宿命の闘い No.1 復讐の狼、No.2
    孤高の狼』、『餓狼伝説2 〜新たなる闘い〜」、『餓狼伝説スペシャル』）

## 登場作品

  - [餓狼傳說](../Page/餓狼傳說.md "wikilink")
  - [拳皇](../Page/拳皇.md "wikilink")
  - [CAPCOM VS SNK](../Page/CAPCOM_VS_SNK.md "wikilink")
  - [生死格鬥5](../Page/生死格鬥5.md "wikilink")
  - [王者荣耀](../Page/王者荣耀.md "wikilink")
  - [Crash Fever](../Page/Crash_Fever.md "wikilink")
  - [神魔之塔](../Page/神魔之塔.md "wikilink")
  - [如果的世界](../Page/如果的世界.md "wikilink")

## 關連項目

  - [拳皇角色列表](../Page/拳皇角色列表.md "wikilink")

## 注釋

[Category:虚构日本人电子游戏角色](../Category/虚构日本人电子游戏角色.md "wikilink")
[Category:虚构女忍者](../Category/虚构女忍者.md "wikilink")
[Category:虛構炎能力者](../Category/虛構炎能力者.md "wikilink")
[Category:拳皇系列角色](../Category/拳皇系列角色.md "wikilink")
[Category:忍者電子遊戲角色](../Category/忍者電子遊戲角色.md "wikilink")
[Category:饿狼传说系列角色](../Category/饿狼传说系列角色.md "wikilink")
[Category:電子遊戲吉祥物](../Category/電子遊戲吉祥物.md "wikilink")