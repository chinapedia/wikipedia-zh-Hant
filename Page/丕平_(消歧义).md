**丕平**（，，，）是[加洛林王朝好几位统治者的名字](../Page/加洛林王朝.md "wikilink")，可以指：

## 人名

### 加洛林王朝統治者

  - **[兰登的丕平](../Page/兰登的丕平.md "wikilink")（Pepin of
    Landen）**（580年－640年2月27日），即**[丕平一世](../Page/丕平一世.md "wikilink")**，有时认为他是一个[圣徒](../Page/圣徒.md "wikilink")。
  - **[丕平二世](../Page/丕平二世.md "wikilink")（Pepin of
    Herstal）**，又称**赫斯托尔的丕平**（635年－714年12月16日），687年作为[宫相成为](../Page/宫相.md "wikilink")[法兰克人的实际统治者](../Page/法兰克王国.md "wikilink")。
  - **[丕平三世](../Page/丕平三世.md "wikilink")**，又称**矮子丕平（Pepin the
    Short）**（714年－768年9月24日），[法兰克国王](../Page/法兰克.md "wikilink")（751年－768年），[查理·马特的儿子](../Page/查理·马特.md "wikilink")，[查理曼的父亲](../Page/查理曼.md "wikilink")，[加洛林王朝的创建者](../Page/加洛林王朝.md "wikilink")。
  - **[驼背丕平](../Page/驼背丕平.md "wikilink")（Pepin the
    Hunchback）**（767年－813年），一稱[丕平四世](../Page/丕平四世.md "wikilink")。查理曼的长子，[丕平三世的孙子](../Page/丕平三世.md "wikilink")。792年企图暗杀父亲查理曼，败露后被送往修道院度过余生。
  - **[意大利的丕平](../Page/意大利的丕平.md "wikilink")（Pepin of
    Italy）**（773年4月－810年7月8日），查理曼的儿子。原名“卡洛曼”（Carloman）,曾任[意大利国王](../Page/意大利.md "wikilink")（781年－810年），死后，查理曼让他的儿子[義大利的伯纳德继承了意大利王位](../Page/義大利的伯纳德.md "wikilink")。
  - **[阿奎丹的丕平一世](../Page/阿基坦丕平.md "wikilink")（Pepin I of
    Aquitaine）**（797年－838年11月或12月13日），[虔诚者路易的儿子](../Page/虔诚者路易.md "wikilink")，查理曼的孙子。
  - **[阿奎丹的丕平二世](../Page/丕平二世_\(阿基坦\).md "wikilink")（Pepin II of
    Aquitaine）**（823年－864年），[阿奎丹的丕平一世的儿子](../Page/阿基坦丕平.md "wikilink")。
  - **[韦尔芒杜瓦伯爵丕平](../Page/丕平_\(韦尔芒杜瓦\).md "wikilink")（Pepin, Count of
    Vermandois）**（815年－），[意大利丕平的孙子](../Page/意大利丕平.md "wikilink")。

### 其他人名

  - [雅克·丕平](../Page/雅克·丕平.md "wikilink")（Jacques Pépin），美國知名廚師。

## 地名

  - [丕平郡
    (威斯康辛州)](../Page/丕平縣_\(威斯康辛州\).md "wikilink")，位於美國[威斯康辛州西北部的一個郡](../Page/威斯康辛州.md "wikilink")。

## 其他

  - [Apple
    Pippin](../Page/Apple_Pippin.md "wikilink")，一款[蘋果公司在](../Page/蘋果公司.md "wikilink")1990年代中期開發的遊戲機。

[P](../Category/人名譯名消歧義.md "wikilink")
[P](../Category/法国历史.md "wikilink")