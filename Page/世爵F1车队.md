**世爵車隊**（**Spyker F1
Team**）是由[荷蘭手工跑車生產廠](../Page/荷蘭.md "wikilink")[世爵](../Page/世爵汽車.md "wikilink")（Spyker）所成立的[一级方程式赛车车队](../Page/一级方程式赛车.md "wikilink")，创建于2006年7月。世爵车队在2006年賽季半途买下了在F1只有曇花一現、參賽不滿一年的的[米兰德车队](../Page/米兰德车队.md "wikilink")（Midland
F1 Racing），并将其车队更名为世爵车队（接手初期曾一度名為Spyker MF1 Racing，但在2007年賽季起正式改名為Spyker
F1 Team），
车队将之前米兰德车队红白相间的车色，更替为自己特色的橙色和银色的车身。2007年賽季快結束之際，[印度商人维加](../Page/印度.md "wikilink")·马尔雅（Vijay
Mallya）和荷兰商人Michiel
Mol所經營的控股公司收購了世爵車隊，对外报价为在8,000万[欧元左右](../Page/欧元.md "wikilink")，並在2008年賽季以[印度力量车队](../Page/印度力量车队.md "wikilink")（Force
India F1）之名出賽。

[Category:一級方程式車隊](../Category/一級方程式車隊.md "wikilink")
[Category:世爵](../Category/世爵.md "wikilink")