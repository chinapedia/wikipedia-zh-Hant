**世界体育竞赛列表**列表世界上各类[体育运动的](../Page/体育运动.md "wikilink")**国际性赛事**，也列表部分知名度较高的**本土聯賽**，按照体育运动的分类排列。

## 综合性运动会

### 國際性運動會

<table style="width:10%;">
<colgroup>
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 3%" />
</colgroup>
<thead>
<tr class="header">
<th><p>賽事名稱</p></th>
<th><p>舉辦機構</p></th>
<th><p>舉辦年份</p></th>
<th><p>舉辦週期</p></th>
<th><p>最近一次舉辦</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/夏季奧林匹克運動會.md" title="wikilink">夏季奧林匹克運動會</a></p></td>
<td><p><a href="../Page/國際奧林匹克委員會.md" title="wikilink">國際奧林匹克委員會</a></p></td>
<td><p>1896年－</p></td>
<td><p>每4年舉行一次</p></td>
<td><p><a href="../Page/2016年夏季奧林匹克運動會.md" title="wikilink">2016年夏季奧林匹克運動會</a>（<a href="../Page/里約熱內盧.md" title="wikilink">里約熱內盧</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/冬季奥林匹克运动会.md" title="wikilink">冬季奥林匹克运动会</a></p></td>
<td><p><a href="../Page/國際奧林匹克委員會.md" title="wikilink">國際奧林匹克委員會</a></p></td>
<td><p>1924年－</p></td>
<td><p>每4年舉行一次</p></td>
<td><p><a href="../Page/2018年冬季奧林匹克運動會.md" title="wikilink">2018年冬季奧林匹克運動會</a>（<a href="../Page/平昌.md" title="wikilink">平昌</a>）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/帕拉林匹克運動會.md" title="wikilink">夏季帕拉林匹克運動會</a></p></td>
<td><p><a href="../Page/國際帕拉林匹克委員會.md" title="wikilink">國際帕拉林匹克委員會</a></p></td>
<td><p>1960年－</p></td>
<td><p>每4年舉行一次</p></td>
<td><p><a href="../Page/2016年夏季帕拉林匹克運動會.md" title="wikilink">2016年夏季帕拉林匹克運動會</a>（<a href="../Page/里約熱內盧.md" title="wikilink">里約熱內盧</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/帕拉林匹克運動會.md" title="wikilink">冬季帕拉林匹克運動會</a></p></td>
<td><p><a href="../Page/國際帕拉林匹克委員會.md" title="wikilink">國際帕拉林匹克委員會</a></p></td>
<td><p>1976年－</p></td>
<td><p>每4年舉行一次</p></td>
<td><p><a href="../Page/2018年冬季帕拉林匹克運動會.md" title="wikilink">2018年冬季帕拉林匹克運動會</a>（<a href="../Page/平昌.md" title="wikilink">平昌</a>）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/特殊奥林匹克运动会.md" title="wikilink">夏季世界特殊奧林匹克運動會</a></p></td>
<td><p><a href="../Page/特殊奧林匹克運動會.md" title="wikilink">特殊奧林匹克運動會</a></p></td>
<td><p>1968年－</p></td>
<td><p>每4年舉行一次</p></td>
<td><p><a href="../Page/2015年世界夏季特殊奧林匹克運動會.md" title="wikilink">2015年世界夏季特殊奧林匹克運動會</a>（<a href="../Page/洛杉磯.md" title="wikilink">洛杉磯</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/特殊奥林匹克运动会.md" title="wikilink">冬季世界特殊奧林匹克運動會</a></p></td>
<td><p><a href="../Page/特殊奧林匹克運動會.md" title="wikilink">特殊奧林匹克運動會</a></p></td>
<td><p>1977年－</p></td>
<td><p>每4年舉行一次</p></td>
<td><p><a href="../Page/2017年世界冬季特殊奧林匹克運動會.md" title="wikilink">2017年世界冬季特殊奧林匹克運動會</a>（<a href="../Page/格拉茲.md" title="wikilink">格拉茲</a>、<a href="../Page/施拉德明.md" title="wikilink">施拉德明</a>）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/聽障奧林匹克運動會.md" title="wikilink">夏季聽障奧林匹克運動會</a></p></td>
<td><p><a href="../Page/國際聽障運動委員會.md" title="wikilink">國際聽障運動委員會</a></p></td>
<td><p>1924年－</p></td>
<td><p>每4年舉行一次</p></td>
<td><p><a href="../Page/2017年夏季特殊奧林匹克運動會.md" title="wikilink">2017年夏季特殊奧林匹克運動會</a>（<a href="../Page/萨姆松.md" title="wikilink">萨姆松</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/聽障奧林匹克運動會.md" title="wikilink">冬季聽障奧林匹克運動會</a></p></td>
<td><p><a href="../Page/國際聽障運動委員會.md" title="wikilink">國際聽障運動委員會</a></p></td>
<td><p>1949年－</p></td>
<td><p>每4年舉行一次</p></td>
<td><p><a href="../Page/2015年冬季聽障奧林匹克運動會.md" title="wikilink">2015年冬季聽障奧林匹克運動會</a>（<a href="../Page/漢特-曼西斯克.md" title="wikilink">漢特-曼西斯克</a>）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/友好运动会.md" title="wikilink">友好运动会</a></p></td>
<td></td>
<td><p>1986年－2001年</p></td>
<td><p>結束停辦</p></td>
<td><p>2001年友好运动会（<a href="../Page/布里斯班.md" title="wikilink">布里斯班</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/世界运动会.md" title="wikilink">世界运动会</a></p></td>
<td><p><a href="../Page/國際世界運動會協會.md" title="wikilink">國際世界運動會協會</a></p></td>
<td><p>1981年－</p></td>
<td><p>每4年舉行一次</p></td>
<td><p>2017年世界运动会（<a href="../Page/弗罗茨瓦夫.md" title="wikilink">弗罗茨瓦夫</a>）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夏季世界先進運動會.md" title="wikilink">夏季世界先進運動會</a></p></td>
<td><p>國際先進運動會協會</p></td>
<td><p>1985年－</p></td>
<td><p>每4年舉行一次</p></td>
<td><p>2013年夏季世界先進運動會（<a href="../Page/杜林.md" title="wikilink">杜林</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/冬季世界先進運動會.md" title="wikilink">冬季世界先進運動會</a></p></td>
<td><p>國際先進運動會協會</p></td>
<td><p>2010年－</p></td>
<td><p>每4年舉行一次</p></td>
<td><p><a href="../Page/2010年冬季世界先進運動會.md" title="wikilink">2010年冬季世界先進運動會</a>（<a href="../Page/布萊德.md" title="wikilink">布萊德</a>）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/青少年奧林匹克運動會.md" title="wikilink">夏季青少年奧林匹克運動會</a></p></td>
<td><p><a href="../Page/國際奧林匹克委員會.md" title="wikilink">國際奧林匹克委員會</a></p></td>
<td><p>2010年－</p></td>
<td><p>每4年舉行一次</p></td>
<td><p><a href="../Page/2014年夏季青少年奧林匹克運動會.md" title="wikilink">2014年夏季青少年奧林匹克運動會</a>（<a href="../Page/南京.md" title="wikilink">南京</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/青少年奧林匹克運動會.md" title="wikilink">冬季青少年奧林匹克運動會</a></p></td>
<td><p><a href="../Page/國際奧林匹克委員會.md" title="wikilink">國際奧林匹克委員會</a></p></td>
<td><p>2012年－</p></td>
<td><p>每4年舉行一次</p></td>
<td><p><a href="../Page/2016年冬季青少年奧林匹克運動會.md" title="wikilink">2016年冬季青少年奧林匹克運動會</a>（<a href="../Page/利勒哈默爾.md" title="wikilink">利勒哈默爾</a>）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/世界大学生运动会.md" title="wikilink">夏季世界大學生運動會</a></p></td>
<td><p><a href="../Page/國際大學生體育聯盟.md" title="wikilink">國際大學生體育聯盟</a></p></td>
<td><p>1959年－</p></td>
<td><p>每2年舉行一次</p></td>
<td><p><a href="../Page/2017年夏季世界大學生運動會.md" title="wikilink">2017年夏季世界大學生運動會</a>（<a href="../Page/臺北市.md" title="wikilink">臺北市</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/世界大学生运动会.md" title="wikilink">冬季世界大學生運動會</a></p></td>
<td><p><a href="../Page/國際大學生體育聯盟.md" title="wikilink">國際大學生體育聯盟</a></p></td>
<td><p>1960年－</p></td>
<td><p>每2年舉行一次</p></td>
<td><p><a href="../Page/2017年冬季世界大學生運動會.md" title="wikilink">2017年冬季世界大學生運動會</a>（<a href="../Page/阿拉木圖.md" title="wikilink">阿拉木圖</a>）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/世界中学生运动会.md" title="wikilink">世界中学生运动会</a></p></td>
<td><p><a href="../Page/國際中學生體育聯盟.md" title="wikilink">國際中學生體育聯盟</a></p></td>
<td><p>1974年－</p></td>
<td><p>不定期</p></td>
<td><p>2016年世界中学生运动会（<a href="../Page/特拉布宗.md" title="wikilink">特拉布宗</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/世界青年运动会.md" title="wikilink">世界青年运动会</a></p></td>
<td><p><a href="../Page/國際奧林匹克委員會.md" title="wikilink">國際奧林匹克委員會</a></p></td>
<td><p>1998年</p></td>
<td><p>只曾經舉辦一屆</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/同乐运动会.md" title="wikilink">同乐运动会</a></p></td>
<td><p><a href="../Page/同乐运动会联合会.md" title="wikilink">同乐运动会联合会</a></p></td>
<td><p>1982年－</p></td>
<td><p>每4年舉行一次</p></td>
<td><p>2018年（<a href="../Page/巴黎.md" title="wikilink">巴黎</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/世界同志運動會.md" title="wikilink">世界同志運動會</a></p></td>
<td></td>
<td><p>2006年－</p></td>
<td><p>每4年舉行一次</p></td>
<td><p>2017年（<a href="../Page/迈阿密海滩.md" title="wikilink">迈阿密海滩</a>）<small>注：大部分体育赛事取消，只进行了三个比赛项目。</small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/英联邦运动会.md" title="wikilink">英联邦运动会</a></p></td>
<td><p>英聯邦運動會聯盟</p></td>
<td><p>1930年－每3年舉行一次</p></td>
<td><p>每4年舉行一次</p></td>
<td><p>2014年英联邦运动会（<a href="../Page/格拉斯哥.md" title="wikilink">格拉斯哥</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/世界輪椅運動會.md" title="wikilink">世界輪椅運動會</a></p></td>
<td><p>International Wheelchair &amp; Amputee Sports Federation / IWAS</p></td>
<td><p>2005年－</p></td>
<td><p>每2年舉行一次</p></td>
<td><p>2015年斯托克·曼德維爾Stoke Mandevill運動會（<a href="../Page/索契.md" title="wikilink">索契</a>）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/伊斯蘭團結運動會.md" title="wikilink">伊斯蘭團結運動會</a></p></td>
<td><p><a href="../Page/伊斯兰合作组织.md" title="wikilink">伊斯兰合作组织</a></p></td>
<td><p>2005年</p></td>
<td><p>每4年舉行一次</p></td>
<td><p>2013年伊斯蘭團結運動會（<a href="../Page/巴庫.md" title="wikilink">巴庫</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/世界軍人運動會.md" title="wikilink">世界軍人運動會</a></p></td>
<td><p><a href="../Page/国际军事体育理事会.md" title="wikilink">国际军事体育理事会</a></p></td>
<td><p>1995年－</p></td>
<td><p>每4年舉行一次</p></td>
<td><p>2015年世界軍人運動會（<a href="../Page/闻庆.md" title="wikilink">闻庆</a>）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/世界冬季軍人運動會.md" title="wikilink">世界冬季軍人運動會</a></p></td>
<td><p><a href="../Page/国际军事体育理事会.md" title="wikilink">国际军事体育理事会</a></p></td>
<td><p>2010年－</p></td>
<td><p>每4年舉行一次</p></td>
<td><p>2017年世界軍人運動會（<a href="../Page/索契.md" title="wikilink">索契</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/世界島國運動會.md" title="wikilink">世界島國運動會</a></p></td>
<td><p>International Island Games Association / IIGA</p></td>
<td><p>1985年－</p></td>
<td><p>每2年舉行一次</p></td>
<td><p>2013年世界島國運動會（<a href="../Page/漢密爾頓.md" title="wikilink">漢密爾頓</a>）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夏季國際少年運動會.md" title="wikilink">夏季國際少年運動會</a></p></td>
<td><p><a href="../Page/國際少年運動會.md" title="wikilink">國際少年運動會</a></p></td>
<td><p>1968年－</p></td>
<td><p>每1年舉行一次</p></td>
<td><p><a href="../Page/2017年夏季國際少年運動會.md" title="wikilink">2017年夏季國際少年運動會</a>（<a href="../Page/考那斯.md" title="wikilink">考那斯</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/冬季國際少年運動會.md" title="wikilink">冬季國際少年運動會</a></p></td>
<td><p><a href="../Page/國際少年運動會.md" title="wikilink">國際少年運動會</a></p></td>
<td><p>1994年－</p></td>
<td><p>不定期</p></td>
<td><p><a href="../Page/2016年冬季國際少年運動會.md" title="wikilink">2016年冬季國際少年運動會</a>（<a href="../Page/因斯布魯克.md" title="wikilink">因斯布魯克</a>）</p></td>
</tr>
</tbody>
</table>

### 只限個別洲份的國家參加的國際性運動會

| 賽事名稱                                               | 舉辦機構                                         | 舉辦年份        | 舉辦週期    | 最近一次舉辦                                                                                                          |
| -------------------------------------------------- | -------------------------------------------- | ----------- | ------- | --------------------------------------------------------------------------------------------------------------- |
| [夏季亞洲運動會](../Page/亚洲运动会.md "wikilink")             | [亞洲奧林匹克理事會](../Page/亞洲奧林匹克理事會.md "wikilink") | 1949年－      | 每4年舉行一次 | [2018年亚洲运动会](../Page/2018年亚洲运动会.md "wikilink")（[雅加达](../Page/雅加达.md "wikilink")、[巨港](../Page/巨港.md "wikilink")） |
| [冬季亞洲運動會](../Page/亞洲運動會.md "wikilink")             | [亞洲奧林匹克理事會](../Page/亞洲奧林匹克理事會.md "wikilink") | 1949年－      | 每4年舉行一次 | [2017年冬季亞洲運動會](../Page/2017年亞洲冬季運動會.md "wikilink")（[札幌](../Page/札幌.md "wikilink")）                              |
| [夏季泛美運動會](../Page/泛美運動會.md "wikilink")             | [泛美體育組織](../Page/泛美體育組織.md "wikilink")       | 1951年－      | 每4年舉行一次 | [2015年泛美運動會](../Page/2015年泛美運動會.md "wikilink")（[多倫多](../Page/多倫多.md "wikilink")）                                |
| [冬季泛美運動會](../Page/泛美運動會.md "wikilink")             | [泛美体育组织](../Page/泛美体育组织.md "wikilink")       | 1990年       | 已經取消    | [1990年冬季泛美運動會](../Page/1990年冬季泛美運動會.md "wikilink")（ [莱尼亚斯](../Page/阿根廷行政区划.md "wikilink") ）                     |
| [非洲運動會](../Page/非洲運動會.md "wikilink")               | [非洲奧委會協會](../Page/非洲奧委會協會.md "wikilink")     | 1965年－      | 每4年舉行一次 | 2015年非洲運動會（[布拉柴維爾](../Page/布拉柴維爾.md "wikilink")）                                                                |
| [中北美洲及加勒比海運動會](../Page/中北美洲及加勒比海運動會.md "wikilink") | 中北美洲及加勒比海體育組織                                | 1926年－      | 每4年舉行一次 | 2014年中北美洲及加勒比海運動會（[維拉克魯茲](../Page/維拉克魯茲.md "wikilink")）                                                         |
| [遠東運動會](../Page/遠東運動會.md "wikilink")               | [遠東奧林匹克委員會](../Page/遠東奧林匹克委員會.md "wikilink") | 1913年－1943年 | 已經取消    | [1934年遠東運動會](../Page/1934年遠東運動會.md "wikilink")（[馬尼拉](../Page/馬尼拉.md "wikilink")）                                |
| [亞洲室內運動會](../Page/亞洲室內運動會.md "wikilink")           | [亞洲奧林匹克理事會](../Page/亞洲奧林匹克理事會.md "wikilink") | 2005年－2009年 | 已經取消    | [2009年亞洲室內運動會](../Page/2009年亞洲室內運動會.md "wikilink")（[河內](../Page/河內.md "wikilink")）                              |
| [亞洲武術運動會](../Page/亞洲武術運動會.md "wikilink")           | [亞洲奧林匹克理事會](../Page/亞洲奧林匹克理事會.md "wikilink") | 2009年       | 已經取消    | [2009年亞洲武藝運動會](../Page/2009年亞洲武藝運動會.md "wikilink")（[曼谷](../Page/曼谷.md "wikilink")）                              |
| [亞洲室內暨武藝運動會](../Page/亞洲室內暨武藝運動會.md "wikilink")     | [亞洲奧林匹克理事會](../Page/亞洲奧林匹克理事會.md "wikilink") | 2013年-      | 每4年舉行一次 | [2013年亞洲室內暨武藝運動會](../Page/2013年亞洲室內暨武藝運動會.md "wikilink")（[阿什哈巴德](../Page/阿什哈巴德.md "wikilink")）                  |
| [亞洲沙灘運動會](../Page/亞洲沙灘運動會.md "wikilink")           | [亞洲奧林匹克理事會](../Page/亞洲奧林匹克理事會.md "wikilink") | 2008年－      | 每2年舉行一次 | [2016年亞洲沙灘運動會](../Page/2016年亞洲沙灘運動會.md "wikilink")（[峴港](../Page/峴港.md "wikilink")）                              |
| [歐洲運動會](../Page/歐洲運動會.md "wikilink")               | [歐洲奧林匹克理事會](../Page/歐洲奧林匹克理事會.md "wikilink") | 2015年-      | 每4年舉行一次 | [2015年歐洲運動會](../Page/2015年歐洲運動會.md "wikilink")（[巴庫](../Page/巴庫.md "wikilink")）                                  |
| [歐洲小國運動會](../Page/歐洲小國運動會.md "wikilink")           | [歐洲小國運動員聯會](../Page/歐洲小國運動員聯會.md "wikilink") | 1985年-      | 每2年舉行一次 | [2015年歐洲小國運動會](../Page/2015年歐洲小國運動會.md "wikilink")（[雷克雅維克](../Page/雷克雅維克.md "wikilink")）                        |

### 地區性運動會

| 賽事名稱                                       | 舉辦機構                                                 | 舉辦年份      | 舉辦週期    | 最近一次舉辦                                                                                                            |
| ------------------------------------------ | ---------------------------------------------------- | --------- | ------- | ----------------------------------------------------------------------------------------------------------------- |
| [東亞運動會](../Page/東亞運動會.md "wikilink")       | [東亞運動會總會](../Page/東亞運動會總會.md "wikilink")             | 1993-2013 | 已經取消    | [2013年東亞運動會](../Page/2013年東亞運動會.md "wikilink")（[天津](../Page/天津.md "wikilink")）                                    |
| [東亞青年運動會](../Page/東亞青年運動會.md "wikilink")   | [東亞運動會總會](../Page/東亞運動會總會.md "wikilink")             | 2019-     |         |                                                                                                                   |
| [東南亞半島運動會](../Page/東南亞半島運動會.md "wikilink") | [亞洲奧林匹克理事會](../Page/亞洲奧林匹克理事會.md "wikilink")         | 1959-1975 | 已經取消    | [1975年東南亞運動會](../Page/1975年東南亞運動會.md "wikilink")（[曼谷](../Page/曼谷.md "wikilink")）                                  |
| [東南亞運動會](../Page/東南亞運動會.md "wikilink")     | [亞洲奧林匹克理事會](../Page/亞洲奧林匹克理事會.md "wikilink")         | 1977-     | 每2年舉行一次 | [2015年東南亞運動會](../Page/2015年東南亞運動會.md "wikilink")（[新加坡市](../Page/新加坡市.md "wikilink")）                              |
| [南亞運動會](../Page/南亞運動會.md "wikilink")       | [亞洲奧林匹克理事會](../Page/亞洲奧林匹克理事會.md "wikilink")         | 1984-     | 每2年舉行一次 | [2016年南亞運動會](../Page/2016年南亞運動會.md "wikilink")（[古瓦哈提及](../Page/古瓦哈提.md "wikilink")[西隆](../Page/西隆.md "wikilink")） |
| [中亞運動會](../Page/中亞運動會.md "wikilink")       | [亞洲奧林匹克理事會](../Page/亞洲奧林匹克理事會.md "wikilink")         | 1995-     | 每4年舉行一次 | [2005年中亞運動會](../Page/2005年中亞運動會.md "wikilink")（）                                                                  |
| [西亞運動會](../Page/西亞運動會.md "wikilink")       | [亞洲奧林匹克理事會](../Page/亞洲奧林匹克理事會.md "wikilink")         | 1997-     | 不定      | [2014年西亞運動會](../Page/2014年西亞運動會.md "wikilink")（[基什島](../Page/基什島.md "wikilink")）                                  |
| [泛阿拉伯运动会](../Page/泛阿拉伯运动会.md "wikilink")   | [阿盟](../Page/阿盟.md "wikilink")                       | 1953-     | 不定      | [2011年泛阿拉伯運動會](../Page/2011年泛阿拉伯運動會.md "wikilink")（[杜哈](../Page/杜哈.md "wikilink")）                                |
| [地中海运动会](../Page/地中海运动会.md "wikilink")     | [地中海運動會國際委員會](../Page/地中海運動會國際委員會.md "wikilink")     | 1951-     | 每4年舉行一次 | [2013年地中海運動會](../Page/2013年地中海運動會.md "wikilink")（[梅爾辛](../Page/梅爾辛.md "wikilink")）                                |
| [歐洲小國運動會](../Page/歐洲小國運動會.md "wikilink")   | [歐洲小國運動員聯會](../Page/歐洲小國運動員聯會.md "wikilink")         | 1985-     | 每2年舉行一次 | [2015年歐洲小國運動會](../Page/2015年歐洲小國運動會.md "wikilink")（[雷克雅維克](../Page/雷克雅維克.md "wikilink")）                          |
| [印度洋島嶼運動會](../Page/印度洋島嶼運動會.md "wikilink") | [印度洋岛国运动会的领导机构](../Page/印度洋岛国运动会的领导机构.md "wikilink") | 1979-     | 每4年举行一次 | [2011年印度洋島嶼運動會](../Page/2011年印度洋島嶼運動會.md "wikilink")（[维多利亚](../Page/维多利亚.md "wikilink")）                          |

## 球类

### 足球

**全球性**

  - [-{zh-hans:国际足联世界杯;
    zh-hk:國際足協世界盃;}-](../Page/世界杯足球赛.md "wikilink")（FIFA
    World Cup）
  - [-{zh-hans:国际足联女子世界杯;
    zh-hk:國際足協女子世界盃;}-](../Page/女子世界杯足球赛.md "wikilink")（FIFA
    Women's World Cup）
  - [-{zh-hans:国际足联联合会杯;
    zh-hk:國際足協洲際國家盃;}-](../Page/国际足联联合会杯足球赛.md "wikilink")（FIFA
    Confederations Cup）
  - [-{zh-hans:国际足联沙滩足球世界杯;
    zh-hk:國際足協沙灘足球世界盃;}-](../Page/国际足联沙滩足球世界杯.md "wikilink")（FIFA
    Beach Soccer World Cup）
  - [-{zh-hans:国际足联世界青年足球锦标赛;
    zh-hk:國際足協世青盃;}-](../Page/世界青年足球锦标赛.md "wikilink")（FIFA
    U-20 World Cup）
  - [-{zh-hans:世界女子青年足球锦标赛;
    zh-hk:國際足協女子世青盃;}-](../Page/世界女子青年足球锦标赛.md "wikilink")（FIFA
    U-20 Women's World Championship）
  - [-{zh-hans:世界少年足球锦标赛;
    zh-hk:世界少年足球錦標賽;}-](../Page/世界少年足球錦標賽.md "wikilink")（FIFA
    U-17 World Cup）
  - [-{zh-hans:国际足联世界少年足球锦标赛;
    zh-hk:國際足協女子世界盃足球賽;}-](../Page/女子世界盃足球賽.md "wikilink")（FIFA
    U-17 Women's World Cup）
  - [豐田杯足球賽](../Page/豐田杯足球賽.md "wikilink")（己停辦）
  - [-{zh-hans:俱乐部世界冠军杯;
    zh-hk:世界冠軍球會盃;}-](../Page/世界冠軍球會盃.md "wikilink")（FIFA
    Club World Cup）
  - [露宿者世界盃](../Page/露宿者世界盃.md "wikilink")（Homeless World Cup）

<!-- end list -->

  - **各洲赛事**
      - **欧洲**
          - [-{zh-hans:欧洲足球锦标赛;
            zh-hk:歐洲國家盃;}-](../Page/欧洲足球锦标赛.md "wikilink")（UEFA
            European Championship ）
          - [-{zh-hans:欧洲足球联合会冠军联赛;
            zh-hk:歐洲聯賽冠軍盃;}-](../Page/欧洲冠军杯.md "wikilink")（UEFA
            Champions League）
          - [-{zh-hans:欧洲联盟杯;
            zh-hk:歐洲足協盃;}-](../Page/欧洲联盟杯.md "wikilink")（UEFA
            Cup）
          - [-{zh-hans:欧洲优胜者杯;
            zh-hk:歐洲盃賽冠軍盃;}-](../Page/欧洲优胜者杯.md "wikilink")（UEFA
            Cup Winners' Cup，己停辦）
          - [-{zh-hans:欧洲超级杯;zh-hk:歐洲超霸盃;zh-tw:歐洲超級盃;}-](../Page/歐洲超級盃.md "wikilink")（UEFA
            European Super Cup）
      - **美洲**
          - [美洲國家盃](../Page/美洲國家盃.md "wikilink")（Copa América）
          - [-{zh-hans:南美解放者杯;
            zh-hk:南美自由盃;}-](../Page/南美解放者杯.md "wikilink")（Copa
            Libertadores）
          - [美洲金盃](../Page/美洲金盃.md "wikilink")
      - **亚洲**
          - [-{zh-hans:亚洲俱乐部杯;
            zh-hk:亞洲聯賽冠軍盃;}-](../Page/亞洲聯賽冠軍盃.md "wikilink")（AFC
            Champions League）
          - [亚洲杯](../Page/亚洲杯足球赛.md "wikilink")（Asian Cup）
          - [-{zh-hans:東亞國家杯;
            zh-hk:東亞足球錦標賽;}-](../Page/東亞國家杯.md "wikilink")
      - **非洲**
          - [非洲杯](../Page/非洲杯足球赛.md "wikilink")（African Cup of Nations）
          - [-{zh-hans:非洲俱乐部杯;
            zh-hk:非洲聯賽冠軍盃;}-](../Page/非洲俱樂部杯.md "wikilink")（CAF
            Champions League）

**各国联赛**

  - 欧洲
      - [-{zh-hans:西班牙足球甲级联赛;
        zh-hk:西班牙甲組足球聯賽;}-](../Page/西班牙足球甲级联赛.md "wikilink")
      - [-{zh-hans:意大利足球甲级联赛;
        zh-hk:意大利甲組足球聯賽;}-](../Page/意大利足球甲级联赛.md "wikilink")
      - [-{zh-hans:英格兰足球甲级联赛;
        zh-hk:英格蘭超級足球聯賽;}-](../Page/英格兰足球超级联赛.md "wikilink")
      - [-{zh-hans:德国足球甲级联赛;
        zh-hk:德國甲組足球聯賽;}-](../Page/德国足球甲级联赛.md "wikilink")
      - [-{zh-hans:法国足球甲级联赛;
        zh-hk:法國甲組足球聯賽;}-](../Page/法国足球甲级联赛.md "wikilink")
      - [-{zh-hans:荷兰足球甲级联赛;
        zh-hk:荷蘭甲組足球聯賽;}-](../Page/荷兰足球甲级联赛.md "wikilink")
      - [-{zh-hans:葡萄牙足球超级联赛;
        zh-hk:葡萄牙超級足球聯賽;}-](../Page/葡萄牙超級足球聯賽.md "wikilink")
      - [-{zh-hans:希腊足球甲级联赛;
        zh-hk:希腊甲組足球聯賽;}-](../Page/希臘足球甲級聯賽.md "wikilink")
      - [-{zh-hans:苏格兰足球超级联赛;
        zh-hk:蘇格蘭超級足球聯賽;}-](../Page/苏格兰足球超级联赛.md "wikilink")
      - [-{zh-hans:丹麥足球超级联赛;
        zh-hk:丹麥超級足球聯賽;}-](../Page/丹麥足球超級聯賽.md "wikilink")
      - [-{zh-hans:比利時足球甲级联赛;
        zh-hk:比利時甲組足球聯賽;}-](../Page/比利時足球甲級聯賽.md "wikilink")
      - [-{zh-hans:芬兰足球超级联赛;
        zh-hk:芬蘭足球超級聯賽;}-](../Page/芬蘭足球超級聯賽.md "wikilink")

<!-- end list -->

  - 美洲
      - [-{zh-hans:巴西足球联赛;
        zh-hk:巴西足球甲級聯賽;}-](../Page/巴西足球甲級聯賽.md "wikilink")
      - [阿根廷足球春季联赛](../Page/阿根廷足球春季联赛.md "wikilink")
        [阿根廷足球秋季联赛](../Page/阿根廷足球秋季联赛.md "wikilink")
      - [美國職業足球大聯盟](../Page/美國職業足球大聯盟.md "wikilink")

<!-- end list -->

  - 亚洲
      - [中国足球超级联赛](../Page/中国足球超级联赛.md "wikilink")
      - [香港甲組足球聯賽](../Page/香港甲組足球聯賽.md "wikilink")
      - [-{zh-hans:日本J联赛;
        zh-hk:日本職業足球聯賽;}-](../Page/日本職業足球聯賽.md "wikilink")（J.LEAGUE)
      - [-{zh-hans:韩国K联赛;
        zh-hk:南韓職業足球聯賽;}-](../Page/韩国足球联赛.md "wikilink")（K.LEAGUE)
      - [沙特足球联赛](../Page/沙特足球联赛.md "wikilink")
      - [伊朗足球联赛](../Page/伊朗足球联赛.md "wikilink")
      - [-{zh-hans:新加坡足球联赛;
        zh-hk:新加坡職業足球聯賽;}-](../Page/新加坡職業足球联赛.md "wikilink")（S-League)

### 篮球

  - 国际赛事
      - [世界篮球锦标赛](../Page/世界篮球锦标赛.md "wikilink")
      - [欧洲篮球锦标赛](../Page/欧洲篮球锦标赛.md "wikilink")
      - [亞洲籃球錦標賽](../Page/亞洲籃球錦標賽.md "wikilink")

<!-- end list -->

  - 各地联赛
      - [NBA](../Page/NBA.md "wikilink")
      - [中国篮球职业联赛](../Page/中国篮球职业联赛.md "wikilink")
      - [香港籃球聯賽](../Page/香港籃球聯賽.md "wikilink")
      - [台灣超級籃球聯賽](../Page/台灣超級籃球聯賽.md "wikilink")
      - [台灣女子超級籃球聯賽](../Page/台灣女子超級籃球聯賽.md "wikilink")
      - [全国篮球职业联赛](../Page/全国篮球职业联赛.md "wikilink")
  - 欧洲联赛
      - [欧洲篮球冠军联赛](../Page/欧洲篮球冠军联赛.md "wikilink")
      - [意大利篮球甲级联赛](../Page/意大利篮球甲级联赛.md "wikilink")

### 排球

#### 全球赛事

  - [奥运会排球赛](../Page/奥运会排球赛.md "wikilink")
  - [世界杯排球赛](../Page/世界杯排球赛.md "wikilink")
  - [世界排球锦标赛](../Page/世界排球锦标赛.md "wikilink")
  - [世界排球大奖赛](../Page/世界排球大奖赛.md "wikilink")
  - [世界排球联赛](../Page/世界排球联赛.md "wikilink")
  - [世界女排大獎賽](../Page/世界女排大獎賽.md "wikilink")
  - [世界青年排球锦标赛](../Page/世界青年排球锦标赛.md "wikilink")

#### 各国联赛

  - [中國排球聯賽](../Page/中國排球聯賽.md "wikilink")

### 网球

  - [-{zh-hans:戴维斯杯; zh-hk:台維斯盃;}-](../Page/台維斯盃.md "wikilink")
  - [澳大利亚网球公开赛](../Page/澳大利亚网球公开赛.md "wikilink")
  - [法国网球公开赛](../Page/法国网球公开赛.md "wikilink")
  - [美国网球公开赛](../Page/美国网球公开赛.md "wikilink")
  - [中国网球公开赛](../Page/中国网球公开赛.md "wikilink")
  - [-{zh-hans:温布尔登网球公开赛;
    zh-hk:溫布頓網球公開賽;}-](../Page/温布尔登网球公开赛.md "wikilink")
  - [ATP世界巡迴賽1000大師賽](../Page/ATP世界巡迴賽1000大師賽.md "wikilink")
  - [职业网球锦标赛](../Page/职业网球锦标赛.md "wikilink")
  - [ATP网球巡回赛](../Page/ATP网球巡回赛.md "wikilink")

### 棒球

  - 全球賽事
      - [世界棒球錦標賽](../Page/世界棒球錦標賽.md "wikilink")
      - [世界棒球經典賽](../Page/世界棒球經典賽.md "wikilink")
  - 各洲賽事
      - [亞洲棒球錦標賽](../Page/亞洲棒球錦標賽.md "wikilink")
      - [亞洲職棒大賽](../Page/亞洲職棒大賽.md "wikilink")
      - [加勒比海大賽](../Page/加勒比海大賽.md "wikilink")
  - 各地聯賽
      - [美國職棒大聯盟](../Page/美國職棒大聯盟.md "wikilink")
      - [日本職棒](../Page/日本職棒.md "wikilink")
      - [韓國職棒](../Page/韓國職棒.md "wikilink")
      - [中華職業棒球聯盟](../Page/中華職業棒球聯盟.md "wikilink")
      - [墨西哥職棒聯盟](../Page/墨西哥職棒聯盟.md "wikilink")
      - [中國棒球聯賽](../Page/中國棒球聯賽.md "wikilink")
      - [古巴棒球聯賽](../Page/古巴棒球聯賽.md "wikilink")

### 乒乓球

#### 全球賽事

  - [世界乒乓球锦标赛](../Page/世界乒乓球锦标赛.md "wikilink")
  - [奧運會乒乓球賽](../Page/奧運會乒乓球賽.md "wikilink")
  - [世界盃乒乓球賽](../Page/世界盃乒乓球賽.md "wikilink")

#### 各国联赛

  - [中國乒乓球超級聯賽](../Page/中國乒乓球超級聯賽.md "wikilink")

### 羽毛球

  - [奥运会羽毛球赛](../Page/奥运会羽毛球赛.md "wikilink")
  - [汤姆斯杯](../Page/世界男子羽毛球团体锦标赛.md "wikilink")
  - [尤伯杯](../Page/世界女子羽毛球团体锦标赛.md "wikilink")
  - [苏迪曼杯](../Page/世界羽毛球混合团体锦标赛.md "wikilink")
  - [世界羽毛球锦标赛](../Page/世界羽毛球锦标赛.md "wikilink")

### 高尔夫球

  - [美国高尔夫大师赛](../Page/美国高尔夫大师赛.md "wikilink")（又译作“名人赛”、“精英赛”）
  - [美国高尔夫公开赛](../Page/美国高尔夫公开赛.md "wikilink")
  - [PGA高尔夫锦标赛](../Page/PGA高尔夫锦标赛.md "wikilink")
  - [英国高尔夫公开赛](../Page/英国高尔夫公开赛.md "wikilink")
  - [PGA巡回赛](../Page/PGA巡回赛.md "wikilink")
  - [PGA欧洲巡回赛](../Page/PGA欧洲巡回赛.md "wikilink")
  - [日本高尔夫巡回赛](../Page/日本高尔夫巡回赛.md "wikilink")
  - [世界高尔夫球锦标赛](../Page/世界高尔夫球锦标赛.md "wikilink")
  - [中国女子职业高尔夫球巡回赛](../Page/中国女子职业高尔夫球巡回赛.md "wikilink") (China LPGA
    Tour) <https://web.archive.org/web/20170520163920/http://clpga.org/>

### 橄欖球

  - [超級盃](../Page/超級盃.md "wikilink")

## 田径

  - [奥运会田径赛](../Page/奥运会田径赛.md "wikilink")
  - [世界杯田径赛](../Page/世界杯田径赛.md "wikilink")
  - [世界田径锦标赛](../Page/世界田径锦标赛.md "wikilink")
  - [黃金田径聯賽](../Page/黃金田径聯賽.md "wikilink")
  - [世界室内田径锦标赛](../Page/世界室内田径锦标赛.md "wikilink")
  - [世界青年田径锦标赛](../Page/世界青年田径锦标赛.md "wikilink")
  - [国际田径大奖赛](../Page/国际田径大奖赛.md "wikilink")
  - [亚洲田径锦标赛](../Page/亚洲田径锦标赛.md "wikilink")

## 游泳

  - [世界游泳錦標賽](../Page/世界游泳錦標賽.md "wikilink")
  - [世界盃游泳賽](../Page/世界盃游泳賽.md "wikilink")
  - [世界盃短池游泳賽](../Page/世界盃短池游泳賽.md "wikilink")

## 赛车

### [方程式赛车](../Page/方程式赛车.md "wikilink")

  - [一级方程式](../Page/一級方程式賽車.md "wikilink")

  - [电动方程式](../Page/电动方程式.md "wikilink")

  - [印第赛车](../Page/印第赛车.md "wikilink")

  -
  - [GP3系列赛](../Page/GP3系列赛.md "wikilink")

  - [国际汽车联合会欧洲三级方程式锦标赛](../Page/國際汽車聯盟歐洲三級方程式錦標賽.md "wikilink")

### 摩托车赛

  - [世界摩托车锦标赛](../Page/世界摩托车锦标赛.md "wikilink")
  - [世界超级摩托车锦标赛](../Page/世界超级摩托车锦标赛.md "wikilink")

### 跑车赛

  - [世界耐力锦标赛](../Page/世界耐力锦标赛.md "wikilink")

  - [勒芒24小时耐力赛](../Page/勒芒24小时耐力赛.md "wikilink")

  - [FIA GT锦标赛](../Page/FIA_GT锦标赛.md "wikilink")

  -
### 拉力赛

  - [世界拉力錦標賽](../Page/世界拉力錦標賽.md "wikilink")

  -
  - [达喀尔拉力赛](../Page/达喀尔拉力赛.md "wikilink")

### 房车赛

  - [世界房车锦标赛](../Page/世界房车锦标赛.md "wikilink")

  -
  - [德国房车大师赛](../Page/德国房车大师赛.md "wikilink")

### 其它

  - [全国运动汽车竞赛协会](../Page/全国运动汽车竞赛协会.md "wikilink")

## 自行车

  - [环法自行车赛](../Page/环法自行车赛.md "wikilink")
  - [环意自行车赛](../Page/环意自行车赛.md "wikilink")
  - [环西自行车赛](../Page/环西自行车赛.md "wikilink")

## 棋牌

### [围棋](../Page/围棋.md "wikilink")

  - [富士通杯](../Page/富士通杯.md "wikilink")
  - [应氏杯围棋赛](../Page/应氏杯围棋赛.md "wikilink")
  - [LG杯世界棋王戦](../Page/LG杯世界棋王戦.md "wikilink")
  - [三星杯世界围棋公开赛](../Page/三星杯世界围棋公开赛.md "wikilink")
  - [春兰杯世界围棋锦标赛](../Page/春兰杯世界围棋锦标赛.md "wikilink")
  - [农心杯世界围棋最强战](../Page/农心杯世界围棋最强战.md "wikilink")（三国围棋擂台赛）

## 其他

  - [NHL](../Page/NHL.md "wikilink")

[Category:體育相關列表](../Category/體育相關列表.md "wikilink")
[國際體育競賽](../Category/國際體育競賽.md "wikilink")
[體育獎項](../Category/體育獎項.md "wikilink")