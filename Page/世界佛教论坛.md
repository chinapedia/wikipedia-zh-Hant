**世界佛教论坛**是[中国佛教协会和](../Page/中国佛教协会.md "wikilink")[中华宗教文化交流协会定期举办的世界性的](../Page/中华宗教文化交流协会.md "wikilink")[佛教对话](../Page/佛教.md "wikilink")、交流、合作的论坛，每三年舉行一次。旨在“为热爱世界、关爱生命、护持佛教、慈悲为怀的有识之士，搭建一个平等、多元、开放的高层次对话、交流、合作的平台。”\[1\]

## 首届世界佛教论坛

首届世界佛教论坛2006年4月13日至16日在中国浙江省[杭州和](../Page/杭州.md "wikilink")[舟山市举行](../Page/舟山市.md "wikilink")，主题为“和谐世界
从心开始”。来自世界37个国家和地区的千余名佛教高僧出席了此次佛教盛事。\[2\]

议题主要包括：

  - 佛教的团结合作。
    佛教中和谐的理念，进一步发掘佛教教义中净化心灵、自净其意的丰富资源；增进佛教大乘与上座二乘、南北传、三大语系的团结与合作；推进新世纪僧伽教育的和谐发展，加强佛教教育与社会教育之间的互动；，推进佛教弘法利生事业；探讨佛教第七次结集的可行性；加强世界各地佛教组织的联系与合作；展望世界佛教青年活动的未来\[3\]。

<!-- end list -->

  - 佛教的社会责任。佛教与环保，增进人与自然和谐；运用佛教伦理提升社会道德，促进人人和谐；进一步推进佛教社会慈善事业；探讨身心净化与人类的永续发展问题\[4\]。

<!-- end list -->

  - 佛教的和平使命。着重探讨：人类的共存共荣问题，研究帮助如何消解地区间纷争与冲突，共同建设人间净土；佛教对世界文化贡献；增进不同文明对话、减少文明隔膜冲突\[5\]。

在本次会议中，[西藏](../Page/西藏.md "wikilink")[第十一世班禅活佛公开露面](../Page/第十一世班禪額爾德尼·確吉傑布.md "wikilink")\[6\]。

## 第二届世界佛教论坛

[Fangongwuxi.jpg](https://zh.wikipedia.org/wiki/File:Fangongwuxi.jpg "fig:Fangongwuxi.jpg")
第二届世界佛教论坛2009年3月28日至4月1日，在[无锡](../Page/无锡.md "wikilink")[灵山](../Page/灵山.md "wikilink")[梵宫和](../Page/梵宫.md "wikilink")[台北举行](../Page/台北市.md "wikilink")。主题为“和谐世界
众缘和合”。在台北[小巨蛋闭幕](../Page/小巨蛋.md "wikilink")。\[7\]

### 参会代表

来自[中国大陆](../Page/中国大陆.md "wikilink")、[台湾](../Page/台湾.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳门和](../Page/澳门.md "wikilink")[韩国](../Page/韩国.md "wikilink")、[日本](../Page/日本.md "wikilink")、[印度](../Page/印度.md "wikilink")、[泰国](../Page/泰国.md "wikilink")、[斯里兰卡](../Page/斯里兰卡.md "wikilink")、[缅甸](../Page/缅甸.md "wikilink")、[柬埔寨](../Page/柬埔寨.md "wikilink")、[老挝](../Page/老挝.md "wikilink")、[越南](../Page/越南.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[马来西亚](../Page/马来西亚.md "wikilink")、[印度尼西亚](../Page/印度尼西亚.md "wikilink")、[孟加拉国](../Page/孟加拉国.md "wikilink")、[美国](../Page/美国.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[澳大利亚](../Page/澳大利亚.md "wikilink")、[英国](../Page/英国.md "wikilink")、[意大利等近](../Page/意大利.md "wikilink")50个国家和地区的佛教界高僧大德、中外政要、学术界与文化界专家学者、企业界人士和新闻媒体记者近两千人共聚一堂，围绕“和谐世界，众缘和合”的主题进行了广泛而深入的交流。\[8\]\[9\]

出席本次世界佛教论坛的法师有[釋星雲](../Page/釋星雲.md "wikilink")、[一诚长老](../Page/一诚长老.md "wikilink")、[彻性法师](../Page/彻性法师.md "wikilink")、[本焕长老](../Page/本焕长老.md "wikilink")、[觉光长老](../Page/觉光.md "wikilink")、[嘉木样·洛桑久美·图丹却吉尼玛仁波切](../Page/嘉木样·洛桑久美·图丹却吉尼玛.md "wikilink")、[惟觉长老](../Page/惟觉长老.md "wikilink")、[祜巴龙庄勐长老](../Page/祜巴龙庄勐.md "wikilink")、[永寿法师](../Page/永寿法师.md "wikilink")、[明海法师](../Page/明海法师.md "wikilink")、[如意法师](../Page/如意法师.md "wikilink")、[济群法师](../Page/济群法师.md "wikilink")、[班禅活佛等佛学高僧主持](../Page/第十一世班禅额尔德尼·确吉杰布.md "wikilink")、参加会议讨论或演讲。另外，学界、商界、影视界人士[李连杰](../Page/李连杰.md "wikilink")、[李宁](../Page/李宁.md "wikilink")、[俞敏洪](../Page/俞敏洪.md "wikilink")、[潘石屹](../Page/潘石屹.md "wikilink")、[刘长乐等也参会世界佛教论坛](../Page/刘长乐.md "wikilink")。\[10\]

### 议程

第二届世界佛教论坛分论坛将探讨18个议题。\[11\]

  - 无锡[灵山分论坛议题](../Page/灵山大佛.md "wikilink")：（3月28日-3月30日）

<!-- end list -->

1.  佛教修学体系\[12\]
2.  佛教教育的机遇和挑战\[13\]
3.  法宝之光——大藏经的整理、保护和研究\[14\]\[15\]\[16\]。
4.  佛教音乐文化的抢救保护与创新发展\[17\]。
5.  高等教育与佛教教育的互相发展\[18\]。
6.  佛教与科学\[19\]
7.  众善奉行——佛教思想与企业的和谐发展。\[20\]
8.  佛教的传播与[民族性](../Page/民族性.md "wikilink")\[21\]\[22\]

<!-- end list -->

  - [台北分论坛议题](../Page/台北.md "wikilink")：（3月30日-4月1日）

<!-- end list -->

1.  佛教的组织管理。
2.  佛教的弘法传播\[23\]\[24\]
3.  佛教的艺文事业。
4.  佛教的慈善关怀\[25\]\[26\]
5.  佛教的心灵环保。
6.  佛教与现代性\[27\]。
7.  佛教的国际交流\[28\]。
8.  佛教的[宗派融合](../Page/宗派融合.md "wikilink")。

电视会谈：

1.  “长老论坛”—两岸四地佛教界交流里程记忆。
2.  “精英论坛”—[儒释道文化交流展现东方智慧](../Page/儒释道.md "wikilink")。

### 评论

  - 第二届世界佛教论坛的举办成功，得到了世界范围的一致肯定及嘉许。\[29\]

<!-- end list -->

  - [密歇根大学教授](../Page/密歇根大学.md "wikilink")[史蒂芬·考威尔认为](../Page/史蒂芬·考威尔.md "wikilink")，第二次世界佛教论坛是非常重要的一种交流方式。佛教是中国历史和文化的重要组成部分，海峡两岸共同弘扬中国传统优秀文化，对[两岸人民都深具意义](../Page/两岸.md "wikilink")。这是两岸佛教界深化交流与合作的一个非常好的平台\[30\]。
  - [北京大学哲学系教授](../Page/北京大学.md "wikilink")[楼宇烈说](../Page/楼宇烈.md "wikilink")，佛教是两岸同胞共同信仰的宗教之一，两岸的佛教交流有助于推动两岸同胞进一步的文化交流\[31\]。
  - [台湾政治大学原教授](../Page/台湾政治大学.md "wikilink")[郑石岩表示](../Page/郑石岩.md "wikilink")，这一交流对于促进两岸同胞之间的感情起到了积极作用\[32\]。
  - 台中慈光禅学院院长惠空法师表示，通过论坛这一平台，全球近５０个国家和地区的佛教领袖看到了当今佛教发展的情况，不同佛教宗派、不同语言、不同民族、不同文化通过论坛实现交流交融。他们将会把佛教推动世界和谐的愿景带回信众中，这对世界和谐、文明发展的推动，将是一个有力的力量。[法鼓山佛教学院院长](../Page/法鼓山.md "wikilink")[惠敏法师说](../Page/惠敏法师.md "wikilink")，论坛为两岸佛教交流提供了一个难得的平台。台湾佛教有特色，长期以来在佛教教育和佛教资讯化方面取得了一些成果，两岸佛教界可以在交流中互相学习，通过交流彼此尊重差异、取长补短\[33\]。
  - [香港佛教联合会会长](../Page/香港佛教联合会.md "wikilink")[觉光长老相信这个世界性的盛会](../Page/觉光.md "wikilink")，一定会促进两岸的和谐发展。\[34\]
  - [BBC认为首届世界佛教论坛的举办意味着中国政府对宗教的控制有所松动](../Page/BBC.md "wikilink")，也“可能是希望藉此舒缓美国等外界对干预宗教自由的指责”\[35\]。也有评论显示，中国有意以佛教力量抗衡以西方国家为主要代表的基督教（新教、天主教等）。
  - [路透社报道认为](../Page/路透社.md "wikilink")，这次海峡两岸合办佛教论坛有助于加强两岸关系，认为佛教可以帮助中国维持稳定、扩大道义影响力。\[36\]
  - [淡江大学国际事务与战略研究所教授](../Page/淡江大学.md "wikilink")[林中斌认为](../Page/林中斌.md "wikilink")，未来宗教软实力会在中国国家战略中发挥更大力量。\[37\]
  - 这次国际论坛的广泛性和代表性受到质疑。有舆论认为，这次论坛是以中国为主的一次统战活动\[38\]。

## 第三屆世界佛教論壇

于2012年4月25日至27日在[香港举行](../Page/香港.md "wikilink")，主題為「和諧世界
同願同行」，並邀請了六十多個國家地區之佛教領袖、學者專家蒞臨香港參與盛會。第三屆世界佛教論壇開幕禮在4月26日在[香港體育館舉行](../Page/香港體育館.md "wikilink")，[香港行政長官](../Page/香港行政長官.md "wikilink")[曾蔭權](../Page/曾蔭權.md "wikilink")，[全國政協副主席](../Page/全國政協.md "wikilink")[董建華以及香港](../Page/董建華.md "wikilink")[民政事務局局長](../Page/民政事務局.md "wikilink")[曾德成受邀成為開幕嘉賓](../Page/曾德成.md "wikilink")。在第三屆世界佛教論壇舉辦的同期，香港佛教界將迎請現存唯一的釋迦牟尼佛頂骨舍利蒞港供奉，並由4月25-30日一連六天於香港體育館供香港民众瞻礼祈福。

## 第四届世界佛教论坛

第四届世界佛教论坛于2015年10月24日在江苏省无锡市举行\[39\]。

## 第五届世界佛教论坛

第五届世界佛教论坛于2018年10月在福建省莆田市举行。

## 参考资料

## 外部链接

  - [世界佛教论坛官方网站](https://web.archive.org/web/20060408204808/http://www.wbf.net.cn/)
  - [世界佛教论坛](https://web.archive.org/web/20060415160456/http://www.sara.gov.cn/GB/xwzx/ztbd/sjfjlt/)
    中国[国家宗教事务局](../Page/国家宗教事务局.md "wikilink")
  - [凤凰网第二届世界佛教论坛专题](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/)
  - [龙泉之声专题](https://web.archive.org/web/20090402123727/http://www.longquanzs.org/wbf/)
    论文汇总
  - [第一届世界佛教论坛宣传片](http://you.video.sina.com.cn/b/14251478-1218555241.html)
  - [第二届世界佛教论坛宣传片](http://you.video.sina.com.cn/b/19581244-1599029332.html)

## 参见

  - [佛教](../Page/佛教.md "wikilink")
  - [中国宗教](../Page/中国宗教.md "wikilink")
  - [灵山](../Page/灵山.md "wikilink")

{{-}}

[世界佛教论坛](../Category/世界佛教论坛.md "wikilink")
[Category:中华人民共和国佛教](../Category/中华人民共和国佛教.md "wikilink")

1.  [关注：第二届世界佛教论坛](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/news/200903/0321_341_54057.shtml)

2.  [回顾：第一届世界佛教论坛](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/news/200903/0321_341_54054_1.shtml)

3.  [回顾：第一届世界佛教论坛](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/news/200903/0321_341_54054_1.shtml)

4.  [回顾：第一届世界佛教论坛](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/news/200903/0321_341_54054_1.shtml)

5.  [首届世界佛教论坛官方网站](http://www.wbf.net.cn/)

6.  [首届世界佛教论坛在杭州开幕](http://www.zj.xinhuanet.com/newscenter/2006-04/14/content_6738635.htm)
    ，[新华网](../Page/新华网.md "wikilink")

7.  [第二届世界佛教论坛在台北闭幕
    五万人祈愿和谐](http://www.huaxia.com/xw/gdxw/2009/04/1375865.html)

8.  [海内外记者聚焦世界佛教论坛 50多家媒体齐聚无锡](http://news.xinhuanet.com/zgjx/2009-03/26/content_11080269.htm)

9.  [和合众缘构建和谐
    两岸携手共襄盛举](http://news.fjnet.com/wywz/tj/200904/t20090427_118541.htm)

10. [李连杰李宁俞敏洪潘石屹参会世界佛教论坛](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/news/200903/0330_341_54723.shtml)

11. [第二届世界佛教论坛](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2)

12. [真禅法师僧教育思想与实践探微](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/lingshanhuichang/xiuxuetixi/200903/0328_356_54496.shtml)

13. [大学课程中佛教教育的方向和实际](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/lingshanhuichang/jiyutiaozhan/200903/0329_354_54576.shtml)

14. [浅谈当代汉文大藏经整理传译之方向](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/lingshanhuichang/fabaozhiguang/200903/0328_353_54377.shtml)

15. [谈《佛光大藏经》](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/lingshanhuichang/fabaozhiguang/200903/0328_353_54367.shtml)

16. [了解《嘉兴藏》](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/lingshanhuichang/fabaozhiguang/200903/0328_353_54365.shtml)

17. [心灵净化之声的佛教音乐古今谈](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/lingshanhuichang/yinyuewenhua/200903/0328_359_54458.shtml)

18. [大学课程中佛教教育的方向和实际](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/lingshanhuichang/jiyutiaozhan/200903/0329_354_54576.shtml)

19. [理性与实验的结合
    科学的佛法](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/lingshanhuichang/fojiaoyukexue/200903/0328_360_54415.shtml)

20. [中国民族大众的观世音信仰](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/lingshanhuichang/zhongshanfengxing/200903/0328_355_54418.shtml)

21. [包容 和谐
    快乐——弥勒信仰与和谐社会浅述](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/lingshanhuichang/chuanbominzuxing/200903/0329_357_54522.shtml)

22. [佛教的传播与民族性](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/lingshanhuichang/chuanbominzuxing/200903/0329_357_54499.shtml)

23. [星云大师：文化国家最重要的软实力](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/taibeifenhuichang/chuanchengchuangxin/200903/0331_375_54817.shtml)

24. [星云大师：建立佛光缘美术馆
    传扬佛教艺术](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/taibeifenhuichang/hongfachuanbo/200903/0331_367_54816.shtml)

25. [敬天爱地聚福缘 灾难频仍 深觉来不及 企盼透过大爱情怀援助
    慈济大陆慈善工作纪要](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/taibeifenhuichang/cishanguanhuai/200903/0331_377_54850.shtml)

26. [1](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/taibeifenhuichang/huanjingbaohu/200903/0331_376_54837.shtml)

27. [近代中国佛教的兴起与汉传佛教的发言](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/taibeifenhuichang/xiandaixing/200903/0331_378_54822.shtml)

28. [汉传佛教面临的挑战与机运](http://fo.ifeng.com/zhuanti/shijiefojiaoluntan2/taibeifenhuichang/guojijiaoliu/200903/0331_379_54823.shtml)

29. [和合众缘构建和谐
    两岸携手共襄盛举](http://news.fjnet.com/wywz/tj/200904/t20090427_118541_6.htm)

30. [与会代表热评第二届世界佛教论坛](http://news.163.com/09/0401/21/55RI2J5H000120GU.html)

31.
32.
33.
34. [觉光长老：佛教放了大光明](http://news.fjnet.com/rw/tj/200905/t20090510_120163.htm)


35. [世界佛教论坛开幕
    班禅罕见露面](http://news.bbc.co.uk/chinese/simp/hi/newsid_4900000/newsid_4905200/4905200.stm)
    BBC中文网

36. [参考消息](http://ckxx.org.cn/other/other2009032905/)  2009年3月29日

37. [台学者：本届论坛跨两岸举行大陆借佛教争台湾民心](http://www.zaobao.com/special/china/taiwan/pages12/taiwan090401.shtml)
    4月1日，《[联合早报](../Page/联合早报.md "wikilink")》

38.

39.