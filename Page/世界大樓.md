**世界大樓**（New York World
Building），是[紐約市早期的一幢](../Page/紐約市.md "wikilink")[摩天大樓](../Page/摩天大樓.md "wikilink")，完工於1890年，在1955年被拆除。它超过[家庭保險大樓成为最高摩天大樓](../Page/家庭保險大樓.md "wikilink")。世界大樓擁有地上20層，屋頂高94.2米，含尖頂高106.4米。它的屋頂高度记录於1894年被[曼哈頓壽險大樓超越](../Page/曼哈頓壽險大樓.md "wikilink")（雖然沒有超過它的尖頂高度）。1899年其尖頂高度被[公園街大樓超越](../Page/公園街大樓.md "wikilink")。

## 參考資料

[Category:曼哈頓摩天大樓](../Category/曼哈頓摩天大樓.md "wikilink")
[Category:前世界最高建築](../Category/前世界最高建築.md "wikilink")
[Category:50米至99米高的摩天大樓](../Category/50米至99米高的摩天大樓.md "wikilink")
[Category:已不存在的摩天大樓](../Category/已不存在的摩天大樓.md "wikilink")
[Category:1955年廢除](../Category/1955年廢除.md "wikilink")