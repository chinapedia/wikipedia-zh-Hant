[Heartland.png](https://zh.wikipedia.org/wiki/File:Heartland.png "fig:Heartland.png")
[Map_Geopolitic_Mackinder.gif](https://zh.wikipedia.org/wiki/File:Map_Geopolitic_Mackinder.gif "fig:Map_Geopolitic_Mackinder.gif")
**世界岛**（Heartland
Theory，**陆心说**）的概念来自于[麦金德于](../Page/麥金德.md "wikilink")1902年在[英国皇家地理学会发表的文章](../Page/英国.md "wikilink")***历史进程中的地理要素***。在这篇文章中，他把[地缘政治分析推广到全球角度](../Page/地缘政治.md "wikilink")。麦金德认为，[地球由两部分构成](../Page/地球.md "wikilink")。由[欧洲](../Page/欧洲.md "wikilink")、[亚洲](../Page/亚洲.md "wikilink")、[非洲组成的世界岛](../Page/非洲.md "wikilink")，是世界最大、人口最多、最富饶的陆地组合。在它的边缘，有一系列相对孤立的大陆，如[美洲大陆](../Page/美洲.md "wikilink")、[澳洲大陆](../Page/澳洲大陆.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[日本及](../Page/日本.md "wikilink")[不列颠群岛](../Page/不列颠群岛.md "wikilink")。在世界岛的中央，是自[伏尔加河到](../Page/伏尔加河.md "wikilink")[长江](../Page/长江.md "wikilink")，自[喜马拉雅山脉到](../Page/喜马拉雅山脉.md "wikilink")[北极的心脏地带](../Page/北极.md "wikilink")。在北极冰冻地带和南方连绵的山脉和沙漠的保护下，这片中心地带只有可能面对来自[西欧的陆地入侵威胁](../Page/西欧.md "wikilink")。麦金德认为，由于古代落后的交通运输条件，这片心脏地带在过去不可能被一个单一[强权所控制](../Page/强权.md "wikilink")。由于人力资源与供给的困难，自古以来，自东向西或自西向东的连续军事扩张不可能实现。

麦金德认为，[铁路的出现已经大大降低了一个单一强权主宰中心地带的难度](../Page/铁路.md "wikilink")。当[欧亚大陆被密集的铁路网覆盖时](../Page/欧亚大陆.md "wikilink")，一个强大的大陆国家将主宰这片自[东欧门户开始的的广袤土地](../Page/东欧.md "wikilink")。而这将是这个国家主宰欧亚大陆，进而主宰世界的前奏：

麦金德的上述结论假设了对于两大海洋强权-[英国和](../Page/英国.md "wikilink")[美国的地缘政治噩梦](../Page/美国.md "wikilink")。一旦[德国或](../Page/德国.md "wikilink")[俄国控制了东欧](../Page/俄国.md "wikilink")，这将是这两大强国主宰世界的前奏。

一般认为，麦金德理论是导致[第一次世界大战和](../Page/第一次世界大战.md "wikilink")[第二次世界大战的原因之一](../Page/第二次世界大战.md "wikilink")。一些人认为，麦金德的理论为[纳粹德国向东扩张创造了理论基础](../Page/纳粹德国.md "wikilink")。然而，尽管二战中的纳粹德国控制了包括[乌克兰在内的东欧大部地区](../Page/乌克兰.md "wikilink")，它却被[苏联打败](../Page/苏联.md "wikilink")。解释之一认为，苏联可以把工业迁移到心脏地带之外的[西伯利亚地区](../Page/西伯利亚.md "wikilink")。然而，关于西伯利亚地区是否属于心脏地带却有争议。麦金德的理论也受到了另外两方面的挑战。一方面，[空军的有效性使地域宽广的世界岛纵深地区面临威胁](../Page/空军.md "wikilink")。另一方面，随着[核武器的出现](../Page/核武器.md "wikilink")，世界岛的纵深对于工业的保护变得毫无意义。

然而，[冷战的开始使麦金德的理论重受青睐](../Page/冷战.md "wikilink")。这主要由于苏联的崛起所导致的。

苏联从[工业方面](../Page/工业.md "wikilink")，[科技方面和](../Page/科技.md "wikilink")[军事方面](../Page/军事.md "wikilink")，完成了对心脏地带的控制。然而冷战的结束和[苏联解体使麦金德的理论再次受到质疑](../Page/苏联解体.md "wikilink")。海洋对于国际商业与国际贸易的重要作用部分抵消了陆权论的影响。

无论如何，麦金德的世界岛理论和与其相伴随的陆权论思想仍然在二十一世纪具有重要的地缘政治意义。一旦欧亚大陆上的强权国家再次崛起，麦金德的理论将再受青睐。

## 參考

  - William R. Keylor，*The Twentieth-Century World and Beyond: An
    International History Since 1900*, 2006. ISBN 0-19-516843-7

## 相關條目

  - [歐亞非大陸](../Page/歐亞非大陸.md "wikilink")
  - [旧大陆](../Page/旧大陆.md "wikilink")

[Category:地缘政治](../Category/地缘政治.md "wikilink")