**世界新七大奇蹟**（）是[瑞士的](../Page/瑞士.md "wikilink")[新七大奇迹协会](../Page/新七大奇迹协会.md "wikilink")（New
7 Wonders
Foundation，N7W）发起的在世界现存建于公元2000年以前的[建筑中挑选七个代替旧的](../Page/建筑.md "wikilink")[世界七大奇蹟的活动](../Page/世界七大奇蹟.md "wikilink")。发起原因是由于[古代世界七大奇蹟中的六项早已不复存在](../Page/古代世界七大奇蹟.md "wikilink")，而且都集中在[地中海地区](../Page/地中海.md "wikilink")\[1\]。其选举方式是由网上和電話免費和付費投票决定，已登記會員可免費投票一次，如要再投票則需付款。投票於2007年7月6日截至，投票結果2007年7月7日則在当地时间9:30在[葡萄牙](../Page/葡萄牙.md "wikilink")[里斯本揭晓](../Page/里斯本.md "wikilink")。

## 历史

根据NOWC的大事件表\[2\]，瑞士商人[伯纳德·韦伯在](../Page/伯纳德·韦伯.md "wikilink")1999年9月启动了这个计划，到了2005年11月24日，77个项目被列入了考虑范围。列入标准是：建筑必须是人造的，2000年之前完成的，保护程度可以让人接受的。2006年1月1日，NOWC宣布列表已缩减到21个，\[3\]
之后，又回应[埃及的指责](../Page/埃及.md "wikilink")，削减到20个，並將[吉薩金字塔歸為](../Page/吉薩金字塔.md "wikilink")「榮譽奇蹟」。

## 新七大奇蹟

<table style="width:75%;">
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><p>名稱</p></th>
<th><p>位於</p></th>
<th><p>圖像</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/羅馬競技場.md" title="wikilink">羅馬競技場</a></strong></p></td>
<td><p><a href="../Page/羅馬.md" title="wikilink">羅馬</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Lightmatter_colosseum.jpg" title="fig:Lightmatter_colosseum.jpg">Lightmatter_colosseum.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/長城.md" title="wikilink">万里長城</a></strong></p></td>
<td><p><a href="../Page/中華人民共和國.md" title="wikilink">中華人民共和國</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:GreatWallNearBeijingWinter.jpg" title="fig:GreatWallNearBeijingWinter.jpg">GreatWallNearBeijingWinter.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/佩特拉.md" title="wikilink">佩特拉</a></strong></p></td>
<td><p><a href="../Page/約旦.md" title="wikilink">約旦</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Petra_Jordan_BW_36.JPG" title="fig:Petra_Jordan_BW_36.JPG">Petra_Jordan_BW_36.JPG</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/里約熱內盧基督像.md" title="wikilink">里約熱內盧基督像</a></strong></p></td>
<td><p><a href="../Page/巴西.md" title="wikilink">巴西</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Corcovado_statue01_2005-03-14.jpg" title="fig:Corcovado_statue01_2005-03-14.jpg">Corcovado_statue01_2005-03-14.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/馬丘比丘.md" title="wikilink">馬丘比丘</a></strong></p></td>
<td><p><a href="../Page/秘魯.md" title="wikilink">秘魯</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Machu_Picchu_early_morning.JPG" title="fig:Machu_Picchu_early_morning.JPG">Machu_Picchu_early_morning.JPG</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/奇琴伊察.md" title="wikilink">契琴伊薩</a></strong></p></td>
<td><p><a href="../Page/墨西哥.md" title="wikilink">墨西哥</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:El_Castillo,_Chichén_Itzá.jpg" title="fig:El_Castillo,_Chichén_Itzá.jpg">El_Castillo,_Chichén_Itzá.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/泰姬瑪哈陵.md" title="wikilink">泰姬瑪哈陵</a></strong></p></td>
<td><p><a href="../Page/印度.md" title="wikilink">印度</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:TajMahalbyAmalMongia.jpg" title="fig:TajMahalbyAmalMongia.jpg">TajMahalbyAmalMongia.jpg</a></p></td>
</tr>
</tbody>
</table>

## 所有评比项目

最初選出之七十七個景點為：

亞洲：

  - [柬埔寨](../Page/柬埔寨.md "wikilink")，[吳哥窟](../Page/吳哥窟.md "wikilink")
  - [中國](../Page/中國.md "wikilink")，[萬里長城](../Page/萬里長城.md "wikilink")
  - 中國[北京](../Page/北京.md "wikilink")，[紫禁城](../Page/紫禁城.md "wikilink")
  - 中國[拉薩](../Page/拉薩.md "wikilink")，[布達拉宮](../Page/布達拉宮.md "wikilink")
  - 中國[西安](../Page/西安.md "wikilink")，[兵馬俑](../Page/兵馬俑.md "wikilink")
  - [印度](../Page/印度.md "wikilink")[阿格拉](../Page/阿格拉.md "wikilink")，[泰姬陵](../Page/泰姬陵.md "wikilink")
  - 印度，[布里哈迪斯瓦拉神廟](../Page/布里哈迪斯瓦拉神廟.md "wikilink")
  - 印度，[Annamalaiyar
    Temple](../Page/:en:Annamalaiyar_Temple.md "wikilink")
  - 印度，[Bahubali Gommateshwara
    Statue](../Page/:en:Bahubali.md "wikilink")
  - [印度](../Page/印度.md "wikilink")，[千柱之廟](../Page/千柱之廟.md "wikilink")
  - [印度](../Page/印度.md "wikilink")[阿姆利則](../Page/阿姆利則.md "wikilink")，[金廟](../Page/金廟.md "wikilink")
  - 印度[馬杜賴](../Page/馬杜賴.md "wikilink")，[密納克西神廟](../Page/密納克西神廟.md "wikilink")
  - 印度[新德里](../Page/新德里.md "wikilink")，[蓮花寺](../Page/莲花寺_\(德里\).md "wikilink")

<!-- end list -->

  - 印度[泰米爾納杜邦](../Page/坦米爾納德.md "wikilink")，[馬哈巴利普拉姆](https://en.wikipedia.org/wiki/Group_of_Monuments_at_Mahabalipuram)
  - 日本京都，[清水寺](../Page/清水寺.md "wikilink")
  - [約旦](../Page/約旦.md "wikilink")，[佩特拉古城](../Page/佩特拉古城.md "wikilink")
  - [馬來西亞](../Page/馬來西亞.md "wikilink")[吉隆坡](../Page/吉隆坡.md "wikilink")，[雙子塔](../Page/雙子塔.md "wikilink")
  - [斯里蘭卡](../Page/斯里蘭卡.md "wikilink")，[獅子岩要塞](../Page/獅子岩要塞.md "wikilink")
  - [泰國](../Page/泰國.md "wikilink")[曼谷](../Page/曼谷.md "wikilink")，Dhammakaya
    Cetiya
  - [阿聯酋](../Page/阿聯酋.md "wikilink")，[帆船酒店](../Page/帆船酒店.md "wikilink")
  - [也門](../Page/也門.md "wikilink")，[薩那城](../Page/薩那城.md "wikilink")

歐洲：

  - [俄羅斯](../Page/俄羅斯.md "wikilink")[莫斯科](../Page/莫斯科.md "wikilink")，[克里姆林宮](../Page/克里姆林宮.md "wikilink")、[華西里·柏拉仁諾教堂及](../Page/華西里·柏拉仁諾教堂.md "wikilink")[紅場](../Page/紅場.md "wikilink")
  - [捷克](../Page/捷克.md "wikilink")[布拉格](../Page/布拉格.md "wikilink")，[布拉格古堡](../Page/布拉格古堡.md "wikilink")
  - 捷克布拉格，[查理四世大橋](../Page/查理四世大橋.md "wikilink")
  - [法國](../Page/法國.md "wikilink")，[凡爾賽宮](../Page/凡爾賽宮.md "wikilink")
  - 法國，[聖米歇爾山](../Page/聖米歇爾山.md "wikilink")
  - [法國](../Page/法國.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")，[艾菲爾鐵塔](../Page/艾菲爾鐵塔.md "wikilink")
  - [希臘](../Page/希臘.md "wikilink")[雅典](../Page/雅典.md "wikilink")，[衛城](../Page/衛城.md "wikilink")
  - 希臘雅典，[帕那辛納克體育場](../Page/帕那辛納克體育場.md "wikilink")
  - [德國](../Page/德國.md "wikilink")，[阿亨大教堂](../Page/阿亨大教堂.md "wikilink")
  - 德國，[Goeltzschtal
    Brigde](../Page/:en:Göltzschtalbrücke.md "wikilink")
  - 德國，[科隆大教堂](../Page/科隆大教堂.md "wikilink")
  - 德國[德雷斯頓](../Page/德雷斯頓.md "wikilink")，[聖母教堂](../Page/聖母教堂.md "wikilink")
  - 德國[菲森](../Page/菲森.md "wikilink")，[新天鵝堡](../Page/新天鵝堡.md "wikilink")
  - 德國[慕尼黑](../Page/慕尼黑.md "wikilink")，[奧運場及奧運公園](../Page/奧運場及奧運公園.md "wikilink")
  - [匈牙利](../Page/匈牙利.md "wikilink")[布達佩斯](../Page/布達佩斯.md "wikilink")，[國會大樓](../Page/國會大樓.md "wikilink")
  - [愛爾蘭](../Page/愛爾蘭.md "wikilink")[米斯郡](../Page/米斯郡.md "wikilink")，[紐格萊奇墓](../Page/紐格萊奇墓.md "wikilink")
  - [意大利](../Page/意大利.md "wikilink")[比薩](../Page/比薩.md "wikilink")，[比薩斜塔](../Page/比薩斜塔.md "wikilink")
  - 意大利[羅馬](../Page/羅馬.md "wikilink")，[羅馬競技場](../Page/羅馬競技場.md "wikilink")
  - 意大利羅馬，[聖彼得教堂](../Page/聖彼得教堂.md "wikilink")
  - 意大利羅馬，[西絲汀教堂](../Page/西絲汀教堂.md "wikilink")
  - 意大利[威尼斯](../Page/威尼斯.md "wikilink")，[总督宫](../Page/总督宫_\(威尼斯\).md "wikilink")
  - [西班牙](../Page/西班牙.md "wikilink")，[聖地亞哥—德孔波斯特拉大教堂](../Page/聖地亞哥—德孔波斯特拉大教堂.md "wikilink")
  - 西班牙[巴塞羅那](../Page/巴塞羅那.md "wikilink")，[聖家堂](../Page/聖家堂.md "wikilink")
  - 西班牙[畢爾巴鄂](../Page/畢爾巴鄂.md "wikilink")，[畢爾巴鄂古根漢美術館](../Page/畢爾包古根漢美術館.md "wikilink")
  - 西班牙[科尔多瓦](../Page/科尔多瓦.md "wikilink")，[清真寺天主堂](../Page/清真寺天主堂.md "wikilink")
  - 西班牙[格拉納達](../Page/格拉納達.md "wikilink")，[阿爾罕布拉宮](../Page/阿爾罕布拉宮.md "wikilink")
  - 西班牙[馬德里](../Page/馬德里.md "wikilink")，[皇宮](../Page/皇宮.md "wikilink")
  - 西班牙[塞維亞](../Page/塞維亞.md "wikilink")，[塞維亞大教堂](../Page/塞維亞大教堂.md "wikilink")
  - 西班牙[塞哥維亞](../Page/塞哥維亞.md "wikilink")，[羅馬水道橋](../Page/羅馬水道橋.md "wikilink")
  - [瑞士](../Page/瑞士.md "wikilink")[盧塞恩](../Page/盧塞恩.md "wikilink")，[教堂橋](../Page/卡贝尔桥.md "wikilink")
  - [土耳其](../Page/土耳其.md "wikilink")[伊斯坦堡](../Page/伊斯坦堡.md "wikilink")，[聖索非亞大教堂](../Page/聖索非亞大教堂.md "wikilink")
  - [英國](../Page/英國.md "wikilink")[阿姆斯伯利](../Page/阿姆斯伯利.md "wikilink")，[巨石陣](../Page/巨石陣.md "wikilink")
  - 英國[倫敦](../Page/倫敦.md "wikilink")，[倫敦塔](../Page/倫敦塔.md "wikilink")
  - 英國倫敦，[大笨鐘](../Page/大笨鐘.md "wikilink")
  - 英國倫敦，[格林威治天文台](../Page/格林威治天文台.md "wikilink")
  - 英國倫敦，[國會大樓](../Page/國會大樓.md "wikilink")
  - 英國倫敦，[聖保羅大教堂](../Page/聖保羅大教堂.md "wikilink")
  - 英國倫敦，[英國航空](../Page/英國航空.md "wikilink")[倫敦眼](../Page/倫敦眼.md "wikilink")

非洲：

  - [埃及](../Page/埃及.md "wikilink")，[吉薩金字塔](../Page/吉薩金字塔.md "wikilink")
  - 埃及，[帝王谷](../Page/帝王谷.md "wikilink")
  - 埃及[亞斯文](../Page/亞斯文.md "wikilink")，[阿布辛貝神廟](../Page/阿布辛拜勒神廟.md "wikilink")
  - [馬里](../Page/馬里.md "wikilink")，[廷巴克圖](../Page/廷巴克圖.md "wikilink")

美洲：

  - [巴西](../Page/巴西.md "wikilink")[里約熱內盧](../Page/里約熱內盧.md "wikilink")，[基督像](../Page/基督像.md "wikilink")
  - [加拿大](../Page/加拿大.md "wikilink")，[加拿大國家電視塔](../Page/加拿大國家電視塔.md "wikilink")
  - [智利](../Page/智利.md "wikilink")[復活節島](../Page/復活節島.md "wikilink")，復活節島[摩艾石像](../Page/摩艾石像.md "wikilink")
  - [墨西哥](../Page/墨西哥.md "wikilink")[特奥蒂瓦坎](../Page/特奥蒂瓦坎.md "wikilink")，[特奥蒂瓦坎金字塔](../Page/太阳金字塔.md "wikilink")
  - 墨西哥[尤卡坦半島](../Page/尤卡坦半島.md "wikilink")，[奇琴伊察金字塔](../Page/奇琴伊察金字塔.md "wikilink")
  - [巴拿馬](../Page/巴拿馬.md "wikilink")，[巴拿馬運河](../Page/巴拿馬運河.md "wikilink")
  - [秘魯](../Page/秘魯.md "wikilink")，[馬丘比丘](../Page/馬丘比丘.md "wikilink")
  - 秘魯，[納斯卡巨畫](../Page/納斯卡巨畫.md "wikilink")
  - [美國](../Page/美國.md "wikilink")[紐約](../Page/紐約.md "wikilink")，[自由神像](../Page/自由神像.md "wikilink")
  - 美國紐約，[帝國大廈](../Page/帝國大廈.md "wikilink")
  - 美國[三藩市](../Page/三藩市.md "wikilink")，[金門大橋](../Page/金門大橋.md "wikilink")
  - 美國[南達科他州](../Page/南達科他州.md "wikilink")，[總統山](../Page/總統山.md "wikilink")

大洋洲：

  - [澳大利亞](../Page/澳大利亞.md "wikilink")[悉尼](../Page/悉尼.md "wikilink")，[悉尼歌劇院](../Page/悉尼歌劇院.md "wikilink")

在七十七個景點，再選出最後二十一个参加评比的项目\[4\] 按拉丁字母顺序列在下：

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><p>奇迹</p></th>
<th><p>性质</p></th>
<th><p>地点</p></th>
<th><p>图像</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/雅典卫城.md" title="wikilink">雅典卫城</a></strong></p></td>
<td><p>文明、民主</p></td>
<td><p><a href="../Page/希腊.md" title="wikilink">希腊</a><a href="../Page/雅典.md" title="wikilink">雅典</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Acropolis3.JPG" title="fig:Acropolis3.JPG">Acropolis3.JPG</a> to the west]]</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/阿尔罕布拉宫.md" title="wikilink">阿尔罕布拉宫</a></strong></p></td>
<td><p>威嚴、國家間的溝通</p></td>
<td><p><a href="../Page/西班牙.md" title="wikilink">西班牙</a><a href="../Page/格拉纳达.md" title="wikilink">格拉纳达</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Alhambra_view.jpg" title="fig:Alhambra_view.jpg">Alhambra_view.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/吴哥窟.md" title="wikilink">吴哥窟</a></strong></p></td>
<td><p>美麗、尊嚴</p></td>
<td><p><a href="../Page/柬埔寨.md" title="wikilink">柬埔寨</a><a href="../Page/吴哥古迹.md" title="wikilink">吴哥古迹</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Angkor_wat_temple.jpg" title="fig:Angkor_wat_temple.jpg">Angkor_wat_temple.jpg</a>]]</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/奇琴伊察.md" title="wikilink">奇琴伊察</a></strong></p></td>
<td><p>崇拜、知识</p></td>
<td><p><a href="../Page/墨西哥.md" title="wikilink">墨西哥</a><a href="../Page/尤卡坦州.md" title="wikilink">尤卡坦州</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:El_Castillo,_Chichén_Itzá.jpg" title="fig:El_Castillo,_Chichén_Itzá.jpg">El_Castillo,_Chichén_Itzá.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/里约热内卢基督像.md" title="wikilink">里约热内卢基督像</a></strong></p></td>
<td><p>迎接、坦诚</p></td>
<td><p><a href="../Page/巴西.md" title="wikilink">巴西</a><a href="../Page/里約熱內盧.md" title="wikilink">里約熱內盧</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Cristo_Redentor_-_Rio_de_Janeiro,_Brasil.jpg" title="fig:Cristo_Redentor_-_Rio_de_Janeiro,_Brasil.jpg">Cristo_Redentor_-_Rio_de_Janeiro,_Brasil.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/罗马竞技场.md" title="wikilink">罗马竞技场</a></strong></p></td>
<td><p>喜悦、痛苦</p></td>
<td><p><a href="../Page/義大利.md" title="wikilink">義大利</a><a href="../Page/羅馬.md" title="wikilink">羅馬</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Colosseum_in_Rome,_Italy_-_April_2007.jpg" title="fig:Colosseum_in_Rome,_Italy_-_April_2007.jpg">Colosseum_in_Rome,_Italy_-_April_2007.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/摩艾石像.md" title="wikilink">摩艾石像</a></strong></p></td>
<td><p>神秘、驚畏</p></td>
<td><p><a href="../Page/智利.md" title="wikilink">智利</a><a href="../Page/復活節島.md" title="wikilink">復活節島</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Moai_Rano_raraku.jpg" title="fig:Moai_Rano_raraku.jpg">Moai_Rano_raraku.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/艾菲尔铁塔.md" title="wikilink">艾菲尔铁塔</a></strong></p></td>
<td><p>科技的挑戰、进步</p></td>
<td><p><a href="../Page/法國.md" title="wikilink">法國</a><a href="../Page/巴黎.md" title="wikilink">巴黎</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tour_eiffel_at_sunrise_from_the_trocadero.jpg" title="fig:Tour_eiffel_at_sunrise_from_the_trocadero.jpg">Tour_eiffel_at_sunrise_from_the_trocadero.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/万里长城.md" title="wikilink">万里长城</a></strong></p></td>
<td><p>堅忍不拔</p></td>
<td><p><a href="../Page/中華人民共和國.md" title="wikilink">中國</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:GreatWallNearBeijingWinter.jpg" title="fig:GreatWallNearBeijingWinter.jpg">GreatWallNearBeijingWinter.jpg</a>]]</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/圣索非亚大教堂.md" title="wikilink">圣索非亚大教堂</a></strong></p></td>
<td><p>信赖、尊重</p></td>
<td><p><a href="../Page/土耳其.md" title="wikilink">土耳其</a><a href="../Page/伊斯坦堡.md" title="wikilink">伊斯坦堡</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aya_sofya.jpg" title="fig:Aya_sofya.jpg">Aya_sofya.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/清水寺.md" title="wikilink">清水寺</a></strong></p></td>
<td><p>明靜、安詳</p></td>
<td><p><a href="../Page/日本.md" title="wikilink">日本</a><a href="../Page/京都.md" title="wikilink">京都</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Qingshuisi.jpg" title="fig:Qingshuisi.jpg">Qingshuisi.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/克里姆林宫.md" title="wikilink">克里姆林宫</a>、<a href="../Page/红场.md" title="wikilink">红场和</a><a href="../Page/华西里·柏拉仁诺教堂.md" title="wikilink">华西里·柏拉仁诺教堂</a></strong></p></td>
<td><p>不屈不朽、象徵主義</p></td>
<td><p><a href="../Page/俄羅斯.md" title="wikilink">俄羅斯</a><a href="../Page/莫斯科.md" title="wikilink">莫斯科</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Moscow_Kremlin.jpg" title="fig:Moscow_Kremlin.jpg">Moscow_Kremlin.jpg</a>]]<br />
<a href="https://zh.wikipedia.org/wiki/File:StBasile_SpasskayaTower_Red_Square_Moscow.hires.jpg" title="fig:StBasile_SpasskayaTower_Red_Square_Moscow.hires.jpg">StBasile_SpasskayaTower_Red_Square_Moscow.hires.jpg</a> and Spasskaya Tower of <a href="../Page/Moscow_Kremlin.md" title="wikilink">Moscow Kremlin</a> at Red Square in <a href="../Page/Moscow.md" title="wikilink">Moscow</a>]]</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/馬丘比丘.md" title="wikilink">馬丘比丘</a></strong></p></td>
<td><p>一致、奉獻</p></td>
<td><p><a href="../Page/祕魯.md" title="wikilink">祕魯</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Peru_Machu_Picchu_Sunrise.jpg" title="fig:Peru_Machu_Picchu_Sunrise.jpg">Peru_Machu_Picchu_Sunrise.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/新天鵝堡.md" title="wikilink">新天鵝堡</a></strong></p></td>
<td><p>夢想、創造力</p></td>
<td><p><a href="../Page/德國.md" title="wikilink">德國</a><a href="../Page/富森.md" title="wikilink">富森</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Castle_Neuschwanstein.jpg" title="fig:Castle_Neuschwanstein.jpg">Castle_Neuschwanstein.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/佩特拉.md" title="wikilink">佩特拉</a></strong></p></td>
<td><p>工藝、保持</p></td>
<td><p><a href="../Page/约旦.md" title="wikilink">约旦</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Petra_Jordan_BW_21.JPG" title="fig:Petra_Jordan_BW_21.JPG">Petra_Jordan_BW_21.JPG</a> at Petra]]</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/吉萨金字塔.md" title="wikilink">吉萨金字塔</a></strong></p></td>
<td><p>不朽、永恆</p></td>
<td><p><a href="../Page/埃及.md" title="wikilink">埃及</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gizeh_Cheops_BW_1.jpg" title="fig:Gizeh_Cheops_BW_1.jpg">Gizeh_Cheops_BW_1.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/自由女神像.md" title="wikilink">自由女神像</a></strong></p></td>
<td><p>寬大、信賴</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/紐約.md" title="wikilink">紐約</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Freiheitsstatue_NYC_full.jpg" title="fig:Freiheitsstatue_NYC_full.jpg">Freiheitsstatue_NYC_full.jpg</a>]]</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/巨石阵.md" title="wikilink">巨石阵</a></strong></p></td>
<td><p>策劃、耐力</p></td>
<td><p><a href="../Page/英國.md" title="wikilink">英國</a><a href="../Page/埃姆斯伯里.md" title="wikilink">埃姆斯伯里</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Stonehenge_back_wide.jpg" title="fig:Stonehenge_back_wide.jpg">Stonehenge_back_wide.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/悉尼歌剧院.md" title="wikilink">悉尼歌剧院</a></strong></p></td>
<td><p>抽像、創造力</p></td>
<td><p><a href="../Page/澳大利亞.md" title="wikilink">澳大利亞</a><a href="../Page/悉尼.md" title="wikilink">悉尼</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Sydney_Opera_House_Sails.jpg" title="fig:Sydney_Opera_House_Sails.jpg">Sydney_Opera_House_Sails.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/泰姬瑪哈陵.md" title="wikilink">泰姬瑪哈陵</a></strong></p></td>
<td><p>熱戀、愛慕</p></td>
<td><p><a href="../Page/印度.md" title="wikilink">印度</a><a href="../Page/阿格拉.md" title="wikilink">阿格拉</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:TajMahalbyAmalMongia.jpg" title="fig:TajMahalbyAmalMongia.jpg">TajMahalbyAmalMongia.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/廷巴克图.md" title="wikilink">廷巴克图</a></strong></p></td>
<td><p>智慧、神秘</p></td>
<td><p><a href="../Page/馬利.md" title="wikilink">馬利</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Sankore_Mosque_in_Timbuktu.jpg" title="fig:Sankore_Mosque_in_Timbuktu.jpg">Sankore_Mosque_in_Timbuktu.jpg</a></p></td>
</tr>
</tbody>
</table>

## 爭議

因為新七大奇跡的評選完全依靠网民投票，虽然前后共有1亿左右的投票量，但仍然不能反映非网友的评价。此外整个评选过程被认为充斥着“商业味”，批评者的理由是：通过网上投票，每台电脑只能投一票，而通过购买纪念品等方法不限制投票数量，而且其揭晓仪式的入场券收入也是重要资金来源。\[5\]

埃及的評論把這次評選，視作挑戰[吉萨金字塔作為唯一保存下来的](../Page/吉萨金字塔.md "wikilink")[古代世界七大奇迹的地位](../Page/古代世界七大奇迹.md "wikilink")。評論員Al-Sayed
al-Naggar在一份主要的國有报章的社論寫道：「這很可能是針對埃及的陰謀，打擊它的文化和古迹。」埃及文化部長Farouq
Hosni批评這項活動「荒唐」，又指发起人韋伯「最關心的是自我宣傳」。埃及研究[世界遺產專家Nagib](../Page/世界遺產.md "wikilink")
Amin指出：「這項投票除了有商業味，更是沒有科學根據。」\[6\]

受埃及指責後，吉薩金字塔被授予特殊地位，從候選名单中去除。網站的通知表示：「新七大奇蹟基金會將吉薩金字塔，就是唯一尚存的古代世界奇蹟，指定为新七大奇蹟的榮譽候選項目，並從候選名單中去除。」

[聯合國教科文組織發表聲明](../Page/聯合國教科文組織.md "wikilink")，與新七奇蹟劃清界線，指投票結果與[世界遺產完全無關](../Page/世界遺產.md "wikilink")，僅是反映投票網友的意見，對[古蹟維護和保存並沒有實際意義](../Page/古蹟.md "wikilink")。在一份2007年6月20日的新聞稿中，再次證實它和這項評選沒有任何關連\[7\]。新聞稿的其中一段說：「韋伯先生的媒體宣傳活動，與聯合國教科文組織藉由編錄[世界遺產列表進行的科學和教育工作](../Page/世界遺產.md "wikilink")，完全沒有相似點。世界新七大奇跡名單，是一項私人活動的結果，只反映能連上網絡的人的意見，而不是整個世界的意見。這次活動的開展，不能以任何顯著和持續的方式，協助維護公眾選出的古跡。」

[教廷亦不滿名單上沒有原](../Page/教廷.md "wikilink")[基督宗教盛行地](../Page/基督宗教.md "wikilink")[歐洲的基督教古蹟](../Page/歐洲.md "wikilink")（雖然聖蘇菲亞大教堂原來是教堂建築，但在1453年以後被改為[清真寺](../Page/清真寺.md "wikilink")，而基督教的祈禱儀式在那裡是被禁止的，而里約熱內盧基督像位於南美洲）\[8\]\[9\]

在新七大奇蹟公佈後，有很多人慶祝，但也有不少人感到憤怒，覺得他們國家的重要古跡被不公平的評選貶低了。一名受歡迎的美籍柬埔寨作家Oudam
Em，對柬埔寨的傳奇建築吳哥窟落選強烈不滿，他認為吳哥窟「無論比規模，比輝煌，比建築的精密，都明顯地把很多被選上的遺跡遠遠比下去。」他續說：「世上如果真的有第八大奇蹟，卻不是吳哥窟的話，那就只因為這壯麗的古蹟沒有被選上七大奇蹟。」\[10\]

[復活節島](../Page/復活節島.md "wikilink")[摩艾石像的智利代表Alberto](../Page/摩艾石像.md "wikilink")
Hotus稱，主辦者韋伯致信表示，摩艾石像總得票排第八位，論「良心」是列在新七大奇蹟的。Hotus說他是唯一接到主辦單位道歉的參與代表。\[11\]

## 参考文献

## 外部链接

  - [新七大奇蹟協會](http://www.new7wonders.com/)

## 參見

  - [世界新七大自然奇觀](../Page/世界新七大自然奇觀.md "wikilink")
  - [世界七大奇蹟](../Page/世界七大奇蹟.md "wikilink")
  - [古代世界七大奇蹟](../Page/古代世界七大奇蹟.md "wikilink")
  - [新七大奇蹟協會](../Page/新七大奇蹟協會.md "wikilink")
  - [世界第八大奇迹](../Page/世界第八大奇迹.md "wikilink")

{{-}}

[de:Weltwunder\#Die „Neuen 7
Weltwunder“](../Page/de:Weltwunder#Die_„Neuen_7_Weltwunder“.md "wikilink")

[Category:七大奇迹](../Category/七大奇迹.md "wikilink")

1.  [长城将竞选世界新七大奇迹 21世界建筑获提名](http://news.xinhuanet.com/house/2006-01/03/content_4002073.htm)．新华网．2006年1月3日
2.  [NOWC 重大事件表](http://www.new7wonders.com/index.php?id=48)
3.
4.  [参选项目表](http://www.new7wonders.com/index.php?id=306)
5.  [世界“新七大奇迹”评选引争议](http://www.chinadaily.com.cn/hqpl/2007-06/28/content_904757.htm)．中国日报网环球在线．2007年06月28日
6.  [Egypt fumes over fresh seven wonders competition for
    pyramids](http://news.yahoo.com/s/afp/20070125/wl_mideast_afp/egyptarchaeology)．法新社．2007年1月25日
7.  [UNESCO confirms that it is not involved in the "New 7 wonders of
    the world"
    campaign](http://portal.unesco.org/en/ev.php-URL_ID=38482&URL_DO=DO_TOPIC&URL_SECTION=201.html)．UNESCO.org．2007年6月7日
8.   [Vatican left aghast by new Seven Wonders
    list](http://travel.timesonline.co.uk/tol/life_and_style/travel/article2033898.ece)．泰晤士報．2007年7月6日
9.  [By popular vote, the 'New 7
    Wonders'](http://www.chicagotribune.com/news/nationworld/la-fg-wonders8jul08,1,6456218.story?coll=chi-news-hed)．芝加哥論壇報．2007年7月8日
10. Oudam.com．[The 8th Wonder of the
    World](http://www.oudam.com/cambodia/angkor-wat-did-not-make-new-seven-wonders-of-the-world.html)
11. Las Últimas Noticias．[Líder pascuense furioso porque le dieron a la
    isla un triunfo
    moral](http://www.lun.com/modulos/catalogo/paginas/2007/07/10/LUCPRDI04LU1007.htm?tipoPantalla=1024)
    ．2007年7月10日