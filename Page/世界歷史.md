[世界歷史簡表.png](https://zh.wikipedia.org/wiki/File:世界歷史簡表.png "fig:世界歷史簡表.png")

**世界歷史**，簡稱**世界史**，又稱**人類史**，一般是指有[人類以來](../Page/人類.md "wikilink")[地球上](../Page/地球.md "wikilink")[歷史的總和](../Page/歷史.md "wikilink")，雖然[世界歷史本身早在人類](../Page/世界.md "wikilink")[文明出現之前就存在](../Page/文明.md "wikilink")，但人類一直到近現代才真正用這個概念來研究和述說歷史。世界历史根据不同的时间段，可以分为[古代史](../Page/古代史.md "wikilink")、[近代史](../Page/近代史.md "wikilink")、[现代史等](../Page/现代史.md "wikilink")；根据不同的地区，可分为不同地区的历史，例如[中国历史](../Page/中国历史.md "wikilink")、[美国历史](../Page/美国历史.md "wikilink")；根据不同的代表事物，可以分为不同事物的历史，例如[環境史](../Page/環境史.md "wikilink")、[航空史](../Page/航空史.md "wikilink")。

## 「世界史」概念和研究的演變

被譽為歷史之父的[古希臘](../Page/古希臘.md "wikilink")[歷史学家](../Page/歷史学家.md "wikilink")[希羅多德所著](../Page/希羅多德.md "wikilink")《[歷史](../Page/歷史_\(希羅多德\).md "wikilink")》一書，雖然只記述部份[歐洲](../Page/歐洲.md "wikilink")、[地中海沿岸](../Page/地中海.md "wikilink")、[西亞等地中心的地區所發生的](../Page/西亞.md "wikilink")[波希戰爭](../Page/波希戰爭.md "wikilink")，卻已發展出國與國間的相對關係，是目前所知較早具有世界史觀念的著作。但因這些記載多來自口耳相傳，故也有人認為著作《[伯羅奔尼撒戰爭史](../Page/伯羅奔尼撒戰爭史.md "wikilink")》的[修昔底德才是歷史之父](../Page/修昔底德.md "wikilink")。

在東方[中國](../Page/中國.md "wikilink")[歷史著作](../Page/歷史.md "wikilink")[司馬遷的](../Page/司馬遷.md "wikilink")《[史記](../Page/史記.md "wikilink")》，大體以[中國為中心](../Page/中國.md "wikilink")，僅略旁及[四夷](../Page/四夷.md "wikilink")(外族)，但也可大致代表當時[中國人所知的世界](../Page/中國人.md "wikilink")。

[中世紀以後](../Page/中世紀.md "wikilink")，西方[史學受到](../Page/史學.md "wikilink")[基督教影響](../Page/基督教.md "wikilink")，開始出現把全世界視為統一的，將世界歷史作為走向[天國歷程的觀念](../Page/天國.md "wikilink")。最明顯受此影響的是的《[世界編年史](../Page/世界編年史.md "wikilink")》，可以說是最早的一本「世界史」著作。

但早期的「世界史」不可能記述本身文明之外的歷史，因此真正意義上的世界史，則必須要待近世的[地理大發現以後才可能逐漸出現](../Page/地理大發現.md "wikilink")，但早期受[文藝復興的人文思潮影響](../Page/文藝復興.md "wikilink")，是先開始對各地區而不是整體的世界歷史產生興趣，而[啟蒙運動則促使西方真正對於全世界的歷史發展產生興趣](../Page/啟蒙運動.md "wikilink")，到十九世紀史學更進一步發展，並形成許多解釋世界歷史的理論，雖然當時對於世界史的了解，常是西方中心及進化史觀式的，但是已近真正意義上的世界歷史。

這種世界史的概念也在西方向外發展的過程中影響到世界各地，如[日本在十九世紀末開始研究世界史](../Page/日本.md "wikilink")，中國[知識分子也開始探求世界歷史](../Page/知識分子.md "wikilink")，但在歐洲，一直到二十世紀中後期，[歐洲史仍然是](../Page/歐洲史.md "wikilink")[學術主流](../Page/學術.md "wikilink")，直到1980年代以來，打破以[西方為中心的觀念後](../Page/西方世界.md "wikilink")，歐洲以及[美國的歷史課程主流](../Page/美國.md "wikilink")，才開始由[西洋史轉向](../Page/西洋史.md "wikilink")[世界史](../Page/世界史.md "wikilink")。

## 石器時代

### 舊石器時代

[World_map_of_prehistoric_human_migrations.jpg](https://zh.wikipedia.org/wiki/File:World_map_of_prehistoric_human_migrations.jpg "fig:World_map_of_prehistoric_human_migrations.jpg")的路線圖，根據[粒線體的](../Page/粒線體.md "wikilink")[族群遺傳學而繪成](../Page/族群遺傳學.md "wikilink")（此為千年前的情況）。\]\]
舊石器時代是石器時代的第一個時期。

根據[遺傳學與](../Page/遺傳學.md "wikilink")[化石學所提供的證據顯示](../Page/化石學.md "wikilink")，現代人類「[智人](../Page/智人.md "wikilink")」的發源地应该是在[非洲](../Page/非洲.md "wikilink")。其大約在二十萬年左右出現，其時為[舊石器時代](../Page/舊石器時代.md "wikilink")，人類已進化了一段很長的時間。人類的祖先，如「[直立人](../Page/直立人.md "wikilink")」曾經使用簡單工具達數千年，但隨著時代演進，工具變得更為精練與複雜。人類亦隨之創造了[語言與](../Page/語言的起源.md "wikilink")[下葬儀式](../Page/葬礼.md "wikilink")。

此時期的人類亦可以裝飾自己的外表。此時所有人類都是[獵人與](../Page/獵人.md "wikilink")[採集者](../Page/採集者.md "wikilink")，並且普遍過著[遊牧的生活](../Page/遊牧.md "wikilink")。

現代人很快便從非洲和亞歐兩洲的非結冰地帶散播至世界各處。在最近的[冰河時期時](../Page/冰河時期.md "wikilink")，人類更大量移民至[北美洲與](../Page/北美洲.md "wikilink")[大洋洲](../Page/大洋洲.md "wikilink")，而現時的溫帶區在當時是極不適合生活的。縱使如此，在冰河期完結時，即大約一萬二千年前，人類已移民至地球上所有非冰封的土地上。

獵人與採集者的社會逐漸細分，並開始出現[階級觀念](../Page/階級.md "wikilink")，而且長途接觸亦變得可能，如[澳洲原住民使用的原始](../Page/澳洲原住民.md "wikilink")「[高速公路](../Page/高速公路.md "wikilink")」。

大部份獵人與採集者的社會最終將發展為農業社會，或融入於其他較大型的農業社會。然而，未發展為農業社會的群體，最後也許被消滅，或維持孤立，這種小型獵人與採集者的社會在今天的偏遠地區仍時有所見。

### 中石器時代

中石器時代為[舊石器時代與](../Page/舊石器時代.md "wikilink")[新石器時代間的一段](../Page/新石器時代.md "wikilink")[人類](../Page/人類.md "wikilink")[科技發展時期](../Page/科技.md "wikilink")。其在[更新世末期](../Page/更新世.md "wikilink")，即大約一萬年前開始，終結於[農業的出現](../Page/農業.md "wikilink")，而這時間在各地區不盡相同。在部份例子裡，如[近東地區](../Page/近東.md "wikilink")，農業在更新世末期已開始出現，因此其中石器時代是極短暫的，且難以定義。在那些沒有受到太多冰河時期衝擊的地區，有時候會比較傾向使用「」這個詞。而那些受到較大環境因素影響，如處於[後冰河時期影響帶的地區](../Page/冰河時期.md "wikilink")，則有著更顯著的中石器時代，最少達數千年之久。以北歐為例，那裡的部落社會因為所居住地區氣候較溫暖而且有著食物供應豐富的沼澤地，所以可以較遲發展農耕，因此其中石器時代較長。這些環境條件製造了具有特色人類生活，並且被紀錄下來，諸如[馬格爾莫斯文化](../Page/馬格爾莫斯文化.md "wikilink")（**）與[阿齊利文化](../Page/阿齊利文化.md "wikilink")（**）。但這樣的環境條件同樣使得北歐的新石器時代遲至公元前四千年才來臨。

此時期的遺跡不多，主要是[貝塚](../Page/贝塚.md "wikilink")（**，或稱垃圾堆）。在森林地區，[砍伐森林的活動開始出現](../Page/砍伐森林.md "wikilink")，但是要在新石器時代才大量進行，因為那時的人們需要通過砍伐森林來開墾更多耕地。

中石器時代的特徵是大量使用著小型而複雜的[燧石工具](../Page/燧石.md "wikilink")，諸如[小結石](../Page/小結石.md "wikilink")（**）與[小鏨子](../Page/小鏨子.md "wikilink")（**）等。[釣魚用具](../Page/釣魚用具.md "wikilink")、石製[手斧與木製物品如](../Page/手斧.md "wikilink")[獨木舟和](../Page/獨木舟.md "wikilink")[弓箭等亦在部份中石器時代的遺址發現](../Page/弓箭.md "wikilink")。

### 新石器時代

新石器時代為石器時代的原始科技與社會發展的最後階段。其約於公元前一萬年開始，而主要特徵是早期[部落群居](../Page/部落群居.md "wikilink")、[農業](../Page/農業.md "wikilink")、[畜牧業與](../Page/畜牧業.md "wikilink")[工具的發展](../Page/工具.md "wikilink")。

#### 農業發展

根據考古學家[戈登·柴爾德所形容](../Page/戈登·柴爾德.md "wikilink")，在大約公元前九千年出現的農耕社會是一項革命性的大轉變。在大約公元前九千五百年，[苏美尔人首先開始務農](../Page/苏美尔.md "wikilink")。在大約公元前七千年，農業傳播至[印度](../Page/印度.md "wikilink")，而在大約公元前六千年，其傳播至埃及，在大約公元前五千年，中國人亦開始務農。在大約公元前二千七百年，農業傳播至[中部美洲](../Page/中部美洲.md "wikilink")。

苏美尔人在大約公元前五千五百年開始有系統地[灌溉農作物](../Page/灌溉.md "wikilink")，並且出現社會分工。而此時[青銅與](../Page/青銅.md "wikilink")[鐵開始取代](../Page/鐵.md "wikilink")[石頭作為農務與戰爭的工具](../Page/石頭.md "wikilink")。在公元前三千年的[歐亞大陸上](../Page/歐亞大陸.md "wikilink")，[銅製與](../Page/銅.md "wikilink")[青銅製的工具](../Page/青銅.md "wikilink")、[裝飾品與](../Page/裝飾品.md "wikilink")[武器開始普及](../Page/武器.md "wikilink")。緊接著青銅時代，在東[地中海](../Page/地中海.md "wikilink")、[中東與](../Page/中東.md "wikilink")[中國等地陸續出現鐵製工具與武器](../Page/中國.md "wikilink")。

在公元前九百年出現的[查文文化誕生前](../Page/查文文化.md "wikilink")，美洲人可能並沒有金屬工具。而在其後的[莫切文化則有著金屬製的盔甲](../Page/莫切文化.md "wikilink")、刀子與餐具。即使使用金屬工具較少的[印加文明亦有著金屬犁頭](../Page/印加文明.md "wikilink")。然而在[秘魯土地上的考古學研究仍然只有很少](../Page/秘魯.md "wikilink")，大部份[奇普](../Page/奇普.md "wikilink")，印加人使用的繩結形式的紀錄工具）在[西班牙征服秘魯時被燒毀](../Page/西班牙征服秘魯.md "wikilink")。儘管如此，在公元2004年，考古學家仍在秘魯發掘出整個古代城市遺跡。部份證據顯示秘魯被歐洲人開發前，當地人已懂得製作[鋼材](../Page/鋼.md "wikilink")。

而河谷在此時則成為早期文明的搖籃，如中國的[黃河](../Page/黃河.md "wikilink")、[埃及的](../Page/埃及.md "wikilink")[尼羅河](../Page/尼羅河.md "wikilink")、[印度的](../Page/印度.md "wikilink")[印度河流域與中東的](../Page/印度河.md "wikilink")[兩河流域](../Page/兩河流域.md "wikilink")。部份過著遊牧生活的人們，如澳洲土著與南非的[布須曼人則在近代前仍沒有農業出現](../Page/布須曼人.md "wikilink")。

農業使社會變得複雜，亦因此出現了[文明](../Page/文明.md "wikilink")。城鎮與市場隨之出現。科技改進了人們控制[自然的能力](../Page/自然.md "wikilink")，而人們亦發展了[交通與](../Page/交通.md "wikilink")[通訊](../Page/通訊.md "wikilink")。

#### 宗教發展

很多歷史學家將[信仰的起源追溯至新石器時代](../Page/信仰.md "wikilink")。大部份此時的信仰包括對母親神（Mother
Goddess）、天父（Sky
Father）、[太陽與](../Page/太陽.md "wikilink")[月亮的崇拜](../Page/月亮.md "wikilink")，並視太陽與月亮為造物主。

後世才出現了[基督教](../Page/基督教.md "wikilink")、[佛教](../Page/佛教.md "wikilink")、[伊斯蘭教等](../Page/伊斯蘭教.md "wikilink")[宗教](../Page/宗教.md "wikilink")。

## 文明的興起

### 城邦

文明的誕生必須依賴於穩定的農業地區，因此農業的出現引起了少數主要轉變，因為農業的生產帶來充足的糧食，使得人們不需要再從事漁獵與游牧性活動，其亦使得人口密度可大量提升，逐漸的人類的社群開始出現分工，在農業生產的穩定條件下，開始出現相因應的相關行業，例如農具：犁、耙的製造而出現手工業，又因為人類的社群需要管理與規劃，政治性的需求因而誕生，從而出現[國家](../Page/國家.md "wikilink")。對於「國家」（**）一詞有著數個定義。[韋伯與](../Page/馬克斯·韋伯.md "wikilink")[伊里亞斯將其定義為一群有組織的人群合理地獨佔着某一特定的地區](../Page/諾博特·伊里亞思.md "wikilink")。

[Greatwall-SA3.jpg](https://zh.wikipedia.org/wiki/File:Greatwall-SA3.jpg "fig:Greatwall-SA3.jpg")，其長達六千七百公里，早於公元前三世紀已開始興建，以保衛中國免於外族的侵襲。其曾被數次重建與擴建。\]\]
在公元前三千五百年至三千年間，首批國家在[美索不達米亞](../Page/美索不達米亞.md "wikilink")、[古埃及與](../Page/古埃及.md "wikilink")[印度河谷出現](../Page/印度河谷.md "wikilink")。在美索不達米亞，最多存在著3000個[城邦](../Page/城邦.md "wikilink")。[古埃及則是先有國家](../Page/古埃及.md "wikilink")，而沒有城鎮，但城鎮很快亦冒起。一個國家需要[軍隊來保衛其領土](../Page/軍事力量.md "wikilink")。而軍隊則需要[官僚機構來負責維持](../Page/官僚機構.md "wikilink")。唯一例外是[印度河流域文明](../Page/印度河流域文明.md "wikilink")，因為該處的國家並沒有證據顯示曾擁有著軍事力量。

在公元前三千年至二千年間，在[中國地區亦出現了很多小型國家](../Page/中國.md "wikilink")。而在此時，[中東地區的國家間開始爆發戰事](../Page/中東.md "wikilink")。世界上首條[和平條約](../Page/和平條約.md "wikilink")，[卡疊什](../Page/卡疊什.md "wikilink")[和約](../Page/埃及赫梯和約.md "wikilink")，是在公元前1259年，由[赫梯人與](../Page/赫梯人.md "wikilink")[古埃及人所簽訂](../Page/古埃及人.md "wikilink")。這時古代著名的大帝國開始陸續出現，諸如[亚述帝国](../Page/亚述.md "wikilink")（公元前8世纪）、[波斯帝國](../Page/阿契美尼德帝国.md "wikilink")（公元前六世紀）、[孔雀帝國和](../Page/孔雀帝國.md "wikilink")[马其顿帝国](../Page/马其顿帝国.md "wikilink")（公元前四世紀）、[秦帝國和](../Page/秦朝.md "wikilink")[帕提亚帝国](../Page/帕提亚帝国.md "wikilink")（公元前三世纪）、[羅馬帝國](../Page/羅馬帝國.md "wikilink")（公元前一世紀）、波斯帝国[萨珊王朝](../Page/萨珊王朝.md "wikilink")（公元三世纪）。

在公元751年，中東與東亞的兩大帝國首次爆發衝突，當時統治中東的[阿拔斯王朝與統治中國的](../Page/阿拔斯王朝.md "wikilink")[唐朝在](../Page/唐朝.md "wikilink")[怛羅斯決戰](../Page/怛羅斯戰役.md "wikilink")，結果唐軍戰敗。而史上以陆地连续性計，最大的帝國[蒙古帝國則在公元](../Page/蒙古帝國.md "wikilink")13世紀出現。此後，大部份居住在东欧、亞洲的人們均隸屬於國家統治下，而在[墨西哥與](../Page/墨西哥.md "wikilink")[西南非亦有著文明國家](../Page/西南非.md "wikilink")。諸國不斷擴張，並控制著越來越多的土地與人口；最後一塊「無主土地」－[南極洲亦在公元](../Page/南極洲.md "wikilink")1878年的[柏林會議裡被瓜分](../Page/1878年柏林會議.md "wikilink")。

### 城市與貿易

[Vascodagama.JPG](https://zh.wikipedia.org/wiki/File:Vascodagama.JPG "fig:Vascodagama.JPG")末至[十六世紀初](../Page/十六世紀.md "wikilink")，[達伽馬發現了印度航線](../Page/達伽馬.md "wikilink")，為歐洲帶來了大量香料。\]\]
在農業發展的同時，糧倉产量也大增。因此不從事農耕的人們也可以獲得糧食，而城市亦因而出現。城市成為國家的重要地區，而居住在城市裡的人們幾乎不從事農業生產。城市由附近的鄉郊地區獲取食糧，作为回報，則為鄉郊地區提供軍事力量保衛其財產。

城市的發展導致了[文明的誕生](../Page/文明.md "wikilink")：首先是下[美索不達米亞的](../Page/美索不達米亞.md "wikilink")[蘇美尔文明](../Page/苏美尔.md "wikilink")（公元前三千五百年），其後是[尼羅河流域的](../Page/尼羅河.md "wikilink")[埃及文明](../Page/古埃及.md "wikilink")（公元前三千三百年），接著是[印度河流域文明](../Page/印度河流域文明.md "wikilink")（公元前二千五百年）與[黃河流域的](../Page/黃河.md "wikilink")[華夏文明](../Page/華夏文明.md "wikilink")（公元前二千年）。有證據顯示當時的城市已頗具規模，並且有著高度社會與經濟複雜性。不過上述文明皆是獨立發展的，互不隸屬。在此時，[書寫與](../Page/書寫.md "wikilink")[貿易正式在人類社會出現](../Page/貿易.md "wikilink")。

在中國，雖然最初的原始部落社會在公元前二千五百年已出現，但首個進入信史時代的朝代卻是約於公元前一千五百年出現的[商朝](../Page/商朝.md "wikilink")。而在公元前二千年左右，[克里特岛](../Page/克里特岛.md "wikilink")、[希臘本土與](../Page/希臘.md "wikilink")[小亚细亚中部的赫梯王国開始出現](../Page/小亚细亚.md "wikilink")。在美洲，[馬雅文明](../Page/馬雅文明.md "wikilink")、[莫切文化與](../Page/莫切文化.md "wikilink")[納斯卡文明等約在公元](../Page/納斯卡文明.md "wikilink")[前十世紀末於](../Page/前十世紀.md "wikilink")[中美洲與](../Page/中美洲.md "wikilink")[秘魯等地出現](../Page/秘魯.md "wikilink")。而用於交易的[貨幣則首先出現於](../Page/貨幣.md "wikilink")[呂底亞](../Page/呂底亞.md "wikilink")。

長途貿易路線於公元前三千年首次出現，位於美索不達米亞的苏美尔人與位於印度河谷的哈拉巴人進行貿易。中國與叙利亞間的[絲綢之路約於公元前二千年出現](../Page/絲綢之路.md "wikilink")，而位於其間的[中亞城市與](../Page/中亞.md "wikilink")[波斯則為此交易路線主要中轉站](../Page/波斯帝國.md "wikilink")。[腓尼基與](../Page/腓尼基.md "wikilink")[古希臘](../Page/古希臘.md "wikilink")[文明在公元前一千年至公元前一世紀間於地中海地區形成了貿易帝國](../Page/文明.md "wikilink")。而[阿拉伯人則於首個千年的末期與第二個千年的初期](../Page/阿拉伯世界.md "wikilink")（公元650年至公元1250年）控制了[印度洋](../Page/印度洋.md "wikilink")、[東亞與](../Page/東亞.md "wikilink")[撒哈拉沙漠的貿易路線](../Page/撒哈拉沙漠.md "wikilink")。在首個千年末期，阿拉伯人與猶太人亦控制了地中海地區的貿易。而在第二個千年初期，[義大利人取代其位置](../Page/義大利人.md "wikilink")，控制了地中海地區的貿易。[法蘭德斯與](../Page/法蘭德斯.md "wikilink")[德意志的城市則在第二個千年初成為北歐貿易路線的中心](../Page/大德意志.md "wikilink")。在任何地區，主要的城市皆位處貿易路線的樞紐地帶。

### 宗教與哲學

新的[哲學與](../Page/哲學.md "wikilink")[宗教在東方與西方世界皆不斷湧現](../Page/宗教.md "wikilink")，特別是在公元[六世紀左右](../Page/六世紀.md "wikilink")。不論何時，世界上均有著很多不同的宗教，如[印度的](../Page/印度.md "wikilink")[印度教與](../Page/印度教.md "wikilink")[佛教](../Page/佛教.md "wikilink")、[波斯的](../Page/波斯帝國.md "wikilink")[祆教等早期主要宗教](../Page/祆教.md "wikilink")。[闪米特诸教在此時亦開始冒起](../Page/闪米特诸教.md "wikilink")。在中國，人們直至今天仍受到[儒](../Page/儒家.md "wikilink")、[墨](../Page/墨家.md "wikilink")、[道三家的思想所影響](../Page/道家.md "wikilink")，其中以儒家為甚。在[漢武帝罷黜百家](../Page/漢武帝.md "wikilink")，獨尊儒術後，中國人二千年來深受其薰陶。在西方，以[柏拉圖與](../Page/柏拉圖.md "wikilink")[亞里士多德為代表的希臘傳統哲學思想](../Page/亞里士多德.md "wikilink")，則在[亞歷山大大帝於公元](../Page/亞歷山大大帝.md "wikilink")[前四世紀進行征服戰爭後散播至整個歐洲和中東](../Page/前四世紀.md "wikilink")。

## 主要文明與地區

### 中世纪封建时代

在公元[前一世紀](../Page/前一世紀.md "wikilink")，於[地中海](../Page/地中海.md "wikilink")、[恒河與](../Page/恒河.md "wikilink")[黃河地區出現了龐大的](../Page/黃河.md "wikilink")[帝國](../Page/帝国.md "wikilink")。在[印度](../Page/印度歷史.md "wikilink")，[孔雀帝國統治了大半的](../Page/孔雀帝國.md "wikilink")[印度次大陸](../Page/印度次大陸.md "wikilink")，而僅存的小部份[印度南部地區則由](../Page/印度南部.md "wikilink")[潘地亞所控制](../Page/潘地亞.md "wikilink")。在[中國](../Page/中國歷史.md "wikilink")，[秦朝及其後的](../Page/秦朝.md "wikilink")[漢朝建立了中央集權式的大帝國](../Page/漢朝.md "wikilink")。在西方，[羅馬人自公元](../Page/古羅馬.md "wikilink")[前三世紀開始以侵略與殖民的手段來擴張領土](../Page/前三世紀.md "wikilink")，於[奧古斯都在位時](../Page/奧古斯都.md "wikilink")，即[耶穌基督出生的年代](../Page/耶穌基督.md "wikilink")，羅馬控制了地中海沿岸大部份地區。

這些大帝國的擴張使得其成為該地區的中心地帶，並在完成擴張後出現了較為和平的時期。這種相對的和平使得商人可以從事國際貿易，而當中最為知名的要算是[絲綢之路的興盛](../Page/絲綢之路.md "wikilink")。但這些帝國亦面對著很多共通問題，如其需以巨額的支出來維持龐大的軍隊和中央官僚體系。為了保持帝國的完整，帝國政府開始向農民徵收重稅，結果出現了土地兼併現象，並且日益加劇。而居住於邊境地區的蠻族亦不時向這些帝國進行侵擾，這加速了其內部的崩潰。在中國，强大的東漢逐漸衰弱并於公元220年分裂為[三個國家](../Page/三國.md "wikilink")，中國史上的第一帝國宣告結束；而在西方，羅馬帝國亦於此時開始分崩離析，日耳曼民族相繼入侵。

但歷史不斷重演，在歐亞、美洲與北非的溫帶地區，大帝國此起彼落，從未間斷。

[羅馬帝國由公元](../Page/羅馬帝國.md "wikilink")[二世紀末開始逐漸崩潰](../Page/二世紀.md "wikilink")，這大約與基督教由[中東散播至](../Page/中東.md "wikilink")[歐洲同時期](../Page/歐洲.md "wikilink")。羅馬帝國的西半部在公元[五世紀被來自](../Page/五世紀.md "wikilink")[德意志地區的日耳曼人所佔領](../Page/大德意志.md "wikilink")，並且分裂為很多國家，而羅馬城則成為[教皇國的領土](../Page/教皇國.md "wikilink")；而帝國的東半部則演變為希臘化的[拜占庭帝國](../Page/拜占庭帝國.md "wikilink")。在數個世紀以後，查理曼大帝國結束，由鄂圖一世繼位，分裂成三個，東、中、西法蘭克，其中，東法蘭克的國王鄂圖一世逼迫教皇加冕為帝，後自稱[神聖羅馬帝國](../Page/神聖羅馬帝國.md "wikilink")，其主要控制了現今的德國與意大利大部份地區。

在中國，秦朝建立了大一统的封建中央集权式的专制国家。之后的四百年间，由统一强盛的汉朝统治。汉朝之后封建国家开始分裂，原居於北方的遊牧民族於公元[四世紀初開始入侵](../Page/四世紀.md "wikilink")，史稱[五胡亂華](../Page/五胡亂華.md "wikilink")，並使中國北方陷入長年的混戰裡。4世纪到6世纪是分裂的南北朝时期。直至公元589年，隋朝統一中國，結束了魏晉南北朝數百年的戰亂。而其後的唐朝在前期的统治里开创了盛世局面。然而唐朝在[安史之亂後開始分崩離析](../Page/安史之亂.md "wikilink")，中國重新陷入混戰，直至公元979年，北宋重新統一中國為止。然而來自北方遊牧民族的壓力不斷增加，整個中國北方於公元1141年陷入[女真族所建立的](../Page/女真族.md "wikilink")[金朝之手](../Page/金朝.md "wikilink")，其後全中國更於公元1279年被[蒙古族所建立的](../Page/蒙古族.md "wikilink")[元朝統治](../Page/元朝.md "wikilink")。[十三世紀的](../Page/十三世紀.md "wikilink")[蒙古帝國控制了大部份歐亞地區](../Page/蒙古帝國.md "wikilink")，成為史上以連續陸地領土計，國土面積最大的國家。

在此時，印度北部由[笈多王朝所控制](../Page/笈多王朝.md "wikilink")，而在印度南部，則由三個小王國所控制，分別是[哲羅](../Page/哲羅.md "wikilink")、[朱羅與](../Page/朱羅王朝.md "wikilink")[帕拉瓦](../Page/帕拉瓦王朝.md "wikilink")。而這種相對穩定的狀態使得[印度教文化得以在公元](../Page/印度教.md "wikilink")[四世紀與](../Page/四世紀.md "wikilink")[五世紀間進入黃金時代](../Page/五世紀.md "wikilink")。
[Llullaillaco_mummies_in_Salta_city,_Argentina.jpg](https://zh.wikipedia.org/wiki/File:Llullaillaco_mummies_in_Salta_city,_Argentina.jpg "fig:Llullaillaco_mummies_in_Salta_city,_Argentina.jpg")[萨尔塔省](../Page/萨尔塔省.md "wikilink")[尤耶亚科山发现的木乃伊](../Page/尤耶亚科山.md "wikilink")\]\]
[Machu-Picchu.jpg](https://zh.wikipedia.org/wiki/File:Machu-Picchu.jpg "fig:Machu-Picchu.jpg")，為印加文明的象徵。\]\]
在[中美洲亦出現了很多的文明](../Page/中美洲.md "wikilink")，其中以[馬雅與](../Page/瑪雅文明.md "wikilink")[阿兹特克較為著名](../Page/阿兹特克.md "wikilink")。而隨著[奧爾梅克文明逐漸式微](../Page/奧爾梅克文明.md "wikilink")，很多馬雅城邦逐漸興起，並且很快便遍佈[猶加敦半島與鄰近地區](../Page/猶加敦半島.md "wikilink")。而其後的阿兹特克帝國則建基於其鄰近文明並且深受被征服的人們如[托爾特克族的影響](../Page/托爾特克.md "wikilink")。

[南美洲的](../Page/南美洲.md "wikilink")[印加文明在公元](../Page/印加文明.md "wikilink")[十四世紀與](../Page/十四世紀.md "wikilink")[十五世紀興起](../Page/十五世紀.md "wikilink")，[印加帝國](../Page/印加帝國.md "wikilink")，又稱為四方帝國（Tawantinsuyu）控制了整個[安第斯山脈](../Page/安第斯山脈.md "wikilink")，並於[庫斯科建都](../Page/庫斯科.md "wikilink")。印加人十分富裕與先進，其建立了卓越的[印加道路系統與偉大的](../Page/印加道路系統.md "wikilink")[石造建築](../Page/石造建築.md "wikilink")。

在公元[七世紀](../Page/七世紀.md "wikilink")，[阿拉伯地區出現了](../Page/阿拉伯世界.md "wikilink")[伊斯蘭教](../Page/伊斯蘭教.md "wikilink")，並且成為世界歷史上舉足輕重的宗教之一，印度（主要是現在的巴基斯坦）、中東與北非的人民均在其後改信伊斯蘭教。

在東北非，[努比亞與](../Page/努比亞.md "wikilink")[衣索比亞成為僅存的兩個沒有改信伊斯蘭教的基督教國家](../Page/衣索比亞.md "wikilink")。伊斯蘭教國家在其時擁有較先進的技術，並且進行著跨[撒哈拉沙漠的貿易](../Page/撒哈拉沙漠.md "wikilink")。而對這些貿易所收取的稅款使得北非的國家變得富裕，亦使撒哈拉沙漠以南乾旱地區（Sahel）出現了一系列的王國。

這段時期（[上古至](../Page/上古.md "wikilink")[中古時代](../Page/中古.md "wikilink")）的特徵是緩慢而穩定的技術改進，其中較為重要的發明為[馬鐙鐵與](../Page/馬鐙.md "wikilink")[犁](../Page/犁.md "wikilink")。

### 文明的傳播與交流

世界歷史的發展並非是各文明地區各自獨立發展其各自的文明，很多的情況都是文明與文明之間互相影響所致。最早期的文明傳播是西亞與[埃及文化傳播到](../Page/埃及.md "wikilink")[克里特島](../Page/克里特島.md "wikilink")，形成[愛琴文明](../Page/愛琴文明.md "wikilink")，[愛琴文明在傳播到](../Page/愛琴文明.md "wikilink")[希臘半島](../Page/希臘.md "wikilink")，誕生出[古希臘文化](../Page/古希臘.md "wikilink")。

文明之間的交流與影響方面，最著名的是[亞歷山大大帝東征](../Page/亞歷山大大帝.md "wikilink")，把[古希臘文化與西亞](../Page/古希臘.md "wikilink")、[波斯文化相融合](../Page/波斯.md "wikilink")，並且進而影響到[印度](../Page/印度.md "wikilink")。再者[中國的張騫通西域](../Page/中國.md "wikilink")，也讓許多西域文物流入[中國](../Page/中國.md "wikilink")，最終成為[中華文化的一部份](../Page/中華文化.md "wikilink")，如：琵琶、葡萄鏡。[中國汉朝的造紙術西傳](../Page/中國.md "wikilink")，促成西方知識的傳播與普及。

歷史上重大的爭戰也往往间接產生文明間的碰撞，進而發生相互影響。[蒙古西征與](../Page/蒙古人西征.md "wikilink")[十字軍東征](../Page/十字軍東征.md "wikilink")，前者把[火藥給帶進歐洲](../Page/火藥.md "wikilink")，但火藥帶來了更可怕的毀滅；後者雖然促成了歐洲的航運和商業發展，但卻使[穆斯林世界](../Page/穆斯林世界.md "wikilink")（[回教世界](../Page/回教世界.md "wikilink")）與[基督教世界的仇恨加深](../Page/基督教世界.md "wikilink")，衝突不斷。

## 近代

### 歐洲的興起

#### 歐洲稱雄世界的背景

早期的農業帝國往往被其週遭環境所限制，其生產力持續低下，並且很易因自然災害而出現[民變](../Page/民變.md "wikilink")，致使政權出現轉變。但是在公元一千年左右，世界歷史出現了質的改變。經由貿易累積而來的先進技術與財富拓闊了世事的可能性。這些轉變在農業大國裡最為顯著，如中國、印度與部份伊斯蘭世界。

中國是首個於公元一千年左右發展出先進的貨幣經濟並且打破早期環境限制的國家。當時中國的自耕農不再自給自足，而是將其生產換取其他物品。而其農業系統是在當時歐亞大陸裡最為發達的，因為其擁有最大的未都市化土地。其技術是當時世界裡最為先進的，並且在[鐵器生產](../Page/鐵器.md "wikilink")、[风箱](../Page/风箱.md "wikilink")、[懸索橋建設](../Page/懸索橋.md "wikilink")、[印刷與](../Page/印刷.md "wikilink")[指南針領域具有壟斷地位](../Page/指南針.md "wikilink")。（參看[李约瑟](../Page/李约瑟.md "wikilink")）。此時中國的[宋朝看似處於和六百年後歐洲出現巨大轉變的同一位置](../Page/宋朝.md "wikilink")，然而其卻於此時受到北方[女真族的攻擊](../Page/女真族.md "wikilink")，只能退居[江南](../Page/江南.md "wikilink")，此時期的中華文化正處於顛峰階段，但是由于北方游牧民族所建立政权的军事侵扰，經濟与文化發展受到沉重打擊，在最後更於公元1279年被[蒙古人所建立的](../Page/蒙古族.md "wikilink")[元朝消滅](../Page/元朝.md "wikilink")。

表面上，於公元十四世紀開始的歐洲[文藝復興只是因應世界其他地方的轉變而誕生](../Page/文藝復興.md "wikilink")。但其卻引發了研究學問的精神，並導致了[人文主義](../Page/人文主義.md "wikilink")、[科學革命](../Page/科學革命.md "wikilink")，並最終致使[工業革命出現](../Page/工業革命.md "wikilink")。然而，在公元十七世紀所出現的科學革命並沒有立即對技術革新起到作用，直至公元[十九世紀後半叶](../Page/十九世紀.md "wikilink")，科學理論上的改進才實際應用至發明創作裡。歐洲在公元十八世紀所發展出的兩大優勢為：企業文化與跨大西洋貿易所積累的財富。但是在公元1750年，世界上[勞動生產力最大的中國](../Page/生產力.md "wikilink")，仍然處於與以跨大西洋貿易為主導的歐洲經濟同一水平上\[1\]。

對於歐洲於公元1750年後開始主導世界，[大英帝國英倫三島成為](../Page/大英帝國.md "wikilink")[工業革命中心的現象](../Page/工業革命.md "wikilink")，有著很多不同的解釋。韋伯認為這是因為[宗教改革使得歐洲人變得更為勤勞](../Page/宗教改革.md "wikilink")。另一種社會經濟學解釋則以人口統計學的角度來解釋：歐洲因為有著終生獨身的神職人員、殖民活動、高死亡率的都市中心、接連的戰爭和遲婚，使得其人口增長相較亞洲的國家而言受到較多的限制。勞工短缺意味著資源可以用在節省勞工的技術改革上，如水輪與磨坊、紡紗機與織布機、蒸汽機與輪船，而不是以巨大的資源來維持龐大的人口。亦有人認為歐洲的制度較為優越，其較世界其他地方更著重[財產權與](../Page/所有權.md "wikilink")[自由市場經濟](../Page/自由市場.md "wikilink")，然而在近年，部份學者如[彭慕蘭開始挑戰這種觀點](../Page/彭慕蘭.md "wikilink")。

歐洲的地理亦在此扮演著重要角色。中東、印度與中國皆被山脈所圍繞，但一旦越過這些天然屏障則是一片廣闊的陸地。對比之下，[阿爾卑斯山](../Page/阿爾卑斯山.md "wikilink")、[庇里牛斯山與其他山脈橫貫歐洲](../Page/庇里牛斯山.md "wikilink")，使得歐洲大陸被分割為數個地區。這在一定程度上使得歐洲大陸在面對中亞民族入侵時得到保護。在火器出現前的年代，所有歐亞大陸的土地均受到中亞草原民族的侵擾。這些遊牧民族在軍事上較處於大陸邊緣的農業國家優越，一旦其成功越過諸如印度與中國外圍的山谷時，則變得銳不可擋，而這些入侵往往是毀滅性的。公元1258年，隨著[巴格達被蒙古人攻陷](../Page/巴格達.md "wikilink")，[伊斯蘭黃金時代消亡](../Page/伊斯蘭黃金時代.md "wikilink")。与此同时，印度與中國亦被其大舉入侵。歐洲大陸东部也遭到严重破坏，但西歐尤其英國較少受到蹂踚。

此外，歐洲的地理亦帶來與別处不同的地緣政治。中國、印度與中東地區在歷史上往往有著一個主導國家。在公元一千六百年左右，[鄂圖曼帝國控制大部份中東地區](../Page/鄂圖曼帝國.md "wikilink")、[明朝主導著中國地區](../Page/明朝.md "wikilink")、而[蒙兀兒帝國則控制著北印度](../Page/蒙兀兒帝國.md "wikilink")。與之對比，歐洲大陸則存在著很多不同且關係錯綜複雜的國家。泛歐洲帝國，除了早期的[羅馬帝國外](../Page/羅馬帝國.md "wikilink")，均持續不了多少時間。然而，在敵對邦國間的高度競爭卻是歐洲在其後成功的因素之一。在其他地方，穩定往往勝於發展。在东亚，倭寇侵扰各国，致使中國[明朝中后期至清朝中期实行](../Page/明朝.md "wikilink")[海禁政策](../Page/海禁.md "wikilink")，海軍發展因这些政策而近乎停滯。在歐洲則沒有可能出現這樣的海禁政策，因為諸國林立，要使各國皆奉行同一政策極為困難。如果任何一個邦國自行引入這樣的一種限制，則其將會很快地墜後於競爭者，結果自取滅亡。

另一無庸置疑，使歐洲興起的地理因素則是[地中海](../Page/地中海.md "wikilink")。上千年來，地中海在欧洲南部及非洲北部是重要的海上交通区域，培養了海上貿易、人際與思想交流和發明創造。

另外，相較於中緯度較適宜人居住的地區，熱帶地區則經常受到[自然災害與](../Page/自然災害.md "wikilink")[寄生蟲的影響](../Page/寄生蟲.md "wikilink")，使得當地人民的較容易生病，對於的科學革命和思想啟發不是那麼好的搖籃。

#### 工业文明的崛起

[唐朝驅逐](../Page/唐朝.md "wikilink")[突厥使得部份](../Page/古突厥.md "wikilink")[突厥人輾轉遁入西亞與東南歐一帶](../Page/突厥人.md "wikilink")，他們乃是後來所稱之的[土耳其人](../Page/土耳其.md "wikilink")，當時這一部份地區正屬[東羅馬帝國所轄](../Page/東羅馬帝國.md "wikilink")，[土耳其人進入此區消滅](../Page/土耳其.md "wikilink")[東羅馬帝國後](../Page/東羅馬帝國.md "wikilink")，許多[東羅馬帝國的學者西遷](../Page/東羅馬帝國.md "wikilink")，因而造成歐洲史上的[文藝復興](../Page/文藝復興.md "wikilink")。[文艺复兴于十四世纪在欧洲开始](../Page/文艺复兴.md "wikilink")。尽管一些现代学者对这一人文艺术思潮是否有益於科学仍尚存疑虑，但是这个时期的确极大地促进了阿拉伯地区与欧洲的融合。这里最重要的成就是所谓的[小帆船](../Page/小帆船.md "wikilink")，这种小帆船结合了阿拉伯[三角帆与欧洲](../Page/三角帆.md "wikilink")[方形索船的优点](../Page/方形索船.md "wikilink")，成为能够横跨[大西洋的第一种舰船](../Page/大西洋.md "wikilink")。由于[航海技术的发展](../Page/航海技术.md "wikilink")，[哥伦布于](../Page/克里斯托弗·哥伦布.md "wikilink")1492年横跨大西洋，使[歐亞非大陸与](../Page/歐亞非大陸.md "wikilink")[美洲的信息与物产联系到一起](../Page/美洲.md "wikilink")。明永樂三年起，[郑和](../Page/郑和.md "wikilink")[七下西洋](../Page/鄭和.md "wikilink")，西至今非洲索马里一带。

从此一些欧洲国家开始进行海上扩张及海外殖民。

哥伦布横跨大西洋作为最著名的对历史產生重大影响的外部关联事件之一，给两个大陆都带来了深远的影响。欧洲给美洲带来了從来未曾遇过的疾病，有一个不是很精确的数字，据说或许有超过九成的美洲土著人死于一连串毁灭性的传染病中。此外由于欧洲所拥有的技术优势，諸如骑兵、钢铁和枪炮等，这使得他们征服了[阿兹特克人](../Page/阿兹特克.md "wikilink")、[印加帝国以及其它的](../Page/印加.md "wikilink")[北美文明](../Page/北美.md "wikilink")。

欧洲人获得的美洲地理学知识，大部份都是由欧洲大陆的大西洋沿岸国家贡献的，例如：[葡萄牙](../Page/葡萄牙.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、[英国](../Page/英国.md "wikilink")、[法国及](../Page/法国.md "wikilink")[荷兰](../Page/荷兰.md "wikilink")。[葡萄牙和](../Page/葡萄牙.md "wikilink")[西班牙是第一批主要的征服者](../Page/西班牙.md "wikilink")，并且對西方世界產生了巨大的影响，但很快，更北边一些的國家，如[英国](../Page/英格兰.md "wikilink")、[法国和](../Page/法兰西.md "wikilink")[荷兰开始取代其位置](../Page/荷兰.md "wikilink")，统治了[大西洋](../Page/大西洋.md "wikilink")。通过一系列的战争，先是[十七世纪和](../Page/十七世纪.md "wikilink")[十八世纪的战争](../Page/十八世纪.md "wikilink")，后是[拿破仑战争](../Page/拿破仑战争.md "wikilink")，英国成为了世界第一强的大帝国，领土遍布全球，在其颠峰时期几乎控制了世界陆地的四分之一，并因此被称为「日不落帝国」。

从美洲的土地上以及人民手中剥夺到的黄金和其它资源，皆被船运至欧洲。与此同时，大量的欧洲殖民者开始向这块大陆移民。为了满足新兴殖民地对劳动力的大量需求，将[非洲人做为](../Page/非洲人.md "wikilink")[奴隶的贩卖活动开始了](../Page/奴隶.md "wikilink")。很快美洲许多地方都充满了奴隶这一下等阶层。在西非，一系列国家沿着[奴隶海岸而兴起](../Page/奴隶海岸.md "wikilink")。这些国家剥削离海岸线更远的非洲内陆各處的人民，逐渐繁荣起来。

[Eertvelt,_Santa_Maria.jpg](https://zh.wikipedia.org/wiki/File:Eertvelt,_Santa_Maria.jpg "fig:Eertvelt,_Santa_Maria.jpg")」，於1628年由所繪，展示了[哥倫布航行所用的](../Page/哥倫布.md "wikilink")[卡瑞克帆船](../Page/卡瑞克帆船.md "wikilink")（carrack）的外貌。\]\]
与此同时，1368年由[汉族的](../Page/汉族.md "wikilink")[朱元璋建立的](../Page/朱元璋.md "wikilink")[明朝取代了元朝在中国的统治](../Page/明朝.md "wikilink")。明朝前期统治平稳，国力强盛。中期出现了[资本主义萌芽](../Page/资本主义萌芽.md "wikilink")。后期出现了统治危机，1644年由[李自成农民起义军所灭](../Page/李自成.md "wikilink")。同时关外的[清朝入关并](../Page/清朝.md "wikilink")[统一了中国](../Page/清朝统一战争.md "wikilink")。清朝前期国力强盛，前期的几个皇帝积极有为，开创了大盛世；并且排除了国内外的分裂势力的危机，巩固了国家安全与统一。后来统治逐渐没落。1840年[鸦片战争以后不断受到欧洲列强的侵略](../Page/第一次鸦片战争.md "wikilink")。1911年[辛亥革命成功](../Page/辛亥革命.md "wikilink")，清朝灭亡，[中華民國建國](../Page/中華民國.md "wikilink")，推翻帝制，實行民主共和政體。

相對於其他地域的人，欧洲人在枪炮制造等軍事技術上處於優勢。就在入侵美洲后不久，歐洲人又开始入侵亚洲。[十九世纪初](../Page/十九世纪.md "wikilink")，不列颠已经控制了[南亚次大陆](../Page/南亚次大陆.md "wikilink")，[埃及和](../Page/埃及.md "wikilink")[马来半岛](../Page/马来半岛.md "wikilink")；法国控制着[印度支那](../Page/印度支那.md "wikilink")；荷兰則占据了[荷属东印度群岛](../Page/荷属东印度群岛.md "wikilink")。英国同时还控制着一些仍属于新石器时代人类的地区，諸如[澳大利亚](../Page/澳大利亚.md "wikilink")、[新西兰以及](../Page/新西兰.md "wikilink")[南非](../Page/南非.md "wikilink")，并且英国在美洲殖民地的居民大量涌入这些地区。到十九世纪末期，连非洲最后一块無主土地也被欧洲列强瓜分了。

欧洲的[理性时期直接引发了](../Page/理性时期.md "wikilink")[科学革命](../Page/科学革命.md "wikilink")，这场革命改变了人们对世界的认识，并為[工业革命打下基礎](../Page/工业革命.md "wikilink")。工业革命发源于英国，在這一過程中產生了新式生产组织及模式如[工厂](../Page/工厂.md "wikilink")、[大规模生產](../Page/大量生產.md "wikilink")、[机械化等](../Page/机械化.md "wikilink")。新的生產模式使歐洲能以更快的速度，更少的劳动力生产更多的产品。理性时期的學術思潮引發了[十八世纪晚期的美国和法国革命](../Page/十八世纪.md "wikilink")，催生了今天所知的[民主](../Page/民主.md "wikilink")。民主的发展对世事和[生活质量产生了深远的影响](../Page/生活质量.md "wikilink")。工业革命时期，世界经济建立在以[煤为能源的基础上](../Page/煤.md "wikilink")，与之相应的是新的[运输方式如](../Page/运输.md "wikilink")[铁路及](../Page/铁路.md "wikilink")[蒸汽船的出现](../Page/蒸汽船.md "wikilink")，這使世界变得更小了。與此同時，工業[污染對](../Page/污染.md "wikilink")[自然環境所造成的破壞](../Page/自然環境.md "wikilink")，以数十倍于原始社會的速度增长。

## 二十世紀

### 科技主導

[Nagasakibomb.jpg](https://zh.wikipedia.org/wiki/File:Nagasakibomb.jpg "fig:Nagasakibomb.jpg")的出现。1945年在[长崎投掷的原子弹宣告了](../Page/长崎市.md "wikilink")[第二次世界大战的结束](../Page/第二次世界大战.md "wikilink")，也标志着[冷战的开始](../Page/冷战.md "wikilink")。\]\]
整个二十世纪见证了欧洲影响力的衰弱，这部分是由于[第一次世界大战和](../Page/第一次世界大战.md "wikilink")[第二次世界大战所带来的欧洲内部的破坏](../Page/第二次世界大战.md "wikilink")，同时也由于[美国和](../Page/美国.md "wikilink")[苏联两个](../Page/苏联.md "wikilink")[超级大国](../Page/超级大国.md "wikilink")（）的崛起。二战后，世界各國為了消除国家间的冲突和阻止未来再現战争，而建立了[联合国](../Page/联合国.md "wikilink")，但这目的并没有完全实现。1990年后，苏联的解体使得美国成为世界上唯一的超级强国，也有些人称之为「[超级強国](../Page/超级強国.md "wikilink")」（）。

二十世纪同样见证了意识形态对于世俗社会的强大影响。首先是俄羅斯在1917年[十月革命后进入了](../Page/十月革命.md "wikilink")[共产主义社会](../Page/共产主义.md "wikilink")。共产主义的意识形态為1945年后的[东欧和](../Page/东欧.md "wikilink")1949年后的[中国所接受](../Page/中国.md "wikilink")，并且于1950年代至1960年代广泛影响了[第三世界国家](../Page/第三世界.md "wikilink")。1920年代，[军国主义的](../Page/军国主义.md "wikilink")[法西斯专政者们控制了](../Page/法西斯.md "wikilink")[德国](../Page/德国.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[日本和](../Page/日本.md "wikilink")[西班牙](../Page/西班牙.md "wikilink")。

这些转变体现于空前的规模和破坏性战争中。[第一次世界大战摧毁了欧洲古老的君主政体](../Page/第一次世界大战.md "wikilink")，削弱了法国和英国。[第二次世界大战见证了军国主义在欧洲的兴起和毁灭以及共产主义在东欧和亚洲的崛起](../Page/第二次世界大战.md "wikilink")。紧接着的就是持续四十年以美苏为首以及他们各自盟友间的[冷战](../Page/冷战.md "wikilink")。[核武器的发明将整个人类社会和各种不同的生活形态都置于一种危险的状态中](../Page/核武器.md "wikilink")。冷战所带来的军备竞赛摧毁了苏联的经济基础，这个庞大的国家终于分崩离析，其部份从属国重新以联盟的方式组成了俄罗斯联邦，而其他的則投向了西方世界的怀抱。

本世纪同样见证了技术的长足进步，以及平均寿命和生活质量的极大提升。世界经济的基础由[煤转化为](../Page/煤.md "wikilink")[石油](../Page/石油.md "wikilink")，新的通信和运输技术的发展使得世界越来越紧密地联系在一起。尽管[城市的污染要低于使用煤的时代](../Page/城市.md "wikilink")，但是技术的进步依然带来了许多[环境问题](../Page/生态环境.md "wikilink")。
[Apollo_17_Cernan_on_moon.jpg](https://zh.wikipedia.org/wiki/File:Apollo_17_Cernan_on_moon.jpg "fig:Apollo_17_Cernan_on_moon.jpg")，為人类最近的一次登月活動。\]\]

二十世纪下半叶，[信息时代和](../Page/信息时代.md "wikilink")[全球化极大地促进了贸易和文化交流](../Page/全球化.md "wikilink")。[太空探测已经超出了](../Page/太空探测.md "wikilink")[太阳系](../Page/太阳系.md "wikilink")。携带[生命密码的](../Page/生命.md "wikilink")[脫氧核糖核酸被发现了](../Page/脫氧核糖核酸.md "wikilink")，[人类基因组也正在排序中](../Page/人类基因组.md "wikilink")，这有望最终改变人类对于[疾病的认识](../Page/疾病.md "wikilink")。现在一年中发表的科技论文比1900年以前所有发表的科技论文的总和还要多\[2\]，并且以十五年增加一倍的速度发展着。\[3\]全球的文化水平也在不断增长，生产[粮食所需的](../Page/粮食.md "wikilink")[劳动力在这个世纪里不断地下降](../Page/劳动力.md "wikilink")。

然而，这个时代还面临着许多可以使人类文明毁于一旦的风险，这些风险可能来源于一些无法控制的全球危机，諸如宗教衝突、[核扩散](../Page/核扩散.md "wikilink")、[温室效应](../Page/温室效应.md "wikilink")、其他由于[化石燃料所引起的](../Page/化石燃料.md "wikilink")[环境退化](../Page/环境问题.md "wikilink")、因争夺[资源而导致的国际](../Page/资源.md "wikilink")[冲突](../Page/冲突.md "wikilink")、快速传播的像[愛滋病之类的](../Page/愛滋病.md "wikilink")[传染病及近地](../Page/传染病.md "wikilink")[小行星和](../Page/小行星.md "wikilink")[彗星的撞击等](../Page/彗星.md "wikilink")。

[国家的发展总是从获得的希望与失去的恐惧中获得动力](../Page/国家.md "wikilink")。国民對自己國家的認同，总是在外来者威胁而產生的斗争裡所获得。在二十世纪終結的时候，世界见证了一个可视为新型的[超级大國](../Page/超级大國.md "wikilink")——欧盟的兴起。二十一世紀一零年代，世界又見證了另一個亞洲國家——[中國的崛起](../Page/中華人民共和國.md "wikilink")。类似于欧盟、[中國](../Page/中華人民共和國.md "wikilink")，[非洲和](../Page/非洲.md "wikilink")[南美的一些国家也采取了一些试探性的步骤](../Page/南美.md "wikilink")。然而，由不同的人，为了不同的目的所组织起来的国家，其产生、兴旺、崩溃还会继续地引发战争，并会伴随着死亡、残疾、疾病、饥饿以及种族屠杀，循環不斷。

### 全球化與西方化

[欧洲諸国在政治上统治着全球](../Page/欧洲.md "wikilink")，它们在[欧洲以外的世界上大部份地区都建立了殖民地](../Page/欧洲.md "wikilink")。[西方文化由于](../Page/西方文化.md "wikilink")[工业革命而开始了](../Page/工业革命.md "wikilink")[现代化进程](../Page/现代化.md "wikilink")，并在[十九世紀至](../Page/十九世紀.md "wikilink")[二十世纪间统治了世界](../Page/二十世纪.md "wikilink")，但同时它也被其它文明影响了。虽然总的趋势是[欧洲强势的统一](../Page/欧洲.md "wikilink")，但在世界的不同地区仍然存在着巨大的文化差异。

从[十五世纪到](../Page/十五世纪.md "wikilink")[十九世纪](../Page/十九世纪.md "wikilink")，[西班牙](../Page/西班牙.md "wikilink")、[葡萄牙](../Page/葡萄牙.md "wikilink")、[荷兰](../Page/荷兰.md "wikilink")、[法兰西和](../Page/法兰西.md "wikilink")[大不列颠等商业帝国统治着海洋](../Page/大不列颠.md "wikilink")。[十八世紀和](../Page/十八世紀.md "wikilink")[十九世纪的工业化以及社会和政治的变革使西方世界的思想家和政治家產生不少的优越感](../Page/十九世纪.md "wikilink")。[非洲和](../Page/非洲.md "wikilink")[亚洲的大部都是由欧洲控制的](../Page/亚洲.md "wikilink")，同时欧洲的后裔还统治着[澳洲和](../Page/澳洲.md "wikilink")[美洲](../Page/美洲.md "wikilink")。关于「重塑世界」的新意识形态也逐渐浮出水面。[社会达尔文主义者和](../Page/社会达尔文主义.md "wikilink")[帝国主义分子大都认为白种人更加优越](../Page/帝国主义.md "wikilink")，这使得他们相信应该靠着引进西方的生产方式、意识形态，諸如[基督教等使](../Page/基督教.md "wikilink")「野蛮人」变得文明起来。用这种方式，尽管不能指望「野蛮人」变得像白人一样，但至少可以使他们获得一种更加「美好」、「道德」的生活方式。[社会主义者和](../Page/社会主义.md "wikilink")[自由主义者也想藉此来使西方国家的工人阶级变得](../Page/自由主义.md "wikilink")「文明」起来。[社会主义者和](../Page/社会主义.md "wikilink")[自由主义者在很大程度上相信](../Page/自由主义.md "wikilink")（并且继续相信）社会在很大程度上要为处于其中的公民的行为负责，并且社会本身也应该朝着更好的方向变化。[美国](../Page/美国.md "wikilink")[保守派](../Page/保守派.md "wikilink")、[欧洲](../Page/欧洲.md "wikilink")[自由主义者以及所有的](../Page/自由主义.md "wikilink")[自由意志主义者都相信](../Page/自由意志.md "wikilink")（并且继续相信）自由和市场的力量。其认为每个人应该为自己负责，并且认为社会应该保证个人的自由以达到人的全面发展。而基督徒，不管具有何种政治意识形态，则都相信个人与教堂或神的联系对于一个完满的生活是一个关键性的因素。[穆斯林](../Page/穆斯林.md "wikilink")、[印度教教徒](../Page/印度教.md "wikilink")、[佛教徒和其他的一些](../Page/佛教.md "wikilink")[宗教则有着其本身的宗教概念](../Page/宗教.md "wikilink")。

[二十世纪经历了这些意识形态领域最剧烈的分化](../Page/二十世纪.md "wikilink")。在[纳粹德国战败后](../Page/納粹德國.md "wikilink")，社会达尔文主义趋于没落。[苏联与](../Page/苏联.md "wikilink")[美国促使了](../Page/美国.md "wikilink")[非殖民化运动](../Page/非殖民化.md "wikilink")。1960年代的[民权运动与](../Page/民權運動.md "wikilink")[嬉皮士反文化运动在](../Page/嬉皮士.md "wikilink")[人文主义意识形态中起了主导地位](../Page/人文主义.md "wikilink")，其影响，直至今天，仍在[西方世界中延续](../Page/西方世界.md "wikilink")。

[社会主义者试图以多种不同的方式改造社会](../Page/社会主义.md "wikilink")。最具代表性的是[社会民主主义和](../Page/社会民主主义.md "wikilink")[共产主义](../Page/共产主义.md "wikilink")。社会民主主义者尝试通过以多党合作的方法来建立社会主义社会。与此同时多数西方国家陆续建设为[福利国家](../Page/福利国家.md "wikilink")，这一做法也得到[左翼基督徒与](../Page/左翼.md "wikilink")[自由主义者思想上的认同](../Page/自由主义.md "wikilink")。但今天[福利国家理念的魅力不及往日](../Page/福利国家.md "wikilink")，因为其投资的低效率拖延了经济的发展。[共产主义者试图通过消灭旧社会以及相生的社会精英与意识形态来建立](../Page/共产主义.md "wikilink")[社会主义社会](../Page/社会主义.md "wikilink")，其结果是导致了大屠杀与物质上的贫穷，其思想也广被质疑。[苏维埃和](../Page/苏维埃.md "wikilink")[中国的领导人与知识分子们发现了西方](../Page/中国.md "wikilink")[市场经济的自我调节机制促使了经济的可持续发展与繁荣](../Page/市场经济.md "wikilink")，而对比之下其传统[计划经济机制却使社会经济陷于泥潭](../Page/计划经济.md "wikilink")，于是他们决心改革自己的经济制度，变得更加[资本主义化](../Page/资本主义.md "wikilink")。由于巨大的既得利益在统治[社会主义的国家领导人手中](../Page/社会主义.md "wikilink")，激烈的权利争斗和庞大的官僚体系，都将向[资本主义靠拢的](../Page/资本主义.md "wikilink")[计划经济带向严重的贪污腐败](../Page/计划经济.md "wikilink")，与贫富的过度悬殊；致使在政治的空间中所能赋予人民的权利与自由是极度的低靡与不堪。

非西方文明起初由西方的[殖民主义者所霸占](../Page/殖民主义.md "wikilink")，其对当地居民施以极其严苛的统治。[民族主义与](../Page/民族主义.md "wikilink")[共产主义运动风行了这些地区](../Page/共产主义.md "wikilink")，萌发了其民族意识和对平等权利的追求。许多[亚洲与](../Page/亚洲.md "wikilink")[非洲的殖民地在](../Page/非洲.md "wikilink")1960年代后取得了独立，但是其经济并没有取得预想中的发展，甚至在独立之后变得更糟。连绵不断的内战与[独裁统治破坏了国家的社会与经济](../Page/独裁.md "wikilink")。今天许多[拉丁美洲国家与亚洲国家开始迈入](../Page/拉丁美洲.md "wikilink")[第一世界行列](../Page/第一世界.md "wikilink")，但多数[非洲与](../Page/非洲.md "wikilink")[中东国家仍在停滞之中](../Page/中东.md "wikilink")。经济的起步迈向[现代化与](../Page/现代化.md "wikilink")[后现代化的脚步](../Page/后现代.md "wikilink")，可能加速[共产主义地域的人们对极端民族意识的偏激认同](../Page/共产主义.md "wikilink")，也可导致对人类[权利与](../Page/权利.md "wikilink")[自由的更加认识与深刻的渴慕](../Page/自由.md "wikilink")。

世界各国的[保守主义者与](../Page/保守主义.md "wikilink")[民主主义者担心社会会在](../Page/民主主义.md "wikilink")[现代化与新意识形态的影响下趋于瓦解](../Page/现代化.md "wikilink")，故试图扭转这种潮流。[保守主义在世界上许多国家与地区仍十分流行](../Page/保守主义.md "wikilink")。在美国政府中，[新保守主义近年有所抬头](../Page/新保守主义.md "wikilink")。[伊斯兰](../Page/伊斯兰.md "wikilink")[原教旨主义者企图对](../Page/原教旨主义.md "wikilink")[西方世界发动战争来阻止](../Page/西方世界.md "wikilink")[伊斯兰教的](../Page/伊斯兰教.md "wikilink")[世俗化](../Page/世俗化.md "wikilink")。许多[中东与](../Page/中东.md "wikilink")[非洲](../Page/非洲.md "wikilink")[撒哈拉以南的国家领导人与知识分子均指责西方](../Page/撒哈拉.md "wikilink")「不道德」的生活方式。这多半来自其宗教里关于来生的信仰以及由此引发的对其永生中「罪行」惩罚的恐惧。

嘗試以军事征服或革命来统一世界的方法并不成功。[民族国家成为了](../Page/民族国家.md "wikilink")[西方世界最重要的构成方式](../Page/西方世界.md "wikilink")。[十九世纪的殖民帝国多以民族国家组成](../Page/十九世纪.md "wikilink")，他们控制了大量的领土与土著人口。在[二十世纪](../Page/二十世纪.md "wikilink")，民族国家组成了联合体。[戰間期](../Page/戰間期.md "wikilink")，[国际联盟软弱地维持着各国的关系并试图防止战争](../Page/国际联盟.md "wikilink")。[第二次世界大战后](../Page/第二次世界大战.md "wikilink")，[联合国成立](../Page/联合国.md "wikilink")，冀望解决单个国家所无法解决的争端，但同样力不从心。[国际联盟和](../Page/国际联盟.md "wikilink")[联合国都是依赖于自己的个体成员国的自愿合作与贡献来工作的](../Page/联合国.md "wikilink")，当这些组织没有大国支持时，根本无法运作。这在1920年代与1930年代间和[冷战時期十分明显](../Page/冷战.md "wikilink")。世界上仍有许多地区并不是由[民族国家构成](../Page/民族国家.md "wikilink")，而由数个民族组成国家，这在[撒哈拉以南的非洲十分常见](../Page/撒哈拉.md "wikilink")。在[阿拉伯地区也仅有一小部分是](../Page/阿拉伯世界.md "wikilink")[民族国家](../Page/民族国家.md "wikilink")。

[自由市场经济的规模与数量自](../Page/自由市场.md "wikilink")[十九世纪以后戏剧性地增长](../Page/十九世纪.md "wikilink")。但直至1989年[苏联解体之前](../Page/苏联.md "wikilink")，国有[计划经济仍被认为是另一切实可行的方案](../Page/计划经济.md "wikilink")。[自由市场经济取得了空前的成功并带来了生活水平的巨大提高](../Page/自由市场.md "wikilink")。货物的自由贸易与信息的流通增进了国与国之间的合作与互相依赖程度，此进程被人们称为[全球化](../Page/全球化.md "wikilink")。

[人口爆炸成为了世界范围内的最大的难题之一](../Page/人口爆炸.md "wikilink")。很早以前已由思想家[马尔萨斯与](../Page/马尔萨斯.md "wikilink")[韦伯提出](../Page/马克斯·韦伯.md "wikilink")。后者担心[中国与](../Page/中国.md "wikilink")[印度将会以欧洲为代价来争夺资源发展自己的经济](../Page/印度.md "wikilink")，并呼吁[德意志](../Page/大德意志.md "wikilink")[帝国主义者采取措施防止大多数德国群众的贫困](../Page/帝国主义.md "wikilink")。[二十世纪的经济与技术发展证明了西方国家的经济增长可通过内部发展实现](../Page/二十世纪.md "wikilink")。如今[第三世界拥有的社会财富已可以和](../Page/第三世界.md "wikilink")[韦伯时代的](../Page/马克斯·韦伯.md "wikilink")[西方世界相比](../Page/西方世界.md "wikilink")。最近几十年，[中国](../Page/中国.md "wikilink")、[印度](../Page/印度.md "wikilink")、与[拉丁美洲的经济发展也同时为西方国家创造了许多就业職位](../Page/拉丁美洲.md "wikilink")。但增长的人口也对有限的资源索以更大的需求，并由此对环境造成了巨大的破坏。

而自[二十世紀開始](../Page/二十世紀.md "wikilink")，[美国文化亦对整个世界造成了巨大的冲击](../Page/美国文化.md "wikilink")。[好莱坞](../Page/好莱坞.md "wikilink")[电影和](../Page/电影.md "wikilink")[爵士乐](../Page/爵士乐.md "wikilink")、[搖滾樂自从](../Page/搖滾樂.md "wikilink")1920年代以来统治了整个[西方世界](../Page/西方世界.md "wikilink")。[青年文化亦源於美国](../Page/青年文化.md "wikilink")，[牛仔裤](../Page/牛仔裤.md "wikilink")、[T恤衫](../Page/T恤.md "wikilink")、美式风格的广告和[流行音乐在](../Page/流行音乐.md "wikilink")1960年代与1970年代后在世界广为传播。而傳統的[西方文化如](../Page/西方文化.md "wikilink")：[占星學](../Page/占星學.md "wikilink")、[塔羅牌更是讓亞洲的](../Page/塔羅牌.md "wikilink")[日本與](../Page/日本.md "wikilink")[台灣所廣泛接納](../Page/台灣.md "wikilink")。

## 參看條目

### 歷史主題

  - [特殊工藝](../Page/特殊工藝.md "wikilink")
  - [史學](../Page/史學.md "wikilink")

### 編年史

  - [世界史年表](../Page/世界史年表.md "wikilink")
  - [史前人類](../Page/史前人類.md "wikilink")
  - [古代史](../Page/古代史.md "wikilink")
  - [中世紀](../Page/中世紀.md "wikilink")
  - [現代史](../Page/現代史.md "wikilink")

### 地區史

  - [非洲歷史](../Page/非洲歷史.md "wikilink")
  - [美洲歷史](../Page/美洲歷史.md "wikilink")
      - [北美洲歷史](../Page/北美洲歷史.md "wikilink")
      - [南美洲歷史](../Page/南美洲歷史.md "wikilink")
  - [大洋洲歷史](../Page/大洋洲歷史.md "wikilink")
      - [澳洲歷史](../Page/澳洲歷史.md "wikilink")
  - [歐洲歷史](../Page/歐洲歷史.md "wikilink")
  - [歐亞歷史](../Page/歐亞歷史.md "wikilink")
  - [亞洲歷史](../Page/亞洲歷史.md "wikilink")
      - [西亞歐歷史](../Page/西亞歐歷史.md "wikilink")
      - [中東歷史](../Page/中東歷史.md "wikilink")
      - [中亞歷史](../Page/中亞歷史.md "wikilink")
      - [東亞歷史](../Page/东亚史.md "wikilink")
      - [北亞歷史](../Page/北亞歷史.md "wikilink")
      - [南亞歷史](../Page/南亞歷史.md "wikilink")
      - [東南亞歷史](../Page/東南亞歷史.md "wikilink")
      - [東北亞歷史](../Page/東北亞歷史.md "wikilink")

### 世界史總論

  - [世界交通史](../Page/世界交通史.md "wikilink")
  - [法律思想史](../Page/法律思想史.md "wikilink")

## 注釋

<div class="references-small">

1.

<references/>

</div>

## 參考書籍

  -
  -
  -
  -
  -
  -
## 外部連結

  -
  -
  -
## 參考資料

  -
  -
[Category:世界史](../Category/世界史.md "wikilink")

1.  [Wolfgang Keller and Carol
    Shiue](http://www.nber.org/cgi-bin/author_papers.pl?author=carol_shiue)
2.  [History of Science, Medicine and Technology.Bibliography of Primary
    Sources: Articles - Strategies for the Development of
    Databases](http://www.ifi.unicamp.br/~ghtc/sources/articles.htm)
3.  [Our affair with El Niño: how we transformed an enchanting Peruvian
    current into a global climate hazard, By S. George
    Philander](http://print.google.com/print?id=TbiVDVY6mRYC&pg=83&lpg=83&prev=http://print.google.com/print%3Fid%3DTbiVDVY6mRYC%26q%3D%2522number%2Bof%2Bscientific%2Bpapers%2Bpublished%2Beach%2Byear%2522&sig=jZLZNf0mWtAqSMUiXyVaBBlSJDA)