**世界水資源協會**（[英語](../Page/英語.md "wikilink")：**W**orld **W**ater
**C**ouncil，縮寫：**WWC**）是一個[非政府組織](../Page/非政府組織.md "wikilink")（NGO）、[政府](../Page/政府.md "wikilink")、和[國際組織共同組成的國際合作成果](../Page/國際組織.md "wikilink")，創立於1996年，目前總部設在[法國](../Page/法國.md "wikilink")[馬賽](../Page/馬賽.md "wikilink")。主要是因應世界上日漸受關注的[水資源問題而發起的](../Page/水資源.md "wikilink")。
該組織曾召開多次[世界水資源論壇](../Page/世界水資源論壇.md "wikilink")（World Water
Forum）以討論水資源相關議題。\[1\]

## 世界水資源論壇

第一屆[世界水資源論壇於](../Page/世界水資源論壇.md "wikilink")1997年在[摩洛哥召開](../Page/摩洛哥.md "wikilink")，目前已舉辦到第八屆。（2018年於[巴西](../Page/巴西.md "wikilink")）。第九屆論壇將於2021年在[孟加拉](../Page/孟加拉.md "wikilink")[達卡舉辦](../Page/達卡.md "wikilink")。\[2\]

| 屆次  | 舉辦地點      | 年份   |
| --- | --------- | ---- |
| 第九屆 | 孟加拉 達卡    | 2021 |
| 第八屆 | 巴西 巴西利亞   | 2018 |
| 第七屆 | 韓國 大邱慶尚北道 | 2015 |
| 第六屆 | 法國 馬賽     | 2012 |
| 第五屆 | 土耳其 伊斯坦堡  | 2009 |
| 第四屆 | 墨西哥       | 2006 |
| 第三屆 | 日本 京都     | 2003 |
| 第二屆 | 荷蘭 海牙     | 2000 |
| 第一屆 | 摩洛哥       | 1997 |

## 參考資料

## 外部連結

  - [世界水資源協會 官方網站](http://www.worldwatercouncil.org/)
  - [第四屆世界水資源論壇 (México 2006)](http://www.worldwaterforum4.org.mx/)
  - [第五屆世界水資源論壇（伊斯坦堡 2009）](https://web.archive.org/web/20080509091433/http://www.worldwaterforum5.org/)

[category:法国智库](../Page/category:法国智库.md "wikilink")

[Category:水](../Category/水.md "wikilink")
[Category:环境组织](../Category/环境组织.md "wikilink")
[Category:国际专业性组织](../Category/国际专业性组织.md "wikilink")
[Category:1996年建立的組織](../Category/1996年建立的組織.md "wikilink")
[Category:非政府组织](../Category/非政府组织.md "wikilink")

1.
2.