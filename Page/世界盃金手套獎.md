**世界盃金手套獎**（英语：Golden Glove Award in FIFA World
Cup），原名“雅辛奖”，是世界杯足球赛的单项奖之一，专门表彰当届最佳守门员。从2010年世界杯起，该奖项改为“世界杯金手套奖”。

## 简述

**世界盃耶辛獎**（Yashin
Award），為紀念[列夫·耶辛其畢生功績](../Page/列夫·耶辛.md "wikilink")，國際足聯設立「耶辛獎」，自1994年授予每屆世界盃上表現最優秀的[守門員](../Page/守門員.md "wikilink")。

至目前為止已頒發過七屆，得獎者都是歐洲球員，其中[法國的](../Page/法國國家足球隊.md "wikilink")[-{zh-hans:法比安·巴特茲;zh-hk:巴夫斯;zh-tw:巴特茲;}-](../Page/法比安·巴特茲.md "wikilink")、[德國的](../Page/德國國家足球隊.md "wikilink")[-{zh-hans:奥利弗·卡恩;zh-hk:簡尼;zh-tw:卡恩;}-](../Page/奧利弗·卡恩.md "wikilink")、[-{zh-hans:诺伊尔;zh-hk:紐亞;zh-tw:諾伊爾;}-](../Page/曼努埃尔·诺伊尔.md "wikilink")、[義大利的](../Page/義大利國家足球隊.md "wikilink")[-{zh-hans:詹路易吉·布冯;zh-hk:保方;zh-tw:布馮;}-和](../Page/詹路易吉·布馮.md "wikilink")[西班牙的](../Page/西班牙國家足球隊.md "wikilink")[-{zh-hans:伊克尔·卡西利亚斯;zh-hk:卡斯拿斯;zh-tw:卡西拉斯;}-都是打入世界盃決賽的球員](../Page/伊克尔·卡西利亚斯.md "wikilink")。

## 歷屆獲獎得主列表

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>獲得者</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1994年世界盃足球賽.md" title="wikilink">1994年 美國</a></p></td>
<td><p><a href="../Page/米歇尔·普吕多姆.md" title="wikilink">-{zh-hans:普吕多姆;zh-hk:普荷美;}-</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1998年世界盃足球賽.md" title="wikilink">1998年 法國</a></p></td>
<td><p><a href="../Page/法比安·巴特茲.md" title="wikilink">-{zh-hans:法比安·巴特兹;zh-hk:巴夫斯;zh-tw:巴特茲;}-</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2002年世界盃足球賽.md" title="wikilink">2002年 韓國/日本</a></p></td>
<td><p><a href="../Page/奧利弗·卡恩.md" title="wikilink">-{zh-hans:奥利弗·卡恩;zh-hk:簡尼;zh-tw:卡恩;}-</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2006年世界盃足球賽.md" title="wikilink">2006年 德國</a></p></td>
<td><p><a href="../Page/詹路易吉·布馮.md" title="wikilink">-{zh-hans:詹路易吉·布冯;zh-hk:保方;zh-tw:布馮;}-</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2010年世界盃足球賽.md" title="wikilink">2010年 南非</a></p></td>
<td><p><a href="../Page/卡西拉斯.md" title="wikilink">-{zh-hans:伊克尔·卡西利亚斯;zh-hk:卡斯拿斯;zh-tw:卡西拉斯;}-</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2014年世界盃足球賽.md" title="wikilink">2014年 巴西</a></p></td>
<td><p><a href="../Page/曼努埃尔·诺伊尔.md" title="wikilink">-{zh-hans:诺伊尔;zh-hk:紐亞;zh-tw:諾伊爾;}-</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2018年世界盃足球賽.md" title="wikilink">2018年 俄羅斯</a></p></td>
<td><p><a href="../Page/泰拔·高圖爾斯.md" title="wikilink">-{zh-hans:库尔图瓦;zh-hk:高圖爾斯;zh-tw:庫爾圖瓦;}-</a></p></td>
</tr>
</tbody>
</table>

## 參見

  - [世界盃金靴獎](../Page/世界盃金靴獎.md "wikilink")

  - [世界盃金球獎](../Page/世界盃金球獎.md "wikilink")

  - [世界足球先生](../Page/世界足球先生.md "wikilink")

  - [歐洲足球先生](../Page/歐洲足球先生.md "wikilink")

  - [世界盃足球賽](../Page/世界盃足球賽.md "wikilink")

  -
[Category:國際足協世界盃](../Category/國際足協世界盃.md "wikilink")
[Category:足球獎項](../Category/足球獎項.md "wikilink")
[Category:足球守门员](../Category/足球守门员.md "wikilink")
[Category:1994年建立的獎項](../Category/1994年建立的獎項.md "wikilink")