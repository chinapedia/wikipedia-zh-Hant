**世界運動會**（簡稱**世運會**或**世運**）是一個國際性的[體育競賽盛會](../Page/體育.md "wikilink")，1981年於[美國](../Page/美國.md "wikilink")[加州](../Page/加州.md "wikilink")[聖克拉拉首次舉辦](../Page/圣克拉拉_\(加利福尼亚州\).md "wikilink")，之後每四年舉行一次，競賽項目以非[奧運會項目為主](../Page/奧運會項目.md "wikilink")，由[國際世界運動總會](../Page/國際世界運動總會.md "wikilink")（IWGA）舉辦，於每屆奧運一年後舉行。與奧運會分夏季和冬季不同，世界運動會沒有冬季項目。

世運會的項目大概介於25至35種之間，數量由各主辦城市的場地設施而定，很少會為了某項目而新建場館設施。被列為「邀請賽」的項目仍會舉辦，但並不會頒發獎牌，就算某些比賽是正式項目，主辦城市仍可將其列為邀請賽。一些世運會的競賽項目已成為奧運的正式比賽項目（如[鐵人三項](../Page/鐵人三項.md "wikilink")），或曾是過去奧運會的項目（如[拔河](../Page/拔河.md "wikilink")）。2004年8月12日，奧運會決議將世運會比賽情形納入挑選奧運新項目的條件，然而，奧運會近年的參賽選手已逼近10,500人，因此世運會的項目很有可能不會再被納入奧運。

2000年與[國際奧委會簽訂備忘錄](../Page/國際奧委會.md "wikilink")，國際世界運動會協會IWGA受國際奧委會IOC所授權及資助，為共同推廣世界運動而努力，共享資源與宣傳。

## 舉辦城市

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>舉辦地點</p></th>
<th><p>舉辦城市</p></th>
<th><p>世界运动会宣布开幕者</p></th>
<th><p>日期</p></th>
<th><p>参赛隊伍</p></th>
<th><p>参赛人数</p></th>
<th><p>大项</p></th>
<th><p>邀请项目</p></th>
<th><p>主体育场</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a></p></td>
<td><p><a href="../Page/圣克拉拉_(加利福尼亚州).md" title="wikilink">聖克拉拉</a></p></td>
<td><p><a href="../Page/國際單項運動總會聯合會.md" title="wikilink">國際單項運動總會聯合會</a><a href="../Page/主席.md" title="wikilink">主席</a></p></td>
<td><p>1981年7月24日至8月2日</p></td>
<td><p>58</p></td>
<td><p>1745</p></td>
<td><p>15</p></td>
<td><p>1</p></td>
<td><p><a href="../Page/巴克肖球场.md" title="wikilink">巴克肖球场</a></p></td>
<td><p>[1]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/英國.md" title="wikilink">英國</a></p></td>
<td><p><a href="../Page/倫敦.md" title="wikilink">倫敦</a></p></td>
<td></td>
<td><p>1985年7月25日至8月4日</p></td>
<td><p>57</p></td>
<td><p>1227</p></td>
<td><p>20</p></td>
<td><p>1</p></td>
<td><p><a href="../Page/巴尼特科普索尔体育场.md" title="wikilink">巴尼特科普索尔体育场</a></p></td>
<td><p>[2]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/西德.md" title="wikilink">西德</a></p></td>
<td><p><a href="../Page/卡尔斯鲁厄.md" title="wikilink">卡尔斯鲁厄</a></p></td>
<td><p><a href="../Page/萨马兰奇.md" title="wikilink">萨马兰奇</a><a href="../Page/国际奥委会#历任主席.md" title="wikilink">国际奥委会主席</a></p></td>
<td><p>1989年7月20日至30日</p></td>
<td><p>50</p></td>
<td><p>1206</p></td>
<td><p>17</p></td>
<td><p>2</p></td>
<td><p><a href="../Page/硕奈克体育学校.md" title="wikilink">硕奈克体育学校</a></p></td>
<td><p>[3]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/荷蘭.md" title="wikilink">荷蘭</a></p></td>
<td><p><a href="../Page/海牙.md" title="wikilink">海牙</a></p></td>
<td><p><a href="../Page/国际奥委会.md" title="wikilink">国际奥委会</a><a href="../Page/副主席.md" title="wikilink">副主席</a></p></td>
<td><p>1993年7月22日至29日</p></td>
<td><p>72</p></td>
<td><p>2264</p></td>
<td><p>22</p></td>
<td><p>4</p></td>
<td><p><a href="../Page/海牙的运动场.md" title="wikilink">海牙的运动场</a></p></td>
<td><p>[4]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1997年世界運動會.md" title="wikilink">1997年</a></p></td>
<td><p><a href="../Page/芬蘭.md" title="wikilink">芬蘭</a></p></td>
<td><p><a href="../Page/拉赫蒂.md" title="wikilink">拉蒂</a></p></td>
<td><p><a href="../Page/萨马兰奇.md" title="wikilink">萨马兰奇</a><a href="../Page/国际奥委会#历任主席.md" title="wikilink">国际奥委会主席</a></p></td>
<td><p>1997年8月7日至17日</p></td>
<td><p>73</p></td>
<td><p>1379</p></td>
<td><p>22</p></td>
<td><p>6</p></td>
<td><p><a href="../Page/芬兰体育学院.md" title="wikilink">芬兰体育学院</a></p></td>
<td><p>[5]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2001年世界運動會.md" title="wikilink">2001年</a></p></td>
<td><p><a href="../Page/日本.md" title="wikilink">日本</a></p></td>
<td><p><a href="../Page/秋田.md" title="wikilink">秋田</a></p></td>
<td><p><a href="../Page/文部科学大臣.md" title="wikilink">文部科学大臣</a></p></td>
<td><p>2001年8月16日至26日</p></td>
<td><p>93</p></td>
<td><p>1968</p></td>
<td><p>22</p></td>
<td><p>5</p></td>
<td><p><a href="../Page/八桥田径体育场.md" title="wikilink">八桥田径体育场</a></p></td>
<td><p>[6]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2005年世界運動會.md" title="wikilink">2005年</a></p></td>
<td><p><a href="../Page/德國.md" title="wikilink">德國</a></p></td>
<td><p><a href="../Page/杜伊斯堡.md" title="wikilink">杜伊斯堡</a></p></td>
<td><p><a href="../Page/奥托·席利.md" title="wikilink">席利</a><a href="../Page/德国联邦内政、建设和家园部.md" title="wikilink">聯邦內政部長</a></p></td>
<td><p>2005年7月14日至24日</p></td>
<td><p>89</p></td>
<td><p>2464</p></td>
<td><p>27</p></td>
<td><p>6</p></td>
<td><p><a href="../Page/MSV体育场.md" title="wikilink">MSV体育场</a></p></td>
<td><p>[7]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2009年世界運動會.md" title="wikilink">2009年</a></p></td>
<td><p><a href="../Page/中华台北.md" title="wikilink">中华台北</a></p></td>
<td><p><a href="../Page/高雄.md" title="wikilink">高雄</a></p></td>
<td><p><a href="../Page/馬英九.md" title="wikilink">馬英九</a><a href="../Page/中華民國總統.md" title="wikilink">總統</a>[8]</p></td>
<td><p>2009年7月16日至7月26日</p></td>
<td><p>84</p></td>
<td><p>2536</p></td>
<td><p>26</p></td>
<td><p>5</p></td>
<td><p><a href="../Page/國家體育場_(高雄市).md" title="wikilink">龙腾体育场</a></p></td>
<td><p>[9]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2013年世界運動會.md" title="wikilink">2013年</a></p></td>
<td><p><a href="../Page/哥倫比亞.md" title="wikilink">哥倫比亞</a></p></td>
<td><p><a href="../Page/卡利.md" title="wikilink">卡利</a>[10]</p></td>
<td></td>
<td><p>2013年7月25日至8月4日</p></td>
<td><p>103</p></td>
<td><p>2982</p></td>
<td><p>26</p></td>
<td><p>5</p></td>
<td><p><a href="../Page/帕斯夸尔・格雷罗体育场.md" title="wikilink">帕斯夸尔・格雷罗体育场</a></p></td>
<td><p>[11]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2017年世界運動會.md" title="wikilink">2017年</a></p></td>
<td><p><a href="../Page/波蘭.md" title="wikilink">波蘭</a></p></td>
<td><p><a href="../Page/弗羅茨瓦夫.md" title="wikilink">弗羅茨瓦夫</a></p></td>
<td><p><a href="../Page/托馬斯·巴赫.md" title="wikilink">巴赫</a><a href="../Page/国际奥委会#历任主席.md" title="wikilink">国际奥委会主席</a></p></td>
<td><p>2017年7月20日至30日</p></td>
<td><p>104</p></td>
<td><p>4500</p></td>
<td><p>26</p></td>
<td><p>4</p></td>
<td><p><a href="../Page/弗罗茨瓦夫市立球场.md" title="wikilink">弗罗茨瓦夫市立球场</a></p></td>
<td><p>[12]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2021年世界運動會.md" title="wikilink">2021年</a></p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a></p></td>
<td><p><a href="../Page/伯明罕_(阿拉巴馬州).md" title="wikilink">伯明罕</a></p></td>
<td></td>
<td><p>2021年7月15日至25日</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/军团球场.md" title="wikilink">军团球场</a></p></td>
<td><p>[13]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2025年世界运动会.md" title="wikilink">2025年</a></p></td>
<td><p>待定</p></td>
<td><p>待定</p></td>
<td></td>
<td><p>2015年</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>[14]</p></td>
</tr>
</tbody>
</table>

<small><sup>1</sup>
[中華民國](../Page/中華民國.md "wikilink")（[臺灣](../Page/臺灣.md "wikilink")）依[國際奧會的協定使用](../Page/國際奧會.md "wikilink")

的名稱參加國際運動賽事，而國際組織，如[國際世界運動會協會也依此協定為慣例做為稱呼](../Page/國際世界運動會協會.md "wikilink")，至今[兩岸對於主權仍有爭議](../Page/兩岸.md "wikilink")，請參見[海峽兩岸關係](../Page/海峽兩岸關係.md "wikilink")。</small>

## 比賽項目

世界運動會過去一直都以非奧運体育項目為主，包括[沙灘排球](../Page/沙灘排球.md "wikilink")、女子[舉重](../Page/舉重.md "wikilink")、[跆拳道等項目在未獲納入奧運之前](../Page/跆拳道.md "wikilink")，都一直为世界運動會的比赛项目。世界運動會現時舉辦的运动項目包括：[蹼泳](../Page/蹼泳.md "wikilink")、[壁球](../Page/壁球.md "wikilink")、[野外定向](../Page/野外定向.md "wikilink")、[劍道](../Page/劍道.md "wikilink")、[箭術](../Page/箭術.md "wikilink")、[撞球](../Page/撞球.md "wikilink")、[健美](../Page/健美.md "wikilink")、[保齡球](../Page/保齡球.md "wikilink")、[曲棍球](../Page/曲棍球.md "wikilink")、[合球等](../Page/合球.md "wikilink")。[2012年奧運會剔除了](../Page/2012年奧運會.md "wikilink")[壘球項目](../Page/壘球.md "wikilink")，故2009年的世運會將壘球列入邀請賽項目；但另一個被剔除的項目[棒球則未列入](../Page/棒球.md "wikilink")。

## 注釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [IWGA官方網站](http://www.theworldgames.org/)

[世界運動會](../Category/世界運動會.md "wikilink")
[Category:運動會](../Category/運動會.md "wikilink")
[Category:1981年建立的週期性體育事件](../Category/1981年建立的週期性體育事件.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13. <https://www.theworldgames.org/editions/Birmingham-USA-2021-13/summary>
14. <https://www.theworldgames.org/editions/Your-City-Your-Country-2025-14>