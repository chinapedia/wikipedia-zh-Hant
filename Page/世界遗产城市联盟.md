**世界遗产城市联盟**（或译**世界遗产城市组织**，[法文](../Page/法文.md "wikilink")：，简称；[英文](../Page/英文.md "wikilink")：，简称：****；[西班牙文](../Page/西班牙文.md "wikilink")：，简称），是[联合国教科文组织的一个下属组织机构](../Page/联合国教科文组织.md "wikilink")，是一个非盈利性、非政府的[国际组织](../Page/国际组织.md "wikilink")，于1993年9月8日在[摩洛哥的](../Page/摩洛哥.md "wikilink")[非斯成立](../Page/非斯.md "wikilink")，总部设在[加拿大的](../Page/加拿大.md "wikilink")[魁北克市](../Page/魁北克市.md "wikilink")。该组织的宗旨是负责沟通和执行[世界遗产委员会会议的各项公约和决议](../Page/世界遗产委员会会议.md "wikilink")，借鉴各遗产城市在文化遗产保护和管理方面的先进经验，进一步促进各遗产城市的保护工作。

至今，该组织已经有242座被列入联合国教科文组织[世界遗产名录的城市作为成员](../Page/世界遗产名录.md "wikilink")\[1\]，其中，7座在[撒哈拉以南非洲](../Page/撒哈拉以南非洲.md "wikilink")，36座在[拉丁美洲和](../Page/拉丁美洲.md "wikilink")[加勒比地区](../Page/加勒比地区.md "wikilink")，30个在[亚洲](../Page/亚洲.md "wikilink")（除[阿拉伯国家和](../Page/阿拉伯国家.md "wikilink")[土耳其](../Page/土耳其.md "wikilink")）及[太平洋地区](../Page/太平洋.md "wikilink")，124座在[欧洲](../Page/欧洲.md "wikilink")（包括土耳其）和[北美地区](../Page/北美地区.md "wikilink")，21座在[阿拉伯国家](../Page/阿拉伯国家.md "wikilink")，其中还有7座城市为其观察员。

## 世界遗产城市

“世界遗产城市”是指[城市类型的](../Page/城市.md "wikilink")[世界遗产](../Page/世界遗产.md "wikilink")，其性质类似于[中国的](../Page/中国.md "wikilink")“[国家历史文化名城](../Page/国家历史文化名城.md "wikilink")”，可以说是[世界级的历史文化名城](../Page/世界.md "wikilink")。[1](https://web.archive.org/web/20090623043919/http://www.js.xinhuanet.com/xin_wen_zhong_xin/2006-06/10/content_7226877.htm)

## 世界遗产城市列表

### [亚洲和太平洋地区](../Page/亚太地区.md "wikilink")（共32座）

#### [中国](../Page/中华人民共和国.md "wikilink")（5座）

[Humble_admin_garden_suzhou_small.jpg](https://zh.wikipedia.org/wiki/File:Humble_admin_garden_suzhou_small.jpg "fig:Humble_admin_garden_suzhou_small.jpg")[拙政园荷花池](../Page/拙政园.md "wikilink")\]\]

  - 中国[苏州](../Page/苏州.md "wikilink")：[苏州古典园林](../Page/苏州古典园林.md "wikilink")（共9座，包括[拙政园](../Page/拙政园.md "wikilink")、[留园](../Page/留园.md "wikilink")、[网师园](../Page/网师园.md "wikilink")、[环秀山庄](../Page/环秀山庄.md "wikilink")、[沧浪亭](../Page/沧浪亭.md "wikilink")、[狮子林](../Page/狮子林.md "wikilink")、[耦园](../Page/耦园.md "wikilink")、[艺圃和](../Page/艺圃.md "wikilink")[退思园](../Page/退思园.md "wikilink")）、[大运河苏州段](../Page/大运河.md "wikilink")。
  - 中国[承德](../Page/承德.md "wikilink")：[承德避暑山庄和外八庙](../Page/承德避暑山庄和外八庙.md "wikilink")。
  - 中国[丽江](../Page/丽江.md "wikilink")：[丽江古城](../Page/丽江古城.md "wikilink")。
  - 中国[澳门](../Page/澳门.md "wikilink")：[澳门历史城区](../Page/澳门历史城区.md "wikilink")。
  - 中国[平遥](../Page/平遥.md "wikilink")：[平遥古城](../Page/平遥古城.md "wikilink")。

#### [以色列和](../Page/以色列.md "wikilink")[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")／[巴勒斯坦国](../Page/巴勒斯坦国.md "wikilink")（3座，包括有争议的耶路撒冷）

  - 以色列[阿卡](../Page/阿卡_\(以色列\).md "wikilink")：阿卡老城。
  - [耶路撒冷](../Page/耶路撒冷.md "wikilink")：[耶路撒冷古城及其城墙](../Page/耶路撒冷古城.md "wikilink")。
  - 以色列[特拉维夫](../Page/特拉维夫.md "wikilink")：[特拉维夫白城](../Page/特拉维夫白城.md "wikilink")。

#### [尼泊尔](../Page/尼泊尔.md "wikilink")（3座）

  - 尼泊尔[巴克塔普尔](../Page/巴克塔普尔.md "wikilink")：[加德满都谷地](../Page/加德满都谷地.md "wikilink")。
  - 尼泊尔[加德满都](../Page/加德满都.md "wikilink")：加德满都谷地。
  - 尼泊尔[勒利德布尔](../Page/勒利德布尔.md "wikilink")（[帕坦](../Page/帕坦.md "wikilink")）：加德满都谷地。

#### [乌兹别克斯坦](../Page/乌兹别克斯坦.md "wikilink")（3座）

  - 乌兹别克斯坦[希瓦](../Page/希瓦.md "wikilink")：[伊钱卡拉](../Page/伊钱卡拉.md "wikilink")。

[Registan_square2014.JPG](https://zh.wikipedia.org/wiki/File:Registan_square2014.JPG "fig:Registan_square2014.JPG")[清真寺](../Page/清真寺.md "wikilink")\]\]

  - 乌兹别克斯坦[撒马尔罕](../Page/撒马尔罕.md "wikilink")：撒马尔罕—文化交汇之地。
  - 乌兹别克斯坦[沙赫里萨布兹](../Page/沙赫里萨布兹.md "wikilink")：沙赫里萨布兹历史中心。

#### [越南](../Page/越南.md "wikilink")（2座）

  - 越南[会安](../Page/会安.md "wikilink")：会安古城。
  - 越南[顺化](../Page/顺化市.md "wikilink")：顺化历史建筑群。

#### [日本](../Page/日本.md "wikilink")（5座）

  - 日本：[白川乡与五箇山的合掌造村落](../Page/白川乡与五箇山的合掌造村落.md "wikilink")。

[Kinkaku-ji.jpg](https://zh.wikipedia.org/wiki/File:Kinkaku-ji.jpg "fig:Kinkaku-ji.jpg")[金阁寺](../Page/金阁寺.md "wikilink")\]\]

  - 日本[京都](../Page/京都.md "wikilink")：[古都京都的文化財](../Page/古都京都的文化財.md "wikilink")。
  - 日本[奈良](../Page/奈良.md "wikilink")：[古都奈良的文化財](../Page/古都奈良的文化財.md "wikilink")。
  - 日本[白川乡和](../Page/白川乡.md "wikilink")[五箇山](../Page/五箇山.md "wikilink")：白川乡与五箇山的合掌造村落。
  - 日本：白川乡与五箇山的合掌造村落。

#### [斯里兰卡](../Page/斯里兰卡.md "wikilink")（2座）

  - 斯里兰卡[康提](../Page/康提.md "wikilink")：康提圣城。
  - 斯里兰卡[加勒](../Page/加勒.md "wikilink")：加勒古城及其城堡。

#### [马来西亚](../Page/马来西亚.md "wikilink")（2座）

  - 马来西亚[马六甲](../Page/马六甲.md "wikilink")：州首府[马六甲市](../Page/马六甲市.md "wikilink")
  - 马来西亚[槟城](../Page/槟城.md "wikilink")：州首府[乔治市](../Page/乔治市.md "wikilink")

#### 其他（7座）

  - [韩国](../Page/韩国.md "wikilink")[安东](../Page/安东市.md "wikilink")

[Baku_Maiden_Tower.jpg](https://zh.wikipedia.org/wiki/File:Baku_Maiden_Tower.jpg "fig:Baku_Maiden_Tower.jpg")[少女塔](../Page/少女塔.md "wikilink")\]\]

  - [阿塞拜疆](../Page/阿塞拜疆.md "wikilink")[巴库](../Page/巴库.md "wikilink")：巴库古城及城内的[希尔凡王宫和](../Page/希尔凡王宫.md "wikilink")[少女塔](../Page/少女塔.md "wikilink")。
  - [伊朗](../Page/伊朗.md "wikilink")[巴姆](../Page/巴姆.md "wikilink")：巴姆古城和文化景观。
  - [老挝](../Page/老挝.md "wikilink")[琅勃拉邦](../Page/琅勃拉邦.md "wikilink")：琅勃拉邦城。
  - [菲律宾](../Page/菲律宾.md "wikilink")[维干](../Page/维干.md "wikilink")：维干古城。
  - [亚美尼亚](../Page/亚美尼亚.md "wikilink")[埃里温](../Page/埃里温.md "wikilink")（[耶烈万](../Page/耶烈万.md "wikilink")）：[埃奇米阿津教堂群和](../Page/埃奇米阿津.md "wikilink")[兹瓦尔特诺茨考古遗蹟](../Page/兹瓦尔特诺茨.md "wikilink")。
  - [印度尼西亚苏腊卡尔塔](../Page/印度尼西亚.md "wikilink")（[梭罗](../Page/梭罗市.md "wikilink")）

### [阿拉伯国家](../Page/阿拉伯国家.md "wikilink")（共21座，不包括有争议的耶路撒冷）

#### [叙利亚](../Page/叙利亚.md "wikilink")（3座）

  - 叙利亚[阿勒颇](../Page/阿勒颇.md "wikilink")：阿勒颇古城。
  - 叙利亚[布斯拉](../Page/布斯拉.md "wikilink")：布斯拉古城。

<!-- end list -->

  - 叙利亚[大马士革](../Page/大马士革.md "wikilink")：大马士革古城。

#### [阿尔及利亚](../Page/阿尔及利亚.md "wikilink")（2座）

  - 阿尔及利亚[阿尔及尔](../Page/阿尔及尔.md "wikilink")：阿尔及尔城堡。
  - 阿尔及利亚[盖尔达耶](../Page/盖尔达耶.md "wikilink")：[姆扎卜山谷](../Page/姆扎卜.md "wikilink")。

#### [摩洛哥](../Page/摩洛哥.md "wikilink")（6座）

  - 摩洛哥[索维拉](../Page/索维拉.md "wikilink")：索维拉老城区。

[Fes,_Old_Medina.jpg](https://zh.wikipedia.org/wiki/File:Fes,_Old_Medina.jpg "fig:Fes,_Old_Medina.jpg")\]\]

  - 摩洛哥[非斯](../Page/非斯.md "wikilink")：[非斯老城区](../Page/非斯老城区.md "wikilink")。
  - 摩洛哥[马拉喀什](../Page/马拉喀什.md "wikilink")：马拉喀什老城区。
  - 摩洛哥[马扎甘](../Page/马扎甘.md "wikilink")：马扎甘的葡萄牙城（[杰迪代](../Page/杰迪代.md "wikilink")）。
  - 摩洛哥[梅克内斯](../Page/梅克内斯.md "wikilink")：梅克内斯古城。
  - 摩洛哥[得土安](../Page/得土安.md "wikilink")：得土安老城区。

#### [突尼斯](../Page/突尼斯.md "wikilink")（4座，观察员城市1座）

  - 突尼斯[莫纳斯提尔](../Page/莫纳斯提尔.md "wikilink")
  - 突尼斯[凯鲁万](../Page/凯鲁万.md "wikilink")：凯鲁万。
  - 突尼斯[苏塞](../Page/苏塞.md "wikilink")：苏塞老城区。
  - 突尼斯[突尼斯城](../Page/突尼斯城.md "wikilink")：突尼斯老城区。

#### [也门](../Page/也门.md "wikilink")（3座）

[Sanaa.JPG](https://zh.wikipedia.org/wiki/File:Sanaa.JPG "fig:Sanaa.JPG")古城\]\]

  - 也门[萨那](../Page/萨那.md "wikilink")：萨那古城。
  - 也门[希巴姆](../Page/希巴姆.md "wikilink")：希巴姆古城。
  - 也门[扎比德](../Page/扎比德.md "wikilink")：扎比德古城。

#### [利比亚](../Page/利比亚.md "wikilink")（2座）

  - [利比亚](../Page/利比亚.md "wikilink")[古达米斯](../Page/古达米斯.md "wikilink")：古达米斯古城。
  - [利比亚](../Page/利比亚.md "wikilink")[的黎波里](../Page/的黎波里.md "wikilink")：

#### 其他（2座）

  - [埃及](../Page/埃及.md "wikilink")[开罗](../Page/开罗.md "wikilink")：[开罗伊斯兰老城](../Page/开罗伊斯兰老城.md "wikilink")、[孟斐斯及其墓地](../Page/孟斐斯_\(埃及\).md "wikilink")—从[吉萨到](../Page/吉萨.md "wikilink")[代赫舒尔的](../Page/代赫舒尔.md "wikilink")[金字塔区](../Page/金字塔.md "wikilink")。
  - [毛里塔尼亚](../Page/毛里塔尼亚.md "wikilink")[欣盖提](../Page/欣盖提.md "wikilink")：欣盖提古城。

### [非洲](../Page/撒哈拉以南非洲.md "wikilink")（共9座，其中观察员城市1座）

#### [塞内加尔](../Page/塞内加尔.md "wikilink")（2座）

  - 塞内加尔[达喀尔](../Page/达喀尔.md "wikilink")
  - 塞内加尔[圣路易岛](../Page/圣路易岛.md "wikilink")：圣路易岛。

#### [马里](../Page/马里.md "wikilink")（2座）

  - 马里[迪耶纳](../Page/迪耶纳.md "wikilink")（[杰内](../Page/杰内.md "wikilink")）：迪耶纳（杰内）古城。

[Djingareiber_cour.jpg](https://zh.wikipedia.org/wiki/File:Djingareiber_cour.jpg "fig:Djingareiber_cour.jpg")琼盖拉柏清真寺\]\]

  - 马里[廷巴克图](../Page/廷巴克图.md "wikilink")：廷巴克图。

#### 其他（5座）

  - [莫桑比克](../Page/莫桑比克.md "wikilink")[莫桑比克岛](../Page/莫桑比克岛.md "wikilink")：莫桑比克岛。
  - [肯尼亚](../Page/肯尼亚.md "wikilink")[拉穆](../Page/拉穆.md "wikilink")：拉穆古城。
  - [坦桑尼亚](../Page/坦桑尼亚.md "wikilink")[桑给巴尔](../Page/桑给巴尔.md "wikilink")：桑给巴尔石头城。
  - [埃塞俄比亚Harar](../Page/埃塞俄比亚.md "wikilink") Jugol
  - [刚果民主共和国Kashusha](../Page/刚果民主共和国.md "wikilink")（观察员）

### [欧洲和](../Page/欧洲.md "wikilink")[北美地区](../Page/北美地区.md "wikilink")（共124座，其中观察员城市1座）

#### [西班牙](../Page/西班牙.md "wikilink")（17座）

  - 西班牙[阿尔卡拉·德·埃纳雷斯](../Page/阿尔卡拉·德·埃纳雷斯.md "wikilink")（[埃纳雷斯堡](../Page/埃纳雷斯堡.md "wikilink")）：埃纳雷斯堡的大学和历史区。
  - 西班牙[阿兰胡埃斯](../Page/阿兰胡埃斯.md "wikilink")：阿兰胡埃斯文化景观。
  - 西班牙[阿维拉](../Page/阿维拉.md "wikilink")：阿维拉古城及城外教堂。
  - 西班牙[巴埃萨](../Page/巴埃萨.md "wikilink")：巴埃萨的文艺复兴建筑群。
  - 西班牙[卡塞雷斯](../Page/卡塞雷斯.md "wikilink")：卡塞雷斯古城。
  - 西班牙[科尔多瓦](../Page/科尔多瓦_\(西班牙\).md "wikilink")：科尔多瓦历史中心。
  - 西班牙[昆卡](../Page/昆卡_\(西班牙\).md "wikilink")：昆卡古城。

[Spain_Andalusia_Granada_BW_2015-10-25_17-22-07.jpg](https://zh.wikipedia.org/wiki/File:Spain_Andalusia_Granada_BW_2015-10-25_17-22-07.jpg "fig:Spain_Andalusia_Granada_BW_2015-10-25_17-22-07.jpg")[阿尔罕布拉宫](../Page/阿尔罕布拉宫.md "wikilink")\]\]

  - 西班牙[格拉纳达](../Page/格拉纳达.md "wikilink")：格拉纳达的[阿尔罕布拉宫](../Page/阿尔罕布拉宫.md "wikilink")、[赫内拉利费和](../Page/赫内拉利费.md "wikilink")[阿尔贝辛](../Page/阿尔贝辛.md "wikilink")。
  - 西班牙[伊维萨岛](../Page/伊维萨岛.md "wikilink")：伊维萨岛的生物多样性和文化。
  - 西班牙[梅里达](../Page/梅里达_\(西班牙\).md "wikilink")：梅里达的考古遗址群。
  - 西班牙[奥维耶多](../Page/奥维耶多.md "wikilink")：奥维耶多和[阿斯图里亚斯王国建筑群](../Page/阿斯图里亚斯王国建筑群.md "wikilink")。
  - 西班牙[萨拉曼卡](../Page/萨拉曼卡.md "wikilink")：萨拉曼卡古城。
  - 西班牙[拉古纳的圣克里斯托瓦尔](../Page/拉古纳的圣克里斯托瓦尔.md "wikilink")：拉古纳的圣克里斯托瓦尔。
  - 西班牙[圣地亚哥·德·孔波斯特拉](../Page/圣地亚哥·德·孔波斯特拉.md "wikilink")：圣地亚哥·德·孔波斯特拉古城、通往圣地亚哥·德·孔波斯特拉之路。
  - 西班牙[塞哥维亚](../Page/塞哥维亚.md "wikilink")：塞哥维亚古城及其水道。
  - 西班牙[托莱多](../Page/托莱多_\(西班牙\).md "wikilink")：托莱多古城。
  - 西班牙[乌韦达](../Page/乌韦达.md "wikilink")：乌韦达的[文艺复兴建筑群](../Page/文艺复兴.md "wikilink")。

#### [法国](../Page/法国.md "wikilink")（11座）

[Cathedral_of_Amiens_front.jpg](https://zh.wikipedia.org/wiki/File:Cathedral_of_Amiens_front.jpg "fig:Cathedral_of_Amiens_front.jpg")\]\]

  - 法国[亚眠](../Page/亚眠.md "wikilink")：亚眠主教座堂。
  - 法国[阿维尼翁](../Page/阿维尼翁.md "wikilink")：阿维尼翁
  - 法国[波尔多](../Page/波尔多.md "wikilink")：波尔多
  - 法国[卡尔卡松](../Page/卡尔卡松.md "wikilink")：卡尔卡松设防城。
  - 法国[勒阿弗尔](../Page/勒阿弗尔.md "wikilink")：勒阿弗尔
  - 法国[圣米歇尔山](../Page/圣米歇尔山.md "wikilink")：圣米歇尔山及其海湾。
  - 法国[里昂](../Page/里昂.md "wikilink")：里昂的历史古蹟。
  - 法国[南锡](../Page/南锡.md "wikilink")：南锡的斯坦尼斯拉斯广场、卡里耶尔广场和阿利扬斯广场。
  - 法国[巴黎](../Page/巴黎.md "wikilink")：巴黎的塞纳河畔。
  - 法国[普罗万](../Page/普罗万.md "wikilink")：普罗万[中世纪集镇](../Page/中世纪.md "wikilink")。
  - 法国[斯特拉斯堡](../Page/斯特拉斯堡.md "wikilink")：斯特拉斯堡—大岛。

#### [葡萄牙](../Page/葡萄牙.md "wikilink")（5座）

  - 葡萄牙[英雄港](../Page/英雄港.md "wikilink")：英雄港中心区。
  - 葡萄牙[埃武拉](../Page/埃武拉.md "wikilink")：埃武拉历史中心。
  - 葡萄牙[吉马朗伊斯](../Page/吉马朗伊斯.md "wikilink")：吉马朗伊斯历史中心。

[Portugal_Porto_GDFL_050326_137.JPG](https://zh.wikipedia.org/wiki/File:Portugal_Porto_GDFL_050326_137.JPG "fig:Portugal_Porto_GDFL_050326_137.JPG")历史中心\]\]

  - 葡萄牙[波尔图](../Page/波尔图.md "wikilink")：波尔图历史中心。
  - 葡萄牙[辛特拉](../Page/辛特拉.md "wikilink")：辛特拉文化景观。

#### [意大利](../Page/意大利.md "wikilink")（27座）

  - 意大利[亚西西](../Page/亚西西.md "wikilink")：亚西西的[圣方济各教堂和其他](../Page/圣方济各教堂.md "wikilink")[方济各会建筑](../Page/方济各会.md "wikilink")。
  - 意大利[卡尔塔吉龙](../Page/卡尔塔吉龙.md "wikilink")：东南[西西里](../Page/西西里.md "wikilink")[诺托谷地的晚期](../Page/诺托谷地.md "wikilink")[巴洛克风格城镇](../Page/巴洛克.md "wikilink")。
  - 意大利[卡普里亚特·圣赫瓦西奥](../Page/卡普里亚特·圣赫瓦西奥.md "wikilink")：阿达的克雷斯皮。
  - 意大利[卡塔尼亚](../Page/卡塔尼亚.md "wikilink")：东南西西里诺托谷地的晚期巴洛克风格城镇。
  - 意大利[费拉拉](../Page/费拉拉.md "wikilink")：[文艺复兴城市费拉拉及其](../Page/文艺复兴.md "wikilink")[波河](../Page/波河.md "wikilink")[三角洲](../Page/三角洲.md "wikilink")。

[From_the_campanile.jpg](https://zh.wikipedia.org/wiki/File:From_the_campanile.jpg "fig:From_the_campanile.jpg")历史中心\]\]

  - 意大利[佛罗伦萨](../Page/佛罗伦萨.md "wikilink")：佛罗伦萨历史中心。
  - 意大利[热那亚](../Page/热那亚.md "wikilink")：热那亚
  - 意大利[马泰拉](../Page/马泰拉.md "wikilink")：马泰拉的岩洞民居。
  - 意大利[米利特洛·瓦勒·迪·卡塔尼亚](../Page/米利特洛·瓦勒·迪·卡塔尼亚.md "wikilink")：东南西西里诺托谷地的晚期巴洛克风格城镇。
  - 意大利[摩德纳](../Page/摩德纳.md "wikilink")：摩德纳的主教座堂、市民塔和大广场。
  - 意大利[莫迪卡](../Page/莫迪卡.md "wikilink")：东南西西里诺托谷地的晚期巴洛克风格城镇。
  - 意大利[那不勒斯](../Page/那不勒斯.md "wikilink")：那不勒斯历史中心。
  - 意大利[诺托](../Page/诺托.md "wikilink")：东南西西里诺托谷地的晚期巴洛克风格城镇。
  - 意大利[帕拉佐洛·阿克雷德](../Page/帕拉佐洛·阿克雷德.md "wikilink")：东南西西里诺托谷地的晚期巴洛克风格城镇。
  - 意大利[皮恩扎](../Page/皮恩扎.md "wikilink")：皮恩扎城历史中心。
  - 意大利[拉古萨](../Page/拉古萨.md "wikilink")：东南西西里诺托谷地的晚期巴洛克风格城镇。

[Colosseum_in_Rome,_Italy_-_April_2007.jpg](https://zh.wikipedia.org/wiki/File:Colosseum_in_Rome,_Italy_-_April_2007.jpg "fig:Colosseum_in_Rome,_Italy_-_April_2007.jpg")\]\]

  - 意大利[罗马](../Page/罗马.md "wikilink")：罗马历史中心。
  - 意大利[萨宾尼塔](../Page/萨宾尼塔.md "wikilink")：萨宾尼塔
  - 意大利[圣吉米尼亚诺](../Page/圣吉米尼亚诺.md "wikilink")：圣吉米尼亚诺历史中心。
  - 意大利[西克里](../Page/西克里.md "wikilink")：东南西西里诺托谷地的晚期巴洛克风格城镇。
  - 意大利[锡耶纳](../Page/锡耶纳.md "wikilink")：锡耶纳历史中心。
  - 意大利[乌尔比诺](../Page/乌尔比诺.md "wikilink")：乌尔比诺历史中心。

[View_of_the_Grand_Canal_from_Rialto_to_Ca'Foscari.jpg](https://zh.wikipedia.org/wiki/File:View_of_the_Grand_Canal_from_Rialto_to_Ca'Foscari.jpg "fig:View_of_the_Grand_Canal_from_Rialto_to_Ca'Foscari.jpg")[运河](../Page/运河.md "wikilink")\]\]

  - 意大利[威尼斯](../Page/威尼斯.md "wikilink")：威尼斯及其[潟湖](../Page/潟湖.md "wikilink")。
  - 意大利[维罗纳](../Page/维罗纳.md "wikilink")：维罗纳城。
  - 意大利[维琴察](../Page/维琴察.md "wikilink")：维琴察城和[威尼托地区的](../Page/威尼托.md "wikilink")[帕拉第奥风格](../Page/帕拉第奥风格.md "wikilink")[别墅](../Page/别墅.md "wikilink")。
  - 意大利[曼托瓦](../Page/曼托瓦.md "wikilink")：曼托瓦
  - 意大利[锡拉库萨](../Page/锡拉库萨.md "wikilink")：锡拉库萨

#### [德国](../Page/德国.md "wikilink")（13座）

[Trier_BW_2013-04-14_15-59-54.JPG](https://zh.wikipedia.org/wiki/File:Trier_BW_2013-04-14_15-59-54.JPG "fig:Trier_BW_2013-04-14_15-59-54.JPG")[圣伯多禄主教座堂](../Page/特里尔主教座堂.md "wikilink")\]\]

  - 德国[特里尔](../Page/特里尔.md "wikilink")：特里尔的[古罗马建筑](../Page/古罗马.md "wikilink")、[圣伯多禄主教座堂和](../Page/特里尔主教座堂.md "wikilink")[圣母教堂](../Page/圣母教堂.md "wikilink")。
  - 德国[班堡](../Page/班堡.md "wikilink")：班堡镇。
  - 德国[不来梅](../Page/不来梅.md "wikilink")：不来梅
  - 德国[德累斯顿](../Page/德累斯顿.md "wikilink")：德累斯顿
  - 德国[戈斯拉尔](../Page/戈斯拉尔.md "wikilink")：[拉默尔斯贝格矿和戈斯拉尔古城](../Page/拉默尔斯贝格矿.md "wikilink")。
  - 德国[吕贝克](../Page/吕贝克.md "wikilink")：[汉萨同盟城市吕贝克](../Page/汉萨同盟.md "wikilink")。
  - 德国[波茨坦](../Page/波茨坦.md "wikilink")：[无忧宫](../Page/无忧宫.md "wikilink")。
  - 德国[奎德林堡](../Page/奎德林堡.md "wikilink")：奎德林堡的牧师会教堂、城堡和古城。
  - 德国[雷根斯堡](../Page/雷根斯堡.md "wikilink")：雷根斯堡

[Nikolaikirche_Rathaus_HST.jpg](https://zh.wikipedia.org/wiki/File:Nikolaikirche_Rathaus_HST.jpg "fig:Nikolaikirche_Rathaus_HST.jpg")圣尼各老主教座堂\]\]

  - 德国[施特拉尔松](../Page/施特拉尔松.md "wikilink")：施特拉尔松历史中心。
  - 德国[魏玛](../Page/魏玛.md "wikilink")：古典魏玛。
  - 德国[维斯马](../Page/维斯马.md "wikilink")：维斯马历史中心。
  - 德国[柏林](../Page/柏林.md "wikilink")：柏林。

#### [斯洛伐克](../Page/斯洛伐克.md "wikilink")（2座）

  - 斯洛伐克[班斯卡—什佳夫尼察](../Page/班斯卡—什佳夫尼察.md "wikilink")：班斯卡—什加夫尼察。
  - 斯洛伐克[巴尔代约夫](../Page/巴尔代约夫.md "wikilink")：巴尔代约夫城保护区。

#### [英国](../Page/英国.md "wikilink")（4座）

[Aerial.view.of.bath.arp.jpg](https://zh.wikipedia.org/wiki/File:Aerial.view.of.bath.arp.jpg "fig:Aerial.view.of.bath.arp.jpg")城鸟瞰\]\]

  - 英国[巴斯](../Page/巴斯.md "wikilink")：巴斯城。
  - 英国[爱丁堡](../Page/爱丁堡.md "wikilink")：爱丁堡的老城和新城。
  - 英国[利物浦](../Page/利物浦.md "wikilink")：利物浦
  - 英国[特尔福德](../Page/特尔福德.md "wikilink")：[铁桥峡谷](../Page/铁桥峡谷.md "wikilink")。

#### [挪威](../Page/挪威.md "wikilink")（2座）

  - 挪威[卑尔根](../Page/卑尔根.md "wikilink")：[布吕根](../Page/布吕根.md "wikilink")。
  - 挪威[勒罗斯](../Page/勒罗斯.md "wikilink")：勒罗斯。

#### [罗马尼亚](../Page/罗马尼亚.md "wikilink")（2座）

  - 罗马尼亚[比耶塔恩](../Page/比耶塔恩.md "wikilink")：[特兰西瓦尼亚的撒克逊人村落及其设防教堂](../Page/特兰西瓦尼亚的撒克逊人村落及其设防教堂.md "wikilink")。
  - 罗马尼亚[锡吉什瓦拉](../Page/锡吉什瓦拉.md "wikilink")：锡吉什瓦拉历史中心。

#### [比利时](../Page/比利时.md "wikilink")（3座，其中观察员城市1座）

  - 比利时[那慕尔](../Page/那慕尔.md "wikilink")（观察员）：比利时[瓦隆地区的](../Page/瓦隆.md "wikilink")[钟楼](../Page/比利时和法国的钟楼.md "wikilink")。

[Bruges_De_Burg.JPG](https://zh.wikipedia.org/wiki/File:Bruges_De_Burg.JPG "fig:Bruges_De_Burg.JPG")城鸟瞰\]\]

  - 比利时[布鲁日](../Page/布鲁日.md "wikilink")：[布鲁日历史中心](../Page/布鲁日历史中心.md "wikilink")。
  - 比利时[布鲁塞尔](../Page/布鲁塞尔.md "wikilink")：[布鲁塞尔大广场](../Page/布鲁塞尔大广场.md "wikilink")、建筑师[维克多·奥尔塔设计的布鲁塞尔主要城市建筑](../Page/维克多·奥尔塔.md "wikilink")。

#### [捷克](../Page/捷克.md "wikilink")（6座）

  - 捷克[捷克克鲁姆洛夫](../Page/捷克克鲁姆洛夫.md "wikilink")：捷克克鲁姆洛夫历史中心。
  - 捷克[霍拉索维采](../Page/霍拉索维采.md "wikilink")：霍拉索维采历史村落保护区。
  - 捷克[库特纳霍拉](../Page/库特纳霍拉.md "wikilink")：库特纳霍拉历史城区及[圣芭芭拉教堂](../Page/圣芭芭拉教堂.md "wikilink")。

[Hradcany.jpg](https://zh.wikipedia.org/wiki/File:Hradcany.jpg "fig:Hradcany.jpg")历史中心\]\]

  - 捷克[布拉格](../Page/布拉格.md "wikilink")：布拉格历史中心。
  - 捷克[泰尔奇](../Page/泰尔奇.md "wikilink")：泰尔奇历史中心。
  - 捷克[特热比奇](../Page/特热比奇.md "wikilink")：特热比奇的[犹太社区和](../Page/犹太.md "wikilink")[圣普罗科庇乌斯主教座堂](../Page/圣普罗科庇乌斯主教座堂.md "wikilink")。

#### [希腊](../Page/希腊.md "wikilink")（4座）

  - 希腊[帕特拉莫斯岛上的镇](../Page/帕特拉莫斯岛上的镇.md "wikilink")：帕特莫斯岛的[科拉历史中心及](../Page/科拉.md "wikilink")[神学家圣约翰修道院和](../Page/神学家圣约翰修道院.md "wikilink")[启示录洞](../Page/启示录洞.md "wikilink")。
  - 希腊[科孚岛](../Page/科孚岛.md "wikilink")：
  - 希腊[罗得岛](../Page/罗得岛.md "wikilink")：罗得岛[中世纪古城](../Page/中世纪.md "wikilink")。
  - Edessa

#### [俄罗斯](../Page/俄罗斯.md "wikilink")（6座）

  - 俄罗斯[杰本宾特](../Page/杰本宾特.md "wikilink")：杰尔宾特的古城和城堡建筑。
  - 俄罗斯[喀山](../Page/喀山.md "wikilink")：喀山城堡历史建筑群。

[Moscow_Kremlin.jpg](https://zh.wikipedia.org/wiki/File:Moscow_Kremlin.jpg "fig:Moscow_Kremlin.jpg")[克里姆林宫](../Page/克里姆林宫.md "wikilink")\]\]

  - 俄罗斯[莫斯科](../Page/莫斯科.md "wikilink")：[克里姆林宫和](../Page/克里姆林宫.md "wikilink")[红场](../Page/红场.md "wikilink")。
  - 俄罗斯[诺夫哥罗德](../Page/诺夫哥罗德.md "wikilink")：诺夫哥罗德和周边地区的历史古蹟。
  - 俄罗斯[圣彼得堡](../Page/圣彼得堡.md "wikilink")：圣彼得堡历史中心及相关建筑群。

<!-- end list -->

  - 俄罗斯[雅罗斯拉夫尔](../Page/雅罗斯拉夫尔.md "wikilink")：雅罗斯拉夫尔城历史中心。

#### [克罗地亚](../Page/克罗地亚.md "wikilink")（3座）

  - 克罗地亚[杜布羅夫尼克](../Page/杜布羅夫尼克.md "wikilink")：杜布罗夫尼克古城。
  - 克罗地亚[斯普利特](../Page/斯普利特.md "wikilink")：斯普利特古建筑群和[戴克里先宫](../Page/戴克里先宫.md "wikilink")。
  - 克罗地亚[特罗吉尔](../Page/特罗吉尔.md "wikilink")：特罗吉尔古城。

#### [奥地利](../Page/奥地利.md "wikilink")（4座）

[Graz_clock_tower.jpg](https://zh.wikipedia.org/wiki/File:Graz_clock_tower.jpg "fig:Graz_clock_tower.jpg")钟楼\]\]

  - 奥地利[格拉茨](../Page/格拉茨.md "wikilink")：格拉茨历史中心。
  - 奥地利[哈尔施塔特](../Page/哈尔施塔特.md "wikilink")：[萨尔茨卡默古特的](../Page/萨尔茨卡默古特.md "wikilink")[哈尔施塔特—达赫施泰因文化景观](../Page/哈尔施塔特—达赫施泰因.md "wikilink")。
  - 奥地利[萨尔茨堡](../Page/萨尔茨堡.md "wikilink")：萨尔茨堡城历史中心。
  - 奥地利[维也纳](../Page/维也纳.md "wikilink")：维也纳历史中心。

#### [土耳其](../Page/土耳其.md "wikilink")（2座）

[Hagia_Sophia_B12-40.jpg](https://zh.wikipedia.org/wiki/File:Hagia_Sophia_B12-40.jpg "fig:Hagia_Sophia_B12-40.jpg")[圣索非亚教堂](../Page/圣索非亚教堂.md "wikilink")\]\]

  - 土耳其[伊斯坦布尔](../Page/伊斯坦布尔.md "wikilink")：伊斯坦布尔历史区。
  - 土耳其[萨夫兰博卢](../Page/萨夫兰博卢.md "wikilink")：萨夫兰博卢城。

#### [波兰](../Page/波兰.md "wikilink")（4座）

  - 波兰[托伦](../Page/托伦.md "wikilink")：托伦[中世纪古城](../Page/中世纪.md "wikilink")。
  - 波兰[克拉科夫](../Page/克拉科夫.md "wikilink")：克拉科夫历史中心。
  - 波兰[华沙](../Page/华沙.md "wikilink")：华沙历史中心。
  - 波兰[扎莫希奇](../Page/扎莫希奇.md "wikilink")：扎莫希奇古城。

#### [瑞典](../Page/瑞典.md "wikilink")（3座，其中观察员城市1座）

  - 瑞典[斯德哥尔摩](../Page/斯德哥尔摩.md "wikilink")（观察员）：[德宁翰宫和](../Page/德宁翰宫.md "wikilink")[斯考哥司基考哥登森林墓园](../Page/斯考哥司基考哥登森林墓园.md "wikilink")。
  - 瑞典[卡尔斯克鲁纳](../Page/卡尔斯克鲁纳.md "wikilink")：[卡尔斯克鲁纳军港](../Page/卡尔斯克鲁纳军港.md "wikilink")。

[Visby_stadtmauer.jpg](https://zh.wikipedia.org/wiki/File:Visby_stadtmauer.jpg "fig:Visby_stadtmauer.jpg")古城墙\]\]

  - 瑞典[维斯比](../Page/维斯比.md "wikilink")：[汉萨同盟城市维斯比](../Page/汉萨同盟.md "wikilink")。

#### [加拿大](../Page/加拿大.md "wikilink")（2座）

  - 加拿大[伦恩堡](../Page/伦恩堡.md "wikilink")：伦恩堡古城。
  - 加拿大[魁北克市](../Page/魁北克市.md "wikilink")：魁北克古城区。

#### 阿尔巴尼亚（2座）

  - Berat
  - Gjirokastra

#### 其他（14座）

[StPetersBasilica01_gobeirne.jpg](https://zh.wikipedia.org/wiki/File:StPetersBasilica01_gobeirne.jpg "fig:StPetersBasilica01_gobeirne.jpg")[圣伯多禄大殿](../Page/圣伯多禄大殿.md "wikilink")\]\]

  - [教廷](../Page/教廷.md "wikilink")[梵蒂冈城](../Page/梵蒂冈城.md "wikilink")：[墙外的圣保罗教堂](../Page/墙外的圣保罗教堂.md "wikilink")、梵蒂冈城。
  - [荷兰](../Page/荷兰.md "wikilink")[贝姆斯特尔](../Page/贝姆斯特尔.md "wikilink")：[贝姆斯特尔圩田](../Page/贝姆斯特尔.md "wikilink")。
  - [瑞士](../Page/瑞士.md "wikilink")[伯尔尼](../Page/伯尔尼.md "wikilink")：伯尔尼古城。
  - [匈牙利](../Page/匈牙利.md "wikilink")[布达佩斯](../Page/布达佩斯.md "wikilink")：布达佩斯，包括[多瑙河岸](../Page/多瑙河.md "wikilink")、[布达城堡地区和](../Page/布达城堡地区.md "wikilink")[安德拉希大街](../Page/安德拉希大街.md "wikilink")。
  - [黑山](../Page/黑山.md "wikilink")[科托尔](../Page/科托尔.md "wikilink")：科托尔的自然与文化历史区。

[Lwow-panorama-m.jpg](https://zh.wikipedia.org/wiki/File:Lwow-panorama-m.jpg "fig:Lwow-panorama-m.jpg")城鸟瞰\]\]

  - [乌克兰](../Page/乌克兰.md "wikilink")[利沃夫](../Page/利沃夫.md "wikilink")：利沃夫历史中心。
  - [卢森堡](../Page/卢森堡.md "wikilink")[卢森堡城](../Page/卢森堡城.md "wikilink")：卢森堡城的老城区和防御工事。
  - [保加利亚](../Page/保加利亚.md "wikilink")[内塞伯尔](../Page/内塞伯尔.md "wikilink")：内塞伯尔古城。
  - [马其顿](../Page/马其顿.md "wikilink")[奥赫里德](../Page/奥赫里德.md "wikilink")：奥赫里德地区的文化历史遗蹟和自然环境。
  - [芬兰](../Page/芬兰.md "wikilink")[劳马](../Page/劳马.md "wikilink")：劳马古城。
  - [拉脱维亚](../Page/拉脱维亚.md "wikilink")[里加](../Page/里加.md "wikilink")：里加历史中心。
  - [爱沙尼亚](../Page/爱沙尼亚.md "wikilink")[塔林](../Page/塔林.md "wikilink")：塔林古城历史中心。
  - [马耳他](../Page/马耳他.md "wikilink")[瓦莱塔](../Page/瓦莱塔.md "wikilink")：瓦莱塔城。
  - [立陶宛](../Page/立陶宛.md "wikilink")[维尔纽斯](../Page/维尔纽斯.md "wikilink")：维尔纽斯历史中心。
  - [圣马力诺](../Page/圣马力诺.md "wikilink")：圣马力诺
  - [波黑](../Page/波黑.md "wikilink")[莫斯塔尔](../Page/莫斯塔尔.md "wikilink")：

### [拉丁美洲和](../Page/拉丁美洲.md "wikilink")[加勒比地区](../Page/加勒比地区.md "wikilink")（共40座）

#### [秘鲁](../Page/秘鲁.md "wikilink")（3座）

  - 秘鲁[阿雷基帕](../Page/阿雷基帕.md "wikilink")：阿雷基帕历史中心。

[Cusco_church_la_compania.jpg](https://zh.wikipedia.org/wiki/File:Cusco_church_la_compania.jpg "fig:Cusco_church_la_compania.jpg")拉孔帕尼亚教堂\]\]

  - 秘鲁[库斯科](../Page/库斯科.md "wikilink")：库斯科城。
  - 秘鲁[利马](../Page/利马.md "wikilink")：利马历史中心。

#### [巴西](../Page/巴西.md "wikilink")（7座）

  - 巴西[巴西利亚](../Page/巴西利亚.md "wikilink")：巴西利亚。
  - 巴西[迪亚曼蒂那](../Page/迪亚曼蒂那.md "wikilink")：迪亚曼蒂纳历史中心。
  - 巴西[戈亚斯](../Page/戈亚斯.md "wikilink")：戈亚斯历史中心。
  - 巴西[奥林达](../Page/奥林达.md "wikilink")：奥林达历史中心。
  - 巴西[欧罗·普雷托](../Page/欧罗·普雷托.md "wikilink")：欧罗·普雷托古城。
  - 巴西[萨尔瓦多](../Page/萨尔瓦多_\(巴西\).md "wikilink")：萨尔瓦多历史中心。
  - 巴西[圣路易斯](../Page/圣路易斯_\(马拉里昂州\).md "wikilink")：圣路易斯历史中心。

#### [墨西哥](../Page/墨西哥.md "wikilink")（10座）

  - 墨西哥[坎佩切](../Page/坎佩切.md "wikilink")：[坎佩切州的](../Page/坎佩切州.md "wikilink")[卡拉克穆尔古](../Page/卡拉克穆尔.md "wikilink")[玛雅城市](../Page/玛雅.md "wikilink")。
  - 墨西哥[瓜纳华托](../Page/瓜纳华托.md "wikilink")：瓜纳华托古城和周围的[银矿](../Page/银矿.md "wikilink")。
  - 墨西哥[墨西哥城](../Page/墨西哥城.md "wikilink")：墨西哥城历史中心和[霍奇米尔科](../Page/霍奇米尔科.md "wikilink")。

[CatedralMorelia.jpg](https://zh.wikipedia.org/wiki/File:CatedralMorelia.jpg "fig:CatedralMorelia.jpg")主教座堂\]\]

  - 墨西哥[莫雷利亚](../Page/莫雷利亚.md "wikilink")：莫雷利亚历史中心。
  - 墨西哥[瓦哈卡](../Page/瓦哈卡.md "wikilink")：瓦哈卡历史中心和[阿尔万山考古遗址](../Page/阿尔万山.md "wikilink")。
  - 墨西哥[普埃布拉](../Page/普埃布拉.md "wikilink")：普埃布拉历史中心。
  - 墨西哥[圣地亚哥·德·克雷塔罗](../Page/圣地亚哥·德·克雷塔罗.md "wikilink")：克雷塔罗历史古蹟区。
  - 墨西哥[特拉科塔尔潘](../Page/特拉科塔尔潘.md "wikilink")：特拉克塔尔潘历史古蹟区。
  - 墨西哥[萨卡特卡斯](../Page/萨卡特卡斯.md "wikilink")：萨卡特卡斯历史中心。
  - 墨西哥San Miguel de Allende

#### [哥伦比亚](../Page/哥伦比亚.md "wikilink")（2座）

[Cartagena_-_Fortaleza_San_Felipe_de_Barajas_-_20050430bis.jpg](https://zh.wikipedia.org/wiki/File:Cartagena_-_Fortaleza_San_Felipe_de_Barajas_-_20050430bis.jpg "fig:Cartagena_-_Fortaleza_San_Felipe_de_Barajas_-_20050430bis.jpg")要塞\]\]

  - 哥伦比亚[卡塔赫纳](../Page/卡塔赫纳_\(哥伦比亚\).md "wikilink")：卡塔赫纳的港口、要塞和古蹟群。
  - 哥伦比亚[圣克鲁斯·德·蒙波斯](../Page/圣克鲁斯·德·蒙波斯.md "wikilink")：圣克鲁斯·德·蒙波斯历史中心。

#### [厄瓜多尔](../Page/厄瓜多尔.md "wikilink")（2座）

  - 厄瓜多尔[昆卡](../Page/昆卡_\(厄瓜多尔\).md "wikilink")：昆卡历史中心。
  - 厄瓜多尔[基多](../Page/基多.md "wikilink")：基多城。

#### [古巴](../Page/古巴.md "wikilink")（4座）

[Havana_Cathedral.JPG](https://zh.wikipedia.org/wiki/File:Havana_Cathedral.JPG "fig:Havana_Cathedral.JPG")的哈瓦那主教座堂\]\]

  - 古巴[哈瓦那](../Page/哈瓦那.md "wikilink")：[哈瓦那旧城及其防御工事](../Page/哈瓦那旧城.md "wikilink")。
  - 古巴[特立尼达](../Page/特立尼达_\(古巴\).md "wikilink")：特立尼达和[洛斯·因赫尼奥斯谷地](../Page/洛斯·因赫尼奥斯谷地.md "wikilink")。
  - Cienfuegos
  - Camaguey

#### [玻利维亚](../Page/玻利维亚.md "wikilink")（2座）

  - 玻利维亚[波托西](../Page/波托西.md "wikilink")：波托西城。
  - 玻利维亚[苏克雷](../Page/苏克雷.md "wikilink")：苏克雷古城。

#### 其他（9座）

  - [危地马拉](../Page/危地马拉.md "wikilink")[安地瓜](../Page/安地瓜.md "wikilink")：[旧危地马拉](../Page/旧危地马拉.md "wikilink")（安地瓜）。
  - [乌拉圭](../Page/乌拉圭.md "wikilink")[科洛尼亚·德·萨克拉门托](../Page/科洛尼亚·德·萨克拉门托.md "wikilink")：科洛尼亚·德·萨克拉门托古城区。
  - [委内瑞拉](../Page/委内瑞拉.md "wikilink")[圣安娜·德·科罗](../Page/圣安娜·德·科罗.md "wikilink")：科罗及其港口。
  - [巴拿马](../Page/巴拿马.md "wikilink")[巴拿马城](../Page/巴拿马城.md "wikilink")：巴拿马城考古遗址和巴拿马城古城区。

[Paramaribo-Waterkant.jpg](https://zh.wikipedia.org/wiki/File:Paramaribo-Waterkant.jpg "fig:Paramaribo-Waterkant.jpg")市中心的欧式住宅群\]\]

  - [苏里南](../Page/苏里南.md "wikilink")[帕拉马里博](../Page/帕拉马里博.md "wikilink")：帕拉马里博内城。
  - [多米尼加](../Page/多米尼加.md "wikilink")[圣多明各](../Page/圣多明各.md "wikilink")：殖民城市圣多明各。
  - [百慕大](../Page/百慕大.md "wikilink")[圣乔治](../Page/圣乔治.md "wikilink")：圣乔治古城及相关要塞。
  - [智利](../Page/智利.md "wikilink")[瓦尔帕莱索](../Page/瓦尔帕莱索.md "wikilink")：港口城市瓦尔帕莱索的古城区。
  - [荷属安的列斯](../Page/荷属安的列斯.md "wikilink")[威廉斯塔德](../Page/威廉斯塔德.md "wikilink")：威廉斯塔德历史区、内城和港口。

## 官方语言

世界遗产城市联盟共有5种[官方语言](../Page/官方语言.md "wikilink")，分别为[法语](../Page/法语.md "wikilink")、[英语](../Page/英语.md "wikilink")、[西班牙语](../Page/西班牙语.md "wikilink")、[阿拉伯语和](../Page/阿拉伯语.md "wikilink")[葡萄牙语](../Page/葡萄牙语.md "wikilink")。

## 参考资料

<div class="references-small">

<references />

</div>

  - [世界遗产城市联盟官方网站（法文）](http://www.ovpm.org)

## 外部链接

  - [蘇州成為“世界遺產城市聯盟”第209個成員城市](https://web.archive.org/web/20090623043919/http://www.js.xinhuanet.com/xin_wen_zhong_xin/2006-06/10/content_7226877.htm)

  - [中国承德、丽江、平遥、苏州等城市成功加入世界遗产城市联盟](https://web.archive.org/web/20070928225241/http://www.wchol.com/html/tebiebaodao/2006/0610/43.html)

[Category:国际组织](../Category/国际组织.md "wikilink")
[Category:世界遗产](../Category/世界遗产.md "wikilink")

1.