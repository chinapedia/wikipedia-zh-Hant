是由[全效工作室开发](../Page/全效工作室.md "wikilink")、[微软发行的](../Page/微软.md "wikilink")[即時戰略遊戲](../Page/即時戰略遊戲.md "wikilink")《[世紀帝國III](../Page/世紀帝國III.md "wikilink")》的第一部官方[资料片](../Page/资料片.md "wikilink")。

## 游戏要素

### 文明

群酋爭霸在原有八个[欧洲文明基础上](../Page/欧洲.md "wikilink")，加入了三个美洲文明[阿茲特克](../Page/阿茲特克.md "wikilink")，[蘇族和](../Page/蘇族.md "wikilink")[易落魁](../Page/易洛魁聯盟.md "wikilink")。

### 单位

由于游戏的[时间线被拓展至](../Page/时间线.md "wikilink")[南北战争以后](../Page/南北战争.md "wikilink")，群酋爭霸中增加了[格林機槍](../Page/格林機槍.md "wikilink")，[裝甲船等](../Page/鐵甲艦.md "wikilink")19世纪后期出现的[武器装备](../Page/武器装备.md "wikilink")。

此外，群酋爭霸中还增加了七種美洲[原住民](../Page/原住民.md "wikilink")，包括[夏安人](../Page/夏安族.md "wikilink")，[納瓦霍人](../Page/納瓦霍人.md "wikilink")，[阿帕契人](../Page/阿帕契族.md "wikilink")，[薩波特克人](../Page/薩波特克文明.md "wikilink")，[休倫人](../Page/休倫人.md "wikilink")，[曼帕契人](../Page/馬普切人.md "wikilink")，[克拉馬人](../Page/克拉馬人.md "wikilink")。

### 时代

#### 革命

原來的八個歐洲國家於工業時代時新增了一個的時代升級——[革命](../Page/革命.md "wikilink")，是帝王時代以外的另一選擇。革命之後，所有拓荒者、森林酷民、拓荒者馬車將變成殖民地民兵（但在革命之後由寶藏拯救或原住民部落所生產的拓荒者及森林酷民則不會自動轉變為殖民地民兵），並失去採集能力，且船運卡片皆強制變成革命式組合。因此，[工廠](../Page/工廠.md "wikilink")、[貿易路線和](../Page/貿易路線.md "wikilink")[漁業將成為主要經濟來源](../Page/漁業.md "wikilink")。

<table>
<tbody>
<tr class="odd">
<td><p><strong>國家</strong></p></td>
<td><p><strong>革命領袖</strong></p></td>
<td><p><strong>好處</strong></p></td>
<td><p><strong>殖民母國</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/米格爾·伊達爾戈·伊·科斯蒂利亞.md" title="wikilink">伊達戈</a></p></td>
<td><p>亡命槍手與亡命騎士各十名。</p></td>
<td><p><a href="../Page/西班牙.md" title="wikilink">西班牙</a>、<a href="../Page/葡萄牙.md" title="wikilink">葡萄牙</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/西蒙·玻利瓦爾.md" title="wikilink">波利法</a></p></td>
<td><p>所有單位增加百分之五的生命值。</p></td>
<td><p><a href="../Page/德國.md" title="wikilink">德國</a>、<a href="../Page/俄羅斯.md" title="wikilink">俄羅斯</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/何塞·德·聖馬丁.md" title="wikilink">聖馬丁</a></p></td>
<td><p>六座帝國榴彈砲。</p></td>
<td><p><a href="../Page/德國.md" title="wikilink">德國</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/喬治·華盛頓.md" title="wikilink">華盛頓</a></p></td>
<td><p>格林機槍擁有更多的生命值。</p></td>
<td><p><a href="../Page/英國.md" title="wikilink">英國</a>、<a href="../Page/荷蘭.md" title="wikilink">荷蘭</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>二十名兇猛的傳奇圖皮戰士相助，所有原住民單位升級到傳奇等級。</p></td>
<td><p><a href="../Page/葡萄牙.md" title="wikilink">葡萄牙</a>、<a href="../Page/荷蘭.md" title="wikilink">荷蘭</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/弗朗西斯科·德保拉·桑坦德爾.md" title="wikilink">桑坦德</a></p></td>
<td><p>三艘裝甲軍艦。</p></td>
<td><p><a href="../Page/法國.md" title="wikilink">法國</a>、<a href="../Page/俄羅斯.md" title="wikilink">俄羅斯</a>、<a href="../Page/鄂圖曼.md" title="wikilink">鄂圖曼</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/杜桑-盧維圖爾.md" title="wikilink">路維杜爾</a></p></td>
<td><p>殖民地民兵擁有更多的生命值。</p></td>
<td><p><a href="../Page/英國.md" title="wikilink">英國</a>、<a href="../Page/法國.md" title="wikilink">法國</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/貝納多·奧希金斯.md" title="wikilink">奧希金斯</a></p></td>
<td><p>十名帝國輕騎兵。</p></td>
<td><p><a href="../Page/西班牙.md" title="wikilink">西班牙</a>、<a href="../Page/鄂圖曼.md" title="wikilink">鄂圖曼</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 戰役

| 第一幕：火燄                               | 第二幕：陰影                               |
| ------------------------------------ | ------------------------------------ |
| 戰鬥之舞                                 | 波茲曼小徑                                |
| 營救                                   | [紅雲戰爭](../Page/紅雲戰爭.md "wikilink")   |
| [布里德山](../Page/布里德山.md "wikilink")   | 佔領                                   |
| 橫越德拉威河                               | 有埋伏！                                 |
| 佛吉峽谷                                 | 轉捩點                                  |
| [薩拉托加](../Page/薩拉托加.md "wikilink")   | 信任                                   |
| 莫里斯敦戰役                               | [小大角戰役](../Page/小大角戰役.md "wikilink") |
| [約克鎮戰役](../Page/約克鎮戰役.md "wikilink") |                                      |

群酋爭霸中，戰役主線依然是環繞布萊克家族在新大陸的故事，但加入更多歷史元素，主要關於[美國獨立戰爭和](../Page/美國獨立戰爭.md "wikilink")[北美印第安戰爭](../Page/北美印第安戰爭.md "wikilink")。主角分別是艾蜜莉亞·布萊克（Amelia
Black）的父親及兒子，耐坦尼爾·布萊克及查伊頓·布萊克。主資料片的敵人藏骨集團（*Circle of
Ossus*）沒有在群酋爭霸中出現。故事旁述依舊是艾蜜莉亞·布萊克。

#### 第一幕：火燄

在戰役《火燄》（*Fire*），主角為耐坦尼爾·布萊克（*Nathaniel
Black*），他是約翰·布萊克與諾娜凱之子、艾蜜莉亞·布萊克的父親。家鄉城市是布萊克家族大莊園。

[獨立戰爭的燎火燒遍了](../Page/美國獨立戰爭.md "wikilink")[十三殖民地](../Page/北美十三州.md "wikilink")，就連北美大地上的印地安人也不免要捲入這場戰事之中，包括耐坦尼爾自幼成長的印地安村落。成年的耐坦尼爾與舅舅坎勇凱（*Kanyenke*）試圖勸止摩和克人（*Mohawk*）及塞內卡人（*Seneca*）加入英軍的一方。他們遭到一群摩和克人的伏擊，解決敵人後他們朝鄰近一處奧奈達人村落前進。在那裡二人發起反攻，摧毀敵人的市鎮中心並趕跑了他們。可是，當他們回到自己的村落時，卻發現摩和克人與一群由塞凡庫協爾上校（*Colonel
Sven
Kuechler*）統率的[黑森僱傭兵已經攻下村落](../Page/赫斯傭兵.md "wikilink")，擄去了耐坦尼爾的母親諾娜凱（*Nonahkee*）。

耐坦尼爾他們組織兵力發起攻擊，庫協爾戰敗逃走，諾娜凱被救回，卻經此一役後[易洛魁聯盟終告解體](../Page/易洛魁聯盟.md "wikilink")，分裂為親英親美兩派。耐坦尼爾的村落支持十三州人民，故此滿腔熱血的他立即帶人趕去[波士頓協防英軍](../Page/波士頓.md "wikilink")，[守衛布里德山](../Page/邦克山戰役.md "wikilink")（*Breed's
Hill*）的堡壘。波士頓戰場並未為接任大陸軍總司令的[喬治·華盛頓帶來什麼有利形勢](../Page/喬治·華盛頓.md "wikilink")，連吃敗仗的他率大陸軍退過[德拉威河](../Page/德拉瓦河.md "wikilink")。華盛頓領導一次小規模反攻，耐坦尼爾隨著他渡過德拉威河，[襲擊了駐軍特倫頓](../Page/特倫頓戰役.md "wikilink")（*Trenton*）的黑森僱傭軍兵營，並[擊潰普林斯頓](../Page/普林斯頓戰役.md "wikilink")（*Princeton*）的留守敵軍，取得一次漂亮的勝利。

大陸軍未能長期維持勝利，不久就在[布蘭迪萬河](../Page/布蘭迪萬河戰役.md "wikilink")（Brandywine
Creek）及日爾曼敦（*Germantown*）敗北，退守[佛吉峽谷](../Page/佛吉谷.md "wikilink")（Valley
Forge）過冬，結果寒冷使大陸軍折損嚴重。耐坦尼爾於是拿出他家族的大筆財產資助大陸軍，間接造成他後人艾蜜莉亞的事業困難。佛吉峽谷的危機過後，大陸軍於[薩拉托加](../Page/薩拉托加戰役.md "wikilink")（*Saratoga*）一役大勝伯戈因，重挫英軍。及後，耐坦尼爾率軍於莫里斯敦（*Morristown*）與庫協爾決戰（真實中沒有此戰役），阻止庫協爾摧毀莫里斯敦的議會。庫協爾不甘戰敗，發動自殺攻擊，被擊敗並死於戰場上。

庫協爾身亡後，耐坦尼爾參加了[查爾斯頓](../Page/查爾斯頓圍城戰.md "wikilink")、[卡姆登及](../Page/卡姆登戰役.md "wikilink")[國王山](../Page/國王山戰役.md "wikilink")（King's
Mountain）的戰鬥。戰爭天秤開始倒向大陸軍，[法國等歐洲國家聯合支持美國](../Page/波旁王朝.md "wikilink")，美法聯軍終於在[約克鎮獲得決定性勝利](../Page/約克鎮圍城戰役.md "wikilink")，英國已告完敗。獨立成功後，耐坦尼爾雖是戰爭英雄，但卻因為花了家族的財產於補給佛吉谷的美軍上而變得一貧如洗，只換來美國官方為耐坦尼爾樹立的一座雕像。

在約克鎮戰役的介紹影片中，可見主資料片第三幕出現的「老傻瓜」再次出現。

#### 第二幕：陰影

在戰役《陰影》（*Shadow*）中，主角為查伊頓·布萊克（*Chayton
Black*），他是艾蜜莉亞·布萊克的兒子。他有一半[蘇族血統](../Page/蘇族.md "wikilink")，並繼承艾蜜莉亞的獵鷹鐵路公司。家鄉城市也是布萊克家族大莊園。

查伊頓繼承了鐵路公司後，把握時機大展拳腳、擴大生意，鋪設一條沿著波茲曼小徑（*Bozeman
Trail*）向西的鐵路。鐵路鋪設過程中，公司惹上麻煩，捲入[紅雲戰爭](../Page/紅雲戰爭.md "wikilink")（*Red
Cloud's War*）的中間。查伊頓在當地結交了[賴瑞米堡](../Page/賴瑞米堡.md "wikilink")（*Fort
Laramie*）的[內戰老兵](../Page/南北戰爭.md "wikilink")、軍需官威廉·"比利"·荷姆（*William
"Billy" Holme*），防衛小徑免受蘇族（*Sioux*）的攻擊。

十年後（1876年），查伊頓回到西部，重遇故人荷姆，發現他已成為當地警長。荷姆告訴他，在[達科塔的](../Page/達科塔.md "wikilink")[黑山](../Page/黑山_\(美國\).md "wikilink")（*Black
Hills*）中蘊藏了大量的金礦，不過最煩惱的是，礦工營地時時受到蘇族人的襲擊騷擾。查伊頓幫忙抵擋了蘇族的攻擊後，請纓去見蘇族的酋長[瘋馬](../Page/瘋馬.md "wikilink")，希望為雙方締結一紙和約。可是荷姆警長在查伊頓開始和談前，竟然帶著一群礦工伏擊了蘇族人，破壞了任何締結和平的機會。

儘管荷姆十分好戰，查伊頓依然站在朋友一邊，幫助他獲取木材建築新堡壘。荷姆築起堡壘後，派查伊頓去摧毀一座沒有挑起衝突的蘇族村莊。查伊頓一口拒絕，並轉而對抗他的朋友，與蘇族人結盟起來反攻荷姆的堡壘。荷姆不敵敗退，遁逃入山脈之中。美國方面派出了[卡斯特將軍](../Page/喬治·阿姆斯特朗·卡斯特.md "wikilink")（*General
Custer*）來到，查伊頓說服卡斯特給他一日時間逮捕荷姆，解決這次動盪的真正根源。查伊頓加入了蘇族一方，在山中追擊著荷姆和他的人馬。最後二人在一處礦場迎面了，查伊頓希望用和平方法解決事件，荷姆卻不顧情義拔槍指向查伊頓，查伊頓先他一步開槍，荷姆被擊中並墮入礦井死亡。

卡斯特按捺不住，準備出兵攻打蘇族。查伊頓趕來試圖勸喻卡斯特息兵，卻被拒絕。卡斯特更表示，查伊頓不可能再騎牆，他必須要選擇身為一個白人還是一個印地安人。查伊頓下定決心，離開了卡斯特等人，策馬加入蘇族人的一方。查伊頓協助蘇族聯合[夏安族作戰](../Page/夏安族.md "wikilink")，參加[小大角戰役](../Page/小大角戰役.md "wikilink")。卡斯特的驕傲自大使他孤軍深入，喪生於慘烈的戰事之中，印地安人獲得了勝利，戰役至此落幕。

戰役後，艾蜜莉亞平靜地說她從此再沒有見過她的兒子，只聽過謠言說查伊頓大概於1890年死在[傷膝河](../Page/傷膝河大屠殺.md "wikilink")，或是帶著妻兒避居在黑山之中。至此，布萊克家族在新大陸的故事完全結束。

### 布萊克家系世系图

## 參見

  - [世紀帝國系列](../Page/世紀帝國系列.md "wikilink")
  - [世紀帝國III](../Page/世紀帝國III.md "wikilink")
  - [世紀帝國III：亞洲王朝](../Page/世紀帝國III：亞洲王朝.md "wikilink")

## 外部連結

  - [世紀帝國III：群酋爭霸 英文官方網站](http://www.ageofempires3.com/)

  - [世紀帝國III 英文官方網站](http://www.ageofempires3.com/)

[31](../Category/世紀帝國系列.md "wikilink")
[Category:后传电子游戏](../Category/后传电子游戏.md "wikilink")
[Category:资料片](../Category/资料片.md "wikilink")
[Category:微軟遊戲](../Category/微軟遊戲.md "wikilink") [Category:Games
for Windows认证游戏](../Category/Games_for_Windows认证游戏.md "wikilink")
[Category:Windows遊戲](../Category/Windows遊戲.md "wikilink") [Category:Mac
OS遊戲](../Category/Mac_OS遊戲.md "wikilink")
[Category:2006年电子游戏](../Category/2006年电子游戏.md "wikilink")