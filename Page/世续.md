**世续**（），字**伯轩**，[索勒豁](../Page/索勒豁.md "wikilink")[金氏](../Page/金氏.md "wikilink")，隶[内务府满洲](../Page/内务府满洲.md "wikilink")[正黄旗](../Page/正黄旗.md "wikilink")。清末[军机大臣](../Page/军机大臣.md "wikilink")。

## 生平

[光绪元年](../Page/光绪.md "wikilink")（1875年）[举人](../Page/举人.md "wikilink")，历任[内务府](../Page/内务府.md "wikilink")[郎中](../Page/郎中_\(官职\).md "wikilink")，擢武备院卿，授[内阁学士](../Page/内阁学士.md "wikilink")。光绪二十二年（1896年），为[总管内务府大臣兼](../Page/总管内务府大臣.md "wikilink")[工部侍郎](../Page/工部侍郎.md "wikilink")。光绪二十六年（1900年），[八国联军入京](../Page/八国联军.md "wikilink")，两宫[西狩](../Page/西狩.md "wikilink")，世续留京联络联军，保护内宫和坛庙。两宫回銮，赏[黄马褂](../Page/黄马褂.md "wikilink")，转吏部兼都统。

光绪三十二年（1906年），命为[军机大臣](../Page/军机大臣.md "wikilink")。历转[文华殿](../Page/文华殿.md "wikilink")[大学士](../Page/大学士.md "wikilink")，充宪政编查馆参预政务大臣。在[慈禧太后和](../Page/慈禧太后.md "wikilink")[光绪帝死后](../Page/光绪帝.md "wikilink")，独言国事艰危，宜立长君，但未被采纳。[宣统改元](../Page/宣统.md "wikilink")，因病告假。宣统三年（1911年）三月，任[资政院总裁](../Page/资政院.md "wikilink")，曾代表议员提出解散庆亲王内阁，另组新内阁。九月免。十月，再任总管内务府大臣。[辛亥革命起事](../Page/辛亥革命.md "wikilink")，首先赞成宣统逊位。[隆裕太后令其磋商优待条件](../Page/隆裕太后.md "wikilink")，授[太保](../Page/太保.md "wikilink")。

民国后接修[清崇陵工程](../Page/清崇陵.md "wikilink")，加[太傅](../Page/太傅.md "wikilink")。1917年[张勋复辟](../Page/张勋复辟.md "wikilink")，惧祸及，力阻之。事变亟，入宿卫，并以殓服自随。编纂《德宗实录》，书成之时，已病不能起。

民国十年（1921年）病逝，享年六十九。[溥仪追赠](../Page/溥仪.md "wikilink")[太师](../Page/太师.md "wikilink")，谥**文端**。

## 参考文献

## 参见

  - [高丽佐领](../Page/高丽佐领.md "wikilink")

{{-}}

[Category:光緒元年乙亥恩科順天鄉試舉人](../Category/光緒元年乙亥恩科順天鄉試舉人.md "wikilink")
[Category:内务府郎中](../Category/内务府郎中.md "wikilink")
[Category:內閣學士](../Category/內閣學士.md "wikilink")
[Category:總管內務府大臣](../Category/總管內務府大臣.md "wikilink")
[Category:清朝工部侍郎](../Category/清朝工部侍郎.md "wikilink")
[Category:鑲紅旗滿洲都統](../Category/鑲紅旗滿洲都統.md "wikilink")
[Category:清朝文華殿大學士](../Category/清朝文華殿大學士.md "wikilink")
[Category:军机大臣](../Category/军机大臣.md "wikilink")
[Category:清朝太保](../Category/清朝太保.md "wikilink")
[民国](../Category/清朝太傅.md "wikilink")
[民国](../Category/清朝追赠太师.md "wikilink")
[Category:正黄旗满洲佐领下人](../Category/正黄旗满洲佐领下人.md "wikilink")
[Category:资政院总裁](../Category/资政院总裁.md "wikilink")
[Category:金姓](../Category/金姓.md "wikilink")
[Category:諡文端](../Category/諡文端.md "wikilink")