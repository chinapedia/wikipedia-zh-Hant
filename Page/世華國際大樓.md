**世華國際大樓**（**Shr-Hwa International
Tower**），亦稱**台中國泰金融大樓**，座落於[台灣](../Page/台灣.md "wikilink")[臺中市](../Page/臺中市.md "wikilink")[西區](../Page/西區_\(台中市\).md "wikilink")[英才路](../Page/英才路.md "wikilink")，由[美國](../Page/美國.md "wikilink")[KPF建築事務所與](../Page/KPF建築事務所.md "wikilink")[CJW聯合建築師事務所合作設計](../Page/CJW聯合建築師事務所.md "wikilink")，是台灣第十四高[摩天大樓](../Page/摩天樓.md "wikilink")、臺中市第二高樓，地上47層，樓高176.65公尺頂樓設有[停機坪](../Page/停機坪.md "wikilink")，大樓本身以玻璃惟幕構弧型漸縮狀，兩旁夾銀色圓柱狀形成。

## 簡史

世華國際大樓原為台中在地企業[經唐實業籌建之](../Page/經唐實業.md "wikilink")「經唐洲際飯店」，1998年因原營建公司財務困難而停工。後由[霖園集團旗下企業](../Page/霖園集團.md "wikilink")[國泰金控收購](../Page/國泰金控.md "wikilink")，於2001年復工，完成於2004年。\[1\]

目前世華國際大樓主要由[霖園管理維護中區分公司](../Page/霖園管理維護.md "wikilink")\[2\]與[國泰世華銀行五權分行進駐](../Page/國泰世華銀行.md "wikilink")。26-45樓為[五星級國際觀光飯店](../Page/五星級.md "wikilink")，2006年11月由[嚴長壽領導的](../Page/嚴長壽.md "wikilink")[麗緻集團進駐](../Page/亞都麗緻大飯店.md "wikilink")、在此成立[台中亞緻大飯店](../Page/台中亞緻大飯店.md "wikilink")。

## 樓層使用

26-45樓：[台中亞緻大飯店](../Page/台中亞緻大飯店.md "wikilink")

## 週邊環境

  - [經國綠園道](../Page/經國綠園道.md "wikilink")-市民廣場-美術綠園道
  - [國立台灣美術館](../Page/國立台灣美術館.md "wikilink")
  - [國立自然科學博物館](../Page/國立自然科學博物館.md "wikilink")
  - [勤美誠品綠園道](../Page/勤美誠品綠園道.md "wikilink")
  - [SOGO廣三崇光百貨](../Page/廣三崇光百貨.md "wikilink")
  - [NOVA資訊廣場](../Page/NOVA資訊廣場.md "wikilink")
  - [全國大飯店](../Page/全國大飯店.md "wikilink")
  - [台中日華金典酒店](../Page/金典酒店.md "wikilink")
  - [臺中市西區中正國民小學](../Page/臺中市西區中正國民小學.md "wikilink")
  - [臺中市立忠明高級中學](../Page/臺中市立忠明高級中學.md "wikilink")
  - [國立臺中第二高級中學](../Page/國立臺中第二高級中學.md "wikilink")

## 圖片集

[File:世華國際大樓2.jpg|世華國際大樓黃昏時](File:世華國際大樓2.jpg%7C世華國際大樓黃昏時)
<File:Shr-Hwa> International Tower.JPG|世華國際大樓
[File:世華國際大樓02.jpg|世華國際大樓與經國園道上的裝置藝術](File:世華國際大樓02.jpg%7C世華國際大樓與經國園道上的裝置藝術)
<File:Shr-Hwa> International
Tower-1.JPG|[經國園道遠觀世華國際大樓與](../Page/經國園道.md "wikilink")[勤美誠品綠園道](../Page/勤美誠品綠園道.md "wikilink")
<File:CMP> BLOCK-2.jpg|從勤美CMP BLOCK拍攝勤美綠園道與世華國際大樓 <File:Taiwan>
043.jpg|鳥瞰經國園道與世華國際大樓 <File:Hotel> one.jpg|由市民廣場觀世華國際大樓

## 參考文獻

## 外部連結

  - [【建築師雜誌社】台中國泰金融大樓](http://www.twarchitect.org.tw/2007.05/A1-4.htm)
  - [【台灣建築經理股份有限公司】工程實績摘要](http://tremc.myweb.hinet.net/tremc/index05.htm)

[S](../Category/台中市建築物.md "wikilink")
[S](../Category/台灣摩天大樓.md "wikilink")
[S](../Category/台中市旅遊人文景點.md "wikilink") [Category:西區
(臺中市)](../Category/西區_\(臺中市\).md "wikilink")
[Category:2004年完工建築物](../Category/2004年完工建築物.md "wikilink")
[Category:150米至199米高的摩天大樓](../Category/150米至199米高的摩天大樓.md "wikilink")
[Category:2004年台灣建立](../Category/2004年台灣建立.md "wikilink")

1.
2.  [霖園管理維護](http://www.linyuan-pro.com.tw/contact-1.htm)