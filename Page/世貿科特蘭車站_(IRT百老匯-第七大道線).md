**世貿科特蘭車站**（），又稱**科特蘭街-世界貿易中心車站**，在牆壁上以「世界貿易中心」顯示，過去分別稱為「科特蘭街」及「科特蘭街-世界貿易中心」，是[紐約地鐵](../Page/紐約地鐵.md "wikilink")[IRT百老匯-第七大道線在](../Page/IRT百老匯-第七大道線.md "wikilink")[曼哈頓下城的一個](../Page/曼哈頓下城.md "wikilink")[地鐵站](../Page/地鐵站.md "wikilink")。此站位於[格林尼治街與](../Page/格林尼治街.md "wikilink")的交界，鄰近[世界貿易中心](../Page/世界貿易中心.md "wikilink")，任何時候都設有[1號線列車服務](../Page/紐約地鐵1號線.md "wikilink")。

原初的科特蘭街車站由[跨區捷運公司](../Page/跨區捷運公司.md "wikilink")（IRT）興建，並在1918年啟用。車站在1960年代原初的[世界貿易中心興建時進行翻新](../Page/世界貿易中心.md "wikilink")。當時車站位於科特蘭街地面的一部分需要拆除以便興建世界貿易中心。科特蘭街車站在[九一一襲擊事件中嚴重損壞](../Page/九一一襲擊事件.md "wikilink")。雖然百老匯-第七大道線在2002年就恢復運行，但車站的重建一直拖延至2015年，[紐約與新澤西港口事務管理局需要首先興建此站正下方的](../Page/紐約與新澤西港口事務管理局.md "wikilink")[PATH車站才可施工](../Page/世界貿易中心車站_\(PATH\).md "wikilink")。經過1.58億美元重建，科特蘭街車站在2018年9月8日以「世貿科特蘭」的名義重新啟用。

車站與PATH的[世界貿易中心車站連接](../Page/世界貿易中心車站_\(PATH\).md "wikilink")，同時亦可進行出站以行人通道前往[福爾頓轉運中心](../Page/福爾頓轉運中心.md "wikilink")。

## 車站結構

<table>
<tbody>
<tr class="odd">
<td><p><strong>G</strong></p></td>
<td><p>街道層</p></td>
<td><p><a href="../Page/維西街.md" title="wikilink">維西街</a>、、<a href="../Page/格林尼治街.md" title="wikilink">格林尼治街</a>、<a href="../Page/九一一國家紀念博物館.md" title="wikilink">九一一紀念博物館</a></p></td>
</tr>
<tr class="even">
<td><p><strong>B1<br />
上層大堂[1]</strong></p></td>
<td><p><small><a href="../Page/側式月台.md" title="wikilink">側式月台</a>，右側開門 </small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>&lt;span style=background-color:#&gt;<strong>百老匯北行</strong></span></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-R.svg" title="fig:纽约地铁R线">纽约地铁R线</a> 往<a href="../Page/森林小丘-71大道車站_(IND皇后林蔭路線).md" title="wikilink">森林小丘-71大道</a><small>（<a href="../Page/市政府車站_(BMT百老匯線).md" title="wikilink">市政府</a>）</small><br />
 <a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-W.svg" title="fig:纽约地铁W线">纽约地铁W线</a>平日（<a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-N.svg" title="fig:纽约地铁N线">纽约地铁N线</a> 深夜）往<a href="../Page/阿斯托利亞-迪特馬斯林蔭路車站_(BMT阿斯托利亞線).md" title="wikilink">迪特馬斯林蔭路</a><small>（<a href="../Page/市政府車站_(BMT百老匯線).md" title="wikilink">市政府</a>）</small></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>&lt;span style=background-color:#&gt;<strong>百老匯南行</strong></span></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-R.svg" title="fig:纽约地铁R线">纽约地铁R线</a> 往<a href="../Page/灣脊區-95街車站_(BMT第四大道線).md" title="wikilink">灣脊區-95街</a><small>（<a href="../Page/雷克托街車站_(BMT百老匯線).md" title="wikilink">雷克托街</a>）</small><br />
 <a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-W.svg" title="fig:纽约地铁W线">纽约地铁W线</a> 平日往<a href="../Page/白廳街-南碼頭車站_(BMT百老匯線).md" title="wikilink">白廳街-南碼頭</a><small>（<a href="../Page/雷克托街車站_(BMT百老匯線).md" title="wikilink">雷克托街</a>）</small><br />
 <a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-N.svg" title="fig:纽约地铁N线">纽约地铁N线</a> 深夜往<a href="../Page/康尼島-斯提威爾大道車站.md" title="wikilink">康尼島-斯提威爾大道</a><small>（<a href="../Page/雷克托街車站_(BMT百老匯線).md" title="wikilink">雷克托街</a>）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><small><a href="../Page/側式月台.md" title="wikilink">側式月台</a>，右側開門 </small></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>露台</p></td>
<td><p>、升降機、扶手電梯及往下層大堂樓梯</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><small><a href="../Page/側式月台.md" title="wikilink">側式月台</a>，右側開門 </small></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><span style="color:#{{NYCS color|red}}"><strong>第七大道北行</strong></span></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-1.svg" title="fig:纽约地铁1号线">纽约地铁1号线</a> 往<a href="../Page/范科特蘭公園-242街車站_(IRT百老匯-第七大道線).md" title="wikilink">范科特蘭公園-242街</a><small>（<a href="../Page/錢伯斯街車站_(IRT百老匯-第七大道線).md" title="wikilink">錢伯斯街</a>）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><span style="color:#{{NYCS color|red}}"><strong>第七大道南行</strong></span></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-1.svg" title="fig:纽约地铁1号线">纽约地铁1号线</a> 往<a href="../Page/南碼頭車站_(IRT百老匯-第七大道線).md" title="wikilink">南碼頭</a><small>（<a href="../Page/雷克托街車站_(IRT百老匯-第七大道線).md" title="wikilink">雷克托街</a>）</small></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><small><a href="../Page/側式月台.md" title="wikilink">側式月台</a>，右側開門 </small></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>西大堂露台</p></td>
<td><p>商店，往<a href="../Page/布魯克菲爾德廣場_(紐約).md" title="wikilink">布魯克菲爾德廣場</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>B2<br />
下層大堂</strong>[2]</p></td>
<td><p><a href="../Page/紐約地鐵.md" title="wikilink">地鐵行人通道</a></p></td>
<td><p>停靠<a href="../Page/錢伯斯街-世界貿易中心／公園廣場車站.md" title="wikilink">錢伯斯街-世界貿易中心</a><br />
途經<a href="../Page/福爾頓轉運中心.md" title="wikilink">福爾頓轉運中心</a></p></td>
</tr>
<tr class="odd">
<td><p>地鐵下通道</p></td>
<td><p><a href="../Page/MetroCard.md" title="wikilink">MetroCard售票機</a>、閘機及往<a href="../Page/科特蘭街車站_(BMT百老匯線).md" title="wikilink">百老匯線月台入口</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>商店及攤擋</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>地鐵下通道</p></td>
<td><p><a href="../Page/MetroCard.md" title="wikilink">MetroCard售票機</a>、閘機及往第七大道線月台入口</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>B3<br />
夾層</strong><includeonly>[3]</includeonly></p></td>
<td><p><a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH收費區</a></p></td>
<td><p>MetroCard/售票機，往<a href="../Page/世界貿易中心車站_(PATH).md" title="wikilink">PATH月台</a></p></td>
</tr>
<tr class="odd">
<td><p>西大堂</p></td>
<td><p>商店，往<a href="../Page/布魯克菲爾德廣場_(紐約).md" title="wikilink">布魯克菲爾德廣場</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>B4<br />
<a href="../Page/世界貿易中心車站_(PATH).md" title="wikilink"><span style="color:black">PATH月台</span></a></strong><includeonly>[4]</includeonly></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1</strong>號軌道</p></td>
<td><p> （僅繁忙時段）往<a href="../Page/霍博肯總站.md" title="wikilink">霍博肯</a><small>（<a href="../Page/交易廣場車站_(PATH).md" title="wikilink">交易廣場</a>）</small></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><small><a href="../Page/島式月台.md" title="wikilink">島式月台</a>（<strong>A</strong>月台）</small></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2</strong>號軌道</p></td>
<td><p> 往<a href="../Page/霍博肯總站.md" title="wikilink">霍博肯</a><small>（<a href="../Page/交易廣場車站_(PATH).md" title="wikilink">交易廣場</a>）</small></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><small><a href="../Page/島式月台.md" title="wikilink">島式月台</a>（<strong>B</strong>月台） </small></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>3</strong>號軌道</p></td>
<td><p>  往<a href="../Page/霍博肯總站.md" title="wikilink">霍博肯</a><small>（<a href="../Page/交易廣場車站_(PATH).md" title="wikilink">交易廣場</a>）</small></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>4</strong>號軌道</p></td>
<td><p>  往<a href="../Page/賓夕法尼亞車站_(紐瓦克).md" title="wikilink">紐瓦克</a><small>（<a href="../Page/交易廣場車站_(PATH).md" title="wikilink">交易廣場</a>）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><small><a href="../Page/島式月台.md" title="wikilink">島式月台</a>（<strong>C</strong>月台） </small></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>5</strong>號軌道</p></td>
<td><p>  往<a href="../Page/賓夕法尼亞車站_(紐瓦克).md" title="wikilink">紐瓦克</a><small>（<a href="../Page/交易廣場車站_(PATH).md" title="wikilink">交易廣場</a>）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><small><a href="../Page/側式月台.md" title="wikilink">側式月台</a>（<strong>D</strong>月台） </small></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

車站原初位於科特蘭街及格林尼治街地下，設有兩個[側式月台和兩條軌道](../Page/側式月台.md "wikilink")。重建的車站位於[九一一國家紀念博物館和格林尼治街地下](../Page/九一一國家紀念博物館.md "wikilink")，與原位置相同\[5\]\[6\]。它維持了兩條軌道、兩個側式月台的配置，位於地面以下20英呎\[7\]\[8\]\[9\]。車站牆壁裝設「世界貿易中心」（）站名牌\[10\]。兩個月台北端設有下通道\[11\]。此站也設有空調系統\[12\]。

2018年在此站以100萬美元裝設名為*CHORUS*的藝術品\[13\]。這個藝術品包括若干文件內的著名語錄，包括[世界人權宣言及](../Page/世界人權宣言.md "wikilink")[美國獨立宣言的名言](../Page/美國獨立宣言.md "wikilink")，雕刻在車站牆壁上\[14\]\[15\]\[16\]。

## 参考文献

[Category:1918年啟用的鐵路車站](../Category/1918年啟用的鐵路車站.md "wikilink")
[Category:2001年关闭的铁路车站](../Category/2001年关闭的铁路车站.md "wikilink")
[Category:九一一恐怖襲擊事件](../Category/九一一恐怖襲擊事件.md "wikilink")
[Category:1918年紐約州建立](../Category/1918年紐約州建立.md "wikilink")
[Category:2001年紐約州廢除](../Category/2001年紐約州廢除.md "wikilink")
[Category:2018年啟用的鐵路車站](../Category/2018年啟用的鐵路車站.md "wikilink")

1.

2.
3.
4.
5.

6.
7.
8.
9.
10.

11.

12.

13.

14.
15.
16.