**丘沈**，生卒年不詳，[中國](../Page/中國.md "wikilink")[十六國初期](../Page/十六國.md "wikilink")[西晉境內的民變領袖之一](../Page/西晉.md "wikilink")。

## 生平

本為山都（在今[中國](../Page/中國.md "wikilink")[湖北省](../Page/湖北省.md "wikilink")[襄樊市附近一帶](../Page/襄樊市.md "wikilink")）縣吏，303年因西晉新野王[司馬歆](../Page/司馬歆.md "wikilink")，為政嚴苛急切，大失所轄原住民之人心，適巧正逢朝廷欲徵民兵討[益州](../Page/益州.md "wikilink")（今[四川省一帶](../Page/四川省.md "wikilink")）[李流之變](../Page/李流.md "wikilink")，民兵不願前往，遂聚而為盜，義陽蠻首領[張昌抓住這個機會募為所用](../Page/張昌.md "wikilink")，並起兵反抗晉政府，丘沈遂在此時被[張昌找出](../Page/張昌.md "wikilink")，改名**劉尼**，被假託為[漢朝皇室後裔並擁立為帝](../Page/漢朝.md "wikilink")，建[年號為](../Page/年號.md "wikilink")[神鳳](../Page/神凤_\(张昌\).md "wikilink")。

丘沈雖稱帝，惟此一政權實際的軍政事務皆操於張昌之手，後來司馬歆在與[張昌軍之戰事中陣亡](../Page/張昌.md "wikilink")，成為西晉末期民變紛起後，第一個死於變民的親王。同年，張昌所部為晉將[陶侃所破](../Page/陶侃.md "wikilink")，餘眾悉降。張昌於次年（304年）被擒斬。而丘沈則不知所終。

## 年号

[西晋时期民變首领](../Page/西晋.md "wikilink")[张昌所立年号为](../Page/张昌.md "wikilink")**神凤**（[303年五月](../Page/303年.md "wikilink")-八月），共计4个月。张昌以山东县吏丘沈为天子，更名为刘尼，称国号“汉”。建元神凤。

  - 纪年
    {| border=1 cellspacing=0

|-
style="font-weight:bold;background-color:\#CCCCCC;color:\#000000;text-align:right"
|神凤||元年 |- style="background-color:\#FFFFFF;text-align:center"
|[公元](../Page/公元纪年.md "wikilink")||[303年](../Page/303年.md "wikilink")
|- style="background-color:\#FFFFFF;text-align:center"
|[干支](../Page/干支纪年.md "wikilink")||[癸亥](../Page/癸亥.md "wikilink")
|}

  - 参看

<!-- end list -->

  - [中国年号列表\#西晉](../Page/中国年号列表#西晉.md "wikilink")
  - 其他时期使用[神凤的年号](../Page/神凤.md "wikilink")
  - 同一时期存在的其他政权的年号
      - [太安](../Page/太安_\(晋\).md "wikilink")（[302年十二月](../Page/302年.md "wikilink")-[303年](../Page/303年.md "wikilink")）：[西晋](../Page/西晋.md "wikilink")[惠帝司马衷年号](../Page/晋惠帝.md "wikilink")
      - [建初](../Page/建初_\(李特\).md "wikilink")（[303年](../Page/303年.md "wikilink")-[304年九月](../Page/304年.md "wikilink")）：[成汉政权](../Page/成汉.md "wikilink")[李特的年号](../Page/李特.md "wikilink")

## 資料來源

  - 《[晉書](../Page/晉書.md "wikilink")》卷一百
  - 《[資治通鑑](../Page/資治通鑑.md "wikilink")》卷八十五
  - 李崇智，《中国历代年号考》，中华书局，2004年12月 ISBN 7101025129

[Category:五胡十六國人物](../Category/五胡十六國人物.md "wikilink")
[Category:晉朝民變領袖](../Category/晉朝民變領袖.md "wikilink")
[S](../Category/丘姓.md "wikilink")
[Category:中國下落不明者](../Category/中國下落不明者.md "wikilink")