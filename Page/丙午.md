**丙午**为[干支之一](../Page/干支.md "wikilink")，顺序为第43个。前一位是[乙巳](../Page/乙巳.md "wikilink")，后一位是[丁未](../Page/丁未.md "wikilink")。論[陰陽](../Page/陰陽.md "wikilink")[五行](../Page/五行.md "wikilink")，[天干之丙屬陽之火](../Page/天干.md "wikilink")，[地支之午屬陽之火](../Page/地支.md "wikilink")，是比例和好。然而在日本則有迷信認為丙午的火氣太旺盛，是多災多難的象徵，因此認為丙午年充滿了不吉利。

## 丙午年

中国传统[纪年](../Page/纪年.md "wikilink")[农历的](../Page/农历.md "wikilink")[干支纪年中一个循环的第](../Page/干支纪年.md "wikilink")43年称“**丙午年**”。以下各个[公元](../Page/公元.md "wikilink")[年份](../Page/年.md "wikilink")，年份數除以60餘46，或年份數減3，除以10的餘數是3，除以12的餘數是7，自當年[立春起至次年立春止的歲次内均为](../Page/立春.md "wikilink")“丙午年”：

<table>
<caption><strong>丙午年</strong></caption>
<thead>
<tr class="header">
<th><p><a href="../Page/第1千年.md" title="wikilink">第1千年</a></p></th>
<th><p><a href="../Page/第2千年.md" title="wikilink">第2千年</a></p></th>
<th><p><a href="../Page/第3千年.md" title="wikilink">第3千年</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/46年.md" title="wikilink">46年</a></li>
<li><a href="../Page/106年.md" title="wikilink">106年</a></li>
<li><a href="../Page/166年.md" title="wikilink">166年</a></li>
<li><a href="../Page/226年.md" title="wikilink">226年</a></li>
<li><a href="../Page/286年.md" title="wikilink">286年</a></li>
<li><a href="../Page/346年.md" title="wikilink">346年</a></li>
<li><a href="../Page/406年.md" title="wikilink">406年</a></li>
<li><a href="../Page/466年.md" title="wikilink">466年</a></li>
<li><a href="../Page/526年.md" title="wikilink">526年</a></li>
<li><a href="../Page/586年.md" title="wikilink">586年</a></li>
<li><a href="../Page/646年.md" title="wikilink">646年</a></li>
<li><a href="../Page/706年.md" title="wikilink">706年</a></li>
<li><a href="../Page/766年.md" title="wikilink">766年</a></li>
<li><a href="../Page/826年.md" title="wikilink">826年</a></li>
<li><a href="../Page/886年.md" title="wikilink">886年</a></li>
<li><a href="../Page/946年.md" title="wikilink">946年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/1006年.md" title="wikilink">1006年</a></li>
<li><a href="../Page/1066年.md" title="wikilink">1066年</a></li>
<li><a href="../Page/1126年.md" title="wikilink">1126年</a></li>
<li><a href="../Page/1186年.md" title="wikilink">1186年</a></li>
<li><a href="../Page/1246年.md" title="wikilink">1246年</a></li>
<li><a href="../Page/1306年.md" title="wikilink">1306年</a></li>
<li><a href="../Page/1366年.md" title="wikilink">1366年</a></li>
<li><a href="../Page/1426年.md" title="wikilink">1426年</a></li>
<li><a href="../Page/1486年.md" title="wikilink">1486年</a></li>
<li><a href="../Page/1546年.md" title="wikilink">1546年</a></li>
<li><a href="../Page/1606年.md" title="wikilink">1606年</a></li>
<li><a href="../Page/1666年.md" title="wikilink">1666年</a></li>
<li><a href="../Page/1726年.md" title="wikilink">1726年</a></li>
<li><a href="../Page/1786年.md" title="wikilink">1786年</a></li>
<li><a href="../Page/1846年.md" title="wikilink">1846年</a></li>
<li><a href="../Page/1906年.md" title="wikilink">1906年</a></li>
<li><a href="../Page/1966年.md" title="wikilink">1966年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/2026年.md" title="wikilink">2026年</a></li>
<li><a href="../Page/2086年.md" title="wikilink">2086年</a></li>
<li><a href="../Page/2146年.md" title="wikilink">2146年</a></li>
<li><a href="../Page/2206年.md" title="wikilink">2206年</a></li>
<li><a href="../Page/2266年.md" title="wikilink">2266年</a></li>
<li><a href="../Page/2326年.md" title="wikilink">2326年</a></li>
<li><a href="../Page/2386年.md" title="wikilink">2386年</a></li>
<li><a href="../Page/2446年.md" title="wikilink">2446年</a></li>
<li><a href="../Page/2506年.md" title="wikilink">2506年</a></li>
<li><a href="../Page/2566年.md" title="wikilink">2566年</a></li>
<li><a href="../Page/2626年.md" title="wikilink">2626年</a></li>
<li><a href="../Page/2686年.md" title="wikilink">2686年</a></li>
<li><a href="../Page/2746年.md" title="wikilink">2746年</a></li>
<li><a href="../Page/2806年.md" title="wikilink">2806年</a></li>
<li><a href="../Page/2866年.md" title="wikilink">2866年</a></li>
<li><a href="../Page/2926年.md" title="wikilink">2926年</a></li>
<li><a href="../Page/2986年.md" title="wikilink">2986年</a></li>
</ul></td>
</tr>
</tbody>
</table>

  - 1906年 - [丙午風災](../Page/丙午風災.md "wikilink")

## 丙午月

天干丁年和壬年，[芒種到](../Page/芒種.md "wikilink")[小暑的時間段](../Page/小暑.md "wikilink")，就是**丙午月**：

  - ……
  - [1977年](../Page/1977年.md "wikilink")6月芒種到7月小暑
  - [1982年](../Page/1982年.md "wikilink")6月芒種到7月小暑
  - [1987年](../Page/1987年.md "wikilink")6月芒種到7月小暑
  - [1992年](../Page/1992年.md "wikilink")6月芒種到7月小暑
  - [1997年](../Page/1997年.md "wikilink")6月芒種到7月小暑
  - [2002年](../Page/2002年.md "wikilink")6月芒種到7月小暑
  - [2007年](../Page/2007年.md "wikilink")6月芒種到7月小暑
  - ……

## 丙午日

## 丙午時

天干丁日和壬日，[北京时间](../Page/北京时间.md "wikilink")（[UTC](../Page/协调世界时.md "wikilink")+8）11時到13時，就是**丙午時**。

## 參考資料

1.
[Category:干支](../Category/干支.md "wikilink")
[\*](../Category/丙午年.md "wikilink")