**丙寅**为[干支之一](../Page/干支.md "wikilink")，顺序为第3个。前一位是[乙丑](../Page/乙丑.md "wikilink")，后一位是[丁卯](../Page/丁卯.md "wikilink")。論[陰陽](../Page/陰陽.md "wikilink")[五行](../Page/五行.md "wikilink")，[天干之丙屬陽之火](../Page/天干.md "wikilink")，[地支之寅屬陽之木](../Page/地支.md "wikilink")，是木生火相生。

## 丙寅年

中国传统[纪年](../Page/纪年.md "wikilink")[农历的](../Page/农历.md "wikilink")[干支纪年中一个循环的第](../Page/干支纪年.md "wikilink")3年称“**丙寅年**”。以下各个[公元年份](../Page/公元.md "wikilink")，年份數除以60餘6，或年份數減3，除以10的餘數是3，除以12的餘數是3，自當年[立春起至次年立春止的歲次内均为](../Page/立春.md "wikilink")“丙寅年”：

<table>
<caption><strong>丙寅年</strong></caption>
<thead>
<tr class="header">
<th><p><a href="../Page/第1千年.md" title="wikilink">第1千年</a></p></th>
<th><p><a href="../Page/第2千年.md" title="wikilink">第2千年</a></p></th>
<th><p><a href="../Page/第3千年.md" title="wikilink">第3千年</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/6年.md" title="wikilink">6年</a></li>
<li><a href="../Page/66年.md" title="wikilink">66年</a></li>
<li><a href="../Page/126年.md" title="wikilink">126年</a></li>
<li><a href="../Page/186年.md" title="wikilink">186年</a></li>
<li><a href="../Page/246年.md" title="wikilink">246年</a></li>
<li><a href="../Page/306年.md" title="wikilink">306年</a></li>
<li><a href="../Page/366年.md" title="wikilink">366年</a></li>
<li><a href="../Page/426年.md" title="wikilink">426年</a></li>
<li><a href="../Page/486年.md" title="wikilink">486年</a></li>
<li><a href="../Page/546年.md" title="wikilink">546年</a></li>
<li><a href="../Page/606年.md" title="wikilink">606年</a></li>
<li><a href="../Page/666年.md" title="wikilink">666年</a></li>
<li><a href="../Page/726年.md" title="wikilink">726年</a></li>
<li><a href="../Page/786年.md" title="wikilink">786年</a></li>
<li><a href="../Page/846年.md" title="wikilink">846年</a></li>
<li><a href="../Page/906年.md" title="wikilink">906年</a></li>
<li><a href="../Page/966年.md" title="wikilink">966年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/1026年.md" title="wikilink">1026年</a></li>
<li><a href="../Page/1086年.md" title="wikilink">1086年</a></li>
<li><a href="../Page/1146年.md" title="wikilink">1146年</a></li>
<li><a href="../Page/1206年.md" title="wikilink">1206年</a></li>
<li><a href="../Page/1266年.md" title="wikilink">1266年</a></li>
<li><a href="../Page/1326年.md" title="wikilink">1326年</a></li>
<li><a href="../Page/1386年.md" title="wikilink">1386年</a></li>
<li><a href="../Page/1446年.md" title="wikilink">1446年</a></li>
<li><a href="../Page/1506年.md" title="wikilink">1506年</a></li>
<li><a href="../Page/1566年.md" title="wikilink">1566年</a></li>
<li><a href="../Page/1626年.md" title="wikilink">1626年</a></li>
<li><a href="../Page/1686年.md" title="wikilink">1686年</a></li>
<li><a href="../Page/1746年.md" title="wikilink">1746年</a></li>
<li><a href="../Page/1806年.md" title="wikilink">1806年</a></li>
<li><a href="../Page/1866年.md" title="wikilink">1866年</a></li>
<li><a href="../Page/1926年.md" title="wikilink">1926年</a></li>
<li><a href="../Page/1986年.md" title="wikilink">1986年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/2046年.md" title="wikilink">2046年</a></li>
<li><a href="../Page/2106年.md" title="wikilink">2106年</a></li>
<li><a href="../Page/2166年.md" title="wikilink">2166年</a></li>
<li><a href="../Page/2226年.md" title="wikilink">2226年</a></li>
<li><a href="../Page/2286年.md" title="wikilink">2286年</a></li>
<li><a href="../Page/2346年.md" title="wikilink">2346年</a></li>
<li><a href="../Page/2406年.md" title="wikilink">2406年</a></li>
<li><a href="../Page/2466年.md" title="wikilink">2466年</a></li>
<li><a href="../Page/2526年.md" title="wikilink">2526年</a></li>
<li><a href="../Page/2586年.md" title="wikilink">2586年</a></li>
<li><a href="../Page/2646年.md" title="wikilink">2646年</a></li>
<li><a href="../Page/2706年.md" title="wikilink">2706年</a></li>
<li><a href="../Page/2766年.md" title="wikilink">2766年</a></li>
<li><a href="../Page/2826年.md" title="wikilink">2826年</a></li>
<li><a href="../Page/2886年.md" title="wikilink">2886年</a></li>
<li><a href="../Page/2946年.md" title="wikilink">2946年</a></li>
</ul></td>
</tr>
</tbody>
</table>

  - 1866年 - [丙寅洋擾](../Page/丙寅洋擾.md "wikilink")

## 丙寅月

天干甲年和己年，[立春到](../Page/立春.md "wikilink")[驚蟄的期間](../Page/驚蟄.md "wikilink")，就是**丙寅月**：

  - ……
  - [1974年](../Page/1974年.md "wikilink")2月立春到3月驚蟄
  - [1979年](../Page/1979年.md "wikilink")2月立春到3月驚蟄
  - [1984年](../Page/1984年.md "wikilink")2月立春到3月驚蟄
  - [1989年](../Page/1989年.md "wikilink")2月立春到3月驚蟄
  - [1994年](../Page/1994年.md "wikilink")2月立春到3月驚蟄
  - [1999年](../Page/1999年.md "wikilink")2月立春到3月驚蟄
  - [2004年](../Page/2004年.md "wikilink")2月立春到3月驚蟄
  - ……

## 丙寅日

## 丙寅時

天干甲日和己日，[北京时间](../Page/北京时间.md "wikilink")（[UTC](../Page/协调世界时.md "wikilink")+8）3時到5時，就是**丙寅時**。

## 參考資料

1.
[Category:干支](../Category/干支.md "wikilink")
[\*](../Category/丙寅年.md "wikilink")