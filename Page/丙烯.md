**丙烯**，分子式[C](../Page/碳.md "wikilink")<sub>3</sub>[H](../Page/氫.md "wikilink")<sub>6</sub>，是无色可燃气体，可以通过[石油](../Page/石油.md "wikilink")[裂解而获得](../Page/裂解.md "wikilink")。在各种[烯烴结构中](../Page/烯烴.md "wikilink")，丙烯为仅次于乙烯较为简单的烯烃结构。
在大量運輸時，使用加壓液化。丙烯的[饱和蒸汽压在](../Page/饱和蒸汽压.md "wikilink")25℃時為1158kPa。

## 性质

在[室温和](../Page/室温.md "wikilink")[标准大气压下](../Page/标准大气压.md "wikilink")，丙烯是无色的气体。并且和许多其它[烯烃一样](../Page/烯烃.md "wikilink")，丙烯带有一种较淡但令人不適的气味。\[1\]

由于丙烯的分子量大于[乙烯](../Page/乙烯.md "wikilink")，所以丙烯具有高于乙烯的密度和沸点。丙烯的沸点略低于[丙烷](../Page/丙烷.md "wikilink")，所以丙烯的挥发性略强于丙烷。尽管在丙烯分子中没有强极性键，但由于丙烯分子不是完全的对称，所以丙烯分子仍具有较小的[偶极矩](../Page/偶极矩.md "wikilink")。

丙烯和[环丙烷具有相同的化学式C](../Page/环丙烷.md "wikilink")<sub>3</sub>H<sub>6</sub>，但它们各原子间的连接方式有所不同，所以丙烯与环丙烷是一对[结构异构体](../Page/结构异构.md "wikilink")。

## 自然界的存在形式

丙烯在自然界中以[发酵作用的副产品的形式产生](../Page/发酵.md "wikilink")。同时在[土卫六的低层大气中观测到了微量的丙烯存在](../Page/土卫六.md "wikilink")。\[2\]

## 生产

丙烯是以[矿物燃料](../Page/矿石燃料.md "wikilink")——[石油](../Page/石油.md "wikilink")、[天然气和](../Page/天然气.md "wikilink")[煤作为原料进行生产的](../Page/煤.md "wikilink")。
丙烯是[炼油和](../Page/炼油厂.md "wikilink")[天然气处理的副产物](../Page/天然气处理.md "wikilink")。在炼油工业中，[乙烯](../Page/乙烯.md "wikilink")、丙烯和其它化合物是通过更大的碳氢化合物[裂化的方式生产的](../Page/裂化.md "wikilink")。丙烯也可以通过对[裂化和其它精炼过程得到的产物进行](../Page/裂化.md "wikilink")[分馏得到](../Page/分馏.md "wikilink")。\[3\]

专门针对丙烯的生产方式有如下几个：

  - [烯烃复分解反应类似歧化反应](../Page/烯烃复分解反应.md "wikilink")，不过是一种[可逆反应](../Page/可逆反应.md "wikilink")。通过[乙烯和](../Page/乙烯.md "wikilink")[丁烯断开双键并重新组合来生成丙烯](../Page/丁烯.md "wikilink")。\[4\]目前丙烯的产率可达
    90wt%。当没有丁烯作为原料时，本法仍可以采用。因为乙烯可以二聚生成丁烯再参与反应。

<!-- end list -->

  - 丙烷脱氢法将[丙烷转化为丙烯和副产品](../Page/丙烷.md "wikilink")[氢气](../Page/氢气.md "wikilink")，目前的产率可以达到85m%。而反应的副产物[氢气常用来作为燃料来为](../Page/氢气.md "wikilink")[脱氢反应提供能量](../Page/脱氢反应.md "wikilink")。在丙烷资源丰富的地区常使用丙烷脱氢法制备丙烯，例如[中东](../Page/中东.md "wikilink")。\[5\]

## 用途

### 主要製品

1.  [聚合](../Page/聚合.md "wikilink")
      - [聚丙烯](../Page/聚丙烯.md "wikilink")　－
        [塑膠](../Page/塑膠.md "wikilink")、人工纖維
      - [乙烯-丙烯共聚物](../Page/乙烯-丙烯共聚物.md "wikilink")　－ 合成橡膠
2.  氧化反應
      - [環氧丙烷](../Page/環氧丙烷.md "wikilink")　－ 軟質玻璃纖維
          - [1,2-丙二醇](../Page/1,2-丙二醇.md "wikilink")　－　[聚酯](../Page/聚酯.md "wikilink")（[PET等](../Page/聚對苯二甲酸乙二酯.md "wikilink")）、[界面活性劑](../Page/界面活性劑.md "wikilink")（AE等）
3.  [傅-克烷基化反應](../Page/傅-克反應.md "wikilink")
      - [異丙苯](../Page/異丙苯.md "wikilink")
          - [苯酚](../Page/苯酚.md "wikilink")　－　[酚醛樹脂](../Page/酚醛樹脂.md "wikilink")、[酚甲烷](../Page/酚甲烷.md "wikilink")、染料、醫藥中間體
4.  水合
      - [2-丙醇](../Page/2-丙醇.md "wikilink")
          - [丙酮](../Page/丙酮.md "wikilink")　－　酯化[甲基丙烯酸](../Page/甲基丙烯酸.md "wikilink")、工業中間體、溶劑
5.  [氨氧化法](../Page/氨氧化.md "wikilink")
      - [丙烯腈](../Page/丙烯腈.md "wikilink")　－　合成橡膠、合成樹脂
6.  [氫甲醯化反應](../Page/氫甲醯化反應.md "wikilink")
      - [丁醛](../Page/丁醛.md "wikilink")　－　工業中間體原料
          - [丁醇](../Page/丁醇.md "wikilink")　－　溶劑、香料、塑化劑、醫藥品原料
7.  [冷媒代號R](../Page/冷媒.md "wikilink")1270

<!-- end list -->

  -
    *註) 合成方式皆為對丙烯進行反應，依層次差別為進一步進行反應後的產物並在「－」符號後表示用途。*

### 藥理

丙烯是一種中樞神經抑制劑，過度接觸可能會導致[鎮靜和](../Page/鎮靜.md "wikilink")[遺忘](../Page/遺忘.md "wikilink")，到[昏迷或](../Page/昏迷.md "wikilink")[死亡](../Page/死亡.md "wikilink")，效果同[苯二氮平類藥物過量](../Page/苯二氮平類藥物.md "wikilink")。吸入還可導致[窒息死亡](../Page/窒息.md "wikilink")。

## 参见

{{\#time:2014年8月1日}}[臺灣高雄氣爆事故](../Page/2014年臺灣高雄氣爆事故.md "wikilink")，疑丙烯造成。\[6\]

## 註釋

[Category:烯烃](../Category/烯烃.md "wikilink")
[Category:单体](../Category/单体.md "wikilink")
[Category:三碳有机物](../Category/三碳有机物.md "wikilink")
[Category:可燃气体](../Category/可燃气体.md "wikilink")
[Category:製冷劑](../Category/製冷劑.md "wikilink")

1.  Collins Discovery Encyclopedia, 1st edition © HarperCollins
    Publishers, 2005
2.
3.
4.
5.  Ashford’s Dictionary of Industrial Chemicals, Third edition, 2011,
    ISBN 978-0-9522674-3-0, pages 7766-9
6.