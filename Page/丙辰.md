**丙辰**为[干支之一](../Page/干支.md "wikilink")，顺序为第53个。前一位是[乙卯](../Page/乙卯.md "wikilink")，后一位是[丁巳](../Page/丁巳.md "wikilink")。論[陰陽](../Page/陰陽.md "wikilink")[五行](../Page/五行.md "wikilink")，[天干之丙屬陽之火](../Page/天干.md "wikilink")，[地支之辰屬陽之土](../Page/地支.md "wikilink")，是火生土相生。

## 丙辰年

中国传统[纪年](../Page/纪年.md "wikilink")[农历的](../Page/农历.md "wikilink")[干支纪年中一个循环的第](../Page/干支纪年.md "wikilink")53年称“**丙辰年**”。以下各个[公元](../Page/公元.md "wikilink")[年份](../Page/年.md "wikilink")，年份數除以60餘56，或年份數減3，除以10的餘數是3，除以12的餘數是5，自當年[立春起至次年立春止的歲次内均为](../Page/立春.md "wikilink")“丙辰年”：

<table>
<caption><strong>丙辰年</strong></caption>
<thead>
<tr class="header">
<th><p><a href="../Page/第1千年.md" title="wikilink">第1千年</a></p></th>
<th><p><a href="../Page/第2千年.md" title="wikilink">第2千年</a></p></th>
<th><p><a href="../Page/第3千年.md" title="wikilink">第3千年</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/56年.md" title="wikilink">56年</a></li>
<li><a href="../Page/116年.md" title="wikilink">116年</a></li>
<li><a href="../Page/176年.md" title="wikilink">176年</a></li>
<li><a href="../Page/236年.md" title="wikilink">236年</a></li>
<li><a href="../Page/296年.md" title="wikilink">296年</a></li>
<li><a href="../Page/356年.md" title="wikilink">356年</a></li>
<li><a href="../Page/416年.md" title="wikilink">416年</a></li>
<li><a href="../Page/476年.md" title="wikilink">476年</a></li>
<li><a href="../Page/536年.md" title="wikilink">536年</a></li>
<li><a href="../Page/596年.md" title="wikilink">596年</a></li>
<li><a href="../Page/656年.md" title="wikilink">656年</a></li>
<li><a href="../Page/716年.md" title="wikilink">716年</a></li>
<li><a href="../Page/776年.md" title="wikilink">776年</a></li>
<li><a href="../Page/836年.md" title="wikilink">836年</a></li>
<li><a href="../Page/896年.md" title="wikilink">896年</a></li>
<li><a href="../Page/956年.md" title="wikilink">956年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/1016年.md" title="wikilink">1016年</a></li>
<li><a href="../Page/1076年.md" title="wikilink">1076年</a></li>
<li><a href="../Page/1136年.md" title="wikilink">1136年</a></li>
<li><a href="../Page/1196年.md" title="wikilink">1196年</a></li>
<li><a href="../Page/1256年.md" title="wikilink">1256年</a></li>
<li><a href="../Page/1316年.md" title="wikilink">1316年</a></li>
<li><a href="../Page/1376年.md" title="wikilink">1376年</a></li>
<li><a href="../Page/1436年.md" title="wikilink">1436年</a></li>
<li><a href="../Page/1496年.md" title="wikilink">1496年</a></li>
<li><a href="../Page/1556年.md" title="wikilink">1556年</a></li>
<li><a href="../Page/1616年.md" title="wikilink">1616年</a></li>
<li><a href="../Page/1676年.md" title="wikilink">1676年</a></li>
<li><a href="../Page/1736年.md" title="wikilink">1736年</a></li>
<li><a href="../Page/1796年.md" title="wikilink">1796年</a></li>
<li><a href="../Page/1856年.md" title="wikilink">1856年</a></li>
<li><a href="../Page/1916年.md" title="wikilink">1916年</a></li>
<li><a href="../Page/1976年.md" title="wikilink">1976年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/2036年.md" title="wikilink">2036年</a></li>
<li><a href="../Page/2096年.md" title="wikilink">2096年</a></li>
<li><a href="../Page/2156年.md" title="wikilink">2156年</a></li>
<li><a href="../Page/2216年.md" title="wikilink">2216年</a></li>
<li><a href="../Page/2276年.md" title="wikilink">2276年</a></li>
<li><a href="../Page/2336年.md" title="wikilink">2336年</a></li>
<li><a href="../Page/2396年.md" title="wikilink">2396年</a></li>
<li><a href="../Page/2456年.md" title="wikilink">2456年</a></li>
<li><a href="../Page/2516年.md" title="wikilink">2516年</a></li>
<li><a href="../Page/2576年.md" title="wikilink">2576年</a></li>
<li><a href="../Page/2636年.md" title="wikilink">2636年</a></li>
<li><a href="../Page/2696年.md" title="wikilink">2696年</a></li>
<li><a href="../Page/2756年.md" title="wikilink">2756年</a></li>
<li><a href="../Page/2816年.md" title="wikilink">2816年</a></li>
<li><a href="../Page/2876年.md" title="wikilink">2876年</a></li>
<li><a href="../Page/2936年.md" title="wikilink">2936年</a></li>
<li><a href="../Page/2996年.md" title="wikilink">2996年</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 丙辰月

天干戊年和癸年，[清明到](../Page/清明.md "wikilink")[立夏的時間段](../Page/立夏.md "wikilink")，就是**丙辰月**：

  - ……
  - [1978年](../Page/1978年.md "wikilink")4月清明到5月立夏
  - [1983年](../Page/1983年.md "wikilink")4月清明到5月立夏
  - [1988年](../Page/1988年.md "wikilink")4月清明到5月立夏
  - [1993年](../Page/1993年.md "wikilink")4月清明到5月立夏
  - [1998年](../Page/1998年.md "wikilink")4月清明到5月立夏
  - [2003年](../Page/2003年.md "wikilink")4月清明到5月立夏
  - [2008年](../Page/2008年.md "wikilink")4月清明到5月立夏
  - ……

## 丙辰日

## 丙辰時

天干戊日和癸日，[北京时间](../Page/北京时间.md "wikilink")（[UTC](../Page/协调世界时.md "wikilink")+8）7時到9時，就是**丙辰時**。

## 参考文献

  -
[Category:干支](../Category/干支.md "wikilink")
[丙辰年](../Category/丙辰年.md "wikilink")