**丙酮**也稱作**二甲基酮**、**二甲基甲酮**，简称**二甲酮**，或称**醋酮**、**木酮**，是最简单的[酮](../Page/酮.md "wikilink")，[化學式CH](../Page/化學式.md "wikilink")<sub>3</sub>COCH<sub>3</sub>，為一種有特殊臭味、[薄荷](../Page/薄荷.md "wikilink")[气味的無色](../Page/气味.md "wikilink")[可燃液體](../Page/可燃液體.md "wikilink")。

## 物理化學性质

在常温下为无色透明液体，易挥发、易燃，有芳香气味。與[水](../Page/水.md "wikilink")、[甲醇](../Page/甲醇.md "wikilink")、[乙醇](../Page/乙醇.md "wikilink")、[乙醚](../Page/乙醚.md "wikilink")、[氯仿和](../Page/氯仿.md "wikilink")[吡啶等均能互溶](../Page/吡啶.md "wikilink")，能溶解油、[脂肪](../Page/脂肪.md "wikilink")、[树脂和](../Page/树脂.md "wikilink")[橡胶等](../Page/橡胶.md "wikilink")，也能溶解[醋酸纖維素和](../Page/醋酸纖維素.md "wikilink")[硝酸纖維素](../Page/硝酸纖維素.md "wikilink")，是一種重要的[揮發性](../Page/揮發性.md "wikilink")[有機溶劑](../Page/有機溶劑.md "wikilink")。
[伦敦爆炸案的始作俑者](../Page/伦敦爆炸案.md "wikilink")：丙酮与浓[过氧化氢低温下合成的](../Page/过氧化氢.md "wikilink")[TATP是一种烈性炸药](../Page/TATP.md "wikilink")，摩擦即可爆炸，不产生火光，爆炸机理是瞬间产生大量气体。故作为制爆品被政府管制。也用于溶解毒品。
丙酮可溶解[乙炔](../Page/乙炔.md "wikilink")，一体积的丙酮可以溶解多体积的乙炔，且随压力的增大溶解的量也增大(詳見：[亨利定律](../Page/亨利定律.md "wikilink"))。

## 主要用途

最常見的用途是用作卸除[指甲油的去光水](../Page/指甲油.md "wikilink")，以及[油漆的](../Page/油漆.md "wikilink")[稀釋劑](../Page/稀釋劑.md "wikilink")；同时可作为有机溶剂，应用于医药、[油漆](../Page/油漆.md "wikilink")、[塑料](../Page/塑料.md "wikilink")、[火药](../Page/火药.md "wikilink")、[树脂](../Page/树脂.md "wikilink")、[橡胶](../Page/橡胶.md "wikilink")、[照相软片等行业](../Page/膠卷.md "wikilink")。

在工業上應用於製造[双酚A](../Page/双酚A.md "wikilink")、[甲基丙烯酸甲酯](../Page/甲基丙烯酸甲酯.md "wikilink")（MMA）、[丙酮氰醇](../Page/丙酮氰醇.md "wikilink")、[甲基异丁基酮等产品](../Page/甲基异丁基酮.md "wikilink")，以及[塑膠](../Page/塑膠.md "wikilink")、[纖維](../Page/纖維.md "wikilink")、[藥物及其他](../Page/藥物.md "wikilink")[化學物質](../Page/化學物質.md "wikilink")。自然界中亦存在天然的丙酮，人體內也含有少量的丙酮。在建材方面，主要作为脂肪族减水剂的主要原料。

## 毒性与医学用途

对人体具有肝毒性，對於黏膜有一定的刺激性，吸入其[蒸氣后可引起](../Page/蒸氣.md "wikilink")[头痛](../Page/头痛.md "wikilink")，[支气管炎等症状](../Page/支气管炎.md "wikilink")。如果大量吸入，还可能失去知覺。日常生活中主要用于脱脂，脱水，固定等等。在[血液和](../Page/血液.md "wikilink")[尿液中為重要检测項目](../Page/尿液.md "wikilink")。

有些[癌症患者尿样丙酮水平会异常升高](../Page/癌症.md "wikilink")。采用低碳水化合物食物疗法减肥的人血液、尿液中的丙酮浓度也异常地高。

## 安全

丙酮与[氯仿不应混合](../Page/氯仿.md "wikilink")（特别是在碱性环境下），两者会发生剧烈的放热反应，若混合可能导致强烈爆炸。

## 制备方法

  - 乾餾法

在異丙苯法尚未發明之前，早期丙酮多由[乙酸鈣的乾餾製得](../Page/乙酸鈣.md "wikilink")。

  - 发酵法

用[丁醇](../Page/丁醇.md "wikilink")[酵母](../Page/酵母.md "wikilink")[发酵可以获得丙酮](../Page/发酵.md "wikilink")。此法為[第一次世界大戰期間](../Page/第一次世界大戰.md "wikilink")，由[哈伊姆·魏茨曼所發展](../Page/哈伊姆·魏茨曼.md "wikilink")，但不久就因產率極低而被放棄。

  - [异丙苯氧化合成法](../Page/异丙苯氧化合成法.md "wikilink")

用[石油工业产品](../Page/石油工业.md "wikilink")[异丙苯在](../Page/异丙苯.md "wikilink")[硫酸的催化下被空气](../Page/硫酸.md "wikilink")[氧化](../Page/氧化.md "wikilink")[重排成丙酮](../Page/重排.md "wikilink")，副产物[苯酚](../Page/苯酚.md "wikilink")。该法产生的废品很少，称为“一箭双雕”法。

## 應用

  - 溶剂：例如卸除[指甲油的去光水中的主要](../Page/指甲油.md "wikilink")（或唯一）成份就是丙酮。也是实验室常备的洗涤用溶剂。
  - 试剂：丙酮在合成上是一种C3合成子，可以用于[有机合成](../Page/有机合成.md "wikilink")。另外丙酮也是一种保护基前体，通过生成缩酮来保护1,2-二醇，或者1,3-二醇。
  - 冷劑：丙酮與乾冰的混合物可當冷劑（攝氏−78度）。

## 参考文献

{{-}}

[Category:家用化学品](../Category/家用化学品.md "wikilink")
[Category:化妆品化学品](../Category/化妆品化学品.md "wikilink")
[Category:生物技术产品](../Category/生物技术产品.md "wikilink")
[Category:烷基酮](../Category/烷基酮.md "wikilink")
[Category:酮类溶剂](../Category/酮类溶剂.md "wikilink")
[Category:燃料添加剂](../Category/燃料添加剂.md "wikilink")
[Category:赋形剂](../Category/赋形剂.md "wikilink")
[Category:三碳有机物](../Category/三碳有机物.md "wikilink")
[Category:易制毒化学品](../Category/易制毒化学品.md "wikilink")