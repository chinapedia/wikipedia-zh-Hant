**丙酮酸**（，[化學式](../Page/化學式.md "wikilink")：CH<sub>3</sub>COCOOH）是一種[α-酮酸](../Page/酮酸.md "wikilink")，其[燃点为](../Page/燃点_\(燃烧\).md "wikilink")82 °C，在[生物化學](../Page/生物化学.md "wikilink")[代謝途徑中扮演重要角色](../Page/代謝途徑.md "wikilink")。丙酮酸的[羧酸鹽](../Page/羧酸鹽.md "wikilink")[陰離子](../Page/阴离子.md "wikilink")（carboxylate
anion）被稱之為**丙酮酸鹽**（**pyruvate**，這個字在繁體中文裡也經常簡單地稱作丙酮酸）。

## 作用

丙酮酸在[糖解作用等过程中有重要作用](../Page/糖解作用.md "wikilink")：

它可以在[细胞質液](../Page/细胞質液.md "wikilink")[葡萄糖进行](../Page/葡萄糖.md "wikilink")[糖酵解的过程中](../Page/糖酵解.md "wikilink")，由[磷酸烯醇式丙酮酸产生](../Page/磷酸烯醇式丙酮酸.md "wikilink")。每[mol葡萄糖在此过程中为细胞提供](../Page/摩尔.md "wikilink")2
mol丙酮酸，2 mol [ATP和](../Page/三磷酸腺苷.md "wikilink")2 mol
[NADH](../Page/NADH.md "wikilink")+H<sup>+</sup>。

丙酮酸可以继续进入[克氏循环继续氧化分解](../Page/三羧酸循环.md "wikilink")。从1 mol葡萄糖中所得的2
mol丙酮酸在[线粒体中可以分解为](../Page/線粒體.md "wikilink")6 mol
CO<sub>2</sub>，8 mol NADH+H<sup>+</sup>,2 mol
[FADH](../Page/FADH.md "wikilink")<sub>2</sub>和2 mol
[GTP](../Page/三磷酸鸟苷.md "wikilink")。

在[无氧呼吸中丙酮酸可以转化为](../Page/呼吸作用.md "wikilink")[乳酸或者](../Page/乳酸.md "wikilink")[乙醇](../Page/乙醇.md "wikilink")。（参见[发酵](../Page/发酵.md "wikilink")）。

丙酮酸的代谢产物会随[尿液被排除](../Page/尿.md "wikilink")。

## 分析

定性分析可使用[苯肼](../Page/苯肼.md "wikilink")，α-或β-[萘酚或者](../Page/萘酚.md "wikilink")[2,4-二硝基苯肼](../Page/2,4-二硝基苯肼.md "wikilink")，定量分析可使用[乳酸脱氢酶](../Page/乳酸脱氢酶.md "wikilink")。其代谢产物主要为苯基丙酮酸，随尿液排出。

## 参考文献

## 参见

  - [三羧酸循环](../Page/三羧酸循环.md "wikilink")
  - [糖原](../Page/糖原.md "wikilink")

{{-}}

[Category:糖酵解](../Category/糖酵解.md "wikilink")
[Category:酮酸](../Category/酮酸.md "wikilink")
[Category:细胞呼吸](../Category/细胞呼吸.md "wikilink")
[Category:运动生理学](../Category/运动生理学.md "wikilink")
[Category:三碳有机物](../Category/三碳有机物.md "wikilink")