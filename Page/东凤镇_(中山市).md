**東鳳鎮**是[中國](../Page/中國.md "wikilink")[廣東省](../Page/廣東省.md "wikilink")[中山市下辖的一个](../Page/中山市.md "wikilink")[镇](../Page/镇.md "wikilink")，位于中山市北部，面積54.8平方千米，人口約14萬。

## 歷史

“東鳳”一名取自於近代的三個轄區：東海，鳳儀，鳳鳴。但現在的含義已轉變為一向東的鳳凰，官方則是以一向東（右）的金色鳳凰作為標誌。

東鳳直到[明中至](../Page/明朝.md "wikilink")[清初才開始有人移居於此](../Page/清朝.md "wikilink")，皆因[元代以前東鳳大片地區仍然為汪洋大海](../Page/元代.md "wikilink")，而此後才續漸由於西江支流的沖漬而淤漬成數個沙洲，如大拗沙，罟步沙，伯公沙等，但現在現在己多去掉“沙”字，而成為當地地名。初時移居當地的人多以垦耕為業，原住地以隣近縣鎮為居多，如本市的[小欖](../Page/小欖.md "wikilink")，[黃圃和](../Page/黃圃.md "wikilink")[順德的](../Page/順德.md "wikilink")[大良](../Page/大良.md "wikilink")，[勒流](../Page/勒流.md "wikilink")。後築成多個以”圍”字為後輟的地域。

東鳳歷史上曾多次與隣近鎮區合並和分開自立。15到16世紀初在最早於漬成的[大拗](../Page/大拗.md "wikilink")，[罟步及](../Page/罟步.md "wikilink")[伯公沙洲上始有人垦耕](../Page/伯公.md "wikilink")，當時分屬大欖都和黃旗都。宣統二年（1910年）改屬三，九區。1946年三區轄域改稱[東海鄉](../Page/東海鄉.md "wikilink")，九區轄域稱鳳儀鄉。1950年[鳳儀鄉又分立為鳳儀和鳳鳴兩鄉](../Page/鳳儀鄉.md "wikilink")。1955年並入[南頭區](../Page/南頭區.md "wikilink")。1957年與南頭分立時稱東鳳鄉。1958年10月成立東鳳公社，於當年年底並入[小欖公社](../Page/小欖公社.md "wikilink")，次年又分出自立。後又分立為東鳳和和平兩公社，屬[南頭區](../Page/南頭區.md "wikilink")。1974年又復析出稱東鳳公社。1983年稱區。1986年12月始成立鄉級鎮至今。
[過往小欖水道之貨櫃輪.JPG](https://zh.wikipedia.org/wiki/File:過往小欖水道之貨櫃輪.JPG "fig:過往小欖水道之貨櫃輪.JPG")

## 地域

東鳳鎮大致為一西北──東南走向的地域。分別位於四極的村級行政區為：最東和最南的[東罟步村](../Page/東罟步村.md "wikilink")，最西的[安樂村](../Page/安樂村.md "wikilink")，以及最北的[同安村](../Page/同安村.md "wikilink")。東鳳地處[珠三角西岸](../Page/珠三角.md "wikilink")，[西江下游支流](../Page/西江.md "wikilink")[鷄鴉水道和](../Page/鷄鴉水道.md "wikilink")[小欖水道相夾的淺陸上](../Page/小欖水道.md "wikilink")。為[中山市一僅次於](../Page/中山市.md "wikilink")[黃圃鎮的最北部鄉鎮](../Page/黃圃鎮.md "wikilink")。[中心排河為一西北](../Page/中心排河.md "wikilink")──東南橫貫東鳳中心的河涌。與本市[南頭鎮](../Page/南頭鎮.md "wikilink")，[黃圃鎮](../Page/黃圃鎮.md "wikilink")，[阜沙鎮](../Page/阜沙鎮.md "wikilink")，[東昇鎮](../Page/東昇鎮.md "wikilink")，[小欖鎮以及佛山市](../Page/小欖鎮.md "wikilink")[順德區的](../Page/順德區.md "wikilink")[容桂街辦和](../Page/容桂街辦.md "wikilink")[均安鎮阰隣](../Page/均安鎮.md "wikilink")。

## 行政区划

东凤镇現下轄1個社区和12個村。

  - 社区：[東興社區](../Page/東興社區.md "wikilink")
  - 村：[同安村](../Page/同安村.md "wikilink")，[伯公村](../Page/伯公村.md "wikilink")，[和泰村](../Page/和泰村.md "wikilink")，[東和平村](../Page/東和平村.md "wikilink")，[東罟步村](../Page/東罟步村.md "wikilink")，[永益村](../Page/永益村.md "wikilink")，[安樂村](../Page/安樂村.md "wikilink")，[民樂村](../Page/民樂村.md "wikilink")，[小瀝村](../Page/小瀝村.md "wikilink")，[穗成村](../Page/穗成村.md "wikilink")，[吉昌村](../Page/吉昌村.md "wikilink")，[西罟步村](../Page/西罟步村.md "wikilink")。

## 經濟

[東鳳電子電器城.JPG](https://zh.wikipedia.org/wiki/File:東鳳電子電器城.JPG "fig:東鳳電子電器城.JPG")
東鳳從改革開放始續漸走向以小家電為主導的電子生產業。但近來各種工業也稍有發展。位於轄區內的[美日潔寶有限公司](../Page/美日潔寶有限公司.md "wikilink")，[鐵將軍防盜設備有限公司](../Page/鐵將軍防盜設備有限公司.md "wikilink")，[高雅線圈制品有限公司](../Page/高雅線圈制品有限公司.md "wikilink")，以及於東鳳設立分廠區的[美的集團股份有限公司為較知名的企業](../Page/美的集團股份有限公司.md "wikilink")。

## 市政和民生

東鳳人口約為13萬多，其中戶籍人口7.16萬，非戶籍人口4.12萬（2005年資料）。而當中還不包括没有登記在案的外來人員。

[東鳳公園.JPG](https://zh.wikipedia.org/wiki/File:東鳳公園.JPG "fig:東鳳公園.JPG")
東鳳近年來在河道污染，道路建設以及市政公園綠地上花了一定力度。從新規劃建成了[東鳳公園](../Page/東鳳公園.md "wikilink")，拆御[舊東鳳市場改建為](../Page/舊東鳳市場.md "wikilink")[鳳凰公園](../Page/鳳凰公園.md "wikilink")。以及在多個地點增建了市民健身和休閒場所。隨著經濟的發展，多個地產也逐步發展起來，如位於[小瀝的](../Page/小瀝.md "wikilink")[逸湖半島地產](../Page/逸湖半島地產.md "wikilink")。以及位於[伯公的](../Page/伯公.md "wikilink")[東御世家地產](../Page/東御世家地產.md "wikilink")。但隨著近年來大量外來勞工的擁入，逐漸出現治安惡化的跡象。公安部門己於最近在各個主要公衆地點安裝了治安監察儀器。

東鳳目前還没有大學，但有中學3間，其中初級中學2間，高級職業學校1間，小學12間，其中公辦小學11間，民辦小學1間，以及幼稚園17間。

  - 高級職業中學：[東鳳理工學校](../Page/東鳳理工學校.md "wikilink")
  - 普通初級中學：[東鳳中學](../Page/東鳳中學.md "wikilink")，[東鳳二中](../Page/東鳳二中.md "wikilink")

## 交通

[105國道南北貫穿西北部](../Page/105國道.md "wikilink")，為一車流輛極度煩忙的干道，并曾發生過多起重大交通事故。[364省道穿越中心全境與](../Page/364省道.md "wikilink")[105國道相交會](../Page/105國道.md "wikilink")，以及較多的鄉級道路和街道，其中[興華路為東鳳最為熱閙的街道之一](../Page/興華路.md "wikilink")。東鳳現時没有當地的巴士公司。但由中山市公共汽車公司營運的巴士經往主要路段：

  - 20路車：由[東鳳車站駛往](../Page/東鳳車站.md "wikilink")[黃圃車站](../Page/黃圃車站.md "wikilink")
  - 31路車：由東鳳[同安駛往](../Page/同安.md "wikilink")[南區](../Page/南區_\(中山市\).md "wikilink")[城南客運站](../Page/城南客運站.md "wikilink")
  - 60路車：由[東鳳醫院駛往](../Page/東鳳醫院.md "wikilink")[東罟村](../Page/東罟村.md "wikilink")，是惟一完全在境內行駛之巴士路線。
  - 73路車：由[城南客運站到](../Page/城南客運站.md "wikilink")[黃圃車站](../Page/黃圃車站.md "wikilink")，其中經往東鳳鎮[東阜公路](../Page/東阜公路.md "wikilink")。
  - 210路車：由[黃圃車站至](../Page/黃圃車站.md "wikilink")[中山汽車總站](../Page/中山汽車總站.md "wikilink")，其中東鳳段經往整條[105國道](../Page/105國道.md "wikilink")。

除此之外當地市民日常也使用渡淪過往[小欖水道到逹](../Page/小欖水道.md "wikilink")[小欖鎮和](../Page/小欖鎮.md "wikilink")[東昇鎮](../Page/東昇鎮.md "wikilink")，或[鷄鴉水道到逹](../Page/鷄鴉水道.md "wikilink")[南頭鎮](../Page/南頭鎮.md "wikilink")。而未來建成之[廣珠城際軌道交通也將過境東鳳](../Page/廣珠城際軌道交通.md "wikilink")，但没有設立上落站。

## 旅遊點

[發展中之逸湖半島地產.JPG](https://zh.wikipedia.org/wiki/File:發展中之逸湖半島地產.JPG "fig:發展中之逸湖半島地產.JPG")
東鳳并没有正式發展旅遊業，但某些地點仍然可以稍為提點：

  - [興華路](../Page/興華路.md "wikilink")：為東鳳開發最為早和商業最為集中的街道之一。
  - 巴士沿線：60路車可欣賞到東鳳最純樸之鄉土風貌。

## 參考文獻

1.  《廣東省中山市地名誌》1989年10月第1版，ISBN 7-5359-0539-3

## 外部連結

  - [東鳳鎮政府](https://web.archive.org/web/20090506053909/http://zsdongfeng.gov.cn/)
  - [東鳳小家電](https://web.archive.org/web/20080919020759/http://www.dfxjd.com/)

[Category:中山建制镇](../Category/中山建制镇.md "wikilink")