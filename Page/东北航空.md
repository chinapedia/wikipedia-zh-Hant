**东北航空**（**Northeast
Airlines**），结业前是一家位于[辽宁](../Page/辽宁.md "wikilink")[沈阳的中国航空公司](../Page/沈阳.md "wikilink")，其基地为[沈阳桃仙国际机场](../Page/沈阳桃仙国际机场.md "wikilink")。

## 代码

  - [IATA航空公司代码](../Page/IATA航空公司代码.md "wikilink")：**NS**
  - [ICAO航空公司代码](../Page/ICAO航空公司代码.md "wikilink")：**DBH**
  - 呼号：Northeast Air

## 历史

**中国东北航空**创立于2006年，于2007年11月8日正式开航，是由沈阳中瑞投资有限公司、四川航空集团公司、沈阳市政府国有资产监督管理委员会、辽宁寰江实业有限公司4家股东共同投资组建，投资总额3.6亿元人民币，注册资金1.6亿元人民币。公司总部和主营基地设在沈阳桃仙国际机场，主要经营国内客、货、邮航空运输及相关业务，视情扩展通用航空业务，将逐步发展成为干、支线航空运输相结合，城乡服务于一体的综合性航空企业。公司运营初始阶段投入一架A319、两架ERJ145支线飞机。首期开通的航线为沈阳至[哈尔滨](../Page/哈尔滨.md "wikilink")、[佳木斯](../Page/佳木斯.md "wikilink")、[牡丹江](../Page/牡丹江.md "wikilink")、[齐齐哈尔](../Page/齐齐哈尔.md "wikilink")、[青岛](../Page/青岛.md "wikilink")、[烟台](../Page/烟台.md "wikilink")、[天津等支线航线以及沈阳至](../Page/天津.md "wikilink")[成都](../Page/成都.md "wikilink")、[重庆](../Page/重庆.md "wikilink")、[温州等干线航线](../Page/温州.md "wikilink")。

## 机队

东北航空拥有的机型（截止至2009年4月）：

  - [空中客车A319-100](../Page/空中客车A319.md "wikilink")：1架
  - [ERJ-145](../Page/ERJ-145.md "wikilink")：2架

[category:中國已結業航空公司](../Page/category:中國已結業航空公司.md "wikilink")

[Category:中国航空公司](../Category/中国航空公司.md "wikilink")
[Category:沈阳公司](../Category/沈阳公司.md "wikilink")
[Category:2006年成立的航空公司](../Category/2006年成立的航空公司.md "wikilink")
[Category:冀中能源](../Category/冀中能源.md "wikilink")
[Category:2010年結業航空公司](../Category/2010年結業航空公司.md "wikilink")