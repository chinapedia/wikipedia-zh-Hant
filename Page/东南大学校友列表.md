东南大学的前身是1902年筹办的[三江师范学堂](../Page/三江师范学堂.md "wikilink")，后经历[两江优级师范学堂](../Page/两江优级师范学堂.md "wikilink")、[南京高等师范学校](../Page/南京高等师范学校.md "wikilink")、[国立东南大学](../Page/国立东南大学.md "wikilink")、[第四中山大学](../Page/第四中山大学.md "wikilink")、[江苏大学](../Page/江苏大学.md "wikilink")、[国立中央大学](../Page/国立中央大学.md "wikilink")、[国立南京大学](../Page/国立南京大学.md "wikilink")、[南京大学等历史时期](../Page/南京大学.md "wikilink")。在1952年的院系调整中，以原[中央大学工学院为主体](../Page/中央大学.md "wikilink")，合并[金陵大学等校有关系科](../Page/金陵大学.md "wikilink")，在原中央大学本部成立了[南京工学院](../Page/南京工学院.md "wikilink")。在1952年前的各个历史时期，东南大学涌现出了大量的杰出人才，这个阶段[东南大学的校友名录与由其前身发展而来的其他姐妹学校的校友名录高度重合](../Page/东南大学.md "wikilink")，这些学校包括[南京大学](../Page/南京大学.md "wikilink")、[南京师范大学](../Page/南京师范大学.md "wikilink")、[南京农业大学等](../Page/南京农业大学.md "wikilink")。东大历史上前身有关系科、**东南大学**（包括整体并入的[南京铁道医学院](../Page/南京铁道医学院.md "wikilink")、[南京交通高等专科学校](../Page/南京交通高等专科学校.md "wikilink")、[南京地质学校](../Page/南京地质学校.md "wikilink")）各种学历层次学习过的学生（包括[肄业生](../Page/肄業.md "wikilink")、转校生、[毕业生](../Page/毕业.md "wikilink")、[研究生](../Page/研究生.md "wikilink")、进修生、成人教育、[留学生](../Page/留学生.md "wikilink")）和工作过的教职工，均为东南大学校友。\[1\]

## 南京工学院校友

1952年的院系调整后，曾在及正在东南大学学习或工作的杰出校友包括：

1、建筑学界：中国“建筑五宗师”中的[童寯](../Page/童寯.md "wikilink")、[杨廷宝和](../Page/杨廷宝.md "wikilink")[刘敦桢三位](../Page/刘敦桢.md "wikilink")（另两位为[吕彦直](../Page/吕彦直.md "wikilink")、[梁思成](../Page/梁思成.md "wikilink")），有“中国水彩画开山大师”之称的建筑学家[李剑晨](../Page/李剑晨.md "wikilink")，建筑学家、中科院院士[齐康](../Page/齐康.md "wikilink")，建筑学家、工程院院士[钟训正](../Page/钟训正.md "wikilink")、[程泰宁](../Page/程泰宁.md "wikilink")，美国[麻省理工学院建筑学系主任](../Page/麻省理工学院.md "wikilink")[张永和](../Page/张永和.md "wikilink")，建筑学家、获[普利策克建筑奖的首位中国公民](../Page/普利策克建筑奖.md "wikilink")[王澍等](../Page/王澍.md "wikilink")。

2、电子、信息与计算机学界：著名电子信息学家[陈章](../Page/陈章.md "wikilink")，国际电气电子工程师协会终身院士（IEEE
life
Fellow）[何振亚](../Page/何振亚.md "wikilink")，电子信息专家、中科院院士、电子科技大学原校长[刘盛纲](../Page/刘盛纲.md "wikilink")，电子学家、中国工程院首批院士、教育部原副部长[韦钰](../Page/韦钰.md "wikilink")，半导体及为电子学家、中国科学院院士[陈星弼](../Page/陈星弼.md "wikilink")，电子信息专家、中国工程院院士[张乃通](../Page/张乃通.md "wikilink")、[李幼平](../Page/李幼平.md "wikilink")、[孙忠良](../Page/孙忠良.md "wikilink")、[陈星弼](../Page/陈星弼.md "wikilink")，计算机专家、中国工程院院士、联想公司原总工[倪光南](../Page/倪光南.md "wikilink")，著名计算机科学家、中国工程院院士、东南大学原校长[顾冠群](../Page/顾冠群.md "wikilink")，加拿大工程院院士、国际电气电子工程师协会院士（IEEE
Fellow）[吴柯](../Page/吴柯.md "wikilink")，移动通信专家、2012年国家技术发明一等奖获得者[尤肖虎等](../Page/尤肖虎.md "wikilink")。

3、机械、动力与自动化学界：工程热物理与自动化专家、中国科学院院士、江省政协原主席[钱锺韩](../Page/钱锺韩.md "wikilink")，自动控制学家、中科院院士[冯纯伯](../Page/冯纯伯.md "wikilink")，惯性技术和精密专家、中国工程院院士、国防科工委原主任[丁衡高上将](../Page/丁衡高.md "wikilink")，著名太空技术专家、两院院士[闵桂荣](../Page/闵桂荣.md "wikilink")，能源工程专家、中国工程院院士[徐寿波](../Page/徐寿波.md "wikilink")，著名电力专家、中国工程院士[黄其励](../Page/黄其励.md "wikilink")，军事电子学家、中国工程院院士[李德毅等](../Page/李德毅.md "wikilink")。

4、化学、化工学界：化工专家、中科院院士、中国化工届一代宗师（培养的学生中当选两院院士的达14人）[时钧](../Page/时钧.md "wikilink")，化工专家、工程院院士[唐明述](../Page/唐明述.md "wikilink")，无机非金属材料专家、中国工程院院士[张耀明等](../Page/张耀明（材料专家）.md "wikilink")。

5、土木、交通学界：著名土木工程专家[丁大钧](../Page/丁大钧.md "wikilink")，土木工程材料专家、中国工程院院士[孙伟](../Page/孙伟.md "wikilink")，结构工程专家、中国工程院院士[吕志涛](../Page/吕志涛.md "wikilink")，公路与交通工程专家、中国工程院院士、新疆自治区常务副主席[黄卫](../Page/黄卫.md "wikilink")，建筑材料专家、中国工程院院士[缪昌文等](../Page/缪昌文.md "wikilink")。

6、生命科学与医学界：生化药理学家、中国科学院院士[孙曼霁](../Page/孙曼霁.md "wikilink")，发酵工程专家、中国工程院院士[伦世仪](../Page/伦世仪.md "wikilink")，遗传生物学家、中国科学院院士[贺林](../Page/贺林.md "wikilink")，基因组学家、中国科学院院士、深圳华大基因研究院荣誉董事[杨焕明等](../Page/杨焕明.md "wikilink")。

7、政界：海关总署署长[于广洲](../Page/于广洲.md "wikilink")，交通部原部长、重庆市原市委书记[黄镇东](../Page/黄镇东.md "wikilink")，福建省原省长[黄小晶](../Page/黄小晶.md "wikilink")，江苏省原政协主席[钱锺韩](../Page/钱锺韩.md "wikilink")，中国地震局原局长[宋瑞祥](../Page/宋瑞祥.md "wikilink")，教育部原副部长[韦钰](../Page/韦钰.md "wikilink")，新疆自治区常务副主席[黄卫](../Page/黄卫.md "wikilink")，江苏省委常委、南京市委书记[杨卫泽](../Page/杨卫泽.md "wikilink")，江苏省副省长[许津荣](../Page/许津荣.md "wikilink")，广西壮族自治区副主席[李康](../Page/李康.md "wikilink")，原湖南省委常委、纪委书记[许云昭](../Page/许云昭.md "wikilink")、澳大利亞參議員[王振亞等](../Page/王振亞.md "wikilink")。

8、教育界：北京师范大学原校长[钟秉林](../Page/钟秉林.md "wikilink")，中山大学原党委书记[李延保](../Page/李延保.md "wikilink")，北京航空航天大学党委书记[胡凌云](../Page/胡凌云.md "wikilink")，北京交通大学原校长[谈振辉](../Page/谈振辉.md "wikilink")，河海大学原党委书记[林萍华](../Page/林萍华.md "wikilink")，南京工业大学、苏州大学原党委书记[王卓君](../Page/王卓君.md "wikilink")，南京信息工程大学校长兼党委书记[李廉水等](../Page/李廉水.md "wikilink")。

9、商界：苏宁集团董事长[张桂平](../Page/张桂平.md "wikilink")，深圳新天下实业有限公司董事长[吴海军](../Page/吴海军.md "wikilink")，杰赛科技董事长[何可玉](../Page/何可玉.md "wikilink")，爱迪尔集团董事长[闵瑜](../Page/闵瑜.md "wikilink")，红太阳集团董事长[杨寿海](../Page/杨寿海.md "wikilink")，金蝶软件集团总裁[徐少春](../Page/徐少春.md "wikilink")，湖南剀达集团董事长[乐根成](../Page/乐根成.md "wikilink")，江苏光一科技公司董事长[龙昌明](../Page/龙昌明.md "wikilink")，北京正略钧策管理咨询集团董事长[赵民](../Page/赵民.md "wikilink")，飞利浦电子中国集团副总裁[季铁安等](../Page/季铁安.md "wikilink")。

## 参见

  - [东南大学](../Page/东南大学.md "wikilink")

## 参考文献

<references />

[\*](../Category/东南大学校友.md "wikilink")
[Category:中国高校校友列表](../Category/中国高校校友列表.md "wikilink")

1.  [东南大学校友总会章程](http://seuaa.seu.edu.cn/s/14/t/1002/06/7a/info1658.htm)