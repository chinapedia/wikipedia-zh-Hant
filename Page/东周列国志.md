《**東周列國志**》是一部長篇[歷史](../Page/歷史.md "wikilink")[章回小说](../Page/章回小说.md "wikilink")，為[明末](../Page/明朝.md "wikilink")[余邵鱼](../Page/余邵鱼.md "wikilink")、[馮夢龍所撰](../Page/馮夢龍.md "wikilink")，[清代的](../Page/清代.md "wikilink")[蔡元放编評](../Page/蔡元放.md "wikilink")。是一部在中國除了《[三国演义](../Page/三国演义.md "wikilink")》之外流传最广、影响最大的[通俗历史演义](../Page/通俗历史演义.md "wikilink")。內容涵蓋[春秋](../Page/春秋.md "wikilink")、[戰國時代約](../Page/戰國.md "wikilink")500餘年的[東周時期歷史](../Page/東周.md "wikilink")，包含[春秋五霸](../Page/春秋五霸.md "wikilink")、[吳越之爭以及](../Page/吳越春秋.md "wikilink")[戰國七雄等許多著名的歷史故事](../Page/戰國七雄.md "wikilink")。

## 成书年代

此书刊本很多，卷首有蔡元放序，所署年代不一，或作乾隆元年（1736年），或作乾隆十七年（1752年），或作乾隆三十二年（1767年）。

## 成书过程

### 余邵鱼撰《列国志传》

[明朝嘉靖](../Page/明朝.md "wikilink")、隆庆年间，[余邵鱼编撰了一部](../Page/余邵鱼.md "wikilink")《列国志传》，8卷226则，约28万字。全书始于[武王伐纣](../Page/武王伐纣.md "wikilink")，终于[秦併六國](../Page/秦併六國.md "wikilink")。这部《列国志传》自称“编年取法麟经，记事一据实录”，但其中仍有许多民间传说故事。

### 冯梦龙编《新列国志》

明朝末年，[冯梦龙在](../Page/冯梦龙.md "wikilink")《列国志传》的基础上进行改编，形成了《新列国志》一书，共108回，增至70餘万字。冯梦龙依据《[左传](../Page/左传.md "wikilink")》、《[史记](../Page/史记.md "wikilink")》等书，删除了旧本《列国志传》中明显不符合史实的故事传说，同时增添了不少重要内容。在改编中，他还删去了武王伐纣到西周衰亡这部分内容，集中写[春秋](../Page/春秋.md "wikilink")、[战国](../Page/战国.md "wikilink")，即[东周的历史](../Page/东周.md "wikilink")。

### 蔡元放編評《东周列国志》

清代[乾隆年间](../Page/乾隆.md "wikilink")，[蔡元放对](../Page/蔡元放.md "wikilink")《新列国志》略加修订润色，并加上序、读法，大量的评语及一些夹注，易名为《东周列国志》，共27卷108回。此书只能说是《新列国志》的评点本，但是近200年来它成了最流行的本子，使《列国志传》和《新列国志》都鲜为人知了。

## 内容

《东周列国志》从[西周末年](../Page/西周.md "wikilink")[周宣王三十九年](../Page/周宣王.md "wikilink")（前789年）写起，到[秦始皇二十六年前](../Page/秦始皇.md "wikilink")221年统一全国为止，包括春秋战国500多年历史，内容极为丰富复杂。作者在描写[春秋五霸](../Page/春秋五霸.md "wikilink")、[战国七雄的兴亡盛衰过程中](../Page/战国七雄.md "wikilink")，对那些腐朽残暴、骄奢淫侈的统治者，如[周幽王](../Page/周幽王.md "wikilink")、[齐襄公](../Page/齐襄公.md "wikilink")、[陈灵公](../Page/陈灵公.md "wikilink")、[宋康王等进行了揭露和否定](../Page/宋康王.md "wikilink")；对改变政治、促进社会发展的开明君主和官吏，如[齐桓公和](../Page/齐桓公.md "wikilink")[管仲](../Page/管仲.md "wikilink")、[魏文侯和](../Page/魏文侯.md "wikilink")[西门豹](../Page/西门豹.md "wikilink")、[秦孝公和](../Page/秦孝公.md "wikilink")[商鞅等](../Page/商鞅.md "wikilink")，予以肯定和赞扬。这是和目前广泛接受的历史评价是一致的。

## 特点和价值

由于[冯梦龙和](../Page/冯梦龙.md "wikilink")[蔡元放都非常强调历史演义必须忠于史实](../Page/蔡元放.md "wikilink")，所以《东周列国志》的史学价值很高，影响相当大。[蔡元放自己就在](../Page/蔡元放.md "wikilink")《读法》中说过：“读《列国志》，全要把作正史看，莫作小说一例看了。”

## 目錄

  - 第1回　[周宣王聞謠輕殺](../Page/周宣王.md "wikilink")
    [杜大夫化厲鳴冤](../Page/杜伯.md "wikilink")
  - 第2回　褒人贖罪獻[美女](../Page/褒姒.md "wikilink")
    [幽王烽火戲諸侯](../Page/周幽王.md "wikilink")
  - 第3回　犬戎主大鬧[鎬京](../Page/鎬京.md "wikilink")
    [周平王東遷](../Page/周平王.md "wikilink")[洛邑](../Page/洛邑.md "wikilink")
  - 第4回　[秦文公郊天應夢](../Page/秦文公.md "wikilink")
    [鄭莊公掘地見母](../Page/鄭莊公.md "wikilink")
  - 第5回　寵虢公[周鄭交質](../Page/周鄭交質.md "wikilink") 助衛逆魯宋興兵
  - 第6回　衛[石碏大義滅親](../Page/石碏.md "wikilink")
    [鄭莊公假命伐宋](../Page/鄭莊公.md "wikilink")
  - 第7回　公孫閼爭車射考叔　[公子翬獻諂賊](../Page/公子翬.md "wikilink")[隱公](../Page/魯隱公.md "wikilink")
  - 第8回　立新君[華督行賂](../Page/華督.md "wikilink") 敗戎兵鄭忽辭婚
  - 第9回　齊侯送[文姜婚魯](../Page/文姜.md "wikilink") 祝聃射周王中肩
  - 第10回　楚[熊通僭號稱王](../Page/熊通.md "wikilink")
    鄭[祭足被脅立庶](../Page/祭足.md "wikilink")
  - 第11回　[宋莊公貪賂搆兵](../Page/宋莊公.md "wikilink") 鄭祭足殺婿逐主
  - 第12回　[衛宣公築臺納媳](../Page/衛宣公.md "wikilink")
    [高渠彌乘間易君](../Page/高渠彌.md "wikilink")
  - 第13回　[魯桓公夫婦如齊](../Page/魯桓公.md "wikilink")
    [鄭子亹君臣為戮](../Page/鄭子亹.md "wikilink")
  - 第14回　[衛侯朔抗王入國](../Page/衛惠公.md "wikilink")
    [齊襄公出獵遇鬼](../Page/齊襄公.md "wikilink")
  - 第15回　雍大夫計殺無知 [魯莊公乾時大戰](../Page/魯莊公.md "wikilink")
  - 第16回　釋檻囚[鮑叔薦仲](../Page/鮑叔牙.md "wikilink")
    戰長勺[曹劌敗齊](../Page/曹劌.md "wikilink")
  - 第17回　宋國納賂誅[長萬](../Page/南宮長萬.md "wikilink")
    楚王杯酒虜[息媯](../Page/息媯.md "wikilink")
  - 第18回　[曹沫手劍劫齊侯](../Page/曹沫.md "wikilink") 桓公舉火爵寧戚
  - 第19回　擒傅瑕厲公復國 殺[子頹惠王反正](../Page/王子頹.md "wikilink")
  - 第20回　[晉獻公違卜立驪姬](../Page/晉獻公.md "wikilink")　[楚成王平亂相子文](../Page/楚成王.md "wikilink")
  - 第21回　[管夷吾智辨俞兒](../Page/管夷吾.md "wikilink")
    [齊桓公兵定](../Page/齊桓公.md "wikilink")[孤竹](../Page/孤竹.md "wikilink")
  - 第22回　[公子友兩定魯君](../Page/季友.md "wikilink") 齊皇子獨對委蛇
  - 第23回　[衛懿公好鶴亡國](../Page/衛懿公.md "wikilink") 齊桓公興兵伐楚
  - 第24回　盟召陵禮款楚大夫　會葵邱義戴周天子
  - 第25回　智[荀息假途滅虢](../Page/荀息.md "wikilink") 窮百里飼牛拜相
  - 第26回　歌扊扅百里認妻 獲陳寶穆公証夢
  - 第27回　[驪姬巧計殺](../Page/驪姬.md "wikilink")[申生](../Page/申生.md "wikilink")
    獻公臨終囑荀息
  - 第28回　[里克兩弒孤主](../Page/里克.md "wikilink") 穆公一平晉亂
  - 第29回　[晉惠公大誅群臣](../Page/晉惠公.md "wikilink") 管夷吾病榻論相
  - 第30回　秦晉大戰龍門山 [穆姬登臺耍大赦](../Page/穆姬.md "wikilink")
  - 第31回　晉惠公怒殺慶鄭 [介子推割股啖君](../Page/介子推.md "wikilink")
  - 第32回　晏蛾兒踰牆殉節 群公子大鬧朝堂
  - 第33回　宋公伐齊納子昭 楚人伏兵劫盟主
  - 第34回　[宋襄公假仁失眾](../Page/宋襄公.md "wikilink") 齊姜氏乘醉遣夫
  - 第35回　晉重耳周遊列國 秦[懷嬴重婚公子](../Page/懷嬴.md "wikilink")
  - 第36回　晉呂郤夜焚公宮 [秦穆公再平晉亂](../Page/秦穆公.md "wikilink")
  - 第37回　[介子推守志焚綿上](../Page/介子推.md "wikilink")　[太叔帶怙寵入宮中](../Page/甘昭公.md "wikilink")
  - 第38回　[周襄王避亂居鄭](../Page/周襄王.md "wikilink")
    [晉文公守信降原](../Page/晉文公.md "wikilink")
  - 第39回　[柳下惠授詞卻敵](../Page/柳下惠.md "wikilink") 晉文公伐衛破曹
  - 第40回　[先軫詭謀激](../Page/先軫.md "wikilink")[子玉](../Page/子玉.md "wikilink")
    晉楚城濮大交兵
  - 第41回　連穀城子玉自殺 踐土壇晉侯主盟
  - 第42回　[周襄王河陽受覲](../Page/周襄王.md "wikilink")
    衛[元咺公館對獄](../Page/元咺.md "wikilink")
  - 第43回　智[寧俞假酖復衛](../Page/寧俞.md "wikilink") 老燭武縋城說秦
  - 第44回　[叔詹據鼎抗晉侯](../Page/叔詹.md "wikilink")
    [弦高假命犒秦軍](../Page/弦高.md "wikilink")
  - 第45回　[晉襄公墨縗敗秦](../Page/晉襄公.md "wikilink") 先元帥免冑殉翟
  - 第46回　楚[商臣宮中弒父](../Page/商臣.md "wikilink") 秦穆公殽谷封尸
  - 第47回　弄玉吹簫雙跨鳳 [趙盾背秦立靈公](../Page/趙盾.md "wikilink")
  - 第48回　刺[先克五將亂晉](../Page/先克.md "wikilink")
    召[士會壽餘紿秦](../Page/士會.md "wikilink")
  - 第49回　[公子鮑厚施買國](../Page/宋文公.md "wikilink")
    [齊懿公竹池遇變](../Page/齊懿公.md "wikilink")
  - 第50回　[東門遂援立子倭](../Page/東門遂.md "wikilink")
    [趙宣子桃園強諫](../Page/趙宣子.md "wikilink")
  - 第51回　責趙盾[董狐直筆](../Page/董狐.md "wikilink")
    誅[鬬椒絕纓大會](../Page/鬬椒.md "wikilink")
  - 第52回　公子宋嘗黿搆逆 [陳靈公衵服戲朝](../Page/陳靈公.md "wikilink")
  - 第53回　[楚莊王納諫復陳](../Page/楚莊王.md "wikilink")
    [晉景公出師救鄭](../Page/晉景公.md "wikilink")
  - 第54回　[荀林父縱屬亡師](../Page/荀林父.md "wikilink") 孟侏儒託優悟主
  - 第55回　[華元登床劫](../Page/華元.md "wikilink")[子反](../Page/子反.md "wikilink")
    老人結草亢杜回
  - 第56回　蕭夫人登臺笑客 逢丑父易服免君
  - 第57回　娶[夏姬](../Page/夏姬.md "wikilink")[巫臣逃晉](../Page/巫臣.md "wikilink")
    圍下宮[程嬰匿孤](../Page/程嬰.md "wikilink")
  - 第58回　說秦伯[魏相迎醫](../Page/呂相.md "wikilink")
    報[魏錡養叔獻藝](../Page/魏錡.md "wikilink")
  - 第59回　寵[胥童晉國大亂](../Page/胥童.md "wikilink") 誅岸賈趙氏復興
  - 第60回　[智武子分軍肆敵](../Page/智武子.md "wikilink") 偪陽城三將鬥力
  - 第61回　[晉悼公駕楚會蕭魚](../Page/晉悼公.md "wikilink")　[孫林父因歌逐獻公](../Page/孫林父.md "wikilink")
  - 第62回　諸侯同心圍齊國 晉臣合計逐[欒盈](../Page/欒盈.md "wikilink")
  - 第63回　老[祁奚力救羊舌](../Page/祁奚.md "wikilink")
    小[范鞅智劫](../Page/范鞅.md "wikilink")[魏舒](../Page/魏舒.md "wikilink")
  - 第64回　曲沃城欒盈滅族 且於門杞梁死戰
  - 第65回　弒齊光崔慶專權 納衛衎[寧喜擅政](../Page/寧喜.md "wikilink")
  - 第66回　殺寧喜子鱄出奔
    戮[崔杼](../Page/崔杼.md "wikilink")[慶封獨相](../Page/慶封.md "wikilink")
  - 第67回　[盧蒲癸計逐慶封](../Page/盧蒲癸.md "wikilink")
    [楚靈王大合諸侯](../Page/楚靈王.md "wikilink")
  - 第68回　賀虒祁[師曠辨新聲](../Page/師曠.md "wikilink")　散家財陳氏買齊國
  - 第69回　[楚靈王挾詐滅陳蔡](../Page/楚靈王.md "wikilink")　[晏平仲巧辯服荊蠻](../Page/晏平仲.md "wikilink")
  - 第70回　殺三兄[楚平王即位](../Page/楚平王.md "wikilink")　劫齊魯[晉昭公尋盟](../Page/晉昭公.md "wikilink")
  - 第71回　晏平仲二桃殺三士　[楚平王娶媳逐世子](../Page/楚平王.md "wikilink")
  - 第72回　棠公尚捐軀奔父難　[伍子胥微服過昭關](../Page/伍子胥.md "wikilink")
  - 第73回　伍員吹簫乞吳市 [專諸進炙刺王僚](../Page/專諸.md "wikilink")
  - 第74回　[囊瓦懼謗誅無極](../Page/囊瓦.md "wikilink")
    [要離貪名刺](../Page/要離.md "wikilink")[慶忌](../Page/慶忌.md "wikilink")
  - 第75回　[孫武子演陣斬美姬](../Page/孫武子.md "wikilink")　[蔡昭侯納質乞吳師](../Page/蔡昭侯.md "wikilink")
  - 第76回　[楚昭王棄郢西奔](../Page/楚昭王.md "wikilink") 伍子胥掘墓鞭屍
  - 第77回　泣秦庭[申包胥借兵](../Page/申包胥.md "wikilink")　退吳師楚昭王返國
  - 第78回　會夾谷[孔子卻齊](../Page/孔子.md "wikilink") 墮三都聞人伏法
  - 第79回　歸女樂黎彌阻孔子　棲會稽[文種通宰嚭](../Page/文種.md "wikilink")
  - 第80回　[夫差違諫釋越](../Page/夫差.md "wikilink")
    [句踐竭力事吳](../Page/句踐.md "wikilink")
  - 第81回　美人計吳宮寵西施　言語科[子貢說列國](../Page/子貢.md "wikilink")
  - 第82回　殺子胥夫差爭歃 納蒯瞶[子路結纓](../Page/子路.md "wikilink")
  - 第83回　誅羋勝葉公定楚 滅夫差越王稱霸
  - 第84回　[智伯決水灌晉陽](../Page/智伯.md "wikilink")
    [豫讓擊衣報襄子](../Page/豫讓.md "wikilink")
  - 第85回　[樂羊子怒餟中山羹](../Page/樂羊.md "wikilink")　[西門豹喬送河伯婦](../Page/西門豹.md "wikilink")
  - 第86回　[吳起殺妻求將](../Page/吳起.md "wikilink")
    [騶忌鼓琴取相](../Page/騶忌.md "wikilink")
  - 第87回　說秦君[衛鞅變法](../Page/衛鞅.md "wikilink")
    辭鬼谷[孫臏下山](../Page/孫臏.md "wikilink")
  - 第88回　孫臏佯狂脫禍 [龐涓兵敗桂陵](../Page/龐涓.md "wikilink")
  - 第89回　馬陵道萬弩射龐涓　咸陽市五牛分商鞅
  - 第90回　[蘇秦合從相六國](../Page/蘇秦.md "wikilink")
    [張儀被激往秦邦](../Page/張儀.md "wikilink")
  - 第91回　學讓國燕噲召兵 偽獻地張儀欺楚
  - 第92回　賽舉鼎[秦武王絕脛](../Page/秦武王.md "wikilink")　莽赴會[楚懷王陷秦](../Page/楚懷王.md "wikilink")
  - 第93回　[趙主父餓死沙邱宮](../Page/趙主父.md "wikilink")　[孟嘗君偷過函谷關](../Page/孟嘗君.md "wikilink")
  - 第94回　[馮驩彈鋏客孟嘗](../Page/馮驩.md "wikilink") 齊王糾兵伐桀宋
  - 第95回　說四國[樂毅滅齊](../Page/樂毅.md "wikilink")
    驅火牛[田單破燕](../Page/田單.md "wikilink")
  - 第96回　[藺相如兩屈秦王](../Page/藺相如.md "wikilink") 馬服君單解韓圍
  - 第97回　死[范睢計逃秦國](../Page/范睢.md "wikilink") 假張祿廷辱魏使
  - 第98回　質平原秦王索魏齊　敗長平[白起坑趙卒](../Page/白起.md "wikilink")
  - 第99回　武安君含冤死杜郵　[呂不韋巧計歸異人](../Page/呂不韋.md "wikilink")
  - 第100回　[魯仲連不肯帝秦](../Page/魯仲連.md "wikilink")
    [信陵君竊符救趙](../Page/信陵君.md "wikilink")
  - 第101回　秦王滅周遷九鼎 [廉頗敗燕殺二將](../Page/廉頗.md "wikilink")
  - 第102回　華陰道信陵敗蒙驁　胡盧河龐煖斬[劇辛](../Page/劇辛.md "wikilink")
  - 第103回　李國舅爭權除黃歇　[樊於期傳檄討秦王](../Page/樊於期.md "wikilink")
  - 第104回　[甘羅童年取高位](../Page/甘羅.md "wikilink")
    [嫪毐偽腐亂秦宮](../Page/嫪毐.md "wikilink")
  - 第105回　茅焦解衣諫秦王 [李牧堅壁卻桓齮](../Page/李牧.md "wikilink")
  - 第106回　王敖反間殺李牧 [田光刎頸薦荊軻](../Page/田光.md "wikilink")
  - 第107回　獻地圖[荊軻鬧秦庭](../Page/荊軻.md "wikilink")　論兵法[王翦代](../Page/王翦.md "wikilink")[李信](../Page/李信.md "wikilink")
  - 第108回　兼六國混一輿圖 號[始皇建立郡縣](../Page/始皇.md "wikilink")

## 外部链接

  - [名著在线网站《东周列国志》在线阅读](http://www.zggdwx.com/dongzhou.html)
  - [《東周列國志》全文免費線上閱覽](http://lieguozhi.blogspot.tw/)

[Category:子部小說家類](../Category/子部小說家類.md "wikilink")
[Category:清朝小說](../Category/清朝小說.md "wikilink")
[Category:白話古典小說](../Category/白話古典小說.md "wikilink")
[Category:東周背景小說](../Category/東周背景小說.md "wikilink")
[Category:西周背景小說](../Category/西周背景小說.md "wikilink")
[Category:秦朝背景小說](../Category/秦朝背景小說.md "wikilink")