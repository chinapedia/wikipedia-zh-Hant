**东安路站**位于[上海](../Page/上海.md "wikilink")[徐汇区](../Page/徐汇区.md "wikilink")[零陵路](../Page/零陵路.md "wikilink")[东安路](../Page/东安路_\(上海\).md "wikilink")，为[上海轨道交通4号线和](../Page/上海轨道交通4号线.md "wikilink")[上海轨道交通7号线的](../Page/上海轨道交通7号线.md "wikilink")[换乘站](../Page/换乘站.md "wikilink")，两线共用站厅，其中4号线位于地下三层，7号线位于地下四层，均为地下[岛式車站](../Page/岛式站台.md "wikilink")，两线可通过站台直接换乘。

## 周边

  - [复旦大学附属肿瘤医院](../Page/复旦大学附属肿瘤医院.md "wikilink")
  - [上海中医药大学附属龙华医院](../Page/上海中医药大学附属龙华医院.md "wikilink")
  - [上海市南洋模范中学](../Page/上海市南洋模范中学.md "wikilink")

## 車站設計

[Dong‘an_Road_Station_Line7_Shanghai_Metro.JPG](https://zh.wikipedia.org/wiki/File:Dong‘an_Road_Station_Line7_Shanghai_Metro.JPG "fig:Dong‘an_Road_Station_Line7_Shanghai_Metro.JPG")
七号线车站为地下[岛式站台](../Page/岛式站台.md "wikilink")。

## 公交换乘

41、41（区间）、44、49、50、72、89、104、144、167、171、178大站车、205、218、301、303、326、572、572（区间）、712、733、734、864、932、933、938、隧道二线、隧道八线、大桥六线、大桥六线（区间）、隧道夜宵一线、徐闵夜宵线、万周专线、徐川专线

## 车站出口

  - 1号口：零陵路南侧，东安路东
  - 2号口：零陵路南侧，东安路西
  - 3号口：零陵路北侧，东安路西
  - 4号口：东安路东侧，零陵路北
  - 5号口：东安路东侧，零陵路南

[Category:徐汇区地铁车站](../Category/徐汇区地铁车站.md "wikilink")
[Category:2005年启用的铁路车站](../Category/2005年启用的铁路车站.md "wikilink")
[Category:以街道命名的上海軌道交通車站](../Category/以街道命名的上海軌道交通車站.md "wikilink")