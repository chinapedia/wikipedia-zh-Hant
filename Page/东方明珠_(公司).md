**上海东方明珠新媒体股份有限公司**（）成立於1992年，前身為「上海東方明珠（集團）股份有限公司」，上市前身代碼為600832.SS，從事[文化](../Page/文化.md "wikilink")[休閑](../Page/休閑.md "wikilink")[娱樂](../Page/娱樂.md "wikilink")（包括建設及經營[東方明珠塔](../Page/東方明珠塔.md "wikilink")、[旅遊](../Page/旅遊.md "wikilink")、[餐飲](../Page/餐飲.md "wikilink")、高檔[消費](../Page/消費.md "wikilink")）、[媒體](../Page/媒體.md "wikilink")（移動[電視](../Page/電視.md "wikilink")、[手機電視](../Page/手機.md "wikilink")）、對外[投資](../Page/投資.md "wikilink")（[股票](../Page/股票.md "wikilink")、[物流](../Page/物流.md "wikilink")、[房地產](../Page/房地產.md "wikilink")、[教育](../Page/教育.md "wikilink")[設施](../Page/設施.md "wikilink")、[物業管理](../Page/物業管理.md "wikilink")）等業務，是[上海市人民政府列入的](../Page/上海市人民政府.md "wikilink")50家重點大型企業之一。母公司是[上海文化廣播影視集團](../Page/上海文化廣播影視集團.md "wikilink")。

## 历史

2014年5月25日，东方明珠宣布，旗下子公司与[索尼电脑娱乐将在](../Page/索尼电脑娱乐.md "wikilink")[上海自由贸易区成立](../Page/上海自由贸易区.md "wikilink")2家[合资公司](../Page/合资公司.md "wikilink")，运作[PlayStation
4在中国大陆地区的硬件与软件服务](../Page/PlayStation_4.md "wikilink")。\[1\]

2015年5月，[百视通启动吸收合并东方明珠的程序](../Page/百视通.md "wikilink")，原“东方明珠”股票（代码600832）于5月20日摘牌停止交易\[2\]。6月19日，百视通已完成重大资产重组所有审批程序与交割程序，公司名称更名为“东方明珠”，并在上海证券交易所挂牌上市\[3\]。原百视通成为东方明珠多媒体旗下子公司。

2015年7月17日至2015年8月14日，东方明珠经[上海文化产权交易所公开挂牌出售](../Page/上海文化产权交易所.md "wikilink")[风行网](../Page/风行网.md "wikilink")63%股权，受让方为深圳市[兆驰股份有限公司](../Page/兆驰股份.md "wikilink")。出售后，东方明珠仍持有风行网19.76%的股权\[4\]。

## 旗下资产

  - [尚世影业](../Page/尚世影业.md "wikilink")
  - [五岸传播](../Page/五岸传播.md "wikilink")
  - [百视通](../Page/百视通.md "wikilink")
  - SiTV
  - [广视通](../Page/广视通.md "wikilink")
  - [风行网](../Page/风行网.md "wikilink")19.76%
  - [东方明珠广播电视塔](../Page/东方明珠广播电视塔.md "wikilink")
  - [上海国际会议中心](../Page/上海国际会议中心.md "wikilink")
  - [七重天宾馆](../Page/七重天宾馆.md "wikilink")
  - [东方绿舟](../Page/东方绿舟.md "wikilink")
  - 东方绿舟度假村
  - 东方绿舟宾馆
  - 东方明珠国际旅行社
  - 城市之光灯光设计
  - 东方明珠文化交流公司
  - [梅赛德斯-奔驰文化中心](../Page/梅赛德斯-奔驰文化中心.md "wikilink")
  - [东方购物](../Page/东方购物.md "wikilink")
  - 艾德思奇
  - 百家合
  - 好十传媒

## 参考资料

## 連結

  - [上海东方明珠新媒体股份有限公司](http://www.oriental-pearl.cn/)

[Category:1992年成立的公司](../Category/1992年成立的公司.md "wikilink")
[Category:总部位于上海的中华人民共和国国有企业](../Category/总部位于上海的中华人民共和国国有企业.md "wikilink")
[Category:上海旅游](../Category/上海旅游.md "wikilink")
[Category:上海文化](../Category/上海文化.md "wikilink")
[Category:上海广播电视台](../Category/上海广播电视台.md "wikilink")

1.  [东方明珠与索尼成立合资公司
    将引入PS主机](http://tech.sina.com.cn/it/2014-05-25/21269398883.shtml).新浪科技.2014-05-25.\[2014-05-26\].
2.
3.
4.