**东方路**是[中国](../Page/中国.md "wikilink")[上海市](../Page/上海市.md "wikilink")[浦东新区的一条南北走向道路](../Page/浦东新区.md "wikilink")。

东方路原名**文登路**，得名于[山东省县名](../Page/山东省.md "wikilink")[文登](../Page/文登.md "wikilink")。1950年代后期，配合新住宅区规划建设辟筑\[1\]，1994年[上海东方电视台建此后改今名](../Page/上海东方电视台.md "wikilink")。道路北端终点设有[其秦线轮渡](../Page/其秦线轮渡.md "wikilink")。

[上海轨道交通四号线途径东方路](../Page/上海轨道交通四号线.md "wikilink")，设有[浦东大道站及](../Page/浦东大道站.md "wikilink")[世纪大道站](../Page/世纪大道站.md "wikilink")。[上海轨道交通六号线设有世纪大道站](../Page/上海轨道交通六号线.md "wikilink")、[浦电路站](../Page/浦电路站.md "wikilink")、[蓝村路站](../Page/蓝村路站.md "wikilink")、[上海儿童医学中心站及](../Page/上海儿童医学中心站.md "wikilink")[临沂新村站](../Page/临沂新村站.md "wikilink")。

## 周邊建築物

  - [仁济东院](../Page/上海交通大学医学院附属仁济医院.md "wikilink")
  - [上海儿童医学中心](../Page/上海儿童医学中心.md "wikilink")
  - [世纪汇广场](../Page/世纪汇广场.md "wikilink")

## 交会道路（北向南）

  - [昌邑路](../Page/昌邑路.md "wikilink")
  - [浦东大道](../Page/浦东大道.md "wikilink")
  - [栖霞路](../Page/栖霞路\(上海\).md "wikilink")
  - [乳山路](../Page/乳山路.md "wikilink")
  - [商城路](../Page/商城路.md "wikilink")
  - [世纪大道](../Page/世纪大道.md "wikilink")、[张杨路](../Page/张杨路.md "wikilink")
  - [潍坊路](../Page/潍坊路.md "wikilink")
  - [向城路](../Page/向城路.md "wikilink")
  - [浦电路](../Page/浦电路.md "wikilink")
  - [峨山路](../Page/峨山路.md "wikilink")
  - [蓝村路](../Page/蓝村路.md "wikilink")
  - [浦建路](../Page/浦建路.md "wikilink")
  - [北园路](../Page/北园路.md "wikilink")
  - [龙阳路](../Page/龙阳路.md "wikilink")
  - [东三里桥路](../Page/东三里桥路.md "wikilink")
  - [兰陵路](../Page/兰陵路.md "wikilink")
  - [浦三路](../Page/浦三路.md "wikilink")
  - [南码头路](../Page/南码头路.md "wikilink")
  - [临沂路](../Page/临沂路.md "wikilink")
  - [沂林路](../Page/沂林路.md "wikilink")
  - [浦东南路](../Page/浦东南路.md "wikilink")

## 参考文献

[Category:浦东新区](../Category/浦东新区.md "wikilink")
[Category:上海道路](../Category/上海道路.md "wikilink")

1.  [上海市政工程志 \>\> 专记 \>\>
    浦东新区市政工程](http://www.shtong.gov.cn/newsite/node2/node2245/node68289/node68305/node68462/index.html)