**东昌府区**是[中国](../Page/中国.md "wikilink")[山东省](../Page/山东省.md "wikilink")[聊城市所辖的一个](../Page/聊城市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")，西临[京杭大运河](../Page/京杭大运河.md "wikilink")。

## 行政区划

东昌府区辖10个街道、8个镇、2个乡：

  - 街道：古楼街道、柳园街道、新区街道、湖西街道、道口铺街道、闫寺街道、凤凰街道、北城街道。
  - 镇：侯营镇、沙镇镇、堂邑镇、梁水镇、斗虎屯镇、郑家镇、张炉集镇、于集镇。
  - 乡：许营乡、朱老庄乡。

其中，东城街道、蒋官屯街道由[聊城经济开发区管理](../Page/聊城经济开发区.md "wikilink")。

## 外部链接

  - [聊城市东昌府区政府网站](http://www.dongchangfu.gov.cn/)

[东昌府区](../Category/东昌府区.md "wikilink")
[区](../Category/聊城区县市.md "wikilink")
[聊城](../Category/山东市辖区.md "wikilink")