**东明路站**位于[上海](../Page/上海.md "wikilink")[浦东新区](../Page/浦东新区.md "wikilink")[成山路](../Page/成山路.md "wikilink")[东明路](../Page/东明路.md "wikilink")，为[上海轨道交通6号线与](../Page/上海轨道交通6号线.md "wikilink")[13号线的地下](../Page/上海轨道交通13号线.md "wikilink")[岛式车站](../Page/岛式站台.md "wikilink")，其中6号线部分于2007年12月29日启用，13号线部分于2018年12月30日启用。

该站6号线站厅分为南、北两个结构，互相在站厅层不相连；13号线换乘通道与6号线北站厅相连。

## 车站出口

  - 1号口（6号线北厅）：东明路东侧，成山路北
  - 2号口（6号线南厅）：东明路东侧，成山路南
  - 3号口：成山路北侧，东明路东
  - 4号口：成山路北侧，邹平路西
  - 5号口：成山路南侧，邹平路西
  - 6号口：成山路南侧，东明路东

## 车站構造

  - 6号线月台配置

<table>
<thead>
<tr class="header">
<th><p>上行</p></th>
<th></th>
<th><p><a href="../Page/高科西路站.md" title="wikilink">高科西路</a>・<a href="../Page/港城路站.md" title="wikilink">港城路方向</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>下行</p></td>
<td></td>
<td><p><a href="../Page/高青路站.md" title="wikilink">高青路</a>・<a href="../Page/东方体育中心站.md" title="wikilink">东方体育中心方向</a></p></td>
</tr>
</tbody>
</table>

  - 13号线月台配置

<table>
<thead>
<tr class="header">
<th><p>上行</p></th>
<th></th>
<th><p><a href="../Page/成山路站.md" title="wikilink">成山路</a>・<a href="../Page/金运路站.md" title="wikilink">金运路方向</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>下行</p></td>
<td></td>
<td><p><a href="../Page/华鹏路站.md" title="wikilink">华鹏路</a>・<a href="../Page/张江路站.md" title="wikilink">张江路方向</a></p></td>
</tr>
</tbody>
</table>

[Category:浦东新区地铁车站](../Category/浦东新区地铁车站.md "wikilink")
[Category:2007年启用的铁路车站](../Category/2007年启用的铁路车站.md "wikilink")
[Category:以街道命名的上海軌道交通車站](../Category/以街道命名的上海軌道交通車站.md "wikilink")