**東碇島**，原属漳州龙海，今属[中華民國](../Page/中華民國.md "wikilink")[金門縣](../Page/金門縣.md "wikilink")[金城鎮管理的島嶼](../Page/金城鎮_\(金門縣\).md "wikilink")，距離[金門縣](../Page/金門縣.md "wikilink")[烈嶼鄉南方](../Page/烈嶼鄉.md "wikilink")27公里處，面積0.016平方公里，比[北碇島小](../Page/北碇島.md "wikilink")，是金門縣境之極南點所在。\[1\]\[2\]島上有由[英國籍工程師韓得善](../Page/英國.md "wikilink")（David
Marr
Henderson）設計、1871年建造的燈塔，現由[交通部航港局管理](../Page/交通部航港局.md "wikilink")，塔身為黑色。東椗與北碇兩島之燈塔合稱「金門雙碇」。\[3\]\[4\]\[5\]

东碇岛原属漳州龙海，在地图等文件资料中，中华人民共和国官方将其仍置于漳州市龙海市下。

## 另见

  - [南碇岛](../Page/南碇岛.md "wikilink")
  - [北碇岛](../Page/北碇岛.md "wikilink")
  - [金門縣](../Page/金門縣.md "wikilink")
  - [金城鎮 (金門縣)](../Page/金城鎮_\(金門縣\).md "wikilink")
  - [烏坵鄉](../Page/烏坵鄉.md "wikilink")
  - [連江縣](../Page/連江縣.md "wikilink")
  - [漳州市](../Page/漳州市.md "wikilink")
  - [龙海市](../Page/龙海市.md "wikilink")
  - [中華民國島嶼列表\#金門群島](../Page/中華民國島嶼列表#金門群島.md "wikilink")
  - [中華民國燈塔列表](../Page/中華民國燈塔列表.md "wikilink")

## 參考資料

## 外部链接

  - [東碇島限制（禁止）水域圖](http://www.mac.gov.tw/public/MMO/MAC/%E9%99%90%E5%88%B6%E3%80%81%E7%A6%81%E6%AD%A2%E6%B0%B4%E5%9F%9F%E5%9C%968.html)
  - [在GOOGLEMAP查看东碇岛位置](http://www.google.com/maps?f=q&hl=en&q=+24%C2%B0+9%2742.00%22N,118%C2%B014%2712.00%22E&ie=UTF8&z=12&ll=24.169308,118.147316&spn=0.139386,0.43396&t=k&om=1&iwloc=A)
  - [綠意盎然的金門東碇島與北碇島](https://archive.is/20121223065928/http://mna.gpwb.gov.tw/mnanew/internet/NewsDetail.aspx?GUID=31655),
    中華民國軍事新聞通訊社, 2007/02/18
  - [東碇行](https://epaper.vac.gov.tw/zh-tw/C/1716%7C1/25745/1/Publish.htm),
    [國軍退除役官兵輔導委員會](../Page/國軍退除役官兵輔導委員會.md "wikilink")
  - [守護燈塔近半世紀
    陳慶虎回歸農家樂](http://www.cna.com.tw/topic/newstopic/996-1/201802160067-1.aspx),
    [中央通訊社](../Page/中央通訊社.md "wikilink"), 2018-02-16

[Category:福建岛屿](../Category/福建岛屿.md "wikilink")
[Category:金門島嶼](../Category/金門島嶼.md "wikilink") [Category:金城鎮
(金門縣)](../Category/金城鎮_\(金門縣\).md "wikilink")

1.
2.
3.
4.
5.