**东福尔**（
）是[挪威南部的一个](../Page/挪威.md "wikilink")[郡](../Page/挪威行政區劃.md "wikilink")，首府为[萨尔普斯堡](../Page/萨尔普斯堡.md "wikilink")。东福尔郡面积4182[平方公里](../Page/平方公里.md "wikilink")，人口262,523（2007年），是挪威人口第6大的郡，也是该国城镇最多的一郡，最大的城市是[腓特烈斯塔](../Page/腓特烈斯塔.md "wikilink")。

## 自治市

东福尔郡有18个自治市，如下：

<table>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Ostfold_municipalities.png" title="fig:Ostfold_municipalities.png">Ostfold_municipalities.png</a></p></td>
<td><ol>
<li><a href="../Page/阿勒馬克.md" title="wikilink">阿勒馬克</a>（）</li>
<li><a href="../Page/阿希姆_(挪威).md" title="wikilink">阿希姆</a>（）</li>
<li><a href="../Page/埃德斯貝格.md" title="wikilink">埃德斯貝格</a>（）</li>
<li><a href="../Page/腓特烈斯塔.md" title="wikilink">腓特烈斯塔</a>（）</li>
<li><a href="../Page/哈尔登.md" title="wikilink">哈尔登</a>（）</li>
<li><a href="../Page/霍伯爾.md" title="wikilink">霍伯爾</a>（）</li>
<li><a href="../Page/瓦勒爾.md" title="wikilink">瓦勒爾</a>（）</li>
<li><a href="../Page/馬克爾_(挪威).md" title="wikilink">馬克爾</a>（）</li>
<li><a href="../Page/莫斯.md" title="wikilink">莫斯</a>（）</li>
<li><a href="../Page/拉克斯塔.md" title="wikilink">拉克斯塔</a>（）</li>
<li><a href="../Page/呂格_(挪威).md" title="wikilink">呂格</a>（）</li>
<li><a href="../Page/勒姆斯庫格.md" title="wikilink">勒姆斯庫格</a>（）</li>
<li><a href="../Page/羅德_(挪威).md" title="wikilink">羅德</a>（）</li>
<li><a href="../Page/萨尔普斯堡.md" title="wikilink">萨尔普斯堡</a>（）</li>
<li><a href="../Page/希普特沃特.md" title="wikilink">希普特沃特</a>（）</li>
<li><a href="../Page/斯彼得贝格.md" title="wikilink">斯彼得贝格</a>（）</li>
<li><a href="../Page/特勒格斯塔.md" title="wikilink">特勒格斯塔</a>（）</li>
<li><a href="../Page/沃勒.md" title="wikilink">沃勒</a>（）</li>
</ol></td>
</tr>
</tbody>
</table>

## 外部链接

  - [东福尔郡官方网站](http://www.ostfold-f.kommune.no)

[Østfold](../Category/挪威行政区划.md "wikilink")