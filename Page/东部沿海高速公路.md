**东部沿海高速公路**（罗湖至盐田新建段俗稱**深鹽二通道**、**梧桐二隧**）是一條位於[中國](../Page/中國.md "wikilink")[廣東省](../Page/廣東省.md "wikilink")[深圳市的一條](../Page/深圳市.md "wikilink")[高速公路](../Page/高速公路.md "wikilink")。起點[羅湖](../Page/羅湖區.md "wikilink")[蓮塘](../Page/蓮塘街道.md "wikilink")，于盐田接[盐坝高速公路往東至](../Page/盐坝高速公路.md "wikilink")[大鹏新區](../Page/大鹏新區.md "wikilink")，东部沿海高速公路亦是[深圳至](../Page/深圳.md "wikilink")[惠州的](../Page/惠州.md "wikilink")[第二快速通道的一部分](../Page/惠深沿海高速.md "wikilink")，概算总投资27.64亿元[人民幣](../Page/人民幣.md "wikilink")\[1\]。

## 東部瓶頸

過去連接[羅湖與](../Page/羅湖區.md "wikilink")[鹽田最直接的通道僅有](../Page/鹽田.md "wikilink")[羅沙路以及](../Page/羅沙路.md "wikilink")[梧桐山隧道](../Page/梧桐山隧道.md "wikilink")。隨著[深圳東部](../Page/深圳.md "wikilink")、[蓮塘經濟發展](../Page/蓮塘街道.md "wikilink")，[羅沙路以及](../Page/羅沙路.md "wikilink")[梧桐山隧道已經不能應付日益上升的交通流量](../Page/梧桐山隧道.md "wikilink")。

興建目的就是改善深圳中心與東部的交通狀況，改善深圳东南部二線的客、货运输条件、疏导港口交通、实现货运车流“东进东出”的作用，对构建畅通的公路联网、优化盐田地区投资环境、促进[盐田港后续建设乃至深圳市经济发展都具有十分重要的意义](../Page/盐田港.md "wikilink")。开车从深圳中心区到盐田縮減至20[分钟](../Page/分钟.md "wikilink")。

## 簡介

全長11.38[公里](../Page/公里.md "wikilink")，雙向6車道，设计行车速度每小時80公里\[2\]，路基宽度27米。橫穿[梧桐山和](../Page/梧桐山.md "wikilink")[夾山](../Page/夾山.md "wikilink")，途經正坑、田东、林场、[盐田港](../Page/盐田港.md "wikilink")，终点[盐田区](../Page/盐田区.md "wikilink")[大梅沙接](../Page/大梅沙.md "wikilink")[惠深沿海高速公路](../Page/惠深沿海高速公路.md "wikilink")。

主要工程有[桥梁](../Page/桥梁.md "wikilink")3座，主线桥梁总长8.8公里，匝道桥梁总长5.1公里；隧道6座，占线路总长的56.1%，其中最长隧道为[林场隧道](../Page/林场隧道.md "wikilink")，长度为2419[米](../Page/米.md "wikilink")；新建互通式立交2处，全线桥隧比例达91%，居[中国首位](../Page/中国.md "wikilink")。

东部沿海高速公路是深圳市往大梅沙的免費公路，经[盐坝高速公路和](../Page/盐坝高速公路.md "wikilink")[惠深沿海高速公路惠州段于稔山连接](../Page/惠深沿海高速公路.md "wikilink")[深汕高速公路](../Page/深汕高速公路.md "wikilink")。

## 互通枢纽及服务设施

## 參見

  - [梧桐山隧道](../Page/梧桐山隧道.md "wikilink")
  - [林場隧道](../Page/林場隧道.md "wikilink")
  - [羅沙公路](../Page/羅沙公路.md "wikilink")

## 参考文献

[Category:深圳市高速公路](../Category/深圳市高速公路.md "wikilink")

1.

2.