**东门街道**是[中国](../Page/中华人民共和国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[深圳市](../Page/深圳市.md "wikilink")[罗湖区下辖的一个](../Page/罗湖区.md "wikilink")[街道](../Page/街道_\(行政区划\).md "wikilink")\[1\]，地处罗湖区中心位置。总面积2.1平方公里。总人口10余万人，其中常住人口4.3万，暂住人口5.5万。

东门街道是深圳市最早的街道办事处之一，东门之名出自于域内的东门商业圈，定名于1999年12月，1979年10月成立之初曾称人民路街道，在1983年12月则为蛟湖街道办事处。

## 范围

东门街道东以文锦路为界，与[翠竹街道](../Page/翠竹街道.md "wikilink")、[黄贝街道相接](../Page/黄贝街道.md "wikilink")；西以人民公园路、[布吉河为界](../Page/布吉河.md "wikilink")，与[桂园街道相接](../Page/桂园街道_\(深圳市\).md "wikilink")；北以笋岗路为界，与[笋岗街道相接](../Page/笋岗街道.md "wikilink")；南以深南东路为界，与[南湖街道相接](../Page/南湖街道_\(深圳市\).md "wikilink")。

## 行政区划

东门街道下辖以下地区：\[2\]

。

## 参考资料

1.  [东门街道办](https://web.archive.org/web/20071013090821/http://szlh.gov.cn/main/zfjg/jdbsc/dm/dmgm/index.shtml?catalogId=1383)，深圳市罗湖区政府网站，2007年9月16日造访。

[Category:罗湖区行政区划](../Category/罗湖区行政区划.md "wikilink")
[罗湖区](../Category/深圳街道_\(行政区划\).md "wikilink")

1.
2.