**东阿镇**，[中国](../Page/中国.md "wikilink")[山东省](../Page/山东省.md "wikilink")[济南市](../Page/济南市.md "wikilink")[平阴县下属的一个](../Page/平阴县.md "wikilink")[镇](../Page/镇.md "wikilink")，位于济南西南，与[聊城市和](../Page/聊城市.md "wikilink")[泰安市相邻](../Page/泰安市.md "wikilink")，下辖55个[行政村](../Page/村民委员会.md "wikilink")，面积95平方公里，人口38839人。

东阿镇古称谷邑或[谷城县](../Page/谷城县.md "wikilink")，始建于[春秋时期](../Page/春秋时期.md "wikilink")，为[管仲的食邑](../Page/管仲.md "wikilink")。其后为谷城县的县城。在1375年至1946年间为[东阿县县城](../Page/东阿县.md "wikilink")，后被划归平阴县，1952年设立东阿镇。1996年，东阿镇被认定为[阿胶故乡](../Page/阿胶.md "wikilink")。

## 人口

2010年[中国第六次人口普查时](../Page/中国第六次人口普查.md "wikilink")，东阿镇共有人口29326人。共有家庭10188户，平均每户2.87人。14岁以下的少年儿童共4173人，占总人口14.22%；15-64岁人口共20905人，占总人口71.28%；65岁及以上的老年人共4248人，占总人口的14.48%。男性共计14094人，占总人口48.05%；女性共计15232，占总人口51.94%。本地居住的人口中，拥有本地户籍的人口为28366人，占96.72%。\[1\]

## 参考资料

  - [平阴县政府网站·东阿镇](https://web.archive.org/web/20080112110748/http://www.pingyin.gov.cn/html/gaikuang/pyxz/2006/1015/06101521232250.html)

[Category:济南乡级行政区划](../Category/济南乡级行政区划.md "wikilink")
[Category:平阴县](../Category/平阴县.md "wikilink")

1.