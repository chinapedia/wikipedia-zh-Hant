《**丟勒的犀牛**》是一幅由[德國](../Page/德國.md "wikilink")[畫家兼](../Page/畫家.md "wikilink")[版畫家](../Page/版畫家.md "wikilink")[阿爾布雷希特·丟勒於](../Page/阿爾布雷希特·杜勒.md "wikilink")1515年作的[木刻版畫](../Page/木刻版畫.md "wikilink")。\[1\]作品建基於一幅[印度犀牛的](../Page/印度犀牛.md "wikilink")[素描](../Page/素描.md "wikilink")，由不知名畫家所畫的，畫上並寫有描述。而畫中的犀牛則是早幾年來到[里斯本的](../Page/里斯本.md "wikilink")，是自從[羅馬帝國以來第一個活生生的例子](../Page/羅馬帝國.md "wikilink")。然而，丟勒本人從沒見過那隻犀牛的實體。在1515年末，[葡萄牙國王](../Page/葡萄牙.md "wikilink")[曼努埃爾一世把這隻動物送贈給](../Page/曼努埃爾一世_\(葡萄牙\).md "wikilink")[教宗](../Page/教宗.md "wikilink")[利奧十世作為禮物](../Page/利奧十世.md "wikilink")，但牠在1516年初一次在[義大利海岸所發生的海難中死去](../Page/義大利.md "wikilink")。自此以後，歐洲再沒見過活生生的犀牛的紀錄，直至到1579年左右才有第二隻犀牛從[印度進貢給](../Page/印度.md "wikilink")[西班牙的](../Page/西班牙.md "wikilink")[腓力二世](../Page/腓力二世_\(西班牙\).md "wikilink")。\[2\]\[3\]

儘管畫中犀牛的構造並不正確，但丟勒的版畫卻风靡了整個歐洲，並在接下來的三個世紀被大量拷貝。在十八世紀晚期以前，這幅畫依然被認為是表達著犀牛的真正模樣。最終，作品被後來一些更真實的素描與繪畫所取代，尤其是有關[犀牛克拉拉的繪圖](../Page/犀牛克拉拉.md "wikilink")。有人曾這樣說：「再沒有動物圖畫像《丟勒的犀牛》般對於藝術影響深厚。」\[4\]

## 從沒見過的動物

1515年5月20日，一隻印度犀牛從[印度被運到里斯本](../Page/印度.md "wikilink")。早在1514年初，[葡屬印度的總督](../Page/葡屬印度.md "wikilink")[阿爾布克爾克派遣大使到](../Page/阿爾布克爾克.md "wikilink")[坎貝](../Page/坎貝.md "wikilink")（即今天的[古吉拉特邦](../Page/古吉拉特邦.md "wikilink")）的統治者[蘇丹](../Page/蘇丹.md "wikilink")[Muzafar二世那裡去](../Page/Muzafar二世.md "wikilink")，尋求准許他可以在[第烏](../Page/第烏.md "wikilink")（Diu）上興建堡壘。任務最終以失敗告吹，但雙方卻交換了外交禮物，當中包括了犀牛。\[5\]

## 丟勒的木刻版畫

## 註解

<div class="references-small" style="-moz-column-count: 2; column-count: 2;">

<references/>

</div>

## 參考資料

[Dalí.Rinoceronte.JPG](https://zh.wikipedia.org/wiki/File:Dalí.Rinoceronte.JPG "fig:Dalí.Rinoceronte.JPG")\]\]

  - (particularly Chapter 5, "The Ill-Fated Rhinoceros")

  - (particularly Chapter 1, "The first Lisbon or 'Dürer Rhinoceros' of
    1515")

  - [David Quammen](../Page/David_Quammen.md "wikilink") (2000), *The
    Boilerplate Rhino: Nature in the Eye of the Beholder*, Scribner,
    ISBN 978-0-684-83728-4 (particularly p.201-209, *The Boilerplate
    Rhino*, previously published in this "Natural Acts" column in
    [*Outside*
    magazine](../Page/Outside_\(magazine\).md "wikilink")，June 1993)

## 外部連結

  - [*Albert
    Dürer*](http://www.gutenberg.org/dirs/etext06/8durr10.txt)，T.
    Sturge Moore所撰，[古騰堡計劃](../Page/古騰堡計劃.md "wikilink")
  - [*Vector Graphic Adaptions of Dürer's
    Rhinoceros*](http://vektordb.lafkon.net/index.php?id=132) in
    [Adobe](../Page/Adobe_Systems.md "wikilink")
    [Illustrator](../Page/Adobe_Illustrator.md "wikilink")
    [Format](../Page/File_format.md "wikilink") by
    [LAFKON](http://www.lafkon.net)（under a [creative
    commons](../Page/creative_commons.md "wikilink") license）
  - [*The
    rhinoceros*](http://www.ngv.vic.gov.au/ngvcollection/durer/zoomArtwork.do?index=0&artworkID=27141)
    by [Albrecht Dürer](../Page/Albrecht_Dürer.md "wikilink")，in the
    collection of the [National Gallery of
    Victoria](../Page/National_Gallery_of_Victoria.md "wikilink")。

[Category:著名犀牛](../Category/著名犀牛.md "wikilink")
[Category:文藝復興時期作品](../Category/文藝復興時期作品.md "wikilink")
[Category:1515年作品](../Category/1515年作品.md "wikilink")
[Category:大英博物館收藏品](../Category/大英博物館收藏品.md "wikilink")
[Category:版畫](../Category/版畫.md "wikilink")
[Category:阿爾布雷希特·杜勒作品](../Category/阿爾布雷希特·杜勒作品.md "wikilink")

1.  有資料錯誤地say 1513, copying a typographical error made by Dürer in one
    of his original drawings and perpetuated in his woodcut. (Bedini,
    p.121.)
2.  Clarke, chapter 2.
3.  A street in Madrid was named *Abada* (rhinoceros in Portuguese)
    after this animal, that had a curious life too:
    [1](http://callesdemadrid.blogspot.com/2006/11/abada-calle-de-la.html).
     (in Spanish)
4.  Quoted in Clarke, p.20.
5.  Bedini, p.112.