**严子均**，[清末](../Page/清.md "wikilink")[民初企业家](../Page/民国.md "wikilink")、商人。[号](../Page/号.md "wikilink")**义彬**，是巨商[严信厚之子](../Page/严信厚.md "wikilink")，[浙江省](../Page/浙江省.md "wikilink")[慈溪县](../Page/慈溪.md "wikilink")（今[宁波](../Page/宁波.md "wikilink")）人。

## 生平

[清朝](../Page/清朝.md "wikilink")[光绪三十三年](../Page/光绪.md "wikilink")（1907年），开始经商，子承父业，帮助打理家族生意，并在[上海开设源吉钱庄和德源钱庄](../Page/上海.md "wikilink")。严子均与当时的[上海](../Page/上海.md "wikilink")[道台](../Page/道台.md "wikilink")（相当于现在的[上海市](../Page/上海市.md "wikilink")[市长](../Page/市长.md "wikilink")）[蔡乃煌常来往](../Page/蔡乃煌.md "wikilink")，交往密切，严子均并帮助承办源通海关官[银行](../Page/银行.md "wikilink")。

严子均之业务遍及很多工商业领域，并在[北京](../Page/北京.md "wikilink")、[天津](../Page/天津.md "wikilink")、[汉口](../Page/汉口.md "wikilink")、[广州](../Page/广州.md "wikilink")、[福州](../Page/福州.md "wikilink")、[香港](../Page/香港.md "wikilink")、[汕头](../Page/汕头.md "wikilink")、[厦门](../Page/厦门.md "wikilink")、[杭州](../Page/杭州.md "wikilink")、[宁波等](../Page/宁波.md "wikilink")[中国各大城市均有业务分支](../Page/中国.md "wikilink")。

[光绪三十四年](../Page/光绪.md "wikilink")，严子均出任源通官银号[董事](../Page/董事.md "wikilink")。[宣统元年](../Page/宣统.md "wikilink")（1909年），严子均被选为上海商务总会第五任协理。1909年，严子均出任上海龙章机器造纸公司协理，并担任上海内地自来水厂公司[董事](../Page/董事.md "wikilink")。同年，被授予[清朝政府农工商部](../Page/清朝.md "wikilink")[员外郎的官职](../Page/员外郎.md "wikilink")。[民国](../Page/民国.md "wikilink")13年（1924年），出任上海总商会特别会董。[民国](../Page/民国.md "wikilink")15年（1926年），严子均出任上海总商会会董。

## 家庭

严子均先后有过张氏与杨俪芬两位夫人。并育有十二个子女，五个儿子分别为[严智多](../Page/严智多.md "wikilink")、[严智珠](../Page/严智珠.md "wikilink")、[严智桐](../Page/严智桐.md "wikilink")、[严智实](../Page/严智实.md "wikilink")、[严智寿](../Page/严智寿.md "wikilink")。其中长子[严智多有女儿](../Page/严智多.md "wikilink")[严仁美](../Page/严仁美.md "wikilink")、[严仁芸](../Page/严仁芸.md "wikilink")，[严仁美为民国时的](../Page/严仁美.md "wikilink")[上海滩名媛](../Page/上海滩.md "wikilink")，严仁芸则嫁给了[杜月笙长子杜维藩](../Page/杜月笙.md "wikilink")。七个女儿中的三个[严彩韵](../Page/严彩韵.md "wikilink")、[严莲韵](../Page/严莲韵.md "wikilink")、[严幼韵都是中国近代知名女性](../Page/严幼韵.md "wikilink")，被并称为“严氏三姐妹”。

## 参考

  - [上海市地方志办公室
    严子均（1872～1931）](http://www.shtong.gov.cn/newsite/node2/node4429/node4432/node70708/node70710/userobject1ai45544.html)
  - [英才辈出的费市严家](http://culture.zjol.com.cn/05culture/system/2005/12/27/006419611.shtml)

[Category:清朝商人](../Category/清朝商人.md "wikilink")
[Category:寧波商幫](../Category/寧波商幫.md "wikilink")
[Category:中華民國企業家](../Category/中華民國企業家.md "wikilink")
[Category:慈溪人](../Category/慈溪人.md "wikilink")
[Category:宁波裔上海人](../Category/宁波裔上海人.md "wikilink")
[Z严子均](../Category/费市严家.md "wikilink")
[Zi子](../Category/嚴姓.md "wikilink")