[PHS_ASTEL_06c2774as.jpg](https://zh.wikipedia.org/wiki/File:PHS_ASTEL_06c2774as.jpg "fig:PHS_ASTEL_06c2774as.jpg")
**个人手持式电话系统**（，缩写），某些市场称为**个人电话存取系统**，在中國大陸俗称**小灵通**，是指一种无线本地[电话技术](../Page/电话.md "wikilink")，采用[微蜂窝通信技术](../Page/微蜂窝通信技术.md "wikilink")。PHS这项技术在1880至1930[兆赫这个](../Page/兆赫.md "wikilink")[波段内运作](../Page/波段.md "wikilink")。

## 技術

PHS技術實際上屬於[數位移動通信技術](../Page/數位移動通信技術.md "wikilink")，屬於[第二代行動通信技術](../Page/2G.md "wikilink")。PHS[基站由於發射功率通常使用](../Page/基站.md "wikilink")10mW，也有使用200mW，500mW的情況，相比與主流[GSM與](../Page/GSM.md "wikilink")[CDMA基站最大](../Page/CDMA.md "wikilink")20W的發射功率，十分有限。因此，覆蓋範圍也要小得多，通信基站與终端間距離較短，而覆蓋較大面積時需要更多的基站。這使得PHS較適合在都市使用，在野外等地使用效果欠佳。在手機的通訊速度世代上，PHS屬於2G的範圍。其設計也使其在通話時有少許延遲（因為其覆蓋面積小，“蜂窩”面積小）。

PHS使用[TDMA](../Page/TDMA.md "wikilink")／[TDD作為它的無線電通訊接口](../Page/TDD.md "wikilink")，以及32K的[ADPCM作為它的聲音傳送編碼](../Page/ADPCM.md "wikilink")。現代的PHS電話也可以支持其他一些[ISP的增值服務](../Page/ISP.md "wikilink")，如：[網際網路窄帶通訊](../Page/網際網路.md "wikilink")、[短信](../Page/短信.md "wikilink")、[電子郵件](../Page/電子郵件.md "wikilink")，甚至圖片傳訊。

PHS這項技術也適用於小範圍的[無線電通訊](../Page/無線電.md "wikilink")。

## 應用

PHS技術最初的發佈是1989年在[日本的](../Page/日本.md "wikilink")[NTT實驗室](../Page/NTT實驗室.md "wikilink")，作為一種與[GSM和](../Page/GSM.md "wikilink")[PDC競爭的技術的形式出現的](../Page/PDC.md "wikilink")。早期是由[日本根據本國國情](../Page/日本.md "wikilink")（多數地方人口密度大、人口的流動性小）開發研製的。
[Mobile_phone_PHS_Japan_1997-2003.jpg](https://zh.wikipedia.org/wiki/File:Mobile_phone_PHS_Japan_1997-2003.jpg "fig:Mobile_phone_PHS_Japan_1997-2003.jpg")-
[NTT DoCoMo](../Page/NTT_DoCoMo.md "wikilink")-
[ASTEL](../Page/ASTEL.md "wikilink")）\]\] 1995年，最初在日本由3家電信運營商來運作：[NTT
Personal](../Page/NTT_Personal.md "wikilink")、[DDI
Pocket和](../Page/Willcom.md "wikilink")[ASTEL](../Page/ASTEL.md "wikilink")。然而，服務推出不久後，該技術被稱為“窮人的蜂窩”，而在日本大受限制，市場佔有率也逐漸下降。之後NTT
Personal被[NTT DoCoMo合併](../Page/NTT_DoCoMo.md "wikilink")。2004年，NTT
DoCoMo決定2007年結束PHS服務。ASTEL由幾個區域公司經營，其中大部分公司已經結束服務了。

但是DDI
Pocket於2001年6月開始無限32kbps上網服務Air-H"，開拓新用途。2002年，Air-H"的最大通訊速度提高為128kbps。2005年，DDI-Pocket的大部分股份由美國投資公司[Carlyle收購](../Page/Carlyle.md "wikilink")，公司名稱改為Willcom，Air-H"服務也改名為Air-Edge。[Willcom開始無限通話服務](../Page/Willcom.md "wikilink")，並提升[Air-Edge的最高通訊速度為](../Page/Air-Edge.md "wikilink")256kbps，2006年再次提高為402kbps，超過[WCDMA第三代行動電話的通訊速度](../Page/WCDMA.md "wikilink")。現在Willcom恢復了過去DDI
Pocket達到的最大客戶數。
在一些國家和地區PHS運營業者大多以較低的收費以及較多的附加業務也受到歡迎，例如[中國大陸](../Page/中國大陸.md "wikilink")、[智利](../Page/智利.md "wikilink")、[臺灣](../Page/臺灣.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[越南](../Page/越南.md "wikilink")、[孟加拉國](../Page/孟加拉國.md "wikilink")、[尼日利亞](../Page/尼日利亞.md "wikilink")、[馬里](../Page/馬里.md "wikilink")、[坦桑尼亞和](../Page/坦桑尼亞.md "wikilink")[宏都拉斯等](../Page/宏都拉斯.md "wikilink")。

  - ：中國大陸的PHS業者有兩家，分別是[中國電信和](../Page/中國電信.md "wikilink")[中國網通](../Page/中國網通.md "wikilink")，這兩家公司的大多數無線市話用戶使用的是基於PHS技術的行動電話。中國網通在[北京](../Page/北京.md "wikilink")、[天津](../Page/天津.md "wikilink")、[河北](../Page/河北.md "wikilink")、[山西](../Page/山西.md "wikilink")、[内蒙古](../Page/内蒙古.md "wikilink")、[遼寧](../Page/遼寧.md "wikilink")、[吉林](../Page/吉林.md "wikilink")、[黑龍江](../Page/黑龍江.md "wikilink")、[河南](../Page/河南.md "wikilink")、[山東等十省區市獨傢經營PHS業務](../Page/山東.md "wikilink")，中國其餘地區的PHS業務則由中國電信獨家經營。由於中國政府的限制性政策，中國的PHS電話只能在有限的區域内使用。PHS電話在中國被稱爲“小靈通”。中國大陸的PHS用戶數曾經在2005年以前大幅成長，最多時達到將近一億用戶，但是自2006年以後用戶數則持續減少。截止2008年年末，中國大陸的PHS用戶數已經跌破7000萬戶。因爲中國大陸的PHS網路系統使用的頻段有可能-{干}-擾[TD-SCDMA網路系統使用的頻段](../Page/TD-SCDMA.md "wikilink")，中國[工業和信息化部由此要求中國電信和中國聯通最遲於](../Page/工業和信息化部.md "wikilink")2011年底前結束PHS服務，此擧引發了中國部分PHS用戶的不滿，且當年的小靈通用戶仍有約2400萬戶，退網工作只進行了計畫的1/4，官方在2011年底前廢除「小靈通」系統、全面實現[3G化的計劃顯然已經遭遇困難](../Page/3G.md "wikilink")\[1\]。2014年9月，為了讓出[4G網絡的頻段](../Page/4G.md "wikilink")，隨著各地的運營商陸續關閉小靈通服務，並且對小靈通用戶採取提供優惠並強制轉網的措施，小靈通已徹底退出中國大陸市場。\[2\]\[3\]

<!-- end list -->

  - ：PHS電話在1997年引進香港，以1895-1906.1兆赫頻帶內操作，主要作室內無線電話用途。但因PHS在香港未見普及，政府決定於2013年5月10日起禁止入口及出售PHS器材，並分階段撤銷PHS無線電通訊器材的豁免領牌安排。而政府提供的本地PHS無線電通訊器三年使用寬限期亦會於2016年5月9日屆滿，以騰出無線電頻譜。\[4\]

<!-- end list -->

  - ：臺灣是由[大眾電信獨家經營PHS業務](../Page/大眾電信.md "wikilink")，該公司的低功率行動電話同樣採用PHS技術。但由於基地台收訊範圍不大的關係，使用人數不多，2012年4月12日，約為80萬人，主要用戶均在北臺灣部分。自大眾電信重整計畫獲通過後，PHS用戶擔心服務是否將終止；2010年5月27日，大眾電信常駐重整人張敏玉表示：“PHS服務至少兩年內不會消失，而且還會持續推出新手機；大眾電信的PHS頻譜執照到2016年4月到期，PHS服務的下一步仍在規劃中。”不敵PHS逐漸沒落，在臺灣主打PHS業務的大眾電信2014年12月26日宣告公司破產，但仍將盡力維持網路正常運作。\[5\]中華民國國家通訊傳播委員會（NCC）於2015年3月11日核准大眾電信終止PHS業務的申請，大眾電信於3月31日之後停止服務，NCC也會收回相關號碼及頻率。

## 参见

  - [UT斯达康](../Page/UT斯达康.md "wikilink")

## 参考

<references/>

## 外部链接

  - [PHS MoU
    Group](https://web.archive.org/web/20051220152832/http://www.phsmou.or.jp/)
  - [WILLCOM,Inc](http://www.willcom-inc.com/)（[英文](http://www.willcom-inc.com/en/)或[日文](http://www.willcom-inc.com/ja/)）
  - [大眾電信](http://www.phs.com.tw)

[Category:无线网络](../Category/无线网络.md "wikilink")
[Category:移动通信标准](../Category/移动通信标准.md "wikilink")

1.  [搜狐IT:死而不僵
    小靈通淪為“皮包公司”忽悠利器](http://informationtimes.dayoo.com/html/2013-05/13/content_2244744.htm)

2.
3.
4.  [撤銷PHS無線電通訊器材豁免領牌安排](http://www.info.gov.hk/gia/general/201304/22/P201304220488.htm)，香港通訊事務管理局
    新聞公報，2012年08月23日
5.  [PHS沒落
    大眾電信宣告破產](http://www.cna.com.tw/news/firstnews/201412260282-1.aspx)，中央社，2014年12月26日