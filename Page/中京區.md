**中京区**（）是[京都市的](../Page/京都市.md "wikilink")11区之一，位於京都市的市中心，地勢平坦，政府機關、金融機構、商業區集中於此。\[1\]

## 歷史

現在的中京區範圍在令制國時代屬於[山城國](../Page/山城國.md "wikilink")[葛野郡及](../Page/葛野郡.md "wikilink")[愛宕郡](../Page/愛宕郡.md "wikilink")，1868年廢止[江戶幕府後](../Page/江戶幕府.md "wikilink")，隸屬於新設立的[京都府](../Page/京都府.md "wikilink")；1879年京都府實施時，京都府下設置[上京區和](../Page/上京區.md "wikilink")[下京區](../Page/下京區.md "wikilink")。1889年日本實施[市制](../Page/市制_\(日本制度\).md "wikilink")，上京區和[下京區合設為](../Page/下京區.md "wikilink")[京都市](../Page/京都市.md "wikilink")，但上京區和下京區的行政區劃仍被保留。1929年，上京區[丸太町通以南的區域與下京區](../Page/丸太町通.md "wikilink")[四条通以北的區域被分出設立現在的中京區](../Page/四条通.md "wikilink")。

1985年後，人口一度跌破10万人，近年來由于，在2005年的日本人口普查中該區的人口再次突破10萬人。

## 交通

區內的主要道路包括東西方向的[丸太町通](../Page/丸太町通.md "wikilink")、[御池通](../Page/御池通.md "wikilink")、[四條通](../Page/四條通.md "wikilink")，南北方向的、[西大路通](../Page/西大路通.md "wikilink")、[千本通](../Page/千本通.md "wikilink")、[大宮通](../Page/大宮通.md "wikilink")、[堀川通](../Page/堀川通.md "wikilink")、[烏丸通](../Page/烏丸通.md "wikilink")、[河原町通](../Page/河原町通.md "wikilink")。

軌道交通則有[京都市營地下鐵](../Page/京都市營地下鐵.md "wikilink")[烏丸線和](../Page/烏丸線.md "wikilink")[東西線](../Page/東西線_\(京都市營地下鐵\).md "wikilink")、[京福電氣鐵道](../Page/京福電氣鐵道.md "wikilink")[嵐山本線可連結京都市內各地](../Page/嵐山本線.md "wikilink")，也有[西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")[山陰本線](../Page/山陰本線.md "wikilink")、[阪急電鐵](../Page/阪急電鐵.md "wikilink")[京都本線可通往其他城市](../Page/京都本線.md "wikilink")。

## 參考資料

## 相關條目

  - [中京](../Page/中京.md "wikilink")

## 外部連結

1.