| image_flag = {{\#property:p41}} | image_map = Central Kalimantan in
Indonesia.svg | map_alt = | map_caption = | coordinates =  |
coor_pinpoint = | coordinates_footnotes = | subdivision_type =
[国家](../Page/世界政區索引.md "wikilink") | subdivision_name =  |
established_title = | established_date = | founder = | named_for = |
seat_type = [首府](../Page/印度尼西亚省份列表.md "wikilink") | seat =
[Lambang_Kota_Palangka_Raya.gif](https://zh.wikipedia.org/wiki/File:Lambang_Kota_Palangka_Raya.gif "fig:Lambang_Kota_Palangka_Raya.gif")
[帕朗卡拉亚](../Page/帕朗卡拉亚.md "wikilink") | government_footnotes = |
leader_party = | leader_title = Governor | leader_name = [Sugianto
Sabran](../Page/Sugianto_Sabran.md "wikilink") | unit_pref = Metric |
area_footnotes = | area_total_km2 = 153564.5 | area_rank =
[2nd](../Page/Provinces_of_Indonesia.md "wikilink") | area_note = |
elevation_footnotes = | elevation_m = | population_total = 2368654 |
population_as_of = 2014 | population_footnotes = \[1\] |
population_density_km2 = auto | population_note = |
demographics_type1 = 人口 | demographics1_footnotes = \[2\] |
demographics1_title1 = [族群](../Page/族群.md "wikilink") | timezone1 =
[WIB](../Page/印度尼西亚标准时间.md "wikilink")
([UTC+7](../Page/UTC+07:00.md "wikilink")) | utc_offset1 = |
postal_code_type = | postal_code = | area_code = | area_code_type
= | registration_plate =
[KH](../Page/Vehicle_registration_plates_of_Indonesia.md "wikilink") |
blank_name_sec1 = [HDI](../Page/人类发展指数.md "wikilink") 2018 |
blank_info_sec1 =  0.704 () | blank1_name_sec1 = HDI rank |
blank1_info_sec1 = [20th](../Page/印度尼西亚各省人类发展指数列表.md "wikilink")
(2014) | website = [www.kalteng.go.id](http://www.kalteng.go.id) |
footnotes = | type =
[Province](../Page/Provinces_of_Indonesia.md "wikilink") |
leader_title2 = Vice Governor | leader_name2 = Said Ismail |
demographics1_info1 = \[3\] | demographics1_title2 =
[宗教](../Page/宗教.md "wikilink") (2017)\[4\] |
demographics1_info2 =  | demographics1_title3 =
[語言](../Page/語言.md "wikilink") | demographics1_info3 =  }}

**中加里曼丹**是[印尼在](../Page/印尼.md "wikilink")[婆羅洲島](../Page/婆羅洲.md "wikilink")[加里曼丹地區的四個省份之一](../Page/加里曼丹.md "wikilink")。首府是[帕朗卡拉亞](../Page/帕朗卡拉亞.md "wikilink")，中加里曼丹[面積約](../Page/面積.md "wikilink")153,564[平方公里](../Page/平方公里.md "wikilink")。中加里曼丹于2018年的[人口約](../Page/人口.md "wikilink")2,300,000。

## 地理

### 周圍地區

位于婆羅洲島中南部地區

  - 東南部接壤[南加里曼丹省](../Page/南加里曼丹.md "wikilink")
  - 南濱[爪哇海](../Page/爪哇海.md "wikilink")。
  - 西北部接壤[西加里曼丹省](../Page/西加里曼丹.md "wikilink")。
  - 北與東北相連[東加里曼丹省](../Page/東加里曼丹.md "wikilink")。

### 地形

西北有[山脈](../Page/山脈.md "wikilink")，中部與南部是[沖積平原](../Page/沖積平原.md "wikilink")，南部部份地區為[沼澤地帶](../Page/沼澤.md "wikilink")。

### 山脈

西北山脈有斯赫瓦納山脈和馬勒山脈，二山脈[平行延伸](../Page/平行.md "wikilink")：

1.  [馬勒山脈](../Page/馬勒山脈.md "wikilink")：有一支脈沿北部邊界延伸。
2.  [斯赫瓦納山脈](../Page/斯赫瓦納山脈.md "wikilink")：最高峰為拉亞山（英文：Raya），[海拔](../Page/海拔.md "wikilink")2,278[公尺](../Page/公尺.md "wikilink")。

### 河流

有卡普阿斯河、桑皮特河（Sampit）、Pembuang河、Aiut河和拉曼道（Lamandau）等河流

### 交通

首府[帕朗卡拉亞及](../Page/帕朗卡拉亞.md "wikilink")[桑皮特鎮有](../Page/桑皮特鎮.md "wikilink")[航空運輸](../Page/航空.md "wikilink")，但其他地方交通極為不便，有些內陸地區完全依賴河運對外聯係，但許多河流的流量受季節影響，極不穩定，影響[交通](../Page/交通.md "wikilink")。有一條已經年久失修，從帕朗卡拉亞到Tangkiling的[高速公路](../Page/高速公路.md "wikilink")，是1960年前[蘇聯幫印尼修建的](../Page/蘇聯.md "wikilink")。

### 經濟

中加里曼丹是全印尼經濟極不發達的地區之一，這是由于南部有大片沼澤地，以及當地交通困難。
中加里曼丹的經濟以[農業為主](../Page/農業.md "wikilink")，農作物有[稻米](../Page/稻米.md "wikilink")、[松香](../Page/松香.md "wikilink")、[花生](../Page/花生.md "wikilink")、[大豆](../Page/大豆.md "wikilink")、[藤條](../Page/藤.md "wikilink")、[野生橡膠](../Page/橡膠.md "wikilink")、[蜂蠟](../Page/蜂蠟.md "wikilink")、[玉米](../Page/玉米.md "wikilink")、[甘薯和](../Page/甘薯.md "wikilink")[木薯](../Page/木薯.md "wikilink")。

有小規模[工業](../Page/工業.md "wikilink")，如[林木業](../Page/林木業.md "wikilink")、[碾米](../Page/碾米.md "wikilink")、磨[麵](../Page/麵.md "wikilink")、[木材加工業](../Page/木材加工業.md "wikilink")。[製造業有造小型](../Page/製造業.md "wikilink")[船只](../Page/船.md "wikilink")、[紡織品](../Page/紡織品.md "wikilink")、和製作[念珠](../Page/念珠.md "wikilink")。

## 暴亂事件

在2001年約2月18日到4月9日這一段時期內，中加里曼丹發生了土生土長的[達雅族與外來的](../Page/達雅族.md "wikilink")[馬都拉族之間的血腥種族衝突](../Page/馬都拉族.md "wikilink")。請參看[2001年中加里曼丹種族衝突事件](../Page/2001年中加里曼丹種族衝突事件.md "wikilink")。

[\*](../Category/中加里曼丹省.md "wikilink")
[Category:加里曼丹](../Category/加里曼丹.md "wikilink")

1.   [Central Bureau of Statistics: *Census
    2010*](http://www.bps.go.id/65tahun/SP2010_agregat_data_perProvinsi.pdf)
    , retrieved 17 January 2011.
2.
3.
4.