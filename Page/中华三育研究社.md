**中华三育研究社**是20世纪上半叶由[基督复临安息日会在中国设立的一所高等教育机构](../Page/基督复临安息日会.md "wikilink")，半工半读。

该校1910年成立于河南省周家口（今[周口市](../Page/周口市.md "wikilink")），名为道医官话学校。1912年[辛亥革命时](../Page/辛亥革命.md "wikilink")，迁至南京四条巷，1913年再迁上海，称为三育大学，校址设在公共租界东区（杨树浦）的赖霍尔路（1915年改名为宁国路）。1925年迁往江苏省[句容县桥头镇](../Page/句容.md "wikilink")（在[南京与](../Page/南京.md "wikilink")[镇江之间的](../Page/镇江.md "wikilink")[沪宁铁路线上](../Page/沪宁铁路.md "wikilink")）西山，占地七百五十亩。1931年为立案问题改称中华三育研究社。

1937年，中华三育研究社因受战争影响停办，校舍全毁。次年与[广州](../Page/广州.md "wikilink")[华南三育研究社](../Page/华南三育研究社.md "wikilink")（中学）在[香港](../Page/香港.md "wikilink")[沙田合辦](../Page/沙田_\(香港\).md "wikilink")，稱中華華南三育研究社，1941年12月太平洋战争發生，又內遷廣東老隆，重庆松堡。1947年返回桥头，重建校园。1948年，中华三育研究社再度南迁九龙清水湾，与广州华南三育研究社合辦，仍稱中華華南三育研究社。1950年返回，改名三育神学院。1951年三育神学院停办，校舍被中共政府接收，改办农校。现为南京财经大学红山学院。

目前中华三育研究社旧址尚存21幢老建筑，2007年列为镇江市文物保护单位。

<references/>

<table style="width:8%;">
<colgroup>
<col style="width: 7%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p><strong>1951年以前中国的教会大学</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/燕京大学.md" title="wikilink">燕京大学</a> | <a href="../Page/齐鲁大学.md" title="wikilink">齐鲁大学</a> | <a href="../Page/金陵大学.md" title="wikilink">金陵大学</a> | <a href="../Page/金陵女子大学.md" title="wikilink">金陵女子大学</a> | <a href="../Page/东吴大学.md" title="wikilink">东吴大学</a><br />
<a href="../Page/圣约翰大学.md" title="wikilink">圣约翰大学</a> | <a href="../Page/沪江大学.md" title="wikilink">沪江大学</a> | <a href="../Page/之江大学.md" title="wikilink">之江大学</a> | <a href="../Page/福建协和大学.md" title="wikilink">福建协和大学</a> | <a href="../Page/华南女子大学.md" title="wikilink">华南女子大学</a><br />
<a href="../Page/岭南大学_(广州).md" title="wikilink">岭南大学 (广州)</a> | <a href="../Page/华中大学.md" title="wikilink">华中大学</a> | <a href="../Page/华西协和大学.md" title="wikilink">华西协和大学</a> | <a href="../Page/湘雅医学院.md" title="wikilink">湘雅医学院</a> | <a href="../Page/协和医学院.md" title="wikilink">协和医学院</a><br />
<a href="../Page/辅仁大学.md" title="wikilink">辅仁大学</a> | <a href="../Page/震旦大学.md" title="wikilink">震旦大学</a> | <a href="../Page/津沽大学.md" title="wikilink">津沽大学</a> | <strong>中华三育研究社</strong><br />
</p></td>
</tr>
</tbody>
</table>

[Category:中華民國大陸時期大專院校](../Category/中華民國大陸時期大專院校.md "wikilink")
[Category:中國教會大學](../Category/中國教會大學.md "wikilink")
[Category:1910年創建的教育機構](../Category/1910年創建的教育機構.md "wikilink")