[Chinese_woman_tractor_driver.jpg](https://zh.wikipedia.org/wiki/File:Chinese_woman_tractor_driver.jpg "fig:Chinese_woman_tractor_driver.jpg")
[China_Harvest.jpg](https://zh.wikipedia.org/wiki/File:China_Harvest.jpg "fig:China_Harvest.jpg")
[缩略图](https://zh.wikipedia.org/wiki/File:China's_Soybean_Production_and_Use_\(11717585785\).jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:中国江苏省太仓市现代农业园.JPG "fig:缩略图")现代农业园\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:20160722_210_libramont.jpg "fig:缩略图")
**中华人民共和国农业**是支持经济建设与发展的基础产业，广义[农业](../Page/农业.md "wikilink")（也称“[第一产业](../Page/第一产业.md "wikilink")”）包括[种植业](../Page/种植业.md "wikilink")、[林业](../Page/林业.md "wikilink")、[畜牧业](../Page/畜牧业.md "wikilink")、[渔业](../Page/渔业.md "wikilink")、[副业](../Page/副业.md "wikilink")。

## 历史

[第二次国共内战时期](../Page/第二次国共内战.md "wikilink")，[中国共产党在其控制的](../Page/中国共产党.md "wikilink")[解放区进行土地改革](../Page/解放区.md "wikilink")。[中华人民共和国建立初期](../Page/中华人民共和国.md "wikilink")，在新解放区继续进行[土地改革运动](../Page/土地改革运动.md "wikilink")。1950年初期，中国政府基本完成了全国范围内的土地改革。3亿多农民无偿分得土地、生产资料，并免除地租。此次改革结束了中国数千年[封建地主制度](../Page/封建.md "wikilink")。

1953年2月15日，[中共中央做出](../Page/中共中央.md "wikilink")《关于农业生产互助合作的决议》，农民和地主等個人一同失去土地[所有权](../Page/所有权.md "wikilink")，僅有[使用權](../Page/使用權.md "wikilink")。\[1\]至此农村土地收归集体，个人不再拥有土地所有权。同年起推行的[统购统销政策](../Page/统购统销.md "wikilink")，又使[政府控制了](../Page/中华人民共和国政府.md "wikilink")[粮食](../Page/粮食.md "wikilink")、[棉花等主要](../Page/棉花.md "wikilink")[农产品资源的生产和销售](../Page/农产品.md "wikilink")。这项政策一直持续到1980年代，通过抽取农产品和[工业品之间的](../Page/工业品.md "wikilink")“[剪刀差](../Page/剪刀差.md "wikilink")”等方式，[农民被政府](../Page/中华人民共和国农民.md "wikilink")[剥削](../Page/剥削.md "wikilink")，最终“一贫如洗”\[2\]。之後人民公社和文革期間人民勞動積極性不足，長期農產量萎縮並發生[三年困难时期饑荒](../Page/三年困难时期.md "wikilink")。而当时实行的[全民计划供给体制](../Page/中华人民共和国居民计划供给体制.md "wikilink")，除了[粮食](../Page/粮食.md "wikilink")、[布料外](../Page/布票.md "wikilink")，占人口绝大多数的中国农民基本被排除出日常物资供给。

1980年代[改革開放中国政府开始推行](../Page/改革開放.md "wikilink")[家庭联产承包责任制](../Page/家庭联产承包责任制.md "wikilink")，將農作物私有化以調動人的勞動積極性，短時間內就解決了饑荒問題和奠定[工業化社會所需的糧食供應穩定](../Page/工業化.md "wikilink")。之後引入[地膜等農業科技大幅提高了農產量](../Page/地膜.md "wikilink")，21世紀後中國僅[小麥產量就為世界第一其次為印度美國](../Page/小麥.md "wikilink")，2005年中央下令廢除[農業稅結束了](../Page/農業稅.md "wikilink")2600年的「皇糧國稅」歷史。

農業科技化方面早在1973年間中國科學家[袁隆平使用](../Page/袁隆平.md "wikilink")[雜交水稻技術](../Page/雜交水稻.md "wikilink")，首次育成三系雜交高產水稻，1975年國務院投入大批人力物力支持得以發揚光大，2004年超級雜交稻實現百畝示範產800公斤後中央下令全面推廣，2007年中國大陸的水稻產量為5億噸，目前中國也是世界上最大稻米生產國，各類稻種合計佔世界稻米35%產量。近代也開始用重离子束加速器、宇宙射线、伽马射线等航天技術跨業支援，對农作物实施诱发突变育種，例如“鲁原502小麥”等成功突變種出現，進一步提高產量。\[3\]

另一方面官方依然繼續加強增加農產量的方法，包含使用[基因工程技術](../Page/基因工程.md "wikilink")，有自力研發也有和國際大糧商合作，但也在2007年後發生[BT63基因改造水稻事件等意外事件可總體前進路線不變](../Page/BT63基因改造水稻事件.md "wikilink")，大致而言中國糧食產量至今依每年增產中，官方也將[糧食安全有最低自給自足能力奉為圭臬](../Page/糧食安全.md "wikilink")。

|       |         |         |         |         |        |        |        |        |
| ----- | ------- | ------- | ------- | ------- | ------ | ------ | ------ | ------ |
| 年     | 2013    | 2014    | 2015    | 2016    | 2017   | 2018   | 2019   | 2020   |
| 糧食生產  | 60635   | 61338   | 61954   | 62862   | 61546  | 62435  | 62515  | 63399  |
| 糧食消費  | 63414   | 64769   | 66153   | 68565   | 71006  | 71877  | 72678  | 73411  |
| 糧食自給率 | 96%     | 94.7%   | 94%     | 91%     | 86.6%  | 86.8%  | 86%    | 86.3%  |
| 穀物生產  | 54137   | 54696   | 55108   | 55904   | 56084  | 56942  | 57730  | 58103  |
| 穀物消費  | 53128   | 53890   | 54663   | 55447   | 56242  | 57048  | 57867  | 58696  |
| 穀物自給率 | 101.90% | 101.50% | 100.81% | 100.82% | 99.72% | 99.81% | 99.76% | 98.99% |

中華人民共和國糧食自給率現狀與預測(单位：万吨)\[4\]

## 种植业

[China_agricultural_1986.jpg](https://zh.wikipedia.org/wiki/File:China_agricultural_1986.jpg "fig:China_agricultural_1986.jpg")
[WheatYield.png](https://zh.wikipedia.org/wiki/File:WheatYield.png "fig:WheatYield.png")
[Terrace_field_yunnan_china.jpg](https://zh.wikipedia.org/wiki/File:Terrace_field_yunnan_china.jpg "fig:Terrace_field_yunnan_china.jpg")[元阳的](../Page/元阳.md "wikilink")[水稻梯田](../Page/水稻.md "wikilink")\]\]
[Qichun_countryside.JPG](https://zh.wikipedia.org/wiki/File:Qichun_countryside.JPG "fig:Qichun_countryside.JPG")農村\]\]
[Qufu_Agricultural_Pest_Control_Center_-_P1060321.JPG](https://zh.wikipedia.org/wiki/File:Qufu_Agricultural_Pest_Control_Center_-_P1060321.JPG "fig:Qufu_Agricultural_Pest_Control_Center_-_P1060321.JPG")
种植业主要分布在中国东部半湿润和湿润的平原地区，80年代起大量利用[地膜技術增產](../Page/地膜.md "wikilink")。

[秦岭](../Page/秦岭.md "wikilink")-[淮河一线以北的中国北方地区](../Page/淮河.md "wikilink")，农作物多为一年一熟、两年三熟或一年二熟，主要农作物有[小麦](../Page/小麦.md "wikilink")、[花生](../Page/花生.md "wikilink")、[甜菜](../Page/甜菜.md "wikilink")、[玉米](../Page/玉米.md "wikilink")、[大豆等](../Page/大豆.md "wikilink")；[秦岭](../Page/秦岭.md "wikilink")-[淮河一线以南的南方地区](../Page/淮河.md "wikilink")，农作物多为一年二熟或一年三熟，主要农作物有水稻、[油菜](../Page/油菜.md "wikilink")、甘蔗等。

|      |             |               |               |                |
| ---- | ----------- | ------------: | ------------: | -------------: |
|      | **產物**\[5\] | **1949**產量(噸) | **1978**產量(噸) | **1999** 產量(噸) |
| 1\.  | 糧食類         |   113,180,000 |   304,770,000 |    508,390,000 |
| 2\.  | 棉花          |       444,000 |     2,167,000 |      3,831,000 |
| 3\.  | 油菜          |     2,564,000 |     5,218,000 |     26,012,000 |
| 4\.  | 甘蔗          |     2,642,000 |    21,116,000 |     74,700,000 |
| 5\.  | 甜菜          |       191,000 |     2,702,000 |      8,640,000 |
| 6\.  | 烤菸草         |        43,000 |     1,052,000 |      2,185,000 |
| 7\.  | 茶業          |        41,000 |       268,000 |        676,000 |
| 8\.  | 水果          |     1,200,000 |     6,570,000 |     62,376,000 |
| 9\.  | 肉類          |     2,200,000 |     8,563,000 |     59,609,000 |
| 10\. | 水產類         |       450,000 |     4,660,000 |     41,220,000 |

  - **粮食产量**：6億0710万吨
      - 夏粮：1億3660万吨
      - 秋粮：4億3649万吨
      - 早稻：3401万吨
  - **[穀物产量](../Page/穀物.md "wikilink")**：5億5727万吨
      - **[稻米](../Page/稻米.md "wikilink")**：2億0643万吨
      - **[小麦](../Page/小麦.md "wikilink")**：1億2617万吨
      - **[玉米](../Page/玉米.md "wikilink")**：2億1567万吨
  - **[棉花產量](../Page/棉花.md "wikilink")**：616万吨
  - **[油料产量](../Page/油料.md "wikilink")**：3517万吨
  - **[糖料产量](../Page/糖料.md "wikilink")**：1億3403万吨
  - **[茶叶产量](../Page/茶叶.md "wikilink")**：209万吨

中国大陸主要商品粮基地：

  - [三江平原](../Page/三江平原.md "wikilink")
  - [松嫩平原](../Page/松嫩平原.md "wikilink")
  - [江淮地区](../Page/江淮地区.md "wikilink")
  - [太湖平原](../Page/太湖平原.md "wikilink")
  - [鄱阳湖平原](../Page/鄱阳湖平原.md "wikilink")
  - [洞庭湖平原](../Page/洞庭湖平原.md "wikilink")
  - [江汉平原](../Page/江汉平原.md "wikilink")
  - [成都平原](../Page/成都平原.md "wikilink")
  - [珠江三角洲](../Page/珠江三角洲.md "wikilink")

**种植面积**：112萬7400平方公里

  - [油料植物](../Page/油料植物.md "wikilink")：14萬0800平方公里
  - [棉花](../Page/棉花.md "wikilink")：4萬2200平方公里
  - [糖料](../Page/糖料.md "wikilink")：1萬9100平方公里

## 林业

林业主要分布在中国东北和西南的天然林区，以及东南部的人工林区。

  - 林業總產值：3兆人民幣(2011年)
  - 人造板產量：2.09億立方米(2011年)
  - 木竹地板產量：6.29億平方公尺(2011年)
  - 經濟木材產量：1.34公噸(2011年)居世界首位\[6\]
  - 木材進出口總值：230億美元(2011年)
  - 木材产量：8178万立方米(2014年)

## 畜牧业

畜牧业主要分布在中国西部地区。

中国四大牧区：

  - 新疆牧区
  - 青海牧区
  - 西藏牧区
  - 内蒙古牧区

产量：

  - **肉类总产量**：8707万吨
      - **猪**肉产量：5671万吨
      - **禽**肉产量：1751万吨
      - **牛**肉产量：689万吨
      - **羊**肉产量：428万吨
  - **禽蛋产量**：2894万吨
  - **牛奶产量**：3725万吨
  - **生猪存栏**：4億6583万头
  - **生猪出栏**：7億3510万头

## 渔业

渔业主要分布在中国东部地区。东部沿海地区是中国海洋捕捞和海洋养殖的基地；长江中下游地区是中国淡水渔业最发达的地区。

中国渔民在从事海洋捕捞时，与他国政府部门发生冲突的事件日益引起关注。[中国和韩国之间的渔业冲突尤其严重](../Page/中韩渔业冲突.md "wikilink")。

  - **水产品产量**：6450万吨
      - [养殖](../Page/养殖.md "wikilink")：4762万吨
      - [捕捞](../Page/捕捞.md "wikilink")：1688万吨

## 国际合作

加拿大-中国农业与食品发展交流中心 （Canada-China Agriculture and Food Development
Exchange Center, CCAgr) 是中国与加拿大农业食品合作最大的平台之一。

## 参考文献

## 外部連結

  - [The Dragon and the Elephant: Agricultural and Rural Reforms in
    China and
    India](https://web.archive.org/web/20141226061002/http://www.ifpri.org/sites/default/files/publications/mtidp87.pdf)
    Edited by Ashok Gulati and Shenggen Fan (2007), Johns Hopkins
    University Press
  - Hsu, Cho-yun. *Han Agriculture* (Washington U. Press, 1980)
  - [Official Statistics from
    FAO](http://www.fao.org/countryprofiles/index.asp?lang=en&iso3=CHN&subj=4)
  - [Farmers, Mao, and Discontent in China: From the Great Leap Forward
    to the Present](http://monthlyreview.org/091214dongping.php) by
    Dongping Han, *[Monthly
    Review](../Page/Monthly_Review.md "wikilink")*, November 2009

## 参见

  - [中国农业](../Page/中国农业.md "wikilink")
  - [中华人民共和国农业部](../Page/中华人民共和国农业部.md "wikilink")
  - [山稻](../Page/山稻.md "wikilink")

{{-}}

[Category:中华人民共和国产业](../Category/中华人民共和国产业.md "wikilink")
[中华人民共和国农业](../Category/中华人民共和国农业.md "wikilink")
[Category:中国各朝代农业](../Category/中国各朝代农业.md "wikilink")

1.  [《关于农业生产互助合作的决议》正式通过](http://cpc.people.com.cn/GB/64162/64165/77552/77569/5328263.html)
2.
3.  [中華網-航天與農業](https://military.china.com/important/11132797/20190408/35608745.html)
4.  [國家統計局-論糧食自給](http://www.stats.gov.cn/tjzs/tjsj/tjcb/dysj/201503/t20150313_693961.html)
5.  [Beijing Official Website
    International](http://ebeijing.gov.cn/BeijingInfo/BJInfoTips/BeijingFigures/t934770.htm)
    . Ebeijing.gov.cn. Retrieved on 2012-02-14.
6.  [1](http://cfdb.forestry.gov.cn:443/showpdf.action). 中国林业发展报告2012.