**中华人民共和国教育**介绍[中华人民共和国建国以来的教育概况](../Page/中华人民共和国.md "wikilink")。中华人民共和国教育事业由[中华人民共和国教育部主管](../Page/中华人民共和国教育部.md "wikilink")。

[清代末期](../Page/清代.md "wikilink")，[中國才開始出現現代教育方式的學校](../Page/中國.md "wikilink")。中华人民共和国成立后，教育事业又得到进一步发展。中國自1986年以來實行為期9年的[義務教育](../Page/義務教育.md "wikilink")，包含6年[小學和](../Page/小學.md "wikilink")3年[初級中學](../Page/初級中學.md "wikilink")，學生年齡在6歲至15歲之間\[1\]。2010年，約有82.5%的學生會選擇繼續為期3年的[高級中學教育](../Page/高級中學.md "wikilink")\[2\]。由政府舉辦的[普通高等學校招生全國統一考試](../Page/普通高等學校招生全國統一考試.md "wikilink")，則是進入大多數[高等教育機構的先決條件](../Page/高等教育.md "wikilink")。2010年，27%的高級中學畢業生會繼續接受4年的[本科教育](../Page/本科教育.md "wikilink")\[3\]。另外政府還設立中級和高級[專科學校](../Page/專科學校.md "wikilink")，提供學生職業教育內容\[4\]。

2016年，中國共計有177,633所小學、52,118所初級中學、13,383所高級中學、10,893所中等职业學校、以及2,596所高等教育機構\[5\]。截至2010年為止，15歲以上人口中有94%接受過教育訓練、而得以[識字](../Page/識字.md "wikilink")\[6\]；相比之下，在1950年只有20%的民眾曾接受過教育\[7\]。

2011年，約有81.4%民眾在不同地方接受過中學教育後畢業\[8\]。而自2005年開始，中國各級政府便籌划经费以減免農村地區孩童的學雜費\[9\]。2006年，政府更承諾9年[義務教育完全免費](../Page/義務教育.md "wikilink")，並提供小學和初級中學階段的課本和學費等補助\[10\]。這使得政府每年教育預算從2003年不到500億美元，增加至2011年超過2,500億美元的預算\[11\]。2009年的[國際學生能力評估計劃中](../Page/國際學生能力評估計劃.md "wikilink")，上海市學生於數學、科學和文化領域上取得世界上最好的成績\[12\]。但儘管中國學生在學習成果評估上獲得極高成績，中國教育面臨著地方和國際社會的多方批評，認為過度強調記憶能力、及農村和城市間具有懸殊的教育品質差距問題\[13\]。

中國各地教育資源分配嚴重不均，每名學生的教育支出因其所在地有所差異。2010年，北京市投資的每人教育總額達[人民幣](../Page/人民幣.md "wikilink")20,023元，但中國[最貧窮的省份](../Page/中國省級行政區人均地區生產總值列表.md "wikilink")[貴州省只有人民幣](../Page/貴州省.md "wikilink")3,204元\[14\]。由于多年来實行依戶籍學區入學的政策，部分学校附近的房价因而暴涨\[15\]。今日中國許多[大學教育朝向國際化發展](../Page/中國內地高等學校列表.md "wikilink")\[16\]，中國著名的高等學府有[清華大學](../Page/清華大學.md "wikilink")、[北京大學](../Page/北京大學.md "wikilink")、[南開大學](../Page/南開大學.md "wikilink")、[復旦大學](../Page/復旦大學.md "wikilink")、[上海交通大學](../Page/上海交通大學.md "wikilink")、[南京大學](../Page/南京大學.md "wikilink")，和[浙江大學等](../Page/浙江大學.md "wikilink")\[17\]\[18\]，及位於[香港但獨立於中國教育制度和歷史的](../Page/香港.md "wikilink")[香港大學](../Page/香港大學.md "wikilink")、[香港中文大學](../Page/香港中文大學.md "wikilink")、[香港科技大學等](../Page/香港科技大學.md "wikilink")\[19\]\[20\]。

## 歷史与现状

[中華人民共和國成立後](../Page/中華人民共和國.md "wikilink")，教育模式参照[蘇聯發展](../Page/蘇聯.md "wikilink")，將原先的[綜合性大學](../Page/綜合性大學.md "wikilink")[進行拆分](../Page/院系調整.md "wikilink")。由於建国之初需要大量的工業技術人才，故當時中國創建了大量的理工科學院和技术学院。50年代初，中國開展了大規模的掃盲運動，掃盲運動的高潮一直持續到50年代末。\[21\]其间，[高考制度于](../Page/高考.md "wikilink")1955年正式設立。1960-1970年代，中國共產黨发动了[上山下乡运动](../Page/上山下乡运动.md "wikilink")，组织了大量城市「知識青年」離開城市，到農村勞動和定居。直到1970年代末開始，大部分知青陸續回到城市，仍有部分於農村生活。\[22\]

1966年[文化大革命的爆發導致全國所有學校進入停課狀態](../Page/文化大革命.md "wikilink")，大學入學考試被取消。[知識分子不被尊重](../Page/知識分子.md "wikilink")，大多數被下放進行體力勞動，有些則遭到殘酷對待，財產被沒收，被批為「[臭老九](../Page/臭老九.md "wikilink")」等。\[23\]许多正在接受教育的人被迫停止繼續教育；沒有接受教育的人喪失了機會，對中國的人口素質和教育事业造成了嚴重的破壞。同时邓小平还认为，文革造成大多学生习惯了‘文革’时瞎捣乱的习气。无政府主义猖獗，大家不守纪律，以为标新立异、我行我素就是英雄，谁也不能拿我怎么样。\[24\]但文革後期知識分子下鄉後，基礎掃盲工作逐漸回到正常軌道。\[25\]

\[26\]自九十年代後期以来，中國開始實行[大學擴招政策](../Page/大學擴招.md "wikilink")，更多的人得到了前往大学学习的机会。但随着时间的推移，2010年代，中国大学教育开始出现进一步改革的尝试，为教育事业的提升和发展带来了一丝曙光。\[27\]

## 學前教育

<table>
<caption>中國教育制度</caption>
<thead>
<tr class="header">
<th><p>教育名称</p></th>
<th><p>等级</p></th>
<th><p>强制程度</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/高等教育.md" title="wikilink">高等教育</a></p></td>
<td><p><a href="../Page/研究生.md" title="wikilink">研究生</a></p></td>
<td><p><a href="../Page/博士.md" title="wikilink">博士</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/碩士.md" title="wikilink">碩士</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/本科.md" title="wikilink">本科</a></p></td>
<td><p><a href="../Page/學士.md" title="wikilink">學士</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>高等<a href="../Page/专科學校.md" title="wikilink">專科</a>（无<a href="../Page/学位.md" title="wikilink">学位</a>）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/高级中学.md" title="wikilink">高级中学</a><br />
（普通高中、<a href="../Page/中專.md" title="wikilink">中專</a>、<a href="../Page/中职.md" title="wikilink">中职</a>）</p></td>
<td><p>高三</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>高二</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>高一</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中国义务教育.md" title="wikilink">九年义务教育</a></p></td>
<td><p>六三制</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/初级中学.md" title="wikilink">初级中学</a></p></td>
<td><p>九年级</p></td>
<td><p><a href="../Page/初级中学.md" title="wikilink">初级中学</a></p></td>
</tr>
<tr class="even">
<td><p>八年级</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>七年级</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小学.md" title="wikilink">小学</a></p></td>
<td><p>六年级</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>五年级</p></td>
<td><p><a href="../Page/小学.md" title="wikilink">小学</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>四年级</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>三年级</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>二年级</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>一年级</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>幼儿园</p></td>
<td><p>学前班</p></td>
<td><p>非强制</p></td>
</tr>
<tr class="odd">
<td><p>大班</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>中班</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>小班</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 九年义务教育

自2017年起，在中华人民共和国境内，所有义务教育学生（包括民办学校学生），一律不收学费、杂费、书本费，并补助家庭经济困难寄宿生生活费，俗称“[两免一补](../Page/两免一补.md "wikilink")”。\[28\]惟各个学校可能依据实际情况，收取作业本费、校服费、军训费、伙食费、寄宿制学校住宿费等。\[29\]

近年来，不少人提议中国应当实施上至学前班、下至高级中等学校的十三年义务教育。\[30\]

### 初等教育

[缩略图](https://zh.wikipedia.org/wiki/File:China_Truants.jpg "fig:缩略图")

#### 小学升入初中

### 初级中等教育

[School_uniforms_in_China_-_01.jpg](https://zh.wikipedia.org/wiki/File:School_uniforms_in_China_-_01.jpg "fig:School_uniforms_in_China_-_01.jpg")的中國中學生\]\]

[缩略图](https://zh.wikipedia.org/wiki/File:Meeting_of_the_morning_of_the_elementary_school,Shangai,China.jpg "fig:缩略图")

#### 初中升入高中或中职（中考）

## 高级中等教育和中等职业教育

### 高级中等教育

[缩略图](https://zh.wikipedia.org/wiki/File:卷子-現代漢語辭典厚度比較.jpg "fig:缩略图")出版的的《[現代漢語詞典](../Page/現代漢語詞典.md "wikilink")》。\]\]

<table>
<caption>普通高中课程大纲规定的各科目必修、选修内容</caption>
<thead>
<tr class="header">
<th><p>width= 80px | 科目</p></th>
<th><p>必修</p></th>
<th><p>选修</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><div style="text-align: center;">
<p><strong>公共学科</strong></p>
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/语文.md" title="wikilink">语文</a></p></td>
<td><p>必修1、必修2、必修3、必修4、必修5</p></td>
<td><p>中外传记作品选读、外国小说欣赏、中国古代诗歌散文欣赏、中国现代诗歌散文欣赏、语言文字应用、先秦诸子选读、中国小说欣赏、文章写作与修改、影视名作欣赏、中外戏剧名作欣赏、外国诗歌散文欣赏、演讲与辩论、中国文化经典研读、新闻阅读与实践、中国民俗文化</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/数学.md" title="wikilink">数学</a></p></td>
<td><p><strong>必修1</strong>*、<strong>必修2</strong>*、<strong>必修3</strong>*、<strong>必修4</strong>*、<strong>必修5</strong>*</p></td>
<td><p><strong>选修1－1*</strong>、<strong>选修1－2*</strong>、<strong>选修2－1**</strong>、<strong>选修2－2**</strong>、<strong>选修2－3**</strong>、 选修3－1（数学史选讲）、<font color=#808080>选修3－2（信息安全与密码）</font>、选修3－3（球面上的几何）、选修3－4（对称与群）、<font color=#808080>选修3－5（欧拉公式与闭曲面分类）</font>、<font color=#808080>选修3－6（三等分角与数域扩充）</font>、选修4－1（几何证明选讲）、选修4－2（矩阵与变换）、<font color=#808080>选修4－3（数列与差分）</font>、<strong>选修4－4（坐标系与参数方程）、选修4－5（不等式选讲）</strong>、选修4－6（初等数论初步）、选修4－7（优选法与试验设计初步）、<font color=#808080>选修4－8（统筹法与图论初步）</font>、选修4－9（风险与决策）、<font color=#808080>选修4－10（开关电路与布尔代数）</font></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/英语.md" title="wikilink">英语</a></p></td>
<td><p>必修1、必修2、必修3、必修4、必修5</p></td>
<td><p>选修6、选修7、选修8、选修9、选修10、选修11、选修（语言知识与技能类：<font color=#808080>初级英语语法与修辞、英汉初级笔译、</font>英语应用文写作<font color=#808080>、英语报刊阅读、英语演讲与辩论</font>）、选修（语言应用类：<font color=#808080>文秘英语、科技英语、</font>信息技术英语、<font color=#808080>初级旅游英语</font>、初级财经英语）、选修（欣赏类：英语文学欣赏入门、<font color=#808080>英语影视欣赏入门、英语戏剧与表演入门、英语歌曲欣赏</font>）</p></td>
</tr>
<tr class="odd">
<td><div style="text-align: center;">
<p><strong>文科综合</strong></p>
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/思想政治.md" title="wikilink">思想政治</a></p></td>
<td><p><strong>必修1</strong>*（经济生活）、<strong>必修2</strong>*（政治生活）、<strong>必修3</strong>*（文化生活）、<strong>必修4</strong>*（生活与哲学）</p></td>
<td><p>选修1（科学社会主义常识）、选修2（经济学常识）、选修3（国家和国际组织常识）、选修4（科学思维常识）、选修5（生活中的法律常识）、选修6（公民道德与伦理常识）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/历史.md" title="wikilink">历史</a></p></td>
<td><p><strong>必修1*</strong>（政治史）、<strong>必修2*</strong>（经济史）、<strong>必修3*</strong>（文化和科技史）</p></td>
<td><p><strong>选修1（历史上重大改革回眸）</strong>、选修2（近代社会的民主思想与实践）、<strong>选修3（20世纪的战争与和平）、选修4（中外历史人物评说）</strong>、选修5（探索历史的奥秘）、选修6（世界文化遗产荟萃）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/地理.md" title="wikilink">地理</a></p></td>
<td><p><strong>必修1*</strong>（自然地理）、<strong>必修2*</strong>（人文地理）、<strong>必修3*</strong>（区域地理）</p></td>
<td><p>选修1（宇宙与地球）、选修2（海洋地理）、<strong>选修3（旅游地理）</strong>、选修4（城乡规划）、选修5（自然灾害与防治）、<strong>选修6（环境保护）</strong>、选修7（地理信息技术与应用）</p></td>
</tr>
<tr class="odd">
<td><div style="text-align: center;">
<p><strong>理科综合</strong></p>
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/物理.md" title="wikilink">物理</a></p></td>
<td><p><strong>必修1</strong>*、<strong>必修2</strong>*</p></td>
<td><p>选修1－1、选修1－2 ；选修2－1、选修2－2、选修2－3 ；<strong>选修3－1*</strong>、<strong>选修3－2*</strong>、<strong>选修3－3、选修3－4</strong>、<strong>选修3－5*</strong>[31]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/化学.md" title="wikilink">化学</a></p></td>
<td><p><strong>必修1*、必修2*</strong></p></td>
<td><p>选修1（化学与生活）、选修2（化学与技术）、<strong>选修3（物质结构与性质）</strong>、<strong>选修4（化学反应原理）*</strong>、<strong>选修5（有机化学基础）</strong>、选修6（实验化学）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/生物.md" title="wikilink">生物</a></p></td>
<td><p><strong>必修1*</strong>（分子与细胞）、<strong>必修2*</strong>（遗传与进化）、<strong>必修3*</strong>（稳态与环境）</p></td>
<td><p><strong>选修1（生物技术实践）</strong>、选修2（生物科学与社会）、<strong>选修3（现代生物科技专题）</strong></p></td>
</tr>
<tr class="odd">
<td><div style="text-align: center;">
<p><strong>其他选考外语</strong></p>
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/俄语.md" title="wikilink">俄语</a></p></td>
<td><p>必修1、必修2、必修3、必修4、必修5</p></td>
<td><p>选修1- 1、选修1-2、选修1-3、选修1-4；<font color=#808080>选修2-1（语言与文化）、选修2-2（文学与艺术）、选修2-3（经贸俄语）、选修2-4（旅游俄语）、选修2-5（科技俄语）</font></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/日语.md" title="wikilink">日语</a></p></td>
<td><p>必修1、必修2、必修3、必修4、必修5</p></td>
<td><p>选修6、选修7、<font color=#808080>选修2-1（影视欣赏入门）</font>、选修2-2（小品演剧入门）、选修2-3（作品欣赏入门）、选修2-4（应用写作入门）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/法语.md" title="wikilink">法语</a></p></td>
<td><p><small>不适用</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/德语.md" title="wikilink">德语</a></p></td>
<td><p><small>不适用</small></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西班牙语.md" title="wikilink">西班牙语</a></p></td>
<td><p><small>不适用</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><small><strong>粗体*</strong>：《普通高等学校招生全国统一考试大纲》中规定的必考内容，未明确提及的科目不予标注<br />
<strong>粗体</strong>：《普通高等学校招生全国统一考试大纲》中规定的选考内容<br />
<strong>粗体**</strong>：理科数学额外必考的科目<br />
<font color=#808080>灰色</font>：未有课本的选修内容<br />
不适用：暂未编订课程标准的学科。</small></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<noinclude>

#### 高考

[缩略图](https://zh.wikipedia.org/wiki/File:National_Higher_Education_Entrance_Examination_NK.JPG "fig:缩略图")高考橫幅\]\]

### 中等职业教育

## 高等教育

### 統計數據

  - 小學淨入學率：99.7%(2013年)、小學升學率：98.3%(2013年)、初中升學率：91.2%(2013年)、高中升學率：87.6%(2013年)\[32\]
  - 各級學校數量，小學：177,633所，初中：52,118所，中等職業學校：10,893所，高中：13,383所，大學2,596所

## 批評

[韩寒在其著作中明确反对中国大陆式的教育体制](../Page/韩寒.md "wikilink")，认为其存在诸多问题，并且不能归咎为中国人口繁多。\[33\]

## 参考文献

## 扩展阅读

  - Borjigin, Monkbat. "[A case study of Language education in the Inner
    Mongolia](http://mitizane.ll.chiba-u.jp/metadb/up/AA11256001/21857148_16_17.pdf)"
    ([Archive](http://www.webcitation.org/6cHL8AD7z); Japanese title:
    内モンゴル自治区における言語教育について ). *Journal of Chiba University Eurasian
    Society* (千葉大学ユーラシア言語文化論集) 16, 261-266, 2014-09-25. Chiba University
    Eurasian Society (千葉大学ユーラシア言語文化論講座). [See profile
    at](https://web.archive.org/web/20151016091736/http://mitizane.ll.chiba-u.jp/meta-bin/mt-pdetail.cgi?cd=00117993)
    Chiba University Repository. [See profile
    at](https://web.archive.org/web/20151016091736/http://ci.nii.ac.jp/naid/120005476923)
    [CiNii](../Page/CiNii.md "wikilink"). - In English with a Japanese
    abstract.

## 外部連結

  - [有關中國教育的紀錄片（采風電影紀錄片 DVD
    發行）](http://www.visiblerecord.com/zh/films/?subject=中國教育)

## 參見

  - [素质教育](../Page/素质教育.md "wikilink")、[应试教育](../Page/应试教育.md "wikilink")
  - [普通高等學校招生全國統一考試](../Page/普通高等學校招生全國統一考試.md "wikilink")
  - [民辦大學](../Page/民辦大學.md "wikilink")
  - [中國大學列表](../Page/中國大學列表.md "wikilink")、[中國職業學校列表](../Page/中國職業學校列表.md "wikilink")
  - [北京教育](../Page/北京教育.md "wikilink")、[上海教育](../Page/上海教育.md "wikilink")、[香港教育](../Page/香港教育.md "wikilink")、[澳门教育](../Page/澳门教育.md "wikilink")
  - [博杭特中学的中国实验班](../Page/博杭特中学的中国实验班.md "wikilink")

{{-}}

[China](../Category/亚洲各国教育.md "wikilink")
[Category:中国各朝代教育](../Category/中国各朝代教育.md "wikilink")
[中华人民共和国教育](../Category/中华人民共和国教育.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.

12.

13.
14.

15.

16.

17.

18.

19.

20.

21.

22.

23.

24.

25.

26.

27.

28.

29.

30.

31.

32. [中國統計年鑑 2014](http://www.stats.gov.cn/tjsj/ndsj/2014/indexch.htm)

33.