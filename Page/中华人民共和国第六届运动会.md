</div>

</div>

**中华人民共和国第六届全国运动会**于1987年11月20日至12月5日在[广州舉行](../Page/广州.md "wikilink")。开幕式於11月20日下午在新落成的广州[天河体育中心举行](../Page/天河体育中心.md "wikilink")，出席开幕式的中央领导人有[赵紫阳](../Page/赵紫阳.md "wikilink")、[万里](../Page/万里.md "wikilink")、[田纪云](../Page/田纪云.md "wikilink")、[秦基偉等](../Page/秦基偉.md "wikilink")，[国际奥委会主席](../Page/国际奥委会.md "wikilink")[萨马兰奇也应邀出席了本届全运会的开幕式](../Page/萨马兰奇.md "wikilink")。

適逢是剛好[無線電視](../Page/TVB.md "wikilink")20週年台慶及[歡樂今宵首播](../Page/EYT.md "wikilink")20週年,
1987年11月20日[無綫電視](../Page/無綫電視.md "wikilink")[翡翠台用人造衛星直播全個開幕禮](../Page/翡翠台.md "wikilink"),並有[韓毓霞](../Page/韓毓霞.md "wikilink"),[鍾保羅擔任主持](../Page/鍾保羅.md "wikilink").在廣州外景的主持有林慧玲及鄧世勤.
當晚[歡樂今宵完後並且播出](../Page/EYT.md "wikilink")『全運群英會』

參與開幕式的香港藝人有[汪明荃](../Page/汪明荃.md "wikilink")、[張德蘭](../Page/張德蘭.md "wikilink")、[蔡楓華](../Page/蔡楓華.md "wikilink")、[徐小鳳](../Page/徐小鳳.md "wikilink")。閉幕式於12月5日晚舉行，[中共中央政治局常委](../Page/中共中央政治局常委.md "wikilink")、[國務院代總理](../Page/國務院總理.md "wikilink")[李鵬宣佈運動會閉幕](../Page/李鵬.md "wikilink")。

## 會徽

會徽由「6」和「羊」組成的火炬和跑道首次將信息通過象形的物體表達出來，以火炬的一朵火焰變型为數字「6」，意味著第六屆全運會，以「羊」字的三横引申為跑道，表示全運會在廣州（羊城）舉行。

此外，第六屆全運會開創了中國體育史上多個「第一」：

  - 第一個吉祥物：「陽陽」，這隻天真可愛、憨態可掬的小羊，面帶微笑，右手高擎着火炬，身着印有全運會會徽的紅色背心，跑步向前。
  - 第一屆由地方政府自籌資金舉辦的全國性運動會；
  - 第一次使用會歌：《中華之光》，[鄭秋楓作曲](../Page/鄭秋楓.md "wikilink")，[瞿琮作詞](../Page/瞿琮.md "wikilink")；
  - 第一次成立服務總公司負責集資；
  - 六運會會徽、吉祥物的專利權第一次以商品經營的形式，出現在[富士等公司產品的包裝上](../Page/富士菲林.md "wikilink")；
  - 第一次发發行體育彩票（共發行22期共7000萬張），籌集到3000萬元[人民幣](../Page/人民幣.md "wikilink")……

## 比赛项目

  - 六运会设有比赛项目44项，表演项目3项
  - 比赛项目有：[足球](../Page/足球.md "wikilink")、[篮球](../Page/篮球.md "wikilink")、[排球](../Page/排球.md "wikilink")、[乒乓球](../Page/乒乓球.md "wikilink")、[网球](../Page/网球.md "wikilink")、[羽毛球](../Page/羽毛球.md "wikilink")、[手球](../Page/手球.md "wikilink")、[曲棍球](../Page/曲棍球.md "wikilink")、女子[垒球](../Page/垒球.md "wikilink")、[棒球](../Page/棒球.md "wikilink")、[田径](../Page/田径.md "wikilink")、[游泳](../Page/游泳.md "wikilink")、[跳水](../Page/跳水.md "wikilink")、[水球](../Page/水球.md "wikilink")、[花样游泳](../Page/花样游泳.md "wikilink")、[举重](../Page/举重.md "wikilink")、[体操](../Page/競技體操.md "wikilink")、[艺术体操](../Page/艺术体操.md "wikilink")、[击剑](../Page/击剑.md "wikilink")、[柔道](../Page/柔道.md "wikilink")、[国际式摔跤](../Page/国际式摔跤.md "wikilink")、[中国式摔跤](../Page/中国式摔跤.md "wikilink")、[技巧](../Page/技巧.md "wikilink")、[围棋](../Page/围棋.md "wikilink")、[中国象棋](../Page/中国象棋.md "wikilink")、[国际象棋](../Page/国际象棋.md "wikilink")、[马术](../Page/马术.md "wikilink")、[现代五项](../Page/现代五项.md "wikilink")、[武术](../Page/武術_\(體育運動\).md "wikilink")、[射击](../Page/射击.md "wikilink")、[射箭](../Page/射箭.md "wikilink")、[赛艇](../Page/赛艇.md "wikilink")、[皮划艇](../Page/皮划艇.md "wikilink")、[帆船](../Page/帆船.md "wikilink")、[帆板](../Page/帆板.md "wikilink")、[蹼泳](../Page/蹼泳.md "wikilink")、[航海模型](../Page/航海模型.md "wikilink")、[航空模型](../Page/航空模型.md "wikilink")、[自行车](../Page/自行车.md "wikilink")、[滑水](../Page/滑水.md "wikilink")、[摩托艇](../Page/摩托艇.md "wikilink")、[摩托车越野](../Page/摩托车越野.md "wikilink")、[无线电测向](../Page/无线电测向.md "wikilink")、[跳伞](../Page/跳伞.md "wikilink")
  - 表演项目有：[高尔夫球](../Page/高尔夫球.md "wikilink")、[保龄球](../Page/保龄球.md "wikilink")、[桥牌](../Page/桥牌.md "wikilink")

## 破纪录人数和人次

10人2队17次破15项世界纪录；3人3次平3项世界纪录；2人2次超2项世界纪界；创造或超过48项亚洲纪录和最好成绩；创造85项全国纪录和最好成绩

大會還評出了12個精神文明代表團和192個精神文明運動隊。

## 獎牌榜

| 名次 | 代表團                                 | 金牌   | 銀牌 | 銅牌 |
| -- | ----------------------------------- | ---- | -- | -- |
| 1  | [广东](../Page/广东.md "wikilink")      | 54   | 35 | 36 |
| 2  | [上海](../Page/上海.md "wikilink")      | 32   | 29 | 25 |
| 3  | [辽宁](../Page/辽宁.md "wikilink")      | 32.5 | 30 | 19 |
| 4  | [北京](../Page/北京.md "wikilink")      | 17   | 23 | 25 |
| 5  | [山东](../Page/山东.md "wikilink")      | 12   | 17 | 19 |
| 6  | [四川](../Page/四川.md "wikilink")      | 17   | 19 | 14 |
| 7  | [湖北](../Page/湖北.md "wikilink")      | 15   | 13 | 20 |
| 8  | [江苏](../Page/江苏.md "wikilink")      | 9    | 15 | 16 |
| 9  | [浙江](../Page/浙江.md "wikilink")      | 17.5 | 13 | 8  |
| 10 | [河北](../Page/河北.md "wikilink")      | 11   | 12 | 15 |
| 11 | [解放军](../Page/解放军代表团.md "wikilink") | 13   | 11 | 15 |
| 12 | [湖南](../Page/湖南.md "wikilink")      | 8    | 15 | 10 |
| 13 | [河南](../Page/河南.md "wikilink")      | 11   | 12 | 12 |
| 14 | [安徽](../Page/安徽.md "wikilink")      | 7    | 12 | 16 |
| 15 | [广西](../Page/广西.md "wikilink")      | 3    | 11 | 17 |
| 16 | [云南](../Page/云南.md "wikilink")      | 8    | 7  | 11 |
| 17 | [黑龙江](../Page/黑龍江省.md "wikilink")   | 13   | 8  | 7  |
| 18 | [福建](../Page/福建.md "wikilink")      | 11   | 10 | 10 |
| 19 | [天津](../Page/天津.md "wikilink")      | 7    | 9  | 7  |
| 20 | [内蒙古](../Page/内蒙古.md "wikilink")    | 14   | 10 | 4  |
| 21 | [吉林](../Page/吉林.md "wikilink")      | 5    | 5  | 7  |
| 22 | [陕西](../Page/陕西.md "wikilink")      | 8    | 5  | 3  |
| 23 | [山西](../Page/山西.md "wikilink")      | 6    | 5  | 6  |
| 24 | [江西](../Page/江西.md "wikilink")      | 3    | 4  | 6  |
| 25 | [甘肃](../Page/甘肃.md "wikilink")      | 4    | 4  | 1  |
| 26 | [新疆](../Page/新疆.md "wikilink")      | 0    | 4  | 4  |
| 27 | [贵州](../Page/贵州.md "wikilink")      | 1    | 3  | 3  |
| 28 | [青海](../Page/青海.md "wikilink")      | 1    | 0  | 3  |
| 29 | [火车头](../Page/火车头体协.md "wikilink")  | 1    | 0  | 2  |
| 30 | [银鹰](../Page/银鹰体协.md "wikilink")    | 0    | 0  | 3  |
| 31 | [宁夏](../Page/宁夏.md "wikilink")      | 1    | 0  | 0  |
| 32 | [前卫](../Page/前卫体协.md "wikilink")    | 0    | 1  | 1  |
| 33 | [石油](../Page/石油体协.md "wikilink")    | 0    | 1  | 0  |
| 34 | [西藏](../Page/西藏自治区.md "wikilink")   | 1    | 0  | 1  |
| 35 | [煤矿](../Page/煤矿体协.md "wikilink")    | 0    | 1  | 1  |

## 注释

{{-}}

[Category:1987年中国](../Category/1987年中国.md "wikilink")
[06](../Category/中华人民共和国全国运动会.md "wikilink")
[Category:1987年綜合運動會](../Category/1987年綜合運動會.md "wikilink")
[Category:中华人民共和国广州市事件](../Category/中华人民共和国广州市事件.md "wikilink")
[Category:广州体育史](../Category/广州体育史.md "wikilink")
[Category:1987年11月](../Category/1987年11月.md "wikilink")
[Category:1987年12月](../Category/1987年12月.md "wikilink")