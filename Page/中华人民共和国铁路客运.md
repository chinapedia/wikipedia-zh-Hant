[Shanghai_North_Railway_Station.JPG](https://zh.wikipedia.org/wiki/File:Shanghai_North_Railway_Station.JPG "fig:Shanghai_North_Railway_Station.JPG")

在**[中国](../Page/中国.md "wikilink")**，**[铁路运输](../Page/铁路运输.md "wikilink")**是主要的旅客运输方式之一，由[中华人民共和国](../Page/中华人民共和国.md "wikilink")[交通运输部下辖的](../Page/中华人民共和国交通运输部.md "wikilink")[国家铁路局管理營運](../Page/国家铁路局.md "wikilink")。每年旅客吞吐量超过10亿人次，仅在2008年全国铁路旅客发送量就达到14亿5640万人\[1\]；其中又以每年[春运期间旅客运输量最大](../Page/春运.md "wikilink")，如2006年春运期間共运送旅客1.44亿人次\[2\]。因此春运又被称为“世界最大规模的人口迁徙”。\[3\]\[4\]

## 旅客列车等级及车次

[Tianjin_Station_06.jpg](https://zh.wikipedia.org/wiki/File:Tianjin_Station_06.jpg "fig:Tianjin_Station_06.jpg")
[Z39_40_41_42_infoboard_in_201812.jpg](https://zh.wikipedia.org/wiki/File:Z39_40_41_42_infoboard_in_201812.jpg "fig:Z39_40_41_42_infoboard_in_201812.jpg")

中国铁路列车车次是前[中华人民共和国铁道部](../Page/中华人民共和国铁道部.md "wikilink")、现[中国铁路总公司对不同行驶方向](../Page/中国铁路总公司.md "wikilink")、不同车种、不同行驶区段和不同运行时刻的[列车编订的标示码](../Page/列车.md "wikilink")，以方便区别。车次用[英文字母和](../Page/英文字母.md "wikilink")[阿拉伯数字表示](../Page/阿拉伯数字.md "wikilink")，按行驶方向的不同以单双数来区别。當列车行驶方向为线路的**上行**方向，车次的数字为[双数](../Page/偶数.md "wikilink")；反之经线路**下行**方向运行，车次的数字为[单数](../Page/奇数.md "wikilink")。详见[中国铁路列车车次](../Page/中国铁路列车车次.md "wikilink")。
部分由外国铁路独立担当的国际口岸联运列车在中国大陆境内运行时采用的铁路车次，由中国铁路与外国铁路运输部门单独商议，不受上述规定约束。

## 座席及卧铺等级

  - **动车组商务座**：座位排列为2+1方式。为最高等级的坐席。
  - **动车组特等座**：席位代码P，京津城际动车组、武广高铁动车组和哈大高铁动车组部分车次设有特等座。位于列首和列尾，座位排列为1+2方式，最初仅发售列尾端的特等座车票，现列首端的特等座车票也有发售。
  - **动车组一等座**：席位代码M。座位排列为2+2方式，中间过道较宽阔。按软席办理。
  - **动车组二等座**：席位代码O。座位排列为3+2方式，座位空间相对一等座较小。按软席办理。
  - **硬座**：席位代码1，座位排列为3+2方式，座椅固定。
  - **软座**：席位代码2，座位排列为2+2方式。少部分列车再分为**特等软座**（席位代码T）、**一等软座**（席位代码7）和**二等软座**（席位代码8）。大部分软座车的座椅靠背均能调节角度。
  - **无座**：为最大限度地提升运能，尤其[春运](../Page/春运.md "wikilink")、[暑运期间](../Page/暑运.md "wikilink")，动车组列车、普速列车分别在二等座、硬座车票售罄后开始发售无座车票，票价与列车编组中最低一档座席等级相同。偶有编组中座席车只挂软座而该班列车无座票价与软座票价相同的特例。关于无座票价是否应较有座票价下浮的话题，在社会各界历来广受热议。\[5\]
  - **硬卧**：席位代码3，每間隔左右兩邊有上中下3個鋪位，即每间隔有6个铺位。双层硬卧车每层每个间隔左右两边有上下2个铺位，即每个间隔有4个铺位，上下加起来一共有8个铺位。间隔为开放式或半包式，不设房门。K3/4次列车的YW18车型之包厢硬卧有包厢门，为硬卧特例。
  - **软卧**：席位代码4，每个房间左右兩邊有上下2個鋪位，即每房间有4个铺位，并设房门。双层软卧车上层每个房间左右两边各有1个铺位，即每个房间有2个铺位，下层布局与单层车相若。软卧比硬卧有更大的空间，铺位也较宽阔。目前已开行卧铺动车组列车，车体为CRH1E和CRH2E。
  - **包厢硬卧**：席位代码5，格局类似于普通软卧，但为硬席，不设中铺，但设有房门。多见于国际列车或使用国际列车车体开行的列车。
  - **[高包软卧](../Page/高级软卧.md "wikilink")**：席位代码6，为最高等级的卧铺，每个包厢有2个铺位和一个沙发，并带有独立洗手间。少数列车有每房间仅1个铺位或3铺位的一人软卧包厢（席位代码H）。3铺位的一人软包为一张下铺大床（双人铺位）及一张单人上铺小床（单人铺位），此种包厢一张车票可进三名旅客。此外，部分CRH1E动车组编有高级软卧车厢，每个包厢有2个铺位、沙发和衣柜，但没有独立卫生间。K3/4次国际列车的RW18车型高包软卧（限售伊尔库斯克以远之站点）为2包房公用一个洗漱间，并未设独立卫生间。单层软卧、双层软卧下层、包厢硬卧和高包软卧上下铺判别依据是席位号的奇偶：奇数为下铺，偶数为上铺。

<File:CRH380AL-SW605903>
20140906.JPG|商务座（[CRH380AL型动车组](../Page/和谐号CRH380A型电力动车组.md "wikilink")）
[File:WuGuang-CRH3-G1001-FirstClass.jpg|一等座](File:WuGuang-CRH3-G1001-FirstClass.jpg%7C一等座)（[CRH3C型动车组](../Page/和諧號CRH3型電力動車組.md "wikilink")）
[File:CRH1-Compartment.jpg|二等座](File:CRH1-Compartment.jpg%7C二等座)（[CRH1A型动车组](../Page/和諧號CRH1型電力動車組.md "wikilink")）
<File:YZ22B> 200908.JPG|22型客车硬座
[File:China_Railway_RZ.jpg|25T型客车软座](File:China_Railway_RZ.jpg%7C25T型客车软座)
<File:T99> 100 Hard Sleeper Car.jpg|25T型客车半包式硬卧 <File:BSP> RW25T T27
20091110.jpg|25T型客车软卧 <File:China> Railway RW19T passenger coaches
20100725 363.jpg|19T型客车高包软卧

## 客運車票

## 常旅客计划

2017年12月20日起，铁路部门推出“铁路畅行”常旅客会员服务。年满12岁的旅客申请并完成身份认证后，即可成为常旅客会员，会员积分达到一定额度可兑换相应车票\[6\]。

## 联运列车

[Board_of_Z97-98_(20190331151627).jpg](https://zh.wikipedia.org/wiki/File:Board_of_Z97-98_\(20190331151627\).jpg "fig:Board_of_Z97-98_(20190331151627).jpg")水牌\]\]

### 港九列车（直通车）

[中国铁路总公司与](../Page/中国铁路总公司.md "wikilink")[港铁公司联合营运来往中國內地及](../Page/香港铁路有限公司.md "wikilink")[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[紅磡站的](../Page/紅磡站.md "wikilink")[城际客运服务](../Page/城际客运服务.md "wikilink")（又称直通车），服务路线包括**[廣東綫](../Page/廣九直通車.md "wikilink")**、**[上海綫](../Page/滬九直通車.md "wikilink")**及**[北京綫](../Page/京九直通車.md "wikilink")**。目前每天有12对（24班）广九直通车运行，车次为Z801～Z802、Z807～Z828（11对，[廣州東](../Page/廣州東站.md "wikilink")—香港紅磡，其中9对停靠[常平站](../Page/常平站.md "wikilink")）及Z803～Z806（1对，[佛山](../Page/佛山.md "wikilink")—廣州東—常平—香港紅磡）\[7\]而来往香港紅磡—[北京](../Page/北京西站.md "wikilink")（Z97/Z98次），和香港紅磡—[上海](../Page/上海站.md "wikilink")（Z99/Z100次）的直通车则隔日开行。直通车的旅客在始发站办理出境手续后即可乘坐列车直达目的地才办理當地入境手续，中途无需下车。直通车车票全国及香港联网发售。

### 国际联运

[Beijing_UlaanBaatar_Moscow_Train.jpg](https://zh.wikipedia.org/wiki/File:Beijing_UlaanBaatar_Moscow_Train.jpg "fig:Beijing_UlaanBaatar_Moscow_Train.jpg")、[中文和](../Page/中文.md "wikilink")[蒙古语西里尔字母表達](../Page/蒙古语西里尔字母.md "wikilink")）\]\]

目前中國开行的国际联运列车有前往[蒙古的](../Page/蒙古.md "wikilink")[乌兰巴托](../Page/乌兰巴托.md "wikilink")、[俄罗斯的](../Page/俄罗斯.md "wikilink")[莫斯科](../Page/莫斯科.md "wikilink")、[伯力和](../Page/伯力.md "wikilink")[海参崴](../Page/海参崴.md "wikilink")、[朝鲜的](../Page/朝鲜.md "wikilink")[平壤](../Page/平壤.md "wikilink")、[哈萨克斯坦的](../Page/哈萨克斯坦.md "wikilink")[阿拉木图和](../Page/阿拉木图.md "wikilink")[努尔-苏丹](../Page/阿斯塔纳.md "wikilink")、[越南的](../Page/越南.md "wikilink")[河內等地的列车](../Page/河內.md "wikilink")，旅客在邊境口岸辦理出境手續，例如北京往河內列車，出境手續在廣西憑祥辦理。由于俄羅斯、蒙古的轨距与中國不同，因此列車需要在边境站换輪，而中越兩國的軌距亦不同，但在中國[憑祥至越南](../Page/憑祥.md "wikilink")[諒山](../Page/諒山.md "wikilink")間鋪有[標準軌](../Page/標準鐵軌.md "wikilink")，并延至河内[嘉林站](../Page/嘉林站.md "wikilink")（混合轨）\[8\]，[北京西站开出的](../Page/北京西站.md "wikilink")[Z5/6次列车會開抵南寧](../Page/Z5/6次列车.md "wikilink")，乘客在南寧換乘[T8701/8702次列车](../Page/T8701/8702次列车.md "wikilink")，在凭祥和同登接受边防检查，最后经[河同铁路抵达嘉林站](../Page/河同铁路.md "wikilink")。部分国际列车国外段车票，铁道部指定中国国际旅行社售票并须加收手续费50元/每人张，火车站只出售国内段车票，且须在指定窗口凭护照或边境通行证或当地身份证购买（一般为值班主任窗口，如北京站1号窗口）。乘客可在网上查询时刻表\[9\]及订票\[10\]。K3次列车国内段、K23次列车国内段、K27/28次列车国内段、Z5/6次列车国内段、T8701/8702次列车国内段、4652/4653/4654/4651次列车国内段、K7023/7024次列车国内段可在12306网站以及各车站普通售票窗口购买。K19/20次列车可在各车站普通售票窗口、铁路授权代售处及自动售票机购买。K4、K24次列车可在二连、朱日和站购买手写代用票。

<table>
<thead>
<tr class="header">
<th><p><strong>路线</strong></p></th>
<th><p><strong>车次</strong></p></th>
<th><p><strong>乘务担当/车辆配属</strong></p></th>
<th><p><strong>开行情况</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>北京—乌兰巴托—莫斯科</p></td>
<td><p><a href="../Page/K3/4次列车.md" title="wikilink">K3/K4</a></p></td>
<td><p>中国铁路总公司北京铁路局</p></td>
<td><p>K3：每星期三由北京站发车，每星期四停靠乌兰巴托站，每星期一到达莫斯科<br />
K4：每星期二由莫斯科发车，每星期日停靠乌兰巴托站，每星期一到达北京站</p></td>
</tr>
<tr class="even">
<td><p>北京—满洲里—莫斯科</p></td>
<td><p><a href="../Page/K19/20次列车.md" title="wikilink">K19/K20</a></p></td>
<td><p>俄罗斯联邦铁路客运股份有限公司莫斯科铁路局</p></td>
<td><p>K19：每星期六由北京站发车，每星期四到达莫斯科<br />
K20：每星期六由莫斯科发车，下星期六到达北京站。因K19/20次与K39/40次部分区段共线，K39次每周五、六停驶；K40次每周四、五停驶，客运高峰期间每周五单向增加K40次。自2016年1月10日起，K39/40次列车改为T39/40次列车，不再与K19/20次列车共线运行。</p></td>
</tr>
<tr class="odd">
<td><p>满洲里—赤塔</p></td>
<td><p><a href="../Page/649/650次列车.md" title="wikilink">649/650</a></p></td>
<td><p>俄罗斯联邦铁路客运股份有限公司后贝加尔铁路局</p></td>
<td><p>北京时间 每周五，周日 14时 自满洲里站发车，抵达赤塔站时间为 分别为 莫斯科时间 每周六和周一 2点53分。运行时间 17:53<br />
莫斯科时间 每周四，周六14:20 自赤塔站发车，抵达满洲里站时间为北京时间 每周五，周日11时。运行时间 15:40</p></td>
</tr>
<tr class="even">
<td><p>绥芬河—格罗迭科沃</p></td>
<td><p><a href="../Page/401/402次列车.md" title="wikilink">401/402</a></p></td>
<td><p>中国铁路总公司哈尔滨铁路局</p></td>
<td><p>北京时间 每日 10:00 自绥芬河站发车，抵达格罗迭科沃站时间为 莫斯科时间 每日 06:30 。 运行时间 1:30<br />
莫斯科时间每日 10:43 自格罗迭科沃站发车，抵达绥芬河站时间为 北京时间 每日 17:14。运行时间1:31。</p></td>
</tr>
<tr class="odd">
<td><p>北京—乌兰巴托</p></td>
<td><p><a href="../Page/K23/24次列车.md" title="wikilink">K23/K24</a></p></td>
<td><p>中国铁路总公司北京铁路局<br />
蒙古国乌兰巴托铁路局</p></td>
<td><p>K23：每星期二由北京站发车，每星期三到达乌兰巴托<br />
K24：每星期四由乌兰巴托发车，每星期五到达北京站<br />
附：K23/K24次与K3/K4次北京—乌兰巴托间共线运行，使用相同时刻<br />
中、蒙共同担当时，中国担当的K23次不变，K24次改为每周五乌兰巴托发，周六到北京<br />
蒙铁担当的K24次占用原中铁担当时刻，K23次每周六北京发，周日到乌兰巴托</p></td>
</tr>
<tr class="even">
<td><p>二连—乌兰巴托</p></td>
<td><p><a href="../Page/681/682/683/684/685/686次列车.md" title="wikilink">681/682/683/684/685/686</a></p></td>
<td><p>蒙古国乌兰巴托铁路局</p></td>
<td><p>每日开行</p></td>
</tr>
<tr class="odd">
<td><p>二连—扎门乌德</p></td>
<td><p><a href="../Page/33/34次列车.md" title="wikilink">33/34</a></p></td>
<td><p>蒙古国乌兰巴托铁路局</p></td>
<td><p>每日开行</p></td>
</tr>
<tr class="even">
<td><p>北京—平壤</p></td>
<td><p><a href="../Page/K27/28次列车.md" title="wikilink">K27/K28</a></p></td>
<td><p>中国铁路总公司北京铁路局<br />
<a href="../Page/朝鲜.md" title="wikilink">朝鲜铁道省平壤铁路局</a></p></td>
<td><p>K27：每星期一、三、四、六由北京站发车，每星期二、四、五、日到达平壤<br />
K28：每星期一、三、四、六由平壤发车，每星期二、四、五、日到达北京</p></td>
</tr>
<tr class="odd">
<td><p>丹东—平壤</p></td>
<td><p><a href="../Page/95次列车.md" title="wikilink">95/96</a></p></td>
<td><p>中国铁路总公司沈阳铁路局<br />
<a href="../Page/朝鲜.md" title="wikilink">朝鲜铁道省平壤铁路局</a></p></td>
<td><p>每日开行。</p></td>
</tr>
<tr class="even">
<td><p>北京西—河内（嘉林）</p></td>
<td><p><a href="../Page/Z5/6次列车.md" title="wikilink">Z5/Z6</a></p></td>
<td><p>中国铁路总公司南宁铁路局</p></td>
<td><p>Z5：每星期四、日由北京西站发车，每星期二、六到达嘉林站<br />
Z6：每星期二、五由嘉林站发车，每星期四、日到达北京西站<br />
国际联运旅客一律分配到Z5/6次列车的9号车厢，并在南宁与T8701/2次列车换乘</p></td>
</tr>
<tr class="odd">
<td><p>乌鲁木齐—阿拉木图</p></td>
<td><p><a href="../Page/K9795/9796次列车.md" title="wikilink">K9795/K9796</a></p></td>
<td><p>中国铁路总公司乌鲁木齐铁路局<br />
<a href="../Page/哈萨克斯坦.md" title="wikilink">哈萨克斯坦铁路客运总公司阿拉木图铁路局</a></p></td>
<td><p>K9795：每星期一、六由乌鲁木齐发车，每星期三、一到达阿拉木图<br />
K9796：每星期一、六由阿拉木图发车，每星期三、一到达乌鲁木齐站</p></td>
</tr>
<tr class="even">
<td><p>乌鲁木齐—努尔-苏丹</p></td>
<td><p><a href="../Page/K9797/9798次列车.md" title="wikilink">K9797/K9798</a></p></td>
<td><p><a href="../Page/哈萨克斯坦.md" title="wikilink">哈萨克斯坦铁路客运总公司阿克斗卡铁路局</a></p></td>
<td><p>K9797：每星期四由乌鲁木齐站发车，每星期日到达努尔-苏丹<br />
K9798：每星期二由努尔-苏丹发车，每星期四到达乌鲁木齐站</p></td>
</tr>
<tr class="odd">
<td><p>南宁—河内（嘉林）</p></td>
<td><p><a href="../Page/T8701/8702次列车.md" title="wikilink">T8701/T8702</a></p></td>
<td><p>中国铁路总公司南宁铁路局</p></td>
<td><p>每日开行</p></td>
</tr>
<tr class="even">
<td><p>呼和浩特—乌兰巴托</p></td>
<td><p><a href="../Page/4652/4653、4654/4651列车.md" title="wikilink">4652/4653/4654/4651</a></p></td>
<td><p>中国铁路总公司呼和浩特铁路局<br />
蒙古国乌兰巴托铁路局</p></td>
<td><p>每星期一、五由呼和浩特站发车，每星期三、日到达乌兰巴托站<br />
每星期一、五由乌兰巴托站发车，每星期二、六到达呼和浩特站</p></td>
</tr>
<tr class="odd">
<td><p>哈尔滨—海参崴—伯力</p></td>
<td><p><a href="../Page/K7023/7024次列车.md" title="wikilink">K7023/K7024</a></p></td>
<td><p>中国铁路总公司哈尔滨铁路局<br />
俄罗斯联邦铁路客运股份有限公司远东铁路局</p></td>
<td><p>该国际加挂车厢（2节）自2013年8月16日起停运。<br />
原运行时间：<br />
K7023：每星期三、六由哈尔滨站发车，每星期五、一到达海参崴、伯力<br />
　K7024：每星期一、四由伯力、海参崴发车，每星期三、六到达哈尔滨站。</p></td>
</tr>
</tbody>
</table>

## 客运车站

## 客運車輛

中国大陆目前铁路客运车辆主要有两种方式：[动车组和普通](../Page/动车组.md "wikilink")[铁路客车](../Page/铁路客车.md "wikilink")，至今机辆模式（[机车牵引客车](../Page/铁路机车.md "wikilink")）的普通客车依然为中国铁路客运的主流。但中国铁路总公司表示，随着[中國高速鐵路网陆续建成](../Page/中國高速鐵路.md "wikilink")，预计到2020年，大陆地区的铁路客运方式将以[中國鐵路高速](../Page/中國鐵路高速.md "wikilink")（[和諧號及](../Page/和諧號.md "wikilink")[復興號](../Page/復興號電力動車組.md "wikilink")）动车组为主\[11\]。

## 注释

## 参考文献

## 外部链接

  - [中国铁路总公司](http://www.china-railway.com.cn/)

  - [国旅在线：国际列车预订系统](http://www.cits.com.cn/citsonlineWeb/switchdo.do?prefix=/intlTrain&page=/intlTrainSearchC.do&channelType=DXFW)，[中国国际旅行社总社](../Page/中国国际旅行社总社.md "wikilink")

  - [中國科普博覽鐵道館](http://www.kepu.com.cn/gb/technology/railway/)

  - [中国铁路客户服务中心](http://www.12306.cn/mormhweb/)

  - [中國鐵道俱樂部 國際列車](http://www.railway.org.cn/special/index.html)

## 參見

  - [鐵路運輸](../Page/鐵路運輸.md "wikilink")
  - [中华人民共和国交通](../Page/中华人民共和国交通.md "wikilink")
  - [中華人民共和國鐵路運輸](../Page/中華人民共和國鐵路運輸.md "wikilink")
  - [中国高速铁路](../Page/中国高速铁路.md "wikilink")
  - [中国铁路大提速](../Page/中国铁路大提速.md "wikilink")
  - [和谐号](../Page/和谐号.md "wikilink")
  - 《[关口知宏之中国铁道大纪行](../Page/關口知宏之中國鐵道大紀行.md "wikilink")》

{{-}}

[Category:中华人民共和国铁路](../Category/中华人民共和国铁路.md "wikilink")

1.  [2008年全国铁路主要指标完成情况](http://www.china-mor.gov.cn/zwgk/zwgk_tjxx_2008_1_12.html)
    ．中华人民共和国铁道部
2.
3.
4.
5.
6.
7.
8.  [Vietnam
    Railways](http://www.vr.com.vn/english/hientaihoatdong.html)
9.  [国际列车时刻表查询](http://www.cits.com.cn/cits/statichelp/T_jieshao.html)
10. [部分国际列车订票](http://ticket.cits.com.cn/)
11. [铁路客运2020年动车组化](http://www.jfdaily.com/isd/sh_62408/dafd/200906/t20090625_685235.htm)