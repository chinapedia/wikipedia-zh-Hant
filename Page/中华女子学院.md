[China_Women's_University_2014-03-30_100540.jpg](https://zh.wikipedia.org/wiki/File:China_Women's_University_2014-03-30_100540.jpg "fig:China_Women's_University_2014-03-30_100540.jpg")
**中华女子学院**是位于[中国](../Page/中国.md "wikilink")[北京市的一所女子大学](../Page/北京市.md "wikilink")，隶属于[中华全国妇女联合会](../Page/中华全国妇女联合会.md "wikilink")（妇联）。

前身是1949年由[宋庆龄](../Page/宋庆龄.md "wikilink")、[何香凝](../Page/何香凝.md "wikilink")、[蔡畅等人创办的新中国女子职业学校](../Page/蔡畅.md "wikilink")，后来作为妇联的干部学校。1995年更名为中华女子学院，并于1996年正式成为[大学](../Page/大学.md "wikilink")，向外招收普通[高等教育本科学生](../Page/高等教育.md "wikilink")。

中华女子学院校址位于北京市[朝阳区](../Page/朝阳区_\(北京市\).md "wikilink")[亚运村姜庄湖](../Page/亚运村.md "wikilink")，校园面积约160亩。另于北京市[昌平区设有北校区](../Page/昌平区.md "wikilink")。教授、副教授约60人，在校[本科生约](../Page/本科生.md "wikilink")3300人。

中华女子学院原在[山东省](../Page/山东省.md "wikilink")[济南市设有](../Page/济南市.md "wikilink")[中华女子学院山东分院](../Page/中华女子学院山东分院.md "wikilink")，该分院后于2010年独立设校，更名为[山东女子学院](../Page/山东女子学院.md "wikilink")。

## 下属院系

  - 艺术系
  - 外语系
  - 学前教育系
  - 法律系
  - 人力资源管理系
  - 经济管理系
  - 汉语国际教育系
  - 社会学系
  - 社会工作系
  - 计算机系
  - 女性学系
  - 金融系
  - 公共教学部，负责公共基础课教学
  - 中华高丽学院，与[韩国](../Page/韩国.md "wikilink")[高丽集团合办](../Page/高丽集团.md "wikilink")，主要进行外语、文秘等职业高等教育
  - 继续教育学院

## 参考文献

## 外部链接

  - [学校主页](http://www.cwu.edu.cn/)

## 参见

  - [中国女子大学](../Page/中国女子大学.md "wikilink")
  - [中华女子学院山东分院](../Page/中华女子学院山东分院.md "wikilink")

{{-}}

[Category:中华人民共和国女子高等学校](../Category/中华人民共和国女子高等学校.md "wikilink")
[Category:北京高等院校](../Category/北京高等院校.md "wikilink")
[Category:1949年創建的教育機構](../Category/1949年創建的教育機構.md "wikilink")