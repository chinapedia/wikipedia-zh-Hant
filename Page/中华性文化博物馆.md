[China_Sex_Museum_map.png](https://zh.wikipedia.org/wiki/File:China_Sex_Museum_map.png "fig:China_Sex_Museum_map.png")

**中华性文化博物馆**（）现位于[江苏省水乡古镇](../Page/江苏省.md "wikilink")[同里](../Page/同里.md "wikilink")，是一座私人[博物馆](../Page/博物馆.md "wikilink")，也是中国第一所[性博物馆](../Page/性博物馆.md "wikilink")。该馆位于古典[园林](../Page/园林.md "wikilink")[退思园东侧](../Page/退思园.md "wikilink")，馆址曾是中国近代最早的女子学校之一：拥有近百年历史的[丽则女校](../Page/丽则女校.md "wikilink")。馆长是[上海大学社会系教授](../Page/上海大学.md "wikilink")[刘达临](../Page/刘达临.md "wikilink")。馆内展品为刘达临等十余年从国内外各地收集而来。

馆内展示了中国数千年来的[古代性文化实物](../Page/中国性文化.md "wikilink")、[雕塑](../Page/雕塑.md "wikilink")、图画、[书籍等](../Page/书籍.md "wikilink")2000余件，其中最古老的展品距今8000多年，距今5000余年的珍稀展品则有20多件。展览内容分为：原始社会中的性、婚姻和妇女、日常生活中的性、特殊领域中的性。并设有远古文化和性崇拜、婚姻家庭和妇女压迫、宗教和外国两性文化等展区。

该馆创建于1995年。当时刘达临举债30万元将展览馆落户[上海](../Page/上海.md "wikilink")[青浦县徐泾镇](../Page/青浦县.md "wikilink")，但由于过于偏远，参观者寥寥。直到1999年，该馆搬迁至上海[南京路步行街附近](../Page/南京路.md "wikilink")。这次虽然离繁华闹市较近，但位于小巷内的该馆仍很难吸引参观者。刘达临试图在步行街竖立指示牌，但未得到主管部门的批准。2001年展览馆被迫搬迁至租金较低的上海武定路，但经济问题仍难以解决。该馆在2003年确定迁址江苏同里，并于2004年4月18日正式对外开放。

社会学家[费孝通题写了该馆馆名](../Page/费孝通.md "wikilink")，并称其为“五千年来第一展”。

## 中国古代性文化展览

中华性文化博物馆部分古代性文化[雕塑](../Page/雕塑.md "wikilink")、[春宫图](../Page/春宫图.md "wikilink")、[书籍等展品曾在中国和世界各地展览](../Page/书籍.md "wikilink")。

  - 1993年10月[上海中国古代性文化展览会](../Page/上海.md "wikilink")。
  - 1994年3月[沈阳中国古代性文化展览会](../Page/沈阳.md "wikilink")。
  - 1995年3月[台湾](../Page/台湾.md "wikilink")[台北市中国古代性文化展览会](../Page/台北市.md "wikilink")
  - 1995年6月[德国](../Page/德国.md "wikilink")[柏林中国古代性文化展览会](../Page/柏林.md "wikilink")
  - 1996年2月[澳大利亚](../Page/澳大利亚.md "wikilink")[墨尔本中国古代性文化展览会](../Page/墨尔本.md "wikilink")。
  - 1999年8月[香港中国古代性文化展览会](../Page/香港.md "wikilink")。
  - 1999年9月[合肥中国古代性文化展览会](../Page/合肥.md "wikilink")。
  - 1999年9月[荷兰](../Page/荷兰.md "wikilink")[鹿特丹中国古代性文化展览会](../Page/鹿特丹.md "wikilink")。
  - 2000年2月[香港中国古代性文化展览会](../Page/香港.md "wikilink")。
  - 2004年11月[湖南](../Page/湖南.md "wikilink")[长沙](../Page/长沙.md "wikilink")
  - 2005年11月[广州](../Page/广州.md "wikilink")。

## 中国情色文化史

中华性文化博物馆部分古代性文化[雕塑](../Page/雕塑.md "wikilink")、[春宫画](../Page/春宫画.md "wikilink")、[书籍等展品的图片](../Page/书籍.md "wikilink")，一部分收入刘达临著《中国性史图鉴》、《中国情色文化史》

## 参考

  - 刘达临 著 《我的性学之路》 中国青年出版社 2001 ISBN 7-104-01415-2
  - 刘达临 编著 《中国性史图鉴》600+ 插图 长春 时代文艺出版社 2003 ISBN 7-5387-1355-7
  - 刘达临 著《中国情色文化史》（上下册）人民日报出版社 2004 ISBN 7-80153-776-9

## 外部連結

  - [中华性文化博物馆](https://web.archive.org/web/20160305205706/http://www.chinasexmuseum.com/)
  - [中华性文化展览](https://web.archive.org/web/20050818124300/http://www2.hu-berlin.de/sexology/CSM/index.htm)

## 参见

  - [中国性文化](../Page/中国性文化.md "wikilink")
      - [中华人民共和国](../Page/中华人民共和国.md "wikilink")

[Category:中华人民共和国私人博物馆](../Category/中华人民共和国私人博物馆.md "wikilink")
[Category:苏州博物馆](../Category/苏州博物馆.md "wikilink")
[Category:中华人民共和国性文化](../Category/中华人民共和国性文化.md "wikilink")
[Category:性博物馆](../Category/性博物馆.md "wikilink")
[Category:吴江区](../Category/吴江区.md "wikilink")