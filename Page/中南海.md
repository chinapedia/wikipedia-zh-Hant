[中南海02.jpg](https://zh.wikipedia.org/wiki/File:中南海02.jpg "fig:中南海02.jpg")
[Beijing_Zhongnanhai_map.jpg](https://zh.wikipedia.org/wiki/File:Beijing_Zhongnanhai_map.jpg "fig:Beijing_Zhongnanhai_map.jpg")
[Zhongnanhai_-_satellite_image_(1967-09-20).jpg](https://zh.wikipedia.org/wiki/File:Zhongnanhai_-_satellite_image_\(1967-09-20\).jpg "fig:Zhongnanhai_-_satellite_image_(1967-09-20).jpg")
**中南海**位于[中国](../Page/中国.md "wikilink")[北京市](../Page/北京市.md "wikilink")[西城区](../Page/西城区.md "wikilink")[西长安街街道](../Page/西长安街街道.md "wikilink")，泛指[紫禁城西方鄰近北海](../Page/紫禁城.md "wikilink")、中海及南海的區域，佔地100公顷，其中水面约50公顷，是[中华人民共和国国务院](../Page/中华人民共和国国务院.md "wikilink")、[中共中央书记处和](../Page/中共中央书记处.md "wikilink")[中共中央办公厅等重要](../Page/中共中央办公厅.md "wikilink")[机关辦公所在地](../Page/机关.md "wikilink")。[中华人民共和国成立後](../Page/中华人民共和国.md "wikilink")，歷代[党和国家领导人都住在这里](../Page/党和国家领导人.md "wikilink")，因此中南海[象徵着](../Page/转喻.md "wikilink")[中国共产党和](../Page/中国共产党.md "wikilink")[中华人民共和国政府的最高權力](../Page/中华人民共和国政府.md "wikilink")。

## 历史

狭义的中南海仅指[北京](../Page/北京.md "wikilink")[故宫西侧的两个连通的湖](../Page/故宫.md "wikilink")「中海」和「南海」。历史上北海、中海及南海统称[太液池](../Page/太液池.md "wikilink")。

### 早期形成

[金朝时期](../Page/金朝.md "wikilink")，在今日中南海的北半部有[太液池和](../Page/太液池.md "wikilink")[大宁宫](../Page/大宁宫.md "wikilink")，[太液秋波为形成于金朝的](../Page/太液秋波.md "wikilink")[燕京八景之一](../Page/燕京八景.md "wikilink")。[元朝修筑大都时](../Page/元朝.md "wikilink")，将太液池包入皇城之中，在其周围布置了三组宫殿，即大内、隆福宫和兴圣宫。元朝的太液池范围相当于今日的北海和中海。

### 明代

[皇城2.jpg](https://zh.wikipedia.org/wiki/File:皇城2.jpg "fig:皇城2.jpg")位於皇城西邊，即是今日中南海與[北海公園的位置所在](../Page/北海公園.md "wikilink")\]\]
中南海和北海的建筑群最终定型始于明代。[明朝](../Page/明朝.md "wikilink")[明成祖定都北京后](../Page/明成祖.md "wikilink")，从1406年起营建新的皇宫，明朝宫城在元朝宫殿的位置基础上向南移动，因此[皇城城墙也随之南移](../Page/北京皇城.md "wikilink")，为丰富皇城园林景观，开挖了南海，挖出的土方和开凿[筒子河的土方堆成](../Page/筒子河.md "wikilink")[万岁山](../Page/万岁山.md "wikilink")（即[景山](../Page/景山.md "wikilink")）。北海、中海、南海统称“太液池”，属于皇城西苑。北海与中海以[金鳌玉蝀桥为界](../Page/北海公园_\(北京\)#金鰲玉蝀橋.md "wikilink")，中海与南海以[蜈蚣桥为界](../Page/蜈蚣桥.md "wikilink")。

### 清代

[清朝定都北京后](../Page/清朝.md "wikilink")，撤消皇城内诸内廷供奉衙署，将[西苑大片土地改为民居](../Page/西苑.md "wikilink")，同时在北海、中海、南海三海周围设置“内红墙”，[御苑范围退缩至内红墙之内](../Page/御苑.md "wikilink")。相对政治象征[紫禁城](../Page/紫禁城.md "wikilink")，清帝更喜园居，[顺治](../Page/顺治.md "wikilink")、[康熙](../Page/康熙.md "wikilink")、[乾隆诸帝均在中南海内兴建殿宇馆轩](../Page/乾隆.md "wikilink")，作为避暑听政之所。[同治](../Page/同治.md "wikilink")、[光绪年间](../Page/光绪.md "wikilink")，[慈禧太后及皇帝按礼制在十二月从](../Page/慈禧太后.md "wikilink")[颐和园移居](../Page/颐和园.md "wikilink")[紫禁城时](../Page/紫禁城.md "wikilink")，也多在中南海内居住，仅行礼时前往紫禁城。[戊戌变法失败后](../Page/戊戌变法.md "wikilink")，慈禧太后曾将光绪帝囚禁于南海中的[瀛台](../Page/瀛台.md "wikilink")。

1900年[义和团运动时期](../Page/义和团.md "wikilink")，中南海成为[俄军驻地](../Page/俄羅斯帝国.md "wikilink")，苑内文物陈设被掠一空，[八国联军总司令](../Page/八国联军.md "wikilink")[瓦德西占领北京后](../Page/瓦德西.md "wikilink")，居住于中南海[仪鸾殿](../Page/仪鸾殿.md "wikilink")。[溥仪即位后](../Page/溥仪.md "wikilink")，曾在中海西岸集灵囿修建摄政王府。

### 中華民国

1911年[清朝](../Page/清朝.md "wikilink")[宣統帝](../Page/宣統帝.md "wikilink")[退位后](../Page/退位.md "wikilink")，根据《[清室優待條件](../Page/清室優待條件.md "wikilink")》，皇室虽得以保留[紫禁城和](../Page/紫禁城.md "wikilink")[颐和园](../Page/颐和园.md "wikilink")，但北、中、南三海即刻归民国所有。中南海随即被[中华民国](../Page/中华民国.md "wikilink")[大总统](../Page/中华民国大总统.md "wikilink")[袁世凯佔用](../Page/袁世凯.md "wikilink")，成为[北洋政府的](../Page/北洋政府.md "wikilink")[大總统府](../Page/大總统府.md "wikilink")。[袁世凯称帝时期](../Page/袁世凯称帝.md "wikilink")，中南海曾改名[新华宫](../Page/新华宫.md "wikilink")，同时[宝月楼拆外墙](../Page/宝月楼.md "wikilink")、更名为[新华门](../Page/新华门.md "wikilink")。从此，新华门取代西苑门成為中南海的正门。此后中南海又先后被用做北洋政府的[大总统和](../Page/中华民国大总统.md "wikilink")[总理办公地](../Page/中华民国国务总理.md "wikilink")、以及[张作霖的大元帅府](../Page/张作霖.md "wikilink")。

[中华民国国民政府接管北平後](../Page/中华民国国民政府.md "wikilink")，中南海作为公园对外开放。[中国抗日战争结束後](../Page/中国抗日战争.md "wikilink")，[國民政府軍事委員會北平行营设在中南海](../Page/國民政府軍事委員會.md "wikilink")，华北剿匪总司令[傅作义搬進中南海办公](../Page/傅作义.md "wikilink")。1949年，[共军正式进驻北平](../Page/北平和平解放.md "wikilink")，[中共中央進駐中南海](../Page/中共中央.md "wikilink")。

### 中华人民共和国

1949年[中华人民共和国成立以后中南海成为](../Page/中华人民共和国.md "wikilink")[中华人民共和国政府驻地](../Page/中华人民共和国政府.md "wikilink")。

[文革时期的](../Page/文革.md "wikilink")1967年1月，[红卫兵曾包围并试图暂时控制中南海](../Page/红卫兵.md "wikilink")。\[1\]

[八九民运时期](../Page/八九民运.md "wikilink")，有数千名学生在中南海新华门门口进行示威，要求共产党的领导人与学生之间展开对话。但最终均被警方驱离。

1999年4月25日，一万多名[法轮功学员到中南海附近的](../Page/法轮功.md "wikilink")[国家信访局进行上访](../Page/国家信访局.md "wikilink")，要求政府结束不合理的对待，当时的国务院总理[朱镕基以及其他高级官员与上访者代表达成协议](../Page/朱镕基.md "wikilink")，随后上访者解散，此事件被称作“[中南海事件](../Page/中南海事件.md "wikilink")”。

中南海也是[毛泽东](../Page/毛泽东.md "wikilink")，[周恩来](../Page/周恩来.md "wikilink")，[刘少奇](../Page/刘少奇.md "wikilink")，[邓小平](../Page/邓小平.md "wikilink")，[陳雲](../Page/陳雲.md "wikilink")、[陈伯达](../Page/陈伯达.md "wikilink")，[胡喬木](../Page/胡喬木.md "wikilink")、[陆定一](../Page/陆定一.md "wikilink")，[江青](../Page/江青.md "wikilink")，[朱德](../Page/朱德.md "wikilink")、[彭德怀](../Page/彭德怀.md "wikilink")、[陈毅](../Page/陈毅.md "wikilink")、[李先念](../Page/李先念.md "wikilink")、[楊尚昆](../Page/楊尚昆.md "wikilink")、[胡耀邦](../Page/胡耀邦.md "wikilink")、[习仲勋](../Page/习仲勋.md "wikilink")、[江澤民](../Page/江澤民.md "wikilink")、[胡錦濤](../Page/胡錦濤.md "wikilink")、[習近平等](../Page/習近平.md "wikilink")[中華人民共和國](../Page/中華人民共和國.md "wikilink")[黨和國家領導人的居所](../Page/黨和國家領導人.md "wikilink")。\[2\]

## 建筑特色

### 中海

  - [紫光阁](../Page/紫光阁.md "wikilink")：位于中海西岸北部，阁高两层，面阔七间，单檐[庑殿顶](../Page/庑殿顶.md "wikilink")，黄剪边绿[琉璃瓦](../Page/琉璃瓦.md "wikilink")，前有五间卷棚[歇山顶](../Page/歇山顶.md "wikilink")[抱厦](../Page/抱厦.md "wikilink")。[明武宗时为平台](../Page/明武宗.md "wikilink")，台上有黄瓦顶小殿。[明世宗时废台](../Page/明世宗.md "wikilink")，修建紫光阁，清康熙时重修，成为皇帝检阅侍卫比武的地方。乾隆二十五年（1760年）和四十年（1775年）两次增建，悬挂功臣图像及各次战役挂图，并陈列缴获的武器。1949年后改建为国事活动场所。阁后有武成殿。面阔五间，单檐卷棚歇山顶。
  - [万善殿](../Page/万善殿.md "wikilink")：位于中海东岸，明代为椒园。原名崇智殿，清顺治时改为现名，殿内供奉三世佛像。万善殿后有千圣殿，上为圆顶，殿内供奉七层千佛塔。
  - [水云榭](../Page/水云榭.md "wikilink")：位于中海东岸水中，为一敞轩，[燕京八景中的](../Page/燕京八景.md "wikilink")“太液秋风”即指此处。
  - [摄政王府](../Page/醇親王府南府.md "wikilink")：位于中南海西北角，北西两面临墙。原位于西苑墙外，光绪十一年扩建西苑时将此地圈入中南海，并将位于原址的蚕池口天主教堂迁往[西什库](../Page/西什库天主堂.md "wikilink")。慈禧太后拟在此修建新园林“集灵囿”，但一直未开工。1909年将此地拨给摄政王[载沣](../Page/载沣.md "wikilink")，修建摄政王府。摄政王府规制同旧[醇王府](../Page/醇親王府.md "wikilink")（北府）相似，包括中路、东路、西一路、西二路、西花园，工程耗银206万两。1911年清朝覆灭时王府仍未竣工，后改为国务院办公地，1918年后先后成为[徐世昌的](../Page/徐世昌.md "wikilink")[总统府](../Page/总统府.md "wikilink")、[陆军部和](../Page/陆军部.md "wikilink")[海军部](../Page/海军部.md "wikilink")、[北平市政府](../Page/北平市政府.md "wikilink")。1949年后改为国务院办公区，1970年代末大规模翻修中南海建筑时曾计划将摄政王府落架大修，但发现建筑质量非常低劣，地基松散，木柱间裂缝用碎砖填充，已无法保留，只得拆除。现存正门和正殿，为会议室。[周恩来曾居住于西花园内的](../Page/周恩来.md "wikilink")**西花厅**，也加以保存。
  - [國務院小禮堂](../Page/國務院小禮堂.md "wikilink")：位於紫光閣的西邊，故又稱為西樓大廳。1949年後，大廳內的一個房間被改建為電影院，每週舉行幾次放映。該建築還包括一個供國務院工作人員使用的小食堂。1979年，電影院被取消，取而代之的是國務院小禮堂。[周恩來總理為抵制財政緊縮政策](../Page/周恩來.md "wikilink")，拒絕進行翻新工作。該建築現在是國務院會議的主要場所。國務院全體會議和國務院常務委員會每週會議均在第一會議室舉行。國務院包括其主要會議室，共有六間會議室，用於各種用途。國務院的北區作為國務院辦公廳使用。

### 南海

  - [新华门](../Page/新华门.md "wikilink")：中南海正门，位於西[长安街上](../Page/长安街.md "wikilink")，为两层楼房，面阔七间，下层中央三间为门洞。卷棚歇山顶，绿剪边黄琉璃瓦。新华门原为[乾隆时期建造的宝月楼](../Page/乾隆.md "wikilink")，[辛亥革命后](../Page/辛亥革命.md "wikilink")，[袁世凯改楼为门](../Page/袁世凯.md "wikilink")，并以“新[中华](../Page/中华民国.md "wikilink")”之意取名为“新华门”。同时在门内修建[影壁](../Page/影壁.md "wikilink")，拆除门外[清真寺](../Page/清真寺.md "wikilink")，在长安街对面修筑花墙挡住破烂民居，并将[义和团运动时被焚毁的](../Page/义和团运动.md "wikilink")[端王府一对石狮移于门前](../Page/端王府.md "wikilink")。现在的新華門兩旁八字影壁墙上有“伟大的[中国共产党万岁](../Page/中国共产党万岁.md "wikilink")”、“战无不胜的[毛泽东思想万岁](../Page/毛泽东思想.md "wikilink")”的标语。门内影壁题字为[毛澤東手书](../Page/毛澤東.md "wikilink")“[為人民服務](../Page/為人民服務.md "wikilink")”。

<!-- end list -->

  - [居仁堂](../Page/居仁堂.md "wikilink")：這座建築原本是一座兩層樓的西式宮殿，在清朝時被稱為海雁塘。[慈禧太后建造的這座建築是為了招待女性客人並接待外國外交官](../Page/慈禧太后.md "wikilink")。在鎮壓[義和團運動後](../Page/義和團運動.md "wikilink")，[八國聯軍指揮官](../Page/八國聯軍.md "wikilink")[瓦德西在伊鑾寺被火燒毀後搬到了這裡](../Page/瓦德西.md "wikilink")。中華民國成立後，袁世凱將這座建築改名為居仁堂，繼續用它來接待遊客。1949年後，該建築物成為中央軍委的第一個總部，然後中央軍委工作人員搬遷到中南海以外的[八一大樓](../Page/八一大樓.md "wikilink")。1956年，中共中央書記處成為一個獨立於[中央委員會主席的機構](../Page/中國共產黨中央委員會主席.md "wikilink")，需要自己的總部。新任書記處負責人[鄧小平選擇了居仁堂為秘書處地址](../Page/鄧小平.md "wikilink")。原建築於1964年拆除，取而代之的是較小的中式涼亭。秘書處辦事處暫時遷至西樓大院的“C棟”，然後於1980年搬遷至勤政殿。

<!-- end list -->

  - [西樓大院](../Page/西樓大院.md "wikilink")：這座建築群以其位於中南海西南角的位置而得名。這些建築物建於20世紀50年代初，為中央辦公廳的工作人員提供辦公室和寓所。除了西樓外，其他建築物被指定為A，B，C，D和F棟，分配給支持總務辦公室的許多秘書或工作人員在這裡工作。早在20世紀90年代，西樓大院就為中心委員會辦公室的年輕工人提供了宿舍。西樓因此成為中央辦公廳主任的工作場所。西樓包括一個大廚房，一個供中南海員工使用的自助餐廳和一個較小的用餐區，可作為會議室供高層領導使用。[中國國家主席的辦公室](../Page/中華人民共和國主席.md "wikilink")，工作人員也位於西樓期間，如[劉少奇從](../Page/劉少奇.md "wikilink")1959年至1967年擔任國家主席的任期內，直至他在[文化大革命中被打倒](../Page/文化大革命.md "wikilink")。同樣，[朱德元帥的辦公室位於西樓](../Page/朱德.md "wikilink")，當時他先後擔任[中國國家副主席和](../Page/中華人民共和國副主席.md "wikilink")[全國人大常委會委員長](../Page/全國人民代表大會常務委員會委員長.md "wikilink")。

<!-- end list -->

  - [丰泽园](../Page/丰泽园.md "wikilink")：在瀛台之北，康熙年间建造，曾为养[蚕之处](../Page/蚕.md "wikilink")。雍正年间皇帝在举行[亲耕礼之前在此演礼](../Page/亲耕礼.md "wikilink")。丰泽园内主体建筑为惇叙殿，光绪年间改名为颐年殿，民国时改名颐年堂，袁世凯曾在此办公。1949年后改为会议场所。颐年堂东为**菊香书屋**，为[毛泽东官邸居住地](../Page/毛泽东.md "wikilink")（[文化大革命後遷居](../Page/文化大革命.md "wikilink")**游泳池**及**202別墅**至辭世）。丰泽园西有荷风蕙露亭、崇雅殿、静憩轩、怀远斋和纯一斋，荷风蕙露亭北为静谷，为一座幽静的小园林。静谷再北为春耦斋，北洋政府时为总统办公处，1949年后改为会议及娱乐场所。

<!-- end list -->

  - [勤政殿](../Page/勤政殿_\(中南海\).md "wikilink")：位于中海与南海之间的堤岸上，正门德昌门即南海的北门。原建筑在中华民国初年已被拆除，仅存地名。1970年代，[汪东兴拆毁了当时的勤政殿建筑](../Page/汪东兴.md "wikilink")，花费690万元人民币兴建了自己的私宅。[汪东兴下台后](../Page/汪东兴.md "wikilink")，该私宅成为[中共中央书记处的办公处](../Page/中共中央书记处.md "wikilink")。\[3\]

<!-- end list -->

  - [怀仁堂](../Page/怀仁堂.md "wikilink")：原址位于春耦斋北，清光绪时修建，取名仪鸾殿，为[慈禧太后居所](../Page/慈禧太后.md "wikilink")。[八国联军](../Page/八国联军.md "wikilink")[统帅](../Page/统帅.md "wikilink")[瓦德西曾在此居住](../Page/瓦德西.md "wikilink")，其间不慎失火，将殿烧毁。慈禧太后后另在中海西岸修建新仪鸾殿，后改名佛照楼，1908年，慈禧病逝于此。民国成立后，[袁世凯称帝前改名怀仁堂](../Page/袁世凯.md "wikilink")，并在此接见外宾、接受元旦朝贺。袁世凯死后，灵堂设于此处。其后[黎元洪](../Page/黎元洪.md "wikilink")、[徐世昌也沿用怀仁堂](../Page/徐世昌.md "wikilink")。[曹锟就任总统后](../Page/曹锟.md "wikilink")，将怀仁堂改为眷属居住场所。北洋政府结束后，怀仁堂长期闲置，成为当时北平市政府举办集体婚礼的场所。1949年中共在北京建权，[中国人民政治协商会议第一届全体会议在怀仁堂召开](../Page/中国人民政治协商会议第一届全体会议.md "wikilink")，1953年将怀仁堂改建为中式二层楼阁样式礼堂，作为中央政府的礼堂，举行各种政治会议和文艺晚会。1976年10月6日，国务院总理[华国锋](../Page/华国锋.md "wikilink")、中央军委副主席[叶剑英通过](../Page/叶剑英.md "wikilink")[中共中央办公厅通知](../Page/中共中央办公厅.md "wikilink")[中共中央政治局到中南海怀仁堂召开常委会](../Page/中共中央政治局.md "wikilink")，在[张春桥](../Page/张春桥.md "wikilink")、[王洪文](../Page/王洪文.md "wikilink")、[姚文元先後到达会议室时](../Page/姚文元.md "wikilink")，分別宣布对他们实施拘禁和隔离审查，同时对[江青在其位于中南海春藕斋西侧](../Page/江青.md "wikilink")201号楼的住处实施同样的拘捕措施，史称[粉碎四人帮或](../Page/粉碎四人帮.md "wikilink")[怀仁堂事变](../Page/怀仁堂事变.md "wikilink")。

<!-- end list -->

  - [西四所](../Page/西四所.md "wikilink")：這四座房舍為懷仁堂西翼的一部分。北洋政府結束後，這些建築物被北京歷史研究所收購。1949年以後，部分[中共領導人住在這裡](../Page/党和国家领导人.md "wikilink")，包括[鄧小平](../Page/鄧小平.md "wikilink")、[李富春](../Page/李富春.md "wikilink")、[陳毅和](../Page/陳毅.md "wikilink")[譚震林](../Page/譚震林.md "wikilink")。鄧小平任[最高領導人期間](../Page/中华人民共和国最高领导人.md "wikilink")，曾利用在西四所的官邸作為非正式會議的會議室，其中包括[中共中央顧問委員會](../Page/中共中央顧問委員會.md "wikilink")、[中共中央政治局和](../Page/中共中央政治局.md "wikilink")[中共中央書記處的成員與](../Page/中共中央書記處.md "wikilink")[八大元老](../Page/八大元老.md "wikilink")。

<!-- end list -->

  - [淑清院](../Page/淑清院.md "wikilink")：位于南海东北角，为乾隆时修建的小型园林，风格类似[北海公园静心斋](../Page/北海公园.md "wikilink")。园内有“流水音”亭、葆光室、蓬瀛在望殿、云绘楼、清音阁、[日知阁](../Page/日知阁.md "wikilink")、万字廊、双环万寿亭等建筑，1949年后将双环万寿亭移至[天坛公园](../Page/天坛.md "wikilink")，并拆除部分古建，修建卫戍部队营房及办公人员宿舍。

<!-- end list -->

  - [瀛台](../Page/瀛台.md "wikilink")：位於南海中的小島，公元1421年，明成祖[朱棣迁都](../Page/朱棣.md "wikilink")[北京](../Page/北京.md "wikilink")。明皇宫及「南台」
    (瀛台)建成，1644年九月，清廷自[盛京遷都](../Page/盛京.md "wikilink")[北京](../Page/北京.md "wikilink")，1655年6月清[顺治帝取](../Page/顺治帝.md "wikilink")「人间仙境」意，命名明南台为「瀛台」。1681年7月清[康熙帝](../Page/康熙帝.md "wikilink")「瀛台听政」，康熙皇帝在這裡研究制定平定內亂的國家方略\[4\]。因定[三藩赐](../Page/三藩.md "wikilink")「瀛台凯旋宴」。1726年夏少年[乾隆于瀛台](../Page/乾隆.md "wikilink")「补桐书屋」读书，著『瀛台记』，乾隆帝曾在这里写下一句话，意思是「得一日之清闲」，所以这里是可以令人感到放松的地方\[5\]。瀛台岛北有石桥
    (瀛台橋)与岸上相连，桥南为仁曜门，门南为翔鸾阁，正殿七间，左右延楼19间。再南为涵元门，内为瀛台主体建筑涵元殿。1898年8月[光緒帝](../Page/光緒帝.md "wikilink")[戊戌政变後即被](../Page/戊戌政变.md "wikilink")[慈禧太后幽禁於涵元殿](../Page/慈禧太后.md "wikilink")，隨後被毒死，[裕德齡的](../Page/裕德齡.md "wikilink")[瀛台泣血記中有詳實紀載](../Page/瀛台泣血記.md "wikilink")。由于岛上存在坡度，该殿北立面为单层建筑，南立面则为两层楼阁，称“蓬莱阁”。涵元殿北有配殿两座，东为庆云殿，西为景星殿；殿南两侧建筑，东为藻韵楼，西为绮思楼。藻韵楼之东有补桐书屋和随安室，乾隆时为书房，东北为待月轩和镜光亭。绮思楼向西为长春书屋和漱芳润，周围有长廊，名为“八音克谐”，及“怀抱爽”亭。1913年12月，瀛台設副總統辦公處，中華民國副總統[黎元洪居於瀛台島](../Page/黎元洪.md "wikilink")。瀛台现为[中國共產黨中央委員會總書記等](../Page/中國共產黨中央委員會總書記.md "wikilink")[最高層領導人辦公](../Page/中國共產黨中央政治局常務委員會.md "wikilink")、居住、举办重大宴会及招待活动的场所。

## 开放

1980年至1989年曾对公众开放中南海的部分景观，包括流水音、菊香书屋、颐年堂、静谷等\[6\]。

## 图集

<File:Xinhua>
Gate.jpg|中南海正门[新华门](../Page/新华门.md "wikilink")（今北京市西城区西长安街174号）
<File:Entrance> of the Imperial
Palace.jpg|1900年中南海[瀛台头](../Page/瀛台.md "wikilink")，包括[翔鸾阁以及阁前漫道](../Page/翔鸾阁.md "wikilink")
<File:Zhongnanhai03.jpg>|[北海与](../Page/北海公園_\(北京\).md "wikilink")[中海之间的](../Page/中海.md "wikilink")[金鳌玉蝀桥](../Page/金鳌玉蝀桥.md "wikilink")
<File:Zhongnanhai06.jpg>|[紫光阁](../Page/紫光阁.md "wikilink")（中海西岸北部）
<File:Zhongnanhai04.jpg>|[中海西岸的办公区](../Page/中海.md "wikilink")
<File:Zhongnanhai07.jpg>|[水云榭](../Page/水云榭.md "wikilink")（中海东岸水中）
<File:Zhongnanhai05.jpg>|[中南海西门](../Page/中南海西门.md "wikilink")（今北京市西城区府右街2号）内的道路
<File:Victory> banquet for the distinguished officers and soldiers at
the Ziguangge (Hall of Purple
Glaze).jpg|清[大小金川之役的慶功宴](../Page/大小金川之役.md "wikilink")，中南海紫光閣（1771年至1776年）
[File:Zhongnanhai.jpg|由北面看中南海](File:Zhongnanhai.jpg%7C由北面看中南海)

## 参考文献

## 外部链接

  - [*TIME*: Walled Heart of China's
    Kremlin](http://www.time.com/time/asia/magazine/99/0927/zhongnanhai.html)
  - [北京中南海](http://www.china.com.cn/chinese/zhuanti/gdyl/559995.htm) -
    [中国网](../Page/中国网.md "wikilink")

## 参见

  - [西山](../Page/西山.md "wikilink")、[玉泉山](../Page/玉泉山.md "wikilink")、[东交民巷](../Page/东交民巷.md "wikilink")、[北戴河](../Page/北戴河.md "wikilink")
  - [中共中央办公厅](../Page/中共中央办公厅.md "wikilink")、[中南海习近平办公室](../Page/中南海习近平办公室.md "wikilink")
  - [北海公园](../Page/北海公园.md "wikilink")
  - [西苑](../Page/西苑.md "wikilink")、[豹房](../Page/豹房.md "wikilink")

{{-}}

[Category:西长安街街道](../Category/西长安街街道.md "wikilink")
[中南海](../Category/中南海.md "wikilink")
[Category:燕京八景](../Category/燕京八景.md "wikilink")
[Category:北京市园林](../Category/北京市园林.md "wikilink")
[Category:北京市湖泊](../Category/北京市湖泊.md "wikilink")
[Category:北京宫殿](../Category/北京宫殿.md "wikilink")
[Category:中国皇家园林](../Category/中国皇家园林.md "wikilink")
[Category:元朝皇家建筑](../Category/元朝皇家建筑.md "wikilink")
[Category:明朝皇家建筑](../Category/明朝皇家建筑.md "wikilink")
[Category:清朝皇家建筑](../Category/清朝皇家建筑.md "wikilink")
[Category:中华民国中央政府建筑物
(北京)](../Category/中华民国中央政府建筑物_\(北京\).md "wikilink")
[Category:中华人民共和国中央政府建筑物
(北京)](../Category/中华人民共和国中央政府建筑物_\(北京\).md "wikilink")
[Category:中華人民共和國國家象徵](../Category/中華人民共和國國家象徵.md "wikilink")

1.  [“文革”中红卫兵冲击中南海](https://read01.com/naL6Ln.html)
2.  [陈伯达之子忆：50年代中南海不为人知的生活_资讯_凤凰网](http://news.ifeng.com/history/1/midang/200902/0225_2664_1031921.shtml)
3.  [胡绩伟，劫后承重任　因对主义诚，书屋2000年第4期](http://news.ifeng.com/history/zhongguoxiandaishi/detail_2012_09/17/17671321_0.shtml)
4.  [習近平奧巴馬瀛台談話部分內容首次曝光](http://news.wenweipo.com/2014/11/14/IN1411140098.htm)
5.  [【揭密】习近平奥巴马在瀛台到底聊了什么（习奥瀛台夜话全记录）](http://www.wtoutiao.com/a/724494.html)

6.  [中南海的往昔旧事](http://www.bjzqw.com/lanmu/bjzh/bjls/2012/0113/2932.html)