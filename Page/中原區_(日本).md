{{ 日本區

`| 自治體名=中原區`

|圖像=[Musashi-Kosugi001.jpg](https://zh.wikipedia.org/wiki/File:Musashi-Kosugi001.jpg "fig:Musashi-Kosugi001.jpg")
|圖像說明=從[東京都](../Page/東京都.md "wikilink")[大田區的](../Page/大田區.md "wikilink")[多摩川河畔遙望對岸](../Page/多摩川.md "wikilink")[武藏小杉地區的](../Page/武藏小杉站.md "wikilink")[摩天樓群](../Page/摩天樓.md "wikilink")（左起為小杉[塔樓](../Page/塔樓.md "wikilink")、幸福庭院大廈高層塔·東塔、[NEC玉川復興城市大廈南塔](../Page/NEC.md "wikilink")·北塔、公園城市武藏小杉中天塔樓、公園城市武藏小杉車站森林塔樓與武藏小杉塔樓）

`| 區章=`
`| 日文原名=中原区`
`| 平假名=なかはらく`
`| 羅馬字拼音=Nakahara-ku`
`| 都道府縣=神奈川縣`
`| 支廳=`
`| 市=川崎市`
`| 編號=14133-0`
`| 面積=14.81`
`| 邊界未定=`
`| 人口=222,922`
`| 統計時間=2008年5月1日`
`| 自治體=`[`川崎市`](../Page/川崎市.md "wikilink")`（`[`幸區`](../Page/幸區.md "wikilink")`、`[`高津區`](../Page/高津區.md "wikilink")`）、`[`橫濱市`](../Page/橫濱市.md "wikilink")`（`[`港北區`](../Page/港北區_\(日本\).md "wikilink")`）`
[`東京都`](../Page/東京都.md "wikilink")`：`[`大田区`](../Page/大田区.md "wikilink")`、`[`世田谷区`](../Page/世田谷区.md "wikilink")
`| 樹=`
`| 花=`
`| 其他象徵物=`
`| 其他象徵=`
`| 郵遞區號=211-8570`
`| 所在地=中原區小杉町三丁目245番地`
`| 電話號碼=44-744-3113`
`| 外部連結=`<http://www.city.kawasaki.jp/65/65nakahara/home/nakahara/index.htm>
`| 經度=`
`| 緯度=`
`| 地圖=`

}}

**中原區**（），[川崎市七区之一](../Page/川崎市.md "wikilink")，[日本職業足球聯賽球隊](../Page/日本職業足球聯賽.md "wikilink")[川崎前鋒的主場所在地](../Page/川崎前鋒.md "wikilink")。

## 交通

### 鐵路

  - [東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")

<!-- end list -->

  - [JR_JN_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JN_line_symbol.svg "fig:JR_JN_line_symbol.svg")
    [南武線](../Page/南武線.md "wikilink")：[平間站](../Page/平間站.md "wikilink") -
    [向河原站](../Page/向河原站.md "wikilink") - 武藏小杉站 -
    [武藏中原站](../Page/武藏中原站.md "wikilink") -
    [武藏新城站](../Page/武藏新城站.md "wikilink")
  - [JR_JO_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JO_line_symbol.svg "fig:JR_JO_line_symbol.svg")
    [橫須賀線](../Page/橫須賀線.md "wikilink")：武藏小杉站

<!-- end list -->

  - [東京急行電鐵](../Page/東京急行電鐵.md "wikilink")

<!-- end list -->

  - [Tokyu_TY_line_symbol.svg](https://zh.wikipedia.org/wiki/File:Tokyu_TY_line_symbol.svg "fig:Tokyu_TY_line_symbol.svg")
    [東橫線](../Page/東橫線.md "wikilink")：[元住吉站](../Page/元住吉站.md "wikilink")
    - 武藏小杉站 - [新丸子站](../Page/新丸子站.md "wikilink")
  - [Tokyu_MG_line_symbol.svg](https://zh.wikipedia.org/wiki/File:Tokyu_MG_line_symbol.svg "fig:Tokyu_MG_line_symbol.svg")
    [目黑線](../Page/目黑線.md "wikilink")：元住吉站 - 武藏小杉站 - 新丸子站

<!-- end list -->

  - 日本貨物鐵道

<!-- end list -->

  - [武藏野線](../Page/武藏野線.md "wikilink")： - 間的小杉隧道路段

## 外部連結