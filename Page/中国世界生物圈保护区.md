以下为32个被纳入[联合国教科文组织](../Page/联合国教科文组织.md "wikilink")“[人与生物圈计划](../Page/人与生物圈计划.md "wikilink")”[世界生物圈保护区网络的](../Page/世界生物圈保护区网络.md "wikilink")[中国生物圈保护区网络成员](../Page/中国生物圈保护区网络.md "wikilink")：

1.  [长白山生物圈保护区](../Page/长白山.md "wikilink")（[吉林](../Page/吉林.md "wikilink")，1979年）
2.  [鼎湖山生物圈保护区](../Page/鼎湖山.md "wikilink")（[广东](../Page/广东.md "wikilink")，1979年）
3.  [卧龙生物圈保护区](../Page/卧龙.md "wikilink")（[四川](../Page/四川.md "wikilink")，1979年）
4.  [梵净山生物圈保护区](../Page/梵净山.md "wikilink")（[贵州](../Page/贵州.md "wikilink")，1986年）
5.  [锡林郭勒草原生物圈保护区](../Page/锡林郭勒草原.md "wikilink")（[内蒙古](../Page/内蒙古.md "wikilink")，1987年）
6.  [武夷山生物圈保护区](../Page/武夷山.md "wikilink")（[福建](../Page/福建.md "wikilink")，1987年）
7.  [神农架生物圈保护区](../Page/神农架.md "wikilink")（[湖北](../Page/湖北.md "wikilink")，1990年）
8.  [中国温带荒漠区](../Page/中国温带荒漠区.md "wikilink")[博格达峰北麓生物圈保护区](../Page/博格达峰.md "wikilink")（[新疆](../Page/新疆.md "wikilink")，1990年）
9.  [盐城生物圈保护区](../Page/盐城.md "wikilink")（[江苏](../Page/江苏.md "wikilink")，1992年）
10. [西双版纳生物圈保护区](../Page/西双版纳.md "wikilink")（[云南](../Page/云南.md "wikilink")，1993年）
11. [天目山生物圈保护区](../Page/天目山.md "wikilink")（[浙江](../Page/浙江.md "wikilink")，1996年）
12. [茂兰生物圈保护区](../Page/貴州茂蘭國家級自然保護區.md "wikilink")（[贵州](../Page/贵州.md "wikilink")，1996年）
13. [丰林生物圈保护区](../Page/丰林.md "wikilink")（[黑龙江](../Page/黑龙江.md "wikilink")，1997年）
14. [九寨沟生物圈保护区](../Page/九寨沟.md "wikilink")（[四川](../Page/四川.md "wikilink")，1997年）
15. [南麂列岛海洋生物圈保护区](../Page/南麂列岛.md "wikilink")（[浙江](../Page/浙江.md "wikilink")，1998年）
16. [山口红树林生态生物圈保护区](../Page/山口红树林生态国家级自然保护区.md "wikilink")（[广西](../Page/广西.md "wikilink")，2000年）
17. [黄龙寺生物圈保护区](../Page/黄龙.md "wikilink")（[四川](../Page/四川.md "wikilink")，2000年）
18. [高黎贡山生物圈保护区](../Page/高黎贡山.md "wikilink")（[云南](../Page/云南.md "wikilink")，2000年）
19. [白水江生物圈保护区](../Page/白水江.md "wikilink")（[甘肃](../Page/甘肃.md "wikilink")，2000年）
20. [赛罕乌拉生物圈保护区](../Page/赛罕乌拉.md "wikilink")（[内蒙古](../Page/内蒙古.md "wikilink")，2001年）
21. [宝天曼生物圈保护区](../Page/宝天曼国家级自然保护区.md "wikilink")（[河南](../Page/河南.md "wikilink")，2001年）
22. [达赉湖生物圈保护区](../Page/达赉湖.md "wikilink")（[内蒙古](../Page/内蒙古.md "wikilink")，2002年）
23. [五大连池生物圈保护区](../Page/五大连池.md "wikilink")（[黑龙江](../Page/黑龙江.md "wikilink")，2003年）
24. [亚丁生物圈保护区](../Page/亚丁.md "wikilink")（[四川](../Page/四川.md "wikilink")，2003年）
25. [珠穆朗玛峰生物圈保护区](../Page/珠穆朗玛峰.md "wikilink")（[西藏](../Page/西藏.md "wikilink")，2004年）
26. [佛坪生物圈保护区](../Page/佛坪.md "wikilink")（[陕西](../Page/陕西.md "wikilink")，2004年）
27. [兴凯湖生物圈保护区](../Page/兴凯湖.md "wikilink")（[黑龙江](../Page/黑龙江.md "wikilink")，2007年）
28. [车八岭生物圈保护区](../Page/车八岭.md "wikilink")（[广东](../Page/广东.md "wikilink")，2007年）
29. [猫儿山生物圈保护区](../Page/猫儿山.md "wikilink")（[广西](../Page/广西.md "wikilink")，2011年）
30. [井冈山生物圈保护区](../Page/井冈山.md "wikilink")（[江西](../Page/江西.md "wikilink")，2012年）
31. [牛背梁生物圈保护区](../Page/牛背梁.md "wikilink")（[陕西](../Page/陕西.md "wikilink")，2012年）
32. [蛇岛、老铁山生物圈保护区](../Page/蛇岛.md "wikilink")（[辽宁](../Page/辽宁.md "wikilink")，2013年）
33. [黄山生物圈保护区](../Page/黄山.md "wikilink")（[安徽](../Page/安徽.md "wikilink")，2018年）

[1](http://www.un.org/chinese/News/story.asp?NewsID=19892)

[category:生物圈保护区](../Page/category:生物圈保护区.md "wikilink")
[category:中國生態](../Page/category:中國生態.md "wikilink")