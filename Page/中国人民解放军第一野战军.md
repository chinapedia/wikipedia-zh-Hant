**中国人民解放军第一野战军**是[中国共产党在](../Page/中国共产党.md "wikilink")[抗日战争后期组建的大型集团部队之一](../Page/抗日战争.md "wikilink")。它也是[第二次国共内战时期中国人民解放军的五大主力部队之一](../Page/第二次国共内战.md "wikilink")。中国人民解放军第一野战军由原[陕甘宁晋绥联防军](../Page/陕甘宁晋绥联防军.md "wikilink")、[晋绥解放区的](../Page/晋绥解放区.md "wikilink")[八路军以及其他地方武装发展起来](../Page/八路军.md "wikilink")，总兵力达15.5万人。

## 历史

全国抗战转入最后大反攻时，1945年8月11日，中共中央军委决定以晋绥军区部队主力组成晋绥野战军，隶属[陕甘宁晋绥联防军](../Page/陕甘宁晋绥联防军.md "wikilink")。联防军司令员贺龙、政治委员关向应分别兼任晋绥野战军司令员、政治委员（关向应因病未到职，贺龙兼代政治委员，后李井泉任政治委员），辖第三五八旅，独立第一、独立第二、独立第三旅和1945年11月成立的独立第四旅、1946年10月成立的独立第五旅。1945年8月21日，中共中央决定陕甘宁边区部队沿用陕甘宁晋绥联防军番号，司令员贺龙，代政治委员高岗，辖教导第一、教导第二旅，新编第四旅，警备第一旅兼关中军分区，警备第三旅兼三边军分区，第三五八旅兼陇东军分区，绥德军分区。1946年11月10日，中央军委决定撤销晋绥野战军、晋北野战军（司令员[周士第](../Page/周士第.md "wikilink")），部队整编成第一、第二、第三纵队，归晋绥军区直接指挥。不久，第一纵队调至陕甘宁晋绥联防军序列。

1947年2月10日，中央军委决定，晋绥军区第一纵队和陕甘宁晋绥联防军所辖新编第四旅、教导旅，警备第一、警备第三旅，共6个旅19个团2.8万余人，组成**陕甘宁野战集团军**，张宗逊任司令员，习仲勋任政治委员。1947年3月13日始，国军以34个旅25万余人从南、西、北三面向陕甘宁解放区发动重点进攻。其中，第一战区司令长官胡宗南指挥的20个旅，除以5个旅担任守备外，集中15个旅约14万余人从南线洛川、宜川等地直攻延安。中共中央决心主动放弃延安，依靠陕北群众基础好、地形险要、回旋余地大等有利条件，将国军胡宗南集团牵制在陕甘宁解放区，消灭其有生力量，以利其他战区作战。并调晋绥军区第二纵队西渡黄河，加入陕甘宁作战。

1947年3月17日，军委副主席兼总参谋长[彭德怀和](../Page/彭德怀.md "wikilink")[中共中央西北局书记](../Page/中共中央西北局.md "wikilink")[习仲勋](../Page/习仲勋.md "wikilink")，统一指挥所有驻陕甘宁的野战部队和地方武装，以第一纵队、第二纵队（3月17日入陕）、教导旅、新编第四旅等部组成**西北野战兵团**，陕甘宁野战集团军番号撤销。3月20日，成立了西北野战兵团指挥机构，彭德怀任西北野战兵团司令员兼政治委员，[张宗逊任副司令员](../Page/张宗逊.md "wikilink")，习仲勋任副政治委员。警备第一、警备第三旅仍归陕甘宁晋绥联防军指挥，担负地方作战任务。

1947年3月至7月，西北野战兵团“陕北三战三捷”，歼敌2.6万余人，以不足3万余人牵制了国民党军30多个旅于陕北战场，有力地策应了其他解放区的作战。

1947年7月31日，中央“小河会议”后，决定晋绥军区划归陕甘宁联防军建制领导，西北野战兵团改名为**西北解放军**，司令员兼政治委员彭德怀，后[张宗逊任第一副司令员](../Page/张宗逊.md "wikilink")，再后[赵寿山任第二副司令员](../Page/赵寿山.md "wikilink")，副政治委员[习仲勋](../Page/习仲勋.md "wikilink")，参谋长[张文舟](../Page/张文舟.md "wikilink")，辖第一、第二纵队和教导旅、新编第四旅、直属山炮营；决定彭德怀、习仲勋、张宗逊、[王震](../Page/王震.md "wikilink")、[刘景范组成中共西北野战军前线委员会](../Page/刘景范.md "wikilink")，彭德怀为书记。

晋绥军区第三纵队于1947年8月上旬西渡黄河到陕北，归西北野战军建制，当即参加了[沙家店战役担任阻援作战](../Page/沙家店战役.md "wikilink")。西北野战军总兵力达4.5万人。[沙家店战役一举歼灭胡宗南集团三大主力师之一的编第三十六师主力](../Page/沙家店战役.md "wikilink")6000余人，挫败了国军对陕北的重点进攻，扭转了西北战局，西北解放军从此由战略防御转入战略反攻。

1943年9月17日，以陕甘宁晋绥联防军警备第一、第三旅和骑兵第六师组成西北野战军第四纵队。1947年9月20日，决定以西野教导旅、新四旅组建第六纵队。1947年11月吕梁军区第五十一、第五十五、第五十八团组成独立第七旅，调入第一纵队建制。至此，西北野战军已发展到5个纵队和1个直属山炮营共7.6万人。

1948年3月17日，陕甘宁晋绥联防军改称[陕甘宁晋绥联防军区](../Page/陕甘宁晋绥联防军区.md "wikilink")，司令员贺龙，政治委员习仲勋，副司令员[王维舟](../Page/王维舟.md "wikilink")，参谋长[张经武](../Page/张经武.md "wikilink")，政治部主任[李卓然](../Page/李卓然.md "wikilink")，直属第一（延属，后组建警四旅）、第二（绥德，后组建警二旅）、第三（三边，新11旅兼）、第四（陇东，辖独13与独14团）、第五（关中，警一旅兼）、第六（黄龙，后组建警四旅）军分区和晋绥军区（二级军区，兵团级，辖绥蒙军区、吕梁军区）。

1949年2月，[西北野战军正式改编为第一野战军](../Page/西北野战军.md "wikilink")。此时下辖7个军（第1至第8军，缺第5军）另两个骑兵师。其中，第1、2、3、7、8军均为晋绥区部队发展而来，第4军为陕甘宁老部队，第6军为抗战后期从[晋察冀](../Page/晋察冀.md "wikilink")、[晋冀鲁豫调到陕甘宁晋绥的八路军](../Page/晋冀鲁豫边区.md "wikilink")。改编为第一野战军时，第7军在参加[太原围城战至夏天才归建](../Page/太原戰役.md "wikilink")，第8军在[绥远从未归建西北](../Page/绥远.md "wikilink")。[彭德怀任司令员兼政治委员](../Page/彭德怀.md "wikilink")，[张宗逊](../Page/张宗逊.md "wikilink")、[赵寿山分任第一](../Page/赵寿山.md "wikilink")、第二副司令员，[阎揆要任参谋长](../Page/阎揆要.md "wikilink")，[甘泗淇任政治部主任](../Page/甘泗淇.md "wikilink")，[王政柱](../Page/王政柱.md "wikilink")、[李夫克任副参谋长](../Page/李夫克.md "wikilink")，[张德生任政治部副主任](../Page/张德生.md "wikilink")。

组建第一野战军后，彭德怀电告中央：

> ...我军概况，现有九万五千人\[警四旅改为十二师，四千人在内\]，一、二两军五个师，每师平均七千八百余\[师编制\]。三军七师近八千人，九师不到六千人。四军三个师多者五千人，少者四千人。六军两个师，每师五千人。刻一九四八年全军伤亡四万二千九百七十余\[其中有被俘失踪逃亡九千人\]，干部占一万一千余\[班级占六千余人，连、排级近四千人，营级以上及医务人员等一千人\]。医愈归队者已有两千余人。经过冬季整训，干部已补齐，预备干部有四千余人。党员占全军三分之一强。[俘虏兵约占全军百分之八十](../Page/俘虏兵.md "wikilink")，连队比例更大，班长绝大多数是俘虏兵，排长、副排长亦占约半数，连指挥员各军中均有个别。绝大多数是四川人\[除四纵外\]，**基本上已成为南方军队**...

1949年5月太原战役结束后，华北第18、第19兵团调入关中转隶第一野战军，此时第一野战军总兵力达34.4万人。

  - 第一军：军长[贺炳炎](../Page/贺炳炎.md "wikilink")，政委[廖汉生](../Page/廖汉生.md "wikilink")，参谋长[陈外欧](../Page/陈外欧.md "wikilink")，政治部主任[冼恒汉](../Page/冼恒汉.md "wikilink")
      - 第一师：师长[黄新廷](../Page/黄新廷.md "wikilink")，政委[余秋里](../Page/余秋里.md "wikilink")
      - 第二师：师长[王尚荣](../Page/王尚荣.md "wikilink")，政委[颜金生](../Page/颜金生.md "wikilink")
      - 第三师：师长[傅传作](../Page/傅传作.md "wikilink")，政委[曹光琳](../Page/曹光琳.md "wikilink")
  - 第二军：军长兼政委[王震](../Page/王震.md "wikilink")，副军长[郭鹏](../Page/郭鹏.md "wikilink")、[顿星云](../Page/顿星云.md "wikilink")，副政委[王恩茂](../Page/王恩茂.md "wikilink")，参谋长[张希钦](../Page/张希钦.md "wikilink")，政治部主任[左齐](../Page/左齐.md "wikilink")。
      - 第四师：师长兼政委[杨秀山](../Page/杨秀山.md "wikilink")
      - 第五师：师长[徐国贤](../Page/徐国贤.md "wikilink")，政委[李铨](../Page/李铨.md "wikilink")
      - 第六师：师长[张仲瀚](../Page/张仲瀚.md "wikilink")，政委[曾涤](../Page/曾涤.md "wikilink")
  - 第三军：军长[许光达](../Page/许光达.md "wikilink")，政委[朱明](../Page/朱明.md "wikilink")，参谋长[李文清](../Page/李文清.md "wikilink")，副参谋长[邓家泰](../Page/邓家泰.md "wikilink")，政治部副主任[江勇](../Page/江勇.md "wikilink")
      - 第七师：师长[唐金龙](../Page/唐金龙.md "wikilink")，政委[梁仁芥](../Page/梁仁芥.md "wikilink")
      - 第八师：师长[杨嘉瑞](../Page/杨嘉瑞.md "wikilink")，政委[孟昭亮](../Page/孟昭亮.md "wikilink")
      - 第九师：师长[朱声达](../Page/朱声达.md "wikilink")，政委[王赤军](../Page/王赤军.md "wikilink")
  - 第四军：军长[王世泰](../Page/王世泰.md "wikilink")，政委[张仲良](../Page/张仲良.md "wikilink")，副军长[孙超群](../Page/孙超群.md "wikilink")，副政委兼政治部主任[朱辉照](../Page/朱辉照.md "wikilink")，参谋长[张文舟](../Page/张文舟.md "wikilink")
      - 第十师：师长[高锦纯](../Page/高锦纯.md "wikilink")，政委[左爱](../Page/左爱.md "wikilink")
      - 第十一师：师长[郭炳坤](../Page/郭炳坤.md "wikilink")，政委[高维嵩](../Page/高维嵩.md "wikilink")
      - 第十二师：师长[郭宝珊](../Page/郭宝珊.md "wikilink")，政委[李宗贵](../Page/李宗贵.md "wikilink")
  - 第六军：军长[罗元发](../Page/罗元发.md "wikilink")，政委[徐立清](../Page/徐立清.md "wikilink")，副政委兼政治部主任[饶正锡](../Page/饶正锡.md "wikilink")，参谋长[唐子奇](../Page/唐子奇.md "wikilink")，副参谋长[陈海涵](../Page/陈海涵.md "wikilink")
      - 第十六师：师长[吴宗先](../Page/吴宗先.md "wikilink")，政委[关盛志](../Page/关盛志.md "wikilink")
      - 第十七师：师长[程悦长](../Page/程悦长.md "wikilink")，政委[黄振棠](../Page/黄振棠.md "wikilink")
  - 第七军：军长[彭绍辉](../Page/彭绍辉.md "wikilink")，政委[孙志远](../Page/孙志远.md "wikilink")，参谋长[王兰麟](../Page/王兰麟.md "wikilink")，政治部主任黄忠学
      - 第十九师：师长[何辉燕](../Page/何辉燕.md "wikilink")，政委[朱绍田](../Page/朱绍田.md "wikilink")
      - 第二十师：师长[张新华](../Page/张新华.md "wikilink")，政委[龙福才](../Page/龙福才.md "wikilink")
  - 第八军：军长[姚喆](../Page/姚喆.md "wikilink")，副军长兼参谋长[王长江](../Page/王长江_\(开国大校\).md "wikilink")，政治部主任[裴周玉](../Page/裴周玉.md "wikilink")。
      - 第二十二师：师长[樊哲祥](../Page/樊哲祥.md "wikilink")，政委[黄立清](../Page/黄立清.md "wikilink")
      - 第二十三师：师长[罗斌](../Page/罗斌.md "wikilink")，政委[姜文华](../Page/姜文华.md "wikilink")
  - 骑兵第一师：师长[康健民](../Page/康健民.md "wikilink")，政委[李佐玉](../Page/李佐玉.md "wikilink")
  - 骑兵第二师：师长[王智](../Page/王智.md "wikilink")，政委[王再兴](../Page/王再兴.md "wikilink")

第一野战军先后发起春季战役和陕中战役，解放了西安及陕西中部广大地区。

1949年11月30日，中共中央军委决定，中国人民解放军第一野战军与西北军区合并，称中国人民解放军第一野战军暨西北军区。同样由[彭德怀担任司令员](../Page/彭德怀.md "wikilink")，[习仲勋任政治委员](../Page/习仲勋.md "wikilink")，[张宗逊](../Page/张宗逊.md "wikilink")、[赵寿山任副司令员](../Page/赵寿山.md "wikilink")，[甘泗淇任副政治委员兼政治部主任](../Page/甘泗淇.md "wikilink")，[阎揆要任参谋长](../Page/阎揆要.md "wikilink")，[王政柱](../Page/王政柱.md "wikilink")、[韩练成任副参谋长](../Page/韩练成.md "wikilink")，[张德生任政治部副主任](../Page/张德生.md "wikilink")。

  - 第一兵团：司令员兼政委[王震](../Page/王震.md "wikilink")，政治部主任[孙志远](../Page/孙志远.md "wikilink")。
      - [第一军](../Page/中国人民解放军第1军.md "wikilink")：军长[贺炳炎](../Page/贺炳炎.md "wikilink")，政委[廖汉生](../Page/廖汉生.md "wikilink")，副军长[王尚荣](../Page/王尚荣.md "wikilink")，副政委[冼恒汉](../Page/冼恒汉.md "wikilink")，参谋长[陈外欧](../Page/陈外欧.md "wikilink")。
          - 第一师：师长[傅传作](../Page/傅传作.md "wikilink")，政委[曾祥煌](../Page/曾祥煌.md "wikilink")。
          - 第二师：师长[王绍南](../Page/王绍南.md "wikilink")，政委[颜金生](../Page/颜金生.md "wikilink")。
          - 第三师：政委[曹光琳](../Page/曹光琳.md "wikilink")。
      - [第二军](../Page/中国人民解放军第2军.md "wikilink")：军长[郭鹏](../Page/郭鹏.md "wikilink")，政委[王恩茂](../Page/王恩茂.md "wikilink")，副军长[顿星云](../Page/顿星云.md "wikilink")，参谋长[张希钦](../Page/张希钦.md "wikilink")，政治部副主任[左齐](../Page/左齐.md "wikilink")。
          - 第四师：师长[杨秀山](../Page/杨秀山.md "wikilink")。
          - 第五师：师长[徐国贤](../Page/徐国贤.md "wikilink")，政委[李铨](../Page/李铨.md "wikilink")。
          - 第六师：师长[张仲瀚](../Page/张仲瀚.md "wikilink")，政委[曾涤](../Page/曾涤.md "wikilink")。
      - [第七军](../Page/中国人民解放军第7军.md "wikilink")：军长[彭绍辉](../Page/彭绍辉.md "wikilink")，政委[罗贵波](../Page/罗贵波.md "wikilink")，参谋长[何辉燕](../Page/何辉燕.md "wikilink")，政治部主任[侯维煜](../Page/侯维煜.md "wikilink")。
          - 第十九师：师长[朱绍田](../Page/朱绍田.md "wikilink")，政委[孙鸿志](../Page/孙鸿志.md "wikilink")。
          - 第二十师：师长[张新华](../Page/张新华.md "wikilink")，政委[龙福才](../Page/龙福才.md "wikilink")。
          - 第二十一师：师长[范忠祥](../Page/范忠祥.md "wikilink")，政委[李建良](../Page/李建良.md "wikilink")。
  - 第二兵团:司令员[许光达](../Page/许光达.md "wikilink")，政委[王世泰](../Page/王世泰.md "wikilink")，副政委[徐立清](../Page/徐立清.md "wikilink")，参谋长[张文舟副參謀長](../Page/张文舟.md "wikilink")\[董熙\]。
      - [第三军](../Page/中国人民解放军第3军.md "wikilink"):军长[黄新廷](../Page/黄新廷.md "wikilink")，政委[朱明](../Page/朱明.md "wikilink")，副军长[唐金龙](../Page/唐金龙.md "wikilink")，副政委[朱辉照](../Page/朱辉照.md "wikilink")，参谋长[朱文清](../Page/朱文清.md "wikilink")
          - 第七师：师长[张开基](../Page/张开基.md "wikilink")，政委[梁仁芥](../Page/梁仁芥.md "wikilink")。
          - 第八师：师长[杨嘉瑞](../Page/杨嘉瑞.md "wikilink")，政委[孟昭亮](../Page/孟昭亮.md "wikilink")。
          - 第九师：师长[朱声达](../Page/朱声达.md "wikilink")，政委[王赤军](../Page/王赤军.md "wikilink")。
      - [第四军](../Page/中国人民解放军第4军.md "wikilink"):军长[张达志](../Page/张达志.md "wikilink")，政委[张仲良](../Page/张仲良.md "wikilink")，副军长[孙超群](../Page/孙超群.md "wikilink")，参谋长[姚知一](../Page/姚知一.md "wikilink")。
          - 第十师：师长[高锦纯](../Page/高锦纯.md "wikilink")，政委[左爱](../Page/左爱.md "wikilink")。
          - 第十一师：师长[郭炳坤](../Page/郭炳坤.md "wikilink")，政委[高维嵩](../Page/高维嵩.md "wikilink")。
          - 第十二师：师长[郭宝珊](../Page/郭宝珊.md "wikilink")，政委[李宗贵](../Page/李宗贵.md "wikilink")。
      - [第六军](../Page/中国人民解放军第6军.md "wikilink")：军长[罗元发](../Page/罗元发.md "wikilink")，政委[张贤约](../Page/张贤约.md "wikilink")，参谋长[张贤约](../Page/张贤约.md "wikilink")，政治部主任[黄振棠](../Page/黄振棠.md "wikilink")。
          - 第十六师：师长[吴宗先](../Page/吴宗先.md "wikilink")，政委[关盛治](../Page/关盛治.md "wikilink")。
          - 第十七师：师长[程悦长](../Page/程悦长.md "wikilink")，政委[黄振棠](../Page/黄振棠.md "wikilink")（兼）。
          - 第十八师：师长[陈刚](../Page/陈刚.md "wikilink")，政委[肖头生](../Page/肖头生.md "wikilink")。
  - 第十九兵团：司令员[杨得志](../Page/杨得志.md "wikilink")，政委[李志民](../Page/李志民.md "wikilink")，副司令员[耿飚](../Page/耿飚.md "wikilink")、[葛晏春](../Page/葛晏春.md "wikilink")，参谋长[耿飚](../Page/耿飚.md "wikilink")，政治部主任[潘自力](../Page/潘自力.md "wikilink")。
      - [第六十三军](../Page/中国人民解放军第63军.md "wikilink")：军长[郑维山](../Page/郑维山.md "wikilink")，政委[王宗槐](../Page/王宗槐.md "wikilink")，副军长兼参谋长[易耀彩](../Page/易耀彩.md "wikilink")，副政委[龙道全](../Page/龙道全.md "wikilink")，政治部主任[陆平](../Page/陆平.md "wikilink")。
          - 第一八七师：师长[张英辉](../Page/张英辉.md "wikilink")，政委[张迈君](../Page/张迈君.md "wikilink")。
          - 第一八八师：师长[宋玉林](../Page/宋玉林.md "wikilink")，政委[李真](../Page/李真.md "wikilink")。
          - 第一八九师：师长[杜瑜华](../Page/杜瑜华.md "wikilink")，政委[蔡长元](../Page/蔡长元.md "wikilink")。
          - 骑兵第六师：师长[刘春芳](../Page/刘春芳.md "wikilink")，政委[刘光裕](../Page/刘光裕.md "wikilink")。
      - [第六十四军](../Page/中国人民解放军第64军.md "wikilink")：军长[曾思玉](../Page/曾思玉.md "wikilink")，政委[王昭](../Page/王昭.md "wikilink")，副军长[唐子安](../Page/唐子安.md "wikilink")，副政委[傅崇碧](../Page/傅崇碧.md "wikilink")。
          - 第一九○师：师长[陈信忠](../Page/陈信忠.md "wikilink")，政委[边疆](../Page/边疆.md "wikilink")。
          - 第一九一师：师长[谢正荣](../Page/谢正荣.md "wikilink")，政委[陈宜贵](../Page/陈宜贵.md "wikilink")。
          - 第一九二师：师长[马卫华](../Page/马卫华.md "wikilink")，政委[王海亭](../Page/王海亭.md "wikilink")。
      - [第六十五军](../Page/中国人民解放军第65军.md "wikilink")：军长[邱蔚](../Page/邱蔚.md "wikilink")，政委[王道邦](../Page/王道邦.md "wikilink")，副军长兼参谋长[王克斌](../Page/王克斌.md "wikilink")，副政委兼政治部主任[蔡顺礼](../Page/蔡顺礼.md "wikilink")。
          - 第一九三师：师长[郑三生](../Page/郑三生.md "wikilink")，政委[杨银生](../Page/杨银生.md "wikilink")。
          - 第一九四师：师长[赵文进](../Page/赵文进.md "wikilink")，政委[陈亚夫](../Page/陈亚夫.md "wikilink")。
          - 第一九五师：师长[王志廉](../Page/王志廉.md "wikilink")（代）。
  - 陕西军区（第十九兵团兼）：司令员[杨得志](../Page/杨得志.md "wikilink")，政治委员[马明方](../Page/马明方.md "wikilink")、[李志民](../Page/李志民.md "wikilink")，副司令员[耿飚](../Page/耿飚.md "wikilink")、[吴岱峰](../Page/吴岱峰.md "wikilink")、[左协中](../Page/左协中.md "wikilink")，参谋长[耿飚](../Page/耿飚.md "wikilink")(兼)，副参谋长[杨捷](../Page/杨捷.md "wikilink")，政治部副主任[牛书申](../Page/牛书申.md "wikilink")。
      - 陕南军区（[第十九军兼](../Page/中国人民解放军第19军.md "wikilink")）：司令员[刘金轩](../Page/刘金轩.md "wikilink")，政委[张邦英](../Page/张邦英.md "wikilink")，副司令员[陈先瑞](../Page/陈先瑞.md "wikilink")，副政委兼政治部主任[李耀](../Page/李耀.md "wikilink")，参谋长[薛克忠](../Page/薛克忠.md "wikilink")。
          - 两郧军分区：司令员[梁励生](../Page/梁励生.md "wikilink")，政委[杨锐](../Page/杨锐.md "wikilink")。
          - 商洛军分区：司令员[孙光](../Page/孙光.md "wikilink")，政委[王力](../Page/王力.md "wikilink")。
          - 安康军分区：司令员[谭友夫](../Page/谭友夫.md "wikilink")，政委[唐方雷](../Page/唐方雷.md "wikilink")。
          - 汉中军分区：司令员[张涛](../Page/张涛.md "wikilink")，政委[白成铭](../Page/白成铭.md "wikilink")。
          - 第五十五师：师长[符生辉](../Page/符生辉.md "wikilink")，政委[强明](../Page/强明.md "wikilink")。
          - 第五十七师：师长[张复振](../Page/张复振.md "wikilink")，政委[张文彬](../Page/张文彬_\(消歧义\).md "wikilink")。
      - 陕北军区：司令员[吴岱峰](../Page/吴岱峰.md "wikilink")，政委[李鹤邦](../Page/李鹤邦.md "wikilink")，副司令员[胡景铎](../Page/胡景铎.md "wikilink")，参谋长[李硕](../Page/李硕.md "wikilink")，政治部主任[惠世恭](../Page/惠世恭.md "wikilink")。
          - 榆林军分区：司令员[惠世恭兼](../Page/惠世恭兼.md "wikilink")，政委[朱侠夫](../Page/朱侠夫.md "wikilink")。
          - 黄龙军分区：司令员[马公里代](../Page/马公里代.md "wikilink")，政委[强自珍](../Page/强自珍.md "wikilink")。
      - 大荔军分区：司令员[王清毅](../Page/王清毅.md "wikilink")，政委[刘文蔚](../Page/刘文蔚.md "wikilink")。
      - 三原军分区：司令员[黄子祥](../Page/黄子祥.md "wikilink")，政委[白治民](../Page/白治民.md "wikilink")。
      - 邠县军分区：司令员[张占云](../Page/张占云.md "wikilink")，政委[杨伯伦](../Page/杨伯伦.md "wikilink")。
      - 渭南军分区：司令员[于占彪](../Page/于占彪.md "wikilink")，政委[白清江](../Page/白清江.md "wikilink")。
      - 咸阳军分区：司令员[王宝珊](../Page/王宝珊.md "wikilink")，政委[张忠](../Page/张忠.md "wikilink")。
      - 宝鸡军分区：司令员[陈国栋](../Page/陈国栋.md "wikilink")，政委[吕剑人](../Page/吕剑人.md "wikilink")。
  - 甘肃军区（第二兵团兼）：司令员[许光达](../Page/许光达.md "wikilink")，政治委员[王世泰](../Page/王世泰.md "wikilink")，副司令员[徐国珍](../Page/徐国珍.md "wikilink")、[任谦](../Page/任谦.md "wikilink")、[周祥初](../Page/周祥初.md "wikilink")，副政委[张德生](../Page/张德生.md "wikilink")、[孙作宾](../Page/孙作宾.md "wikilink")，参谋长[张文舟](../Page/张文舟.md "wikilink")，政治部主任[朱明](../Page/朱明.md "wikilink")，副参谋长[侯世奎](../Page/侯世奎.md "wikilink")，政治部副主任[王再兴](../Page/王再兴.md "wikilink")。
      - 天水军分区（\[第七军兼）：副军长兼司令员[孙超群](../Page/孙超群.md "wikilink")，政委[高峰](../Page/高峰.md "wikilink")
      - 平凉军分区：司令员[何远平](../Page/何远平.md "wikilink")，政委[李科](../Page/李科.md "wikilink")。
      - 庆阳军分区：司令员[刘明山](../Page/刘明山.md "wikilink")，政委[党永亮](../Page/党永亮.md "wikilink")。
      - 武都军分区：司令员[陈仕南](../Page/陈仕南.md "wikilink")，政委[李正廷](../Page/李正廷.md "wikilink")。
      - 岷山军分区：司令员[李启贤](../Page/李启贤.md "wikilink")，政委[罗斌](../Page/罗斌.md "wikilink")。
      - 张掖军分区（第三军第七师兼）：师长兼司令员[张开基](../Page/张开基.md "wikilink")，政委[刘昌汉](../Page/刘昌汉.md "wikilink")。
      - 武威军分区（第三军第八师兼）：师长兼司令员[杨嘉瑞](../Page/杨嘉瑞.md "wikilink")，政委[王俊](../Page/王俊.md "wikilink")。
      - 酒泉军分区（第三军第九师兼）：师长兼司令员[朱声达](../Page/朱声达.md "wikilink")，政委[刘长亮](../Page/刘长亮.md "wikilink")。
      - 临夏军分区（第四军第十一师兼）：师长兼司令员[郭炳坤](../Page/郭炳坤.md "wikilink")，政委[高维嵩](../Page/高维嵩.md "wikilink")。
      - 独立第一师：师长[高凌云](../Page/高凌云.md "wikilink")，政委[江子英](../Page/江子英.md "wikilink")。
      - 骑兵第二师：师长[王智](../Page/王智.md "wikilink")，政委[惠志高](../Page/惠志高.md "wikilink")。
  - 宁夏军区（第六十五军兼）：司令员[邱蔚](../Page/邱蔚.md "wikilink")，政委[王道邦](../Page/王道邦.md "wikilink")，副司令员[黄罗斌](../Page/黄罗斌.md "wikilink")、[曹又参](../Page/曹又参.md "wikilink")，参谋长[牛化东](../Page/牛化东.md "wikilink")，政治部主任[孙润华](../Page/孙润华.md "wikilink")。
      - 独立第一师：师长[李治州](../Page/李治州.md "wikilink")，政委[麻志浩](../Page/麻志浩.md "wikilink")
  - 青海军区（第一军兼）
  - 新疆军区（第一兵团兼）：司令员兼政委[彭德怀](../Page/彭德怀.md "wikilink")（兼），代司令员兼代政委[王震](../Page/王震.md "wikilink")，副司令员[陶峙岳](../Page/陶峙岳.md "wikilink")、[赛福鼎·艾则孜](../Page/赛福鼎·艾则孜.md "wikilink")，参谋长[张希钦](../Page/张希钦.md "wikilink")，政治部主任[徐立清](../Page/徐立清.md "wikilink")，副参谋长[曹震五](../Page/曹震五.md "wikilink")、[哈沙洛夫](../Page/哈沙洛夫.md "wikilink")，政治部副主任[曾涤](../Page/曾涤.md "wikilink")、[支亚](../Page/支亚.md "wikilink")。
      - 迪化军区（第六军兼）：司令员[罗元发](../Page/罗元发.md "wikilink")，政委[张贤约](../Page/张贤约.md "wikilink")，副司令员[诺苏甫汉](../Page/诺苏甫汉.md "wikilink")，参谋长[陈海涵](../Page/陈海涵.md "wikilink")，政治部主任[魏志明](../Page/魏志明.md "wikilink")，政治部副主任[何农泰益甫](../Page/何农泰益甫.md "wikilink")。
      - 伊宁军区（第五军兼）：司令员[法铁依·伊凡诺维奇·列斯肯](../Page/法铁依·伊凡诺维奇·列斯肯.md "wikilink")，政委[顿星云](../Page/顿星云.md "wikilink")，副司令员兼参谋长[马尔果夫·伊斯哈科夫](../Page/马尔果夫·伊斯哈科夫.md "wikilink")，副政治委员[曹达诺夫·扎伊尔](../Page/曹达诺夫·扎伊尔.md "wikilink")，政治部主任[努力也夫](../Page/努力也夫.md "wikilink")，政治部副主任[李辉和](../Page/李辉和.md "wikilink")。
          - 第13师：师长[买买提伊敏·伊敏诺夫](../Page/买买提伊敏·伊敏诺夫.md "wikilink")，政治委员[马洪山](../Page/马洪山.md "wikilink")
          - 第14师：师长[阿里木坚诺夫·乌拉拜音](../Page/依不拉音拜.md "wikilink")，政治委员[胡政](../Page/胡政.md "wikilink")。
      - 喀什军区（第二军兼）：司令员[郭鹏](../Page/郭鹏.md "wikilink")，政委[王恩茂](../Page/王恩茂.md "wikilink")，副司令员[伊漫诺夫](../Page/伊漫诺夫.md "wikilink")，参谋长[贺盛贵](../Page/贺盛贵.md "wikilink")，政治部主任[左齐](../Page/左齐.md "wikilink")，政治部副主任[塞拉代夫](../Page/塞拉代夫.md "wikilink")。
      - 第22兵团：司令员[陶峙岳](../Page/陶峙岳.md "wikilink")，政治委员[王震](../Page/王震.md "wikilink")，副司令员[赵锡光](../Page/赵锡光.md "wikilink")、副政治委员[饶正锡](../Page/饶正锡.md "wikilink")，参谋长[陶晋初](../Page/陶晋初.md "wikilink")，政治部主任[李铨](../Page/李铨.md "wikilink")。
          - 第九军：军长[赵锡光](../Page/赵锡光.md "wikilink")，政治委员[张仲瀚](../Page/张仲瀚.md "wikilink")，副军长[王根僧](../Page/王根僧.md "wikilink")、[陈德法](../Page/陈德法.md "wikilink")，参谋长[李祖堂](../Page/李祖堂.md "wikilink")，副参谋长[曾文思](../Page/曾文思.md "wikilink")、[朱文盈](../Page/朱文盈.md "wikilink")。
              - 第二十五师：师长[刘振世](../Page/刘振世.md "wikilink")、政委[贺振新](../Page/贺振新.md "wikilink")
              - 第二十六师：师长[罗汝正](../Page/罗汝正.md "wikilink")、政委[王季龙](../Page/王季龙.md "wikilink")
              - 第二十七师：师长[陈俊](../Page/陈俊.md "wikilink")、政委[龙炳初](../Page/龙炳初.md "wikilink")
          - 骑兵第七师：师长[韩有文](../Page/韩有文.md "wikilink")、政委[于春山](../Page/于春山.md "wikilink")
          - 骑兵第八师：师长[马平林](../Page/马平林.md "wikilink")、政委[张献奎](../Page/张献奎.md "wikilink")

1950年4月，根据中共中央军委决定，中国人民解放军第一野战军番号撤销，所属部队归[西北军区建制](../Page/西北军区.md "wikilink")。

## 参考文献

## 外部链接

  - [中国人民解放军第一野战军](https://web.archive.org/web/20070929153740/http://www.ndcnc.gov.cn/datalib/2001/MartialCyclopaedic/DL/DL-3155)

[\#1](../Category/中国人民解放军野战军.md "wikilink")