**中国人民解放军陆军第七十一集团军**，隶属[东部战区陆军](../Page/中国人民解放军东部战区陆军.md "wikilink")，军部驻地[江苏省](../Page/江苏省.md "wikilink")[徐州市](../Page/徐州市.md "wikilink")。

## 沿革

[第二次国共内战初期](../Page/第二次国共内战.md "wikilink")，组建**晋冀鲁豫野战军第六纵队**（即后来步兵第三十四旅的前身），该纵队是从1932年11月在[鄂豫皖苏区重新组建的](../Page/鄂豫皖苏区.md "wikilink")[红二十五军七十四师一部](../Page/红二十五军.md "wikilink")、[抗日战争中成立的](../Page/抗日战争.md "wikilink")[八路军](../Page/八路军.md "wikilink")[一二九师东进纵队一部](../Page/129师.md "wikilink")、一二九师三八六旅七七一团一部、以及河北抗日义勇军冀察游击队一部发展而来。1945年10月15日，[晋冀鲁豫军区将](../Page/晋冀鲁豫军区.md "wikilink")[太行军区部队组成的](../Page/太行军区.md "wikilink")[韦（杰）](../Page/韦杰.md "wikilink")[张（国传）支队](../Page/张国传.md "wikilink")、[石（志本）](../Page/石志本.md "wikilink")[何（柱成）支队](../Page/何柱成.md "wikilink")、[秦（基伟）](../Page/秦基伟.md "wikilink")[向（守志）支队与太行军区第六军分区直属队合编成为晋冀鲁豫野战军第六纵队](../Page/向守志.md "wikilink")，司令员[王宏坤](../Page/王宏坤.md "wikilink")，政委[段君毅](../Page/段君毅.md "wikilink")。下辖第十六、第十七、第十八旅。1946年10月，在[巨野战役徐庄战斗中](../Page/巨野战役.md "wikilink")，第六纵队第十八旅第五十二团第一连机枪班班长[王克勤率领全班击退敌军多次反击](../Page/王克勤.md "wikilink")，守住了阵地。战后，[延安](../Page/延安.md "wikilink")《[解放日报](../Page/解放日报.md "wikilink")》发表《普遍开展王克勤运动》的社论，从而在全军开展了“王克勤运动”，提高了部队战斗力。1947年8月，在[挺进中原强渡](../Page/挺进中原.md "wikilink")[汝河战斗中](../Page/汝河.md "wikilink")，第六纵队第十八旅为[中原野战军司政机关突围打开道路](../Page/中原野战军.md "wikilink")。1948年5月，改称**中原野战军第六纵队**，司令员[王近山](../Page/王近山.md "wikilink")，政委[杜义德](../Page/杜义德.md "wikilink")。1948年7月，第六纵队攻克[襄阳城](../Page/襄阳.md "wikilink")，俘虏特务头子[康泽](../Page/康泽.md "wikilink")。战后第十七旅第四十九团获授“襄阳特功团”称号。参加[淮海战役后](../Page/淮海战役.md "wikilink")，第六纵队于1949年2月9月在[安徽省](../Page/安徽省.md "wikilink")[蒙城县改编为](../Page/蒙城县.md "wikilink")**中国人民解放军第十二军**，军长兼政委[王近山](../Page/王近山.md "wikilink")，各旅分别改称第三十四、第三十五、第三十六师。此后，第十二军在[中国人民解放军第二野战军第三兵团建制内](../Page/中国人民解放军第二野战军.md "wikilink")，参加了[渡江战役及进军](../Page/渡江战役.md "wikilink")[四川](../Page/四川.md "wikilink")，1949年11月29日参与协同攻占[重庆](../Page/重庆.md "wikilink")。此后又参加[成都战役](../Page/成都战役.md "wikilink")\[1\]。

1950年12月，第十二军参加[朝鲜战争](../Page/朝鲜战争.md "wikilink")，改称**[中国人民志愿军第十二军](../Page/中国人民志愿军.md "wikilink")**，原第十一军的第三十一师编入第十二军，第三十六师（不含第一〇六团）脱离第十二军建制。第十二军入[朝后](../Page/朝鲜半岛.md "wikilink")，随即投入[第五次战役](../Page/抗美援朝战争第五次战役.md "wikilink")，在第五次战役第二阶段，第三十一师第九十一团在被敌军重兵包围在[三八线以南近](../Page/三八线.md "wikilink")100公里时，成建制突围归队。在[上甘岭战役中](../Page/上甘岭战役.md "wikilink")，第三十一师收复并巩固了除两个班阵地外的全部表面阵地。在抗美援朝战争中，第十二军涌现出[杨春增](../Page/杨春增.md "wikilink")（第三十五师第一〇四团副排长）、[伍先华](../Page/伍先华.md "wikilink")（第三十四师第一〇〇团第二连班长）、[胡修道](../Page/胡修道.md "wikilink")（第三十一师第九十一团第五连战士）等战斗英雄。在解放战争和抗美援朝战争中，第十二军共歼敌约50万人\[2\]。

1954年4月，第十二军从朝鲜回国，先后驻防[安徽省](../Page/安徽省.md "wikilink")[合肥市和](../Page/合肥市.md "wikilink")[江苏省](../Page/江苏省.md "wikilink")[徐州市](../Page/徐州市.md "wikilink")。1960年代初，第十二军涌现出“[郭兴福教学法](../Page/郭兴福.md "wikilink")”（郭兴福时任第三十四师第一〇〇团第二连副连长），在[叶剑英](../Page/叶剑英.md "wikilink")、[罗瑞卿的倡导下推广到全军](../Page/罗瑞卿.md "wikilink")，掀起了“大比武”运动。1969年12月[中国人民解放军全军重排番号](../Page/中国人民解放军.md "wikilink")，第三十一师改称第三十六师\[3\]。

1985年，在[百万大裁军中](../Page/百万大裁军.md "wikilink")，陆军第十二军改编为**中国人民解放军陆军第十二集团军**（原代号为中国人民解放军83226部队，后代号改为中国人民解放军73061部队），隶属[南京军区](../Page/南京军区.md "wikilink")，军部仍驻江苏省徐州市，下辖第三十四、第三十五、第三十六师和原属第六十军的第一七九师（即“临汾旅”，1971年开始担任迎外表演任务）共计4个步兵师，并编入坦克第二师（“王杰班”即该师工兵营第一连第五班）、炮兵旅、高炮旅。1989年5月，步兵第三十六师全师空运[北京参与执行](../Page/北京.md "wikilink")[戒严任务](../Page/六四事件.md "wikilink")。1990年代初，步兵第三十六师被中央军委确定为应急机动作战师。1998年后，步兵第三十四师改为摩步旅，坦克第二师改编为装甲师。步兵第三十五师改为江苏省陆军预备役师。在[1999年国庆阅兵中](../Page/首都各界庆祝中华人民共和国成立50周年大会.md "wikilink")，由陆军第十二集团军反坦克团的[红箭8-E导弹发射车组成车辆第](../Page/红箭-8反坦克导弹.md "wikilink")7方队接受检阅\[4\]。

2016年，中国人民解放军七大军区撤销，成立五大战区，陆军第十二集团军划归[中国人民解放军东部战区陆军](../Page/中国人民解放军东部战区陆军.md "wikilink")\[5\]。

2017年4月18日，[中共中央总书记](../Page/中共中央总书记.md "wikilink")、[国家主席](../Page/中华人民共和国主席.md "wikilink")、[中央军委主席](../Page/中央军事委员会主席.md "wikilink")[习近平在](../Page/习近平.md "wikilink")[北京](../Page/北京.md "wikilink")[八一大楼接见新调整组建的](../Page/八一大楼.md "wikilink")84个军级单位主官，并对各单位发布训令；[中共中央政治局委员](../Page/中共中央政治局委员.md "wikilink")、[中央军委副主席](../Page/中央军事委员会副主席.md "wikilink")[许其亮宣读习近平主席签发的中央军委关于调整组建军兵种部队和省军区系统军级单位的命令](../Page/许其亮.md "wikilink")、决定\[6\]。在这次军级单位调整组建中，以陆军第十二集团军为基础，调整组建**中国人民解放军陆军第七十一集团军**\[7\]\[8\]\[9\]。军部驻地江苏省徐州市\[10\]。

## 编制

1985年陆军第十二军改编为陆军第十二集团军，辖步兵第三十四师、第三十六师，撤销第三十五师（以师部为基础组建陆军第十二集团军炮兵旅旅部，下属第一〇三团划归第三十四师建制，以第一〇四团为基础组建第十二集团军工兵团，第一〇五团机关和师直通信营编为第十二集团军通信团，师炮兵团划归第十二集团军炮兵旅建制），原属第六十军的步兵第一七九师（即著名的“临汾旅”，1971年开始担任迎外表演任务）改属该集团军，并编入坦克第二师（“王杰班”即该师工兵营一连五班）、炮兵旅（三十五师师部、三十五师炮兵团、十二军炮兵团）和高炮旅。1998年后，步兵第三十四师改为摩步旅，步兵第一七九师改为摩步旅，坦克第二师改编为装甲师。2003年，步兵第三十六师改为摩步旅，2013年又改编为特种作战旅\[11\]。

2016年[深化国防和军队改革前](../Page/深化国防和军队改革.md "wikilink")，陆军第十二集团军下辖：机械化步兵第三十四旅\[12\]、机械化步兵第三十五旅\[13\]、摩托化步兵第一七九旅\[14\]、装甲第二旅\[15\]、炮兵旅\[16\]、防空旅\[17\]、特种作战旅\[18\]，以及集团军直属的通信团\[19\]、工兵团\[20\]等。

2017年组建的陆军第七十一集团军下辖：

  - 重型合成第二旅
  - 重型合成第三十五旅
  - 重型合成第一六〇旅
  - 中型合成第一七八旅
  - 轻型合成第一七九旅
  - 重型合成第二三五旅
  - 陆航第七十一旅
  - 炮兵第七十一旅
  - 防空第七十一旅
  - 特战第七十一旅
  - 工化第七十一旅
  - 勤务支援第七十一旅

## 历任领导

### 晋冀鲁豫野战军、中原野战军第六纵队

  - 第六纵队司令员

<!-- end list -->

  - [王宏坤](../Page/王宏坤.md "wikilink")（1945年11月—1946年9月，晋冀鲁豫军区副司令员兼）\[21\]
  - [王近山](../Page/王近山.md "wikilink")（1948年5月—1949年2月）

<!-- end list -->

  - 第六纵队副司令员

<!-- end list -->

  - 王近山（1945年11月—1946年7月）
  - [韦杰](../Page/韦杰.md "wikilink")（1945年11月—1948年2月）

<!-- end list -->

  - 第六纵队政治委员

<!-- end list -->

  - [段君毅](../Page/段君毅.md "wikilink")（1945年11月—1946年4月）\[22\]
  - [杜义德](../Page/杜义德.md "wikilink")（1946年—1949年2月）

<!-- end list -->

  - 第六纵队副政治委员

<!-- end list -->

  - [鲍先志](../Page/鲍先志.md "wikilink")（1947年6月—1948年5月）

<!-- end list -->

  - 第六纵队参谋长

<!-- end list -->

  - [张廷发](../Page/张廷发.md "wikilink")（1945年11月—1946年）
  - [姚继鸣](../Page/姚继鸣.md "wikilink")（1946年5月—1948年5月）

<!-- end list -->

  - 第六纵队政治部主任

<!-- end list -->

  - 鲍先志（1945年11月—1948年5月，1947年6月起为副政治委员兼）
  - [李震](../Page/李震_\(官员\).md "wikilink")（1948年5月—1949年2月，副主任代理）

<!-- end list -->

  - 第六纵队政治部副主任

<!-- end list -->

  - 李震（1947年12月—1949年2月）

<!-- end list -->

  - 第六纵队供给部部长

<!-- end list -->

  - [王耀显](../Page/王耀显.md "wikilink")（？—1949年2月）

<!-- end list -->

  - 第六纵队卫生部部长

<!-- end list -->

  - [詹少联](../Page/詹少联.md "wikilink")（1945年11月—1949年2月，兼卫生部政治委员）

### 中国人民解放军第十二军

  - 中国人民解放军第十二军军长

<!-- end list -->

1.  [王近山](../Page/王近山.md "wikilink")（1949年2月—1951年2月）
2.  [曾绍山](../Page/曾绍山.md "wikilink")（1951年2月—1953年4月）
3.  [肖永银](../Page/肖永银.md "wikilink")（1953年10月—1954年6月，副军长代理；1954年6月—1955年1月）
4.  [李德生](../Page/李德生.md "wikilink")
    <small>少将</small>（1955年3月—1955年9月，副军长代理；1955年9月—1957年9月）
5.  肖永银 <small>少将</small>（1957年9月—1960年8月）
6.  李德生 <small>少将</small>（1960年8月—1974年12月）
7.  [官俊亭](../Page/官俊亭.md "wikilink")（1975年8月—1978年5月）
8.  [成冲霄](../Page/成冲霄.md "wikilink")（1978年5月—1981年6月）
9.  [任保俗](../Page/任保俗.md "wikilink")（1981年6月—1983年5月）
10. [郭锡章](../Page/郭锡章.md "wikilink")（1983年5月—1985年8月）

<!-- end list -->

  - 中国人民解放军第十二军副军长

<!-- end list -->

  - [肖永银](../Page/肖永银.md "wikilink")（1949年2月—1954年6月）
  - [王蕴瑞](../Page/王蕴瑞.md "wikilink")（？—？）
  - [李德生](../Page/李德生.md "wikilink")（1952年9月—1955年9月）
  - [尤太忠](../Page/尤太忠.md "wikilink") <small>少将</small>（1954年—1957年）
  - [阮贤榜](../Page/阮贤榜.md "wikilink") <small>少将</small>（？—1961年4月）
  - [贺光华](../Page/贺光华.md "wikilink") <small>少将</small>（1957年—1959年）
  - [谭友夫](../Page/谭友夫.md "wikilink") <small>少将</small>（？—？）
  - [官俊亭](../Page/官俊亭.md "wikilink") <small>少将</small>（？—1975年8月）
  - [李长林](../Page/李长林.md "wikilink") <small>大校</small>（1960年—1969年）
  - [娄学政](../Page/娄学政.md "wikilink")（1968年7月—1979年2月）
  - [成冲霄](../Page/成冲霄.md "wikilink")（1968年8月—1971年1月）
  - [张秀伦](../Page/张秀伦.md "wikilink")（？—？）
  - [任保裕](../Page/任保裕.md "wikilink")（？—？）
  - [权银刚](../Page/权银刚.md "wikilink")（？—？）
  - [李全贵](../Page/李全贵.md "wikilink")（？—1978年8月）
  - [赵金来](../Page/赵金来.md "wikilink")（1975年10月—1983年）
  - [许克杰](../Page/许克杰.md "wikilink")（1976年9月—？）\[23\]
  - [甄申](../Page/甄申.md "wikilink")（？—1983年5月）
  - [张双春](../Page/张双春.md "wikilink")（？—？）

<!-- end list -->

  - 中国人民解放军第十二军政治委员

<!-- end list -->

1.  王近山（1949年2月—1951年2月，军长兼）
2.  [曾绍山](../Page/曾绍山.md "wikilink")（1951年2月—1953年4月，军长兼）
3.  李震（1953年4月—1954年5月）
4.  [史景班](../Page/史景班.md "wikilink") <small>少将</small>（1962年6月—1963年6月）
5.  [张文碧](../Page/张文碧.md "wikilink") <small>少将</small>（1963年10月—1970年）
6.  [宋佩璋](../Page/宋佩璋.md "wikilink")（1970年—1971年1月，兼任）
7.  [成冲霄](../Page/成冲霄.md "wikilink")（1971年1月—1975年5月）
8.  [魏金山](../Page/魏金山.md "wikilink")（1978年5月—1982年10月）
9.  [刘新增](../Page/刘新增.md "wikilink")（1983年5月—1985年8月）

<!-- end list -->

  - 中国人民解放军第十二军副政治委员

<!-- end list -->

  - 李震（1949年2月—1953年4月）
  - [刘昌](../Page/刘昌.md "wikilink")（？—？）
  - [史景班](../Page/史景班.md "wikilink") <small>少将</small>（？—1962年6月）
  - [王翀](../Page/王翀.md "wikilink") <small>大校</small>（？—？）
  - [张春森](../Page/张春森.md "wikilink")（？—？）
  - [宋佩璋](../Page/宋佩璋.md "wikilink")（？—1967年8月）
  - [潘启琦](../Page/潘启琦.md "wikilink")（？—？）
  - [于永贤](../Page/于永贤.md "wikilink")（？—？）
  - [尹忠尉](../Page/尹忠尉.md "wikilink")（1970年12月—？）
  - 赵金来（1970年12月—1975年10月）
  - [李天茂](../Page/李天茂.md "wikilink")（？—？）
  - [平昌喜](../Page/平昌喜.md "wikilink")（1975年10月—1983年5月）
  - [刘新增](../Page/刘新增.md "wikilink")（？—1983年5月）
  - [马魁鸾](../Page/马魁鸾.md "wikilink")（？—？）
  - [史水洲](../Page/史水洲.md "wikilink")（1983年—1985年）\[24\]

<!-- end list -->

  - 中国人民解放军第十二军参谋长

<!-- end list -->

  - 肖永银（1949年2月—？，副军长兼）
  - [王蕴瑞](../Page/王蕴瑞.md "wikilink")（？—？）
  - 李德生（1952年9月—1955年9月，先后为副军长、第一副军长兼）
  - 阮贤榜 <small>少将</small>（？—？）
  - 娄学政 <small>大校</small>（1964年6月—1967年8月）
  - [林有声](../Page/林有声.md "wikilink")（？—？）
  - 任保裕（？—？）
  - [陆俊义](../Page/陆俊义.md "wikilink")（1970年8月—1976年）
  - 甄申（？—？）
  - [方刚](../Page/方刚.md "wikilink")（？—？）
  - [平涛](../Page/平涛.md "wikilink")（？—？）
  - [刘伦贤](../Page/刘伦贤.md "wikilink")（1983年5月—1985年8月）

<!-- end list -->

  - 中国人民解放军第十二军副参谋长

<!-- end list -->

  - [王毓淮](../Page/王毓淮.md "wikilink")（1949年2月—1950年）
  - 贺光华（1951年—1953年）
  - [宗书阁](../Page/宗书阁.md "wikilink")（？—？）
  - [唐永舜](../Page/唐永舜.md "wikilink")（？—？）
  - [王文科](../Page/王文科.md "wikilink")（？—？）
  - 林有声（？—？）
  - [张秀伦](../Page/张秀伦.md "wikilink")（？—？）
  - 李全贵（？—？）
  - 赵金来（1970年3月—1970年12月）
  - 方刚（？—？）
  - 权银刚（1970年9月—？）
  - [姜智敏](../Page/姜智敏.md "wikilink")（？—？）
  - 平涛（？—？）
  - [韩荣庆](../Page/韩荣庆.md "wikilink")（？—？）
  - [杨宪洲](../Page/杨宪洲.md "wikilink")（？—？）
  - [吕全金](../Page/吕全金.md "wikilink")（？—？）
  - [李水保](../Page/李水保.md "wikilink")（？—？）
  - [胡修道](../Page/胡修道.md "wikilink")（？—？）

<!-- end list -->

  - 中国人民解放军第十二军政治部主任

<!-- end list -->

  - [李开湘](../Page/李开湘.md "wikilink")（1949年2月—？）
  - 李震（？—？）
  - 史景班（？—？）
  - 王翀（？—？）
  - [宋佩璋](../Page/宋佩璋.md "wikilink")（？—？）
  - [李宝奇](../Page/李宝奇.md "wikilink")（？—？）
  - [潘启琦](../Page/潘启琦.md "wikilink")（？—？）
  - [李天茂](../Page/李天茂.md "wikilink")（？—？）
  - 马魁鸾（？—？）
  - 史水洲（？—1978年）\[25\]
  - [李连生](../Page/李连生.md "wikilink")（1979年4月—1983年）
  - [温宗仁](../Page/温宗仁.md "wikilink")（1983年5月—1985年8月）

<!-- end list -->

  - 中国人民解放军第十二军政治部副主任

<!-- end list -->

  - [唐平铸](../Page/唐平铸.md "wikilink")（1949年2月—？）
  - [刘昌](../Page/刘昌_\(开国大校\).md "wikilink")（？—？）
  - [钟良树](../Page/钟良树.md "wikilink")（？—1969年1月）
  - [寿文魁](../Page/寿文魁.md "wikilink")（1962年—1964年）
  - [于永贤](../Page/于永贤.md "wikilink")（？—？）
  - [李天茂](../Page/李天茂.md "wikilink")（？—？）
  - [刘新增](../Page/刘新增.md "wikilink")（？—？）
  - [张友复](../Page/张友复.md "wikilink")（？—？）
  - [郝一针](../Page/郝一针.md "wikilink")（？—？）
  - [韩笑](../Page/韩笑.md "wikilink")（？—？）
  - [陈用贤](../Page/陈用贤.md "wikilink")（？—？）

<!-- end list -->

  - 中国人民解放军第十二军后勤部部长

<!-- end list -->

  - 王耀显（1949年2月—？，供给部部长）
  - 詹少联（1949年2月—？，卫生部部长）
  - 宗书阁（？—？）
  - [张志](../Page/张志.md "wikilink")（？—？，后勤部政治委员兼）
  - [聂国先](../Page/聂国先.md "wikilink")（？—？）
  - [阎作惠](../Page/阎作惠.md "wikilink")（？—？）
  - [韩荣庆](../Page/韩荣庆.md "wikilink")（？—？）
  - [吴斗泉](../Page/吴斗泉.md "wikilink")（？—？）
  - [张九鼎](../Page/张九鼎.md "wikilink")（？—？）
  - [王祥清](../Page/王祥清.md "wikilink")（？—？）

<!-- end list -->

  - 中国人民解放军第十二军后勤部政治委员

<!-- end list -->

  - 张志（？—？）

……

### 中国人民解放军陆军第十二集团军

  - 中国人民解放军陆军第十二集团军军长

<!-- end list -->

1.  郭锡章 <small>少将</small>（1985年8月—1990年4月）
2.  [陈希滔](../Page/陈希滔.md "wikilink") <small>少将</small>（1990年4月—1996年1月）
3.  [徐承云](../Page/徐承云.md "wikilink") <small>少将</small>（1996年1月—2002年1月）
4.  [戚建国](../Page/戚建国.md "wikilink") <small>少将</small>（2002年1月—2005年5月）
5.  [王教成](../Page/王教成.md "wikilink") <small>少将</small>（2005年7月—2007年12月）
6.  [韩卫国](../Page/韩卫国.md "wikilink")
    <small>少将</small>（2008年2月—2013年12月）\[26\]
7.  [王春宁](../Page/王春宁.md "wikilink")
    <small>少将</small>（2014年1月—2016年8月）\[27\]\[28\]

<!-- end list -->

  - 中国人民解放军陆军第十二集团军副军长

<!-- end list -->

  - [徐文义](../Page/徐文义.md "wikilink") <small>少将</small>（1985年8月—1990年6月）
  - [陈炳德](../Page/陈炳德.md "wikilink") <small>少将</small>（1985年8月—1990年6月）
  - [王文惠](../Page/王文惠.md "wikilink") <small>少将</small>（1990年6月—1995年12月）
  - [徐承云](../Page/徐承云.md "wikilink") <small>少将</small>（1994年3月—1996年1月）
  - [郝敬民](../Page/郝敬民.md "wikilink") <small>少将</small>（1996年7月—2002年3月）
  - [王永怀](../Page/王永怀.md "wikilink") <small>少将</small>（？—？）
  - [刘华建](../Page/刘华建.md "wikilink") <small>少将</small>（？—2003年7月）
  - [傅怡](../Page/傅怡.md "wikilink")
    <small>少将</small>（2004年6月—2009年10月）\[29\]
  - [黄成林](../Page/黄成林.md "wikilink") <small>少将</small>（2005年—？）
  - [杨茂明](../Page/杨茂明.md "wikilink") <small>少将</small>（2005年12月—？）\[30\]
  - [阎圣](../Page/阎圣.md "wikilink") <small>少将</small>（2007年—2010年）
  - [巴本强](../Page/巴本强.md "wikilink") <small>少将</small>（2008年—？）\[31\]
  - [兰政](../Page/兰政.md "wikilink") <small>少将</small>（2009年10月—？）
  - [倪文鑫](../Page/倪文鑫.md "wikilink")
    <small>少将</small>（2013年—2016年）\[32\]
  - [李广泉](../Page/李广泉.md "wikilink")
    <small>少将</small>（2014年—2017年）\[33\]\[34\]\[35\]
  - [邝铭](../Page/邝铭.md "wikilink")
    <small>少将</small>（2016年—2017年）\[36\]\[37\]

<!-- end list -->

  - 中国人民解放军陆军第十二集团军政治委员

<!-- end list -->

1.  温宗仁 <small>少将</small>（1985年8月—1994年10月）
2.  [潘瑞吉](../Page/潘瑞吉.md "wikilink")
    <small>少将</small>（1994年10月—2000年12月）
3.  [王健](../Page/王健_\(中将\).md "wikilink")
    <small>少将</small>（2000年12月—2005年7月）
4.  [苗华](../Page/苗华.md "wikilink")
    <small>少将</small>（2005年7月—2010年12月）\[38\]
5.  [白吕](../Page/白吕.md "wikilink") <small>少将</small>（2011年2月—2013年1月）
6.  [张学杰](../Page/张学杰.md "wikilink") <small>少将</small>（2013年1月—2014年8月）
7.  [周皖柱](../Page/周皖柱.md "wikilink")
    <small>少将</small>（2014年8月—2017年1月）\[39\]
8.  [孟中康](../Page/孟中康.md "wikilink")
    <small>少将</small>（2017年1月—4月）\[40\]\[41\]\[42\]

<!-- end list -->

  - 中国人民解放军陆军第十二集团军副政治委员

<!-- end list -->

  - [史水洲](../Page/史水洲.md "wikilink")
    <small>少将</small>（1985年—1990年）\[43\]
  - [胡道仁](../Page/胡道仁.md "wikilink") <small>少将</small>（1989年7月—1992年4月）
  - [王传友](../Page/王传友.md "wikilink") <small>少将</small>（1992年—1994年3月）
  - [戴长友](../Page/戴长友.md "wikilink") <small>少将</small>（？—？）
  - [魏亮](../Page/魏亮.md "wikilink") <small>少将</small>（2002年3月—2004年6月）
  - [陶正明](../Page/陶正明.md "wikilink") <small>少将</small>（2004年5月—2005年7月）
  - [李笃信](../Page/李笃信.md "wikilink") <small>少将</small>（2005年8月—2006年10月）
  - [周明贵](../Page/周明贵.md "wikilink")
    <small>少将</small>（2006年10月—2010年6月）\[44\]
  - [凌希](../Page/凌希.md "wikilink") <small>少将</small>（2010年7月—2015年3月）
  - [羊敏君](../Page/羊敏君.md "wikilink")
    <small>少将</small>（2015年3月—2017年）\[45\]\[46\]

<!-- end list -->

  - 中国人民解放军陆军第十二集团军参谋长

<!-- end list -->

1.  陈炳德 <small>少将</small>（1985年8月—1990年6月，副军长兼）
2.  [季崇武](../Page/季崇武.md "wikilink") <small>少将</small>（？—？）
3.  [吴继科](../Page/吴继科.md "wikilink") <small>少将</small>（？—？）
4.  [张中华](../Page/张中华.md "wikilink") <small>少将</small>（1995年9月—1999年4月）
5.  刘华建 <small>少将</small>（？—？）
6.  [彭水根](../Page/彭水根.md "wikilink")
    <small>少将</small>（2004年—2006年8月）\[47\]
7.  [马成效](../Page/马成效.md "wikilink")
    <small>少将</small>（2008年5月—2011年2月）\[48\]
8.  [倪文鑫](../Page/倪文鑫.md "wikilink")
    <small>少将</small>（2011年—2011年12月）\[49\]
9.  [黎火辉](../Page/黎火辉.md "wikilink") <small>少将</small>（2011年12月—2015年3月）
10. [郑敏](../Page/郑敏.md "wikilink") <small>少将</small>（2015年3月—12月）\[50\]
11. [孔军](../Page/孔军.md "wikilink")
    <small>少将</small>（2015年12月—2017年）\[51\]\[52\]

<!-- end list -->

  - 中国人民解放军陆军第十二集团军副参谋长

<!-- end list -->

  - [施晶](../Page/施晶.md "wikilink")（？—？）
  - 季崇武（？—？）
  - [徐盘荣](../Page/徐盘荣.md "wikilink")（？—？）
  - [张秀才](../Page/张秀才.md "wikilink")（？—？）
  - [任明发](../Page/任明发.md "wikilink")（？—？）

……

  - [宗晨阳](../Page/宗晨阳.md "wikilink")（？—2001年2月）

……

  - [倪海峰](../Page/倪海峰.md "wikilink")（？—2009年）
  - [徐建会](../Page/徐建会.md "wikilink")（？—？）
  - [戴元安](../Page/戴元安.md "wikilink")（？—？）
  - 孔军（？—2015年12月）\[53\]
  - [王鹏](../Page/王鹏.md "wikilink")（？—？）\[54\]
  - [汤辛](../Page/汤辛.md "wikilink")（2016年—2017年）\[55\]

<!-- end list -->

  - 中国人民解放军陆军第十二集团军政治部主任

<!-- end list -->

1.  [张宝康](../Page/张宝康.md "wikilink") <small>少将</small>（1985年8月—1988年6月）
2.  [胡道仁](../Page/胡道仁.md "wikilink") <small>少将</small>（1988年6月—1989年7月）
3.  [张玉江](../Page/张玉江.md "wikilink") <small>少将</small>（？—？）
4.  [陆凤彬](../Page/陆凤彬.md "wikilink") <small>少将</small>（1990年6月—1994年3月）
5.  [高武生](../Page/高武生.md "wikilink") <small>少将</small>（1994年3月—1996年7月）
6.  [魏亮](../Page/魏亮.md "wikilink") <small>少将</small>（？—2002年3月）
7.  [朱福熙](../Page/朱福熙.md "wikilink") <small>少将</small>（2002年—2003年）
8.  陶正明 <small>少将</small>（2003年6月—2004年5月）
9.  [李笃信](../Page/李笃信.md "wikilink") <small>少将</small>（2004年6月—2005年8月）
10. [周明贵](../Page/周明贵.md "wikilink") <small>少将</small>（2005年8月—2006年10月）
11. [王新海](../Page/王新海.md "wikilink") <small>少将</small>（2006年—2008年）
12. [白吕](../Page/白吕.md "wikilink")
    <small>少将</small>（2008年4月—2011年2月）\[56\]
13. [杨笑祥](../Page/杨笑祥.md "wikilink")
    <small>少将</small>（2011年2月—2015年3月）\[57\]
14. [李军](../Page/李军_\(少将\).md "wikilink")
    <small>少将</small>（2015年3月—2017年）\[58\]\[59\]\[60\]

<!-- end list -->

  - 中国人民解放军陆军第十二集团军政治部副主任

<!-- end list -->

  - [王福如](../Page/王福如.md "wikilink")（？—？）
  - [李昌林](../Page/李昌林.md "wikilink")（？—？）
  - [潘瑞吉](../Page/潘瑞吉.md "wikilink")（？—？）
  - [孙长和](../Page/孙长和.md "wikilink")（？—？）
  - [季林元](../Page/季林元.md "wikilink")（？—？）
  - [杨国华](../Page/杨国华.md "wikilink")（？—？）

……

  - [吕先景](../Page/吕先景.md "wikilink")（？—2010年12月）
  - [李少](../Page/李少.md "wikilink")（？—2015年）\[61\]
  - [秦树桐](../Page/秦树桐.md "wikilink")（？—2013年）\[62\]
  - [戴学志](../Page/戴学志.md "wikilink")（2012年—？）\[63\]
  - [吴晓荣](../Page/吴晓荣.md "wikilink")（？—2017年）\[64\]

<!-- end list -->

  - 中国人民解放军陆军第十二集团军后勤部部长

<!-- end list -->

  - [李义生](../Page/李义生.md "wikilink")（？—？）
  - [狄玉增](../Page/狄玉增.md "wikilink")（？—？）

……

  - [顾正军](../Page/顾正军.md "wikilink")（？—？）
  - 倪文鑫（？—2010年）\[65\]
  - [王业明](../Page/王业明.md "wikilink")（？—2017年）\[66\]\[67\]

<!-- end list -->

  - 中国人民解放军陆军第十二集团军装备部部长

<!-- end list -->

  - [张良富](../Page/张良富.md "wikilink")（？—？，装备技术部部长）

……

  - [谢德志](../Page/谢德志.md "wikilink")（？—？）
  - [尹健](../Page/尹健.md "wikilink")（？—？）\[68\]
  - 李广泉（？—2014年）
  - 汤辛（？—2016年）\[69\]

<!-- end list -->

  - 中国人民解放军陆军第十二集团军纪律检查委员会书记

<!-- end list -->

  - 史水洲（1985年—1990年，副政治委员兼）\[70\]

……

### 中国人民解放军陆军第七十一集团军

  - 中国人民解放军陆军第七十一集团军军长

<!-- end list -->

1.  [王印芳](../Page/王印芳.md "wikilink")
    <small>少将</small>（2017年3月—2018年1月）\[71\]
2.  [李中林](../Page/李中林.md "wikilink") <small>少将</small>（2018年1月—）\[72\]

<!-- end list -->

  - 中国人民解放军陆军第七十一集团军副军长

<!-- end list -->

  - [李志忠](../Page/李志忠_\(少将\).md "wikilink")
    <small>少将</small>（2017年—2018年6月）\[73\]

<!-- end list -->

  - 中国人民解放军陆军第七十一集团军政治委员

<!-- end list -->

1.  [徐德清](../Page/徐德清.md "wikilink")
    <small>少将</small>（2017年3月—2018年5月）\[74\]
2.  [尹红星](../Page/尹红星.md "wikilink") <small>少将</small>（2018年5月—）\[75\]

<!-- end list -->

  - 中国人民解放军陆军第七十一集团军副政治委员

<!-- end list -->

1.  [李军](../Page/李军.md "wikilink")
    <small>少将</small>（2017年3月—2018年6月）\[76\]

<!-- end list -->

  - 中国人民解放军陆军第七十一集团军参谋长

<!-- end list -->

  - [段应民](../Page/段应民.md "wikilink")（2017年—）\[77\]

<!-- end list -->

  - 中国人民解放军陆军第七十一集团军副参谋长

<!-- end list -->

  - 中国人民解放军陆军第七十一集团军政治工作部主任

<!-- end list -->

  - [张晓](../Page/张晓_\(解放军\).md "wikilink")（2017年—）\[78\]

<!-- end list -->

  - 中国人民解放军陆军第七十一集团军政治工作部副主任

<!-- end list -->

  - 中国人民解放军陆军第七十一集团军保障部部长

<!-- end list -->

  - 中国人民解放军陆军第七十一集团军保障部副部长

<!-- end list -->

  - [王世乐](../Page/王世乐.md "wikilink") <small>大校</small>（2017年—）\[79\]

<!-- end list -->

  - 中国人民解放军陆军第七十一集团军纪律检查委员会书记

<!-- end list -->

  - 中国人民解放军陆军第七十一集团军纪律检查委员会副书记

## 荣誉

曾被授予荣誉称号的单位有：

  - **[临汾旅](../Page/临汾旅.md "wikilink")**：原陆军第十二集团军步兵第一七九旅。2017年转隶陆军第七十一集团军\[80\]
  - **红军团**：原陆军第十二集团军步兵第三十六师第一〇六团
  - **[朱德警卫团](../Page/朱德.md "wikilink")**：原陆军第十二集团军步兵第三十六师第一〇八团，后为陆军第十二军直属工兵团。2017年转隶陆军第七十一集团军\[81\]
  - **百将团**：原陆军第十二集团军步兵第三十六师第一〇六团
  - **尊干爱兵模范连**：原陆军第十二集团军步兵第三十四旅第三营第七连
  - **[王杰班](../Page/王杰_\(解放军\).md "wikilink")**：原陆军第十二集团军装甲第二师工兵營第一连第五班。2017年转隶陆军第七十一集团军\[82\]\[83\]
  - **济南第一团**：2017年转隶陆军第七十一集团军\[84\]
  - **模范尖刀连**：2017年转隶陆军第七十一集团军\[85\]
  - **草地党支部**：2017年转隶陆军第七十一集团军\[86\]

## 参考文献

## 参见

  - [中国人民解放军东部战区陆军](../Page/中国人民解放军东部战区陆军.md "wikilink")
  - [中国人民解放军南京军区](../Page/中国人民解放军南京军区.md "wikilink")

{{-}}

[Category:中国人民解放军驻徐州单位](../Category/中国人民解放军驻徐州单位.md "wikilink")
[中国人民解放军陆军第七十一集团军](../Category/中国人民解放军陆军第七十一集团军.md "wikilink")

1.

2.
3.
4.
5.

6.

7.

8.

9.

10.

11.
12.

13.

14.

15.

16.

17.

18.

19.

20.

21.

22.
23.

24.
25.
26.
27.

28.

29.

30.
31.
32.
33.
34.
35.
36.
37.

38.
39.
40.

41.

42.

43.
44.
45.

46.
47.

48.
49.

50.
51.
52.
53.
54.

55.
56.
57.
58.
59.
60.
61.

62.

63.

64.

65.
66.
67.
68.
69.
70.

71.

72.

73.

74.

75.

76.

77.

78.

79.

80.

81.
82.
83.

84.
85.
86.