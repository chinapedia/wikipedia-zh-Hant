《**中国人的精神**》（），又名《**春秋大义**》，是[国学大师](../Page/国学.md "wikilink")[辜鸿铭的一部英文作品](../Page/辜鸿铭.md "wikilink")。该书由一系列关于中国人精神论文集结而成，文中阐释中国人的精神生活，宣扬[中国传统文化的价值](../Page/中国传统文化.md "wikilink")，并主张用[儒家思想对西方社会进行改造](../Page/儒家.md "wikilink")。

## 版本

  - 1914年，该系列论文发表于[英文](../Page/英文.md "wikilink")[报纸](../Page/报纸.md "wikilink")《[中国评论](../Page/中国评论.md "wikilink")》
  - 1915年，[北京每日新闻社首版](../Page/北京每日新闻社.md "wikilink")，其中文题名为《[春秋大义](../Page/春秋大义.md "wikilink")》
  - 1916年，出版[德文版](../Page/德文.md "wikilink")，译者奥卡·A.H.[施密茨](../Page/施密茨.md "wikilink")
  - 1917年（？），出版[法文版](../Page/法文.md "wikilink")
  - 1922年，[商务印书馆再版](../Page/商务印书馆.md "wikilink")
  - 1941年，出版日文版，译者[鱼返善雄](../Page/鱼返善雄.md "wikilink")
  - 1987年，[黄兴涛与](../Page/黄兴涛.md "wikilink")[宋小庆合译](../Page/宋小庆.md "wikilink")[中文版](../Page/中文.md "wikilink")
  - 1999年，[外语教学与研究出版社重版](../Page/外语教学与研究出版社.md "wikilink")（英文版）

## 主要内容

该书分为序言，导论，正文，附录四大部分。

### 序言

通过与[美国人](../Page/美国人.md "wikilink")、[英国人](../Page/英国人.md "wikilink")、[德国人](../Page/德国人.md "wikilink")、[法国人对比](../Page/法国人.md "wikilink")，指出[中国人深刻](../Page/中国人.md "wikilink")（depth），博大（broadness）、简朴（simplicity）并具有灵性（delicacy）。

| 国民性                              | 深刻（depth） | 博大（broadness） | 简朴（simplicity） | 灵性（delicacy） |
| -------------------------------- | --------- | ------------- | -------------- | ------------ |
| [美国人](../Page/美国人.md "wikilink") | **缺乏**    | 有             | 有              | 缺乏           |
| [英国人](../Page/英国人.md "wikilink") | 有         | **缺乏**        | 有              | 缺乏           |
| [德国人](../Page/德国人.md "wikilink") | 有         | 有             | **缺乏**         | 缺乏           |
| [法国人](../Page/法国人.md "wikilink") | 有但不如德国人   | 有但不如美国人       | 有但不如英国人        | **有**        |
| [中国人](../Page/中国人.md "wikilink") | 有         | 有             | 有              | 有            |

### 导论

善良公民的信仰（Introduction: The Religion of Good-citizenship）

### 正文

  - 第一章 中国人的精神（The Spirit of the Chinese People）
  - 第二章 中国妇女（The Chinese Woman）
  - 第三章 中国语言（The Chinese Language）
  - 第四章 约翰·斯密斯在中国（John Smith in China）
  - 第五章 一位著名[汉学家](../Page/汉学.md "wikilink")（A Great Sinologue）
  - 第六章 汉学，第一部分（Chinese Scholarship, Part I）
  - 第七章 汉学，第二部分（Chinese Scholarship, Part II）

### 附录

"崇拜群众的宗教"或名"战争与出路"（Appendix: The Religion of Mob-Worship or The War and
the Way out）

## 参考文献

## 外部链接

  - [华夏文化
    辜鸿铭：中国人的精神](https://web.archive.org/web/20061107173429/http://gb.chinabroadcast.cn/3601/2004/09/16/342@301802.htm)
  - [辜鸿铭的“春秋大义”](http://www.guoxue.com/master/guhongming/ghm005.htm)
  - [陶赟](../Page/陶赟.md "wikilink"):
    [《辜鸿铭《中国人的精神》导读》](http://www.taoyunhome.org/2017/03/-7.html)

## 參見

  - [春秋大義](../Page/春秋大義.md "wikilink")

[Category:1915年書籍](../Category/1915年書籍.md "wikilink")