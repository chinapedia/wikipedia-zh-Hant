**中国佛教协会**，是[中华人民共和国成立后的](../Page/中华人民共和国.md "wikilink")[中国佛教组织](../Page/中国佛教.md "wikilink")，1953年初成立。

## 历史

1952年11月4日至5日，中国佛教协会发起人会议在北京举行。此次会议研究、讨论中国佛教协会的宗旨、任务、组织，并通过《中国佛教协会发起书》。而会议发起人有[虚云](../Page/释虚云.md "wikilink")、[喜饶嘉措](../Page/喜饶嘉措.md "wikilink")、[噶喇藏](../Page/噶喇藏.md "wikilink")、[圆瑛](../Page/釋圓瑛.md "wikilink")、[柳霞·土登塔巴](../Page/柳霞·土登塔巴.md "wikilink")、[丹巴日杰](../Page/丹巴日杰.md "wikilink")、[罗桑巴桑](../Page/罗桑巴桑.md "wikilink")、[多吉占东](../Page/多吉占东.md "wikilink")、[能海](../Page/能海.md "wikilink")、[法尊](../Page/法尊.md "wikilink")、[巨赞](../Page/巨赞.md "wikilink")、[陈铭枢](../Page/陈铭枢.md "wikilink")、[吕澂](../Page/吕澂.md "wikilink")、[赵朴初](../Page/趙樸初.md "wikilink")、[董鲁安](../Page/董鲁安.md "wikilink")、[叶恭绰](../Page/叶恭绰.md "wikilink")、[林宰平](../Page/林宰平.md "wikilink")、[向达](../Page/向达.md "wikilink")、[周叔迦](../Page/周叔迦.md "wikilink")、[郭朋等](../Page/郭朋.md "wikilink")。最终会议推举组成中国佛教协会筹备处，赵朴初任主任。\[1\]

1953年5月30日，在[北京广济寺举办中国佛教协会成立会议](../Page/广济寺_\(北京市\).md "wikilink")。121位[活佛](../Page/活佛.md "wikilink")、[喇嘛](../Page/喇嘛.md "wikilink")、[法师](../Page/法师.md "wikilink")、[居士参加大会](../Page/居士.md "wikilink")，他们来自[汉](../Page/汉族.md "wikilink")、[藏](../Page/藏族.md "wikilink")、[蒙](../Page/蒙古族.md "wikilink")、[傣](../Page/傣族.md "wikilink")、[满](../Page/满族.md "wikilink")、[苗](../Page/苗族.md "wikilink")、撒里维吾尔（[裕固](../Page/裕固族.md "wikilink")）等7个民族。喜饶嘉措致开幕词，赵朴初报告中国佛教协会成立、发起经过及筹备工作。\[2\]

## 历任会长

1.  [圆瑛](../Page/釋圓瑛.md "wikilink") [法师](../Page/法师.md "wikilink") 第一届
    1953年－1953年9月
2.  [喜饶嘉措](../Page/喜饶嘉措.md "wikilink") [活佛](../Page/活佛.md "wikilink")
    第二至三届
    1957年－[文化大革命期间](../Page/文化大革命.md "wikilink")，中国佛协会工作被中断，领导人遭迫害。
3.  [趙樸初](../Page/趙樸初.md "wikilink") [居士](../Page/居士.md "wikilink")
    第四至六届 1980年－2000年5月
4.  [一诚](../Page/一诚.md "wikilink") 法师 第七届 2002年－ 2010年
5.  [传印](../Page/传印.md "wikilink") 法师 第八届 2010年－ 2015年
6.  [学诚](../Page/学诚.md "wikilink") 法师 第九届 2015年 - 2018年8月15日\[3\]
7.  [演觉](../Page/演觉.md "wikilink")（代理\[4\]2018年8月15日-)

## 历届领导人

### 第一届（1953年－1957年）

  - 名誉会长：[逹赖喇嘛](../Page/十四世达赖.md "wikilink")、[班禅额尔德尼·却吉坚赞](../Page/班禅额尔德尼·却吉坚赞.md "wikilink")、[虚云法师](../Page/虚云法师.md "wikilink")、[查干葛根](../Page/查干葛根.md "wikilink")
  - 会长：[圆瑛](../Page/圆瑛.md "wikilink")
  - 副会长：[喜饶嘉措](../Page/喜饶嘉措.md "wikilink")、[公德林·晋美吉村](../Page/公德林·晋美吉村.md "wikilink")、[能海](../Page/能海.md "wikilink")、[赵朴初](../Page/赵朴初.md "wikilink")、[噶喇藏](../Page/噶喇藏.md "wikilink")、[祜巴](../Page/松溜·阿嘎牟尼.md "wikilink")、[阿旺嘉措](../Page/阿旺嘉措.md "wikilink")
  - 秘书长：赵朴初（兼）
  - 副秘书长：[巨赞](../Page/巨赞.md "wikilink")、[周叔迦](../Page/周叔迦.md "wikilink")、[郭朋](../Page/郭朋.md "wikilink")\[5\]

### 第二届（1957年－1962年）

  - 会长：[喜饶嘉措](../Page/喜饶嘉措.md "wikilink")
  - 副会长：[噶登赛持](../Page/赛赤·罗桑丹贝坚赞.md "wikilink")、[应慈](../Page/应慈.md "wikilink")、[静权](../Page/静权.md "wikilink")、[松榴·阿戛木尼亚](../Page/松榴·阿戛木尼亚.md "wikilink")（景洪）、[能海](../Page/能海.md "wikilink")、[赵朴初](../Page/赵朴初.md "wikilink")
  - 秘书长：赵朴初（兼）
  - 副秘书长：[巨赞](../Page/巨赞.md "wikilink")（兼）、[周叔迦](../Page/周叔迦.md "wikilink")（兼）、[郭朋](../Page/郭朋.md "wikilink")、[义方](../Page/义方.md "wikilink")、[李一平](../Page/李一平.md "wikilink")\[6\]

### 第三届（1962年－1980年）

  - 名誉会长：[班禅额尔德尼·却吉坚赞](../Page/班禅额尔德尼·却吉坚赞.md "wikilink")、[应慈](../Page/应慈.md "wikilink")
  - 会长：[喜饶嘉措](../Page/喜饶嘉措.md "wikilink")
  - 副会长：[阿旺嘉措](../Page/阿旺嘉措.md "wikilink")、[噶丹赤巴·土登贡嘎](../Page/噶丹赤巴·土登贡嘎.md "wikilink")、[赵朴初](../Page/赵朴初.md "wikilink")、[能海](../Page/能海.md "wikilink")、[松留·阿嘎牟尼](../Page/松留·阿嘎牟尼.md "wikilink")（景洪）、[噶喇藏](../Page/噶喇藏.md "wikilink")、[巨赞](../Page/巨赞.md "wikilink")、[周叔迦](../Page/周叔迦.md "wikilink")、[悟古纳](../Page/悟古纳.md "wikilink")、[嘉木样](../Page/嘉木样·洛桑久美·图丹却吉尼玛.md "wikilink")
  - 秘书长：赵朴初（兼）
  - 副秘书长：[巨赞](../Page/巨赞.md "wikilink")、[周叔迦](../Page/周叔迦.md "wikilink")、[石鸣珂](../Page/石鸣珂.md "wikilink")、[明真](../Page/明真.md "wikilink")、[正果](../Page/正果.md "wikilink")、[逝波](../Page/逝波.md "wikilink")、[隆莲](../Page/隆莲.md "wikilink")\[7\]

### 第四届（1980年－1987年）

  - 名誉会长：[班禅额尔德尼·却吉坚赞](../Page/班禅额尔德尼·却吉坚赞.md "wikilink")
  - 会长：[赵朴初](../Page/赵朴初.md "wikilink")
  - 副会长：[帕巴拉·格列朗杰](../Page/帕巴拉·格列朗杰.md "wikilink")（藏族）、[坚白赤列](../Page/坚白赤列.md "wikilink")、[嘉木样·洛桑久美·图丹却吉尼玛](../Page/嘉木样·洛桑久美·图丹却吉尼玛.md "wikilink")、[夏茸尕布](../Page/夏茸尕布·洛桑隆仁丹贝嘉措.md "wikilink")、[巨赞](../Page/巨赞.md "wikilink")、[正果](../Page/正果.md "wikilink")、[明真](../Page/明真.md "wikilink")、[宫明·姜巴曲日木](../Page/宫明·姜巴曲日木.md "wikilink")
  - 秘书长：赵朴初（暂兼）
  - 副秘书长：巨赞、正果、明真、[隆莲](../Page/隆莲.md "wikilink")、[逝波](../Page/逝波.md "wikilink")、[李荣熙](../Page/李荣熙.md "wikilink")\[8\]

### 第五届理事会（1987年－1993年）

  - 名誉会长：[班禅额尔德尼·却吉坚赞](../Page/班禅额尔德尼·却吉坚赞.md "wikilink")
  - 会长：[赵朴初](../Page/赵朴初.md "wikilink")
  - 副会长：[帕巴拉·格列朗杰](../Page/帕巴拉·格列朗杰.md "wikilink")（藏族）、[嘉木样·洛桑久美·图丹却吉尼玛](../Page/嘉木样·洛桑久美·图丹却吉尼玛.md "wikilink")、[夏茸尕布](../Page/夏茸尕布·洛桑隆仁丹贝嘉措.md "wikilink")、[正果](../Page/正果.md "wikilink")、[明真](../Page/明真.md "wikilink")、[宫明·姜巴曲日木](../Page/宫明·姜巴曲日木.md "wikilink")、[李荣熙](../Page/李荣熙.md "wikilink")、[色结堪苏·伦珠陶凯](../Page/色结堪苏·伦珠陶凯.md "wikilink")、[贡唐仓](../Page/贡唐仓·丹贝旺旭.md "wikilink")、[嘉雅](../Page/嘉雅·洛桑丹白尖赞.md "wikilink")、[乌兰](../Page/乌兰·嘎拉桑凯日布丹毕尼玛.md "wikilink")、[圆拙](../Page/圆拙.md "wikilink")、[隆莲](../Page/隆莲.md "wikilink")、[刀述仁](../Page/刀述仁.md "wikilink")、[周绍良](../Page/周绍良.md "wikilink")、[多吉扎·江白洛桑](../Page/多吉扎·江白洛桑.md "wikilink")（藏族）
  - 秘书长：周绍良（兼）
  - 副秘书长：刀述仁（兼）、[游骧](../Page/游骧.md "wikilink")\[9\]

### 第六届理事会（1993年－2002年）

  - 会长：[赵朴初](../Page/赵朴初.md "wikilink")
  - 副会长：[帕巴拉·格列朗杰](../Page/帕巴拉·格列朗杰.md "wikilink")（藏族）、[嘉木样·洛桑久美·图丹却吉尼玛](../Page/嘉木样·洛桑久美·图丹却吉尼玛.md "wikilink")（藏族）、[李荣熙](../Page/李荣熙.md "wikilink")、[贡唐仓·丹贝旺旭](../Page/贡唐仓·丹贝旺旭.md "wikilink")（藏族）、[乌兰](../Page/乌兰·嘎拉桑凯日布丹毕尼玛.md "wikilink")、[圆拙](../Page/圆拙.md "wikilink")、[隆莲](../Page/隆莲.md "wikilink")（女）、[刀述仁](../Page/刀述仁.md "wikilink")（傣）、[周绍良](../Page/周绍良.md "wikilink")、[多吉扎·江白洛桑](../Page/多吉扎·江白洛桑.md "wikilink")（藏族）、[明旸](../Page/明旸.md "wikilink")、[伍并亚·温撒](../Page/伍并亚·温撒.md "wikilink")（傣）、[布米·强巴洛卓](../Page/布米·强巴洛卓.md "wikilink")（藏）、[真禅](../Page/真禅.md "wikilink")、[茗山](../Page/茗山.md "wikilink")、[却西](../Page/却西.md "wikilink")（藏）、[阿嘉·洛桑图丹久美嘉措](../Page/阿嘉·洛桑图丹久美嘉措.md "wikilink")（蒙）、[净慧](../Page/净慧.md "wikilink")、[策墨林·丹增赤烈](../Page/策墨林·丹增赤烈.md "wikilink")（藏）、[一诚](../Page/一诚.md "wikilink")、[圣辉](../Page/圣辉.md "wikilink")、[香根·巴登多吉](../Page/香根·巴登多吉.md "wikilink")（藏族）
  - 秘书长：刀述仁（傣）（兼）
  - 副秘书长：[游骧](../Page/游骧.md "wikilink")（留任）、[萧秉权](../Page/萧秉权.md "wikilink")（留任）、[学诚](../Page/学诚.md "wikilink")、[那仓·向巴昂翁](../Page/那仓·向巴昂翁.md "wikilink")（藏）\[10\]

<!-- end list -->

  - 中国佛教协会咨议委员会

<!-- end list -->

  - 主席：[圆拙](../Page/圆拙.md "wikilink")
  - 副主席：[本焕](../Page/本焕.md "wikilink")、[妙善](../Page/妙善.md "wikilink")、[妙湛](../Page/妙湛.md "wikilink")、[宽霖](../Page/宽霖.md "wikilink")、[清定](../Page/清定.md "wikilink")、[明开](../Page/明开.md "wikilink")、[遍能](../Page/遍能.md "wikilink")、[热旦嘉措](../Page/热旦嘉措.md "wikilink")（藏族）、[松布](../Page/松布.md "wikilink")（土）、[明学](../Page/明学.md "wikilink")、[云峰](../Page/云峰.md "wikilink")、[仁德](../Page/仁德_\(僧侶\).md "wikilink")、[佛源](../Page/佛源.md "wikilink")、[古嘉赛](../Page/古嘉赛.md "wikilink")（藏族）\[11\]

### 第七届理事会（2002年－2010年）

  - 名誉会长：[帕巴拉·格列朗杰](../Page/帕巴拉·格列朗杰.md "wikilink")（藏族）
  - 会长：[一诚](../Page/一诚.md "wikilink")
  - 副会长：[圣辉](../Page/圣辉.md "wikilink")、[嘉木样·洛桑久美·图丹却吉尼玛](../Page/嘉木样·洛桑久美·图丹却吉尼玛.md "wikilink")（藏族）、[刀述仁](../Page/刀述仁.md "wikilink")（傣族）、[净慧](../Page/净慧.md "wikilink")、[学诚](../Page/学诚.md "wikilink")、[多吉扎·江白洛桑](../Page/多吉扎·江白洛桑.md "wikilink")（藏族）、[策墨林·单增赤列](../Page/策墨林·单增赤列.md "wikilink")（藏族）、[香根·巴登多吉](../Page/香根·巴登多吉.md "wikilink")（藏族）、[明学](../Page/明学.md "wikilink")、[加羊加措](../Page/加羊加措.md "wikilink")（藏族）、[刘炳森](../Page/刘炳森.md "wikilink")、[珠康·土登克珠](../Page/珠康·土登克珠.md "wikilink")（藏族）、[加木样·图布丹](../Page/加木样·图布丹.md "wikilink")（蒙）、[根通](../Page/根通.md "wikilink")、[戒忍](../Page/戒忍.md "wikilink")、[永寿](../Page/永寿.md "wikilink")、[永信](../Page/永信.md "wikilink")、[明生](../Page/明生.md "wikilink")、[都龙庄](../Page/都龙庄.md "wikilink")（傣）、[觉醒](../Page/觉醒.md "wikilink")
  - 秘书长：[学诚](../Page/学诚.md "wikilink")
  - 副秘书长：[那仓·向巴昂翁](../Page/那仓·向巴昂翁.md "wikilink")（藏族）、[张琳](../Page/张琳.md "wikilink")、[怀善](../Page/怀善.md "wikilink")、[丛铭](../Page/丛铭.md "wikilink")、[如瑞](../Page/如瑞.md "wikilink")（女）、[蘧俊忠](../Page/蘧俊忠.md "wikilink")、[照诚](../Page/照诚.md "wikilink")
  - 特邀顾问：[季羡林](../Page/季羡林.md "wikilink")、[启功](../Page/启功.md "wikilink")、[李玉玲](../Page/李玉玲.md "wikilink")

<!-- end list -->

  - 中国佛教协会咨议委员会

<!-- end list -->

  - 主席：[本焕](../Page/本焕.md "wikilink")
  - 副主席：[乌兰](../Page/乌兰·嘎拉桑凯日布丹毕尼玛.md "wikilink")（蒙）、[隆莲](../Page/隆莲.md "wikilink")（女）、[周绍良](../Page/周绍良.md "wikilink")、[伍并亚·温撒](../Page/伍并亚·温撒.md "wikilink")（傣）、[布米·强巴洛卓](../Page/布米·强巴洛卓.md "wikilink")（藏）等17位\[12\]

### 第八届理事会（2010年－2015年）

  - 名誉会长：[帕巴拉·格列朗杰](../Page/帕巴拉·格列朗杰.md "wikilink")、[本焕](../Page/本焕.md "wikilink")（2012年4月圆寂）、[一诚](../Page/一诚.md "wikilink")　
  - 会长：[传印](../Page/传印.md "wikilink")
  - 副会长：[嘉木样·洛桑久美·图丹却吉尼玛](../Page/嘉木样·洛桑久美·图丹却吉尼玛.md "wikilink")、[刀述仁](../Page/刀述仁.md "wikilink")、[圣辉](../Page/圣辉.md "wikilink")、[净慧](../Page/净慧.md "wikilink")、[学诚](../Page/学诚.md "wikilink")、[班禅额尔德尼·确吉杰布](../Page/班禅额尔德尼·确吉杰布.md "wikilink")、[珠康·土登克珠](../Page/珠康·土登克珠.md "wikilink")、[多吉扎·江白洛桑](../Page/多吉扎·江白洛桑.md "wikilink")、[策墨林·单增赤列](../Page/策墨林·单增赤列.md "wikilink")、[香根·巴登多吉](../Page/香根·巴登多吉.md "wikilink")、[永寿](../Page/永寿.md "wikilink")、[永信](../Page/永信.md "wikilink")、[明生](../Page/明生.md "wikilink")、[祜巴龙庄勐](../Page/祜巴龙庄勐.md "wikilink")、[觉醒](../Page/觉醒.md "wikilink")、[如瑞](../Page/如瑞.md "wikilink")、[贾拉森](../Page/贾拉森.md "wikilink")、[湛如](../Page/湛如.md "wikilink")、[妙江](../Page/妙江.md "wikilink")、[心澄](../Page/心澄.md "wikilink")、[道慈](../Page/道慈.md "wikilink")、[纯一](../Page/纯一.md "wikilink")、[正慈](../Page/正慈.md "wikilink")、[印顺](../Page/释印顺_\(中国佛教协会\).md "wikilink")、[增勤](../Page/增勤.md "wikilink")
  - 秘书长：[王健](../Page/王健.md "wikilink")
  - 副秘书长：[那仓·向巴昂翁](../Page/那仓·向巴昂翁.md "wikilink")、[张琳](../Page/张琳.md "wikilink")、[怀善](../Page/怀善.md "wikilink")、[宗家顺](../Page/宗家顺.md "wikilink")、[卢浔](../Page/卢浔.md "wikilink")、[演觉](../Page/演觉.md "wikilink")、[常藏](../Page/常藏.md "wikilink")、[胡雪峰](../Page/胡雪峰_\(喇嘛\).md "wikilink")、[赛赤·确吉洛智嘉措](../Page/赛赤·确吉洛智嘉措.md "wikilink")、[都罕听](../Page/都罕听.md "wikilink")、[印乐](../Page/印乐.md "wikilink")、[慧明](../Page/慧明.md "wikilink")、[肖占军](../Page/肖占军.md "wikilink")、[慧庆](../Page/慧庆.md "wikilink")、[普法](../Page/普法.md "wikilink")、[吴国平](../Page/吴国平.md "wikilink")、[延藏](../Page/延藏.md "wikilink")
  - 特邀顾问：[李玉玲](../Page/李玉玲.md "wikilink")、[刘长乐](../Page/刘长乐.md "wikilink")、[方立天](../Page/方立天.md "wikilink")、[田青](../Page/田青.md "wikilink")

<!-- end list -->

  - 中国佛教协会咨议委员会

<!-- end list -->

  - 主席：[惟贤](../Page/惟贤.md "wikilink")
  - 副主席：[新成](../Page/新成.md "wikilink")、[慧海](../Page/慧海.md "wikilink")、[明学](../Page/明学.md "wikilink")、[加羊加措](../Page/加羊加措.md "wikilink")、[加木样·图布丹](../Page/加木样·图布丹.md "wikilink")、[根通](../Page/根通.md "wikilink")、[明哲](../Page/明哲.md "wikilink")、[海山](../Page/海山.md "wikilink")、[妙安](../Page/妙安.md "wikilink")、[松纯](../Page/松纯.md "wikilink")、[无相](../Page/无相.md "wikilink")、[可明](../Page/可明.md "wikilink")、[惟正](../Page/惟正.md "wikilink")、[界诠](../Page/界诠.md "wikilink")\[13\]

### 第九届理事会（2015年－）

  - 名誉会长：[帕巴拉·格列朗杰](../Page/帕巴拉·格列朗杰.md "wikilink")、[传印](../Page/传印.md "wikilink")、[一诚](../Page/一诚.md "wikilink")　
  - 会长：[学诚](../Page/学诚.md "wikilink")(2015-2018年8月15日\[14\])、[演觉](../Page/演觉.md "wikilink")（代理\[15\]2018年8月15日-)
  - 副会长：[嘉木样·洛桑久美·图丹却吉尼玛](../Page/嘉木样·洛桑久美·图丹却吉尼玛.md "wikilink")（藏）、[祜巴龙庄勐](../Page/祜巴龙庄勐.md "wikilink")（傣）、[班禅额尔德尼·确吉杰布](../Page/班禅额尔德尼·确吉杰布.md "wikilink")（藏）、[圣辉](../Page/圣辉.md "wikilink")、[道慈](../Page/道慈.md "wikilink")、[珠康·土登克珠](../Page/珠康·土登克珠.md "wikilink")（藏）、[策墨林·单增赤列](../Page/策墨林·单增赤列.md "wikilink")（藏）、[香根·巴登多吉](../Page/香根·巴登多吉.md "wikilink")（藏）、[永寿](../Page/释永寿.md "wikilink")、[永信](../Page/释永信.md "wikilink")、[明生](../Page/明生.md "wikilink")、[觉醒](../Page/释觉醒.md "wikilink")、[如瑞](../Page/如瑞.md "wikilink")（女）、[湛如](../Page/湛如.md "wikilink")、[妙江](../Page/妙江.md "wikilink")、[心澄](../Page/心澄.md "wikilink")、[纯一](../Page/纯一.md "wikilink")、[正慈](../Page/正慈.md "wikilink")、[印顺](../Page/释印顺_\(中国佛教协会\).md "wikilink")、[增勤](../Page/增勤.md "wikilink")、[演觉](../Page/演觉.md "wikilink")、[宗性](../Page/宗性.md "wikilink")、[仁青安杰](../Page/仁青安杰.md "wikilink")（藏）、[直孔穷仓·洛桑强巴](../Page/直孔穷仓·洛桑强巴.md "wikilink")（藏）、[赵九九](../Page/赵九九.md "wikilink")（蒙）、[普法](../Page/普法.md "wikilink")、[静波](../Page/静波.md "wikilink")、[胡雪峰](../Page/胡雪峰_\(喇嘛\).md "wikilink")（蒙）、[明海](../Page/明海.md "wikilink")、[东宝仲巴](../Page/东宝仲巴.md "wikilink")（藏）、[诏等傣](../Page/诏等傣.md "wikilink")（傣）、[则悟](../Page/则悟.md "wikilink")
  - 秘书长：[刘威](../Page/刘威.md "wikilink")
  - 副秘书长：[张琳](../Page/张琳.md "wikilink")、[卢浔](../Page/卢浔.md "wikilink")、[常藏](../Page/常藏.md "wikilink")、[赛赤·确吉洛智嘉措](../Page/赛赤·确吉洛智嘉措.md "wikilink")（藏）、[都罕听](../Page/都罕听.md "wikilink")（傣）、[印乐](../Page/印乐.md "wikilink")、[慧明](../Page/慧明.md "wikilink")、[肖占军](../Page/肖占军.md "wikilink")、[慧庆](../Page/慧庆.md "wikilink")、[吴国平](../Page/吴国平.md "wikilink")、[扎西坚才](../Page/扎西坚才.md "wikilink")（藏）、[郭莽仓](../Page/郭莽仓.md "wikilink")（藏）、[达扎·尕让托布旦拉西降措](../Page/达扎·尕让托布旦拉西降措.md "wikilink")（藏）、[宏度](../Page/宏度.md "wikilink")、[刘宇](../Page/刘宇.md "wikilink")、[张军](../Page/张军.md "wikilink")、[全柏音](../Page/全柏音.md "wikilink")（藏）、[照诚](../Page/照诚.md "wikilink")、[怡藏](../Page/怡藏.md "wikilink")、[身振](../Page/身振.md "wikilink")、[光泉](../Page/光泉.md "wikilink")、[秋爽](../Page/秋爽.md "wikilink")、[怀梵](../Page/怀梵.md "wikilink")、[纯闻](../Page/纯闻.md "wikilink")
  - 名誉理事：[杨钊](../Page/杨钊.md "wikilink")、[刘长乐](../Page/刘长乐.md "wikilink")、[楼宇烈](../Page/楼宇烈.md "wikilink")、[李玉玲](../Page/李玉玲.md "wikilink")、[田青](../Page/田青.md "wikilink")、[净因](../Page/净因.md "wikilink")

<!-- end list -->

  - 中国佛教协会咨议委员会

<!-- end list -->

  - 主席：[明学](../Page/明学.md "wikilink")
  - 副主席：[刀述仁](../Page/刀述仁.md "wikilink")、[新成](../Page/新成.md "wikilink")、[加羊加措](../Page/加羊加措.md "wikilink")（藏）、[加木样·图布丹](../Page/加木样·图布丹.md "wikilink")（蒙）、[根通](../Page/根通.md "wikilink")、[多吉扎·江白洛桑](../Page/多吉扎·江白洛桑.md "wikilink")（藏）、[梦参](../Page/梦参.md "wikilink")、[德林](../Page/德林.md "wikilink")、[妙安](../Page/妙安.md "wikilink")、[松纯](../Page/松纯.md "wikilink")、[无相](../Page/无相.md "wikilink")、[可明](../Page/可明.md "wikilink")、[界诠](../Page/界诠.md "wikilink")、[那仓·向巴昂翁](../Page/那仓·向巴昂翁.md "wikilink")（藏）、[怀善](../Page/怀善.md "wikilink")、[照元](../Page/照元.md "wikilink")、[清德](../Page/清德.md "wikilink")、[白光](../Page/白光.md "wikilink")、[道元](../Page/道元.md "wikilink")、[罗让旦巴](../Page/罗让旦巴.md "wikilink")（藏）\[16\]

<!-- end list -->

  - 中国佛教协会专门委员会

<!-- end list -->

  - 藏传佛教工作委员会
      - 主任：[嘉木样·洛桑久美·图丹却吉尼玛](../Page/嘉木样·洛桑久美·图丹却吉尼玛.md "wikilink")
      - 副主任：[珠康·土登克珠](../Page/珠康·土登克珠.md "wikilink")
        　[东宝·仲巴呼图克图](../Page/东宝·仲巴呼图克图.md "wikilink")
        　[赛赤·确吉洛智嘉措](../Page/赛赤·确吉洛智嘉措.md "wikilink")
        [达扎·尕让托布旦拉西降措](../Page/达扎·尕让托布旦拉西降措.md "wikilink")
      - 委　员：[新色·旺杰](../Page/新色·旺杰.md "wikilink")
        [土旦宁布](../Page/土旦宁布.md "wikilink")
        [扎西坚才](../Page/扎西坚才.md "wikilink")
        [达娃次仁](../Page/达娃次仁.md "wikilink")
        [罗松江村](../Page/罗松江村.md "wikilink")
        [杰珠·嘎玛白旦久美](../Page/杰珠·嘎玛白旦久美.md "wikilink")
        [甲登·洛绒向巴](../Page/甲登·洛绒向巴.md "wikilink")
        　[多吉扎西](../Page/多吉扎西.md "wikilink")
        [戈教·云登嘉措](../Page/戈教·云登嘉措.md "wikilink")
        　[香根·边玛仁青](../Page/香根·边玛仁青.md "wikilink")
        　[坚赞博桑](../Page/坚赞博桑.md "wikilink")
        　[关加益西](../Page/关加益西.md "wikilink")（摩梭）　[丁科仓](../Page/丁科仓.md "wikilink")
        [贡去金巴](../Page/贡去金巴.md "wikilink")
        [罗桑宗周](../Page/罗桑宗周.md "wikilink")
        　　[阿其堪布](../Page/阿其堪布.md "wikilink")
        　[土观·嘉木样洛桑旦贝尼玛](../Page/土观·嘉木样洛桑旦贝尼玛.md "wikilink")
        [夏日仓·丹增久美噶旦尖措](../Page/夏日仓·丹增久美噶旦尖措.md "wikilink")
        [噶尔哇·阿旺桑波](../Page/噶尔哇·阿旺桑波.md "wikilink")
        [更藏班玛南杰](../Page/更藏班玛南杰.md "wikilink")
        [新克木](../Page/新克木.md "wikilink")
        　[嘎拉僧希日布](../Page/嘎拉僧希日布.md "wikilink")（[赵九九](../Page/赵九九.md "wikilink")）
        [嘎拉僧图布丹却吉旺曲](../Page/嘎拉僧图布丹却吉旺曲.md "wikilink")（洞阔尔）　[洛桑义希·成来坚措](../Page/洛桑义希·成来坚措.md "wikilink")
        [郭莽仓](../Page/郭莽仓.md "wikilink")

[郭莽·俄赛尖措](../Page/郭莽·俄赛尖措.md "wikilink")
[全柏音](../Page/全柏音.md "wikilink")

  - 南传上座部佛教工作委员会
      - 主　任：[祜巴龙庄勐](../Page/祜巴龙庄勐.md "wikilink")
      - 副主任：[诏等傣](../Page/诏等傣.md "wikilink")
        　[都罕听](../Page/都罕听.md "wikilink")
      - 委　员：[提卡达希](../Page/提卡达希.md "wikilink")
        　[召问地达](../Page/召问地达.md "wikilink")
        [伍巴地亚](../Page/伍巴地亚.md "wikilink")
        [召细利](../Page/召细利.md "wikilink")
        [王光华](../Page/王光华.md "wikilink")（佤）　[都岩发](../Page/都岩发.md "wikilink")
        [康南山](../Page/康南山.md "wikilink")
        [岩看堂](../Page/岩看堂.md "wikilink")（布朗）
  - 汉传佛教教务教风委员会
      - 主　任：[道慈](../Page/道慈.md "wikilink")
      - 副主任：如瑞（女）　 正慈　印乐　纯闻
      - 委　员：了尘　中慧　长顺　正恺　本英　本悟　妙安　妙真　国亮　昌善　明杰　性妙　空性
        觉灯　海信　能开　智文　智海　耀正　常道　智勇　智超　妙果　成清　大智（女）
        心亮（女）　吉祥（女）　传德（女）　性康（女）
  - 佛教教育委员会
      - 主　任：[湛如](../Page/湛如.md "wikilink")
      - 副主任：宗性　明海　慧明　光泉
      - 委　员：大安　广如　心见　永兴　圣凯　达诠　达照　向学　昌明　明向　明基　法通　界象
        济群　振宇　宽容　理证　理净　隆相　智明　普仁　普钰　道坚（羌）　万如（女）
        如妙（女）　如意（女）　法源（女）
  - 慈善公益委员会
      - 主　任：[明生](../Page/明生.md "wikilink")
      - 副主任：静波　则悟　肖占军　身振
      - 委　员：一度　大恩　广霖　仁昌　世良　弘如　圣清　成刚　向愿　妙林　贤志　贤宗　净芳
        净明　法性　泽道　定恒　素全　法海　海空　常辉　悲寂　智丰　照观　慈满　耀智　妙乐（女）　妙贤（女　满）　莲华（女）　张汝兰（女）
  - 权益保护委员会
      - 主　任：[圣辉](../Page/圣辉.md "wikilink")
      - 副主任：演觉　[胡雪峰](../Page/胡雪峰.md "wikilink")（蒙）　慧庆　宏度
      - 委　员：中谛　心舫　正开　光明　光慧　延慈　妙航　果明　果真　明禅　法成　俊才　觉海
        圆持　圆藏　理因　道极　道证　道源　照睿　大行（女）　彻慧
        （女）　明慧（女）　思智（女）　能净（女）　渊博（女）　陈星桥
  - 海外交流委员会
      - 主　任：[永信](../Page/释永信.md "wikilink")
      - 副主任：印顺　普法　常藏　吴国平
      - 委　员：中勇　心照　本性　可祥　可潜　代林（蒙）　传正　园慈　妙有　妙侠　明徹　法量　素慧　宽旭　宽严　通植　崇化　崇慈　普正　道藏　廓尘　嘎喇森奥斯尔尼玛（包天虎）　演道　慧光　曙正　定慧（女）　星慈（女）
  - 文化艺术委员会
      - 主　任：[觉醒](../Page/释觉醒.md "wikilink")
      - 副主任：心澄　纯一　照诚　秋爽
      - 委　员：大初　大岳　月真　心广　心明　允观　永悟　宏满　果光　怡学　诚信　赵雄　信光
        奚白　能修　清远　淳法　惟航　谛性　隆醒　智如　意寂　演龙　常慧（女）　藏青（女）　陈建华　高士涛
  - 居士事务委员会
      - 主　任：[永寿](../Page/释永寿.md "wikilink")
      - 副主任：妙江　增勤　怡藏　怀梵
      - 委　员：大明　万恒　圣君　学悟　悟空　寂仁　续学　澈性　本净（女）　有缘（女）　宏国（女）常宏（女）　常瑞（女）　惟信（女）　王立军　王博勤　王孺童　吕维新　安虎生　李进（壮）　张国铭　张海忠　周富根　郑子昌　胡志文　洪利民　陶书同　董镇华　刘燕梅（女）陈妙丽（女）

## 机构设置

  - 中国佛教协会工作部门

<!-- end list -->

  - 办公室
  - 教务部
  - 国际部
  - 专委会办公室
  - 寺庙工作办公室
  - 综合研究室
  - 《[法音](../Page/法音.md "wikilink")》编辑部
  - [中国佛教文化研究所](../Page/中国佛教文化研究所.md "wikilink")
  - [中国佛教图书文物馆](../Page/中国佛教图书文物馆.md "wikilink")
  - [中国佛学院](../Page/中国佛学院.md "wikilink")
  - [南京金陵刻经处](../Page/南京金陵刻经处.md "wikilink")

<!-- end list -->

  - 直属寺院

<!-- end list -->

  - [北京广济寺](../Page/广济寺_\(北京市\).md "wikilink")
  - [北京灵光寺](../Page/灵光寺_\(北京\).md "wikilink")
  - [北京法源寺](../Page/法源寺_\(北京\).md "wikilink")
  - [深圳弘法寺](../Page/弘法寺_\(深圳\).md "wikilink")
  - [尼泊尔中华寺](../Page/中华寺_\(尼泊尔\).md "wikilink")

## 注释

## 参考文献

## 外部链接

  - [中国佛教协会](http://www.chinabuddhism.com.cn/)

## 参见

  - [中国佛教会](../Page/中国佛教会.md "wikilink")
  - [中国寺院列表](../Page/中国寺院列表.md "wikilink")
  - [汉族地区佛教全国重点寺院](../Page/汉族地区佛教全国重点寺院.md "wikilink")

{{-}}

[1](../Category/中华人民共和国全国性宗教团体.md "wikilink")
[中国佛教协会](../Category/中国佛教协会.md "wikilink")
[Category:中华人民共和国佛教组织](../Category/中华人民共和国佛教组织.md "wikilink")
[Category:1953年建立的组织](../Category/1953年建立的组织.md "wikilink")
[Category:中华人民共和国协会](../Category/中华人民共和国协会.md "wikilink")

1.

2.

3.  [学诚辞去中国佛教协会会长，《北京日报》，2018年8月15日](http://news.sina.com.cn/c/2018-08-15/doc-ihhtfwqr5224871.shtml)

4.
5.  现代佛学1953年第6期，第17页

6.  现代佛学1957年第5期，第28页

7.  现代佛学1962年第2期，第9页

8.  法音1981年第1期/总第1期，第20页

9.  法音1987年第3期/总第37期，第35页

10. 法音1993年第12期/总第112期，第49页

11. 法音1993年第12期/总第112期，第50页

12. 阿兰若，中国佛教协会第七次全国代表会议在京隆重举行，法音2002年10期

13. 陈星桥，中国佛教协会第八次全国代表会议在京隆重举行，法音2010年第2期 (总第306期) ，第54页

14.
15.
16. [中国佛教协会召开第九届理事会第一次会议](http://www.chinabuddhism.com.cn/xw/yw1/2015-04-20/8581.html)