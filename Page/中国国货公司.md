**中國國貨公司**（**China Merchandise
Corp.**）是1933年由[方液仙於](../Page/方液仙.md "wikilink")[上海設立的公司](../Page/上海.md "wikilink")。該公司當時在「[支持國貨](../Page/支持國貨.md "wikilink")」的運動上，有很重要的影響力。中華人民共和國取得政權後，1958年被[華潤集團收購股權](../Page/華潤集團.md "wikilink")，之後在1969年和港資成立[大華國貨有限公司](../Page/大華國貨有限公司.md "wikilink")，到了1993年，集團已和[大華國貨有限公司合併](../Page/大華國貨有限公司.md "wikilink")，由[華潤百貨取代](../Page/華潤百貨.md "wikilink")，在1996年再分拆中成药部门，成為現時[華潤堂至今](../Page/華潤堂.md "wikilink")。

## 歷史

### 緣起

[九一八事變后](../Page/九一八事變.md "wikilink")，全中国人民抗日情绪高涨，广泛开展了提倡国货，抵制日货运动。[方液仙由此萌发了创办中国国货公司的设想](../Page/方液仙.md "wikilink")。

### 过程

[ZGGH.jpeg](https://zh.wikipedia.org/wiki/File:ZGGH.jpeg "fig:ZGGH.jpeg")
[方液仙以自己创办的](../Page/方液仙.md "wikilink")[中国化学工业社联合](../Page/中国化学工业社.md "wikilink")[美亚织绸厂](../Page/美亚织绸厂.md "wikilink")、[五和织造厂](../Page/五和织造厂.md "wikilink")、[华生电器厂](../Page/华生电器厂.md "wikilink")、[鸿兴布厂](../Page/鸿兴布厂.md "wikilink")、[华昌钢精厂](../Page/华昌钢精厂.md "wikilink")、[中华珐琅厂](../Page/中华珐琅厂.md "wikilink")、[亚浦灯泡厂](../Page/亚浦灯泡厂.md "wikilink")、[华福帽厂等](../Page/华福帽厂.md "wikilink")9家厂商，于1932年九一八周年纪念日，在[上海](../Page/上海.md "wikilink")[南京路绮华公司举办九厂国货临时联合商场](../Page/南京路_\(上海\).md "wikilink")。

1933年2月，方液仙经过多方活动，與[蕢延芳](../Page/蕢延芳.md "wikilink")、[任士剛等人投入資本](../Page/任士剛.md "wikilink")10萬，联合二百多家厂家，在上海南京东路大陆商场开设了中国国货公司，方液仙任董事长兼总经理（后由[李康年任经理](../Page/李康年.md "wikilink")），方液仙、[葉友才](../Page/葉友才.md "wikilink")、蕢延芳、任士剛為董事。国货公司采取薄利多销、商品寄售的方式，加上其服务周到，所以营业鼎盛。半年后又扩充了二楼南部商场，营业得到不断发展。

上海的中国国货公司规模大，货品齐全价格合理，加上当时人们爱国热情高涨，因而生意十分兴隆。为了把上海的中国国货公司模式推向全国，产销协会在1934年1月改组为
“中国国货公司介绍所全国联合办事处”（简称国货联办处）。

自从国货联办处成立后，从1934年1月开始到1935年底的两年内，在各地开设的中国国货公司计有：上海、南京、武汉、郑州、长沙、温州、镇江、济南、徐州、嘉兴、西安、昆明、福州、汕头、香港、广州、贵阳、重庆、成都、桂林、广州湾（即现湛江）以及新加坡等地方共22家。

## 1933年中国国货公司开幕宣言

## 參照

  - [方液仙](../Page/方液仙.md "wikilink")
  - [國貨](../Page/國貨.md "wikilink")
  - [國貨公司](../Page/國貨公司.md "wikilink")
  - [王志莘](../Page/王志莘.md "wikilink")

## 参考資料

  - [上海档案信息网首页-\>海上人物-\>产业翘楚](https://web.archive.org/web/20071206111216/http://archives.sh.cn/hshrw/chyqch/200301200181.htm)
  - [方之雄](../Page/方之雄.md "wikilink")、[方曾澤與](../Page/方曾澤.md "wikilink")[方曾規編纂](../Page/方曾規.md "wikilink")，《爱国实业家方液仙》，南昌市：江西人民出版社，1992年12月
    ISBN 7-210-01161-7
  - [乐承耀編纂](../Page/乐承耀.md "wikilink")，《近代宁波商人与社会经济》，北京：人民出版社，2007年8月
    ISBN 978-7-01-006358-4
  - [从“国货运动”到中心百货](https://web.archive.org/web/20070705181526/http://wuhan.yiyou.com/html/17/199.html)
  - [抗日战争中的宁波商人](http://culture.zjol.com.cn/05culture/system/2005/12/27/006419585.shtml)
  - \[<http://www.gzzxws.gov.cn/gzws>../cg/cgml/cg8/200808/t20080826_3818_1.htm
    《上海工商界星五聚餐会的缘起及其提倡国货的活动》\]陈醒吾編，中国人民政治协商会议广州市委员会

## 外部链接

[category:華潤原旗下企業](../Page/category:華潤原旗下企業.md "wikilink")

[Category:1933年成立的公司](../Category/1933年成立的公司.md "wikilink")
[Category:1993年結業的公司](../Category/1993年結業的公司.md "wikilink")
[Category:中國百貨公司](../Category/中國百貨公司.md "wikilink")
[Category:中华人民共和国国有企业](../Category/中华人民共和国国有企业.md "wikilink")