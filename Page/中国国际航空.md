[AirChinaBuildingShunyi1.JPG](https://zh.wikipedia.org/wiki/File:AirChinaBuildingShunyi1.JPG "fig:AirChinaBuildingShunyi1.JPG")
[AirChinaInfoBuilding1.jpg](https://zh.wikipedia.org/wiki/File:AirChinaInfoBuilding1.jpg "fig:AirChinaInfoBuilding1.jpg")
[Air_China_B747-4J6_B-2447_EDDS_01.jpg](https://zh.wikipedia.org/wiki/File:Air_China_B747-4J6_B-2447_EDDS_01.jpg "fig:Air_China_B747-4J6_B-2447_EDDS_01.jpg")
**中国国际航空股份有限公司**（[IATA](../Page/IATA.md "wikilink")：**CA**，[ICAO](../Page/ICAO.md "wikilink")：**CCA**，、、），简称**国航**，是[中国大陆第二大的](../Page/中华人民共和国.md "wikilink")[国有](../Page/国有企业.md "wikilink")[航空企业](../Page/航空公司.md "wikilink")，也是中国大陆唯一的[载旗民用航空公司](../Page/载旗航空公司.md "wikilink")，总部设在[北京](../Page/北京.md "wikilink")，以[北京首都国际机场为基地](../Page/北京首都国际机场.md "wikilink")。其前身中国国际航空公司成立于1988年。2002年10月，中国国际航空公司联合中国航空总公司和[中国西南航空公司](../Page/中国西南航空公司.md "wikilink")，成立了中国航空集团公司，并以联合三方的航空运输资源为基础，组建新的中国国际航空公司。**中国航空集团有限公司**与[中国东方航空集团有限公司和](../Page/中国东方航空.md "wikilink")[中国南方航空集团有限公司合称中国三大航空集团](../Page/中国南方航空.md "wikilink")，经营业务包括航空客运、航空货运及物流两大核心产业。

中国国际航空股份有限公司2017年营业总额1213.63亿元人民币，全年净利润72.4亿元人民币。

## 歷史

国航的历史最早可追溯到1955年的[中国民航北京管理局飞行总队](../Page/中国民用航空总局.md "wikilink")，1988年中國民航被分為多家航空公司，分別為[中國東方航空](../Page/中國東方航空.md "wikilink")、[中國南方航空](../Page/中國南方航空.md "wikilink")、[中國北方航空](../Page/中國北方航空.md "wikilink")、[中國西南航空](../Page/中國西南航空.md "wikilink")、[中國西北航空和专门负责国际航线的航空公司](../Page/中國西北航空.md "wikilink")**中国国际航空**，但國航在后期也負責国内航线。

1990年代，中國政府大幅調整和改革民航事業。1994年，中國國務院批准國外資金投資中國的機場建設和民航業，放寬外國製造的民航機輸入中國，使航空公司的機隊急速現代化。90年代後期，國航的營運狀況並不理想，更出現虧損情況。

2002年10月，以原先之中國國際航空公司為基礎，聯合中國航空集團和[中國西南航空公司組成中国航空集团公司](../Page/中國西南航空公司.md "wikilink")，并以联合三方的航空运输资源为基础，组建新的中国国际航空公司，仍为中国载国旗和担任中国领导人专机任务的航空公司。

2004年9月30日，经[国务院国有资产监督管理委员会批准](../Page/国务院国有资产监督管理委员会.md "wikilink")，作为中国航空集团公司控股的航空运输主业公司，中国国际航空股份有限公司（以下简称国航）在北京正式成立，进行了[股份制改革](../Page/股份制.md "wikilink")，继续保留原中国国际航空公司的名称，并使用中国国际航空公司的标志。同年12月，国航进行首次境外发售[股票](../Page/股票.md "wikilink")；12月15日，公司在[香港和](../Page/香港.md "wikilink")[伦敦证交所挂牌上市](../Page/伦敦.md "wikilink")。

2006年8月18日，公司在[上海证券交易所上市](../Page/上海证券交易所.md "wikilink")。2006年5月，國航與德國[漢莎航空簽署同意書](../Page/漢莎航空.md "wikilink")，於2007年12月12日正式加入[星空联盟](../Page/星空联盟.md "wikilink")（Star
Alliance），成为国际航空联盟成员的大型国家航空公司之一，也是聯盟中首位來自中國大陆的成員航空公司；而旗下子公司[深圳航空也將在](../Page/深圳航空.md "wikilink")2012年加入聯盟。2006年9月28日，國航把[港龍航空的股份全部售于](../Page/港龍航空.md "wikilink")[國泰航空](../Page/國泰航空.md "wikilink")，使港龍航空成為國泰航空的全資公司。而國航亦入股國泰航空使兩者成為互控公司及重要合作夥伴。

2007年度，国航入选了世界品牌500强，为中国民航唯一入选公司；2007年国航被世界品牌实验室评为中国500最具价值品牌；美国评级机构[标准普尔评为中国大陆上市公司百强](../Page/标准普尔.md "wikilink")；国航品牌被英国《[金融时报](../Page/金融时报.md "wikilink")》和美国[麦肯锡管理咨询公司联合评定为中国大陆十大国际品牌之一](../Page/麦肯锡.md "wikilink")，是中國大陆航空公司第一的品牌。2008年，国航获得世界品牌价值实验室颁发的“中国最佳信誉品牌”奖项。

2008年，國航成為了[北京奥运会航空客运合作伙伴](../Page/北京奥运会.md "wikilink")。

2009年8月17日，中国国际航空斥資63.35億元向中信泰富購入4.9億股國泰航空的股權，相等國泰12.5%已發行股本，而[太古則斥資](../Page/太古.md "wikilink")12.13億元，購入2%國泰股權。完成交易後，國航於國泰的持股量再由17.49%升至29.99%，而太古的持股量則由39.97%升至41.97%。[中信泰富於國泰的持股量則減至](../Page/中信泰富.md "wikilink")2.98%。

2010年3月22日，中国国际航空出資6.82億元人民幣增持[深圳航空股權](../Page/深圳航空.md "wikilink")，由原先25%增至51%，成為其控股股東。

2011年，公司總營業額933.43億元，純利約74.76億元人民幣，同比跌38.75%。

2011年6月，中國國際航空首度獲得[Skytrax授予四星級航空公司之評級](../Page/Skytrax.md "wikilink")。

2011年4月11日，以公务专、包机飞行为主要业务的北京航空有限责任公司获准营业，中国国际航空控股51%。

2012年3月27日，北京航空有限责任公司与瑞士商务机公司VistaJe联合组建一支以公务机为主的商业喷射机队。

2013年7月3日，国航在一架編號為B-6525的[空中客車A330](../Page/空中客車A330.md "wikilink")-300上首度试行空中[无线上网服务](../Page/无线上网.md "wikilink")，这是中国首次全球卫星通讯互联网航班飞行。但国航并没有公布正式提供服务时如何收取费用，有部分人士表示为免费上网服务。空中[无线上网服务启用后](../Page/无线上网.md "wikilink")，旅客可以开启使用[笔记本电脑](../Page/笔记本电脑.md "wikilink")、[平板电脑等电子设备](../Page/平板电脑.md "wikilink")，登录飞行上网界面就可以实现空中上网，但[手机仍被禁止](../Page/手机.md "wikilink")。而空中上网服务带宽有限，机上系统能提供的上网服务并不多，仅支持微博和邮件，而且都内置在机上系统内。\[1\]

2018年1月2日，国航发布公告称，根据国家有关国有企业公司制改制工作部署，经[国务院国有资产监督管理委员会](../Page/国务院国有资产监督管理委员会.md "wikilink")《关于中国航空集团公司改制有关事项的批复》（国资改革〔2017〕1182号）批准，国航控股股东中国航空集团公司由全民所有制企业改制为国有独资企业，改制后名称为中国航空集团有限公司，由国务院国有资产监督管理委员会代表国务院履行出资人职责\[2\]。

2018年8月30日[中國國際航空以](../Page/中國國際航空.md "wikilink")24.39億人民幣出售[中國國際貨運航空](../Page/中國國際貨運航空.md "wikilink")51%股權给控股股東[中航集團](../Page/中航集團.md "wikilink")，預期出售收益4.11億人民幣

## 标志

国航的企业标识由一只艺术化的[凤凰和](../Page/凤凰.md "wikilink")[邓小平书写的](../Page/邓小平.md "wikilink")“中国国际航空公司”以及英文“AIR
CHINA”构成。凤凰是[中华民族古代](../Page/中华民族.md "wikilink")[传说中的神鸟](../Page/传说.md "wikilink")，也是中华民族自古以来所崇拜的吉祥鸟。据《[山海经](../Page/山海经.md "wikilink")》中记述：凤凰出于东方君子国，飞跃巍峨的[昆仑山](../Page/昆仑山.md "wikilink")，翱翔于四海之外，飞到哪里就给哪里带来吉祥和安宁。国航航徽标志是凤凰，同时又是英文“[VIP](../Page/VIP.md "wikilink")”（尊贵客人）的艺术变形，颜色为中国传统的大红，具有吉祥、圆满、祥和、幸福的寓意，正是希望这神圣的生灵及其有关它的美丽的传说带给朋友们吉祥和幸福。代表国航视每一位乘客及货主为上宾看待。

自国航开航以来至今，除了垂直尾翼的中国国旗换成凤凰，机身上的CAAC标志和“中国民航”字样改为“国旗+AIR
CHINA+中国国际航空公司”的排列外，其余部分涂装设计均沿用自CAAC时期的经典腰线涂装。

另外，國航是中國大陸的航空公司中唯一在機身塗有國旗的航空公司\[3\]，但飛往台灣的航班，機身上的國旗都會遮蓋掉，或是派出沒有國旗的彩繪機飛航台灣航線。

## 營運基地

中國國際航空的主要枢纽机场为[北京首都國際機場](../Page/北京首都國際機場.md "wikilink")、[成都雙流國際機場和](../Page/成都雙流國際機場.md "wikilink")[上海浦東國際機場](../Page/上海浦東國際機場.md "wikilink")。\[4\]此外，国航在广州、杭州、重庆等地设有分公司。

## 机队

[Air_China_Boeing_747-89L_Intercontinental_B-2486.jpg](https://zh.wikipedia.org/wiki/File:Air_China_Boeing_747-89L_Intercontinental_B-2486.jpg "fig:Air_China_Boeing_747-89L_Intercontinental_B-2486.jpg")在約翰·
F·肯尼迪國際機場；該機於2015年1月7日執行北京至紐約的首個747-8航班，以紀念北京-紐約航線開通34週年{{\#tag:ref|1981年1月7日，[中国民航北京管理局使用](../Page/中国民航.md "wikilink")[波音747SP开通北京](../Page/波音747SP.md "wikilink")-上海虹桥-旧金山-纽约航线，航班号为CA981/982；该航班2002年改为[不经停航班](../Page/不经停航班.md "wikilink")，但仍使用981/982这对航班号|group="注"}}\[5\]\]\]
[B-6498@HKG_(20190328160055).jpg](https://zh.wikipedia.org/wiki/File:B-6498@HKG_\(20190328160055\).jpg "fig:B-6498@HKG_(20190328160055).jpg")型客机\]\]
[B-2046@PEK_(20180411170211).jpg](https://zh.wikipedia.org/wiki/File:B-2046@PEK_\(20180411170211\).jpg "fig:B-2046@PEK_(20180411170211).jpg")\]\]
[B-1396@PEK_(20171213150043).jpg](https://zh.wikipedia.org/wiki/File:B-1396@PEK_\(20171213150043\).jpg "fig:B-1396@PEK_(20171213150043).jpg")\]\]
[B-1591@PEK_(20190101144741).jpg](https://zh.wikipedia.org/wiki/File:B-1591@PEK_\(20190101144741\).jpg "fig:B-1591@PEK_(20190101144741).jpg")型客機即將降落於[北京首都國際机场](../Page/北京首都國際机场.md "wikilink")\]\]
[B-5916@HKG_(20181101144044).jpg](https://zh.wikipedia.org/wiki/File:B-5916@HKG_\(20181101144044\).jpg "fig:B-5916@HKG_(20181101144044).jpg")于[香港国际机场](../Page/香港国际机场.md "wikilink")\]\]
[B-1086@PEK_(20180814174850).jpg](https://zh.wikipedia.org/wiki/File:B-1086@PEK_\(20180814174850\).jpg "fig:B-1086@PEK_(20180814174850).jpg")于[北京首都國際机场](../Page/北京首都國際机场.md "wikilink")\]\]
[B-6596@HKG_(20180903114732).jpg](https://zh.wikipedia.org/wiki/File:B-6596@HKG_\(20180903114732\).jpg "fig:B-6596@HKG_(20180903114732).jpg")于[香港国际机场](../Page/香港国际机场.md "wikilink")\]\]
[Air_China_Airbus_A319-131_B-6236_(8734402941).jpg](https://zh.wikipedia.org/wiki/File:Air_China_Airbus_A319-131_B-6236_\(8734402941\).jpg "fig:Air_China_Airbus_A319-131_B-6236_(8734402941).jpg")于[重庆江北国际机场](../Page/重庆江北国际机场.md "wikilink")\]\]
中國國際航空早期機隊由[波音707](../Page/波音707.md "wikilink")，[波音737-200和](../Page/波音737-200.md "wikilink")[三叉戟](../Page/霍克薛利三叉戟型.md "wikilink")，[BAe-146等老舊機隊組成](../Page/BAe-146.md "wikilink")。1994年，國務院批准放寬外國製造的民航機輸入中國，使國航的機隊急速現代化。目前國航的機隊主要由[波音及](../Page/波音.md "wikilink")[空中巴士飞机所組成](../Page/空中巴士.md "wikilink")。除此之外，国航直属的分公司——北京航空也拥有多架公务机。

2011年3月，中國國際航空正式向波音公司訂購7架[747-8客機](../Page/波音747-8.md "wikilink")，並在2012年9月11日獲得確認。首架飞机已于2014年10月1日飞抵北京首都机场。

### 现役

截至2018年8月，中國國際航空的機隊平均機齡6.8年，詳情如下\[6\]\[7\]\[8\]\[9\]\[10\]\[11\]\[12\]：

<center>

<table>
<caption><strong>中國國際航空機隊</strong></caption>
<thead>
<tr class="header">
<th><p>機型</p></th>
<th><p>數量</p></th>
<th><p>訂購中</p></th>
<th><p>引擎</p></th>
<th><p>載客量</p></th>
<th><p>机型代码</p></th>
<th><p>航线 [13][14]</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><abbr title="頭等艙">F</abbr></p></td>
<td><p><abbr title="公务舱">B</abbr></p></td>
<td><p><abbr title="高级經濟艙">E+</abbr></p></td>
<td><p><abbr title="經濟艙">E</abbr></p></td>
<td><p>總計</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>長途客機</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/空中客车A330.md" title="wikilink">空中巴士A330-200</a></p></td>
<td><p>26</p></td>
<td><p>—</p></td>
<td><p><a href="../Page/劳斯莱斯瑞达700.md" title="wikilink">罗尔斯·罗伊斯遄达 772C-60</a></p></td>
<td><p>—</p></td>
<td><p>30</p></td>
<td><p>—</p></td>
<td><p>207</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>—</p></td>
<td><p>12</p></td>
<td><p>—</p></td>
<td><p>271</p></td>
<td><p>283</p></td>
<td><p>国内（高原航线）</p></td>
<td><p>B-6070/71/72/81，配属西南分公司<br />
公务舱座椅倾斜度165°，经济舱不设个人娱乐系统。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/空中巴士A330.md" title="wikilink">空中巴士A330-300</a></p></td>
<td><p>29</p></td>
<td><p>6</p></td>
<td><p><a href="../Page/劳斯莱斯瑞达700.md" title="wikilink">罗尔斯·罗伊斯遄达 772C-60</a></p></td>
<td><p>—</p></td>
<td><p>30</p></td>
<td><p>16</p></td>
<td><p>255</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
<td><p>36</p></td>
<td><p>20</p></td>
<td><p>255</p></td>
<td><p>311</p></td>
<td><p>333</p></td>
<td><p>较晚引进的A330-300采用新布局[15]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/空中客车A330neo.md" title="wikilink">空中客车A330-800/900</a></p></td>
<td><p>14</p></td>
<td><p>24</p></td>
<td><p><a href="../Page/罗尔斯·罗伊斯遄达.md" title="wikilink">罗尔斯·罗伊斯 Trent 7000</a></p></td>
<td><p>有待确定</p></td>
<td><p>国内/欧洲/亚太</p></td>
<td><p>预计2019年10月交付</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音747-8.md" title="wikilink">波音747-89L</a></p></td>
<td><p>6</p></td>
<td><p>—</p></td>
<td><p><a href="../Page/通用电气GEnx发动机.md" title="wikilink">通用电气GEnx-2B67</a></p></td>
<td><p>12</p></td>
<td><p>54</p></td>
<td><p>66</p></td>
<td><p>233</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音747.md" title="wikilink">波音747-4J6</a></p></td>
<td><p>3</p></td>
<td><p>—</p></td>
<td><p><a href="../Page/普惠.md" title="wikilink">普惠</a> <a href="../Page/普惠PW4000.md" title="wikilink">PW4056</a></p></td>
<td><p>10</p></td>
<td><p>42</p></td>
<td><p>—</p></td>
<td><p>292</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音777.md" title="wikilink">波音777-39LER</a></p></td>
<td><p>20</p></td>
<td><p>—</p></td>
<td><p><a href="../Page/通用电气GE90发动机.md" title="wikilink">通用电气GE90-115B</a></p></td>
<td><p>8</p></td>
<td><p>42</p></td>
<td><p>—</p></td>
<td><p>261</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>36</p></td>
<td><p>—</p></td>
<td><p>356</p></td>
<td><p>392</p></td>
<td><p>77W</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音777X.md" title="wikilink">波音777-9</a></p></td>
<td><p>16</p></td>
<td><p>12</p></td>
<td><p><a href="../Page/通用电气GE9X发动机.md" title="wikilink">通用电气GE9X发动机</a></p></td>
<td><p>有待确定</p></td>
<td><p>国内/东南亚/北美洲/亚太</p></td>
<td><p>预计2020年7月起交付</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音787.md" title="wikilink">波音787-9</a></p></td>
<td><p>14</p></td>
<td><p>1</p></td>
<td><p><a href="../Page/劳斯莱斯瑞达1000.md" title="wikilink">罗尔斯·罗伊斯遄达 1000</a></p></td>
<td><p>—</p></td>
<td><p>30</p></td>
<td><p>34</p></td>
<td><p>229</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音公務機.md" title="wikilink">波音747-89L</a></p></td>
<td><p>1</p></td>
<td><p>—</p></td>
<td><p><a href="../Page/通用电气GEnx发动机.md" title="wikilink">通用电气GEnx-2B67</a></p></td>
<td><p>2</p></td>
<td><p>20</p></td>
<td><p>—</p></td>
<td><p>40</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/空中客车A350.md" title="wikilink">空中巴士A350-900</a></p></td>
<td><p>8</p></td>
<td><p>2</p></td>
<td><p><a href="../Page/罗尔斯·罗伊斯遄达XWB.md" title="wikilink">罗尔斯·罗伊斯遄达 XWB</a></p></td>
<td><p>—</p></td>
<td><p>32</p></td>
<td><p>24</p></td>
<td><p>256</p></td>
</tr>
<tr class="even">
<td><p><strong>中短途客機</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/空中客车A320.md" title="wikilink">空中巴士A319-100</a></p></td>
<td><p>33</p></td>
<td><p>—</p></td>
<td><p><a href="../Page/CFM56.md" title="wikilink">CFM56-5B7/3</a><br />
<a href="../Page/國際航空發動機V2500.md" title="wikilink">IAE V2522-A5</a></p></td>
<td><p>—</p></td>
<td><p>8</p></td>
<td><p>—</p></td>
<td><p>120</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/空中客车A320neo系列.md" title="wikilink">空中巴士A319neo</a></p></td>
<td><p>—</p></td>
<td><p>8</p></td>
<td><p>有待確定</p></td>
<td><p>有待確定</p></td>
<td><p>有待確定</p></td>
<td><p>33架A320neo系列订单中8架为319neo[16]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/空中巴士A320.md" title="wikilink">空中巴士A320-200</a></p></td>
<td><p>46</p></td>
<td><p>11[17]</p></td>
<td><p>CFM56-5B4<br />
IAE V2527-A5</p></td>
<td><p>—</p></td>
<td><p>8</p></td>
<td><p>—</p></td>
<td><p>150</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/空中客车A320neo系列.md" title="wikilink">空中巴士A320neo</a></p></td>
<td><p>5</p></td>
<td><p>8[18]</p></td>
<td><p>PW 1127G<br />
<a href="../Page/CFM_International_LEAP-X.md" title="wikilink">CFM LEAP-1A</a></p></td>
<td><p>—</p></td>
<td><p>8</p></td>
<td><p>—</p></td>
<td><p>150</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/空中客车A320系列.md" title="wikilink">空中巴士A321-200</a></p></td>
<td><p>61</p></td>
<td><p>—</p></td>
<td><p>CFM56-5B2/3<br />
IAE V2530-A5</p></td>
<td><p>—</p></td>
<td><p>16</p></td>
<td><p>—</p></td>
<td><p>161</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
<td><p>12</p></td>
<td><p>—</p></td>
<td><p>173</p></td>
<td><p>185</p></td>
<td><p>321</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/空中客车A320neo系列.md" title="wikilink">空中巴士A321neo</a></p></td>
<td><p>1</p></td>
<td><p>11</p></td>
<td><p><a href="../Page/普惠PW1000G发动机.md" title="wikilink">普惠PW1000G发动机</a></p></td>
<td><p>—</p></td>
<td><p>12</p></td>
<td><p>—</p></td>
<td><p>182</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音737新世代.md" title="wikilink">波音737-79L</a></p></td>
<td><p>17</p></td>
<td><p>—</p></td>
<td><p>CFM56-7B24</p></td>
<td><p>—</p></td>
<td><p>8</p></td>
<td><p>—</p></td>
<td><p>120</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音737新世代.md" title="wikilink">波音737-89L</a></p></td>
<td><p>116</p></td>
<td><p>60</p></td>
<td><p>CFM56-7B26</p></td>
<td><p>—</p></td>
<td><p>12</p></td>
<td><p>—</p></td>
<td><p>147</p></td>
</tr>
<tr class="even">
<td><p>—</p></td>
<td><p>8</p></td>
<td><p>—</p></td>
<td><p>159</p></td>
<td><p>167</p></td>
<td><p>738</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
<td><p>8</p></td>
<td><p>—</p></td>
<td><p>168</p></td>
<td><p>176</p></td>
<td><p>73N</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音737MAX.md" title="wikilink">波音737 MAX 8</a></p></td>
<td><p>16</p></td>
<td><p>29</p></td>
<td><p><a href="../Page/CFM_International_LEAP-X.md" title="wikilink">CFM International LEAP-1B</a></p></td>
<td><p>—</p></td>
<td><p>8</p></td>
<td><p>—</p></td>
<td><p>163</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/C919.md" title="wikilink">中国商飞C919</a></p></td>
<td><p>—</p></td>
<td><p>20</p></td>
<td><p><a href="../Page/CFM_International_LEAP-X.md" title="wikilink">CFM International LEAP-1C</a></p></td>
<td><p>有待確定</p></td>
<td><p>有待確定</p></td>
<td><p>預計2020年起交付。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>總數</strong></p></td>
<td><p><strong>405</strong></p></td>
<td><p><strong>166</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

</center>

### 货运机队

[Boeing_747-4FTF(SCD),_Air_China_Cargo_JP6908081.jpg](https://zh.wikipedia.org/wiki/File:Boeing_747-4FTF\(SCD\),_Air_China_Cargo_JP6908081.jpg "fig:Boeing_747-4FTF(SCD),_Air_China_Cargo_JP6908081.jpg")\]\]

<center>

<table>
<caption><strong>中國國際航空貨運機隊</strong></caption>
<thead>
<tr class="header">
<th><p><font color=black>機型</p></th>
<th><p><font color=black>數量</p></th>
<th><p><font color=black>訂購中</p></th>
<th><p><font color=black>引擎</p></th>
<th><p><font color=black>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/波音747.md" title="wikilink">波音747-4J6F（74F）</a></p></td>
<td><p>3</p></td>
<td><p>—</p></td>
<td><p><a href="../Page/普惠.md" title="wikilink">普惠</a> <a href="../Page/普惠PW4000.md" title="wikilink">PW4056</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音777.md" title="wikilink">波音777-FFT （77F）</a></p></td>
<td><p>8</p></td>
<td><p>—</p></td>
<td><p>通用电气GE90-110B</p></td>
<td><p>2013年3月訂購<br />
原為國泰航空之訂單，其後於2013年3月1日國泰宣佈改為訂購3架747-8F後正式轉移訂單予國航<br />
首架量產機（編號為B-2095）於2013年12月18日交付</p></td>
</tr>
<tr class="odd">
<td><p><strong>總數</strong></p></td>
<td><p><strong>11</strong></p></td>
<td><p><strong>—</strong></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

</center>

### 已退役

<center>

[Air_China_Boeing_767-300ER_Gilbert.jpg](https://zh.wikipedia.org/wiki/File:Air_China_Boeing_767-300ER_Gilbert.jpg "fig:Air_China_Boeing_767-300ER_Gilbert.jpg")（已于2012年全数退役）\]\]

| 機型                                                        | 服役年份 | 退役年份                                                                                                                                                                                                                       |
| --------------------------------------------------------- | ---- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [空中巴士A340-300](../Page/空中客车A340#A340-300系列.md "wikilink") | 1997 | 2014                                                                                                                                                                                                                       |
| [BAe 146-100](../Page/BAe_146.md "wikilink")              | 1987 | 2002 \[19\]                                                                                                                                                                                                                |
| [波音707-3J6B/3J6C](../Page/波音707.md "wikilink")            | 1974 | 1993                                                                                                                                                                                                                       |
| [波音737-2T4](../Page/波音737.md "wikilink")                  | 1987 | 1995 {{\#tag:ref|1995年轉投[長城航空 (寧波)](../Page/長城航空_\(寧波\).md "wikilink")|group="注"}}                                                                                                                                         |
| 波音737-66N                                                 | 2003 | 2009                                                                                                                                                                                                                       |
| [波音747SP-J6](../Page/波音747SP.md "wikilink")               | 1980 | 1999                                                                                                                                                                                                                       |
| [波音747-2J6BM](../Page/波音747.md "wikilink")                | 1983 | 1998                                                                                                                                                                                                                       |
| [波音747-2J6F/2J6SF](../Page/波音747.md "wikilink")           | 1998 | 2010                                                                                                                                                                                                                       |
| 波音747-4J6M                                                | 1989 | 2013 {{\#tag:ref|分兩批引進，B-2456、B-2458、B-2460為1989\~1990年引进的第一批，已改为货机，B-2467\~B-2471為1997\~1999年引进的第二批，於2013年陸續退役。B-2470在美國封存；B-2471於2013年下半年被拆解，機身將用於北京首都國際機場消防演習。\[20\]B-2469為最後一架退役的747-400M，於2014年被出售。\[21\]|group="注"}} |
| [波音757-23A](../Page/波音757.md "wikilink")                  | 1997 | 2015                                                                                                                                                                                                                       |
| [波音767-2J6ER](../Page/波音767.md "wikilink")                | 1985 | 2009 {{\#tag:ref|有兩架因事故報廢，見“飛行事故”一段|group="注"}}                                                                                                                                                                            |
| 波音767-3J6                                                 | 1993 | 2012                                                                                                                                                                                                                       |
| 波音767-332ER/3Q8ER/33AER                                   | 2002 | 2012 {{\#tag:ref|除[B-2499外皆屬租借](../Page/江澤民專機事件.md "wikilink")|group="注"}}                                                                                                                                                 |
| 波音777-2J6                                                 | 1998 | 2018                                                                                                                                                                                                                       |
| [霍克薛利三叉戟型](../Page/霍克薛利三叉戟型.md "wikilink")                | 1974 | 1991                                                                                                                                                                                                                       |
| [洛克希德L-100-30](../Page/洛克希德L-100-30.md "wikilink")        | 1987 | 1999                                                                                                                                                                                                                       |
| [运7-100](../Page/运-7.md "wikilink")                       | 1987 | 1996                                                                                                                                                                                                                       |

</center>

## 目的地

[Airchina_destinationcountries.svg](https://zh.wikipedia.org/wiki/File:Airchina_destinationcountries.svg "fig:Airchina_destinationcountries.svg")
中国国际航空共开通了200多條國內外航線，国内通航城市超过80个，国际及地区际通航城市超过40个，以[欧亚大陆和](../Page/欧亚大陆.md "wikilink")[北美航线为主](../Page/北美.md "wikilink")，[南半球的航线有](../Page/南半球.md "wikilink")[印度尼西亚](../Page/印度尼西亚.md "wikilink")、[澳大利亚](../Page/澳大利亚.md "wikilink")、[新西兰](../Page/新西兰.md "wikilink")、[南非](../Page/南非.md "wikilink")、[埃塞俄比亚](../Page/埃塞俄比亚.md "wikilink")、[巴西等國](../Page/巴西.md "wikilink")。

另：中国国际航空是除[高丽航空外唯一可于朝鲜平壤](../Page/高丽航空.md "wikilink")[顺安国际机场通航的航空公司](../Page/顺安国际机场.md "wikilink")（航班編號為CA121/122，只限星期一和五，和只使用B-5043\~5045，最常用為B-5044），即唯一可在[朝鲜民主主义人民共和国内固定通航的外国航空公司](../Page/朝鲜民主主义人民共和国.md "wikilink")，於2017年11月22日至2018年6月5日期間因客流原因及中国配合联合国制裁朝鲜而停航。\[22\]\[23\]

### 代码共享

中国国际航空已与以下航空公司签署[代码共享协议](../Page/代码共享.md "wikilink")（截止于2016年7月）\[24\]：

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/加拿大航空.md" title="wikilink">加拿大航空</a></li>
<li><a href="../Page/澳门航空.md" title="wikilink">澳门航空</a></li>
<li><a href="../Page/新西兰航空.md" title="wikilink">新西兰航空</a></li>
<li><a href="../Page/全日空.md" title="wikilink">全日空</a></li>
<li><a href="../Page/韩亚航空.md" title="wikilink">韩亚航空</a></li>
<li><a href="../Page/奥地利航空.md" title="wikilink">奥地利航空</a></li>
<li><a href="../Page/国泰航空.md" title="wikilink">国泰航空</a></li>
<li><a href="../Page/华夏航空.md" title="wikilink">华夏航空</a>[25]</li>
</ul></td>
<td><ul>
<li><a href="../Page/国泰港龙航空.md" title="wikilink">国泰港龙航空</a></li>
<li><a href="../Page/埃及航空.md" title="wikilink">埃及航空</a></li>
<li><a href="../Page/以色列航空.md" title="wikilink">以色列航空</a></li>
<li><a href="../Page/埃塞俄比亚航空.md" title="wikilink">埃塞俄比亚航空</a> [26]</li>
<li><a href="../Page/长荣航空.md" title="wikilink">长荣航空</a> [27]</li>
<li><a href="../Page/芬兰航空.md" title="wikilink">芬兰航空</a></li>
<li><a href="../Page/夏威夷航空.md" title="wikilink">夏威夷航空</a>[28]</li>
<li><a href="../Page/昆明航空.md" title="wikilink">昆明航空</a>[29]</li>
</ul></td>
<td><ul>
<li><a href="../Page/LOT波兰航空.md" title="wikilink">LOT波兰航空</a></li>
<li><a href="../Page/汉莎航空.md" title="wikilink">汉莎航空</a></li>
<li><a href="../Page/北欧航空.md" title="wikilink">北欧航空</a></li>
<li><a href="../Page/山东航空.md" title="wikilink">山东航空</a></li>
<li><a href="../Page/深圳航空.md" title="wikilink">深圳航空</a></li>
<li><a href="../Page/南非航空.md" title="wikilink">南非航空</a> [30]</li>
<li><a href="../Page/TAP葡萄牙航空.md" title="wikilink">TAP葡萄牙航空</a></li>
<li><a href="../Page/西藏航空.md" title="wikilink">西藏航空</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/土耳其航空.md" title="wikilink">土耳其航空</a></li>
<li><a href="../Page/立荣航空.md" title="wikilink">立荣航空</a></li>
<li><a href="../Page/联合航空.md" title="wikilink">联合航空</a></li>
<li><a href="../Page/维珍航空.md" title="wikilink">维珍航空</a></li>
<li><a href="../Page/新加坡航空.md" title="wikilink">新加坡航空</a></li>
<li><a href="../Page/勝安航空.md" title="wikilink">勝安航空</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 特殊涂装機

[Air_China_B-2047_China-France_PEK.JPG](https://zh.wikipedia.org/wiki/File:Air_China_B-2047_China-France_PEK.JPG "fig:Air_China_B-2047_China-France_PEK.JPG")
[Air_China_Airbus_A320_Gu-1.jpg](https://zh.wikipedia.org/wiki/File:Air_China_Airbus_A320_Gu-1.jpg "fig:Air_China_Airbus_A320_Gu-1.jpg")
[B-6075-2008-05-27-YVR.jpg](https://zh.wikipedia.org/wiki/File:B-6075-2008-05-27-YVR.jpg "fig:B-6075-2008-05-27-YVR.jpg")火炬传递涂装的A330-200，现已改为“紫金号”涂装\]\]
[Air_China_Airbus_A330-243_B-6075_MUC_2015_04.jpg](https://zh.wikipedia.org/wiki/File:Air_China_Airbus_A330-243_B-6075_MUC_2015_04.jpg "fig:Air_China_Airbus_A330-243_B-6075_MUC_2015_04.jpg")
[B-5422_-_Air_China_-_Boeing_737-89L(WL)_-_Phoenix_Livery_-_PEK_(17009117992).jpg](https://zh.wikipedia.org/wiki/File:B-5422_-_Air_China_-_Boeing_737-89L\(WL\)_-_Phoenix_Livery_-_PEK_\(17009117992\).jpg "fig:B-5422_-_Air_China_-_Boeing_737-89L(WL)_-_Phoenix_Livery_-_PEK_(17009117992).jpg")
[Air_China_Boeing_777-300ER_in_Smiling_China_Livery.jpg](https://zh.wikipedia.org/wiki/File:Air_China_Boeing_777-300ER_in_Smiling_China_Livery.jpg "fig:Air_China_Boeing_777-300ER_in_Smiling_China_Livery.jpg")
[Air_China_Boeing_777-300ER_Zhao-1.jpg](https://zh.wikipedia.org/wiki/File:Air_China_Boeing_777-300ER_Zhao-1.jpg "fig:Air_China_Boeing_777-300ER_Zhao-1.jpg")涂装的波音777-300ER\]\]
[Air_China_A320-200(B-6610)_(6288814379).jpg](https://zh.wikipedia.org/wiki/File:Air_China_A320-200\(B-6610\)_\(6288814379\).jpg "fig:Air_China_A320-200(B-6610)_(6288814379).jpg")
[缩略图](https://zh.wikipedia.org/wiki/File:B-1083@PEK_\(20181229150742\).jpg "fig:缩略图")（B-1083）降落于[北京首都国际机场](../Page/北京首都国际机场.md "wikilink")\]\]

  - B-2006号波音B777-39L/ER客机，涂有“爱China”涂装，该机为国航首批20架777-300ER订单之最后一架，为庆祝[中华人民共和国建国](../Page/中华人民共和国.md "wikilink")65周年到来之际而喷绘，已于2014年9月27日交付
  - B-2032号波音B777-39L/ER客机，涂为[星空联盟涂装](../Page/星空联盟.md "wikilink")
  - B-2035号波音B777-39L/ER客机，涂有“微笑中国”主题涂装\[31\]\[32\]
  - B-2047号波音B777-39L/ER客机，涂有“庆祝中法建交50周年”涂装\[33\]
  - B-2059号波音B777-2J6客机，涂为紫宸号彩绘（第二套方案）（现已退役）
  - B-2060号波音B777-2J6客机，涂为紫金/紫轩号彩绘（第二套方案）（现已退役）
  - B-2376号空客A320-214客机，涂为牡丹主题彩绘（丁香紫）（现已退役）
  - B-2377号空客A320-214客机，涂为牡丹主题彩绘（梦幻蓝）（现已退役）
  - B-2642号波音B737-89L客机，涂有“第七届中国花卉博览会”涂装（现已退役）
  - B-5176号波音B737-86N客机，曾涂为北京2008年奥运会“奥运吉祥”彩绘涂装，后改为牡丹主题彩绘（银）
  - B-5177号波音B737-86N客机，曾涂为北京2008年奥运会“奥运吉祥”彩绘涂装，后改为牡丹主题彩绘（粉紫）
  - B-5178号波音B737-86N客机，曾涂为北京2008年奥运会“奥运吉祥”彩绘涂装，后改为牡丹主题彩绘（银）
  - B-5198号波音B737-89L客机，涂为牡丹主题彩绘（鹅蛋黄）
  - B-5201号波音B737-79L客机，曾涂为北京2008年奥运会彩绘涂装
  - B-5202号波音B737-79L客机，曾涂为北京2008年奥运会彩绘涂装
  - B-5211号波音B737-79L客机，曾涂为北京2008年奥运会彩绘涂装，后改为牡丹主题彩绘（中国红）
  - B-5214号波音B737-79L客机，涂为牡丹主题彩绘（板栗棕）
  - B-5390号波音B737-89L客机，涂为牡丹主题彩绘（香槟金）
  - B-5422号波音B737-89L客机，为国航接收的第50架波音737-800飞机，涂有“凤凰”彩绘涂装
  - B-5425号波音B737-89L客机，涂有“[2019年北京世界園藝博覽會](../Page/2019年北京世界園藝博覽會.md "wikilink")”主题涂装
  - B-5497号波音B737-89L客机，涂有“[2019年北京世界園藝博覽會](../Page/2019年北京世界園藝博覽會.md "wikilink")”主题涂装\[34\]
  - B-1083号空客A350-941飞机，涂有“[2019年北京世界園藝博覽會](../Page/2019年北京世界園藝博覽會.md "wikilink")”主题涂装
  - B-5977号空客A330-343客机，为国航接收的第50架空客A330飞机，涂有“国航第50架A330”图标
  - B-6075号空客A330-243客机，曾涂为“和谐之旅——北京2008奥林匹克火炬接力”涂装，后重新涂为紫金/紫轩号彩绘（第一套方案），2019后重新涂为星空联盟涂装
  - B-6076号空客A330-243客机，曾涂为紫宸号彩绘（第一套方案），后重新涂为星空联盟涂装\[35\]
  - B-6091号空客A330-243客机，涂为星空联盟涂装
  - B-6093号空客A330-243客机，涂为星空联盟涂装
  - B-6383号空客A321-213客机，涂为星空联盟涂装
  - B-6361号空客A321-213客机，涂有“秀美四川”主题涂装（红）\[36\]
  - B-6365号空客A321-213客机，涂有“秀美四川”主题涂装（橙）
  - B-6610号空客A320-214客机，涂有“锦绣湖北”涂装
  - 中国国际航空内蒙古公司B-5226号波音B737-79L飞机，涂有“天骄内蒙古”涂装\[37\]

## 常旅客计划

凤凰知音（Phoenix
Miles，2013年前称国航知音）是中国国际航空的飞行常客奖励计划，是中国大陆最早创立的的飞行常客奖励计划，乘客拥有知音卡片后，可通过搭乘国航及下属公司以及星空联盟成员等伙伴航空公司航班累积里程，来换取免费机票及升舱等多项待遇。凤凰知音亦与酒店、汽车租赁公司、银行等商家合作，提供里程积累、兑换及各类增值服务。

## 評價

  - [Skytrax評估](../Page/Skytrax.md "wikilink")

<!-- end list -->

  - 2013年：四星級航空公司
  - 2014年：三星級航空公司\[38\]

## 飞行事故

  - 1998年10月28日，一架執行北京－[昆明](../Page/昆明.md "wikilink")－[仰光的](../Page/仰光.md "wikilink")[CA905號航班的](../Page/中國國際航空905號班機劫機事件.md "wikilink")[波音737-300](../Page/波音737.md "wikilink")（註冊編號B-2949）被機長袁斌劫至[台北中正機場](../Page/台灣桃園國際機場.md "wikilink")。
  - 1999年4月1日，一架波音747-2J6BSF（註冊編號B-2446）執行由紐約飛往芝加哥的CA9018號貨運班機，在[奥黑尔国际机场降落之後](../Page/奥黑尔国际机场.md "wikilink")[入侵正在起飛的大韓航空36號班機](../Page/1999年芝加哥奧黑爾機場跑道入侵事故.md "wikilink")（波音747-400）所用的14R跑道，兩機並未相撞。\[39\]
  - 2002年4月15日，一架執行从[北京至](../Page/北京.md "wikilink")[釜山的](../Page/釜山.md "wikilink")[CA129號航班的](../Page/中國國際航空129號班機.md "wikilink")[波音767-200ER客机](../Page/波音767.md "wikilink")（註冊編號B-2552）在[韩国](../Page/韩国.md "wikilink")[釜山坠毁](../Page/釜山.md "wikilink")，机上166人中有128人死亡。这是国航1988年改組之後迄今为止唯一的一次重大空难事故。\[40\]
  - 2007年7月1日，一架執行從[北京至](../Page/北京.md "wikilink")[迪拜的CA](../Page/迪拜.md "wikilink")941號航班的波音767客機（註冊編號B-2553），於下午5時17分在[北京首都國際機場起飛前](../Page/北京首都國際機場.md "wikilink")，起落架突然意外收起，機頭著地，造成機上2人受傷。\[41\]這架波音767-200當時即將退出國航機隊，事後由於蒙皮出現褶皺，不得不提前報廢。
  - 2007年10月27日1时45分，因遭遇雷击，一架从北京飞往[昆明的CA](../Page/昆明.md "wikilink")4174次航班被迫返回[北京首都国际机场](../Page/北京首都国际机场.md "wikilink")，意外未造成人员伤亡，但被业内认定为“严重飞行事故征候”。由于當時国航应急服务欠完善，引发乘客不满。\[42\]
  - 2008年6月20日，一架波音777-200執行從[巴黎至北京的CA](../Page/巴黎.md "wikilink")934航班，因在飞行过程中仪表显示左侧一号发动机滑油压力降低，为确保安全，机组选择了较近机场进行备降，并于北京时间20日上午9时在[莫斯科安全降落](../Page/莫斯科.md "wikilink")。
  - 2008年7月8日，一架從[天津至](../Page/天津.md "wikilink")[香港的CA](../Page/香港.md "wikilink")103航班在[香港國際機場降落後](../Page/香港國際機場.md "wikilink")，在跑道滑行時和跑道上一架[港機工程的車輛碰撞](../Page/香港飛機工程.md "wikilink")，無人受傷。
  - 2009年3月29日，一架從[哈尔滨至北京CA](../Page/哈尔滨.md "wikilink")1666航班一架波音737-800型客机在进港滑行时冲出停机线，左侧机翼撞上廊桥受损。机上载有近百名乘客，幸而未造成人员伤亡。
  - 2009年12月17日，一架從[纽约至北京CA](../Page/纽约.md "wikilink")982航班波音747-400型客机在从纽约[甘迺迪國際機場起飞三个小时后因为增压系统故障被迫折返](../Page/甘迺迪國際機場.md "wikilink")，事件中无人受伤。
  - 2013年5月9日，一架[波音777-300ER](../Page/波音777.md "wikilink")（註冊編號B-2037）執行從北京至巴黎的CA933航班，在飛經[立陶宛上空時發生單側發動機故障](../Page/立陶宛.md "wikilink")，其后備降在[斯德哥爾摩阿蘭達機場](../Page/斯德哥爾摩－阿蘭達機場.md "wikilink")，事故中無人受傷。\[43\]\[44\]事後第四天（5月13日），國航使用一架[波音747-400M客貨機執行北京飛往斯德哥爾摩的CA](../Page/波音747.md "wikilink")911号航班（該航班平時由[空中巴士A330-300執行](../Page/空中巴士A330-300.md "wikilink")），以運載供故障機更換的[GE90-115B引擎](../Page/通用电气GE90发动机.md "wikilink")。\[45\]\[46\]
  - 2015年12月10日，CA1822航班一架飞机在[福州機場準備起飛時右發動機冒火](../Page/福州機場.md "wikilink")，機場消防隊救火時認錯飛機，先把沒著火的福州航空噴了一遍，之後才噴冒火的CA1822。\[47\]
  - 2016年4月9日，一架從北京至[東京的CA](../Page/東京.md "wikilink")181航班在[東京國際機場準備降落時](../Page/東京國際機場.md "wikilink")，機場塔台因地面跑道仍有飛機滑行而要求該航班重飛，而在此之前已經准許飛機降落。機組人員後無視航空管理員指令，將航班按照原本計劃強行降落，幸好前面降落的航班已駛離跑道，未有造成人員傷亡。\[48\]
  - 2017年6月4日晚，一架執行香港至成都CA428航班的[空中巴士A320-200](../Page/空中巴士A320系列.md "wikilink")（註冊編號B-6822）从[香港國際機場起飛後](../Page/香港國際機場.md "wikilink")，在約3000呎高空突然左轉，飛往[大澳上空方向](../Page/大澳.md "wikilink")，由於[大嶼山一帶有不少高峰](../Page/大嶼山.md "wikilink")，空管人員即時指示機師往右飛並爬升，但對話期間出現雜音，在10多秒後客機才改變航道。\[49\]國航回應稱机组对指令存在疑问，因无线电频率繁忙，在与管制确认指令过程中先行转弯，造成短时偏离正常离场轨迹，经管制员提醒后机组及时修正。\[50\]
  - 2018年7月10日晚，一架執行[香港至](../Page/香港.md "wikilink")[大連CA](../Page/大连周水子国际机场.md "wikilink")106航班的[波音737-800](../Page/波音737.md "wikilink")（註冊編號B-5851）客机从[香港國際機場起飛](../Page/香港國際機場.md "wikilink")30分鐘後，在巡航阶段出现座舱高度警告，机组手动释放客舱氧气面罩、宣布紧急状态（Mayday）并在12分鐘內从約10600米高空紧急下降至約3500米高度\[51\]。下降到3000米高度后，机组发现问题并重新接通空调组件，增压恢复正常，在客艙应急氧氣不符合适航条件下又重新爬升至7500米并继续飞往目的地\[52\]，於當天10時29分降落在大連周水子國際機場，無人傷亡\[53\]。中国民航局7月13日回应的初步调查结果显示，事件系副驾驶在駕駛艙內吸[电子烟](../Page/电子烟.md "wikilink")，为防止烟味弥漫到客舱而欲关闭循环空调组件，期间未通知机长且错误关闭相邻空调组件，致使客舱氧气不足、高度告警，事件详细原因仍在调查核实\[54\]。中国国际航空于7月13日晚间宣布，决定对涉事机组停止飞行资格、依法解除劳动合同，对有关管理人员严肃处理，并建议民航局在完成调查程序后吊销机组人员的飞行员驾驶执照\[55\]。中国民航局于7月17日召开的安全会议中通报了有关处理决定，国航被令安全整顿3个月，限期整改；机长的航线运输、商用执照被吊销且不再受理，副驾驶（在座）商用执照吊销且不再受理，副驾驶（观察员）商用执照吊销6个月、停飞24个月，签派员执照吊销24个月；对国航罚款共计5万元；削减国航总部[波音737航班总飞行量](../Page/波音737.md "wikilink")10%，每月削减5400小时\[56\]。
  - 2019年3月4日晚，一架执行从北京至[洛杉矶的CA](../Page/洛杉矶.md "wikilink")983航班的[波音777-300ER](../Page/波音777.md "wikilink")（注册编号B-2040）客机从[北京首都国际机场起飞后](../Page/北京首都国际机场.md "wikilink")，在俄罗斯空域飞行中飞机出现后货舱火警信息。为确保安全，该航机于北京时间3月5日凌晨2点55分备降俄罗斯[乌戈尔尼机场](../Page/烏戈爾尼機場.md "wikilink")，并实施紧急撤离程序，无人伤亡。落地后经检查，飞机货仓正常且无过火痕迹，初步判定为飞机火警信息故障。\[57\]

## 其他事件

  - 2007年4月，一架執行從[北京至](../Page/北京.md "wikilink")[紐約CA](../Page/紐約.md "wikilink")981號航班的客機，在[肯尼迪國際機場降落後](../Page/肯尼迪國際機場.md "wikilink")，機師和機場控制塔出現溝通問題，通讯录音被公布在[Youtube受到关注](../Page/Youtube.md "wikilink")。甘迺迪機場控制塔人員批評國航機師英語水平低，但國航方面指甘迺迪機場控制塔人員沒有使用[國際民航組織通用的](../Page/國際民航組織.md "wikilink")。\[58\]
  - 2011年2月24日晚，一架从[深圳至](../Page/深圳.md "wikilink")[上海的CA](../Page/上海.md "wikilink")1894号班机上由于两名乘客对座位不满意，[乘务员劝说无果后](../Page/乘务员.md "wikilink")，最后导致全体乘客下飞机，重新安检，以致航班取消。这段发生在[机舱和机场](../Page/机舱.md "wikilink")[跑道上的不愉快经历](../Page/跑道.md "wikilink")，已在网上形成了“我有白金卡”事件，一夜间“我有白金卡”成为网络热词。\[59\]后被证实这两名乘客并无白金卡。\[60\]
  - 2014年上映的喜剧电影《[The
    Interview](../Page/采访_\(电影\).md "wikilink")》（又称《刺杀金正恩》）里面出现的一架披着国航涂装747客机，虽然机身上书“郑州航空”和“ZHANGZHOU
    AIR”，但从机尾清晰的注册号来看，这是明显来自国航机队的B-2443，“ZHANGZHOU
    AIR”前面的拼音和郑州对不上，但是字体形式却沿袭AIR
    CHINA。
  - 2014年12月17日，一架從[重慶飛往](../Page/重慶.md "wikilink")[香港CA](../Page/香港.md "wikilink")433號航班起飛後不久，由于两名乘客不滿後排一名兩歲大嬰兒太嘈吵，繼而發生糾紛和打架，險些要折返重慶江北國際機場。\[61\]\[62\]
  - 2016年9月，国航机上杂志《中国之翼》在《伦敦
    礼帽下的风度与坚硬》文章中的出行安全提示部分，出现了不恰当的文字表述。文章中出现了提醒旅客在访问伦敦“有些印巴聚集区和黑人聚集区”时要多加小心的语句，被指涉嫌[种族歧视](../Page/种族歧视.md "wikilink")，引发争议\[63\]。为此，国航相关负责人表示，《中国之翼》刊登的文章不代表国航的观点。而国航相关负责人在发现此问题后，国航立即在所有航班中撤下了该杂志，并要求杂志社认真吸取教训，加强内容审核，避免类似问题再次发生\[64\]。此事件发生后，[中华人民共和国外交部发言人](../Page/中华人民共和国外交部发言人.md "wikilink")[华春莹在例行记者会上也提及此事](../Page/华春莹.md "wikilink")，表示“中国政府一贯主张和坚持各民族一律平等，坚决反对一切形式的种族歧视。这一立场是一贯、明确的\[65\]”。
  - 2018年3月22日上午，一架國航[波音737客機執飛](../Page/波音737.md "wikilink")[天津濱海至香港的CA](../Page/天津滨海国际机场.md "wikilink")103號班機，抵港後被發現機頭雷達罩損毀破洞，懷疑在飛行途中撞上雀鳥。該破洞面積約1平方米，沾有血跡，客機安全降落，無人受傷\[66\]。
  - 2018年4月15日，一架從[长沙至北京的](../Page/长沙.md "wikilink")[CA1350航班在飞至](../Page/中国国际航空1350号班机劫持事件.md "wikilink")[郑州上空时机组宣布被劫持并将应答机设置为](../Page/郑州.md "wikilink")7500，其后备降在[郑州新郑机场](../Page/郑州新郑机场.md "wikilink")。初步了解为“公共安全原因”备降。\[67\]后据调查，系乘客用[钢笔挟持乘务员导致班机备降](../Page/钢笔.md "wikilink")\[68\]。
  - 2018年7月26日，国航一架由[巴黎飞往](../Page/戴高乐机场.md "wikilink")[北京的CA](../Page/北京首都国际机场.md "wikilink")876號班機，其AOC接到疑似恐怖信息。为确保班机安全，该班机返航巴黎并安全落地\[69\]。后经查实，疑似的恐怖信息是由一名未登机的澳大利亚籍的乘客发出，该旅客后被警方拘留，而机场所属的博比尼法院检察官就此事件立案展开调查。据检察官称，涉事乘客是因为他没有及时登上飞机，同时又打电话给航空公司，客服误将该乘客所称“他航站楼被挡在了警方的警戒线后，警察正在调查机场一个被遗弃的包裹，他说这个包裹里可能有炸弹。”听成了“航班上有一枚炸弹”。但经过调查没有发现任何起诉他的理由，随后警方将他无罪释放\[70\]。

## 註釋

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  -
## 外部链接

  - [中国国际航空](http://www.airchina.com)

  -
  -
  -
## 参见

  - [星空联盟](../Page/星空联盟.md "wikilink")
  - [中国国际货运航空](../Page/中国国际货运航空.md "wikilink")
  - [国泰航空](../Page/国泰航空.md "wikilink")
  - [CA129航班事故](../Page/中国国际航空129号班机空难.md "wikilink")

{{-}}

[Category:总部位于北京市的中华人民共和国中央企业](../Category/总部位于北京市的中华人民共和国中央企业.md "wikilink")
[中国国际航空](../Category/中国国际航空.md "wikilink")
[Category:中国航空公司](../Category/中国航空公司.md "wikilink") [China,
PR](../Category/国家航空公司.md "wikilink")
[Category:香港上市綜合企業公司](../Category/香港上市綜合企業公司.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")

1.

2.

3.

4.
5.

6.

7.

8.

9.  [Air China International - Seat
    Maps](http://www.airchina.de/en/managemytrip/seatmaps.html)
    Airchina.com Retrieved 2013-08-27

10. [Air China fleet list at ch-aviation.ch.
    Retrieved 2009-12-18.](http://www.ch-aviation.ch/airlinepage.php)

11.

12.

13.

14.

15.

16.

17.

18.
19.

20.

21.

22. <http://news.163.com/17/1122/12/D3RJE62O000181O6.html> 国航暂停北京至平壤航线
    复航要看市场情况

23. [停飛半年…北韓中國復航 月底加碼包機](https://www.worldjournal.com/5603132/)

24. [Partnerships](http://www.airchina.com/gr/en/about_us/airline_partners/airline_partners.shtml)
     *airchina.com* Retrieved 2013-09-07

25.

26.

27. [Air China, EVA Airways to join hands on code sharing
    services](http://www.chinaknowledge.com/Newswires/News_Detail.aspx?type=1&cat=RND&NewsID=%2038213),
    ChinaKnowledge, Oct. 29, 2010

28.

29.

30.

31.

32.

33.

34.

35.

36.

37.

38.

39.

40. 新华网,[中国国际航空公司4.15釜山空难调查结果公布](http://news.xinhuanet.com/newscenter/2005-05/08/content_2930455.htm)

41. 新华网,[国航一架客机起落架出现故障
    机头触地](http://www.bj.xinhuanet.com/bjpd_tpk/2007-07/02/content_10452446.htm)


42. 陈鹏，[新华时评：国航应急服务缺位令人寒心](http://news.xinhuanet.com/newscenter/2007-11/02/content_6999199.htm)，新华网

43.

44.

45. [视频：援兵驾到——实拍国航CA911波音747霸气着陆瑞典斯德哥尔摩阿兰达国际机场](http://v.youku.com/v_show/id_XNTU2OTg4NDUy.html)，优酷网

46.

47. [网曝消防队扑火时乌龙扑错飞机
    一片雪白](http://news.haiwainet.cn/n/2015/1210/c3541092-29439889.html)，360新聞

48. [無視空管指令　國航客機自行降落羽田機場](http://news.mingpao.com/ins/instantnews/web_tc/article/20160412/s00005/1460432626940),明報

49.

50.

51. [国航不安全事件背后：飞机驾驶舱里到底能不能抽烟？_新浪财经_新浪网](http://finance.sina.com.cn/chanjing/cyxw/2018-07-13/doc-ihfhfwmu3718344.shtml)

52. [热点 |
    果然！民航局回应国航“不安全事件”，副驾驶飞机上吸电子烟！|国航|飞机|民航局_新浪新闻](http://news.sina.com.cn/o/2018-07-14/doc-ihfhfwmv0636239.shtml)

53.

54.

55. [国航：停止CA106航班机组飞行资格、并解除劳动合同-中新网](http://www.chinanews.com/sh/2018/07-13/8566449.shtml)

56.

57.

58.

59.

60.

61.

62.

63.

64.

65.

66.

67. [国航CA1350航班备降
    应答机代码A7500意指劫机](https://3g.163.com/news/article/DFEFLA0D000181O6.html)

68.

69.

70.