## 文学

  - I0　[文学理论](../Page/文学.md "wikilink")

:\*I01 [文艺美学](../Page/文艺美学.md "wikilink")

:\*I02 文学理论的基本问题

:\*I03 [文艺工作者](../Page/文艺工作者.md "wikilink")

:\*I04 [文学创作论](../Page/文学创作论.md "wikilink")

:\*I05 各体文学理论和创作方法

:\*I06
[文学评论](../Page/文学评论.md "wikilink")、[文学欣赏](../Page/文学欣赏.md "wikilink")

  - I1　[世界文学](../Page/世界文学.md "wikilink")

::\*I1-61 文学词典

::\*I106 各体文学评论与研究

::\*I109
[文学史](../Page/文学史.md "wikilink")、[文学思想史](../Page/文学思想史.md "wikilink")

:\*I11 [作品集](../Page/作品集.md "wikilink")

:\*I12 [诗歌集](../Page/诗歌集.md "wikilink")

:\*I13 [戏剧](../Page/戏剧.md "wikilink")

:\*I14 [小说集](../Page/小说集.md "wikilink")

:\*I15 [报告文学集](../Page/报告文学集.md "wikilink")

:\*I16 [散文集](../Page/散文集.md "wikilink")

:\*I17 [民间文学集](../Page/民间文学集.md "wikilink")

:\*I18 [儿童文学](../Page/儿童文学.md "wikilink")

  - I2　[中国文学](../Page/中国文学.md "wikilink")

::\*I2-49 中国文学普及读物

::\*I2-61 中国文学词典

::\*I2-62 中国文学手册、指南

::\*I200 [方针政策及其阐述](../Page/方针政策及其阐述.md "wikilink")

::\*I206 [文学评价和研究](../Page/文学评价.md "wikilink")

:::\*I206.09 [文学批评史](../Page/文学批评史.md "wikilink")

::\*I207 各体[文学评论和研究](../Page/文学评论.md "wikilink")

:::\*I207.2
[诗歌](../Page/诗歌.md "wikilink")、[韵文](../Page/韵文.md "wikilink")

:::\*I207.3 [戏剧](../Page/戏剧.md "wikilink")

:::\*I207.4 [小说](../Page/小说.md "wikilink")

:::::\*I207.411 《[红楼梦](../Page/红楼梦.md "wikilink")》评论与研究

:::::\*I207.412 《[水浒](../Page/水浒.md "wikilink")》的评论与研究

:::::\*I207.413 《[三国演义](../Page/三国演义.md "wikilink")》评论与研究

:::\*I207.5 [报告文学](../Page/报告文学.md "wikilink")

:::\*I207.6 [散文](../Page/散文.md "wikilink")

:::\*I207.7 [民间文学](../Page/民间文学.md "wikilink")

:::\*I207.8 [儿童文学](../Page/儿童文学.md "wikilink")

::\*I209
[文学史](../Page/文学史.md "wikilink")、[文学思想史](../Page/文学思想史.md "wikilink")

:\*I21 作品集

::\*I210 [鲁迅作品及研究](../Page/鲁迅.md "wikilink")

:::\*I210.1 全集

:::\*I210.2 选集、文集、语录

:::\*I210.4 [杂文](../Page/杂文.md "wikilink")、散文

:::\*I210.5 [诗](../Page/诗.md "wikilink")

:::\*I210.6 [小说](../Page/小说.md "wikilink")

:::\*I210.7
[日记](../Page/日记.md "wikilink")、[书信](../Page/书信.md "wikilink")

::::\*I210.96 鲁迅思想的学习和研究

::::\*I210.97 著作研究

::\*I211 作品综合集

::\*I218 地方作品综合集

::\*I219 群众文艺演唱材料

:\*I22 [诗歌](../Page/诗歌.md "wikilink")、[韵文](../Page/韵文.md "wikilink")

::\*I222 古代作品

::\*I226 现代作品（1911年－1949年）

::\*I227 当代作品（1949年－）

:::\*I227.2 抒情诗

:\*I23 [戏剧](../Page/戏剧.md "wikilink")

::\*I230 综合集

::\*I232 [京剧](../Page/京剧.md "wikilink")

::\*I233
[歌剧](../Page/歌剧.md "wikilink")、[歌舞剧](../Page/歌舞剧.md "wikilink")、[秧歌剧](../Page/秧歌剧.md "wikilink")

::\*I234 [话剧](../Page/话剧.md "wikilink")

::\*I235
[电影](../Page/电影.md "wikilink")、[电视](../Page/电视.md "wikilink")、[广播](../Page/广播.md "wikilink")[剧本](../Page/剧本.md "wikilink")

::\*I236 [地方剧](../Page/地方剧.md "wikilink")

::\*I237 [古代戏曲](../Page/古代戏曲.md "wikilink")

::\*I239 [曲艺](../Page/曲艺.md "wikilink")

:::\*I239.8 [评书](../Page/评书.md "wikilink")

:\*I24 [小说](../Page/小说.md "wikilink")

::\*I242 古代至近代作品（－1919年）

:::\*I242.1 [笔记小说](../Page/笔记小说.md "wikilink")

:::\*I242.3
[话本](../Page/话本.md "wikilink")、[评话](../Page/评话.md "wikilink")

:::\*I242.4 [章回小说](../Page/章回小说.md "wikilink")

:::\*I242.7 [短篇小说](../Page/短篇小说.md "wikilink")

::\*I246 现代作品（1919年－1949年）

:::\*I246.1 [笔记小说](../Page/笔记小说.md "wikilink")

:::\*I246.3 [评话](../Page/评话.md "wikilink")

:::\*I246.4 [章回小说](../Page/章回小说.md "wikilink")

:::\*I246.5 新体长篇、中篇小说

:::\*I246.7 [新体短篇小说](../Page/新体短篇小说.md "wikilink")

:::\*I246.8 [故事](../Page/故事.md "wikilink")

::\*I247 当代作品（1949年－）

:::\*I247.4 [章回小说](../Page/章回小说.md "wikilink")

:::\*I247.5 新体长、中篇小说

::::\*I247.51 [革命斗争小说](../Page/革命斗争小说.md "wikilink")

::::\*I247.52 [军事小说](../Page/军事小说.md "wikilink")

::::\*I247.53 [史传小说](../Page/史传小说.md "wikilink")

::::\*I247.54 经济、政治建设小说

::::\*I247.55 [科幻小说](../Page/科幻小说.md "wikilink")

::::\*I247.56 [惊险推理小说](../Page/惊险推理小说.md "wikilink")

::::\*I247.57 [社会言情小说](../Page/社会言情小说.md "wikilink")

::::\*I247.58 [武侠小说](../Page/武侠小说.md "wikilink")

::::\*I247.59 其他题材小说

:::\*I247.7 新体[短篇小说](../Page/短篇小说.md "wikilink")

:::\*I247.8
[故事](../Page/故事.md "wikilink")、[微型小说](../Page/微型小说.md "wikilink")

:\*I25 [报告文学](../Page/报告文学.md "wikilink")

::\*I251 [回忆录](../Page/回忆录.md "wikilink")

::\*I252
[厂史](../Page/厂史.md "wikilink")、[村史](../Page/村史.md "wikilink")、[家史](../Page/家史.md "wikilink")

::\*I253 [通讯](../Page/通讯.md "wikilink")、[特写](../Page/特写.md "wikilink")

:::\*I253.1
[政治](../Page/政治.md "wikilink")、[法律](../Page/法律.md "wikilink")

:::\*I253.2
[军事](../Page/军事.md "wikilink")、[国防建设](../Page/国防.md "wikilink")

:::\*I253.3 [经济](../Page/经济.md "wikilink")

:::\*I253.4
[文化](../Page/文化.md "wikilink")、[教育](../Page/教育.md "wikilink")、[体育](../Page/体育.md "wikilink")

:::\*I253.5
[文学](../Page/文学.md "wikilink")、[艺术](../Page/艺术.md "wikilink")

:::\*I253.6
[科学与](../Page/科学.md "wikilink")[工程技术](../Page/工程技术.md "wikilink")

:::\*I253.7
[社会生活与](../Page/社会生活.md "wikilink")[社会问题](../Page/社会问题.md "wikilink")

:::\*I253.9 其他

:\*I26 [散文](../Page/散文.md "wikilink")

::\*I262 古代作品（公元前21世纪－公元前475年）

::\*I263 [封建社会作品](../Page/封建社会.md "wikilink")（公元前475年－1840年）

::\*I265 近代作品（1840年－1919年）

::\*I266 现代作品（1919年－1949年）

:::\*I266.1
[随笔](../Page/随笔.md "wikilink")、[杂文](../Page/杂文.md "wikilink")

:::\*I266.3 [小品文](../Page/小品文.md "wikilink")

:::\*I266.4 [游记](../Page/游记.md "wikilink")

:::\*I266.5
[书信](../Page/书信.md "wikilink")、[日记](../Page/日记.md "wikilink")

::\*I267 当代作品（1949年－）

::\*I269 [杂著](../Page/杂著.md "wikilink")

:\*I27 [民间文学](../Page/民间文学.md "wikilink")

::\*I276 古代至现代作品（－1949年）

::\*I277 当代作品（1949年－）

:::\*I277.2 [民间歌谣](../Page/民间歌谣.md "wikilink")

:::\*I277.3
[民间故事](../Page/民间故事.md "wikilink")、[传说](../Page/传说.md "wikilink")

:::\*I277.4 [寓言](../Page/寓言.md "wikilink")

:::\*I277.5 [神话](../Page/神话.md "wikilink")

:::\*I277.7 [谚语](../Page/谚语.md "wikilink")

:::\*I277.8
[谜语](../Page/谜语.md "wikilink")、[笑话](../Page/笑话.md "wikilink")、[幽默](../Page/幽默.md "wikilink")

:\*I28 [儿童文学](../Page/儿童文学.md "wikilink")

::\*I286 古代至现代作品（－1949年）

::\*I287 当代作品（1949年－）

:::\*I287.2
[诗歌](../Page/诗歌.md "wikilink")、[童谣](../Page/童谣.md "wikilink")

:::\*I287.3
[儿童戏剧](../Page/儿童戏剧.md "wikilink")、[歌舞剧](../Page/歌舞剧.md "wikilink")

:::\*I287.4 [儿童小说](../Page/儿童小说.md "wikilink")

::::\*I287.45 长篇、中篇小说

::::\*I287.47
[短篇小说](../Page/短篇小说.md "wikilink")、[微型小说](../Page/微型小说.md "wikilink")

:::\*I287.5 [儿童故事](../Page/儿童故事.md "wikilink")

:::\*I287.6 [儿童散文](../Page/儿童散文.md "wikilink")

:::\*I287.7
[童话](../Page/童话.md "wikilink")、[寓言](../Page/寓言.md "wikilink")

:::\*I287.8 [图画故事](../Page/图画故事.md "wikilink")

:\*I29 [少数民族文学](../Page/少数民族文学.md "wikilink")

::\*I299 [宗教文学](../Page/宗教文学.md "wikilink")

  - I3 [亚洲文学](../Page/亚洲文学.md "wikilink")

::\*I313 [日本文学](../Page/日本文学.md "wikilink")

:::\*I313.4 [小说](../Page/小说.md "wikilink")

::::\*I313.45 [现代小说](../Page/现代小说.md "wikilink")

::\*I312 [朝鲜文学](../Page/朝鲜文学.md "wikilink")

::\*I333 [越南文学](../Page/越南文学.md "wikilink")

::\*I336 [泰国文学](../Page/泰国文学.md "wikilink")

::\*I339 [新加坡文学](../Page/新加坡文学.md "wikilink")

::\*I351 [印度文学](../Page/印度文学.md "wikilink")

:\*I37 西亚（西南亚）文学

::\*I371 阿拉伯地区文学

  - I4 [非洲文学](../Page/非洲文学.md "wikilink")
  - I5 [欧洲文学](../Page/欧洲文学.md "wikilink")

:\*I51 东欧、中欧文学

::\*I512 前[苏联文学](../Page/苏联文学.md "wikilink")

::\*I513 [波兰文学](../Page/波兰文学.md "wikilink")

::\*I514 [捷克文学](../Page/捷克文学.md "wikilink")

::\*I515 [匈牙利文学](../Page/匈牙利文学.md "wikilink")

::\*I516 [德国文学](../Page/德国文学.md "wikilink")

:::\*I516.4 小说

::\*I561 [英国文学](../Page/英国文学.md "wikilink")

::\*I521 [奥地利文学](../Page/奥地利文学.md "wikilink")

::\*I532 [瑞典文学](../Page/瑞典文学.md "wikilink")

::\*I533 [挪威文学](../Page/挪威文学.md "wikilink")

::\*I534 [丹麦文学](../Page/丹麦文学.md "wikilink")

::\*I545 [希腊文学](../Page/希腊文学.md "wikilink")

::\*I546 [意大利文学](../Page/意大利文学.md "wikilink")

::\*I562 [爱尔兰文学](../Page/爱尔兰文学.md "wikilink")

::\*I565 [法国文学](../Page/法国文学.md "wikilink")

  - I6 [大洋洲文学](../Page/大洋洲文学.md "wikilink")

::\*I611 [澳大利亚文学](../Page/澳大利亚文学.md "wikilink")

  - I7 [美洲文学](../Page/美洲文学.md "wikilink")

::\*I711 [加拿大文学](../Page/加拿大文学.md "wikilink")

::\*I712 [美国文学](../Page/美国文学.md "wikilink")

::\*I777 [巴西文学](../Page/巴西文学.md "wikilink")

::\*I775 [哥伦比亚文学](../Page/哥伦比亚文学.md "wikilink")

::\*I783 [阿根廷文学](../Page/阿根廷文学.md "wikilink")

-----

[Category:中国图书馆图书分类法](../Category/中国图书馆图书分类法.md "wikilink")