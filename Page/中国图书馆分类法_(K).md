## 历史、地理

  - K0 [史学理论](../Page/史学.md "wikilink")

:\*K03 史学专论

:\*K09 [史学史](../Page/史学史.md "wikilink")

  - K1 [世界史](../Page/世界史.md "wikilink")

:\*K10 [通史](../Page/通史.md "wikilink")

::\*K101 [革命史](../Page/革命史.md "wikilink")

::\*K103 [文化史](../Page/文化史.md "wikilink")

::\*K105 [历史事件](../Page/历史事件.md "wikilink")

::\*K106 [史料](../Page/史料.md "wikilink")

::\*K107 研究、考订、评论

::\*K108 [年表](../Page/年表.md "wikilink")

::\*K109 世界历史普及读物

:\*K11 [上古史](../Page/上古史.md "wikilink")

:\*K12 [古代史](../Page/古代史.md "wikilink")

:\*K13 [中世纪史](../Page/中世纪史.md "wikilink")

:\*K14 [近代史](../Page/近代史.md "wikilink")（1640年－1917年）

:\*K15 [现代史](../Page/现代史.md "wikilink")（1917年－）

::\*K152 [第二次世界大战](../Page/第二次世界大战.md "wikilink")

:\*K18 [民族史志](../Page/民族史.md "wikilink")

  - K2 [中国历史](../Page/中国历史.md "wikilink")

:\*K20 [通史](../Page/通史.md "wikilink")

::\*K201 [革命史](../Page/革命史.md "wikilink")

::\*K203 [文化史](../Page/文化史.md "wikilink")

::\*K204 古代史籍

:::\*K204.2 [史记](../Page/史记.md "wikilink")

::\*K205 [历史事件](../Page/历史事件.md "wikilink")

::\*K206 [史料](../Page/史料.md "wikilink")

:::\*K206.5
[诏令](../Page/诏令.md "wikilink")、[奏议](../Page/奏议.md "wikilink")

:::\*K206.6
[笔记](../Page/笔记.md "wikilink")、[掌故](../Page/掌故.md "wikilink")、[回忆录](../Page/回忆录.md "wikilink")

::\*K207 历史研究、考订、评论

::\*K208 [年表](../Page/年表.md "wikilink")

::\*K209 [中国历史普及读物](../Page/中国历史.md "wikilink")

:\*K21 [原始社会](../Page/原始社会.md "wikilink")

:\*K22 [奴隶社会](../Page/奴隶社会.md "wikilink")（公元前21世纪－公元前475年）

:\*K23 [封建社会](../Page/封建社会.md "wikilink")（公元前475年－公元581年）

:\*K24 隋唐至清前朝（581年－1840年）

::\*K241 [隋朝](../Page/隋朝.md "wikilink")

::\*K242 [唐朝](../Page/唐朝.md "wikilink")

::\*K243 [五代](../Page/五代.md "wikilink")、[十国](../Page/十国.md "wikilink")

::\*K244 [北宋](../Page/北宋.md "wikilink")

::\*K245 [南宋](../Page/南宋.md "wikilink")

::\*K246 辽、金

::\*K247 [元朝](../Page/元朝.md "wikilink")

::\*K248 [明朝](../Page/明朝.md "wikilink")

::\*K249 [清朝](../Page/清朝.md "wikilink")

:\*K25 [半殖民地半封建社会](../Page/半殖民地半封建社会.md "wikilink")（1840年－1949年）

::\*K258 [中华民国早期](../Page/中华民国.md "wikilink")

:\*K26 [新民主主义革命时期](../Page/新民主主义革命.md "wikilink")（1919年－1949年）

::\*K261
[五四运动和](../Page/五四运动.md "wikilink")[中国共产党成立后](../Page/中国共产党.md "wikilink")（1919年－1924年）

::\*K262 [第一次国内革命战争时期](../Page/第一次国内革命战争.md "wikilink")（1924年－1927年）

::\*K263
[第二次国内革命战争](../Page/第二次国内革命战争.md "wikilink")（[土地革命战争](../Page/土地革命战争.md "wikilink")）时期（1927年－1937年）

::\*K264
[日本帝国主义入侵及](../Page/日本帝国主义.md "wikilink")[全国抗日民主运动](../Page/全国抗日民主运动.md "wikilink")

::\*K265 [抗日战争时期](../Page/抗日战争.md "wikilink")

::\*K266
[第三次国内革命](../Page/第三次国内革命战争.md "wikilink")（[国共内战](../Page/国共内战.md "wikilink")）时期（1945年－1949年）

::\*K269 [解放区的革命建设和发展](../Page/解放区.md "wikilink")

:\*K27
[中华人民共和国](../Page/中华人民共和国.md "wikilink")：[社会主义革命和](../Page/社会主义革命.md "wikilink")[社会主义建设时期](../Page/社会主义建设.md "wikilink")（1949年－）

:\*K28 民族史志

:\*K29 地方史志

::::\*K296.58 [香港](../Page/香港.md "wikilink")

::::\*K296.59 [澳门](../Page/澳门.md "wikilink")

:::\*K295.8 [台湾](../Page/台湾.md "wikilink")

  - K3 [亚洲史](../Page/亚洲史.md "wikilink")
  - K4 [非洲史](../Page/非洲史.md "wikilink")
  - K5 [欧洲史](../Page/欧洲史.md "wikilink")
  - K6 [大洋洲史](../Page/大洋洲史.md "wikilink")
  - K7 [美洲史](../Page/美洲史.md "wikilink")
  - K81 [传记](../Page/传记.md "wikilink")

:\*K810 传记研究与编写

::\*K810.1 传记写作法

::\*K810.2 [谱系学](../Page/谱系学.md "wikilink")

:\*K811 世界人物传记

:::\*K811-61
[人名词典](../Page/人名.md "wikilink")、[姓氏词典](../Page/姓氏.md "wikilink")

::::\*K811-64
[生卒年表](../Page/生卒年表.md "wikilink")、[疑年表](../Page/疑年表.md "wikilink")、[年谱](../Page/年谱.md "wikilink")

:\*K812 人物总传：按时代分

:\*K813 人物总传：按地区分

:\*K815 人物总传：按学科分

:\*K82 中国人物传记

::::\*K82-61 人名词典、姓氏词典

::::\*K82-64 生卒年表、疑年表、年谱

::\*K820 人物总传：按时代分

:::\*K820.8 人物总传：按地区分

::\*K825 人物传记：按学科分

:::\*K825.1 [哲学](../Page/哲学.md "wikilink")

::::\*K825.19 [法律](../Page/法律.md "wikilink")

:::\*K825.2 [军事](../Page/军事.md "wikilink")

:::\*K825.3 [经济](../Page/经济.md "wikilink")

::::\*K825.31 [经济学家](../Page/经济学家.md "wikilink")

::::\*K825.34
[金融](../Page/金融.md "wikilink")、[保险](../Page/保险.md "wikilink")

::::\*K825.38 [企业家](../Page/企业家.md "wikilink")

:::\*K825.4
[文化](../Page/文化.md "wikilink")、[教育](../Page/教育.md "wikilink")、[体育](../Page/体育.md "wikilink")

::::\*K825.42
[新闻](../Page/新闻.md "wikilink")、[广播](../Page/广播.md "wikilink")、[出版](../Page/出版.md "wikilink")

::::\*K825.46 [教育](../Page/教育.md "wikilink")

::::\*K825.47 [体育](../Page/体育.md "wikilink")

:::\*K825.5
[语言](../Page/语言.md "wikilink")、[文字](../Page/文字.md "wikilink")

:::\*K825.6 [文学](../Page/文学.md "wikilink")

:::\*K825.7 [艺术](../Page/艺术.md "wikilink")

::::\*K825.72 [美术](../Page/美术.md "wikilink")

::::\*K825.76
[音乐](../Page/音乐.md "wikilink")、[舞蹈](../Page/舞蹈.md "wikilink")

::::\*K825.78
[戏剧](../Page/戏剧.md "wikilink")、[电影](../Page/电影.md "wikilink")、[电视](../Page/电视.md "wikilink")

:::\*K825.8
[历史](../Page/历史.md "wikilink")、[地理](../Page/地理.md "wikilink")

:::\*K826.1 自然技术、[工程技术](../Page/工程技术.md "wikilink")

::::\*K826.11 数理科学

::::\*K826.13 [化学](../Page/化学.md "wikilink")

::::\*K826.14
[天文学](../Page/天文学.md "wikilink")、[地球科学](../Page/地球科学.md "wikilink")

::::\*K826.15 [生物学](../Page/生物学.md "wikilink")

::::\*K826.16 [工程技术](../Page/工程技术.md "wikilink")

:::\*K826.2
[医学](../Page/医学.md "wikilink")、[卫生](../Page/卫生.md "wikilink")

:::\*K826.3
[农业](../Page/农业.md "wikilink")、[林业](../Page/林业.md "wikilink")、[畜牧业](../Page/畜牧业.md "wikilink")、[渔业](../Page/渔业.md "wikilink")

::\*K827 [社会政治人物传](../Page/社会政治人物.md "wikilink")

::\*K828 社会各界人物

:::\*K828.1
[工人](../Page/工人.md "wikilink")、[农民](../Page/农民.md "wikilink")

:::\*K828.2 [公务员](../Page/公务员.md "wikilink")

:::\*K828.3 [个体劳动者](../Page/个体劳动者.md "wikilink")

:::\*K828.4
[青年](../Page/青年.md "wikilink")、[学生](../Page/学生.md "wikilink")

:::\*K828.5 [妇女](../Page/妇女.md "wikilink")

:::\*K828.6 [残疾人](../Page/残疾人.md "wikilink")

:::\*K828.8 [华侨](../Page/华侨.md "wikilink")

:::\*K828.9 其他人物

::\*K833 亚洲人物传记

::\*K834 非洲人物传记

::\*K835 欧洲人物传记

::\*K836 大洋洲人物传记

::\*K837 美洲人物传记

:\*K85 [文物](../Page/文物.md "wikilink")[考古](../Page/考古.md "wikilink")

::\*K852 [古文献学](../Page/古文献学.md "wikilink")

::\*K853 [纹章学](../Page/纹章学.md "wikilink")

::\*K854 考古方法

:\*K86 世界文物考古

:\*K87 中国文物考古

::\*K873 [出土文物图录](../Page/出土文物.md "wikilink")

::\*K875 各种用品器物

::\*K876 各种材料器物

::\*K877 [古书契](../Page/古书契.md "wikilink")

::\*K878 [遗址](../Page/遗址.md "wikilink")

::\*K879 [美术考古](../Page/美术考古.md "wikilink")

:\*K89 [风俗习惯](../Page/风俗习惯.md "wikilink")

::\*K890 [民俗学](../Page/民俗学.md "wikilink")

::\*K891 世界风俗习惯

::\*K892 中国风俗习惯

::::\*K892.21
[生育](../Page/生育.md "wikilink")、[诞辰](../Page/诞辰.md "wikilink")

::::\*K892.26
[礼仪](../Page/礼仪.md "wikilink")、[礼节](../Page/礼节.md "wikilink")

:::\*K892.9 [古代礼制](../Page/古代礼制.md "wikilink")

  - K9 [地理](../Page/地理.md "wikilink")

:\*K90 [地理学](../Page/地理学.md "wikilink")

:\*K91 世界地理

:\*K92 [中国地理](../Page/中国地理.md "wikilink")

::\*K928 专类地理

:::\*K928.1
[疆界](../Page/疆界.md "wikilink")（包括[海疆](../Page/海疆.md "wikilink")）

:::\*K928.2 [政治区划](../Page/政治区划.md "wikilink")

:::\*K928.3 [山](../Page/山.md "wikilink")

:::\*K928.4 [水](../Page/水.md "wikilink")

:::\*K928.5
[城市](../Page/城市.md "wikilink")、[村落](../Page/村落.md "wikilink")

:::\*K928.6 [历史地理](../Page/历史地理.md "wikilink")

:::\*K928.7 [名胜古迹](../Page/名胜古迹.md "wikilink")

::::\*K928.70 各地名胜古迹汇编

:::\*K928.8 现代著名建筑

:::\*K928.9
[旅行](../Page/旅行.md "wikilink")、[游记](../Page/游记.md "wikilink")

:\*K93 [亚洲地理](../Page/亚洲地理.md "wikilink")

:\*K94 [非洲地理](../Page/非洲地理.md "wikilink")

:\*K95 [欧洲地理](../Page/欧洲地理.md "wikilink")

:\*K96 [大洋洲地理](../Page/大洋洲地理.md "wikilink")

:\*K97 [美洲地理](../Page/美洲地理.md "wikilink")

:\*K99 [地图](../Page/地图.md "wikilink")

::\*K991 世界

::\*K992 中国

::\*K993 [亚洲地图](../Page/亚洲地图.md "wikilink")

::\*K994 [非洲地图](../Page/非洲地图.md "wikilink")

::\*K995 [欧洲地图](../Page/欧洲地图.md "wikilink")

::\*K996 [大洋洲地图](../Page/大洋洲地图.md "wikilink")

::\*K997 [美洲地图](../Page/美洲地图.md "wikilink")

-----

[Category:中国图书馆图书分类法](../Category/中国图书馆图书分类法.md "wikilink")