## 医药、卫生

:\*R-61 医药卫生词典

:\*R-62 医药卫生手册

  - R1
    [预防医学](../Page/预防医学.md "wikilink")、[卫生学](../Page/卫生学.md "wikilink")

:\*R11 卫生基础科学

:\*R12
[环境卫生](../Page/环境卫生.md "wikilink")、[环境医学](../Page/环境医学.md "wikilink")

:\*R13 [劳动卫生](../Page/劳动卫生.md "wikilink")

:\*R14
[放射卫生](../Page/放射卫生.md "wikilink")、[战备卫生](../Page/战备卫生.md "wikilink")

:\*R15
[营养卫生](../Page/营养卫生.md "wikilink")、[食品卫生](../Page/食品卫生.md "wikilink")

:\*R16 [个人卫生](../Page/个人卫生.md "wikilink")

::\*R161 一般保健法

::\*R163 生活制度与卫生

::\*R167 [性卫生](../Page/性卫生.md "wikilink")

::\*R169 [计划生育与卫生](../Page/计划生育.md "wikilink")

:\*R17 妇幼、儿童、少年卫生

:\*R18 [流行病学与防疫](../Page/流行病学.md "wikilink")

:\*R19
[保健组织与事业](../Page/保健组织.md "wikilink")（[卫生事业管理](../Page/卫生事业管理.md "wikilink")）

  - R2 [中医](../Page/中医.md "wikilink")

:\*R21 中医预防、卫生学

::\*R212 [养生](../Page/养生.md "wikilink")

:\*R22 中医基础理论

:\*R24 [中医临床学](../Page/中医临床学.md "wikilink")

::\*R241 [中医诊断学](../Page/中医诊断学.md "wikilink")

::\*R243 [中草药治疗学](../Page/中草药治疗学.md "wikilink")

::\*R244
[外治法](../Page/外治法.md "wikilink")（[物理疗法](../Page/物理疗法.md "wikilink")）

::\*R245 [针灸疗法](../Page/针灸.md "wikilink")

::\*R247 其他疗法

:\*R25 [中医内科](../Page/中医内科.md "wikilink")

:\*R26 [中医外科](../Page/中医外科.md "wikilink")

::\*R271 [中医妇产科](../Page/中医妇产科.md "wikilink")

::\*R272 [中医儿科](../Page/中医儿科.md "wikilink")

::\*R273 [中医肿瘤科](../Page/中医肿瘤科.md "wikilink")

::\*R274 [中医骨伤科](../Page/中医骨伤科.md "wikilink")

::\*R275 [中医皮科](../Page/中医皮科.md "wikilink")

::\*R276 [中医五官科](../Page/中医五官科.md "wikilink")

::\*R277 中医其他学科

::\*R278 [中医急症学](../Page/中医急症学.md "wikilink")

:\*R28 [中药学](../Page/中药学.md "wikilink")、[方剂学](../Page/方剂学.md "wikilink")

::\*R289 [方剂学](../Page/方剂学.md "wikilink")

::\*R2-62 中医各科手册

  - R3 [基础医学](../Page/基础医学.md "wikilink")

:\*R31 医用一般科学

:\*R32 [人体形态学](../Page/人体形态学.md "wikilink")

::\*R322 [系统解剖学](../Page/系统解剖学.md "wikilink")

:\*R33 [人体生理学](../Page/人体生理学.md "wikilink")

::\*R338 [神经生理](../Page/神经生理.md "wikilink")

::\*R339 [感觉器官生理学](../Page/感觉器官生理学.md "wikilink")

:\*R36 [病理学](../Page/病理学.md "wikilink")

:\*R37
[医学微生物学](../Page/医学微生物学.md "wikilink")（[病原细菌学](../Page/病原细菌学.md "wikilink")、[病原微生物学](../Page/病原微生物学.md "wikilink")）

:\*R38 [医学寄生虫学](../Page/医学寄生虫学.md "wikilink")

::\*R392 [医学免疫学](../Page/医学免疫学.md "wikilink")

::\*R394 [医学遗传学](../Page/医学遗传学.md "wikilink")

::\*R395
[医学心理学](../Page/医学心理学.md "wikilink")、[病理心理学](../Page/病理心理学.md "wikilink")

  - R4 [临床医学](../Page/临床医学.md "wikilink")

:\*R44 [诊断学](../Page/诊断学.md "wikilink")

:\*R45 [治疗学](../Page/治疗学.md "wikilink")

:\*R47 [护理学](../Page/护理学.md "wikilink")

:\*R48 [临终关怀学](../Page/临终关怀学.md "wikilink")

:\*R49 [康复医学](../Page/康复医学.md "wikilink")

  - R5 [内科学](../Page/内科学.md "wikilink")

:\*R51 [传染病](../Page/传染病.md "wikilink")

:\*R52 [结核病](../Page/结核病.md "wikilink")

:\*R53 [寄生虫病](../Page/寄生虫病.md "wikilink")

:\*R54
[心脏](../Page/心脏.md "wikilink")、[血管](../Page/血管.md "wikilink")（循环系）疾病

:\*R55 [血液及](../Page/血液.md "wikilink")[淋巴系疾病](../Page/淋巴.md "wikilink")

:\*R56 呼吸系及胸部疾病

:\*R57 消化系及腹部疾病

:\*R58
[内分泌腺疾病及](../Page/内分泌.md "wikilink")[代谢病](../Page/代谢病.md "wikilink")

:\*R59
[全身性疾病](../Page/全身性疾病.md "wikilink")、[地方病学](../Page/地方病学.md "wikilink")

  - R6 [外科学](../Page/外科学.md "wikilink")

:\*R61 [外科手术学](../Page/外科手术学.md "wikilink")

:\*R62 [整形外科学](../Page/整形外科学.md "wikilink")

:\*R63 [外科感染](../Page/外科感染.md "wikilink")

:\*R64 [创伤外科学](../Page/创伤外科学.md "wikilink")

:\*R65 外科学各论

:\*R68 [骨科学](../Page/骨科学.md "wikilink")

:\*R69 [泌尿科学](../Page/泌尿科学.md "wikilink")

:\*R71 [妇产科学](../Page/妇产科学.md "wikilink")

::\*R711 [妇科学](../Page/妇科学.md "wikilink")

::\*R713 [妇科手术](../Page/妇科手术.md "wikilink")

::\*R714 [产科学](../Page/产科学.md "wikilink")

::\*R715 [临床优生学](../Page/临床优生学.md "wikilink")

::\*R717 [助产学](../Page/助产学.md "wikilink")

::\*R719 [产科手术](../Page/产科手术.md "wikilink")

:\*R72 [儿科学](../Page/儿科学.md "wikilink")

::\*R722
[新生儿](../Page/新生儿.md "wikilink")、[早产儿疾病](../Page/早产儿.md "wikilink")

::\*R723 [婴儿营养障碍](../Page/婴儿营养障碍.md "wikilink")

::\*R725 [小儿内科学](../Page/小儿内科学.md "wikilink")

::\*R726 [小儿外科学](../Page/小儿外科学.md "wikilink")

::\*R729 小儿其他疾病

:\*R73 [肿瘤学](../Page/肿瘤学.md "wikilink")

::\*R730 一般性问题

::\*R732 [心血管肿瘤](../Page/心血管肿瘤.md "wikilink")

::\*R733
[造血器及](../Page/造血器.md "wikilink")[淋巴系肿瘤](../Page/淋巴.md "wikilink")

::\*R734 [呼吸系肿瘤](../Page/呼吸系肿瘤.md "wikilink")

::\*R735 [消化系肿瘤](../Page/消化系肿瘤.md "wikilink")

::\*R736 [内分泌腺肿瘤](../Page/内分泌腺肿瘤.md "wikilink")

::\*R737 [泌尿生殖器肿瘤](../Page/泌尿生殖器肿瘤.md "wikilink")

::\*R738 [运动系肿瘤](../Page/运动系肿瘤.md "wikilink")

:::\*R739.4 [神经系肿瘤](../Page/神经系肿瘤.md "wikilink")

:::\*R739.5 [皮肤肿瘤](../Page/皮肤肿瘤.md "wikilink")

:::\*R739.6 [耳鼻咽喉肿瘤](../Page/耳鼻咽喉肿瘤.md "wikilink")

:::\*R739.7 [眼肿瘤](../Page/眼肿瘤.md "wikilink")

:::\*R739.8 [口腔](../Page/口腔.md "wikilink")、颌面部肿瘤

:::\*R739.9 其他部位肿瘤

:\*R74
[神经病学与](../Page/神经病学.md "wikilink")[精神病学](../Page/精神病学.md "wikilink")

::\*R749 [精神病学](../Page/精神病学.md "wikilink")

:\*R75
[皮肤病学与](../Page/皮肤病学.md "wikilink")[性病学](../Page/性病学.md "wikilink")

::\*R759 [性病学](../Page/性病学.md "wikilink")

:\*R76 [耳鼻咽喉科学](../Page/耳鼻咽喉科学.md "wikilink")

::\*R764
[耳科学](../Page/耳科学.md "wikilink")、[耳疾病](../Page/耳疾病.md "wikilink")

::\*R765
[鼻科学](../Page/鼻科学.md "wikilink")、[鼻疾病](../Page/鼻疾病.md "wikilink")

::\*R766
[咽科学](../Page/咽科学.md "wikilink")、[咽疾病](../Page/咽疾病.md "wikilink")

::\*R767
[喉科学](../Page/喉科学.md "wikilink")、[喉疾病](../Page/喉疾病.md "wikilink")

::\*R768
[气管与](../Page/气管.md "wikilink")[食管镜学](../Page/食管镜学.md "wikilink")

:\*R77 [眼科学](../Page/眼科学.md "wikilink")

::\*R772 [眼纤维膜疾病](../Page/眼纤维膜疾病.md "wikilink")

::\*R773 [眼色素层疾病](../Page/眼色素层疾病.md "wikilink")

::\*R774
[视网膜与](../Page/视网膜.md "wikilink")[视神经疾病](../Page/视神经.md "wikilink")

::\*R775 [眼压与](../Page/眼压.md "wikilink")[青光眼](../Page/青光眼.md "wikilink")

::\*R776
[晶状体与](../Page/晶状体.md "wikilink")[玻璃体疾病](../Page/玻璃体.md "wikilink")

::\*R777 [眼附属器官疾病](../Page/眼附属器官疾病.md "wikilink")

::\*R778 [眼屈光学](../Page/眼屈光学.md "wikilink")

:::\*R779.1 [眼损伤与异物](../Page/眼损伤.md "wikilink")

:::\*R779.6 [眼外科手术学](../Page/眼外科手术学.md "wikilink")

:::\*R779.7 [小儿眼科学总论](../Page/小儿眼科学.md "wikilink")

:::\*R779.9 [热带眼科学](../Page/热带眼科学.md "wikilink")

:\*R78 [口腔科学](../Page/口腔科学.md "wikilink")

::\*R781 [口腔内科学](../Page/口腔内科学.md "wikilink")

::\*R782 [口腔颌面部外科学](../Page/口腔颌面部外科学.md "wikilink")

::\*R783 [口腔矫形学](../Page/口腔矫形学.md "wikilink")

::\*R787 [老年口腔疾病](../Page/老年口腔疾病.md "wikilink")

::\*R788 [儿童口腔疾病](../Page/儿童口腔疾病.md "wikilink")

:\*R79 外国[民族医学](../Page/民族医学.md "wikilink")

  - R8 [特种医学](../Page/特种医学.md "wikilink")

:\*R81 [放射医学](../Page/放射医学.md "wikilink")

:\*R83 [航海医学](../Page/航海医学.md "wikilink")

:\*R84 [潜水医学](../Page/潜水医学.md "wikilink")

:\*R85 [航空航天医学](../Page/航空航天医学.md "wikilink")

:\*R87 [运动医学](../Page/运动医学.md "wikilink")

  - R9 [药学](../Page/药学.md "wikilink")

:\*R91
[药物基础科学](../Page/药物.md "wikilink")、[药物分析](../Page/药物分析.md "wikilink")

::\*R914 [药物化学](../Page/药物化学.md "wikilink")

:\*R92
[药典](../Page/药典.md "wikilink")、[药方集](../Page/药方集.md "wikilink")（[处方集](../Page/处方集.md "wikilink")）、[药物鉴定](../Page/药物鉴定.md "wikilink")

:\*R93
[生药学](../Page/生药学.md "wikilink")（[天然药物学](../Page/天然药物化学.md "wikilink")）

:\*R94 [药剂学](../Page/药剂学.md "wikilink")

:\*R95 [药事组织](../Page/药事管理学.md "wikilink")

:\*R96 [药理学](../Page/药理学.md "wikilink")

:\*R99 [毒物学](../Page/毒理学.md "wikilink")

  - R0 各科[病例](../Page/病例.md "wikilink")

-----

[Category:中国图书馆图书分类法](../Category/中国图书馆图书分类法.md "wikilink")