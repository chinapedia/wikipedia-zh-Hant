## 农业科学

  - S-0 一般性理论

:\*S-01 农业科学研究方针、政策及其阐述

  - S1 [农业基础科学](../Page/农业.md "wikilink")

:\*S11 [农业数学](../Page/农业数学.md "wikilink")

:\*S12 [农业物理学](../Page/农业物理学.md "wikilink")

:\*S13 [农业化学](../Page/农业化学.md "wikilink")

:\*S14 [肥料学](../Page/肥料学.md "wikilink")

:\*S15 [土壤学](../Page/土壤学.md "wikilink")

:\*S16 [农业气象学](../Page/农业气象学.md "wikilink")

:\*S17 [农业地理学](../Page/农业地理学.md "wikilink")

:\*S18 [农业生物学](../Page/农业生物学.md "wikilink")

:\*S19 [农业生产环境保护](../Page/农业生产环境保护.md "wikilink")

  - S2 [农业工程](../Page/农业工程.md "wikilink")

:\*S21
[农业动力](../Page/农业动力.md "wikilink")、[农村能源](../Page/农村能源.md "wikilink")

:\*S22 [农业机械及](../Page/农业机械.md "wikilink")[农具](../Page/农具.md "wikilink")

:\*S23 [农业机械化](../Page/农业机械化.md "wikilink")

:\*S24 [农业电气化与自动化](../Page/农业电气化.md "wikilink")

:\*S25 [农业航空](../Page/农业航空.md "wikilink")

:\*S26 [农业建筑](../Page/农业建筑.md "wikilink")

:\*S27 [农田水利](../Page/农田水利.md "wikilink")

:\*S28
[农田基本建设](../Page/农田基本建设.md "wikilink")、[农垦](../Page/农垦.md "wikilink")

:\*S29
[农业工程勘测](../Page/农业工程勘测.md "wikilink")、[土地测量](../Page/土地测量.md "wikilink")

  - S3 [农学](../Page/农学.md "wikilink")（[农艺学](../Page/农艺学.md "wikilink")）

:\*S31 [作物生物学原理](../Page/作物生物学.md "wikilink")、栽培技术与方法

:\*S32 [作物品种与品种资源](../Page/作物品种.md "wikilink")

:\*S33
[作物遗传育种与](../Page/作物遗传育种.md "wikilink")[良种繁育](../Page/良种繁育.md "wikilink")

:\*S34
[耕作学与](../Page/耕作学.md "wikilink")[有机农业](../Page/有机农业.md "wikilink")

:\*S35 [播种](../Page/播种.md "wikilink")、[栽植](../Page/栽植.md "wikilink")

:\*S36 [田间管理](../Page/田间管理.md "wikilink")

:\*S37 [农产品收获](../Page/农产品.md "wikilink")、加工及贮藏

:\*S38 农产品的综合利用

:\*S39 [农产副业技术](../Page/农产副业技术.md "wikilink")

  - S4 [植物保护](../Page/植物保护.md "wikilink")

:\*S41 [植物检疫](../Page/植物检疫.md "wikilink")

:\*S42 [气象灾害及其防治](../Page/气象灾害.md "wikilink")

:\*S43 [病虫害及其防治](../Page/病虫害.md "wikilink")

:\*S44 [鸟兽害及其防治](../Page/鸟兽害.md "wikilink")

:\*S45 [有害植物及其清除](../Page/有害植物.md "wikilink")

:\*S46 其他灾害及其防治

:\*S47 各种防治方法

:\*S48
[农药防治](../Page/农药防治.md "wikilink")（[化学防治](../Page/化学防治.md "wikilink")）

:\*S49 [植物保护机械](../Page/植物保护机械.md "wikilink")

  - S5 [农作物](../Page/农作物.md "wikilink")

:\*S51 [禾谷类作物](../Page/禾谷类作物.md "wikilink")

:\*S52 [豆类作物](../Page/豆类作物.md "wikilink")

:\*S53 [薯类作物](../Page/薯类作物.md "wikilink")

:\*S54 [饲料作物](../Page/饲料作物.md "wikilink")、[牧草](../Page/牧草.md "wikilink")

:\*S55 [绿肥作物](../Page/绿肥作物.md "wikilink")

:\*S56 [经济作物](../Page/经济作物.md "wikilink")

:\*S57 [野生植物](../Page/野生植物.md "wikilink")

:\*S58 [野生植物](../Page/野生植物.md "wikilink")

:\*S59 热带、亚热带作物

  - S6 [园艺](../Page/园艺.md "wikilink")

:\*S60 一般性问题

:\*S61 [苗圃学](../Page/苗圃学.md "wikilink")

:\*S62
[温室园艺](../Page/温室园艺.md "wikilink")（[保护地栽培](../Page/保护地栽培.md "wikilink")）

:\*S63 [蔬菜园艺](../Page/蔬菜园艺.md "wikilink")

:\*S65 [瓜果园艺](../Page/瓜果园艺.md "wikilink")

:\*S66 [果树园艺](../Page/果树园艺.md "wikilink")

:\*S68
[观赏园艺](../Page/观赏园艺.md "wikilink")（[花卉和](../Page/花卉.md "wikilink")[观赏树木](../Page/观赏树木.md "wikilink")）

  - S7 [林业](../Page/林业.md "wikilink")

:\*S71 林业基础科学

:\*S72
[造林学](../Page/造林学.md "wikilink")、[林木育种及](../Page/林木育种.md "wikilink")[造林技术](../Page/造林技术.md "wikilink")

:\*S73 [绿化建设](../Page/绿化建设.md "wikilink")

:\*S75
[森林经营学](../Page/森林经营学.md "wikilink")、[森林计测学](../Page/森林计测学.md "wikilink")（[测树学](../Page/测树学.md "wikilink")）、[森林经理学](../Page/森林经理学.md "wikilink")

:\*S76 [森林保护学](../Page/森林保护学.md "wikilink")

:\*S77
[森林工程](../Page/森林工程.md "wikilink")、[林业机械](../Page/林业机械.md "wikilink")

:\*S78 森林采运与利用

:\*S79 [森林树种](../Page/森林树种.md "wikilink")

  - S8
    [畜牧](../Page/畜牧.md "wikilink")、[动物医学](../Page/动物医学.md "wikilink")、[狩猎](../Page/狩猎.md "wikilink")、[蚕](../Page/蚕.md "wikilink")、[蜂](../Page/蜂.md "wikilink")

:\*S81 普通[畜牧学](../Page/畜牧学.md "wikilink")

:\*S82 [家畜](../Page/家畜.md "wikilink")

:\*S83 [家禽](../Page/家禽.md "wikilink")

:\*S85
[动物医学](../Page/动物医学.md "wikilink")（[兽医学](../Page/兽医学.md "wikilink")）

:\*S86
[狩猎](../Page/狩猎.md "wikilink")、[野生动物驯养](../Page/野生动物驯养.md "wikilink")

:\*S87 畜禽产品的综合利用

:\*S88 [蚕桑](../Page/蚕桑.md "wikilink")

:\*S89 [养蜂](../Page/养蜂.md "wikilink")、[益虫饲养](../Page/益虫饲养.md "wikilink")

  - S9 [水产](../Page/水产.md "wikilink")、[渔业](../Page/渔业.md "wikilink")

:\*S91 水产基础科学

:\*S92 水产地区分布、[水产志](../Page/水产志.md "wikilink")

:\*S93 [水产资源](../Page/水产资源.md "wikilink")

:\*S94 [水产保护学](../Page/水产保护学.md "wikilink")

:\*S95 [水产工程](../Page/水产工程.md "wikilink")

:\*S96 [水产养殖技术](../Page/水产养殖技术.md "wikilink")

:\*S97 [水产捕捞](../Page/水产捕捞.md "wikilink")

:\*S98 水产物运输、保鲜、贮藏、加工、包装

-----

[Category:中国图书馆图书分类法](../Category/中国图书馆图书分类法.md "wikilink")