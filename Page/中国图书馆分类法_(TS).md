  - TS [轻工业](../Page/轻工业.md "wikilink")、[手工业](../Page/手工业.md "wikilink")

:\*TS0 一般性问题

:\*TS1
[纺织工业](../Page/纺织工业.md "wikilink")、[染整工业](../Page/染整工业.md "wikilink")

:\*TS2 [食品工业](../Page/食品工业.md "wikilink")

::\*TS21 [粮食加工工业](../Page/粮食加工工业.md "wikilink")

::\*TS22 [食用油脂加工工业](../Page/食用油脂加工工业.md "wikilink")

::\*TS23 [淀粉工业](../Page/淀粉工业.md "wikilink")

::\*TS24 [制糖工业](../Page/制糖工业.md "wikilink")

:::\*TS251
[屠宰及](../Page/屠宰.md "wikilink")[肉类加工工业](../Page/肉类加工工业.md "wikilink")

:::\*TS252 [乳品加工工业](../Page/乳品加工工业.md "wikilink")

:::\*TS253 [蛋品加工工业](../Page/蛋品加工工业.md "wikilink")

:::\*TS254 [水产加工工业](../Page/水产加工工业.md "wikilink")

:::\*TS255
[水果](../Page/水果.md "wikilink")、[蔬菜](../Page/蔬菜.md "wikilink")、[坚果加工工业](../Page/坚果.md "wikilink")

::\*TS26 [酿造工业](../Page/酿造工业.md "wikilink")

::\*TS27 [饮料冷食制造工业](../Page/饮料冷食制造工业.md "wikilink")

::\*TS29 [罐头工业](../Page/罐头工业.md "wikilink")

:\*TS3 [制盐工业](../Page/制盐工业.md "wikilink")

:\*TS4 [烟草工业](../Page/烟草工业.md "wikilink")

:\*TS5 [皮革工业](../Page/皮革工业.md "wikilink")

:\*TS6
[木材加工工业](../Page/木材加工工业.md "wikilink")、[家具制造工业](../Page/家具制造工业.md "wikilink")

:::::\*TS664-64 [家具图谱](../Page/家具.md "wikilink")

:\*TS7 [造纸工业](../Page/造纸工业.md "wikilink")

:\*TS8 [印刷工业](../Page/印刷工业.md "wikilink")

::\*TS91 [五金制品工业](../Page/五金制品工业.md "wikilink")

::\*TS93 [工艺美术制品工业](../Page/工艺美术制品工业.md "wikilink")

:::\*TS932 [雕塑工艺品](../Page/雕塑工艺品.md "wikilink")

:::\*TS933 [石料美术制品](../Page/石料美术制品.md "wikilink")

:::\*TS934 [金属工艺美术制品](../Page/金属工艺美术制品.md "wikilink")

:::\*TS935
[刺绣](../Page/刺绣.md "wikilink")、[编结](../Page/编结.md "wikilink")、[制毯](../Page/制毯.md "wikilink")

::::\*TS935.1 [中国刺绣](../Page/中国刺绣.md "wikilink")

::::\*TS935.5 [编结](../Page/编结.md "wikilink")

:::\*TS938 [民间工艺美术制品](../Page/民间工艺美术制品.md "wikilink")

::\*TS94
[服装工业](../Page/服装工业.md "wikilink")、[制鞋工业](../Page/制鞋工业.md "wikilink")

::::\*TS941 [服装工业](../Page/服装工业.md "wikilink")

::::\*TS941.7-9 流行装、时装

::::::\*TS941.7-91 春季时装

::::::\*TS941.7-92 夏季时装

::::::\*TS941.7-93 秋季时装

::::::\*TS941.7-94 冬季时装

:::\*TS943 [制鞋工业](../Page/制鞋工业.md "wikilink")

::\*TS95 其他轻工业、手工业

::\*TS97 生活服务技术

:::\*TS971 [美食学](../Page/美食学.md "wikilink")

:::\*TS972 饮食调制技术及设备

::::\*TS972.1
[烹饪法](../Page/烹饪法.md "wikilink")、[食谱](../Page/食谱.md "wikilink")、[菜谱](../Page/菜谱.md "wikilink")

:::::\*TS972.19 [调酒技术](../Page/调酒技术.md "wikilink")

::::\*TS972.2 [饮食设备与管理](../Page/饮食设备.md "wikilink")

::::\*TS972.3 饮食管理

:::\*TS973 [洗染](../Page/洗染.md "wikilink")、[缝补](../Page/缝补.md "wikilink")

:::\*TS974
[美容](../Page/美容.md "wikilink")、[沐浴](../Page/沐浴.md "wikilink")、清洁

::::\*TS974.1 [美容](../Page/美容.md "wikilink")

:::\*TS975 [居住管理](../Page/居住管理.md "wikilink")

:::\*TS976
[家庭管理](../Page/家庭管理.md "wikilink")、[家庭生活](../Page/家庭生活.md "wikilink")、[家庭服务](../Page/家庭服务.md "wikilink")

::::\*TS976.1 [家庭管理](../Page/家庭管理.md "wikilink")

::::\*TS976.2 [家庭卫生保健](../Page/家庭卫生保健.md "wikilink")

::::\*TS976.3 家庭生活知识

::::\*TS976.4 [穿着](../Page/穿着.md "wikilink")

::::\*TS976.7 家庭服务

::::\*TS976.8 [家庭用品与设备](../Page/家庭用品.md "wikilink")

::::\*TS976.9 [家庭自动化](../Page/家庭自动化.md "wikilink")

-----

  -

[Category:中国图书馆图书分类法](../Category/中国图书馆图书分类法.md "wikilink")