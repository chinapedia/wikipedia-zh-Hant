**中国工农红军第三军团**，简称**红三军团**，[中国工农红军主力部队之一](../Page/中国工农红军.md "wikilink")。

1930年6月，根据[中共中央军委命令](../Page/中共中央军委.md "wikilink")，[红五军和](../Page/红五军.md "wikilink")[红八军合编成](../Page/红八军.md "wikilink")**红三军团**，总指挥[彭德怀](../Page/彭德怀.md "wikilink")、总政治委员[滕代远](../Page/滕代远.md "wikilink")（后[杨尚昆代](../Page/杨尚昆.md "wikilink")）、参谋长[邓萍](../Page/邓萍.md "wikilink")、政治部主任[袁国平](../Page/袁国平.md "wikilink")，全军1万余人。下辖红五军：军长彭德怀兼任（后邓萍兼任），政治委员滕代远兼任（后[张纯清代](../Page/张纯清.md "wikilink")）；红八军，军长[李灿](../Page/李灿.md "wikilink")（后[何长工代理](../Page/何长工.md "wikilink")），政治委员何长工（后[邓乾元代理](../Page/邓乾元.md "wikilink")）。不久，[红十六军也划归红三军团建制](../Page/红十六军.md "wikilink")：军长[胡一鸣](../Page/胡一鸣.md "wikilink")、政治委员[李楚屏](../Page/李楚屏.md "wikilink")。
[195107_1930年红三军团攻克长沙.png](https://zh.wikipedia.org/wiki/File:195107_1930年红三军团攻克长沙.png "fig:195107_1930年红三军团攻克长沙.png")
红三军团组成后，根据[李立三控制下的](../Page/李立三.md "wikilink")[中共中央指示](../Page/中共.md "wikilink")，开展了对[长沙的攻击](../Page/长沙.md "wikilink")，并于7月27日一度攻占长沙。不久，由于遭到[何键的反攻](../Page/何键.md "wikilink")，红三军团被迫撤出长沙，受到严重损失。8月23日，红三军团与[红一军团在](../Page/红一军团.md "wikilink")[湖南](../Page/湖南.md "wikilink")[浏阳](../Page/浏阳.md "wikilink")[永和市会师](../Page/永和鎮_\(瀏陽市\).md "wikilink")，双方自发合编为[红一方面军](../Page/红一方面军.md "wikilink")。

1932年3月，根据中共中央要求，红一方面军撤销，红三军团改辖红五军、[红七军和](../Page/红七军.md "wikilink")[红十四军](../Page/红十四军.md "wikilink")，6月5日，红十五军回归红三军团。1933年6月，中央红军进行整编，取消了军一级的编制，红三军团下辖第四师、第五师、第六师。第四师：师长[张锡龙](../Page/张锡龙.md "wikilink")（后[洪超代](../Page/洪超.md "wikilink")）、政治委员[彭雪枫](../Page/彭雪枫.md "wikilink")（后[黄克诚代](../Page/黄克诚.md "wikilink")）；第五师师长[寻淮洲](../Page/寻淮洲.md "wikilink")（后[李天佑代](../Page/李天佑.md "wikilink")）、政治委员[乐少华](../Page/乐少华.md "wikilink")（后为[陈阿金](../Page/陈阿金.md "wikilink")、[钟赤兵代](../Page/钟赤兵.md "wikilink")）；第六师：师长洪超（后彭雪枫代），政治委员陈阿金（后为[江华](../Page/江华.md "wikilink")、[徐策代](../Page/徐策.md "wikilink")）、政治部主任[欧阳钦](../Page/欧阳钦.md "wikilink")。

1934年1月，中央军委取消了红一方面军的编制，红三军团直属中央指挥，10月10日，红三军团开始参加[长征](../Page/长征.md "wikilink")，此时各级主官为：军团长彭德怀、政治委员杨尚昆、参谋长邓萍、政治部主任[罗荣桓](../Page/罗荣桓.md "wikilink")，下辖三个师：第四师师长洪超（后彭雪枫、[张宗逊代](../Page/张宗逊.md "wikilink")）、政治委员[黄克诚](../Page/黄克诚.md "wikilink")；第五师师长李天佑、政治委员[钟赤兵](../Page/钟赤兵.md "wikilink")；第六师师长[曹里怀](../Page/曹里怀.md "wikilink")、政治委员[徐策](../Page/徐策.md "wikilink")。

1935年[遵义会议后](../Page/遵义会议.md "wikilink")，红三军团由于损失巨大，取消了师一级的编制，改组为4个团。7月21日，在中央红军和[红四方面军会师后](../Page/红四方面军.md "wikilink")，红三军团番号取消，部队归红一军团指挥，编为红一军团第四师。

1937年，原红三军团主力改编为[八路军](../Page/八路军.md "wikilink")[115师](../Page/115师.md "wikilink")[343旅](../Page/343旅.md "wikilink")[686团](../Page/686团.md "wikilink")，李天佑任团长，投入[抗日战争战场](../Page/抗日战争_\(中国\).md "wikilink")。

[Category:中国工农红军军团](../Category/中国工农红军军团.md "wikilink")
[Category:1930年建立的军事组织](../Category/1930年建立的军事组织.md "wikilink")
[Category:1935年解散的軍事組織](../Category/1935年解散的軍事組織.md "wikilink")