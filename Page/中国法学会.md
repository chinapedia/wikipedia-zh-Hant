**中国法学会**是[中华人民共和国法学界和法律界的全国性群众团体和学术团体](../Page/中华人民共和国.md "wikilink")，成立于1982年7月，其前身是**中国政治法律学会**。

## 历史沿革

## 组织

下设法理学、[宪法学](../Page/中国法学会宪法学研究会.md "wikilink")、行政法学、民法学、商法学、经济法学、刑法学、诉讼法学、比较法学、环境资源法学等研究会。

## 历届理事会

\[1\]

  - 中国法学会第一届理事会（1982年7月-1986年5月）

<!-- end list -->

  - 名誉会长：[杨秀峰](../Page/杨秀峰.md "wikilink")
  - 会长：
      - [武新宇](../Page/武新宇.md "wikilink")（1982年7月-1983年11月）
      - [张友渔](../Page/張友漁_\(法學家\).md "wikilink")（1983年11月-1985年8月）
      - [王仲方](../Page/王仲方.md "wikilink")（1985年8月-1986年5月）
  - 副会长：[王一夫](../Page/王一夫.md "wikilink")
    [梁文英](../Page/梁文英.md "wikilink")
    [王汉斌](../Page/王汉斌.md "wikilink")
    [朱剑明](../Page/朱剑明.md "wikilink")
    [项淳一](../Page/项淳一.md "wikilink")
    [甘重斗](../Page/甘重斗.md "wikilink")
    [钱端升](../Page/钱端升.md "wikilink")
    [宦乡](../Page/宦乡.md "wikilink")
    [陈守一](../Page/陈守一.md "wikilink")
    [王叔文](../Page/王叔文_\(法学家\).md "wikilink")
    [曹海波](../Page/曹海波.md "wikilink")
    [李广祥](../Page/李广祥.md "wikilink")
    [盛愉](../Page/盛愉.md "wikilink")
  - 秘书长：[陈为典](../Page/陈为典.md "wikilink")（1985年5月-1986年5月）

<!-- end list -->

  - 中国法学会第二届理事会（1986年5月-1991年5月）

<!-- end list -->

  - 名誉会长：[张友渔](../Page/張友漁_\(法學家\).md "wikilink")
    [钱端升](../Page/钱端升.md "wikilink")
    [刘复之](../Page/刘复之.md "wikilink")
  - 会长：[王仲方](../Page/王仲方.md "wikilink")
  - 常务副会长：[朱剑明](../Page/朱剑明.md "wikilink")
  - 副会长：[任建新](../Page/任建新.md "wikilink") [顾明](../Page/顾明.md "wikilink")
    [李石生](../Page/李石生.md "wikilink") [高西江](../Page/高西江.md "wikilink")
    [鲁坚](../Page/鲁坚.md "wikilink") [梁国庆](../Page/梁国庆.md "wikilink")
    [张彦宁](../Page/张彦宁.md "wikilink")
    [王叔文](../Page/王叔文_\(法学家\).md "wikilink")
    [盛愉](../Page/盛愉.md "wikilink") [张国华](../Page/张国华.md "wikilink")
    [高铭暄](../Page/高铭暄.md "wikilink") [甘绩华](../Page/甘绩华.md "wikilink")
    [陈为典](../Page/陈为典.md "wikilink")
  - 秘书长：
      - [陈为典](../Page/陈为典.md "wikilink")（1986年5月-1988年10月兼）
      - [宋树涛](../Page/宋树涛.md "wikilink")（1988年10月-1991年5月）

<!-- end list -->

  - 中国法学会第三届理事会（1991年5月-1997年1月）

<!-- end list -->

  - 名誉会长：[张友渔](../Page/張友漁_\(法學家\).md "wikilink")
    [王汉斌](../Page/王汉斌.md "wikilink")
    [任建新](../Page/任建新.md "wikilink")
  - 会长： [邹瑜](../Page/邹瑜.md "wikilink")
  - 常务副会长：[朱剑明](../Page/朱剑明.md "wikilink")
  - 副会长：[林准](../Page/林准.md "wikilink") [俞雷](../Page/俞雷.md "wikilink")
    [佘孟孝](../Page/佘孟孝.md "wikilink")
    [邹恩同](../Page/邹恩同.md "wikilink")
    [高西江](../Page/高西江.md "wikilink")
    [梁国庆](../Page/梁国庆.md "wikilink")
    [王叔文](../Page/王叔文_\(法学家\).md "wikilink")
    [孙琬钟](../Page/孙琬钟.md "wikilink")
    [王家福](../Page/王家福.md "wikilink")
    [罗豪才](../Page/罗豪才.md "wikilink")
    [高铭暄](../Page/高铭暄.md "wikilink")
    [陈光中](../Page/陈光中.md "wikilink")
    [巫昌祯](../Page/巫昌祯.md "wikilink")
    [宋树涛](../Page/宋树涛.md "wikilink")（1993年增补）
  - 秘书长：[宋树涛](../Page/宋树涛.md "wikilink")（兼）

<!-- end list -->

  - 中国法学会第四届理事会（1997年1月-2003年11月）

<!-- end list -->

  - 名誉会长：[王汉斌](../Page/王汉斌.md "wikilink")
  - 会长：[任建新](../Page/任建新.md "wikilink")
  - 常务副会长：[佘孟孝](../Page/佘孟孝.md "wikilink")
    [孙琬钟](../Page/孙琬钟.md "wikilink")
  - 副会长：[邹恩同](../Page/邹恩同.md "wikilink")
    [梁国庆](../Page/梁国庆.md "wikilink")
    [罗豪才](../Page/罗豪才.md "wikilink")
    [张秀夫](../Page/张秀夫.md "wikilink")
    [陈冀平](../Page/陈冀平.md "wikilink")
    [牛平](../Page/牛平.md "wikilink")
    [王叔文](../Page/王叔文_\(法学家\).md "wikilink")
    [王家福](../Page/王家福.md "wikilink")
    [高铭暄](../Page/高铭暄.md "wikilink")
    [陈光中](../Page/陈光中.md "wikilink")
    [巫昌祯](../Page/巫昌祯.md "wikilink")
    [罗锋](../Page/罗锋.md "wikilink")
    [卞耀武](../Page/卞耀武.md "wikilink")
    [魏振瀛](../Page/魏振瀛.md "wikilink")
    [孙在雍](../Page/孙在雍.md "wikilink")
    [宋树涛](../Page/宋树涛.md "wikilink")
  - 秘书长：[宋树涛](../Page/宋树涛.md "wikilink")（兼）

<!-- end list -->

  - 中国法学会第五届理事会（2003年11月-2009年1月）

<!-- end list -->

  - 会长：[韩杼滨](../Page/韩杼滨.md "wikilink")
  - 常务副会长：[刘飏](../Page/刘飏.md "wikilink")
  - 副会长：[牛平](../Page/牛平.md "wikilink") [王景荣](../Page/王景荣.md "wikilink")
    [石泰峰](../Page/石泰峰.md "wikilink") [刘法合](../Page/刘法合.md "wikilink")
    [刘家琛](../Page/刘家琛.md "wikilink") [孙谦](../Page/孙谦.md "wikilink")
    [孙在雍](../Page/孙在雍.md "wikilink") [朱苏力](../Page/朱苏力.md "wikilink")
    [宋大涵](../Page/宋大涵.md "wikilink")
    [宋树涛](../Page/宋树涛.md "wikilink")
    [张文显](../Page/张文显.md "wikilink")
    [陈冀平](../Page/陈冀平.md "wikilink")
    [周成奎](../Page/周成奎.md "wikilink")
    [罗锋](../Page/罗锋.md "wikilink")
    [段正坤](../Page/段正坤.md "wikilink")
    [夏勇](../Page/夏勇.md "wikilink")
    [徐显明](../Page/徐显明.md "wikilink")
    [袁曙宏](../Page/袁曙宏.md "wikilink")
    [曾宪义](../Page/曾宪义.md "wikilink")
  - 秘书长：[宋树涛](../Page/宋树涛.md "wikilink")（兼）

<!-- end list -->

  - 中国法学会第六届理事会（2009年1月—2013年11月）

<!-- end list -->

  - 会长：[韩杼滨](../Page/韩杼滨.md "wikilink")
  - 常务副会长：[刘飏](../Page/刘飏.md "wikilink")
  - 副会长：[马建](../Page/马建_\(政府官员\).md "wikilink")
    [王利明](../Page/王利明.md "wikilink")
    [王其江](../Page/王其江.md "wikilink")
    [石泰峰](../Page/石泰峰.md "wikilink")
    [刘继贤](../Page/刘继贤.md "wikilink")
    [安建](../Page/安建.md "wikilink")
    [朱孝清](../Page/朱孝清.md "wikilink")
    [吴志攀](../Page/吴志攀.md "wikilink")
    [宋大涵](../Page/宋大涵.md "wikilink")
    [张文显](../Page/张文显.md "wikilink")
    [张苏军](../Page/张苏军.md "wikilink")
    [李林](../Page/李林.md "wikilink")
    [李清林](../Page/李清林.md "wikilink")
    [沈德咏](../Page/沈德咏.md "wikilink")
    [陈冀平](../Page/陈冀平.md "wikilink")
    [周成奎](../Page/周成奎.md "wikilink")
    [胡忠](../Page/胡忠.md "wikilink")
    [孟宏伟](../Page/孟宏伟.md "wikilink")
    [徐显明](../Page/徐显明.md "wikilink")
    [袁曙宏](../Page/袁曙宏.md "wikilink")
  - 秘书长：[林中梁](../Page/林中梁.md "wikilink")

<!-- end list -->

  - 中国法学会第七届理事会（2013年11月—2019年3月）

<!-- end list -->

  - 会长：[王乐泉](../Page/王乐泉.md "wikilink")
  - 常务副会长：[陈冀平](../Page/陈冀平.md "wikilink")
  - 副会长：[鲍绍坤](../Page/鲍绍坤.md "wikilink")
    [张鸣起](../Page/张鸣起.md "wikilink")
    [张文显](../Page/张文显.md "wikilink")
    [马建](../Page/马建_\(政府官员\).md "wikilink")
    [王利明](../Page/王利明.md "wikilink") 王其江 朱孝清 任海泉 江必新 孙谦 李伟 李林 吴志攀
    张苏军 郎胜 姜伟 [袁曙宏](../Page/袁曙宏.md "wikilink") 徐显明 黄进
  - 秘书长：鲍绍坤(兼)

### 历任会长

| 姓名                                       | 籍贯     | 任职                | 学历背景        | 曾任最高领导职务                    |
| ---------------------------------------- | ------ | ----------------- | ----------- | --------------------------- |
| [武新宇](../Page/武新宇.md "wikilink")         | 山西阳高   | 1982年7月－1983年11月  | 北平师范大学      | 内务部副部长、全国人大常委兼副秘书长          |
| [张友渔](../Page/張友漁_\(法學家\).md "wikilink") | 山西灵石   | 1983年11月－1985年8月  | 北京法政大学法律系   | 中共北京市委副书记、中国社会科学院副院长、全国人大常委 |
| [王仲方](../Page/王仲方.md "wikilink")         | 安徽芜湖   | 1985年8月－1991年5月   | 延安抗日军政大学    | 全国政协法律委员会副主任                |
| [邹瑜](../Page/邹瑜.md "wikilink")           | 广西博白   | 1991年5月－1997年1月   | 陕北公学        | 司法部部长、全国人大常委                |
| [任建新](../Page/任建新.md "wikilink")         | 山西襄汾   | 1997年1月－2003年11月  | 北京大学工学院     | 中共中央书记处书记、全国政协副主席、最高人民法院院长  |
| [韩杼滨](../Page/韩杼滨.md "wikilink")         | 黑龙江哈尔滨 | 2003年11月－2013年11月 | 北京经济函授大学    | 中央纪委副书记、最高人民检察院检察长          |
| [王乐泉](../Page/王乐泉.md "wikilink")         | 山东寿光   | 2013年11月－2019年3月  | 中共中央党校      | 中央政治局委员、中央政法委副书记            |
| [王晨](../Page/王晨_\(政治人物\).md "wikilink")  | 北京     | 2019年3月－          | 中国社会科学院研究生院 | 中央政治局委员、全国人大常委会副委员长         |

## 中國法學會理事（港區）

2013年11月中國法學會第七次全國會員代表大會開幕，中共中央總書記、國家主席習近平，中共中央政治局常委、全國人大委員長張德江，中共中央政治局常委、中央書記處書記劉雲山等黨和國家領導人出席。原中共中央政治局委員、中央政法委副書記王樂泉當選中國法學會會長。會議選舉產生法學會新一屆理事會和領導機構，其中13名香港法律界代表獲選新一屆港區理事。\[2\]

  - [马恩国](../Page/马恩国.md "wikilink")
  - [马耀添](../Page/马耀添.md "wikilink")
  - [王贵国](../Page/王贵国.md "wikilink")
  - [冯华健](../Page/冯华健.md "wikilink")
  - [陈弘毅](../Page/陈弘毅.md "wikilink")
  - 林峰
  - [周永健](../Page/周永健.md "wikilink")
  - [郑若骅](../Page/郑若骅.md "wikilink")
  - [赵文宗](../Page/赵文宗.md "wikilink")
  - [黄英豪](../Page/黄英豪.md "wikilink")
  - [黄继儿](../Page/黄继儿.md "wikilink")
  - [廖长江](../Page/廖长江.md "wikilink")
  - [廖长城](../Page/廖长城.md "wikilink")

## 出版物

《[中国法学](../Page/中国法学.md "wikilink")》是中国法学会主办的双月刊，1984年创刊，为中国法学界最具影响、最具权威的学术期刊。曾连获三届[国家新闻出版总署颁发的国家期刊奖](../Page/国家新闻出版总署.md "wikilink")\[3\]。

## 参考文献

## 外部链接

  - [中国法学会](http://www.chinalaw.org.cn)

## 参见

  - [中国法官协会](../Page/中国法官协会.md "wikilink")
  - [中国检察官协会](../Page/中国检察官协会.md "wikilink")
  - [中国警察协会](../Page/中国警察协会.md "wikilink")
  - [中国律师协会](../Page/中国律师协会.md "wikilink")

{{-}}

[中国法学会](../Category/中国法学会.md "wikilink")
[Category:中华人民共和国法律组织](../Category/中华人民共和国法律组织.md "wikilink")
[Category:1982年中国建立](../Category/1982年中国建立.md "wikilink")
[Category:1982年建立的组织](../Category/1982年建立的组织.md "wikilink")

1.  [历任领导，中国法学会，于2011-10-21查阅](http://www.chinalaw.org.cn/Column/Column_Template4.aspx?ColumnID=109)
2.  [中國法學會港理事增逾倍
    或討論政改](http://paper.wenweipo.com/2013/12/01/HK1312010011.htm)文匯報
    2013-12-01
3.  [中国法学](http://www.xsqk.org/qikan/about1272.html)