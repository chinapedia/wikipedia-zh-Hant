**中国温泉之乡（城、都）**是对[中国著名](../Page/中国.md "wikilink")[温泉所在地的命名](../Page/温泉.md "wikilink")，原由[中国矿业联合会评定](../Page/中国矿业联合会.md "wikilink")，2010年起经[中国国土资源部重新评审命名](../Page/中国国土资源部.md "wikilink")（包括“地热能开发利用示范单位”）。

## 中国温泉之都

  - [重庆市](../Page/重庆市.md "wikilink")
  - [天津市](../Page/天津市.md "wikilink")
  - [福州市](../Page/福州市.md "wikilink")

## 中国温泉之城

  - [辽宁省](../Page/辽宁省.md "wikilink")[辽阳市](../Page/辽阳市.md "wikilink")[弓长岭区](../Page/弓长岭区.md "wikilink")
  - [辽宁省](../Page/辽宁省.md "wikilink")[葫芦岛市](../Page/葫芦岛市.md "wikilink")[兴城](../Page/兴城.md "wikilink")
  - [云南省](../Page/云南省.md "wikilink")[洱源县城](../Page/茈碧湖镇.md "wikilink")
  - [山东](../Page/山东.md "wikilink")[临沂](../Page/临沂.md "wikilink")
  - [陕西](../Page/陕西.md "wikilink")[咸阳](../Page/咸阳.md "wikilink")
  - [湖北](../Page/湖北.md "wikilink")[咸宁](../Page/咸宁.md "wikilink")
  - [湖南](../Page/湖南.md "wikilink")[郴州](../Page/郴州.md "wikilink")
  - [广东](../Page/广东.md "wikilink")[清远](../Page/清远.md "wikilink")
  - [浙江](../Page/浙江.md "wikilink")[武义](../Page/武义.md "wikilink")

## 中国温泉之乡

华北地区

  - [北京市](../Page/北京市.md "wikilink")[小汤山](../Page/小汤山.md "wikilink")
  - [天津市](../Page/天津市.md "wikilink")[东丽湖](../Page/东丽湖.md "wikilink")
  - [河北省](../Page/河北省.md "wikilink")[雄县](../Page/雄县.md "wikilink")
  - [河北省](../Page/河北省.md "wikilink")[霸州](../Page/霸州.md "wikilink")
  - [河北省](../Page/河北省.md "wikilink")[固安](../Page/固安.md "wikilink")
  - [内蒙古自治区](../Page/内蒙古自治区.md "wikilink")[克什克腾旗](../Page/克什克腾旗.md "wikilink")

华东地区

  - [山东省](../Page/山东省.md "wikilink")[威海](../Page/威海.md "wikilink")
  - [江苏省](../Page/江苏省.md "wikilink")[南京市](../Page/南京市.md "wikilink")[浦口区](../Page/浦口区.md "wikilink")[汤泉镇](../Page/汤泉镇.md "wikilink")
  - 江苏省南京市[汤山](../Page/汤山.md "wikilink")
  - 江苏省[连云港市](../Page/连云港市.md "wikilink")[东海县](../Page/东海县.md "wikilink")
  - [江西省](../Page/江西省.md "wikilink")[宜春市](../Page/宜春市.md "wikilink")[明月山](../Page/明月山.md "wikilink")[温汤镇](../Page/温汤镇.md "wikilink")
  - [安徽省](../Page/安徽省.md "wikilink")[合肥](../Page/合肥.md "wikilink")

东北地区

  - [黑龙江省](../Page/黑龙江省.md "wikilink")[林甸](../Page/林甸.md "wikilink")

西北地区

  - [陕西省](../Page/陕西省.md "wikilink")[临潼](../Page/临潼.md "wikilink")

西南地区

  - [贵州省](../Page/贵州省.md "wikilink")[石阡](../Page/石阡.md "wikilink")
  - [贵州省](../Page/贵州省.md "wikilink")[思南](../Page/思南.md "wikilink")
  - [重庆市](../Page/重庆市.md "wikilink")[巴南](../Page/巴南.md "wikilink")
  - [四川省](../Page/四川省.md "wikilink")[广元市](../Page/广元市.md "wikilink")

华中地区

  - [湖北省](../Page/湖北省.md "wikilink")[应城](../Page/应城.md "wikilink")

华南地区

  - [海南省](../Page/海南省.md "wikilink")[琼海](../Page/琼海.md "wikilink")
  - [广东省](../Page/广东省.md "wikilink")[阳江](../Page/阳江.md "wikilink")
  - [广东省](../Page/广东省.md "wikilink")[恩平](../Page/恩平.md "wikilink")
  - [广东省](../Page/广东省.md "wikilink")[龙门县](../Page/龙门县.md "wikilink")
  - [福建省](../Page/福建省.md "wikilink")[永泰县](../Page/永泰县.md "wikilink")
  - [福建省](../Page/福建省.md "wikilink")[连江县](../Page/连江县.md "wikilink")

## 温泉（地热）开发利用示范单位

  - [陕西省](../Page/陕西省.md "wikilink")[咸阳市](../Page/咸阳市.md "wikilink")[绿源地热能开发项目](../Page/绿源地热能开发项目.md "wikilink")
  - [天津工业大学](../Page/天津工业大学.md "wikilink")

## 浅层地温能开发利用示范单位

  - [陕西省](../Page/陕西省.md "wikilink")[咸阳市](../Page/咸阳市.md "wikilink")[天虹基•紫韵东城小区](../Page/天虹基•紫韵东城小区.md "wikilink")
  - [辽宁省](../Page/辽宁省.md "wikilink")[沈阳市](../Page/沈阳市.md "wikilink")[钓鱼台7号小区](../Page/钓鱼台7号小区.md "wikilink")

{{-}}

[\*](../Category/中国温泉.md "wikilink")
[\*](../Category/中国旅游景点.md "wikilink")
[Category:温泉列表](../Category/温泉列表.md "wikilink")
[Category:中华人民共和国奖励](../Category/中华人民共和国奖励.md "wikilink")