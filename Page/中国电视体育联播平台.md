**中国电视体育联播平台**（**C**hina **S**ports **P**rograms
**N**etwork，**CSPN**）为[中国的一家](../Page/中国.md "wikilink")[体育联播平台](../Page/体育.md "wikilink")，由数家地方省级[电视台的体育频道同步播出](../Page/电视台.md "wikilink")。最早由[辽宁电视台](../Page/辽宁电视台.md "wikilink")、[江苏电视台](../Page/江苏电视台.md "wikilink")、[山东电视台](../Page/山东电视台.md "wikilink")、[湖北电视台和](../Page/湖北电视台.md "wikilink")[新疆电视台与神州天地影视传媒有限公司策划发起](../Page/新疆电视台.md "wikilink")，神州天地体育传媒（北京）有限公司担任总运营商，外资[Mediaset持有](../Page/Mediaset.md "wikilink")49%的股份。2007年10月1日试播，随后[江西电视台](../Page/江西电视台.md "wikilink")、[内蒙古电视台加入](../Page/内蒙古电视台.md "wikilink")。2008年1月1日改版后正式开播。江苏电视台于2008年结束營業后退出。辽宁电视台也于2009年结束营业后退出。

CSPN开播后对[2008年欧洲足球锦标赛全程转播](../Page/2008年欧洲足球锦标赛.md "wikilink")，并邀请[黄健翔全程担任嘉宾解说](../Page/黄健翔.md "wikilink")。\[1\]CSPN在[北京市](../Page/北京市.md "wikilink")[大兴区星光影视园建立节目制作基地](../Page/大兴区.md "wikilink")，實施“联合引进、联合制作、联合播出”，各成员台节目、[广告同步播出](../Page/广告.md "wikilink")。2008年5月，CSPN搬入全新办公大楼，占地4000余[平方米](../Page/平方米.md "wikilink")，制作基地分为制作区与办公区。2009年10月，CSPN与[搜狐结成](../Page/搜狐.md "wikilink")[十一运会战略合作伙伴](../Page/十一运会.md "wikilink")。\[2\]

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [官方网站](https://archive.is/20130424141140/http://www.cspn.cn/)

[Category:中国电视](../Category/中国电视.md "wikilink")
[Category:2007年建立](../Category/2007年建立.md "wikilink")

1.  [CSPN打造欧锦赛盛宴
    黄健翔携手米卢联袂解说](http://sports.tom.com/2008-04-03/0430/85258280.html)

2.  [CSPN与搜狐结成全运战略合作伙伴
    实现强强联合](http://www.cspn.cn/templates/CN_NewsCenter/index.aspx?nodeid=3&page=ContentPage&contentid=1573)