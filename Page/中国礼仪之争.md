[Jesuites_en_chine.jpg](https://zh.wikipedia.org/wiki/File:Jesuites_en_chine.jpg "fig:Jesuites_en_chine.jpg")在华传教士\]\]

**中国礼仪之争**，指17世纪至18世纪[西方](../Page/西方.md "wikilink")[天主教](../Page/天主教.md "wikilink")[传教士就中国传统礼仪是否与天主教義相容](../Page/传教士.md "wikilink")，从而和清王朝在学术和政治上发生的冲突\[1\]\[2\]。在天主教内先后有两种看法，[耶稣会认为](../Page/耶稣会.md "wikilink")[祭祖](../Page/祭祖.md "wikilink")、祭祀[孔子](../Page/孔子.md "wikilink")（[祭孔](../Page/祭孔.md "wikilink")）乃世俗的禮儀，与天主教教义相容，在一定范围内，是应该被容忍的；而以[道明会和](../Page/道明会.md "wikilink")[方济会则认为这与天主教教义相悖](../Page/方济会.md "wikilink")，不可容忍，并因此向罗马教皇请示报告。

在道明会建议下，罗马教廷在1645年通过通谕，禁止中国教徒祭祖祭孔。但之后在耶稣会的游说下，罗马教廷在1656年，解除了这个禁令。\[3\]这个风波曾一度使中国和亚洲其它地区，包括[日本](../Page/日本.md "wikilink")\[4\]和[印度](../Page/印度.md "wikilink")\[5\]的天主教徒都受到了影响。

在这场冲突中，[清朝皇帝和罗马教廷的几个教皇](../Page/清朝皇帝.md "wikilink")（包括[克勉十一世和](../Page/克勉十一世.md "wikilink")[克勉十四世](../Page/克勉十四世.md "wikilink")）分歧越来越大，最终使得罗马教廷进行了直接干预。虽然到了后期，道明会和方济会已不像当初激烈反对耶稣会的思想，但教皇們始终持强硬态度。克勉十一世在1704年下达谕令禁止教徒进行祭祖祭孔的仪式。1742年，本笃十四世重申禁令，并禁止一切的辩论。\[6\]

在两个世纪后的1939年，罗马教廷开始重新审视这个问题。[庇护十二世在](../Page/庇护十二世.md "wikilink")1939年12月8日颁布了一项谕令，同意教徒进行祭祖仪式和祭孔仪式。后来，在[第二次梵蒂冈大公会议](../Page/第二次梵蒂冈大公会议.md "wikilink")（1962-1965）上，祭祖祭孔被正式认可，成为教义的一部分。\[7\]

## 背景

[Ricciportrait.jpg](https://zh.wikipedia.org/wiki/File:Ricciportrait.jpg "fig:Ricciportrait.jpg")\]\]

1582年，[耶稣会](../Page/耶稣会.md "wikilink")[意大利籍传教士](../Page/意大利.md "wikilink")[罗明坚及](../Page/罗明坚.md "wikilink")[利玛窦抵達](../Page/利玛窦.md "wikilink")[廣東](../Page/廣東.md "wikilink")[肇庆](../Page/肇庆.md "wikilink")，為近代天主教入華揭開序幕。

1644年[清兵入关时](../Page/清兵入关.md "wikilink")，天主教入華已62年，[德意志籍傳教士](../Page/德意志.md "wikilink")[汤若望協助編制曆法](../Page/汤若望.md "wikilink")，獲清廷信任，天主教得以迅速發展。

1664年，耶稣会住院共38所，耶稣会士来华人数累計82人，全国的[教堂已经有](../Page/教堂.md "wikilink")156座，全国天主教徒达245000人之多\[8\]。

1665年，[杨光先發表](../Page/杨光先.md "wikilink")《辟缪论》，批評汤若望[历法不準](../Page/历法.md "wikilink")，當時顧命大臣[鰲拜不滿](../Page/鰲拜.md "wikilink")[外國人參議朝政](../Page/外國人.md "wikilink")，於是支持楊光先，[監禁汤若望](../Page/監禁.md "wikilink")，天主教在华發展直挫，史称「[曆獄](../Page/曆獄.md "wikilink")」。

[Portrait_of_the_Kangxi_Emperor_in_Court_Dress.jpg](https://zh.wikipedia.org/wiki/File:Portrait_of_the_Kangxi_Emperor_in_Court_Dress.jpg "fig:Portrait_of_the_Kangxi_Emperor_in_Court_Dress.jpg")\]\]
往後，[康熙帝執政](../Page/康熙帝.md "wikilink")，[禁錮](../Page/禁錮.md "wikilink")[鰲拜](../Page/鰲拜.md "wikilink")，重新起用外國人。[南怀仁利用西方發明對中國貢獻](../Page/南怀仁.md "wikilink")，建立天主教士的聲望，又與[利类思和](../Page/利类思.md "wikilink")[安文思共同上奏](../Page/安文思.md "wikilink")，为汤若望[平反](../Page/平反.md "wikilink")。1669年（康熙八年）9月5日，康熙颁旨：「恶人杨光先捏词天主教係[邪教](../Page/邪教.md "wikilink")，已经议复禁止。今看得供奉天主教并无恶乱之处，相应将天主教仍令伊等照旧供奉。」康熙初年，成為傳教士在中國的蜜月期。

1689年，清朝与[俄罗斯帝國準備劃訂疆界](../Page/俄罗斯帝國.md "wikilink")，商議[尼布楚条约](../Page/尼布楚条约.md "wikilink")，精於[拉丁文的传教士負責代表中方與](../Page/拉丁文.md "wikilink")[俄國人溝通](../Page/俄國人.md "wikilink")，這條條約最終以拉丁文簽訂。南怀仁等亦协助清廷铸造火炮，平定[三藩之乱](../Page/三藩之乱.md "wikilink")。他们准确预测[日食](../Page/日食.md "wikilink")，使皇帝可以为相关祭奠做好准备；一些耶稣会传教士則成为宫廷画家。

1692年（康熙三十一年），康熙下达一道容教令，標誌著傳教士的勢力攀上高峰：

[康熙年間](../Page/康熙.md "wikilink")，新來[中國的](../Page/中國.md "wikilink")[多明我會及](../Page/多明我會.md "wikilink")[方濟會教士反對明末以來耶穌會教士容許中國教徒](../Page/方濟會.md "wikilink")[祭天](../Page/祭天.md "wikilink")、[敬孔](../Page/祭孔.md "wikilink")、[祀祖等禮俗](../Page/祭祖.md "wikilink")，爆發[禮儀之爭](../Page/禮儀之爭.md "wikilink")，因此1720年（康熙五十九年）清廷決定禁教（未嚴格執行），1723年（[雍正元年](../Page/雍正.md "wikilink")）[清世宗開始嚴格執行禁教](../Page/清世宗.md "wikilink")，再加上日後傳教士介入世宗未[登極時](../Page/登極.md "wikilink")，與兄弟爭奪[皇位之事](../Page/皇位.md "wikilink")，還有擔心民眾信奉[天主教後](../Page/天主教.md "wikilink")，國家命令無法貫徹，且會被[教廷與西方天主教國家控制](../Page/教廷.md "wikilink")，因此清世宗下令除留京任職的傳教士外，其餘一律送往[澳門](../Page/澳門.md "wikilink")，各地天主堂被拆毀，或改成[公廁](../Page/公廁.md "wikilink")，屢下禁令，不許民眾對此信仰。[清高宗](../Page/清高宗.md "wikilink")[乾隆年間](../Page/乾隆.md "wikilink")，取締尤烈，因此傳教活動幾乎消失，直到西元1842[清宣宗時期](../Page/清宣宗.md "wikilink")（[道光二十年](../Page/道光.md "wikilink")）爆發[鴉片戰爭後](../Page/鴉片戰爭.md "wikilink")，簽訂中國首條[不平等條約](../Page/不平等條約.md "wikilink")——《[南京條約](../Page/南京條約.md "wikilink")》才解除禁令。

## 争论序幕

禮儀之爭的序幕，背後是一場[天主教](../Page/天主教.md "wikilink")「[華化](../Page/中國化.md "wikilink")」的爭論。

1601年，意大利耶稣会传教士利玛窦抵達[燕京](../Page/燕京.md "wikilink")，一度自喻為「西僧」，但他旋即發現中國文化由[儒家士大夫掌握](../Page/儒家.md "wikilink")。他相信要中國人接受天主教，必須從[士大夫階層着手](../Page/士大夫.md "wikilink")，他於是改稱為「西儒」，研習[儒家文明](../Page/儒家文明.md "wikilink")，穿起士大夫服饰，向中國人介紹[記憶術](../Page/記憶術.md "wikilink")、[地圖](../Page/地圖.md "wikilink")、[天文等西方技術](../Page/天文.md "wikilink")，以此表明他們並非文化低落的「西[夷](../Page/夷.md "wikilink")」。

[Confucius_Tang_Dynasty.jpg](https://zh.wikipedia.org/wiki/File:Confucius_Tang_Dynasty.jpg "fig:Confucius_Tang_Dynasty.jpg")
利玛窦容許中國教徒繼續[祭天](../Page/祭天.md "wikilink")、[祭祖](../Page/祭祖.md "wikilink")、[祭孔的舊俗](../Page/祭孔.md "wikilink")，利玛窦主張[中國人所謂的](../Page/中國人.md "wikilink")「天」和「上帝」本質上與天主教所說的「唯一真神」沒有分別，故祭天並無問題。而祭祀[祖先與](../Page/祖先.md "wikilink")[孔子](../Page/孔子.md "wikilink")，這些只屬緬懷先人與敬仰哲人的儀式，與信仰也沒有甚麼干涉；只要不摻入祈求、崇拜等迷信成分，本質上並沒有違反[天主教](../Page/天主教.md "wikilink")[教義](../Page/教義.md "wikilink")。利玛窦的傳教策略和方式，一直為之後到中國傳教的耶穌會士所遵從，是為「[利玛窦規矩](../Page/利玛窦規矩.md "wikilink")」

1610年，利玛窦[去世](../Page/去世.md "wikilink")，死前指定意大利人[龙华民接任教会中职务](../Page/龙华民.md "wikilink")，龍華民成為引發「礼仪之争」的第一人。他於1597年（明[万历二十五年](../Page/万历.md "wikilink")）进入中国，先在[韶州传教](../Page/韶州.md "wikilink")，1609年入[北京](../Page/北京.md "wikilink")，对利玛窦的思想和传教方法有不同看法，但利玛窦死後才提出。當他接任中国耶稣会总会长后，主张废除「天」、「上帝」、「天主」等词，一律采用译音，並指「天」是指苍苍之天，而「上帝」并不是代表造物主，主张應將「天主」依[拉丁文音译为](../Page/拉丁文.md "wikilink")「陡斯」或「斗斯」（即）；也有人主张只许用「天主」，而不能用「天」与「上帝」之称。

耶穌會教士雖然對兩派主張意見分歧，但为避免纷争鬧大，耶稣会決定焚毁五十多篇反对利玛窦的作品，统一该会立场。這次糾紛只成為會內事務，但到了1628年，在华传教士在[江苏](../Page/江苏.md "wikilink")[嘉定舉行会议](../Page/嘉定.md "wikilink")，讨论敬祖及Deus的译名问题。与会者意见很不一致，最終認為敬[孔祭祖问题應沿用](../Page/孔子.md "wikilink")「[利玛窦规矩](../Page/利玛窦规矩.md "wikilink")」；对于译名，则主张采用龙华民一派的音譯。

直至[道明會進入這場紛爭](../Page/道明會.md "wikilink")，禮儀之爭才正式升級。道明會在華傳教事業，起步較耶穌會略遲。當耶穌會教士在中國[朝廷及](../Page/朝廷.md "wikilink")[士大夫階層享有聲望時](../Page/士大夫.md "wikilink")，1631年1月2日或3日，道明會的[高奇神甫才從](../Page/高奇神甫.md "wikilink")[菲律賓抵達中國](../Page/菲律賓.md "wikilink")[福建北部的](../Page/福建.md "wikilink")[福安](../Page/福安.md "wikilink")，正式開始對華傳教，接任的[黎玉范神父向教廷報告](../Page/黎玉范.md "wikilink")，指責耶穌會寬容中國信徒祭祖、敬孔，終引起[羅馬介入](../Page/羅馬教廷.md "wikilink")。

当时耶稣会受[葡萄牙国王保护](../Page/葡萄牙国王.md "wikilink")，基地是[葡萄牙占据的](../Page/葡萄牙.md "wikilink")[澳门](../Page/澳门.md "wikilink")，道明會受[西班牙国王保护](../Page/西班牙国王.md "wikilink")，基地是[西班牙占据](../Page/西班牙.md "wikilink")[菲律宾](../Page/菲律宾.md "wikilink")[马尼拉](../Page/马尼拉.md "wikilink")。而葡西两国在海上对抗，关系紧张。

## 政治風波

### 羅馬早期反复立場

早期的礼仪之争仍是天主教徒的学术问题，並未演化成国家之间的政治议題，但隨著各在華天主教會的競爭加劇，修會各自尋求[羅馬](../Page/羅馬.md "wikilink")[教廷的支持](../Page/教廷.md "wikilink")，爭論才演化成政治事件。事件中，羅馬教廷的態度經歷多次轉變。

1643年，道明會傳教士[黎玉范返回欧洲](../Page/黎玉范.md "wikilink")，向罗马教廷传信部控告耶稣会士，提出**十七问**，要點概略如下：

[Retrato_del_Papa_Inocencio_X._Roma,_by_Diego_Velázquez.jpg](https://zh.wikipedia.org/wiki/File:Retrato_del_Papa_Inocencio_X._Roma,_by_Diego_Velázquez.jpg "fig:Retrato_del_Papa_Inocencio_X._Roma,_by_Diego_Velázquez.jpg")
1645年9月12日，罗马教廷经教皇[英诺森十世批准](../Page/英诺森十世.md "wikilink")，发布通諭禁止天主教徒参加祭祖祀孔，但到了1651年，耶稣会教士[卫匡国到罗马向教皇申辩](../Page/卫匡国.md "wikilink")，1656年教皇[亚历山大七世決定](../Page/亚历山大七世.md "wikilink")-{}-准许耶稣会士照他们的理解参加[祭孔等活动](../Page/祭孔.md "wikilink")，只要不妨碍教徒的根本信仰。
[Alexander_VII.jpg](https://zh.wikipedia.org/wiki/File:Alexander_VII.jpg "fig:Alexander_VII.jpg")

### 在華教士對立

1667年，因「[曆狱](../Page/曆狱.md "wikilink")」而被羁押在[广州的包括耶稣会](../Page/广州.md "wikilink")、道明會、[方济会会士共](../Page/方济会.md "wikilink")23人召开了一场长达四十天的会议，讨论在华传教的方针，最后通过的决议之一，是遵守1656年教皇的裁定。其中道明会士[闵明我](../Page/闵明我.md "wikilink")）始终持不同意见，在获释后立即返欧，并于1676年在[马德里出版](../Page/马德里.md "wikilink")《中国历史、政治、伦理和宗教概观》（）一书上册，三年后出版下册，抨击在华耶稣会士的传教方式，罗马的耶稣会总会于是紧急将该书寄至中国，并要求各地的会士传阅并提供驳斥的论据。罗马教廷经过讨论，决定不更改1656年的命令。

1687年，[法國國王](../Page/法國國王.md "wikilink")[路易十四派遣耶稣会士洪若翰](../Page/路易十四.md "wikilink")、李明、張誠、白晉、劉應以「國王數學家」的名义赴华，在經歷與葡萄牙籍傳教士的鬥爭後，[耶稣会在華法國傳教區終於成立](../Page/耶稣会在華法國傳教區.md "wikilink")，首任會長為張誠。该会成员大多贊成所谓“利玛窦规矩”。只有劉應持反對意見。

1693年（康熙三十二年）3月26日，[巴黎外方传教会的](../Page/巴黎外方传教会.md "wikilink")[颜珰主教打破各方妥协](../Page/颜珰.md "wikilink")，在他所管辖的[福建代牧区内](../Page/福建代牧区.md "wikilink")，发布了禁止中国教徒实行中国礼仪的禁令，自此争议迅速扩大，由纯宗教学术问题，逐渐演变成为清朝和[羅馬教廷之间的政治斗争](../Page/羅馬教廷.md "wikilink")。羅馬教廷在1701年（康熙四十年）和1719年（康熙五十八年）先后派[铎罗](../Page/铎罗.md "wikilink")（）和[嘉乐](../Page/嘉乐.md "wikilink")（）两位特使来华，期间也发布了一系列的禁教令。铎罗使华以失败而告终，自1720年12月31日起嘉乐来华后康熙接见嘉乐宗主教前后共十三次\[9\]，礼遇很隆，对于敬孔敬祖的问题，当面不愿多言，也不许嘉乐奏请遵行禁约。嘉乐宗主教因有了铎罗的经历，遇事很谨慎。看到事情不能转圆时，乃奏请回罗马\[10\]。

## 教宗禁令

[Clement_XI.jpg](https://zh.wikipedia.org/wiki/File:Clement_XI.jpg "fig:Clement_XI.jpg")
虽然之后的欧洲舆论认为，[儒教是一种世俗的](../Page/儒教.md "wikilink")[哲学而非](../Page/哲学.md "wikilink")[宗教](../Page/宗教.md "wikilink")——因为它不符合西方宗教的标准，但教宗坚持儒家的祭典与天主教教义冲突。\[11\]

1704年12月20日，他正式发布教旨：\[12\]

1705年（康熙四十四年）罗马教廷派特使[铎罗来华](../Page/铎罗.md "wikilink")，1707年铎罗在[江寧](../Page/江寧.md "wikilink")（今[南京市](../Page/南京市.md "wikilink")）发布禁令：

公元1715年，教宗克勉十一世又颁布《自[登基之日](../Page/登基.md "wikilink")》（**）\[13\]的[教宗通谕](../Page/教宗通谕.md "wikilink")，重申必须绝对遵守1704年的禁令，否则将受[绝罚](../Page/绝罚.md "wikilink")。所有传教士必须宣誓服从。\[14\]\[15\]\[16\]

教宗[本篤十四世於是在](../Page/本篤十四世.md "wikilink")1742年頒佈《自從上主聖意》（*Ex quo
singulari*），重申“自登基之日”禁約。这个通谕还禁止在华传教士讨论「礼仪」问题。本笃十四通谕宣布之后，“异论顿息，人心翕然，迄今二百年，各省传教神父，勿论何国何会，悉遵教皇谕旨，宣传天主正教，共遵一途，共守一道，毫无争执歧异之端，而圣教之至一至圣，亦愈彰明较著焉。”\[17\]

## 清廷的应对

1705年（康熙四十四年）罗马教廷派特使[铎罗来华](../Page/铎罗.md "wikilink")，1707年铎罗在[江寧](../Page/江寧.md "wikilink")（今[南京市](../Page/南京市.md "wikilink")）发布禁令，[康熙帝令将铎罗押往](../Page/康熙帝.md "wikilink")[澳门交葡萄牙人看管](../Page/澳门.md "wikilink")。康熙帝還嚴斥：「众西洋人，自今以后，若不遵[利玛窦规矩](../Page/利玛窦规矩.md "wikilink")，断不准在中国住，必逐回去。」1710年，铎罗死于澳门监狱中\[18\]，康熙帝为澄清中国礼仪之争，派遣法国天主教传教士[艾若瑟出使罗马教廷](../Page/艾若瑟.md "wikilink")，[樊守义随行](../Page/樊守义.md "wikilink")。兩人到达[罗马后](../Page/罗马.md "wikilink")，将康熙帝关于铎罗来华及中国礼节问题和西洋教务问题的旨意，详细向教皇呈述。教皇不愿艾若瑟返回中国，至1718年，罗马教皇收到康熙皇帝朱笔文书才放行\[19\]。

康熙六十年（1721年）康熙閱取[罗马教廷](../Page/罗马教廷.md "wikilink")[特使嘉乐所带来的](../Page/特使.md "wikilink")「自登基之日」禁約后说：

耶稣会士为此颇为担忧。因此在嘉乐宣布教皇谕旨时，附加了八条变通的办法：

[Benoit_XIV.jpg](https://zh.wikipedia.org/wiki/File:Benoit_XIV.jpg "fig:Benoit_XIV.jpg")\]\]

但这并没有使康熙皇帝改变主意，传旨曰：
1721年3月，嘉乐离华返欧。1733年，北京的两位主教向教徒宣布「嘉乐八条」及1715年教皇通谕，令其遵守。1735年，教皇[克勉十二世](../Page/克勉十二世.md "wikilink")（，
1730年－1740年在位）认为「嘉乐八条」与教义不合，宣布废除。

教宗[本篤十四世於是在](../Page/本篤十四世.md "wikilink")1742年頒佈《自從上主聖意》（*Ex quo
singulari*），重申“自登基之日”禁約。这个通谕还禁止在华传教士讨论「礼仪」问题。本笃十四通谕宣布之后，“异论顿息，人心翕然，迄今二百年，各省传教神父，勿论何国何会，悉遵教皇谕旨，宣传天主正教，共遵一途，共守一道，毫无争执歧异之端，而圣教之至一至圣，亦愈彰明较著焉。”\[20\]

教廷態度的變化令雍正帝不滿，諭旨說：
之后[雍正帝再次下令禁教](../Page/雍正帝.md "wikilink")。而[乾隆时期](../Page/乾隆.md "wikilink")，传教士虽在宫中受到很高礼遇，但仍不能在华展开传教。[嘉庆](../Page/嘉庆.md "wikilink")、[道光两朝继续执行禁教政策](../Page/道光.md "wikilink")，[中国天主教只能地下发展](../Page/中国天主教.md "wikilink")。

## 争议结束

1932年，[日本耶稣会所办的](../Page/大日本帝国.md "wikilink")[上智大学部分學生参拜](../Page/上智大学.md "wikilink")[靖国神社不行礼](../Page/靖国神社.md "wikilink")，即所谓“”，引起了当时的日本[军部的强烈反弹](../Page/军部.md "wikilink")。时任[广岛](../Page/广岛.md "wikilink")[主教](../Page/主教.md "wikilink")
Johannes
Ross（1875年－1969年）为解决这一问题，遂展开对教史的研究试图找出有利的案例和证据。结果发现1258年时教廷曾颁发一件通谕，允许教徒可以参加非天主教的仪式，由于此谕先于中国礼仪之争，因此对重视传统的罗马教会而言，具有相当的权威性。在几经讨论之后，罗马教廷发布通告，允许日本教徒在[神社中低头行礼](../Page/神社.md "wikilink")，因为此举“除了表示他们对祖国的热爱，对[天皇的忠诚外](../Page/天皇.md "wikilink")，别无他意”。

该事件为日后解决中国礼仪之争埋下了伏笔。1934年，[溥仪在日本](../Page/溥仪.md "wikilink")[关东军的扶植之下登基为](../Page/关东军.md "wikilink")[满洲国](../Page/满洲国.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")，将祭孔定为强制的文化政策。梵蒂冈因此再度面临两个多世纪以前的礼仪问题。由于满洲国政府在回复[吉林主教](../Page/吉林.md "wikilink")[高德惠的讯问时](../Page/高德惠.md "wikilink")，答复曰：“尊孔仪式的唯一目的，就是彰显对孔子的崇敬之情，绝对不带宗教的特质。”再加上有先前日本神社事件的前例，教宗[庇护十一世于](../Page/庇护十一世.md "wikilink")1935年发布命令，要求满洲国教区主教应谨慎确认祭孔无宗教特质，而神职人员在行过反对中国礼仪的宣誓之后，还应等待主教们的指导，以避免疑惑和争执。

[Piusxiib.jpg](https://zh.wikipedia.org/wiki/File:Piusxiib.jpg "fig:Piusxiib.jpg")\]\]
1939年，教宗[庇护十二世](../Page/庇护十二世.md "wikilink")（，1939年－1958年在位）颁布《众所皆知》（*Plane
compertum est*）通谕：

但教廷并不承认先前相关的通谕是错误的，而是认为过去宗教性的祭祖、祭孔观念，历经数百年后已变成了[世俗性活动](../Page/世俗.md "wikilink")，故可以被酌情允许。此一宣言对亚洲[儒家文化圈内的天主教徒亦是一种解放](../Page/儒家文化圈.md "wikilink")，如[越南天主教也在](../Page/越南天主教.md "wikilink")1964年针对祭拜祖先、[民族英雄和战争亡魂等问题上获得教宗的特许](../Page/民族英雄.md "wikilink")。

## 注释

## 参考文献

## 外部链接

  - [黃一農](../Page/黃一農.md "wikilink")：〈[被忽略的聲音──介紹中國天主教徒對「禮儀問題」態度的文獻](https://web.archive.org/web/20140827082629/http://hss.nthu.edu.tw/~ylh/uploadfiles/paper119_0.pdf)〉.
  - 吳旻、韓崎：〈[禮儀之爭與中國天主教徒](http://www1.ihns.ac.cn/members/hanqi/pdf.databank/2005-02.pdf)〉。

## 参见

  - [中国基督教](../Page/中国基督教.md "wikilink")：[中国基督教史](../Page/中国基督教史.md "wikilink")
  - [罗马天主教会](../Page/罗马天主教会.md "wikilink")：[圣座外交](../Page/圣座外交.md "wikilink")
  - [中国天主教](../Page/中国天主教.md "wikilink")、[中梵关系](../Page/中梵关系.md "wikilink")
  - [明朝天主教](../Page/明朝天主教.md "wikilink")：[南京教案](../Page/南京教案.md "wikilink")
  - [康熙历狱](../Page/康熙历狱.md "wikilink")、[清中期禁教](../Page/清中期禁教.md "wikilink")、[清朝外交](../Page/清朝外交.md "wikilink")
  - [耶稣会](../Page/耶稣会.md "wikilink")：[在华耶稣会士列表](../Page/在华耶稣会士列表.md "wikilink")
  - [反基督教](../Page/反基督教.md "wikilink")：[反天主教](../Page/反天主教.md "wikilink")、[教案
    (宗教)](../Page/教案_\(宗教\).md "wikilink")

{{-}}

[中国礼仪之争](../Category/中国礼仪之争.md "wikilink")
[Category:中梵关系](../Category/中梵关系.md "wikilink")
[Category:明朝天主教](../Category/明朝天主教.md "wikilink")
[Category:清朝天主教](../Category/清朝天主教.md "wikilink")
[Category:中国天主教相关争议](../Category/中国天主教相关争议.md "wikilink")
[Category:清圣祖](../Category/清圣祖.md "wikilink")
[Category:康熙](../Category/康熙.md "wikilink")
[Category:雍正](../Category/雍正.md "wikilink")
[Category:乾隆](../Category/乾隆.md "wikilink")
[Category:嘉庆](../Category/嘉庆.md "wikilink")
[Category:道光](../Category/道光.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.  孙尚扬、钟明旦：《1840年前的中国基督教》，学苑出版社，2004年第328页。

9.  [中梵外交关系史，81页](http://books.google.com/books?id=bauZSll2dbgC&lpg=PA81&hl=zh-CN&pg=PA81#v=onepage&q&f=false).
    台湾商务印书馆发行.

10. 羅光：《教廷與中國使節史》

11.

12.

13. 《自登基之日》（**）载《中国礼仪之争西文文献一百篇》，[上海古籍出版社](../Page/上海古籍出版社.md "wikilink")
    2001年。

14.
15. [中國教會的禮儀之爭（1715年）](http://www.peterpoon.idv.hk/history/conflict.htm)

16. [现代欧洲中心论者对莱布尼茨的抱怨](http://www.lixue.org/Leibniz/L2.2/L2.2.html)

17. 萧若瑟：《天主教传行中国考》，第340页。

18. [阎宗临](../Page/阎宗临.md "wikilink")：《康熙与克莱芒十一世》，《中外交通史》

19. 阎宗临：《[身见录校后记](../Page/身见录.md "wikilink")》

20. 萧若瑟：《天主教传行中国考》，第340页。