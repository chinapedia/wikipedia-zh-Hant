**中国社会科学院**（，[縮寫為](../Page/縮寫.md "wikilink")），简称**社科院**，是[中华人民共和国在](../Page/中华人民共和国.md "wikilink")[哲学](../Page/哲学.md "wikilink")、[社会科学领域研究的最高的和最全面的国家级学术机构与综合研究中心](../Page/社会科学.md "wikilink")，为正部级[国务院直属事业单位](../Page/国务院直属事业单位.md "wikilink")。

中国社会科学院成立于1977年5月，前身是[中国科学院哲学社会科学部](../Page/中国科学院.md "wikilink")。社科院主要研究对象为中国与世界各国的[语言](../Page/语言.md "wikilink")、[哲学](../Page/哲学.md "wikilink")、[法学](../Page/法学.md "wikilink")、[经济](../Page/经济.md "wikilink")、[宗教](../Page/宗教.md "wikilink")、[民族](../Page/民族.md "wikilink")、[考古](../Page/考古.md "wikilink")、[历史和](../Page/历史.md "wikilink")[文学等问题](../Page/文学.md "wikilink")。目前，社科院內共分為6个學部，下轄37個研究所。

中国社会科学院在中国国内外具有较高的学术影响力。美国[宾夕法尼亚大学智库研究项目](../Page/宾夕法尼亚大学.md "wikilink")（TTCSP）研究编写的《全球智库报告2015》中，中國社會科學院排名全球第31名，為中國排名最高的[智庫](../Page/智庫.md "wikilink")\[1\]。

## 沿革

中国社会科学学术机构的历史可追溯到自[唐朝开始设立的最高学术机构](../Page/唐朝.md "wikilink")[翰林院](../Page/翰林院.md "wikilink")。

1949年11月，中国科学院在[北京成立](../Page/北京.md "wikilink")。[中国科学院哲学社会科学部最初设有语言研究所](../Page/中国科学院哲学社会科学部.md "wikilink")、哲学研究所、法学研究所、经济研究所、世界经济研究所、民族研究所、世界宗教研究所、考古研究所、历史研究所、近代史研究所、世界历史研究所、文学研究所、外国文学研究所和情报资料研究室等14个研究单位，总人数2200多人。

1977年5月，经中央决定，组建“中国社会科学院”，不再保留“中国科学院哲学社会科学部”。中国社会科学院建院之后先后成立了工业经济研究所等16个研究和出版单位。苏联东欧研究所、西亚非洲研究所和拉丁美洲研究所亦在建院初期划归中国社会科学院。

1981年以后，社科院又成立了数量与技术经济研究所等6个科研院所，研究所总数扩张到31个，研究中心则有45个。

## 职责

根据《》，社科院承担下列职责：

## 机构设置

根据《》，社科院设置下列机构：

### 内设机构

### 直属事业单位

#### 直属研究单位

  - [文哲学部](../Page/中国社会科学院文哲学部.md "wikilink")

<!-- end list -->

  - [语言研究所](../Page/中国社会科学院语言研究所.md "wikilink")
  - [哲学研究所](../Page/中国社会科学院哲学研究所.md "wikilink")
  - [文学研究所](../Page/中国社会科学院文学研究所.md "wikilink")
  - [民族文学研究所](../Page/中国社会科学院民族文学研究所.md "wikilink")
  - [外国文学研究所](../Page/中国社会科学院外国文学研究所.md "wikilink")
      - [文化研究中心](../Page/中国社会科学院文化研究中心.md "wikilink")
  - [世界宗教研究所](../Page/中国社会科学院世界宗教研究所.md "wikilink")

<!-- end list -->

  - [历史学部（中国历史研究院）](../Page/中国社会科学院历史学部.md "wikilink")

<!-- end list -->

  - [考古研究所](../Page/中国社会科学院考古研究所.md "wikilink")
  - [历史研究所](../Page/中国社会科学院历史研究所.md "wikilink")
  - [近代史研究所](../Page/中国社会科学院近代史研究所.md "wikilink")
  - [世界历史研究所](../Page/中国社会科学院世界历史研究所.md "wikilink")
  - [中国边疆研究所](../Page/中国社会科学院中国边疆研究所.md "wikilink")
  - [台湾研究所](../Page/中国社会科学院台湾研究所.md "wikilink")

<!-- end list -->

  - [马克思主义研究学部](../Page/中国社会科学院马克思主义研究学部.md "wikilink")

<!-- end list -->

  - [马克思主义研究院](../Page/中国社会科学院马克思主义研究院.md "wikilink")
  - [中国特色社会主义理论体系研究中心](../Page/中国特色社会主义理论体系研究中心.md "wikilink")
  - [当代中国研究所](../Page/中国社会科学院当代中国研究所.md "wikilink")
      - [当代中国出版社](../Page/当代中国出版社.md "wikilink")
  - [信息情报研究院](../Page/中国社会科学院信息情报研究院.md "wikilink")

{{-}}

  - ; [社会政法学部](../Page/中国社会科学院社会政法学部.md "wikilink")

<!-- end list -->

  - [法学研究所](../Page/中国社会科学院法学研究所.md "wikilink")
  - [国际法研究所](../Page/中国社会科学院国际法研究所.md "wikilink")
  - [政治学研究所](../Page/中国社会科学院政治学研究所.md "wikilink")
  - [社会学研究所](../Page/中国社会科学院社会学研究所.md "wikilink")
  - [社会发展战略研究院](../Page/中国社会科学院社会发展战略研究院.md "wikilink")
  - [新闻与传播研究所](../Page/中国社会科学院新闻与传播研究所.md "wikilink")
  - [民族学与人类学研究所](../Page/中国社会科学院民族学与人类学研究所.md "wikilink")

<!-- end list -->

  - [国际研究学部](../Page/中国社会科学院国际研究学部.md "wikilink")

<!-- end list -->

  - [世界经济与政治研究所](../Page/中国社会科学院世界经济与政治研究所.md "wikilink")
  - [俄罗斯东欧中亚研究所](../Page/中国社会科学院俄罗斯东欧中亚研究所.md "wikilink")
  - [欧洲研究所](../Page/中国社会科学院欧洲研究所.md "wikilink")
  - [西亚非洲研究所](../Page/中国社会科学院西亚非洲研究所.md "wikilink")
  - [拉丁美洲研究所](../Page/中国社会科学院拉丁美洲研究所.md "wikilink")
  - [亚太与全球战略研究院](../Page/中国社会科学院亚太与全球战略研究院.md "wikilink")
  - [美国研究所](../Page/中国社会科学院美国研究所.md "wikilink")
  - [日本研究所](../Page/中国社会科学院日本研究所.md "wikilink")
  - [和平发展研究所](../Page/中国社会科学院和平发展研究所.md "wikilink")

<!-- end list -->

  - [经济学部](../Page/中国社会科学院经济学部.md "wikilink")

<!-- end list -->

  - [经济研究所](../Page/中国社会科学院经济研究所.md "wikilink")
  - [工业经济研究所](../Page/中国社会科学院工业经济研究所.md "wikilink")
  - [农村发展研究所](../Page/中国社会科学院农村发展研究所.md "wikilink")
  - [财经战略研究院](../Page/中国社会科学院财经战略研究院.md "wikilink")
  - [金融研究所](../Page/中国社会科学院金融研究所.md "wikilink")
  - [数量经济与技术经济研究所](../Page/中国社会科学院数量经济与技术经济研究所.md "wikilink")
  - [人口与劳动经济研究所](../Page/中国社会科学院人口与劳动经济研究所.md "wikilink")
  - [城市发展与环境研究所](../Page/中国社会科学院城市发展与环境研究所.md "wikilink")

#### 直属高等学校

  - [中国社会科学院大学](../Page/中国社会科学院大学.md "wikilink")
      - [中国社会科学院研究生院](../Page/中国社会科学院研究生院.md "wikilink")

#### 其他直属事业单位

### 直属企业单位

  - [中国人文科学发展公司（中国经济技术研究咨询有限公司）](../Page/中国人文科学发展公司.md "wikilink")
  - [中国经营出版传媒集团](../Page/中国经营出版传媒集团.md "wikilink")

## 组织人事

### 历任院长

[Chinese_Academy_of_Social_Sciences_building_photo_from_VOA.jpg](https://zh.wikipedia.org/wiki/File:Chinese_Academy_of_Social_Sciences_building_photo_from_VOA.jpg "fig:Chinese_Academy_of_Social_Sciences_building_photo_from_VOA.jpg")

1.  [胡乔木](../Page/胡乔木.md "wikilink")（1977-1982）
2.  [马　洪](../Page/马洪.md "wikilink")（1982-1985）
3.  [胡乔木](../Page/胡乔木.md "wikilink")（1985-1988）
4.  [胡　绳](../Page/胡绳.md "wikilink")（1988-1998）
5.  [李铁映](../Page/李铁映.md "wikilink")（1998-2003）
6.  [陈奎元](../Page/陈奎元.md "wikilink")（2003-2013）
7.  [王伟光](../Page/王伟光.md "wikilink")（2013-2018）\[2\]
8.  [谢伏瞻](../Page/谢伏瞻.md "wikilink")（2018-）

### 学部委员

下列名单列出历次评选出的中国科学院哲学社会科学部学部委员、中国社会科学院学部委员，依姓氏笔划顺序排列：

#### 中國科學院哲學社會科學部学部委员

中国科学院学部成立于1955年，是中国国家最高科学技术咨询机构，原设哲学社会科学部。1977年，中国社会科学院组建，中科院哲学社会科学部相应撤销。

#### 學部委員

中国社会科学院于2006年设置6个学部，并组织学部委员、荣誉学部委员评选\[3\]。社科院学部委员为中华人民共和国最高社會科學荣誉之一，每四年增选一次。

## 参见

  - [中国科学院](../Page/中国科学院.md "wikilink")
  - [中国工程院](../Page/中国工程院.md "wikilink")
  - [中国社会科学院大学](../Page/中国社会科学院大学.md "wikilink")
  - [中國社會科學院研究生院](../Page/中國社會科學院研究生院.md "wikilink")
  - [中央文史研究馆](../Page/中央文史研究馆.md "wikilink")

## 参考文献

## 外部链接

  - [官方網站](http://www.cssn.cn/)

[Category:中华人民共和国社会科学研究机构](../Category/中华人民共和国社会科学研究机构.md "wikilink")
[Category:正部级国务院直属事业单位](../Category/正部级国务院直属事业单位.md "wikilink")
[中国社会科学院](../Category/中国社会科学院.md "wikilink")
[Category:国家社会科学院](../Category/国家社会科学院.md "wikilink")
[Category:中国智库](../Category/中国智库.md "wikilink")
[Category:1977年中國建立](../Category/1977年中國建立.md "wikilink")

1.
2.
3.