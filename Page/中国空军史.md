## 清末

[Chinese_army_balloon_LCCN2014688610.jpg](https://zh.wikipedia.org/wiki/File:Chinese_army_balloon_LCCN2014688610.jpg "fig:Chinese_army_balloon_LCCN2014688610.jpg")

  - 1901年，《皇朝經濟文編》刊載《飛機考》，作為中國最早介紹飛機的文章。\[1\]

<!-- end list -->

  - 1905年，[湖廣總督](../Page/湖廣總督.md "wikilink")[張之洞從](../Page/張之洞.md "wikilink")[日本購進](../Page/日本.md "wikilink")2個[山田式偵察氣球](../Page/山田式偵察氣球.md "wikilink")。該型[氫氣球為橢圓形](../Page/氫氣球.md "wikilink")，直徑3米、長10米，人在空中利用[旗語或籃中的](../Page/旗語.md "wikilink")[電話和地面聯繫](../Page/電話.md "wikilink")，有指揮、偵察及傳遞信息的作用，作為新式軍械，曾在[武昌](../Page/武昌.md "wikilink")[閱馬場東兵營演放](../Page/閱馬場.md "wikilink")。\[2\]

<!-- end list -->

  - 1908年1月2日，[湖北陸軍第](../Page/湖北.md "wikilink")8鎮氣球隊成立，[工兵營營長](../Page/工兵.md "wikilink")[王永泉兼隊長](../Page/王永泉.md "wikilink")，並聘日籍教練，配備山田式氣球具。同年五、六月，[江蘇陸軍第](../Page/江蘇.md "wikilink")9
    鎮氣球隊、[直隸陸軍第](../Page/直隸.md "wikilink")4鎮氣球隊相繼成立，分別由工兵營營長[鄧質儀](../Page/鄧質儀.md "wikilink")、[高凝辰兼任隊長](../Page/高凝辰.md "wikilink")，各有1具山田式氣球。

<!-- end list -->

  - 1909年，清廷[軍咨處](../Page/軍咨處.md "wikilink")、[海軍處](../Page/海軍處.md "wikilink")、[陸軍部正式頒布](../Page/陸軍部.md "wikilink")《陸軍氣球預備法》，制定了購買氣球、培養人才、各省組織氣球偵察隊等規定。同年2月，[陸軍大學堂編印了](../Page/陸軍大學堂.md "wikilink")《氣球學》，講授氣球升空的原理，並教授士兵如何操控氣球。

<!-- end list -->

  - 1908年10月，自“河間秋操”、“彰德秋操”之後，清廷又在[安徽](../Page/安徽.md "wikilink")[太湖籌劃了](../Page/太湖县.md "wikilink")“太湖秋操”。清廷派陸軍部[右侍郎](../Page/右侍郎.md "wikilink")[蔭昌](../Page/蔭昌.md "wikilink")、[兩江總督](../Page/兩江.md "wikilink")[端方為檢閱大臣](../Page/端方.md "wikilink")，以檢閱[南洋地區包括湖北第](../Page/南洋.md "wikilink")8鎮、江蘇第9鎮、安徽第31混成協等各鎮新軍編練成效。這次演習中的特色是首次使用氣球。新成立的氣球偵察隊奉命參加操練，施放偵察氣球。

<!-- end list -->

  - 1910年2月,清政府派人出國考察軍事,考察團成員[徐元甫](../Page/徐元甫.md "wikilink")、[田凱亭在日本曾乘坐軍用](../Page/田凱亭.md "wikilink")[熱氣球](../Page/熱氣球.md "wikilink"),並實施了無地面纜繩的空中飄行，成為最早乘坐自由氣球飛行的中國人。\[3\]

<!-- end list -->

  - 1910年，清政府在[北京](../Page/北京.md "wikilink")[南范建立中國最早的航空機構](../Page/南范.md "wikilink")，為中國空軍的開端。\[4\]

## 中華民國

### 北洋政府时期

[Caudron_G.3_in_Chinese_service.jpg](https://zh.wikipedia.org/wiki/File:Caudron_G.3_in_Chinese_service.jpg "fig:Caudron_G.3_in_Chinese_service.jpg")
[The_Rosamonde.jpg](https://zh.wikipedia.org/wiki/File:The_Rosamonde.jpg "fig:The_Rosamonde.jpg")

  - 1913年，成立[南苑航空學校以及附設修理工廠](../Page/南苑航空學校.md "wikilink")，购买[法国](../Page/法国.md "wikilink")[高德隆式戰機](../Page/高德隆式戰機.md "wikilink")12架，最大時速96千米，起飛距離100米，正式组建空军。

<!-- end list -->

  - 1913年，北洋政府[陸軍部於北京設立](../Page/陸軍部.md "wikilink")「航空研究所」。\[5\]

<!-- end list -->

  - 1913年開始正式將飛機用於軍事。當時蒙古發生動亂，北洋政府派出一架飛機到[多倫偵察兼示威](../Page/多倫.md "wikilink")。\[6\]

<!-- end list -->

  - 1914年4月展開第一次空襲行動，為北洋政府對[白朗起義軍的鎮壓](../Page/白朗.md "wikilink")。南苑航校校長[秦國鏞親率](../Page/秦國鏞.md "wikilink")4架飛機轟炸白朗軍，然其效用不明。\[7\]

<!-- end list -->

  - 1915年，南苑附設修理工廠用外國[發動機設計製造發動機後置型的陸上飛機](../Page/發動機.md "wikilink")。

<!-- end list -->

  - 1917年6月[张勋复辟时](../Page/张勋复辟.md "wikilink")，[段祺瑞曾经派飞机轰炸过紫禁城與張勛住宅及豐台兵營](../Page/段祺瑞.md "wikilink")。此后空军在内战如[直奉战争中频频显露身手](../Page/直奉战争.md "wikilink")。

<!-- end list -->

  - 1918年，北洋政府[海軍部於](../Page/海軍部.md "wikilink")[福州船政局創辦](../Page/福州船政局.md "wikilink")[海軍飛機工程部](../Page/海軍飛機工程部.md "wikilink")，為第一個正規的中國軍用飛機工廠。

<!-- end list -->

  - 1918年，北洋政府海軍部在[福建](../Page/福建.md "wikilink")[馬尾成立](../Page/馬尾.md "wikilink")[飛潛學校](../Page/飛潛學校.md "wikilink")，為製造[海軍飛機和培養相關人才的先驅](../Page/海軍飛機.md "wikilink")。

<!-- end list -->

  - 1920年以後，各地军阀纷纷组建自有的航空學校或[航空隊](../Page/航空隊.md "wikilink")，以及軍械或機械班，從而培養人才。

<!-- end list -->

  - 1921年，[中華航空協會成立](../Page/中華航空協會.md "wikilink")。

<!-- end list -->

  - 1922年，[上海](../Page/上海.md "wikilink")[江南造船所建成世界上第一個浮動廠棚](../Page/江南造船所.md "wikilink")（[水上機場](../Page/水上機場.md "wikilink")）。

<!-- end list -->

  - 1923年，[北京航空協會成立](../Page/北京航空協會.md "wikilink")。

<!-- end list -->

  - 1924年，北洋政府[航空署成立](../Page/航空署.md "wikilink")「北京航空學校」。\[8\]

<!-- end list -->

  - 1925年，[廣東航空同志會成立](../Page/廣東航空同志會.md "wikilink")。\[9\]

### 南京國民政府时期

[The_gate_of_the_Central_Aviation_School.jpg](https://zh.wikipedia.org/wiki/File:The_gate_of_the_Central_Aviation_School.jpg "fig:The_gate_of_the_Central_Aviation_School.jpg")校門\]\]
[The_Welding_Gang.jpg](https://zh.wikipedia.org/wiki/File:The_Welding_Gang.jpg "fig:The_Welding_Gang.jpg")前留影\]\]

  - 1928年
    南京[國民政府中央陸軍軍官學校於](../Page/國民政府.md "wikilink")[南京創立](../Page/南京.md "wikilink")[航空班](../Page/航空班.md "wikilink")。

<!-- end list -->

  - 1931年
    [航空班遷至杭州筧橋](../Page/航空班.md "wikilink")，航空班擴編改制為航空學校，並由德籍顧問協助各項飛行訓練工作。

<!-- end list -->

  - 1932年
    更名為「**中央航空學校**」，校址定於[杭州](../Page/杭州.md "wikilink")[筧橋](../Page/筧橋.md "wikilink")。

<!-- end list -->

  - 1934年將航空署改組為航空委員會，主責空軍發展。

<!-- end list -->

  - 1936年[清華大學成立](../Page/清華大學.md "wikilink")[航空研究所](../Page/航空研究所.md "wikilink")，中日戰爭中和[西南聯大航空系合併成立](../Page/西南聯大.md "wikilink")[航空研究院](../Page/航空研究院.md "wikilink")。

<!-- end list -->

  - 1937年正式制訂空軍[軍旗樣式](../Page/軍旗.md "wikilink")，原直屬於[陸軍總司令部的空軍自此成為獨立軍種](../Page/陸軍總司令部.md "wikilink")。\[10\]

<!-- end list -->

  - 1937年[淞沪会战](../Page/淞沪会战.md "wikilink")，中國空軍在時任空軍前敵總司令部防空總臺長[陳一白少将用无线电精确指挥下](../Page/陳一白.md "wikilink")，第一次击落外国战机，称为[八一四空战](../Page/八一四空战.md "wikilink")，以后8月14日被命名为[空军节](../Page/空军节.md "wikilink")。

<!-- end list -->

  - 1937年10月美國人[舒米德](../Page/舒米德.md "wikilink")（）來中國參與中國空軍，陸續有各國飛行員加入，並在[漢口組織編](../Page/漢口.md "wikilink")[空軍第14隊](../Page/空軍第14隊.md "wikilink")。後因該隊的指揮問題，政府於1938年3月裁撤。\[11\]\[12\]

<!-- end list -->

  - 1938年4月政府以半年為期，雇用法籍飛行員組成[空軍第41隊](../Page/空軍第41隊.md "wikilink")，因軍紀及作戰表現於同年10月起解聘。\[13\]

<!-- end list -->

  - [抗戰爆發後遷至](../Page/中國抗日戰爭.md "wikilink")[雲南](../Page/雲南.md "wikilink")[昆明](../Page/昆明.md "wikilink")，並於1938年正式定名為「**空軍軍官學校**」。

<!-- end list -->

  - 抗战中，中國空軍大量接受[苏联和](../Page/苏联.md "wikilink")[美国的军事援助](../Page/美国.md "wikilink")，并且组建了[苏联航空志愿队和美國志願大隊](../Page/苏联航空志愿队.md "wikilink")[飛虎隊有力的支援了](../Page/飛虎隊.md "wikilink")[抗战](../Page/抗战.md "wikilink")。美國志願大隊改制為美軍陸航的第十四航空隊，曾於1943年11月25日以超低空飛行越過台灣海峽，突襲日本陸航新竹基地，摧毀日軍陸航各式戰機達50至60餘架。對日抗战期间，中國空軍出现出一批[王牌飞行员包括](../Page/王牌飞行员.md "wikilink")[高志航](../Page/高志航.md "wikilink")、[李桂丹](../Page/李桂丹.md "wikilink")、[樂以琴](../Page/樂以琴.md "wikilink")、[陳瑞鈿](../Page/陳瑞鈿.md "wikilink")、[刘粹刚](../Page/刘粹刚.md "wikilink")、[柳哲生](../Page/柳哲生.md "wikilink")、[王光复等](../Page/王光复.md "wikilink")。

<!-- end list -->

  - 1938年5月19日下午3时，中國空軍[徐煥升](../Page/徐煥升.md "wikilink")、[佟彦博驾兩架](../Page/佟彦博.md "wikilink")[B-10由漢口起飛](../Page/B-10.md "wikilink")，[陳衣凡駕僅剩最後一架](../Page/陳衣凡.md "wikilink")[He
    111A轟炸機擔任後勤支援運輸](../Page/He_111轟炸機.md "wikilink")，向日本[熊本和](../Page/熊本.md "wikilink")[福岡等大城市投下傳單](../Page/福岡.md "wikilink")，向日本國民揭露有關日軍在華侵略，最後三機安全返回。此事不單轟動全中國，也上了英美報紙新聞，美國報章更登出日機在中國狂轟濫炸的相片作為比較。這是中國空軍第一次跨国境任务。

<!-- end list -->

  - 抗戰勝利後「**空軍軍官學校**」遷回筧橋，之後因[國共內戰爆發](../Page/國共內戰.md "wikilink")，「**空軍軍官學校**」於1949年隨國民政府撤退至台灣，並於[高雄縣](../Page/高雄縣.md "wikilink")[岡山鎮](../Page/岡山區.md "wikilink")（今高雄市岡山區）現址復校。

<!-- end list -->

  - 1941年美國宣戰並協助中國空軍作戰以後，曾多次派機轟炸臺灣，並有部分美機在台墜落，飛行員遭日軍逮捕成為俘虜，戰後政府即要求駐台日軍善後連絡部部長[安籐利吉呈報被俘虜的飛行員狀況](../Page/安籐利吉.md "wikilink")。\[14\]

## 國民政府空軍（台灣）

## 解放军空军 1949年－至今

## 參考資料

[中国空军史](../Page/category:中国空军史.md "wikilink")

1.

2.

3.

4.
5.

6.

7.
8.
9.

10.
11.

12.

13.
14.