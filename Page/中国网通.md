[China_Netcom_Corporation_中国網絡通信A075919CNC.jpg](https://zh.wikipedia.org/wiki/File:China_Netcom_Corporation_中国網絡通信A075919CNC.jpg "fig:China_Netcom_Corporation_中国網絡通信A075919CNC.jpg")

**中國網絡通信集團公司**（簡稱**中國網通**、**CNC**）現為中國綜合[電信商](../Page/電信.md "wikilink")[中國聯通的一部份](../Page/中國聯通.md "wikilink")；合併前為中國[固網電信商](../Page/固網電信.md "wikilink")。

## 歷史

中國網通拆分自[中國電信於中國北方的業務](../Page/中國電信.md "wikilink")，主要是提供全面的[固網電信業務服務網絡](../Page/固網電信.md "wikilink")，主要覆蓋中國北方十個[省](../Page/省.md "wikilink")、[自治区](../Page/自治区.md "wikilink")、[直轄市](../Page/直轄市.md "wikilink")，當中包括[北京](../Page/北京.md "wikilink")、[天津](../Page/天津.md "wikilink")、[山東](../Page/山東.md "wikilink")、[辽宁](../Page/辽宁.md "wikilink")、[黑龙江](../Page/黑龙江.md "wikilink")、[吉林](../Page/吉林.md "wikilink")、[河北](../Page/河北.md "wikilink")、[山西](../Page/山西.md "wikilink")、[河南](../Page/河南.md "wikilink")、[内蒙古等](../Page/内蒙古.md "wikilink")，包括固定電話服務、[寬頻和其他](../Page/寬頻.md "wikilink")[互聯網相關服務](../Page/互聯網.md "wikilink")，以及商務與[數據通信服務](../Page/數據通信.md "wikilink")；公司亦曾一度經營國際電信服務（其前身為[亞洲環球電訊](../Page/亞洲環球電訊.md "wikilink")
Asia Global
Crossing），但在2006年售予以[馬世民為首的投資基金](../Page/馬世民.md "wikilink")。中國網通的母公司亦持有香港主要綜合電訊服務供應商之一[電訊盈科的](../Page/電訊盈科.md "wikilink")20%權益，為其第二大股東。中國網通曾在[香港交易所和](../Page/香港交易所.md "wikilink")[紐約證券交易所上市](../Page/紐約證券交易所.md "wikilink")，前上市編號0906（香港）/CN（紐約）。

公司在[香港註冊](../Page/香港.md "wikilink")，上市公司總裁(CEO)為[田溯寧](../Page/田溯寧.md "wikilink")，主席為[左迅生](../Page/左迅生.md "wikilink")，控股股東為中國政府，西班牙主要電訊公司
[Telefónica](../Page/Telefónica.md "wikilink")
則為公司的[策略性股東](../Page/策略性股東.md "wikilink")。

2008年10月6日起，中國網通將[併入](../Page/2008年中國電訊業重組.md "wikilink")[中國聯通](../Page/中國聯通.md "wikilink")，取消在[香港交易所及](../Page/香港交易所.md "wikilink")[紐約證券交易所的上市地位](../Page/紐約證券交易所.md "wikilink")。

中國網通於香港聯交所買賣網通股份的最後日期及於紐約證券交易所買賣網通美國託存股份的最後日期預期為二零零八年十月六日（星期一）。\[1\]

2008年10月14日，[香港高等法院宣布同意](../Page/香港高等法院.md "wikilink")[中国联通和中国网通合并](../Page/中国联通.md "wikilink")，10月15日，中国联通和中国网通宣布合并成功，在北京成立新的[中国联合网络通信有限公司](../Page/中国联合网络通信有限公司.md "wikilink")（英文仍沿用中国联通的China
Unicom，标志亦为联通标志，但标志字体与联通原有标志不同），作为上市公司中国联合网络通信（香港）股份有限公司（China
Unicom（Hong Kong）Limited）在中国大陆的运营公司。\[2\]\[3\]

## 参见

  - [2008年中國電信業重組](../Page/2008年中國電信業重組.md "wikilink")
  - [中国联通](../Page/中国联通.md "wikilink")

## 参考文献

## 外部連結

  - [中國網通](https://web.archive.org/web/20061129014718/http://www.cnc.cn/)

[Category:中华人民共和国电信业中央企业](../Category/中华人民共和国电信业中央企业.md "wikilink")
[Category:总部位于北京市的中华人民共和国中央企业](../Category/总部位于北京市的中华人民共和国中央企业.md "wikilink")
[Category:中華人民共和國已撤銷中央企業](../Category/中華人民共和國已撤銷中央企業.md "wikilink")
[Category:中国联通](../Category/中国联通.md "wikilink")
[Category:前恒生指數成份股](../Category/前恒生指數成份股.md "wikilink")
[Category:前恒生香港中資企業指數成份股](../Category/前恒生香港中資企業指數成份股.md "wikilink")
[Category:香港交易所已除牌公司](../Category/香港交易所已除牌公司.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")
[Category:1999年成立的公司](../Category/1999年成立的公司.md "wikilink")
[Category:2008年結業公司](../Category/2008年結業公司.md "wikilink")
[Category:中國互聯網服務提供商](../Category/中國互聯網服務提供商.md "wikilink")

1.  [因應聯通網通合併，恆指系列成份股變動提前至下周一收市後生效](http://hk.news.yahoo.com/article/080930/9/8h91.html)
2.  [中国联通 中国网通联合公告](http://www.cnc.cn/mj/news.asp?Unid=12335)
3.  [中国网通 中国联通红筹公司合并成功
    中国联合网络通信有限公司在京宣告成立](http://www.cnc.cn/mj/news.asp?Unid=12335)