**中国航海日**，是由[中国国务院于](../Page/中华人民共和国国务院.md "wikilink")[2005年5月批准的一项](../Page/2005年5月.md "wikilink")[纪念日](../Page/纪念日.md "wikilink")。国务院决定自2005年起每年[7月11日定为](../Page/7月11日.md "wikilink")[中国的航海日](../Page/中国.md "wikilink")，同时作为[世界海事日在中国的实施日期](../Page/世界海事日.md "wikilink")。

2005年7月11日是[明朝](../Page/明朝.md "wikilink")[航海家](../Page/航海家.md "wikilink")[郑和下西洋十](../Page/郑和下西洋.md "wikilink")[甲子](../Page/甲子.md "wikilink")600周年的纪念日，因此有了上述决定。同时还决定在[上海兴建综合性航海博物馆](../Page/上海.md "wikilink")──[中国航海博物馆](../Page/中国航海博物馆.md "wikilink")，并且于每年航海日免费向公众开放。2014年航海日首次发布中国航海日公告，呼吁社会各界对航海事业发展给予更多的关注和支持。\[1\]

航海日是由[中国政府主导](../Page/中华人民共和国政府.md "wikilink")，全民参加的法定纪念日活动。中国设立此纪念日的初衷在于：纪念郑和下西洋；宣传、发扬海事工作；加快中国海洋事业发展。

## 历年主题

  - [2005年](../Page/2005年.md "wikilink") 热爱祖国、睦邻友好、科学航海
  - [2006年](../Page/2006年.md "wikilink") 爱我蓝色国土，发展航海事业
  - [2007年](../Page/2007年.md "wikilink") 落实科学发展观，构建海洋和谐
  - [2008年](../Page/2008年.md "wikilink") 中国航海－改革开放30年暨国际海事组织－为海运服务60年
  - [2009年](../Page/2009年.md "wikilink") 庆祝新中国60周年·迎接航海新挑战
  - [2010年](../Page/2010年.md "wikilink") 海洋 海峡 海员
  - [2011年](../Page/2011年.md "wikilink") 兴海护海 舟行天下
  - [2012年](../Page/2012年.md "wikilink") 感知郑和 拥抱海洋
  - [2013年](../Page/2013年.md "wikilink") 通江达海 兴海强国
  - [2014年](../Page/2014年.md "wikilink") 大航海 新丝路
  - [2015年](../Page/2015年.md "wikilink") 新絲路引領新常態 航海夢共築中國夢
  - [2016年](../Page/2016年.md "wikilink") 建安全高效綠色航運 助海上絲路創新發展

## 历届论坛举办城市

  - 第一届 [北京](../Page/北京.md "wikilink")
  - 第二届 [上海](../Page/上海.md "wikilink")
  - 第三届 山东[青岛](../Page/青岛.md "wikilink")
  - 第四届 江苏[太仓](../Page/太仓.md "wikilink")
  - 第五届 辽宁[大连](../Page/大连.md "wikilink")
  - 第六届 福建[泉州](../Page/泉州.md "wikilink")
  - 第七届 浙江舟山（浙江[舟山群岛新区](../Page/舟山群岛新区.md "wikilink")）
  - 第八届 江苏[南京](../Page/南京.md "wikilink")
  - 第九届 江苏[南通](../Page/南通.md "wikilink")
  - 第十届 山东[日照](../Page/日照.md "wikilink")
  - 第十一届 浙江[宁波](../Page/宁波.md "wikilink")
  - 第十二届 浙江[宁波](../Page/宁波.md "wikilink")

## 外部链接

  - [中国航海日](https://archive.is/20140711222441/http://maritimeday.gov.cn/)

## 参考文献

[Category:中華人民共和國紀念日](../Category/中華人民共和國紀念日.md "wikilink")
[Category:中华人民共和国主题日](../Category/中华人民共和国主题日.md "wikilink")
[Category:中国航海](../Category/中国航海.md "wikilink")
[Category:郑和下西洋](../Category/郑和下西洋.md "wikilink")
[Category:7月主題日](../Category/7月主題日.md "wikilink")

1.