**中国证券史**描述中国[股票](../Page/股票.md "wikilink")、[债券及](../Page/债券.md "wikilink")[認股證的發展歷程](../Page/权证.md "wikilink")。

## 早期發展

中国证券史可追溯至[明末](../Page/明朝.md "wikilink")[清初](../Page/清朝.md "wikilink")，在一些收益高的高[风险行业](../Page/风险.md "wikilink")，采用了“招商集资、合股经营”的经营方式，与参与者之间签订的[契约](../Page/契约.md "wikilink")，成为中国最早的股票雏形。1840年代外国在华企业发行[外资](../Page/外资.md "wikilink")[证券](../Page/证券.md "wikilink")。

1872年，中国第一家[股份制企业和中国人自己发行的第一张股票诞生](../Page/股份有限公司.md "wikilink")。证券的出现促进了[证券交易的发展](../Page/证券交易.md "wikilink")。最早的证券交易也只是外商之间进行，后来才出现华商证券交易。1869年中国第一家从事股票买卖的[证券公司成立](../Page/投资银行.md "wikilink")。1882年9月[上海平准股票公司成立](../Page/上海平准股票公司.md "wikilink")，制定了相关章程，使证券交易无序发展变得更加规范。

## 中华民国大陆时期

## 中华人民共和国时期

[People_snapped_up_stocks_in_Shenzhen_in_the_1990s.jpg](https://zh.wikipedia.org/wiki/File:People_snapped_up_stocks_in_Shenzhen_in_the_1990s.jpg "fig:People_snapped_up_stocks_in_Shenzhen_in_the_1990s.jpg")。\]\]

### 1949年－1999年

1949年中华人民共和国成立后，证券交易市场被当作“资本主义的产物”被政府勒令关闭停止。直到[改革开放以后](../Page/改革开放.md "wikilink")，股票才作为[国有企业改革的一种手段](../Page/国有企业.md "wikilink")，得以被政府承认其合法地位。

1990年12月19日，[上海证券交易所正式开业](../Page/上海证券交易所.md "wikilink")，1991年7月3日，[深圳证券交易所正式开业](../Page/深圳证券交易所.md "wikilink")。这两所交易所的成立标志着中华人民共和国证券市场的形成。1992年中国开始向境外发行股票，2月，第一支[B股](../Page/B股.md "wikilink")（上海真空电子器件股份有限公司B股）在上海证券交易所挂牌交易。1996年12月，股票交易实行涨跌停-{制度}-（即指涨跌幅不得超过前一交易日的10%）。

### 2000年以後

从2005年夏天开始，中国大陆股市开始进行[股权分置改革](../Page/股权分置.md "wikilink")，简称“股改”。[股权分置问题主要是将公司](../Page/股权分置.md "wikilink")[大股东持有不能在](../Page/大股东.md "wikilink")[交易所流通的](../Page/交易所.md "wikilink")[股票](../Page/股票.md "wikilink")（包括国有股份以及其他性质的股份）通过向[流通股股东支付一定的](../Page/流通股股东.md "wikilink")[对价](../Page/对价.md "wikilink")，获得流通权。股改的大致方式为该公司大股东送股，发放[红利](../Page/红利.md "wikilink")，宣布资产注入，发放[权证等等](../Page/权证.md "wikilink")，已经完成股改的股票被添加“G”标记。股改的过程同时导致了2001年以来的[熊市结束及](../Page/市場趨勢.md "wikilink")21世纪的大陆股票市场的第一次大[行情](../Page/牛市.md "wikilink")。

2006年10月9日起，原股改完成的股票的G标记取消，未股改股票加上S标记。这标志着大陆[A股的股改工作基本完成](../Page/A股.md "wikilink")。股改的主要目的在于实现股票的全流通，并使持股人的持股成本相同。但现在出现的问题在于管理层利用持股成本差异套现。

## 相关条目

  - [中国股票市场](../Page/中国股票市场.md "wikilink")
  - [中国债券市场](../Page/中国债券市场.md "wikilink")

## 参考文献

[中国金融史](../Category/中国金融史.md "wikilink")
[Category:证券](../Category/证券.md "wikilink")