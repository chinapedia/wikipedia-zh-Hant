**中國音樂**，據目前的考古發現，中國音樂可追溯至9000多年前，[禮樂制度被賦予維繫](../Page/禮樂制度.md "wikilink")[社會秩序的功能](../Page/社會.md "wikilink")。音樂與人心[情感間的關係受到很大的重視](../Page/情感.md "wikilink")，《[詩經](../Page/詩經.md "wikilink")》大[序](../Page/序.md "wikilink")、《[禮記](../Page/禮記.md "wikilink")·[樂記](../Page/樂記.md "wikilink")》、《[荀子](../Page/荀子.md "wikilink")·樂論篇》、《[史記](../Page/史記.md "wikilink")·樂書》等描述[儒家思想認為音樂對](../Page/儒家.md "wikilink")[人性具有教化啟迪的作用](../Page/人性.md "wikilink")。同時從唐代的胡琴到近代的西方音樂，中國音樂又在吸收外來音樂要素的過程過不斷充實發展。

## 緒論

[ZhongNiShi.jpg](https://zh.wikipedia.org/wiki/File:ZhongNiShi.jpg "fig:ZhongNiShi.jpg")\]\]
中国古代**[诗](../Page/诗.md "wikilink")**、**[歌](../Page/歌.md "wikilink")**是不分的，即文学和音乐是紧密相联系的。现存最早的汉语诗歌总集《[诗经](../Page/诗经.md "wikilink")》中的诗篇当时都是配有曲调，为人口头传唱的，这个传统一直延续下去。汉代的官方诗歌集成《[汉乐府](../Page/汉乐府.md "wikilink")》、[唐诗](../Page/唐诗.md "wikilink")、[宋词当时也都能歌唱](../Page/宋词.md "wikilink")。現代亦有音乐家为古诗谱曲演唱，如[苏轼描写](../Page/苏轼.md "wikilink")[中秋佳节的](../Page/中秋.md "wikilink")《[水调歌头](../Page/水调歌头.md "wikilink")》。

歷朝政府皆有專門從事音樂的部門，用於祭祀、宴飲等。為贵族娱乐的“伶人”常常沒有被記載，唐代著名歌手[李龟年也只常出现在唐诗中](../Page/李龟年.md "wikilink")。中国古代“[士大夫](../Page/士大夫.md "wikilink")”的“[琴棋书画](../Page/琴棋书画.md "wikilink")”，“琴”指[古琴](../Page/古琴.md "wikilink")，古琴音樂往往是士大夫之間欣赏，他們對古琴的研究形成了“琴學”。明清兩代出版了很多的琴譜、琴書，文獻資料非常丰富，並且形成了很多的琴派。

中国古代音乐属于[五声音阶体系](../Page/五声音阶.md "wikilink")，五声音阶上的五个级被称为「五声」，分別為宫、商、角、-{徵}-、羽。

## 歷史

### 遠古時期

[Neolithic_bone_flute.jpg](https://zh.wikipedia.org/wiki/File:Neolithic_bone_flute.jpg "fig:Neolithic_bone_flute.jpg")[舞阳县出土的](../Page/舞阳县.md "wikilink")[賈湖骨笛将中国音乐文化上推至](../Page/賈湖骨笛.md "wikilink")9000年，现存[河南博物院](../Page/河南博物院.md "wikilink")\]\]
中国音乐最早可追溯到9000年前。[贾湖遗址出土的](../Page/贾湖遗址.md "wikilink")[贾湖骨笛](../Page/贾湖骨笛.md "wikilink")，距今约9000年至7500年。\[1\]距今约7000年，各地开始出现骨哨、原始陶埙。

[葛天氏](../Page/葛天氏.md "wikilink")“三人操牛尾，投足以歌八阕”，可見远古音乐文化具有歌、舞、乐互相结合的特点。“敬天常”、“奋五谷”、
“总禽兽之极”反映先民对农业、畜牧业以及自然规律的认识。这些歌、舞、乐互为一体的原始乐舞还与原始氏族的图腾崇拜想联系。例如黄帝氏族曾以云为图腾，他的乐舞就叫做《云门》。关于原始的歌曲形式，《吕氏春秋》记涂山氏之女《候人歌》歌词仅只“候人兮猗”一句，而只有“候人”二字有实意。这便是音乐的萌芽，是一种孕而未化的语言。

傳說"[伏羲](../Page/伏羲.md "wikilink")、[神農作琴瑟](../Page/神農.md "wikilink")"。伏羲作琴瑟，大抵出於草創，未能完善，傳至神農時，神農又加於研究，於是琴瑟之制，始漸如後世之制。\[2\]。又有[伶倫制音律](../Page/伶倫.md "wikilink")、作歌舞，[仰延制樂譜](../Page/仰延.md "wikilink")，[大容作樂](../Page/大容.md "wikilink")，[黃帝制笙竽](../Page/黃帝.md "wikilink")，[夔作鼓](../Page/夔.md "wikilink")，[伶倫](../Page/伶倫.md "wikilink")、[榮將鑄鐘等傳說](../Page/榮將.md "wikilink")。

### 夏、商时期

乐舞逐渐脱离「原始氏族乐舞为氏族共有」的特点。从内容上看，它们渐渐离开了原始的图腾崇拜，转而为对征服自然的人的颂歌。例如[夏禹治水](../Page/夏禹.md "wikilink")，造福人民，遂有歌颂夏禹的乐舞《[大夏](../Page/大夏.md "wikilink")》。[夏桀无道](../Page/夏桀.md "wikilink")，[商汤伐之](../Page/商汤.md "wikilink")，遂有歌颂商汤伐桀的乐舞《[大蠖](../Page/大蠖.md "wikilink")》。商代巫风盛行，于是出现了专司祭祀的巫（女巫）和觋（男巫），在行祭时舞蹈、歌唱，是最早以音乐为职业的人。

酋長以乐舞祭祀天、祖先，同时又以乐舞獲得感官的享受，反映出生产力较原始时代进步，从而使音乐文化具备了发展的条件。傳說夏代有鳄鱼皮蒙制的[鼍鼓](../Page/鼍鼓.md "wikilink")，商代有[木腔蟒皮鼓](../Page/木腔蟒皮鼓.md "wikilink")、[石磐](../Page/石磐.md "wikilink")、[双鸟饕餮纹铜鼓](../Page/双鸟饕餮纹铜鼓.md "wikilink")、[编钟](../Page/编钟.md "wikilink")、[编铙](../Page/编铙.md "wikilink")。

公元前5000餘年的陶[埙从当时的单音孔](../Page/埙.md "wikilink")、2音孔发展到5音孔，可发出12个[半音的](../Page/半音.md "wikilink")[音列](../Page/音列.md "wikilink")。根据陶埙发音推断，[五声音阶出现在](../Page/五声音阶.md "wikilink")[新石器时代的晚期](../Page/新石器时代.md "wikilink")，而[七声音階至少在](../Page/七声音階.md "wikilink")[商](../Page/商.md "wikilink")、[殷时期出现](../Page/殷.md "wikilink")。

### 周朝

[VM_4736_Hubei_Provincial_Museum_-_bianzhong_performance.jpg](https://zh.wikipedia.org/wiki/File:VM_4736_Hubei_Provincial_Museum_-_bianzhong_performance.jpg "fig:VM_4736_Hubei_Provincial_Museum_-_bianzhong_performance.jpg")演奏\]\]

[西周时期宫廷首先建立完备的](../Page/西周.md "wikilink")[礼乐制度](../Page/礼乐制度.md "wikilink")，[清](../Page/清.md "wikilink")[劉寶楠](../Page/劉寶楠.md "wikilink")《[論語正義](../Page/論語正義.md "wikilink")》：「《左‧隱五年》傳考仲子之公將萬焉，公問羽數於眾仲，眾仲對曰：『天子用八，諸侯用六，大夫四，士二。』」另[服虔](../Page/服虔.md "wikilink")《[左傳解誼](../Page/左傳解誼.md "wikilink")》：「天子八八，諸侯六八，大夫四八，士二八。」。

具代表性的舞樂有黄帝时的《[云门](../Page/云门.md "wikilink")》，[尧时的](../Page/尧.md "wikilink")《[咸池](../Page/咸池.md "wikilink")》，[舜时的](../Page/舜.md "wikilink")《[大韶](../Page/大韶.md "wikilink")》，[禹时的](../Page/禹.md "wikilink")《[大夏](../Page/大夏_\(舞蹈\).md "wikilink")》，[商时的](../Page/商朝.md "wikilink")《[大濩](../Page/大濩.md "wikilink")》，[周時的](../Page/周朝.md "wikilink")《[大武](../Page/大武_\(樂舞\).md "wikilink")》，合稱「[大舞](../Page/大舞.md "wikilink")」或“六代乐舞”。

周代有采风制度，收集民歌，以观风俗、察民情。赖此保留了大量民歌，经[孔子删訂](../Page/孔子.md "wikilink")，形成了中国第一部诗歌总集《[诗经](../Page/诗经.md "wikilink")》。它收有自[西周初到](../Page/西周.md "wikilink")[春秋中叶](../Page/春秋時期.md "wikilink")500多年的入乐诗歌共305篇。《詩經》中詩的分類，有「四始六義」之說。「四始」指《風》、《大雅》、《小雅》、《頌》的四篇列首位的詩。「六義」則指「風、雅、頌，賦、比、興」。「風、雅、頌」是按音樂的不同對《詩經》的分類，「賦、比、興」是《詩經》的表現手法。歌曲尾部的高潮部分，已有专门的名称“乱”。

《诗经》成书前后，[屈原所編寫的](../Page/屈原.md "wikilink")《[九歌](../Page/九歌.md "wikilink")》將原為[楚國民間祭神時的演唱和表演改編與加工](../Page/楚國.md "wikilink")，寫成格調高雅的詩歌，具有浓重的[楚文化特征](../Page/楚.md "wikilink")。

1978年出土的[曾侯乙墓裏](../Page/曾侯乙墓.md "wikilink")8种124件乐器，包括重要的[曾侯乙編鐘](../Page/曾侯乙編鐘.md "wikilink")、[曾侯乙編磬等](../Page/曾侯乙編磬.md "wikilink")，提供大量當時宮廷音樂的資料，包括樂器制式、演奏編制、[十二律的理论](../Page/十二律.md "wikilink")、[音名](../Page/音名.md "wikilink")、[旋宫法等](../Page/旋宫法.md "wikilink")。

《[管子](../Page/管子.md "wikilink")‧地员篇》所记载的“[三分损益法](../Page/三分损益法.md "wikilink")”所生產的[自然律能發出](../Page/自然律.md "wikilink")[自然泛音](../Page/自然泛音.md "wikilink")，它為頻率的倍數關係，因此聽起來是和諧的聲音，但會造成“黄钟不能还原”。

### 秦、汉时期

[Bianqing_from_Marquis_Yi's_tomb.jpg](https://zh.wikipedia.org/wiki/File:Bianqing_from_Marquis_Yi's_tomb.jpg "fig:Bianqing_from_Marquis_Yi's_tomb.jpg")\]\]

[秦](../Page/秦.md "wikilink")[汉时开始出现](../Page/汉.md "wikilink")"[乐府](../Page/乐府.md "wikilink")"。它继承了[周代对](../Page/周代.md "wikilink")[采风制度](../Page/采风制度.md "wikilink")，搜集、整理改变民间音乐，並集中大量乐工在[宴享](../Page/宴享.md "wikilink")、[郊祀](../Page/郊祀.md "wikilink")、[朝贺等场合演奏](../Page/朝贺.md "wikilink")。这些用作演唱的歌词，被称为[乐府诗](../Page/乐府诗.md "wikilink")。乐府，后来又被引申为泛指各种入乐或不入乐的歌词，甚至一些[戏曲和](../Page/戏曲.md "wikilink")[器乐也都称之为](../Page/器乐.md "wikilink")[乐府](../Page/乐府.md "wikilink")。

[汉代主要的歌曲形式是](../Page/汉.md "wikilink")「相和歌」。它从最初的「一人唱，三人和」的清唱，渐次发展为有[丝](../Page/弦樂器.md "wikilink")、[竹乐器伴奏的](../Page/管樂器.md "wikilink")「相和大曲」，并且具「艳、趋、乱」的曲体结构，它对[隋](../Page/隋.md "wikilink")、[唐时的歌舞大曲有着重要影响](../Page/唐.md "wikilink")。汉代時西北边疆兴起了[鼓吹乐](../Page/鼓吹乐.md "wikilink")。它以不同编制的[吹管乐器和](../Page/吹管乐器.md "wikilink")[打击乐器构成多种鼓吹形式](../Page/打击乐器.md "wikilink")，如横吹、骑吹、黄门鼓吹等等。它们或在马上演奏，或在行进中演奏，用于[军乐礼仪](../Page/军乐.md "wikilink")、宫廷宴饮以及民间娱乐。今日尚存的民间吹打乐，当有汉代鼓吹的遗绪。而「[百戏](../Page/百戏.md "wikilink")」是将[歌舞](../Page/歌舞.md "wikilink")、[杂技](../Page/杂技.md "wikilink")、[角抵合在一起表演的节目](../Page/角抵.md "wikilink")。

[汉代律学上的成就是](../Page/汉.md "wikilink")[京房以三分损益法将八度音程华划为六十律](../Page/京房.md "wikilink")。这种理论在音乐实践上虽无意义，但体现了律学思维的精微性。从理论上达到了「五十三平均律」的效果。

### 三国、两晋、南北朝时期

[Sheng_(Chinese_mouth_organ).jpg](https://zh.wikipedia.org/wiki/File:Sheng_\(Chinese_mouth_organ\).jpg "fig:Sheng_(Chinese_mouth_organ).jpg")\]\]
中国古代的雅乐在东汉桓灵二帝以后宫廷中已经丧失。曹操找到了精通雅乐的杜夔，使雅乐重新恢复了，黄钟大吕重新在宫廷演奏。礼乐制度得以复归。

由[相和歌中的清商调发展起来的](../Page/相和歌.md "wikilink")[清商乐得到曹操父子的喜爱而得到大发展](../Page/清商乐.md "wikilink")。曹操留下的大量乐府诗就是当时用于歌唱的歌词。曹丕时，正式设立音乐机构[清商署](../Page/清商署.md "wikilink")，到梁陈时期，都设立清商署。两晋之交的战乱，使[清商乐流入南方](../Page/清商乐.md "wikilink")，与南方民歌[吴歌](../Page/吴歌.md "wikilink")、[西曲结合](../Page/西曲.md "wikilink")，形成了不同于相和歌的清商乐。在[北魏孝文帝时](../Page/北魏.md "wikilink")，对清商乐大加收集整理，使之成为与雅乐、胡乐并重的中国重要乐种，被后世称为“华夏正声”。[汉代以来](../Page/汉.md "wikilink")，随着[丝绸之路的畅通](../Page/丝绸之路.md "wikilink")，[西域诸国的歌曲开始传入](../Page/西域.md "wikilink")。[北凉时](../Page/北凉.md "wikilink")[吕光把在](../Page/吕光.md "wikilink")[隋](../Page/隋.md "wikilink")[唐](../Page/唐.md "wikilink")[燕乐中占有重要位置的](../Page/燕乐.md "wikilink")[龟兹乐带到中土](../Page/龟兹.md "wikilink")。

[古琴藝術趋于成熟](../Page/古琴.md "wikilink")，在[汉代已经出现了题解琴曲标题的古琴专著](../Page/汉.md "wikilink")《[琴操](../Page/琴操.md "wikilink")》。[魏晉时](../Page/魏晉.md "wikilink")[嵇康在其所著](../Page/嵇康.md "wikilink")《[琴賦](../Page/琴賦.md "wikilink")》一书中有「徽以中山之玉」的记载。这说明当时的人们已经知道古琴上徽位泛音的产生。当时大量文人琴家相继出现，如[蔡邕](../Page/蔡邕.md "wikilink")、[嵇康](../Page/嵇康.md "wikilink")、[阮籍等](../Page/阮籍.md "wikilink")，《[广陵散](../Page/广陵散.md "wikilink")》（《聂政刺秦王》）、《[猗兰操](../Page/猗兰操.md "wikilink")》、《[酒狂](../Page/酒狂.md "wikilink")》等著名琴曲问世。

这一时期律学上的重要成就，包括[晋代](../Page/晋代.md "wikilink")[荀勖找到](../Page/荀勖.md "wikilink")[管乐器的](../Page/管乐器.md "wikilink")「[管口校正数](../Page/管律_\(音樂\).md "wikilink")」。[南朝宋](../Page/南朝.md "wikilink")[何承天在三分损益法上](../Page/何承天_\(南朝\).md "wikilink")，以[等差數列](../Page/等差數列.md "wikilink")、[疊加原理](../Page/疊加原理.md "wikilink")，创立了十分接近[十二平均律的新律](../Page/十二平均律.md "wikilink")，初步解决了三分损益律[黄钟不能还原的难题](../Page/黄钟.md "wikilink")。

### 隋、唐时期

[隋](../Page/隋.md "wikilink")[唐两代](../Page/唐.md "wikilink")，政权统一，特别是唐代，政治稳定，经济兴旺，统治者奉行开放政策，不斷吸收他方文化，加上魏晋以来已经孕育着的各族音乐文化融合打基础，终于萌发了以歌舞音乐为主要标志的音乐艺术的全面发展的高峰。

[唐代宫廷宴享的音乐](../Page/唐.md "wikilink")，称作「[燕乐](../Page/燕乐.md "wikilink")」。[隋](../Page/隋.md "wikilink")、[唐时期的](../Page/唐.md "wikilink")[七步乐](../Page/七步乐.md "wikilink")、[九部乐就属于](../Page/九部乐.md "wikilink")[燕乐](../Page/燕乐.md "wikilink")。它们分别是各族以及部分外国的民间音乐，主要有[清商乐](../Page/清商乐.md "wikilink")、[西凉乐](../Page/西凉.md "wikilink")、[高昌乐](../Page/高昌.md "wikilink")、[龟兹乐](../Page/龟兹.md "wikilink")、[康国乐](../Page/康国.md "wikilink")、[安国乐](../Page/安国.md "wikilink")、[天竺乐](../Page/天竺.md "wikilink")、[高丽乐等](../Page/高丽.md "wikilink")。其中[龟兹乐](../Page/龟兹乐.md "wikilink")、[西凉乐更为重要](../Page/西凉乐.md "wikilink")。

燕乐分为[坐部伎和](../Page/坐部伎.md "wikilink")[立部伎演奏](../Page/立部伎.md "wikilink")，[白居易](../Page/白居易.md "wikilink")《[立部伎](../Page/立部伎.md "wikilink")》诗形容坐部伎的演奏水平高于立部伎。风靡一时的唐代歌舞大曲是燕乐中独树一帜的奇葩，它继承了相和大曲的传统，融会了[九部乐中各族音乐的精华](../Page/九部乐.md "wikilink")，形成了「[散序](../Page/散序.md "wikilink")、[中序或](../Page/中序.md "wikilink")[拍序](../Page/拍序.md "wikilink")、破或舞遍」的结构形式。见于《[教坊录](../Page/教坊录.md "wikilink")》著录的唐大曲曲名共有46个，其中《[霓裳羽衣舞](../Page/霓裳羽衣舞.md "wikilink")》为[唐玄宗所作](../Page/唐玄宗.md "wikilink")，兼有清雅的法曲风格，为世所称道。白居易写有生动描绘该大曲演出过程的诗篇《[霓裳羽衣舞歌](../Page/霓裳羽衣舞歌.md "wikilink")》。

受到[龟兹音乐理论的影响](../Page/龟兹.md "wikilink")，唐代出现了八十四调，燕乐二十八调的乐学理论。唐代[曹柔创立的](../Page/曹柔.md "wikilink")[减字谱一直沿用至近代](../Page/减字谱.md "wikilink")。

[唐代末年盛行一种有故事情节](../Page/唐代.md "wikilink")，有角色和化妆表演，载歌载舞，同时兼有伴唱和伴奏的歌舞戏。大面、踏搖娘、撥頭、參軍戲等这已经是一种小型的雏形[戏曲](../Page/戏曲.md "wikilink")。

文学史上堪称一绝的唐诗在当时是可以入乐歌唱的。当时歌伎曾以能歌名家诗为快；诗人也以自己的诗作入乐后流传之广来衡量自己的写作水平。

唐代音乐文化的繁荣还表现为有一系列音乐教育的机构，如[教坊](../Page/教坊.md "wikilink")、[梨园](../Page/梨园.md "wikilink")、[大乐署](../Page/大乐署.md "wikilink")、[鼓吹署以及专门教习幼童的](../Page/鼓吹署.md "wikilink")[梨园](../Page/梨园.md "wikilink")[别教园](../Page/别教园.md "wikilink")。这些机构以严密的考绩，造就着一批批才华出众的音乐家。

在唐代的乐队中，[琵琶是主要乐器之一](../Page/琵琶.md "wikilink")。它已经与今日的[琵琶形制相差无几](../Page/琵琶.md "wikilink")。现今[南管琵琶在形制上和演奏方法上还保留着唐琵琶的某些特点](../Page/南管.md "wikilink")。

### 宋、金、元时期

[宋](../Page/宋.md "wikilink")、[金](../Page/金.md "wikilink")、[元时期音乐文化的发展以市民音乐的勃兴为重要标志](../Page/元.md "wikilink")。随着都市商品经济的繁荣，适应市民阶层文化生活的游艺场「[瓦舍](../Page/瓦舍.md "wikilink")」、「[勾栏](../Page/勾栏.md "wikilink")」应运而生。在瓦舍、勾栏中有[叫声](../Page/叫声.md "wikilink")、[嘌唱](../Page/嘌唱.md "wikilink")、[小唱](../Page/小唱.md "wikilink")、[唱赚等艺术歌曲的演唱](../Page/唱赚.md "wikilink")，以及说唱[崖词](../Page/崖词.md "wikilink")、[陶真](../Page/陶真.md "wikilink")、[鼓子词](../Page/鼓子词.md "wikilink")、[诸宫调](../Page/诸宫调.md "wikilink")，[杂剧](../Page/杂剧.md "wikilink")、[院本亦相繼出現](../Page/院本.md "wikilink")。当中唱赚中的[缠令](../Page/缠令.md "wikilink")、[缠达两种曲式结构对后世戏曲以及器乐的曲式结构有着一定的影响](../Page/缠达.md "wikilink")。而[鼓子词则影响到后世的说唱音](../Page/鼓子词.md "wikilink")[乐鼓词](../Page/乐鼓词.md "wikilink")。[诸宫调是这一时期成熟起来的大型说唱曲种](../Page/诸宫调.md "wikilink")。

承[隋](../Page/隋.md "wikilink")[唐曲子词发展的遗绪](../Page/唐.md "wikilink")，[宋代发展出长短句的](../Page/宋代.md "wikilink")「词调音乐」。「词调音乐」可以分为引、慢、近、拍、令等词牌形式。在填词的手法上有「[摊破](../Page/摊破.md "wikilink")」、「[减字](../Page/减字.md "wikilink")」、「[偷声](../Page/偷声.md "wikilink")」等。[南宋](../Page/南宋.md "wikilink")[姜夔能作词](../Page/姜夔.md "wikilink")，又能依词度曲，他有17首自度曲和1首减字谱的琴歌《[古怨](../Page/古怨.md "wikilink")》传世。这些作品多表达了作者关人文的心情，並擅描绘清幽悲凉的意境，如《[扬州慢](../Page/扬州慢.md "wikilink")》、《[鬲溪梅令](../Page/鬲溪梅令.md "wikilink")》、《[杏花天影](../Page/杏花天影.md "wikilink")》等。[郭楚望的代表作](../Page/郭楚望.md "wikilink")《[潇湘水云](../Page/潇湘水云.md "wikilink")》开[古琴流派之先河](../Page/古琴.md "wikilink")，作品表现了作者享受山河間的盎然意趣。在弓弦乐器的发展中，宋代出现了「[马尾胡琴](../Page/马尾胡琴.md "wikilink")」的记载。

宋代是[戏曲趋于成熟的时代](../Page/戏曲.md "wikilink")。它的标志是[南宋时](../Page/南宋.md "wikilink")[南戏的出现](../Page/南戏.md "wikilink")。南戏又称[温州杂剧](../Page/温州杂剧.md "wikilink")、[永嘉杂剧](../Page/永嘉杂剧.md "wikilink")，其音乐丰富而自然。最初时一些民间小调，演唱时可以不受宫调的限制。后来发展为[曲牌体戏曲音乐时](../Page/曲牌体.md "wikilink")，还出现了组织不同曲牌的若干乐句构成一种新曲牌的「[集曲](../Page/集曲.md "wikilink")」形式。南戏在演唱形式上已有独唱、对唱、合唱等多种。传世的三种南戏剧本《[张协状元](../Page/张协状元.md "wikilink")》等见于《[永乐大曲](../Page/永乐大曲.md "wikilink")》。戏曲艺术在元代出现了以元杂剧为代表的高峰。元杂剧的兴盛最初在北方，渐向南方发展，与南方戏曲发生交融。代表性的元杂剧作家有[关汉卿](../Page/关汉卿.md "wikilink")、[马致远](../Page/马致远.md "wikilink")、[郑光祖](../Page/郑光祖.md "wikilink")、[白朴](../Page/白朴.md "wikilink")、[王实甫](../Page/王实甫.md "wikilink")、[乔吉甫](../Page/乔吉甫.md "wikilink")，世称六大家。典型作品如[关汉卿的](../Page/关汉卿.md "wikilink")《[窦娥冤](../Page/窦娥冤.md "wikilink")》、《[单刀会](../Page/单刀会.md "wikilink")》，[王实甫的](../Page/王实甫.md "wikilink")《[西厢记](../Page/西厢记.md "wikilink")》。元杂剧有严格的结构，每部作品由「四折一楔子」构成。一折内限用同一[宫调](../Page/宫调.md "wikilink")，一韵到底，常由一个角色（末或旦）主唱，这些规则，有时也有突破，如王实甫的《[西厢记](../Page/西厢记.md "wikilink")》达5本20折。元杂剧对南方戏曲的影响，造成[溫州南戲的进一步成熟](../Page/溫州南戲.md "wikilink")。出现了一系列典型剧作，如《[拜月庭](../Page/拜月庭.md "wikilink")》、《[琵琶记](../Page/琵琶记.md "wikilink")》等等。这些剧本经历代流传，至今仍在上演。当时南北曲的风格已经初步确立，以[七声音阶为主的北曲沉雄](../Page/七声音阶.md "wikilink")；以[五声音阶为主的南曲柔婉](../Page/五声音阶.md "wikilink")。随着元代戏曲艺术的发展，出现了最早的总结戏曲演唱理论的专著，即燕南之庵的《[唱论](../Page/唱论.md "wikilink")》，而[周德清的](../Page/周德清.md "wikilink")《[中原音韵](../Page/中原音韵.md "wikilink")》则是北曲最早的韵书，他把北方语言分为19个韵部，并且把字调分为阴平、阳平、上声、去声4种。这对后世音韵学的研究以及戏曲说唱音乐的发展均有很大的影响。

[南管](../Page/南管.md "wikilink")，大略是在宋元時期成形的，當時的[泉州地區商旅雲集](../Page/泉州.md "wikilink")，人文薈萃，使得具有唐代大曲遺風的南管得以蓬勃發展。元代北曲傳入南方，其對南管的影響包含曲牌的吸收，以及「一聲四節」歌法的成熟。從樂器發展來看，南管的上四管體製也是在元代出現。此外，南管歌器相分的特徵，即樂器按譜而彈、不隨歌者曲調而變，亦是根於北曲而定型於元\[3\]。

[元代時期](../Page/元代.md "wikilink")[三弦的出现值得注意](../Page/三弦.md "wikilink")。宋代出现了[燕乐音阶的记载](../Page/燕乐.md "wikilink")。早期的[工尺谱谱式也在](../Page/工尺谱.md "wikilink")[张炎](../Page/张炎.md "wikilink")《[词源](../Page/词源.md "wikilink")》和[沈括的](../Page/沈括.md "wikilink")《[梦溪笔谈](../Page/梦溪笔谈.md "wikilink")》中出现。近代通行的工尺谱直接导源于此时。

### 明、清时期

明清社会經濟發展發達，市民阶层日益壮大，音乐文化的发展趨向世俗化。明代的民间小曲内容丰富，虽然良莠不齐，但其影响之广已经达到「不问男女」，「人人习之」的程度。由此私人收集编辑，刊刻小曲成风，而且从民歌小曲到唱本、戏文、琴曲均有私人刊本问世。如[冯梦龙编辑的](../Page/冯梦龙.md "wikilink")《[山歌](../Page/山歌.md "wikilink")》，[朱权编辑的](../Page/朱权.md "wikilink")《[神奇秘谱](../Page/神奇秘谱.md "wikilink")》等。
[Erhu.png](https://zh.wikipedia.org/wiki/File:Erhu.png "fig:Erhu.png")\]\]
明清时期说唱音乐异彩纷呈。其中南方的[弹词](../Page/弹词.md "wikilink")，北方的[鼓词](../Page/鼓词.md "wikilink")，以及[牌子曲](../Page/牌子曲.md "wikilink")、[琴书](../Page/琴书.md "wikilink")、[道情类的说唱曲种更为重要](../Page/道情.md "wikilink")。南方秀丽的弹词以[苏州弹词影响最大](../Page/苏州弹词.md "wikilink")。北方的[鼓词以](../Page/鼓词.md "wikilink")[山东大鼓](../Page/山东大鼓.md "wikilink")，冀中的[木板大鼓](../Page/木板大鼓.md "wikilink")、[西河大鼓](../Page/西河大鼓.md "wikilink")、[京韵大鼓较为重要](../Page/京韵大鼓.md "wikilink")。

牌子曲类的说唱有单弦，[河南大调曲子等](../Page/河南大调曲子.md "wikilink")；琴书类说唱有[山东琴书](../Page/山东琴书.md "wikilink")、[四川扬琴等](../Page/四川扬琴.md "wikilink")；[道情类说唱有](../Page/道情.md "wikilink")[浙江道情](../Page/浙江道情.md "wikilink")、[陕西道情](../Page/陕西道情.md "wikilink")、[湖北渔鼓等](../Page/湖北渔鼓.md "wikilink")，少数民族也出现了一些说唱曲如[蒙古说书](../Page/蒙古说书.md "wikilink")、[白族的](../Page/白族.md "wikilink")[大本曲](../Page/大本曲.md "wikilink")。

明清时期歌舞音乐如汉族的各种[秧歌](../Page/秧歌.md "wikilink")、[维吾尔族的](../Page/维吾尔族.md "wikilink")[木卡姆](../Page/木卡姆.md "wikilink")、[藏族的](../Page/藏族.md "wikilink")[囊玛](../Page/囊玛.md "wikilink")、[壮族的](../Page/壮族.md "wikilink")[铜鼓舞](../Page/铜鼓舞.md "wikilink")、[傣族的](../Page/傣族.md "wikilink")[孔雀舞](../Page/孔雀舞.md "wikilink")、[彝族的](../Page/彝族.md "wikilink")[跳月](../Page/跳月.md "wikilink")、[苗族的](../Page/苗族.md "wikilink")[芦笙舞等等](../Page/芦笙舞.md "wikilink")。

以声腔的流布为特点，明清戏曲音乐出现了新的发展高峰。明初四大声腔有[海盐](../Page/海盐.md "wikilink")、[余姚](../Page/余姚.md "wikilink")、[弋阳](../Page/弋阳.md "wikilink")、[昆山诸腔](../Page/昆山.md "wikilink")，其中的昆山腔经由[江苏](../Page/江苏.md "wikilink")[太仓](../Page/太仓.md "wikilink")[魏良甫等人的改革](../Page/魏良甫.md "wikilink")，以曲调细腻流畅，发音讲究字头、字腹、字尾而赢得人们的喜爱。[昆山腔又经过南北曲的汇流](../Page/昆山腔.md "wikilink")，形成了一时为戏曲之冠的[昆剧](../Page/昆剧.md "wikilink")。最早的[昆剧剧目是明](../Page/昆剧.md "wikilink")[梁辰鱼的](../Page/梁辰鱼.md "wikilink")《[浣纱记](../Page/浣纱记.md "wikilink")》，其余重要的剧目如明汤显祖的《[牡丹亭](../Page/牡丹亭.md "wikilink")》、[清洪升的](../Page/清洪升.md "wikilink")《[长生殿](../Page/长生殿.md "wikilink")》等。弋阳腔以其灵活多变的特点对各地的方言小戏发生重要影响，使得各地小戏日益增多，如各种高腔戏。明末清初，北方以陕西西秦腔为代表的[梆子腔得到很快的发展](../Page/梆子腔.md "wikilink")，它影响到山西的蒲州梆子、陕西的同州梆子、河北梆子、河南梆子。这种高亢、豪爽的梆子腔在北方各省经久不衰。晚清，由西皮和二黄两种基本曲调构成的[皮黄腔](../Page/皮黄腔.md "wikilink")，在北京初步形成，由此产生了影响遍及全国的[京剧](../Page/京剧.md "wikilink")。

明清时期，器乐的发展表现为民间出现了多种器乐合奏的形式。如北京的[智化寺管乐](../Page/智化寺管乐.md "wikilink")，[河北吹歌](../Page/河北吹歌.md "wikilink")，[江南丝竹](../Page/江南丝竹.md "wikilink")，[十番锣鼓等等](../Page/十番锣鼓.md "wikilink")。明代的《[平沙落雁](../Page/平沙落雁.md "wikilink")》、清代的《[流水](../Page/流水.md "wikilink")》等琴曲以及一批丰富的琴歌《[阳关三叠](../Page/阳关三叠.md "wikilink")》、《[胡笳十八拍](../Page/胡笳十八拍.md "wikilink")》等广为流传。[琵琶乐曲自元末明初有](../Page/琵琶.md "wikilink")《[海青拿天鹅](../Page/海青拿天鹅.md "wikilink")》以及《[十面埋伏](../Page/十面埋伏_\(樂曲\).md "wikilink")》等名曲问世，至清代还出现了[华秋萍编辑的最早的](../Page/华秋萍.md "wikilink")《[琵琶谱](../Page/琵琶谱.md "wikilink")》。

16世紀晚[明時期](../Page/明.md "wikilink")，西洋音樂通過傳教士傳到中國。在[利瑪竇進京呈現給](../Page/利瑪竇.md "wikilink")[萬歷皇帝的禮品單中](../Page/萬歷.md "wikilink")，有西琴一張，據考這是一張[古鋼琴](../Page/古鋼琴.md "wikilink")。\[4\]

中国明代音乐家朱载堉于万历十二年（1584年）首次提出“新法密率”（见《律吕精义》、《乐律全书》），推算出以比率
\(\sqrt [12] {2}\)
将八度音等分为十二等分的算法，计算出[十二平均律的相邻两个](../Page/十二平均律.md "wikilink")[半音间的长度比值](../Page/半音.md "wikilink")，精确到25位数字，為世界首创。他并依此法制造出符合十二平均律律管及律准的樂器，是世界上最早的十二平均律乐器。

[清初](../Page/清.md "wikilink")，傳教士[徐日升教授](../Page/徐日升.md "wikilink")[康熙皇帝西方樂理](../Page/康熙皇帝.md "wikilink")，並著有《[律呂纂要](../Page/律呂纂要.md "wikilink")》一書。[五線譜也在這個時候傳入中國](../Page/五線譜.md "wikilink")。[乾隆皇帝的時候](../Page/乾隆皇帝.md "wikilink")，在宮中還組建了一支西洋樂隊，樂器有[小提琴](../Page/小提琴.md "wikilink")、[大提琴](../Page/大提琴.md "wikilink")、[低音提琴](../Page/低音提琴.md "wikilink")、[木管樂器](../Page/木管樂器.md "wikilink")、[豎笛](../Page/豎笛.md "wikilink")、[木琴](../Page/木琴.md "wikilink")、[風琴](../Page/風琴.md "wikilink")、古琴，演奏時戴西洋假髮\[5\]。

## 近代

[BeijingStreet2.jpg](https://zh.wikipedia.org/wiki/File:BeijingStreet2.jpg "fig:BeijingStreet2.jpg")

1910年代到1920年代的[新文化運動期間](../Page/新文化運動.md "wikilink")，很多到海外留學的中國音樂家回國之後，開始演奏歐洲古典音樂，也開始用五線譜紀錄新作品。大城市裡組成了新興交響樂團，混合歐洲古典音樂和爵士樂，在音樂廳和收音機裡非常流行。在1930年代的[上海達到其鼎盛時期](../Page/上海.md "wikilink")。從三十年代興起的上海[時代曲](../Page/時代曲.md "wikilink")，成為中國流行音樂的始祖。

民間音樂家為中國樂器的演奏發展創造了新的階段，[劉天華定制](../Page/劉天華.md "wikilink")[二胡把位](../Page/二胡.md "wikilink")，改進演奏手法，並創作10首二胡獨奏曲。[阿炳創作二胡曲](../Page/阿炳.md "wikilink")《[二泉映月](../Page/二泉映月.md "wikilink")》，《聽松》，琵琶曲《大浪淘沙》。儘管當時時世動亂，但中國民族音樂不論在獨奏和樂隊合奏方面都有很大的發展。

在[北伐戰爭時期](../Page/北伐戰爭.md "wikilink")，中國的音樂家為配合革命，作了大量的革命歌曲，在國民革命軍中廣為傳唱，有的是用國外通俗歌曲旋律直接配以革命歌詞。

在[抗日戰爭時期](../Page/抗日戰爭.md "wikilink")，音樂家創作大量的抗日歌曲。[冼星海的](../Page/冼星海.md "wikilink")《[黃河大合唱](../Page/黃河大合唱.md "wikilink")》，反映了當時全民抗日的精神。[聶耳為電影配曲作的](../Page/聶耳.md "wikilink")《[義勇軍進行曲](../Page/義勇軍進行曲.md "wikilink")》，成為抗日軍民的軍歌被到處傳唱，[中華人民共和國成立後](../Page/中華人民共和國.md "wikilink")，將義勇軍進行曲定為[國歌](../Page/國歌.md "wikilink")。

1942年延安會議之後，[共產黨把其控制地區的](../Page/共產黨.md "wikilink")[民歌改寫成革命歌曲](../Page/民歌.md "wikilink")，如[陝西民歌](../Page/陝西.md "wikilink")《[東方紅](../Page/東方紅_\(歌曲\).md "wikilink")》。改寫的目的是在大多是[文盲的農民人口中傳播](../Page/文盲.md "wikilink")[共產主義思想](../Page/共產主義.md "wikilink")。

[文革期間](../Page/文革.md "wikilink")，音樂為政治所壟斷，音樂家不是服從政治，就是被指控以各種罪名加以迫害。西方音樂皆不合法，而被推出的革命歌曲和所謂的「[語錄歌](../Page/語錄歌.md "wikilink")」、「[樣板戲](../Page/樣板戲.md "wikilink")」，皆極富政治色彩和立場。在歡迎[美國總統](../Page/美國.md "wikilink")[尼克松的宴會上](../Page/尼克松.md "wikilink")，樂隊演奏美國歌曲《[草堆上的火雞](../Page/草堆上的火雞.md "wikilink")》，當時的文化部長竟向總理抗議。中國音樂進入一個低谷時期。

「樣板戲」雖為政治產物，而且主題單一，但在[管弦樂團引入](../Page/管弦樂團.md "wikilink")[京劇的](../Page/京劇.md "wikilink")[京胡和](../Page/京胡.md "wikilink")[板鼓](../Page/板鼓.md "wikilink")，更突出了京劇的音樂特點，也算是一種中西結合的發展。

## 中国少数民族音乐

中華人民共和國版圖內的[少数民族音乐有着丰富多样的样式和内容](../Page/少数民族.md "wikilink")，如[藏族音乐](../Page/藏族.md "wikilink")、[蒙古族音乐](../Page/蒙古族.md "wikilink")、[壮族音乐等](../Page/壮族.md "wikilink")。

### 藏族音乐

[藏族是个能歌善舞的民族](../Page/藏族.md "wikilink")，他们的歌曲旋律优美辽阔、婉转动听。藏族音乐大体上可以分为[佛教音乐和民间音乐](../Page/藏傳佛教.md "wikilink")。佛教音乐中最著名的是[喇嘛唱的无词的歌颂曲调](../Page/喇嘛.md "wikilink")。

[藏族民歌高亢嘹亮](../Page/藏族.md "wikilink")，曲调悠扬，以[五声音階为主](../Page/五声音階.md "wikilink")。歌舞形式有“[果谐](../Page/果谐.md "wikilink")”、“[果卓](../Page/果卓.md "wikilink")”等。

正规戏剧方面，现在已经挖掘整理演出了藏族传统[歌剧](../Page/歌剧.md "wikilink")《[格萨尔王](../Page/格萨尔王.md "wikilink")》。
[LBW-M1-bronze_drum.jpg](https://zh.wikipedia.org/wiki/File:LBW-M1-bronze_drum.jpg "fig:LBW-M1-bronze_drum.jpg")\]\]
[Miao_musicians.jpg](https://zh.wikipedia.org/wiki/File:Miao_musicians.jpg "fig:Miao_musicians.jpg")\]\]

### 满洲族音乐

中国的[东北和](../Page/中国东北.md "wikilink")[北京等地是](../Page/北京.md "wikilink")[满洲族的聚居地](../Page/满洲族.md "wikilink")。满洲族最有名的民间乐器是源自清朝的[八角鼓](../Page/八角鼓.md "wikilink")。满洲族的[摇篮曲](../Page/摇篮曲.md "wikilink")《[悠悠扎](../Page/悠悠扎.md "wikilink")》等作品经常被用现代的汉语翻唱。

### 蒙古族音乐

[蒙古族民歌分](../Page/蒙古族.md "wikilink")“[长调民歌](../Page/长调民歌.md "wikilink")”和“[短调民歌](../Page/短调民歌.md "wikilink")”。“长调”有许多无意义的谐音字拉长唱腔，有草原空阔的风格。特色乐器是[马头琴](../Page/马头琴.md "wikilink")，是一种拉弦乐器，由于琴柱上一般都雕刻一个马头装饰，所以由此命名。

### 壮族音乐

[壮族聚居区](../Page/壮族.md "wikilink")[广西是民歌的故乡](../Page/广西壮族自治区.md "wikilink")，男女青年经常对歌，有人甚至说壮族人一生唱歌的时间比说话的时间长。[壮族民歌和](../Page/壮族.md "wikilink")[汉族音乐风格基本相似](../Page/汉族.md "wikilink")，以五声音阶为主。歌词有明显的对仗格式，内容则以象征、比喻等手法表述，以生活中的交流为主，有时歌词也引用中国古典故事和典故。

### 傣族音乐

[傣族音乐和南亚地区](../Page/傣族.md "wikilink")[缅甸](../Page/缅甸.md "wikilink")、[泰国的音乐风格类似](../Page/泰国.md "wikilink")，曲调婉转柔美，典型的乐器是[葫芦丝和](../Page/葫芦丝.md "wikilink")[象脚鼓](../Page/象脚鼓.md "wikilink")。

### 纳西古乐

纳西古乐是在[云南](../Page/云南省.md "wikilink")[丽江](../Page/丽江.md "wikilink")[纳西族老人中间演奏的音乐](../Page/纳西族.md "wikilink")，据说是从[明朝时中原地区传入的](../Page/明朝.md "wikilink")，由于当地交通不便，和外界交往極少，將明朝音樂保留下來，现在只有一批老人乐队可以演奏，正在培养接班人，是中国14世纪音乐的活化石，受到音乐界的广泛关注。

### 侗族音乐

[侗族](../Page/侗族.md "wikilink")“大歌”是中国唯一采用和声的民歌系统，基本为女声无伴奏合唱，由各声部嗓音的和声配合非常和谐，曾在国际引起轰动，并多次获奖。

### 维吾尔族音乐

[维吾尔族音乐基本是](../Page/维吾尔族.md "wikilink")[中亞音乐风格](../Page/中亞.md "wikilink")，非常注重[节奏](../Page/节奏.md "wikilink")，用[手鼓可以打出几十种不同的节奏](../Page/手鼓.md "wikilink")，乐器主要是[都它尔和](../Page/都它尔.md "wikilink")[热瓦甫](../Page/热瓦甫.md "wikilink")。维吾尔族的传统音乐《[十二木卡姆](../Page/十二木卡姆.md "wikilink")》包罗万象，是许多民间音乐的源头。

### 塔吉克族音乐

[塔吉克族音乐和](../Page/塔吉克族.md "wikilink")[汉族音乐有较大的区别](../Page/汉族.md "wikilink")，善于运用[半音](../Page/半音.md "wikilink")，旋律婉转多变。[雷振邦为](../Page/雷振邦.md "wikilink")[电影](../Page/电影.md "wikilink")《冰山上的来客》配的歌曲，运用了塔吉克民歌的旋律。

### 朝鲜族音乐

[朝鲜族音乐和](../Page/朝鲜族.md "wikilink")[朝鲜](../Page/朝鲜.md "wikilink")/[韩国音乐基本相同](../Page/韩国.md "wikilink")，主要乐器有[长鼓和](../Page/长鼓.md "wikilink")[伽倻琴](../Page/伽倻琴.md "wikilink")，伽倻琴类似中国古代的筝，比现代的筝小，弹法也不一样，是放到盘坐的膝盖上弹。

### 京族音乐

[京族音乐和](../Page/京族.md "wikilink")[越南音乐基本相同](../Page/越南.md "wikilink")，主要乐器有[独弦琴](../Page/独弦琴.md "wikilink")。

## 戏曲音乐

[中国的](../Page/中国.md "wikilink")[戏曲曲艺是在中国较为普及的艺术之一](../Page/戏曲曲艺.md "wikilink")。中国戏曲多以高调的唱腔为主，共有600多个地方戏剧品种，中國各地的戏曲音乐代表有：[昆曲](../Page/昆曲.md "wikilink")、[京劇](../Page/京劇.md "wikilink")、[河北梆子](../Page/河北梆子.md "wikilink")、[豫剧](../Page/豫剧.md "wikilink")、[评剧](../Page/评剧.md "wikilink")、[越剧](../Page/越剧.md "wikilink")、[粤剧](../Page/粤剧.md "wikilink")、[秦腔](../Page/秦腔.md "wikilink")、[黄梅戏](../Page/黄梅戏.md "wikilink")、[潮剧](../Page/潮剧.md "wikilink")、[薌劇](../Page/薌劇.md "wikilink")、[高甲戏和](../Page/高甲戏.md "wikilink")[傩戏等](../Page/傩戏.md "wikilink")。昆曲被稱為中國劇曲的源頭，並被[联合国列为世界口头文化遗产保护目录](../Page/联合国.md "wikilink")。

## 現代國樂

[中华人民共和国建立之后](../Page/中华人民共和国.md "wikilink")，將[苏联的流行歌曲翻译成中文](../Page/苏联.md "wikilink")。並於各地建立[交响曲团](../Page/交响曲团.md "wikilink")，演奏[古典音乐和中国作曲家的新作](../Page/古典音乐.md "wikilink")。东欧的乐团曾多次到中国表演，中国乐团亦曾出国表演。中国音乐家用西方的乐器方法写作具有中国风味的音乐，比较成功的有《[梁祝小提琴協奏曲](../Page/梁祝小提琴協奏曲.md "wikilink")》。

建了[东方歌舞团](../Page/东方歌舞团.md "wikilink")，专门学习、演唱[第三世界的](../Page/第三世界.md "wikilink")[民歌和乐曲](../Page/民歌.md "wikilink")，从此第三世界的音乐开始对中国音乐产生影响。

中国民族乐队的[配器](../Page/配器.md "wikilink")、合奏方式也基本定型，生产出不少成功的民族交响曲。

每年[春节在](../Page/春节.md "wikilink")[维也纳金色大厅举行中国新年音乐会](../Page/维也纳.md "wikilink")。

## 古典音乐

[乾隆組建中國第一個管弦樂團](../Page/乾隆.md "wikilink")，但只限於個人喜好。1900年代，北京成立了中国第一个铜管乐团。成立于1913年的[金陵女子大学设有音乐系](../Page/金陵女子大学.md "wikilink")，对传播西洋音乐发挥了很大作用。1927年，[国立音乐专科学校在](../Page/国立音乐专科学校.md "wikilink")[上海成立](../Page/上海.md "wikilink")。1930年代，上海成立了中国第一个[交响乐团](../Page/交响乐团.md "wikilink")。

20世纪起有许多中国人在国际[钢琴](../Page/钢琴.md "wikilink")、[小提琴](../Page/小提琴.md "wikilink")、[聲樂](../Page/聲樂.md "wikilink")、[歌剧演唱比赛中得奖](../Page/歌剧.md "wikilink")，中国音乐家创作了大量聲樂和用西方乐器演奏的作品。

### 中國音樂與西方音樂比較

中國音樂從很早已經掌握[七聲音階和](../Page/七聲音階.md "wikilink")[十二平均律](../Page/十二平均律.md "wikilink")，但偏好[五聲音階](../Page/五聲音階.md "wikilink")，追求[旋律](../Page/旋律.md "wikilink")、[節奏變化](../Page/節奏.md "wikilink")，輕視[和聲的作用](../Page/和聲.md "wikilink")。西方音樂從[古希臘的五聲音階](../Page/古希臘.md "wikilink")，逐漸發展到七聲音階，直到十二平均律；從單聲部發展到運用[和聲](../Page/和聲.md "wikilink")。

## 流行音乐

自[中国](../Page/中国.md "wikilink")[改革开放以来](../Page/改革开放.md "wikilink")，流行音乐首先从[香港及](../Page/香港.md "wikilink")[台灣进入中国](../Page/台灣.md "wikilink")。[中華民國的](../Page/中華民國.md "wikilink")[校園民歌和](../Page/校園民歌.md "wikilink")[邓丽君的演唱](../Page/邓丽君.md "wikilink")，在中国大受欢迎。中国第一次公开的[香港歌曲演出為](../Page/香港.md "wikilink")[张明敏在](../Page/张明敏.md "wikilink")[中国中央电视台春节联欢晚会演出的](../Page/中国中央电视台春节联欢晚会.md "wikilink")《我的中国心》，並且在中国受到極大迴響。此后，中国的流行歌曲開始吸收[世界各種音乐](../Page/世界.md "wikilink")。

近年来，中國與其它地区的华人流行音乐不断交流，开始出现互相融合的趋势。开始出现“**全球华语流行音乐**”的总体称谓，海外各大流行音乐榜单的发布和编制开始关注大陆市场。

中國的流行音乐当中，民谣性质的音乐，它们的典型代表是[校园民谣](../Page/校园民谣.md "wikilink")、[都市民谣](../Page/都市民谣.md "wikilink")、[军营民谣](../Page/军营民谣.md "wikilink")，这些民谣音乐在流行音乐当中亦占有一席之地，民谣淳朴的曲调，通俗的歌词感动了很多人。

## 摇滚音乐

中国[改革开放之后](../Page/改革开放.md "wikilink")，中國人開始接觸[摇滚樂](../Page/摇滚樂.md "wikilink")，并组建乐队，进行模仿与创作。

1986年5月9日，[北京工人体育馆的纪念](../Page/北京工人体育馆.md "wikilink")“'86国际和平年”中国百名歌星演唱会中，[崔健演唱](../Page/崔健.md "wikilink")《[一无所有](../Page/一无所有_\(歌曲\).md "wikilink")》，為中國第一次搖滾樂首次公開演出。

80年代末和90年代初，陆续出现[唐朝](../Page/唐朝乐队.md "wikilink")、[黑豹](../Page/黑豹樂隊.md "wikilink")、[轮回](../Page/轮回乐队.md "wikilink")、[超载](../Page/超载.md "wikilink")、[指南针](../Page/指南针乐队.md "wikilink")、[北京1989等乐队](../Page/北京1989.md "wikilink")。

1994年，[香港举行](../Page/香港.md "wikilink")“[中国摇滚乐势力](../Page/中国摇滚乐势力.md "wikilink")”演唱会，演出者皆為來自中國的搖滾樂隊。[窦唯](../Page/窦唯.md "wikilink")、[张楚和](../Page/张楚.md "wikilink")[何勇在当时被称为](../Page/何勇.md "wikilink")[魔岩三杰](../Page/魔岩三杰.md "wikilink")。

随后，各种中国摇滚乐风出現：流行的[郑钧](../Page/郑钧.md "wikilink")、[许巍和](../Page/许巍.md "wikilink")[零点](../Page/零点.md "wikilink")，[低保真的](../Page/低保真.md "wikilink")[朋克乐队](../Page/朋克.md "wikilink")[盘古](../Page/盘古乐队.md "wikilink")，英式路线的[麦田守望者和](../Page/麦田守望者.md "wikilink")[清醒](../Page/清醒_\(乐队\).md "wikilink")，民俗摇滚的[子曰](../Page/子曰.md "wikilink")（现已更名“爻释·子曰”）和[二手玫瑰](../Page/二手玫瑰.md "wikilink")，以及[苍蝇](../Page/苍蝇.md "wikilink")、[左小祖咒和](../Page/左小祖咒.md "wikilink")[王磊](../Page/王磊.md "wikilink")、[花儿](../Page/花儿.md "wikilink")、[新裤子等](../Page/新裤子.md "wikilink")。[摩登天空](../Page/摩登天空.md "wikilink")、[京文唱片以及娱乐公司在推出新乐手和乐队](../Page/京文唱片.md "wikilink")、举办演唱会中也做出了很多尝试。

## 宗教音乐

中国的[宗教音乐可分为](../Page/宗教音乐.md "wikilink")[道教音乐](../Page/道教.md "wikilink")、[汉传佛教音乐](../Page/汉传佛教.md "wikilink")、[藏传佛教音乐以及其它宗教音乐](../Page/藏传佛教.md "wikilink")。

## 教育

  - [北京的](../Page/北京.md "wikilink")[中国音乐学院培养民族音乐家](../Page/中国音乐学院.md "wikilink")
  - [中央音乐学院偏向于培养运用西方音乐的音乐家](../Page/中央音乐学院.md "wikilink")。
  - [上海的](../Page/上海.md "wikilink")[上海音乐学院](../Page/上海音乐学院.md "wikilink")
  - [天津的](../Page/天津.md "wikilink")[天津音乐学院](../Page/天津音乐学院.md "wikilink")
  - [广州的](../Page/广州.md "wikilink")[星海音乐学院](../Page/星海音乐学院.md "wikilink")
  - [西安的](../Page/西安.md "wikilink")[西安音乐学院](../Page/西安音乐学院.md "wikilink")
  - [沈阳的](../Page/沈阳.md "wikilink")[沈阳音乐学院](../Page/沈阳音乐学院.md "wikilink")
  - [武汉的](../Page/武汉.md "wikilink")[武汉音乐学院](../Page/武汉音乐学院.md "wikilink")
  - [成都的](../Page/成都.md "wikilink")[四川音乐学院](../Page/四川音乐学院.md "wikilink")

## 其他

  - [中文音乐期刊](../Page/中文音乐期刊.md "wikilink")
  - [中国民族乐器](../Page/中国民族乐器.md "wikilink")
  - [中國傳統音樂](../Page/中國傳統音樂.md "wikilink")

## 参考

<div class="references-small">

<references />

</div>

  - 陳致：〈[說「夏」與「雅」：宗周禮樂形成與變遷的民族音樂學考察](http://www.litphil.sinica.edu.tw/home/publish/PDF/Bulletin/19/19-1-53.pdf.pdf)〉。
  - 羅泰：〈[曾侯乙以前的中国古代乐论——从南宫乎钟的甬部铭文说起](http://www.nssd.org/articles/article_read.aspx?id=1002585353)〉。
  - Lee Yuan-Yuan and Shen, Sinyan. *Chinese Musical Instruments
    (Chinese Music Monograph Series)*. 1999. Chinese Music Society of
    North America Press. ISBN 1-880464-03-9
  - Shen, Sinyan. *Chinese Music in the 20th Century (Chinese Music
    Monograph Series)*. 2001. Chinese Music Society of North America
    Press. ISBN 1-880464-04-7

## 外部链接

  - [中国近现代音乐史教案](http://www.docin.com/p-16861214.html)

<!-- end list -->

  - [HQ-Videos: Traditional Chinese Songs 陽春白雪 (White Snow in the Spring
    Sunlight) and 小月儿高 (The Moon is
    High)](http://www.chinese-music.de/index.php?option=com_content&view=article&id=9&Itemid=9)
  - [香港電台-中樂妙賞](http://programme.rthk.org.hk/channel/radio/programme.php?name=radio5/elderlyoldmusic&p=5164)

[\*](../Category/中国音乐.md "wikilink")

1.  [贾湖骨笛,九千年的绝响](http://sjb.qlwb.com.cn/qlwb/content/20120723/ArticelC20002FM.htm)，齊魯晚報
2.  柳诒徵：《中国文化史》 [第二章
    洪水以前之製作](http://www.china.com.cn/book/txt/2008-11/06/content_16722474.htm)
3.
4.  張西平:《[跟隨利瑪竇到中國·西樂縈繞紫禁城](http://www.showchina.org/zwwhjlxl/gslmddzg/200902/t270137.htm)》,五洲傳播出版社。
5.  張西平:《[跟隨利瑪竇到中國·西樂縈繞紫禁城](http://www.showchina.org/zwwhjlxl/gslmddzg/200902/t270137.htm)》,五洲傳播出版社。