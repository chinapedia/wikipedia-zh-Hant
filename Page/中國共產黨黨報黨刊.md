**中国共产党党报党刊**，指[中华人民共和国时期](../Page/中华人民共和国.md "wikilink")[中国共产党各级机关出版的官方报纸](../Page/中国共产党.md "wikilink")、刊物。[中共中央的主要党报是](../Page/中共中央.md "wikilink")《[人民日报](../Page/人民日报.md "wikilink")》，党刊是《[求是](../Page/求是.md "wikilink")》杂志。此外，中共各省级、地级（甚至县级）党委都出版党报。

## 中共中央主要党报党刊

  - [人民日报](../Page/人民日报.md "wikilink")·[环球时报](../Page/环球时报.md "wikilink")（[中共中央](../Page/中共中央.md "wikilink")）
  - [光明日报](../Page/光明日报.md "wikilink")（[中宣部](../Page/中宣部.md "wikilink")）
  - [经济日报](../Page/经济日报_\(中华人民共和国\).md "wikilink")（[国务院](../Page/国务院.md "wikilink")、[中宣部](../Page/中宣部.md "wikilink")）
  - [解放军报](../Page/解放军报.md "wikilink")（[解放军总政治部](../Page/解放军总政治部.md "wikilink")）
  - 《[求是](../Page/求是.md "wikilink")》杂志

### [两报一刊](../Page/两报一刊.md "wikilink")

“[文革](../Page/文革.md "wikilink")”及其前后“两报一刊”为：

  - 《[人民日报](../Page/人民日报.md "wikilink")》
  - 《[解放军报](../Page/解放军报.md "wikilink")》
  - 《[求是](../Page/求是.md "wikilink")》杂志

## 中共中央单位党委机关报

中央部门（单位）[机关报](../Page/机关报.md "wikilink")：

  - [人民铁道报](../Page/人民铁道报.md "wikilink")（原[铁道部](../Page/铁道部.md "wikilink")）

## 中共地方党委机关报

中共地方黨報為中國各级行政區黨委下設機關報，通常以城市名稱命名，也通常是一個地方中影響力最大的報紙，如福建省[泉州市的](../Page/泉州市.md "wikilink")[泉州晚報](../Page/泉州晚報.md "wikilink")，雲南省[昆明市的](../Page/昆明市.md "wikilink")[昆明日報等](../Page/昆明日報.md "wikilink")。

  - [北京日报](../Page/北京日报.md "wikilink")
  - [解放日报](../Page/解放日报.md "wikilink")（[中共上海市委](../Page/中共上海市委.md "wikilink")）
  - [天津日报](../Page/天津日报.md "wikilink")
  - [河北日报](../Page/河北日报.md "wikilink")
  - [山西日报](../Page/山西日报.md "wikilink")
  - [内蒙古日报](../Page/内蒙古日报.md "wikilink")（[中共内蒙古自治区党委](../Page/中共内蒙古自治区党委.md "wikilink")）
  - [辽宁日报](../Page/辽宁日报.md "wikilink")
  - [吉林日报](../Page/吉林日报.md "wikilink")
  - [黑龙江日报](../Page/黑龙江日报.md "wikilink")
  - [南方日报](../Page/南方日报.md "wikilink")（[中共广东省委](../Page/中共广东省委.md "wikilink")）
  - [广西日报](../Page/广西日报.md "wikilink")
  - [甘肃日报](../Page/甘肃日报.md "wikilink")
  - [大众日报](../Page/大众日报.md "wikilink")（[中共山东省委](../Page/中共山东省委.md "wikilink")）
  - [安徽日报](../Page/安徽日报.md "wikilink")
  - [新华日报](../Page/新华日报.md "wikilink")（[中共江苏省委](../Page/中共江苏省委.md "wikilink")）
  - [浙江日报](../Page/浙江日报.md "wikilink")（[中共浙江省委](../Page/中共浙江省委.md "wikilink")）
  - [福建日报](../Page/福建日报.md "wikilink")
  - [河南日报](../Page/河南日报.md "wikilink")
  - [湖北日报](../Page/湖北日报.md "wikilink")（[中共湖北省委](../Page/中共湖北省委.md "wikilink")）
  - [江西日报](../Page/江西日报.md "wikilink")
  - [陕西日报](../Page/陕西日报.md "wikilink")
  - [青海日报](../Page/青海日报.md "wikilink")
  - [兵团日报](../Page/兵团日报.md "wikilink")
  - [重庆日报](../Page/重庆日报_\(中华人民共和国\).md "wikilink")
    （[中共重庆市委](../Page/中共重庆市委.md "wikilink"))
  - [四川日报](../Page/四川日报.md "wikilink")
  - [云南日报](../Page/云南日报.md "wikilink")
  - [贵州日报](../Page/贵州日报.md "wikilink")
  - [西藏日报](../Page/西藏日报.md "wikilink")
  - [宁夏日报](../Page/宁夏日报.md "wikilink")
  - [海南日报](../Page/海南日报.md "wikilink")
  - [湖南日报](../Page/湖南日报.md "wikilink")

## 中共主要党刊

  - [求是](../Page/求是.md "wikilink")
  - [半月谈](../Page/半月谈.md "wikilink")
  - [人民论坛](../Page/人民论坛.md "wikilink")
  - [支部生活 (广东)](../Page/支部生活_\(广东\).md "wikilink")
  - [支部生活 (上海)](../Page/支部生活_\(上海\).md "wikilink")
  - [共产党员](../Page/共产党员_\(杂志\).md "wikilink")（辽宁）
  - [新湘评论](../Page/新湘评论.md "wikilink")（湖南）

## 参考文献

## 外部链接

  - [关于做好2010年度党报党刊征订发行工作的通知，江西省委教育工委](http://www.jxedu.gov.cn/zwgk/zwggjx/2009/10/41810.html)
  - [雄县努力完成党报征订发行任务](http://www.bd.gov.cn/Article_Show.asp?ArticleID=5623)
  - [政府强行征订地方党报的问题咨询(新浪新闻)](https://web.archive.org/web/20140101061659/http://publish.sina.com.cn/show.php?p_id=52&t_id=15746&s_id=0)

## 参见

  - [机关报](../Page/机关报.md "wikilink")
      - [中共中央机关报](../Page/中共中央机关报.md "wikilink")
  - [黨媒姓黨](../Page/黨媒姓黨.md "wikilink")

{{-}}

[\*](../Category/中华人民共和国时期中共党报.md "wikilink")
[\*](../Category/中华人民共和国时期中共党刊.md "wikilink")
[Category:中华人民共和国合称](../Category/中华人民共和国合称.md "wikilink")
[Category:中华人民共和国政治术语](../Category/中华人民共和国政治术语.md "wikilink")
[Category:中国共产党制度](../Category/中国共产党制度.md "wikilink")