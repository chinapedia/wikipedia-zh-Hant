本條目列舉了中國大陸地區的部分主要隧道。

## 按类型分类

### 公路隧道

| 隧道名                                      | 全长（米） | 所属公路                                         | 備考         |
| ---------------------------------------- | ----- | -------------------------------------------- | ---------- |
| [港珠澳大桥海底隧道](../Page/港珠澳大桥.md "wikilink") | 6.7公里 | [珠三角環線高速公路](../Page/珠三角環線高速公路.md "wikilink") | 2019年，全线贯通 |
| [加林山隧道](../Page/加林山隧道.md "wikilink")     | 3641  | [珠三角環線高速公路](../Page/珠三角環線高速公路.md "wikilink") | 2016年      |

**中国大陆主要公路隧道**

### 鐵路隧道

| 隧道名                                      | 全长（米）  | 所属铁路                                         | 備考                                  |
| ---------------------------------------- | ------ | -------------------------------------------- | ----------------------------------- |
| [关角隧道](../Page/关角隧道.md "wikilink")       | 32,605 | [西格复线](../Page/青藏铁路#西格复线.md "wikilink")      | 2014年4月15日，双线正洞全线贯通                 |
| [西秦岭隧道](../Page/西秦岭隧道.md "wikilink")     | 28,238 | [兰渝铁路](../Page/兰渝铁路.md "wikilink")           | 双洞，在建                               |
| [太行山隧道](../Page/太行山隧道.md "wikilink")     | 27,839 | [石太客专](../Page/石太客专.md "wikilink")           | 双洞，已通车                              |
| [南吕梁山隧道](../Page/南吕梁山隧道.md "wikilink")   | 23,465 | [山西中南部铁路通道](../Page/山西中南部铁路通道.md "wikilink") | 双洞，在建                               |
| [中天山隧道](../Page/中天山隧道.md "wikilink")     | 22,467 | [南疆铁路](../Page/南疆铁路.md "wikilink")           | 双洞，在建                               |
| [青云山隧道](../Page/青云山隧道.md "wikilink")     | 22,175 | [向莆铁路](../Page/向莆铁路.md "wikilink")           | 双洞，在建                               |
| [燕山隧道](../Page/燕山隧道.md "wikilink")       | 21,204 | [张唐铁路](../Page/张唐铁路.md "wikilink")           | 双洞，在建                               |
| [吕梁山隧道](../Page/吕梁山隧道.md "wikilink")     | 20,750 | [太中银铁路](../Page/太中银铁路.md "wikilink")         | 双洞，已通车                              |
| [乌鞘岭隧道](../Page/乌鞘岭隧道.md "wikilink")     | 20,050 | [兰新铁路](../Page/兰新铁路.md "wikilink")           | 双洞，已通车                              |
| [秦岭隧道](../Page/秦岭隧道.md "wikilink")       | 18,460 | [西康铁路](../Page/西康铁路.md "wikilink")           | 双洞，已通车                              |
| [戴云山隧道](../Page/戴云山隧道.md "wikilink")     | 15,623 | [昌福铁路](../Page/昌福铁路.md "wikilink")           | 双洞，已通车                              |
| [武夷山隧道](../Page/武夷山隧道.md "wikilink")     | 14,659 | [昌福铁路](../Page/昌福铁路.md "wikilink")           | 单洞双线，已通车                            |
| [北武夷山隧道](../Page/北武夷山隧道.md "wikilink")   | 14,646 | [合福客运专线](../Page/合福客运专线.md "wikilink")       | 单洞双线，在建                             |
| [大瑶山隧道](../Page/大瑶山隧道.md "wikilink")     | 14,300 | [京广铁路](../Page/京广铁路.md "wikilink")           | 单洞双线，已通车                            |
| [尤溪隧道](../Page/尤溪隧道.md "wikilink")       | 12,974 | [昌福铁路](../Page/昌福铁路.md "wikilink")           | 单洞双线，已通车                            |
| [长梁山隧道](../Page/长梁山隧道.md "wikilink")     | 12,784 | [朔黄铁路](../Page/朔黄铁路.md "wikilink")           | 单洞双线，已通车                            |
| [圆梁山隧道](../Page/圆梁山隧道.md "wikilink")     | 11,070 | [渝怀铁路](../Page/渝怀铁路.md "wikilink")           | 单洞，已通车                              |
| [狮子洋隧道](../Page/狮子洋隧道.md "wikilink")     | 10,800 | [广深港客运专线](../Page/广深港客运专线.md "wikilink")     | 双线，已通车，国内第一条高速铁路水下特长盾构隧道，“中国铁路世纪隧道” |
| [浏阳河隧道](../Page/浏阳河隧道.md "wikilink")     | 10,115 | [武广客运专线](../Page/武广客运专线.md "wikilink")       | 单洞双线，已通车                            |
| [孟村隧道](../Page/孟村隧道.md "wikilink")       | 10,002 | [云桂铁路](../Page/云桂铁路.md "wikilink")           | 单洞双线，在建                             |
| [昆仑山隧道](../Page/昆仑山隧道.md "wikilink")     | 1,686  | [青藏铁路](../Page/青藏铁路.md "wikilink")           | 青藏铁路格拉段最长的隧道，也是世界上最长的高原冻土隧道         |
| [风火山隧道](../Page/风火山隧道.md "wikilink")     | 1,338  | [青藏铁路](../Page/青藏铁路.md "wikilink")           | 世界第一高海拔铁路隧道，轨面海拔高程为4905米            |
| [八達嶺鐵路隧道](../Page/八達嶺鐵路隧道.md "wikilink") | 1,091  | [京包铁路](../Page/京包铁路.md "wikilink")           | 中国自力修建的第一座越岭铁路隧道                    |

**中国大陆主要铁路隧道**

### 地铁隧道

此处仅列出长度大于15公里的地铁隧道

<table>
<thead>
<tr class="header">
<th><p>名称</p></th>
<th><p>长度</p></th>
<th><p>完成年份</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/广州地铁.md" title="wikilink">广州地铁</a><a href="../Page/广州地铁3号线.md" title="wikilink">3号线</a></p></td>
<td></td>
<td><p>2005 - 2010</p></td>
<td><p><a href="../Page/机场南站_(广州地铁).md" title="wikilink">机场南</a>/<a href="../Page/天河客运站_(地铁).md" title="wikilink">天河客运站</a>－<a href="../Page/体育西路站.md" title="wikilink">体育西路</a>－<a href="../Page/番禺广场站.md" title="wikilink">番禺广场</a>；世界最长的地铁隧道</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/北京地铁.md" title="wikilink">北京地铁</a><a href="../Page/北京地铁10号线.md" title="wikilink">10号线</a></p></td>
<td></td>
<td><p>2008 - 2013</p></td>
<td><p>环线</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/南京地铁3号线.md" title="wikilink">南京地铁3号线</a></p></td>
<td></td>
<td><p>2010 - 2015</p></td>
<td><p><a href="../Page/星火路站.md" title="wikilink">星火路站</a>－<a href="../Page/秣周东路站.md" title="wikilink">秣周东路站</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/上海地鐵.md" title="wikilink">上海地鐵</a><a href="../Page/上海轨道交通7号线.md" title="wikilink">7号线</a></p></td>
<td></td>
<td><p>2009 - 2011</p></td>
<td><p><a href="../Page/潘广路站.md" title="wikilink">潘广路</a>－<a href="../Page/花木路站.md" title="wikilink">花木路</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/深圳地铁.md" title="wikilink">深圳地铁</a><a href="../Page/深圳地鐵1號綫.md" title="wikilink">1號綫</a></p></td>
<td></td>
<td><p>2009 - 2011</p></td>
<td><p><a href="../Page/罗湖站_(深圳).md" title="wikilink">罗湖</a>－<a href="../Page/固戍站.md" title="wikilink">固戍</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/深圳地铁.md" title="wikilink">深圳地铁</a><a href="../Page/深圳地鐵2號綫.md" title="wikilink">2號綫</a></p></td>
<td></td>
<td><p>2010 - 2011</p></td>
<td><p><a href="../Page/赤湾站.md" title="wikilink">赤湾</a>－<a href="../Page/新秀站.md" title="wikilink">新秀</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/上海地鐵.md" title="wikilink">上海地鐵</a><a href="../Page/上海轨道交通10号线.md" title="wikilink">10号线</a></p></td>
<td></td>
<td><p>2010</p></td>
<td><p><a href="../Page/虹桥火车站站.md" title="wikilink">虹桥火车站</a>/<a href="../Page/航中路站.md" title="wikilink">航中路</a>－<a href="../Page/新江湾城站.md" title="wikilink">新江湾城</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/广佛地铁.md" title="wikilink">广佛地铁</a></p></td>
<td></td>
<td><p>2010 - 2016</p></td>
<td><p><a href="../Page/燕崗站_(廣州).md" title="wikilink">燕崗</a>－<a href="../Page/新城東站.md" title="wikilink">新城東</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/广州地铁.md" title="wikilink">广州地铁</a><a href="../Page/广州地铁2号线.md" title="wikilink">2号线</a></p></td>
<td></td>
<td><p>2010</p></td>
<td><p><a href="../Page/广州南站_(地铁).md" title="wikilink">广州南站</a>－<a href="../Page/嘉禾望岗站.md" title="wikilink">嘉禾望岗</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/北京地铁.md" title="wikilink">北京地铁</a><a href="../Page/北京地铁4号线.md" title="wikilink">4号线</a>－<a href="../Page/大兴线.md" title="wikilink">大兴线</a></p></td>
<td></td>
<td><p>2009 - 2010</p></td>
<td><p><a href="../Page/安河桥北站.md" title="wikilink">安河桥北</a>－<a href="../Page/公益西桥站.md" title="wikilink">公益西桥</a>－<a href="../Page/新宫站_(北京).md" title="wikilink">新宫</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/上海地鐵.md" title="wikilink">上海地鐵</a><a href="../Page/上海轨道交通2号线.md" title="wikilink">2号线</a></p></td>
<td></td>
<td><p>2000</p></td>
<td><p><a href="../Page/徐泾东站.md" title="wikilink">徐泾东</a>－<a href="../Page/龙阳路站.md" title="wikilink">龙阳路</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/广州地铁.md" title="wikilink">广州地铁</a><a href="../Page/广州地铁5号线.md" title="wikilink">5号线</a></p></td>
<td></td>
<td><p>2009</p></td>
<td><p><a href="../Page/坦尾站.md" title="wikilink">坦尾东</a>－<a href="../Page/文冲站.md" title="wikilink">文冲</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/南京地铁4号线.md" title="wikilink">南京地铁4号线</a></p></td>
<td></td>
<td><p>2011 - 2017</p></td>
<td><p><a href="../Page/龙江站_(南京地铁).md" title="wikilink">龙江站</a>－<a href="../Page/西岗桦墅站.md" title="wikilink">西岗桦墅站</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/上海地鐵.md" title="wikilink">上海地鐵</a><a href="../Page/上海轨道交通8号线.md" title="wikilink">8号线</a></p></td>
<td></td>
<td><p>2007 - 2009</p></td>
<td><p><a href="../Page/市光路站.md" title="wikilink">市光路</a>－<a href="../Page/芦恒路站.md" title="wikilink">芦恒路</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/北京地铁.md" title="wikilink">北京地铁</a><a href="../Page/北京地铁1号线.md" title="wikilink">1号线</a></p></td>
<td></td>
<td><p>1971 - 2000</p></td>
<td><p><a href="../Page/苹果园站.md" title="wikilink">苹果园</a>－<a href="../Page/大望路站.md" title="wikilink">大望路</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/武汉地铁2号线.md" title="wikilink">武汉地铁2号线</a></p></td>
<td></td>
<td><p>2006 - 2012</p></td>
<td><p><a href="../Page/金银潭站.md" title="wikilink">金银潭</a>－<a href="../Page/光谷广场站.md" title="wikilink">光谷广场</a>，目前仅完成一期工程</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/沈阳地铁.md" title="wikilink">沈阳地铁</a><a href="../Page/沈阳地铁1号线.md" title="wikilink">1号线</a></p></td>
<td></td>
<td><p>2010</p></td>
<td><p><a href="../Page/十三号街站.md" title="wikilink">十三号街</a>－<a href="../Page/黎明广场站.md" title="wikilink">黎明广场</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西安地铁.md" title="wikilink">西安地铁</a><a href="../Page/西安地铁2号线.md" title="wikilink">2号线</a></p></td>
<td></td>
<td><p>2011 - 2014</p></td>
<td><p><a href="../Page/北客站站.md" title="wikilink">北客站</a>－<a href="../Page/韦曲南站.md" title="wikilink">韦曲南</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/上海地鐵.md" title="wikilink">上海地鐵</a><a href="../Page/上海轨道交通9号线.md" title="wikilink">9号线</a></p></td>
<td></td>
<td><p>2007 - 2010</p></td>
<td><p><a href="../Page/杨高中路站.md" title="wikilink">杨高中路</a>－<a href="../Page/九亭站.md" title="wikilink">九亭</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/苏州轨道交通.md" title="wikilink">苏州轨道交通</a><a href="../Page/苏州轨道交通1号线.md" title="wikilink">1号线</a></p></td>
<td></td>
<td><p>2007－2011</p></td>
<td><p><a href="../Page/木渎站.md" title="wikilink">木渎</a>－<a href="../Page/钟南街站.md" title="wikilink">钟南街</a>；其中<a href="../Page/金鸡湖.md" title="wikilink">金鸡湖底部分隧道是中国最长的湖底地铁隧道</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/深圳地铁.md" title="wikilink">深圳地铁</a><a href="../Page/深圳地铁9号线.md" title="wikilink">9号线</a></p></td>
<td></td>
<td><p>2016</p></td>
<td><p><a href="../Page/红树湾站.md" title="wikilink">红树湾</a>－<a href="../Page/文锦站.md" title="wikilink">文锦</a> [1]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/成都地铁.md" title="wikilink">成都地铁</a><a href="../Page/成都地铁1号线.md" title="wikilink">1号线</a></p></td>
<td></td>
<td><p>2010</p></td>
<td><p><a href="../Page/升仙湖站.md" title="wikilink">升仙湖</a>－<a href="../Page/世纪城站.md" title="wikilink">世纪城</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/北京地铁.md" title="wikilink">北京地铁</a><a href="../Page/北京地铁2号线.md" title="wikilink">2号线</a></p></td>
<td></td>
<td><p>1969 - 1987</p></td>
<td><p>环线，<a href="../Page/西直门站.md" title="wikilink">西直门</a>－<a href="../Page/北京站_(地铁).md" title="wikilink">北京站</a>－西直门</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/上海轨道交通.md" title="wikilink">上海轨道交通</a><a href="../Page/上海轨道交通4号线.md" title="wikilink">4号线</a></p></td>
<td></td>
<td><p>2005－2007</p></td>
<td><p><a href="../Page/海伦路站.md" title="wikilink">海伦路</a>－<a href="../Page/蓝村路站.md" title="wikilink">蓝村路</a>－<a href="../Page/大木桥路站.md" title="wikilink">大木桥路</a>－<a href="../Page/宜山路站.md" title="wikilink">宜山路</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/南京地铁.md" title="wikilink">南京地铁</a><a href="../Page/南京地铁2号线.md" title="wikilink">2号线</a></p></td>
<td></td>
<td><p>2010</p></td>
<td><p><a href="../Page/雨润大街站.md" title="wikilink">雨润大街</a>－<a href="../Page/马群站.md" title="wikilink">马群西</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/上海轨道交通.md" title="wikilink">上海轨道交通</a><a href="../Page/上海轨道交通6号线.md" title="wikilink">6号线</a></p></td>
<td></td>
<td><p>2007</p></td>
<td><p><a href="../Page/博兴路站.md" title="wikilink">博兴路</a>－<a href="../Page/东方体育中心站.md" title="wikilink">东方体育中心</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/苏州轨道交通.md" title="wikilink">苏州轨道交通</a><a href="../Page/苏州轨道交通2号线.md" title="wikilink">2号线</a></p></td>
<td></td>
<td><p>2009－2012</p></td>
<td><p><a href="../Page/陆慕站.md" title="wikilink">陆慕</a>－<a href="../Page/宝带桥南站.md" title="wikilink">宝带桥南</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/北京地铁.md" title="wikilink">北京地铁</a><a href="../Page/北京地铁8号线.md" title="wikilink">8号线</a></p></td>
<td></td>
<td><p>2008 - 2012</p></td>
<td><p><a href="../Page/回龙观东大街站.md" title="wikilink">回龙观东大街</a>－<a href="../Page/鼓楼大街站.md" title="wikilink">鼓楼大街</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/上海轨道交通.md" title="wikilink">上海轨道交通</a><a href="../Page/上海轨道交通1号线.md" title="wikilink">1号线</a></p></td>
<td></td>
<td><p>1995</p></td>
<td><p><a href="../Page/上海南站站.md" title="wikilink">上海南站</a>－<a href="../Page/上海马戏城站.md" title="wikilink">上海马戏城</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/北京地铁.md" title="wikilink">北京地铁</a><a href="../Page/北京地铁5号线.md" title="wikilink">5号线</a></p></td>
<td></td>
<td><p>2007</p></td>
<td><p><a href="../Page/惠新西街北口站.md" title="wikilink">惠新西街北口</a>－<a href="../Page/宋家庄站_(北京).md" title="wikilink">宋家庄</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/深圳地铁.md" title="wikilink">深圳地铁</a><a href="../Page/深圳地铁3号线.md" title="wikilink">3号线</a></p></td>
<td></td>
<td><p>2010－2011</p></td>
<td><p><a href="../Page/水贝站.md" title="wikilink">水贝</a>－<a href="../Page/益田站_(深圳).md" title="wikilink">益田</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/广州地铁.md" title="wikilink">广州地铁</a><a href="../Page/广州地铁4号线.md" title="wikilink">4号线</a></p></td>
<td></td>
<td><p>2005</p></td>
<td><p><a href="../Page/新造站.md" title="wikilink">新造</a>－<a href="../Page/黃村站_(廣州).md" title="wikilink">黄村</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/北京地铁.md" title="wikilink">北京地铁</a><a href="../Page/北京地铁9号线.md" title="wikilink">9号线</a></p></td>
<td></td>
<td><p>2011－2012</p></td>
<td><p><a href="../Page/国家图书馆站.md" title="wikilink">国家图书馆</a>－<a href="../Page/郭公庄站.md" title="wikilink">郭公庄</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/广州地铁.md" title="wikilink">广州地铁</a><a href="../Page/广州地铁1号线.md" title="wikilink">1号线</a></p></td>
<td></td>
<td><p>1997</p></td>
<td><p><a href="../Page/花地湾站.md" title="wikilink">花地湾</a>－<a href="../Page/广州东站_(地铁).md" title="wikilink">广州东站</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/天津地铁.md" title="wikilink">天津地铁</a><a href="../Page/天津地铁1号线.md" title="wikilink">1号线</a></p></td>
<td></td>
<td><p>1976 - 2006</p></td>
<td><p><a href="../Page/勤俭道站.md" title="wikilink">勤俭道</a>－<a href="../Page/土城站_(天津市).md" title="wikilink">土城</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/上海轨道交通.md" title="wikilink">上海轨道交通</a><a href="../Page/上海轨道交通2号线.md" title="wikilink">2号线</a></p></td>
<td></td>
<td><p>2000</p></td>
<td><p><a href="../Page/张江高科站.md" title="wikilink">张江高科</a>－<a href="../Page/凌空路站.md" title="wikilink">凌空路</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/广州地铁.md" title="wikilink">广州地铁</a><a href="../Page/广州地铁8号线.md" title="wikilink">8号线</a></p></td>
<td></td>
<td><p>2003 - 2010</p></td>
<td><p><a href="../Page/凤凰新村站.md" title="wikilink">凤凰新村</a>－<a href="../Page/万胜围站_(地铁).md" title="wikilink">万胜围</a></p></td>
</tr>
</tbody>
</table>

### 水利隧道

## 按地区排列

### 华北

#### 北京

  - [晓月隧道](../Page/晓月隧道.md "wikilink")：位于北京五環路上，是北京市區內唯一的隧道，也是北京環路的唯一一条隧道。
  - [卧龙岗隧道](../Page/卧龙岗隧道.md "wikilink")
  - [东方红隧道](../Page/东方红隧道.md "wikilink")
  - [居庸关隧道](../Page/居庸关隧道.md "wikilink")
  - [八达岭隧道](../Page/八达岭隧道.md "wikilink")
  - [羊山隧道](../Page/羊山隧道.md "wikilink")
  - [分水岭隧道](../Page/分水岭隧道.md "wikilink")
  - [黄土梁隧道](../Page/黄土梁隧道.md "wikilink")
  - [汤河口隧道](../Page/汤河口隧道.md "wikilink")
  - [创新隧道](../Page/创新隧道.md "wikilink")
  - [古北口隧道](../Page/古北口隧道.md "wikilink")
  - [大屯路隧道](../Page/大屯路隧道.md "wikilink")
  - [担礼隧道](../Page/担礼隧道.md "wikilink")
  - [松树龄隧道](../Page/松树龄隧道.md "wikilink")
  - [德胜口隧道](../Page/德胜口隧道.md "wikilink")
  - [青松岭隧道](../Page/青松岭隧道.md "wikilink")
  - [长操隧道](../Page/长操隧道.md "wikilink")
  - [二道河隧道](../Page/二道河隧道.md "wikilink")
  - [岔道城隧道](../Page/岔道城隧道.md "wikilink")
  - [弹琴峡隧道](../Page/弹琴峡隧道.md "wikilink")
  - [柏峪隧道](../Page/柏峪隧道.md "wikilink")
  - [五合隧道](../Page/五合隧道.md "wikilink")
  - [大岭后隧道](../Page/大岭后隧道.md "wikilink")
  - [栢查子隧道](../Page/栢查子隧道.md "wikilink")
  - [琉璃庙隧道](../Page/琉璃庙隧道.md "wikilink")
  - [苍术会隧道](../Page/苍术会隧道.md "wikilink")
  - [麒麟楼隧道](../Page/麒麟楼隧道.md "wikilink")
  - [慧忠路隧道](../Page/慧忠路隧道.md "wikilink")
  - [九道河隧道](../Page/九道河隧道.md "wikilink")
  - [山京沟隧道](../Page/山京沟隧道.md "wikilink")
  - [陡岭子隧道](../Page/陡岭子隧道.md "wikilink")
  - [东老峪隧道](../Page/东老峪隧道.md "wikilink")
  - [潭峪沟隧道](../Page/潭峪沟隧道.md "wikilink")
  - [河防口隧道](../Page/河防口隧道.md "wikilink")
  - [栗园厂隧道](../Page/栗园厂隧道.md "wikilink")
  - [黎园岭隧道](../Page/黎园岭隧道.md "wikilink")
  - [邓家湾隧道](../Page/邓家湾隧道.md "wikilink")
  - [东长峪隧道](../Page/东长峪隧道.md "wikilink")
  - [火郎峪隧道](../Page/火郎峪隧道.md "wikilink")
  - [横岺根隧道](../Page/横岺根隧道.md "wikilink")
  - [前安岭隧道](../Page/前安岭隧道.md "wikilink")
  - [安岭梁隧道](../Page/安岭梁隧道.md "wikilink")
  - [西驼古隧道](../Page/西驼古隧道.md "wikilink")
  - [黑古沿隧道](../Page/黑古沿隧道.md "wikilink")
  - [横城子隧道](../Page/横城子隧道.md "wikilink")
  - [山羊洼一号隧道](../Page/山羊洼一号隧道.md "wikilink")
  - [山羊洼二号隧道](../Page/山羊洼二号隧道.md "wikilink")
  - [石佛寺一号隧道](../Page/石佛寺一号隧道.md "wikilink")
  - [石佛寺二号隧道](../Page/石佛寺二号隧道.md "wikilink")
  - [金鼎湖一号隧道](../Page/金鼎湖一号隧道.md "wikilink")
  - [云柏一号隧道](../Page/云柏一号隧道.md "wikilink")
  - [云柏二号隧道](../Page/云柏二号隧道.md "wikilink")
  - [西圈一号隧道](../Page/西圈一号隧道.md "wikilink")
  - [金鼎湖二号隧道](../Page/金鼎湖二号隧道.md "wikilink")
  - [西圈二号隧道](../Page/西圈二号隧道.md "wikilink")
  - [108国道罗喉岭隧道](../Page/108国道罗喉岭隧道.md "wikilink")

#### 天津

  - [越秀路隧道](../Page/越秀路隧道.md "wikilink")
  - [五经路隧道](../Page/五经路隧道.md "wikilink")
  - [东纵快速路隧道](../Page/东纵快速路隧道.md "wikilink")
  - [海河东路隧道](../Page/海河东路隧道.md "wikilink")
  - [津湾张自忠路隧道](../Page/津湾张自忠路隧道.md "wikilink")
  - [中央大道海河隧道](../Page/中央大道海河隧道.md "wikilink")
  - [西青道下沉隧道](../Page/西青道下沉隧道.md "wikilink")

#### 石家庄

  - [西柏坡隧道](../Page/西柏坡隧道.md "wikilink")
  - [木盘隧道](../Page/木盘隧道.md "wikilink")

#### 秦皇岛

  - [界岭隧道](../Page/界岭隧道.md "wikilink")
  - [王子店隧道](../Page/王子店隧道.md "wikilink")
  - [朱杖子隧道](../Page/朱杖子隧道.md "wikilink")
  - [青松岭隧道](../Page/青松岭隧道.md "wikilink")

### 东北

#### 哈尔滨

  - [天恒山隧道](../Page/天恒山隧道.md "wikilink")

#### 沈阳

  - [浑河隧道](../Page/浑河隧道.md "wikilink")
  - [五爱隧道](../Page/五爱隧道.md "wikilink")

#### 大连

  - [松树隧道](../Page/松树隧道.md "wikilink")
  - [金州隧道](../Page/金州隧道.md "wikilink")
  - [太平坦隧道](../Page/太平坦隧道.md "wikilink")
  - [白垩纪隧道](../Page/白垩纪隧道.md "wikilink")
  - [南隈子隧道](../Page/南隈子隧道.md "wikilink")
  - [大龙湾隧道](../Page/大龙湾隧道.md "wikilink")
  - [白云街隧道](../Page/白云街隧道.md "wikilink")
  - [毛沟东隧道](../Page/毛沟东隧道.md "wikilink")
  - [北良港公路隧道](../Page/北良港公路隧道.md "wikilink")

### 华东

#### 上海

跨长江隧道

  - [上海长江隧道](../Page/上海长江隧道.md "wikilink")

跨黄浦江隧道（自下游至上游）

  - [外环隧道](../Page/外环隧道.md "wikilink")
  - [翔殷路隧道](../Page/翔殷路隧道.md "wikilink")
  - [军工路隧道](../Page/军工路隧道.md "wikilink")
  - [大连路隧道](../Page/大连路隧道.md "wikilink")
  - [新建路隧道](../Page/新建路隧道.md "wikilink")
  - [延安东路隧道](../Page/延安东路隧道.md "wikilink")
  - [人民路隧道](../Page/人民路隧道.md "wikilink")
  - [复兴东路隧道](../Page/复兴东路隧道.md "wikilink")
  - [西藏南路隧道](../Page/西藏南路隧道.md "wikilink")
  - [打浦路隧道](../Page/打浦路隧道.md "wikilink")：中国第一条水底公路隧道。
  - [龙耀路隧道](../Page/龙耀路隧道.md "wikilink")
  - [上中路隧道](../Page/上中路隧道.md "wikilink")
  - [上海外滩观光隧道](../Page/上海外滩观光隧道.md "wikilink")（仅供行人观光通行）

其他

  - [外滩隧道](../Page/外滩隧道.md "wikilink")

#### 武汉

  - [武汉长江隧道](../Page/武汉长江隧道.md "wikilink")：长江上第一条过江隧道，双向4车道，位于武汉市区，连接汉口和武昌，于2008年12月通车。
  - [武汉地铁2号线过江隧道](../Page/武汉地铁2号线过江隧道.md "wikilink")：长江上第一条地铁过江隧道
  - [三阳路长江隧道](../Page/三阳路长江隧道.md "wikilink")
  - [中山路隧道](../Page/中山路隧道.md "wikilink")
  - [水果湖隧道](../Page/水果湖隧道.md "wikilink")
  - [地大隧道](../Page/地大隧道.md "wikilink")

#### 南昌

  - [青山湖隧道](../Page/青山湖隧道.md "wikilink")
  - [洛阳路隧道](../Page/洛阳路隧道.md "wikilink")
  - [萧峰隧道](../Page/萧峰隧道.md "wikilink")

#### 杭州

跨[钱塘江隧道](../Page/钱塘江.md "wikilink")

  - [庆春路过江隧道](../Page/庆春路过江隧道.md "wikilink")
  - [钱江隧道](../Page/钱江隧道.md "wikilink")

#### 福州

  - [馬尾隧道](../Page/馬尾隧道.md "wikilink")
  - [鼓山隧道](../Page/鼓山隧道.md "wikilink")
  - [快安隧道](../Page/快安隧道.md "wikilink")
  - [潼关隧道](../Page/潼关隧道.md "wikilink")
  - [颜岐隧道](../Page/颜岐隧道.md "wikilink")
  - [牛岩山隧道](../Page/牛岩山隧道.md "wikilink")（在建）

#### 厦门

  - [翔安隧道](../Page/翔安隧道.md "wikilink")：中国第一条由国内专家自行设计的海底隧道，世界第一条采用钻爆法施工的海底隧道，于2005年8月9日正式动工建设，于2009年11月6日全线贯通，于2010年4月26日正式通车。
  - [东渡隧道](../Page/东渡隧道.md "wikilink")
  - [梧村隧道](../Page/梧村隧道.md "wikilink")
  - [新阳隧道](../Page/新阳隧道.md "wikilink")
  - [凤溪隧道](../Page/凤溪隧道.md "wikilink")

#### 泉州

  - [垵口隧道](../Page/垵口隧道.md "wikilink")
  - [前鸥隧道](../Page/前鸥隧道.md "wikilink")
  - [马跳隧道](../Page/马跳隧道.md "wikilink")

#### 江苏

  - [独墅湖隧道](../Page/独墅湖隧道.md "wikilink")：穿越[苏州城中独墅湖的一条隧道](../Page/苏州.md "wikilink")，地面部分双向8车道，隧道部分双向6车道，隧道部分长3.46公里，投资24.5亿元，于2007年10月10日通车。
  - [南京长江隧道](../Page/南京长江隧道.md "wikilink")
  - [玄武湖隧道](../Page/玄武湖隧道.md "wikilink")
  - [御窑路隧道](../Page/御窑路隧道.md "wikilink")

### 华南

#### 广东

  - [港珠澳大桥海底隧道](../Page/港珠澳大桥.md "wikilink")
  - [珠江隧道](../Page/珠江隧道.md "wikilink")
  - [龙头山隧道](../Page/龙头山隧道.md "wikilink")
  - [石陂隧道](../Page/石陂隧道.md "wikilink")
  - [梧桐山隧道](../Page/梧桐山隧道.md "wikilink")
  - [林场隧道](../Page/林场隧道.md "wikilink")
  - [龙华隧道](../Page/龙华隧道.md "wikilink")
  - [盐田隧道](../Page/盐田隧道.md "wikilink")
  - [雅宝隧道](../Page/雅宝隧道.md "wikilink")
  - [加林山隧道](../Page/加林山隧道.md "wikilink")
  - [板樟山隧道](../Page/板樟山隧道.md "wikilink")
  - [鳳凰山隧道](../Page/鳳凰山隧道.md "wikilink")
  - [拱北隧道](../Page/拱北隧道.md "wikilink")
  - [德胜路隧道](../Page/德胜路隧道.md "wikilink")

#### 香港

#### 澳门

  - [松山隧道](../Page/松山隧道.md "wikilink")
  - [氹仔隧道](../Page/氹仔隧道.md "wikilink")

#### 海口

  - [侨中路隧道](../Page/侨中路隧道.md "wikilink")

### 西南

#### 重庆

  - [朝天门隧道](../Page/朝天门隧道.md "wikilink")
  - [真武山隧道](../Page/真武山隧道.md "wikilink")
  - [黄沙溪隧道](../Page/黄沙溪隧道.md "wikilink")
  - [重庆八一隧道](../Page/重庆八一隧道.md "wikilink")
  - [圆梁山隧道](../Page/圆梁山隧道.md "wikilink")
  - [中梁山隧道](../Page/中梁山隧道.md "wikilink")
  - [大坡隧道](../Page/大坡隧道.md "wikilink")
  - [老土隧道](../Page/老土隧道.md "wikilink")
  - [三建隧道](../Page/三建隧道.md "wikilink")

#### 贵阳

  - [黔春隧道](../Page/黔春隧道.md "wikilink")
  - [偏坡隧道](../Page/偏坡隧道.md "wikilink")
  - [土巴寨隧道](../Page/土巴寨隧道.md "wikilink")
  - [曹坝冲隧道](../Page/曹坝冲隧道.md "wikilink")

#### 成都

  - [土桥隧道](../Page/土桥隧道.md "wikilink")
  - [渠江路隧道](../Page/渠江路隧道.md "wikilink")
  - [大渡河路隧道](../Page/大渡河路隧道.md "wikilink")
  - [友谊隧道](../Page/友谊隧道.md "wikilink")
  - [龙池隧道](../Page/龙池隧道.md "wikilink")

### 其他地區

  - [秦岭终南山公路隧道](../Page/秦岭终南山公路隧道.md "wikilink")：位居世界第二長、亞洲第一長的公路隧道。
  - [台灣海峽隧道](../Page/台灣海峽隧道.md "wikilink")：計畫中，不但面臨政經問題，困難度及危險性也極高
  - 川藏公路[海子山隧道](../Page/海子山隧道.md "wikilink")：全長11.36公里。
  - [港珠澳大桥海底隧道段](../Page/港珠澳大桥.md "wikilink")：兴建中。

#### 兰州

  - [高岭子隧道](../Page/高岭子隧道.md "wikilink")
  - [车道岭隧道](../Page/车道岭隧道.md "wikilink")
  - [太平隧道](../Page/太平隧道.md "wikilink")

## 相關條目

  - [台灣隧道](../Page/台灣隧道.md "wikilink")

[Category:中国隧道](../Category/中国隧道.md "wikilink")
[Category:隧道列表](../Category/隧道列表.md "wikilink")
[Category:列表索引](../Category/列表索引.md "wikilink")

1.