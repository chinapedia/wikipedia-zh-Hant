[BattleOfShanghaiBaby.gif](https://zh.wikipedia.org/wiki/File:BattleOfShanghaiBaby.gif "fig:BattleOfShanghaiBaby.gif")被炸毁的废墟中一名哭泣的宝宝的著名照片。\]\]

《**中國娃娃**》（）是[中國抗日戰爭期間極為著名之](../Page/中國抗日戰爭.md "wikilink")[戰地攝影作品](../Page/戰地攝影.md "wikilink")\[1\]，該作品首度公開於1937年的美國《[生活雜誌](../Page/生活雜誌.md "wikilink")》。攝影者為[美籍華裔戰地記者](../Page/美籍華裔.md "wikilink")[王小亭](../Page/王小亭.md "wikilink")，時間地點則為[淞滬會戰](../Page/淞滬會戰.md "wikilink")、8月28日的[上海](../Page/上海市_\(中華民國\).md "wikilink")[南火車站](../Page/上海南站_\(1908年–1937年\).md "wikilink")。

名為《中國娃娃》的該照片，因為殘破的戰爭廢墟與哭泣的小嬰兒圖像，不但震驚[美國輿論界](../Page/美國.md "wikilink")，甚至迅速流傳於全世界。有許多人認為，該照片是美國民眾從支持到反對[日本侵略中國的關鍵之一](../Page/大日本帝国.md "wikilink")\[2\]。《生活雜誌》事後粗算，該照片光是當期就有1億3千6百萬人瀏覽閱讀\[3\]。这张照片也被称为《没有母亲的中国娃娃》（）\[4\]、血腥的星期六（）\[5\]、上海火车站的婴儿（）\[6\]。

## 背景

1937年7月7日[盧溝橋事變發生](../Page/盧溝橋事變.md "wikilink")，中日全面戰爭爆發。1937年8月，[日軍派出](../Page/日軍.md "wikilink")[上海派遣軍](../Page/上海派遣軍.md "wikilink")，之後更名為中國派遣軍的該軍隊並迅速增加至50萬，並開始向上海進攻。另一方面，[中華民國政府則共派出](../Page/中華民國政府.md "wikilink")[國民革命軍](../Page/國民革命軍.md "wikilink")70萬展開上海保衛戰。

因為上海屬國際都市，[中日戰爭隨即引起世界各地新聞媒體注目](../Page/抗日战争.md "wikilink")。而其中，提供[英](../Page/英國.md "wikilink")[美報紙雜誌通訊的](../Page/美國.md "wikilink")[萬國新聞通訊社](../Page/萬國新聞通訊社.md "wikilink")、[赫斯特新聞社也都派員參與報導](../Page/赫斯特新聞社.md "wikilink")。而華裔攝影新聞記者[王小亭](../Page/王小亭.md "wikilink")，則屬於赫斯特新聞社記者。

王小亭（1900年－1983年）為出生於美國之華裔知名攝影師，於上海開設照相館\[7\]\[8\]。於美學成後，王小亭即擔任英美公司的新聞短片攝影師。之後，他於1925年－1937年間，歷任「萬國新聞通訊社」攝影記者、上海申報新聞攝影記者及美國赫斯特新聞社記者。事實上，他為中國第一位姓名可考之新聞攝影記者\[9\]。王小亭在8月14日曾拍攝許多戰爭照片，當天也被稱為「血腥星期天」\[10\]。

## 照片场景

一个满身鲜血的幼童坐在上海火车站废墟旁的铁轨上，惶恐地嚎啕大哭。

## 淞滬會戰

8月13日，超过10,000名日本海軍陸戰隊攻擊上海[虹口区](../Page/虹口区.md "wikilink")，與中国保安队正面會戰。日本本來预期3天就可以轻松征服上海，3个月就可征服中国，不過卻遭遇了意想不到的中國軍隊激烈抵抗。激戰十日後的8月22日，日本正式派出陸軍第3、8和11师團在海军炮轰的配合下，发起[两栖作战](../Page/两栖作战.md "wikilink")，继续在[川沙](../Page/川沙.md "wikilink")、[宝山登陆](../Page/寶山區_\(上海市\).md "wikilink")。中國國軍於日舰猛烈炮火，无法进行有效反攻。

8月23日晨，日军陸軍第11师团連同日本海軍與中国海防司令[沈鴻烈部戰艦激戰](../Page/沈鴻烈.md "wikilink")，隨後，日軍以船艦噸量與數量佔優勢，中国海防失守。之後，日軍30餘艘敌舰密集炮火掩护下，向[狮子林](../Page/狮子林.md "wikilink")、川沙口登陆，攻击宝山、月浦、罗店。[美国国务卿](../Page/美国国务卿.md "wikilink")[赫尔呼吁](../Page/科德尔·赫尔.md "wikilink")，中日双方停战。不過，因[國民政府軍事委員會委員長](../Page/國民政府軍事委員會.md "wikilink")[蔣中正決定全面抗戰](../Page/蔣中正.md "wikilink")，中日雙方拒絕停戰呼籲，於是發生淞滬會戰中期的[張華浜戰鬥](../Page/張華浜戰鬥.md "wikilink")。

1937年8月23日至27日，以上海市區張華浜及上海南火車站一帶的巷戰展開，日軍遭到中國國軍突襲並產生意料之外死傷。不過因為日軍陸軍增援及海空軍取得優勢，日軍迅速獲得勝利，但是為了肅清中國頑抗的堅守殘軍以及展現軍力，仍於張華浜戰鬥後之8月28日對於上海展開[戰略轟炸](../Page/戰略轟炸.md "wikilink")，而地點即為張華浜港區與上海南火車站之非租界地區。

## 拍攝過程

[Shanghaibabywithboysout.jpg](https://zh.wikipedia.org/wiki/File:Shanghaibabywithboysout.jpg "fig:Shanghaibabywithboysout.jpg")》杂志上\]\]
[Babyrescued.jpg](https://zh.wikipedia.org/wiki/File:Babyrescued.jpg "fig:Babyrescued.jpg")

8月28日上午開始，越過港口的日軍，出動百多架次的轟炸機於上海進行戰略性轟炸。1930年代中期，上海共有上海北與上海南兩火車站，上海北火車站因為處於戰區，運輸於8月中旬停止。因此上海南火車站成為上海唯一交通通道。8月28日下午1時45分左右，日軍展開對張華浜周圍要點展開轟炸。其中，日軍飛機十二架次轟炸機執行任務時轟炸了南火車站（日軍隨後辯稱為誤炸），而該轟炸當場炸死700人，後經中方清算，上海南火車站轟炸所犧牲的上海市民共達千人左右。

此轟炸進行中，任職於美國赫斯特新聞社之新聞記者王小亭所在辦公地點，相當靠近南火車站。據王小亭自述，《中國娃娃》作品即是奔逃期間所拍攝作品。另外，依照相關16mm紀錄片記載，轟炸過程中，中國一位救難人員越過廢墟抱起一重傷幼童放置鐵軌旁，並隨即投入其他救援行動。而此被男子放置鐵軌旁的待救援重傷孩童即是王小亭該《中國娃娃》的攝影作品主角。

這卷赫斯特新聞社底片經王小亭沖洗後，隨即被美的軍艦自上海送往[菲律賓](../Page/菲律賓.md "wikilink")[馬尼拉](../Page/馬尼拉.md "wikilink")，再通過[泛美航空飛機運抵美國](../Page/泛美航空.md "wikilink")[紐約](../Page/紐約.md "wikilink")，並交售美國[生活雜誌](../Page/生活雜誌.md "wikilink")，成為該周刊當期封面。該照片不但扭轉美國對於中國抗日戰爭的觀點，也造成該期周刊於全世界熱銷。據生活雜誌估計，仅當期就有1.36億人看了這張照片。

因為此照片，日軍轟炸行為遭到全世界輿論抨擊。日軍隨後也因此辯稱南火車站並非該轟炸任務的目標，而該轟炸純屬轟炸飛機機員將火車站移動列車誤認為是從張華浜撤退至上海其他市區之中國國軍，更宣稱該轟炸純粹於誤炸。除此，日軍則懸賞10萬元緝拿該照片的拍攝者。

## 常见错误用法

有不少[中国大陆的](../Page/中国大陆.md "wikilink")[网民误以为此图的拍摄的背景是在](../Page/网民.md "wikilink")[南京大屠杀时的](../Page/南京大屠杀.md "wikilink")[南京](../Page/南京.md "wikilink")，因此此图有意无意间经常被误当作是[南京大屠杀的](../Page/南京大屠杀.md "wikilink")[日军暴行图](../Page/日军暴行.md "wikilink")。事实上，此图摄于1937年的[上海](../Page/上海.md "wikilink")[淞沪会战期间](../Page/淞沪会战.md "wikilink")。

## 注釋

## 参考文献

### 引用

### 来源

  - 《[生活雜誌](../Page/生活雜誌.md "wikilink")》
  - 新華網
  - 《戰地記者——媒介時代的英雄》，人民網
  - 《上海星期日》，2005年9月4日《新聞晨報》
  - [終戰六十，困惑的事實！──關於德、日的戰爭影像](http://blog.roodo.com/chita/archives/374280.html)
  - [台灣新聞協會－王小亭觀看的力量](https://web.archive.org/web/20120326193453/http://www.atj.org.tw/newscon1.asp?page=prev1243)

## 参见

  - [淞滬會戰](../Page/淞滬會戰.md "wikilink")
  - [日本战争罪行](../Page/日本战争罪行.md "wikilink")
      - [无差别轰炸](../Page/无差别轰炸.md "wikilink")
  - [上海南站 (1908年–1937年)](../Page/上海南站_\(1908年–1937年\).md "wikilink")
  - 《[生活雜誌](../Page/生活雜誌.md "wikilink")》
      - [王小亭](../Page/王小亭.md "wikilink")

[Category:抗日戰爭背景作品](../Category/抗日戰爭背景作品.md "wikilink")
[Category:摄影作品](../Category/摄影作品.md "wikilink")
[Category:1937年作品](../Category/1937年作品.md "wikilink")
[Category:1937年上海](../Category/1937年上海.md "wikilink")
[Category:淞滬會戰](../Category/淞滬會戰.md "wikilink")
[Category:日军对中国城市的无差别轰炸](../Category/日军对中国城市的无差别轰炸.md "wikilink")
[Category:戰爭中的兒童](../Category/戰爭中的兒童.md "wikilink")

1.

2.

3.

4.

5.

6.

7.
8.  Camhi, Leslie. ["Film: A Dragon Lady and a Quiet Cultural
    Warrior"](http://www.nytimes.com/2004/01/11/movies/film-a-dragon-lady-and-a-quiet-cultural-warrior.html).
    *The New York Times*, January 11, 2004. Retrieved on July 3, 2011.

9.  [台灣記者協會，《王小亭觀看的力量》](http://www.atj.org.tw/newscon1.asp?page=prev1243)


10.