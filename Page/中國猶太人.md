**中國猶太人**的歷史既悠久又神秘。雖然[中國關於](../Page/中國.md "wikilink")[猶太人最早的文獻記錄可以追溯到公元](../Page/猶太人.md "wikilink")7世紀到8世紀間，但猶太人可能在更早的[汉朝就已經來到了中國](../Page/汉朝.md "wikilink")。從[唐](../Page/唐朝.md "wikilink")[宋年間一直到](../Page/宋朝.md "wikilink")[清朝](../Page/清朝.md "wikilink")，猶太人都一直生活在一個相對隔離的社群中，大部分都居住在[開封](../Page/開封.md "wikilink")。到1949年[中華人民共和國成立時](../Page/中華人民共和國.md "wikilink")，幾乎所有的中國猶太人都放棄了他們的宗教和文化信仰。不過在20世紀末和21世紀初，一些國際猶太人組織開始幫助中國猶太人恢復他們的傳統。

在19世紀和20世紀早期，隨著中國向西方開放[通商口岸以及割讓](../Page/通商口岸.md "wikilink")[半殖民地](../Page/半殖民地.md "wikilink")，一些猶太人移民到中國的[上海](../Page/上海.md "wikilink")、[天津](../Page/天津.md "wikilink")、[香港等商業中心](../Page/香港.md "wikilink")。隨後又有数万名猶太人爲了躲避[俄國的](../Page/俄國.md "wikilink")[大革命和](../Page/1917年俄國革命.md "wikilink")[納粹的](../Page/納粹.md "wikilink")[屠殺而來到中國](../Page/猶太人大屠殺.md "wikilink")。

在貿易擴大和全球化愈演愈烈的今天，有來自不同國家和不同宗教背景的猶太人暫時性或永久性地居住在中國當代的商業中心。

在中國開展[共產主義運動高潮的時代](../Page/共產主義.md "wikilink")，有一些著名的外國顧問和技術專家也是猶太人。其中部分人甚至入了中國國籍，成爲永久居民。

## 概述

中國的猶太人，包括[開封猶太人和其他社群](../Page/開封猶太人.md "wikilink")。

公元1世紀後，猶太人逃離家園並散佈在整個[歐亞大陸上](../Page/歐亞大陸.md "wikilink")，其中以[中亞地區最爲集中](../Page/中亞.md "wikilink")。到9世紀時，已經有大量的猶太商人通過各種渠道來到中國。

[第一次鴉片戰爭後](../Page/第一次鴉片戰爭.md "wikilink")，中國被迫開放通商口岸和割讓半殖民地，隨之一些猶太人在[大英帝國的保護下來到中國](../Page/大英帝國.md "wikilink")。他們中的大部分都來自當時[大英帝國的殖民地](../Page/大英帝國.md "wikilink")[印度或](../Page/印度.md "wikilink")[伊拉克](../Page/伊拉克.md "wikilink")。在20世紀的頭幾十年内，又有一些猶太人來到中國的經濟中心城市[上海和](../Page/上海.md "wikilink")[香港](../Page/香港.md "wikilink")。

1917年[俄國革命之後](../Page/1917年俄國革命.md "wikilink")，更多的猶太人以[難民的身份逃入中國](../Page/難民.md "wikilink")。在1930和1940年代，猶太人爲了逃避[納粹在](../Page/納粹.md "wikilink")[西歐的屠殺也大批地湧入中國](../Page/西歐.md "wikilink")，這些難民多屬[歐洲籍](../Page/歐洲.md "wikilink")。其中[上海](../Page/上海.md "wikilink")、[天津和](../Page/天津.md "wikilink")[哈尔滨以接納猶太難民的數量而聞名世界](../Page/哈尔滨.md "wikilink")。

幾個世紀以來，因爲和漢人通婚以及[文化同化](../Page/文化同化.md "wikilink")，加上中國歷朝歷代均無對猶太人進行打壓，開封猶太人已經和[漢人沒有多少區別](../Page/漢人.md "wikilink")，[中華人民共和國政府也不承認他們是一個獨立的](../Page/中華人民共和國政府.md "wikilink")[少數民族](../Page/少數民族.md "wikilink")。除非這些遺棄了祖先信仰的猶太人重新皈依[猶太教](../Page/猶太教.md "wikilink")，否則他們不具備移民[以色列的條件](../Page/以色列.md "wikilink")。

今天少数中國猶太人的後裔還生活在漢族和[回族社群中](../Page/回族.md "wikilink")。關於中國的猶太教和猶太人的學術研究也成爲了西方學者的焦點，在20世紀末取得了不少成果。

## 歷史

[上海犹太难民纪念馆.jpg](https://zh.wikipedia.org/wiki/File:上海犹太难民纪念馆.jpg "fig:上海犹太难民纪念馆.jpg")（今上海犹太难民纪念馆）\]\]
[Jewish_Club_Shanghai1.JPG](https://zh.wikipedia.org/wiki/File:Jewish_Club_Shanghai1.JPG "fig:Jewish_Club_Shanghai1.JPG")\]\]
[1280px-Tientsin_heping-Judaictemple-Chent-001.jpg](https://zh.wikipedia.org/wiki/File:1280px-Tientsin_heping-Judaictemple-Chent-001.jpg "fig:1280px-Tientsin_heping-Judaictemple-Chent-001.jpg")\]\]

  - 據稱，歷史上遷入中國的猶太人都源于古代[以色列王國的](../Page/以色列王國.md "wikilink")[失蹤的十支派](../Page/失蹤的十支派.md "wikilink")。
  - 中國猶太人中最著名的一支，[開封猶太人](../Page/開封猶太人.md "wikilink")，是在[北宋時期從中國西北部](../Page/北宋.md "wikilink")[伊斯蘭教地區遷入中國的](../Page/伊斯蘭教.md "wikilink")[河南省](../Page/河南省.md "wikilink")。

### 早期記錄

文獻資料顯示，因爲類似的風俗，中國的猶太人常常被其他中國人錯認作[穆斯林](../Page/穆斯林.md "wikilink")。在[元代](../Page/元代.md "wikilink")1329年到1354年的編年史文獻中，人們找到了可能是中國人對中國猶太人的最早的稱呼：“竹忽”、“朱乎得”（很可能來自於[希伯來語的יהודים](../Page/希伯來語.md "wikilink")，“猶太人”的意思）。這段編年史提到中國政府命令所有的中國猶太人進[京](../Page/大都.md "wikilink")。

然而，在中國之外的關於中國猶太人的記錄要比這早得多。一名9世紀的[阿拉伯旅行家Abou](../Page/阿拉伯.md "wikilink")
Zeyd Al Hassan認爲猶太教徒也是878年在[广州](../Page/广州.md "wikilink")（广府，Khanfu，
خانفو
）被屠杀的十二万人中的幾個宗教派系之一\[1\]。在8世紀，遷入中國的猶太人的數量已經不少，以至於中國政府必須指派特定的官員來管理、監督他們。

在[元代](../Page/元代.md "wikilink")，著名的[意大利旅行家](../Page/意大利.md "wikilink")[馬可·波羅和](../Page/馬可·波羅.md "wikilink")[摩洛哥使節](../Page/摩洛哥.md "wikilink")[伊本·巴圖塔分別來到中國](../Page/伊本·巴圖塔.md "wikilink")，在他們的遊記中都提到了中國猶太人的存在。伊本·巴圖塔写道，“杭州是一个十分美丽的城市，杭州的第二街区居住着犹太人，基督教徒，和土耳其人，人数众多”。\[2\]。

現代[西方對於中國猶太人的最早正式記錄可以追溯至](../Page/西方.md "wikilink")17世紀在[北京的](../Page/北京.md "wikilink")[耶穌會](../Page/耶穌會.md "wikilink")[傳教士的文獻](../Page/傳教士.md "wikilink")。

1605年，一位年輕的中國猶太人拜見了耶穌會傳教士[利瑪竇](../Page/利瑪竇.md "wikilink")，告訴他中國猶太社群信奉[一神教](../Page/一神教.md "wikilink")。據説，當這位猶太人看到[基督教的](../Page/基督教.md "wikilink")《[聖母子圖](../Page/聖母子圖.md "wikilink")》時，他誤認作是描繪《[希伯來聖經](../Page/希伯來聖經.md "wikilink")》裡的人物[利百加和](../Page/利百加.md "wikilink")[以掃或](../Page/以掃.md "wikilink")[雅各的繪畫](../Page/以色列_\(人名\).md "wikilink")。他還告訴利瑪竇他來自[開封](../Page/開封.md "wikilink")，在那裏生活著大量的猶太人。

利瑪竇派遣了一位中國教徒去拜訪開封。隨後，其他的耶穌会教士（大部分都是[歐洲人](../Page/歐洲人.md "wikilink")）也拜訪了這座城市。他們發現開封猶太社群建造了自己的[禮拜寺](../Page/猶太會堂.md "wikilink")，該寺面朝[東](../Page/東.md "wikilink")，收藏了大量的書刊手稿。

据钱文忠教授考证，北宋来华并汉化的犹太人改为17个汉姓。包括白、赵、金等。

### 起源

公元76年（[漢朝年間](../Page/漢朝.md "wikilink")），[羅馬帝國皇帝](../Page/羅馬帝國.md "wikilink")[提圖斯](../Page/提圖斯.md "wikilink")（Titus）佔領[耶路撒冷](../Page/耶路撒冷.md "wikilink")，隨後最初的一批猶太人經由[波斯遷入中國](../Page/波斯.md "wikilink")。一位歐洲神甫在1900年的論述中提到了一種假設，認爲猶太人有可能在[宋代從](../Page/宋代.md "wikilink")[印度沿海路來到中國](../Page/印度.md "wikilink")。

在開封發現的三根有碑銘的[石碑](../Page/石碑.md "wikilink")（Stele）爲人們提供了三種假設。最古老的一根追溯到1489年，紀念一個[猶太會堂的建造](../Page/猶太會堂.md "wikilink")，上面記載著猶太人在公元前2世紀到公元2世紀。

### 19世紀

移居[上海的第一波犹太人在](../Page/上海.md "wikilink")19世纪下半叶到达，许多是来自伊拉克的[西班牙系犹太人](../Page/西班牙系犹太人.md "wikilink")。这些犹太人的职业多为鸦片商人。

最早来到的一位是[伊利亚斯·大卫·沙逊](../Page/伊利亚斯·大卫·沙逊.md "wikilink")，在1845年为他父亲在孟买的公司开设了上海分公司。从此逐渐有一些犹太人从印度移居上海，大部分都是从孟买来的[老沙逊洋行](../Page/老沙逊洋行.md "wikilink")（[David
Sassoon](../Page/大衛·沙遜.md "wikilink") &
Co）职员。这个社团主要由[亚洲](../Page/亚洲.md "wikilink")
、德国和俄国犹太人组成，也有少数来自奥地利、法国和意大利。犹太人在对华鸦片贸易中占据极大份额，他们控制了大部分的鸦片贸易，同时也从事孟买棉纱的交易。少量人服务于租界的市政机构，例如[新沙逊洋行](../Page/新沙逊洋行.md "wikilink")（E.
D. Sassoon &
Co.）的股东[哈同就曾先后任职于](../Page/哈同.md "wikilink")[上海法租界公董局和](../Page/上海法租界公董局.md "wikilink")[上海公共租界工部局](../Page/上海公共租界工部局.md "wikilink")。

### 20世纪上半葉

1940年时，估计在中国的犹太人口共有36,000人\[3\]
。20世纪上半葉，在中国的犹太人主要居住在[上海](../Page/上海.md "wikilink")、[天津和](../Page/天津.md "wikilink")[哈尔滨](../Page/哈尔滨.md "wikilink")。

最初来自中东、途径印度和香港前来经商的[瑟法底猶太人](../Page/瑟法底猶太人.md "wikilink")，都在上海经营一些实力雄厚的洋行，拥有相当的社会地位。后来从俄国来了大批犹太难民，使得犹太人在上海的地位有所下降。

[1917年俄国革命之后](../Page/1917年俄国革命.md "wikilink")，沙俄贵族、[白军军官及数万名](../Page/白军.md "wikilink")[俄国犹太人](../Page/俄国.md "wikilink")（许多人是因资产或农地平分的反共人士）与[东正教徒一起迁移到中国东北的](../Page/东正教.md "wikilink")[哈尔滨](../Page/哈尔滨.md "wikilink")。其中包括以色列第12任总理[奥尔默特的祖父和父母](../Page/奥尔默特.md "wikilink")，俄国首相的家庭。

### 二戰前後

另一波18000名犹太移民，在1930年代从[德国](../Page/德国.md "wikilink")、[奥地利和](../Page/奥地利.md "wikilink")[波兰进入上海](../Page/波兰.md "wikilink")\[4\]。上海是一个开放的城市，对各种移民都很宽容，而當時在中國一些諸如[何凤山的外交官亦出於人道理由而向這些猶太移民簽發庇護護照](../Page/何凤山.md "wikilink")。1943年，日本占领军要求这18,000名犹太人，正式名称为「无国籍难民」，重新安置在上海虹口区一块面积3/4平方英里的区域裡
\[5\]，这一时期进入上海的犹太人总数等于前往[澳大利亚](../Page/澳大利亚.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[印度](../Page/印度.md "wikilink")、[新西兰和](../Page/新西兰.md "wikilink")[南非的犹太人人数的总和](../Page/南非.md "wikilink")。许多在中国的犹太人后来前往[中东创建了现代](../Page/中东.md "wikilink")[以色列国](../Page/以色列国.md "wikilink")。

在大屠杀期间，上海由于是世界上少数几个不需要签证的地方之一，成为犹太难民的一个重要的避风港。

在[中国抗日战争期间](../Page/中国抗日战争.md "wikilink")，犹太人对中国人提供了大量帮助，如[雅各布·罗森菲尔德博士](../Page/雅各布·罗森菲尔德.md "wikilink")，作为战地医生为共产党军队提供帮助，而[莫里斯·柯恩成了](../Page/莫里斯·柯恩.md "wikilink")[蒋中正的保镖](../Page/蒋中正.md "wikilink")，还有一些住在[香港的著名犹太商人在战争期间为中国难民提供了慈善捐助](../Page/香港.md "wikilink")。

战争后期，纳粹代表施加压力，要求日军制定计划，消灭上海犹太人，这一计划被犹太社团的领袖获悉。然而在日本人付诸实施之前，战争就结束了。

### 20世紀后半葉

[第二次世界大战之后](../Page/第二次世界大战.md "wikilink")，犹太人的生存威胁因当时法西斯的战败而消失，欧洲重新进入和平期以及犹太复国主义在犹太人中的盛行从而引起了大部分犹太人移民回迁到[欧洲](../Page/欧洲.md "wikilink")，或者前往[中东希翼建立](../Page/中东.md "wikilink")[犹太国](../Page/以色列.md "wikilink")，只有少数人留了下来。值得注意的是，[中华人民共和国成立以后](../Page/中华人民共和国.md "wikilink")，有两位最著名的住在中国并同情共产党的外国人都是犹太血统，他们是[沙博理和](../Page/沙博理.md "wikilink")[伊斯雷爾·愛潑斯坦](../Page/伊斯雷爾·愛潑斯坦.md "wikilink")。萨拉·埃马斯，一个在中国长大的犹太移民，成为两国建交后第一个从中国移民到以色列的犹太人
\[6\] \[7\]。

### 21世紀

今天有15个中国城市建起了犹太会堂，供犹太侨民和本地犹太人礼拜\[8\]。

2005年，以色列驻华大使馆在[长城举办了](../Page/长城.md "wikilink")[光明节庆典](../Page/光明节.md "wikilink")\[9\]。

## 中國著名的猶太人

  - [沙博理](../Page/沙博理.md "wikilink")

  - [漢斯·米勒](../Page/漢斯·米勒.md "wikilink")

  - [伊斯雷爾·愛潑斯坦](../Page/伊斯雷爾·愛潑斯坦.md "wikilink")

  -
## 参考资料

## 外部链接

  - [犹太人在中国-参考书目](http://www.daat.ac.il/DAAT/bibliogr/shlomi/bibel4-2.htm)


## 參見

  - [開封猶太人](../Page/開封猶太人.md "wikilink")
  - [失蹤的十支派](../Page/失蹤的十支派.md "wikilink")
  - [中華人民共和國宗教](../Page/中華人民共和國宗教.md "wikilink")
  - [中以關係](../Page/中以關係.md "wikilink")

{{-}}

[category:中国民族史](../Page/category:中国民族史.md "wikilink")
[category:中國未識別民族](../Page/category:中國未識別民族.md "wikilink")

[C](../Category/犹太人各族群.md "wikilink")
[Category:猶太歷史](../Category/猶太歷史.md "wikilink")
[中国犹太人](../Category/中国犹太人.md "wikilink")

1.  伟烈亚力 《中国的犹太人》 Alexander Wylie, Israelites in China, *China
    Researches* Historical section p15， 1897 Shanghai
2.  [伟烈亚力](../Page/伟烈亚力.md "wikilink")《中国的犹太人》Alexander Wylie,
    Israelites in China, *China Researches* Historical p17, 1897,
    Shanghai
3.  包括[满洲国](../Page/满洲国.md "wikilink")，天主教百科全书
4.
5.
6.
7.
8.
9.