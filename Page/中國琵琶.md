**琵琶**，又稱**批把**，中国传统[彈撥樂器](../Page/彈撥樂器.md "wikilink")，有2000多年的歷史。名稱來源一說為“推手為琵，引手為琶”，一說為樂器形似枇杷果。

分直颈和曲颈两种，两者皆适用於马背上使用。直颈琵琶的琴身成圆形，类似今日的[阮咸](../Page/阮咸.md "wikilink")，又称[秦汉子或](../Page/秦汉子.md "wikilink")[秦琵琶](../Page/秦琵琶.md "wikilink")，是产生于[秦朝至](../Page/秦朝.md "wikilink")[汉代的乐器](../Page/汉代.md "wikilink")。曲颈琵琶的琴身成椭圆梨形，近似今日的[琵琶](../Page/琵琶.md "wikilink")，是从马背上的民族传进中原的乐器。

## 歷史

[Pictura_CARPE_DIEM.jpg](https://zh.wikipedia.org/wiki/File:Pictura_CARPE_DIEM.jpg "fig:Pictura_CARPE_DIEM.jpg")-[北涼](../Page/北涼.md "wikilink")《燕居行樂圖》\]\]
[A_palace_concert.jpg](https://zh.wikipedia.org/wiki/File:A_palace_concert.jpg "fig:A_palace_concert.jpg")
琵琶又称“批把”，最早见于史载的是汉代[刘熙](../Page/刘熙.md "wikilink")《释名·释乐器》：“批把本出于胡中，马上所鼓也。推手前曰批，引手却曰把，象其鼓时，因以为名也。”意即批把是骑在马上弹奏的乐器，向前弹出称做批，向后挑进称做把；根据它演奏的特点而命名为“批把”。在古代，敲、击、弹、奏都称为鼓。当时的游牧人骑在马上好弹琵琶，因此为“马上所鼓也”。

[南北朝時](../Page/南北朝.md "wikilink")[竹林七賢中的](../Page/竹林七賢.md "wikilink")[阮咸善於演奏琵琶](../Page/阮咸_\(西晉\).md "wikilink")，所以後世称這種樂器為[阮咸](../Page/阮咸_\(樂器\).md "wikilink")，或是秦漢琵琶。秦漢琵琶琴身呈圓盤狀而直項。

曲項琵琶在[魏晉南北朝時代從](../Page/魏晉南北朝.md "wikilink")[龟兹傳到](../Page/龟兹.md "wikilink")[北周](../Page/北周.md "wikilink")\[1\]，其琴柄向後折曲，琴身作半梨形，為現代琵琶的前身。當時琵琶是橫抱用[撥子彈奏](../Page/撥子.md "wikilink")，馬背上彈奏，或用於伴奏或合奏\[2\]。當時西域音樂蔚為潮流，並用於交誼場合\[3\]。

[唐朝時期](../Page/唐朝.md "wikilink")，西域琵琶樂為詩人推崇，[白居易有](../Page/白居易.md "wikilink")《[琵琶行](../Page/琵琶行.md "wikilink")》诗，[元稹有](../Page/元稹.md "wikilink")《琵琶歌》；西域音樂受國家重視與鼓勵，宮廷設[梨園教坊負責](../Page/梨園教坊.md "wikilink")。西域琵琶樂師中著名的有[曹剛](../Page/曹剛.md "wikilink")、[裴神符](../Page/裴神符.md "wikilink")、[裴興奴等](../Page/裴興奴.md "wikilink")。[唐太宗時](../Page/唐太宗.md "wikilink")，[裴神符](../Page/裴神符.md "wikilink")**廢撥用手爪**\[4\]\[5\]，但不成主流；當時有僧侶帶琵琶到日本，日本保留唐制至今，稱[日本琵琶](../Page/日本琵琶.md "wikilink")（，），日本仍保存有唐時琵琶實物，如[正倉院的](../Page/正倉院.md "wikilink")**螺鈿紫檀五絃琵琶**。
[Gu_Hongzhong's_Night_Revels_1.jpg](https://zh.wikipedia.org/wiki/File:Gu_Hongzhong's_Night_Revels_1.jpg "fig:Gu_Hongzhong's_Night_Revels_1.jpg")》\]\]
宋朝以後，「[夷夏之防](../Page/夷夏之防.md "wikilink")」趨於嚴密，西域樂師及樂器不用於宮廷，琵琶技藝遂流入民間。至於在宋、元、明三代時，關於琵琶音樂發展的記載十分有限，樂譜方面更未有所發現。

明朝，琵琶形制發展成4[相](../Page/相.md "wikilink")8[品](../Page/品.md "wikilink")，彈奏方式採直抱指彈。1819年，[華秋蘋等人合編的](../Page/華秋蘋.md "wikilink")《[琵琶譜](../Page/琵琶譜.md "wikilink")》於[江蘇](../Page/江蘇.md "wikilink")[無錫出版](../Page/無錫.md "wikilink")，是目前所發現最早印行的琵琶樂譜。此後琵琶樂譜相繼出版，重要的出版物有1895年[李芳園所編的](../Page/李芳園.md "wikilink")《-{[南北派琵琶十三套大曲新譜](../Page/南北派琵琶十三套大曲新譜.md "wikilink")}-》、1916年[沈肇洲所編的](../Page/沈肇洲.md "wikilink")《[瀛洲古調](../Page/瀛洲古調.md "wikilink")》、1936年[徐卓所編的](../Page/徐卓.md "wikilink")《[瀛洲古調](../Page/瀛洲古調.md "wikilink")》、1926年[沈浩初所編的](../Page/沈浩初.md "wikilink")《[養正軒琵琶譜](../Page/養正軒琵琶譜.md "wikilink")》、1934年[何柳堂所編的](../Page/何柳堂.md "wikilink")《[琵琶樂譜](../Page/琵琶樂譜.md "wikilink")》。這些樂譜的編印者都是文人出身，亦有流傳於民間藝人之間的手抄本。
[1870年代的演奏者。](https://zh.wikipedia.org/wiki/File:MUSICIANS_1.jpg "fig:1870年代的演奏者。")
[Pipa3.jpg](https://zh.wikipedia.org/wiki/File:Pipa3.jpg "fig:Pipa3.jpg")
[Concert_de_musique_chinoise_Nanguan_(Auditorium_du_musée_Guimet)_(8027971895).jpg](https://zh.wikipedia.org/wiki/File:Concert_de_musique_chinoise_Nanguan_\(Auditorium_du_musée_Guimet\)_\(8027971895\).jpg "fig:Concert_de_musique_chinoise_Nanguan_(Auditorium_du_musée_Guimet)_(8027971895).jpg")藝術家王心心彈奏南管琵琶\]\]
[民國时期開始按照](../Page/民國.md "wikilink")[十二平均律增加相](../Page/十二平均律.md "wikilink")、品。目前的琵琶主要是6相24四品，全是半音排列，甚至發展出8相30品。

由於歷久遠，有較多古曲流傳，形成不同的表演流派。古曲按音樂類型可分為文曲、武曲，按樂曲型式可分為小曲、套曲、大-{曲}-，多為有標題的多段體，內容或寫景、敘事、寓意於景。

## 流派

### 南派

保持隋唐時期古典的形式以及演奏風格和曲目，橫抱用撥子彈，主要可見於[南管](../Page/南管.md "wikilink")、[北管音樂](../Page/北管.md "wikilink")，此外傳到日本的[日本琵琶也主要是這一系](../Page/日本琵琶.md "wikilink")。

### 北派

直抱、指（指甲，多用假指甲）彈。

### 南管琵琶

南管琵琶保留唐代時的橫抱方式，但已不用撥子，而是用指彈。

## 形制結構

### 南琵琶

[潮州音樂及日本所用](../Page/潮州音樂.md "wikilink")，保留[隋唐形制](../Page/隋朝.md "wikilink")。

### 北琵琶

現時常見的琵琶，有4根弦，名稱為子弦、中弦、老弦、纏弦。空弦定音为A－D－E－A，琴頸、琴身上共有6個相、24個品，除了最高音的1個品之外，其餘相、品均按十二平均律的半音排列。

### 南管琵琶

[南管音樂所使用的琵琶](../Page/南管.md "wikilink")，形制仍保留唐代琵琶的樣貌，琴身較為寬闊，面板因要配合南管較為輕柔的演奏，在製作時略為下凹，降低共鳴的音量。

### 怒族琵琶

[怒族琵琶為怒族傳統音樂會使用的撥弦樂器](../Page/怒族.md "wikilink")，當地稱之為「達比亞」，琴身有橢圓形或長梯形。

## 演奏指法

左手食指、中指、無名指及小指按弦，在一些近代作品中，在高音區域用大指按弦。向左或右推拉弦線以改變音高並製造滑音。並弦、絞弦等技法，發出特別效果。

大部份演奏者使用假指甲撥弦，一些對音色有特別要求的演奏者會使用真指甲撥弦。五個手指上皆以膠布固定假指甲在真指甲上。食指和大指以「滾指」能夠造成長音效果。而食指、中指、無名指、小指、大指五個手指「輪指」亦能夠造成長音效果。技法主要有[彈](../Page/彈.md "wikilink")、[挑](../Page/挑弦.md "wikilink")、[摭](../Page/摭.md "wikilink")、分、[摘](../Page/摘.md "wikilink")、[滾](../Page/滾_\(弦樂\).md "wikilink")、[輪](../Page/輪_\(弦樂\).md "wikilink")、[掃](../Page/掃_\(弦樂\).md "wikilink")、[拂等指法](../Page/掃_\(弦樂\).md "wikilink")，亦可以拍打琴身來得到敲擊樂的效果。

## 重要曲目

### 古曲

  - 《[敦煌琵琶譜](../Page/敦煌琵琶譜.md "wikilink")》，於1900年左右在[敦煌](../Page/敦煌.md "wikilink")[莫高窟藏经洞发现](../Page/莫高窟.md "wikilink")，經考為[唐代樂譜](../Page/唐代.md "wikilink")。通常称“敦煌乐谱”。
  - 《[玉輪抱](../Page/玉輪抱.md "wikilink")》，[王維](../Page/王維.md "wikilink")
  - 文曲
      - 《[塞上曲](../Page/塞上曲.md "wikilink")》
      - 《[月兒高](../Page/月兒高_\(音樂\).md "wikilink")》
      - 《[春江花月夜](../Page/春江花月夜.md "wikilink")》
      - 《[飛花點翠](../Page/飛花點翠.md "wikilink")》
      - 《[六要](../Page/六要.md "wikilink")》
      - 《[陳隋](../Page/陳隋.md "wikilink")》
  - 武曲
      - 《[十面埋伏](../Page/十面埋伏_\(樂曲\).md "wikilink")》
      - 《[霸王卸甲](../Page/霸王卸甲.md "wikilink")》
  - 小調連奏
      - 《[陽春白雪](../Page/陽春白雪.md "wikilink")》

### 1920年代

  - 《[歌舞引](../Page/歌舞引.md "wikilink")》 1927年,
    [劉天華](../Page/劉天華.md "wikilink")
  - 《[虛籟](../Page/虛籟.md "wikilink")》 1929年, 劉天華

### 1950年代

  - 《[大浪淘沙](../Page/大浪淘沙.md "wikilink")》 1950年發表,
    [華彥鈞](../Page/華彥鈞.md "wikilink")
  - 《[龍船](../Page/龍船.md "wikilink")》 1950年發表, 華彥鈞
  - 《[昭君出塞](../Page/昭君出塞.md "wikilink")》 1950年發表, 華彥鈞
  - 《[歡樂的日子](../Page/歡樂的日子_\(樂曲\).md "wikilink")》 1958年,
    [馬聖龍](../Page/馬聖龍.md "wikilink")

### 1960年代

  - 《[趕花會](../Page/趕花會.md "wikilink")》 1960年,
    [葉緒然](../Page/葉緒然.md "wikilink")
  - 《[天山之春](../Page/天山之春.md "wikilink")》1961年,[烏斯滿江](../Page/烏斯滿江.md "wikilink")、[俞禮純作曲](../Page/俞禮純.md "wikilink")，[王範地改編](../Page/王範地.md "wikilink")
  - 《[彝族舞曲](../Page/彝族舞曲.md "wikilink")》
    1965年,[芧源](../Page/芧源.md "wikilink")、[劉鐵山作曲](../Page/劉鐵山.md "wikilink")，[王惠然改編](../Page/王惠然.md "wikilink")

### 1970年代

  - 《[草原小姐妹琵琶協奏曲](../Page/草原英雄小姐妹#琵琶協奏曲.md "wikilink")》 1973年,
    [劉德海](../Page/劉德海.md "wikilink")、[吳祖強](../Page/吳祖強.md "wikilink")、[王燕樵](../Page/王燕樵.md "wikilink")
  - 《[花木蘭琵琶協奏曲](../Page/花木蘭琵琶協奏曲.md "wikilink")》 1979年,
    [顧冠仁](../Page/顧冠仁.md "wikilink")
  - 《[火把節之夜](../Page/火把節之夜.md "wikilink")》 1979年,
    [吳俊生](../Page/吳俊生.md "wikilink")

### 1980年代

  - 《[祝福琵琶協奏曲](../Page/祝福琵琶協奏曲.md "wikilink")》 1980年,
    [趙季平](../Page/趙季平.md "wikilink")
  - 《[新翻羽調綠腰](../Page/新翻羽調綠腰.md "wikilink")》 1982年,
    [方錦龍](../Page/方錦龍.md "wikilink")
  - 《[九連鈺](../Page/九連鈺.md "wikilink")》«Nine Jade Chains» 1983年,
    [楊靜](../Page/杨静_\(音乐家\).md "wikilink")
  - 《[品訴](../Page/品訴.md "wikilink")》 «Disclosure» 1984年,
    [楊靜](../Page/杨静_\(音乐家\).md "wikilink")
  - 《人生篇‧[天鵝](../Page/天鵝_\(音樂\).md "wikilink")》 1984年,
    [劉德海](../Page/劉德海.md "wikilink")
  - 《人生篇‧[老童](../Page/老童.md "wikilink")》 1984年,
    [劉德海](../Page/劉德海.md "wikilink")
  - 《人生篇‧[春蠶](../Page/春蠶_\(樂曲\).md "wikilink")》 1984年, 劉德海
  - 《人生篇‧[童年 (樂曲)](../Page/童年_\(樂曲\).md "wikilink")》 1985年, 劉德海
  - 《人生篇‧[秦俑 (樂曲)](../Page/秦俑_\(樂曲\).md "wikilink")》 1985年, 劉德海
  - 《[琵琶協奏曲](../Page/琵琶協奏曲_\(樂曲\).md "wikilink")》 1986年,
    [羅永暉](../Page/羅永暉.md "wikilink")
  - 《[功夫 (樂曲)](../Page/功夫_\(樂曲\).md "wikilink")》 1987年,
    [林樂培](../Page/林樂培.md "wikilink")
  - 《[怒琵琶協奏曲](../Page/怒琵琶協奏曲.md "wikilink")》,
    [盧亮輝](../Page/盧亮輝.md "wikilink")

### 1990年代

  - 《[天靈靈敘事曲](../Page/天靈靈敘事曲.md "wikilink")》（雙琵琶敘事曲） 1990年,
    [羅永暉](../Page/羅永暉.md "wikilink")
  - 《[太行歡歌](../Page/太行歡歌.md "wikilink")》（琵琶重奏曲） 1993年,
    [吳俊生](../Page/吳俊生.md "wikilink")
  - 《[龜茲舞曲](../Page/龜茲舞曲.md "wikilink")》 «Dance along the old Silk-road»
    1993年 [楊靜](../Page/杨静_\(音乐家\).md "wikilink")
  - 《[春秋琵琶協奏曲](../Page/春秋琵琶協奏曲.md "wikilink")》 1994年,
    [唐建平](../Page/唐建平.md "wikilink")
  - 《[撥墨仙人](../Page/撥墨仙人_\(樂曲\).md "wikilink")》 1996年,
    [羅永暉](../Page/羅永暉.md "wikilink")
  - 《[琵琶協奏曲](../Page/琵琶協奏曲_\(樂曲\).md "wikilink")》 1997年,
    [三木稔](../Page/三木稔.md "wikilink")
  - 《[千章掃](../Page/千章掃.md "wikilink")》 1997年,
    [羅永暉](../Page/羅永暉.md "wikilink")
  - 《[曙光 (樂曲)](../Page/曙光_\(樂曲\).md "wikilink")》 1997年,
    [林樂培](../Page/林樂培.md "wikilink")

### 2000年代

  - 《[楚漢協奏曲](../Page/楚漢協奏曲.md "wikilink")》 2001年,
    [賴德和](../Page/賴德和.md "wikilink")
  - 《[昭陵六駿](../Page/昭陵六駿_\(樂曲\).md "wikilink")》 2001年,
    [劉德海](../Page/劉德海.md "wikilink")
  - 《[楊家將協奏曲](../Page/楊家將協奏曲.md "wikilink")》 2003年,
    [鍾耀光](../Page/鍾耀光.md "wikilink")
  - 《[千秋頌隨想曲](../Page/千秋頌隨想曲.md "wikilink")》 ,
    [楊春林](../Page/楊春林.md "wikilink")
  - 《[月兒高幻想曲](../Page/月兒高幻想曲.md "wikilink")》,
    [黃曉飛](../Page/黃曉飛.md "wikilink")
  - 《[昭君別情](../Page/昭君別情.md "wikilink")》, 黃曉飛
  - 《[龍鳳圖騰](../Page/龍鳳圖騰.md "wikilink")》,
    [賈群達](../Page/賈群達.md "wikilink")
  - 《[間歇泉](../Page/間歇泉.md "wikilink")》«Geyser»,
    2000年，[楊靜](../Page/杨静_\(音乐家\).md "wikilink")
  - 《[夢斷敦煌](../Page/夢斷敦煌.md "wikilink")》«Severed Dream of Dunhuang»,
    2001年，[楊靜](../Page/杨静_\(音乐家\).md "wikilink")
  - 《[愛怨](../Page/愛怨.md "wikilink")》«Aien»（歌劇）2006年，[三木稔](../Page/三木稔.md "wikilink")

## 相關條目

  - [琵琶](../Page/琵琶.md "wikilink")
  - [阮咸 (乐器)](../Page/阮咸_\(乐器\).md "wikilink")
  - [日本琵琶](../Page/日本琵琶.md "wikilink")
  - [越南琵琶](../Page/越南琵琶.md "wikilink")

## 參考資料

[ja:琵琶\#中国の琵琶](../Page/ja:琵琶#中国の琵琶.md "wikilink")

[Category:中國琵琶](../Category/中國琵琶.md "wikilink")
[Category:汉语外来词](../Category/汉语外来词.md "wikilink")

1.  《[隋书](../Page/隋书.md "wikilink")·音乐志》：[周武帝时有](../Page/宇文邕.md "wikilink")[龟兹人](../Page/龟兹.md "wikilink")，曰苏祗婆，从[突厥皇后入国](../Page/突厥.md "wikilink")，善胡琵琶，听其所奏，一均之中，间有七声
2.  《隋书·音乐志》：[龟兹国](../Page/龟兹.md "wikilink")……其歌曲有《善善摩尼》，解曲有《婆伽兒》，舞曲有《小天》，又有《疏勒盐》。其乐器有[竖箜篌](../Page/竖箜篌.md "wikilink")、琵琶、五弦、笙、笛、箫、筚篥、毛员鼓、都昙鼓、答腊鼓、腰鼓、羯鼓、鸡娄鼓、铜拔、贝等十五
3.  [顏氏家訓](../Page/顏氏家訓.md "wikilink")：齊朝一士大夫有一兒年十七，頗曉書疏，教其鮮卑語及彈琵琶，稍欲通解，以此服事公卿，無不寵愛
4.  《新唐書‧禮樂志十一》：五弦，如琵琶而小，北國所出，舊以木撥彈，樂工裴神符初以手彈，太祖悅甚，後人習為搊琵琶
5.  無名氏《國史異纂》：貞觀中，彈琵琶裴洛兒，始廢撥用手爪，所謂搊琵琶也