**中國算學**是指[古代中国](../Page/古代中国.md "wikilink")[傳統的](../Page/傳統.md "wikilink")[数学](../Page/数学.md "wikilink")[体系](../Page/体系.md "wikilink")，簡稱**中算**，具有發展的[獨創性](../Page/獨創性.md "wikilink")，且具备[完整体系](../Page/完整.md "wikilink")。其基本[特征在于将](../Page/特征.md "wikilink")[实用问题](../Page/实用问题.md "wikilink")（包括[几何学问题](../Page/几何学.md "wikilink")）[代数化](../Page/代数.md "wikilink")，[转化为](../Page/转化.md "wikilink")[线性方程组](../Page/线性方程组.md "wikilink")、高次[多项式](../Page/多项式.md "wikilink")[方程](../Page/方程.md "wikilink")、或高次多项式[方程组](../Page/方程组.md "wikilink")，主要利用[机械化的](../Page/机械.md "wikilink")[算具和](../Page/算具.md "wikilink")[算法求解](../Page/算法.md "wikilink")，或進行刻板的、有系統的逐次[消元](../Page/方程组#消元法.md "wikilink")[过程](../Page/过程.md "wikilink")，為求将多元线性方程组、或多元高次方程组转化为单[变数式或单变数多项式](../Page/变数.md "wikilink")。由於中国传统数学以算为主，故稱為[算學](../Page/算學.md "wikilink")。[算筹](../Page/算筹.md "wikilink")、[算盘就是中国古代的](../Page/算盘.md "wikilink")“[计算机](../Page/计算机.md "wikilink")”，又稱為算具。[算经中的术文和](../Page/算经.md "wikilink")[珠算](../Page/珠算.md "wikilink")[口诀就是计算程序](../Page/口诀.md "wikilink")，又稱為算法。[中国数学史又称为中算史](../Page/中国数学史.md "wikilink")，並影響到[漢字文化圈其他地區的傳統數學](../Page/漢字文化圈.md "wikilink")，如[日本的](../Page/日本.md "wikilink")[和算](../Page/和算.md "wikilink")，[朝鮮半島的](../Page/朝鮮半島.md "wikilink")[韓算](../Page/韓算.md "wikilink")，以及[越南](../Page/越南.md "wikilink")、[琉球的算學](../Page/琉球.md "wikilink")。

从[秦](../Page/秦.md "wikilink")[汉以来](../Page/汉.md "wikilink")，直到[宋](../Page/宋.md "wikilink")[元](../Page/元.md "wikilink")，中国算学一直[领先](../Page/领先.md "wikilink")[世界](../Page/世界.md "wikilink")。而[代数学基本是中国的创造](../Page/代数学.md "wikilink")。\[1\]

## 歷史

中国算学起源于[仰韶文化](../Page/仰韶文化.md "wikilink")，距今有五千余年历史，在周公时代，数乃是[六艺之一](../Page/六艺.md "wikilink")。在[春秋时代](../Page/春秋时代.md "wikilink")[十进位制的](../Page/十进位制.md "wikilink")[筹算已经普及](../Page/筹算.md "wikilink")。著名日本数学史家[三上义夫指出](../Page/三上义夫.md "wikilink")，“中国之算学史，其有长期之发展，不能不谓之为世界中稀有之例也”\[2\]中国算学的发展有二三千年之久，如此长久的发展历史為世界罕見。

### 上古至西汉

[Shang_numerals.jpg](https://zh.wikipedia.org/wiki/File:Shang_numerals.jpg "fig:Shang_numerals.jpg")

中国[考古学家在](../Page/考古学家.md "wikilink")[陕西发现的几十万年前](../Page/陕西.md "wikilink")[蓝田猿人遗留的不规则的石球](../Page/蓝田猿人.md "wikilink")。几万年前[山西原始人制作的石球形状规则](../Page/山西.md "wikilink")。
到了[新石器时代](../Page/新石器时代.md "wikilink")，出现空心陶球。七千年前[河姆渡人遗址中发现圆筒](../Page/河姆渡人.md "wikilink")，圆珠等形状。新石器时代[陶器上出现有规则的图案](../Page/陶器.md "wikilink")。半坡出土文物中有双耳陶器，三足陶器，有的陶器上刻有四叶纹，说明上古时代已有1，2，3，4等数字概念。

[十进位制起源于古中国](../Page/十进位制.md "wikilink")，最遲在公元前1400年的[商代就已經出現](../Page/商代.md "wikilink")。从一万多年前[山顶洞人遗址中出土的骨管](../Page/山顶洞人.md "wikilink")，上刻有可能表示[十进位制的圆形](../Page/十进位制.md "wikilink")、长形刻符\[3\]。1974年－1978年中国考古学家从[青海](../Page/青海.md "wikilink")[乐都县出土数万件](../Page/乐都县.md "wikilink")[新石器时代的遗物](../Page/新石器时代.md "wikilink")，其中有些骨片上有不同数目的刻纹，表示1到8之数，未发现有10道以上刻纹，与存在十进位制相符。

[筹算至少在](../Page/筹算.md "wikilink")[战国初年筹算已然出现](../Page/战国.md "wikilink")。它使用[商代发明的](../Page/商代.md "wikilink")[十进位制计数](../Page/十进位制.md "wikilink")，利用[九九表可以很方便地进行](../Page/九九表.md "wikilink")[四则运算以及](../Page/四则运算.md "wikilink")[乘方](../Page/乘方.md "wikilink")、[开方等较复杂运算](../Page/开方.md "wikilink")，并可以对[零](../Page/零.md "wikilink")、[负数和](../Page/负数.md "wikilink")[分数作出表示与](../Page/分数.md "wikilink")[计算](../Page/计算.md "wikilink")。

春秋战国时代已经形成数学的九个分支-[九数](../Page/九数.md "wikilink")，包括方田（田地测算）、粟米（粮食换算比率）、差分（[赋税的分配](../Page/赋税.md "wikilink")）、少广（田亩[面积和长阔](../Page/面积.md "wikilink")）、商功（[工程土方估计](../Page/工程.md "wikilink")）、均输（[运输费用的分配](../Page/运输.md "wikilink")）、方程（[方程式](../Page/方程式.md "wikilink")）、盈不足、旁要（[勾股问题](../Page/勾股.md "wikilink")）。[郑玄引](../Page/郑玄.md "wikilink")《周礼注》：“九数：、方田、粟米、
差分、 少广、商功、 军输、 方程、 盈不足、 旁要。”\[4\]。

[西漢初期出現了一本數學教科書](../Page/西漢.md "wikilink")《[算数书](../Page/算数书.md "wikilink")》，其一些内容明显和《[九章算术](../Page/九章算术.md "wikilink")》平行。有学者认为算数书可能是九章算术的母本\[5\]。《九章算术》成书于大约1世纪，但也可能早在200BC就已存在。多数学者相信直到九章算术定形时中国的数学和古代地中海世界的数学多少是独立的发展的。當中的开平方、开立方、算术应用、正负数、连立一次方程、二次方程等都领先世界几个世纪\[6\]西汉的[张苍](../Page/张苍.md "wikilink")、[耿寿昌增补和整理](../Page/耿寿昌.md "wikilink")《九章算术》，寫成定本，详细说明开平方、开立方、和求解线性方程组的算法。[张衡发明](../Page/张衡.md "wikilink")\(\sqrt{10}\)、\(\frac{92}{29}\)、\(\frac{730}{232}\)作為[圓周率的值](../Page/圓周率.md "wikilink")\[7\]。

## 魏晉南北朝

此一時期（220年-581年），中國數學在四方面有長足進展，分別為直角三角形三邊關係的確認、[測量學](../Page/測量學.md "wikilink")、平面面積和立體體積的計算，以及推算圓周率，由[趙爽](../Page/趙爽.md "wikilink")、[劉徽](../Page/劉徽.md "wikilink")、[祖沖之與](../Page/祖沖之.md "wikilink")[祖暅父子](../Page/祖暅.md "wikilink")4人個別或相繼完成。趙爽是魏晉時人，著有《周髀算經注》，其中「勾股圓方圖注」附有圖示，列出有關直角三角形三邊關係的命題21條，分屬「[勾股](../Page/勾股.md "wikilink")」定理、「[弦圖](../Page/弦圖.md "wikilink")」定理、「[勾實之矩](../Page/勾實之矩.md "wikilink")」定理與「[股實之矩](../Page/股實之矩.md "wikilink")」定理。當中唯有「勾股」定理已見於《[周髀算經](../Page/周髀算經.md "wikilink")》。

稍後的[劉徽亦魏晉時人](../Page/劉徽.md "wikilink")，著有《九章算術注》，為《[九章算術](../Page/九章算術.md "wikilink")》各種算法提出簡括証明。他並在注文中提出[割圓術](../Page/割圆术_\(刘徽\).md "wikilink")，以内接正六邊形开始，逐次倍加邊數的方法，逐步逼近圓周率。前三世紀，希臘數學家[阿基米德已用正多邊形逐漸增加邊數的方法求圓周率](../Page/阿基米德.md "wikilink")，但他兼用內接和外切兩種計算，得到出的估計值\[{223 \over 71}  <\Pi <  {22 \over 7}\]；也就是
\(3.140845 < \pi < 3.142857\)\[8\]。劉徽的[割圓術相比更為簡便](../Page/割圓術.md "wikilink")，刘徽所得的π=3.1416也优于阿基米德\[9\]。《九章算術注》亦提出[重差術](../Page/海岛算经#重差理论的历史.md "wikilink")，應用中国传统的[出入相补原理](../Page/出入相补.md "wikilink")，來[測量山高水深等數值](../Page/測量.md "wikilink")。他又指出《九章算術》計算球體體積方法錯誤，但亦未能提出更準確方法。這個疑問須留待[祖沖之解決](../Page/祖沖之.md "wikilink")。

[祖沖之與](../Page/祖沖之.md "wikilink")[祖暅父子使中國數學發展創一高峰](../Page/祖暅.md "wikilink")。祖沖之著有《綴術》、《九章術義注》及《重差注》（一說《綴術》乃祖暅所作），惜俱失佚。數學上祖沖之的最大貢獻有推算圓周率及計算球體體積（一說後者乃祖暅之法）。他繼承[劉徽的](../Page/劉徽.md "wikilink")[割圓術](../Page/割圓術.md "wikilink")，計算圓周率準確至小數點後7位數(3.1415926\<π\<3.1415927)，這個記錄保持了900多年，至15世紀方為阿拉伯數學家[卡西打破](../Page/卡西.md "wikilink")。祖沖之（或祖暅）並以直截面積相比的方法，解決球體體積問題（在西方，球體體積問題前三世紀[阿基米德已解決](../Page/阿基米德.md "wikilink")），其法今存於唐代[李淳風的](../Page/李淳風.md "wikilink")《九章算術注》中。祖沖之計算方法巧妙，應用現今所謂「[卡瓦列里定理](../Page/卡瓦列里定理.md "wikilink")」：「等高處的截面面積相等，則二立體的體積相等。」此定理今人公認是意大利數學家[卡瓦列里](../Page/卡瓦列里.md "wikilink")（Gavalieri）所創，因而命名，其實早已為祖沖之所應用。

曆法方面，[祖沖之編定](../Page/祖沖之.md "wikilink")「[大明曆](../Page/大明曆.md "wikilink")」，在身故後10年為梁朝所采用，取代[何承天](../Page/何承天.md "wikilink")（370年-447年）欠準確的舊曆。

## 隋唐

唐代，有關[重差術的注文被抽出單行](../Page/海岛算经#重差理论的历史.md "wikilink")，題為《[海島算經](../Page/海島算經.md "wikilink")》，成为《[算经十书](../Page/算经十书.md "wikilink")》之一。[刘徽创造的四次重差观测术](../Page/刘徽.md "wikilink")，“使中国[测量学达到登峰造极的地步](../Page/测量学.md "wikilink")”\[10\]，使“中国在數學测量学的成就，超越西方约一千年”（美国数学家弗兰克·斯委特兹语）\[11\]

## 宋朝

[秦九韶的](../Page/秦九韶.md "wikilink")
《[数书九章](../Page/数书九章.md "wikilink")》发展了一元高次方程求数值解的程序化、机械化算法。

## 金元

元代[朱世杰的](../Page/朱世杰.md "wikilink")
《[四元玉鉴](../Page/四元玉鉴.md "wikilink")》发展了多至四元的多项式方程组的消元和求解的算法。

## 明清

明代[朱載堉发明](../Page/朱載堉.md "wikilink")[十二平均律时](../Page/十二平均律.md "wikilink")，使用80档大算盘，计算开平方，开立方到小数点后25位。

  - [算学宝鉴](../Page/算学宝鉴.md "wikilink")

## 著名古代中国数学家

[南北朝时的](../Page/南北朝.md "wikilink")[祖冲之是第一个準確计算](../Page/祖冲之.md "wikilink")[圓周率值到小数点后](../Page/圓周率.md "wikilink")6位的人。

[南宋末年时的](../Page/南宋.md "wikilink")[秦九韶推导出](../Page/秦九韶.md "wikilink")[中国剩余定理](../Page/中国剩余定理.md "wikilink")。

## 特點

### 實用性

中国算学具有强烈的实用数学特点，和抽象化的希腊数学形成鲜明的对照。从《[周髀算經](../Page/周髀算經.md "wikilink")》《[九章算术](../Page/九章算术.md "wikilink")》、
《[海岛算经](../Page/海岛算经.md "wikilink")》
到《[数书九章](../Page/数书九章.md "wikilink")》等[算經十書其取材都和](../Page/算經十書.md "wikilink")[天文](../Page/天文.md "wikilink")、[曆算](../Page/曆算.md "wikilink")、[农业](../Page/农业.md "wikilink")、[测量](../Page/测量.md "wikilink")、[工程等实用问题](../Page/工程.md "wikilink")。中算的“学以致用”和希腊的几何学迥然不同。\[12\]。

### 機械化

中国算学有别于希腊数学的特点，是机械化、算法化、和构造性，与希腊数学重逻辑推理相对照。中國算學使用[算筹](../Page/算筹.md "wikilink")、[算盘為工具](../Page/算盘.md "wikilink")，以算经中的术文和珠算口诀作為计算程序。中国古代算学家擅长计算，[祖冲之精确计算](../Page/祖冲之.md "wikilink")[圆周率](../Page/圆周率.md "wikilink")，领先世界近一千年就是一个很好的例子。明代[朱載堉发明](../Page/朱載堉.md "wikilink")[十二平均律时](../Page/十二平均律.md "wikilink")，使用80档大算盘，计算开平方，开立方到小数点后25位，又是一例。

### 代數化

中国算学基本特征在于将实用问题（包括[几何学问题](../Page/几何学.md "wikilink")）[代数化](../Page/代数.md "wikilink")，转化为[线性方程组](../Page/线性方程组.md "wikilink")、[高次多项式方程](../Page/高次多项式方程.md "wikilink")、或[高次多项式方程组](../Page/高次多项式方程组.md "wikilink")。然后运用算具通过刻板的、机械的逐次消元程序求将多元线性方程组、或多元高次方程组转化为单变数式或单变数多项式。再经过机械化的算法和算具求解。这和西方数学讲究“存在性”，“完备性”而不重实用成鲜明的对照。经西汉的[张苍](../Page/张苍.md "wikilink")、[耿寿昌增补和整理成定本的](../Page/耿寿昌.md "wikilink")《[九章算术](../Page/九章算术.md "wikilink")》已经详细说明开平方、开立方、和求解线性方程组的算法。宋代[秦九韶的](../Page/秦九韶.md "wikilink")
《[数书九章](../Page/数书九章.md "wikilink")》发展了一元高次方程求数值解的程序化、机械化算法。[元代](../Page/元代.md "wikilink")[朱世杰的](../Page/朱世杰.md "wikilink")
《[四元玉鉴](../Page/四元玉鉴.md "wikilink") 》更进一步发展了多至四元的多项式方程组的消元和求解的算法。\[13\]

## 注釋

## 參考文獻

  - [吴文俊主编](../Page/吴文俊.md "wikilink")
    《[中国数学史大系](../Page/中国数学史大系_\(吴文俊主编\).md "wikilink")》
  - 吴文俊 《数学机械化》 前言 《科学出版社》 ISBN 7-03-010765-0
  - 郭书春 主编 李兆华副主编 《中国科学技术史》 《数学卷》 科学出版社 2010 ISBN 978-7-03-029055-3
  - [李约瑟原著](../Page/李约瑟.md "wikilink")， 柯林·罗南改编：《中华科学文明史》 第二卷第一章。The
    Shorter Science & Civilisation in China 2, p5, Cambridge University
    Press, ISBN 0-521-23582-0
  - 《李俨.钱宝琮科学史全集》卷1-10 页辽宁教育出版社. 1998
  - 王渝生 刘纯主编 《中国数学史大系》卷一至卷十一 河北科学技术出版社
  - 邹大海著《中国数学的兴起和先秦数学 》《中国数学史大系》河北科学技术出版社
  - 孔国平著《李冶朱世杰与金元数学》》《中国数学史大系》河北科学技术出版社
  - 劳汉生著《珠算与实用算术》》《中国数学史大系》河北科学技术出版社
  - 张奠宙著 《中国近代数学的发展》》《中国数学史大系》河北科学技术出版社
  - [三上义夫](../Page/三上义夫.md "wikilink") 《中国算学之特色》 绪论 1933年
    商务印书馆《万有文库》\#0400
  - 三上义夫 The Development of Mathematics in China and Japan 1913 Leipzig
  - 沈康身编 《算数书解说》，吴文俊主编 《中国数学史大系》副卷第一卷 北京师范大学出版社 2004 ISBN
    7-303-05292-5/O
  - 杜石然：《數學．歷史．社會》（瀋陽：遼寧教育出版社，2003）。

## 参看

  - [筹算](../Page/筹算.md "wikilink")
  - [算盘](../Page/算盘.md "wikilink")
  - [苏州码子](../Page/苏州码子.md "wikilink")
  - [中文數字](../Page/中文數字.md "wikilink")
  - [和算](../Page/和算.md "wikilink")

## 外部链接

  - [古算書原典](http://ctext.org/mathematics/zh)
  - [中國古代數學史](http://kss.hkcampus.net/~kss-wsf/)

[中國算學](../Category/中國算學.md "wikilink")

1.  吴文俊文集 3-5页
2.  三上义夫 绪论
3.  吴文俊 第一卷第二编 《中国数学的萌芽》
4.  郭书春 第三章 39-44页
5.  沈康身编 《算数书解说》 18页
6.  吴文俊 《吴文俊文集·中国数学对世界文化的伟大贡献》第4页
7.  吴文俊主编 《中国数学史大系》 第三卷 第一编 第二节 张衡的数学研究 第5页
8.  阿基米德原著 《量圆》 《中国数学史大系》 副卷第一 第二章 第三编 希腊 197-203页
9.  吴文俊主编《中国数学史大系》 副卷第一卷 第二章 第三编 希腊:阿基米德著 《量圆》 203页
10. 引自[吴文俊主编](../Page/吴文俊.md "wikilink")
    《[中国数学史大系](../Page/中国数学史大系_\(吴文俊主编\).md "wikilink")》第三卷
    248页 ISBN 7-303-04557-0/O
11. "Quite Simply, in the endeavors of mathematical surveying, China's
    accomplishments exceeded those realized in the West by about one
    thousand years", 見 弗兰克·斯委特兹:
    《海岛算经:古代中国的[测量学和数学](../Page/测量学.md "wikilink")》第四章第二节
    比较回顾： 中国测量学的成就。（Frank J. Swetz: The Sea Island Mathematical
    Manual,Surveying and Mathematics in Ancient China 4.2 Chinese
    Surveying Accomplishments, A Comparative Retrospection 第63页 The
    Pennsylvania State University Press, 1992 ISBN 0-271-00799-0 ）
12. 吴文俊 第一卷 第一编 第五章 第二节
13. 吴文俊 《数学机械化》 前言