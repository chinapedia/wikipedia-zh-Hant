本文詳列在**[中華人民共和國](../Page/中華人民共和國.md "wikilink")**通過**自選自聖**程序任命的**“[中国天主教](../Page/中国天主教.md "wikilink")”[主教](../Page/主教.md "wikilink")**。

## 概要

天主教主教的任命，原是[聖座由各地的](../Page/聖座.md "wikilink")[神父中決定人選後](../Page/神父.md "wikilink")，再經由[祝聖儀式就職](../Page/祝聖.md "wikilink")；但部分中華人民共和國的[神職人員與教會組織](../Page/神職人員.md "wikilink")（如[愛國會](../Page/中国天主教爱国会.md "wikilink")）自行選出與祝聖主教，因而被稱為「自選自聖」。這些主教由於在[祝聖時未得到聖座的確認](../Page/祝聖.md "wikilink")，所以其主教身份原來在[中國内地](../Page/中國内地.md "wikilink")（不含[香港與](../Page/香港.md "wikilink")[澳門](../Page/澳門.md "wikilink")）以外的地區普遍不被承認，而且大多數都被[教宗](../Page/教宗.md "wikilink")[逐出教會](../Page/绝罚.md "wikilink")。但現時雙方採取了折衷的做法，教廷會承認自選自聖的主教作為當地**教友領袖**的地位。部分自選自聖主教在就任後也通過各種途徑獲得了聖座的承認。

## 自選自聖主教列表

<table style="width:98%;">
<colgroup>
<col style="width: 9%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出生</p></th>
<th><p>逝世</p></th>
<th><p>出生地</p></th>
<th><p>晉牧</p></th>
<th><p>牧職</p></th>
<th><p>其他职务</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/董光清.md" title="wikilink">董光清</a>*</p></td>
<td><p>1917年4月1日</p></td>
<td><p>2007年5月12日</p></td>
<td><p><a href="../Page/湖北省.md" title="wikilink">湖北省</a><a href="../Page/孝感县.md" title="wikilink">孝感县</a></p></td>
<td><p>1958年4月13日</p></td>
<td><p><a href="../Page/汉口教區.md" title="wikilink">汉口教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/袁文华.md" title="wikilink">袁文华</a></p></td>
<td><p>1905年</p></td>
<td><p>1973年1月</p></td>
<td><p><a href="../Page/山东省.md" title="wikilink">山东省</a><a href="../Page/济南市.md" title="wikilink">济南市</a></p></td>
<td><p>1958年4月13日</p></td>
<td><p><a href="../Page/武昌教區.md" title="wikilink">武昌教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王守谦.md" title="wikilink">王守谦</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1958年4月</p></td>
<td><p><a href="../Page/永年教區.md" title="wikilink">永年教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/常守彝.md" title="wikilink">常守彝</a></p></td>
<td><p>1912年</p></td>
<td><p>1987年</p></td>
<td><p><a href="../Page/河北省.md" title="wikilink">河北省</a><a href="../Page/蔚县.md" title="wikilink">蔚县</a></p></td>
<td><p>1958年4月</p></td>
<td><p><a href="../Page/宣化教區.md" title="wikilink">宣化教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/潘少卿.md" title="wikilink">潘少卿</a></p></td>
<td></td>
<td><p>1983年</p></td>
<td><p>？</p></td>
<td><p>1958年4月</p></td>
<td><p><a href="../Page/西湾子教區.md" title="wikilink">西湾子教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蓝柏露.md" title="wikilink">蓝柏露</a></p></td>
<td></td>
<td><p>1976年</p></td>
<td><p>？</p></td>
<td><p>1958年4月</p></td>
<td><p><a href="../Page/永平教區.md" title="wikilink">永平教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/赵牖民.md" title="wikilink">赵牖民</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/辽宁省.md" title="wikilink">辽宁省</a><a href="../Page/朝阳县.md" title="wikilink">朝阳县</a></p></td>
<td><p>1958年4月</p></td>
<td><p><a href="../Page/锦州教區.md" title="wikilink">锦州教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陈原才.md" title="wikilink">陈原才</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1958年4月</p></td>
<td><p><a href="../Page/贵阳教區.md" title="wikilink">贵阳教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李熙亭.md" title="wikilink">李熙亭</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1958年4月</p></td>
<td><p><a href="../Page/成都教區.md" title="wikilink">成都教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李德培.md" title="wikilink">李德培</a></p></td>
<td></td>
<td><p>1992年</p></td>
<td><p>？</p></td>
<td><p>1958年4月</p></td>
<td><p><a href="../Page/天津教區.md" title="wikilink">天津教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/周益斋.md" title="wikilink">周益斋</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1958年4月</p></td>
<td><p><a href="../Page/蚌埠教區.md" title="wikilink">蚌埠教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王其威.md" title="wikilink">王其威</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1958年4月</p></td>
<td><p><a href="../Page/保定教區.md" title="wikilink">保定教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/董文隆.md" title="wikilink">董文隆</a></p></td>
<td><p>1902年</p></td>
<td><p>1978年</p></td>
<td><p><a href="../Page/山东省.md" title="wikilink">山东省</a><a href="../Page/莱芜市.md" title="wikilink">莱芜市</a></p></td>
<td><p>1958年6月1日</p></td>
<td><p><a href="../Page/济南教區.md" title="wikilink">济南教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宗怀德.md" title="wikilink">宗怀德</a></p></td>
<td><p>1917年</p></td>
<td><p>1997年6月27日</p></td>
<td><p><a href="../Page/山东省.md" title="wikilink">山东省</a><a href="../Page/桓台县.md" title="wikilink">桓台县</a></p></td>
<td><p>1958年6月1日</p></td>
<td><p><a href="../Page/周村教區.md" title="wikilink">周村教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/贾福善.md" title="wikilink">贾福善</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1958年6月1日</p></td>
<td><p><a href="../Page/益都教區.md" title="wikilink">益都教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李明月.md" title="wikilink">李明月</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1958年6月1日</p></td>
<td><p><a href="../Page/曹州教區.md" title="wikilink">曹州教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/熊德琏.md" title="wikilink">熊德琏</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1958年10月26日</p></td>
<td><p><a href="../Page/长沙教區.md" title="wikilink">长沙教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李震林.md" title="wikilink">李震林</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1958年10月26日</p></td>
<td><p><a href="../Page/澧县教區.md" title="wikilink">澧县教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/杨高坚.md" title="wikilink">杨高坚</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1958年10月26日</p></td>
<td><p><a href="../Page/常德教區.md" title="wikilink">常德教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭则谦.md" title="wikilink">郭则谦</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1958年10月26日</p></td>
<td><p><a href="../Page/衡阳教區.md" title="wikilink">衡阳教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李树仁.md" title="wikilink">李树仁</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1958年10月26日</p></td>
<td><p><a href="../Page/岳阳教區.md" title="wikilink">岳阳教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/胡钦明.md" title="wikilink">胡钦明</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1958年10月4日</p></td>
<td><p><a href="../Page/南昌教區.md" title="wikilink">南昌教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陈独清.md" title="wikilink">陈独清</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1958年10月4日</p></td>
<td><p><a href="../Page/赣州教區.md" title="wikilink">赣州教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黄曙.md" title="wikilink">黄曙</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1958年10月4日</p></td>
<td><p><a href="../Page/余江教區.md" title="wikilink">余江教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李圣学.md" title="wikilink">李圣学</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1959年</p></td>
<td><p><a href="../Page/汉中教區.md" title="wikilink">汉中教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王维民.md" title="wikilink">王维民</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1959年</p></td>
<td><p><a href="../Page/吉林教區.md" title="wikilink">吉林教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王瑞寰.md" title="wikilink">王瑞寰</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1959年</p></td>
<td><p><a href="../Page/哈尔滨教區.md" title="wikilink">哈尔滨教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/姚光裕.md" title="wikilink">姚光裕</a></p></td>
<td></td>
<td><p>1963年</p></td>
<td><p>？</p></td>
<td><p>1959年</p></td>
<td><p><a href="../Page/北京教區.md" title="wikilink">北京教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/涂世华.md" title="wikilink">涂世华</a></p></td>
<td><p>1919年11月22日</p></td>
<td><p>2017年1月4日</p></td>
<td><p><a href="../Page/湖北省.md" title="wikilink">湖北省沔阳</a></p></td>
<td><p>1959年</p></td>
<td><p><a href="../Page/汉阳教區.md" title="wikilink">汉阳教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/张鸣谦.md" title="wikilink">张鸣谦</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1959年</p></td>
<td><p><a href="../Page/宜昌教區.md" title="wikilink">宜昌教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李维光.md" title="wikilink">李维光</a></p></td>
<td><p>1898年</p></td>
<td><p>1964年</p></td>
<td><p><a href="../Page/江苏省.md" title="wikilink">江苏省</a><a href="../Page/无锡县.md" title="wikilink">无锡县</a></p></td>
<td><p>1959年</p></td>
<td><p><a href="../Page/南京教區.md" title="wikilink">南京教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郁成才.md" title="wikilink">郁成才</a></p></td>
<td><p>1917年8月27日</p></td>
<td><p>2006年3月18日</p></td>
<td><p><a href="../Page/上海市.md" title="wikilink">上海市</a><a href="../Page/崇明县.md" title="wikilink">崇明县</a></p></td>
<td><p>1959年</p></td>
<td><p><a href="../Page/海门教區.md" title="wikilink">海门教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/钱余荣.md" title="wikilink">钱余荣</a>*</p></td>
<td><p>1914年</p></td>
<td><p>2013年3月22日</p></td>
<td><p><a href="../Page/江苏省.md" title="wikilink">江苏省</a><a href="../Page/铜山县_(清朝).md" title="wikilink">铜山县</a></p></td>
<td><p>1959年</p></td>
<td><p><a href="../Page/徐州教區.md" title="wikilink">徐州教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沈初鸣.md" title="wikilink">沈初鸣</a></p></td>
<td><p>1889年</p></td>
<td><p>1974年</p></td>
<td><p><a href="../Page/上海市.md" title="wikilink">上海市</a></p></td>
<td><p>1959年</p></td>
<td><p><a href="../Page/苏州教區.md" title="wikilink">苏州教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王矩光.md" title="wikilink">王矩光</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1959年</p></td>
<td><p><a href="../Page/宜宾教區.md" title="wikilink">宜宾教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/张家树.md" title="wikilink">张家树</a></p></td>
<td><p>1893年</p></td>
<td><p>1988年2月</p></td>
<td><p><a href="../Page/上海市.md" title="wikilink">上海市</a><a href="../Page/南汇县.md" title="wikilink">南汇县</a></p></td>
<td><p>1958年4月13日</p></td>
<td><p><a href="../Page/上海教區.md" title="wikilink">上海教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吴国焕.md" title="wikilink">吴国焕</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1960年4月27日</p></td>
<td><p><a href="../Page/杭州教區.md" title="wikilink">杭州教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/舒其谁.md" title="wikilink">舒其谁</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1960年4月27日</p></td>
<td><p><a href="../Page/宁波教區.md" title="wikilink">宁波教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/方志刚.md" title="wikilink">方志刚</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1960年4月27日</p></td>
<td><p><a href="../Page/温州教區.md" title="wikilink">温州教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/张曰津.md" title="wikilink">张曰津</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1960年4月</p></td>
<td><p><a href="../Page/烟台教區.md" title="wikilink">烟台教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/石麟阁.md" title="wikilink">石麟阁</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1961年5月</p></td>
<td><p><a href="../Page/兖州教區.md" title="wikilink">兖州教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/何春明.md" title="wikilink">何春明</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1962年1月24日</p></td>
<td><p><a href="../Page/开封教區.md" title="wikilink">开封教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/刘安祉.md" title="wikilink">刘安祉</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1962年1月24日</p></td>
<td><p><a href="../Page/正定教區.md" title="wikilink">正定教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/孔令忠.md" title="wikilink">孔令忠</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1962年1月24日</p></td>
<td><p><a href="../Page/昆明教區.md" title="wikilink">昆明教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夏学谦.md" title="wikilink">夏学谦</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1962年1月24日</p></td>
<td><p><a href="../Page/韶州教區.md" title="wikilink">韶州教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李德化.md" title="wikilink">李德化</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1962年1月24日</p></td>
<td><p><a href="../Page/太原教區.md" title="wikilink">太原教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/高庸.md" title="wikilink">高庸</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1962年1月24日</p></td>
<td><p><a href="../Page/汾阳教區.md" title="wikilink">汾阳教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/林泉.md" title="wikilink">林泉</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1962年1月24日</p></td>
<td><p><a href="../Page/福州教區.md" title="wikilink">福州教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/叶荫云.md" title="wikilink">叶荫云</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1962年</p></td>
<td><p><a href="../Page/惠阳教區.md" title="wikilink">惠阳教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郗民援.md" title="wikilink">郗民援</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1962年8月5日</p></td>
<td><p><a href="../Page/洛阳教區.md" title="wikilink">洛阳教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/石明良.md" title="wikilink">石明良</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1963年3月31日</p></td>
<td><p><a href="../Page/重庆教區.md" title="wikilink">重庆教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/范导江.md" title="wikilink">范导江</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1963年4月7日</p></td>
<td><p><a href="../Page/南充教區.md" title="wikilink">南充教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/傅鐵山.md" title="wikilink">傅鐵山</a></p></td>
<td><p>1931年11月3日</p></td>
<td><p>2007年4月20日</p></td>
<td><p><a href="../Page/河北省.md" title="wikilink">河北省</a><a href="../Page/清苑县.md" title="wikilink">清苑县</a></p></td>
<td><p>1979年</p></td>
<td><p><a href="../Page/北京教区.md" title="wikilink">北京教区主教</a></p></td>
<td><p><a href="../Page/第十届全国人大常委会.md" title="wikilink">第十届全国人大常委会</a><a href="../Page/全国人民代表大会常务委员会副委员长.md" title="wikilink">副委员长</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/刘景和.md" title="wikilink">刘景和</a>*</p></td>
<td><p>1919年</p></td>
<td></td>
<td><p><a href="../Page/河北省.md" title="wikilink">河北省</a><a href="../Page/唐山市.md" title="wikilink">唐山市</a></p></td>
<td><p>1981年</p></td>
<td><p><a href="../Page/永平教區.md" title="wikilink">永平教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/范文兴.md" title="wikilink">范文兴</a>*</p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1981年</p></td>
<td><p><a href="../Page/景县教區.md" title="wikilink">景县教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/钱惠民.md" title="wikilink">钱惠民</a></p></td>
<td><p>1911年</p></td>
<td><p>1993年</p></td>
<td><p><a href="../Page/江苏省.md" title="wikilink">江苏省</a><a href="../Page/张家港市.md" title="wikilink">张家港市</a></p></td>
<td><p>1981年</p></td>
<td><p><a href="../Page/南京教區.md" title="wikilink">南京教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/马龙麟.md" title="wikilink">马龙麟</a></p></td>
<td><p>1918年</p></td>
<td><p>1999年6月9日</p></td>
<td><p><a href="../Page/上海市.md" title="wikilink">上海市</a></p></td>
<td><p>1981年7月24日</p></td>
<td><p><a href="../Page/苏州教区.md" title="wikilink">苏州教区主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/刘定汉.md" title="wikilink">刘定汉</a>*</p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1982年10月6日</p></td>
<td><p><a href="../Page/献县教區.md" title="wikilink">献县教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李思德.md" title="wikilink">李思德</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1984年12月7日</p></td>
<td><p><a href="../Page/上海教區.md" title="wikilink">上海教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金魯賢.md" title="wikilink">金魯賢</a>*</p></td>
<td><p>1916年6月20日</p></td>
<td><p>2013年4月27日</p></td>
<td><p><a href="../Page/上海市.md" title="wikilink">上海市</a><a href="../Page/南市区_(上海市).md" title="wikilink">南市区</a></p></td>
<td><p>1985年1月27日</p></td>
<td><p><a href="../Page/上海教區.md" title="wikilink">上海教區主教</a></p></td>
<td><p>第七届全国政协常务委员</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/朱峰青.md" title="wikilink">朱峰青</a></p></td>
<td></td>
<td></td>
<td><p>？</p></td>
<td><p>1988年11月27日</p></td>
<td><p><a href="../Page/杭州教區.md" title="wikilink">杭州教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/刘元仁.md" title="wikilink">刘元仁</a></p></td>
<td><p>1923年</p></td>
<td><p>2005年4月20日</p></td>
<td><p><a href="../Page/江苏省.md" title="wikilink">江苏省</a><a href="../Page/启东市.md" title="wikilink">启东市</a></p></td>
<td><p>1993年12月</p></td>
<td><p><a href="../Page/南京教區.md" title="wikilink">南京教區主教</a></p></td>
<td><p><a href="../Page/中国天主教主教团.md" title="wikilink">中国天主教主教团主席</a><br />
<a href="../Page/中国天主教爱国会.md" title="wikilink">中国天主教爱国会副主席</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蒋陶然.md" title="wikilink">蒋陶然</a>*</p></td>
<td><p>1926年</p></td>
<td><p>2010年11月15日</p></td>
<td><p><a href="../Page/河北省.md" title="wikilink">河北省</a><a href="../Page/清苑县.md" title="wikilink">清苑县</a></p></td>
<td><p>1989年5月21日</p></td>
<td><p><a href="../Page/石家庄教区.md" title="wikilink">石家庄教区主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/潘德世.md" title="wikilink">潘德世</a></p></td>
<td><p>1924年10月</p></td>
<td><p>2005年12月3日</p></td>
<td><p><a href="../Page/河北省.md" title="wikilink">河北省</a><a href="../Page/定兴县.md" title="wikilink">定兴县</a></p></td>
<td><p>1991年</p></td>
<td><p><a href="../Page/保定教區.md" title="wikilink">保定教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/詹思禄.md" title="wikilink">詹思禄</a></p></td>
<td><p>1961年</p></td>
<td></td>
<td><p><a href="../Page/福建省.md" title="wikilink">福建省</a><a href="../Page/宁德市.md" title="wikilink">宁德市</a></p></td>
<td><p>2000年1月6日</p></td>
<td><p><a href="../Page/福宁教區.md" title="wikilink">福宁教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陆新平.md" title="wikilink">陆新平</a>*</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/江苏省.md" title="wikilink">江苏省</a><a href="../Page/海门市.md" title="wikilink">海门市</a></p></td>
<td><p>2000年1月6日</p></td>
<td><p><a href="../Page/南京教区.md" title="wikilink">南京教区主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/苏长山.md" title="wikilink">苏长山</a></p></td>
<td><p>1926年</p></td>
<td><p>2006年12月4日</p></td>
<td><p><a href="../Page/河北省.md" title="wikilink">河北省</a><a href="../Page/清苑县.md" title="wikilink">清苑县</a></p></td>
<td><p>2000年1月6日</p></td>
<td><p><a href="../Page/保定教區.md" title="wikilink">保定教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/方建平.md" title="wikilink">方建平</a>*</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/河北省.md" title="wikilink">河北省</a><a href="../Page/唐山市.md" title="wikilink">唐山市</a></p></td>
<td><p>2000年1月6日</p></td>
<td><p><a href="../Page/唐山教区.md" title="wikilink">唐山教区主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/靳道远.md" title="wikilink">靳道远</a>*</p></td>
<td></td>
<td></td>
<td></td>
<td><p>2000年1月6日</p></td>
<td><p><a href="../Page/长治教區.md" title="wikilink">长治教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/曹湘德.md" title="wikilink">曹湘德</a>*</p></td>
<td></td>
<td></td>
<td></td>
<td><p>2000年6月25日</p></td>
<td><p><a href="../Page/杭州教區.md" title="wikilink">杭州教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬英林.md" title="wikilink">馬英林</a></p></td>
<td><p>1966年</p></td>
<td><p> </p></td>
<td><p><a href="../Page/河北省.md" title="wikilink">河北省</a><a href="../Page/南宫市.md" title="wikilink">南宫市</a></p></td>
<td><p>2006年4月30日</p></td>
<td><p><a href="../Page/天主教昆明教区.md" title="wikilink">昆明教区主教</a></p></td>
<td><p><a href="../Page/中国天主教主教团.md" title="wikilink">中国天主教主教团主席</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/刘新红.md" title="wikilink">刘新红</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/安徽省.md" title="wikilink">安徽省</a><a href="../Page/阜阳市.md" title="wikilink">阜阳市</a></p></td>
<td><p>2006年5月3日</p></td>
<td><p><a href="../Page/安徽教區.md" title="wikilink">安徽教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王仁雷.md" title="wikilink">王仁雷</a>*</p></td>
<td><p>1970年4月</p></td>
<td></td>
<td><p>？</p></td>
<td><p>2006年11月30日</p></td>
<td><p><a href="../Page/徐州教區.md" title="wikilink">徐州教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭金才.md" title="wikilink">郭金才</a></p></td>
<td><p>1968年2月</p></td>
<td></td>
<td><p><a href="../Page/河北省.md" title="wikilink">河北省</a><a href="../Page/承德市.md" title="wikilink">承德市</a></p></td>
<td><p>2010年11月20日</p></td>
<td><p><a href="../Page/承德教区.md" title="wikilink">承德教区主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/梁建森.md" title="wikilink">梁建森</a>*</p></td>
<td><p>1964年5月</p></td>
<td></td>
<td><p><a href="../Page/广东省.md" title="wikilink">广东省</a><a href="../Page/台山市.md" title="wikilink">台山市</a></p></td>
<td><p>2011年3月30日</p></td>
<td><p><a href="../Page/江门教区.md" title="wikilink">江门教区主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雷世银.md" title="wikilink">雷世银</a></p></td>
<td><p>1963年</p></td>
<td></td>
<td><p><a href="../Page/四川省.md" title="wikilink">四川省</a><a href="../Page/峨眉山市.md" title="wikilink">峨眉山市</a></p></td>
<td><p>2011年6月29日</p></td>
<td><p><a href="../Page/乐山教區.md" title="wikilink">乐山教區主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黄炳章.md" title="wikilink">黄炳章</a></p></td>
<td><p>1967年1月</p></td>
<td></td>
<td><p><a href="../Page/广东省.md" title="wikilink">广东省</a><a href="../Page/惠来县.md" title="wikilink">惠来县</a></p></td>
<td><p>2011年7月14日</p></td>
<td><p><a href="../Page/汕头教區.md" title="wikilink">汕头教區主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/罗雪刚.md" title="wikilink">罗雪刚</a>*</p></td>
<td><p>1964年2月</p></td>
<td></td>
<td><p><a href="../Page/四川省.md" title="wikilink">四川省</a><a href="../Page/眉山市.md" title="wikilink">眉山市</a></p></td>
<td><p>2011年11月30日</p></td>
<td><p><a href="../Page/宜宾教区.md" title="wikilink">宜宾教区主教</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陈功鳌.md" title="wikilink">陈功鳌</a>*</p></td>
<td><p>1964年9月</p></td>
<td></td>
<td><p><a href="../Page/四川省.md" title="wikilink">四川省</a><a href="../Page/广安市.md" title="wikilink">广安市</a></p></td>
<td><p>2012年4月19日</p></td>
<td><p><a href="../Page/南充教区.md" title="wikilink">南充教区主教</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/屈蔼林.md" title="wikilink">屈蔼林</a>*</p></td>
<td><p>1961年5月</p></td>
<td></td>
<td><p><a href="../Page/湖南省.md" title="wikilink">湖南省</a><a href="../Page/衡阳市.md" title="wikilink">衡阳市</a></p></td>
<td><p>2012年4月25日</p></td>
<td><p><a href="../Page/湖南教区.md" title="wikilink">湖南教区主教</a></p></td>
<td></td>
</tr>
</tbody>
</table>

  - 注1：晋铎指祝圣为司祭（神父，七品圣职）« Ordinatione presbyterorum » ，祝圣主教为晋牧«
    Consecratione electi in episcopum »。
  - 注2：打星號（\*）者為得到圣座確認的牧職。

## 参考文献

## 外部链接

## 参见

  - [華人天主教主教列表](../Page/華人天主教主教列表.md "wikilink")
      - [教廷認可之中國主教列表](../Page/教廷認可之中國主教列表.md "wikilink")
  - [中華人民共和國與梵蒂岡關係](../Page/中華人民共和國與梵蒂岡關係.md "wikilink")
      - [叙任权斗争](../Page/叙任权斗争.md "wikilink")
  - [中國天主教](../Page/中國天主教.md "wikilink")
      - [三自爱国教会](../Page/三自爱国教会.md "wikilink")
          - [中国天主教爱国会](../Page/中国天主教爱国会.md "wikilink")
          - [中国天主教主教团](../Page/中国天主教主教团.md "wikilink")
      - [中国天主教地下教会](../Page/中国天主教地下教会.md "wikilink")

{{-}}

[中华人民共和国自选自圣的天主教主教](../Category/中华人民共和国自选自圣的天主教主教.md "wikilink")
[Category:中国天主教主教列表](../Category/中国天主教主教列表.md "wikilink")
[Category:中华人民共和国人物列表](../Category/中华人民共和国人物列表.md "wikilink")