**同盟國中國戰區中國陸軍總司令部**設立\[1\]於1944年12月，組成成員為[中華民國](../Page/中華民國.md "wikilink")[國民革命軍](../Page/國民革命軍.md "wikilink")，其任務為協助[同盟國作戰](../Page/同盟國.md "wikilink")，並負責中國西南部各戰區與[緬甸北部的反攻與防守戰情](../Page/緬甸.md "wikilink")。另一方面，也配合同盟國各國部隊（如[史迪威部](../Page/史迪威.md "wikilink")）綜合運用。

駐守於[雲南](../Page/雲南.md "wikilink")[昆明的中國陸軍總司令部總司令為](../Page/昆明.md "wikilink")[國民政府軍事委員會參謀總長](../Page/國民政府軍事委員會.md "wikilink")[何應欽兼任](../Page/何應欽.md "wikilink")，轄有[遠征軍](../Page/中国远征军.md "wikilink")[衛立煌部](../Page/衛立煌.md "wikilink")、黔桂湘[湯恩伯部](../Page/湯恩伯.md "wikilink")、[第四戰區](../Page/抗日战争第四战区.md "wikilink")[張發奎部](../Page/張發奎.md "wikilink")、滇越邊境[盧漢部](../Page/盧漢.md "wikilink")、及[杜聿明的](../Page/杜聿明.md "wikilink")[第五集团军和](../Page/国民革命军第五集团军.md "wikilink")[李玉堂的](../Page/李玉堂.md "wikilink")[第二十七集团军](../Page/国民革命军第二十七集团军.md "wikilink")，共有二十八個軍、八十六個師，以一整編師八千人來核算，規模在六十五萬兵員以上。之後，雖縮編為[第一方面軍](../Page/中國陸軍第一方面軍.md "wikilink")（[盧漢為司令官](../Page/盧漢.md "wikilink")）、[第二方面軍](../Page/中國陸軍第二方面軍.md "wikilink")（[張發奎](../Page/張發奎.md "wikilink")）、[第三方面軍](../Page/中國陸軍第三方面軍.md "wikilink")（[湯恩伯](../Page/湯恩伯.md "wikilink")，1945年之後，搶灘[上海接受](../Page/上海.md "wikilink")[日軍投降](../Page/日軍.md "wikilink")）及[第四方面軍](../Page/中國陸軍第四方面軍.md "wikilink")（[王耀武](../Page/王耀武.md "wikilink")）等四個[方面軍](../Page/方面軍.md "wikilink")，不過配置[美軍配備的該司令部軍力仍為中國陸軍主力](../Page/美軍.md "wikilink")。这四个方面军在1945年8月後，同许多[抗日战争战区一样](../Page/抗日战争战区.md "wikilink")，成为[代表同盟国接受日軍受降的受降區的基本受降单位](../Page/第二次世界大战中国战区受降仪式.md "wikilink")。

## 戰鬥序列

### 1945年1月1日編成

  - 中國戰區陸軍總司令部：何應欽 （昆明）
      - 中國遠征軍
      - 黔桂湘邊區
      - [第四戰區暨第二十七集團軍](../Page/第四戰區.md "wikilink")
      - 滇越邊區
      - 第五集團軍，昆明防守司令

### 1945年4月1日改編

  - 中國戰區陸軍總司令部：何應欽 （昆明）
      - 總司令部直轄
          - [第54軍](../Page/國民革命軍第五十四軍.md "wikilink")（第8、36、198師）[闕漢騫](../Page/闕漢騫.md "wikilink")
          - [新6軍](../Page/國民革命軍新六軍.md "wikilink")（第14、新22、[青年軍207師](../Page/青年軍207師.md "wikilink")）[廖耀湘](../Page/廖耀湘.md "wikilink")
          - 砲兵第5、14、41、49團
          - 通信兵第6團
          - 憲兵第20團
      - 昆明防守司令部：[杜聿明](../Page/杜聿明.md "wikilink")
          - [第5軍](../Page/國民革命軍第五軍.md "wikilink")（第200、45、96師）[邱清泉](../Page/邱清泉.md "wikilink")
          - [第8軍](../Page/國民革命軍第八軍.md "wikilink")（第166、103、榮1師）[李彌](../Page/李彌.md "wikilink")
          - 昆明警備司令：暫9師
          - 第48師[鄭庭笈](../Page/鄭庭笈.md "wikilink")，裝甲兵第1團
      - 第一方面軍：[盧漢](../Page/盧漢.md "wikilink")（由滇越邊區第一、九集團軍改編）
          - [第60軍](../Page/國民革命軍第六十軍.md "wikilink")（第182、184師）[安恩溥](../Page/安恩溥.md "wikilink")
          - [第93軍](../Page/國民革命軍第九十三軍.md "wikilink")（暫18、20、22師）[張沖](../Page/張沖.md "wikilink")
          - [第52軍](../Page/國民革命軍第五十二軍.md "wikilink")（第2、25、195師）[趙公武](../Page/趙公武.md "wikilink")
          - 第93師
          - 山砲兵團
          - 砲兵團
      - 第二方面軍：[張發奎](../Page/張發奎.md "wikilink")（由[第四戰區改編](../Page/抗日戰爭第四戰區.md "wikilink")）駐桂西
          - [第46軍](../Page/國民革命軍第四十六軍.md "wikilink")（第188、175、新19師）[黎行恕](../Page/黎行恕.md "wikilink")～[韓鍊成](../Page/韓鍊成.md "wikilink")
          - [第64軍](../Page/國民革命軍第六十四軍.md "wikilink")（第131、156、159師）[張馳](../Page/張馳.md "wikilink")
          - [第62軍](../Page/國民革命軍第六十二軍.md "wikilink")（第151、95、157、158師）[黃濤](../Page/黃濤.md "wikilink")
          - 高雷守備區：[鄧鄂](../Page/鄧鄂.md "wikilink")
          - 憲兵第5團
      - 第三方面軍：[湯恩伯](../Page/湯恩伯.md "wikilink")（由黔桂湘邊區改編），1945年9月6日空運[上海](../Page/上海.md "wikilink")
          - 第二十七集團軍：[李玉堂](../Page/李玉堂.md "wikilink")
              - [第20軍](../Page/國民革命軍第二十軍.md "wikilink")（第133、134師）[楊幹才](../Page/楊幹才.md "wikilink")
              - [第26軍](../Page/國民革命軍第二十六軍.md "wikilink")（第41、44、169師）[丁治磐](../Page/丁治磐.md "wikilink")
          - [第94軍](../Page/國民革命軍第九十四軍.md "wikilink")（第121、43、5師）[牟廷芳](../Page/牟廷芳.md "wikilink")
          - [第13軍](../Page/國民革命軍第十三軍.md "wikilink")（第4、89、54師）[石覺](../Page/石覺.md "wikilink")
          - [第71軍](../Page/國民革命軍第七十一軍.md "wikilink")（第87、91、88師）[陳明仁](../Page/陳明仁.md "wikilink")
          - 砲兵第1旅[彭孟緝](../Page/彭孟緝.md "wikilink")
          - 砲兵第51團[李道恭](../Page/李道恭.md "wikilink")
      - 第四方面軍：[王耀武](../Page/王耀武.md "wikilink")（由黔桂湘邊區第二十四集團軍改編），1945年9月6日進入[長沙](../Page/長沙.md "wikilink")
          - [第73軍](../Page/國民革命軍第七十三軍.md "wikilink")（第15、77、193師）[韓濬](../Page/韓濬.md "wikilink")
          - [第74軍](../Page/國民革命軍第七十四軍.md "wikilink")（第51、57、58師）[施中誠](../Page/施中誠.md "wikilink")
          - [第100軍](../Page/國民革命軍第一百軍.md "wikilink")（第19、63師）[李天霞](../Page/李天霞.md "wikilink")
          - [第18軍](../Page/國民革命軍第十八軍.md "wikilink")（第11、18、118師）[胡璉](../Page/胡璉.md "wikilink")
          - 砲兵指揮組

## 行憲裁編

1946年5月31日，國民政府宣佈中國因即將實施[憲政](../Page/憲政.md "wikilink")，明令裁撤軍事委員會及其所屬各部會、以及[行政院之](../Page/行政院.md "wikilink")[軍政部](../Page/軍政部.md "wikilink")，改於行政院設立[國防部](../Page/中華民國國防部.md "wikilink")。1946年6月1日，中國陸軍總司令部與[國民政府軍事委員會軍訓部合併編成為](../Page/國民政府軍事委員會.md "wikilink")[陸軍總司令部](../Page/中華民國陸軍總司令部.md "wikilink")，直屬同時成立的國防部。

## 參考文獻

### 引用

### 来源

  - 书籍

<!-- end list -->

  - [何應欽](../Page/何應欽.md "wikilink")：《日軍侵華八年抗戰史》，1982年，臺北：黎明文化事業公司
  - [劉鳳翰](../Page/劉鳳翰.md "wikilink")：《抗日戰史論集》，1987年，三民書局

## 参见

  - [抗日战争战区](../Page/抗日战争战区.md "wikilink")
  - [中缅印战区](../Page/中缅印战区.md "wikilink")
  - [國防部陸軍司令部](../Page/國防部陸軍司令部.md "wikilink")

{{-}}

[Category:抗日战争](../Category/抗日战争.md "wikilink")
[陸](../Category/中華民國的司令部與指揮部.md "wikilink")

1.