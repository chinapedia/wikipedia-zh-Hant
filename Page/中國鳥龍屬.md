**中國鳥龍屬**（屬名：*Sinornithosaurus*，意為「中國的鳥蜥蜴」），是種[馳龍科的](../Page/馳龍科.md "wikilink")[有羽毛恐龍](../Page/有羽毛恐龍.md "wikilink")，化石發現於[中國的](../Page/中國.md "wikilink")[義縣組](../Page/義縣組.md "wikilink")，年代為下[白堊紀的中](../Page/白堊紀.md "wikilink")[巴列姆階](../Page/巴列姆階.md "wikilink")\[1\]。中國鳥龍是第五個發現的有羽毛恐龍，並且在1999年被發現時，是最類似[鳥類的有羽毛恐龍](../Page/鳥類.md "wikilink")。其他馳龍科恐龍，例如[伶盜龍](../Page/伶盜龍.md "wikilink")，也被認為擁有羽毛。第一個標本發現於遼寧省的四合屯，屬於義縣組的九佛堂地層，地質年代約1億2450萬年前。其他標本發現於遼寧省的大王仗子地層，地質年代較年輕，約1億2200萬年前\[2\]。

[徐星等人敘述了中國鳥龍](../Page/徐星.md "wikilink")，並提出[種系發生學的研究](../Page/種系發生學.md "wikilink")，認為牠們是種原始的馳龍科動物\[3\]。徐星也提出牠們的頭部與肩膀非常類似[始祖鳥與其他](../Page/始祖鳥.md "wikilink")[鳥翼類](../Page/鳥翼類.md "wikilink")。這兩個特徵可證實最早期的馳龍類較像鳥類，而晚期的馳龍類比較不像鳥類。這個演化時序上的矛盾，質疑了鳥類演化自恐龍的理論\[4\]。

中國鳥龍是體型最小的馳龍科恐龍之一，身長估計約90公分\[5\]。

## 有羽毛的奔跑者

[Sinornithosaurus_SIZE.png](https://zh.wikipedia.org/wiki/File:Sinornithosaurus_SIZE.png "fig:Sinornithosaurus_SIZE.png")
在中國鳥龍的標本周圍岩石都發現了羽毛的痕跡，環繞者身體與前肢。在相同地層也發現許多[鳥翼類的化石](../Page/鳥翼類.md "wikilink")，中國鳥龍與當地鳥翼類的羽毛痕跡非常類似\[6\]。羽毛痕跡長3到4.5公分，由絲狀物所構成，可大致分成兩種形態，顯示它們是早期的羽毛。第一種是數條絲狀物聚合成一叢，類似現代鳥類的[絨羽](../Page/絨羽.md "wikilink")。第二種則分佈於前肢周圍，多叢絲狀物附著至一個主幹，類似現代鳥類的羽毛結構。然而，中國鳥龍的羽毛並沒有次要分支與小型羽支，而現代鳥類的飛羽具有這些結構，這些特徵可形成現代鳥類的分散羽片\[7\]。

中國鳥龍的發現支持了鳥類飛行的「從地面起飛」理論。而「從樹上滑翔」理論則假設鳥類是從樹棲恐龍演化而來，這些樹棲恐龍從樹枝間滑翔移動。另一方面，「從地面起飛」理論則假設鳥類從奔跑恐龍演化而來，這些奔跑恐龍的羽毛具有隔熱作用，或是求偶的展示物。然而，四翼恐龍[小盜龍的發現](../Page/小盜龍.md "wikilink")，顯示原本的鳥類演化理論過度單純化，不同的[近鳥類恐龍同時發展出](../Page/近鳥類.md "wikilink")「從地面起飛」與「從樹上滑翔」的演化模式，並各自擁有相對應的生活方式。

除了羽毛以外，中國鳥龍可以拍打牠們的手臂，牠們是最早發現擁有類似鳥類[肩帶的恐龍之一](../Page/胸帶.md "wikilink")。中國鳥龍也擁有類似鳥類的[骨盆與後肢](../Page/骨盆.md "wikilink")，以及非常長的手臂。

「Dave」（編號NGMC -
91）的羽毛覆蓋了整副身體，包含了眼睛前方的頭部、頸部、前肢、大腿，尾巴末端則有菱形羽塊，類似[始祖鳥](../Page/始祖鳥.md "wikilink")\[8\]。

[Dave_NGMC_91.jpg](https://zh.wikipedia.org/wiki/File:Dave_NGMC_91.jpg "fig:Dave_NGMC_91.jpg")

## 分類

中國鳥龍屬於[馳龍科](../Page/馳龍科.md "wikilink")，馳龍科是一群行動敏捷的[肉食性恐龍](../Page/肉食性.md "wikilink")，擁有鐮刀狀的第二趾爪，也包括[恐爪龍與](../Page/恐爪龍.md "wikilink")[猶他盜龍等屬](../Page/猶他盜龍.md "wikilink")。千禧中國鳥龍生存於下[白堊紀的](../Page/白堊紀.md "wikilink")[阿普第階](../Page/阿普第階.md "wikilink")，約1億2500萬年前，使牠們成為最早、最原始的馳龍科恐龍之一。郝氏中國鳥龍、編號NGMC
91標本都發現於年代較晚的地層，約1億2200萬年前。中國鳥龍身上的羽毛，顯示較晚期的馳龍科可能也擁有羽毛，而非鱗片或簡易原始羽毛。

中國鳥龍目前已擁有至少兩個種。[模式種是](../Page/模式種.md "wikilink")**千禧中國鳥龍**（*S.
millenii*），是在1999年被敘述。第二個種是**郝氏中國鳥龍**（*S.
haoiana*），是在2004年所敘述，與千禧中國鳥龍的差異在於[頭顱骨與臀部的特徵](../Page/頭顱骨.md "wikilink")\[9\]。種名*haoiana*獻給[中科院院士](../Page/中科院院士.md "wikilink")、古生物學家[郝诒纯](../Page/郝诒纯.md "wikilink")。\[10\]一個暱稱為「Dave」的[手盜龍類標本](../Page/手盜龍類.md "wikilink")（編號NGMC
91），其保存狀態極好，可能代表中國鳥龍的第三個種，或是幼年個體。但2007年的[種系發生學研究](../Page/種系發生學.md "wikilink")，顯示這個標本可能是[小盜龍的近親](../Page/小盜龍.md "wikilink")\[11\]。在2011年，[菲力·森特](../Page/菲力·森特.md "wikilink")（Phil
Senter）提出這是因為標本的錯誤鑑定導致的結果，並將編號NGMC
91標本編入於千禧中國鳥龍\[12\]。在2012年的另一份研究，也將這個標本編入於千禧中國鳥龍\[13\]。

## 發現

[Sinornithosaurus_IVPP_V12811.jpg](https://zh.wikipedia.org/wiki/File:Sinornithosaurus_IVPP_V12811.jpg "fig:Sinornithosaurus_IVPP_V12811.jpg")\]\]
中國鳥龍是由[北京](../Page/北京.md "wikilink")[中國科學院古脊椎動物與古人類研究所的](../Page/中國科學院古脊椎動物與古人類研究所.md "wikilink")[徐星](../Page/徐星.md "wikilink")、[汪筱林](../Page/汪筱林.md "wikilink")、[吳肖春等人發現於](../Page/吳肖春.md "wikilink")[中國](../Page/中國.md "wikilink")[遼寧省的](../Page/遼寧省.md "wikilink")[義縣組](../Page/義縣組.md "wikilink")。化石幾乎完整，並擁有羽毛痕跡。在中國鳥龍之前，義縣組還發現了四種[有羽毛恐龍](../Page/有羽毛恐龍.md "wikilink")：[原始祖鳥](../Page/原始祖鳥.md "wikilink")、[中華龍鳥](../Page/中華龍鳥.md "wikilink")、[羽龍](../Page/羽龍.md "wikilink")、以及[北票龍](../Page/北票龍.md "wikilink")。

中國鳥龍的[正模標本](../Page/正模標本.md "wikilink")（編號IVPP
V12811）目前存放在[中國](../Page/中國.md "wikilink")[北京的](../Page/北京.md "wikilink")[古脊椎動物與古人類研究所](../Page/古脊椎動物與古人類研究所.md "wikilink")。第二個標本（編號NGMC
91）可能是個幼年的中國鳥龍。在2001年，[季強等人勉強地替NGMC](../Page/季強.md "wikilink")
91化石標名，因為該標本的狀態雖然相當完好，但是在分開石塊時，幾乎所有的骨頭都散開了，只能辨認出石塊上的骨頭輪廓。這使科學家難以辨認該化石的特徵，無法確定牠們的歸類\[14\]。這個編號NGMC
91的標本又俗稱「Dave」，發現於中國[遼寧省](../Page/遼寧省.md "wikilink")[凌源市](../Page/凌源市.md "wikilink")，與模式標本的發現地相距130公里，目前存放在[中國地質博物館](../Page/中國地質博物館.md "wikilink")。另外，在「Dave」的腳部附近，發現了一種[魚類](../Page/魚類.md "wikilink")（[狼鰭魚](../Page/狼鰭魚.md "wikilink")）的化石。

## 古生物學

[Sinornithosaurus.jpg](https://zh.wikipedia.org/wiki/File:Sinornithosaurus.jpg "fig:Sinornithosaurus.jpg")

### 毒牙恐龍

在2009年，一群[美國與](../Page/美國.md "wikilink")[中國科學家檢驗一個千禧中國鳥龍的顱骨](../Page/中國.md "wikilink")，發現牠們的牙齒能夠分泌毒液，是第一個被確認的能夠分泌毒液的恐龍。研究人員發現，千禧中國鳥龍的上頜中段有長牙，後側有一條明顯的溝痕，許多現代有毒動物也具有類似的長牙。除此之外，他們還發現長牙上側的上頜骨內有帶狀空間，研究人員推測這裡可能容納類似毒腺的軟組織\[15\]\[16\]。

研究人員推論，千禧中國鳥龍演化出有毒腺體與長牙，並以[鳥類等小型動物為食](../Page/鳥類.md "wikilink")，將毒液注入獵物的體內，類似現代的[蛇](../Page/蛇.md "wikilink")。他們並推測，千禧中國鳥龍的嘴部前端有微向前傾、短而尖狀牙齒，是用來剝去鳥類的羽毛\[17\]。

在2010年，另一個研究團隊提出不同看法，質疑中國鳥龍並非有毒動物。牠們提出許多獸腳類恐龍也具有類似的有溝痕牙齒，例如某些馳龍科，因此這並非中國鳥龍獨有的特徵。他們也提出化石曾遭到擠壓、變形，因此齒槽也受到變形，所屬牙齒被推擠出來，因此牙齒本身並沒有特別長，而是化石化過程中的擠壓結果。最後，他們認為先前研究發現的毒腺空間，可能只是正常的鼻竇\[18\]。

同年，主張中國鳥龍是有毒動物的研究人員們，在同份期刊提出他們的反駁。他們認同許多獸腳類恐龍也具有有溝痕牙齒，但僅限於有羽毛的[手盜龍類](../Page/手盜龍類.md "wikilink")；並進一步提出假設，認為原始主龍類可能已具有分泌毒液能力，而某些後期[演化支仍保有這項原始特徵](../Page/演化支.md "wikilink")。他們也認同長牙並非位於原始的位置，曾在化石化過程中被移動位置，但仍表示牙齒本身就比較長。他們提出中國鳥龍的未經研究標本，牙齒呈天然狀態，則已顯示該牙齒是比較長的\[19\]。

### 羽毛顏色

在2010年的一項研究，根據羽毛的顯微細胞結構，推測中國鳥龍的羽毛有明顯的不同顏色\[20\]。

### 活躍時段

在2011年，科學家比較[恐龍](../Page/恐龍.md "wikilink")、現代[鳥類與](../Page/鳥類.md "wikilink")[爬行動物的](../Page/爬行動物.md "wikilink")[鞏膜環大小](../Page/鞏膜環.md "wikilink")，提出中國鳥龍可能屬於無定時活躍性的動物，覓食、移動行為跟白天黑夜沒有正相關，只休息短暫時間\[21\]。

## 參考資料

## 外部連結

  -
  - [千禧中國鳥龍的標本特徵](http://www.dinodata.org/index.php?option=com_content&task=view&id=7436&Itemid=67)
    DinoData

  - [科學家首次找到毒恐龍
    最早在中國境內發現(圖)](http://dailynews.sina.com/bg/news/int/chinanews/20091222/0036982524.html)

[Category:有羽毛恐龍](../Category/有羽毛恐龍.md "wikilink")
[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:小盜龍亞科](../Category/小盜龍亞科.md "wikilink")
[Category:热河生物群](../Category/热河生物群.md "wikilink")

1.  Swisher, Carl C., Wang, Yuan-qing, Wang, Xiao-lin, Xu, Xing, Wang,
    Yuan. (1999). "Cretaceous age for the feathered dinosaurs of
    Liaoning, China". *Nature* 400:58-61 1 July 1999.

2.

3.

4.  Xu, Xing, Wu, Xiao-Chun. (2001). "Cranial morphology of
    *Sinornithosaurus millenii* Xu et al. 1999
    (Dinosauria:Theropoda:Dromaeosauridae) from the Yixian Formation of
    Liaoning, China". *Canadian Journal of Earth Sciences* 38:1739-1752
    (2001)

5.  Holtz, Thomas R. Jr. (2011) *Dinosaurs: The Most Complete,
    Up-to-Date Encyclopedia for Dinosaur Lovers of All Ages,*
    [Winter 2010
    Appendix.](http://www.geol.umd.edu/~tholtz/dinoappendix/HoltzappendixWinter2010.pdf)

6.
7.

8.
9.

10.

11.

12.

13.

14. Qiang, Ji, Norell, Mark A., Gao, Ke-Qin, Ji, Shu-An, Ren, Dong.
    (2001) "The distribution of integumentary structures in a feathered
    dinosaur" "Nature" 410:1084-1087 26 April 2001.

15. Fountain, Henry (2009-12-28). ["Add Venom to Arsenal of Dinosaurs on
    the Hunt"](http://www.nytimes.com/2009/12/29/science/29obdino.html)
    *The New York Times*.

16. Gong, E., L.D. Martin, D.E. Burnham, and A.R. Falk. (2009). "The
    birdlike raptor *Sinornithosaurus* was venomous." *Proceedings of
    the National Academy of Sciences*, (not yet published)

17.
18. Gianechini, F.A., Agnolín, F.L. and Ezcurra, M.D. (2010). "A
    reassessment of the purported venom delivery system of the bird-like
    raptor *Sinornithosaurus*." *Paläontologische Zeitschrift*, in
    press.

19. Gong, E., Martin, L.D., Burnham, D.A. and Falk, A.R. (2010).
    "Evidence for a venomous *Sinornithosaurus*." *Paläontologische
    Zeitschrift*, in press.

20. Zhang, Fucheng; Kearns, Stuart L.; Orr, Patrick J.; Benton, Michael
    J.; Zhou, Zhonghe; Johnson, Diane; Xu, Xing and Wang, Xiaolin
    (2010). Fossilized melanosomes and the colour of Cretaceous
    dinosaurs and birds. Nature, 463(7284), p. 1075.
    <http://oro.open.ac.uk/22432/2/41064696.pdf>

21.