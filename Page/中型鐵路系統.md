[VAL256_on_Taipei_MRT_Muzha_Line_20050320.jpg](https://zh.wikipedia.org/wiki/File:VAL256_on_Taipei_MRT_Muzha_Line_20050320.jpg "fig:VAL256_on_Taipei_MRT_Muzha_Line_20050320.jpg")

**中型鐵路系統**（，MCS）又稱**中運量系統**，是規模屆乎[重型鐵路系統和](../Page/重型鐵路系統.md "wikilink")[輕軌之間的一種](../Page/輕軌.md "wikilink")[鐵路系統](../Page/鐵路.md "wikilink")，而中低運量鋼輪鋼軌系統又可視為「**[輕軌捷運系統](../Page/輕軌捷運系統.md "wikilink")**」（Light
Rail Rapid
Transit）。但這個詞的應用並非對此類型鐵路體系的普遍稱呼，在俄羅斯，[莫斯科地铁L1号线](../Page/莫斯科地铁L1号线.md "wikilink")（每小時6,700人次）就自稱為「」，即「輕級捷運」（或「輕級地鐵」）之意，[西門子公司的全自動捷運系統](../Page/西門子公司.md "wikilink")[VAL亦用了同義的](../Page/VAL.md "wikilink")「light
metro」來宣傳自己的載客級別（每小時30,000人次）\[1\]。中型鐵路系統通常使用於對[城市軌道交通系統有需求](../Page/城市軌道交通系統.md "wikilink")，但人流或地形又未必適合使用高運量的情況下興建。但不同於[輕軌](../Page/輕軌系統.md "wikilink")，中型鐵路系統一定會有獨立路軌，而且路程一般會較輕軌為長。

中型鐵路系統普遍每小時載客10,000至35,000人次\[2\]；[中華民國交通部則定義為每小時單方向可運送](../Page/中華民國交通部.md "wikilink")6,000人至20,000人\[3\]。列車設計方面，一般較高運量為少，通常只有四至六-{zh-tw:節;zh-hk:卡;zh-cn:节;}-車廂。車輪方面，列車可採用鋼輪或[膠輪](../Page/膠輪路軌系統.md "wikilink")，而後者可使鐵路的定線設計更為靈活。而較短的月台亦降低車站的建造及維修成本。

## 各地中型鐵路系統例子

以下是使用中型鐵路系統規格興建的鐵路線：

###

  - [臺北捷運](../Page/臺北捷運.md "wikilink")**[文湖線](../Page/文湖線.md "wikilink")**（CITYFLO650，膠輪4卡組，保留增至6卡組的空間）
  - 臺北捷運**[新北投支線](../Page/新北投支線.md "wikilink")**（鋼輪3卡組，以重型鐵路規格來建設，目前以中型鐵路規格營運，保留增至6卡組進入重型鐵路模式的可能）
  - 臺北捷運**[小碧潭支線](../Page/小碧潭支線.md "wikilink")**（鋼輪3卡組，以重型鐵路規格來建設，中型鐵路規格營運）
  - 臺北捷運[環狀線](../Page/臺北捷運環狀線.md "wikilink")（第一階段興建中，已於2011年動工，鋼輪4卡組；第二階段綜合規劃中）
  - 臺北捷運[東側南北向捷運](../Page/臺北市東側南北向捷運系統.md "wikilink")（可行性研究中，鋼輪4卡組）
  - 臺北捷運[萬大樹林線](../Page/萬大樹林線.md "wikilink")（第一期興建中，已於2013年動工；第二期尚未興建）
  - 臺北捷運[民生汐止線](../Page/民生汐止線.md "wikilink")（第一階段、第二階段綜合規劃中）
  - [新北捷運](../Page/新北捷運.md "wikilink")[三鶯線](../Page/三鶯線.md "wikilink")（興建中，鋼輪2卡組，於2016年動工；桃園八德段綜合規劃中）
  - [桃園捷運](../Page/桃園捷運.md "wikilink")[機場線](../Page/桃園捷運機場線.md "wikilink")（鋼輪4卡組，以重型鐵路規格來建設，中型鐵路規格營運；中壢延伸線興建中，已於2014年動工）
  - 桃園捷運[綠線](../Page/桃園捷運綠線.md "wikilink")（已核定，尚未動工；大溪延伸線、中壢延伸線皆待核定，尚未動工）
  - [臺中捷運](../Page/臺中捷運.md "wikilink")[烏日文心北屯線](../Page/烏日文心北屯線.md "wikilink")（興建中，鋼輪2卡組，已於2009年動工）
  - 臺中捷運[綠線延伸線](../Page/臺中捷運綠線.md "wikilink")（提報中央審查中）
  - 臺中捷運[藍線](../Page/臺中捷運藍線.md "wikilink")（提報中央審查中；第二階段尚待辦理評估中）
  - [臺南捷運](../Page/臺南捷運.md "wikilink")[綠線](../Page/臺南捷運綠線.md "wikilink")（規畫中）
  - 臺南捷運[藍線](../Page/臺南捷運藍線.md "wikilink")（規畫中）
  - [高雄捷運](../Page/高雄捷運.md "wikilink")**[紅線](../Page/高雄捷運紅線.md "wikilink")**（鋼輪3輛一組，以重型鐵路規格來建設，目前以中型鐵路規格營運，預留未來擴編6輛一組進入重型鐵路模式）
  - 高雄捷運**[橘線](../Page/高雄捷運橘線.md "wikilink")**（鋼輪3輛一組，以重型鐵路規格來建設，目前以中型鐵路規格營運，預留未來擴編6輛一組進入重型鐵路模式）
  - 高雄捷運[黃線](../Page/高雄捷運黃線.md "wikilink")（尚待核定，尚未動工）

###

[Ma_On_Shan_Station.jpg](https://zh.wikipedia.org/wiki/File:Ma_On_Shan_Station.jpg "fig:Ma_On_Shan_Station.jpg")在2004年建成通車，是全球唯一一條用交流電供電的中型鐵路系統。\]\]

  - [港鐵](../Page/港鐵.md "wikilink")**[馬鞍山綫](../Page/馬鞍山綫.md "wikilink")**（以重型鐵路規格來建設，通車初期以鋼輪4卡組中型鐵路規格營運，當為配合2019年[屯馬綫通車後併入屯馬綫](../Page/屯馬綫.md "wikilink")，以於2017年1月15日起陸續更換為8卡編組車輛；但馬鞍山綫已於2017年12月23日就全面以8卡車行走；並進入重型鐵路模式）
  - 港鐵**[迪士尼綫](../Page/迪士尼綫.md "wikilink")**（鋼輪4卡組，以重型鐵路規格來建設，中型鐵路規格營運）
  - 港鐵**[南港島綫](../Page/南港島綫.md "wikilink")**（鋼輪3卡組，以重型鐵路規格來建設，中型鐵路規格營運\[4\]）

###

  - [武汉地铁1号线](../Page/武汉地铁1号线.md "wikilink")、[广州地铁APM线](../Page/珠江新城旅客自动输送系统.md "wikilink")

###

  - [新加坡地鐵](../Page/新加坡地鐵.md "wikilink")[環線](../Page/新加坡地鐵環線.md "wikilink")（鋼輪3卡組，以重型鐵路規格來建設，中型鐵路規格營運）
  - [新加坡地鐵](../Page/新加坡地鐵.md "wikilink")[濱海市區線](../Page/新加坡地鐵濱海市區線.md "wikilink")（鋼輪3卡組，以重型鐵路規格來建設，中型鐵路規格營運）

###

  -
###

  - [曼谷市](../Page/曼谷市.md "wikilink")[集體運輸系統](../Page/曼谷集體運輸系統.md "wikilink")

###

  - [紐約](../Page/紐約.md "wikilink")[甘迺迪國際機場捷運](../Page/甘迺迪國際機場捷運.md "wikilink")、[洛杉磯捷運紅線](../Page/洛杉磯捷運.md "wikilink")、紫線

###

[Cline.jpg](https://zh.wikipedia.org/wiki/File:Cline.jpg "fig:Cline.jpg")[加拿大線](../Page/加拿大線.md "wikilink")[列車](../Page/列車.md "wikilink")\]\]

  - [多倫多](../Page/多倫多.md "wikilink")[士嘉堡輕鐵](../Page/士嘉堡輕鐵.md "wikilink")（Scarborough
    RT）
  - [溫哥華](../Page/溫哥華.md "wikilink")[高架列車](../Page/溫哥華高架列車.md "wikilink")（Sky
    Train）

###

  - [莫斯科地鐵](../Page/莫斯科地鐵.md "wikilink")[L1線](../Page/布托沃线.md "wikilink")

###

  - [哥本哈根地鐵](../Page/哥本哈根地鐵.md "wikilink")

###

  - [倫敦](../Page/倫敦.md "wikilink")[碼頭區輕便鐵路](../Page/碼頭區輕便鐵路.md "wikilink")

## 相關條目

  - [重型鐵路系統](../Page/重型鐵路系統.md "wikilink")
  - [輕型鐵路系統](../Page/輕型鐵路系統.md "wikilink")

## 参考资料

<div class="references-small">

<references/>

[Category:鐵路](../Category/鐵路.md "wikilink")

1.  [西門斯TS VAL 及
    NeoVAL](http://www.transportation.siemens.com/ts/en/pub/products/mt/products/val.htm)

2.  [港鐵南港島線 \> 常見問題 \>
    設計及建造](http://www.mtr-southislandline.hk/tc/faq/faq.html#design)

3.  台灣交通部交通統計名詞
4.