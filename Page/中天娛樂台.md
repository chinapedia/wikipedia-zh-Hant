**中天娛樂台**是[中天電視旗下的](../Page/中天電視.md "wikilink")[電視頻道](../Page/電視頻道.md "wikilink")，以播出[娛樂性質的](../Page/娛樂.md "wikilink")[電視節目為主](../Page/電視節目.md "wikilink")，並有多個節目與[中視無線台聯播](../Page/中視無線台.md "wikilink")。前身為[中視衛星傳播股份有限公司經營的](../Page/中視衛星傳播股份有限公司.md "wikilink")「**中視衛星**」頻道，與[中天綜合台共用節目播出](../Page/中天綜合台.md "wikilink")。

## 簡史

2005年12月31日晚上，首次播出2006[台北最HIGH新年城](../Page/台北最HIGH新年城.md "wikilink")

2009年11月28日上午，頻道召開改版記者會，增加中國元素，提出新口號「翻轉娛樂，Fun瘋中國」。

2014年11月開始在[Youtube測試HD高畫質訊號](../Page/Youtube.md "wikilink")。

## 外部連結

  - [中天電視](http://www.ctitv.com.tw/)

[娛樂台](../Category/中天電視.md "wikilink")
[Category:台灣電視播放頻道](../Category/台灣電視播放頻道.md "wikilink")