**中天綜合台**，是[中天電視旗下的頻道之一](../Page/中天電視.md "wikilink")，與[中天娛樂台共用節目播出](../Page/中天娛樂台.md "wikilink")。

## 簡史

  - 中天綜合台是[中天電視下屬頻道](../Page/中天電視.md "wikilink")，前身為2001年1月開播的[勁報電視台](../Page/勁報電視台.md "wikilink")（Power
    TV）。
  - 2001年1月，勁報電視台（Power TV）開播。
  - 2001年4月，勁報電視台更名為「中天資訊台」。
  - 2001年6月，[中視衛星更名為](../Page/中視衛星.md "wikilink")[勁道數位電視](../Page/勁道數位電視.md "wikilink")。
  - 2002年6月，[中國時報系接手經營勁道數位電視](../Page/中國時報集團.md "wikilink")，勁道數位電視更名為中天電視。
  - 2004年1月，中天資訊台更名為「中天綜合台」。
  - 2005年起，[中天綜合台定頻在有線電視](../Page/中天綜合台.md "wikilink")36頻道。
  - 2007年12月17日，《精彩臺灣》系列節目改定於[中天娛樂台](../Page/中天娛樂台.md "wikilink")，《[全民最大黨](../Page/全民最大黨.md "wikilink")》改定於[中天綜合台](../Page/中天綜合台.md "wikilink")（於2010年8月2日改定中天娛樂台）。
  - 2014年11月，[中天綜合台開始在](../Page/中天綜合台.md "wikilink")[Youtube測試HD高畫質訊號](../Page/Youtube.md "wikilink")。

## 得獎紀錄

### 電視金鐘獎

|- | 2005年 |《[康熙來了](../Page/康熙來了.md "wikilink")》
|[第40屆金鐘獎綜藝節目主持人獎](../Page/第40屆金鐘獎.md "wikilink")
|  |- | 2005年 |《[全民大悶鍋](../Page/全民大悶鍋.md "wikilink")》
|[第40屆金鐘獎](../Page/第40屆金鐘獎.md "wikilink") 綜藝節目獎 |  |- | 2005年
|《[費玉清的清音樂](../Page/費玉清.md "wikilink")》
|[第40屆金鐘獎](../Page/第40屆金鐘獎.md "wikilink") 歌唱音樂節目獎 |
|- | 2016年 |《[小明星大跟班](../Page/小明星大跟班.md "wikilink")》
|[第51屆金鐘獎](../Page/第51屆金鐘獎.md "wikilink") 綜藝節目主持人獎 |  |- |
2017年 |《[這些年那些事](../Page/這些年那些事.md "wikilink")》
|[第52屆金鐘獎](../Page/第52屆金鐘獎.md "wikilink") 戲劇節目最佳男主角獎 |
 |- |}

## 參考資料

## 外部連結

  - [中天電視](http://www.ctitv.com.tw/)

[Category:台灣電視播放頻道](../Category/台灣電視播放頻道.md "wikilink")
[綜合臺](../Category/中天電視.md "wikilink")
[Category:2001年成立的电视台或电视频道](../Category/2001年成立的电视台或电视频道.md "wikilink")