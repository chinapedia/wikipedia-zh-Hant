**中央财经大学**，简称**中央财大**或**央財，中财**，是所位于中国北京市的高校，为[中华人民共和国](../Page/中华人民共和国.md "wikilink")[教育部直属](../Page/教育部.md "wikilink")“[211工程](../Page/211工程.md "wikilink")”高校，是[优势学科创新平台项目高校之一](../Page/优势学科创新平台项目.md "wikilink")，2012年4月成为首个被国家教育部、国家财政部、北京市共建高校，2017年9月被列入[国家双一流计划一流学科建设高校](../Page/世界一流大学和一流学科建设.md "wikilink")。

中央财经大学追溯于1949年的中央税务学校，历经中央财政学院、中央财经学院、中央财政金融学院等发展阶段，最终定名为现在的中央财经大学。

## 学校历史

[cufe.gif](https://zh.wikipedia.org/wiki/File:cufe.gif "fig:cufe.gif")
[中华人民共和国在北京建立政权伊始](../Page/中华人民共和国.md "wikilink")，当时的中共领导人[陈云提议建立一家有](../Page/陈云.md "wikilink")[财政](../Page/财政.md "wikilink")、[税务](../Page/税务.md "wikilink")、[会计](../Page/会计.md "wikilink")、[审计等专业的大专院校](../Page/审计.md "wikilink")。由此，中央财经大学的前身——中央税务学校于1949年11月6日在北京建立\[1\]。

1952年，中国进行了高等院校调整工作，[北京大学](../Page/北京大学.md "wikilink")、[燕京大学](../Page/燕京大学.md "wikilink")、[清华大学](../Page/清华大学.md "wikilink")、[辅仁大学的财经系科并入了中央财政学院](../Page/辅仁大学.md "wikilink")，同时，该校更名为“中央财经学院”，并直接由中国高教部领导；后来又改为由[中国财政部](../Page/中华人民共和国财政部.md "wikilink")、[中国人民银行共同领导](../Page/中国人民银行.md "wikilink")，校名也再次更换为“中央财政金融学院”\[2\]。

该学校在[文革期间处于停办的状态](../Page/文革.md "wikilink")，校舍也被北京卷烟厂占有\[3\]；直到[文革之后复校时才收回](../Page/文革.md "wikilink")，现在学校当中还可以看到部分[文革遗迹](../Page/文革.md "wikilink")。

1978年2月，学校复校，并由[财政部一家领导](../Page/中华人民共和国财政部.md "wikilink")。1996年5月16日，学校再度更名，“中央财经大学”宣告成立，也使学校从单纯的财经类院校，慢慢的向以财经专业为主，以财经相关的分支专业为辅的财经综合类高校转变\[4\]。1999年10月29日，时任[中共中央总书记](../Page/中共中央总书记.md "wikilink")、[中华人民共和国主席的](../Page/中华人民共和国主席.md "wikilink")[江泽民为该校](../Page/江泽民.md "wikilink")50周年校庆题词：“努力办好中央财经大学，为现代化建设输送高质量管理人才”。

2009年7月6日，中央财经大学宣布以该校标志性雕塑“吞吐大荒”为设计核心的新版校徽正式启用，以取代使用多年的老版校徽\[5\]。

2009年10月17日，该校举办庆典，庆祝中央财经大学建校60周年\[6\]。[CUFE60校庆.jpg](https://zh.wikipedia.org/wiki/File:CUFE60校庆.jpg "fig:CUFE60校庆.jpg")

学校现为[211工程大学](../Page/211工程.md "wikilink")，2006年12月入选首批985“优势学科创新平台”，是进入首批985平台建设的5所院校中唯一财经类高校\[7\]並參與[央視財經50指數編制](../Page/央視財經50指數.md "wikilink")。2007年1月，入选[国家建设高水平大学公派研究生项目](../Page/国家建设高水平大学公派研究生项目.md "wikilink")，是首批入选的46所高校中唯一的财经类高校。2011年9月，该校获批研究生院。也是首批拥有研究生院的五所财经高校之一。2017年9月被列入[国家双一流计划一流学科建设高校](../Page/世界一流大学和一流学科建设.md "wikilink")，建设学科为一级学科“应用经济学”，是唯一入选建设该学科且非自定的财经高校（其余两所为[北京大学](../Page/北京大学.md "wikilink")、[中国人民大学](../Page/中国人民大学.md "wikilink")）。

### 数据

截至2013年9月，全校事业编制教职工1563人，其中专任教师1061人。教授230人，占专任教师总数的21.68%，副教授374人，占专任教师总数的35.25%；具有博士学位者695人，占专任教师总数的65.50%。学校现有全日制学生14827人，其中，本科生9800人，硕士研究生3997人，博士研究生615人，留学生415人。

该校目前拥有49个本科专业、8个国家特色专业建设点和7个北京市特色专业建设点；拥有11个国家重点学科（一级学科国家重点学科1个、二级学科国家重点学科1个）、以及工商管理一级学科、统计学、政治经济学、世界经济、交叉学科经济信息管理等北京市一级二级重点学科；拥有应用经济学、理论经济学、工商管理、统计学一级学科博士学位授权，38个博士学位授权点，77个硕士学位授权点和11个专业学位授权点；拥有北京市级以上精品课程17门，曾获得5项国家级教学成果奖和21项北京市教学成果奖。拥有应用经济学、理论经济学、工商管理、统计学、马克思主义哲学5个博士后科研流动站；拥有国家“优势学科创新平台”首批5个试点项目之一\[8\]。

### 学校环境

<div style="text-align: right;">

[CUFEMB.jpg](https://zh.wikipedia.org/wiki/File:CUFEMB.jpg "fig:CUFEMB.jpg")

</div>

学校紧邻[北京交通大学](../Page/北京交通大学.md "wikilink")、[中国政法大学研究生院](../Page/中国政法大学.md "wikilink")、[中国农业科学院](../Page/中国农业科学院.md "wikilink")、[北京理工大学](../Page/北京理工大学.md "wikilink")、[北京外国语大学](../Page/北京外国语大学.md "wikilink")、[北京邮电大学](../Page/北京邮电大学.md "wikilink")、[北京师范大学等高等学府](../Page/北京师范大学.md "wikilink")；且在[海淀区](../Page/海淀区.md "wikilink")[上庄镇设立本科一年级分部](../Page/上庄镇.md "wikilink")，原设立于[清河街道的分部已经撤销](../Page/清河.md "wikilink")。由于学校的规模已经无法适应办学的需求，中央财经大学在与[北京市政府和](../Page/北京市.md "wikilink")[教育部签订一系列协议之后](../Page/教育部.md "wikilink")，在[北京市](../Page/北京市.md "wikilink")[昌平区](../Page/昌平区.md "wikilink")[沙河镇](../Page/沙河.md "wikilink")（城铁昌平线沙河高教园站）设立新校址作为新的本科生校址，并于2009年9月8日正式投入使用，第一批约4,500名新生已经入驻\[9\]。

学校内设有一座主教学楼、一座电教楼和一座行政楼，可满足基本教学和办公。教学楼北侧设有两座食堂楼和图书馆。东侧为小区内的老式平房，该校的[MBA中心和中国保险](../Page/MBA.md "wikilink")[精算研究院均设立在此](../Page/精算.md "wikilink")。西侧是体育场，其中包含一个标准田径／足球场，另外还有6个露天篮球场和两个网球场。该校在2007年中重新整修体育场，该工程已于2007年11月完工并交付使用，完工后的体育场带有标准塑胶跑道和人工草皮足球场；地下为停车场。

校内设有五座学生宿舍楼、研究生宿舍区和留学生公寓等宿舍楼，其中5座学生宿舍楼分别以学1\~学5命名，除学5楼（研究生宿舍）外，条件比较简陋，一般为6\~8人间，没有独立卫浴，洗澡必须到学校东侧的公共浴室；为解决学生住宿问题，学校在校外也租有宿舍供研究生和部分本科一年级学生使用，命名为学6\~学8。2006年，学校又在附近的北京城建属地租得部分楼宇的使用权，也暂时缓解了主校区的压力。留学生公寓条件较好，配备了独立卫浴和厨房，以及[电视](../Page/电视.md "wikilink")、[冰箱](../Page/冰箱.md "wikilink")、[空调和](../Page/空调.md "wikilink")[互联网接入等设备](../Page/互联网.md "wikilink")\[10\]。

[CUFE新综合楼.jpg](https://zh.wikipedia.org/wiki/File:CUFE新综合楼.jpg "fig:CUFE新综合楼.jpg")校内原设有“专家宾馆”，经过重新装修，现仍对外营业。学校内另有打印、小卖部、书报亭、公用电话等服务场所，但公共服务设施还是相对稀缺。

近来，学校加快校园基础设施建设。目前，该学校在主校区西南侧正在建设的新综合大楼已经完工，取名为“中财大厦”。该楼东侧部分现已建成融金中财大酒店\[11\]，目前已投入使用；西侧为研究生宿舍楼及各类仿真实验室，已于2007年8月底竣工并投入使用。目前研究生新生已经入住该楼，部分办公室及实验室也迁至该楼。学校东南角原有的停车场和车队办公处已拆除，在此建立的新综合楼已于2009年10月启用。

2017年6月20日下午，中央财经大学沙河校区发生一起凶杀案，一名校外男子在杀死一名财经大学的女大学生后自杀\[12\]。

## 组织机构

### 历任校长

  - **[李予昂](../Page/李予昂.md "wikilink")**(1949.11-1952.6)
  - **[陈岱孙](../Page/陈岱孙.md "wikilink")**(1952.8-1953.8)
  - **[王绍鳌](../Page/王绍鳌.md "wikilink")**(1952.8-1955.8)
  - **[胡立教](../Page/胡立教.md "wikilink")**(1955.8-1956.9)
  - **[安志成](../Page/安志成.md "wikilink")**(1957.9-1959.4)
  - **[贝仲宁](../Page/贝仲宁.md "wikilink")**(1960.8-1961.5)
  - **[陈如龙](../Page/陈如龙.md "wikilink")**(1964.10-1966.5)
  - **[戎子和](../Page/戎子和.md "wikilink")**(1978.6-1983.8)
  - **[陈菊铨](../Page/陈菊铨.md "wikilink")**(1983.8-1985.11)
  - **[王柯敬](../Page/王柯敬.md "wikilink")**(1992.9-2003.6)
  - **[王广谦](../Page/王广谦.md "wikilink")**(2003.7-2017.9)
  - **[王瑶琪](../Page/王瑶琪.md "wikilink")**(2017.9-)\[13\]

### 著名教授

全职教授：[陈岱孙](../Page/陈岱孙.md "wikilink")、[崔书香](../Page/崔书香.md "wikilink")、[姜维壮](../Page/姜维壮.md "wikilink")、[李天民](../Page/李天民.md "wikilink")、[李爽](../Page/李爽.md "wikilink")、[黄少安](../Page/黄少安.md "wikilink")、[王广谦](../Page/王广谦.md "wikilink")、[李健](../Page/李健_\(中央财经大学教授\).md "wikilink")、[孟焰](../Page/孟焰.md "wikilink")、[祁怀锦](../Page/祁怀锦.md "wikilink")、[刘姝威](../Page/刘姝威.md "wikilink")、[邹恒甫](../Page/邹恒甫.md "wikilink")、[郝演苏](../Page/郝演苏.md "wikilink")、[贺强](../Page/贺强.md "wikilink")、[李俊生](../Page/李俊生.md "wikilink")、[张礼卿](../Page/张礼卿.md "wikilink")、[潘省初](../Page/潘省初.md "wikilink")、[陈文灯](../Page/陈文灯.md "wikilink")、[郭田勇](../Page/郭田勇.md "wikilink")、[王瑞华](../Page/王瑞华.md "wikilink")
兼职教授：[吴晓灵](../Page/吴晓灵.md "wikilink")、[戴相龙](../Page/戴相龙.md "wikilink")、[巴曙松](../Page/巴曙松.md "wikilink")、[陈建成](../Page/陈建成.md "wikilink")

### 下设院系

<table>
<tbody>
<tr class="odd">
<td><p>财政税务学院</p></td>
<td><p>金融学院</p></td>
<td><p>会计学院</p></td>
</tr>
<tr class="even">
<td><p>保险学院 中国精算研究院</p></td>
<td><p>统计与数学学院、数学教学部</p></td>
<td><p>国际经济与贸易学院</p></td>
</tr>
<tr class="odd">
<td><p>经济学院</p></td>
<td><p>商学院　 MBA教育中心</p></td>
<td><p>管理科学与工程学院</p></td>
</tr>
<tr class="even">
<td><p>政府管理学院</p></td>
<td><p>体育经济与管理学院</p></td>
<td><p>法学院</p></td>
</tr>
<tr class="odd">
<td><p>社会与心理学院</p></td>
<td><p>马克思主义学院</p></td>
<td><p>文化与传媒学院</p></td>
</tr>
<tr class="even">
<td><p>外国语学院</p></td>
<td><p>信息学院</p></td>
<td><p>财经研究院 </p>
<ul>
<li>北京财经研究基地</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>国防经济与管理研究院</p></td>
<td><p>中国财政发展协同创新中心</p></td>
<td><p>国际文化交流学院</p></td>
</tr>
<tr class="even">
<td><p>继续教育学院 </p>
<ul>
<li>继续教育工作管理办公室 </li>
<li>网络教育学院 </li>
<li>培训学院 </li>
</ul></td>
<td><p>经济学与公共政策优势学科创新平台 </p>
<ul>
<li>中国经济与管理研究院 </li>
<li>中国金融发展研究院 </li>
<li>中国公共财政与政策研究院 </li>
<li>人力资本与劳动经济研究中心</li>
</ul></td>
<td><p>北京学院 </p></td>
</tr>
</tbody>
</table>

\[14\]

### 名人校友

  - [田纪云](../Page/田纪云.md "wikilink") 中央政治局委员，国务院副总理
  - [王丙乾](../Page/王丙乾.md "wikilink")
    原财政部部长，[全国人大常委会副委员长](../Page/全国人大常委会副委员长.md "wikilink")
  - [王光英](../Page/王光英.md "wikilink") 原全国人大常委会副委员长
  - [戴相龙](../Page/戴相龙.md "wikilink")
    [全国社会保障基金理事会党组书记](../Page/全国社会保障基金理事会.md "wikilink")、理事长
    [中国人民银行原行长](../Page/中国人民银行.md "wikilink")、原[天津市市长](../Page/天津市.md "wikilink")
  - [金人庆](../Page/金人庆.md "wikilink") 国务院发展研究中心副主任（正部长级），原中国财政部部长
  - [李金华](../Page/李金华_\(审计长\).md "wikilink")
    第十一届[中国人民政治协商会议全国委员会副主席](../Page/中国人民政治协商会议.md "wikilink")、原中国财政部国家审计署审计长
  - [樊晡生](../Page/樊晡生.md "wikilink") 中国人民银行 党委书记（正部长级）
  - [段晓兴](../Page/段晓兴.md "wikilink") 中国人民银行 党组成员、行长助理 ， 华夏银行行长
  - [史纪良](../Page/史纪良.md "wikilink") 中国农业银行 行长、银监会副主席
  - [陈剖建](../Page/陈剖建.md "wikilink") 天安集团董事长
  - [段方晓](../Page/段方晓.md "wikilink") 瑞泰集团副总裁
  - [缪建民](../Page/缪建民.md "wikilink")
    [中国人寿副总经理](../Page/中国人寿.md "wikilink")
    人寿资产董事长
  - [冯知杰](../Page/冯知杰.md "wikilink")
    [中国人寿财险副总经理](../Page/中国人寿.md "wikilink")
  - [王启人](../Page/王启人.md "wikilink")
    中共驻[澳门特别行政区联络办公室主任](../Page/澳门特别行政区.md "wikilink")
  - [李克穆](../Page/李克穆.md "wikilink") 中国保监会 党委副书记、副主席
  - [骆玉林](../Page/骆玉林.md "wikilink")
    [青海省常务副省长](../Page/青海省.md "wikilink")
  - [穆虹](../Page/穆虹.md "wikilink")
    中国[国家发改委副主任](../Page/国家发展与改革委员会.md "wikilink")、原[广西壮族自治区人民政府副主席](../Page/广西壮族自治区.md "wikilink")
  - [丁燕生](../Page/丁燕生.md "wikilink")
    [中银香港及中银香港](../Page/中银香港.md "wikilink")（控股）有限公司副总裁
  - [汪晓峰](../Page/汪晓峰.md "wikilink") 金信信托董事长
  - [宫少林](../Page/宫少林.md "wikilink") 招商银行副行长 招商证券董事长
  - [牛锡明](../Page/牛锡明.md "wikilink")
    [交通银行行长](../Page/交通银行.md "wikilink")\[15\]、原[中国工商银行副行长](../Page/中国工商银行.md "wikilink")
  - [陶礼明](../Page/陶礼明.md "wikilink") 中国邮政储蓄银行总行 行长
  - [魏东](../Page/魏东.md "wikilink") 已故涌金公司原总裁
  - [王成铭](../Page/王成铭.md "wikilink") 中央金融工委副书记
  - [叶一新](../Page/叶一新.md "wikilink") 中国银行澳门分行总经理
  - [孙志强](../Page/孙志强.md "wikilink")
    解放军[总后勤部副部长](../Page/总后勤部.md "wikilink")、财务部长、总后勤部党委委员、十六届中央委员
  - [刘嘉德](../Page/刘嘉德.md "wikilink") 人寿股份副总裁
  - [乔伟](../Page/乔伟.md "wikilink")
    原[交通银行副行长](../Page/交通银行.md "wikilink")
  - [沈若雷](../Page/沈若雷.md "wikilink") 原工行上海分行行长 现花旗人寿董事长
  - [邵淳](../Page/邵淳.md "wikilink") 原华夏证券总裁
  - [金建栋](../Page/金建栋.md "wikilink") 国泰君安总裁
  - [李东荣](../Page/李东荣.md "wikilink") 中国人民银行党委委员、副行长
  - [侯凯](../Page/侯凯.md "wikilink") 中纪委常委，审计署副审计长
  - [余蔚平](../Page/余蔚平.md "wikilink") 财政部党组成员、部长助理
  - [翟熙贵](../Page/翟熙贵.md "wikilink") 原审计署副审计长
  - [程法光](../Page/程法光.md "wikilink") 原国家税务总局副局长
  - [杨凌隆](../Page/杨凌隆.md "wikilink") 中央财经领导小组办公室 副主任
  - [丁先觉](../Page/丁先觉.md "wikilink") 国务院金融理事会 主席
  - [范巍](../Page/范巍.md "wikilink") 国务院重点金融机构监事会 主席
  - [魏礼江](../Page/魏礼江.md "wikilink") 国务院重点金融机构监事会 主席
  - [高瑞科](../Page/高瑞科.md "wikilink") 湖北省人民政府 原副省长
  - [谭丽霞](../Page/谭丽霞.md "wikilink") 海尔集团副总裁 财务总监
  - [戴凤举](../Page/戴凤举.md "wikilink") 中国再保险集团 原董事长、总经理

## 参看

  - [中华人民共和国教育部直属高等学校列表](../Page/中华人民共和国教育部直属高等学校列表.md "wikilink")
  - [211工程](../Page/211工程.md "wikilink")
  - [国家重点学科](../Page/国家重点学科.md "wikilink")

## 外部链接

  - [中财官方网站](http://www.cufe.edu.cn/)
  - [中财论坛](http://www.cufebbs.net/) 由中财学生自主发起运营维护的校园论坛
  - [中央财经大学校友总会](http://alumni.cufe.edu.cn/) 中央财经大学校友总会

## 參考資料

[Category:中华人民共和国教育部直属高等学校](../Category/中华人民共和国教育部直属高等学校.md "wikilink")
[Category:北京高等院校](../Category/北京高等院校.md "wikilink")
[Category:211工程](../Category/211工程.md "wikilink")
[Category:1949年創建的教育機構](../Category/1949年創建的教育機構.md "wikilink")
[中央财经大学](../Category/中央财经大学.md "wikilink")
[Category:财经院校](../Category/财经院校.md "wikilink")
[Category:一流学科建设高校](../Category/一流学科建设高校.md "wikilink")

1.

2.
3.

4.
5.

6.

7.

8.

9.

10. [留学生公寓条件](http://sice.cufe.edu.cn/news.asp?id=189)

11.

12.

13.

14.

15.