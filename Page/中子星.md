[PIA18848-PSRB1509-58-ChandraXRay-WiseIR-20141023.jpg](https://zh.wikipedia.org/wiki/File:PIA18848-PSRB1509-58-ChandraXRay-WiseIR-20141023.jpg "fig:PIA18848-PSRB1509-58-ChandraXRay-WiseIR-20141023.jpg")[PSR
B1509-58的辐射](../Page/PSR_B1509-58.md "wikilink")，一个快速旋转的中子星，使得周边气体[X射线发光](../Page/X射线.md "wikilink")（金色，来自[钱德拉](../Page/钱德拉X射线天文台.md "wikilink")）并且照亮[星云的其他部分](../Page/星云.md "wikilink")，这里以[红外线可见](../Page/红外线.md "wikilink")（蓝色与红色，来自[WISE](../Page/廣域紅外線巡天探測衛星.md "wikilink")）。\]\]

**中子星**（），是[恒星演化到末期](../Page/恒星演化.md "wikilink")，經由[引力坍縮發生](../Page/引力坍縮.md "wikilink")[超新星爆炸之後](../Page/超新星.md "wikilink")，可能成為的少數[終點之一](../Page/緻密星.md "wikilink")。恆星在核心的氫、氦、碳等元素於[核聚变反應中耗盡](../Page/核聚变.md "wikilink")，当它们最终轉變成鐵元素時便無法从[核聚变中获得能量](../Page/核聚变.md "wikilink")。失去熱輻射壓力支撐的外圍物質受重力牽引會急速向核心墜落，有可能导致外壳的動能轉化為熱能向外爆發產生[超新星爆炸](../Page/超新星.md "wikilink")，或者根据恒星质量的不同，恒星的内部区域被压缩成[白矮星](../Page/白矮星.md "wikilink")、中子星或[黑洞](../Page/黑洞.md "wikilink")。[白矮星被压缩成中子星的過程中恒星遭受劇烈的壓縮使其組成物質中的](../Page/白矮星.md "wikilink")[電子併入](../Page/電子.md "wikilink")[質子轉化成](../Page/質子.md "wikilink")[中子](../Page/中子.md "wikilink")，直徑大約只有十餘公里，但上面一立方厘米的物質便可重達十億噸，且旋轉速度極快。由於其磁軸和自轉軸並不重合，[磁場旋轉時所產生的無線電波等各种辐射可能會以一明一滅的方式傳到](../Page/磁場.md "wikilink")[地球](../Page/地球.md "wikilink")，有如人眨眼，此時稱作[脈衝星](../Page/脈衝星.md "wikilink")。

一顆典型的中子星質量介於太陽質量的1.35到2.1倍，半徑則在10至20公里之間（質量越大半徑收縮得越小），也就是太陽半徑的30,000至70,000分之一。因此，中子星的[密度在每立方公分](../Page/密度.md "wikilink")8×10<sup>13</sup>克至2×10<sup>15</sup>克間，此密度大約是[原子核的密度](../Page/原子核.md "wikilink")\[1\]。

緻密恆星的質量低於[1.44倍太陽質量](../Page/錢德拉塞卡極限.md "wikilink")，則可能是[白矮星](../Page/白矮星.md "wikilink")，但质量大於[奧本海默-沃爾可夫極限](../Page/奧本海默-沃爾可夫極限.md "wikilink")（3.2倍[太陽質量](../Page/太陽質量.md "wikilink")）的恆星会继续發生[引力坍縮](../Page/引力坍縮.md "wikilink")，則無可避免的將產生[黑洞](../Page/黑洞.md "wikilink")。

由於中子星保留母恆星大部分的[角動量](../Page/角動量.md "wikilink")，但半徑只是母恆星極微小的量，[轉動慣量的減少導致轉速迅速的增加](../Page/轉動慣量.md "wikilink")，產生非常高的自轉速率，周期從[毫秒脈衝星的](../Page/毫秒脈衝星.md "wikilink")700分之一秒到30秒都有。中子星的高密度也使它有強大的[表面重力](../Page/表面重力.md "wikilink")，強度是[地球的](../Page/地球.md "wikilink")2×10<sup>11</sup>到3×10<sup>12</sup>倍。[逃逸速度是將物體由重力場移動至無窮遠的距離所需要的](../Page/逃逸速度.md "wikilink")[速度](../Page/速度.md "wikilink")，是測量[重力的一項指標](../Page/重力.md "wikilink")。一顆中子星的[逃逸速度大約在](../Page/逃逸速度.md "wikilink")10,000至150,000公里／秒之間，也就是可以達到光速的一半。換言之，物體落至中子星表面的速度也將達到150,000公里／秒。更具體的說明，如果一個普通體重（70公斤）的人遇到中子星，他撞擊到中子星表面的能量將相當於二億噸[TNT當量的威力](../Page/TNT當量.md "wikilink")（四倍於全球最巨大的核彈[大沙皇的威力](../Page/沙皇炸彈.md "wikilink")）\[2\]。

## 历史上的发现

[Neutron_star_cross_section.svg](https://zh.wikipedia.org/wiki/File:Neutron_star_cross_section.svg "fig:Neutron_star_cross_section.svg")
1932年，[英國](../Page/英國.md "wikilink")[剑桥大学](../Page/剑桥大学.md "wikilink")[卡文迪许实验室的](../Page/卡文迪许实验室.md "wikilink")[詹姆斯·查德威克发现](../Page/詹姆斯·查德威克.md "wikilink")[中子](../Page/中子.md "wikilink")\[3\]（因此获得1935年的[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")）。[苏联著名物理学家](../Page/苏联.md "wikilink")[列夫·朗道当时正在丹麦著名物理学家](../Page/列夫·朗道.md "wikilink")[波尔那里访问](../Page/波尔.md "wikilink")，参加了波尔召集的新发现的中子的讨论。讨论会上，朗道敏锐地推断如果恒星质量超过[钱德拉塞卡极限](../Page/钱德拉塞卡极限.md "wikilink")，也不会一直塌缩下去，因为电子会被压进氦原子核中，质子和电子将会因引力的作用结合在一起成为中子。中子和电子一样，也是遵循泡利不相容原理的[费米子](../Page/费米子.md "wikilink")，因此这些中子在一起产生的“中子简并压”力，可以抗衡引力使得恒星成为密度比白矮星大得多的稳定的中子星。但朗道的想法并没有发表。1934年，[美国](../Page/美国.md "wikilink")[威尔逊山天文台工作的](../Page/威尔逊山天文台.md "wikilink")[沃尔特·巴德和](../Page/沃尔特·巴德.md "wikilink")[弗里茨·兹威基发表文章称](../Page/弗里茨·兹威基.md "wikilink")，中子简并压力能够支持质量超过[钱德拉塞卡极限的恒星](../Page/钱德拉塞卡极限.md "wikilink")，预言中子星的存在\[4\]。為尋找[超新星爆炸的解釋](../Page/超新星.md "wikilink")，他們提議中子星是超新星爆炸後的產物。超新星是突然出現在天空中的垂死恆星，在出現後的幾天或整個星期內，在可見光的亮度上可以超越整個[星系](../Page/星系.md "wikilink")。巴德和茨威基正確的解釋產生中子星時釋放出的重力束縛能，供給超新星的能量：“在超新星形成的過程中大量的質量被湮滅”。如果在中心的大質量恆星在他崩潰之前的質量是太陽質量的3倍，那麼在中心可能形成一顆2倍太陽質量的中子星。被釋放出來的[束縛能](../Page/结合能.md "wikilink")（E
=
mc<sup>2</sup>）相當於一個太陽的質量全數轉化成能量，這足以作為[超新星最後的能量來源](../Page/超新星.md "wikilink")。

[第二次世界大战爆发前不久](../Page/第二次世界大战.md "wikilink")，美国物理学家[罗伯特·奥本海默和](../Page/罗伯特·奥本海默.md "wikilink")[乔治·沃尔科夫提出系统的中子星理论](../Page/乔治·沃尔科夫.md "wikilink")，认为在质量与太阳相似的恒星内部可以达到简并中子的流体静力学平衡，但是并没有引起天文学界的重视。

1965年，英国射电天文学家[安東尼·休伊什和Samuel](../Page/安東尼·休伊什.md "wikilink")
Okoye在公元1054年的超新星（[天關客星](../Page/天關客星.md "wikilink")）爆炸後的殘骸「[蟹狀星雲](../Page/蟹狀星雲.md "wikilink")」\[5\]發現一個異於平常的高電波亮度溫度源。

1967年，[剑桥大学](../Page/剑桥大学.md "wikilink")[卡文迪许实验室的](../Page/卡文迪许实验室.md "wikilink")[安东尼·休伊什学生](../Page/安东尼·休伊什.md "wikilink")[乔丝琳·贝尔发现](../Page/乔丝琳·贝尔.md "wikilink")1.337秒有规律的无线电脉冲，人们将这一类新天体称为“[脉冲星](../Page/脉冲星.md "wikilink")”，并且确认它们就是30年前朗道预言的中子星，发出的脉冲是中子星快速旋转的结果。

1968年有人提出脉冲星是快速旋转的中子星\[6\]。

1969年，在[1054年超新星爆发的残骸](../Page/天關客星.md "wikilink")[蟹状星云中](../Page/蟹状星云.md "wikilink")，发现了一颗射电脉冲星（中子星），证明了[脉冲星](../Page/脉冲星.md "wikilink")、中子星和[超新星之间的关系](../Page/超新星.md "wikilink")。

1971年，[里卡尔多·贾科尼等人发现](../Page/里卡尔多·贾科尼.md "wikilink")[半人马座的](../Page/半人马座.md "wikilink")[X射线源](../Page/X射线源.md "wikilink")[半人马座X-3具有](../Page/半人马座X-3.md "wikilink")4.8秒的周期\[7\]，他們解釋這是一顆炙熱的中子星環繞者另一顆恆星的結果，能量來源是持續不斷掉落至中子星表面的氣體釋放出的引力势能。这是第一颗证认的[X射线双星](../Page/X射线双星.md "wikilink")。

1974年，[安東尼·休伊什因為在脈衝星的發現上所扮演的角色而獲得](../Page/安東尼·休伊什.md "wikilink")[諾貝爾物理學獎](../Page/諾貝爾物理學獎.md "wikilink")，但是共同的發現者Samuel
Okoye和[乔丝琳·贝尔並未一同獲獎](../Page/乔丝琳·贝尔.md "wikilink")。

## 一些能觀測的中子星

  - [X-射線爆發](../Page/X-射線爆發.md "wikilink")–中子星與低質量恆星共同組成的聯星，在質量吸積的過程中會造成中子星表面不規則的能量爆發。
  - [脈衝星](../Page/脈衝星.md "wikilink")–一般的說法是由於中子星強大的磁場，使得發射的電磁波隨著中子星的自轉，以脈衝的形式定期的朝向我們發射。
  - [磁星](../Page/磁星.md "wikilink")–磁場特別強大的中子星，有些磁星能夠連續的發射[軟γ射線](../Page/軟γ射線.md "wikilink")。

### 脈衝星

當脈衝星被發現之後，快速的脈衝（大約1秒鐘，在1960年代的天文學是很不尋常的）被半認真的視為[外星高智生命傳送來的訊息](../Page/SETI.md "wikilink")，隨後被半開玩笑的稱為小綠人，標示為*LGM-1*。但在更多的，以不同的自轉週期散佈在天空各處的脈衝星被發現之後，就迅速的排除了這種可能性。而在發現[船帆座脈衝星和](../Page/船帆座.md "wikilink")[超新星殘骸的關聯性之後](../Page/超新星殘骸.md "wikilink")，更進一步發現[蟹狀星雲的能量來自一顆脈衝星](../Page/蟹狀星雲.md "wikilink")，不得不令人信服脈衝星是中子星的解釋。

### 磁星

还有另外一种中子星，称作磁星。磁星具有大约10<sup>11</sup>
[特斯拉的磁场](../Page/特斯拉.md "wikilink")，大约是普通中子星的1000倍。这足以在月球轨道的一半距离上擦除地球上的一张信用卡。作为对比，地球的自然磁场是大约6×10<sup>－5</sup>特斯拉；一小块[钕磁铁的磁场大约是](../Page/钕磁铁.md "wikilink")1特斯拉；多数用于数据存储的磁介质可以被10<sup>-3</sup>特斯拉的磁场擦除。

磁星有时会产生X射线脉冲。大约每10年，银河系中就会有某一颗磁星爆发出很强的[伽马射线](../Page/伽马射线.md "wikilink")。磁星有比较长的自转周期，一般为5到12秒，

## 巨大核心

中子星大致分成三层，核心部分因压力更大，由[超子组成](../Page/超子.md "wikilink")；中间层则是自由中子，表面因中子进行β衰变成[电子](../Page/电子.md "wikilink")、[质子](../Page/质子.md "wikilink")、[微中子](../Page/微中子.md "wikilink")。因具有[原子核的某些包括密度在内的性质](../Page/原子核.md "wikilink")。因此，在流行的科学文献中，中子星有时被称为巨型原子核。然而在其他方面，中子星和真正的原子核是很不一样的。例如，原子核是靠[强相互作用结合在一起](../Page/强相互作用.md "wikilink")，而中子星是靠[引力相互作用结合在一起](../Page/引力相互作用.md "wikilink")。根据当今主流理论，把它们看作[天体会更正確一些](../Page/天体.md "wikilink")。

## 中子星的例子

  - [PSR J0108-1431](../Page/PSR_J0108-1431.md "wikilink") –最近的中子星
  - [LGM-1](../Page/LGM-1.md "wikilink") –第一个公认的无线电脉冲星
  - [PSR B1257+12](../Page/PSR_B1257+12.md "wikilink")
    –第一个中子星发现有行星（毫秒脉冲星）
  - [SWIFT J1756.9-2508](../Page/SWIFT_J1756.9-2508.md "wikilink")
    –一个毫秒脉冲星与行星质量范围的的恒星型伴侣（在褐矮星下面）
  - [PSR
    B1509-58](../Page/PSR_B1509-58.md "wikilink")“宇宙之手”的照片，由[钱德拉X射线天文台拍摄](../Page/钱德拉X射线天文台.md "wikilink")。
  - [PSR J0348+0432](../Page/PSR_J0348+0432.md "wikilink") –至今發現質量最高的中子星

## 媒體

Image:Neutron Star Manhattan.ogv|包含五十萬地球质量的中子星球形直径为 Image:Crash and
Burst.ogv|[中子星碰撞模擬動畫](../Page/中子星碰撞.md "wikilink") Image:Neutron star
collision.ogv|中子星碰撞

## 相關條目

  - [夸克星](../Page/夸克星.md "wikilink")
  - [中子](../Page/中子.md "wikilink")
  - [脈衝星](../Page/脈衝星.md "wikilink")
  - [毫秒脈衝星](../Page/毫秒脈衝星.md "wikilink")
  - [磁星](../Page/磁星.md "wikilink")

## 参考文献

<div class="references-small">

<references />

  -
  -

</div>

## 外部連結

  - [Introduction to neutron
    stars](http://www.astro.umd.edu/~miller/nstar.html)
  - [NASA Sees Hidden Structure Of Neutron Star In
    Starquake](http://www.spacedaily.com/reports/NASA_Sees_Hidden_Structure_Of_Neutron_Star_In_Starquake.html)
    (SpaceDaily)[April 26](../Page/April_26.md "wikilink") 2006
  - [Mysterious X-ray sources may be lone neutron
    stars](http://www.newscientistspace.com/article.ns?id=dn9397&feedId=online-news_rss20)
    - New Scientist
  - [Massive neutron star rules out exotic
    matter](http://www.newscientistspace.com/article.ns?id=dn9428&feedId=online-news_rss20)
    - According to a new analysis, exotic states of matter such as free
    quarks or BECs do not arise inside neutron stars (New Scientist)
  - [Neutron star clocked at mind-boggling
    velocity](http://www.newscientistspace.com/article.ns?id=dn9730&feedId=online-news_rss20)
    - A neutron star has been clocked travelling at more than 1500
    kilometres per second (New Scientist)

[N](../Category/簡併恆星.md "wikilink") [N](../Category/恒星演化.md "wikilink")
[N](../Category/中子.md "wikilink") [\*](../Category/中子星.md "wikilink")
[N](../Category/奇特物质.md "wikilink")

1.
2.  \(\frac{(150,000 \ km/s)^2 \times \ 70 \ kg \times 1/2}{4.184 \times 10^{15} \ J/megaton} = 188.2 \ megatons\)
3.
4.
5.
6.
7.
    \[<http://adsabs.harvard.edu/cgi-bin/nph-bib_query?bibcode=1971ApJ>...167L..67G
    \]