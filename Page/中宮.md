**中宮**，只見於古代[中國及部分](../Page/中國.md "wikilink")[東亞地區](../Page/東亞.md "wikilink")，指的是[紫微垣](../Page/紫微垣.md "wikilink")\[1\]或天極星\[2\]，「洛書軌跡」等風水或陰陽學，稱土為中宮\[3\]，或九宮、洛書九星中間為中宮，[秦漢以後](../Page/秦漢.md "wikilink")[王后演變成](../Page/王后.md "wikilink")[皇后](../Page/皇后.md "wikilink")，則稱皇后居住的宮室，或稱皇后本身。原來王后的宮室「[後宮](../Page/後宮.md "wikilink")」，反而不在中宮之列。

## 辭源

君主時代在建設宮城時，大多將皇后的宮室建於子午線上，位於後宮的中心，因而稱之中宮。

## 中國

中宮，或稱皇后\[4\]\[5\]，也象徵皇后居住的宮殿\[6\]
，如[漢朝](../Page/漢朝.md "wikilink")[長樂宮中的長秋宮](../Page/長樂宮.md "wikilink")、[紫禁城](../Page/紫禁城.md "wikilink")[坤寧宮等等](../Page/坤寧宮.md "wikilink")，而坤寧宮雖自[雍正以後不再作為皇后寢宮](../Page/雍正.md "wikilink")，但仍是實質意義上的中宮。

## 日本

[日本的中宮](../Page/日本.md "wikilink")，最早是皇后、[皇太后](../Page/皇太后.md "wikilink")、[太皇太后的總稱](../Page/太皇太后.md "wikilink")。自[村上天皇立](../Page/村上天皇.md "wikilink")[女御](../Page/女御.md "wikilink")[藤原安子為皇后起](../Page/藤原安子.md "wikilink")，中宮成為皇后別稱。[一條天皇時](../Page/一條天皇.md "wikilink")，權臣[藤原道長迫使一條天皇冊立兩位皇后](../Page/藤原道長.md "wikilink")，並為此將中宮作為獨立、等同於皇后的正式封號，從此天皇能同時擁有兩正配，造成**一帝二-{后}-**的[平妻現象](../Page/平妻.md "wikilink")。此後諸多天皇在冊立皇后時，多先立為中宮，日後再立第二位正配時，便將先前的中宮改號為名義上較高的皇后，新立的第二位正配則是中宮。

自[平安時代前期開始](../Page/平安時代.md "wikilink")，中務省轄下有所謂[中宮職](../Page/中宮職.md "wikilink")，是專職服侍與負責與中宮相關的事物。在上下任天皇交替時，若前代天皇中宮仍享有中宮職的承職，則新任天皇皇后只能稱皇后。[二戰以後](../Page/二戰.md "wikilink")，[日本皇室實行](../Page/日本皇室.md "wikilink")[一夫一妻制](../Page/一夫一妻制.md "wikilink")，中宮一號遂廢。最後一位中宮是[光格天皇的中宮](../Page/光格天皇.md "wikilink")[欣子內親王](../Page/欣子內親王.md "wikilink")。

|width="25%" align="center"|前一位階：
[皇后](../Page/皇后.md "wikilink") |width="50%" align="center"|**日本後宮位階**
|width="25%" align="center"|次一位階：
[女御](../Page/女御.md "wikilink")

## 朝鮮王朝

[朝鮮王朝是](../Page/朝鮮王朝.md "wikilink")[中國的](../Page/中國.md "wikilink")[朝貢國](../Page/朝貢國.md "wikilink")，其君主只能稱地位較次的[國王而非](../Page/國王.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")，因此國王的正室於生前亦不得稱[皇后](../Page/皇后.md "wikilink")，稱較次的[王妃](../Page/王妃.md "wikilink")，死後方能[追諡為](../Page/諡號.md "wikilink")[王后](../Page/王后.md "wikilink")。

王妃在口語上的稱謂是**中殿**、**坤殿**或**中宮**，其宮室[交泰殿亦稱做](../Page/交泰殿.md "wikilink")**中宮殿**。國王換代後，升為[王大妃或](../Page/王大妃.md "wikilink")[大王大妃的王妃](../Page/王大妃.md "wikilink")，口語上的稱謂則是**上殿**、**慈殿**。宮中僕役、臣民和[嬪御稱王妃為](../Page/嬪御.md "wikilink")**中殿娘娘**（[韓語](../Page/韓語.md "wikilink")：），是[敬語](../Page/敬語.md "wikilink")，音譯「媽媽」，也通用於尊稱男性，相當於[漢語的](../Page/漢語.md "wikilink")「[娘娘](../Page/娘娘.md "wikilink")」、「爺」。

## 注釋

[Category:朝鮮後宮](../Category/朝鮮後宮.md "wikilink")
[Category:日本後宮](../Category/日本後宮.md "wikilink")
[Category:中國後宮](../Category/中國後宮.md "wikilink")
[Category:皇族称谓](../Category/皇族称谓.md "wikilink")

1.  《宋史》卷四十九 志第二 天文二；《步天歌》
2.  《史記．卷二七．天官書》：「中宮天極星，其一明者，太一常居也。」
3.  《白虎通德論·五行》
4.  《新唐書．卷一一二．馮元常傳》：「嘗密諫中宮權重，宜少抑，帝雖置其計，而內然之，由是為武后所惡。」
5.  《封神演義》: 中宮元配皇后姜氏，西宮妃黃氏，馨慶宮妃楊氏。
6.  《漢書．卷一一．哀帝紀》：「春秋母以子貴，尊定陶太后曰恭皇太后，丁姬曰恭皇后，各置左右詹事，食邑如長信宮、中宮。」