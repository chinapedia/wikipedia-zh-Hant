**《中山世鑑》**是[琉球國歷史上第一部正史](../Page/琉球國.md "wikilink")，為琉球立國的三部典籍之一，記錄了[琉球開國的神話以及](../Page/琉球國.md "wikilink")[琉球王國從](../Page/琉球王國.md "wikilink")[舜天王朝至](../Page/舜天王朝.md "wikilink")[第二尚氏王朝](../Page/第二尚氏王朝.md "wikilink")[尚清王代歷史](../Page/尚清王.md "wikilink")，並且對琉球所屬36島的情況進行了詳細描述。該書由[琉球攝政](../Page/琉球攝政.md "wikilink")[尚盛主持](../Page/尚盛.md "wikilink")，大臣[向象賢執筆](../Page/向象賢.md "wikilink")，於[慶安三年](../Page/慶安.md "wikilink")（1650年）開始編撰。其中，卷首部分是用[漢文書寫](../Page/漢文.md "wikilink")；正卷部分共5卷，則是用[日本語寫成](../Page/日本語.md "wikilink")。

## 《中山世鑑》編纂者

  - [國相](../Page/琉球攝政.md "wikilink")：
      - [尚盛](../Page/尚盛.md "wikilink")（金武王子朝貞）
  - [法司](../Page/法司.md "wikilink")：
      - [馬加美](../Page/馬加美.md "wikilink")（大里親方良安）
      - [章邦彥](../Page/章邦彥.md "wikilink")（宜野灣親方正成）
      - [向錫類](../Page/向錫類.md "wikilink")（國頭親方朝李）
  - 纂修司：
      - [向象賢](../Page/向象賢.md "wikilink")（羽地按司朝秀）

## 《中山世鑑》總目錄

  - 首卷：中山世鑑序，琉球國中山王舜天以來世系圖，先國王尚圓以來世系圖，琉球國中山王世繼總論；
  - 卷一：[琉球開闢之事](../Page/阿摩美久.md "wikilink")，[天孫王統](../Page/天孫王朝.md "wikilink")，[舜天王統](../Page/舜天王朝.md "wikilink")；
  - 卷二：[英祖王統](../Page/英祖王朝.md "wikilink")，[察度王統](../Page/察度王朝.md "wikilink")；
  - 卷三：[尚巴志王統](../Page/第一尚氏.md "wikilink")；
  - 卷四：[尚圓王統](../Page/尚圓王.md "wikilink")；
  - 卷五：[尚圓王統](../Page/第二尚氏.md "wikilink")[尚清王代](../Page/尚清王.md "wikilink")。

## 參見

  - [阿摩美久](../Page/阿摩美久.md "wikilink")
  - [中山世譜](../Page/中山世譜.md "wikilink")
  - [球陽](../Page/球陽.md "wikilink")

## 外部連結

  - [《中山世鑑》寫本（田島利三郎抄寫，藏於琉球大學圖書館伊波普猷文庫）](https://web.archive.org/web/20110722080146/http://manwe.lib.u-ryukyu.ac.jp/cgi-bin/disp-img.cgi?file=iha0230)

  - [国立国会図書館デジタルコレクション -
    琉球国中山世鑑](http://dl.ndl.go.jp/info:ndljp/pid/1217404?tocOpened=1)

  - [中山世鑑　ちゅうざんせいかん](http://miko.org/~uraki/kuon/furu/text/chuuzan/seikan.htm)

[Category:琉球國](../Category/琉球國.md "wikilink")
[Category:亚洲历史著作](../Category/亚洲历史著作.md "wikilink")
[Category:琉球史書](../Category/琉球史書.md "wikilink")