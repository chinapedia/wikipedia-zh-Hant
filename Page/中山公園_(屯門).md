[Hung_Lau_090111_3.JPG](https://zh.wikipedia.org/wiki/File:Hung_Lau_090111_3.JPG "fig:Hung_Lau_090111_3.JPG")
[Hung_Lau_090111_16.JPG](https://zh.wikipedia.org/wiki/File:Hung_Lau_090111_16.JPG "fig:Hung_Lau_090111_16.JPG")
**中山公園**，又稱**屯門中山公園**、**龍門路中山公園**，位於[香港](../Page/香港.md "wikilink")[屯門區](../Page/屯門區.md "wikilink")[蝴蝶灣以北](../Page/蝴蝶灣.md "wikilink")，面對[龍門路](../Page/龍門路.md "wikilink")，鄰近[蝴蝶邨及](../Page/蝴蝶邨.md "wikilink")[兆山苑](../Page/兆山苑.md "wikilink")。相傳此地是[孫中山當年與](../Page/孫中山.md "wikilink")[革命同志聚會的地方](../Page/兴中会.md "wikilink")。公園內的青山紅樓屬於1920年代至1930年代的建築物風格，為[一級歷史建築](../Page/香港一級歷史建築列表.md "wikilink")。公園尚有孫中山的[銅像及](../Page/銅像.md "wikilink")[紀念碑](../Page/紀念碑.md "wikilink")，還曾經有6株[桄榔樹](../Page/桄榔樹.md "wikilink")，由[黃興及孫中山所種植](../Page/黃興.md "wikilink")，目前均已經枯萎，只是餘下紀念碑文。[香港民國派人士每年都會在這裡慶祝](../Page/香港親台人士.md "wikilink")[元旦日和](../Page/元旦日.md "wikilink")[雙十國慶](../Page/雙十國慶.md "wikilink")，升起[青天白日滿地紅旗](../Page/青天白日滿地紅旗.md "wikilink")，並且高唱[國歌](../Page/中華民國國歌.md "wikilink")。

目前中山公園暫時關閉，進行裝修。

自2018年中華民國國慶起，[香港民國派人士原定在紅樓內舉辦中華民國相關活動](../Page/香港親台人士.md "wikilink")，遭到業主刁難。

## 與興中會的關連

香港商人[李陞的兒子](../Page/李陞.md "wikilink")[李紀堂在](../Page/李紀堂.md "wikilink")1901年擁有及經營的「[青山農場](../Page/青山農場.md "wikilink")」（新生農場）。李紀堂在往[日本的輪船上認識](../Page/日本.md "wikilink")[孫中山](../Page/孫中山.md "wikilink")，其後李紀堂在1895年加入[興中會](../Page/興中會.md "wikilink")，隨後將青山農場提供給[興中會使用](../Page/興中會.md "wikilink")。當時[農場的面積廣闊](../Page/農場.md "wikilink")，種植[瓜類](../Page/瓜類.md "wikilink")、[水果](../Page/水果.md "wikilink")、[蔬菜](../Page/蔬菜.md "wikilink")、飼養[雞](../Page/雞.md "wikilink")
及[鴨](../Page/鴨.md "wikilink")，能夠自給自足。青山農場地點隱蔽，是[興中會的秘密基地](../Page/興中會.md "wikilink")，用作軍事訓練及儲存[武器](../Page/武器.md "wikilink")，後部分藏有軍械和炸藥密庫，左邊山坡則有練習射擊的靶場\[1\]。著名的[惠州起義及](../Page/惠州起義.md "wikilink")[廣州](../Page/廣州.md "wikilink")[黃花崗起義事件](../Page/黃花崗起義.md "wikilink")，皆在青山農場策劃。今天的中山公園就是昔日青山農場的所在地。昔日青山農場佔地250餘畝\[2\]，積極支持革命的李紀堂曾將父親留給他的一半遺產捐出以資助廣州起義。紅樓一帶樹木繁密，容易避人耳目。興中會廣東各縣成員雲集於此，策劃1911年黃花崗之役。其後農場租予海豐人洪德全，設為新生農場，紅樓則為農場辦事處\[3\]。

## 公園內建築

### 青山紅樓

[Hung_Lau.jpg](https://zh.wikipedia.org/wiki/File:Hung_Lau.jpg "fig:Hung_Lau.jpg")
[Hung_Lau_under_damage_20170219.JPG](https://zh.wikipedia.org/wiki/File:Hung_Lau_under_damage_20170219.JPG "fig:Hung_Lau_under_damage_20170219.JPG")
[Save_Hung_Lau_protest_20170219.JPG](https://zh.wikipedia.org/wiki/File:Save_Hung_Lau_protest_20170219.JPG "fig:Save_Hung_Lau_protest_20170219.JPG")
**青山紅樓**，又稱**新生農場紅樓**，或簡稱**紅樓**，由於位於中山公園內，故又常稱作**中山公園紅樓**。建築物是一棟兩層的混合中西建築方式之[紅磚房屋](../Page/紅磚.md "wikilink")，屬1920年代至1930年代建築風格。位置上，紅樓屬坐西北向東南的[乾宅](../Page/乾宅.md "wikilink")，背靠[青山](../Page/青山_\(香港\).md "wikilink")（亦為屯門舊名），鄰近[白角](../Page/白角_\(屯門\).md "wikilink")。紅樓內藏有一革命理論[牌匾](../Page/牌匾.md "wikilink")，相傳為孫中山手筆；孫中山曾在紅樓內的一張[木桌子上策劃革命行動](../Page/木.md "wikilink")。紅樓於2009年12月18日，列為一級歷史建築，但未升級為法定古蹟。

不過到2016年11月，原由李兆堃持有的地皮轉手至一間新成立的睿麗有限公司（董事為大陸人肖俊峯）。新買家在2017年1月25日發出律師函，要求住戶在一周內搬離。到2月12日，住戶發現紅樓的圍牆及一些樹木被破壞，感到不開心。20名居民感到徬徨無助，希望獲安排上樓。現場亦有數名保安在紅樓外駐守，阻止其他人進入\[4\]。

立法會議員[朱凱廸](../Page/朱凱廸.md "wikilink")、[尹兆堅和屯門區議員](../Page/尹兆堅.md "wikilink")[朱順雅](../Page/朱順雅.md "wikilink")、元朗區議員[麥業成](../Page/麥業成.md "wikilink")、[黃偉賢均認為紅樓應盡快宣布為暫定法定古蹟](../Page/黃偉賢.md "wikilink")，覺得整件事不尋常\[5\]。

2月19日下午，保育紅樓中山公園聯席會議發起「萬眾同心護紅樓」行動，逾300人到場聲援。大會向前來聲援的人士派發象徵民主、自由及博愛的紅白藍絲帶。[朱凱迪指紅樓是活生生的歷史見證及代表政治的自由空間](../Page/朱凱迪.md "wikilink")。不過目前一級歷史建築不包括中山公園，要求政府的保育方案不限紅樓，亦應包括中山公園。\[6\]

不過古諮會主席林筱魯則指，紅樓的歷史價值可能只是「以訛傳訛」，認為業主沒計劃拆卸，暫不建議屯門紅樓成為暫定古蹟。\[7\]

2017年3月8日，兩名工人進入紅樓以鐵鎚強行拆毀兩扇窗，居民擔心有塌樓危險報警，屋宇署人員到場調查後張貼勸喻信，指有人涉未經許可作違法清拆工程。\[8\]。至3月9日，古諮會因應3月8日事件而優先討論「紅樓列為暫定古蹟」，主席林筱魯認為業主行動已觸及屋宇條例，建議通過將紅樓列為暫定古蹟，最終在未有委員反對下獲通過。\[9\]。

### 銅像及紀念碑

[Flag_of_ROC_09012011_2.ogv](https://zh.wikipedia.org/wiki/File:Flag_of_ROC_09012011_2.ogv "fig:Flag_of_ROC_09012011_2.ogv")

孫逸仙博士半身銅像：1969年（民國五十八年）設立，附有孫中山（孫逸仙）的[遺囑](../Page/總理遺囑.md "wikilink")：

孫逸仙博士紀念碑：在1969年元旦豎立，矗立於紅樓右面憩息小園中，碑高約20餘英呎，其頂部為紅圈內嵌[中華民國國徽](../Page/中華民國國徽.md "wikilink")）。碑正下方有[香港名流](../Page/香港.md "wikilink")[周埈年爵士所撰](../Page/周埈年.md "wikilink")370餘字的碑文。紀念碑兩旁展示了多幅[中華民國國旗](../Page/中華民國國旗.md "wikilink")，是香港少數長期公開展示青天白日滿地紅旗的地方之一。

[File:Sunyatsenstatus.JPG|近觀孫逸仙博士的半身銅像](File:Sunyatsenstatus.JPG%7C近觀孫逸仙博士的半身銅像)
<File:Hung> Lau 090111 8.JPG|銅像及紀念碑前的基石 <File:Hung> Lau 090111
29.JPG|銅像及紀念碑右側 <File:Hung> Lau 090111 15.JPG|香港黃煜堃先生撰寫的碑文
<File:Hung> Lau 090111 14.JPG|香港孫逸仙紀念會撰寫的碑文 <File:Hung> Lau 090111
30.JPG|慶祝中華民國成立百周年橫額 <File:Hung> Lau 090111 21.JPG|高掛青天白日滿地紅旗

### 桄榔樹

六株桄榔樹：紅樓前原有六株，其中3株是[孫逸仙博士](../Page/孫逸仙.md "wikilink")（孫中山先生）所種植，1960年遭受電殛，現已枯死，另外3株是[黃興先生所種](../Page/黃興.md "wikilink")，可惜其中兩株分別在2000年夏季及秋季枯死，僅餘的樹幹亦在2001年7月被惡意砍掉，另一株亦在2001年9月倒下。

紀念碑文 ：

[File:1995-06-14_Red-house_004.jpg|1995年6月，三株桄榔樹原貌](File:1995-06-14_Red-house_004.jpg%7C1995年6月，三株桄榔樹原貌)
[File:1995-06-14_Red-house_005.jpg|1995年6月，三株桄榔樹原貌](File:1995-06-14_Red-house_005.jpg%7C1995年6月，三株桄榔樹原貌)
<File:Hung> Lau 090111 11.JPG|2011年1月時的桄榔樹

## 公共交通

<div class="NavFrame collapsed" style="color: black; background-color: yellow; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: yellow; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: yellow; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - [輕鐵](../Page/香港輕鐵.md "wikilink")[蝴蝶站](../Page/蝴蝶站.md "wikilink")
      - [610](../Page/香港輕鐵610線.md "wikilink"):[元朗總站](../Page/元朗站_\(輕鐵\).md "wikilink")↔[屯門碼頭](../Page/屯門碼頭.md "wikilink")（經[大興](../Page/大興.md "wikilink")）
      - [615](../Page/香港輕鐵615線.md "wikilink"):[元朗總站](../Page/元朗站_\(輕鐵\).md "wikilink")↔[屯門碼頭](../Page/屯門碼頭.md "wikilink")（經[良景](../Page/良景.md "wikilink")）
      - [615P](../Page/香港輕鐵615P線.md "wikilink"):[兆康](../Page/兆康站_\(輕鐵\).md "wikilink")↔[屯門碼頭](../Page/屯門碼頭.md "wikilink")（經[良景](../Page/良景.md "wikilink")）

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 參考資料

<div class="references-small">

<references/>

</div>

## 相關條目

  - [下白泥碉堡](../Page/下白泥碉堡.md "wikilink")
  - [中山公園](../Page/中山公園.md "wikilink")
  - [中山紀念公園](../Page/中山紀念公園.md "wikilink")
  - [孫中山紀念館](../Page/孫中山紀念館_\(香港\).md "wikilink")
  - [孫中山史蹟徑](../Page/孫中山史蹟徑.md "wikilink")
  - [調景嶺](../Page/調景嶺.md "wikilink")

## 外部連結

  - [屯門中山公園](https://digital.lib.hkbu.edu.hk/sunyatsen/?park=%E5%B1%AF%E9%96%80%E4%B8%AD%E5%B1%B1%E5%85%AC%E5%9C%92)，收錄於[中山公園數據庫](https://digital.lib.hkbu.edu.hk/sunyatsen/index.php)，由[香港浸會大學圖書館](https://digital.lib.hkbu.edu.hk/digital/project.php)提供
  - [香港自遊樂在18區介紹](http://www.gohk.gov.hk/chi/welcome/tm_spots.html)
  - [屯門青山農場與辛亥革命運動](https://archive.is/20040709095153/http://202.76.36.61/vol%2013/vol13Doc2_3.htm)
  - [屯門中山公園／紅樓](http://bigdogkee.blogspot.com/2006_12_01_archive.html)，bigdogkee's
    blog
  - [紅樓 -
    屯門](https://web.archive.org/web/20071008215424/http://www.hkinnovation.com/bighongkong/bhkphoto/redhouse/)，大香港旅遊王
  - [青山紅樓在2009年1月的外觀影像](http://www.youtube.com/watch?v=QT30P9f9Z-A)

[分類:香港主題公園](../Page/分類:香港主題公園.md "wikilink")

[港](../Category/中山公園.md "wikilink")
[Category:香港一級歷史建築](../Category/香港一級歷史建築.md "wikilink")
[Category:蝴蝶灣](../Category/蝴蝶灣.md "wikilink")
[Category:屯門區公園](../Category/屯門區公園.md "wikilink")

1.

2.
3.
4.

5.

6.

7.

8.

9.