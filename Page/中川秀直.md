**中川秀直**（），出生于[東京都](../Page/東京都.md "wikilink")[新宿区](../Page/新宿区.md "wikilink")，是[日本政治家](../Page/日本.md "wikilink")，[日本众议院议员](../Page/日本众议院.md "wikilink")，[日本自由民主党成员](../Page/日本自由民主党.md "wikilink")，曾任自民党干事长、科学技术厅长官和[森喜朗内阁的](../Page/森喜朗.md "wikilink")[内阁官房长官](../Page/内阁官房长官.md "wikilink")。

## 簡介

2000年在任[森喜朗内阁的内阁官房长官时](../Page/森喜朗.md "wikilink")，因[绯闻及与](../Page/绯闻.md "wikilink")[黑社会有染等负面](../Page/黑社会.md "wikilink")[丑闻而被迫辞职](../Page/丑闻.md "wikilink")。

后长期担任自民党国会对策委员会委员长。在[小泉纯一郎内阁时期](../Page/小泉纯一郎.md "wikilink")，被小泉纯一郎重用，担任自民党内[党三役之一的政调会长](../Page/党三役.md "wikilink")。

2006年[安倍晋三出任首相后](../Page/安倍晋三.md "wikilink")，做为监护人出任自民党干事长。2007年参议院大选自民党惨败，首次失去第一大黨地位，中川引咎辞职，不再担任自民党干事长职务。

2008年在[麻生太郎任首相期间](../Page/麻生太郎.md "wikilink")，中川和[加藤紘一](../Page/加藤紘一.md "wikilink")、[武部勤合作](../Page/武部勤.md "wikilink")，成为自民党内反麻生活动的主要领导人之一。

[Category:日本自由民主党干事长](../Category/日本自由民主党干事长.md "wikilink")
[Category:日本自由民主黨政務調查會長](../Category/日本自由民主黨政務調查會長.md "wikilink")
[Category:日本自由民主黨國會對策委員長](../Category/日本自由民主黨國會對策委員長.md "wikilink")
[Category:新自由俱樂部黨員](../Category/新自由俱樂部黨員.md "wikilink")
[Category:日本内阁官房长官](../Category/日本内阁官房长官.md "wikilink")
[Category:日本科学技术厅长官](../Category/日本科学技术厅长官.md "wikilink")
[Category:第一次橋本內閣閣僚](../Category/第一次橋本內閣閣僚.md "wikilink")
[Category:第二次森內閣閣僚](../Category/第二次森內閣閣僚.md "wikilink")
[Category:日本廢除死刑運動者](../Category/日本廢除死刑運動者.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:慶應義塾大學校友](../Category/慶應義塾大學校友.md "wikilink")
[Category:日本眾議院議員
1976–1979](../Category/日本眾議院議員_1976–1979.md "wikilink")
[Category:日本眾議院議員
1980–1983](../Category/日本眾議院議員_1980–1983.md "wikilink")
[Category:日本眾議院議員
1983–1986](../Category/日本眾議院議員_1983–1986.md "wikilink")
[Category:日本眾議院議員
1986–1990](../Category/日本眾議院議員_1986–1990.md "wikilink")
[Category:日本眾議院議員
1993–1996](../Category/日本眾議院議員_1993–1996.md "wikilink")
[Category:日本眾議院議員
1996–2000](../Category/日本眾議院議員_1996–2000.md "wikilink")
[Category:日本眾議院議員
2000–2003](../Category/日本眾議院議員_2000–2003.md "wikilink")
[Category:日本眾議院議員
2003–2005](../Category/日本眾議院議員_2003–2005.md "wikilink")
[Category:日本眾議院議員
2005–2009](../Category/日本眾議院議員_2005–2009.md "wikilink")
[Category:日本眾議院議員
2009–2012](../Category/日本眾議院議員_2009–2012.md "wikilink")
[Category:日本經濟新聞社人物](../Category/日本經濟新聞社人物.md "wikilink")
[Category:廣島縣選出日本眾議院議員](../Category/廣島縣選出日本眾議院議員.md "wikilink")
[Category:中國比例代表區選出日本眾議院議員](../Category/中國比例代表區選出日本眾議院議員.md "wikilink")