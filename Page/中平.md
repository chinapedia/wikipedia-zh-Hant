**中平**（184年十二月－189年三月）是[东汉皇帝](../Page/东汉.md "wikilink")[汉灵帝刘宏的第四个](../Page/汉灵帝.md "wikilink")[年号](../Page/年号.md "wikilink")，也是[汉献帝刘协的第二个](../Page/汉献帝.md "wikilink")[年号](../Page/年号.md "wikilink")。[汉朝使用这个年号时间共记](../Page/汉朝.md "wikilink")6年。

使用“中平”為年號是為了慶祝平定[黃巾之亂](../Page/黃巾之亂.md "wikilink")。

中平六年四月，汉灵帝駕崩，汉少帝[刘辩即位](../Page/刘辩.md "wikilink")。改元[光熹元年](../Page/光熹.md "wikilink")。

漢獻帝[永汉元年](../Page/永汉_\(汉献帝\).md "wikilink")（189年）十二月，即中平六年十二月，“诏除光熹、[昭宁](../Page/昭宁.md "wikilink")、永汉三号”，复称中平六年。

## 纪年

| 中平                               | 元年                                 | 二年                                 | 三年                                 | 四年                                 | 五年                                 | 六年                                 |
| -------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- |
| [公元](../Page/公元纪年.md "wikilink") | [184年](../Page/184年.md "wikilink") | [185年](../Page/185年.md "wikilink") | [186年](../Page/186年.md "wikilink") | [187年](../Page/187年.md "wikilink") | [188年](../Page/188年.md "wikilink") | [189年](../Page/189年.md "wikilink") |
| [干支](../Page/干支纪年.md "wikilink") | [甲子](../Page/甲子.md "wikilink")     | [乙丑](../Page/乙丑.md "wikilink")     | [丙寅](../Page/丙寅.md "wikilink")     | [丁卯](../Page/丁卯.md "wikilink")     | [戊辰](../Page/戊辰.md "wikilink")     | [己巳](../Page/己巳.md "wikilink")     |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2004年12月 ISBN 7101025129

[Category:东汉年号](../Category/东汉年号.md "wikilink")
[Category:2世纪年号](../Category/2世纪年号.md "wikilink")
[Category:180年代中国](../Category/180年代中国.md "wikilink")