[Dimsum_breakfast_in_Hong_Kong.jpg](https://zh.wikipedia.org/wiki/File:Dimsum_breakfast_in_Hong_Kong.jpg "fig:Dimsum_breakfast_in_Hong_Kong.jpg")的[點心](../Page/點心.md "wikilink")\]\]
**中華式小吃**指源自[華人地區的](../Page/華人.md "wikilink")[小吃](../Page/小吃.md "wikilink")，一般根据本地的特产制作。有的中式小吃由于取材比较普遍，很快流行到其他地区；但有的小吃由于口味独特或只用本地材料，只能局限在一个地区。现代由于人口流动大，材料运输方便，许多原来局限在一个地区的中華小吃，迅速向各地扩散。著名小吃則靠著口耳相傳而與大眾文化緊密結合，甚至成為當地代表的飲食，例如[台南的台南](../Page/台南.md "wikilink")[擔仔麵](../Page/擔仔麵.md "wikilink")、[台灣的](../Page/永和區.md "wikilink")[永和豆漿](../Page/永和豆漿.md "wikilink")、[北京的](../Page/北京.md "wikilink")[豆汁儿](../Page/豆汁儿.md "wikilink")。

然而，中華式小吃發展至今，在部份地區已經有了另外一種的意涵。尤其是台灣的小吃，雖然一樣是講究採用當地新鮮的食材，但是製作方法繁複、作工講究，比講究填飽肚子的主餐更為繁瑣，是追求利潤的餐廳不願意販賣的，例如[宜蘭的](../Page/宜蘭縣.md "wikilink")[糕渣](../Page/糕渣.md "wikilink")。這種小吃，已經是一種在地的飲食文化，絕非只是在三餐之間填飽肚子，追求不餓肚子的層次。

## 对生活的影响

小吃通常不是每个家庭的正餐，人们往往需要到市场摊贩或者小吃店中才能买到小吃。近代以来的[中国工业化进程](../Page/中国工业史.md "wikilink")，促使[工人阶级](../Page/工人.md "wikilink")、市民阶层不断壮大。他们吃[早餐的需求](../Page/早餐.md "wikilink")，亦使原来商品化程度较低的民间小吃进入餐饮市场\[1\]。餐饮业的竞争，使得各地从业者互相学习，推陈出新\[2\]。在我们物质生活越来越发达的今天，人们对小吃的热爱已成为生活当中不可分割的一部分。随着社会经济的发展，各个地区的特色小吃也走出了地方，小吃的群体也越来越多，几乎每个城市都有自己特色的小吃，形成了特殊的小吃经济群体，提升了饮食、经济、生活上的质量。中华小吃经过若干年的发展，这些小吃群体和其他饮食文化一起组成了今天的中华饮食文化。

## 中国九大小吃

  - [南京小吃](../Page/南京小吃.md "wikilink")
  - [苏州小吃](../Page/苏州小吃.md "wikilink")
  - [上海小吃](../Page/上海小吃.md "wikilink")
  - [长沙小吃](../Page/长沙小吃.md "wikilink")
  - [北京小吃](../Page/北京小吃.md "wikilink")
  - [成都小吃](../Page/成都小吃.md "wikilink")
  - [开封小吃](../Page/开封小吃.md "wikilink")
  - [岭南小吃](../Page/岭南小吃.md "wikilink")
  - [西安小吃](../Page/西安小吃.md "wikilink")

## 中国四大小吃群

[南京](../Page/南京.md "wikilink")[夫子庙小吃](../Page/夫子庙.md "wikilink")、[苏州](../Page/苏州.md "wikilink")[玄妙观小吃](../Page/玄妙观.md "wikilink")、[上海](../Page/上海.md "wikilink")[城隍庙小吃和](../Page/城隍庙.md "wikilink")[湖南](../Page/湖南.md "wikilink")[长沙](../Page/长沙.md "wikilink")[火宫殿小吃](../Page/火宫殿.md "wikilink")。

### 南京夫子庙小吃

南京夫子庙小吃群位于繁荣的秦淮河畔。这里小食摊贩经营的油炸干、豆腐脑、五香回卤干、五香茶叶蛋、乌龟子、酥烧饼、小笼包饺、千层油糕、多种浇头的各式汤面等品种，价廉物美，尤以配套装笼的什色点心最受消费者欢迎。

代表有：

  - 牛肉锅贴
  - 小煮面
  - 盐水鸭
  - 烤鸭
  - 豆腐涝
  - 什锦菜包
  - 鸭油烧饼
  - 甜豆沙包
  - 鸡汁回卤干
  - 鸭血粉丝汤
  - 牛肉粉丝汤
  - 小笼包饺
  - 赤豆元宵
  - 糖芋苗
  - 卤鸭肫干
  - 小笼包
  - 炒螺蛳
  - 五香蛋
  - 凉粉

### 苏州玄妙观小吃

苏州玄妙观小吃历史悠久，小吃文化丰富多样。玄妙观小吃群位于苏州闹市中心观前街，集姑苏点心、小吃于一市，著名的有五芳斋的五香排骨、升美斋的鸡鸭血汤、小有天的藕粉圆子、炸酥豆糖粥等，此外还有千张包子、观振兴面馆的各种苏式面条、净素菜包子等等；此外还有供人们茶余酒后闲吃的品种：盐金花菜、腌黄连头、去皮油氽果玉、油氽黄豆、酱螺蛳、油氽臭豆腐、油氽粢饭糕、烘山芋、油三角粽等，均是价廉物美，具有浓郁江南风味的小吃。

代表有：

  - 梅花糕/海棠糕；
  - 糍饭团；
  - 萝卜丝饼；
  - 三角包；
  - 虾籽鱼；
  - 玫瑰瓜子；
  - [陆长兴香菇面](../Page/陆长兴.md "wikilink")；
  - [朱鸿兴闷肉面](../Page/朱鸿兴.md "wikilink")；
  - [松鹤楼卤鸭面](../Page/松鹤楼.md "wikilink")；
  - 担子糖粥、八宝粥；
  - 蜜汁豆腐干；
  - 松仁[粽子糖](../Page/粽子糖.md "wikilink")；
  - 藏书羊肉；
  - [黄天源糕团](../Page/黄天源.md "wikilink")、猪油咸糕；
  - 震源[生煎](../Page/生煎.md "wikilink")、哑巴生煎、山塘黄天源生煎；
  - [义昌福包子](../Page/义昌福.md "wikilink")、南园包子；
  - 绿扬馄饨、绿扬蟹黄小笼包；
  - [枣泥麻饼](../Page/枣泥麻饼.md "wikilink")；
  - 茉莉花茶、[碧螺春茶](../Page/碧螺春.md "wikilink")；
  - 桂花酒酿；
  - [陆稿荐酱汁肉](../Page/陆稿荐.md "wikilink")（始与康熙年间唯一老字号以“陆稿荐”为注册商标的）；
  - 一品香卤菜。

### 上海城隍庙小吃

城隍庙小吃是上海小吃的重要组成部分。形成于清末民初，地处上海旧城商业中心。其著名小吃有[南翔馒头店的](../Page/南翔馒头店.md "wikilink")[南翔小笼馒头](../Page/小籠饅頭.md "wikilink")，满园春的百果酒酿圆子、八宝饭、甜酒酿，湖滨点心店的重油酥饼，绿波廊餐厅的枣泥酥饼、三丝眉毛酥。此外还有许多特色小吃如：面筋百叶、糟田螺、汆鱿鱼等。最为消费者喜爱的，莫过于是：汤包、百叶、油面筋。这是人们最青睐的“三主件”。此外还有[生煎馒头](../Page/生煎馒头.md "wikilink")、南翔小笼、三鲜小馄饨、海鲜馄饨、蟹壳黄等。

### 长沙火宫殿小吃

长沙火宫殿小吃群始建于1747年，1941年重建，集中湖南各地风味小吃于市，具有浓郁的地方风味，其特色小吃有姜二爹的臭豆腐，张桂生的馓子，李子泉的神仙钵饭，胡桂英的猪血，邓春香的红烧蹄花，罗三的米粉及三角豆腐、牛角蒸饺等，共300余个品种。目前火宫殿小吃在继承传统的基础上，开发新品，形成系列。

主要品种有糯米粽子、麻仁奶糖、浏阳茴饼、浏阳豆豉、湘宾春卷等。

## 南京小吃

[南京小吃](../Page/南京小吃.md "wikilink")，历史悠久，品种繁多，自六朝时期流传至今，多达80多个品种。名点小吃有荤有素，甜咸俱有，形态各异，其中代表是秦淮河夫子庙地区，其风味小吃是中国四大小吃群之一。
夫子庙的点心小吃许是缘于当年的秦淮画舫，手工精细，造型美观，选料考究，风味独特。经过多年的努力，其中七家点心店制作的小吃，经专家鉴定，将八套秦淮风味名点小吃正式命名为“秦淮八绝”。前国家副主席荣毅仁在夫子庙品尝秦淮风味小吃后，题写横幅：“小吃好吃”，亦作“吃好吃小”。

### 秦淮八绝

第一绝：永和园的黄桥烧饼和开洋干丝

第二绝：蒋有记的牛肉汤和牛肉锅贴

第三绝：六凤居的豆腐涝和葱油饼

第四绝：奇芳阁的鸭油酥烧饼和什锦菜包

第五绝：奇芳阁的麻油素干丝和鸡丝浇面

第六绝：莲湖糕团店的桂花夹心小元宵和五色小糕

第七绝：瞻园面馆熏鱼银丝面和薄皮包饺

第八绝：魁光阁的五香豆和五香蛋

### 特色小吃

  - [鸭血汤](../Page/鸭血汤.md "wikilink")
  - [旺鸡蛋](../Page/旺鸡蛋.md "wikilink")
  - [盐水鸭](../Page/盐水鸭.md "wikilink")
  - [煮干丝](../Page/煮干丝.md "wikilink")
  - [凉粉](../Page/凉粉_\(北方\).md "wikilink")
  - [状元豆](../Page/状元豆.md "wikilink")/[五香蛋](../Page/五香蛋.md "wikilink")
  - [如意回卤干](../Page/如意回卤干.md "wikilink")
  - [小笼馒头](../Page/小笼馒头.md "wikilink")
  - [牛肉锅贴](../Page/牛肉锅贴.md "wikilink")
  - [什锦豆腐涝](../Page/什锦豆腐涝.md "wikilink")
  - [蒸饺](../Page/蒸饺.md "wikilink")
  - [糯米藕](../Page/糯米藕.md "wikilink")
  - [桂花糖芋苗](../Page/桂花糖芋苗.md "wikilink")
  - [五香鹌鹑蛋](../Page/五香鹌鹑蛋.md "wikilink")
  - [牛肉粉丝汤](../Page/牛肉粉丝汤.md "wikilink")
  - [炒螺丝](../Page/炒螺丝.md "wikilink")

## 北京小吃

[Noodles_With_Sesame_Sauce_(麻醬麵).jpg](https://zh.wikipedia.org/wiki/File:Noodles_With_Sesame_Sauce_\(麻醬麵\).jpg "fig:Noodles_With_Sesame_Sauce_(麻醬麵).jpg")
中國北方的小吃講求制作需时短或可以比较长时间储存，随吃随取，不必像烹调主餐那麽费事。

北京小吃俗稱**碰頭食**或**菜茶**、**茶食**，是北京人津津乐道的一项北京特色。北京除了故宫、颐和园这样的人文建筑，大抵最可以吸引人的就是这北京小吃。延续至今的北京小吃有些已经流传有近千年的历史。由于北京作为都城，先后有不同民族的统治者，所以北京小吃又融入了汉、回、满各族特色以及沿承的宫廷风味特色。在小吃烹调方式上更是煎炒烹炸烤涮烙样样齐全。舒乙以四个字概括北京小吃：“小吃大义”。

北京小吃可以参考：[豌豆黄](../Page/豌豆黄.md "wikilink")、[豆汁](../Page/豆汁.md "wikilink")、[焦圈](../Page/焦圈.md "wikilink")、[爆肚](../Page/爆肚.md "wikilink")、[驴打滚](../Page/驴打滚.md "wikilink")、[艾窝窝](../Page/艾窝窝.md "wikilink")、[炒肝](../Page/炒肝.md "wikilink")、[炸灌肠](../Page/炸灌肠.md "wikilink")、[白水羊头](../Page/白水羊头.md "wikilink")、[茶汤](../Page/茶汤_\(小吃\).md "wikilink")、[它似蜜](../Page/它似蜜.md "wikilink")、[萨其马](../Page/萨其马.md "wikilink")……

过去经营小吃的基本都是家族单传，各有独特的风味，字号也是食品加姓的命名方式，例如爆肚冯、羊头马、年糕杨、奶酪魏等等。但建国后这些个体手工艺者无法在社会主义环境下生存，基本公私合营或兼并而消亡。目前经营小吃风味比较正统的是[南来顺](../Page/南来顺.md "wikilink")、[护国寺小吃店](../Page/护国寺小吃店.md "wikilink")、北海[仿膳饭庄](../Page/仿膳.md "wikilink")、牛街清真小吃超市、锦芳小吃店等。另后海附近的九门小吃将各品牌传统北京小吃聚合在一地，使食客不必东奔西跑即可品尝到多种正宗北京风味小吃。

关于北京小吃的参考书目：《燕都小食品杂咏》、《故都食物杂咏》

### 北京小吃列表

  - [爆肚](../Page/爆肚.md "wikilink")
  - [卤煮火烧](../Page/卤煮火烧.md "wikilink")
  - [白水羊头](../Page/白水羊头.md "wikilink")
  - [褡裢火烧](../Page/褡裢火烧.md "wikilink")
  - [炒疙瘩](../Page/炒疙瘩.md "wikilink")
  - [麻豆腐](../Page/麻豆腐.md "wikilink")
  - [茶汤](../Page/茶汤_\(小吃\).md "wikilink")
  - [宫廷奶酪](../Page/宫廷奶酪.md "wikilink")
  - [炒肝](../Page/炒肝.md "wikilink")
  - [炒红果](../Page/炒红果.md "wikilink")
  - [豆腐脑](../Page/豆腐脑.md "wikilink")
  - [豆汁](../Page/豆汁.md "wikilink")
  - [焦圈](../Page/焦圈.md "wikilink")
  - [驴打滚](../Page/驴打滚.md "wikilink")
  - [萨其马](../Page/萨其马.md "wikilink")
  - [艾窝窝](../Page/艾窝窝.md "wikilink")
  - [豌豆黄](../Page/豌豆黄.md "wikilink")
  - [芸豆卷](../Page/芸豆卷.md "wikilink")
  - [绿豆糕](../Page/绿豆糕.md "wikilink")
  - [茯苓夹饼](../Page/茯苓饼.md "wikilink")
  - [冰糖葫芦](../Page/冰糖葫芦.md "wikilink")
  - [山楂糕](../Page/山楂糕.md "wikilink")
  - [肉末烧饼](../Page/肉末烧饼.md "wikilink")
  - [酱牛肉](../Page/酱牛肉.md "wikilink")
  - [羊杂碎](../Page/羊杂碎.md "wikilink")
  - [炸灌腸](../Page/炸灌腸.md "wikilink")
  - [面茶](../Page/面茶.md "wikilink")
  - [它似蜜](../Page/它似蜜.md "wikilink")
  - [杏仁豆腐](../Page/杏仁豆腐.md "wikilink")
  - [北京油餅](../Page/中式面饼#北京油餅.md "wikilink")
  - [豆餡燒餅](../Page/中式面饼#豆餡燒餅.md "wikilink")
  - [麻醬麵](../Page/麻醬麵.md "wikilink")

## 成都小吃

成都小吃是中国[成都市各类具有浓郁地方特色的小吃的总称](../Page/成都.md "wikilink")。成都小吃历史悠久、品种繁多，同川菜一样，在中国饮食文化中，占有相当重要的地位。成都小吃这个包括了众多的各种面点、羹汤、肉食。受到[川菜的影响](../Page/川菜.md "wikilink")，成都小吃也以口味众多而闻名，常用的口味有香甜、咸甜、椒麻、[红油](../Page/红油.md "wikilink")、怪味、家常、糖醋、蒜泥等十余种。有些时候，成都小吃也可以用来指代“成都名小吃”，即一些拥有悠久历史，以经营小吃为主的知名餐厅。

成都小吃主要有：[蛋烘糕](../Page/蛋烘糕.md "wikilink")、玻璃烧麦、[担担面](../Page/担担面.md "wikilink")、鸡丝凉面、[宜宾燃面](../Page/宜宾燃面.md "wikilink")、灯影牛肉、小笼粉蒸牛肉、[钵钵鸡](../Page/钵钵鸡.md "wikilink")、肥肠粉、川味凉面、川北凉粉、冰粉

## 开封小吃

开封的小吃是中原地区的代表，比如开封小笼灌汤包、马豫兴桶子鸡、三线鲜莲花酥、五香兔肉、开封套四宝、鲤鱼焙面、烩面、红薯泥、花生糕、朱仙镇豆腐丝等，具有开封特色。著名地區有鼓楼广场、西司广场夜市。

## 臺灣小吃

臺灣小吃以[臺南一地最有名氣](../Page/臺南市.md "wikilink")。從明鄭時期留下的[閩南口味](../Page/閩南.md "wikilink")，中間又融合了[日治時代的](../Page/台灣日治時期.md "wikilink")[日本料理](../Page/日本料理.md "wikilink")，到1949年大量中國大陆各省[移民進入台灣](../Page/移民.md "wikilink")，同時也帶來了他們的家鄉口味，這些各地傳統的口味到台灣之後與台灣舊有的飲食文化互相觀摩影響，又融合出了新的獨特口味。[經濟的蓬勃發展](../Page/經濟.md "wikilink")，也使得人們不僅滿足於吃飽，更進一步地追求不同的飲食口味變化。因此，在台灣，小吃並不只是三餐之間的填充物，反而是可以成為正餐的一種飲食。

## 注释

## 参看

  - [小吃](../Page/小吃.md "wikilink")
  - [台灣小吃](../Page/台灣小吃.md "wikilink")
  - [香港街頭小吃](../Page/香港街頭小吃.md "wikilink")
  - [小吃列表](../Page/小吃列表.md "wikilink")

[小吃](../Category/小吃.md "wikilink")
[Category:中華文化](../Category/中華文化.md "wikilink")

1.
2.