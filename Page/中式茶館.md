[Shanghai-Huxinting_Tea_House.jpg](https://zh.wikipedia.org/wiki/File:Shanghai-Huxinting_Tea_House.jpg "fig:Shanghai-Huxinting_Tea_House.jpg")[豫園的茶馆](../Page/豫園.md "wikilink")\]\]

**中式茶館**指的是供人飲用[中國茶的傳統店鋪](../Page/中國茶.md "wikilink")，是[中國茶文化的重要部份](../Page/中國茶文化.md "wikilink")。除品茶外，[茶館在中國傳統中也是主要的社交](../Page/茶館.md "wikilink")、休閒場所之一。茶館在不同地区有不同的名称和风俗，有[北京茶馆](../Page/北京茶馆.md "wikilink")、[四川茶馆](../Page/四川茶馆.md "wikilink")、[天津茶社](../Page/天津茶社.md "wikilink")、[杭州茶室](../Page/杭州茶室.md "wikilink")、[廣東茶樓等等](../Page/廣東茶樓.md "wikilink")，形成了中国的茶馆文化。

## 歷史

[Qingming_Festival_Detail_15.jpg](https://zh.wikipedia.org/wiki/File:Qingming_Festival_Detail_15.jpg "fig:Qingming_Festival_Detail_15.jpg")》中的[北宋](../Page/北宋.md "wikilink")[汴京茶馆](../Page/開封.md "wikilink")\]\]
[Teahouse_drawing_in_Qing_dynasty.JPG](https://zh.wikipedia.org/wiki/File:Teahouse_drawing_in_Qing_dynasty.JPG "fig:Teahouse_drawing_in_Qing_dynasty.JPG")
根据中国古典典籍，中國最早的茶馆出现在[唐朝](../Page/唐朝.md "wikilink")[开元年间](../Page/开元.md "wikilink")（713年
- 741年），称为**茗铺**。

[宋代是茶馆最為興盛的時期](../Page/宋代.md "wikilink")，称为**茶肆**「茶坊」、「茶樓」等等。茶肆内设花架，安排奇松异槐，敲锣卖歌，招揽顾客，按不同季节卖应时茶汤；主要是給人喝茶或從事一些娛樂的場所，宋朝人去茶館消費已經成為日常生活中不可或缺的活動之一，在汴京，只要是人潮聚集的地方就會有茶館開設，而且因為宋朝商店可以營業到夜晚，此時茶坊人潮更是絡繹不絕，到了[南宋](../Page/南宋.md "wikilink")，臨安的茶坊相比於汴京更講究排場，數量和形式也增加許多，甚至鄉村也不意外，茶館也如雨後春筍般出現，還有的相當氣派，像鄂州南草市茶店就有兩層樓的店面，另外還有的是流動的茶攤、茶擔以及提瓶賣茶人，他們分佈在城市、鄉村的各個角落，茶攤會在夜市出現，方便遊客品茶，宋代的茶館也有其特色，茶館通常會雇用精通茶藝的「茶博士」來經營，是一種專業化的技術人員，也會分為各種類型，供市民子弟學習樂器及聚會的「量賣茶樓」，職業介紹所性質的「市頭」，有表演的[歌妓和陪坐侍客](../Page/歌妓.md "wikilink")[妓女的](../Page/妓女.md "wikilink")「水茶坊」、「花茶坊」，還有讀書人聚集的「大街車兒茶肆」，但其實沒有硬性規定，但這也算是品牌形象的確立，另外茶坊會依據時令不同而推出不同的產品，例如冬月就會賣七寶擂茶，還有一些茶坊會舉辦藝術文化活動，會雇用[樂妓](../Page/樂妓.md "wikilink")，是吸引人的主要手段，或是變成教唱的場所，有专门教授富家子弟的乐器班、歌唱班。有一些則是有說書人在茶坊裡說書。\[1\]

有的茶肆杭州大街上还有两三间茶楼，楼上安放[妓女](../Page/妓女.md "wikilink")，名为[花茶坊](../Page/花茶坊.md "wikilink")。

[明朝出现私家](../Page/明朝.md "wikilink")[园林](../Page/园林.md "wikilink")，有的设有私家[茶寮](../Page/茶寮.md "wikilink")；**茶馆**一词也开始出现。[張岱所著](../Page/張岱.md "wikilink")《[陶庵梦忆](../Page/陶庵梦忆.md "wikilink")·露兄》写道：“崇祯癸酉，有好事者，开茶馆”。

清代茶馆发展成为大众娱乐场所。京师茶馆，有清茶馆、大茶馆、书茶馆、酒茶馆和园林茶馆等几种。大茶馆和酒茶馆就是现在的大饭店和酒楼。清茶馆无酒，店铺里头排列着长茶案，茶客可以自己带[茶叶](../Page/茶叶.md "wikilink")，手提鸟笼，入座买水。园林茶馆多设立在北京郊区风景区如[西山](../Page/西山.md "wikilink")、[香山等处](../Page/香山_\(北京\).md "wikilink")，泡一壶上茶，欣赏满山的红叶。最有代表性的是书茶馆。客人一面饮茶一面欣赏说书先生说演的《三国》、《东周列国》、《罗通》、《包公》。后来还有[相声](../Page/相声.md "wikilink")、[梆子等](../Page/梆子.md "wikilink")。[乾隆末年江南有河滨茶馆](../Page/乾隆.md "wikilink")、卖茶、瓜子、糖果、[春卷](../Page/春卷.md "wikilink")，[烧卖](../Page/烧卖.md "wikilink")、[水饺子](../Page/水饺.md "wikilink")。上海茶馆最早出现在[同治初年](../Page/同治.md "wikilink")，“丽水台”茶楼，楼高三层。到了光绪丙子上海有了[粵式茶楼](../Page/粵式茶楼.md "wikilink")，卖茶和各种小吃，早茶卖[鱼生粥](../Page/鱼生粥.md "wikilink")，午茶有[点心](../Page/点心.md "wikilink")、[蒸粉果](../Page/粉粿.md "wikilink")；夜茶有[莲子粥](../Page/莲子粥.md "wikilink")、[杏仁露](../Page/杏仁露.md "wikilink")。

民國初年廣州一些茶樓開始聘用[女招待招徠顧客](../Page/女招待.md "wikilink")，這種設有女招待的茶室稱為[女子茶室](../Page/女子茶室.md "wikilink")，女招待又稱為[茶花](../Page/茶花_\(職業\).md "wikilink")。雖然茶花一般不提供性服務，但當時一些人仍認為這種女子茶室有傷風化。

現代除了以品茗、吃點心為主的茶館外，還有些專門為顧客提供場地進行[茶藝的茶館](../Page/中國茶藝.md "wikilink")，稱為[茶藝館](../Page/茶藝館.md "wikilink")。

2012年，[中華人民共和國商務部發佈了](../Page/中華人民共和國.md "wikilink")《茶館經營服務規範》（SB/T
10654-2012）行業標準，解決了[中國大陸茶館行業無標準可依的問題](../Page/中國大陸.md "wikilink")。

## 地方文化

### 北京茶館

北京的茶馆是北京的一个特色，[老舍先生曾经有话剧](../Page/老舍.md "wikilink")[茶馆刻画茶馆里的众生百相](../Page/茶馆.md "wikilink")。现在在北京前门西，开设了[老舍茶馆](../Page/老舍茶馆.md "wikilink")，里面有一些曲艺节目表演。

### 粵式茶樓

[广州和](../Page/广州.md "wikilink")[香港的](../Page/香港.md "wikilink")[茶楼规模很大](../Page/茶楼.md "wikilink")，有二三层楼，周末十分繁忙，常须排队拿号。茶以[香片](../Page/香片.md "wikilink")，[普洱茶为主](../Page/普洱茶.md "wikilink")、间有[菊花茶等](../Page/菊花茶.md "wikilink")。香港的茶楼点心有一百多年的历史、种类繁多、其中[虾饺](../Page/虾饺.md "wikilink")、[烧卖](../Page/烧卖.md "wikilink")、[肠粉](../Page/肠粉.md "wikilink")、[春卷](../Page/春卷.md "wikilink")、[牛百叶](../Page/牛百叶.md "wikilink")、[鸡扎](../Page/鸡.md "wikilink")、[凤爪等已成经典的粤式茶点](../Page/凤爪.md "wikilink")，风行全球。香港現時僅存小量單純提供點心的茶樓（例如[中環的](../Page/中環.md "wikilink")[陸羽茶室](../Page/陸羽茶室.md "wikilink")），絕大部份都被同時提供點心、小菜和酒席的[酒樓所取代](../Page/酒樓.md "wikilink")。

### 江南茶館

[苏州茶馆除了喝茶](../Page/苏州茶馆.md "wikilink")、品尝点心外，还可以欣赏“大书”和“小书”。大书相当于北方的[说书](../Page/说书.md "wikilink")，小书是指[苏州评弹](../Page/评弹.md "wikilink")。

### 海外中式茶館

南洋茶市，[福建人多饮](../Page/福建人.md "wikilink")[乌龙茶](../Page/乌龙茶.md "wikilink")，[广东人好饮](../Page/广东人.md "wikilink")[香片](../Page/香片.md "wikilink")，小食品有[叉烧包](../Page/叉烧包.md "wikilink")、[鸡肉大包](../Page/大包.md "wikilink")、[咖喱牛肉包等](../Page/牛肉包.md "wikilink")。

十九世纪中叶开始大批华人移民[美](../Page/美国.md "wikilink")[加](../Page/加拿大.md "wikilink")，把粤式点心带到[美洲](../Page/美洲.md "wikilink")。北美茶樓多数经营早午茶市，多名為“轩”、“园”等，以[粵式茶樓为多](../Page/粵式茶樓.md "wikilink")，近年多了沪式茶館。沪式茶館供[龙井茶](../Page/龙井茶.md "wikilink")、[葱油饼](../Page/葱油饼.md "wikilink")、[小籠饅頭](../Page/小籠饅頭.md "wikilink")。[周末或](../Page/周末.md "wikilink")[假日西人也常来](../Page/假日.md "wikilink")[享受](../Page/享乐主义.md "wikilink")，手拿[筷子](../Page/筷子.md "wikilink")，品尝[点心和](../Page/点心.md "wikilink")[中國茶](../Page/中國茶.md "wikilink")。

## 圖片

<File:Tongli> teahouse.JPG||[同里镇河滨茶馆](../Page/同里镇.md "wikilink")
<File:A> teahouse in Suzhou.jpg|蘇州江南茶馆
<File:Ciqikou3.JPG>|[重庆](../Page/重庆.md "wikilink")[磁器口茶馆](../Page/磁器口.md "wikilink")
[File:Peking-049.JPG|北京的茶館](File:Peking-049.JPG%7C北京的茶館)

## 參考資料

<div class="references-small">

  - 王笛：《茶館：成都的公共生活和微觀世界，1900-1950》（北京：社會科學文獻出版社，2009）。

  -
  -

</div>

## 相關條目

  - [中國茶文化](../Page/中國茶文化.md "wikilink")
  - [台灣茶文化](../Page/台灣茶文化.md "wikilink")
  - [飲茶](../Page/飲茶.md "wikilink")
  - [酒樓](../Page/酒樓.md "wikilink")
  - [點心](../Page/點心.md "wikilink")
  - [珍珠奶茶](../Page/珍珠奶茶.md "wikilink")
  - [茶寮](../Page/茶寮.md "wikilink")
  - [早餐](../Page/早餐.md "wikilink")

[category:中國茶史](../Page/category:中國茶史.md "wikilink")

[茶馆](../Category/茶馆.md "wikilink") [館](../Category/中國茶文化.md "wikilink")

1.