**中文电码**，又称**中文商用电码**、**中文电报码**或**中文电报明码**，原本是於[电报之中传送中文信息的方法](../Page/电报.md "wikilink")。它是第一个把汉字化作电子讯号的编码表。

自[摩尔斯电码在](../Page/摩尔斯电码.md "wikilink")1835年发明后，一直只能用来传送[英语或以](../Page/英语.md "wikilink")[拉丁字母拼写的文字](../Page/拉丁字母.md "wikilink")。1873年，法国驻华人员威基杰（S·A·Viguer）参照《[康熙字典](../Page/康熙字典.md "wikilink")》的部首排列方法，挑选了常用汉字6800多个，编成了第一部汉字电码本《电报新书》。\[1\]\[2\]后由任[上海电报局首任总办的](../Page/上海电报局.md "wikilink")[郑观应将其改编成为](../Page/郑观应.md "wikilink")《中国电报新编》。\[3\]\[4\]

中文电码表采用了四位阿拉伯数字作代号，从0001到9999按四位数顺序排列，用四位数字-{}-表示最多一万个汉字、字母和符号。汉字先按部首，后按笔划排列。字母和符号放到电码表的最尾。後來由於一萬個漢字不足以應付戶籍管理的要求，又有第二字面漢字的出現。在香港，兩個字面都採用同一編碼，由輸入員人手選擇字面；在台灣，第二字面的漢字會在開首補上“1”字，變成5個數字的編碼。

## 表示方法

一般采用[摩尔斯电码的](../Page/摩尔斯电码.md "wikilink")[短碼](../Page/摩尔斯电码#数字.md "wikilink")

### 数字（長碼版）

| 字符                                                       | 代码      | 字符                                                       | 代码      | 字符                                                       | 代码      | 字符                                                       | 代码      | 字符                                                       | 代码      |
| -------------------------------------------------------- | ------- | -------------------------------------------------------- | ------- | -------------------------------------------------------- | ------- | -------------------------------------------------------- | ------- | -------------------------------------------------------- | ------- |
| [1](../Page/Media:1_number_morse_code.ogg.md "wikilink") | `·----` | [2](../Page/Media:2_number_morse_code.ogg.md "wikilink") | `··---` | [3](../Page/Media:3_number_morse_code.ogg.md "wikilink") | `···--` | [4](../Page/Media:4_number_morse_code.ogg.md "wikilink") | `····-` | [5](../Page/Media:5_number_morse_code.ogg.md "wikilink") | `·····` |
| [6](../Page/Media:6_number_morse_code.ogg.md "wikilink") | `-····` | [7](../Page/Media:7_number_morse_code.ogg.md "wikilink") | `--···` | [8](../Page/Media:8_number_morse_code.ogg.md "wikilink") | `---··` | [9](../Page/Media:9_number_morse_code.ogg.md "wikilink") | `----·` | [0](../Page/Media:0_number_morse_code.ogg.md "wikilink") | `-----` |

### 数字（短碼版）

| 字符                                                       | 代码      | 字符                                                       | 代码      | 字符                                                       | 代码      | 字符                                                       | 代码      | 字符                                                       | 代码      |
| -------------------------------------------------------- | ------- | -------------------------------------------------------- | ------- | -------------------------------------------------------- | ------- | -------------------------------------------------------- | ------- | -------------------------------------------------------- | ------- |
| [1](../Page/Media:A_morse_code.ogg.md "wikilink")        | `·-`    | [2](../Page/Media:..-_morse_code.ogg.md "wikilink")      | `··-`   | [3](../Page/Media:...-_morse_code.ogg.md "wikilink")     | `···-`  | [4](../Page/Media:4_number_morse_code.ogg.md "wikilink") | `····-` | [5](../Page/Media:5_number_morse_code.ogg.md "wikilink") | `·····` |
| [6](../Page/Media:6_number_morse_code.ogg.md "wikilink") | `-····` | [7](../Page/Media:7_number_morse_code.ogg.md "wikilink") | `--···` | [8](../Page/Media:8_number_morse_code.ogg.md "wikilink") | `---··` | [9](../Page/Media:9_number_morse_code.ogg.md "wikilink") | `----·` | [0](../Page/Media:0_number_morse_code.ogg.md "wikilink") | `-----` |

## 应用

中文电码可用作电脑裡的[中文输入法](../Page/中文输入法.md "wikilink")，但因中文电码是“无理码”，记忆困难，一般用户几乎无法熟练地掌握使用。

在[港](../Page/香港.md "wikilink")[澳](../Page/澳門.md "wikilink")，每个有中文姓名的市民的[身份证上](../Page/身份证.md "wikilink")，均会在他的姓名下面，印有中文电码。在[香港](../Page/香港.md "wikilink")，外國人取得的入港[簽證亦有印上中文电码](../Page/簽證.md "wikilink")。在香港很多政府或商业机构的表格中，都会要求填写者填写他的中文电码，以便輸入電腦。

## 参考资料

## 参看

  - [中文输入法](../Page/中文输入法.md "wikilink")
  - [摩尔斯电码](../Page/摩尔斯电码.md "wikilink")
  - [韵目代日](../Page/韵目代日.md "wikilink")
  - [1983年《标准电码本（修订本）》的维基词典页面](../Page/wikt:附录:中文电码/中国大陆1983.md "wikilink")

## 外部連結

  - [中文编码网页](http://code.web.idv.hk/)
  - [南极星网上电码转换](http://www.njstar.com/tools/telecode/)
  - [Chinese Commercial Code 中文電碼](http://chinesecommercialcode.net/)
  - [VimIM
    中文电报输入查询](http://code.google.com/p/vimim/wiki/VimIM_Telegraph_Code)

[Category:电报](../Category/电报.md "wikilink")
[Category:字符集](../Category/字符集.md "wikilink")
[Category:中文信息处理](../Category/中文信息处理.md "wikilink")
[Category:中文输入法](../Category/中文输入法.md "wikilink")
[Category:摩斯電碼](../Category/摩斯電碼.md "wikilink")

1.
2.
3.
4.