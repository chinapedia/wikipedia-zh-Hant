在1990年代中期，为了同时照顾不同的用户群，一些输入法同时进行了形码、音形码、形音码的编码（例如认知码）。下表中的分类中，仅列入输入法的最主要变种。

## 拼音輸入法（音碼）

中文的同音字很多，所以字音輸入法的重碼率偏高，不易直接输入正确结果，需倚重輸入法軟體在智能輸入上的設計。

  - [注音输入法](../Page/注音输入法.md "wikilink")（注音）
      - 整句輸入的注音輸入法（智慧型預先猜字輸入法，智慧特性來自預先建立的字典字詞或輸入習慣學習推測的自動新建詞）
          - [自然輸入法](../Page/自然輸入法.md "wikilink") -
            [網際智慧公司](../Page/網際智慧.md "wikilink")
          - [新酷音輸入法](../Page/新酷音輸入法.md "wikilink") -
            [GPL](../Page/GPL.md "wikilink")[自由軟體](../Page/自由軟體.md "wikilink")
          - [微軟新注音輸入法](../Page/微軟新注音輸入法.md "wikilink") - 隨附於視窗作業系統內
          - [巧音輸入法](../Page/巧音輸入法.md "wikilink") -
            自由軟體（僅適用於[類UNIX作業系統上](../Page/Unix-like.md "wikilink")）
          - [忘形輸入法](../Page/忘形輸入法.md "wikilink") -
            [倚天資訊公司](../Page/倚天資訊.md "wikilink")（已停止發展）
          - [漢音輸入法](../Page/漢音輸入法.md "wikilink") -
            松下電器技術開發股份有限公司（隨附於麥金塔作業系統內，原本可望成為微軟新注音輸入法，因授權問題未達成協議而終止）
          - [一一輸入法](../Page/一一輸入法.md "wikilink")
          - [Yahoo\!奇摩輸入法](../Page/Yahoo!奇摩輸入法.md "wikilink") - 好打注音模式。
  - [汉语拼音输入法](../Page/汉语拼音输入法.md "wikilink")（主要分为[全拼](../Page/全拼.md "wikilink")、[双拼两类](../Page/双拼.md "wikilink")。另有[简拼输入法](../Page/简拼输入法.md "wikilink")。现在的拼音输入法一般可以根据用户的需要，进行相应的设置。）
      - 词语输入的拼音输入法
          - [智能ABC输入法](../Page/智能ABC输入法.md "wikilink")
          - [拼音加加输入法](../Page/拼音加加.md "wikilink")
          - [紫光拼音输入法](../Page/紫光拼音输入法.md "wikilink")
          - [新华拼音输入法](../Page/新华拼音输入法.md "wikilink")
      - 整句输入的拼音输入法
          - [搜狗拼音输入法](../Page/搜狗拼音输入法.md "wikilink")
          - [Google拼音输入法](../Page/Google拼音输入法.md "wikilink")（谷歌拼音輸入法）
          - [QQ拼音输入法](../Page/QQ拼音输入法.md "wikilink")
          - [微软拼音输入法](../Page/微软拼音输入法.md "wikilink")
          - [聰明打字输入法](../Page/聰明打字输入法.md "wikilink")
          - [黑马神拼输入法](../Page/黑马神拼.md "wikilink")
          - [智能狂拼输入法](../Page/智能狂拼.md "wikilink")
  - 南京官話拼音輸入法（南京話拼音）
      - [南京官話拼音輸入法](../Page/南京話#拉丁拼音.md "wikilink")
  - [粵語拼音輸入法](../Page/粵語拼音輸入法.md "wikilink")（粤语拼音）
      - [速打粵語拼音輸入法](../Page/速打粵語拼音輸入法.md "wikilink")
  - [上海吳語注音輸入法](../Page/上海吳語注音輸入法.md "wikilink") - GPL授權
  - [亚伟速录](../Page/亚伟速录.md "wikilink")（需专用的键盘，一般为专业速录员使用。）
  - [閩南語輸入法](../Page/閩南語輸入法.md "wikilink")
      - [臺灣閩南語漢字輸入法](../Page/臺灣閩南語漢字輸入法.md "wikilink")（臺羅、白話字）
      - [信望愛台語客語輸入法](../Page/信望愛台語客語輸入法.md "wikilink")（臺羅、白話字）
      - [吳守禮台語注音輸入法](../Page/吳守禮台語注音輸入法.md "wikilink")（臺灣方音符號）

## 字形輸入法（形碼）

  - [二筆輸入法](../Page/二筆輸入法.md "wikilink")（二笔-{全形}-版）
  - [三角編號法](../Page/三角編號法.md "wikilink")
  - [四角號碼](../Page/四角號碼.md "wikilink")
  - [五笔字型输入法](../Page/五笔字型输入法.md "wikilink")
      - [万能五笔](../Page/万能五笔.md "wikilink")
      - [极品五笔](../Page/极品五笔.md "wikilink")
      - [极点五笔](../Page/极点五笔.md "wikilink")
      - [海峰五笔](../Page/海峰五笔.md "wikilink")
  - [六一輸入法](../Page/六一輸入法.md "wikilink")
  - [九方输入法](../Page/九方输入法.md "wikilink")
  - [十易碼輸入法](../Page/十易碼輸入法.md "wikilink")
  - [形筆輸入法](../Page/形筆輸入法.md "wikilink")
  - [倉頡輸入法](../Page/倉頡輸入法.md "wikilink")
      - [速成輸入法](../Page/速成輸入法.md "wikilink")（[簡易輸入法](../Page/簡易輸入法.md "wikilink")）
          - [快意速成輸入法](../Page/快意速成輸入法.md "wikilink")
      - [蘇式倉頡輸入法](../Page/蘇式倉頡輸入法.md "wikilink")
      - [快速倉頡輸入法](../Page/快速倉頡輸入法.md "wikilink") -
        [GPLv](../Page/GPL.md "wikilink")3授權
      - [全方位倉頡輸入法](../Page/全方位倉頡輸入法.md "wikilink")
      - [新倉頡輸入法](../Page/新倉頡輸入法.md "wikilink")
      - [大新倉頡輸入法](../Page/大新倉頡輸入法.md "wikilink")
      - [輕鬆輸入法](../Page/輕鬆輸入法.md "wikilink") -
        [GPL授權](../Page/GPL.md "wikilink")
      - [Yahoo\!奇摩輸入法](../Page/Yahoo!奇摩輸入法.md "wikilink") - 支援倉頡輸入法
  - [大易輸入法](../Page/大易輸入法.md "wikilink")
  - [行列输入法](../Page/行列输入法.md "wikilink")
  - [嘸蝦米輸入法](../Page/嘸蝦米輸入法.md "wikilink")
      - [偽蝦米輸入法](../Page/偽蝦米輸入法.md "wikilink")
  - [華象直覺輸入法](../Page/華象直覺輸入法.md "wikilink")
  - [快碼输入法](../Page/快碼输入法.md "wikilink")
  - [縱橫輸入法](../Page/縱橫輸入法.md "wikilink")
  - [表形码](../Page/表形码.md "wikilink")
  - [首尾字型输入法](../Page/首尾字型输入法.md "wikilink")
  - [郑码输入法](../Page/郑码输入法.md "wikilink")
  - [山人全息码](../Page/山人全息码.md "wikilink") - 山人通用输入法
  - [徐碼输入法](../Page/徐碼输入法.md "wikilink")（支持港台人打字习惯的，繁简通打明体字输入法）

## 音形結合码（包括音形码和形音碼）

  - [二笔输入法](../Page/二笔输入法.md "wikilink")（二笔音形版）
      - 青松二笔
      - 超强快码
      - 超强二笔
      - 超强音形
      - 自由二笔
  - [自然码](../Page/自然码.md "wikilink")
  - [子来输入法](../Page/子来输入法.md "wikilink")
  - [王林快码](../Page/王林快码.md "wikilink")
  - [认知码](../Page/认知码.md "wikilink")
  - [汉码形音码](../Page/汉码形音码.md "wikilink")

## 內碼輸入法（無理碼）

這並非一般意義上的輸入法。在[中文信息處理中](../Page/中文信息處理.md "wikilink")，會先決定[字符集](../Page/字符集.md "wikilink")，並賦予每個字符一個編號或編碼，稱作[內碼](../Page/內碼.md "wikilink")。而一般的輸入法，則是以人類可以理解並記憶的方式，為每個字符編碼，稱作[外碼](../Page/外碼.md "wikilink")。內碼輸入法是指直接透過指定字符的內碼來做輸入。但因內碼並非人所能理解並記憶，且不同的字符集就會有不同的內碼，換言之，同一個字在不同字符集中會有不同的內碼，使用者需重新記憶。因此，這並非一種實際可用的輸入法。

  - [大五碼](../Page/大五碼.md "wikilink")（Big5码）
  - [倚天碼](../Page/倚天碼.md "wikilink")
  - [国标码](../Page/国家标准代码.md "wikilink")（如[GB
    2312](../Page/GB_2312.md "wikilink")、[GBK](../Page/GBK.md "wikilink")、[GB
    18030等](../Page/GB_18030.md "wikilink")）
  - [中文电码](../Page/中文电码.md "wikilink")
  - [統一碼](../Page/統一碼.md "wikilink")（Unicode）
  - [GB区位码和GB内码](../Page/区位码_\(国标\).md "wikilink")

## 聲韻輸入法

聲韻輸入法用滑鼠輸入中文。先點擊聲母，再點擊韻母；或先點擊韻母，再點擊聲母，就出現全部同聲同韻的字以供選擇。聲母韻母均用近音字提示，不必記憶。近音檢字法和粵音檢字法，是聲韻輸入法的代表。參看外部連結。

## 其它特徵

用[筆劃](../Page/筆劃.md "wikilink")、[部首](../Page/部首.md "wikilink")、[筆順](../Page/筆順.md "wikilink")

  - [筆劃輸入法](../Page/筆劃輸入法.md "wikilink")
  - [五笔画输入法](../Page/五笔画输入法.md "wikilink")
  - [六碼筆劃輸入法](../Page/六碼筆劃輸入法.md "wikilink")
  - [部首輸入法](../Page/部首輸入法.md "wikilink")
  - [佚筆、佚拼輸入法](../Page/佚筆、佚拼輸入法.md "wikilink")
  - [筆順輸入法](../Page/筆順輸入法.md "wikilink")
  - [S\&R 中文笔画智能输入法](../Page/S&R_中文笔画智能输入法.md "wikilink")

## 參看

  - [中文輸入法](../Page/中文輸入法.md "wikilink")

## 外部連結

  - [速打粵語拼音輸入法](http://www.cukda.com/ime/)

[Category:软件列表](../Category/软件列表.md "wikilink")
[Category:输入法](../Category/输入法.md "wikilink")
[中文输入法](../Category/中文输入法.md "wikilink")