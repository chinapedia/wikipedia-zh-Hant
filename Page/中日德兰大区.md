[Midtjylland_municipalities.svg](https://zh.wikipedia.org/wiki/File:Midtjylland_municipalities.svg "fig:Midtjylland_municipalities.svg")

[Map_DK_Region_Midtjylland.png](https://zh.wikipedia.org/wiki/File:Map_DK_Region_Midtjylland.png "fig:Map_DK_Region_Midtjylland.png")
**中日德蘭大區**（Region
Midtjylland）是[丹麥五大區之一](../Page/丹麥.md "wikilink")，面積13,053平方公里，2008年人口1,237,041人。首府[維堡](../Page/維堡_\(丹麥\).md "wikilink")。

2007年1月1日由原來的[靈克賓郡](../Page/靈克賓郡.md "wikilink")、[維堡郡的大部分](../Page/維堡郡.md "wikilink")、[瓦埃勒郡北部和](../Page/瓦埃勒郡.md "wikilink")[奧胡斯郡](../Page/奧胡斯郡.md "wikilink")（[瑪麗艾厄市的西部除外](../Page/瑪麗艾厄市.md "wikilink")）合併而成。下分19市。

[\*](../Category/中日德兰大区.md "wikilink")