[Evergreen_Laurel_Hotel_(Keelung)_and_ships_20080930.jpg](https://zh.wikipedia.org/wiki/File:Evergreen_Laurel_Hotel_\(Keelung\)_and_ships_20080930.jpg "fig:Evergreen_Laurel_Hotel_(Keelung)_and_ships_20080930.jpg")

**中正區**是[台灣](../Page/台灣.md "wikilink")[基隆市的一個](../Page/基隆市.md "wikilink")[區](../Page/市轄區.md "wikilink")，位於基隆[市中心東北](../Page/市中心.md "wikilink")、[基隆港東面](../Page/基隆港.md "wikilink")，為[基隆市政府所在地](../Page/基隆市政府.md "wikilink")。

## 歷史

戰後，統合[日治時代](../Page/台灣日治時期.md "wikilink")[昭和](../Page/昭和.md "wikilink")6年（1931年）[町名改正設置的](../Page/町名改正.md "wikilink")[義重町](../Page/義重町.md "wikilink")、[日新町](../Page/日新町_\(基隆市\).md "wikilink")、[入船町](../Page/入船町_\(基隆市\).md "wikilink")、[真砂町](../Page/真砂町.md "wikilink")、[濱町](../Page/濱町.md "wikilink")、[社寮町](../Page/社寮町.md "wikilink")、[八斗子](../Page/八斗子.md "wikilink")、[基隆嶼](../Page/基隆嶼.md "wikilink")、[花瓶嶼](../Page/花瓶嶼.md "wikilink")、[棉花嶼](../Page/棉花嶼.md "wikilink")、[彭佳嶼設置第一區](../Page/彭佳嶼.md "wikilink")，1946年改稱中正區。

## 行政區

  - [入船里](../Page/入船里.md "wikilink")、中砂里、正砂里、正濱里、[長潭-{里}-](../Page/長潭里.md "wikilink")、砂灣里、真砂里、義重里、碧砂里
  - 八斗里、中船里、正船里、平寮里、和憲里、[建國里](../Page/建國里_\(基隆市\).md "wikilink")、海濱里、新富里、德義里
  - [中正里](../Page/中正里_\(基隆市\).md "wikilink")、中濱里、正義里、社寮里、砂子-{里}-、信義里、港通里、新豐里

## 人口

## 教育

### 大專院校

  - [國立臺灣海洋大學](../Page/國立臺灣海洋大學.md "wikilink")

### 高級職業學校

  - [國立基隆高級海事職業學校](../Page/國立基隆高級海事職業學校.md "wikilink")

### 高級中學

  - [基隆市立八斗高級中學](../Page/基隆市立八斗高級中學.md "wikilink")
  - [基隆市私立二信高級中學](../Page/基隆市私立二信高級中學.md "wikilink")

### 國民中學

  - 基隆市立中正國民中學
  - [基隆市私立二信高級中學附設國民中學](../Page/基隆市私立二信高級中學.md "wikilink")
  - [基隆市立八斗高級中學附設國民中學](../Page/基隆市立八斗高級中學.md "wikilink")
  - 基隆市立正濱國民中學

### 國民小學

  - [基隆市中正區中正國民小學](http://www.ccps.kl.edu.tw/)
  - [基隆市中正區正濱國民小學](http://ms1.cbps.kl.edu.tw/)
  - [基隆市中正區忠孝國民小學](http://www.csps.kl.edu.tw/)
  - [基隆市中正區八斗國民小學](http://www.bdps.kl.edu.tw/)
  - [基隆市中正區和平國民小學](http://www.hpps.kl.edu.tw/)

## 交通

### 鐵路

  - [臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")

<!-- end list -->

  - [深澳線](../Page/深澳線.md "wikilink")：[海科館車站](../Page/海科館車站.md "wikilink")
    - [八斗子車站](../Page/八斗子車站.md "wikilink")

### 公路

  - [TW_PHW2.svg](https://zh.wikipedia.org/wiki/File:TW_PHW2.svg "fig:TW_PHW2.svg")[台2線](../Page/台2線.md "wikilink")：[北部濱海公路](../Page/北部濱海公路.md "wikilink")
  - [TW_PHW62a.svg](https://zh.wikipedia.org/wiki/File:TW_PHW62a.svg "fig:TW_PHW62a.svg")[台62甲線](../Page/台62線#甲線.md "wikilink")
      - [基隆端](../Page/基隆端.md "wikilink")
  - [TW_CHW102.svg](https://zh.wikipedia.org/wiki/File:TW_CHW102.svg "fig:TW_CHW102.svg")[市道102號](../Page/市道102號.md "wikilink")
  - [TW_THWtp123.png](https://zh.wikipedia.org/wiki/File:TW_THWtp123.png "fig:TW_THWtp123.png")[北123線](../Page/北123線.md "wikilink")（調和街、基瑞公路）

## 旅遊

  - [基隆文化中心](../Page/基隆文化中心.md "wikilink")(含基隆故事館)
  - [中正公園](../Page/中正公園_\(基隆市\).md "wikilink")
  - [國立海洋科技博物館](../Page/國立海洋科技博物館.md "wikilink")
  - [望幽谷](../Page/望幽谷.md "wikilink")
  - \[[https://www.facebook.com/kcich/\]基隆市原住民文化會館](https://www.facebook.com/kcich/%5D基隆市原住民文化會館)
  - [八-{斗}-子濱海公園](../Page/八斗子.md "wikilink")
  - 望海巷
  - [碧砂漁港](../Page/碧砂漁港.md "wikilink")
  - [海門天險](../Page/二沙灣砲臺.md "wikilink")(基隆二沙灣砲台)
  - [和平島天顯宮](http://www.tian-xian-gong.org.tw)
  - [正濱漁港](../Page/正濱漁港.md "wikilink")
  - [社寮天后宮](../Page/社寮天后宮.md "wikilink")（又稱和平島天后宮）
  - [和平島海濱公園](../Page/和平島.md "wikilink")
  - 主普壇(中元祭祀文物館)
  - 放水燈
  - [基隆嶼](../Page/基隆嶼.md "wikilink")

## 外部連結

  - [基隆市中正區公所](http://www.klzz.gov.tw/)

{{-}}

[category:基隆市行政區劃](../Page/category:基隆市行政區劃.md "wikilink")
[分類:以國家元首命名的行政區](../Page/分類:以國家元首命名的行政區.md "wikilink")

[中正區_(基隆市)](../Category/中正區_\(基隆市\).md "wikilink")