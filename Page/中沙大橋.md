**中沙大橋**橫跨[台灣](../Page/台灣.md "wikilink")[彰化縣](../Page/彰化縣.md "wikilink")[溪州鄉與](../Page/溪州鄉.md "wikilink")[雲林縣](../Page/雲林縣.md "wikilink")[西螺鎮](../Page/西螺鎮.md "wikilink")，全長跨越2,345公尺，橋下跨越的為[濁水溪上游三公里處](../Page/濁水溪.md "wikilink")。1978年建成時為台灣第一長橋，至2004年才由[福爾摩沙高速公路自](../Page/福爾摩沙高速公路.md "wikilink")[龍井交流道南至](../Page/龍井交流道.md "wikilink")[烏日交流道北的高架橋](../Page/烏日交流道.md "wikilink")（約25公里）所取代。

## 名稱

原訂名**濁水溪橋**，現名「中沙」二字的「中」是指[中華民國](../Page/中華民國.md "wikilink")，而「沙」則是指中華民國當時的邦交國[沙烏地阿拉伯王國](../Page/沙烏地阿拉伯王國.md "wikilink")，由於沙烏地阿拉伯以無息貸款予中華民國三千萬美元，使此橋得以興建，因此完工後奉行政院核定命名為中沙大橋來表彰紀念\[1\]\[2\]\[3\]\[4\]，並在南邊的[西螺服務區南站設有時任](../Page/西螺服務區.md "wikilink")[行政院長](../Page/行政院長.md "wikilink")[孫運璿題字](../Page/孫運璿.md "wikilink")「友誼永存」紀念碑。

## 工程、歷史、近況

中沙大橋於1976年4月開工，該橋本身也為[中山高速公路上的一環](../Page/中山高速公路.md "wikilink")，同時也是該路興建施工時最艱難的一段，也因如此，中山高速公路全線完工後的通車典禮也選擇在此橋舉辦，橋與整段高速公路一同完工（1978年10月31日）、一同正式啟用。

中沙大橋的橋樑是使用[預力空心基椿及](../Page/預力空心基椿.md "wikilink")[預力樑的設計手法](../Page/預力樑.md "wikilink")，但該橋完工已逾40年，1999年[九二一大地震後進行防震補強工程](../Page/九二一大地震.md "wikilink")，2007年隨中山高速公路員林至高雄路段擴寬工程後也完成了拓寬與改建工作。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:1978年完工橋梁](../Category/1978年完工橋梁.md "wikilink")
[Category:十大建設](../Category/十大建設.md "wikilink")
[Category:彰化縣橋梁](../Category/彰化縣橋梁.md "wikilink")
[Category:雲林縣橋梁](../Category/雲林縣橋梁.md "wikilink")
[Category:濁水溪橋樑](../Category/濁水溪橋樑.md "wikilink")
[Category:溪州鄉](../Category/溪州鄉.md "wikilink")
[Category:西螺鎮](../Category/西螺鎮.md "wikilink")
[Category:台灣公路橋](../Category/台灣公路橋.md "wikilink")
[Category:梁桥](../Category/梁桥.md "wikilink")
[Category:中華民國與沙烏地阿拉伯關係](../Category/中華民國與沙烏地阿拉伯關係.md "wikilink")
[Category:國道一號 (中華民國)](../Category/國道一號_\(中華民國\).md "wikilink")

1.
2.
3.  【白嘉莉傳奇】《大社會·會大咖》東森傳播 2009.10.02播出
4.  《[臺灣區國道高速公路全線通車紀念](http://www.freeway.gov.tw/NorthArchives//Upload/201503/%E8%87%BA%E7%81%A3%E5%8D%80%E5%9C%8B%E9%81%93%E9%AB%98%E9%80%9F%E5%85%AC%E8%B7%AF%E5%85%A8%E7%B7%9A%E9%80%9A%E8%BB%8A%E7%B4%80%E5%BF%B5.pdf)》，交通部臺灣區國道高速公路工程局，中華民國67年10月31日