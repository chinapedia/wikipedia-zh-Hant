| image_flag = {{\#property:p41}} | image_alt = | image_caption =
Clockwise, from top left : [婆羅浮屠](../Page/婆羅浮屠.md "wikilink"),
[Mangkunegaran Palace](../Page/Mangkunegaran_Palace.md "wikilink"),
Village in the [Dieng Plateau](../Page/Dieng_Plateau.md "wikilink"),
[Serayu River](../Page/Serayu_River.md "wikilink"),
[Karimunjava](../Page/Karimunjava.md "wikilink"), Fishermen on
[拉瓦珀寧湖](../Page/拉瓦珀寧湖.md "wikilink"), Rice paddy with
[默拉皮火山](../Page/默拉皮火山.md "wikilink") and
[默巴布火山](../Page/默巴布火山.md "wikilink") in the
background | translit_lang1 = 其他 | nickname = | motto =
ꦥꦿꦱꦺꦠꦾꦈꦭꦃꦱꦏ꧀ꦠꦶꦨꦏ꧀ꦠꦶꦥꦿꦗ

Prasetya Ulah Sakti Bhakti Praja
([爪哇語](../Page/爪哇語.md "wikilink"))
(meaning: *A vow of devotion with all might to the country*) |
image_map = Central Java in Indonesia.svg | map_alt = | map_caption =
| coordinates =  | coor_pinpoint = | coordinates_footnotes = |
subdivision_type = [国家](../Page/世界政區索引.md "wikilink") |
subdivision_name =  | established_title = 建立 | established_date
=1950年8月15日 | founder = | named_for = | seat_type =
[首府](../Page/印度尼西亚省份列表.md "wikilink") | seat =
[Lambang_Kota_Semarang.png](https://zh.wikipedia.org/wiki/File:Lambang_Kota_Semarang.png "fig:Lambang_Kota_Semarang.png")
[三寶瓏](../Page/三寶瓏.md "wikilink") | government_footnotes = |
governing_body = Central Java Regional Government | leader_party =
[PDI-P](../Page/印度尼西亚斗争派民主党.md "wikilink") | leader_title = 首长 |
leader_name = [甘查爾·普拉諾沃](../Page/甘查爾·普拉諾沃.md "wikilink") | unit_pref =
Metric | area_footnotes = | area_total_km2 = 32800.69 | area_note =
| elevation_footnotes = | elevation_m = | population_total = 34257900
| population_as_of = 2017 | population_footnotes = \[1\] |
population_density_km2 = auto | population_rank = 第3名 |
population_note = | demographics_type1 = 人口 | demographics1_footnotes
= | demographics1_title1 = [族群](../Page/族群.md "wikilink") | timezone1 =
[印度尼西亚标准时间](../Page/印度尼西亚标准时间.md "wikilink") | utc_offset1 = +7 |
postal_code_type = 邮政编码 | postal_code = 50xxx, 51xxx, 52xxx |
area_code = (62)2xx | area_code_type = 电话区号 | geocode = | iso_code =
ID-JT | registration_plate = [AA, AD, K, G, H,
R](../Page/Vehicle_registration_plates_of_Indonesia.md "wikilink") |
blank_name_sec1 = 人均GRP | blank_info_sec1 = US$ 2,326 |
blank1_name_sec1 = GRP rank | blank1_info_sec1 =
[25th](../Page/印度尼西亚各省人均地区生产总值列表.md "wikilink") |
blank_name_sec2 = Largest city by area | blank_info_sec2 = Semarang
–  | blank1_name_sec2 = Largest city by population |
blank1_info_sec2 = Semarang – (1,555,984 – 2010) | website =  |
footnotes = | type = 省 | leader_title2 = 副首长 | leader_name2 = [Taj
Yasin Maimoen](../Page/Taj_Yasin_Maimoen.md "wikilink") |
elevation_max_m = 3428 | demographics1_info1 =
[爪哇族](../Page/爪哇族.md "wikilink") (98%),
[華人](../Page/印度尼西亞華人.md "wikilink") (1%)
[印度人](../Page/印度人.md "wikilink") (0.5%) | demographics1_title2 =
[宗教](../Page/宗教.md "wikilink") | demographics1_info2 =
[伊斯兰教](../Page/伊斯兰教.md "wikilink") 95.74%,
[基督教](../Page/基督教.md "wikilink") 4.95%,
[印度教](../Page/印度教.md "wikilink") 0.05%,
[佛教](../Page/佛教.md "wikilink") 0.22%,
[儒家](../Page/儒家.md "wikilink") 0.03%, and
[Kejawen](../Page/Kejawen.md "wikilink") | translit_lang1_type1 =
[爪哇语](../Page/爪哇字母.md "wikilink") | translit_lang1_info1 =
ꦗꦮꦠꦼꦔꦃ | demographics1_title3 =
[語言](../Page/語言.md "wikilink") | demographics1_info3 =
[印尼语](../Page/印尼语.md "wikilink") (official)
[爪哇語](../Page/爪哇語.md "wikilink") (native) | registration_plate_type =
[Vehicle
sign](../Page/Vehicle_registration_plates_of_Indonesia.md "wikilink") |
blank2_name_sec1 = [HDI](../Page/人类发展指数.md "wikilink") |
blank2_info_sec1 =  0.700() | blank3_name_sec1 = HDI排名 |
blank3_info_sec1 = [12th](../Page/印度尼西亚各省人类发展指数列表.md "wikilink") |
blank2_name_sec2 = Largest regency by area | blank2_info_sec2 =
[芝拉扎县](../Page/芝拉扎县.md "wikilink") –  | blank3_name_sec2 =
Largest regency by population | blank3_info_sec2 = [Brebes
Regency](../Page/Brebes_Regency.md "wikilink") – (1,733,869 – 2010) }}
**中爪哇省**（[印尼語](../Page/印尼語.md "wikilink")：）是[印尼的一個](../Page/印尼.md "wikilink")[省](../Page/印度尼西亚行政区划.md "wikilink")，位於[爪哇島的中部](../Page/爪哇島.md "wikilink")，首府是[三寶瓏](../Page/三寶瓏.md "wikilink")。現任省長是[甘查爾·普拉諾沃](../Page/甘查爾·普拉諾沃.md "wikilink")，由副省長代理。

## 地理及人口

中爪哇省位於[東爪哇省及](../Page/東爪哇省.md "wikilink")[西爪哇省之間](../Page/西爪哇省.md "wikilink")，北面是[爪哇海](../Page/爪哇海.md "wikilink")，南面是[印度洋](../Page/印度洋.md "wikilink")，面積有40,548平方公里。無論是文化上或歷史上，位於其南部的[日惹市](../Page/日惹.md "wikilink")（）都屬於中爪哇區，但現時是印尼的一個獨立政治個體。

中爪哇有部份區域，如[梭羅](../Page/梭羅市.md "wikilink")（）、[班尤馬](../Page/班尤馬.md "wikilink")（）等，都有其獨特的歷史、文化、語言及傳統。

根據2000年的人口統計，中爪哇有人口30,851,144人，是印尼人口第3高的省份。（2010年）人口普查，有3238万人。

另外，位於東北部的[穆里亞半島](../Page/穆里亞半島.md "wikilink")（Muria）將會興建[核電廠](../Page/核電廠.md "wikilink")。西南部的[努薩安邦島有多所高度設防監獄](../Page/努薩安邦島.md "wikilink")。

## 市鎮列表

[administrative_districts_central_java_zh.png](https://zh.wikipedia.org/wiki/File:administrative_districts_central_java_zh.png "fig:administrative_districts_central_java_zh.png")
中爪哇省有29個縣、7個縣級市，茲詳列如下：

### 市

[马格朗市](../Page/马格朗.md "wikilink")
(Magelang)、[北加浪岸市](../Page/北加浪岸.md "wikilink")
(Pekalongan)、[普禾格多](../Page/普禾格多.md "wikilink")
(Purwokerto)、[沙拉迪加](../Page/沙拉迪加.md "wikilink")
(Salatiga)、[三寶瓏](../Page/三寶瓏.md "wikilink")
(Semarang)、[梭羅](../Page/梭羅市.md "wikilink")（Surakarta，又譯「蘇拉加達」）、[直葛市](../Page/直葛.md "wikilink")
(Tegal)

### 縣

  - [安巴拉瓦](../Page/安巴拉瓦.md "wikilink") (Ambarawa)

  - (Banjar Negara)、[班尤馬](../Page/班尤馬.md "wikilink")
    (Banyumas)、[峇登](../Page/峇登.md "wikilink")
    (Batang)、[布勞拉](../Page/布勞拉.md "wikilink")
    (Blora)、[博約拉利](../Page/博約拉利.md "wikilink")（Boyolali，又譯「波約拉利」）、[勿里碧](../Page/勿里碧.md "wikilink")（Brebes，又譯「貝雷貝斯」）

  - [芝拉扎市](../Page/芝拉扎.md "wikilink") (Cilacap)

  - [淡目](../Page/淡目.md "wikilink") (Demak)

  - [哥波安](../Page/哥波安.md "wikilink")（Grobogan，又譯[克羅坡安](../Page/克羅坡安.md "wikilink")、格羅波干）

  - [傑柏拉](../Page/傑柏拉.md "wikilink") (Jepara)

  - [Kajen](../Page/Kajen.md "wikilink")，[卡朗安雅](../Page/卡朗安雅.md "wikilink")（Karang
    Anyar，又譯「卡朗安耶」）, (Kebumen), [肯德爾](../Page/肯德爾.md "wikilink")
    (Kendal), [克拉登](../Page/克拉登.md "wikilink")
    (Klaten)、[古突士](../Page/古突士.md "wikilink") (Kudus)

  - [Mungkid](../Page/Mungkid.md "wikilink")

  - [巴蒂](../Page/巴蒂.md "wikilink")
    (Pati)、[Pemalang](../Page/Pemalang.md "wikilink")，[Purbalingga](../Page/Purbalingga.md "wikilink")，[普沃達迪](../Page/普沃達迪.md "wikilink")
    (Purwodadi)、[普沃勒佐](../Page/普沃勒佐.md "wikilink") (Purworejo)

  - [南旺](../Page/南旺.md "wikilink") (Rembang)

  - [斯萊曼](../Page/斯萊曼.md "wikilink")
    (Slawi)、[Sragen](../Page/Sragen.md "wikilink")、[Sukoharjo](../Page/Sukoharjo.md "wikilink")

  - [淡滿光](../Page/淡滿光.md "wikilink") (Temanggung)

  - [Wonogiri](../Page/Wonogiri.md "wikilink")，[禾諾波](../Page/禾諾波.md "wikilink")
    (Wonosobo)

## 知名人物

  - 前[總統](../Page/印尼總統.md "wikilink")[蘇哈托](../Page/蘇哈托.md "wikilink")

## 旅遊

  - [婆羅浮屠](../Page/婆羅浮屠.md "wikilink")
  - [沙朗安湖](../Page/沙朗安湖.md "wikilink")

## 参考文献

## 参见

  - [夏连特拉王朝](../Page/夏连特拉王朝.md "wikilink")
  - [潘迪亞](../Page/潘迪亞.md "wikilink")

{{-}}

[中爪哇省](../Category/中爪哇省.md "wikilink")
[Category:爪哇島](../Category/爪哇島.md "wikilink")

1.