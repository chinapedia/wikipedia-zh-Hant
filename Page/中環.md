[Hong_Kong_Central_District_Night_View_201305.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_Central_District_Night_View_201305.jpg "fig:Hong_Kong_Central_District_Night_View_201305.jpg")
[Central_Western_View_201007.jpg](https://zh.wikipedia.org/wiki/File:Central_Western_View_201007.jpg "fig:Central_Western_View_201007.jpg")望向中環\]\]
[Central_Circa_1910_IMG_5786.JPG](https://zh.wikipedia.org/wiki/File:Central_Circa_1910_IMG_5786.JPG "fig:Central_Circa_1910_IMG_5786.JPG")\]\]
[1950年代中環沿岸.JPG](https://zh.wikipedia.org/wiki/File:1950年代中環沿岸.JPG "fig:1950年代中環沿岸.JPG")
[Hong_Kong_Central_Outlying_Islands_Ferry_Pier_in_1971.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_Central_Outlying_Islands_Ferry_Pier_in_1971.jpg "fig:Hong_Kong_Central_Outlying_Islands_Ferry_Pier_in_1971.jpg")
[Central,_Hong_Kong_-_panoramio_(20).jpg](https://zh.wikipedia.org/wiki/File:Central,_Hong_Kong_-_panoramio_\(20\).jpg "fig:Central,_Hong_Kong_-_panoramio_(20).jpg")
**中環**（），又稱**中區**（，簡稱**Central**），位於[香港島](../Page/香港島.md "wikilink")[中西區](../Page/中西區_\(香港\).md "wikilink")，是[香港的](../Page/香港.md "wikilink")[商業中心及政治中心](../Page/中心商業區.md "wikilink")。

[香港交易所](../Page/香港交易所.md "wikilink")、多家大型[銀行](../Page/銀行.md "wikilink")、跨國金融機構及外國[領事館都設在中環](../Page/領事館.md "wikilink")。香港的[終審法院](../Page/香港終審法院.md "wikilink")、[禮賓府](../Page/禮賓府.md "wikilink")（前港督府）以及全港第二高建築物[國際金融中心二期也位於中環](../Page/國際金融中心二期.md "wikilink")。[香港特別行政區政府總部](../Page/香港特別行政區政府總部.md "wikilink")、[立法會綜合大樓和](../Page/立法會綜合大樓.md "wikilink")[解放軍駐港部隊](../Page/解放軍駐港部隊.md "wikilink")[中環軍營則位於鄰近中環的](../Page/中環軍營.md "wikilink")[金鐘](../Page/金鐘.md "wikilink")[添馬](../Page/添馬.md "wikilink")。

中環之名源於19世紀時香港華人對香港島北岸城區的通俗分區「[四環九約](../Page/四環九約.md "wikilink")」，「中環」為其中一環；至於港英政府對[維多利亞城](../Page/維多利亞城.md "wikilink")的官方分區則可分爲東、中、西三區，中環之地即「中區」。在此之後雖然中區的界線略有變化，但其核心區域一直都是中環一帶；後來香港政府也採納「中環」作爲官方地名，「中環」和「中區」也幾乎成爲同義詞，一般坊間應用多稱「中環」，「中區」則爲行政性的區劃。

中環也是香港的交通樞紐，是四條[港鐵路線的交匯點](../Page/港鐵.md "wikilink")，其中[機場快綫通往](../Page/機場快綫.md "wikilink")[赤鱲角](../Page/赤鱲角.md "wikilink")[香港國際機場](../Page/香港國際機場.md "wikilink")。[中環天星碼頭有頻繁渡輪橫越](../Page/中環天星碼頭.md "wikilink")[維多利亞港往來](../Page/維多利亞港.md "wikilink")[尖沙咀](../Page/尖沙咀.md "wikilink")，[港外線碼頭有定期航班往來](../Page/港外線碼頭.md "wikilink")[離島](../Page/香港離島.md "wikilink")。

## 歷史

中環是香港的心臟地帶，也是[香港開埠後最早開發的地區](../Page/香港開埠.md "wikilink")，也是香港的[商業中心](../Page/商業中心.md "wikilink")。

在1841年1月，[英國人佔領並登陸港島時作人口統計](../Page/英國人.md "wikilink")，當時稱「群大路」的中上環一帶只在有50約人居住，反而位於南部的[赤柱有約](../Page/赤柱.md "wikilink")2000人\[1\]。不過基於中環鄰近以前還屬於[清朝的](../Page/清朝.md "wikilink")[九龍半島](../Page/九龍半島.md "wikilink")[尖沙咀](../Page/尖沙咀.md "wikilink")\[2\]，有攻守之利，而且中環背靠高山而前有[深水港](../Page/維多利亞港.md "wikilink")，適合船隻航行，最後便率先在中環建立其[軍事基地](../Page/軍事基地.md "wikilink")，並迅速興建多條幹道。在今[下亞厘畢道的山坡](../Page/下亞厘畢道.md "wikilink")，1841年開埠時已劃為政府專用地段，作為[殖民地政府象徵的](../Page/香港殖民地時期#香港政府.md "wikilink")[港督府便座落在](../Page/港督府.md "wikilink")[政府山的中央位置](../Page/政府山.md "wikilink")。

1874年桂文燦編纂的《廣東圖說》同治刊本，當中以[群帶路標誌為](../Page/阿群帶路圖.md "wikilink")[上環](../Page/上環.md "wikilink")、中環、[下環](../Page/灣仔.md "wikilink")。而1866年的《新安縣全圖》，當時的[群帶路所標示的位置亦在中環現址相近](../Page/阿群帶路圖.md "wikilink")。因此歷史上，中環屬於[維多利亞城的其中一部份](../Page/維多利亞城.md "wikilink")。

遠在[第二次世界大戰前及戰後初期](../Page/第二次世界大戰.md "wikilink")，位於[香港島的中環已是當時香港的主要商業中心](../Page/香港島.md "wikilink")。由於土地不敷應用，在[港督](../Page/香港總督.md "wikilink")[德輔任內](../Page/德輔.md "wikilink")（1887年至1891年），中環進行了多次[填海工程](../Page/填海.md "wikilink")。今日的[香港會所](../Page/香港會所.md "wikilink")、[皇后像廣場](../Page/皇后像廣場.md "wikilink")、[前立法會大樓](../Page/立法會大樓_\(香港\).md "wikilink")（現為[香港終審法院所在地](../Page/香港終審法院.md "wikilink")）等，也是當時的填海工程所建成的。

1970至1980年代是中環的全盛時期，當時中環不斷興建[摩天大廈](../Page/摩天大廈.md "wikilink")，包括各銀行總部。加上[金融市場開始興旺](../Page/金融市場.md "wikilink")，不少香港重要的商業活動均在中環進行，不少香港人都以在中環上班為榮。\[3\]但由於中環土地即使多次填海都始終不能滿足需要，加上中環的[辦公室租金一直居高不下](../Page/辦公室.md "wikilink")，這個港島的主要商業區便逐漸擴展至分別位處中環東西兩面的[金鐘和](../Page/金鐘.md "wikilink")[上環](../Page/上環.md "wikilink")，以及[灣仔北岸](../Page/灣仔.md "wikilink")。不過貴為香港的心臟和金融市場中心，中環的商業活動仍然相當頻繁。

雖然中環至今仍然有多幢歷史建築得以留存，但由於[中心商業區的發展重建壓力及政府保育政策的缺失](../Page/中心商業區.md "wikilink")，歷年來中環有不少歷史建築未及得到保育便已經被拆卸或重建，例如[香港大會堂](../Page/香港大會堂.md "wikilink")、舊[匯豐銀行總行](../Page/匯豐銀行.md "wikilink")、舊[郵政總局](../Page/香港郵政總局.md "wikilink")、舊[香港會會所等等](../Page/香港會.md "wikilink")。近年政府遷拆[中環天星碼頭及](../Page/中環天星碼頭.md "wikilink")[皇后碼頭更引起巨大爭議及衝突](../Page/皇后碼頭.md "wikilink")。2009年，時任特首[曾蔭權提出多項中環歷史建築保育計劃](../Page/曾蔭權.md "wikilink")。擬保育的建築包括[中區政府合署東座及中座](../Page/中區政府合署.md "wikilink")、前[中央警署](../Page/中央警署.md "wikilink")、前[域多利監獄及前](../Page/域多利監獄.md "wikilink")[裁判司署](../Page/裁判司署.md "wikilink")、[中環街市](../Page/中環街市.md "wikilink")、-{[荷里活道](../Page/荷里活道.md "wikilink")}-前警察宿舍（[英皇書院](../Page/英皇書院.md "wikilink")）等。此外政府亦和[香港聖公會基恩小學洽商保育](../Page/香港聖公會基恩小學.md "wikilink")[慈幼英文學校建築群](../Page/慈幼英文學校.md "wikilink")。

## 著名地點、街道及建築物

### 地點及街道

  - [香港動植物公園](../Page/香港動植物公園.md "wikilink")
  - [利源東街](../Page/利源東街.md "wikilink")
  - [蘭桂坊](../Page/蘭桂坊.md "wikilink")
  - [荷李活道](../Page/荷李活道.md "wikilink")
  - [皇后像廣場](../Page/皇后像廣場.md "wikilink")
  - [遮打花園](../Page/遮打花園.md "wikilink")

<File:The> Cenotaph 2014.jpg|[和平紀念碑](../Page/和平紀念碑_\(香港\).md "wikilink")
<File:D'aguilar> Street 2016.jpg|晚上的[德己立街](../Page/德己立街.md "wikilink")
<File:LKF> Street
View01.jpg|[蘭桂坊一帶](../Page/蘭桂坊.md "wikilink")[酒吧林立](../Page/酒吧.md "wikilink")

### 建築物

<div style="-moz-column-count:2;">

  - [終審法院大樓](../Page/終審法院大樓.md "wikilink")
  - [香港大會堂](../Page/香港大會堂.md "wikilink")
  - [香港郵政總局](../Page/香港郵政總局.md "wikilink")
  - [中環街市](../Page/中環街市.md "wikilink")
  - [匯豐總行大廈](../Page/香港匯豐總行大廈.md "wikilink")
  - [渣打銀行大廈](../Page/渣打銀行大廈_\(香港\).md "wikilink")
  - [中銀大廈](../Page/中銀大廈_\(香港\).md "wikilink")
  - [國際金融中心一期](../Page/國際金融中心_\(香港\).md "wikilink")、二期（國金、IFC）
  - [香港四季酒店](../Page/香港四季酒店.md "wikilink")
  - [怡和大廈](../Page/怡和大廈.md "wikilink")
  - [置地廣場](../Page/置地廣場.md "wikilink")
  - [交易廣場](../Page/交易廣場.md "wikilink")
  - [大館](../Page/大館.md "wikilink")
  - [中環中心](../Page/中環中心.md "wikilink")
  - [長江集團中心](../Page/長江集團中心.md "wikilink")
  - [美國駐香港總領事館](../Page/美國駐香港總領事館.md "wikilink")
  - [香港海事博物馆](../Page/香港海事博物馆.md "wikilink")（中環八號碼頭）
  - [律政中心](../Page/律政中心.md "wikilink")（前政府總部）
  - [禮賓府](../Page/禮賓府.md "wikilink")
  - [中國人民解放軍駐香港部隊大廈](../Page/中國人民解放軍駐香港部隊大廈.md "wikilink")
  - [太子大廈](../Page/太子大廈.md "wikilink")
  - [約克大廈](../Page/約克大廈.md "wikilink")
  - [遮打大廈](../Page/遮打大廈.md "wikilink")
  - [歷山大廈](../Page/歷山大廈.md "wikilink")
  - [中建大廈](../Page/中建大廈.md "wikilink")
  - [畢打行](../Page/畢打行.md "wikilink")
  - [會德豐大廈](../Page/會德豐大廈.md "wikilink")
  - [萬宜大廈](../Page/萬宜大廈.md "wikilink")
  - [中保集團大廈](../Page/中保集團大廈.md "wikilink")
  - [香港摩天輪](../Page/香港摩天輪.md "wikilink")

</div>

<File:HK> HSBC Main Building
2008.jpg|[匯豐總行大廈](../Page/香港匯豐總行大廈.md "wikilink")
<File:Hong> Kong Legislative Council
Building.jpg|[終審法院大樓](../Page/終審法院大樓.md "wikilink")
<File:IFC>, Hong Kong Island
(2796343561).jpg|[國際金融中心二期](../Page/國際金融中心_\(香港\).md "wikilink")
[File:Hkcatholiccathedral.jpg|聖母無原罪主教座堂外貌](File:Hkcatholiccathedral.jpg%7C聖母無原罪主教座堂外貌)
<File:Tai> Kwun Parade Ground 201806.jpg|[大館](../Page/大館.md "wikilink")
<File:HK> Central Pedder Street Crosswalk LV Citibank Bus
Taxi.JPG|[畢打街北段](../Page/畢打街.md "wikilink")，左為[置地廣塲](../Page/置地廣塲.md "wikilink")

### 酒店

  - [文華東方酒店](../Page/香港文華東方酒店.md "wikilink")
  - [置地文華東方酒店](../Page/置地文華東方酒店.md "wikilink")
  - [四季酒店](../Page/香港四季酒店.md "wikilink")
  - [The Murray](../Page/The_Murray.md "wikilink")

### 中學

  - [聖若瑟書院](../Page/聖若瑟書院.md "wikilink")
  - [聖保羅男女中學](../Page/聖保羅男女中學.md "wikilink")
  - [高主教書院](../Page/高主教書院.md "wikilink")
  - [英華女學校](../Page/英華女學校.md "wikilink")

### 教堂、會堂和廟宇

  - [香港聖公會](../Page/香港聖公會.md "wikilink")[聖約翰座堂](../Page/聖約翰座堂_\(香港\).md "wikilink")
  - [香港聖公會](../Page/香港聖公會.md "wikilink")[會督府](../Page/會督府.md "wikilink")
  - [香港聖公會](../Page/香港聖公會.md "wikilink")[聖保羅堂](../Page/聖保羅堂.md "wikilink")
  - [聖母無原罪主教座堂](../Page/香港聖母無原罪主教座堂.md "wikilink")
  - [基督科學教會香港第一分會](../Page/基督科學教會香港第一分會.md "wikilink")
  - [香港佑寧堂](../Page/香港佑寧堂.md "wikilink")
  - [香港浸信會](../Page/香港浸信會.md "wikilink")

## 交通

作為商業中心區，中環交通極之方便八達。[港島綫駛經](../Page/港島綫.md "wikilink")[中環站](../Page/中環站.md "wikilink")（），[荃灣綫以](../Page/荃灣綫.md "wikilink")[中環站為終點站](../Page/中環站.md "wikilink")、[東涌綫和](../Page/東涌綫.md "wikilink")[機場快綫也以位處本區的](../Page/機場快綫.md "wikilink")[香港站為市區總站](../Page/香港站.md "wikilink")，[中環站](../Page/中環站.md "wikilink")—[香港站是一個重要交通樞紐](../Page/香港站.md "wikilink")，兩站相連，乘客可通過站內通道在不拍卡出閘情況下在兩站間往來。港島綫穿梭香港島北岸、荃灣綫來往香港島與[九龍](../Page/九龍.md "wikilink")；[東涌綫往返](../Page/東涌綫.md "wikilink")[香港島](../Page/香港島.md "wikilink")、[西九龍](../Page/西九龍.md "wikilink")、[新界](../Page/新界.md "wikilink")[荔景](../Page/荔景.md "wikilink")、[青衣島與](../Page/青衣島.md "wikilink")[大嶼山東涌之間](../Page/大嶼山.md "wikilink")，也可在中途站[欣澳站轉乘](../Page/欣澳站.md "wikilink")[迪士尼綫](../Page/迪士尼綫.md "wikilink")；[機場快綫為旅客提供市區至](../Page/機場快綫.md "wikilink")[香港國際機場的快捷交通工具選擇](../Page/香港國際機場.md "wikilink")，前往[香港國際機場只需時](../Page/香港國際機場.md "wikilink")24分鐘。港鐵亦計劃延長東涌綫至[添馬站](../Page/添馬站.md "wikilink")（），接駁[將軍澳綫前往](../Page/將軍澳綫.md "wikilink")[將軍澳新市鎮](../Page/將軍澳新市鎮.md "wikilink")。

巴士方面，[干諾道中和](../Page/干諾道.md "wikilink")[德輔道中都有許多巴士路線往來港九新界各區](../Page/德輔道.md "wikilink")，當中不少更是以中環為總站。[香港電車則駛經德輔道中](../Page/香港電車.md "wikilink")。

渡輪方面，中環近[維多利亞港海旁可謂香港渡輪服務交通樞紐](../Page/維多利亞港.md "wikilink")，[港外線碼頭設有多條航線往來](../Page/港外線碼頭.md "wikilink")[多座離島](../Page/香港離島.md "wikilink")，包括[中環至長洲](../Page/中環至長洲航線.md "wikilink")（[新渡輪](../Page/新渡輪.md "wikilink")）\[4\]、中環至[大嶼山](../Page/大嶼山.md "wikilink")[梅窩](../Page/梅窩.md "wikilink")（新渡輪）\[5\]、中環至[愉景灣](../Page/愉景灣.md "wikilink")（[港九小輪](../Page/港九小輪.md "wikilink")）\[6\]、中環至[南丫島](../Page/南丫島.md "wikilink")[榕樹灣](../Page/榕樹灣.md "wikilink")、南丫島[索罟灣和中環至](../Page/索罟灣.md "wikilink")[馬灣](../Page/馬灣.md "wikilink")[珀麗灣等等](../Page/珀麗灣.md "wikilink")\[7\]。[天星小輪是跨越](../Page/天星小輪.md "wikilink")[維多利亞港](../Page/維多利亞港.md "wikilink")、往來香港島和九龍的歷史悠久和特色交通工具，[中環天星碼頭有頻繁渡輪](../Page/中環天星碼頭.md "wikilink")[往來九龍尖沙咀](../Page/中環至尖沙咀航線.md "wikilink")。中環往來尖沙咀航線服務的歷史更可追溯到1870年代的[蒸汽船](../Page/蒸汽船.md "wikilink")\[8\]。昔日天星小輪還有[中環至紅磡航線](../Page/中環至紅磡航線.md "wikilink")，惟因乘客量不足，於2011年4月1日起停辦。

<File:Central> Station 2017 05 part27.jpg|中環站港島綫月台 <File:Pier> 7 - Hong
Kong - Sarah
Stierch.jpg|[中環天星碼頭](../Page/中環天星碼頭.md "wikilink")（右，往尖沙咀）和[香港海事博物館](../Page/香港海事博物館.md "wikilink")
<File:Hong> Kong
Harbor.jpg|[港外線碼頭連同左方的中環公眾碼頭和中環天星碼頭](../Page/港外線碼頭.md "wikilink")
<File:Silver> Star 20120919.JPG|一艘天星小輪停靠於中環碼頭

### 主要交通幹道

  - [夏慤道](../Page/夏慤道.md "wikilink")
  - [干諾道中](../Page/干諾道.md "wikilink")
  - [金鐘道](../Page/金鐘道.md "wikilink")
  - [皇后大道中](../Page/皇后大道.md "wikilink")
  - [德輔道中](../Page/德輔道.md "wikilink")
  - [堅道](../Page/堅道.md "wikilink")
  - [羅便臣道](../Page/羅便臣道.md "wikilink")
  - [龍和道](../Page/龍和道.md "wikilink")

<File:Des> Voeux Road, Central,
c1920s-30s.jpg|[德輔道中](../Page/德輔道.md "wikilink")（1920至30年代）
<File:Des> Voeux Road Central near World Wide
House.jpg|德輔道中近[環球大廈](../Page/環球大廈.md "wikilink")
<File:Connaught> Road, Central.JPG|[干諾道中](../Page/干諾道.md "wikilink")

### 公共交通

<div class="NavFrame collapsed" style="color: black; background-color: white; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: white; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: white; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")、<font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")：[中環站](../Page/中環站.md "wikilink")
  - <font color="{{東涌綫色彩}}">█</font>[東涌綫](../Page/東涌綫.md "wikilink")、<font color={{機場快綫色彩}}>█</font>[機場快綫](../Page/機場快綫.md "wikilink")：[香港站](../Page/香港站.md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

<!-- end list -->

  - 紅色小巴

<!-- end list -->

  - [西環](../Page/西環.md "wikilink")－[銅鑼灣](../Page/銅鑼灣.md "wikilink")
  - [荃灣](../Page/荃灣.md "wikilink")－[灣仔](../Page/灣仔.md "wikilink")

<!-- end list -->

  - [渡海小輪](../Page/渡海小輪.md "wikilink")

<!-- end list -->

  - 中環－[尖沙咀](../Page/尖沙咀.md "wikilink")
  - 中環－[長洲](../Page/長洲.md "wikilink")
  - 中環－[梅窩](../Page/梅窩.md "wikilink")（[大嶼山](../Page/大嶼山.md "wikilink")）
  - 中環－[坪洲](../Page/坪洲.md "wikilink")
  - 中環－[榕樹灣](../Page/榕樹灣.md "wikilink")（[南丫島](../Page/南丫島.md "wikilink")）
  - 中環－[索罟灣](../Page/索罟灣.md "wikilink")（南丫島）
  - 中環－[愉景灣](../Page/愉景灣.md "wikilink")
  - 中環－[珀麗灣](../Page/珀麗灣.md "wikilink")（[馬灣](../Page/馬灣.md "wikilink")）

<!-- end list -->

  - [中環至半山自動扶梯系統](../Page/中環至半山自動扶梯系統.md "wikilink")

</div>

</div>

## 區議會議席分佈

由於中環海傍主要由[辦公室及](../Page/辦公室.md "wikilink")[酒店組成](../Page/酒店.md "wikilink")，常住人口主要集中在[威靈頓街以南範圍](../Page/威靈頓街.md "wikilink")。為方便比較，以下列表會東至[紅棉道](../Page/紅棉道.md "wikilink")，西至[吉士笠街](../Page/吉士笠街.md "wikilink")、[租庇利街](../Page/租庇利街.md "wikilink")、[香港四季酒店至](../Page/香港四季酒店.md "wikilink")[中區政府碼頭](../Page/中區政府碼頭.md "wikilink")、南至[下亞厘畢道](../Page/下亞厘畢道.md "wikilink")、[雲咸街](../Page/雲咸街.md "wikilink")、[荷李活道為範圍](../Page/荷李活道.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>年度/範圍</p></th>
<th><p>2000-2003</p></th>
<th><p>2004-2007</p></th>
<th><p>2008-2011</p></th>
<th><p>2012-2015</p></th>
<th><p>2016-2019</p></th>
<th><p>2020-2023</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>中環</strong>全區</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

註：實際上[中環選區亦包括鄰近的](../Page/中環_\(選區\).md "wikilink")[金鐘及](../Page/金鐘.md "wikilink")[添馬地區](../Page/添馬.md "wikilink")，以至鄰近[上環港鐵站的](../Page/上環站.md "wikilink")[上環商業區部分](../Page/上環.md "wikilink")。以上範圍尚有其他細微分別，請參閱有關區議會選舉選區分界地圖。

## 參看

  - [中西區](../Page/中西區_\(香港\).md "wikilink")
  - [半山區](../Page/半山區.md "wikilink")
  - [太平山](../Page/太平山_\(香港\).md "wikilink")
  - [中區](../Page/中區_\(香港\).md "wikilink")
  - [上環](../Page/上環.md "wikilink")
  - [金鐘](../Page/金鐘.md "wikilink")
  - [讓愛與和平佔領中環](../Page/讓愛與和平佔領中環.md "wikilink")

## 注釋

## 參考資料

## 外部連結

  - [香港自由行－中環篇](http://hkfreetour.com/?p=331)

{{-}}

{{-}}

[Category:中西區 (香港)](../Category/中西區_\(香港\).md "wikilink")
[Category:中環](../Category/中環.md "wikilink")
[Category:香港商業區](../Category/香港商業區.md "wikilink")

1.

2.

3.

4.
5.

6.

7.

8.  《油尖旺區風物誌》，油尖旺區議會