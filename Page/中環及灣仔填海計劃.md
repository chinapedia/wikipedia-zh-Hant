[Central_and_Wan_Chai_Reclamation_aerial_view_2018.jpg](https://zh.wikipedia.org/wiki/File:Central_and_Wan_Chai_Reclamation_aerial_view_2018.jpg "fig:Central_and_Wan_Chai_Reclamation_aerial_view_2018.jpg")
[Central_and_Wan_Chai_Reclamation_Overview_201007.jpg](https://zh.wikipedia.org/wiki/File:Central_and_Wan_Chai_Reclamation_Overview_201007.jpg "fig:Central_and_Wan_Chai_Reclamation_Overview_201007.jpg")
[Central_and_Wan_Chai_Reclamation_200906.jpg](https://zh.wikipedia.org/wiki/File:Central_and_Wan_Chai_Reclamation_200906.jpg "fig:Central_and_Wan_Chai_Reclamation_200906.jpg")
[HK_Central_Piers_Construction_Site_n_Victoria_Harbour_9-Dec-2012_ICC_Kln.JPG](https://zh.wikipedia.org/wiki/File:HK_Central_Piers_Construction_Site_n_Victoria_Harbour_9-Dec-2012_ICC_Kln.JPG "fig:HK_Central_Piers_Construction_Site_n_Victoria_Harbour_9-Dec-2012_ICC_Kln.JPG")
[Tim_Mei_Avenue_(1).JPG](https://zh.wikipedia.org/wiki/File:Tim_Mei_Avenue_\(1\).JPG "fig:Tim_Mei_Avenue_(1).JPG")

《**中環及灣仔填海計劃**》（）是一份在[香港](../Page/香港.md "wikilink")[香港島區進行的大型](../Page/香港島.md "wikilink")[填海計劃](../Page/填海.md "wikilink")，旨在為[中環灣仔繞道](../Page/中環灣仔繞道.md "wikilink")（即[4號幹線重組工程](../Page/4號幹線.md "wikilink")）及多條擬建的[鐵路支線提供土地](../Page/香港鐵路運輸.md "wikilink")，並且建造一條世界級的[海濱長廊](../Page/海濱長廊.md "wikilink")，為區內提供額外休憩及[綠化用地](../Page/綠化.md "wikilink")，以提升[維多利亞港海旁的吸引力](../Page/維多利亞港.md "wikilink")。《中環及灣仔填海計劃》為5期進行，於1993年起啟動第一期，至今其中首4期已經完成，餘下《**灣仔發展計劃**》第2期進行中，於2018年完成。

## 計劃沿革

### 都會計劃

在1983年10月發表的《[海港填海及市區發展研究](../Page/海港填海及市區發展研究.md "wikilink")》提出了在中環及灣仔進行填海工程的需要。當時，[香港政府計劃對](../Page/香港殖民地時期#香港政府.md "wikilink")[香港市區作出粗略的](../Page/香港市區.md "wikilink")[都會概念規劃](../Page/都會.md "wikilink")，以應付於21世紀的發展需要。因此，香港政府於1987年至1990年期間進行《[都會計劃選定策略](../Page/都會計劃.md "wikilink")》研究，作為2011年前重整都會區的土地用途、[運輸及](../Page/運輸.md "wikilink")[環境規劃架構](../Page/環境.md "wikilink")，策略涉及廣泛的市區土地規劃，包括[荃灣與](../Page/荃灣.md "wikilink")[葵青](../Page/葵青.md "wikilink")、[西九龍](../Page/西九龍.md "wikilink")、[啟德](../Page/啟德.md "wikilink")、[九龍中部及東部](../Page/九龍.md "wikilink")、香港島西部、東部及南部。《都會計劃選定策略》於1991年9月17日獲得[行政局通過](../Page/行政局.md "wikilink")，相關[香港政府部門根據策略逐步落實為指定地區規劃目標](../Page/香港政府部門.md "wikilink")，進而制定發展綱領。《都會計劃選定策略》為《中環及灣仔填海計劃》提供了原始的規劃概念。

由於[香港人口急劇增長](../Page/香港人口.md "wikilink")，市區人口於1999年達到《都會計劃選定策略》擬訂的上限（420萬人）。同時，[亞洲金融風暴的影響以及公眾強烈反對在](../Page/亞洲金融風暴.md "wikilink")[維多利亞港進行新的填海工程](../Page/維多利亞港.md "wikilink")，以填海供應新土地的發展方式受到限制，原來的《都會計劃選定策略》基本建議不再適用。因此《都會計劃選定策略》於1998年及2003年分兩階段完成全面檢討研究，以修正原來的規劃策略。

### 後過渡期的基礎建設發展

隨著[啟德機場的容量於](../Page/啟德機場.md "wikilink")1994年至1996年接近飽和，香港政府於1970年代起就新機場選址及規劃進行研究，惟建築費用高昂，計劃一直無落實。1989年，香港因為[六四事件引起信心危機](../Page/六四事件.md "wikilink")，[香港總督](../Page/香港總督.md "wikilink")[衛奕信隨即在](../Page/衛奕信.md "wikilink")10月11日宣讀的《[施政報告](../Page/施政報告.md "wikilink")》中，宣佈興建新機場及相關配套設施，即後來的《[香港機場核心計劃](../Page/香港機場核心計劃.md "wikilink")》，藉此穩定[香港市民信心](../Page/香港市民.md "wikilink")。《香港機場核心計劃》落實了《中區填海計劃》第1期及《[西九龍填海計劃](../Page/西九龍填海計劃.md "wikilink")》等填海工程。同時，為了維持香港在「後過渡期」的經濟持續發展，香港政府於1990年代極力推動大型[基礎建設工程](../Page/基礎建設.md "wikilink")，位於[愛秩序灣避風塘內的](../Page/愛秩序灣.md "wikilink")《[愛秩序灣發展計劃](../Page/愛秩序灣發展計劃.md "wikilink")》、位於[黃埔花園海濱的](../Page/黃埔花園.md "wikilink")《[紅磡灣填海計劃](../Page/紅磡灣填海計劃.md "wikilink")》第1期及第2期、在[添馬艦海軍基地舊址進行的](../Page/添馬艦_\(香港\).md "wikilink")《中區填海計劃》第2期、為[香港會議展覽中心新翼提供土地的](../Page/香港會議展覽中心.md "wikilink")《灣仔填海計劃》第1期等填海工程，相繼於1990年代中葉落成。

### 《保護海港條例》與有關訴訟

隨著多項大型填海工程相繼啟動，逐漸使到部份[香港社會人士認為香港政府漠視](../Page/香港社會.md "wikilink")[天然資源](../Page/天然資源.md "wikilink")，盲目填海造地，因而引起強烈不滿。[保護海港協會於](../Page/保護海港協會.md "wikilink")1995年11月成立，成功得到17萬名香港市民簽名支持立法保護海港，《[保護海港條例](../Page/保護海港條例.md "wikilink")》（《[香港法例](../Page/香港法例.md "wikilink")》第531章[1](http://www.legislation.gov.hk/blis_ind.nsf/CurAllChinDoc?OpenView&Start=523&Count=30&Expand=531.1#531.1)）於1996年由[香港立法局議員](../Page/香港立法局.md "wikilink")[陸恭蕙以](../Page/陸恭蕙.md "wikilink")[私人草案形式提交立法局](../Page/私人草案.md "wikilink")，於1997年6月27日獲得通過。條例訂明[維多利亞港為](../Page/維多利亞港.md "wikilink")[香港人的特別公有資產及天然財產](../Page/香港人.md "wikilink")，必須受到保護和保存，並且設定了不准許在維多利亞港進行填海工程的法定原則，所有公職人員和公共機構在行使任何歸屬權力時，都必須要考慮以上的法律原則。

[香港主權移交後](../Page/香港主權移交.md "wikilink")，大型基礎建竣仍然是[香港政府促進](../Page/香港政府.md "wikilink")[香港經濟的主要措施之一](../Page/香港經濟.md "wikilink")。同時，由於香港島[4號幹線之主要道路的負荷即將飽和](../Page/4號幹線.md "wikilink")，香港政府認為有急切需要填海興建[中環灣仔繞道](../Page/中環灣仔繞道.md "wikilink")，以抒緩[香港交通擠塞問題](../Page/香港交通.md "wikilink")，及取代[干諾道](../Page/干諾道.md "wikilink")、[夏愨道](../Page/夏愨道.md "wikilink")、[告士打道及](../Page/告士打道.md "wikilink")[維園道成為](../Page/維園道.md "wikilink")4號幹線一部分。2002年12月17日，[行政會議批准](../Page/香港行政會議.md "wikilink")《中區填海計劃》第3期工程，填海面積達23.11公頃，於同年12月27日正式刊憲。

其後，保護海港協會認為[城市規劃委員會通過](../Page/城市規劃委員會.md "wikilink")《灣仔填海計劃》第2期工程違反《保護海港條例》，逐於2003年2月27日向法院提出[司法覆核](../Page/司法覆核.md "wikilink")；法院於同年7月8日裁決城市規劃委員會敗訴，使到上述工程的批准被迫取消，並且指出有關填海計劃必須附合3個條件測試：

1.  有迫切性、具充分理由及有即時需要；
2.  沒有其他切實可行的選擇；
3.  對海港造成的損害減至最少。

保護海港協會認為此判決對於正在進行的《中區填海計劃》第3期工程具有約束力，遂要求香港政府停止有關工程，但是香港政府以停工將會令到承建商向香港政府提出鉅額索償為理由拒絕，堅持繼續工程。保護海港協會繼而於8月26日申請直接向[終審法院上訴](../Page/香港終審法院.md "wikilink")，為灣仔填海工程的判決尋求進一步的法律裁定及詮釋；該司法覆核於9月27日獲得接納，[房屋及規劃地政局局長](../Page/房屋及規劃地政局局長.md "wikilink")[孫明揚於是宣佈有關工程暫時停止](../Page/孫明揚.md "wikilink")。10月6日，高等法院以有關工程尚未到達不可彌補的階段為理由，裁決香港政府勝訴，並且毋須暫緩填海工程\[1\]，保護海港協會其後提出上訴。為免爭議持續加劇，香港政府主動按照法院提出的3個條件檢討有關工程，並且積極約見相關的專業團體與民間環境保護組織，以爭取香港市民的認同與支持。為了回應保護海港協會就《中區填海計劃》第3期提出的兩個減少填海面積方案，與[海港之友印製的小冊](../Page/海港之友.md "wikilink")《維港初階》所載的一些指控，香港政府先後於12月1日及18日印發名為《我們的海港─過去、現在、未來》及《中區填海第三期工程面面觀》的小冊，以圖文兼茂的方式申明進行《中區填海計劃》第3期的興建理由與法律依據。12月5日，香港政府完成了《中區填海計劃》第3期工程的檢討，認為附合法院提出的3項條件\[2\]。高等法庭原訟庭於2004年3月9日裁決駁回保護海港協會就《中區（擴展部分）分區計劃大綱圖》所提出的司法覆核\[3\]，保護海港協會其後宣佈不予上訴，有關訴訟至此告一段落。

### 優化海濱研究

為了檢討《灣仔填海計劃》第2期，並且就擬定進行填海的《[東南九龍發展計劃](../Page/東南九龍發展計劃.md "wikilink")》諮詢，香港政府於2004年4月成立[共建維港委員會](../Page/共建維港委員會.md "wikilink")，由[李焯芬教授擔任主席](../Page/李焯芬.md "wikilink")，委員會由6名官方成員及23名來自不同專業團體、環境保護組織、保護海港組織、商界等領域的非官方成員\[4\]。委員會於2005年1月展開名為《優化灣仔及鄰近地區海濱的研究》，透過公眾參與方式，凝聚香港市民就灣仔海濱規劃概念的共識，從而擬訂發展概念圖則及分區計劃大綱草圖。

### 中環新海濱城市設計研究

[The_Hong_Kong_Observation_Wheel_Night_view_201512.jpg](https://zh.wikipedia.org/wiki/File:The_Hong_Kong_Observation_Wheel_Night_view_201512.jpg "fig:The_Hong_Kong_Observation_Wheel_Night_view_201512.jpg")\]\]
[Uddberbelly_Hong_Kong_201512.jpg](https://zh.wikipedia.org/wiki/File:Uddberbelly_Hong_Kong_201512.jpg "fig:Uddberbelly_Hong_Kong_201512.jpg")

《[中環新海濱城市設計研究](../Page/中環新海濱城市設計研究.md "wikilink")》是[規劃署於](../Page/規劃署.md "wikilink")2000年代展開的[中環](../Page/中環.md "wikilink")[海濱與](../Page/海濱.md "wikilink")[城市設計研究的公眾參與活動](../Page/城市設計.md "wikilink")，旨在透過展覽及公作坊，與[香港社會各界共同設計優化中環新海濱的城市設計大綱及為主要用地擬備規劃](../Page/香港社會.md "wikilink")／設計綱領，同時探討重組[皇后碼頭及重建](../Page/皇后碼頭.md "wikilink")[天星鐘樓的選址及設計意念](../Page/天星鐘樓.md "wikilink")。透過公眾參與過程，將公眾意見融匯於研究中，以達致[可持續發展及滿足公眾的期望](../Page/可持續發展.md "wikilink")。

第一階段公眾參與於2007年5月3日開始至6月30日完成，研究重點為重組皇后碼頭和重建舊天星鐘樓的可能選址與設計概念。第二階段公眾參與於2008年4月11日開始至7月10日完成，研究顧問因應第一階段所收集的公眾意見，提出了經優化的城市設計大綱、主要用地的設計概念、以及重組皇后碼頭和重建天星鐘樓的不同地點的設計概念。

2013年起，中環新海濱部分臨海用地以短期租約形式租出，提供[摩天輪及其他遊樂設施](../Page/摩天輪.md "wikilink")，為到大型活動提供場地，部分用地則為建造以簡約設計為主的臨時休憩用地和寵物公園\[5\]\[6\]\[7\]\[8\]。

2013年10月，[地政總署再以短期租約方式為位於](../Page/地政總署.md "wikilink")[龍和道](../Page/龍和道.md "wikilink")[香港大會堂正前方一幅逾](../Page/香港大會堂.md "wikilink")36,000平方米用地招標，除了可以用作臨時表演場地，亦供予商業推廣及展覽用途；然而，規定中標者每年提供不少於12項、不少於120日的免費公眾活動，其中兩項必須最少佔該用地一半\[9\]。

## 工程

### 中區填海計劃

#### 第1期

[HK_ifc_Overview.jpg](https://zh.wikipedia.org/wiki/File:HK_ifc_Overview.jpg "fig:HK_ifc_Overview.jpg")

《中區填海計劃》第1期是[香港機場核心計劃的工程之一](../Page/香港機場核心計劃.md "wikilink")，於[上環與](../Page/上環.md "wikilink")[卜公碼頭之間填造約](../Page/卜公碼頭.md "wikilink")20公頃土地，將中區海岸線向前伸展最多達350米，耗資27億1千萬港元；主要用作興建[港鐵](../Page/港鐵.md "wikilink")[香港站](../Page/香港站.md "wikilink")、擴展中環商業區及重置[中環碼頭](../Page/中環碼頭.md "wikilink")。工程於1993年9月展開，於1996年3月竣工。

現有主要建築物：

  - [國際金融中心一期及二期](../Page/國際金融中心_\(香港\).md "wikilink")
  - [香港四季酒店](../Page/香港四季酒店.md "wikilink")
  - [四季匯](../Page/四季匯.md "wikilink")
  - [港鐵](../Page/港鐵.md "wikilink")[香港站](../Page/香港站.md "wikilink")
  - [中環1號碼頭](../Page/中環碼頭.md "wikilink")（即[中區政府碼頭](../Page/中區政府碼頭.md "wikilink")）
  - [中環2號至7號碼頭](../Page/中環碼頭.md "wikilink")

現有道路：

  - [民吉街](../Page/民吉街.md "wikilink")
  - [民祥街](../Page/民祥街.md "wikilink")
  - [民寶街](../Page/民寶街.md "wikilink")
  - [民光街](../Page/民光街.md "wikilink")
  - [民輝街](../Page/民輝街.md "wikilink")
  - [民耀街](../Page/民耀街.md "wikilink")
  - [金融街](../Page/金融街.md "wikilink")
  - [港中里](../Page/港中里.md "wikilink")

#### 第2期

[Tamar_site,_Hong_Kong.jpg](https://zh.wikipedia.org/wiki/File:Tamar_site,_Hong_Kong.jpg "fig:Tamar_site,_Hong_Kong.jpg")
[Tamar_Development_View_201308.jpg](https://zh.wikipedia.org/wiki/File:Tamar_Development_View_201308.jpg "fig:Tamar_Development_View_201308.jpg")（攝於2013年8月）\]\]

《中區填海計劃》第2期原址為[添馬艦海軍船塢](../Page/添馬艦_\(香港\).md "wikilink")，總填海面積為5.3[公頃](../Page/公頃.md "wikilink")，主要進行[添馬艦發展工程](../Page/添馬艦發展工程.md "wikilink")，興建[新政府總部](../Page/政府總部_\(添馬艦\).md "wikilink")、[行政長官辦公室](../Page/行政長官辦公室_\(添馬艦\).md "wikilink")、[立法會綜合大樓](../Page/立法會綜合大樓.md "wikilink")、[添馬公園和](../Page/添馬公園.md "wikilink")[中西區海濱長廊](../Page/中西區海濱長廊.md "wikilink")；耗資3億2千萬港元，工程於1994年2月展開，於1999年竣工。

現有主要建築物：

  - [中信大廈](../Page/中信大廈_\(香港\).md "wikilink")
  - [政府總部](../Page/政府總部_\(添馬艦\).md "wikilink")
  - [行政長官辦公室](../Page/行政長官辦公室_\(添馬艦\).md "wikilink")
  - [立法會綜合大樓](../Page/立法會綜合大樓.md "wikilink")

現有道路：

  - [添美道](../Page/添美道.md "wikilink")
  - [龍匯道](../Page/龍匯道.md "wikilink")
  - [添華道](../Page/添華道.md "wikilink")

#### 第3期

[Central_Reclamation_Phase_3_Site_view_201501.jpg](https://zh.wikipedia.org/wiki/File:Central_Reclamation_Phase_3_Site_view_201501.jpg "fig:Central_Reclamation_Phase_3_Site_view_201501.jpg")

《中區填海計劃》第3期為添馬艦對出海濱，總填海面積為18公頃（原計劃為32公頃，於1999年修訂為23公頃，於2000年4月再將東面的5.3公頃土地納入灣仔發展計劃第2期），連接《中區填海計劃》第1期及《灣仔發展計劃》第2期的填海區；主要用作興建新天星碼頭（包括改建原有7號碼頭及新建8號碼頭）、公眾碼頭、鐵路及道路預留位置、[中環軍用碼頭泊位預留位置及](../Page/中環軍用碼頭.md "wikilink")[中西區海濱長廊等](../Page/中西區海濱長廊.md "wikilink")。同時提供土地發展中環灣仔繞道、[P2道路網](../Page/龍和道.md "wikilink")（現龍和道）及[機場鐵路掉車隧道等](../Page/大嶼山機場鐵路.md "wikilink")；耗資37億9千萬港元，工程於2003年2月28日展開，填海工程於2008年年底竣工。餘下的道路、排水及中環灣仔繞道隧道管道工程於2011年10月大致完成\[10\]。P2道路網及其相關道路已在2018年6月18日早上6時啟用。

為了配合《中區填海計劃》第3期，香港政府計劃於中環至灣仔海濱一帶興建5幢電力供應大樓，以及連接地下泵房的通風塔。[土木工程拓展署建議在大樓外牆進行](../Page/土木工程拓展署.md "wikilink")[綠化](../Page/綠化.md "wikilink")，擬定出3個方案，包括將大廈外牆打造成為竹林或者樹木模樣，令到建築物更能夠融入海濱環境。2013年10月24日，[海濱事務委員會轄下的港島區海濱發展專責小組舉行會議討論有關建議](../Page/海濱事務委員會.md "wikilink")\[11\]。

此外，當中佔地面積達36,600平方米的可用空間命名為「中環海濱活動空間」，被政府委託Central Venue Management
Limited代為獨家管理及營運，於2014年5月生效至2017年結束。\[12\]

而中環9號至10號碼頭附近的空地則設置高60米的[香港摩天輪](../Page/香港摩天輪.md "wikilink")，於2014年12月5日下午5時開幕。\[13\]

現有主要建築物：

  - [中環7號碼頭](../Page/中環碼頭.md "wikilink")
  - [中環8號碼頭](../Page/中環碼頭.md "wikilink")（即[香港海事博物館](../Page/香港海事博物館.md "wikilink")）
  - [中環9號碼頭](../Page/中環碼頭.md "wikilink")
  - [中環10號碼頭](../Page/中環碼頭.md "wikilink")

現有道路：

  - [龍和道](../Page/龍和道.md "wikilink")

<File:Central> Reclamation Phase 3 - 2008-01-12
1.ogg|卸泥中的挖泥船，民耀街對上的[中區行人天橋系統上](../Page/中區行人天橋系統.md "wikilink")
（攝於2008年1月） <File:Central> Reclamation Phase 3 - 2008-01-12
2.ogg|推泥入海中的挖土機，民耀街對上的[中區行人天橋系統上](../Page/中區行人天橋系統.md "wikilink")
（攝於2008年1月） <File:Central> Reclamation Phase 3
200608.jpg|鳥瞰《中區填海計劃》第3期的土地
（攝影於2006年8月） <File:Central> Reclamation Phase 3 - 2008-01-12
4.jpg|《中區填海計劃》第3期的海堤
（攝於2008年1月） <File:Central> Reclamation Phase 3 - 2008-05-25
1.jpg|《中區填海計劃》第3期
（攝於2008年5月） <File:Central> Reclamation Phase 3 Site view
201308.jpg|《中區填海計劃》第3期
（攝於2013年8月）

### 灣仔發展計劃

#### 第1期

[Wan_Chai_Overview_2008.jpg](https://zh.wikipedia.org/wiki/File:Wan_Chai_Overview_2008.jpg "fig:Wan_Chai_Overview_2008.jpg")新翼\]\]

《灣仔發展計劃》第1期工程是在[香港會議展覽中心以北建造一座約](../Page/香港會議展覽中心.md "wikilink")7公頃的[人工島](../Page/人工島.md "wikilink")，用以興建香港會議展覽中心新翼，耗資5億9千萬港元，工程於1994年3月展開，於1995年10月完成。

現有主要建築物：

  - [香港會議展覽中心新翼](../Page/香港會議展覽中心.md "wikilink")
  - [金紫荊廣場](../Page/金紫荊廣場.md "wikilink")

現有道路：

  - [博覽道](../Page/博覽道.md "wikilink")
  - [博覽道中](../Page/博覽道中.md "wikilink")
  - [博覽道東](../Page/博覽道東.md "wikilink")

#### 第2期

[Eastern_part_of_Hong_Kong_Island.jpg](https://zh.wikipedia.org/wiki/File:Eastern_part_of_Hong_Kong_Island.jpg "fig:Eastern_part_of_Hong_Kong_Island.jpg")
[New_Wan_Chai_Pier_201408.jpg](https://zh.wikipedia.org/wiki/File:New_Wan_Chai_Pier_201408.jpg "fig:New_Wan_Chai_Pier_201408.jpg")

《灣仔發展計劃》第2期工程是在香港會議展覽中心新翼以東至[銅鑼灣避風塘沿岸](../Page/銅鑼灣避風塘.md "wikilink")，建造土地興建中環灣仔繞道、鐵路及道路預留位置及海濱長廊等設施，原計劃包括在銅鑼灣避風塘海堤興建海心公園。由於香港政府重新組織了《中區填海計劃》第3期的部份工程，其中在香港會議展覽中心新翼以西至[中信大廈對出的海旁填造面積](../Page/中信大廈.md "wikilink")5.3公頃的工程被併為此計劃的一部份。

原來計劃填海26公頃，擬定於2004年年初展開工程，於2010年完成。其後因為[城市規劃委員會於](../Page/城市規劃委員會.md "wikilink")2004年受到訴訟挑戰失敗，因而被迫重新檢討計劃，交由[共建維港委員會進行優化海濱研究](../Page/共建維港委員會.md "wikilink")，重新進規劃。有關研究於2007年6月進入詳細規劃階段，於同年底完成發展圖則。《灣仔發展計劃》第2期工程包括於香港會議展覽中心新舊翼間之水道範圍內，進行土地平整和興建中環灣仔繞道隧道，以及重置臨海受影響的設施。工程合約為香港建造業創造了約900個就業職位。

《灣仔發展計劃》第2期的主要目標為於灣仔北及[北角提供興建中環灣仔繞道所需的土地](../Page/北角.md "wikilink")。項目落成後所新增的土地將會發展成為一條世界級海濱長廊，連接中環新海濱長廊，供予公眾享用。此項工程於2009年12月展開\[14\]。

2013年1月30日，[土木工程拓展署批出灣仔發展計劃第](../Page/土木工程拓展署.md "wikilink")2期的第4份工程合約，包括在灣仔西進行土地平整、興建[中環灣仔繞道](../Page/中環灣仔繞道.md "wikilink")、中部通風大樓地基、雨水箱形暗渠以及相關道路工程，合約總值33億5千萬港元。工程隨即於翌日展開，預計於2017年年中完成，配合中環灣仔繞道於同年通車\[15\]。

[土木工程拓展署於](../Page/土木工程拓展署.md "wikilink")2015年3月27日表示，承建商工人於2014年年底在舊灣仔碼頭對開範圍清理海底沉積物，以備進行《灣仔發展計劃》第2期填海及[中環灣仔繞道隧道工程時](../Page/中環灣仔繞道.md "wikilink")，於海床約6米下發現一個體積長約40米、闊約20米和高約2米的大型不動金屬物體，懷疑為沉船的部分殘骸；初步資料經過至少3個月的勘探，而勘探工程則仍在繼續\[16\]。發展局表示已經暫停在該範圍內所進行的工序。有學者估計該些殘骸可能屬於於[第二次世界大戰期間被擊沉的戰艦](../Page/第二次世界大戰.md "wikilink")，亦有可能是於同時期被[英國皇家海軍自行炸沉的](../Page/英國皇家海軍.md "wikilink")[添馬艦](../Page/添馬艦_\(軍艦\).md "wikilink")。\[17\]而[香港浸會大學歷史學系助理教授鄺智文則認為](../Page/香港浸會大學.md "wikilink")，第二次世界大戰時期闊度達20米的船隻不多，推算殘骸之來源可能為[大日本帝國海軍的](../Page/大日本帝國海軍.md "wikilink")「音羽山丸」[運油船](../Page/運油船.md "wikilink")\[18\]。

##### 海濱設計研究

2015年6月，[規劃署展開](../Page/規劃署.md "wikilink")「灣仔北及北角海濱城市設計研究」第一階段公眾參與，為填海計劃範圍設全面的規劃及設計綱領，作為未來發展海濱優化的指引，以建設灣仔及北角海濱地區成為一個更具吸引力、更方便暢達、更具活力及可持續發展的海濱，供公眾享用。署方擬將灣仔北及北角海濱分為5大主題區，包括慶典、渡輪碼頭畔、新水上康樂、活力避風塘及東岸公園主題區。另外，當局又計劃提出興建有遮擋的行人通道，連接渡輪碼頭畔主題區及新灣仔碼頭；而在銅鑼灣避風塘及前公眾貨物裝卸區的水域則引進浮動泳池、下水台階及木板走道等，拉近人與水之間的距離。工程預計2017年起陸續完成。\[19\]

<File:Wan> Chai Reclamation 201007.jpg|填海工程（2010年7月） <File:Wan> Chai
Reclamation 201107.jpg|填海工程（2011年7月） <File:Wan> Chai Reclamation
201410.jpg|填海工程（2014年10月） <File:Wan> Chai Development Project Phase II
under reclamation in September 2015.JPG|填海工程（2015年9月） <File:Wan> Chai
Reclamation Work in Nov 2015.JPG|填海工程（2015年11月） <File:Wan> Chai
Reclamation in Jun 2016.jpg|填海工程（2016年6月） <File:Wan> Chai Reclamation
site 201707.jpg|填海工程（2018年7月）

現有主要建築物：

  - [灣仔碼頭](../Page/灣仔碼頭.md "wikilink")

## 受工程影響建築物

[Clock_Tower,_Star_Ferry_Pier_in_Central.jpg](https://zh.wikipedia.org/wiki/File:Clock_Tower,_Star_Ferry_Pier_in_Central.jpg "fig:Clock_Tower,_Star_Ferry_Pier_in_Central.jpg")
[Wan_Chai_East_View_2009.jpg](https://zh.wikipedia.org/wiki/File:Wan_Chai_East_View_2009.jpg "fig:Wan_Chai_East_View_2009.jpg")
[Fenwick_Pier_(Pontoon).JPG](https://zh.wikipedia.org/wiki/File:Fenwick_Pier_\(Pontoon\).JPG "fig:Fenwick_Pier_(Pontoon).JPG")
[Closed_fenwick_public_pier_2.JPG](https://zh.wikipedia.org/wiki/File:Closed_fenwick_public_pier_2.JPG "fig:Closed_fenwick_public_pier_2.JPG")

**中區填海計劃第1期**

  - [卜公碼頭](../Page/卜公碼頭.md "wikilink")
  - [中環海濱公園](../Page/中環海濱公園.md "wikilink")
  - [統一碼頭](../Page/統一碼頭.md "wikilink")
  - [港內線渡輪碼頭](../Page/港內線渡輪碼頭.md "wikilink")
  - [港外線渡輪碼頭](../Page/港外線渡輪碼頭.md "wikilink")
  - 中區政府碼頭

**中區填海計劃第2期**

  - [添馬艦](../Page/添馬艦_\(香港\).md "wikilink")[海軍基地](../Page/海軍.md "wikilink")

**中區填海計劃第3期**

  - [添馬艦公園](../Page/添馬艦公園.md "wikilink")
  - 中區直昇機坪及軍用碼頭
  - [愛丁堡廣場碼頭](../Page/愛丁堡廣場碼頭.md "wikilink")
  - [大會堂碼頭](../Page/大會堂碼頭.md "wikilink")
  - [皇后碼頭](../Page/皇后碼頭.md "wikilink")

**灣仔發展計劃第1期**

  - [香港會議展覽中心海濱廣場](../Page/香港會議展覽中心.md "wikilink")

**灣仔發展計劃第2期**

  - [灣仔碼頭](../Page/灣仔碼頭.md "wikilink")
  - [分域碼頭](../Page/分域碼頭.md "wikilink")
  - 分域碼頭街梯台\[20\]
  - [灣仔貨物裝卸區](../Page/灣仔貨物裝卸區.md "wikilink")
  - [灣仔海濱長廊](../Page/灣仔海濱長廊.md "wikilink")
  - [香港遊艇會](../Page/香港遊艇會.md "wikilink")
  - [警官俱樂部](../Page/警官俱樂部.md "wikilink")
  - [怡和午炮](../Page/怡和午炮.md "wikilink")
  - [銅鑼灣避風塘](../Page/銅鑼灣避風塘.md "wikilink")

## 参考文献

### 引用

### 来源

  - [房屋及規劃地政局](../Page/房屋及規劃地政局.md "wikilink")：
      - [「灣仔發展計劃第二期相關事件時序表」](https://web.archive.org/web/20061202110850/http://www.hplb.gov.hk/reclamation/chi/basic/wdp2_chronology.htm)
      - [「中區填海第三期工程的事件表」](https://web.archive.org/web/20080417165212/http://www.devb-plb.gov.hk/reclamation/chi/basic/central_chronology040331.htm)

## 外部連結

  - [中環新海濱城市設計研究](https://web.archive.org/web/20110927080913/http://www.pland.gov.hk/pland_en/p_study/prog_s/UDS/index_chi.htm)
  - [中環第三期填海工程環境評估](http://www.criii-cedd.com/)
  - [共建維港委員會](http://www.harbourfront.org.hk)
  - [保護海港協會](http://www.harbourprotection.org)
  - [土木工程拓展署—中環及灣仔填海計劃](https://web.archive.org/web/20060514140455/http://www.cedd.gov.hk/tc/about/achievements/regional/regi_central.htm)
  - [灣仔填海-兩年影像記錄 (YouTube
    video)](https://www.youtube.com/watch?v=wvF6rPemuPc)
  - [灣仔填海-半潛船安裝大型預製件 (YouTube
    video)](https://www.youtube.com/watch?v=unTrgO8O-Zc)

## 参见

  - 地理

<!-- end list -->

  - [香港地理](../Page/香港地理.md "wikilink")
  - [維多利亞港](../Page/維多利亞港.md "wikilink")
  - [添馬艦](../Page/添馬艦_\(香港\).md "wikilink")

<!-- end list -->

  - 工程及發展規劃

<!-- end list -->

  - [香港填海工程](../Page/香港填海工程.md "wikilink")
  - [香港機場核心計劃](../Page/香港機場核心計劃.md "wikilink")
  - [中環灣仔繞道](../Page/中環灣仔繞道.md "wikilink")
  - [添馬艦發展工程](../Page/添馬艦發展工程.md "wikilink")

<!-- end list -->

  - 相關部門及組織

<!-- end list -->

  - [房屋及規劃地政局](../Page/房屋及規劃地政局.md "wikilink")
  - [土木工程拓展署](../Page/土木工程拓展署.md "wikilink")
  - [城市規劃委員會](../Page/城市規劃委員會.md "wikilink")
  - [共建維港委員會](../Page/共建維港委員會.md "wikilink")
  - [保護海港協會](../Page/保護海港協會.md "wikilink")

<!-- end list -->

  - 公眾參與

<!-- end list -->

  - [中環新海濱城市設計研究](../Page/中環新海濱城市設計研究.md "wikilink")

[Category:香港填海工程](../Category/香港填海工程.md "wikilink")
[Category:中環](../Category/中環.md "wikilink")
[Category:灣仔](../Category/灣仔.md "wikilink")
[Category:維多利亞港](../Category/維多利亞港.md "wikilink")

1.  房屋及規劃地政局[「高等法院就中區填海作出裁決」](http://www.info.gov.hk/gia/general/200310/06/1006239.htm)，2003年10月6日
2.  房屋及規劃地政局[「中區填海第三期工程檢討完成」](http://www.devb.gov.hk/tc/publications_and_press_releases/press/index_id_1790.html)，2003年12月5日
3.  房屋及規劃地政局[「政府歡迎法庭有關中區填海第三期工程的裁決」](http://www.devb.gov.hk/tc/publications_and_press_releases/press/index_id_2096.html)，2004年3月9日
4.  房屋及規劃地政局[「政府公布共建維港委員會成員名單」](http://www.devb.gov.hk/tc/publications_and_press_releases/press/index_id_2531.html)，2004年4月28日
5.  [啟德發展
    海濱長廊達11公里](http://orientaldaily.on.cc/cnt/news/20130415/00176_043.html)
    《東方日報》 2013年4月15日
6.  [陳茂波﹕啟德跑道公園年內開放](http://hk.news.yahoo.com/陳茂波-啟德跑道公園年內開放-211849599.html)
     《明報》 2013年4月15日
7.  \[<http://hk.news.yahoo.com/中環新海濱地-市民有望坐摩天輪-220512309.html>; 中環新海濱地
    市民有望坐摩天輪\] 《星島日報》 2013年4月15日
8.  \[<http://hk.news.yahoo.com/陳茂波-部分臨海用地今年將以短租形式租出-092100195.html>;
    陳茂波：部分臨海用地今年將以短租形式租出\]
9.  [中環海濱招租供表演展覽](https://hk.news.yahoo.com/%E4%B8%AD%E7%92%B0%E6%B5%B7%E6%BF%B1%E6%8B%9B%E7%A7%9F%E4%BE%9B%E8%A1%A8%E6%BC%94%E5%B1%95%E8%A6%BD-214746383.html)
    《星島日報》 2013年10月2日
10. 2012年1月13日
11. [中環海濱擬建綠色供電樓](http://orientaldaily.on.cc/cnt/news/20131023/00176_049.html)
    《東方日報》 2013年10月23日
12. [中環海濱活動空間網頁](http://cvm.com.hk/tc/index.php)
13.
14. [灣仔發展計劃第二期](http://www.wd2.gov.hk/chi/introduction.html) 2011年9月8日
15. [灣仔發展計劃第二期工程合約批出](http://hk.news.yahoo.com/灣仔發展計劃第二期工程合約批出-081500631.html)
16. [「添馬艦」重見天日？](http://orientaldaily.on.cc/cnt/news/20150328/00176_008.html)
    《東方日報》 2015年3月28日
17.
18. [殘骸真身是英艦還是日船？](http://orientaldaily.on.cc/cnt/news/20150328/00176_010.html)
    《東方日報》 2015年3月28日
19. [「灣仔北及北角海濱城市設計研究」第一階段公眾參與展開 2015-06-15
    香港政府資訊中心](http://www.info.gov.hk/gia/general/201506/15/P201506150411.htm)
20. [永久封閉灣仔分域碼頭街梯台](http://www.td.gov.hk/tc/traffic_notices/index_id_24585.html)