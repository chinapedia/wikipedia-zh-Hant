[CentralAmericaLocation.svg](https://zh.wikipedia.org/wiki/File:CentralAmericaLocation.svg "fig:CentralAmericaLocation.svg")
[CIA_map_of_Central_America.png](https://zh.wikipedia.org/wiki/File:CIA_map_of_Central_America.png "fig:CIA_map_of_Central_America.png")
**中美洲**是一個地理概念，依據不同的劃分法，屬於北美洲或南美洲的一部份，一般指連接[北美洲與](../Page/北美洲.md "wikilink")[南美洲之間的](../Page/南美洲.md "wikilink")[地峽](../Page/地峽.md "wikilink")，現有七至八個國家（若算入[墨西哥](../Page/墨西哥.md "wikilink")）。

## 範圍

範圍北起[瓜地馬拉](../Page/瓜地馬拉.md "wikilink")，南至[巴拿馬的地區](../Page/巴拿馬.md "wikilink")，面積約50多萬平方公里。本地區國家的面積都不大，大部分的土地為多山的森林區，地峽東部有狹長的平原，面臨[加勒比海及](../Page/加勒比海.md "wikilink")[墨西哥灣](../Page/墨西哥灣.md "wikilink")，氣候溼熱。居民多為印地安人、西班牙人或是兩者混血的後裔，人口多集中在氣候較為涼爽的高原台地或丘陵上。由於火山灰堆積，因此土壤肥沃。農產品有香蕉、咖啡、甘蔗與玉米。但此地峽區地殼活動頻繁，因此常有地震及火山活動。

<table>
<thead>
<tr class="header">
<th><p>简称</p></th>
<th><p>全称（官方名称）</p></th>
<th><p><a href="../Page/国旗.md" title="wikilink">国旗</a></p></th>
<th><p><a href="../Page/政府形式.md" title="wikilink">政府形式</a></p></th>
<th><p><a href="../Page/国体.md" title="wikilink">国体</a></p></th>
<th><p><a href="../Page/首都.md" title="wikilink">首都</a></p></th>
<th><p><a href="../Page/官方语言.md" title="wikilink">官方语言</a></p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>[1]</p></td>
<td><p>巴拿马共和国</p></td>
<td></td>
<td><p><a href="../Page/总统制.md" title="wikilink">总统制</a></p></td>
<td><p><a href="../Page/共和制.md" title="wikilink">共和制</a></p></td>
<td><p><a href="../Page/巴拿马城.md" title="wikilink">巴拿马城</a></p></td>
<td><p><a href="../Page/西班牙语.md" title="wikilink">西班牙语</a></p></td>
<td><p>[2]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>哥斯达黎加共和国</p></td>
<td></td>
<td><p><a href="../Page/总统制.md" title="wikilink">总统制</a></p></td>
<td><p><a href="../Page/共和制.md" title="wikilink">共和制</a></p></td>
<td><p><a href="../Page/聖荷西_(哥斯大黎加).md" title="wikilink">圣荷西</a></p></td>
<td><p><a href="../Page/西班牙语.md" title="wikilink">西班牙语</a></p></td>
<td><p>[3]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>尼加拉瓜共和国</p></td>
<td></td>
<td><p><a href="../Page/总统制.md" title="wikilink">总统制</a></p></td>
<td><p><a href="../Page/共和制.md" title="wikilink">共和制</a></p></td>
<td><p><a href="../Page/马那瓜.md" title="wikilink">马那瓜</a></p></td>
<td><p><a href="../Page/西班牙语.md" title="wikilink">西班牙语</a></p></td>
<td><p>[4]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>洪都拉斯共和国</p></td>
<td></td>
<td><p><a href="../Page/总统制.md" title="wikilink">总统制</a></p></td>
<td><p><a href="../Page/共和制.md" title="wikilink">共和制</a></p></td>
<td><p><a href="../Page/特古西加尔巴.md" title="wikilink">特古西加尔巴</a></p></td>
<td><p><a href="../Page/西班牙语.md" title="wikilink">西班牙语</a></p></td>
<td><p>[5]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>萨尔瓦多共和国</p></td>
<td></td>
<td><p><a href="../Page/总统制.md" title="wikilink">总统制</a></p></td>
<td><p><a href="../Page/共和制.md" title="wikilink">共和制</a></p></td>
<td><p><a href="../Page/圣萨尔瓦多.md" title="wikilink">圣萨尔瓦多</a></p></td>
<td><p><a href="../Page/西班牙语.md" title="wikilink">西班牙语</a></p></td>
<td><p>[6]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>危地马拉共和国</p></td>
<td></td>
<td><p><a href="../Page/总统制.md" title="wikilink">总统制</a></p></td>
<td><p><a href="../Page/共和制.md" title="wikilink">共和制</a></p></td>
<td><p><a href="../Page/危地马拉城.md" title="wikilink">危地马拉城</a></p></td>
<td><p><a href="../Page/西班牙语.md" title="wikilink">西班牙语</a></p></td>
<td><p>[7]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>伯利兹</p></td>
<td></td>
<td><p><a href="../Page/君主立宪制.md" title="wikilink">君主立宪制</a></p></td>
<td><p><a href="../Page/议会制.md" title="wikilink">议会制</a></p></td>
<td><p><a href="../Page/贝尔莫潘.md" title="wikilink">贝尔莫潘</a></p></td>
<td><p><a href="../Page/英语.md" title="wikilink">英语</a></p></td>
<td><p>[8]</p></td>
</tr>
</tbody>
</table>

  - 根据[联合国的](../Page/联合国.md "wikilink")[世界地理分區列表
    (聯合國)的划分](../Page/世界地理分區列表_\(聯合國\).md "wikilink")，中美洲也包括[墨西哥](../Page/墨西哥.md "wikilink")。

## 歷史簡介

1492年[哥倫布首次在](../Page/哥倫布.md "wikilink")[巴哈馬群島登上美洲後](../Page/巴哈馬.md "wikilink")，開啟了西歐各國殖民美洲的濫觴。而西班牙軍隊積極介入之後，將此區的印地安文明[阿茲特克帝國與](../Page/阿茲特克.md "wikilink")[印加帝國給消滅](../Page/印加.md "wikilink")。1821年中美洲脫離西班牙獨立，並出現一个叫[中美洲联合省的国家](../Page/中美洲联合省.md "wikilink")，地域包括现在的[萨尔瓦多](../Page/萨尔瓦多.md "wikilink")、[危地马拉](../Page/危地马拉.md "wikilink")、[洪都拉斯](../Page/洪都拉斯.md "wikilink")、[尼加拉瓜](../Page/尼加拉瓜.md "wikilink")、[哥斯达黎加等地](../Page/哥斯达黎加.md "wikilink")。可惜在1838年來[中美洲联合省解體](../Page/中美洲联合省.md "wikilink")，分裂成為現今中美洲各個國家。

## 人口

<table>
<caption>中美洲國家</caption>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p><a href="../Page/世界各国和地区面积列表.md" title="wikilink">面积</a><br />
(km²)[9]</p></th>
<th><p><a href="../Page/国家人口列表.md" title="wikilink">人口</a><br />
( est.)</p></th>
<th><p><a href="../Page/国家人口密度列表.md" title="wikilink">人口密度</a><br />
(per km²)</p></th>
<th><p>首都</p></th>
<th><p><a href="../Page/官方语言.md" title="wikilink">官方语言</a></p></th>
<th><p><a href="../Page/人类发展指数.md" title="wikilink">HDI</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/貝爾墨邦.md" title="wikilink">貝爾墨邦</a></p></td>
<td><p>英語</p></td>
<td><p>0,715 <span style="color:#0c0;"><strong>高</strong></span></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/聖荷西_(哥斯大黎加).md" title="wikilink">聖荷西</a></p></td>
<td><p>西班牙語</p></td>
<td><p>0,766 <span style="color:#0c0;"><strong>高</strong></span></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/聖薩爾瓦多.md" title="wikilink">聖薩爾瓦多</a></p></td>
<td><p>西班牙語</p></td>
<td><p>0,666 <span style="color:#fc0;"><strong>中</strong></span></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/瓜地馬拉市.md" title="wikilink">瓜地馬拉市</a></p></td>
<td><p>西班牙語</p></td>
<td><p>0,627 <span style="color:#fc0;"><strong>中</strong></span></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/德古斯加巴.md" title="wikilink">德古斯加巴</a></p></td>
<td><p>西班牙語</p></td>
<td><p>0,606 <span style="color:#fc0;"><strong>中</strong></span></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/馬拿瓜.md" title="wikilink">馬拿瓜</a></p></td>
<td><p>西班牙語</p></td>
<td><p>0,631 <span style="color:#fc0;"><strong>中</strong></span></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/巴拿馬城.md" title="wikilink">巴拿馬城</a></p></td>
<td><p>西班牙語</p></td>
<td><p>0,780 <span style="color:#0c0;"><strong>高</strong></span></p></td>
</tr>
<tr class="even">
<td><p>Total</p></td>
<td></td>
<td></td>
<td></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
</tbody>
</table>

<table>
<caption>中美洲最大城市</caption>
<thead>
<tr class="header">
<th><p><strong>城市</strong></p></th>
<th><p><strong>国家</strong></p></th>
<th><p><strong>人口</strong></p></th>
<th><p><strong>测量年份</strong></p></th>
<th><p><strong>占比</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>(1) <a href="../Page/瓜地馬拉市.md" title="wikilink">瓜地馬拉市</a></p></td>
<td></td>
<td><p>5,700,000</p></td>
<td><p>2010</p></td>
<td><p>26%</p></td>
</tr>
<tr class="even">
<td><p>(2) <a href="../Page/聖薩爾瓦多.md" title="wikilink">聖薩爾瓦多</a></p></td>
<td></td>
<td><p>2,415,217</p></td>
<td><p>2009</p></td>
<td><p>39%</p></td>
</tr>
<tr class="odd">
<td><p>(3) <a href="../Page/馬拿瓜.md" title="wikilink">馬拿瓜</a></p></td>
<td></td>
<td><p>1,918,000</p></td>
<td><p>2012</p></td>
<td><p>34%</p></td>
</tr>
<tr class="even">
<td><p>(4) <a href="../Page/德古斯加巴.md" title="wikilink">德古斯加巴</a></p></td>
<td></td>
<td><p>1,819,000</p></td>
<td><p>2010</p></td>
<td><p>24%</p></td>
</tr>
<tr class="odd">
<td><p>(5) <a href="../Page/圣佩德罗苏拉.md" title="wikilink">圣佩德罗苏拉</a></p></td>
<td></td>
<td><p>1,600,000</p></td>
<td><p>2010</p></td>
<td><p>21%+4</p></td>
</tr>
<tr class="even">
<td><p>(6) <a href="../Page/巴拿馬城.md" title="wikilink">巴拿馬城</a></p></td>
<td></td>
<td><p>1,400,000</p></td>
<td><p>2010</p></td>
<td><p>37%</p></td>
</tr>
<tr class="odd">
<td><p>(7) <a href="../Page/聖荷西_(哥斯大黎加).md" title="wikilink">聖荷西 (哥斯大黎加)</a></p></td>
<td></td>
<td><p>1,275,000</p></td>
<td><p>2013</p></td>
<td><p>30%</p></td>
</tr>
</tbody>
</table>

## 参考文献

## 参见

  - [中美洲合眾國](../Page/中美洲合眾國.md "wikilink")
  - [中美洲統合體](../Page/中美洲統合體.md "wikilink")

{{-}}

[中美洲](../Category/中美洲.md "wikilink")
[Category:美洲](../Category/美洲.md "wikilink")
[Category:加勒比地區](../Category/加勒比地區.md "wikilink")
[Category:北美洲](../Category/北美洲.md "wikilink")

1.  巴拿马位于美洲的中心位置，以[巴拿马地峡及](../Page/巴拿马地峡.md "wikilink")[运河划分南北美洲](../Page/巴拿马运河.md "wikilink")。

2.

3.

4.

5.

6.

7.

8.

9.