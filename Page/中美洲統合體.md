**中美洲統合體**（，缩写为**SICA**）於1991年12月13日成立，是一個[中美洲國家政府間的組織](../Page/中美洲.md "wikilink")。該組織以觀察員身份受邀參與[聯合國大會](../Page/聯合國大會.md "wikilink")，並且在[聯合國總部設有常駐代表團](../Page/聯合國總部.md "wikilink")。其總部設於[薩爾瓦多](../Page/薩爾瓦多.md "wikilink")。

## 歷史

中美洲步入政治、經濟和文化統合軌道源自於1907年推動成立中美洲法院，並於1951年簽訂聖薩爾瓦多條約正式成立[中美洲法院](../Page/中美洲法院.md "wikilink")，但在部份成員國發生領土衝突，因此中美洲一體化目標尚未完成。

直至1991年正式成立中美洲統合體才顯示中美洲一體化目標漸入佳境，該組織制定了明確的法律基礎以避免成員國之間再次發生衝突。中美洲統合體成員國包括中美地峽七國及位於[加勒比海的](../Page/加勒比海.md "wikilink")[多明尼加](../Page/多明尼加共和國.md "wikilink")。

中美洲統合體包含[中美洲議會](../Page/中美洲議會.md "wikilink")、[中美洲經濟整合銀行及中美洲共同市場等超國家組織](../Page/中美洲經濟一體化銀行.md "wikilink")。

## 與中國的關係

[聯合國在](../Page/聯合國.md "wikilink")1971年於[聯合國大會2758號決議中把](../Page/聯合國大會2758號決議.md "wikilink")[台灣的](../Page/台灣.md "wikilink")[中華民國在](../Page/中華民國.md "wikilink")[聯合國的](../Page/聯合國.md "wikilink")[中國席位轉給](../Page/中國.md "wikilink")[中國大陸的](../Page/中國大陸.md "wikilink")[中華人民共和國後](../Page/中華人民共和國.md "wikilink")，中華人民共和國與[美國關係轉和](../Page/美國.md "wikilink")，開始有更多國家轉而與中華民國斷交，而與中華人民共和國建交。不少於世界各地的國際組織亦只會把「中國」一詞解釋成「中華人民共和國」。但中華人民共和國在[不結盟運動的情況下](../Page/不結盟運動.md "wikilink")，不加入任何政治性國際聯盟組織（[聯合國除外](../Page/聯合國.md "wikilink")），更與一向親美反共的[中美洲國家關係疏遠](../Page/中美洲.md "wikilink")，反之中華民國著重維持與[中美洲各國](../Page/中美洲.md "wikilink")（[墨西哥除外](../Page/墨西哥.md "wikilink")）的外交關係，[中美洲和](../Page/中美洲.md "wikilink")[加勒比海不少國家也因親美反共的立場](../Page/加勒比海.md "wikilink")，同樣比較傾向支持中華民國，一些曾與中華人民共和國建交的國家如[尼加拉瓜](../Page/尼加拉瓜.md "wikilink")，也大多轉而向中華民國建交，現時中美洲統合體更是世界上唯一一個承認[中華民國政府合法的國際組織](../Page/中華民國政府.md "wikilink")，中華民國在2002年成為該組織的區域外觀察員，以此架構下成立中華民國與中美洲國家混合委員會外交部長會議，1992年至今已舉辦了17次。

雖然在1990年代時中美洲統合體國家一直都與[中華民國維持關係](../Page/中華民國.md "wikilink")，而不與[中華人民共和國建交](../Page/中華人民共和國.md "wikilink")（會員國大都是天主教國家，與[聖座維持緊密關係](../Page/聖座.md "wikilink")）。但在中華人民共和國擁有可幫助第三世界國家提升經濟發展條件下，各國的立場逐漸轉變，2007年6月1日，[哥斯達黎加與中華人民共和國建交](../Page/哥斯達黎加.md "wikilink")，結束與中華民國63年的外交關係。2017年6月13日，[巴拿馬與中華人民共和國建交](../Page/巴拿馬.md "wikilink")，結束與中華民國106年的外交關係。2018年5月1日，[多明尼加與中華人民共和國建交](../Page/多明尼加.md "wikilink")，結束與中華民國77年的外交關係。2018年8月21日，[薩爾瓦多與中華人民共和國建交](../Page/薩爾瓦多.md "wikilink")，結束與中華民國85年的外交關係。現今中美洲統合體八個成員國僅剩四個成員國與中華民國維持正式關係，足以可見中華人民共和國在中美洲的影響力越來越大。

## 總部大樓

中美洲統合體總部大樓位於[聖薩爾瓦多](../Page/聖薩爾瓦多.md "wikilink")，佔地9,919.31[平方公尺](../Page/平方公尺.md "wikilink")，土地由薩爾瓦多政府捐贈，由[中華民國政府資助興建](../Page/中華民國政府.md "wikilink")。\[1\]中美洲統合體為感謝中華民國協助中美洲區域統合進程所作貢獻，將總部大樓6樓之活動大廳命名為「中華民國廳」。\[2\]

## 參见

  - [拉美和加勒比國家共同體](../Page/拉美和加勒比國家共同體.md "wikilink")
  - [加勒比共同體](../Page/加勒比共同體.md "wikilink")

## 參考資料

[Category:大陆联盟](../Category/大陆联盟.md "wikilink")
[Category:哥斯大黎加外交](../Category/哥斯大黎加外交.md "wikilink")
[Category:薩爾瓦多外交](../Category/薩爾瓦多外交.md "wikilink")
[Category:瓜地馬拉外交](../Category/瓜地馬拉外交.md "wikilink")
[Category:宏都拉斯外交](../Category/宏都拉斯外交.md "wikilink")
[Category:尼加拉瓜外交](../Category/尼加拉瓜外交.md "wikilink")
[Category:多明尼加外交](../Category/多明尼加外交.md "wikilink")
[Category:中華民國外交](../Category/中華民國外交.md "wikilink")
[Category:美洲国际性组织](../Category/美洲国际性组织.md "wikilink")
[Category:1991年建立的組織](../Category/1991年建立的組織.md "wikilink")
[Category:中美洲政治](../Category/中美洲政治.md "wikilink")
[Category:联合国大会观察员](../Category/联合国大会观察员.md "wikilink")
[Category:超国家联盟](../Category/超国家联盟.md "wikilink")
[Category:多边开发银行](../Category/多边开发银行.md "wikilink")

1.  [中美洲統合體總部](https://www.sica.int/sgsica/edificio_en.aspx)
2.  [中華民國總統府新聞](http://www.president.gov.tw/NEWS/21046)