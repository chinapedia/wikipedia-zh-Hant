[HK_British_Consulate_name_Supreme_Court_Rd.JPG](https://zh.wikipedia.org/wiki/File:HK_British_Consulate_name_Supreme_Court_Rd.JPG "fig:HK_British_Consulate_name_Supreme_Court_Rd.JPG")的門牌\]\]

**中英聯合聯絡小組**（**Sino-British Joint Liaison
Group**）成立於1985年5月27日，是因應[中](../Page/中華人民共和國.md "wikilink")[英兩國](../Page/英國.md "wikilink")[政府磋商](../Page/政府.md "wikilink")[香港主權交接有關的事宜而產生的聯絡機構](../Page/香港回归.md "wikilink")。該機構是根據《[中英聯合聲明](../Page/中英聯合聲明.md "wikilink")》及其附件二的規定而成立。

聯合聯絡小組只是聯絡機構而不是權力機構，負責就《聯合聲明》實施進行磋商，討論政權交接事宜，並就有關事項交換情況並進行磋商。聯合聯絡小組的工作到2000年1月1日為止。小組成立後，共舉行了47次全體會議，其中18次在[香港舉行](../Page/香港.md "wikilink")，15次在英國[倫敦舉行](../Page/倫敦.md "wikilink")，14次在中國[北京舉行](../Page/北京.md "wikilink")。\[1\]

聯合聯絡小組曾使用[金鐘](../Page/金鐘.md "wikilink")[堅尼地道的](../Page/堅尼地道.md "wikilink")[舊英童學校作為辦公室](../Page/舊英童學校.md "wikilink")。

## 成立緣起

中英兩國於1984年4月11日就香港前途問題舉行[第十二輪談判](../Page/中英香港問題談判#第十二輪談判.md "wikilink")，中方代表在[鄧小平授意下](../Page/鄧小平.md "wikilink")，向英方提出「中英聯合委員會草案」，英方代表擔心此舉會導致1997年前中英共管香港事務，削弱[港督的權力](../Page/港督.md "wikilink")，使[殖民地政府淪為](../Page/香港殖民地時期#香港政府.md "wikilink")「[跛腳鴨](../Page/跛腳鴨.md "wikilink")」，更有可能影響[香港和](../Page/香港.md "wikilink")[澳門移交後之長遠發展](../Page/澳門.md "wikilink")，故此極力反對草案。同年6月召開的[第十六輪談判](../Page/中英香港問題談判#第十六輪談判.md "wikilink")，雙方決定成立起草正式文件的工作小組，商討聯合機構的組織草案，但英方對此態度並不積極。在鄧小平的指示下，中方首席代表[周南於](../Page/周南.md "wikilink")1984年7月28日向英方代表[柯利達提出最終方案](../Page/柯利達.md "wikilink")：1984年底簽署正式協議、聯合小組1988年1月1日進駐香港、直到2000年首季解散等，否則英方須承擔談判破裂的風險。英國首相[戴卓爾夫人聽取正在北京訪問的](../Page/戴卓爾夫人.md "wikilink")[外相](../Page/英國外相.md "wikilink")[賀維及](../Page/賀維.md "wikilink")[港督](../Page/港督.md "wikilink")[尤德匯報後決定妥協](../Page/尤德.md "wikilink")，但英方其後提出小組於1993年進駐，2002年解散等要求均被中方拒絕。最後雙方協議成立聯合聯絡小組，於1988年7月1日進駐香港，直到2000年1月1日解散\[2\]。

## 成員

### 首席代表

<table>
<tbody>
<tr class="odd">
<td><p>中方首席代表：</p>
<ul>
<li><a href="../Page/柯在鑠.md" title="wikilink">柯在鑠大使</a>（1985年-1990年）</li>
<li><a href="../Page/郭豐民.md" title="wikilink">郭豐民大使</a>（1990年-1994年）</li>
<li><a href="../Page/趙稷華.md" title="wikilink">趙稷華大使</a>（1994年-1997年）</li>
<li><a href="../Page/王桂生.md" title="wikilink">王桂生大使</a>（1997年-1998年）</li>
<li><a href="../Page/吳紅波.md" title="wikilink">吳紅波大使</a>（1998年-1999年）</li>
</ul></td>
<td><p>英方首席代表：</p>
<ul>
<li><a href="../Page/衛奕信.md" title="wikilink">衛奕信大使</a>（DR.D.C.WILSON）（1985年-1987年）</li>
<li><a href="../Page/麥若彬.md" title="wikilink">麥若彬大使</a>（R.J.T.McLAREN）（1987年-1989年）</li>
<li><a href="../Page/高德年.md" title="wikilink">高德年大使</a>（A.C.GALSWORTHY）（1989年-1993年）</li>
<li><a href="../Page/戴維斯_(H.L.DAVIES).md" title="wikilink">戴維斯大使</a>（H.L.DAVIES）（1993年-1997年）</li>
<li><a href="../Page/包雅倫.md" title="wikilink">包雅倫大使</a>（ALAN PAUL）（1997年-1999年）</li>
</ul></td>
</tr>
</tbody>
</table>

### 其他代表

[孫明揚](../Page/孫明揚.md "wikilink")（1989至91年）（1997至99年）（曾任教育局局長）

[吳榮奎](../Page/吳榮奎.md "wikilink")（1994至97年）（曾任公務員敍用委員會主席）

## 最後一次會議

1999年12月7日至8日，中英聯合聯絡小組第四十七次，也是最後一次會議在[香港禮賓府舉行](../Page/香港禮賓府.md "wikilink")。中方出席人員有[吳紅波大使](../Page/吳紅波.md "wikilink")（首席代表）、[呂平](../Page/呂平.md "wikilink")、[孫明揚](../Page/孫明揚.md "wikilink")、（[香港特區政府](../Page/香港特區政府.md "wikilink")[政制及內地事務局局長](../Page/政制及內地事務局.md "wikilink")）、[成綬三及有關專家和工作人員](../Page/成綬三.md "wikilink")；英方出席人員則有[包雅倫大使](../Page/包雅倫.md "wikilink")（首席代表）、[華利文](../Page/華利文.md "wikilink")（[英國外交部中華事務司司長](../Page/英國外交部中華事務司.md "wikilink")）、[艾志安](../Page/艾志安.md "wikilink")（[英國駐中國大使館](../Page/英國駐中國大使館.md "wikilink")[政務參贊](../Page/政務參贊.md "wikilink")）、[楊安俊](../Page/楊安俊.md "wikilink")（[英國外交部法律顧問](../Page/英國外交部.md "wikilink")）、[韓菊](../Page/韓菊.md "wikilink")（英方首席代表辦事處[一等秘書](../Page/一等秘書.md "wikilink")）。\[3\]　

## 专家小组

  - 国际权利与义务专家小组
  - 出入境和居留权问题专家小组
  - 关于过渡期的财政预算案编制和有关问题的专家小组
  - 档案移交专家小组
  - 政府资产专家小组
  - 移交仪式专家小组
  - 防务及治安问题专家小组
  - 法律本地化专家小组
  - 法律适应化专家小组
  - 终审法院问题专家小组
  - 知识产权问题专家小组
  - 投资保护协定专家小组
  - 移交逃犯协定专家小组
  - 刑事司法协助问题专家小组
  - 香港與外國對等承諾及執行民商事判決問題專家小組
  - 香港民航協定及香港與灣之間航綫協議安排問題專家小組
  - 移交被判刑人问题专家小组
  - 专营权、合约及有关问题专家小组
  - 公务员问题专家小组
  - 滞港越南船民和难民问题专家小组
  - 香港排污计划问题专家小组
  - 退休保障及社会福利问题专家小组

……

## 中英联合联络小组机场委员会

## 參考文獻

## 外部連結

  - [《中英聯合聲明》附件二：「關於中英聯合聯絡小組」](https://web.archive.org/web/20080519110402/http://www.cmab.gov.hk/tc/issues/app2.htm)，1984年12月19日
  - [政務司司長中英聯合聯絡小組酒會演辭](http://www.info.gov.hk/gia/general/199912/21/1221220.htm)，香港特區政府新聞稿，1999年12月21日
  - [行政長官表揚中英聯合聯絡小組歷史貢獻](http://www.info.gov.hk/gia/general/199912/21/1221242.htm)，香港特區政府新聞稿，1999年12月21日

## 参见

  - [香港主權移交](../Page/香港主權移交.md "wikilink")
  - [中英談判](../Page/中英談判.md "wikilink")
  - [中華人民共和國政府和大不列顛及北愛爾蘭聯合王國政府關於香港問題的聯合聲明](../Page/中華人民共和國政府和大不列顛及北愛爾蘭聯合王國政府關於香港問題的聯合聲明.md "wikilink")
  - [舊英童學校](../Page/舊英童學校.md "wikilink")
  - [中英關係](../Page/中英關係.md "wikilink")

{{-}}

[Category:香港回归](../Category/香港回归.md "wikilink")
[Category:中华人民共和国中央政府历史](../Category/中华人民共和国中央政府历史.md "wikilink")
[Category:英国政府历史](../Category/英国政府历史.md "wikilink")
[Category:1985年香港建立](../Category/1985年香港建立.md "wikilink")
[Category:2000年香港废除](../Category/2000年香港废除.md "wikilink")
[Category:1985年建立的組織](../Category/1985年建立的組織.md "wikilink")
[Category:2000年解散的組織](../Category/2000年解散的組織.md "wikilink")

1.  [《中英聯合聲明》及其實施情況，政制及内地事務局，于2010-11-19查閲](http://www.cmab.gov.hk/tc/issues/joint2.htm#3)
2.  宗道一等編（2007）《周南口述：遙想當年羽扇綸巾》，濟南：齊魯書社，2007年6月初版，288至293頁
3.  [中英聯合聯絡小組最後一次會議將在香港舉行，中新社，香港1999年11月30日電](http://big5.chinanews.com.cn:89/1999-12-1/26/9753.html)