《**中華五千年**》是一個[香港電台廣播節目](../Page/香港電台.md "wikilink")，一九八三年四月四日開播\[1\]至二千年停播，一共九百集，為香港電台[最長壽的歷史教育](../Page/長壽節目.md "wikilink")[廣播劇](../Page/廣播劇.md "wikilink")。

## 概述

這個節目由香港歷史學者[張偉國撰稿](../Page/張偉國.md "wikilink")。最初由[魏便利主持](../Page/魏便利.md "wikilink")，編導[鄭啟明間中亦有主持](../Page/鄭啟明.md "wikilink")（由明朝時期開始）。由清朝後期開始，[梁家永加入主持行列](../Page/梁家永.md "wikilink")；在民國以後，梁家永正式接任主持，鄭啟明則繼續間中主持。開場及尾場分別有「播音皇帝」[鍾偉明報幕](../Page/鍾偉明.md "wikilink")，及後由[蔡雅各報幕](../Page/蔡雅各.md "wikilink")，最後尾場由主持報幕。

本節目的開場曲及收場曲，均出自《[黃河鋼琴協奏曲](../Page/黃河鋼琴協奏曲.md "wikilink")》前奏《黃河船夫曲》1970年代的版本。在第一集中最初定出整套廣播劇由[原始時代到](../Page/原始時代.md "wikilink")[文化大革命為止](../Page/文化大革命.md "wikilink")，但最後至[中華人民共和國開國為止](../Page/中华人民共和国开国大典.md "wikilink")。第九百集為總結。

介紹對每個時代政治、文化、社會有重大影響的事件。除了以講述以外，還會依據史實以戲劇化形式表達出來。

節目以中學生及對歷史感興趣的家庭聽眾為主，播出後聽眾反應熱烈，並且得到教育等各界支持\[2\]。香港電台在1987年4月12日為紀念本節目播出4週年，舉辦午餐座談會，邀請了[司徒華任主禮嘉賓及作專題演講](../Page/司徒華.md "wikilink")\[3\]。由於節目深受歡迎，香港電台其後推出同類型節目《[神州五十年](../Page/神州五十年.md "wikilink")》；而且為方便市民大眾，香港電台將全數九百集以[RealMedia形式上載到網頁](../Page/RealMedia.md "wikilink")\[4\]，及於2013年推出[流動應用程式](../Page/流動應用程式.md "wikilink")《中華五千年盛世版》。近年因應在[香港電台第五台](../Page/香港電台第五台.md "wikilink")《文化四合院》時段重播，香港電台推出聲音清晰的[podcast版](../Page/podcast.md "wikilink")，每週更新一次，與重播的重溫同步推出。

## 內容

中華五千年對古代的史事著墨較少，對近代的愈來愈多。

  - 001集－ 40集：原始時代、夏、商、周、春秋戰國
  - 041集－ 84集：秦朝、西漢、東漢
  - 085集－102集：三國時代
  - 103集－152集：西晉、五胡十六國、魏晉南北朝
  - 152集－189集：隋代、唐代、貞觀之治、武則天、開玄之治
  - 190集－260集：安史之亂、藩鎮割據、牛李黨爭、黃巢之亂、遼代、五代十國
  - 261集－321集：北宋、王安石變法
  - 322集－353集：南宋、金代
  - 354集－427集：蒙古興起、吞金滅宋、元朝
  - 427集－467集：朱元璋興起、明朝建立、靖難之變、明成祖永樂皇帝、鄭和
  - 468集－525集：明朝中葉
  - 526集－584集：滿洲興起、明末民變、滿清入關、鄭成功抗清
  - 585集－638集：康熙、雍正、乾隆
  - 639集－689集：清朝中葉、列強環峙
  - 690集－728集：甲午之戰、百日維新、孫中山革命、武昌起義
  - 729集－781集：袁世凱、北洋政府、五四運動、國民革命軍、中共成立、蔣介石北伐、軍閥混戰
  - 782集－849集：日軍侵華、八年抗戰
  - 850集－899集：國共內戰、中共建政
  - 900集：〈總結〉

## 年度與集數

  - 1983年4月4日（星期一）首播，逢週一播放。
  - 首年播放了39集。此後每年約播放52集，到2000年完結。

<table>
<thead>
<tr class="header">
<th><p>播放年份</p></th>
<th><p>由</p></th>
<th><p>至</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1983</strong></p></td>
<td><p>001集：軒轅黃帝</p></td>
<td><p>039集：秦王政即位</p></td>
<td><p><small>1983年12月26日晚上七時，香港電台第一台播放第39回</small>[5]</p></td>
</tr>
<tr class="even">
<td><p><strong>1984</strong></p></td>
<td><p>040集：韓非與李斯</p></td>
<td><p>091集：割據江東</p></td>
<td><p><small>1984年12月31日晚上七時，香港電台第一台播放第91回</small>[6]<br />
<small>1984年9月17日轉播廣州亞洲盃外圍賽，節目暫停一次</small></p></td>
</tr>
<tr class="odd">
<td><p><strong>1985</strong></p></td>
<td><p>092集：三顧草蘆</p></td>
<td><p>142集：蘇綽定制</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1986</strong></p></td>
<td><p>143集：梁朝文風</p></td>
<td><p>194集</p></td>
<td><p><small>1986年1月6日晚上七時，香港電台第一台播放第143回</small>[7]</p></td>
</tr>
<tr class="odd">
<td><p><strong>1987</strong></p></td>
<td><p>195集</p></td>
<td><p>245集</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1988</strong></p></td>
<td><p>246集</p></td>
<td><p>296集</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1989</strong></p></td>
<td><p>297集</p></td>
<td><p>347集</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1990</strong></p></td>
<td><p>348集</p></td>
<td><p>399集</p></td>
<td><p><small>1990年9月16日週日重播第384回「漢法之爭」；<br />
09月23日週日重播第385回「釣魚城之戰」</small>[8]</p></td>
</tr>
<tr class="odd">
<td><p><strong>1991</strong></p></td>
<td><p>400集</p></td>
<td><p>450集</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1992</strong></p></td>
<td><p>451集</p></td>
<td><p>500集</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1993</strong></p></td>
<td><p>501集</p></td>
<td><p>550集</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1994</strong></p></td>
<td><p>551集</p></td>
<td><p>600集</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1995</strong></p></td>
<td><p>601集</p></td>
<td><p>651集：林則徐禁煙</p></td>
<td><p><small>1995年12月25日播出第651回</small></p></td>
</tr>
<tr class="even">
<td><p><strong>1996</strong></p></td>
<td><p>652集：鴉片戰爭</p></td>
<td><p>701集</p></td>
<td><p><small>1996年1月1日播出第652回</small><br />
<small>1996年5月6日播放第669回「忠王東征」；<br />
05月13日播放第670回「祁門統兵」</small></p></td>
</tr>
<tr class="odd">
<td><p><strong>1997</strong></p></td>
<td><p>702集</p></td>
<td><p>753集</p></td>
<td><p><small>1997年12月1日播放第749回「孫越會談」；<br />
12月8日播放第750回「國共合作」</small></p></td>
</tr>
<tr class="even">
<td><p><strong>1998</strong></p></td>
<td><p>754集</p></td>
<td><p>806集</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1999</strong></p></td>
<td><p>807集</p></td>
<td><p>858集</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2000</strong></p></td>
<td><p>859集</p></td>
<td><p>900集：總結、回顧</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 播音員

演員主要為香港電台廣播劇組藝員及演藝組。

[鍾偉明飾演](../Page/鍾偉明.md "wikilink")[鄭玄](../Page/鄭玄.md "wikilink")、[劉備](../Page/劉備.md "wikilink")、[嵇康](../Page/嵇康.md "wikilink")、[魏徵](../Page/魏徵.md "wikilink")、[宋璟](../Page/宋璟.md "wikilink")、[後唐明宗](../Page/後唐明宗.md "wikilink")、[宋太祖](../Page/宋太祖.md "wikilink")、[楊業](../Page/楊業.md "wikilink")、[寇準](../Page/寇準.md "wikilink")、[文彥博](../Page/文彥博.md "wikilink")、南宋[張浚](../Page/張浚.md "wikilink")、金代[王重陽](../Page/王重陽.md "wikilink")、蒙古[也速該](../Page/也速該.md "wikilink")、清代[李光地](../Page/李光地.md "wikilink")、[利瑪竇](../Page/利瑪竇.md "wikilink")、[道衍](../Page/道衍.md "wikilink")、[曾國藩](../Page/曾國藩.md "wikilink")、[安治泰](../Page/安治泰.md "wikilink")、[孫中山](../Page/孫中山.md "wikilink")、[王國維](../Page/王國維.md "wikilink")、[方振武](../Page/方振武.md "wikilink")、[張羣](../Page/張羣.md "wikilink")、[鄭正秋](../Page/鄭正秋.md "wikilink")、[沈鈞儒](../Page/沈鈞儒.md "wikilink")、[陳濟棠](../Page/陳濟棠.md "wikilink")、[毛怡卿](../Page/毛怡卿.md "wikilink")、[宋哲元](../Page/宋哲元.md "wikilink")、[趙戴文](../Page/趙戴文.md "wikilink")、[熊式輝](../Page/熊式輝.md "wikilink")、[盧漢](../Page/盧漢.md "wikilink")、[羅富國](../Page/羅富國.md "wikilink")、[楊慕琦](../Page/楊慕琦.md "wikilink")、[羅斯福](../Page/富蘭克林·德拉諾·羅斯福.md "wikilink")、[毛澤民](../Page/毛澤民.md "wikilink")、[陳布雷](../Page/陳布雷.md "wikilink")、[劉伯承](../Page/劉伯承.md "wikilink")、[陳寅恪](../Page/陳寅恪.md "wikilink")、[宋希濂](../Page/宋希濂.md "wikilink")、[馬步芳等人物](../Page/馬步芳.md "wikilink")。

[李再唐飾演的歷史人物](../Page/李再唐.md "wikilink")，例如[秦始皇](../Page/秦始皇.md "wikilink")、[漢武帝](../Page/漢武帝.md "wikilink")、[關羽](../Page/關羽.md "wikilink")、後趙[石虎](../Page/石虎.md "wikilink")、[唐太宗](../Page/唐太宗.md "wikilink")、[孟浩然](../Page/孟浩然.md "wikilink")、[後梁太祖](../Page/後梁太祖.md "wikilink")、[王安石](../Page/王安石.md "wikilink")、[岳飛](../Page/岳飛.md "wikilink")、[拖雷](../Page/拖雷.md "wikilink")、[明成祖](../Page/明成祖.md "wikilink")、[張居正](../Page/張居正.md "wikilink")、[小西行長](../Page/小西行長.md "wikilink")、[多爾袞](../Page/多爾袞.md "wikilink")、法國國王[路易十四](../Page/路易十四.md "wikilink")、[褚英](../Page/褚英.md "wikilink")、[清高宗乾隆皇帝](../Page/清高宗.md "wikilink")、[洪秀全](../Page/洪秀全.md "wikilink")、[袁世凱](../Page/袁世凱.md "wikilink")、[毛澤東等人物](../Page/毛澤東.md "wikilink")。

[李學斌飾演](../Page/李學斌_\(電台主持\).md "wikilink")[軒轅黃帝](../Page/軒轅黃帝.md "wikilink")、[舜](../Page/舜.md "wikilink")、[禹](../Page/禹.md "wikilink")、[成湯](../Page/成湯.md "wikilink")、[張儀](../Page/張儀.md "wikilink")、[孫臏](../Page/孫臏.md "wikilink")、[趙武靈王](../Page/趙武靈王.md "wikilink")、[荊軻](../Page/荊軻.md "wikilink")、[張良](../Page/張良.md "wikilink")、[漢文帝](../Page/漢文帝.md "wikilink")、[衛青](../Page/衛青.md "wikilink")、[李陵](../Page/李陵.md "wikilink")、[漢宣帝](../Page/漢宣帝.md "wikilink")、[諸葛亮](../Page/諸葛亮.md "wikilink")、[祖逖](../Page/祖逖.md "wikilink")、[王猛](../Page/王猛.md "wikilink")、[北魏太武帝](../Page/北魏太武帝.md "wikilink")、陳朝[陳霸先](../Page/陳霸先.md "wikilink")、[薛仁貴等人物](../Page/薛仁貴.md "wikilink")。

[朱曼子飾演](../Page/朱曼子.md "wikilink")[嫘祖](../Page/嫘祖.md "wikilink")、[娥皇](../Page/娥皇.md "wikilink")、[申后](../Page/申后.md "wikilink")、[趙莊姬](../Page/趙莊姬.md "wikilink")、[趙惠后](../Page/趙惠后.md "wikilink")、[如姬](../Page/如姬.md "wikilink")、[呂后](../Page/呂后.md "wikilink")、[衛子夫](../Page/衛子夫.md "wikilink")、[孝宣王皇后](../Page/孝宣王皇后.md "wikilink")、[王昭君](../Page/王昭君.md "wikilink")、[和熹皇后](../Page/和熹皇后.md "wikilink")、[竇妙](../Page/竇妙.md "wikilink")、[綠珠](../Page/綠珠.md "wikilink")、[張夫人](../Page/張夫人_\(前秦宣昭帝\).md "wikilink")、[王貞風](../Page/王貞風.md "wikilink")、[章要兒](../Page/章要兒.md "wikilink")、[宣華夫人](../Page/宣華夫人.md "wikilink")、[長孫皇后](../Page/長孫皇后.md "wikilink")、[文成公主](../Page/文成公主.md "wikilink")、[武則天](../Page/武則天.md "wikilink")、遼聖宗朝[蕭太后](../Page/蕭綽.md "wikilink")、金末[楊妙真](../Page/楊妙真.md "wikilink")、清高宗[繼皇后](../Page/繼皇后.md "wikilink")、[慈禧太后](../Page/慈禧太后.md "wikilink")(八國聯軍入京前)、[宋慶齡及](../Page/宋慶齡.md "wikilink")[貝絲·杜魯門等人物](../Page/貝絲·杜魯門.md "wikilink")。

[蔡雅各飾演](../Page/蔡雅各.md "wikilink")[李廣利](../Page/李廣利.md "wikilink")、[漢景帝](../Page/漢景帝.md "wikilink")、[曹植](../Page/曹植.md "wikilink")、[唐中宗](../Page/唐中宗.md "wikilink")、[高仙芝](../Page/高仙芝.md "wikilink")、[唐肅宗](../Page/唐肅宗.md "wikilink")、[牟羽可汗](../Page/牟羽可汗.md "wikilink")、[唐德宗](../Page/唐德宗.md "wikilink")、北宋[呂蒙正](../Page/呂蒙正.md "wikilink")<sub>（273回）</sub>、[蘇轍](../Page/蘇轍.md "wikilink")、南宋[韓世忠](../Page/韓世忠.md "wikilink")、[虞允文](../Page/虞允文.md "wikilink")、蒙古[札木合](../Page/札木合.md "wikilink")、清代[努爾哈赤](../Page/努爾哈赤.md "wikilink")。

[簡偉安飾演](../Page/簡偉安.md "wikilink")[周武王](../Page/周武王.md "wikilink")、[周成王](../Page/周成王.md "wikilink")、[文種](../Page/文種.md "wikilink")、[秦二世](../Page/秦二世.md "wikilink")、[項莊](../Page/項莊.md "wikilink")、[漢哀帝](../Page/漢哀帝.md "wikilink")、[太史慈](../Page/太史慈.md "wikilink")、[劉禪](../Page/劉禪.md "wikilink")、[曹髦](../Page/曹髦.md "wikilink")、[拓跋珪](../Page/拓跋珪.md "wikilink")、北魏[孝明帝](../Page/魏孝明帝.md "wikilink")、[宇文邕](../Page/宇文邕.md "wikilink")、唐代孝達和尚<sub>（164回）</sub>等。

[林友榮飾演](../Page/林友榮.md "wikilink")[屈原](../Page/屈原.md "wikilink")、[劉邦](../Page/劉邦.md "wikilink")、[晁錯](../Page/晁錯.md "wikilink")、[司馬懿](../Page/司馬懿.md "wikilink")、[唐高宗](../Page/唐高宗.md "wikilink")、[高力士](../Page/高力士.md "wikilink")、[杜甫](../Page/杜甫.md "wikilink")、金[兀朮](../Page/兀朮.md "wikilink")、金末[李全](../Page/李全.md "wikilink")、清代[柴大紀](../Page/柴大紀.md "wikilink")、[道光皇帝](../Page/道光皇帝.md "wikilink")、[威妥瑪](../Page/威妥瑪.md "wikilink")、[康有為](../Page/康有為.md "wikilink")、[黎元洪](../Page/黎元洪.md "wikilink")、[馮國璋](../Page/馮國璋.md "wikilink")、[錢能訓](../Page/錢能訓.md "wikilink")、[蔣介石等人物](../Page/蔣介石.md "wikilink")。

[陳炳球飾演](../Page/陳炳球.md "wikilink")[帝堯](../Page/帝堯.md "wikilink")、[伊尹](../Page/伊尹.md "wikilink")、[周公旦](../Page/周公旦.md "wikilink")、[孔子](../Page/孔子.md "wikilink")、[廉頗](../Page/廉頗.md "wikilink")、[魏延](../Page/魏延.md "wikilink")、遼代[耶律乙辛](../Page/耶律乙辛.md "wikilink")、北宋[呂惠卿](../Page/呂惠卿.md "wikilink")、蒙古[克烈部](../Page/克烈部.md "wikilink")[王汗](../Page/王汗.md "wikilink")，還有[佐治·邦雅曼·克里蒙梭](../Page/乔治·克列孟梭.md "wikilink")、[張景惠](../Page/張景惠.md "wikilink")、[周壽臣](../Page/周壽臣.md "wikilink")、[榮宗敬](../Page/榮宗敬.md "wikilink")、[魏德邁等人物](../Page/魏德邁.md "wikilink")。

[曾永強飾演姜太公](../Page/曾永強.md "wikilink")[呂尚](../Page/呂尚.md "wikilink")、[孫武](../Page/孫武.md "wikilink")、[趙高](../Page/趙高.md "wikilink")、[張角](../Page/張角.md "wikilink")、[王允](../Page/王允.md "wikilink")、[狄仁傑](../Page/狄仁傑.md "wikilink")、[李林甫](../Page/李林甫.md "wikilink")、[李白](../Page/李白.md "wikilink")、[史浩](../Page/史浩.md "wikilink")、[王振](../Page/王振.md "wikilink")、[葉夢熊](../Page/葉夢熊.md "wikilink")、[詹姆斯·康德黎](../Page/詹姆斯·康德黎.md "wikilink")、[奕劻](../Page/奕劻.md "wikilink")、[馬林](../Page/马林_\(共产国际\).md "wikilink")<small>（共產國際）</small>、[梁士詒](../Page/梁士詒.md "wikilink")、[陸榮廷](../Page/陸榮廷.md "wikilink")、[陸征祥](../Page/陸征祥.md "wikilink")、[吳稚暉](../Page/吳稚暉.md "wikilink")、[吳佩孚](../Page/吳佩孚.md "wikilink")、[孔祥熙](../Page/孔祥熙.md "wikilink")、[閻錫山](../Page/閻錫山.md "wikilink")、[梅貽琦](../Page/梅貽琦.md "wikilink")、[磯谷廉介](../Page/磯谷廉介.md "wikilink")、[周佛海](../Page/周佛海.md "wikilink")、[酆悌](../Page/酆悌.md "wikilink")、[科德爾·赫爾](../Page/科德爾·赫爾.md "wikilink")、[顧祝同](../Page/顧祝同.md "wikilink")、[鈴木貫太郎](../Page/鈴木貫太郎.md "wikilink")、[林獻堂](../Page/林獻堂.md "wikilink")、[葛量洪](../Page/葛量洪.md "wikilink")、[董必武等人物](../Page/董必武.md "wikilink")。

[莫家駒飾演](../Page/莫家駒.md "wikilink")[夏桀](../Page/夏桀.md "wikilink")、[子路](../Page/子路.md "wikilink")、[樊噲](../Page/樊噲.md "wikilink")、[張飛](../Page/張飛.md "wikilink")、[雍闓](../Page/雍闓.md "wikilink")、[安祿山](../Page/安祿山.md "wikilink")、[周祖培](../Page/周祖培.md "wikilink")、[克洛德維希·卡爾·維克托](../Page/克洛德維希·卡爾·維克托.md "wikilink")、[崔玉貴](../Page/崔玉貴.md "wikilink")、[倪嗣冲](../Page/倪嗣冲.md "wikilink")、[靳雲鵬](../Page/靳雲鵬.md "wikilink")、[陳炯明](../Page/陳炯明.md "wikilink")、[吳景濂](../Page/吳景濂.md "wikilink")、[馮玉祥](../Page/馮玉祥.md "wikilink")、[張作相](../Page/張作相.md "wikilink")、[謝晉元](../Page/謝晉元.md "wikilink")、[彭德懷](../Page/彭德懷.md "wikilink")、[莫德庇](../Page/莫德庇.md "wikilink")、[陳光](../Page/陳光.md "wikilink")、[湯恩伯](../Page/湯恩伯.md "wikilink")、[任弼時](../Page/任弼時.md "wikilink")、[史太林](../Page/史太林.md "wikilink")、[傅作義](../Page/傅作義.md "wikilink")、[李彌等人物](../Page/李彌.md "wikilink")。

[溫　泉飾演](../Page/溫泉_\(播音員\).md "wikilink")[董仲舒](../Page/董仲舒.md "wikilink")、[司馬遷](../Page/司馬遷.md "wikilink")、[班固](../Page/班固.md "wikilink")、三國時代[劉放](../Page/劉放.md "wikilink")、[鄧艾](../Page/鄧艾.md "wikilink")，北宋[呂誨](../Page/呂誨.md "wikilink")，元太宗[窩闊台](../Page/窩闊台.md "wikilink")，還有[朱弘昭](../Page/朱弘昭.md "wikilink")、[楊鎬](../Page/楊鎬.md "wikilink")、[豐臣秀吉](../Page/豐臣秀吉.md "wikilink")、[榮祿](../Page/榮祿.md "wikilink")、[瑞澂](../Page/瑞澂.md "wikilink")、[張勳](../Page/張勳.md "wikilink")、[牧野伸顯](../Page/牧野伸顯.md "wikilink")、[張作霖](../Page/張作霖.md "wikilink")、[陳維周](../Page/陳維周.md "wikilink")、[黃金榮](../Page/黃金榮.md "wikilink")、[韓復榘](../Page/韓復榘.md "wikilink")、[鄭孝胥](../Page/鄭孝胥.md "wikilink")、[何東](../Page/何東.md "wikilink")、[盛世才](../Page/盛世才.md "wikilink")、[曾養甫](../Page/曾養甫.md "wikilink")、[龍雲](../Page/龍雲.md "wikilink")、[東條英機](../Page/東條英機.md "wikilink")、[陳儀等人物](../Page/陳儀.md "wikilink")。

[楊麗仙飾演](../Page/楊麗仙.md "wikilink")[女英](../Page/女英.md "wikilink")、[何太后](../Page/何太后.md "wikilink")、[楊麗華](../Page/楊麗華.md "wikilink")、[楊貴妃等](../Page/楊貴妃.md "wikilink")。

[車森梅飾演](../Page/車森梅.md "wikilink")[虞姬](../Page/虞姬.md "wikilink")、[趙飛燕](../Page/趙飛燕.md "wikilink")、[拖雷王妃](../Page/拖雷.md "wikilink")[莎兒合黑帖妮](../Page/唆魯禾帖尼.md "wikilink")、[吳敬君](../Page/吳敬君.md "wikilink")、[宋美齡](../Page/宋美齡.md "wikilink")。

[姚秀鈴飾演](../Page/姚秀鈴.md "wikilink")[張麗華](../Page/張麗華.md "wikilink")、[上官婉兒](../Page/上官婉兒.md "wikilink")、[珍妃](../Page/珍妃.md "wikilink")、[溥儀](../Page/溥儀.md "wikilink")(童年)、[丁玲](../Page/丁玲.md "wikilink")、[裘麗琳](../Page/裘麗琳.md "wikilink")、[崔月犁等人物](../Page/崔月犁.md "wikilink")。

[丁茵飾演](../Page/丁茵.md "wikilink")[班昭](../Page/班昭.md "wikilink")、[孫太夫人](../Page/孫破虜吳夫人.md "wikilink")<small>（孫策、孫權生母）</small>、[北魏馮太后](../Page/北魏馮太后.md "wikilink")、[田川氏
(鄭成功母)](../Page/田川氏_\(鄭成功母\).md "wikilink")、[孝莊文皇后](../Page/孝莊文皇后.md "wikilink")<small>（順治帝生母）</small>、[慈禧太后](../Page/慈禧太后.md "wikilink")
(五大臣出洋<small>(718回)</small>以後)。

[譚翠蓮飾演陳朝](../Page/譚翠蓮.md "wikilink")[冼夫人](../Page/冼夫人.md "wikilink")、蒙古[月倫太后](../Page/訶額侖.md "wikilink")<sub>（第354-355回）</sub>、元太宗皇后[脫列哥那](../Page/脫列哥那.md "wikilink")、[宋靄齡](../Page/宋靄齡.md "wikilink")、[陳璧君](../Page/陳璧君.md "wikilink")、[毛福梅等人物](../Page/毛福梅.md "wikilink")。

[勞韻妍飾演](../Page/勞韻妍.md "wikilink")[華陽夫人](../Page/華陽夫人.md "wikilink")、[太平公主等](../Page/太平公主.md "wikilink")。

[謝蘊儀飾演](../Page/謝蘊儀.md "wikilink")[武宣卞皇后](../Page/武宣卞皇后.md "wikilink")<small>（曹丕、曹植生母）</small>、[蕭文壽](../Page/蕭文壽.md "wikilink")，金代[元好問母親王氏](../Page/元好問.md "wikilink")<sub>（365回）</sub>。

足球評論員[李德能長年活躍於廣播劇演出](../Page/李德能.md "wikilink")，飾演東漢[班勇](../Page/班勇.md "wikilink")，三國時代[關平](../Page/關平.md "wikilink")、[李勝](../Page/李勝.md "wikilink")，五胡十六國[苻堅](../Page/苻堅.md "wikilink")，梁代[王僧辯](../Page/王僧辯.md "wikilink")，唐代[唐睿宗](../Page/唐睿宗.md "wikilink")、[楊國忠](../Page/楊國忠.md "wikilink")、[史朝義](../Page/史朝義.md "wikilink")，晚唐宰相[李德裕](../Page/李德裕.md "wikilink")，[遼道宗](../Page/遼道宗.md "wikilink")、[耶律大石](../Page/耶律大石.md "wikilink")，北宋[王欽若](../Page/王欽若.md "wikilink")、南宋[曲端](../Page/曲端.md "wikilink")，[成吉思汗](../Page/成吉思汗.md "wikilink")、[明太祖](../Page/明太祖.md "wikilink")、[清太宗](../Page/清太宗.md "wikilink")、民國[馬師曾等人物](../Page/馬師曾.md "wikilink")。

[倪秉郎飾演](../Page/倪秉郎.md "wikilink")[楚莊王](../Page/楚莊王.md "wikilink")<sub>（013回）</sub>、[謝石](../Page/謝石.md "wikilink")<sub>（116回）</sub>、[惠能](../Page/惠能.md "wikilink")<sub>（179回）</sub>。

飾演[趙孝成王](../Page/趙孝成王.md "wikilink")、[夏侯嬰](../Page/夏侯嬰.md "wikilink")、[霍去病](../Page/霍去病.md "wikilink")、[呂隆](../Page/呂隆.md "wikilink")。

[何錦華飾演](../Page/何錦華.md "wikilink")[文姜](../Page/文姜.md "wikilink")、[驪姬](../Page/驪姬.md "wikilink")、[孝景王皇后](../Page/孝景王皇后.md "wikilink")、[鄂邑公主等](../Page/鄂邑公主.md "wikilink")。

[周影飾演唐代](../Page/周影.md "wikilink")[永泰公主](../Page/永泰公主.md "wikilink")。

[殷勤](../Page/殷勤.md "wikilink")（胡榮堅）飾演唐代[南霽雲](../Page/南霽雲.md "wikilink")<sub>（192回）</sub>、[李惟岳](../Page/李惟岳.md "wikilink")<sub>（201回）</sub>、[姚令言](../Page/姚令言.md "wikilink")<sub>（202回）</sub>。

[高俊](../Page/高俊_\(播音員\).md "wikilink")（嚴照昌）飾演[令狐潮](../Page/令狐潮.md "wikilink")<sub>（192回）</sub>。

[羅嘉玲飾演蒙古](../Page/羅嘉玲.md "wikilink")[帖木侖](../Page/帖木侖.md "wikilink")<sub>（第354回）</sub>、元代魯國公主[祥哥剌吉](../Page/祥哥剌吉.md "wikilink")<sub>（第409、415回）</sub>、韃靼[三娘子](../Page/三娘子.md "wikilink")<sub>（503回）</sub>、明末名妓[柳如是](../Page/柳如是.md "wikilink")<sub>（553-554回）</sub>。

[劉昭文飾演](../Page/劉昭文.md "wikilink")[遼太宗耶律德光](../Page/遼太宗.md "wikilink")<sub>（249回）</sub>、[遼穆宗](../Page/遼穆宗.md "wikilink")<sub>（269回）</sub>、北宋[石守信](../Page/石守信.md "wikilink")、[宋真宗](../Page/宋真宗.md "wikilink")、[岳飛部將](../Page/岳飛.md "wikilink")[張顯](../Page/張顯.md "wikilink")、金朝[李通](../Page/李通_\(金\).md "wikilink")、蒙古[合撒兒](../Page/合撒兒.md "wikilink")<sub>（第355回）</sub>。

[蔡映玉飾演遼朝](../Page/蔡映玉.md "wikilink")[述律太后](../Page/述律平.md "wikilink")<sub>（249回）</sub>、宋太宗[李皇后](../Page/明德李皇后_\(宋朝\).md "wikilink")<sub>（274回）</sub>、遼朝趙國公主[耶律糾里](../Page/耶律糾里.md "wikilink")<sub>（301回）</sub>、南宋[梁紅玉](../Page/梁紅玉.md "wikilink")<sub>（325回）</sub>、蒙古豁阿婆婆（老僕豁阿黑臣）<sub>（第355回）</sub>。

[曾月娥飾演](../Page/曾月娥.md "wikilink")[小周后](../Page/小周后.md "wikilink")、[李清照](../Page/李清照.md "wikilink")、[鐵木真夫人](../Page/鐵木真.md "wikilink")[孛兒帖](../Page/孛兒帖.md "wikilink")<sub>（第355回）</sub>，還有[婉容](../Page/婉容.md "wikilink")、[龍國壁](../Page/龍國壁.md "wikilink")、[章亞若等人物](../Page/章亞若.md "wikilink")。

[翟耀輝飾演安南丁朝先皇](../Page/翟耀輝.md "wikilink")[丁部領](../Page/丁部領.md "wikilink")<sub>（264回）</sub>、北宋[慕容延釗](../Page/慕容延釗.md "wikilink")<sub>（262回）</sub>、[張齊賢](../Page/張齊賢.md "wikilink")<sub>（273回）</sub>、南宋[張俊](../Page/張俊.md "wikilink")<sub>（328回）</sub>、金朝[完顏亮](../Page/完顏亮.md "wikilink")、[丘處機](../Page/丘處機.md "wikilink")（青年）、民國[吳宓](../Page/吳宓.md "wikilink")、[石敬亭等人物](../Page/石敬亭.md "wikilink")。

[張建浩飾演契丹太子](../Page/張建浩.md "wikilink")[耶律倍](../Page/耶律倍.md "wikilink")<sub>（249回）</sub>、遼道宗皇太子[耶律濬](../Page/耶律濬.md "wikilink")<sub>（301回）</sub>、[宋孝宗](../Page/宋孝宗.md "wikilink")，還有[載灃](../Page/載灃.md "wikilink")、[秦德純](../Page/秦德純.md "wikilink")、[陳潭秋](../Page/陳潭秋.md "wikilink")、[劉少奇](../Page/劉少奇.md "wikilink")、[費正清等人物](../Page/費正清.md "wikilink")。

[鍾傑良](../Page/鍾傑良.md "wikilink")（鍾傑武）飾演遼道宗駙馬[蕭撻不也](../Page/蕭撻不也.md "wikilink")<sub>（301回）</sub>、十四阿哥[胤禵](../Page/胤禵.md "wikilink")、[奕訢](../Page/奕訢.md "wikilink")、[李經方](../Page/李經方.md "wikilink")、[袁克定](../Page/袁克定.md "wikilink")、[曹銳](../Page/曹銳.md "wikilink")、[瓦西里·康斯坦丁諾維奇·布留赫爾](../Page/瓦西里·康斯坦丁諾維奇·布留赫爾.md "wikilink")、[溥儀](../Page/溥儀.md "wikilink")、[酒井隆](../Page/酒井隆.md "wikilink")、[華克之](../Page/華克之.md "wikilink")、[佟麟閣](../Page/佟麟閣.md "wikilink")、[楊成武](../Page/楊成武.md "wikilink")、[朱紹良](../Page/朱紹良.md "wikilink")、[龍繩祖](../Page/龍繩祖.md "wikilink")、[蔣渭川等人物](../Page/蔣渭川.md "wikilink")</sub>。

[吳忠泰飾演](../Page/吳忠泰_\(播音員\).md "wikilink")[葉名琛](../Page/葉名琛.md "wikilink")。

## 客串

  - 每逢講述到了特別精彩歷史時代的故事，比如三國時代、北宋王安石變法、反清革命、國共內戰，就會邀請其他著名播音前輩、知名配音員、唱片騎師（DJ）、節目主持，甚至明星、教育等界別人士客串。也曾邀得前任香港民政事務局局長[何志平演出](../Page/何志平.md "wikilink")\[9\]。
  - 九十年代中期開始，邀請更多香港電台各節目主持、港台其他部門職員參與演出。

[程乃根](../Page/程乃根.md "wikilink") 飾演
[何進](../Page/何進.md "wikilink")、[玄奘法師](../Page/玄奘.md "wikilink")<sub>（164、169回）</sub>等人物。

[朱克](../Page/朱克.md "wikilink") 飾演 [董卓](../Page/董卓.md "wikilink")。

[李學儒](../Page/李學儒.md "wikilink") 飾演
[呂布](../Page/呂布.md "wikilink")、英國大使[巴夏禮](../Page/巴夏禮.md "wikilink")（Sir
Harry Smith Parkes）。

[李夏威](../Page/李夏威.md "wikilink") 飾演 [曹操](../Page/曹操.md "wikilink")。

[何嘉麗](../Page/何嘉麗_\(歌手\).md "wikilink") 飾演
[漢獻帝](../Page/漢獻帝.md "wikilink")（童年）。

[熊良錫飾演](../Page/熊良錫.md "wikilink")[荀彧](../Page/荀彧.md "wikilink")。

[黃兆強飾演](../Page/黃兆強.md "wikilink")[孔融](../Page/孔融.md "wikilink")。

[黃志成飾演](../Page/黃志成.md "wikilink")[孫策](../Page/孫策.md "wikilink")。

[張炳強飾演](../Page/張炳強.md "wikilink")[周瑜](../Page/周瑜.md "wikilink")。

[源家祥飾演](../Page/源家祥.md "wikilink")[魯肅](../Page/魯肅.md "wikilink")。

[林超榮飾演](../Page/林超榮.md "wikilink")[陸績](../Page/陸績.md "wikilink")、[姜維](../Page/姜維.md "wikilink")，還有[賈似道幕客廖瑩中](../Page/賈似道.md "wikilink")<sub>（387回）</sub>。

主持[梁家永飾演](../Page/梁家永.md "wikilink")[楊修](../Page/楊修.md "wikilink")、[章太炎](../Page/章太炎.md "wikilink")。

[盧偉力飾演](../Page/盧偉力.md "wikilink")[孫權](../Page/孫權.md "wikilink")、[鍾會](../Page/鍾會.md "wikilink")、[隋煬帝](../Page/隋煬帝.md "wikilink")、[唐玄宗](../Page/唐玄宗.md "wikilink")、[伍廷芳](../Page/伍廷芳.md "wikilink")、[李大釗](../Page/李大釗.md "wikilink")、[周信芳](../Page/周信芳.md "wikilink")、[聞一多](../Page/聞一多.md "wikilink")、[丘吉爾](../Page/丘吉爾.md "wikilink")、[曾聯松等人物](../Page/曾聯松.md "wikilink")。

[鄭丹瑞飾演](../Page/鄭丹瑞.md "wikilink")[曹丕](../Page/曹丕.md "wikilink")、[王維](../Page/王維.md "wikilink")。

[陳毓祥飾演](../Page/陳毓祥.md "wikilink")[李恢](../Page/李恢_\(三國\).md "wikilink")。

[麥潤壽飾演](../Page/麥潤壽.md "wikilink")[馬謖](../Page/馬謖.md "wikilink")。

[黃浩義飾演三國時代](../Page/黃浩義.md "wikilink")[司馬師](../Page/司馬師.md "wikilink")。

[曲家穗飾演](../Page/曲家穗.md "wikilink")[陳平](../Page/陈平_\(汉朝\).md "wikilink")、[袁紹](../Page/袁紹.md "wikilink")、[孫資](../Page/孫資.md "wikilink")。

[陳潤成飾演](../Page/陳潤成.md "wikilink") [曹爽](../Page/曹爽.md "wikilink")。

[陳中堅](../Page/陳中堅.md "wikilink") 飾演 [桓範](../Page/桓範.md "wikilink")。

[蕭亮有一段時間替香港電台主持節目](../Page/蕭亮.md "wikilink")，曾飾演三國時代[司馬昭](../Page/司馬昭.md "wikilink")、宋代[司馬光](../Page/司馬光.md "wikilink")。

[伍家廉飾演](../Page/伍家廉.md "wikilink")[徐庶](../Page/徐庶.md "wikilink")、[宮崎滔天](../Page/宮崎滔天.md "wikilink")、[蔡鍔](../Page/蔡鍔.md "wikilink")、[越飛](../Page/越飛.md "wikilink")、[張學良等人物](../Page/張學良.md "wikilink")。

足球評論員[何鑑江飾演](../Page/何鑑江.md "wikilink")[孟獲](../Page/孟獲.md "wikilink")。

足球評論員[何靜江飾演](../Page/何靜江.md "wikilink")[岡村寧次](../Page/岡村寧次.md "wikilink")。

編導及主持人[鄭啟明曾飾演](../Page/鄭啟明.md "wikilink")[秦舞陽](../Page/秦舞陽.md "wikilink")、[漢元帝](../Page/漢元帝.md "wikilink")、[盧植](../Page/盧植.md "wikilink")、[魏明帝曹叡](../Page/魏明帝.md "wikilink")、印度[戒日王](../Page/戒日王朝.md "wikilink")<sub>（169回）</sub>、李後主[李煜](../Page/李煜.md "wikilink")、[范仲淹](../Page/范仲淹.md "wikilink")、[宋高宗趙構](../Page/宋高宗.md "wikilink")、[耶律楚材](../Page/耶律楚材.md "wikilink")、[李從榮](../Page/李從榮.md "wikilink")、[汪直](../Page/汪直_\(宦官\).md "wikilink")、[舒爾哈齊](../Page/舒爾哈齊.md "wikilink")、[光緒皇帝](../Page/光緒皇帝.md "wikilink")、[陸皓東](../Page/陸皓東.md "wikilink")、[廖仲愷](../Page/廖仲愷.md "wikilink")、[蔡元培](../Page/蔡元培.md "wikilink")、[曹雲祥](../Page/曹雲祥.md "wikilink")、[王明](../Page/王明.md "wikilink")、[瞿秋白](../Page/瞿秋白.md "wikilink")、[鄒韜奮](../Page/鄒韜奮.md "wikilink")、[貝德士](../Page/貝德士.md "wikilink")、[田漢](../Page/田漢.md "wikilink")、[戴安瀾](../Page/戴安瀾.md "wikilink")、[項英](../Page/項英.md "wikilink")、[何思源等人物](../Page/何思源.md "wikilink")。

編導[葉世雄飾演](../Page/葉世雄.md "wikilink")[荀攸](../Page/荀攸.md "wikilink")、[楊儀](../Page/楊儀.md "wikilink")、五代[李從厚](../Page/李從厚.md "wikilink")、遼代[張孝傑](../Page/張孝傑.md "wikilink")、清代[代善](../Page/代善.md "wikilink")、[鄧廷鏗](../Page/鄧廷鏗.md "wikilink")、民國[曹錕](../Page/曹錕.md "wikilink")、[陳廉伯](../Page/陳廉伯.md "wikilink")、[胡毅生](../Page/胡毅生.md "wikilink")、[榮德生](../Page/榮德生.md "wikilink")、[橋本群](../Page/橋本群.md "wikilink")、[康生](../Page/康生.md "wikilink")、[昭和天皇](../Page/昭和天皇.md "wikilink")、[王震等人物](../Page/王震.md "wikilink")。

[巢紅薇](../Page/巢紅薇.md "wikilink") 飾
[曹芳](../Page/曹芳.md "wikilink")<sub>（098回）</sub>、[嵇紹](../Page/嵇紹.md "wikilink")<sub>（100回）</sub>。

副[廣播處長](../Page/廣播處.md "wikilink")[戴健文](../Page/戴健文.md "wikilink") 飾
[王經](../Page/王經.md "wikilink")<sub>（099回）</sub>、[章士釗](../Page/章士釗.md "wikilink")、[鄧小平等人物](../Page/鄧小平.md "wikilink")。

[邵盧善](../Page/邵盧善.md "wikilink") 飾
[晉武帝](../Page/晉武帝.md "wikilink")<sub>（103回）</sub>、[姚興](../Page/姚興.md "wikilink")<sub>（122回）</sub>。

[梁麗芬](../Page/梁麗芬.md "wikilink") 飾
晉惠帝后[賈南風](../Page/賈南風.md "wikilink")<sub>（104回）</sub>

體育節目主持[梁國滔](../Page/梁國滔.md "wikilink") 飾
[朱振](../Page/朱振.md "wikilink")<sub>（104回）</sub>、[慕容暐](../Page/慕容暐.md "wikilink")<sub>（117回）</sub>、[拓跋嗣](../Page/拓跋嗣.md "wikilink")<sub>（123回）</sub>、高昌人馬玄智<sub>（169回）</sub>、唐代[張易之](../Page/張易之.md "wikilink")<sub>（180-181回）</sub>。

[林珊珊](../Page/林珊珊.md "wikilink") 飾
[拓跋觚](../Page/拓跋觚.md "wikilink")<sub>（118回）</sub>。

[陳耀華](../Page/陳耀華.md "wikilink") 飾
[王凝之](../Page/王凝之.md "wikilink")<sub>（120回）</sub>、唐中宗太子[李重俊](../Page/李重俊.md "wikilink")<sub>（182回）</sub>、馬林
(共產國際)、[茅盾](../Page/茅盾.md "wikilink")、[徐向前等人物](../Page/徐向前.md "wikilink")。

[陳婉紅](../Page/陳婉紅.md "wikilink") 飾
隋煬帝[蕭皇后](../Page/蕭皇后_\(隋煬帝\).md "wikilink")<sub>（155回）</sub>、[唐雪卿](../Page/唐雪卿.md "wikilink")。

[傅菁葦](../Page/傅菁葦.md "wikilink") 飾
唐代[瓜州太守李昌](../Page/瓜州_\(古代\).md "wikilink")<sub>（164回）</sub>、[長孫無忌](../Page/長孫無忌.md "wikilink")。

[曾智華](../Page/曾智華.md "wikilink") 飾
[張虔陀](../Page/張虔陀.md "wikilink")<sub>（189回）</sub>。

[張菀容](../Page/張菀容.md "wikilink") 飾
[韓愈妻子盧氏](../Page/韓愈.md "wikilink")<sub>（209回）</sub>。

[勞浩榮飾演後唐莊宗](../Page/勞浩榮.md "wikilink")[李存勗](../Page/李存勗.md "wikilink")<sub>（238-243回）</sub>、後周世宗[柴榮](../Page/柴榮.md "wikilink")<sub>（259-260回）</sub>、宋代[韓琦](../Page/韓琦.md "wikilink")、[宋欽宗](../Page/宋欽宗.md "wikilink")、[辛棄疾](../Page/辛棄疾.md "wikilink")、金代[元好問](../Page/元好問.md "wikilink")<sub>（365回）</sub>。

[許金峰飾演](../Page/許金峰.md "wikilink")[宋神宗](../Page/宋神宗.md "wikilink")。

[梁家輝飾演](../Page/梁家輝.md "wikilink")[宋徽宗](../Page/宋徽宗.md "wikilink")<sub>（310-315回）</sub>。

[盧世昌飾演](../Page/盧世昌_\(香港\).md "wikilink")[元世祖忽必烈](../Page/元世祖.md "wikilink")<sub>（382-407回）</sub>，[嘉慶帝](../Page/嘉慶帝.md "wikilink")、還有民國[徐世昌](../Page/徐世昌.md "wikilink")、[馬歇爾](../Page/喬治·卡特萊特·馬歇爾.md "wikilink")、[白崇禧](../Page/白崇禧.md "wikilink")、[聶榮臻等人物](../Page/聶榮臻.md "wikilink")。

[董麗誠飾演宋代](../Page/董麗誠.md "wikilink")[尹洙](../Page/尹洙.md "wikilink")<sub>284回</sub>、[宋仁宗](../Page/宋仁宗.md "wikilink")、[秦檜](../Page/秦檜.md "wikilink")、[范成大](../Page/范成大.md "wikilink")、[郝經](../Page/郝經.md "wikilink")<sub>（386-387回）</sub>。

[周志強飾演](../Page/周志強.md "wikilink")[辛棄疾的同伴賈瑞](../Page/辛棄疾.md "wikilink")<sub>（340回）</sub>、[賈似道密使宋京](../Page/賈似道.md "wikilink")<sub>（387回）</sub>。

[羅君左飾演](../Page/羅君左.md "wikilink")[元武宗海山](../Page/元武宗.md "wikilink")<sub>（409-411回）</sub>。

[李家仁飾演](../Page/李家仁.md "wikilink")[宋濂](../Page/宋濂.md "wikilink")、[明穆宗等](../Page/明穆宗.md "wikilink")。

[馮偉棠飾演](../Page/馮偉棠.md "wikilink")[吳三桂](../Page/吳三桂.md "wikilink")、[咸豐皇帝](../Page/咸豐皇帝.md "wikilink")、[周恩來等人物](../Page/周恩來.md "wikilink")。

[錢佩卿飾演](../Page/錢佩卿.md "wikilink")[陳圓圓](../Page/陳圓圓.md "wikilink")、[小鳳仙](../Page/小鳳仙.md "wikilink")、[胡蝶等人物](../Page/胡蝶.md "wikilink")。

[鄭子誠飾演](../Page/鄭子誠.md "wikilink")[康熙](../Page/康熙.md "wikilink")、[袁崇煥](../Page/袁崇煥.md "wikilink")、[林則徐](../Page/林則徐.md "wikilink")、[侯方域等人物](../Page/侯方域.md "wikilink")。

[洪朝豐飾演](../Page/洪朝豐.md "wikilink")[鄭成功](../Page/鄭成功.md "wikilink")。

[周國豐飾演](../Page/周國豐_\(DJ\).md "wikilink")[崇禎皇帝](../Page/崇禎皇帝.md "wikilink")、[宋教仁](../Page/宋教仁.md "wikilink")、[傅斯年](../Page/傅斯年.md "wikilink")、[陳立夫](../Page/陳立夫.md "wikilink")、[王實味](../Page/王實味.md "wikilink")、[吳國禎](../Page/吳國禎.md "wikilink")、[梁思成等人物](../Page/梁思成.md "wikilink")。

[施介強飾演](../Page/施介強.md "wikilink")[楊應龍](../Page/楊應龍.md "wikilink")、[杜松](../Page/杜松.md "wikilink")、[弘光帝](../Page/弘光帝.md "wikilink")、[林爽文](../Page/林爽文.md "wikilink")、[僧格林沁](../Page/僧格林沁.md "wikilink")、[段祺瑞](../Page/段祺瑞.md "wikilink")、[孫傳芳](../Page/孫傳芳.md "wikilink")、[顧竹軒](../Page/顧竹軒.md "wikilink")、[朱德等人物](../Page/朱德.md "wikilink")。

[丁家湘飾演](../Page/丁家湘.md "wikilink")[袁宗皋](../Page/袁宗皋.md "wikilink")、[嚴嵩](../Page/嚴嵩.md "wikilink")、[宇喜多秀家](../Page/宇喜多秀家.md "wikilink")、[雍正皇帝](../Page/雍正皇帝.md "wikilink")、[張彪](../Page/張彪_\(江蘇\).md "wikilink")、[吳炳湘](../Page/吳炳湘.md "wikilink")、[鮑羅廷](../Page/米哈伊爾·馬爾科維奇·鮑羅廷.md "wikilink")、[薛覺先](../Page/薛覺先.md "wikilink")、[林彪](../Page/林彪.md "wikilink")、[赫爾利等人物](../Page/赫爾利.md "wikilink")。

[戴偉光飾演](../Page/戴偉光.md "wikilink")[馬鴻逵](../Page/馬鴻逵.md "wikilink")。

[王祖藍飾演](../Page/王祖藍.md "wikilink")[蔣君章](../Page/蔣君章.md "wikilink")。

本劇的撰稿人[張偉國曾飾演](../Page/張偉國.md "wikilink")[梁漱溟](../Page/梁漱溟.md "wikilink")。

助理[廣播處長](../Page/廣播處.md "wikilink")[周偉材飾演](../Page/周偉材.md "wikilink")[倭仁](../Page/倭仁.md "wikilink")、[陶啟勝](../Page/陶啟勝.md "wikilink")、[內田康哉](../Page/內田康哉.md "wikilink")、[司徒拔](../Page/司徒拔.md "wikilink")、[芳澤謙吉](../Page/芳澤謙吉.md "wikilink")、[土肥原賢二](../Page/土肥原賢二.md "wikilink")、[杜月笙](../Page/杜月笙.md "wikilink")、[上官雲相等人物](../Page/上官雲相.md "wikilink")。

[蔡浩樑飾演](../Page/蔡浩樑.md "wikilink")[汪精衛](../Page/汪精衛.md "wikilink")、[徐樹錚](../Page/徐樹錚.md "wikilink")、[松井石根](../Page/松井石根.md "wikilink")、[黃克誠](../Page/黃克誠.md "wikilink")、[杜魯門等人物](../Page/杜魯門.md "wikilink")。

[楊吉璽飾演](../Page/楊吉璽.md "wikilink")[伊藤博文](../Page/伊藤博文.md "wikilink")、[陳獨秀](../Page/陳獨秀.md "wikilink")、[吉田茂](../Page/吉田茂.md "wikilink")、[板垣征四郎](../Page/板垣征四郎.md "wikilink")、[張治中](../Page/張治中.md "wikilink")、[陳毅](../Page/陳毅.md "wikilink")、[杜重遠](../Page/杜重遠.md "wikilink")、[潘漢年等人物](../Page/潘漢年.md "wikilink")。

[李志剛飾演](../Page/李志剛_\(香港\).md "wikilink")[蔣翊武](../Page/蔣翊武.md "wikilink")、[謝紹敏](../Page/謝紹敏.md "wikilink")、[蘇兆徵](../Page/蘇兆徵.md "wikilink")、[夏斗寅](../Page/夏斗寅.md "wikilink")、[于學忠](../Page/于學忠.md "wikilink")、[卜萬蒼](../Page/卜萬蒼.md "wikilink")、[威廉·亨瑞·端納](../Page/威廉·亨瑞·端納.md "wikilink")、[柳川平助等人物](../Page/柳川平助.md "wikilink")。

[陳永業飾演](../Page/陳永業.md "wikilink")[夏衍](../Page/夏衍.md "wikilink")。

[曾葉發飾演](../Page/曾葉發.md "wikilink")[蔣經國](../Page/蔣經國.md "wikilink")。

[陳敬創飾演](../Page/陳敬創.md "wikilink")[黃興](../Page/黃興.md "wikilink")、[顧維鈞](../Page/顧維鈞.md "wikilink")、[李之龍](../Page/李之龍.md "wikilink")、[余漢謀等人物](../Page/余漢謀.md "wikilink")。

[張圭陽飾演](../Page/張圭陽.md "wikilink")[李宗仁](../Page/李宗仁.md "wikilink")、[梁漱溟等人物](../Page/梁漱溟.md "wikilink")。

[程振鵬飾演](../Page/程振鵬.md "wikilink")[哲布尊丹巴呼圖克圖](../Page/哲布尊丹巴呼圖克圖.md "wikilink")。

[胡世傑飾演](../Page/胡世傑.md "wikilink")[戴季陶](../Page/戴季陶.md "wikilink")、[宋子文](../Page/宋子文.md "wikilink")、[許世友](../Page/許世友.md "wikilink")、[黃敬等人物](../Page/黃敬.md "wikilink")。

[韋佩文飾演](../Page/韋佩文.md "wikilink")[阮玲玉](../Page/阮玲玉.md "wikilink")、[傅冬等人物](../Page/傅冬.md "wikilink")。

[譚樹強飾演四川軍閥](../Page/譚樹強.md "wikilink")[鄧錫侯](../Page/鄧錫侯.md "wikilink")。

[李仁傑飾演](../Page/李仁傑.md "wikilink")[和珅](../Page/和珅.md "wikilink")、[謝福音](../Page/謝福音.md "wikilink")、[胡漢民](../Page/胡漢民.md "wikilink")、[劉紀文](../Page/劉紀文.md "wikilink")、[楊虎城](../Page/楊虎城.md "wikilink")、[徐祖詒](../Page/徐祖詒.md "wikilink")、[葉劍英等人物](../Page/葉劍英.md "wikilink")。

[區麗雅飾演](../Page/區麗雅.md "wikilink")[史良](../Page/史良.md "wikilink")、[龔澎](../Page/龔澎.md "wikilink")、[何宜文等人物](../Page/何宜文.md "wikilink")。

新聞部[鄧志濤飾演](../Page/鄧志濤.md "wikilink")[黄郛](../Page/黄郛.md "wikilink")、[秦邦憲](../Page/秦邦憲.md "wikilink")、[葉挺](../Page/葉挺.md "wikilink")、[周士第](../Page/周士第.md "wikilink")、[李先念等人物](../Page/李先念.md "wikilink")。

[陳燕萍飾演](../Page/陳燕萍.md "wikilink")[何香凝](../Page/何香凝.md "wikilink")。

[吳志森](../Page/吳志森.md "wikilink")，香港電台電視節目《[頭條新聞](../Page/頭條新聞.md "wikilink")》的主持，飾演[杜聿明](../Page/杜聿明.md "wikilink")、[薛岳](../Page/薛岳.md "wikilink")、[戴笠](../Page/戴笠.md "wikilink")、[張治中等人物](../Page/張治中.md "wikilink")。

歷史學家[麥勁生飾演北宋](../Page/麥勁生.md "wikilink")[狄青](../Page/狄青.md "wikilink")、民國[唐紹儀](../Page/唐紹儀.md "wikilink")、[胡適](../Page/胡適.md "wikilink")、[宋子文等人物](../Page/宋子文.md "wikilink")。

## 參考資料

## 外部連結

  - [網上中華五千年](http://rthk.hk/chiculture/fivethousandyears/)
  - [中華五千年盛世版](http://rthk.hk/tools/fivethousandyears/details.htm)
  - [香港電台-{|zh-cn:播客;
    zh-hk:Podcasts}-中華五千年](http://podcast.rthk.hk/podcast/item_all.php?pid=336&lang=zh-CN)

[Category:香港電台廣播節目](../Category/香港電台廣播節目.md "wikilink")

1.  1983年4月4日(星期一)，香港《華僑日報》，第二張；第二頁
2.  1987年4月14日(星期二)，香港《華僑日報》，第三張；第一頁 編年形式叙述歷史 始於遠古終於文革 「中華五千年」節目最宜中學生收聽
3.  1987年4月11日(星期六)，香港《華僑日報》，第六張；第四頁 港台節目「中華五千年」 四週年紀念午餐座談會
4.  港台：經典如何現代化？ 《溯蘭》Vol. 5，第14頁，2008年7月
5.  香港電台節目表「今樂府」，《華僑日報》，1983年12月26日，頁十九。[剪報](http://i58.photobucket.com/albums/g270/kameyou_ho/voice/1983-12-26_zpsoljfzp4a.jpg)
6.  香港電台節目表「今樂府」，《華僑日報》，1984年12月31日，頁十一。[剪報](http://i58.photobucket.com/albums/g270/kameyou_ho/voice/1984-12-31a_zps2jjicywy.jpg)
7.  「電台電視」節目表，《華僑日報》，1986年01月06日，頁13（第四張第一頁）。
8.  《Air Wave》雜誌，1990年9月號，頁65。
9.  港台：經典如何現代化？ 《溯蘭》Vol. 5，第14頁，2008年7月