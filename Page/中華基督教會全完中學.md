**中華基督教會全完中學**是一所位於[香港](../Page/香港.md "wikilink")[葵青區上角街的](../Page/葵青區.md "wikilink")[基督教](../Page/基督教.md "wikilink")[中學](../Page/中學.md "wikilink")，由[中華基督教會於](../Page/中華基督教會.md "wikilink")1969年創辦。校舍佔地近5,000平方呎，樓高六層，有標準課室24間，特別室11間。校內設施包括禮堂、籃球場、圖書館、停車場。

## 教學語言

此校本為一所[英文中學](../Page/英文中學.md "wikilink")。於1998年，為配合[教育署的](../Page/教育署.md "wikilink")[母語教學政策](../Page/母語教學.md "wikilink")，教學語言曾一度轉為[中文](../Page/中文.md "wikilink")，直至2011-12學年再度轉回英語教學。

## 知名校友

  - [郭家麒](../Page/郭家麒.md "wikilink")：前[立法會議員（醫學界）](../Page/第三屆香港立法會.md "wikilink")、現任[立法會議員（新界西）](../Page/第五屆香港立法會.md "wikilink")、外科醫生
  - [黃德祥](../Page/黃德祥.md "wikilink")：[聖母醫院行政總監](../Page/聖母醫院_\(香港\).md "wikilink")
  - [屎萊姆](../Page/屎萊姆.md "wikilink")：知名youtuber
  - [天航](../Page/天航.md "wikilink")：香港作家，香港小說會幹事
  - [羅莘桉](../Page/羅莘桉.md "wikilink")：前觀塘民政事務專員\[1\]；此前曾擔任[政務司司長新聞秘書](../Page/政務司司長.md "wikilink")
  - [龐秋雁](../Page/龐秋雁.md "wikilink")：前亞洲電視女藝員
  - [陳勇](../Page/陳勇_\(社會工作者\).md "wikilink")：現任[新界社團聯會](../Page/新界社團聯會.md "wikilink")（新社聯）理事長、[民建聯副主席](../Page/民建聯.md "wikilink")、[港區全國人大代表](../Page/港區全國人大代表.md "wikilink")、勞工及經濟事務委員會委員以及香港專業及資深行政人員協會成員
  - 陳承龍：港股策略王周刊出版社社長

## 組織

全完中學設有四社，分別為智、信、望和愛，並會每年舉辦一次學生會選舉。[校友會及家長教師會亦於](../Page/校友會.md "wikilink")1994年相繼成立。

## 外部連結

[Category:葵涌](../Category/葵涌.md "wikilink")
[Category:中華基督教會中學](../Category/中華基督教會中學.md "wikilink")
[P](../Category/葵青區中學.md "wikilink")
[Category:1969年創建的教育機構](../Category/1969年創建的教育機構.md "wikilink")
[Category:校友會](../Category/校友會.md "wikilink")
[Category:香港英文授課中學](../Category/香港英文授課中學.md "wikilink")

1.