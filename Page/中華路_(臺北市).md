[TW_PHW3.svg](https://zh.wikipedia.org/wiki/File:TW_PHW3.svg "fig:TW_PHW3.svg")

[Ximending_section_of_Highway_3_20070925.jpg](https://zh.wikipedia.org/wiki/File:Ximending_section_of_Highway_3_20070925.jpg "fig:Ximending_section_of_Highway_3_20070925.jpg")
台北市內有兩條**中華路**，市區路段位於[臺北市](../Page/臺北市.md "wikilink")[萬華](../Page/萬華區.md "wikilink")、[中正兩區區界](../Page/中正區_\(臺北市\).md "wikilink")，北起[忠孝西路口](../Page/忠孝西路.md "wikilink")，南至[水源路口](../Page/水源路.md "wikilink")。其中[忠孝西路至](../Page/忠孝西路.md "wikilink")[愛國西路為中華路最核心的部份](../Page/愛國西路.md "wikilink")，上述路段也是省道[台3線一部分](../Page/台3線.md "wikilink")。此路底下有[臺北捷運](../Page/臺北捷運.md "wikilink")[板南線](../Page/板南線.md "wikilink")、[松山新店線](../Page/松山新店線.md "wikilink")、[台鐵縱貫線北段及](../Page/縱貫線_\(北段\).md "wikilink")[台灣高速鐵路等多條鐵路隧道通過](../Page/台灣高速鐵路.md "wikilink")，另一條則位於士林區陽明山地區與仰德大道四段相交無分段的道路。

過去市中心的中華路中曾為[大臺北地區規模最大的公有綜合](../Page/大臺北地區.md "wikilink")[中華商場所在地](../Page/中華商場.md "wikilink")。後來為因應[都市更新與](../Page/都市更新.md "wikilink")[捷運施工等需求而於](../Page/台北捷運.md "wikilink")1992年拆除，原址則配合中華路改造計畫，成為林蔭大道的一部分。

## 歷史概述

[缩略图](https://zh.wikipedia.org/wiki/File:MRT_Songshan_Line_Construction_at_Section_1_Zhonghua_Road.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:在中華路天橋往西門方向拍攝.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:在中華路天橋往北門方向拍攝.jpg "fig:缩略图")
中華路(市區)過去[忠孝西路至](../Page/忠孝西路.md "wikilink")[愛國西路的部分](../Page/愛國西路.md "wikilink")，與[清領時期所興建](../Page/台灣清治時期.md "wikilink")[台北府城的西城牆位置大致重疊](../Page/台北府城.md "wikilink")。到了[日治時期](../Page/台灣日治時期.md "wikilink")，西城牆連同[西門於](../Page/台北府城西門.md "wikilink")1900年代初被全面拆除，城牆原址隨後配合已填平的西護城河稍作拓寬，改築成為西[三線路](../Page/三線路.md "wikilink")（即今天的中華路）；由於西三線路在興築之前已有鐵路沿路行經，所以總體路幅比其他三條三線路都來得寬廣。

## 行經行政區域

（由北至南）

  - [士林區](../Page/士林區.md "wikilink")（與市區段無銜接）
  - [中正區](../Page/中正區_\(臺北市\).md "wikilink")（單號）
  - [萬華區](../Page/萬華區.md "wikilink")（路西，雙號）

## 道路設計

### 車道數

  - 一段：雙向各五車道，第三車道為[公車專用道](../Page/公車專用道.md "wikilink")，人行道為2段[自行車專用道](../Page/自行車專用道.md "wikilink")，80米。
  - 二段（[愛國西路](../Page/愛國西路.md "wikilink")-[艋舺大道](../Page/艋舺大道.md "wikilink")）：雙向各五車道，50米。
  - 二段（[艋舺大道](../Page/艋舺大道.md "wikilink")-[西藏路](../Page/西藏路.md "wikilink")）：雙向各二車道，20米。
  - 二段（[西藏路](../Page/西藏路.md "wikilink")-[水源路](../Page/水源路_\(臺北市\).md "wikilink")）：雙向各一車道，15米。

### 號碼

  - 一段：單號1\~95，雙號2\~216
  - 二段：單號1\~503，雙號2\~680

## 分段

中華路共分兩段
[中華路看向忠孝橋.jpg](https://zh.wikipedia.org/wiki/File:中華路看向忠孝橋.jpg "fig:中華路看向忠孝橋.jpg")

  - 一段：北始於[忠孝西路二段](../Page/忠孝西路.md "wikilink")，南於[愛國西路與中華路二段相接](../Page/愛國西路.md "wikilink")，屬省道[台3線](../Page/台3線.md "wikilink")。
  - 二段：北於[愛國西路與中華路一段相接](../Page/愛國西路.md "wikilink")，南抵[水源路](../Page/水源路_\(臺北市\).md "wikilink")；愛國西路至和平西路為省道[台3線](../Page/台3線.md "wikilink")。

## 沿線設施

(由北往南)

  - 一段[TW_PHW3.svg](https://zh.wikipedia.org/wiki/File:TW_PHW3.svg "fig:TW_PHW3.svg")
      - [台北府城北門](../Page/台北府城北門.md "wikilink")（忠孝西路口）
      - [財政部臺北國稅局](../Page/財政部臺北國稅局.md "wikilink")（2號）
      - [西門町](../Page/西門町.md "wikilink")：成都路口靠萬華區一側，年輕人聚集的商圈
      - [台北府城西門](../Page/台北府城西門.md "wikilink")[裝置藝術與舊址石碑](../Page/裝置藝術.md "wikilink")
      - [捷運](../Page/台北捷運.md "wikilink")[西門站](../Page/西門站.md "wikilink")（門牌位於寶慶路32之1號B1）
      - [台鐵](../Page/台鐵.md "wikilink")[西門緊急停靠站](../Page/西門緊急停靠站.md "wikilink")
      - [台北市公共自行車租賃系統](../Page/台北市公共自行車租賃系統.md "wikilink")[捷運](../Page/台北捷運.md "wikilink")[西門站](../Page/西門站.md "wikilink")\[3號出口\]
      - [台糖騰雲大樓](../Page/台糖.md "wikilink")（39號）
      - 萬企大樓（41號、漢口街一段144號，[萬華企業](../Page/萬華企業.md "wikilink")、[愛爾達電視總部所在地](../Page/愛爾達電視.md "wikilink")）
      - 原[中國時報大樓](../Page/中國時報.md "wikilink")（51號，[伊林娛樂總部所在地](../Page/伊林娛樂.md "wikilink")）
      - [西門地下街](../Page/西門地下街.md "wikilink")（51之1號）
          - [西門智慧圖書館](../Page/西門智慧圖書館.md "wikilink")
      - [臺北市立國樂團與](../Page/臺北市立國樂團.md "wikilink")[經濟部礦務局](../Page/經濟部礦務局.md "wikilink")（53號，[臺北市中山堂中華路側](../Page/臺北市中山堂.md "wikilink")）
      - [錢櫃KTV台北中華新館](../Page/錢櫃KTV.md "wikilink")（55號）
      - [英雄廣場商業大樓](../Page/英雄廣場商業大樓.md "wikilink")（59號）
      - [臺北市萬華區福星國民小學](../Page/臺北市萬華區福星國民小學.md "wikilink")（66號）
      - [國軍文藝活動中心](../Page/國軍文藝活動中心.md "wikilink")（69號）
      - [行政院環境保護署](../Page/行政院環境保護署.md "wikilink")（83號）
      - [西本願寺建築群遺跡](../Page/西本願寺_\(台北市\).md "wikilink")（無門牌，本路段與長沙街二段路口）
      - [台北市公共自行車租賃系統西本願寺廣場站](../Page/台北市公共自行車租賃系統.md "wikilink")
      - [臺北市立文獻館](../Page/臺北市立文獻館.md "wikilink")（174之1號）
      - [台北市公共自行車租賃系統中華桂林路口站](../Page/台北市公共自行車租賃系統.md "wikilink")
      - [臺灣銀行經濟研究處](../Page/臺灣銀行.md "wikilink")（198號）
  - 二段
      - [臺北市立聯合醫院和平婦幼院區和平分部](../Page/臺北市立聯合醫院和平婦幼院區.md "wikilink")（33號，[廣州街口](../Page/廣州街_\(台北\).md "wikilink")，原台北市立和平醫院）
  - 廣州街口
      - [警察廣播電臺](../Page/警察廣播電臺.md "wikilink")（廣州街17號）
  - 莒光立體停車場\[機械\]（莒光路口）
  - [捷運](../Page/台北捷運.md "wikilink")[萬大線](../Page/萬大線.md "wikilink")[廈安站](../Page/廈安站.md "wikilink")（本路段與[西藏路路口道路下方](../Page/西藏路.md "wikilink")）\[施工中\]
  - [南機場國民住宅](../Page/南機場國民住宅.md "wikilink")
  - [臺北市中正區忠義國民小學](../Page/臺北市中正區忠義國民小學.md "wikilink")(307巷17號)
  - 國盛國宅（[南海路口](../Page/南海路.md "wikilink")）
  - 國輝國宅
  - [臺北市立古亭國民中學](../Page/臺北市立古亭國民中學.md "wikilink")（465號）
      - [台北市公共自行車租賃系統古亭國中站](../Page/台北市公共自行車租賃系統.md "wikilink")
      - 古亭國中地下停車場（606巷1號）
  - 財團法人萬華醫院（606巷6號）

## 文化

在電影《[英雄本色](../Page/英雄本色_\(1986年電影\).md "wikilink")》中，[周潤發所主演的小馬哥曾徘迴於中華路與天橋上](../Page/周潤發.md "wikilink")。

## 參考資料

## 參見

  - [臺北市主要道路列表](../Page/臺北市主要道路列表.md "wikilink")

[Category:台北市街道](../Category/台北市街道.md "wikilink")
[Category:萬華區](../Category/萬華區.md "wikilink") [Category:中正區
(臺北市)](../Category/中正區_\(臺北市\).md "wikilink")