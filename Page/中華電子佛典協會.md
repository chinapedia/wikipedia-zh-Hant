**中華電子佛典協會**（Chinese Buddhist Electronic Text Association 簡稱
**CBETA**）由臺大哲學系[釋恆清教授發起](../Page/釋恆清教授.md "wikilink")，結合佛教界從事佛學電子化的團體及個人，於1998年2月15日成立。該協會係一獨立運作機構，不隸屬於任何宗派團體與組織，成立的目的主要是希望免費提供電子佛典資料庫讓各界作非營利性使用。

中華電子佛典協會的成立主要有兩大因緣：一是1997年10月，[蕭鎮國免費提供二十五冊](../Page/蕭鎮國.md "wikilink")[中文資訊交換碼](../Page/中文資訊交換碼.md "wikilink")《[大正藏](../Page/大正藏.md "wikilink")》電子稿予[國立台灣大學](../Page/國立台灣大學.md "wikilink")[佛學研究中心主任釋恆清教授](../Page/佛學.md "wikilink")，另一是1998年初，釋恆清教授向北美「[印順基金會](../Page/印順基金會.md "wikilink")」募得美金一百萬元，作為《[大正藏](../Page/大正藏.md "wikilink")》數位化的經費。

該協會成立後，由[惠敏法師出任主任委員](../Page/惠敏法師.md "wikilink")、杜正民擔任總幹事及一個專業團隊開始進行電子佛典數位化工作。為了解決版權問題，由恆清法師偕同惠敏法師、杜正民、維習安等人，與日本大藏出版株式會社洽商，於
1998 年 9 月 30 日獲得大藏出版株式會社之授權並簽約。

簽約後開始進行經典輸入，在[網頁技術上做到標記](../Page/網頁.md "wikilink")（Markup）處理，以[SGML](../Page/SGML.md "wikilink")／[XML的方式呈現](../Page/XML.md "wikilink")，完全符合
[TEI](../Page/TEI.md "wikilink")（Text Encoding
Initiative）準則。1999年1月，[惠敏法師於國際](../Page/惠敏法師.md "wikilink")「電子佛典推進協議會」（Electronic
Buddhist Text Initiative, EBTI）發表「CBETA and Taisho Tripitaka
Project」一文，使佛典電子化正式步入國際舞台。

《大正藏》數位化於2003年完成。同年，該協會又獲得新加坡華僑黃淑玲夫婦及一位隱名的賴姓大德全額贊助及「[西蓮教育基金會](../Page/西蓮教育基金會.md "wikilink")」的參與，開始進行《[卍新纂續藏經](../Page/卍新纂續藏經.md "wikilink")》（Shinsan
Zakuzokyo，簡稱《[卍續藏](../Page/卍續藏.md "wikilink")》）的電子數位化工作，並於2007
年完成。此後，該協會每年均有新增佛典的收錄，至目前為止，「[電子佛典集成](../Page/電子佛典集成.md "wikilink")」光牒（又稱CBETA）已收錄三套大藏經（包括《[大正藏](../Page/大正藏.md "wikilink")》、《[卍續藏](../Page/卍續藏.md "wikilink")》、《[南傳大藏經](../Page/南傳大藏經.md "wikilink")》）及現代佛教新編文獻，包括印順法師佛學著作集等。

## 參考

  - 杜正民，{{〈}}漢文電子大藏經的製作緣起與作業流程——以「中華電子佛典協會」為例{{〉}}，《[佛學研究中心學報](../Page/佛學研究中心學報.md "wikilink")》，4期，1999年7月
  - 周伯戡，{{〈}}評CBETA電子大正藏{{〉}}，《[佛學研究中心學報](../Page/佛學研究中心學報.md "wikilink")》，7期，2002年7月
  - 侯坤宏採訪，{{〈}}CBETA電子佛典的建置和推廣{{〉}}，《[杏壇衲履：恆清法師訪談錄](../Page/杏壇衲履：恆清法師訪談錄.md "wikilink")》，國史館，2007年

## 外部連結

  - [中華電子佛典協會首頁](http://www.cbeta.org/index.php)

[Category:中華民國協會](../Category/中華民國協會.md "wikilink")
[Category:台灣佛教組織](../Category/台灣佛教組織.md "wikilink")