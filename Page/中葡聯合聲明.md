《**中華人民共和國政府和葡萄牙共和國政府關於澳門問題的聯合聲明**》（），簡稱《**中葡聯合聲明**》，是[中華人民共和國](../Page/中華人民共和國.md "wikilink")（簡稱[中國](../Page/中國.md "wikilink")）與[葡萄牙共和國](../Page/葡萄牙共和國.md "wikilink")（簡稱[葡萄牙](../Page/葡萄牙.md "wikilink")）就[澳門問題共同發表的一份](../Page/澳門.md "wikilink")[聲明](../Page/聲明.md "wikilink")，於1987年4月13日由[中國國務院總理](../Page/中國國務院總理.md "wikilink")[趙紫陽與](../Page/趙紫陽.md "wikilink")[葡萄牙總理](../Page/葡萄牙總理.md "wikilink")[施華高在](../Page/施華高.md "wikilink")[中國](../Page/中國.md "wikilink")[北京簽訂](../Page/北京.md "wikilink")。中葡兩國政府在1988年1月15日互相交換批准書，《中葡聯合聲明》正式生效。

這份聲明指出，澳門地區（包括[澳門半島](../Page/澳門半島.md "wikilink")、[氹仔島和](../Page/氹仔島.md "wikilink")[路環島](../Page/路環島.md "wikilink")）是中國領土，[葡萄牙於](../Page/葡萄牙.md "wikilink")1999年12月20日[把主權交還中國](../Page/澳門回歸.md "wikilink")，[中華人民共和國政府於同日恢復對澳門行使主權](../Page/中華人民共和國政府.md "wikilink")。聲明也列出了中國對澳門的基本方針。在「[一國兩制](../Page/一國兩制.md "wikilink")」的原則下，中國會確保其[社會主義制度不會在](../Page/社會主義.md "wikilink")[澳門特別行政區實行](../Page/澳門特別行政區.md "wikilink")，維持澳門的[資本主義制度和生活方式](../Page/資本主義.md "wikilink")「[五十年不變](../Page/五十年不變.md "wikilink")」。這些基本政策，後來都由《[澳門特別行政區基本法](../Page/澳門特別行政區基本法.md "wikilink")》加以規定。

## 背景

## 外部連結

  - [《中葡聯合聲明》原文](http://bo.io.gov.mo/bo/i/88/23/dc/cn/Default.asp)（中、葡、英語）

[Category:1987年条约](../Category/1987年条约.md "wikilink")
[Category:1987年中国政治](../Category/1987年中国政治.md "wikilink")
[Category:1987年葡萄牙](../Category/1987年葡萄牙.md "wikilink")
[Category:澳门回归](../Category/澳门回归.md "wikilink")
[Category:中葡条约](../Category/中葡条约.md "wikilink")
[Category:中葡關係](../Category/中葡關係.md "wikilink")
[Category:中華人民共和國條約](../Category/中華人民共和國條約.md "wikilink")
[Category:葡萄牙共和国条约](../Category/葡萄牙共和国条约.md "wikilink")
[Category:領土變更相關條約](../Category/領土變更相關條約.md "wikilink")
[Category:一國兩制政治](../Category/一國兩制政治.md "wikilink")
[Category:中华人民共和国北京市政治史](../Category/中华人民共和国北京市政治史.md "wikilink")
[Category:聯合聲明](../Category/聯合聲明.md "wikilink")
[Category:葡属澳门](../Category/葡属澳门.md "wikilink")