**中西伯利亞高原**（）是[俄罗斯](../Page/俄罗斯.md "wikilink")[西伯利亞中部的一个](../Page/西伯利亞.md "wikilink")[火成岩](../Page/火成岩.md "wikilink")（具體稱為洪水[玄武岩](../Page/玄武岩.md "wikilink")）[高原](../Page/高原.md "wikilink")，介于[葉尼塞河与](../Page/葉尼塞河.md "wikilink")[勒拿河之间](../Page/勒拿河.md "wikilink")。\[1\]

## 地貌

中西伯利亚高原面積\[2\]，平均海拔，最高點海拔为。屬[大陸性氣候](../Page/大陸性氣候.md "wikilink")，冬天漫長寒冷，平均气温左右，极端低温可达，高原东北部的[奥伊米亚康和](../Page/奥伊米亚康.md "wikilink")[上扬斯克更是能达到](../Page/上扬斯克.md "wikilink")以下；夏季则短促温和。年均降水量300～600毫米。絕大部分為針葉林覆蓋（[落葉松為優勢種](../Page/落葉松.md "wikilink")）。因蒸发量小，中西伯利亚高原河流众多，均为典型的[山地河流](../Page/山地河流.md "wikilink")，受地形限制向北流去。[下通古斯河為主要河流](../Page/下通古斯河.md "wikilink")，[贝加尔湖为世界最深湖](../Page/贝加尔湖.md "wikilink")，最深。\[3\]\[4\]\[5\]

## 资源

中西伯利亚高原地质上称为西伯利亚暗色岩（），含丰富的[煤](../Page/煤.md "wikilink")、[铁](../Page/铁.md "wikilink")、[金](../Page/金.md "wikilink")、[钻石](../Page/钻石.md "wikilink")、[天然气等矿产资源](../Page/天然气.md "wikilink")。\[6\]而且西伯利亚河流众多，使其水力资源充沛。\[7\]但因生存环境恶劣导致这些资源难以开发，仅高原南部有少量水电、工矿业。\[8\]

## 主要城市

中西伯利亚的城市主要分布在较为温暖的南部地区，下面列出部分主要城市：

  -
  - [坎斯克](../Page/坎斯克.md "wikilink")

  - [车尔尼雪夫斯基](../Page/车尔尼雪夫斯基.md "wikilink")

  - [安加尔斯克](../Page/安加尔斯克.md "wikilink")

  - [克拉斯諾亞爾斯克](../Page/克拉斯諾亞爾斯克.md "wikilink")

  -
  - [布拉茨克](../Page/布拉茨克.md "wikilink")

  - [米尔内 (萨哈共和国)](../Page/米尔内_\(萨哈共和国\).md "wikilink")

  - [乌达奇内](../Page/乌达奇内.md "wikilink")

  - [伊尔库茨克](../Page/伊尔库茨克.md "wikilink")

  - [诺里尔斯克](../Page/诺里尔斯克.md "wikilink")

  - [乌斯季伊利姆斯克](../Page/乌斯季伊利姆斯克.md "wikilink")

  - [雅库茨克](../Page/雅库茨克.md "wikilink")

  - [斯韦特雷](../Page/斯韦特雷.md "wikilink")<ref name="E">

</ref>\[9\]

## 参见

  - [西西伯利亚平原](../Page/西西伯利亚平原.md "wikilink")

## 参考资料和注释

### 参考资料

### 注释

<references group="注"/>

[category:俄羅斯地理](../Page/category:俄羅斯地理.md "wikilink")
[category:高原](../Page/category:高原.md "wikilink")

1.

2.  [中国大陆资料多为](../Page/中国大陆.md "wikilink")

3.
4.

5.

6.
7.

8.
9.  本段落含有翻译自德语维基百科的内容。