[Image-cosco_logo.gif](https://zh.wikipedia.org/wiki/File:Image-cosco_logo.gif "fig:Image-cosco_logo.gif")

**中遠酒店（Cosco
Hotel）**，為[香港的一間三星級酒店](../Page/香港.md "wikilink")，位處[香港島](../Page/香港島.md "wikilink")[西環](../Page/西環.md "wikilink")[堅尼地城海旁路](../Page/堅尼地城.md "wikilink")20-21號。中遠酒店於1997年10月開幕，共提供50間豪華客房，由[中遠香港集團及](../Page/中遠\(香港\)集團.md "wikilink")[廣州遠洋公司管理](../Page/廣州遠洋公司.md "wikilink")。

## 酒店設施

  - 餐廳
  - [停車場](../Page/停車場.md "wikilink")(月租)

## 餐廳

  - 德記

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

  - [電車](../Page/香港電車.md "wikilink")

</div>

</div>

## 參看

  - [香港酒店列表](../Page/香港酒店列表.md "wikilink")
  - [中遠(香港)集團](../Page/中遠\(香港\)集團.md "wikilink")
  - [廣州遠洋公司](../Page/廣州遠洋公司.md "wikilink")

## 外部連結

  - [中遠酒店](http://www.coscohotel.com.hk)

[Category:堅尼地城酒店](../Category/堅尼地城酒店.md "wikilink")
[Category:中遠集團物業](../Category/中遠集團物業.md "wikilink")