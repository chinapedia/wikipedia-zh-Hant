[CBC_1951-1956.JPG](https://zh.wikipedia.org/wiki/File:CBC_1951-1956.JPG "fig:CBC_1951-1956.JPG")
[CBC_Hall_of_Chubu-Nippon_Broadcasting.jpg](https://zh.wikipedia.org/wiki/File:CBC_Hall_of_Chubu-Nippon_Broadcasting.jpg "fig:CBC_Hall_of_Chubu-Nippon_Broadcasting.jpg")
[CBC_Broadcasting_Center.jpg](https://zh.wikipedia.org/wiki/File:CBC_Broadcasting_Center.jpg "fig:CBC_Broadcasting_Center.jpg")

**中部日本放送株式会社**（），簡稱**CBC**、**中日放送**，是[日本一家以](../Page/日本.md "wikilink")[名古屋為基地的](../Page/名古屋市.md "wikilink")[廣播](../Page/廣播.md "wikilink")[控股公司](../Page/控股公司.md "wikilink")，並同時經營[廣播電台與](../Page/廣播電台.md "wikilink")[電視台](../Page/電視台.md "wikilink")，目前最大股東為[中日新聞社](../Page/中日新聞社.md "wikilink")。其旗下的[CBC电视台為](../Page/CBC电视台_\(日本\).md "wikilink")[TBS](../Page/東京放送控股.md "wikilink")[電視聯播網](../Page/電視聯播網.md "wikilink")（[JNN](../Page/JNN.md "wikilink")）的成员，[CBC电台为](../Page/CBC电台.md "wikilink")[廣播聯播網](../Page/廣播聯播網.md "wikilink")[JRN的成員](../Page/JRN.md "wikilink")。

中部日本放送成立於1950年12月15日，電台廣播於1951年9月1日開播，為日本首家民營電台，其[呼号](../Page/呼号.md "wikilink")“JOAR”至今未变；電視廣播則開播於1956年12月1日，放送範圍是[中京廣域圈](../Page/中京廣域圈.md "wikilink")（[愛知縣](../Page/愛知縣.md "wikilink")、[岐阜縣和](../Page/岐阜縣.md "wikilink")[三重縣](../Page/三重縣.md "wikilink")）。2013年4月1日，中部日本放送的電台廣播業務由子公司[CBC電台繼承](../Page/CBC電台.md "wikilink")，中部日本放送的電視頻道呼號由「JOAR-DTV」改為「JOGX-DTV」，電視頻道名稱同步從「中部日本放送」改為「CBC電視台」（）。2014年4月1日，中部日本放送的電視廣播業務由子公司[CBC電視繼承](../Page/CBC電視.md "wikilink")，中部日本放送則轉型為依照[日本法律設立的](../Page/日本法律.md "wikilink")「認定廣播控股公司」（），成為[東京以外的第一家](../Page/核心局.md "wikilink")。

## 外部連結

  - [中部日本放送](http://hicbc.com/)

[Category:日本廣播控股公司](../Category/日本廣播控股公司.md "wikilink")
[Category:愛知縣公司](../Category/愛知縣公司.md "wikilink")
[Category:1950年成立的公司](../Category/1950年成立的公司.md "wikilink")