[Mesoamérica_y_Centroamerica_prehispanica_siglo_XVI.svg](https://zh.wikipedia.org/wiki/File:Mesoamérica_y_Centroamerica_prehispanica_siglo_XVI.svg "fig:Mesoamérica_y_Centroamerica_prehispanica_siglo_XVI.svg")
[Political_divisions_of_Mexico_1836-1845_(location_map_scheme).svg](https://zh.wikipedia.org/wiki/File:Political_divisions_of_Mexico_1836-1845_\(location_map_scheme\).svg "fig:Political_divisions_of_Mexico_1836-1845_(location_map_scheme).svg")
[Monte_Albán-12-05oaxaca024.jpg](https://zh.wikipedia.org/wiki/File:Monte_Albán-12-05oaxaca024.jpg "fig:Monte_Albán-12-05oaxaca024.jpg")的蹴球场\]\]

**中部美洲**是一个历史上的[地区和](../Page/地区.md "wikilink")[文化區](../Page/文化區.md "wikilink")，地理上位于[北美洲](../Page/北美洲.md "wikilink")。覆盖区域自中部[墨西哥延伸经过](../Page/墨西哥.md "wikilink")[伯利兹](../Page/伯利兹.md "wikilink")、[危地马拉](../Page/危地马拉.md "wikilink")、[萨尔瓦多](../Page/萨尔瓦多.md "wikilink")、[洪都拉斯](../Page/洪都拉斯.md "wikilink")、[尼加拉瓜](../Page/尼加拉瓜.md "wikilink")，一直到[哥斯达黎加北部](../Page/哥斯达黎加.md "wikilink")。
**中部美洲文明**指的是[西班牙殖民前](../Page/西班牙美洲殖民地.md "wikilink")（又称[前哥倫布時期](../Page/前哥倫布時期.md "wikilink")）的这段文明\[1\]\[2\]。
中部美洲是世界六大古代[文明地区之一](../Page/文明.md "wikilink")，同时也是美洲文明之一（另一为[小北文明](../Page/小北文明.md "wikilink")，位于今[秘鲁](../Page/秘鲁.md "wikilink")）。基本上「[中美洲](../Page/中美洲.md "wikilink")」一詞主要為地理上的概念，而「中部美洲」則以文化歷史上的分界而言，比「中美洲」涵蓋的範圍更大。

中部美洲文明共有的特色包括：這些文明的雕刻上常可發現三石[壁爐圖騰概念](../Page/壁爐.md "wikilink")（暗示了他們的居家環境）；某種涼鞋的穿著；以及以[玉米為主的](../Page/玉米.md "wikilink")[集約農業](../Page/集約農業.md "wikilink")；他們崇拜的神話系統都包括一個雨神、一個太陽神、一個[羽蛇神](../Page/羽蛇神.md "wikilink")（亦即[阿茲特克人的](../Page/阿茲特克人.md "wikilink")[克察尔科亚特尔](../Page/克察尔科亚特尔.md "wikilink")）；科技包括一套[二十進位的數字系統](../Page/二十進位.md "wikilink")，以及每年260日為週期的的神曆和另一套太陽曆（見：[中美洲曆法](../Page/中美洲曆法.md "wikilink")）；在建築方面，這些印地安人建築了階梯延伸至頂端的[金字塔](../Page/金字塔.md "wikilink")；在娛樂方面，他們發展出具有宗教意義的球賽（見：[中美洲蹴球](../Page/中美洲蹴球.md "wikilink")）；除這些以外，還有各式各樣的藝術及文化傳統。

中部美洲同時也是[人類語言學上的一個標準範例](../Page/人類語言學.md "wikilink")：所有主要的[中美洲語言都表現出某個共同特徵集合的部分特色](../Page/中美洲語言.md "wikilink")。中部美洲的經濟及地理政治都受惠自[通用語的大量使用](../Page/通用語.md "wikilink")，也就是[納瓦特爾語至少從](../Page/納瓦特爾語.md "wikilink")7世紀開始的廣泛使用；這樣的發展可能還可向前推2,000年。

中部美洲文明包括[奧爾梅克](../Page/奧爾梅克.md "wikilink")、[特奧蒂瓦坎](../Page/特奧蒂瓦坎.md "wikilink")、[薩波特克](../Page/薩波特克文明.md "wikilink")、[馬雅](../Page/瑪雅文明.md "wikilink")、[米斯特克](../Page/米斯特克.md "wikilink")、[瓦斯特克](../Page/瓦斯特克族.md "wikilink")、[托托納克](../Page/托托納克.md "wikilink")、[托爾特克](../Page/托爾特克.md "wikilink")、[塔拉斯卡](../Page/塔拉斯卡.md "wikilink")，以及[阿茲特克](../Page/阿茲特克.md "wikilink")。

## 相关条目

  -
  - [美洲原住民](../Page/美洲原住民.md "wikilink")

  - [拉丁美洲](../Page/拉丁美洲.md "wikilink")

  -
  -
## 引注

## 参考书目

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - . *The Aztecs Under Spanish Rule*. Stanford: Stanford University
    Press 1964.

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - , general editor. *Handbook of Middle American Indians*. Austin:
    University of Texas Press 1964-1976.

  -
  -
## 外部链接

  - [Maya
    Culture](https://web.archive.org/web/20101205233059/http://authenticmaya.com/maya_culture.htm)

  - [Mesoweb.com: a comprehensive site for Mesoamerican
    civilizations](http://www.mesoweb.com/)

  - [Museum of the Templo
    Mayor](https://web.archive.org/web/20091213122020/http://www.templomayor.inah.gob.mx/)
    (Mexico)

  - [National Museum of Anthropology and
    History](https://web.archive.org/web/20070224213250/http://www.mna.inah.gob.mx/)
    (Mexico)

  - [Selected
    bibliography](https://web.archive.org/web/20060626041644/http://naya.org.ar/biblioteca/bibliografia_militarismo_mesoamerica.htm)
    concerning war in Mesoamerica

  - [WAYEB: European Association of Mayanists](http://www.wayeb.org/)

  - [Arqueologia
    Iberoamericana](http://www.laiesken.net/arqueologia/index_en.html):
    Open access international scientific journal devoted to the
    archaeological study of the American and Iberian peoples. It
    contains research articles on Mesoamerica.

  - *[Vistas: Visual Culture in Spanish
    America, 1520–1820](http://www.fordham.edu/vistas)*

  -
[Category:美洲文明](../Category/美洲文明.md "wikilink")
[Category:伯利兹历史](../Category/伯利兹历史.md "wikilink")
[Category:萨尔瓦多历史](../Category/萨尔瓦多历史.md "wikilink")
[Category:墨西哥歷史](../Category/墨西哥歷史.md "wikilink")

1.  "Meso-America," *[牛津英語詞典](../Page/牛津英語詞典.md "wikilink")*, 2nd ed.
    (rev.) 2002. () Oxford, UK: 牛津大学出版社; p. 906.
2.  (2000): *Atlas del México Prehispánico. Revista Arqueología
    mexicana*. Número especial 5. Julio de 2000. Raíces/ Instituto
    Nacional de Antropología e Historia. México.