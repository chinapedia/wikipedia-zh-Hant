**中非共和國國徽**中心圖案為一面盾徽，中間小盾象徵[中非共和國在非洲的位置](../Page/中非共和國.md "wikilink")；四個象限分別為[象頭](../Page/非洲象.md "wikilink")、[大樹](../Page/猴麵包樹.md "wikilink")、[鑽石和黑人的手](../Page/鑽石.md "wikilink")（獨立時執政黨「[黑非洲社會进化運動](../Page/黑非洲社會进化運動.md "wikilink")」的標誌），左右有[國旗](../Page/中非共和國國旗.md "wikilink")。上方有升起的太陽（太陽上的日期1958年12月1日為成立「自治共和國」的日子）和以[桑戈語書寫的](../Page/桑戈語.md "wikilink")「天下人人平等」綬帶。下方飾帶上以[法語書有國家格言](../Page/法語.md "wikilink")「統一、尊嚴、勤勞」和[馬爾他十字勳章](../Page/馬爾他十字.md "wikilink")。

[Category:中非共和國](../Category/中非共和國.md "wikilink")
[C](../Category/國徽.md "wikilink")