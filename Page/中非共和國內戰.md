**中非共和國內戰**（**Central African Republic Bush
War**），是指在發生2004年到2007年[中非共和國境內的一場內戰](../Page/中非共和國.md "wikilink")，與[查德內戰和](../Page/查德內戰.md "wikilink")[達爾富爾衝突皆為](../Page/達爾富爾衝突.md "wikilink")[中非戰爭的一部分](../Page/中非戰爭.md "wikilink")。

## 參考文獻

[\*](../Category/中非共和國內戰.md "wikilink")
[Category:法國戰爭](../Category/法國戰爭.md "wikilink")
[Category:查德戰爭](../Category/查德戰爭.md "wikilink")
[Category:中非共和國戰爭](../Category/中非共和國戰爭.md "wikilink")
[Category:中非共和國歷史](../Category/中非共和國歷史.md "wikilink")
[Category:中非共和國－查德關係](../Category/中非共和國－查德關係.md "wikilink")
[Category:2004年衝突](../Category/2004年衝突.md "wikilink")
[Category:2005年衝突](../Category/2005年衝突.md "wikilink")
[Category:2006年衝突](../Category/2006年衝突.md "wikilink")
[Category:2007年衝突](../Category/2007年衝突.md "wikilink")