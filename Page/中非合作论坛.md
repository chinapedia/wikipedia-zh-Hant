[Forum_on_China-Africa_Cooperation.svg](https://zh.wikipedia.org/wiki/File:Forum_on_China-Africa_Cooperation.svg "fig:Forum_on_China-Africa_Cooperation.svg")和[布吉納法索](../Page/布吉納法索.md "wikilink")\]\]

**中非合作论坛**（；）是[中华人民共和国和](../Page/中华人民共和国.md "wikilink")[非洲国家之间](../Page/非洲.md "wikilink")
\[1\]为进一步加强友好合作、促进共同发展而举行的定期对话论坛。中非合作论坛每三年舉行一次，首届部长级会议于2000年10月10日在中国[北京举行](../Page/北京.md "wikilink")。另外每屆會議舉辦地點會由中國北京和非洲國家輪流舉行。

论坛成員為中國及和中國有外交關係的非洲國家及非洲組織。目前沒有參加的非洲國家為和[中華民國有邦交的](../Page/中華民國.md "wikilink")[史瓦帝尼及因与摩洛哥有领土主权争议](../Page/史瓦帝尼.md "wikilink")，不被包括中华人民共和国在内的主權国家所承认的[阿拉伯撒哈拉民主共和国](../Page/阿拉伯撒哈拉民主共和国.md "wikilink")。

## 第一届部长级会议

第一届部长级会议于2000年10月10日在中国[北京举行](../Page/北京.md "wikilink")，中国方面出席的主要领导有[江泽民](../Page/江泽民.md "wikilink")、[朱镕基](../Page/朱镕基.md "wikilink")、[胡锦涛等](../Page/胡锦涛.md "wikilink")，非洲方面出席的有：[多哥总统](../Page/多哥.md "wikilink")[纳辛贝·埃亚德马](../Page/纳辛贝·埃亚德马.md "wikilink")、[阿尔及利亚总统](../Page/阿尔及利亚.md "wikilink")[阿布杜拉齐兹·布特弗利卡](../Page/阿布杜拉齐兹·布特弗利卡.md "wikilink")、[赞比亚总统](../Page/赞比亚.md "wikilink")[弗雷德里克·奇卢巴](../Page/弗雷德里克·奇卢巴.md "wikilink")、[坦桑尼亚总统](../Page/坦桑尼亚.md "wikilink")[本杰明·威廉·姆卡帕以及](../Page/本杰明·威廉·姆卡帕.md "wikilink")[非洲统一组织秘书长](../Page/非洲统一组织.md "wikilink")[萨利姆·艾哈迈德·萨利姆等](../Page/萨利姆·艾哈迈德·萨利姆.md "wikilink")。共有45个国家的80余名部长和17个国际和地区组织代表应邀与会。

会议的议题有两个，分別是「推动建立国际政治经济新秩序」及「加强中非在经贸领域的合作」。同時会议亦通过了两个文件：《中非合作论坛北京宣言》及《中非经济和社会发展合作纲领》。

## 第二届部长级会议

第二届部长级会议于2003年12月15日在[埃塞俄比亚](../Page/埃塞俄比亚.md "wikilink")[亚的斯亚贝巴举行](../Page/亚的斯亚贝巴.md "wikilink")。中国方面由[國務院總理](../Page/國務院總理.md "wikilink")[温家宝出席](../Page/温家宝.md "wikilink")，非洲方面出席的有[莫桑比克总统](../Page/莫桑比克.md "wikilink")[若阿金·阿尔贝托·希萨诺](../Page/若阿金·阿尔贝托·希萨诺.md "wikilink")、[乌干达总统](../Page/乌干达.md "wikilink")[约韦里·穆塞韦尼](../Page/约韦里·穆塞韦尼.md "wikilink")、[苏丹共和国总统](../Page/苏丹共和国.md "wikilink")[奥马尔·哈桑·艾哈迈德·巴希尔](../Page/奥马尔·哈桑·艾哈迈德·巴希尔.md "wikilink")、[津巴布韦总统](../Page/津巴布韦.md "wikilink")[罗伯特·穆加贝](../Page/罗伯特·穆加贝.md "wikilink")、[刚果共和国总统](../Page/刚果共和国.md "wikilink")[德尼·萨苏-恩格索](../Page/德尼·萨苏-恩格索.md "wikilink")，[科摩罗总统](../Page/科摩罗.md "wikilink")[阿扎利·阿苏马尼](../Page/阿扎利·阿苏马尼.md "wikilink")、[非盟委员会主席](../Page/非盟.md "wikilink")[阿尔法·乌马尔·科纳雷](../Page/阿尔法·乌马尔·科纳雷.md "wikilink")、埃塞俄比亚总理[梅莱斯·泽纳维等](../Page/梅莱斯·泽纳维.md "wikilink")。45个国家的70名部长及部分国际和非洲地区组织的代表参加会议。另外，中非企业家大会同时举行，500多企业在会上达成10亿余[美元的交易意向](../Page/美元.md "wikilink")。

本次会议的主题是务实合作、面向行动

会议通过了《亚的斯亚贝巴行动计划》，中国承诺在2004年至2006年的三年间继续增加对非洲的援助，具体措施有：

  - 为非洲培训各类人才1万人；
  - 开放市场，对最不发达国家部分商品给予免[关税待遇](../Page/关税.md "wikilink")；
  - 拓展旅游合作，开放埃塞俄比亚、[肯尼亚](../Page/肯尼亚.md "wikilink")、坦桑尼亚、赞比亚、[毛里求斯](../Page/毛里求斯.md "wikilink")、[塞舌尔](../Page/塞舌尔.md "wikilink")、津巴布韦和[突尼斯](../Page/突尼斯.md "wikilink")8个非洲国家为“[中国公民自费出国旅游目的地](../Page/中国公民自费出国旅游目的地.md "wikilink")”；
  - 举办“相约北京”国际艺术节和“中国文化非洲行”活动，促进非洲文化在中国传播；
  - 增加民间交流。

此次會議加入了於2003年和中國復交的[利比里亞](../Page/利比里亞.md "wikilink")。

## 北京峰会暨第三届部长级会议

第三届部长级会议于2006年11月3日在中国北京举行，中国和48个非洲国家的外交部长、负责经济合作事务的部长和代表出席了会议\[2\]。11月4日，[中非合作论坛北京峰会召开](../Page/中非合作论坛北京峰会.md "wikilink")，[中华人民共和国主席](../Page/中华人民共和国主席.md "wikilink")[胡锦涛](../Page/胡锦涛.md "wikilink")，48个[非洲国家的元首](../Page/非洲.md "wikilink")、政府首脑或代表，[非洲联盟委员会主席科纳雷以及地区和国际组织的代表出席](../Page/非洲联盟.md "wikilink")\[3\]。本次會議的焦點在於中方邀請了五個與[中華民國有外交關係國家的官員出席](../Page/中華民國.md "wikilink")。53个非洲国家中有48个国家派出代表团参加，其中有42个国家元首亲自带队。11月5日，中非合作论坛北京峰会闭幕，通过了《中非合作论坛北京峰会宣言》和《中非合作论坛-北京行动计划（2007至2009年）》。\[4\]

此次會議加入了分別於2005年和2006年和中國復交的[塞內加爾和](../Page/塞內加爾.md "wikilink")[乍得](../Page/乍得.md "wikilink")。

## 第四屆部長級會議

中非合作论坛第四屆部長級會議於2009年11月在[埃及](../Page/埃及.md "wikilink")[沙姆沙伊赫擧行](../Page/沙姆沙伊赫.md "wikilink")。此次会议主题为“深化中非新型战略伙伴关系，谋求可持续发展”，会议通过了《中非合作论坛沙姆沙伊赫宣言》和《中非合作论坛——沙姆沙伊赫行动计划（2010～2012年）》

此次會議加入了於2008年和中國建交的[馬拉維](../Page/馬拉維.md "wikilink")。

## 第五届部长级会议

中非合作论坛第五届部长级会议于 2013
年在北京举行。此次会议主题为“继往开来，开创中非新型战略伙伴关系新局面”，会议通过了《中非合作论坛第五届部长级会议北京宣言》和《中非合作论坛第五届部长级会议——北京行动计划（2013年至2015年）》。

## 第六屆部长级会议

中非合作论坛第六届部长级会议于2015年12月在南非[約翰尼斯堡举行](../Page/約翰尼斯堡.md "wikilink")。

## 第七屆部长级会议

[Kerry_Hotel_Beijing,_2018_Beijing_Summit_of_FOCAC,_Sep_2018.jpg](https://zh.wikipedia.org/wiki/File:Kerry_Hotel_Beijing,_2018_Beijing_Summit_of_FOCAC,_Sep_2018.jpg "fig:Kerry_Hotel_Beijing,_2018_Beijing_Summit_of_FOCAC,_Sep_2018.jpg")的[纳米比亚和](../Page/纳米比亚.md "wikilink")[莱索托领导人下榻北京嘉里大酒店](../Page/莱索托.md "wikilink")\]\]

[中非合作论坛北京峰会于](../Page/2018年中非北京峰会.md "wikilink")2018年9月3日至4日在北京举行。多个非洲国家领导人抵达与中国国家主席习近平会面。在中国推动“[一带一路](../Page/一带一路.md "wikilink")”以及[中美贸易战的大背景下](../Page/中美贸易战.md "wikilink")，中国积极谋求在非洲有更大的影响。在今年论坛举行前，中方已经向非洲多国送出大礼，落实一些投资项目。

据[新华社报道](../Page/新华社.md "wikilink")，这次峰会吸引多国领导人出席，是“中国在非洲影响力日益增强的结果”，峰会有望构建“更加紧密中非命运共同体”，和对接“一带一路”与非洲发展。

中国国家主席习近平在峰会开幕式上发表主旨讲话时表示，“中国愿以打造新时代更加紧密的中非命运共同体为指引，在推进中非‘十大合作计划’基础上，同非洲国家密切配合，未来3年和今后一段时间重点实施‘八大行动’”，全方位规划了中非合作的优先领域和重点方向。
本次峰会规模空前，这说明中非合作具有强大的生命力。非洲国家致力于建立共同市场，中国积极维护多边贸易体制，双方有着共同点。在贸易保护主义抬头的当今世界，中国与非洲的合作为南南合作树立典范，有力地维护了多边贸易体制，“我们的未来充满光明”。

中非领导人围绕“合作共赢，携手构建更加紧密的中非命运共同体”这一峰会主题，共叙友情，共商合作，共话未来。这是中非友好大家庭的又一次大团圆，也是中国今年举办的规模最大、外国领导人出席最多的主场外交。众多非洲国家领导人和非盟委员会主席率团与会，[联合国秘书长](../Page/联合国秘书长.md "wikilink")[古特瑞斯作为特邀嘉宾](../Page/古特瑞斯.md "wikilink")、27个国际和非洲地区组织作为观察员也出席了峰会有关活动。

此次會議新增了於2013年和中華民國斷交並於2016年和中國復交的[甘比亞及在](../Page/甘比亞.md "wikilink")2016、2018年先後与中华民国斷交，並与中华人民共和国复交的[圣多美普林西比](../Page/圣多美普林西比.md "wikilink")、[布吉纳法索](../Page/布吉纳法索.md "wikilink")。

## 参考文献

  - 中國的第三世界政策與地緣政治策略. 李榭熙; [香港社會科學學報](../Page/香港社會科學學報.md "wikilink");
    36 2009.春-夏; 頁139-161

## 外部链接

  - [中非合作论坛官方网站](http://www.focac.org/chn/)

## 参见

  - [中非合作关系](../Page/中非合作关系.md "wikilink")

{{-}}

[Category:中华人民共和国与非洲国家关系](../Category/中华人民共和国与非洲国家关系.md "wikilink")
[Category:国际组织](../Category/国际组织.md "wikilink")

1.
2.  谭晶晶、郝亚琳：[中非论坛部长级会议召开](http://news.china.com.cn/chinanet/china.cgi?docid=99468643,59472306&server=192.168.5.192&port=3000)，中国网，2006年11月4日。
3.  谭晶晶：[中非合作论坛北京峰会开幕](http://www.chinadaily.com.cn/hqkx/2006-11/04/content_724535.htm)，中国日报，2006年11月4日。
4.  梁霓霓，[中非合作论坛北京峰会闭幕](http://news.xinhuanet.com/world/2006-11/05/content_5292446.htm)，新华网