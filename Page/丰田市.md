**丰田市**（），是[日本的](../Page/日本.md "wikilink")[爱知县的城市之一](../Page/爱知县.md "wikilink")。

## 地理

位于[名古屋市的东方约](../Page/名古屋市.md "wikilink")30公里，人口约42万人的城市。

### 接鄰行政區

  - [愛知縣](../Page/愛知縣.md "wikilink")

<!-- end list -->

  - [安城市](../Page/安城市.md "wikilink")
  - [岡崎市](../Page/岡崎市.md "wikilink")
  - [刈谷市](../Page/刈谷市.md "wikilink")
  - [瀨戶市](../Page/瀨戶市.md "wikilink")
  - [知立市](../Page/知立市.md "wikilink")
  - [日進市](../Page/日進市.md "wikilink")
  - [長久手市](../Page/長久手市.md "wikilink")
  - [設樂町](../Page/設樂町.md "wikilink")
  - [三好市](../Page/三好市.md "wikilink")

<!-- end list -->

  - [岐阜縣](../Page/岐阜縣.md "wikilink")

<!-- end list -->

  - [惠那市](../Page/惠那市.md "wikilink")
  - [土岐市](../Page/土岐市.md "wikilink")
  - [瑞浪市](../Page/瑞浪市.md "wikilink")

<!-- end list -->

  - [長野縣](../Page/長野縣.md "wikilink")

<!-- end list -->

  - [根羽村](../Page/根羽村.md "wikilink")

## 歴史

自1938年[丰田汽车設置以來](../Page/丰田汽车.md "wikilink")，本市就以[汽车](../Page/汽车.md "wikilink")[产业作为發展核心的](../Page/产业.md "wikilink")[工业城市](../Page/工业.md "wikilink")。有松平姓发祥地，松平乡。

丰田市舊名是**举母市**，不過因為丰田汽车使該市能夠迅速發展起來，於1959年更名為**丰田市**。

[2005年日本国际博览会的会场跨越](../Page/2005年日本国际博览会.md "wikilink")**丰田市**、[濑户市及](../Page/濑户市.md "wikilink")[长久手町等](../Page/长久手町.md "wikilink")3个城市。

## 國際関係

  - 姉妹都市

<!-- end list -->

  - [底特律](../Page/底特律.md "wikilink")（[美国](../Page/美国.md "wikilink")
    [密歇根州](../Page/密歇根州.md "wikilink")
    [韦恩縣](../Page/韦恩县_\(密歇根州\).md "wikilink")）-
    [1960年](../Page/1960年.md "wikilink")

  - [本德](../Page/本德_\(俄勒冈州\).md "wikilink")（[美国](../Page/美国.md "wikilink")
    [俄勒冈州](../Page/俄勒冈州.md "wikilink")
    [德舒特縣](../Page/德舒特縣.md "wikilink")）-
    [1997年](../Page/1997年.md "wikilink")

  - [德比](../Page/德比_\(英格蘭\).md "wikilink")（[英国](../Page/英国.md "wikilink")
    [英格兰](../Page/英格兰.md "wikilink") [德比郡](../Page/德比郡.md "wikilink")）-
    [1998年](../Page/1998年.md "wikilink")

  - [南德比郡](../Page/南德比郡.md "wikilink")（[英国](../Page/英国.md "wikilink")
    [英格兰](../Page/英格兰.md "wikilink")
    [德比郡](../Page/德比郡.md "wikilink")）- 1998年

## 教育

  - 大學

<!-- end list -->

  - 愛知学泉大學
  - 愛知工業大學
  - 桜花学園大學 豐田校园
  - [中京大學](../Page/中京大學.md "wikilink") 豐田校园
  - 日本赤十字豊田看護大學

## 交通

### 鐵道

  - 中央車站：[豊田市站](../Page/豊田市站.md "wikilink")、[新豊田站](../Page/新豊田站.md "wikilink")

<!-- end list -->

  - [Meitetsu_logomark_2.svg](https://zh.wikipedia.org/wiki/File:Meitetsu_logomark_2.svg "fig:Meitetsu_logomark_2.svg")[名古屋鐵道](../Page/名古屋鐵道.md "wikilink")（名鉄）

<!-- end list -->

  - [三河線](../Page/三河線.md "wikilink")

<!-- end list -->

  - [Aikan_logomark.svg](https://zh.wikipedia.org/wiki/File:Aikan_logomark.svg "fig:Aikan_logomark.svg")[愛知環狀鐵道](../Page/愛知環狀鐵道.md "wikilink")（愛環）

<!-- end list -->

  - [愛知環狀鐵道線](../Page/愛知環狀鐵道線.md "wikilink")

<!-- end list -->

  - [Aichi_Rapid_Transit_logo.png](https://zh.wikipedia.org/wiki/File:Aichi_Rapid_Transit_logo.png "fig:Aichi_Rapid_Transit_logo.png")[愛知高速交通](../Page/愛知高速交通.md "wikilink")（Linimo）

<!-- end list -->

  - [東部丘陵線](../Page/東部丘陵線.md "wikilink")

### 道路

  - [東名高速公路](../Page/東名高速公路.md "wikilink")
  - [新東名高速公路](../Page/新東名高速公路.md "wikilink")
  - [伊勢灣岸自動車道](../Page/伊勢灣岸自動車道.md "wikilink")
  - [東海環狀自動車道](../Page/東海環狀自動車道.md "wikilink")

<File:Meitetsu> Toyotashi Station ac.jpg|豊田市站 <File:Aikan> Shin-Toyota
Sta. - panoramio.jpg|新豊田站 <File:TOYOTA> Interchange on TOMEI EXPWY and
SHIN-TOMEI EXPWY.jpg|[豐田系統交流道](../Page/豐田系統交流道.md "wikilink")
<File:Isewangan20180501B.jpg>|[伊勢灣岸自動車道](../Page/伊勢灣岸自動車道.md "wikilink")･[東海環狀自動車道](../Page/東海環狀自動車道.md "wikilink")

## 观光景点

  - [豐田體育場](../Page/豐田體育場.md "wikilink")
  - 松平郷
  - 勘八峡
  - 足助（[重要傳統的建造物群保存地區](../Page/重要傳統的建造物群保存地區.md "wikilink")）
  - 香嵐渓
  - 猿投神社（[三河國](../Page/三河國.md "wikilink")
    [三宮](../Page/一宮.md "wikilink")）

<File:Toyota> Stadium, Mori-cho Toyota
2012.JPG|[豐田體育場](../Page/豐田體育場.md "wikilink")
[File:Matsudairago1.jpg|松平郷](File:Matsudairago1.jpg%7C松平郷)
<File:Kampachi> Gorge, Toyota-city 2018.jpg|勘八峡 <File:Asuke> Shintamachi
Yamashiroya Ryokan ac (1).jpg|足助 <File:Kourankei> was illuminated
香嵐渓のライトアップ - panoramio.jpg|香嵐渓 <File:Sanage> Jinja shrine
haiden, Sanage-cho Toyota 2013.jpg|猿投神社

## 関連項目

[Category:豐田市](../Category/豐田市.md "wikilink")
[Category:以人名命名的行政区](../Category/以人名命名的行政区.md "wikilink")