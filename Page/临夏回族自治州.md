**臨夏回族自治州**，简称**临夏州**，是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[甘肃省下辖的](../Page/甘肃省.md "wikilink")[自治州](../Page/自治州.md "wikilink")，位于甘肃省中部，[黄河上游](../Page/黄河.md "wikilink")，[黄土高原与](../Page/黄土高原.md "wikilink")[青藏高原交界](../Page/青藏高原.md "wikilink")。首府临夏市是古代[丝绸之路](../Page/丝绸之路.md "wikilink")、唐蕃古道的重镇，[明代四大](../Page/明代.md "wikilink")[茶马司之一](../Page/茶马.md "wikilink")，有“河湟雄镇”之称。临夏州[汉族人口比例约](../Page/汉族.md "wikilink")40%，[回族人口比例约](../Page/回族.md "wikilink")32%。

## 历史

[秦代置枹罕县](../Page/秦代.md "wikilink")（fú
hǎn），属[陇西郡](../Page/陇西郡.md "wikilink")，治所在今临夏县[韩集镇双城](../Page/韩集镇_\(临夏县\).md "wikilink")。[西汉](../Page/西汉.md "wikilink")[始元六年](../Page/始元.md "wikilink")（前81年），改属[金城郡](../Page/金城郡.md "wikilink")。[东汉](../Page/东汉.md "wikilink")[建武十二年](../Page/建武.md "wikilink")（36年），撤金城郡，枹罕县改属陇西郡。[西晋](../Page/西晋.md "wikilink")[太康二年](../Page/太康.md "wikilink")（281年），废枹罕县，立枹罕护军。[永嘉年间](../Page/永嘉.md "wikilink")，枹罕县属[晋兴郡](../Page/晋兴郡.md "wikilink")。[十六国](../Page/十六国.md "wikilink")[前凉](../Page/前凉.md "wikilink")[太元二十一年](../Page/太元.md "wikilink")（344年），析[凉州新置](../Page/凉州.md "wikilink")[河州](../Page/河州_\(前凉\).md "wikilink")，州、郡均治枹罕。后境地分別为[前秦](../Page/前秦.md "wikilink")、[西秦](../Page/西秦.md "wikilink")、[后秦](../Page/后秦.md "wikilink")、[西夏](../Page/西夏.md "wikilink")、[北魏](../Page/北魏.md "wikilink")、[西魏](../Page/西魏.md "wikilink")、[北周统治](../Page/北周.md "wikilink")。
[缩略图](https://zh.wikipedia.org/wiki/File:5637-Linxia-City-Dongguan-back-street.jpg "fig:缩略图")
[隋](../Page/隋.md "wikilink")[开皇三年](../Page/开皇.md "wikilink")（583年）废枹罕县入河州。[大业三年](../Page/大业.md "wikilink")（607年），河州改枹罕郡。[唐朝](../Page/唐朝.md "wikilink")[武德元年](../Page/武德.md "wikilink")（618年），复置枹罕县，为河州治所。[天宝元年](../Page/天宝_\(唐朝\).md "wikilink")（742年），改河州为[安乡郡](../Page/安乡郡.md "wikilink")。[宝应元年](../Page/宝应_\(唐朝\).md "wikilink")（762年），安乡郡为[吐蕃所占](../Page/吐蕃.md "wikilink")。
[缩略图](https://zh.wikipedia.org/wiki/File:5665-Linxia-City-Hongyuan-Park-east-gate.jpg "fig:缩略图")
[北宋](../Page/北宋.md "wikilink")[熙宁六年](../Page/熙宁.md "wikilink")（1073年），大将[王韶收复河州](../Page/王韶.md "wikilink")，复置枹罕县，属[熙河路](../Page/熙河路.md "wikilink")。熙宁九年（1076年）省枹罕县入安乡县。[金朝](../Page/金朝.md "wikilink")[天会九年](../Page/天会_\(金朝\).md "wikilink")（1131年），河州为金所据。[皇统二年](../Page/皇统.md "wikilink")（1142年），移州治至枹罕城。[贞元二年](../Page/贞元_\(金朝\).md "wikilink")（1154年）复置枹罕县，为河州治。[大定二十七年](../Page/大定_\(金朝\).md "wikilink")（1187年），枹罕县改属临洮路。[成吉思汗二十一年](../Page/成吉思汗.md "wikilink")（1227年），蒙古军攻占河州。[元朝](../Page/元朝.md "wikilink")[至元六年](../Page/至元_\(忽必烈\).md "wikilink")（1269年）省枹罕县入安乡县。[明朝](../Page/明朝.md "wikilink")[洪武四年](../Page/洪武.md "wikilink")（1371年），置[河州卫](../Page/河州卫.md "wikilink")。

[清朝](../Page/清朝.md "wikilink")[乾隆年间](../Page/乾隆.md "wikilink")，[苏四十三反清](../Page/苏四十三.md "wikilink")，是为[第一次河煌事变](../Page/第一次河煌事变.md "wikilink")。[同治元年](../Page/同治.md "wikilink")（1862年），中国西北地区发生[同治陕甘回变](../Page/同治陕甘回变.md "wikilink")。回民、撒拉族等[穆斯林与汉民之间相互仇杀](../Page/穆斯林.md "wikilink")，战乱持续十余年，终被清政府镇压。其中，河州莫尼沟[阿訇](../Page/阿訇.md "wikilink")[马占鳌起兵反清](../Page/马占鳌.md "wikilink")，即[第二次河湟事变](../Page/第二次河湟事变.md "wikilink")。1872年，马占鳌率众归降朝廷，奠定了河州[马家军崛起的基础](../Page/马家军.md "wikilink")，终成为[民国时期实际控制甘肃](../Page/民国时期.md "wikilink")、[宁夏](../Page/宁夏.md "wikilink")、[青海等地的地方](../Page/青海.md "wikilink")[军阀](../Page/军阀.md "wikilink")。[光绪二十一年](../Page/光绪.md "wikilink")（1895年），[第三次河湟事变](../Page/第三次河湟事变.md "wikilink")。

[民国二年](../Page/民国.md "wikilink")（1913年），改设导河县，属[兰山道](../Page/兰山道.md "wikilink")。1927年，废道改区，属兰山区。1928年，[马仲英反叛](../Page/马仲英.md "wikilink")，引发[第四次河湟事变](../Page/第四次河湟事变.md "wikilink")。民国18年（1929年），改称临夏县。民国33年（1944年），属甘肃省临夏行政督察专员公署，仍辖临夏、临洮、宁定、和政、永靖、夏河等6县。1936年5月，改临夏行政督察专员公署为甘肃省第五区行政督察专员公署，将临洮县划归第一行政督察区。1939年4月，将夏河县划归第一行政督察区。1940年，改[康乐设治局为康乐县](../Page/康乐设治局.md "wikilink")。

1949年8月，[陕甘宁边区人民政府发布第](../Page/陕甘宁边区.md "wikilink")120号命令，设立[临夏行政督察专员公署](../Page/临夏专区.md "wikilink")，辖临夏、临洮、洮沙（治今临洮县[太石镇](../Page/太石镇.md "wikilink")）、宁定（治今广河县[城关镇](../Page/城关镇_\(广河县\).md "wikilink")）、和政、康乐、永靖、夏河等8县。1949年8月下旬，[中国人民解放军第一野战军第一兵团解放临夏地区](../Page/中国人民解放军第一野战军.md "wikilink")。1949年9月初，在临夏县成立洮西行政督察专员公署，辖上述8县。1949年9月下旬，改洮西专区为临夏专区；将临洮县划归新设的临洮专区。

1950年5月，洮沙县划归临洮专区，夏河县改由省直辖；将岷县分区的临潭县划归临夏分区。以临夏县东乡以及永靖、和政、宁定等县部分地区设立东乡自治区（县级）。1950年6月，析临夏县城区置临夏市（县级），临夏县由城区迁驻韩家集。1950年9月，改临夏行政督察专员公署为临夏分区专员公署。

1953年，临夏市改由省直辖；宁定县改设[广通回族自治区](../Page/广河县.md "wikilink")（县级）；临潭县划归[甘南藏族自治区](../Page/甘南藏族自治区.md "wikilink")；10月[东乡自治区改为](../Page/东乡族自治县.md "wikilink")[东乡族自治区](../Page/东乡族自治县.md "wikilink")。

1955年7月，东乡族自治区改名东乡族自治县；广通回族自治区改名广通回族自治县。1956年11月，撤销临夏专区，设立**临夏回族自治州**。辖临夏市和临夏、和政、永靖、康乐4县及东乡族自治县、广通回族自治县。寻改广通回族自治县为广通县。

1957年，[广通县改称](../Page/广通县.md "wikilink")[广河县](../Page/广河县.md "wikilink")。1958年，永靖、临夏2县并入临夏市；广河、康乐2县并入和政县，1961年恢复。1973年12月，甘肃省报告国务院：撤销临夏市，并入临夏县，临夏县由韩家集迁驻城关镇（今临夏市城区）。1980年6月，国务院批准：析临夏县西北部置积石山保安族东乡族撒拉族自治县。1983年8月，国务院批准：恢复临夏市（县级），以临夏县的城关镇和城关、南龙、折桥、枹罕4个公社为临夏市的行政区域。临夏县迁驻韩家集（第三次）。

## 地理

临夏州位于甘肃中部，省会[兰州以南](../Page/兰州.md "wikilink")，西为[青海省边界](../Page/青海省.md "wikilink")，南面是[甘南藏族自治州](../Page/甘南藏族自治州.md "wikilink")，东邻[定西市](../Page/定西市.md "wikilink")。地形以高原、山区、黄土丘陵区为主。平均海拔2000米以上。[黄河贯穿西北部](../Page/黄河.md "wikilink")，在永靖县[刘家峡前形成](../Page/刘家峡.md "wikilink")[刘家峡水库](../Page/刘家峡水库.md "wikilink")。黄河主要支流——大夏河和[洮河从邻近的](../Page/洮河.md "wikilink")[甘南州流向刘家峡水库](../Page/甘南州.md "wikilink")。洮河亦是临夏州与定西市的一部分边界。全年平均气温为8℃，无霜期155天，为[温带半干旱气候](../Page/温带半干旱气候.md "wikilink")，年均降水量只有442毫米。

## 政治

### 现任领导

<table>
<caption>临夏回族自治州四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党临夏回族自治州委员会.md" title="wikilink">中国共产党<br />
临夏回族自治州委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/临夏回族自治州人民代表大会.md" title="wikilink">临夏回族自治州人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/临夏回族自治州人民政府.md" title="wikilink">临夏回族自治州人民政府</a><br />
<br />
州长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议临夏回族自治州委员会.md" title="wikilink">中国人民政治协商会议<br />
临夏回族自治州委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/杨元忠.md" title="wikilink">杨元忠</a>[1]</p></td>
<td><p><a href="../Page/马明杰.md" title="wikilink">马明杰</a>[2]</p></td>
<td><p><a href="../Page/马相忠_(1962年).md" title="wikilink">马相忠</a>[3]</p></td>
<td><p><a href="../Page/王正君.md" title="wikilink">王正君</a>[4]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p><a href="../Page/回族.md" title="wikilink">回族</a></p></td>
<td><p>回族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/陕西省.md" title="wikilink">陕西省</a><a href="../Page/澄城县.md" title="wikilink">澄城县</a></p></td>
<td><p>甘肃省<a href="../Page/广河县.md" title="wikilink">广河县</a></p></td>
<td><p>甘肃省<a href="../Page/会宁县.md" title="wikilink">会宁县</a></p></td>
<td><p>甘肃省<a href="../Page/会宁县.md" title="wikilink">会宁县</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2015年5月</p></td>
<td><p>2016年11月</p></td>
<td><p>2018年7月</p></td>
<td><p>2016年4月</p></td>
</tr>
</tbody>
</table>

### 行政区划

[缩略图](https://zh.wikipedia.org/wiki/File:5836-Linxia-City-Nanmen-Guangchang-Xinhua-Bookstore.jpg "fig:缩略图")
下辖1个[县级市](../Page/县级市.md "wikilink")、5个[县](../Page/县_\(中华人民共和国\).md "wikilink")、2个[自治县](../Page/自治县.md "wikilink")。

  - 县级市：[臨夏市](../Page/臨夏市.md "wikilink")
  - 县：[臨夏县](../Page/臨夏县.md "wikilink")、[康乐县](../Page/康乐县.md "wikilink")、[永靖县](../Page/永靖县.md "wikilink")、[广河县](../Page/广河县.md "wikilink")、[和政县](../Page/和政县.md "wikilink")
  - 自治县：[东乡族自治县](../Page/东乡族自治县.md "wikilink")、[积石山保安族东乡族撒拉族自治县](../Page/积石山保安族东乡族撒拉族自治县.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p><strong>临夏回族自治州行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[5]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>622900</p></td>
</tr>
<tr class="odd">
<td><p>622901</p></td>
</tr>
<tr class="even">
<td><p>622921</p></td>
</tr>
<tr class="odd">
<td><p>622922</p></td>
</tr>
<tr class="even">
<td><p>622923</p></td>
</tr>
<tr class="odd">
<td><p>622924</p></td>
</tr>
<tr class="even">
<td><p>622925</p></td>
</tr>
<tr class="odd">
<td><p>622926</p></td>
</tr>
<tr class="even">
<td><p>622927</p></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>临夏回族自治州各市（县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[6]（2010年11月）</p></th>
<th><p>户籍人口[7]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>临夏回族自治州</p></td>
<td><p>1946677</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>临夏市</p></td>
<td><p>274466</p></td>
<td><p>14.10</p></td>
</tr>
<tr class="even">
<td><p>临夏县</p></td>
<td><p>326123</p></td>
<td><p>16.75</p></td>
</tr>
<tr class="odd">
<td><p>康乐县</p></td>
<td><p>233173</p></td>
<td><p>11.98</p></td>
</tr>
<tr class="even">
<td><p>永靖县</p></td>
<td><p>180161</p></td>
<td><p>9.25</p></td>
</tr>
<tr class="odd">
<td><p>广河县</p></td>
<td><p>227466</p></td>
<td><p>11.68</p></td>
</tr>
<tr class="even">
<td><p>和政县</p></td>
<td><p>185083</p></td>
<td><p>9.51</p></td>
</tr>
<tr class="odd">
<td><p>东乡族自治县</p></td>
<td><p>284507</p></td>
<td><p>14.62</p></td>
</tr>
<tr class="even">
<td><p>积石山保安族东乡族撒拉族自治县</p></td>
<td><p>235698</p></td>
<td><p>12.11</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全州[常住人口为](../Page/常住人口.md "wikilink")1946677人\[8\]。其中，男性人口为985235人，占50.61%；女性人口为961442人，占49.39%。人口性别比（以女性为100）为102.47。0－14岁人口为422288人，占21.69%；15－64岁人口为1365200人，占70.13%；65岁及以上人口为159189人，占8.18%。

### 民族

全州常住人口中，[汉族人口为](../Page/汉族.md "wikilink")772872人，占39.70%；各[少数民族人口为](../Page/少数民族.md "wikilink")1173805人，占60.30%。

临夏是个多元民族、宗教的地方，回族占境内的人口约三分之一。信奉[伊斯兰教的民族包括](../Page/伊斯兰教.md "wikilink")[回族](../Page/回族.md "wikilink")、[东乡族](../Page/东乡族.md "wikilink")、[保安族](../Page/保安族.md "wikilink")、[撒拉族等](../Page/撒拉族.md "wikilink")，占全州人口超过一半。另外还有[土族](../Page/土族.md "wikilink")、[藏族等少数民族](../Page/藏族.md "wikilink")。

{{-}}

| 民族名称         | [汉族](../Page/汉族.md "wikilink") | [回族](../Page/回族.md "wikilink") | [东乡族](../Page/东乡族.md "wikilink") | [保安族](../Page/保安族.md "wikilink") | [土族](../Page/土族.md "wikilink") | [撒拉族](../Page/撒拉族.md "wikilink") | [藏族](../Page/藏族.md "wikilink") | [满族](../Page/满族.md "wikilink") | [蒙古族](../Page/蒙古族.md "wikilink") | [土家族](../Page/土家族.md "wikilink") | 其他民族 |
| ------------ | ------------------------------ | ------------------------------ | -------------------------------- | -------------------------------- | ------------------------------ | -------------------------------- | ------------------------------ | ------------------------------ | -------------------------------- | -------------------------------- | ---- |
| 人口数          | 772872                         | 614905                         | 505976                           | 17834                            | 13844                          | 12585                            | 7806                           | 350                            | 221                              | 60                               | 224  |
| 占总人口比例（%）    | 39.70                          | 31.59                          | 25.99                            | 0.92                             | 0.71                           | 0.65                             | 0.40                           | 0.02                           | 0.01                             | 0.00                             | 0.01 |
| 占少数民族人口比例（%） | \---                           | 52.39                          | 43.11                            | 1.52                             | 1.18                           | 1.07                             | 0.67                           | 0.03                           | 0.02                             | 0.01                             | 0.02 |

**临夏回族自治州民族构成（2010年11月）**\[9\]

## 经济建设

[6058-Liujiaxia-Dam.jpg](https://zh.wikipedia.org/wiki/File:6058-Liujiaxia-Dam.jpg "fig:6058-Liujiaxia-Dam.jpg")

临夏州是全国最贫困的地区之一，全州除首府临夏市外，其余县份均为[国家级贫困县](../Page/国家级贫困县.md "wikilink")。2010年，全州人均生产总值5441元，约804美元，东乡族自治县更只有2785元，即411美元。

2011年，全州[生产总值](../Page/生产总值.md "wikilink")128.78亿元，比上年增长13.1%。其中，第一产业增加值27亿元，比上年增长5.2%；第二产业增加值39.94亿元，比上年增长15.3%；第三产业增加值61.84亿元，比上年增长15.4%。\[10\]

  - [刘家峡水电站位于](../Page/刘家峡水电站.md "wikilink")[永靖县境内](../Page/永靖县.md "wikilink")[黄河干流上](../Page/黄河.md "wikilink")，是当时亚洲第一座百万千瓦级大型水电站。始建于1958年9月，1974年12月竣工。中共中央总书记[胡锦涛和夫人](../Page/胡锦涛.md "wikilink")[刘永清](../Page/刘永清.md "wikilink")，曾亲身参加刘家峡水电站的建设。

## 交通

穿越南北，通过永靖县、东乡族自治县、临夏市、临夏县。[兰青铁路在州境北部](../Page/兰青铁路.md "wikilink")[永靖县经过](../Page/永靖县.md "wikilink")。
S2兰郎高速经过广河县、和政县、临夏县、临夏市。

## 著名景点

  - [炳灵寺石窟](../Page/炳灵寺石窟.md "wikilink")——位于永靖县[积石山大寺沟西侧的崖壁上](../Page/积石山.md "wikilink")。[西晋初年开凿在黄河北岸大寺沟的峭壁之上](../Page/西晋.md "wikilink")，正式建立于[西秦](../Page/西秦.md "wikilink")[建弘元年](../Page/建弘.md "wikilink")（420年），[明朝](../Page/明朝.md "wikilink")[永乐后称](../Page/永乐.md "wikilink")[炳灵寺](../Page/炳灵寺.md "wikilink")。
  - [临夏南关大寺](../Page/临夏南关大寺.md "wikilink")——[伊斯兰教](../Page/伊斯兰教.md "wikilink")[清真寺](../Page/清真寺.md "wikilink")，位于[临夏市南门广场](../Page/临夏市.md "wikilink")，是甘肃规模较大，历史较悠久的清真寺。
  - [临夏老华寺](../Page/临夏老华寺.md "wikilink")——伊斯兰教清真寺，亦称大华寺，位于[临夏市华寺街](../Page/临夏市.md "wikilink")。始建于1368年，距今已经有600多年的历史，为甘肃规模较大、历史较久远的清真寺。
  - [临夏东关大清真寺](../Page/临夏东关大清真寺.md "wikilink")
  - [临夏万寿观](../Page/临夏万寿观.md "wikilink")
  - [八坊十三巷](../Page/八坊十三巷.md "wikilink")——位于[临夏市主城区](../Page/临夏市.md "wikilink")，是一片具有回族民族特色的历史文化街区，[国家4A级景区](../Page/国家4A级景区.md "wikilink")，被称为民族建筑艺术“大观园”、“甘肃新地标”和临夏文化旅游新名片。

<center>

[File:Bingling02.jpg|炳灵寺石窟](File:Bingling02.jpg%7C炳灵寺石窟)
[File:5616-Linxia-Dongguan-Mosque.jpg|临夏市东关大清真寺](File:5616-Linxia-Dongguan-Mosque.jpg%7C临夏市东关大清真寺)
[File:5676-Linxia-City-Laohua-Mosque.jpg|临夏市老华寺](File:5676-Linxia-City-Laohua-Mosque.jpg%7C临夏市老华寺)
[File:5757-Linxia-Wanshou-Guan-pagoda.jpg|临夏万寿观](File:5757-Linxia-Wanshou-Guan-pagoda.jpg%7C临夏万寿观)
[File:5747-Linxia-Chengjiao-Si.jpg|临夏城郊清真寺](File:5747-Linxia-Chengjiao-Si.jpg%7C临夏城郊清真寺)
[File:5648-Linxia-City-market-produce.jpg|临夏市场](File:5648-Linxia-City-market-produce.jpg%7C临夏市场)

</center>

## 参见

  - [河州](../Page/河州.md "wikilink")

## 外部链接

  - [临夏回族自治州人民政府门户网站](https://www.linxia.gov.cn/)

## 注释

## 参考文献

{{-}}

[临夏回族自治州](../Category/临夏回族自治州.md "wikilink")
[州](../Category/甘肃市州.md "wikilink")
[甘](../Category/回族自治州.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10. [《2011年临夏回族自治州国民经济和社会发展统计公报》](http://www.gstj.gov.cn/doc/ShowArticle.asp?ArticleID=13386)