**临淄**位于[中華人民共和國](../Page/中華人民共和國.md "wikilink")[山东省中部的一座](../Page/山东省.md "wikilink")[石油](../Page/石油.md "wikilink")[化工工业城市](../Page/化工.md "wikilink")，是[淄博市的一个](../Page/淄博市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")。在古代曾为[西周](../Page/西周.md "wikilink")、[春秋](../Page/春秋.md "wikilink")、[战国时的大国](../Page/战国.md "wikilink")[齐国国都](../Page/齐国.md "wikilink")，1994年被[國務院定为](../Page/中華人民共和國國務院.md "wikilink")[国家历史文化名城](../Page/国家历史文化名城.md "wikilink")。

## 历史

临淄，古称**营丘**，秦汉时作**临菑**。相传上古时期的[伏羲](../Page/伏羲.md "wikilink")、[颛顼曾在这里活动](../Page/颛顼.md "wikilink")。此后为爽鸠氏、蒲姑氏等部族的聚居地。公元前约1046年，[周朝建立后](../Page/周朝.md "wikilink")，[周武王把首席功臣](../Page/周武王.md "wikilink")[太公望分封于此](../Page/太公望.md "wikilink")，建立了侯国[齐国](../Page/齐国.md "wikilink")，定都营丘。六世[齐胡公迁薄姑](../Page/齐胡公.md "wikilink")。七世[齐献公迁回营丘](../Page/齐献公.md "wikilink")，因其地临淄水，遂改名为临淄，此后临淄之名一直延用至今。
[Linzi_model_2010_06_06.jpg](https://zh.wikipedia.org/wiki/File:Linzi_model_2010_06_06.jpg "fig:Linzi_model_2010_06_06.jpg")
齐国是[西周](../Page/西周.md "wikilink")、[春秋](../Page/春秋.md "wikilink")、[战国时的大国](../Page/战国.md "wikilink")，因此临淄作为其都城，历史上极为繁荣。目前[临淄齐国故城的城址尚存](../Page/临淄齐国故城.md "wikilink")，分民众生活的大城和国君居住的小城，两城相连，周长21公里，面积15平方公里，共有十三座城门。城内干道纵横交错，排成“井”字形，并有完善的供水、排水系统。城外还有埋葬齐国六位君主的[田齐王陵等多处](../Page/田齐王陵.md "wikilink")[先秦墓葬](../Page/先秦.md "wikilink")。

[东周时的临淄人物阜盛](../Page/东周.md "wikilink")，齐相国[管仲著](../Page/管仲.md "wikilink")《[管子](../Page/管子_\(書籍\).md "wikilink")》。[孔子曾来此闻韶乐](../Page/孔子.md "wikilink")，[孟子也曾为齐王担任客卿](../Page/孟子.md "wikilink")。[庄子曾被](../Page/庄子.md "wikilink")[齐湣王聘为相](../Page/齐湣王.md "wikilink")。战国时临淄设[稷下学宫](../Page/稷下学宫.md "wikilink")，以招徕[诸子百家来此讲学辩论](../Page/诸子百家.md "wikilink")、著书立说，[荀子即为稷下的祭酒之一](../Page/荀子.md "wikilink")。游学于此的还有[驺衍](../Page/驺衍.md "wikilink")、[宋钘](../Page/宋钘.md "wikilink")、[尹文](../Page/尹文.md "wikilink")、[彭蒙](../Page/彭蒙.md "wikilink")、[田骈](../Page/田骈.md "wikilink")、[慎到等](../Page/慎到.md "wikilink")，号为[稷下学派](../Page/稷下学派.md "wikilink")。当时的临淄邻近[渤海](../Page/渤海.md "wikilink")，因而兼具[鱼](../Page/鱼.md "wikilink")[盐之利](../Page/盐.md "wikilink")，经济繁荣、贸易发达，各种冶[铁](../Page/铁.md "wikilink")、炼[铜](../Page/铜.md "wikilink")、铸钱、制[陶](../Page/陶.md "wikilink")、[纺织的作坊遍布城市内外](../Page/纺织.md "wikilink")。

临淄作为齐国的都城，共延续了八百余年历史，直到被[秦国所灭](../Page/秦国.md "wikilink")。齊國都城[臨淄鼎盛時有](../Page/臨淄.md "wikilink")7萬戶，約35萬人口。[秦置临淄县](../Page/秦.md "wikilink")，属[齐郡](../Page/齐郡.md "wikilink")。两汉时，临淄为五都之一。[汉高祖刘邦封](../Page/汉高祖.md "wikilink")[韩信为齐王](../Page/韩信.md "wikilink")，都临淄。韩信死后，高祖将长子[刘肥封于临淄](../Page/刘肥.md "wikilink")，建[齐国](../Page/齐郡.md "wikilink")，辖七十余城。文帝时将齐国一分为七。据史载，当时的临淄约有十万户人家，依然是东部地区最大的城市。及至[晋代](../Page/晋代.md "wikilink")，临淄多遭兵祸，政权更迭频繁，其中[南燕曾短暂地建都于此](../Page/南燕.md "wikilink")。此后临淄在[青州的地位被](../Page/青州.md "wikilink")[益都所取代](../Page/益都.md "wikilink")。[南北朝后的历代](../Page/南北朝.md "wikilink")，临淄先后属[齐郡](../Page/齐郡.md "wikilink")、青州、益都府、益都路、青州府等管辖。[元末原城址被废弃](../Page/元.md "wikilink")，而在东南部另筑新城，即今临淄城区。

## 经济

[Linzi_road.JPG](https://zh.wikipedia.org/wiki/File:Linzi_road.JPG "fig:Linzi_road.JPG")
临淄靠近[东营](../Page/东营.md "wikilink")[胜利油田](../Page/胜利油田.md "wikilink")，附近也多产[煤炭和](../Page/煤炭.md "wikilink")[铁矿石](../Page/铁.md "wikilink")。1969年临淄县被并入工矿城市[淄博市](../Page/淄博.md "wikilink")，成为其临淄区。区驻地辛店位于[胶济铁路线上](../Page/胶济铁路.md "wikilink")，是一座新兴的工业城镇。目前经济以[化学工业为主](../Page/化学.md "wikilink")，[齐鲁石化公司是](../Page/齐鲁石化公司.md "wikilink")[中国石油化工集团的核心企业之一](../Page/中国石油化工集团.md "wikilink")，全国特大型国有企业，拥有居中国前列的[乙烯](../Page/乙烯.md "wikilink")、合成[树脂](../Page/树脂.md "wikilink")、[橡胶](../Page/橡胶.md "wikilink")、[烧碱生产能力](../Page/烧碱.md "wikilink")。经国务院批准由山东省政府和中石化集团合作并以齐鲁石化公司为依托基地建立起来的[齐鲁化学工业园区是继上海南京之后的全国第三大化学工业园区](../Page/齐鲁化学工业园区.md "wikilink")。华能辛店电厂是一座装机容量达百万千瓦的大型发电企业。此外还有建材、纺织、[机电等工业门类](../Page/机电.md "wikilink")。农业盛产[小麦](../Page/小麦.md "wikilink")、[玉米](../Page/玉米.md "wikilink")、[棉花等](../Page/棉花.md "wikilink")，有“鲁中粮仓”之称。

## 物产

  - 特产[青州花边大套](../Page/青州花边大套.md "wikilink")，系用抽纱手工织成，颇具盛名。
  - [西红柿](../Page/西红柿.md "wikilink")，临淄还具有“西红柿之乡”之美称。

## 文化和旅游

[Linzi_Road_signs.JPG](https://zh.wikipedia.org/wiki/File:Linzi_Road_signs.JPG "fig:Linzi_Road_signs.JPG")

  - 2004年7月15日，经[国际足联认定](../Page/国际足联.md "wikilink")，世界[足球起源于中国山东淄博临淄](../Page/足球.md "wikilink")。早在2300多年前[蹴鞠就已诞生并流行于中国临淄](../Page/蹴鞠.md "wikilink")，古代的蹴鞠就是足球的前身。
  - [国家历史文化名城](../Page/国家历史文化名城.md "wikilink")：临淄
  - [全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")：[临淄齐国故城](../Page/临淄齐国故城.md "wikilink")
    [田齐王陵](../Page/田齐王陵.md "wikilink")
    [桐林遗址](../Page/桐林遗址.md "wikilink")
  - [殉马坑](../Page/殉马坑.md "wikilink")
    [中国古车博物馆](../Page/中国古车博物馆.md "wikilink")
    [齐国故城博物馆](../Page/齐国故城博物馆.md "wikilink")

## 外部链接

  - [临淄区政府网站](http://www.linzi.gov.cn/)

[鲁](../Category/国家历史文化名城.md "wikilink")
[临淄区](../Category/临淄区.md "wikilink")
[区](../Category/淄博区县.md "wikilink")
[淄博](../Category/山东市辖区.md "wikilink")