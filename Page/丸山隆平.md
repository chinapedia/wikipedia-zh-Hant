**丸山隆平**，於[京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[右京區出身](../Page/右京區.md "wikilink")，是日本[傑尼斯事務所旗下偶像團體](../Page/傑尼斯事務所.md "wikilink")「[關西傑尼斯8](../Page/關西傑尼斯8.md "wikilink")」成員之一，團體以樂團形式演出時負責演奏[貝斯](../Page/貝斯.md "wikilink")。

## 基本資料

  - [日本歌手](../Page/日本.md "wikilink")、演員，A型。
  - 身高：175cm
  - 入社時間：1997年9月6日，原本與[澁谷昴同為](../Page/澁谷昴.md "wikilink")1996年9月22日一起通過甄選，但似乎因為書面資料遺失，於是隔年又再度參加甄選而正式進入事務所。
  - 入社契機：父母親寄履歷到事務所
  - 關8中的代表色：橘色

## 個人簡介

  - 羅馬音：MARUYAMA RYUHEI
  - 暱稱：MARU
  - 為家裡的長男，有一個弟弟、一個妹妹
  - 雙親皆為[健美教練](../Page/健美.md "wikilink")(曾說過因為雙親體格健壯，所以不曾有過叛逆期)
  - 與同為關8成員的[安田章大](../Page/安田章大.md "wikilink")、[澁谷昴](../Page/澁谷昴.md "wikilink")、[大倉忠義組成](../Page/大倉忠義.md "wikilink")「すばるBAND」
  - 與同為關8成員的[安田章大組成名為](../Page/安田章大.md "wikilink")「山田」的相聲組合
  - 成在節目中與[安田章大](../Page/安田章大.md "wikilink")、[大倉忠義組成](../Page/大倉忠義.md "wikilink")「關ジミ3」，後來被[澁谷昴更名為](../Page/澁谷昴.md "wikilink")「大山田」
  - 尊敬的事務所前輩：[二宮和也](../Page/二宮和也.md "wikilink")([嵐](../Page/嵐.md "wikilink"))、[草彅剛](../Page/草彅剛.md "wikilink")（[SMAP](../Page/SMAP.md "wikilink")）
  - 演唱會上的口頭禪是「PANG！！」，發明多種口頭禪在演唱會與節目中使用，增加與歌迷間的互動，如:「PA---SUNG！！」與「TEKISA---SU」等，其中最常使用的仍為「PANG！！」。
  - 有[懼高症](../Page/懼高症.md "wikilink")(2009年1月31日所撥出的Can\!ジャニ節目『尋找最令人想驚聲尖叫的遊樂設施』一集，與[横山裕一同出外景時曾嘗試](../Page/横山裕.md "wikilink")[高空彈跳](../Page/高空彈跳.md "wikilink"))
  - 中學時代參與田徑社，擅長[馬拉松](../Page/馬拉松.md "wikilink")，在[Johnny's
    Jr.時期有過夏威夷路跑第一名的紀錄](../Page/Johnny's_Jr..md "wikilink")(21.0975KM、1小時28分57秒)，不擅長球類運動。
  - 雖然有駕照，但不擅長開車。
  - 喜歡年長的女性。
  - 崇拜日本怪談演說者[稲川淳二](../Page/稲川淳二.md "wikilink")。
  - 喜歡的音樂人有[東京事變和](../Page/東京事變.md "wikilink")[菅止戈男](../Page/菅止戈男.md "wikilink")。

## 参與團體

  - KYOTO大原村
  - B.I.G. West
      - 團員：[錦戶亮](../Page/錦戶亮.md "wikilink")、[安田章大](../Page/安田章大.md "wikilink")、**丸山隆平**
  - J2000
  - V.WEST
  - ヤンジャニ
  - 関ジャニ∞
  - 年男ユニット

## 參與演出

※僅列出個人活動部分，團體共同參與的演出請參照[關8相關欄目](../Page/關西傑尼斯8.md "wikilink")。

### 綜藝

  - 週刊V.WEST（[關西電視台](../Page/關西電視台.md "wikilink")、[BS富士](../Page/BS富士.md "wikilink")）2001年4月～2002年9月結束播放
  - 痛快！EVERYDAY（[關西電視台](../Page/關西電視台.md "wikilink")）2007年4月～2008年6月結束播放
  - [ありえへん∞世界](../Page/ありえへん∞世界.md "wikilink")（[東京電視台](../Page/東京電視台.md "wikilink")）2008年4月15日～
  - 愛の修羅バラ\!（[關西電視台](../Page/關西電視台.md "wikilink")、中京電視台）2009年4月5日～2010年12月26日結束播放
  - [サタデープラス](../Page/サタデープラス.md "wikilink")（[MBS](../Page/MBS.md "wikilink")）2015年4月4日～

### 電視劇

  - 七人のサムライ
    J家の反乱／城之内隆平　1999年4月9日～2000年9月30日（[朝日放送系列](../Page/朝日放送.md "wikilink")）
  - 盜國物語／森蘭丸 2005年1月2日（[東京電視台系列](../Page/東京電視台.md "wikilink")）
  - 名探偵キャサリンVS十津川警部 京右禅の謎～華の密室殺人事件／土田四郎
    2006年8月7日（[TBS](../Page/TBS.md "wikilink")）
  - 自行車少年記／草太 2006年11月23日（[愛知電視台](../Page/愛知電視台.md "wikilink")）
  - 関ジャニ∞青春drama serise「Double」（複体）／丸山
    2006年12月28日（[關西電視台](../Page/關西電視台.md "wikilink")）
  - 輪違屋糸里～女たちの新撰組／沖田總司 2007年9月9日、10日（[TBS系](../Page/TBS.md "wikilink")）
  - [音樂孩子王](../Page/音樂孩子王.md "wikilink")／斉藤守
    2009年1月16日～3月13日（[朝日放送系列](../Page/朝日放送.md "wikilink")）
  - 0号室の客
    第2話／五十嵐修　2009年11月20日～12月4日（[富士電視台](../Page/富士電視台.md "wikilink")）
  - [飛特族、買個家。](../Page/飛特族、買個家。.md "wikilink")／豊川哲平　2010年10月19日～12月21日（[富士電視台](../Page/富士電視台.md "wikilink")）
  - 24時間テレビスペシャルドラマ『生きてるだけでなんくるないさ』／岡島健太郎　2011年8月20日（[日本電視台](../Page/日本電視台.md "wikilink")）
  - [草莓之夜](../Page/草莓之夜.md "wikilink")／湯田康平　2012年1月10日～3月20日（[富士電視台](../Page/富士電視台.md "wikilink")）
  - 4夜連続ドラマ『O-PARTS
    〜オーパーツ〜』／柿沢雄一（主演)　2012年2月27日～3月1日（[富士電視台](../Page/富士電視台.md "wikilink")）
  - [工作大未來—從13歲開始迎向世界](../Page/工作大未來—從13歲開始迎向世界.md "wikilink") 最終話／渡辺大紀
    2012年3月9日（[朝日電視台](../Page/朝日電視台.md "wikilink")）
  - [敏行快跑](../Page/敏行快跑.md "wikilink")／田西敏行（主演）
    2012年7月6日～9月7日（[朝日電視台](../Page/朝日電視台.md "wikilink")）
  - [不准哭，小原](../Page/不准哭，小原.md "wikilink")／田中君
    2013年1月19日～3月23日（[日本電視台](../Page/日本電視台.md "wikilink")）
  - [世界奇妙物語](../Page/世界奇妙物語.md "wikilink")2013春季特別篇-石油が出た／植田泰史
    2013年5月11日（[富士電視台](../Page/富士電視台.md "wikilink")）
  - [靈異教師神眉](../Page/靈異教師神眉.md "wikilink")／鵺野鳴介（神眉）
    2014年10月11日\~2014年12月13日（[日本電視台](../Page/日本電視台.md "wikilink")）
  - [誘拐法庭\~7DAYS\~](../Page/誘拐法庭~7DAYS~.md "wikilink")／宇津井秀樹
    2018年10月7日（[朝日電視台](../Page/朝日電視台.md "wikilink")）
  - よつば銀行 原島浩美がモノ申す\!〜この女に賭けろ〜／加東亜希彦
    2019年1月21日（[東京電視台系列](../Page/東京電視台.md "wikilink")）

### 電影

  - [ワイルド7](../Page/ワイルド7.md "wikilink")／パイロウ（[華納兄弟電影公司](../Page/華納兄弟.md "wikilink")，[羽住英一郎導演](../Page/羽住英一郎.md "wikilink")）　2011年12月21日(日本上映)
  - [關八戰隊](../Page/關八戰隊.md "wikilink")／主演‧丸ノ内正悟(Orange)（[東寶](../Page/東寶.md "wikilink")，[堤幸彥導演](../Page/堤幸彥.md "wikilink")）
    2012年7月28日上映
  - [殺人草莓夜](../Page/殺人草莓夜_\(電影\).md "wikilink")／湯田康平（[東寶](../Page/東寶.md "wikilink")，[佐藤祐市導演](../Page/佐藤祐市.md "wikilink")）
    2013年1月26日（日本上映）
  - [円卓](../Page/円卓.md "wikilink")／ジビキ先生（[東寶](../Page/東寶.md "wikilink")，[行定勲導演](../Page/行定勲.md "wikilink")）　2014年6月（日本上映）
  - [泥棒役者](../Page/泥棒役者.md "wikilink")／大貫はじめ（[西田征史導演](../Page/西田征史.md "wikilink")）　2017年11月18日（日本上映）

### 廣播

  - 聞くジャニ∞（MBS RADIO）（2004年10月2日\~2009年4月4日結束播放）※不定期出演
  - 村上信五の週刊関ジャニ通信（ABC RADIO）（2004年10月\~）※不定期出演
  - 関ジャニ∞　村上信五と丸山隆平のレコメン！（文化放送）（2013年4月4日 -2016年3月24日)固定主持人

### CM

  - 焙りコーン（HOUSE食品）（2002年）
    當時的[V.WEST成員共同出演](../Page/V.WEST.md "wikilink")
  - モンスターハンターポータブル3rd（CAPCOM）（2011年）與[横山裕](../Page/横山裕.md "wikilink"),[村上信五](../Page/村上信五.md "wikilink"),[澀谷昴四人共同出演](../Page/澀谷昴.md "wikilink")
  - ICE BOX「熱くなった後に篇」（森永製菓）（2013年4月29日 -
    ）與[安田章大](../Page/安田章大.md "wikilink"),[錦戸亮三人共同出演](../Page/錦戸亮.md "wikilink")

### 舞台劇

  - 1998年4月18日～7月12日「KYO TO KYO」春公演
  - 1998年7月18日～8月31日「KYO TO KYO」サマーフェスティバル
  - 1998年9月6日～11月29日「KYO TO KYO」秋公演
  - 2011年1月16日～2月23日「ギルバート・グレイプ」(原著:[What's Eating Gilbert
    Grape](http://en.wikipedia.org/wiki/What%27s_Eating_Gilbert_Grape))
  - 2012年4月27日～5月29日「BOB（ボブ）」
  - 2016年6月26日～7月25日「[馬克白](../Page/馬克白.md "wikilink")（[マクベス](../Page/マクベス.md "wikilink")）」-
    主演（[東京地球座](../Page/東京地球座.md "wikilink")）

### 演唱會

  - 1998年12月27日～ 1999年 1月 6日Johnny's Jr.「Johnny's Winter Concert」橫濱・大阪
  - 1999年 5月 2日～ 6月20日Johnny's Jr.「Fresh Spring Concert '99」橫濱・大阪
  - 1999年 8月13日～15日関西Johnny's Jr.「FIRST LIVE」Zeep大阪
  - 1999年10月 9日Johnny's Jr.「特急投球コンサート」（東京巨蛋）
  - 1999年12月20日～ 25日関西Johnny's Jr.「X'mas present」（大阪松竹座）
  - 2006年12月18日～ 20日「関ジャニ∞-丸山隆平 Solo　Concert　2006冬」

## 影音作品

※團體共同參與的作品請參照[關8相關欄目](../Page/關西傑尼斯8.md "wikilink")。

### DVD

  - 2005年5月27日　国盗り物語(ビクターエンタテインメント)
  - 2007年6月22日　自転車少年記(東宝)
  - 2009年7月24日　歌のおにいさん(メディアファクトリー)
  - 2010年3月10日　0号室の客 DVD-BOX1(TCエンタテインメント)
  - 2011年3月16日　フリーター、家を買う。(ポニーキャニオン)
  - 2011年10月26日　24 HOUR TELEVISION スペシャルドラマ2011「生きてるだけで なんくるないさ」(バップ)
  - 2012年5月23日　ワイルド7(ワーナー・ホーム・ビデオ)
  - 2012年6月29日　ストロベリーナイト シーズン1(ポニーキャニオン)
  - 2012年7月25日　13歳のハローワーク(メディアファクトリー)
  - 2012年9月19日　O-PARTS \~オーパーツ\~(ポニーキャニオン)
  - 2012年12月19日　ボーイズ・オン・ザ・ラン(ポニーキャニオン)
  - 2013年1月25日　エイトレンジャー(東宝)
  - 2013年7月24日　泣くな、はらちゃん(バップ)

## 外部連結

  - [INFINITY RECORDS](http://www.infinity-r.jp/)
  - [Johnnys官網](http://www.johnnys-net.jp/)

[Category:日本男歌手](../Category/日本男歌手.md "wikilink")
[Category:日本男演員](../Category/日本男演員.md "wikilink")
[Category:京都府出身人物](../Category/京都府出身人物.md "wikilink")
[Category:日本男性偶像](../Category/日本男性偶像.md "wikilink")
[Category:關西傑尼斯8](../Category/關西傑尼斯8.md "wikilink")
[Category:傑尼斯事務所所屬藝人](../Category/傑尼斯事務所所屬藝人.md "wikilink")
[Category:TVnavi日劇年度大賞新人獎](../Category/TVnavi日劇年度大賞新人獎.md "wikilink")