**丹吉尔**（[柏柏爾語](../Page/柏柏爾語.md "wikilink")：；[阿拉伯語](../Page/阿拉伯語.md "wikilink")：****；轉寫：**''Tanja**''；[西班牙語](../Page/西班牙語.md "wikilink")：；[法語](../Page/法語.md "wikilink")：)，又譯**坦幾亞**，是[北非國家](../Page/北非.md "wikilink")[摩洛哥北部的一個濱海城市](../Page/摩洛哥.md "wikilink")，在[直布羅陀海峽西面的入口](../Page/直布羅陀海峽.md "wikilink")，與[大西洋及](../Page/大西洋.md "wikilink")[地中海的交界](../Page/地中海.md "wikilink")。有定期渡輪連接[西班牙本土最南端的城市](../Page/西班牙.md "wikilink")[塔里法](../Page/塔里法.md "wikilink")（）。

## 历史

丹吉尔是一個歷史名城，早在前6世纪，[腓尼基人稱雄於地中海時](../Page/腓尼基人.md "wikilink")，就已在當地建立殖民地。由於丹吉尔位處於地中海的出口，所以一直以來都是兵家必爭之地。從15世紀開始，坦幾亞一直都是[西班牙和](../Page/西班牙.md "wikilink")[葡萄牙兩個爭奪的地方](../Page/葡萄牙.md "wikilink")；[第二次世界大战结束](../Page/第二次世界大战.md "wikilink")、摩洛哥獨立之後，1956年摩洛哥收回行政权；1960年完全恢复主权；1962年起成为自由港，并为王室避暑地；1965年建立自由贸易区。

## 交通

### 往来交通

有渡輪前往[西班牙](../Page/西班牙.md "wikilink")[阿爾赫西拉斯市](../Page/阿爾赫西拉斯.md "wikilink")（西班牙語：Algeciras）及英屬[直布羅陀](../Page/直布羅陀.md "wikilink")，航程均約80分鐘。

## 位置

## 名人

  - [伊本·白图泰](../Page/伊本·白图泰.md "wikilink")，14世纪旅行作家。

<!-- end list -->

  - 1997年[诺贝尔物理学奖得主](../Page/诺贝尔物理学奖.md "wikilink")[克洛德·科昂-唐努德日的姓氏中的](../Page/克洛德·科昂-唐努德日.md "wikilink")“Tannoudji”与该地名有关。

## 参考资料

[Category:丹吉尔](../Category/丹吉尔.md "wikilink")
[Category:特殊政區](../Category/特殊政區.md "wikilink")