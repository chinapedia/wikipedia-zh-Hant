**丹尼尔·卡尼曼**（，，），生於[英國托管巴勒斯坦](../Page/巴勒斯坦托管地.md "wikilink")[特拉维夫](../Page/特拉维夫.md "wikilink")，[以色列裔](../Page/以色列.md "wikilink")[美国](../Page/美国.md "wikilink")[心理学家](../Page/心理学.md "wikilink")。由于在[展望理论的贡献](../Page/展望理论.md "wikilink")，获得2002年[诺贝尔经济学奖](../Page/诺贝尔经济学奖.md "wikilink")。

## 生平

在1920年代，他的父母由[立陶宛移居到](../Page/立陶宛.md "wikilink")[法國](../Page/法國.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")。但丹尼爾·卡內曼在[特拉維夫出生](../Page/特拉維夫.md "wikilink")，當時他母親在此拜訪親戚。丹尼爾·卡內曼的童年在巴黎長大。1940年，[納粹德國佔領巴黎](../Page/納粹德國.md "wikilink")，其父親是第一批被挑選出來集中的[法國猶太人](../Page/法國猶太人.md "wikilink")，但因其父親雇主的介入幫忙，在六週後，其父親幸運的被釋放。其家庭在接下來的戰爭時期，都沒有被官方逮捕，生活正常。其父親在1944年因[糖尿病過世](../Page/糖尿病.md "wikilink")。1948年，其家庭移居至英國托管下的巴勒斯坦，正好是在以色列建國前夕。

1954年，丹尼爾·卡內曼在[耶路撒冷希伯來大學取得學士學位](../Page/耶路撒冷希伯來大學.md "wikilink")，主修心理學，副修數學。畢業後，他進入[以色列國防軍的心理學部門工作](../Page/以色列國防軍.md "wikilink")。他負責是評估預備進入軍官訓練班的申請者，其心理素質是否符合要求，並且發展出一套用來評估申請者的心理測試。

他於1958年前往美國[柏克萊加州大學](../Page/柏克萊加州大學.md "wikilink")，攻讀心理學博士。取得博士學位後，1961年回到[耶路撒冷希伯來大學擔任講師](../Page/耶路撒冷希伯來大學.md "wikilink")。1966年升任資深講師。在這個時期，他主要研究視覺心理學。除了在耶路撒冷希伯來大學的教職外，他曾訪問英美多所大學，如1965年到1966年，在[密西根大學任訪問學者](../Page/密西根大學.md "wikilink")；1966年到1967年，在[哈佛大学認知心理學中心](../Page/哈佛大学.md "wikilink")，擔任講師；1967年到1968年，至[劍橋大學應用心理學研究中心](../Page/劍橋大學.md "wikilink")，任訪問學者。

在希伯來大學時，卡內曼開始與[阿摩司·特沃斯基合作研究](../Page/阿摩司·特沃斯基.md "wikilink")。1977年至1978年間，他們同樣都是[史丹佛大學](../Page/史丹佛大學.md "wikilink")（Center
for Advanced Study in the Behavioral
Sciences）的研究員之一。在史丹佛大學時，經濟學家[理查德·塞勒當時也在此研究](../Page/理查德·塞勒.md "wikilink")，啟發了卡內曼與特沃斯基進入[行為經濟學領域](../Page/行為經濟學.md "wikilink")。1978年，卡內曼離開耶路撒冷希伯來大學，到加拿大[不列顛哥倫比亞大學教書](../Page/不列顛哥倫比亞大學.md "wikilink")。1979年，與[阿摩司·特沃斯基共同發表了](../Page/阿摩司·特沃斯基.md "wikilink")[展望理論](../Page/展望理論.md "wikilink")。

## 参考资料

  - [诺贝尔官方网站丹尼尔·卡内曼自传](http://nobelprize.org/nobel_prizes/economics/laureates/2002/kahneman-autobio.html)

[Category:诺贝尔经济学奖获得者](../Category/诺贝尔经济学奖获得者.md "wikilink")
[Category:猶太諾貝爾獎獲得者](../Category/猶太諾貝爾獎獲得者.md "wikilink")
[Category:美國諾貝爾獎獲得者](../Category/美國諾貝爾獎獲得者.md "wikilink")
[Category:以色列諾貝爾獎獲得者](../Category/以色列諾貝爾獎獲得者.md "wikilink")
[Category:美国文理科学院院士](../Category/美国文理科学院院士.md "wikilink")
[Category:美国心理学家](../Category/美国心理学家.md "wikilink")
[Category:以色列科學家](../Category/以色列科學家.md "wikilink")
[Category:社會心理學家](../Category/社會心理學家.md "wikilink")
[Category:猶太科學家](../Category/猶太科學家.md "wikilink")
[Category:犹太经济学家](../Category/犹太经济学家.md "wikilink")
[Category:耶路撒冷希伯来大学教师](../Category/耶路撒冷希伯来大学教师.md "wikilink")
[Category:普林斯頓大學教師](../Category/普林斯頓大學教師.md "wikilink")
[Category:加州大學柏克萊分校校友](../Category/加州大學柏克萊分校校友.md "wikilink")
[Category:耶路撒冷希伯來大學校友](../Category/耶路撒冷希伯來大學校友.md "wikilink")
[Category:立陶宛猶太人](../Category/立陶宛猶太人.md "wikilink")
[Category:移民美國的以色列人](../Category/移民美國的以色列人.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:以色列猶太人](../Category/以色列猶太人.md "wikilink")
[Category:行为科学高等研究中心学者](../Category/行为科学高等研究中心学者.md "wikilink")
[Category:特拉維夫人](../Category/特拉維夫人.md "wikilink")