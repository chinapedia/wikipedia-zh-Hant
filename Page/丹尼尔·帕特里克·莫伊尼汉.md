**丹尼尔·帕特里克·「帕特」·莫伊尼汉**（，）生於[俄克拉何马州](../Page/俄克拉何马州.md "wikilink")[塔尔萨](../Page/塔尔萨_\(俄克拉何马州\).md "wikilink")，[美国社会学家](../Page/美国.md "wikilink")、政治家，[民主党员](../Page/民主黨_\(美國\).md "wikilink")，曾任[美国驻印度大使](../Page/美国驻印度大使.md "wikilink")（1973年－1975年）、[美国驻联合国大使](../Page/美国驻联合国大使.md "wikilink")（1975年－1976年）和[美国参议员](../Page/美国参议员.md "wikilink")（1977年－2001年）。

## 外部链接

  - [AP
    obituary](http://www.cbsnews.com/stories/2003/03/27/politics/printable546301.shtml)
  - [Senator Moynihan's congressional
    biography](http://bioguide.congress.gov/scripts/biodisplay.pl?index=M001054)
  - [MoynihanStation.org](http://newpennstation.org/site)
  - [Moynihan Commission
    Report](http://www.fas.org/sgp/library/moynihan/index.html)
  - [George Will Tribute
    Column](http://www.townhall.com/columnists/GeorgeWill/2003/03/27/pat_moynihan,_rip)
  - [Moynihan Institute of Global
    Affairs](https://web.archive.org/web/20060221231144/http://www.maxwell.syr.edu/moynihan/default.html)

[Category:總統自由勳章獲得者](../Category/總統自由勳章獲得者.md "wikilink")
[Category:美国民主党联邦参议员](../Category/美国民主党联邦参议员.md "wikilink")
[Category:聯合國安全理事會主席](../Category/聯合國安全理事會主席.md "wikilink")
[Category:美國駐印度大使](../Category/美國駐印度大使.md "wikilink")
[Category:美國社會學家](../Category/美國社會學家.md "wikilink")
[Category:哈佛大學教師](../Category/哈佛大學教師.md "wikilink")
[Category:麻省理工學院教師](../Category/麻省理工學院教師.md "wikilink")
[Category:倫敦政治經濟學院校友](../Category/倫敦政治經濟學院校友.md "wikilink")
[Category:塔夫茨大學校友](../Category/塔夫茨大學校友.md "wikilink")
[Category:美國海軍軍官](../Category/美國海軍軍官.md "wikilink")
[Category:美國第二次世界大戰人物](../Category/美國第二次世界大戰人物.md "wikilink")
[Category:美國天主教徒](../Category/美國天主教徒.md "wikilink")
[Category:愛爾蘭裔美國人](../Category/愛爾蘭裔美國人.md "wikilink")
[Category:紐約市人](../Category/紐約市人.md "wikilink")
[Category:奧克拉荷馬州人](../Category/奧克拉荷馬州人.md "wikilink")
[Category:安葬於阿靈頓國家公墓者](../Category/安葬於阿靈頓國家公墓者.md "wikilink")