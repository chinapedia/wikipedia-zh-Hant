**丹江口市**是由[中華人民共和國](../Page/中華人民共和國.md "wikilink")[湖北省](../Page/湖北省.md "wikilink")[十堰市所辖的一个](../Page/十堰市.md "wikilink")[縣級市](../Page/縣級市.md "wikilink")，位於[汉江中上游](../Page/汉江.md "wikilink")，[漢水和](../Page/漢水.md "wikilink")[丹江的交會處](../Page/丹江.md "wikilink")，[丹江口水庫的大壩即位於該市](../Page/丹江口水庫.md "wikilink")。另外[道教的聖地](../Page/道教.md "wikilink")[武當山也位於丹江口市境内](../Page/武當山.md "wikilink")，有**中國水都**的外號。全市面积为3120平方公里，人口为50万人，但随着[南水北调中线移民](../Page/南水北调.md "wikilink")，人口将有变动。

## 历史

### 古代史

  - 位於[禹貢九州中的](../Page/禹貢.md "wikilink")[豫州境內](../Page/豫州.md "wikilink")。
  - [春秋属麇](../Page/春秋.md "wikilink")，鲁文公十一年（公元前616年）楚子伐麇，麇亡归楚。[战国属韩及楚](../Page/战国.md "wikilink")，称均陵。
  - [秦朝](../Page/秦朝.md "wikilink")[秦始皇二十六年](../Page/秦始皇.md "wikilink")（公元前221年）分全国为36郡，均陵归武当县，属南阳郡。
  - [汉朝汉高祖五年](../Page/汉朝.md "wikilink")（公元前202年）设武当县，因武当山得名，隶属南阳郡。东汉建安十三年（208年），属[南乡郡](../Page/南乡郡.md "wikilink")。
  - [晋朝太康十年](../Page/晋朝.md "wikilink")（公元289年）11月改南乡郡为[顺阳郡](../Page/顺阳郡.md "wikilink")，武当属[顺阳郡](../Page/顺阳郡.md "wikilink")。永嘉之乱（310年）以江左平阳郡民流寓此，增平阳县与武当同属始平郡。
  - [南北朝时期](../Page/南北朝.md "wikilink")，永初元年（公元420年），改始平郡为齐兴郡。
  - [梁朝太清元年](../Page/梁朝.md "wikilink")（公元547年）设均阳县，革兴郡为兴州，管理武当、平阳、均阳三县。
  - [西魏废帝元年](../Page/西魏.md "wikilink")（公元552年）省平阳并入武当，改兴州为丰州（因丰乡城而名）。
  - [隋朝开皇三年](../Page/隋朝.md "wikilink")（公元583年）罢郡丰州。五年（585年）改丰州为均州（因均水而名）。大业初（605年）废州，设淅阳郡。义宁二年（618年）割淅阳郡置武当郡。
  - [宋朝](../Page/宋朝.md "wikilink")（公元960年）设均州武当郡，隶京西南路，领县武当、郧乡。宣和元年（1119年）升为武当军节度。
  - [元朝忽必烈十三年](../Page/元朝.md "wikilink")（1276年）南征，省武当军为均州。
  - [明朝洪武二年](../Page/明朝.md "wikilink")（1369年）废武当县入均州，洪武九年（1376年）省武当入州，直属湖广布政使司；成化十二年（1476年）改属襄阳府，此后均州无领县，由直隶州降为散州。
  - [清朝](../Page/清朝.md "wikilink")，沿袭明朝体制。

### 现代史

  - 1949年属[陕西省](../Page/陕西省.md "wikilink")[两郧专区](../Page/两郧专区.md "wikilink")。
  - 1950年属湖北省[郧阳专区](../Page/郧阳专区.md "wikilink")。
  - 1952年隶[襄阳专区行政公署](../Page/襄阳专区.md "wikilink")。
  - 1960年撤销均县并入[光化县](../Page/光化县.md "wikilink")。
  - 1962年恢复均县。
  - 1965年复归郧阳地区管辖。
  - 1983年8月19日，撤销均县，设立丹江口市（县级）。
  - 1994年郧阳地区与十堰市合并，由十堰市管辖。

## 气候

属亚热带半湿润季风气候区，由于秦岭的天然屏障和丹江口水库水全效益以及垂直地貌的影响，形成了“四季分明、光照充足、热量丰富、雨热同季，无霜期长，相对湿度大“的气候环境。

## 自然资源

  - **农作物资源**，农作物品种共有18科、49属、575个品种（系），有油类、丝类、茶类、烟类、果类、食用菌类、纤维类和漆类等160多个品种。
  - **森林资源**，共有木本种子植物79科，206属，403种。武当山山区有古树名木24科、33属、46种、435株。珍稀树种有银杏、鹅掌秋、巴山松、七叶树、铁尖杉、水杉、榔榆、白皮松、金铁松、柳杉等。林业资源处于消大于长的状态。
  - **中药材资源**，全市共有820种，分别隶属于226科。其中植物类药材723种，动、矿物类药材97种。
  - **鱼类资源**，分布的鱼类有5目10科61种。其中鲤科42种，占71%。经济鱼类有青、草、鲢、鳊、鲤、鲫、鲂、鲶等20余种；名贵鱼类有银鳔、长吻、鳗鲡、团头鲂、黄鳝
  - **稀有野生动物资源**，[二类保护动物林麝](../Page/二类保护动物.md "wikilink")（香獐子）、鸳鸯、大鲵，[三类保护动物有小灵猫](../Page/三类保护动物.md "wikilink")、大灵猫、红腹锦鸡等。野生毛皮兽类资源有狼、狐、黄鼬、狗獾、猪獾、水獭、小麂、野猪、岩松鼠等。
  - **矿产资源**，矿种共35种。其中：金属矿6种，非金属矿29种。矿种量，占全[湖北省](../Page/湖北省.md "wikilink")110种的31.8%。已探明有储量的19种，占全省77种的24.7%。

## 风景名胜

  - [武当山](../Page/武当山.md "wikilink")
  - [牛河森林公园](../Page/牛河森林公园.md "wikilink")

## 著名人物

  - [陈世美](../Page/陈世美.md "wikilink")

## 参看

  - [均州](../Page/均州.md "wikilink")
  - [丹江口水利枢纽工程](../Page/丹江口水利枢纽工程.md "wikilink")

## 参考资料

  - [中国水都调水源头
    丹江口市](https://web.archive.org/web/20160304115311/http://sybm.shiyan.gov.cn/wszjsy/ShowArticle.asp?ArticleID=240)

[丹江口市](../Category/丹江口市.md "wikilink")
[市](../Category/十堰区县市.md "wikilink")
[十堰市](../Category/湖北县级市.md "wikilink")
[鄂](../Category/国家级贫困县.md "wikilink")