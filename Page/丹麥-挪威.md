**丹麦-挪威联合王国**（[丹麥文](../Page/丹麥文.md "wikilink")／[瑞典文](../Page/瑞典文.md "wikilink")：Danmark-Norge），1524年－1814年，[北欧国家](../Page/北欧.md "wikilink")，由原[卡尔马联合](../Page/卡尔马联合.md "wikilink")（1397年－1523年）演变而来。1523年[瑞典王国独立后](../Page/瑞典王国.md "wikilink")，[挪威继续与丹麦联合](../Page/挪威.md "wikilink")。此後数百年间，[丹麦和](../Page/丹麦.md "wikilink")[瑞典不断争雄](../Page/瑞典.md "wikilink")，[挪威长期作为](../Page/挪威.md "wikilink")[丹麦牵制](../Page/丹麦.md "wikilink")[瑞典北方的基地而存在](../Page/瑞典.md "wikilink")。

1658年与1660年之间，[瑞典王国一度冲破](../Page/瑞典王国.md "wikilink")[丹麦的封锁](../Page/丹麦.md "wikilink")，占领[挪威中部的](../Page/挪威.md "wikilink")[特隆赫姆港](../Page/特隆赫姆.md "wikilink")，但最终由于1659年[瑞典侵略](../Page/瑞典.md "wikilink")[丹麦失败而被后者收复](../Page/丹麦.md "wikilink")。

1801年，[英国怀疑](../Page/英国.md "wikilink")[丹麦与](../Page/丹麦.md "wikilink")[法国结盟](../Page/法国.md "wikilink")，故用军舰轰击[哥本哈根](../Page/哥本哈根.md "wikilink")，且封锁[挪威](../Page/挪威.md "wikilink")，造成其境内大饥荒。

1814年，原[法国皇帝](../Page/法国皇帝.md "wikilink")[拿破仑一世麾下大将](../Page/拿破仑一世.md "wikilink")，[瑞典](../Page/瑞典.md "wikilink")[王储](../Page/王储.md "wikilink")[卡尔·约翰](../Page/卡尔·约翰.md "wikilink")（后来的[卡尔十四世](../Page/卡尔十四世.md "wikilink")）进攻[丹麦](../Page/丹麦.md "wikilink")，迫使之于1月14日签订《[基尔条约](../Page/基尔条约.md "wikilink")》，割让[挪威](../Page/挪威.md "wikilink")。5月17日，[丹麦](../Page/丹麦.md "wikilink")[王储](../Page/王储.md "wikilink")——[挪威总督](../Page/挪威君主.md "wikilink")[克里斯蒂安·弗雷德里克](../Page/克里斯蒂安·弗雷德里克.md "wikilink")（后来的克里斯蒂安八世）宣布[挪威独立](../Page/挪威.md "wikilink")，卡尔·约翰前往征讨，最终于8月14日签订[莫斯和约](../Page/挪威歷史.md "wikilink")，将[挪威并入](../Page/挪威.md "wikilink")[瑞典](../Page/瑞典.md "wikilink")。丹麦－挪威联合体终结。1815年[维也纳和会上](../Page/维也纳和会.md "wikilink")，列强承认[挪威是](../Page/挪威.md "wikilink")[瑞典的领土](../Page/瑞典.md "wikilink")。
[Våbenskjold_Rosenborg.jpg](https://zh.wikipedia.org/wiki/File:Våbenskjold_Rosenborg.jpg "fig:Våbenskjold_Rosenborg.jpg")

[\*](../Category/丹麦-挪威.md "wikilink")
[Category:欧洲历史上的王国](../Category/欧洲历史上的王国.md "wikilink")
[Category:欧洲历史上的共主邦联](../Category/欧洲历史上的共主邦联.md "wikilink")