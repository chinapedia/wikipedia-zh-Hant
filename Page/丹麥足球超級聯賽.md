{{ 足球聯賽資料 | name = 丹麥足球超級聯賽 | another_name = Danish Superliga | image =
Danish Superliga 2010.svg | pixels = 175px | country =  | confed =
[UEFA](../Page/歐洲足球協會聯盟.md "wikilink")（[歐洲](../Page/歐洲.md "wikilink")） |
founded = 1991年 | teams = 14 隊 | relegation =
[丹麥足球甲級聯賽](../Page/丹麥足球甲級聯賽.md "wikilink")
| level = 第 1 級 | domestic_cup = [丹麥盃](../Page/丹麥盃.md "wikilink") |
international_cup = [歐洲聯賽冠軍盃](../Page/歐洲聯賽冠軍盃.md "wikilink")
[歐霸盃](../Page/歐足聯歐洲聯賽.md "wikilink") | season = 2017–18 | champions =
[米迪蘭特](../Page/中日德蘭足球俱樂部.md "wikilink") | most_successful_club =
[哥本哈根](../Page/哥本哈根足球會.md "wikilink") | no_of_titles = 12 次 |
website = [丹超官網](http://www.superliga.dk/) | current = }}

**丹麥足球超級聯賽**（[丹麥語](../Page/丹麥語.md "wikilink")：**Superligaen**，獲冠名[贊助稱為](../Page/贊助.md "wikilink")**[SAS](../Page/北歐航空.md "wikilink")
Ligaen**），通常简称“**丹超**”，是由[丹麥足球協會管理](../Page/丹麥足球協會.md "wikilink")，丹麥職業[足球界最高組別聯賽](../Page/足球.md "wikilink")。丹超聯賽隊伍共有
14 支。超級聯賽始於 1991–92 年球季。原本是於1946年成立的丹麥甲組足球聯賽，至1991年聯賽規格升級，變成現在的丹麥超級足球聯賽。

倘以1991年起計，贏得較多丹超聯賽冠軍較多的，數以[哥本哈根和](../Page/哥本哈根足球會.md "wikilink")[邦比為首](../Page/邦比足球會.md "wikilink")，分別奪得了
12 次和 6 次冠軍。

## 參賽球隊

2018–19年丹麥足球超級聯賽參賽隊伍共有 14 支。

| 中文名稱                                     | 英文名稱                     | 所在城市                                  | 上季成績     |
| ---------------------------------------- | ------------------------ | ------------------------------------- | -------- |
| [米迪蘭特](../Page/中日德蘭足球俱樂部.md "wikilink")  | FC Midtjylland           | [海寧](../Page/海寧_\(丹麥\).md "wikilink") | 第 1 位    |
| [邦比](../Page/邦比足球會.md "wikilink")        | Brøndby IF               | [邦比](../Page/邦比自治市.md "wikilink")     | 第 2 位    |
| [洛斯查蘭特](../Page/北西蘭足球俱樂部.md "wikilink")  | FC Nordsjælland          | [法魯姆](../Page/法魯姆.md "wikilink")      | 第 3 位    |
| [哥本哈根](../Page/哥本哈根足球會.md "wikilink")    | FC Copenhagen            | [哥本哈根](../Page/哥本哈根.md "wikilink")    | 第 4 位    |
| [阿爾堡](../Page/阿爾堡足球會.md "wikilink")      | AaB Fodbold              | [奧爾堡](../Page/奧爾堡.md "wikilink")      | 第 5 位    |
| [賀森斯](../Page/霍爾森斯聯合俱樂部.md "wikilink")   | AC Horsens               | [霍爾森斯](../Page/霍爾森斯.md "wikilink")    | 第 6 位    |
| [霍布羅](../Page/霍布羅體育會.md "wikilink")      | Hobro IK                 | [霍布羅](../Page/霍布羅.md "wikilink")      | 第 7 位    |
| [桑德捷斯基](../Page/桑德捷斯基足球會.md "wikilink")  | SønderjyskE              | [夏達斯利夫](../Page/夏達斯利夫.md "wikilink")  | 第 8 位    |
| [奧丹斯](../Page/歐登塞足球俱樂部.md "wikilink")    | Odense Boldklub          | [歐登塞](../Page/歐登塞.md "wikilink")      | 第 9 位    |
| [阿曉斯](../Page/奧胡斯體操協會俱樂部.md "wikilink")  | Aarhus Gymnastikforening | [奧胡斯](../Page/奧胡斯.md "wikilink")      | 第 10 位   |
| [蘭達斯](../Page/蘭訥斯足球俱樂部.md "wikilink")    | Randers FC               | [蘭訥斯](../Page/蘭訥斯.md "wikilink")      | 第 13 位   |
| [維積利](../Page/維積利足球會.md "wikilink")      | Vejle Boldklub           | [瓦埃勒](../Page/瓦埃勒.md "wikilink")      | 甲組，第 1 位 |
| [艾斯堡](../Page/埃斯比約聯合足球俱樂部.md "wikilink") | Esbjerg fB               | [埃斯比約](../Page/埃斯比約.md "wikilink")    | 甲組，第 2 位 |
| [雲德斯素](../Page/雲德斯素聯足球會.md "wikilink")   | Vendsyssel FF            | [文敘瑟爾](../Page/文敘瑟爾.md "wikilink")    | 甲組，第 3 位 |

## 歷屆冠軍

以下為歷屆聯賽冠軍：\[1\]

## 球會奪冠次數

<table>
<thead>
<tr class="header">
<th><p>球會</p></th>
<th><p>冠軍次數</p></th>
<th><p>冠軍年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/哥本哈根足球會.md" title="wikilink">哥本哈根</a></p></td>
<td><center>
<p>12</p></td>
<td><p>1993, 2001, 2003, 2004, 2006, 2007, 2009, 2010, 2011, 2013, 2016, 2017</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/邦比足球會.md" title="wikilink">邦比</a></p></td>
<td><center>
<p>6</p></td>
<td><p>1991, 1996, 1997, 1998, 2002, 2005</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿爾堡足球會.md" title="wikilink">阿爾堡</a></p></td>
<td><center>
<p>4</p></td>
<td><p>1995, 1999, 2008, 2014</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中日德蘭足球俱樂部.md" title="wikilink">米迪蘭特</a></p></td>
<td><center>
<p>2</p></td>
<td><p>2015, 2018</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林比足球俱樂部.md" title="wikilink">寧比</a></p></td>
<td><center>
<p>1</p></td>
<td><p>1992</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/錫爾克堡體育協會.md" title="wikilink">施克堡</a></p></td>
<td><center>
<p>1</p></td>
<td><p>1994</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/赫福爾格足球會.md" title="wikilink">赫福爾格</a></p></td>
<td><center>
<p>1</p></td>
<td><p>2000</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/北西蘭足球俱樂部.md" title="wikilink">洛斯查蘭特</a></p></td>
<td><center>
<p>1</p></td>
<td><p>2012</p></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [丹麥足協](http://www.dbu.dk)

[丹麥足球超級聯賽](../Category/丹麥足球超級聯賽.md "wikilink")

1.