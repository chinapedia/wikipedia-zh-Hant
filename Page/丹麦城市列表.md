以下为主要**丹麥城市**列表：
[Denmark-CIA_WFB_Map.png](https://zh.wikipedia.org/wiki/File:Denmark-CIA_WFB_Map.png "fig:Denmark-CIA_WFB_Map.png")
[Kopenhagen_Innenstadt.JPG](https://zh.wikipedia.org/wiki/File:Kopenhagen_Innenstadt.JPG "fig:Kopenhagen_Innenstadt.JPG")首都[哥本哈根](../Page/哥本哈根.md "wikilink")\]\]
[Århus_city_trafikhavn.jpg](https://zh.wikipedia.org/wiki/File:Århus_city_trafikhavn.jpg "fig:Århus_city_trafikhavn.jpg")\]\]
[Odense_Inner_Harbour-flats.jpg](https://zh.wikipedia.org/wiki/File:Odense_Inner_Harbour-flats.jpg "fig:Odense_Inner_Harbour-flats.jpg")\]\]
[Udsigt_over_Aalborg_fra_Skovbakken.jpg](https://zh.wikipedia.org/wiki/File:Udsigt_over_Aalborg_fra_Skovbakken.jpg "fig:Udsigt_over_Aalborg_fra_Skovbakken.jpg")\]\]

## 列表

### 按字母排列

  - [阿灵厄](../Page/阿灵厄.md "wikilink")（丹麦语：Allinge）
  - [Ans](../Page/Ans.md "wikilink")
  - [Assens](../Page/Assens.md "wikilink")
  - [Augustenborg](../Page/Augustenborg.md "wikilink")
  - [Beder](../Page/Beder.md "wikilink")
  - [比隆](../Page/比隆_\(丹麦\).md "wikilink")
  - [Birkerød](../Page/Birkerød.md "wikilink")
  - [Bjerringbro](../Page/Bjerringbro.md "wikilink")
  - [Blenstrup](../Page/Blenstrup.md "wikilink")
  - [Bogense](../Page/Bogense.md "wikilink")
  - [Brande](../Page/Brande.md "wikilink")
  - [Brønderslev](../Page/Brønderslev.md "wikilink")
  - [Christiansfeld](../Page/Christiansfeld.md "wikilink")
  - [哥本哈根](../Page/哥本哈根.md "wikilink")（København）
  - [Dragør](../Page/Dragør.md "wikilink")
  - [Ebeltoft](../Page/Ebeltoft.md "wikilink")
  - [赫尔辛格](../Page/赫尔辛格.md "wikilink")（Helsingør）
  - [埃斯比約](../Page/埃斯比約.md "wikilink")（Esbjerg）
  - [Espergærde](../Page/Espergærde.md "wikilink")
  - [Fakse](../Page/Fakse.md "wikilink")
  - [Farum](../Page/Farum.md "wikilink")
  - [Fjerritslev](../Page/Fjerritslev.md "wikilink")
  - [Fredensborg](../Page/Fredensborg.md "wikilink")
  - [弗雷德里西亚](../Page/弗雷德里西亚.md "wikilink")
  - [腓特烈斯贝](../Page/腓特烈斯贝.md "wikilink")
  - [腓特烈港](../Page/腓特烈港.md "wikilink")
  - [Frederikssund](../Page/Frederikssund.md "wikilink")
  - [Frederiksværk](../Page/Frederiksværk.md "wikilink")
  - [Fåborg](../Page/Fåborg.md "wikilink")
  - [Gilleleje](../Page/Gilleleje.md "wikilink")
  - [Grenå](../Page/Grenå.md "wikilink")
  - [Greve Strand](../Page/Greve_Strand.md "wikilink")
  - [Grindsted](../Page/Grindsted.md "wikilink")
  - [Gråsten](../Page/Gråsten.md "wikilink")
  - [Gudhjem](../Page/Gudhjem.md "wikilink")
  - [Gesten](../Page/Gesten.md "wikilink")
  - [Haderslev](../Page/Haderslev.md "wikilink")
  - [Hadsten](../Page/Hadsten.md "wikilink")
  - [Hadsund](../Page/Hadsund.md "wikilink")
  - [Haslev](../Page/Haslev.md "wikilink")
  - [Hedensted](../Page/Hedensted.md "wikilink")
  - [Helsinge](../Page/Helsinge.md "wikilink")
  - [海宁](../Page/海宁.md "wikilink")
  - [Hillerød](../Page/Hillerød.md "wikilink")
  - [Hinnerup](../Page/Hinnerup.md "wikilink")
  - [Hirtshals](../Page/Hirtshals.md "wikilink")
  - [Hjarup](../Page/Hjarup.md "wikilink")
  - [Hjørring](../Page/Hjørring.md "wikilink")
  - [Hobro](../Page/Hobro.md "wikilink")
  - [Holbæk](../Page/Holbæk.md "wikilink")
  - [Holstebro](../Page/Holstebro.md "wikilink")
  - [Holte](../Page/Holte.md "wikilink")
  - [霍尔森斯](../Page/霍尔森斯.md "wikilink")
  - [Hvalsø](../Page/Hvalsø.md "wikilink")
  - [Hvidovre](../Page/Hvidovre.md "wikilink")
  - [Høje Tåstrup](../Page/Høje-Tåstrup.md "wikilink")
  - [霍斯霍姆](../Page/霍斯霍姆.md "wikilink")
  - [Ikast](../Page/Ikast.md "wikilink")
  - [Ishøj](../Page/Ishøj.md "wikilink")
  - [耶灵](../Page/耶灵.md "wikilink")
  - [Jyllinge](../Page/Jyllinge.md "wikilink")
  - [卡斯楚普](../Page/卡斯楚普.md "wikilink")
  - [Kalundborg](../Page/Kalundborg.md "wikilink")
  - [Kerteminde](../Page/Kerteminde.md "wikilink")
  - [Kjellerup](../Page/Kjellerup.md "wikilink")
  - [科灵](../Page/科灵.md "wikilink")
  - [Kongens灵比](../Page/灵比-Tårbæk.md "wikilink")
  - [Korsør](../Page/Korsør.md "wikilink")
  - [科厄](../Page/科厄.md "wikilink")
  - [Lemvig](../Page/Lemvig.md "wikilink")
  - [Lillerød](../Page/Lillerød.md "wikilink")
  - [Lystrup](../Page/Lystrup.md "wikilink")
  - [Løgstør](../Page/Løgstør.md "wikilink")
  - [Løgumkloster](../Page/Løgumkloster.md "wikilink")
  - [Løkken](../Page/Løkken.md "wikilink")
  - [Mariager](../Page/Mariager.md "wikilink")
  - [Maribo](../Page/Maribo.md "wikilink")
  - [Marstal](../Page/Marstal.md "wikilink")
  - [Middelfart](../Page/Middelfart.md "wikilink")
  - [Møldrup](../Page/Møldrup.md "wikilink")
  - [Nakskov](../Page/Nakskov.md "wikilink")
  - [Nexø](../Page/Nexø.md "wikilink")
  - [Nibe](../Page/Nibe.md "wikilink")
  - [Nivå](../Page/Nivå.md "wikilink")
  - [Nordby](../Page/Nordby.md "wikilink")
  - [新堡](../Page/新堡.md "wikilink")
  - [Nykøbing Falster](../Page/Nykøbing_Falster.md "wikilink")
  - [Nykøbing Mors](../Page/Nykøbing_Mors.md "wikilink")
  - [Nykøbing Sjælland](../Page/Nykøbing_Sjælland.md "wikilink")
  - [Næstved](../Page/Næstved.md "wikilink")
  - [Nørresundby](../Page/Nørresundby.md "wikilink")
  - [Odder](../Page/Odder.md "wikilink")
  - [欧登塞](../Page/欧登塞.md "wikilink")（Odense）
  - [Olsker](../Page/Olsker.md "wikilink")
  - [Padborg](../Page/Padborg.md "wikilink")
  - [Præstø](../Page/Præstø.md "wikilink")
  - [兰讷斯](../Page/兰讷斯.md "wikilink")
  - [Ranum](../Page/Ranum.md "wikilink")
  - [里伯](../Page/里伯.md "wikilink")
  - [Ringe](../Page/Ringe.md "wikilink")
  - [Ringkøbing](../Page/Ringkøbing.md "wikilink")
  - [Ringsted](../Page/Ringsted.md "wikilink")
  - [罗斯基勒](../Page/罗斯基勒.md "wikilink")
  - [Rudkøbing](../Page/Rudkøbing.md "wikilink")
  - [Rø](../Page/Rø.md "wikilink")
  - [Rødby](../Page/Rødby.md "wikilink")
  - [Rønne](../Page/Rønne.md "wikilink")
  - [Rørvig](../Page/Rørvig.md "wikilink")
  - [Sakskøbing](../Page/Sakskøbing.md "wikilink")
  - [Sandvig](../Page/Sandvig.md "wikilink")
  - [Silkeborg](../Page/Silkeborg.md "wikilink")
  - [Skagen](../Page/Skagen.md "wikilink")
  - [Skanderborg](../Page/Skanderborg.md "wikilink")
  - [Skive](../Page/Skive_\(丹麦\).md "wikilink")
  - [Skjern](../Page/Skjern.md "wikilink")
  - [Skælskør](../Page/Skælskør.md "wikilink")
  - [Slagelse](../Page/Slagelse.md "wikilink")
  - [Smidstrup Strand](../Page/Smidstrup_Strand.md "wikilink")
  - [Smørumnedre](../Page/Smørumnedre.md "wikilink")
  - [Solrød](../Page/Solrød.md "wikilink")
  - [Sorø](../Page/Sorø.md "wikilink")
  - [Stenløse](../Page/Stenløse.md "wikilink")
  - [Store Heddinge](../Page/Store_Heddinge.md "wikilink")
  - [Strib](../Page/Strib.md "wikilink")
  - [Struer](../Page/Struer.md "wikilink")
  - [Stubbekøbing](../Page/Stubbekøbing.md "wikilink")
  - [Svaneke](../Page/Svaneke.md "wikilink")
  - [斯汶堡](../Page/斯汶堡.md "wikilink")
  - [Sæby](../Page/Sæby.md "wikilink")
  - [森讷堡](../Page/森讷堡.md "wikilink")
  - [Thisted](../Page/Thisted.md "wikilink")
  - [Thyborøn](../Page/Thyborøn.md "wikilink")
  - [Tranbjerg](../Page/Tranbjerg.md "wikilink")
  - [Trærød](../Page/Trærød.md "wikilink")
  - [Tønder](../Page/Tønder.md "wikilink")
  - [Tårbæk](../Page/灵比-Tårbæk.md "wikilink")
  - [Tåstrup](../Page/Tåstrup.md "wikilink")
  - [Vamdrup](../Page/Vamdrup.md "wikilink")
  - [Varde](../Page/Varde.md "wikilink")
  - [Vejen](../Page/Vejen.md "wikilink")
  - [瓦埃勒](../Page/瓦埃勒.md "wikilink")
  - [维堡](../Page/维堡.md "wikilink")
  - [Virum](../Page/Virum.md "wikilink")
  - [Vojens](../Page/Vojens.md "wikilink")
  - [Vorbasse](../Page/Vorbasse.md "wikilink")
  - [Vordingborg](../Page/Vordingborg.md "wikilink")
  - [Værløse](../Page/Værløse.md "wikilink")
  - [Ærøskøbing](../Page/Ærøskøbing.md "wikilink")
  - [Åbenrå](../Page/Åbenrå.md "wikilink")
  - [奧爾堡](../Page/奧爾堡.md "wikilink")（Ålborg）
  - [Ålestrup](../Page/Ålestrup.md "wikilink")
  - [奧胡斯](../Page/奧胡斯.md "wikilink")（Århus）
  - [Års](../Page/Års.md "wikilink")
  - [Aastrup](../Page/Aastrup.md "wikilink")

### 按人口排列

[Nyhavn-panorama.jpg](https://zh.wikipedia.org/wiki/File:Nyhavn-panorama.jpg "fig:Nyhavn-panorama.jpg")\]\]
[Aarhus_Teater_(2005).jpg](https://zh.wikipedia.org/wiki/File:Aarhus_Teater_\(2005\).jpg "fig:Aarhus_Teater_(2005).jpg")\]\]
[Odense_-_Sankt_Knuds_kirke_2005-07-16.jpeg](https://zh.wikipedia.org/wiki/File:Odense_-_Sankt_Knuds_kirke_2005-07-16.jpeg "fig:Odense_-_Sankt_Knuds_kirke_2005-07-16.jpeg")\]\]
[Aalborg_NyTorv_2004_ubt.jpeg](https://zh.wikipedia.org/wiki/File:Aalborg_NyTorv_2004_ubt.jpeg "fig:Aalborg_NyTorv_2004_ubt.jpeg")\]\]
[Esbjerg_HarbourfromWatertower.jpg](https://zh.wikipedia.org/wiki/File:Esbjerg_HarbourfromWatertower.jpg "fig:Esbjerg_HarbourfromWatertower.jpg")\]\]

<table>
<caption>丹麦人口最多的城市，截至[1]<sup>1</sup></caption>
<tbody>
<tr class="odd">
<td><p><strong>#</strong></p></td>
<td style="text-align: left;"><p><strong>城市</strong></p></td>
<td><p><strong>人口2010</strong></p></td>
<td><p><strong>变化2009</strong></p></td>
<td><p><strong>人口2009</strong></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td style="text-align: left;"><p><a href="../Page/哥本哈根.md" title="wikilink">哥本哈根</a><sup>a</sup></p></td>
<td><p>528,208</p></td>
<td><p>9,634</p></td>
<td><p>518,574</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td style="text-align: left;"><p><a href="../Page/奥胡斯.md" title="wikilink">奥胡斯</a></p></td>
<td><p>242,914</p></td>
<td><p>3,049</p></td>
<td><p>239,865</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td style="text-align: left;"><p><a href="../Page/欧登塞.md" title="wikilink">欧登塞</a></p></td>
<td><p>166,305</p></td>
<td><p>7,627</p></td>
<td><p>158,678</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td style="text-align: left;"><p><a href="../Page/奥尔堡.md" title="wikilink">奥尔堡</a><sup>b</sup></p></td>
<td><p>102,312</p></td>
<td><p>815</p></td>
<td><p>101,497</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td style="text-align: left;"><p><a href="../Page/腓特烈斯贝.md" title="wikilink">腓特烈斯贝</a><sup>a</sup></p></td>
<td><p>96,718</p></td>
<td><p>1,689</p></td>
<td><p>95,029</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td style="text-align: left;"><p><a href="../Page/埃斯比约.md" title="wikilink">埃斯比约</a></p></td>
<td><p>71,459</p></td>
<td><p>434</p></td>
<td><p>71,025</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td style="text-align: left;"><p><a href="../Page/根措夫特.md" title="wikilink">根措夫特</a><sup>a</sup></p></td>
<td><p>71,052</p></td>
<td><p>1,258</p></td>
<td><p>69,794</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td style="text-align: left;"><p><a href="../Page/格拉德萨克斯.md" title="wikilink">格拉德萨克斯</a><sup>a</sup></p></td>
<td><p>64,102</p></td>
<td><p>869</p></td>
<td><p>63,233</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td style="text-align: left;"><p><a href="../Page/兰讷斯.md" title="wikilink">兰讷斯</a></p></td>
<td><p>60,227</p></td>
<td><p>385</p></td>
<td><p>59,842</p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td style="text-align: left;"><p><a href="../Page/科灵.md" title="wikilink">科灵</a></p></td>
<td><p>57,087</p></td>
<td><p>838</p></td>
<td><p>56,249</p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td style="text-align: left;"><p><a href="../Page/霍森斯.md" title="wikilink">霍森斯</a></p></td>
<td><p>52,998</p></td>
<td><p>480</p></td>
<td><p>52,518</p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td style="text-align: left;"><p><a href="../Page/灵比-托拜克.md" title="wikilink">灵比-托拜克</a><sup>a</sup></p></td>
<td><p>51,887</p></td>
<td><p>355</p></td>
<td><p>51,532</p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td style="text-align: left;"><p><a href="../Page/瓦埃勒.md" title="wikilink">瓦埃勒</a></p></td>
<td><p>50,832</p></td>
<td><p>178</p></td>
<td><p>50,654</p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td style="text-align: left;"><p><a href="../Page/Hvidovre.md" title="wikilink">Hvidovre</a><sup>a</sup></p></td>
<td><p>49,724</p></td>
<td><p>358</p></td>
<td><p>49,366</p></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td style="text-align: left;"><p><a href="../Page/罗斯基勒.md" title="wikilink">罗斯基勒</a></p></td>
<td><p>46,701</p></td>
<td><p>409</p></td>
<td><p>46,292</p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td style="text-align: left;"><p><a href="../Page/赫尔辛格.md" title="wikilink">赫尔辛格</a></p></td>
<td><p>46,125</p></td>
<td><p>97</p></td>
<td><p>46,028</p></td>
</tr>
<tr class="even">
<td><p>17</p></td>
<td style="text-align: left;"><p><a href="../Page/海宁.md" title="wikilink">海宁</a></p></td>
<td><p>45,890</p></td>
<td><p>420</p></td>
<td><p>45,470</p></td>
</tr>
<tr class="odd">
<td><p>18</p></td>
<td style="text-align: left;"><p><a href="../Page/锡尔克堡.md" title="wikilink">锡尔克堡</a></p></td>
<td><p>42,396</p></td>
<td><p>417</p></td>
<td><p> 41,979</p></td>
</tr>
<tr class="even">
<td><p>19</p></td>
<td style="text-align: left;"><p><a href="../Page/奈斯特韦兹.md" title="wikilink">奈斯特韦兹</a></p></td>
<td><p>41,729</p></td>
<td><p>12</p></td>
<td><p> 41,717</p></td>
</tr>
<tr class="odd">
<td><p>20</p></td>
<td style="text-align: left;"><p><a href="../Page/Greve_Strand.md" title="wikilink">Greve Strand</a><sup>a</sup></p></td>
<td><p>40,762</p></td>
<td><p>127</p></td>
<td><p>40,889</p></td>
</tr>
<tr class="even">
<td><p>21</p></td>
<td style="text-align: left;"><p><a href="../Page/Tårnby.md" title="wikilink">Tårnby</a><sup>a</sup></p></td>
<td><p>40,045</p></td>
<td><p>169</p></td>
<td><p>40,214</p></td>
</tr>
<tr class="odd">
<td><p>22</p></td>
<td style="text-align: left;"><p><a href="../Page/Fredericia.md" title="wikilink">Fredericia</a></p></td>
<td><p>39,513</p></td>
<td><p>29</p></td>
<td><p> 39,484</p></td>
</tr>
<tr class="even">
<td><p>23</p></td>
<td style="text-align: left;"><p><a href="../Page/Ballerup.md" title="wikilink">Ballerup</a><sup>a</sup></p></td>
<td><p>38,760</p></td>
<td><p>31</p></td>
<td><p> 38,729</p></td>
</tr>
<tr class="odd">
<td><p>24</p></td>
<td style="text-align: left;"><p><a href="../Page/Rødovre.md" title="wikilink">Rødovre</a><sup>a</sup></p></td>
<td><p>36,233</p></td>
<td><p>5</p></td>
<td><p>36,228</p></td>
</tr>
<tr class="even">
<td><p>25</p></td>
<td style="text-align: left;"><p><a href="../Page/维堡.md" title="wikilink">维堡</a></p></td>
<td><p>35,656</p></td>
<td><p>548</p></td>
<td><p> 35,108</p></td>
</tr>
<tr class="odd">
<td><p>26</p></td>
<td style="text-align: left;"><p><a href="../Page/科厄.md" title="wikilink">科厄</a></p></td>
<td><p>34,937</p></td>
<td><p>204</p></td>
<td><p>34,733</p></td>
</tr>
<tr class="even">
<td><p>27</p></td>
<td style="text-align: left;"><p><a href="../Page/Holstebro.md" title="wikilink">Holstebro</a></p></td>
<td><p>34,024</p></td>
<td><p>38</p></td>
<td><p> 34,062</p></td>
</tr>
<tr class="odd">
<td><p>28</p></td>
<td style="text-align: left;"><p><a href="../Page/邦比.md" title="wikilink">邦比</a><sup>a</sup></p></td>
<td><p>33,588</p></td>
<td><p>174</p></td>
<td><p>33,762</p></td>
</tr>
<tr class="even">
<td><p>29</p></td>
<td style="text-align: left;"><p><a href="../Page/塔斯楚普.md" title="wikilink">塔斯楚普</a><sup>a</sup></p></td>
<td><p>32,260</p></td>
<td><p>158</p></td>
<td><p> 32,102</p></td>
</tr>
<tr class="odd">
<td><p>30</p></td>
<td style="text-align: left;"><p><a href="../Page/Slagelse.md" title="wikilink">Slagelse</a></p></td>
<td><p>31,918</p></td>
<td><p>236</p></td>
<td><p> 31,682</p></td>
</tr>
<tr class="even">
<td><p>31</p></td>
<td style="text-align: left;"><p><a href="../Page/Hillerød.md" title="wikilink">Hillerød</a></p></td>
<td><p>29,951</p></td>
<td><p>268</p></td>
<td><p> 29,683</p></td>
</tr>
<tr class="odd">
<td><p>32</p></td>
<td style="text-align: left;"><p><a href="../Page/Albertslund.md" title="wikilink">Albertslund</a><sup>a</sup></p></td>
<td><p>27,457</p></td>
<td><p>249</p></td>
<td><p> 27,706</p></td>
</tr>
<tr class="even">
<td><p>33</p></td>
<td style="text-align: left;"><p><a href="../Page/Sønderborg.md" title="wikilink">Sønderborg</a></p></td>
<td><p>27,194</p></td>
<td><p>15</p></td>
<td><p> 27,179</p></td>
</tr>
<tr class="odd">
<td><p>34</p></td>
<td style="text-align: left;"><p><a href="../Page/Svendborg.md" title="wikilink">Svendborg</a></p></td>
<td><p>27,113</p></td>
<td><p>66</p></td>
<td><p> 27,179</p></td>
</tr>
<tr class="even">
<td><p>35</p></td>
<td style="text-align: left;"><p><a href="../Page/Herlev.md" title="wikilink">Herlev</a><sup>a</sup></p></td>
<td><p>26,462</p></td>
<td><p>173</p></td>
<td><p> 26,635</p></td>
</tr>
<tr class="odd">
<td><p>36</p></td>
<td style="text-align: left;"><p><a href="../Page/Holbæk.md" title="wikilink">Holbæk</a></p></td>
<td><p>27,157</p></td>
<td><p>534</p></td>
<td><p> 26,623</p></td>
</tr>
<tr class="even">
<td><p>37</p></td>
<td style="text-align: left;"><p><a href="../Page/Hjørring.md" title="wikilink">Hjørring</a></p></td>
<td><p>24,892</p></td>
<td><p>71</p></td>
<td><p>24,963</p></td>
</tr>
<tr class="odd">
<td><p>38</p></td>
<td style="text-align: left;"><p><a href="../Page/Hørsholm.md" title="wikilink">Hørsholm</a><sup>3</sup></p></td>
<td><p>23,814</p></td>
<td><p>279</p></td>
<td><p> 23,535</p></td>
</tr>
<tr class="even">
<td><p>39</p></td>
<td style="text-align: left;"><p><a href="../Page/Frederikshavn.md" title="wikilink">Frederikshavn</a></p></td>
<td><p>23,331</p></td>
<td><p>18</p></td>
<td><p>23,511</p></td>
</tr>
<tr class="odd">
<td><p>40</p></td>
<td style="text-align: left;"><p><a href="../Page/Glostrup.md" title="wikilink">Glostrup</a><sup>a</sup></p></td>
<td><p>21,296</p></td>
<td><p>New</p></td>
<td><p>New</p></td>
</tr>
<tr class="even">
<td><p>41</p></td>
<td style="text-align: left;"><p><a href="../Page/Haderslev.md" title="wikilink">Haderslev</a></p></td>
<td><p>21,293</p></td>
<td><p>142</p></td>
<td><p> 21,435</p></td>
</tr>
<tr class="odd">
<td><p>42</p></td>
<td style="text-align: left;"><p><a href="../Page/Nørresundby.md" title="wikilink">Nørresundby</a><sup>b</sup></p></td>
<td><p>21,120</p></td>
<td><p>156</p></td>
<td><p> 20,964</p></td>
</tr>
<tr class="even">
<td><p>43</p></td>
<td style="text-align: left;"><p><a href="../Page/Ringsted.md" title="wikilink">Ringsted</a></p></td>
<td><p>20,767</p></td>
<td><p>192</p></td>
<td><p>20,575</p></td>
</tr>
<tr class="odd">
<td><p>44</p></td>
<td style="text-align: left;"><p><a href="../Page/Ølstykke-Stenløse.md" title="wikilink">Ølstykke-Stenløse</a><sup>2</sup></p></td>
<td><p>20,648</p></td>
<td><p>New</p></td>
<td><p>New</p></td>
</tr>
<tr class="even">
<td><p>45</p></td>
<td style="text-align: left;"><p><a href="../Page/Skive.md" title="wikilink">Skive</a></p></td>
<td><p>20,565</p></td>
<td><p>121</p></td>
<td><p> 20,686</p></td>
</tr>
<tr class="odd">
<td></td>
<td style="text-align: left;"></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<sup>1</sup> = 所有人口超过20,000的城市
<sup>2</sup> =
[Ølstykke-Stenløse](../Page/Ølstykke-Stenløse.md "wikilink") is a
new city, created by conurbation between
[Ølstykke](../Page/Ølstykke.md "wikilink") and
[Stenløse](../Page/Stenløse.md "wikilink") on 2010-01-01.
<sup>3</sup> = The city *Hørsholm* is a part of several
municipalities.
<sup>a</sup> = [哥本哈根都市区](../Page/哥本哈根都市区.md "wikilink")（da:
*Hovedstadsområdet*）的一部分
<sup>b</sup> = 奥尔堡都市区的一部分

## 属地

### 法罗群岛

[法罗群岛是](../Page/法罗群岛.md "wikilink")[丹麥的海外自治領地](../Page/丹麥.md "wikilink")，地理位置介乎挪威海和[北大西洋中間](../Page/北大西洋.md "wikilink")，處於[挪威到](../Page/挪威.md "wikilink")[冰島之間距離一半的位置](../Page/冰島.md "wikilink")。法羅群島陸地面積1,399平方公里，由17個有人島和若干個無人島組成，居民共46,662人（2004中期估計）。首府[托尔斯港居民多為斯堪的納維亞人後代](../Page/托尔斯港.md "wikilink")，基督教[路德會的教友](../Page/路德會.md "wikilink")，官方語言為[法羅語和](../Page/法羅語.md "wikilink")[丹麥語](../Page/丹麥語.md "wikilink")。

### 格陵兰

[格陵兰是世界上最大的](../Page/格陵兰.md "wikilink")[島嶼](../Page/島嶼.md "wikilink")，面積2,166,086平方公里，大約81%都由冰雪覆蓋。假若島上所有冰雪都溶化的話，全島的外貌就好像一個浮在海上的巨型甜甜圈。格陵蘭這個名稱的意思為「綠色的土地」，在[丹麥王國下是一個有著特殊地位的自治地区](../Page/丹麥.md "wikilink")，首府[戈特霍布](../Page/戈特霍布.md "wikilink")。全境大部分處在[北極圈內](../Page/北極圈.md "wikilink")，氣候寒冷，隔海峽與[加拿大與](../Page/加拿大.md "wikilink")[冰島兩國相望](../Page/冰島.md "wikilink")。

## 参考

## 参见

  - [各国城市列表](../Page/各国城市列表.md "wikilink")

## 外部链接

  - [Big Map of Denmark](ftp://ftp2.kms.dk/download/kort/d850.jpg)，from
    [Kort & Matrikelstyrelsen](http://www.kms.dk/)
  - [丹麦地图](https://web.archive.org/web/20060316104537/http://www.world-gazetteer.com/s/p_dk.htm)
  - [法罗群岛地图](https://web.archive.org/web/20071001004708/http://www.world-gazetteer.com/s/p_fo.htm)
  - [格陵兰地图](https://web.archive.org/web/20071001004613/http://www.world-gazetteer.com/s/p_gl.htm)

[城市](../Category/丹麥相關列表.md "wikilink")
[丹麥城市](../Category/丹麥城市.md "wikilink")
[Dan](../Category/歐洲城市列表.md "wikilink")

1.  Statistics Denmark: [BEF44: Folketal pr. 1. januar fordelt på
    byer](http://www.statistikbanken.dk/statbank5a/SelectVarVal/Define.asp?Maintable=BEF44&PLanguage=0)