**丹麦语**（，，）属于[印欧语系](../Page/印欧语系.md "wikilink")－[日尔曼语族](../Page/日尔曼语族.md "wikilink")－[北日尔曼语支](../Page/北日尔曼语支.md "wikilink")，通行於[丹麦王国以及其属地](../Page/丹麦.md "wikilink")[法罗群岛](../Page/法罗群岛.md "wikilink")、[格陵兰](../Page/格陵兰.md "wikilink")，也零星通行於[德国](../Page/德国.md "wikilink")、[挪威和](../Page/挪威.md "wikilink")[瑞典境内的部分地区](../Page/瑞典.md "wikilink")。

## 字母

丹麦语字母表是由[拉丁字母组成](../Page/拉丁字母.md "wikilink")。丹麦语和[挪威语十分相似](../Page/挪威语.md "wikilink")，而两者也使用相同的字母。

## 历史

大多数丹麦语词汇都是从[古北歐語中演变过来](../Page/古北歐語.md "wikilink")，很多新的词汇都是古老词汇变化和组合而成。丹麦语词汇中也有相当一部分来自[低地德语](../Page/低地德语.md "wikilink")。后来，[高地德语](../Page/高地德语.md "wikilink")、[法语和](../Page/法语.md "wikilink")[英语对丹麦语的影响超越了低地德语](../Page/英语.md "wikilink")。由于英语和丹麦语同属于[日尔曼语族](../Page/日尔曼语族.md "wikilink")，因此这两种语言中相似的词汇很多。例如，以下这些丹麦语词汇对于讲英语的人来说就十分容易辨认：have、over、under、for、kat，因为它们和英语中的对应词汇结构完全相同或相似。然而这些词汇在丹麦语中的读音却和它们在英语中的读音有天壤之别。此外，当by作为后缀的时候，意为“城镇”，这在一些古老的[英国地名中仍然保持着](../Page/英国.md "wikilink")，例如Whitby和Selby等等，可以看作是[维京时期丹麦人曾占领和统治过](../Page/维京.md "wikilink")[英格兰的痕迹](../Page/英格兰.md "wikilink")。丹麦语的发音对于学习这种语言的人来说是非常难于掌握的。不同于法语或[德语](../Page/德语.md "wikilink")，大量丹麦语词汇在形式上并不符合发音规则。

用丹麦语写作的著名人物包括：[存在主义哲学家索因](../Page/存在主义.md "wikilink")·[克尔凯郭尔](../Page/克尔凯郭尔.md "wikilink")、著名[童话作家](../Page/童话.md "wikilink")[汉斯·克里斯钦·安徒生](../Page/安徒生.md "wikilink")、剧作家[路德维希·霍尔伯格](../Page/路德维希·霍尔伯格.md "wikilink")。20世纪曾经有三位丹麦作家获得诺贝尔文学奖，他们分别是[卡尔·阿道夫·盖勒鲁普](../Page/卡尔·阿道夫·盖勒鲁普.md "wikilink")、[亨利克·蓬托皮丹和](../Page/亨利克·蓬托皮丹.md "wikilink")[约翰内斯·威廉·延森](../Page/约翰内斯·威廉·延森.md "wikilink")。

《[圣经](../Page/圣经.md "wikilink")》的第一个丹麦语译本于1550年出版。

## 类别

和丹麦语最具亲缘关系的语言是同属于[北日尔曼语支的](../Page/北日尔曼语支.md "wikilink")[挪威语和](../Page/挪威语.md "wikilink")[瑞典语](../Page/瑞典语.md "wikilink")。书面丹麦语和挪威语极其相似，尽管这三种语言的语音系统截然不同。如果一个人通晓其中任何一种语言，他便可以毫无困难的阅读其他两种语言。但研究表明说挪威语的人理解丹麦语与瑞典语的能力要比说丹麦语的人与说瑞典语的人相互理解对方语言的能力强的多，反过来说丹麦语的人和说瑞典语的人理解挪威语的能力也远强于他们之间的相互理解。由于三种语言的相似程度非常高，因此有些语言学家甚至将它们定义为一种语言的三个方言。

## 分布与官方地位

丹麦语是[丹麦的官方语言](../Page/丹麦.md "wikilink")，[格陵兰的两种官方语言之一](../Page/格陵兰.md "wikilink")（另一种是[格陵兰语](../Page/格陵兰语.md "wikilink")），[法罗群岛的两种官方语言之一](../Page/法罗群岛.md "wikilink")（另一种是[法罗语](../Page/法罗语.md "wikilink")）。此外，在位于[德国境内和丹麦接壤的](../Page/德国.md "wikilink")[石勒苏益格地区](../Page/石勒苏益格.md "wikilink")，也有小规模的人群讲丹麦语。通行于石勒苏益格地区的丹麦语被德国官方正式承认为德国的区域性语言之一，并受到政府的保护。

## 主要方言

[Danishdialectmap.png](https://zh.wikipedia.org/wiki/File:Danishdialectmap.png "fig:Danishdialectmap.png")

### [勃恩霍尔姆方言](../Page/勃恩霍尔姆.md "wikilink")

通行於勃恩霍尔姆地区，丹麦东部。

### 菲因方言

通行於[菲因岛](../Page/菲因岛.md "wikilink")，丹麦中部。

### 标准丹麦语

通行於西兰岛，丹麦中东部。

### 日德兰方言

[日德兰语](../Page/日德兰语.md "wikilink")（）
通行於[日德兰半岛](../Page/日德兰半岛.md "wikilink")，丹麦西部。

  -   - [西日德兰方言](../Page/西日德兰方言.md "wikilink")
      - [东日德兰方言](../Page/东日德兰方言.md "wikilink")
      - [北日德兰方言](../Page/北日德兰方言.md "wikilink")
      - [南日德兰方言](../Page/南日德兰方言.md "wikilink")
      - [中日德兰方言](../Page/中日德兰方言.md "wikilink")

### 南部瑞典方言

  -   - [哈兰语](../Page/哈兰语.md "wikilink")
      - [南部瑞典语](../Page/南部瑞典语.md "wikilink")
      - [斯马兰语](../Page/斯马兰语.md "wikilink")

在历史上，由于[瑞典南部曾长期属于丹麦的领土](../Page/瑞典.md "wikilink")，因此“[南部瑞典语](../Page/南部瑞典语.md "wikilink")”曾经被认为是丹麦语的一种方言，和[勃恩霍尔姆方言合称为](../Page/勃恩霍尔姆.md "wikilink")“东部丹麦语”。现在，讲“南部瑞典语”的人都生活在瑞典境内，因此瑞典官方自然将这种语言看作是瑞典语的方言了。[语言学上通常将南部瑞典语看作是丹麦语和](../Page/语言学.md "wikilink")[瑞典语之间的过渡语言](../Page/瑞典语.md "wikilink")。

### 其他方言

  - [samsk](../Page/samsk.md "wikilink")
  - [aerosk](../Page/aerosk.md "wikilink")
  - [langelandsk](../Page/langelandsk.md "wikilink")
  - [falstersk](../Page/falstersk.md "wikilink")
  - [monsk](../Page/monsk.md "wikilink")

### 要点

在[丹麦的学校里教授的丹麦语被称为](../Page/丹麦.md "wikilink")“书面丹麦语”，这是一种比较正规和严格的形式，主要通行於大城市。

在不同的小岛屿和社区之间存在着大量的方言。一些方言彼此间可以交流，而对于有些方言，居住范围超过50公里的人们就很难听懂了；现今的丹麦由于实行强制性义务教育和实施对学生的经济支持福利，与电视传媒的发展，课堂上的标准丹麦语开始逐渐大众化。尽管如此，一些和书面丹麦语相去甚远的小方言至今仍在一些乡村通行着。然而现在，这些乡村地区的年轻人们通常都将自己家乡的方言和书面丹麦语结合起来，这样使得他们和来自丹麦其他地方的人们也能沟通。

## 语音

对于讲其他语言的人来说，丹麦语的语音是非常难以掌握的。和抑扬顿挫的[挪威语](../Page/挪威语.md "wikilink")、[瑞典语比起来](../Page/瑞典语.md "wikilink")，丹麦语语调显得过于平坦而单调。字母r发咽喉擦音，即在口腔深处气流和咽喉摩擦而发出的音，这和将r发为颤音的[斯拉夫语言及](../Page/斯拉夫语族.md "wikilink")[罗曼语言极为不同](../Page/罗曼语族.md "wikilink")。

[斯堪的纳维亚语言的三个元音](../Page/斯堪的纳维亚.md "wikilink") 、、
的发音对于初学者来说也很难掌握。丹麦有个非常经典的绕口令：（意为“浇了[奶油的红莓](../Page/奶油.md "wikilink")[布丁](../Page/布丁.md "wikilink")”），难倒了很多外国人，因为这短短的一句中包括了三个“”（两种不同的发音方式）、咽喉擦音r、重音gr组合和软化的d（发音类似与英语with中的th）。

  -  – 发音类似英语单词met中的e

  -  – 在英语中没有对应的语音，不过和单词 Bird 中的 ir
    去掉“r”音的情况有些相似[1](http://www.faqs.org/faqs/nordic-faq/part1_INTRODUCTION/section-7.html)。和[德语中的元音](../Page/德语.md "wikilink")
    [ö](../Page/ö.md "wikilink") 相同， 和[法语单词](../Page/法语.md "wikilink")
    feu 中的[元音发音基本相同](../Page/元音.md "wikilink")。

  -  –
    发音类似于英语单词cause中au组合的发音，但是稍微短一些。在丹麦语中字母o有时也发同样的音，例如在onkel（叔叔）一词中就如此。

由于丹麦语的语音系统非常难于掌握，有些丹麦人也如此揶揄自己的语言：“丹麦语与其说是一种语言，不如说是一种咽喉疾病。”另一种说法更生动：“说话的时候口中仿佛含着一个滚烫的[马铃薯](../Page/马铃薯.md "wikilink")”。

丹麦语的发音规律还包括：

  - 字母“d”的发音的软化现象。字母d位于词尾元音后，或元音与/ə/, -ig,
    -isk间时发为，其他情况下为。dt、ds和词末ld等组合中的d不发音。

  - ：存在于很多词汇的发音中。多表现为[嘎裂音](../Page/嘎裂音.md "wikilink")（creaky
    voice），有时也发音为[喉塞音](../Page/喉塞音.md "wikilink")。为丹麦语所特有，很难掌握。

## 语法

丹麦语[动词的](../Page/动词.md "wikilink")[不定式一般以元音字母e结尾](../Page/不定式.md "wikilink")。动词依[时态的不同而变形](../Page/时态.md "wikilink")，却没有[人称和数的变化](../Page/人称.md "wikilink")。例如，动词不定式spise（吃）的一般现在时是spiser，无论[主语是第一](../Page/主语.md "wikilink")、第二还是第三人称，单数还是复数，它的形式都不发生变化。尽管丹麦语的动词变化规律比较简单，但是丹麦语中还存在大量的[不规则动词](../Page/不规则动词.md "wikilink")。最晚近公布的丹麦语[语法规则允许很多不规则动词按照规则动词的标准来变位](../Page/语法.md "wikilink")，也允许许多名词按照其读音来拼写。

在丹麦语中，名词有两个语法性：通性与中性。和[德语一样](../Page/德语.md "wikilink")，古丹麦语中名词有三个性，分别是阳性、中性和阴性。在近代的语言改革之中，阴性和阳性名词合并组成通性名词。通性名词的数量大致占名词总数的75％。在多数情况下，名词属于哪个性是没有规律的，必须硬性记忆。

包括丹麦语在内的[斯堪的那维亚语言还有一个显著的特点](../Page/斯堪的那维亚.md "wikilink")，就是[定冠词后置现象](../Page/冠詞.md "wikilink")。例如，通性名词“男人”在丹麦语里是mand，“一个男人”则是en
mand，其中en是通性名词的[不定冠词](../Page/冠詞.md "wikilink")。但若要表示“这个男人”，也就是特指的情况下，并不像英语一样有一个对应的定冠词the，而是要将不定冠词en后置，变成manden。不过需要注意的是，如果[名词前面有](../Page/名词.md "wikilink")[形容词修饰](../Page/形容词.md "wikilink")，定冠词就不能后置，而是和[英语一样要放在前面](../Page/英语.md "wikilink")。比如如果想要表达“这个高大的男人”（the
big man），就要写成den store
mand。其中den在丹麦语中大致相当于英语中的this，在此起到定冠词的结构作用。至于[中性名词](../Page/名詞.md "wikilink")，[冠词的用法和通性名词完全一致](../Page/冠词.md "wikilink")，只不过以et代替en，以det代替den。例如，中性名词“房子”在丹麦语里是hus，“一幢房子”就是et
hus，“这幢房子”是huset，“这幢高大的房子”则是det store hus。

丹麦语的另外一个显著的特点是合成词非常多。通常词汇就按照其意义自由合成，这点和德语也非常相似。例如单词kvindehåndboldlandsholdet的意思就是“这支国家女子手球队”，由“kvinde”、“hånd”、“bold”、“lands”、“hold”和后置定冠词“et”合成的。

## 书写规则

丹麦语使用[拉丁字母来书写](../Page/拉丁字母.md "wikilink")，只是在字母表的末尾多了三个特殊的元音：、 和
。这三个字母是1948年开始施行的[正字法中规定采用的](../Page/正字法.md "wikilink")。在此之前，“”由代替、“”则由代替。在今天的丹麦，一些旧的地名和人名仍然采用这种古老的拼写方式。

现代丹麦语和现代[挪威语的](../Page/挪威语.md "wikilink")[字母表完全相同](../Page/字母表.md "wikilink")，但是读音却有很大差异。

## 常用的短语和表达

  - 丹麦语：dansk
  - 你好：hej
  - 再见：farvel
  - 请：Vær's go
  - 谢谢：tak
  - 这个：denne
  - 多少钱？ hvor meget?／Hvad koster det?
  - 英语：engelsk
  - 是的：ja
  - 不：nej
  - 我可以为你拍照吗？ Må jeg tage et billede af dig?
  - 卫生间在哪里？ Hvor er toilettet?
  - 你从哪里来？ Hvor kommer du fra?
  - 你讲英语吗？ Taler du engelsk?
  - 干杯！ Skål
  - 早上好！ God morgen\!
  - 对不起！ Undskyld\!
  - 你叫什么名字？ Hvad hedder du?
  - 我叫…：Jeg hedder ...
  - 你好吗？ Hvordan går det?／Hva så
  - 我很好。Det går godt.
  - 我不太好。Det går ikke så godt.
  - 你呢？ Hvad med dig?
  - 我能帮你吗？ Kan jeg hjælpe dig?
  - 我就随便看看。 Jeg kigger bare.

## 參考文獻

## 參看

  - [丹麦语和挪威语字母](../Page/丹麦语和挪威语字母.md "wikilink")
  - [日德兰语](../Page/日德兰语.md "wikilink")

## 外部链接

  - \[[http://arquivo.pt/wayback/20080218151543/http://translate.google.com/translate_t\#auto|zh-CN](http://arquivo.pt/wayback/20080218151543/http://translate.google.com/translate_t#auto%7Czh-CN)|
    使用Google自动将丹麦语翻译成简体中文\]
  - \[[http://arquivo.pt/wayback/20080218151543/http://translate.google.com/translate_t\#auto|zh-TW](http://arquivo.pt/wayback/20080218151543/http://translate.google.com/translate_t#auto%7Czh-TW)|
    使用Google自动将丹麦语翻译成繁体中文\]
  - [免费的丹麦语词典](http://www.dicts.info/dictlist1.php?k1=23)
  - [Danish 101](http://www.101languages.net/danish/) Learn Danish
    online
  - [为期10天的丹麦语强化教程](http://www.speakdanish.dk/index.html)
  - [关于丹麦语的一些信息](https://web.archive.org/web/20050721084225/http://www.dsn.dk/omdsn_en.htm)
  - [丹－英词典](https://web.archive.org/web/20050531211439/http://www.websters-online-dictionary.org/definition/danish-english/)（来自
    [Webster's Online
    Dictionary](https://web.archive.org/web/20120223164907/http://www.websters-online-dictionary.org/)）
  - [丹麦语语法](https://web.archive.org/web/20090201172800/http://hjem.tele2adsl.dk/johnmadsen/Danish/danish.html)

[Category:北日耳曼语支](../Category/北日耳曼语支.md "wikilink")
[Category:挪威語言](../Category/挪威語言.md "wikilink")
[Category:丹麦语](../Category/丹麦语.md "wikilink")
[Category:主謂賓語序語言](../Category/主謂賓語序語言.md "wikilink")
[Category:屈折語](../Category/屈折語.md "wikilink")