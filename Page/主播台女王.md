《**首席女主播**》（）為[日本](../Page/日本.md "wikilink")[富士電視台出品及製作的](../Page/富士電視台.md "wikilink")[電視劇](../Page/日劇.md "wikilink")，全11集。於2006年4月17日至6月26日星期一21:00～21:54在該台全國系列網絡播放（第一回和最終回擴大15分鐘，播放至22:09）。[香港](../Page/香港.md "wikilink")[無綫電視已購入該劇的香港地區播映權](../Page/電視廣播有限公司.md "wikilink")，並安排在2007年4月14日起於[無綫劇集台首播](../Page/無綫劇集台.md "wikilink")，及在2007年10月15日起於[明珠台](../Page/無綫電視明珠台.md "wikilink")、2009年4月19日起於[翡翠台及](../Page/翡翠台.md "wikilink")[高清翡翠台播放](../Page/高清翡翠台.md "wikilink")。[臺灣的播映權則由](../Page/臺灣.md "wikilink")[緯來日本台購入](../Page/緯來日本台.md "wikilink")，於2007年1月18日至1月31日間於晚間9時至10時播放。

女主角王牌主播的角色由[天海祐希飾演](../Page/天海祐希.md "wikilink")，這也是她首次擔任富士電視台「[月9](../Page/月9.md "wikilink")」（星期一晚間九點播出的日劇）的女主角。

本劇的第三集劇情為[占卜師涉嫌詐欺疑雲](../Page/占卜.md "wikilink")，由於日本知名占卜師[細木数子向富士電視台抗議](../Page/細木数子.md "wikilink")，故富士電視台決定，日後在日本國內重播該劇、或發行DVD、或販售海外版權時，一律抽掉該集。

## 劇情介紹

## 角色

| 演員                                   | 角色          | 粵語配音                                     |
| ------------------------------------ | ----------- | ---------------------------------------- |
| [天海祐希](../Page/天海祐希.md "wikilink")   | 樁木 春香(38歲)  | [黃玉娟](../Page/黃玉娟.md "wikilink")         |
| [矢田亞希子](../Page/矢田亞希子.md "wikilink") | 飛鳥 望美(27歲)  | [曾佩儀](../Page/曾佩儀.md "wikilink")         |
| [玉木宏](../Page/玉木宏.md "wikilink")     | 蟹原 健介(27歲)  | [黃榮璋](../Page/黃榮璋.md "wikilink")         |
| [谷原章介](../Page/谷原章介.md "wikilink")   | 結城 雅人(36歲)  | [翟耀輝](../Page/翟耀輝.md "wikilink")         |
| [松下奈緒](../Page/松下奈緒.md "wikilink")   | 野原 芽衣(24歲)  | [林元春](../Page/林元春.md "wikilink")         |
| [松田翔太](../Page/松田翔太.md "wikilink")   | 伊賀 俊平(24歲)  | [李致林](../Page/李致林.md "wikilink")         |
| [田丸麻紀](../Page/田丸麻紀.md "wikilink")   | 蟹原 珠子(27歲)  | [曾秀清](../Page/曾秀清.md "wikilink")         |
| [須籐理彩](../Page/須籐理彩.md "wikilink")   | 紺野 令子(30歲)  | [程文意](../Page/程文意.md "wikilink")         |
| [矢島健一](../Page/矢島健一.md "wikilink")   | 角高 孝男(43歲)  | [張炳強](../Page/張炳強.md "wikilink")         |
| [卜字高雄](../Page/卜字高雄.md "wikilink")   | 蟹原 三郎(59歲)  | [黃子敬](../Page/黃子敬.md "wikilink")         |
| [生瀨勝久](../Page/生瀨勝久.md "wikilink")   | 石場 小吉識(43歲) | [李錦綸](../Page/李錦綸_\(配音員\).md "wikilink") |
| [兒玉清](../Page/兒玉清.md "wikilink")     | 柴田 勝俊(59歲)  | [譚炳文](../Page/譚炳文.md "wikilink")         |

  - 報導工作人員

<!-- end list -->

  - [海老原敬介](../Page/海老原敬介.md "wikilink")
  - [黑瀨真二](../Page/黑瀨真二.md "wikilink")
  - [執行利一](../Page/執行利一.md "wikilink")
  - [巽佳子](../Page/巽佳子.md "wikilink")
  - [關鐘美](../Page/關鐘美.md "wikilink")
  - [石川雄也](../Page/石川雄也.md "wikilink")
  - [莊島康哲](../Page/莊島康哲.md "wikilink")
  - [金子久美](../Page/金子久美.md "wikilink")
  - [豊原愛](../Page/豊原愛.md "wikilink")

其他（客串）

  - 加山聰子：[大路惠美](../Page/大路惠美.md "wikilink")（第1話）
  - 西園寺真嗣：[平山廣行](../Page/平山廣行.md "wikilink")（第1話）
  - 通山秀一：[東根作壽英](../Page/東根作壽英.md "wikilink")（第1話）

<!-- end list -->

  - 財前誠：[溫水洋一](../Page/溫水洋一.md "wikilink")（第2話）
  - 山村醫學部長：[須永慶](../Page/須永慶.md "wikilink")（第2話）
  - 財前隆史：[鹽顯治](../Page/鹽顯治.md "wikilink")（第2話）
  - 財前彩香：[佐佐木麻緒](../Page/佐佐木麻緒.md "wikilink")（第2話）

<!-- end list -->

  - 宮部天花：[黑田福美](../Page/黑田福美.md "wikilink")（第3話）
  - 澤木和美：[中村榮子](../Page/中村榮子.md "wikilink")（第3話）
  - 澤木正美：[中園友乃](../Page/中園友乃.md "wikilink")（第3話）
  - 服部圭吾：[葛山信吾](../Page/葛山信吾.md "wikilink")（第4話）

<!-- end list -->

  - 保阪智世：[涼](../Page/涼.md "wikilink")（第5話）
  - 保阪敏行：[福本伸一](../Page/福本伸一.md "wikilink")（第5話）
  - 保阪大輔：[深澤嵐](../Page/深澤嵐.md "wikilink")（第5話）
  - 岡崎幸助：[田窪一世](../Page/田窪一世.md "wikilink")（第5話）
  - 大島製作人：[笠原浩夫](../Page/笠原浩夫.md "wikilink")（第5話）
  - 山岸猛夫：[岸田真彌](../Page/岸田真彌.md "wikilink")（第5話）
  - 櫻井尚樹：[東幹久](../Page/東幹久.md "wikilink")（第6話）
  - 佐野竹彥：[弓削智久](../Page/弓削智久.md "wikilink")（第6話）
  - 四方田代議士：[大林丈史](../Page/大林丈史.md "wikilink")（第6話）
  - 澤村優衣：[島村麻美](../Page/島村麻美.md "wikilink")（第7話）
  - 瀨田敬一：[根本慎太郎](../Page/根本慎太郎.md "wikilink")（第7話）
  - 濱田：[姜暢雄](../Page/姜暢雄.md "wikilink")（第7話）
  - 胡蘿蔔田的伯伯：[市原清彥](../Page/市原清彥.md "wikilink")（第7話）
  - 胡蘿蔔人：[小崇小敏](../Page/小崇小敏.md "wikilink")（第7話）
  - 內藤久美：[中越典子](../Page/中越典子.md "wikilink")（第8話）
  - 沼田理事長：[長谷川初範](../Page/長谷川初範.md "wikilink")（第8話）
  - 教頭：[不破萬作](../Page/不破萬作.md "wikilink")（第8話）
  - 沼田幸彥：[馬場徹](../Page/馬場徹.md "wikilink")（第8話）
  - 祥司：[齊籐嘉樹](../Page/齊籐嘉樹.md "wikilink")（第8話）
  - 剛史：[千代將太](../Page/千代將太.md "wikilink")（第8話）
  - 克己：[荒川優](../Page/荒川優.md "wikilink")（第8話）
  - 山田正夫：[渡邊哲](../Page/渡邊哲.md "wikilink")（第9話）
  - 山田佳奈：[伊籐步](../Page/伊籐步.md "wikilink")（第9話）
  - 橫山部長：[入江雅人](../Page/入江雅人.md "wikilink")（第9話）
  - 結城英雄：[伊武雅刀](../Page/伊武雅刀.md "wikilink")（第9話～最終回）

## 工作人員

  - 劇本：[坂元裕二](../Page/坂元裕二.md "wikilink")
  - 編輯：[保原賢一郎](../Page/保原賢一郎.md "wikilink")
  - 演出：[平野眞](../Page/平野眞.md "wikilink")、[葉山浩樹](../Page/葉山浩樹.md "wikilink")、[七高剛](../Page/七高剛.md "wikilink")
  - 音楽：[佐藤直紀](../Page/佐藤直紀.md "wikilink")
  - 製作人：[現王園佳正](../Page/現王園佳正.md "wikilink")
  - 製作：富士電視台製作中心
  - 製作著作：富士電視台

## 主題曲

  - 《Dear friend》（演唱：[Sowelu](../Page/Sowelu.md "wikilink")）

## 播放日期、副題、收視率

| 集數                  | 播放日期       | 副題         | 收視率\[1\]                                 |
| ------------------- | ---------- | ---------- | ---------------------------------------- |
| 1                   | 2006年4月17日 | 呼風喚雨的女人    | <font color="red">23.1%</font>           |
| 2                   | 2006年4月24日 | 英雄的告白      | 19.6%                                    |
| 3                   | 2006年5月1日  | 戀愛運零的逆襲    | 18.7%                                    |
| 4                   | 2006年5月8日  | 消失的大獨家     | <span style="color: green;">15.6%</span> |
| 5                   | 2006年5月15日 | 被瞄準的女人     | 18.6%                                    |
| 6                   | 2006年5月22日 | 終極宿敵       | 16.8%                                    |
| 7                   | 2006年5月29日 | 主播變更       | 17.3%                                    |
| 8                   | 2006年6月5日  | 相戀的兩人      | 17.5%                                    |
| 9                   | 2006年6月12日 | 突然的辭退宣告！   | 17.1%                                    |
| 10                  | 2006年6月19日 | 失控的女人      | 18.1%                                    |
| 大結局                 | 2006年6月26日 | 再見了…傳說中的女人 | 18.5%                                    |
| colspan= 3|**平均收視** | **18.3%**  |            |                                          |

## 註釋

## 節目的變遷

[Category:2006年日本電視劇集](../Category/2006年日本電視劇集.md "wikilink")
[Category:緯來電視外購日劇](../Category/緯來電視外購日劇.md "wikilink")
[Category:無綫電視外購劇集](../Category/無綫電視外購劇集.md "wikilink")
[Category:電視台背景作品](../Category/電視台背景作品.md "wikilink")
[Category:新聞行業題材電視節目](../Category/新聞行業題材電視節目.md "wikilink")
[Category:日語電視劇](../Category/日語電視劇.md "wikilink")
[Category:坂元裕二劇本作品](../Category/坂元裕二劇本作品.md "wikilink")

1.  [關東收視](../Page/關東.md "wikilink")，由[Video
    Research社調查](../Page/Video_Research.md "wikilink")。