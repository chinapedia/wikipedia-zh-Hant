**-{zh-hans:主板; zh-hant:主機板}-**（，简称）；又称**-{zh-hans:主机板;
zh-hant:主板}-**、**系统板**、**逻辑板**(**logic
board**)、**母板**、**底板**等，是构成复杂电子系统例如[電子計算機的中心或者主](../Page/電子計算機.md "wikilink")[電路板](../Page/印刷電路板.md "wikilink")。

典型的主板能提供一系列接合點，供[處理器](../Page/處理器.md "wikilink")、[顯示卡](../Page/顯示卡.md "wikilink")、[硬碟機](../Page/硬碟機.md "wikilink")、[記憶體](../Page/內存.md "wikilink")、對外裝置等裝置接合。它們通常直接插入有關插槽，或用線路連接。主機板上最重要的構成元件是[晶片組](../Page/晶片組.md "wikilink")（）。早期的晶片組通常由[北橋和](../Page/北橋.md "wikilink")[南橋組成](../Page/南橋.md "wikilink")，現在的Intel/AMD主板絕大多數採用單晶片設計，增強其效能（自Intel
5系列晶片組/AMD
APU平台開始，晶片組採用單晶片組設計，主板上只有南橋，Intel將其稱為PCH，AMD將其稱為FCH，北橋被移入CPU中以增強效能）。晶片組為主機板提供一個通用平台供不同裝置連接，控制不同裝置的溝通。它亦包含對不同擴充插槽的支援，例如[處理器](../Page/處理器.md "wikilink")、[PCI
Express](../Page/PCI_Express.md "wikilink")。晶元組亦為主機板提供額外功能，例如內建於晶片組/CPU的顯示卡，整合的聲卡。現在的家用主板絕大多數內建聲卡、網路卡。一些高價主板也集成[紅外通訊技術](../Page/紅外通訊技術.md "wikilink")、[藍牙和](../Page/藍牙.md "wikilink")[802.11](../Page/802.11.md "wikilink")（Wi-Fi）等功能。

## 主要组成

下圖的[LGA
1366主機板包含](../Page/LGA_1366.md "wikilink")[南橋和](../Page/南桥.md "wikilink")[北橋](../Page/北桥.md "wikilink")，這是最後一代使用雙晶片的主機板。之後所有Intel與AMD的主機板僅有南橋，北橋和內建顯示核心已整合到CPU。
[P6T_Deluxe_V2.jpg](https://zh.wikipedia.org/wiki/File:P6T_Deluxe_V2.jpg "fig:P6T_Deluxe_V2.jpg")

## 溫度與可靠度

主機板一般會使用[散热片](../Page/散热片.md "wikilink")，例如[北橋](../Page/北橋.md "wikilink")[晶片](../Page/晶片.md "wikilink")，在以前這樣子被動的散熱方式可以滿足需求。直到了1990年，因為處理器的頻率提升以及功率的上昇，所以CPU散熱器需要掛載風扇以滿足需求。除此之外，主機板也有機殼的風扇幫助散熱。較新的主機板更整合了溫度感測器用於偵測CPU溫度，透過[BIOS或是](../Page/BIOS.md "wikilink")[作業系統分析溫度變化以控制風扇的轉動速度](../Page/作業系統.md "wikilink")。

## BIOS 啟動

電腦主機板是電腦的主要骨幹，上面有實體電路並且用電路將各元件做聯結，而可控制主機板上邏輯電路運作的是使用者、軟體程式以及輸入裝置；但是要從關機狀態啟動，則必須執行初始化的軟體指令。

大部分的主機板會有 [BIOS](../Page/BIOS.md "wikilink") 儲存在
[EEPROM](../Page/EEPROM.md "wikilink")
晶片內，用於對主機板作啟動的初始化；在啟動的過程中包含記憶體、周邊裝置都會被測試以及做初始設定，這個過程稱為
[加电自检](../Page/加电自检.md "wikilink")（[POST](../Page/POST.md "wikilink")），若是在
POST 的過程中出現錯誤，則主機會發出"嗶"聲或是出現錯誤訊息在螢幕上。

從2011年起，大部分的零售主板已採用[UEFI](../Page/UEFI.md "wikilink")
BIOS，一些廠商（尤其是[微星科技](../Page/微星科技.md "wikilink")）還率先匯入圖形界面UEFI
BIOS、線上更新BIOS等技術。

大約從[Pentium
III起](../Page/Pentium_III.md "wikilink")，主板廠商逐步在BIOS中加入了[超頻功能](../Page/超頻.md "wikilink")，但是自Intel
[Sandy
Bridge微架構開始](../Page/Sandy_Bridge微架構.md "wikilink")，Intel對超頻進行大幅限制，不支援超頻的Intel處理器（如大部分型號不以K結尾的桌上型Intel
Core處理器）難以超頻。同樣從[AMD
APU開始](../Page/AMD_APU.md "wikilink")，AMD限制CPU產品的超頻，但從
[AMD
Ryzen系列後AMD只鎖](../Page/AMD_Ryzen.md "wikilink")[A320主機板](../Page/AMD_Ryzen.md "wikilink")。OEM主板的BIOS通常不支援超頻。基於穩定性考慮，伺服器超頻是不允許的。

## CPU插座

不同CPU系列使用不同插槽。後期CPU插槽，數字多數与針腳數量相同。前期CPU插槽則根據問世次序命名。

### [Intel處理器插槽](../Page/Intel.md "wikilink")

[Personal_computer,_exploded_6.svg](https://zh.wikipedia.org/wiki/File:Personal_computer,_exploded_6.svg "fig:Personal_computer,_exploded_6.svg")

  - [Socket 6](../Page/Socket_6.md "wikilink") -
    [80486DX4。](../Page/Intel_80486.md "wikilink")
  - [Socket 7](../Page/Socket_7.md "wikilink") -
    [Intel](../Page/Intel.md "wikilink") [Pentium
    OverDrive](../Page/Pentium_OverDrive.md "wikilink") 和 Pentium
    MMX、[AMD](../Page/AMD.md "wikilink")
    [K5](../Page/K5.md "wikilink")、[K6](../Page/K6.md "wikilink")、[K6-2](../Page/K6-2.md "wikilink")、[K6-3](../Page/K6-3.md "wikilink")、[K6-2+](../Page/K6-2+.md "wikilink")、[K6-3+](../Page/K6-3+.md "wikilink")、[Rise
    mP6](../Page/Rise_Technology.md "wikilink")、[IDT](../Page/IDT.md "wikilink")／Centaur
    的WinChip、WinChip2和一些[IBM及](../Page/IBM.md "wikilink")[Cyrix处理器](../Page/Cyrix.md "wikilink")。
  - [Socket 8](../Page/Socket_8.md "wikilink") - [Pentium
    Pro。](../Page/Pentium_Pro.md "wikilink")
  - [Slot 1](../Page/Slot_1.md "wikilink") - [Pentium
    II](../Page/Pentium_II.md "wikilink")、[Pentium
    III和](../Page/Pentium_III.md "wikilink")[Celeron](../Page/Celeron.md "wikilink")（233MHz—1.13GHz）。
  - [Slot 2](../Page/Slot_2.md "wikilink") - [Pentium II
    Xeon](../Page/Xeon.md "wikilink")、[Pentium III
    Xeon處理器](../Page/至强.md "wikilink")（Pentium II/III 核心）。
  - Socket PAC418 - [Itanium處理器](../Page/Itanium.md "wikilink")。
  - Socket PAC611 - [Itanium 2處理器](../Page/Itanium_2.md "wikilink")。
  - [Socket 370](../Page/Socket_370.md "wikilink") -
    [Celeron](../Page/Celeron.md "wikilink") 和 [Pentium
    III](../Page/Pentium_III.md "wikilink")（800MHz—1.4GHz）。
  - [Socket 423](../Page/Socket_423.md "wikilink") - [Pentium
    4](../Page/Pentium_4.md "wikilink") 和
    [Celeron](../Page/Celeron.md "wikilink")（建基於Willamette核心）。
  - [Socket 478](../Page/Socket_478.md "wikilink") - [Pentium
    4](../Page/Pentium_4.md "wikilink") 和
    [Celeron](../Page/Celeron.md "wikilink")（建基於Northwood、Prescott，以及Willamette
    核心）。
  - [Socket 479](../Page/Socket_479.md "wikilink") - [Pentium
    M](../Page/Pentium_M.md "wikilink") 和 [Celeron
    M](../Page/Celeron_M.md "wikilink")（建基於 Banias 和 Dothan 核心）。
  - Socket 480 - [Pentium M](../Page/Pentium_M.md "wikilink")（建基於 Yonah
    核心）。
  - Socket 603／[604](../Page/Socket_604.md "wikilink")-
    [Xeon](../Page/Xeon.md "wikilink") - 建基於 Northwood 和 Willamette
    Pentium 4 核心。
  - [LGA 775](../Page/LGA_775.md "wikilink") - [Pentium
    D和](../Page/Pentium_D.md "wikilink")[Pentium
    4和](../Page/Pentium_4.md "wikilink")[Pentium
    XE和](../Page/奔騰Extreme_Edition.md "wikilink")[Pentium
    Dual-Core](../Page/Pentium_Dual-Core.md "wikilink")、[Celeron](../Page/Celeron.md "wikilink")、[Intel
    Core](../Page/Intel_Core.md "wikilink")
    2、部分[Xeon處理器](../Page/Xeon.md "wikilink")（建基於Northwood和Prescott、Presler、Yorkfield及Conroe核心）。
  - [LGA 771](../Page/LGA_771.md "wikilink") -
    [Xeon處理器](../Page/Xeon.md "wikilink")。
  - [LGA 1156](../Page/LGA_1156.md "wikilink") - 第1代[Core
    i3](../Page/Core_i3.md "wikilink")、[Core
    i5](../Page/Core_i5.md "wikilink")、[Core
    i7處理器](../Page/Core_i7.md "wikilink")。
  - [LGA 1366](../Page/LGA_1366.md "wikilink") - [Core
    i7處理器](../Page/Core_i7.md "wikilink")。
  - [LGA 1155](../Page/LGA_1155.md "wikilink") - [Sandy Bridge Core
    i3](../Page/Sandy_Bridge.md "wikilink")，[Sandy Bridge Core
    i5](../Page/Sandy_Bridge.md "wikilink")，[Sandy Bridge Core
    i7](../Page/Sandy_Bridge.md "wikilink") ,[Ivy Bridge Core
    i3](../Page/Ivy_Bridge.md "wikilink")，[Ivy Bridge Core
    i5](../Page/Ivy_Bridge.md "wikilink")，[Ivy Bridge Core
    i7處理器](../Page/Ivy_Bridge.md "wikilink")。
  - [LGA 2011](../Page/LGA_2011.md "wikilink") - [Sandy Bridge E Core
    i7](../Page/Sandy_Bridge.md "wikilink")，[Ivy Bridge E Core
    i7處理器](../Page/Ivy_Bridge.md "wikilink")，對應X79晶片組，LGA
    2011-3對應X99。
  - [LGA 1150](../Page/LGA_1150.md "wikilink") -
    [Haswell架構及](../Page/Haswell.md "wikilink")[Broadwell架構处理器](../Page/Broadwell.md "wikilink")。
  - [LGA 1151](../Page/LGA_1151.md "wikilink") -
    [Skylake及](../Page/Skylake.md "wikilink")[Kaby
    Lake架構处理器](../Page/Kaby_Lake.md "wikilink")。

### [AMD處理器插槽](../Page/AMD.md "wikilink")

[ASRock_K7VT4A_Pro_Mainboard_Labeled_English.svg](https://zh.wikipedia.org/wiki/File:ASRock_K7VT4A_Pro_Mainboard_Labeled_English.svg "fig:ASRock_K7VT4A_Pro_Mainboard_Labeled_English.svg")

  - [Slot A](../Page/Slot_A.md "wikilink") -
    [Athlon](../Page/Athlon.md "wikilink") 處理器。
  - [Socket 462](../Page/Socket_462.md "wikilink")（即是 [Socket
    A](../Page/Socket_A.md "wikilink")）-
    [Athlon](../Page/Athlon.md "wikilink")、[Athlon
    XP](../Page/Athlon_XP.md "wikilink")、[Sempron](../Page/Sempron.md "wikilink")、和[Duron處理器](../Page/Duron.md "wikilink")
    。
  - [Socket 754](../Page/Socket_754.md "wikilink") - 低階[Athlon
    64](../Page/Athlon_64.md "wikilink") 和
    [Sempron](../Page/Sempron.md "wikilink") 處理器 ，只支援單通道[DDR
    Ram](../Page/DDR_SDRAM.md "wikilink")。
  - [Socket 939](../Page/Socket_939.md "wikilink") - [Athlon
    64](../Page/Athlon_64.md "wikilink") 和 [Athlon 64
    X2和](../Page/Athlon_64_X2.md "wikilink")[Athlon 64
    FX](../Page/Athlon_64_FX.md "wikilink") 處理器
    ，支援[双通道](../Page/Dual_Channel.md "wikilink")[DDR
    Ram和雙核心技術](../Page/DDR_SDRAM.md "wikilink")。
  - [Socket 940](../Page/Socket_940.md "wikilink") -
    [Opteron](../Page/Opteron.md "wikilink") 和 早期 [Athlon 64
    FX](../Page/Athlon_64_FX.md "wikilink") 處理器 。
  - [Socket AM2](../Page/Socket_AM2.md "wikilink") - [Athlon
    64](../Page/Athlon_64.md "wikilink") 及
    [Sempron處理器](../Page/Sempron.md "wikilink")，支援[雙通道](../Page/雙通道.md "wikilink")[DDR-II
    Ram](../Page/DDR2.md "wikilink")（有940支針腳，但排列跟[Socket
    940不同](../Page/Socket_940.md "wikilink")）。
  - [Socket AM2+](../Page/Socket_AM2+.md "wikilink") - [Athlon
    64](../Page/Athlon_64.md "wikilink")、[Sempron處理器](../Page/Sempron.md "wikilink")
    及最新的 [Phenom](../Page/Phenom.md "wikilink") 处理器
    ，支援[雙通道](../Page/雙通道.md "wikilink")[DDR2](../Page/DDR2.md "wikilink")（内置核心显卡，支援HT3.0等技術）。
  - [Socket AM3](../Page/Socket_AM3.md "wikilink") -
    [AthlonⅡ](../Page/AMD_Athlon_II.md "wikilink")、[PhenomⅡ](../Page/AMD_Phenom_II.md "wikilink")、[SempronⅡ處理器](../Page/AMD_Sempron處理器列表.md "wikilink")
    ，支援[雙通道](../Page/雙通道.md "wikilink")[DDR2](../Page/DDR2.md "wikilink")（向下兼容AM2+接口）。
  - [Socket AM3+](../Page/Socket_AM3+.md "wikilink") - [AMD
    FX系列](../Page/AMD_FX.md "wikilink") 向下兼容AM3接口。
  - [Socket FM1](../Page/Socket_FM1.md "wikilink") - [AMD
    APU系列](../Page/AMD_APU.md "wikilink") -
    [A4、A6、A8處理器](../Page/AMD加速处理器.md "wikilink")，核心代号是Llano，支援[雙通道](../Page/雙通道.md "wikilink")[DDR3
    1866内存](../Page/DDR3_SDRAM.md "wikilink")，内置核心显卡。
  - [Socket FM2](../Page/Socket_FM2.md "wikilink") - [AMD
    APU系列](../Page/AMD_APU.md "wikilink") -
    [A4、A6、A8、A10處理器](../Page/AMD加速处理器.md "wikilink"),
    核心代号是Trinity,支援[雙通道](../Page/雙通道.md "wikilink")[DDR3
    1866内存](../Page/DDR3_SDRAM.md "wikilink")，内置核心显卡,支持高清三联屏
    不兼容FM1接口。
  - [Socket AM4](../Page/Socket_AM4.md "wikilink") - [AMD
    Ryzen系列](../Page/AMD_Ryzen.md "wikilink") - [Ryzen
    7、](../Page/AMD_Ryzen.md "wikilink") [Ryzen
    5](../Page/AMD_Ryzen.md "wikilink") 、[Ryzen
    3](../Page/AMD_Ryzen.md "wikilink") 、
    [Athlon核心代号是Summit](../Page/AMD_Ryzen.md "wikilink")
    Bridge
    ，以及舊製程的[A6、A8、A10、A12處理器](../Page/AMD加速处理器.md "wikilink")，核心代號Bristol
    Ridge，不向下兼容[Socket AM3+](../Page/Socket_AM3+.md "wikilink") 和[Socket
    FM2](../Page/Socket_FM2.md "wikilink") 接口。

## 擴展插槽

[缩略图](https://zh.wikipedia.org/wiki/File:IIc_Plus_motherboard.JPG "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Motorola_Xoom_-_main_board-0119.jpg "fig:缩略图")的主機板\]\]
主板通常有數條插槽供扩展之用。AGP插槽用來接上顯示卡，現被PCI-Express
16x逐漸取代。PCI亦开始被PCI-Express取代。制造商根据逻辑芯片的限制来决定插槽的类型和数量。

通常有：

  - [ISA](../Page/工业标准结构.md "wikilink") -
    8/16-bit，舊制式扩展插槽，現無人使用，16-bit版相容於8-bit版。2004年後已被淘汰。
  - MCA - 32-bit，微通道，IBM特有技術，僅用於IBM PS/2系列個人電腦上。早已被淘汰。
  - [EISA](../Page/EISA.md "wikilink") - 32-bit，COMPAQ提出，相容8-bit
    ISA與16-bit ISA。
  - [Local Bus](../Page/系統匯流排.md "wikilink") -
    32-bit，初期無標準，各晶片組業者自訂自用，如OPTi、SiS皆有自訂版本。
  - [VLB](../Page/VLB.md "wikilink") - 32-bit，VESA訂立的Local
    Bus，是業者紊亂各自訂立Local Bus後的收斂標準。
  - [PCI](../Page/外设组件互连标准.md "wikilink") -
    32/64-bit，由Intel提出，是PC平台上首個真正做到通用、相容性好的匯流排介面，取代Local
    Bus、VLB之用，較常用，能接上电視卡、声效卡、網路卡。
  - [PCI-X](../Page/PCI-X.md "wikilink") - 64-bit，工作站、伺服器用
  - [AGP](../Page/AGP.md "wikilink") -
    32-bit，接上或升級顯示卡之用（上代），另有供電強化的AGP-Pro，64-bit版AGP雖有制訂的規格標準，但從未真正實現過。已被PCI-Express
    x16取代。
  - [PCI-Express](../Page/PCI-Express.md "wikilink")
    ，串列傳輸，1-bit～16-bit皆可，簡稱PCI-E或PCIe，與ISA、PCI等相同，皆為通用型擴充槽，即不限定連接擴充用途。PCIe
    x16/PCIe x 8主要由於連接顯示卡或者高階RAID卡。PCIe x4可連接高階NVMe SSD或者低階RAID卡。PCIe
    x1可連接低速網路卡、聲卡等。
  - AMR - Intel發起，用來連接軟體音效卡、軟體數據機（調變解調器）之用。
  - ACR - AMD發起，功用同AMR，期望取代AMR。
  - CNR - Intel再發起，功用同AMR，期望取代AMR。

## 可选的外部接口

这些全部都是可选的。

  - [IDE](../Page/高技術配置.md "wikilink") - 接駁硬碟、光碟機和軟碟機，已被淘汰，Intel Sandy
    Bridge及以後的主板都不提供IDE介面。
  - [SATA](../Page/SATA.md "wikilink") - 接駁硬碟和光碟機。
  - [SATA 2](../Page/SATA_2.md "wikilink") -
    同樣可接駁硬碟和光碟機，但速度比SATA快一倍，已被SATA
    3和SATA Express取代。
  - [SATA 3](../Page/SATA_3.md "wikilink") - Intel從7系列晶片組開始提供原生的SATA3支援。
  - [E-SATA](../Page/E-SATA.md "wikilink") - 接駁E-SATA介面的外置硬碟。
  - [PS/2](../Page/PS/2接口.md "wikilink") - 用來接駁鍵盤和滑鼠之用，正在被USB取代中。
  - [USB](../Page/USB.md "wikilink") - 接駁所有USB介面的裝置。
  - [IEEE 1394](../Page/IEEE_1394.md "wikilink") -
    用來接駁專業的數碼攝錄機，現已經已被淘汰，由[Thunderbolt取代](../Page/Thunderbolt.md "wikilink")。
  - [Thunderbolt](../Page/Thunderbolt.md "wikilink") -
    [Intel用來跟](../Page/Intel.md "wikilink")[USB競爭](../Page/USB.md "wikilink")，也能接數碼攝錄機，3代接頭改用[USB相同的結頭Type-C](../Page/USB_Type-C.md "wikilink")。
  - [M.2](../Page/M.2.md "wikilink") -
    主要用於連接基於[NVMe協定的SSD](../Page/NVMe.md "wikilink")，部分高階的NVMe
    SSD則是基於PCIe介面。
  - [藍牙](../Page/蓝牙.md "wikilink") - 無線接駁硬體、手機或內聯網。
  - [網路卡](../Page/网卡.md "wikilink") - 連接上網或內聯網。
  - [音效卡](../Page/声卡.md "wikilink") - 通常有三个，代表喇叭、麥克風和線路輸入。
  - 內建[顯示卡](../Page/顯示卡.md "wikilink") -
    [D型口或](../Page/D-Sub.md "wikilink")[DVI](../Page/DVI.md "wikilink")，[HDMI。](../Page/HDMI.md "wikilink")
  - [SCSI](../Page/SCSI.md "wikilink") - 接駁高階硬碟，已被SAS取代。
  - [SAS](../Page/串列SCSI.md "wikilink") -
    接駁高階硬碟，在伺服器十分常見，但價值較SCSI低，而且向下相容SATA介面硬碟。
  - [Fibre Channel](../Page/乙太網路光纖通道標準.md "wikilink") -
    接駁比SCSI、SAS更高階硬碟，價值較貴，是伺服器的頂級配備。
  - [RS232](../Page/RS232.md "wikilink") - 接駁舊式滑鼠、手寫板或其他工業電腦裝置。

## 主板規格

不同主機板規格有不同功用，所以大小也有不同。

  - XT（8.5 × 11"或216 × 279 mm）
  - [AT](../Page/AT規格.md "wikilink")（12 × 11"–13"或305 × 279–330 mm）
  - [Baby-AT](../Page/Baby-AT.md "wikilink")（8.5" × 10"–13"或216 mm ×
    254-330 mm）
  - EATX（12" × 13"或305mm × 330 mm）
  - [Mini ATX](../Page/Mini_ATX.md "wikilink")（11.2" × 8.2"或284 mm × 208
    mm）
  - [MicroATX](../Page/MicroATX.md "wikilink")（1996; 9.6" × 9.6"或244 mm
    × 244 mm）- 擴充插口比ATX規格少。
  - [Mini-ITX](../Page/Mini-ITX.md "wikilink") （170mm x 170mm）
  - LPX（9" × 11"–13"或229 mm × 279–330 mm）
  - picoBTX：主板最長203.20毫米，最多一個擴充卡插槽。
  - microBTX：主板最長264.16毫米，最多四個擴充卡插槽。
  - [ATX](../Page/ATX規格.md "wikilink")（12" × 9.6"或305 mm × 244 mm）
  - [BTX](../Page/BTX規格.md "wikilink")：主板最長325.12毫米，最多七個擴充卡插槽。
  - [DTX](../Page/DTX規格.md "wikilink")：主板尺寸為203 mm乘244 mm，最多兩個擴充卡插槽。

## 知名主機板生產商

[缩略图](https://zh.wikipedia.org/wiki/File:ASUS_MainBoard_with_CPU_and_CPU_cooler_and_Memories.JPG "fig:缩略图")

  - [宏碁](../Page/宏碁電腦.md "wikilink")（ACER）
  - [華碩](../Page/華碩電腦.md "wikilink")（ASUSTek）
  - [技嘉](../Page/技嘉科技.md "wikilink")（Gigabyte）
  - [微星](../Page/微星科技.md "wikilink")（MSI）
  - [七彩虹](../Page/世和资讯.md "wikilink")（Coloful）
  - [映泰](../Page/映泰集团.md "wikilink")（Biostar）
  - 昂达（Onda）
  - [英特尔](../Page/英特尔.md "wikilink") (Intel)
  - [超微](../Page/超微電腦.md "wikilink") (AMD)
  - [華擎](../Page/華擎科技.md "wikilink")（ASRock）
  - 精英（ECS）
  - [威盛電子](../Page/威盛電子.md "wikilink")（VIA）
  - [鴻海](../Page/鴻海.md "wikilink") (Foxcon)
  - [铭瑄](../Page/铭瑄科技.md "wikilink") (Maxsun)
  - 盈通（Yeston）
  - 磐英（EPoX）
  - 友通（DFI）
  - [建碁](../Page/建碁.md "wikilink")（Aopen）
  - 浩鑫（Shuttle）
  - 輝煌（Magic-Pro）
  - 大眾電腦（FIC）
  - [泰安電腦](../Page/泰安電腦.md "wikilink")（Tyan）
  - [超微電腦](../Page/Supermicro.md "wikilink")（Supermicro）
  - [艾維克](../Page/艾維克.md "wikilink")（EVGA）

## 参见

  - [BIOS](../Page/BIOS.md "wikilink")
  - [EFI](../Page/EFI.md "wikilink")
  - [晶片組](../Page/晶片組.md "wikilink")
  - [電子設計自動化](../Page/電子設計自動化.md "wikilink")

## 外部連結

  - [新浪网 - 追根究底
    常见主板品牌代工关系大揭秘](http://tech.sina.com.cn/h/2006-09-20/1034104819.shtml)
  - [我们的主板和显卡是如何给CPU和GPU供电的？](http://www.expreview.com/52325.html)
  - [主板供电全面解析](https://web.archive.org/web/20170413070851/http://www.pc426.com/article-68-1.html)

[Category:主板](../Category/主板.md "wikilink")
[Category:硬體](../Category/硬體.md "wikilink")