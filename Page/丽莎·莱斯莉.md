**丽莎·莱斯莉**（，），前[美国职业](../Page/美国.md "wikilink")[篮球运动员](../Page/篮球.md "wikilink")，曾效力于[WNBA](../Page/WNBA.md "wikilink")[洛杉矶火花队](../Page/洛杉矶火花队.md "wikilink")，司职[中锋](../Page/中锋.md "wikilink")，WNBA联盟创始球员之一，她是第一位在WNBA正式比赛中成功[扣篮的女篮运动员](../Page/扣篮.md "wikilink")，还曾效力过[意大利和](../Page/意大利.md "wikilink")[俄罗斯的职业女篮联赛](../Page/俄罗斯.md "wikilink")。

## 榮譽

  - 首名達到3,000分的WNBA球員
  - 1997年（平均每場9.5個）及1998年（平均每場10.2個）WNBA籃板球領導球員
  - 首名球員同一季內囊括常規賽、WNBA決賽及全明星賽最有價值球員
  - 2002年WNBA決賽及全明星賽最有價值球員
  - 四屆[奧運會](../Page/奧運會.md "wikilink")（1996、2000、2004、2008）[金牌成員](../Page/金牌.md "wikilink")
  - 首名WNBA球員在正式比赛中成功[扣篮](../Page/扣篮.md "wikilink")

## 海外联赛效力生涯

  - 1994-1995 :  阿尔卡莫体育俱乐部
  - 2005-2006 :  莫斯科斯巴达地区

## 家庭

2007年6月16日，莱斯莉生下了自己的第一个孩子，是位女孩，她的老公迈克·洛克伍德是一名飞行员。他们给自己的女儿起名叫劳伦·朱丽叶·洛克伍德。

## 参考资料

## 外部链接

  - [WNBA球员档案](https://web.archive.org/web/20070623052056/http://www.wnba.com/playerfile/lisa_leslie/index.html)
  - [WNBA chat
    transcript](http://www.wnba.com/sparks/news/leslie_chat_021011.html)
  - [美国奥运代表团中对莱斯莉的介绍](https://archive.is/20040820174859/http://www.usolympicteam.com/26_1100.htm)

[Category:非洲裔美國籃球運動員](../Category/非洲裔美國籃球運動員.md "wikilink")
[Category:美国女子篮球运动员](../Category/美国女子篮球运动员.md "wikilink")
[Category:美國奧林匹克運動會金牌得主](../Category/美國奧林匹克運動會金牌得主.md "wikilink")
[Category:1996年夏季奧林匹克運動會獎牌得主](../Category/1996年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:2000年夏季奧林匹克運動會金牌得主](../Category/2000年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會金牌得主](../Category/2004年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:2008年夏季奧林匹克運動會獎牌得主](../Category/2008年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:1996年夏季奥林匹克运动会篮球运动员](../Category/1996年夏季奥林匹克运动会篮球运动员.md "wikilink")
[Category:2000年夏季奧林匹克運動會籃球運動員](../Category/2000年夏季奧林匹克運動會籃球運動員.md "wikilink")
[Category:2004年夏季奧林匹克運動會籃球運動員](../Category/2004年夏季奧林匹克運動會籃球運動員.md "wikilink")
[Category:2008年夏季奧林匹克運動會籃球運動員](../Category/2008年夏季奧林匹克運動會籃球運動員.md "wikilink")
[Category:奧林匹克運動會籃球獎牌得主](../Category/奧林匹克運動會籃球獎牌得主.md "wikilink")
[Category:加州人](../Category/加州人.md "wikilink")