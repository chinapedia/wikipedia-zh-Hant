**久保带人**，是[日本男性](../Page/日本.md "wikilink")[漫画家](../Page/漫画家.md "wikilink")，[广岛县出身](../Page/广岛县.md "wikilink")\[1\]\[2\]。

## 簡歷

  - 1995年 - 高中3年級初次投稿作品時、擔任的責任編輯便很欣賞這位新人。
  - 1995年 - 『FIRE IN THE SKY』於最終選拔中被淘汰。
  - 1996年 - 《[週刊少年Jump](../Page/週刊少年Jump.md "wikilink")》增刊上發表出道作『ULTRA
    UNHOLY HEARTED MACHINE』。當時的筆名為「」。
  - 1996年 - 《週刊少年Jump》36號上發表『』。
  - 1997年 - 《週刊少年Jump》51號上發表『BAD SHIELD UNITED』。
  - 1999年 -
    《週刊少年Jump》34號上開始連載『ZOMBIEPOWDER.』（至2000年11號）。坐飛機[上京時](../Page/上京.md "wikilink")，於本作開始將筆名變更為「」。
  - 2000年 - 《[赤丸Jump](../Page/赤丸Jump.md "wikilink")》2001
    Winter上發表『BLEACH』的前身與prototype（原型）版。
  - 2001年 - 《週刊少年Jump》36・37合併號、開始連載『BLEACH』（至2016年38號）。
  - 2004年 -
    『BLEACH』[電視動畫化](../Page/電視動畫.md "wikilink")。（[東京電視台系](../Page/東京電視台.md "wikilink")）
  - 2005年 -
    『BLEACH』獲得第50回（[平成](../Page/平成.md "wikilink")16年度）[小學館漫畫賞少年漫畫部門的受獎](../Page/小學館漫畫賞.md "wikilink")。
  - 2006年 -
    『BLEACH』[劇場版化](../Page/劇場版.md "wikilink")。（[東寶系](../Page/東寶.md "wikilink")）

## 作品

### 漫畫

  - ZOMBIEPOWDER.（2000年、[Jump
    Comics](../Page/Jump_Comics.md "wikilink")（[集英社](../Page/集英社.md "wikilink")）、全4卷）
      - [ZOMBIEPOWDER.](../Page/ZOMBIEPOWDER..md "wikilink")（1999年－2000年連載、週刊少年Jump、全篇收錄）

      - ULTRA UNHOLY HEARTED MACHINE（1996年、週刊少年Jump增刊、久保宣章名義）

      - （1996年、週刊少年Jump、久保宣章名義）

      - BAD SHIELD UNITED（1997年、週刊少年Jump、久保宣章名義）
  - [BLEACH](../Page/BLEACH.md "wikilink")（2001年－2016年、週刊少年Jump連載）
  - BURN THE WITCH，短篇，刊於2018年週刊少年JUMP33號

### 插畫

  - （著：松原真琴）

### 畫集

  - All Colour But The Black All（2006年）

### 人物設定

  - 新櫻花大戰

## 参考

## 外部連結

  -
  - [BLEACH.com](http://www.j-bleach.com/) - BLEACH官方网站。

  - [BLEACH -POP WEB
    JUMP-](https://web.archive.org/web/20051001075658/http://jump.shueisha.co.jp/bleach/)
    - 少年JUMP周刊官方网站

  -
[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:廣島縣出身人物](../Category/廣島縣出身人物.md "wikilink")

1.
2.