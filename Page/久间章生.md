**久间章生**（），[日本](../Page/日本.md "wikilink")[政治家](../Page/政治家.md "wikilink")，[日本国会](../Page/日本国会.md "wikilink")[自民党籍](../Page/自由民主党_\(日本\).md "wikilink")[众议员](../Page/众议员.md "wikilink")，2013年退休，並受頒[旭日大綬章](../Page/旭日大綬章.md "wikilink")。

## 生平

1940年生于[九州](../Page/九州_\(日本\).md "wikilink")[长崎县](../Page/长崎县.md "wikilink")[南岛原市](../Page/南岛原市.md "wikilink")，1964年毕业于[东京大学法学部](../Page/东京大学.md "wikilink")，曾于1996年－1998年[桥本龙太郎执政时期任](../Page/桥本龙太郎.md "wikilink")[防卫厅长官](../Page/防卫厅.md "wikilink")，2006年9月出任[安倍晋三内阁防卫厅长官](../Page/安倍晋三.md "wikilink")。2007年1月[日本防卫厅升格为](../Page/日本防卫厅.md "wikilink")[防卫省后](../Page/日本防卫省.md "wikilink")，久间章生是首任[防卫大臣](../Page/日本防卫大臣.md "wikilink")。

2007年6月30日，久间章生在[千叶县](../Page/千叶县.md "wikilink")[柏市的](../Page/柏市.md "wikilink")[丽泽大学演讲时](../Page/丽泽大学.md "wikilink")，称“长崎遭美国[原子弹袭击后的确经历了悲惨的灾难](../Page/原子弹.md "wikilink")，但是[二战由此而宣告结束](../Page/第二次世界大战.md "wikilink")，从这个方面想，美国-{向}-日本投下原子弹是‘无奈之举’，而且美国投下原子弹有阻止[苏联参加对日作战的目的](../Page/苏联.md "wikilink")”\[1\]。此演讲立刻在日本朝野引起强烈批评和抗议。迫于国内各方面压力，久间章生于2007年7月3日宣布辞去防卫大臣一职\[2\]，7月5日正式離職，由被舆论称为“一枝花”的[小池百合子女士接任](../Page/小池百合子.md "wikilink")\[3\]。

## 参考来源

[Category:二戰後日本政治人物](../Category/二戰後日本政治人物.md "wikilink")
[Category:日本防衛大臣](../Category/日本防衛大臣.md "wikilink")
[Category:日本防衛廳長官](../Category/日本防衛廳長官.md "wikilink")
[Category:第二次橋本內閣閣僚](../Category/第二次橋本內閣閣僚.md "wikilink")
[Category:第一次安倍內閣閣僚](../Category/第一次安倍內閣閣僚.md "wikilink")
[Category:日本自由民主黨總務會長](../Category/日本自由民主黨總務會長.md "wikilink")
[Category:东京大学校友](../Category/东京大学校友.md "wikilink")
[Category:長崎縣出身人物](../Category/長崎縣出身人物.md "wikilink")
[Category:日本眾議院議員
1980–1983](../Category/日本眾議院議員_1980–1983.md "wikilink")
[Category:日本眾議院議員
1983–1986](../Category/日本眾議院議員_1983–1986.md "wikilink")
[Category:日本眾議院議員
1986–1990](../Category/日本眾議院議員_1986–1990.md "wikilink")
[Category:日本眾議院議員
1990–1993](../Category/日本眾議院議員_1990–1993.md "wikilink")
[Category:日本眾議院議員
1993–1996](../Category/日本眾議院議員_1993–1996.md "wikilink")
[Category:日本眾議院議員
1996–2000](../Category/日本眾議院議員_1996–2000.md "wikilink")
[Category:日本眾議院議員
2000–2003](../Category/日本眾議院議員_2000–2003.md "wikilink")
[Category:日本眾議院議員
2003–2005](../Category/日本眾議院議員_2003–2005.md "wikilink")
[Category:日本眾議院議員
2005–2009](../Category/日本眾議院議員_2005–2009.md "wikilink")
[Category:長崎縣選出日本眾議院議員](../Category/長崎縣選出日本眾議院議員.md "wikilink")

1.  [人民网2007年7月2日报道](http://world.people.com.cn/GB/1029/42354/5936219.html)
2.  [中新网2007年7月03日报道](http://news.163.com/07/0703/12/3IFQNS87000120GU.html)
3.  [新华网2007年7月4日报道](http://news.xinhuanet.com/video/2007-07/04/content_6326297.htm)