[UkraineNumbered.png](https://zh.wikipedia.org/wiki/File:UkraineNumbered.png "fig:UkraineNumbered.png")

**乌克兰行政区划**包括1个[自治共和国](../Page/自治共和国.md "wikilink")、2个[直辖市和](../Page/直辖市.md "wikilink")24个[州](../Page/州_\(烏克蘭\).md "wikilink")\[1\]\[2\]
，其中[克里米亚自治共和国和](../Page/克里米亚自治共和国.md "wikilink")[塞瓦斯托波尔市已于](../Page/塞瓦斯托波尔.md "wikilink")2014年宣布脱离乌克兰，但未获乌克兰承认。

## 一级行政区

<table>
<tbody>
<tr class="odd">
<td></td>
<td style="text-align: center;"><p><strong>名称</strong></p></td>
<td style="text-align: center;"><p><strong>首府</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>自治共和国</strong></p></td>
<td style="text-align: center;"><p>4</p></td>
<td style="text-align: center;"><p><a href="../Page/克里米亚自治共和国.md" title="wikilink">克里米亚自治共和国</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>直辖市</strong></p></td>
<td style="text-align: center;"><p> </p></td>
<td style="text-align: center;"><p><a href="../Page/基辅.md" title="wikilink">基辅</a></p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td style="text-align: center;"><p><a href="../Page/塞瓦斯托波尔.md" title="wikilink">塞瓦斯托波尔</a></p></td>
<td style="text-align: center;"><p> </p></td>
</tr>
<tr class="odd">
<td><p><strong>州</strong></p></td>
<td style="text-align: center;"><p>1</p></td>
<td style="text-align: center;"><p><a href="../Page/切尔卡瑟州.md" title="wikilink">切尔卡瑟州</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td style="text-align: center;"><p><a href="../Page/切尔尼戈夫州.md" title="wikilink">切尔尼戈夫州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/切尔尼戈夫.md" title="wikilink">切尔尼戈夫</a></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td style="text-align: center;"><p><a href="../Page/切尔诺夫策州.md" title="wikilink">切尔诺夫策州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/切尔诺夫策.md" title="wikilink">切尔诺夫策</a></p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td style="text-align: center;"><p><a href="../Page/第聂伯罗彼得罗夫斯克州.md" title="wikilink">第聂伯罗彼得罗夫斯克州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/第聂伯罗.md" title="wikilink">第聂伯罗</a></p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td style="text-align: center;"><p><a href="../Page/顿涅茨克州.md" title="wikilink">顿涅茨克州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/顿涅茨克.md" title="wikilink">顿涅茨克</a> （法律上）<br />
<a href="../Page/克拉馬托爾斯克.md" title="wikilink">克拉馬托爾斯克</a> （事實上）</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td style="text-align: center;"><p><a href="../Page/伊万诺-弗兰科夫斯克州.md" title="wikilink">伊万诺-弗兰科夫斯克州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/伊万诺-弗兰科夫斯克.md" title="wikilink">伊万诺-弗兰科夫斯克</a></p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td style="text-align: center;"><p><a href="../Page/哈尔科夫州.md" title="wikilink">哈尔科夫州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/哈尔科夫.md" title="wikilink">哈尔科夫</a></p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td style="text-align: center;"><p><a href="../Page/赫尔松州.md" title="wikilink">赫尔松州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/赫尔松.md" title="wikilink">赫尔松</a></p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td style="text-align: center;"><p><a href="../Page/赫梅利尼茨基州.md" title="wikilink">赫梅利尼茨基州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/赫梅利尼茨基.md" title="wikilink">赫梅利尼茨基</a></p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td style="text-align: center;"><p><a href="../Page/基洛沃格勒州.md" title="wikilink">基洛沃格勒州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/克洛佩夫尼茨基.md" title="wikilink">克洛佩夫尼茨基</a></p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td style="text-align: center;"><p><a href="../Page/基辅州.md" title="wikilink">基辅州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/基辅.md" title="wikilink">基辅</a></p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td style="text-align: center;"><p><a href="../Page/卢甘斯克州.md" title="wikilink">卢甘斯克州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/卢甘斯克.md" title="wikilink">卢甘斯克</a> （法律上）<br />
<a href="../Page/北顿涅茨克.md" title="wikilink">北顿涅茨克</a> （事實上）</p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td style="text-align: center;"><p><a href="../Page/利沃夫州.md" title="wikilink">利沃夫州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/利沃夫.md" title="wikilink">利沃夫</a></p></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td style="text-align: center;"><p><a href="../Page/尼古拉耶夫州.md" title="wikilink">尼古拉耶夫州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/尼古拉耶夫.md" title="wikilink">尼古拉耶夫</a></p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td style="text-align: center;"><p><a href="../Page/敖德萨州.md" title="wikilink">敖德萨州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/敖德萨.md" title="wikilink">敖德萨</a></p></td>
</tr>
<tr class="even">
<td><p>17</p></td>
<td style="text-align: center;"><p><a href="../Page/波尔塔瓦州.md" title="wikilink">波尔塔瓦州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/波尔塔瓦.md" title="wikilink">波尔塔瓦</a></p></td>
</tr>
<tr class="odd">
<td><p>18</p></td>
<td style="text-align: center;"><p><a href="../Page/罗夫诺州.md" title="wikilink">罗夫诺州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/罗夫诺.md" title="wikilink">罗夫诺</a></p></td>
</tr>
<tr class="even">
<td><p>19</p></td>
<td style="text-align: center;"><p><a href="../Page/苏梅州.md" title="wikilink">苏梅州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/苏梅.md" title="wikilink">苏梅</a></p></td>
</tr>
<tr class="odd">
<td><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/捷尔诺波尔州.md" title="wikilink">捷尔诺波尔州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/捷尔诺波尔.md" title="wikilink">捷尔诺波尔</a></p></td>
</tr>
<tr class="even">
<td><p>21</p></td>
<td style="text-align: center;"><p><a href="../Page/文尼察州.md" title="wikilink">文尼察州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/文尼察.md" title="wikilink">文尼察</a></p></td>
</tr>
<tr class="odd">
<td><p>22</p></td>
<td style="text-align: center;"><p><a href="../Page/沃伦州.md" title="wikilink">沃伦州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/卢茨克.md" title="wikilink">卢茨克</a></p></td>
</tr>
<tr class="even">
<td><p>23</p></td>
<td style="text-align: center;"><p><a href="../Page/外喀尔巴阡州.md" title="wikilink">外喀尔巴阡州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/乌日霍罗德.md" title="wikilink">乌日霍罗德</a></p></td>
</tr>
<tr class="odd">
<td><p>24</p></td>
<td style="text-align: center;"><p><a href="../Page/扎波罗热州.md" title="wikilink">扎波罗热州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/扎波罗热.md" title="wikilink">扎波罗热</a></p></td>
</tr>
<tr class="even">
<td><p>25</p></td>
<td style="text-align: center;"><p><a href="../Page/日托米尔州.md" title="wikilink">日托米尔州</a></p></td>
<td style="text-align: center;"><p><a href="../Page/日托米尔州.md" title="wikilink">日托米尔</a></p></td>
</tr>
</tbody>
</table>

## 註解

## 参考文献

## 外部連結

  -
  -
  -
  -
{{-}}

[U](../Category/欧洲国家行政区划.md "wikilink")
[乌克兰行政区划](../Category/乌克兰行政区划.md "wikilink")

1.  [Regions of Ukraine and their
    composition](http://w1.c1.rada.gov.ua/pls/z7502/a002) . [Verkhovna
    Rada](../Page/Verkhovna_Rada.md "wikilink") website.
2.  [Politics and society in
    Ukraine](https://books.google.com/books?id=zm0WAQAAIAAJ&q=Ukraine+27+regions:+24+oblasts,+autonomous+republic+cities+with+special+status&dq=Ukraine+27+regions:+24+oblasts,+autonomous+republic+cities+with+special+status&hl=nl&sa=X&ei=Rr_VUrrHGKSr0QXB-4HgDg&ved=0CIEBEOgBMAk)
    by [Paul D'Anieri](../Page/Paul_D'Anieri.md "wikilink"), [Robert
    Kravchuk](../Page/Robert_Kravchuk.md "wikilink"), and [Taras
    Kuzio](../Page/Taras_Kuzio.md "wikilink"), [Westview
    Press](../Page/Westview_Press.md "wikilink"), 1999,  (page 292)