**乌头属**又名**附子属**（[学名](../Page/学名.md "wikilink")：**）是一种属于[毛茛科的开花植物的](../Page/毛茛科.md "wikilink")[属](../Page/属_\(生物\).md "wikilink")，这个属有250多个[种](../Page/种_\(生物\).md "wikilink")。又名**萴**（音同“仄”）。

这些[多年生植物主要生活在北半球山地潮湿的草地上](../Page/多年生植物.md "wikilink")。它们的[叶子呈深绿色](../Page/叶子.md "wikilink")，没有托叶。叶子呈掌状，分五至七瓣。每瓣又分三片，边缘粗糙锋利。叶子在茎上交错或者螺旋排列，下部的叶子有长的柄。

乌头属植物非常美观，在高大直立的主干的顶端有一簇[总状花序的蓝色的](../Page/总状花序.md "wikilink")、紫色的、白色的、黄色的或者粉色的两侧对称的、拥有许多[雄蕊的](../Page/雄蕊.md "wikilink")[花](../Page/花.md "wikilink")。其五片[花萼中的一片形似一个圆筒状的帽子](../Page/花萼.md "wikilink")。每朵花有二至十枚蜜腺状的[花瓣](../Page/花瓣.md "wikilink")。最上面的两个花瓣很大，它们位于帽状的花萼下，有很长的柄。它们的顶端有一个空的针，里面含有[花蜜](../Page/花蜜.md "wikilink")。其它花瓣小或者完全退化了。三至五片[心皮在底部融在一起](../Page/心皮.md "wikilink")。

[果实为](../Page/果实.md "wikilink")[蓇葖果](../Page/蓇葖果.md "wikilink")。

乌头属的学名*Aconitum*可能来自小亚细亚的一座山的名字。在[希腊神话中传说](../Page/希腊神话.md "wikilink")[海格力斯将冥界](../Page/海格力斯.md "wikilink")[哈得斯的三頭犬](../Page/哈得斯.md "wikilink")[刻耳柏洛斯捕到地面上](../Page/刻耳柏洛斯.md "wikilink")。这条恶狗的唾液淌到的地方就长出了乌头。

## 普及

乌头属植物是[第三纪时的极地植物](../Page/第三纪.md "wikilink")，随[冰川期从](../Page/冰川期.md "wikilink")[西伯利亚传播到欧洲](../Page/西伯利亚.md "wikilink")、亚洲和美洲。

## 传粉

传播花粉的昆虫不易接近乌头属的花蜜，只有有很长的虹吸管的[大黄蜂才能获得它的花蜜](../Page/大黄蜂.md "wikilink")，因此两者之间互相配合发展，以至于两者的分布地区一致。

## 药用和毒性

《[本草纲目](../Page/本草纲目.md "wikilink")》记载，乌头“辛、温、有大毒”。乌头属毒性非常高，它们也是非常重要的[药用植物](../Page/药用植物.md "wikilink")。其毒性主要来自[乌头碱](../Page/乌头碱.md "wikilink")。未受伤的[皮肤就已经可以吸收乌头碱](../Page/皮肤.md "wikilink")。皮肤敏感的人可以通过接触而[过敏](../Page/过敏.md "wikilink")。在[药学中主要使用乌头的](../Page/药学.md "wikilink")[根](../Page/根.md "wikilink")，这里的药性浓度非常高。依照2015年版《[中国药典](../Page/中国药典.md "wikilink")》之规范，未经[炮制的生草乌](../Page/炮制.md "wikilink")、生川乌“内服宜慎”；炮制后的乌头属药材，如需内服，应先煎、久煎，以减低毒性，每次用量不得超过3克；即便外用，也应控制药量。

由于乌头属的剧毒，中国政府严禁私人买卖未经炮制的乌头属药材（如草乌、川乌），[保健食品中不得添加乌头](../Page/保健食品.md "wikilink")。然而至今仍经常有擅自烹制乌头药膳或者制作乌头药酒，食用或饮用后导致中毒甚至死亡的案例。

## 种类

  - *Aconitum ajanense*
  - [两色乌头](../Page/两色乌头.md "wikilink")（*Aconitum albo-violaceum*）
  - *Aconitum altaicum*
  - [阿尔泰乌头](../Page/阿尔泰乌头.md "wikilink")（*Aconitum ambiguum*）
  - [狭盔乌头](../Page/狭盔乌头.md "wikilink")（*Aconitum angusticassidatum*）
  - [铁棒锤](../Page/铁棒锤.md "wikilink")（*Aconitum anthora*）
  - [拟黄花乌头](../Page/拟黄花乌头.md "wikilink")（*Aconitum anthoroideum*）
  - [空茎乌头](../Page/空茎乌头.md "wikilink")（*Aconitum apetalum*）
  - *Aconitum axilliflorum*
  - *Aconitum baburinii*
  - [贝加尔乌头](../Page/贝加尔乌头.md "wikilink")（*Aconitum baicalense*）
  - [细叶黄乌头](../Page/细叶黄乌头.md "wikilink")（*Aconitum barbatum*）
  - *Aconitum besserianum*
  - [二花乌头](../Page/二花乌头.md "wikilink")（*Aconitum biflorum*）
  - *Aconitum bucovinense*
      - *Aconitum burnatii* ssp. *burnatii*
  - [乌头](../Page/乌头.md "wikilink")（*Aconitum carmichaelii*）
  - *Aconitum charkeviczii*

[Aconitum-reclinatum01.jpg](https://zh.wikipedia.org/wiki/File:Aconitum-reclinatum01.jpg "fig:Aconitum-reclinatum01.jpg")

  - [展花乌头](../Page/展花乌头.md "wikilink")（*Aconitum chasmanthum*）
  - *Aconitum cochleare*
  - *Aconitum columbianum*
      - *Aconitum columbianum* ssp. *columbianum*
      - *Aconitum columbianum* ssp. *viviparum*
  - *Aconitum confertiflorum*
  - *Aconitum consanguineum*
  - [黄花乌头](../Page/黄花乌头.md "wikilink")（*Aconitum coreanum*）
  - *Aconitum crassifolium*
  - *Aconitum cymbulatum*
  - *Aconitum decipiens*
  - *Aconitum degenii* (syn,onym of *A. variegatum* ssp. *paniculatum*)
  - *Aconitum delphinifolium*
      - *Aconitum delphiniifolium* ssp. *chamissonianum*
      - *Aconitum delphiniifolium* ssp. *delphiniifolium*
      - *Aconitum delphiniifolium* ssp. *paradoxum*
  - *Aconitum desoulavyi*
  - *Aconitum eulophum*（异名*A. anthora*）
  - *[Aconitum ferox](../Page/Aconitum_ferox.md "wikilink")* : Indian
    Aconite
  - *Aconitum firmum*
  - [薄叶乌头](../Page/薄叶乌头.md "wikilink")（*Aconitum fischeri*）

[Aconitum-uncinatum01.jpg](https://zh.wikipedia.org/wiki/File:Aconitum-uncinatum01.jpg "fig:Aconitum-uncinatum01.jpg")

  - *Aconitum flerovii*
  - *Aconitum gigas*
  - *Aconitum gracile*（异名*A. variegatum* ssp. *variegatum*）
  - *Aconitum helenae*
  - *Aconitum hosteanum*
  - *Aconitum infectum*
  - *Aconitum jacquinii*（异名*A. anthora*）
  - [鸭绿乌头](../Page/鸭绿乌头.md "wikilink")（*Aconitum jaluense*）
  - *Aconitum jenisseense*
  - *Aconitum karafutense*
  - [多根乌头](../Page/多根乌头.md "wikilink")（*Aconitum karakolicum*）
  - [吉林乌头](../Page/吉林乌头.md "wikilink")（*Aconitum kirinense*）
  - *Aconitum krylovii*
  - *Aconitum kunasilense*
  - *Aconitum kurilense*
  - [北乌头](../Page/北乌头.md "wikilink")（*Aconitum kusnezoffii*）
  - *Aconitum kuzenevae*
  - *Aconitum lasiocarpum*（异名*A. variegatum* ssp. *valesiacum*）
  - [毛喉乌头](../Page/毛喉乌头.md "wikilink")（*Aconitum lasiostomum*）
  - [白喉乌头](../Page/白喉乌头.md "wikilink")（*Aconitum leucostomum*）
  - *Aconitum longiracemosum*
  - *Aconitum lycoctonum*

[Aconitum_napellus01.jpg](https://zh.wikipedia.org/wiki/File:Aconitum_napellus01.jpg "fig:Aconitum_napellus01.jpg")

  -   - *Aconitum lycoctonum* ssp. *lycoctonum*
      - *Aconitum lycoctonum* ssp. *neapolitanum*
      - *Aconitum lycoctonum* ssp. *vulparia*

  - [细叶乌头](../Page/细叶乌头.md "wikilink")（*Aconitum macrorhynchum*）

  - *Aconitum maximum*

  - *Aconitum miyabei*

  - *Aconitum moldavicum*

  - *Aconitum montibaicalense*

  - *Aconitum nanum*

  - [舟形烏頭](../Page/舟形烏頭.md "wikilink")／通常附子（*Aconitum napellus*）

      - *Aconitum napellus* ssp. *corsicum*
      - *Aconitum napellus* ssp. *lusitanicum*
      - *Aconitum napellus* ssp. *napellus*
      - *Aconitum napellus* var. *giganteum*
      - *Aconitum napellus* ssp. *vulgare*

  - *Aconitum nasutum*

  - *Aconitum nemorosum*（异名*A. anthora*）

  - [林地乌头](../Page/林地乌头.md "wikilink")（*Aconitum nemorum*）

  - *Aconitum neosachalinense*

  - *Aconitum noveboracense*

  - *Aconitum ochotense*

  - *Aconitum odontandrum*（异名*A. variegatum* ssp. *variegatum*）

  - *Aconitum orientale*

  - *Aconitum paniculatum*

      - *Aconitum paniculatum* ssp. pyrenaicum
      - *Aconitum paniculatum* ssp. variegatum

  - *Aconitum paradoxum*

  - *Aconitum pascoi*

  - *Aconitum pavlovae*

  - *Aconitum pilipes*

  - *Aconitum plicatum*

  - *Aconitum podolicum*

  - *Aconitum productum*

  - *Aconitum pseudanthora*（异名*A. anthora*）

  - *Aconitum pseudokusnezowii*

  - *Aconitum puchonroenicum*

  - [大苞乌头](../Page/大苞乌头.md "wikilink")（*Aconitum raddeanum*）

  - *Aconitum ranunculoides*

  - *Aconitum reclinatum*

  - *Aconitum rogoviczii*

  - *Aconitum romanicum*

  - [圆叶乌头](../Page/圆叶乌头.md "wikilink")（*Aconitum rotundifolium*）

  - *Aconitum rubicundum*

  - *Aconitum sachalinense*

  - *Aconitum sajanense*

  - *Aconitum saxatile*

  - [宽叶蔓乌头](../Page/宽叶蔓乌头.md "wikilink")（*Aconitum sczukinii*）

  - [紫花高乌头](../Page/紫花高乌头.md "wikilink")（*Aconitum septentrionale*）

  - *Aconitum seravschanicum*

  - *Aconitum sichotense*

  - [阿尔泰乌头](../Page/阿尔泰乌头.md "wikilink")（*Aconitum smirnovii*）

  - [华北乌头](../Page/华北乌头.md "wikilink")（*Aconitum soongaricum*）

  - *Aconitum stoloniferum*

  - *Aconitum stubendorffii*

  - *Aconitum subalpinum*

  - *Aconitum subglandulosum*

  - *Aconitum subvillosum*

  - *Aconitum sukaczevii*

  - *Aconitum taigicola*

  - [伊犁乌头](../Page/伊犁乌头.md "wikilink")（*Aconitum talassicum*）

  - [甘青乌头](../Page/甘青乌头.md "wikilink")（*Aconitum tanguticum*）

  - *Aconitum tauricum*

  - *Aconitum turczaninowii*

  - [草地乌头](../Page/草地乌头.md "wikilink")（*Aconitum umbrosum*）

  - *Aconitum uncinatum*

      - *Aconitum uncinatum* ssp. *muticum*
      - *Aconitum uncinatum* ssp. *uncinatum*

  - *Aconitum variegatum*

      - *Aconitum variegatum* ssp. *paniculatum*
      - *Aconitum variegatum* ssp. *pyrenaicum*
      - *Aconitum variegatum* ssp. *valesiacum*
      - *Aconitum variegatum* ssp. *variegatum*

  - [蔓乌头](../Page/蔓乌头.md "wikilink")（*Aconitum volubile*）

  - *Aconitum vulparia*（异名*A. lycoctonum* ssp. *vulparia*）

  - *Aconitum woroschilovii*

## 外部連結

  - [烏頭屬
    Aconitum](http://libproject.hkbu.edu.hk/was40/search?lang=ch&channelid=1288&searchword=parent_latin=Aconitum)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)
  - [川烏 Chuan
    Wu](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00752)
    中藥標本數據庫 (香港浸會大學中醫藥學院)
  - [生川烏 Sheng Chuan
    Wu](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00208)
    中藥標本數據庫 (香港浸會大學中醫藥學院)
  - [美沙烏頭堿
    Mesaconitine](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00066)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)
  - [烏頭堿
    Aconitine](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00067)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)
  - [海帕烏頭堿
    Hypaconitine](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00065)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

[Category:藥用植物](../Category/藥用植物.md "wikilink")
[乌头属](../Category/乌头属.md "wikilink")
[Category:有毒植物](../Category/有毒植物.md "wikilink")