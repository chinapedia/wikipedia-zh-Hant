**乌恰县**是[中国](../Page/中国.md "wikilink")[新疆维吾尔自治区](../Page/新疆维吾尔自治区.md "wikilink")[克孜勒苏柯尔克孜自治州所辖的一个](../Page/克孜勒苏柯尔克孜自治州.md "wikilink")[县](../Page/县.md "wikilink")，是中國通公路的領土最西端。

## 行政区划

下辖2个[镇](../Page/行政建制镇.md "wikilink")、9个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 時區

烏恰縣是全世界最西段使用[UTC+8的地區](../Page/UTC+8.md "wikilink")。當[中國標準時間](../Page/中國標準時間.md "wikilink")（同[北京時間](../Page/北京時間.md "wikilink")）的時鐘指向8時00分，烏恰縣[經度所在實際時間則仍為](../Page/經度.md "wikilink")4時55分。

## 参考文献

{{-}}

[克孜勒苏](../Category/新疆县份.md "wikilink")
[乌恰县](../Category/乌恰县.md "wikilink")
[县](../Category/克孜勒苏县市.md "wikilink")
[新](../Category/国家级贫困县.md "wikilink")