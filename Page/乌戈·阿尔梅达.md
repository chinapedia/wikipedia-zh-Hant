**乌戈·阿尔梅达**（****，）全名**乌戈·米格尔·佩雷拉·德·阿尔梅达**（），[葡萄牙足球运动员](../Page/葡萄牙.md "wikilink")。

## 生平

這名球員早年出身葡萄牙班霸球會[波圖](../Page/波爾圖足球俱樂部.md "wikilink")，但他一直未獲重用，並先後被外借到多支球會。2006年他加盟[德甲球會](../Page/德甲.md "wikilink")[云达不来梅](../Page/云达不来梅足球俱乐部.md "wikilink")，第一季他為球會上陣41場比賽，取得10個入球。2007/08年球季他於首12場聯賽射入7球，成為了球會核心之一。

2010年12月25日云达不来梅證實將合約尚餘半年的曉高·艾美達以200萬[歐元賣到](../Page/歐元.md "wikilink")[土超的](../Page/土超.md "wikilink")[比錫達斯](../Page/贝西克塔斯体操俱乐部.md "wikilink")\[1\]。

他於2004年首次代表國家隊上陣，但一直到2006年以後才獲得國家隊重用。他於[2008年歐洲國家盃外圍賽對](../Page/2008年歐洲國家盃.md "wikilink")[阿塞拜疆的比賽中射入](../Page/阿塞拜疆.md "wikilink")1球。到決賽週他更包括於23人大軍之名單中。

## 參考資料

## 外部链接

  - [uefa.com
    bio](http://www.uefa.com/competitions/ucl/players/player=58953/index.html)
  - [Career statistics at
    fussballdaten.de](http://www.fussballdaten.de/spieler/almeidahugo/)

[Category:葡萄牙足球运动员](../Category/葡萄牙足球运动员.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:波圖球員](../Category/波圖球員.md "wikilink")
[Category:利亞拿球員](../Category/利亞拿球員.md "wikilink")
[Category:博維斯塔球員](../Category/博維斯塔球員.md "wikilink")
[Category:雲達不來梅球員](../Category/雲達不來梅球員.md "wikilink")
[Category:比錫達斯球員](../Category/比錫達斯球員.md "wikilink")
[Category:切塞納球員](../Category/切塞納球員.md "wikilink")
[Category:古賓球員](../Category/古賓球員.md "wikilink")
[Category:马哈奇卡拉安日球员](../Category/马哈奇卡拉安日球员.md "wikilink")
[Category:漢諾威球員](../Category/漢諾威球員.md "wikilink")
[Category:葡超球員](../Category/葡超球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:土超球員](../Category/土超球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:俄超球員](../Category/俄超球員.md "wikilink")
[Category:德國外籍足球運動員](../Category/德國外籍足球運動員.md "wikilink")
[Category:土耳其外籍足球運動員](../Category/土耳其外籍足球運動員.md "wikilink")
[Category:義大利外籍足球運動員](../Category/義大利外籍足球運動員.md "wikilink")
[Category:俄羅斯外籍足球運動員](../Category/俄羅斯外籍足球運動員.md "wikilink")
[Category:2004年夏季奧林匹克運動會足球運動員](../Category/2004年夏季奧林匹克運動會足球運動員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")
[Category:2012年歐洲國家盃球員](../Category/2012年歐洲國家盃球員.md "wikilink")
[Category:2014年世界盃足球賽球員](../Category/2014年世界盃足球賽球員.md "wikilink")

1.