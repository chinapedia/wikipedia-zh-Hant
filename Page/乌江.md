**乌江**为[长江上游南岸最大支流](../Page/长江.md "wikilink")，也是流经[贵州省的最大河流](../Page/贵州省.md "wikilink")。于[重庆市](../Page/重庆市.md "wikilink")[涪陵区汇入长江](../Page/涪陵区.md "wikilink")，全长1037千米（其中贵州境内889千米），流域总面积为115747平方千米（其中贵州境内66807平方千米）。\[1\]\[2\]

## 水系

乌江有南北两源：南源[三岔河](../Page/三岔河.md "wikilink")，北源[六冲河](../Page/六冲河.md "wikilink")，习惯上以三岔河为乌江干流。

三岔河发源于贵州省[毕节地区](../Page/毕节地区.md "wikilink")[威宁县](../Page/威宁县.md "wikilink")，地处[乌蒙山东麓](../Page/乌蒙山.md "wikilink")；北源六冲河发源于贵州省[毕节地区](../Page/毕节地区.md "wikilink")[赫章县](../Page/赫章县.md "wikilink")[可乐乡](../Page/可乐彝族苗族乡.md "wikilink")。两者于[黔西县](../Page/黔西县.md "wikilink")、[织金县](../Page/织金县.md "wikilink")、[清镇市交界处汇入](../Page/清镇市.md "wikilink")[东风水库](../Page/东风水库.md "wikilink")，称**鸭池河**，[乌渡河汇入后称](../Page/乌渡河.md "wikilink")**六广河**，[乌江渡后称为乌江](../Page/乌江渡水库.md "wikilink")。\[3\]

乌江干流横穿贵州中部和东北部，于[重庆市](../Page/重庆市.md "wikilink")[涪陵区汇入长江](../Page/涪陵区.md "wikilink")。支流流经[湖北省](../Page/湖北省.md "wikilink")[恩施土家族苗族自治州](../Page/恩施土家族苗族自治州.md "wikilink")。

[乌江渡以上为上游](../Page/乌江渡水库.md "wikilink")，从乌江渡到贵州[沿河县城为中游](../Page/沿河县.md "wikilink")，从沿河到涪陵为下游。主要支流有[猫跳河](../Page/猫跳河.md "wikilink")、[清水江](../Page/清水江.md "wikilink")、[濯河](../Page/濯河.md "wikilink")、[洪渡河](../Page/洪渡河.md "wikilink")、[阿蓬江](../Page/阿蓬江.md "wikilink")、[芙蓉江等](../Page/芙蓉江.md "wikilink")。\[4\]

## 水库和水电开发

乌江总落差达2123.5米，河床平均[坡降达](../Page/坡降.md "wikilink")2.05‰，[水能蕴藏量丰富](../Page/水能.md "wikilink")。20世纪60年代即规划在乌江干流建立11个梯级电站：北源六冲河上的[洪家渡水电站](../Page/洪家渡水电站.md "wikilink")，南源三岔河上的[普定水电站](../Page/普定水电站.md "wikilink")、[引子渡水电站](../Page/引子渡水电站.md "wikilink")，干流上的[东风水电站](../Page/东风水电站.md "wikilink")、[索风营水电站](../Page/索风营水电站.md "wikilink")、[乌江渡水电站](../Page/乌江渡水电站.md "wikilink")、[构皮滩水电站](../Page/构皮滩水电站.md "wikilink")、[思林水电站](../Page/思林水电站.md "wikilink")、[沙沱水电站](../Page/沙沱水电站.md "wikilink")、[彭水水电站](../Page/彭水水电站.md "wikilink")、大溪口水电站（已因[三峡水库蓄水取消规划](../Page/三峡水库.md "wikilink")）。\[5\]

## 参考文献

[乌江水系](../Category/乌江水系.md "wikilink")
[Category:贵州水路运输](../Category/贵州水路运输.md "wikilink")
[Category:重庆水路运输](../Category/重庆水路运输.md "wikilink")

1.
2.
3.
4.  [乌江介绍](http://www.cqshua.com/show.asp?id=852)
5.