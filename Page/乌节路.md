[Orchard_Road_light_up_Xmas_2005.JPG](https://zh.wikipedia.org/wiki/File:Orchard_Road_light_up_Xmas_2005.JPG "fig:Orchard_Road_light_up_Xmas_2005.JPG")的夜色。\]\]
**烏節路**（，），是[新加坡的一條有名的購物區及](../Page/新加坡.md "wikilink")[商業](../Page/商業.md "wikilink")[大街](../Page/大街.md "wikilink")，位於[新加坡中區之內](../Page/新加坡中區.md "wikilink")。

馬路兩旁大興[商場和](../Page/商場.md "wikilink")[百貨公司林立](../Page/百貨公司.md "wikilink")，包括[義安城](../Page/義安城.md "wikilink")、[威士馬廣場](../Page/威士馬廣場.md "wikilink")、[愛雍．烏節](../Page/ION_Orchard.md "wikilink")、313@Somerset、Orchard
Central 及会德丰广场等。

烏節路英文名稱「Orchard Road」起源於舊時該路曾經是果園。

## 歷史

大約於[1830年代](../Page/1830年.md "wikilink")，烏節路曾經是果園，除了種植水果外，尚有[甘蔗](../Page/甘蔗.md "wikilink")、[胡椒](../Page/胡椒.md "wikilink")、[肉豆蔻之栽種](../Page/肉豆蔻.md "wikilink")。至[1860年代](../Page/1860年.md "wikilink")，種植園被大量平房所取代，並且有山路穿越。早於[1890年代](../Page/1890年.md "wikilink")，[暹羅國王](../Page/暹羅.md "wikilink")[朱拉隆功從泰國駐新加坡大使手中購下位於烏節路之物業](../Page/朱拉隆功.md "wikilink")，並於相鄰購置另兩個物業。三者成為日後[泰國駐新加坡大使館](../Page/泰國.md "wikilink")。

於二十世紀早期，[英國人曾評價烏節路住宅區環境](../Page/英國.md "wikilink")，可堪比英國[德文郡](../Page/德文郡.md "wikilink")。

## 交通

[新加坡地鐵南北線有三個車站途經烏節路](../Page/新加坡地鐵南北線.md "wikilink")，分別是：

  - [烏節地鐵站](../Page/烏節地鐵站.md "wikilink")
  - [索美塞地鐵站](../Page/索美塞地鐵站.md "wikilink")
  - [多美歌地鐵站](../Page/多美歌地鐵站.md "wikilink")

## 商場

  - [ION Orchard](../Page/ION_Orchard.md "wikilink")
  - [義安城](../Page/義安城.md "wikilink")
  - [伟乐坊](../Page/伟乐坊.md "wikilink")

## 酒店

  - [文華大酒店](../Page/文華大酒店.md "wikilink")

## 參考文獻

  - National Heritage Board (2002), *Singapore's 100 Historic Places*,
    Archipelago Press, ISBN 9814068233
  - Victor R Savage, Brenda S A Yeoh (2003), *Toponymics - A Study of
    Singapore Street Names*, Eastern Universities Press, ISBN 9812102051
  - RedDot Publishing Inc (2005), *The Official Map of Singapore*,
    RedDot Publishing Inc.

[Category:新加坡街道](../Category/新加坡街道.md "wikilink")
[Category:新加坡旅遊景點](../Category/新加坡旅遊景點.md "wikilink")