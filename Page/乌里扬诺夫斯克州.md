**乌里扬诺夫斯克州**（）是[俄羅斯聯邦主體之一](../Page/俄羅斯聯邦主體.md "wikilink")，屬[伏爾加聯邦管區](../Page/伏爾加聯邦管區.md "wikilink")。位於[東歐大草原北部邊緣](../Page/東歐大草原.md "wikilink")，[伏爾加河在中部流過](../Page/伏爾加河.md "wikilink")。面積37,300平方公里，人口1,382,811（2002年）。首府[乌里扬诺夫斯克位於伏爾加河岸](../Page/乌里扬诺夫斯克.md "wikilink")。成立於1943年1月19日，是[港口型經濟特區之一](../Page/港口型經濟特區.md "wikilink")。

工業中心為乌里扬诺夫斯克和[季米特洛夫格勒](../Page/季米特洛夫格勒_\(俄罗斯\).md "wikilink")。農業以畜牧業為主，種植業次之。

## 人口

乌里扬诺夫斯克州居民55％居住在两个人口在25,000以上的城市：[乌里扬诺夫斯克和](../Page/乌里扬诺夫斯克.md "wikilink")[季米特洛夫格勒](../Page/季米特洛夫格勒_\(俄罗斯\).md "wikilink")。

[俄羅斯人佔總人口](../Page/俄羅斯人.md "wikilink")72%，[韃靼人佔](../Page/韃靼人.md "wikilink")12%，[楚瓦什人佔](../Page/楚瓦什人.md "wikilink")8%，[莫爾多瓦人佔](../Page/莫爾多瓦人.md "wikilink")4%。

## 注释

## 参考文献

[Category:伏爾加聯邦管區](../Category/伏爾加聯邦管區.md "wikilink")
[Category:俄罗斯州份](../Category/俄罗斯州份.md "wikilink")