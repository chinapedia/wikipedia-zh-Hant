**乐昌市**是[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[韶关市下辖的一个](../Page/韶关市.md "wikilink")[县级市](../Page/县级市.md "wikilink")，位于韶关市北部，是广东省最北面的县级行政区，北与[湖南省](../Page/湖南省.md "wikilink")[宜章](../Page/宜章.md "wikilink")、[汝城两县交界](../Page/汝城.md "wikilink")。

## 历史

[夏](../Page/夏朝.md "wikilink")、[商朝为扬州之域](../Page/商朝.md "wikilink")，[春秋时属越](../Page/春秋.md "wikilink")，[战国时属楚](../Page/战国.md "wikilink")，[秦朝时属](../Page/秦朝.md "wikilink")[南海郡](../Page/南海郡.md "wikilink")。秦朝到兩漢，
曾有一座任囂城（
在樂昌市區西南貳里）508年，[南梁分](../Page/梁_\(南朝\).md "wikilink")[曲江县地设置](../Page/曲江县.md "wikilink")[梁化县](../Page/梁化县_\(广东\).md "wikilink")，（
梁化縣縣城在如今的樂昌市區）公元598年（[隋朝开皇十八年](../Page/隋朝.md "wikilink")）改名乐昌县，
根據祝鵬先生考證， 隋代到唐朝末年， 樂昌縣城位於如今的樂昌市區（ 舊時為樂昌縣城） 西南三里，

（ 地名舊縣城） 根據祝鵬編著的《 廣東省廣州市佛山地區韶關地區沿革地理 》， 樂昌縣城遷移到現金位置（
如今的樂昌市區）广东省于1993年8月30日提交《关于乐昌县撤县设市的请示》，1994年正式撤县设市。
是一个历史悠久的城市。

2014年，乐昌市委班子共九人含乐昌市委书记李维员、乐昌市委纪委书记莫剑锋因收受红包被查处。\[1\]

參考文獻： 《 廣東省廣州市佛山地區韶關地區沿革地理 》頁65， 67， 頁132-133（ 祝鵬編著， 學林出版社， 上海，
1984年10月）

## 地理

乐昌境内地貌主要分流水地貌和岩溶地貌两大类。地势中部和北面较高，向东西两侧递减，西部有大东山，中部有大瑶山、东北部有九峰山。全市山地占72%，[丘陵占](../Page/丘陵.md "wikilink")13.5%，[盆地](../Page/盆地.md "wikilink")[平原占](../Page/平原.md "wikilink")14.5%。

## 气候

乐昌地处中[亚热带](../Page/亚热带.md "wikilink")，具有山地[气候特征](../Page/气候.md "wikilink")。年均气温19.6℃，降雨量1500mm，[无霜期](../Page/无霜期.md "wikilink")304天。

## 行政区划

现辖：

  - 1街道：[乐城街道](../Page/乐城街道.md "wikilink")
  - 16镇：[北乡镇](../Page/北乡镇.md "wikilink") ·
    [九峰镇](../Page/九峰镇.md "wikilink") ·
    [廊田镇](../Page/廊田镇.md "wikilink") ·
    [长来镇](../Page/长来镇.md "wikilink") ·
    [梅花镇](../Page/梅花镇.md "wikilink") ·
    [三溪镇](../Page/三溪镇.md "wikilink") ·
    [坪石镇](../Page/坪石镇.md "wikilink") ·
    [黄圃镇](../Page/黄圃镇.md "wikilink") ·
    [五山镇](../Page/五山镇.md "wikilink") ·
    [两江镇](../Page/两江镇.md "wikilink") ·
    [沙坪镇](../Page/沙坪镇.md "wikilink") ·
    [云岩镇](../Page/云岩镇.md "wikilink") ·
    [秀水镇](../Page/秀水镇.md "wikilink") ·
    [大源镇](../Page/大源镇.md "wikilink") ·
    [庆云镇](../Page/庆云镇.md "wikilink") ·
    [白石镇](../Page/白石镇.md "wikilink")

## 参考文献

[乐昌市](../Category/乐昌市.md "wikilink")
[市](../Category/韶关区县市.md "wikilink")
[韶关市](../Category/广东县级市.md "wikilink")

1.  《[广东乐昌市11名市委常委9人涉贪腐](http://politics.people.com.cn/n/2015/0106/c1001-26330548.html)》