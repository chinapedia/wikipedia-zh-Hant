[Map_of_The_east_barbarian_0.png](https://zh.wikipedia.org/wiki/File:Map_of_The_east_barbarian_0.png "fig:Map_of_The_east_barbarian_0.png")諸国与乐浪郡位置。\]\]
[漢四郡.jpg](https://zh.wikipedia.org/wiki/File:漢四郡.jpg "fig:漢四郡.jpg")年间（前100年—前97年）辽东政治地图\]\]
[Ancient_Korea_Taihougun.png](https://zh.wikipedia.org/wiki/File:Ancient_Korea_Taihougun.png "fig:Ancient_Korea_Taihougun.png")
[Basket_from_Lo-lang.jpg](https://zh.wikipedia.org/wiki/File:Basket_from_Lo-lang.jpg "fig:Basket_from_Lo-lang.jpg")
**乐浪郡**（[諺文](../Page/諺文.md "wikilink")：낙랑군(南)<sup>Nangnang-gun</sup>/락랑군(北)<sup>Rangnang-gun</sup>），是[漢武帝在公元前](../Page/漢武帝.md "wikilink")108年设置的[朝鲜](../Page/朝鲜半岛.md "wikilink")[四郡之一](../Page/汉四郡.md "wikilink")，该郡的主体族群为韩人，自汉朝设立郡治之后，韩人的传统祭祀活动依然没有废止。\[1\]乐浪郡[治所在](../Page/治所.md "wikilink")[朝鲜县](../Page/朝鲜县.md "wikilink")（今[平壤](../Page/平壤.md "wikilink")[大同江南岸](../Page/大同江.md "wikilink")），管辖朝鲜半岛西北部，对朝鲜、[日本诸部落](../Page/日本.md "wikilink")\[2\]有很大的影响力。4世纪初，被[高句麗吞并](../Page/高句麗.md "wikilink")。

公元前109年，漢武帝派兵由水陸兩路進攻，滅[卫满朝鲜](../Page/卫满朝鲜.md "wikilink")，次年置为**樂浪**、[玄菟](../Page/玄菟郡.md "wikilink")、[臨屯](../Page/臨屯郡.md "wikilink")、[真番四郡](../Page/真番郡.md "wikilink")，属于[幽州](../Page/幽州.md "wikilink")。治所朝鲜县城是故卫氏朝鲜都城[王险城](../Page/王险城.md "wikilink")。

公元前82年，[汉昭帝罢临屯](../Page/汉昭帝.md "wikilink")、真番二郡入乐浪郡，以真番故县置南部都尉;次年[玄菟郡後為夷](../Page/玄菟郡.md "wikilink")[貊所侵](../Page/貊.md "wikilink")，徙郡於[高句骊县](../Page/高句骊县.md "wikilink")（今[辽宁省](../Page/辽宁省.md "wikilink")[新宾县西南](../Page/新宾县.md "wikilink")）西北，更以沃沮為縣，屬樂浪東部都尉(以岭东七县置东部都尉)。

[東漢末](../Page/東漢.md "wikilink")，[公孫康分屯有县以南荒地分置](../Page/公孫康.md "wikilink")[帶方郡](../Page/帶方郡.md "wikilink")，郡治在[帶方縣](../Page/帶方縣.md "wikilink")。辖境与从前乐浪南部都尉的管区相同。

## 高句麗與樂浪郡

《[三國史記](../Page/三國史記.md "wikilink")》記載公元37年[大武神王向鸭绿江南的乐浪郡发动进攻](../Page/大武神王.md "wikilink")，一度占据。\[3\]
七年后，光武帝派兵渡海收复了乐浪，阻止了高句丽的扩张。49年2月，[慕本王派遣将军袭击后汉的北平](../Page/慕本王.md "wikilink")、渔阳、上谷、太原等四个郡。辽东太守[蔡彤以恩义及信义向慕本王对质](../Page/蔡彤.md "wikilink")，并透过和亲使两国的关系得以回复。
据《三国史记》[东川王本纪记载](../Page/东川王.md "wikilink")，东川王二十一年（247年），“王以丸都城经乱不可复都，筑平壤城，移民及庙社。平壤者，本仙人[王俭之宅也](../Page/檀君.md "wikilink")，或云王之都王险。”\[4\]
不过《[晋书](../Page/晋书.md "wikilink")》卷一四《地理志》记载：“[咸宁二年](../Page/咸宁_\(西晋\).md "wikilink")（276年）十月，分昌黎、辽东、玄菟、带方、乐浪等郡国五置[平州](../Page/平州.md "wikilink")”，“乐浪郡，汉置，统县六，户三千七百”，“带方郡，[公孙度置](../Page/公孙度.md "wikilink")，统县七，户四千九百”。因此247年东川王所城的平壤应该為國內城的衛城，而不在大同江。
\[5\]
[西晋](../Page/西晋.md "wikilink")[八王之乱后](../Page/八王之乱.md "wikilink")，[中原大乱](../Page/中原.md "wikilink")，高句丽开始南下压迫乐浪郡。高句丽[美川王十二年](../Page/美川王.md "wikilink")（311年）夺取西安平县（位于今[鸭绿江口北岸](../Page/鸭绿江.md "wikilink")），切断了乐浪,
[带方二郡与辽东的联系](../Page/带方.md "wikilink")。《三国史记·高句丽本纪》记载，高句丽“侵乐浪郡，虏获男女二千余口。”；[故国原王四十一年](../Page/故国原王.md "wikilink")，“百济王率兵三万来攻平壤城”；[小兽林王七年](../Page/小兽林王.md "wikilink")“百济将兵三万来侵平壤”；[广开土王四年](../Page/广开土王.md "wikilink")，“与百济战于浿水之上”。这些都表明公元4世纪初高句丽已夺取了二郡，控制了大同江流域。\[6\]

313年初，据有乐浪，[带方二郡的](../Page/带方.md "wikilink")[张统因不堪长期孤军与](../Page/张统.md "wikilink")[高句丽](../Page/高句丽.md "wikilink")、[百濟作战而率千余家迁到](../Page/百濟.md "wikilink")[辽西投靠](../Page/辽西.md "wikilink")[慕容廆](../Page/慕容廆.md "wikilink")，慕容廆後为其在辽西侨置乐浪郡（《资治通鉴》卷八八，建兴元年条）。不过在乐浪故地发现的4世纪和5世纪初的带有晋朝年号和官衔的晋人墓铭和砖铭表明313年後到5世纪初，还有一定数量的晋人居住在朝鲜半岛北部并继续奉晋朝正朔，并未被高句丽或百濟降服。

前燕慕容氏是最后一次给高句丽以巨创的中国地方政权。342年冬，[慕容皝毁高句丽丸都城](../Page/慕容皝.md "wikilink")，不过并不能恢复中国以前对朝鲜半岛北部的控制，只好接受高句丽的臣服。
\[7\]
《资治通鉴》卷97载故国原王在前燕伐高句丽的次年“遣其弟称臣入朝于燕，贡珍异以千数”。迫于前燕压力，高句丽迁都到[平壤](../Page/平壤.md "wikilink")。\[8\]

371年，当时的百济世子[近仇首王率](../Page/近仇首王.md "wikilink")3万军队拿下樂浪并处死了高句丽故国原王，百济短期獲得樂浪地区。高句丽[好太王和](../Page/好太王.md "wikilink")[长寿王两代多次大败百济](../Page/长寿王.md "wikilink")，百济势力被逐出了乐浪郡。427年前后，百济继续争夺乐浪、带方两郡的控制權。

## 平壤與樂浪郡

在中国史籍中，“平壤”之名首见《[魏书](../Page/魏书.md "wikilink")·高句丽传》：‘[北魏太武帝](../Page/北魏太武帝.md "wikilink")[太延元年](../Page/太延.md "wikilink")（435年）遣员外散骑侍郎李敖拜琏（即[长寿王](../Page/长寿王.md "wikilink")）为都督辽海诸军事
征东将军 领护东夷中郎将 辽东郡开国公
高句丽王敖至其所居平壤城。’而同期南朝似乎記載高句麗的都城還在丸都山下。後隋唐史採用北史記載認為平壤就是漢朝樂浪郡。《[新唐书](../Page/新唐书.md "wikilink")·東夷》记载，“其君居平壤城，亦谓长安城，汉乐浪郡也.......随山屈缭为郛,南涯水,王筑宫其左,又有国内城,汉城,号别都。”

1935年到1937年，在今日[朝鲜民主主义人民共和国](../Page/朝鲜民主主义人民共和国.md "wikilink")[首都平壤的](../Page/首都.md "wikilink")[樂浪區域](../Page/樂浪區域.md "wikilink")，发现了乐浪郡城遗址和大约2000余座汉墓。在该遗迹中出土的[漆器十分精美](../Page/漆器.md "wikilink")，史称“乐浪漆器”。朝鮮民主主義人民共和國發掘的安鹤宫遗址被認為是平壤的早期建築。而吉林[集安的将军坟被認為是长寿王的陵墓即承认长寿王死后又归葬](../Page/集安.md "wikilink")“故国”，这是一种可能，尽管无文献记载。

另外还有一种可能，即长寿王迁都之平壤並非近日之平壤。427年，高句丽长寿王迁都平壤很有可能不是今日大同江畔的平壤，而是國內城衛城或者和國內同一个城。今日之平壤，為586年（[隋文帝](../Page/隋文帝.md "wikilink")[開皇六年](../Page/開皇.md "wikilink")）平原王遷都到最後一個首都長安城，新唐書認為長安城就是平壤即樂浪郡，於是國內城附近的平壤就與今日之平壤混淆。

## 參考資料

## 参见

  - [樂浪東部都尉](../Page/樂浪東部都尉.md "wikilink")
  - [樂浪南部都尉](../Page/樂浪南部都尉.md "wikilink")
  - [樂浪區域](../Page/樂浪區域.md "wikilink")
  - [汉四郡](../Page/汉四郡.md "wikilink")

{{-}}

[Category:汉朝的郡](../Category/汉朝的郡.md "wikilink")
[Category:曹魏的郡](../Category/曹魏的郡.md "wikilink")
[Category:西晋的郡](../Category/西晋的郡.md "wikilink")
[Category:漢四郡](../Category/漢四郡.md "wikilink")
[Category:朝鲜半岛古代的郡](../Category/朝鲜半岛古代的郡.md "wikilink")

1.  《[三国志](../Page/三国志.md "wikilink")》：「居韩地，自号韩王。其后绝灭，今韩人犹有奉其祭祀者。汉时属乐浪郡，四时朝谒。」

2.  《[漢書](../Page/漢書.md "wikilink")·地理志》：「樂浪海中有倭人，分為百餘國，以歲時來獻見云。」

3.  《三国史记》卷14：“二十年　王袭乐浪　灭之”

4.  [中国与朝鲜半岛关系史论](http://book.ifeng.com/section.php?book_id=512&id=35101)

5.
6.
7.
8.