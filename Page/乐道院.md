**乐道院**（**Courtyard of the Happy
Way**）是19世纪末和20世纪初**[美北长老会差会](../Page/美北长老会.md "wikilink")**牧师[狄乐播在中国建立的规模最大的一个传教基地](../Page/狄乐播.md "wikilink")，位于[山东省](../Page/山东省.md "wikilink")[潍县县城东南](../Page/潍县.md "wikilink")5里。乐道院集教堂、医院、学校、公园为一体，用以传教、办学和开办诊所。今天这里是[潍坊市广文中学和](../Page/潍坊市广文中学.md "wikilink")[潍坊市人民医院](../Page/潍坊市人民医院.md "wikilink")。1942年3月到1945年8月，这里还曾是日军关押大批英美侨民的著名的**潍县集中营（Weihsien
Internment
Camp）**。乐道院里一些欧洲风格的建筑依旧保存完好。2013年，潍县乐道院暨西方侨民集中营旧址列入第四批山东省文物保护单位。

## 历史

### 创建背景

[鸦片战争肇始](../Page/鸦片战争.md "wikilink")，随着一系列不平等条约的签订，封建专制的[大清帝国国门大开](../Page/大清帝国.md "wikilink")，西方国家取得了在华自由传教的权利。由此西方教会和文化势力由沿海向内陆逐步渗透。

### 早期历史

1881年，美北长老会传教士狄乐播和狄珍珠夫妇来到潍县，在东关虞河东岸兴建教会建筑群乐道院，包括教堂、学堂、诊所3部分。
到1900年6月25日被[义和团焚毁前](../Page/义和团.md "wikilink")，已经有楼房42间，平房136间。1902年，长老会用[庚子赔款](../Page/庚子赔款.md "wikilink")14万两重建乐道院。占地面积200亩。西半部是[文华中学](../Page/文华中学.md "wikilink")、[文美女中](../Page/文美女中.md "wikilink")（一度办过广文大学，[齐鲁大学的前身](../Page/齐鲁大学.md "wikilink")）和大教堂，东半部是医院。其中有[广文大学](../Page/广文大学.md "wikilink")，医院还附设了医护学校（今[潍坊卫生学校](../Page/潍坊卫生学校.md "wikilink")）。院内，两三层的样式各异的青砖红瓦的西式小洋楼错落有致，掩映在一片绿荫中。潍坊附近也是长老会在华传教最成功的地区，后来成立了3个教区：潍安区会、乐寿区会和昌潍区会。这一时期曾在这里生活的的名人包括《[时代周刊](../Page/時代_\(雜誌\).md "wikilink")》的创办人[亨利·路思义及其父亲](../Page/亨利·路思义.md "wikilink")[亨利·温特斯·路思义](../Page/亨利·溫特斯·路思義.md "wikilink")。

乐道院位于当时潍县东关门东南三里处虞河南岸，其旧址即在今山东潍坊二中及毗邻的潍坊市人民医院院内。潍坊二中前身即原乐道院所办学校——广文中学，而潍坊人民医院即是由当时的教会医院发展而来。当年的乐道院“占地200多亩，曾一度作为昌潍一带的教会、教育和医疗卫生中心。西方教士、教师、医务人员麇集在此活动，其场所很是显要，院内的钟楼为当时潍县城东部的标志性建筑物”。

### 潍县集中营

[Weixiancamp.jpg](https://zh.wikipedia.org/wiki/File:Weixiancamp.jpg "fig:Weixiancamp.jpg")
1937年7月7日“[卢沟桥事变](../Page/卢沟桥事变.md "wikilink")”后，日军于当年底占领胶东半岛，由于当时美国等对日本持中立态度，所以日军对乐道院基本不加干涉，因此先于潍县沦陷的烟台、青岛等地的外国侨民开始前来潍县乐道院避难，而此时院内的传教、教学和医务活动尚能正常开展。

1941年[太平洋战争爆发后](../Page/太平洋战争.md "wikilink")，美国政府将侨居[旧金山等地的日本侨民集中到](../Page/旧金山.md "wikilink")[洛杉矶附近的指定地区](../Page/洛杉矶.md "wikilink")，限制其自由。为了对美国的行为实施报复，日军将中国占领区内的美国、[英国](../Page/英国.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[澳大利亚](../Page/澳大利亚.md "wikilink")、[新西兰等敌国未来得及撤退的的旅华侨民关押隔离](../Page/新西兰.md "wikilink")。1942年3月，日本[宪兵占领乐道院](../Page/宪兵.md "wikilink")，将其改建为关押华北各地2008名西方侨民的[集中营](../Page/集中营.md "wikilink")。后来因为交换战俘释放了500人。在这里关押的西方人中包括不少知名人士：[华北神学院院长](../Page/华北神学院.md "wikilink")[赫士博士](../Page/赫士.md "wikilink")，1924年奥运会400米冠军英国人、天津新学中学教师李爱锐（[伊利克·里达尔](../Page/伊利克·里达尔.md "wikilink")，1945年2月21日死于集中营）、曾任[蒋介石顾问的美国人](../Page/蒋介石.md "wikilink")[雷震遠](../Page/雷震遠.md "wikilink")、齐鲁大学教务长[德位思](../Page/德位思.md "wikilink")、后任[美国驻华大使的](../Page/美国驻华大使.md "wikilink")[辅仁大学附中教师](../Page/辅仁大学.md "wikilink")[恒安石](../Page/恒安石.md "wikilink")（1921－2001年2月6日，1944年6月9日夜成功越狱）、後任[美國駐韓大使及台北美使館副館長的](../Page/美國駐韓大使.md "wikilink")[來天惠](../Page/來天惠.md "wikilink")（1943年獲釋）等。

被关押人员中包括327名儿童，大部分是[烟台芝罘学校](../Page/烟台芝罘学校.md "wikilink")（内地会传教士子弟学校）的学生。经过争取，孩子们得到在集中营上学的权利。在营中，芝罘学校的学生们一共毕业了三届。

1945年8月14日日本[无条件投降](../Page/无条件投降.md "wikilink")。1945年8月17日上午9时30分，[美军](../Page/美军.md "wikilink")「鸭子行动队」从[昆明驾驶](../Page/昆明.md "wikilink")[B-24轰炸机飞抵集中营](../Page/B-24轰炸机.md "wikilink")，解放了1500名难友。

2006年，潍县集中营陈列馆成立。

## 参考资料

### 引用

### 来源

  - [East Meets West 02 -- Weifang's Historic School and
    Hospital](https://web.archive.org/web/20131014173816/http://www.3us.com/thread-64360-1-1.html)

## 延伸閱讀

  -
  -
  -
  -
## 外部連結

  -
  - [Documents, paintings, sketches, and memories of survivors of
    Weixian](http://www.weihsien-paintings.org/)

  - [BBC Radio 4 Program on survivors of the Weixian Internment
    Camp](http://www.bbc.co.uk/programmes/b00rdwm5)

  - [This American Life. Episode 559: Captain's Log
    Jun 26, 2015](http://www.thisamericanlife.org/radio-archives/episode/559/captains-log?act=1)

[Category:潍坊建筑物](../Category/潍坊建筑物.md "wikilink")
[Category:潍坊历史](../Category/潍坊历史.md "wikilink")
[Category:日本战俘集中营](../Category/日本战俘集中营.md "wikilink")