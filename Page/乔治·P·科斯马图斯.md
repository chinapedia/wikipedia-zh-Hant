**乔治·P·科斯马图斯**（George P.
Cosmatos，）是一名希臘和義大利男導演。兒子[帕诺斯·科斯马图斯也是名導演](../Page/帕诺斯·科斯马图斯.md "wikilink")。

曾用名：George Pan Cosmatos / George Cosmatos / Yorgo Pan Cosmatos。

## 生平

[希腊](../Page/希腊.md "wikilink")[导演乔治](../Page/导演.md "wikilink")·P·科斯马图斯，1941年1月4日生于[意大利](../Page/意大利.md "wikilink")[佛罗伦萨](../Page/佛罗伦萨.md "wikilink")，2005年4月19日因[肺癌逝世于](../Page/肺癌.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")[维多利亚](../Page/維多利亞_\(不列顛哥倫比亞\).md "wikilink")。

科斯马图斯早年在[伦敦求学](../Page/伦敦.md "wikilink")，毕业后曾担任[奥托·普雷明格的巨作](../Page/奥托·普雷明格.md "wikilink")《[出埃及记](../Page/出埃及记_\(电影\).md "wikilink")》（Exodus,
1960）的助理导演，将这部[莱昂·尤瑞斯描绘](../Page/莱昂·尤瑞斯.md "wikilink")[以色列诞生的史诗搬上银幕](../Page/以色列.md "wikilink")。接着科斯马图斯参与了《[希腊人佐巴](../Page/希腊人佐巴.md "wikilink")》（Zorba
the Greek, 1964）的拍摄，他自己也在其中出演了一个脸上长粉刺的年轻人角色。

科斯马图斯在[埃及和](../Page/埃及.md "wikilink")[塞浦路斯长大](../Page/塞浦路斯.md "wikilink")，据说他可以说六种语言。

70年代，科斯马图斯导演了由[马塞洛·马斯楚安尼主演的](../Page/马塞洛·马斯楚安尼.md "wikilink")《[屠杀令](../Page/屠杀令.md "wikilink")》（Rappresaglia,
1973）和由[索菲亚·罗兰主演的](../Page/索菲亚·罗兰.md "wikilink")《[卡桑德拉大桥](../Page/卡桑德拉大桥.md "wikilink")》（Cassandra
Crossing,
1976）这两部电影，他在[意大利名声鹊起](../Page/意大利.md "wikilink")。1979年，科斯马图斯制作导演了[二战题材的](../Page/二战.md "wikilink")[动作片](../Page/动作片.md "wikilink")《[逃往雅典娜](../Page/逃往雅典娜.md "wikilink")》，这部电影集合了包括[罗杰·摩尔](../Page/罗杰·摩尔.md "wikilink")，[大卫·尼温](../Page/大卫·尼温.md "wikilink")，[特利·萨瓦拉斯](../Page/特利·萨瓦拉斯.md "wikilink")，[埃利奥特·古尔德和](../Page/埃利奥特·古尔德.md "wikilink")[歌迪亚·卡汀娜在内的全明星阵容](../Page/歌迪亚·卡汀娜.md "wikilink")，影片也大获成功。

科斯马图斯来到[好莱坞之后并不成功](../Page/好莱坞.md "wikilink")，1985年，他因为《[第一滴血2](../Page/第一滴血2.md "wikilink")》（Rambo:
First Blood Part II,
1985）而被提名[金酸莓奖最差导演](../Page/金酸莓奖.md "wikilink")，他和[史泰龙合作的另一部电影](../Page/史泰龙.md "wikilink")《[眼镜蛇](../Page/眼镜蛇_\(电影\).md "wikilink")》（Cobra,
1988）也恶评如潮。

1993年，科斯马图斯导演的《[墓碑镇](../Page/墓碑镇.md "wikilink")》（Tombstone,
1993）获得了评论界的一致认可，这部电影取材于[美国](../Page/美国.md "wikilink")[枪手](../Page/枪手.md "wikilink")[“医生”霍利德和](../Page/“医生”霍利德.md "wikilink")[怀亚特·厄普的传奇故事](../Page/怀亚特·厄普.md "wikilink")，特别是[方·基墨对霍利德的诠释大获好评](../Page/方·基墨.md "wikilink")，为他个人带来了[MTV电影奖最佳男主角的提名](../Page/MTV电影奖.md "wikilink")。

2005年4月19日，科斯马图斯因肺癌病逝，享壽64岁。

## 作品

### 导演

1.  [火线惊爆点](../Page/火线惊爆点.md "wikilink") Shadow Conspiracy (1997)
2.  [墓碑镇](../Page/墓碑镇.md "wikilink") Tombstone (1993)
3.  [烈血海底城](../Page/烈血海底城.md "wikilink") Leviathan (1989)
4.  [眼镜蛇](../Page/眼镜蛇_\(电影\).md "wikilink") Cobra (1986)
5.  [第一滴血續集](../Page/第一滴血續集.md "wikilink") Rambo: First Blood 2 (1985)
6.  [人鼠大战](../Page/人鼠大战.md "wikilink") Of Unknown Origin (1983)
7.  [逃往雅典娜](../Page/逃往雅典娜.md "wikilink") Escape to Athena (1979)
8.  [卡桑德拉大桥](../Page/卡桑德拉大桥.md "wikilink") The Cassandra Crossing (1976)
9.  [屠杀令](../Page/屠杀令.md "wikilink") Rappresaglia (1973)

### 编剧

1.  逃往雅典娜 Escape to Athena (1979)
2.  卡桑德拉大桥 The Cassandra Crossing (1976)

### 演员

1.  [希腊人佐巴](../Page/希腊人佐巴.md "wikilink") Zorba the Greek (1964)

## 外部链接

  -
[Category:1941年出生](../Category/1941年出生.md "wikilink")
[Category:2005年逝世](../Category/2005年逝世.md "wikilink")
[Category:意大利导演](../Category/意大利导演.md "wikilink")
[Category:希腊导演](../Category/希腊导演.md "wikilink")
[Category:罹患肺癌逝世者](../Category/罹患肺癌逝世者.md "wikilink")