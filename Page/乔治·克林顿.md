**乔治·克林顿**（**George
Clinton**，），[美国军人](../Page/美国.md "wikilink")、政治家，[民主共和党成员](../Page/民主共和党.md "wikilink")，曾任[纽约州州长](../Page/纽约州州长.md "wikilink")（1777年-1795年、1801年-1804年）和[美国副总统](../Page/美国副总统.md "wikilink")（1805年-1812年）。

下列縣名是以他的姓氏命名作為紀念。

  - [柯林頓縣 (紐約州)](../Page/柯林頓縣_\(紐約州\).md "wikilink")
  - [柯林頓縣 (俄亥俄州)](../Page/柯林頓縣_\(俄亥俄州\).md "wikilink")

## 外部链接

  - [Architect of the Capitol: George
    Clinton](http://www.aoc.gov/cc/art/nsh/clinton.cfm)
  - [An examination of the Clinton
    Lineage](http://freepages.genealogy.rootsweb.com/~wynkoop/webdocs/clinton.htm)
  - [Find-A-Grave profile for George
    Clinton](http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=209)

[Category:大陆军将领](../Category/大陆军将领.md "wikilink")
[Category:美国民主党共和党州长](../Category/美国民主党共和党州长.md "wikilink")
[Category:18世纪美国政治家](../Category/18世纪美国政治家.md "wikilink")
[Category:美國陸軍准將](../Category/美國陸軍准將.md "wikilink")