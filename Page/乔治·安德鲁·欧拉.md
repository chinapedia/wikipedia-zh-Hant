**乔治·安德鲁·欧拉**（、，），出生于[布达佩斯](../Page/布达佩斯.md "wikilink")，美籍[匈牙利化学家](../Page/匈牙利.md "wikilink")。他在[超强酸稳定](../Page/超强酸.md "wikilink")[碳正离子的研究中有杰出贡献](../Page/碳正离子.md "wikilink")。他曾获得1994年[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")，并在不久后获得[普利斯特理奖章](../Page/普利斯特理奖章.md "wikilink")——[美国化学会所颁发的最高荣誉](../Page/美国化学会.md "wikilink")。

## 生平

欧拉毕业后在[布达佩斯技术与经济大学任教](../Page/布达佩斯技术与经济大学.md "wikilink")。由于受到[匈牙利十月事件的影响](../Page/匈牙利十月事件.md "wikilink")，他和他的家人先后移居[英国和](../Page/英国.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")。他在加拿大时与另一位匈牙利化学家Stephen
J.
Kuhn加入了渥太华[萨尼亚的](../Page/萨尼亚.md "wikilink")[陶氏化工公司](../Page/陶氏化工.md "wikilink")，并在那时开始了对碳正离子的研究。1965年，他回到了[凯斯西储大学的学术部](../Page/凯斯西储大学.md "wikilink")，1977年来到了[南加州大学](../Page/南加州大学.md "wikilink")。欧拉在1971年成为了美国[归化公民](../Page/归化.md "wikilink")。

欧拉现在是[南加州大学的著名教授](../Page/南加州大学.md "wikilink")，并且是[Loker碳氢化合物研究机构](../Page/Loker碳氢化合物研究机构.md "wikilink")（Loker
Hydrocarbon Research
Institute）的主任。他在2005年写了一篇推广[甲醇经济的文章](../Page/甲醇经济.md "wikilink")。

## 贡献

他在搜寻稳定的[碳正离子时](../Page/碳正离子.md "wikilink")，发现了质子化的[甲烷可被](../Page/甲烷.md "wikilink")[超强酸](../Page/超强酸.md "wikilink")（如[FSO<sub>3</sub>H](../Page/氟磺酸.md "wikilink")-[SbF<sub>5</sub>](../Page/五氟化锑.md "wikilink")）所稳定。

  -
    CH<sub>4</sub> + H<sup>+</sup> → CH<sub>5</sub><sup>+</sup>

最近他的研究已经从[烃转移到](../Page/烃.md "wikilink")[甲醇经济上](../Page/甲醇经济.md "wikilink")。

## 参考资料

  - George A. Olah, Alain Goeppert, G.K. Surya Prakash, *Beyond Oil and
    Gas: The Methanol Economy*, Angewandte Chemie International Edition
    Volume 44, Issue 18, Pages 2636 - 2639, 2005
  - [诺贝尔化学奖官方网站](http://nobelprize.org/nobel_prizes/chemistry/laureates/1994/index.html)

## 外部链接

  - [甲醇作为替代燃料](http://www.npr.org/templates/story/story.php?storyId=5369301)
    与欧拉在[全国公共广播电台的讨论录音](../Page/全国公共广播电台.md "wikilink")
  - [我对碳正离子及其在化学中的角色的研究](http://www.nobelprize.org/nobel_prizes/chemistry/laureates/1994/olah-lecture.html)
    乔治·安德鲁·欧拉1994年12月8日所做的诺贝尔奖演讲

[Category:匈牙利化学家](../Category/匈牙利化学家.md "wikilink")
[Category:诺贝尔化学奖获得者](../Category/诺贝尔化学奖获得者.md "wikilink")
[Category:犹太科学家](../Category/犹太科学家.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:匈牙利猶太人](../Category/匈牙利猶太人.md "wikilink")
[Category:布達佩斯科技經濟大學校友](../Category/布達佩斯科技經濟大學校友.md "wikilink")
[Category:普里斯特利奖章获得者](../Category/普里斯特利奖章获得者.md "wikilink")
[Category:陶氏化学人物](../Category/陶氏化学人物.md "wikilink")