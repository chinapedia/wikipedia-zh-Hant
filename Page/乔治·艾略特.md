[George_Eliot_at_30_by_François_D'Albert_Durade.jpg](https://zh.wikipedia.org/wiki/File:George_Eliot_at_30_by_François_D'Albert_Durade.jpg "fig:George_Eliot_at_30_by_François_D'Albert_Durade.jpg")
**瑪麗·安妮·「瑪麗安」或「瑪麗安」艾凡斯**（**Mary Anne「Mary Ann」Or「Marian」
Evans**\[1\]，），筆名**喬治·艾略特**（**George
Eliot**），是一位[英國](../Page/英國.md "wikilink")[小説家](../Page/小説家.md "wikilink")。她的作品包括《佛羅斯河畔上的磨坊》（1860年）和《米德尔马契》（1871年－1872年）等。

## 作品

  - *Amos Barton*　艾摩斯・巴頓
  - *Adam Bede*　亞當・柏德
  - *The Mill on the Floss*　佛羅斯河畔上的磨坊
  - *Silas Marner*　織工馬南傳
  - *Middlemarch*　米德尔马契
  - *Daniel Deronda* 丹尼爾・德隆達
  - *Scenes of Clerical Life*教区生活场景 1858
  - 《[掀开面纱](../Page/掀开面纱.md "wikilink")》，1859年

## 注釋

<div class="references-small">

<references />

</div>

## 外部链接

  -
  -
  -
  -
  -
  -
  -
[Category:英国小说家](../Category/英国小说家.md "wikilink")
[Category:英格蘭散文家](../Category/英格蘭散文家.md "wikilink")
[Category:英格蘭翻譯家](../Category/英格蘭翻譯家.md "wikilink")
[Category:英格蘭哲學家](../Category/英格蘭哲學家.md "wikilink")
[Category:19世紀哲學家](../Category/19世紀哲學家.md "wikilink")
[Category:19世纪小说家](../Category/19世纪小说家.md "wikilink")
[Category:19世纪翻译家](../Category/19世纪翻译家.md "wikilink")
[Category:使用筆名的作家](../Category/使用筆名的作家.md "wikilink")
[Category:英格蘭懷疑論者](../Category/英格蘭懷疑論者.md "wikilink")
[Category:華威郡人](../Category/華威郡人.md "wikilink")
[Category:威爾斯裔英格蘭人](../Category/威爾斯裔英格蘭人.md "wikilink")
[Category:皇家哈洛威學院校友](../Category/皇家哈洛威學院校友.md "wikilink")

1.  她出生時的受洗名為「Mary Anne」〔瑪麗·安妮〕，於1857年更改為「Mary
    Ann」（瑪麗安），又於1859年更改為「Marian」（瑪麗安），又於1880年恢復為「Mary
    Ann」（瑪麗安）。