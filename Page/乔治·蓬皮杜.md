**喬治·尚·雷蒙·龐畢度**（，，[法語](../Page/法語.md "wikilink")[發音](../Page/國際音標.md "wikilink")），[法國政治家](../Page/法國.md "wikilink")，前任[法國總統](../Page/法國總統.md "wikilink")。

## 生平

1911年，出生于法国中部康塔勒省蒙布迪夫市的小学教员家庭。

1934年，毕业于巴黎高等师范学校。

蓬皮杜從政前曾是一位教師，在[马赛和](../Page/马赛.md "wikilink")[巴黎等地任教](../Page/巴黎.md "wikilink")，[二戰結束後出任](../Page/第二次世界大战.md "wikilink")[國會議員](../Page/法國國會.md "wikilink")。

1944年至1946年任[戴高乐临时政府办公厅专员](../Page/戴高乐.md "wikilink")，1946年至1949年出任旅遊部長，1954年至1958年任罗思柴尔德銀行行長。

## 總理

1958年[戴高樂重新上台後](../Page/戴高樂.md "wikilink")，加入他的內閣任職。1962年4月16日獲戴高樂任命為[總理](../Page/總理.md "wikilink")，1968年7月10日辭職，其後被戴高樂選定為接班人。

## 總統

1969年6月15日，龐比度得到58.22%的高票當選成為[法國總統](../Page/法國總統.md "wikilink")，执政期间，繼續奉行[戴高乐的法兰西民族独立政策](../Page/戴高乐.md "wikilink")。

### 任內去世

1974年4月2日因[卡勒氏病逝世](../Page/多發性骨髓瘤.md "wikilink")。

## 作品

  - 《Anthologie de la Poésie Française, Livre de Poche/Hachette》， 1961年
  - 《Le Nœud gordien, éd. Plon》， 1974年
  - 《Entretiens et discours, deux vol., éd. Plon》，1975年
  - 《Pour rétablir une vérité, éd. Flammarion》， 1982年

## 參看

  - [蓬皮杜艺术中心](../Page/蓬皮杜艺术中心.md "wikilink")

[Category:法国总统](../Category/法国总统.md "wikilink")
[Category:巴黎高等師範學院校友](../Category/巴黎高等師範學院校友.md "wikilink")
[Category:巴黎政治大学校友](../Category/巴黎政治大学校友.md "wikilink")
[Category:路易大帝中学校友](../Category/路易大帝中学校友.md "wikilink")