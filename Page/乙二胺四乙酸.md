**乙二胺四乙酸**（），常缩写为**EDTA**，是一种[有机化合物](../Page/有机化合物.md "wikilink")。它是一個六齿[配體](../Page/配體_\(化學\).md "wikilink")，可以螯著多種[金屬](../Page/金屬.md "wikilink")[離子](../Page/離子.md "wikilink")。它的4個[酸和](../Page/羧酸.md "wikilink")2個[胺的部分都可作為配體的齿](../Page/胺.md "wikilink")，與[錳](../Page/錳.md "wikilink")（II）、[銅](../Page/銅.md "wikilink")（II）、[鐵](../Page/鐵.md "wikilink")（III）及[鈷](../Page/鈷.md "wikilink")（II）等金屬離子組成[螯合物](../Page/螯合物.md "wikilink")。\[1\]

## 名稱問題

[化學家曾經用多種不同的字來形容EDTA](../Page/化學家.md "wikilink")，例如將[配體本身叫做EDTA](../Page/配體_\(化學\).md "wikilink")<sup>4−</sup>，而它的[共軛酸叫做H](../Page/共軛酸.md "wikilink")<sub>4</sub>EDTA。

## 配合物

[Metal-EDTA.png](https://zh.wikipedia.org/wiki/File:Metal-EDTA.png "fig:Metal-EDTA.png")\]\]

EDTA
和其他化合物如H<sub>2</sub>IDA（亞胺基二乙酸）和H<sub>3</sub>NTA（氨三乙酸）等屬於同類的[螯合物](../Page/螯合物.md "wikilink")，這些配體都是由一種叫做[甘氨酸的](../Page/甘氨酸.md "wikilink")[氨基酸製成的](../Page/氨基酸.md "wikilink")。EDTA类的螯合物都極易溶於水。這些配體都为三或四齿配体，而合成出來的螯合物都是[光學異構物](../Page/光學異構物.md "wikilink")。\[2\]

## 合成

1935年，費迪南德·明茨使用[乙二胺和](../Page/乙二胺.md "wikilink")[氯乙酸來首次製備合成](../Page/氯乙酸.md "wikilink")。\[3\]
現今合成
EDTA主要用[乙二胺與](../Page/乙二胺.md "wikilink")[甲醛及](../Page/甲醛.md "wikilink")[氰化鈉合成](../Page/氰化鈉.md "wikilink")。\[4\]

  -
    H<sub>2</sub>NCH<sub>2</sub>CH<sub>2</sub>NH<sub>2</sub> + 4
    CH<sub>2</sub>O + 4 NaCN + 4 H<sub>2</sub>O →
    (NaO<sub>2</sub>CCH<sub>2</sub>)<sub>2</sub>NCH<sub>2</sub>CH<sub>2</sub>N(CH<sub>2</sub>CO<sub>2</sub>Na)<sub>2</sub>
    + 4 NH<sub>3</sub>

<!-- end list -->

  -
    (NaO<sub>2</sub>CCH<sub>2</sub>)<sub>2</sub>NCH<sub>2</sub>CH<sub>2</sub>N(CH<sub>2</sub>CO<sub>2</sub>Na)<sub>2</sub>
    + 4 HCl →
    (HO<sub>2</sub>CCH<sub>2</sub>)<sub>2</sub>NCH<sub>2</sub>CH<sub>2</sub>N(CH<sub>2</sub>CO<sub>2</sub>H)<sub>2</sub>
    + 4 NaCl

## 用途

在1999年，歐洲EDTA年消耗量為35,000噸，在美國為50,000噸，其重要的用途如下：

### EDTA-2Na

「EDTA-2Na」的中文化學名為「**乙二胺四乙酸二鈉**」（ethylenediaminetetraacetic acid disodium
salt dihydrate），有工業用及食用兩類，兩者化學成分相同，但濃度及容许杂质上有所区別。EDTA-2Na
用途非常廣泛，它是抗氧化劑之一，具有螯合金屬離子的特質，意思是，就像螃蟹的螯夾住金屬離子，因此經常被運用在工業上，像用來清理鍋爐，有時也被用來治療汞中毒，可作為食品、工業與醫療用途。\[5\]
\[6\]

#### 工業級

  - [工業](../Page/工業.md "wikilink")：清理[重金屬離子及Ca](../Page/重金屬.md "wikilink")<sup>2+</sup>和Mg<sup>2+</sup>離子。
  - [肥皂](../Page/肥皂.md "wikilink")：與[硬水中的Ca](../Page/硬水.md "wikilink")<sup>2+</sup>和Mg<sup>2+</sup>離子結合來降低硬度。
  - [攝影](../Page/攝影.md "wikilink")：以Fe(III)EDTA作為氧化劑。
  - [紡織品](../Page/紡織品.md "wikilink")：與重金屬結合。
  - [油生產](../Page/油.md "wikilink")：加上EDTA 來防止礦物沈澱。
  - [牛奶](../Page/牛奶.md "wikilink")：清洗牛奶瓶。
  - [氮氧化物](../Page/氮氧化物.md "wikilink")：廢氣處理。

#### 食品級

食品級的EDTA純度必在99％以上，是廣泛被核准的食品添加物
(如美國FDA)，屬抗氧化劑與品質改良劑，有防止油脂氧化、食品褐變及乳化食品等功用。\[7\]\[8\]

  - [食物](../Page/食物.md "wikilink")：作為[抗氧化劑](../Page/抗氧化劑.md "wikilink")，其作用為螯合金屬離子，以避免油脂的氧化。
  - [化妝品](../Page/化妝品.md "wikilink")：加上EDTA 來保存化妝品。
  - [汽水](../Page/軟性飲料.md "wikilink")：EDTA可避免含有[抗壞血酸和](../Page/抗壞血酸.md "wikilink")[苯甲酸鈉的汽水產生](../Page/苯甲酸鈉.md "wikilink")[致癌物質](../Page/致癌物質.md "wikilink")[苯](../Page/苯.md "wikilink")。
  - [水的測試](../Page/水.md "wikilink")：測試水的[硬度](../Page/硬水.md "wikilink")。

#### 醫療級

醫療級的EDTA因質地純淨，可作為重金屬中毒的解毒劑。此外，抽血樣本加入EDTA後，可避免在送驗過程中凝集。\[9\]\[10\]

  - [生物](../Page/生物.md "wikilink")[医学](../Page/医学.md "wikilink")：
      - [汞毒治療](../Page/汞中毒.md "wikilink")：使用EDTA的二鈉鈣鹽-[乙二胺四乙酸二鈉鈣](../Page/乙二胺四乙酸二鈉鈣.md "wikilink")\[11\]（EDTA
        Na<sub>2</sub>-Ca）治療重金屬中毒。可治療汞中毒的人，也可治療鉛中毒的人。利用EDTA與重金屬結合的特性，生成穏定而可溶的鹽，隨尿液排出。
      - [检验医学](../Page/检验医学.md "wikilink")：作为[血液等液体类](../Page/血液.md "wikilink")[标本的](../Page/标本.md "wikilink")[抗凝剂](../Page/抗凝剂.md "wikilink")（如[全血细胞计数时所采用的血液标本](../Page/全血细胞计数.md "wikilink")）。
      - [牙醫學](../Page/牙醫學.md "wikilink")：在[根管治療術中用來清除一些有機或無機的物質](../Page/根管治療術.md "wikilink")。
      - [生物化學](../Page/生物化學.md "wikilink")：在[分子生物學中用EDTA](../Page/分子生物學.md "wikilink")
        來防止金屬離子對[酶的影响](../Page/酶.md "wikilink")\[12\] 。

## 檢測和分析方法

最靈敏檢測生物樣品中的EDTA的方法，是使用毛細管電泳質譜分析（簡稱SRM-CE/MS）。此方法在人的血漿中偵測極限為7.3 ng/mL
，最低定量濃度則為15 ng/mL。\[13\] 此種方法適用於樣品量小於7-8 nL。\[14\]

## 環境問題

EDTA
不能在[污水處理中被分解](../Page/污水處理.md "wikilink")，不過在控制著[pH值的情況下](../Page/pH值.md "wikilink")，EDTA經長時間後就可幾乎完全反應，屆時要將其他[微生物抽離](../Page/微生物.md "wikilink")，來讓EDTA完成結合過程。

在水中，太多或太少的螯合物都會影響[浮游生物和](../Page/浮游生物界.md "wikilink")[藻類的數量增減](../Page/藻類.md "wikilink")。

EDTA對細菌來說是有毒的，在[哺乳類動物的糞便中會破壞生物的](../Page/哺乳類.md "wikilink")[細胞膜](../Page/細胞膜.md "wikilink")。

## 其他事項

  - [麥當勞的食物曾經被化驗到含有EDTA](../Page/麥當勞.md "wikilink")。
  - [漢堡王](../Page/漢堡王.md "wikilink")（Burger
    King）在[台灣的連鎖店附送的沙拉醬](../Page/台灣.md "wikilink")，包裝標示含有「乙烯二胺四醋酸二鈉鈣」，是屬EDTA的鈣鈉鹽。
  - 2013年[臺灣查獲臺南](../Page/臺灣.md "wikilink")「立光農工業公司」(目前已改名為永詠股份有限公司)
    把工業用的乙二胺四乙酸二鈉（EDTA-2Na），配成原料複方，價格只有食用級原料的1/3，再以通常價格賣給[統一以及](../Page/統一企業.md "wikilink")[愛之味等大企業](../Page/愛之味.md "wikilink")，製作成寒天、豆花、布丁等相關產品。\[15\]
  - EDTA
    螯合物的[英文字全寫](../Page/英語.md "wikilink")「Ethylenediaminetetraacetates」是[北美版](../Page/北美洲.md "wikilink")[Scrabble](../Page/Scrabble.md "wikilink")
    遊戲之中最長的英文字。
  - 在[電影](../Page/電影.md "wikilink")《[幽靈刺客](../Page/幽靈刺客.md "wikilink")》和《幽靈刺客II》之中，劇中主角以EDTA作為對付吸血鬼的武器。

## 參考文獻

## 相關條目

  - [配體](../Page/配體_\(化學\).md "wikilink")
  - [螯合物](../Page/螯合物.md "wikilink")
  - [全血细胞计数](../Page/全血细胞计数.md "wikilink")
  - [2013年臺灣食品安全問題事件](../Page/2013年臺灣食品安全問題事件.md "wikilink")

## 外部連結

  - [pH-Spectrum of EDTA
    complexes](http://www.theoprax-research.com/pool.html)
  - [EDTA: Molecule of the
    Month](http://www.chm.bris.ac.uk/motm/edta/edtah.htm)
  - <https://web.archive.org/web/20061029103725/http://www.chem.utk.edu/~chem319/Experiments/Exp6.pdf>

[Category:螯合配体](../Category/螯合配体.md "wikilink")
[Category:防腐剂](../Category/防腐剂.md "wikilink")
[Category:解毒剂](../Category/解毒剂.md "wikilink")
[Category:乙酸](../Category/乙酸.md "wikilink")
[Category:胺](../Category/胺.md "wikilink")
[Category:世界卫生组织基本药物](../Category/世界卫生组织基本药物.md "wikilink")
[Category:照相药品](../Category/照相药品.md "wikilink")

1.  Holleman, A. F.; Wiberg, E. "Inorganic Chemistry" Academic Press:
    San Diego, 2001. ISBN 0-12-352651-5.

2.  Kirchner, S. Barium (Ethylenediaminetetracetato) Cobalt(III)
    4-Hydrate" Inorganic Syntheses, McGraw-Hill:
    [紐約](../Page/紐約.md "wikilink"), 1957年; 第5冊, 第186至188頁

3.  F. Münz "Polyamino carboxylic acids to [I. G.
    Farbenindustrie](../Page/IG_Farben.md "wikilink"), DE 718 981, 1935;
    US 2 130 505, 1938.

4.  [Synthesis of
    EDTA](http://www.chm.bris.ac.uk/motm/edta/synthesis_of_edta.htm)

5.  [EDTA-2Na小檔案](http://www.libertytimes.com.tw/2013/new/jun/1/today-fo3-2.htm)
     - 自由電子報，2013-6-1

6.  [新聞辭典／乙烯二胺四醋酸二鈉（EDTA-Na2） |
    食品安全亮紅燈](http://www.udn.com/2013/6/1/NEWS/NATIONAL/NATS10/7935945.shtml)
     - 聯合新聞網，2013-6-1

7.
8.
9.
10.
11. 常用藥物手冊-王世祥 周自永 中華書局(香港)有限公司

12.

13.

14.
15. [快訊／統一和愛之味誤用工業原料　依蕾特布丁誤用過期原料](http://www.ettoday.net/news/20130531/215191.htm)
    - ETtoday新聞， 2013年05月31日