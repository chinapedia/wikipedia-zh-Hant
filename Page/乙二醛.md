**乙二醛**是一个[有机化合物](../Page/有机化合物.md "wikilink")，[化学式为OCHCHO](../Page/化学式.md "wikilink")，由两个[醛基](../Page/醛基.md "wikilink")-C<sup>=O</sup><sub>-H</sub>相连。它是最简单的[二醛](../Page/二醛.md "wikilink")，室温下为黄色液体。

## 制取

工业上，乙二醛可由[乙二醇在](../Page/乙二醇.md "wikilink")[银或](../Page/银.md "wikilink")[铜催化下的气相氧化](../Page/铜.md "wikilink")，或用[硝酸溶液氧化](../Page/硝酸.md "wikilink")[乙醛制得](../Page/乙醛.md "wikilink")。实验室中，乙二醛是通过用[亚硒酸氧化](../Page/亚硒酸.md "wikilink")[乙醛制取](../Page/乙醛.md "wikilink")。\[1\]无水乙二醛可由固态水合物与[五氧化二磷共热制得](../Page/五氧化二磷.md "wikilink")。\[2\]

## 应用

乙二醛的应用有：

  - [高分子化学中用作](../Page/高分子化学.md "wikilink")[增溶剂和](../Page/增溶剂.md "wikilink")[交联剂](../Page/交联.md "wikilink")
  - 用于[有机合成中](../Page/有机合成.md "wikilink")，尤其是构建[杂环如](../Page/杂环.md "wikilink")[咪唑时](../Page/咪唑.md "wikilink")\[3\]

## 溶液

通常乙二醛以40%溶液的形式出售。它与其他小分子[醛类似](../Page/醛.md "wikilink")，可以形成[水合物](../Page/水合物.md "wikilink")，而且水合物缩合生成一系列的“[寡聚体](../Page/寡聚体.md "wikilink")”，结构尚不清楚。目前出售的至少有以下两种水合物：

  - 二聚乙二醛二水合物：\[(CHO)<sub>2</sub>\]<sub>2</sub>\[H<sub>2</sub>O\]<sub>2</sub>、*trans*-2,3-二羟基-1,4-二噁烷
    (CAS\# 4845-50-5, m.p. 91-95 C)
  - 三聚乙二醛二水合物：\[(CHO)<sub>2</sub>\]<sub>3</sub>(H<sub>2</sub>O)<sub>4</sub>
    (CAS\# 4405-13-4)

根据估计，乙二醛水溶液浓度低于1M时，它主要以单体或水合物的形式存在，即OCHCHO、OCHCH(OH)<sub>2</sub>或(HO)<sub>2</sub>CHCH(OH)<sub>2</sub>。浓度大于1M时，主要为二聚体型，可能为[缩醛](../Page/缩醛.md "wikilink")／[酮结构](../Page/缩酮.md "wikilink")，分子式为\[(HO)CH\]<sub>2</sub>O<sub>2</sub>CHCHO。\[4\]

## 参考资料

<div class="references-small">

<references/>

</div>

[Category:二醛](../Category/二醛.md "wikilink")
[Category:二碳有机物](../Category/二碳有机物.md "wikilink")

1.
2.
3.
4.