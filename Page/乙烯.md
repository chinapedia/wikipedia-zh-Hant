**乙烯**是由两个[碳原子和四个](../Page/碳.md "wikilink")[氢原子组成的化合物](../Page/氢.md "wikilink")。两个碳原子之间用双键连接。

乙烯為合成纖維、合成橡膠、合成塑料（[聚乙烯及](../Page/聚乙烯.md "wikilink")[聚氯乙烯](../Page/聚氯乙烯.md "wikilink")）、合成[乙醇](../Page/乙醇.md "wikilink")（酒精）的基本化工原料，也用於制造[氯乙烯](../Page/氯乙烯.md "wikilink")、[苯乙烯](../Page/苯乙烯.md "wikilink")、[環氧乙烷](../Page/環氧乙烷.md "wikilink")、[醋酸](../Page/醋酸.md "wikilink")、[乙醛](../Page/乙醛.md "wikilink")、乙醇和[炸藥等](../Page/炸藥.md "wikilink")，且可用作水果和蔬菜的催熟剂，是一種已證實的[植物激素](../Page/植物激素.md "wikilink")，也是石油化工發展水準之指標。

## 分子結構

乙烯有4個氫原子，碳原子之間以雙鍵連接，6個原子共面。H-C-C键角为121.3°；H-C-H键角为117.4°，接近120°，是理想的[sp<sup>2</sup>混成軌域](../Page/Sp2杂化.md "wikilink")。這種分子也比較僵硬：旋轉C=C鍵是一個高吸热過程，需要打破[π鍵而保留](../Page/π鍵.md "wikilink")[σ鍵](../Page/σ鍵.md "wikilink")。

雙鍵是一個高電子密度的區域，因而大部分反應發生在這個位置。

## 生產方法

乙烯是由[石油化工](../Page/石油.md "wikilink")[裂解而成](../Page/裂解.md "wikilink")。在這個過程中，氣態或輕液態[烴被加熱到](../Page/烴.md "wikilink")750-950℃，誘使許多[自由基反應](../Page/自由基.md "wikilink")，然後立即[淬火凍結](../Page/淬火.md "wikilink")。這個过程中，大分子碳氫化合物轉化為較小分子的碳氫化合物，並生成[不飽和烴](../Page/不飽和烴.md "wikilink")。

## 化學反應

乙烯在石油化工等行業是一個極為重要的試劑，它的产量标志着一个国家的石油化工的能力。它可以進行多種類型的反應，從而製出各種化工產品。

其反應包括：

1.  加成，可以与[水](../Page/水.md "wikilink")、[卤素单质](../Page/卤素.md "wikilink")、[卤化氢](../Page/卤化氢.md "wikilink")、[氰化氢等加成](../Page/氰化氢.md "wikilink")。
2.  氧化，
3.  烷基化，
4.  聚合，生成[聚乙烯](../Page/聚乙烯.md "wikilink")。
5.  羰基合成反应，
6.  催熟水果和蔬菜。

## 实验室制法

工业上所用的乙烯，主要是从石油化工所生产的气体产品中分离出来的。在[实验室中](../Page/实验室.md "wikilink")，则通常是通过加热酒精和[浓硫酸的](../Page/浓硫酸.md "wikilink")[混合物](../Page/混合物.md "wikilink")，使[酒精分解而制得乙烯](../Page/酒精.md "wikilink")。在这个反应中，浓硫酸起[催化剂和](../Page/催化剂.md "wikilink")[脱水剂的作用](../Page/脱水剂.md "wikilink")，其反应方程式如下：

  -
    {|

| rowspan="2" |CH<sub>3</sub>CH<sub>2</sub>OH | style="border-bottom:
3px double black;"|浓H<sub>2</sub>SO<sub>4</sub> | rowspan="2"
|CH<sub>2</sub>=CH<sub>2</sub>↑ + H<sub>2</sub>O |- |
<span style="color: red; ">

<center>

△

</center>

</span> |}

**注意**，此反应应当迅速加热[混合物](../Page/混合物.md "wikilink")，否则将会产生副产物[乙醚](../Page/乙醚.md "wikilink")。

## 毒性现象

乙烯在低浓度时，有刺激作用，高浓度时具有较强的[麻醉作用](../Page/麻醉.md "wikilink")，但无明显兴奋阶段，麻醉快，甦醒也快。对[皮肤粘膜没有刺激作用](../Page/皮肤.md "wikilink")。主要中毒途径是[呼吸道吸入](../Page/呼吸.md "wikilink")，其次为皮肤接触。

突然吸入80\~90%高浓度乙烯，可立刻引起[意志丧失](../Page/意志.md "wikilink")；吸入75－90%乙烯与[氧的混合气可引起麻醉](../Page/氧.md "wikilink")；吸入61%乙烯4分钟，产生反应迟钝；吸入25－45%乙烯有痛觉消失和[记忆力减退](../Page/记忆.md "wikilink")。

## 植物激素

乙烯在[植物生理上扮演](../Page/植物生理學.md "wikilink")[植物激素的角色](../Page/植物激素.md "wikilink")\[1\]\[2\]\[3\]。作为植物的催熟剂，以氣體方式微量作用在植物，刺激或調節[果實成熟](../Page/果實.md "wikilink")、開花和植物[葉片掉落](../Page/葉片.md "wikilink")。因以氣體形式擴散，甚至會影響其他种类的植物。

在植物體內乙烯合成主要是由[甲硫胺酸做起始物](../Page/蛋氨酸.md "wikilink")，1-胺基環丙烷-1-羧酸(ACC)為關鍵中間產物。可以天然或人工合成。

## 參見

  - [2014年高雄氣爆事故](../Page/2014年高雄氣爆事故.md "wikilink")

## 參考文獻

## 延伸閲讀

  -
  -
  -
## 外表鏈接

  - [International Chemical Safety
    Card 0475](http://www.inchem.org/documents/icsc/icsc/eics0475.htm)
  - [European Chemicals Bureau
    Datasheet](http://ecb.jrc.ec.europa.eu/iuclid-datasheet/74851.pdf)
  - [Speculations Towards a General Plant Hormone
    Theory](http://www.planthormones.info)
  - [MSDS](https://web.archive.org/web/20061113092037/http://www.novachem.com/ProductServices/docs/Ethylene_MSDS_EN.pdf)

[Category:烯烃](../Category/烯烃.md "wikilink")
[Category:植物激素](../Category/植物激素.md "wikilink")
[Category:单体](../Category/单体.md "wikilink")
[Category:全身麻醉药](../Category/全身麻醉药.md "wikilink")
[Category:二碳有机物](../Category/二碳有机物.md "wikilink")
[Category:可燃气体](../Category/可燃气体.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")

1.
2.
3.  [乙烯字典-Guidechem.com](http://www.guidechem.com/dictionary/74-85-1.html)