**乙酰胺**是由[乙酸衍生出的](../Page/乙酸.md "wikilink")[酰胺](../Page/酰胺.md "wikilink")，分子式为CH<sub>3</sub>CONH<sub>2</sub>。纯品在室温下为白色晶状固体，可由[乙酸铵失水获得](../Page/乙酸铵.md "wikilink")。它被用作[增塑剂](../Page/增塑剂.md "wikilink")，也是有机合成的重要原料。

乙酰胺并不十分易燃，但燃烧时会放出刺激性的烟雾。吸入、吞食、皮肤及眼部接触均有毒，皮肤和眼部接触可能会造成变红或疼痛。

乙酰胺的衍生物[*N*,*N*-二甲基乙酰胺](../Page/N,N-二甲基乙酰胺.md "wikilink")（DMA）是乙酰胺[氨基的两个氢被](../Page/氨基.md "wikilink")[甲基取代后形成的化合物](../Page/甲基.md "wikilink")，常被用作[溶剂](../Page/溶剂.md "wikilink")；[*N*-甲基乙酰胺则是演示](../Page/N-甲基乙酰胺.md "wikilink")[肽键的最简单的例子](../Page/肽键.md "wikilink")。

最近[绿湾射电天文望远镜](../Page/绿湾射电天文望远镜.md "wikilink")（GBT）在[银河系中心部分检测到了一些](../Page/银河系.md "wikilink")[有机化合物](../Page/有机化合物.md "wikilink")，乙酰胺即是其一。由于乙酰胺中含有酰胺键，与连接蛋白质中[氨基酸的肽键类似](../Page/氨基酸.md "wikilink")，因此该发现支持了[生命是由简单有机化合物演化来的理论](../Page/生命起源.md "wikilink")。

## 致癌可能

有实验室研究表明，乙酰胺对动物有致癌作用。它也被[IARC划分为第](../Page/国际癌症研究机构.md "wikilink")2B类人类可能致癌物。

## 参考资料

  -
## 外部链接

  -
  - [Mindat.org](http://www.mindat.org/min-13.html)

  - [Webmineral.org](http://www.webmineral.com/data/Acetamide.shtml)

[Category:塑化劑](../Category/塑化劑.md "wikilink")
[Category:空气污染物](../Category/空气污染物.md "wikilink")
[Category:IARC第2B类致癌物质](../Category/IARC第2B类致癌物质.md "wikilink")
[Category:乙酰胺](../Category/乙酰胺.md "wikilink")
[Category:二碳有机物](../Category/二碳有机物.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")