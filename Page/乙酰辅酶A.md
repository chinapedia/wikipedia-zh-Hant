[Coenzym_A_beschriftet.svg](https://zh.wikipedia.org/wiki/File:Coenzym_A_beschriftet.svg "fig:Coenzym_A_beschriftet.svg")
[Acetyl-CoA.svg](https://zh.wikipedia.org/wiki/File:Acetyl-CoA.svg "fig:Acetyl-CoA.svg")
**乙酰辅酶A**\[1\]（）是活化了的[乙酸](../Page/乙酸.md "wikilink")，由[乙酰基](../Page/乙酰基.md "wikilink")（CH<sub>3</sub>CO-）与[辅酶A的](../Page/辅酶A.md "wikilink")[巯基以高能的](../Page/巯基.md "wikilink")[硫酯键相连](../Page/硫酯键.md "wikilink")。乙醯輔酶A是[脂肪酸的](../Page/脂肪酸.md "wikilink")[β-氧化及](../Page/β-氧化.md "wikilink")[糖酵解后产生的](../Page/糖酵解.md "wikilink")[丙酮酸脱羧後的产物](../Page/丙酮酸.md "wikilink")。

在[三羧酸循环的第一步](../Page/三羧酸循环.md "wikilink")，乙酰基转移到[草酰乙酸中](../Page/草酰乙酸.md "wikilink")，生成[柠檬酸](../Page/柠檬酸.md "wikilink")，因此這個循環也稱作「檸檬酸循環」。

## 生化意义

乙酰辅酶A是人体内重要的化学物质。首先，它是[丙酮酸脱羧](../Page/丙酮酸脱羧.md "wikilink")、脂肪酸β-氧化的产物。当丙酮酸激进入[线粒体的基质后](../Page/线粒体.md "wikilink")，它会被[丙酮酸脱氢酶复合体转化为乙酰辅酶A](../Page/丙酮酸脱氢酶复合体.md "wikilink")，因为在此过程中，丙酮酸会被氧化（氢原子转移到NAD<sup>+</sup>生成[NADH](../Page/NADH.md "wikilink")）及其[羧基会以](../Page/羧基.md "wikilink")[二氧化碳的形式离开](../Page/二氧化碳.md "wikilink")，故此过程被称为「丙酮酸脱羧」。同时，它是脂肪酸合成、[胆固醇合成和](../Page/胆固醇.md "wikilink")[生酮作用的碳来源](../Page/生酮作用.md "wikilink")。三大营养物质的彻底氧化殊途同归，都会生成乙酰辅酶A以进入三羧酸循环。

## 参见

  - [三羧酸循环](../Page/三羧酸循环.md "wikilink")
  - [甲羟戊酸途径](../Page/甲羟戊酸途径.md "wikilink")
  - [脂肪酸代谢](../Page/脂肪酸代谢.md "wikilink")
  - [酰基辅酶A](../Page/酰基辅酶A.md "wikilink")
  - [乙酰辅酶A合成酶](../Page/乙酰辅酶A合成酶.md "wikilink")
  - [丙二酰辅酶A脱羧酶](../Page/丙二酰辅酶A脱羧酶.md "wikilink")

## 外部連結

  - [檸檬酸循環（citric acid cycle, TCA
    cycle）](http://smallcollation.blogspot.com/2013/06/citric-acid-cycle.html)
  - [结构](https://web.archive.org/web/20051123010213/http://medlib.med.utah.edu/NetBiochem/FattyAcids/2_4.html)
  - [模型](https://web.archive.org/web/20050616084956/http://www.biocheminfo.org/klotho/html/acetyl-CoA.html)

[de:Coenzym
A\#Acetyl-CoA](../Page/de:Coenzym_A#Acetyl-CoA.md "wikilink")

[Category:代谢](../Category/代谢.md "wikilink")
[Category:硫酯](../Category/硫酯.md "wikilink")
[Category:輔酶](../Category/輔酶.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")

1.