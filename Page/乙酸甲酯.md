**乙酸甲酯**，分子式CH<sub>3</sub>COOCH<sub>3</sub>，是一种有机酸[酯](../Page/酯.md "wikilink")，亦称作**醋酸甲酯**。

## 物理性质

乙酸甲酯是无色、有香味、易挥发、易燃的液体。折光率为1.3595。溶于[水](../Page/水.md "wikilink")，与[乙醇](../Page/乙醇.md "wikilink")、[乙醚以任意比](../Page/乙醚.md "wikilink")[互溶](../Page/互溶.md "wikilink")，易溶于[丙酮](../Page/丙酮.md "wikilink")、[氯仿](../Page/氯仿.md "wikilink")、[苯等](../Page/苯.md "wikilink")。蒸气与空气可形成爆炸性[混合物](../Page/混合物.md "wikilink")，爆炸体积极限4.1%\~14%。

## 生物作用

有[麻醉性](../Page/麻醉性.md "wikilink")，对[黏膜有刺激性](../Page/黏膜.md "wikilink")。

## 用途

可用作[溶纤剂](../Page/溶纤剂.md "wikilink")、喷漆溶剂，以及用于制备[人造革](../Page/人造革.md "wikilink")。

## 制备

由[乙酸與](../Page/乙酸.md "wikilink")[甲醇直接進行](../Page/甲醇.md "wikilink")[酯化反應製得](../Page/酯化反應.md "wikilink")。

\[\ CH_3COOH+CH_3OH \ \xrightarrow \qquad\ CH_3COOCH_3+H_2O\]

[Category:甲酯](../Category/甲酯.md "wikilink")
[Category:酯類溶劑](../Category/酯類溶劑.md "wikilink")
[Category:乙酸酯](../Category/乙酸酯.md "wikilink")