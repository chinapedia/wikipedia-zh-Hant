[Acetate-anion-3D-balls.png](https://zh.wikipedia.org/wiki/File:Acetate-anion-3D-balls.png "fig:Acetate-anion-3D-balls.png")
**乙酸盐**俗称**醋酸盐**，是[乙酸所成的盐](../Page/乙酸.md "wikilink")，含有乙酸根离子CH<sub>3</sub>COO<sup>−</sup>，即[乙酸去掉](../Page/乙酸.md "wikilink")[羧基](../Page/羧基.md "wikilink")[质子后形成的](../Page/质子.md "wikilink")[阴离子](../Page/阴离子.md "wikilink")。

## 乙酸根離子

乙酸根離子的化學式是\[C<sub>2</sub>H<sub>3</sub>O<sub>2</sub>\]<sup>−</sup>，它是一種[羧酸根离子](../Page/羧酸根离子.md "wikilink")，並且是[乙酸的](../Page/乙酸.md "wikilink")[共軛鹼](../Page/共軛鹼.md "wikilink")。乙酸根離子是從乙酸的去質子化而獲得：

  -
    CH<sub>3</sub>COOH → CH<sub>3</sub>COO<sup>−</sup> + H<sup>+</sup>

## 符号

[有機化學中](../Page/有機化學.md "wikilink")，CH<sub>3</sub>COO—称作[乙酰氧基](../Page/乙酰氧基.md "wikilink")。由于[乙酰基](../Page/乙酰基.md "wikilink")（Acetyl）是以簡寫「Ac」來表示，乙酰氧基则为AcO—。

[无机化学中](../Page/无机化学.md "wikilink")，Ac<sup>−</sup>一般指乙酸根（Acetate），因此[乙酸和](../Page/乙酸.md "wikilink")[乙酸鈉分別以](../Page/乙酸鈉.md "wikilink")「HAc」和「NaAc」來表示。

雖然[元素](../Page/元素.md "wikilink")[錒的化學符號同是](../Page/錒.md "wikilink")「Ac」，但由於錒在有機化學中並沒有任何角色，所以很少會與乙酸根造成誤解。

## 結構

<File:Acetate-anion-3D-vdW.png>|

<center>

乙酸根離子的空间填充模型

</center>

<File:Acetate-anion-2D.png>|

<center>

乙酸根離子的[結構式](../Page/結構式.md "wikilink")

</center>

<File:Acetate-anion-resonance-hybrid-2D-skeletal.png>|

<center>

乙酸根離子的共振結構圖

</center>

<File:Acetate-anion-canonical-form-2D-skeletal.png>|

<center>

乙酸根離子的骨架結構

</center>

<File:Acetate-ester-2D-skeletal.png>|

<center>

乙酸酯的通式

</center>

<File:Acetyl-group-2D-skeletal.png>|

<center>

乙酰基

</center>

## 参见

  - [乙酸酯](../Page/乙酸酯.md "wikilink")
  - [乙酰化](../Page/乙酰化.md "wikilink")
  - [醋酸纖維素](../Page/醋酸纖維素.md "wikilink")

[乙酸盐](../Category/乙酸盐.md "wikilink")
[Category:羧酸阴离子](../Category/羧酸阴离子.md "wikilink")