**九如鄉**位於[台灣](../Page/臺灣.md "wikilink")[屏東縣西北方](../Page/屏東縣.md "wikilink")，北臨[里港鄉](../Page/里港鄉.md "wikilink")，西鄰[高雄市](../Page/高雄市.md "wikilink")[大樹區](../Page/大樹區.md "wikilink")，東鄰[鹽埔鄉](../Page/鹽埔鄉.md "wikilink")，東南連[長治鄉](../Page/長治鄉.md "wikilink")，南接[屏東市](../Page/屏東市.md "wikilink")。

本鄉位處[屏東平原之上](../Page/屏東平原.md "wikilink")，地形平坦，地勢由東北向西南略為傾斜，[高屏溪支流](../Page/高屏溪.md "wikilink")[武洛溪流貫鄉境](../Page/武洛溪.md "wikilink")，氣候上則屬[熱帶季風氣候](../Page/熱帶季風氣候.md "wikilink")。

## 歷史

**九如鄉**舊名「[九塊厝](../Page/九塊厝.md "wikilink")」，相傳最早是由張、楊、黃、鄭等九戶人家來此開墾，因而得名。[清朝時隸屬於](../Page/清朝.md "wikilink")[鳳山縣](../Page/鳳山縣_\(臺灣\).md "wikilink")，[日治初期劃歸阿猴廳](../Page/台灣日治時期.md "wikilink")。1920年台灣地方改制，在此設置「-{[九塊庄](../Page/九塊庄.md "wikilink")}-」，劃歸[高雄州](../Page/高雄州.md "wikilink")[屏東郡管轄](../Page/屏東郡.md "wikilink")。

戰後初期改劃設為[高雄縣九塊鄉](../Page/高雄縣.md "wikilink")，後劃入[屏東市](../Page/屏東市_\(省轄市\).md "wikilink")，易名「九如」，設置「九如區」。1950年改設[屏東縣九如鄉至今](../Page/屏東縣.md "wikilink")。

## 歷屆首長

### \-{庄}-長

  - [龔天降](../Page/龔天降.md "wikilink")

### 區長

### 鄉長

#### 九塊鄉時期

#### 九如鄉時期

  - 第一、二、三屆鄉長 陳清榮
  - 第四屆鄉長 楊坤杞
  - 第五、六屆鄉長 古認冬
  - 第七、八屆鄉長 黃金裕
  - 第九屆鄉長 古認冬
  - 第十、十一屆鄉長 楊泰春
  - 第十二、十三屆鄉長 許世裕
  - 第十四屆鄉長 許文瑞
  - 第十五、十六屆鄉長 許重慶
  - 第十七、十八屆鄉長 張振亮

## 行政區域

  - 九明村、九塊村、大坵村、玉泉村、後-{庄}-村、耆老村
  - 九清村、三塊村、玉水村、東寧村、洽興村

<table>
<tbody>
<tr class="odd">
<td><p><font size="-1"><strong>九如鄉行政區劃</strong></font></p></td>
</tr>
<tr class="even">
<td><div style="position: relative;font-size:100%">
<p><a href="https://zh.wikipedia.org/wiki/File:Jiouru_villages.svg" title="fig:Jiouru_villages.svg">Jiouru_villages.svg</a>           </p>
</div></td>
</tr>
</tbody>
</table>

## 人口

## 警政治安

  - [屏東縣警察局-{里}-港分局](http://www.ptpolice.gov.tw/_Branches/pingdung/Default.aspx)
      - 九如分駐所：屏東縣九如鄉九明村九如路二段177號　Tel：(08)7392424

## 交通

### 公路

  - 國道

<!-- end list -->

  - （福爾摩沙高速公路）

      - [高屏溪斜張橋](../Page/高屏溪斜張橋.md "wikilink")
      - [九如交流道](../Page/九如交流道.md "wikilink")（391k）

<!-- end list -->

  - 省道

<!-- end list -->

  -
<!-- end list -->

  - 縣道

<!-- end list -->

  - （2014年5月1日公告新編）：三民路、[國道三號橋下側車道](../Page/福爾摩沙高速公路.md "wikilink")

<!-- end list -->

  - 鄉道

<!-- end list -->

  -
  -
  -
  -
  -
  -
## 教育

### 國民中學

  - [屏東縣立九如國民中學](http://www.jrjh.ptc.edu.tw/)

### 國民小學

  - [屏東縣九如鄉九如國民小學](http://www.jrps.ptc.edu.tw)
  - [屏東縣九如鄉後庄國民小學](http://www.hjps.ptc.edu.tw)
  - [屏東縣九如鄉三多國民小學](http://www.sdes.ptc.edu.tw)
  - [屏東縣九如鄉惠農國民小學](http://www.hnps.ptc.edu.tw)

## 旅遊

  - [高屏溪斜張橋](../Page/高屏溪斜張橋.md "wikilink")：連接高、屏兩縣市，橋面由不對稱的紅色斜張鋼索銜接，主橋跨徑330公尺，側跨徑180公尺，橋面寬35公尺、全長2617公尺，橋塔高度184公尺，特殊的A字造型，結合了「力」與「美」的設計。若以全世界單塔斜張橋而論，其跨徑僅次於[德國跨越萊茵河橋](../Page/德國.md "wikilink")，為全世界排名第二，而亞洲第一。

  - [陳家古厝](http://www.pthg.gov.tw/townjrt/CP.aspx?s=9970&cp=1&n=17219)

  - [龔家古厝](http://www.pthg.gov.tw/townjrt/CP.aspx?s=9980&cp=1&n=17220)

  - [耆老社區排水溝](http://www.pthg.gov.tw/chinese/town/PTT02/p05_8.asp)

  - [玉泉村螢火蟲園區](http://www.pthg.gov.tw/TownJrt/CP.aspx?s=5763&cp=1&n=14847)

  - [九如三山國王廟](../Page/九如三山國王廟.md "wikilink")

  - [香蕉研究所](https://web.archive.org/web/20070117104141/http://www.banana.org.tw/main.asp)

## 特產

  - [香蕉](../Page/香蕉.md "wikilink")
  - [椰子](../Page/椰子.md "wikilink")
  - [檸檬](../Page/檸檬.md "wikilink")

## 參考資料

## 外部連結

  - [屏東縣九如鄉立圖書館](http://www.cultural.pthg.gov.tw/pthglib/blog/blog01.aspx?USER=23)
  - [台灣香蕉研究所](http://www.banana.org.tw/)
  - [屏東縣九如鄉民俗推展協會](http://sixstar.moc.gov.tw/blog/A904001)
  - [九如戶政事務所](http://www.pthg.gov.tw/plancab/CP.aspx?s=1285&cp=8&n=11086)
  - [九如衛生所](https://archive.is/20121222120813/http://jor-pth.doh.gov.tw/pub/LIT_6.asp?ctyp=LITERATURE&pcatid=0&catid=1829)

[九如鄉](../Category/九如鄉.md "wikilink")