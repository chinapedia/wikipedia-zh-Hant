《**九层糕**》（Beautiful
Connection），[新加坡新传媒](../Page/新加坡.md "wikilink")8频道时装剧集，于2002年8月份首播，共20集，监制郭令送。

## 演员表

### 主要演员

  - [朱咪咪](../Page/朱咪咪.md "wikilink") 饰 肥妈
  - [黄碧仁](../Page/黄碧仁.md "wikilink") 饰 范可怜
  - [陈丽贞](../Page/陈丽贞（演員）.md "wikilink") 饰 范可莉
  - [谢韶光](../Page/谢韶光.md "wikilink") 饰 Lion King
  - [欧　萱](../Page/欧萱.md "wikilink") 饰 范可可

### 其他演员

  - [陳瓊華](../Page/:en:Tan_Kheng_Hua.md "wikilink")
  - [巫许马丽](../Page/巫许马丽.md "wikilink")
  - [吴振宇](../Page/吴振天.md "wikilink")
  - [程旭輝](../Page/:en:Henry_Thia.md "wikilink")
  - [黄士南](../Page/黄士南.md "wikilink")
  - [白薇秀](../Page/:en:Joanne_Peh.md "wikilink")
  - [戚玉武](../Page/戚玉武.md "wikilink")

## 奖项

  - [謝韶光夺得](../Page/謝韶光.md "wikilink")《红星大奖2002》最佳男主角奖
  - [黄碧仁夺得](../Page/黄碧仁.md "wikilink")《红星大奖2002》最佳女主角奖
  - [欧萱夺得](../Page/欧萱.md "wikilink")《红星大奖2002》最佳新人奖
  - 《红星大奖2002》最佳电视剧
  - 《红星大奖2002》最高收视率电视剧

[J九](../Category/新加坡電視劇.md "wikilink")
[J九](../Category/2002年電視劇集.md "wikilink")