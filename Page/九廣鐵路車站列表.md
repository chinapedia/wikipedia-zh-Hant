以下是[九廣鐵路在](../Page/九廣鐵路.md "wikilink")[兩鐵合併前的車站列表](../Page/兩鐵合併.md "wikilink")：

| 圖例                              |
| ------------------------------- |
|                                 |
|                                 |
|                                 |
|                                 |
|                                 |
|                                 |
|                                 |
|                                 |
| <font color=white>●</font>      |
|                                 |
| <font color=white>●</font>      |
|                                 |
| <font color=white>**M**</font>| |
|                                 |
| <font color=white>**M**</font>  |
|                                 |
| <font color=white>**M**</font>  |
|                                 |
| <font color=white>**E**</font>  |
|                                 |
| <font color=white>**L**</font>  |
|                                 |

## [東鐵](../Page/東鐵綫.md "wikilink")

| [<font color=#6699FF>東鐵 East Rail</font>](../Page/東鐵綫.md "wikilink") |
| -------------------------------------------------------------------- |
|                                                                      |
|                                                                      |
| [<font color=white>◎</font>](../Page/#西鐵.md "wikilink")              |
|                                                                      |
|                                                                      |
|                                                                      |
| [<font color=white>**M**</font>](../Page/九龍塘站_\(香港\).md "wikilink")  |
|                                                                      |
| [<font color=white>◎</font>](../Page/#馬鐵.md "wikilink")              |
|                                                                      |
| <font color=white>■</font>                                           |
|                                                                      |
|                                                                      |
|                                                                      |
| <font color=white>▲</font>                                           |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
| <font color=white>▲</font>                                           |
|                                                                      |
|                                                                      |
|                                                                      |

## [馬鐵](../Page/馬鞍山綫.md "wikilink")

| [<font color=#A52A2A>馬鐵 Ma On Shan Rail</font>](../Page/馬鞍山綫.md "wikilink") |
| --------------------------------------------------------------------------- |
|                                                                             |
|                                                                             |
| [<font color=white>◎</font>](../Page/#東鐵.md "wikilink")                     |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |
|                                                                             |

## [西鐵](../Page/西鐵綫.md "wikilink")

| [<font color=#CA4699>西鐵 West Rail</font>](../Page/西鐵綫.md "wikilink") |
| -------------------------------------------------------------------- |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
|                                                                      |
| [<font color=white>**L**</font>](../Page/天耀站.md "wikilink")          |
|                                                                      |
|                                                                      |
|                                                                      |
| [<font color=white>**L**</font>](../Page/河田站.md "wikilink")          |
|                                                                      |

## 輕鐵

## 參見

[港鐵車站列表](../Page/港鐵車站列表.md "wikilink")

[en:List of Hong Kong KCR
stations](../Page/en:List_of_Hong_Kong_KCR_stations.md "wikilink")

[\*](../Category/九廣鐵路.md "wikilink")