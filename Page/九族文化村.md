{{ Expand |time=2008-03-21}}
[櫻花在臺灣南投九族文化村_Cherry_blossom_-_Sakura_in_Formosa_Aboriginal_Culture_Village,_Nantou,_TAIWAN.jpg](https://zh.wikipedia.org/wiki/File:櫻花在臺灣南投九族文化村_Cherry_blossom_-_Sakura_in_Formosa_Aboriginal_Culture_Village,_Nantou,_TAIWAN.jpg "fig:櫻花在臺灣南投九族文化村_Cherry_blossom_-_Sakura_in_Formosa_Aboriginal_Culture_Village,_Nantou,_TAIWAN.jpg")
[櫻花盛開在臺灣南投九族文化村_Cherry_blossom_-_Sakura_in_Formosa_Aboriginal_Culture_Village,_Nantou,_TAIWAN.jpg](https://zh.wikipedia.org/wiki/File:櫻花盛開在臺灣南投九族文化村_Cherry_blossom_-_Sakura_in_Formosa_Aboriginal_Culture_Village,_Nantou,_TAIWAN.jpg "fig:櫻花盛開在臺灣南投九族文化村_Cherry_blossom_-_Sakura_in_Formosa_Aboriginal_Culture_Village,_Nantou,_TAIWAN.jpg")
**九族文化村**（）是一座位於[臺灣](../Page/臺灣.md "wikilink")[南投縣](../Page/南投縣.md "wikilink")[魚池鄉的主題樂園](../Page/魚池鄉_\(臺灣\).md "wikilink")，名稱由來為成立時、中華民國中央政府認定的9個[臺灣原住民族](../Page/臺灣原住民族.md "wikilink")\[1\]。園區全區面積62[公頃](../Page/公頃.md "wikilink")，是依山建造的遊樂園區，全區制高點位在觀山樓，[海拔標高約](../Page/海拔.md "wikilink")900[公尺](../Page/公尺.md "wikilink")。園區內部以台灣16個原住民族族群的各項[文化特色展示為主要特色](../Page/文化.md "wikilink")，另設有多項遊樂設施及[日月潭纜車](../Page/日月潭纜車.md "wikilink")，連接九族文化村觀山樓到[救國團日月潭青年活動中心](../Page/救國團日月潭青年活動中心.md "wikilink")。

## 歷史

  - 1979年，婦友欣業[有限公司投資成立](../Page/有限公司.md "wikilink")「九族文化村」。富有工業股份有限公司創辦人[張榮義發起創辦](../Page/張榮義.md "wikilink")「九族」為主題的[遊樂園](../Page/遊樂園.md "wikilink")，[陳奇祿](../Page/陳奇祿.md "wikilink")[博士](../Page/博士.md "wikilink")、[千千岩助太郎博士](../Page/千千岩助太郎.md "wikilink")、[收藏家](../Page/收藏家.md "wikilink")[徐瀛洲等人指導及提供諸多資料](../Page/徐瀛洲.md "wikilink")，[姚德雄設計師策劃](../Page/姚德雄.md "wikilink")。
  - 1983年，動工闢建。
  - 1986年7月27日，正式啟用。
  - 1992年，九族文化村成立歡樂世界，室內樂園部份開始營運，10項設施全新登場，包括水舞廣場，星際旅行，3D電影，單軌車，皇家火車，音樂馬車，老爺車，熱氣球，大章魚，太空山，侏羅紀探險，夏威夷巨浪，愛之船相繼落成。
  - 1995年，大章魚淘汰、原地設立室內海盜船，金礦山探險設施耗資新台幣三億元正式登場。
  - 1998年，馬雅探險耗資新台幣六億元，台灣唯一懸吊式雲宵飛車，眼鏡蛇式旋轉，正式在九族與大家相見。
  - 2000年，UFO入侵九族文化村，耗資三億，台灣最高自由落體正式參見。
  - 2001年，九族文化村開始舉辦「日月潭九族櫻花祭」，園區內種植超過5000株櫻花。
  - 2001年，九族纜車用不同的角度看見九族文化村，三億元空中纜車正式與大家見面。
  - 2008年，加勒比海探險水上雲宵飛車，耗費三億六千萬打造倒沖下洗開幕。
  - 2009年，九族文化村闢建完成[日月潭纜車](../Page/日月潭纜車.md "wikilink")，由九族文化村觀山樓通往[救國團日月潭青年活動中心](../Page/救國團日月潭青年活動中心.md "wikilink")，將兩個景點串接起來。該空中纜車路線全長1.877公里。
  - 2011年7月～12月，首次海外授權的《[航海王](../Page/航海王.md "wikilink")》樂園，由九族文化村取得台灣獨家主辦權。
  - 2011年，夏威夷巨浪與愛之船設施正式退役。
  - 2013年，阿拉丁廣場增設轉轉車與跳跳蛙二項新設施。
  - 2013年，獲得日本花協會認證為「櫻花名所優選之地」，同時也是日本唯一海外認證「賞櫻名所」。
  - 2018年12月28日，「西班牙海岸」華麗登場，斥資六億、耗時六年，西班牙高第建築＋三大新遊樂設施以及加勒比海(改名:無敵艦隊)、皇家火車併入。\[2\]

## 園區環境

全園區規劃分成三座主題園區：

  - 歐洲花園

歐洲宮廷的舞姿水沙連歐洲花園，占地約六公頃，遼闊的皇家氣度，展現歐洲宮廷的花園風情，是台灣最大的歐式花園，也是台灣許多偶像劇拍攝地，巴洛克的音樂隨著羅馬噴泉的水舞響起，在這浪漫的歐洲花園裡，可以乘著**水沙連小火車**，迎著微風，悠閒的遊覽花園的大氣度，或是散步走過花園的噴泉與鐘樓，在花園裡還有巍峨的麗宮。
(每年的三月在這歐洲花園裡，還有個浪漫的紫戀薰衣草花季)

  - 原住民文化

[Formosan_Aboriginal_Culture_Village_Atayal.jpg](https://zh.wikipedia.org/wiki/File:Formosan_Aboriginal_Culture_Village_Atayal.jpg "fig:Formosan_Aboriginal_Culture_Village_Atayal.jpg")
園區內皆有仿造各族實屋來打造[原住民](../Page/台灣原住民.md "wikilink")[建築](../Page/建築.md "wikilink")，並有展現原住民[舞蹈技藝的表演](../Page/舞蹈.md "wikilink")[劇場](../Page/劇場.md "wikilink")：娜魯灣劇場與石音劇場。

  - 歡樂世界

分為六大主題，可以一票玩到底

  - 阿拉丁廣場：

單軌電車、老爺車、皇家火車、音樂馬車、海盜船／VR風暴幽靈船、轉轉車、太空山／VR星際戰艦、熱氣球、侏儸紀探險、跳跳蛙。

  - 未來世界：

影舞者劇場、星際旅遊、UFO自由落體、九族纜車。

  - 西部探險區:

金礦山探險

  - 馬雅村：

博幼車、馬雅村、馬雅探險

  - 西班牙海岸：

大漩渦、飛行船、水戰船、皇家火車、無敵艦隊。

  - 觀山樓：

九族纜車、日月潭纜車。

## 營業時間

### 園區開放時間

  - 平日/ 09：30\~17：00 假日/ 09：30\~17：30
  - 售票時間/ 09:30\~15：00
  - 遊樂設施開放時間以現場售票處公告為準

### 遊樂設施開放時間

<table>
<thead>
<tr class="header">
<th><p>時間</p></th>
<th><p>日月潭&amp;九族纜車</p></th>
<th><p>無敵艦隊</p></th>
<th><p>UFO</p></th>
<th><p>馬雅探險&amp;金礦山探險</p></th>
<th><p>大漩渦</p></th>
<th><p>其他設施</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>平日</p></td>
<td><p>10:30-16:00</p></td>
<td><p>10:00-12:00<br />
12:30-16:00</p></td>
<td><p>10:00-12:30</p></td>
<td><p>10:00-17:00</p></td>
<td><p>10:00-12:30<br />
13:00-16:00</p></td>
<td><p>09:30-17:00</p></td>
</tr>
<tr class="even">
<td><p>假日</p></td>
<td><p>10:00-16:30</p></td>
<td><p>10:00-12:00<br />
12:30-17:00</p></td>
<td><p>10:00-12:30<br />
13:00-17:00</p></td>
<td><p>10:00-17:00</p></td>
<td><p>10:00-12:30<br />
13:00-17:00</p></td>
<td><p>09:00-17:30</p></td>
</tr>
</tbody>
</table>

  - 部落區、博幼車至 17:00
  - 九族文化博物館開放時間 平、假日 / 10:00-17:00
  - 日月潭纜車站休館日：每個月的第一個星期三為休館日。

## 相關條目

  - [日月潭](../Page/日月潭.md "wikilink")
  - [日月潭纜車](../Page/日月潭纜車.md "wikilink")

## 參考資料

<references />

## 外部連結

  - [九族文化村](http://www.nine.com.tw)

  - [九族文化村](http://facvnine.pixnet.net/blog)痞客邦

  -
  -
  -
  - 九族APP
    [Android](https://play.google.com/store/apps/details?id=geo.grid.nine)、[iOS](https://itunes.apple.com/tw/app/%E4%B9%9D%E6%97%8F%E6%96%87%E5%8C%96%E6%9D%91/id1259867307?mt=8)

[Category:南投縣旅遊景點](../Category/南投縣旅遊景點.md "wikilink")
[Category:台灣遊樂園](../Category/台灣遊樂園.md "wikilink")
[Category:台灣原住民文化](../Category/台灣原住民文化.md "wikilink")
[Category:台灣名數9](../Category/台灣名數9.md "wikilink")
[Category:1986年開幕的主題樂園](../Category/1986年開幕的主題樂園.md "wikilink")
[Category:1986年台灣建立](../Category/1986年台灣建立.md "wikilink")

1.  [泰雅族](../Page/泰雅族.md "wikilink")、[賽夏族](../Page/賽夏族.md "wikilink")、[鄒族](../Page/鄒族.md "wikilink")、[布農族](../Page/布農族.md "wikilink")、[卑南族](../Page/卑南族.md "wikilink")、[魯凱族](../Page/魯凱族.md "wikilink")、[達悟族](../Page/達悟族.md "wikilink")、[阿美族以及](../Page/阿美族.md "wikilink")[排灣族](../Page/排灣族.md "wikilink")。
2.  [最新訊息](https://www.nine.com.tw/webc/html/news/02.aspx?num=84633)，九族文化村，2018-12-22