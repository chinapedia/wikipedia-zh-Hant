**九江郡**，是中国古代[秦朝至](../Page/秦朝.md "wikilink")[三国的一个](../Page/三国.md "wikilink")[郡级行政区划](../Page/郡.md "wikilink")。后改为[淮南郡](../Page/淮南郡.md "wikilink")。

## 历史

[秦朝置三十六郡中](../Page/秦朝.md "wikilink")，就有九江郡，其治所在[寿春](../Page/寿春.md "wikilink")（今[安徽](../Page/安徽.md "wikilink")[寿县](../Page/寿县.md "wikilink")）。辖境约相当于今天[安徽](../Page/安徽.md "wikilink")、[河南](../Page/河南.md "wikilink")[淮河以南](../Page/淮河.md "wikilink")，[湖北](../Page/湖北.md "wikilink")[黄冈以东和](../Page/黄冈.md "wikilink")[江西全省](../Page/江西.md "wikilink")，为九江郡历来范围最大之时，因此地有众多水泽而得名。

秦末汉初之际，割西境置[衡山郡](../Page/衡山郡.md "wikilink")，割南境置[庐江](../Page/廬江郡_\(秦朝\).md "wikilink")、[豫章二郡](../Page/豫章郡.md "wikilink")，九江郡范围大大缩小，只包括今天[安徽省中部](../Page/安徽省.md "wikilink")[淮河以南](../Page/淮河.md "wikilink")、[瓦埠湖流域以东](../Page/瓦埠湖.md "wikilink")、[巢湖以北地区](../Page/巢湖.md "wikilink")。[汉高祖时](../Page/汉高祖.md "wikilink")，改九江郡为[淮南国](../Page/淮南国.md "wikilink")，封给了淮南厉王[刘长](../Page/刘长.md "wikilink")。[汉武帝](../Page/汉武帝.md "wikilink")[元狩元年](../Page/元狩.md "wikilink")（前122年），淮南王[刘安因涉嫌谋反自杀后](../Page/刘安.md "wikilink")，淮南国撤销，复称九江郡。[东汉保留郡置](../Page/东汉.md "wikilink")，且[扬州刺史部历个治所](../Page/扬州刺史部.md "wikilink")[历阳县](../Page/历阳县.md "wikilink")（今[和县](../Page/和县.md "wikilink")）、[寿春](../Page/寿春.md "wikilink")、[合肥都在郡内](../Page/合肥.md "wikilink")，成为扬州首郡。

[三国时期](../Page/三国时期.md "wikilink")，[曹魏](../Page/曹魏.md "wikilink")[嘉平初年](../Page/嘉平_\(曹魏\).md "wikilink")，改九江为[淮南郡](../Page/淮南郡.md "wikilink")。从此九江不再作为郡名存在。

## 太守

  - 西汉

<!-- end list -->

  - [朱建](../Page/朱建.md "wikilink")
  - [刘襄](../Page/刘襄.md "wikilink")
  - [张苍](../Page/张苍.md "wikilink")
  - 某春
  - [张释之](../Page/张释之.md "wikilink")
  - 申屠臾
  - [王嘉](../Page/王嘉.md "wikilink")
  - [马宫](../Page/马宫.md "wikilink")
  - [房凤](../Page/房凤.md "wikilink")

<!-- end list -->

  - 东汉

<!-- end list -->

  - [梁统](../Page/梁统.md "wikilink")
  - [宗均](../Page/宋均.md "wikilink")
  - [玄贺](../Page/玄贺.md "wikilink")
  - 雍窦
  - 丘腾，[漢沖帝](../Page/漢沖帝.md "wikilink")[建康元年](../Page/建康_\(东汉\).md "wikilink")（144年）九月己未，丘騰有罪，知罪法深大，懷挾姦巧，稽留道路，下獄而死。
  - 邓显
  - 杨岑
  - 鲁峻
  - 费政
  - 文穆
  - [卢植](../Page/卢植.md "wikilink")
  - [阳球](../Page/阳球.md "wikilink")
  - [服虔](../Page/服虔.md "wikilink")
  - 威某
  - 荆修
  - [刘邈](../Page/刘邈.md "wikilink")
  - [边让](../Page/边让.md "wikilink")
  - [周昂](../Page/周昂.md "wikilink")
  - [陈纪](../Page/陈纪.md "wikilink")
  - 武端
  - 旗况

## 行政区划

治所阴陵（今安徽凤阳周圩西南）。领14县：阴陵、西曲阳、寿春、当涂、下蔡、平阿、义成、钟离、成德、合肥、浚遒、全椒、阜陵、历阳。

## 与九江市的关系

今天的[江西省](../Page/江西省.md "wikilink")[九江市](../Page/九江市.md "wikilink")，虽在秦置九江郡的范围内，但当时并未设县。而到西汉初年建县于此，称柴桑时，九江郡范围已经大幅北退，当中还有整个[庐江郡](../Page/庐江郡.md "wikilink")。所以两者虽然名称相同，也曾非常短暂的出现统辖关系，但除此之外几乎毫无关系。[九江市首次获得九江之名](../Page/九江市.md "wikilink")，要到[明朝时改](../Page/明朝.md "wikilink")[江州路为](../Page/江州路.md "wikilink")[九江府](../Page/九江府.md "wikilink")，治德化（即今九江市），而在此之前的历史中出现的“九江”很少指今天江西省范围。

[Category:秦朝的郡](../Category/秦朝的郡.md "wikilink")
[Category:汉朝的郡](../Category/汉朝的郡.md "wikilink")
[Category:曹魏的郡](../Category/曹魏的郡.md "wikilink")
[Category:江西的郡](../Category/江西的郡.md "wikilink")
[Category:湖南的郡](../Category/湖南的郡.md "wikilink")
[Category:安徽的郡](../Category/安徽的郡.md "wikilink")
[Category:河南的郡](../Category/河南的郡.md "wikilink")
[Category:前3世纪建立的行政区划](../Category/前3世纪建立的行政区划.md "wikilink")
[Category:240年代废除的行政区划](../Category/240年代废除的行政区划.md "wikilink")