**[九龍巴士N](../Page/九龍巴士.md "wikilink")241線**是[香港一條來往](../Page/香港.md "wikilink")[紅磡站及](../Page/紅磡站.md "wikilink")[青衣](../Page/青衣島.md "wikilink")[長宏邨的通宵路線](../Page/長宏邨.md "wikilink")，途經[長安邨](../Page/長安邨.md "wikilink")、[長康邨](../Page/長康邨.md "wikilink")、[長青邨](../Page/長青邨.md "wikilink")、[葵芳](../Page/葵芳.md "wikilink")、[荔景山路](../Page/荔景山路.md "wikilink")、[青山道](../Page/青山道.md "wikilink")、[旺角](../Page/旺角.md "wikilink")、[尖沙咀等地](../Page/尖沙咀.md "wikilink")。本路線主要用途是方便市民於途徑沿線日間路線（包括然不限於6、41A、42A及46線）收車後仍可乘搭本線回家。

深宵特別巴士路線**N41X**，由紅磡站單向往長宏，途經尖沙咀、油麻地、旺角及[奧運站後](../Page/奧運站.md "wikilink")，取道[青葵公路直達](../Page/青葵公路.md "wikilink")[長康邨](../Page/長康邨.md "wikilink")、[長青邨](../Page/長青邨.md "wikilink")、[翠怡花園](../Page/翠怡花園.md "wikilink")、[青衣邨](../Page/青衣邨.md "wikilink")、長安邨及[長亨邨](../Page/長亨邨.md "wikilink")。

## 歷史

  - 1990年11月18日：本線前身**241S線**開辦，由長安往返[旺角火車站](../Page/旺角東站.md "wikilink")，是新界西區首條通宵常規路線。
  - 1991年6月23日：往旺角方向改經青山道及[大埔道](../Page/大埔道.md "wikilink")；往長安方向改經大埔道及[元州街](../Page/元州街.md "wikilink")。
  - 1992年4月5日：[油尖旺區總站延長至九龍車站](../Page/油尖旺區.md "wikilink")（現[紅磡鐵路站](../Page/紅磡站.md "wikilink")）。並於同日增設雙向分段收費，以取代[206S線的服務](../Page/九龍巴士206S線.md "wikilink")。
  - 1994年4月15日：[葵青區總站延長至](../Page/葵青區.md "wikilink")[長亨](../Page/長亨邨.md "wikilink")。
  - 1995年2月20日：來回程繞經[荔景山路](../Page/荔景山路.md "wikilink")。
  - 1996年5月1日：為配合九巴重組[通宵路線](../Page/通宵路線.md "wikilink")，本路線正式更名為N241。
  - 1997年7月14日：配合與[N260線於本路線半途轉乘](../Page/九龍巴士N260線.md "wikilink")，由上車付費改為下車付費，並於美孚站或之後向乘客派發分段收費代用券。
  - 2002年11月3日：來回程繞經[清麗苑和](../Page/清麗苑.md "wikilink")[美荔道](../Page/美荔道.md "wikilink")。
  - 2009年7月26日：總站遷往[長宏](../Page/長宏邨.md "wikilink")。
  - 2015年7月18日：由下車付費改回上車付費。而所有雙向分段收費由以現金加分段收費代用券於下車時付費改為以八達通卡上車時付全費並於下車前回贈車費差額。

多年來深宵由九龍[尖沙咀](../Page/尖沙咀.md "wikilink")、[旺角市區前往](../Page/旺角.md "wikilink")[青衣的巴士服務](../Page/青衣.md "wikilink")，由通宵線N241提供，惟該線需要途經[深水埗](../Page/深水埗.md "wikilink")、[長沙灣](../Page/長沙灣.md "wikilink")、[美孚](../Page/美孚.md "wikilink")、[荔景山路前往](../Page/荔景山路.md "wikilink")，路線相當迂迴，中途停站頻密，由市區返回青衣的行車時間相當長。「[泥鯭的](../Page/泥鯭的.md "wikilink")」看準N241線行車時間長及走線迂迴的弱點，於[旺角](../Page/旺角.md "wikilink")[山東街中旅社一帶接載乘客返回青衣](../Page/山東街.md "wikilink")，雖屬非法且收費高昂但快捷得多（每位$22，車程約15分鐘），仍深受居民歡迎\[1\]。

為提供深宵直接由南九龍區前往青衣的巴士服務，及為打擊「[泥鯭的](../Page/泥鯭的.md "wikilink")」，[運輸署連同九巴於](../Page/運輸署.md "wikilink")《2017-2018年度巴士路線計劃》中，建議N241線其中兩班往[青衣方向班次](../Page/青衣.md "wikilink")，在[彌敦道起改經](../Page/彌敦道.md "wikilink")[亞皆老街](../Page/亞皆老街.md "wikilink")、[櫻桃街](../Page/櫻桃街.md "wikilink")、[深旺道](../Page/深旺道.md "wikilink")、[海輝道](../Page/海輝道.md "wikilink")、[連翔道](../Page/連翔道.md "wikilink")、[西九龍公路及](../Page/西九龍公路.md "wikilink")[青葵公路返回](../Page/青葵公路.md "wikilink")[青衣南橋](../Page/青衣南橋.md "wikilink")（即依日間線往青衣方向走線），不經主線途經的[深水埗](../Page/深水埗.md "wikilink")、[長沙灣](../Page/長沙灣.md "wikilink")、[美孚](../Page/美孚.md "wikilink")、[荔景山路](../Page/荔景山路.md "wikilink")，編號為**N41X**\[2\]。

  - 2017年10月31日：特快深宵路線N41X投入服務，每日開出00:45及01:10兩班\[3\]。

## 服務時間（詳細班次）

  - N241

| 每日[紅磡站開](../Page/紅磡站.md "wikilink") |
| ----------------------------------- |
| 00:15                               |
| 01:25                               |
| 02:40                               |
| 04:00                               |
| 05:20                               |

| 每日[長宏開](../Page/長宏邨.md "wikilink") |
| ---------------------------------- |
| 00:05                              |
| 01:10                              |
| 02:30                              |
| 03:50                              |
| 05:10                              |

  - N41X

<!-- end list -->

  - 每日[紅磡站開](../Page/紅磡站.md "wikilink")：00:45、01:10

## 收費

### N241

  - 紅磡站開

| 上車站       | 下車站       |
| --------- | --------- |
| **美孚**或之前 | **長宏**或之前 |
| 紅磡站起      | $9.1^     |
| 美孚起       |           |

  - 有 ^ 之收費必須以八達通卡繳付，並須於下車前再次確認八達通卡方可享有此優惠。

<!-- end list -->

  - 長宏開

| 上車站       | 下車站        |
| --------- | ---------- |
| **美孚**或之前 | **紅磡站**或之前 |
| 長宏起       | $9.1^      |
| 美孚起       |            |

  - 有 ^ 之收費必須以八達通卡繳付，並須於下車前再次確認八達通卡方可享有此優惠。

### N41X

| 上車站        | 下車站       |
| ---------- | --------- |
| **君匯港**或之前 | **長宏**或之前 |
| 紅磡站起       | $9.1^     |
| 過青衣大橋後     |           |

### 轉乘優惠

  - 八達通
    N241

由本線於登車後2小時內以同一張八達通卡於美孚轉乘[N237線往葵盛方向](../Page/九龍巴士N237線.md "wikilink")、[N260線往屯門方向或](../Page/九龍巴士N260線.md "wikilink")[N269線往天水圍方向](../Page/九龍巴士N269線.md "wikilink")，而各程收費如下表列。

<table>
<thead>
<tr class="header">
<th><p>轉乘路線<br />
<small>（轉往或轉自）</small></p></th>
<th><p>八達通轉乘優惠</p></th>
<th><p>指定轉乘地點</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>N237線</p></td>
<td><p>$2.4</p></td>
<td><p>美孚</p></td>
</tr>
<tr class="even">
<td><p>N260線</p></td>
<td><p>$2.5</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>N269線</p></td>
<td><p>$2.3</p></td>
<td></td>
</tr>
</tbody>
</table>

  - 龍運機場巴士及九巴轉乘計劃

## 行車路線

**紅磡站開**經：[暢運道](../Page/暢運道.md "wikilink")、[漆咸道南](../Page/漆咸道南.md "wikilink")、[梳士巴利道](../Page/梳士巴利道.md "wikilink")、[彌敦道](../Page/彌敦道.md "wikilink")、[大埔道](../Page/大埔道.md "wikilink")、[元州街](../Page/元州街.md "wikilink")、[興華街](../Page/興華街.md "wikilink")、[長沙灣道](../Page/長沙灣道.md "wikilink")、[荔枝角道](../Page/荔枝角道.md "wikilink")、[美孚巴士總站](../Page/美孚巴士總站.md "wikilink")、長沙灣道、掉頭處、[美荔道](../Page/美荔道.md "wikilink")、[荔灣道](../Page/荔灣道.md "wikilink")、[荔景山路](../Page/荔景山路.md "wikilink")、[葵福路](../Page/葵福路.md "wikilink")、[興芳路](../Page/興芳路.md "wikilink")、[葵青路](../Page/葵青路.md "wikilink")、[葵青橋](../Page/葵青橋.md "wikilink")、青衣交匯處、[青衣路](../Page/青衣路.md "wikilink")、[青康路](../Page/青康路.md "wikilink")、[涌美路](../Page/涌美路.md "wikilink")、[青衣鄉事會路](../Page/青衣鄉事會路.md "wikilink")、[楓樹窩路](../Page/楓樹窩路.md "wikilink")、[青衣西路](../Page/青衣西路.md "wikilink")、[担杆山交匯處](../Page/担杆山交匯處.md "wikilink")、[青敬路](../Page/青敬路.md "wikilink")、[長安巴士總站](../Page/長安巴士總站.md "wikilink")、[担杆山路](../Page/担杆山路.md "wikilink")、担杆山交匯處、青衣西路及[寮肚路](../Page/寮肚路.md "wikilink")。

**長宏開**經：寮肚路、[長亨巴士總站](../Page/長亨巴士總站.md "wikilink")、寮肚路、青衣西路、担杆山交匯處、青敬路、長安巴士總站、担杆山路、担杆山交匯處、青衣西路，楓樹窩路、青衣鄉事會路、涌美路、青康路、青衣路、青衣交匯處、葵青橋、葵青路、興芳路、葵福路、荔景山路、荔灣道、美荔道、長沙灣道、[通州西街](../Page/通州西街.md "wikilink")、[青山道](../Page/青山道.md "wikilink")、大埔道、彌敦道、梳士巴利道、漆咸道南及暢運道。

  - N41X沿N241駛至彌敦道後，改經[亞皆老街](../Page/亞皆老街.md "wikilink")、[櫻桃街](../Page/櫻桃街.md "wikilink")、[深旺道](../Page/深旺道.md "wikilink")、[海輝道](../Page/海輝道.md "wikilink")、[連翔道](../Page/連翔道.md "wikilink")、[西九龍公路](../Page/西九龍公路.md "wikilink")、[青葵公路](../Page/青葵公路.md "wikilink")，然後返回N241原線

### 沿線車站

[N241RtMap.png](https://zh.wikipedia.org/wiki/File:N241RtMap.png "fig:N241RtMap.png")

  - N241

| [紅磡站開](../Page/紅磡站.md "wikilink") | [長宏開](../Page/長宏邨.md "wikilink")         |
| --------------------------------- | ---------------------------------------- |
| **序號**                            | **車站名稱**                                 |
| 1                                 | [紅磡站巴士總站](../Page/紅磡站.md "wikilink")     |
| 2                                 | [香港歷史博物館](../Page/香港歷史博物館.md "wikilink") |
| 3                                 | [香港科學館](../Page/香港科學館.md "wikilink")     |
| 4                                 | [麼地道](../Page/麼地道.md "wikilink")         |
| 5                                 | [尖東站](../Page/尖東站.md "wikilink")         |
| 6                                 | [中間道](../Page/中間道.md "wikilink")         |
| 7                                 | [海防道](../Page/海防道.md "wikilink")         |
| 8                                 | [九龍公園](../Page/九龍公園.md "wikilink")       |
| 9                                 | [金巴利道](../Page/金巴利道.md "wikilink")       |
| 10                                | [尖沙咀警署](../Page/尖沙咀警署.md "wikilink")     |
| 11                                | [寶靈街](../Page/寶靈街.md "wikilink")         |
| 12                                | [南京街](../Page/南京街_\(香港\).md "wikilink")  |
| 13                                | [九龍中央郵政局](../Page/九龍中央郵政局.md "wikilink") |
| 14                                | [眾坊街](../Page/眾坊街.md "wikilink")         |
| 15                                | [文明里](../Page/文明里.md "wikilink")         |
| 16                                | [碧街](../Page/碧街.md "wikilink")           |
| 17                                | [奶路臣街](../Page/奶路臣街.md "wikilink")       |
| 18                                | [快富街](../Page/快富街.md "wikilink")         |
| 19                                | [弼街](../Page/弼街.md "wikilink")           |
| 20                                | [太子站](../Page/太子站.md "wikilink")         |
| 21                                | [福華街休憩公園](../Page/福華街.md "wikilink")     |
| 22                                | [北河街](../Page/北河街.md "wikilink")         |
| 23                                | [營盤街](../Page/營盤街.md "wikilink")         |
| 24                                | [東京街](../Page/東京街.md "wikilink")         |
| 25                                | [發祥街](../Page/發祥街.md "wikilink")         |
| 26                                | [元州商場](../Page/元州邨#元州商場.md "wikilink")   |
| 27                                | [興華街](../Page/興華街.md "wikilink")         |
| 28                                | [荔枝角站](../Page/荔枝角站.md "wikilink")       |
| 29                                | [美孚巴士總站](../Page/美孚巴士總站.md "wikilink")   |
| 30                                | [蘭秀道](../Page/蘭秀道.md "wikilink")         |
| 31                                | 荔灣道                                      |
| 32                                | 九華徑                                      |
| 33                                | [荔灣花園](../Page/荔灣花園.md "wikilink")       |
| 34                                | 清麗商場                                     |
| 35                                | 清麗苑                                      |
| 36                                | 瑪嘉烈醫院                                    |
| 37                                | 葵涌醫院                                     |
| 38                                | 救世軍荔景院                                   |
| 39                                | 荔景邨樂景樓                                   |
| 40                                | 荔景邨日景樓                                   |
| 41                                | 葵青劇院                                     |
| 42                                | 葵青交匯處                                    |
| 43                                | 長青邨青桃樓                                   |
| 44                                | [美景花園](../Page/美景花園.md "wikilink")       |
| 45                                | 長青巴士總站                                   |
| 46                                | 青盛苑                                      |
| 47                                | 長康邨康順樓                                   |
| 48                                | [職安健學院](../Page/職安健學院.md "wikilink")     |
| 49                                | 長康邨康富樓                                   |
| 50                                | [青衣警署](../Page/青衣警署.md "wikilink")       |
| 51                                | 青怡花園                                     |
| 52                                | 青衣邨宜業樓                                   |
| 53                                | 楓樹窩體育館                                   |
| 54                                | 長安邨                                      |
| 55                                | [曉峰園](../Page/曉峰園.md "wikilink")         |
| 56                                | [長亨邨停車場](../Page/長亨邨.md "wikilink")      |
| 57                                | 長宏巴士總站                                   |

  - N41X

[N41XRtMap.png](https://zh.wikipedia.org/wiki/File:N41XRtMap.png "fig:N41XRtMap.png")

| 紅磡站開   |
| ------ |
| **序號** |
| 1      |
| 2      |
| 3      |
| 4      |
| 5      |
| 6      |
| 7      |
| 8      |
| 9      |
| 10     |
| 11     |
| 12     |
| 13     |
| 14     |
| 15     |
| 16     |
| 17     |
| 18     |
| 19     |
| 20     |
| 21     |
| 22     |
| 23     |
| 24     |
| 25     |
| 26     |
| 27     |
| 28     |
| 29     |
| 30     |
| 31     |
| 32     |
| 33     |
| 34     |

## 小資料

  - 由於本線非常迂迴，由[長宏巴士總站開至美孚](../Page/長宏巴士總站.md "wikilink")，往往長達半小時，青衣中南部居民往往寧願改乘的士；而西部的居民則改乘收費較高昂的[城巴N21線](../Page/城巴N21線.md "wikilink")（該路線往九龍方向的分段收費則比本線平）。\[4\]\[5\]
  - 2003年[新界區專線小巴88S線取消後](../Page/新界區專線小巴88S線.md "wikilink")，本線乃[美景花園](../Page/美景花園.md "wikilink")、[長青邨青槐樓](../Page/長青邨.md "wikilink")、[長青邨青桃樓](../Page/長青邨.md "wikilink")、[藍澄灣及](../Page/藍澄灣.md "wikilink")[香港專業教育學院青衣分校一帶唯一深宵公共交通工具](../Page/香港專業教育學院青衣分校.md "wikilink")，其它出入青衣之深宵公共交通工具包括[新界區專線小巴402S線](../Page/新界區專線小巴402S線.md "wikilink")、[城巴N21線](../Page/城巴N21線.md "wikilink")、[龍運巴士N31線皆不途徑該等地點](../Page/龍運巴士N31線.md "wikilink")（該等路線只途經青衣西部的屋邨如[長康邨](../Page/長康邨.md "wikilink")、[長亨邨和](../Page/長亨邨.md "wikilink")[長宏邨](../Page/長宏邨.md "wikilink")）。
  - 2014年9月21日或之前，本路線頭車由於要遷就N260線的[美孚頭車時間](../Page/美孚.md "wikilink")，於23:30在[紅磡站開出](../Page/紅磡站.md "wikilink")，是當時各常規通宵線中最早的。

## 參考來源

  -
  -
## 注釋

## 外部連結

  - [九巴N241線官方網站路線資料](http://www.kmb.hk/chinese.php?page=search&prog=route_no.php&route_no=N241)

[N241](../Category/九龍巴士路線.md "wikilink")
[N241](../Category/香港通宵巴士路線.md "wikilink")
[N241](../Category/油尖旺區巴士路線.md "wikilink")
[N241](../Category/葵青區巴士路線.md "wikilink")

1.  香港01，〈[一程通宵巴57個站
    N241搭客深宵「長征」](https://www.hk01.com/%E6%96%B0%E8%81%9E/2117/%E4%B8%80%E7%A8%8B%E9%80%9A%E5%AE%B5%E5%B7%B457%E5%80%8B%E7%AB%99-N241%E6%90%AD%E5%AE%A2%E6%B7%B1%E5%AE%B5-%E9%95%B7%E5%BE%81-)〉［新聞］，2016年1月15日。
2.  香港特別行政區政府運輸署，〈[2017-2018年度葵青區巴士路線計劃](http://www.td.gov.hk/filemanager/tc/util_uarticle_cp/2017-2018%20kwt%20rpp.pdf)〉，2017年3月23日。
3.  [九巴宣傳單張](http://www.kmb.hk/chi/pdf/N41X_leaflet.pdf)
4.  [【青衣島民哀歌．一】搭N車兜足57個站　夜更工友寧在公司hea天光](https://www.hk01.com/社區專題/182406/青衣島民哀歌-一-搭n車兜足57個站-夜更工友寧在公司hea天光)，HK01，2018年4月27日
5.  [【青衣島民哀歌．二】街坊搭N241瞓醒都未返屋企　何不增設小巴？](https://www.hk01.com/%E7%A4%BE%E5%8D%80%E5%B0%88%E9%A1%8C/182414/%E9%9D%92%E8%A1%A3%E5%B3%B6%E6%B0%91%E5%93%80%E6%AD%8C-%E4%BA%8C-%E8%A1%97%E5%9D%8A%E6%90%ADn241%E7%9E%93%E9%86%92%E9%83%BD%E6%9C%AA%E8%BF%94%E5%B1%8B%E4%BC%81-%E4%BD%95%E4%B8%8D%E5%A2%9E%E8%A8%AD%E5%B0%8F%E5%B7%B4)，HK01，2018年4月27日