**[九龍巴士N](../Page/九龍巴士.md "wikilink")48線**是[香港的一條節日特別巴士路線](../Page/香港.md "wikilink")，來往[愉翠苑及](../Page/愉翠苑.md "wikilink")[灣景花園](../Page/灣景花園.md "wikilink")。\[1\]

## 歷史

  - 2003年2月1日：投入服務
  - 2005年[農曆年初一](../Page/農曆新年.md "wikilink")：來回程均繞經[沙田市中心](../Page/沙田市中心.md "wikilink")
  - 2008年農曆年初一：往[沙田方向改經](../Page/沙田_\(香港\).md "wikilink")[大涌道](../Page/大涌道.md "wikilink")、[沙咀道及](../Page/沙咀道.md "wikilink")[大河道](../Page/大河道.md "wikilink")

## 服務時間

只在特定日子行走，詳情請留意九巴公佈

  - [愉翠苑開](../Page/愉翠苑.md "wikilink")：0010-0500（15-25分鐘一班）
  - [灣景花園開](../Page/灣景花園.md "wikilink")：0040-0520（15-25分鐘一班）

## 收費

  - 全程收費：$15.3
      - 可風中學往灣景花園：$9.3
      - 沙田市中心總站往愉翠苑：$8.6

## 途經街道

### [愉翠苑開](../Page/愉翠苑.md "wikilink")

  - [牛皮沙街](../Page/牛皮沙街.md "wikilink")，[插桅杆街](../Page/插桅杆街.md "wikilink")，[銀城街](../Page/銀城街.md "wikilink")，[小瀝源路](../Page/小瀝源路.md "wikilink")，[大涌橋路](../Page/大涌橋路.md "wikilink")，[火炭路](../Page/火炭路.md "wikilink")，[源禾路](../Page/源禾路.md "wikilink")，[沙田鄉事會路](../Page/沙田鄉事會路.md "wikilink")，[大埔公路](../Page/大埔公路.md "wikilink")，[沙田市中心巴士總站](../Page/沙田市中心巴士總站.md "wikilink")，[沙田正街](../Page/沙田正街.md "wikilink")，[橫壆街](../Page/橫壆街.md "wikilink")，源禾路，沙田鄉事會路，大埔公路，[城門隧道公路](../Page/城門隧道公路.md "wikilink")，[城門隧道](../Page/城門隧道.md "wikilink")，[和宜合交匯處](../Page/和宜合交匯處.md "wikilink")，[和宜合道](../Page/和宜合道.md "wikilink")，[昌榮路](../Page/昌榮路.md "wikilink")，昌榮路迴旋處，[青山公路](../Page/青山公路.md "wikilink")，[海安路](../Page/海安路.md "wikilink")，[麗志路及青山公路](../Page/麗志路.md "wikilink")。

### [灣景花園開](../Page/灣景花園.md "wikilink")

  - 青山公路，[大涌道](../Page/大涌道.md "wikilink")，[沙咀道](../Page/沙咀道.md "wikilink")，[大河道](../Page/大河道.md "wikilink")，青山公路，昌榮路迴旋處，昌榮路，和宜合道，和宜合交匯處，城門隧道，城門隧道公路，大埔公路，沙田鄉事會路，大埔公路，沙田市中心巴士總站，沙田正街，橫壆街，源禾路，火炭路，大涌橋路，小瀝源路，銀城街，插桅杆街，牛皮沙街及[翠欣街](../Page/翠欣街.md "wikilink")。

### 車站一覽

| [灣景花園方向](../Page/灣景花園.md "wikilink") | [愉翠苑方向](../Page/愉翠苑.md "wikilink")         |
| ------------------------------------ | ------------------------------------------ |
| **車站**                               | **車站名稱**                                   |
| 1                                    | [愉翠苑巴士總站](../Page/愉翠苑.md "wikilink")       |
| 2                                    | [愉田苑](../Page/愉田苑.md "wikilink")           |
| 3                                    | [銀城商場](../Page/銀城商場.md "wikilink")         |
| 4                                    | [沙田第一城](../Page/沙田第一城.md "wikilink")       |
| 5                                    | [禾輋邨](../Page/禾輋邨.md "wikilink")           |
| 6                                    | [瀝源邨](../Page/瀝源邨.md "wikilink")           |
| 7                                    | [沙田市中心巴士總站](../Page/沙田市中心.md "wikilink")   |
| 8                                    | [城門隧道轉車站](../Page/城門隧道.md "wikilink")（落客站） |
| 9                                    | 城門隧道轉車站（上客站）                               |
| 10                                   | [可風中學](../Page/可風中學（嗇色園主辦）.md "wikilink")  |
| 11                                   | [梨木樹邨](../Page/梨木樹邨.md "wikilink")         |
| 12                                   | [和宜合道運動場](../Page/和宜合道運動場.md "wikilink")   |
| 13                                   | 昌榮路                                        |
| 14                                   | [健全街](../Page/健全街.md "wikilink")           |
| 15                                   | [大窩口站](../Page/大窩口站.md "wikilink")         |
| 16                                   | [眾安街](../Page/眾安街.md "wikilink")           |
| 17                                   | [建明街](../Page/建明街.md "wikilink")           |
| 18                                   | [福來邨永康樓](../Page/福來邨.md "wikilink")        |
| 19                                   | 荃景圍天橋                                      |
| 20                                   | [柴灣角街](../Page/柴灣角街.md "wikilink")         |
| 21                                   | 麗城花園三期                                     |
| 22                                   | 灣景花園總站                                     |
|                                      | 23                                         |

## 參考資料

<references />

  - 《巴士路線發展綱要(2)──荃灣．屯門．元朗》，BSI(香港)，ISBN 9628414720003

[N048](../Category/九龍巴士路線.md "wikilink")
[N048](../Category/九龍巴士特別巴士路線.md "wikilink")
[N048](../Category/荃灣區巴士路線.md "wikilink")
[N048](../Category/沙田區巴士路線.md "wikilink")

1.  <http://www.681busterminal.com/n48.html>