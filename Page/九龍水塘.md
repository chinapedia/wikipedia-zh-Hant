[Kowloon_Byewash_Reservoir_201707.jpg](https://zh.wikipedia.org/wiki/File:Kowloon_Byewash_Reservoir_201707.jpg "fig:Kowloon_Byewash_Reservoir_201707.jpg")
**九龍水塘**是[香港](../Page/香港.md "wikilink")[九龍半島的一組](../Page/九龍半島.md "wikilink")[水塘](../Page/香港水庫.md "wikilink")，由**九龍水塘**（**Kowloon
Reservoir**）、**九龍副水塘**（**Kowloon Byewash
Reservoir**）及**九龍接收水塘**（**Kowloon Reception
Reservoir**）三個水塘組成，為香港地理正中心。水塘位於[金山郊野公園之內](../Page/金山郊野公園.md "wikilink")。雖然水塘群以九龍為名，但根據現時的[地區行政](../Page/香港行政區劃.md "wikilink")，三個水塘同樣座落於[新界](../Page/新界.md "wikilink")[沙田區西南部](../Page/沙田區.md "wikilink")。另一方面，附近的[石梨貝水塘有時會被視為九龍水塘群之一員](../Page/石梨貝水塘.md "wikilink")。

[File:HK_KowloonReservoir.JPG|九龍水塘](File:HK_KowloonReservoir.JPG%7C九龍水塘)
[File:HK_KowloonByewashReservoir.JPG|九龍副水塘](File:HK_KowloonByewashReservoir.JPG%7C九龍副水塘)
[File:HK_KowloonReceptionReservoir.JPG|九龍接收水塘](File:HK_KowloonReceptionReservoir.JPG%7C九龍接收水塘)

## 歷史

[英國於](../Page/英國.md "wikilink")1898年取得[界限街以北的](../Page/界限街.md "wikilink")[新九龍及](../Page/新九龍.md "wikilink")[新界後](../Page/新界.md "wikilink")，為了提供食水給新九龍及新界南部，香港政府於1901年在[針山以南及](../Page/針山.md "wikilink")[畢架山之西一帶展開興建水塘的工程](../Page/畢架山.md "wikilink")，並於1910年落成啟用。水塘容量達3.5億[加侖](../Page/加侖.md "wikilink")，是新界的首個水塘。1922年堤壩改以石建成，並將高度提高至100[呎](../Page/呎.md "wikilink")，令水塘容量增加150萬加侖。堤壩的外形為弧形，設計獨特。

1926年，香港政府又建造了九龍接收水塘（俗稱德羅塘），以接收[城門水塘的食水](../Page/城門水塘.md "wikilink")，再輸送到石梨貝濾水廠過濾。1930年，九龍副水塘（俗稱新塘）興建，並於1931年建成，儲水量為1億8,500萬加侖。

政府在2009年將九龍水塘5項歷史建築物列為[香港法定古蹟](../Page/香港法定古蹟.md "wikilink")，包括主壩、主壩水掣房、溢洪壩、溢洪壩記錄儀器房及記錄儀器房。

## 九龍水塘群

<table>
<thead>
<tr class="header">
<th><p>名稱</p></th>
<th><p>竣工年份</p></th>
<th><p>容量<br />
（百萬立方米）</p></th>
<th><p>主壩長/高度<br />
（米）</p></th>
<th><p>郊野公園</p></th>
<th><p>行政區劃</p></th>
<th><p>鄰近道路/<br />
行人徑</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/九龍水塘.md" title="wikilink">九龍水塘</a></p></td>
<td><p>1906年</p></td>
<td><p>1.578</p></td>
<td><p>182.9 / 33.2</p></td>
<td><p><a href="../Page/金山郊野公園.md" title="wikilink">金山</a></p></td>
<td><p><a href="../Page/沙田區.md" title="wikilink">沙田區</a></p></td>
<td><p><a href="../Page/大埔公路.md" title="wikilink">大埔公路</a>（沙田段）、<br />
金山路、<br />
<a href="../Page/麥理浩徑.md" title="wikilink">麥理浩徑第</a>6段、<br />
<a href="../Page/衛奕信徑.md" title="wikilink">衛奕信徑第</a>6段、<br />
金山樹木研習徑<sup>1</sup> <sup>4</sup></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/石梨貝水塘.md" title="wikilink">石梨貝水塘</a></p></td>
<td><p>1925年</p></td>
<td><p>0.374</p></td>
<td><p>83.5 / 22.3</p></td>
<td><p>石梨貝水塘緩跑徑<sup>1</sup> <sup>4</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/九龍接收水塘.md" title="wikilink">九龍接收水塘</a></p></td>
<td><p>1926年</p></td>
<td><p>0.121</p></td>
<td><p>70.1 / 13.0</p></td>
<td><p>德羅塘緩跑徑<sup>2</sup> <sup>3</sup>、<br />
金山樹木研習徑<sup>1</sup> <sup>4</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/九龍副水塘.md" title="wikilink">九龍副水塘</a></p></td>
<td><p>1931年</p></td>
<td><p>0.800</p></td>
<td><p>106.0 / 41.1</p></td>
<td><p>金山路、<br />
長源路、<br />
金山樹木研習徑<sup>1</sup> <sup>4</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<sup>1</sup> - 經金山路進入
<sup>2</sup> - 經長源路進入
<sup>3</sup> - 經石梨貝水塘緩跑徑進入
<sup>4</sup> - 經德羅塘緩跑徑進入
<sup>5</sup> - 經金山樹木研習徑進入

## 參見

  - [石梨貝水塘](../Page/石梨貝水塘.md "wikilink")
  - [金山郊野公園](../Page/金山郊野公園.md "wikilink")

## 外部連結

  - [香港水務署 —
    九龍水塘及石梨貝水塘](https://web.archive.org/web/20160304110111/http://www.wsd.gov.hk/tc/education/history/the_legacy_of_waterworks/heritages_at_kowloon_reservoir_group/index.html)

## 遠足資料

  -
[Category:香港水庫](../Category/香港水庫.md "wikilink")
[Category:沙田區](../Category/沙田區.md "wikilink")