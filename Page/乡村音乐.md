**鄉村音樂**（），也稱為**鄉村與西部**（）或簡稱為**鄉村**（），是一種當代的[流行音樂](../Page/流行音樂.md "wikilink")，起源於[美國南部與](../Page/美國南部.md "wikilink")[阿帕拉契山區](../Page/阿帕拉契山脈.md "wikilink")。鄉村音樂的根源可追溯至1920年代，融合了[傳統民謠音樂](../Page/民俗音樂.md "wikilink")、[凱爾特音樂](../Page/凱爾特音樂.md "wikilink")、[福音音樂](../Page/福音音樂.md "wikilink")、[藍調及](../Page/藍調.md "wikilink")[美國民間音樂](../Page/美國民間音樂.md "wikilink")<small>\[1\]</small>。1940年代，當[鄉巴佬音樂](../Page/鄉巴佬音樂.md "wikilink")（*hillbilly
music*）地位漸衰時，人們開始以統一的術語「鄉村音樂」作為稱呼，1970年代更大大普及，並在世界各地取代了「鄉村與西部」的稱呼<small>\[2\]</small>。

## 起源

**鄉村音樂**這個術語今天被用來描述許多風格和其他子流派音樂風格。鄉村音樂的起源是[美國工人階級的](../Page/美國工人階級.md "wikilink")[民間音樂](../Page/美國民間音樂.md "wikilink")，融合了[流行歌曲](../Page/流行歌曲.md "wikilink")、[愛爾蘭和](../Page/愛爾蘭.md "wikilink")[凱爾特的小提琴曲](../Page/凱爾特.md "wikilink")、[傳統英國民謠和美國西部](../Page/傳統英國民謠.md "wikilink")[牛仔歌曲](../Page/牛仔歌曲.md "wikilink")，以及來自歐洲移民社區的各種音樂傳統。\[3\]\[4\]\[5\]歷史紀錄上，已被廣泛應用於鄉村音樂。\[6\]

## 影響

鄉村音樂出了兩位十分知名的暢銷歌手。一位是[艾維斯·普里斯萊](../Page/艾維斯·普里斯萊.md "wikilink")，人稱[貓王](../Page/貓王.md "wikilink")，同時也是新音樂類型「[搖滾樂](../Page/搖滾.md "wikilink")」的代表人物。另一位，當代音樂家[葛司·布魯克斯](../Page/葛司·布魯克斯.md "wikilink")（*Garth
Brooks*），擁有一億兩千八百萬專輯銷售量，則是美國史上最暢銷的歌手<small>\[7\]</small>。他個人就擁有六張[鑽石唱片](../Page/钻石唱片.md "wikilink")，和[披頭士樂團並列為擁有最多](../Page/披頭士.md "wikilink")[鑽石唱片的藝人](../Page/钻石唱片.md "wikilink")\[8\]。

## 參考文獻

[Category:美國民間音樂](../Category/美國民間音樂.md "wikilink")
[Category:美國音樂](../Category/美國音樂.md "wikilink")
[Category:鄉村音樂](../Category/鄉村音樂.md "wikilink")

1.  Peterson, Richard A. (1999). *Creating Country Music: Fabricating
    Authenticity*, p.9. ISBN 0-226-66285-3.

2.
3.

4.

5.  Country music - Definition from WordWeb <http://wordweb.info/free/>

6.

7.  [1](http://www.acountry.com/music/2008_academy_of_country_music_awards_winners/)
    "The Academy of Country Music also honored Garth Brooks with the
    first-ever Crystal Milestone Award. The award is given to an artist
    or industry leader to commemorate a specific, remarkable
    achievement. With 128 million unit sales, Brooks has been certified
    the top-selling solo recording artist in U.S. history."

8.  [美國唱片協會官網鑽石唱片列表](http://www.riaa.com/goldandplatinum.php?content_selector=top-100-albums)