**書法**()是书写文字的方法，一種書寫的[藝術](../Page/藝術.md "wikilink")。書法縱向穿透兩個層面：「使用」與「審美」；橫向跨越文字兩個層面：「[文字](../Page/文字.md "wikilink")」、「[繪畫](../Page/繪畫.md "wikilink")」。書法也是一種研究書寫者心理與性格的工具。當代對實踐書法的定義有：
[Abiword_Mac.png](https://zh.wikipedia.org/wiki/File:Abiword_Mac.png "fig:Abiword_Mac.png")

  - 「一種賦予[符號有和諧協調](../Page/符號.md "wikilink")、富表情、靈巧孰練風格的藝術」\[1\]
  - 「筆跡的經歷是[美學的演變](../Page/美學.md "wikilink")，受人、時、地的技巧、物料、傳播速率所局限」\[2\]
  - 「筆跡的風格可說成「手跡」、「手藝」、「字母」」\[3\]

現代書法範圍很广泛，從功能性[題字及](../Page/題字.md "wikilink")[刻字設計](../Page/刻字.md "wikilink")，到精緻藝術的[抽象手寫標記](../Page/抽象.md "wikilink")，文字可能易懂，也可能不考慮其易讀性\[4\]。古典書法有別於[字体排印学和非古典的手寫字](../Page/字体排印学.md "wikilink")，不過[書法家可能也熟悉這些部份](../Page/書法家.md "wikilink")\[5\]\[6\]\[7\]\[8\]。

書法仍常使用在[喜帖及邀請函](../Page/喜帖.md "wikilink")、[字型設計及](../Page/字型.md "wikilink")[字体排印学](../Page/字体排印学.md "wikilink")、原創手寫字母或字的[标志設計](../Page/标志.md "wikilink")、[宗教藝術](../Page/宗教藝術.md "wikilink")、公告、[圖案設計及书法藝術](../Page/圖案設計.md "wikilink")、石刻題字及紀念文獻等。書法也常用在[電影及](../Page/電影.md "wikilink")[電視的](../Page/電視.md "wikilink")[道具或動態圖像](../Page/道具.md "wikilink")、推薦信、[出生證明書](../Page/出生證明書.md "wikilink")、[死亡證明書](../Page/死亡證.md "wikilink")、地圖等場合\[9\]\[10\]

## 東亞書法

[Liang's_calligraphy.jpg](https://zh.wikipedia.org/wiki/File:Liang's_calligraphy.jpg "fig:Liang's_calligraphy.jpg")的書法作品\]\]
東亞書法的典型概念是「蘸[墨](../Page/墨.md "wikilink")、握[毛筆](../Page/毛筆.md "wikilink")、寫[漢字](../Page/漢字.md "wikilink")」，講求[線條揮灑中透露出來的那股書卷氣](../Page/線條.md "wikilink")。書法被認為是[東亞的重要藝術和最優雅的寫字形式](../Page/東亞.md "wikilink")。寫好漢字，歷來都是一種素養，它源自[中國書法](../Page/中國書法.md "wikilink")，受東亞文明社會敬重並被廣泛實踐，包括：中國、[日本](../Page/日本.md "wikilink")、[越南](../Page/越南.md "wikilink")、[朝鮮](../Page/朝鮮半島.md "wikilink")。
[中國書法是東亞書法中最典型的代表](../Page/中國書法.md "wikilink")，也是東亞書法的統稱，因為書寫的字體大多是中國的文字[漢字](../Page/漢字.md "wikilink")，所有的東亞書法也是最早由中國傳過去而演變出來的。
除傳統[文房四寶外](../Page/文房四寶.md "wikilink")，近來因書寫工具的革新，更有[硬筆書法的興起](../Page/硬筆書法.md "wikilink")。

## 蒙古書法

[Guyuk_khan's_Stamp_1246.jpg](https://zh.wikipedia.org/wiki/File:Guyuk_khan's_Stamp_1246.jpg "fig:Guyuk_khan's_Stamp_1246.jpg")上用的即為蒙古書法，這是在[贵由寄給教宗](../Page/贵由.md "wikilink")[依諾增爵四世的信上找到的](../Page/依諾增爵四世.md "wikilink")\]\]
是[蒙古的書法](../Page/蒙古.md "wikilink")，和東亞書法類似，蒙古書法主要也是用[毛筆寫成](../Page/毛筆.md "wikilink")\[11\]。蒙古書法使用傳統的[蒙古字母](../Page/蒙古字母.md "wikilink")，在2013年，蒙古書法列名在[人类非物质文化遗产代表作名录中](../Page/人类非物质文化遗产代表作名录.md "wikilink")\[12\]。

## 南亞書法

### 尼泊爾書法

尼泊爾書法主要是由[蘭札文所寫](../Page/蘭札文.md "wikilink")，其文字本身及蘭擦文（Lanydza
Script）、帕巴文（Phagpa
Script）或庫蒂拉文（Kutila）等衍生文字會用在[西藏](../Page/西藏.md "wikilink")、[尼泊爾](../Page/尼泊爾.md "wikilink")、[不丹](../Page/不丹.md "wikilink")、[列城](../Page/列城.md "wikilink")、[蒙古](../Page/蒙古.md "wikilink")、中國沿海、日本及韓國等地，用在寫[六字真言或是其他衍生自](../Page/六字真言.md "wikilink")[梵语或](../Page/梵语.md "wikilink")[巴利语的](../Page/巴利语.md "wikilink")[佛经文字](../Page/佛经.md "wikilink")。

### 泰國

泰國書法主要是以[梵语所寫](../Page/梵语.md "wikilink")。在歷史上泰國書法曾限制在巴利语經典的神聖文字中。
泰書法會出現在泰國皇家成員的個人旗幟中，會將姓名的首字用泰國書法表示。 一些書也曾用泰國書法的組合方式印刷。

### 藏字書法

[BonpoBook.jpg](https://zh.wikipedia.org/wiki/File:BonpoBook.jpg "fig:BonpoBook.jpg")文字\]\]
[西藏屬](../Page/西藏.md "wikilink")[印度文化圈一部分](../Page/印度文化圈.md "wikilink")，有好幾百年間曾是[佛教心臟地帶](../Page/佛教.md "wikilink")，藏字文字起源於[印度文字](../Page/婆罗米系文字.md "wikilink")，其文字在宗教區很重要，是其重要文化。西藏的貴族如[喇嘛和](../Page/喇嘛.md "wikilink")[布達拉宮的居住者](../Page/布達拉宮.md "wikilink")，通常擅於書法。藏字書法中和[世俗有關的部份不多](../Page/世俗.md "wikilink")，但確實存在（而某個程度也和藏傳佛教有關）。幾乎所有宗教高層人士手寫的文件都和藏字書法有關，包括[達賴喇嘛寄出的信件](../Page/達賴喇嘛.md "wikilink")，以及其他宗教界及世俗高層人士的手寫文件中。藏字書法在其[轉經筒上格外明顯](../Page/轉經筒.md "wikilink")，不過這些文字是鍛造出來的，而不是刻劃出來的。藏字書法最早是用蘆葦所寫，現在多半也用凿尖的笔書寫。

## 西洋（希臘、拉丁）書法

西方書法與傳統中國書法不同的是，西方書法很少用毛筆寫的，而是用硬筆與紙面呈一定角度書寫而成，其中最有名的筆為[鵝毛筆](../Page/鵝毛筆.md "wikilink")。早期的[字母是大約西元前](../Page/字母.md "wikilink")3000年發明的。從[希臘字母到](../Page/希臘字母.md "wikilink")[拉丁字母](../Page/拉丁字母.md "wikilink")，一開始先發展[大寫體](../Page/大寫體.md "wikilink")，後來才發展[小寫體](../Page/小寫體.md "wikilink")。

[羅馬人的第一批書接著出現](../Page/羅馬人.md "wikilink")。這批書只有簡單的幾個摺頁，而且是用[羊皮紙做的](../Page/羊皮紙.md "wikilink")，[蘆葦筆在這時也被](../Page/蘆葦筆.md "wikilink")[鵝毛筆取代](../Page/鵝毛筆.md "wikilink")。[基督教信仰在此時給予西方書法發展上很重要的助力](../Page/基督教.md "wikilink")。因為這时抄寫[聖經和其他聖書的活動非常盛行](../Page/聖經.md "wikilink")。在[愛爾蘭](../Page/愛爾蘭.md "wikilink")，僧侶過去都使用[安色尔体](../Page/安色尔体.md "wikilink")（Uncial
letters）。在[蘇格蘭和其他地方](../Page/蘇格蘭.md "wikilink")，從這之後開始以「島國風格」稱呼這種書寫方式，這也是[泥金裝飾手抄本的全盛時期](../Page/泥金裝飾手抄本.md "wikilink")。

[查理曼接著為西方書法帶來劇變](../Page/查理曼.md "wikilink")。他任用約克大修道院院長[阿尔琴](../Page/阿尔琴.md "wikilink")（Alcuin），阿尔琴來到查理曼的首都[亞琛](../Page/亞琛.md "wikilink")，並擔下了將所有文字體修定的重責大任。他接著也為查里曼大帝發展了新的手寫體並命名為「」（Carolingian
minuscule style）。

[哥德字母接著在](../Page/哥德字母.md "wikilink")11世紀出現了，雖該字母不流行，但名稱相似的[哥德體成為中世晚期書法的主流](../Page/哥德體.md "wikilink")。[義大利也有](../Page/義大利.md "wikilink")（Chancery
hand）和[義大利手寫斜體](../Page/意大利体.md "wikilink")。這時候的泥金裝飾手抄本的字體也被影響了，因此又盛行了一陣子。然而15世紀後，[約翰·古騰堡發明的](../Page/約翰·古騰堡.md "wikilink")[活字印刷術](../Page/活字印刷術.md "wikilink")，使手寫和手工裝飾的書很快的就退出流行了，不過西方書法並未就此消失。

[Westerncalligraphy.jpg](https://zh.wikipedia.org/wiki/File:Westerncalligraphy.jpg "fig:Westerncalligraphy.jpg")
19世紀末，[威廉·莫里斯和](../Page/威廉·莫里斯.md "wikilink")[英國](../Page/英國.md "wikilink")[艺术与手工艺运动的其他人](../Page/艺术与手工艺运动.md "wikilink")，重新發現並再度發揚西方書法之美。有許多書法家如、等，都受到了他的影響。

西方重要的當代書法家是與[赫尔曼·察普夫](../Page/赫尔曼·察普夫.md "wikilink")。隨著手寫在溝通行為中越來越少見，西方書法逐漸只在特殊情況與活動中見到，大部份用在[喜帖與通知上面](../Page/喜帖.md "wikilink")。

## 伊斯兰书法

[Basmalah-1wm.svg](https://zh.wikipedia.org/wiki/File:Basmalah-1wm.svg "fig:Basmalah-1wm.svg")的伊斯蘭書法，內容是[太斯米](../Page/太斯米.md "wikilink")\]\]

伊斯兰书法是隨著[伊斯兰教及](../Page/伊斯兰教.md "wikilink")[阿拉伯文所進展的](../Page/阿拉伯文.md "wikilink")。因為用的是阿拉伯字母，有些人也稱為「阿拉伯書法」，但伊斯兰书法是比較準確的名詞，包括各種伊斯兰书法家創作的作品，地點從[西班牙南部的](../Page/西班牙.md "wikilink")[安達魯西亞到中國](../Page/安達魯西亞.md "wikilink")。

[伊斯兰教的国家裡](../Page/伊斯兰教.md "wikilink")，書法與西方書法用的筆大致上是相同的。由于伊斯兰教禁止[偶像崇拜](../Page/偶像崇拜.md "wikilink")，没有像基督教世界里那样存在大量宗教题材的绘画，于是使用[阿拉伯字母的书法作品成了最重要的视觉艺术品](../Page/阿拉伯字母.md "wikilink")。从[西班牙到](../Page/西班牙.md "wikilink")[印度](../Page/印度.md "wikilink")，用阿拉伯字母书写的书法作品随处可见。

像[清真寺牆上及天花板上的](../Page/清真寺.md "wikilink")[阿拉伯式花紋就是伊斯兰书法](../Page/阿拉伯式花紋.md "wikilink")。[穆斯林世界的當代藝術家也傳承伊斯兰书法](../Page/穆斯林世界.md "wikilink")，用在题字或抽象圖案上。

伊斯兰书法是伊斯兰世界中視覺藝術的最高境界，也是其[靈性世界的藝術](../Page/靈性.md "wikilink")。毫无争议地，伊斯兰书法已经成为伊斯兰艺术中最崇敬的形式，因为它提供了与伊斯兰教和穆斯林的语言之间的联系。[古兰经在阿拉伯語的發展及演化上有重要的角色](../Page/古兰经.md "wikilink")，也延伸到伊斯兰书法中。諺語及古兰经中的經文仍是伊斯兰书法的重要來源。

一般認為伊斯兰书法在[鄂圖曼帝國表現最為出色](../Page/鄂圖曼帝國.md "wikilink")。土耳其的伊斯兰书法家有最精緻及最有創造力的作品。伊斯坦堡是各種伊斯兰书法及的开放展馆，在[清真寺](../Page/清真寺.md "wikilink")、喷泉、学校及住宅的铭文都是伊斯兰书法。

近现代及当代伊斯兰书法，则集中在土耳其、埃及和伊拉克三国，代表人物有土耳其的哈米德·阿梅迪、埃及的赛义德·易卜拉欣和伊拉克的哈希姆·穆罕默德\[13\]。1986年举行的首届国际阿拉伯书法大赛，除[沙特阿拉伯和叙利亚各获得一银一铜外](../Page/沙特阿拉伯.md "wikilink")，其余所有奖牌均被土耳其书法家囊括\[14\]。

## 瑪雅文化

瑪雅书法是以[瑪雅文字來表示](../Page/瑪雅文字.md "wikilink")，現在的瑪雅书法主要用在[墨西哥](../Page/墨西哥.md "wikilink")[尤卡坦州的](../Page/尤卡坦州.md "wikilink")[印章及纪念碑中](../Page/印章.md "wikilink")。瑪雅书法很少用在官方機構中，不過在[坎佩切州及](../Page/坎佩切州.md "wikilink")[金塔納羅奧州等地](../Page/金塔納羅奧州.md "wikilink")，瑪雅书法是用拉丁文字表示。有些南墨西哥的公司會在其企業符號使用瑪雅书法。有些社群及現代的瑪雅兄弟會也會用瑪雅书法作為組織的符號。

大部分墨西哥的考古遗址（像在[契琴伊薩](../Page/契琴伊薩.md "wikilink")、、[乌斯马尔](../Page/乌斯马尔.md "wikilink")、[卡拉克穆尔等地](../Page/卡拉克穆尔.md "wikilink")）會有其瑪雅书法。石刻纪念碑也常见到古老的玛雅书法。

## 圖集

<File:Ijazah3.jpg>|[鄂圖曼帝國](../Page/鄂圖曼帝國.md "wikilink")[教統](../Page/教統.md "wikilink")，以阿拉伯文寫成，是在西元1791年，[伊斯兰历](../Page/伊斯兰历.md "wikilink")1206年
<File:Chayyam> guyand kasan behescht ba hur chosch ast
small.png|Shikasta [波斯体文字](../Page/波斯体.md "wikilink") <File:Asemic>
graffiti.jpg|及抽象书法
[File:Calligraphy_demonstration_at_the_Ukrainian_Cultural_Center_in_Tallinn,_Estonia.webm|在](File:Calligraphy_demonstration_at_the_Ukrainian_Cultural_Center_in_Tallinn,_Estonia.webm%7C在)[爱沙尼亚首都](../Page/爱沙尼亚.md "wikilink")[塔林中](../Page/塔林.md "wikilink")[乌克兰文化中心的书法展示](../Page/乌克兰.md "wikilink")

## 参见

  - [書寫](../Page/書寫.md "wikilink")
  - [美術字體](../Page/美術字體.md "wikilink")
  - [古文字学](../Page/古文字学.md "wikilink")
  - [婆罗米文](../Page/婆罗米文.md "wikilink")
  - [具象詩](../Page/具象詩.md "wikilink")
  - [手写体](../Page/手写体.md "wikilink")

## 相關

  - [毛筆](../Page/毛筆.md "wikilink")
  - [文房四寶](../Page/文房四寶.md "wikilink")
  - [漢字](../Page/漢字.md "wikilink")
  - [楷書九十二法](../Page/楷書九十二法.md "wikilink")

## [書法家](../Page/書法家.md "wikilink")

書法家是以書法為主要創作的藝術家。

  - [中國書法家列表](../Page/中國書法家列表.md "wikilink")

## 参考文献

  - 書目

<!-- end list -->

  -
  -
  -
## 外部連結

  - [French Renaissance
    Paleography](https://paleography.library.utoronto.ca) This is a
    scholarly maintained site that presents over 100 carefully selected
    French manuscripts from 1300 to 1700, with tools to decipher and
    transcribe them.
  - [Kallipos](http://www.kallipos.de/)
  - [Manuscript Pen Company](http://www.calligraphy.co.uk)
  - [Scribblers](http://www.scribblers.co.uk)
  - [Blam\! Design](http://hans.presto.tripod.com/engli001.html)
  - [Calligraphy](http://webarchive.loc.gov/all/20011109101848/http://dmoz.org/arts/visual_arts/calligraphy/)
    at [DMOZ](../Page/DMOZ.md "wikilink")

<!-- end list -->

  - 书法博物館

<!-- end list -->

  - [The Pen Museum](http://www.birminghamheritage.org.uk/pentrade.html)
    at Birmingham Heritage forum
  - [Birmingham Pen
    Room](https://web.archive.org/web/20081220044310/http://penroom.co.uk/default.aspx),
    writing and pen museum (Archive)
  - [Contemporary museum of calligraphy
    (Russia)](https://web.archive.org/web/20130417191341/http://www.calligraphy-museum.com/eng/default.aspx)
  - [Schrift-und Heimatmuseum, Pettenbach
    (Austria)](http://www.schriftmuseum.at/)
  - [Hill Museum & Manuscript Library](http://www.hmml.org/)
  - [Klingspor
    Museum](http://www.klingspor-museum.de/UeberdasMuseum.html)
  - [Manuscript Museum of the Library of
    Alexandria](https://web.archive.org/web/20141019061047/http://www.manuscriptcenter.org/museum/)
  - [The Karpeles Manuscript Library
    Museums](http://www.rain.org/~karpeles/)
  - [The Modern Calligraphy Collection of the National Art Library at
    the Victoria and Albert
    Museum](http://www.vam.ac.uk/content/articles/n/nal-modern-calligraphy/)

[書法](../Category/書法.md "wikilink")
[Category:排版](../Category/排版.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12. [中華民國 文化部文化資產局 　臺灣世界遺產潛力點
    蒙古書法](http://twh.boch.gov.tw/non_material/intro.aspx?id=299)
13. 陈进惠. 近现代阿拉伯书法的开拓先锋\[J\]. 中国穆斯林. 2007(05)
14. 李文彦. 浅谈阿拉伯书法艺术\[J\]. 阿拉伯世界. 1989(01)