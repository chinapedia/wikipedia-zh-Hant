[Koe_in_weiland_bij_Gorssel.JPG](https://zh.wikipedia.org/wiki/File:Koe_in_weiland_bij_Gorssel.JPG "fig:Koe_in_weiland_bij_Gorssel.JPG")
**乳牛**（也称**奶牛**）是专门培养出来产[牛奶的](../Page/牛奶.md "wikilink")[母牛](../Page/母牛.md "wikilink")。

## 簡介

一般未曾生育的母牛不产[牛奶](../Page/牛奶.md "wikilink")，更加不会长时间产出人類借以飲用的[鮮奶](../Page/鮮奶.md "wikilink")，母牛-{只}-有在生產了幼牛后才会产奶。一般一头母牛应该在两岁时产第一头幼牛，随后约每年产一头幼牛。有时两头幼牛之间的间隔也会长一些，但这对产奶不利。

今天大多数母牛是通过[人工授精的方法](../Page/人工授精.md "wikilink")[受精的](../Page/受精.md "wikilink")，其优点在于农民不必饲养公牛，而且能够保证生产出来的小牛品質良好。这样对此感兴趣的农民也可以学习繁殖技术。此外人工授精时传播传染病的可能性比自然授精要小。

## 時間

为了使得乳牛的[乳房获得必要的休息](../Page/乳房.md "wikilink")，乳牛在产[小牛](../Page/小牛.md "wikilink")60天前就不再被挤奶了。在这段时间里乳房中的分泌组织获得再生。此外假如本来乳房被感染的话也可以获得治疗。受感染的乳房会影响奶的品質。在现代化的产奶企业中小牛一產下就与母牛分离。一开始几天里它还获得母亲的[牛初乳](../Page/牛初乳.md "wikilink")，这些初乳中含有重要的预防疾病的抗体。此后小牛获得代替饲料。公小牛一般被卖给肉牛农场，母小牛则尽快获得含高脂肪的饲料来促进其[瘤胃的发育](../Page/瘤胃.md "wikilink")，使得它尽快成为一头乳牛。

乳牛的怀孕期为280天。小牛出生后就開始重新产奶。在一开始的六個星期中奶量不断提高，一直到每日25至60升，然后不斷下降，直到下一個小牛出生。

一頭牛的自然壽命約是20年，但反覆地產小牛和產奶的乳牛的壽命只有六至七年。

## 產量

[Milk_glass.jpg](https://zh.wikipedia.org/wiki/File:Milk_glass.jpg "fig:Milk_glass.jpg")
一头乳牛每年可以产至1.8万升奶（极少数以上），随地区和种类的不同牛的产奶量不同。产量越高的牛也越难饲养。一头每天产50升牛隻的新陈代谢能量与一个马拉松长跑运动员运动时一样高。假如它不能获得适量饲料的话会生病，其生产时间也会降低。

除奶量外奶的脂肪和蛋白质含量也非常重要，是决定其价格的重要因素。对于两用牛（比如[西门塔尔牛](../Page/西门塔尔牛.md "wikilink")）来说，产奶能力和肉用能力都非常重要。

所谓“高产量乳牛”的产奶期在四至五年之间。最主要缩短产奶期的原因有不当饲养环境以及由不优质挤奶机造成乳房的慢性发炎。高产量乳牛的产子率也比较低，过一段时间后它们完全丧失产子能力。
[FeedingCows.JPG](https://zh.wikipedia.org/wiki/File:FeedingCows.JPG "fig:FeedingCows.JPG")

## 品質辨別

总的来说生奶中含的细菌和[體细胞应该尽量少](../Page/體细胞.md "wikilink")。生奶中含[细菌多一般代表挤奶机不清洁](../Page/细菌.md "wikilink")，或者存放时冷却有问题。生奶中含體细胞多则说明乳房有病。这些體细胞是免疫细胞，就像人類的白血球，它们会在乳房有急性或者慢性发炎时进入奶。[乳房发炎的原因很多](../Page/乳房发炎.md "wikilink")。饲养环境不清洁、挤奶时不清洁、挤奶擠不好、产奶时间过长、传染病、饲料不良、遗传因素均可能影响乳房卫生和生奶的品質。乳牛的乳房大一般不代表牠的产奶量高，而是其[结缔组织虚弱](../Page/结缔组织.md "wikilink")。而一头乳牛每天进食和[反芻时约咀嚼三万次](../Page/反芻.md "wikilink")，并产生90升[唾液](../Page/唾液.md "wikilink")。

## 主要的乳牛品種

  - [荷斯坦牛](../Page/荷斯坦牛.md "wikilink")（Holstein）
  - [娟珊牛](../Page/娟珊牛.md "wikilink")（Jersey）
  - [瑞士褐牛](../Page/瑞士褐牛.md "wikilink")（Brown Swiss）
  - [更賽牛](../Page/更賽牛.md "wikilink")（Guernsey）
  - [愛爾夏牛](../Page/愛爾夏牛.md "wikilink")（Ayrshire）
  - [乳用短角牛](../Page/乳用短角牛.md "wikilink")（Milking Shorthorn）

## 另見

  - [肉牛](../Page/肉牛.md "wikilink")

[Category:牛屬](../Category/牛屬.md "wikilink")
[Category:乳品业](../Category/乳品业.md "wikilink")
[Category:牛科](../Category/牛科.md "wikilink")
[Category:家牛品种](../Category/家牛品种.md "wikilink")