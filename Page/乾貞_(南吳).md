**乾貞**（927年十一月－929年十月）是[南吴睿帝](../Page/南吴.md "wikilink")[杨溥的](../Page/杨溥_\(吴\).md "wikilink")[年號](../Page/年號.md "wikilink")，共计3年。

928年六月[荆南武信王](../Page/荆南.md "wikilink")[高季兴被吴封为秦王](../Page/高季兴.md "wikilink")，用此年号。文獻王[高从诲继续沿用此年号到](../Page/高从诲.md "wikilink")929年六月\[1\]。

## 紀年

| 乾貞                               | 元年                             | 二年                             | 三年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 927年                           | 928年                           | 929年                           |
| [干支](../Page/干支纪年.md "wikilink") | [丁亥](../Page/丁亥.md "wikilink") | [戊子](../Page/戊子.md "wikilink") | [己丑](../Page/己丑.md "wikilink") |

## 參看

  - [中國年號索引](../Page/中國年號索引.md "wikilink")
  - 其他政权使用的[乾貞年号](../Page/乾貞.md "wikilink")
  - 同期存在的其他政权年号
      - [天成](../Page/天成_\(李嗣源\).md "wikilink")（926年四月至930年二月）：後唐—[李嗣源之年號](../Page/李嗣源.md "wikilink")
      - [宝正](../Page/宝正.md "wikilink")（926年至931年）：吳越—錢鏐之年號
      - [白龍](../Page/白龍.md "wikilink")（925年十二月至928年二月）：南漢—劉龑之年號
      - [大有](../Page/大有_\(年號\).md "wikilink")（928年三月至942年三月）：南漢—劉龑之年號
      - [天應](../Page/天應_\(鄭隆亶\).md "wikilink")（927年）：[大長和](../Page/大長和.md "wikilink")—[鄭隆亶之年號](../Page/鄭隆亶.md "wikilink")
      - [尊聖](../Page/尊聖.md "wikilink")（928年至929年）：[大天興](../Page/大天興.md "wikilink")—[趙善政之年號](../Page/趙善政.md "wikilink")
      - [天顯](../Page/天顯.md "wikilink")（926年二月至938年）：[契丹](../Page/遼朝.md "wikilink")—[耶律阿保機](../Page/耶律阿保機.md "wikilink")、[耶律德光之年號](../Page/耶律德光.md "wikilink")
      - [甘露](../Page/甘露_\(耶律倍\).md "wikilink")（926年至936年）：[東丹](../Page/東丹.md "wikilink")—[耶律倍之年號](../Page/耶律倍.md "wikilink")
      - [同慶](../Page/同慶_\(尉遲烏僧波\).md "wikilink")（912年至966年）：[于闐](../Page/于闐.md "wikilink")—[尉遲烏僧波之年號](../Page/尉遲烏僧波.md "wikilink")
      - [天授](../Page/天授_\(高麗太祖\).md "wikilink")（918年－933年）：[高麗](../Page/高麗.md "wikilink")—[高麗太祖王建之年號](../Page/高麗太祖.md "wikilink")
      - [延長](../Page/延長_\(醍醐天皇\).md "wikilink")（923年閏四月十一日至931年四月二十六日）：平安時代醍醐天皇之年號

## 参考文献

1.  [徐红岚](../Page/徐红岚.md "wikilink")，《中日朝三国历史纪年表》，辽宁教育出版社，1998年5月 ISBN
    7538246193

[Category:南吴年号](../Category/南吴年号.md "wikilink")
[Category:荆南年号](../Category/荆南年号.md "wikilink")
[Category:920年代中国政治](../Category/920年代中国政治.md "wikilink")

1.  李崇智，中国历代年号考，中华书局，2004年12月，156 ISBN 7101025129