《**亂Online**》是[大型多人在线角色扮演游戏](../Page/大型多人在线角色扮演游戏.md "wikilink")，遊戲風格屬於校園環境，是一款不限地區，是終身免費的[網路遊戲](../Page/網路遊戲.md "wikilink")。在遊戲或官方網站常會看到「亂」字出現，正是強調該款遊戲的名稱。

## 遊戲營運

### 營運狀況

《勇Online》是由[韓國Min](../Page/韓國.md "wikilink")
Communications,開發，由[飛雅高科技](../Page/飛雅高科技.md "wikilink")（Feya
Technologies Co.,
Ltd.）授權在[台灣代理](../Page/台灣.md "wikilink")，在2005年2月24日開始封測，同年4月12日開放公測。而香港方面由雲起遊戲代理，在2005年4月16至4月26進行封測\[1\]，並於同年4月29日中午12:00正式公開測試\[2\]，香港地區已停止營運\[3\]。

2005年8月1日由「[英屬維京群島商](../Page/英屬維京群島.md "wikilink")[依斯楚數位娛樂有限公司台灣分公司](../Page/依斯楚數位娛樂.md "wikilink")」（XtraVision
Entertainment
Ltd.）接手，另有[日本](../Page/日本.md "wikilink")、[大陸](../Page/大陸.md "wikilink")、[香港](../Page/香港.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[菲律賓等地區伺服器](../Page/菲律賓.md "wikilink")。2007年5月25日，依斯楚數位娛樂有限公司與Min
Communications, Inc.發生糾紛\[4\]。2007年10月29日，由Min Communications,
Inc.在台設立新公司「韓商[銘信數位娛樂有限公司台灣分公司](../Page/銘信數位娛樂.md "wikilink")」（Min
Communications Co., Ltd.；簡稱「Min-TW」）營運，包裝《[勇
Online](../Page/勇_Online.md "wikilink")》重新推出，而《亂
Online》在依斯楚仍正常營運。2009年4月8日，[行政院公平交易委員會第](../Page/行政院公平交易委員會.md "wikilink")909次委員會議決議，認定依斯楚數位娛樂與銘信數位娛樂「雙方各自在其官方網站上發布不利對方之聲明稿」之行為屬於「為競爭之目的，散布足以損害對方營業信譽之不實情事，均違反《[公平交易法](../Page/公平交易法.md "wikilink")》第22條規定」，命令兩家公司停止違法行為，兩家公司分別遭[罰鍰](../Page/罰鍰.md "wikilink")[新臺幣](../Page/新臺幣.md "wikilink")20萬元。\[5\]

### 收費方式

在各伺服器網站皆可看到終身免費口號，其收費來源是取自各伺服器網站開設販賣部。販賣部為販售虛擬道具與促銷折扣相關活動的線上店面，玩家透過遊戲包或點數卡向官方網站辦理儲值動作，存入點數後，玩家可進行購買虛擬道具，該款遊戲是屬於道具零售制。另外依遊戲公司推出禮盒，可一次購買齊全虛擬道具。

## 遊戲特色

遊戲分成四座學院：玄嚴、鳳凰、聖門、虎令。虎令學院是座荒廢學院，遊戲設計的環境是玩家的練功地圖，其於三座學院是玩家們自己的歸宿。

### 服裝

該四座學院各有自己的一套[校服](../Page/校服.md "wikilink")，是呈現代表各學院的風格。

1.  玄嚴學院：
2.  聖門學院：
3.  鳳凰學院：

### 懲罰系統

玩家若於非PK場所或非PK時間下殺害其他玩家，玩家ID便會由白漸紅，一旦變紅色ID，成為玩家公認俗稱「紅人」，所有玩家有權可以殺死紅人。當玩家變成紅人，噴裝率增高，稅率會調高，需要至所屬學院宿舍進行打掃工作，直到ID恢復原樣。則在這個期間之內不得使用「好友傳送卡」、「學院歸還卡」之類的道具。

### PK系統

《亂
Online》PK系統曾在2005年11月4日－同年11月14日期間開放校內PK活動，但維持原PK制度，並規定玩家等級20（含）以下無法參加，並且ID不會變紅色\[6\]。
2006年4月4日進行NonPK分流開放，2006年9月8日－同年9月30日開放投票，由玩家投票是否開放PK時段\[7\]，同年10月2日公布結果是前五服開放，後三服維持現狀，在這NonPK到現在的期間，則造成人氣大量下滑。

  - 自由PK：PK有一定的時段，大約每隔*50分～70分*，畫面便會出現提醒玩家：*現在是PK時段*。一進入該時段內，全數玩家ID皆為紅色（包含校內），大家可以自由進行PK，但僅限於校外。
  - 公會戰：亦稱組織戰，由官方設定的固定時間，開放給玩家進行公會戰，參戰地點在商洞、聖門、鳳凰、玄嚴各電算室內，有一小時時間限制，時間結束且無其它公會取得電算室，取得者享有所取得電算室擁有權與道具獎勵，可任意調整稅率（最多可調至50%），例：某公會在玄嚴電算室取得該電算室，表示該公會可得到遊戲公司贈予道具獎勵外，還有調整玄嚴洞稅率權利。加入公會戰並不限定學院，其條件如下：

## 參考來源

## 外部連結

  - [《亂 Online》韓國官方網站](http://ran.hicocoon.com/)
  - [《亂2 Online》台灣官方網站](http://www.ran.com.tw/gamesite/index.aspx)
  - [《勇 Online》台灣官方網站(原開發商在台經營之分公司所代理)](http://www.yong-online.com.tw/)
  - [《亂
    Online》日本官方網站](https://web.archive.org/web/20090922023026/http://ranonline.jp/index.action)
  - [《亂 Online》馬來西亞官方網站](http://ran.myrosso.com/)
  - [《亂
    Online》香港官方網站](https://web.archive.org/web/20080220071947/http://ran.runup.com.hk/index2.php)
  - [《亂 Online》國际服官方網站](http://www.ran-world.com)

[Category:大型多人在线角色扮演游戏](../Category/大型多人在线角色扮演游戏.md "wikilink")
[Category:2005年电子游戏](../Category/2005年电子游戏.md "wikilink")
[Category:學校背景遊戲](../Category/學校背景遊戲.md "wikilink")
[Category:Windows游戏](../Category/Windows游戏.md "wikilink")
[Category:韩国开发电子游戏](../Category/韩国开发电子游戏.md "wikilink")

1.
2.
3.
4.  RU 報導，[〈《亂 Online》傳出代理爭議
    韓原廠揚言將與依斯楚解約〉](http://gnn.gamer.com.tw/5/26775.html)，[巴哈姆特電玩資訊站](../Page/巴哈姆特電玩資訊站.md "wikilink")，2007年5月25日
5.  行政院公平交易委員會
    新聞稿，[〈依斯楚公司及銘信公司散布足以損害對方營業信譽之不實情事各罰20萬元〉](http://www.ftc.gov.tw/internet/main/doc/docDetail.aspx?uid=126&docid=10477)，2009年4月8日
6.  [亂Online校園PvP測試活動開跑](http://news.gamebase.com.tw/news/signlenews.jsp?news_no=21125)
7.  [亂Online學員自治投票，9月8日正式開跑](http://news.gamebase.com.tw/news/signlenews.jsp?news_no=24242)