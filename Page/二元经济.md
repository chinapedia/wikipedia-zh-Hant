**二元经济模型**也称作**两部门模型**（*Dual Sector
model*）是1979年[诺贝尔经济学奖获得者美国](../Page/诺贝尔经济学奖.md "wikilink")[经济学家](../Page/经济学家.md "wikilink")[威廉·阿瑟·刘易斯在](../Page/威廉·阿瑟·刘易斯.md "wikilink")1954年发表的《劳动无限供给下的经济发展》中提出的[发展经济学模型](../Page/发展经济学.md "wikilink")。

## 模型内容

“二元经济”指[发展中国家的](../Page/发展中国家.md "wikilink")[经济是由两个不同的](../Page/经济.md "wikilink")[经济部门组成](../Page/经济部门.md "wikilink")。一是**維持生計部门**，二是**现代部门**。

### 維持生計部门

自给自足的[农业及简单的](../Page/农业.md "wikilink")、零星的[商业](../Page/商业.md "wikilink")、[服务业](../Page/服务业.md "wikilink")，[劳动生产率很低](../Page/劳动生产率.md "wikilink")，[边际劳动生产率接近零甚至小于零](../Page/边际劳动生产率.md "wikilink")，[非熟练劳动的工资极低](../Page/非熟练劳动.md "wikilink")，在该部门存在大量的[隐蔽性失业](../Page/隐蔽性失业.md "wikilink")，但容纳着[发展中国家的绝大部分](../Page/发展中国家.md "wikilink")[劳动力](../Page/劳动力.md "wikilink")。

  - 生产方式：“维持生计”的产业

### 现代部门

技术较先进的[工矿业](../Page/工矿业.md "wikilink")、[建筑业](../Page/建筑业.md "wikilink")、近代[商业](../Page/商业.md "wikilink")、[服务业](../Page/服务业.md "wikilink")、容纳的就业[劳动力较少](../Page/劳动力.md "wikilink")，[劳动生产率较高](../Page/劳动生产率.md "wikilink")，工资水平较高，在传统部门的工资之上。

  - 生产方式：使用[再生产性](../Page/再生产.md "wikilink")[资本谋取](../Page/资本.md "wikilink")[利润](../Page/利润.md "wikilink")，具有典型的[资本主义特征](../Page/资本主义.md "wikilink")

### [劳动力供给的无限性](../Page/劳动力.md "wikilink")

存在着大量的[剩余劳动的传统部门的人均收入水平决定了现代部门工资的下限](../Page/剩余劳动.md "wikilink")，现代部门从传统部门大量吸收[劳动力](../Page/劳动力.md "wikilink")，而其工资水平基本保持不变。这是该模型的**理论核心**。

现代部门的[利润来自劳动产出大于工资总量的部分](../Page/利润.md "wikilink")，并不断把[利润转化为](../Page/利润.md "wikilink")[资本扩大](../Page/资本.md "wikilink")[再生产](../Page/再生产.md "wikilink")，直至传统部门的[剩余劳动被全部吸收](../Page/剩余劳动.md "wikilink")。于是，现代部门大大扩张，传统部门只有在[剩余劳动被吸收完毕后](../Page/剩余劳动.md "wikilink")[劳动生产率才能提高](../Page/劳动生产率.md "wikilink")，传统部门的就业者的收入才能改善。

## 模型结论

  - [经济发展的实质](../Page/经济发展.md "wikilink")，就是现代部门的不断扩张和传统部门的不断萎缩。
  - [工业化过程中](../Page/工业化.md "wikilink")，传统部门为现代部门输送[剩余劳动](../Page/剩余劳动.md "wikilink")，以廉价[劳动力为现代部门创造](../Page/劳动力.md "wikilink")[利润](../Page/利润.md "wikilink")，累积扩大[再生产的资本](../Page/再生产.md "wikilink")。
  - [剩余劳动未输送完毕的时候](../Page/剩余劳动.md "wikilink")，传统部门的[劳动生产率处于停滞状态](../Page/劳动生产率.md "wikilink")。
  - 传统部门是次要的、从属的、消极被动的；现代部门是积极能动的。

## 爭議及批評

  - 刘易斯並沒有考慮到農工業平均成長的重要性，以及再生資本型利潤的投資會對農工業造成的影響；若資本家不將利潤投資在傳統部門，則現代部門的發展會受到危害。
  - 若以傳統部門平均工資設定為現代部門下限才能吸收傳統部門的勞動力，即代表所吸收的勞動力不屬於傳統部門剩餘勞動力。因此，在現代部門吸收勞動力的過程中，傳統部門勞動生產率即會提高。
  - 需考慮現代部門的[職業訓練](../Page/職業訓練.md "wikilink")。

## 参考文献

  - 刘易斯《二元经济论》，北京经济学院出版社，1988年

## 参见

  - [威廉·阿瑟·刘易斯](../Page/威廉·阿瑟·刘易斯.md "wikilink")（William Arthur Lewis）
  - [现代化](../Page/现代化.md "wikilink")
  - [中华人民共和国户籍制度](../Page/中华人民共和国户籍制度.md "wikilink")、[三农问题](../Page/三农问题.md "wikilink")
  - [人口过剩](../Page/人口过剩.md "wikilink")
  - [隐性失业](../Page/隐性失业.md "wikilink")
  - [结构性失业](../Page/结构性失业.md "wikilink")

[Category:发展经济学](../Category/发展经济学.md "wikilink")