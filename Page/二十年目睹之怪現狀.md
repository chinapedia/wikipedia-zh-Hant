《**二十年目睹之怪現狀**》，又名《**晚清二十年目睹之怪現狀**》，是由[筆名](../Page/筆名.md "wikilink")“我佛山人”的[晚清](../Page/清朝.md "wikilink")[吳趼人](../Page/吳趼人.md "wikilink")（吳沃堯）所作的長篇[章回小說](../Page/章回小說.md "wikilink")。

全書由“死裡逃生”在市集上碰到一位大漢賣“九死一生”的筆記展開序幕。“九死一生”是這部小說的[第一人稱主人翁](../Page/第一人稱.md "wikilink")，由於其認為在亂世中能夠僥倖活著實在是九死一生，因而以此為號焉。一開始時主人翁為父奔喪，但家族的長輩與父親的友人覬覦他家的財產。所幸他受到一位旧时学兄吴继之的幫助，之後便在吳繼之手下經商，遊遍中國各地，藉此描寫當時亂世的總總現象，最後終於經商失敗。書中着重描绘人性的醜惡，尤其[洋場與](../Page/洋場.md "wikilink")[官場中的人](../Page/官場.md "wikilink")、事。

[魯迅在](../Page/魯迅.md "wikilink")《[中國小說史略](../Page/中國小說史略.md "wikilink")》中稱，劉鶚的《老殘遊記》、[李寶嘉的](../Page/李寶嘉.md "wikilink")《[官場現形記](../Page/官場現形記.md "wikilink")》、[吳趼人的](../Page/吳趼人.md "wikilink")《[二十年目睹之怪現狀](../Page/二十年目睹之怪現狀.md "wikilink")》和[曾樸的](../Page/曾樸.md "wikilink")《[孽海花](../Page/孽海花.md "wikilink")》為「[晚清四大譴責小說](../Page/晚清四大譴責小說.md "wikilink")」。

| [晚清](../Page/晚清.md "wikilink")[四大谴责小说](../Page/晚清四大谴责小说.md "wikilink") |
| ---------------------------------------------------------------------- |
| 作者                                                                     |
| [刘鹗](../Page/刘鹗.md "wikilink")                                         |
| [李寶嘉](../Page/李寶嘉.md "wikilink")                                       |
| [吳趼人](../Page/吳趼人.md "wikilink")                                       |
| [曾樸](../Page/曾樸.md "wikilink")                                         |

## 主要人物

  - 我，不愿通过科举做官的人
  - 吴继之，中进士后为官，后来还兼营商业买卖，“我”的同学兼挚友，我后来成为吴的幕友和商业助手、合伙人。
  - 文述农，吴继之的助手
  - 管德泉，吴继之的助手
  - 金子安，吴继之的助手
  - 伯父，我的伯父，重利的官员
  - 符弥轩
  - 苟观察

## 参考文献

  - [鲁迅](../Page/鲁迅.md "wikilink")《[中國小說史略](../Page/中國小說史略.md "wikilink")》第二十八篇
    《清末之譴責小說》

## 外部链接

  - [在线全文阅读](http://liulangcat.com/100/1006.html)

{{-}}

[Category:清朝小說](../Category/清朝小說.md "wikilink")
[Category:清朝晚期背景小說](../Category/清朝晚期背景小說.md "wikilink")
[Category:政治小說](../Category/政治小說.md "wikilink")
[Category:谴责小说](../Category/谴责小说.md "wikilink")
[Category:1900年代小说](../Category/1900年代小说.md "wikilink")
[Category:南京市背景小說](../Category/南京市背景小說.md "wikilink")
[Category:上海背景小说](../Category/上海背景小说.md "wikilink")
[Category:20世纪中国小说](../Category/20世纪中国小说.md "wikilink")