**二叠纪**（Permian）是约2.9～2.5亿年前[古生代的最后一个](../Page/古生代.md "wikilink")[地质时代](../Page/地质时代.md "wikilink")，在[石炭紀](../Page/石炭紀.md "wikilink")、[三叠紀之間](../Page/三叠紀.md "wikilink")。定义二叠纪的岩石层是比较分明的，但开始、结束的精确年代却有争议。其不精确度可达数百万年。以往，二叠纪为二分，目前二叠纪使用三分法：Cisuralian、Guadalupian、[樂平世](../Page/樂平世.md "wikilink")（Lopingian）。Permian源自[俄罗斯的](../Page/俄罗斯.md "wikilink")[彼尔姆州](../Page/彼尔姆州.md "wikilink")。中文译为二叠纪一說是在德國的同年代地層的上層是[镁质](../Page/镁.md "wikilink")[灰岩](../Page/灰岩.md "wikilink")，下層是紅色[砂岩之故](../Page/砂岩.md "wikilink")\[1\]。

## 生物

  - 早期的植物以[真蕨](../Page/真蕨.md "wikilink")、[种子蕨为主](../Page/种子蕨.md "wikilink")。晚期有较大变化，[鳞木类](../Page/鱗木科.md "wikilink")、[芦木类](../Page/芦木.md "wikilink")、[种子蕨](../Page/种子蕨.md "wikilink")、[柯达树等趋于衰微](../Page/柯达树.md "wikilink")、灭绝，代之以较耐旱的[裸子植物](../Page/裸子植物.md "wikilink")，[松柏类大为增加](../Page/松柏門.md "wikilink")，[苏铁类开始发展](../Page/苏铁.md "wikilink")。
  - [腕足类继续繁盛](../Page/腕足类.md "wikilink")，[长身贝类占优势](../Page/長身貝目.md "wikilink")；[软体动物也是重要部分](../Page/软体动物.md "wikilink")，[菊石类有明显分异](../Page/菊石.md "wikilink")；[苔藓虫逐渐衰退](../Page/苔藓虫.md "wikilink")；[三叶虫趋于灭绝](../Page/三叶虫.md "wikilink")；[昆虫开始迅速发展](../Page/昆虫.md "wikilink")。庞大的[离片锥目](../Page/离片锥目.md "wikilink")[两栖动物进一步发展](../Page/两栖动物.md "wikilink")，仍然占据统治地位。[爬行动物首次大量繁盛](../Page/爬行动物.md "wikilink")，[杯龙目](../Page/杯龙目.md "wikilink")、[始鳄目继续存在](../Page/始鳄目.md "wikilink")，[盘龙目](../Page/盘龙目.md "wikilink")、[兽孔目等](../Page/兽孔目.md "wikilink")[合弓纲动物具有优势](../Page/合弓纲.md "wikilink")。
  - 二叠纪末发生了[二叠纪-三叠纪灭绝事件](../Page/二叠纪-三叠纪灭绝事件.md "wikilink")，90%至95%的海洋生物灭绝，其詳細原因目前尚不明確。

## 地理

  - 二叠纪早期寒冷、[冰川广布](../Page/冰川.md "wikilink")；晚期海退、干旱。[南美](../Page/南美.md "wikilink")、[非洲南部](../Page/非洲.md "wikilink")、[印度半岛](../Page/印度半岛.md "wikilink")、[巴基斯坦盐岭](../Page/巴基斯坦.md "wikilink")、[澳大利亚](../Page/澳大利亚.md "wikilink")、[南极洲](../Page/南极洲.md "wikilink")、[中国](../Page/中国.md "wikilink")[西藏南部等地有](../Page/西藏.md "wikilink")[冰碛岩](../Page/冰碛.md "wikilink")、冰水沉积。
  - 早期[火山活动广泛](../Page/火山.md "wikilink")，晚期趋于沉寂。北美的[阿巴拉契亚运动](../Page/阿巴拉契亚.md "wikilink")，是二叠纪末强烈的构造运动。西部的科迪勒拉碰撞带在连续地壳运动中，伴有强烈的火山活动。[乌拉尔残余地槽在晚二叠世褶皱隆起](../Page/乌拉尔.md "wikilink")，欧亚陆域融为一体。[中亚和中国北部](../Page/中亚.md "wikilink")、西南部的板块碰撞带于二叠纪经历了一段褶皱、变质、火山活动，包括[花岗岩侵入及中](../Page/花岗岩.md "wikilink")、酸性[熔岩](../Page/熔岩.md "wikilink")、[凝灰岩的喷出](../Page/凝灰岩.md "wikilink")。
  - [特提斯海](../Page/特提斯海.md "wikilink")，存在于北纬30°～40°，自[地中海西部达](../Page/地中海.md "wikilink")[印度尼西亚](../Page/印度尼西亚.md "wikilink")；南支沿[澳大利亚西海岸到南纬](../Page/澳大利亚.md "wikilink")30°；东北支与覆盖中国的陆表海相连，与日本离散小板块相通，向北与乌拉尔残余海相通。二叠纪末大面积的海退，使世界大部分海域退缩殆尽；但中国华南、巴基斯坦、伊朗一带二叠、三叠纪间保持海域环境。二叠紀未期時，因大量地面以快速下陷至地球深處，令大量火山活動，所以大部份物種也在這時[滅絕](../Page/滅絕.md "wikilink")

## 參考資料

[纪](../Category/地质年代.md "wikilink")

1.  [台灣海洋生態資訊學習網
    二疊紀](http://study.nmmba.gov.tw/Modules/Knowledge/KnowledgeShow.aspx?ItemID=212&TabID=37)