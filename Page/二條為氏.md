**二條為氏**（，即生於[貞應元年](../Page/貞應.md "wikilink")，卒於[弘安](../Page/弘安.md "wikilink")9年9月14日），又稱**藤原為氏**，[日本](../Page/日本.md "wikilink")[鎌倉時代中期的](../Page/鎌倉時代.md "wikilink")[公家人物](../Page/公家.md "wikilink")、[和歌](../Page/和歌.md "wikilink")[歌人](../Page/歌人.md "wikilink")，歌道流派[二條派始祖](../Page/二條派.md "wikilink")。他是[權大納言](../Page/大納言.md "wikilink")[藤原為家的長男](../Page/藤原為家.md "wikilink")，[御子左家嫡系](../Page/御子左家.md "wikilink")。母親是[宇都宮賴綱之女](../Page/宇都宮賴綱.md "wikilink")，同母弟[京極為教](../Page/京極為教.md "wikilink")，異母弟[冷泉為相](../Page/冷泉為相.md "wikilink")、[冷泉為守](../Page/冷泉為守.md "wikilink")，有子[二條為世](../Page/二條為世.md "wikilink")。

自[嘉祿二年](../Page/嘉祿.md "wikilink")（1226年）正月（日本舊曆）敘爵，歷任左中將、[參議](../Page/參議.md "wikilink")、右衛門督、[權中納言等職](../Page/中納言.md "wikilink")，[文永四年](../Page/文永.md "wikilink")（1267年）二月官至[正二位權大納言](../Page/正二位.md "wikilink")。

作為[大覺寺統的近侍官員兼為出色的和歌歌人](../Page/大覺寺統.md "wikilink")，二條為氏於當時和歌歌壇具極大影響力。《[大日本史](../Page/大日本史.md "wikilink")》評價他「才思敏捷，能賦險題」。[弘安元年](../Page/弘安.md "wikilink")（1278年）奉[龜山上皇之命編撰](../Page/龜山天皇.md "wikilink")《[續拾遺和歌集](../Page/續拾遺和歌集.md "wikilink")》。此外，他也編撰了以宇都宮歌壇為中心的歌集《[新和歌集](../Page/新和歌集.md "wikilink")》。其本人作品收錄於其子孫編撰的家集《大納言為氏卿集》，而被錄入各[敕撰和歌集的作品計有](../Page/敕撰和歌集.md "wikilink")232首。

不過，他與兩位弟弟京極為教、冷泉為相關係惡劣：除了在和歌創作上分庭抗禮外，在[建治元年](../Page/建治.md "wikilink")（1275年），其父藤原為家逝世後，二條為氏作為長男繼承御子左家，與庶母[阿佛尼](../Page/阿佛尼.md "wikilink")（冷泉為相生母）因[播磨國細川莊領地相爭](../Page/播磨國.md "wikilink")，阿佛尼上告至[鎌倉幕府](../Page/鎌倉幕府.md "wikilink")。訴訟糾纏兩代，至阿佛尼和二條為氏死後，為氏之子二條為世當家時仍未平息。至[正和二年](../Page/正和_\(日本\).md "wikilink")（1313年），經鎌倉幕府將軍[守邦親王命](../Page/守邦親王.md "wikilink")[北條高時等裁決](../Page/北條高時.md "wikilink")，終判領地歸冷泉為相所有。此事使御子左家分裂為二條、京極、冷泉三家。

弘安八年（1285年），二條為氏落髮出家，法名**覺阿**，翌年（1286年）65歲時去世。

## 參考資料

<div class="references-small">

1.  [藤原為氏
    千人万首](http://www.asahi-net.or.jp/~sg2h-ymst/yamatouta/sennin/tameuji.html)，2007年10月24日驗證。
2.  [大日本史　卷之二百廿一　列傳第一百卌八　歌人四](http://miko.org/~uraki/kuon/furu/text/dainihonsi/dns221.htm)，2007年10月24日驗證。

</div>

[N](../Category/鎌倉時代歌人.md "wikilink")
[N](../Category/鎌倉時代公家.md "wikilink")
[Category:御子左流](../Category/御子左流.md "wikilink")