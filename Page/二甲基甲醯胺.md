**二甲基甲醯胺**（，[縮寫](../Page/縮寫.md "wikilink")**DMF**）是一種透明液體，能和[水及大部分有機溶劑互溶](../Page/水.md "wikilink")。它是[化學反應的常用](../Page/化學反應.md "wikilink")[溶劑](../Page/溶劑.md "wikilink")。純二甲基甲醯胺是沒有氣味的，但工業級或變質的二甲基甲醯胺則有魚腥味，因其含有[二甲基胺的不純物](../Page/二甲基胺.md "wikilink")。名稱來源是由於它是[甲醯胺](../Page/甲醯胺.md "wikilink")（[甲酸的](../Page/甲酸.md "wikilink")[醯胺](../Page/醯胺.md "wikilink")）的二[甲基取代物](../Page/甲基.md "wikilink")，而二個甲基都位於***N***（[氮](../Page/氮.md "wikilink")）[原子上](../Page/原子.md "wikilink")。

二甲基甲醯胺是高沸點的[極性](../Page/極性.md "wikilink")([親水性](../Page/親水性.md "wikilink"))非質子性溶劑，能促進[S<sub>N</sub>2反應機構的進行](../Page/SN2.md "wikilink")。
二甲基甲醯胺是利用[甲酸和](../Page/甲酸.md "wikilink")[二甲基胺製造的](../Page/二甲基胺.md "wikilink")。二甲基甲醯胺在強鹼如[氫氧化鈉或強酸如](../Page/氫氧化鈉.md "wikilink")[鹽酸或](../Page/鹽酸.md "wikilink")[硫酸的存在下是不穩定的](../Page/硫酸.md "wikilink")（尤其在高溫下），並[水解為甲酸與二甲基胺](../Page/水解.md "wikilink")。

## 用途

N,N-二甲基甲醯胺主要用作一低揮發性的溶劑。N,N-二甲基甲醯胺被用於[聚丙烯腈纖維及](../Page/聚丙烯腈纖維.md "wikilink")[塑料的製造上](../Page/塑料.md "wikilink")。在[製藥工業中](../Page/製藥工業.md "wikilink")，它也用於製造[殺蟲劑](../Page/殺蟲劑.md "wikilink")、[接著劑](../Page/接著劑.md "wikilink")、人造[皮革](../Page/皮革.md "wikilink")、纖維、軟片及表面塗裝等。

N,N-二甲基甲醯胺是[Bouveault醛合成反應及](../Page/Bouveault醛合成反應.md "wikilink")[Vilsmeier-Haack反應](../Page/Vilsmeier-Haack反應.md "wikilink")（另一有用的[醛類合成反應](../Page/醛.md "wikilink")）的試劑。

在[核磁共振光譜中](../Page/核磁共振.md "wikilink")，N,N-二甲基甲醯胺的甲基上的質子形成二個單峰，因為在核磁共振的時間尺度中，[羰基碳](../Page/羰基.md "wikilink")-氮鍵的旋轉速率很慢。[羰基碳](../Page/羰基.md "wikilink")-氮鍵的鍵級大於一，而醯胺碳-[氧鍵的鍵級則小於二](../Page/氧.md "wikilink")。[醯胺的紅外線光譜中](../Page/醯胺.md "wikilink")，C=O帶通常在小於1700cm<sup>−1</sup>處，因其C=O鍵因為從氮供給氧的[電子密度而減弱](../Page/電子.md "wikilink")。

N,N-二甲基甲醯胺會滲透大部分的[塑料並使其](../Page/塑料.md "wikilink")[膨脹](../Page/膨脹.md "wikilink")，故常用作[油漆清除劑的成分之一](../Page/油漆清除劑.md "wikilink")。

## 製造

N,N-二甲基甲醯胺可利用[二甲胺和](../Page/二甲胺.md "wikilink")[一氧化碳在催化下以](../Page/一氧化碳.md "wikilink")製造。

## 安全

在N,N-二甲基甲醯胺中使用[氫化鈉進行反應具有一定的危險性](../Page/氫化鈉.md "wikilink")。有報導指出在26°C的低溫下，仍發生放熱的分解反應。在實驗室規模中，任何熱量的釋放（通常）很快會被發現並使用冰浴加以控制，因此N,N-二甲基甲醯胺與氫化鈉的組合仍十分常用。然而，在[試驗工廠規模下曾有許多事故發生](../Page/試驗工廠.md "wikilink")。

## 毒性

N,N-二甲基甲醯胺與人類的[癌症發生有關](../Page/癌症.md "wikilink")，並能導致[新生兒缺陷](../Page/新生兒缺陷.md "wikilink")。在一些工業部門中，婦女是禁止使用N,N-二甲基甲醯胺的。N,N-二甲基甲醯胺黏稠而難以蒸發，是十分令人煩惱的溶劑，在許多反應中，可用[二甲基亞碸來代替](../Page/二甲基亞碸.md "wikilink")。許多製造商在物質安全資料表中，將N,N-二甲基甲醯胺的健康危害列為(終身)或(慢性)，因為N,N-二甲基甲醯胺不太容易被人體排出。

雖然[美國國家環境保護局](../Page/美國國家環境保護局.md "wikilink")（EPA）並不認為N,N-二甲基甲醯胺有致癌危險，根據[國際癌症研究機構](../Page/國際癌症研究機構.md "wikilink")（IARC），N,N-二甲基甲醯胺可能是一[致癌物](../Page/致癌物.md "wikilink")。
N,N-二甲基甲醯胺（DMF）經各種途徑吸收後，主要由肝内代謝，排泄較快，主要目標器官為肝臟
，腎臟也有一定損害。主要經肝内微粒體混合功能氧化酶進行脱甲基化作用，脱去一個甲基，代謝產物為一甲基甲醯胺和甲醯胺，代謝迅速；甲醯胺在血中滞留稍長，進而代謝為甲酸和氨排出。部分二甲基甲醯胺以原形物從尿和呼氣排出。

## 防护措施

呼吸制度防护: 氧气中纯度超标时，佩戴自吸净化式防毒面具。佩戴氧气呼吸器。眼睛防护: 戴化工安全防护眼镜。身上防护: 穿防静电工作。手防护:
戴橡胶手套。其他: 工作现场严禁抽烟、进食和饮水。工作完毕，淋浴更衣。

## 參考資料

1.  Redlich, C; Beckett, W. S.; Sparer, J.; Barwick, K. W.; Riely, C.
    A.; Miller, H.; Sigal, S. L.; Shalat, S. L.; and Cullen, M. R.;
    1988. Liver disease associated with occupational exposure to the
    solvent dimethylformamide. *Ann. Intern. Med.* 108:680-686.

## 外部連結

  - 二甲基甲醯胺在
    [有機合成](https://web.archive.org/web/20060528075613/http://www.orgsyn.org/orgsyn/chemname.asp?nameID=36366)
    上的應用
  - [簡明國際化學品評估文件31:
    N,N-二甲基甲醯胺](http://www.inchem.org/documents/cicads/cicads/cicad31.htm)
  - 二甲基甲醯胺的
    [物質安全資料表](http://ptcl.chem.ox.ac.uk/MSDS/DI/N,N-dimethylformamide.html)
  - [二甲基甲醯胺慢性毒性摘要](http://www.oehha.org/air/chronic_rels/pdf/68122.pdf)
    ([PDF](../Page/PDF.md "wikilink"))
  - [Chemical Book
    N,N-二甲基甲酰胺](http://www.chemicalbook.com/ProductChemicalPropertiesCB2854115.htm)

[Category:醯胺](../Category/醯胺.md "wikilink")
[Category:酰胺溶剂](../Category/酰胺溶剂.md "wikilink")