**二硫代氨基甲酸銨**是一種黃色的晶體，[分子式為NH](../Page/分子式.md "wikilink")<sub>4</sub>S(C=S)NH<sub>2</sub>
，可溶於[水](../Page/水.md "wikilink")，二硫代氨基甲酸銨主要用於[有機合成中的](../Page/有機合成.md "wikilink")[環合反應](../Page/環合反應.md "wikilink")，在化學分析上用於代替[硫化氫和](../Page/硫化氫.md "wikilink")[硫化銨](../Page/硫化銨.md "wikilink")。

## 外部連結

  - [化工词典](http://www.chemyq.com/xz/xz1/1858dsdio.htm)

[Category:有机酸铵盐](../Category/有机酸铵盐.md "wikilink")
[Category:二硫代氨基甲酸盐](../Category/二硫代氨基甲酸盐.md "wikilink")