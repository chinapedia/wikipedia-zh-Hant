**于会泳**，生于[山东](../Page/山东省.md "wikilink")[乳山](../Page/乳山市.md "wikilink")，[中国音乐家](../Page/中华人民共和国.md "wikilink")，[文化大革命时期政治人物](../Page/文化大革命.md "wikilink")。[中共十大中央委员](../Page/中国共产党第十届中央委员会委员列表.md "wikilink")，[文化部部长](../Page/中华人民共和国文化部.md "wikilink")。1976年10月[四人帮倒台](../Page/四人帮.md "wikilink")，于会泳被免去一切职务、审查[逮捕](../Page/逮捕.md "wikilink")，1977年[自杀身亡](../Page/自杀.md "wikilink")。[中共中央认定他为](../Page/中共中央.md "wikilink")[江青反革命集团的重要骨干](../Page/江青反革命集团.md "wikilink")、[死党](../Page/死党.md "wikilink")，撤销党内外一切职务，开除党籍\[1\]。

## 生平

他出生于[山东省](../Page/山东省.md "wikilink")[乳山市](../Page/乳山市.md "wikilink")[海阳所镇西泓](../Page/海阳所镇.md "wikilink")-{于}-家村\[2\]。从小就通过盲人演唱接触了[福山大鼓](../Page/福山大鼓.md "wikilink")、[蓬莱大鼓](../Page/蓬莱大鼓.md "wikilink")、[荣城大鼓](../Page/荣城大鼓.md "wikilink")、[海阳大鼓](../Page/海阳大鼓.md "wikilink")，熟悉掺合着[皮黄](../Page/皮黄.md "wikilink")、[奉调](../Page/奉调.md "wikilink")、[犁铧调](../Page/犁铧调.md "wikilink")、[西河调](../Page/西河调.md "wikilink")、[高拨子等的民间](../Page/高拨子.md "wikilink")[曲艺](../Page/曲艺.md "wikilink")[小调](../Page/小调.md "wikilink")、[民歌小调](../Page/民歌.md "wikilink")，通过自学掌握了[二胡](../Page/二胡.md "wikilink")、[三弦](../Page/三弦.md "wikilink")、[笛子的演奏技巧](../Page/笛子.md "wikilink")。1942年任教于[解放区海阳所小学](../Page/解放区.md "wikilink")。

1946年参加胶东文工团，先后在美术队、音乐队工作。由于刻苦学习音乐业务知识，工作积极。1949年9月，抽调参加[国立音乐院上海分院](../Page/国立音乐院.md "wikilink")（今[上海音乐学院](../Page/上海音乐学院.md "wikilink")）“音乐教育专修班”。被院长[贺绿汀看中](../Page/贺绿汀.md "wikilink")，边学习边代民歌课。其间加入[中共](../Page/中共.md "wikilink")。1950年毕业时因学习成绩总分第一留在院属音工团，边参加创作演出，边旁听作曲系本科的和声班的课。

同年调入本院民族音乐研究所，主攻曲艺说唱。出版有收集、整理的《[山东大鼓](../Page/山东大鼓.md "wikilink")》和参与整理的《陕西榆林小曲》（音乐出版社，1957）、专著《单弦牌子曲分析》（1958）。先后创作有民族管弦乐合奏曲《[闯将令](../Page/闯将令.md "wikilink")》、歌曲《渔歌》、《杨柳叶子青》、《伟大的毛主席》、《北京的朝霞》、《幸福花开遍地香》、《不唱山歌心不爽》等。担任戏曲音乐讲习班教学。1959年发表《关于我国民间音乐调式的命名》（《音乐研究》）。1960年开设“民间曲调研究”专题系列讲座，次年开始“[腔词关系研究](../Page/腔词关系研究.md "wikilink")”（1963年完成；全文12万字、附有230个多曲谱的油印本），被称为“在分类研究基础上的综合研究……为形态学的非程式性研究起了先导作用……影响是深远的”。成为[上海音乐学院本科和专修科的指定必修教材](../Page/上海音乐学院.md "wikilink")。1962年晋升讲师，1963年任民族音乐理论系副主任。1964年发表《关于京剧现代戏音乐的若干问题》（《上海戏剧》）。1965年奉调上海“戏改创作小组”参加《[海港](../Page/海港.md "wikilink")》的音乐创作，和《[智取威虎山](../Page/智取威虎山.md "wikilink")》音乐的后期润饰工作，发表《戏曲音乐必须为塑造英雄形象服务》、《评郭建光的唱腔音乐设计》（《[文汇报](../Page/文汇报.md "wikilink")》）。

1966年6月被上海音乐学院定为“[三反分子](../Page/三反分子.md "wikilink")”，被揪回学院“小牛鬼组”看押监督，写检查交待，看大字报接受批判，参加劳动，受到[抄家的冲击](../Page/抄家.md "wikilink")。10月接[中央文革指示](../Page/中央文革.md "wikilink")，参加[样板戏进京公演](../Page/样板戏.md "wikilink")，得到[张春桥回校造反的密旨](../Page/张春桥.md "wikilink")。12月写出《十四点质问》等[大字报](../Page/大字报.md "wikilink")，参加“教工造反团”、“钟望阳专案组”。1967年担任[上海京剧院领导](../Page/上海京剧院.md "wikilink")、[上海音乐学院革委会副主任](../Page/上海音乐学院.md "wikilink")、上海市文化系统革委会筹备委员会主任。1968年主持、参与批斗[贺绿汀的大会](../Page/贺绿汀.md "wikilink")，在《[让文艺界永远成为宣传毛泽东思想的阵地](../Page/让文艺界永远成为宣传毛泽东思想的阵地.md "wikilink")》（5月23日《文汇报》）一文中首次提出“[三突出](../Page/三突出.md "wikilink")”。1969年作为代表参加[中共九大](../Page/中共九大.md "wikilink")，被增补为上海市革委常委。

1970年担任[国务院文化组组员](../Page/中華人民共和國國務院.md "wikilink")，全面主持[样板戏工作](../Page/样板戏.md "wikilink")，试验运用中西混合乐队为样板戏伴奏，负责把《[智取威虎山](../Page/智取威虎山.md "wikilink")》、《[红灯记](../Page/红灯记.md "wikilink")》、《[红色娘子军](../Page/红色娘子军.md "wikilink")》、《[白毛女](../Page/白毛女.md "wikilink")》等样板戏剧目拍摄成电影，举办京剧训练班，为《[杜鹃山](../Page/杜鹃山.md "wikilink")》作曲，督阵、指导《[平原作战](../Page/平原作战.md "wikilink")》《[龙江颂](../Page/龙江颂.md "wikilink")》的加工修改。尤其是《[杜鹃山](../Page/杜鹃山.md "wikilink")》的念白用词曲长短句式的韵白体制，柯湘唱腔采用歌剧主导动机的系统结构进行设计，取得很好效果。1973年任国务院文化组副组长，1975年任国务院文化部部长。以[江青](../Page/江青.md "wikilink")、[张春桥的亲疏任用人事](../Page/张春桥.md "wikilink")，全面贯彻他们的政治、文化主张。在其指使下，负责“初澜”、“江天”写作班子，积极组织了对[晋剧](../Page/晋剧.md "wikilink")《[三上桃峰](../Page/三上桃峰.md "wikilink")》、[北京饭店国画](../Page/北京饭店.md "wikilink")、无标题音乐、1975年[邓小平整顿文艺的批判](../Page/邓小平.md "wikilink")，扼杀电影《[海霞](../Page/海霞_\(电影\).md "wikilink")》、《[创业](../Page/创业_\(电影\).md "wikilink")》。布置样板团把《[春苗](../Page/春苗.md "wikilink")》、《战船台》、《[决裂](../Page/决裂.md "wikilink")》等改编成[京剧](../Page/京剧.md "wikilink")，限期赶写、摄制“与走资派作斗争的戏”（电影《[盛大的节日](../Page/盛大的节日.md "wikilink")》、《[反击](../Page/反击.md "wikilink")》）。1976年10月被列入四人帮组阁名单，拟任职务[中共中央政治局委员](../Page/中共中央政治局.md "wikilink")、[国务院副总理](../Page/中华人民共和国国务院副总理.md "wikilink")。

1976年10月被免去一切职务，并被审查逮捕。被隔离审查半年多时间里写出近17万字交代材料。1977年8月28日，于会泳服[来苏水自杀](../Page/来苏水.md "wikilink")，抢救无效身亡。终年52岁。

## 家庭

于会泳与妻子[女中音](../Page/女中音.md "wikilink")[任珂育有二女一男](../Page/任珂.md "wikilink")。于会泳自杀后，任珂与于会泳的母亲在1978年底回到上海居住。\[3\]

## 参考文献

### 引用

### 书籍

  - 温乐群等编《“文化大革命”中的名人之升》，北京：中央民族学院出版社1993年版；“威海典藏”www.weihai.gov.cn/whdc/html/14/12010401.htm
    73K 2002-7-10）
  - 《腔词关系研究》，于会泳著，中央音乐学院出版社2008年版，ISBN 9787810962476

{{-}}

[Category:中华人民共和国文化部部长](../Category/中华人民共和国文化部部长.md "wikilink")
[Category:中国共产党第十届中央委员会委员](../Category/中国共产党第十届中央委员会委员.md "wikilink")
[Category:中華人民共和國音樂家](../Category/中華人民共和國音樂家.md "wikilink")
[Category:中华人民共和国自杀政治人物](../Category/中华人民共和国自杀政治人物.md "wikilink")
[Category:京剧作曲家](../Category/京剧作曲家.md "wikilink")
[Category:中国作曲家](../Category/中国作曲家.md "wikilink")
[Category:中国指挥家](../Category/中国指挥家.md "wikilink")
[Category:文革时期人物](../Category/文革时期人物.md "wikilink")
[Category:中国共产党党员
(1949年入党)](../Category/中国共产党党员_\(1949年入党\).md "wikilink")
[Category:被开除中国共产党党籍者](../Category/被开除中国共产党党籍者.md "wikilink")
[Category:上海音乐学院校友](../Category/上海音乐学院校友.md "wikilink")
[Category:乳山人](../Category/乳山人.md "wikilink")
[H](../Category/于姓.md "wikilink")

1.
2.
3.