**于占元**（）為[京劇名](../Page/京劇.md "wikilink")[武生](../Page/武生.md "wikilink")，生于[香港](../Page/香港.md "wikilink")，1940年代在[上海享負盛名](../Page/上海.md "wikilink")，乃知名武戲教席兼後台武戲管事。後因大陸政治環境惡化而南遷[香港](../Page/香港.md "wikilink")，1960年代開辦中國戲劇研究學院。

## 影響

門下弟子眾多，桃李滿門，包括[七小福](../Page/七小福_\(演員\).md "wikilink")：元龍（即[洪金寶](../Page/洪金寶.md "wikilink")）、元樓（即[成龍](../Page/成龍.md "wikilink")）、[元彪](../Page/元彪.md "wikilink")、[元奎](../Page/元奎.md "wikilink")、[元華](../Page/元華.md "wikilink")、元武（[周元武](../Page/周元武.md "wikilink")）、[元泰](../Page/元泰_\(演員\).md "wikilink")7人。其他的有元慶（即[袁和平](../Page/袁和平.md "wikilink")）、[元秋](../Page/元秋.md "wikilink")、[元德](../Page/元德.md "wikilink")、[元俊](../Page/元俊.md "wikilink")
（即[吳元俊](../Page/吳元俊.md "wikilink")）、[元彬](../Page/元彬.md "wikilink")（[陶周坤](../Page/陶周坤.md "wikilink")）、元寶（[徐佑麟](../Page/徐佑麟.md "wikilink")），以及[林正英等](../Page/林正英.md "wikilink")。其女兒[-{于}-素秋為當時之著名](../Page/于素秋.md "wikilink")[電影紅星](../Page/電影.md "wikilink")，1970年代移居[美国](../Page/美国.md "wikilink")；其長女-{于}-素春的夫婿[韓英傑](../Page/韓英傑.md "wikilink")，是當時武術指導之一。

## 相關作品

[邵氏兄弟聯同](../Page/邵氏兄弟.md "wikilink")[嘉禾電影曾把于占元教導](../Page/嘉禾電影.md "wikilink")[七小福時的經歷拍成電影](../Page/七小福_\(演員\).md "wikilink")《[七小福](../Page/七小福_\(1988年電影\).md "wikilink")》，由[羅啟銳執導](../Page/羅啟銳.md "wikilink")，七小福之一的[洪金寶飾演于占元](../Page/洪金寶.md "wikilink")。該片大受好評，更贏得[金馬獎](../Page/金馬獎.md "wikilink")[最佳劇情片](../Page/金馬獎獎項列表#最佳劇情片.md "wikilink")。由於[洪金寶飾演傳奇人物師父于占元](../Page/洪金寶.md "wikilink")，演技神似嚴肅，備受肯定，榮獲第33屆[亞太影展最佳男主角獎](../Page/亞太影展.md "wikilink")。同年獲得第8屆[香港電影金像獎最佳男主角獎](../Page/香港電影金像獎.md "wikilink")。

## 參考

  - [從《七小福》看香港動作片發展](https://www.hk01.com/%E6%AD%A6%E5%82%99%E5%BF%97/153431/%E5%BE%9E-%E4%B8%83%E5%B0%8F%E7%A6%8F-%E7%9C%8B%E9%A6%99%E6%B8%AF%E5%8B%95%E4%BD%9C%E7%89%87%E7%99%BC%E5%B1%95)
  - [京劇與香港武打電影](https://www.hk01.com/%E6%AD%A6%E5%82%99%E5%BF%97/146022/%E4%BA%AC%E5%8A%87%E8%88%87%E9%A6%99%E6%B8%AF%E6%AD%A6%E6%89%93%E9%9B%BB%E5%BD%B1)

[Category:香港武打演員](../Category/香港武打演員.md "wikilink")
[Category:香港武術家](../Category/香港武術家.md "wikilink")
[J](../Category/于姓.md "wikilink")
[Category:京剧演员](../Category/京剧演员.md "wikilink")
[Category:北京人](../Category/北京人.md "wikilink")
[Category:移民美国的中国人](../Category/移民美国的中国人.md "wikilink")