**于孔兼**（），[字](../Page/表字.md "wikilink")**元時**，[號](../Page/號.md "wikilink")**泰景**，[直隸](../Page/南直隸.md "wikilink")[金壇](../Page/金壇.md "wikilink")（今屬[江蘇](../Page/江蘇.md "wikilink")）人，[晚明政治人物](../Page/晚明.md "wikilink")，同[進士出身](../Page/進士.md "wikilink")。

## 生平

應天府鄉試第一百二名，后參加會試第七十五名。萬曆八年，登進士第三甲第一百七十二名\[1\]\[2\]\[3\]。授[九江府](../Page/九江府.md "wikilink")[推官](../Page/推官.md "wikilink")。入為[禮部](../Page/禮部.md "wikilink")[主事](../Page/主事.md "wikilink")，再調儀制[郎中](../Page/郎中.md "wikilink")。[萬曆十七年](../Page/萬曆.md "wikilink")（1589年）[神宗下詔諸子中並封三王](../Page/神宗.md "wikilink")。孔兼與[員外郎](../Page/員外郎.md "wikilink")[陳泰來聯名上疏](../Page/陳泰來.md "wikilink")，神宗不理，孔兼又諫，終於被採納。

[萬曆二十一年](../Page/萬曆.md "wikilink")（1593年）[考功司](../Page/考功司.md "wikilink")[郎中](../Page/郎中.md "wikilink")[赵南星主持](../Page/赵南星.md "wikilink")[京察](../Page/京察.md "wikilink")，罢免大量冗官，得罪[首辅](../Page/首辅.md "wikilink")[王锡爵](../Page/王锡爵.md "wikilink")。赵南星因此被削職，[孙鑨被停俸](../Page/孙鑨.md "wikilink")。于孔兼上疏營救趙南星，皇帝恨他多言，謫為[吉安府](../Page/吉安府.md "wikilink")[判官](../Page/判官.md "wikilink")，遂辭官歸。家居二十年，筑室於县城西郊，其堂曰“志矩”，其亭曰“八卦”，著书不辍，曾講學[東林書院](../Page/東林書院.md "wikilink")。著有《春曹疏》、《山居稿》、《江州餘草》、《浮雲山居集》、《菜根譚題詞》。[天啟時追封為](../Page/天啟.md "wikilink")[光禄寺](../Page/光禄寺.md "wikilink")[少卿](../Page/少卿.md "wikilink")。[于玉立是其侄](../Page/于玉立.md "wikilink")。

曾祖父[于鎰](../Page/于鎰.md "wikilink")，曾任知縣
贈都察院右副都御史；祖父[于湛](../Page/于湛.md "wikilink")，曾任都察院右副都御史
右都御史；父親[于未](../Page/于未.md "wikilink")，曾任貢士。母吳氏\[4\]。

## 參考書目

  - 《明史》[卷231·列傳第一百十九](../Page/s:明史/卷231.md "wikilink")

## 参考文献

[Category:明朝九江府推官](../Category/明朝九江府推官.md "wikilink")
[Category:明朝禮部主事](../Category/明朝禮部主事.md "wikilink")
[Category:明朝禮部郎中](../Category/明朝禮部郎中.md "wikilink")
[Category:金壇人](../Category/金壇人.md "wikilink")
[K](../Category/于姓.md "wikilink")

1.
2.
3.
4.