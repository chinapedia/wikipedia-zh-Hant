**于貝爾·雷弗**（，），[加拿大](../Page/加拿大.md "wikilink")[天体物理学家](../Page/天体物理.md "wikilink")，科普推广者。

[Hubert_Reeves.jpg](https://zh.wikipedia.org/wiki/File:Hubert_Reeves.jpg "fig:Hubert_Reeves.jpg")

## 生平

出生于[蒙特利尔](../Page/蒙特利尔.md "wikilink")，1953年从[蒙特利尔大学科学系本科毕业](../Page/蒙特利尔大学.md "wikilink")，1955年从[麦吉尔大学获得](../Page/麦吉尔大学.md "wikilink")[硕士学位](../Page/硕士.md "wikilink")，硕士论文是“氢氦元素正电子的形成”1960年在[美国](../Page/美国.md "wikilink")[康乃尔大学获得](../Page/康乃尔大学.md "wikilink")[博士学位](../Page/博士.md "wikilink")，研究方向为[原子核物理](../Page/原子核.md "wikilink")。1960
年到 1964
年他在蒙特利尔大学教授物理，他还是[美國太空總署的科学顾问](../Page/美國太空總署.md "wikilink")。1965年之后他常住[法国](../Page/法国.md "wikilink")，在法国科学院从事研究工作。1971年他同两个学生合作发表了有关星系原子结构的论文。4年后他的著名论文
B2FH 被认为填补了宇宙形成大爆炸理论的空白。于贝尔·雷弗是1991年度加拿大 Order of Canada 奖获得者。

## 榮譽

  - 在 1976
    年被[法國總統](../Page/法國總統.md "wikilink")[季斯卡授予](../Page/季斯卡.md "wikilink")「國家功績勳章」
    （）的騎士勳位（）。
  - 在 1986
    年被法國總統[密特朗授予](../Page/密特朗.md "wikilink")[法國榮譽軍團勳章騎士勳位](../Page/法國榮譽軍團勳章.md "wikilink")，在
    1994 年再被前者授予軍官勳位（）。在 2003
    被法國總統[希拉克授予司令勳位](../Page/希拉克.md "wikilink")（Commandeur）。
  - 在 1991 年被授予[加拿大勳章](../Page/加拿大勳章.md "wikilink")（）騎士勳位，在 2003
    年再被授予高一等的同伴勳位（）。
  - 在 1994 年被授予魁北克民族勳章（）軍官勳位。
  - 小行星 9631 于贝尔·雷弗以他命名。

## 部分出版

  -
  -
  -
  -
  -
  -
  -
  -
## 外部連結

  - [Official site](http://www.hubertreeves.info/)

  - [Biography in
    English](http://science.ca/scientists/scientistprofile.php?pID=213)

[Category:加拿大物理学家](../Category/加拿大物理学家.md "wikilink")
[Category:康乃爾大學校友](../Category/康乃爾大學校友.md "wikilink")
[Category:麥吉爾大學校友](../Category/麥吉爾大學校友.md "wikilink")
[Category:蒙特婁大學校友](../Category/蒙特婁大學校友.md "wikilink")
[Category:法裔加拿大人](../Category/法裔加拿大人.md "wikilink")
[Category:蒙特婁人](../Category/蒙特婁人.md "wikilink")
[Category:天體物理學家](../Category/天體物理學家.md "wikilink")
[Category:阿尔伯特·爱因斯坦奖章获得者](../Category/阿尔伯特·爱因斯坦奖章获得者.md "wikilink")