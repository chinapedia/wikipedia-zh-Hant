\-{H|于越}-

**于越**，是[遼代的官名](../Page/遼代.md "wikilink")，来自于[突厥](../Page/古突厥语.md "wikilink")-[回鹘语üge](../Page/回鹘语.md "wikilink")\~öge（意为“官人、智者”，形容词意为“尊敬的”、“贤明的”\[1\]）\[2\]，位於百官之上，是[君主對功勞最](../Page/君主.md "wikilink")[大臣子的最高獎勵](../Page/大臣.md "wikilink")。在[遼代](../Page/遼代.md "wikilink")9位[皇帝](../Page/皇帝.md "wikilink")210年的統治期間，只有10人被拜為於越。于越是[辽国大于越府的首辅](../Page/辽国.md "wikilink")，无具体职掌，就是“大之极矣，所以没品。”

《[辽史](../Page/辽史.md "wikilink")·国语解》称：“于越，贵官，无所职。其位居北、南大王上，非有大功德者不授。”该官职不但在《[宋史](../Page/宋史.md "wikilink")》（卷4）及《辽史》（卷490）中都可见到，而且还常见于[敦煌出土的](../Page/敦煌.md "wikilink")9～11世纪的[于阗语](../Page/于阗语.md "wikilink")、[回鹘语文献中](../Page/回鹘语.md "wikilink")。据《辽史·属国表》：“统和八年（990年）六月，阿萨兰回鹘于越、达剌干各遣使来贡。”说明这个词很可能是直接来自回鹘语。\[3\]

其中著名的于越有拥立[阿保机称帝的](../Page/阿保机.md "wikilink")[耶律曷鲁](../Page/耶律曷鲁.md "wikilink")；拥立[耶律兀欲](../Page/耶律兀欲.md "wikilink")，[耶律璟继位的](../Page/耶律璟.md "wikilink")[耶律屋质](../Page/耶律屋质.md "wikilink")；在抵挡宋太宗赵光义北伐时，射伤[赵光义的](../Page/赵光义.md "wikilink")[耶律休哥](../Page/耶律休哥.md "wikilink")；辽道宗[耶律洪基讨平](../Page/耶律洪基.md "wikilink")[耶律重元的叛乱中救驾的](../Page/耶律重元.md "wikilink")[耶律仁先](../Page/耶律仁先.md "wikilink")（金庸小说[萧峰的原型](../Page/萧峰.md "wikilink")）。

## 参见

  - [夷離堇](../Page/夷離堇.md "wikilink")
  - [夷离毕](../Page/夷离毕.md "wikilink")
  - [惕隐](../Page/惕隐.md "wikilink")

## 注释

<references />

[Category:辽朝官制](../Category/辽朝官制.md "wikilink")

1.  热合木吐拉·艾山：回鹘文化对契丹的影响，新疆社会科学：维文,2009,vol.2:p.57-67。文字版可于[此处](http://big.hi138.com/wenhua/lishixue/200605/52239.asp)查看。

2.  [契丹的语言和契丹的大小字](http://www.im-eph.com/gb/sbwh/2008-12/03/content_3846.htm)


3.