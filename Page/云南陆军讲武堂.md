**雲南陆军讲武堂**，又稱**昆明講武堂**，是[中国近代一所著名的军事院校](../Page/中国.md "wikilink")，原是[清朝为编练新式](../Page/清朝.md "wikilink")[陆军](../Page/陆军.md "wikilink")，加强边防而设的一所军事学校。建立時与[天津讲武堂和](../Page/天津讲武堂.md "wikilink")[奉天讲武堂并称三大](../Page/奉天讲武堂.md "wikilink")[讲武堂](../Page/讲武堂.md "wikilink")，後与[黄埔军校](../Page/黄埔军校.md "wikilink")、[保定陆军军官学校齐名](../Page/保定陆军军官学校.md "wikilink")。

旧址位于昆明市[五华区](../Page/五华区.md "wikilink")[翠湖西侧](../Page/翠湖_\(昆明\).md "wikilink")，紧邻[云南省图书馆](../Page/云南省图书馆.md "wikilink")。

## 历史

1907年，清朝[雲貴總督](../Page/雲貴總督.md "wikilink")[王文韶委托曾公派留学](../Page/王文韶.md "wikilink")[日本陆军士官学校的](../Page/日本陆军士官学校.md "wikilink")[胡景伊在](../Page/胡景伊.md "wikilink")[云南陆军武备学堂和](../Page/云南陆军武备学堂.md "wikilink")[云南陆军小学基础上筹办云南陆军讲武堂](../Page/云南陆军小学.md "wikilink")，选址为[昆明翠湖承华圃](../Page/昆明.md "wikilink")，筹办期间[云贵总督易為](../Page/云贵总督.md "wikilink")[李經羲](../Page/李經羲.md "wikilink")，李改命与[胡景伊同校毕业的](../Page/胡景伊.md "wikilink")[韩建铎与胡景伊](../Page/韩建铎.md "wikilink")，共同于1909年8月再办讲武堂，首任总办由云南兵备处总办[高尔登兼任](../Page/高尔登.md "wikilink")，[李根源任校监](../Page/李根源.md "wikilink")，次年李接任总办。自1909年至1935年停办共开班22期，每期一年半至两年，分[步兵](../Page/步兵.md "wikilink")、[骑兵](../Page/骑兵.md "wikilink")、[炮兵](../Page/炮兵.md "wikilink")、[工兵四科](../Page/工兵.md "wikilink")（后增设有[辎重](../Page/辎重.md "wikilink")、[航空](../Page/航空.md "wikilink")、[空降等科](../Page/空降.md "wikilink")），1919年，李根源还在[广东](../Page/广东.md "wikilink")[韶关开设过讲武堂分校](../Page/韶关.md "wikilink")，共办两期。第15期还有归国[华侨和](../Page/华侨.md "wikilink")[朝鲜](../Page/朝鲜.md "wikilink")、[越南的留学生](../Page/越南.md "wikilink")，在抗戰期間为东南亚国家培养大量指挥军官。1912年改称**云南陆军讲武学校**。1935年后改名为“中央陆军学校第五分校”，开办至1945年停办。1949年[中华人民共和国建立后](../Page/中华人民共和国.md "wikilink")，改为[中国人民解放军昆明步兵学校](../Page/中国人民解放军昆明步兵学校.md "wikilink")。目前讲武堂的遗址用作博物馆用途，一方面展览历史文物；一方面展览云南地方艺术品，并由某拍卖公司入驻。

## 建筑

现存讲武堂的主要建筑物，是一院规模宏大的[中国传统的](../Page/中国传统建筑.md "wikilink")[走马转角楼式的土](../Page/走马转角楼式.md "wikilink")、木、石材建筑物，呈正方形，东、西、南、北楼各约长120米，宽10米，对称衔接，浑然一体，四角有拱形门洞可出人。南北楼为学员宿舍，南楼中部突起，为阅操楼。今农展馆一片，是当时的[阅兵](../Page/阅兵.md "wikilink")[操場](../Page/操場.md "wikilink")。东楼是[办公室](../Page/办公室.md "wikilink")，西楼是[學科](../Page/學科.md "wikilink")[教室](../Page/教室.md "wikilink")。大楼西北面的平房，是当时的[礼堂](../Page/礼堂.md "wikilink")。中华人民共和国成立后，这座建筑经多次修缮，保存较为完好。

## 历任校长

**云南陆军讲武堂总办**

  - [韩建铎](../Page/韩建铎.md "wikilink")（1907年－1907年，筹备阶段）
  - [胡景伊](../Page/胡景伊.md "wikilink")（1907年－1909年，筹备阶段）
  - [高尔登](../Page/高尔登.md "wikilink")（1909年－1910年）
  - [李根源](../Page/李根源.md "wikilink")（1910年－1911年8月22日）
  - [张毅](../Page/张毅_\(民国云南\).md "wikilink")（1911年－1912年）
  - [谢汝翼](../Page/谢汝翼.md "wikilink")（1912年－1912年）

**云南陆军讲武学校校长**\[1\]

  - [刘祖武](../Page/刘祖武.md "wikilink")（1912年－1913年）
  - [顾品珍](../Page/顾品珍.md "wikilink")（1914年－1915年）
  - [张子贞](../Page/张子贞.md "wikilink")（1916年－1916年）
  - [韩凤楼](../Page/韩凤楼.md "wikilink")（1917年－1917年）
  - [郑开文](../Page/郑开文.md "wikilink")（1917年－1918年）
  - [吴和宣](../Page/吴和宣.md "wikilink")（1918年－1918年）
  - [唐继虞](../Page/唐继虞.md "wikilink")（1919年－1919年）
  - [唐继虞](../Page/唐继虞.md "wikilink")（1920年－1920年，高等军事学校校长）
  - [韩建铎](../Page/韩建铎.md "wikilink")（1921年－1921年）
  - [戢翼翘](../Page/戢翼翘.md "wikilink")（？－？）
  - [刘国栋](../Page/刘国栋_\(云南\).md "wikilink")（1923年－1925年）
  - [高向春](../Page/高向春.md "wikilink")（1926年－1926年，讲武学校预科主任）
  - [龙云](../Page/龙云.md "wikilink")（1927年－1927年）
  - [胡若愚](../Page/胡若愚_\(云南\).md "wikilink")（1927年－1927年）
  - [吴和宣](../Page/吴和宣.md "wikilink")（1928年－1928年）
  - [王兆翔](../Page/王兆翔.md "wikilink")（1928年－1928年）

## 著名人物

由于该校教员多从[日本士官学校](../Page/日本士官学校.md "wikilink")（[日語的](../Page/日語.md "wikilink")「士官」等於[漢語的](../Page/漢語.md "wikilink")「軍官」，俗稱「將校」。漢語的「士官」在日語稱為「下士官」。）毕业，很多人在[日本](../Page/日本.md "wikilink")[留學时就是倾向](../Page/留學.md "wikilink")[革命黨的分子](../Page/革命黨.md "wikilink")，一部分就是[孙中山先生领导的](../Page/孙中山.md "wikilink")[同盟会会员](../Page/同盟会.md "wikilink")。他们采取多种方式，在学生中传播革命概要，使学校成为培养反清革命的重要据点，团结云南革命力量的核心。历届毕业生中有些后来加入[中國共產黨成為](../Page/中國共產黨.md "wikilink")[人民解放軍的重要](../Page/人民解放軍.md "wikilink")[將軍](../Page/將軍.md "wikilink")，如[朱德](../Page/朱德.md "wikilink")、[叶剑英等](../Page/叶剑英.md "wikilink")。

### 教官

  - [李根源](../Page/李根源.md "wikilink")
  - [顾品珍](../Page/顾品珍.md "wikilink")
  - [唐继尧](../Page/唐继尧.md "wikilink")
  - [李烈钧](../Page/李烈钧.md "wikilink")
  - [张开儒](../Page/张开儒.md "wikilink")
  - [罗佩金](../Page/罗佩金.md "wikilink")
  - [刘存厚](../Page/刘存厚.md "wikilink")
  - [赵又新](../Page/赵又新.md "wikilink")
  - [杨　杰](../Page/楊杰_\(大理\).md "wikilink")
  - [蔡润生](../Page/蔡润生.md "wikilink")
  - [李鴻祥](../Page/李鴻祥.md "wikilink")
  - [欧阳沂](../Page/欧阳沂.md "wikilink")

### 校友

  - [武元甲](../Page/武元甲.md "wikilink")
  - [崔庸健](../Page/崔庸健.md "wikilink")
  - [李范奭](../Page/李范奭.md "wikilink")
  - [权基玉](../Page/权基玉.md "wikilink")
  - [朱德](../Page/朱德.md "wikilink")
  - [叶剑英](../Page/叶剑英.md "wikilink")
  - [胡瑛](../Page/胡瑛.md "wikilink")
  - [盛世才](../Page/盛世才.md "wikilink")
  - [赖心辉](../Page/赖心辉.md "wikilink")
  - [龙云](../Page/龙云.md "wikilink")
  - [寸性奇](../Page/寸性奇.md "wikilink")
  - [周保中](../Page/周保中.md "wikilink")
  - [曾泽生](../Page/曾泽生.md "wikilink")
  - [范石生](../Page/范石生.md "wikilink")
  - [楊蓁](../Page/楊蓁.md "wikilink")
  - [唐淮源](../Page/唐淮源.md "wikilink")
  - [朱培德](../Page/朱培德.md "wikilink")
  - [金汉鼎](../Page/金汉鼎.md "wikilink")
  - [卢汉](../Page/卢汉.md "wikilink")
  - [王甲本](../Page/王甲本.md "wikilink")
  - [赵锡光](../Page/赵锡光.md "wikilink")
  - [毕士悌](../Page/毕士悌.md "wikilink")
  - [李范奭](../Page/李范奭.md "wikilink")
  - [段希文](../Page/段希文.md "wikilink")
  - [樊云章](../Page/樊云章.md "wikilink")
  - [张鉴桂](../Page/张鉴桂.md "wikilink")

## 参考文献

## 外部链接

## 参见

  - [讲武堂](../Page/讲武堂.md "wikilink")
      - [奉天讲武堂](../Page/奉天讲武堂.md "wikilink")
      - [天津讲武堂](../Page/天津讲武堂.md "wikilink")
      - [四川陸軍講武堂](../Page/四川陸軍講武堂.md "wikilink")

{{-}}

[Category:讲武堂](../Category/讲武堂.md "wikilink")
[Category:云南军事院校](../Category/云南军事院校.md "wikilink")
[Category:云南全国重点文物保护单位](../Category/云南全国重点文物保护单位.md "wikilink")
[Category:昆明文物保护单位](../Category/昆明文物保护单位.md "wikilink")
[Category:1945年廢除](../Category/1945年廢除.md "wikilink")
[Category:五华区](../Category/五华区.md "wikilink")

1.  续云南通志长编（上册），云南省志编纂委员会办公室，1985年，第1144页