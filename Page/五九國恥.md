[Yuan_shikai.jpg](https://zh.wikipedia.org/wiki/File:Yuan_shikai.jpg "fig:Yuan_shikai.jpg")\]\]

**五九國恥**指1915年5月9日，[中華民國第](../Page/中華民國.md "wikilink")1任[大總統](../Page/中華民國大總統.md "wikilink")[袁世凱经过与日本長达](../Page/袁世凱.md "wikilink")105天艱困的谈判和周旋之后，被迫接受[日本](../Page/日本.md "wikilink")《[二十一條](../Page/二十一條.md "wikilink")》中的十二条內容之事件。\[1\]条约签订后，袁世凯亲书两道密谕，要各省文武长官勿忘签约的5月9日为国耻日，密谕号召“凡百职司，日以‘亡国灭种’四字悬诸心目”，“申儆人民，忍辱负重”\[2\]\[3\]。[全国教育联合会决定](../Page/全国教育联合会.md "wikilink")，各学校每年以5月9日为“国耻纪念日”举行纪念，借此警励国人毋忘此日，誓雪国耻這一天被民眾稱為「國恥日」。\[4\]

## 概述

1914年，[第一次世界大戰爆發](../Page/第一次世界大戰.md "wikilink")，日本派兵佔領[德國在](../Page/德國.md "wikilink")[中國](../Page/中國.md "wikilink")[山東的](../Page/山東.md "wikilink")[殖民地和](../Page/殖民地.md "wikilink")[鐵路](../Page/鐵路.md "wikilink")。1915年，日本欲乘[歐](../Page/歐洲.md "wikilink")[美各國無暇東顧](../Page/美國.md "wikilink")，秘密向[袁世凱提出](../Page/袁世凱.md "wikilink")《[二十一條](../Page/二十一條.md "wikilink")》條款，要求中國承認日本取代德國的特權，進一步擴大日本在[滿](../Page/中國東北.md "wikilink")[蒙的權益](../Page/蒙古地方.md "wikilink")，並聘用[日本人為](../Page/日本人.md "wikilink")[行政顧問等職位](../Page/行政.md "wikilink")。日本的要求接近於將中國淪為其[保護國](../Page/保護國.md "wikilink")。\[5\]

1915年1至4月，袁世凯一面派外交总长[陆征祥](../Page/陆征祥.md "wikilink")、次长[曹汝霖与日本谈判](../Page/曹汝霖.md "wikilink")，一方面暗中泄露二十一条内容，希望获得[英](../Page/英国.md "wikilink")[美等国的支持以抵制日本](../Page/美国.md "wikilink")。中国的谈判代表多次拒绝条约中的部份内容，迫使日本做出让步。在谈判期间，日本以“换防”为名，增兵[大连](../Page/大连.md "wikilink")、[青岛](../Page/青岛.md "wikilink")、[塘沽等地](../Page/塘沽.md "wikilink")，进行武力威胁。至5月7日，日本政府向袁世凯政府发出最后通牒，限令于5月9日前答复，其中仅把原来的第五号内容改为日后另行协商。\[6\]在日本的[最後通牒之下](../Page/最後通牒.md "wikilink")，袁世凱被迫派[外交總長](../Page/中華民國外交.md "wikilink")[陸徵祥及次長](../Page/陸徵祥.md "wikilink")[曹汝霖簽署接受二十一條部分條款](../Page/曹汝霖.md "wikilink")，史上稱為「五九國恥」，虽然如此，但正是袁世凯本人在签订后，为告诫国民，亲自将这一天定为“国耻日”。

## 參見

  - [中華帝國](../Page/中華帝國.md "wikilink")
  - [袁世凱](../Page/袁世凱.md "wikilink")
  - [二十一條](../Page/二十一條.md "wikilink")

## 参考文献

## 参见

  - [二十一条](../Page/二十一条.md "wikilink")
  - [巴黎和会](../Page/巴黎和会.md "wikilink")[五四运动](../Page/五四运动.md "wikilink")

[Category:1915年中国政治事件](../Category/1915年中国政治事件.md "wikilink")
[Category:中華民國與日本關係](../Category/中華民國與日本關係.md "wikilink")

1.

2.

3.

4.

5.
6.