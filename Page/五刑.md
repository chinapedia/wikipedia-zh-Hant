**五刑**是[中国](../Page/中国.md "wikilink")[古代五种](../Page/古代.md "wikilink")[刑罚之统称](../Page/刑罚.md "wikilink")，在不同时期，五种刑罚的具体所指并不相同。在[西汉](../Page/西汉.md "wikilink")[文帝前](../Page/汉文帝.md "wikilink")，五刑指[墨](../Page/墨刑.md "wikilink")、[劓](../Page/劓.md "wikilink")、[刖](../Page/刖刑.md "wikilink")、[宫](../Page/宫刑.md "wikilink")、[大辟](../Page/大辟.md "wikilink")；[隋唐之後](../Page/隋唐.md "wikilink")，五刑则指[笞](../Page/鞭笞.md "wikilink")、[杖](../Page/杖.md "wikilink")、[徒](../Page/徒刑.md "wikilink")、[流](../Page/流放.md "wikilink")、[死](../Page/死刑.md "wikilink")。五刑是对中国古代刑罚的部分概括，并不代表全部刑罚制度。

## 起源

五刑最早源于[有苗氏部落](../Page/有苗氏.md "wikilink")，另有一說源於上古時代[蚩尤領導的](../Page/蚩尤.md "wikilink")[九黎族](../Page/九黎族.md "wikilink")。有苗氏亡于[夏启后](../Page/夏启.md "wikilink")，夏启将有苗氏推行的刖、劓、琢、黥等刑加以损益，形成了墨、劓、刖、宫、大辟五种刑罚，并使之成为主要的刑罚体系。自[夏以后](../Page/夏.md "wikilink")、[商](../Page/商.md "wikilink")、[周及](../Page/周.md "wikilink")[春秋之际](../Page/春秋時期.md "wikilink")，五刑一直被作为主体刑而广泛使用。先秦时期的五刑在[汉文帝时期因为](../Page/汉文帝.md "wikilink")[缇萦上书而被废除](../Page/缇萦.md "wikilink")，由笞、杖、徒、流、死五種刑罰取代。

## 先秦时期的五刑

先秦时期的五刑中除大辟为[死刑](../Page/死刑.md "wikilink")，其余四种皆为[肉刑](../Page/肉刑.md "wikilink")，并对人体造成不可回復之傷害。

  - 墨，又称黥，在受刑者面上或[额头](../Page/额头.md "wikilink")[刺青](../Page/刺青.md "wikilink")，并染上墨。
  - 劓，割去受刑者的[鼻子](../Page/鼻.md "wikilink")。
  - 刖，夏称膑，周称刖，秦称斩趾。斩掉受罚者左[脚](../Page/脚.md "wikilink")、右脚或双脚。有另一说称膑是去掉[膝盖骨](../Page/膝盖.md "wikilink")。
  - 宫，又称淫刑、腐刑、蚕室刑，割去受罚者的[生殖器](../Page/生殖器.md "wikilink")。
  - 大辟，即死刑，分为斬（[枭首](../Page/枭首.md "wikilink")、[弃市](../Page/弃市.md "wikilink")）、[刺](../Page/刺刑.md "wikilink")、[绞](../Page/絞刑.md "wikilink")、[烹](../Page/烹刑.md "wikilink")、[坑](../Page/活埋.md "wikilink")、[腰斬](../Page/腰斬.md "wikilink")、[车裂](../Page/车裂.md "wikilink")、[凌迟等](../Page/凌迟.md "wikilink")，還有死後[鞭屍](../Page/鞭屍.md "wikilink")、[戮屍](../Page/戮屍.md "wikilink")、[脯刑](../Page/脯刑.md "wikilink")、[醢刑等](../Page/醢刑.md "wikilink")。

## 漢至清朝的五刑

  - 笞，用小荆条拧成的刑具抽打受刑者[臀部](../Page/臀.md "wikilink")，[清朝时刑具改为竹板](../Page/清朝.md "wikilink")。分五等：一十、二十、三十、四十、五十。
  - 杖，用粗荆条拧成的刑具抽打受刑者的[背](../Page/背.md "wikilink")、臀和[腿](../Page/腿.md "wikilink")。也分五等：六十、七十、八十、九十和一百。
  - 徒，强制犯人劳役。分五等：一年，一年半，两年，两年半，三年。
  - 流，将犯人[流放到](../Page/流放.md "wikilink")[邊疆](../Page/邊疆.md "wikilink")，不准回乡。分三等：两千里，两千五百里，三千里。
  - 死，隋廢[斬](../Page/斬首.md "wikilink")，隋唐死刑一般為[絞](../Page/絞刑.md "wikilink")。[宋後加](../Page/宋朝.md "wikilink")[凌迟](../Page/凌迟.md "wikilink")。明[斬首復見於律](../Page/斬首.md "wikilink")。

## 女犯五刑

对[女性犯人](../Page/女性.md "wikilink")，五刑是指：

  - [刑舂](../Page/刑舂.md "wikilink")
  - [拶刑](../Page/拶刑.md "wikilink")
  - 杖刑
  - 赐死
  - [宫刑](../Page/宫刑.md "wikilink")

## 外部链接

  - [五刑和肉刑](https://web.archive.org/web/20070204232718/http://www.chinaculture.org/gb/cn_zggk/2004-06/28/content_55223.htm)
  - [中国法制史](https://web.archive.org/web/20081122131737/http://www.hongen.com/proedu/flxy/flss/fd/zhk/fs051804.htm)

[Category:中國古代刑罰](../Category/中國古代刑罰.md "wikilink")