**五合星团**（）是位于[银河系中心](../Page/银河系.md "wikilink")（[人馬座方向](../Page/人馬座.md "wikilink")）附近的一个[疏散星团](../Page/疏散星团.md "wikilink")，距地球约26000[光年](../Page/光年.md "wikilink")，是迄今为止发现的质量最大的疏散星团之一。

五合星团距离银河系中心大约100光年，年龄只有4百万年，是一个相对年轻的星团。由于尘埃的遮挡，至1990年才通过[红外望远镜发现它](../Page/红外望远镜.md "wikilink")。在[红外波段](../Page/红外波段.md "wikilink")，星团中有5颗星非常明亮，因此得名为五合星团，又称五星团、五重星团、五胞胎星团等。

这个星团中有一些大质量的恒星，比如目前银河系中发现的质量最大的恒星──[手枪星就位于其中](../Page/手枪星.md "wikilink")。这些大质量恒星吹出猛烈的[星风](../Page/星风.md "wikilink")，星风相互碰撞，将气体加热到很高的温度，产生明亮的[X射线](../Page/X射线.md "wikilink")。五合星团中有一些红色和蓝色的巨星正处于[超新星爆发的边缘](../Page/超新星爆发.md "wikilink")。由于受到银河系中心的[引力影响](../Page/引力.md "wikilink")，科学家预言这个星团会在数百万年后解体。

## 参考资料

## 外部链接

  - 哈勃太空望远镜[1997年9月拍摄的五合星团](http://hubblesite.org/newscenter/newsdesk/archive/releases/1999/30/)
  - 钱德拉望远镜[在X射线波段拍摄的五合星团](http://chandra.harvard.edu/photo/2004/quint/)
  - [五合星团的动画](http://chandra.harvard.edu/photo/2004/quint/animations.html)
  - [Scientists find 'pinwheels' in Quintuplet
    cluster](http://www.umich.edu/news/index.html?Releases/2006/Aug06/r081706a)，美國密歇根大學2006年8月17日

[Category:疏散星团](../Category/疏散星团.md "wikilink")
[Category:人马座](../Category/人马座.md "wikilink")