**五城目町**（）是一位于[秋田縣中部的](../Page/秋田縣.md "wikilink")[町](../Page/町.md "wikilink")。面積214.94平方公里，總人口11,706人。

## 地理

  - 山
      - [馬場目岳](../Page/馬場目岳.md "wikilink")
      - [森山](../Page/森山.md "wikilink")。
  - 河川: [馬場目川](../Page/馬場目川.md "wikilink")

### 相鄰的自治体

  - [秋田市](../Page/秋田市.md "wikilink")
  - [南秋田郡](../Page/南秋田郡.md "wikilink")：[八郎潟町](../Page/八郎潟町.md "wikilink")、[井川町](../Page/井川町.md "wikilink")、[大潟村](../Page/大潟村.md "wikilink")
  - [北秋田郡](../Page/北秋田郡.md "wikilink")：[上小阿仁村](../Page/上小阿仁村.md "wikilink")
  - [山本郡](../Page/山本郡.md "wikilink")：[三種町](../Page/三種町.md "wikilink")

## 外部連結

  - [五城目町](https://web.archive.org/web/20070808021633/http://www.cs.town.gojome.akita.jp/gojome/yakuba/index.htm)