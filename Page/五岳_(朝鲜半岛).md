朝鲜半岛的**五岳**，指的是[朝鲜半岛的五座名](../Page/朝鲜半岛.md "wikilink")[山](../Page/山.md "wikilink")，其命名是受到了[中国五岳文化的影响](../Page/五岳.md "wikilink")。五岳源于[新罗时代](../Page/新罗.md "wikilink")，当时，[新罗统一朝鲜半岛之后](../Page/统一新罗.md "wikilink")，新罗王采取了中国化的政策，在地名上全面采用了[中国的体例](../Page/中国.md "wikilink")，也设置了五岳。\[1\]其中以吐含山（土含山）为东岳、[鸡龙山](../Page/鸡龙山.md "wikilink")（界龙山）为西岳，地理山（支离山）为南岳、太伯山（太白山，现位于[江原道](../Page/江原道.md "wikilink")[太白市](../Page/太白市.md "wikilink")，非[妙香山](../Page/妙香山.md "wikilink")）\[2\]为北岳、八公山（父岳）为中岳。其中，八公山位于[大邱附近](../Page/大邱.md "wikilink")，作为中岳受到崇拜，可以看出大邱的重要程度。\[3\]后来，[李氏朝鲜的五岳演变成了](../Page/李氏朝鲜.md "wikilink")[白头山](../Page/白头山.md "wikilink")、[妙香山](../Page/妙香山.md "wikilink")、[金刚山](../Page/金刚山.md "wikilink")、[三角山](../Page/三角山.md "wikilink")（现在的[北汉山](../Page/北汉山.md "wikilink")）、[智异山](../Page/智异山.md "wikilink")，\[4\]
其中三角山为中岳，位于[首尔城北区](../Page/首尔.md "wikilink")，现在韩国的国立公园。\[5\]

## 参见

  - [中国五岳](../Page/五岳.md "wikilink")
  - [天符经](../Page/天符经.md "wikilink")

## 注釋

<div class="references-small">

<references />

</div>

[Category:朝鮮半島山峰](../Category/朝鮮半島山峰.md "wikilink")

1.  [中国传统地名学对韩国的影响](http://www.guoxue.com/jrxz/zgdmxskl/diming5-1.htm)
2.  [《三国遗事》](http://books.google.cn/books?id=KrcBSpLn9-sC&pg=PA29&lpg=PA29&dq=%E8%A7%A3%E6%85%95%E6%BC%B1+%E6%89%B6%E4%BD%99&source=web&ots=XqbCJnsqXA&sig=iXK0yyL7a00nI0GEjFENPDwa6Jg&hl=zh-CN&sa=X&oi=book_result&resnum=9&ct=result#PPA29,M1)
3.  [八公山](http://chinese.daegu.go.kr/AboutDaegu/Introduction/history.asp)
4.
5.  [中国与朝鲜交往史料——世祖卷](http://blog.tianya.cn/blogger/post_show.asp?BlogID=757727&PostID=10396624)