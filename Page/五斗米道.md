**-{五斗米道}-**又称**[正一盟威之道](../Page/正一道.md "wikilink")**（**[正一道](../Page/正一道.md "wikilink")**）、**[天师道](../Page/天师道.md "wikilink")**，是[道教最早的一个派别](../Page/道教.md "wikilink")。据史书记载，在[东汉](../Page/东汉.md "wikilink")[顺帝时期](../Page/汉顺帝.md "wikilink")，由[张道陵在](../Page/张道陵.md "wikilink")[蜀郡](../Page/蜀郡.md "wikilink")[鹤鸣山](../Page/鹤鸣山.md "wikilink")（今[四川](../Page/四川.md "wikilink")[成都市](../Page/成都.md "wikilink")[大邑县北](../Page/大邑.md "wikilink")）创立。据《[后汉书](../Page/后汉书.md "wikilink")》、《[三国志](../Page/三国志.md "wikilink")》记载，凡入道者须出-{五斗米}-，故得此名，因又称为“**米巫**”、“**米贼**”、“**米道**”。另外，也有人认为，这个名称也可能和崇拜五方星斗（-{南斗}-、[北斗等](../Page/北斗七星.md "wikilink")）和[斗姆有关](../Page/斗姆.md "wikilink")，-{五斗米}-就是“-{五斗}-姆”（另一說法是-{五斗}-崇拜和蜀地的彌教結合而成，即「-{五斗}-彌」教）。因教徒尊张道陵为[天师](../Page/天师.md "wikilink")，又称“[天师道](../Page/天师道.md "wikilink")”。

## 历史

\-{五斗米道}-初入道者称为“[鬼卒](../Page/鬼卒.md "wikilink")”，骨干称为“[祭酒](../Page/祭酒.md "wikilink")”，并以“治”为[传教单位](../Page/传教.md "wikilink")。张道陵死后，传其子[张衡](../Page/张衡_\(道教\).md "wikilink")，衡死后，传子[张鲁](../Page/张鲁.md "wikilink")。[东汉末年](../Page/东汉.md "wikilink")，張鲁自命[諸侯](../Page/諸侯.md "wikilink")，佔据[汉中](../Page/汉中.md "wikilink")，建立持续达近30年[政教合一的政权](../Page/政教合一.md "wikilink")，后投降[曹操](../Page/曹操.md "wikilink")。

此外，还有一种说法认为是[张修在](../Page/张修.md "wikilink")[汉中创立了](../Page/汉中.md "wikilink")-{五斗米道}-，而張魯只是繼承人。因为，[曹丕所作的](../Page/曹丕.md "wikilink")《[典略](../Page/典略.md "wikilink")》记载:“灵帝熹平中，妖贼大起，
[三辅有](../Page/三辅.md "wikilink")[骆曜](../Page/骆曜.md "wikilink")，光和中，东方有[张角](../Page/张角.md "wikilink")，汉中有[張脩](../Page/張脩.md "wikilink")。骆曜教民缅匿法，角为太平道，脩为五斗⋯脩法，略与角同⋯⋯后角被诛，修亦亡，及鲁在汉中，因其民信行脩业，遂增饰之。”在提到灵帝时妖贼大起时，提到了张脩和五斗米道，但没有提及张道陵和张衡父子。《典略》还点明，五斗米道本来起源于[張脩](../Page/張脩.md "wikilink")，后来张鲁夺取了汉中，因为百姓相信张脩的五斗米道，便对其进行增加修饰，把五斗米道创始人的身份安在了自己的祖父张道陵身上。

[西晋后](../Page/西晋.md "wikilink")，-{五斗米道}-逐渐分化，一部分传播于[士族與](../Page/士族.md "wikilink")[官吏中](../Page/官吏.md "wikilink")，另一部分仍秘密活动于[农民中](../Page/农民.md "wikilink")。[东晋後期](../Page/东晋.md "wikilink")，-{五斗米道}-[领袖](../Page/领袖.md "wikilink")[孙恩](../Page/孙恩.md "wikilink")、[卢循领导了长达十余年的](../Page/卢循.md "wikilink")[民变](../Page/民变.md "wikilink")，世稱「[孫恩盧循之亂](../Page/孫恩盧循之亂.md "wikilink")」。[北魏时](../Page/北魏.md "wikilink")，[嵩山道士](../Page/嵩山.md "wikilink")[寇谦之](../Page/寇谦之.md "wikilink")“革新”-{五斗米道}-，自称奉[太上老君之命](../Page/太上老君.md "wikilink")，清整道教，“除去三张伪法”
，创立新天师道。此后-{五斗米道}-改称[天师道](../Page/天师道.md "wikilink")。并得到[太武帝的赞许](../Page/拓跋焘.md "wikilink")。[唐宋以后的道教正一派](../Page/唐宋.md "wikilink")，上承三张世系，以[江西](../Page/江西.md "wikilink")[龙虎山天师府为中心](../Page/龙虎山.md "wikilink")，是为江南各符箓道派之正宗。

## 内容

\-{五斗米道}-以[老子为教主](../Page/老子.md "wikilink")，基本经典是《[道德经](../Page/道德经.md "wikilink")》和[張陵編寫的](../Page/張陵.md "wikilink")『老子想爾注』，是一种[多神教](../Page/多神教.md "wikilink")，以[长生不老或](../Page/长生不老.md "wikilink")[成仙为其最高目标](../Page/成仙.md "wikilink")。其[道术主要是通过章表](../Page/道术.md "wikilink")，符咒招神驱鬼（例如用[三官手书来治病](../Page/三官手书.md "wikilink")），以及[行气](../Page/行气.md "wikilink")、[导引](../Page/导引.md "wikilink")、[房中术等](../Page/房中术.md "wikilink")。主要活动在[成都周围](../Page/成都.md "wikilink")，也可能在[少数民族中传播](../Page/少数民族.md "wikilink")。许多学者认为-{五斗米道}-与当地[少数民族所盛行的](../Page/少数民族.md "wikilink")[巫术有很密切的关系](../Page/巫术.md "wikilink")。道士能替信徒[上章向](../Page/上章.md "wikilink")[三清](../Page/三清.md "wikilink")、[玉帝](../Page/玉帝.md "wikilink")、[三官等神](../Page/三官.md "wikilink")，[許願求福](../Page/許願.md "wikilink")。

## 戒律

\-{五斗米道}-的戒律主要要求教徒不能酗酒、殺生、淫盗。教徒必须在一定的时候到治所集会，听讲“科禁威仪”，回家向家人传达，共同遵守。

## 二十四治

张道陵为-{五斗米道}-创设了二十四治，也就是24个教区。

|      |                                           |
| ---- | ----------------------------------------- |
| 上治八品 | 陽-{平治}-、鹿堂山治、鶴鳴山治、漓沅山治、葛王貴山治、更除治、秦中治、真多治。 |
| 中治八品 | 昌利治、隸上治、湧泉治、稠禾更治、北-{平治}-、本竹治、蒙秦治、平蓋治。     |
| 下治八品 | 雲台山治、濜口治、後城治、公慕治、平剛治、主簿山治、玉局治、北邙治。        |

當時的地理分佈為：

|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |

每個治由「祭酒」主持教務，稱為「都功」。其中以陽平、鹿堂、鶴鳴為最上三治。而陽平治則為二十四治之首治，「都功」由張天師擔任，因此張天師持「陽-{平治}-都功印」。

治乃行政概念，並非按地理劃分，例如陽-{平治}-的位置，即隨張天師的遷移而改變，所以當後代張天師遷徙到[江西](../Page/江西.md "wikilink")[廣信府](../Page/廣信府.md "wikilink")[貴溪縣](../Page/貴溪縣.md "wikilink")[龍虎山時](../Page/龍虎山.md "wikilink")，亦稱之為「陽-{平治}-」。

## 参看

  - [天師道](../Page/天師道.md "wikilink")
  - [張道陵](../Page/張道陵.md "wikilink")
  - [老子想尔注](../Page/老子想尔注.md "wikilink")

## 延伸閱讀

  - 石泰安：〈[公元2世纪政治的宗教的道教运动](http://www.nssd.org/articles/article_read.aspx?id=71747265504848514849485051)〉。

[category:道派](../Page/category:道派.md "wikilink")
[category:道教历史](../Page/category:道教历史.md "wikilink")
[category:晋朝宗教](../Page/category:晋朝宗教.md "wikilink")

[Category:汉朝宗教](../Category/汉朝宗教.md "wikilink")