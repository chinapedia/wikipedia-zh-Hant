**五瀨町**（）是位于[日本](../Page/日本.md "wikilink")[宮崎縣西北部的一個](../Page/宮崎縣.md "wikilink")[城鎮](../Page/城鎮.md "wikilink")，屬[西臼杵郡](../Page/西臼杵郡.md "wikilink")。全境位於[九州山地內](../Page/九州山地.md "wikilink")。雖然屬於宮崎縣，但由於交通的關係，生活圈屬於熊本縣。

## 历史

### 年表

  - 1889年5月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬[鞍岡村和](../Page/鞍岡村.md "wikilink")[三所村](../Page/三所村.md "wikilink")。
  - 1956年8月1日：鞍岡村和三所村[合併為](../Page/市町村合併.md "wikilink")**五瀨町**。

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年5月1日</p></th>
<th><p>1889年－1944年</p></th>
<th><p>1945年－1954年</p></th>
<th><p>1955年－1988年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>鞍岡村</p></td>
<td><p>1956年8月1日<br />
合併為五瀨町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>三所村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 行政

  - 町長：[飯干辰己](../Page/飯干辰己.md "wikilink")（2期目）

## 交通

### 道路

  - 高速道路

<!-- end list -->

  - [九州中央自動車道](../Page/九州中央自動車道.md "wikilink")：[五瀨交流道](../Page/五瀨交流道.md "wikilink")

## 教育

### 中等教育學校

  - [宮崎縣立五瀨中等教育學校](../Page/宮崎縣立五瀨中等教育學校.md "wikilink")：日本第一個中等教育學校

## 參考資料

## 外部連結

  - [五瀨町商工會](http://www.miya-shoko.or.jp/gokase/)