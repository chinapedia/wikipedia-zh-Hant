**五甲龍成宮**，是[臺灣](../Page/臺灣.md "wikilink")[高雄市](../Page/高雄市.md "wikilink")[鳳山區](../Page/鳳山區.md "wikilink")[五甲庄的一間](../Page/五甲庄.md "wikilink")[媽祖廟](../Page/媽祖廟.md "wikilink")、[祖師廟](../Page/祖師廟.md "wikilink")，在五甲自強夜市裏，地址為鳳山區五甲二路730巷6號。在[五甲眾多寺廟中](../Page/五甲.md "wikilink")，稱得上是規模最雄偉的廟宇，故俗稱「**五甲大廟**」，簡稱「**五甲廟**」。龍成宮主體殿宇建築亦獨步全台，主祀[天上聖母](../Page/天上聖母.md "wikilink")、[清水祖師](../Page/清水祖師.md "wikilink")、戴府元帥，合稱「三[境主](../Page/境主.md "wikilink")」，庇佑五甲居民。龍成宮不僅是鳳山五甲地區十餘萬人口的信仰中心，因為附設閱覽室，同時也是鳳山一帶青年學子溫習功課的好去處，常常座無虛席，一位難求。

## 主神：「三境主」的由來

[五甲龍成宮內.jpg](https://zh.wikipedia.org/wiki/File:五甲龍成宮內.jpg "fig:五甲龍成宮內.jpg")
[湄洲天后宮](../Page/湄洲天后宮.md "wikilink")[媽祖](../Page/媽祖.md "wikilink")[香火最先隨渡海來臺的移民來臺](../Page/香火.md "wikilink")，[清聖祖](../Page/清聖祖.md "wikilink")[康熙年間](../Page/康熙.md "wikilink")，五甲庄民為之塑造金身奉祀。[清高宗](../Page/清高宗.md "wikilink")[乾隆年間](../Page/乾隆.md "wikilink")，庄民先為[湄洲媽祖像興建草壇奉祀](../Page/湄洲.md "wikilink")，此壇即五甲龍成宮的前身。

而[清水祖師則是五甲庄一位楊姓人士](../Page/清水祖師.md "wikilink")，某日在海邊[捕魚時](../Page/捕魚.md "wikilink")，看到海上飄來一尊福建[泉州](../Page/泉州.md "wikilink")[安溪的守護神](../Page/安溪.md "wikilink")「[清水祖師](../Page/清水祖師.md "wikilink")」的神像，突然金光閃閃，海波變色。楊即認為是一大祥瑞，迎回家中供奉[香火](../Page/香火.md "wikilink")，靈驗不斷，於是也開放給庄民膜拜。

其後，有一福建來的雜貨商，背負[元帥神戴府元帥金身](../Page/王爺神.md "wikilink")，到達五甲做生意，卻在五甲巧遇當年在福建的同鄉舊識，因覺有緣，慨將戴府元帥金身留給五甲的同鄉供奉。[清德宗](../Page/清德宗.md "wikilink")[光緒十二年](../Page/光緒.md "wikilink")（西元1886年），此壇因庄民欲壯神威，將三尊神像合祀，遂以「三[境主](../Page/境主.md "wikilink")」（三位本境的主神）尊稱之。

## 廟宇重建

[五甲庄開發的起源](../Page/五甲庄.md "wikilink")，可溯至[明鄭](../Page/明鄭.md "wikilink")[永曆十五年](../Page/永曆.md "wikilink")（西元1661年），[鄭成功在登陸](../Page/鄭成功.md "wikilink")[鹿耳門後](../Page/鹿耳門.md "wikilink")，派遣[軍人南下此地](../Page/軍人.md "wikilink")[屯田](../Page/屯田.md "wikilink")，到了清末，還只是一個數百人的小村落，1970年代，高雄市[前鎮地區發展加工出口區](../Page/前鎮區.md "wikilink")，外來人口大量湧入，並延伸至時為[高雄縣](../Page/高雄縣.md "wikilink")[鳳山市的五甲地區](../Page/鳳山區.md "wikilink")，使五甲地區人口暴漲，地價狂飆，隨著2010年的[五都改制](../Page/五都改制.md "wikilink")，鳳山改隸[高雄市](../Page/高雄市.md "wikilink")，地價更是漲聲不斷。

五甲地區人丁未旺之前，龍成宮僅是庄內居民信仰中心。拜土地與房屋交易之盛，務農的五甲在地人頓成富裕的地主「田僑」，咸認是「三境主」（[媽祖](../Page/媽祖.md "wikilink")、[清水祖師](../Page/清水祖師.md "wikilink")、[戴府元帥](../Page/戴府元帥.md "wikilink")）神威顯赫庇佑所致。於是在西元1983年動工重建龍成宮，歷五年，終於在西元1988年舉行落成建醮。除了「三境主」之外也加祀了[釋迦文佛](../Page/釋迦文佛.md "wikilink")、[觀音菩薩](../Page/觀音菩薩.md "wikilink")、[至聖先師](../Page/孔子.md "wikilink")、[玉皇大帝](../Page/玉皇大帝.md "wikilink")、[太上老君](../Page/太上老君.md "wikilink")、[三官大帝](../Page/三官大帝.md "wikilink")、[斗姥元君](../Page/斗姥元君.md "wikilink")、[神農大帝](../Page/神農大帝.md "wikilink")、[南斗星君](../Page/南斗星君.md "wikilink")、[北斗星君](../Page/北斗星君.md "wikilink")、[東嶽大帝](../Page/東嶽大帝.md "wikilink")、[文昌帝君](../Page/文昌帝君.md "wikilink")、[堅牢地神](../Page/堅牢地神.md "wikilink")、[中壇元帥](../Page/中壇元帥.md "wikilink")、[關聖帝君](../Page/關聖帝君.md "wikilink")、[延平郡王](../Page/國姓爺.md "wikilink")、[東廚帝君](../Page/灶君.md "wikilink")、[城隍尊神](../Page/城隍爺.md "wikilink")、[註生娘娘](../Page/註生娘娘.md "wikilink")、[哪吒太子](../Page/哪吒.md "wikilink")、[甲子太歲星君](../Page/太歲.md "wikilink")、[千里眼將軍](../Page/千里眼.md "wikilink")、[順風耳將軍](../Page/順風耳.md "wikilink")、[福德正神等](../Page/福德正神.md "wikilink")[儒](../Page/儒.md "wikilink")、[道](../Page/道教.md "wikilink")、[釋三教神佛](../Page/佛教.md "wikilink")。

龍成宮佔地8300[平方公尺](../Page/平方公尺.md "wikilink")（約2500[坪](../Page/坪.md "wikilink")），金碧輝煌，高聳的廟脊，從遠處望去，成為五甲顯著的地標。龍成宮並採用現代建築格局，不論廊柱、神龕、藻井、石木雕工、彩繪等做工與建材，均為一時之選。許多旅行團、進香團並將五甲龍成宮列為參訪廟宇，外地香客不絕，與[高雄市](../Page/高雄市.md "wikilink")[三民區](../Page/三民區.md "wikilink")[三鳳宮齊名](../Page/三鳳宮.md "wikilink")。

龍成宮熱心公益事業，並附設佔地約500坪的圖書館供市民、學子利用，許多學子在參拜完後下樓讀書，並在獲得佳績後返回廟宇祭拜還願，捐[香火錢](../Page/香火錢.md "wikilink")，甚至是辦[酬神戲](../Page/酬神戲.md "wikilink")，感謝神恩。

## 祭典

大型祭典：皆有[酬神戲](../Page/酬神戲.md "wikilink")、作法會等。

  - 正月初一賀歲祭典
  - 正月初四[接神祭典](../Page/接神.md "wikilink")
  - 正月初六[清水祖師佛誕](../Page/清水祖師.md "wikilink")
  - 正月九日[玉皇誕](../Page/玉皇誕.md "wikilink")
  - 正月十五[上元節](../Page/上元節.md "wikilink")、迎[太歲](../Page/太歲.md "wikilink")：舉行乞[紅龜粿儀式](../Page/紅龜粿.md "wikilink")，由信徒[擲筊比賽](../Page/擲筊.md "wikilink")，相傳求得「龜王」，一年內會事事順心。
  - 三月廿三[天后誕](../Page/天后誕.md "wikilink")
  - 七月十五[中元節](../Page/中元節.md "wikilink")
  - 八月初八戴元帥生日
  - 十二月廿四[送神](../Page/送神.md "wikilink")，謝[太歲](../Page/太歲.md "wikilink")

其餘眾神佛生日皆舉辦簡便的誦經祝壽儀式。

## 參見

  - [鳳山區](../Page/鳳山區.md "wikilink")
  - [臺灣民間信仰](../Page/臺灣民間信仰.md "wikilink")

## 參考資料

  - 《**五甲龍成宮**沿革》
  - [五甲龍成宮官方網站](http://www.longcheng.org.tw)
  - [五甲龍成宮信眾粉絲專頁](https://www.facebook.com/%E9%AB%98%E9%9B%84%E4%BA%94%E7%94%B2%E9%BE%8D%E6%88%90%E5%AE%AE-444800102256087/)
  - [走讀臺灣，鳳山市](http://fengshan.itgo.com/8-6.htm)
  - [街頭巷尾－話鳳山老地名](https://market.cloud.edu.tw/content/local/kaushoun/chinnan/5/oldplace/oldplace.htm)
  - [從地名瞭解臺灣歷史
    政治大學臺灣史研究所戴寶村教授](http://www.tces.chc.edu.tw/alan/taiwan/h/h3/h33/h335.html)

[Category:高雄媽祖廟](../Category/高雄媽祖廟.md "wikilink")
[Category:清水祖師廟](../Category/清水祖師廟.md "wikilink")
[Category:高雄市廟宇](../Category/高雄市廟宇.md "wikilink")
[Category:鳳山區](../Category/鳳山區.md "wikilink")
[Category:高雄市旅遊景點](../Category/高雄市旅遊景點.md "wikilink")