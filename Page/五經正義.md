《**五經正義**》，原名《五經義疏》，專指《[周易](../Page/周易.md "wikilink")》、《[尚書](../Page/尚書_\(經\).md "wikilink")》、《[毛詩](../Page/毛詩.md "wikilink")》、《[禮記](../Page/禮記.md "wikilink")》、《[春秋](../Page/春秋_\(史書\).md "wikilink")》五部[儒家](../Page/儒家.md "wikilink")[經書](../Page/十三经.md "wikilink")（通稱[五經](../Page/五經.md "wikilink")）的[注解之](../Page/注解.md "wikilink")[疏義](../Page/疏義.md "wikilink")。由[唐太宗詔令](../Page/唐太宗.md "wikilink")[孔穎達主持](../Page/孔穎達.md "wikilink")，由諸儒共參議，[貞觀十六年撰成](../Page/貞觀_\(唐太宗\).md "wikilink")，續有修正，直到[唐高宗](../Page/唐高宗.md "wikilink")[永徽四年才頒布](../Page/永徽.md "wikilink")。初名「義贊」，詔改稱「正義」。

《五經正義》的編纂被視為官方統一[南學](../Page/南學.md "wikilink")、[北學的手段](../Page/北學.md "wikilink")，使[六朝以來南](../Page/六朝.md "wikilink")、北地區各自發展的經學思想殊途而歸一，是後來各朝代相繼編纂官方經學解釋的濫觴。至高宗時頒行，成為科舉考試的標準課本，然而[朱熹對](../Page/朱熹.md "wikilink")《周易正義》評價不高，認為孔氏負責編撰的《五經義疏》，《春秋》最好，《诗》、《礼记》次之，《书》、《易》为下。清代《[四库全书总目](../Page/四库全书总目.md "wikilink")》认为，孔疏不仅“墨守专门”，唯[王弼注是从](../Page/王弼.md "wikilink")，而且“至于诠释文句，多用空言”，“亦非考证之疏矣”。

## 內容

  - 《周易正義》10卷，根據[魏](../Page/曹魏.md "wikilink")[王弼](../Page/王弼.md "wikilink")、[晉](../Page/晉朝.md "wikilink")[韓康伯二人的注](../Page/韓康伯.md "wikilink")。从内容上来说，第一，综述经传源流，第二，解题释例；第三，疏解经传及注文。
  - 《尚書正義》20卷，根據[西漢](../Page/西漢.md "wikilink")[孔安國](../Page/孔安國.md "wikilink")（實際上為偽託）的傳。内容为，第一，解题释例；第二，注疏校勘；第三，信伪与变伪。
  - 《毛詩正義》70卷，根據[西漢](../Page/西漢.md "wikilink")[毛公的傳](../Page/毛公.md "wikilink")、[東漢](../Page/東漢.md "wikilink")[鄭玄的箋](../Page/鄭玄.md "wikilink")。内容为，第一，解题释例；第二，注释校勘。
  - 《禮記正義》63卷，根據[西漢](../Page/西漢.md "wikilink")[戴聖的](../Page/戴聖.md "wikilink")[小戴禮記](../Page/小戴禮記.md "wikilink")、東漢鄭玄的注。内容只包括解题释例和注释。
  - 《春秋正義》60卷，又稱《春秋左傳正義》，根據[左氏傳](../Page/左氏傳.md "wikilink")、[西晉](../Page/西晉.md "wikilink")[杜預的經傳集解](../Page/杜預.md "wikilink")。内容包括解题释例和注释校勘。

## 編纂過程

太宗時期（貞觀年間）：

孔穎達總監。

  - 周易：[馬嘉運](../Page/馬嘉運.md "wikilink")、[趙乾葉](../Page/趙乾葉.md "wikilink")、[蘇德融](../Page/蘇德融.md "wikilink")、[趙弘智等](../Page/趙弘智.md "wikilink")。
  - 尚書：[王德韶](../Page/王德韶.md "wikilink")、[李子雲](../Page/李子雲.md "wikilink")、[朱長才](../Page/朱長才.md "wikilink")、蘇德融、[隨德素](../Page/隨德素.md "wikilink")、[王士雄](../Page/王士雄.md "wikilink")、趙弘智等。
  - 毛詩：王德韶、[齊威](../Page/齊威.md "wikilink")、趙乾葉、[賈普曜](../Page/賈普曜.md "wikilink")、趙弘智等。
  - 禮記：[朱子奢](../Page/朱子奢.md "wikilink")、[李善信](../Page/李善信.md "wikilink")、[賈公彥](../Page/賈公彥.md "wikilink")、[柳士宣](../Page/柳士宣.md "wikilink")、[范義頵](../Page/范義頵.md "wikilink")、[張權](../Page/張權.md "wikilink")、[周玄達](../Page/周玄達.md "wikilink")、[趙君贊](../Page/趙君贊.md "wikilink")、王士雄、趙弘智等。
  - 春秋：[谷那律](../Page/谷那律.md "wikilink")、[楊士勛](../Page/楊士勛.md "wikilink")、朱長才、馬嘉運、王德韶、蘇德融、隨德素、趙弘智。

高宗時期（永徽二年左右）：

  - [長孫無忌](../Page/長孫無忌.md "wikilink")、[于志寧](../Page/于志寧.md "wikilink")、[高季輔](../Page/高季輔.md "wikilink")、[張行成等中書](../Page/張行成.md "wikilink")、門下與[國子監三館](../Page/國子監.md "wikilink")[學士](../Page/學士.md "wikilink")。

## 外部連結

  - 野間文史著，金培懿譯：〈[《五經正義》之研究](http://www.litphil.sinica.edu.tw/home/publish/PDF/Newsletter/58/%E9%87%8E%E9%96%93%E6%96%87%E5%8F%B21-20.pdf)〉。
  - 長瀨誠著，黃桂譯：〈[關於五經正義單疏本](http://www.litphil.sinica.edu.tw/home/publish/PDF/Newsletter/40/40-1-11.pdf)〉。
  - [周易正義查詢](http://yijing.cdict.info/yijing_gi.php) 易經古籍交互查詢系統

[Wujing](../Category/儒學著作.md "wikilink")
[Category:唐朝典籍](../Category/唐朝典籍.md "wikilink")
[Category:7世紀書籍](../Category/7世紀書籍.md "wikilink")