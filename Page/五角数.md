[Pentnumbers.svg](https://zh.wikipedia.org/wiki/File:Pentnumbers.svg "fig:Pentnumbers.svg")

**五邊形數**是能排成[五邊形的](../Page/五邊形.md "wikilink")[多邊形數](../Page/多邊形數.md "wikilink")。其概念類似[三角形數及](../Page/三角形數.md "wikilink")[平方數](../Page/平方數.md "wikilink")，不過五邊形數和[三角形數及](../Page/三角形數.md "wikilink")[平方數不同](../Page/平方數.md "wikilink")，所對應的形狀沒有[旋轉對稱](../Page/旋轉對稱.md "wikilink")（Rotational
symmetry）的特性。

第\(n\)個五邊形數可用以下公式求得

\[p_n = \frac{3n^2-n}{2}\]

且\(n>0\)。

首幾個五邊形數為[1](../Page/1.md "wikilink"), [5](../Page/5.md "wikilink"),
[12](../Page/12.md "wikilink"), [22](../Page/22.md "wikilink"),
[35](../Page/35.md "wikilink"), [51](../Page/51.md "wikilink"),
[70](../Page/70.md "wikilink"), [92](../Page/92.md "wikilink"),
[117](../Page/117.md "wikilink")...
([OEIS:A000326](../Page/OEIS:A000326.md "wikilink"))，其奇偶排列是「奇奇偶偶」。

第\(n\)個五邊形數是第\(3n-1\)個[三角形數的](../Page/三角形數.md "wikilink")\(\frac{1}{3}\)。首\(n\)個五邊形數的[算術平均數是第](../Page/算術平均數.md "wikilink")\(n\)個三角形數。

## 五邊形數測試

利用以下的公式可以測試一個正整數*x*是否是五邊形數（此處不考慮廣義五邊形數）：

\[n = \frac{\sqrt{24x+1} + 1}{6}.\]

  - 若n是[自然數](../Page/自然數.md "wikilink")，則x是五邊形數，而且恰為第n個五邊形數。
  - 若n不是[自然數](../Page/自然數.md "wikilink")，則x不是五邊形數。

## 用五邊形數的和來表示整數

依照[費馬多邊形數定理](../Page/費馬多邊形數定理.md "wikilink")，任何整數都可以表示為不超過5個五邊形數的和。但大多數的整數都可以表示不超過3個五邊形數的和\[1\]。在小於\(10^6\)的整數中，只有以下6個整數需用5個五邊形數的和來表示：

9, 21, 31, 43, 55, 89
([OEIS:A133929](../Page/OEIS:A133929.md "wikilink"))

而以下210個整數需用4個五邊形數的和來表示：

4, 8, 9, 16, 19, 20, ..., 20250, 33066
([OEIS:A003679](../Page/OEIS:A003679.md "wikilink"))

## 廣義五邊形數

廣義五邊形數的公式和五邊形數相同，只是n可以為負數和零，n 依序為0, 1, -1, 2, -2, 3, -3,
4...，廣義五邊形數也可以用下式表示：

\[p_n = \frac{3n^2 \pm n}{2}\]

n 依序為0, 1, 2, 3, 4...，

其產生的數列如下：

0, 1, 2, 5, 7, 12, 15, 22, 26, 35, 40, 51, 57, 70, 77, 92, 100, 117,
126, 145, 155, 176, 187, 210, 222, 247, 260, 287, 301, 330, 345, 376,
392, 425, 442, 477, 495, 532, 551, 590, 610, 651, 672, 715, 737, 782,
805, 852, 876, 925, 950, 1001, 1027, 1080, 1107, 1162, 1190, 1247, 1276,
1335... ([OEIS:A001318](../Page/OEIS:A001318.md "wikilink"))

在[歐拉的](../Page/歐拉.md "wikilink")[整數分拆理論中](../Page/整數分拆.md "wikilink")，[五邊形數定理說明廣義五邊形數和](../Page/五邊形數定理.md "wikilink")[整數分拆的關係](../Page/整數分拆.md "wikilink")。

用第n個五邊形數（n\>2）排列組成的正五邊形，外圍點的個數有\(5(n-1)\)個，因此在內部的點個數為：

\[\frac{3n^2-n}{2} - 5(n-1) = \frac{3n^2-11n+10}{2} = \frac{(3n-5) (n-2)}{2} = \frac{3(n-2)^2+(n-2)}{2}\]

剛好也是一個廣義五邊形數。

所有的整數都可以表示成不超過3個廣義五邊形數的和\[2\]。

若三角形數可以被3整除，則除以3之後的數必為廣義五邊形數\[3\]。

## 廣義五邊形數和中心六邊形數

廣義五邊形數和[中心六邊形數有密切的關係](../Page/中心六邊形數.md "wikilink")。將中心六邊形數以陣列的方式排出，並且從中間將正六邊形分為二個梯形，較大的梯形可以表示為五邊形數，而較小的梯形可以表示為廣義五邊形數，因此中心六邊形數可以表示為二個廣義五邊形數的和（五邊形數也是廣義五邊形數的一種）：

<table>
<thead>
<tr class="header">
<th><p>1=1+0</p></th>
<th></th>
<th><p>7=5+2</p></th>
<th></th>
<th><p>19=12+7</p></th>
<th></th>
<th><p>37=22+15</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:RedDotX.svg" title="fig:RedDotX.svg">RedDotX.svg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a><a href="https://zh.wikipedia.org/wiki/File:GrayDotX.svg" title="fig:GrayDotX.svg">GrayDotX.svg</a></p></td>
</tr>
</tbody>
</table>

一般來言：

\[3n(n-1)+1 = \tfrac{1}{2}n(3n-1)+\tfrac{1}{2}(1-n)[3(1-n)-1]\]

等式右側為二個廣義五邊形數，且第一項是五邊形數(*n* ≥ 1)。

## 參見

  - [五邊形數定理](../Page/五邊形數定理.md "wikilink")

## 參考資料

  - [Leonard Euler: On the remarkable properties of the pentagonal
    numbers](../Page/:arxiv:math/0505373.md "wikilink")

## 外部連結

  - [Pentagon, Kartenhaus und
    Summenzerlegung](http://matheplanet.com/default3.html?article=277)（德文）

[5](../Category/多邊形數及多面體數.md "wikilink")

1.

2.
3.