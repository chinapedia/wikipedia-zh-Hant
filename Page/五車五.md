**五車五**（**β Tau** /
**金牛座β**）為[金牛座第二亮星](../Page/金牛座.md "wikilink")，視星等為1.7等。因為位於金牛座與[御夫座的邊界](../Page/御夫座.md "wikilink")，所以在[拜耳命名法中曾重複命名為為](../Page/拜耳命名法.md "wikilink")**御夫座γ**，但是現在已經不再使用。

## 物理特性

五車五的[絕對星等為](../Page/絕對星等.md "wikilink")-1.34，這與金牛座的[疏散星團](../Page/疏散星團.md "wikilink")[昴星團裏的另一顆恒星](../Page/昴星團.md "wikilink")[昴宿四接近](../Page/昴宿四.md "wikilink")。五車五是一顆[B型的巨星](../Page/恒星分類.md "wikilink")，光度為太陽的700倍。\[1\]
然而由於距地球只有約130光年，而昴宿四距地球約360光年，所以五車五在金牛座裏亮度排在第二，僅次於[畢宿五](../Page/畢宿五.md "wikilink")。

由於它距[銀道平面以西僅幾度](../Page/銀道.md "wikilink")，五車五所在天區的[星雲和](../Page/星雲.md "wikilink")[星團很密集](../Page/星團.md "wikilink")。\[2\]
與[太陽相比較](../Page/太陽.md "wikilink")，五車五含有相當豐富的[錳](../Page/錳.md "wikilink")，不過缺乏[鈣與](../Page/鈣.md "wikilink")[鎂](../Page/鎂.md "wikilink")。\[3\]\[4\]五車五已經開始離開[主序帶](../Page/主序帶.md "wikilink")，將會變成一顆[巨星](../Page/巨星.md "wikilink")。

當月球的升交點接近[晝夜平分點時](../Page/晝夜平分點.md "wikilink")，月球將會遮掩五車五，而五車五在2007年時曾經被[月球所掩](../Page/月球.md "wikilink")。大部分的掩星只有在[南半球可以觀測到](../Page/南半球.md "wikilink")，這是因為五車五位於月掩區的北端，只有很罕見的情況下可以在[加利福尼亞州南部觀測得到](../Page/加利福尼亞州.md "wikilink")。\[5\]

## 伴星

五車五有一顆暗淡的伴星BD+28
795B，[PA為](../Page/位置角.md "wikilink")239°，距離主星33.4[弧分](../Page/弧分.md "wikilink")。\[6\]\[7\]

## 參考資料

## 外部連結

  - [Elnath](https://web.archive.org/web/20051123222352/http://www.astro.uiuc.edu/~kaler/sow/elnath.html)
  - Jim Kaler's
    Stars:[Elnath](https://web.archive.org/web/20051123222352/http://www.astro.uiuc.edu/~kaler/sow/elnath.html)
  - [Image of Elnath](http://antwrp.gsfc.nasa.gov/apod/ap100305.html)
    from [APOD](../Page/Astronomy_Picture_of_the_Day.md "wikilink")

[金牛座, β](../Category/拜耳天體.md "wikilink")
[Category:巨星](../Category/巨星.md "wikilink")
[Category:金牛座](../Category/金牛座.md "wikilink")
[五车5](../Category/五车_\(星官\).md "wikilink")
[035497](../Category/HD和HDE天體.md "wikilink")
[025428](../Category/HIP天體.md "wikilink") [金牛座,
112](../Category/佛氏天體.md "wikilink")

1.
2.

3.
4.
5.  [Abrams Planetarium - Skywatcher's
    Diary](http://www.pa.msu.edu/abrams/SWD/2006/SWD_0609-g.html)

6.

7.