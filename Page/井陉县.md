**井县**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[河北省](../Page/河北省.md "wikilink")[石家庄市所辖的一个](../Page/石家庄市.md "wikilink")[县](../Page/县_\(中华人民共和国\).md "wikilink")。

## 井陉历史文化

井陉境内很早就已有人类聚居。

井陉战国时属于[中山国](../Page/中山国.md "wikilink")，春秋时属于[鲜虞国](../Page/鲜虞国.md "wikilink")，鲜虞国和中山国则为同一民族——[鲜虞民族所建](../Page/鲜虞.md "wikilink")。后与当地殷商遗民逐渐融合，形成其独特的地域文化。

井陉人则为[鲜虞中山人之后裔](../Page/鲜虞中山人.md "wikilink")，其风俗以“善歌舞，热爱艺术”著称。中山之地多出倡优，由于地薄人众，歌舞卖艺古时是当地的一大特殊民俗，男女无别，个个盛装打扮，俊男美女充斥各诸侯后宫，鲜虞中山音律以慷慨悲歌为一大艺术特点，燕赵自古多慷慨悲歌之士，其中对于悲壮苍凉的审美追求固此是出于当时的鲜虞中山之地罢，当今据考证井陉当地有一种民间歌舞——“[拉花](../Page/拉花.md "wikilink")”，既“拉荒”的谐音，便是出于鲜虞中山歌舞曲调的一种继承和延伸，其音乐深沉古朴，悲怆苍凉，正是中山国慷慨悲歌的体现。

中山男子擅长制作精美的物品，出土的文物中不难看出其精致程度令世人惊叹。

当下井陉素有民间艺术之乡的称号大概是由此而来。

## 行政区划

下辖10个[镇](../Page/行政建制镇.md "wikilink")、7个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 旅游

[Daliangjiang.JPG](https://zh.wikipedia.org/wiki/File:Daliangjiang.JPG "fig:Daliangjiang.JPG")大梁江村古建筑\]\]
井陉山脉绵延，河川纵横，奇山异景，令人赞叹。

  - [苍岩山](../Page/苍岩山.md "wikilink")
  - [仙台山](../Page/仙台山.md "wikilink")
  - [锦山](../Page/锦山.md "wikilink")
  - [秦皇古驿道](../Page/秦皇古驿道.md "wikilink")

## 外部链接

  - [井陉县简介](https://web.archive.org/web/20060218185120/http://www.hebiic.gov.cn/173county/sjz/sjzjxx/index.asp)

[井陉县](../Category/井陉县.md "wikilink")
[县](../Category/石家庄县级行政区.md "wikilink")
[石家庄](../Category/河北省县份.md "wikilink")