**帕纳约提斯·亚历山大·“亚历克赛”·拉拉斯**（**Panayotis Alexander (Alexi)
Lalas**，），[美国职业](../Page/美国.md "wikilink")[足球运动员](../Page/足球.md "wikilink")，司职中后卫。他的山羊鬍子給球迷留下了探刻印像。

拉拉斯出生于[密歇根州](../Page/密歇根州.md "wikilink")[伯明翰的一个](../Page/伯明翰_\(密歇根州\).md "wikilink")[希腊移民家庭](../Page/希腊.md "wikilink")。他於1990年开始代表[美国国家足球队出赛](../Page/美国国家足球队.md "wikilink")。1994年，凭借着在[美国世界杯上的良好表现](../Page/美国世界杯.md "wikilink")，他加盟了[意甲的](../Page/意甲.md "wikilink")[帕多瓦足球俱乐部](../Page/帕多瓦足球俱乐部.md "wikilink")，成为第一个登陆意甲的美国职业球员，当赛季帮助球队保级成功，并且还攻破了AC米兰队的球门（帕多瓦队2-0取胜）。1995年，他获得[美国足球先生称号](../Page/美国足球先生.md "wikilink")。

1996年，随着帕多瓦队的降级，拉拉斯回到[美国职业足球大联盟](../Page/美国职业足球大联盟.md "wikilink")，并于1998年[法国世界杯后结束国家队生涯](../Page/法国世界杯.md "wikilink")。他一共代表国家队出场96次，进9球。2003年，拉拉斯在[洛杉矶银河足球俱乐部退役](../Page/洛杉矶银河足球俱乐部.md "wikilink")。

退役后，他一度出任[圣何塞地震足球俱乐部总经理](../Page/圣何塞地震足球俱乐部.md "wikilink")。2005年，他出任[安舒茨娱乐集团旗下的](../Page/安舒茨娱乐集团.md "wikilink")[纽约大都会明星足球俱乐部主席兼总经理](../Page/纽约大都会明星足球俱乐部.md "wikilink")。2006年4月，他转而出任也是在安舒茨娱乐集团旗下的洛杉矶银河队主席兼总经理。

2007年，拉拉斯以高价签下[大卫·贝克汉姆](../Page/大卫·贝克汉姆.md "wikilink")，引起全球轰动。

## 参考资料

  - [个人简历](http://www.ussoccerplayers.com/players/alexi_lalas/)
  - [拉拉斯的MySpace](http://www.myspace.com/alexilalas)

[Category:美国足球运动员](../Category/美国足球运动员.md "wikilink")
[Category:帕多瓦球員](../Category/帕多瓦球員.md "wikilink")
[Category:新英格蘭革命球員](../Category/新英格蘭革命球員.md "wikilink")
[Category:紐約紅牛球員](../Category/紐約紅牛球員.md "wikilink")
[Category:堪薩斯城巫師球員](../Category/堪薩斯城巫師球員.md "wikilink")
[Category:洛杉磯銀河球員](../Category/洛杉磯銀河球員.md "wikilink")
[Category:美職球員](../Category/美職球員.md "wikilink")
[Category:1992年夏季奧林匹克運動會足球運動員](../Category/1992年夏季奧林匹克運動會足球運動員.md "wikilink")
[Category:1993年美洲盃球員](../Category/1993年美洲盃球員.md "wikilink")
[Category:1994年世界盃足球賽球員](../Category/1994年世界盃足球賽球員.md "wikilink")
[Category:1995年美洲盃球員](../Category/1995年美洲盃球員.md "wikilink")
[Category:1996年夏季奥林匹克運動會足球運動員](../Category/1996年夏季奥林匹克運動會足球運動員.md "wikilink")
[Category:1998年世界盃足球賽球員](../Category/1998年世界盃足球賽球員.md "wikilink")
[Category:義大利外籍足球運動員](../Category/義大利外籍足球運動員.md "wikilink")