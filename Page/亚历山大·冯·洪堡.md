[Humboldt,_Alexander_von_1847.jpg](https://zh.wikipedia.org/wiki/File:Humboldt,_Alexander_von_1847.jpg "fig:Humboldt,_Alexander_von_1847.jpg")
**弗里德里希·威廉·海因里希·亚历山大·冯·洪堡**（德语：**Friedrich Wilhelm Heinrich Alexander
von
Humboldt**，），德国[自然科学家](../Page/自然科学.md "wikilink")、[自然地理学家](../Page/自然地理学.md "wikilink")，近代[气候学](../Page/气候学.md "wikilink")、[植物地理学](../Page/植物地理学.md "wikilink")、[地球物理学的创始人之一](../Page/地球物理学.md "wikilink")；涉猎科目很广，特别是[生物学与](../Page/生物学.md "wikilink")[地质学](../Page/地质学.md "wikilink")。教育家、[柏林大學創始人](../Page/柏林大學.md "wikilink")[威廉·馮·洪堡是其兄](../Page/威廉·馮·洪堡.md "wikilink")。

他被誉为现代地理学的金字塔和现代地理学之父。还是[英国皇家学会外籍会员](../Page/英国皇家学会.md "wikilink")。

## 生平

亚历山大·冯·洪堡出生於[普魯士首都](../Page/普魯士.md "wikilink")[柏林](../Page/柏林.md "wikilink")。年輕時曾於[耶拿](../Page/耶拿.md "wikilink")、[哥廷根和](../Page/哥廷根.md "wikilink")[法蘭克福](../Page/奧得河畔法蘭克福.md "wikilink")，1799年至1804年間曾前往[南美洲旅行探險](../Page/南美洲.md "wikilink")，就當地自然環境包括火山海洋植物礦產氣候水文等各方面都進行科學研究與分析，並在當地發現許多新物種；後曾於[美国和](../Page/美国.md "wikilink")[中亚进行科学考察](../Page/中亚.md "wikilink")，著有《中部亚洲》。

其考察成果和据此所作的概括对科学理论方面作出了重要贡献。并有30卷《新大陆热带地区旅行记》刊行于世。

[AvHumboldts_Americatravel_map_zh.svg](https://zh.wikipedia.org/wiki/File:AvHumboldts_Americatravel_map_zh.svg "fig:AvHumboldts_Americatravel_map_zh.svg")
亚历山大·冯·洪堡享有諸多美誉，如「[哥伦布第二](../Page/哥伦布.md "wikilink")」、「科学王子」和「新[亚里士多德](../Page/亚里士多德.md "wikilink")」（法国发行的巴黎科学院纪念币上语句）。他也是众多学会的成员，曾在利奥波德尼斯赫-卡罗琳森（）自然科学家学会、普鲁士科学院、巴伐利亚科学院任职。

亚历山大·冯·洪堡常年规律的在巴黎生活，并是法国巴黎地理学学会的主席（président de la Société de géographie
de
Paris）。巴黎是当年的世界科学之都，亚历山大·冯·洪堡入选了法兰西学院，得到了当时同时代名人的认可。同时期当选成为法兰西学院的外籍院士。

晚年所写的5卷《[宇宙](../Page/宇宙_\(洪堡\).md "wikilink")》（，原著用法文写成）是他描述地球自然地理的著作。

其对地理学和生物学有巨大贡献。如他认为自然界为一巨大的整体，各种自然现象相互联系，并依其内部力量不断运动发展；亦常从直接观察的事实出发，运用比较法，揭示自然现象之间的因果关系。同时，他开创了许多地理学界的重要概念，如等温线、等压线、地形剖面图、海拔温度梯度、洋流、植被的水平与垂直分布、气候带分布、温度垂直递减率、大陆东西岸温度差异、大陆性与海洋气候、地形对气候形成的作用等，并探讨气候同动植物的水平分异和垂直分异的关系；此外，他也发现地磁强度从极地向赤道递减的规律，火山分布与地下裂隙的关系等。

## 纪念

### 以洪堡命名的物种

  - *Spheniscus humboldti*洪堡企鹅（[漢波德企鵝](../Page/漢波德企鵝.md "wikilink")）
  - *Lilium humboldtii*洪堡百合
  - *Phragmipedium humboldtii*洪堡兰
  - *Quercus humboldtii*南美柞树
  - *Conepatus humboldtii*洪堡猪鼻[臭鼬](../Page/臭鼬.md "wikilink")
  - *Annona humboldtii*
    [新热带](../Page/新热带.md "wikilink")[树或](../Page/树.md "wikilink")[灌木](../Page/灌木.md "wikilink")
  - *Annona humboldtiana*新热带树或灌木
  - *Utricularia Humboldtii*一种陸生狸藻（食虫植物）
  - *Geranium humboldtii*洪堡天竺葵

### 以洪堡命名的地理名词

  - [洪堡寒流](../Page/秘魯寒流.md "wikilink")（太平洋）
  - [洪堡河及接收河水的](../Page/洪堡河.md "wikilink")[洪堡湖](../Page/洪堡湖.md "wikilink")、[洪堡低地](../Page/洪堡低地.md "wikilink")：位於美国[內華達州](../Page/內華達州.md "wikilink")
  - [洪堡海](../Page/洪堡海.md "wikilink")（[拉丁语](../Page/拉丁语.md "wikilink")：Mare
    Humboldtianum，洪堡之海)：[月球上的](../Page/月球.md "wikilink")[月海之一](../Page/月海.md "wikilink")

### 以洪堡命名的團體

  - [洪堡基金会](../Page/洪堡基金会.md "wikilink")

## 同时代的人的评价

  - [威廉·冯·洪堡](../Page/威廉·冯·洪堡.md "wikilink")："*亚历山大注定要结合思想和实践，否则就会一直保持对未知的求知欲一直到老年。他的深入，他敏锐的头脑和他的令人难以置信的速度，是一种罕见的组合。"
    –"*Alexander is destined to combine ideas and follow chains of
    thoughts which would otherwise have remained unknown for ages. His
    depth, his sharp mind and his incredible speed are a rare
    combination."
  - [查尔斯·达尔文](../Page/查尔斯·达尔文.md "wikilink")："*他是有史以來最偉大的旅行科學家.*" – "*I
    have always admired him; now I worship him.*" \[1\]
  - [约翰·沃尔夫冈·歌德](../Page/约翰·沃尔夫冈·歌德.md "wikilink")："*洪堡給了我們真正的寶藏*"
  - [弗里德里希·席勒](../Page/弗里德里希·席勒.md "wikilink")："*亞歷山大讓人印象深刻，因為比起他的兄弟，他現出了！*"

  - [西蒙·玻利瓦尔](../Page/西蒙·玻利瓦尔.md "wikilink")："*亚历山大·冯·洪堡做到的比任何一個征服者都還要多，他是一個真正的美洲探險者*"

  - [José de la Luz y
    Caballero](../Page/José_de_la_Luz_y_Caballero.md "wikilink"):
    "*哥倫布給了歐洲一個新世界。而洪堡提供了關於它的物理，材料，智力和道德方面的知識*"
  - [克劳德·贝托莱](../Page/克劳德·贝托莱.md "wikilink")："*這個人的學識好比一間學院.*"
  - [托马斯·杰斐逊](../Page/托马斯·杰斐逊.md "wikilink")："*我認為他是我所遇過最重要的科學家。*"
  - [伯伊斯－雷蒙德](../Page/伯伊斯－雷蒙德.md "wikilink")："*每一個科學家都是洪堡的後人。我們都是他的家人。.*"

  - [羅伯特·印格索](../Page/羅伯特·印格索.md "wikilink")："*他之於科學有如[莎士比亞之於歌劇](../Page/莎士比亞.md "wikilink")。*"


## 相關小說

  - 《Die Vermessung der Welt》－Daniel Kehlmann
      - 《测量世界》丹尼尔·克尔曼，上海[三联书店](../Page/三联书店.md "wikilink")，2006年
      - 《丈量世界》作者：[丹尼爾．凱曼](../Page/丹尼爾．凱曼.md "wikilink")，譯者：闕旭玲，商周出版
      - 出版日期：2006年12月28日，ISBN 9789861247953
      - 描寫兩位德國人-地理學家亞歷山大·馮·洪堡與數學家[高斯在](../Page/高斯.md "wikilink")18與19世紀之交，各自以自己的方法去測量這個世界。
      - 《博物學家的自然創世紀：亞歷山大．馮．洪堡德用旅行與科學丈量世界，重新定義自然》 作者： Andrea Wulf 譯者：陳義仁
        果力文化 出版日期：2016/09/05

## 注释

## 外部連結

  - [柏林洪堡大学](http://www.hu-berlin.de)
  - [亚历山大·冯·洪堡基金会](http://www.avh.de/en/index.htm)

[Category:科普利獎章獲得者](../Category/科普利獎章獲得者.md "wikilink")
[Category:英國皇家學會院士](../Category/英國皇家學會院士.md "wikilink")
[Category:瑞典皇家科學院院士](../Category/瑞典皇家科學院院士.md "wikilink")
[Category:普魯士科學院院士](../Category/普魯士科學院院士.md "wikilink")
[Category:绅士科学家](../Category/绅士科学家.md "wikilink")
[Category:德国探险家](../Category/德国探险家.md "wikilink")
[Category:南美洲探險家](../Category/南美洲探險家.md "wikilink")
[Category:德國旅行家](../Category/德國旅行家.md "wikilink")
[Category:德国地理学家](../Category/德国地理学家.md "wikilink")
[Category:德國地質學家](../Category/德國地質學家.md "wikilink")
[Category:德國氣象學家](../Category/德國氣象學家.md "wikilink")
[Category:德國動物學家](../Category/德國動物學家.md "wikilink")
[Category:法蘭克福（歐得河）大學校友](../Category/法蘭克福（歐得河）大學校友.md "wikilink")
[Category:哥廷根大學校友](../Category/哥廷根大學校友.md "wikilink")
[Category:耶拿大學校友](../Category/耶拿大學校友.md "wikilink")
[Category:德意志貴族](../Category/德意志貴族.md "wikilink")
[Category:布蘭登堡人](../Category/布蘭登堡人.md "wikilink")
[Category:柏林人](../Category/柏林人.md "wikilink")
[Category:丹麦皇家科学院院士](../Category/丹麦皇家科学院院士.md "wikilink")
[Category:美國哲學會會士](../Category/美國哲學會會士.md "wikilink")
[\*](../Category/亞歷山大·洪堡.md "wikilink")

1.  穆爾黑德，達爾文與小獵犬號：物種原始的發現之旅，p.62.