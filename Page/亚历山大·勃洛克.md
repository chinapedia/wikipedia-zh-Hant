**亚历山大·亚历山德罗维奇·布洛克**（[俄语](../Page/俄语.md "wikilink")：****，1880年11月28日[圣彼得堡](../Page/圣彼得堡.md "wikilink")
-
1921年8月7日），[二十世纪早期](../Page/二十世纪.md "wikilink")[俄罗斯诗人](../Page/俄罗斯.md "wikilink")、戏曲家，代表作《[十二个](../Page/十二个.md "wikilink")（the
twelve）》。

## 外部链接

  - [Biografía y poemas de Alexander
    Blok](http://amediavoz.com/blok.htm)
  - [English translation of Blok Poem at The New
    Formalist](http://www.thenewformalist.com/index13.html#block)
  - [Alice Koonen reading Blok's
    poem](http://max.mmlc.northwestern.edu/~mdenner/Demo/texts/restaurant.html)
  - [Leon Trotsky's article on Alexander
    Blok](http://www.marxists.org/archive/trotsky/1924/lit_revo/ch03.htm)
  - [In Defense of A.
    Blok](http://www.berdyaev.com/berdiaev/berd_lib/1931_358.html)

[B](../Category/1880年出生.md "wikilink")
[B](../Category/1921年逝世.md "wikilink")
[B](../Category/俄罗斯诗人.md "wikilink")
[Category:象征主义诗人](../Category/象征主义诗人.md "wikilink")