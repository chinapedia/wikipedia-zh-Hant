**亚历山大里亚奉献**（[英语](../Page/英语.md "wikilink")：****，[法语](../Page/法语.md "wikilink")：****）是一个[历史](../Page/历史.md "wikilink")[名词](../Page/名词.md "wikilink")，它描述的是公元前34年[罗马共和国](../Page/罗马共和国.md "wikilink")[后三头之一的](../Page/后三头.md "wikilink")[马克·安东尼在](../Page/马克·安东尼.md "wikilink")[亚历山大里亚](../Page/亚历山大里亚.md "wikilink")（今[埃及](../Page/埃及.md "wikilink")[亚历山大港](../Page/亚历山大港.md "wikilink")）宣布把共和国东方的大片[领土赠送给埃及](../Page/领土.md "wikilink")[女王](../Page/女王.md "wikilink")[克利奥帕特拉七世和她的子女这一历史事件](../Page/克利奥帕特拉七世.md "wikilink")。

## 背景

### 后三头联盟瓦解

### 埃及艳后的野心

### 征亚美尼亚归来

## 经过

现在对于“亚历山大里亚奉献”经过的了解主要来源于[古罗马](../Page/古罗马.md "wikilink")[史学家](../Page/史学家.md "wikilink")[普鲁塔克所著的](../Page/普鲁塔克.md "wikilink")《安东尼传》。

### 仪式

安东尼擅自在亚历山大里亚举行凯旋仪式几天后，又在亚历山大里亚的一个露天大型[体育场举行了一场准备得更为精心](../Page/体育场.md "wikilink")、规模更为宏大的典礼。在体育场中央有一个银色的[舞台](../Page/舞台.md "wikilink")，上面放置着两个巨大的黄金宝座，大宝座前还有4个较小的、规格稍低的黄金宝座。当典礼开始后，妆扮得像[埃及神话中的最高女神](../Page/埃及神话.md "wikilink")[伊西斯的埃及艳后](../Page/伊西斯.md "wikilink")[克利奥帕特拉七世女王登上其中一个大宝座](../Page/克利奥帕特拉七世.md "wikilink")，而穿着类似[罗马神话中的酒神](../Page/罗马神话.md "wikilink")[巴克科斯](../Page/巴克科斯.md "wikilink")\[1\]的[马克·安东尼则也公然像](../Page/马克·安东尼.md "wikilink")[国王一样登上另外一个大宝座](../Page/国王.md "wikilink")，埃及艳后的4个子女，包括与[恺撒所生的长子和与安东尼所生的](../Page/恺撒.md "wikilink")3个子女则坐在4个小宝座上，亚历山大里亚的很多居民都赶来围观。

### 领土分配

仪式的主要内容就是为埃及艳后及其子女分配[领土并授予新的](../Page/领土.md "wikilink")[称号](../Page/称号.md "wikilink")，这一切都是安东尼宣布的。

1.  埃及艳后[克利奥帕特拉七世被尊为](../Page/克利奥帕特拉七世.md "wikilink")“[众王之女王](../Page/众王之女王.md "wikilink")”，而她与[恺撒所生的](../Page/恺撒.md "wikilink")[恺撒里昂](../Page/恺撒里昂.md "wikilink")（即[托勒密十五世](../Page/托勒密十五世.md "wikilink")）则被尊为“[众王之王](../Page/众王之王.md "wikilink")”，两人[共治](../Page/共治.md "wikilink")[埃及](../Page/埃及.md "wikilink")、[塞浦路斯](../Page/塞浦路斯.md "wikilink")、[阿非利加和](../Page/阿非利加.md "wikilink")[叙利亚](../Page/叙利亚.md "wikilink")，即是这些领地的女王和国王，不过称号更为尊贵。
2.  埃及艳后与安东尼所生的大儿子[亚历山大·赫利俄斯的穿戴像](../Page/亚历山大·赫利俄斯.md "wikilink")[波斯帝国](../Page/波斯帝国.md "wikilink")[阿契美尼德王朝的君主](../Page/阿契美尼德王朝.md "wikilink")，他被宣布为[亚美尼亚](../Page/亚美尼亚.md "wikilink")、[米底亚和](../Page/米底亚.md "wikilink")[帕提亚的](../Page/帕提亚.md "wikilink")“伟大的国王”，这些领地除了帕提亚之外都是昔日[塞琉西帝国的领土](../Page/塞琉西帝国.md "wikilink")。
3.  埃及艳后与安东尼所生的唯一的女儿[克利奥帕特拉·塞勒涅二世](../Page/克利奥帕特拉·塞勒涅二世.md "wikilink")（也称[克利奥帕特拉八世](../Page/克利奥帕特拉八世.md "wikilink")）被宣布为[昔兰尼加和](../Page/昔兰尼加.md "wikilink")[克利特岛](../Page/克利特岛.md "wikilink")\[2\]的女王。
4.  埃及艳后与安东尼所生的小儿子[托勒密·费拉德尔甫斯身穿](../Page/托勒密·费拉德尔甫斯.md "wikilink")[马其顿王族的礼服](../Page/马其顿王国.md "wikilink")，被宣布为[叙利亚和](../Page/叙利亚.md "wikilink")[小亚细亚](../Page/小亚细亚.md "wikilink")（[奇里乞亚](../Page/奇里乞亚.md "wikilink")）\[3\]的国王。

### 其他

## 结果

## 參考文獻

[Category:前34年](../Category/前34年.md "wikilink")
[Category:埃及历史](../Category/埃及历史.md "wikilink")
[Category:托勒密王朝](../Category/托勒密王朝.md "wikilink")

1.  相当于[希腊神话中的](../Page/希腊神话.md "wikilink")[狄俄尼索斯](../Page/狄俄尼索斯.md "wikilink")。
2.  一说[利比亚](../Page/利比亚.md "wikilink")。
3.  一说还有[腓尼基](../Page/腓尼基.md "wikilink")。