**-{zh-hans:亚历山德里亚;zh-hk:亞歷山大;zh-tw:亞歷山卓;}-**（[英语](../Page/英语.md "wikilink")：Alexandria），又譯為**-{zh-hans:亚历山卓;zh-hk:亞歷山卓;zh-tw:亞歷山大;}-**，是[美國](../Page/美國.md "wikilink")[維吉尼亞州的独立市](../Page/維吉尼亞州.md "wikilink")，位于[华盛顿哥伦比亚特区以南约](../Page/华盛顿哥伦比亚特区.md "wikilink")6英里[波托马克河畔](../Page/波托马克河.md "wikilink")。由于非常接近美国首都，因此亚历山卓的居民大多就职于联邦政府部门、军队或为这些部门提供服务的私人公司，其中最大的雇主是[美国国防部](../Page/美国国防部.md "wikilink")。

## 友好城市

  - [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg")[法国](../Page/法国.md "wikilink")[卡昂](../Page/卡昂.md "wikilink")

## 参见

  - [罗纳德·里根华盛顿国家机场](../Page/罗纳德·里根华盛顿国家机场.md "wikilink")
  - [华盛頓杜勒斯国际机场](../Page/华盛頓杜勒斯国际机场.md "wikilink")
  - [巴尔的摩/华盛顿瑟古德·马歇尔国际机场](../Page/巴尔的摩/华盛顿瑟古德·马歇尔国际机场.md "wikilink")

## 外部链接

  - [亚历山卓官方网站](https://web.archive.org/web/20130801112657/http://alexandria.gov/)


[A](../Category/弗吉尼亚州城市.md "wikilink")