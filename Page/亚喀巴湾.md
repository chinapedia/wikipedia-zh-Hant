**阿卡巴湾（Gulf of Aqaba）**（；Khalyj
al-'Aqabah），也称**埃拉特湾**（）\[1\]，是[红海的一个大](../Page/红海.md "wikilink")[海湾](../Page/海湾.md "wikilink")，位于[紅海北端的](../Page/紅海.md "wikilink")[西奈半岛以东](../Page/西奈半岛.md "wikilink")，[阿拉伯大陆以西](../Page/阿拉伯大陆.md "wikilink")。其海岸線分屬四個國家：[埃及](../Page/埃及.md "wikilink")、[以色列](../Page/以色列.md "wikilink")、[约旦和](../Page/约旦.md "wikilink")[沙地阿拉伯](../Page/沙地阿拉伯.md "wikilink")。

[西奈半岛在红海北部形成了两个海湾](../Page/西奈半岛.md "wikilink")，阿卡巴湾在其东方，而[苏伊士湾则在其西方](../Page/苏伊士湾.md "wikilink")。阿卡巴湾最宽处宽24[公里](../Page/公里.md "wikilink")，从[蒂朗海峡向北延绵](../Page/蒂朗海峡.md "wikilink")160公里至以色列与埃及和约旦的边界，这里有三个重要城市：埃及的[塔巴](../Page/塔巴.md "wikilink")、以色列的[埃拉特和约旦的](../Page/埃拉特.md "wikilink")[亚喀巴](../Page/亚喀巴.md "wikilink")，都是战略上重要的经济港口和驰名的渡假胜地。1950年代以阿衝突之際，沙烏地阿拉伯將阿卡巴灣的蒂朗（Tiran）和塞納菲爾（Sanafir）2座無人島托付埃及保護，2016年計畫將這2座島歸還給沙國\[2\]。

阿卡巴湾也是[东非大裂谷的一部分](../Page/东非大裂谷.md "wikilink")，最深處位於海灣的中央，有1850米深，比更寬闊的苏伊士湾還要深100米。

## 历史

早在[埃及第四王朝的文献](../Page/埃及第四王朝.md "wikilink")，便记载有从底比斯的[以琳到红海湾的海角](../Page/以琳.md "wikilink")[埃拉特之间的贸易路线](../Page/埃拉特.md "wikilink")。[第五王朝](../Page/埃及第五王朝.md "wikilink")、[第六王朝](../Page/埃及第六王朝.md "wikilink")、[第八王朝](../Page/埃及第八王朝.md "wikilink")、[第十二王朝以及](../Page/埃及第十二王朝.md "wikilink")[第十八王朝都有派人横穿](../Page/埃及第十八王朝.md "wikilink")[红海到](../Page/红海.md "wikilink")[朋特之地探索的记录](../Page/朋特之地.md "wikilink")，其中第十八王朝的[哈特谢普苏特建造了一支舰队](../Page/哈特谢普苏特.md "wikilink")，以经六个月的行程到朋特之地进行贸易。底比斯用征服得来的[努比亚黄金和奴隶前往南方的](../Page/努比亚.md "wikilink")[库施进行贸易](../Page/库施.md "wikilink")，换来[乳香](../Page/乳香.md "wikilink")、[没药](../Page/没药.md "wikilink")、[沥青](../Page/沥青.md "wikilink")、[泡碱](../Page/泡碱.md "wikilink")、杜松油、亚麻及铜制饰品等，用来在[卡纳克制作木乃伊](../Page/卡纳克神庙.md "wikilink")。埃及人在红海湾海角的居民点可追溯至第十八王朝时期。

位于海湾最北端的古城艾依拉（今[亞喀巴](../Page/亞喀巴.md "wikilink")）是与[納巴泰人贸易的节点](../Page/納巴泰人.md "wikilink")。罗马人修筑了[新图拉真大道](../Page/新图拉真大道.md "wikilink")（），和[国王大道在亚喀巴连接](../Page/国王大道（黎凡特）.md "wikilink")，连接了亚、非及黎凡特与红海之间的贸易。

亚喀巴亦曾是[奧斯曼土耳其帝國的主要港口](../Page/奧斯曼土耳其帝國.md "wikilink")，经由[汉志铁路连接](../Page/汉志铁路.md "wikilink")[大马士革及](../Page/大马士革.md "wikilink")[麦地那](../Page/麦地那.md "wikilink")。在[第一次世界大战期间](../Page/第一次世界大战.md "wikilink")，决定性地结束了奧斯曼对[大叙利亚地区长达](../Page/大叙利亚.md "wikilink")500年的统治。

## 參考資料

## 參看

  - [南西奈省](../Page/南西奈省.md "wikilink")
  - [亚喀巴省](../Page/亚喀巴省.md "wikilink")
  - [焦夫省](../Page/焦夫省_\(沙地阿拉伯\).md "wikilink")、[塔布克省](../Page/塔布克省.md "wikilink")

## 外部链接

  - [以色列外交部的网页——红海海洋和平公园](https://web.archive.org/web/20061115192216/http://www.mfa.gov.il/MFA/Peace%20Process/Regional%20Projects/Gulf%20of%20Aqaba-%20Environment)

[Category:东非大裂谷](../Category/东非大裂谷.md "wikilink")
[Category:以色列-約旦邊界](../Category/以色列-約旦邊界.md "wikilink")
[Category:埃及-以色列邊界](../Category/埃及-以色列邊界.md "wikilink")
[Category:埃及-沙烏地阿拉伯邊界](../Category/埃及-沙烏地阿拉伯邊界.md "wikilink")
[Category:約旦-沙烏地阿拉伯邊界](../Category/約旦-沙烏地阿拉伯邊界.md "wikilink")
[Category:印度洋海灣](../Category/印度洋海灣.md "wikilink")
[Category:红海](../Category/红海.md "wikilink")
[Category:埃及海灣](../Category/埃及海灣.md "wikilink")
[Category:約旦海灣](../Category/約旦海灣.md "wikilink")
[Category:沙烏地阿拉伯海灣](../Category/沙烏地阿拉伯海灣.md "wikilink")

1.   以色列環境保護部.
    （[原始内容](https://web.archive.org/web/20070812011652/http://sviva.gov.il/bin/en.jsp?enPage=e_BlankPage&enDisplay=view&enDispWhat=Zone&enDispWho=Protecting_the_Gulf&enZone=Protecting_the_Gulf)存档于2007-08-12）.
2.  [埃及國會委員會通過
    紅海戰略無人島嶼送沙國](http://news.ltn.com.tw/news/world/breakingnews/2100521)