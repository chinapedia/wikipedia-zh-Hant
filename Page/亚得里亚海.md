[thumb](../Page/文件:Yadeliyahai-ditu-zh.png.md "wikilink")
**亚得里亚海**（，，，），是[地中海的一部份水域](../Page/地中海.md "wikilink")，分隔了[義大利半島](../Page/義大利半島.md "wikilink")（[亞平寧半島](../Page/亞平寧半島.md "wikilink")）和[巴爾幹半島](../Page/巴爾幹半島.md "wikilink")，也分隔了[亞平寧山脈與](../Page/亞平寧山脈.md "wikilink")[狄那里克阿尔卑斯山脉及其臨近地區](../Page/狄那里克阿尔卑斯山脉.md "wikilink")。亚得里亚海西岸屬於[意大利](../Page/意大利.md "wikilink")，東岸則分別屬於[斯洛文尼亚](../Page/斯洛文尼亚.md "wikilink")、[克罗地亚](../Page/克罗地亚.md "wikilink")、[波斯尼亚和黑塞哥维那](../Page/波斯尼亚和黑塞哥维那.md "wikilink")、[蒙特內哥羅和](../Page/蒙特內哥羅.md "wikilink")[阿尔巴尼亚](../Page/阿尔巴尼亚.md "wikilink")。亚得里亚海透过位於其南部的[奥特朗托海峡与](../Page/奥特朗托海峡.md "wikilink")[愛奥尼亚海相连](../Page/愛奥尼亚海.md "wikilink")。[波河](../Page/波河.md "wikilink")、[阿迪傑河](../Page/阿迪傑河.md "wikilink")、[奧凡托河等河流流入亚得里亚海](../Page/奧凡托河.md "wikilink")；海中有近1200個島嶼，其中只有69個有人居住。

## 海中島嶼和沿岸城市

### 的島嶼

如[克爾克島](../Page/克爾克島.md "wikilink")、[姆列特島](../Page/姆列特島.md "wikilink")、[布拉奇島](../Page/布拉奇島.md "wikilink")、[帕島](../Page/帕島.md "wikilink")、[拉布島](../Page/拉布島.md "wikilink")、[洛希尼島](../Page/洛希尼島.md "wikilink")、[科爾丘拉島](../Page/科爾丘拉島.md "wikilink")、[維斯島](../Page/維斯島.md "wikilink")、[茨雷斯島](../Page/茨雷斯島.md "wikilink")、[赫瓦爾島等](../Page/赫瓦爾島.md "wikilink")。

### 沿岸城市

  - [威尼斯](../Page/威尼斯.md "wikilink")、[第里雅斯特](../Page/第里雅斯特.md "wikilink")、[安科納](../Page/安科納.md "wikilink")、[佩斯卡拉](../Page/佩斯卡拉.md "wikilink")、[巴里](../Page/巴里.md "wikilink")、[里米尼](../Page/里米尼.md "wikilink")、[布林迪西等](../Page/布林迪西.md "wikilink")

  - [科佩爾](../Page/科佩爾.md "wikilink")、[皮蘭](../Page/皮蘭.md "wikilink")、[伊佐拉等](../Page/伊佐拉.md "wikilink")

  - [里耶卡](../Page/里耶卡.md "wikilink")、[斯普利特](../Page/斯普利特.md "wikilink")、[杜布羅夫尼克](../Page/杜布羅夫尼克.md "wikilink")、[普拉](../Page/普拉.md "wikilink")、[扎達爾等](../Page/扎達爾.md "wikilink")

  - [巴爾](../Page/巴爾.md "wikilink")、[科托](../Page/科托.md "wikilink")、[布德瓦](../Page/布德瓦.md "wikilink")、[烏爾齊尼等](../Page/烏爾齊尼.md "wikilink")

  - [都拉斯](../Page/都拉斯.md "wikilink")、[夫羅勒等](../Page/夫羅勒.md "wikilink")

## 圖集

<File:Adriatic> Sea.jpg|亚得里亚海的衛星圖 <File:Locatie> Adriatische
zee.PNG|亚得里亚海與歐洲相對位置圖
<File:Dubrovnik-port.jpg>|[杜布羅夫尼克](../Page/杜布羅夫尼克.md "wikilink")，[克罗地亚](../Page/克罗地亚.md "wikilink")
<File:Beach1> Durres Albania
2005-07-10.jpg|[都拉斯](../Page/都拉斯.md "wikilink")，[阿爾巴尼亞](../Page/阿爾巴尼亞.md "wikilink")
<File:NeumCoastBH.jpg>|[涅姆](../Page/涅姆.md "wikilink")，[波赫聯邦](../Page/波赫聯邦.md "wikilink")
<File:Tremiti> 01.jpg|特雷米蒂島（Isole
Tremiti），[意大利](../Page/意大利.md "wikilink")

## 参考文献

## 外部連結

  - [亚得里亚海东岸天气预报](http://www.geabios.com/services/meteo/wv2.11/wv.htm?picurl=http://www.oc.phys.uoa.gr/waves/med/wavem&picext=.jpg&picset=06;12;18;24;30;36;42;48;54)

## 参见

  - [伊斯特拉半島](../Page/伊斯特拉半島.md "wikilink")
  - [達爾馬提亞](../Page/達爾馬提亞.md "wikilink")
  - [科托爾灣](../Page/科托爾灣.md "wikilink")
  - [威尼斯潟湖](../Page/威尼斯潟湖.md "wikilink")

{{-}}

[亚得里亚海](../Category/亚得里亚海.md "wikilink")