**亚洲杯电视围棋快棋赛**（），是现存历史最悠久的国际[围棋比赛之一](../Page/围棋.md "wikilink")，成立于1989年。该项赛事由[日本](../Page/日本.md "wikilink")[NHK](../Page/NHK.md "wikilink")、[韩国](../Page/韩国.md "wikilink")[KBS和](../Page/KBS.md "wikilink")[中国](../Page/中国.md "wikilink")[CCTV三家电视台](../Page/CCTV.md "wikilink")，以及[日本棋院](../Page/日本棋院.md "wikilink")、[韩国棋院和](../Page/韩国棋院.md "wikilink")[中国棋院轮流举办](../Page/中国棋院.md "wikilink")。参赛者为当年日本[NHK杯](../Page/NHK杯电视围棋淘汰赛.md "wikilink")、韩国[KBS杯和中国](../Page/KBS杯.md "wikilink")[中信银行杯](../Page/中信银行杯.md "wikilink")（原CCTV杯）的冠亚军，以及上届冠军7人。比赛规则规定：30秒内走一手，另有10次共10分钟的机动时间，一局在2个半小时左右结束。比赛采用单败淘汰制，其中上届冠军直接进入半决赛。冠军奖金250万日元、亚军50万日元，由NHK支付。

## 历届冠亚军

<table>
<thead>
<tr class="header">
<th></th>
<th><p>年</p></th>
<th><p>地点</p></th>
<th><p>冠军</p></th>
<th><p>亚军</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第1届</p></td>
<td><p>1989</p></td>
<td><p><a href="../Page/东京.md" title="wikilink">东京</a></p></td>
<td><p><a href="../Page/武宫正树.md" title="wikilink">武宫正树</a></p></td>
<td><p><a href="../Page/小林觉.md" title="wikilink">小林觉</a></p></td>
</tr>
<tr class="even">
<td><p>第2届</p></td>
<td><p>1990</p></td>
<td><p>东京</p></td>
<td><p>武宫正树（2）</p></td>
<td><p><a href="../Page/李昌镐.md" title="wikilink">李昌镐</a></p></td>
</tr>
<tr class="odd">
<td><p>第3届</p></td>
<td><p>1991</p></td>
<td><p><a href="../Page/汉城.md" title="wikilink">汉城</a></p></td>
<td><p>武宫正树（3）</p></td>
<td><p><a href="../Page/曹大元.md" title="wikilink">曹大元</a></p></td>
</tr>
<tr class="even">
<td><p>第4届</p></td>
<td><p>1992</p></td>
<td><p>东京</p></td>
<td><p>武宫正树（4）</p></td>
<td><p><a href="../Page/曹薰铉.md" title="wikilink">曹薰铉</a></p></td>
</tr>
<tr class="odd">
<td><p>第5届</p></td>
<td><p>1993</p></td>
<td><p><a href="../Page/北京.md" title="wikilink">北京</a></p></td>
<td><p><a href="../Page/依田纪基.md" title="wikilink">依田纪基</a></p></td>
<td><p><a href="../Page/徐奉洙.md" title="wikilink">徐奉洙</a></p></td>
</tr>
<tr class="even">
<td><p>第6届</p></td>
<td><p>1994</p></td>
<td><p><a href="../Page/釜山.md" title="wikilink">釜山</a></p></td>
<td><p><a href="../Page/大竹英雄.md" title="wikilink">大竹英雄</a></p></td>
<td><p><a href="../Page/钱宇平.md" title="wikilink">钱宇平</a></p></td>
</tr>
<tr class="odd">
<td><p>第7届</p></td>
<td><p>1995</p></td>
<td><p><a href="../Page/千叶.md" title="wikilink">千叶</a></p></td>
<td><p>李昌镐</p></td>
<td><p>曹薰铉</p></td>
</tr>
<tr class="even">
<td><p>第8届</p></td>
<td><p>1996</p></td>
<td><p><a href="../Page/青岛.md" title="wikilink">青岛</a></p></td>
<td><p>李昌镐（2）</p></td>
<td><p><a href="../Page/刘昌赫.md" title="wikilink">刘昌赫</a></p></td>
</tr>
<tr class="odd">
<td><p>第9届</p></td>
<td><p>1997</p></td>
<td><p><a href="../Page/庆州.md" title="wikilink">庆州</a></p></td>
<td><p><a href="../Page/俞斌.md" title="wikilink">俞斌</a></p></td>
<td><p><a href="../Page/王立诚.md" title="wikilink">王立诚</a></p></td>
</tr>
<tr class="even">
<td><p>第10届</p></td>
<td><p>1998</p></td>
<td><p>东京</p></td>
<td><p>依田纪基（2）</p></td>
<td><p><a href="../Page/马晓春.md" title="wikilink">马晓春</a></p></td>
</tr>
<tr class="odd">
<td><p>第11届</p></td>
<td><p>1999</p></td>
<td><p><a href="../Page/天津.md" title="wikilink">天津</a></p></td>
<td><p>依田纪基（3）</p></td>
<td><p>李昌镐</p></td>
</tr>
<tr class="even">
<td><p>第12届</p></td>
<td><p>2000</p></td>
<td><p>庆州</p></td>
<td><p>曹薰铉</p></td>
<td><p>李昌镐</p></td>
</tr>
<tr class="odd">
<td><p>第13届</p></td>
<td><p>2001</p></td>
<td><p>东京</p></td>
<td><p>曹薰铉（2）</p></td>
<td><p><a href="../Page/睦镇硕.md" title="wikilink">睦镇硕</a></p></td>
</tr>
<tr class="even">
<td><p>第14届</p></td>
<td><p>2002</p></td>
<td><p>北京</p></td>
<td><p>李昌镐（3）</p></td>
<td><p>曹薰铉</p></td>
</tr>
<tr class="odd">
<td><p>第15届</p></td>
<td><p>2003</p></td>
<td><p>汉城</p></td>
<td><p><a href="../Page/周鹤洋.md" title="wikilink">周鹤洋</a></p></td>
<td><p><a href="../Page/三村智保.md" title="wikilink">三村智保</a></p></td>
</tr>
<tr class="even">
<td><p>第16届</p></td>
<td><p>2004</p></td>
<td><p>东京</p></td>
<td><p>俞斌（2）</p></td>
<td><p><a href="../Page/宋泰坤.md" title="wikilink">宋泰坤</a></p></td>
</tr>
<tr class="odd">
<td><p>第17届</p></td>
<td><p>2005</p></td>
<td><p>北京</p></td>
<td><p><a href="../Page/张栩.md" title="wikilink">张栩</a></p></td>
<td><p><a href="../Page/赵汉乘.md" title="wikilink">赵汉乘</a></p></td>
</tr>
<tr class="even">
<td><p>第18届</p></td>
<td><p>2006</p></td>
<td><p><a href="../Page/首尔.md" title="wikilink">首尔</a></p></td>
<td><p><a href="../Page/王檄.md" title="wikilink">王檄</a></p></td>
<td><p>李昌镐</p></td>
</tr>
<tr class="odd">
<td><p>第19届</p></td>
<td><p>2007</p></td>
<td><p>东京</p></td>
<td><p><a href="../Page/李世乭.md" title="wikilink">李世乭</a></p></td>
<td><p><a href="../Page/陈耀烨.md" title="wikilink">陈耀烨</a></p></td>
</tr>
<tr class="even">
<td><p>第20届</p></td>
<td><p>2008</p></td>
<td><p>北京</p></td>
<td><p>李世乭（2）</p></td>
<td><p>赵汉乘</p></td>
</tr>
<tr class="odd">
<td><p>第21届</p></td>
<td><p>2009</p></td>
<td><p>首尔</p></td>
<td><p><a href="../Page/孔杰.md" title="wikilink">孔杰</a></p></td>
<td><p>李世乭</p></td>
</tr>
<tr class="even">
<td><p>第22届</p></td>
<td><p>2010</p></td>
<td><p>东京</p></td>
<td><p>孔杰（2）</p></td>
<td><p><a href="../Page/结城聪.md" title="wikilink">结城聪</a></p></td>
</tr>
<tr class="odd">
<td><p>第23届</p></td>
<td><p>2011</p></td>
<td><p>北京</p></td>
<td><p>孔杰（3）</p></td>
<td><p><a href="../Page/白洪淅.md" title="wikilink">白洪淅</a></p></td>
</tr>
<tr class="even">
<td><p>第24届</p></td>
<td><p>2012</p></td>
<td><p>首尔</p></td>
<td><p>白洪淅</p></td>
<td><p>孔杰</p></td>
</tr>
<tr class="odd">
<td><p>第25届</p></td>
<td><p>2013</p></td>
<td><p>东京</p></td>
<td><p><a href="../Page/井山裕太.md" title="wikilink">井山裕太</a></p></td>
<td><p><a href="../Page/朴廷桓.md" title="wikilink">朴廷桓</a></p></td>
</tr>
<tr class="even">
<td><p>第26届</p></td>
<td><p>2014</p></td>
<td><p>北京</p></td>
<td><p>李世乭（3）</p></td>
<td><p><a href="../Page/河野临.md" title="wikilink">河野临</a></p></td>
</tr>
<tr class="odd">
<td><p>第27届</p></td>
<td><p>2015</p></td>
<td><p>首尔</p></td>
<td><p>李世乭（4）</p></td>
<td><p>朴廷桓</p></td>
</tr>
<tr class="even">
<td><p>第28届</p></td>
<td><p>2016</p></td>
<td><p>東京</p></td>
<td><p><a href="../Page/李欽誠.md" title="wikilink">李欽誠</a></p></td>
<td><p><a href="../Page/申真諝.md" title="wikilink">申真諝</a></p></td>
</tr>
<tr class="odd">
<td><p>第29届</p></td>
<td><p>2017</p></td>
<td><p>平湖</p></td>
<td><p><a href="../Page/罗玄.md" title="wikilink">罗玄</a></p></td>
<td><p>李世乭</p></td>
</tr>
<tr class="even">
<td><p>第30届</p></td>
<td><p>2018</p></td>
<td><p>首尔</p></td>
<td><p><a href="../Page/金志锡.md" title="wikilink">金志锡</a></p></td>
<td><p>罗玄</p></td>
</tr>
</tbody>
</table>

## 第1届亚洲电视快棋赛

## 第2届亚洲电视快棋赛

## 第3届亚洲电视快棋赛

## 第4届亚洲电视快棋赛

## 第5届亚洲电视快棋赛

## 第6届亚洲电视快棋赛

## 第7届亚洲电视快棋赛

## 第8届亚洲电视快棋赛

## 第9届亚洲电视快棋赛

## 第10届亚洲电视快棋赛

## 第11届亚洲电视快棋赛

## 第12届亚洲电视快棋赛

## 第13届亚洲电视快棋赛

## 第14届亚洲电视快棋赛

## 第15届亚洲电视快棋赛

## 第16届亚洲电视快棋赛

## 第17届亚洲电视快棋赛

## 第18届亚洲电视快棋赛

## 第19届亚洲电视快棋赛

## 第20届亚洲电视快棋赛

## 第21届亚洲电视快棋赛

于2009年6月9日-12日在韩国[金浦机场梅菲尔德](../Page/金浦机场.md "wikilink")(MayField)酒店举行。

## 第22届亚洲电视快棋赛

## 第23届亚洲电视快棋赛

## 第24届亚洲电视快棋赛

## 第25届亚洲电视快棋赛

  - ：[王檄](../Page/王檄.md "wikilink")、[江维杰](../Page/江维杰.md "wikilink")

  - ：[朴廷桓](../Page/朴廷桓.md "wikilink")、[李昌镐](../Page/李昌镐.md "wikilink")、[李世石](../Page/李世石.md "wikilink")\[1\]

  - ：[结城聪](../Page/结城聪.md "wikilink")、[井山裕太](../Page/井山裕太.md "wikilink")

## 第26届亚洲电视快棋赛

  - ：[李钦诚](../Page/李钦诚.md "wikilink")、[陶欣然](../Page/陶欣然.md "wikilink")；

  - ：[李世石](../Page/李世石.md "wikilink")、[朴廷桓](../Page/朴廷桓.md "wikilink")；

  - ：[结城聪](../Page/结城聪.md "wikilink")、[井山裕太](../Page/井山裕太.md "wikilink")、[河野临](../Page/河野临.md "wikilink")\[2\]

## 第27届亚洲电视快棋赛

  - ：[李世乭](../Page/李世乭.md "wikilink")、[李東勳](../Page/李東勳_\(围棋棋手\).md "wikilink")、[朴廷桓](../Page/朴廷桓.md "wikilink")；

  - ：[伊田笃史](../Page/伊田笃史.md "wikilink")、[一力辽](../Page/一力辽.md "wikilink")；

  - ：[廖行文](../Page/廖行文.md "wikilink")、[杨鼎新](../Page/杨鼎新.md "wikilink")

## 第28届亚洲电视快棋赛

  - ：[李世乭](../Page/李世乭.md "wikilink")、[申真谞](../Page/申真谞.md "wikilink")、[朴廷桓](../Page/朴廷桓.md "wikilink")；

  - ：[张栩](../Page/张栩.md "wikilink")、寺山-{怜}-；

  - ：[李钦诚](../Page/李钦诚.md "wikilink")、[芈昱廷](../Page/芈昱廷.md "wikilink")

## 第29届亚洲电视快棋赛

2017年9月15日至17日，浙江[平湖市雷克大酒店](../Page/平湖市.md "wikilink")

  - ：[李世乭九段](../Page/李世乭.md "wikilink")、[罗玄八段](../Page/罗玄.md "wikilink")；

  - ：[井山裕太九段](../Page/井山裕太.md "wikilink")、[一力辽七段](../Page/一力辽.md "wikilink")；

  - ：[李钦诚九段](../Page/李钦诚.md "wikilink")、[李轩豪七段](../Page/李轩豪.md "wikilink")、[张涛六段](../Page/张涛_\(围棋\).md "wikilink")

## 第30届亚洲电视快棋赛

  - ：[朴廷桓九段](../Page/朴廷桓.md "wikilink")、[金志锡九段](../Page/金志锡.md "wikilink")、[罗玄八段](../Page/罗玄.md "wikilink")；

  - ：[井山裕太九段](../Page/井山裕太.md "wikilink")、[志田达哉七段](../Page/志田达哉.md "wikilink")

  - ：[范蕴若五段](../Page/范蕴若.md "wikilink")、[范廷钰九段](../Page/范廷钰.md "wikilink")

## 参考

<references />

## 外部链接

  - [テレビ囲碁アジア選手権_財団法人日本棋院](http://www.nihonkiin.or.jp/match/tv-asia/index.html)
  - [亞洲盃電視快棋賽歷屆冠亞軍_韓國棋院](http://www.baduk.or.kr/info/gijun_view.asp?cmpt_tnmt_div=2&cmpt_code=2101)
  - [亚洲电视快棋赛_TOM棋圣道场](http://weiqi.sports.tom.com/zhuantinew/75.html)
  - [亚洲电视围棋快棋赛_CCTV](http://cctv.com/sports/relation/040805/10.html)
  - [Asian TV Cup_gobase.org](http://gobase.org/games/nn/asiatv/)

[Y](../Category/国际围棋比赛.md "wikilink")

1.  上届冠军白洪淅由于军人特殊身份将放弃今年的国际比赛资格[白洪淅因兵役无缘亚洲杯李世石递补
    三国名单确定](http://sports.sina.com.cn/go/2013-05-26/10296588841.shtml)
2.  [NHK杯结城聪胜河野临三连霸
    二人与井山共战亚洲杯](http://sports.sina.com.cn/go/2014-03-23/14337080518.shtml)