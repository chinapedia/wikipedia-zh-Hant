[Subtropical1.png](https://zh.wikipedia.org/wiki/File:Subtropical1.png "fig:Subtropical1.png")
[Antananarivo_(atamari).jpg](https://zh.wikipedia.org/wiki/File:Antananarivo_\(atamari\).jpg "fig:Antananarivo_(atamari).jpg")
[Wetland_Hong_Kong.jpg](https://zh.wikipedia.org/wiki/File:Wetland_Hong_Kong.jpg "fig:Wetland_Hong_Kong.jpg")
**亚热带**，又稱**副热带**，是[地球上的一种](../Page/地球.md "wikilink")[气候地带](../Page/气候.md "wikilink")。一般亚热带位于[温带靠近](../Page/温带.md "wikilink")[热带的地区](../Page/热带.md "wikilink")（大約23.5°N～40°N、23.5°S～40°S之间）。亚热带的气候特点是其[夏季与热带相似](../Page/夏季.md "wikilink")，平均氣溫高於攝氏22度，亚热带地区的夏天可以比熱帶地区更炎熱；但[冬季受靠近極區的大陸高壓及季風影响](../Page/冬季.md "wikilink")，氣温明显比热帶寒冷，但較少出現下雪天氣。最冷月均温在[摄氏](../Page/摄氏.md "wikilink")0度到18度之間。亞熱帶的降雨有季節性，通常分為雨季及旱季。

## 次级分区

[南亚热带](../Page/南亚热带.md "wikilink")、[中亚热带](../Page/中亚热带.md "wikilink")、[北亚热带](../Page/北亚热带.md "wikilink")

## 气候

### 气候型

[副热带季风气候](../Page/副热带季风气候.md "wikilink")、[副熱帶夏乾氣候（地中海型氣候）](../Page/地中海型氣候.md "wikilink")、[副热带湿润气候](../Page/副热带湿润气候.md "wikilink")、[副熱帶乾旱半乾旱氣候](../Page/副热带干旱半干旱气候.md "wikilink")

[柯本氣候分類法](../Page/柯本氣候分類法.md "wikilink")，亞熱帶氣候屬於C類，分別是[副熱帶濕潤氣候](../Page/副熱帶濕潤氣候.md "wikilink")（Cfa,
Cwa）、[海洋性氣候](../Page/海洋性氣候.md "wikilink")（Cfb, Cwb,
Cfc）及[地中海式氣候](../Page/地中海式氣候.md "wikilink")（Csa, Csb）。

### 天气系统

活动于本带的天气系统有[热带气旋](../Page/热带气旋.md "wikilink")、[锋面气旋](../Page/锋面气旋.md "wikilink")、[季风槽](../Page/季风槽.md "wikilink")、[副热带高压](../Page/副热带高压.md "wikilink")、[西风带等](../Page/西风带.md "wikilink")

## 土壤

本区土壤偏[酸性分布着大范围的](../Page/酸性.md "wikilink")[红壤和](../Page/红壤.md "wikilink")[黄土](../Page/黄土.md "wikilink")。本区土壤由于受雨水淋溶和常年的微生物分解，大部分地区相对其他地区土壤贫瘠。

## 植被

该地区植被特点是[根系广大](../Page/根系.md "wikilink")，不论是在溼润地区或是干旱地区。主要植被类型有[亚热带常绿阔叶林](../Page/亚热带常绿阔叶林.md "wikilink")、[亚热带常绿硬叶林](../Page/亚热带常绿硬叶林.md "wikilink")、[沙漠植被](../Page/沙漠植被.md "wikilink")\[1\]。

## 参考文献

## 外部链接

  -
## 参见

  - [副热带高压](../Page/副热带高压.md "wikilink")
  - [热带](../Page/热带.md "wikilink")、[温带](../Page/温带.md "wikilink")

{{-}}

[Category:气候带](../Category/气候带.md "wikilink")
[亚热带](../Category/亚热带.md "wikilink")

1.  <http://jtsb.scib.ac.cn/jtsb_cn/ch/index.aspx> 热带亚热带植物学报