**亚瑟·威廉·泰德，第一代泰德男爵**（****，）[GCB](../Page/巴斯勳章.md "wikilink")，[英国皇家空军元帅](../Page/英国空军元帅列表.md "wikilink")。他在[一戰時是一名](../Page/一戰.md "wikilink")[陸軍皇家飛行隊戰鬥機機師以及飛行中隊隊長](../Page/英國皇家飛行隊.md "wikilink")，[二戰期間負責指揮](../Page/二戰.md "wikilink")[中東的皇家空軍](../Page/中東.md "wikilink")，曾參與在地中海和北非的行動，包括[克里特撤退和北非的](../Page/克里特島戰役.md "wikilink")[十字軍行動](../Page/:en:_Operation_Crusader.md "wikilink")。他的轟炸有[泰德地毯之稱](../Page/:en:Tedder_Carpet.md "wikilink")。後來泰德成為[盟軍地中海空軍司令部司令](../Page/:en:Mediterranean_Air_Command.md "wikilink")、參與計劃[入侵西西里和](../Page/盟軍入侵西西里.md "wikilink")[義大利](../Page/入侵義大利.md "wikilink")。當[大君主作戰制定時](../Page/大君主作戰.md "wikilink")，泰德成為[盟軍遠征部隊最高司令部副司令](../Page/盟軍遠征部隊最高司令部.md "wikilink")，地位僅次於[德懷特·艾森豪](../Page/德懷特·艾森豪.md "wikilink")。戰後成為英國皇家空軍參謀長，期間面對大量機師流失，因此他主張招聘人手，同時把增編一倍，1948年他落實[柏林封鎖的空運](../Page/柏林封鎖.md "wikilink")。

## 生平

### 早年生涯

1890年7月11日，亞瑟·威廉·泰德在[格拉斯哥北面的](../Page/格拉斯哥.md "wikilink")[斯特靈出世](../Page/:en:_Stirlingshire.md "wikilink")，\[1\]母親名叫艾米莉·夏洛特·泰德(Emily
Charlotte Tedder)，原姓布萊森（Bryson）；父親亞瑟·約翰·泰德爵士（Sir Arthur John
Tedder）是一名海關専員，負責設計老年退休金計劃， \[2\]
父親的職業令年輕的泰德認識英國不同的島嶼，包括1895至1898年在[昔德蘭群島的](../Page/昔德蘭群島.md "wikilink")[勒威克以及](../Page/:en:Lerwick.md "wikilink")1899至1901年在[埃爾根](../Page/:en:_Elgin,_Moray.md "wikilink")。\[3\]
1902年，泰德一家搬往[克羅伊登](../Page/克羅伊登.md "wikilink")，泰德亦入讀[惠特吉夫特學校](../Page/:en:_Whitgift_School.md "wikilink")。1909年泰德入讀[劍橋大學](../Page/劍橋大學.md "wikilink")
\[4\]
泰德當時屬於[莫德林學院](../Page/劍橋大學莫德林學院.md "wikilink")，他在劍橋時修讀歷史。\[5\]1912年6月，泰德獲得了二等二級營譽學位。
\[6\]

1912年暑假，泰德在[柏林學習德文](../Page/柏林.md "wikilink")。\[7\]
新學年開始時，泰德回到莫德林完成第四年的學習，準備將來成為外交官。\[8\]1913年9月2日，當他在大學的最後一天時，泰德任官為的[少尉](../Page/少尉.md "wikilink")。\[9\]

泰德在大學畢業後成為軍校學生，並於1914年2月離開英國，前往[斐濟服役](../Page/斐濟.md "wikilink")。\[10\]當[一戰爆發時](../Page/一戰.md "wikilink")，泰德發現斐濟的生活與他的興趣有落差，因此他回到英國，加入正規部隊。\[11\]

### 軍事生涯

#### 第一次世界大戰

1914年10月14日，泰德擢升為[中尉](../Page/中尉.md "wikilink")。\[12\]
12月時回到英國。他調往一個位於多塞特海岸的預備役。1915年2月，泰德的膝蓋嚴重受傷，\[13\]令他無法完成整個步兵役。雖然他大部份時間都在[加萊的一個軍營裏](../Page/加萊.md "wikilink")，\[14\]他申請調往\[英國皇家飛行隊\]\]。\[15\]

1916年1月，泰德加入皇家飛行隊，並入讀第一航空學校(No. 1 School of
Aeronautics)。\[16\]他在1916年3月21日擢升為[上尉](../Page/上尉_\(英國陸軍與皇家海軍陸戰隊\).md "wikilink")。\[17\]4月他進入[中央飛行學校](../Page/:en:Central_Flying_School.md "wikilink")，在那裏他學懂駕駛飛機。\[18\]1916年6月，他成為[第25中隊](../Page/:en:No._XXV_Squadron_RAF.md "wikilink")
的飛行員，駕駛著[布里斯托偵察機在](../Page/:en:Bristol_Scout.md "wikilink")[西線服役](../Page/西方戰線_\(第一次世界大戰\).md "wikilink")。\[19\]8月9日，泰德成為中隊的指揮官。\[20\]\[21\]1917年的第一天，泰德擢升成[少校](../Page/少校.md "wikilink")\[22\]並委任成[第70中隊的指揮官](../Page/:en:No._LXX_Squadron_RAF.md "wikilink")。\[23\]泰德與裝備了[Sopwith
1½
Strutter的新中隊留在西線](../Page/:en:Sopwith_1½_Strutter.md "wikilink")。\[24\]1917年5月26日，他獲授意大利[銀製勇敢勳章](../Page/銀製勇敢勳章.md "wikilink")。\[25\]

1917年6月25日，泰德上任的。\[26\]6月24日，他成為駐埃及第38大隊長，\[27\]7月23日他臨時擢升成[中校](../Page/中校.md "wikilink")。\[28\]（中校軍階於1919年4月2日結束）\[29\]

#### 戰間期

1919年5月，泰德成為[Bircham
Newton皇家空軍基地的第](../Page/:en:_RAF_Bircham_Newton.md "wikilink")274中隊長，該中隊裝備了[Handley
Page
V/1500](../Page/:en:Handley_Page_V/1500.md "wikilink")——當時皇家空軍最大的轟炸機。\[30\]同年8月1日，泰德接受了新成立的皇家空軍永久的任命。\[31\]
1920年2月，中隊改名成第207中隊並裝備[DH9a轟炸機](../Page/:en:DH9a.md "wikilink")。1922-23年，由於恰納卡萊危機(Chanak
Crisis)爆發，第207中隊派往土耳其。\[32\]在1923年末至1924春期間泰德在[皇家海軍參謀學院進修](../Page/:en:RN_Staff_College.md "wikilink")。\[33\]

1924年元旦，泰德正式成為皇家空軍的[中校](../Page/:en:Wing_Commander_\(rank\).md "wikilink")，\[34\]並於9月成為[RAF
Digby的基地指揮官和位於當地的第](../Page/:en:RAF_Digby.md "wikilink")2飛行訓練學校(No. 2
Flying Training
School)的校長，其後於1927年1月泰德成為[空軍部中訓練部的參謀](../Page/:en:Air_Ministry.md "wikilink")。\[35\]
1928年泰德入讀[帝國防禦學院並於](../Page/:en:Imperial_Defence_College.md "wikilink")1929年1月成為[皇家空軍參謀學院的助理校長](../Page/:en:RAF_Staff_College,_Andover.md "wikilink")。\[36\]1931年1月1日，泰德擢升成上校\[37\]並於翌年1月前往位於[RAF
Eastchurch的空軍軍備學校成為校長](../Page/:en:RAF_Eastchurch.md "wikilink")。\[38\]1934年4月4日，泰德成為訓練部的主任。\[39\]同年7月1日，泰德成為空軍准將。\[40\]

1936年11月,泰德獲任命為英國皇家空軍遠東地區的指揮官，\[41\]指揮著從[緬甸到](../Page/英屬緬甸.md "wikilink")[香港和](../Page/英屬香港.md "wikilink")[婆羅洲的皇家空軍部隊](../Page/婆羅洲.md "wikilink")。\[42\]1937年2月1日，泰德獲得[巴斯勳章](../Page/巴斯勳章.md "wikilink")\[43\]，並於1937年7月1日擢升成少將。\[44\]1938年7月，泰德成為空軍部中研究部的主任。\[45\]

#### 第二次世界大戰

[Tedder1942.jpg](https://zh.wikipedia.org/wiki/File:Tedder1942.jpg "fig:Tedder1942.jpg")
1939年戰爭爆發時，泰德調往新成立的,
但他未能與戰機生產大臣好好合作，和首相[邱吉爾的關係也欠佳](../Page/溫斯頓·邱吉爾.md "wikilink")。1940年11月29日，他成為\[46\]
副總司令並臨時晉升成空軍中將\[47\](於1942年4月正式晉升\[48\])。

泰德於1941年6月1日成為中東戰區總司令\[49\] \[50\] 。他並非邱吉爾的首選但原屬意人選空軍少將被俘，故改任泰德。\[51\]
作為皇家空軍中東戰區總司令，他指揮皇家空軍於地中海及北非的空中行動，包括1941年5月於[克里特島的](../Page/克里特島.md "wikilink")[撤退行動及](../Page/克里特島戰役.md "wikilink")1941年年尾位於北非的\[52\]。泰德於1942年新年獲頒[爵級司令巴斯勳章](../Page/巴斯勳章.md "wikilink")\[53\]。1942年7月3日，他臨時晉升成空軍上將。\[54\]

泰德監督於[西部沙漠空中武力的建立](../Page/西部沙漠戰役.md "wikilink")，並且建立一個套有效的行動及管理政策，這套政策被視為盟軍取得[第二次阿拉曼戰役勝利的關鍵](../Page/第二次阿拉曼戰役.md "wikilink")。\[55\]
他的其中一個轟炸戰術後來稱為。\[56\]他於1942年11月27日獲頒[爵級大十字巴斯勳章以表揚他在中東的勤務](../Page/巴斯勳章.md "wikilink")。\[57\]
[Meeting_of_the_Supreme_Command.jpg](https://zh.wikipedia.org/wiki/File:Meeting_of_the_Supreme_Command.jpg "fig:Meeting_of_the_Supreme_Command.jpg")

1943年2月泰德成為總司令，\[58\]隸屬於地中海戰區總司令[艾森豪威爾將軍](../Page/艾森豪威爾.md "wikilink")，並參與在盟軍入侵[西西里及](../Page/西西里島戰役.md "wikilink")[意大利的行動](../Page/入侵義大利.md "wikilink")。\[59\]他於1943年8月27日獲頒美國\[60\]及於1943年10月1日獲頒大十字勳位。\[61\]
1943年12月泰德成為司令。\[62\]

當[大君主作戰開始計劃時](../Page/大君主作戰.md "wikilink")，泰德於1944年1月經艾森豪威爾任命為[盟軍遠征部隊最高司令部](../Page/盟軍遠征部隊最高司令部.md "wikilink")（SHAEF）副司令。\[63\]
可是他對[伯納德·勞·蒙哥馬利將軍產生反感](../Page/伯納德·勞·蒙哥馬利.md "wikilink")，並於後來[諾曼地戰役中批評蒙哥馬利的表現並提出把蒙哥馬利退出指揮](../Page/諾曼地戰役.md "wikilink")。\[64\]
在戰爭的最後一年，盟軍在西線的[突出部之戰受德軍壓制](../Page/突出部之戰.md "wikilink")，泰德奉派[蘇聯尋求紅軍協助](../Page/蘇聯.md "wikilink")。\[65\]德國無條件投降時，泰德在代表艾森豪威爾簽署。\[66\]1945年6月6日，他正式昇為空軍上將。\[67\]
1945年8月28日，泰德獲頒蘇聯[一級庫圖佐夫勳章](../Page/庫圖佐夫勳章.md "wikilink")\[68\]
及於1945年9月12日晉升成英國皇家空軍元帥。\[69\]
[Zhukov_reads_capitulation_act.jpg](https://zh.wikipedia.org/wiki/File:Zhukov_reads_capitulation_act.jpg "fig:Zhukov_reads_capitulation_act.jpg")[朱可夫元帥](../Page/朱可夫.md "wikilink")，他正在宣讀投降條款。\]\]

## 參考

## 參考資料

  -
  -
  -
  -
  -
  -
[Category:英國皇家空軍參謀長](../Category/英國皇家空軍參謀長.md "wikilink")
[T](../Category/英國皇家空軍元帥.md "wikilink")
[Category:英國皇家空軍武官](../Category/英國皇家空軍武官.md "wikilink")
[T](../Category/聯合王國男爵.md "wikilink")
[Category:柏金遜症患者](../Category/柏金遜症患者.md "wikilink")
[Category:奧蘭治-拿騷騎士大十字勳章授勛者](../Category/奧蘭治-拿騷騎士大十字勳章授勛者.md "wikilink")

1.

2.
3.
4.
5.
6.
7.
8.
9.
10. Probert, p. 36

11.
12.

13.
14.
15.
16.
17.

18.

19.
20.

21.
22.

23.
24.
25.

26.
27.
28.

29.

30.
31.

32. [The National Archives](../Page/The_National_Archives.md "wikilink")
    AIR 8/59

33.
34.
35.
36.
37.
38.
39.

40.

41.
42.
43.

44.

45.
46.
47.

48.

49.
50.

51.
52.
53.

54.

55. Probert, p. 37

56.

57.

58.
59. Probert, p. 38

60.

61.

62.
63.
64.
65.
66.
67.

68.

69.