[Sulfoxide.png](https://zh.wikipedia.org/wiki/File:Sulfoxide.png "fig:Sulfoxide.png")
**亚砜**是含有亚硫酰基（\>S=O）[官能团的一类](../Page/官能团.md "wikilink")[有机化合物](../Page/有机化合物.md "wikilink")，可由[硫醚](../Page/硫醚.md "wikilink")[氧化得到](../Page/氧化.md "wikilink")。常见的亚砜有[氯化亚砜](../Page/氯化亚砜.md "wikilink")、[二甲基亞碸](../Page/二甲基亞碸.md "wikilink")、[二苯基亞碸等](../Page/二苯基亞碸.md "wikilink")。

## 性质

亚砜的通式为R-S<sup>=O</sup>-R'，其中R和R'是有机[基团](../Page/基团.md "wikilink")。亚砜中的成键与氧化叔膦（R<sub>3</sub>P=O）类似，由于[电负性差异](../Page/电负性.md "wikilink")，S=O键中，硫显正价，氧显负价：

  -

      -
        [Sulfoxide-resonance.png](https://zh.wikipedia.org/wiki/File:Sulfoxide-resonance.png "fig:Sulfoxide-resonance.png")

硫原子为[四面体结构](../Page/四面体.md "wikilink")，有一对[孤对电子](../Page/孤对电子.md "wikilink")，类似于sp<sup>3</sup>杂化的碳原子。当硫所连接的两个基团不相同时，就会产生[手性](../Page/手性_\(化学\).md "wikilink")，比如[甲基苯基亚砜](../Page/甲基苯基亚砜.md "wikilink")。

  -

      -
        [Sulfoxide-tetrahedral.png](https://zh.wikipedia.org/wiki/File:Sulfoxide-tetrahedral.png "fig:Sulfoxide-tetrahedral.png")

有时构型转换所需的能量相当高，室温下的[外消旋速率很慢](../Page/外消旋.md "wikilink")，以至于对映体相对稳定。有些手性的亚砜在医药中有应用，比如[埃索美拉唑和](../Page/埃索美拉唑.md "wikilink")[阿莫达非](../Page/阿莫达非.md "wikilink")。此外，亚砜也被用作[手性辅助剂](../Page/手性辅助剂.md "wikilink")。\[1\]

## 反应

亚砜中硫的[氧化态为](../Page/氧化态.md "wikilink")0，处于[硫醚](../Page/硫醚.md "wikilink")（-2）和[砜](../Page/砜.md "wikilink")（+2）之间。因此氧化[硫醚时](../Page/硫醚.md "wikilink")，依次会得到亚砜、砜。例如氧化[二甲基硫醚时可以得到](../Page/二甲硫醚.md "wikilink")[二甲基亞碸和](../Page/二甲基亞碸.md "wikilink")[二甲基砜](../Page/二甲基砜.md "wikilink")。很多有手性的亚砜可从非手性的硫醚为原料，在[过渡金属和手性配体的存在下以](../Page/过渡金属.md "wikilink")[不对称催化氧化反应合成](../Page/不对称催化氧化反应.md "wikilink")。

亚砜可由强碱如[氢化钠](../Page/氢化钠.md "wikilink")[去质子化](../Page/去质子化.md "wikilink")\[2\]，很多亚砜（如[DMSO](../Page/二甲基亚砜.md "wikilink")）都是很好的[配體和](../Page/配體.md "wikilink")[烷化剂](../Page/烷化剂.md "wikilink")。

## 参考资料

<div class="references-small">

<references/>

</div>

[Y](../Category/官能团.md "wikilink")
[Category:有机硫化合物](../Category/有机硫化合物.md "wikilink")
[\*](../Category/亚砜.md "wikilink")

1.  *Oxidation of sulfides to chiral sulfoxides using Schiff
    base-vanadium (IV) complexes* Ángeles Gama, Lucía Z. Flores-López,
    Gerardo Aguirre, Miguel Parra-Hake, Lars H. Hellberg, and Ratnasamy
    Somanathan [Arkivoc](../Page/Arkivoc.md "wikilink") MX-789E **2003**
    [Online
    article](http://www.arkat-usa.org/ark/journal/2003/I11_Mexico/MX-789E/789E.asp)

2.  Iwai, I.; Ide, J. "2,3-Diphenyl-1,3-Butadiene" *[Organic
    Syntheses](../Page/Organic_Syntheses.md "wikilink")*, Collected
    Volume 6, p.531 (1988).
    <http://www.orgsyn.org/orgsyn/orgsyn/prepContent.asp?prep=CV6P0531.pdf>.