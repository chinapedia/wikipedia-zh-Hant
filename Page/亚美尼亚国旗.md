**亚美尼亚国旗**，即[亚美尼亚](../Page/亚美尼亚.md "wikilink")[三色旗](../Page/三色旗.md "wikilink")，由三个等宽水平长条组成，最上面是[红色](../Page/红色.md "wikilink")，中间为[蓝色](../Page/蓝色.md "wikilink")，底部为[黃色](../Page/黃色.md "wikilink")，每个长条宽20厘米。亚美尼亚最高[苏维埃于](../Page/苏维埃.md "wikilink")1990年8月24日采用了当前的国旗。2006年6月15日，[亚美尼亚议会通过了](../Page/亚美尼亚议会.md "wikilink")《亚美尼亚共和国国旗法》。

在历史上，亚美尼亚国旗经历了许多变迁。在古代，亚美尼亚王朝的旗帜是由不同象征的动物图案构成。在20世纪，则出现了不同种类的[苏维埃旗帜](../Page/亚美尼亚苏维埃社会主义共和国国旗.md "wikilink")。\[1\]

## 三色旗的象征意义

国旗中各种颜色的意义有许多种解释。许多人认为其中紅色代表亚美尼亚军人在战争中所流的鮮血，藍色代表潔淨的天空並象徵自由，黃色代表肥沃的土地並有農人辛勤耕種。\[2\]

[亚美尼亚宪法中所提供的官方说法则是](../Page/亚美尼亚宪法.md "wikilink")：

## 图案

由于亚美尼亚政府没有详细说明国旗上红蓝黃三种颜色的深浅度，所以目前有两种版本同时流传。流传较广的版本图案更加明亮，而使用较少的版本则颜色偏暗。下面的表格给出了两种版本的近似RGB值。\[3\]

|                                       |                                                                                                           |                                                                                                                                            |
| ------------------------------------- | --------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------ |
|                                       | 流传较广的版本                                                                                                   | 使用较少的版本                                                                                                                                    |
|                                       | [Flag_of_Armenia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Armenia.svg "fig:Flag_of_Armenia.svg") | [Flag_of_Armenia_(variant).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Armenia_\(variant\).svg "fig:Flag_of_Armenia_(variant).svg") |
| <span style="color: red;">红</span>    | 255-0-0                                                                                                   | 216-28-63                                                                                                                                  |
| <span style="color: blue;">蓝</span>   | 0-0-170                                                                                                   | 85-117-196                                                                                                                                 |
| <span style="color: orange;">黃</span> | 255-153-0                                                                                                 | 239-107-0                                                                                                                                  |

## 亚美尼亚国旗的历史

[Artaxiad_standard2.svg](https://zh.wikipedia.org/wiki/File:Artaxiad_standard2.svg "fig:Artaxiad_standard2.svg")

[阿尔塔什斯王朝](../Page/阿尔塔什斯王朝.md "wikilink")（公元前189年至公元元年）在古代亚美尼亚的国旗与现在的版本之间毫无共通之处。古代国旗上常常画有龙、鹰，或其他一些神秘的[图腾](../Page/图腾.md "wikilink")（有时是一头狮子）。旗子常系紧在长杆末端，鼓舞军队浴血奋战。当降临节来临时，[亚美尼亚帝国的历代王朝往往会悬挂风格各异的旗帜](../Page/亚美尼亚帝国.md "wikilink")，比如[阿尔塔什斯王朝的旗帜](../Page/阿尔塔什斯.md "wikilink")，在红色的背景之上绘上两头互相瞪视的鹰，中间隔着一朵金色花朵。

### 19世纪

[1885ArmenianFlag.svg](https://zh.wikipedia.org/wiki/File:1885ArmenianFlag.svg "fig:1885ArmenianFlag.svg")

当亚美尼亚被[奥斯曼帝国和](../Page/奥斯曼帝国.md "wikilink")[波斯瓜分](../Page/波斯.md "wikilink")，亚美尼亚国旗的概念在一段时间里消失了。然而，在1885年[巴黎亚美尼亚留学生协会希望参加](../Page/巴黎亚美尼亚留学生协会.md "wikilink")[维克多·雨果的葬礼时能够带上一面国旗](../Page/维克多·雨果.md "wikilink")，并为此求助一名[亚美尼亚天主教牧师时](../Page/亚美尼亚天主教.md "wikilink")，神父[葛文·亚立珊](../Page/葛文·亚立珊.md "wikilink")（[Ghevont
Alishan](../Page/:en:Ghevont_Alishan.md "wikilink")）决定为他们设计一面。亚立珊的第一个设计方案与今天的国旗十分相似，那是一面水平的三色旗。最上面的长条是红色，象征复活节后的第一个星期天（“红色”星期天），中间是绿色的长条，代表复活节的“绿色”星期天，而底部则是任意选择的颜色——白色，以完成三色旗的设计。\[4\]
在法国，亚立珊又拿出了第二个方案，今天被看作“民族主义者亚美尼亚旗”。这个方案同样是三色旗，但是是垂直的三色旗，与[法国国旗类似](../Page/法国国旗.md "wikilink")。它的图案从左到右分别是红、绿、蓝色，象征着[诺亚在](../Page/诺亚.md "wikilink")[亚拉拉特山停靠后看到的彩虹](../Page/亚拉拉特山.md "wikilink")。

### 外高加索民主联邦共和国时期

[Flag_of_the_Transcaucasian_Federation.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Transcaucasian_Federation.svg "fig:Flag_of_the_Transcaucasian_Federation.svg")

1828年，在最后一次[俄波战争后](../Page/俄波战争.md "wikilink")，[波斯属亚美尼亚被并入](../Page/波斯属亚美尼亚.md "wikilink")[俄罗斯帝国的版图](../Page/俄罗斯帝国.md "wikilink")，今天被称作[俄属亚美尼亚](../Page/俄属亚美尼亚.md "wikilink")。在俄罗斯帝国崩溃后，亚美尼亚曾一度宣布独立，与[格鲁吉亚和](../Page/格鲁吉亚.md "wikilink")[阿塞拜疆一起加入短命的](../Page/阿塞拜疆.md "wikilink")[外高加索民主联邦共和国](../Page/外高加索民主联邦共和国.md "wikilink")。这个统一国家维持了不到一年即宣告解体。由于共和国的短命，它不曾使用过任何国旗。然而据历史学家考证，外高加索可能用过一种水平的黃、黑、红的三色旗。\[5\]。联邦在1918年5月26日解体，由于格鲁吉亚宣布独立，成立[格鲁吉亚民主共和国](../Page/格鲁吉亚民主共和国.md "wikilink")。亚美尼亚和阿塞拜疆也于两天后宣布独立，分别成立了[亚美尼亚民主共和国和](../Page/亚美尼亚民主共和国.md "wikilink")[阿塞拜疆民主共和国](../Page/阿塞拜疆民主共和国.md "wikilink")。

### 亚美尼亚民主共和国时期

[Flag_of_the_First_Republic_of_Armenia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_First_Republic_of_Armenia.svg "fig:Flag_of_the_First_Republic_of_Armenia.svg")

独立之后[亚美尼亚民主共和国采用了现代的三色旗](../Page/亚美尼亚民主共和国.md "wikilink")。当史蒂芬·马尔哈相在国家委员会露面后\[6\]，亚美尼亚政府选择了[鲁本王朝国旗的三种颜色红](../Page/鲁本王朝.md "wikilink")、蓝、黄作为国旗的图案，不久黄色又被橙色代替，因为橙色与其他颜色搭配的更好并看起来更令人振奋。独立亚美尼亚国旗的长宽比为3：2，但在1990年8月24日，亚美尼亚最高苏维埃采用原有国旗图案时，长宽比改为2：1\[7\]。

### 早期苏维埃亚美尼亚及外高加索苏维埃社会主义联邦共和国时期

[Flag_of_Transcaucasian_SFSR.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Transcaucasian_SFSR.svg "fig:Flag_of_Transcaucasian_SFSR.svg")

1920年11月29日，亚美尼亚的[布尔什维克建立了](../Page/布尔什维克.md "wikilink")[亚美尼亚苏维埃社会主义共和国](../Page/亚美尼亚苏维埃社会主义共和国.md "wikilink")。一个新的国旗方案被提出，并于1922年2月2日的[亚美尼亚苏维埃社会主义共和国第一届](../Page/亚美尼亚苏维埃社会主义共和国.md "wikilink")[苏维埃大会上获得通过](../Page/苏维埃大会.md "wikilink")\[8\]。这面国旗仅仅使用了一个月，因为3月12日，亚美尼亚与[格鲁吉亚苏维埃社会主义共和国及](../Page/格鲁吉亚苏维埃社会主义共和国.md "wikilink")[阿塞拜疆苏维埃社会主义共和国联合组成了外高加索苏维埃社会主义联邦共和国](../Page/阿塞拜疆苏维埃社会主义共和国.md "wikilink")。1922年12月30日，外高加索苏维埃社会主义联邦共和国成为共同组建[苏联的四个苏维埃共和国之一](../Page/苏联.md "wikilink")。
外高加索苏维埃共和国国旗的图案为一个嵌入镰刀与斧头图案的五角星，下面用[无衬线的字体用俄文字母写着](../Page/无衬线体.md "wikilink")“Z-S-F-S-R”，是“外高加索苏维埃社会主义联邦共和国”（“Zakavkazskaya
Sovetskaya Federativnaya Socialisticheskaya
Respublika”）的缩写\[9\]。1936年，外高加索苏维埃社会主义联邦共和国被重新划分为它原来的3个加盟共和国。

### 亚美尼亚苏维埃社会主义共和国时期

[Flag_of_SSRA.svg](https://zh.wikipedia.org/wiki/File:Flag_of_SSRA.svg "fig:Flag_of_SSRA.svg")
[Flag_of_Armenian_SSR_(1937-1940).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Armenian_SSR_\(1937-1940\).svg "fig:Flag_of_Armenian_SSR_(1937-1940).svg")
[Flag_of_Armenian_SSR_(1940-1952).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Armenian_SSR_\(1940-1952\).svg "fig:Flag_of_Armenian_SSR_(1940-1952).svg")
[Flag_of_Armenian_SSR.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Armenian_SSR.svg "fig:Flag_of_Armenian_SSR.svg")
作为一个[苏维埃社会主义共和国](../Page/苏维埃社会主义共和国.md "wikilink")，亚美尼亚于1936年设计了第一面国旗。与[苏联国旗十分相似](../Page/苏联国旗.md "wikilink")，这面旗子是红色的背景，一角绘有黄色的[锤子与镰刀图案](../Page/锤子与镰刀.md "wikilink")，其下面用亚美尼亚式[衬线体写有大写的字母](../Page/衬线体.md "wikilink")“H-Kh-S-H”，在西亚美尼亚语言中是“Haygagan
Khorhurtayin Sodzialistakan Hanrabedutyun” （亚美尼亚苏维埃社会主义共和国）的缩写。1940年代,
旗子上的字母改为东亚美尼亚语言的缩写“H-S-S-R”，在东亚美尼亚读法中表示“Hayastani Sovetakan
Sotsialistikakan
Respublika”。1952年提出一个新的国旗方案，大写字母被完全去掉而在同样位置替换上一条水平的蓝色条纹。这个国旗图案一直保持到1991年亚美尼亚脱离[苏联独立](../Page/苏联.md "wikilink")，国旗直接被恢复到苏维埃时代之前民主共和国时的图案。这面三色旗先前在亚美尼亚第一次宣布独立之后用于亚美尼亚民族主义运动。

### 歷代國旗一览表

## 国旗的使用

[Moscow,_embassy_of_Armenia_(2).jpg](https://zh.wikipedia.org/wiki/File:Moscow,_embassy_of_Armenia_\(2\).jpg "fig:Moscow,_embassy_of_Armenia_(2).jpg")[大使馆屋顶的国旗](../Page/大使馆.md "wikilink")\]\]
在亚美尼亚关于国旗的法律中规定在下列场所每天都要升国旗：

  - 公共建筑物；
  - 亚美尼亚教区所在地；
  - 亚美尼亚国民大会；
  - 亚美尼亚护宪法院；
  - 代表亚美尼亚的各种办事处。

法律允许普通公民让国旗在自家宅院上空飘扬，但国旗必须高于地面2.5米以上。禁止使用变脏、褪色或失去光泽的国旗。\[10\]

[ArmenianEmbassyWashingtonDC01.jpg](https://zh.wikipedia.org/wiki/File:ArmenianEmbassyWashingtonDC01.jpg "fig:ArmenianEmbassyWashingtonDC01.jpg")亚美尼亚大使馆上空的国旗\]\]

### 法定升国旗日

法律鼓励每天升国旗，但只有下面的日期必需升国旗：

  - 1月1日、1月2日－新年
  - 1月6日－圣诞
  - 3月8日－国际妇女节
  - 4月7日－母亲节
  - 4月24日－亚美尼亚大屠杀纪念日
  - 5月1日－国际劳动节
  - 5月9日－卫国战争胜利日
  - 5月28日－国庆
  - 7月5日－行宪纪念日
  - 9月21日－独立日
  - 12月7日－斯皮塔克大地震纪念日\[11\]\[12\]

## 国旗的影响

1992年6月2日，宣布自治的[纳戈尔诺-卡拉巴赫共和国](../Page/纳戈尔诺-卡拉巴赫共和国.md "wikilink")（今[阿尔札赫共和国](../Page/阿尔札赫共和国.md "wikilink")）采用了一面基于亚美尼亚三色旗设计的旗帜。纳戈尔诺-卡拉巴赫国旗较亚美尼亚三色旗添绘了白色五阶地毯状图案，该图案从右边两角开始向中间延伸并连接于一个点，这个点距右边缘的长度等于国旗全长的三分之一。\[13\]
这个白色图像象征 Artsakh（纳戈尔诺卡拉巴赫）当前与正统亚美尼亚的分离和它对与“祖国”最终统一的渴望。\[14\]
这面旗的长宽比为2：1，与亚美尼亚三色旗相同。\[15\]

除了[阿尔札赫共和国的国旗](../Page/阿尔札赫共和国.md "wikilink")，亚美尼亚三色旗还影响了泛亚美尼亚运动会会旗的图案设计。在这面浅蓝色旗帜的中央是六个连在一起的环。第六个环，即橙色环，与蓝色环和红色环连在一起，象征着亚美尼亚。六环之上绘有一团火焰，火焰由亚美尼亚国旗的三色构成。\[16\]

国旗在米卡耶尔·加扎里·纳尔班甸作词的亚美尼亚的国歌中亦有提及。尤其国歌的第二节及第三节歌咏了国旗的创造:

## 国旗的演变与影响

### 亞美尼亞獨立後國旗時間軸

<table>
<tbody>
<tr class="odd">
<td><table>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_First_Republic_of_Armenia.svg" title="fig:Flag_of_the_First_Republic_of_Armenia.svg">Flag_of_the_First_Republic_of_Armenia.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Transcaucasian_SFSR.svg" title="fig:Flag_of_Transcaucasian_SFSR.svg">Flag_of_Transcaucasian_SFSR.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_SSRA.svg" title="fig:Flag_of_SSRA.svg">Flag_of_SSRA.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Armenian_SSR_(1937-1940).svg" title="fig:Flag_of_Armenian_SSR_(1937-1940).svg">Flag_of_Armenian_SSR_(1937-1940).svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Armenian_SSR_(1940-1952).svg" title="fig:Flag_of_Armenian_SSR_(1940-1952).svg">Flag_of_Armenian_SSR_(1940-1952).svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Armenian_SSR.svg" title="fig:Flag_of_Armenian_SSR.svg">Flag_of_Armenian_SSR.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Armenia.svg" title="fig:Flag_of_Armenia.svg">Flag_of_Armenia.svg</a></p></td>
</tr>
<tr class="even">
<td><p>1918－1920</p></td>
<td><p>1920–1922</p></td>
<td><p>1922–1936</p></td>
<td><p>1936–1940</p></td>
<td><p>1940–1952</p></td>
<td><p>1952–1990</p></td>
<td><p>1990-至今</p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

## 参考文献

## 參見

  - [亚美尼亚国徽](../Page/亚美尼亚国徽.md "wikilink")
  - [亞美尼亞旗幟列表](../Page/亞美尼亞旗幟列表.md "wikilink")

{{-}}

[A](../Category/国旗.md "wikilink")
[Category:亚美尼亚国家象征](../Category/亚美尼亚国家象征.md "wikilink")
[Category:亞美尼亞旗幟](../Category/亞美尼亞旗幟.md "wikilink")
[Category:水平三色旗](../Category/水平三色旗.md "wikilink")

1.  [亚美尼亚国旗的演变](http://www.armenianheritage.com/hiflag.htm)．Armenianheritage.com．於2007年1月5日查阅．

2.  [亚美尼亚旗](http://www.vexilla-mundi.com/armenia_flag.html)．Vexilla
    Mundi．於2007年1月6日查阅．

3.  所列旗帜RGB值由 “Jasc Paint Shop Pro计划”收集。

4.
5.  [Закавказская
    Федерация（外高加索民主联邦共和国）](http://hrono.rspu.ryazan.ru/heraldicum/flagi/gruzia/federat.htm)
    （[俄文](../Page/俄文.md "wikilink")）．俄罗斯旗帜学及纹章学中心（2003年5月30日）．於2006年12月27日查阅．

6.  [Республика
    Армения（亚美尼亚民主共和国）](http://hrono.rspu.ryazan.ru/heraldicum/flagi/armenia/ar.htm)
    （俄语）．俄罗斯旗帜学及纹章学中心．於2006年1月9日查阅．

7.  [亚美尼亚：第一共和国(1918–1921)](http://www.fotw.net/flags/am_.html#1918)
    ．世界各国国旗．於2007年1月9日查阅．

8.  [a b Cоветская Армения
    (苏维埃亚美尼亚)](http://www.hrono.ru/heraldicum/flagi/armenia/assr.htm)（[俄语](../Page/俄语.md "wikilink")）。俄罗斯旗帜学及纹章学中心，於2007年1月20日查阅。

9.
10. [亚美尼亚旗](http://www.fotw.net/flags/am.html)．世界国旗之页．於2006年12月29日查阅．

11. [世界各国国旗日
    世界各国国旗网](http://www.crwflags.com/fotw/flags/fdw.html)．於2006年12月20日查阅．

12. [关于亚美尼亚](http://un.cti.depaul.edu/public/Armenia/1/English)
    ．亚美尼亚驻联合国使团网页．於2006年12月29日查阅．

13. [纳戈尔诺-卡拉巴赫的国旗、国徽、国歌](http://www.nkr.am/eng/gov/atributy.html)
    ．纳戈尔诺卡拉巴赫外交部．於2007年1月9日查阅．

14. [纳戈尔诺卡拉巴赫国旗](http://www.crwflags.com/fotw/flags/az-artsa.html)．世界各国国旗网．於2007年1月9日查阅．

15.
16. [亚美尼亚：运动会会旗](http://www.crwflags.com/fotw/flags/am@.html)．世界各国国旗网．於2007年1月9日查阅．