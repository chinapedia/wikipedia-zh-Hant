**亞美尼亞語**（或**Hayerēn**）是[亞美尼亞共和國及不被普遍承认的](../Page/亞美尼亞共和國.md "wikilink")[納戈爾諾-卡拉巴赫共和國的官方语言](../Page/納戈爾諾-卡拉巴赫共和國.md "wikilink")，屬於[印欧语系的一支](../Page/印欧语系.md "wikilink")，但它沒有任何近似語言，其38個字母的拼音系統更是獨特的，不少語言學家因此對亞美尼亞語產生興趣。\[1\]
亞美尼亞語目前廣泛被[亞美尼亞人使用](../Page/亞美尼亞人.md "wikilink")。

語言學家將[亞美尼亞語歸為](../Page/亞美尼亞語.md "wikilink")[印歐語系的一個獨立語族](../Page/印歐語系.md "wikilink")。亞美尼亞語與[希臘語有一些共有衍徵](../Page/希臘語.md "wikilink")。部分語言學家將[亞美尼亞語](../Page/亞美尼亞語.md "wikilink")、[弗里吉亞語](../Page/弗里吉亞語.md "wikilink")（Phrygian）及[印度－伊朗語族歸類於](../Page/印度－伊朗語族.md "wikilink")[印歐語系之下](../Page/印歐語系.md "wikilink")，因為它們有一些共有衍徵。最近亦有一些語言學家提出將[希臘語](../Page/希臘語.md "wikilink")、[亞美尼亞語](../Page/亞美尼亞語.md "wikilink")、[弗里吉亞語](../Page/弗里吉亞語.md "wikilink")（Phrygian）及[阿爾巴尼亞語歸入](../Page/阿爾巴尼亞語.md "wikilink")[印歐語系下的一個](../Page/印歐語系.md "wikilink")「[巴爾幹語族](../Page/巴爾幹語族.md "wikilink")」（Balkan）。

亞美尼亞語有一個漫長的文學史，其現存最古老的文學作品是五世紀的[聖經翻譯](../Page/聖經.md "wikilink")。它的詞彙在其歷史上受[伊朗的語言影響](../Page/伊朗.md "wikilink")，特別是帕提亞語（Pathian），此外還受[希臘語](../Page/希臘語.md "wikilink")，[拉丁語](../Page/拉丁語.md "wikilink")，[古法語](../Page/古法語.md "wikilink")，[波斯語](../Page/波斯語.md "wikilink")，[阿拉伯語](../Page/阿拉伯語.md "wikilink")，[土耳其語和其他語言影響](../Page/土耳其語.md "wikilink")。現代亞美尼亞語有兩種規範的形式，東亞美尼亞語和西亞美尼亞語，兩者都可以用於理解當代亞美尼亞方言。幾乎絕跡的Lomavren語是一個深受[羅姆語](../Page/羅姆語.md "wikilink")（吉普賽語）影響的方言，使用亞美尼亞語的語法及大量由羅姆語演變而來的詞彙，例如數字。

## 語音系統

### 元音

現代亞美尼亞語有6個單元音。

<table>
<thead>
<tr class="header">
<th></th>
<th><p><a href="../Page/前元音.md" title="wikilink">前元音</a></p></th>
<th><p><a href="../Page/央元音.md" title="wikilink">央元音</a></p></th>
<th><p><a href="../Page/後元音.md" title="wikilink">後元音</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/展脣元音.md" title="wikilink">展脣元音</a></p></td>
<td><p><a href="../Page/圓唇元音.md" title="wikilink">圓唇元音</a></p></td>
<td><p><a href="../Page/展脣元音.md" title="wikilink">展脣元音</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/閉元音.md" title="wikilink">閉元音</a></p></td>
<td><p><br />
<strong></strong><br />
i</p></td>
<td></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中元音.md" title="wikilink">中元音</a></p></td>
<td><p><br />
<strong>, </strong><br />
e, ē</p></td>
<td></td>
<td><p><br />
<strong><br />
</strong> ë</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/開元音.md" title="wikilink">開元音</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table>

### 辅音

<table>
<caption>东亚美尼亚辅音表[2]</caption>
<thead>
<tr class="header">
<th></th>
<th><p><a href="../Page/鼻音_(辅音).md" title="wikilink">鼻音</a></p></th>
<th><p><a href="../Page/塞音.md" title="wikilink">塞音</a>/<a href="../Page/塞擦音.md" title="wikilink">塞擦音</a></p></th>
<th><p><a href="../Page/擦音.md" title="wikilink">擦音</a></p></th>
<th><p><a href="../Page/近音.md" title="wikilink">近音</a>/<a href="../Page/边音.md" title="wikilink">边音</a>/<a href="../Page/颤音.md" title="wikilink">颤音</a>/<a href="../Page/弹音.md" title="wikilink">弹音</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/唇音.md" title="wikilink">唇音</a></p></td>
<td><p><br />
<strong></strong></p></td>
<td><p>, , <br />
<strong></strong>, <strong></strong>, <strong></strong></p></td>
<td><p>, <br />
<strong></strong>, <strong></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/舌尖音.md" title="wikilink">舌尖音</a></p></td>
<td><p><br />
<strong></strong></p></td>
<td><p>, ,  '''/ ''' , , <br />
<strong></strong>, <strong></strong>, <strong></strong> / <strong></strong>, <strong></strong>, <strong></strong></p></td>
<td><p><br />
<strong></strong>, <strong></strong></p></td>
<td><p>//<br />
<strong></strong> / ''' ''' / ''' '''</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/舌面音.md" title="wikilink">舌面音</a>~<a href="../Page/硬腭音.md" title="wikilink">硬腭音</a></p></td>
<td></td>
<td><p>, , <br />
<strong></strong>, <strong></strong>, <strong></strong></p></td>
<td><p>, <br />
<strong></strong>, <strong></strong></p></td>
<td><p><br />
<strong></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/软腭音.md" title="wikilink">软腭音</a>~<a href="../Page/声门音.md" title="wikilink">声门音</a></p></td>
<td></td>
<td><p><br />
<strong></strong>, <strong></strong>, <strong></strong></p></td>
<td><p>~~<br />
<strong></strong>, <strong></strong></p></td>
<td><p><br />
<strong></strong></p></td>
</tr>
</tbody>
</table>

## 文字

[Keyboard_Layout_Armenian.png](https://zh.wikipedia.org/wiki/File:Keyboard_Layout_Armenian.png "fig:Keyboard_Layout_Armenian.png")的亚美尼亚[键盘布局](../Page/键盘.md "wikilink")。\]\]
亚美尼亚字母表（）是用来书写亚美尼亚语的一种独有[字母文字](../Page/字母系统.md "wikilink")，由亚美尼亚语言学家及神学家[圣梅斯罗布在约公元](../Page/圣梅斯罗布.md "wikilink")405年发明。其最早包含36个字母。在中世纪两个字母օ
(o) and ֆ (f)被加入字母表。

在1920年代的书写改革中，引入了新字母և（大写ԵՎ），它是ե与ւ的[连体字母](../Page/连体字母.md "wikilink")。而字母Ւ
ւ被取消，并作为新字母ՈՒ ու(在此之前为一个[二合字母](../Page/二合字母.md "wikilink"))的一部分。

## 參看

  - [ARMSCII](../Page/ARMSCII.md "wikilink")（亞美尼亞語字符編碼）

## 參考文獻

### 引用

### 来源

  - Adjarian, Herchyah H. (1909) *Classification des dialectes
    arméniens, par H. Adjarian.* Paris: Honoro Champion.
  - Clackson, James. 1994. *The Linguistic Relationship Between Armenian
    and Greek.* London: Publications of the Philological Society, No 30.
    (and Oxford: Blackwell Publishing)
  - Fortson, Benjamin W. (2004) *Indo-European Language and Culture.*
    Oxford: Blackwell Publishing.
  - Hübschmann, Heinrich (1875) "Über die Stellung des armenischen im
    Kreise der indogermanischen Sprachen," *Zeitschrift für
    Vergleichende Sprachforschung* 23.5-42. [English
    translation](https://web.archive.org/web/20051221170107/http://www.utexas.edu/cola/depts/lrc/iedocctr/ie-docs/lehmann/reader/Chapter12.html)
  - Mallory, J. P. (1989) *In Search of the Indo-Europeans: Language,
    Archaeology and Myth.* London: Thames & Hudson.
  - Vaux, Bert. 1998. *The Phonology of Armenian.* Oxford: Clarendon
    Press.
  - Vaux, Bert. 2002. "The Armenian dialect of Jeruslame." in Armenians
    in the Holy Land. "Louvain: Peters.

## 外部链接

  - [Free online resources for
    learners](https://web.archive.org/web/20040103183050/http://www.sprachprofi.de.vu/english/arm.htm)
  - <https://web.archive.org/web/20040512100147/http://www.ethnologue.com/show_iso639.asp?code=hye>
  - <http://www.cilicia.com>
  - [亚美尼亚语字母](http://www.omniglot.com/writing/armenian.htm)

{{-}}

[Category:印欧语系](../Category/印欧语系.md "wikilink")
[亚美尼亚语](../Category/亚美尼亚语.md "wikilink")

1.  David Maybury-Lewis, Peoples of the World : Their Cultures,
    Traditions, and Ways of Life ,National Geographic (November 1, 2001)
2.