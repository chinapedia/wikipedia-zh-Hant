**亚非语系**，又称**非亞語系**、**非洲－亞洲語系**或**阿非罗－亚细亚语系**，旧称**闪含语系**或**閃米特－含米特語系**\[1\]\[2\]，是现今世界的主要[语系之一](../Page/语系.md "wikilink")，包含约300种语言\[3\]，主要分布在[亚洲西部的](../Page/亚洲.md "wikilink")[阿拉伯半岛](../Page/阿拉伯半岛.md "wikilink")、[非洲北部和](../Page/非洲.md "wikilink")[非洲之角](../Page/非洲之角.md "wikilink")。全球约有4.95亿人母语为亚非语系语言，使之成为世界上母语人口第四多的语系，位在[印欧语系](../Page/印欧语系.md "wikilink")、[汉藏语系和](../Page/汉藏语系.md "wikilink")[尼日尔-刚果语系之后](../Page/尼日尔-刚果语系.md "wikilink")\[4\]。

亚非语系包括六个不同的[语族](../Page/语族.md "wikilink")：[柏柏尔语族](../Page/柏柏尔语族.md "wikilink")、[乍得语族](../Page/乍得语族.md "wikilink")、[闪米特语族](../Page/闪米特语族.md "wikilink")、[库希特语族](../Page/库希特语族.md "wikilink")、[奥摩语族和已灭绝的](../Page/奥摩语族.md "wikilink")[埃及语族](../Page/埃及语.md "wikilink")。亚非语系中使用人数最多的语言是[现代标准阿拉伯语](../Page/现代标准阿拉伯语.md "wikilink")，而如果包括所有[阿拉伯语变体的母语人口在内](../Page/阿拉伯语变体.md "wikilink")，亚非语系的“[阿拉伯语](../Page/阿拉伯语.md "wikilink")”共有2000万人作为母语，广泛分布在西亚、北非和东非地区\[5\]。其他广泛使用的亚非语系语言还包括[豪萨语](../Page/豪萨语.md "wikilink")\[6\]、[奥罗莫语](../Page/奥罗莫语.md "wikilink")、[阿姆哈拉语](../Page/阿姆哈拉语.md "wikilink")、[索马里语](../Page/索马里语.md "wikilink")、[希伯来语](../Page/希伯来语.md "wikilink")\[7\]、[提格里尼亚语](../Page/提格里尼亚语.md "wikilink")、[卡拜尔语](../Page/卡拜尔语.md "wikilink")、[亚拉姆语](../Page/亚拉姆语.md "wikilink")\[8\]\[9\]和[中阿特拉斯柏柏尔语](../Page/中阿特拉斯柏柏尔语.md "wikilink")\[10\]。

除上述的现存主要语言之外，许多具有极大历史价值的[已灭绝语言也属于亚非语系](../Page/语言灭绝.md "wikilink")，如[埃及语](../Page/埃及语.md "wikilink")、[阿卡德语](../Page/阿卡德语.md "wikilink")、[圣经希伯来语和](../Page/圣经希伯来语.md "wikilink")[古亚拉姆语等](../Page/古亚拉姆语.md "wikilink")。

## 历史

亚非语系旧称闪含语系或閃米特-含米特語系，是从《[圣经](../Page/圣经.md "wikilink")》的传说中，[挪亚的两个儿子的名字来源的](../Page/挪亚.md "wikilink")，圣经说他的儿子[闪是](../Page/闪姆.md "wikilink")[希伯来人和](../Page/希伯来人.md "wikilink")[亚述人的祖先](../Page/亚述人.md "wikilink")，另一个儿子[含是非洲人的祖先](../Page/含.md "wikilink")\[11\]。

## 特征

亚非语系主要的共同特征是：

  - 是一种[屈折语](../Page/屈折语.md "wikilink")
  - 辅音除了有[清辅音和](../Page/清音.md "wikilink")[浊辅音外](../Page/浊音.md "wikilink")，还有一种[重辅音](../Page/重辅音.md "wikilink")，是在口腔后部和喉腔形成的
  - [动词有人称](../Page/动词.md "wikilink")[前缀](../Page/前缀.md "wikilink")
  - 有[格和](../Page/格_\(语法\).md "wikilink")[性的区别](../Page/性_\(语法\).md "wikilink")，但比[印欧语系简单](../Page/印欧语系.md "wikilink")
  - [词根基本由辅音组成](../Page/词根.md "wikilink")

## 分支

其分支为

  - [柏柏尔语族](../Page/柏柏尔语族.md "wikilink")
      - [东柏柏尔语支](../Page/东柏柏尔语支.md "wikilink")
      - [古安切语支](../Page/古安切语支.md "wikilink")（Guanche，位於西班牙在非洲的領土[加那利群岛](../Page/加那利群岛.md "wikilink")）
      - [北柏柏尔语支](../Page/北柏柏尔语支.md "wikilink")
          - [塔马塞特语](../Page/塔马塞特语.md "wikilink") ()
          - [卡比勒语](../Page/卡比勒语.md "wikilink") ()
      - 塔马舍克语支（圖阿雷格語支）
          - [塔马舍克语](../Page/塔马舍克语.md "wikilink") ()
      - [哲纳加语](../Page/哲纳加语.md "wikilink") ()
  - [乍得语族](../Page/乍得语族.md "wikilink")
      - [比乌-曼达拉语支](../Page/比乌-曼达拉语支.md "wikilink")
      - [东乍得语支](../Page/东乍得语支.md "wikilink")
      - [马萨语支](../Page/马萨语支.md "wikilink")（Masa）
      - [西乍得语支](../Page/西乍得语支.md "wikilink")
          - [豪萨语](../Page/豪萨语.md "wikilink") ()
  - [库希特语族](../Page/库希特语族.md "wikilink")
      - [中库希特语支](../Page/中库希特语支.md "wikilink")
          - [比林语](../Page/比林语.md "wikilink") ()
      - [东库希特语支](../Page/东库希特语支.md "wikilink")
          - [锡达莫语](../Page/锡达莫语.md "wikilink") ()
          - [奥罗莫语](../Page/奥罗莫语.md "wikilink") ()
          - [阿法语](../Page/阿法语.md "wikilink") ()
          - [萨霍语](../Page/萨霍语.md "wikilink") ()
          - [索馬里語](../Page/索馬里語.md "wikilink") ()
      - [北库希特语支](../Page/北库希特语支.md "wikilink")
          - [贝扎语](../Page/贝扎语.md "wikilink")、[贝贾语](../Page/贝贾语.md "wikilink")
            ()
      - [南库希特语支](../Page/南库希特语支.md "wikilink")
  - [埃及语族](../Page/埃及语族.md "wikilink")
      - [埃及语](../Page/埃及语.md "wikilink")（已灭绝）()
      - [科普特语](../Page/科普特语.md "wikilink")()
  - [奥摩语族](../Page/奥摩语族.md "wikilink")
      - [北奥摩语支](../Page/北奥摩语支.md "wikilink")
          - [瓦拉莫语](../Page/瓦拉莫语.md "wikilink") ()
      - [南奥摩语支](../Page/南奥摩语支.md "wikilink")
  - [闪米特语族](../Page/闪米特语族.md "wikilink")
      - [东闪米特语支](../Page/东闪米特语支.md "wikilink")
          - [阿卡德语](../Page/阿卡德语.md "wikilink")（已灭绝）()
      - [中闪米特语支](../Page/中闪米特语支.md "wikilink")
          - [亚拉姆语](../Page/亚拉姆语.md "wikilink") ()
          - [图罗尤语](../Page/图罗尤语.md "wikilink") ()
          - [曼底安语](../Page/曼底安语.md "wikilink") ()
          - [古典叙利亚语](../Page/叙利亚语.md "wikilink") ()
          - [阿拉伯语](../Page/阿拉伯语.md "wikilink") ()
          - [马耳他语](../Page/马耳他语.md "wikilink") ()
          - [古希伯来语](../Page/古希伯来语.md "wikilink") ()
          - [希伯来语](../Page/希伯来语.md "wikilink") ()
          - [撒马利亚语](../Page/撒马利亚语.md "wikilink")（已灭绝）()
      - [南闪米特语支](../Page/南闪米特语支.md "wikilink")
          - [吉兹语](../Page/吉兹语.md "wikilink") ()
          - [提格雷语](../Page/提格雷语.md "wikilink") ()
          - [提格利尼亚语](../Page/提格利尼亚语.md "wikilink") ()
          - [阿姆哈拉语](../Page/阿姆哈拉语.md "wikilink") ()
          - [哈勒尔语](../Page/哈勒尔语.md "wikilink") ()
  - 未分类
      - Birale ()

## 參看

  - [閃姆](../Page/閃姆.md "wikilink")
  - [含姆](../Page/含姆.md "wikilink")
  - [閃米特人](../Page/閃米特人.md "wikilink")
  - [含米特人種](../Page/含米特人種.md "wikilink")

## 參考文獻

## 外部链接

  - <http://www.ethnologue.com/show_family.asp?subid=89997>

[Category:語系](../Category/語系.md "wikilink")
[亞非語系](../Category/亞非語系.md "wikilink")

1.
2.  Robert Hetzron, "Afroasiatic Languages" in Bernard Comrie, *The
    World's Major Languages*, 2009, , p. 545
3.
4.
5.
6.
7.
8.  Nordhoff, Sebastian; Hammarström, Harald; Forkel, Robert;
    Haspelmath, Martin, eds. (2013). "Northeastern Neo-Aramaic".
    Glottolog 2.2. Leipzig: Max Planck Institute for Evolutionary
    Anthropology.
9.  Beyer, Klaus; John F. Healey (trans.) (1986). The Aramaic Language:
    its distribution and subdivisions. Göttingen: Vandenhoeck und
    Ruprecht. p. 44. .
10.
11.