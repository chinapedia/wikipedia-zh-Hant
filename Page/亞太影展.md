**亞太影展**（）是[亞太電影製片人協會的年度活動](../Page/亞太電影製片人協會.md "wikilink")，聯盟常設秘書處所在地是[台灣](../Page/台灣.md "wikilink")。1968年和1981年曾因故停辦影展\[1\]\[2\]。亞太影展因為資金問題，於2007、2008及2011年停辦。

2018年，亞洲太平洋電影製片人聯盟 ( Federation of Motion Picture Producers in
Asia-Pacific ,簡稱FPA )
3月25日在台中日月千禧酒店舉辦國際理事高峰會，由各國代表理事出席，正式宣告第58屆亞太影展將由台灣承辦，並且一致推崇由台灣資深電影導演李祐寧擔任第58屆主席。\[3\]

「第58屆亞太影展」親善大使，敲定由張鈞甯擔任，她希望透過這次在台灣舉辦國際電影盛事，讓電影人支持電影產業，也希望以地主身分，讓亞太地區眾多電影人，更加認識台灣、看見台灣電影人所拍攝的電影。\[4\]

## 歷史

<span class="s1"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span><span class="s4"></span>
<span class="s1"></span>

  - 1953年，[大映董事長永田雅一為拓展](../Page/大映.md "wikilink")[東南亞市場而計劃舉辦一個定期影展](../Page/東南亞.md "wikilink")，於是邀請[邵逸夫等人聯手創辦](../Page/邵逸夫.md "wikilink")「東南亞電影製片人協會」；當時成員有[日本](../Page/日本.md "wikilink")、[中華民國](../Page/中華民國.md "wikilink")、[英屬香港](../Page/英屬香港.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[菲律賓及](../Page/菲律賓.md "wikilink")[泰國](../Page/泰國.md "wikilink")。<span class="s1"></span>
      - <span class="s4"></span> 第1屆「東南亞影展-金禾獎1954年5月8日至20日
        ，」於[東京舉行](../Page/東京.md "wikilink")，型態為競賽型影展。
      - <span class="s4"></span>第2屆「東南亞影展-金獅獎」1955年5月14日至21日，於[新加坡舉行](../Page/新加坡.md "wikilink")。
      - <span class="s4"></span>第3屆「東南亞影展-金球獎」1956年6月20日，在[香港舉行](../Page/香港.md "wikilink")。
      - <span class="s4"></span>第4屆「亞洲影展」1957年5月，於日本(第二次)舉行，因成員國擴展至[東南亞以外國家](../Page/東南亞.md "wikilink")，固更名為「亞洲影展」。
      - <span class="s4"></span>第5屆「亞洲影展-金龍獎」1958年4月26日，第五屆於[馬尼拉舉行](../Page/马尼拉.md "wikilink")。
      - <span class="s4"></span>第6屆「亞洲影展-金鑼獎」1959年5月3日-8日於[馬來西亞](../Page/马来西亚.md "wikilink")[吉隆坡舉辦](../Page/吉隆坡.md "wikilink")。
      - <span class="s4"></span>第7屆「亞洲影展-金鑼獎」1960年4月5日-9日於[東京](../Page/东京.md "wikilink")(第三次)舉行。
      - <span class="s4"></span>第8屆「亞洲影展」1961年3月7日-11日於[馬尼拉舉行](../Page/马尼拉.md "wikilink")。
      - <span class="s4"></span>第9屆「亞洲影展」1962年5月12日-16日於[韓國](../Page/大韩民国.md "wikilink")[漢城舉行](../Page/首爾.md "wikilink")。
      - <span class="s4"></span>第10屆「亞洲影展-金禾獎」1963年4月15日-16日於日本(第四次)[東京舉行](../Page/东京.md "wikilink")。
      - <span class="s4"></span>第11屆「亞洲影展」1964年6月15日-19日於[台灣台北](../Page/臺灣.md "wikilink")(第一次)舉行。
      - <span class="s4"></span>第12屆「亞洲影展-金禾獎」1965年5月8日-14日於[日本](../Page/日本.md "wikilink")(第五次)[京都舉行](../Page/京都市.md "wikilink")。
      - <span class="s4"></span>第13屆「亞洲影展-金禾獎」1966年5月5日-9日於[韓國](../Page/大韩民国.md "wikilink")[漢城舉行](../Page/首爾.md "wikilink")。
      - <span class="s4"></span>第14屆「亞洲影展-金禾獎」1967年9月27日-10月3日於日本(第六次)[東京舉行](../Page/东京.md "wikilink")。
      - <span class="s4"></span>1968年停辦一年。
      - <span class="s4"></span>第15屆「亞洲影展-金禾獎」1969年6月14日-6月20日於[菲律賓](../Page/菲律宾.md "wikilink")[馬尼拉舉行](../Page/马尼拉.md "wikilink")。
      - <span class="s4"></span>第16屆「亞洲影展-金禾獎」1970年6月14日-6月18日於[印尼](../Page/印度尼西亚.md "wikilink")[雅加達舉行](../Page/雅加达.md "wikilink")。
      - <span class="s4"></span>第17屆「亞洲影展-金禾獎」1971年6月5日-6月9日於[台灣](../Page/臺灣.md "wikilink")(第二次)台北舉行。
      - <span class="s4"></span>第18屆「亞洲影展」1972年5月17日-21日於[韓國](../Page/大韩民国.md "wikilink")[漢城舉行](../Page/首爾.md "wikilink")，改為觀摩型影展，以聯誼性質為主。
      - <span class="s4"></span>第19屆「亞洲影展」1973年5月14日-18日於[新加坡舉行亞洲影展](../Page/新加坡.md "wikilink")。
      - <span class="s4"></span>第20屆「亞洲影展」1974年6月11日-15日於[台灣](../Page/臺灣.md "wikilink")(第三次)台北舉行，更創下影展記錄，共頒出高達77個獎項<span class="s4"></span>。
      - <span class="s4"></span>第21屆亞洲影展1975年6月12日-15日於[雅加達舉行](../Page/雅加达.md "wikilink")<span class="s4"></span>。
      - <span class="s4"></span>第22屆亞洲影展1976年6月15日-17日於[韓國](../Page/大韩民国.md "wikilink")[釜山舉行](../Page/釜山廣域市.md "wikilink")。
      - <span class="s4"></span>第23屆亞洲影展1977年11月18日-25日於[泰國](../Page/泰国.md "wikilink")[曼谷舉行](../Page/曼谷.md "wikilink")
        ，[台灣由於政治原因而未參加](../Page/臺灣.md "wikilink")，大會決議自第24屆開始改以各國首都或電影城市的名義參加活動。
      - <span class="s4"></span>第24屆亞洲影展1978年10月2日-6日於[澳洲](../Page/澳大利亚.md "wikilink")[雪梨舉行](../Page/雪梨_\(城市\).md "wikilink")。
      - <span class="s4"></span>第25屆亞洲影展1979年7月3日-6日於[新加坡舉行](../Page/新加坡.md "wikilink")。
      - <span class="s4"></span>第26屆亞洲影展1980年6月28日-7月2日於[印尼](../Page/印度尼西亚.md "wikilink")[日惹與](../Page/日惹.md "wikilink")[巴厘島舉行](../Page/巴厘岛.md "wikilink")。
      - <span class="s4"></span>1981年停辦一年。
      - <span class="s4"></span>第27屆亞洲影展恢復競賽型式，1982年9月11日-14日於[馬來西亞](../Page/马来西亚.md "wikilink")[吉隆坡舉行](../Page/吉隆坡.md "wikilink")。
      - <span class="s4"></span>第28屆亞洲影展，1983年11月於台北(第四次)舉行，改回競賽型影展，並加入[澳洲](../Page/澳洲.md "wikilink")、[紐西蘭兩國](../Page/紐西蘭.md "wikilink")，故重新定名為「亞太影展」；當年10月製作了主題曲〈亞太頌〉，孫儀作詞、[劉家昌作曲](../Page/劉家昌.md "wikilink")、[林家慶編曲](../Page/林家慶_\(音樂人\).md "wikilink")，[國防部藝術工作總隊音樂官蔡淑慎中文主唱](../Page/國防部藝術工作總隊.md "wikilink")，[淡江中學校友李佩蒂](../Page/淡江中學.md "wikilink")（本名李筱貞）英文主唱，[國光合唱團和音](../Page/國光合唱團.md "wikilink")，[中視大樂隊伴奏](../Page/中視大樂隊.md "wikilink")[<sup>\[3\]</sup>](../Page/亞太影展#cite_note-3.md "wikilink")。
      - <span class="s4"></span>第29屆亞太影展，1984年12月於[泰國](../Page/泰国.md "wikilink")[曼谷舉行](../Page/曼谷.md "wikilink")。
      - <span class="s4"></span>第30屆亞太影展，1985年7月於[日本](../Page/日本.md "wikilink")(第七次)[東京舉行](../Page/东京.md "wikilink")。
      - <span class="s4"></span>第31屆亞太影展，1986年9月於[韓國](../Page/大韩民国.md "wikilink")[漢城舉行](../Page/首爾.md "wikilink")。
      - <span class="s4"></span>第32屆亞太影展，1987年10月於[台灣](../Page/臺灣.md "wikilink")(第五次)[台北舉行](../Page/臺北.md "wikilink")。
      - <span class="s4"></span>第33屆亞太影展，1988年於[泰國](../Page/泰国.md "wikilink")[普吉島舉行](../Page/普吉島.md "wikilink")。
      - <span class="s4"></span>第34屆亞太影展，1989年於[印尼](../Page/印度尼西亚.md "wikilink")[雅加達舉行](../Page/雅加达.md "wikilink")。
      - <span class="s4"></span>第35屆亞太影展，1990年於[菲律賓](../Page/菲律宾.md "wikilink")[馬尼拉舉行](../Page/马尼拉.md "wikilink")。
      - <span class="s4"></span>第36屆亞太影展，1991年於[台灣](../Page/臺灣.md "wikilink")(第六次)[台北舉行](../Page/臺北.md "wikilink")。
      - <span class="s4"></span>第37屆亞太影展，1992年於[韓國](../Page/大韩民国.md "wikilink")[漢城舉行](../Page/首爾.md "wikilink")。
      - <span class="s4"></span>第38屆亞太影展，1993年9月於[日本](../Page/日本.md "wikilink")(第八次)[福岡舉行](../Page/福岡市.md "wikilink")。
      - <span class="s4"></span>第39屆亞太影展，1994年9月於[澳洲](../Page/澳大利亚.md "wikilink")[雪梨舉行](../Page/雪梨.md "wikilink")。
      - <span class="s4"></span>第40屆亞太影展，1995年7月於[印尼](../Page/印度尼西亚.md "wikilink")[雅加達舉行](../Page/雅加达.md "wikilink")。
      - <span class="s4"></span>第41屆亞太影展，1996年於[紐西蘭](../Page/新西兰.md "wikilink")[奧克蘭舉行](../Page/奧克蘭_\(紐西蘭\).md "wikilink")。
      - <span class="s4"></span>第42屆亞太影展，1997年10月於[韓國](../Page/大韩民国.md "wikilink")[濟州島舉行](../Page/济州岛.md "wikilink")。
      - <span class="s4"></span>第43屆亞太影展，1998年於[台灣](../Page/臺灣.md "wikilink")(第七次)[台北舉行](../Page/臺北.md "wikilink")。
      - <span class="s4"></span>第44屆亞太影展，1999年於[泰國](../Page/泰国.md "wikilink")[曼谷舉行](../Page/曼谷.md "wikilink")。
      - <span class="s4"></span>第45屆亞太影展，2000年於[越南](../Page/越南.md "wikilink")[河內舉行](../Page/河內市.md "wikilink")。
      - <span class="s4"></span>第46屆亞太影展，2001年於[印度](../Page/印度.md "wikilink")[雅加達](../Page/雅加达.md "wikilink")。
      - <span class="s4"></span>第47屆亞太影展，2002年於[韓國](../Page/大韩民国.md "wikilink")[漢城舉行](../Page/首爾.md "wikilink")。
      - <span class="s4"></span>第48屆亞太影展，2003年10月20-23日於[伊朗設拉子舉行](../Page/伊朗.md "wikilink")。
      - <span class="s4"></span>第49屆亞太影展，2004年9月24日於[日本](../Page/日本.md "wikilink")(第九次)[福岡舉行](../Page/福岡市.md "wikilink")。
      - <span class="s4"></span>第50屆亞太影展，2005年10月2日於[馬來西亞](../Page/马来西亚.md "wikilink")[吉隆坡舉行](../Page/吉隆坡.md "wikilink")。
      - <span class="s4"></span>第51屆亞太影展，2006年11月24日於[台灣](../Page/臺灣.md "wikilink")(第八次)[台北舉行](../Page/臺北.md "wikilink")。
      - <span class="s4"></span>亞太影展因為資金問題，於2007、2008(52屆亞太影展)停辦2年。
      - <span class="s4"></span>第53屆亞太影展，2009年12月19日於[台灣](../Page/臺灣.md "wikilink")(第九次)[高雄舉行](../Page/高雄市.md "wikilink")。
      - <span class="s4"></span>第54屆亞太影展，2010年12月4日於[台灣](../Page/臺灣.md "wikilink")(第十次)[台北舉行](../Page/臺北.md "wikilink")。
      - <span class="s4"></span>亞太影展因為資金問題，於2011停辦一年。
      - <span class="s4"></span>第55屆亞太影展，2012年12月15日於[澳門舉行](../Page/澳門.md "wikilink")。
      - <span class="s4"></span>第56屆亞太影展，2013年11月29日於[澳門舉行](../Page/澳門.md "wikilink")。
      - <span class="s4"></span>亞太影展因為資金問題，於2014–2016停辦三年。
      - <span class="s4"></span>第57屆亞太影展，2017年7月30日於[柬埔寨](../Page/柬埔寨.md "wikilink")[金邊舉行](../Page/金边.md "wikilink")。
      - <span class="s4"></span>第58屆亞太影展，2018年9月1日於[中華民國](../Page/中華民國.md "wikilink")([臺灣](../Page/臺灣.md "wikilink"))[台北](../Page/臺北.md "wikilink")(第11次)舉行，並統整亞太65年來重點訊息，並設立專屬於亞太影展的官方網站<span class="s1"></span><span class="s4"></span>。
      - <span class="s4"></span>第59屆亞太影展，2019年將於[澳洲舉辦](../Page/澳洲.md "wikilink")。

## 會員

參與國家和地區：

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 獎項

【競賽獎項】

  - 最佳男配角
  - 最佳化妝
  - 最佳服裝設計
  - 最佳紀錄片
  - 最佳音效
  - 最佳美術設計
  - 最佳女配角
  - 最佳剪輯
  - 最佳劇本
  - 最佳短片
  - 最佳動畫
  - 最佳特效
  - 最佳攝影
  - 最佳配樂
  - 最佳電影歌曲
  - 最佳新媒體影片
  - 最佳新人
  - 最佳導演
  - 最佳男主角
  - 最佳女主角
  - 最佳影片

【特殊獎項】

  - 傑出貢獻獎
  - 終身成就獎

## 註釋及參考文獻

  - 參考來源

<!-- end list -->

  - 參考文獻

<!-- end list -->

  -
## 歷屆影展

  - [第54屆亞太影展](../Page/第54屆亞太影展.md "wikilink")
  - [第56屆亞太影展](../Page/第56屆亞太影展.md "wikilink")
  - [第58屆亞太影展](../Page/第58屆亞太影展.md "wikilink")

## 外部連結

  - [亞太影展官方網站](https://www.apff.film/)

  -
  -
  -
[亞太影展](../Category/亞太影展.md "wikilink")
[Category:亞洲電影獎項](../Category/亞洲電影獎項.md "wikilink")
[Category:1954年建立](../Category/1954年建立.md "wikilink")

1.
2.
3.
4.