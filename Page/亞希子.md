**亞希子**（、），[日本女](../Page/日本.md "wikilink")[演員](../Page/演員.md "wikilink")，前[SDN48成員之一](../Page/SDN48.md "wikilink")。原名是**大木
亞紀子**（，日語中的「」音和「」同樣發為「ki」音）。同為女演員的[奈津子是其](../Page/奈津子.md "wikilink")[雙胞胎姊姊](../Page/雙胞胎.md "wikilink")。

曾先後為[研音及](../Page/研音.md "wikilink")[Dig-esT事務所屬下的藝人](../Page/Dig-esT.md "wikilink")，目前為自由身。

## 簡歷

  - 首部演出的作品是2004年的『[唐澤壽明](../Page/唐澤壽明.md "wikilink") Presents 記憶的力量
    II』。
  - 與其雙胞胎姊姊[奈津子比較](../Page/奈津子.md "wikilink")，她飾演較多溫和文靜的角色。
  - 喜歡閱讀書籍。[寫作](../Page/寫作.md "wikilink")、打籃球、長跑是她的特長。
  - 曾就讀於[千葉縣](../Page/千葉縣.md "wikilink")[立檢見川高等學校](../Page/千葉縣立檢見川高等學校.md "wikilink")，後來轉學至[東京都](../Page/東京都.md "wikilink")[日出高等學校](../Page/日出中學校、高等學校.md "wikilink")，並於該校畢業。
  - 除了雙胞胎姊姊奈津子外，家中尚有二位姊姊，全家共有四姊妹。
  - 在2008年12月20日，她與奈津子一同離開了[研音](../Page/研音.md "wikilink")，並在其後加盟[WINT
    ARTS事務所](../Page/WINT_ARTS.md "wikilink")。
  - 在2010年5月15日，她與奈津子一同開始以[SDN48](../Page/SDN48.md "wikilink")2期生的身分活動，直至2012年3月31日為止。
  - 在2011年6月1日，她與奈津子一起從WINT
    ARTS轉投[Dig-esT事務所](../Page/Dig-esT.md "wikilink")。
  - 在2014年10月1日，奈津子自己從Dig-esT轉投[Asia Business
    Partners事務所](../Page/Asia_Business_Partners.md "wikilink")\[1\]，令亞希子與她不再是同一事務所旗下的藝人。
  - 在2015年6月6日，她宣布以營業、編輯的身分加入[Sirabee（<span lang="ja">しらべぇ</span>）編輯部](../Page/博報堂DY控股#AD+VENTURE.md "wikilink")。藝能活動方面，則在離開Dig-esT後以自由身繼續。

### 其他人物資料

  - 喜歡的學科是國語（日語）、英語和音樂。
  - 喜歡的運動是籃球。

## 參與歌曲

### 單曲CD選拔曲

  - [SDN48](../Page/SDN48.md "wikilink")「Under Girls A」名義
      -
      -
      -
  - SDN48「Under Girls B」名義
      -
      -
### 劇場公演Unit曲

  - [SDN48](../Page/SDN48.md "wikilink") 1st Stage 2期生「」公演
      - Black boy
      - I'm sure

## 演出

### 電視劇

  - [野豬大改造](../Page/野豬大改造.md "wikilink")（[日本電視台](../Page/日本電視台.md "wikilink")、2005年10月
    - 12月）飾演佐伯奈美

  - [芭蕾舞蹈家](../Page/芭蕾舞蹈家.md "wikilink")（日本電視台、2006年4月 - 6月）飾演吉村冬美

  - [新三人時代](../Page/新三人時代.md "wikilink")（又譯為「水漾青春」，[朝日電視台](../Page/朝日電視台.md "wikilink")、2006年7月
    - 9月）飾演

  - [鐵板少女小茜\!\!](../Page/鐵板少女小茜!!.md "wikilink")（[東京廣播公司](../Page/TBS電視台.md "wikilink")、2006年10月
    - 12月）飾演（桂橘子）

  - [未來遊園地](../Page/未來遊園地.md "wikilink")（朝日電視台、2007年9月28日）飾演

  - [P\&G Pantene Drama
    Special](../Page/P&G_Pantene_Drama_Special.md "wikilink")
    [畫牌美人](../Page/畫牌美人.md "wikilink")（）（[富士電視台](../Page/富士電視台.md "wikilink")、2008年2月11日）飾演一條左京

  - 未來遊園地2（朝日電視台、2008年3月28日）飾演

  - [Hit
    Maker阿久悠物語](../Page/Hit_Maker阿久悠物語.md "wikilink")（日本電視台、2008年8月1日）飾演[The
    Peanuts](../Page/The_Peanuts.md "wikilink")

  - 第一夜「Camouflage」（東京廣播公司、2008年10月6日）飾演高峰百合

  - [鈴里高校書道部](../Page/鈴里高校書道部.md "wikilink")（[日本放送協會](../Page/日本放送協會.md "wikilink")、2010年1月
    - 2月）飾演

  - （[TBS電視台等](../Page/TBS電視台.md "wikilink")、2010年12月30日）飾演

  - （富士電視台、2011年2月19日）飾演

  - （第6集）（富士電視台、2014年）飾演悅子

### 其他電視節目

  - [唐澤壽明](../Page/唐澤壽明.md "wikilink") Presents 記憶的力量
    II（[日本電視台](../Page/日本電視台.md "wikilink")、2004年6月25日） - 首演

  - [Disney
    365](../Page/Disney_365.md "wikilink")（[衛星廣播](../Page/衛星廣播.md "wikilink")
    [Disney Channel](../Page/Disney_Channel.md "wikilink")、2006年5月）

  - [Quiz\! Hexagon
    II](../Page/Quiz!_Hexagon_II.md "wikilink")（[富士電視台](../Page/富士電視台.md "wikilink")、2007年2月7日）

  - [明石家3頻道](../Page/明石家3頻道.md "wikilink")（[東京廣播公司](../Page/東京廣播公司.md "wikilink")、2007年2月28日
    21:00）

  - [BURUBURU
    Untouchable](../Page/BURUBURU_Untouchable.md "wikilink")（[朝日放送](../Page/朝日放送.md "wikilink")、2007年4月13日
    - 5月4日（逢週五））

  - [熱血！Hobby
    Stadium](../Page/熱血！Hobby_Stadium.md "wikilink")（[衛星廣播](../Page/衛星廣播.md "wikilink")276頻道（[Kids
    Station](../Page/Kids_Station.md "wikilink")）、2007年4月14日 - ）（常規演出）

  - [BS
    BRUNCH](../Page/BS_BRUNCH.md "wikilink")（[BS-i](../Page/BS-i.md "wikilink")、2007年11月3日
    - ）（常規演出）

  - （富士電視台、2007年10月 - ）（常規演出）

  - [God Tongue～The God Tongue
    神之舌～](../Page/God_Tongue.md "wikilink")（[東京電視台](../Page/東京電視台.md "wikilink")、2008年5月14日）

  - （[朝日電視台](../Page/朝日電視台.md "wikilink")、2010年6月30日、8月4日、11日）

### 電影

  - [歌魂♪](../Page/歌魂♪.md "wikilink")（[日活](../Page/日活.md "wikilink")、2008年4月5日公映）飾演松本楓

  - （2008年10月18日公映）

### 音樂會

  - 第3公演（[橫濱體育館](../Page/橫濱體育館.md "wikilink")、2010年3月25日）

  - 第3公演（[代代木第一體育館](../Page/國立代代木競技場#第一體育館.md "wikilink")、2010年7月11日）

### 舞台劇

  - （[Theater Green](../Page/Theater_Green.md "wikilink")、2012年4月12日 -
    16日）（與[奈津子交替飾演同一角色](../Page/奈津子.md "wikilink")）

  - （Theater Green BIG TREE THEATER、2012年7月12日 - 16日）

  - 第1回Dig/esT Produce公演「」（[Sun-mall
    Studio](../Page/Sun-mall_Studio.md "wikilink")、2013年1月16日 - 27日）

### 廣告

  - [TOTO](../Page/TOTO.md "wikilink")
  - [NTT東日本電報](../Page/NTT東日本.md "wikilink")
  - [Panasonic](../Page/Panasonic.md "wikilink")
  - [GODIVA](../Page/GODIVA.md "wikilink")\[2\]

### DVD

  - 亞希子（[LIVERPOOL](../Page/LIVERPOOL.md "wikilink")、2007年9月21日發售）
  - 奈津子·亞希子 with（LIVERPOOL、2007年12月21日發售）
  - 亞希子 恐縮BODY（[GRASSOC](../Page/GRASSOC.md "wikilink")、2011年7月14日發售）

### 其他

  - 手機戀愛劇
    100Scene之戀（Vol.2，第3集）（[VOLTAGE](../Page/VOLTAGE.md "wikilink")、2008年8月12日
    - ）
  - [任天堂DS遊戲](../Page/任天堂DS.md "wikilink")
    [犬公司DS](../Page/犬公司.md "wikilink")（）（[CyberFront](../Page/CyberFront.md "wikilink")、2009年）聲演茶野小姐（茶野さん）

## 書籍

### 寫真集

  - futari（[木村晴](../Page/木村晴.md "wikilink")、[WANI
    BOOKS](../Page/WANI_BOOKS.md "wikilink")、2006年4月26日發售、ISBN
    4-8470-2925-9）

<!-- end list -->

  -
    與雙胞胎姊姊奈津子一起拍攝的寫真集。

### 雜誌

| 名稱            | 發售日        | [ISBN](../Page/ISBN.md "wikilink")（如有） |
| ------------- | ---------- | -------------------------------------- |
| 第三文明          | 2006/11/01 |                                        |
| TV JAPAN      | 2006/11/15 |                                        |
| Kindai        | 2006/11/22 |                                        |
| memew         | 2006/11/28 | ISBN 4-7648-7138-6                     |
| memew DX 2006 | 2006/12/28 | ISBN 4-7648-7133-5                     |

## 關連項目

  - [奈津子](../Page/奈津子.md "wikilink")
  - [夏帆](../Page/夏帆.md "wikilink")
  - [志保](../Page/志保.md "wikilink")

## 外部連結

  - [Sirabee官方個人詳細資料](http://sirabee.com/author/akiko/)

  - [奈津子及亞希子官方Blog「」](http://ameblo.jp/natsuko-akiko/)

  -
  -
  - （2016年5月起）

  - [研音前官方個人詳細資料](https://web.archive.org/web/20080121213914/http://www.ken-on.co.jp/artist/html/akiko.html)（[Internet
    Archive連結](../Page/Internet_Archive.md "wikilink")）

  - [Dig-esT前官方個人詳細資料](https://web.archive.org/web/20150204073805/http://dig-est.jp/akiko/index.php)（Internet
    Archive連結）

  - [Yahoo\! Japan -
    亞希子的個人資料](https://archive.is/20130501055425/dir.yahoo.co.jp/talent/1/w06-0111.html)

[Category:日本電視演員](../Category/日本電視演員.md "wikilink")
[Category:千葉縣出身人物](../Category/千葉縣出身人物.md "wikilink")
[Category:在日本出生的雙胞胎](../Category/在日本出生的雙胞胎.md "wikilink")
[Category:日本女性偶像](../Category/日本女性偶像.md "wikilink")
[Category:日本寫真偶像](../Category/日本寫真偶像.md "wikilink")
[Category:SDN48前成员](../Category/SDN48前成员.md "wikilink")

1.  [☆皆様にお知らせ☆](http://ameblo.jp/natsuko-akiko/entry-11932882149.html) -
    官方Blog
2.