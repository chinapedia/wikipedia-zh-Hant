[Alessandro_Baricco.jpg](https://zh.wikipedia.org/wiki/File:Alessandro_Baricco.jpg "fig:Alessandro_Baricco.jpg")
**亞歷山卓·巴利科**（Alessandro
Baricco，）出生於[都靈](../Page/都靈.md "wikilink")，是[義大利最受讀者愛戴的小說家之一](../Page/義大利.md "wikilink")。

|             |
| ----------- |
| __TOC__ |

## 生平

巴利科大學就讀哲學系，指導教授為[強尼·伐第摩](../Page/強尼·伐第摩.md "wikilink")（Gianni
Vattimo），並在[音樂學院取得鋼琴主修文憑](../Page/音樂學院.md "wikilink")，他曾經發表過幾篇音樂評論性質的散文(1988年，*賦格的天才*)。後來他以樂評身份進入[共和國報](../Page/共和國報.md "wikilink")，後來又在[新聞報擔任文化版編輯](../Page/新聞報.md "wikilink")，於新聞界嶄露頭角。

除了廣播節目以外，巴利科也曾在電視台工作。1993年為**RAI**([義大利廣播電視公司](../Page/義大利廣播電視公司.md "wikilink"))第三頻道執導廣播劇「*愛情似箭*」，1994年又和新聞工作者喬凡娜·祖寇尼（Giovanna
Zucconi）合作，為[文學節目](../Page/文學.md "wikilink")「*Pickwick 讀與寫*」擔任企畫和執行工作。

他出版的[小說包括](../Page/小說.md "wikilink")：《[憤怒的城堡](../Page/憤怒的城堡.md "wikilink")》（1991年）、《[海洋，海](../Page/海洋，海.md "wikilink")》（1993年）、《[絹](../Page/絹.md "wikilink")》（1996年）、《[City](../Page/City.md "wikilink")》（1999年）、《[不流血](../Page/不流血.md "wikilink")》（2002年）。此外於1994年他出版了劇場文本《[1900：獨白](../Page/1900：獨白.md "wikilink")》，後來多次以口白方式被改編為劇場作品，並由[導演](../Page/導演.md "wikilink")[朱塞培·托納透雷拍攝成](../Page/朱塞培·托納透雷.md "wikilink")[電影](../Page/電影.md "wikilink")《[海上鋼琴師](../Page/海上鋼琴師.md "wikilink")》（1998年）。

之後巴利科將發表在新聞報的文章，編輯成兩本書出版（1995年的《*Barnum* 》和1998年的《*Barnum 2*
》）。2004年他依據瑪利亞·格拉齊亞·雀尼（Maria Grazia
Ciani）翻譯的[荷馬](../Page/荷馬.md "wikilink")[史詩](../Page/史詩.md "wikilink")《[伊利亞德](../Page/伊利亞德.md "wikilink")》，將其改編為劇場朗讀作品《[荷馬，伊利亞德](../Page/荷馬，伊利亞德.md "wikilink")》。2005年11月小說「[這個故事](../Page/這個故事.md "wikilink")」問世。

經過電視台的製作經驗後，他與幾位合夥人一起在[都靈創辦了](../Page/都靈.md "wikilink")[荷登學校](../Page/荷登學校.md "wikilink")，以跨學科的方法教導敘事及寫作技巧。

## 作品

### 小說

  - [憤怒的城堡](../Page/憤怒的城堡.md "wikilink")（*Castelli di rabbia*）
  - [海洋，海](../Page/海洋，海.md "wikilink")（*Oceano Mare*）
  - *[City](../Page/City.md "wikilink")*
  - [不流血](../Page/不流血.md "wikilink")（*Senza sangue*）
  - [這個故事](../Page/這個故事.md "wikilink")（*Questa storia*）
  - [绢](../Page/绢.md "wikilink")("Seta")

### 戲劇作品

  - [1900：獨白](../Page/1900：獨白.md "wikilink")（*Novecento*）
  - 達薇拉·羅阿（*Davila Roa*）,從未出版，在1996年由路卡·倫科尼搬上舞台演出
  - 西班牙黨（*Partita Spagnola*）
  - [荷馬，伊利亞德](../Page/荷馬，伊利亞德.md "wikilink")（*Omero, Iliade*）

### 選集

  - *Barnum*
  - *Barnum 2*
  - 將來，全球化與未來世界手冊（*Next. Piccolo libro sulla globalizzazione e il mondo
    che verrà*）,2002年。

### 散文

  - 賦格的天才，論羅西尼的戲劇音樂（*Il genio in fuga. Sul teatro musicale di Rossini*）,
    1988年 及 1997年
  - 海格爾的靈魂與威斯康辛的母牛（ *L'anima di Hegel e le mucche del Wisconsin*）, 1992年

## 外部連結

  - [書迷為巴利科架設的網站，義大利文](https://web.archive.org/web/20170527115651/http://www.oceanomare.com/)
  - [《荷馬，伊利亞德》的劇場朗讀](http://www.raccontodelliliade.it)
  - [位於都靈的荷頓學校](http://www.scuolaholden.it)

[B](../Category/意大利小说家.md "wikilink")
[B](../Category/都靈大學校友.md "wikilink")