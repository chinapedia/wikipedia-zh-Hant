[Veljusa_Monastery_St._Alexander_of_Alexandria.jpg](https://zh.wikipedia.org/wiki/File:Veljusa_Monastery_St._Alexander_of_Alexandria.jpg "fig:Veljusa_Monastery_St._Alexander_of_Alexandria.jpg")
**亞歷山大的亞歷山大**（327年4月17日卒于[亞歷山大城](../Page/亞歷山大城.md "wikilink")），于公元313年至327年任亞歷山大的[主教](../Page/亞歷山大宗主教.md "wikilink")。在与[阿里烏教派争论的初期他是](../Page/阿里烏教派.md "wikilink")[阿利烏的反对党的领导人之一](../Page/阿利烏.md "wikilink")。

## 生平

亞歷山大在民众和僧侣中皆享受很高的声誉，据称他对穷人非常大方，为人简朴、公正、有风度。

亞歷山大是与阿里乌教派发生争论初期的主要人物之一。这场争论的开始是在一次辩论中阿利乌责怪亚历山大持[撒伯流的异端](../Page/撒伯流.md "wikilink")。后来当阿利乌公开了他的学说后亚历山大一开始试图说服阿利乌放弃他的学说。结果他自己的僧侣责怪他对阿利乌太缓和了。当这场争论越闹越大后亚历山大于320年在亚历山大港召集了一次大会。在这次大会上上百名埃及和利比亚的主教们谴责了阿利乌。

阿利乌离开埃及寻找[該撒利亞的優西比烏和](../Page/該撒利亞的優西比烏.md "wikilink")[尼克摩底亚的优西比乌的支持](../Page/尼克摩底亚的优西比乌.md "wikilink")，这样一来这场争论事实上扩展到了当时的整个基督教教会。罗马皇帝[君士坦丁一世派人给亚历山大和阿利乌各送了一封信](../Page/君士坦丁一世_\(罗马帝国\).md "wikilink")，要求他们和解，而不要因为“这样一件小事”争吵。

亞歷山大一世在325年召开的[基督教](../Page/基督教.md "wikilink")[第一次尼西亚公会议上猛烈反对](../Page/第一次尼西亚公会议.md "wikilink")[阿里乌派](../Page/阿里乌派.md "wikilink")，指责他们是[异端](../Page/异端.md "wikilink")。他是东正教派的领导人。

亚历山大的著作中仅有三封信留下来了，这三封信件是记载阿里乌教派争论的重要文献。第一封是写给[君士坦丁堡牧首](../Page/君士坦丁堡牧首列表.md "wikilink")[亚历山大的](../Page/亚历山大_\(君士坦丁堡\).md "wikilink")，第二封是写给所有主教的，第三封是革除阿利乌的神职的信。他的继承人[亚他那修在他的著作中引用了亚历山大的信和祈祷](../Page/亚他那修.md "wikilink")。

按照[科普特教派的纪录亚历山大是其第](../Page/科普特教派.md "wikilink")19位教宗。

在东正教和天主教教会中亚历山大是圣人，其纪念日是2月26日，在科普特教会中其纪念日是4月22日。

{{-}}

[Category:327年逝世](../Category/327年逝世.md "wikilink")
[Category:亞歷山大港牧首](../Category/亞歷山大港牧首.md "wikilink")
[Category:埃及天主教徒](../Category/埃及天主教徒.md "wikilink")
[Category:罗马帝国基督徒](../Category/罗马帝国基督徒.md "wikilink")
[Category:4世纪基督徒](../Category/4世纪基督徒.md "wikilink")
[Category:4世纪古罗马人](../Category/4世纪古罗马人.md "wikilink")