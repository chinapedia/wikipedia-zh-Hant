**亞歷山大丹尼士Enviro 200
Dart**（）是一款由[英國](../Page/英國.md "wikilink")[亞歷山大丹尼士生產的兩軸單層巴士](../Page/亞歷山大丹尼士.md "wikilink")。它也是亞歷山大丹尼士製造的其中一款Enviro系列巴士，於2006年面世，主要取代[飛鏢SLF及Pointer車身](../Page/丹尼士飛鏢.md "wikilink")。

## 設計

Enviro 200
Dart的底盤可以配合[康明斯ISBe歐盟四型](../Page/康明斯.md "wikilink")／歐盟五型四／六氣缸環保標準引擎，採用SCR選擇性還原催化裝置，需要添加[AdBlue](../Page/AdBlue.md "wikilink")。另外配合Allison
T280R五前速波箱、Allison 2100四/五前速波箱、Voith DIWA 823.3E三前速波箱或Voith DIWA
854.5四前速波箱，車身採用[鋁製支架](../Page/鋁.md "wikilink")，[玻璃纖維外殼](../Page/玻璃纖維.md "wikilink")。巴士車廂佈局、空調及[暖氣視乎按照用家需要](../Page/暖氣.md "wikilink")。

Enviro 200可裝配原廠車身以達致底盤車身一體化，車嘴沿用同廠Enviro 400的設計。而底盤可裝配東蘭開夏／Darwen／Optare
Esteem（已於2009年停產）或者MCV Evolution車身。於2007年初，亞歷山大丹尼士宣佈Enviro
200車身可配猛獅14.240（代號A66）底盤。

Enviro
200原先在亞歷山大丹尼士位於蘇格蘭Falkirk的廠房裝嵌車身，但自2007年中開始，車身裝嵌逐步轉移到屬下Plaxton位於英格蘭Scarborough的廠房。

2008年底，亞歷山大丹尼士在Euro Bus Expo 2008展覽會展出Enviro 200的柴油／電力混合驅動版Enviro
200H，沿用BAE Systems的HybriDrive驅動系統，並以康明斯ISBe四氣缸引擎發電。

## Enviro 200 Dart在英國

[Stagecoach_in_Swindon_fleet_no.36128.JPG](https://zh.wikipedia.org/wiki/File:Stagecoach_in_Swindon_fleet_no.36128.JPG "fig:Stagecoach_in_Swindon_fleet_no.36128.JPG")
Enviro
200在英國倫敦多間巴士公司使用，行走倫敦的合約巴士線，另外在倫敦以外不少巴士公司購入，包括大規模的巴士集團[捷達集團](../Page/捷達.md "wikilink")、[爱瑞发及](../Page/爱瑞发.md "wikilink")[第一集團](../Page/第一集團.md "wikilink")。2009年，5架Enviro
200H於英國倫敦開始試行，由Transdev London（現London United）營運。

### 配Enviro 200車身的猛獅底盤

配Enviro 200的猛獅14.240，自2008年，英國捷達已經接收82部。另外倫敦Metrobus公司在2008年接收3部。

## Enviro 200 Dart在香港

由於香港的路面環境遠比英國惡劣，車身亦需裝上空調機，故引擎馬力需有所調整，香港版本全選用[康明斯ISBe歐盟四型](../Page/康明斯.md "wikilink")／歐盟五型六氣缸環保標準引擎，但馬力則由英國原廠設定的205匹（151kW）提升至225匹（165kW）。最大扭力則調至850Nm。

### 港鐵巴士

[MTRBus_E200BUS.jpg](https://zh.wikipedia.org/wiki/File:MTRBus_E200BUS.jpg "fig:MTRBus_E200BUS.jpg")
[MTR_E200_(1).JPG](https://zh.wikipedia.org/wiki/File:MTR_E200_\(1\).JPG "fig:MTR_E200_(1).JPG")
2007年1月12日，[九廣鐵路公司宣佈購買](../Page/九廣鐵路公司.md "wikilink")11輛11.3米Enviro 200
Dart歐盟四型單層巴士（編號901-911），用以取代車齡約17年的三菱MK117J單層巴士（編號301-311），並全數租予[港鐵公司使用](../Page/港鐵公司.md "wikilink")。使港鐵成為香港境內首間使用此款型號的公共交通機構。

2008年8月1日，首輛Enviro 200
Dart單層巴士扺港並由工作人員駕駛至港鐵火炭巴士車廠。巴士於車輛調較完成及領取客運營業證後，編號901（NM4716）和902（NM4760）的巴士於2008年8月31日下午首航[K68線](../Page/港鐵巴士K68線.md "wikilink")，其餘9輛亦已於2009年3月前投入服務。全數11輛巴士加入[港鐵巴士](../Page/港鐵巴士.md "wikilink")（新界西北）車隊，服務於[屯門區及](../Page/屯門區.md "wikilink")[元朗區](../Page/元朗區.md "wikilink")。

這款巴士被安排以企位為主的車廂環境，全車可容納27名座位乘客和50位企位乘客，對疏導短途路線的客量有莫大幫助，所以在繁忙時間，[K58線](../Page/港鐵巴士K58線.md "wikilink")、[K73線特別車和](../Page/港鐵巴士K73線.md "wikilink")[K75S線都有它們的蹤影](../Page/港鐵巴士K75S線.md "wikilink")。

### 九龍巴士

[KMB_SE8539_208_2018.jpg](https://zh.wikipedia.org/wiki/File:KMB_SE8539_208_2018.jpg "fig:KMB_SE8539_208_2018.jpg")
[九龍巴士於](../Page/九龍巴士.md "wikilink")2010年訂購30輛10.4米Enviro 200
Dart巴士，配備[歐盟五型至六型之間排放標準](../Page/歐洲汽車尾氣排放標準.md "wikilink")（環境友好/EEV）引擎，取代年事已高的1993年三菱MK117、1995年三菱MK217型單層巴士及1994年[丹尼士飛鏢單層巴士](../Page/丹尼士飛鏢.md "wikilink")。首部巴士（車牌號碼︰PU4458，車隊編號︰AAU1）已於2010年10月17日抵港，並在2011年3月30日正式出牌，其餘29輛Enviro
200
Dart已於同年4月至9月期間出牌\[1\]。其中十二部巴士（AAU1、2、3、6、8、10、14、16、18、21、22、23）安裝手動減速器\[2\]
，以便行駛陡峭的荃錦公路的線。現時該批巴士主要行走客量較低或有用車限制的路線，包括、\[3\]、、及線，於清明節及重陽節期間更會行走[14S線](../Page/九龍巴士14S線.md "wikilink")。
AAU於2016年9月全數更換LED光管照明。

2012年，九巴訂購11輛10.4米Enviro 200
Dart單層巴士，新車配置了Hanover發光二極管（LED）橙色電子顯示屏，上海凱倫自動報站機，Vogel
Revo S
座椅，改用單門設計\[4\]\[5\]，取代1996年[丹尼士飛鏢單層巴士](../Page/丹尼士飛鏢.md "wikilink")\[6\]。首部巴士（車牌號碼︰SE5971，車隊編號︰AAS1）已於2013年4月抵港，並於8月26日正式出牌，其餘10輛Enviro
200
Dart已於同年8月至9月期間出牌。該批巴士目前除AAS2及6被調往[九龍灣車廠外](../Page/九巴九龍灣車廠.md "wikilink")，其餘均屬於[荔枝角車廠](../Page/九巴荔枝角車廠.md "wikilink")，現時主要行走、203C及線。

### 愉景灣交通服務

2014年2月8日，[愉景灣交通服務證實訂購首批五輛Enviro](../Page/愉景灣巴士.md "wikilink")200
Dart單層巴士，全數已於2014年3月1日抵港\[7\]。當中有一輛長度為11.3米，其餘四輛長度為10.4米。其中3輛長度10.4米巴士（DBAY5、36、37）已於2014年4月11日正式出牌，並於4月16日正式投入服務。另外1輛11.3米巴士（DBAY135）已於2014年4月16日正式出牌，4月17日正式投入服務。

### 珀麗灣客運

珀麗灣客運於2016年分兩批訂購輛11.3米Enviro 200
Dart巴士，配備[歐盟五型至六型之間排放標準](../Page/歐洲汽車尾氣排放標準.md "wikilink")（環境友好/EEV）引擎，現時主要行走NR330(青衣)及NR332(葵芳)綫。這批車是該型號全球最後的訂單，之後隨即停產，並停止接受新訂單及開始清售貨尾。

## 參見

其他亞歷山大丹尼士巴士產品：

  - [飛鏢SLF](../Page/丹尼士飛鏢巴士.md "wikilink")
  - [三叉戟二型](../Page/丹尼士三叉戟二型.md "wikilink")
  - [Enviro 200](../Page/亞歷山大丹尼士Enviro_200.md "wikilink")
  - [Enviro 200 MMC](../Page/亞歷山大丹尼士Enviro_200_MMC.md "wikilink")
  - [Enviro 300](../Page/亞歷山大丹尼士Enviro_300.md "wikilink")
  - [Enviro 350H](../Page/亞歷山大丹尼士Enviro_350H.md "wikilink")
  - [Enviro 400](../Page/亞歷山大丹尼士Enviro_400.md "wikilink")
  - [Enviro 500](../Page/亞歷山大丹尼士Enviro_500.md "wikilink")
  - [Enviro 500 MMC](../Page/亞歷山大丹尼士Enviro_500_MMC.md "wikilink")

## 外部連結

  - [Enviro 200
    Dart型號官方說明](https://web.archive.org/web/20110616210315/http://www.alexander-dennis.com/product-details.php?s=82&subs=42&tableID=219&itemID=1)

## 備註及參考文獻

[Category:巴士型號](../Category/巴士型號.md "wikilink")
[Category:香港單層巴士](../Category/香港單層巴士.md "wikilink")
[Category:丹尼士巴士](../Category/丹尼士巴士.md "wikilink")

1.
2.  [已安裝手動減速器的巴士](http://kmb.hk2007.net/y01.html) ，KMB.HK2007.NET
3.  只有安裝手動減速器的Enviro 200 Dart巴士才會行走51線
4.  [香港討論區：已確定九巴新E200用單門\!\!有相](http://www.discuss.com.hk/viewthread.php?tid=21698868)，2013-03-31
5.  [hkiTalk：左邊兩隻門 .
    AAU](http://www.hkitalk.net/HKiTalk2/thread-795708-1-1.html)，2013-08-16
6.  [柏斯敦巴士台關於單門Enviro200的資料](https://www.facebook.com/photo.php?fbid=524379014266471&set=pb.459548207416219.-2207520000.1370249978.&type=3&src=https%3A%2F%2Ffbcdn-sphotos-e-a.akamaihd.net%2Fhphotos-ak-prn2%2F969278_524379014266471_84768400_n.jpg&size=800%2C602)
7.  [DbAY
    E200](http://www.hkitalk.net/HKiTalk2/thread-826807-1-1.html)，hkitalk.net