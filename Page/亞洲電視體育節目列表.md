下表只列出在[本港台及](../Page/本港台.md "wikilink")[國際台播放的體育節目](../Page/亞洲電視國際台.md "wikilink")。

## 本港台

  - [2002世界盃賽事精華](../Page/2002世界盃賽事精華.md "wikilink")（2002年）
  - [東亞足球錦標賽2003](../Page/東亞足球錦標賽2003.md "wikilink")（2003年）（主持：[丁偉傑](../Page/丁偉傑.md "wikilink")、[何靜江](../Page/何靜江.md "wikilink")、[陳炳安](../Page/陳炳安.md "wikilink")）
  - [歐洲聯賽冠軍盃直播](../Page/歐洲聯賽冠軍盃直播.md "wikilink")（2003－2006年）（主持：[丁偉傑](../Page/丁偉傑.md "wikilink")、[何靜江](../Page/何靜江.md "wikilink")、[陳炳安](../Page/陳炳安.md "wikilink")，嘉賓主持：[徐國安](../Page/徐國安.md "wikilink")、[吳群立](../Page/吳群立.md "wikilink")、[梁帥榮](../Page/梁帥榮.md "wikilink")）
  - [歐洲聯賽冠軍盃雜誌](../Page/歐洲聯賽冠軍盃雜誌.md "wikilink")（2003－2006年）（主持：[丁偉傑](../Page/丁偉傑.md "wikilink")）
  - [西班牙甲組足球聯賽精華](../Page/西班牙甲組足球聯賽精華.md "wikilink")（2003－2006年）
  - [歐洲國家盃2004](../Page/歐洲國家盃2004.md "wikilink")（2004年）
  - 2004[雅典奧運](../Page/雅典奧運.md "wikilink")（2004年）（主持：[黃子雄](../Page/黃子雄.md "wikilink")、[徐嘉乐](../Page/徐嘉乐.md "wikilink")、[張慧慈](../Page/張慧慈.md "wikilink")、[繆美詩](../Page/繆美詩.md "wikilink")）
  - [體壇一週](../Page/體壇一週.md "wikilink")（2005-2008年）
  - [世界盃球星大特寫](../Page/世界盃球星大特寫.md "wikilink")（2006年）
  - [世界盃列強巡禮](../Page/世界盃列強巡禮.md "wikilink")（2006年）（主持：[丁偉傑](../Page/丁偉傑.md "wikilink")）
  - [2006世界盃賽事精華](../Page/2006世界盃賽事精華.md "wikilink")（2006年）（主持：[丁偉傑](../Page/丁偉傑.md "wikilink")、[何靜江](../Page/何靜江.md "wikilink")、[陳炳安](../Page/陳炳安.md "wikilink")、[梁帥榮](../Page/梁帥榮.md "wikilink")）
  - [德國盃精華](../Page/德國盃精華.md "wikilink")（2006年）
  - [德國盃](../Page/德國盃.md "wikilink")（nowTV借出評述員）
  - [日本職业聯賽直播](../Page/日本職业聯賽直播.md "wikilink")（2006年）（主持：[徐嘉諾](../Page/徐嘉諾.md "wikilink")、[何靜江](../Page/何靜江.md "wikilink")）
  - [為中國加油](../Page/為中國加油.md "wikilink")（2008[北京奧運特備節目](../Page/北京奧運.md "wikilink")）（2008年）（主持：[李麗珊](../Page/李麗珊.md "wikilink")、[顧紀筠](../Page/顧紀筠.md "wikilink")、[谷德昭](../Page/谷德昭.md "wikilink")、[林曉峰](../Page/林曉峰.md "wikilink")、[袁彩雲](../Page/袁彩雲.md "wikilink")）
  - [蘇格蘭超級聯賽](../Page/蘇格蘭超級聯賽.md "wikilink")（2008-2009年）（主持：[蘇子傑](../Page/蘇子傑.md "wikilink")、[徐國安](../Page/徐國安.md "wikilink")、[梁禮勤](../Page/梁禮勤.md "wikilink")）
  - [國際足球戰報](../Page/國際足球戰報.md "wikilink")（2009年）（主持：[蘇子傑](../Page/蘇子傑.md "wikilink")、[梁禮勤](../Page/梁禮勤.md "wikilink")）
  - [U21歐洲國家盃2009](../Page/U21歐洲國家盃2009.md "wikilink")（2009年）（主持：[蘇子傑](../Page/蘇子傑.md "wikilink")、[徐國安](../Page/徐國安.md "wikilink")、[梁禮勤](../Page/梁禮勤.md "wikilink")）
  - 足球狂熱（2013年）（主持：[徐國安](../Page/徐國安.md "wikilink")、[伍偉樂](../Page/伍偉樂.md "wikilink")、[黃文韜](../Page/黃文韜.md "wikilink")、[邢安邦](../Page/邢安邦.md "wikilink")、[梁承傑](../Page/梁承傑.md "wikilink")）
  - 香港超級聯賽（2014-2015年）

| 首播日期   | 播映時間        | 類別 | 節目名稱                                                 | 主持/演出/旁白 | 官方網頁 | 備註 |
| ------ | ----------- | -- | ---------------------------------------------------- | -------- | ---- | -- |
| 01月11日 | 01:10-03:05 | 足球 | [足球狂熱 港超聯：太陽飛馬 對 和富大埔](../Page/足球狂熱.md "wikilink")   |          |      |    |
| 01月11日 | 03:05-04:55 | 足球 | [足球狂熱 港超聯：黃大仙 對 標準流浪](../Page/足球狂熱.md "wikilink")    |          |      |    |
| 01月11日 | 14:20-16:30 | 足球 | [足球狂熱 港超聯：東方 對 天行元朗](../Page/足球狂熱.md "wikilink")     |          |      |    |
| 01月18日 | 01:35-03:25 | 足球 | [足球狂熱 高級組銀牌賽：傑志 對 東方](../Page/足球狂熱.md "wikilink")    |          |      |    |
| 01月25日 | 01:35-03:25 | 足球 | [足球狂熱 港超聯：和富大埔 對 YFC澳滌](../Page/足球狂熱.md "wikilink")  |          |      |    |
| 02月1日  | 01:30-03:25 | 足球 | [足球狂熱 聯賽盃：東方 對 天行元朗](../Page/足球狂熱.md "wikilink")     |          |      |    |
| 02月1日  | 01:10-03:00 | 足球 | [足球狂熱 港超聯：標準流浪 對 YFC 澳滌](../Page/足球狂熱.md "wikilink") |          |      |    |
| 02月15日 | 01:30-02:55 | 賽馬 | [賽後你點睇](../Page/賽後你點睇.md "wikilink")                 |          |      |    |
| 02月28日 | 01:30-03:20 | 足球 | [足球狂熱 足總盃：天行元朗 對 沙田](../Page/足球狂熱.md "wikilink")     |          |      |    |
| 03月8日  | 01:35-03:25 | 足球 | [足球狂熱 港超聯：和富大埔 對 東方](../Page/足球狂熱.md "wikilink")     |          |      |    |
| 03月15日 | 01:45-03:40 | 足球 | [足球狂熱 港超聯：天行元朗 對 黃大仙](../Page/足球狂熱.md "wikilink")    |          |      |    |
| 04月12日 | 01:45-03:35 | 足球 | [足球狂熱 足總盃：傑志 對 和富大埔](../Page/足球狂熱.md "wikilink")     |          |      |    |
| 04月19日 | 01:40-03:30 | 足球 | [足球狂熱 港超聯：黃大仙 對 和富大埔](../Page/足球狂熱.md "wikilink")    |          |      |    |

## 國際台

  - [NBA每周精選](../Page/NBA每周精選.md "wikilink")（1994－2005年）
  - [NBA精華](../Page/NBA精華.md "wikilink")（1994－2005年）
  - [歐洲聯賽冠軍盃精華](../Page/歐洲聯賽冠軍盃精華.md "wikilink")（2003－2006年）（主持：[TIM
    BREDBURY](../Page/TIM_BREDBURY.md "wikilink")、[DEREK
    CURRIE](../Page/DEREK_CURRIE.md "wikilink")、粵語評述：[丁偉傑](../Page/丁偉傑.md "wikilink")）
  - [歐洲聯賽冠軍盃精選](../Page/歐洲聯賽冠軍盃精選.md "wikilink")（2003－2006年）（主持：[TIM
    BREDBURY](../Page/TIM_BREDBURY.md "wikilink")、[DEREK
    CURRIE](../Page/DEREK_CURRIE.md "wikilink")、粵語評述：[丁偉傑](../Page/丁偉傑.md "wikilink")）
  - [歐洲聯賽冠軍盃雜誌](../Page/歐洲聯賽冠軍盃雜誌.md "wikilink")（2003－2006年）
  - [西班牙甲組足球聯賽精華](../Page/西班牙甲組足球聯賽精華.md "wikilink")（2003－2006年）
  - [歐洲國家盃2004](../Page/歐洲國家盃2004.md "wikilink")（2004年）
  - [世界盃列強巡禮](../Page/世界盃列強巡禮.md "wikilink")（2006年）
  - [世界盃列強大檢閱](../Page/世界盃列強大檢閱.md "wikilink")（2006年）
  - 亞洲聯賽冠軍盃（2008年）
  - 超級帆船賽（2009年）
  - 足球狂熱（2013年）（主持：[徐國安](../Page/徐國安.md "wikilink")、[伍偉樂](../Page/伍偉樂.md "wikilink")、[黃文韜](../Page/黃文韜.md "wikilink")、[邢安邦](../Page/邢安邦.md "wikilink")、[梁承傑](../Page/梁承傑.md "wikilink")）
  - 蘇格蘭超級聯賽（2013--）（主持：[徐國安](../Page/徐國安.md "wikilink")、[邢安邦](../Page/邢安邦.md "wikilink")、
    [何栢霖](../Page/何栢霖.md "wikilink")、[梁承傑](../Page/梁承傑.md "wikilink")、[伍偉樂](../Page/伍偉樂.md "wikilink")）
  - 香港超級聯賽（2014-2015年）

## 參見

[列表](../Category/亞洲電視節目.md "wikilink")