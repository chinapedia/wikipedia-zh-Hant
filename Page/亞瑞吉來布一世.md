<table>
<tbody>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

**亞瑞吉來布一世**（**Argeleb
I**），是[英國作家](../Page/英國.md "wikilink")[約翰·羅納德·瑞爾·托爾金](../Page/約翰·羅納德·瑞爾·托爾金.md "wikilink")（J.R.R.
Tolkien）的史詩式奇幻作品《[魔戒三部曲](../Page/魔戒.md "wikilink")》中的虛構人物。

他是[雅西頓](../Page/雅西頓.md "wikilink")（Arthedain）第七任國王，任內展開與[安格馬](../Page/安格馬.md "wikilink")（Angmar）的戰爭，最終在[阿蒙蘇爾](../Page/阿蒙蘇爾.md "wikilink")（Amon
Sûl）的防衛戰中陣亡。

## 名字

**亞瑞吉來布**（Argeleb）一名是[辛達語](../Page/辛達語.md "wikilink")，意思是「銀之君王」*Silver
king*。\[1\]自他開始，雅西頓國王和[登丹人酋長取名都包含](../Page/登丹人酋長.md "wikilink")「亞」（Ar）一字，象徵他們擁有[亞爾諾](../Page/亞爾諾.md "wikilink")（Arnor）的合法王權。\[2\]

## 總覽

亞瑞吉來布是亞爾諾的[登丹人](../Page/登丹人.md "wikilink")（Dúnedain），是國王[馬維吉爾](../Page/馬維吉爾.md "wikilink")（Malvegil）的兒子及繼承人。\[3\]不清楚他有沒有任何兄弟姊妹。他的兒子是[亞維力格](../Page/亞維力格一世.md "wikilink")（Arveleg）。\[4\]

他是雅西頓第七任國王，若把亞爾諾諸王計算在內則是第十七任國王。他任內其他兩個登丹人北方王國的[埃西鐸](../Page/埃西鐸.md "wikilink")（Isildur）血脈均已中斷，於是亞瑞吉來布宣佈統一整個亞爾諾。另外，安格馬於他父親時建立，在他統治時開始攻擊登丹人，同時雅西頓亦面對著[魯道爾](../Page/魯道爾.md "wikilink")（Rhudaur）的挑戰，因為對方想奪取風雲頂（Weathertop）的主權。

他生於第三紀元1226年，死於1356年，\[5\]享年130歲。

## 生平

### 早年

亞瑞吉來布在[第三紀元](../Page/第三紀元.md "wikilink")1226年出生，\[6\]應該是生於首都[佛諾斯特](../Page/佛諾斯特.md "wikilink")（Fornost）。1349年，他的父親馬維吉爾逝世，由亞瑞吉來布繼位。\[7\]

### 雅西頓國王

他成為國王後宣佈一統亞爾諾，其他兩國登丹人國家的反應截然不同，魯道爾作出強烈反彈，而[卡多蘭](../Page/卡多蘭.md "wikilink")（Cardolan）則沒有異議。為了警備魯道爾的入侵，亞瑞吉來布於風雲丘（Weather
Hills）修築防禦工事和堡壘。

第三紀元1356年，魯道爾在獲安格馬的秘密協助下進攻風雲頂。在阿蒙蘇爾的激烈攻防戰中，亞瑞吉來布被敵人殺死，結束了7年的統治，由他的兒子亞維力格繼位。\[8\]

## 半精靈家系

## 參考

  - 《[中土世界的歷史](../Page/中土世界的歷史.md "wikilink")》
      - 《[中土世界的民族](../Page/中土世界的民族.md "wikilink")》
  - 《[魔戒三部曲](../Page/魔戒.md "wikilink")》及其附錄

## 資料來源

[category:中土大陸的角色](../Page/category:中土大陸的角色.md "wikilink")
[category:中土大陸的登丹人](../Page/category:中土大陸的登丹人.md "wikilink")
[category:魔戒三部曲中的人物](../Page/category:魔戒三部曲中的人物.md "wikilink")

[pl:Królowie Arthedainu\#Argeleb
I](../Page/pl:Królowie_Arthedainu#Argeleb_I.md "wikilink")

1.  [Thain's Book: Argeleb
    I](http://www.tuckborough.net/dunedain.html#Argeleb1)

2.  《[魔戒三部曲](../Page/魔戒三部曲.md "wikilink")》 2001年 聯經初版翻譯 附錄一帝王本紀及年表

3.
4.
5.
6.
7.
8.