**亞瑟·衛斯理**是小说
《[哈利·波特](../Page/哈利·波特.md "wikilink")》中的虚构人物，在[魔法部麻瓜人工製品濫用局工作](../Page/魔法部.md "wikilink")，後來接掌魔法部長[盧夫·昆爵因應局勢所設立的新單位](../Page/盧夫·昆爵.md "wikilink")－偽造防禦咒語暨防護品偵查沒收處，帶領十名員工。他和他妻子[茉莉·衛斯理一共育有六名兒子和一名女兒](../Page/茉莉·衛斯理.md "wikilink")，分別是[查理·衛斯理](../Page/查理·衛斯理_\(哈利波特\).md "wikilink")、[比爾·衛斯理](../Page/比爾·衛斯理.md "wikilink")、[派西·衛斯理](../Page/派西·伊格內修斯·衛斯理.md "wikilink")、[弗雷·衛斯理、喬治·衛斯理](../Page/弗雷和喬治·衛斯理.md "wikilink")、[榮恩·衛斯理和](../Page/榮恩·衛斯理.md "wikilink")[金妮·衛斯理](../Page/金妮·衛斯理.md "wikilink")。而他的媳婦[妙麗·格蘭傑跟榮恩又育有兩個孫兒玫瑰和雨果](../Page/妙麗·格蘭傑.md "wikilink")。

亞瑟熱愛麻瓜的一切用品，但是又不是很熟練。他曾經買一部二手的麻瓜車回來用魔法加以改造成飛天車。根據亞瑟的說法，只要不是真的去使用改造過的麻瓜物品就不算違法。

但是根據榮恩的說法，如果亞瑟要突襲檢查他們家，亞瑟就是第一個要被抓的人。

由於亞瑟薪金不高，孩子又多，所以家裡大多數的東西都是二手的，在《》中，榮恩就曾駕駛他的飛天汽車撞上[渾拚柳](../Page/渾拚柳.md "wikilink")。最後這輛飛天車就留在校園的森林裡，亞瑟並沒有試圖找回來。

在第五集的時候，亞瑟因為出鳳凰會的任務而在魔法部遭到佛地魔的蛇的攻擊，由於哈利當時可以跟佛地魔的想法相連，所以哈利立刻提出警告，而亞瑟也順利地在魔法部其他人發現之前被送到聖蒙果醫院治療。

他的護法是一隻鼬鼠。

## 參見

  - [哈利·波特](../Page/哈利·波特.md "wikilink")
  - [榮恩·衛斯理](../Page/榮恩·衛斯理.md "wikilink")

## 參考書籍

  - 《哈利波特—消失的密室》

[cs:Weasleyovi\#Arthur
Weasley](../Page/cs:Weasleyovi#Arthur_Weasley.md "wikilink") [en:Order
of the Phoenix (fiction)\#Arthur
Weasley](../Page/en:Order_of_the_Phoenix_\(fiction\)#Arthur_Weasley.md "wikilink")
[fr:Personnages secondaires de l’univers de Harry Potter\#Arthur
Weasley](../Page/fr:Personnages_secondaires_de_l’univers_de_Harry_Potter#Arthur_Weasley.md "wikilink")
[hu:Weasley család\#Arthur
Weasley](../Page/hu:Weasley_család#Arthur_Weasley.md "wikilink")
[it:Weasley (famiglia)\#Arthur
Weasley](../Page/it:Weasley_\(famiglia\)#Arthur_Weasley.md "wikilink")
[no:Familien Wiltersen\#Arthur
Wiltersen](../Page/no:Familien_Wiltersen#Arthur_Wiltersen.md "wikilink")
[ro:Familia Weasley\#Arthur
Weasley](../Page/ro:Familia_Weasley#Arthur_Weasley.md "wikilink")
[sv:Familjen Weasley\#Arthur
Weasley](../Page/sv:Familjen_Weasley#Arthur_Weasley.md "wikilink")
[tr:Weasley ailesi\#Arthur
Weasley](../Page/tr:Weasley_ailesi#Arthur_Weasley.md "wikilink")

[Category:哈利波特人物](../Category/哈利波特人物.md "wikilink")