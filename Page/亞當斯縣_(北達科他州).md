**亞當斯縣**（）是[美國](../Page/美國.md "wikilink")[北達科他州西南部的一個縣](../Page/北達科他州.md "wikilink")，南鄰[南達科他州](../Page/南達科他州.md "wikilink")，面積2,561平方公里。根據[2010年人口普查](../Page/美國2010年人口普查.md "wikilink")，本縣共有人口2,343人。\[1\]本縣縣治為[赫廷傑](../Page/赫廷傑_\(北達科他州\).md "wikilink")（Hettinger）。

本縣於1907年4月17日成立\[2\]，縣名以[密爾沃基鐵路公司土地收購專員](../Page/密爾沃基鐵路.md "wikilink")[約翰·昆西·亞當斯命名](../Page/約翰·昆西·亞當斯_\(北達科他\).md "wikilink")。\[3\]\[4\]

## 地理

[Milwdepot-austinmn.jpg](https://zh.wikipedia.org/wiki/File:Milwdepot-austinmn.jpg "fig:Milwdepot-austinmn.jpg")
根據[2000年人口普查](../Page/2000年美国人口普查.md "wikilink")，亞當斯縣的總面積為，其中有，即99.91%為陸地；，即0.09%為水域。\[5\]

### 毗鄰縣

  - [北達科他州](../Page/北達科他州.md "wikilink")
    [鮑曼縣](../Page/鮑曼縣_\(北達科他州\).md "wikilink")：西方
  - 北達科他州 [斯洛普縣](../Page/斯洛普縣_\(北達科他州\).md "wikilink")：西北方
  - 北達科他州 [赫廷傑縣](../Page/赫廷傑縣_\(北達科他州\).md "wikilink")：北方
  - 北達科他州 [格蘭特縣](../Page/格蘭特縣_\(北達科他州\).md "wikilink")：東北方
  - 北達科他州 [蘇縣](../Page/蘇縣_\(北達科他州\).md "wikilink")：東方
  - [南達科他州](../Page/南達科他州.md "wikilink")
    [珀金斯縣](../Page/珀金斯縣_\(南達科他州\).md "wikilink")：南方
  - 南達科他州 [哈定縣](../Page/哈定縣_\(南達科他州\).md "wikilink")：西南方

## 人口

根據[2000年人口普查](../Page/2000年美國人口普查.md "wikilink")，亞當斯縣擁有2,593居民、1,121住戶和725家庭。\[6\]其[人口密度為每平方英里](../Page/人口密度.md "wikilink")2.6居民（每平方公里1居民）。\[7\]本縣擁有1,416間房屋单位，其密度為每平方英里1.4間（每平方公里0.6間）。\[8\]而人口是由98.5%[白人](../Page/歐裔美國人.md "wikilink")、0.54%[黑人](../Page/非裔美國人.md "wikilink")、0.31%[土著](../Page/美國土著.md "wikilink")、0.15%[亞洲人](../Page/亞裔美國人.md "wikilink")、0.04%[太平洋岛民](../Page/太平洋岛民.md "wikilink")、0.12%其他[種族和](../Page/種族.md "wikilink")0.35%[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人佔了人口](../Page/拉丁美洲人.md "wikilink")0.27%。在98.5%白人中，有40.6%是[德國人](../Page/德國人.md "wikilink")、27.9%是[挪威人](../Page/挪威人.md "wikilink")、5.6%是[愛爾蘭人](../Page/愛爾蘭人.md "wikilink")。\[9\]

在1,12住户中，有26.6%擁有一個或以上的兒童（18歲以下）、56.6%為夫妻、5.5%為單親家庭、35.3%為非家庭、32.6%為獨居、17.9%住戶有同居長者。平均每戶有2.24人，而平均每個家庭則有2.85人。在2,593居民中，有23.2%為18歲以下、4.1%為18至24歲、21.7%為25至44歲、27.0%為45至64歲以及24.1%為65歲以上。人口的年齡中位數為46歲，女子對男子的性別比為100：91.5。成年人的性別比則為100：90.6。\[10\]

本縣的住戶收入中位數為$29,079，而家庭收入中位數則為$34,306。男性的收入中位數為$23,073，而女性的收入中位數則為$18,714，[人均收入為](../Page/人均收入.md "wikilink")$18,425。約8.5%家庭和10.4%人口在[貧窮線以下](../Page/貧窮線.md "wikilink")，包括11.1%兒童（18歲以下）及11.1%長者（65歲以上）。\[11\]

## 參考文獻

[A](../Category/北达科他州行政区划.md "wikilink")

1.

2.
3.

4.  [County
    History](http://www.nd.gov/content.htm?parentCatID=83&id=County%20History)
    , State of North Dakota

5.

6.  [Population Profile of the United
    States: 2000](http://www.census.gov/population/www/pop-profile/profile2000.html)

7.  [Statistical profile of Alger County,
    Michigan](http://censtats.census.gov/data/MI/05026003.pdf) , United
    States Census Bureau, Census 2000

8.  [State and County QuickFacts](http://quickfacts.census.gov/qfd/)

9.  [State and County QuickFacts](http://quickfacts.census.gov/qfd/)

10. [American FactFinder](http://factfinder.census.gov/)

11.