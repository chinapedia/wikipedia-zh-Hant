**亞當斯縣**（）是[美國](../Page/美國.md "wikilink")[愛阿華州西南部的一個縣](../Page/愛阿華州.md "wikilink")，面積1,102平方公里。根據[2010年人口普查](../Page/美國2010年人口普查.md "wikilink")，共有人口4,029人，是全州人口最少的縣。\[1\]本縣自從2000年開始便一直都是全州人口最少的縣。\[2\]本縣縣治為[科寧](../Page/科寧_\(愛阿華州\).md "wikilink")（Corning）。

## 歷史

亞當斯縣是於1851年由愛阿華州立法機關成立，其名稱紀念美國第二任[總統](../Page/美國總統.md "wikilink")[約翰·亞當斯或其子](../Page/約翰·亞當斯.md "wikilink")，第六任總統[約翰·昆西·亞當斯](../Page/約翰·昆西·亞當斯.md "wikilink")。本縣於1853年3月12日正式分離[波特瓦特米縣](../Page/波特瓦特米縣_\(愛阿華州\).md "wikilink")。後來，[蒙哥馬里縣和](../Page/蒙哥馬里縣_\(艾奧瓦州\).md "wikilink")[猶尼昂縣先後自本縣成立](../Page/猶尼昂縣_\(愛阿華州\).md "wikilink")。本縣首個縣治為[昆西](../Page/昆西_\(愛阿華州\).md "wikilink")。於1872年，科寧成為本縣的新縣治。\[3\]

## 地理

[Icarian_Colony_sign_and_buildings.jpg](https://zh.wikipedia.org/wiki/File:Icarian_Colony_sign_and_buildings.jpg "fig:Icarian_Colony_sign_and_buildings.jpg")
根據[2000年人口普查](../Page/2000年美国人口普查.md "wikilink")，亞當斯縣的總面積為，其中有，即99.56%為陸地；，即0.44%為水域。\[4\]

### 毗鄰縣

所有亞當斯縣的毗鄰縣皆為愛阿華州的毗鄰縣。

  - [卡斯縣](../Page/卡斯縣_\(愛阿華州\).md "wikilink")：西北方
  - [亞代爾縣](../Page/亞代爾縣_\(愛阿華州\).md "wikilink")：東北方
  - [猶尼昂縣](../Page/猶尼昂縣_\(愛阿華州\).md "wikilink")：東方
  - [泰勒縣](../Page/泰勒縣_\(愛阿華州\).md "wikilink")：南方
  - [蒙哥馬里縣](../Page/蒙哥馬利縣_\(愛阿華州\).md "wikilink")：西方

## 人口

[USA_Adams_County,_Iowa_age_pyramid.svg](https://zh.wikipedia.org/wiki/File:USA_Adams_County,_Iowa_age_pyramid.svg "fig:USA_Adams_County,_Iowa_age_pyramid.svg")

### 2010年

根據[2010年人口普查](../Page/2010年美國人口普查.md "wikilink")，亞當斯縣擁有4,029居民，其[人口密度為每平方英里](../Page/人口密度.md "wikilink")9.48居民（每平方公里3.66居民）。\[5\]本縣擁有2,010間房屋单位，而其中的1,715間已有住戶。\[6\]

### 2000年

根據[2000年人口普查](../Page/2000年美國人口普查.md "wikilink")，亞當斯縣擁有4,482居民、1,867住戶和1,235家庭。\[7\]其[人口密度為每平方英里](../Page/人口密度.md "wikilink")11居民（每平方公里4居民）。\[8\]本縣擁有2,109間房屋单位，其密度為每平方英里5間（每平方公里2間）。\[9\]而人口是由98.91%[白人](../Page/歐裔美國人.md "wikilink")、0.07%[黑人](../Page/非裔美國人.md "wikilink")、0.45%[土著](../Page/美國土著.md "wikilink")、0.18%[亞洲人](../Page/亞裔美國人.md "wikilink")、0.04%其他[種族和](../Page/種族.md "wikilink")0.36%[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人佔了人口](../Page/拉丁美洲人.md "wikilink")0.58%。\[10\]

在1,867住户中，有28%擁有一個或以上的兒童（18歲以下）、58%為夫妻、5.50%為單親家庭、33.8%為非家庭、30%為獨居、17.6%住戶有同居長者。平均每戶有2.34人，而平均每個家庭則有2.91人。在4,482居民中，有23.9%為18歲以下、6.3%為18至24歲、24.1%為25至44歲、24.2%為45至64歲以及21.4%為65歲以上。人口的年齡中位數為42歲，女子對男子的性別比為100：96.7。成年人的性別比則為100：92.7。\[11\]

本縣的住戶收入中位數為$30,453，而家庭收入中位數則為$40,030。男性的收入中位數為$26,510
，而女性的收入中位數則為$20,645
，[人均收入為](../Page/人均收入.md "wikilink")$15,550。約6.4%家庭和9.3%人口在[貧窮線以下](../Page/貧窮線.md "wikilink")，包括11.4%兒童（18歲以下）及11.1%長者（65歲以上）。\[12\]

## 參考文獻

## 外部連結

  - [官方網站](http://www.adamscountyia.com/)

[A](../Category/艾奧瓦州行政區劃.md "wikilink")

1.  [2010 census data](http://2010.census.gov/2010census/data/)

2.
3.  [Adams County](http://www.adamscountyia.com/)

4.

5.  ["2010 Census P.L. 94-171 Summary File
    Data"](http://www2.census.gov/census_2010/01-Redistricting_File--PL_94-171/California/).
    United States Census Bureau.

6.

7.  [Population Profile of the United
    States: 2000](http://www.census.gov/population/www/pop-profile/profile2000.html)

8.  [Statistical profile of Alger County,
    Michigan](http://censtats.census.gov/data/MI/05026003.pdf) , United
    States Census Bureau, Census 2000

9.  [State and County QuickFacts](http://quickfacts.census.gov/qfd/)

10. [Census 2000 gateway](http://www.census.gov/main/www/cen2000.html)

11. [American FactFinder](http://factfinder.census.gov/)

12.