**艾哈迈德·哈桑·兹韦勒**（，又译**泽维尔**、**扎威尔**，），[埃及](../Page/埃及.md "wikilink")[化學家](../Page/化學.md "wikilink")，[飛秒化學上的專家](../Page/飛秒化學.md "wikilink")。他研究的技術能將研究化學反應的時間尺度縮減至[飛秒](../Page/飛秒.md "wikilink")，透過攝影將化學反應中每個微細變化忠實地紀錄。

## 早年和教育

艾哈迈德·兹韦勒1946年2月25日生于埃及[达曼胡尔](../Page/达曼胡尔.md "wikilink")，在[Disuq长大](../Page/Disuq.md "wikilink")。他在[亞歷山大大學取得学士學位](../Page/亞歷山大大學.md "wikilink")，后来迁往美国，在[賓夕法尼亞大學取得博士学位](../Page/賓夕法尼亞大學.md "wikilink")。后来在[加州大学伯克利分校完成了博士後工作](../Page/加州大学伯克利分校.md "wikilink")\[1\]。

## 学术生涯

在加州大学伯克利分校作完博士后后，他于1976年被任命为[加州理工学院教师](../Page/加州理工学院.md "wikilink")。1990年，他被选为首任化学物理莱纳斯·鲍林主席\[2\]。1982年他加入美国国籍。

兹韦勒曾被提名并参加奥巴马的[总统科技顾问委员会](../Page/美国总统科技顾问委员会.md "wikilink")\[3\]。

### 研究

泽韦尔主要的工作是[飞秒化学](../Page/飞秒化学.md "wikilink")。用一种超快激光技术，它能够允许描述非常短时间内发生的化学反应——短到可以分析选定的化学反应过渡态\[4\]。

## 獎項

  - 1993年：[沃爾夫化學獎](../Page/沃爾夫化學獎.md "wikilink")
  - 1999年：[諾貝爾化學獎](../Page/諾貝爾化學獎.md "wikilink")，以表揚他在飛秒化學上的成就，是繼1978年[和平獎得主](../Page/諾貝爾和平獎.md "wikilink")[薩達特](../Page/薩達特.md "wikilink")、1988年[文學獎得主](../Page/諾貝爾文學獎.md "wikilink")[納吉布·馬哈富茲後獲諾貝爾獎的埃及人](../Page/納吉布·馬哈富茲.md "wikilink")
  - 1999年：Grand Collar of the Nile

## 外部連結

  - [個人網站](https://web.archive.org/web/20051024002343/http://www.its.caltech.edu/~femto/)
  - [SciScape專題文章：飛秒化學先驅獲頒1999諾貝爾化學獎](https://web.archive.org/web/20060212171133/http://www.sciscape.org/articles/npc99/index.html)

## 逝世

2016年8月2日，亞米德·齊威爾因癌症謝世。

## 参考资料

[Category:亚历山大大学校友](../Category/亚历山大大学校友.md "wikilink")
[Category:賓夕法尼亞大學校友](../Category/賓夕法尼亞大學校友.md "wikilink")
[Category:加州理工学院教授](../Category/加州理工学院教授.md "wikilink")
[Category:美國穆斯林](../Category/美國穆斯林.md "wikilink")
[Category:美国諾貝爾獎獲得者](../Category/美国諾貝爾獎獲得者.md "wikilink")
[Category:埃及諾貝爾獎獲得者](../Category/埃及諾貝爾獎獲得者.md "wikilink")
[Category:诺贝尔化学奖获得者](../Category/诺贝尔化学奖获得者.md "wikilink")
[Category:埃及化学家](../Category/埃及化学家.md "wikilink")
[Category:沃爾夫化學獎得主](../Category/沃爾夫化學獎得主.md "wikilink")
[Category:物理化學家](../Category/物理化學家.md "wikilink")
[Category:皇家学会外籍会员](../Category/皇家学会外籍会员.md "wikilink")
[Category:美国国家科学院院士](../Category/美国国家科学院院士.md "wikilink")
[Category:中国科学院外籍院士](../Category/中国科学院外籍院士.md "wikilink")
[Category:归化美国公民的埃及人](../Category/归化美国公民的埃及人.md "wikilink")
[Category:戴维奖章](../Category/戴维奖章.md "wikilink")
[Category:富兰克林研究所本杰明·富兰克林奖章获得者](../Category/富兰克林研究所本杰明·富兰克林奖章获得者.md "wikilink")
[Category:阿尔伯特·爱因斯坦世界科学奖获得者](../Category/阿尔伯特·爱因斯坦世界科学奖获得者.md "wikilink")
[Category:宗座科学院院士](../Category/宗座科学院院士.md "wikilink")

1.

2.
3.

4.