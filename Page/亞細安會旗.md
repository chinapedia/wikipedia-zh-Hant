[-{zh-hans:东南亚国家联盟;zh-hk:東南亞國家聯盟;zh-sg:亚细安;zh-tw:東南亞國協;}-目前的會旗是在](../Page/东南亚国家联盟.md "wikilink")1993年11月開始使用的。背景為[藍色](../Page/藍色.md "wikilink")，有10棵[稻草在一個被填滿](../Page/稻草.md "wikilink")[紅色的圈內](../Page/紅色.md "wikilink")，這10棵稻草代表了東南亞國協的10個成員國。

\-{zh-hans:东盟;zh-hk:東盟;zh-sg:亚细安;zh-tw:東協;}-的前會旗和目前的非常類似，但只有6棵稻草，這6棵稻草代表了當時的5個創辦國，即[馬來西亞](../Page/馬來西亞.md "wikilink")，[新加坡](../Page/新加坡.md "wikilink")，[印尼](../Page/印尼.md "wikilink")，[菲律賓和](../Page/菲律賓.md "wikilink")[泰國](../Page/泰國.md "wikilink")，第6根稻草則代表之後加入的[汶萊](../Page/汶萊.md "wikilink")。稻草之下也寫著「*ASEAN*」。當時的背景為白色，而非現在的藍色，而圓圈為[黃色](../Page/黃色.md "wikilink")，稻草為[褐色](../Page/褐色.md "wikilink")。

## 外部連結

  - [ASEAN](https://web.archive.org/web/20070125130618/http://www.aseansec.org/7095.htm)
    - Official description of the flag
  - [Flag of the
    World](http://www.crwflags.com/fotw/flags/int-asea.html) -
    Description at FotW along with history

[Category:亞細安](../Category/亞細安.md "wikilink")
[Category:国际组织旗帜](../Category/国际组织旗帜.md "wikilink")
[Category:亚洲旗帜](../Category/亚洲旗帜.md "wikilink")