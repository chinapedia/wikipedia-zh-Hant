[Kung_Heimer_och_Aslög.jpg](https://zh.wikipedia.org/wiki/File:Kung_Heimer_och_Aslög.jpg "fig:Kung_Heimer_och_Aslög.jpg")

**亞絲拉琪**，、Aslög。又名：克萊卡，Kraka、Kráka。是[北歐神話中](../Page/北歐神話.md "wikilink")，[布倫希爾德和](../Page/布倫希爾德.md "wikilink")[齐格弗里德的](../Page/齐格弗里德.md "wikilink")[女兒](../Page/女兒.md "wikilink")。她出現在《[詩體埃達](../Page/詩體埃達.md "wikilink")》、《[沃爾松格傳說](../Page/沃爾松格傳說.md "wikilink")》（Volsunga
saga）、《[朗納爾·洛德布羅克傳奇](../Page/朗納爾·洛德布羅克傳奇.md "wikilink")》（）等故事之中。

神話中，[布倫希爾德和](../Page/布倫希爾德.md "wikilink")[齊格魯德相遇時定下終身](../Page/齊格魯德.md "wikilink")，然後生下了亞絲拉琪。當他們雙雙死亡之後，亞絲拉琪就由布倫希爾德的外祖父[赫默爾](../Page/赫默爾.md "wikilink")（）撫養，赫默爾為了亞絲拉琪的安全，做了一個可以藏住小女孩的[豎琴](../Page/豎琴.md "wikilink")，然後裝成貧窮豎琴演奏師的樣子旅行。途中，赫默爾借宿於農家，農夫阿奇（Aki）和農夫妻子格麗瑪（Grima）認為豎琴中有金子，格麗瑪更教唆阿奇殺掉老人，但之後他們卻只在豎琴中發現一個三歲的小女孩。為避免事端，農夫夫婦決定養大她，他們叫她克萊卡（Kraka），意思為「[烏鴉](../Page/烏鴉.md "wikilink")」，而且只給女孩骯髒的衣服和最差的環境成長。

[Kraka_by_Winge.jpg](https://zh.wikipedia.org/wiki/File:Kraka_by_Winge.jpg "fig:Kraka_by_Winge.jpg")繪，1862年\]\]
亞絲拉琪長大後，非常[美麗](../Page/美麗.md "wikilink")、聰慧，一日沐浴時被[維京人的王](../Page/維京人.md "wikilink")[朗納爾·洛德布羅克](../Page/朗納爾·洛德布羅克.md "wikilink")（Ragnar
Lodbrok）發現，朗納爾為測驗她的智慧，出了一道題：「穿著衣服但仍是裸體，帶著同伴卻仍獨自一人，吃著東西卻仍是空肚」。亞絲拉琪於是披上一張漁網（也有說法是披散著長髮），咬著[洋蔥帶著一隻](../Page/洋蔥.md "wikilink")[狗前往王那裡](../Page/狗.md "wikilink")，朗納爾欣賞她的[智慧](../Page/智慧.md "wikilink")，於是跟她結婚。

當他們[洞房之時](../Page/洞房.md "wikilink")，亞絲拉琪要求朗納爾三天後才能[行房](../Page/行房.md "wikilink")，因為她認為自己身體疲憊，恐影響孩子[健康](../Page/健康.md "wikilink")，但朗納爾不肯。之後出身的[長子果然全身無](../Page/長子.md "wikilink")[硬骨](../Page/硬骨.md "wikilink")，只有[軟骨](../Page/軟骨.md "wikilink")，因此被稱為「[无骨者伊瓦尔](../Page/无骨者伊瓦尔.md "wikilink")」，因瓦爾之後練[箭術](../Page/箭術.md "wikilink")，達到百發百中的地步。他們還生下了幾個兒子：勇敢的[比約恩](../Page/比約恩.md "wikilink")（）、[维塞克](../Page/维塞克.md "wikilink")（Hvitserk）、[拉瓦爾德](../Page/拉瓦爾德.md "wikilink")（）。

之後，朗納爾去[瑞典拜訪時](../Page/瑞典.md "wikilink")，瑞典大臣建議他娶[瑞典](../Page/瑞典.md "wikilink")[公主而休掉亞絲拉琪](../Page/公主.md "wikilink")。這個消息在朗納爾回到家前，就有三隻[鳥告訴了亞絲拉琪](../Page/鳥.md "wikilink")，她憤怒的責備丈夫並坦承自己高貴的身世，為了證明自己的身份，她[預言自己下一個要出生的孩子](../Page/預言.md "wikilink")[眼睛裡會有蛇](../Page/眼睛.md "wikilink")，果真這個[兒子的一隻眼睛內出現了一條白](../Page/兒子.md "wikilink")[蛇](../Page/蛇.md "wikilink")。那條白蛇圍繞著[瞳孔](../Page/瞳孔.md "wikilink")，並咬著自己的[尾巴](../Page/尾巴.md "wikilink")。於是，這個[兒子便被稱為](../Page/兒子.md "wikilink")「蛇眼西格德」\[1\]（Sigurd
Snake-in-the-Eye）。

## 註腳

<div class="references-small">

<references />

</div>

[Category:北歐神話人物](../Category/北歐神話人物.md "wikilink")
[Category:神話中的王后](../Category/神話中的王后.md "wikilink")

1.  參見：[銜尾蛇](../Page/銜尾蛇#北歐神話.md "wikilink")