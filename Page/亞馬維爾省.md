**阿尔马维尔省**位於[亞美尼亞中北](../Page/亞美尼亞.md "wikilink")，與首都[葉里溫](../Page/葉里溫.md "wikilink")、[阿拉加措特恩省](../Page/阿拉加措特恩省.md "wikilink")、[-{zh-hans:阿拉拉特;
zh-hant:亞拉拉特;}-省等省份及](../Page/亞拉拉特省.md "wikilink")[土耳其相鄰](../Page/土耳其.md "wikilink")。省內有一座國際機場。萨达拉帕特战役纪念馆和穆萨达拉电阻纪念馆是该省的主要景点之一。

该省以古代阿马维尔城市命名，亚美尼亚的13个历史城市之一。

## Gallery

<center>

<File:Zvartnots> cathedral ruins.jpg|[Zvartnots
Cathedral](../Page/Zvartnots_Cathedral.md "wikilink") of the 7th
century, a UNESCO World heritage site <File:Shoghakat.jpg>|[Shoghakat
Church](../Page/Shoghakat_Church.md "wikilink") of 1694
<File:Բամբակաշատի> Սուրբ Աստվածածին Եկեղեցի 05.jpg|The church
of [Bambakashat](../Page/Bambakashat.md "wikilink"), 1901
[File:Musaler.jpg|Memorial](File:Musaler.jpg%7CMemorial) to the [Musa
Dagh](../Page/Musa_Dagh.md "wikilink") resistance in
[Musaler](../Page/Musaler.md "wikilink") <File:Սուրբ> Աստվածածին եկեղեցի
(51).JPG|The church of
[Shahumyan](../Page/Shahumyan,_Armavir.md "wikilink") village

</center>

## 外部链接

  - [Armavir Province Tourist
    Guide](http://www.armeniapedia.org/index.php?title=Rediscovering_Armenia_Guidebook-_Armavir_Marz)