**亡神星**\[1\]（**小行星90482**，**奧迦斯**）是[柯伊伯带的天體](../Page/柯伊伯带.md "wikilink")，被發現時的[臨時編號為](../Page/臨時編號.md "wikilink")**2004
DW**，發現者是[加州理工學院的](../Page/加州理工學院.md "wikilink")[米高·布朗](../Page/米高·布朗.md "wikilink")、[雙子星天文台的](../Page/雙子星天文台.md "wikilink")[乍德·特魯希略和](../Page/乍德·特魯希略.md "wikilink")[耶魯大學的](../Page/耶魯大學.md "wikilink")[大衛·拉比諾維茨](../Page/大衛·拉比諾維茨.md "wikilink")。據以認定發現的影像是在2004年2月17日取得的，但往回則追溯到了1951年11月8日的影像。

## 軌道

[TheKuiperBelt_Orbits_Orcus2.svg](https://zh.wikipedia.org/wiki/File:TheKuiperBelt_Orbits_Orcus2.svg "fig:TheKuiperBelt_Orbits_Orcus2.svg")（q）和[遠日點](../Page/遠日點.md "wikilink")（Q）。\]\]
小行星90482是一顆典型的[類冥天體](../Page/類冥天體.md "wikilink")（與[海王星有](../Page/海王星.md "wikilink")2：3[軌道共振關係](../Page/軌道共振.md "wikilink")），軌道的形狀與冥王星相似（兩者的近日點都在[黃道上方](../Page/黃道.md "wikilink")），只是指向的方向不同。因為與[海王星的軌道接近](../Page/海王星.md "wikilink")，所以會有軌道共振，但是與海王星始終保持足夠遠的距離，兩者的離角總在60度以上。

亡神星的軌道離心率和傾角都與冥王星相似，然而與海王星有著相同的共振使它很不自然的總在與冥王星相對的相位上；所以亡神星有時會被描述為*反冥王星*\[2\]。

## 物理性質

### 大小和亮度

小行星90482的[絕對星等是](../Page/絕對星等.md "wikilink")2.3等（另一個古柏帶天體[创神星是](../Page/创神星.md "wikilink")2.6等））。在2007年第一季的報告中，[史匹哲太空望遠鏡在開始運作的前三年中](../Page/史匹哲太空望遠鏡.md "wikilink")，曾經觀察创神星在[遠紅外線的星等](../Page/遠紅外線.md "wikilink")，測量他的大小為946.3<sup>+74.1</sup><sub>-72.3</sub>公里\[3\]。小行星90482的高[反照率約達](../Page/反照率.md "wikilink")20%。

### 顏色和光譜

亡神星表面相對明亮，反照率達30%，呈灰色，且充滿水份。[歐洲南方天文台在](../Page/歐洲南方天文台.md "wikilink")[紅外線觀測的結果](../Page/紅外線.md "wikilink")，與[水冰和](../Page/水冰.md "wikilink")[碳的混合物一致](../Page/碳.md "wikilink")。\[4\]。更進一步，[雙子星天文台觀測的紅外線光譜證實有非常大量的水冰](../Page/雙子星天文台.md "wikilink")，至少覆蓋了表面的15%至30%，
但不會超過50%。這意味著冰的含量低於[卡戎](../Page/冥衛一.md "wikilink")，但不會比[泰坦少](../Page/土衛六.md "wikilink")；為數有限的[甲烷冰](../Page/甲烷冰.md "wikilink")（少於30%）在將來可能會發現其他的成分。\[5\]

即使軌道相似的古柏帶天體，在顏色和光譜上也都會有所變化。亡神星的衛星呈現的自然色彩與紅色的[伊克西翁](../Page/小行星28978.md "wikilink")（在2001年發現的一顆類冥天體）類似。

## 衛星

在2007年2月22日的IAUC 8812公告了衛星的發現
[1](http://www.johnstonsarchive.net/astro/astmoons/am-90482.html)，並且已經確認了衛星的軌道。衛星在距離0.25[秒的距離上被發現](../Page/弧秒.md "wikilink")，亮度低了2.7等\[6\]，假設兩者的[反照率相同](../Page/反照率.md "wikilink")，則衛星的直徑大約是220[公里](../Page/公里.md "wikilink")。2009年3月，发现者布朗通过他的专栏博客公开征集小行星90482衛星的名称，最终选定伊特鲁里亚（亚平宁半岛的原住民族）神话中引导亡魂进入地府的女神——万斯（Vanth）作为卫星的建议名称，并上报IAU。\[7\]

[亡衛一以近乎完美的圓形環繞亡神星公轉](../Page/亡衛一.md "wikilink")，公轉週期約10天。\[8\]發現者布朗認為亡神星與其衛星可能互相被對方[潮汐鎖定](../Page/潮汐鎖定.md "wikilink")，情況就如冥王星和凱倫一樣。\[9\]

## 名稱

依據[國際天文聯合會的](../Page/國際天文聯合會.md "wikilink")[命名慣例](../Page/天體命名#行星的命名.md "wikilink")，與[类冥小天體應該以神話中](../Page/类冥小天體.md "wikilink")[冥府的神來命名](../Page/冥府.md "wikilink")。發現者因此建議以古羅馬[伊特鲁里亚神话的](../Page/伊特鲁里亚神话.md "wikilink")[亡者之神為名](../Page/奥迦斯.md "wikilink")，並且在2004年11月22日獲得[命名委員會的同意](../Page/命名委員會.md "wikilink")，正式命名為奧迦斯（Orcus）。

## 外部連結與參考資料

  - [MPEC 2004-D09](http://cfa-www.harvard.edu/mpec/K04/K04D09.html)
    announcing the discovery but attributing it to [Raymond J.
    Bambery](../Page/Raymond_J._Bambery.md "wikilink"), [Steven H.
    Pravdo](../Page/Steven_H._Pravdo.md "wikilink"), [Michael D.
    Hicks](../Page/Michael_D._Hicks.md "wikilink"), [Kenneth J.
    Lawrence](../Page/Kenneth_J._Lawrence.md "wikilink"), [Daniel
    MacDonald](../Page/Daniel_MacDonald.md "wikilink"), [Eleanor F.
    Helin](../Page/Eleanor_F._Helin.md "wikilink") and [Robert
    Thicksten](../Page/Robert_Thicksten.md "wikilink") /
    [NEAT](../Page/NEAT.md "wikilink")
  - [MPEC 2004-D13](http://cfa-www.harvard.edu/mpec/K04/K04D13.html)
    correcting MPEC 2004-D09
  - [Chad Trujillo's page on 2004
    DW](https://web.archive.org/web/20070322195011/http://www.chadtrujillo.com/2004dw/)
  - [First BBC
    article](http://news.bbc.co.uk/2/hi/science/nature/3506329.stm)
  - [First New Scientist
    article](http://www.newscientist.com/news/news.jsp?id=ns99994702)
  - [Updated orbital elements from Lowell
    Observatory](https://web.archive.org/web/20070522133751/http://www.lowell.edu/users/buie/kbo/astrom/90482.html)
  - [AstDys](http://hamilton.dm.unipi.it/cgi-bin/astdys/astibo?objects:Orcus;main)
    orbital elements
  - [Orbital simulation](http://neo.jpl.nasa.gov/cgi-bin/db?sstr=Orcus)
    from JPL (Java)

## 參考

<references />

[Category:海王星外天體](../Category/海王星外天體.md "wikilink")
[Category:2004年发现的小行星](../Category/2004年发现的小行星.md "wikilink")

1.

2.

3.
4.

5.   [Preprint on arXiv.](http://arxiv.org/abs/astro-ph/0504280)

6.  [Distant
    EKO](http://www.boulder.swri.edu/ekonews/issues/past/n051/html/index.html)
    The Kuiper Belt Electronic newsletter, March 2007

7.

8.
9.