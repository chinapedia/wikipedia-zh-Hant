[Jiaoliping_Station_(Taiwan).jpg](https://zh.wikipedia.org/wiki/File:Jiaoliping_Station_\(Taiwan\).jpg "fig:Jiaoliping_Station_(Taiwan).jpg")下的交力坪車站月台\]\]
**交力坪車站**位於[台灣](../Page/台灣.md "wikilink")[嘉義縣](../Page/嘉義縣.md "wikilink")[竹崎鄉](../Page/竹崎鄉.md "wikilink")，為[林務局](../Page/行政院農業委員會林務局.md "wikilink")[阿里山森林鐵路](../Page/阿里山森林鐵路.md "wikilink")[阿里山線之](../Page/阿里山線.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")，本站過去曾為上山和下山班車的交會地點。

## 車站構造

左邊是辦公室，右邊是候車室。

## 利用狀況

  - 阿里山線(主線)的停靠站。

## 車站週邊

  - [瑞太古道](../Page/瑞太古道.md "wikilink")

  -
## 歷史

  - 1912年10月1日：設站。

## 外部連結

  - [交力坪車站（阿里山森林鐵路）](http://railway.forest.gov.tw/ContentView.aspx?Type=11)
  - [交力坪車站（竹崎鄉公所）](https://web.archive.org/web/20111003145713/http://www.chuchi.gov.tw/03tour/01view.asp?id=388)

## 鄰近車站



[Category:竹崎鄉鐵路車站](../Category/竹崎鄉鐵路車站.md "wikilink")