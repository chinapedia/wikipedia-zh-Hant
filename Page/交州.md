**交州**是古地名，其范围在其近八百年的历史中常有变化，通常包括現在的[越南北](../Page/越南.md "wikilink")、中部和[中国](../Page/中国.md "wikilink")[广西壮族自治区的一部分](../Page/广西壮族自治区.md "wikilink")。有时还包括現在的中国[广东省](../Page/广东省.md "wikilink")、[海南省](../Page/海南省.md "wikilink")。

## 汉代

在战国时期，在[长江以南的各个古代部落被中原文化统称为](../Page/长江.md "wikilink")[百越](../Page/百越.md "wikilink")。在今天的[广东](../Page/广东.md "wikilink")、[广西](../Page/广西.md "wikilink")、[越南北部等地生活着](../Page/越南.md "wikilink")[南越](../Page/南越.md "wikilink")、[西瓯](../Page/西瓯.md "wikilink")、[骆越等百越族支系的原始部落](../Page/骆越.md "wikilink")。约前218年，[秦朝君主](../Page/秦朝.md "wikilink")[秦始皇派遣](../Page/秦始皇.md "wikilink")[屠睢率领](../Page/屠睢.md "wikilink")50万秦军分五路进攻[岭南和](../Page/岭南.md "wikilink")[闽越地区的百越族](../Page/闽越.md "wikilink")。前214年，秦始皇派遣[任嚣](../Page/任嚣.md "wikilink")、[赵佗率领秦军重新发动攻势](../Page/赵佗.md "wikilink")，很快将岭南地区完全纳入了秦朝的版图。同年，秦朝在岭南地区设立了[南海郡](../Page/南海郡.md "wikilink")、[桂林郡](../Page/桂林郡.md "wikilink")、[象郡三郡](../Page/象郡.md "wikilink")。前206年秦帝国灭亡。约前203年，赵佗占据南海，兼併桂林郡和象郡，在岭南地区建立[南越国](../Page/南越国.md "wikilink")。析象郡为[交趾](../Page/交趾.md "wikilink")、[九真两郡](../Page/九真.md "wikilink")。

[西汉时](../Page/西汉.md "wikilink")，前111年（[漢武帝](../Page/漢武帝.md "wikilink")[元鼎六年](../Page/元鼎.md "wikilink")）汉平[南越国](../Page/南越国.md "wikilink")，在原南越国地方设[交趾刺史部](../Page/交趾刺史部.md "wikilink")，是汉代十三[刺史部之一](../Page/刺史部.md "wikilink")，也是汉朝最南部疆域。交州设有[南海郡](../Page/南海郡.md "wikilink")、[蒼梧郡](../Page/蒼梧郡.md "wikilink")、[鬱林郡](../Page/鬱林郡.md "wikilink")、[合浦郡](../Page/合浦郡.md "wikilink")、[交阯郡](../Page/交阯郡.md "wikilink")、[九真郡](../Page/九真郡.md "wikilink")、[日南郡](../Page/日南郡.md "wikilink")、[珠崖郡](../Page/珠崖郡.md "wikilink")、[儋耳郡共九個](../Page/儋耳郡.md "wikilink")[郡](../Page/郡.md "wikilink")\[1\]，其中在今天的越南部分交州有三郡：**交趾**（一作**交阯**，今越南[东京](../Page/东京_\(越南\).md "wikilink")，即越南北部）、**九真**（今越南[清化省](../Page/清化省.md "wikilink")、[乂安省](../Page/乂安省.md "wikilink")、[河靜省](../Page/河靜省.md "wikilink")、[廣平省](../Page/廣平省.md "wikilink")，即越南中北部）、**日南**（郡治今越南[顺化以南](../Page/顺化.md "wikilink")，即越南中部及以南一部分）五十六县。州治在广信[苍梧即今](../Page/苍梧.md "wikilink")[梧州](../Page/梧州.md "wikilink")，后孙权析交州北部置广州，交州州治南迁交阯郡[龙编县](../Page/龙编县.md "wikilink")（今[越南](../Page/越南.md "wikilink")[北宁省](../Page/北宁省.md "wikilink")）。

[东汉初年当地人在](../Page/东汉.md "wikilink")[徵氏姐妹的带领下建立了三年割據的政权](../Page/徵氏姐妹.md "wikilink")，后被[马援平叛](../Page/马援.md "wikilink")。东汉末年（192年），[占族人](../Page/占族.md "wikilink")[区连造反](../Page/区连.md "wikilink")，杀死[汉朝日南郡](../Page/汉朝.md "wikilink")[象林縣令](../Page/象林縣.md "wikilink")，占据了原日南郡的大部分地区（越南中部），以[婆罗门教为国教](../Page/婆罗门教.md "wikilink")，建立[占婆国](../Page/占婆.md "wikilink")，和中国以[顺化为界](../Page/顺化.md "wikilink")。203年，交趾正式改为**[交州刺史部](../Page/交州刺史部.md "wikilink")**\[2\]。

## 三国

在[東漢末年中原大亂時](../Page/東漢.md "wikilink")，交州在[士燮的統治下](../Page/士燮.md "wikilink")，成為相對而言和平安定的地區，包括今天的廣東、廣西及越南北部。許多中原人士移入當地，如[许靖](../Page/许靖.md "wikilink")、[袁沛](../Page/袁沛.md "wikilink")、邓小孝、徐元贤、张子云、[许慈](../Page/许慈.md "wikilink")、[刘巴](../Page/刘巴.md "wikilink")、[程秉](../Page/程秉.md "wikilink")、[薛综](../Page/薛综.md "wikilink")、袁忠、桓邵等。这對於漢人中原文化傳入交州有很大的助益。这些交州的人才之后很多到[三国各政权中位居高官](../Page/三国.md "wikilink")。交州作为[益州](../Page/益州.md "wikilink")、[荆州](../Page/荆州.md "wikilink")、[扬州的南鄰而被](../Page/扬州_\(古代\).md "wikilink")[曹操](../Page/曹操.md "wikilink")、[刘表](../Page/刘表.md "wikilink")、[刘备](../Page/刘备.md "wikilink")、[孙权等势力争夺](../Page/孙权.md "wikilink")。

211年（[漢獻帝](../Page/漢獻帝.md "wikilink")[建安十六年](../Page/建安.md "wikilink")），趁刘备西征益州之际，[东吴派遣大将](../Page/东吴.md "wikilink")[步骘为交州刺史](../Page/步骘.md "wikilink")。交州成了东吴的势力范围。士燮將兒子收至[东吴為人質](../Page/东吴.md "wikilink")，並每年貢獻當地寶物以維持其政權。[孫權加封其為左將軍](../Page/孫權.md "wikilink")。

220年（漢獻帝[延康元年](../Page/延康.md "wikilink")）[呂岱代步騭為交州刺史](../Page/呂岱.md "wikilink")。223年刘备死后，士燮诱导益州豪姓[雍闿反叛归附东吴](../Page/雍闿.md "wikilink")。[诸葛亮平叛继而占领](../Page/诸葛亮.md "wikilink")[南中之后派](../Page/南中.md "wikilink")[李恢领交州刺史](../Page/李恢.md "wikilink")，争夺交州。226年士燮去世後，吳國将[南岭以南诸郡以今天广西](../Page/南岭.md "wikilink")[北海市](../Page/北海市.md "wikilink")[合浦为界](../Page/合浦.md "wikilink")，以北[广州](../Page/广州_\(古代\).md "wikilink")，以南为交州。呂岱、[戴良分别为广州](../Page/戴良.md "wikilink")、交州刺史，士燮的儿子[士徽为](../Page/士徽.md "wikilink")[九真太守](../Page/九真.md "wikilink")。士徽不服反叛，呂岱帶兵攻入交州、九真，之后重新把广州和交州合并。

263年，[蜀汉灭亡之后](../Page/蜀汉.md "wikilink")，次年（[魏元帝](../Page/魏元帝.md "wikilink")[咸熙元年](../Page/咸熙.md "wikilink")）交州[吕兴等造反](../Page/吕兴.md "wikilink")，试图得到[曹魏的支持](../Page/曹魏.md "wikilink")，但被很快镇压。[西晋继承曹魏之后](../Page/西晋.md "wikilink")，通过益州、南中派[杨稷等领兵到交州](../Page/杨稷.md "wikilink")，大破吳軍。东吴大都督[薛珝](../Page/薛珝.md "wikilink")、[苍梧太守](../Page/苍梧.md "wikilink")[陶璜等率兵击败西晋](../Page/陶璜.md "wikilink")，重新掌握交州。

## 晋、南朝

280年[晋朝统一中国](../Page/晋朝.md "wikilink")，[陶璜等降晋](../Page/陶璜.md "wikilink")。之后交州依附于[南朝各朝代](../Page/南朝.md "wikilink")。542年龙兴（今越南[太平省](../Page/太平省.md "wikilink")）人[李賁造反](../Page/李賁.md "wikilink")。544年建国号为“万春”，越南稱[前李朝](../Page/前李朝.md "wikilink")。次年，[梁朝大将](../Page/梁朝.md "wikilink")[陈霸先镇压平亂](../Page/陈霸先.md "wikilink")。之后陈霸先班师回朝篡位，建立[陈朝](../Page/陈朝.md "wikilink")。

## 隋唐

571年李贲的兄長[李天宝手下](../Page/李天宝.md "wikilink")[李佛子占据交州](../Page/李佛子.md "wikilink")。后来因李佛子不愿去朝觐[隋朝被隋朝军队击败俘虏](../Page/隋朝.md "wikilink")，押解回朝。交州归隋朝统治。

[唐朝分](../Page/唐朝.md "wikilink")[岭南为](../Page/岭南.md "wikilink")[广州](../Page/廣州_\(古代\).md "wikilink")、[桂州](../Page/桂州.md "wikilink")、[容州](../Page/容州.md "wikilink")、[邕州和交州五个都护府](../Page/邕州.md "wikilink")，简称岭南五管。交州辖今越南等地。624年（[唐高祖](../Page/唐高祖.md "wikilink")[武德七年](../Page/武德.md "wikilink")）改称都督府，679年（[唐高宗](../Page/唐高宗.md "wikilink")[儀鳳四年](../Page/儀鳳.md "wikilink")）又改为安南都护府。从此交州便正式被称作[安南](../Page/安南都護府.md "wikilink")。

## 后续历史

**交州**或**交趾**的名称在此之后又出现了几次。939年当地军人[吳權叛亂](../Page/吳權.md "wikilink")，击败中国[五代十国时的](../Page/五代十国.md "wikilink")[南汉军队](../Page/南汉.md "wikilink")。968年新興勢力領袖[丁部領建立](../Page/丁部領.md "wikilink")「[大瞿越](../Page/大瞿越.md "wikilink")」國，兩年後自稱[皇帝並使用](../Page/皇帝.md "wikilink")[年號](../Page/年號.md "wikilink")，取得事實上的獨立。但后来先后接受[宋朝的册封为](../Page/宋朝.md "wikilink")「交趾郡王」、「安南国王」。

13世纪，[蒙古帝国](../Page/蒙古帝国.md "wikilink")、[元朝曾多次大规模進攻该地但均失败](../Page/元朝.md "wikilink")。元朝为征服该地曾设立[交趾行省](../Page/交趾行省.md "wikilink")。

[明成祖时期](../Page/明成祖.md "wikilink")，越南[陳朝君主遭外戚](../Page/陳朝_\(越南\).md "wikilink")[胡氏篡奪](../Page/胡季犛.md "wikilink")，國內混亂，明朝應陳朝的遺臣請求推翻胡氏政權。1407年至1428年明軍征服越南。之後明在此進行[直接統治](../Page/安南屬明時期.md "wikilink")，設郡县、置[交趾承宣布政使司](../Page/交趾承宣布政使司.md "wikilink")，在越南采取移风易俗及推行儒学教化的政策，激起了越南的反抗行動。在成祖驾崩不久，1428年越南[后黎朝击败明军重新恢復獨立](../Page/后黎朝.md "wikilink")，但仍維持與中原政權的宗藩關係。

之后越南的政权几经分裂、合并、扩张，吞并了[占婆和](../Page/占婆.md "wikilink")[下高棉](../Page/湄公河三角洲.md "wikilink")，取得了除了从前交州之外的今天越南的中南部和南部[湄公河三角洲地区](../Page/湄公河.md "wikilink")。

19世纪初[阮朝的](../Page/阮朝.md "wikilink")[阮福映被](../Page/阮福映.md "wikilink")[清代](../Page/清代.md "wikilink")[嘉庆帝册封为](../Page/嘉庆帝.md "wikilink")「越南国王」。

[法国的殖民统治之後逐步走向獨立](../Page/法国.md "wikilink")，1945年[日軍的軍事佔領結束](../Page/日軍.md "wikilink")，開始出現了[北越](../Page/北越.md "wikilink")、[南越兩政權](../Page/南越.md "wikilink")，1975年北越在[中共的大力援助下擊敗了有](../Page/中共.md "wikilink")[美軍助陣的南越](../Page/美軍.md "wikilink")，[越南戰爭結束](../Page/越南戰爭.md "wikilink")，[越南社会主义共和国成立](../Page/越南社会主义共和国.md "wikilink")。

## 参考文献

## 參見

  - [交趾郡](../Page/交趾郡.md "wikilink")
  - [交趾支那](../Page/交趾支那.md "wikilink")
  - [越南历史](../Page/越南历史.md "wikilink")
  - [越南北屬時期](../Page/越南北屬時期.md "wikilink")
  - [廣州](../Page/广州_\(古代\).md "wikilink")

{{-}}

[Category:南朝的州](../Category/南朝的州.md "wikilink")
[Category:隋朝的州](../Category/隋朝的州.md "wikilink")
[Category:唐朝的州](../Category/唐朝的州.md "wikilink")
[Category:广西的州](../Category/广西的州.md "wikilink")
[Category:广东的州](../Category/广东的州.md "wikilink")
[Category:海南的州](../Category/海南的州.md "wikilink")
[Category:越南古代的州](../Category/越南古代的州.md "wikilink")

1.  [《漢書 地理志第八下》](../Page/s:漢書/卷028下.md "wikilink")
2.  [《晉書 卷十五
    志第五》武帝元鼎六年](../Page/s:晉書/卷015.md "wikilink")，讨平吕嘉，以其地为南海、苍梧、郁林、合浦、日南、九真、交趾七郡，盖秦时三郡之地。元封中，又置儋耳、珠崖二郡，置交趾刺史以督之。昭帝始元五年，罢儋耳并珠崖。元帝初元三年，又罢珠崖郡。后汉马援平定交部，始调立城郭置井邑。顺帝永和九年，交趾太守周敞求立为州，朝议不许，即拜敞为交趾刺史。桓帝分立高兴郡，灵帝改曰高凉。建安八年，张津为刺史，土燮交趾太守，共表立为州，乃拜津为交州牧。