[Inside_the_Stock_Exchange_2012.jpg](https://zh.wikipedia.org/wiki/File:Inside_the_Stock_Exchange_2012.jpg "fig:Inside_the_Stock_Exchange_2012.jpg")
《**交易現場**》（）是[香港](../Page/香港.md "wikilink")[電視廣播有限公司](../Page/電視廣播有限公司.md "wikilink")[新聞及資訊部製作的財經新聞節目](../Page/無綫新聞.md "wikilink")。現時節目每逢港股交易日在該公司旗下頻道[翡翠台](../Page/翡翠台.md "wikilink")、[明珠台和](../Page/明珠台.md "wikilink")[無綫財經·資訊台播出](../Page/無綫財經·資訊台.md "wikilink")。

香港其他電視台同類節目有[香港有線電視的](../Page/香港有線電視.md "wikilink")《[交易所直播室](../Page/交易所直播室.md "wikilink")》

## 各頻道版本

### [翡翠台](../Page/翡翠台.md "wikilink")

在2006年2月6日，即[香港交易所完成翻新工程當天首播](../Page/香港交易所.md "wikilink")。節目現由與[無綫電視關係密切的](../Page/無綫電視.md "wikilink")[上海商業銀行特約贊助播出](../Page/上海商業銀行.md "wikilink")。在港交所直播室進行直播。於港股交易日分五個時段（包括09:30、11:40、12:45、14:45、16:20）播放，除了09:30的時段有其四十五分鐘嘉賓分析，以及12:45的時段有嘉賓分析後市外，其餘是與《[財經第一線](../Page/財經第一線.md "wikilink")》近似的股市消息提要。

由2008年1月2日起，第一個時段的《交易現場》延長十五分鐘播放（09:30-10:15），並加強節目內容。除接聽更多觀眾來電和解答電郵外，亦提供嘉賓股份推介。

由2008年7月14日起，《交易現場》節目改用16:9技術製作。

2011年3月7日起，為配合[香港交易所提早港股開市時間](../Page/香港交易所.md "wikilink")，第一個時段的《交易現場》提早至09:00-09:45播出。

由2013年4月2日起，《交易現場》節目改以高清製作。

由2014年11月17日起，由於09:30-09:45播出《[滬港互通](../Page/滬港互通.md "wikilink")》（2016年12月5日起改為《[深港滬通](../Page/深港滬通.md "wikilink")》），滬港通及深港通交易日第一個時段的《交易現場》縮短至半小時播放。若當日為港股交易日及非滬港通、深港通交易日，第一個時段的《交易現場》的時長仍為45分鐘。

### [明珠台](../Page/明珠台.md "wikilink")

[普通話版本的](../Page/普通話.md "wikilink")《交易現場》在2007年9月17日於[明珠台啟播](../Page/明珠台.md "wikilink")，由10:30開始播放，為半小時版本，與[翡翠台](../Page/翡翠台.md "wikilink")09:00那節差不多；其後三個時段（11:30、14:00、15:58）會有約1-2分鐘的版本，由交易所主持交代股市最新情況。其餘時間（11:15開始）均由節目《[金融行情](../Page/金融行情.md "wikilink")》（）更新股票市場最新消息。

2013年4月2日起，普通話版本《交易現場》節目改以高清製作。

2018年7月23日起，[普通話版本](../Page/普通話.md "wikilink")《交易現場》節目改以一小時播放。播映時段為10:00-11:00及13:30-14:30。

2019年1月2日起，普通話版本《交易現場》新增15:55-16:00播映時段。

2019年1月17日起，所有明珠台《交易現場》更名為《交易直播室》（），播映時段不變。

### [新聞2台](../Page/直播新聞台.md "wikilink")

節目播放時間為10:20-10:45。但已因應[明珠台播放這個節目而停止播放](../Page/明珠台.md "wikilink")。

### [高清翡翠台](../Page/高清翡翠台.md "wikilink")／[J5](../Page/J5.md "wikilink")／[無綫財經台](../Page/無綫財經台.md "wikilink")／[無綫財經·資訊台](../Page/無綫財經·資訊台.md "wikilink")

由2008年1月2日起啟播，節目每節約5-10分鐘左右（10:00、11:50、15:05、16:30），特約贊助和翡翠台相同。其間則播出節目《[智富360°](../Page/智富360°.md "wikilink")》。

由2008年7月14日起，《交易現場》節目改用16:9技術製作。

由2012年1月3日起，16:30播出的一節《交易現場》取消。

由2013年4月2日起，《交易現場》節目改以高清製作。

2012年7月以前，當[高清翡翠台需要播映](../Page/高清翡翠台.md "wikilink")[NBA等直播賽事時](../Page/NBA.md "wikilink")，節目將暫時改於[J2播映](../Page/J2.md "wikilink")。（該項措施已因無綫電視不再擁有NBA在香港的播映權而取消）

2014年11月17日至2016年2月19日，由於10:10-10:15播出《[滬港直播室](../Page/滬港直播室.md "wikilink")》，10:05的一節《交易現場》提早至10:00-10:10播出。

隨著[高清翡翠台](../Page/高清翡翠台.md "wikilink")2016年2月22日改名為[J5](../Page/J5.md "wikilink")，播映時間同時改為10:05、13:25、15:10，每節約5分鐘。[J5於](../Page/J5.md "wikilink")2017年8月15日改名為無綫財經台，該台於2018年1月20日再改名為[無綫財經·資訊台](../Page/無綫財經·資訊台.md "wikilink")，播映時間不變。

### 與台灣[TVBS合作](../Page/TVBS_\(頻道\).md "wikilink")

2011年8月1日，台灣[TVBS的財經新聞節目](../Page/TVBS_\(頻道\).md "wikilink")《創富新聞》開播（前身為《創富報導》），固定於每日下午1:45連線香港[TVB](../Page/TVB.md "wikilink")《交易現場》明珠台普通話版主播[潘姿伶播報約](../Page/潘姿伶.md "wikilink")10分鐘的港股行情。

## 主持人

### 粵語：[新聞2台](../Page/直播新聞台.md "wikilink")（已停播）、[翡翠台](../Page/翡翠台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")、[J5](../Page/J5.md "wikilink")、[無綫財經台](../Page/無綫財經台.md "wikilink")、[無綫財經·資訊台](../Page/無綫財經·資訊台.md "wikilink")

現任

  -   - [伍嘉文](../Page/伍嘉文.md "wikilink")
      - [李嘉惠](../Page/李嘉惠.md "wikilink")
      - [劉雅文](../Page/劉雅文.md "wikilink")
      - [陳志嘉](../Page/陳志嘉.md "wikilink")
      - [王瑩](../Page/王瑩.md "wikilink")
      - [曾熙雯](../Page/曾熙雯.md "wikilink")
      - [謝家俊](../Page/謝家俊.md "wikilink")
      - [楊暢](../Page/楊暢.md "wikilink")
      - [張偉麟](../Page/張偉麟.md "wikilink")
      - [翁家揚](../Page/翁家揚.md "wikilink")
      - [丘紫薇](../Page/丘紫薇.md "wikilink")

前任

  -   - [范巧茹](../Page/范巧茹.md "wikilink")
      - [伍思齊](../Page/伍思齊.md "wikilink")
      - [林小珍](../Page/林小珍.md "wikilink")
      - [陳聞賢](../Page/陳聞賢.md "wikilink")
      - [陳正犖](../Page/陳正犖.md "wikilink")
      - [潘禮瑤](../Page/潘禮瑤.md "wikilink")

### 普通話：[明珠台](../Page/明珠台.md "wikilink")

現任

  -   - [潘姿伶](../Page/潘姿伶.md "wikilink")
      - [張晉](../Page/張晉_\(主持\).md "wikilink")
      - [林競](../Page/林競.md "wikilink")
      - [楊暢](../Page/楊暢.md "wikilink")

前任

  -   - [段潔](../Page/段潔.md "wikilink")
      - [孫霏](../Page/孫霏.md "wikilink")

## 嘉賓

  - [黃智慧](../Page/黃智慧.md "wikilink")（Trury）金利豐證券研究部經理
  - [熊麗萍](../Page/熊麗萍.md "wikilink")（Conita）獨立股評人
  - [沈振盈](../Page/沈振盈.md "wikilink")（沈大師）訊匯證券行政總裁
  - [郭思治](../Page/郭思治.md "wikilink")（郭Sir）[民眾證券董事總經理](../Page/民眾證券.md "wikilink")
  - [彭偉新](../Page/彭偉新.md "wikilink")（Castor）[京華山一研究部主管](../Page/京華山一.md "wikilink")
  - [梁偉源](../Page/梁偉源.md "wikilink")（Steven）大華繼顯執行董事
  - [黃敏碩](../Page/黃敏碩.md "wikilink")（Michael）康證有限公司董事
  - [李偉傑](../Page/李偉傑.md "wikilink")（Jason）獨立股評人
  - [黃志陽](../Page/黃志陽.md "wikilink")（Jackson）獨立股評人
  - [潘鐵珊](../Page/潘鐵珊.md "wikilink")（Patrick）[亞洲創富證券行政總裁](../Page/亞洲創富.md "wikilink")
  - [沈慶洪](../Page/沈慶洪.md "wikilink")（Patrick）[騰祺基金管理投資管理董事](../Page/騰祺基金.md "wikilink")
  - [姚浩然](../Page/姚浩然.md "wikilink")（Patrick）[時富資產管理董事總經理](../Page/時富.md "wikilink")
  - [歐業亨](../Page/歐業亨.md "wikilink")（Victor）
    [恩霖資產管理董事](../Page/恩霖資產.md "wikilink")
  - [葉尚志](../Page/葉尚志.md "wikilink")（Linus）第一[上海市場首席策略師](../Page/上海.md "wikilink")
  - [李惠嫻](../Page/李惠嫻.md "wikilink")（Hannah）大華繼顯香港策略師
  - [鄺民彬](../Page/鄺民彬.md "wikilink")（Ben）[凱基證券執行董事](../Page/凱基證券.md "wikilink")
  - [謝明光](../Page/謝明光.md "wikilink")
    [匯盈證券董事及總經理](../Page/匯盈證券.md "wikilink")
  - [蘇顯邦](../Page/蘇顯邦.md "wikilink")
    [創陞證券行政總裁](../Page/創陞證券.md "wikilink")
  - [盧志明](../Page/盧志明.md "wikilink")（Ken） 大唐金融副總裁
  - [蘇沛豐](../Page/蘇沛豐.md "wikilink")（Daniel） 招銀國際策略師
  - [鄧聲興](../Page/鄧聲興.md "wikilink")（Kenny）君陽證券行政總裁
  - [連敬涵](../Page/連敬涵.md "wikilink")（Kingston）小牛金服行政總裁
  - [藺常念](../Page/藺常念.md "wikilink")（Francis）智易東方證券行政總裁
  - [羅尚沛](../Page/羅尚沛.md "wikilink")（Eugene）銀河證券業務部發展董事
  - [溫天納](../Page/溫天納.md "wikilink")（Ronald）博大資本國際投資銀行業務總裁
  - [陸庭龍](../Page/陸庭龍.md "wikilink")（Alan）[恒生銀行私人銀行及信託服務主管](../Page/恒生銀行.md "wikilink")
  - [郭家耀](../Page/郭家耀.md "wikilink")（Matthew）中國銀盛財富管理首席策略師
  - [白韌](../Page/白韌.md "wikilink")（Peter）[中銀國際證券部執行董事](../Page/中銀國際.md "wikilink")
  - [黃國英](../Page/黃國英.md "wikilink")（Alex）[豐盛融資有限公司資產管理公司董事](../Page/豐盛融資有限公司.md "wikilink")
  - [陳炳強](../Page/陳炳強.md "wikilink")（Thomas）[悠誠資產管理首席投資總監](../Page/悠誠資產.md "wikilink")
  - [涂國彬](../Page/涂國彬.md "wikilink")（Mark）[永豐金融研究部主管](../Page/永豐金融.md "wikilink")
  - [趙晞文](../Page/趙晞文.md "wikilink")（Hayman）信達國際研究部董事
  - [陳煜強](../Page/陳煜強.md "wikilink")（Y.K.）[輝立資本管理策略師及基金經理](../Page/輝立資本管理.md "wikilink")
  - [張益祖](../Page/張益祖.md "wikilink")（Gilbert）[鴻昇證券有限公司董事兼總經理](../Page/鴻昇證券有限公司.md "wikilink")
  - [植耀輝](../Page/植耀輝.md "wikilink")（Stanley）[耀才證券研究部經理](../Page/耀才證券.md "wikilink")
  - [林嘉麒](../Page/林嘉麒.md "wikilink")（KK）[宏滙資產管理董事](../Page/宏滙資產.md "wikilink")
  - [盧志威](../Page/盧志威.md "wikilink")（William）[豐盛融資組合交易經理](../Page/豐盛融資.md "wikilink")
  - [陳鳳珠](../Page/陳鳳珠.md "wikilink") 華金研究國際研究部總監
  - [黃麗幗](../Page/黃麗幗.md "wikilink")（Ruby）海通國際股票衍生產品銷售助理副總裁
  - [麥嘉嘉](../Page/麥嘉嘉.md "wikilink")（Avis）志昇證券研究部董事

## 各頻道查詢熱線電話及電郵

於翡翠台09:00的一節及明珠台播出的交易現場會向觀眾提供熱線電話及電郵向嘉賓解答投資疑難。

### 翡翠台

  - 電話：1831388
  - 即時通訊軟件（微信、whasapp等）：96663247
  - 電郵：hkstock@tvb.com.hk

### 明珠台

  - 電話：1831399
  - 即時通訊軟件（微信、whasapp等）：96663247
  - 電郵：pth.stock@tvb.com.hk

## 主題音樂

  - [Cybernews](https://www.youtube.com/watch?v=xoq4ojKFP78&list=PL83205487CA345097&index=7)（此音樂也曾用於台灣[東森新聞台整點新聞下節預告的配樂](../Page/東森新聞台.md "wikilink")）

## 參見

[Category:無綫新聞節目](../Category/無綫新聞節目.md "wikilink")
[Category:香港電視新聞節目](../Category/香港電視新聞節目.md "wikilink")
[Category:香港股市](../Category/香港股市.md "wikilink")
[Category:財經節目](../Category/財經節目.md "wikilink")