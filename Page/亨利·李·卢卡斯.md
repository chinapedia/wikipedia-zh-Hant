**亨利·李·卢卡斯**（，），[美国人](../Page/美国人.md "wikilink")，出生于[弗吉尼亚州](../Page/弗吉尼亚州.md "wikilink")[黑堡镇](../Page/黑堡_\(弗吉尼亚州\).md "wikilink")，被该州[高等法庭裁定曾犯下多起杀人罪行之凶犯](../Page/法庭.md "wikilink")。

卢卡斯曾向[弗州](../Page/弗吉尼亚州.md "wikilink")[警方供稱他自](../Page/警方.md "wikilink")1975年中期至1983年年杀害了三千多人。之后经「卢卡斯重案组」（Lucas
Task
Force）反复核对，认为比较可信的数字可能是三百五十人。然而主审的[德州](../Page/德州.md "wikilink")[检察官Jim](../Page/检察官.md "wikilink")
Mattox 认为此数字依然太耸人听闻，并以此抨擊重案组办案不力，听信谣传。

1998年当时的[德州](../Page/德州.md "wikilink")[州长](../Page/美国州长.md "wikilink")[乔治·沃克·布什批准将卢卡斯的](../Page/乔治·沃克·布什.md "wikilink")[死刑改](../Page/死刑.md "wikilink")[判为](../Page/判决.md "wikilink")[终身监禁](../Page/终身监禁.md "wikilink")，三年后已經64歲的卢卡斯於狱中因[心脏病发作過世](../Page/心脏病.md "wikilink")。

虽然卢卡斯被法庭指控非一人犯案，尚有同党帮凶[奥蒂斯·荼勒](../Page/奥蒂斯·荼勒.md "wikilink")（Ottis
Toole），
但就某些已定罪的案件来看，若說卢卡斯是美国史上杀人最多、手段最残忍並且最猖狂的「[连环杀手](../Page/连环杀手.md "wikilink")」，也是名副其實的。

## 早年经历

卢卡斯1936年8月3日出生于[弗吉尼亚州](../Page/弗吉尼亚州.md "wikilink")[黑堡镇](../Page/黑堡_\(弗吉尼亚州\).md "wikilink")，其生母维奥拉·卢卡斯是个经常使用[暴力的](../Page/暴力.md "wikilink")[妓女](../Page/妓女.md "wikilink")，而生父安德森・卢卡斯原是[铁道公司小职员](../Page/铁道.md "wikilink")，在一次事故失去双脚后经常酗酒。生母对生活十分不满，经常向着他和异父之兄弟发泄。他自小已经习惯观看母親和其他男人鬼混。在他的回忆中，他曾因母亲用木板打破他的头而三日昏迷不醒。另一次父亲的朋友送给他们一头[驴](../Page/驴.md "wikilink")，其母见到后马上用[枪将驴击毙](../Page/枪.md "wikilink")。

卢卡斯后来招认第一次的谋杀是发生于1951年，他杀死一名反抗他[强奸意图的女生](../Page/强奸.md "wikilink")，然而之後他又再次翻供。卢于1954年在[弗州](../Page/弗吉尼亚州.md "wikilink")[里士满市因](../Page/里士满_\(维吉尼亚州\).md "wikilink")[入屋盗窃罪被捕](../Page/盗窃.md "wikilink")，并获判入狱六年，其间从[监狱逃跑并再被拘捕](../Page/监狱.md "wikilink")。

## 弑杀生母

在1960年1月12日，盧卡斯在不斷的爭吵下也許是意外的殺死了母親。這次爭吵的起因是關於盧卡斯是否該回家照顧他逐漸年邁的母親。盧卡斯聲稱她用掃帚打他的頭，然後盧卡斯用刀刺向她的頸部後，母親便倒地。盧卡斯立即逃走，在之後他說到:「我所記得的是一直打她的頸，但後來我看到她倒地便打算抓住她。但返回要讓她起來之時，才發現她已經死了。然後我發覺自己手上的小刀，原來她是被割傷倒地。」

事實上盧卡斯的母親還沒有死，當盧卡斯的姊姊 Opal
回家之後，發現母親還在一灘血上苟延殘喘著。她叫了[救護車但為時已晚](../Page/救護車.md "wikilink")，警察的官方報告上寫道她的死是因為襲擊而引發的[心臟病](../Page/心臟病.md "wikilink")。

盧卡斯回到[維珍尼亞州](../Page/維珍尼亞州.md "wikilink")，再駕車回到[密西根州](../Page/密西根州.md "wikilink")，但在[俄亥俄州時因密西根州的逮捕狀而被逮捕](../Page/俄亥俄州.md "wikilink")。

盧卡斯聲稱攻擊他母親是出自防衛，但這項自辯被駁回，他被判二級謀殺，在[密西根州監禁](../Page/密西根州.md "wikilink")20至40年。他服刑十年半後在1970年6月獲得釋放。

盧卡斯在[美國南部流浪](../Page/美國南部.md "wikilink")，也從事過一些短期工作。在1976年至1978年於[佛羅里達州和](../Page/佛羅里達州.md "wikilink")
Ottis Toole 相識，並自稱和處於[青春期](../Page/青春期.md "wikilink")，曾逃出少年拘留所的外甥女
Frieda Powell 有過親密行為。盧卡斯和 Toole 有時會叫 Powell為「Becky」，其中一半目的是要掩飾其身份，因為
Powell 喜歡這名字多於她原本的名字。

盧卡斯和 Powell 據報導是情侶，盧卡斯後來說他在這段時間殺了超過一百人，有些是 Toole
的同伴。三人後來在[德州的](../Page/德州.md "wikilink")「Stoneburg」安頓，在一個名為「祈禱之屋」（The
House of Prayer）的宗教社區居住下來，而社區負責人「Ruben Moore」也為盧卡斯找到一份蓋屋頂的工作，容許他和 Powell
在社區內的一個小型住宅居住。

Powell 開始想家，所以盧卡斯同意與她搬回[佛州](../Page/佛州.md "wikilink")。盧卡斯說他和 Powell
在[德州](../Page/德州.md "wikilink")[鮑伊縣一個連餐廳的](../Page/鮑伊縣_\(德克薩斯州\).md "wikilink")[加油站有過爭執](../Page/加油站.md "wikilink")，他說
Powell 準備和一個貨車司機離開。根據該餐廳的[侍應](../Page/侍應.md "wikilink") Shellady
的說法，其在法庭上認同了盧卡斯的口供。

## 1983年被捕后招供

## 招供曾犯下千起谋杀命案

## 「卢卡斯报告」当中前后矛盾之处

## 橙色袜子（Orange Socks）

## 不满社会言论

## 虚构小说与电影

  - 犯罪情节片：连环杀手亨利 1，1986年公演，John McNaughton执导
  - 犯罪情节片：连环杀手亨利 2
  - 记录片：连环杀手的自白

## 参照

<references/>

  - Sara L. Knox, "The Productive Power of Confessions of Cruelty" 2001
    [1](http://jefferson.village.virginia.edu/pmc/issue.501/11.3knox.html#foot6)
  - Brad Shellady, "Henry: Fabrication of a Serial Killer", included in
    *Everything You Know Is Wrong: The Disinformation Guide to Secrets
    and Lies*, 2002; Russ Kick, editor.
  - Michael A. Kroll, "Condemned in Texas: When Innocence Doesn't
    Matter", 1998
    [2](http://webarchive.loc.gov/all/20011116105356/http://www.pacificnews.org/jinn/stories/4.13/980624-innocence.html)
  - "The Death Penalty In Texas: Lethal Injustice", Amnesty
    International, 1998
    [3](https://web.archive.org/web/20071126123750/http://web.amnesty.org/library/Index/engAMR510101998)
  - "Failing the Future: Death Penalty Developments, March 1998 - March
    2000" Amnesty International, 2000
    [4](https://web.archive.org/web/20071015201843/http://web.amnesty.org/library/index/ENGAMR510032000)
  - "Henry Lee Lucas able to confuse authorities and then beat death",
    Jim Henderson, 1998 Houston Chronicle
    [5](http://www.chron.com/content/chronicle/page1/98/06/28/lucas.html)
  - "Sheriff's wife among 4 dead in shooting", Melissa Nelson, 2007
    Associated Press (Yahoo News)
    [6](http://news.yahoo.com/s/ap/20070131/ap_on_re_us/sheriff_s_house_shooting_17)

## 外部链接

  - [Biography of Henry Lee Lucas at Courtroom Television Network's
    *Crime
    Library*](https://web.archive.org/web/20041209021144/http://crimelibrary.com/serial_killers/predators/lucas/confess_1.html)

[Category:連環殺手](../Category/連環殺手.md "wikilink")
[Category:美國罪犯](../Category/美國罪犯.md "wikilink")