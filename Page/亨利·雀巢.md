**亨利·雀巢**（****或****，1814年—1890年），[德国](../Page/德国.md "wikilink")[法兰克福出生的](../Page/法兰克福.md "wikilink")[瑞士](../Page/瑞士.md "wikilink")[企业家](../Page/企业家.md "wikilink")。

## 生平

亨利·雀巢出生于[法蘭克福的一个富豪家庭](../Page/法蘭克福.md "wikilink")\[1\]，是約翰·乌利齐·马提亚·雀巢（Johann
Ulrich Matthias Nestle）和安娜-瑪利亞·凯瑟琳娜·艾爾曼（Anna-Maria Catharina
Ehemant）十四个孩子中的第十一个。他的兄弟後來當過法蘭克福市長\[2\]。

1833年，雀巢家族因[政治迫害以及社会不安](../Page/政治迫害.md "wikilink")，逃到瑞士，定居[沃韦](../Page/沃韦.md "wikilink")。亨利·雀巢本是一个[药剂师助手](../Page/药剂师.md "wikilink")，1843年他在沃韦开办了一个[矿泉水工厂](../Page/矿泉水.md "wikilink")。1860年间，他开始研究可以减少[婴儿](../Page/婴儿.md "wikilink")[死亡率的](../Page/死亡率.md "wikilink")[奶制品](../Page/奶制品.md "wikilink")，这样的人工奶制品尤其可以在城市内给无[奶母的婴儿服用](../Page/奶母.md "wikilink")。1866年[雀巢公司成立](../Page/雀巢.md "wikilink")，而本公司英文有舒適的安定下來和依偎的意思，他的[婴儿营养麦片粥开始在全](../Page/婴儿营养麦片粥.md "wikilink")[欧洲畅销](../Page/欧洲.md "wikilink")。

1868年，亨利·雀巢使用[鳥巢作為雀巢公司徽號](../Page/鳥巢.md "wikilink")，而該徽號亦在1875年成為[註冊商標](../Page/註冊商標.md "wikilink")；1890年，亨利·雀巢因[心臟病去世](../Page/心臟病.md "wikilink")。

## 参考文献

## 外部連接

  - [Henri Nestlé
    biography](http://switzerland.isyours.com/e/guide/lake_geneva/nestle.html).
    Switzerland.isyours.com.
  - [History of
    Nestlé](http://www.nestle.com/AboutUs/History/Pages/History.aspx).
    Nestlé.com.

[Category:雀巢](../Category/雀巢.md "wikilink")
[Category:瑞士企业家](../Category/瑞士企业家.md "wikilink")
[N](../Category/1814年出生.md "wikilink")
[N](../Category/1890年逝世.md "wikilink")
[Category:法蘭克福人](../Category/法蘭克福人.md "wikilink")

1.  Koese, Yavuz. (2008). "Nestle in the Ottoman Empire: Global
    Marketing with Local Flavor 1870–1927". *[Enterprise and
    Society](../Page/Enterprise_and_Society.md "wikilink")*, 9:4. pp
    724–761 <doi:10.1093/es/khn045>
2.