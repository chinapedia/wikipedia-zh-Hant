**亨利縣**（**Henry County,
Indiana**）是[美國](../Page/美國.md "wikilink")[印地安納州東部的一個縣](../Page/印地安納州.md "wikilink")。面積1,023平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口48,508人。縣治[紐卡斯爾](../Page/紐卡斯爾_\(印地安納州\).md "wikilink")（New
Castle）。

縣政府成立於1822年1月1日，以紀念[美國開國元勳](../Page/美國開國元勳.md "wikilink")、美國獨立後首任維吉尼亞州[州長](../Page/維吉尼亞州州長.md "wikilink")[派屈克·亨利](../Page/派屈克·亨利.md "wikilink")（Patrick
Henry）。

[H](../Category/印第安納州的縣.md "wikilink")