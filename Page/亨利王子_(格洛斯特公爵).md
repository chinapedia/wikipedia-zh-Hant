**格洛斯特公爵亨利王子殿下**（，）是英国国王[乔治五世和](../Page/乔治五世_\(英国\).md "wikilink")[瑪麗王后的第三子](../Page/瑪麗王后_\(英國\).md "wikilink")、[爱德华八世和](../Page/爱德华八世.md "wikilink")[乔治六世的弟弟](../Page/乔治六世.md "wikilink")。

[香港](../Page/香港.md "wikilink")[灣仔區的](../Page/灣仔區.md "wikilink")[告士打道即以其命名](../Page/告士打道.md "wikilink")。

## 早年生活

亨利王子1900年3月31日生于[桑德林漢姆府的约克别墅](../Page/桑德林漢姆府.md "wikilink")，5月17日在[温莎城堡的私人教堂中受洗](../Page/温莎城堡.md "wikilink")。他的曾祖母[维多利亚女王也成了他的教母之一](../Page/维多利亚女王.md "wikilink")。

## 经历

亨利王子是第一个入读学校的英王之子。他在小学以体育出众，中学则入读[伊顿公学](../Page/伊顿公学.md "wikilink")，中学毕业后入伍加入第十皇家骠骑兵团。他有志成为该团司令，但由于皇家事务需要不得不结束军旅生涯。1928年获封格洛斯特公爵。1935年他遵从父母之命与爱丽丝·孟特古·道格拉斯·斯高特结婚，婚后育有两子。

1939-40年格洛斯特公爵被派驻法国担任联络官。由于原定担任此职位的弟弟肯特公爵在空难中丧生，又获任命为驻[澳大利亚总督](../Page/澳大利亚总督.md "wikilink")，1947年提前卸任回英国，以便在乔治六世和伊丽莎白皇后出访南非时担任首席[国务参事监国](../Page/国务参事.md "wikilink")。

1972年其长子威廉在自己驾驶飞机时坠毁丧生。1974年亨利死后其公爵位由次子理查继承。他的遗孀2004年去世，是英国皇室历史上最长寿者。

## 頭銜

  - 1900年-1901年：約克的亨利王子殿下 (*HRH* Prince Henry of York)
  - 1901年-1901年：康沃爾和約克的亨利王子殿下 (*HRH* Prince Henry of Cornwall and York)
  - 1901年-1910年：威爾斯的亨利王子殿下 (*HRH* Prince Henry of Wales)
  - 1910年-1928年：亨利王子殿下 (*HRH* The Prince Henry)
  - 1928年-1974年：格洛斯特公爵殿下 (*HRH* The Duke of Gloucester)

## 家庭

### 祖先

<center>

</center>

[Category:英国君主儿子](../Category/英国君主儿子.md "wikilink")
[H](../Category/英國王爵.md "wikilink")
[Category:温莎王朝贵族](../Category/温莎王朝贵族.md "wikilink")
[Category:萨克森-科堡-哥达王朝](../Category/萨克森-科堡-哥达王朝.md "wikilink")
[Category:澳大利亞總督](../Category/澳大利亞總督.md "wikilink")
[Category:聯合王國公爵](../Category/聯合王國公爵.md "wikilink")
[Category:英国陆军元帅](../Category/英国陆军元帅.md "wikilink")
[Category:諾福克郡人](../Category/諾福克郡人.md "wikilink")