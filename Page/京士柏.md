[HK_King's_Park_201108.jpg](https://zh.wikipedia.org/wiki/File:HK_King's_Park_201108.jpg "fig:HK_King's_Park_201108.jpg")望向京士柏一帶。位於本圖右方的屋苑為衛理苑，其左方為伊利沙伯醫院。衛理苑右方屋苑是[何文田的](../Page/何文田.md "wikilink")[愛民邨](../Page/愛民邨.md "wikilink")。\]\]

**京士柏**（）是[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[油尖旺區的一個地方](../Page/油尖旺區.md "wikilink")，以[加士居道](../Page/加士居道.md "wikilink")、[公主道](../Page/公主道.md "wikilink")／[衛理道及](../Page/衛理道.md "wikilink")[窩打老道為界](../Page/窩打老道.md "wikilink")。

京士柏主要由[信旗山](../Page/信旗山.md "wikilink")、[京士柏山兩座小丘](../Page/京士柏山.md "wikilink")、[火棚及皇囿組成](../Page/火棚.md "wikilink")。19世紀後期至20世紀初期，皇囿一帶被駐守[槍會山軍營的](../Page/槍會山軍營.md "wikilink")[駐港英軍用作練靶場之用](../Page/駐港英軍.md "wikilink")。當[射擊進行期間](../Page/射擊.md "wikilink")，英軍便會在其北面的一座山丘掛起紅色的警示旗幟，信旗山因而得名。京士柏一名沿自其英文「King's
Park」的諧音直譯，因[英皇](../Page/英国君主.md "wikilink")[愛德華七世而起得名](../Page/愛德華七世.md "wikilink")，早年曾經意譯為**皇囿**，[香港日佔時期改名為](../Page/香港日佔時期.md "wikilink")**九龍競技塲**，[第二次世界大戰後因舊名太艱深](../Page/第二次世界大戰.md "wikilink")，改為以[粵語音譯為京士柏](../Page/粵語.md "wikilink")。

## 氣候

<div style="width:80%;">

## 公眾設施

### 醫療

  - [伊利沙伯醫院](../Page/伊利沙伯醫院.md "wikilink") (加士居道30號)
  - [香港紅十字會輸血服務中心](../Page/香港紅十字會輸血服務中心.md "wikilink")
  - [循道衛理聯合教會楊震牙科診所](../Page/循道衛理聯合教會.md "wikilink")

### 科學

  - [香港天文台](../Page/香港天文台.md "wikilink")[京士柏氣象站](../Page/京士柏氣象站.md "wikilink")──於1951年6月1日啟用，負責監察[香港市區的天氣狀況](../Page/香港市區.md "wikilink")。

### 政府

  - 前[南九龍裁判法院](../Page/南九龍裁判法院.md "wikilink")──[土地審裁處及](../Page/土地審裁處.md "wikilink")[勞資審裁處](../Page/勞資審裁處.md "wikilink")
  - [槍會山軍營](../Page/槍會山軍營.md "wikilink")

## 社區設施

  - [京士柏花園](../Page/京士柏花園.md "wikilink")
  - [京士柏道花園](../Page/京士柏道花園.md "wikilink")
  - [京士柏休憩公園](../Page/京士柏休憩公園.md "wikilink")
  - [京士柏上配水庫遊樂場](../Page/京士柏上配水庫遊樂場.md "wikilink")
  - [京士柏遊樂場](../Page/京士柏遊樂場.md "wikilink")
  - [京士柏運動場](../Page/京士柏運動場.md "wikilink")
  - [京士柏曲棍球場](../Page/京士柏曲棍球場.md "wikilink")
  - 京士柏網球場 (京士柏道23號)
  - 油麻地配水庫休憩花園

## 私人屋苑

[King's_Park_Hill_(full_view).jpg](https://zh.wikipedia.org/wiki/File:King's_Park_Hill_\(full_view\).jpg "fig:King's_Park_Hill_(full_view).jpg")是區內的[豪華住宅屋苑](../Page/豪華住宅.md "wikilink")\]\]

  - [衛理苑](../Page/衛理苑.md "wikilink")
  - [爵士花園](../Page/爵士花園.md "wikilink")
  - [帝庭園](../Page/帝庭園.md "wikilink")
  - [京士柏山](../Page/京士柏山_\(住宅\).md "wikilink")
  - [君頤峰](../Page/君頤峰.md "wikilink")

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/香港巴士.md" title="wikilink">巴士</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港小巴.md" title="wikilink">專線小巴</a></dt>

</dl>
<ul>
<li>紅色公共小巴</li>
</ul></td>
</tr>
</tbody>
</table>

### 區內道路

  - [衛理道](../Page/衛理道.md "wikilink")
  - [衛理徑](../Page/衛理徑.md "wikilink")
  - [京士柏道](../Page/京士柏道.md "wikilink")
  - [京士柏山道](../Page/京士柏山道.md "wikilink")
  - [伊利沙伯醫院路](../Page/伊利沙伯醫院路.md "wikilink")
  - [南丁格爾路](../Page/南丁格爾路.md "wikilink")
  - [真義里](../Page/真義里.md "wikilink")
  - [窩打老道](../Page/窩打老道.md "wikilink")
  - [加士居道](../Page/加士居道.md "wikilink")
  - [公主道](../Page/公主道.md "wikilink")

## 教育

  - [香港理工大學第八期校園Z座](../Page/香港理工大學#第八期校園_（Z座）.md "wikilink")
  - [東華學院](../Page/東華學院.md "wikilink") (衛理道31號)
  - [九龍華仁書院](../Page/九龍華仁書院.md "wikilink") (窩打老道56號)
  - [拔萃女書院](../Page/拔萃女書院.md "wikilink") (佐敦道1號)
  - [拔萃女小學](../Page/拔萃女小學.md "wikilink") (佐敦道1號)
  - [真光女書院](../Page/真光女書院.md "wikilink") (窩打老道54號A)
  - [基督教香港信義會信義中學](../Page/基督教香港信義會信義中學.md "wikilink") (油麻地窩打老道52號)
  - [循道中學](../Page/循道中學.md "wikilink") (加士居道50號)
  - [循道學校](../Page/循道學校.md "wikilink") (衛理道12號)
  - [天主教新民書院](../Page/天主教新民書院.md "wikilink") (石壁道2號)
  - [九龍婦女福利會李炳紀念學校](../Page/九龍婦女福利會李炳紀念學校.md "wikilink") (衛理道33號)

### 已遷

  - [培正專業書院](../Page/培正專業書院.md "wikilink")，2014年10月加士居道校舍租約期滿後遷到佐敦上海街80號華海廣場
  - [東華三院關啟明小學](../Page/東華三院關啟明小學.md "wikilink")，原址改建成
    [東華學院](../Page/東華學院.md "wikilink")

## 會所

  - [香港欖球總會京士柏運動場](../Page/香港欖球總會.md "wikilink") (衛理徑11號)
  - [香港女童軍總會總部](../Page/香港女童軍總會.md "wikilink")
  - [三軍會](../Page/三軍會.md "wikilink") (加士居道1號)
  - [西洋波會](../Page/西洋波會.md "wikilink")
  - [九龍木球會](../Page/九龍木球會.md "wikilink")
  - [九龍草地滾球會](../Page/九龍草地滾球會.md "wikilink")
  - [印度會](../Page/印度會.md "wikilink")
  - [西洋波會](../Page/西洋波會.md "wikilink")
  - [菲律賓會所](../Page/菲律賓會所.md "wikilink")
  - [香港政府華員會](../Page/香港政府華員會.md "wikilink")
  - [南華體育會九龍會所](../Page/南華體育會.md "wikilink") (衛理徑6號)
  - [巴基斯坦聯誼會](../Page/巴基斯坦聯誼會.md "wikilink")
  - [香港基督教青年會京士柏百周年紀念中心](../Page/香港基督教青年會#京士柏百周年紀念中心.md "wikilink")
  - [帝庭園會所](../Page/帝庭園會所.md "wikilink")

### 已重建

  - [東華三院林百欣青少年中心](../Page/東華三院林百欣青少年中心.md "wikilink")，原址改建成
    [東華學院](../Page/東華學院.md "wikilink")

## 區議會

|                                         |          |
| --------------------------------------- | -------- |
| 選區                                      | 備註       |
| [京士柏](../Page/京士柏_\(選區\).md "wikilink") | 己分拆      |
| [尖東及京士柏](../Page/尖東及京士柏.md "wikilink")  | 成立於2016年 |
|                                         |          |

## 参考文献

{{-}}

{{-}}

[Category:京士柏](../Category/京士柏.md "wikilink")
[Category:以英国君主命名的事物](../Category/以英国君主命名的事物.md "wikilink")