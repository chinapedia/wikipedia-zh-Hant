[StephensonNo3-sm.jpg](https://zh.wikipedia.org/wiki/File:StephensonNo3-sm.jpg "fig:StephensonNo3-sm.jpg")[胥各庄站](../Page/胥各庄站.md "wikilink")\]\]
[TongkuStation-PostCard1.jpg](https://zh.wikipedia.org/wiki/File:TongkuStation-PostCard1.jpg "fig:TongkuStation-PostCard1.jpg")上的塘沽站（现称[塘沽南站](../Page/塘沽南站.md "wikilink")）\]\]
[TianSqRRStation.jpg](https://zh.wikipedia.org/wiki/File:TianSqRRStation.jpg "fig:TianSqRRStation.jpg")\]\]

**京奉鐵路**是[中國](../Page/中國.md "wikilink")[清朝末年修建的一條](../Page/清朝.md "wikilink")[鐵路](../Page/鐵路.md "wikilink")。

京奉铁路上最早建成的路段是1881年建成通车的**[唐胥铁路](../Page/唐胥铁路.md "wikilink")**（[唐山至](../Page/唐山.md "wikilink")[胥各庄](../Page/胥各庄.md "wikilink")）。1890年，在唐胥铁路的基础上动工进行扩展，向南至[津沽铁路上的林西镇并通过津沽铁路延伸至](../Page/津沽铁路.md "wikilink")[天津](../Page/天津.md "wikilink")，向北至[山海关](../Page/山海关.md "wikilink")（历史上曾称“榆关”），故改称**津榆铁路**，。1894年，津榆铁路由天津经[津蘆铁路](../Page/津蘆铁路.md "wikilink")（天津-北京卢沟桥）延伸至北京\[1\]，改称**京榆铁路**，又称**[京山铁路](../Page/京山铁路.md "wikilink")**\[2\]。1898年10月，[清政府修筑铁路将京榆铁路延伸至](../Page/清政府.md "wikilink")[奉天](../Page/奉天.md "wikilink")（今称沈阳），改称**关内外铁路**，并与英国、俄国签定关内外铁路借款合同。1907年8月又改称**京奉铁路**（Peking-Mukden
Railway，P.M.R.）。

1911年[中华民国成立后](../Page/中华民国.md "wikilink")，京奉铁路改称**北宁铁路**（Peking-Liaoning
Railway,
P.N.RY），由[北洋政府交通部管辖](../Page/北洋政府.md "wikilink")。1912年铁路全线通车，并与由[日本管辖的](../Page/日本.md "wikilink")[南满铁路接轨](../Page/南满铁路.md "wikilink")。（南满铁路已于1907年改建为[標準軌距](../Page/標準軌距.md "wikilink")）1928年6月4日，[奉系](../Page/奉系.md "wikilink")[军阀](../Page/军阀.md "wikilink")[张作霖撤回奉天时](../Page/张作霖.md "wikilink")，在北宁铁路[皇姑屯站被炸身亡](../Page/皇姑屯站.md "wikilink")，史稱[皇姑屯事件](../Page/皇姑屯事件.md "wikilink")。同年6月15日，[南京国民政府宣布完成](../Page/南京国民政府.md "wikilink")[北伐](../Page/北伐.md "wikilink")，并将北京改名[北平](../Page/北平.md "wikilink")，北宁铁路也改称**平奉铁路**。1929年4月15日，南京[国民政府铁道部再次将平奉铁路改称](../Page/国民政府铁道部.md "wikilink")**北宁铁路**。[九一八事变发生后](../Page/九一八事变.md "wikilink")，日军控制了北宁铁路关外（[山海关以北](../Page/山海关.md "wikilink")）。[七七事变后](../Page/七七事变.md "wikilink")，北宁铁路全线被日军控制。

1949年10月，[中华人民共和国成立](../Page/中华人民共和国.md "wikilink")，北宁铁路改称**京瀋鐵路**，后正式与原[南满铁路哈尔滨至沈阳段合并为](../Page/南满铁路.md "wikilink")**京哈铁路**。后来受唐山大地震、压煤改线以及[京哈](../Page/京哈铁路.md "wikilink")、[京沪客运通道贯通里程的影响](../Page/京沪铁路.md "wikilink")，铁路各段几经易名，至段现为[京沪铁路的一部分](../Page/京沪铁路.md "wikilink")，至及天津至段现为[津山铁路的一部分](../Page/津山铁路.md "wikilink")，七道桥至段因压煤改线被重新命名为[七滦铁路](../Page/七滦铁路.md "wikilink")，至段现为[沈山铁路](../Page/沈山铁路.md "wikilink")。

## 註釋

## 延伸阅读

Crush, Peter (2013) “Imperial Railways of North China” – “关内外铁路” 皮特·柯睿思
著. ISBN 978-7-5166-0564-6.
([描述](http://www.ebay.com/itm/121333631163?ssPageName=STRK:MESELX:IT&_trksid=p3984.m1558.l2649))

[Category:中国铁路线](../Category/中国铁路线.md "wikilink")
[Category:已更名的中国铁路线](../Category/已更名的中国铁路线.md "wikilink")
[Category:京奉铁路](../Category/京奉铁路.md "wikilink")

1.
2.  [二五设邮传部筹赎回路权](http://lz.book.sohu.com/chapter-13120-4-1.html)，《曹汝霖一生之回忆》，曹汝霖