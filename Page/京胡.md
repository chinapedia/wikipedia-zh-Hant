[Jinghu.jpg](https://zh.wikipedia.org/wiki/File:Jinghu.jpg "fig:Jinghu.jpg")
**京胡**，[弓弦樂器](../Page/弓弦樂器.md "wikilink")。早期有「胡琴」、「二鼓子」之称。京胡从徽戏中的[徽胡发展而来](../Page/徽胡.md "wikilink")。现在婺剧中的徽戏仍使用徽胡。[清](../Page/清.md "wikilink")[乾隆末年](../Page/乾隆.md "wikilink")（1785年左右）随[皮簧腔的发展逐渐形成](../Page/皮簧腔.md "wikilink")，是[京剧](../Page/京剧.md "wikilink")、[汉剧的主要伴奏乐器之一](../Page/汉剧.md "wikilink")，故得名。

京胡的琴杆和琴筒采用紫竹、白竹或染竹制作，琴桿一般有5节，第一节和第二节有弦轴，最后一节插在琴筒中，琴杆在琴筒中的段开有长方形对穿的孔，成为琴筒的复共鸣部分。琴筒直径为釐米左右，一端开口，另一端蒙上蛇皮。音色清脆而嘹亮。

早期京劇流行高调儿，遂缩短琴杆，缩小琴筒，有的还蒙上蟒皮，用[软弓拉弦](../Page/软弓.md "wikilink")。19世纪以后才开始出现[硬弓](../Page/硬弓.md "wikilink")，使京胡的发音更刚劲、嘹亮。20世纪初，京剧演员讲究行腔圆润，不断降低音高，为之伴奏的京胡的琴杆、琴筒随之加长。

1930年前后，京剧空前繁荣，北京的乐器店有许多将牌匾改为胡琴铺，而有名的琴师也自己招聘工人制作销售京胡。

## 京胡演奏家

  - 「四大名家」

<!-- end list -->

  -
    [梅雨田](../Page/梅雨田.md "wikilink")
    [孙佐臣](../Page/孙佐臣.md "wikilink")
    [陆彦庭](../Page/陆彦庭.md "wikilink")
    [王云亭](../Page/王云亭.md "wikilink")

<!-- end list -->

  - [徐兰沅](../Page/徐兰沅.md "wikilink")
  - [李慕良](../Page/李慕良.md "wikilink")
  - [杨宝忠](../Page/杨宝忠.md "wikilink")

## 四大名琴

4把按京劇[四大名旦的嗓音条件和演唱风格而制的京胡](../Page/京剧四大名旦.md "wikilink")，全由[史善朋所制造](../Page/史善朋.md "wikilink")，由於能烘托突出京劇四大流派的艺术特色而聞名，現被收藏於[北京湖廣會館](../Page/北京湖廣會館.md "wikilink")。

## 50位京剧名琴师

  - [沈湘泉](../Page/沈湘泉.md "wikilink")
  - [柏如意](../Page/柏如意.md "wikilink")
  - [谢双寿](../Page/谢双寿.md "wikilink")
  - [戴韵芳](../Page/戴韵芳.md "wikilink")
  - [王晓韵](../Page/王晓韵.md "wikilink")
  - [李春泉](../Page/李春泉.md "wikilink")
  - [樊景泰](../Page/樊景泰.md "wikilink")
  - [贾东林](../Page/贾东林.md "wikilink")
  - [汪桂芬](../Page/汪桂芬.md "wikilink")
  - [梅雨田](../Page/梅雨田.md "wikilink")
  - [孙佐臣](../Page/孙佐臣.md "wikilink")
  - [袭桂仙](../Page/袭桂仙.md "wikilink")
  - [陈彦衡](../Page/陈彦衡.md "wikilink")
  - [陈道安](../Page/陈道安.md "wikilink")
  - [穆铁芬](../Page/穆铁芬.md "wikilink")
  - [陈鸿寿](../Page/陈鸿寿.md "wikilink")
  - [杨宝忠](../Page/杨宝忠.md "wikilink")
  - [趙濟羹](../Page/趙濟羹.md "wikilink")
  - [陆五](../Page/陆五.md "wikilink")
  - [李佩卿](../Page/李佩卿.md "wikilink")
  - [王瑞芝](../Page/王瑞芝.md "wikilink")
  - [赵砚奎](../Page/赵砚奎.md "wikilink")
  - [徐兰沅](../Page/徐兰沅.md "wikilink")
  - [汪本贞](../Page/汪本贞.md "wikilink")
  - [张长林](../Page/张长林.md "wikilink")
  - [郭少臣](../Page/郭少臣.md "wikilink")
  - [倪秋萍](../Page/倪秋萍.md "wikilink")
  - [杜奎三](../Page/杜奎三.md "wikilink")
  - [田宝林](../Page/田宝林.md "wikilink")
  - [茹莱卿](../Page/茹莱卿.md "wikilink")
  - [王少卿](../Page/王少卿.md "wikilink")
  - [时德宝](../Page/时德宝.md "wikilink")
  - [杨深泉](../Page/杨深泉.md "wikilink")
  - [谭嘉瑞](../Page/谭嘉瑞.md "wikilink")
  - [周长华](../Page/周长华.md "wikilink")
  - [苏盛琴](../Page/苏盛琴.md "wikilink")
  - [鍾世章](../Page/鍾世章.md "wikilink")
  - [王云亭](../Page/王云亭.md "wikilink")
  - [厉彦芝](../Page/厉彦芝.md "wikilink")
  - [何顺信](../Page/何顺信.md "wikilink")
  - [李慕良](../Page/李慕良.md "wikilink")
  - [沈雁西](../Page/沈雁西.md "wikilink")
  - [唐在炘](../Page/唐在炘.md "wikilink")

## 京胡曲牌

  - 《[夜深沉](../Page/夜深沉.md "wikilink")》
  - 《五字开门》
  - 《柳摇金》
  - 《小開門》
  - 《傍妝台》
  - 《漢東山》
  - 《春日景和》
  - 《寄生草》
  - 《哭皇天》
  - 《八岔》
  - 《回回曲》
  - 《東方贊》
  - 《西皮隔尾》（《西皮革尾》）
  - 《新八岔》（20世紀新創作，[徐兰沅](../Page/徐兰沅.md "wikilink")，1說[李慕良作曲](../Page/李慕良.md "wikilink")）
  - 《新東方贊》（20世紀新創作，[李慕良作曲](../Page/李慕良.md "wikilink")）

《小開門》、《哭皇天》、《八岔》等許多傳統曲牌分正反西皮（西皮空弦音la-mi；6-3，反西皮是正西皮的移調降調，空弦音還是la-mi；6-3）、正反二黃（正二黃空弦音sol-re；5-2，反二黃空弦音do-sol；1-5）。

## 參考資料

  - [百年文物：北京湖廣會館](http://big5.ce.cn/ztpd/cjzt/sssh/2004guoqing/gqxfzn/xfznjh/200409/17/t20040917_1788774.shtml)

[Category:胡琴](../Category/胡琴.md "wikilink")
[Category:京剧](../Category/京剧.md "wikilink")