**京都市營地下鐵**（）是一個由[京都市交通局營運的](../Page/京都市交通局.md "wikilink")[地下鐵系統](../Page/地下鐵.md "wikilink")，共有[烏丸線和](../Page/烏丸線.md "wikilink")[東西線](../Page/東西線_\(京都市營地下鐵\).md "wikilink")2線路。京都市營地下鐵的兩條路線除了連結[京都市內除了](../Page/京都市.md "wikilink")[西京區之外的](../Page/西京區.md "wikilink")10個次級行政區外，東西線在隔鄰的[宇治市內也設有一座車站](../Page/宇治市.md "wikilink")。雖然在營運上是以「市營地下鐵」稱呼之，但在一些與法規或特許權相關的官方文件上，此系統是以**京都市高速鐵道**命名。

## 历史

京都最早规划建设地铁是于1968年京都市交通管理措施委员会，1972年获得营业执照，1974年开工，1981年通车。

## 路線列表

[Kyoto_Subway.PNG](https://zh.wikipedia.org/wiki/File:Kyoto_Subway.PNG "fig:Kyoto_Subway.PNG")
[Kyoto_Subway_series_10_batch_1_and_6.JPG](https://zh.wikipedia.org/wiki/File:Kyoto_Subway_series_10_batch_1_and_6.JPG "fig:Kyoto_Subway_series_10_batch_1_and_6.JPG")10系列列車\]\]
[Kyoto_Municipal_Subway_50_Series_5614.jpg](https://zh.wikipedia.org/wiki/File:Kyoto_Municipal_Subway_50_Series_5614.jpg "fig:Kyoto_Municipal_Subway_50_Series_5614.jpg")50系列列車\]\]

<table>
<thead>
<tr class="header">
<th><p>路線顏色</p></th>
<th><p>記號</p></th>
<th><p>中文線名</p></th>
<th><p>日文線名</p></th>
<th><p>起點</p></th>
<th><p>終點</p></th>
<th><p>距離（公里）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>綠色</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Subway_KyotoKarasuma.png" title="fig:Subway_KyotoKarasuma.png">Subway_KyotoKarasuma.png</a></p></td>
<td><p><a href="../Page/烏丸線.md" title="wikilink">烏丸線</a></p></td>
<td></td>
<td><p><a href="../Page/國際會館站.md" title="wikilink">國際會館</a>（K01）</p></td>
<td><p><a href="../Page/竹田站_(京都府).md" title="wikilink">竹田</a>（K15）</p></td>
<td><p>13.7</p></td>
</tr>
<tr class="even">
<td><p>橘色</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Subway_KyotoTozai.png" title="fig:Subway_KyotoTozai.png">Subway_KyotoTozai.png</a></p></td>
<td><p><a href="../Page/東西線_(京都市營地下鐵).md" title="wikilink">東西線</a></p></td>
<td></td>
<td><p><a href="../Page/六地藏站.md" title="wikilink">六地藏</a>（T01）</p></td>
<td><p><a href="../Page/太秦天神川站.md" title="wikilink">太秦天神川</a>（T17）</p></td>
<td><p>17.5</p></td>
</tr>
</tbody>
</table>

## 營運狀況

2016年度，京都地下鐵利潤達16億[日圓](../Page/日圓.md "wikilink")，平均一天利潤400萬日圓，累積[赤字達](../Page/赤字.md "wikilink")3077億日圓。由于京都是古都，在施工時常會挖到古跡，導致進程延誤，虧損加重。

也因為是日本的古都，其地鐵規模遠不如[東京和](../Page/東京的地下鐵系統.md "wikilink")[大阪](../Page/大阪市營地鐵.md "wikilink")，就連[札幌和](../Page/札幌市營地下鐵.md "wikilink")[名古屋的地鐵路線規模都遠超過京都](../Page/名古屋市營地下鐵.md "wikilink")，與同樣因歷史古蹟導致地鐵建設受阻的義大利[羅馬以及中国](../Page/羅馬地鐵.md "wikilink")[西安境遇十分相似](../Page/西安地铁.md "wikilink")。

京都地鐵因為線路只有兩條，無法到達京都部份地區，因而必須搭配公車系統一起使用。

## 票價

| 区间            | 票价（日币） |
| ------------- | ------ |
| 1区（1 - 3km）   | 210    |
| 2区（3 - 7km）   | 260    |
| 3区（7 - 11km）  | 290    |
| 4区（11 - 15km） | 320    |
| 5区（15km以上）    | 350    |

## 参考文献

## 外部链接

  - [京都市交通局](http://www.city.kyoto.jp/kotsu/)
  - [京都市巴士・地下鐵導遊指南](https://www2.city.kyoto.lg.jp/kotsu/webguide/index.html)

[Category:日本地鐵](../Category/日本地鐵.md "wikilink")
[京都市交通局](../Category/京都市交通局.md "wikilink")
[Category:京都府鐵路](../Category/京都府鐵路.md "wikilink")