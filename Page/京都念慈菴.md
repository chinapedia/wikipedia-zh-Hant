[Nin_Jiom_Pei_Pa_Koa_at_HKTDC_Food_Expo_2011.jpg](https://zh.wikipedia.org/wiki/File:Nin_Jiom_Pei_Pa_Koa_at_HKTDC_Food_Expo_2011.jpg "fig:Nin_Jiom_Pei_Pa_Koa_at_HKTDC_Food_Expo_2011.jpg")的展攤\]\]
**京都念慈菴**（），始于[清朝](../Page/清朝.md "wikilink")，是一家以生產[枇杷膏為主的老牌](../Page/枇杷膏.md "wikilink")[中藥製造商](../Page/中藥.md "wikilink")，公司總部分設於[香港和](../Page/香港.md "wikilink")[臺灣](../Page/臺灣.md "wikilink")，所產製的枇杷膏可改善[呼吸系統不適](../Page/呼吸系統.md "wikilink")，諸如[痰多痰稠](../Page/痰.md "wikilink")、[咽喉乾癢](../Page/喉.md "wikilink")、聲音沙啞、傷風[咳嗽](../Page/咳嗽.md "wikilink")，並能止咳化痰、潤喉利咽、清熱養陰等等。京都念慈菴的[商標稱為](../Page/商標.md "wikilink")「孝親圖」。

京都念慈菴宣稱其天然入藥，除了藥用之外，製造商建議可作為保健的[健康食品使用](../Page/健康食品.md "wikilink")。另外由於其特殊氣味，也有人將它作為[甜點或](../Page/甜點.md "wikilink")[飲品的材料之一](../Page/飲品.md "wikilink")。近年廠商有將枇杷膏成份製成[潤喉糖產品](../Page/潤喉糖.md "wikilink")，但不含[川貝成份](../Page/川貝.md "wikilink")，被歸類為食品而非藥品。2018年於美國更有人將之加入[雞尾酒](../Page/雞尾酒.md "wikilink")，一瓶市價為7.8美元的京都念慈菴川貝枇杷膏，在網上炒賣至70美元\[1\]。

## 歷史

根据药品单张的文言文记载\[2\]，京都念慈菴的歷史可追溯到[清朝](../Page/清朝.md "wikilink")[康熙年間](../Page/康熙.md "wikilink")，當時有一[順天府人](../Page/順天府.md "wikilink")[楊謹](../Page/楊謹.md "wikilink")，字慎之，幼年喪父，母陳氏出身名門，勤於家政，教子有方，楊謹品學兼優，連科報捷，出任縣宰（相當於今日的縣長）。楊謹事母至[孝](../Page/孝.md "wikilink")，鄉里號稱為楊孝廉，而母親陳氏年邁多病，久為[咳多](../Page/咳.md "wikilink")[痰的沉痾所苦](../Page/痰.md "wikilink")，遍求名醫皆長治不癒。楊謹時聞名醫[葉天士的事蹟後](../Page/葉天士.md "wikilink")，不畏跋山涉水，千里躬求，親自拜訪並延回府中看診。葉天士斷太夫人為積勞成傷，又復誤於診治，不是平常湯藥所能奏效。葉於是授以秘方，叮囑須依此法煉製成膏，早晚服食，持之有理，楊太夫人自服此藥後病狀轉佳，後常以此膏和水代替茶湯服用，終於根治楊太夫人久咳痰多之宿疾。

後楊母高齡過世，享年八十四歲，彌留時，囑孝廉曰：「余昔病時，痛苦莫名，但求速死，不意獲食此膏，痼疾解除，復我人生樂趣，得享天年，天士之功實同再造，汝宜多製此膏施贈，俾世之同病者，得渡苦海，則恩同報我矣。」囑咐楊謹要廣製此藥，造福世人。於是楊謹以「念慈菴」之名製膏佈施，凡患久咳癆疾、內傷吐血、氣弱痰壅等症者，見症施藥，屢試屢應，於是名傳遐邇，踵門求藥者不可數計。菴名「念慈」，用以示紀念慈親之意，並命名此膏為「[川貝枇杷膏](../Page/川貝.md "wikilink")」，「念慈菴川貝枇杷膏」的名聲也逐漸名揚四方。\[3\]及後楊孝廉去世，但求藥者日多，其後人遂設肆於[北京](../Page/北京.md "wikilink")，冠之「京都」二字而成為「京都念慈菴川貝枇杷膏」\[4\]，並隨藥附送原方，以符先人濟世之旨。\[5\]

此膏一傳近兩百年，[民國時](../Page/民國.md "wikilink")，楊家後人楊仲籬、楊萬如兄弟為避[日軍侵華](../Page/日軍侵華.md "wikilink")，舉家南遷[廣州後又轉往](../Page/廣州.md "wikilink")[香港](../Page/香港.md "wikilink")，並打算[移民](../Page/移民.md "wikilink")[巴西](../Page/巴西.md "wikilink")。二人為免枇杷膏就此絕世，有違祖訓，所以將「念慈菴」鋪號與藥方託付給中正藥房老闆[謝兆邦](../Page/謝兆邦.md "wikilink")（[蘋果西打第二任董事長](../Page/蘋果西打.md "wikilink")）。謝秉持楊氏心意，1946年在香港設立「京都念慈菴總廠有限公司」，繼續以正統正方正藥惠之世人，1961年到[台灣設廠](../Page/台灣.md "wikilink")，陸續銷行枇杷膏到[亞](../Page/亞洲.md "wikilink")、[歐](../Page/歐洲.md "wikilink")、[美](../Page/美洲.md "wikilink")、[澳等數十國](../Page/澳洲.md "wikilink")，延續至今。

## 產品認證

「京都念慈菴」已取得多項「GMP 優良製造規範」和「[ISO
9000](../Page/國際標準化組織.md "wikilink")」的優質管理認證、以及香港優質產品標誌「Ｑ嘜」、「[香港十大名牌](../Page/香港名牌選舉.md "wikilink")」和SuperBrand等認證。

## 廣告

早年京都念慈菴曾在[香港](../Page/香港.md "wikilink")[商業電台整點新聞後播放廣告歌](../Page/商業電台.md "wikilink")，老一輩香港人皆耳熟能詳，該廣告由[馮偉棠旁白](../Page/馮偉棠.md "wikilink")，[馬淑逑主唱](../Page/馬淑逑.md "wikilink")。

TVB鎮台之寶「Do姐」[鄭裕玲及歌后](../Page/鄭裕玲.md "wikilink")[楊千嬅為代言人](../Page/楊千嬅.md "wikilink")。

2017年冠名在[腾讯视频播出的](../Page/腾讯视频.md "wikilink")《[吐槽大会](../Page/吐槽大会.md "wikilink")》和《[脱口秀大会](../Page/脱口秀大会.md "wikilink")》，[口号分别是](../Page/口号.md "wikilink")“大笑养肺，不笑浪费，宇宙养肺老字号京都念慈庵”和“肺要经常养，大笑不缺氧”\[6\]\[7\]。

## 成份

### 膏滋劑

  - 複方川貝枇杷膏：[川貝](../Page/川貝.md "wikilink")、[枇杷](../Page/枇杷.md "wikilink")、[沙參](../Page/沙參.md "wikilink")、[茯苓](../Page/茯苓.md "wikilink")、[陳皮](../Page/陳皮.md "wikilink")、[桔梗](../Page/桔梗.md "wikilink")、[法半夏](../Page/法半夏.md "wikilink")、[瓜蔞仁](../Page/瓜蔞仁.md "wikilink")、[款冬花](../Page/款冬花.md "wikilink")、[遠志](../Page/遠志.md "wikilink")、[乾薑](../Page/乾薑.md "wikilink")、[薄荷腦](../Page/薄荷腦.md "wikilink")、[蜂蜜](../Page/蜂蜜.md "wikilink")、[蓮子](../Page/蓮子.md "wikilink")、[苦杏仁油](../Page/苦杏仁油.md "wikilink")、[橙皮油](../Page/橙皮油.md "wikilink")、[草莓香料](../Page/草莓.md "wikilink")、[鳳梨香料](../Page/鳳梨.md "wikilink")、[麥芽糖](../Page/麥芽糖.md "wikilink")、[蔗糖](../Page/蔗糖.md "wikilink")。
  - 人參川貝枇杷膏：川貝、枇杷葉、[人參](../Page/人參.md "wikilink")、茯苓、陳皮、法半夏、款冬花、乾薑、蜂蜜、[杏仁](../Page/杏仁.md "wikilink")、[阿膠](../Page/阿膠.md "wikilink")、[五味子](../Page/五味子.md "wikilink")、[桑白皮](../Page/桑白皮.md "wikilink")、[薏苡仁](../Page/薏苡仁.md "wikilink")、[紫蘇葉](../Page/紫蘇葉.md "wikilink")、[百合](../Page/百合.md "wikilink")、[甘草](../Page/甘草.md "wikilink")、[大棗](../Page/大棗.md "wikilink")。
  - 川貝羅漢果枇杷膏：桔梗、遠志、瓜簍仁、法半夏、[貝母](../Page/貝母.md "wikilink")、枇杷葉、[羅漢果](../Page/羅漢果.md "wikilink")、[菊花](../Page/菊花.md "wikilink")、陳皮、[生薑](../Page/生薑.md "wikilink")、[橘紅](../Page/橘紅.md "wikilink")、蜂蜜、麥芽糖、薄荷腦。
  - 清潤無糖枇杷膏：麥芽糖醇(甜味劑)、植物抽出物(枇杷葉、陳皮、魚腥草、杏仁、甘草、桔梗、金銀花、茯苓、麥冬、百合、乾薑、橄欖葉)、水溶性纖維(難消化性糊精)、異麥芽寡糖、香料
  - 蜜煉枇杷膏：枇杷葉、陳皮、百合、杏仁、菊花、[甘草](../Page/甘草.md "wikilink")、茯苓、[麥冬](../Page/麥冬.md "wikilink")、桔梗、乾薑、蜂蜜、麥芽糖、蔗糖、枇杷膏香料。
  - 兒童枇杷膏：枇杷葉、[魚腥草](../Page/魚腥草.md "wikilink")、陳皮、茯苓、桔梗、乾薑、麥冬、[黨參](../Page/黨參.md "wikilink")、遠志、[西洋參](../Page/西洋參.md "wikilink")、百合、杏仁、甘草、[綜合維他命](../Page/綜合維他命.md "wikilink")、麥芽糖、蔗糖、蜂蜜、香料。
  - 蜜煉四物（膏狀）：[當歸](../Page/當歸.md "wikilink")、[川芎](../Page/川芎.md "wikilink")、[芍藥](../Page/芍藥.md "wikilink")、[熟地](../Page/熟地.md "wikilink")、[生地](../Page/生地.md "wikilink")、[紅棗](../Page/紅棗.md "wikilink")、[桂圓](../Page/桂圓.md "wikilink")、甘草、黨參、[白术](../Page/白术.md "wikilink")、茯苓、麥芽糖、蔗糖、[黑糖](../Page/黑糖.md "wikilink")、蜂蜜。

### 科學中藥

  - [川貝止嗽散](../Page/川貝止嗽散.md "wikilink")：桔梗、[川石斛](../Page/川石斛.md "wikilink")、[半夏](../Page/半夏.md "wikilink")、川貝母、[蘇子](../Page/蘇子.md "wikilink")、茯苓、薄荷、杏仁、[桑白皮](../Page/桑白皮.md "wikilink")、橘紅、[穀芽](../Page/穀芽.md "wikilink")、甘草。
  - [葛根湯](../Page/葛根湯.md "wikilink")：[葛根](../Page/葛根.md "wikilink")、[麻黃](../Page/麻黃.md "wikilink")、[桂枝](../Page/桂枝.md "wikilink")、白芍、炙甘草、生薑、大棗。
  - [銀翹散](../Page/銀翹散.md "wikilink")：[連翹](../Page/連翹.md "wikilink")、[金銀花](../Page/金銀花.md "wikilink")、桔梗、薄荷、[淡竹葉](../Page/淡竹葉.md "wikilink")、甘草、[荊芥](../Page/荊芥.md "wikilink")、[淡豆鼓](../Page/淡豆鼓.md "wikilink")、[牛蒡子](../Page/牛蒡子.md "wikilink")、[蘆根](../Page/蘆根.md "wikilink")。
  - [小柴胡湯](../Page/小柴胡湯.md "wikilink")：柴胡、[黃芩](../Page/黃芩.md "wikilink")、人參、炙甘草、半夏、生薑、大棗。
  - [六味地黃丸](../Page/六味地黃丸.md "wikilink")：熟地黃、[山茱萸](../Page/山茱萸.md "wikilink")、[山藥](../Page/山藥.md "wikilink")、[澤瀉](../Page/澤瀉.md "wikilink")、[牡丹皮](../Page/牡丹皮.md "wikilink")、茯苓。

### 湯劑、液劑

  - [四物湯](../Page/四物湯.md "wikilink")：熟地黃、白芍、當歸、川芎。
  - [四君子湯](../Page/四君子湯.md "wikilink")：人參、茯苓、炙甘草、白朮、生薑、大棗。
  - [葡萄糖胺液](../Page/氨基葡萄糖.md "wikilink")

## 參考來源

## 外部連結

  - [京都念慈菴香港](http://www.ninjiom.com)
      -
  - [京都念慈菴台湾](http://www.nin-jiom.com.tw)
      -
  - [蜜煉川貝枇杷膏 Mi Lian Chuan Bei Pi Pa
    Gao](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00152)
    中藥標本數據庫 (香港浸會大學中醫藥學院)

[Category:中成藥生產商](../Category/中成藥生產商.md "wikilink")
[Category:香港製藥公司](../Category/香港製藥公司.md "wikilink")
[Category:台灣工商業機構](../Category/台灣工商業機構.md "wikilink")
[Category:1946年成立的公司](../Category/1946年成立的公司.md "wikilink")

1.
2.  [蜜煉川贝枇杷膏单张图片，取自互联网](https://imgsa.baidu.com/forum/w%3D580/sign=aac59996d81b0ef46ce89856edc551a1/0ca5ceea15ce36d3db3e528830f33a87e950b164.jpg)
3.
4.
5.  藥品單張
6.
7.  [“宇宙养肺老字号”京都念慈菴冠名《脱口秀大会》，爆笑一夏](https://news.qq.com/a/20170811/032772.htm)，腾讯新闻