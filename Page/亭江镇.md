**亭江镇**是[中国](../Page/中华人民共和国.md "wikilink")[福建省](../Page/福建省.md "wikilink")[福州市](../Page/福州市.md "wikilink")[马尾区下辖的一个镇](../Page/马尾区.md "wikilink")，位于福州市东部。亭江镇北面为[连江县](../Page/连江县_\(福州市\).md "wikilink")[琯头镇](../Page/琯头镇.md "wikilink")，南面为[马尾区](../Page/马尾区.md "wikilink")[罗星街道](../Page/罗星街道.md "wikilink")，西面为[宦溪镇](../Page/宦溪镇.md "wikilink")，东面为[琅岐镇](../Page/琅岐镇.md "wikilink")、[长乐市](../Page/长乐市.md "wikilink")。

## 历史沿革

亭江镇[唐朝属闽县晋安乡的合浦里和海滨里](../Page/唐朝.md "wikilink")。[宋朝属闽县晋安东乡的合浦北里](../Page/宋朝.md "wikilink")”和合浦南里”。[元朝及](../Page/元朝.md "wikilink")[明朝行政区划没有改变](../Page/明朝.md "wikilink")。[清朝](../Page/清朝.md "wikilink")，合浦北里和合浦南里改为江右里、江左里。

[中华民国时期](../Page/中华民国.md "wikilink")，亭江镇属闽侯县2区(闽安镇区)。[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，属闽侯县第3区(马江区)。1962年，亭江镇划归福州市郊区，为亭江区公所，后改为亭江人民公社。1984年，改公社为乡。1988年改名亭江镇。

## 行政区划

亭江镇共辖3个[社区](../Page/社区.md "wikilink")、17个[行政村](../Page/行政村.md "wikilink")，分别是：

亭头社区、闽安社区、闽亭社区、敖溪村、盛美村、东盛村、笏山村、前洋村、康坂村、白眉村、象洋村、英屿村、东岐村、长柄村、洪塘村、亭头村、闽安村、西边村、长安村、东街村

## 教育

宋朝，亭江就有朱熹讲学的怡山书院。目前，亭江有1所大学，3所中学，18所小学，19所幼儿园。

## 简介

亭江镇山多田少，从清代咸丰、同治年间(1851～1874年)起，就有许多村民被迫背井离乡，到海外谋生。

## 外部链接

  - [亭江便民服务网 （官方网站）](http://www.mwtjw.com/)

[镇](../Category/马尾区乡镇.md "wikilink")