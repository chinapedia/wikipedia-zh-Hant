[親電芳香取代反應](https://zh.wikipedia.org/wiki/File:Electrophilic_aromatic_substitution.svg "fig:親電芳香取代反應")

**亲电芳香取代反应**是指[芳香环系上的](../Page/芳香環.md "wikilink")[取代基](../Page/取代基.md "wikilink")（通常是[氢原子](../Page/氢.md "wikilink")）被[亲电试剂](../Page/亲电试剂.md "wikilink")[取代的反应](../Page/取代反应.md "wikilink")。该反应中最重要的类型包括芳香环系的[硝化反应](../Page/硝化反应.md "wikilink")、[卤代反应](../Page/卤代反应.md "wikilink")、[磺化反应以及](../Page/磺化反应.md "wikilink")[傅-克反应](../Page/傅-克反应.md "wikilink")。

## 常见反应举例

本节所举例子都是基于苯环体系的，对于在[杂环芳香体系和](../Page/杂环化合物.md "wikilink")[稠环芳香体系上进行的亲电取代反应](../Page/多環性芳香化合物.md "wikilink")，请参见第4节。

### 硝化反应

[zh_Electrophilic_aromatic_substitution_nitration.png](https://zh.wikipedia.org/wiki/File:zh_Electrophilic_aromatic_substitution_nitration.png "fig:zh_Electrophilic_aromatic_substitution_nitration.png")的硝化\]\]

[苯上的氢可被](../Page/苯.md "wikilink")[硝基正离子](../Page/硝基.md "wikilink")(NO<sub>2</sub><sup>+</sup>)取代而形成[硝基苯](../Page/硝基苯.md "wikilink")。NO<sub>2</sub><sup>+</sup>通常是通过浓[硝酸和浓](../Page/硝酸.md "wikilink")[硫酸的](../Page/硫酸.md "wikilink")[混合物产生的](../Page/混合物.md "wikilink")。[甲苯的硝化可产生](../Page/甲苯.md "wikilink")[2,4,6-三硝基甲苯](../Page/三硝基甲苯.md "wikilink")，即TNT；[苯酚经硝化可得](../Page/苯酚.md "wikilink")[2,4,6-三硝基苯酚](../Page/2,4,6-三硝基苯酚.md "wikilink")，即苦味酸。这两者都可用为[炸药](../Page/炸药.md "wikilink")。

### 磺化反应

[zh_Electrophilic_aromatic_substitution_sulfonation.png](https://zh.wikipedia.org/wiki/File:zh_Electrophilic_aromatic_substitution_sulfonation.png "fig:zh_Electrophilic_aromatic_substitution_sulfonation.png")

苯可与[发烟硫酸反应](../Page/发烟硫酸.md "wikilink")，使一个氢原子被[磺酸基](../Page/磺酸基.md "wikilink")(-SO<sub>3</sub>H)取代而得到[苯磺酸](../Page/苯磺酸.md "wikilink")。发烟硫酸中的[三氧化硫充当亲电试剂](../Page/三氧化硫.md "wikilink")。与硝化反应不同，这个反应是可逆的，当把苯磺酸与稀硫酸共热到100-175℃时，磺酸基可被[质子取代而得回苯](../Page/质子.md "wikilink")。

### 卤代反应

[zh_Electrophilic_aromatic_substitution_chlorination.png](https://zh.wikipedia.org/wiki/File:zh_Electrophilic_aromatic_substitution_chlorination.png "fig:zh_Electrophilic_aromatic_substitution_chlorination.png")催化\]\]

在非质子[路易斯酸如无水](../Page/路易斯酸.md "wikilink")[氯化铁](../Page/氯化铁.md "wikilink")[催化下](../Page/催化.md "wikilink")，[卤素单质](../Page/卤素.md "wikilink")（如[氯](../Page/氯.md "wikilink")、[溴](../Page/溴.md "wikilink")）可与苯反应，生成[卤代苯](../Page/卤代苯.md "wikilink")。值得注意的是，反应中的亲电试剂并非卤素[正离子](../Page/阳离子.md "wikilink")，而是卤素分子与氯化铁的络合物。

### 傅-克反应

### 其他反应

  - [瑞穆尔-悌曼反应](../Page/瑞穆尔-悌曼反应.md "wikilink")
  - [重氮偶合反应](../Page/重氮偶合反应.md "wikilink")
  - [柯尔伯-施密特反应](../Page/柯尔伯-施密特反应.md "wikilink")

## 参见

[化学反应列表](../Page/化学反应列表.md "wikilink")

## 外部連結

  - [Aromatic Substitution Reactions -
    MSU](https://archive.is/20121215085720/http://www.cem.msu.edu/~reusch/VirtTxtJml/benzrx1.htm)

[Category:取代反应](../Category/取代反应.md "wikilink")
[Category:反应机理](../Category/反应机理.md "wikilink")