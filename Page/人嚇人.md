，是一部在1982年上映的香港[恐怖片](../Page/恐怖片.md "wikilink")，由[午馬導演](../Page/午馬.md "wikilink")，並由[洪金寶](../Page/洪金寶.md "wikilink")、[午馬](../Page/午馬.md "wikilink")、[林正英](../Page/林正英.md "wikilink")、[鍾楚紅主演](../Page/鍾楚紅.md "wikilink")。本片曾經榮獲[1984年香港電影金像獎的最佳電影](../Page/1984年香港電影金像獎.md "wikilink")、最佳導演、最佳編劇、最佳男主角、最佳武術指導提名。

## 演員表

  - [洪金寶](../Page/洪金寶.md "wikilink") 飾演 朱宏利
  - [林正英](../Page/林正英.md "wikilink") 飾演 二叔公
  - [鍾楚紅](../Page/鍾楚紅.md "wikilink") 飾演 阿雲
  - [午馬](../Page/午馬.md "wikilink") 飾演 馬麟祥

## 外部链接

  - {{@movies|fdhk30084587}}

  -
  -
  -
  -
  -
  -
[D](../Category/香港驚悚片.md "wikilink")
[2](../Category/1980年代香港電影作品.md "wikilink")
[D](../Category/妖怪题材作品.md "wikilink")