**人工語言**（又稱**人造語言**\[1\]，，簡稱****)，是許多因特定目的、用途，為了某特定使用族群，而人為創造出來的[語言](../Page/語言.md "wikilink")，包括[文法](../Page/文法.md "wikilink")、[單字等等](../Page/單詞.md "wikilink")。人工語言不像[自然語言一樣會隨人類的語言文化而發展](../Page/自然語言.md "wikilink")，但是，它們在被創造之後，卻可能因而產生特定的影響力，隨著人類文化如真實語言一樣地演進。

人工语言可按创造意图分为：

1.  **[辅助语言](../Page/國際輔助語言.md "wikilink")**（英語：，简称auxlang，亦稱[國際輔助語言](../Page/國際輔助語言.md "wikilink")，英語：International
    Auxiliary Language，簡稱IAL）
2.  **[艺术语言](../Page/艺术语言.md "wikilink")**（英語：，简称artlang）
3.  **[工程语言](../Page/工程语言.md "wikilink")**（英語：，简称englang）

## 人工語言列表

## 輔助語言

国际辅助语的目的是为了使全人类以一个共同的语言交流，作为[世界语言](../Page/世界语言.md "wikilink")。现代最普遍的世界语言是[英语](../Page/英语.md "wikilink")，有上亿的使用人数，是多个国家的外语必修课；而影响力最大的国际辅助语[世界语](../Page/世界语.md "wikilink")，也只有约两百万人使用，国际辅助语目前并没有能够作为世界语言的影响力。

常見的國際輔助語如下，更多的國際輔助語請參見[人工語言列表](../Page/人工語言列表.md "wikilink")。

1.  [索来索语](../Page/索来索语.md "wikilink")（Solresol，[法國人Jean](../Page/法國人.md "wikilink")
    François Sudre 作）
2.  [沃拉普克语](../Page/沃拉普克语.md "wikilink")（Volapük，基于欧洲语言）
3.  [世界語](../Page/世界語.md "wikilink")（Esperanto，[維基百科用語之一](../Page/維基百科.md "wikilink")）
4.  [伊多語](../Page/伊多語.md "wikilink")（Ido，世界语改版，维基百科用语之一）
5.  [西方国际语](../Page/西方国际语.md "wikilink")
6.  [诺维亚语](../Page/诺维亚语.md "wikilink")（Novial，基于欧洲语言）
7.  [格罗沙语](../Page/格罗沙语.md "wikilink")（Glosa，基于欧洲语言）
8.  [国际语](../Page/国际语.md "wikilink")（Interlingua，維基百科用語之一）
9.  [欧洲语](../Page/欧洲语.md "wikilink")（Europanto）
10. [國際手語](../Page/國際手語.md "wikilink")

## 工程語言

### 人際交流

1.  [逻辑语](../Page/逻辑语.md "wikilink")（la
    lojban.，[維基用語之一](../Page/維基.md "wikilink")）
2.  [拉丹語](../Page/拉丹語.md "wikilink")（Láadan）
3.  [道本语](../Page/道本语.md "wikilink")（Toki
    pona，源自[皮钦语](../Page/皮钦语.md "wikilink")）
4.  [逸语](../Page/逸语.md "wikilink")（Rhi has）

## 約限語言

意指為了更簡單方便使用，或使非母語人士更容易上手而某種程度轉換的[自然語言](../Page/自然語言.md "wikilink")。

1.  [基本英語](../Page/基本英語.md "wikilink")
2.  [全球語](../Page/全球語.md "wikilink")

## 藝術語言

1.  [福爾摩沙語](../Page/福爾摩沙語_\(撒瑪納札\).md "wikilink") (Formosan)

### J·R·R·托尔金

[魔戒作者](../Page/魔戒.md "wikilink")[托爾金發明的各種](../Page/托爾金.md "wikilink")[中土世界的語言](../Page/中土世界的語言.md "wikilink")，包括[精靈語](../Page/精靈語.md "wikilink")、[矮人語](../Page/矮人語.md "wikilink")


1.  [昆雅语](../Page/昆雅语.md "wikilink")（Quenya）（[諾多精靈語](../Page/諾多精靈.md "wikilink")，基於[芬兰语](../Page/芬兰语.md "wikilink")）
2.  [辛达林](../Page/辛达林.md "wikilink")（Sindarin）（[灰精靈語言](../Page/灰精靈.md "wikilink")，基於[威尔士语](../Page/威尔士语.md "wikilink")）
3.  [黑暗語](../Page/黑暗語.md "wikilink")（Black Speech）
4.  [半獸人語](../Page/半獸人語.md "wikilink")（Orkish）
5.  [矮人語](../Page/矮人語.md "wikilink")（Khuzdul）（基於[闪-含语系的語言](../Page/闪-含语系.md "wikilink")）
6.  [阿登奈克語](../Page/阿登奈克語.md "wikilink")（Adûnaic）
7.  [西方通用語](../Page/西方通用語.md "wikilink")（Westron）
8.  [帖勒瑞林語](../Page/帖勒瑞林語.md "wikilink")（Telerin）
9.  [多瑞亞林語](../Page/多瑞亞林語.md "wikilink")（Doriathrin）
10. [南多林語](../Page/南多林語.md "wikilink")（Nandorin）
11. [洛汗语](../Page/洛汗语.md "wikilink")（Rohirric）
12. [古辛達林](../Page/古辛達林.md "wikilink")（Old Sindarin）
13. [依爾克林語](../Page/依爾克林語.md "wikilink")（Ilkorin）
14. [雅維瑞語](../Page/雅維瑞語.md "wikilink")（Avarin）
15. [樹人語](../Page/樹人語.md "wikilink")（Entish）
16. [主神語](../Page/主神語.md "wikilink")（Valarin）
17. [古精靈語](../Page/古精靈語.md "wikilink")（Primitive Elvish）
18. [早期諾多精靈語](../Page/早期諾多精靈語.md "wikilink")（Gnomish；Goldogrin）（早期發明的[精靈語](../Page/精靈語.md "wikilink")，[辛达林的前身](../Page/辛达林.md "wikilink")）
19. [早期昆雅語](../Page/早期昆雅語.md "wikilink")（Qenya）（早期發明的[精靈語](../Page/精靈語.md "wikilink")，[昆雅的前身](../Page/昆雅.md "wikilink")）

### 奇幻文學 、科幻小說、線上遊戲等架空世界

1.  由[魔戒作者](../Page/魔戒.md "wikilink")[托爾金所創造的各種語言亦可算作此類](../Page/托爾金.md "wikilink")
2.  [英國人](../Page/英國人.md "wikilink")[喬治·奧威爾小說](../Page/喬治·奧威爾.md "wikilink")《[一九八四](../Page/一九八四.md "wikilink")》的[新語](../Page/新語_\(一九八四\).md "wikilink")（Newspeak）
3.  Suzette Haden Elgin 的[拉丹語](../Page/拉丹語.md "wikilink")
4.  《[龍與地下城](../Page/龍與地下城.md "wikilink")》的[黑暗精靈語](../Page/黑暗精靈語.md "wikilink")
5.  [灰衣使者的精靈語](../Page/灰衣使者.md "wikilink")
6.  《[戰鎚](../Page/戰鎚.md "wikilink")》裡的精靈族語言[艾爾薩林語](../Page/艾爾薩林語.md "wikilink")
7.  《[星艦奇航](../Page/星艦奇航記.md "wikilink")》中的外星語言[克林貢語](../Page/克林貢語.md "wikilink")、[瓦肯語](../Page/瓦肯語.md "wikilink")、
8.  《[星界的紋章](../Page/星界的紋章.md "wikilink")》中的外星語言[亞維語](../Page/亞維語_\(虛構語言\).md "wikilink")
9.  《[龍族](../Page/龍族.md "wikilink")》中的[傑彭語](../Page/傑彭語.md "wikilink")
10. 《[失落的帝国](../Page/失落的帝国.md "wikilink")》中的古代語言[亞特蘭提斯語](../Page/亞特蘭提斯語.md "wikilink")
11. 《[魔塔大陸](../Page/魔塔大陸.md "wikilink")》中的[詩魔法語言](../Page/Hymmnos语.md "wikilink")(Hymmnos)
12. 《[阿凡达](../Page/Avatar.md "wikilink")》中[纳美人使用的語言](../Page/纳美人.md "wikilink")[纳美語](../Page/纳美語.md "wikilink")
13. 《[星海爭霸](../Page/星际争霸.md "wikilink")》系列中[星灵使用的星灵语](../Page/神族_\(星海争霸\).md "wikilink")
14. 《[冰与火之歌](../Page/冰与火之歌.md "wikilink")》中[多斯拉其人使用的](../Page/多斯拉其人.md "wikilink")[多斯拉其语](../Page/多斯拉其语.md "wikilink")
15. 《[模拟人生](../Page/模拟人生.md "wikilink")》中的[Simlish](../Page/Simlish.md "wikilink")
16. [多斯拉其語](../Page/多斯拉其語.md "wikilink")(Dothraki language)
17. [Loxian語](../Page/Loxian.md "wikilink")
18. [上古卷轴中的](../Page/上古卷轴.md "wikilink")[多瓦祖爾語](../Page/多瓦祖爾語.md "wikilink")，又稱龍語
19. [神奇寶貝中的神奇寶貝文字](../Page/神奇寶貝.md "wikilink")
20. [Homestuck中的troll文字](../Page/Homestuck.md "wikilink")
21. [漂流武士中的精靈語言](../Page/漂流武士.md "wikilink")
22. [鬥陣特攻中的智械語](../Page/鬥陣特攻.md "wikilink")

## 其他

1.  （基于[日耳曼語](../Page/日耳曼語.md "wikilink")）

2.  （基于[日耳曼語](../Page/日耳曼語.md "wikilink")）

3.  [Dastmen](https://web.archive.org/web/20140921230116/http://dastmen.com/eng/index.html)

4.  [福尔摩沙语](../Page/福尔摩沙语.md "wikilink")（[沙麻納扎](../Page/喬治·撒瑪納札.md "wikilink")'s
    creating）《[福爾摩啥](../Page/福爾摩啥.md "wikilink")》

### 秘密溝通

1.  [兒童黑話](../Page/兒童黑話.md "wikilink")（Pig Latin）
2.  [駭客語](../Page/駭客語.md "wikilink")（Leet）

### 非人靈長目（黑猩猩）與人之交流

1.  [耶基斯語](../Page/耶基斯語.md "wikilink")

## 参考文献

### 引用

### 来源

  -
  -
  -
  -
  - ["Babel's modern
    architects"](http://articles.latimes.com/2007/aug/24/science/sci-conlang24),
    by Amber Dance. *The Los Angeles Times*, 24 August 2007 (Originally
    published as "In their own words -- literally")

## 參見

  - [先創語言與非先創語言](../Page/先創語言與非先創語言.md "wikilink")

  - [人工語言列表](../Page/人工語言列表.md "wikilink")

  - [國際輔助語言](../Page/國際輔助語言.md "wikilink")

  - [Wikipedia:人工語言維基百科](../Page/Wikipedia:人工語言維基百科.md "wikilink")

  - [藝術語言](../Page/藝術語言.md "wikilink")

  - [通用語](../Page/通用語.md "wikilink")

  - [本体语言](../Page/本体语言.md "wikilink")

  -
  - [知识表达](../Page/知识表达.md "wikilink")

## 外部链接

  - [Wikia上的人造語言維基(中文)](http://zh.yuyan.wikia.com/)
  - [一個使用維基系統的人造語言網站(英文)](https://web.archive.org/web/20080618214613/http://www.langmaker.com/db/Main_Page)
  - [Wikia上的人造語言维基網站(英文)](http://conlang.wikia.com/)
  - [The Linguist List
    列出的人工語言](https://web.archive.org/web/20080602142554/http://linguistlist.org/forms/langs/get-familyid.cfm?CFTREEITEMKEY=AE)
  - [國際輔助語言（英文）](http://interlanguages.net/)

{{-}}

[Category:语言](../Category/语言.md "wikilink")
[人工語言](../Category/人工語言.md "wikilink")
[Category:语言学](../Category/语言学.md "wikilink")
[Category:语言列表](../Category/语言列表.md "wikilink")

1.