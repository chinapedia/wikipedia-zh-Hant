[Peoples_Square.jpg](https://zh.wikipedia.org/wiki/File:Peoples_Square.jpg "fig:Peoples_Square.jpg")


**上海人民广场**（）位于[上海市](../Page/上海市.md "wikilink")[黄浦区](../Page/黄浦区.md "wikilink")，是上海的政治、经济、文化、旅游中心和[交通枢纽](../Page/交通枢纽.md "wikilink")，也是上海最为重要的[地标之一](../Page/地标.md "wikilink")。

广义上的“人民广场”主要是由一个开放式的[广场](../Page/广场.md "wikilink")、[人民公园以及周边一些](../Page/上海人民公园.md "wikilink")[文化](../Page/文化.md "wikilink")、[旅游](../Page/旅游.md "wikilink")、[商业建筑等组成](../Page/商业.md "wikilink")。

## 历史

[Race_Couse_in_Shanghai_1912.jpg](https://zh.wikipedia.org/wiki/File:Race_Couse_in_Shanghai_1912.jpg "fig:Race_Couse_in_Shanghai_1912.jpg")
[People_Square_Shanghai_Night_View_2010.jpg](https://zh.wikipedia.org/wiki/File:People_Square_Shanghai_Night_View_2010.jpg "fig:People_Square_Shanghai_Night_View_2010.jpg")

人民广场在[上海开埠前是一片水田](../Page/上海1840年代.md "wikilink")。[道光二十八年](../Page/道光.md "wikilink")（1848年）[英租界扩张至此](../Page/上海公共租界.md "wikilink")，修护城河，筑泥城，被称为“泥城浜”。[小刀会](../Page/小刀会.md "wikilink")[起义期间](../Page/起义.md "wikilink")，[咸丰四年](../Page/咸丰.md "wikilink")（1854年4月3日），小刀会和[英軍](../Page/英軍.md "wikilink")、[美軍在此打击过](../Page/美軍.md "wikilink")[清军](../Page/清军.md "wikilink")，史称“泥城之战”。

[同治二年](../Page/同治.md "wikilink")（1863年）“跑马总会”在英领事馆的支持下强征泥城浜以西地段，这里成了“[上海跑马厅](../Page/上海跑马厅.md "wikilink")”，后来又叫跑马厅，是当时举行[赛马等活动的场所](../Page/赛马.md "wikilink")。开始“华人不得入内”，[宣统元年](../Page/宣统.md "wikilink")（1909年）中国人才可以进入。

1941年[太平洋战争爆发后](../Page/太平洋战争.md "wikilink")，跑马厅成为[日本侵略军的兵营](../Page/日本.md "wikilink")。[抗日战争后](../Page/抗日战争.md "wikilink")，跑马厅又成为美军军营和仓库。

1946年，上海市政府与跑马厅总会进行了数次商洽，欲收回跑马厅。跑马总会于第二年4月9日，向[香港政府按公司登记法登记成](../Page/香港.md "wikilink")3个公司。[上海跑马总会有限公司](../Page/上海跑马总会有限公司.md "wikilink")（会所与看台）；[上海跑马总会马厩有限公司](../Page/上海跑马总会马厩有限公司.md "wikilink")（马厩和宿舍）；[上海跑马总会场地有限公司](../Page/上海跑马总会场地有限公司.md "wikilink")（草地跑道）。此外，中间的430亩公共体育场，仍用[万国体育会的名义向外联系](../Page/万国体育会.md "wikilink")。以上4个单位，实质是跑马总会一家。当时，上海市政府迫于民意，没有同意跑马厅再开赛马，为此，跑马总会只得同意将跑马厅部分土地出让给政府，但要市政府在郊区以10倍的土地建成赛马场与之交换，后因共军接近上海，未成事实。

1951年8月28日，[上海军管会宣布收回跑马厅](../Page/上海军管会.md "wikilink")。9月，[上海市人民政府将跑马厅改建成](../Page/上海市人民政府.md "wikilink")[人民公园和](../Page/人民公园.md "wikilink")[人民广场](../Page/上海人民广场.md "wikilink")。1954年5月31日，跑马总会所有建筑物由人民政府接管，跑马总会大楼改为[上海图书馆](../Page/上海图书馆.md "wikilink")。1957年在[黄陂北路](../Page/黄陂北路_\(上海\).md "wikilink")190号兴建[上海体育宫](../Page/上海体育宫.md "wikilink")。以后又修建了上海市人大办公楼（后重建为上海市府办公楼─[人民大厦](../Page/人民大厦.md "wikilink")）、[上海博物馆](../Page/上海博物馆.md "wikilink")、[上海城市规划展示馆等建筑](../Page/上海城市规划展示馆.md "wikilink")，1992年9月14日，[人民广场综合改造工程开工](../Page/上海人民广场.md "wikilink")。1997年，[上海体育宫动迁至大渡河路](../Page/上海体育宫.md "wikilink")1860号，原址被建为[上海大剧院](../Page/上海大剧院.md "wikilink")。1997年，上海图书馆迁至淮海中路新馆，[上海美术馆进驻原跑马总会大楼](../Page/上海美术馆.md "wikilink")。

1970年4月25日，上海人民广场举行万民公审基督徒[张愚之](../Page/张愚之.md "wikilink")、[陆道雄反革命案](../Page/陆道雄.md "wikilink")，并由电视实况转播。当场宣布他们[死刑立即执行](../Page/死刑.md "wikilink")，在游街示众之后被处决\[1\]\[2\]\[3\]\[4\]。

## 周边建筑

[1964-02_1964年_上海人民广场人民公园.jpg](https://zh.wikipedia.org/wiki/File:1964-02_1964年_上海人民广场人民公园.jpg "fig:1964-02_1964年_上海人民广场人民公园.jpg")
[The_government_building_of_shanghai.jpg](https://zh.wikipedia.org/wiki/File:The_government_building_of_shanghai.jpg "fig:The_government_building_of_shanghai.jpg")\]\]

### 历史建筑

  - [国际饭店](../Page/国际饭店.md "wikilink")
  - [西侨青年会](../Page/上海体育大厦.md "wikilink")
  - [金门大酒店](../Page/金门大酒店.md "wikilink")
  - [大上海电影院](../Page/大上海电影院.md "wikilink")
  - [沐恩堂](../Page/慕尔堂.md "wikilink")
  - [大光明电影院](../Page/大光明电影院_\(上海\).md "wikilink")
  - [逸夫舞台](../Page/逸夫舞台.md "wikilink")
  - [跑马总会](../Page/跑马总会.md "wikilink")

### 现代建筑

  - [上海大剧院](../Page/上海大剧院.md "wikilink")
  - [上海博物馆](../Page/上海博物馆.md "wikilink")
  - [上海城市规划展示馆](../Page/上海城市规划展示馆.md "wikilink")
  - [香港名店街](../Page/香港名店街.md "wikilink")
  - [迪美购物中心](../Page/迪美购物中心.md "wikilink")
  - [人民大厦](../Page/人民大厦.md "wikilink")（上海市政府大楼）
  - [南京东路步行街](../Page/南京东路步行街.md "wikilink")
  - [上海市第一百货商店](../Page/上海市第一百货商店.md "wikilink")
  - [人民公园](../Page/上海人民公园.md "wikilink")
  - [和平影都](../Page/和平影都.md "wikilink")
  - [上海新世界丽笙大酒店](../Page/上海新世界丽笙大酒店.md "wikilink")
  - [世茂国际广场](../Page/世茂国际广场.md "wikilink")
  - [上海明天广场JW万豪酒店](../Page/上海明天广场JW万豪酒店.md "wikilink")

## 公共交通

### [轨道交通](../Page/上海轨道交通.md "wikilink")

[人民广场站](../Page/人民广场站_\(上海市\).md "wikilink")（[1号线](../Page/上海轨道交通1号线.md "wikilink")、[2号线](../Page/上海轨道交通2号线.md "wikilink")、[8号线](../Page/上海轨道交通8号线.md "wikilink")）。

### [地面交通](../Page/上海公交.md "wikilink")

01、18、20、23、37、46、48、49、71、108、109、112、112区间、123、123区间、127、145、167、311、312、318、324、330、451、454、455、518、537、584、782、783、783区间、789、802、864、916、921、925B、930、934、935、936、952、952B、983、隧道三线、隧道六线、隧道夜宵线、上川专线、新川专线、沪朱高速快线、沪青专线、沪青盈专线、沪北青专线等。（详细公交线路请见：[上海市公交线路列表](../Page/上海市公交线路列表.md "wikilink")）

## 参考文献

## 外部链接

## 参见

  - [南京路
    (上海)](../Page/南京路_\(上海\).md "wikilink")、[外滩](../Page/外滩.md "wikilink")、[黄浦区](../Page/黄浦区.md "wikilink")
  - [上海世纪广场](../Page/上海世纪广场.md "wikilink")、[陆家嘴](../Page/陆家嘴.md "wikilink")
  - [徐家汇](../Page/徐家汇.md "wikilink")、[五角场](../Page/江湾五角场.md "wikilink")、[新天地](../Page/新天地.md "wikilink")

{{-}}

[上海人民广场](../Category/上海人民广场.md "wikilink")
[Category:上海地方](../Category/上海地方.md "wikilink")
[Category:上海广场](../Category/上海广场.md "wikilink")
[Category:人民广场](../Category/人民广场.md "wikilink")
[Category:上海交通](../Category/上海交通.md "wikilink")
[Category:上海旅游景点](../Category/上海旅游景点.md "wikilink")
[Category:黄浦区建筑物](../Category/黄浦区建筑物.md "wikilink")

1.  [张乐晨（张愚之之女）见证](http://noah.ccim.org/htdocs/cclife.nsf/e68dc19e63a9f71985256b42005d2dae/e465e39fef1c1cee852568410016f8d5!OpenDocument)
2.  《倪柝声的最后20年》，同案犯陶珠莉的见证，
3.  陆道雄之妻邵圣清：[神时代的见证人](http://sggx002.ccblog.net/archives/2006/12737.html)
4.  [张愚之师母见证：张愚之弟兄殉道記](http://churchinyuanlin.tacomall.com.tw/weeknews/527_20060101.htm)