**《人民画报》**，或称《人民》画报，创刊于1946年8月1日，由[晋冀鲁豫军区政治部人民画报社编辑出版](../Page/晋冀鲁豫军区.md "wikilink")。1948年5月，《[晋察冀画报](../Page/晋察冀画报.md "wikilink")》与《人民画报》合并，改出《[华北画报](../Page/华北画报.md "wikilink")》。\[1\]

《人民画报》从1946年8月1日创刊，至1948年5月结束。期间共出版[画报](../Page/画报.md "wikilink")8期，[增刊](../Page/增刊.md "wikilink")1期，发表[照片共计](../Page/照片.md "wikilink")210幅，共发行13000份。\[2\]《人民画报》除第4期为[木刻和](../Page/木刻.md "wikilink")[漫画外](../Page/漫画.md "wikilink")，其余都刊登以[战事为住的](../Page/战事.md "wikilink")[新闻照片](../Page/新闻照片.md "wikilink")。\[3\]

《人民画报》创刊号为16开本，连[封面](../Page/封面.md "wikilink")[封底共](../Page/封底.md "wikilink")24页，刊照片40幅，占13页；[木刻](../Page/木刻.md "wikilink")、[漫画等](../Page/漫画.md "wikilink")7页。画报刊有[毛泽东和](../Page/毛泽东.md "wikilink")[朱德的](../Page/朱德.md "wikilink")[肖像](../Page/肖像.md "wikilink")，以及[刘伯承](../Page/刘伯承.md "wikilink")、[邓小平](../Page/邓小平.md "wikilink")、[薄一波](../Page/薄一波.md "wikilink")、[杨秀峰等军政要人的题词](../Page/杨秀峰.md "wikilink")。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:中华民国大陆时期已停刊报纸](../Category/中华民国大陆时期已停刊报纸.md "wikilink")
[Category:中共党报](../Category/中共党报.md "wikilink")
[Category:1946年中国建立](../Category/1946年中国建立.md "wikilink")
[Category:1948年廢除](../Category/1948年廢除.md "wikilink")
[R](../Category/畫報.md "wikilink")

1.
2.
3.