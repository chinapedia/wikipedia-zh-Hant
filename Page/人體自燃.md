**人体自燃**或**人体自焚**描述的是一种[超常现象](../Page/超常现象.md "wikilink")，即[人體可在沒有與任何](../Page/人體.md "wikilink")[火源接觸下](../Page/火.md "wikilink")，體內突然起火，而且通常溫度高到短時間內就可以將人體絕大部分燒化為灰燼，（目前所知人体自燃的例子幾乎都燒到只剩一部分軀體）但周圍的東西（包括[可燃物體](../Page/可燃物.md "wikilink")）卻可以沒有燒掉，因此有部分人能够幸存。不过，这[现象的存在性和](../Page/现象.md "wikilink")[科学性却引起不少争议](../Page/科学.md "wikilink")。

试图解释这一现象的观点，引援歷史上和現代也可能发生的一些个案，认为造成人体自燃的原因包括[球狀閃電引起](../Page/球狀閃電.md "wikilink")、[靜電引起](../Page/靜電.md "wikilink")、[灯芯效应](../Page/灯芯效应.md "wikilink")、體內[酮體過多等](../Page/酮體.md "wikilink")。

## 著名案例

1951年，[美國](../Page/美國.md "wikilink")[佛羅里達州](../Page/佛羅里達州.md "wikilink")[聖彼得斯堡一名婦女](../Page/聖彼得斯堡_\(佛羅里達州\).md "wikilink")夫人在公寓內因自燃而死亡，她所坐的椅子也連帶被燒毀了，但房間的其他部分卻近乎完好無損\[1\]\[2\]。

較近期的人体自燃事件發生於2017年12月，[英國一位](../Page/英國.md "wikilink")[退休老人走在街道上時突然莫名地著火燃燒](../Page/退休.md "wikilink")，造成嚴重燒傷，送醫後仍宣告不治。警方表示無從得知火從何而來\[3\]。

## 文化參照

英國作家[查爾斯·狄更斯小說](../Page/查爾斯·狄更斯.md "wikilink")《[荒涼山莊](../Page/荒涼山莊.md "wikilink")》中，反派角色克魯克即死於人體自燃\[4\]。

## 參考資料

<references />

## 外部連結

  - [“人体自燃”是真是假？](http://www.xys.org/xys/netters/Fang-Zhouzi/science/shc.txt)[方舟子](../Page/方舟子.md "wikilink")
  - [CCTV.com-86歲老人會自燃？](https://web.archive.org/web/20080919134414/http://big5.cctv.com/program/zoujinkexue/20070528/102308.shtml)
  - [人体自焚之謎](https://web.archive.org/web/20070927225942/http://119.sd.cn/InfoCenter/Pagelet/PageShow.ASP?CnnStr=zine&Where=RID:1434)
  - [人体自燃的原因何在？](https://web.archive.org/web/20071009005350/http://www.7gua.net/qiwen/bujie/90832.html)
  - [人体自燃死亡之谜](https://web.archive.org/web/20070929224400/http://www.ucren.com/Myart/20061202eSlWGj.html)
  - [上帝的惩罚？难解的人体自燃之谜](http://xlpigbbs.zhongsou.com/l.dll?DoRequest_r&id=1571050&word=%B6%C1%CA%E9&cid=1528)
  - [男子身体突然起火，其女友下体惨被烧焦](https://archive.is/20120712101835/http://news.taizhou.com/news/2008-1/26/c92f93cj77yo8nv6.html)
  - [「人體自燃」的可能性？　酮體過高讓人燒得只剩腳丫](http://www.ettoday.net/news/20120827/93501.htm)

[S](../Category/超常現象.md "wikilink") [S](../Category/死因.md "wikilink")
[S](../Category/燃燒.md "wikilink")

1.  [No New Clues In Reeser Death; Debris Sent To
    Lab](https://news.google.com/newspapers?id=sARZAAAAIBAJ&sjid=lE8DAAAAIBAJ&pg=3822,1539798&dq=mary+reeser&hl=en)，St.
    Petersburg Times

2.

3.

4.