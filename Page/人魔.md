是一部2001年的美國[驚悚電影](../Page/驚悚.md "wikilink")，為《[沉默的羔羊](../Page/沉默的羔羊.md "wikilink")》的續集電影，改編自[湯瑪斯·哈里斯同名小說](../Page/湯瑪斯·哈里斯.md "wikilink")。導演是[雷利·史考特](../Page/雷利·史考特.md "wikilink")，[安東尼·霍普金斯飾演男主角](../Page/安東尼·霍普金斯.md "wikilink")[漢尼拔·萊克特](../Page/漢尼拔·萊克特.md "wikilink")，[茱莉安·摩爾飾演女主角](../Page/茱莉安·摩爾.md "wikilink")[FBI探員](../Page/FBI.md "wikilink")。該片贏得連續兩週的北美票房冠軍。為了不想再強化漢尼拔的魅力，電影改編後的結局與原著小說並不同。電影中女主角拒絕了漢尼拔，小說中最後女主角則與漢尼拔在一起。

## 劇情

電影開頭時已是上一集事件發生後10年，與手下正在想辦法攻入一名女毒犯的巢穴，而記者亦在現場等消息。幾經勸告後，毒犯與她的手下終於出來，但女毒犯懷中卻抱著她的嬰兒。最後，因同事的失誤，毒犯與[FBI探員發生槍戰](../Page/FBI.md "wikilink")，女毒犯與其四名手下在鏡頭前被射殺，只留下在哭泣的嬰兒。[司法部官員Paul](../Page/美國司法部.md "wikilink")
Krendler（[雷·李歐塔飾](../Page/雷·李歐塔.md "wikilink")）因以前對史黛琳求愛不遂，一直懷恨在心，現在即借此機會報復，將整件事歸咎於史黛琳。

新聞大肆報導此事件及史黛琳的歷史，當然亦包括她原來曾負責[漢尼拔·萊克特一案](../Page/漢尼拔·萊克特.md "wikilink")。萊克特的一名受害人極為富有，看到新聞後利用他的財富影響FBI，要求他們將史黛琳調回偵查萊克特的案件。Verger原來是一名有[戀童癖的](../Page/戀童癖.md "wikilink")[虐待狂](../Page/虐待狂.md "wikilink")，因萊克特的緣故而弄到自己毀容及[四肢癱瘓](../Page/四肢癱瘓.md "wikilink")，現在一心只希望報仇。

正如Verger所估計，萊克特得知史黛琳受責後，寄了一封信給史黛琳。信中沒任何線索，但信紙上卻帶有一絲淡淡的香氣。史黛琳找[香水專家一看](../Page/香水.md "wikilink")，原來這是一種很罕見的潤膚霜，世上只幾間店有賣，而其中一間則在[佛羅倫斯](../Page/佛羅倫斯.md "wikilink")。

## 演員

  - [安東尼·鶴健士](../Page/安東尼·鶴健士.md "wikilink") 飾
    [漢尼拔·萊克特](../Page/漢尼拔·萊克特.md "wikilink")

  - [茱莉安·摩亞](../Page/茱莉安·摩亞.md "wikilink") 飾

  - [加利·奧文](../Page/加利·奧文.md "wikilink") 飾

  - [雷·李歐塔](../Page/雷·李歐塔.md "wikilink") 飾 Paul Krendler

  - [弗蘭基·費森](../Page/弗蘭基·費森.md "wikilink") 飾 Barney Matthews

  - 飾 Chief Inspector Rinaldo Pazzi

  - 飾 Allegra Pazzi

  - 飾 Dr. Cordell Doemling

  - 飾 Evelda Drumgo

  - 飾 Sogliato

  - 飾 FBI Agent Pearsall

  - 飾 FBI Asst. Director Noonan

  - James Opher 飾 DEA Agent John Eldridge

  - 飾 Gnocco

  - 飾 Carlo Deogracias

  - 飾 Beatrice

## 血腥鏡頭

此片比上一集更血腥噁心，特別是[漢尼拔·萊克特在Paul](../Page/漢尼拔·萊克特.md "wikilink")
Krendler還在生時打開他的頭骨，把他的腦袋一片一片切下來，煎熟再餵給Krendler自己吃那一幕。

## 前傳

  - 《[沉默的赤龍](../Page/沉默的赤龍.md "wikilink")》（2002）
  - 《》（2007）
  - 《[雙面人魔](../Page/雙面人魔.md "wikilink")》（2013–2015）－電視劇

## 外部連結

  - [官方網站](http://www.hannibalmovie.com/)

  -

  -

  -

[Category:2001年电影](../Category/2001年电影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:2000年代驚悚片](../Category/2000年代驚悚片.md "wikilink")
[Category:美國驚悚片](../Category/美國驚悚片.md "wikilink")
[Category:續集電影](../Category/續集電影.md "wikilink")
[Category:環球影業電影](../Category/環球影業電影.md "wikilink")
[Category:人吃人題材電影](../Category/人吃人題材電影.md "wikilink")
[Category:連環殺手題材電影](../Category/連環殺手題材電影.md "wikilink")
[Category:心理驚悚片](../Category/心理驚悚片.md "wikilink")
[Category:驚悚小說改編電影](../Category/驚悚小說改編電影.md "wikilink")
[Category:漢尼拔·萊克特](../Category/漢尼拔·萊克特.md "wikilink")
[Category:佛羅倫斯背景電影](../Category/佛羅倫斯背景電影.md "wikilink")
[Category:北卡羅來納州取景電影](../Category/北卡羅來納州取景電影.md "wikilink")
[Category:維吉尼亞州取景電影](../Category/維吉尼亞州取景電影.md "wikilink")