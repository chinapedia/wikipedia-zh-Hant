[Ninnaji_Kyoto02s3s4350.jpg](https://zh.wikipedia.org/wiki/File:Ninnaji_Kyoto02s3s4350.jpg "fig:Ninnaji_Kyoto02s3s4350.jpg")
[Peacock_Myōō.jpg](https://zh.wikipedia.org/wiki/File:Peacock_Myōō.jpg "fig:Peacock_Myōō.jpg")\]\]
**仁和寺**（）是一座位於[日本](../Page/日本.md "wikilink")[京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[右京區的](../Page/右京區.md "wikilink")[佛教寺院](../Page/佛教.md "wikilink")，是[真言宗御室派的](../Page/真言宗御室派.md "wikilink")[總本山](../Page/本山_\(佛教\).md "wikilink")，由[宇多天皇在](../Page/宇多天皇.md "wikilink")[仁和](../Page/仁和.md "wikilink")4年（888年）開基（創建）。[山號](../Page/山號.md "wikilink")**大內山**的仁和寺主要奉祀的是[阿彌陀如來](../Page/阿彌陀如來.md "wikilink")，1994年時與多座京都地區的古蹟一同以「[古都京都的文化財](../Page/古都京都的文化財.md "wikilink")」名義，被[聯合國科教文組織列入](../Page/聯合國科教文組織.md "wikilink")[世界遺產名單之列](../Page/世界遺產.md "wikilink")。

通常是由[皇室或貴族擔任](../Page/日本皇室.md "wikilink")[住持的仁和寺](../Page/住持.md "wikilink")，是所謂的[門跡寺院之一](../Page/門跡寺院.md "wikilink")，宇多天皇在卸位[出家後於此以](../Page/出家.md "wikilink")[法皇的身份居住修行](../Page/太上法皇.md "wikilink")，因此獲得**御室御所**的別稱。直到近代[明治維新之後不再由皇族擔任住持](../Page/明治維新.md "wikilink")，才改稱為「舊御室御所」。

除了宗教與歷史上的重要性外，仁和寺也以其境內的[櫻花聞名](../Page/櫻花.md "wikilink")，境內約有200棵被稱為「御室櫻」的櫻花樹。這些櫻花樹除了樹高比一般的櫻花樹低、枝幹低伸給人一種像是在低頭請安的視覺印象，而有「多福櫻」（）的別稱外，由於它們約在每年4月20日過後才會盛開，是京都各地的賞櫻勝地中花期最晚的，因此到此處賞櫻常被認為是花季的完結收尾。1990年時，仁和寺的御室櫻獲選為[日本櫻名所100選之一](../Page/日本櫻名所100選.md "wikilink")。

## 參考文獻

  - 井上靖、塚本善隆監修、山本健吉、森諦円著『古寺巡礼京都11　仁和寺』、淡交社、1977
  - 竹村俊則『昭和京都名所図会 洛西』駸々堂、1983
  - 『週刊朝日百科　日本の国宝』14号（仁和寺）、朝日新聞社、1997
  - 『日本歴史地名大系　京都市の地名』、平凡社
  - 『角川日本地名大辞典　京都府』、角川書店
  - 『国史大辞典』、吉川弘文館

## 相關條目

  - ：仁和寺之鎮守神

  - [日本國寶列表](../Page/日本國寶列表.md "wikilink")

  - 《[徒然草](../Page/徒然草.md "wikilink")》：[吉田兼好法師所著的](../Page/吉田兼好.md "wikilink")[隨筆文學](../Page/隨筆.md "wikilink")，其第五十二段的內容中有提及仁和寺與[石清水八幡宮等京都古剎](../Page/石清水八幡宮.md "wikilink")。

## 外部連結

  - [仁和寺官方網站](http://www.ninnaji.or.jp/)

[Category:日本世界遺產](../Category/日本世界遺產.md "wikilink")
[Category:登錄有形文化財](../Category/登錄有形文化財.md "wikilink")
[Category:京都市佛寺](../Category/京都市佛寺.md "wikilink")
[Category:日本国宝](../Category/日本国宝.md "wikilink")
[\*](../Category/真言宗御室派.md "wikilink")
[Category:勅願寺](../Category/勅願寺.md "wikilink")
[Category:日本櫻名所100選](../Category/日本櫻名所100選.md "wikilink")
[Category:京都市重要文化財](../Category/京都市重要文化財.md "wikilink")
[Category:右京區](../Category/右京區.md "wikilink")
[Category:888年亞洲建立](../Category/888年亞洲建立.md "wikilink")
[Category:京都府史跡](../Category/京都府史跡.md "wikilink")
[Category:日本國指定名勝](../Category/日本國指定名勝.md "wikilink")