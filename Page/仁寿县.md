**仁寿县**，古称陵州，为中国[四川省](../Page/四川省.md "wikilink")[眉山市辖县](../Page/眉山市.md "wikilink")，位于[四川盆地中西部](../Page/四川盆地.md "wikilink")[丘陵地带](../Page/丘陵.md "wikilink")，为[四川省肉类生产第二大、中国第12大县市](../Page/中国肉类生产百强县列表.md "wikilink")。

## 历史

仁寿县名的来历，一说来自[隋朝](../Page/隋朝.md "wikilink")[开皇十五年建成的](../Page/开皇.md "wikilink")[仁寿宫](../Page/仁寿宫.md "wikilink")，一说来自县城西十里的仁寿水，另一说和怀仁有关。“仁寿”寓意吉祥，意为仁爱长寿。

[秦朝时](../Page/秦朝.md "wikilink")，仁寿县境属武阳县，归[蜀郡](../Page/蜀郡.md "wikilink")。[汉武帝时](../Page/汉武帝.md "wikilink")，武阳县属新设的[犍为郡](../Page/犍为郡.md "wikilink")。[南北朝时](../Page/南北朝.md "wikilink")，[南梁置](../Page/南梁.md "wikilink")[怀仁郡](../Page/怀仁郡.md "wikilink")，下设怀仁县，[西魏时改为普宁县](../Page/西魏.md "wikilink")。[隋朝](../Page/隋朝.md "wikilink")[开皇十八年](../Page/开皇.md "wikilink")（593年），改普宁为仁寿，为陵州治，始有仁寿县名。

[唐朝](../Page/唐朝.md "wikilink")[天宝元年](../Page/天宝_\(唐朝\).md "wikilink")（742年），改陵州为仁寿郡。[南宋](../Page/南宋.md "wikilink")[乾道六年](../Page/乾道_\(南宋\).md "wikilink")（1170年），属隆州。[明朝](../Page/明朝.md "wikilink")[洪武六年](../Page/洪武.md "wikilink")（1373年），分仁寿县复置[井研县](../Page/井研县.md "wikilink")。[清朝](../Page/清朝.md "wikilink")[雍正五年](../Page/雍正.md "wikilink")（1727年），由[成都府改隶属](../Page/成都府.md "wikilink")[川南道](../Page/川南道.md "wikilink")[资州](../Page/资州.md "wikilink")。

民国元年，仁寿县属成都府，1913年改属[下川南道](../Page/下川南道.md "wikilink")，1914年下川南道改称[永宁道](../Page/永宁道.md "wikilink")，1924年废道，直属[四川省政府区](../Page/四川省政府.md "wikilink")。1950年属[川南行政区](../Page/川南行政区.md "wikilink")[内江专区](../Page/内江专区.md "wikilink")，1958年改属[乐山专区](../Page/乐山专区.md "wikilink")，1985年属省辖[乐山市](../Page/乐山市.md "wikilink")，1997年划归新成立的[眉山地区管辖](../Page/眉山地区.md "wikilink")。现属眉山市。

2017年9月，因眉山市政府欲将仁寿县的黑龙滩、视高、清水等六个城镇划归天府新区眉山片区，引发当地数万民众游行示威。\[1\]
被划走的视高镇属当地的工业重镇，而黑龙潭当则为当地著名景区，引发民众不满，认为是对其优质的资源的抢夺。\[2\]

## 地理

仁寿位于四川盆地中西部，属丘陵地带；西为[岷江](../Page/岷江.md "wikilink")，东为[沱江](../Page/沱江.md "wikilink")，位于两大水系之间；北邻[成都平原](../Page/成都平原.md "wikilink")，其中以[龙泉山相隔](../Page/龙泉山.md "wikilink")。仁寿县城文林镇与[成都市相距](../Page/成都市.md "wikilink")90公里，境内北临龙泉山的文宫镇与成都相距50余公里。中华人民共和国成立后，仁寿县曾隶属乐山地区，1997年改属眉山地区。仁寿周边，北邻[双流](../Page/双流.md "wikilink")、[龙泉驿](../Page/龙泉驿.md "wikilink")，东邻[简阳市](../Page/简阳市.md "wikilink")、[资阳市](../Page/资阳市.md "wikilink")、[资中县](../Page/资中县.md "wikilink")，南邻[威远县](../Page/威远县.md "wikilink")、[荣县](../Page/荣县.md "wikilink")、井研县，西邻[青神县](../Page/青神县.md "wikilink")、眉山市、[彭山县](../Page/彭山县.md "wikilink")。

全境面积约2606平方公里，人口约162万，为四川人口第一大县（全国第三），是四川盆地典型的人口稠密地区。

属亚热带季风湿润气候，年均气温17.4℃，年均降雨1009.4㎜，年均日照1196.6小时，无霜期312天。

## 经济

仁寿是全国产粮大县，全国商品粮生产基地，全国瘦肉型猪生产基地，因所产[枇杷质优](../Page/枇杷.md "wikilink")，被授予“中国枇杷之乡”称号。

## 交通

境内道路交错。纵有[213国道北接成都和双流国际机场](../Page/213国道.md "wikilink")，南通乐山和自贡；横有106省道西接[成昆铁路和](../Page/成昆铁路.md "wikilink")[成乐高速公路](../Page/成乐高速公路.md "wikilink")，东连[成渝铁路和](../Page/成渝铁路.md "wikilink")[成渝高速公路](../Page/成渝高速公路.md "wikilink")。成自泸高速，遂资眉高速过境。

## 教育

中学：

  - [仁寿一中](../Page/仁寿一中.md "wikilink")，国家示范中学。
  - 铧强中学
  - 华兴中学

仁寿职业教育发达，有职业学校7所，在校生超过23000人。

## 旅游

  - 黑龙滩湖（湖面23.6平方公里的人工湖，散布着85个岛屿，有龙岩、淡水现字等景观，是四川最大的人工湖）
  - “中华第一胸佛”牛角寨大佛（为建造乐山大佛的原型）
  - 禾嘉中华溶洞（深达30公里）
  - 天梯公园
  - 奎星阁
  - 双堡牌坊

## 特产

  - 枇杷
  - 丰水梨
  - 仁寿芝麻糕
  - 汪洋干巴牛肉

## 知名人物

  - [何㮚](../Page/何㮚.md "wikilink")，北宋宰相，状元。曾组织京师保卫战。
  - [韩驹](../Page/韩驹.md "wikilink")，宋代文学家
  - [虞允文](../Page/虞允文.md "wikilink")，南宋军事家、政治家，抗金名相。
  - [唐式遵](../Page/唐式遵.md "wikilink")，抗日将领
  - [黄汲清](../Page/黄汲清.md "wikilink")，近代地质学家。
  - [石鲁](../Page/石鲁.md "wikilink")：现代中国画家，[长安画派创始人](../Page/长安画派.md "wikilink")。

## 外部链接

  - [仁寿县人民政府](http://www.rs.gov.cn/)

[仁寿县](../Category/仁寿县.md "wikilink") [县](../Category/眉山区县.md "wikilink")
[眉山](../Category/四川省县份.md "wikilink")

1.  [四川划新区消息引民愤
    数万民众抗议爆冲突](http://www.epochtimes.com/gb/17/9/22/n9659834.htm)，大纪元，2017年9月23日。
2.  [四川数万人抗议区域调整　官方启动反恐级维稳](http://www.rfa.org/cantonese/news/protest-09232017100700.html)，自由亚洲电台，2017年9月24日。