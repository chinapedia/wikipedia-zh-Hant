《**仁心仁術**》（）是一个由[美国全国广播公司播放的醫療主題](../Page/美国全国广播公司.md "wikilink")[电视剧系列](../Page/电视剧.md "wikilink")。该片基于小说家[迈克尔·克莱顿的创作](../Page/迈克尔·克莱顿.md "wikilink")，故事通过[伊利诺斯州](../Page/伊利诺斯州.md "wikilink")[芝加哥市库克县县综合医院這家虛構醫院的急诊室裡外所发生的种种故事](../Page/芝加哥.md "wikilink")，展现出[美国](../Page/美国.md "wikilink")[社会八方面相](../Page/社会.md "wikilink")。1994年9月19日首播，2009年4月2日播出了第15季最後一集、也是整個系列的最後一集。15季間共播出331集，成為美國電視史上最長壽的黃金時段醫療電視劇。本劇获得23項[艾美奖獎項](../Page/艾美奖.md "wikilink")，其中包括1996年傑出電視劇獎（Outstanding
Drama
Series），並總共獲124次艾美獎提名，為劇集類節目中獲提名最多者\[1\]。由于该剧的热播，中国，[香港](../Page/香港.md "wikilink")、[韩国](../Page/韩国.md "wikilink")、[日本](../Page/日本.md "wikilink")、[台灣等地都相继拍摄了同类型的电视剧](../Page/台灣.md "wikilink")。

## 历史與製作

故事源於1974年，作家[迈克尔·克莱顿根據他在一家繁忙的醫院急診室裡擔任](../Page/迈克尔·克莱顿.md "wikilink")[住院醫生的個人經歷](../Page/醫生.md "wikilink")，寫了一個劇本
\[2\]。可是這個劇本沒有獲得注意，克賴頓便轉向其他題材發展。1990年，他發表了小說《[侏羅紀公園](../Page/侏羅紀公園.md "wikilink")》，三年後與導演[史蒂芬·斯皮尔伯格合作將小說改拍為電影](../Page/史蒂芬·斯皮尔伯格.md "wikilink")\[3\]。完成後二人便著手製作《仁心仁術》，並打算將他拍成一集兩小時的電視劇[試播集](../Page/電視試播.md "wikilink")，而非正規電影\[4\]。斯皮尔伯格的[Amblin
Entertainment公司任用](../Page/:en:Amblin_Entertainment.md "wikilink")[約翰·韋爾斯為劇集的](../Page/:en:約翰·韋爾斯.md "wikilink")[執行監製](../Page/執行製作人.md "wikilink")。用來拍攝該試播集的劇本與克莱顿於1974的原版大致相同，最大的更改是將陸蘇珊的角色改為女性、班彼特的角色改為[非裔美國人](../Page/非裔美國人.md "wikilink")，而試播集本身縮短了約20分鐘，以遷就網絡電視兩小時（包含廣告）的播映時間\[5\]。由于缺乏时间和資金，因此试拍集是在[洛杉矶前](../Page/洛杉矶.md "wikilink")[琳达維斯塔社区医院的舊址裡實地拍摄](../Page/:en:Linda_Vista_Community_Hospital.md "wikilink")，该医院於1990年已經停止运营\[6\]。其後，製造組在[华纳兄弟位於加州](../Page/华纳兄弟.md "wikilink")[柏本克的製片廠裡搭建了參照](../Page/柏本克_\(加利福尼亞州\).md "wikilink")[洛杉磯縣綜合醫院急症室的佈景](../Page/:en:LAC+USC_Medical_Center.md "wikilink")；另外由於故事發生於芝加哥，所以亦經常於芝加哥市內取景，當中包括著名的[芝加哥地鐵車站月台](../Page/芝加哥地鐵.md "wikilink")\[7\]。

## 主要演員與角色表

註：本篇內的角色香港譯名出自香港[無綫電視的版本](../Page/無綫電視.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>演員</p></th>
<th><p>角色</p></th>
<th><p>粵語配音<br />
TVB</p></th>
<th><p>季度</p></th>
<th><p>演出<br />
集數</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>4</p></td>
<td><p>5</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/安東尼·愛德華.md" title="wikilink">安東尼·愛德華</a><br />
<a href="../Page/:en:Anthony_Edwards.md" title="wikilink">Anthony Edwards</a></p></td>
<td><p><a href="../Page/馬克·格林.md" title="wikilink">馬克·格林</a><br />
<a href="../Page/:en:Mark_Greene.md" title="wikilink">Mark Greene</a></p></td>
<td><p><a href="../Page/陳欣_(配音員).md" title="wikilink">陳欣</a>(S.1-8)</p></td>
<td><p><strong>主演</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/佐治·古尼.md" title="wikilink">佐治·古尼</a><br />
<a href="../Page/:en:George_Clooney.md" title="wikilink">George Clooney</a></p></td>
<td><p>羅道格<br />
<a href="../Page/:en:Doug_Ross.md" title="wikilink">Doug Ross</a></p></td>
<td><p><a href="../Page/林保全.md" title="wikilink">林保全</a>(S.1-6)</p></td>
<td><p><strong>主演</strong></p></td>
<td><p><strong>特別<br />
客串</strong></p></td>
</tr>
<tr class="even">
<td><p>雪莉·史特靈費特<br />
<a href="../Page/:en:Sherry_Stringfield.md" title="wikilink">Sherry Stringfield</a></p></td>
<td><p>陸蘇珊<br />
<a href="../Page/:en:Susan_Lewis.md" title="wikilink">Susan Lewis</a></p></td>
<td><p><a href="../Page/陸惠玲.md" title="wikilink">陸惠玲</a>(S.1-3)<br />
<a href="../Page/曾秀清.md" title="wikilink">曾秀清</a>(S.8-12,15)</p></td>
<td><p><strong>主演</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/諾亞·懷利.md" title="wikilink">諾亞·懷利</a><br />
<a href="../Page/:en:Noah_Wyle.md" title="wikilink">Noah Wyle</a></p></td>
<td><p>卡約翰<br />
<a href="../Page/:en:John_Carter_(ER).md" title="wikilink">John Carter</a></p></td>
<td><p><a href="../Page/蘇強文.md" title="wikilink">蘇強文</a>(S.1)<br />
<a href="../Page/馮錦堂.md" title="wikilink">馮錦堂</a>(S.2-12,15)</p></td>
<td><p><strong>主演</strong></p></td>
<td><p><strong>特別<br />
客串</strong></p></td>
</tr>
<tr class="even">
<td><p>艾力·拉沙爾<br />
<a href="../Page/:en:Eriq_La_Salle.md" title="wikilink">Eriq La Salle</a></p></td>
<td><p>班彼得<br />
<a href="../Page/:en:Peter_Benton.md" title="wikilink">Peter Benton</a></p></td>
<td><p><a href="../Page/招世亮.md" title="wikilink">招世亮</a>(S.1-8)<br />
<a href="../Page/李錦綸_(配音員).md" title="wikilink">李錦綸</a>(S.15)</p></td>
<td><p><strong>主演</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/茱莉安娜·瑪格里斯.md" title="wikilink">茱莉安娜·瑪格里斯</a><br />
<a href="../Page/:en:Julianna_Margulies.md" title="wikilink">Julianna Margulies</a></p></td>
<td><p>夏卡洛<br />
<a href="../Page/:en:Carol_Hathaway.md" title="wikilink">Carol Hathaway</a></p></td>
<td><p><a href="../Page/謝潔貞.md" title="wikilink">謝潔貞</a><br />
<a href="../Page/梁少霞.md" title="wikilink">梁少霞</a></p></td>
<td><p><strong>主演</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>歌莉亞·魯賓<br />
<a href="../Page/:en:Gloria_Reuben.md" title="wikilink">Gloria Reuben</a></p></td>
<td><p>布詹妮<br />
<a href="../Page/:en:Jeanie_Boulet.md" title="wikilink">Jeanie Boulet</a></p></td>
<td><p><a href="../Page/蔡惠萍.md" title="wikilink">蔡惠萍</a>(S.1-6)</p></td>
<td><p>colspan="1" style="background:#ADD8E6" text-align:center;"|<strong>客串</strong></p></td>
<td><p><strong>主演</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/溫明娜.md" title="wikilink">溫明娜</a><br />
<a href="../Page/:en:Ming-Na.md" title="wikilink">Ming-Na</a></p></td>
<td><p>陳景梅/<br />
秦黛碧/<br />
秦珍美<br />
<a href="../Page/:en:Jing-Mei_&quot;Deb&quot;_Chen.md" title="wikilink">Jing-Mei Chen</a></p></td>
<td><p><a href="../Page/雷碧娜.md" title="wikilink">雷碧娜</a></p></td>
<td><p>colspan="1" style="background:#ADD8E6" text-align:center;"|<strong>客串</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>羅拉·伊妮斯<br />
<a href="../Page/:en:Laura_Innes.md" title="wikilink">Laura Innes</a></p></td>
<td><p>韋嘉莉<br />
<a href="../Page/:en:Kerry_Weaver.md" title="wikilink">Kerry Weaver</a></p></td>
<td><p><a href="../Page/盧素娟.md" title="wikilink">盧素娟</a>(S.2-11)<br />
<a href="../Page/黃玉娟.md" title="wikilink">黃玉娟</a>(S.12-13,15)</p></td>
<td></td>
<td><p>colspan="1" style="background:#ADD8E6" text-align:center;"|<strong>客串</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/瑪利亞·貝洛.md" title="wikilink">瑪利亞·貝洛</a><br />
<a href="../Page/:en:Maria_Bello.md" title="wikilink">Maria Bello</a></p></td>
<td><p>戴安娜<br />
<a href="../Page/:en:Anna_Del_Amico.md" title="wikilink">Anna Del Amico</a></p></td>
<td><p><a href="../Page/鄭麗麗.md" title="wikilink">鄭麗麗</a></p></td>
<td></td>
<td><p>colspan="1" style="background:#ADD8E6" text-align:center;"|<strong>客串</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿莉克絲·金斯頓.md" title="wikilink">阿莉克絲·金斯頓</a><br />
<a href="../Page/:en:Alex_Kingston.md" title="wikilink">Alex Kingston</a></p></td>
<td><p>康依麗<br />
<a href="../Page/:en:Elizabeth_Corday.md" title="wikilink">Elizabeth Corday</a></p></td>
<td><p><a href="../Page/陸惠玲.md" title="wikilink">陸惠玲</a></p></td>
<td></td>
<td><p><strong>主演</strong></p></td>
</tr>
<tr class="odd">
<td><p>保羅·麥肯<br />
<a href="../Page/:en:Paul_McCrane.md" title="wikilink">Paul McCrane</a></p></td>
<td><p>盧樂彬<br />
Robert Romano</p></td>
<td><p><a href="../Page/陳永信.md" title="wikilink">陳永信</a>(S.4-10)</p></td>
<td></td>
<td><p><strong>常規</strong></p></td>
</tr>
<tr class="even">
<td><p>凱莉·馬田<br />
<a href="../Page/:en:Kellie_Martin.md" title="wikilink">Kellie Martin</a></p></td>
<td><p>凌露思<br />
<a href="../Page/:en:Lucy_Knight.md" title="wikilink">Lucy Knight</a></p></td>
<td><p><a href="../Page/林元春.md" title="wikilink">林元春</a></p></td>
<td></td>
<td><p><strong>主演</strong></p></td>
</tr>
<tr class="odd">
<td><p>高蘭·維斯耶克<br />
<a href="../Page/:en:Goran_Višnjić.md" title="wikilink">Goran Višnjić</a></p></td>
<td><p>鄺路加<br />
<a href="../Page/:en:Luka_Kovač.md" title="wikilink">Luka Kovač</a></p></td>
<td><p><a href="../Page/蘇強文.md" title="wikilink">蘇強文</a></p></td>
<td></td>
<td><p><strong>主演</strong></p></td>
</tr>
<tr class="even">
<td><p>米高·米雪<br />
<a href="../Page/:en:Michael_Michele.md" title="wikilink">Michael Michele</a></p></td>
<td><p>費嘉露<br />
<a href="../Page/:en:Cleo_Finch.md" title="wikilink">Cleo Finch</a></p></td>
<td><p><a href="../Page/黃鳳英.md" title="wikilink">黃鳳英</a></p></td>
<td></td>
<td><p><strong>主演</strong></p></td>
</tr>
<tr class="odd">
<td><p>艾力·柏拉甸奴<br />
<a href="../Page/:en:Erik_Palladino.md" title="wikilink">Erik Palladino</a></p></td>
<td><p>文迪夫<br />
<a href="../Page/:en:Dave_Malucci.md" title="wikilink">Dave Malucci</a></p></td>
<td><p><a href="../Page/陳卓智.md" title="wikilink">陳卓智</a></p></td>
<td></td>
<td><p><strong>主演</strong></p></td>
</tr>
<tr class="even">
<td><p>莫拉·蒂妮<br />
<a href="../Page/:en:Maura_Tierney.md" title="wikilink">Maura Tierney</a></p></td>
<td><p>洛雅碧<br />
<a href="../Page/:en:Abby_Lockhart.md" title="wikilink">Abby Lockhart</a></p></td>
<td><p><a href="../Page/鄭麗麗.md" title="wikilink">鄭麗麗</a></p></td>
<td></td>
<td><p><strong>主演</strong></p></td>
</tr>
<tr class="odd">
<td><p>沙里夫·阿特金斯<br />
<a href="../Page/:en:Sharif_Atkins.md" title="wikilink">Sharif Atkins</a></p></td>
<td><p>賈米高<br />
<a href="../Page/:en:Michael_Gallant.md" title="wikilink">Michael Gallant</a></p></td>
<td><p><a href="../Page/李錦綸_(配音員).md" title="wikilink">李錦綸</a><br />
<a href="../Page/郭志權.md" title="wikilink">郭志權</a></p></td>
<td></td>
<td><p><strong>主演</strong></p></td>
</tr>
<tr class="even">
<td><p>麥凱·費法<br />
<a href="../Page/:en:Mekhi_Phifer.md" title="wikilink">Mekhi Phifer</a></p></td>
<td><p>彭格蘭<br />
<a href="../Page/:en:Greg_Pratt.md" title="wikilink">Greg Pratt</a></p></td>
<td><p><a href="../Page/林保全.md" title="wikilink">林保全</a></p></td>
<td></td>
<td><p>colspan="1" style="background:#ADD8E6" text-align:center;"|<strong>客串</strong></p></td>
</tr>
<tr class="odd">
<td><p>帕敏德·納格拉<br />
<a href="../Page/:en:Parminder_Nagra.md" title="wikilink">Parminder Nagra</a></p></td>
<td><p>榮蓮娜<br />
<a href="../Page/:en:Neela_Rasgotra.md" title="wikilink">Neela Rasgotra</a></p></td>
<td><p><a href="../Page/陳凱婷.md" title="wikilink">陳凱婷</a></p></td>
<td></td>
<td><p><strong>主演</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/琳達·卡德里尼.md" title="wikilink">琳達·卡德里尼</a><br />
<a href="../Page/:en:Linda_Cardellini.md" title="wikilink">Linda Cardellini</a></p></td>
<td><p>鄧心美<br />
<a href="../Page/:en:Sam_Taggart.md" title="wikilink">Samantha Taggart</a></p></td>
<td><p><a href="../Page/劉惠雲.md" title="wikilink">劉惠雲</a>(S.10)<br />
<a href="../Page/曾佩儀.md" title="wikilink">曾佩儀</a>(S.11-15)</p></td>
<td></td>
<td><p><strong>主演</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/史考特·格瑞恩斯.md" title="wikilink">史考特·格瑞恩斯</a><br />
<a href="../Page/:en:Scott_Grimes.md" title="wikilink">Scott Grimes</a></p></td>
<td><p>莫雅志<br />
<a href="../Page/:en:Archie_Morris.md" title="wikilink">Archie Morris</a></p></td>
<td><p><a href="../Page/潘文柏.md" title="wikilink">潘文柏</a></p></td>
<td></td>
<td><p>colspan="2" style="background:#ADD8E6" text-align:center;"|<strong>客串</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夏恩·威斯特.md" title="wikilink">夏恩·威斯特</a><br />
<a href="../Page/:en:Shane_West.md" title="wikilink">Shane West</a></p></td>
<td><p>白雷<br />
Ray Barnett</p></td>
<td><p><a href="../Page/鄧肇基.md" title="wikilink">鄧肇基</a>(S.11)<br />
<a href="../Page/陳卓智.md" title="wikilink">陳卓智</a>(S.12-13)</p></td>
<td></td>
<td><p><strong>主演</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/約翰·斯塔莫斯.md" title="wikilink">約翰·斯塔莫斯</a><br />
<a href="../Page/:en:John_Stamos.md" title="wikilink">John Stamos</a></p></td>
<td><p>紀東尼<br />
<a href="../Page/:en:Tony_Gates.md" title="wikilink">Tony Gates</a></p></td>
<td><p><a href="../Page/黃啟昌.md" title="wikilink">黃啟昌</a></p></td>
<td></td>
<td><p>colspan="1" style="background:#ADD8E6" text-align:center;"|<strong>客串</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大衛·萊昂斯.md" title="wikilink">大衛·萊昂斯</a><br />
<a href="../Page/:en:David_Lyons_(actor).md" title="wikilink">David Lyons</a></p></td>
<td><p>寶世民<br />
<a href="../Page/:en:Simon_Brenner.md" title="wikilink">Simon Brenner</a></p></td>
<td><p><a href="../Page/陳廷軒.md" title="wikilink">陳廷軒</a></p></td>
<td></td>
<td><p>colspan="1" style="background:#ADD8E6" text-align:center;"|<strong>客串</strong></p></td>
</tr>
<tr class="odd">
<td><p>安祖娜·巴塞特<br />
<a href="../Page/:en:Angela_Bassett.md" title="wikilink">Angela Bassett</a></p></td>
<td><p>白<br />
<a href="../Page/:en:Catherine_Banfield.md" title="wikilink">Catherine Banfield</a></p></td>
<td><p><a href="../Page/許盈.md" title="wikilink">許盈</a></p></td>
<td></td>
<td><p><strong>主演</strong></p></td>
</tr>
</tbody>
</table>

此統計包含演員以主角、客串或無名身份演出的集數。**以下附註列舉了演員脫離固定班底後再次以主角身份演出的情況。**
[安東尼·愛德華於第](../Page/安東尼·愛德華.md "wikilink")179集（第8季第22集，當季最後一集）以主角身份演出，但他其實並沒有在該集中出現，而飾演的葛馬克亦已於前一集因[腦癌逝世](../Page/腦癌.md "wikilink")。另外，他也在第316集（第15季第7集）以主角身份演出，這集是葛馬克的回顧集，他以多次回憶片段的方式出現其中\[8\]。
[佐治·古尼於第](../Page/佐治·古尼.md "wikilink")328集（第15季第19集）以主角身份演出，他飾演的羅道格當時是一位末期器官捐贈者的主診醫生。
\-{zh-hans:雪莉·斯蒂琳费尔德;zh-hk:雪莉·史特靈費特;zh-tw:雪莉·斯蒂琳費爾德;}-於第331集（第15季第22集，全系列最後一集）以主角身份演出，她飾演的陸蘇珊出席了約書亞·卡特中心（卡約翰資助成立的醫療中心）的開幕儀式。
[諾亞·懷利於第](../Page/諾亞·懷利.md "wikilink")325集（第15季第16集）起以主角身份演出五集，他此前亦於第12季的時候以客串身份演出過四集\[9\]。
\-{zh-hans:艾瑞克·拉·塞里;zh-hk:艾力·拉沙爾;zh-tw:艾瑞克·拉·塞里;}-於第172集（第8季第15集）以主角身份演出，他飾演的班彼特短暫地與[阿莉克丝·金斯顿所飾演的康依麗一同出現](../Page/阿莉克丝·金斯顿.md "wikilink")；第178集（第8季第21集），班彼特出席了葛馬克的葬禮；第328集（第15季第19集），班彼特協助卡約翰的[腎臟](../Page/腎臟.md "wikilink")[移植手術](../Page/器官移植.md "wikilink")；第331集，班彼特出席了約書亞·卡特中心的開幕儀式。
[茱莉安娜·瑪格里斯於第](../Page/茱莉安娜·瑪格里斯.md "wikilink")328集（第15季第19集）以主角身份演出，她飾演的夏卡洛協調一位器官捐贈者的器官移植。
羅拉·伊妮斯和[阿莉克丝·金斯顿均於第](../Page/阿莉克丝·金斯顿.md "wikilink")331集（第15季第22集，全系列最後一集）以主角身份演出，她們的角色出席了約書亞·卡特中心的開幕儀式。
高蘭·維斯耶克於第295集（第14季第5集）起以主角身份演出八集。
\-{zh-hans:迈克尔·米谢勒;zh-hk:米高·米雪;zh-tw:麥可·米雪兒;}-於第178集（第8季第21集）以主角身份演出，她的角色費嘉露出席了葛馬克的葬禮。

## 收視率

<table>
<thead>
<tr class="header">
<th><p>季度</p></th>
<th><p>首播</p></th>
<th><p>結束</p></th>
<th><p>年度</p></th>
<th><p>排名</p></th>
<th><p>觀眾<br />
(百萬)</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>1994年9月19日</p></td>
<td><p>1995年5月18日</p></td>
<td><p>1994-1995</p></td>
<td><p><strong>#2</strong>[10]</p></td>
<td><p><strong>19.08</strong>[11]</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>1995年9月21日</p></td>
<td><p>1996年5月16日</p></td>
<td><p>1995-1996</p></td>
<td><p><strong>#1</strong>[12]</p></td>
<td><p><strong>21.09</strong>[13]</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>1996年9月26日</p></td>
<td><p>1997年5月15日</p></td>
<td><p>1996-1997</p></td>
<td><p><strong>#1</strong>[14]</p></td>
<td><p><strong>20.56</strong>[15]</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>1997年9月25日</p></td>
<td><p>1998年5月14日</p></td>
<td><p>1997-1998</p></td>
<td><p><strong>#2</strong>[16]</p></td>
<td><p><strong>19.99</strong>[17]</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>1998年9月24日</p></td>
<td><p>1999年5月20日</p></td>
<td><p>1998-1999</p></td>
<td><p><strong>#1</strong>[18]</p></td>
<td><p><strong>17.94</strong>[19]</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>1999年9月30日</p></td>
<td><p>2000年5月18日</p></td>
<td><p>1999-2000</p></td>
<td><p><strong>#4</strong>[20]</p></td>
<td><p><strong>25.0</strong>[21]</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>2000年10月12日</p></td>
<td><p>2001年5月17日</p></td>
<td><p>2000-2001</p></td>
<td><p><strong>#2</strong>[22]</p></td>
<td><p><strong>22.4</strong>[23]</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>2001年9月27日</p></td>
<td><p>2002年5月16日</p></td>
<td><p>2001-2002</p></td>
<td><p><strong>#3</strong>[24]</p></td>
<td><p><strong>22.1</strong>[25]</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>2002年9月26日</p></td>
<td><p>2003年5月15日</p></td>
<td><p>2002-2003</p></td>
<td><p><strong>#4</strong>[26]</p></td>
<td><p><strong>20.0</strong>[27]</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>2003年9月25日</p></td>
<td><p>2004年5月13日</p></td>
<td><p>2003-2004</p></td>
<td><p><strong>#8</strong>[28]</p></td>
<td><p><strong>19.5</strong>[29]</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>2004年9月23日</p></td>
<td><p>2005年5月19日</p></td>
<td><p>2004-2005</p></td>
<td><p><strong>#16</strong>[30]</p></td>
<td><p><strong>15.5</strong>[31]</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>2005年9月22日</p></td>
<td><p>2006年5月18日</p></td>
<td><p>2005-2006</p></td>
<td><p><strong>#30</strong>[32]</p></td>
<td><p><strong>12.3</strong>[33]</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>2006年9月21日</p></td>
<td><p>2007年5月17日</p></td>
<td><p>2006-2007</p></td>
<td><p><strong>#31</strong>[34]</p></td>
<td><p><strong>11.5</strong>[35]</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p>2007年9月27日</p></td>
<td><p>2008年5月15日</p></td>
<td><p>2007-2008</p></td>
<td><p><strong>#55</strong>[36]</p></td>
<td><p><strong>9.16</strong>[37]</p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>2008年9月25日</p></td>
<td><p>2009年4月2日</p></td>
<td><p>2008-2009</p></td>
<td><p><strong>#36</strong>[38]</p></td>
<td><p><strong>10.3</strong>[39]</p></td>
</tr>
</tbody>
</table>

## 香港播映情況

  -
    第1季:1995年
    第2-7季:有待確認
    第8季:2002年5月14日
    第9季:2003年4月1日
    第10季:2004年4月8日
    第11季:2005年5月11日
    第12季:2006年8月31日
    第13季:2007年10月27日
    第14季:2009年2月2日
    第15季:2009年8月24日

## 參考資料

## 外部連結

  - [華納兄弟官方網站](http://www2.warnerbros.com/ertv/home.html)

  - [NBC官方網站](http://www.nbc.com/ER/)

  -
## 節目變遷

[Category:1990年代美國電視劇](../Category/1990年代美國電視劇.md "wikilink")
[Category:1994年開播的美國電視影集](../Category/1994年開播的美國電視影集.md "wikilink")
[Category:2000年代美國電視劇](../Category/2000年代美國電視劇.md "wikilink")
[Category:2009年停播的美國電視影集](../Category/2009年停播的美國電視影集.md "wikilink")
[Category:美國劇情電視劇](../Category/美國劇情電視劇.md "wikilink")
[Category:美國醫學電視劇](../Category/美國醫學電視劇.md "wikilink")
[Category:英語電視劇](../Category/英語電視劇.md "wikilink")
[Category:皮博迪獎得主](../Category/皮博迪獎得主.md "wikilink")
[Category:黃金時段艾美獎最佳戲劇類影集](../Category/黃金時段艾美獎最佳戲劇類影集.md "wikilink")
[Category:芝加哥背景電視節目](../Category/芝加哥背景電視節目.md "wikilink")
[Category:NBC電視節目](../Category/NBC電視節目.md "wikilink")
[Category:华纳兄弟电视公司制作的电视节目](../Category/华纳兄弟电视公司制作的电视节目.md "wikilink")
[Category:無綫電視外購劇集](../Category/無綫電視外購劇集.md "wikilink")
[Category:中視外購電視劇](../Category/中視外購電視劇.md "wikilink")
[Category:美國演員工會獎電視戲劇類最佳集體演出](../Category/美國演員工會獎電視戲劇類最佳集體演出.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.  <http://www.nbcumv.com/entertainment/release_detail.nbc/entertainment-20080904000000-nbcannouncesgolden.html>

9.  <http://www.nbcumv.com/entertainment/storylines.nbc/er.html>

10. [''](http://www.classictvhits.com/tvratings/1995.htm)  (from May
    2003). Retrieved October 23, 2006.

11.
12.
13.
14.
15.
16.
17.
18.
19.
20. A Dramatic Achievement (Variety Magazine) – Maynard, Kevin: [*While
    cast revolves, auds stay
    involved*](http://www.erheadquarters.com/media/gallery/200th_variety/variety200_article4.htm)
     (from May 2003). Retrieved October 23, 2006.

21. Quotenmeter.de:
    [*US-Jahrescharts 1999/2000*](http://www.quotenmeter.de/index.php?newsid=9946).
    Retrieved October 23, 2006.

22.

23.
24.

25.
26.

27.
28.

29.
30.

31.
32.

33.
34.

35.
36.

37.
38.

39.