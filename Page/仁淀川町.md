**仁淀川町**（）是位於[高知縣中部山區的一城鎮](../Page/高知縣.md "wikilink")。[仁淀川從轄區內由西向東流過](../Page/仁淀川.md "wikilink")。

## 历史

### 沿革

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：
      - [吾川郡](../Page/吾川郡.md "wikilink")：富岡村、池川村、大崎村、名野川村。
      - [高岡郡](../Page/高岡郡.md "wikilink")：別府村、長者村。
  - 1913年4月1日：池川村改制為[池川町](../Page/池川町.md "wikilink")。
  - 1941年7月1日：池川町和富岡村[合併為新設置的池川町](../Page/市町村合併.md "wikilink")。
  - 1943年5月1日：大崎村的部份地區被併入池川町。
  - 1954年7月20日：長者村的部份地區被併入[越知町](../Page/越知町.md "wikilink")。
  - 1954年9月1日：別府村和長者村合併為[仁淀村](../Page/仁淀村.md "wikilink")。
  - 1955年2月1日：大崎村和名野川村合併為[吾川村](../Page/吾川村.md "wikilink")。
  - 2005年8月1日：池川町、吾川村、仁淀村合併為**仁淀川町**。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>富岡村</p></td>
<td><p>1941年7月1日<br />
池川町</p></td>
<td><p>池川町</p></td>
<td><p>2005年8月1日<br />
仁淀川町</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>池川村</p></td>
<td><p>1913年4月1日<br />
池川町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大崎村</p></td>
<td><p>1943年5月1日<br />
併入池川町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>大崎村</p></td>
<td><p>1955年2月1日<br />
吾川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>名野川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>別府村</p></td>
<td><p>1954年9月1日<br />
仁淀村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>長者村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1954年7月20日<br />
併入越知町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 觀光資源

  - 中津溪谷
  - 岩屋川溪谷
  - 安居溪谷
  - 鳥形山森林植物公園
  - 秋葉祭

## 學校

  - 高等學校

<!-- end list -->

  - [高知縣立仁淀高等學校](../Page/高知縣立仁淀高等學校.md "wikilink")

## 本地出身之名人

  - [金子直吉](../Page/金子直吉.md "wikilink")：[企業家](../Page/企業家.md "wikilink")
  - [吉田類](../Page/吉田類.md "wikilink")：酒場詩人

## 外部連結

  - [仁淀川漁業協同組合](http://www.niyodogawa.or.jp/)