**今治市**（）是位于[愛媛縣東北部](../Page/愛媛縣.md "wikilink")[高繩半島的城市](../Page/高繩半島.md "wikilink")，臨[瀨戶內海](../Page/瀨戶內海.md "wikilink")，其人口在愛媛縣內僅次于[松山市位居第二](../Page/松山市.md "wikilink")，在全四國地區中為第五大城，僅次於四個縣的縣政府所在地。

以漁業以及漁船製造業為主，現也逐步擴及到電機、機械製造領域，其他方面則是運輸業和零售業為主的服務業，[太陽石油和住友金屬算是少數的重工業](../Page/太陽石油.md "wikilink")，傳統特產的伯方鹽業和櫻井漆器則依附於觀光業而小規模生存。

## 歷史

[View_of_Imabari.jpg](https://zh.wikipedia.org/wiki/File:View_of_Imabari.jpg "fig:View_of_Imabari.jpg")\]\]
過去曾是[今治城的城下町](../Page/今治城.md "wikilink")，古代是海上的交通要道，也是[伊予國](../Page/伊予國.md "wikilink")[國府所在地](../Page/國府.md "wikilink")。

[江戶時代屬於](../Page/江戶時代.md "wikilink")[今治藩的領地](../Page/今治藩.md "wikilink")，在[廢藩置縣後一度成為](../Page/廢藩置縣.md "wikilink")[今治縣](../Page/今治縣.md "wikilink")，隨後與[松山縣](../Page/松山縣.md "wikilink")、[西条縣](../Page/西条縣.md "wikilink")、[小松縣合併為新設置的松山縣](../Page/小松縣.md "wikilink")（後又改名為[石鐵縣](../Page/石鐵縣.md "wikilink")），並成為縣政府所在地。直到又與[神山縣合併為](../Page/神山縣.md "wikilink")[愛媛縣後](../Page/愛媛縣.md "wikilink")，才失去了縣政府所在地的地位。

### 年表

  - 1889年12月15日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：
      - [越智郡](../Page/越智郡.md "wikilink")：**今治町**、日吉村、近見村、立花村、櫻井村、富田村、清水村、日高村、東伯方村、西伯方村、宮窪村、鈍川村、九和村、鴨部村、龍岡村、津倉村、龜山村、大山村、上朝倉村、下朝倉村、瀨戶崎村、盛口村、鏡村、宮浦村、岡山村、關前村。
      - [野間郡](../Page/野間郡.md "wikilink")：波止濱村、菊間村、歌仙村、龜岡村、波方村、小西村、大井村、乃萬村。
  - 1897年4月1日：野間郡和越智郡合併為新設立的越智郡。
  - 1899年7月1日：渦浦村從龜山村分村。
  - 1908年1月1日：
      - 波止濱村改制為[波止濱町](../Page/波止濱町.md "wikilink")。
      - 菊間村改制為[菊間町](../Page/菊間町.md "wikilink")。
  - 1917年10月1日：櫻井村改制為[櫻井町](../Page/櫻井町_\(愛媛縣\).md "wikilink")。
  - 1920年2月11日：今治町和日吉村[合併為](../Page/市町村合併.md "wikilink")**今治市**。\[1\]
  - 1925年4月1日：歌仙村被併入菊間町。
  - 1933年2月11日：近見村被併入今治市。
  - 1940年1月1日：立花村被併入今治市。
  - 1940年11月1日：東伯方村改制並改名為[伯方町](../Page/伯方町.md "wikilink")。
  - 1952年8月1日：宮窪村改制為[宮窪町](../Page/宮窪町.md "wikilink")。
  - 1954年3月31日：
      - 鈍川村、九和村、鴨部村、龍岡村合併為玉川村。
      - 津倉村、龜山村、渦浦村和大山村的部份地區（福田、泊、田浦地區）合併為[吉海町](../Page/吉海町.md "wikilink")。
      - 大山村的其餘地區（早川、余所國地區）被併入宮窪町。
  - 1955年1月1日：伯方町和西伯方村合併為新設置的[伯方町](../Page/伯方町.md "wikilink")。
  - 1955年2月1日：櫻井町、富田村、清水村、日高村、乃萬村、波止濱町被併入今治市。
  - 1955年3月31日：
      - 大井村和小西村合併為[大西町](../Page/大西町.md "wikilink")。
      - 瀨戶崎村和盛口村合併為上浦村。
      - 鏡村和宮浦村合併為[大三島町](../Page/大三島町.md "wikilink")。
      - 龜岡村和菊間町合併為新設置的菊間町。
  - 1955年8月1日：吉海町的馬島地區被併入今治市。
  - 1956年3月31日：上朝倉村和下朝倉村合併維[朝倉村](../Page/朝倉村_\(愛媛縣\).md "wikilink")。
  - 1956年9月23日：岡山村被併入大三島町。
  - 1960年3月1日：波方村改制為[波方町](../Page/波方町.md "wikilink")。
  - 1960年5月1日：波方町的部份地區被併入今治市。
  - 1962年4月1日：玉川村改制為[玉川町](../Page/玉川町_\(愛媛縣\).md "wikilink")。
  - 1964年4月1日：上浦村改制為[上浦町](../Page/上浦町_\(愛媛縣\).md "wikilink")。
  - 2005年1月16日：菊間町、大西町、波方町、玉川町、朝倉村、吉海町、宮窪町、伯方町、大三島町、上浦町、關前村和今治市合併為新設置的**今治市**。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>菊間村</p></td>
<td><p>1908年1月1日<br />
菊間町</p></td>
<td><p>菊間町</p></td>
<td><p>1955年3月31日<br />
合併為菊間町</p></td>
<td><p>2005年1月16日<br />
合併為今治市</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>歌仙村</p></td>
<td><p>1925年4月1日<br />
併入菊間町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>龜岡村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>波方村</p></td>
<td><p>1960年3月1日<br />
波方町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>小西村</p></td>
<td><p>1955年3月31日<br />
合併為大西町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>大井村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>波止濱村</p></td>
<td><p>1908年1月1日<br />
波止濱町</p></td>
<td><p>1955年2月1日<br />
併入今治市</p></td>
<td><p>今治市</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>乃萬村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>今治町</p></td>
<td><p>1920年2月11日<br />
今治市</p></td>
<td><p>今治市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>日吉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>近見村</p></td>
<td><p>1933年2月11日<br />
併入今治市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>立花村</p></td>
<td><p>1940年1月1日<br />
併入今治市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>櫻井村</p></td>
<td><p>1917年10月1日<br />
櫻井町</p></td>
<td><p>1955年2月1日<br />
併入今治市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>富田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>清水村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>日高村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>東伯方村</p></td>
<td><p>1940年11月1日<br />
改制並改名為伯方町</p></td>
<td><p>1955年1月1日<br />
合併為伯方町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>西伯方村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>鈍川村</p></td>
<td><p>1954年3月31日<br />
合併為玉川村</p></td>
<td><p>1962年4月1日<br />
玉川町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>九和村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>鴨部村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>龍岡村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>津倉村</p></td>
<td><p><br />
<br />
1954年3月31日<br />
合併為吉海町<br />
<br />
<br />
</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>龜山村</p></td>
<td><p>龜山村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1899年7月1日<br />
渦浦村分村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>大山村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1954年3月31日<br />
併入宮窪町</p></td>
<td><p>宮窪町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>宮窪村</p></td>
<td><p>1952年8月1日<br />
宮窪町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>上朝倉村</p></td>
<td><p>1956年3月31日<br />
合併為朝倉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>下朝倉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>瀨戶崎村</p></td>
<td><p>1955年3月31日<br />
合併為上浦村</p></td>
<td><p>1964年4月1日<br />
上浦町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>盛口村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>鏡村</p></td>
<td><p>1955年3月31日<br />
合併為大三島町</p></td>
<td><p>大三島町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>宮浦村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>岡山村</p></td>
<td><p>1956年9月23日<br />
併入大三島町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>關前村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 行政

### 歷任市長

1.  森秀雄（1946年-1948年）
2.  山本幸助（1948年-1952年）
3.  村瀨武男（1952年-1953年）
4.  田坂敬三郎（1954年-1962年）
5.  羽藤栄一（1962年-1982年）
6.  岡島一夫（1982年-1998年）
7.  繁信順一（1998年-2005年）

<!-- end list -->

  - 平成合併後

<!-- end list -->

1.  越智忍 （2005年-2009年2月20日）
2.  菅良二 （2009年2月20日-憲任）

## 交通

### 鐵路

  - [四國旅客鐵道](../Page/四國旅客鐵道.md "wikilink")
      - [予讚線](../Page/予讚線.md "wikilink")：[菊間站](../Page/菊間站.md "wikilink")
        - [伊予龜岡站](../Page/伊予龜岡站.md "wikilink") -
        [大西站](../Page/大西站.md "wikilink") -
        [波方站](../Page/波方站.md "wikilink") -
        [波止濱站](../Page/波止濱站.md "wikilink") -
        [今治站](../Page/今治站.md "wikilink") -
        [伊予富田站](../Page/伊予富田站.md "wikilink") -
        [伊予櫻井站](../Page/伊予櫻井站.md "wikilink")

|                                                                                           |                                                                                                                                                                                    |
| ----------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Onishi_stn.jpg](https://zh.wikipedia.org/wiki/File:Onishi_stn.jpg "fig:Onishi_stn.jpg") | [Shikoku_Railway_-_Imabari_Station_-_01.JPG](https://zh.wikipedia.org/wiki/File:Shikoku_Railway_-_Imabari_Station_-_01.JPG "fig:Shikoku_Railway_-_Imabari_Station_-_01.JPG") |

### 港口

[Munakata_Port.jpg](https://zh.wikipedia.org/wiki/File:Munakata_Port.jpg "fig:Munakata_Port.jpg")
轄區內包含許多離島，大多數島嶼皆設有港口及定期渡輪，但在[西瀨戶自動車道通車後](../Page/西瀨戶自動車道.md "wikilink")，許多島嶼可藉由公路通往本土，因此部份渡輪的航班開始減少甚至停駛。

  - 主要港口

<!-- end list -->

  - [今治港](../Page/今治港.md "wikilink")
  - [波方港](../Page/波方港.md "wikilink")
  - [波止濱港](../Page/波止濱港.md "wikilink")
  - [伯方港](../Page/伯方港.md "wikilink")
  - [菊間港](../Page/菊間港.md "wikilink")
  - [宮浦港](../Page/宮浦港_\(今治市\).md "wikilink")
  - [吉海港](../Page/吉海港.md "wikilink")
  - [友浦港](../Page/友浦港.md "wikilink")
  - [津島港](../Page/津島港.md "wikilink")
  - [岡村港](../Page/岡村港.md "wikilink")

### 道路

[Kurushimakaikyou_ohashi.jpg](https://zh.wikipedia.org/wiki/File:Kurushimakaikyou_ohashi.jpg "fig:Kurushimakaikyou_ohashi.jpg")

  - 高速道路

<!-- end list -->

  - [西瀨戶自動車道](../Page/西瀨戶自動車道.md "wikilink")：[大三島交流道](../Page/大三島交流道.md "wikilink")
    - [上浦休息區](../Page/上浦休息區.md "wikilink") -
    [伯方島交流道](../Page/伯方島交流道.md "wikilink") -
    [大島北交流道](../Page/大島北交流道.md "wikilink") -
    [大島南交流道](../Page/大島南交流道.md "wikilink") -
    [來島海峽服務區](../Page/來島海峽服務區.md "wikilink") -
    [今治北交流道](../Page/今治北交流道.md "wikilink") -
    [今治交流道](../Page/今治交流道.md "wikilink")
  - [今治小松自動車道](../Page/今治小松自動車道.md "wikilink")：[今治湯之浦交流道](../Page/今治湯之浦交流道.md "wikilink")

## 觀光資源

[Imabari_casrtle5.JPG](https://zh.wikipedia.org/wiki/File:Imabari_casrtle5.JPG "fig:Imabari_casrtle5.JPG")
[Ooyamazumi-jinja_Haiden.JPG](https://zh.wikipedia.org/wiki/File:Ooyamazumi-jinja_Haiden.JPG "fig:Ooyamazumi-jinja_Haiden.JPG")

  - [今治城](../Page/今治城.md "wikilink")
  - 愛媛文華館
  - 伊予櫻井漆器會館
  - 村上水軍博物館
  - 櫻井石溫泉
  - 鈍川温泉
  - 來島海峡展望館
  - 喜多浦八幡神社
  - 藝予要塞遺跡
  - [大山祇神社](../Page/大山祇神社.md "wikilink")
  - [龜老山展望公園](../Page/龜老山.md "wikilink")
  - [毛巾美術館一廣](../Page/毛巾美術館一廣.md "wikilink")

## 教育

  - 大學

<!-- end list -->

  - [今治明德短期大學](../Page/今治明德短期大學.md "wikilink")

<!-- end list -->

  - 高等學校

<!-- end list -->

  - [愛媛縣立今治西高等學校](../Page/愛媛縣立今治西高等學校.md "wikilink")
  - [愛媛縣立今治北高等學校](../Page/愛媛縣立今治北高等學校.md "wikilink")
      - 大三島分校
  - [愛媛縣立今治南高等學校](../Page/愛媛縣立今治南高等學校.md "wikilink")
  - [愛媛縣立今治工業高等學校](../Page/愛媛縣立今治工業高等學校.md "wikilink")
  - [愛媛縣立伯方高等學校](../Page/愛媛縣立伯方高等學校.md "wikilink")
  - [今治明徳高等學校](../Page/今治明徳高等學校.md "wikilink")
      - 矢田分校
  - [今治精華高等學校](../Page/今治精華高等學校.md "wikilink")
  - [日本ウェルネス高等學校](../Page/日本ウェルネス高等學校.md "wikilink")

<!-- end list -->

  - [愛媛縣立今治西高等學校](../Page/愛媛縣立今治西高等學校.md "wikilink")
  - [愛媛縣立今治工業高等學校](../Page/愛媛縣立今治工業高等學校.md "wikilink")
  - [日本健康高等學校](../Page/日本健康高等學校.md "wikilink")
  - [今治精華高等學校](../Page/今治精華高等學校.md "wikilink")

### 專修學校

  - [今治看護專門學校](../Page/今治看護專門學校.md "wikilink")
  - [今治商業專門學校](../Page/今治商業專門學校.md "wikilink")

### 特殊學校

  - [愛媛縣立今治養護學校](../Page/愛媛縣立今治養護學校.md "wikilink")

## 姊妹、友好城市

### 日本

  - [太田市](../Page/太田市.md "wikilink")（[群馬縣](../Page/群馬縣.md "wikilink")）
  - [尾道市](../Page/尾道市.md "wikilink")（[廣島縣](../Page/廣島縣.md "wikilink")）

### 海外

  - [巴拿馬城](../Page/巴拿馬城.md "wikilink")（[巴拿馬](../Page/巴拿馬.md "wikilink")[巴拿馬省](../Page/巴拿馬省.md "wikilink")）

  - [莱克兰](../Page/莱克兰_\(佛罗里达州\).md "wikilink")（[美國](../Page/美國.md "wikilink")[佛羅里達州](../Page/佛羅里達州.md "wikilink")[波爾克縣](../Page/波爾克縣_\(佛羅里達州\).md "wikilink")）

## 本地出身之名人

### 政治

  - [越智伊平](../Page/越智伊平.md "wikilink")：前[眾議院](../Page/眾議院.md "wikilink")[議員](../Page/議員.md "wikilink")，曾任[建設大臣](../Page/建設大臣.md "wikilink")、[運輸大臣](../Page/運輸大臣.md "wikilink")、[農林水產大臣](../Page/農林水產大臣.md "wikilink")
  - [菊川忠雄](../Page/菊川忠雄.md "wikilink")：前眾議院議員
  - [白石洋一](../Page/白石洋一.md "wikilink")：眾議院議員
  - [砂田重政](../Page/砂田重政.md "wikilink")：前眾議院議員，曾任[防衛廳長官](../Page/防衛廳長官.md "wikilink")
  - [野間赳](../Page/野間赳.md "wikilink")：前參議院議員
  - [村上誠一郎](../Page/村上誠一郎.md "wikilink")：眾議院議員，曾任內閣特命擔當大臣
  - [村上紋四郎](../Page/村上紋四郎.md "wikilink")：前眾議院議員、前今治市長
  - [山本順三](../Page/山本順三.md "wikilink")：參議院議員
  - [藤原武平太](../Page/藤原武平太.md "wikilink")：[情報処理推進機構理事長](../Page/情報処理推進機構.md "wikilink")、曾任日本駐美國大使

### 體育

  - [伊藤豐明](../Page/伊藤豐明.md "wikilink")：前自行車競速選手
  - [越智大祐](../Page/越智大祐.md "wikilink")：職業棒球選手
  - [越智亮介](../Page/越智亮介.md "wikilink")：職業足球選手
  - [菅和範](../Page/菅和範.md "wikilink")：職業足球選手
  - [重松通雄](../Page/重松通雄.md "wikilink")：前職業棒球選手
  - [高井保弘](../Page/高井保弘.md "wikilink")：前職業棒球選手
  - [藤本修二](../Page/藤本修二.md "wikilink")：前職業棒球選手
  - [真木和](../Page/真木和.md "wikilink")：田徑選手

### 演藝

  - [青野敏行](../Page/青野敏行.md "wikilink")：搞笑藝人
  - [伊吹友里](../Page/伊吹友里.md "wikilink")：演歌歌手
  - [近藤等則](../Page/近藤等則.md "wikilink")：爵士演奏者
  - [扇崎秀薗](../Page/扇崎秀薗.md "wikilink")：日本舞踊家
  - [越智志帆](../Page/越智志帆.md "wikilink")：流行歌手，藝名[Superfly](../Page/Superfly.md "wikilink")
  - [谷本久美子](../Page/谷本久美子.md "wikilink")：爵士歌手

### 學術

  - [越智道雄](../Page/越智道雄.md "wikilink")：英語文學學者
  - [矢内原忠雄](../Page/矢内原忠雄.md "wikilink")：經濟學家、[東京大學校長](../Page/東京大學.md "wikilink")
  - [渡辺弘之](../Page/渡辺弘之.md "wikilink")：生物學家

### 藝文

  - [智内兄助](../Page/智内兄助.md "wikilink")：畫家
  - [MAYA MAXX](../Page/MAYA_MAXX.md "wikilink")：畫家
  - [村上三島](../Page/村上三島.md "wikilink")：[書法家](../Page/書法家.md "wikilink")
  - [森一生](../Page/森一生.md "wikilink")：電影導演
  - [馬越嘉彦](../Page/馬越嘉彦.md "wikilink")：[動畫師](../Page/動畫師.md "wikilink")、[人物設計](../Page/人物設計.md "wikilink")

### 其他

  - [凝然](../Page/凝然.md "wikilink")：華嚴宗高僧
  - [清水克彦](../Page/清水克彦.md "wikilink")：[新聞主播](../Page/新聞主播.md "wikilink")
  - [丹下健三](../Page/丹下健三.md "wikilink")：建築師
  - [長井健司](../Page/長井健司.md "wikilink")：記者
  - [村上徹](../Page/村上徹.md "wikilink")：建築師

## 參考資料

## 外部連結

  - [今治市觀光情報](http://www.city.imabari.ehime.jp/kanko/)

  - [今治地方觀光協會](http://www.oideya.gr.jp/)

  - [今治物產協會](https://web.archive.org/web/20120127235524/http://www.i-bussan.jp/)

  - [今治旅館組合](http://www.imabari-hotelguide.jp/)

  - [今治市港灣振興協會](http://imabariports.jp/)

  - [今治市及越智郡11町村合併協議會](https://web.archive.org/web/20120415073422/http://www.city.imabari.ehime.jp/gappei/)

[\*](../Category/今治市.md "wikilink")

1.