**介休市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[山西省的一个](../Page/山西省.md "wikilink")[县级市](../Page/县级市.md "wikilink")，由[晋中市代管](../Page/晋中市.md "wikilink")。介休是為紀念[晋文公的臣子](../Page/晋文公.md "wikilink")[介之推亡於境内的绵山](../Page/介之推.md "wikilink")（介山）上而得名。置縣已有二千五百年以上的歷史。

## 历史

[春秋](../Page/春秋时期.md "wikilink")[晋顷公十二年](../Page/晋顷公.md "wikilink")（前514年）置邬县。[秦置界休县](../Page/秦朝.md "wikilink")。新莽改为界美。[东汉复名界休](../Page/东汉.md "wikilink")。[西晋改介休县](../Page/西晋.md "wikilink")，以[春秋时](../Page/春秋时期.md "wikilink")[晋文公的臣子](../Page/晋文公.md "wikilink")[介之推死亡在其境内的绵山](../Page/介之推.md "wikilink")（介山）上而得名。[十六国时期邬县](../Page/十六国.md "wikilink")、介休县废。[北魏](../Page/北魏.md "wikilink")[太和八年](../Page/太和_\(北魏孝文帝\).md "wikilink")（484年）置介休，十九年复置邬县。[北齐天保年间并入永安县](../Page/北齐.md "wikilink")。[北周](../Page/北周.md "wikilink")[宣政元年](../Page/宣政.md "wikilink")（578年）恢复。[大成元年](../Page/大成_\(宇文赟\).md "wikilink")（579年）改名平昌县。隋[开皇十年](../Page/开皇.md "wikilink")（590年）析置[灵石县](../Page/灵石县.md "wikilink")，十八年复名介休县。其后先后隶属[太原郡](../Page/太原郡.md "wikilink")、[西河郡](../Page/西河郡.md "wikilink")、[汾州](../Page/汾州.md "wikilink")、[汾州府等](../Page/汾州府.md "wikilink")。1992年撤县设市。

## 地理

### 区位

介休地处[太原盆地和](../Page/太原盆地.md "wikilink")[太岳山的结合部](../Page/太岳山.md "wikilink")，太原盆地西南端，太岳山北侧，[汾河南畔](../Page/汾河.md "wikilink")。

### 气候

介休属半干旱大陆性气候，四季分明，季风气候显著。冬寒冷，夏季闷热，春秋较短。最冷月（1月）平均气温−4.4℃，最热月（7月）平均气温23.9℃，年平均气温10.6℃，降水量455毫米。

## 行政区划

介休市辖5个[街道](../Page/街道办事处.md "wikilink")、7个[镇](../Page/镇.md "wikilink")、3个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")。

  - 街道：[北关街道](../Page/北关街道_\(介休市\).md "wikilink")、[西关街道](../Page/西关街道_\(介休市\).md "wikilink")、[东南街道](../Page/东南街道_\(介休市\).md "wikilink")、[西南街道](../Page/西南街道_\(介休市\).md "wikilink")、[北坛街道](../Page/北坛街道.md "wikilink")
  - 镇：[义安镇](../Page/义安镇_\(介休市\).md "wikilink")、[张兰镇](../Page/张兰镇.md "wikilink")、[连福镇](../Page/连福镇.md "wikilink")、[洪山镇](../Page/洪山镇_\(介休市\).md "wikilink")、[龙凤镇](../Page/龙凤镇_\(介休市\).md "wikilink")、[绵山镇](../Page/绵山镇.md "wikilink")、[义棠镇](../Page/义棠镇.md "wikilink")
  - 乡：[城关乡](../Page/城关乡_\(介休市\).md "wikilink")、[宋古乡](../Page/宋古乡.md "wikilink")、[三佳乡](../Page/三佳乡.md "wikilink")

## 交通

[Jiexiu_Railway_Station_2013.08.24_07-01-00.jpg](https://zh.wikipedia.org/wiki/File:Jiexiu_Railway_Station_2013.08.24_07-01-00.jpg "fig:Jiexiu_Railway_Station_2013.08.24_07-01-00.jpg")\]\]

  - 铁路：[同蒲铁路](../Page/同蒲铁路.md "wikilink")（[大同](../Page/大同市.md "wikilink")－[风陵渡](../Page/风陵渡.md "wikilink")）、[介西铁路](../Page/介西铁路.md "wikilink")（介休－[阳泉曲镇](../Page/阳泉曲镇.md "wikilink")）[介休站](../Page/介休站.md "wikilink")
  - 公路：、、、

## 经济

农业主产[小麦](../Page/小麦.md "wikilink")、[玉米](../Page/玉米.md "wikilink")、[高粱等](../Page/高粱.md "wikilink")。境内[煤炭储量丰富](../Page/煤炭.md "wikilink")，是中国重要的焦煤产地，有汾西矿业公司等重要企业。依托于煤炭，介休近年来[炼焦产业也发展迅速](../Page/炼焦.md "wikilink")，但亦带来不少环境污染。

## 物产

  - 矿产有煤炭、铝钒土、石膏、粘土、石英矿、硫铁、石灰岩、紫砂、蛭石、紫木节、水晶、硝石、磷矿、石棉等。
  - 野生动物有土豹、林麝、青羊、穿山甲、马雕、狼、狐、獾、山猪等。
  - 中草药材有猪苓、地骨皮、香加皮、荆芩、山桃仁、酸枣仁、麻黄、黄芩、板蓝根、紫苏、椿根皮、绵贝、元胡、大叶三七、荔枝草、野山参等。
  - 土特产主要有亨乐牌黄酒、鸑鷟香、贯馅糖、绵山牌陈醋。

## 风景名胜

  - [全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")：[祆神楼](../Page/祆神楼.md "wikilink")、[介休后土庙](../Page/介休后土庙.md "wikilink")、[洪山窑址](../Page/洪山窑址.md "wikilink")、[张壁古堡](../Page/张壁古堡.md "wikilink")、[回銮寺](../Page/回銮寺.md "wikilink")、[介休东岳庙](../Page/介休东岳庙.md "wikilink")、[太和岩牌楼](../Page/太和岩牌楼.md "wikilink")、[介休五岳庙](../Page/介休五岳庙.md "wikilink")、[介休城隍庙](../Page/介休城隍庙.md "wikilink")、[云峰寺石佛殿](../Page/云峰寺石佛殿.md "wikilink")、[介休源神庙](../Page/介休源神庙.md "wikilink")
  - [山西省文物保护单位](../Page/山西省文物保护单位.md "wikilink")：[郭有道墓](../Page/郭有道墓.md "wikilink")
  - [晋中市文物保护单位](../Page/晋中市文物保护单位.md "wikilink")：[介休文庙](../Page/介休文庙.md "wikilink")、[介休关帝庙](../Page/介休关帝庙.md "wikilink")、[南垣山墓群](../Page/南垣山墓群.md "wikilink")、[曹墓](../Page/曹墓.md "wikilink")
  - [介休市文物保护单位](../Page/介休市文物保护单位.md "wikilink")
  - [绵山](../Page/绵山.md "wikilink")：是山西省風景名勝區，中國[清明節](../Page/清明節.md "wikilink")（[寒食節](../Page/寒食節.md "wikilink")）發源地，中國寒食清明文化研究中心，中國寒食清明文化博物館。區域跨介休、靈石、沁源三市縣地界，最高海拔2560米，是太嶽山的一條支脈。綿山起源于春秋時晉國[介子推攜母隱居被焚在山上](../Page/介子推.md "wikilink")，所以綿山又名介山，綿山早在北魏之時就有寺廟建築，唐初時已具有相當規模的佛教禪林。

## 参考文献

[Category:晋中](../Category/晋中.md "wikilink")
[Category:山西县级市](../Category/山西县级市.md "wikilink")
[晋](../Category/中国小城市.md "wikilink")