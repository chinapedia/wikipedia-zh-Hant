**仙女座
Ⅲ**是位於[仙女座的一個](../Page/仙女座.md "wikilink")[矮橢球星系](../Page/矮橢球星系.md "wikilink")，距離大約244萬[光年](../Page/光年.md "wikilink")，
它是[本星系群的一員](../Page/本星系群.md "wikilink")，也是[M31的](../Page/仙女座星系.md "wikilink")[衛星星系](../Page/衛星星系.md "wikilink")。仙女座
Ⅲ是[Sydney van Der
Bergh](../Page/Sydney_van_Der_Bergh.md "wikilink")\[1\]在1970和1971年的照相乾板上發現的。

## 相關條目

  - [仙女座星系的衛星星系](../Page/仙女座星系的衛星星系.md "wikilink")

## 外部連結

  - [**SEDS**: Dwarf Spheroidal Galaxy Andromeda
    III](https://web.archive.org/web/20060220023953/http://www.seds.org/~spider/spider/LG/and3.html)

## 參考資料

<div class="references-small">

<references/>

</div>

[Category:矮橢球星系](../Category/矮橢球星系.md "wikilink")
[Category:仙女座次集團](../Category/仙女座次集團.md "wikilink")
[Andromeda 03](../Category/仙女座.md "wikilink")

1.