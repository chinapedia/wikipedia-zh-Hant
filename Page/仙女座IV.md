**仙女座
Ⅳ**可能是一個[矮不規則星系](../Page/矮不規則星系.md "wikilink")，有人認為[仙女座大星系的](../Page/仙女座大星系.md "wikilink")[衛星星系](../Page/衛星星系.md "wikilink")，亦有人認為其實離仙女座大星系很遠的。但是它也可能不是個[星系](../Page/星系.md "wikilink")，而只是失落的[星團或只是背景中的影像](../Page/星團.md "wikilink")。

## 歷史

它是被[Sydney van Der
Bergh發現的](../Page/Sydney_van_Der_Bergh.md "wikilink")。\[1\]

## 參考資料

<div class="references-small">

<references/>

</div>

[Category:仙女座次集團](../Category/仙女座次集團.md "wikilink")
[Category:矮不規則星系](../Category/矮不規則星系.md "wikilink")
[Andromeda 04](../Category/仙女座.md "wikilink")

1.