**仙女座
Ⅸ**是在2004年被Zucker等人從[史隆數位巡天的恆星測光儀中解析而發現的星系](../Page/史隆數位巡天.md "wikilink")。它是位於[仙女座的一個](../Page/仙女座.md "wikilink")[矮橢球星系](../Page/矮橢球星系.md "wikilink")，也是[仙女座星系的衛星星系](../Page/仙女座星系.md "wikilink")。\[1\]在發現之際，它是已知的星系中表面光度最低的Σ<sub>*V*</sub>～26.8mags
arcsec<sup>-2</sup>，並且是本質的絕對星等最暗的星系。\[2\]

仙女座
Ⅸ是從史隆數位巡天在2002年10月5日沿著M31的長軸方向掃描得到的數據中檢出的，McConnacrchie等人估計他的距離與M31幾乎完全相同（2005）。

## 外部連結

  - [SEDS webpage for Andromeda
    IX](https://web.archive.org/web/20060220024042/http://www.seds.org/~spider/spider/LG/and9.html)

## 參考資料

<div class="references-small">

<references/>

</div>

[Category:矮橢球星系](../Category/矮橢球星系.md "wikilink")
[Category:仙女座次集團](../Category/仙女座次集團.md "wikilink")
[Andromeda 09](../Category/仙女座.md "wikilink")

1.
2.