**仙女魚目**（[学名](../Page/学名.md "wikilink")：）又名**仙鱼目**、**蜥鱼目**，是**圆鳞派**（；又作**圆鳞总目**）的唯一一[目](../Page/目.md "wikilink")，属于[脊索动物门](../Page/脊索动物门.md "wikilink")[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")。

## 分类

圆鳞派是[新鳍亚纲](../Page/新鳍亚纲.md "wikilink")[真骨下纲之下的一个演化支](../Page/真骨下纲.md "wikilink")，是[栉鳞派的](../Page/栉鳞派.md "wikilink")[姐妹群](../Page/姐妹群.md "wikilink")，与其它[新真骨鱼亚群的演化关系如下](../Page/新真骨鱼亚群.md "wikilink")：

### 内部分类

本目可分成4個[亞目](../Page/亞目.md "wikilink")：

  - [合齒魚亞目](../Page/合齒魚亞目.md "wikilink")（Synodontoidei）
  - [青眼魚亞目](../Page/青眼魚亞目.md "wikilink")（Chlorophthalmoidei）
  - [帆蜥魚亞目](../Page/帆蜥魚亞目.md "wikilink")（Alepisauroidei）
  - [巨尾魚亞目](../Page/巨尾魚亞目.md "wikilink")（Giganturoidei）

## 参考文献

[\*](../Category/仙女魚目.md "wikilink")