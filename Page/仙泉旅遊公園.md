[SuoiTien1.JPG](https://zh.wikipedia.org/wiki/File:SuoiTien1.JPG "fig:SuoiTien1.JPG")
**仙泉旅遊公園**（）是一個位於[越南](../Page/越南.md "wikilink")[胡志明市](../Page/胡志明市.md "wikilink")[第九郡的](../Page/胡志明市第九郡.md "wikilink")[遊樂場](../Page/遊樂場.md "wikilink")。該公園分為幾個不同的區域，而它的地形、景點則和越南的歷史傳說有關，例如[嫗姬和](../Page/嫗姬.md "wikilink")[貉龍君的事跡](../Page/貉龍君.md "wikilink")，以及山精和水精的交戰。公園內設有一個人工海水泳池供泳客使用，另外公園內還有一個[恐龍花園和一個人造](../Page/恐龍.md "wikilink")[海灘](../Page/海灘.md "wikilink")。海灘的後面設有一座大型[瀑布](../Page/瀑布.md "wikilink")，瀑布旁邊的石壁上鐫刻了貉龍君的肖像。

仙泉公園不但設有傳統的主題公園設施、景點，還設有一座鬱鬱蔥蔥的[花園和一座](../Page/花園.md "wikilink")[動物園](../Page/動物園.md "wikilink")，並佈置了不同的裝飾物，例如一些塗上藍色或橙色[粉彩的巨](../Page/粉彩.md "wikilink")[龍雕刻和軟身的紅色](../Page/龍.md "wikilink")[佛像](../Page/佛像.md "wikilink")。

正在興建的[胡志明市地鐵](../Page/胡志明市地鐵.md "wikilink")1號線其中一個車站位於仙泉旅遊公園附近，將於2018年投入服務\[1\]
。

## 圖集

[File:SuoiTien3.JPG|仙泉公園的人造泳池和](File:SuoiTien3.JPG%7C仙泉公園的人造泳池和)[貉龍君像](../Page/貉龍君.md "wikilink")。
<File:Tượng> Hai Bà Trưng ở Suối
Tiên.JPG|[二徵夫人像](../Page/二徵夫人.md "wikilink")
|仙泉公園的過山車 Kim Lân Sơn Xuất Thế.jpg|獅群和假山

## 参考文献

## 外部連結

  - [仙泉旅遊公園官方網頁](http://www.suoitien.com)

[Category:胡志明市](../Category/胡志明市.md "wikilink")
[Category:越南主题公园](../Category/越南主题公园.md "wikilink")

1.