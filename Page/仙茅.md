**仙茅**（[学名](../Page/学名.md "wikilink")：），又名**地棕根**、**地棕**、**独茅根**、**独茅**、**独脚仙茅**、**山党参**、**仙茅参**、**海南参**、**婆罗门参**、**芽瓜子**，为[石蒜科](../Page/石蒜科.md "wikilink")[仙茅属多年生](../Page/仙茅属.md "wikilink")[草本植物](../Page/草本植物.md "wikilink")。分布于[印度尼西亚](../Page/印度尼西亚.md "wikilink")、[日本](../Page/日本.md "wikilink")、[东南亚](../Page/东南亚.md "wikilink")、[台湾岛以及](../Page/台湾岛.md "wikilink")[中国大陆的](../Page/中国大陆.md "wikilink")[广西](../Page/广西.md "wikilink")、[广东](../Page/广东.md "wikilink")、[贵州](../Page/贵州.md "wikilink")、[福建](../Page/福建.md "wikilink")、[云南](../Page/云南.md "wikilink")、[浙江](../Page/浙江.md "wikilink")、[四川](../Page/四川.md "wikilink")、[湖南](../Page/湖南.md "wikilink")、[江西等地](../Page/江西.md "wikilink")，生长于海拔500米至1,600米的地区，一般生长在草地、林中或荒坡上，属中国原产的植物（非从国外引种）。

## 形态

  - 多年生[草本](../Page/草本.md "wikilink")。高10～14厘米。地下常有单一、不分支的根茎，黑褐色，肉质断面白色。
  - 叶根生，3～6枚，具柄，披针形，革质，长约10～30厘米，宽0.6～2厘米，有散生长毛。
  - 花茎极短，藏于[叶鞘之内](../Page/叶鞘.md "wikilink")，花黄色。

## 生长环境

  - 喜生于山坡、丘陵的草地、灌木丛旁。
  - [长江以南均有生长](../Page/长江.md "wikilink")。主产[四川](../Page/四川.md "wikilink")。

## 加工

药用根茎。春初、夏末采集。除去须根和根头，洗净，切断，晒干备用。

## 药用

中医上讲仙茅辛苦而甘温，有小毒。补肾壮阳，散寒除痹。西医则发现其中所含的[仙茅苷可用来抑制](../Page/仙茅苷.md "wikilink")[β淀粉样蛋白聚合](../Page/β淀粉样蛋白.md "wikilink")。\[1\]

### 主治

  - 肾虚腰痛，[神经衰弱](../Page/神经衰弱.md "wikilink")，[阳萎](../Page/阳萎.md "wikilink")。
  - 妇女[更年期](../Page/更年期.md "wikilink")[高血压](../Page/高血压.md "wikilink")。
  - 慢性肾炎。
  - 无名肿毒。

## 参考文献

  -
## 外部連結

  - [仙茅
    Xianmao](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00304)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)
  - [仙茅
    Xianmao](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00110)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)
  - [仙茅 Xian
    Mao](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00546)
    中藥標本數據庫 (香港浸會大學中醫藥學院)
  - [仙茅苷
    Curculigoside](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00161)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

[仙茅](../Category/仙茅属.md "wikilink")
[Category:球根花卉](../Category/球根花卉.md "wikilink")
[Category:中草藥](../Category/中草藥.md "wikilink")
[Category:1788年描述的植物](../Category/1788年描述的植物.md "wikilink")

1.