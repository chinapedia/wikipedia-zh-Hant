**-{zh-hans:辛西亞;zh-hk:仙西雅;zh-tw:辛西亞;}-·安·“仙迪”·歌羅馥**（，），[美國](../Page/美國.md "wikilink")[超級名模](../Page/超級名模.md "wikilink")，曾是[維多利亞的秘密](../Page/維多利亞的秘密.md "wikilink")（Victoria's
Secret）的[模特兒](../Page/模特兒.md "wikilink")。[西北大學化學系肄業](../Page/西北大學.md "wikilink")。育有1子1女，包括同是模特儿的女兒[凱婭·格伯](../Page/凱婭·格伯.md "wikilink")。

2016年初，仙迪·歌羅馥正式結束了34年（1987年入行）模特兒生涯，她在過去拍攝[廣告](../Page/廣告.md "wikilink")、[封面女郎等貢獻](../Page/封面女郎.md "wikilink")，唯一最不理想的是[電影事業](../Page/電影.md "wikilink")\[1\]。

## 參考

[Category:美國女性模特兒](../Category/美國女性模特兒.md "wikilink")
[Category:美國電影演員](../Category/美國電影演員.md "wikilink")
[Category:美國西北大學校友](../Category/美國西北大學校友.md "wikilink")
[Category:伊利諾州人](../Category/伊利諾州人.md "wikilink")
[Category:法國裔美國人](../Category/法國裔美國人.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:英格蘭裔美國人](../Category/英格蘭裔美國人.md "wikilink")

1.  [50歲仙蒂歌羅馥超模引退](http://the-sun.on.cc/cnt/entertainment/20160202/00476_012.html)
    [太陽報](../Page/太陽報.md "wikilink") 2016年2月2日