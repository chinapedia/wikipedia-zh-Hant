**仙鹤寺**，一称**礼拜寺**，是[中国](../Page/中国.md "wikilink")[江苏省](../Page/江苏省.md "wikilink")[扬州市的一座](../Page/扬州市.md "wikilink")[清真寺](../Page/清真寺.md "wikilink")，与[广州](../Page/广州.md "wikilink")[怀圣寺](../Page/怀圣寺.md "wikilink")、[泉州](../Page/泉州.md "wikilink")[清净寺](../Page/清净寺.md "wikilink")、[杭州](../Page/杭州.md "wikilink")[凤凰寺并称为中国沿海四大](../Page/凤凰寺.md "wikilink")[伊斯兰教寺庙](../Page/伊斯兰教.md "wikilink")，也是中国现存较早的清真寺之一。位于扬州老城区南门街，汶河南路东侧。是[江苏省文物保护单位](../Page/江苏省文物保护单位.md "wikilink")，也是扬州著名景点之一。

[Jingjingtang_Xianhesi.jpg](https://zh.wikipedia.org/wiki/File:Jingjingtang_Xianhesi.jpg "fig:Jingjingtang_Xianhesi.jpg")仙鹤寺始建于[元](../Page/元朝.md "wikilink")[至元十二年](../Page/至元.md "wikilink")（[南宋](../Page/南宋.md "wikilink")[德祐元年](../Page/德祐.md "wikilink")，1275年），由前来扬州传教的[阿拉伯人](../Page/阿拉伯人.md "wikilink")[普哈丁所建](../Page/普哈丁.md "wikilink")，普哈丁是伊斯兰教创始人[穆罕默德第十六世裔孙](../Page/穆罕默德.md "wikilink")。全寺形如[仙鹤](../Page/仙鹤.md "wikilink")，故而得名。该寺大部后毁于战火，[明](../Page/明朝.md "wikilink")[洪武二十三年](../Page/洪武.md "wikilink")（1390年），阿拉伯传教士哈三重建此寺。[嘉靖二年](../Page/嘉靖.md "wikilink")（1523年）再次重建。后大殿等主体建筑又多次重修。

目前，仙鹤寺仍是扬州[穆斯林的主要宗教活动场所](../Page/穆斯林.md "wikilink")。

## 参考文献

  - [扬州文化网·仙鹤寺](https://web.archive.org/web/20070312064851/http://www.yztoday.com/whjnew/newsbh.asp?NewsID=214&BigClassName=&BigClassID=24&SmallClassID=36)
  - [扬州名寺名观](http://www.15766.com/gnly/jsu/g/200607/1497.html)

## 外部链接

## 参见

  - [中国伊斯兰教史](../Page/中国伊斯兰教史.md "wikilink")

{{-}}

[Category:扬州宗教建筑](../Category/扬州宗教建筑.md "wikilink")
[Category:江苏清真寺](../Category/江苏清真寺.md "wikilink")