**代**（338年－376年）是[十六国时期](../Page/十六国.md "wikilink")[鲜卑](../Page/鲜卑.md "wikilink")[拓跋部建立的](../Page/拓跋部.md "wikilink")[政权](../Page/政权.md "wikilink")，是[北魏的前身](../Page/北魏.md "wikilink")。
[thumb](../Page/file:Map_of_Sixteen_Kingdoms_2.png.md "wikilink")
魏晋时期鲜卑拓跋部原[游牧于](../Page/游牧.md "wikilink")[云中](../Page/云中.md "wikilink")（今[内蒙古](../Page/内蒙古.md "wikilink")[托克托东北](../Page/托克托.md "wikilink")）一带。[曹魏](../Page/曹魏.md "wikilink")[甘露三年](../Page/甘露_\(曹髦\).md "wikilink")（258年）大人[拓跋力微徙居](../Page/拓跋力微.md "wikilink")[盛乐](../Page/盛乐.md "wikilink")（今内蒙古[和林格尔北](../Page/和林格尔.md "wikilink")），召集诸部，确立了在部落中的大[酋长地位](../Page/酋长.md "wikilink")。力微卒，诸部离叛。晋[元康五年](../Page/元康_\(晋惠帝\).md "wikilink")（295年）力微子[禄官统部](../Page/拓跋禄官.md "wikilink")，分国人为中、东、西三部，自领东部。[永嘉元年](../Page/永嘉_\(西晋\).md "wikilink")（307年）禄官卒，其侄[猗卢总领三部](../Page/拓跋猗卢.md "wikilink")，有骑士40余万。[永嘉之乱后](../Page/永嘉之乱.md "wikilink")，[中原紛扰](../Page/中原.md "wikilink")，晋[并州](../Page/并州.md "wikilink")[刺史](../Page/刺史.md "wikilink")[刘琨表请封猗卢为代](../Page/刘琨.md "wikilink")[公](../Page/公爵.md "wikilink")，后进封代王。

以后数传至[拓跋什翼犍](../Page/拓跋什翼犍.md "wikilink")。什翼犍曾为[质子在](../Page/人质.md "wikilink")[后赵都](../Page/后赵.md "wikilink")[襄国](../Page/襄国.md "wikilink")（今[河北](../Page/河北.md "wikilink")[邢台](../Page/邢台.md "wikilink")）生活多年，深受[汉文化影响](../Page/汉族.md "wikilink")。[咸康四年](../Page/咸康_\(晋成帝\).md "wikilink")（338年）即代王位，置百官，制[法律](../Page/法律.md "wikilink")，由[部落联盟最终转变为](../Page/部落.md "wikilink")[国家形式](../Page/国家.md "wikilink")。代[建国三年](../Page/建国_\(拓跋什翼犍\).md "wikilink")（340年）定都云中盛乐城（又名石卢城），后又在故城南筑盛乐新城，发展[农业](../Page/农业.md "wikilink")。建国三十九年（376年）[前秦](../Page/前秦.md "wikilink")[苻坚发兵击代](../Page/苻坚.md "wikilink")。什翼犍兵败奔逃后被杀。代亡。

统治地区包括今内蒙古中南部、山西北部。以其所据之地为秦朝时[代郡故地](../Page/代郡.md "wikilink")，故称代王。又王室为拓跋，故又称“**拓跋代**”\[1\]。

386年代国被[拓跋圭重建](../Page/拓跋圭.md "wikilink")，同年改国号为“魏”，史称“[北魏](../Page/北魏.md "wikilink")”。

## 君主列表

## 参考文献

<div class="references-small">

<references>

</references>

</div>

[代國](../Page/category:代國.md "wikilink")

[Category:五胡十六国](../Category/五胡十六国.md "wikilink")
[Category:中國古代民族與國家](../Category/中國古代民族與國家.md "wikilink")
[Category:338年建立的國家或政權](../Category/338年建立的國家或政權.md "wikilink")
[Category:中国历史政权](../Category/中国历史政权.md "wikilink")
[Category:拓跋部](../Category/拓跋部.md "wikilink")

1.