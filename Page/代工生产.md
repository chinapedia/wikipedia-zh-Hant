[Electronics_factory_in_Shenzhen.jpg](https://zh.wikipedia.org/wiki/File:Electronics_factory_in_Shenzhen.jpg "fig:Electronics_factory_in_Shenzhen.jpg")位於[深圳市的電子代工廠房](../Page/深圳市.md "wikilink")\]\]
**代工生产**（）簡稱：委託製造
，又译**原始设备制造商**，指由采购方提供设备和技术，由制造方负责生产、提供人力和场地，采购方负责销售的一种现代流行生产方式。

OEM廠商通常擁有充裕、廉價的勞動力，提供國際市場所需的製造、組裝產品之委託服務。

## 计算机软件

OEM密钥是软件公司发售授权许可的一种模式，其最大的特点是一般直接附着于[主板或硬盘等重要硬件上](../Page/主板.md "wikilink")。[微软公司在这一方面是一个著名的例子](../Page/微软.md "wikilink")，为他们的[Windows操作系统提供OEM版的软件](../Page/Microsoft_Windows.md "wikilink")。OEM[产品密钥比相对应的零售版价格更为低廉](../Page/产品密钥.md "wikilink")，但与后者使用了相同的软件。OEM密钥主要是提供给直接的代工生产厂家与系统开发者，所以一般是作为[批量许可密钥订单出售给这种生产厂家](../Page/批量许可.md "wikilink")(例如[惠普](../Page/惠普.md "wikilink"),
[戴尔](../Page/戴尔.md "wikilink"),
[东芝](../Page/东芝.md "wikilink")，[联想等](../Page/联想集团.md "wikilink"))。个人同样可以购买它们作为个人使用
(包括[虚拟硬件](../Page/虚拟硬件.md "wikilink"))，也可用于他们所组建的电脑的销售或转销。在每一个微软有关于OEM的[最终用户许可协议中](../Page/最终用户许可协议.md "wikilink")，产品密钥都与它所原来安装的电脑主板相绑定，之后这种密钥*一般*是不能在电脑之间进行转让的。这与零售密钥相反，后者可以被转让,
它们只能为一台电脑进行一次激活。一次重大的硬件更换会触发一个再激活提醒，这与零售版相同。

直接代工制造商对于安装媒体等事情承担了正式的责任，尽管在销售电脑硬件时并不要求它提供这种服务，实际上也可以不这么做以降低成本。相反，制造商所提供的服务更多是在主存储设备上恢复分区，供用户修复或恢复其系统到出厂状态等。系统制造商对直接代工生产厂商的安装介质有不同的要求。\[1\]
在需要从微软官方获得下载介质有效产品密钥的Windows版本中（如Windows
7），OEM密钥将被拒绝使用，当事人将需要从制造商处获得有关激活系统的技术支持。

## 參見

  - [ODM](../Page/ODM.md "wikilink")
  - [OBM（自有品牌生產）](../Page/自有品牌生產.md "wikilink")
  - [EMS（電子專業製造服務）](../Page/電子專業製造服務.md "wikilink")
  - [加工出口工廠](../Page/加工出口工廠.md "wikilink")

## 参考文献

[Category:製造業](../Category/製造業.md "wikilink")

1.