[右](https://zh.wikipedia.org/wiki/File:International_Morse_Code.svg "fig:右")是一种着名的代码\]\]
在[通信和](../Page/通信.md "wikilink")[資訊處理中](../Page/資訊處理週期.md "wikilink")，**代码**（**code**）是指一套转换[信息的规则系统](../Page/信息.md "wikilink")，例如将一个[字母](../Page/字母.md "wikilink")、[單詞](../Page/單詞.md "wikilink")、声音、图像或[手势转换为另一种形式或表达](../Page/手势.md "wikilink")，有时还会[缩短或](../Page/数据压缩.md "wikilink")[加密以便通过某种](../Page/保密性.md "wikilink")[信道或存储](../Page/信道.md "wikilink")[媒体通信](../Page/传播媒体.md "wikilink")。一个最早的例子是[語言的发明](../Page/語言.md "wikilink")，它使人可以通过[说话将他看到](../Page/说话.md "wikilink")、听到、感受到或想到的事情表达给其他人。但是，说话的通信范围局限于声音可以有效传播、辨识的范围，并且发言只能传达给现有的听众。将言谈转化为[视觉](../Page/视觉系统.md "wikilink")[符号的](../Page/符号.md "wikilink")[寫作扩大了跨越](../Page/寫作.md "wikilink")[时间](../Page/时间.md "wikilink")、[空间的通信表达](../Page/空间.md "wikilink")。代码有时亦称**代号**等。

而编码（encoding）能将的信息转化为便于通信或存储的符号。解码（Decoding）则是将其逆向还原的过程，将代码符号转化回收件人可以理解的形式。

编码的其中一个原因是在、口语或写作难以实现实现的情况下进行通信。例如，[旗语可以用特定标记表达特定信息](../Page/旗语.md "wikilink")，站在远处的另一个人可以解读标识来重现该信息。

## 参见

  -
  - [密碼 (密碼學)](../Page/密碼_\(密碼學\).md "wikilink")

  -
  -
  -
  - [符號學](../Page/符號學.md "wikilink")

  - [代号](../Page/代号.md "wikilink")（Code name）

## 参考资料

[Category:编码](../Category/编码.md "wikilink")
[Category:信号处理](../Category/信号处理.md "wikilink")