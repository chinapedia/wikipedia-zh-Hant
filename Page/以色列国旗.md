**以色列國國旗**的基本圖案是[猶太教](../Page/猶太教.md "wikilink")[祈禱者的裹頭巾](../Page/祈禱.md "wikilink")(塔利特)，上飾有[藍色](../Page/藍色.md "wikilink")[大衛星](../Page/大衛星.md "wikilink")。

[大卫星](../Page/大卫星.md "wikilink")（即六芒星，又名大卫之盾、所罗门封印、犹太星），是[犹太教和犹太文化的标志](../Page/犹太教.md "wikilink")。以色列建国后将大卫星放在以色列国旗上，因此大卫星也成为了以色列的象征。

在2007年，[以色列測量懸掛在](../Page/以色列.md "wikilink")[馬薩達猶太古堡壘上的國旗](../Page/馬薩達.md "wikilink")，結果顯示該面國旗重量達5.2噸、高100多公尺，獲證實打破世界紀錄，成為当时世界上最大的國旗。

## 图册

[File:Israel-flag01.jpg|挂在海滩的以色列国旗](File:Israel-flag01.jpg%7C挂在海滩的以色列国旗)
[File:Israelisuperflag.jpg|懸掛在](File:Israelisuperflag.jpg%7C懸掛在)[馬薩達沙滩上的以色列國旗](../Page/馬薩達.md "wikilink")，重量達5.2噸、高100多公尺。

## 其他

[Category:以色列国家象征](../Category/以色列国家象征.md "wikilink")
[Y](../Category/国旗.md "wikilink")
[Category:1948年面世的旗幟](../Category/1948年面世的旗幟.md "wikilink")