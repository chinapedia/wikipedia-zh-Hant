[Sack_of_jerusalem.JPG](https://zh.wikipedia.org/wiki/File:Sack_of_jerusalem.JPG "fig:Sack_of_jerusalem.JPG")[提圖斯凱旋門上的圖案為](../Page/提圖斯凱旋門.md "wikilink")[藍本設計的](../Page/藍本.md "wikilink")。\]\]
**[以色列國徽](../Page/以色列.md "wikilink")**是一個由[橄欖圍繞的](../Page/橄欖_\(木樨科\).md "wikilink")[猶太教燈臺](../Page/猶太教燈臺.md "wikilink")，下方寫著「」（[希伯來語裡的](../Page/希伯來語.md "wikilink")「以色列」）。

猶太教燈臺3,000年來都是[猶太教的象徵](../Page/猶太教.md "wikilink")，這種燈臺在古代的[耶路撒冷聖殿中便已使用](../Page/耶路撒冷.md "wikilink")。而橄欖枝則象徵著和平。

以色列國徽也是[以色列護照封面所用的圖案](../Page/以色列護照.md "wikilink")。

以色列國在1948年建國時採用了這個國徽，圖案是由Willie Wind設計的\[1\]。

## 参考文献

## 参见

  - [以色列国旗](../Page/以色列国旗.md "wikilink")、[以色列国歌](../Page/以色列国歌.md "wikilink")
  - [大卫星](../Page/大卫星.md "wikilink")

{{-}}

[I](../Category/國徽.md "wikilink")
[Category:以色列国家象征](../Category/以色列国家象征.md "wikilink")

1.  [Willie Wind web site](http://geocities.com/williwwind/)