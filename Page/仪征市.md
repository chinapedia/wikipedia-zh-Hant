**仪徵市**（簡化字後「-{徵}-」被簡化為「-{征}-」，轉成繁體後仍以「-{徵}-」為宜），位于[中華人民共和国](../Page/中華人民共和国.md "wikilink")[江苏省的中西部](../Page/江苏.md "wikilink")，[南京和](../Page/南京.md "wikilink")[扬州之间](../Page/扬州.md "wikilink")，由[扬州市代管](../Page/扬州市.md "wikilink")。是[长江中下游地区一座重要的](../Page/长江.md "wikilink")[石油](../Page/石油.md "wikilink")[化学工业城市](../Page/化学.md "wikilink")。

## 城市历史

秦时，因江边露出一沙地，称白沙，[唐朝置扬子县](../Page/唐朝.md "wikilink")，[宋朝时因铸造皇帝铜像](../Page/宋朝.md "wikilink")「仪容逼真」，始称仪真，[明代设](../Page/明.md "wikilink")[仪真县](../Page/仪真县.md "wikilink")，[清](../Page/清.md "wikilink")[雍正元年](../Page/雍正.md "wikilink")（1723年）改[仪徵县](../Page/仪徵县.md "wikilink")。末代皇帝[溥仪统治年间为](../Page/溥仪.md "wikilink")[避讳](../Page/避讳.md "wikilink")，曾短期改[扬子县](../Page/扬子县.md "wikilink")，后恢复。

元明清时期，作为[运河入江的主要口门之一](../Page/运河.md "wikilink")，仪徵的商业（特别是盐商）曾经非常繁盛，被列为明朝全国最主要的16个大城市之一\[1\]。

1853年[太平天国战争期间](../Page/太平天国.md "wikilink")，仪徵受到重创，淮盐转输也改到[泰兴](../Page/泰兴.md "wikilink")[口岸镇](../Page/口岸街道.md "wikilink")（今[泰州市](../Page/泰州市.md "wikilink")[高港区](../Page/高港区.md "wikilink")）。1873年，因瓜洲江坍城陷，淮盐总栈又迁到仪徵县城东南12里的[十二圩](../Page/十二圩.md "wikilink")，使得那裡形成一个具有相当规模的商业城镇，号称小都会，建有电厂等当时少见的都市设施和10多个会馆，直到[抗日战争期间才萧条下去](../Page/抗日战争.md "wikilink")。

1982年开始在[胥浦兴建大型企业](../Page/胥浦.md "wikilink")[仪徵化纤公司后](../Page/仪徵化纤.md "wikilink")，仪徵的城市地位再度上升。1986年仪徵撤县设市。目前隶属于地级[扬州市](../Page/扬州市.md "wikilink")。

## 地理环境

仪徵地处[长江北岸省沿江开发地带](../Page/长江.md "wikilink")，拥有28千米长江黄金岸线，北部毗邻[安徽省](../Page/安徽.md "wikilink")[天长市](../Page/天长市.md "wikilink")。[润扬长江大桥在其东侧](../Page/润扬长江大桥.md "wikilink")，[宁启铁路](../Page/宁启铁路.md "wikilink")、宁通高速公路横贯东西，333省道纵穿南北，并正在建设新333省道，南部建有沿江高等级公路。全市地形以平原为主，中北部有小片丘陵，年平均气温15℃，降水量1015毫米。

## 承办大型活动

  - 2018：江苏省园艺博览会\[2\]
  - 2018 : 江苏省省运会\[3\]
  - 2021：[国际园艺博览会](../Page/国际园艺博览会.md "wikilink")(A2/B1级)\[4\]

## 物产

  - 矿产资源主要有黄砂、雨花石及矿泉水。
  - 水产资源有[青鱼](../Page/青鱼.md "wikilink")、[草鱼](../Page/草鱼.md "wikilink")、[鲢](../Page/鲢.md "wikilink")、[鳙](../Page/鳙.md "wikilink")、[鲥等](../Page/鲥.md "wikilink")13种及贝类11种。
  - 植物资源有松、槐、杨、柳、桑、茶、桃、梨、苹果等。
  - 土特产品有朴席，[十二圩五香茶乾](../Page/十二圩.md "wikilink")、皓名茶，龙河大京果、[紫菜苔](../Page/紫菜苔.md "wikilink")，岔镇老鹅等。

## 交通

  - 公路：主要有、、、、等。
  - 公共交通：仪征市公交市民卡于2016年5月6日首次启用并与[南京](../Page/南京.md "wikilink")、[镇江互通](../Page/镇江.md "wikilink")。同年12月市民卡系统升级成功，并发行接入江苏交通统一清算系统的二代市民卡。市内公共自行车已于2016年11月投入使用，同时仪征综合客运枢纽也即将进行建设。

市内智能公交站已于2018年建成使用。

## 名胜古蹟

  - [状元井](../Page/状元井.md "wikilink")
  - [东关水闸](../Page/东关水闸.md "wikilink")
  - [鼓楼](../Page/鼓楼.md "wikilink")
  - [天宁塔](../Page/天宁塔.md "wikilink")
  - [周太谷墓](../Page/周太谷墓.md "wikilink")
  - [庙山汉墓](../Page/庙山汉墓.md "wikilink")
  - [铜山森林公园](../Page/铜山森林公园.md "wikilink")
  - [红山体育公园](../Page/红山体育公园.md "wikilink")
  - [天乐湖](../Page/天乐湖.md "wikilink")
  - [捺山地质公园](../Page/捺山地质公园.md "wikilink")
  - [扬州仪征芍药园](../Page/扬州仪征芍药园.md "wikilink")
  - [扬州西郊森林公园](../Page/扬州西郊森林公园.md "wikilink")
  - [仪征龙山风景区](../Page/仪征龙山风景区.md "wikilink")

## 荣誉称号

  - 江苏省文明城市
  - 国家生态市
  - 全国科技进步先进市
  - 全国绿化模范市
  - 中国名茶之乡
  - 国家卫生城市
  - 中国中小城市科技发展百强县市
  - 2018全国综合实力百强县市（第60名）\[5\]

## 參考文獻

[Category:仪徵市](../Category/仪徵市.md "wikilink")
[扬州](../Category/江苏县级市.md "wikilink")
[市](../Category/扬州区县市.md "wikilink")
[Category:长江沿岸城市](../Category/长江沿岸城市.md "wikilink")
[苏](../Category/中国中等城市.md "wikilink")
[苏](../Category/国家卫生城市.md "wikilink")

1.  当时活跃于全国的[徽商这样评价全国的各大城市](../Page/徽商.md "wikilink")：“今之所谓都会者，则大之而为两京，江、浙、闽、广诸省（会）；次之而苏、松、淮、扬诸府；临清、济宁诸州；仪真、芜湖诸县；瓜州、景德诸镇……。”万历《歙志》卷一
2.
3.
4.
5.