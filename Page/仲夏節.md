**仲夏節**是[夏至來臨時的慶祝活動](../Page/夏至.md "wikilink")，在[北歐是一個重要的節日](../Page/北歐.md "wikilink")。仲夏節在[丹麥](../Page/丹麥.md "wikilink")、[芬蘭及](../Page/芬蘭.md "wikilink")[瑞典都是公眾假期](../Page/瑞典.md "wikilink")。仲夏節是[歐洲北部地區居民的重要傳統節慶活動](../Page/歐洲.md "wikilink")，在[東歐](../Page/東歐.md "wikilink")、[中歐](../Page/中歐.md "wikilink")、[英國](../Page/英國.md "wikilink")、[爱尔兰](../Page/爱尔兰.md "wikilink")、[冰島等地也有慶祝仲夏節](../Page/冰島.md "wikilink")，但北歐與英國尤甚。在一些地方，當地居民會在這一天豎立[仲夏柱](../Page/仲夏柱.md "wikilink")（Maypole）。有分析稱仲夏節在北歐當地成為一個慶祝的重要節日，和當地偏寒冷的氣候有關係。

而在葡萄牙前殖民地[澳門](../Page/澳門.md "wikilink")，也有慶祝聖若翰洗者節，但並不是與夏至有關，而是慶祝1622年6月24日，[荷蘭試圖入侵失敗](../Page/荷蘭.md "wikilink")。此日在澳門回歸前稱為「城市日」。

### 習俗

在古代，歐洲的一些人們相信如果在仲夏夜前夕摘取[金盞花等草藥](../Page/金盞花.md "wikilink")，將具有神奇的治癒效果；當地居民於外出時便點起[火炬或](../Page/火炬.md "wikilink")[篝火](../Page/篝火.md "wikilink")，以驅逐野外的孤魂或精靈。

直到今日，在英國[巨石陣](../Page/巨石陣.md "wikilink")（古代遺址）等地，人們仍會按当地古老的[儀式慶祝仲夏節](../Page/儀式.md "wikilink")，亦會在慶祝期間點起巨型的篝火。

## 參看

  - [芬蘭](../Page/芬蘭.md "wikilink")[伴侶島](../Page/伴侶島.md "wikilink")
  - [伊凡·庫帕拉節](../Page/伊凡·庫帕拉節.md "wikilink")，仲夏節在斯拉夫文化的對應節日

## 参考

[分類:丹麥文化](../Page/分類:丹麥文化.md "wikilink")
[分類:凱爾特文化](../Page/分類:凱爾特文化.md "wikilink")
[分類:挪威文化](../Page/分類:挪威文化.md "wikilink")
[分類:歐洲文化](../Page/分類:歐洲文化.md "wikilink")
[分類:瑞典文化](../Page/分類:瑞典文化.md "wikilink")
[分類:芬蘭文化](../Page/分類:芬蘭文化.md "wikilink")

[Category:施洗約翰](../Category/施洗約翰.md "wikilink")
[Category:欧洲节日](../Category/欧洲节日.md "wikilink")
[Category:6月節日](../Category/6月節日.md "wikilink")
[Category:傳統節日](../Category/傳統節日.md "wikilink")
[Category:夏季節日](../Category/夏季節日.md "wikilink")