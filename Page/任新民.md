**任新民**（\[1\]），生於中華人民共和國[安徽省](../Page/安徽省.md "wikilink")[宁国县](../Page/宁国县.md "wikilink")（今安徽寧國市）。[航天技术和](../Page/航天技术.md "wikilink")[火箭发动机专家](../Page/火箭发动机.md "wikilink")，中国[导弹与](../Page/导弹.md "wikilink")[航天事业开创人之一](../Page/中国航天.md "wikilink")。曾任中华人民共和国的卫星工程总设计师。曾获[两弹一星功勋奖章](../Page/两弹一星功勋奖章.md "wikilink")。[中國科學院院士](../Page/中國科學院院士.md "wikilink")、[國際宇航科學院院士](../Page/國際宇航科學院.md "wikilink")。

## 生平

### 求學

  - 1928年春考入安徽省第四中学（今[安徽省宣城中学](../Page/安徽省宣城中学.md "wikilink")）。
  - 1934年考入南京[國立中央大學](../Page/國立中央大學_\(南京\).md "wikilink")，1937年转入[重庆兵工学校大学部](../Page/重庆兵工学校.md "wikilink")，1940年（民國29年兵工第5期）毕业\[2\]。
  - 1945-1949年，赴美[辛辛那堤磨床銑床廠實習](../Page/辛辛那堤.md "wikilink")；後就讀於[美国](../Page/美国.md "wikilink")[密歇根大学研究院](../Page/密歇根大学.md "wikilink")，获机械工程硕士和工程力学博士学位。

### 工作

  - 1940-1945年，任重慶兵工學校大學部助教、講師；兵工署重慶21廠技術員。
  - 1948年，任[紐約州立大學水牛城分校聘任講師](../Page/紐約州立大學水牛城分校.md "wikilink")。
  - 1949-1952年，任华东军区军事科学研究室任研究员。
  - 1952-1956年，任[哈尔滨军事工程学院教務處副處長](../Page/哈尔滨军事工程学院.md "wikilink")、教授、教授會（教研室）主任、砲兵工程系副主任。1955年被授予上校軍銜。
  - 1956-1965年，任[國防部第五研究院總體研究室主任](../Page/国防部五院.md "wikilink")、設計部主任、分院副院長兼設計部主任。
  - 1965-1982年，任第七機械工業部第一研究院副院長兼研究所所長，部科研生產組副組長、七機部副部長。
  - 1982-1988年，任航天工業部科學技術委員會主任。
  - 1988-1993年，任航空航天工業部高級技術顧問。
  - 1993年，任航天工業總公司高級技術顧問。
  - 2002年，獲得[南京大學](../Page/南京大學.md "wikilink")「世紀校友學術成就金質獎章」。
  - 2006年，獲得「中國航天事業五十年最高榮譽獎」。
  - [长征一号运载火箭的技术负责人领导了中国第一颗人造卫星](../Page/长征一号.md "wikilink")[東方紅一號的发射](../Page/東方紅一號.md "wikilink")；担任[東方紅二號试验通信卫星](../Page/東方紅二號.md "wikilink")、实用通信卫星、[风云一号气象卫星](../Page/风云一号.md "wikilink")、发射外国卫星、[长征三号运载火箭工程等六项大型航天工程的总设计师](../Page/长征三号.md "wikilink")，主持研制和发射工作\[3\]。

### 榮譽

  - 1979年，任[中国宇航学会首任理事长](../Page/中国宇航学会.md "wikilink")。
  - 1980年，当选为[中国科学院院士](../Page/中国科学院.md "wikilink")（学部委员）。
  - 1999年9月18日，在北京人民大會堂庆祝[中华人民共和国成立](../Page/中华人民共和国.md "wikilink")50周年之际，由中共中央、
    国务院及中央军委制作了“[两弹一星](../Page/两弹一星.md "wikilink")”功勋奖章，授予23位为研制“两弹一星”作出突出贡献的科技专家。
  - 2006年10月，与[钱学森](../Page/钱学森.md "wikilink")、[屠守锷](../Page/屠守锷.md "wikilink")、[黄纬禄](../Page/黄纬禄.md "wikilink")、[梁守槃等共](../Page/梁守槃.md "wikilink")5位专家获“中国航天事业五十年最高荣誉奖”。

### 去世

2017年2月12日15时，任新民因病医治无效在[北京逝世](../Page/北京.md "wikilink")，享年102岁\[4\]。2月16日上午，任新民的遗体在[北京八宝山革命公墓火化](../Page/北京八宝山革命公墓.md "wikilink")。

## 参考文献

## 外部链接

  - [两弹一星元勋:任新民](https://web.archive.org/web/20070501151705/http://www.xztc.edu.cn/hangtian/ldyx/rxm.htm)
  - [发射我国第一颗人造地球卫星的功臣——任新民](http://www.people.com.cn/GB/keji/25509/30062/30121/2798983.html)
  - [任新民：“一辈子只干这一件事”](http://www.gmw.cn/content/2005-10/11/content_315075.htm)

## 参见

  - [东方红人造卫星系列](../Page/东方红人造卫星系列.md "wikilink")
  - [中国航天史](../Page/中国航天史.md "wikilink")

{{-}}

[Category:中国科学院技术科学部院士](../Category/中国科学院技术科学部院士.md "wikilink")
[Category:中華人民共和國物理學家](../Category/中華人民共和國物理學家.md "wikilink")
[Category:中国航天工程师](../Category/中国航天工程师.md "wikilink")
[Category:中華人民共和國工程師](../Category/中華人民共和國工程師.md "wikilink")
[Category:中華人民共和國人瑞](../Category/中華人民共和國人瑞.md "wikilink")
[Category:南京大学校友](../Category/南京大学校友.md "wikilink")
[Category:重慶兵工學校校友](../Category/重慶兵工學校校友.md "wikilink")
[Category:國防大學理工學院教師](../Category/國防大學理工學院教師.md "wikilink")
[Category:密西根大學校友](../Category/密西根大學校友.md "wikilink")
[Category:紐約州立大學教師](../Category/紐約州立大學教師.md "wikilink")
[Category:寧國人](../Category/寧國人.md "wikilink")
[Xin新](../Category/任姓.md "wikilink")
[0](../Category/东南大学校友.md "wikilink")
[工](../Category/中央大学校友.md "wikilink")
[Category:南京工业大学校友](../Category/南京工业大学校友.md "wikilink")
[Category:宣城中学校友](../Category/宣城中学校友.md "wikilink")
[Category:安徽科学家](../Category/安徽科学家.md "wikilink")

1.
2.  「兵工學校的創始和演變」、「抗日戰中兵工廠全盛時代」，世界日報，文 / 陳葭元（民國26年班畢），2004年11月14日
3.
4.