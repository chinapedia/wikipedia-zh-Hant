**任港秀**（，），1998年她和[李思蓓](../Page/李思蓓.md "wikilink")、[梁雪湄](../Page/梁雪湄.md "wikilink")、[姚樂怡等組成](../Page/姚樂怡.md "wikilink")[電波少女](../Page/電波少女.md "wikilink")（「愛心少女」前身，現已解散），同年於[無綫電視藝員訓練班第](../Page/無綫電視藝員訓練班.md "wikilink")12期畢業，出道初期廣告、電影、電視節目、電視劇等工作不少，但自從和富商[羅兆輝外遊](../Page/羅兆輝.md "wikilink")[巴黎治療毒瘡後](../Page/巴黎.md "wikilink")，而惹出大禍，以致工作量大減，其後被無綫解約。之後曾經短暫加入[亞洲電視](../Page/亞洲電視.md "wikilink")，不久後退出[娛樂圈](../Page/娛樂圈.md "wikilink")。

現於佐敦播道會泉福堂任職幹事（自2011年底開始）。

## 簡歷

生於[上海](../Page/上海.md "wikilink")，七歲時父親意外離世，自此與母親和弟弟相依為命。於[樂善堂余近卿中學畢業後投考無綫電視藝員訓練班](../Page/樂善堂余近卿中學.md "wikilink")，1998年畢業後簽約[無綫電視成為藝員](../Page/無綫電視.md "wikilink")。2年後，一次節目宣傳記者會因為身穿性感泳裝而受到[香港媒體關注](../Page/香港媒體.md "wikilink")，自此改走[性感路線](../Page/性感.md "wikilink")。\[1\]
兩三年間參與了演出70部電影，包括色情[三級片](../Page/三級片.md "wikilink")。\[2\]\[3\]

## 演出作品

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

  - 《[刑事偵緝檔案IV](../Page/刑事偵緝檔案IV.md "wikilink")》飾 張素玉（檔案八：黑色周五）張素玲妹,武傑女友
  - 《[Loving You](../Page/Loving_You.md "wikilink")》
  - 《[醫神華陀](../Page/醫神華陀.md "wikilink")》
  - 《[鐵翼情緣](../Page/鐵翼情緣.md "wikilink")》
  - 《[創世紀](../Page/創世紀_\(電視劇\).md "wikilink")》飾 Elaine
  - 《[寵物情緣](../Page/寵物情緣.md "wikilink")》飾 Janet
  - 《[烈火雄心II](../Page/烈火雄心II.md "wikilink")》飾 伴娘（第16集）
  - 《[談談情 練練武](../Page/談談情·練練武.md "wikilink")》飾 馮寶珊（Cat）

### 電視劇（[亞洲電視](../Page/亞洲電視.md "wikilink")）

  - 《[法內情2002](../Page/法內情2002.md "wikilink")／[法内有情天](../Page/法内有情天.md "wikilink")》（客串）

### 節目主持（[無綫電視](../Page/無綫電視.md "wikilink")）

  - 《全線大搜查》
  - 《新地任你點》

### 電影

  - 《[魔鬼使徒](../Page/魔鬼使徒.md "wikilink")》
  - 《[灯草和尚](../Page/灯草和尚.md "wikilink")》
  - 2005《[孤魂劫](../Page/孤魂劫.md "wikilink")》
  - 2003《[陰陽路十八之鬼上身](../Page/陰陽路.md "wikilink")》
  - 2003《[陰陽路十九之我對眼見到嘢](../Page/陰陽路.md "wikilink")》
  - 2003《[江湖篇之金牌打手](../Page/江湖篇.md "wikilink")》
  - 2003《[色慾都市之天生買衫狂](../Page/色慾都市之天生買衫狂.md "wikilink")》
  - 2003《[黑枪党](../Page/黑枪党.md "wikilink")》
  - 2003《[真凶](../Page/真凶.md "wikilink")》
  - 2002《[迷离警界之天使名模](../Page/迷离警界.md "wikilink")》
  - 2002《[迷离警界之鬼车](../Page/迷离警界.md "wikilink")》
  - 2003《[江湖篇之大佬](../Page/江湖篇.md "wikilink")》
  - 2002《[横财就手](../Page/横财就手.md "wikilink")》
  - 2002《[偷窺無罪](../Page/偷窺無罪.md "wikilink")》
  - 2000《[我爱你](../Page/我爱你.md "wikilink")》
  - 1999《[摩登姑婆屋](../Page/摩登姑婆屋.md "wikilink")》

## 參考

[任](../Category/香港新教徒.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Kong](../Category/任姓.md "wikilink")
[Category:樂善堂余近卿中學校友](../Category/樂善堂余近卿中學校友.md "wikilink")

1.

2.
3.  [任港秀陪羅兆輝歐遊被無線炒魷](http://www.southcn.com/ent/celeb3/rengangxiu/gallery/200508100179.htm)