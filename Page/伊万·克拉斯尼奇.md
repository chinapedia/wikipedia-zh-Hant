**伊万·克拉斯尼奇**（**Ivan
Klasnić**，），是一名出生于[德国的](../Page/德国.md "wikilink")[克羅地亞足球运动員](../Page/克羅地亞.md "wikilink")，司職前鋒，曾效力于[德甲球队](../Page/德甲.md "wikilink")[美因茨](../Page/美因茨05足球俱乐部.md "wikilink")，现在是自由身。

## 生平

卡斯歷在[西德](../Page/西德.md "wikilink")[漢堡出生](../Page/漢堡.md "wikilink")，父母都是克羅地亞人。他先在[聖保利打起](../Page/聖保利足球俱樂部.md "wikilink")，四年後被德甲的雲達不萊梅相中，被邀請到旗下。當時卡斯歷被認為是最有前途的年輕球員，除具速度外還有高入球量。早在聖保利時在95場比賽中已入26球。由於他於德國出生，又長居於德國，德國國家隊曾邀請他代表德國出賽，但遭到他拒絕。

卡斯歷在轉投雲達不萊梅後主要跟陣中[巴西前鋒](../Page/巴西.md "wikilink")[-{A拍檔](../Page/艾尔顿.md "wikilink")，在艾尔顿於2004年離隊後，才與[德國前鋒](../Page/德國.md "wikilink")[高路斯組成](../Page/高路斯.md "wikilink")「雙K」。他於2003－04年球季在聯賽取得13個入球，並交出十次助攻，為雲達不萊梅贏得該季[德甲聯賽及](../Page/德甲.md "wikilink")[德國足協盃](../Page/德國足協盃.md "wikilink")（決賽對[亞琛時射入一球以](../Page/亚琛足球俱乐部.md "wikilink")3-1獲勝）[雙料冠軍](../Page/雙料冠軍.md "wikilink")。翌年[歐冠杯主場對](../Page/歐冠杯.md "wikilink")[比利時球隊](../Page/比利時.md "wikilink")[安德列治的分組賽中](../Page/安德莱赫特足球俱乐部.md "wikilink")，卡斯歷更大演帽子戲法，該比賽雲達不萊梅以5-1勝出。而作客面對同一球隊，卡斯歷也梅開二度助己隊獲勝。只是球隊出線後被[法甲五連霸](../Page/法甲.md "wikilink")[里昂兩回合以](../Page/里昂足球俱樂部.md "wikilink")2-10蹂躪，才令雲達不萊梅無法在歐冠杯再進一步。

卡斯歷早於2004年代表[克羅地亞國家隊出戰](../Page/克羅地亞國家足球隊.md "wikilink")[歐洲國家杯](../Page/2004年欧洲足球锦标赛.md "wikilink")，但大賽中他只是後備，而國家隊亦欠缺表現。他的正選位置要到2006年[世界杯外圍賽才得以鞏固](../Page/2006年世界盃外圍賽.md "wikilink")。在世界盃舉行前夕，他於2006年3月友賽[阿根廷射入一球](../Page/阿根廷國家足球隊.md "wikilink")，以3-2勝出；其後對[奧地利勝](../Page/奧地利國家足球隊.md "wikilink")4-1的熱身賽中，他更梅開二度。

但卡斯歷儘管表現出色，卻常受傷病影響，加盟不萊梅首季他曾遭遇兩次重創；2005－06年球季亦因傷缺席大半季；而世界杯後他更两次换[肾](../Page/肾.md "wikilink")，但是仍然康复，参加了[2008年欧锦赛并且取得入球](../Page/2008年欧锦赛.md "wikilink")。

2008年4月卡斯歷以當初加盟雲達不萊梅時腎功能只剩下67%，但當時負責體檢的兩名不來梅隊醫迪曼斯基和古哈卻沒有發現這一問題，為此決定起訴兩人，並且索賠150萬[歐元](../Page/歐元.md "wikilink")。卡斯歷認為無法繼續和這兩名隊醫合作，因此拒絕與不來梅續約，季後轉會到[南特](../Page/南特足球俱乐部.md "wikilink")，簽約4年<small>\[1\]</small>。

卡斯歷在[法甲首季上陣](../Page/法甲.md "wikilink")28場僅射入6球，而南特亦於季後降班[法乙](../Page/法乙.md "wikilink")。2009/10年球卡斯歷有復甦表現，開季頭5場即射入4球。2009年9月1日在轉會窗關閉前獲借用到[英超球會](../Page/英超.md "wikilink")[保頓一季](../Page/博尔顿足球俱乐部.md "wikilink")<small>\[2\]</small>。期間表現出色，在各項賽事合共上陣32場並射入8球，於季後約滿南特回復[自由身](../Page/自由球員.md "wikilink")，於2010年8月4日免費正式加盟保頓，簽約兩年<small>\[3\]</small>。2011/12赛季结束后，降级到英冠联赛的博尔顿决定放弃和克拉什尼奇续约，克拉什尼奇成为自由球员。

2012年9月，克拉什尼奇和德甲球队[美因茨签约一年](../Page/美因茨05足球俱乐部.md "wikilink")。\[4\]

## 榮譽

  - 德甲聯賽冠軍：2003/04年
  - 德國盃：2004年

## 參考資料

## 外部連結

  - [個人官方網站](http://www.ivanklasnic.com)

  -
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")
[Category:克羅埃西亞足球運動員](../Category/克羅埃西亞足球運動員.md "wikilink")
[Category:聖保利球員](../Category/聖保利球員.md "wikilink")
[Category:雲達不萊梅球員](../Category/雲達不萊梅球員.md "wikilink")
[Category:南特球員](../Category/南特球員.md "wikilink")
[Category:保頓球員](../Category/保頓球員.md "wikilink")
[Category:緬恩斯球員](../Category/緬恩斯球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:漢堡人](../Category/漢堡人.md "wikilink")

1.  [克拉斯尼奇别德甲 肾斗士转投法甲南特](http://dfo.cn/dfo/show.asp?id=8077)
2.
3.
4.