**伊丹市**（）是位於日本[兵庫縣東南部的城市](../Page/兵庫縣.md "wikilink")，屬於[大阪市的](../Page/大阪市.md "wikilink")[衛星城市](../Page/衛星城市.md "wikilink")，距離大阪市市中心僅約10公里距離\[1\]。轄內東側有、西側通過，全市大部分區域皆平坦；[JR西日本的](../Page/JR西日本.md "wikilink")[福知山線以南北芳巷通過市區](../Page/福知山線.md "wikilink")，[阪急](../Page/阪急.md "wikilink")[伊丹線也從南面通往市中心](../Page/伊丹線.md "wikilink")。

[大阪國際機場的大部份區域位于該市轄區內](../Page/大阪國際機場.md "wikilink")，機場也被通稱為「伊丹機場」。

主要產業包括[園藝及](../Page/園藝.md "wikilink")[釀酒](../Page/釀酒.md "wikilink")。

## 歷史

在[鎌倉時代屬於](../Page/鎌倉時代.md "wikilink")[攝關家領地](../Page/攝關家.md "wikilink")，並交由治理，到[戰國時代伊丹氏在此築有](../Page/戰國時代.md "wikilink")，並支配[攝津國約三分之一的範圍](../Page/攝津國.md "wikilink")。\[2\]

戰國時代末期，成為[織田信長家臣](../Page/織田信長.md "wikilink")[荒木村重的領地](../Page/荒木村重.md "wikilink")，並在重整伊丹城後將之改名為「有岡城」，由於荒木村重的謀反，本地爆發了[有岡城之戰](../Page/有岡城之戰.md "wikilink")\[3\]，[黑田孝高也因此被囚於此長達一年](../Page/黑田孝高.md "wikilink")。在此役結束後，有岡城被廢除不再使用。\[4\]

[江戶時代後成為攝關家](../Page/江戶時代.md "wikilink")[近衛家的領地](../Page/近衛家.md "wikilink")，此地的[酿酒業開始發展](../Page/酿酒.md "wikilink")；直到[明治維新](../Page/明治維新.md "wikilink")1870年實施[廢藩置縣後](../Page/廢藩置縣.md "wikilink")，被併入新設的[兵庫縣](../Page/兵庫縣.md "wikilink")。1889年日本實施[町村制](../Page/町村制.md "wikilink")，設立了****\[5\]，隸屬[川邊郡](../Page/川邊郡.md "wikilink")，1940年伊丹町和相鄰的[合併為](../Page/市町村合併.md "wikilink")**伊丹市**，1947年再將神津村併入，形成現在的伊丹市的範圍。\[6\]

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年-1926年</p></th>
<th><p>1926年-1944年</p></th>
<th><p>1944年-1954年</p></th>
<th><p>1954年-1989年</p></th>
<th><p>1989年-現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>伊丹町</p></td>
<td><p>1940年11月10日<br />
伊丹市</p></td>
<td><p>伊丹市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>稻野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>神津村</p></td>
<td><p>1947年3月1日<br />
併入伊丹市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 機場

[大阪國際機場的大部分區域都位在伊丹市轄區內](../Page/大阪國際機場.md "wikilink")，因此一般也被通稱為「伊丹機場」。1994年[关西国际机场啟用後](../Page/关西国际机场.md "wikilink")，國際航線轉移至關西國際機場，此機場以日本國內線為主。

### 鐵路

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）
      - [福知山線](../Page/福知山線.md "wikilink")（JR寶塚線）：（←[川西市](../Page/川西市.md "wikilink")）
        -  -  - （[尼崎市](../Page/尼崎市.md "wikilink")）
      - [山陽新幹線](../Page/山陽新幹線.md "wikilink")：路線通過南部轄區，但未設置車站。
  - [阪急電鐵](../Page/阪急電鐵.md "wikilink")
      - [伊丹線](../Page/伊丹線.md "wikilink")： -  - - （尼崎市）

[File:JRW-Kita-ItamiStation-MainGate-1.jpg|JR北伊丹車站](File:JRW-Kita-ItamiStation-MainGate-1.jpg%7CJR北伊丹車站)
<File:JR-Itami> Station.JPG|JR伊丹車站
[File:HK-ItamiStation.jpg|阪急伊丹車站](File:HK-ItamiStation.jpg%7C阪急伊丹車站)
[File:HK-Shin-ItamiStation-WestGate.jpg|阪急新伊丹車站](File:HK-Shin-ItamiStation-WestGate.jpg%7C阪急新伊丹車站)
[File:HK-InanoStation-WestGate.jpg|阪急稻野車站](File:HK-InanoStation-WestGate.jpg%7C阪急稻野車站)

### 公路

  - 高速道路

<!-- end list -->

  - [中國自動車道](../Page/中國自動車道.md "wikilink")：（[寶塚市](../Page/寶塚市.md "wikilink")）
    - （通過轄區內，但未設有交流道） - （寶塚市）
      -
        距離最近的交流道為[寶塚交流道](../Page/寶塚IC.md "wikilink")、[中國池田交流道](../Page/中國池田交流道.md "wikilink")、[中國豐中交流道](../Page/中國豐中交流道.md "wikilink")。
  - [名神高速道路](../Page/名神高速道路.md "wikilink")：
      -
        由南方約1公里處通過，距離最近的交流道為[尼崎交流道](../Page/尼崎IC.md "wikilink")、[豐中交流道](../Page/豐中交流道.md "wikilink")
  - [阪神高速11號池田線](../Page/阪神高速11號池田線.md "wikilink")：（[池田市](../Page/池田市.md "wikilink")）
    - （通過轄區內，但未設有交流道） - （池田市）
      -
        距離最近的交流道為[大阪機場出入口](../Page/大阪機場出入口.md "wikilink")、[豐中北出入口](../Page/豐中北出入口.md "wikilink")、[豐中交流道](../Page/豐中交流道.md "wikilink")

<!-- end list -->

  - 國道

<!-- end list -->

  - 、

## 觀光資源

[Aramaki_rose_park_mainroad_20071019.jpg](https://zh.wikipedia.org/wiki/File:Aramaki_rose_park_mainroad_20071019.jpg "fig:Aramaki_rose_park_mainroad_20071019.jpg")

  - 遺址

  -
  -
  -
  -
  -
## 教育

### 大學

  - [大手前大學](../Page/大手前大學.md "wikilink")（伊丹校區）

## 姊妹、友好都市

### 日本

  - [大村市](../Page/大村市.md "wikilink")（[長崎縣](../Page/長崎縣.md "wikilink")）
  - [飯南町](../Page/飯南町.md "wikilink")（[島根縣](../Page/島根縣.md "wikilink")[飯石郡](../Page/飯石郡.md "wikilink")）
  - [常滑市](../Page/常滑市.md "wikilink")（[愛知縣](../Page/愛知縣.md "wikilink")）

### 海外

  - [哈瑟爾特](../Page/哈瑟爾特.md "wikilink")（[比利时](../Page/比利时.md "wikilink")[弗拉芒大區](../Page/弗拉芒大區.md "wikilink")[林堡省](../Page/林堡省_\(比利时\).md "wikilink")）

  - [佛山市](../Page/佛山市.md "wikilink")（[中國](../Page/中國.md "wikilink")[廣東省](../Page/廣東省.md "wikilink")）

### 本地出身之名人

  - [有村架純](../Page/有村架純.md "wikilink")：演員
  - [石末龍治](../Page/石末龍治.md "wikilink")：足球選手
  - [上村愛子](../Page/上村愛子.md "wikilink")：[自由式滑雪選手](../Page/自由式滑雪.md "wikilink")
  - [奥井雅美](../Page/奥井雅美.md "wikilink")：歌手
  - [北川博敏](../Page/北川博敏.md "wikilink")：棒球選手
  - [木下百花](../Page/木下百花.md "wikilink")：[NMB48成員](../Page/NMB48.md "wikilink")
  - [木山隆之](../Page/木山隆之.md "wikilink")：足球選手
  - [坂本勇人](../Page/坂本勇人.md "wikilink")：棒球選手
  - [杉本美香](../Page/杉本美香.md "wikilink")：柔道選手
  - [田中将大](../Page/田中将大.md "wikilink")：棒球選手
  - [中島裕之](../Page/中島裕之.md "wikilink")：棒球選手
  - [米本拓司](../Page/米本拓司.md "wikilink")：足球選手

## 參考資料

## 相關條目

  - （有岡城）

  - [有岡城之戰](../Page/有岡城之戰.md "wikilink")

## 外部連結

  - [伊丹市的介紹](http://www.city.itami.lg.jp/shokai/)

  -

  -

  -

<!-- end list -->

1.

2.

3.
4.

5.
6.