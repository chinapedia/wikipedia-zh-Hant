**伊仙町**（，）是[鹿兒島縣轄下位於](../Page/鹿兒島縣.md "wikilink")[德之島上南部的一個城鎮](../Page/德之島.md "wikilink")，屬[大島郡](../Page/大島郡_\(鹿兒島縣\).md "wikilink")。

日本歷代最[長壽者有兩位出生於此](../Page/壽命.md "wikilink")（[泉重千代](../Page/泉重千代.md "wikilink")120歲、[本鄉竈](../Page/本鄉竈.md "wikilink")116歲）。\[1\]

伊仙町常因町內選舉（町長選舉與町議員選舉）的紛爭被日本國內媒體報導。

## 沿革

  - 1908年4月1日：奄美群島實施[島嶼町村制](../Page/島嶼町村制.md "wikilink")，設置島尻村。
  - 1921年6月29日 改名為伊仙村。
  - 1962年1月1日 改制為伊仙町。\[2\]

## 教育

  - [鹿兒島縣立德之島農業高等學校](../Page/鹿兒島縣立德之島農業高等學校.md "wikilink")

## 本地出身之名人

  - [泉重千代](../Page/泉重千代.md "wikilink")：前世界最高齡者、現最長壽之男性紀錄保持者
  - [本鄉竈](../Page/本鄉竈.md "wikilink")：前世界最高齡者
  - [德田虎雄](../Page/德田虎雄.md "wikilink")：前[日本眾議院](../Page/日本眾議院.md "wikilink")[議員](../Page/議員.md "wikilink")、特定醫療法人德洲會理事長
  - [德田毅](../Page/德田毅.md "wikilink")：[日本眾議院](../Page/日本眾議院.md "wikilink")[議員](../Page/議員.md "wikilink")、德田虎雄之子
  - [泉芳朗](../Page/泉芳朗.md "wikilink")：[奄美群島祖國恢復運動的指導者](../Page/奄美群島祖國恢復運動.md "wikilink")・[詩人](../Page/詩人.md "wikilink")

## 參考資料

## 外部連結

  - [伊仙町商工會](http://isen.kashoren.or.jp/)

<!-- end list -->

1.
2.