[Ivan_III_of_Russia.jpg](https://zh.wikipedia.org/wiki/File:Ivan_III_of_Russia.jpg "fig:Ivan_III_of_Russia.jpg")
[Ivan_III_of_Russia_3.jpg](https://zh.wikipedia.org/wiki/File:Ivan_III_of_Russia_3.jpg "fig:Ivan_III_of_Russia_3.jpg")
**伊凡三世·瓦西里耶维奇**（，1440年—1505年10月17日），是[莫斯科大公](../Page/莫斯科大公.md "wikilink")，在位时间1462年-1505年。人稱**伊凡大帝**，被部分俄罗斯史学家认为是[俄罗斯帝国的开创者](../Page/俄罗斯帝国.md "wikilink")。

伊凡三世是使[俄罗斯取得了独立的莫斯科大公](../Page/莫斯科大公国.md "wikilink")。伊凡三世时期，莫斯科大公国最终统一了[雅罗斯拉夫尔](../Page/雅罗斯拉夫尔.md "wikilink")、[诺夫哥罗德](../Page/诺夫哥罗德.md "wikilink")、[彼尔姆](../Page/彼尔姆.md "wikilink")、[特维尔这几个俄罗斯公国](../Page/特维尔.md "wikilink")，这几个公国被伊凡三世以前的莫斯科大公们多次征服过并向莫斯科公国臣服。蒙古人的[金帐汗国](../Page/金帐汗国.md "wikilink")（1219—1502年）不久由于内斗而分裂成几个小汗国（[喀山汗国](../Page/喀山汗国.md "wikilink")、[阿斯特拉罕汗国](../Page/阿斯特拉罕汗国.md "wikilink")、[诺盖汗国](../Page/诺盖汗国.md "wikilink")、[昔班尼国](../Page/昔班尼.md "wikilink")、[克里米亚汗国](../Page/克里米亚汗国.md "wikilink")），伊凡三世于是在1480年停止对金帐汗国的纳贡，从而结束了两个半世纪的金帐汗国统治，并在1502年灭掉了金帐汗国。

1497年，伊凡三世颁布了法典（Sudebnik），建立了莫斯科大公国的政府机构。在这部法典中，规定了拜占廷的[双头鹰国徽为俄罗斯](../Page/双头鹰.md "wikilink")[国徽](../Page/国徽.md "wikilink")，并且将其图案刻在了俄国[国玺上](../Page/国玺.md "wikilink")。同年，一面镀金的双头鹰徽记被安放在了[克里姆林宫的斯巴斯基塔楼上](../Page/克里姆林宫.md "wikilink")。俄罗斯获得了象征自己国家的标志。同样在这部法典中，他还限制了农民的流动，规定只有在圣尤里节（俄曆11月26日）前后两周期间，农民要還清債務才可离开土地四处走动，如地主不願放行，就會避開，農民要再等一年。开辟了莫斯科公国的[农奴化进程](../Page/农奴.md "wikilink")。

原配夫人去世后，1473年娶[拜占庭帝国](../Page/拜占庭帝国.md "wikilink")[巴列奥略王朝的](../Page/巴列奥略王朝.md "wikilink")[索菲娅·帕列奥罗格公主](../Page/索菲娅·帕列奥罗格.md "wikilink")（此时拜占庭帝国已经灭亡，索非娅为亡国皇室旁支）。1500年，其子[瓦西里三世为获取大公权力公然造反](../Page/瓦西里三世.md "wikilink")，伊凡三世束手无策。1502年，伊凡三世被迫将大公称号授予[瓦西里](../Page/瓦西里.md "wikilink")，大权旁落的伊凡三世在抑郁中死去。

## 参考文献

## 参见

  - [俄罗斯历史](../Page/俄罗斯历史.md "wikilink")

[Category:莫斯科大公](../Category/莫斯科大公.md "wikilink")
[Category:1440年出生](../Category/1440年出生.md "wikilink")
[Category:1505年逝世](../Category/1505年逝世.md "wikilink")