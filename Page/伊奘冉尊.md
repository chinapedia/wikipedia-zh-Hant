}}
[Kobayashi_Izanami_and_Izanagi.jpg](https://zh.wikipedia.org/wiki/File:Kobayashi_Izanami_and_Izanagi.jpg "fig:Kobayashi_Izanami_and_Izanagi.jpg")畫家[小林永濯筆下的](../Page/小林永濯.md "wikilink")[伊奘諾尊](../Page/伊奘諾尊.md "wikilink")（右）及**伊奘冉尊**（左）\]\]
**伊奘冉尊**（
，《[古事記](../Page/古事記.md "wikilink")》作伊邪那美命，又作伊耶那美）是[日本神話中開天闢地的](../Page/日本神話.md "wikilink")[神祇](../Page/神祇.md "wikilink")，為兄長[伊奘諾尊](../Page/伊奘諾尊.md "wikilink")（）的妹妹同時也為妻子。又稱作黃泉津大神、道敷大神。

## 概要

[伊奘諾尊與伊奘冉尊](../Page/伊奘諾尊.md "wikilink")，祂們是[日本神話裡的](../Page/日本神話.md "wikilink")[神世七代的神明之一](../Page/神世七代.md "wikilink")。

以下為各典籍記載之神世七代神名對照表：

{{-}}

## 神話故事

詳細可參考[神誕生或](../Page/神誕生.md "wikilink")[國誕生](../Page/国诞生.md "wikilink")。

與祂的兄長[伊奘諾尊](../Page/伊奘諾尊.md "wikilink")（）作為日本神話中開天闢地的第七代兄妹神祇出場。日本列島以及許多島嶼等（如：淡路島、隱歧島、包括日本本島在內）皆由牠們所創造，其後兄妹產生眾島和諸神。

**伊奘冉尊**在生產火神[軻遇突智](../Page/軻遇突智.md "wikilink")（日語：かぐつち，《[古事記](../Page/古事記.md "wikilink")》作[迦具土神](../Page/迦具土.md "wikilink")、火之迦具土神等，《[日本書紀](../Page/日本書紀.md "wikilink")》作[軻遇突智](../Page/軻遇突智.md "wikilink")、火產靈）時，因其火而燒傷[陰部而臥病在床最後死亡](../Page/生殖器.md "wikilink")，在死前的嘔吐物、尿等也同時誕生出其他神祇。之後[伊奘諾尊憤而殺死](../Page/伊奘諾尊.md "wikilink")[軻遇突智](../Page/軻遇突智.md "wikilink")。

**伊奘冉尊**死後，[伊奘諾尊思念](../Page/伊奘諾尊.md "wikilink")**伊奘冉尊**而到[黃泉之國並且打算帶](../Page/黃泉.md "wikilink")**伊奘冉尊**回到地上[葦原中津國](../Page/葦原中國.md "wikilink")。當時約定在回到地上之前絕對不能回頭看**伊奘冉尊**，這樣**伊奘冉尊**才能得以回到地上。之後因為在路途中回頭看到**伊奘冉尊**從腐敗的屍體逐漸回復成人的過程而讓**伊奘冉尊**無法復活完全，並且嚇到逃跑。**伊奘冉尊**對於無法完全復活而感到憤怒便開始追[伊奘諾尊並且派出鬼追](../Page/伊奘諾尊.md "wikilink")。最後[伊奘諾尊在連繫黃泉路跟地上](../Page/伊奘諾尊.md "wikilink")[葦原中津國之間的](../Page/葦原中國.md "wikilink")[黃泉比良坂](../Page/黃泉比良坂.md "wikilink")（日語：よもつひらさか）用一顆大岩石阻擋道路。

之後**伊奘冉尊**再也回不到地上[葦原中津國並且成為](../Page/葦原中國.md "wikilink")[黃泉的主宰神](../Page/黃泉.md "wikilink")，因此也被稱作黃泉津大神、道敷大神。

## 內部連結

  - [國誕生](../Page/國誕生.md "wikilink")
  - [神誕生](../Page/神誕生.md "wikilink")

[I](../Category/日本女神.md "wikilink") [Category:天神
(日本神話)](../Category/天神_\(日本神話\).md "wikilink")
[I](../Category/地母神.md "wikilink")
[Category:死神](../Category/死神.md "wikilink")
[Category:豐穰神](../Category/豐穰神.md "wikilink")
[Category:創造神](../Category/創造神.md "wikilink")