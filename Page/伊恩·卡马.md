**伊恩·卡马**（**Ian
Khama**，\[1\]），[博茨瓦纳政治家](../Page/博茨瓦纳.md "wikilink")，[博茨瓦纳民主党成员](../Page/博茨瓦纳民主党.md "wikilink")，曾任[博茨瓦纳副总统](../Page/博茨瓦纳副总统.md "wikilink")（1998年-2008年），2008年4月1日起任[博茨瓦纳总统](../Page/博茨瓦纳总统.md "wikilink")。

卡马是博茨瓦纳首任总统[塞雷茨·卡马的长子](../Page/塞雷茨·卡马.md "wikilink")，1998年退出军界，目前仍保留[中将军衔](../Page/中将.md "wikilink")。

## 參考資料

|-

[分類:桑德赫斯特皇家軍事學院校友](../Page/分類:桑德赫斯特皇家軍事學院校友.md "wikilink")

[Category:博茨瓦纳总统](../Category/博茨瓦纳总统.md "wikilink")
[Category:博茨瓦納副總統](../Category/博茨瓦納副總統.md "wikilink")
[Category:博茨瓦納民主黨黨員](../Category/博茨瓦納民主黨黨員.md "wikilink")
[Category:博茨瓦納將軍](../Category/博茨瓦納將軍.md "wikilink")
[Category:博茨瓦納飛行員](../Category/博茨瓦納飛行員.md "wikilink")

1.  ["True to tradition, Khama is born to rule
    Botswana"](http://www.pretorianews.co.za/index.php?fArticleId=4330211),
    Sapa-AFP (*Pretoria News*), 1 April 2008.