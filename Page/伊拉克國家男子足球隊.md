**伊拉克國家足球隊**是為中東國家[伊拉克的](../Page/伊拉克.md "wikilink")[足球代表隊](../Page/足球.md "wikilink")，它是由[伊拉克足球協會負責管理](../Page/伊拉克足球協會.md "wikilink")。伊拉克擁有自己的足球聯賽—[伊拉克足球聯賽](../Page/伊拉克足球聯賽.md "wikilink")。伊拉克曾經在1986年時晉身[世界盃的決賽周](../Page/1986年世界盃足球賽.md "wikilink")，是其中一支最早晉身世界杯決賽周的阿拉伯國家球隊，卻在該屆在首圈出局。

## 歷史

伊拉克首次參加世界杯外圍賽是1974年的西德世界杯。在[1986年世界杯外圍賽](../Page/1986年世界杯.md "wikilink")，伊拉克淘汰[卡塔爾](../Page/卡塔爾國家足球隊.md "wikilink")、[阿聯酋及](../Page/阿聯酋國家足球隊.md "wikilink")[敘利亞等球隊](../Page/敘利亞國家足球隊.md "wikilink")，取得當年西亞區唯一的出線資格，晉身1986年世界杯決賽周，這也是伊拉克至今唯一一次參與世界杯決賽周。伊拉克在決賽周分別敗給[墨西哥](../Page/墨西哥國家足球隊.md "wikilink")、[比利時及](../Page/比利時國家足球隊.md "wikilink")[巴拉圭](../Page/巴拉圭國家足球隊.md "wikilink")，分組賽三戰全敗出局。

在[萨达姆·侯赛因政權統治伊拉克](../Page/薩達姆·侯賽因.md "wikilink")
期間(1979年至2003年)，伊拉克的體育部曾一度由侯賽因之子[乌代·侯赛因所掌管](../Page/烏代·侯賽因.md "wikilink")，他曾恐嚇及規定若球隊被對手擊敗的話，球員以及教練都會受到如鞭打、刖足或踢有刺的足球等的刑罰<small>\[1\]</small>，但烏代的恐嚇手段反而令伊拉克隊在國際賽上取不到更好的成績。後來經過[伊拉克戰爭後侯賽因政權倒台](../Page/伊拉克戰爭.md "wikilink")，烏代掌管伊拉克足球的時代亦告結束。雖然伊拉克被美軍進駐，民族和宗教衝突持續發生，伊拉克足球隊的主場賽事更因安全問題需要安排在其他國家舉行，但伊拉克足球隊反而在國際性賽事中比侯賽因時代有更良好的表現，如在[雅典奧運中](../Page/2004年夏季奧林匹克運動會.md "wikilink")，他們先後擊敗[葡萄牙](../Page/葡萄牙國家足球隊.md "wikilink")、[澳洲等國家隊](../Page/澳洲國家足球隊.md "wikilink")，最終取得第四；而在2007年的[亞洲盃](../Page/2007年亞洲盃足球賽.md "wikilink")，他們在被看淡的情況下，先在分組賽擊敗奪標熱門澳洲，以小組首名出線，之後又在準決賽戰勝另一支熱門[-{zh-hans:韩国;
zh-hk:南韓;}-](../Page/南韓國家足球隊.md "wikilink")，首次晉身至決賽之中。於決賽又以1比0戰勝三屆冠軍[沙特阿拉伯](../Page/沙特阿拉伯國家足球隊.md "wikilink")，傳奇地第一次奪得[亞洲盃冠軍](../Page/亞洲盃足球賽.md "wikilink")。

奪取亞洲杯的伊拉克沒法將好成績延續到[2010年世界杯外圍賽中](../Page/2010年世界杯.md "wikilink")，他們在首輪分組賽不敵澳洲及[卡塔爾黯然出局](../Page/卡塔爾國家足球隊.md "wikilink")，至[2011年亞洲杯](../Page/2011年亞洲杯.md "wikilink")，衛冕冠軍的伊拉克雖能以分組賽次名晉級次圈淘汰賽，但再次以0-1敗給澳洲出局。

[2014年世界杯外圍賽首輪分組賽](../Page/2014年世界杯外圍賽.md "wikilink")，伊拉克力壓[約旦及](../Page/約旦國家足球隊.md "wikilink")[中國](../Page/中國國家足球隊.md "wikilink")，以小組首名出線次輪分組賽。但是10强赛伊拉克队在2012年11月教练[济科辞职后成绩再度下滑](../Page/济科.md "wikilink")，在多哈以0-1敗给[日本後](../Page/日本國家足球隊.md "wikilink")，提早一輪比賽宣告出局。

[2015年亞洲盃足球賽](../Page/2015年亞洲盃足球賽.md "wikilink")，伊拉克分組初賽壓倒[約旦以小組次名出線](../Page/約旦國家足球隊.md "wikilink")，八強賽與[伊朗激戰](../Page/伊朗國家足球隊.md "wikilink")120分鐘打成3-3平手，最後憑互射十二碼戰勝伊朗晉級四強，但伊拉克在準決賽以0-2敗給[南韓](../Page/韓國國家足球隊.md "wikilink")，再在季軍戰以2-3敗給[阿聯酋](../Page/阿聯酋國家足球隊.md "wikilink")，得到殿軍。

[2018年世界杯外圍賽首輪分組賽](../Page/2018年世界杯外圍賽.md "wikilink")，伊拉克落後於[泰國](../Page/泰國國家足球隊.md "wikilink")，以最佳小組次名出線次輪分組賽。在12强赛伊拉克队不敵[日本](../Page/日本國家足球隊.md "wikilink")、[沙特阿拉伯](../Page/沙特阿拉伯國家足球隊.md "wikilink")、[澳洲等球隊](../Page/澳洲國家足球隊.md "wikilink")，只得小組第五名出局。

[2019年亞洲盃足球賽](../Page/2019年亞洲盃足球賽.md "wikilink")，伊拉克以小組次名出線十六強，但在十六強賽以0-1不敵[卡塔爾出局](../Page/卡塔爾國家足球隊.md "wikilink")。

到目前為止，伊拉克足球隊的所有主場賽事更因安全問題需要安排在其他國家舉行，最近一次在自己主場比賽已是2002年7月22日在[巴格達Al](../Page/巴格達.md "wikilink")
Shaab Stadium。新主場[巴斯拉Basra](../Page/巴斯拉.md "wikilink") Sports
City至目前都還未能舉辦國際賽事。

## 世界盃成績

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/世界盃足球賽.md" title="wikilink">世界盃參賽紀錄</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>年份</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1930年世界盃足球賽.md" title="wikilink">1930年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1934年世界盃足球賽.md" title="wikilink">1934年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1938年世界盃足球賽.md" title="wikilink">1938年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1950年世界盃足球賽.md" title="wikilink">1950年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1954年世界盃足球賽.md" title="wikilink">1954年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1958年世界盃足球賽.md" title="wikilink">1958年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1962年世界盃足球賽.md" title="wikilink">1962年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1966年世界盃足球賽.md" title="wikilink">1966年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1970年世界盃足球賽.md" title="wikilink">1970年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1974年世界盃足球賽.md" title="wikilink">1974年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1978年世界盃足球賽.md" title="wikilink">1978年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1982年世界盃足球賽.md" title="wikilink">1982年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1986年世界盃足球賽.md" title="wikilink">1986年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1990年世界盃足球賽.md" title="wikilink">1990年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1994年世界盃足球賽.md" title="wikilink">1994年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1998年世界盃足球賽.md" title="wikilink">1998年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2002年世界盃足球賽.md" title="wikilink">2002年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2006年世界盃足球賽.md" title="wikilink">2006年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2010年世界盃足球賽.md" title="wikilink">2010年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2014年世界盃足球賽.md" title="wikilink">2014年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2018年世界盃足球賽.md" title="wikilink">2018年</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>總結</strong></p></td>
</tr>
</tbody>
</table>

### 1986年世界杯足球赛

-----

-----

## 洲際國家盃成績

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>最終成績</p></th>
<th><p>場數</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>得球</p></th>
<th><p>失球</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1992年洲際國家盃.md" title="wikilink">1992年至</a><a href="../Page/2005年洲際國家盃.md" title="wikilink">2005年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2009年洲際國家盃.md" title="wikilink">2009年</a></p></td>
<td><p>第一圈</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2013年洲際國家盃.md" title="wikilink">2013年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2017年洲際國家盃.md" title="wikilink">2017年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><strong>總結</strong></p></td>
<td><p>1/10</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
</tr>
</tbody>
</table>

### 2009年联合会杯

-----

-----

## 亞洲盃成績

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>最終成績</p></th>
<th><p>總排名</p></th>
<th><p>場次</p></th>
<th><p>勝</p></th>
<th><p>和*</p></th>
<th><p>負</p></th>
<th><p>得球</p></th>
<th><p>失球</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1956年亞洲盃足球賽.md" title="wikilink">1956年</a></p></td>
<td><p><em>並未參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1960年亞洲盃足球賽.md" title="wikilink">1960年</a></p></td>
<td><p><em>並未參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1964年亞洲盃足球賽.md" title="wikilink">1964年</a></p></td>
<td><p><em>並未參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1968年亞洲盃足球賽.md" title="wikilink">1968年</a></p></td>
<td><p><em>並未參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1972年亞洲盃足球賽.md" title="wikilink">1972年</a></p></td>
<td><p>第一圈</p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1976年亞洲盃足球賽.md" title="wikilink">1976年</a></p></td>
<td><p>殿軍</p></td>
<td><p>4</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>3</p></td>
<td><p>6</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1980年亞洲盃足球賽.md" title="wikilink">1980年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1984年亞洲盃足球賽.md" title="wikilink">1984年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1988年亞洲盃足球賽.md" title="wikilink">1988年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1992年亞洲盃足球賽.md" title="wikilink">1992年</a></p></td>
<td><p><em>並未參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1996年亞洲盃足球賽.md" title="wikilink">1996年</a></p></td>
<td><p>八強</p></td>
<td><p>6</p></td>
<td><p>4</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>6</p></td>
<td><p>4</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2000年亞洲盃足球賽.md" title="wikilink">2000年</a></p></td>
<td><p>八強</p></td>
<td><p>7</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>5</p></td>
<td><p>7</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2004年亞洲盃足球賽.md" title="wikilink">2004年</a></p></td>
<td><p>八強</p></td>
<td><p>8</p></td>
<td><p>4</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>5</p></td>
<td><p>7</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2007年亞洲盃足球賽.md" title="wikilink">2007年</a></p></td>
<td><p>冠軍</p></td>
<td><p>1</p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>7</p></td>
<td><p>2</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2011年亞洲盃足球賽.md" title="wikilink">2011年</a></p></td>
<td><p>八強</p></td>
<td><p>8</p></td>
<td><p>4</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>3</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2015年亞洲盃足球賽.md" title="wikilink">2015年</a></p></td>
<td><p>殿軍</p></td>
<td><p>4</p></td>
<td><p>6</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>3</p></td>
<td><p>8</p></td>
<td><p>9</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2019年亞洲盃足球賽.md" title="wikilink">2019年</a></p></td>
<td><p>十六強</p></td>
<td><p>-</p></td>
<td><p>4</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>6</p></td>
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p><strong>總結</strong></p></td>
<td><p>9/17</p></td>
<td><p>-</p></td>
<td><p>39</p></td>
<td><p>15</p></td>
<td><p>8</p></td>
<td><p>16</p></td>
<td><p>44</p></td>
<td><p>45</p></td>
</tr>
</tbody>
</table>

## 亞運會成績

  - 1951年 至 1970年 –*未有參與*
  - 1974年 – *首圈*
  - 1978年 – 第四
  - 1982年 –**金牌**
  - 1986年 – 八強
  - 1990年 至 2002年 - *未有參與*
  - [2006年](../Page/2006年亞洲運動會.md "wikilink") - **銀牌**

## 西亞國家盃成績

  - 2000年 – 季軍
  - 2002年 - **冠軍**
  - 2004年 – 殿軍
  - 2007年 - 亞軍

## 阿拉伯盃

  - 1963年 - *未有參與*
  - 1964年 - **冠軍**
  - 1966年 - **冠軍**
  - 1985年 - **冠軍**
  - 1988年 - **冠軍**
  - 1992年 至 2002年 - *未有參與*

## 資料來源

## 外部連結

  - [football-iraq.net](http://www.football-iraq.net)
  - [iraq-football.net](http://www.iraq-football.net)
  - [iraqfootball.org](http://www.iraqfootball.org)
  - [freewebs.com/Iraqfootball](https://web.archive.org/web/20071017004210/http://www.freewebs.com/iraqfootball/)
  - [National & International Iraqi Information of
    Soccer](http://www.niiis.com)
  - [Iraqi national team on
    FIFA.com](http://www.fifa.com/associations/association=irq/index.html)
  - [穆巴拉克Hassanin足球在伊拉克的博客](http://iraqsport.wordpress.com/)

[Category:亞洲足球代表隊](../Category/亞洲足球代表隊.md "wikilink")
[Category:伊拉克足球](../Category/伊拉克足球.md "wikilink")
[Iraq](../Category/亚洲杯足球赛冠军队伍.md "wikilink")

1.  [Under Uday, soccer was a game of life and
    death](http://www.theage.com.au/articles/2003/04/19/1050172797737.html)