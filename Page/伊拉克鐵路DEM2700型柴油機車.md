**DEM2700型**，是供[伊拉克鐵路使用的](../Page/伊拉克.md "wikilink")[柴油機車車種之一](../Page/柴油機車.md "wikilink")。

## 概要

這款機車由[中国](../Page/中国.md "wikilink")[大连机车製造](../Page/大连机车.md "wikilink")，為數50輛，中國廠方型號為**東風10FI型**（DF10FI）。2001年，伊拉克在經[联合国同意下](../Page/联合国.md "wikilink")，向[俄罗斯](../Page/俄罗斯.md "wikilink")、[法国及](../Page/法国.md "wikilink")[中国訂購](../Page/中国.md "wikilink")[鐵路機車](../Page/鐵路機車.md "wikilink")，其中大連廠先獲得30輛柴油機車訂單，至同年11月15日，大連廠獲伊拉克加訂20輛機車，使訂單數量增至50輛。

這50輛機車在設計上均作出改動，以適應當地炎熱、乾燥及大風沙的氣候。全數均於2002年年完成製造，並分批於大連港裝船運往伊拉克。

## 外部連結

  - [出口伊拉克内燃机车](http://www.dloco.com/ebusiness/GB/product_detail.asp?catalogid=17&productid=4)
  - [我国客货两用机车首次出口伊拉克](http://www.chinamet.com.cn/cn/meinfo/jmkx/details.jsp?id=27563)
  - [大连内燃机车大批量出口](http://news.xinhuanet.com/chanjing/2002-06/18/content_445542.htm)
  - [出口伊拉克内燃机车项目](http://www.cnrgc.com/LISTS/article/_MAINPAGE/gjmy/default3.aspx?id=1149)

[Category:伊拉克铁路](../Category/伊拉克铁路.md "wikilink")
[Category:柴油機車](../Category/柴油機車.md "wikilink")
[Category:中车大连制铁路机车](../Category/中车大连制铁路机车.md "wikilink")
[Category:Co-Co軸式機車](../Category/Co-Co軸式機車.md "wikilink")
[Category:中国出口柴油机车](../Category/中国出口柴油机车.md "wikilink")