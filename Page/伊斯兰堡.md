**伊斯蘭堡**（[乌尔都语](../Page/乌尔都语.md "wikilink")：，乌尔都语转写：）是[巴基斯坦的](../Page/巴基斯坦.md "wikilink")[首都](../Page/首都.md "wikilink")，位於該國的[伊斯兰堡首都区](../Page/伊斯兰堡首都区.md "wikilink")。伊斯兰堡在2011年有两百多万人口\[1\]，并与紧邻着的城市[拉瓦尔品第组成一个超過](../Page/拉瓦尔品第.md "wikilink")450万人口、位列全国第三的城市群\[2\]。自城市建立起来就吸引了来自全国的人口，使伊斯兰堡成为巴基斯坦最有世界性和高城市化率的城市。作为国家首都，巴基斯坦政府、巴基斯坦总统府（Aiwan-e-Sadr）驻地于此。巴基斯坦一座国家纪念碑也坐落于这里。伊斯兰堡也是大量外交官、政客和政府员工的接待处。伊斯兰堡的首都開發委員會（CDA）对该城市公共建设工程管理负责。

伊斯兰堡位于巴基斯坦东北的[波特瓦尔高原上](../Page/波特瓦尔高原.md "wikilink")，海拔540米，属[副热带湿润气候](../Page/副热带湿润气候.md "wikilink")。在历史上，这个地区处于一个前往[旁遮普和](../Page/旁遮普省.md "wikilink")[开伯尔-普什图的十字路口处](../Page/开伯尔-普什图省.md "wikilink")，而[马格拉关口则扮演前往两个地区的入口的角色](../Page/马格拉山.md "wikilink")\[3\]。伊斯兰堡修建于1960年代以取代当时的首都[卡拉奇](../Page/卡拉奇.md "wikilink")。现在伊斯兰堡是有秩序的国际城市，被认为是巴基斯坦最发达的城市，也被列为Gamma+级的[全球城市](../Page/全球城市.md "wikilink")\[4\]。但自21世纪初，伊斯兰堡多次遭到恐怖主义威胁。在2007年7月当地的[红色清真寺的一](../Page/红色清真寺.md "wikilink")[伊斯兰学校发生了](../Page/伊斯兰学校.md "wikilink")[伊斯兰原教旨主义武装分子和巴政府之间的武装冲突](../Page/伊斯兰恐怖主义.md "wikilink")、2008年6月2日的[丹麦驻巴基斯坦大使馆爆炸案以及同年](../Page/丹麦驻巴基斯坦大使馆爆炸案.md "wikilink")9月20日的[伊斯蘭瑪巴德萬豪酒店爆炸案](../Page/伊斯蘭瑪巴德萬豪酒店爆炸案.md "wikilink")。

南亚最大的[清真寺](../Page/清真寺.md "wikilink")\[5\]、世界第四大\[6\]\[7\]的清真寺[费萨尔清真寺坐落于伊斯兰堡](../Page/费萨尔清真寺.md "wikilink")。在伊斯兰堡有16所承认的大学，包括[伊斯兰堡空军大学](../Page/伊斯兰堡空军大学.md "wikilink")（AU）、[巴基斯坦真纳大学](../Page/巴基斯坦真纳大学.md "wikilink")（QAU）以及[巴基斯坦国立科技大学](../Page/巴基斯坦国立科技大学.md "wikilink")（NUST），其中[阿拉玛·伊克巴尔开放大学](../Page/阿拉玛·伊克巴尔开放大学.md "wikilink")（AIOU）按校友人數排名是世界第四大学校。

## 词源

“伊斯兰堡”（"Islamabad"）一词来自"Islam"和"abad"，其意为“伊斯兰的城市”。"*Islam*"是阿拉伯语单词指代[伊斯兰教这个宗教](../Page/伊斯兰教.md "wikilink")，"*-abad*"来自波斯语对地名的称呼，意为“有人居住的地方”或“城市”\[8\]。

## 历史

伊斯兰堡首都区及伊斯兰堡坐落的[波特瓦尔高原上的人类聚居遗迹被认为是](../Page/波特瓦尔高原.md "wikilink")[亚洲最早的人类遗迹之一](../Page/亚洲.md "wikilink")\[9\]。在高原上发现的一些早期[石器时代的遗迹可以追溯至](../Page/石器时代.md "wikilink")50万年前\[10\]，在[索安河流域台地发掘的遗迹为存在于](../Page/索安河.md "wikilink")[冰期的早期人类的活动迹象提供了证据](../Page/冰期.md "wikilink")。

巴基斯坦1947年建国时，首都位于国内南部的港口城市[卡拉奇](../Page/卡拉奇.md "wikilink")。伊斯兰堡因多种原因以首都的规格而展开建设。前巴基斯坦总统[阿尤布·汗想要让各地区经济平衡发展](../Page/阿尤布·汗.md "wikilink")，但传统上巴基斯坦的经济发展围绕着以卡拉奇为中心带有殖民痕迹的地域为重点。同时卡拉奇属于热带气候\[11\]，且位于全国的最南端处濒临[阿拉伯海](../Page/阿拉伯海.md "wikilink")，国防上考虑也不安全。对于巴基斯坦而言需要一座能与内地各区域联系便捷的城市\[12\]\[13\]。因而政府几经考虑迁都问题直到1959年2月才确定在波特瓦尔高原离拉瓦尔品第东北的地方建设一座新首都，取名为“伊斯兰堡”。1970年基本建成市区面积907平方公里，多现代化建筑，并具有传统的伊斯兰特色。分居民、行政、商业、使馆、工业、绿化等区，四边各有一条[高速公路](../Page/高速公路.md "wikilink")。行政区南侧是外交使館区；西侧是一条宽阔宪法大街，隔街相对的是公共事业区，内有国家银行、广播公司、电视台、电信局等单位。

2008年9月20日，發生[伊斯蘭瑪巴德萬豪酒店爆炸案](../Page/伊斯蘭瑪巴德萬豪酒店爆炸案.md "wikilink")。

[File:ISS007-E-13090.jpeg|伊斯兰堡空拍](File:ISS007-E-13090.jpeg%7C伊斯兰堡空拍)
[File:Islamabad_-_Pakistan_Monument_by_Night.JPG|巴基斯坦紀念碑](File:Islamabad_-_Pakistan_Monument_by_Night.JPG%7C巴基斯坦紀念碑)
[File:ZTBL_Headquarters.jpg|農業銀行](File:ZTBL_Headquarters.jpg%7C農業銀行)
[File:Ise_building2.png|證交所](File:Ise_building2.png%7C證交所)
[File:International_Islamic_University_in_Islamabad,_Pakistan.jpg|國際伊斯蘭大學](File:International_Islamic_University_in_Islamabad,_Pakistan.jpg%7C國際伊斯蘭大學)
[File:Jinnah_Market.jpg|商業區](File:Jinnah_Market.jpg%7C商業區)
[File:Ripples_video_store.jpg|影碟店販售有歐美和東亞戲劇](File:Ripples_video_store.jpg%7C影碟店販售有歐美和東亞戲劇)
|人馬座大樓 Girls in school in Khyber Pakhtunkhwa, Pakistan
(7295675962).jpg|西北方郊區高中

## 教育

世界第二大（依校友人數）的大学[阿拉玛·伊克巴尔开放大学](../Page/阿拉玛·伊克巴尔开放大学.md "wikilink")（）於此。另有中、小学100多所，高等院校有真纳大学、伊克巴尔开放大学、伊斯兰大学、国家现代语言学院等。

## 氣候

## 友好城市

  - [北京市](../Page/北京市.md "wikilink")

  - [安卡拉](../Page/安卡拉.md "wikilink")

  - [首爾](../Page/首爾.md "wikilink")

  - [馬德里](../Page/馬德里.md "wikilink")

  - [耶加達](../Page/耶加達.md "wikilink")

  - [阿布達比](../Page/阿布達比.md "wikilink")

  - [安曼](../Page/安曼.md "wikilink")

## 參考文獻

## 外部連結

  -
  -
  - \[//maps.google.com/maps?q=伊斯兰堡 谷歌地圖\]

[伊斯蘭堡](../Category/伊斯蘭堡.md "wikilink")
[Category:伊斯蘭堡首都區](../Category/伊斯蘭堡首都區.md "wikilink")
[Category:巴基斯坦省會](../Category/巴基斯坦省會.md "wikilink")
[Category:巴基斯坦城市](../Category/巴基斯坦城市.md "wikilink")
[Category:計劃首都](../Category/計劃首都.md "wikilink")
[Category:1960年代建立](../Category/1960年代建立.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.