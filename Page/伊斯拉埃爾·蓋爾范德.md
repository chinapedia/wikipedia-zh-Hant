**伊斯拉埃爾·莫伊塞耶維奇·蓋爾范德**（；；），出生在[烏克蘭的](../Page/烏克蘭.md "wikilink")[猶太裔科学家](../Page/猶太裔.md "wikilink")。他是[20世纪最伟大的](../Page/20世纪.md "wikilink")[数学家之一](../Page/数学家.md "wikilink")，同时也是[生物学家](../Page/生物学家.md "wikilink")、[教育家](../Page/教育家.md "wikilink")。他一生共发表了超过800篇论文，同时出版了30余部专著。他还是特别科学学校的首创者，通过他在[莫斯科大学办的讨论班](../Page/莫斯科大学办.md "wikilink")，几代学生从他这里得到知识，受到启发。更重要的是，他的学生们，例如[塞迈雷迪·安德烈](../Page/塞迈雷迪·安德烈.md "wikilink")、[Alexandre
Kirillov等](../Page/Alexandre_Kirillov.md "wikilink")，延续了他的方式。

## 生平

### 早期生涯

蓋爾范德出生于[德涅斯特河岸边的一个会计家庭](../Page/德涅斯特河.md "wikilink")。他先后在[意第绪语](../Page/意第绪语.md "wikilink")、俄语以及乌克兰语学校学习，很早就表现出对数学的特殊兴趣。1923年，全家搬到了[文尼察州](../Page/文尼察州.md "wikilink")，在这里，他录取到了一所化学职业学校，和同学David
Milman成为了朋友。

他專長于[泛函分析](../Page/泛函分析.md "wikilink")，是一位多產的數學家。他的主要工作有:

  - 伊斯拉埃爾·蓋爾范德-奈馬克定理
  - 孤立子理論（蓋爾范德-狄基方程）
  - [巴拿赫代数理論的蓋爾范德表示](../Page/巴拿赫代数.md "wikilink")
  - 複典型[李群的表示理論](../Page/李群.md "wikilink")
  - 無限維空間上的分布理論和[测度](../Page/测度.md "wikilink")
  - [常微分方程的蓋爾范德](../Page/常微分方程.md "wikilink")-列維坦理論

等等。

## 外部連結

  - [Gelfand
    summary](https://web.archive.org/web/20090109033431/http://www.gap-system.org/~history/Mathematicians/Gelfand.html)

  -
  -
[Category:烏克蘭犹太人](../Category/烏克蘭犹太人.md "wikilink")
[Category:俄国数学家](../Category/俄国数学家.md "wikilink")
[Category:烏克蘭数学家](../Category/烏克蘭数学家.md "wikilink")
[Category:20世纪数学家](../Category/20世纪数学家.md "wikilink")
[Category:21世纪数学家](../Category/21世纪数学家.md "wikilink")
[Category:沃尔夫数学奖得主](../Category/沃尔夫数学奖得主.md "wikilink")
[Category:法兰西科学院院士](../Category/法兰西科学院院士.md "wikilink")
[Category:莫斯科國立大學校友](../Category/莫斯科國立大學校友.md "wikilink")
[Category:苏联生物学家](../Category/苏联生物学家.md "wikilink")
[Category:京都奖获得者](../Category/京都奖获得者.md "wikilink")