[Italian_Front_1915-1917.jpg](https://zh.wikipedia.org/wiki/File:Italian_Front_1915-1917.jpg "fig:Italian_Front_1915-1917.jpg")
**伊松佐河戰役**指的是發生於1915年6月至1917年11月，在[第一次世界大戰](../Page/第一次世界大戰.md "wikilink")[義大利戰線東方的](../Page/義大利戰線_\(第一次世界大戰\).md "wikilink")[伊松佐河](../Page/伊松佐河.md "wikilink")（Isonzo，[斯洛維尼亞語稱](../Page/斯洛維尼亞語.md "wikilink")[索亞河](../Page/索亞河.md "wikilink")，Soča）發生的一系列戰役。

[伊松佐河河谷於第一次世界大戰時是義大利戰線的阿爾卑斯山戰線的一部分](../Page/伊松佐河.md "wikilink")，主要是[意大利王国與](../Page/意大利王國_\(1861年–1946年\).md "wikilink")[奧匈帝國交戰的地方](../Page/奧匈帝國.md "wikilink")，後來歷史學家都稱呼這裡為**伊松佐河戰線**（Isonzo
Front）或**索亞河戰線**（Soška fronta）。

## 各次戰役

  - [第一次伊松佐河之役](../Page/第一次伊松佐河之役.md "wikilink")，1915年6月23日至7月1日
  - [第二次伊松佐河之役](../Page/第二次伊松佐河之役.md "wikilink")，1915年7月18日至8月3日
  - [第三次伊松佐河之役](../Page/第三次伊松佐河之役.md "wikilink")，1915年10月18日至11月3日
  - [第四次伊松佐河之役](../Page/第四次伊松佐河之役.md "wikilink")，1915年11月10日至12月2日
  - [第五次伊松佐河之役](../Page/第五次伊松佐河之役.md "wikilink")，1916年3月9日至3月17日
  - [第六次伊松佐河之役](../Page/第六次伊松佐河之役.md "wikilink")，1916年8月6日至8月17日
  - [第七次伊松佐河之役](../Page/第七次伊松佐河之役.md "wikilink")，1916年9月14日至9月17日
  - [第八次伊松佐河之役](../Page/第八次伊松佐河之役.md "wikilink")，1916年10月10日至10月12日
  - [第九次伊松佐河之役](../Page/第九次伊松佐河之役.md "wikilink")，1916年11月1日至11月4日
  - [第十次伊松佐河之役](../Page/第十次伊松佐河之役.md "wikilink")，1917年5月12日至6月8日
  - [第十一次伊松佐河之役](../Page/第十一次伊松佐河之役.md "wikilink")，1917年8月19日至9月12日
  - [第十二次伊松佐河之役](../Page/第十二次伊松佐河之役.md "wikilink")，1917年10月24日至11月7日，又稱[卡波雷托戰役](../Page/卡波雷托戰役.md "wikilink")

## 結果與影響

[伊松佐河戰役](../Page/伊松佐河戰役.md "wikilink")，[意軍共](../Page/意軍.md "wikilink")[傷亡](../Page/傷亡.md "wikilink")100餘萬人，未達成[戰役目的](../Page/戰役.md "wikilink")，但[牽制了大量](../Page/牽制.md "wikilink")[同盟國軍隊](../Page/同盟國.md "wikilink")，有力支援了[協約國軍隊在](../Page/協約國.md "wikilink")[東西兩線的](../Page/東西兩線.md "wikilink")[作戰](../Page/作戰.md "wikilink")。

[\*](../Category/伊松佐河戰役.md "wikilink")
[Category:1910年代意大利](../Category/1910年代意大利.md "wikilink")
[Category:1910年代奥地利](../Category/1910年代奥地利.md "wikilink")