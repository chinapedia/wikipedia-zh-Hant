**伊澤多喜男**（），[台灣日治時期第](../Page/台灣日治時期.md "wikilink")10任[總督](../Page/台灣總督.md "wikilink")。

伊澤多喜男，日本[長野縣人](../Page/長野縣.md "wikilink")，1895年畢業於東京帝大政治科，高等文官及格。任[愛知縣等五縣參事官等職後](../Page/愛知縣.md "wikilink")，轉任警視廳部長。1907年起歷任[和歌山](../Page/和歌山縣.md "wikilink")、[愛媛](../Page/愛媛縣.md "wikilink")、[新潟縣縣知事](../Page/新潟縣.md "wikilink")（縣長），1914年任[警視總監](../Page/警視總監.md "wikilink")，1916年\~1941年敕選為[貴族院議員](../Page/貴族院.md "wikilink")。日本治台初期隨樺山總督抵台的學務部長[伊澤修二為其兄](../Page/伊澤修二.md "wikilink")。

伊澤多喜男婉拒入閣之請而要求台灣總督之職。1924年9月1日起任臺灣第10任總督，喊出「台灣統治之對象非十五萬之（日本）內地人，而為三百數十萬之本島住民」，讓台灣之菁英與智識份子頗為期待。任內大事：1924年[台灣青果株式會社成立](../Page/台灣青果株式會社.md "wikilink")，推展香蕉的外銷；1925年10月，[林本源製糖會社因甘蔗收購價格苛刻而與蔗農](../Page/林本源製糖會社.md "wikilink")（[李應章醫師成立之](../Page/李應章.md "wikilink")[二林蔗農組合](../Page/二林蔗農組合.md "wikilink")）衝突，引發[二林事件](../Page/二林事件.md "wikilink")；1926年6月28日[簡吉](../Page/簡吉.md "wikilink")、[趙港在鳳山成立全島](../Page/趙港.md "wikilink")[農民組合](../Page/農民組合.md "wikilink")；東部鐵路全線開通；為“[蓬萊米](../Page/蓬萊米.md "wikilink")”命名。任內在回東京養病時，被推舉為[東京市市長](../Page/東京市.md "wikilink")，1926年7月1日去總督乙職，從此長居日本而未再回台灣。

與[濱口雄幸有深交](../Page/濱口雄幸.md "wikilink")，被視為[立憲民政黨系的影武者](../Page/立憲民政黨.md "wikilink")，善權謀，儼然有勢於進步官僚之中。1934年[岡田啟介內閣成立時](../Page/岡田啟介.md "wikilink")，嘗與[後藤文夫等人試圖合組純官僚之內閣](../Page/後藤文夫.md "wikilink")。後任樞密顧問官。

有《伊澤多喜男》傳記；《伊澤多喜男關係文書》等文獻傳世。

## 参见

  - [台灣日治時期](../Page/台灣日治時期.md "wikilink")
  - [台灣總督](../Page/台灣總督.md "wikilink")

## 參考文獻

  - 伊沢多喜男伝記編纂委員会編『伊沢多喜男』羽田書店、1951年
  - 伊沢多喜男文書研究会編『伊沢多喜男関係文書』芙蓉書房出版、2000年　ISBN 4829502517
  - 大西比呂志編『伊沢多喜男と近代日本』芙蓉書房出版、2003年　ISBN 4829503327

[Category:貴族院敕選議員](../Category/貴族院敕選議員.md "wikilink")
[Category:新潟縣知事](../Category/新潟縣知事.md "wikilink")
[Category:愛媛縣知事](../Category/愛媛縣知事.md "wikilink")
[Category:和歌山縣知事](../Category/和歌山縣知事.md "wikilink")
[Category:臺灣總督](../Category/臺灣總督.md "wikilink")
[Category:東京市長](../Category/東京市長.md "wikilink")
[Category:東京大學校友](../Category/東京大學校友.md "wikilink")
[Category:長野縣出身人物](../Category/長野縣出身人物.md "wikilink")
[Category:日本樞密顧問官](../Category/日本樞密顧問官.md "wikilink")