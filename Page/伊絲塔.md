[Ishtar_vase_Louvre_AO17000-detail.jpg](https://zh.wikipedia.org/wiki/File:Ishtar_vase_Louvre_AO17000-detail.jpg "fig:Ishtar_vase_Louvre_AO17000-detail.jpg")
 **伊絲塔**（<sup>[D](../Page/DINGIR.md "wikilink")</sup>*IŠTAR*
[B010ellst.png](https://zh.wikipedia.org/wiki/File:B010ellst.png "fig:B010ellst.png")
[B153ellst.png](https://zh.wikipedia.org/wiki/File:B153ellst.png "fig:B153ellst.png")
；**Ishtar**，又譯作**伊什塔爾**、**伊西塔**）是[美索不達米亞宗教所崇奉的女神](../Page/美索不達米亞.md "wikilink")\[1\]，亦即是[蘇美爾人的女神](../Page/蘇美爾人.md "wikilink")[伊南娜和](../Page/伊南娜.md "wikilink")[閃米特人的女神](../Page/閃米特人.md "wikilink")[阿斯塔蒂](../Page/阿斯塔蒂.md "wikilink")。獅子是伊絲塔女神的象徵動物\[2\]。

伊絲塔原本就是一個雙面女神，既是豐饒與愛之神，同時也是戰爭女神，一般認為與[金星日夜不同的雙面性有關](../Page/金星.md "wikilink")。希臘羅馬神話中則以[雅典娜和](../Page/雅典娜.md "wikilink")[阿佛羅狄忒分別代表戰爭與愛這兩個面相](../Page/阿佛羅狄忒.md "wikilink")。

## 神話故事

### 伊絲塔下冥界

詳見[伊南娜](../Page/伊南娜.md "wikilink")

### 伊絲塔與吉爾伽美什

《[吉爾伽美什史詩](../Page/吉爾伽美什史詩.md "wikilink")》是美索不達米亞文化中最著名的史詩，講述半人半神的人類的國王吉爾迦美什追尋永生的過程。雖有許多版本，但是目前最完整的[楔形文字文獻版本是在](../Page/楔形文字.md "wikilink")[尼尼微城圖書館廢墟出土](../Page/尼尼微.md "wikilink")，內容是[亞述巴尼拔王在位時以](../Page/亞述巴尼拔.md "wikilink")[阿卡德語撰寫的](../Page/阿卡德.md "wikilink")。

吉爾伽美什生來天賦異稟，是[烏魯克之王](../Page/烏魯克.md "wikilink")，擁有2/3的神血統。由於他暴虐無道，天神派下長着兩隻大角，人頭獸身的[恩奇杜](../Page/恩奇杜.md "wikilink")（Enkidou/Enkidu）來威脅他。他來到烏魯克時，烏魯克的人民把他當成國王一樣崇敬，惹來了吉爾伽美什的嫉妒，因此和恩奇杜大打出手，兩人打得不分軒輊，結果是兩人不分勝敗並成為好友。兩人一起打死了可怕的山林守護神胡姆巴巴/胡瓦瓦（Humbaba/Huwawa）。

吉爾伽美什和恩奇杜打死胡姆巴巴後，吉爾伽美什在河裡清洗身體，此時他健美的英姿引起了烏魯克守護女神伊絲塔的青睞，伊絲塔便展開熱烈追求，但是被追求的吉爾伽美什不為所動，他還指責伊絲塔對歷任情人始亂終棄的淫亂行為（包括杜姆茲，巴比倫語稱塔姆茲）。為此伊絲塔非常憤怒，之後她還請父親安努降下天牛來懲罰吉爾伽美什，但那降下的天牛隨後卻被吉爾伽美什和恩奇杜兩人聯手殺死了。

殺死天牛沒多久，恩奇杜卻因殺死怪物後造成的精神衰弱死去。吉爾伽美什因恩奇杜的死去而產生感慨，開始四處尋找永生的方法，經過多時的尋找後，終於找到不死者烏塔那匹茲姆（Utnapishtim）。吉爾伽美什向烏塔那匹茲姆尋求永生的秘密，烏塔那匹茲姆答應了吉爾伽美什的請求，將他永生的祕密說給吉爾伽美什聽，於是他講述了[大洪水的故事](../Page/大洪水.md "wikilink")，聽完故事後吉爾伽美什瞭解到永生無法自行爭取，只得灰心地回到烏魯克。

## 資料來源

[I](../Category/美索不达米亚神话.md "wikilink")
[I](../Category/女神.md "wikilink") [I](../Category/戰神.md "wikilink")
[I](../Category/愛神.md "wikilink") [I](../Category/金星神.md "wikilink")
[I](../Category/西亞神祇.md "wikilink")

1.  [大英簡明百科](http://sc.hrd.gov.tw/ebintra/Content.asp?Query=&ContentID=11966)
2.  [東西方印章比較研究—以神話肖形印為例](http://webcache.googleusercontent.com/search?q=cache:sYyKOj_3OcMJ:cart.ntua.edu.tw/download.php%3Ffilename%3D1764_d92d7d5f.pdf%26dir%3Darchive%26title%3DFile+&cd=9&hl=zh-TW&ct=clnk&gl=tw)