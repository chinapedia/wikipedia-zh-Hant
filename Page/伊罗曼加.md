**伊罗曼加鎮**（）是位於[澳大利亞](../Page/澳大利亞.md "wikilink")[昆士蘭遠西南部的一個小鎮](../Page/昆士蘭州.md "wikilink")，這個城鎮位於一個名叫「伊罗曼加盆地」的早期[白垩纪的內陸海的邊緣](../Page/白垩纪.md "wikilink")。這個城鎮以盛產油井、[蛋白石](../Page/蛋白石.md "wikilink")\[1\]以及許多於1860年代來到並在此佔地的牛羊畜牧者的開拓而聞名，這個城鎮的名字可追溯回西元1860年左右。

## 流行文化

由於名稱Eromanga和日語「エロ漫画」的羅馬字相同，意思是[色情漫畫](../Page/色情漫畫.md "wikilink")，此地被多部漫畫引用，包括[草莓棉花糖](../Page/草莓棉花糖.md "wikilink")、[武士弗拉明戈](../Page/武士弗拉明戈.md "wikilink")、[情色漫畫老師](../Page/情色漫畫老師.md "wikilink")。

## 外部連結

[E](../Category/昆士蘭州城市.md "wikilink")
[E](../Category/昆士蘭州.md "wikilink")

1.  [昆士蘭州自然資源暨礦業廳（Department of Natural Resources and
    Mines）](http://www.nrm.qld.gov.au/property/placenames/)