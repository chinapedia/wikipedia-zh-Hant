《**伊莉莎白：輝煌年代**》（***Elizabeth: The Golden
Age***）是一部2007年的歷史電影，亦是《[-{zh-hk:傳奇女王伊利沙伯;
zh-tw:伊莉莎白;
zh-hans:伊丽莎白;}-](../Page/伊莉莎白_\(電影\).md "wikilink")》的[續集](../Page/續集.md "wikilink")。電影沿用了上一集的導演[錫哈·加培和包括](../Page/錫哈·加培.md "wikilink")[凱特·布蘭琪在內的一眾演員](../Page/凱特·布蘭琪.md "wikilink")。故事以16世紀的英國為背景，內容圍繞著已達中年的英女皇[伊丽莎白一世如何克服在統治國家及私人生活上都必須面對的種種壓力](../Page/伊丽莎白一世.md "wikilink")，籍此描繪英國在[伊丽莎白一世的統治下怎樣達到它的輝煌時期](../Page/伊丽莎白一世.md "wikilink")。

電影的全球首映於2007年9月9日在[多倫多國際電影節舉行](../Page/多倫多國際電影節.md "wikilink")。

## 演員表

  - [凯特·布兰切特](../Page/凯特·布兰切特.md "wikilink") 饰演
    [伊丽莎白一世](../Page/伊丽莎白一世_\(英格兰\).md "wikilink")
  - [克萊夫·歐文](../Page/克萊夫·歐文.md "wikilink") 饰演
    [沃爾特·雷利爵士](../Page/沃爾特·雷利.md "wikilink")
  - [傑佛瑞·羅許](../Page/傑佛瑞·羅許.md "wikilink") 饰演
    [法蘭西斯·華爾辛漢爵士](../Page/法蘭西斯·華爾辛漢.md "wikilink")
  - [艾比·柯妮許](../Page/:en:Abbie_Cornish.md "wikilink") 饰演
    [伊麗莎白·思羅克莫頓](../Page/:en:Elizabeth_Raleigh.md "wikilink")
  - [Jordi Molla](../Page/:en:Jordi_Molla.md "wikilink") 饰演
    [腓力二世](../Page/腓力二世_\(西班牙\).md "wikilink")
  - Coral Beed 饰演 宮廷女士
  - Christian Brassington 饰演
    [奧地利大公查理二世](../Page/:en:Charles_II,_Archduke_of_Austria.md "wikilink")
  - Robert Cambrinus 饰演 奧地利駐英大使
  - [Jazz Dhiman](../Page/:en:Jazz_Dhiman.md "wikilink") 饰演 西班牙貴族
  - [湯姆·荷蘭德](../Page/湯姆·荷蘭德.md "wikilink") 饰演
    [阿米愛斯·伯勒特爵士](../Page/:en:Amias_Paulet.md "wikilink")
  - David Legeno 饰演 劊子手
  - [Samantha Morton](../Page/:en:Samantha_Morton.md "wikilink") 饰演
    [瑪麗一世](../Page/瑪麗一世_\(蘇格蘭\).md "wikilink")
  - Luke Mowatt 饰演 [白廳宮禁衛](../Page/白廳宮.md "wikilink")
  - [埃迪·雷德梅尼](../Page/埃迪·雷德梅尼.md "wikilink") 饰演
    [安東尼·巴賓頓爵士](../Page/:en:Anthony_Babington.md "wikilink")
  - Vidal Sancho 饰演 西班牙大臣
  - Brice Stratford 饰演 女皇禁軍之首

## 外部連結

  - [官方網頁](http://www.elizabeththegoldenage.net)
  - [國際網頁](http://www.elizabeththegoldenage.com)
  - [環球製片](http://www.UniversalPictures.com)
  - [Working Title
    Films](https://web.archive.org/web/20070429181430/http://www.workingtitlefilms.com/film.php?filmID=102)
  - [YouTube](http://www.youtube.com/watch?v=vITxj7Tq4f4)

[Category:英國傳記片](../Category/英國傳記片.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:英國劇情片](../Category/英國劇情片.md "wikilink")
[Category:2007年電影](../Category/2007年電影.md "wikilink")
[Category:續集電影](../Category/續集電影.md "wikilink")
[Category:伊莉莎白一世題材電影](../Category/伊莉莎白一世題材電影.md "wikilink")
[Category:瑪麗一世題材電影](../Category/瑪麗一世題材電影.md "wikilink")
[Category:2000年代歷史電影](../Category/2000年代歷史電影.md "wikilink")
[Category:宮廷題材電影](../Category/宮廷題材電影.md "wikilink")
[Category:通道影業電影](../Category/通道影業電影.md "wikilink")