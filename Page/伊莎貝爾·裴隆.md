**玛丽亚·埃斯特拉·马丁内斯·卡塔斯·德·庇隆**（；），又称**伊萨贝尔·马丁内斯·德·庇隆**（Isabel Martínez de
Perón），[阿根廷](../Page/阿根廷.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")，她原为芭蕾舞剧团演员，后来成为胡安·多明戈·裴隆之妻。胡安·裴隆第三次当选为阿根廷总统时，伊萨贝尔担任副总统。丈夫去世后她接任了总统职务，成为[西半球第一个女性国家元首](../Page/西半球.md "wikilink")。\[1\]

## 早年经历

玛丽亚·庇隆出生于阿根廷[拉里奥哈的一个中下层之家](../Page/拉里奥哈_\(阿根廷\).md "wikilink")\[2\]，五年级时她就不得不辍学\[3\]。20世纪50年代她成为了夜总会的舞女，并且取了伊萨贝尔这个名字作为艺名。\[4\]伊萨贝尔原为塞万提斯国家芭蕾舞剧团演员，1956年在[巴拿马巡回演出时遇到头一年被政变推翻的前总统](../Page/巴拿马.md "wikilink")[胡安·多明戈·裴隆将军](../Page/胡安·多明戈·裴隆.md "wikilink")，遂随其流亡，1961年11月15日他们在西班牙结了婚，伊萨贝尔成为[胡安·裴隆的第三任妻子](../Page/胡安·裴隆.md "wikilink")\[5\]\[6\]。由于庇隆在政变中被推翻，他被禁止返回阿根廷，于是他的妻子成为了他的代理人\[7\]。1973年9月，庇隆夫妇作为正义党主席和第一副主席回国，搭档竞选获胜，庇隆再次成为总统，庇隆夫人成为第一位女副总统兼议长。

## 伊萨贝尔总统

1974年6月28日，胡安·貝隆的心脏多次发病。正在欧洲的伊萨贝尔被召回阿根廷，并在第二天秘密宣誓成为代理总统\[8\]。1974年7月1日，胡安因为支气管炎和流感逝世。之后时任副总统的她接任[总统](../Page/总统.md "wikilink")，就职时43岁，成为阿根廷[历史上第一位女总统](../Page/历史.md "wikilink")，同时也是拉丁美洲最年轻的国家元首，她同时也接任正义党主席\[9\]。伊萨贝尔上任后开始寻求国民的支持，一开始，就连之前和胡安·贝隆不合的极端政治团体都公开表示支持。不过后来伊萨贝尔取消了和选民以及政治团体之间的会面，国民对她的丧夫之痛也渐渐消退。她的政府开始清除左翼势力，之后又发生了一系列政治谋杀，1974年9月开始人们组织了一连串的罢工活动。从此民众们重新认识了伊萨贝尔，对她的支持也开始跌落。\[10\]

另外，伊萨贝尔政府中的社会福利部长也是民众反对的对象。此人审查各种国内外政策，几乎成了实际上的总理\[11\]。雷加还参与组建了（也称三A），这个准军事组织在1973年到1974年之间进行了大约300起谋杀活动。对象包括前总统[阿图罗·弗隆迪西的兄弟西尔维奥](../Page/阿图罗·弗隆迪西.md "wikilink")·阿图罗·弗隆迪西（Silvio
Frondizi）等人。\[12\]面对财政赤字，财政部长决定使用激进的休克疗法。他将比索贬值一半，5月和8月间的消费品价格翻倍，由此引发的抗议活动席卷了整个阿根廷。\[13\]

她选择向军方求助，任命[魏地拉为军队领导并允许军方自由处置破坏分子](../Page/魏地拉.md "wikilink")。为了避免政变，伊萨贝尔在1975年末提前进行了选举\[14\]\[15\]。1976年3月23日，工作到很晚的伊萨贝尔跟工作人员一起为自己的行政主管庆祝生日。她得到军方正在进行可疑活动的警告，于是登上了总统专用的直升机。可是这架直升机没有把她带到总统住宅，反而去了空军基地。在那里，伊萨贝尔被解除职务并遭到逮捕\[16\]。此后，亲贝隆的许多政府官员遭到逮捕，其中有些在随后的[肮脏战争中消失](../Page/肮脏战争_\(阿根廷\).md "wikilink")\[17\]。伊萨贝尔本人在[拉安戈斯圖拉鎮等地被关押了](../Page/拉安戈斯圖拉鎮.md "wikilink")5年，1981年她被流放至西班牙。1983年军人统治结束[民主政府允许她回国](../Page/民主.md "wikilink")，同年她返回阿根廷。1985年辞去正义党主席，退出政治\[18\]。之後居住在西班牙。\[19\]

## 阿根廷的国际通缉令

2007年1月11日，阿根廷[门多萨省法官对她发布了一份国际通缉令](../Page/门多萨省.md "wikilink")，罪名是她在1974年至1976年当政期间与一位名为埃克托尔·阿尔多·法杰蒂·加列戈的学生失踪案件有牵连，当时的贝隆总统签署了三项法令，允许采取包括军事手段在内的行动打击反政府行为。\[20\]不過，2008年3月28日阿根廷的引渡要求最終被[西班牙最高法院以证据不足为由拒絕](../Page/西班牙最高法院.md "wikilink")。而伊莎貝爾·贝隆及其[律师则认为当事人年纪太大不符合引渡条件](../Page/律师.md "wikilink")。\[21\]

## 參考文獻

[Category:阿根廷總統](../Category/阿根廷總統.md "wikilink")
[Category:阿根廷副總統](../Category/阿根廷副總統.md "wikilink")
[Category:阿根廷第一夫人](../Category/阿根廷第一夫人.md "wikilink")
[Category:阿根廷女性政治人物](../Category/阿根廷女性政治人物.md "wikilink")
[Category:女性總統](../Category/女性總統.md "wikilink")
[Category:女性政府首腦](../Category/女性政府首腦.md "wikilink")
[Category:冷戰時期領袖](../Category/冷戰時期領袖.md "wikilink")
[Category:被政變推翻的領導人](../Category/被政變推翻的領導人.md "wikilink")
[Category:阿根廷正義黨黨員](../Category/阿根廷正義黨黨員.md "wikilink")
[Category:藝人出身的政治人物](../Category/藝人出身的政治人物.md "wikilink")
[Category:阿根廷舞者](../Category/阿根廷舞者.md "wikilink")
[Category:在西班牙的阿根廷人](../Category/在西班牙的阿根廷人.md "wikilink")
[Category:西班牙裔阿根廷人](../Category/西班牙裔阿根廷人.md "wikilink")
[Category:拉里奧哈省人](../Category/拉里奧哈省人.md "wikilink")

1.  [Isabel Perón
    biography](http://www.biography.com/people/isabel-per%C3%B3n-39252).
    biography

2.  Binayán Carmona, Narciso. Maria Estela Martinez Cartas said one day:
    Zanga Cutiricutanga, that words were a tipic words in that
    years.*Historia genealógica Argentina*. EMECE, 1999, p.578.

3.

4.  Eloy Martínez, Tomás. *La Novela de Perón*. Random House, 1985.

5.

6.
7.  Page, Joseph. *Perón: A Biography*. Random House, 1983.

8.
9.  [1974: First female president for
    Argentina](http://news.bbc.co.uk/onthisday/hi/dates/stories/june/29/newsid_2857000/2857121.stm)

10. Crawley, Eduardo. *A House Divided*. St. Martin's Press, 1985.

11. *Encyclopædia Britannica, Book of the Year, 1976* (subject:
    Argentina).

12. Andersen, Martin. *Dossier Secreto*. Westview Press, 1993.

13. Lewis, Paul. *The Crisis of Argentine Capitalism*. University of
    North Carolina Press, 1990.

14.
15. [The night before the Argentine military coup 24 March 1976,
    according to
    Videla](http://en.mercopress.com/2011/03/21/the-night-before-the-argentine-military-coup-24-march-1976-according-to-videla)

16. Lewis, Paul.*Guerrillas and Generals*. University of North Carolina
    Press, 2002.

17. [Detienen en Valencia al ex dirigente de la Triple A argentina
    Almirón
    Sena](http://www.elmundo.es/elmundo/2006/12/28/internacional/1167325991.html),
    El Mundo, 28 \[2006-12-28\]

18. *Encyclopædia Britannica. Book of the Year, 1985:* Argentina.

19.
20. [阿根廷法官下令逮捕前总统伊萨贝尔·庇隆](http://news.163.com/08/0429/12/4AMRPLNV000120GU.html)

21. [西班牙拒绝引渡阿根廷前总统伊莎贝尔·庇隆](http://news.163.com/08/0429/12/4AMRPLNV000120GU.html)