**伊藤清**（，），[日本](../Page/日本.md "wikilink")[數學家](../Page/數學家.md "wikilink")，[日本學士院院士](../Page/日本學士院.md "wikilink")，生于[日本](../Page/日本.md "wikilink")[三重县](../Page/三重县.md "wikilink")[員辨郡](../Page/員辨郡.md "wikilink")（現[員辨市](../Page/員辨市.md "wikilink")）。西方文献中他的姓氏常写为Itô。

伊藤清研究[隨機過程](../Page/隨機過程.md "wikilink")，他在1944年和1946年的两份著作立下[隨機积分和](../Page/隨機积分.md "wikilink")[随机微分方程的理论基础](../Page/随机微分方程.md "wikilink")，所以他被视为[随机分析的创立者](../Page/随机分析.md "wikilink")。他的理论被應用于很多不同领域，包括[自然科学和](../Page/自然科学.md "wikilink")[經濟学](../Page/經濟学.md "wikilink")，例如[金融數學中](../Page/金融數學.md "wikilink")[期權定價用的](../Page/期權.md "wikilink")[布莱克-斯科尔斯模型](../Page/布莱克-斯科尔斯模型.md "wikilink")。[诺贝尔经济学奖获奖者](../Page/诺贝尔经济学奖.md "wikilink")[迈伦·斯科尔斯遇到伊藤时](../Page/迈伦·斯科尔斯.md "wikilink")，向他握手，称赞他的理论。从这个插曲可以明白伊藤的成就不仅对数学，对社会科学也带来很大影响。他是少有的在世的时候看到自己的理论研究被应用到现实生活中的数学家之一。

## 生平

他在[东京帝国大学开始学习数学](../Page/东京帝国大学.md "wikilink")，被概率论的微积分吸引。他感興趣於[安德雷·柯爾莫哥洛夫和](../Page/安德雷·柯爾莫哥洛夫.md "wikilink")[保羅·萊維的工作](../Page/保羅·皮埃爾·萊維.md "wikilink")，和後來[杜布的正規化概念](../Page/约瑟夫·利奧·杜布.md "wikilink")。

1940年他發表了《論緊群上的概率分佈》(*On the probability distribution on a compact
group*) 。在這門學科的發展中這是重要著作。

1945年他獲得博士學位。1945年後他感興趣於[隨機過程](../Page/隨機過程.md "wikilink")，特別是[布朗運動](../Page/布朗運動.md "wikilink")。

1952年他在[京都大學任教授](../Page/京都大學.md "wikilink")，講解他的概率論。之後有訪問[普林斯頓](../Page/普林斯頓.md "wikilink")、[孟買](../Page/孟買.md "wikilink")、[奧胡斯大學和](../Page/奧胡斯大學.md "wikilink")[康奈爾大學](../Page/康奈爾大學.md "wikilink")，直到1979年退休。

伊藤清的工作獲得許多肯定，他的獲獎包括1987年的[沃爾夫獎和](../Page/沃爾夫獎.md "wikilink")1998年的[京都獎](../Page/京都獎.md "wikilink")。2006年他獲授予第一個[高斯獎](../Page/高斯獎.md "wikilink")。2008年11月18日因呼吸衰竭病逝於[京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[左京區](../Page/左京區.md "wikilink")，終年93歲。

他把數學和美麗的形式連繫起來。在一文中引用[莫扎特的音樂和](../Page/莫扎特.md "wikilink")[科隆大教堂](../Page/科隆大教堂.md "wikilink")，他說這些都啟發他創造他的公式：「比如莫扎特的音樂連那些不懂樂理的人也被深刻感染；科隆大教堂使不懂基督教的遊客也為之驚歎。但是，數學構造之美，對表達邏輯法則的數值公式不明白的話，卻不能欣賞到。」

[伊藤引理是他以邏輯法則創造的精華](../Page/伊藤引理.md "wikilink")。

因他而名還有[伊藤過程](../Page/伊藤過程.md "wikilink")（又稱廣義[維納過程](../Page/維納過程.md "wikilink")）、[伊藤公式和](../Page/伊藤引理.md "wikilink")[伊藤微積分](../Page/伊藤微積分.md "wikilink")。

## 參見

  - [日本人沃爾夫獎得主](../Page/日本人沃爾夫獎得主.md "wikilink")

[Category:日本数学家](../Category/日本数学家.md "wikilink")
[Category:20世纪数学家](../Category/20世纪数学家.md "wikilink")
[Category:三重縣出身人物](../Category/三重縣出身人物.md "wikilink")
[Category:沃尔夫数学奖得主](../Category/沃尔夫数学奖得主.md "wikilink")
[Category:法兰西科学院院士](../Category/法兰西科学院院士.md "wikilink")
[Category:文化勳章獲得者](../Category/文化勳章獲得者.md "wikilink")
[Category:東京大學校友](../Category/東京大學校友.md "wikilink")
[Category:卡尔·弗里德里希·高斯奖获得者](../Category/卡尔·弗里德里希·高斯奖获得者.md "wikilink")
[Category:京都奖获得者](../Category/京都奖获得者.md "wikilink")