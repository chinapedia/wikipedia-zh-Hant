**伊達忠宗**（）是[江戶時代初期的](../Page/江戶時代.md "wikilink")[陸奥](../Page/陸奥.md "wikilink")[仙台藩第](../Page/仙台藩.md "wikilink")2代藩主。[伊達政宗次男](../Page/伊達政宗.md "wikilink")，正室是[池田輝政之女](../Page/池田輝政.md "wikilink")[振姬](../Page/振姬.md "wikilink")。因為有鞏固仙台藩地位的功績而被評為「守成的名君」（）。

## 生平

[慶長](../Page/慶長.md "wikilink")12年（1607年），與此年出生的[德川家康的五女](../Page/德川家康.md "wikilink")[市姬成立婚約](../Page/市姬.md "wikilink")，但是市姬在3年後死去，於是以[池田輝政的女兒](../Page/池田輝政.md "wikilink")[振姬](../Page/振姬.md "wikilink")（家康的孫女）成為[德川秀忠的養女並嫁給忠宗](../Page/德川秀忠.md "wikilink")。身為庶長子的兄長[秀宗在慶長](../Page/伊達秀宗.md "wikilink")19年（1614年）的[大坂冬之陣與父親一同参戰](../Page/大坂冬之陣.md "wikilink")，在戰後被[大御所德川家康賜予](../Page/大御所.md "wikilink")[伊予宇和島](../Page/伊予.md "wikilink")10萬石而成為別家，於是忠宗被定為伊達宗家的後繼者。

[寬永](../Page/寬永.md "wikilink")13年（1636年）5月，因為父親政宗死去而繼任[家督](../Page/家督.md "wikilink")。忠宗在同年8月成為藩主，雖然是初次接觸藩政，但是馬上就決定藩政的執行體制。首先令其他藩的[家老](../Page/家老.md "wikilink")‧[奉行](../Page/奉行.md "wikilink")6人中的[石母田宗賴](../Page/石母田宗賴.md "wikilink")、[中島意成](../Page/中島意成.md "wikilink")、[茂庭良綱](../Page/茂庭良綱.md "wikilink")、[奥山常良](../Page/奥山常良.md "wikilink")4人留任，為了替代[津田景康](../Page/津田景康.md "wikilink")、[遠藤玄信而新加入](../Page/遠藤玄信.md "wikilink")[津田賴康](../Page/津田賴康.md "wikilink")（景康的兒子）、[古内重廣](../Page/古内重廣.md "wikilink")。更把一直以來都是單任制‧負責指導和監督奉行的[評定役改為多人制](../Page/評定役.md "wikilink")，於是把役職的内容變成奉行的補助機關，並任命津田景康、遠藤玄信、[片倉重綱](../Page/片倉重綱.md "wikilink")、[古內義重](../Page/古內義重.md "wikilink")、[鴇田周如](../Page/鴇田周如.md "wikilink")5人（「評定役」在後來成為進行裁判的評定所負責人的役職名）。此外還任命了7名負責監察的「目付役」，在翌年制定包括於家中禁止私自處罰等條項的法度，強化了藩內的統制。

在財政方面，於寬永17年（1640年）至寬永20年（1643年）實施「寬永總[檢地](../Page/檢地.md "wikilink")」。此時把仙台藩的計算方法（1反＝360歩）改為全國標準（1反＝300歩）（[二割出目](../Page/二割出目.md "wikilink")），把[貫高制的換算基準固定](../Page/貫高制.md "wikilink")（1貫＝10石）來與[石高制對等](../Page/石高制.md "wikilink")，更以檢地的結果來重新分配家臣團的知行地。還把領內余剩的[米由藩買進並運到](../Page/米.md "wikilink")[江戶售賣](../Page/江戶.md "wikilink")（被稱為[買米制](../Page/買米制.md "wikilink")）。在忠宗一代因為買米制是非強制性，所以對[農民來說是非常有益](../Page/農民.md "wikilink")，買米代金被稱為「御惠金」，此舉還成為了促進開發新田的原動力。

在寬永16年（1639年）為了使用政龐而在[仙台城建造二之丸](../Page/仙台城.md "wikilink")，亦對寺社進行大幅度擴建。在寬永14年（1637年）為了祭祀政宗而建立[瑞鳳殿和](../Page/瑞鳳殿.md "wikilink")[瑞鳳寺](../Page/瑞鳳寺.md "wikilink")，在寛永17年建築[白山神社的社殿](../Page/白山神社.md "wikilink")。在寬永20年創建[滿福寺](../Page/滿福寺.md "wikilink")，在[慶安](../Page/慶安.md "wikilink")2年（1649年）再建因為火災而燒燬的[孝勝寺](../Page/孝勝寺.md "wikilink")，在翌年（1650年）建立[愛宕神社](../Page/愛宕神社.md "wikilink")，在[承應](../Page/承應.md "wikilink")3年（1654年）以6年時間在[東照宮勸請和遷宮](../Page/東照宮.md "wikilink")（[仙台東照宮](../Page/仙台東照宮.md "wikilink")）。在承應4年（1655年）於仙台東照宮進行祭禮，在仙台進行祭禮成為現在的[仙台青葉祭的起源](../Page/仙台青葉祭.md "wikilink")。

在[萬治元年](../Page/萬治.md "wikilink")（1658年）7月12日死去，享年60歲。古内重廣等人殉死。因為嫡男[光宗在](../Page/伊達光宗.md "wikilink")[正保](../Page/正保.md "wikilink")2年（1645年）死去，[家督由六男](../Page/家督.md "wikilink")[綱宗繼承](../Page/伊達綱宗.md "wikilink")。三男[田村宗良因為母親](../Page/田村宗良.md "wikilink")[愛姬的遺言而再興](../Page/愛姬.md "wikilink")[田村氏](../Page/田村氏.md "wikilink")，在第4代藩主[綱村一代把](../Page/伊達綱村.md "wikilink")[岩沼藩分知而成為](../Page/岩沼藩.md "wikilink")[大名](../Page/大名.md "wikilink")。

## 官職位階履歷

※日期＝日本舊曆

  - [慶長](../Page/慶長.md "wikilink")16年（1611年）12月13日：[元服](../Page/元服.md "wikilink")，由將軍[德川秀忠下賜一字而命名](../Page/德川秀忠.md "wikilink")「忠宗」，敘任[正五位下](../Page/正五位下.md "wikilink")[美作守](../Page/美作守.md "wikilink")。
  - [元和](../Page/元和.md "wikilink")2年（1616年）10月2日：昇敘[從四位下](../Page/從四位下.md "wikilink")，兼任[侍從](../Page/侍從.md "wikilink")。
  - [寬永元年](../Page/寬永.md "wikilink")（1624年）6月23日：轉任[越前守](../Page/越前守.md "wikilink")，侍從職不變。
  - 寬永3年（1626年）8月19日：轉任[左近衛權少將](../Page/左近衛權少將.md "wikilink")，越前守不變。
  - 寬永13年（1636年）7月：成為[陸奧國](../Page/陸奧國.md "wikilink")[仙台藩藩主](../Page/仙台藩.md "wikilink")。
  - 寬永16年（1639年）4月14日：兼任[陸奧守](../Page/陸奧守.md "wikilink")，越前守改變。

## 登場作品

  - 電視劇

<!-- end list -->

  - 『』（演員：[林成年](../Page/林成年.md "wikilink")）
  - 『[獨眼龍政宗](../Page/獨眼龍政宗.md "wikilink")』（演員：[小林正幸](../Page/小林正幸.md "wikilink")→[野村宏伸](../Page/野村宏伸.md "wikilink")）
  - 『[葵德川三代](../Page/葵德川三代.md "wikilink")』（演員：[池田幹](../Page/池田幹.md "wikilink")）

|-style="text-align: center; background: \#FFE4E1;" |align="center"
colspan="3"|**伊達忠宗**  |-

## 相關條目

  - [伊達氏](../Page/伊達氏.md "wikilink")

[Category:伊達氏](../Category/伊達氏.md "wikilink")
[Category:仙台藩](../Category/仙台藩.md "wikilink")
[Category:外樣大名](../Category/外樣大名.md "wikilink")