**伊達爾戈县**（）是位於[美國](../Page/美國.md "wikilink")[德克薩斯州南部的一個縣](../Page/德克薩斯州.md "wikilink")，[格蘭德河在南界](../Page/格蘭德河.md "wikilink")（同時是美國和[墨西哥的邊界](../Page/墨西哥.md "wikilink")）流過。面積4,099平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口569,463人。縣治[愛丁堡](../Page/愛丁堡_\(德克薩斯州\).md "wikilink")（）。

成立於1852年。縣名紀念[墨西哥獨立之父](../Page/墨西哥獨立.md "wikilink")[米格爾·伊達爾戈](../Page/米格爾·伊達爾戈.md "wikilink")。


[H](../Category/得克萨斯州行政区划.md "wikilink")