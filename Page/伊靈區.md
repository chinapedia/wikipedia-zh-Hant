**伊靈區**（，讀音：）是[英國](../Page/英國.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")[大倫敦](../Page/大倫敦.md "wikilink")[外倫敦的](../Page/外倫敦.md "wikilink")[自治市](../Page/倫敦自治市.md "wikilink")，[人口](../Page/人口.md "wikilink")
306,400，面積55.53[平方公里](../Page/平方公里.md "wikilink")。

## 友好城市

  - [比拉尼區](../Page/比拉尼區.md "wikilink")

  - [马尔康巴勒尔](../Page/马尔康巴勒尔.md "wikilink")

  - [施泰因富特县](../Page/施泰因富特县.md "wikilink")

## 註釋

## 外部連結

  - [伊靈區政府網](http://www.ealing.gov.uk)

[Category:大倫敦](../Category/大倫敦.md "wikilink")