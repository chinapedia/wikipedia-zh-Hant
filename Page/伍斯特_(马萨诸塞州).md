**伍斯特**（ ，本地
）\[1\]位于[美国](../Page/美国.md "wikilink")[马萨诸塞州中部](../Page/马萨诸塞州.md "wikilink")，是[伍斯特县的县治所在](../Page/伍斯特县_\(马萨诸塞州\).md "wikilink")，人口约18万（2010年）\[2\]，是[新英格蘭地區第二大城市](../Page/新英格蘭.md "wikilink")，僅次於[波士頓](../Page/波士頓.md "wikilink")\[3\]。它位於波士頓以西，[普羅維登斯以北](../Page/普罗维登斯_\(罗德岛州\).md "wikilink")。城市里保留有許多[維多利亞式建築](../Page/維多利亞式建築.md "wikilink")。

## 歷史

這片地區最早的居民為[尼普穆克族](../Page/尼普穆克族.md "wikilink")，他們稱此地為“Quinsigamond”，聚居地在[奧伯恩的Pakachoag](../Page/奧伯恩_\(馬薩諸塞州\).md "wikilink")
Hill附近\[4\] 。1673年英格蘭殖民者約翰·艾略特（John Eliot）和丹尼爾·古金（Daniel
Gookin）開始計劃在這裡建立殖民地，次年7月13日尼普穆克族答應了他們的要求，給予了他們八平方英里的土地。\[5\]

1675年尼普穆克族和殖民者之間爆發[菲利普国王战争](../Page/菲利普国王战争.md "wikilink")，最終殖民者離開了這裡，尼普穆克族燒掉了他們留下的建築\[6\]。1713年[喬納斯·賴斯再次在此定居](../Page/喬納斯·賴斯.md "wikilink")\[7\]，1722年6月14日此地以英格蘭的[伍斯特為名正式建立政府](../Page/伍斯特.md "wikilink")\[8\]。1731年4月2日成為新成立的伍斯特郡的郡治。

## 地理与气候

伍斯特属温带[大陆性气候](../Page/大陆性气候.md "wikilink")，四季分明，降雨与日照充足。冬季寒冷，微潮，日最高气温低于或等于的平均日数为47天，日最低气温低于或等于的平均日数为32天，低于或等于的平均日数为10.0天；夏季相对炎热潮湿，日最高气温超过的日数年均有13天，以上的气温为罕见。\[9\]最冷月（1月）均温，极端最低气温（1943年2月16日）。最热月（7月）均温，极端最高气温（1911年7月4日）。无霜期平均为176天（4月23日至10月15日）；可测量降雪平均期为11月15日至4月4日。\[10\]年均降水量约，年极端最少降水量为（1941年），最多为（1972年）。\[11\]年均降雪量为；1954–55年的降雪量最少，积累降雪量只有，1992–93年的降雪量最多，积累降雪量为。\[12\]\[13\]

## 教育

[wpi_boytonhall.JPG](https://zh.wikipedia.org/wiki/File:wpi_boytonhall.JPG "fig:wpi_boytonhall.JPG")\]\]
[Worcesteracademy3.jpg](https://zh.wikipedia.org/wiki/File:Worcesteracademy3.jpg "fig:Worcesteracademy3.jpg")\]\]

伍斯特有多所大學，包括：

  - [聖母升天學院](../Page/聖母升天學院.md "wikilink")：1904年建立的羅馬天主教學院
  - [貝克學院](../Page/貝克學院.md "wikilink")：1784年成立於[萊斯特](../Page/萊斯特_\(馬薩諸塞州\).md "wikilink")，1887年在伍斯特設立校區\[14\]
  - [克拉克大學](../Page/克拉克大學.md "wikilink")，1887年建立的大學
  - [圣十字學院](../Page/圣十字學院.md "wikilink")，1843年建立的天主教學院，是新英格蘭地區最古老的天主教學院，現在是美國最好的文理學院之一\[15\]
  - [麻省藥科與健康科學學院](../Page/麻省藥科與健康科學學院.md "wikilink")，1823年建立的私立學院
  - [昆西加蒙湖社区学院](../Page/昆西加蒙湖社区学院.md "wikilink")，1963年成立的社區學院
  - [馬薩諸塞州大學醫學院](../Page/馬薩諸塞州大學醫學院.md "wikilink")，1970年建立的醫學院
  - [伍斯特理工學院](../Page/伍斯特理工學院.md "wikilink")，1865年建立的理工學院
  - [伍斯特州立大學](../Page/伍斯特州立大學.md "wikilink")，1874年建立的州立大學

## 参考文献

## 外部鏈接

  - [官方網站](http://www.worcesterma.gov/)
  - [公共圖書館](http://www.worcpublib.org/)
  - [伍斯特歷史博物館](http://www.worcesterhistory.org/)
  - [Preservation
    Worcester](https://web.archive.org/web/20090512101145/http://www.preservationworcester.org/pages/education.html)
  - [Official Worcester visitors
    website](http://www.destinationworcester.org/)
  - [Worcester Regional Chamber of
    Commerce](http://www.worcesterchamber.org/)
  - [Worcester Regional Research Bureau](http://www.wrrb.org/)
  - [Worcester Public Schools official website](http://www.wpsweb.com/)
  - [Colleges of Worcester Consortium](http://www.cowc.org/)
  - [Worcester Business Directory](http://www.directoryofworcester.com/)
  - [91.3FM WCUW - Worcester's Community Radio
    Station](http://www.wcuw.org/)
  - [UMass Memorial's EMS Service for the City of
    Worcester](http://www.worcesterems.com/)
  - [2011 Crash Report for Worcester,
    MA](http://www.peterventuralaw.com/wp-content/uploads/2013/11/20131107-Worcester-Accidents.jpg)

[W](../Category/马萨诸塞州城市.md "wikilink")

1.

2.

3.  The third largest city is [Providence, Rhode
    Island](../Page/Providence,_Rhode_Island.md "wikilink"), with a
    population of 178,042.

4.  Lincoln, William (1862). *History of Worcester, Massachusetts*, pp.
    22-23. Worcester: Charles Hersey.

5.

6.
7.  Worcester Society of Antiquity (1903). *Exercises Held at the
    Dedication of a Memorial to Major Jonas Rice, the First Permanent
    Settler of Worcester, Massachusetts, Wednesday, October 7, 1903.*
    Charles Hamilton Press, Worcester. 72pp.

8.

9.
10.
11.
12.
13.

14.

15.