{{ redirect2|斯大林格勒|同名的俄羅斯電影|斯大林格勒 (2013年电影)}}
**伏尔加格勒**（，[羅馬化](../Page/羅馬化.md "wikilink")：）是[俄羅斯南部](../Page/俄羅斯.md "wikilink")[伏爾加格勒州的](../Page/伏爾加格勒州.md "wikilink")[都市](../Page/都市.md "wikilink")，也是伏爾加格勒州的[首府](../Page/首府.md "wikilink")。該城始建于1589年，原名**察里津**（），1925年4月10日改称**斯大林格勒**（），1961年改為現名。2013年1月31日，伏尔加格勒市[杜马通过决议](../Page/杜马.md "wikilink")，在每年的8月23日（[苏德互不侵犯条约签署日](../Page/苏德互不侵犯条约.md "wikilink")）、2月2日（[斯大林格勒保卫战胜利日](../Page/斯大林格勒保卫战.md "wikilink")）、5月9日（[纳粹德国投降日和](../Page/德国投降.md "wikilink")[歐戰勝利紀念日](../Page/歐戰勝利紀念日.md "wikilink")）、9月2日（[日本投降日](../Page/日本投降.md "wikilink")）、6月22日（苏德战争的[巴巴罗萨作战开始日](../Page/巴巴罗萨作战.md "wikilink")）和11月19日（斯大林格勒保卫战的[天王星作战开始日](../Page/天王星作战.md "wikilink")）这六个纪念日里将市名改为斯大林格勒。

## 历史

[Volgograd_de_insulo.jpg](https://zh.wikipedia.org/wiki/File:Volgograd_de_insulo.jpg "fig:Volgograd_de_insulo.jpg")
这里是[乌兹别克汗的首都](../Page/乌兹别克汗.md "wikilink")。1589年，[沙皇俄国在](../Page/沙皇俄国.md "wikilink")[察里津河和](../Page/察里津河.md "wikilink")[伏尔加河的交汇处的一个岛上建造了察里津要塞](../Page/伏尔加河.md "wikilink")。察里津是[鞑靼语的名称](../Page/鞑靼语.md "wikilink")。在1670年和1774年察里津两次分别被和[普加乔夫起义军攻陷](../Page/普加乔夫起义.md "wikilink")。到19世纪，这里已经是伏尔加河上的重要港口和商业中心：全俄罗斯第三大[铁路枢纽](../Page/铁路.md "wikilink")，[木材](../Page/木材.md "wikilink")、[盐](../Page/盐.md "wikilink")、[石油的集散地](../Page/石油.md "wikilink")，伏尔加河下游最大的商业和工业城市。

在1917年11月7日—1922年10月的[俄国内战时期](../Page/俄国内战.md "wikilink")，这里发生了[察里津战役](../Page/察里津战役.md "wikilink")（1918年7月—1919年2月，又称[察里津保卫战](../Page/察里津保卫战.md "wikilink")）。[斯大林](../Page/斯大林.md "wikilink")（本姓朱加什维利）时任北高加索军区军事委员会主席。斯大林、[伏罗希洛夫等人在此指挥](../Page/伏罗希洛夫.md "wikilink")[苏联红军成功地抵抗了](../Page/苏联红军.md "wikilink")[邓尼金的](../Page/邓尼金.md "wikilink")[白军的进攻](../Page/白军.md "wikilink")。1919年6月30日白军最终占领了这座城市。1920年1月又被红军收回，并在1925年被命名为斯大林格勒。在斯大林执政时期，这里成了苏联重要的[重工业中心](../Page/重工业.md "wikilink")，[粮食](../Page/粮食.md "wikilink")、[石油和](../Page/石油.md "wikilink")[煤炭的主要产区](../Page/煤炭.md "wikilink")，和[铁道](../Page/铁道.md "wikilink")、伏尔加河运集散中心。北高加索的粮食，[巴库的石油](../Page/巴库.md "wikilink")，[中亚的](../Page/中亚.md "wikilink")[棉花](../Page/棉花.md "wikilink")，都要通过这一工业铁路枢纽和伏尔加河的港口，才能提供给苏联的中心地区。

[苏德战争](../Page/苏德战争.md "wikilink")（[蓝色行动](../Page/蓝色行动.md "wikilink")）期间，这里是著名[蓝色行动](../Page/蓝色行动.md "wikilink")（1942年6月28日—11月18日）以及包括[天王星行动](../Page/天王星行动.md "wikilink")（1942年11月19日—11月22日）、[土星行动](../Page/土星行动.md "wikilink")（1942年12月12日—1943年2月19日）（总共是1942年6月28日—1943年2月19日）的[斯大林格勒保卫战的战场](../Page/斯大林格勒战役.md "wikilink")。[纳粹德国在](../Page/纳粹德国.md "wikilink")1941年7月7日—9月26日的[基辅防御战役之后占领左岸](../Page/基辅战役.md "wikilink")[乌克兰](../Page/乌克兰.md "wikilink")，之后俄罗斯南部的斯大林格勒成了苏联中央地区通向南方重要经济区域的咽喉，战略地位十分重要，加上这座城市是以当时苏联的最高领袖命名的，因此斯大林格勒的得失对战局和人心都会产生巨大的影响。战争中24万余纳粹德国德意志国防军陆军及其罗马尼亚王国盟友的士兵和47.87万人苏联红军阵亡，平民伤亡不计其数。斯大林格勒全市90%的建筑被毁，但纳粹德国国防军陆军只是占领了这里二个月。1943年3月之后苏联迅速重建斯大林格勒。1945年斯大林格勒和其他三座城市（[列宁格勒](../Page/列宁格勒.md "wikilink")、[塞瓦斯托波尔和](../Page/塞瓦斯托波尔.md "wikilink")[敖德萨](../Page/敖德萨.md "wikilink")）成为第一批“英雄城市”。

1961年因[赫鲁晓夫](../Page/赫鲁晓夫.md "wikilink")“反对斯大林个人崇拜”这里被更名为“伏尔加格勒”。此后至今不时有人提议把它的名字改回斯大林格勒。在[契尔年科执政的](../Page/康斯坦丁·乌斯季诺维奇·契尔年科.md "wikilink")1985年改名几乎成功。2005年4月16日[俄罗斯联邦共产党中央委员会主席](../Page/俄罗斯联邦共产党.md "wikilink")[久加诺夫说](../Page/久加诺夫.md "wikilink")“我们应该积极支持前线老兵的呼吁，将伏尔加格勒改回原名斯大林格勒。”

2013年1月31日，伏尔加格勒市杜马通过一项决议，在每年的8月23日（苏德互不侵犯条约签署日）、2月2日（斯大林格勒保卫战胜利日）、5月9日（纳粹德国无条件投降日和二战欧洲战场胜利日）、9月2日（对日作战胜利纪念日）、6月22日（苏德战争的巴巴罗萨作战开始日）和11月19日（斯大林格勒保卫战的天王星作战开始日）这六个纪念日裡将市名改为斯大林格勒。\[1\]

## 地理

[Volgograd_oficeja_konstruajho.jpg](https://zh.wikipedia.org/wiki/File:Volgograd_oficeja_konstruajho.jpg "fig:Volgograd_oficeja_konstruajho.jpg")

伏尔加格勒位于俄罗斯联邦西南部的[北高加索地区](../Page/北高加索.md "wikilink")，[伏尔加河下游](../Page/伏尔加河.md "wikilink")，[顿河大弯曲部以东约](../Page/顿河.md "wikilink")60公里处。城区沿河岸绵延约100公里。伏尔加格勒面积565[平方公里](../Page/平方公里.md "wikilink")。

伏尔加格勒是一个俄国南方城市，平均气温[夏季](../Page/夏季.md "wikilink")26度，[冬季零下](../Page/冬季.md "wikilink")9度。气候属[温带大陆性湿润气候](../Page/温带大陆性湿润气候.md "wikilink")，风景秀丽，气候宜人。

## 行政区划

伏尔加格勒市是[伏尔加格勒州的州府](../Page/伏尔加格勒州.md "wikilink")。
[Volgograd_centro.jpg](https://zh.wikipedia.org/wiki/File:Volgograd_centro.jpg "fig:Volgograd_centro.jpg")

伏尔加格勒市内有8个区：

  -
  -
  -
  -
  -
  -
  -
  -
## 人口

[Volgograd_nova_distrikto.jpg](https://zh.wikipedia.org/wiki/File:Volgograd_nova_distrikto.jpg "fig:Volgograd_nova_distrikto.jpg")

截至2010年，统计人口1,021,215，其中近90%是[俄罗斯人](../Page/俄罗斯.md "wikilink")，其他人口较多的少数民族有：[乌克兰人](../Page/乌克兰.md "wikilink")、[喀山人](../Page/喀山.md "wikilink")、[日耳曼人](../Page/日耳曼.md "wikilink")、[鞑靼人等](../Page/鞑靼.md "wikilink")。

## 交通经济

[Volgograd_trolebuso_3.jpg](https://zh.wikipedia.org/wiki/File:Volgograd_trolebuso_3.jpg "fig:Volgograd_trolebuso_3.jpg")
伏尔加格勒位于连接俄国南方两大水系伏尔加河和[顿河的](../Page/顿河_\(俄羅斯\).md "wikilink")[伏爾加-頓河運河的东端](../Page/伏爾加-頓河運河.md "wikilink")，有铁路和[莫斯科](../Page/莫斯科.md "wikilink")（1073公里。）、[乌克兰](../Page/乌克兰.md "wikilink")、[高加索](../Page/高加索.md "wikilink")、[西伯利亚等地区相连](../Page/西伯利亚.md "wikilink")。今天的伏尔加格勒的主要工业包括：[造船](../Page/造船.md "wikilink")、[石化](../Page/石化.md "wikilink")、[钢铁](../Page/钢铁.md "wikilink")、[铝材](../Page/铝材.md "wikilink")、[机械](../Page/机械.md "wikilink")、[汽车和](../Page/汽车.md "wikilink")[化工](../Page/化工.md "wikilink")。北面不远有一座大型的[水力发电站](../Page/水力发电站.md "wikilink")。

## 友好城市

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_United_Kingdom.svg" title="fig:Flag_of_the_United_Kingdom.svg">Flag_of_the_United_Kingdom.svg</a><a href="../Page/英国.md" title="wikilink">英国</a><a href="../Page/考文垂.md" title="wikilink">考文垂</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_Czech_Republic.svg" title="fig:Flag_of_the_Czech_Republic.svg">Flag_of_the_Czech_Republic.svg</a><a href="../Page/捷克.md" title="wikilink">捷克</a><a href="../Page/俄斯特拉发.md" title="wikilink">俄斯特拉发</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Finland.svg" title="fig:Flag_of_Finland.svg">Flag_of_Finland.svg</a><a href="../Page/芬兰.md" title="wikilink">芬兰</a><a href="../Page/凯米.md" title="wikilink">凯米</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Belgium.svg" title="fig:Flag_of_Belgium.svg">Flag_of_Belgium.svg</a><a href="../Page/比利时.md" title="wikilink">比利时</a><a href="../Page/列日.md" title="wikilink">列日</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_France.svg" title="fig:Flag_of_France.svg">Flag_of_France.svg</a><a href="../Page/法国.md" title="wikilink">法国</a><a href="../Page/第戎.md" title="wikilink">第戎</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg" title="fig:Flag_of_Italy.svg">Flag_of_Italy.svg</a><a href="../Page/意大利.md" title="wikilink">意大利</a><a href="../Page/都灵.md" title="wikilink">都灵</a></li>
</ul></td>
<td><p> </p></td>
<td><ul>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Egypt.svg" title="fig:Flag_of_Egypt.svg">Flag_of_Egypt.svg</a><a href="../Page/埃及.md" title="wikilink">埃及</a><a href="../Page/塞得港.md" title="wikilink">塞得港</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_India.svg" title="fig:Flag_of_India.svg">Flag_of_India.svg</a><a href="../Page/印度.md" title="wikilink">印度</a><a href="../Page/金奈.md" title="wikilink">金奈</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Japan.svg" title="fig:Flag_of_Japan.svg">Flag_of_Japan.svg</a><a href="../Page/日本.md" title="wikilink">日本</a><a href="../Page/广岛.md" title="wikilink">广岛</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg" title="fig:Flag_of_Germany.svg">Flag_of_Germany.svg</a><a href="../Page/德国.md" title="wikilink">德国</a><a href="../Page/科隆.md" title="wikilink">科隆</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg" title="fig:Flag_of_Germany.svg">Flag_of_Germany.svg</a><a href="../Page/德国.md" title="wikilink">德国</a><a href="../Page/开姆尼茨.md" title="wikilink">开姆尼茨</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_United_States.svg" title="fig:Flag_of_the_United_States.svg">Flag_of_the_United_States.svg</a><a href="../Page/美国.md" title="wikilink">美国</a><a href="../Page/克利夫兰_(俄亥俄州).md" title="wikilink">-{zh-hans:克利夫兰; zh-hk:克里夫蘭 (俄亥俄州);}-</a></li>
</ul></td>
<td><p> </p></td>
<td><ul>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Canada.svg" title="fig:Flag_of_Canada.svg">Flag_of_Canada.svg</a><a href="../Page/加拿大.md" title="wikilink">加拿大</a><a href="../Page/多伦多.md" title="wikilink">多伦多</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_People&#39;s_Republic_of_China.svg" title="fig:Flag_of_the_People&#39;s_Republic_of_China.svg">Flag_of_the_People's_Republic_of_China.svg</a><a href="../Page/中华人民共和国.md" title="wikilink">中华人民共和国</a><a href="../Page/吉林市.md" title="wikilink">吉林市</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_People&#39;s_Republic_of_China.svg" title="fig:Flag_of_the_People&#39;s_Republic_of_China.svg">Flag_of_the_People's_Republic_of_China.svg</a><a href="../Page/中华人民共和国.md" title="wikilink">中华人民共和国</a><a href="../Page/成都.md" title="wikilink">成都</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Serbia.svg" title="fig:Flag_of_Serbia.svg">Flag_of_Serbia.svg</a><a href="../Page/塞尔维亚.md" title="wikilink">塞尔维亚</a><a href="../Page/克鲁舍瓦茨.md" title="wikilink">克鲁舍瓦茨</a></li>
<li><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Bulgaria.svg" title="fig:Flag_of_Bulgaria.svg">Flag_of_Bulgaria.svg</a><a href="../Page/保加利亚.md" title="wikilink">保加利亚</a><a href="../Page/鲁塞.md" title="wikilink">鲁塞</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 參考文獻

## 外部連結

  - [Official website of Volgograd](http://www.volgadmin.ru/en/)

  - [Official website of Volgograd](http://www.volgadmin.ru/)

  - [Unofficial website of Volgograd](http://www.volgograd.ru/)

  - [Volgograd tourist information](http://www.visitvolgograd.info)

  - [Sights of
    Volgograd](http://www.russia-travel.ws/regions/Volgograd/)

  - [Photo Gallery from
    Volgograd](https://web.archive.org/web/20071014112808/http://galerie.knez.cz/ALBUM/stalingrad/)

  - [Stalingrad - Bilder einer erbitterten
    Schlacht](http://www.stalingrad-stalingrad.de)

  - [Volgograd State University](http://www.volsu.ru/)

[伏尔加格勒](../Category/伏尔加格勒.md "wikilink")
[В](../Category/伏尔加格勒州城市.md "wikilink")
[Category:蘇聯的英雄城市](../Category/蘇聯的英雄城市.md "wikilink")

1.  <https://www.telegraph.co.uk/history/world-war-two/9839666/Russia-revives-Stalingrad-city-name.html>