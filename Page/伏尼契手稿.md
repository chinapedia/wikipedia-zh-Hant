[Voynich_manuscript_excerpt.svg](https://zh.wikipedia.org/wiki/File:Voynich_manuscript_excerpt.svg "fig:Voynich_manuscript_excerpt.svg")

**伏尼契手稿**（Voynich
manuscript），是一本內容不明的神秘[書籍](../Page/书.md "wikilink")，共240頁，附有[插图](../Page/插图.md "wikilink")，而作者不詳。書中所用[字母及](../Page/字母.md "wikilink")[語言至今无人能识别](../Page/語言.md "wikilink")，與現代的語言完全搭不上，似乎是[中古世紀](../Page/中古世紀.md "wikilink")[煉金術士之參考書籍](../Page/煉金術.md "wikilink")。書名**伏尼契**來自名為的[波蘭裔美國人书商](../Page/波蘭裔美國人.md "wikilink")，他於1912年在[義大利買下此书](../Page/義大利.md "wikilink")。2005年，这部书入藏[耶鲁大学的](../Page/耶鲁大学.md "wikilink")[拜内克古籍善本图书馆](../Page/拜内克古籍善本图书馆.md "wikilink")，编号MS
408。

2011年透過[放射性碳定年法檢測出其書成於](../Page/放射性碳定年法.md "wikilink")[十五世紀初](../Page/十五世紀.md "wikilink")，估計在1404-1438年間，可信度達95%。这份手稿被發現以來，專業和業餘的[譯解密碼員](../Page/譯解密碼員.md "wikilink")，如[第二次世界大戰期間](../Page/第二次世界大戰.md "wikilink")[英](../Page/英國.md "wikilink")[美頂尖](../Page/美國.md "wikilink")[解碼專家](../Page/解碼專家.md "wikilink")，都積極研究它，但是未能破譯出隻字片語。一連串的失敗令伏尼契手稿儼然成為密碼術历史中的「[聖杯](../Page/聖杯.md "wikilink")」。不過，也有不少人認為手稿只不過是個[惡作劇](../Page/惡作劇.md "wikilink")——因為書中的符號排列全無意義可言。\[1\]

## 内容

伏尼契手稿用上等[牛犊皮纸](../Page/牛犊皮纸.md "wikilink")（）制成，全书大约240页，但伏尼契获得之前就已经散失了部分书页。书中的文字和素描图都是用[鹅毛笔写成](../Page/鹅毛笔.md "wikilink")，图画上的彩色部分应该是后期才补上的。

### 例證

手稿內的插圖對於揭露書中內容未能有太大幫助，但是藉着插圖卻可以看出全書分為六個篇或章節，每篇的風格和主題各有不同。除了最後一篇全以文字寫成之外，手稿內的每頁最少都有一幅插圖。每個章節的慣用名稱詳見如下：
[f34r.jpg](https://zh.wikipedia.org/wiki/File:f34r.jpg "fig:f34r.jpg")

  - **草藥**：每頁顯示了一種[植物](../Page/植物.md "wikilink")（某些頁有兩種），部分段落描述了當時[歐洲的典型](../Page/歐洲.md "wikilink")[草藥](../Page/草藥.md "wikilink")。部分插圖比「醫藥」章節的草圖更大更清晰。

<!-- end list -->

  - **天文**：含有天體圖，其中有[太陽](../Page/太陽.md "wikilink")、[月亮和星辰](../Page/月亮.md "wikilink")，應與[天文學或](../Page/天文學.md "wikilink")[占星術相關](../Page/占星術.md "wikilink")。其中有12幅圖描繪了[黃道12宮的星相](../Page/黃道12宮.md "wikilink")（[星座](../Page/星座.md "wikilink")）標記（兩條魚代表[雙魚座](../Page/雙魚座.md "wikilink")、一頭公牛代表[金牛座](../Page/金牛座.md "wikilink")、手持弓箭的戰士代表[人馬座等等](../Page/人馬座.md "wikilink")）。每一符號都畫有約30個女子環繞，每個女子都捧著一顆標有名稱的星星，大部分女子是[裸體的](../Page/裸體.md "wikilink")。章節最後兩頁（[水瓶座和](../Page/水瓶座.md "wikilink")[魔羯座](../Page/魔羯座.md "wikilink")，一月和二月的星座）已經缺失，而[白羊座和金牛座的兩頁已經撕裂為](../Page/白羊座.md "wikilink")4張圖，每張上各有15顆星星。有些圖案是畫在可摺疊的書頁上。

<!-- end list -->

  - **生物學**：大量的文字並配有插圖。插圖主要是裸體女子在水池中或盆中洗澡的樣子，這些池或盆用管子連接成網狀。其中部分可以很容易的看出像人體的[器官](../Page/器官.md "wikilink")，有些女子頭戴[皇冠](../Page/皇冠.md "wikilink")。

<!-- end list -->

  - **宇宙論**：很多圓形的圖案，但都很模糊。這一章節也有可摺疊的書頁，有一頁可擴展為6頁，包含了一幅以「[鋪道](../Page/鋪道.md "wikilink")」連接的九座「島嶼」的地圖，其中還有城堡，和一座[火山](../Page/火山.md "wikilink")。

<!-- end list -->

  - **醫藥**：很多帶標記的圖畫，如各種植物的部分如[根部](../Page/根部.md "wikilink")、[葉子等等](../Page/葉子.md "wikilink")。物體都類似於[藥物](../Page/藥物.md "wikilink")，在書頁旁另有文字說明。

<!-- end list -->

  - **配方**：很多短的段落，每一段都標有一個花形或者星形的標注。

### 原文

[Voynich_manuscript_bathtub_example_77v_cropped.jpg](https://zh.wikipedia.org/wiki/File:Voynich_manuscript_bathtub_example_77v_cropped.jpg "fig:Voynich_manuscript_bathtub_example_77v_cropped.jpg")
[Voynich_Manuscript_(135).jpg](https://zh.wikipedia.org/wiki/File:Voynich_Manuscript_\(135\).jpg "fig:Voynich_Manuscript_(135).jpg")
由右邊不對稱的段落推斷，文字是由左至右書寫。段落沒有明顯的[標點符號](../Page/標點符號.md "wikilink")。從流暢的字型及排列整齊，似乎謄寫員清楚明白自己在寫什麼，而不是落筆前故意逐字編造。

手稿含有17萬個字跡，字跡之間有窄分隔，大部分由一至兩筆寫成。一套有20至30種不同字跡的字母系統幾乎說明了整個手稿。關於某些字跡是否為個別[字母存在一些爭論](../Page/字母.md "wikilink")。部分奇怪的字母僅出現一或兩次。較闊的分隔可分辨出約3.5萬個不同長度的「詞彙」，大致符合[語音學的規律](../Page/語音學.md "wikilink")，例如部分字母在每一個詞彙經常出現（正如[英語的](../Page/英語.md "wikilink")[韻母](../Page/韻母.md "wikilink")）等。

[統計分析發現](../Page/統計.md "wikilink")，文稿的文字規律與[自然語言類似](../Page/自然語言.md "wikilink")。例如，詞彙的出現頻率符合[齊夫定律](../Page/齊夫定律.md "wikilink")，而詞彙的[熵](../Page/熵_\(信息论\).md "wikilink")（每詞約10[位元](../Page/位元.md "wikilink")）亦與[英語或](../Page/英語.md "wikilink")[拉丁語相類似](../Page/拉丁語.md "wikilink")。

然而，手稿的語言與歐洲語言不太相似。例如，幾乎沒有詞彙是多過10個字母，亦幾乎沒有1或2個字母的詞彙。詞彙內的字母分布獨特，有些字母僅出現在字首，有些在字尾，有些在詞彙中間，這特點與[阿拉伯字母相似](../Page/阿拉伯字母.md "wikilink")，但不見於[拉丁字母](../Page/拉丁字母.md "wikilink")、[希臘字母或](../Page/希臘字母.md "wikilink")[西里尔字母](../Page/西里尔字母.md "wikilink")。詞彙的重複程度也高於一般歐洲語言。同一詞彙可能一句出現三次（相當於在英語出現*and
and and*）。[統計學上](../Page/統計學.md "wikilink")，手稿的詞彙數目相對較少。

## 作者推测

有不少人被推測為伏尼契手稿的原作者，這裡只列出一些知名的人物。

  - [罗吉尔·培根](../Page/罗吉尔·培根.md "wikilink")
  - [约翰·迪伊](../Page/约翰·迪伊.md "wikilink")
  - [埃德华·凯莱](../Page/埃德华·凯莱.md "wikilink")

## 騙局論

有人認為伏尼契手稿只是內容顯得很怪異難解的[騙局](../Page/騙局.md "wikilink")。但問題隨之而來：如果沒有預期的觀眾（即創作者的同時代人），作者為什麼要用這樣一種複雜而困難的方式愚弄人？

據說這份手稿可能曾被[神聖羅馬帝國皇帝](../Page/神聖羅馬帝國.md "wikilink")[魯道夫二世買下](../Page/魯道夫二世_\(神聖羅馬帝國\).md "wikilink")，如果此事當真，顯然作者是以精美的插圖，運用[卡登格](../Page/卡登格.md "wikilink")（Cardan
grille）填上文字來騙錢\[2\]。

## 破解

2014年3月，語言學教授Stephen
Bax聲稱他成功破譯部分手稿。他參考了[中世紀各類手稿](../Page/中世紀.md "wikilink")，並運用[閃米特語系中的](../Page/闪米特语族.md "wikilink")[阿拉伯語來破譯伏尼契手稿的神秘含義](../Page/阿拉伯語.md "wikilink")。他結合[語言分析](../Page/語言分析.md "wikilink")，破譯了17個象徵符號，揭示出了10個單詞的意思，包括[杜松](../Page/杜松子.md "wikilink")（Juniper）、[芫荽](../Page/芫荽.md "wikilink")（Coriander）、[鹿食草](../Page/鹿食草.md "wikilink")（Hellebore）、[黑枯茗](../Page/黑枯茗.md "wikilink")（Nigella
Sativa）、[棉花](../Page/棉花.md "wikilink")（Cotton）和[藏紅花](../Page/藏紅花.md "wikilink")（Saffron）等植物名詞，以及[喀戎](../Page/喀戎.md "wikilink")（Chiron）（[希臘神話中一個半人馬的名字](../Page/希臘神話.md "wikilink")）。

教授說：「我不認為這本書來自外星球或是什麼魔法師的傑作諸如此類的。我想它本質上就是一本關於大自然的書，不然怎麼會畫滿了植物和各類星體。我用以阿拉伯文和其他語言文字記載的中世紀藥草手稿來破解那些圖形，它們中很多都指向植物類，而且會是你意想不到的種類。我覺得，這手稿源自[亞洲](../Page/亞洲.md "wikilink")。手稿上的語言是獨一無二的，寫法很淘氣，像來自[中土世界](../Page/中土世界.md "wikilink")。」

另外有研究者相信書中303種植物中至少有37種生長於[中美洲](../Page/中美洲.md "wikilink")，也就是現在的[墨西哥一帶](../Page/墨西哥.md "wikilink")。手稿寫於15至16世紀，很有可能使用的是[納瓦特族](../Page/納瓦特族.md "wikilink")（墨西哥南部和中美洲[印第安各族](../Page/印第安.md "wikilink")）的[納瓦特語](../Page/納瓦特爾語.md "wikilink")。\[3\]但反對者認為Stephen
Bax更動了很多字母以符合他的學說，而且語法上也不符合拉丁縮寫的規則。

2018年，來自[加拿大](../Page/加拿大.md "wikilink")[艾伯塔大學的葛雷格](../Page/艾伯塔大學.md "wikilink")．康達拉克(Greg
Kondrak)和布萊德利．哈爾(Bradley
Hauer)，利用[人工智能演算發現有](../Page/人工智能.md "wikilink")80%的文法符合[希伯來文語法](../Page/希伯來文.md "wikilink")，並且成功翻譯出第一行話為「她向牧師、家人、人民提出建議。」\[4\]，但這語氣不順暢，伏尼契手稿中的文本太雜，重覆的字符太多，不像流暢的作文，這意味著可能最終只能破解手稿上的名詞，而無法完整翻譯。

## 參見

  - [羅洪特寫本](../Page/羅洪特寫本.md "wikilink")

## 註釋

## 延伸閱讀

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 外部連結

  - [央視-伏尼契手稿](https://www.youtube.com/watch?v=-hmw9vE5z1s)

  - [Voynich
    Manuscript](http://beinecke.library.yale.edu/digitallibrary/voynich.html)
    From the digital collection of the [Beinecke Rare Book and
    Manuscript Library at Yale
    University](http://www.library.yale.edu/beinecke/)

  - [René Zandbergen's introductory site](http://www.voynich.nu/) about
    the Voynich Manuscript

  - [Nick Pelling's research site](http://www.ciphermysteries.com/)

  -
  - [*Nature* news article: World's most mysterious book may be a
    hoax](http://www.nature.com/nsu/031215/031215-5.html) A summary of
    Gordon Rugg's paper directed towards a more general audience

  - [*Scientific American*: The Mystery of the Voynich
    Manuscript](http://www.sciam.com/article.cfm?chanID=sa006&colID=1&articleID=0000E3AA-70E1-10CF-AD1983414B7F0000)

[V](../Category/密碼學歷史.md "wikilink")
[V](../Category/未解譯的代碼及密碼.md "wikilink")
[F](../Category/密码学.md "wikilink") [F](../Category/绘画作品.md "wikilink")
[F](../Category/手抄本.md "wikilink") [V](../Category/未解读文字.md "wikilink")

1.  記者鮑蓉蓉／綜合報導,"地球上的外星書「伏尼契手稿」專家破身世之謎"[1](http://www.nownews.com/2011/02/13/545-2688513.htm),[NOWnews](../Page/NOWnews.md "wikilink")
    今日新聞網,2011/02/13 11:47.
2.  [神秘語言?騙錢偽作? ----
    伏尼契手稿](https://archive.is/20130428154539/http://tw.myblog.yahoo.com/paulmcy1216/article?mid=587)
3.
4.