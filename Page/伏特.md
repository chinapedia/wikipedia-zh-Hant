[NISTvoltChip.jpg](https://zh.wikipedia.org/wiki/File:NISTvoltChip.jpg "fig:NISTvoltChip.jpg")晶片陣列。\]\]

**伏特**（）是[国际单位制中](../Page/国际单位制.md "wikilink")[电压的单位](../Page/电压.md "wikilink")，符号**V**。

在一根均匀的、[宽度和](../Page/宽度.md "wikilink")[温度固定的](../Page/温度.md "wikilink")[导线上假如有一](../Page/导线.md "wikilink")[安培](../Page/安培.md "wikilink")[电流流动](../Page/电流.md "wikilink")，那么导线的[电阻在一定的距离内將](../Page/电阻.md "wikilink")[电能转化为](../Page/电能.md "wikilink")[热能](../Page/热能.md "wikilink")1瓦（W=1J/s）。这个[距离之间的电压差就被定义为一伏特](../Page/距离.md "wikilink")：

此单位是以发明[电池的](../Page/电池.md "wikilink")[意大利](../Page/意大利.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")——[亚历山德罗·伏打的名字命名的](../Page/亚历山德罗·伏打.md "wikilink")。

## 定義

伏特的[國際單位制（SI）定義是以基本的](../Page/国际单位制#單位與詞頭.md "wikilink")[電學單位](../Page/電學.md "wikilink")：安培（縮寫為英文字母
A），和[力學單位](../Page/力學.md "wikilink")[瓦特](../Page/瓦特.md "wikilink")（縮寫為英文字母
W）而導出的。依據國際單位制中的安培，將伏特定義為：「在載有一[安培恆定電流的導線上](../Page/安培.md "wikilink")，當兩點之間導線上的[功率耗散為一](../Page/功率.md "wikilink")[瓦特](../Page/瓦特.md "wikilink")(1W
= 1 J/S)時，這兩點之間的[電位差](../Page/電位差.md "wikilink")」\[1\]。

\[\mbox{V} = \dfrac{\mbox{W}}{\mbox{A}} = \dfrac{\mbox{J}}{\mbox{A} \cdot \mbox{s}} = \dfrac{\mbox{N} \cdot \mbox{m} }{\mbox{A} \cdot \mbox{s}}\]

而以基本單位寫出定義公式即為： （公斤 · 平方公尺） ·（秒<sup>−3</sup> · 安培<sup>−1</sup> ）

  -
    <math alt="volt equals kilogram times meter squared per ampere per second cubed">

\\text{V} = \\frac{\\text{potential energy}}{\\text{charge}} =
\\frac{\\text{N} {\\cdot} \\text{m}}{\\text{C}} = \\frac{\\text{kg}
{\\cdot} \\text{m}^2}{\\text{A} {\\cdot} \\text{s}^3}.</math>

也可定義為單位[庫侖電荷的](../Page/庫侖.md "wikilink")[能量](../Page/能量.md "wikilink"),

\[\mbox{V} = \dfrac{\mbox{J}}{\mbox{C}}\]

### 利用約瑟夫森結的定義

自1990年起國際間利用[約瑟夫森結的實驗來實測一伏特的量值](../Page/約瑟夫森結.md "wikilink")，此實驗配合轉換係數[約瑟夫森常數](../Page/約瑟夫森常數.md "wikilink")，即可量測一伏特，約瑟夫森常數在18世紀的[國際度量衡大會訂定](../Page/國際度量衡大會.md "wikilink")，其數值如下：

  -
    *K*<sub>{J-90}</sub> =
    2*[e](../Page/基本電荷.md "wikilink")*/*[h](../Page/普朗克常數.md "wikilink")*
    = 0.4835979 GHz/µV.

一般而言此實驗都是使用數千或上萬的約瑟夫森結陣列，用10至80GHz的微波（依實驗設計不同而不同）做為激發訊號\[2\]。

## 電的水力類比

有時會用水的管路系統來說明電路，電壓（電位差）可以類比為水壓差，而電流會類比為水在管路特定位置的[體積流率](../Page/體積流率.md "wikilink")，是由管路的寬度（可類比為[電導率](../Page/電導率.md "wikilink")）及管路二端子的水壓差（可類比為電壓）決定。

在歐姆式導體中，導體兩端電壓和流過導體的電流會成正比關係，[歐姆定律就在說明上述的關係](../Page/歐姆定律.md "wikilink")。

## 常見電壓

[Electronic_multi_meter.jpg](https://zh.wikipedia.org/wiki/File:Electronic_multi_meter.jpg "fig:Electronic_multi_meter.jpg")可以量測二點之間的電壓\]\]

[BateriaR14.jpg](https://zh.wikipedia.org/wiki/File:BateriaR14.jpg "fig:BateriaR14.jpg")

以下是一些常見電壓源的電壓：

  - [神經細胞的](../Page/神經.md "wikilink")[靜息電位](../Page/靜息電位.md "wikilink")：大約
    −75 mV\[3\]。
  - 一顆可充電的[NiMH或](../Page/鎳氫電池.md "wikilink")[NiCd](../Page/鎳鎘電池.md "wikilink")[電池](../Page/電池.md "wikilink")：1.2 V。
  - [水銀電池](../Page/水銀電池.md "wikilink")：1.355 V。
  - 一顆不能充電的[鹼性電池](../Page/鹼性電池.md "wikilink")：1.5 V。
  - 可充電的[LiFePO<sub>4</sub>電池](../Page/磷酸锂铁.md "wikilink")：3.3 V。
  - 可充電的[鋰離子聚合物電池](../Page/鋰離子聚合物電池.md "wikilink")：3.75 V。
  - [TTL電源](../Page/電晶體－電晶體邏輯.md "wikilink")：5 V。
  - [PP3電池](../Page/PP3電池.md "wikilink")：9 V。
  - [車輛的電力系統](../Page/車輛.md "wikilink")：標準電壓12 V，放電時約11.8 V，充電時約12.8 V，若在車輛行進中的充電電壓，可以到13.8–14.4 V。
  - [家用電源](../Page/市電.md "wikilink")：[歐洲](../Page/歐洲.md "wikilink")、[亚洲](../Page/亚洲.md "wikilink")、[非洲為](../Page/非洲.md "wikilink")230 V，[北美為](../Page/北美.md "wikilink")120 V，[日本為](../Page/日本.md "wikilink")100 V，[台灣為](../Page/台灣.md "wikilink")110 V，[中国大陆](../Page/中国大陆.md "wikilink")、[香港为](../Page/香港.md "wikilink")220 V，此處所列的電壓均為交流電壓的[RMS值](../Page/均方根.md "wikilink")（其他國家的電源，請參考[家用電源列表](../Page/家用電源列表.md "wikilink")）。
  - [卡車或貨車的電源](../Page/卡車.md "wikilink")：24 V DC。
  - [城市軌道交通系統的](../Page/城市軌道交通系統.md "wikilink")[第三軌供電](../Page/軌道供電.md "wikilink")：電壓依國家不同，由500 V
    DC到1500 V DC不等。
  - 鐵路的[接触网供电](../Page/接触网供电.md "wikilink")：交流電壓[RMS值](../Page/均方根.md "wikilink")25kV，頻率為50或60Hz。
  - [三相交流](../Page/三相電.md "wikilink")[輸電](../Page/輸電系統.md "wikilink")：中國1000kV，美國765kV，英國400kV
  - [高壓直流輸電](../Page/高壓直流輸電.md "wikilink")：最高±1100kV（[新疆](../Page/新疆.md "wikilink")[准東至](../Page/准東.md "wikilink")[安徽](../Page/安徽.md "wikilink")[皖南](../Page/皖南.md "wikilink")）
  - [閃電](../Page/閃電.md "wikilink")：多半在100MV左右。

上述的電壓源若提供交流電，其列的電壓值是以[RMS值為準](../Page/均方根.md "wikilink")，若一個中心值為0的[弦波電壓](../Page/弦波.md "wikilink")，其峰值電壓會是RMS電壓的\(\sqrt{2}\)倍。

## 伏特的歷史

在19世紀時，[路易吉·伽伐尼發現死](../Page/路易吉·伽伐尼.md "wikilink")[青蛙的](../Page/青蛙.md "wikilink")[肌肉接觸火花時會顫動](../Page/肌肉.md "wikilink")，認為電存在于生物體內，[亞歷山德羅·伏打不認同他的看法](../Page/亞歷山德羅·伏打.md "wikilink")，也開始從事有關電的研究。亞歷山德羅·伏打發明了[伏打電堆](../Page/伏打電堆.md "wikilink")，是第一個現代的化學[電池](../Page/電池.md "wikilink")，可以提供穩定的電流，亞歷山德羅·伏打也發現了在電堆兩極的金屬為[鋅及](../Page/鋅.md "wikilink")[銀時](../Page/銀.md "wikilink")，產生電力的效果最好。在19世紀80年代時国际电工協會，也就是現在的[国际电工委员会](../Page/国际电工委员会.md "wikilink")（IEC），確立伏特為電動勢的單位。當時伏特的定義是一個流過一安培電流的導體，其功率消耗為一瓦特時，導體二端的電位差。

在1893年時，國際上將伏特定義為[克拉克電池](../Page/克拉克電池.md "wikilink")（）電動勢的1/1.434。在1908年時，由於國際上已改以[歐姆及](../Page/歐姆.md "wikilink")[安培來定義伏特](../Page/安培.md "wikilink")，因此不再使用上述的定義，不過相關的設備在1948年才不再使用。

在使用約瑟夫森結來做為伏特的基準前，國際上的實驗室使用一種稱為[標準電池的特殊電池做為伏特的基準](../Page/標準電池.md "wikilink")，美國在1905-1972年時使用一種特殊設計的標準電池，稱為[韋斯頓電池](../Page/韋斯頓電池.md "wikilink")。

## 參考

## 相關連結

  - [電子伏特](../Page/電子伏特.md "wikilink")

[V](../Category/电学单位.md "wikilink")
[V](../Category/国际单位制导出单位.md "wikilink")

1.  [BIPM SI Brochure: Appendix 1,
    p. 144](http://www.bipm.org/utils/common/pdf/si_brochure_8_en.pdf)
2.
3.  Bullock, Orkand, and Grinnell, pp. 150–151; Junge, pp. 89–90;
    Schmidt-Nielsen, p. 484