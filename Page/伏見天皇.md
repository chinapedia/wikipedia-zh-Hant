**伏見天皇**（；），第92代[日本天皇](../Page/日本天皇.md "wikilink")。諱熙仁（）。

他是[後深草天皇第](../Page/後深草天皇.md "wikilink")2皇子。[弘安](../Page/弘安.md "wikilink")10年[後宇多天皇退位](../Page/後宇多天皇.md "wikilink")，由伏見天皇繼任。伏見天皇在位時間為1287年至1298年為止\[1\]。

## 生平

伏見天皇原本為[後深草天皇之第二皇子熙仁親王](../Page/後深草天皇.md "wikilink")\[2\]，後來於1275年成為[大覺寺統](../Page/大覺寺統.md "wikilink")[後宇多天皇](../Page/後宇多天皇.md "wikilink")（伏見天皇的堂兄）的皇[太子](../Page/太子.md "wikilink")。因為後深草上皇認為應由其子嗣繼承皇位，由幕府調停斡旋下，弘安十年（1287年）後宇多天皇讓位給後深草上皇之第二皇子熙仁親王\[3\]。

在此之後一段短暫的時期中，[大覺寺統與](../Page/大覺寺統.md "wikilink")[持明院統的勢力僵持不下](../Page/持明院統.md "wikilink")。兩年之後，[後深草天皇退位](../Page/後深草天皇.md "wikilink")，結束了他的統治，[院政停止運作](../Page/院政.md "wikilink")，伏見天皇終於可以直接控制政府。

1289年，伏見天皇將自己的兒子（未來的[後伏見天皇](../Page/後伏見天皇.md "wikilink")）立為皇太子。1290年4月19日，宮中的淺原為賴一族企圖暗殺天皇（[淺原事件](../Page/淺原事件.md "wikilink")），雖然並未成功。

伏見天皇在位期間，貴族的勢力遭到政府削減，但是[幕府的權力持續增加](../Page/幕府.md "wikilink")。伏見天皇在1298年8月退位，[後伏見天皇開始他的統治時代](../Page/後伏見天皇.md "wikilink")，伏見天皇負責[院政的運作](../Page/院政.md "wikilink")。

但是三年之後（1301年），[大覺寺統再度崛起](../Page/大覺寺統.md "wikilink")，迫使[後伏見天皇退位](../Page/後伏見天皇.md "wikilink")，由[後宇多天皇第一皇子邦治親王繼承](../Page/後宇多天皇.md "wikilink")，成為[後二條天皇](../Page/後二條天皇.md "wikilink")。

1308年時，伏見天皇與幕府成功合作，伏見天皇第四皇子[花園天皇成功即位](../Page/花園天皇.md "wikilink")。[大覺寺統與](../Page/大覺寺統.md "wikilink")[持明院統其後對立](../Page/持明院統.md "wikilink")，政府分裂成為南北朝並立時代。

在伏見天皇的統治期間，[大覺寺統與](../Page/大覺寺統.md "wikilink")[持明院統勢力互相排擠](../Page/持明院統.md "wikilink")。

1313年10月，退位的伏見皇帝削髮为[僧](../Page/和尚.md "wikilink")。權力正式轉移到他的兒子後伏見天皇手中。

伏見天皇在1317年去世，但是他的兒子[花園天皇沒有參加為他舉行的哀悼儀式](../Page/花園天皇.md "wikilink")，這是前所未有的行為。但是這種行為並無不合理之處，因為花園天皇已成為哥哥後伏見天皇的「兒子」\[4\]。伏見天皇與其他天皇一起葬在[京都](../Page/京都.md "wikilink")[伏見區的深草十二帝陵](../Page/伏見區.md "wikilink")（深草北陵）\[5\]。

## 公卿

[公卿是是指公家和基於日本律令規定的太政官當中之最高幹部職位](../Page/公卿.md "wikilink")，即[太政大臣](../Page/太政大臣.md "wikilink")、[左大臣](../Page/左大臣.md "wikilink")、[右大臣](../Page/右大臣.md "wikilink")、[大納言](../Page/大納言.md "wikilink")、[中納言](../Page/中納言.md "wikilink")、[參議](../Page/參議.md "wikilink")（或從三位以上（非參議））等高官。

關白為天皇成年後，輔助總理萬機的重要職位。伏見天皇統治期間出現的[關白包括](../Page/關白.md "wikilink")：

<table border=1 cellpadding=1 cellspacing=0>

<tr>

<th align="center" style="background:#efefef;">

關白

</th>

<th align="center" style="background:#efefef;">

在職年

</th>

</tr>

<tr>

<td>

[二條師忠](../Page/二條師忠.md "wikilink")

</td>

<td>

1287年－1289年

</td>

</tr>

<tr>

<td>

[近衛家基](../Page/近衛家基.md "wikilink")

</td>

<td>

1289年－1291年

</td>

</tr>

<tr>

<td>

[九條忠教](../Page/九條忠教.md "wikilink")

</td>

<td>

1291年－1293年

</td>

</tr>

<tr>

<td>

近衛家基

</td>

<td>

1293年－1296年

</td>

</tr>

<tr>

<td>

[鷹司兼忠](../Page/鷹司兼忠.md "wikilink")

</td>

<td>

1296年－1298年

</td>

</tr>

</td>

</tr>

</table>

## 年號

伏見天皇統治多年，總共使用三個年號\[6\]。

  - [弘安](../Page/弘安.md "wikilink")
    1287年[10月21日](../Page/十月廿一.md "wikilink") -
    1288年[4月28日](../Page/四月廿八.md "wikilink")
  - [正應](../Page/正應.md "wikilink") 1288年4月28日 -
    1293年[8月5日](../Page/八月初五.md "wikilink")
  - [永仁](../Page/永仁.md "wikilink") 1293年8月5日 -
    1298年[7月22日](../Page/七月廿二.md "wikilink")

## 子嗣

伏見天皇原本為[後深草天皇](../Page/後深草天皇.md "wikilink")
第二皇子。母親為[左大臣](../Page/左大臣.md "wikilink")[洞院實雄的女兒](../Page/洞院實雄.md "wikilink")、玄輝門院[藤原愔子](../Page/藤原愔子.md "wikilink")。

  - 中宮：西園寺（藤原）鏱子（[永福門院](../Page/永福門院.md "wikilink")）（1271-1342） -
    [西園寺實兼女兒](../Page/西園寺實兼.md "wikilink")
  - 典侍（准三后）：[五辻（藤原）經子](../Page/五辻經子.md "wikilink")（中納言典侍） -
    [五辻經氏女兒](../Page/五辻經氏.md "wikilink")
      - 第一皇子：胤仁親王（[後伏見天皇](../Page/後伏見天皇.md "wikilink")）（1288-1336）
  - [洞院（藤原）季子](../Page/洞院季子.md "wikilink")（顯親門院）（1265-1336） -
    [洞院實雄女兒](../Page/洞院實雄.md "wikilink")
      - 第一皇女：[璹子内親王](../Page/璹子内親王.md "wikilink")（朔平門院）（1287-1310）
      - 第三皇子：寬性法親王（1289-1346） - [仁和寺御室](../Page/仁和寺.md "wikilink")
      - 第三皇女：[延子内親王](../Page/延子内親王.md "wikilink")（延明門院）（1291-?）
      - 第四皇子：富仁親王（[花園天皇](../Page/花園天皇.md "wikilink")）（1297-1348）
  - [洞院（藤原）英子](../Page/洞院英子.md "wikilink") -
    [洞院公宗女兒](../Page/洞院公宗.md "wikilink")
      - 第二皇女：[誉子内親王](../Page/誉子内親王.md "wikilink")（章義門院）（?-1336）
  - 典侍：權大納言局 - [中院（源）具氏女兒](../Page/中院具氏.md "wikilink")
      - 第六皇子：[尊悟入道親王](../Page/尊悟入道親王.md "wikilink")（1299-1359）
  - 三善衡子（播磨内侍） - 三善俊衡女兒
      - 第五皇子：[尊圓法親王](../Page/尊圓法親王.md "wikilink")（1298-1356）
  - 正親町（藤原）守子（東御方） - [正親町實明女兒](../Page/正親町實明.md "wikilink")
      - 皇子：寛胤法親王 　
      - 皇子：道凞法親王
  - [高倉茂通女兒](../Page/高倉茂通.md "wikilink")
      - 第七皇子：尊凞法親王
  - 春日局 - 僧任快女兒
      - 第二皇子：惠助法親王（1289-1328）
  - 西御方 - 僧深源女兒
      - 第八皇子：聖珍法親王

## 系圖

## 參考資料

## 參考文獻

  - Ponsonby-Fane, Richard Arthur Brabazon. (1959). [*The Imperial House
    of
    Japan.*](http://books.google.com/books?id=SLAeAAAAMAAJ&q=The+Imperial+House+of+Japan&dq=The+Imperial+House+of+Japan&client=firefox-a&pgis=1)
    Kyoto: Ponsonby Memorial Society.
    [OCLC 194887](http://www.worldcat.org/wcpa/oclc/194887)
  - Titsingh, Isaac. (1834). *Nihon Odai Ichiran*; ou, [*Annales des
    empereurs du
    Japon.*](http://books.google.com/books?id=18oNAAAAIAAJ&dq=nipon+o+dai+itsi+ran)
    Paris: Royal Asiatic Society, Oriental Translation Fund of Great
    Britain and Ireland.
    [OCLC 5850691](http://www.worldcat.org/title/nipon-o-dai-itsi-ran-ou-annales-des-empereurs-du-japon/oclc/5850691)
  - Varley, H. Paul. (1980). [*Jinnō Shōtōki: A Chronicle of Gods and
    Sovereigns.*](http://books.google.com/books?id=tVv6OAAACAAJ&dq=) New
    York: Columbia University Press. ISBN 0-231-04940-4; ISBN
    978-0-231-04940-5;
    [OCLC 5914584](http://www.worldcat.org/title/chronicle-of-gods-and-sovereigns-jinno-shotoki-of-kitabatake-chikafusa/oclc/59145842)

[Category:鎌倉時代天皇](../Category/鎌倉時代天皇.md "wikilink")
[Category:日本書法家](../Category/日本書法家.md "wikilink")
[Category:鎌倉時代歌人](../Category/鎌倉時代歌人.md "wikilink")

1.  Titsingh, Isaac. (1834). *Annales des empereurs du Japon,* pp.
    269-274; Varley, H. Paul. (1980). *Jinnō Shōtōki.* pp. 237-238.
2.  Titsingh, p. 269; Varley, p. 237.
3.  Titsingh, p. 269; Varley, p. 44; a distinct act of *senso* is
    unrecognized prior to Emperor Tenji; and all sovereigns except Jitō,
    Yōzei, Go-Toba, and Emperor Fushimi have *senso* and *sokui* in the
    same year until the reign of Emperor Go-Murakami.
4.  Varley, p. 241.
5.  Ponsonby-Fane, Richard. (1959). *The Imperial House of Japan,* p.
    422.
6.  Titsingh, p. 269.