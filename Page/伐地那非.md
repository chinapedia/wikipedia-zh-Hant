[000805lg_Levitra.jpg](https://zh.wikipedia.org/wiki/File:000805lg_Levitra.jpg "fig:000805lg_Levitra.jpg")

**伐地那非**（）是一种橙色的小药丸，绰号“火焰”。它是非常强效的PDE-5抑制剂。用於治療男性陰莖[勃起功能障礙](../Page/勃起功能障礙.md "wikilink")。該藥能夠協助男性在性行為時能達到及維持勃起。其品牌名稱為「Levitra」，中國大陸譯名為「艾力达」，香港譯名為「立威大」，台灣則譯為「樂威壯」（德國[拜耳](../Page/拜耳.md "wikilink")）。製劑有[片劑](../Page/片劑.md "wikilink")，內含鹽酸伐地那非2.5mg、5mg、10mg或20mg。

## 外部連結

  - [(Levitra官方網站)](http://www.levitra.com/)
  - [PubChem
    Information](http://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?cid=110634)

[Category:PDE5抑制剂](../Category/PDE5抑制剂.md "wikilink")
[Category:葛兰素史克](../Category/葛兰素史克.md "wikilink")
[Category:先灵-葆雅](../Category/先灵-葆雅.md "wikilink")
[Category:拜耳品牌](../Category/拜耳品牌.md "wikilink")
[Category:哌嗪](../Category/哌嗪.md "wikilink")
[Category:砜](../Category/砜.md "wikilink")
[Category:芳香醚](../Category/芳香醚.md "wikilink")
[Category:内酰胺](../Category/内酰胺.md "wikilink")