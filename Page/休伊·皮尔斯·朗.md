[HueyPLong.jpg](https://zh.wikipedia.org/wiki/File:HueyPLong.jpg "fig:HueyPLong.jpg")

**休伊·皮尔斯·朗**（，又译作**休伊·龙**、**休伊·朗格**，），[美国民主黨籍](../Page/美国民主黨.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")，前任[路易斯安那州州長及](../Page/路易斯安那州.md "wikilink")[联邦參议员](../Page/联邦參议员.md "wikilink")。

## 簡介

出生于[路易斯安那州](../Page/路易斯安那州.md "wikilink")[溫菲爾德](../Page/溫菲爾德_\(路易斯安那州\).md "wikilink")。早年曾中途退学，自学法律，后简短就读于[奧克拉荷馬大學和](../Page/奧克拉荷馬大學.md "wikilink")[杜蘭大學法学院](../Page/杜蘭大學.md "wikilink")，获得律师资格。

他在从事法律工作期间步入政坛，1924年竞选路易斯安那州长未成，1928年当选州长。他的口号是“人人皆是無冕之王”，他执政期间越过州议会行使权力，大大增加对富人的税收，加强福利事业帮助穷人，获得优秀政绩，但被一些同屬民主黨的反对者指为[独裁](../Page/独裁.md "wikilink")。

1930年他被选入参议院，之后卸任州长职务，但依靠州里下层人民的支持繼續控制州事务。他在1933年後公开反對[羅斯福新政](../Page/羅斯福新政.md "wikilink")，后来试图参选[美国总统并有望取代](../Page/美国总统.md "wikilink")[小羅斯福](../Page/小羅斯福.md "wikilink")，但在1935年於[巴吞魯日遭政敌指派](../Page/巴吞魯日_\(路易斯安那州\).md "wikilink")[刺客暗杀](../Page/刺客.md "wikilink")。

他在[美国历史上是一个有争议的人物](../Page/美国历史.md "wikilink")，支持者认为他的州改革改善了穷人的生活水準，反对者认为他实行独裁，破坏权力制衡制度。

朗的妻子[罗斯·迈克康奈尔·朗与儿子](../Page/罗斯·迈克康奈尔·朗.md "wikilink")[拉塞尔·朗在他之后都曾担任过路易斯安那州的](../Page/拉塞尔·朗.md "wikilink")[联邦参议员一职](../Page/美国参议院.md "wikilink")，保持民主党在南方的影響力。

## 注釋

<div class="references">

</div>

[Category:反饑餓倡導者](../Category/反饑餓倡導者.md "wikilink")
[Category:美国民主党联邦参议员](../Category/美国民主党联邦参议员.md "wikilink")
[Category:路易斯安那州联邦参议员](../Category/路易斯安那州联邦参议员.md "wikilink")
[Category:路易斯安那州州長](../Category/路易斯安那州州長.md "wikilink")
[Category:美國民主黨州長](../Category/美國民主黨州長.md "wikilink")
[Category:美國民主黨員](../Category/美國民主黨員.md "wikilink")
[Category:美國律師](../Category/美國律師.md "wikilink")
[Category:律師出身的政治人物](../Category/律師出身的政治人物.md "wikilink")
[Category:杜蘭大學校友](../Category/杜蘭大學校友.md "wikilink")
[Category:奧克拉荷馬大學校友](../Category/奧克拉荷馬大學校友.md "wikilink")
[Category:美國浸禮宗教徒](../Category/美國浸禮宗教徒.md "wikilink")
[Category:路易斯安那州人](../Category/路易斯安那州人.md "wikilink")
[Category:遇刺身亡的美國政治人物](../Category/遇刺身亡的美國政治人物.md "wikilink")