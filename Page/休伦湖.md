[Great_Lakes_Lake_Huron.png](https://zh.wikipedia.org/wiki/File:Great_Lakes_Lake_Huron.png "fig:Great_Lakes_Lake_Huron.png")
**休伦湖**（**Lake
Huron**）是[北美洲](../Page/北美洲.md "wikilink")[五大湖之一](../Page/五大湖.md "wikilink")，位于[美国](../Page/美国.md "wikilink")[密歇根州和](../Page/密歇根州.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")[安大略省之间](../Page/安大略.md "wikilink")。休伦湖由早期[法国探险者命名](../Page/法国.md "wikilink")，名字来源于居住于附近地区的[印地安人](../Page/印地安人.md "wikilink")[休伦族](../Page/休伦族.md "wikilink")。

## 地理

休伦湖水面的海拔高度是176米，湖的平均深度为59米，面积59,600平方公里，蓄水量3,568立方公里，湖岸线长6,159公里。

## 參考文獻

## 外部链接

  - [休伦湖鱼的种类](http://www.seagrant.wisc.edu/greatlakesfish/textonly/LakeHuron.html)
  - [休伦湖数据](https://web.archive.org/web/20050515233722/http://www.ndbc.noaa.gov/Maps/EastGL.shtml)

[休伦湖](../Category/休伦湖.md "wikilink")
[Category:北美洲跨國湖泊](../Category/北美洲跨國湖泊.md "wikilink")
[Category:美加邊界](../Category/美加邊界.md "wikilink")
[Category:密西根州地理](../Category/密西根州地理.md "wikilink")
[Category:安大略省地理](../Category/安大略省地理.md "wikilink")