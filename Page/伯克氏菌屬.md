**伯克氏菌屬**，或**伯克氏菌**，又譯**伯克霍爾德菌**（[學名](../Page/學名.md "wikilink")*Burkholderia*），是[伯克氏菌科的一個](../Page/伯克氏菌科.md "wikilink")[屬](../Page/屬.md "wikilink")，這個屬下最出名的有[鼻疽伯克氏菌](../Page/鼻疽伯克氏菌.md "wikilink")（*B.
mallei*），是一種會在[馬或其他相關](../Page/馬.md "wikilink")[動物身上引起](../Page/動物.md "wikilink")[馬鼻疽的](../Page/馬鼻疽.md "wikilink")[病菌](../Page/病菌.md "wikilink")；屬下亦有引起[类鼻疽的](../Page/类鼻疽.md "wikilink")[類鼻疽伯克氏菌](../Page/類鼻疽伯克氏菌.md "wikilink")（*B.
pseudomallei*）及引起[人類](../Page/人類.md "wikilink")[肺部](../Page/肺部.md "wikilink")[囊肿性纤维化感染的](../Page/囊肿性纤维化.md "wikilink")[洋蔥伯克氏菌](../Page/洋蔥伯克氏菌.md "wikilink")（*B.
cepacia*）。

伯克氏菌屬（以前被认为是[假單胞菌屬的一部份](../Page/假單胞菌屬.md "wikilink")）是一种好氧，能运动，棒状的[革蘭氏陰性菌](../Page/革蘭氏陰性菌.md "wikilink")。这一属中包括一些植物和动物的病原菌以及一些重要的环境微生物。其中*B.
xenovorans*是著名的[殺蟲劑及](../Page/殺蟲劑.md "wikilink")[多氯聯苯的生物降解者](../Page/多氯聯苯.md "wikilink")。伯克氏菌可用于[生物降解](../Page/生物降解.md "wikilink")、农业中的生物控制及促進植物生長的根圈微生物等。但由於有可能對人類造成的潜在危害，这些用途仍然处在商討之中。由於伯克氏菌具有相对较高的[抗生素的抗藥性及高運動性](../Page/抗生素.md "wikilink")，鼻疽伯克氏菌及類鼻疽伯克氏菌有時會被認為是針對[家畜及人類的](../Page/家畜.md "wikilink")[生物戰潛在媒介](../Page/生物戰.md "wikilink")。

其属名伯克氏菌（*Burkholderia*）来源于其发现者，康奈尔大学的植物生理学家Walter H. Burkholder.

## 參見

  - [細菌分類表](../Page/細菌分類表#β-變形菌綱\(Betaproteobacteria\).md "wikilink")

[Category:变形菌门](../Category/变形菌门.md "wikilink")