**伯利恒**（，意为“肉篮子”；，意为“[面包房](../Page/面包.md "wikilink")”；；[天主教譯](../Page/天主教.md "wikilink")**白冷**）是一座位于[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")[西岸地区的](../Page/西岸地区.md "wikilink")[城市](../Page/城市.md "wikilink")，座落在[耶路撒冷以南](../Page/耶路撒冷.md "wikilink")10[公里处](../Page/公里.md "wikilink")，人口約3萬人。该城是[伯利恒省的](../Page/伯利恒省.md "wikilink")[首府](../Page/首府.md "wikilink")，也是当地文化和旅游业的中心\[1\]\[2\]。

对于[基督教而言](../Page/基督教.md "wikilink")，伯利恒是[耶稣的出生地](../Page/耶稣.md "wikilink")，也是世界上最早出现基督徒团体的地方之一，不过近年来由于移民的原因，该城基督徒团体的规模已有所收缩\[3\]。根据《[圣经](../Page/圣经.md "wikilink")》记载，伯利恒也是[大卫的出生地和加冕成为以色列国王的地方](../Page/大卫.md "wikilink")。城外有对[犹太教有重要意义的](../Page/犹太教.md "wikilink")[拉结墓](../Page/拉结.md "wikilink")。

伯利恒在历史上曾经被众多的帝国所统治。目前，[以色列控制着伯利恒的进出口](../Page/以色列.md "wikilink")，而日常行政由[巴勒斯坦民族权力机构进行管理](../Page/巴勒斯坦民族权力机构.md "wikilink")\[4\]
在现代，伯利恒以穆斯林占多数，但仍然拥有巴勒斯坦最大的基督徒社区之一\[5\]。伯利恒都市聚集区包括毗邻的2个城镇：Beit Jala和Beit
Sahour，以及Aida和Beit
Jibrin难民营。伯利恒的经济支柱是旅游业，尤其是在[圣诞节期间](../Page/圣诞节.md "wikilink")，由于该市拥有[圣诞教堂](../Page/圣诞教堂.md "wikilink")，成为基督徒的朝圣地。伯利恒拥有30多家旅馆和300个手工业作坊\[6\]。

## 历史

### 聖經时代

该城位于多山的[犹大王国](../Page/犹大王国.md "wikilink")，最初叫“以法他”\[7\]，意为“富饶”；又名“伯利恒以法他”\[8\]、“犹大伯利恒”\[9\]、和“[大卫之城](../Page/大卫.md "wikilink")”\[10\]。[圣经](../Page/圣经.md "wikilink")第一次提到该城，是说到[雅各最钟爱的妻子](../Page/雅各_\(旧约人物\).md "wikilink")[拉结在生](../Page/拉结.md "wikilink")[便雅悯时](../Page/便雅悯.md "wikilink")，难产死在这里，埋葬在通往以法他的路边，就在城市的北面\[11\]。而根据[路得记的记载](../Page/路得记.md "wikilink")，山谷向东就是[摩押人女子](../Page/摩押人.md "wikilink")[路得故事的发生地点](../Page/路得.md "wikilink")，那里就是她来到财主[波阿斯的田地拾取麦穗的地方](../Page/波阿斯.md "wikilink")，也有她和婆婆[拿俄米回到这座城市的小路](../Page/拿俄米.md "wikilink")。

伯利恒也是以色列第二位国王[大卫的出生地](../Page/大卫.md "wikilink")，也是他受先知[撒母耳用](../Page/撒母耳.md "wikilink")[圣膏油膏抹](../Page/圣膏油.md "wikilink")，成為以色列國王的地方\[12\]；当大卫在[亚杜兰洞时](../Page/亚杜兰洞.md "wikilink")，三个勇士冒着生命危险闯过[非利士人的营地](../Page/非利士人.md "wikilink")，到伯利恒城门旁的井里打水献给大卫\[13\]。

### 耶稣的出生地

[Jesus_birthplace_in_Bethlehem.jpg](https://zh.wikipedia.org/wiki/File:Jesus_birthplace_in_Bethlehem.jpg "fig:Jesus_birthplace_in_Bethlehem.jpg")

在[耶稣出生以前的数百年](../Page/耶稣.md "wikilink")，以色列的[先知](../Page/先知.md "wikilink")[弥迦在](../Page/弥迦.md "wikilink")《[弥迦书](../Page/弥迦书.md "wikilink")》的第5章第2节（“伯利恒以法他阿，你在犹大诸城中为小，将来必有一位从你那里为我而出，在以色列中作掌权者；祂是从亘古，从太初而出。”）中，预言[弥赛亚将要出生在伯利恒](../Page/弥赛亚.md "wikilink")。根据[路加福音](../Page/路加福音.md "wikilink")2章4节的记载，[耶稣的父母本来居住在](../Page/耶稣.md "wikilink")[拿撒勒](../Page/拿撒勒.md "wikilink")，但是由于罗马皇帝[奥古斯都下令在全国范围内普查人口](../Page/奥古斯都.md "wikilink")，而回到祖先大卫的城伯利恒，因此耶稣就出生在伯利恒。此后，他们全家又回到了拿撒勒。[马太福音](../Page/马太福音.md "wikilink")2章则记载[希律杀死了伯利恒城内及其四境所有](../Page/希律.md "wikilink")2岁以内的男孩\[14\]。在这卷福音书中，记载耶稣的养父约瑟在梦中得到警告，全家得以及时逃往埃及，躲过了这次屠杀，直到希律死后才返回。约瑟再一次在梦中受到天使的警告，没有返回犹大，而是全家前往[加利利](../Page/加利利.md "wikilink")，定居在拿撒勒城\[15\]。

根据[福音书](../Page/福音书.md "wikilink")（[路加福音](../Page/路加福音.md "wikilink")2章4节和[马太福音](../Page/马太福音.md "wikilink")2章1节），[耶稣正是他们等待的弥赛亚](../Page/耶稣.md "wikilink")，降生在伯利恒，尽管他在[拿撒勒长大](../Page/拿撒勒.md "wikilink")。马太记载[希律在耶稣降生后杀害了伯利恒城里以及四境所有两岁以下的孩子](../Page/希律.md "wikilink")\[16\]。

### 罗马和拜占廷时期

[BethlehemInsideCN.jpg](https://zh.wikipedia.org/wiki/File:BethlehemInsideCN.jpg "fig:BethlehemInsideCN.jpg")

在132年-135年的Bar Kokhba's
起义期间，罗马人将城市摧毁，并在基督降生地的遗址上建造了一座供奉[阿多尼斯的神庙](../Page/阿多尼斯.md "wikilink")。直到326年，第一位基督徒皇帝[康斯坦丁一世的母亲](../Page/康斯坦丁一世.md "wikilink")[海伦娜太后在访问伯利恒时](../Page/海伦娜.md "wikilink")，才建起了第一座基督教堂\[17\]。

在529年的[撒玛利亚人起义期间](../Page/撒玛利亚人.md "wikilink")，伯利恒遭到洗劫，城墙和[圣诞教堂遭到毁坏](../Page/圣诞教堂.md "wikilink")，但是不久根据东罗马皇帝[查士丁尼一世的命令得到重建](../Page/查士丁尼一世.md "wikilink")。614年，[波斯人侵入巴勒斯坦](../Page/波斯人.md "wikilink")，征服了伯利恒。一个稍后的文献中记载了波斯人在圣诞教堂看见用马赛克镶嵌的[东方三博士的波斯风格的服装](../Page/东方三博士.md "wikilink")，就克制了毁坏圣诞教堂的行动。\[18\]

### 阿拉伯统治和十字軍

637年，在[耶路撒冷被穆斯林军队占领之后不久](../Page/耶路撒冷.md "wikilink")，第二任哈里发[奥马尔一世访问伯利恒](../Page/奥马尔一世.md "wikilink")，应许圣诞教堂将继续给基督徒使用\[19\]。但是在教堂旁边，他祈祷过的地方，建起了奥马尔清真寺\[20\]。

1099年，[十字军占领了伯利恒](../Page/十字军.md "wikilink")，他们加固了城防，并且新修了一座修道院，还修建了圣诞教堂北面的回廊，希腊东正教神职人员受到驱逐，代之以罗马天主教的神职人员。1100年圣诞节，法兰克[耶路撒冷王国的第一位国王鲍尔温一世在伯利恒加冕](../Page/耶路撒冷王国.md "wikilink")，同年，在该市设立了[天主教主教区](../Page/天主教.md "wikilink")\[21\]。

1187年，埃及和叙利亚的苏丹[萨拉丁从十字军手中夺取了伯利恒](../Page/萨拉丁.md "wikilink")，罗马天主教神职人员被迫离开，希腊东正教神职人员得以重返。1192年，萨拉丁同意让2名罗马天主教神父和2名助祭返回伯利恒。不过，由于欧洲朝圣者大为减少，该城丧失了朝圣贸易。在1229年－1244年之间，根据一项条约，伯利恒又曾短暂地处于十字军的控制之下。1250年，由于[拜巴尔一世的掌权](../Page/拜巴尔一世.md "wikilink")，对基督教的宽容结束了，神职人员离开该市。1263年，伯利恒的城墙被拆除。在下一个世纪，罗马天主教神职人员再度返回该市，在圣诞教堂的隔壁建立起自己的修道院。希腊东正教会获得管理圣诞教堂的权利，并同罗马天主教会和亚美尼亚正教会共同获得管理圣诞洞穴的权利\[22\]。

[Bethlehem_Polenov.jpg](https://zh.wikipedia.org/wiki/File:Bethlehem_Polenov.jpg "fig:Bethlehem_Polenov.jpg")

### 奥斯曼帝国

[Karl_Oenike,_Einzug_der_Pilger_in_Bethlehem_(1894).jpg](https://zh.wikipedia.org/wiki/File:Karl_Oenike,_Einzug_der_Pilger_in_Bethlehem_\(1894\).jpg "fig:Karl_Oenike,_Einzug_der_Pilger_in_Bethlehem_(1894).jpg")

从1517年开始，奥斯曼帝国控制了伯利恒，天主教和东正教之间为圣诞教堂的管理权进行痛苦的争议\[23\]。从1831年到1841年，巴勒斯坦在[穆罕默德·阿里
(埃及)的统治之下](../Page/穆罕默德·阿里_\(埃及\).md "wikilink")。在这期间，伯利恒经历了一次地震，以及穆斯林区被阿里的军队所摧毁，显然是对一场谋杀的报复。1841年，伯利恒回到奥斯曼帝国的统治之下，直到[第一次世界大战末期](../Page/第一次世界大战.md "wikilink")\[24\]。

### 20世纪

由于英法等协约国在第一次世界大战中获胜，在战后获得了托管所占领的奥斯曼帝国省份的权利。1923年9月29日，伯利恒和约旦河西岸巴勒斯坦大部分领土归属英国托管。1947年，[联合国大会解决巴勒斯坦争端](../Page/联合国大会.md "wikilink")，通过[巴勒斯坦分治决议](../Page/联合国大会181号决议.md "wikilink")，伯利恒包括在由[联合国管理的](../Page/联合国.md "wikilink")[耶路撒冷特别国际飞地内](../Page/耶路撒冷.md "wikilink")。\[25\]

在[1948年以阿战争期间](../Page/1948年以阿战争.md "wikilink")，[约旦占领了伯利恒](../Page/约旦.md "wikilink")\[26\]。1947年－1948年，许多来自以色列人占领区的难民来到伯利恒，主要居住在城北去耶路撒冷道路旁的Beit
Jibrin（al-'Azza）、Aida和城南的Deheisheh（山下和所罗门池之间的地方），后来成为正式的难民营\[27\]。这些难民改变了伯利恒的种族构成。

1967年，以色列从约旦手中夺取了伯利恒和整个[西岸地区](../Page/西岸地区.md "wikilink")。1995年12月24日，伯利恒被划为“A区”，成为[巴勒斯坦民族权力机构完全控制的地区](../Page/巴勒斯坦民族权力机构.md "wikilink")\[28\]。

### 第二次巴勒斯坦大起义

在2000年开始的[第二次巴勒斯坦大起义期间](../Page/第二次巴勒斯坦大起义.md "wikilink")，伯利恒的基础设施和旅游业遭受重创。\[29\]\[30\]
2002年，[以色列国防军发动大规模军事进攻](../Page/以色列国防军.md "wikilink")，大约200名巴勒斯坦人，包括一批武装人员逃进圣诞教堂寻求庇护。以色列国防军对伯利恒圣诞教堂的围困持续了5周，有9名巴勒斯坦武装人员和教堂的敲钟人被屠杀。最后，13名巴勒斯坦抵抗人员被送往不同的欧洲和非洲国家，围困结束\[31\]。

## 地理与气候

伯利恒的海拔高度大约为775米，比邻近的耶路撒冷高30米。\[32\]伯利恒位于犹大山地的南部。该市位于[加沙和](../Page/加沙.md "wikilink")[地中海东南方](../Page/地中海.md "wikilink")73公里，[约旦](../Page/约旦.md "wikilink")[安曼以西](../Page/安曼.md "wikilink")75公里，[以色列](../Page/以色列.md "wikilink")[特拉维夫东南方](../Page/特拉维夫.md "wikilink")59公里。\[33\]附近的城市有：北面的Beit
Safafa和耶路撒冷，西面的Beit Jala，南面的Beit Fajjar和东面的Beit Sahur。Beit Jala
后面的市镇与伯利恒构成了一个都市集结区。

伯利恒的冬季，从12月中旬到3月中旬，气候寒冷多雨。1月是最冷的月份，气温介于1－13摄氏度。从5月到9月，气候温暖晴朗。8月是最热的月份，气温高达27摄氏度。伯利恒的年平均降水量为700毫米，其中70%的降水集中于11月到1月。\[34\]

## 人口

| 年份               | 人口           |
| ---------------- | ------------ |
| 1945             | 8,820\[35\]  |
| 1961             | 22,450\[36\] |
| 1983             | 16,300       |
| 1997             | 21,930\[37\] |
| 2004 (Projected) | 28,010       |
| 2005 (Projected) | 29,020       |
| 2006 (Projected) | 29,930       |
|                  |              |

根据巴勒斯坦中央统计署资料，2007年中伯利恒人口为25,266人。\[38\]而在1997年人口统计时，该市人口为21,670人，其中包括6,570名[巴勒斯坦难民](../Page/巴勒斯坦难民.md "wikilink")，占该市人口的30.3%\[39\]\[40\]。1998年，该市的宗教构成为：[逊尼派穆斯林占](../Page/逊尼派穆斯林.md "wikilink")67%，[基督徒占](../Page/基督徒.md "wikilink")33%，其中大部分是希腊东正教和罗马天主教。\[41\]2005年，基督徒人口总数下降到只占20%。\[42\]尽管伊斯兰教已经成为伯利恒的主要宗教，但该城仍然只有一座[清真寺](../Page/清真寺.md "wikilink")，就是位于马槽广场的奥玛尔清真寺。\[43\]

1997年，伯利恒居民的年龄组成为：10岁以下者占27.4%，10-19岁者占20%，20-29岁者占17.3%，30-44岁者占17.7%，45-64岁者占12.1%，65岁以上者占5.3%。男性共有11,079人，女性有10,594人。\[44\]

### 种族变化

由于移民以及出生率较低等原因，伯利恒的基督徒比重持续下降。1947年，基督徒占伯利恒人口的75%，但到了1998年，比重已经下降到33%\[45\]\[46\]。在2000年－2003年期间，伯利恒有2000名基督徒移居外地，穆斯林占人口多数\[47\]。伯利恒现任市长维克多·巴塔尔萨博士告诉[美国之音记者](../Page/美国之音.md "wikilink")：“由于来自身心两方面的压力，和恶劣的经济形势，许多人，包括基督徒和穆斯林都移居外地，但显然出走的基督徒更多，因为他们已经是少数了。”\[48\]

</blockquote>

根据临时协定进行统治的巴勒斯坦民族权力机构的官方态度表示同意伯利恒地区的基督徒拥有平等地位，不过还是有武装派别少数针对基督徒的暴力事件。\[49\]
[第二次巴勒斯坦大起义的爆发以及随之而来的旅游业衰退已经使基督徒少数派受到影响](../Page/第二次巴勒斯坦大起义.md "wikilink")，由于他们是伯利恒许多旅馆和针对外国游客的服务业的拥有者，因而在经济上遭受重大打击。\[50\]
一项关于基督徒为何离开该市的统计分析归因于缺少经济和教育机会，特别是因为基督徒的中产阶级身份和较高的教育水准。\[51\]

2006年，巴勒斯坦人研究与文化对话中心对伯利恒基督徒进行了一次民意测验，发现90%的人都有穆斯林朋友，73.3%的人认为巴勒斯坦民族权力机构尊重该市的基督教遗产，78%的人将大批逃离该市的原因归因于以色列对该地区的旅行限制。\[52\]

该市的基督徒拒絕穆斯林恐怖分子[哈马斯](../Page/哈马斯.md "wikilink")，尽管有一些居民因为伊斯兰教在该市的影响增加而匿名批评该穆斯林恐怖分子，例如不习惯当地清真寺频繁地召集祈祷。在哈马斯穆斯林恐怖分子控制下，基督徒要继续忍受缺少法律和秩序的现状，当地黑手党利用低效率的法庭以及认为基督徒可能较少保护自己，偷窃活动盛行。\[53\]\[54\]\[55\]

## 经济

[Bethlehem-Manger-Square.jpg](https://zh.wikipedia.org/wiki/File:Bethlehem-Manger-Square.jpg "fig:Bethlehem-Manger-Square.jpg")
[Bethlehem-03-Church_of_the_Nativity.jpg](https://zh.wikipedia.org/wiki/File:Bethlehem-03-Church_of_the_Nativity.jpg "fig:Bethlehem-03-Church_of_the_Nativity.jpg")

### 商业

商业是伯利恒的主要产业，特别是在[圣诞节期间](../Page/圣诞节.md "wikilink")。该市的主要街道和市场布满了出售用当地[橄榄树雕刻成的手工艺品](../Page/橄榄树.md "wikilink")、香料、珠宝和果仁蜜饼等食品的店铺。\[56\]\[57\]宗教用品是伯利恒的重要产业，包括用橄榄树和[珍珠母制作的装饰品](../Page/珍珠母.md "wikilink")，橄榄木雕像、盒子和十字架。\[58\]

从1885年开始，伯利恒酿造Cremisan 酒，目前向几个国家出口。这种酒由 Cremisan修道院的修士酿造，所用大部分葡萄来自
al-Khader 地区。这个修道院每年产酒700,000 升\[59\]。

### 旅游业

2000年以前，伯利恒与巴勒斯坦其余城市不同，以旅游业为当地主要产业，因此大多数居民不在以色列工作。\[60\]
旅游收入占该市经济的65%。\[61\]

[圣诞教堂是伯利恒主要的游览胜地和基督徒朝圣中心](../Page/圣诞教堂.md "wikilink")。它位于伯利恒的中心[马槽广场](../Page/马槽广场.md "wikilink")，建在一个叫“圣穴”的地穴之上，据基督教传统这里是耶稣降生的地方。由[康斯坦丁大帝建造于公元](../Page/君士坦丁一世_\(羅馬帝國\).md "wikilink")330年，这也许是世界上现存的最古老的教堂。靠近它的另一个洞穴，据说拉丁教父[耶柔米在那里花了三十年时间](../Page/耶柔米.md "wikilink")，将圣经翻译成[拉丁语](../Page/拉丁语.md "wikilink")（[武加大译本](../Page/武加大译本.md "wikilink")）\[62\]。

## 文化

[Bethlehem_woman_edited.jpg](https://zh.wikipedia.org/wiki/File:Bethlehem_woman_edited.jpg "fig:Bethlehem_woman_edited.jpg")
[Bethlehem_Christmas2.JPG](https://zh.wikipedia.org/wiki/File:Bethlehem_Christmas2.JPG "fig:Bethlehem_Christmas2.JPG")

### 服饰

在以色列建国以前，伯利恒的服饰在整个犹大山地和沿海平原的村镇广为流行。伯利恒和周围村庄的刺绣女工以专业制作结婚礼服而著称。\[63\]
伯利恒是生产一种“全面色彩效果和金属光泽”的刺绣的中心\[64\]。

伯利恒的礼服通常为靛青色织物，用当地羊毛织成的无袖上装（*bisht*）。特殊场合的女装则用有斑纹的丝绸制成，通常都有复杂的刺绣。\[65\]

伯利恒服饰的独特之处在于将金线、银线或丝线绣在丝绸、羊毛、毛毡或天鹅绒上，制成衣服，设计了程式化的 floral patterns with
free or rounded lines。这种技术用于制作高贵的结婚礼服（*thob
malak*），和已婚妇女穿着的*shatwehs*。其中有一些可以追溯到[拜占庭时期](../Page/拜占庭.md "wikilink")，有些可以追溯到奥斯曼帝国贵族的正式装束。由于伯利恒是一个基督徒城镇，当地妇女也能够用刺绣和银色锦缎设计制作教堂圣衣。\[66\]

### 博物馆

伯利恒拥有4座博物馆。圣诞剧院与博物馆向游客提供31个3D模型，描绘耶稣一生的各个重要阶段。它的剧院放映长度为20分钟的短片。Badd
Giacaman 博物馆将时间带回到18世纪，主要展示历史，以及橄榄油的生产过程。Baituna al-Talhami
博物馆成立于1972年，展示伯利恒居民的文化。\[67\]国际圣诞博物馆由[UNESCO设计](../Page/UNESCO.md "wikilink")，旨在展示“在一个唤起的气氛中的艺术上高品质”的作品。\[68\]

### 文化中心

伯利恒的巴勒斯坦遗产中心成立于1991年，旨在保护和促进巴勒斯坦服饰、艺术和民间传说。\[69\]另一个文化中心——伯利恒国际中心则集中展示伯利恒当地文化。

### 圣诞庆祝

在伯利恒，圣诞节仪式在3个不同的日期举行：罗马天主教和新教各教派在12月24日庆祝，希腊东正教会、科普特正教会、叙利亚正教会的基督徒在1月6日庆祝圣诞节，而亚美尼亚正教会在1月19日庆祝圣诞节\[70\]。大部分圣诞游行通过圣诞教堂外面的马槽广场。天主教仪式在圣凯瑟琳教堂举行，而新教仪式在
Shepherds' Fields 举行。\[71\]

## 政府

[Bethlehem-hamasrally.JPG](https://zh.wikipedia.org/wiki/File:Bethlehem-hamasrally.JPG "fig:Bethlehem-hamasrally.JPG")集会\]\]
伯利恒市政委员会拥有15名选举产生的成员，包括市长和副市长。一项特别条款规定该市的市长和市政委员会多数成员必须是基督徒，其余席位则不受宗教限制。\[72\]
市政委员会中包括几个政党的成员，[巴勒斯坦解放组织的左翼派别](../Page/巴勒斯坦解放组织.md "wikilink")，例如[解放巴勒斯坦人民阵线和巴勒斯坦人民党通常支配了备用席位](../Page/解放巴勒斯坦人民阵线.md "wikilink")。2005年，哈马斯赢得了大多数开放席位。伯利恒是伯利恒行政区的首府。\[73\]

## 教育

根据巴勒斯坦中央统计署资料，1997年，伯利恒10岁以上人口的识字率为84%。该市人口中，10,414人正在学校就读（小学4,015人，初中3,578人，高中2,821人）。大约14.1%的高中生获得毕业证书。\[74\]2006年，在伯利恒共有135所学校；100所由巴勒斯坦民族权力机构教育部管理，7所[United
Nations Relief and Works
Agency](../Page/United_Nations_Relief_and_Works_Agency_for_近东巴勒斯坦难民.md "wikilink")
(UNRWA)，28所为私立\[75\]。

[伯利恒大学由罗马天主教会以](../Page/伯利恒大学.md "wikilink")[喇沙會的傳統创办于](../Page/喇沙會.md "wikilink")1973年，招收各种宗教信仰的学生。伯利恒大学是西岸地区最早建立的大学，並可追溯到1893年當[喇沙會在巴勒斯坦和埃及各地開辦學校的時候](../Page/喇沙會.md "wikilink")。\[76\]

## 交通

[Bethlehem-street2.JPG](https://zh.wikipedia.org/wiki/File:Bethlehem-street2.JPG "fig:Bethlehem-street2.JPG")
[BethlehemRoad.jpg](https://zh.wikipedia.org/wiki/File:BethlehemRoad.jpg "fig:BethlehemRoad.jpg")

### 公共汽车

伯利恒拥有4个私人经营的汽车站，开通前往[耶路撒冷](../Page/耶路撒冷.md "wikilink")、[希伯仑](../Page/希伯仑.md "wikilink")、Nahalin、Battir和Beit
Fajjar 的班车。持西岸执照的公共汽车和出租车未经允许不允许进入以色列，包括耶路撒冷。\[77\]

### 行动限制

伯利恒到耶路撒冷的主要道路在耶路撒冷的市区边界处（拉结墓）被切断
[1](http://news.bbc.co.uk/2/hi/middle_east/4490671.stm)
。伯利恒居民只允许经过特别许可前往耶路撒冷（该地区主要的社会、经济和宗教中心），通常也被拒绝。到西岸巴勒斯坦控制区其他地方旅行也有限制，有时受到阻止。该市定期进行严格的宵禁，不允许居民离开家。

以色列修筑的西岸栅栏已经对伯利恒产生了巨大的政治、社会和经济影响。栅栏经过该市建成区的北侧，距离阿依达难民营和另外一边的[耶路撒冷市边界都仅有数米远](../Page/耶路撒冷.md "wikilink")。\[78\]

从伯利恒地区到[西岸地区其他地方的大部分进出口現已被以色列的檢查哨和路障所控制](../Page/西岸地区.md "wikilink")，限制的严格程度受到以色列安全部门的指示。伯利恒的巴勒斯坦居民前往耶路撒冷需要通行证。\[79\]
在过去获得通行证进入，served 在许多方面 as an urban anchor to 伯利恒，已经非常罕见
自从第二次巴勒斯坦大起义的暴力活动开始后，以色列随后竖起了一个终点站，以减少2个邻近城市之间的通行。\[80\]\[81\]

巴勒斯坦人未经许可不允许进入市郊的犹太人圣地拉结墓。由于伯利恒和附近的圣经中的[所罗门池位于](../Page/所罗门池.md "wikilink")[A区](../Page/奥斯陆协定.md "wikilink")，[以色列公民没有以色列军事当局的允许](../Page/以色列.md "wikilink")，也不得前往那里。\[82\]

## 友好城市

伯利恒拥有下列友好城市：\[83\]

  - [Marrickville](../Page/Marrickville,_New_South_Wales.md "wikilink"),
    [澳大利亚](../Page/澳大利亚.md "wikilink")

  - [Steyr](../Page/Steyr.md "wikilink"),
    [奥地利](../Page/奥地利.md "wikilink")

  - [纳塔尔](../Page/纳塔尔.md "wikilink")，[巴西](../Page/巴西.md "wikilink")

  - [Valinhos](../Page/Valinhos.md "wikilink"),
    [巴西](../Page/巴西.md "wikilink")

  - [Třebechovice pod
    Orebem](../Page/Třebechovice_pod_Orebem.md "wikilink"),
    [捷克共和国](../Page/捷克共和国.md "wikilink")

  - [Villa Alemana](../Page/Villa_Alemana.md "wikilink"),
    [智利](../Page/智利.md "wikilink")

  - [康赛浦西翁](../Page/Concepción,_Chile.md "wikilink"),
    [智利](../Page/智利.md "wikilink")

  - [Paray-le-Monial](../Page/Paray-le-Monial.md "wikilink"),
    [法国](../Page/法国.md "wikilink")

  - [斯特拉斯堡](../Page/斯特拉斯堡.md "wikilink")，[法国](../Page/法国.md "wikilink")

  - [雅典](../Page/雅典.md "wikilink")，[希腊](../Page/希腊.md "wikilink")

  - [科隆](../Page/科隆.md "wikilink")，[德国](../Page/德国.md "wikilink")\[84\]

  - [佛罗伦萨](../Page/佛罗伦萨.md "wikilink")，[意大利](../Page/意大利.md "wikilink")

  - [Greccio](../Page/Greccio.md "wikilink"),
    [意大利](../Page/意大利.md "wikilink")

  - [米兰](../Page/米兰.md "wikilink")，[意大利](../Page/意大利.md "wikilink")

  - [阿西西](../Page/阿西西.md "wikilink")，[意大利](../Page/意大利.md "wikilink")

  - [奥维多](../Page/奥维多.md "wikilink")，[意大利](../Page/意大利.md "wikilink")

<!-- end list -->

  - [帕维亚](../Page/帕维亚.md "wikilink"),
    [意大利](../Page/意大利.md "wikilink")\[85\]

  - [维罗纳](../Page/维罗纳.md "wikilink")，[意大利](../Page/意大利.md "wikilink")

  - [蒙特雷](../Page/蒙特雷.md "wikilink"), [墨西哥](../Page/墨西哥.md "wikilink")

  - [拉巴特](../Page/拉巴特.md "wikilink")，[摩洛哥](../Page/摩洛哥.md "wikilink")

  - [海牙](../Page/海牙.md "wikilink")，[荷兰](../Page/荷兰.md "wikilink")

  - [Sarpsborg](../Page/Sarpsborg.md "wikilink")，[挪威](../Page/挪威.md "wikilink")

  - [库斯科](../Page/库斯科.md "wikilink")，[秘鲁](../Page/秘鲁.md "wikilink")

  - [圣彼得堡](../Page/圣彼得堡.md "wikilink")，[俄罗斯](../Page/俄罗斯.md "wikilink")

  - [比勒陀利亚](../Page/比勒陀利亚.md "wikilink")，[南非](../Page/南非.md "wikilink")

  - [萨拉戈萨](../Page/萨拉戈萨.md "wikilink")，[西班牙](../Page/西班牙.md "wikilink")\[86\]

  - [科尔多瓦](../Page/科尔多瓦.md "wikilink")，[西班牙](../Page/西班牙.md "wikilink")

  - [Leganés](../Page/Leganés.md "wikilink"),
    [西班牙](../Page/西班牙.md "wikilink")

  - [格拉斯哥](../Page/格拉斯哥.md "wikilink")，[英国](../Page/英国.md "wikilink")

  - [Burlington](../Page/Burlington,_Vermont.md "wikilink"),
    [美国](../Page/美国.md "wikilink")

  - [奥兰多](../Page/奥兰多.md "wikilink")，[美国](../Page/美国.md "wikilink")

## 參考文献

## 外部链接

  - [伯利恒市网站](http://www.bethlehem-city.org)
  - [Franciscan Custody of the Holy Land website - pages on
    伯利恒](https://web.archive.org/web/20151105212027/http://www.christusrex.org/www1/ofm/sites/TSbtmenu.html)
  - [伯利恒 2000
    project](https://web.archive.org/web/20060321000427/http://www.bethlehem2000.org/main.html)
  - [Open 伯利恒civil society project](http://www.openbethlehem.org/)
  - [伯利恒大学](http://www.bethlehem.edu)
  - [Deheisheh
    难民营（阿拉伯语）](https://web.archive.org/web/20080317080849/http://www.dheisheh.ps/)
  - [WikiVoyage: 伯利恒](https://en.wikivoyage.org/wiki/Bethlehem)
  - [Craft products from 伯利恒](http://www.hadeel.org/)
  - [伯利恒和平中心](https://web.archive.org/web/20071008005239/http://www.peacenter.org/)
  - [Bible Land
    Library](https://web.archive.org/web/20080221144101/http://www.biblelandshop.net/LIBRARY/Library10.html)
  - [Bethlehem: Muslim-Christian living
    together](http://www.palestine-family.net/index.php?nav=65&hits=20&searchword=al+khader&pageflip=26-22&did=604-1&searchResult=searchResult)
  - [Photo Gallery of Bethlehem
    from 2007](http://www.ianandwendy.com/Israel/Bethlehem/slideshow.htm)

## 參見

  - [耶稣降生](../Page/耶稣降生.md "wikilink")
      - [伯利恆之星](../Page/伯利恆之星.md "wikilink")
      - [圣诞教堂](../Page/圣诞教堂.md "wikilink")
  - [巴勒斯坦行政区划](../Page/巴勒斯坦行政区划.md "wikilink")
      - [伯利恒省](../Page/伯利恒省.md "wikilink")

{{-}}

[Category:伯利恒省](../Category/伯利恒省.md "wikilink")
[伯利恒](../Category/伯利恒.md "wikilink")
[Category:巴勒斯坦城市](../Category/巴勒斯坦城市.md "wikilink")
[Category:巴勒斯坦基督徒地区](../Category/巴勒斯坦基督徒地区.md "wikilink")
[Category:圣城](../Category/圣城.md "wikilink")
[Category:希伯来圣经中的地名](../Category/希伯来圣经中的地名.md "wikilink")
[Category:摩西五经中的城市](../Category/摩西五经中的城市.md "wikilink")

1.

2.

3.

4.

5.
6.

7.  创世纪35章16节、19节，48章7节；路得记4:11

8.  弥迦书5:2

9.  撒母耳记上17:12

10. 路加福音2:4

11. 创世纪48章7节

12. [撒母耳记上](../Page/撒母耳记.md "wikilink")16章4-13节

13. [撒母耳记下](../Page/撒母耳记.md "wikilink")23章13-17节

14. 马太福音2章16、18节；耶利米书31章15节

15. 马太福音2章22-23节

16. 马太福音2章16、18节；耶利米书31章15节

17.
18.
19.
20.

21.
22.
23.
24.
25.

26. [耶路撒冷年表，3,000年的城市史](http://www.insideout.org/documentaries/jerusalem/land/timeline2.html)
     (2001-02) National Public Radio和BBC News

27. [关于伯利恒](http://www.bethlehem.ps/about/)  伯利恒文化遗产保护中心

28.

29.

30.

31.

32.

33. [从伯利恒到特拉维夫的距离](http://www.timeanddate.com/worldclock/distanceresult.html?p1=1048&p2=676)，[从伯利恒到加沙的距离](http://www.timeanddate.com/worldclock/distanceresult.html?p1=1048&p2=702)
    Time and Date AS / Steffen Thorsen

34.

35.

36. 以色列中央统计署资料

37. [巴勒斯坦人口，按地区、性别和年龄分组
    伯利恒行政区](http://www.pcbs.gov.ps/Portals/_pcbs/phc_97/bet_t1.aspx)
     (1997) 巴勒斯坦中央统计署。于2007-12-23查阅

38.
39.
40.

41.

42.

43.
44.
45.
46. 《國家地理雜誌》中文版，2007年12月號 No.84，第32頁，ISBN 4-712389-610089-12

47. [市长汉纳·纳赛尔接受采访](http://www.nytimes.com/2003/12/24/international/middleeast/24CND-MIDE.html?hp)

48.

49.

50.
51.

52.

53.

54.

55.

56.

57.

58.
59.

60.
61.

62.
63.

64.

65.
66.
67.
68.
69.

70.

71.

72.
73.

74.

75.

76.

77.

78.
79.

80.
81.

82.
83. [Twinning with
    Palestine](http://www.twinningwithpalestine.net/groupsinternational.html)
     Britain Palestine Twinning Network

84.

85.

86. [Zaragoza Internacional: HERMANAMIENTOS ZARAGOZA,
    ESPAÑA:](http://cmisapp.zaragoza.es/ciudad/zaragozainternacional/hermanamientos.htm)
     Ayuntamiento de Zaragoza