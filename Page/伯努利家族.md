**伯努利**（**Bernoulli**）家族是一个商人和学者家族，来自[瑞士](../Page/瑞士.md "wikilink")[巴塞尔](../Page/巴塞尔.md "wikilink")。家族的建立人，[莱昂·伯努利](../Page/莱昂·伯努利.md "wikilink")，于16世纪从[比利时](../Page/比利时.md "wikilink")[安特卫普移民到巴塞尔](../Page/安特卫普.md "wikilink")。很多艺术家和科学家出自伯努利家族，特别是18世纪：

  - [雅各布·伯努利](../Page/雅各布·伯努利.md "wikilink")（也做詹姆斯或雅各，1654–1705），在[概率方面的贡献突出](../Page/概率.md "wikilink")，以[伯努利分布而闻名](../Page/伯努利分布.md "wikilink")。
  - [约翰·伯努利](../Page/约翰·伯努利.md "wikilink")（1667–1748），雅各布的弟弟，以最速降线而闻名，教授过[欧拉](../Page/欧拉.md "wikilink")。
      - [小尼古拉·伯努利](../Page/小尼古拉·伯努利.md "wikilink")（1695–1726），约翰的大儿子
      - [丹尼尔·伯努利](../Page/丹尼尔·伯努利.md "wikilink")（1700–1782），约翰的小儿子，在[流体力学方面贡献突出](../Page/流体力学.md "wikilink")，以[伯努利定律而闻名](../Page/伯努利定律.md "wikilink")。
      - [大尼古拉·伯努利](../Page/大尼古拉·伯努利.md "wikilink")（1687–1759），雅各布和约翰的侄子。

## 世系图

## 參見

  - [伯努利盒](../Page/伯努利盒.md "wikilink")
  - [伯努利分布](../Page/伯努利分布.md "wikilink")
  - [伯努利数](../Page/伯努利数.md "wikilink")
  - [伯努利多项式](../Page/伯努利多项式.md "wikilink")
  - [伯努利检验](../Page/伯努利检验.md "wikilink")
  - [伯努利原理](../Page/伯努利原理.md "wikilink")
  - [白努利定律](../Page/白努利定律.md "wikilink")
  - [伯努利微分方程](../Page/伯努利微分方程.md "wikilink")

## 参考

  -
[Category:伯努利家族](../Category/伯努利家族.md "wikilink")