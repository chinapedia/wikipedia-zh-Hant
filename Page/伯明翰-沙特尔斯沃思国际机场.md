**伯明翰-沙特尔斯沃思國際機場**（，），是一座位於[美國](../Page/美國.md "wikilink")[阿拉巴馬州](../Page/阿拉巴馬州.md "wikilink")[伯明翰的民用機場](../Page/伯明翰_\(阿拉巴馬州\).md "wikilink")。該機場2005服務300萬，是[阿拉巴馬州最大机場](../Page/阿拉巴馬州.md "wikilink")。

## 歷史

## 擴充

當前有幾個資產改進項目進行中，包括$2千萬空運貨物複雜擴展並且復出空運承運人圍裙地區。
最近官員宣佈了包括增加另一個廣場和一個新的行李螢幕區的大堂擴展。大堂擴展和現代化，將在2008年初開始，也將包括更加現代的旅客便利，升級讓步和聯邦檢查服務批准的國際門。
終端擴展的估計成本是$161萬美元。

## 航點

[BHM_map.PNG](https://zh.wikipedia.org/wiki/File:BHM_map.PNG "fig:BHM_map.PNG")

伯明翰-沙特尔斯沃思國際機場現在有一個興建中的航廈。

### B大堂

  - [美國航空](../Page/美國航空.md "wikilink") (達拉斯)
      - [美國老鷹航空公司](../Page/美國老鷹航空公司.md "wikilink") (芝加哥-奥海爾)
  - [大陸航空](../Page/大陸航空.md "wikilink") (休斯敦-洲際)
      - [大陸快運由](../Page/大陸快運.md "wikilink")[ExpressJet航空公司經營](../Page/ExpressJet航空公司.md "wikilink")
        (休斯敦-洲際，紐約-紐瓦克)
  - [西北航空公司](../Page/西北航空公司.md "wikilink") (底特律，孟菲斯)
      - [西北航空l聯繫由](../Page/西北航空l聯繫.md "wikilink")[石峰航空公司經營](../Page/石峰航空公司.md "wikilink")
        (底特律，孟菲斯)
  - [全美航空](../Page/全美航空.md "wikilink")
      - [全美航空快運由](../Page/全美航空快運.md "wikilink")[Mesa航空公司](../Page/Mesa航空公司.md "wikilink")
        經營(夏洛特)
      - [全美航空快運由](../Page/全美航空快運.md "wikilink")[PSA航空公司經營](../Page/PSA航空公司.md "wikilink")
        (夏洛特)
      - [全美航空快運由](../Page/全美航空快運.md "wikilink")[共和國航空公司經營](../Page/共和國航空公司.md "wikilink")
        (夏洛特，費城)

### C大堂

  - [達美航空公司](../Page/達美航空公司.md "wikilink") (亞特蘭大)
      - [達美聯繫由](../Page/達美聯繫.md "wikilink")[大西洋東南航空公司](../Page/大西洋東南航空公司.md "wikilink")
        經營(亞特蘭大)
      - [達美聯繫由](../Page/達美聯繫.md "wikilink")[Chautauqua航空公司經營](../Page/Chautauqua航空公司.md "wikilink")
        (辛辛那提/北肯塔基)
      - [達美聯繫由](../Page/達美聯繫.md "wikilink")[Comair經營](../Page/Comair.md "wikilink")
        (辛辛那提或北肯塔基、紐約-拉瓜地亞，奧蘭多)
      - [達美聯繫由](../Page/達美聯繫.md "wikilink")[自由航空公司](../Page/自由航空公司.md "wikilink")
        經營(奧蘭多)
      - [達美聯繫由](../Page/達美聯繫.md "wikilink")[SkyWest經營](../Page/SkyWest.md "wikilink")
        (亞特蘭大，鹽湖城)
  - [ExpressJet航空公司](../Page/ExpressJet航空公司.md "wikilink")
    (新奧爾良、羅利或者Durham)
  - [西南航空](../Page/西南航空.md "wikilink")
    (巴爾的摩/華盛頓,芝加哥-中途，達拉斯愛田，休斯敦愛好，傑克遜維爾，拉斯維加斯，路易斯維爾，納稀威，新奧爾良\[11月4日開始\]，奧蘭多，菲尼斯，聖路易斯，坦帕)
  - [聯合航空公司](../Page/聯合航空公司.md "wikilink")
      - [聯合快運由](../Page/聯合快運.md "wikilink")[Mesa航空公司經營](../Page/Mesa航空公司.md "wikilink")
        (芝加哥O'Hare，華盛頓Dulles)
      - [聯合快運由](../Page/聯合快運.md "wikilink")[SkyWest經營](../Page/SkyWest.md "wikilink")
        (芝加哥O'Hare，丹佛)

## 外部連結

  - [伯明翰國際機場](http://www.bhamintlairport.com/)

[B](../Category/美國機場.md "wikilink")
[S](../Category/冠以人名的美洲機場.md "wikilink")