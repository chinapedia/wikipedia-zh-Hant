**伯沙撒**（Belshazzar，[阿卡德語](../Page/阿卡德語.md "wikilink")：Bel-sarra-usur）是[新巴比倫王國的最後一位統治者](../Page/新巴比倫王國.md "wikilink")（嚴格來說是共同攝政王），[那波尼德之子](../Page/那波尼德.md "wikilink")。

## 歷史及考古資料中的伯沙撒

伯沙撒的父王那波尼德在大約前549年離開首都[巴比倫城](../Page/巴比倫.md "wikilink")，前往阿拉伯沙漠的綠洲[泰馬](../Page/泰馬.md "wikilink")，專注於崇拜月神[南納](../Page/南納.md "wikilink")。那波尼德任命伯沙撒為共同攝政。

前540年，那波尼德為了防禦[波斯人入侵](../Page/波斯.md "wikilink")，從泰馬返回巴比倫。在前539年，伯沙撒留在首都防守，波斯的[居魯士二世在同年征服了巴比倫](../Page/居魯士二世.md "wikilink")，伯沙撒被殺，那波尼德被俘（一說投降）。\[1\]

## 聖經中的伯沙撒

[舊約聖經](../Page/舊約聖經.md "wikilink")[但以理書第](../Page/但以理書.md "wikilink")5章記載了關於伯沙撒的一些事情，後世有人質疑該等內容的準確性，亦有人就那些質疑提出了辯護。詳見[但以理條目](../Page/但以理.md "wikilink")。

## 参考

[Category:巴比倫](../Category/巴比倫.md "wikilink")
[Category:新巴比倫君主](../Category/新巴比倫君主.md "wikilink")
[Category:巴比倫人](../Category/巴比倫人.md "wikilink")
[Category:末代帝王](../Category/末代帝王.md "wikilink")
[Category:被處決者](../Category/被處決者.md "wikilink")

1.  Bienkowski and Millard 2000, Leick 1999, Sasson 1995:
    *biograghies*;Brown 1995:*selected kings*; Roux 1992:*historical
    context*