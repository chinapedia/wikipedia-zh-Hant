**伯特氏锯鳞鱼**又稱**凸颔松毬**、**大鳞鋸鱗魚**、**伯特氏松毬鱼**、**凸颌锯鳞鱼**，俗名**鐵線婆**、**鐵甲**，是[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[金眼鯛目](../Page/金眼鯛目.md "wikilink")[金鱗魚科的其中一](../Page/金鱗魚科.md "wikilink")[种](../Page/种.md "wikilink")。

## 分布

本魚分布在[印度](../Page/印度.md "wikilink")[太平洋海域](../Page/太平洋.md "wikilink")，如南[日本](../Page/日本.md "wikilink")、夏威夷、社会群岛及印度洋珊瑚区以及[台灣恒春等地的淺水區域](../Page/台灣.md "wikilink")。\[1\]

## 特徵

本魚之背鰭硬棘外緣為[橙黃色](../Page/橙黃色.md "wikilink")，成魚的下顎比上顎要來的突出。鱗片較為平滑，前鰓蓋骨下無強棘，吻部較為短鈍。體長可達30公分。

## 生態

本魚棲息深度依地域不同而有所改變，如在[日本](../Page/日本.md "wikilink")[本州](../Page/本州.md "wikilink")、[四國南部棲息較深的岩礁區裡](../Page/四國.md "wikilink")，而在[琉球群島則棲息在較淺的珊瑚礁水域](../Page/琉球群島.md "wikilink")。為[夜行性魚類](../Page/夜行性.md "wikilink")，喜躲在礁隙或洞穴中，以[甲殼類為食](../Page/甲殼類.md "wikilink")。

## 經濟利用

[食用魚](../Page/食用魚.md "wikilink")，但肉質糜爛，一般煮[湯為宜](../Page/湯.md "wikilink")，其內臟有累積熱帶魚毒，避免食用這些部位。

## 参考文献

  -
[Category:食用鱼](../Category/食用鱼.md "wikilink")
[berndti](../Category/鋸鱗魚属.md "wikilink")

1.