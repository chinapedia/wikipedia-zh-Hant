**伯禽**（），生年月不詳，**姬**姓，亦称**禽父**。[周朝諸侯国](../Page/周朝.md "wikilink")[魯国第一任君主](../Page/魯国.md "wikilink")，[周公旦长子](../Page/周公旦.md "wikilink")。《史記》記載就任年在周公東征，即[周成王元年](../Page/周成王.md "wikilink")（約前1042年）。

[周公東征之後](../Page/周公東征.md "wikilink")，周成王将[商朝遺民六族和](../Page/商朝.md "wikilink")[泰山之南的原](../Page/泰山.md "wikilink")[奄国土地](../Page/奄国.md "wikilink")、人民封給周公，為魯国。由于周公需要留在朝中，因此派其長子伯禽赴魯国就任。

伯禽到任之後，在[齐太公的](../Page/齐太公.md "wikilink")[齊国軍隊支援下平定了](../Page/齊国.md "wikilink")[淮夷和](../Page/淮夷.md "wikilink")[徐戎的叛乱](../Page/徐国.md "wikilink")，奠定了周朝在[淮河以北地区的统治](../Page/淮河.md "wikilink")。在進軍過程中，伯禽在費地作《費誓》激励士氣，這篇文辞被記记載在[尚書之中](../Page/尚書.md "wikilink")。

伯禽与齐国第二代君主[齐丁公](../Page/齐丁公.md "wikilink")、[卫国第二代君主](../Page/卫国.md "wikilink")[卫康伯以及](../Page/卫康伯.md "wikilink")[晋国第二代君主](../Page/晋国.md "wikilink")[晋侯燮共事](../Page/晋侯燮.md "wikilink")[周康王](../Page/周康王.md "wikilink")。周康王分三位[诸侯以珍宝之器](../Page/诸侯.md "wikilink")。而同事周康王的[楚君熊绎却无分](../Page/楚熊绎.md "wikilink")。春秋时期的前530年，[楚灵王仍忿然提起此事](../Page/楚灵王.md "wikilink")\[1\]。

伯禽在位共46年，魯国在他的統治下成為著名的“礼儀之邦”，疆域北至泰山、南達[徐州](../Page/徐州.md "wikilink")、東至[黄海](../Page/黄海.md "wikilink")、西抵[陽穀一带](../Page/陽穀.md "wikilink")，成為在今[山東境内与齊国抗衡的大国](../Page/山東.md "wikilink")。

[中国历史博物馆藏禽簋](../Page/中国历史博物馆.md "wikilink")，《[殷周金文集成](../Page/殷周金文集成.md "wikilink")》编号“七·四〇四一”。其铭文记载了周成王讨伐东方的奄侯，周公谋划这次征伐，而“禽”也就是当时为周王室大祝的伯禽，在助祭时宣读祝辞\[2\]。

## 家族

父親

  - [周文公](../Page/周文公.md "wikilink")

兄弟

  - 次弟[周平公](../Page/周平公.md "wikilink")
  - 三弟[蔣伯齡](../Page/蔣伯齡.md "wikilink")
  - 四弟[邢侯](../Page/邢侯.md "wikilink")
  - 五弟[祭伯](../Page/祭伯.md "wikilink")
  - [茅叔](../Page/茅叔.md "wikilink")
  - [凡伯](../Page/凡伯.md "wikilink")
  - [胙伯](../Page/胙伯.md "wikilink")

兒子

  - 長子：酋，后为[魯考公](../Page/魯考公.md "wikilink")
  - 次子：熙，后为[魯煬公](../Page/魯煬公.md "wikilink")
  - 三子：子颜
  - 四子：延陵

## 在位年與西曆對照表

<div class="NavFrame" style="clear: both; border: 1px solid #999; margin: 0.5em auto;">

<div class="NavHead" style="background-color: #CCCCFF; font-size: 90%; border-left: 3em soli; text-align: center; font-weight: bold;">

在位年與西曆對照表

</div>

<div class="NavContent" style="padding: 1em 0 0 0; font-size: 90%; text-align: center;">

| 魯公伯禽                           | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             | 八年                             | 九年                             | 十年                             |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| 西元                             | 前1042年                         | 前1041年                         | 前1040年                         | 前1039年                         | 前1038年                         | 前1037年                         | 前1036年                         | 前1035年                         | 前1034年                         | 前1033年                         |
| [干支](../Page/干支.md "wikilink") | [己亥](../Page/己亥.md "wikilink") | [庚子](../Page/庚子.md "wikilink") | [辛丑](../Page/辛丑.md "wikilink") | [壬寅](../Page/壬寅.md "wikilink") | [癸卯](../Page/癸卯.md "wikilink") | [甲辰](../Page/甲辰.md "wikilink") | [乙巳](../Page/乙巳.md "wikilink") | [丙午](../Page/丙午.md "wikilink") | [丁未](../Page/丁未.md "wikilink") | [戊申](../Page/戊申.md "wikilink") |
| 魯公伯禽                           | 十一年                            | 十二年                            | 十三年                            | 十四年                            | 十五年                            | 十六年                            | 十七年                            | 十八年                            | 十九年                            | 二十年                            |
| 西元                             | 前1032年                         | 前1031年                         | 前1030年                         | 前1029年                         | 前1028年                         | 前1027年                         | 前1026年                         | 前1025年                         | 前1024年                         | 前1023年                         |
| [干支](../Page/干支.md "wikilink") | [己酉](../Page/己酉.md "wikilink") | [庚戌](../Page/庚戌.md "wikilink") | [辛亥](../Page/辛亥.md "wikilink") | [壬子](../Page/壬子.md "wikilink") | [癸丑](../Page/癸丑.md "wikilink") | [甲寅](../Page/甲寅.md "wikilink") | [乙卯](../Page/乙卯.md "wikilink") | [丙辰](../Page/丙辰.md "wikilink") | [丁巳](../Page/丁巳.md "wikilink") | [戊午](../Page/戊午.md "wikilink") |
| 魯公伯禽                           | 二十一年                           | 二十二年                           | 二十三年                           | 二十四年                           | 二十五年                           | 二十六年                           | 二十七年                           | 二十八年                           | 二十九年                           | 三十年                            |
| 西元                             | 前1022年                         | 前1021年                         | 前1020年                         | 前1019年                         | 前1018年                         | 前1017年                         | 前1016年                         | 前1015年                         | 前1014年                         | 前1013年                         |
| [干支](../Page/干支.md "wikilink") | [己未](../Page/己未.md "wikilink") | [庚申](../Page/庚申.md "wikilink") | [辛酉](../Page/辛酉.md "wikilink") | [壬戌](../Page/壬戌.md "wikilink") | [癸亥](../Page/癸亥.md "wikilink") | [甲子](../Page/甲子.md "wikilink") | [乙丑](../Page/乙丑.md "wikilink") | [丙寅](../Page/丙寅.md "wikilink") | [丁卯](../Page/丁卯.md "wikilink") | [戊辰](../Page/戊辰.md "wikilink") |
| 魯公伯禽                           | 三十一年                           | 三十二年                           | 三十三年                           | 三十四年                           | 三十五年                           | 三十六年                           | 三十七年                           | 三十八年                           | 三十九年                           | 四十年                            |
| 西元                             | 前1012年                         | 前1011年                         | 前1010年                         | 前1009年                         | 前1008年                         | 前1007年                         | 前1006年                         | 前1005年                         | 前1004年                         | 前1003年                         |
| [干支](../Page/干支.md "wikilink") | [己巳](../Page/己巳.md "wikilink") | [庚午](../Page/庚午.md "wikilink") | [辛未](../Page/辛未.md "wikilink") | [壬申](../Page/壬申.md "wikilink") | [癸酉](../Page/癸酉.md "wikilink") | [甲戌](../Page/甲戌.md "wikilink") | [乙亥](../Page/乙亥.md "wikilink") | [丙子](../Page/丙子.md "wikilink") | [丁丑](../Page/丁丑.md "wikilink") | [戊寅](../Page/戊寅.md "wikilink") |
| 魯公伯禽                           | 四十一年                           | 四十二年                           | 四十三年                           | 四十四年                           | 四十五年                           | 四十六年                           |                                |                                |                                |                                |
| 西元                             | 前1002年                         | 前1001年                         | 前1000年                         | 前999年                          | 前998年                          | 前997年                          |                                |                                |                                |                                |
| [干支](../Page/干支.md "wikilink") | [己卯](../Page/己卯.md "wikilink") | [庚辰](../Page/庚辰.md "wikilink") | [辛巳](../Page/辛巳.md "wikilink") | [壬午](../Page/壬午.md "wikilink") | [癸未](../Page/癸未.md "wikilink") | [甲申](../Page/甲申.md "wikilink") |                                |                                |                                |                                |

</div>

</div>

## 註釋

## 參考

  - 韓兆琦，《新譯史記讀本》，台北，三民書局，2008年2月初版

  - 楊伯峻，《春秋左傳注》，高雄，復文圖書出版社，1991年二版

  -
[Category:魯國君主](../Category/魯國君主.md "wikilink")
[Category:西周人](../Category/西周人.md "wikilink")
[Category:姬姓](../Category/姬姓.md "wikilink")

1.  杨伯峻：《春秋左传注》，中华书局1990年，第1339页。
2.  陈秉新、李立芳：《出土夷族史料辑考》，安徽大学出版社2005年12月第1版，第127页。