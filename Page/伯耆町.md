**伯耆町**（）是位于日本[鳥取縣西部的一個城鎮](../Page/鳥取縣.md "wikilink")。

「伯耆」之名取自本地的舊[令制國國名](../Page/令制國.md "wikilink")「[伯耆國](../Page/伯耆國.md "wikilink")」。\[1\]

## 歷史

### 年表

  - 1889年10月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：
      - [日野郡](../Page/日野郡.md "wikilink")：金澤村、溝口村、榮村、金岩村、旭村、二部村、野上村、吉壽村、日吉村。
      - [會見郡](../Page/會見郡.md "wikilink")：大幡村、幡鄉村。
  - 1896年3月29日：[汗入郡和會見郡合併為](../Page/汗入郡.md "wikilink")[西伯郡](../Page/西伯郡.md "wikilink")。
  - 1912年1月1日：吉壽村和日吉村[合併為八鄉村](../Page/市町村合併.md "wikilink")。
  - 1914年2月1日：溝口村、榮村和金岩村合併為新設置的溝口村。
  - 1918年4月1日：金澤村和米原村合併為日光村。
  - 1921年12月1日：二部村和野上村合併為新設置的二部村。
  - 1931年10月1日：溝口村和旭村合併為[溝口町](../Page/溝口町.md "wikilink")。
  - 1954年4月1日：溝口町、二部村和日光村的部分地區（原金澤村）合併為新設置的溝口町。
  - 1955年3月31日：八鄉村、大幡村和幡鄉村的部分地區合併為[岸本町](../Page/岸本町.md "wikilink")。
  - 2005年1月1日：岸本町與溝口町合併為**伯耆町**。\[2\]

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年10月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1988年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>溝口村</p></td>
<td><p>1914年2月1日<br />
溝口村</p></td>
<td><p>1931年10月1日<br />
溝口町</p></td>
<td><p>1954年4月1日<br />
溝口町</p></td>
<td><p>2005年1月1日<br />
伯耆町</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>榮村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>金岩村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>旭村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>二部村</p></td>
<td><p>1921年12月1日<br />
二部村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>野上村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>吉壽村</p></td>
<td><p>1912年1月1日<br />
八鄉村</p></td>
<td><p><br />
<br />
1955年3月31日<br />
岸本町<br />
<br />
<br />
</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>日吉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大幡村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>幡鄉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1955年3月31日<br />
併入手間村</p></td>
<td><p>1955年4月25日<br />
合併為會見町</p></td>
<td><p>2004年10月1日<br />
合併為南部町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")
      - [伯備線](../Page/伯備線.md "wikilink")：（←[江府町](../Page/江府町.md "wikilink")）
        - [伯耆溝口車站](../Page/伯耆溝口車站.md "wikilink") -
        [岸本車站](../Page/岸本車站.md "wikilink") -
        （[米子市](../Page/米子市.md "wikilink")）

|                                                                                                                |
| -------------------------------------------------------------------------------------------------------------- |
| [Kishimoto_Station.jpg](https://zh.wikipedia.org/wiki/File:Kishimoto_Station.jpg "fig:Kishimoto_Station.jpg") |
|                                                                                                                |

### 道路

  - 高速道路

<!-- end list -->

  - [米子自動車道](../Page/米子自動車道.md "wikilink")：[溝口交流道](../Page/溝口交流道.md "wikilink")
    - [大山休息區](../Page/大山休息區.md "wikilink")

## 觀光資源

[Shoji_Ueda_Museum_of_Photography01bs3200.jpg](https://zh.wikipedia.org/wiki/File:Shoji_Ueda_Museum_of_Photography01bs3200.jpg "fig:Shoji_Ueda_Museum_of_Photography01bs3200.jpg")

  - 植田正治攝影美術館

## 本地出身之名人

  - [野坂浩賢](../Page/野坂浩賢.md "wikilink")：曾任[眾議院](../Page/眾議院_\(日本\).md "wikilink")[議員](../Page/議員.md "wikilink")、[建設大臣和](../Page/建設大臣.md "wikilink")[村山富市內閣](../Page/村山富市內閣.md "wikilink")[官房長官](../Page/官房長官.md "wikilink")。
  - [大江賢次](../Page/大江賢次.md "wikilink")：小說作家。
  - [長谷川崇夫](../Page/長谷川崇夫.md "wikilink")：[搖滾樂歌手](../Page/搖滾樂.md "wikilink")、[創作歌手](../Page/創作歌手.md "wikilink")。
  - [無良隆志](../Page/無良隆志.md "wikilink")：前[花式滑冰選手](../Page/花式滑冰.md "wikilink")。
  - [安綱](../Page/安綱.md "wikilink")（大原安綱）：[平安時代中期位於伯耆國的鍛刀匠](../Page/平安時代.md "wikilink")。

## 參考資料

## 外部連結

  - [伯耆町官方網頁](http://en.go-to-japan.jp/daisenguide/)

  - [伯耆町觀光指引](http://www.houki-town.jp/kanko/)

<!-- end list -->

1.

2.