**伯顏**（；），是[中國](../Page/中國.md "wikilink")[元朝后期的軍事和政治人物](../Page/元朝.md "wikilink")，[蒙古](../Page/蒙古族.md "wikilink")[蔑兒乞部人](../Page/蔑兒乞部.md "wikilink")。曾權傾一時，後被[元順帝治罪](../Page/元順帝.md "wikilink")。

## 生平

伯顏的祖父曾隨[元憲宗](../Page/元憲宗.md "wikilink")[蒙哥伐](../Page/蒙哥.md "wikilink")[南宋](../Page/南宋.md "wikilink")，父親是[隆福太后宮中](../Page/隆福太后.md "wikilink")[宿衛的統領](../Page/宿衛.md "wikilink")。伯顏的出生年份没有记载
。[元成宗時](../Page/元成宗.md "wikilink")，伯顏被派為[元武宗的侍從](../Page/元武宗.md "wikilink")，1299年參與对[海都汗的討伐](../Page/海都.md "wikilink")，因功元武宗賜與伯顏「拔都兒」的稱號。元武宗即位後，任[吏部](../Page/吏部.md "wikilink")[尚書等高官](../Page/尚書.md "wikilink")。[泰定帝去世](../Page/泰定帝.md "wikilink")，權臣[燕鐵木兒欲從](../Page/燕鐵木兒.md "wikilink")[江陵迎立武宗之子懷王](../Page/江陵.md "wikilink")（即[元文宗](../Page/元文宗.md "wikilink")）；伯顏時任[河南行省](../Page/河南行省.md "wikilink")[平章政事](../Page/平章政事.md "wikilink")，全力支持並護送懷王北行。文宗即位後，屢昇官至中書[右丞相](../Page/右丞相.md "wikilink")。

[元惠宗](../Page/元惠宗.md "wikilink")（[元順帝](../Page/元順帝.md "wikilink")）時，燕鐵木兒之子[唐其勢](../Page/唐其勢.md "wikilink")、[塔剌海兩人涉嫌](../Page/塔剌海.md "wikilink")[謀反](../Page/謀反.md "wikilink")，被殺；伯顏得以逐漸獨攬軍政大權，大量引用親信為高官，包括其姪子[脫脫](../Page/脫脫.md "wikilink")，甚至派脱脱监视元顺帝。而元順帝则对伯顏的作法越来越不滿，便聯合实际上亦对伯顏有所不满的脫脫，於1340年發動行動，將伯顏治罪降官至[河南](../Page/河南.md "wikilink")，同年再降至[廣東](../Page/廣東.md "wikilink")，後死於途中。

## 轶事

伯顏掌權期间專權枉法，且排斥[漢族以及](../Page/漢族.md "wikilink")[漢化](../Page/漢化.md "wikilink")，停辦[科舉](../Page/科舉.md "wikilink")。为防止[南人造反](../Page/南人.md "wikilink")，禁止江南农家用铁禾钗。对[汉人](../Page/汉人.md "wikilink")、[南人祈神赛社](../Page/南人.md "wikilink")、习学枪棒武术以至演唱戏剧等，都横加禁止，以防他们聚众闹事。甚至曾提出殺光[張](../Page/張姓.md "wikilink")、[王](../Page/王姓.md "wikilink")、[劉](../Page/劉姓.md "wikilink")、[李](../Page/李姓.md "wikilink")、[趙五姓漢族民眾的想法](../Page/趙姓.md "wikilink")。《元史‧順帝紀》載：「伯顏請殺張、王、劉、李、趙五姓漢人，帝不從。」

## 参考文献

  - [《元史·伯颜传》](http://www.guoxue.com/shibu/24shi/yuanshi/yuas_138.htm)

[Category:元朝右丞相](../Category/元朝右丞相.md "wikilink")
[Category:元朝太师](../Category/元朝太师.md "wikilink")
[Category:元朝王爵](../Category/元朝王爵.md "wikilink")
[B](../Category/元朝官员.md "wikilink")
[B](../Category/元朝軍事人物.md "wikilink")
[B](../Category/元朝蒙古人.md "wikilink")