在[範疇論中](../Page/範疇論.md "wikilink")，函子\(F, G\)若滿足\(\mathrm{Hom}(F(-),-) = \mathrm{Hom}(-,G(-))\)，則稱之為一對**伴隨函子**，其中\(G\)稱為\(F\)的**右伴隨函子**，而\(F\)是\(G\)的**左伴隨函子**。伴隨函子在範疇論中是個極基本而有用的概念。

## 定義

設\(F: \mathcal{C}_1 \to \mathcal{C}_2, \; G: \mathcal{C}_2 \to \mathcal{C}_1\)為函子，若存在[雙函子的同構](../Page/雙函子.md "wikilink")

  -
    \(\mathrm{Hom}_{\mathcal{C}_2}(F(-),-) \simeq \mathrm{Hom}_{\mathcal{C}_1}(-,G(-))\)

則稱\(F, G\)為一對**伴隨函子**，\(G\)稱為\(F\)的**右伴隨函子**，而\(F\)是\(G\)的**左伴隨函子**。

上述同構進一步給出兩個同構

  -
    \(\mathrm{Hom}_{\mathcal{C}_2}(F \circ G(-),-) \simeq \mathrm{Hom}_{\mathcal{C}_1}(G(-), G(-))\)
    \(\mathrm{Hom}_{\mathcal{C}_2}(F(-), F(-)) \simeq \mathrm{Hom}_{\mathcal{C}_1}(-, G \circ F(-))\)

分別在同構的左右兩側置\(\mathrm{id}_{F(-)}\)與\(\mathrm{id}_{G(-)}\)，遂得到函子間的態射（即[自然變換](../Page/自然變換.md "wikilink")）：

  -
    \(\mathrm{id}_{\mathcal{C}_1} \to G \circ F \quad\)（**單位**）
    \(F \circ G \to \mathrm{id}_{\mathcal{C}_2} \quad\)（**上單位**）

定義中的雙函子同構由單位與上單位唯一決定。

## 正合性

设\(F, G\)是一對伴隨函子，若\(F\)為右正合则\(G\)為左正合；此命題可由[正合函子與](../Page/正合函子.md "wikilink")[極限的定義直接導出](../Page/極限_\(範疇論\).md "wikilink")。

## 例子

伴隨函子在數學中處處可見，以下僅舉出幾個例子：

  - [自由對象與](../Page/自由對象.md "wikilink")[遺忘函子是一對伴隨函子](../Page/遺忘函子.md "wikilink")，舉[群範疇為例](../Page/群.md "wikilink")，此時單位態射不外是集合\(X\)到它生成的[自由群](../Page/自由群.md "wikilink")\(F(X)\)的包含映射。
  - [積與](../Page/積_\(範疇論.md "wikilink")[對角函子](../Page/對角函子.md "wikilink")。
  - 設\(R\)為[環](../Page/環.md "wikilink")，\(M\)為右\(R\)-模，則\(M \otimes_R - : _R\mathbf{Mod} \to \mathbf{Ab}\)與\(\mathrm{Hom}_\Z (-,M): \mathbf{Ab} \to _R\mathbf{Mod}\)為一對伴隨函子。當\(R\)可交換時，上式的\(\Z\)可代為\(R\)，\(\mathbf{Ab}\)可代為\(_R\mathbf{Mod}\)。
  - [層的正像與逆像](../Page/層_\(數學\).md "wikilink")。
  - [群表示理論中的](../Page/群表示理論.md "wikilink")**弗羅貝尼烏斯互反定理**（詳閱[誘導表示](../Page/誘導表示.md "wikilink")）。

## 文獻

  - Masaki Kashiwara and Pierre Schapira, *Categories and Sheaves*,
    Springer. ISBN 3-540-27949-0

## 外部連結

  - [Pierre Schapira, Categories and Homological
    Algebra](https://web.archive.org/web/20070710013127/http://www.institut.math.jussieu.fr/%7Eschapira/polycopies/Cat.pdf)

[Category:函子](../Category/函子.md "wikilink")