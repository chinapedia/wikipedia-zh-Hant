**似提姆龍屬**（屬名：*Timimus*）是種[虛骨龍類](../Page/虛骨龍類.md "wikilink")[獸腳亞目](../Page/獸腳亞目.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，可能屬於[似鳥龍下目](../Page/似鳥龍下目.md "wikilink")。似提姆龍的屬名是以化石發現者[提姆·富蘭納瑞](../Page/提姆·富蘭納瑞.md "wikilink")（Tim
Flannery）為名。似提姆龍生存於早[白堊紀的](../Page/白堊紀.md "wikilink")[阿爾比階](../Page/阿爾比階.md "wikilink")，約1億600萬年前。似提姆龍的化石發現於[澳洲東南部的](../Page/澳洲.md "wikilink")[恐龍灣](../Page/恐龍灣.md "wikilink")。

## 發現與種

在1991年，澳洲東南部的恐龍灣發現兩個[股骨](../Page/股骨.md "wikilink")，分別來自於成年與未成年個體，而這兩塊骨頭發現時相距不到1公尺。

[模式種是](../Page/模式種.md "wikilink")**赫氏似提姆龍**（*T.
hermani*），是由[托馬斯·里奇](../Page/托馬斯·里奇.md "wikilink")（Thomas
Rich）博士與[帕特·里奇](../Page/帕特·里奇.md "wikilink")（Patricia
Vickers-Rich）夫婦在1993年到1994年正式敘述、命名。屬名意為「Tim的模仿者」，同時以他們的兒子Timothy
Rich、澳洲古生物學家[提姆·富蘭納瑞](../Page/提姆·富蘭納瑞.md "wikilink")（Tim
Flannery）為名，「*mimus*」則是似鳥龍下目的常見字根；種名則是以贊助恐龍灣挖掘計畫的John
Herman為名\[1\]。

[正模標本](../Page/正模標本.md "wikilink")（編號**NMV
P186303**）是其中的成年個體左[股骨](../Page/股骨.md "wikilink")，屬於Eumeralla組地層，地質年代約1億600萬年前，相當於[白堊紀早期的](../Page/白堊紀.md "wikilink")[阿爾比階](../Page/阿爾比階.md "wikilink")。[副模標本](../Page/副模標本.md "wikilink")（編號**NMV
P186323**）是其中的位成年個體股骨目前也發現了一些歸類於似提姆龍的脊椎骨，發現於相同地點。

在1994年，托馬斯·里奇博士宣稱因為該地區的露出化石有限，不太可能發現更進一步的似提姆龍化石，目前所發現的[正模標本已經是最完整的化石](../Page/正模標本.md "wikilink")。而正模標本的特徵足以被鑑定為屬於[似鳥龍下目](../Page/似鳥龍下目.md "wikilink")，而且是這個[演化支的新屬](../Page/演化支.md "wikilink")。因此這個新屬成為科學文獻裡的一個參考點\[2\]。

## 敘述

正模標本的股骨長44公分，根據估計，該成年個體身長約2.5公尺\[3\]。副模標本的股骨長19.5公分。似提姆龍的修長骨頭顯示牠們的輕盈的[動物](../Page/動物.md "wikilink")。這些股骨有些可鑑定特徵，股骨末端髁部沒有伸肌溝，這是種基礎似鳥龍類的特徵\[4\]。股骨頭的頂部平坦。股骨的前粗隆部的位置高，相當於大粗隆部的位置。

## 種系發生學

在1994年，里奇夫婦將似提姆龍歸類於似鳥龍下目的[似鳥龍科](../Page/似鳥龍科.md "wikilink")。在[南半球很少發現似鳥龍類的化石](../Page/南半球.md "wikilink")。似提姆龍的發現，代表似鳥龍類可能曾存活在南方各大陸，甚至起源於南方各大陸。同年，[湯瑪斯·荷茲](../Page/湯瑪斯·荷茲.md "wikilink")（Thomas
Holtz）則執疑似提姆龍並不屬於似鳥龍類\[5\]。目前的普遍看法是，似提姆龍缺乏跟似鳥龍類的[共有衍徵](../Page/共有衍徵.md "wikilink")（Synapomorphies），因此缺乏牠們屬於似鳥龍類的證據。似提姆龍可能屬於某個[虛骨龍類](../Page/虛骨龍類.md "wikilink")[演化支](../Page/演化支.md "wikilink")。某些研究人員認為似提姆龍是[疑名](../Page/疑名.md "wikilink")\[6\]。一個2012年研究提出似提姆龍屬於[暴龍超科](../Page/暴龍超科.md "wikilink")\[7\]。

## 古生物學

在白堊紀早期，澳洲接近[南極圈](../Page/南極圈.md "wikilink")，會有[永晝](../Page/永晝.md "wikilink")、[永夜的現象](../Page/永夜.md "wikilink")。在1996年，[化石細微結構專家](../Page/化石.md "wikilink")[金薩米·杜藍](../Page/金薩米·杜藍.md "wikilink")（Anusuya
Chinsamy-Turan），檢驗似提姆龍與[雷利諾龍的化石](../Page/雷利諾龍.md "wikilink")，發現牠們擁有不同的骨頭組織。[稜齒龍類的雷利諾龍有連續速率的骨頭沉澱物](../Page/稜齒龍類.md "wikilink")，而[似鳥龍類的似提姆龍擁有週期性的骨頭結構樣式](../Page/似鳥龍類.md "wikilink")，這顯示似提姆龍可能在較冷季節有[冬眠行為](../Page/冬眠.md "wikilink")\[8\]。近年在維多利亞州東南部Inverloch鎮發現一個化石，來自於第三腳趾的第一節趾骨，底部有凹陷性骨折現象，這骨頭可能屬於似提姆龍、或是其近親\[9\]。

## 參考資料

  -
  -
  - [Online abstract of preceding
    article](https://web.archive.org/web/20070311040301/http://www.vertpaleo.org/jvp/18-385-390.html)

## 外部链接

  - [恐龙.NET](https://web.archive.org/web/20170527222928/http://dinosaur.net.cn/)——中国古生物门户网站。
  - [似提姆龍簡介與資料](http://www.alphalink.com.au/~dannj/timimus.htm)

[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:虛骨龍類](../Category/虛骨龍類.md "wikilink")
[Category:大洋洲恐龍](../Category/大洋洲恐龍.md "wikilink")
[Category:可疑的恐龍](../Category/可疑的恐龍.md "wikilink")

1.  T.H. Rich and P. Vickers-Rich, 1994, "Neoceratopsians and
    ornithomimosaurs: dinosaurs of Gondwana origin?", *National
    Geographic Research and Exploration* **10**(1): 129-131

2.

3.  Long, J.A. (1998). *Dinosaurs of Australia and New Zealand and Other
    Animals of the Mesozoic Era*, Harvard University Press, p. 108

4.
5.  Holtz, T. R., Jr. 1994. "The phylogenetic position of the
    Tyrannosauridae: Implications for theropod systematics". *Journal of
    Paleontology* **68**: 1100-1117

6.  S.A. Hocknull, M.A. White, T.R. Tischler, A.G. Cook, N.D. Calleja,
    T. Sloan, and D.A. Elliot. 2009. "New mid-Cretaceous (latest Albian)
    dinosaurs from Winton, Queensland, Australia". *PLoS ONE*
    **4**(7):e6190: 1-51

7.  Benson, R. B. J.; Rich, T. H.; Vickers-Rich, P.; Hall, M. (2012).
    Farke, Andrew A. ed. "Theropod Fauna from Southern Australia
    Indicates High Polar Diversity and Climate-Driven Dinosaur
    Provinciality". PLoS ONE 7 (5): e37122.

8.  Chinsamy, A., Rich, T., and Rich-Vickers, P. (1996). "Bone histology
    of dinosaurs from Dinosaur Cove, Australia", *Journal of Vertebrate
    Paleontology* **16**(Supplement to No.3), 28A

9.  Molnar, R. E., 2001, Theropod paleopathology: a literature survey:
    In: Mesozoic Vertebrate Life, edited by Tanke, D. H., and Carpenter,
    K., Indiana University Press, p. 337-363.