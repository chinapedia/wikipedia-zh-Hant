**伽利略變換**是[-{zh-cn:经典力学; zh-hk:經典力學;
zh-tw:古典力學}-中用以在兩個只以均速相對移動的](../Page/經典力學.md "wikilink")[參考系之間變換的方法](../Page/参考系.md "wikilink")，屬於一種被動態變換。伽利略变换明顯成立的公式在物體以接近[光速運動时](../Page/光速.md "wikilink")、亦或者是电磁过程不会成立，這是[相對論效應造成的](../Page/狹義相對論.md "wikilink")。

[伽利略·伽利萊在解釋均速運動時制定了這一套概念](../Page/伽利略·伽利莱.md "wikilink")。\[1\]他用其解釋[球體滾下](../Page/球體.md "wikilink")[斜面這一力學問題](../Page/斜面.md "wikilink")，並測量出[地球表面](../Page/地球.md "wikilink")[引力](../Page/引力.md "wikilink")[加速度的數值](../Page/加速度.md "wikilink")。

## 平移

[Galilean_transformation_zh.svg](https://zh.wikipedia.org/wiki/File:Galilean_transformation_zh.svg "fig:Galilean_transformation_zh.svg")
伽利略變換建基於人們加減物體速度的直覺。在其核心，伽利略變換假設時間和空間是[絕對的](../Page/絕對同時.md "wikilink")。

這項假設在[洛伦兹变换中被捨棄](../Page/洛伦兹变换.md "wikilink")，因此就算在[相對論性速度下](../Page/狹義相對論.md "wikilink")，洛伦兹变换也是成立的；而伽利略變換則是洛伦兹变换的低速近似值。

以下為伽利略變換的數學表達式，其中和分別為同一個事件在兩個坐標系S和S'中的坐標。兩個坐標系以相對均速運行（[速度為](../Page/速度.md "wikilink")*v*），運行方向為，原點在時間為t=t'=0時重合。
\[2\] \[3\] \[4\] \[5\]

\[x'=x-vt\,\]

\[y'=y \,\]

\[z'=z \,\]

\[t'=t \,\]

最後一條方程式意味著時間是不受觀測者的相對運動影響的。

利用[線性代數的術語來說](../Page/線性代數.md "wikilink")，這種變換是個[錯切](../Page/錯切.md "wikilink")，是矩陣對向量進行變換的一個過程。當參考系只沿著*x*軸移動時，伽利略變換只作用於兩個分量：

\[(x', t') = (x,t) \begin{pmatrix} 1 & 0 \\-v & 1 \end{pmatrix}.\]
雖然在伽利略變換中沒有必要用到矩陣表達法，但是用了矩陣就可以和狹義相對論中的變換法進行比較。

## 三種伽利略變換

[Galilean_transform_of_world_line.gif](https://zh.wikipedia.org/wiki/File:Galilean_transform_of_world_line.gif "fig:Galilean_transform_of_world_line.gif")所看到的[時空](../Page/時空.md "wikilink")。

縱軸為時間，橫軸為距離，虛線為觀測者在時空中的軌跡。圖的下半部是已經發生了的事件，上半部則是未來的事件。圖中小點為時空中的事件。

世界線的斜率為觀測者的相對速率。注意觀測者在加速時所看到的時空會進行[錯切](../Page/錯切.md "wikilink")。\]\]

伽利略變換可以唯一寫成由時空的旋轉、平移和匀速運動[複合而成的函數](../Page/複合函數.md "wikilink")。\[6\]設**x**為三維空間中的一點，*t*為一維時間中的一點。時空當中的任何一點可以表達為[有序對](../Page/有序對.md "wikilink")(**x**,*t*)。速度為**v**的匀速運動表達為\((\mathbf{x},t) \mapsto (\mathbf{x}+t\mathbf{v},t)\)，其中**v**在**R**<sup>3</sup>內。平移表達為\((\mathbf{x},t) \mapsto (\mathbf{x}+\mathbf{a},t+b)\)，其中**a**在**R**<sup>3</sup>內，*b*在**R**內。旋轉表達為\((\mathbf{x},t) \mapsto (G\mathbf{x},t)\)，其中為某[正交變換](../Page/正交變換.md "wikilink")。\[7\]作為一個[李群](../Page/李群.md "wikilink")，伽利略變換的維度為10。\[8\]

## 伽利略群的中心擴張

這裡我們只考慮[伽利略群的](../Page/伽利略群.md "wikilink")[李代數](../Page/李代數.md "wikilink")。結果能夠輕易延伸到[李群](../Page/李群.md "wikilink")。L的李代數由H、P<sub>i</sub>、C<sub>i</sub>和L<sub>ij</sub>[張成](../Page/線性生成空間.md "wikilink")（[反對稱張量](../Page/反對稱張量.md "wikilink")），並能夠受[交換子的作用](../Page/交換子.md "wikilink")，其中

\[[H,P_i]=0 \,\!\]

\[[P_i,P_j]=0 \,\!\]

\[[L_{ij},H]=0 \,\!\]

\[[C_i,C_j]=0 \,\!\]

\[[L_{ij},L_{kl}]=i [\delta_{ik}L_{jl}-\delta_{il}L_{jk}-\delta_{jk}L_{il}+\delta_{jl}L_{ik}] \,\!\]

\[[L_{ij},P_k]=i[\delta_{ik}P_j-\delta_{jk}P_i] \,\!\]

\[[L_{ij},C_k]=i[\delta_{ik}C_j-\delta_{jk}C_i] \,\!\]

\[[C_i,H]=i P_i \,\!\]

\[[C_i,P_j]=0 \,\!.\]

H為時間平移的生成元（[哈密顿算符](../Page/哈密顿算符.md "wikilink")），P<sub>i</sub>為平移的生成元（[動量算符](../Page/動量算符.md "wikilink")），C<sub>i</sub>為伽利略變換的生成元，而L<sub>ij</sub>為旋轉的生成元（[角動量算符](../Page/角動量算符.md "wikilink")）。

現在我們可以對H'、P'<sub>i</sub>、C'<sub>i</sub>、L'<sub>ij</sub>（反對稱張量）、M所張成的李群進行中心擴張，使得M與一切都[可交換](../Page/可交換.md "wikilink")（位於[中心](../Page/中心_\(群论\).md "wikilink")，「中心擴張」因此得名）：

\[[H',P'_i]=0 \,\!\]

\[[P'_i,P'_j]=0 \,\!\]

\[[L'_{ij},H']=0 \,\!\]

\[[C'_i,C'_j]=0 \,\!\]

\[[L'_{ij},L'_{kl}]=i [\delta_{ik}L'_{jl}-\delta_{il}L'_{jk}-\delta_{jk}L'_{il}+\delta_{jl}L'_{ik}] \,\!\]

\[[L'_{ij},P'_k]=i[\delta_{ik}P'_j-\delta_{jk}P'_i] \,\!\]

\[[L'_{ij},C'_k]=i[\delta_{ik}C'_j-\delta_{jk}C'_i] \,\!\]

\[[C'_i,H']=i P'_i \,\!\]

\[[C'_i,P'_j]=i M\delta_{ij} \,\!\]

## 參見

  - [洛伦兹群](../Page/洛伦兹群.md "wikilink")
  - [龐加萊群](../Page/龐加萊群.md "wikilink")

## 備註

[Category:理論物理](../Category/理論物理.md "wikilink")
[Category:经典力学](../Category/经典力学.md "wikilink")

1.  Galileo 1638 *Discorsi e Dimostrazioni Matematiche, intorno á due
    nuoue scienze* **191** - **196**, published by [Lowys
    Elzevir](../Page/Lowys_Elzevir.md "wikilink") ([Louis
    Elsevier](../Page/Louis_Elsevier.md "wikilink")), Leiden, or *[Two
    New Sciences](../Page/Two_New_Sciences.md "wikilink")*, English
    translation by [Henry Crew](../Page/Henry_Crew.md "wikilink") and
    [Alfonso de Salvio](../Page/Alfonso_de_Salvio.md "wikilink") 1914,
    reprinted on pages 515-520 of *On the Shoulders of Giants*: The
    Great Works of Physics and Astronomy. [Stephen
    Hawking](../Page/Stephen_Hawking.md "wikilink"), ed. 2002 ISBN
    978-0-7624-1348-5

2.  , [Chapter 2 §2.6,
    p. 42](http://books.google.be/books?id=lfGE-wyJYIUC&pg=PA42)

3.  , [Chapter 38 §38.2,
    p. 1046,1047](http://books.google.be/books?id=B8K_ym9rS6UC&pg=PA1047)

4.  , [Chapter 9 §9.1,
    p. 261](http://books.google.be/books?id=1DZz341Pp50C&pg=PA261)

5.  , [Chapter 5,
    p. 83](http://books.google.be/books?id=JokgnS1JtmMC&pg=PA83)

6.

7.
8.