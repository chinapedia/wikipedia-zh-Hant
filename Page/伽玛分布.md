{\\Gamma(k)\\,\\theta^k}\\,\\\!</math> |cdf
=\(\frac{\gamma(k, x/\theta)}{\Gamma(k)}\,\!\) |mean =\(k \theta\,\!\)
|median =no simple closed form |mode =\((k-1) \theta\,\!\) for
\(k \geq 1\,\!\) | variance =\(k \theta^2\,\!\) |skewness
=\(\frac{2}{\sqrt{k}}\,\!\) |kurtosis =\(\frac{6}{k}\,\!\) |entropy
=\(k + \ln\theta + \ln\Gamma(k) \!\)
\(+ (1-k)\psi(k) \!\) |mgf =\((1 - \theta\,t)^{-k}\,\!\) for
\(t < 1/\theta\,\!\) |char =\((1 - \theta\,i\,t)^{-k}\,\!\) }}
**伽玛分布**是[統計學的一種連續](../Page/統計學.md "wikilink")[機率函數](../Page/機率函數.md "wikilink")。伽玛分佈中的[參數α](../Page/參數.md "wikilink")，稱為形狀參數，β稱為尺度參數。

## 實驗定義與觀念

假設隨機變數X為等到第α件事發生所需之等候時間，。

## 機率密度函數

令\(X \sim \Gamma(\alpha, \beta)\)，且令\(\lambda = \frac{1}{\beta}\)
（即\(X \sim \Gamma(\alpha, \frac{1}{\lambda})\)），則：

\(f \left( x \right)
=
\frac{x^\left(\alpha-1\right)\lambda^\alpha e^\left(-\lambda x\right)}{\Gamma\left(\alpha \right)}\)，<span style="font-size:larger;">x</span>
\> 0

其中[Gamma函数之特徵為](../Page/Γ函数.md "wikilink")：

\(\begin{cases} \Gamma(\alpha)=(\alpha-1)! & \mbox{if }\alpha\mbox{ is }\mathbb{Z}^+
\\ \Gamma(\alpha)=(\alpha-1)\Gamma(\alpha-1)& \mbox{if }\alpha\mbox{ is }\mathbb{R}^+
\\ \Gamma \left( \frac{1}{2} \right) = \sqrt{\pi}
\end{cases}\)

## [矩母函数](../Page/矩母函数.md "wikilink")、[概率母函数](../Page/概率母函数.md "wikilink")、[期望值](../Page/期望值.md "wikilink")、[方差](../Page/方差.md "wikilink")

  - Gamma分配的[矩母函数](../Page/矩母函数.md "wikilink")（m.g.f）

\[M_{x}\left( t \right)
=
E\left( e^{xt} \right)
=
\frac{\lambda^\alpha}{\Gamma\left(\alpha\right)}
\int_{0}^{\infty}
e^{xt}x^{\alpha-1}e^{-\lambda x} dx
=
\left( \frac{\lambda}{\lambda-t} \right)^{\alpha}\]

  - [概率母函数](../Page/概率母函数.md "wikilink")（p.g.f）

\[K_x\left(t\right)
=
\ln M_x\left( t \right)
=
\alpha\left[\ln\lambda-\ln\left(\lambda-t\right)\right]\]

  - [期望值](../Page/期望值.md "wikilink")

\[\frac { dK_x \left( t \right) } {dt}
=
\frac {\alpha} {\lambda-t}
 ,\quad when(t=0),
E\left( X \right)
=
\frac{\alpha}{\lambda}\]

  - [方差](../Page/方差.md "wikilink")

\[\frac { d^2K_x \left( t \right) } {dt^2}
=
\frac {\alpha} {\left(\lambda-t\right)^2}
 ,\quad when(t=0),
\sigma^2\left( X \right)
=
\frac{\alpha}{\lambda^2}\]

## Gamma的加成性

當兩隨機變數服從Gamma分配，互相獨立，且單位時間內頻率相同時，Gamma分布具有加成性。

\[\coprod
\begin{cases} r.v.X\sim \Gamma \left( \alpha_1,{\color{Red}\lambda} \right)
\\
r.v.Y\sim \Gamma \left( \alpha_2,{\color{Red}\lambda} \right)
\end{cases}
\Longrightarrow
X+Y\sim \Gamma \left( {\color{red}\alpha_1+\alpha_2},\lambda \right)\]

## 外部連結

  - [LDA-math-神奇的Gamma函数](http://cos.name/2013/01/lda-math-gamma-function/)
  - [分布计算器](http://www.vias.org/simulations/simusoft_distcalc.html)（英文）

{{-}}

[Category:连续分布](../Category/连续分布.md "wikilink")
[Category:阶乘与二项式主题](../Category/阶乘与二项式主题.md "wikilink")