**但丁**（ダンテ，Dante）是《[惡魔獵人系列](../Page/惡魔獵人系列.md "wikilink")》的遊戲角色，系列首款作品在2001年發售。但丁是第一至三集的主角，以華麗的動作身手成為一個受歡迎的人物。在四代中，但丁將會擔任遊戲後半段的主角。

## 設計來源

最初但丁的設計靈感是來自[生化危機系列](../Page/生化危機系列.md "wikilink")\[1\]。遊戲監督[三上真司在製作](../Page/三上真司.md "wikilink")《[生化危機4](../Page/生化危機4.md "wikilink")》中，要求導演[神谷英樹打造新的遊戲環境](../Page/神谷英樹.md "wikilink")，結果開發出來變成與生化危機系列大異的遊戲，三上真司於是決定在生化危機系列以外開創新的遊戲系列，最初假定名為「Devil
May Care」，到正式宣布前才決定為「Devil May Cry」。交由神谷英樹繼續負責，而主角但丁亦在這個時候誕生。

## 人物

特徵是有著高大的身型以及銀白色頭髮，經常身穿正紅色的大衣，其性格狂放不羈，在戰鬥時卻喜歡嘲諷敵人。由於但丁的母親伊娃（Eva）為人類，而父親斯巴達（Sparda）為惡魔，故但丁具有「半人半魔」的身分，而但丁與他的兄弟維吉爾（Vergil）是雙胞胎。

## 角色背景

父親是傳說中的魔劍士斯巴達,與具有天使血統的混血人類女性伊娃相愛生下雙胞胎但丁和維吉爾，由於斯巴達打破魔界的禁忌而行蹤不明,但一代的開頭動畫則是說明斯巴達已經死亡。

在《[DmC：惡魔獵人](../Page/DmC：惡魔獵人.md "wikilink")》當中，補充了但丁的母親伊娃為天使的劇情，而傳說中只有天使以及惡魔所共同生下的魔人才能打倒魔帝穆圖斯。

## 戰鬥武器和能力

但丁常用的武器包括雙槍「黑檀 & 象牙」（Ebony &
Ivory，正傳遊戲中以[M1911手槍為原型](../Page/M1911手槍.md "wikilink")，DmC：鬼泣中以[沙漠之鷹手槍為原型](../Page/沙漠之鷹手槍.md "wikilink")）、自在地操控與人齊高的雙手大劍「反逆」（Rebellion）以及各式各樣的武器。但丁由於擁有一半惡魔的血統，在成年職業時代與兄長維吉爾（Vergil）的爭鬥中讓「魔人化」的力量覺醒，使得但丁可以將肉體化身為魔人，解放出超乎凡人的特異能力。

## 參考資料

<references />

[Category:卡普空角色](../Category/卡普空角色.md "wikilink")
[Category:惡魔獵人系列](../Category/惡魔獵人系列.md "wikilink")
[Category:虛構商人](../Category/虛構商人.md "wikilink")
[Category:虛構罪犯](../Category/虛構罪犯.md "wikilink")
[Category:具有加速癒合能力的虛構角色](../Category/具有加速癒合能力的虛構角色.md "wikilink")
[Category:具有超人类力气的虚构角色](../Category/具有超人类力气的虚构角色.md "wikilink")
[Category:武术家电子游戏角色](../Category/武术家电子游戏角色.md "wikilink")
[Category:虛構私家偵探](../Category/虛構私家偵探.md "wikilink")
[Category:虛構雙胞胎](../Category/虛構雙胞胎.md "wikilink")
[Category:虛構治安維持者](../Category/虛構治安維持者.md "wikilink")
[Category:虛構男性電子遊戲角色](../Category/虛構男性電子遊戲角色.md "wikilink")
[Category:虚构恶魔](../Category/虚构恶魔.md "wikilink")
[Category:虛構半人類](../Category/虛構半人類.md "wikilink")
[Category:虛構反英雄](../Category/虛構反英雄.md "wikilink")

1.