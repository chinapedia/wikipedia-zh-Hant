[MTCBus_Route285_671AD.jpg](https://zh.wikipedia.org/wiki/File:MTCBus_Route285_671AD.jpg "fig:MTCBus_Route285_671AD.jpg")的降大樑公車。\]\]
[LO3X-NWFB.JPG](https://zh.wikipedia.org/wiki/File:LO3X-NWFB.JPG "fig:LO3X-NWFB.JPG")的中低底盤[冷氣](../Page/冷氣.md "wikilink")[雙層巴士](../Page/雙層巴士.md "wikilink")[利蘭奧林匹克](../Page/利蘭奧林匹克.md "wikilink")，行走於香港島的銅鑼灣，現已全數退役。\]\]
[KMB_283_HR9234_MEL_19_03_2014.jpg](https://zh.wikipedia.org/wiki/File:KMB_283_HR9234_MEL_19_03_2014.jpg "fig:KMB_283_HR9234_MEL_19_03_2014.jpg")[九巴的](../Page/九巴.md "wikilink")[冷氣](../Page/冷氣.md "wikilink")[雙層巴士](../Page/雙層巴士.md "wikilink")[富豪奧林匹克](../Page/富豪奧林匹克.md "wikilink")11米，已於2016年9月6日退出載客行列\]\]

**中低底盤公車**（），又稱為**降大樑公車**或**低入口公車**。

## 概要

中低底盤公車（非低底盤公車）的登車門踏步僅有1級或2級踏步，便能登上公車，乘車時特別方便，老弱婦孺受益明顯，而年輕力壯者也很需要，因為中低底盤公車的上下車時間較少，可以增加公車速度，方便乘客登車。

## 中低底盤公車起源

過往巴士因為使用前置[引擎設計](../Page/引擎.md "wikilink")，[傳動軸跨過地台底部](../Page/傳動軸.md "wikilink")，令巴士地台難以造得低。1960年代至1970年代開始盛行後置引擎巴士，因傳動軸不用經過地台底，令地台可以造得更低，所以，中低底盤巴士絕大部分也是後置引擎巴士。1970年代開始在日本盛行，車廂地台比舊款客車貼近路面，並且只有一至兩臺階，後來被[低底盤公車](../Page/特低地台巴士.md "wikilink")（日語：ノンステップバス）取代。

## 低底盤公車出現

由於上落車門並沒有任何梯級的[特低地台巴士](../Page/特低地台巴士.md "wikilink")（亦稱低底盤公車，日本稱，意指無梯級的巴士）出現，但[特低地台巴士目前仍有許多缺點](../Page/特低地台巴士.md "wikilink")，目前大部分客運公司傾向兩種車混用。

不過在[香港](../Page/香港.md "wikilink")，由於[特低地台巴士才定義作低底盤公車](../Page/特低地台巴士.md "wikilink")，在此條目所述的中低底盤公車不會當成低底盤公車，只會當成一般地台公車（或稱非低底盤公車）。所有此條目所述的中低底盤公車沒有為使用[輪椅的乘客的設備](../Page/輪椅.md "wikilink")，故此當巴士公司連續使用非[低底盤公車的時候](../Page/特低地台巴士.md "wikilink")，使用[輪椅的乘客即使見到本條目所定義的](../Page/輪椅.md "wikilink")**中低底盤公車**（如[富豪奧林匹克](../Page/富豪奧林匹克.md "wikilink")）也不會登車，故他們曾經常投訴沒有足夠[低底盤公車服務](../Page/特低地台巴士.md "wikilink")。為了方便他們，[九巴](../Page/九巴.md "wikilink")、[城巴](../Page/城巴.md "wikilink")、[新巴和](../Page/新巴.md "wikilink")[龍運已經在](../Page/龍運巴士.md "wikilink")2000年只購買[低底盤公車](../Page/特低地台巴士.md "wikilink")，當中[龍運已在](../Page/龍運巴士.md "wikilink")2010年全數使用[低底盤公車行走](../Page/特低地台巴士.md "wikilink")，[新巴也在](../Page/新巴.md "wikilink")2015年10月全數使用[低底盤公車行走](../Page/特低地台巴士.md "wikilink")，而其他巴士公司預料在2016至2017年間全線以[低底盤公車行走](../Page/特低地台巴士.md "wikilink")\[1\]。

在歐美，「」的定義也是在上落車門，除了巴士地台和人行道的高差所做成的階級外，沒有任何梯級，和[特低地台巴士定義一樣](../Page/特低地台巴士.md "wikilink")。

## 參考資料

<references/>

## 相關條目

  - [新幹線路線公車](../Page/新幹線路線公車.md "wikilink")

## 外部連結

  - [寶立華車輛工業股份有限公司 -
    中低地板、超低地板公車簡介](http://www.boulevard.com.tw/Monograph.html)

[Category:巴士](../Category/巴士.md "wikilink")
[Category:低地台交通](../Category/低地台交通.md "wikilink")

1.