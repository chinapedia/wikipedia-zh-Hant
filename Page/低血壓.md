**低血壓**在[生理學及](../Page/生理學.md "wikilink")[醫學上是指](../Page/醫學.md "wikilink")[血壓不正常地低](../Page/血壓.md "wikilink")。比起病症，低血壓較適合稱作一種生理狀況。目前[世界衛生組織沒有訂定低血壓的標準](../Page/世界衛生組織.md "wikilink")，但如果一般成人[肱動脈](../Page/肱動脈.md "wikilink")[血壓小於](../Page/血壓.md "wikilink")（90/60mmHg）時，可能會被判斷有低血壓。縱然沒有明確指明，但它一般都與[休克有關聯](../Page/休克.md "wikilink")。

## 自然生理學

[血壓是由身體的](../Page/血壓.md "wikilink")[自主神經系統不斷的調節](../Page/自主神經系統.md "wikilink")，透過使用精細的感應器、[神經及](../Page/神經.md "wikilink")[激素來平衡傾向](../Page/激素.md "wikilink")

## 成因及結構

[低血容積是最普遍導致低血壓的原因](../Page/低血容積.md "wikilink")。這可以是由於[出血或失血](../Page/出血.md "wikilink")、飢餓時液體攝取量不足、或是過度失去水份，如[痢疾或](../Page/痢疾.md "wikilink")[嘔吐等](../Page/嘔吐.md "wikilink")。低血容積經常會因使用[利尿劑等而引發](../Page/利尿劑.md "wikilink")，不同的[藥物使用亦會引起此情況](../Page/藥物.md "wikilink")。

儘管在正常的血容積下，因嚴重[充血性心臟衰竭](../Page/心臟衰竭.md "wikilink")、大型的[心肌梗死或](../Page/心肌梗死.md "wikilink")[心動過緩而造成](../Page/心動過緩.md "wikilink")[心輸出量的減少](../Page/心輸出量.md "wikilink")，同樣會引起低血壓，並可以急劇發展成[休克](../Page/休克.md "wikilink")。[心律不齊亦同樣經常會造成低血壓](../Page/心律不齊.md "wikilink")。[乙型交感神經阻斷劑在使用後會令](../Page/乙型交感神經阻斷劑.md "wikilink")[心跳下降及降低心肌供血的能力](../Page/心跳.md "wikilink")，亦是造成低血壓的另一成因。

因[腦部或](../Page/腦部.md "wikilink")[脊髓受損或](../Page/脊髓.md "wikilink")[自律神經失調引起的](../Page/自律神經失調.md "wikilink")[交感神經的輸出下降或](../Page/交感神經.md "wikilink")[副交感神經活躍化](../Page/副交感神經.md "wikilink")，這會導致過度的[血管舒張或不足的血管阻力](../Page/血管舒張.md "wikilink")，造成低血壓。其他引起過度血管舒張的原因有[敗血病](../Page/敗血病.md "wikilink")、[酸中毒或使用如](../Page/酸中毒.md "wikilink")[鈣離子阻斷劑或](../Page/鈣離子阻斷劑.md "wikilink")[血管緊張素轉化酶抑制劑等藥物](../Page/血管緊張素轉化酶抑制劑.md "wikilink")。很多麻醉藥及技術，包括[脊椎麻醉及大部份的](../Page/脊椎麻醉.md "wikilink")[吸入式麻醉都會造成明顯的血管舒張](../Page/吸入式麻醉.md "wikilink")。

## 综合症

[體位型低血壓](../Page/姿位性低血壓.md "wikilink")，又稱「[姿勢性低血壓](../Page/姿勢性低血壓.md "wikilink")」，是一種普遍的低血壓型態。它會在改變身體位置時，尤其是從坐下或躺下的姿勢改為直立的姿勢時出現。它一般都是暫時性的低血壓，是因[自主神經系統延誤了正常的調節功能](../Page/自主神經系統.md "wikilink")。在[低血容積時會出現此情況](../Page/低血容積.md "wikilink")。而當使用不同[藥物時](../Page/藥物.md "wikilink")，尤其是上述那些令血壓降低的藥物或是精神科藥物，如[抗抑鬱藥等](../Page/抗抑鬱藥.md "wikilink")，都會有此副作用。在躺下、坐下或站立時簡單量度[血壓及](../Page/血壓.md "wikilink")[心跳都能確定體位型低血壓的情況](../Page/心跳.md "wikilink")。

[神經心源性暈厥是](../Page/神經心源性暈厥.md "wikilink")[自律神經失調的一種](../Page/自律神經失調.md "wikilink")，尤以在直立時會出現不正常的血壓下降。[傾斜試驗可以用作測試此情況](../Page/傾斜試驗.md "wikilink")。神經心源性暈厥與[血管迷走性暈厥有所關聯](../Page/血管迷走性暈厥.md "wikilink")，因兩都是由於[副交感神經的](../Page/副交感神經.md "wikilink")[迷走神經增加活動所引起](../Page/迷走神經.md "wikilink")。

另一種較少見的是[餐後低血壓](../Page/餐後低血壓.md "wikilink")，在用餐後的30-75分鐘出現。由於在[消化時大量的](../Page/消化.md "wikilink")[血液會被送往](../Page/血液.md "wikilink")[小腸](../Page/小腸.md "wikilink")，身體必須增加心跳及[血管舒張來抵消局部的](../Page/血管舒張.md "wikilink")[血容積改變](../Page/血容積.md "wikilink")。一般相信這種形式的低血壓是因失調或自主神經系統老化未能作出適當的調節而起的。

## 指標

對於大部份的人來說，健康的[血壓介乎於](../Page/血壓.md "wikilink")115/75mmHg。血壓少量的下降，儘管少如20mmHg，都會有短暫的低血壓。

## 徵狀

低血壓的主要徵狀是[頭昏及全身無力](../Page/頭昏及全身無力.md "wikilink")。如果[血壓低至某個程度](../Page/血壓.md "wikilink")，可能會出現[昏厥](../Page/昏厥.md "wikilink")，甚至[癲癇等情況](../Page/癲癇.md "wikilink")。低血壓一般都會有以下徵狀，但多是與低血壓的成因有關，而非低血壓本身的影響：

  - [胸痛](../Page/胸痛.md "wikilink")
  - [呼吸困難](../Page/呼吸困難.md "wikilink")
  - [心律不齊](../Page/心律不齊.md "wikilink")
  - [發熱高於](../Page/發熱.md "wikilink")38.3℃或101℉
  - [頭疼](../Page/頭疼.md "wikilink")
  - [頸部僵硬](../Page/頸部僵硬.md "wikilink")
  - 嚴重的[背痛](../Page/背痛.md "wikilink")
  - [咳嗽帶有](../Page/咳嗽.md "wikilink")[痰](../Page/痰.md "wikilink")
  - 長期的[痢疾或](../Page/痢疾.md "wikilink")[嘔吐](../Page/嘔吐.md "wikilink")
  - [吞嚥困難](../Page/吞嚥困難.md "wikilink")
  - [排尿困難](../Page/排尿困難.md "wikilink")
  - [尿液帶有惡臭](../Page/尿.md "wikilink")
  - 使用[藥物後的不良反應](../Page/藥物.md "wikilink")
  - 激烈或危害生命的[過敏反應](../Page/過敏.md "wikilink")
  - 眩暈
  - 癲癇
  - [昏迷](../Page/昏迷.md "wikilink")
  - [疲勞](../Page/疲勞.md "wikilink")

## 參考

  - [WebMD：了解低血壓](https://web.archive.org/web/20050924055339/http://my.webmd.com/content/article/54/61510.htm)

## 內部連結

  - [血壓](../Page/血壓.md "wikilink")
  - [高血壓](../Page/高血壓.md "wikilink")
  - [循環系統](../Page/循環系統.md "wikilink")

[Category:血管系统疾病](../Category/血管系统疾病.md "wikilink")