低音提琴家是指专业演奏[低音提琴并取得一定成就的人](../Page/低音提琴.md "wikilink")。
和其他乐器，尤其是和其他[弦乐器不同的是](../Page/弦乐器.md "wikilink")，低音提琴家的独立性较低。这表现在低音提琴家往往作为某个乐团的演奏员活跃在舞台上，或者以在教育机构担任教师为其主要活动，而不是全职的独奏者。目前活跃在世界上的低音提琴家有很多，但知名全职的独奏者很少，其中享有世界级声望的是[美国演奏家](../Page/美国.md "wikilink")[盖瑞·卡尔](../Page/盖瑞·卡尔.md "wikilink")(Gary
Karr)。
例如，很少有小提琴独奏家在广泛开展自己的独奏事业的同时，还担任某个乐团的小提琴演奏员的职务。也较少有这样的大提琴家、双簧管演奏家、钢琴演奏家。因为他们的全职独奏活动已经足以支撑其他们的音乐事业，不需要也不可能再以乐队演奏作为补充。但相当一部分低音提琴演奏家均在乐团有固定职位。

## 历史上的低音提琴演奏家

历史上最著名的低音提琴演奏家有：
[意大利的](../Page/意大利.md "wikilink")[德拉戈奈蒂](../Page/德拉戈奈蒂.md "wikilink")(Dragonetti)和同样是意大利的[波蒂西尼](../Page/波蒂西尼.md "wikilink")(Giovanni
Bottesini)，以及20世纪的[俄罗斯指挥大师](../Page/俄罗斯.md "wikilink")[库塞维茨基](../Page/库塞维茨基.md "wikilink")(Koussevitzky)。
其中[德拉戈奈蒂是](../Page/德拉戈奈蒂.md "wikilink")“乐圣”[贝多芬的同时代人](../Page/贝多芬.md "wikilink")，他精湛的演奏打动了贝多芬，也改变了贝多芬对这个乐器的印象。从此在贝多芬的[交响曲中](../Page/交响曲.md "wikilink")，[低音提琴声部得到了极大的发展](../Page/低音提琴.md "wikilink")。而这一影响也深远地持续到贝多芬之后的音乐世代。
[波蒂西尼则被称为](../Page/波蒂西尼.md "wikilink")“低音提琴上的[帕格尼尼](../Page/帕格尼尼.md "wikilink")”，因为他成功地将小提琴上的高级辉煌技巧移植到低音提琴上，并首度让低音提琴真正地成为一个高度技巧性的独奏乐器。
[库塞维茨基是](../Page/库塞维茨基.md "wikilink")20世纪上半叶享有世纪声望的[指挥家](../Page/指挥家.md "wikilink")，但他同时也是著名的低音提琴独奏家。他为这个乐器所写的《低音提琴[协奏曲](../Page/协奏曲.md "wikilink")》已经成为每个低音提琴学习者的必备曲目。

## 现代低音提琴演奏家

### 德奥低音提琴演奏家

[沃尔夫冈·居特勒](../Page/沃尔夫冈·居特勒.md "wikilink") (Wolfgang Guettler)
前[柏林爱乐乐团首席低音提琴](../Page/柏林爱乐乐团.md "wikilink")，低音提琴演奏组Geatles的首席成员。现德国卡尔斯鲁厄音乐学院低音提琴教授
[路德维克·史特来歇](../Page/路德维克·史特来歇.md "wikilink")(Ludwig Streicher)
[奥地利著名低音提琴家](../Page/奥地利.md "wikilink")，低音提琴教育家
[克劳斯·施托尔](../Page/克劳斯·施托尔.md "wikilink")(Klaus Stoll)
前[柏林爱乐乐团首席低音提琴](../Page/柏林爱乐乐团.md "wikilink")，低音提琴教育家
阿洛以斯·波施 (Alois Posch)
奥地利低音提琴家，[维也纳爱乐乐团低音提琴演奏员](../Page/维也纳爱乐乐团.md "wikilink")

### 东欧低音提琴演奏家

[奥维迪乌·巴迪拉](../Page/奥维迪乌·巴迪拉.md "wikilink") (Ovidiu Badila)
[罗马尼亚低音提琴家](../Page/罗马尼亚.md "wikilink")。他的演奏技术曾在欧洲低音提琴届引起轰动。
[博若·帕拉季克](../Page/博若·帕拉季克.md "wikilink") (Bozo Paradzik)
[克罗地亚低音提琴家](../Page/克罗地亚.md "wikilink")。现[德国](../Page/德国.md "wikilink")[弗莱堡音乐学院低音提琴教授](../Page/弗莱堡.md "wikilink")。
[罗曼·帕德科洛](../Page/罗曼·帕德科洛.md "wikilink") (Roman Patkolo)
[斯洛伐克青年低音提琴家](../Page/斯洛伐克.md "wikilink")。低音提琴演奏组Bassiona
Amorosa的首席成员。
布戈斯拉夫·富尔托克 (Bugoslaw Furtok) [波兰青年低音提琴家](../Page/波兰.md "wikilink")。

### 美国低音提琴演奏家

[盖瑞·卡尔](../Page/盖瑞·卡尔.md "wikilink") (Gary Karr)
美国著名低音提琴演奏家。世界第一位全职低音提琴独奏家。
[爱德加·麦耶](../Page/爱德加·麦耶.md "wikilink") (Edgar Meyer) 美国著名低音提琴家、作曲家。
[杰夫·布列蒂奇](../Page/杰夫·布列蒂奇.md "wikilink") (Jeff Bradetich)
美国著名低音提琴独奏家、教育家

### 苏俄低音提琴演奏家

[罗炅·阿萨金](../Page/罗炅·阿萨金.md "wikilink") (Rodion Azarkhin)
苏联著名低音提琴演奏家。曾成功地用低音提琴演奏[巴赫的小提琴独奏曲](../Page/巴赫.md "wikilink")《[恰空](../Page/恰空.md "wikilink")》

### 法国低音提琴演奏家

[丹尼尔·马里耶](../Page/丹尼尔·马里耶.md "wikilink") (Daniel Mariller)
法国低音提琴独奏家。曾以他改编并演奏的《辛德勒名单》而闻名于业内。

### 意大利低音提琴演奏家

[弗朗哥·彼得拉季](../Page/弗朗哥·彼得拉季.md "wikilink") (Franco Petracchi)
意大利著名低音提琴独奏家、作曲家。
[朱泽陪·艾托雷](../Page/朱泽陪·艾托雷.md "wikilink") (Giuseppe Ettore) 意大利青年低音提琴家

### 日本低音提琴演奏家

[池松宏](../Page/池松宏.md "wikilink") (Hiroshi Ikematsu)
在巴西长大的日本低音提琴独奏家，新西兰交响乐团的低音提琴演奏员。

### 中国低音提琴演奏家

[陈子平](../Page/陈子平.md "wikilink") 2006年4月中央音乐学院低音提琴教研室成立后，受聘为首届主任。
[张达寻](../Page/张达寻.md "wikilink")
哈尔滨出生的青年低音提琴独奏家。其精湛的演奏艺术轰动了北美业界。
[张小笛](../Page/张小笛.md "wikilink")
中国低音提琴独奏家，[中国爱乐乐团低音提琴声部首席](../Page/中国爱乐乐团.md "wikilink")。

### 台灣低音提琴演奏家

[饒大鷗](../Page/饒大鷗.md "wikilink") （Da-ho
Rau）台灣低音提琴獨奏家，教育家，前台灣[台北市立交響樂團低音提琴首席](../Page/台北市立交響樂團.md "wikilink")

[傅永和](../Page/傅永和.md "wikilink") (Yung-Ho
Fu)台灣低音提琴獨奏家，台灣[國家交響樂團低音提琴首席](../Page/國家交響樂團.md "wikilink")

### 英国低音提琴演奏家

邓肯·麦克蒂尔 (Duncan McTier)

### 加拿大低音提琴演奏家

乔尔·夸灵顿 (Joel
Quarrington)。坚持用[五度低音提琴进行独奏演奏](../Page/五度低音提琴.md "wikilink")。

## 非古典音乐风格的低音提琴演奏家

[雷·布朗](../Page/雷·布朗.md "wikilink") (Ray Brown)
美国著名[爵士低音提琴演奏家](../Page/爵士.md "wikilink")。
[丹尼尔·汤谱逊](../Page/丹尼尔·汤谱逊.md "wikilink") (Daniel Thompson)
英国著名爵士低音提琴演奏家
[查尔斯·明古斯](../Page/查尔斯·明古斯.md "wikilink") (Charles Mingus) 美国著名爵士低音提琴演奏家

## 外部链接

  - [哪些是你心目中最知名的低音提琴演奏家？](http://bbs.cnstrad.com/thread-4583-1-3.html)

[低音提琴家](../Category/低音提琴家.md "wikilink")