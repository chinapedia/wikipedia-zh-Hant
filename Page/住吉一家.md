**住吉一家**，總本部設立於[東京](../Page/東京.md "wikilink")，日本賭徒系的黑道組織。為[住吉會的核心組織](../Page/住吉會.md "wikilink")，同時也是日本[指定暴力團](../Page/指定暴力團.md "wikilink")。與[山口組](../Page/山口組.md "wikilink")、[稻川會並稱為日本三大幫](../Page/稻川會.md "wikilink")。

[明治初期](../Page/明治.md "wikilink")，由[伊藤松五郎於](../Page/伊藤松五郎.md "wikilink")[東京](../Page/東京.md "wikilink")[芝浦成立](../Page/芝浦.md "wikilink")。

1948年，[阿部重作繼承三代目](../Page/阿部重作.md "wikilink")。

1962年，[磧上義光繼承四代目](../Page/磧上義光.md "wikilink")。

1967年，[中里一家的四代目](../Page/中里一家.md "wikilink")·[堀
政夫繼承五代目](../Page/堀政夫.md "wikilink")。並在1982年將住吉連合改編為「住吉連合會」並就任會長。1988年晉升為總裁，1990年10月病死。

1991年2月，住吉一家[向後二代目](../Page/向後.md "wikilink")·[西口茂男繼承六代目](../Page/西口茂男.md "wikilink")，並將核心組織的「住吉連合會」再次改編為「[住吉會](../Page/住吉會.md "wikilink")」並就任會長。1998年6月，西口晉升為[住吉會總裁](../Page/住吉會.md "wikilink")。

2005年4月17日，住吉會二代目會長·[福田晴瞭繼承七代目](../Page/福田晴瞭.md "wikilink")。

## 歷代總長

  - 初代：[伊藤松五郎](../Page/伊藤松五郎.md "wikilink")
  - 二代目：[倉持直吉](../Page/倉持直吉.md "wikilink")
  - 三代目（1948年～1962年）：[阿部重作](../Page/阿部重作.md "wikilink")
  - 四代目（1962年～1967年）：[磧上義光](../Page/磧上義光.md "wikilink")（舊住吉會會長）
  - 五代目（1967年～1990年）：[堀
    政夫](../Page/堀政夫.md "wikilink")（住吉連合會總裁、[中里一家四代目](../Page/中里一家.md "wikilink")）
  - 六代目（1991年～2005年）：[西口茂男](../Page/西口茂男.md "wikilink")（住吉會總裁、住吉一家[向後二代目](../Page/向後.md "wikilink")）
  - 七代目（2005年～）：[福田晴瞭](../Page/福田晴瞭.md "wikilink")（住吉會二代目會長、住吉一家[小林會二代目會長](../Page/小林會.md "wikilink")）

## 旗下組織

  - [淺草高橋組五代目](../Page/淺草高橋組.md "wikilink") - 東京
    淺草。五代目・[菊池紘一](../Page/菊池紘一.md "wikilink")
  - [稻葉興業會](../Page/稻葉興業會.md "wikilink") - 東京 立川。二代目・小玉政光
  - [奧州山口五代目](../Page/奧州山口.md "wikilink") - 山形市。五代目・大場 嵩
  - [大前田八代目](../Page/大前田.md "wikilink") - 群馬 伊勢崎。八代目・高橋八州男
  - [荻窪初代](../Page/荻窪.md "wikilink") - 東京 荻窪。初代·村田次弘
  - [音羽七代目](../Page/音羽.md "wikilink") - 東京 文京區。七代目・松坂浩三
  - [共和六代目](../Page/共和_\(日本暴力團\).md "wikilink") - 千葉 富里市。六代目・關 功
  - [向後五代目](../Page/向後.md "wikilink") - 東京 新宿。五代目・醍醐正夫
  - [小林會](../Page/小林會.md "wikilink") - 東京 銀座。三代目會長・小林忠紘
  - [西海家七代目](../Page/西海家.md "wikilink") - 仙台。七代目・守屋長清
      - [早坂會](../Page/早坂會.md "wikilink") - 仙台。三代目・富田一郎
      - [藤川會](../Page/藤川會.md "wikilink") - 仙台。三代目會長・三浦登志和
  - [三心會](../Page/三心會.md "wikilink") - 北海道 根室。會長・高橋房一
  - [四軒會](../Page/四軒會.md "wikilink") - 東京 吉祥寺。佐伯茂雄
  - [十條領家](../Page/十條領家.md "wikilink") - 太田健真
  - [親和會](../Page/亲和会_\(住吉一家\).md "wikilink") - 栃木 栃木市。二代目・小松澤英雄
      - 栃木二代目 - 栃木 栃木市。二代目・小松澤英雄
      - [矢畑五代目](../Page/矢畑.md "wikilink") - 五代目・川邊泰介
      - [大和屋六代目](../Page/大和屋.md "wikilink") - 茨城。六代目・大塚 勝
  - [大日本興行](../Page/大日本興行.md "wikilink") - 東京。三代目・伊藤嘉彥
      - [神明大場二代目](../Page/神明大場.md "wikilink") - 2代目・松本 啟
  - [瀧野川八代目](../Page/瀧野川.md "wikilink") - 東京 大塚。八代目・栗原忠男
  - [鶴川五代目](../Page/鶴川.md "wikilink") - 五代目・關口弘
  - [同人會](../Page/同人會.md "wikilink")
  - [中里六代目](../Page/中里.md "wikilink") - 千葉 野田。六代目・永井 實
  - [西村會二代目](../Page/西村會.md "wikilink") - 青森 三澤。會長・南鄉國光
  - [沼澤會](../Page/沼澤會.md "wikilink") - 東京。會長・沼澤春男
  - [花田會](../Page/花田會.md "wikilink") - 東京 江東區。會長・山城 功
  - [日野六代目](../Page/日野_\(日本暴力團\).md "wikilink") - 東京。六代目・水川源平
  - [平塚六代目](../Page/平塚.md "wikilink") - 埼玉 大宮。六代目・中村勝年
  - [廣川會](../Page/廣川會.md "wikilink")
  - [深川江島五代目](../Page/深川江島.md "wikilink") - 五代目・小倉正治
  - [武州前川八代目](../Page/武州前川.md "wikilink") - 埼玉。八代目・天野桂三
  - [府中初代](../Page/府中.md "wikilink") - 東京 府中。初代・長久保征夫
  - [牧友睦四代目](../Page/牧友睦.md "wikilink") - 東京 紀尾井町。會長・關 秀夫
  - 馬橋七代目 - 千葉
  - [丸唐會](../Page/丸唐會.md "wikilink") - 福島。會長・井田勝基
      - [神谷三代目](../Page/神谷.md "wikilink") - 三代目・大谷 進
      - [湊九代目](../Page/湊.md "wikilink") - 9代目：[菊池
        馨](../Page/菊池馨.md "wikilink")
  - [武藏屋十代目](../Page/武藏屋.md "wikilink") - 東京 台東區。十代目・松廣昭平
  - [村田會二代目](../Page/村田會.md "wikilink") - 二代目・末次久泰
  - [家根彌八代目](../Page/家根彌.md "wikilink") - 八代目・金子幸市
  - [領家六代目](../Page/領家.md "wikilink") - 六代目・齊田 宏

[Category:住吉会](../Category/住吉会.md "wikilink")
[Category:關東地區暴力團](../Category/關東地區暴力團.md "wikilink")
[Category:港區 (東京都)](../Category/港區_\(東京都\).md "wikilink")