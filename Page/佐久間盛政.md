**佐久間盛政**（1554年？—1583年7月1日）是[日本戰國時代至](../Page/日本戰國時代.md "wikilink")[安土桃山時代的武將](../Page/安土桃山時代.md "wikilink")。[織田氏家臣](../Page/織田氏.md "wikilink")。父親是[佐久間盛次](../Page/佐久間盛次.md "wikilink")。母親是[柴田勝家的姐姐](../Page/柴田勝家.md "wikilink")（『[寬政重修諸家譜](../Page/寬政重修諸家譜.md "wikilink")』。一說是妹妹）。

## 生平

### 織田家臣時期

在[天文](../Page/天文_\(後奈良天皇\).md "wikilink")23年（1554年）於[尾張御器所](../Page/尾張.md "wikilink")（現今[名古屋市](../Page/名古屋市.md "wikilink")[昭和區御器所](../Page/昭和區.md "wikilink")）出生。幼名是**理助**（**理介**）。

[永祿](../Page/永祿.md "wikilink")11年（1568年），參與[觀音寺城之戰](../Page/觀音寺城之戰.md "wikilink")（此戰為[初陣](../Page/初陣.md "wikilink")）。[元龜元年](../Page/元龜.md "wikilink")（1570年），進攻[越前國](../Page/越前國.md "wikilink")，並參與[野洲河原之戰](../Page/野洲河原之戰.md "wikilink")。[天正元年](../Page/天正_\(日本\).md "wikilink")（1573年），參與[槙島城之戰](../Page/槙島城之戰.md "wikilink")。在許多戰事中立下戰功。

天正3年（1575年），在叔父[柴田勝家被賜予越前一國後](../Page/柴田勝家.md "wikilink")，成為勝家的[與力](../Page/與力.md "wikilink")，擔任柴田軍的先鋒。此後，在與[北陸的](../Page/北陸.md "wikilink")[一向一揆等的戰鬥中](../Page/一向一揆.md "wikilink")，亦立下戰功，受[織田信長賜予](../Page/織田信長.md "wikilink")。

天正5年（1577年），在[越後國的](../Page/越後國.md "wikilink")[上杉謙信開始南下時](../Page/上杉謙信.md "wikilink")，被信長派遣到[加賀國](../Page/加賀國.md "wikilink")，並在御幸塚（現今[石川縣](../Page/石川縣.md "wikilink")[小松市](../Page/小松市.md "wikilink")）築起城砦，負責守備（『[信長公記](../Page/信長公記.md "wikilink")』）。

[thumb藏](../Page/file:Morimasa.jpg.md "wikilink")）\]\]
天正8年（1580年）6月，在[鳥越城之戰中](../Page/鳥越城之戰.md "wikilink")，於兩次敗北後，織田軍在11月攻陷[加賀一向一揆的](../Page/加賀一向一揆.md "wikilink")[尾山御坊](../Page/尾山御坊.md "wikilink")。成為加賀[金澤城初代城主](../Page/金澤城.md "wikilink")，被賜予加賀半國的支配權。

天正9年（1581年），在勝家赴往[安土城時](../Page/安土城.md "wikilink")，[上杉景勝等乘機侵入加賀](../Page/上杉景勝.md "wikilink")，並攻陷白山城（[舟岡城](../Page/舟岡城.md "wikilink")）。此時，前往救援，在城池已被攻陷的情況下，擊破上杉軍。翌年（1582年），[能登國的](../Page/能登國.md "wikilink")[溫井景隆等因為景勝的煽動而起兵](../Page/溫井景隆.md "wikilink")，在死守時，受[前田利家邀請](../Page/前田利家.md "wikilink")，於是前往救援，並擊殺景隆等（[荒山合戰](../Page/荒山合戰.md "wikilink")）。

天正10年（1582年）6月2日，在[本能寺之變當日](../Page/本能寺之變.md "wikilink")，跟隨勝家攻擊上杉家的[越中國](../Page/越中國.md "wikilink")[松倉城](../Page/松倉城.md "wikilink")。在勝家率軍往越前撤退，並打算討伐[明智光秀而](../Page/明智光秀.md "wikilink")[上洛時](../Page/上洛.md "wikilink")，向勝家說明情勢，於是勝家放棄上洛（『[村井賴重覺書](../Page/村井賴重覺書.md "wikilink")』。不過此説成疑）。

### 賤岳之戰

[thumb](../Page/file:SakumaMorimasa.png.md "wikilink")
[thumb畫](../Page/file:Sakuma_Morimasa.jpg.md "wikilink")）\]\]
[勝家在](../Page/柴田勝家.md "wikilink")[清洲會議後](../Page/清洲會議.md "wikilink")，與[羽柴秀吉對立](../Page/羽柴秀吉.md "wikilink")，[天正](../Page/天正_\(日本\).md "wikilink")11年（1583年），雙方在[近江國](../Page/近江國.md "wikilink")畔對陣。最初雙方有展開持久戰之勢，在從兄弟[柴田勝豐](../Page/柴田勝豐.md "wikilink")（勝家的養子）投向秀吉一側後，勝豐的家臣秘密前來陣中，並傳達秀吉正前往大垣。

[thumb畫](../Page/file:鬼玄蕃.png.md "wikilink")）\]\]
得到這個情報後，向勝家提議急襲[中川清秀的陣地](../Page/中川清秀.md "wikilink")，起初勝家反對，但是最終妥協，條件是「攻陷城砦後立即回來」。在殺死清秀後，準備攻向[羽柴秀長的陣地](../Page/羽柴秀長.md "wikilink")。此後，向守備賤岳砦的[桑山重晴命令](../Page/桑山重晴.md "wikilink")「降伏並讓出城砦」，桑山則回答「不抵抗，不過希望能等到日落」，賤岳砦的陷落似乎無法避免。

另一方面，以船渡過[琵琶湖的](../Page/琵琶湖.md "wikilink")[丹羽長秀軍登陸](../Page/丹羽長秀.md "wikilink")，並與本應在日落時從城砦退去的桑山隊會合，於是為了加強攻勢而攻佔賤岳砦的作戰失敗（『[柴田退治記](../Page/柴田退治記.md "wikilink")』）。而秀吉則命令之前就準備好的軍隊，以強行軍的方式返回戰場（），於是，因為深入敵陣而被包圍，在[前田利家等部隊撤退後](../Page/前田利家.md "wikilink")，部隊與勝家本陣的連絡被切斷。

結果，勝家軍被秀吉軍大敗，自身則逃往[加賀國](../Page/加賀國.md "wikilink")。

### 最後

在逃走途中，在[越前國府中附近的中村山中](../Page/越前國.md "wikilink")，被鄉民捕獲，在明白自己命數已盡後，提出與[秀吉對話的要求](../Page/羽柴秀吉.md "wikilink")（而引渡的鄉民卻立即被處刑）。在引渡途中，被[淺野長政嘲問](../Page/淺野長政.md "wikilink")「被稱為鬼玄蕃的你，為什麼戰敗後還不自殺呢」（），但是盛政回答「[源賴朝公被](../Page/源賴朝.md "wikilink")[大庭景親擊敗時](../Page/大庭景親.md "wikilink")，躲進樹洞並逃走，最後不是成就了大事嗎」（），這個回答令到周圍的人相當感慨。

雖然得到秀吉賞識，並承諾在平定[九州後](../Page/九州.md "wikilink")，賜予[肥後國](../Page/肥後國.md "wikilink")。不過以無法忘記[信長和](../Page/織田信長.md "wikilink")[勝家的大恩為由拒絕](../Page/柴田勝家.md "wikilink")，並說「如果得生並見到秀吉殿的話，我一定會殺死閣下吧。請賜下死罪」（）。於是秀吉放棄說服，並命令使用保留[武士名譽的](../Page/武士_\(日本\).md "wikilink")[切腹](../Page/切腹.md "wikilink")，不過提出希望以敗軍之將的身份被處刑，因此對秀吉說「可以的話，讓我乘車，並向上下的人們看見我受綑綁的樣子，從[一條的十字路向](../Page/一條通.md "wikilink")[下京遊街示眾](../Page/下京.md "wikilink")，這樣就好了。如此一來，秀吉殿的威光亦會響徹天下吧」（）（『』）。秀吉得知這個要求後，贈送二重。因為不喜歡花紋和款式，提出「往死的衣裝應該像在戰場的大一樣，鮮明顯眼才好。這樣才會讓盛政想死」（），希望得到染上大紋的紅色廣袖，而裡面則是使用紅梅的小袖，秀吉對此說「直到最後還不忘武者之心的人哦。好吧好吧」（），於是賜予新的小袖。

此後，隨著秀吉在[京都市中乘車遊街示眾](../Page/京都.md "wikilink")，此時「年齡三十，為了一看聞名於世的鬼玄蕃，貴賤上下站在馬車道上，男女並立觀看。盛政一面以怒目環顧，一面前行」（）（『』），被帶到宇治槙島後，被[斬首](../Page/斬首.md "wikilink")。享年30歲（有異說）。秀吉到最後仍感到可惜，在遊行時，秘密遞上短刀，令自己至少能像武士一樣切腹，不過予以拒絕，並從容往死。戒名是**英伯善俊大禪定門**。

辭世句為「」。

## 死後

[Biyoujinja1.JPG](https://zh.wikipedia.org/wiki/File:Biyoujinja1.JPG "fig:Biyoujinja1.JPG")
有一名叫[虎姬的女兒](../Page/虎姬_\(中川秀成\).md "wikilink")，成為盛政的義弟[新庄直賴養女](../Page/新庄直賴.md "wikilink")，後來因為秀吉的命令，嫁予[中川清秀的次男](../Page/中川清秀.md "wikilink")[秀成](../Page/中川秀成.md "wikilink")，成為[豐後國](../Page/豐後國.md "wikilink")[岡藩藩主的夫人](../Page/岡藩.md "wikilink")。因此，[菩提寺是](../Page/菩提寺.md "wikilink")[大分縣](../Page/大分縣.md "wikilink")[竹田市的](../Page/竹田市.md "wikilink")[英雄寺](../Page/英雄寺.md "wikilink")。

在虎姬死後，秀成因為虎姬的遺願，令五男[勝成復興自家](../Page/佐久間勝成.md "wikilink")，其子孫在[大分市存續至今](../Page/大分市.md "wikilink")。

另外，還有其他子孫繼承家名，並成為[尾張德川家的家臣](../Page/尾張德川家.md "wikilink")（『[士林泝洄](../Page/士林泝洄.md "wikilink")』、『[尾張群書系圖部集](../Page/尾張群書系圖部集.md "wikilink")』、『[尾張國諸家系圖](../Page/尾張國諸家系圖.md "wikilink")』）。虎姬的女兒誕下的兒子中，以[重行為初代](../Page/佐久間重行.md "wikilink")，一直存續至[重直](../Page/佐久間重直.md "wikilink")、[重勝](../Page/佐久間重勝.md "wikilink")、[重賢](../Page/佐久間重賢.md "wikilink")、[重豐](../Page/佐久間重豐.md "wikilink")、[雅重](../Page/佐久間雅重.md "wikilink")。在[京都所司代](../Page/京都所司代.md "wikilink")[板倉重宗和](../Page/板倉重宗.md "wikilink")[信濃國](../Page/信濃國.md "wikilink")藩主[佐久間勝之](../Page/佐久間勝之.md "wikilink")（弟弟）的幫助下，第2代的重直擔任[上州安中](../Page/上野國.md "wikilink")、坂元兩所[奉行](../Page/奉行.md "wikilink")。在雅重一代，由恢復為本姓[三浦氏](../Page/三浦氏.md "wikilink")，此後斷絕。

## 人物、逸話

  - 因為勇猛，被稱為**鬼玄蕃**、**夜叉玄蕃**，受人恐懼。
  - 在『』中，記載為「身長六尺」（約182cm）。不論數値的真偽，可知是一位巨漢。
  - 被『[尾張武人物語](../Page/尾張武人物語.md "wikilink")』評價為「在尾張出生的猛將猛卒之中，若要探其強將，如佐久間盛政，當被推為其中第一的人」（）。

## 登場作品

  - 小說

<!-- end list -->

  - 『』（作者：，收錄在『』的短編）

## 外部連結

  - [武家家伝＿佐久間氏](http://www2.harimaya.com/sengoku/html/sakuma_k.html)

## 相關條目

  -
  -
[Category:戰國武將](../Category/戰國武將.md "wikilink")
[Category:尾張國出身人物](../Category/尾張國出身人物.md "wikilink")
[Category:16世紀出生](../Category/16世紀出生.md "wikilink")
[Category:1583年逝世](../Category/1583年逝世.md "wikilink")
[Category:佐久間氏](../Category/佐久間氏.md "wikilink")
[Category:被斬首者](../Category/被斬首者.md "wikilink")