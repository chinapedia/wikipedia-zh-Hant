**佐佐成政**（）是[日本戰國時代至](../Page/日本戰國時代.md "wikilink")[安土桃山時代的武將](../Page/安土桃山時代.md "wikilink")、[大名](../Page/大名.md "wikilink")。父親是[佐佐成宗](../Page/佐佐成宗.md "wikilink")（盛政）。通稱**內藏助**。[鷹司孝子](../Page/鷹司孝子.md "wikilink")（本理院）的外祖父。

是[尾張國](../Page/尾張國.md "wikilink")的[土豪](../Page/土豪.md "wikilink")。可能是流[佐佐木氏一族](../Page/佐佐木氏.md "wikilink")。

## 生平

### 尾張時期

生年不詳（有諸多說法），家中三男。因為兄長[佐佐政次和](../Page/佐佐政次.md "wikilink")[佐佐孫介相繼戰死](../Page/佐佐孫介.md "wikilink")，於是在[永祿](../Page/永祿.md "wikilink")3年（1560年）繼任[家督](../Page/家督.md "wikilink")，成為比良城城主。

仕於[織田信長](../Page/織田信長.md "wikilink")，成為後不斷立下戰功而嶄露頭角。永祿4年（1561年），在[森部之戰中](../Page/森部之戰.md "wikilink")，與[池田恒興一同殺死敵將](../Page/池田恒興.md "wikilink")[稻葉又右衛門常通](../Page/稻葉常通.md "wikilink")（[稻葉一鐵的叔父](../Page/稻葉一鐵.md "wikilink")），立下大功。永祿10年（1567年），被提拔為[黑母衣眾之一](../Page/黑母衣眾.md "wikilink")。[元龜元年](../Page/元龜.md "wikilink")（1570年）6月，在[姉川之戰的前哨戰中](../Page/姉川之戰.md "wikilink")，與[簗田廣正和](../Page/簗田廣正.md "wikilink")[中條家忠等人一同率領少數馬廻眾擔當](../Page/中條家忠.md "wikilink")[殿軍](../Page/殿軍.md "wikilink")，指揮[鐵砲隊十分活躍](../Page/鐵砲.md "wikilink")（『[信長公記](../Page/信長公記.md "wikilink")』、『』）。

[天正](../Page/天正_\(日本\).md "wikilink")2年（1574年），長男[松千代丸在與](../Page/佐佐松千代丸.md "wikilink")[長島一向一揆作戰時戰死](../Page/長島一向一揆.md "wikilink")。天正3年（1575年）5月，在[長篠之戰中](../Page/長篠之戰.md "wikilink")，與[前田利家](../Page/前田利家.md "wikilink")、[野野村正成](../Page/野野村正成.md "wikilink")、[福富秀勝](../Page/福富秀勝.md "wikilink")、[塙直政等人率領鐵砲隊](../Page/塙直政.md "wikilink")。

### 府中三人眾時期

天正3年（1575年）9月，信長制壓[越前國後](../Page/越前國.md "wikilink")，任命[柴田勝家為](../Page/柴田勝家.md "wikilink")[北陸方面的軍團長](../Page/北陸.md "wikilink")。此時，與前田利家、[不破光治三人](../Page/不破光治.md "wikilink")（[府中三人眾](../Page/府中三人眾.md "wikilink")）一同以[與力](../Page/與力.md "wikilink")、[目付的身份](../Page/目付.md "wikilink")，輔助勝家並給予越前府中3萬3千石，築起並以之為居城。府中三人眾除了負責輔助勝家外，還是織田軍中半獨立的遊撃軍，在[石山合戰](../Page/石山合戰.md "wikilink")、平定[播磨國和征伐](../Page/播磨國.md "wikilink")[荒木村重時都有參戰](../Page/荒木村重.md "wikilink")，其中，府中三人眾負責執行對荒木一族的處刑。天正5年（1578年）8月，為了攻擊侵入[能登國的上杉軍](../Page/能登國.md "wikilink")，與勝家等人一同入侵[加賀國](../Page/加賀國.md "wikilink")，但在[七尾城陷落後撤退](../Page/七尾城.md "wikilink")。

### 越中時期

[天正](../Page/天正_\(日本\).md "wikilink")8年（1580年），聯合[神保長住](../Page/神保長住.md "wikilink")，平定與[一向一揆和](../Page/一向一揆.md "wikilink")[上杉氏交戰的最前線](../Page/上杉氏.md "wikilink")[越中國](../Page/越中國.md "wikilink")。同年秋天，修築了「佐佐堤」。天正9年（1581年）2月，正式被給予半個越中國。翌年（1582年），因為長住失勢而成為一國[守護](../Page/守護.md "wikilink")，以[富山城為居城並進行大規模改修](../Page/富山城.md "wikilink")。

天正10年（1582年），在[本能寺之變發生時](../Page/本能寺之變.md "wikilink")，北陸方面的織田軍經過3個月圍攻後，終於攻略上杉軍在北陸最後的據點（[魚津城之戰](../Page/魚津城之戰.md "wikilink")），但是在接到本能寺之變的消息後，諸將都各自返回自己的領地，因此遭到上杉軍的反撃，成政因為要防禦上杉軍而不能動身。而柴田勝家雖然亦打算[上洛](../Page/上洛.md "wikilink")，不過[羽柴秀吉在與](../Page/羽柴秀吉.md "wikilink")[毛利氏對峙中](../Page/毛利氏.md "wikilink")，選擇與毛利氏和睦，令[中國大返還](../Page/中國大返還.md "wikilink")（）成功，早一步返回[畿內](../Page/畿內.md "wikilink")，並擊殺[明智光秀](../Page/明智光秀.md "wikilink")。

[thumb](../Page/file:Sasaragoe.jpg.md "wikilink")
在[清洲會議中](../Page/清洲會議.md "wikilink")，柴田勝家與羽柴秀吉爭奪織田家中實權時，投向柴田方。天正11年（1583年），在[賤岳之戰中](../Page/賤岳之戰.md "wikilink")，因為要在越中防備[上杉景勝而不能參戰](../Page/上杉景勝.md "wikilink")，派出叔父[佐佐平左衛門率領](../Page/佐佐平左衛門.md "wikilink")6百援軍參戰，自身則沒有參戰，結果，因為[前田利家倒向秀吉軍](../Page/前田利家.md "wikilink")，勝家戰敗自盡。之後受到上杉景勝不斷壓迫，於是交出女兒成為人質和剃髪後，向秀吉降伏，被允許繼續以越中一國為領地。翌年（1584年），[小牧長久手之戰開始後](../Page/小牧長久手之戰.md "wikilink")，在3月期間的書狀中，顯示投向秀吉方的立場，不過在夏天期間，投向[德川家康和](../Page/德川家康.md "wikilink")[織田信雄陣營](../Page/織田信雄.md "wikilink")，並攻擊秀吉方的利家的（[末森城之戰](../Page/末森城之戰.md "wikilink")）。此時，因為與[越後國的上杉景勝敵對](../Page/越後國.md "wikilink")，需要兩面作戰，因此陷入苦戰。在秀吉與信雄議和後，家康亦選擇停戰。為了令家康再次舉兵，在嚴冬中越過[飛驒山脈](../Page/飛驒山脈.md "wikilink")[立山山系](../Page/立山.md "wikilink")，並進入[濱松城游說家康](../Page/濱松城.md "wikilink")，但是未能説服家康再次出兵，亦得不到織田信雄和[瀧川一益的回應](../Page/瀧川一益.md "wikilink")。

[Sassa_Narimasa's_Tonsure_Monument.jpg](https://zh.wikipedia.org/wiki/File:Sassa_Narimasa's_Tonsure_Monument.jpg "fig:Sassa_Narimasa's_Tonsure_Monument.jpg")[富山市的](../Page/富山市.md "wikilink")）\]\]
天正13年（1585年），秀吉親自向越中出兵，以10萬大軍包圍富山城。成政在織田信雄仲介下，向秀吉降伏（[越中征伐](../Page/越中征伐.md "wikilink")）。被秀吉沒收除了越中東部的以外的所有領地，並與妻子一同被移往[大坂](../Page/大坂.md "wikilink")，以後以[御伽眾的身份仕於秀吉](../Page/御伽眾.md "wikilink")。天正15年（1587年），被賜予羽柴的姓氏。

### 肥後時期

[天正](../Page/天正_\(日本\).md "wikilink")15年（1587年），在[九州征伐中立下戰功](../Page/九州征伐.md "wikilink")，被賜予[肥後國](../Page/肥後國.md "wikilink")。受秀吉指示要推行改革，急忙推行[太閤檢地引發](../Page/太閤檢地.md "wikilink")[國人暴動而無力鎮壓](../Page/國人.md "wikilink")（）。因此被秀吉責罰，[安國寺惠瓊的求情亦沒有效果](../Page/安國寺惠瓊.md "wikilink")，被命令在[攝津國尼崎的](../Page/攝津國.md "wikilink")[切腹](../Page/切腹.md "wikilink")，享年53歲。戒名是**成政寺庭月道閑大居士**。

[Castle_Site_of_Hira.jpg](https://zh.wikipedia.org/wiki/File:Castle_Site_of_Hira.jpg "fig:Castle_Site_of_Hira.jpg")[名古屋市西區比良的](../Page/名古屋市.md "wikilink")）\]\]
是「」。

## 人物

  - 當時[越中國經常河川氾濫形成水災](../Page/越中國.md "wikilink")，成政在數年的越中統治中建造堤防，成功防止水災而被領民仰慕。那堤防被命名為「濟民堤」、「佐佐堤」，現在當地仍有遺留下來的結構。

<!-- end list -->

  - 能力受到[織田信長高度評價而被提拔成為一國大名](../Page/織田信長.md "wikilink")，是受到信長的實力主義恩惠的其中一人。在[織田信雄降伏後](../Page/織田信雄.md "wikilink")，選擇與[德川家康合作對抗秀吉是因為對](../Page/德川家康.md "wikilink")[織田氏非常忠誠](../Page/織田氏.md "wikilink")。

<!-- end list -->

  - 被認為相當討厭秀吉。在信長在生時，因為於[北陸方面作戰而與秀吉的敵對者](../Page/北陸.md "wikilink")[柴田勝家編在一起](../Page/柴田勝家.md "wikilink")；根據前田家的文書記錄，有一段時期，攻略被上杉方奪去的[富山城時曾與勝家發生過很大的爭執](../Page/富山城.md "wikilink")，因此很難說是因為親密關係而合作一起反秀吉。實際上以防備上杉家的方針下，在[賤岳之戰中沒有派遣援兵](../Page/賤岳之戰.md "wikilink")，而且在勝家滅亡後沒有抗戰就馬上剃髪並向秀吉投降，因此亦有可能是因為和勝家雙方都是不想成為背後的敵人而結成的利害關係。

## 逸話

  - 本來是從屬於[織田信安](../Page/織田信安.md "wikilink")（『』）。

<!-- end list -->

  - 『[信長公記](../Page/信長公記.md "wikilink")』第一卷中記載，成政曾經企圖暗殺信長。

<!-- end list -->

  - 在[永祿](../Page/永祿.md "wikilink")5年（1562年）進攻[美濃國時](../Page/美濃國.md "wikilink")，於[輕海之戰中](../Page/輕海之戰.md "wikilink")，將敵將稻葉又衛門的首級讓予[前田利家](../Page/前田利家.md "wikilink")，利家又讓給成政，最後由[柴田勝家把首級帶回](../Page/柴田勝家.md "wikilink")，根據獲得首級的次序，信長給予3人褒獎（『常山記談』、『[名將言行錄](../Page/名將言行錄.md "wikilink")』等）。但是根據『信長公記』，稻葉是被成政和[池田恒興殺死的](../Page/池田恒興.md "wikilink")，與柴田和前田的逸話很可能是因為後來他們在北陸軍方面同行而創作的故事。實際上，利家在[十阿彌殺害事件而被罰流放後](../Page/十阿彌.md "wikilink")，成政就很討厭利家。

<!-- end list -->

  - 登用新家臣時，會給予比最初提示的俸祿還高的薪金，被視為非常慷慨的大人。從不拒絕希望仕官的人，而且與他們會見時，不會在意家世或血緣。而是重視過去立下的武功。

<!-- end list -->

  - 在[天正](../Page/天正_\(日本\).md "wikilink")18年（1590年）的[小田原征伐中](../Page/小田原征伐.md "wikilink")，[蒲生氏鄉向秀吉請求使用](../Page/蒲生氏鄉.md "wikilink")「三階菅笠」[馬印](../Page/馬印.md "wikilink")，秀吉說「三階菅笠是武勇的佐佐成政使用的馬印。如果能立下相應的戰功的話就允許使用吧」（「」），氏郷聽到此話後，在小田原征伐中奮戰並滿身傷痕，於是就成功地被允許使用該馬印（『常山紀談』）。秀吉和前田氏對成政的惡評是後世創作，令到成政總是受到過低評價，秀吉和前田都認同成政作為軍事指揮官的能力，並有很多讚賞記錄。

<!-- end list -->

  - 在[富山縣](../Page/富山縣.md "wikilink")[吳東地區中](../Page/吳東.md "wikilink")，在[江戶時代開始](../Page/江戶時代.md "wikilink")，就與於[賤岳之戰中背叛上司柴田勝家的](../Page/賤岳之戰.md "wikilink")[加賀藩](../Page/加賀藩.md "wikilink")、[富山藩藩主前田家相對](../Page/富山藩.md "wikilink")，到最後還盡忠節、以及施行治水工事等善政的佐佐成政的人氣相當高。但是與此同時，以[高岡市為中心的吳西地區中](../Page/高岡市.md "wikilink")，第3代藩主[前田利常的](../Page/前田利常.md "wikilink")[菩提寺在高岡](../Page/菩提寺.md "wikilink")，以及在越中亦不是在加賀的支藩富山藩，而是在加賀藩本領內，對加賀前田百萬石亦受到相當的敬愛。

<!-- end list -->

  - 在越過[飛驒山脈](../Page/飛驒山脈.md "wikilink")[立山山系](../Page/立山.md "wikilink")「」的路線中，有埋藏金的傳説。當地有「朝日、夕陽發光的鍬崎，七種連結七神，在許多黃金前發光」（）的歌謡流傳，這段文字被傳是解開黄金之謎的關鍵。

[Isobe_no_Ippon_Enoki.jpg](https://zh.wikipedia.org/wiki/File:Isobe_no_Ippon_Enoki.jpg "fig:Isobe_no_Ippon_Enoki.jpg")[富山市](../Page/富山市.md "wikilink")）\]\]

  - 有被稱為「早百合」的美麗側室。成政相當寵愛早百合，後來早百合懷孕。此時成政正在城外並有「早百合與人私通。腹中的孩子不是成政大人的孩子」的流言流傳。歸城後的成政聽聞後極度憤怒，沒有問清楚究竟就把早百合帶到，抓著她的頭髪並把她殺死。不只如此，還把早百合一族18人全部斬首，用和[磔刑把佢們殺死](../Page/磔刑_\(日本\).md "wikilink")。在早百合死去時大叫「我在這裡被斬殺的同時，怨恨會成為惡鬼，在數年把你的子孫全部殺死，令你的家名斷絕」（）（『』）。還有早百合留下「立山的黑百合開花時，佐佐家將會滅亡」（）的詛咒後死去（[黑百合傳説](../Page/黑百合傳説.md "wikilink")）。根據[佐佐瑞雄](../Page/佐佐瑞雄.md "wikilink")（成政的姪兒，[佐佐直勝的子孫](../Page/佐佐直勝.md "wikilink")）的說法，他的母親曾說過「我們的家中絕對不能讓百合科的花生存」（）。在早百合被殺的神通川河邊，在風雨之夜就會有女人頭和[鬼火出現](../Page/鬼火.md "wikilink")，這被說是「」。亦有很多早百合無念地死去的故事流傳，不過這些故事被認為是在成政死後才被創作出來。除了這些故事以外，在成政死後，與勝家同樣有許多被貶低而真偽不明的逸話流傳。

<!-- end list -->

  - 每年7月尾，在[富山市上瀧地區](../Page/富山市.md "wikilink")，有佐佐成政戰國時代祭典等舉辦。

## 登場作品

  - 小説

<!-- end list -->

  - 『』（作者：）
  - 『』（作者：）
  - 『』（作者：）
  - 『』（作者：）

<!-- end list -->

  - 電視劇

<!-- end list -->

  - 『[利家與松](../Page/利家與松.md "wikilink")』（[NHK大河劇](../Page/NHK大河劇.md "wikilink")，2002年，演員：）
  - 『[軍師官兵衛](../Page/軍師官兵衛.md "wikilink")』（NHK大河劇，2014年，演員：）

## 外部連結

  - [武家家伝＿佐々氏](http://www2.harimaya.com/sengoku/html/sasa_k.html)

## 相關條目

  -
[Category:戰國武將](../Category/戰國武將.md "wikilink")
[Category:尾張國出身人物](../Category/尾張國出身人物.md "wikilink")
[Category:佐佐氏](../Category/佐佐氏.md "wikilink")
[Category:織豐政權大名](../Category/織豐政權大名.md "wikilink")
[Category:日本自殺者](../Category/日本自殺者.md "wikilink")
[Category:織田信長](../Category/織田信長.md "wikilink")
[Category:越中國出身人物](../Category/越中國出身人物.md "wikilink")