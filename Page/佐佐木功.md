**佐佐木功**（，）是[日本](../Page/日本.md "wikilink")[歌手](../Page/歌手.md "wikilink")、[演員](../Page/演員.md "wikilink")、[聲優](../Page/聲優.md "wikilink")。本名（讀音相同）。

## 經歷

生於[東京都](../Page/東京都.md "wikilink")，住在[千葉縣](../Page/千葉縣.md "wikilink")[市川市](../Page/市川市.md "wikilink")。

私立曉星小學校、[私立武藏中學校·高等學校畢業](../Page/武藏中學校・高等學校.md "wikilink")。成績優秀，原本以東京大學當做升學的目標，但是最後並沒有升學。

在就讀[武藏高校時](../Page/武藏中學校·高等學校.md "wikilink")，就開始從事歌手的活動，在1960年推出『[你就是真愛](../Page/你就是真愛.md "wikilink")』（翻唱自貓王的曲目）一曲以[Rock-a-Billy歌手身分出道](../Page/Rock-a-Billy.md "wikilink")，被稱為**日本的貓王**。之後以出演[新劇與](../Page/新劇.md "wikilink")[音樂劇為大宗](../Page/音樂劇.md "wikilink")，以演員工作為主要活動。

1972年，以聲優身份為[龍之子製作公司製作的](../Page/龍之子製作公司.md "wikilink")『[科學小飛俠](../Page/科學小飛俠.md "wikilink")』的「大明」一角配音，翌年擔任『[新造人間凱翔](../Page/新造人間凱翔.md "wikilink")』的主題曲演唱，從此成為動畫歌手，演唱大量動畫與特攝電視劇主題曲。1974年時，因為演唱電視動畫「[宇宙戰艦大和號](../Page/宇宙戰艦大和號.md "wikilink")」的主題曲而風靡一時，創造百萬銷售量的紀錄。

之後也以演員、歌手、旁白等身分活躍，後來又專門在西洋電影裡為[席維斯·史特龍與](../Page/席維斯·史特龍.md "wikilink")[克里斯多夫·李維等人飾演的角色配音](../Page/克里斯多夫·李維.md "wikilink")。

現任妻子為聲優[上田美由紀](../Page/上田美由紀.md "wikilink")，兩人於1981年各自帶著自己的孩子再婚。後來他們將這段辛苦的故事於1986年著成「攜子再婚的單程票」（與妻子共著）出版。

興趣是音響、家庭劇院等物，他曾經多次登上相關的專業雜誌。

以往他使用漢字本名“佐佐木功”為擔任演員工作時的名字，但在聲優與演唱的工作時則使用平假名“”為藝名，自2004年起他統一以“”為藝名。之後他曾經在一個歌唱節目中表示，這是為了懷念自己曾經生逢Rock-a-billy大流行的年代，是那個美好時光的見證人。

## 演出作品

### 電影

  - 『[白晝的陷阱](../Page/白晝的陷阱.md "wikilink")』
  - 『[假面騎士ZO](../Page/假面騎士ZO.md "wikilink")』
  - 『[太陽的墓場](../Page/太陽的墓場.md "wikilink")』
  - 『[特搜戰隊刑事連者 THE MOVIE](../Page/特搜戰隊刑事連者_#劇場版.md "wikilink")』（只有聲音登場）
  - 『[OOO·电王·All Riders Let's Go Kamen
    Riders](../Page/OOO·电王·All_Riders_Let's_Go_Kamen_Riders.md "wikilink")』（飾演中年的直樹，在電影末尾登場）

### 電視劇

  - 『[八代將軍吉宗](../Page/八代將軍吉宗.md "wikilink")』（1995年[NHK大河劇](../Page/NHK大河劇.md "wikilink")　[神尾春央](../Page/神尾春央.md "wikilink")）
  - 『[德川慶喜](../Page/德川慶喜_\(大河劇\).md "wikilink")』（1998年[NHK大河劇](../Page/NHK大河劇.md "wikilink")　[脇坂中務大輔安宅](../Page/脇坂安宅.md "wikilink")）
  - 『[葵德川三代](../Page/葵德川三代.md "wikilink")』（2000年[NHK大河劇](../Page/NHK大河劇.md "wikilink")　[細川忠興](../Page/細川忠興.md "wikilink")）
  - 『[新選組！](../Page/新選組！.md "wikilink")』（2004年[NHK大河劇](../Page/NHK大河劇.md "wikilink")　[內山彥次郎](../Page/內山彥次郎.md "wikilink")）
  - 『[天地人](../Page/天地人.md "wikilink")』（2009年[NHK大河劇](../Page/NHK大河劇.md "wikilink")　[大道寺政繁](../Page/大道寺政繁.md "wikilink")）
  - 『[警衛人](../Page/警衛人.md "wikilink")』第48話「結婚的秘密」、第66話「直達沖繩」（1966年、[大映電視室](../Page/大映電視室.md "wikilink")、[TBS](../Page/東京放送.md "wikilink")）
  - 『[奔紅](../Page/奔紅.md "wikilink")』
  - 『[怪奇大作戰](../Page/怪奇大作戰.md "wikilink")』第14話「晚安」
  - 『[恐怖劇場　失調](../Page/恐怖劇場_失調.md "wikilink")』第13話「蜘蛛之女」
  - 『[晚會的盡頭](../Page/晚會的盡頭.md "wikilink")』（[榎本武揚](../Page/榎本武揚.md "wikilink")）
  - 『[巨獸特搜傑斯比翁](../Page/巨獸特搜傑斯比翁.md "wikilink")』
  - 『[妖術武藝帳](../Page/妖術武藝帳.md "wikilink")』
  - 『[古畑任三郎](../Page/古畑任三郎.md "wikilink")』
  - [THE FISHING](../Page/THE_FISHING.md "wikilink")（旁白、有時本人也會出演）
  - [大家都是馬蓋先](../Page/大家都是馬蓋先.md "wikilink")
  - [午後は○○おもいッきりテレビ](../Page/午後は○○おもいッきりテレビ.md "wikilink")

### 動畫配音

  - 『[科學小飛俠](../Page/科學小飛俠.md "wikilink")』系列（大明）

<!-- end list -->

  -
    ※第二部起也演唱主題曲。第一部使用漢字名字"佐佐木功"，第二部與F使用平假名"ささきいさお"。

<!-- end list -->

  - 『[再見了宇宙戰艦大和號](../Page/再見了宇宙戰艦大和號.md "wikilink")』『[宇宙戰艦大和號2](../Page/宇宙戰艦大和號2.md "wikilink")』（齊藤始）
  - 『[宇宙戰艦大和號 完結編](../Page/宇宙戰艦大和號_完結編.md "wikilink")』（島大介）
  - 『[愛的騎士](../Page/愛的騎士.md "wikilink")』（加藤剛）
  - 『[三國演義](../Page/三國演義_\(動畫\).md "wikilink")』（[呂布](../Page/呂布.md "wikilink")）

### 電影與影集配音

  - 『[洛基](../Page/洛基_\(電影\).md "wikilink")』（洛基・巴波亞）'''
  - 『[-{zh-hans:兰博;zh-hant:藍波}-](../Page/第一滴血.md "wikilink")』（藍波）
  - 『[大開殺戒](../Page/大開殺戒.md "wikilink")』（傑克・卡特）
  - 『[超人](../Page/超人.md "wikilink")』（超人／克拉克・肯特）
  - 『[霹靂遊俠](../Page/霹靂遊俠.md "wikilink")』（李麥克）
  - 『[宇宙戰艦大和號](../Page/SPACE_BATTLESHIP_大和號.md "wikilink")』（解說）
  - 『[霹靂遊俠NEXT(2008)](../Page/霹靂遊俠.md "wikilink")』（李麥克）（只在序章・後篇登場）

## 代表曲

  - 『風の会話』（[三國演義主題曲](../Page/三國演義_\(動畫\).md "wikilink")）
  - 『G.I.Blues』
  - 『Rock・Fly・Baby』
  - 『宇宙戰艦大和號』（[宇宙戰艦大和號](../Page/宇宙戰艦大和號.md "wikilink")）
  - 『由大和放入愛』（[宇宙戰艦大和號](../Page/宇宙戰艦大和號.md "wikilink")。[澤田研二於](../Page/澤田研二.md "wikilink")1978年為動畫電影《さらば宇宙戦艦ヤマト
    愛の戦士たち》所唱的主題曲，由[大野克夫作曲](../Page/大野克夫.md "wikilink")，POLYDOR發行）
  - 『行け行け飛雄馬』（[新巨人之星](../Page/巨人之星.md "wikilink")）
  - 『ゲッターロボ\!』（[蓋特機器人](../Page/蓋特機器人.md "wikilink")）
  - 『たたかえ\! キャシャーン』（[新造人間凱翔](../Page/新造人間凱翔.md "wikilink")）
  - 『秘密戰隊五連者』（[秘密戰隊五連者](../Page/秘密戰隊五連者.md "wikilink")）
  - 『宇宙鐵人』（[宇宙鐵人](../Page/宇宙鐵人.md "wikilink")）
  - 『とべ\! グレンダイザー』（[UFOロボ グレンダイザー](../Page/金剛戰神.md "wikilink")）
  - 『大空魔竜ガイキング』（[大空魔竜ガイキング](../Page/大空魔竜ガイキング.md "wikilink")）
  - 『斗え\!\!超神ビビューン』（[超神ビビューンOP](../Page/超神ビビューン.md "wikilink")）
  - 『すきだッ ダンガードA』（[惑星ロボ ダンガードA](../Page/霹靂日光號.md "wikilink")）
  - 『われらガッチャマン』（[科學小飛俠Ⅱ](../Page/科學小飛俠.md "wikilink")）
  - 『ガッチャマンファイター』（[科學小飛俠Ｆ（ファイター）](../Page/科學小飛俠.md "wikilink")）
  - 『戦え\!レッドタイガー』（[UFO大戦争戦え\!レッドタイガーOP](../Page/UFO大戦争戦え!レッドタイガー.md "wikilink")）
  - 『立て\! 闘将ダイモス』（[闘将ダイモス](../Page/闘将ダイモス.md "wikilink")）
  - 『銀河鐵道999』（[銀河鐵道999](../Page/銀河鐵道999.md "wikilink")）
  - 『加油\! 宇宙的戰士』（[宇宙大帝ゴッドシグマ](../Page/神勇戰士.md "wikilink")）
  - 『進め\!ゴレンジャー』（與[堀江美都子合唱](../Page/堀江美都子.md "wikilink")。[秘密戰隊五連者OP](../Page/秘密戰隊五連者.md "wikilink")）
  - 『秘密戰隊五連者』（[こおろぎ'73の](../Page/こおろぎ'73.md "wikilink")「バンバラバンバンバン」が有名な[秘密戰隊五連者初期ED](../Page/秘密戰隊五連者.md "wikilink")）
  - 『見よ\!ゴレンジャー』（[秘密戰隊五連者後期ED](../Page/秘密戰隊五連者.md "wikilink")）
  - 『ジャッカー電撃隊』（[ジャッカー電撃隊OP](../Page/ジャッカー電撃隊.md "wikilink")）
  - 『いつか、花は咲くだろう』（[ジャッカー電撃隊ED](../Page/ジャッカー電撃隊.md "wikilink")）
  - 『ウルトラマンの歌』（[実相寺昭雄監督作品ウルトラマン主題歌](../Page/実相寺昭雄監督作品ウルトラマン.md "wikilink")／with
    [哥倫比亞搖籠會](../Page/音羽搖籠會.md "wikilink")）
  - 『ぼくらのウルトラマン』（[ウルトラ6兄弟VS怪獣軍団主題歌](../Page/ウルトラ6兄弟VS怪獣軍団.md "wikilink")／with
    哥倫比亞搖籠會）
  - 『ザ☆ウルトラマン』（[ザ☆ウルトラマンOP](../Page/ザ☆ウルトラマン.md "wikilink")／with
    哥倫比亞搖籠會）
  - 『耀け！8人ライダー』（[仮面ライダー
    8人ライダーVS銀河王主題歌](../Page/仮面ライダー_8人ライダーVS銀河王.md "wikilink")／with
    [ザ・チャープス](../Page/ザ・チャープス.md "wikilink")）
  - 『君の青春は輝いているか』（[超人機メタルダーOP](../Page/超人機メタルダー.md "wikilink")）
  - 『ウルトラセブンの歌99』／『ウルトラセブンのバラード』（[ウルトラセブン1999最終章6部作主題歌](../Page/ウルトラセブン1999最終章6部作.md "wikilink")）
  - 『ミッドナイトデカレンジャー』（[特捜戦隊デカレンジャーED](../Page/特捜戦隊デカレンジャー.md "wikilink")／with
    森の木児童合唱団)

等多數

### 廣告歌曲

  - 『焙煎大蒜』（[日本食研](../Page/日本食研.md "wikilink")）
  - 『どキレイダーのテーマ』（Colorio、[Epson](../Page/Epson.md "wikilink")）
  - 『アミノサプリ』（[KIRIN Beverage](../Page/KIRIN_Beverage.md "wikilink")）

### 其他

  -
## 著書

  - 攜子再婚的單程票（與妻子共著・講談社）

## 相關網站

  - [佐佐木功的官方網站](http://www5c.biglobe.ne.jp/~isao/)

[Category:日本男性配音員](../Category/日本男性配音員.md "wikilink")
[Category:日本男演員](../Category/日本男演員.md "wikilink")
[Category:日本男歌手](../Category/日本男歌手.md "wikilink")
[Category:動畫歌手](../Category/動畫歌手.md "wikilink")
[Category:千葉縣出身人物](../Category/千葉縣出身人物.md "wikilink")