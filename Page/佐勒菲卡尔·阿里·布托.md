**佐勒菲卡尔·阿里·布托**（[烏爾都語](../Page/烏爾都語.md "wikilink")：**ذوالفقار علی
بھٹو**，Zulfikar Ali
Bhutto，），[巴基斯坦](../Page/巴基斯坦.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")，巴基斯坦政治家，曾任总理及总统。[巴基斯坦人民黨的創黨領袖](../Page/巴基斯坦人民黨.md "wikilink")。

## 生平

佐勒菲卡尔·阿里·布托是[信德族](../Page/信德族.md "wikilink")，曾在美国加利福尼亚大学学习法律，后在英国开律师业和执教。1953年回巴基斯坦，在卡拉奇开律师业。

## 政治生涯

1957年出任驻联合国大使。1958年阿尤布·汗夺取政权后，任贸易部长。1963年任外交部长，他极力使巴基斯坦摆脱对西方强国的依赖，进一步密切与[中国的关系](../Page/中国.md "wikilink")。1965年辞职，建立了巴基斯坦人民党。后来，[阿尤布·汗的政府被](../Page/阿尤布·汗.md "wikilink")[叶海亚·汗将军推翻](../Page/叶海亚·汗.md "wikilink")。东巴基斯坦独立成为孟加拉国以后，1971年12月20日叶海亚·汗又把政权移交给布托。

布托出任总统后，他把关键性的工业收归国有，并且向土地占有者征税。1973年改任总理兼外交、国防和内政部长。

1976年5月27日，阿里·布托出访中国，是中共中央主席[毛泽东最后一次接见的外宾](../Page/毛泽东.md "wikilink")。

1977年，他被軍人[穆罕默德·齐亚·哈克推翻並囚禁](../Page/穆罕默德·齐亚·哈克.md "wikilink")。布托以1974年下令杀害政敌的罪名受审，1979年4月4日因涉嫌谋杀[Ahmed
Kasuri被](../Page/:en:Sahibzada_Ahmad_Raza_Khan_Kasuri.md "wikilink")[穆罕默德·齐亚·哈克处以](../Page/穆罕默德·齐亚·哈克.md "wikilink")[绞刑](../Page/绞刑.md "wikilink")。

他的女兒[贝娜齐尔·布托於](../Page/贝娜齐尔·布托.md "wikilink")1988－1990年，1993－1996年曾經擔任巴基斯坦總理，是伊斯蘭國家首位女總理。2007年12月27日，她在參加競選集會時遇刺身亡。

[Category:巴基斯坦總統](../Category/巴基斯坦總統.md "wikilink")
[Category:巴基斯坦總理](../Category/巴基斯坦總理.md "wikilink")
[Category:冷戰時期領袖](../Category/冷戰時期領袖.md "wikilink")
[Category:巴基斯坦國民議會議長](../Category/巴基斯坦國民議會議長.md "wikilink")
[Category:巴基斯坦外交部長](../Category/巴基斯坦外交部長.md "wikilink")
[Category:巴基斯坦內政部長](../Category/巴基斯坦內政部長.md "wikilink")
[Category:巴基斯坦人民黨黨員](../Category/巴基斯坦人民黨黨員.md "wikilink")
[Category:被處決的總統](../Category/被處決的總統.md "wikilink")
[Category:被巴基斯坦處決者](../Category/被巴基斯坦處決者.md "wikilink")
[Category:被處決的巴基斯坦人](../Category/被處決的巴基斯坦人.md "wikilink")
[Category:巴基斯坦罪犯](../Category/巴基斯坦罪犯.md "wikilink")
[Category:巴基斯坦穆斯林](../Category/巴基斯坦穆斯林.md "wikilink")
[Category:牛津大学基督堂学院校友](../Category/牛津大学基督堂学院校友.md "wikilink")
[Category:加州大學柏克萊分校校友](../Category/加州大學柏克萊分校校友.md "wikilink")
[Z](../Category/布托家族.md "wikilink")
[Category:信德族](../Category/信德族.md "wikilink")