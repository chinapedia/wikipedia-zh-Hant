[Satamisaki_Park_View_Kagoshima001.JPG](https://zh.wikipedia.org/wiki/File:Satamisaki_Park_View_Kagoshima001.JPG "fig:Satamisaki_Park_View_Kagoshima001.JPG")

**佐多岬**（**さたみさき**）是位于[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")[肝属郡](../Page/肝属郡.md "wikilink")[南大隅町佐多馬籠](../Page/南大隅町.md "wikilink")，面向[大隅海峡的一個](../Page/大隅海峡.md "wikilink")[海岬](../Page/海岬.md "wikilink")。是[九州本島的最南端](../Page/九州島.md "wikilink")。被劃入[霧島屋久國立公園之中](../Page/霧島屋久國立公園.md "wikilink")。

<File:Satamisaki.jpg>| <File:Satamisaki> observatory.JPG|
<File:Satamisaki> light house.JPG| <File:佐多岬IMGP0402.JPG>|

## 外部連結

  - [佐多岬（南大隅町首頁）](https://web.archive.org/web/20110614022218/http://www.town.minamiosumi.lg.jp/minami04/minami06.asp)
  - [佐多岬灯台過去12小時的天氣數據（鹿兒島海上保安部）](https://web.archive.org/web/20080407171705/http://www.kaiho.mlit.go.jp/10kanku/kagoshima/mics/page/anzenni-kansuru-jouhou/kishou-kaishou/satamisaki.htm)
  - [佐多岬與燈塔](https://web.archive.org/web/20060904101927/http://gauss0jp.hp.infoseek.co.jp/satamisa.htm)　画像及游記。
  - [大隅半島佐多岬](https://web.archive.org/web/20070929050416/http://www.tanoshimimura.com/photo/kagoshima/sata.htm)　照片集

[category:鹿兒島縣地理](../Page/category:鹿兒島縣地理.md "wikilink")

[Category:南大隅町](../Category/南大隅町.md "wikilink")
[Category:日本海岬](../Category/日本海岬.md "wikilink")
[Category:菲律賓海](../Category/菲律賓海.md "wikilink")
[Category:東海](../Category/東海.md "wikilink")