**佐川町**（）是位於[高知縣中部柳瀨川沿岸的一個城鎮](../Page/高知縣.md "wikilink")。

## 歷史

轄內的不動岩屋洞窟遺跡顯示最早在[石器時代已有人居住](../Page/石器時代.md "wikilink")。

在[江戶時代為](../Page/江戶時代.md "wikilink")[土佐藩](../Page/土佐藩.md "wikilink")[筆頭家老](../Page/筆頭家老.md "wikilink")[深尾氏的](../Page/深尾氏.md "wikilink")[城下町](../Page/城下町.md "wikilink")。

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：[高岡郡加茂村](../Page/高岡郡.md "wikilink")、**佐川村**、斗賀野村、尾川村、黑岩村。
  - 1900年1月10日：佐川村改制為**佐川町**。
  - 1954年3月20日：佐川町、斗賀野村、尾川村、黑岩村[合併為新設置的](../Page/市町村合併.md "wikilink")**佐川町**。
  - 1954年4月15日：佐川町的部分地區被併入[越知町](../Page/越知町.md "wikilink")。
  - 1954年10月10日：佐川町的部分地區被併入越知町。
  - 1955年2月10日：
      - [日高村的部份地區被併入佐川町](../Page/日高村.md "wikilink")。
      - 加茂村被廢除，部份轄區被併入佐川町。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>加茂村</p></td>
<td><p>1954年10月15日<br />
合併為日高村</p></td>
<td><p>日高村</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>加茂村</p></td>
<td><p>1955年2月10日<br />
併入日高村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1955年2月10日<br />
併入佐川町</p></td>
<td><p>佐川町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>佐川村</p></td>
<td><p>1900年1月10日<br />
佐川町</p></td>
<td><p>1954年3月20日<br />
佐川町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>斗賀野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>尾川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>黑岩村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 行政

### 歷任町長

1.  渡邊勉（1977年 \~ 1993年）
2.  和田啓作（1993年 \~ 1997年）
3.  中山博司（1997年 \~ 2005年）
4.  榎並谷哲夫（2005年 \~ 現任）

## 教育

  - 高等學校

<!-- end list -->

  - [高知縣立佐川高等學校](../Page/高知縣立佐川高等學校.md "wikilink")

## 交通

### 鐵路

  - [四國旅客鐵道](../Page/四國旅客鐵道.md "wikilink")
      - [土讚線](../Page/土讚線.md "wikilink")：[土佐加茂車站](../Page/土佐加茂車站.md "wikilink")
        - [西佐川車站](../Page/西佐川車站.md "wikilink") -
        [佐川車站](../Page/佐川車站_\(日本\).md "wikilink") -
        [襟野野車站](../Page/襟野野車站.md "wikilink") -
        [斗賀野車站](../Page/斗賀野車站.md "wikilink")

|                                                                                                                          |                                                                                                                         |
| ------------------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------- |
| [Tosa-Kamo_station_01.jpg](https://zh.wikipedia.org/wiki/File:Tosa-Kamo_station_01.jpg "fig:Tosa-Kamo_station_01.jpg") | [Nishisakawa_station1.jpg](https://zh.wikipedia.org/wiki/File:Nishisakawa_station1.jpg "fig:Nishisakawa_station1.jpg") |
| [Erinono_station1.jpg](https://zh.wikipedia.org/wiki/File:Erinono_station1.jpg "fig:Erinono_station1.jpg")              | [Togano_station_02.jpg](https://zh.wikipedia.org/wiki/File:Togano_station_02.jpg "fig:Togano_station_02.jpg")         |

## 觀光資源

  - 青山文庫
  - 青源寺
  - 乘台寺
  - 不動岩屋洞窟遺跡
  - 司牡丹
  - 竹村家住宅：國家重要文化財
  - 佐川地質館
  - 長谷溪谷

## 姊妹、友好城市

### 日本

  - [北見市](../Page/北見市.md "wikilink")（[北海道](../Page/北海道.md "wikilink")）：過去原與[常呂郡](../Page/常呂郡.md "wikilink")[常呂町締結](../Page/常呂町.md "wikilink")，後合併為北見市。
  - [南部町](../Page/南部町_\(鳥取縣\).md "wikilink")（[鳥取県](../Page/鳥取県.md "wikilink")[西伯郡](../Page/西伯郡.md "wikilink")）過去原與[西伯町締結](../Page/西伯町.md "wikilink")，後合併為南部町。

## 本地出深之名人

  - [田中光顯](../Page/田中光顯.md "wikilink")：[江戶時代](../Page/江戶時代.md "wikilink")[土佐藩](../Page/土佐藩.md "wikilink")[家老](../Page/家老.md "wikilink")[深尾氏](../Page/深尾氏.md "wikilink")[家臣](../Page/家臣.md "wikilink")，明治時代成為中央政府官員，曾擔任[内閣書記官長](../Page/内閣書記官長.md "wikilink")、[元老院議官](../Page/元老院.md "wikilink")、會計檢查院長、[警視總監](../Page/警視總監.md "wikilink")、[貴族院議員](../Page/貴族院.md "wikilink")、[宮内大臣](../Page/宮内大臣.md "wikilink")。
  - [尾崎清光](../Page/尾崎清光.md "wikilink")：政治運動人士
  - [黑鐵弘](../Page/黑鐵弘.md "wikilink")：[漫畫家](../Page/漫畫家.md "wikilink")
  - [坂東眞砂子](../Page/坂東眞砂子.md "wikilink")：[作家](../Page/作家.md "wikilink")
  - [廣井勇](../Page/廣井勇.md "wikilink")：日本[港灣工程之父](../Page/港灣工程.md "wikilink")
  - [牧野富太郎](../Page/牧野富太郎.md "wikilink")：[植物學家](../Page/植物學.md "wikilink")
  - [森下雨村](../Page/森下雨村.md "wikilink")：作家
  - [森下一仁](../Page/森下一仁.md "wikilink")：[架空小說作家](../Page/架空小說.md "wikilink")
  - [渡邊智男](../Page/渡邊智男.md "wikilink")：前[職業棒球](../Page/職業棒球.md "wikilink")[投手](../Page/投手.md "wikilink")
  - [吉村光示](../Page/吉村光示.md "wikilink")：前[職業足球選手](../Page/職業足球.md "wikilink")
  - [山口智](../Page/山口智.md "wikilink")：職業足球選手
  - [小野大輔](../Page/小野大輔.md "wikilink")：[配音員](../Page/配音員.md "wikilink")、[歌手](../Page/歌手.md "wikilink")
  - [楠木繁夫](../Page/楠木繁夫.md "wikilink")：[歌手](../Page/歌手.md "wikilink")

## 外部連結

  - [佐川町觀光情報](https://web.archive.org/web/20120204202609/http://www.town.sakawa.kochi.jp/tourism/)