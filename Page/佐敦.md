[Jordan_View_from_ICC_201105.jpg](https://zh.wikipedia.org/wiki/File:Jordan_View_from_ICC_201105.jpg "fig:Jordan_View_from_ICC_201105.jpg")
[Jordan_Overview_201807.jpg](https://zh.wikipedia.org/wiki/File:Jordan_Overview_201807.jpg "fig:Jordan_Overview_201807.jpg")和佐敦（2018年）\]\]
[Jordan_View_201605.jpg](https://zh.wikipedia.org/wiki/File:Jordan_View_201605.jpg "fig:Jordan_View_201605.jpg")
[Tsim_Sha_Tsui_and_Kwun_Chung_1845.png](https://zh.wikipedia.org/wiki/File:Tsim_Sha_Tsui_and_Kwun_Chung_1845.png "fig:Tsim_Sha_Tsui_and_Kwun_Chung_1845.png")
[HK_DiocesanGirlsSchool.jpg](https://zh.wikipedia.org/wiki/File:HK_DiocesanGirlsSchool.jpg "fig:HK_DiocesanGirlsSchool.jpg")(2007年)\]\]
[Austin_Road_201709.jpg](https://zh.wikipedia.org/wiki/File:Austin_Road_201709.jpg "fig:Austin_Road_201709.jpg")\]\]
[HK_TST_Austin_Road_KBGC_Club.JPG](https://zh.wikipedia.org/wiki/File:HK_TST_Austin_Road_KBGC_Club.JPG "fig:HK_TST_Austin_Road_KBGC_Club.JPG")\]\]
[HK_Jordan_佐敦_廟街_Temple_Street_night_Sauna_Spa_shop_signs_Apr-2013.JPG](https://zh.wikipedia.org/wiki/File:HK_Jordan_佐敦_廟街_Temple_Street_night_Sauna_Spa_shop_signs_Apr-2013.JPG "fig:HK_Jordan_佐敦_廟街_Temple_Street_night_Sauna_Spa_shop_signs_Apr-2013.JPG")的[芬蘭浴](../Page/芬蘭浴.md "wikilink")\]\]
[Union_Square_Overview_201008.jpg](https://zh.wikipedia.org/wiki/File:Union_Square_Overview_201008.jpg "fig:Union_Square_Overview_201008.jpg")\]\]
[West_Kowloon_Waterfront_Promenade.jpg](https://zh.wikipedia.org/wiki/File:West_Kowloon_Waterfront_Promenade.jpg "fig:West_Kowloon_Waterfront_Promenade.jpg")\]\]
[Western_Harbour_Tunnel.JPG](https://zh.wikipedia.org/wiki/File:Western_Harbour_Tunnel.JPG "fig:Western_Harbour_Tunnel.JPG")\]\]
**佐敦**（），原稱**官涌**（），位於[香港](../Page/香港.md "wikilink")[九龍區的核心地帶](../Page/九龍區.md "wikilink")，座落[油尖旺區的西南部](../Page/油尖旺區.md "wikilink")。區內樓宇林立，當中以[商場和](../Page/商場.md "wikilink")[商業大廈為主](../Page/商業大廈.md "wikilink")。區內交通十分方便，除了[彌敦道繁忙的路面交通外](../Page/彌敦道.md "wikilink")，[港鐵](../Page/港鐵.md "wikilink")[荃灣綫](../Page/荃灣綫.md "wikilink")[佐敦站位於佐敦的中心地區](../Page/佐敦站.md "wikilink")、彌敦道之下。佐敦與[西九龍填海區為鄰](../Page/西九龍.md "wikilink")，[東涌綫和](../Page/東涌綫.md "wikilink")[機場快綫](../Page/機場快綫.md "wikilink")[九龍站及](../Page/九龍站_\(港鐵\).md "wikilink")[廣深港高鐵](../Page/廣深港高鐵.md "wikilink")[香港西九龍站都位於填海區](../Page/香港西九龍站.md "wikilink")。因為[佐敦道和佐敦站都位於此地區中心位置](../Page/佐敦道.md "wikilink")，所以香港市民約定俗成地稱此地為「佐敦」，同時將地區西半的「**官涌**」範圍也一併稱為佐敦。惟「佐敦」並非正式地名，香港官方地圖抑或私營出版者所印製的地圖一般都沒有佐敦或官涌之地名。**佐敦**的情況就如香港其他地區一樣，都因為鐵路站的名稱影響周邊的地名，如[太子](../Page/太子_\(香港\).md "wikilink")；而「官涌」一名則漸被淡忘。

## 地理

### 大約界線

佐敦位於[尖沙咀以北](../Page/尖沙咀.md "wikilink")、[油麻地以南](../Page/油麻地.md "wikilink")，[佐敦道自西向東穿越此地](../Page/佐敦道.md "wikilink")。「佐敦」一地沒有明確界線，但一般人認為佐敦的地理範圍是[連翔道以東](../Page/連翔道.md "wikilink")、[西貢街以南](../Page/西貢街.md "wikilink")、[漆咸道以西及](../Page/漆咸道.md "wikilink")[柯士甸道以北](../Page/柯士甸道.md "wikilink")。區域的中心為[佐敦道](../Page/佐敦道.md "wikilink")，而且被[港鐵](../Page/港鐵.md "wikilink")[佐敦站貫穿](../Page/佐敦站.md "wikilink")。

### 名稱爭議

部分人視佐敦為一個獨立的地區，位於[油麻地及](../Page/油麻地.md "wikilink")[尖沙嘴之間](../Page/尖沙嘴.md "wikilink")，不屬於兩者。相反，部分人視佐敦為油麻地的南部（[甘肅街及佐敦道之南](../Page/甘肅街.md "wikilink")）或尖沙咀的北部（尖沙咀警署鄰近佐敦）。而事實上佐敦西面一帶原稱為官涌，因被[港鐵](../Page/港鐵.md "wikilink")[佐敦站名稱所影響而被統稱為佐敦](../Page/佐敦站.md "wikilink")。

## 歷史

### 西面部分

佐敦西面部分過去名為**官涌**，此地名於[1819年編印的](../Page/1819年.md "wikilink")《[新安縣志](../Page/新安縣志.md "wikilink")》已有記載，當時官涌村屬官富司管轄\[1\]\[2\]，旁之[官涌山為戰略要地](../Page/官涌山.md "wikilink")，[林則徐](../Page/林則徐.md "wikilink")[1839年在此展開](../Page/1839年.md "wikilink")[官涌之戰擊退英軍](../Page/官涌之戰.md "wikilink")，[1840年在此設臨衝炮台](../Page/1840年.md "wikilink")作防範\[3\]，後被英軍佔領和拆卸。現時[官涌街和](../Page/官涌街.md "wikilink")[炮台街就是昔日的遺址](../Page/炮台街.md "wikilink")。官涌山後因1909年興建[油麻地避風塘而被夷平](../Page/油麻地避風塘.md "wikilink")。

### 地鐵通車

1979年，[香港地鐵](../Page/香港地鐵.md "wikilink")「[修正早期系統](../Page/修正早期系統.md "wikilink")」通車，並於此設有一站。因該站主要位於[佐敦道](../Page/佐敦道.md "wikilink")，地鐵當局用街道名「佐敦」為車站，而沒有用西面「官涌」的舊地名。時至今日，**佐敦**已約定俗成成為該區通用名稱，並廣為香港人所熟悉。區內只剩下[官涌街](../Page/官涌街.md "wikilink")、[官涌市政大廈](../Page/官涌市政大廈.md "wikilink")（[官涌街市和](../Page/官涌街市.md "wikilink")[官涌體育館](../Page/官涌體育館.md "wikilink")）及「官涌大樓」仍沿用「官涌」之舊名，作為區內的標記。

## 著名地點

  - [拔萃女書院](../Page/拔萃女書院.md "wikilink")
  - [槍會山軍營](../Page/槍會山軍營.md "wikilink")
  - [文華新村](../Page/文華新村.md "wikilink")
  - [九龍佐治五世紀念公園](../Page/九龍佐治五世紀念公園.md "wikilink")
  - [九龍佑寧堂](../Page/九龍佑寧堂.md "wikilink")
  - [佐敦薈](../Page/佐敦薈.md "wikilink")，前址為因發生五級大火而拆卸的[嘉利大廈](../Page/嘉利大廈.md "wikilink")
  - [德信學校](../Page/德信學校.md "wikilink")
  - [裕華國貨](../Page/裕華國貨.md "wikilink")
  - [澳洲牛奶公司](../Page/澳洲牛奶公司.md "wikilink")
  - [福樂大廈](../Page/福樂大廈.md "wikilink")
  - [佐敦道官立小學](../Page/佐敦道官立小學.md "wikilink")

## 交通

### 主要交通幹道

  - [佐敦道](../Page/佐敦道.md "wikilink")
  - [彌敦道](../Page/彌敦道.md "wikilink")
  - [廣東道](../Page/廣東道.md "wikilink")
  - [加士居道](../Page/加士居道.md "wikilink")
  - [漆咸道](../Page/漆咸道.md "wikilink")
  - [柯士甸道](../Page/柯士甸道.md "wikilink")

### 公共交通

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")：[佐敦站](../Page/佐敦站.md "wikilink")
  - <font color="{{西鐵綫色彩}}">█</font>[西鐵綫](../Page/西鐵綫.md "wikilink")：[柯士甸站A](../Page/柯士甸站.md "wikilink")、F出口（需步行約5-10分鐘）
  - <font color="{{東涌綫色彩}}">█</font>[東涌綫](../Page/東涌綫.md "wikilink")、<font color={{機場快綫色彩}}>█</font>[機場快綫](../Page/機場快綫.md "wikilink")：[九龍站C](../Page/九龍站_\(港鐵\).md "wikilink")1出口
  - <font color="{{東鐵綫色彩}}">█</font>[東鐵綫](../Page/東鐵綫.md "wikilink")、<font color={{西鐵綫色彩}}>█</font>[西鐵綫](../Page/西鐵綫.md "wikilink")：[紅磡站A](../Page/紅磡站.md "wikilink")1、D1出口
  - <font color="{{高速鐵路色彩}}">█</font>[廣深港高速鐵路](../Page/廣深港高速鐵路.md "wikilink")：[香港西九龍站](../Page/香港西九龍站.md "wikilink")

<!-- end list -->

  - [巴士](../Page/巴士.md "wikilink")

<!-- end list -->

  - [九巴](../Page/九龍巴士.md "wikilink")
  - 過海隧道巴士
  - [跨境巴士](../Page/跨境巴士.md "wikilink")
      - 佐敦設有多個跨境巴士站，提供多條跨境巴士路線前往內地，包括前往[皇崗口岸的油尖](../Page/皇崗口岸.md "wikilink")24小時跨境快線

<!-- end list -->

  - [小巴](../Page/小巴.md "wikilink")

<!-- end list -->

  - 綠色[專線小巴](../Page/專線小巴.md "wikilink")
  - 紅色[公共小巴前往](../Page/公共小巴.md "wikilink")[荃灣](../Page/荃灣.md "wikilink")、[屯門](../Page/屯門.md "wikilink")、[元朗](../Page/元朗.md "wikilink")

## 區議會議席分佈

## 註釋

## 參考資料

<references />

  - [油尖旺區簡史](https://web.archive.org/web/20070928042442/http://qcrc.qef.org.hk/webpage/19984196/1/ytmhistory.html)

## 參看

  - [官涌山](../Page/官涌山.md "wikilink")
  - [渡船角](../Page/渡船角.md "wikilink")
  - [佐敦道](../Page/佐敦道.md "wikilink")
  - [油麻地](../Page/油麻地.md "wikilink")
  - [尖沙咀](../Page/尖沙咀.md "wikilink")
  - [西九龍](../Page/西九龍.md "wikilink")

{{-}}

{{-}}

[Category:官涌](../Category/官涌.md "wikilink")
[Category:佐敦](../Category/佐敦.md "wikilink")
[Category:油尖旺區](../Category/油尖旺區.md "wikilink")
[Category:香港通俗分區](../Category/香港通俗分區.md "wikilink")

1.  《九龍街道命名考源》梁濤 著，第25頁，市政局出版，1993年
2.  《香港歷史文化小百科16－趣談九龍街道》，爾東著，第148-149頁，明報出版社，2004年11月，ISBN 962-8871-46-3
3.