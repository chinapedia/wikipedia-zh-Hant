**佐敷站**（）位於[熊本縣](../Page/熊本縣.md "wikilink")[葦北郡](../Page/葦北郡.md "wikilink")[蘆北町](../Page/蘆北町.md "wikilink")，屬[肥薩橙鐵道線的](../Page/肥薩橙鐵道線.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。

此乃蘆北町的中心車站，鹿兒島本線時代是特急列車停車站。

## 車站構造

島式1面2線和相對式1面1線月台的地上站。車站是有人站。

### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>方向</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>肥薩橙鐵道線</p></td>
<td><p>上行</p></td>
<td><p><a href="../Page/八代站.md" title="wikilink">八代方向</a>　</p></td>
</tr>
<tr class="even">
<td><p>下行</p></td>
<td><p><a href="../Page/水俁站.md" title="wikilink">水俁方向</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>|2</p></td>
<td><p>肥薩橙鐵道線</p></td>
<td><p>下行</p></td>
<td><p>水俁方向</p></td>
</tr>
</tbody>
</table>

## 車站附近

  - 芦北町役場
  - 芦北警署
  - 芦北町立佐敷中學校
  - 芦北養護學校

## 歷史

  - 1925年4月15日 - 日本國有鐵道（國鐵）肥薩線的站啟用。
  - 1927年10月17日 - 編入鹿兒島本線，成為鹿兒島本線的站。
  - 1987年4月1日 - 國鐵分割民營化，成為[九州旅客鐵道的站](../Page/九州旅客鐵道.md "wikilink")。
  - 2004年3月13日 - 第三部門化、成為肥薩橙鐵道的站。

## 相鄰車站

  - 肥薩橙鐵道

    肥薩橙鐵道線

      -

        快速「[超級橙](../Page/肥薩橙鐵道線#快速「スーパーおれんじ」・「オーシャンライナーさつま」.md "wikilink")」（只限週末假日運行）

          -

            －**佐敷**－[水俁](../Page/水俁站.md "wikilink")（游泳季節只限1號、4號
            －**佐敷**－[水俁](../Page/水俁站.md "wikilink")）

        「\[\[肥薩橙鐵道線\#快速「おれんじ食堂」|橙食堂}}」（不定期運行）

          -
            日奈久溫泉－**佐敷**－

        普通

          -

            －**佐敷**－

## 參見

[Category:熊本縣鐵路車站](../Category/熊本縣鐵路車站.md "wikilink")
[Shiki](../Category/日本鐵路車站_Sa.md "wikilink")
[Category:肥薩橙鐵道車站](../Category/肥薩橙鐵道車站.md "wikilink")
[Category:1925年啟用的鐵路車站](../Category/1925年啟用的鐵路車站.md "wikilink")
[Category:蘆北町](../Category/蘆北町.md "wikilink")
[Category:九州旅客鐵道廢站](../Category/九州旅客鐵道廢站.md "wikilink")