**佐藤健**（，），[日本](../Page/日本.md "wikilink")[男演員](../Page/演員.md "wikilink")，隸屬[Amuse經紀公司](../Page/Amuse.md "wikilink")。2007年在[特摄剧](../Page/特摄剧.md "wikilink")《[假面騎士电王](../Page/假面騎士电王.md "wikilink")》中主演，以一人分饰八角的演技獲得注目。2012年起，主演話題漫畫改編的真人版電影《[神劍闖江湖](../Page/浪客劍心_\(電影\).md "wikilink")》大获成功，在日本本土电影票房突破125億，並於2014年8月1日推出其第二篇續作「[京都大火篇](../Page/神劍闖江湖_京都大火篇.md "wikilink")」及9月13日的「[傳說的最後時刻篇](../Page/浪客剑心：传说的最后时刻篇.md "wikilink")」。2015年10月3日《[爆漫王](../Page/爆漫王.md "wikilink")》於日本公映，榮獲日本當週票房冠軍。東京Drama
Award
2015（東京ドラマアウォード2015）以《[天皇的御廚](../Page/天皇的御廚.md "wikilink")》榮獲最佳男主角。

## 簡歷

佐藤健在高中二年級的[黃金週於](../Page/黃金週.md "wikilink")[原宿的](../Page/原宿.md "wikilink")[竹下通被現在事務所的星探挖角](../Page/竹下通.md "wikilink")\[1\]。
2006年，初次在《[變身公主](../Page/變身公主.md "wikilink")》亮相；初次出演，就被選為主角「河野亨」。翌年，被選中當《[假面騎士電王](../Page/假面騎士電王.md "wikilink")》的主角「野上良太郎」。
2009年，以《[我的帥管家](../Page/我的帥管家.md "wikilink")》獲得[第60回日劇學院賞男配角獎](../Page/第60回日劇學院賞.md "wikilink")。2010年，出演NHK大河劇《[龍馬傳](../Page/龍馬傳.md "wikilink")》，並於同年2010年10月，初次主演連續劇《[Q10](../Page/Q10.md "wikilink")》。
2011年，出演電影《[BECK](../Page/BECK.md "wikilink")》主角田中幸雄，並獲得及[日本電影影評人大獎的新人賞](../Page/日本電影影評人大獎.md "wikilink")。
2012年4月，出演舞台劇《羅密歐與茱麗葉》中的羅密歐一角，該劇也是佐藤初次挑戰舞台劇。\<
2012年8月，飾演話題漫畫改編的真人版電影《[神劍闖江湖](../Page/浪客劍心_\(電影\).md "wikilink")》主角緋村劍心，連續兩週日本票房冠軍，吸引237萬人次觀眾，更創下30.1億日幣超高票房。並於2014年公開兩篇續作，3集作品累計票房紀錄高達125億日元。
[2012年度（平成24年）興収１０億円以上番組　（平成25年1月発表）](http://www.eiren.org/toukei/index.html)日本映画製作者連盟
2013年1月
2015年，主演[TBS電視台開台電視劇](../Page/TBS電視台.md "wikilink")《[天皇的御廚](../Page/天皇的御廚.md "wikilink")》，平均收視率達14.9%。

2015東京Drama Award（東京ドラマアウォード2015）
以《[天皇的御廚](../Page/天皇的御廚.md "wikilink")》榮獲最佳男主角。

## 人物

  - 不服輸，尤其是在遊戲上。
  - 拍攝《[神劍闖江湖](../Page/浪客劍心_\(電影\).md "wikilink")》時，所有武打場面全都自己上陣。因為覺得「既然那些特技演員們都做得到，自己和他們也沒什麼不一樣，沒有道理做不到」而努力！
  - 是會讓特技演員汗顏感動的演员。
  - 國中時非常努力唸書，曾經得過全學年第一名，上了高中就覺得不想再這麼認真了，所以幾乎都在睡覺！
  - 高中時，曾經有一年的時間故意裝得很酷，不和同學們打交道。而在某次因前堂課上課睡著，要換教室上課時沒有半個同學來叫醒他，當他醒來時教室只剩自己一人後，覺得還是不要再裝酷好了，而開始和同學們打成一片。
  - 家中只有媽媽、他和一個小4歲的妹妹及2隻貓。興趣是和貓玩耍\[2\]。2隻貓名字分別是「こちろー」及「ぷちろー」。本人曾在官方BLOG介紹他們，並附上圖片<ref>

[佐藤健官方BLOG於2008年10月27日刊登。](https://archive.is/20130430230142/ameblo.jp/takeru-s/entry-10156822892.html)[佐藤健官方BLOG於2008年10月28日刊登。](https://archive.is/20130501172439/ameblo.jp/takeru-s/entry-10157129255.html)</ref>。

  - 擅長玩水平思考遊戲、奧賽羅、[魔術方塊和](../Page/魔術方塊.md "wikilink")[Breaking](../Page/Breaking.md "wikilink")\[3\]。在演出的《[假面騎士電王](../Page/假面騎士電王.md "wikilink")》中，亦有他親自跳舞的場面。
  - 2007年11月1日，因為左胸被診斷有[氣胸](../Page/氣胸.md "wikilink")，休息了約10天\[4\]。現在已經康復。
  - 與[ONE OK
    ROCK主唱](../Page/ONE_OK_ROCK.md "wikilink")[TAKA為朋友](../Page/森內貴寬.md "wikilink")。和[城田優及](../Page/城田優.md "wikilink")[三浦翔平私底下也經常在一起活動](../Page/三浦翔平.md "wikilink")。
  - 讀初中時開始是[深津繪里支持者](../Page/深津繪里.md "wikilink")。
  - 喜歡的藝人有[Mr.Children](../Page/Mr.Children.md "wikilink")\[5\]、[ONE OK
    ROCK與](../Page/ONE_OK_ROCK.md "wikilink")[椎名林檎](../Page/椎名林檎.md "wikilink")\[6\]。
  - 圈内的好朋友有同事务所的[三浦春馬](../Page/三浦春馬.md "wikilink")，[三浦翔平和](../Page/三浦翔平.md "wikilink")[神木隆之介](../Page/神木隆之介.md "wikilink")。
  - 拍攝電影《BECK》期間，在做拍攝開始前的廣播體操時，被導演發現手碰不到肩膀。\[7\]
  - 2013年底電影《她愛上了我的謊》上『新堂本兄弟』\[8\]宣傳，提到手碰不到肩膀，於是堂本剛即興表演了〝佐藤健之舞〞，笑翻大眾。
  - 2016年5月電影《如果這世界貓消失了》5月29日的舞台問候時提到去給整骨師整骨之後居然手能碰到肩膀了，並且立刻表演了，得到粉絲的熱烈掌聲（於2016年6月13日『たけてれ』\[9\]中播放畫面）。

## 演出作品

※主演用 **粗體字體** 顯示

### 電視劇

| 年份                                 | 電視台                                                   | 劇名                                                 | 飾演          |
| ---------------------------------- | ----------------------------------------------------- | -------------------------------------------------- | ----------- |
| 2006                               | [EX](../Page/朝日電視.md "wikilink")                      | [變身公主](../Page/變身公主.md "wikilink")                 | 河野亨         |
| 2007                               | [TX](../Page/東京電視台.md "wikilink")                     | [死神的歌謠](../Page/死神的歌謠.md "wikilink") (第7・8・11話)    | **市原カンタロウ** |
| EX                                 | [假面騎士電王](../Page/假面騎士電王.md "wikilink")                | **野上良太郎**                                          |             |
| 2008                               | [TBS](../Page/TBS電視台.md "wikilink")                   | [不良學園](../Page/不良學園.md "wikilink")(ROOKIE)         | 岡田優也        |
| TBS                                | [不良學園](../Page/不良學園.md "wikilink")(ROOKIE) (特別篇)      | 岡田優也                                               |             |
| TBS                                | [血色星期一](../Page/血色星期一.md "wikilink") (第一季)            | 九條音彌                                               |             |
| 2009                               | [CX](../Page/富士電視台.md "wikilink")                     | [咩妹的完美執事](../Page/咩妹的完美執事.md "wikilink")           | 柴田劍人        |
| TBS                                | [MR.BRAIN](../Page/MR.BRAIN.md "wikilink") (第4・5話)    | 中川優                                                |             |
| [NTV](../Page/日本電視台.md "wikilink") | [MW 惡魔遊戲](../Page/MW_\(漫畫\)#.md "wikilink") (單元劇)     | **森岡隆志**                                           |             |
| CX                                 | [毛骨悚然撞鬼經驗](../Page/毛骨悚然撞鬼經驗.md "wikilink")「顔の道」 (第5話) | **藤沢翔太郎**                                          |             |
| 2010                               | [NHK](../Page/NHK.md "wikilink")                      | [龍馬傳](../Page/龍馬傳.md "wikilink")                   | 岡田以蔵        |
| TBS                                | [血色星期一](../Page/血色星期一.md "wikilink") (第二季)            | 九条音彌                                               |             |
| NTV                                | [Q10](../Page/Q10.md "wikilink")                      | **深井平太**                                           |             |
| 2011                               | TBS                                                   | [冬日櫻花](../Page/冬日櫻花.md "wikilink")                 | 稲葉肇         |
| CX                                 | 最後之絆 沖繩．被撕裂的兄弟 (單元劇)                                  | **東江康治**                                           |             |
| 2013                               | TBS                                                   | [鳶](../Page/鳶_\(小說\)#TBS.E7.89.88.md "wikilink")   | 市川旭         |
| \-                                 | [她愛上了我的謊外傳](../Page/她愛上了我的謊.md "wikilink")            | **小笠原秋**                                           |             |
| 2014                               | CX                                                    | [Bitter Blood](../Page/Bitter_Blood.md "wikilink") | **佐原夏輝**    |
| 2015                               | TBS                                                   | [天皇的御廚](../Page/天皇的御廚.md "wikilink")               | **秋山篤蔵**    |
| 2018                               | NHK                                                   | [半邊藍天](../Page/一半，藍色。.md "wikilink")               | 萩尾律         |
| TBS                                | [繼母與女兒的藍調](../Page/繼母與女兒的藍調.md "wikilink")            | 麥田章                                                |             |

### 電影

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>日期</p></th>
<th><p>出品</p></th>
<th><p>片名</p></th>
<th><p>原片名</p></th>
<th><p>角色</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2007</p></td>
<td><p>8月4日</p></td>
<td><p>東映</p></td>
<td><p>劇場版 <a href="../Page/假面騎士電王_老子，誕生！.md" title="wikilink">假面騎士電王 老子，誕生！</a></p></td>
<td><p>劇場版 仮面ライダー電王 俺、誕生</p></td>
<td><p><strong>野上良太郎</strong></p></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p>4月12日</p></td>
<td><p>東映</p></td>
<td><p>劇場版 <a href="../Page/假面騎士電王&amp;Kiva_Climax刑事.md" title="wikilink">假面騎士電王&amp;Kiva Climax刑事</a></p></td>
<td><p>劇場版 仮面ライダー電王＆キバ　クライマックス刑事（デカ</p></td>
<td><p><strong>野上良太郎</strong></p></td>
</tr>
<tr class="odd">
<td><p>10月4日</p></td>
<td><p>東映</p></td>
<td><p>劇場版 <a href="../Page/再見，假面騎士電王_Final_Countdown.md" title="wikilink">再見，假面騎士電王 Final Countdown</a></p></td>
<td><p>劇場版 さらば仮面ライダー電王 ファイナル・カウントダウン</p></td>
<td><p>野上良太郎 *特別演出</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><p>5月1日</p></td>
<td><p><a href="../Page/松竹.md" title="wikilink">松竹</a></p></td>
<td><p>GOEMON</p></td>
<td><p>GOEMON</p></td>
<td><p>霧隱才藏（青年時代）</p></td>
</tr>
<tr class="odd">
<td><p>5月30日</p></td>
<td><p><a href="../Page/東寶.md" title="wikilink">東寶</a></p></td>
<td><p><a href="../Page/教頭當家.md" title="wikilink">教頭當家</a></p></td>
<td><p>ROOKIES (ルーキーズ) -卒業-</p></td>
<td><p>岡田優也</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td><p>5月8日</p></td>
<td><p><a href="../Page/東寶.md" title="wikilink">東寶</a></p></td>
<td><p>劇場版 TRICK 3</p></td>
<td><p>劇場版 TRICK 霊能力者バトルロイヤル</p></td>
<td><p>中森翔平</p></td>
</tr>
<tr class="odd">
<td><p>9月4日</p></td>
<td><p><a href="../Page/松竹.md" title="wikilink">松竹</a></p></td>
<td><p><a href="../Page/BECK.md" title="wikilink">BECK</a></p></td>
<td><p>BECK</p></td>
<td><p>田中幸雄</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p>8月25日</p></td>
<td><p><a href="../Page/華納兄弟.md" title="wikilink">華納兄弟</a></p></td>
<td><p><a href="../Page/浪客劍心_(電影).md" title="wikilink">神劍闖江湖</a></p></td>
<td><p>るろうに剣心</p></td>
<td><p><strong><a href="../Page/緋村劍心.md" title="wikilink">緋村劍心</a></strong></p></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p>6月1日</p></td>
<td><p><a href="../Page/東寶.md" title="wikilink">東寶</a></p></td>
<td><p><a href="../Page/完美的蛇頸龍之日.md" title="wikilink">完美的蛇頸龍之日</a></p></td>
<td><p>リアル〜完全なる首長竜の日</p></td>
<td><p><strong>藤田浩市</strong></p></td>
</tr>
<tr class="even">
<td><p>12月14日</p></td>
<td><p><a href="../Page/東寶.md" title="wikilink">東寶</a></p></td>
<td><p><a href="../Page/她愛上了我的謊.md" title="wikilink">她愛上了我的謊</a></p></td>
<td><p>カノジョは嘘を愛しすぎてる</p></td>
<td><p><strong>小笠原秋</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014</p></td>
<td><p>8月1日</p></td>
<td><p><a href="../Page/華納兄弟.md" title="wikilink">華納兄弟</a></p></td>
<td><p><a href="../Page/神劍闖江湖_京都大火篇.md" title="wikilink">神劍闖江湖 京都大火篇</a></p></td>
<td><p>るろうに剣心 京都大火編</p></td>
<td><p><strong>緋村劍心</strong></p></td>
</tr>
<tr class="even">
<td><p>9月13日</p></td>
<td><p><a href="../Page/華納兄弟.md" title="wikilink">華納兄弟</a></p></td>
<td><p><a href="../Page/浪客劍心：傳說的最後時刻篇.md" title="wikilink">神劍闖江湖 傳說的最終篇</a></p></td>
<td><p>るろうに剣心 伝説の最期編</p></td>
<td><p><strong>緋村劍心</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td><p>6月6日</p></td>
<td><p><a href="../Page/松竹.md" title="wikilink">松竹</a></p></td>
<td><p>廁所的聖母像</p></td>
<td><p>トイレのピエタ</p></td>
<td><p>清潔員 金澤</p></td>
</tr>
<tr class="even">
<td><p>10月3日</p></td>
<td><p><a href="../Page/東寶.md" title="wikilink">東寶</a></p></td>
<td><p><a href="../Page/爆漫王.md" title="wikilink">爆漫王</a></p></td>
<td><p>バクマン。</p></td>
<td><p><strong>真城最高</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016</p></td>
<td><p>5月14日</p></td>
<td><p><a href="../Page/東寶.md" title="wikilink">東寶</a></p></td>
<td><p><a href="../Page/如果這世界貓消失了.md" title="wikilink">如果這世界貓消失了</a></p></td>
<td><p>世界から猫が消えたなら</p></td>
<td><p><strong>僕／惡魔</strong></p></td>
</tr>
<tr class="even">
<td><p>10月15日</p></td>
<td><p><a href="../Page/東寶.md" title="wikilink">東寶</a></p></td>
<td><p><a href="../Page/何者#電影.md" title="wikilink">何者</a></p></td>
<td><p>何者</p></td>
<td><p><strong>二宮拓人</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017</p></td>
<td><p>9月30日</p></td>
<td><p><a href="../Page/東寶.md" title="wikilink">東寶</a></p></td>
<td><p><a href="../Page/亞人_(電影).md" title="wikilink">亞人</a></p></td>
<td><p>亜人</p></td>
<td><p><strong>永井圭</strong></p></td>
</tr>
<tr class="even">
<td><p>12月16日</p></td>
<td><p><a href="../Page/松竹.md" title="wikilink">松竹</a></p></td>
<td><p><a href="../Page/跨越8年的新娘.md" title="wikilink">跨越8年的新娘</a></p></td>
<td><p>8年越しの花嫁　奇跡の実話</p></td>
<td><p><strong>西澤尚志</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2018</p></td>
<td><p>4月20日</p></td>
<td><p><a href="../Page/東寶.md" title="wikilink">東寶</a></p></td>
<td><p><a href="../Page/殺戮重生犬屋敷.md" title="wikilink">殺戮重生犬屋敷</a></p></td>
<td><p>いぬやしき</p></td>
<td><p>獅子神浩</p></td>
</tr>
<tr class="even">
<td><p>10月19日</p></td>
<td><p><a href="../Page/東寶.md" title="wikilink">東寶</a></p></td>
<td><p><a href="../Page/億男.md" title="wikilink">億男</a></p></td>
<td><p>億男</p></td>
<td><p><strong>一男</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11月23日</p></td>
<td><p><a href="../Page/KADOKAWA.md" title="wikilink">KADOKAWA</a></p></td>
<td><p><a href="../Page/Hard_core_平成地獄Brothers.md" title="wikilink">Hard core 平成地獄Brothers</a></p></td>
<td><p>ハード·コア</p></td>
<td><p>權藤左近</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12月22日</p></td>
<td><p>東映</p></td>
<td><p>劇場版 <a href="../Page/假面騎士平成Generations_Forever.md" title="wikilink">假面騎士平成Generations Forever</a></p></td>
<td><p>劇場版 仮面ライダー平成ジェネレーションズFOREVER</p></td>
<td><p>野上良太郎 *特別演出</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2019</p></td>
<td><p>2月22日</p></td>
<td><p><a href="../Page/GAGA.md" title="wikilink">GAGA</a></p></td>
<td><p><a href="../Page/馬拉松武士.md" title="wikilink">馬拉松武士</a></p></td>
<td><p>サムライマラソン</p></td>
<td><p><strong>唐沢甚内</strong></p></td>
</tr>
<tr class="even">
<td><p>秋季公開預定</p></td>
<td><p>日活</p></td>
<td><p>一夜</p></td>
<td></td>
<td><p><strong>稻村雄二</strong>[10]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>8月2日</p></td>
<td><p><a href="../Page/東寶.md" title="wikilink">東寶</a></p></td>
<td><p>勇者斗恶龙：你的故事</p></td>
<td><p>ドラゴンクエスト ユア・ストーリー</p></td>
<td><p><strong>Ryuka</strong> *声出演[11]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2020</p></td>
<td><p>夏季公開預定</p></td>
<td><p><a href="../Page/華納兄弟.md" title="wikilink">華納兄弟</a></p></td>
<td><p><a href="../Page/浪客劍心_(電影).md" title="wikilink">神劍闖江湖</a> 最終章 「上篇」「下篇」</p></td>
<td><p>るろうに剣心 最終章</p></td>
<td><p><strong>緋村劍心</strong>[12]</p></td>
</tr>
</tbody>
</table>

### 舞台劇

  - [羅密歐與茱麗葉](../Page/羅密歐與茱麗葉.md "wikilink") （2012年4月29－6月10日）飾演 **羅密歐**

### 得獎

  - [第60回日劇學院賞](../Page/第60回日劇學院賞.md "wikilink")
    助演男優賞『[咩咩的完美執事](../Page/咩咩的完美執事.md "wikilink")』（2009年）
  - BLOG of the year 2009 男性タレント・俳優部門（2009年）
  - 第20回[日本電影影評人大獎](../Page/日本電影影評人大獎.md "wikilink") 新進男演員獎（南俊子獎）（2010年）
  - 第35回新人獎（2011年）
  - ジャパンアクションアワード ベストアクション男優部門 最優秀賞（2013年）
  - ジャパンアクションアワード ベストアクション男優賞（2015年）
  - [第85回日劇學院賞](../Page/第85回日劇學院賞.md "wikilink")
    主演男優賞『[天皇的御廚](../Page/天皇的御廚.md "wikilink")』（2015年）
  - 2015[東京國際戲劇節](../Page/東京國際戲劇節.md "wikilink")
    主演男優賞『[天皇的御廚](../Page/天皇的御廚.md "wikilink")』（2015年）
  - 第24回[橋田賞](../Page/橋田賞.md "wikilink")『[天皇的御廚](../Page/天皇的御廚.md "wikilink")』（2016年）
  - 第42回放送文化基金賞・演技賞『[天皇的御廚](../Page/天皇的御廚.md "wikilink")』（2016年）
  - 第41回[日本電影學院賞](../Page/日本電影學院賞.md "wikilink") 優秀主演男優賞「8年越しの花嫁
    奇跡の実話」(2017年)
  - 第13回[CONFiDENCE日劇大獎](../Page/CONFiDENCE日劇大獎.md "wikilink") 助演男優賞
    NHK連続テレビ小説「半分、青い。」、TBS系火曜ドラマ「義母と娘のブルース」(2018年)
  - [第98回日劇學院賞](../Page/第98回日劇學院賞.md "wikilink") 助演男優賞『半分、青い。』（2018年）
  - 2018年[CONFiDENCE日劇大獎年度最佳男配角獎](../Page/CONFiDENCE日劇大獎.md "wikilink")（[一半，藍色。](../Page/一半，藍色。.md "wikilink")、[繼母與女兒的藍調](../Page/繼母與女兒的藍調.md "wikilink")）\[13\]

### 廣告

  - オロナミンC（2007年）
  - [Bandai](../Page/萬代.md "wikilink")「DXデンガッシャー デンオウベルト」（2007年）
  - [NIKE](../Page/NIKE.md "wikilink")「運動衫」スチール（2007年 - 2008年）
  - [HUDSON](../Page/HUDSON.md "wikilink")「めざせ\!\!釣りマスター
    世界にチャレンジ\!編」（2008年）
  - [Right-on](../Page/Right-on.md "wikilink")（2008年 / 2009年）
  - [LOTTE](../Page/LOTTE.md "wikilink")
      - 「Fit's（犬の散歩 篇/ 噛むとフニャン 篇/ 奈良篇/ 美術館篇/ LINK篇/ 電車篇/ 小旅行篇/
        鑑賞篇・友達作ろう篇/「Fit's MAGIQ MIX7」/ 変わった篇/ 京阪電車篇/
        意外な場所で篇）」（2009年-2013年）
      - 「ガーナチョコレート（母の日ガーナ 篇）」（2009年5月3日 - 10日、期間限定）
  - [Lotte ICE
    CREAM](../Page/Lotte_ICE_CREAM.md "wikilink")「Coolish」（2009年）
  - [伊藤園](../Page/伊藤園.md "wikilink")「W BLACK」 / 「MINERAL
    SPARKIES」（2009年）
  - [KOSE](../Page/KOSE.md "wikilink")「cosmagic」(2010年 - )
  - [suntory](../Page/三得利.md "wikilink")「ほろよい（白sawa-情書篇-1 / 白sawa-情書篇-2
    / 品牌-聯繫篇）」（2010年3月9日 - 2012年、2013年）
  - [Sony Music Japan International
    Inc.](../Page/Sony_Music_Japan_International_Inc..md "wikilink")「タイム・フライズ…1994-2009（Oasis）」（2010年
    - ）
  - [NTT東日本](../Page/NTT東日本.md "wikilink")「モバイルWi-Fiルータ『光ポータブル』（「フレッツ
    おでかけパック」編 /「フレッツ・ミルエネ フレッツで節電」編 /「にねん割 眠る耳元で」編）（2010年 - 2012年）
  - [ゆうちょ銀行](../Page/ゆうちょ銀行.md "wikilink")「日本全国、ゆうちょ家族。（都会でも地方でも篇 /
    年金自動受取り篇）/「年金定期」篇」/「貯めるなら。」篇 /「定額貯金ありがとうキャンペーン」篇
    /「EXTAGEカード新登場」篇 / 「語り」篇・「花火」篇 /「今もそっくり」篇）（2010年 - 2012年）
  - [JANJAN ソース焼そば](../Page/JANJAN_ソース焼そば.md "wikilink") ( 2010年 -
    2012年)
  - 三菱自動車「ek カスタム」/ カッコek ウエスタン篇（2013年）
  - ロッテ「爽」 爽 イン ザ 風呂篇（2014年6月7日）
  - 「Rawlings」（2014年9月）
  - ロッテ「のど飴」大抜擢編（2014年9月9日）
  - Rawlings「2015年春夏コレクション」（2014年12月4日）
  - 「STAYHOT ウマい篇」（2015年6月5日）
  - 『日清のどん兵衛』（2015年9月28日）
  - サントリー「知多」（2015年11月1日）
  - 「earth music\&ecology ×『世界から猫が消えたなら』」（2016年3月1日）
  - トヨタ自動車「TOYOTA GAZOO Racing 宣言篇 2016」※ナレーション担当 （TOYOTA GAZOO Racing
    宣言 - 人篇／TOYOTA GAZOO Racing 宣言 - 道篇）（2016年5月27日）
  - 「Softbank」( 白戸家「ギガ変身」篇 / 「ハーフタイム（ドア）」篇 / 「ビュンビュン」篇 / 「ピンボール」篇 /
    「七福神」篇 / 「大容量」篇 / 「吹き出し」篇/ 「信じるな」篇 )（2016年9月8日）
  - 「ナイーブ naive沐浴乳」(2017年3月2日公布消息，3月31日CM正式播放，舞者邀請了寶塚團員參加演出)
  - [suntory](../Page/三得利.md "wikilink")「ほろよい（ほろよいの声 篇 / ただいま 篇 / 好きな色
    篇）」（2018年11月21日）

### PV

  - Bahashishi「オアシス」「約束」（2007年）
  - Bahashishi「キセキ」（2008年）
  - Mayday「Do You Ever Shine?」（2014年）

## 唱片

  - 『變身公主 登場人物歌曲系列』Vol.1
    Treasure　歌：豊実琴（[鎌苅健太](../Page/鎌苅健太.md "wikilink")）・四方谷裕史郎（[藤田玲](../Page/藤田玲.md "wikilink")）・河野亨（佐藤健）
  - 『變身公主 登場人物歌曲系列』Vol.5 YES\!　歌：河野亨（佐藤健）
  - 『Double-Action』 歌：野上良太郎、桃塔羅斯（佐藤健、[關俊彥](../Page/關俊彥.md "wikilink")）
  - 『Perfect-Action -Double-Action Complete Collection-』
      - 「Double-Action Sword
        form」歌：野上良太郎、桃塔羅斯（佐藤健、[關俊彥](../Page/關俊彥.md "wikilink")）
      - 「Double-Action Rod form」
        歌：野上良太郎、浦塔羅斯（佐藤健、[遊佐浩二](../Page/遊佐浩二.md "wikilink")）
      - 「Double-Action Ax form」
        歌：野上良太郎、金塔羅斯（佐藤健、[寺杣昌紀](../Page/寺杣昌紀.md "wikilink")）
      - 「Double-Action Gun form」
        歌：野上良太郎、龍塔羅斯（佐藤健、[鈴村健一](../Page/鈴村健一.md "wikilink")）
  - 『Real-Action』歌：野上良太郎（佐藤健）
  - 「Double-Action Wing form」
    歌：野上良太郎、SIEG（佐藤健、[三木真一郎](../Page/三木真一郎.md "wikilink")）

## 寫真集

  - CD Photo book 「Pre-go 〜ZERO〜」（2007年發售）
  - 寫真集「400DAYS」（2008年7月28日發售）
  - 寫真集「Intently」（2008年1月發售）
  - 寫真集「深呼吸。」（2009年1月11日發售）
  - 寫真集「So Far So Good」（2010年10月15日發售）
  - 寫真集「NOUVELLES」（2011年1月發售）
  - 寫真集「6 1/2 ～2007-2013 佐藤健の6年半～ vol.1 さくらんぼ」（2013年11月29日發售）
  - 寫真集「6 1/2 ～2007-2013 佐藤健の6年半～ vol.2 ロックバラード 」（2013年12月6日發售）
  - 寫真集「6 1/2 ～2007-2013 佐藤健の6年半～ Vol.3 風」（2013年12月13日發售）
  - 寫真集「ALTERNATIVE」（2014年7月24日發售）
  - 電影公式書「世界から猫が消えたなら」オフィシャルフォトブック（2016年4月21日發售）
  - 寫真集＋DVD「X(ten)」（2016年9月12日發售）
  - 「るろうにほん 熊本へ」（2017年4月14日發售）
  - 佐藤健in半分，青い。PHOTO BOOK（2018年9月5日發售）

## DVD

  - First DVD「My Color」（2008年2月發售）
  - THE GAME 〜Boy's Film Show〜（2009年12月發售）
  - HT 〜N.Y.の中心で、鍋をつつく〜（2010年2月17日發售）
  - THE GAME 〜Boy's Film Show〜2010（2010年10月10日發售）
  - HT〜赤道の真下で鍋をつつく〜 （2011年11月11日發售）
  - 「たけてれDVD vol.1 」（初回限定版）～（2014年7月24日發售）
  - 「たけてれDVD vol.1 」（通常版）～（2015年8月7日發售）
  - 「たけてれDVD vol.2」～（2015年8月26日發售）
  - 「たけてれDVD vol.3」～（2016年9月12日發售）
  - 「たけてれDVD vol.4」～（2017年8月30日發售）
  - 「たけてれDVD vol.5」～（2018年11月14日發售）

## 參考資料

## 外部連結

  - [佐藤健官方個人介紹頁](http://artist.amuse.co.jp/artist/satoh_takeru/)

  -

  - [佐藤健個人網站](http://www.satohtakeru.com/)

  - [佐藤健官方BLOG](https://archive.is/20130111175707/ameblo.jp/takeru-s/)（已於2011年7月1日關閉）

  - [Amuse Facebook](https://www.facebook.com/amuse.jp/)

  - [佐藤健的星志时间轴](https://starlog.cn/s/佐藤健)

[Category:日本男演員](../Category/日本男演員.md "wikilink")
[Category:Amuse所屬藝人](../Category/Amuse所屬藝人.md "wikilink")
[Category:埼玉市出身人物](../Category/埼玉市出身人物.md "wikilink")
[Category:假面騎士系列主演演員](../Category/假面騎士系列主演演員.md "wikilink")
[Category:橋田賞得主](../Category/橋田賞得主.md "wikilink")
[Category:東京國際戲劇節最佳男主角得主](../Category/東京國際戲劇節最佳男主角得主.md "wikilink")
[Category:日劇學院賞最佳男配角得主](../Category/日劇學院賞最佳男配角得主.md "wikilink")
[Category:日劇學院賞最佳男主角得主](../Category/日劇學院賞最佳男主角得主.md "wikilink")
[Category:CONFiDENCE日劇大獎最佳男配角得主](../Category/CONFiDENCE日劇大獎最佳男配角得主.md "wikilink")

1.  『ムーブ\!』2008年10月6日播放。

2.  [佐藤健官方網站](http://www.amuse.co.jp/artist/Takeru_Satou/profile.html)

3.
4.  [日刊體育新聞
    「最弱ライダー」佐藤健、肺気胸で療養2007年11月4日刊登。](http://www.nikkansports.com/entertainment/p-et-tp0-20071104-278607.html)

5.  『東京フレンドパークII』2008年10月20日播放。

6.  佐藤健舊官方BIOG「WITH」中於2010年1月15日刊登。

7.  『笑っていいとも』2012年8月23日播放。

8.  『新堂本兄弟』2013年12月8日播放

9.  『たけてれ』為網路直播節目，不定期直播（通常一個月一次）與粉絲互動，節目中會聊近況。

10.

11.

12.

13.