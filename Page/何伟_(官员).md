**何偉**（），原名**霍恒德**，[河南](../Page/河南.md "wikilink")[汝南人](../Page/汝南.md "wikilink")，[中华人民共和国官员](../Page/中华人民共和国.md "wikilink")。1934年毕业於[武漢](../Page/武漢.md "wikilink")[華中大學](../Page/華中大學.md "wikilink")，1936年3月加入[中國共產黨](../Page/中國共產黨.md "wikilink")。

何偉曾於1949年9月到1952年7月，任广西省委副书记兼桂北区党委书记，1952年12月任广西第一届政协副主席。1952年12月—1955年1月\[1\]，在[廣州市第五屆各界人民代表會議及廣州市第一屆人民代表大會第一次會議中](../Page/廣州市.md "wikilink")，擔任廣州市委第一书记、[市長](../Page/市長.md "wikilink")。

1958年，接替[罗贵波](../Page/罗贵波.md "wikilink")，担任[中华人民共和国驻越南大使](../Page/中国驻越南大使列表.md "wikilink")\[2\]。1962年，由[朱其文接任](../Page/朱其文.md "wikilink")。1962年4月任河南省委第二书记，1964年当选第三届全国人大代表。1964年5月，出任[教育部部长](../Page/中华人民共和国教育部.md "wikilink")、党组书记。

1973年3月，因为在[文化大革命中身心受到严重摧残](../Page/文化大革命.md "wikilink")，在北京含冤去世，享年63岁。

## 参考文献

<div class="references-small">

<references />

</div>

{{-}}

[H](../Category/中華人民共和國教育部部長.md "wikilink")
[H](../Category/中华人民共和国政府官员.md "wikilink")
[H](../Category/1910年出生.md "wikilink")
[H](../Category/1973年逝世.md "wikilink")
[H](../Category/汝南人.md "wikilink")
[W伟](../Category/何姓.md "wikilink")
[Category:中华人民共和国驻越南大使](../Category/中华人民共和国驻越南大使.md "wikilink")
[Category:第三届全国人大代表](../Category/第三届全国人大代表.md "wikilink")
[Category:中国共产党党员
(1936年入党)](../Category/中国共产党党员_\(1936年入党\).md "wikilink")
[Category:霍姓](../Category/霍姓.md "wikilink")

1.  1953年3月12日廣州市改為中央[直轄市](../Page/直轄市.md "wikilink")。1955年1月1日廣州市改為[廣東省轄市](../Page/廣東省.md "wikilink")。同年1月15日[廣州市人民政府改為](../Page/廣州市人民政府.md "wikilink")[廣州市人民委員會](../Page/廣州市人民委員會.md "wikilink")。
2.