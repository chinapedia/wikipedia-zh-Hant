**何卓瑩**（，），是[亞洲電視合約女藝員](../Page/亞洲電視.md "wikilink")，[Starj &
Snazz娛樂旗下藝人](../Page/Starj_&_Snazz娛樂.md "wikilink")，曾演出多部[電視劇](../Page/電視劇.md "wikilink")。也就是靠美少女廚神參賽入行的。她曾就讀於[基華小學](../Page/基華小學.md "wikilink")、[德愛中學畢業](../Page/德愛中學.md "wikilink")。於[2008年加入](../Page/2008年.md "wikilink")[亞洲電視](../Page/亞洲電視.md "wikilink")，就讀[2008年](../Page/2008年.md "wikilink")[亞洲電視藝員訓練班](../Page/亞洲電視藝員訓練班.md "wikilink")，並曾主持《[AV事務所](../Page/AV事務所_\(電視節目\).md "wikilink")》及兒童節目《[有腦E班](../Page/有腦E班.md "wikilink")》。\[1\]\[2\]\[3\]\[4\]

## 演出作品

### MV

  - [2012年林一峰](../Page/2012年.md "wikilink") 清水mv

### 現主持電視節目

  - [aTV](../Page/亞洲電視.md "wikilink")：[通識小學堂](../Page/通識小學堂.md "wikilink")
    ([2010年](../Page/2010年.md "wikilink")-)[通識小學堂
    Video](http://www.youtube.com/view_play_list?p=C4F04173BF548DB0)

### 曾主持電視節目

  - [TVB](../Page/TVB.md "wikilink")：[味分高下](../Page/味分高下.md "wikilink")
    味之天使 - 「蘋果」 ([2007年](../Page/2007年.md "wikilink"))
  - [TVB](../Page/TVB.md "wikilink")：[歡樂滿東華](../Page/歡樂滿東華.md "wikilink")
    ([2007年](../Page/2007年.md "wikilink"))
  - [aTV](../Page/亞洲電視.md "wikilink")：[AV事務所](../Page/AV事務所_\(電視節目\).md "wikilink")
    ([2008年](../Page/2008年.md "wikilink"))
  - [aTV](../Page/亞洲電視.md "wikilink")：[今日法庭](../Page/今日法庭.md "wikilink")
    ([2008年](../Page/2008年.md "wikilink"))
  - [aTV](../Page/亞洲電視.md "wikilink")：[開學啦](../Page/開學啦.md "wikilink")
    ([2008年](../Page/2008年.md "wikilink"))
  - [aTV](../Page/亞洲電視.md "wikilink")：[有腦E班](../Page/有腦E班.md "wikilink")
    ([2008年](../Page/2008年.md "wikilink")-[2009年](../Page/2009年.md "wikilink"))
  - [aTV](../Page/亞洲電視.md "wikilink")：[生活著數台](../Page/生活著數台.md "wikilink")
    ([2008年](../Page/2008年.md "wikilink")-[2009年](../Page/2009年.md "wikilink"))
  - [Roadshow](../Page/Roadshow.md "wikilink")：美味香港之婚宴美食巡禮
    ([2009年](../Page/2009年.md "wikilink"))
  - [aTV](../Page/亞洲電視.md "wikilink")：[開心大發現2009](../Page/開心大發現2009.md "wikilink")
    ([2009年](../Page/2009年.md "wikilink")) [開心大發現2009
    Video](http://www.youtube.com/view_play_list?p=8BF1654D1C8240DE)
  - [aTV](../Page/亞洲電視.md "wikilink")：[香港亂噏](../Page/香港亂噏.md "wikilink")
    ([2009年](../Page/2009年.md "wikilink"))
  - [aTV](../Page/亞洲電視.md "wikilink")：[魅力新塘](../Page/魅力新塘.md "wikilink")
    ([2010年](../Page/2010年.md "wikilink"))
    [魅力新塘Video](http://www.youtube.com/view_play_list?p=D6EB67985ECC2F3C)
  - [aTV](../Page/亞洲電視.md "wikilink")：[娛樂紅人館](../Page/娛樂紅人館.md "wikilink")
    ([2010年](../Page/2010年.md "wikilink"))
    [《娛樂紅人館》Video](http://www.youtube.com/view_play_list?p=8163EC097E59B4C4)
  - [aTV](../Page/亞洲電視.md "wikilink")：[日本築地美食大追擊](../Page/日本築地美食大追擊.md "wikilink")
    ([2010年](../Page/2010年.md "wikilink"))
    [日本築地美食大追擊Video](http://www.youtube.com/view_play_list?p=1DE1B3D3F9EB286E)
  - [aTV](../Page/亞洲電視.md "wikilink")：[開心大發現2010](../Page/開心大發現2010.md "wikilink")
    ([2010年](../Page/2010年.md "wikilink")) [開心大發現2010
    Video](http://www.youtube.com/view_play_list?p=81909E45A8A72C91)
  - [aTV](../Page/亞洲電視.md "wikilink")：[沙溪尋寶之旅](../Page/沙溪尋寶之旅.md "wikilink")
    ([2010年](../Page/2010年.md "wikilink")) [沙溪尋寶之旅
    Video](http://www.youtube.com/view_play_list?p=CE369E1A6E437EA1)
  - [aTV](../Page/亞洲電視.md "wikilink")：[尋找百姓家](../Page/尋找百姓家.md "wikilink")
    ([2011年](../Page/2011年.md "wikilink")-)[尋找百姓家
    Video](http://www.youtube.com/my_playlists?p=FD6986B6D934B8C2)
  - [aTV](../Page/亞洲電視.md "wikilink")：[兔氣揚眉過肥年](../Page/兔氣揚眉過肥年.md "wikilink")
    ([2011年](../Page/2011年.md "wikilink"))[兔氣揚眉過肥年
    Video](http://www.youtube.com/my_playlists?p=218987540DBBDE5D)
  - [東亞銀行](../Page/東亞銀行.md "wikilink")：新世代「由你指導」
    ([2011年](../Page/2011年.md "wikilink"))[新世代「由你指導」網址](https://web.archive.org/web/20110112032741/http://www.udirect.com.hk/about_angel.php)、[Video](http://www.youtube.com/user/UdirectHK#p/a/u/1/j1XH3dHb27w)
  - [aTV](../Page/亞洲電視.md "wikilink")：[亞洲小天使·我愛NO.1](../Page/亞洲小天使·我愛NO.1.md "wikilink")
    ([2011年](../Page/2011年.md "wikilink"))

### 曾演出電視劇

  - [好美麗診所嘿哈](../Page/好美麗診所嘿哈.md "wikilink")
    ([2010年](../Page/2010年.md "wikilink")) 飾 Angel
    ([台灣](../Page/台灣.md "wikilink"))
  - [aTV](../Page/亞洲電視.md "wikilink")：[法網群英](../Page/法網群英.md "wikilink")
    ([2010年](../Page/2010年.md "wikilink")) 飾 Sera
    ([鄧特希工作室](../Page/鄧特希工作室.md "wikilink"))[Angel
    演出部份-法網群英Video](http://www.youtube.com/view_play_list?p=0C15A32D4777D6C3)
  - [aTV](../Page/亞洲電視.md "wikilink")：[勝者為王](../Page/勝者為王_\(2010年電視劇\).md "wikilink")
    ([2010年](../Page/2010年.md "wikilink")) 飾 田詩詩
    ([新視線集團](../Page/新視線集團.md "wikilink"))[Angel演出部份-勝者為王Video](http://www.youtube.com/view_play_list?p=F89E244ACBFB2FA9)
  - [aTV](../Page/亞洲電視.md "wikilink")：[香港GoGoGo](../Page/香港GoGoGo.md "wikilink")
    ([2010年](../Page/2010年.md "wikilink")) 飾 Angel
    [Angel演出部份Video](http://www.youtube.com/my_playlists?p=EC6DA578FF6360ED)
  - [aTV](../Page/亞洲電視.md "wikilink")：[親密損友](../Page/親密損友.md "wikilink")
    ([2011年](../Page/2011年.md "wikilink")) 飾 Angel
  - [香港電台](../Page/香港電台.md "wikilink")：《同行者》([2016年](../Page/2016年.md "wikilink"))
    飾 社工

### 曾主持電台節目

  - [Uonlive](../Page/Uonlive.md "wikilink")：[廿二世紀少女](../Page/廿二世紀少女.md "wikilink")
    ([2009年](../Page/2009年.md "wikilink"))

### 曾演出電影

  - [2004年](../Page/2004年.md "wikilink")：《[娛樂大腳八](../Page/娛樂大腳八.md "wikilink")》
  - [2008年](../Page/2008年.md "wikilink")：《[十分鍾情](../Page/十分鍾情.md "wikilink")》
  - [2008年](../Page/2008年.md "wikilink")：《[有隻僵屍暗戀你](../Page/有隻僵屍暗戀你.md "wikilink")》
  - [2009年](../Page/2009年.md "wikilink")：《[明媚時光](../Page/明媚時光.md "wikilink")》
  - [2010年](../Page/2010年.md "wikilink")：《[殭屍新戰士](../Page/殭屍新戰士.md "wikilink")》飾
    Joey
  - [2012年](../Page/2012年.md "wikilink")：《[寒戰](../Page/寒戰.md "wikilink")》
  - [2015年](../Page/2015年.md "wikilink")：《[踏血尋梅](../Page/踏血尋梅.md "wikilink")》
  - [2016年](../Page/2016年.md "wikilink")：《[寒戰II](../Page/寒戰II.md "wikilink")》
  - [2018年](../Page/2018年.md "wikilink")：《[對倒](../Page/對倒.md "wikilink")》
    飾 嘉儀

### 曾拍攝廣告

  - [馬會](../Page/馬會.md "wikilink") [足智彩介紹](../Page/足智彩.md "wikilink")
    2009-[世界杯](../Page/世界杯.md "wikilink")2010
    [足智彩Vedio](http://www.youtube.com/view_play_list?p=D73CE12254543473)
  - [萬寧](../Page/萬寧.md "wikilink") 系列廣告 香港 2009
    [萬寧廣告Vedio](http://www.youtube.com/my_playlists?p=F51D496FF38FF2D9)
  - [圓玄學院](../Page/圓玄學院.md "wikilink")[菊花展](../Page/菊花.md "wikilink") 香港
    2008-2010
    [圓玄學院菊花展廣告Vedio](http://www.youtube.com/watch?v=IdSTc8tO4zA)
  - [藍妹啤酒](../Page/藍妹啤酒.md "wikilink")（平面廣告） 香港 2007
  - [索尼](../Page/索尼.md "wikilink")(6758.T) 香港 2007
  - [益力多](../Page/益力多.md "wikilink") 飾 Judy (2267.T) 香港 2007
  - [香港中華煤氣](../Page/香港中華煤氣.md "wikilink")(0003.HK) 香港 2006
    [煤氣廣告Vedio](http://www.youtube.com/watch?v=KZ22a8Or_Rg)
  - [Super X](../Page/Super_X.md "wikilink") (Sports Wear) 香港 2004

### 曾擔任大使

  - 第四屆[九龍城區區節大使](../Page/九龍城區.md "wikilink") 香港 2010
    [第四屆九龍城區區節大使Video](http://www.youtube.com/my_playlists?p=914B671D2F6DEE2D)

## 參考

## 外部連結

  -
  -
  - [何卓瑩 網誌](http://hk.myblog.yahoo.com/angel-hcy)

  - [何卓瑩官方論壇](http://www.angelho.net/)

  - [何卓瑩 有腦E班小遊戲](http://hk.myblog.yahoo.com/acute_lau)

  - [aTV 2010復活節promo Video](http://www.youtube.com/watch?v=wKcTYXeiR-k)

[Category:香港新教徒](../Category/香港新教徒.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:香港女性模特兒](../Category/香港女性模特兒.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Category:香港客家人](../Category/香港客家人.md "wikilink")
[Z](../Category/何姓.md "wikilink")
[Category:香港兒童節目主持](../Category/香港兒童節目主持.md "wikilink")
[Category:國立政治大學校友](../Category/國立政治大學校友.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")

1.  [1](https://hk.entertainment.appledaily.com/entertainment/daily/article/20080402/10938390)
2.  [2](http://hd.stheadline.com/ent/ent_content.asp?contid=24637)
3.  [3](http://the-sun.on.cc/cnt/lifestyle/20091023/00481_001.html)
4.  [4](http://yule.sohu.com/20081025/n260242579.shtml)