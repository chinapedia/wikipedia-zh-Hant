**喬塞普·卡雷拉斯**（，發音:\[ʒuˈzɛp
kəˈreɾəs\]\[1\]，），是[加泰隆尼亞](../Page/加泰隆尼亞.md "wikilink")[歌唱家](../Page/歌唱家.md "wikilink")，20世紀後半葉的世界三大[男高音之一](../Page/男高音.md "wikilink")，是演繹[威爾第和](../Page/威爾第.md "wikilink")[普契尼作品中男高音角色的權威之一](../Page/普契尼.md "wikilink")。

## 簡歷

卡雷拉斯出生於加泰隆尼亞的[巴塞隆納](../Page/巴塞隆納.md "wikilink")，自幼便開始展現自己的音樂天賦。他的首次公開表演便是在八歲那年，在演唱著名[詠嘆調](../Page/詠嘆調.md "wikilink")《》。十一歲那年，卡雷拉斯便在[利塞奥大剧院擔任童聲高音](../Page/利塞奥大剧院.md "wikilink")，曾出演《[波希米亞人](../Page/波希米亚人_\(歌剧\).md "wikilink")》第二幕中的小頑童和[法雅所作的](../Page/曼努埃尔·德·法利亚.md "wikilink")《》中的旁白。

其後卡氏入讀，並在自己畢業後於利塞奥大劇院的首演，擔任《[諾爾瑪](../Page/諾爾瑪.md "wikilink")》中的男主角。他的首演獲得了和他同場獻藝的當紅女高音[蒙特賽拉特·卡芭葉的垂青](../Page/蒙特賽拉特·卡芭葉.md "wikilink")。卡芭葉隨後邀請卡雷拉斯主唱[多尼采蒂的](../Page/葛塔諾·多尼采蒂.md "wikilink")《[魯克蕾齊亞·波吉亞](../Page/魯克蕾齊亞·波吉亞.md "wikilink")》（*Lucrezia
Borgia*），這個機會成為卡雷拉斯事業上的首個突破。

24歲那年，卡雷拉斯首次在[倫敦登台](../Page/倫敦.md "wikilink")，與卡芭葉同台在一場《》的歌劇音樂會中演唱。此後，他們兩人在共同主唱過超過15部歌劇。

1972年，卡雷拉斯第一次在[美國演出](../Page/美國.md "wikilink")，劇目為《[蝴蝶夫人](../Page/蝴蝶夫人.md "wikilink")》。1974年，卡氏分別在[歐洲幾大歌劇院的排演劇目中首次登台主唱](../Page/歐洲.md "wikilink")，包括[維也納國立歌劇院的](../Page/維也納國立歌劇院.md "wikilink")《弄臣》、[倫敦皇家歌劇院的](../Page/倫敦皇家歌劇院.md "wikilink")《[茶花女](../Page/茶花女_\(歌劇\).md "wikilink")》和[紐約](../Page/紐約.md "wikilink")[大都會歌劇院的](../Page/大都會歌劇院.md "wikilink")《[托斯卡](../Page/托斯卡.md "wikilink")》。次年，他在[米蘭](../Page/米蘭.md "wikilink")[斯卡拉大劇院首次亮相](../Page/斯卡拉大劇院.md "wikilink")，主唱了《[化妆舞会
(歌剧)](../Page/化妆舞会_\(歌剧\).md "wikilink")》。此時28歲的卡雷拉斯，已經擔綱主唱過24部歌劇了。

1987年，正當卡雷拉斯的事業攀上高峰時，他卻被診斷出患上[血癌](../Page/血癌.md "wikilink")，並據說只有十分之一的生存機會。經過一年多的化療和放射性治療，最終卡氏靠[骨髓移植得以痊癒](../Page/骨髓移植.md "wikilink")，重回歌劇演唱界。自此，卡雷拉斯更加關注血癌病人的權益。1988年，他成立了。自1995年起，他每年都在[德國](../Page/德國.md "wikilink")[萊比錫為血癌患者舉行慈善音樂會](../Page/萊比錫.md "wikilink")。

1990年[羅馬](../Page/羅馬.md "wikilink")[足球世界杯期間](../Page/1990年世界盃足球賽.md "wikilink")，卡氏和[帕瓦罗蒂](../Page/卢奇亚诺·帕瓦罗蒂.md "wikilink")、[多明哥舉行了首場](../Page/普拉西多·多明哥.md "wikilink")「[三大男高音演唱會](../Page/三大男高音演唱會.md "wikilink")」。原本音樂會是為卡雷拉斯的血癌基金會募款的，也是帕、多二人歡迎卡氏回到舞台的專場。最後，數以億計的人透過各種媒體欣賞了這場音樂會。

2016年2月，宣告展開為期1年的告別演唱會。

另一方面，卡雷拉斯也偶爾出演一些西班牙本地的[查瑞拉歌劇](../Page/查瑞拉歌劇.md "wikilink")，還曾在[伯恩斯坦的指揮下](../Page/伯恩斯坦.md "wikilink")，錄製了伯恩斯坦自己所作的音樂劇《[西区故事](../Page/西城故事.md "wikilink")》。

## 其他軼事

在美國[電視連續劇](../Page/電視.md "wikilink")《[宋飛正傳](../Page/宋飛正傳.md "wikilink")》中，宋飛和他的朋友記得三大男高音中帕瓦罗蒂和多明哥，但總忘記卡雷拉斯的名字，總以「另外一個（*the
other guy*）」稱代卡氏。另一個角色鮑伯（Bob）則反而視卡氏為偶像，以「其他兩個（*those two other
guys*）」稱呼帕、多二人。

## 參考資料

<div class="references-small">

  - Warrack, J. & West, E. (1996). *The Concise Oxford Dictionary of
    Opera - 3rd Edition*. New York, NY: Oxford University Press. Page
    80. ISBN 0-19-280028-0.
  - Kennedy M.(2004). *The Concise Oxford Dictionary of Music - 4th
    Edition*. New York, NY: Oxford University Press. Page 127. ISBN
    0-19-860884-5.

</div>

## 站外連結

  -
[Category:加泰隆尼亞人](../Category/加泰隆尼亞人.md "wikilink")
[Category:巴塞羅那人](../Category/巴塞羅那人.md "wikilink")
[Category:加泰罗尼亚独立运动人物](../Category/加泰罗尼亚独立运动人物.md "wikilink")
[Category:西班牙男歌手](../Category/西班牙男歌手.md "wikilink")
[Category:男高音](../Category/男高音.md "wikilink")
[Category:阿斯图里亚斯亲王奖获得者](../Category/阿斯图里亚斯亲王奖获得者.md "wikilink")

1.  [加泰隆尼亞姓氏中](../Page/加泰隆尼亞.md "wikilink")，Carreras是父親的姓氏、Coll是母親的姓氏、i代表「與」的意思