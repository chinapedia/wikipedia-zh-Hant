[Jose_Ortega_y_Gasset.jpg](https://zh.wikipedia.org/wiki/File:Jose_Ortega_y_Gasset.jpg "fig:Jose_Ortega_y_Gasset.jpg")
**何塞·奧特嘉·伊·加塞特**（，），[简称](../Page/简称.md "wikilink")**奧特嘉**，是一位[西班牙的](../Page/西班牙.md "wikilink")[哲學家](../Page/哲學家.md "wikilink")、[報業從業人員及](../Page/報紙.md "wikilink")[評論家](../Page/評論家.md "wikilink")。其[哲学](../Page/哲学.md "wikilink")[思想主要是](../Page/思想.md "wikilink")[存在主義](../Page/存在主義.md "wikilink")、[歷史哲學和对西班牙](../Page/歷史.md "wikilink")[民族性的批判](../Page/民族性.md "wikilink")。

## 生平

奧特嘉在1883年生於西班牙馬德里。1904年獲博士學位。1905年遊學德國，歷時三年，奧特嘉在此深刻地吸收新康德學派的思想。這一年間，正是發生[马克斯·韦伯](../Page/马克斯·韦伯.md "wikilink")《資本主義與新教倫理》一書內容於《社會科學與政治文庫》No.20中發表後的論戰風波，而韦伯正是持新康德主義西南歷史學派方法論者。1910年任馬德里大學\[形而上學\]教授。1923年為《[西方的沒落](../Page/西方的沒落.md "wikilink")》一書西班牙譯本作序。1931年任國會議員，同年因發表批評意見而不見容於國會，辭議員。

## 著作書目

  - 《藝術的非人性化》1925 (La deshumanización del Arte e Ideas sobre la novela)
  - 《小說的筆記》
  - 《語言的本質》1927 (Espíritu de la letra)
  - 《群眾的反叛》1930 (La rebelión de las masas)
  - 《論伽利略》 1933 (En torno a Galileo)
  - 《自我沉思與自我疏離》1939 (Ensimismamiento y alteración)
  - 《技術論》 1939 (Meditación de la técnica)
  - 《概念與信念》 1940 (Ideas y Crencias)
  - 《愛》 1940出版，後散收錄在各書，台灣有中譯版
  - 《論萊布尼茲》 1958
  - 《年輕民族的考察》 1958
  - 《世界史詮釋》 1960
  - 《哲學的起源》

## 知名語錄

  - 「不必要的工作會使人情緒低落。\[1\]」(可能的解釋之一是：不要浪費時間做些無意義的事情，不然就是要附加這些事情意義。)

## 參考文獻

## 備註、注釋

<references group="注"/>

<div class="references-small">

</div>

[G](../Category/西班牙哲學家.md "wikilink")
[G](../Category/德烏斯托大學校友.md "wikilink")
[G](../Category/馬德里康普頓斯大學校友.md "wikilink")

1.  裝訂：平裝，2nd edition.