[Révérend_R.O._Hall,_consacré_évêque_de_Hong-Kong_à_l'église_Saint_Paul_de_Londres.jpg](https://zh.wikipedia.org/wiki/File:Révérend_R.O._Hall,_consacré_évêque_de_Hong-Kong_à_l'église_Saint_Paul_de_Londres.jpg "fig:Révérend_R.O._Hall,_consacré_évêque_de_Hong-Kong_à_l'église_Saint_Paul_de_Londres.jpg")
[Stela_of_Ming_Wah_Dai_Ha.jpg](https://zh.wikipedia.org/wiki/File:Stela_of_Ming_Wah_Dai_Ha.jpg "fig:Stela_of_Ming_Wah_Dai_Ha.jpg")[明華大廈](../Page/明華大廈.md "wikilink")[奠基石](../Page/奠基石.md "wikilink")\]\]
**何明華會督**，[CMG](../Page/CMG.md "wikilink")，[MC](../Page/軍功十字勳章.md "wikilink")（，）是[香港聖公會的前任](../Page/香港聖公會.md "wikilink")[主教](../Page/主教.md "wikilink")。他於1932年十月二十五日被祝聖為主教，當年[聖誕節後一週在港舉行陞座禮](../Page/聖誕節.md "wikilink")，於1932年至1951年任[維多利亞教區](../Page/維多利亞教區.md "wikilink")[主教及](../Page/維多利亞主教.md "wikilink")[中華聖公會港粵教區](../Page/中華聖公會.md "wikilink")(後稱華南教區)會督(即主教)，1951年至1967年任[聖公會港澳教區會督](../Page/聖公會港澳教區.md "wikilink")。

何明華曾於1944年按立[李添嬡](../Page/李添嬡.md "wikilink")，後者成為了普世聖公宗史上首位女牧師。

1965年，[香港大學授予他名譽](../Page/香港大學.md "wikilink")[神學博士學位](../Page/神學博士.md "wikilink")。\[1\]

## 以何明華命名的建築物及機構

與何明華會督有關的學校/建築物/機構包括：

1.  [香港中文大學](../Page/香港中文大學.md "wikilink")[崇基學院](../Page/崇基學院.md "wikilink")[明華堂](../Page/明華堂.md "wikilink")
2.  [香港聖公會何明華會督中學](../Page/香港聖公會何明華會督中學.md "wikilink")
3.  [何明華會督銀禧中學](../Page/何明華會督銀禧中學.md "wikilink")
4.  [香港聖公會明華神學院](../Page/聖公會明華神學院.md "wikilink")
5.  [聖雅各福群會](../Page/聖雅各福群會.md "wikilink")
6.  [聖基道兒童院](../Page/聖基道兒童院.md "wikilink")
7.  [聖公會聖匠堂社區中心](../Page/聖公會聖匠堂社區中心.md "wikilink")
8.  [童膳會](../Page/童膳會.md "wikilink")
9.  [筲箕灣](../Page/筲箕灣.md "wikilink")[明華大廈](../Page/明華大廈.md "wikilink")
10. [聖公會聖本德中學何明華會督堂](../Page/聖公會聖本德中學.md "wikilink")

## 書籍

  - Hall, Ronald O. (1942). *[The Art of the Missionary: Fellow Workers
    with the Church in
    China](http://anglicanhistory.org/asia/skh/hall/hall-art.pdf)*.
    London: Student Christian Movement Press.

## 参考文献

## 参见

  - [普世聖公宗](../Page/普世聖公宗.md "wikilink")
  - [中華聖公會](../Page/中華聖公會.md "wikilink")
  - [香港聖公會](../Page/香港聖公會.md "wikilink")
  - [維多利亞教區](../Page/維多利亞教區.md "wikilink")
  - [維多利亞主教](../Page/維多利亞主教.md "wikilink")
  - [聖公會港澳教區](../Page/聖公會港澳教區.md "wikilink")

{{-}}

[Category:香港聖公會聖品](../Category/香港聖公會聖品.md "wikilink")
[Category:香港大學名譽博士](../Category/香港大學名譽博士.md "wikilink")
[Category:軍功十字勳章](../Category/軍功十字勳章.md "wikilink")

1.  [香港大學第64屆頒授典禮讚詞：何明華會督](https://www4.hku.hk/hongrads/index.php/chi/archive/graduate_detail/222)