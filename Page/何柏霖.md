**何柏霖**
（，），[香港](../Page/香港.md "wikilink")[足球評述員](../Page/足球評述員.md "wikilink")，[香港足球評述員協會現屆執委會主席](../Page/香港足球評述員協會.md "wikilink")。

何柏霖2001年經[有線電視選拔及培訓](../Page/香港有線電視.md "wikilink")10個月後，正式入行成為[足球評述員](../Page/足球.md "wikilink")，在有線[足球台及](../Page/香港有線電視足球台.md "wikilink")[體育台](../Page/香港有線電視體育台.md "wikilink")[講波及主持足球節目](../Page/講波.md "wikilink")。

效力有線期間，主力評述[西甲及](../Page/西甲.md "wikilink")[德甲賽事各六年](../Page/德甲.md "wikilink")，亦曾評述三屆[世界盃及](../Page/世界盃.md "wikilink")2004[歐洲國家盃](../Page/歐洲國家盃.md "wikilink")。評述風格上，以緊貼直播畫面為先，配合自家搜集球場內外資料，並適時滲入歐遊睇波朝聖見聞，務求強化[講波內容](../Page/講波.md "wikilink")。曾現場直播報導[UEFA
Champions League決賽](../Page/UEFA_Champions_League.md "wikilink")，[UEFA
Europa
League決賽](../Page/歐足協歐霸聯賽.md "wikilink")，[女子世界盃決賽週](../Page/女子世界盃.md "wikilink")。

2013年8月，何柏霖未有與有線電視續約，改以自由身繼續在行業工作。暫別有線前最後一個節目，為主持德甲50週年特輯，一連五集「德甲邁向新世紀」。

2013年9月起，何柏霖先後亮相[now寬頻電視](../Page/now寬頻電視.md "wikilink")101台足球遊戲節目
「球分」、為[亞洲電視評述](../Page/亞洲電視.md "wikilink")[蘇格蘭超級聯賽及第一季](../Page/蘇格蘭超級聯賽.md "wikilink")[香港超級聯賽](../Page/香港超級聯賽.md "wikilink")、為[太陽飛馬擔任賽日駐場司儀](../Page/太陽飛馬.md "wikilink")、為[車路士足球學校(香港)主持網上頻道Channel](../Page/車路士足球學校\(香港\).md "wikilink")
Blue。

2014年9月，回巢有線主持2014仁川[亞運節目](../Page/亞運.md "wikilink")：「亞運最精彩」。

2014-15球季，為[亞洲電視主持第一季](../Page/亞洲電視.md "wikilink")[香港超級聯賽](../Page/香港超級聯賽.md "wikilink")，包括揭幕戰傑志對和富大埔，以及本地盃賽決賽。他提及，在香港從事體育傳媒，一定以評述本地波為榮。

為記錄2015年9月3日，港隊於中港大戰力守不敗，他首度二次創作歌詞 「守得救」(原曲林峰 - 愛不疚)。

2015-16球季，何柏霖重投德甲及歐霸直播，加盟[FOX Sports](../Page/FOX_Sports.md "wikilink")
( [now寬頻電視](../Page/now寬頻電視.md "wikilink") 670, 671,
672台)，擔任評述及球員譯名統籌工作。期間主持德國國家打比[拜仁慕尼黑對](../Page/拜仁慕尼黑.md "wikilink")[多蒙特](../Page/多蒙特.md "wikilink")、魯爾區打比[多蒙特對](../Page/多蒙特.md "wikilink")[史浩克04](../Page/史浩克04.md "wikilink")、2016歐霸決賽[利物浦對](../Page/利物浦.md "wikilink")[西維爾等球賽](../Page/西維爾.md "wikilink")。

2016-17球季，他在FOX
Sports擴闊評述內容，首度參與[東南亞足球錦標賽及](../Page/東南亞足球錦標賽.md "wikilink")[亞洲聯賽冠軍盃直播](../Page/亞洲聯賽冠軍盃.md "wikilink")。

為強化評述內容，何柏霖趁有空檔就遊歷多國球場，務求在直播上融入實地朝聖體會。先後前往德國、西班牙、英格蘭、俄羅斯、北歐、日本、澳洲等。其觀點「與其隔岸觀火，不如實地朝聖」，正好表達球迷透過電視直播欣賞球賽之餘，總要動身出發一次，立體感受賽日體驗。

## 主持有線節目

  - 世界盃 (2002, 06, 10)

` 2010緊貼智利及西班牙比賽`

  - 德國足球聯賽 (2001-03, 2009-13)
  - 德國足球聯賽國家打比- [拜仁慕尼黑](../Page/拜仁慕尼黑.md "wikilink") 對
    [多蒙特](../Page/多蒙特.md "wikilink")
  - 德國足球聯賽 - [拜仁慕尼黑](../Page/拜仁慕尼黑.md "wikilink") 頒獎典禮 (2010)
  - 德甲精彩半世紀 (2013)
  - 德國足球聯賽精華 / 德甲最精彩
  - 英格蘭超級聯賽 (2004-07, 2010-13)

` 東北打比：`[`新特蘭`](../Page/新特蘭.md "wikilink")` 對 `[`紐卡素`](../Page/紐卡素.md "wikilink")
` `[`曼聯`](../Page/曼聯.md "wikilink")` 奪冠之戰`

  - 英格蘭超級聯賽精華 / 英超最精彩
  - 英超雜誌
  - 英格蘭聯賽盃決賽 - [車路士](../Page/車路士.md "wikilink") 對
    [利物浦](../Page/利物浦.md "wikilink") (2005)
  - 英格蘭足總盃
  - 歐洲聯賽冠軍盃 (2009-13)

` `[`阿仙奴`](../Page/阿仙奴.md "wikilink")` 對 `[`巴塞隆拿`](../Page/巴塞隆拿.md "wikilink")` `
` 現場報導：2013決賽 - `[`拜仁慕尼黑`](../Page/拜仁慕尼黑.md "wikilink")` 對 `[`多蒙特`](../Page/多蒙特.md "wikilink")` `

  - 歐聯最精彩
  - 歐聯雜誌
  - 歐霸盃足球聯賽 (2009-13)

` 緊貼`[`車路士比賽`](../Page/車路士.md "wikilink")
` 現場報導：2010決賽-`[`富咸`](../Page/富咸.md "wikilink")` 對 `[`馬德里體育會`](../Page/馬德里體育會.md "wikilink")` `

  - 歐霸最精彩
  - 澳洲職業聯賽精華 / A-League最精彩
  - 西班牙足球聯賽 (2003-09)
  - 西班牙足球聯賽國家打比 - [巴塞隆拿](../Page/巴塞隆拿.md "wikilink") 對
    [皇家馬德里](../Page/皇家馬德里.md "wikilink")
  - 西班牙足球聯賽 - [巴塞隆拿](../Page/巴塞隆拿.md "wikilink") 頒獎典禮 (2009)
  - 西班牙足球聯賽精華 / 西甲最精彩
  - 世界足球雜誌
  - 歐洲足球雜誌
  - 亞洲足球新世紀(2002)
  - J-League
  - J-League得點放送
  - 中國超級聯賽
  - 歐洲國家盃(2004)
  - 女子世界盃

` 現場報導：2011分組賽-`[`日本`](../Page/日本.md "wikilink")` 對 `[`墨西哥`](../Page/墨西哥.md "wikilink")` `

  - 美洲國家盃
  - 倫敦奧運足球(2012)
  - 亞運最精彩 (2014)

## 工作

  - 香港有線體育評述員 (2001-13)
  - 自由身評述員 (2013--)

` `[`德國甲組聯賽`](../Page/德國甲組聯賽.md "wikilink")` (2015--)  `[`FOX``
 ``Sports`](../Page/FOX_Sports.md "wikilink")
` `[`香港超級聯賽`](../Page/香港超級聯賽.md "wikilink")` (2014-15)  `[`亞洲電視`](../Page/亞洲電視.md "wikilink")`  `
` `[`蘇格蘭超級聯賽`](../Page/蘇格蘭超級聯賽.md "wikilink")` (2013--)  `[`亞洲電視`](../Page/亞洲電視.md "wikilink")
` `[`法國甲組聯賽`](../Page/法國甲組聯賽.md "wikilink")` (2014)  `[`無線網絡電視`](../Page/無線網絡電視.md "wikilink")
` `[`球分`](../Page/球分.md "wikilink")` 嘉賓 (2013)  `[`now``
 ``101台`](../Page/now_101台.md "wikilink")` `
` `[`致勝館`](../Page/致勝館.md "wikilink")` 嘉賓 (2014)  `[`有線球彩台`](../Page/有線球彩台.md "wikilink")
` `[`世盃狂熱`](../Page/世盃狂熱.md "wikilink")` 主持 (2014) `[`新城知訊台`](../Page/新城知訊台.md "wikilink")`     `

  - [足球周刊](../Page/足球周刊.md "wikilink")，[全民媒體專欄作家](../Page/全民媒體.md "wikilink")。專欄亦曾見於[Milk](../Page/Milk.md "wikilink")
    , [明報](../Page/明報.md "wikilink")。
  - 雜誌"keymansoho (足球版圖)" 編輯 (2010-12)
  - 雜誌"keymansoho (足球版圖)" 專欄作家 (2010)
  - [香港賽馬會](../Page/香港賽馬會.md "wikilink") Channel JC 編輯 (2004-07)
  - 2008北京奧運 - [亞洲電視](../Page/亞洲電視.md "wikilink") 特約資料搜集 (2008)

## 個人資料

  - 姓名: 何柏霖

` 正寫為「栢」`

  - 英文名: Ryson

` fb.com/berlinhohk`

  - 國籍: 香港

## 香港足球評述員協會

2006年, 何柏霖加入全新同業組織[香港足球評述員協會](../Page/香港足球評述員協會.md "wikilink")，成為創會會員之一。
2012年，獲選入協會最新一屆執委會。 2014年，當選協會新一屆執委會主席。

## 外部連結

  -
  -
  - [何柏霖 - Blogger](http://hkrysonho.blogspot.hk/)

  -
  - [何柏霖二創港隊打氣歌 -
    守得夠](http://www.youtube.com/watch?v=2HBlHEtJ6ME&index=1&list=PL4gahV_AHY5o2VxNc3e0J9nfmihtt9Ql1/)

[何](../Category/香港主持人.md "wikilink")
[何](../Category/香港足球評述員.md "wikilink")
[Category:何姓](../Category/何姓.md "wikilink")
[Category:觀塘官立中學校友](../Category/觀塘官立中學校友.md "wikilink")