**何甘棠**（1866年9月16日—1950年），本名**何啟棠**，
字**棣生**，是[香港望族](../Page/香港望族.md "wikilink")[何啟東家族成員](../Page/何啟東家族.md "wikilink")，是富商[何東及](../Page/何啟東.md "wikilink")[何福的同母異父之弟](../Page/何啟福.md "wikilink")，何甘棠非荷蘭裔猶太人何仕文跟施娣所生的兒子，而是施娣与中國男子郭兴贤所生的兒子\[1\]，是纯粹华人一名，曾任[怡和洋行](../Page/怡和洋行.md "wikilink")[買辦等職](../Page/買辦.md "wikilink")，為香港著名商人及社會活動家。

## 生平

何甘棠早年就讀於香港[中央書院](../Page/皇仁書院.md "wikilink")。畢業後，在[渣打洋行經營保險](../Page/渣打洋行.md "wikilink")、糖業等生意。後在[中國內地及](../Page/中國.md "wikilink")[澳門](../Page/澳門.md "wikilink")、東南亞遍設商號，經營金融、糖業、花紗、煤炭、雜貨等業務。1911年，何甘棠辭去渣打洋行職務，十年後的1921年更將各地商號結束，專注於社會公益事業。

何甘棠曾多次出資襄助學校和醫院建設。1904年，香港疫症流行，何甘棠與[馮華川](../Page/馮華川.md "wikilink")、[劉鑄伯等創辦公立醫局](../Page/劉鑄伯.md "wikilink")，免費開診。1906年，出任[東華醫院理事會主席](../Page/東華醫院.md "wikilink")，創立[廣華醫院](../Page/廣華醫院.md "wikilink")。1908年，任職潔淨局時，極力為華人爭取自治自理權。1915年，創辦[聖約翰救傷隊香港分會](../Page/聖約翰救傷隊.md "wikilink")。職是之故，[英皇佐治五世曾分別在](../Page/英皇佐治五世.md "wikilink")1924年及1925年頒授他「聖約翰官佐勳銜」及「聖約翰爵士勳銜」。以表揚其對香港聖約翰救傷隊的創立及發展的貢獻。此外，1922年、1924年，香港政府擬任命何氏為[定例局議員](../Page/立法會.md "wikilink")，均辭不就。歷任太平局紳、保良局紳、團防局紳等職。

1927年，何甘棠與[容顯龍兩人在](../Page/容顯龍.md "wikilink")[港督](../Page/港督.md "wikilink")[金文泰爵士協商下](../Page/金文泰.md "wikilink")，成為由英國人所創立的[香港賽馬會之首批](../Page/香港賽馬會.md "wikilink")[華人會員](../Page/華人.md "wikilink")，當時賽馬會長期拒絕華人參與，此後亦漸漸為華人爭得了參賽資格。1942年2月10日，香港正值[日治時期](../Page/香港日治時期.md "wikilink")，何甘棠被委任為[香港競馬會之首任主席及賽事裁判](../Page/香港競馬會.md "wikilink")。翌年，何甘棠以年老為由辭任，戰後復稱香港賽馬會。

何甘棠一生曾四次獲得[國民政府授予的](../Page/國民政府.md "wikilink")[嘉禾勳章](../Page/嘉禾勳章.md "wikilink")。1924年，英國聖約翰救傷總會授予爵位獎章及慈善銀獎章。1928年，英國政府授予[OBE勳章](../Page/OBE.md "wikilink")。

## 家庭

[Hotung_boys.jpg](https://zh.wikipedia.org/wiki/File:Hotung_boys.jpg "fig:Hotung_boys.jpg")、[何東](../Page/何東.md "wikilink")、**何甘棠**，後排左起︰[何福](../Page/何啟福.md "wikilink")、[陳啟明](../Page/陳啟明_\(香港\).md "wikilink")、[羅長肇](../Page/羅長肇.md "wikilink")。\]\]

何甘棠有12名妻妾（不包括情婦），子女多達30餘人，其中較為著名者有四子[何世威](../Page/何世威.md "wikilink")，曾在政府供職，擔任[華人永遠墳場管理委員會秘書等職](../Page/華人永遠墳場管理委員會.md "wikilink")。此外，三女[何愛瑜是何甘棠跟一位居住在上海的情婦本身是欧亚混血儿的张琼仙所收養的歐亞](../Page/何愛瑜.md "wikilink")[混血兒](../Page/混血兒.md "wikilink")。何愛瑜是[李小龍生母](../Page/李小龍.md "wikilink")。當年何甘棠在家中開台看戲，李小龍的[父親](../Page/父親.md "wikilink")[李海泉是](../Page/李海泉.md "wikilink")[粵劇名伶](../Page/粵劇.md "wikilink")，因工作關係在[甘棠第邂逅何甘棠的女兒何愛瑜](../Page/甘棠第.md "wikilink")，及後李海泉與何愛瑜移居[美國](../Page/美國.md "wikilink")，生下[李振藩](../Page/李振藩.md "wikilink")，後稱「李小龍」。

依據大排行，何甘棠家族稱為**何五宅**，何甘棠則為之取名為**何昌善堂**。

  - 施娣
      - **[何啟棠](../Page/何啟棠.md "wikilink")（**何甘棠**）**,
        字**棣生**（施娣與中國男子郭兴贤所生）\[2\]
          - [何世傑](../Page/何世傑.md "wikilink")＝Winnie Choa
            （蔡立志女兒）、[董琰瓊](../Page/董琰瓊.md "wikilink")
              - [何鴻圖](../Page/何鴻圖.md "wikilink")
              - [何晴翠](../Page/何晴翠.md "wikilink") （Pansy）
              - [何鴻書](../Page/何鴻書.md "wikilink")
              - [何鴻基](../Page/何鴻基.md "wikilink")
              - [何晴霞](../Page/何晴霞.md "wikilink") （Lilian）
              - [何鴻業](../Page/何鴻業.md "wikilink") （John）
              - [何鴻惠](../Page/何鴻惠.md "wikilink")
              - [何鴻迪](../Page/何鴻迪.md "wikilink") （Richard）
              - [何晴笑](../Page/何晴笑.md "wikilink") （Wendy）
              - [何鴻亨](../Page/何鴻亨.md "wikilink")（全由[董琰瓊所出](../Page/董琰瓊.md "wikilink")）
          - [何世華](../Page/何世華.md "wikilink")＝[施瑞芳](../Page/施瑞芳.md "wikilink")（May，又名瑜芝）
              - [何晴熹](../Page/何晴熹.md "wikilink") （Connie）
              - [何晴灝](../Page/何晴灝.md "wikilink") （Gertrude）
          - [何世文](../Page/何世文.md "wikilink")
            ＝[鄧愛華](../Page/鄧愛華.md "wikilink")（Ivy）
              - [何晴麗](../Page/何晴麗.md "wikilink") （Rita）
              - [何鴻堯](../Page/何鴻堯.md "wikilink") （養子）
              - [何寶珠](../Page/何寶珠.md "wikilink") （養女）
          - [何世昌](../Page/何世昌.md "wikilink")＝[黎宗愉](../Page/黎宗愉.md "wikilink")
              - [何鴻發](../Page/何鴻發.md "wikilink") （Gerald）
              - [何晴暉](../Page/何晴暉.md "wikilink") （Jane）
          - [何世安](../Page/何世安.md "wikilink")
          - [何世樂](../Page/何世樂.md "wikilink")＝[趙慧真](../Page/趙慧真.md "wikilink")
              - [何鴻森](../Page/何鴻森.md "wikilink") （Peter）
              - [何鴻星](../Page/何鴻星.md "wikilink")
              - [何晴朗](../Page/何晴朗.md "wikilink")
          - [何世康](../Page/何世康.md "wikilink")
          - [何世健](../Page/何世健.md "wikilink")
          - [何世威](../Page/何世威.md "wikilink")＝[張意蓮](../Page/張意蓮.md "wikilink")
          - [何世勇](../Page/何世勇.md "wikilink")＝[麥碧嬋](../Page/麥碧嬋.md "wikilink")
              - [何耀明](../Page/何耀明.md "wikilink") （Simon）
              - [何慧敏](../Page/何慧敏.md "wikilink") （Vivien）
              - [何慧玲](../Page/何慧玲.md "wikilink") （Miranda）
              - [何慧姬](../Page/何慧姬.md "wikilink")
              - [何慧蘭](../Page/何慧蘭.md "wikilink")
          - [何世雄](../Page/何世雄.md "wikilink")＝[羅瑞英](../Page/羅瑞英.md "wikilink")
              - [何智慧](../Page/何智慧.md "wikilink") （Sabrina）
              - [何智穎](../Page/何智穎.md "wikilink") （Belinda）
              - [何智聰](../Page/何智聰.md "wikilink") （David）
              - [何智明](../Page/何智明.md "wikilink") （Christina）
          - [何世剛](../Page/何世剛.md "wikilink")＝[周式宜](../Page/周式宜.md "wikilink")
              - [何宏達](../Page/何宏達.md "wikilink") （Stanley）
              - [何宏恩](../Page/何宏恩.md "wikilink") （Stephen）
          - [何世強](../Page/何世強.md "wikilink")
          - [何世猛](../Page/何世猛.md "wikilink")＝[黃美芙](../Page/黃美芙.md "wikilink")
          - [何世烈](../Page/何世烈.md "wikilink")
          - [何世吉](../Page/何世吉.md "wikilink")
          - [何柏齡](../Page/何柏齡.md "wikilink")(Elizabeth)＝[謝家寶](../Page/謝家寶.md "wikilink")（Simon）
              - [謝德安](../Page/謝德安.md "wikilink")
                （Andrew）＝[何婉璋](../Page/何婉璋.md "wikilink")（Priscilla）
              - [謝珮賢](../Page/謝珮賢.md "wikilink") （Lucy）
              - [謝俏賢](../Page/謝俏賢.md "wikilink") （Agnes）
              - [謝慧賢](../Page/謝慧賢.md "wikilink") （Mary）
              - [謝瑞賢](../Page/謝瑞賢.md "wikilink") （Ann）
              - [謝琮賢](../Page/謝琮賢.md "wikilink") （Kitty）=
                [張榮冕](../Page/張榮冕.md "wikilink")
          - [何柏貞](../Page/何柏貞.md "wikilink")（Elsie）＝[蔡寶耀](../Page/蔡寶耀.md "wikilink")（蔡立志兒子,
            另有一妾董氏）
              - [蔡慧中](../Page/蔡慧中.md "wikilink")（Agnes）
              - [蔡慧媛](../Page/蔡慧媛.md "wikilink")（Mollie）
              - [蔡慧真](../Page/蔡慧真.md "wikilink")（Phyllis）
              - [蔡慧鴻](../Page/蔡慧鴻.md "wikilink")（Leatrice）
              - [蔡慧箴](../Page/蔡慧箴.md "wikilink")（Daisy，董氏所出）
              - [蔡永業](../Page/蔡永業.md "wikilink")（Gerald，董氏所出）
          - [何柏堅](../Page/何柏堅.md "wikilink")（Flora）＝[伍朝恩](../Page/伍朝恩.md "wikilink")
          - [何柏頤](../Page/何柏頤.md "wikilink")
          - [何柏韶](../Page/何柏韶.md "wikilink") （Pansy）
          - [何柏芳](../Page/何柏芳.md "wikilink")
            （Rebecca）＝[彭秀山](../Page/彭秀山.md "wikilink")（Peter）
              - [彭綺玲](../Page/彭綺玲.md "wikilink")（Jenny）
              - [彭婉玲](../Page/彭婉玲.md "wikilink")（Ruby）
              - [彭楚玲](../Page/彭楚玲.md "wikilink")（Winnie）
              - [彭翠玲](../Page/彭翠玲.md "wikilink")（Becky）
              - [彭福玲](../Page/彭福玲.md "wikilink")（Angela）
              - [彭建成](../Page/彭建成.md "wikilink")（Peter）
          - [何柏端](../Page/何柏端.md "wikilink") （Jeneive）
          - [何柏瑞](../Page/何柏瑞.md "wikilink")
            （Nancy）＝[林顯光](../Page/林顯光.md "wikilink")
          - [何柏嫦](../Page/何柏嫦.md "wikilink")
            （Rose）＝[馬煊洪](../Page/馬煊洪.md "wikilink")（Charles）
              - [馬寶珠](../Page/馬寶珠.md "wikilink")（Rosaline）
              - [馬寶洸](../Page/馬寶洸.md "wikilink")（Philip）
              - [馬寶姍](../Page/馬寶姍.md "wikilink")（Christina）
          - [何柏娥](../Page/何柏娥.md "wikilink")
            （Stella）＝[麥志顯](../Page/麥志顯.md "wikilink")（Peter）
              - [麥信安](../Page/麥信安.md "wikilink")（Anthony）
          - [何柏奔](../Page/何柏奔.md "wikilink")
          - [何柏顏](../Page/何柏顏.md "wikilink")
          - [何柏媛](../Page/何柏媛.md "wikilink")
            （Monica）＝[郭生材](../Page/郭生材.md "wikilink")（Philip）
              - [郭澍堅](../Page/郭澍堅.md "wikilink")（Eric）
              - [郭雅倩](../Page/郭雅倩.md "wikilink")（Serena）
          - [何柏蓉](../Page/何柏蓉.md "wikilink")（一名愛瑜，何甘棠與情妇本身是欧亚混血儿张琼仙所收養的歐亞混血兒）（1907年-1996年）＝[李海泉](../Page/李海泉.md "wikilink")
              - [李秋圓](../Page/李秋圓.md "wikilink") （Phoebe）
              - [李秋勤](../Page/李秋勤.md "wikilink") （Agnes）
              - [李忠琛](../Page/李忠琛.md "wikilink")
                (Peter，1939年-2008年)＝[林燕妮](../Page/林燕妮.md "wikilink")（離婚）、[張瑪莉](../Page/張瑪莉.md "wikilink")
                  - [李凱豪](../Page/李凱豪.md "wikilink")（[林燕妮所出](../Page/林燕妮.md "wikilink")）
                  - [李偉豪](../Page/李偉豪.md "wikilink")
                    （[張瑪莉所出](../Page/張瑪莉.md "wikilink")）
                  - [李珏頤](../Page/李珏頤.md "wikilink")
                    （[張瑪莉所出](../Page/張瑪莉.md "wikilink")）
              - [李振藩](../Page/李振藩.md "wikilink")（[李小龍](../Page/李小龍.md "wikilink"),
                Bruce Lee）＝蓮達
                  - [李國豪](../Page/李國豪_\(演員\).md "wikilink")
                  - [李香凝](../Page/李香凝.md "wikilink")
              - [李振輝](../Page/李振輝.md "wikilink") （Robert）＝
                [黎小斌](../Page/黎小斌.md "wikilink")（藝名[森森](../Page/森森.md "wikilink")，離婚）
                  - [李嘉豪](../Page/李嘉豪.md "wikilink")

## 甘棠第

[甘棠第之模型.JPG](https://zh.wikipedia.org/wiki/File:甘棠第之模型.JPG "fig:甘棠第之模型.JPG")

甘棠第為何甘棠住宅，始建於1914年。何甘棠去世後，何氏家族於1960年將之售予鄭氏家族，其後鄭氏又將其轉讓與[耶穌基督後期聖徒教會](../Page/耶穌基督後期聖徒教會.md "wikilink")。2002年，該教會擬將之清拆，於原址建立一座宗教暨教育中心。2004年，政府以港幣五千三百萬元向教會收購甘棠第，並耗資九千一百萬元作修葺，設立孫中山紀念館，介紹[孫中山的生平事蹟](../Page/孫中山.md "wikilink")、革命活動與及其香港的關係。紀念館於2006年底啟用，時值孫中山140週年誕辰。

## 參見

  - [何東家族](../Page/何東家族.md "wikilink")
  - [何東](../Page/何東.md "wikilink")
  - [何福](../Page/何啟福.md "wikilink")

## 參考資料

  - 網頁

<!-- end list -->

  - [成報:
    韋基舜感嘆香港政府錯配歷史](https://web.archive.org/web/20070311002137/http://www.singpao.com/20050222/local/678029.html)，曾與[古物古蹟辦事處在報章上筆戰](../Page/古物古蹟辦事處.md "wikilink")。
  - [文匯報:
    丁新豹介紹何與孫有淵源。](https://web.archive.org/web/20070312063116/http://www.wenweipo.com/news.phtml?news_id=HK0612050017&cat=003HK)
  - [甘棠第 -
    孫中山紀念館所在地](http://carolalbum.mysinablog.com/index.php?op=ViewArticle&articleId=407461)
  - [何愛瑜是何甘棠跟居住在上海的情婦张琼仙所收養的歐亞混血兒](http://gwulo.com/node/5214?page=1)

<!-- end list -->

  - 文獻

<!-- end list -->

  - 香港聖約翰救護機構：《聖約翰通訊》2004年9月號第六期

[Category:香港歷史](../Category/香港歷史.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港馬主](../Category/香港馬主.md "wikilink")
[Category:怡和](../Category/怡和.md "wikilink")
[H](../Category/OBE勳銜.md "wikilink")
[Category:皇仁書院校友](../Category/皇仁書院校友.md "wikilink")
[Category:何東家族](../Category/何東家族.md "wikilink")
[Category:東華三院主席](../Category/東華三院主席.md "wikilink")
[Category:香港慈善家](../Category/香港慈善家.md "wikilink")

1.  Tracing My Children's Lineage 何鸿銮 著

2.