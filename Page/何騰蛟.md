**何騰蛟**（），字**雲從**，[貴州](../Page/貴州.md "wikilink")[黎平人](../Page/黎平.md "wikilink")\[1\]。明末軍事人物。

著有《[明中湘王何騰蛟集](../Page/明中湘王何騰蛟集.md "wikilink")》一卷，[諡](../Page/諡.md "wikilink")**文烈**。

## 生平

### 天啟崇祯年间

騰蛟出身書香[門第](../Page/門第.md "wikilink")，祖父[何志清](../Page/何志清.md "wikilink")，[明](../Page/明.md "wikilink")[嘉靖貢生](../Page/嘉靖.md "wikilink")，父訓課極嚴，有一天問書不解，父怒舉石[硯擊騰蛟頭](../Page/硯.md "wikilink")，罵說：「子不受教，擊死無悔。」

明[天啟元年](../Page/天啟.md "wikilink")（1621年），中辛酉科[舉人](../Page/舉人.md "wikilink")。[崇禎年間](../Page/崇禎.md "wikilink")，被授與[南陽](../Page/南陽.md "wikilink")[知縣](../Page/知縣.md "wikilink")，吏治精敏\[2\]。後被拔擢為大興知縣，精敏如治南陽，遷郎署，擢鞏昌兵備副使。以善撫兵將，為秦督[洪承疇所推薦](../Page/洪承疇.md "wikilink")\[3\]。

崇禎十五年（1642年）春，奉命出任[湖北鄖陽兵備道](../Page/湖北.md "wikilink")。

崇禎十六年（1643年），[張獻忠攻陷](../Page/張獻忠.md "wikilink")[武昌](../Page/武昌.md "wikilink")，楚撫[王聚奎](../Page/王聚奎.md "wikilink")、[王揚基以失機論治](../Page/王揚基.md "wikilink")，廷臣視楚為戒途，無敢赴者。三月，由[史可法力薦](../Page/史可法.md "wikilink")，任右僉都[御史](../Page/御史.md "wikilink")，巡撫[湖廣](../Page/湖廣.md "wikilink")。

### 弘光隆武年间

弘光元年（1645年、順治二年）明将[左良玉从武昌起兵反叛南明进攻南京](../Page/左良玉.md "wikilink")，何騰蛟拒绝加入，进入湖南，聚集过去手下的僚属[堵胤锡](../Page/堵胤锡.md "wikilink")、[傅上瑞](../Page/傅上瑞.md "wikilink")、[严起恒](../Page/严起恒.md "wikilink")、[章旷](../Page/章旷.md "wikilink")、[周大启](../Page/周大启.md "wikilink")、[吴晋锡等](../Page/吴晋锡.md "wikilink")，招兵买马。何騰蛟給湖南地方官員寫信道：“新上為南陽故人，魚水之合，吾輩皆有緣也。”六月，清軍進入[南京](../Page/南京.md "wikilink")，[弘光朝覆滅](../Page/弘光.md "wikilink")。

五月[李自成戰死後](../Page/李自成.md "wikilink")，[大順軍餘部四散](../Page/大順.md "wikilink")。在清軍兩路追擊下，遂投何騰蛟，聯合抗清。大順軍另一支[李赤心](../Page/李赤心.md "wikilink")（李過）、[高一功部入荊南](../Page/高一功.md "wikilink")。何騰蛟陸續招募[左良玉餘部](../Page/左良玉.md "wikilink")[馬進忠](../Page/馬進忠.md "wikilink")、[張先璧](../Page/張先璧.md "wikilink")、[黃朝宣等將](../Page/黃朝宣.md "wikilink")。授黃朝宣、張先璧、[劉承先](../Page/劉承先.md "wikilink")、李赤心（李過）、[郝搖旗](../Page/郝搖旗.md "wikilink")(永忠)、[袁宗第](../Page/袁宗第.md "wikilink")、[王進才](../Page/王進才.md "wikilink")、[董英](../Page/董英.md "wikilink")、馬進忠、[馬士秀](../Page/馬士秀.md "wikilink")、[曹志建](../Page/曹志建.md "wikilink")、[王允成](../Page/王允成_\(南明\).md "wikilink")、[盧鼎等為](../Page/盧鼎.md "wikilink")[總兵](../Page/總兵.md "wikilink")，開鎮湖南，時稱“[十三鎮](../Page/十三鎮.md "wikilink")”。

隆武二年（1646年）春，清兵[勒克德渾進攻](../Page/勒克德渾.md "wikilink")[江西](../Page/江西.md "wikilink")，騰蛟領軍由[長沙出發](../Page/長沙.md "wikilink")。四月十四日清兵攻贛州，[楊廷麟死守半年](../Page/楊廷麟.md "wikilink")，何騰蛟坐視不救，僅派遣南安伯[郝永忠帶兵馬一萬余名經郴州入龍泉](../Page/郝永忠.md "wikilink")（今江西遂川縣），郝永忠從長沙出發，經過衡州，九月初二日至郴州，即觀望不前\[4\]。另一支部隊[張先璧部也在行至同江西接境的攸縣后就](../Page/張先璧.md "wikilink")“屯師不進”\[5\]。十月初三日，清軍大舉攻城，至四日深夜，清軍登城拆垛，蜂擁入城，城陴和巷戰死者如麻，[楊廷麟投清水塘自盡](../Page/楊廷麟.md "wikilink")，至黎明，清軍佔領贛州城。

后郝搖旗部敗清軍於[岳州](../Page/岳州.md "wikilink")、[藤溪](../Page/藤溪.md "wikilink")，抵住清军。

### 永曆年间

永曆元年（1647年）初，[孔有德](../Page/孔有德.md "wikilink")、[尚可喜](../Page/尚可喜.md "wikilink")、[耿仲明率清軍進攻湖南](../Page/耿仲明.md "wikilink")，三月陷岳州、湘陰，直指[長沙](../Page/長沙.md "wikilink")。抗清陣線發生分裂。湖南总督何腾蛟、总兵王进才撤退。

永曆二年（顺治五年、1648年）[永历帝在桂林](../Page/永历帝.md "wikilink")，加封腾蛟为太师、进爵为侯，子孙世袭。閏三月，明降將[金聲桓](../Page/金聲桓.md "wikilink")、[李成棟等在江西](../Page/李成棟.md "wikilink")、广东倒戈反清，清軍撤出湖南巩固武昌，抗清局面復振。何騰蛟趁勢反攻，取[全州](../Page/全州.md "wikilink")、[永州](../Page/永州.md "wikilink")、[衡州](../Page/衡州.md "wikilink")。大顺军余部[忠贞营在堵胤锡的领导下占领湖南大量州县](../Page/忠贞营.md "wikilink")，围困长沙。何騰蛟命堵胤锡率忠贞营离开，準備自己進攻長沙，然而久攻不下。內部紛爭又起。馬進忠西走[武岡](../Page/武岡.md "wikilink")，常德失防，湖南大亂，不可收拾。

永曆三年（1649年）各鎮各懷二心，局勢惡化，一月，騰蛟往長沙見忠贞营[李赤心](../Page/李赤心.md "wikilink")（李過），以少數兵力入守湘潭。二月，清兵破湘潭，俘騰蛟，騰蛟絕食七日，從容吟絕命詩：“天乎人事苦難留，眉鎖湘江水不流。煉石有心嗟一木，凌雲無計慰三洲。河山赤地風悲角，社稷懷人雨溢秋。盡瘁未能時已逝，年年鵑血染宗周。”同月，騰蛟[自縊於流水大埠橋邊](../Page/自縊.md "wikilink")，終年58歲。[永曆帝聞訊](../Page/永曆帝.md "wikilink")，三軍縞素痛哭，諡**文烈**。

## 歸處另說

二说，接《明史》“（李）勇，腾蛟旧部将也，率其卒罗拜，劝腾蛟降。腾蛟大叱，勇遂拥之去。绝食七日，乃杀之。”腾蛟实未死，部份旧部同其归隐四川顺庆府（[张献忠战死遗址一带](../Page/张献忠.md "wikilink")），死后葬于灵龟山，有实坟，封号“龟山之人”。

## 評價

史家[顧誠則認為隆武之亡](../Page/顧誠.md "wikilink")，不能只歸咎於[鄭芝龍](../Page/鄭芝龍.md "wikilink")\[6\]，何騰蛟本人實難辭其咎。隆武帝知道鄭芝龍跋扈難制，希望何騰蛟派精兵迎駕，移蹕江西，“清軍進攻贛州，隆武帝進退維谷的時候。何騰蛟為了保住自己的權勢，竟然置大局于不顧，一連幾個月不派使者向隆武帝報告情況”，“隆武帝被清軍俘殺，閩、贛、粵相繼失陷，何騰蛟在湖南的處境也越來越困難。”\[7\]

## 注釋

## 參考書目

  - [王夫之](../Page/王夫之.md "wikilink")《永歷實錄》卷七《何騰蛟傳》
  - [顧誠](../Page/顧誠.md "wikilink")：《[南明史](../Page/南明史.md "wikilink")》第六節清軍占領贛南

[category:明朝軍事人物](../Page/category:明朝軍事人物.md "wikilink")
[category:南明人物](../Page/category:南明人物.md "wikilink")
[category:南明自殺人物](../Page/category:南明自殺人物.md "wikilink")
[T](../Page/category:何姓.md "wikilink")

[Category:天啓元年辛酉科舉人](../Category/天啓元年辛酉科舉人.md "wikilink")
[Category:明朝郡王](../Category/明朝郡王.md "wikilink")
[Category:諡文烈](../Category/諡文烈.md "wikilink")

1.  [王夫之](../Page/王夫之.md "wikilink")《永歷實錄》卷七《何騰蛟傳》：何騰蛟，字雲從，貴州黎平人。
2.  [王夫之](../Page/王夫之.md "wikilink")《永歷實錄》卷七《何騰蛟傳》：崇禎間，授南陽知縣，吏治精敏，以最聞。
3.  [王夫之](../Page/王夫之.md "wikilink")《永歷實錄》
4.  顧誠《南明史》第六節清軍占領贛南
5.  《永歷實錄》卷七《何騰蛟傳》
6.  [楊鳳苞稱](../Page/楊鳳苞.md "wikilink")“福京之亡，亡于鄭芝龍之通款”，見《[南疆繹史](../Page/南疆繹史.md "wikilink")》跋四
7.  《[南明史](../Page/南明史.md "wikilink")》第六節清軍占領贛南