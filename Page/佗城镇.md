**佗城镇**，又称**佗城**、**赵佗故城**，是[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[河源市](../Page/河源市.md "wikilink")[龙川县下辖的一个](../Page/龙川县.md "wikilink")[镇](../Page/镇.md "wikilink")，位于龙川县的南部。佗城镇是一个历史悠久的古城，广东最早的县城治所，被称「珠江东水开端，岭南古县第一」、[岭南](../Page/岭南.md "wikilink")[客家文明发祥地之一](../Page/客家.md "wikilink")。前214年，[秦始皇派兵平定](../Page/秦始皇.md "wikilink")[南越之后](../Page/南越.md "wikilink")，龙川县第一任[县令](../Page/县令.md "wikilink")[赵佗即在此筑城设立治所](../Page/赵佗.md "wikilink")，佗城也因赵佗而得名。直到1949年，佗城一直是龙川县的政治、经济和文化中心。佗城境内古迹众多，主要有[坑仔里遗址](../Page/坑仔里遗址.md "wikilink")、[正相塔](../Page/正相塔.md "wikilink")、[龙川学宫](../Page/龙川学宫.md "wikilink")、大成殿、[越王井](../Page/越王井.md "wikilink")、越王庙等。

佗城镇下辖17个村和1个社区，2004年，全镇人口为3.8万人。境内交通便利，[205国道](../Page/205国道.md "wikilink")、[梅河高速公路](../Page/梅河高速公路.md "wikilink")、[京九铁路和](../Page/京九铁路.md "wikilink")[广梅汕铁路穿越全镇](../Page/广梅汕铁路.md "wikilink")。

## 参考资料

  - [佗城镇](https://web.archive.org/web/20061208012056/http://www.gdhylc.gov.cn/xzfc_list.jsp?secFlag=080100&thFlag=080103&secName=%E4%B9%A1%E9%95%87%E9%A3%8E%E9%87%87&thName=%E4%BD%97%E5%9F%8E%E9%95%87)，龙川县政府网站。

[Category:龙川县行政区划](../Category/龙川县行政区划.md "wikilink")
[龙川县](../Category/河源建制镇.md "wikilink")
[Category:龙川旅游](../Category/龙川旅游.md "wikilink")
[Category:龙川县](../Category/龙川县.md "wikilink")
[Category:河源旅游](../Category/河源旅游.md "wikilink")