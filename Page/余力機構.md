**余力機構**是已解散的香港樂隊，成立於1997年。三位成員中，[陳輝陽仍活躍於作曲界](../Page/陳輝陽.md "wikilink")；；於1980－1990年代活躍的[因葵則已甚少推出詞作](../Page/因葵.md "wikilink")。

## 成員

  - 主音：[余力姬](../Page/余力姬.md "wikilink")
  - 作曲：[陳輝陽](../Page/陳輝陽.md "wikilink")
  - 作詞：[因葵](../Page/因葵.md "wikilink")

## 音樂作品

## 獎項

  - [1997年度叱咤樂壇生力軍組合金獎](../Page/1997年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink")

[Category:香港演唱團體](../Category/香港演唱團體.md "wikilink")
[Category:1997年成立的音樂團體](../Category/1997年成立的音樂團體.md "wikilink")
[Category:2000年解散的音樂團體](../Category/2000年解散的音樂團體.md "wikilink")