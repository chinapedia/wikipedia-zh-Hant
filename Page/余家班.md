**余家班**，[台灣](../Page/台灣.md "wikilink")[高雄縣著名政治家族](../Page/高雄縣.md "wikilink")-{余}-家的俗稱，其創始人為[余登發](../Page/余登發.md "wikilink")。在高雄地區屬於「黑派」（[鳳山](../Page/鳳山區.md "wikilink")）。主要對手派系有「白派」（[岡山](../Page/岡山區.md "wikilink")）、「紅派」（[旗山](../Page/旗山區.md "wikilink")），白紅兩派皆為[謝東閔在](../Page/謝東閔.md "wikilink")[高雄縣縣長任內所扶植](../Page/高雄縣縣長.md "wikilink")。

至2008年止，余家班成員在歷年選舉中，先後當選過6次高雄縣縣長、1次鄉長、8次[臺灣省議員](../Page/臺灣省議員.md "wikilink")、11次[立法委員及](../Page/立法委員.md "wikilink")2次[國民大會代表](../Page/國民大會代表.md "wikilink")，在台灣眾多政治世家中，可謂無出其右。

[2008年中華民國立法委員選舉](../Page/2008年中華民國立法委員選舉.md "wikilink")，余家班的[余政憲獲得](../Page/余政憲.md "wikilink")[民主進步黨提名在](../Page/民主進步黨.md "wikilink")[高雄縣第二選舉區](../Page/高雄縣.md "wikilink")（[茄萣鄉](../Page/茄萣區.md "wikilink")、[湖內鄉](../Page/湖內區.md "wikilink")、[路竹鄉](../Page/路竹區.md "wikilink")、[永安鄉](../Page/永安區.md "wikilink")、[彌陀鄉](../Page/彌陀區.md "wikilink")、[梓官鄉](../Page/梓官區.md "wikilink")、[岡山鎮](../Page/岡山區.md "wikilink")、[橋頭鄉](../Page/橋頭區.md "wikilink")）參選立法委員，最終以近2萬票之差距慘敗給前[行政院秘書長](../Page/立法院秘書長.md "wikilink")[林益世](../Page/林益世.md "wikilink")，令政壇頗為訝異。2012年7月，余政憲在民進黨中執委選舉中首度以零票落選。

## 余家班成員

第一代：

  - [余登發](../Page/余登發.md "wikilink")：曾任橋頭鄉鄉長，第一屆[國民大會代表](../Page/國民大會代表.md "wikilink")，第4屆[高雄縣縣長](../Page/高雄縣縣長.md "wikilink")（1960年—1963年）。

第二代：

  - [余陳月瑛](../Page/余陳月瑛.md "wikilink")：余登發之子[余瑞言之妻](../Page/余瑞言.md "wikilink")，曾任4屆省議員，1次增額[立法委員](../Page/立法委員.md "wikilink")，第10、11屆[高雄縣縣長](../Page/高雄縣縣長.md "wikilink")（1985年—1993年）。
  - [黃余秀鸞](../Page/黃余秀鸞.md "wikilink")：余登發女兒，曾任一次增額[立法委員](../Page/立法委員.md "wikilink")。
  - [黃友仁](../Page/黃友仁.md "wikilink")：余登發女婿，黃余秀鸞之夫，第8屆[高雄縣縣長](../Page/高雄縣縣長.md "wikilink")。

第三代：

  - [余政憲](../Page/余政憲.md "wikilink")：余瑞言與余陳月瑛之子，余登發長孫，曾任2次[立法委員](../Page/立法委員.md "wikilink")，第12、13屆[高雄縣縣長](../Page/高雄縣縣長.md "wikilink")（1993年—2001年），2002年—2004年擔任[內政部長](../Page/中華民國內政部.md "wikilink")。
  - [鄭貴蓮](../Page/鄭貴蓮.md "wikilink")：余政憲妻子，曾任1屆[國民大會代表](../Page/國民大會代表.md "wikilink")、1屆[立法委員](../Page/立法委員.md "wikilink")。
  - [余政道](../Page/余政道.md "wikilink")：余政憲之弟，曾任1屆省議員，4屆[立法委員](../Page/立法委員.md "wikilink")。
  - [余玲雅](../Page/余玲雅.md "wikilink")：余瑞言與余陳月瑛之女，政憲、政道之姊，曾任3屆[台灣省議員](../Page/台灣省.md "wikilink")、2屆[立法委員](../Page/立法委員.md "wikilink")。

## 家系圖

## 參見

  - [台灣地方派系](../Page/台灣地方派系.md "wikilink")

[\*](../Category/余家班.md "wikilink") [Y余](../Category/政治家族.md "wikilink")
[Category:台灣家族](../Category/台灣家族.md "wikilink")