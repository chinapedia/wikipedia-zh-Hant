**余文儀**（），[字](../Page/表字.md "wikilink")**叔子**，[號](../Page/號.md "wikilink")**寶岡**，[浙江](../Page/浙江.md "wikilink")[诸暨人](../Page/诸暨.md "wikilink")，[清朝政治人物](../Page/清朝.md "wikilink")，同[進士出身](../Page/進士.md "wikilink")。

## 生平

[乾隆二年](../Page/乾隆.md "wikilink")（1737年）登丁巳恩科[進士](../Page/進士.md "wikilink")，任[刑部](../Page/刑部.md "wikilink")[主事](../Page/主事.md "wikilink")。[乾隆二十年](../Page/乾隆.md "wikilink")（1755年）任[福建](../Page/福建.md "wikilink")[福寧府](../Page/福寧府.md "wikilink")[知府](../Page/知府.md "wikilink")，改[漳州府](../Page/漳州府.md "wikilink")[知府](../Page/知府.md "wikilink")。[乾隆二十六年](../Page/乾隆.md "wikilink")（1761年）任[台灣府知府](../Page/台灣府知府.md "wikilink")、[台灣府海防補盜同知](../Page/台灣府海防補盜同知.md "wikilink")、[台灣道](../Page/台灣道.md "wikilink")；任內續修《[臺灣府志](../Page/臺灣府志.md "wikilink")》，並鎮壓[黃教之役](../Page/黃教之役.md "wikilink")。[乾隆三十五年](../Page/乾隆.md "wikilink")（1770年）後，升[福建巡撫](../Page/福建巡撫.md "wikilink")、[刑部尚書等](../Page/刑部尚書.md "wikilink")。

## 參考文獻

  - 劉寧顏編，《重修台灣省通志》，[台北市](../Page/台北市.md "wikilink")，[台灣省文獻委員會](../Page/台灣省文獻委員會.md "wikilink")，1994年。
  - 張炳楠（監修），李汝和（主修），《台灣省通志》，[台灣省政府](../Page/台灣省政府.md "wikilink")，1961年。
  - [李騰嶽等](../Page/李騰嶽.md "wikilink")（監修），[王詩琅](../Page/王詩琅.md "wikilink")（纂修），1981年，《台灣省通志稿—人物志》，台北市，成文出版社。

[category:清朝福建巡抚](../Page/category:清朝福建巡抚.md "wikilink")
[category:诸暨人](../Page/category:诸暨人.md "wikilink")
[W](../Page/category:余姓.md "wikilink")

[Category:清朝刑部主事](../Category/清朝刑部主事.md "wikilink")
[Category:清朝福寧府知府](../Category/清朝福寧府知府.md "wikilink")
[Category:清朝漳州府知府](../Category/清朝漳州府知府.md "wikilink")
[Category:臺灣府知府](../Category/臺灣府知府.md "wikilink")
[Category:台灣府海防補盜同知](../Category/台灣府海防補盜同知.md "wikilink")
[Category:福建分巡台灣道](../Category/福建分巡台灣道.md "wikilink")
[Category:福建分巡台灣兵備道](../Category/福建分巡台灣兵備道.md "wikilink")
[Category:清朝刑部尚書](../Category/清朝刑部尚書.md "wikilink")