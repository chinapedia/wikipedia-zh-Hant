**佛法僧目**（[学名](../Page/学名.md "wikilink")：）是[鸟纲中的一个](../Page/鸟纲.md "wikilink")[目](../Page/目_\(生物\).md "wikilink")。这一目的鸟分布广泛，形态结构多样，各科特化程度高。

## 分布

佛法僧目的鸟在除两极和部分海洋岛屿外地区都有分布。很多科分布局限于[热带](../Page/热带.md "wikilink")、[亚热带地区](../Page/亚热带.md "wikilink")，其它科则分布比较广泛。其中鹃鴗科仅在[马达加斯加岛有分布](../Page/马达加斯加.md "wikilink")。

## 形态特征

佛法僧目的鸟形态多样，体形大小不一，[羽色大多艳丽](../Page/羽毛.md "wikilink")，[喙的形状多样](../Page/喙.md "wikilink")。腿短，足的前三趾基部有不同程度的并合，为并趾足。

## 习性

这一目的鸟生存环境多样，鸣声简单。多数种类以昆虫和小动物为食，有些种类食鱼，还有些种类食果实。繁殖于洞穴中，多是双亲育雏，雏鸟晚成型。

## 名稱由來

此名稱來自日本。夜間森林中“but po
so”（音似日語“佛法僧”）的鳥鳴聲，被認為是其中的一種[三寶鳥所發](../Page/三寶鳥.md "wikilink")，故名。1935年發現此鳥鳴聲實為[普通角鴞所發](../Page/普通角鴞.md "wikilink")。

## 分类

  - [翠鸟亚目](../Page/翠鸟亚目.md "wikilink") Alcedini
      - [翠鸟科](../Page/翠鸟科.md "wikilink") Alcedinidae
      - [翡翠科](../Page/翡翠科.md "wikilink") Halcyonidae
      - [短尾鴗科](../Page/短尾鴗科.md "wikilink") Todidae
      - [翠鴗科](../Page/翠鴗科.md "wikilink") Momotidae
  - [蜂虎亚目](../Page/蜂虎亚目.md "wikilink") Meropes
      - [蜂虎科](../Page/蜂虎科.md "wikilink") Meropidae
  - [佛法僧亚目](../Page/佛法僧亚目.md "wikilink") Coracii
      - [佛法僧科](../Page/佛法僧科.md "wikilink") Coraciidae

以前传统的形态学分类将[鹃鴗科](../Page/鹃鴗科.md "wikilink")（）、[戴胜科](../Page/戴胜科.md "wikilink")（Upupidae）、[林戴胜科](../Page/林戴胜科.md "wikilink")（Phoeniculidae）与[犀鸟科](../Page/犀鸟科.md "wikilink")（）归类于此，但遗传分子学研究显示，这四科与佛法僧科鸟类的亲缘关系甚至比[鴷形目更疏远](../Page/鴷形目.md "wikilink")，于是鹃鴗科独立列入[鹃鴗目](../Page/鹃鴗目.md "wikilink")，后三者则列入[犀鸟目](../Page/犀鸟目.md "wikilink")。

## 参考文献

  - 中国动物志 鸟纲 第七卷 科学出版社 2003年 ISBN 7-03-011418-3''

[\*](../Category/佛法僧目.md "wikilink")