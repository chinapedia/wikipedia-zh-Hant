Landscapes of Vác

` | photo2b = Kemence, Hungary - panoramio - Kaszás Tibor (4).jpg{{!}}Landscapes of Kemence`
` | spacing = 2`
` | position = center`
` | color_border = white`
` | color = white`
` | size = 255`
` | foot_montage = Descending, from top: the `[`Börzsöny``
 ``Mountain`](../Page/Börzsöny.md "wikilink")` in `[`Nagybörzsöny`](../Page/Nagybörzsöny.md "wikilink")` village, landscapes of `[`Vác`](../Page/Vác.md "wikilink")`, and landscapes of `[`Kemence`](../Page/Kemence.md "wikilink")`}}`

| image_flag = FLAG-Pest-megye.svg | image_shield = HUN Pest megye
COA.png | map_alt = | map_caption = Pest County within Hungary |
subdivision_type =
[Country](../Page/List_of_sovereign_states.md "wikilink") |
subdivision_name =  | subdivision_type1 =
[Region](../Page/Regions_of_Hungary.md "wikilink") | subdivision_name1
= [Central Hungary](../Page/Central_Hungary.md "wikilink") | seat_type
= County seat | seat = [Budapest](../Page/Budapest.md "wikilink") |
parts_type = [Districts](../Page/Districts_of_Hungary.md "wikilink") |
parts = 18 districts | p1 = [Aszód
District](../Page/Aszód_District.md "wikilink") | p2 = [Budakeszi
District](../Page/Budakeszi_District.md "wikilink") | p3 = [Cegléd
District](../Page/Cegléd_District.md "wikilink") | p4 = [Dabas
District](../Page/Dabas_District.md "wikilink") | p5 = [Dunakeszi
District](../Page/Dunakeszi_District.md "wikilink") | p6 = [Érd
District](../Page/Érd_District.md "wikilink") | p7 = [Gödöllő
District](../Page/Gödöllő_District.md "wikilink") | p8 = [Gyál
District](../Page/Gyál_District.md "wikilink") | p9 = [Monor
District](../Page/Monor_District.md "wikilink") | p10= [Nagykáta
District](../Page/Nagykáta_District.md "wikilink") | p11= [Nagykőrös
District](../Page/Nagykőrös_District.md "wikilink") | p12=
[Pilisvörösvár
District](../Page/Pilisvörösvár_District.md "wikilink") | p13=
[Ráckeve District](../Page/Ráckeve_District.md "wikilink") | p14=
[Szentendre District](../Page/Szentendre_District.md "wikilink") | p15=
| p16= [Szob District](../Page/Szob_District.md "wikilink") | p17= [Vác
District](../Page/Vác_District.md "wikilink") | p18= [Vecsés
District](../Page/Vecsés_District.md "wikilink") | government_footnotes
= | leader_title = President of the General Assembly | leader_name =
István Szabó | leader_party =
[Fidesz](../Page/Fidesz.md "wikilink")-[KDNP](../Page/Christian_Democratic_People's_Party_\(Hungary\).md "wikilink")
| area_total_km2 = 6393.14 | area_rank = [3rd in
Hungary](../Page/Counties_of_Hungary.md "wikilink") | population_as_of
= 2015 | population_total = 1226115\[1\] | population_rank = [1st in
Hungary](../Page/Counties_of_Hungary.md "wikilink") |
population_density_km2 = auto | image_map = HU county Pest.svg |
postal_code_type = [Postal
code](../Page/Postal_codes_in_Hungary.md "wikilink") | postal_code =
20xx – 23xx, 2440, 2461, 260x – 263x,
2680 – 2683, 27xx | area_code_type = [Area
code(s)](../Page/Telephone_numbers_in_Hungary.md "wikilink") |
area_code = (+36) 23, 24, 26, 27, 28, 29, 53 | iso_code =  |website =
}}
**佩斯州**（[匈牙利語](../Page/匈牙利語.md "wikilink")：****）是[匈牙利中部的一個](../Page/匈牙利.md "wikilink")[州](../Page/匈牙利行政區劃.md "wikilink")，位於首都[布達佩斯周圍](../Page/布達佩斯.md "wikilink")，北界[斯洛伐克](../Page/斯洛伐克.md "wikilink")。面積6,394平方公里，人口1,077,300（2004年）。首府[布達佩斯](../Page/布達佩斯.md "wikilink")。

下設1市、40鎮、145村。

[\*](../Category/佩斯州.md "wikilink")
[Category:匈牙利州級行政區](../Category/匈牙利州級行政區.md "wikilink")

1.  [nepesseg.com, population data of Hungarian
    settlements](http://nepesseg.com/pest/)