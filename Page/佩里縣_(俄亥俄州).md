**佩里县**（**Perry County,
Ohio**）是[美國](../Page/美國.md "wikilink")[俄亥俄州東部的一個縣](../Page/俄亥俄州.md "wikilink")。面積1,069平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口34,078人。縣治[新列克星敦](../Page/新列克星敦.md "wikilink")
(New Lexington)。

成立於1817年12月26日。縣名紀念[1812年戰爭英雄](../Page/1812年戰爭.md "wikilink")[奧利弗·哈澤德·佩里](../Page/奧利弗·哈澤德·佩里.md "wikilink")。

## 參考文獻

[P](../Category/俄亥俄州行政區劃.md "wikilink")
[Category:阿巴拉契亚地区的县](../Category/阿巴拉契亚地区的县.md "wikilink")