## FD接環

## 新FD鏡頭

### [定焦鏡頭](../Page/定焦鏡頭.md "wikilink")

#### [廣角鏡頭](../Page/廣角鏡頭.md "wikilink")

  - [FD 7.5mm f/5.6
    Fisheye](../Page/佳能_FD_7.5mm_f/5.6_Fishey鏡頭.md "wikilink")
  - [FD 14mm f/2.8L](../Page/佳能_FD_14mm鏡頭.md "wikilink")
  - [FD 15mm f/2.8](../Page/佳能_FD_15mm鏡頭.md "wikilink")
  - [FD 17mm f/4](../Page/佳能_FD_17mm鏡頭.md "wikilink")
  - [FD 20mm f/2.8](../Page/佳能_FD_20mm鏡頭.md "wikilink")
  - [FD 24mm f/2](../Page/佳能_FD_24mm鏡頭.md "wikilink")
  - [FD 24mm f/1.4L](../Page/佳能_FD_24mm鏡頭.md "wikilink")
  - [FD 28mm f/2](../Page/佳能_FD_28m鏡頭.md "wikilink")
  - [FD 28mm f/2.8](../Page/佳能_FD_28mm鏡頭.md "wikilink")
  - [FD 35mm 2](../Page/佳能_FD_35m鏡頭.md "wikilink")
  - [FD 35mm 2 I](../Page/佳能_FD_35m鏡頭.md "wikilink")
  - [FD 35mm 2 II](../Page/佳能_FD_35m鏡頭.md "wikilink")
  - [FD 35mm 2 III](../Page/佳能_FD_35m鏡頭.md "wikilink")
  - [FD 35mm 2 S.S.C. I](../Page/佳能_FD_35m鏡頭.md "wikilink")
  - [FD 35mm 2 S.S.C. II](../Page/佳能_FD_35m鏡頭.md "wikilink")
  - [FD 35mm f/2.8](../Page/佳能_FD_35m鏡頭.md "wikilink")

#### [標準鏡頭](../Page/標準鏡頭.md "wikilink")

  - [FD 50mm f/2](../Page/佳能_FD_50mm_鏡頭.md "wikilink")
  - [FD 50mm f/1.8](../Page/佳能_FD_50mm_鏡頭.md "wikilink")
  - [FD 50mm f/1.4](../Page/佳能_FD_50mm_鏡頭.md "wikilink")
  - [FD 50mm f/1.2](../Page/佳能_FD_50mm_鏡頭.md "wikilink")
  - [FD 50mm f/1.2L](../Page/佳能_FD_50mm_鏡頭.md "wikilink")
  - [FD 55mm f/1.2 AL](../Page/佳能_FD_55mm_鏡頭.md "wikilink")

### [中距離鏡頭](../Page/中距離鏡頭.md "wikilink")

  - [FD 85mm f/2.8 Soft Focus](../Page/佳能_FD_85mm_鏡頭.md "wikilink")
  - [FD 85mm f/1.8](../Page/佳能_FD_85mm_鏡頭.md "wikilink")
  - [FD 85mm f/1.2L](../Page/佳能_FD_85mm_鏡頭.md "wikilink")
  - [FD 100mm f/2.8](../Page/佳能_FD_100mm_鏡頭.md "wikilink")
  - [FD 100mm f/2](../Page/佳能_FD_100mm_鏡頭.md "wikilink")

### [遠攝鏡頭](../Page/遠攝鏡頭.md "wikilink")

  - [FD 135mm f/2](../Page/佳能_FD_135mm_鏡頭.md "wikilink")
  - [FD 135mm f/2.8](../Page/佳能_FD_135mm_鏡頭.md "wikilink")
  - [FD 135mm f/3.5](../Page/佳能_FD_135mm_鏡頭.md "wikilink")
  - [FD 200mm f/4](../Page/佳能_FD_200mm_鏡頭.md "wikilink")
  - [FD 200mm f/2.8](../Page/佳能_FD_200mm_鏡頭_鏡頭.md "wikilink")
  - [FD 200mm f/1.8L](../Page/佳能_FD_200mm_鏡頭_鏡頭.md "wikilink")

#### [超遠攝鏡頭](../Page/超遠攝鏡頭.md "wikilink")

  - [FD 300mm f/2.8L](../Page/佳能_FD_300mm_鏡頭.md "wikilink")
  - [FD 300mm f/4L](../Page/佳能_FD_300mm_鏡頭.md "wikilink")
  - [FD 300mm f/4](../Page/佳能_FD_300mm_鏡頭.md "wikilink")
  - [FD 300mm f/5.6](../Page/佳能_FD_300mm_鏡頭.md "wikilink")
  - [FD 400mm f/4.5](../Page/佳能_FD_400mm_鏡頭.md "wikilink")
  - [FD 400mm f/2.8L](../Page/佳能_FD_400mm_鏡頭.md "wikilink")
  - [FD 500mm f/4.5L](../Page/佳能_FD_500mm_鏡頭.md "wikilink")
  - [Reflex 500mm f/8](../Page/佳能Reflex_500mm_鏡頭.md "wikilink")
  - [FD 600mm f/4.5](../Page/佳能_FD_600mm_鏡頭.md "wikilink")
  - [FD 800mm f/5.6L](../Page/佳能_FD_800mm_鏡頭.md "wikilink")

#### [微距鏡頭](../Page/微距鏡頭.md "wikilink")

  - [FD 50mm f/3.5 Macro](../Page/佳能_FD_50mm_微距鏡頭.md "wikilink")
  - [FD 100mm f/4 Macro](../Page/佳能_FD_100mm_微距鏡頭.md "wikilink")
  - [FD 200mm f/4 Macro](../Page/佳能_FD_200mm_微距鏡頭.md "wikilink")

#### 其他鏡頭

  - [TS 35mm f/2.8](../Page/佳能_TS_35mm_f/2.8.md "wikilink")

### [變焦鏡頭](../Page/變焦鏡頭.md "wikilink")

[Canon_FD_lens_35-70mm_f3.5-4.5.jpg](https://zh.wikipedia.org/wiki/File:Canon_FD_lens_35-70mm_f3.5-4.5.jpg "fig:Canon_FD_lens_35-70mm_f3.5-4.5.jpg")

  - [FD 20-35mm f/3.5L](../Page/佳能_FD_20-35mm_鏡頭.md "wikilink")
  - [FD 24-35mm f/3.5L](../Page/佳能_FD_24-35mm_鏡頭.md "wikilink")
  - [FD 28-50mm f/3.5](../Page/佳能_FD_28-50mm_鏡頭.md "wikilink")
  - [FD 28-55mm f/3.5-4.5](../Page/佳能_FD_28-55mm_鏡頭.md "wikilink")
  - [FD 28-85mm f/4](../Page/佳能_FD_28-85mm_鏡頭.md "wikilink")
  - [FD 35-70mm f/3.5-4.5](../Page/佳能_FD_35-70mm_鏡頭.md "wikilink")
  - [FD 35-70mm f/2.8-3.5](../Page/佳能_FD_35-70mm_鏡頭.md "wikilink")
  - [FD 35-70mm f/4](../Page/佳能_FD_35-70mm_鏡頭.md "wikilink")
  - [FD 35-105mm f/3.5](../Page/佳能_FD_35-105mm_鏡頭.md "wikilink")
  - [FD 35-105mm f/3.5-4.5](../Page/佳能_FD_35-105mm_鏡頭.md "wikilink")
  - [FD 50-135mm f/3.5](../Page/佳能_FD_50-135mm_鏡頭.md "wikilink")
  - [FD 50-300mm f/4.5L](../Page/佳能_FD_50-300mm_f/4.5_鏡頭.md "wikilink")
  - [FD 70-150mm f/4.5](../Page/佳能_FD_70-150mm_鏡頭.md "wikilink")
  - [FD 70-210mm f/4](../Page/佳能_FD_70-210mm_鏡頭.md "wikilink")
  - [FD 80-200mm f/4](../Page/佳能_FD_80-200mm_鏡頭.md "wikilink")
  - [FD 80-200mm f/4L](../Page/佳能_FD_80-200mm_鏡頭.md "wikilink")

## 關連條目

  - [佳能產品列表](../Page/佳能產品列表.md "wikilink")
  - [佳能EF接環鏡頭](../Page/佳能EF接環鏡頭.md "wikilink")
  - [佳能EF-S接環鏡頭](../Page/佳能EF-S接環鏡頭.md "wikilink")

[FD](../Page/category:佳能鏡頭.md "wikilink")

[Category:鏡頭接環](../Category/鏡頭接環.md "wikilink")