**佳能 Digital IXUS 500**（在北美市场称作**PowerShot S500**，日本市场则称为**IXY Digital
500**）

**IXUS 500**与同时推出的[IXUS
430为之前受好评的](../Page/Canon_Digital_IXUS_430.md "wikilink")[Canon
Digital IXUS
400的升级版](../Page/Canon_Digital_IXUS_400.md "wikilink")。**IXUS
500**的继任者是[Canon Digital IXUS
700](../Page/Canon_Digital_IXUS_700.md "wikilink")。

## 主要参数

  - 500 万有效[像素](../Page/像素.md "wikilink")
  - 3倍光学变焦
  - 1/1.8 英寸 [CCD](../Page/CCD.md "wikilink")
  - 1.5寸可旋转液晶屏，分辨率11.8万象素
  - 快门：15～1/2000秒
  - [ISO](../Page/ISO.md "wikilink")：50/100/200/400
  - 9点智能AiAF对焦／中央单点
  - 有声短片记录（[Motion JPEG编码与单声道音频](../Page/Motion_JPEG.md "wikilink")）
  - 使用[CF卡作为存储介质](../Page/CF卡.md "wikilink")
  - 使用[DIGIC数字处理芯片](../Page/DIGIC.md "wikilink")
  - 使用内置NB-1LH[锂离子电池](../Page/锂离子电池.md "wikilink")
  - [Exif 2.2](../Page/EXIF.md "wikilink")
  - 尺寸 87 x 57 x 28 mm
  - 不带电池约185克

## CCD问题

部分**Digital IXUS
500**相机在高温多湿的使用环境下，[CCD可能出现故障](../Page/CCD.md "wikilink")，使得无论液晶屏或者保存的图像都无法正确显示。佳能公司对这一部分的产品实行免费维修。

## IXUS 500拍摄照片

<File:United> States Postal Service Truck.jpg <File:San> Francisco
Public Library Main Branch Facade.jpg <File:San> Francisco Parking
Attendant Vehicle.jpg <File:Amoeba> Music San Francisco Facade.jpg

## 参见

  - [Canon Digital IXUS](../Page/Canon_Digital_IXUS.md "wikilink")
  - [Canon Digital IXUS
    430](../Page/Canon_Digital_IXUS_430.md "wikilink")
  - [Canon Digital IXUS
    400](../Page/Canon_Digital_IXUS_400.md "wikilink")
  - [Canon Digital IXUS
    700](../Page/Canon_Digital_IXUS_700.md "wikilink")

## 外部链接

  - [佳能（中国）](http://www.canon.com.cn)
  - [数码照相机CCD问题对象机型的追加](https://web.archive.org/web/20070222004926/http://www.canon.com.cn/front/service/announce/ann.jsp?id=42)－佳能（中国）

[en:Canon DIGITAL IXUS
500](../Page/en:Canon_DIGITAL_IXUS_500.md "wikilink")

[Category:佳能相機](../Category/佳能相機.md "wikilink")