[Canon_PowerShot_A610.jpg](https://zh.wikipedia.org/wiki/File:Canon_PowerShot_A610.jpg "fig:Canon_PowerShot_A610.jpg")

**佳能 PowerShot
A610**是一款[佳能推出的](../Page/佳能.md "wikilink")[数码相机](../Page/数码相机.md "wikilink")，属于[Canon
PowerShot系列](../Page/Canon_PowerShot.md "wikilink")，于[2005年8月发布](../Page/2005年8月.md "wikilink")。

与**A610**同时发布的还有[Canon PowerShot
A620](../Page/Canon_PowerShot_A620.md "wikilink")，功能基本一致，像素较A620略低。

**PowerShot A610**/**PowerShot A620**是[Canon PowerShot
A95的继任者](../Page/Canon_PowerShot_A95.md "wikilink")。

## 主要性能参数

  - 5 百万 有效[象素](../Page/象素.md "wikilink")
  - 4倍光学[变焦](../Page/变焦.md "wikilink")
  - 1/1.8 英寸 [CCD](../Page/CCD.md "wikilink")
  - 2.0寸可旋转TFT液晶屏，分辨率11.5万象素
  - 快门：15～1/2500秒
  - [ISO](../Page/ISO.md "wikilink")：50/100/200/400
  - 9点智能[对焦](../Page/对焦.md "wikilink")
  - 有声短片记录（[Motion JPEG编码与单声道音频](../Page/Motion_JPEG.md "wikilink")）
  - 使用[SD卡](../Page/SD卡.md "wikilink")／[MMC卡作为存储介质](../Page/MMC卡.md "wikilink")
  - 使用[DIGIC II数字处理芯片](../Page/DIGIC.md "wikilink")
  - 使用4节[AA电池](../Page/AA电池.md "wikilink")
  - [Exif 2.2](../Page/EXIF.md "wikilink")
  - 不带电池约235克

## 改机

佳能PowerShot A6x0系列拥有较大尺寸的CCD，为优质成像提供了保证。有爱好者研究出对 A6x0 及[S2
IS与](../Page/Canon_PowerShot_S2_IS.md "wikilink")[S3
IS进行修改而使其能输出](../Page/Canon_PowerShot_S3_IS.md "wikilink")[RAW格式图像的方法](../Page/RAW.md "wikilink")。这使A6x0，特别是其中最便宜的A610变得更加具有吸引力。

## 参见

  - [Canon PowerShot](../Page/Canon_PowerShot.md "wikilink")
  - [Canon PowerShot A620](../Page/Canon_PowerShot_A620.md "wikilink")
  - [Canon PowerShot A95](../Page/Canon_PowerShot_A95.md "wikilink")

## 外部链接

  - [佳能（中国）](http://www.canon.com.cn)
  - [谁是性价之王？佳能A610相机评测](http://lcd.itdoor.net/pages/132,34564,2,1133758984.html)
    - 倚天
  - [A6x0、S3IS破解](http://www.xitek.com/forum/showthread.php?threadid=418343)
    - 色影无忌上转载的破解方法及讨论

[Category:佳能相機](../Category/佳能相機.md "wikilink")