**佳能 PowerShot
A620**是一款[佳能出品的](../Page/佳能.md "wikilink")[数码相机](../Page/数码相机.md "wikilink")，属于[Canon
PowerShot系列](../Page/Canon_PowerShot.md "wikilink")，它于[2005年8月发布](../Page/2005年8月.md "wikilink")。

与Canon PowerShot A620同时发布的还有[Canon PowerShot
A610](../Page/Canon_PowerShot_A610.md "wikilink")，功能基本一致，像素比A620略低。

**PowerShot
A620**被期望取代[A95](../Page/Canon_PowerShot_A95.md "wikilink")。**A620**的继任者是[Canon
PowerShot A640](../Page/Canon_PowerShot_A640.md "wikilink")。

## 主要性能参数

[Canon_PowerShot_A620_(01327).jpg](https://zh.wikipedia.org/wiki/File:Canon_PowerShot_A620_\(01327\).jpg "fig:Canon_PowerShot_A620_(01327).jpg")

  - 7.1 百万 有效[象素](../Page/象素.md "wikilink")
  - 4倍光学[变焦](../Page/变焦.md "wikilink")
  - 1/1.8 英寸 [CCD](../Page/CCD.md "wikilink")
  - 2.0寸可旋转TFT液晶屏，分辨率11.5万象素
  - 快门：15～1/2500秒
  - [ISO](../Page/ISO.md "wikilink")：50/100/200/400
  - 9点智能[对焦](../Page/对焦.md "wikilink")
  - 有声短片记录（[Motion JPEG编码与单声道音频](../Page/Motion_JPEG.md "wikilink")）
  - 使用[SD卡](../Page/SD卡.md "wikilink")／[MMC卡作为存储介质](../Page/MMC卡.md "wikilink")
  - 使用[DIGIC II数字处理芯片](../Page/DIGIC.md "wikilink")
  - 使用4节[AA电池](../Page/AA电池.md "wikilink")
  - [Exif 2.2](../Page/EXIF.md "wikilink")
  - 不带电池约235克

## 改机

佳能PowerShot A6x0系列拥有较大尺寸的CCD，为优质成像提供了保证。有爱好者研究出对 A6x0 及[S2
IS与](../Page/Canon_PowerShot_S2_IS.md "wikilink")[S3
IS进行修改而使其能输出](../Page/Canon_PowerShot_S3_IS.md "wikilink")[RAW格式图像的方法](../Page/RAW.md "wikilink")。这使A6x0变得更加具有吸引力。

## 参见

  - [Canon PowerShot](../Page/Canon_PowerShot.md "wikilink")
  - [Canon PowerShot A610](../Page/Canon_PowerShot_A610.md "wikilink")
  - [Canon PowerShot A95](../Page/Canon_PowerShot_A95.md "wikilink")
  - [Canon PowerShot A640](../Page/Canon_PowerShot_A640.md "wikilink")

## 外部链接

  - [佳能（中国）](http://www.canon.com.cn)
  - [A6x0、S3IS破解](http://www.xitek.com/forum/showthread.php?threadid=418343)
    - 色影无忌上转载的破解方法及讨论

[Category:佳能數位相機](../Category/佳能數位相機.md "wikilink")