**佳能 PowerShot A650
IS**是一款[佳能](../Page/佳能.md "wikilink")[数码相机](../Page/数码相机.md "wikilink")，于[2007年8月推出](../Page/2007年8月.md "wikilink")。

A650 IS继承了[Canon PowerShot
A640的优点](../Page/Canon_PowerShot_A640.md "wikilink")，并且加入了在消费级市场日益受到关注的影像稳定器（IS）。A650
IS作为PowerShot A系列拥有最大面积CCD的PowerShot A
6x0系列相机的最新机种，也成为了佳能在消费类数码相机市场的事实旗舰机种。

2007年9月－10月，部分A650 IS出现质量问题，连带影响了正常A650 IS的销售，几乎一夜之间PowerShot A650
IS消失在市面上。

## 主要参数

  - 1210万 有效[象素](../Page/象素.md "wikilink")
  - 6倍光学[变焦](../Page/变焦.md "wikilink")
  - 1/1.7 英寸 [CCD](../Page/CCD.md "wikilink")
  - 2.5寸可旋转液晶屏，分辨率17.3万象素
  - 快门：15～1/2000秒
  - [ISO](../Page/ISO.md "wikilink")：80/100/200/400/800/1600
  - 9点智能AiAF对焦／面部优先／中央单点
  - 有声短片记录（[Motion JPEG编码与单声道音频](../Page/Motion_JPEG.md "wikilink")）
  - 使用[SDHC卡](../Page/SDHC卡.md "wikilink")／[SD卡](../Page/SD卡.md "wikilink")／[MMC卡作为存储介质](../Page/MMC卡.md "wikilink")
  - 使用[DIGIC III数字处理芯片](../Page/DIGIC.md "wikilink")
  - 使用4节AA电池
  - [Exif 2.2](../Page/EXIF.md "wikilink")
  - 不带电池约300克

## 退市

2007年9月－10月，部分已经售出的**Canon PowerShot A650 IS**出现质量问题。

发生问题的相机编号第5个数字为“0”。佳能公司对这一部分的相机进行回收，已售出的则给予免费维修。事件发生后，市面上所有的**Canon
PowerShot A650 IS**都不见了。A650 IS也许就此黯淡退出市场。

## 参见

  - [Canon PowerShot](../Page/Canon_PowerShot.md "wikilink")
  - [Canon PowerShot A710
    IS](../Page/Canon_PowerShot_A710_IS.md "wikilink")
  - [Canon PowerShot A570
    IS](../Page/Canon_PowerShot_A570_IS.md "wikilink")
  - [Canon PowerShot A720
    IS](../Page/Canon_PowerShot_A720_IS.md "wikilink")

## 外部链接

  - [佳能（中国）](http://www.canon.com.cn)
  - [PowerShot](https://web.archive.org/web/20071215103605/http://www.canon.com.cn/front/product/product_main.jsp?id=2416&content=spec_attache_consume&type=spec)－佳能（中国）
  - [A650
    IS上市](http://www.pconline.com.cn/digital/dc/hangqing/hk/0709/1114150.html)－PConline
  - [佳能公布A650问题相机序号，免费维修](http://news.mydrivers.com/1/92/92383.htm)－驱动之家
  - [佳能A650相机问题 国内已经开始召回](http://www.17tech.com/news/36756.shtml)－17TECH

[pl:Canon PowerShot A650
IS](../Page/pl:Canon_PowerShot_A650_IS.md "wikilink")

[Category:佳能相機](../Category/佳能相機.md "wikilink")