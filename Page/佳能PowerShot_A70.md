**佳能 PowerShot
A70**是一款[佳能相机](../Page/佳能.md "wikilink")，于[2003年3月推出](../Page/2003年3月.md "wikilink")。原本市场定位为取代之前的[A30](../Page/Canon_PowerShot_A30.md "wikilink")、[A40](../Page/Canon_PowerShot_A40.md "wikilink")。

这款相机借鉴了PowerShot
G系列的一些成功经验并加以应用，如加入光圈优先、快门优先、全手动等拍摄模式，在爱好者中得到了“手动王”的美誉。相对低廉的价格（中国大陆时价约三千[人民币](../Page/人民币.md "wikilink")）及其所提供的可以媲美高端机型品质的画面，赢得了消费者的青睐，成为佳能占据市场的主力机型。

市场地位被其后一年的[PowerShot
A75所取代](../Page/Canon_PowerShot_A75.md "wikilink")，A75也继承了“手动王”的称号。但因为A75相对A70改动较小，有时把这两款相机一同看待。

该机型有着此系列的[E18错误问题](../Page/E18错误.md "wikilink")。

后来由于[CCD问题](../Page/CCD.md "wikilink")，佳能在全球范围进行召回并且提供免费更换服务。

## 主要参数

  - 三百万有效[象素](../Page/象素.md "wikilink")
  - 3倍光学[变焦](../Page/变焦.md "wikilink")
  - 1/2.7 英寸 [CCD](../Page/CCD.md "wikilink")
  - 1.5寸TFT液晶屏，分辨率11.8万象素。
  - 5点智能[对焦](../Page/对焦.md "wikilink")
  - 有声短片记录（[MJPEG编码与raw音频](../Page/MJPEG.md "wikilink")）
  - 使用[CF卡作为存储介质](../Page/CF卡.md "wikilink")
  - 使用[DIGIC数字处理芯片](../Page/DIGIC.md "wikilink")
  - 使用4节[AA电池](../Page/AA电池.md "wikilink")

## 常见故障

  - CCD，紫斑

佳能所使用的[索尼产CCD在高温多湿的环境下使用可能导致所拍照片有紫斑](../Page/索尼.md "wikilink")，甚至令相機無法拍攝。佳能对此进行了全球范围的召回，并进行免费维修。

  - [E18错误](../Page/E18错误.md "wikilink")

当用户的相机镜头伸出或退回受到阻挡，比较可能的情况就是相机被放置在拥挤狭小的环境中，无意地碰到了开机键，相机产生**E18错误**。不同于CCD问题，佳能拒绝对发生此问题的相机进行免费维修。高额的维修费用和较高的故障率备受用户诟病。

## 参见

  - [Canon PowerShot](../Page/Canon_PowerShot.md "wikilink")
  - [E18错误](../Page/E18错误.md "wikilink")

## 外部链接

  - [佳能相机博物馆 PowerShot
    A70](https://web.archive.org/web/20070203045540/http://www.canon.com/camera-museum/camera/digital/data/2003_ps-a70.html)
  - [PowerShot
    A70详细参数](https://web.archive.org/web/20070613135116/http://www.canon.com/camera-museum/camera/digital/data/2003_ps-a70_s.html)

[en:Canon PowerShot A70](../Page/en:Canon_PowerShot_A70.md "wikilink")

[Category:佳能相機](../Category/佳能相機.md "wikilink")