**佳能 PowerShot S2 IS**
是一部2005年推出的五百万[象素的](../Page/象素.md "wikilink")[佳能长焦](../Page/佳能.md "wikilink")[数码相机](../Page/数码相机.md "wikilink")。它是[Canon
PowerShot S1
IS的升级版本](../Page/Canon_PowerShot_S1_IS.md "wikilink")，带有12x光学变焦，以及[图像稳定器](../Page/图像稳定器.md "wikilink")，使用[DiG\!C
II数字处理芯片](../Page/DIGIC.md "wikilink")。

S2 IS的继任者是[Canon PowerShot S3
IS](../Page/Canon_PowerShot_S3_IS.md "wikilink")。两部机器相似化程度较高，S3
IS只是使用了较大面积的CCD并且在象素上升级到了六百万。

## 主要参数

  - 500万有效[象素](../Page/象素.md "wikilink")
  - 1/2.5 英寸 [CCD](../Page/CCD.md "wikilink")
  - 12x 光学变焦
  - 光学影像稳定系统
  - 超声波马达
  - 带有iSAPS功能的佳能[DIGIC II处理器](../Page/DIGIC.md "wikilink")
  - 支援[PictBridge协议以及佳能直接打印功能](../Page/PictBridge.md "wikilink")
  - 13种拍摄模式
  - 光圈范围：F2.7/F3.5 到 F8
  - 最小快门：1/3500 s，最大快门：15 s
  - 640 x 480 15/30 帧 无限制有声短片
  - 电子取景器
  - 连拍功能：1.7 张每秒 最多24张照片。同时也支持间隔拍摄
  - 1.8 "（翻转式）LCD（11.5万像素）
  - 尺寸：113 x 78 x 76 mm
  - 重量：405 g

## 参见

  - [Canon PowerShot](../Page/Canon_PowerShot.md "wikilink")
  - [Canon PowerShot S1 IS](../Page/Canon_PowerShot_S1_IS.md "wikilink")
  - [Canon PowerShot S3 IS](../Page/Canon_PowerShot_S3_IS.md "wikilink")

## 外部链接

  - [佳能（中国）](http://www.canon.com.cn/)
  - [0cm超级微距\!佳能12X光变S2
    IS发布](http://www.pconline.com.cn/digital/dc/hangqing/hk/0505/625967.html)
  - [来了个狠角色：佳能S2
    IS发布](http://www.beareyes.com.cn/2/lib/200504/23/20050423118.htm)

[en:Canon PowerShot S2
IS](../Page/en:Canon_PowerShot_S2_IS.md "wikilink") [pl:Canon PowerShot
S2 IS](../Page/pl:Canon_PowerShot_S2_IS.md "wikilink")

[Category:佳能相機](../Category/佳能相機.md "wikilink")