**佳能 EOS
40D**是一部[佳能生產的](../Page/佳能.md "wikilink")[CMOS](../Page/CMOS.md "wikilink")[數位單鏡反光相機](../Page/數位單鏡反光相機.md "wikilink")，於2007年8月推出市面，市面上分為連鏡頭套裝（kit
set，包括一支EF-S 17-85mm IS鏡頭）和不連鏡頭套裝（body set）兩種。像素為1010萬像素，與[佳能 EOS
400D](../Page/佳能_EOS_400D.md "wikilink")、[佳能 EOS-1Ds Mark
III一樣](../Page/佳能_EOS-1Ds_Mark_III.md "wikilink")。和EOS-1Ds Mark
III、[Canon PowerShot G9一起發佈](../Page/Canon_PowerShot.md "wikilink")。

[Category:数码单反相机](../Category/数码单反相机.md "wikilink")
[Category:佳能相機](../Category/佳能相機.md "wikilink")