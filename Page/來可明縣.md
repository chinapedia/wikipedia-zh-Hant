**來可明縣**（）是位於[美国](../Page/美国.md "wikilink")[賓夕法尼亞州中北部的一個縣](../Page/賓夕法尼亞州.md "wikilink")。面積3,221平方公里。根据[美国人口调查局](../Page/美国人口调查局.md "wikilink")2000年统计，共有人口120,044人。县治[威廉斯波特](../Page/威廉斯波特_\(賓夕法尼亞州\).md "wikilink")。

成立於1795年4月13日。縣名來自[来可明溪](../Page/来可明溪.md "wikilink")（[特拉華語的意思是](../Page/特拉華語.md "wikilink")「多砂石的河」）──[薩斯奎哈納河西支的支流之一](../Page/薩斯奎哈納河.md "wikilink")。

[Category:賓夕法尼亞州行政區劃](../Category/賓夕法尼亞州行政區劃.md "wikilink")
[Category:阿巴拉契亚地区的县](../Category/阿巴拉契亚地区的县.md "wikilink")
[Category:1795年賓夕法尼亞州建立](../Category/1795年賓夕法尼亞州建立.md "wikilink")