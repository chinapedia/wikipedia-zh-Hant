**侍女**是指貼身照顧、侍候主人的[女僕或](../Page/女僕.md "wikilink")[隨從](../Page/隨從.md "wikilink")，又稱**侍婢**、**近身女僕**、**貼身女僕**、**近身[丫鬟](../Page/丫鬟.md "wikilink")**、**貼身丫鬟**、**使女**（[粵語稱](../Page/粵語.md "wikilink")**妹仔**、**[近身妹仔](../Page/近身.md "wikilink")**、**貼身妹仔**）。[宮廷女性如](../Page/宮廷.md "wikilink")[女王](../Page/女皇.md "wikilink")、[皇后](../Page/皇后.md "wikilink")、[妃嬪](../Page/妃嬪.md "wikilink")、[公主等都會有](../Page/公主.md "wikilink")[宮女侍候](../Page/宮女.md "wikilink")。

## 與女主人關係

東亞古代社會的[奴婢與](../Page/奴婢.md "wikilink")[長工分別並不明顯](../Page/長工.md "wikilink")，無論是賣身為婢還是長期受僱幫傭的[女子都稱為](../Page/女子.md "wikilink")[丫鬟](../Page/丫鬟.md "wikilink")。19世紀末，英國人於[治下之香港提出](../Page/英屬香港.md "wikilink")「反蓄婢運動」，原因是認為婢女相當於[奴隸](../Page/奴隸.md "wikilink")，沒人權、沒報酬，於是1921年9月由反對蓄婢人士成立[反對蓄婢會](../Page/反對蓄婢會.md "wikilink")，殊不知中國傳統社會並不認同「婢即為奴」，更視蓄婢為善行，可以避免年幼女童會因為家境貧窮而遭殺害，又或流離失所\[1\]，更甚者女主人與侍女可以感情深厚如姊妹。這一場運動正好反映中國傳統觀念與西方文化的差異和衝突\[2\]。

## 與男主人關係

在西方，侍女（**Handmaiden**）也是[妾的](../Page/妾.md "wikilink")[委婉語](../Page/委婉.md "wikilink")，當[妻子](../Page/妻子.md "wikilink")[不育時](../Page/不育.md "wikilink")，男性可能會找侍女為他生兒育女，例如《聖經》人物[辟拉](../Page/辟拉.md "wikilink")，是[雅各妻子](../Page/雅各.md "wikilink")[拉結的侍女](../Page/拉結.md "wikilink")，她為雅各生了兩個孩子。[瑪格麗特‧愛特伍於](../Page/瑪格麗特‧愛特伍.md "wikilink")1985年發表的[小說](../Page/小說.md "wikilink")《[侍女的故事](../Page/侍女的故事.md "wikilink")》（）中的侍女是另一例。

在古代中國，有些丫鬟會受到主人虐待或被主人賣掉，而有些會被男主人看上而成為[寵婢](../Page/寵婢.md "wikilink")、納為[妾侍甚至成為](../Page/妾侍.md "wikilink")[妻子](../Page/妻子.md "wikilink")。亦有些丫鬟受到主人厚待，與主人感情深厚。上層社會女性出嫁時常會帶同侍女陪嫁，這些侍女有時會成為丈夫的妾。[女性以從](../Page/女性.md "wikilink")[娘家帶來的侍女為丈夫的妾](../Page/娘家.md "wikilink")，既可幫助自己約束丈夫，又因原有的侍女身分而容易使喚，比外納的妾要放心，例如《紅樓夢》裡的[平兒本來是](../Page/平兒.md "wikilink")[王熙鳳的侍婢](../Page/王熙鳳.md "wikilink")，王熙鳳嫁給[賈璉](../Page/賈璉.md "wikilink")，就要平兒作妾。也有一些侍女被男主人收為[寵婢](../Page/寵婢.md "wikilink")（情婦），又稱「通房丫頭」。

## 文學

在西方的[小說](../Page/小說.md "wikilink")、[文學裡](../Page/文學.md "wikilink")，尤其是[奇幻和](../Page/奇幻.md "wikilink")[科幻小說](../Page/科幻小說.md "wikilink")（例如《[魔戒](../Page/魔戒.md "wikilink")》、《[星球大戰](../Page/星球大戰.md "wikilink")》和《[異世奇人](../Page/異世奇人.md "wikilink")》）常會有侍女出現，[神話如](../Page/神話.md "wikilink")[北歐神話裡的](../Page/北歐神話.md "wikilink")[女神](../Page/女神.md "wikilink")（例如[弗麗嘉](../Page/弗麗嘉.md "wikilink")）也常會有侍女。中國的小說、戲曲裡也常有侍女的形象，例如《[鶯鶯傳](../Page/鶯鶯傳.md "wikilink")》和《[西廂記](../Page/西廂記.md "wikilink")》的[紅娘](../Page/紅娘.md "wikilink")、《[牡丹亭](../Page/牡丹亭.md "wikilink")》的春香、《[紅樓夢](../Page/紅樓夢.md "wikilink")》的眾多侍婢如[襲人](../Page/襲人.md "wikilink")、[晴雯等](../Page/晴雯.md "wikilink")。[聖經人物如](../Page/聖經.md "wikilink")[利亞](../Page/利亞.md "wikilink")、[聖母瑪利亞](../Page/聖母瑪利亞.md "wikilink")，是作為神之侍女的形象出現。\[<http://www.biblegateway.com/passage/?search=luke%201:38;&version=9>;\]

## 另見

  - [家務工](../Page/家務工.md "wikilink")
  - [監護工](../Page/監護工.md "wikilink")
  - [女官](../Page/女官.md "wikilink")

## 參考資料

<references/>

  - [論《紅樓夢》的妾](http://218.22.232.3/Files/01zhengcefagui/hongloumeng/%E8%AE%BA%E3%80%8A%E7%BA%A2%E6%A5%BC%E6%A2%A6%E3%80%8B%E7%9A%84%E5%A6%BE.PDF)
  - [英文版維基百科Handmaiden](http://en.wikipedia.org/wiki/Handmaiden)

[pl:Handmaiden](../Page/pl:Handmaiden.md "wikilink")

[Category:家務勞動](../Category/家務勞動.md "wikilink")
[Category:個人照護職業](../Category/個人照護職業.md "wikilink")
[Category:女性](../Category/女性.md "wikilink")
[Category:定型角色](../Category/定型角色.md "wikilink")

1.  <http://programme.rthk.org.hk/rthk/tv/programme.php?name=tv/hkhistory&d=2009-01-28&p=4337&e=83388&m=episode>
2.  <http://programme.rthk.org.hk/channel/radio/programme.php?name=/hkhistory&d=2009-01-29&p=4351&e=84154&m=episode>