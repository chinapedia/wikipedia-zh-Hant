**侏罗纪**（Jurassic）是一个[地质年代](../Page/地质年代.md "wikilink")，界于[三叠纪和](../Page/三叠纪.md "wikilink")[白垩纪之间](../Page/白垩纪.md "wikilink")，約1億9960萬年前（誤差值為60萬年）到1億4550萬年前（誤差值為400萬年）。侏羅紀是[中生代的第二个](../Page/中生代.md "wikilink")[纪](../Page/紀_\(地質學\).md "wikilink")，開始於[三疊紀-侏羅紀滅絕事件](../Page/三疊紀-侏羅紀滅絕事件.md "wikilink")。虽然这段时间的岩石标志非常明显和清晰，其开始和结束的准确时间却如同其它古远的[地质时代](../Page/地质时代.md "wikilink")，无法非常精确地被确定。

侏羅紀前期，因為經歷[大滅絕](../Page/大滅絕.md "wikilink")，所以各種動植物都非常稀少（屬於休養生息的階段），但其中[恐龍總目一枝獨秀](../Page/恐龍總目.md "wikilink")，伺機稱霸陸地。侏羅紀中晚期以後，[恐龍成為地球上最繁榮昌盛的優勢物種](../Page/恐龍.md "wikilink")，此後會統治地球1.5億年，直到[白堊紀-第三紀滅絕事件為止](../Page/白堊紀-第三紀滅絕事件.md "wikilink")。

现已发现的化石，记载了侏罗纪气候环境和构造活动十分独特。[盘古联合大陆Pangea](../Page/盘古大陆.md "wikilink")，自[泥盆纪形成](../Page/泥盆纪.md "wikilink")（4亿年前）以来，三叠纪持续维持，但在晚三叠世开始分裂。中晚侏罗世时，十足的[板块运动](../Page/板块运动.md "wikilink")，导致了[南美洲的南部从](../Page/南美洲.md "wikilink")[非洲分开](../Page/非洲.md "wikilink")。[劳亚古陆Laurasia](../Page/勞亞古大陸.md "wikilink")（其中包括[北美和](../Page/北美.md "wikilink")[欧亚大陆](../Page/欧亚大陆.md "wikilink")）也逐渐地从非洲和南美洲分离开，造就了[大西洋和](../Page/大西洋.md "wikilink")[墨西哥湾](../Page/墨西哥湾.md "wikilink")。沿着这些裂谷大陆的边缘，[火山活动频繁](../Page/火山.md "wikilink")。与此同时，欧亚大陆（欧亚）南下，缩小了[特提斯洋](../Page/特提斯洋.md "wikilink")。侏罗纪[海平面的不断上升](../Page/海平面.md "wikilink")，北美和欧洲间形成了大陆边缘的海道。侏罗纪时期，地球上要比三叠纪时拥有更多的独立陆块，导致海岸带增多。

整个侏罗纪世界，大多数时期处于温暖和潮湿，酷似温室气候。当时繁盛的森林植被，形成了如今[澳大利亚和](../Page/澳大利亚.md "wikilink")[南极洲丰富的](../Page/南极洲.md "wikilink")[煤炭资源](../Page/煤炭.md "wikilink")。尽管那时有局部的干旱地区，但绝大多数盘古大陆，均处于郁郁葱葱的[绿洲](../Page/绿洲.md "wikilink")。劳亚大陆Laurasia和南部的[冈瓦纳大陆生物群](../Page/冈瓦纳大陆.md "wikilink")，在许多方面，仍然十分独特。不过侏罗纪时，动物群具备了较多的洲际色彩。现已发现，一些动物和植物物种，几乎遍及全世界，而不是只被限制在特定区域。

距今約1億8000万年至1億4400萬年前的侏羅紀後期，邦格亞大陸開始正式分裂。北美洲邊向右繞轉，邊遠離非洲。此時，大陸仍和[北歐連在一起](../Page/北歐.md "wikilink")，但接觸的地區幾乎都為淺海，故無連綿不斷的乾燥陸地。

## 語源

侏罗纪是由[亞歷山大·布隆尼亞爾命名](../Page/亞歷山大·布隆尼亞爾.md "wikilink")，名稱取自於[德國](../Page/德國.md "wikilink")、[法國](../Page/法國.md "wikilink")、[瑞士邊界的](../Page/瑞士.md "wikilink")[侏罗山](../Page/侏罗山.md "wikilink")，侏罗山有很多大規模的海相[石灰岩露頭](../Page/石灰岩.md "wikilink")。

中文名称源自旧时日本人使用日语汉字音读的音译名“侏罗纪”（音读：ジュラキ，罗马字：juraki）。

## 侏罗纪亚层

侏罗纪由三叠纪晚期灭绝事件开始，一般分为早、中、晚三世，在[歐洲又名里阿斯世](../Page/歐洲.md "wikilink")、道格世、麻姆世：\[1\]

|                                                        |                                |
| ------------------------------------------------------ | ------------------------------ |
| **[晚侏罗世](../Page/晚侏罗世.md "wikilink")**，麻姆世（Malm）       |                                |
| [提通期](../Page/提通期.md "wikilink")（Tithonian）：           | 150.8 ± 4.0 – 145.5 ± 4.0 百万年前 |
| [启莫里期](../Page/启莫里期.md "wikilink")（Kimmeridgean）：      | 155.7 ± 4.0 – 150.8 ± 4.0 百万年前 |
| [牛津期](../Page/牛津期.md "wikilink")（Oxfordian）：           | 161.2 ± 4.0 – 155.7 ± 4.0 百万年前 |
| **[中侏罗世](../Page/中侏罗世.md "wikilink")**，道格世（Dogger）     |                                |
| [卡洛维期](../Page/卡洛维期.md "wikilink")（Callovian）：         | 164.7 ± 4.0 – 161.2 ± 4.0 百万年前 |
| [巴通期](../Page/巴通期.md "wikilink")（Bathonian）：           | 167.7 ± 3.5 – 164.7 ± 4.0 百万年前 |
| [巴柔期](../Page/巴柔期.md "wikilink")（Bajocian）：            | 171.6 ± 3.0 – 167.7 ± 3.5 百万年前 |
| [阿连期](../Page/阿连期.md "wikilink")（Aalenian）：            | 175.6 ± 2.0 – 171.6 ± 3.0 百万年前 |
| **[早侏罗世](../Page/早侏罗世.md "wikilink")**，里阿斯世（Lias）      |                                |
| [托阿尔期](../Page/托阿尔期.md "wikilink")（Toarcian）：          | 183.0 ± 1.5 – 175.6 ± 2.0 百万年前 |
| [普连斯巴奇期](../Page/普连斯巴奇期.md "wikilink")（Pliensbachian）： | 189.6 ± 1.5 – 183.0 ± 1.5 百万年前 |
| [锡内穆期](../Page/锡内穆期.md "wikilink")（Sinemurian）：        | 196.5 ± 1.0 – 189.6 ± 1.5 百万年前 |
| [海塔其期](../Page/海塔其期.md "wikilink")（Hettangian）：        | 199.6 ± 0.6 – 196.5 ± 1.0 百万年前 |

## 地理

[MakhteshGadolCenter02.jpg](https://zh.wikipedia.org/wiki/File:MakhteshGadolCenter02.jpg "fig:MakhteshGadolCenter02.jpg")南部的Matmor組地層。\]\]
侏羅紀時期的大氣層[氧氣含量是現今的](../Page/氧氣.md "wikilink")130%，[二氧化碳含量是](../Page/二氧化碳.md "wikilink")[工業時代前的](../Page/工業革命.md "wikilink")7倍，氣溫則是高於今日約攝氏3°。

侏羅纪早期，[盘古大陆分裂为兩块](../Page/盘古大陆.md "wikilink")：北方的[勞亞大陸](../Page/勞亞大陸.md "wikilink")，與南方的[岡瓦那大陸](../Page/岡瓦那大陸.md "wikilink")。[墨西哥灣出現](../Page/墨西哥灣.md "wikilink")，位於[北美洲與](../Page/北美洲.md "wikilink")[猶加敦半島之間](../Page/猶加敦半島.md "wikilink")。刚开始的北[大西洋比较窄](../Page/大西洋.md "wikilink")。而南大西洋要到白堊紀時，岡瓦那大陸分裂，才開始出現。\[2\][特提斯洋開始閉合](../Page/特提斯洋.md "wikilink")，[地中海出現](../Page/地中海.md "wikilink")。这个纪的气候比较暖和，没有[冰川的遺跡](../Page/冰川.md "wikilink")。如同三疊紀，南北極地区没有陆地，也沒有[冰帽的證據](../Page/冰帽.md "wikilink")。
[Europasaurus_holgeri_Scene_2.jpg](https://zh.wikipedia.org/wiki/File:Europasaurus_holgeri_Scene_2.jpg "fig:Europasaurus_holgeri_Scene_2.jpg")的[歐羅巴龍](../Page/歐羅巴龍.md "wikilink")。大型恐龍成為侏羅紀的優勢動物。\]\]

[西欧海岸发现了许多侏罗纪的海洋生物](../Page/西欧.md "wikilink")，顯示這地區當時是[熱帶的淺海地區](../Page/熱帶.md "wikilink")。著名的地點包含[英國南部的](../Page/英國.md "wikilink")[侏羅紀海岸](../Page/侏羅紀海岸.md "wikilink")，以及[德國的晚三疊紀](../Page/德國.md "wikilink")[候斯瑪登與](../Page/候斯瑪登.md "wikilink")[索倫霍芬地層](../Page/索倫霍芬.md "wikilink")。\[3\]北美洲的侏羅紀地層較少露頭，因此較少被發現。\[4\]在侏羅紀晚期，[森丹斯海](../Page/森丹斯海.md "wikilink")（[陸緣海](../Page/陸緣海.md "wikilink")）在[美國北部與](../Page/美國.md "wikilink")[加拿大留下許多海相沉積層](../Page/加拿大.md "wikilink")。這個時期的北美洲地層大多為陸相沉積層，例如[莫里遜組](../Page/莫里遜組.md "wikilink")。

侏羅紀的海洋富含[鈣](../Page/鈣.md "wikilink")，在非生物性[碳酸鈣沈澱物中](../Page/碳酸鈣.md "wikilink")，主要的成分是[鎂離子低的鈣](../Page/鎂.md "wikilink")。各地常見[碳酸岩與鈣質](../Page/碳酸岩.md "wikilink")[鮞石](../Page/鮞石.md "wikilink")、鈣質膠結物，[無脊椎動物常具有鈣質外殼](../Page/無脊椎動物.md "wikilink")。

在侏羅紀中期，[北美洲的](../Page/北美洲.md "wikilink")[科迪勒拉山系形成](../Page/科迪勒拉山系.md "wikilink")（[內華達造山運動](../Page/內華達造山運動.md "wikilink")），是已知最早的侏羅紀大型岩基。\[5\]侏羅紀的重要露頭分布於以下地區：[俄羅斯](../Page/俄羅斯.md "wikilink")、[印度](../Page/印度.md "wikilink")、[南美](../Page/南美.md "wikilink")、[日本](../Page/日本.md "wikilink")、[澳大拉西亞](../Page/澳大拉西亞.md "wikilink")、以及[英國](../Page/英國.md "wikilink")

## 動物群

[Fischsaurier_fg01.jpg](https://zh.wikipedia.org/wiki/File:Fischsaurier_fg01.jpg "fig:Fischsaurier_fg01.jpg")的化石，發現於[德國南部](../Page/德國.md "wikilink")[霍斯馬登的](../Page/霍斯馬登.md "wikilink")[里阿斯統](../Page/里阿斯統.md "wikilink")[板岩](../Page/板岩.md "wikilink")。\]\]
[JurassicMarineIsrael.JPG](https://zh.wikipedia.org/wiki/File:JurassicMarineIsrael.JPG "fig:JurassicMarineIsrael.JPG")與[雙殼綱](../Page/雙殼綱.md "wikilink")[殼菜蛤科化石](../Page/殼菜蛤科.md "wikilink")，發現於[以色列南部的侏羅紀](../Page/以色列.md "wikilink")[石灰岩層](../Page/石灰岩.md "wikilink")。\]\]
[Gigandipus.JPG](https://zh.wikipedia.org/wiki/File:Gigandipus.JPG "fig:Gigandipus.JPG")西南部[蒙納夫組](../Page/蒙納夫組.md "wikilink")（下侏羅紀）的足跡化石。\]\]

### 陸地

#### 早期

[恐龙剛剛崛起](../Page/恐龙.md "wikilink")，[板龍](../Page/板龍.md "wikilink")、[萊森龍等中型](../Page/萊森龍.md "wikilink")[蜥腳亞目開始興盛](../Page/蜥腳亞目.md "wikilink")，其主要對手有[獸腳亞目的](../Page/獸腳亞目.md "wikilink")[南十字龍和](../Page/南十字龍.md "wikilink")[雙冠龍](../Page/雙冠龍.md "wikilink")。[鳥臀目恐龍還未真正發展起來](../Page/鳥臀目.md "wikilink")。

#### 中晚期

[恐龍正式開始興盛和繁衍壯大](../Page/恐龍.md "wikilink")，成為整個陸地上的優勢物種。
大型[蜥腳下目恐龍大批繁衍包含](../Page/蜥腳下目.md "wikilink")：[圓頂龍](../Page/圓頂龍.md "wikilink")、[雷龍](../Page/雷龍.md "wikilink")、[迷惑龍](../Page/迷惑龍.md "wikilink")、[馬門溪龍](../Page/馬門溪龍.md "wikilink")、[梁龍](../Page/梁龍.md "wikilink")、[腕龍等](../Page/腕龍.md "wikilink")，牠們以草原上的大型[蕨類](../Page/蕨類.md "wikilink")、大型[蘇鐵為食](../Page/蘇鐵.md "wikilink")，或以更長的[針葉林為食](../Page/針葉林.md "wikilink")。

大型掠食者包含[角鼻龍](../Page/角鼻龍.md "wikilink")、[斑龍](../Page/斑龍.md "wikilink")、[蠻龍](../Page/蠻龍.md "wikilink")、[異特龍](../Page/異特龍.md "wikilink")。[鳥臀目的數量較](../Page/鳥臀目.md "wikilink")[蜥臀目恐龍少](../Page/蜥臀目.md "wikilink")，但[劍龍下目與小型](../Page/劍龍下目.md "wikilink")[鳥腳下目恐龍佔據者中到小型的](../Page/鳥腳下目.md "wikilink")[草食性恐龍生態位](../Page/草食性.md "wikilink")。

第一批有體毛的小型[哺乳類於此時出現](../Page/哺乳類.md "wikilink")。但是數量稀少，對爬蟲類而言不構成威脅，只能在樹洞或者地下以[昆蟲為食](../Page/昆蟲.md "wikilink")。

### 海洋

侏罗纪的海洋裡，[鱼类持續繁榮和](../Page/鱼.md "wikilink")[海生爬行动物逐漸壯大](../Page/海生爬行动物.md "wikilink")、成為優勢物種，此時的[海生爬行動物包含](../Page/海生爬行動物.md "wikilink")：[魚龍目](../Page/魚龍目.md "wikilink")、[蛇頸龍目](../Page/蛇頸龍目.md "wikilink")、海生[鱷魚](../Page/鱷魚.md "wikilink")（[地蜥鱷科與](../Page/地蜥鱷科.md "wikilink")[真蜥鱷科](../Page/真蜥鱷科.md "wikilink")）。

在[無脊椎動物方面](../Page/無脊椎動物.md "wikilink")，出現了數種新動物，包含[厚殼蛤類](../Page/厚殼蛤類.md "wikilink")（一群可形成暗礁的多樣化[雙殼綱動物](../Page/雙殼綱.md "wikilink")）與[箭石目](../Page/箭石目.md "wikilink")。侏羅紀時期的有殼無脊椎動物相當多樣，有殼無脊椎動物造成的生物侵蝕增加，尤其是生痕化石。

[浮游生物的](../Page/浮游生物.md "wikilink")12個[演化支中](../Page/演化支.md "wikilink")，有4或5個演化支在侏羅紀時期首次出現、或輻射演化。\[6\]

### 天空

[翼龍目成為非常常見的飛行動物](../Page/翼龍.md "wikilink")，佔據着各種生態位。侏羅紀後期，第一種[鸟类出現](../Page/鸟.md "wikilink")，牠們演化自小型[虛骨龍類恐龍](../Page/虛骨龍類.md "wikilink")，但是並未成為天空的優勢物種。

## 植物群

[Douglas_fir_leaves_and_bud.jpg](https://zh.wikipedia.org/wiki/File:Douglas_fir_leaves_and_bud.jpg "fig:Douglas_fir_leaves_and_bud.jpg")是侏羅紀常見的植物\]\]
三疊紀的乾旱、大陸型氣候，在侏羅紀逐漸消失，尤其是在高[緯度地區](../Page/緯度.md "wikilink")。侏羅紀的氣候溫暖、潮濕，使地表佈滿大型森林。\[7\]侏羅紀的[裸子植物相當多樣化](../Page/裸子植物.md "wikilink")。\[8\]如同三疊紀，此時期的優勢植物是[裸子植物的](../Page/裸子植物.md "wikilink")[松柏綱](../Page/松柏綱.md "wikilink")，它們十分多樣化，是各地大型森林的主要成員。繁盛於侏羅紀，並存活至今的松柏綱包含：[南洋杉科](../Page/南洋杉科.md "wikilink")、[三尖杉科](../Page/三尖杉科.md "wikilink")、[松科](../Page/松科.md "wikilink")、[羅漢松科](../Page/羅漢松科.md "wikilink")、[紅豆杉科](../Page/紅豆杉科.md "wikilink")、[杉科](../Page/杉科.md "wikilink")。\[9\]已滅絕的松柏綱則包含：低緯度的優勢植物[掌鱗杉科](../Page/掌鱗杉科.md "wikilink")、以及灌木大小的[本內蘇鐵目](../Page/本內蘇鐵目.md "wikilink")。\[10\][蘇鐵也相當常見](../Page/蘇鐵.md "wikilink")，[銀杏與](../Page/銀杏.md "wikilink")[樹蕨常出現在森林中](../Page/樹蕨.md "wikilink")。\[11\]較小型[蕨類](../Page/蕨類.md "wikilink")，可能是優勢低矮植物。*Caytoniacea*是群灌木至小型數木大小的植物，是這個時期的另一群重要植物。\[12\]\[13\][北半球的中高緯度地區](../Page/北半球.md "wikilink")，類似銀杏的植物特別普遍。\[14\]在[南半球](../Page/南半球.md "wikilink")，[羅漢松則是常見的植物](../Page/羅漢松.md "wikilink")，銀杏與[線銀杏目較為少見](../Page/線銀杏目.md "wikilink")\[15\]\[16\]在海洋中，[珊瑚藻開始出現](../Page/珊瑚藻.md "wikilink")。\[17\]

## 相關條目

  - [地质时代](../Page/地质时代.md "wikilink")
  - [侏羅紀公園系列](../Page/侏羅紀公園系列.md "wikilink")

## 參考資料

<references/>

  - Mader, Sylvia (2004) Biology, eighth edition
  - Ogg, Jim; June, 2004, *Overview of Global Boundary Stratotype
    Sections and Points (GSSP's)*
    <https://web.archive.org/web/20060423084018/http://www.stratigraphy.org/gssp.htm>
    Accessed April 30, 2006.
  - Stanley, S.M. and Hardie, L.A. (1998). "Secular oscillations in the
    carbonate mineralogy of reef-building and sediment-producing
    organisms driven by tectonically forced shifts in seawater
    chemistry". Palaeogeography, Palaeoclimatology, Palaeoecology 144:
    3-19.
  - Stanley, S.M. and Hardie, L.A. (1999). "Hypercalcification;
    paleontology links plate tectonics and geochemistry to
    sedimentology". GSA Today 9: 1-7.
  - Taylor, P.D. and Wilson, M.A., 2003. Palaeoecology and evolution of
    marine hard substrate communities. Earth-Science Reviews 62: 1-103.
    [1](https://web.archive.org/web/20080706190232/http://www.wooster.edu/geology/Taylor%26Wilson2003.pdf)

[\*](../Category/侏羅紀.md "wikilink")
[\*](../Category/侏罗纪爬行动物.md "wikilink")

1.  Kazlev, M. Alan (2002) [Palaeos
    website](http://www.palaeos.com/Mesozoic/Jurassic/Jurassic.htm)
    Accessed July. 22, 2008

2.

3.  [Jurassic
    Period](http://www.urweltmuseum.de/Englisch/museum_eng/Geologie_eng/Tektonik_eng.htm)


4.

5.  Monroe, James S., and Reed Wicander. (1997) *The Changing Earth:
    Exploring Geology and Evolution*, 2nd ed. Belmont: West Publishing
    Company, 1997. ISBN 0-314-09577-2

6.
7.  Haines, Tim (2000) *Walking with Dinosaurs: A Natural History*, New
    York: Dorling Kindersley Publishing, Inc., p. 65. ISBN
    0-563-38449-2.

8.
9.
10.
11.
12. Behrensmeyer, Anna K., Damuth, J.D., DiMichele, W.A., Potts, R.,
    Sues, H.D. & Wing, S.L. (eds.) (1992), *Terrestrial Ecosystems
    through Time: the Evolutionary Paleoecology of Terrestrial Plants
    and Animals*, [University of Chicago
    Press](../Page/University_of_Chicago_Press.md "wikilink"), Chicago
    and London, ISBN 0-226-04154-9 (cloth), ISBN 0-226-04155-7 (paper)

13.
14.
15.
16.
17.