[教宗](../Page/教宗.md "wikilink")**諾森二世**（，），本名**Gregorio
Papareschi**，於1130年2月14日—1143年9月24日岀任教宗。

## 譯名列表

  - 依諾增爵二世：[天主教香港教區禮儀委員會：禧年專頁](http://catholic-dlc.org.hk/2000.htm)作依諾增爵。
  - 因諾森特二世：《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作因諾森特。
  - 英諾森二世：[《大英簡明百科知識庫》2005年版作英諾森](http://wordpedia.britannica.com/concise/content.aspx?id=4166&hash=S4qRyNX7zlhlyoEjrdWypA%3D%3D&t=3)。
  - 諾森二世：[香港天主教教區檔案　歷任教宗作諾森](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)。
  - 意諾增爵二世：[天主教天津教区](https://web.archive.org/web/20070311132024/http://www.catholic.tj.cn/p/Article/Jhls/Lsjs/200507/7285.html)作意諾增爵。

[Category:教宗](../Category/教宗.md "wikilink")
[Category:羅馬人](../Category/羅馬人.md "wikilink")
[Category:義大利出生的教宗](../Category/義大利出生的教宗.md "wikilink")