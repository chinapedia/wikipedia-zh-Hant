[Pessac_Château_Haut-Brion.jpg](https://zh.wikipedia.org/wiki/File:Pessac_Château_Haut-Brion.jpg "fig:Pessac_Château_Haut-Brion.jpg")
**侯伯王酒庄**（），又稱為**红颜容酒庄**、**奥比昂酒庄**、**上布里昂酒庄**或**歐波里翁酒莊**，是位於[法国](../Page/法国.md "wikilink")[佩薩克的著名](../Page/佩薩克.md "wikilink")[葡萄酒庄园](../Page/葡萄酒庄园.md "wikilink")。该酿酒庄园是由官方检查证明名称与产地属实的品牌（Appellation
de Haut-Brion contrôlée）。庄园佔地51公顷，葡萄園年產12000-15000箱葡萄酒。

侯伯王酒庄的[红葡萄酒在目前法國官方排名中](../Page/红葡萄酒.md "wikilink")，位列第一等（Premier Grand
Cru），与[玛歌酒庄](../Page/玛歌酒庄.md "wikilink")、[拉度酒庄](../Page/拉度酒庄.md "wikilink")、[木桐酒庄和](../Page/木桐酒庄.md "wikilink")[拉菲酒庄共享该殊荣](../Page/拉菲酒庄.md "wikilink")。

## 歷史

雖然一般認為葡萄是從羅馬時期開始種植的，但實際有文件證明種植葡萄的年代最早是1423年。而侯伯王酒庄的記錄是在1525年，是當Jeanne de
Bellon嫁給Jean de Pontac時所帶的嫁妝。

侯伯王酒庄是第一個有記錄進口到美國的五大酒莊，是[湯瑪斯·傑佛遜在法國旅行時買了](../Page/湯瑪斯·傑佛遜.md "wikilink")6箱寄回他位在[維吉尼亞的莊園](../Page/維吉尼亞.md "wikilink")。

著名的擁有者经营侯伯王酒庄的时间超過了4個世紀，其中包括了：法國海軍司令、地區主教、法國元帥、阿基坦（[Guyenne](../Page/Guyenne.md "wikilink")）區首長、拿破仑的外交大臣[夏爾·莫里斯·德塔列朗-佩里戈爾以及](../Page/夏爾·莫里斯·德塔列朗-佩里戈爾.md "wikilink")[約翰·甘迺迪任總統時的美國駐法大使](../Page/約翰·甘迺迪.md "wikilink")
C·道格拉斯·狄龍（[C. Douglas Dillon](../Page/C._Douglas_Dillon.md "wikilink")）。

現在的擁有者是美國銀行家[Clarence
Dillon的孫女](../Page/Clarence_Dillon.md "wikilink")[Duchesse de
Mouchy](../Page/Duchesse_de_Mouchy.md "wikilink")，也是[Domaine
Clarence股份有限公司的主席](../Page/Domaine_Clarence.md "wikilink")。侯伯王酒庄是五大酒莊中唯一被美國人所擁有的酒庄。

在1960年時，侯伯王酒庄是第一個革命性的採用不鏽鋼桶發酵技術的酒莊。

## 釀酒

侯伯王酒庄種植了55%的[赤霞珠（卡本內-蘇維濃）](../Page/赤霞珠.md "wikilink")、25%的[梅洛](../Page/梅洛.md "wikilink")、20%的[品丽珠](../Page/品丽珠.md "wikilink")，葡萄平均株齡為30年。

侯伯王酒庄的優等酒是在不鏽鋼桶裡發酵後，放在新橡木桶裡陳酿24\~27個月。整個釀酒過程由[Jean-Philippe
Delmas管理](../Page/Jean-Philippe_Delmas.md "wikilink")。

英國文學家[Samuel Pepys](../Page/Samuel_Pepys.md "wikilink")（又被稱為
Ho-Bryan）、哲學家[約翰·洛克](../Page/約翰·洛克.md "wikilink")、法國樞機主教[黎塞留公爵及美國總統](../Page/黎塞留.md "wikilink")[湯瑪斯·傑佛遜都曾記載](../Page/湯瑪斯·傑佛遜.md "wikilink")、描述过產自侯伯王酒庄的特級酒。

## 裝瓶

[Margaux94_1.jpg](https://zh.wikipedia.org/wiki/File:Margaux94_1.jpg "fig:Margaux94_1.jpg")
值得一提的是，侯伯王酒庄的酒瓶形狀較類似[勃根地酒瓶而非](../Page/勃根地.md "wikilink")[波爾多的傳統酒瓶](../Page/波爾多.md "wikilink")。

## 外部链接

  - [官方网站](http://www.haut-brion.com/)

[H](../Category/法國葡萄酒.md "wikilink") [H](../Category/製酒商.md "wikilink")
[H](../Category/酒廠.md "wikilink")