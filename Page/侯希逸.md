**侯希逸**（），[营州](../Page/营州_\(北魏\).md "wikilink")（在今[辽宁省](../Page/辽宁省.md "wikilink")[朝阳市](../Page/朝阳市.md "wikilink")）人，[唐朝另一著名将领](../Page/唐朝.md "wikilink")[李正己的表兄](../Page/李正己.md "wikilink")。

自幼習武，長七尺，勇猛有力，原為[安祿山部將](../Page/安祿山.md "wikilink")，[天宝十四年](../Page/天宝_\(唐朝\).md "wikilink")（755年），[安史之亂爆發](../Page/安史之亂.md "wikilink")，侯希逸當時是[平盧節度使](../Page/平盧節度使.md "wikilink")[徐歸道的](../Page/徐歸道.md "wikilink")[裨將](../Page/裨將.md "wikilink")，不願反叛唐朝，遂與安東都護[王玄志襲殺徐歸道](../Page/王玄志.md "wikilink")，歸順朝廷。[乾元元年](../Page/乾元.md "wikilink")，平盧節度使[王玄志病死](../Page/王玄志.md "wikilink")，部將[李懷玉殺王玄志之子](../Page/李懷玉.md "wikilink")，推侯希逸為節度使，朝廷委曲求全，勉強同意。侯希逸見平戶難守，且行且戰，集结在[山东](../Page/山东.md "wikilink")，攻陷[青州](../Page/青州_\(古代\).md "wikilink")。[寶應元年](../Page/宝应_\(年号\).md "wikilink")（762年）五月，以功封平盧、淄青二鎮節度使，統領青、淄（治今山東[淄川](../Page/淄川.md "wikilink")）、齊（治今山東[濟南市](../Page/濟南市.md "wikilink")）、沂（治山東[臨沂](../Page/臨沂.md "wikilink")）、密（治今山東[諸誠](../Page/諸誠.md "wikilink")）、海（治今[江蘇](../Page/江蘇.md "wikilink")[連雲港西南](../Page/連雲港.md "wikilink")[海州区](../Page/海州区_\(连云港市\).md "wikilink")）之州，有治蹟。

[唐代宗](../Page/唐代宗.md "wikilink")[大歷十一年九月](../Page/大歷.md "wikilink")，任檢校尚書右僕射、上柱國，受封淮陽郡王。遂漸驕恣，怠於政事，晚年好佛，大建佛事，軍民苦之。[永泰元年](../Page/永泰_\(唐朝\).md "wikilink")（765年），侯希逸外出打獵，與巫者夜宿於城外，軍士閉門不納，推舉兵馬使李懷玉為帥，希逸只好奔歸長安，唐朝任命李怀玉为平卢、淄青节度使。建中二年（781年），迁司空，未及正式拜官，侯希逸病死。

## 評價

《新唐書·侯希逸傳》记载“（[平卢军](../Page/平卢军.md "wikilink")）与贼确，数有功。然孤军无援，又为奚侵掠，乃拔其军二万，浮海入青州据之，平卢遂陷。[肃宗因以希逸为平卢](../Page/唐肃宗.md "wikilink")、淄青节度使。”[安史之乱后](../Page/安史之乱.md "wikilink")，[平卢军军力严重受损](../Page/平卢军.md "wikilink")，使其无法在营州地区立足，从而南迁入今天的山东境内。进入山东后，李正己与侯希逸重视农业，法制的建设，得到了迅速的发展。《新唐书》记载：“希逸初领淄青，甚著声称，理兵务农，远近美之。”“正己用刑严峻，所在不敢偶语；然法令齐一，赋均而轻，拥兵十万，雄据东方。”

## 參考文獻

  - 《新唐書·侯希逸傳》卷一四四

[H](../Category/平盧節度使.md "wikilink")
[Category:唐朝御史大夫](../Category/唐朝御史大夫.md "wikilink")
[H](../Category/淄青節度使.md "wikilink")
[Category:唐朝检校工部尚书](../Category/唐朝检校工部尚书.md "wikilink")
[Category:唐朝检校尚书右仆射](../Category/唐朝检校尚书右仆射.md "wikilink")
[Category:唐朝司空](../Category/唐朝司空.md "wikilink")
[H](../Category/唐朝异姓郡王.md "wikilink")
[Category:唐朝追赠太保](../Category/唐朝追赠太保.md "wikilink")
[Category:唐朝佛教人物](../Category/唐朝佛教人物.md "wikilink")
[Category:唐朝高句丽人](../Category/唐朝高句丽人.md "wikilink")
[Category:朝阳人](../Category/朝阳人.md "wikilink")
[X](../Category/侯姓.md "wikilink")