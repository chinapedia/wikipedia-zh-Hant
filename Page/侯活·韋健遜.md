**侯活·韋健遜**（****，）是一名前[英格蘭足球員和領隊](../Page/英格蘭.md "wikilink")，他的兒子[賓·韋健遜](../Page/賓·韋健遜.md "wikilink")（[Ben
Wilkinson](../Page/:en:Ben_Wilkinson.md "wikilink")）亦是一名足球員，效力[約克城](../Page/約克城足球會.md "wikilink")。

## 球員生涯

韋健遜曾效力[錫周三](../Page/錫周三.md "wikilink")、[白禮頓和](../Page/白禮頓.md "wikilink")[波士頓聯](../Page/波士頓聯足球會.md "wikilink")，他在波士頓聯時以球員身份兼任領隊，並於[諾士郡展開其全職領隊的生涯](../Page/諾士郡.md "wikilink")。

## 領隊生涯

### 諾士郡

1982/83球季，韋健遜協助傳奇領隊[占美·施維爾](../Page/占美·施維爾.md "wikilink")（[Jimmy
Sirrel](../Page/:en:Jimmy_Sirrel.md "wikilink")）執教[諾士郡](../Page/諾士郡.md "wikilink")，並成功帶領球隊升上頂級聯賽。

### 錫周三

1983—88年，韋健遜擔任[錫周三的領隊](../Page/錫周三.md "wikilink")，在韋健遜任教的首季，[錫周三便升上頂級聯賽](../Page/錫周三.md "wikilink")，[錫周三在韋健遜離去](../Page/錫周三.md "wikilink")4年後降班。

### 列斯聯

韋健遜的領隊生涯在[列斯聯到達顛峰](../Page/列斯聯.md "wikilink")，1988年成為這支[西約克郡球隊的領隊後](../Page/西約克郡.md "wikilink")，於1989/90球季韋健遜羅致[史特根等猛將下](../Page/史特根.md "wikilink")，勇奪乙組聯賽冠軍，升級上甲組聯賽。韋健遜帶領球隊升級後，未有得意忘形，反而迅速購入[麥亞里士打和](../Page/麥亞里士打.md "wikilink")[約翰·盧傑斯](../Page/約翰·盧傑斯.md "wikilink")（[John
Lukic](../Page/:en:John_Lukic.md "wikilink")）等球員，並從青年隊提拔了[史必和](../Page/史必.md "wikilink")[大衛·碧提](../Page/大衛·碧提.md "wikilink")（[David
Batty](../Page/:en:David_Batty.md "wikilink")）去壯大球隊的實力。當季，[列斯聯以第](../Page/列斯聯.md "wikilink")4名完成賽事。1992年，[列斯聯贏得改制](../Page/列斯聯.md "wikilink")[英超前最後一個甲組聯賽冠軍](../Page/英超.md "wikilink")，直至2007年，韋健遜是最後一名[英格蘭籍領隊帶領球隊奪得](../Page/英格蘭.md "wikilink")[英格蘭聯賽冠軍](../Page/英格蘭.md "wikilink")，除了贏得甲組冠軍外，韋健遜更帶領球隊於同年以4-3擊敗[利物浦而奪得](../Page/利物浦足球俱乐部.md "wikilink")[慈善盾](../Page/英格蘭社區盾.md "wikilink")，然而，[列斯聯並不能適應](../Page/列斯聯.md "wikilink")[英超](../Page/英超.md "wikilink")，屢季在下游徘徊，1996/97球季，[列斯聯頭場對](../Page/列斯聯.md "wikilink")[曼聯賽事中](../Page/曼聯.md "wikilink")，以4-0落敗，韋健遜賽後迅即被解僱。

### 英格蘭

離開[列斯聯後](../Page/列斯聯.md "wikilink")，韋健遜成為[英格蘭足球總會的技術主管](../Page/英格蘭足球總會.md "wikilink")，並先後於1999年和2000年擔任[英格蘭的臨時領隊](../Page/英格蘭足球代表隊.md "wikilink")，除此之外，韋健遜亦曾擔任[英格蘭U21的領隊](../Page/英格蘭U21.md "wikilink")。

### 新特蘭

2002年，韋健遜離開[英格蘭足球總會](../Page/英格蘭足球總會.md "wikilink")，轉為擔任[新特蘭領隊](../Page/新特蘭足球會.md "wikilink")\[1\]。然而，[新特蘭在](../Page/新特蘭足球會.md "wikilink")[英超取得當時最低的](../Page/英超.md "wikilink")19分完成整個賽季（後來被[打比郡於](../Page/打比郡.md "wikilink")2008年被打破），最終韋健遜於2003年3月被解僱。\[2\]

### 現時

2004年3月，韋健遜成為[上海申花的主教練](../Page/上海申花.md "wikilink")，不過僅兩個月便因私人理由離職。\[3\]2004年10月，韋健遜成為[李斯特城的一隊教練](../Page/李斯特城.md "wikilink")。\[4\]2004年12月，韋健遜成為[諾士郡的](../Page/諾士郡.md "wikilink")[非執行董事](../Page/非執行董事.md "wikilink")\[5\]，於2006年6月到2007年9月間任技術總監，其後完全脫離諾士郡。2009年1月9日獲委為[錫周三非受薪足球顧問](../Page/谢周三足球俱乐部.md "wikilink")\[6\]，2010年5月17日韋健遜以義務形式接任受到財困兼降班[英甲的錫周三的臨時主席](../Page/英甲.md "wikilink")。

韋健遜現在是[英格蘭足球聯賽領隊協會](../Page/英格蘭足球聯賽領隊協會.md "wikilink")（League Managers
Association）的主席\[7\]。

## 參考資料

[Category:英格蘭足球運動員](../Category/英格蘭足球運動員.md "wikilink")
[Category:錫周三球員](../Category/錫周三球員.md "wikilink")
[Category:白禮頓球員](../Category/白禮頓球員.md "wikilink")
[Category:波士頓聯球員](../Category/波士頓聯球員.md "wikilink")
[Category:波士頓聯領隊](../Category/波士頓聯領隊.md "wikilink")
[Category:諾士郡領隊](../Category/諾士郡領隊.md "wikilink")
[Category:錫周三領隊](../Category/錫周三領隊.md "wikilink")
[Category:列斯聯領隊](../Category/列斯聯領隊.md "wikilink")
[Category:英格蘭足球代表隊領隊](../Category/英格蘭足球代表隊領隊.md "wikilink")
[Category:新特蘭領隊](../Category/新特蘭領隊.md "wikilink")
[Category:上海申花教练](../Category/上海申花教练.md "wikilink")
[Category:英超領隊](../Category/英超領隊.md "wikilink")

1.  [Wilkinson takes Sunderland
    job](http://news.bbc.co.uk/sport1/hi/football/teams/s/sunderland/2315749.stm)
2.  [Fans' shock at Wilkinson
    departure](http://news.bbc.co.uk/1/hi/england/2838339.stm)
3.  [Wilkinson leaves
    Shanghai](http://news.bbc.co.uk/sport1/hi/football/3731223.stm)
4.  [Wilkinson accepts Leicester
    role](http://news.bbc.co.uk/sport1/hi/football/teams/l/leicester_city/3735504.stm)
5.  [Wilkinson returns to Notts
    County](http://news.bbc.co.uk/sport1/hi/football/teams/n/notts_county/4135371.stm)
6.   [Wilkinson returns to
    Hillsborough](http://news.bbc.co.uk/sport2/hi/football/teams/s/sheff_wed/7819513.stm)
7.  [LMA Structure](http://www.leaguemanagers.com/lma/structure-3.html)