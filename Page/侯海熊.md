**侯海熊**，[台灣](../Page/台灣.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")，[國立台灣大學](../Page/國立台灣大學.md "wikilink")[法律學系](../Page/法律學系.md "wikilink")[畢業](../Page/畢業.md "wikilink")，前[民主進步黨](../Page/民主進步黨.md "wikilink")（民進黨）黨員，曾任[律師](../Page/律師.md "wikilink")、[台灣台南地方法院檢察署](../Page/台灣台南地方法院檢察署.md "wikilink")[檢察官](../Page/檢察官.md "wikilink")、[台灣台北地方法院檢察署檢察官](../Page/台灣台北地方法院檢察署.md "wikilink")、第2屆[全國不分區](../Page/全國不分區.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")、民進黨第2屆中央執行委員、民進黨[嘉義縣黨部主任委員](../Page/嘉義縣.md "wikilink")、泛亞國際證券投資顧問股份有限公司董事長。

1985年[嘉義縣長](../Page/嘉義縣長.md "wikilink")[選舉](../Page/1985年中華民國縣市長選舉.md "wikilink")，[黨外推薦的侯海熊與](../Page/黨外.md "wikilink")[中國國民黨](../Page/中國國民黨.md "wikilink")（國民黨）提名的[何嘉榮參選](../Page/何嘉榮.md "wikilink")，侯海熊以七千票、9%選票之差落選。

1992年，侯海熊屢次向《[自立早報](../Page/自立早報.md "wikilink")》社長[吳豐山指控當時](../Page/吳豐山.md "wikilink")《自立早報》主跑[立法院](../Page/立法院.md "wikilink")[新聞的](../Page/新聞.md "wikilink")[記者](../Page/記者.md "wikilink")[羅碧霞的報導對他不友善](../Page/羅碧霞.md "wikilink")，並揚言控告《自立早報》與羅碧霞。吳豐山找來羅碧霞詢問原委之後，與羅碧霞共同決定「任憑侯海熊去告」，羅碧霞照常主跑立法院新聞。1995年1月11日，由於侯海熊關切涉入[洪福案](../Page/洪福案.md "wikilink")（1994年[洪福證券](../Page/洪福證券.md "wikilink")[違約交割案](../Page/違約交割.md "wikilink")）的立委[翁大銘的](../Page/翁大銘.md "wikilink")[公司被](../Page/公司.md "wikilink")[搜索一案](../Page/搜索.md "wikilink")，侯海熊被民進黨中央評議委員會（中評會）以「時地不宜」為由開除黨籍，連帶喪失不分區立委資格，[江鵬堅遞補其缺額](../Page/江鵬堅.md "wikilink")。1995年3月25日，侯海熊因洪福案被判處[有期徒刑八個月](../Page/有期徒刑.md "wikilink")。1995年12月，侯海熊落選。

1994年間，台灣[詐欺慣犯](../Page/詐欺.md "wikilink")[張儷瓊以中文假名](../Page/張儷瓊.md "wikilink")「黃雅玲」與英文假名「Alin」打[電話給侯海熊](../Page/電話.md "wikilink")，自稱是[三陽工業董事長](../Page/三陽工業.md "wikilink")[黃世惠的女兒](../Page/黃世惠.md "wikilink")，說黃世惠外孫罹患[肝病必須在](../Page/肝病.md "wikilink")[三軍總醫院住院治療](../Page/三軍總醫院.md "wikilink")，請侯海熊代為安排病床；侯海熊信以為真，被[詐騙](../Page/詐騙.md "wikilink")[新台幣](../Page/新台幣.md "wikilink")6550萬元。事後張儷瓊避不見面，引起侯海熊懷疑，侯海熊直接打電話到三陽工業，得知黃世惠確實有一個女兒Alin長年居住國外、不太會說中文，才知道自己上當。2003年8月26日，[棄保潛逃的張儷瓊在逛街時被其他被害人認出](../Page/棄保潛逃.md "wikilink")，當場被[逮捕](../Page/逮捕.md "wikilink")。

2001年10月，泛亞投顧停止對外營業。2002年9月2日，泛亞投顧正式申報停業登記，並已註銷[中華民國證券投資信託暨顧問商業同業公會](../Page/中華民國證券投資信託暨顧問商業同業公會.md "wikilink")（投信投顧公會）會籍。2009年，[中福振業改選](../Page/中福振業.md "wikilink")[董事](../Page/董事.md "wikilink")、監事，侯海熊當選監察人。

## 外部連結

  - [立法院國會圖書館
    侯海熊個人資料](http://npl.ly.gov.tw/do/www/commissionerInfo?id=628&expire=02&act=commit&appdateId=0)

[H侯](../Category/被開除民主進步黨黨籍者.md "wikilink")
[H侯](../Category/民主進步黨中執委.md "wikilink")
[H侯](../Category/台灣律師.md "wikilink")
[H侯](../Category/中華民國檢察官.md "wikilink")
[H侯](../Category/第2屆中華民國立法委員.md "wikilink")
[H侯](../Category/國立臺灣大學法律學院校友.md "wikilink")