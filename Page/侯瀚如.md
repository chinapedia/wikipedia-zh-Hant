[Hou_hanrou.jpg](https://zh.wikipedia.org/wiki/File:Hou_hanrou.jpg "fig:Hou_hanrou.jpg")
**侯瀚如**（）是一位藝術策展人，批評家。

## 生平

1963年生於中國[廣州](../Page/廣州.md "wikilink")，1988年碩士畢業，1990年移居[巴黎](../Page/巴黎.md "wikilink")。曾為[荷蘭](../Page/荷蘭.md "wikilink")[阿姆斯特丹皇家美術學院教授](../Page/阿姆斯特丹皇家美術學院.md "wikilink")、[美國](../Page/美國.md "wikilink")[明尼蘇達州](../Page/明尼蘇達州.md "wikilink")[沃克藝術中心國際藝術顧問委員會成員](../Page/沃克藝術中心.md "wikilink")、中國當代藝術奬評委。同時也是歐洲、亞洲以及美國很多藝術基金會的委員和評委。英文文集《在中間地帶》（On
the Mid-Ground）於2002年由 Timezone 8 出版。目前任職於美國舊金山藝術學院（[San Francisco Art
Institute](http://en.wikipedia.org/wiki/San_Francisco_Art_Institute)）。

旅法之前，侯瀚如已活躍於中國國內的前衛藝術界，參與了包括1989年於[北京中國美術館舉行的](../Page/北京.md "wikilink")「中國現代藝術展」（China
/
Avant-Garde）在內的多個重要展覽的策劃工作。1990年代初，侯與瑞士出生的策展人[漢斯·烏爾裡希·奧布裡斯特](../Page/漢斯·烏爾裡希·奧布裡斯特.md "wikilink")（[Hans
Ulrich
Obrist](http://en.wikipedia.org/wiki/Hans_Ulrich_Obrist)）共同策劃了「運動中的城市」（Cities
on the
Move）展覽，在亞洲多個城市展出，引起較大反響。兩人於2005年再次合作，策劃了第二屆廣州三年展（策展人尚包括郭曉彥）。除此之外亦是2007年伊斯坦布爾雙年展總策展人。

侯瀚如被廣泛認為是將中國當代藝術引介到西方世界中的重要人物之一。

## 外部链接

  - [Archive of writing on *Art
    Practical*](http://www.artpractical.com/archive/contributor/hou_hanru/)
  - [Artpratical.com](http://www.artpractical.com/feature/sharing_a_sensibility_a_conversation_with_hou_hanru/)

[category:广州人](../Page/category:广州人.md "wikilink")
[category:侯姓](../Page/category:侯姓.md "wikilink")

[Category:中国策展人](../Category/中国策展人.md "wikilink")
[Category:中央美术学院校友](../Category/中央美术学院校友.md "wikilink")
[Category:旧金山艺术学院教师](../Category/旧金山艺术学院教师.md "wikilink")
[Category:中国艺术评论家](../Category/中国艺术评论家.md "wikilink")
[Category:在法国的中国人](../Category/在法国的中国人.md "wikilink")
[Category:在意大利的中国人](../Category/在意大利的中国人.md "wikilink")
[Category:在美国的中国人](../Category/在美国的中国人.md "wikilink")
[Category:广东艺术家](../Category/广东艺术家.md "wikilink")