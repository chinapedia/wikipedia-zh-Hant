**侯耀华**（），[满族人](../Page/满族人.md "wikilink")，生于[北平](../Page/北平.md "wikilink")，[喜剧](../Page/喜剧.md "wikilink")[演员](../Page/演员.md "wikilink")、[主持人](../Page/主持人.md "wikilink")，[相声大师](../Page/相声.md "wikilink")[侯宝林次子](../Page/侯宝林.md "wikilink")，相声演员[侯耀文的胞兄](../Page/侯耀文.md "wikilink")。

## 影视艺术界

侯耀华1984年开始涉足影视剧艺术界，主演过[电影](../Page/电影.md "wikilink")《[顽主](../Page/顽主.md "wikilink")》、《[减肥旅行团](../Page/减肥旅行团.md "wikilink")》、《[傍晚她敲响我的门](../Page/傍晚她敲响我的门.md "wikilink")》，[电视连续剧](../Page/电视连续剧.md "wikilink")《[大马路小胡同](../Page/大马路小胡同.md "wikilink")》、《[请拨315](../Page/请拨315.md "wikilink")》、《[编辑部的故事](../Page/编辑部的故事.md "wikilink")》、《[豁口](../Page/豁口.md "wikilink")》、《[小房东](../Page/小房东.md "wikilink")》等。同时，侯耀华也曾担任[电视主持人](../Page/电视主持人.md "wikilink")，主持过[北京电视台](../Page/北京电视台.md "wikilink")《[非常接触](../Page/非常接触.md "wikilink")》、山東[齐鲁电视台](../Page/齐鲁电视台.md "wikilink")《[家家乐天天](../Page/家家乐天天.md "wikilink")》。

2009年12月16日，侯耀华正式拜相声演员[常宝华为师](../Page/常宝华.md "wikilink")，拜师仪式由[李金斗主持](../Page/李金斗.md "wikilink")，[王谦祥](../Page/王谦祥.md "wikilink")、[李增瑞](../Page/李增瑞.md "wikilink")、[石富宽](../Page/石富宽.md "wikilink")、[唐杰忠等笑星前来捧场](../Page/唐杰忠.md "wikilink")\[1\]。

## 争议

  - [侯耀文因病过世后](../Page/侯耀文.md "wikilink")，其[遗产分配曾陷入纠纷并引发](../Page/遗产.md "wikilink")[诉讼](../Page/诉讼.md "wikilink")，引发社会关注。诉讼最终以[调解解决](../Page/调解.md "wikilink")\[2\]\[3\]\[4\]\[5\]\[6\]。
  - 2009年11月、2015年9月，侯耀华被点名曝光代言多个虚假广告\[7\]。
  - 2017年12月，自称侯耀华女徒弟的安娜金在[微博上接连发出二人的亲密合影](../Page/微博.md "wikilink")，此事引发争议\[8\]\[9\]\[10\]。

## 参注

## 外部链接

  - [《非常接触》侯耀华博客](http://blog.sina.com.cn/houyaohua)

`|width=30% align=center|`**`從師`**
`|width=40% align=center|`**`輩分`**
`|width=30% align=center|`**`收徒`**
`|-`

|width=30% align=center|[常宝华](../Page/常宝华.md "wikilink")

`|width=40% align=center|`**`文`**
` |width=30% align=center |`[`劉樺`](../Page/劉樺.md "wikilink")`、`[`魏三兒`](../Page/魏三兒.md "wikilink")`、`[`釘當`](../Page/釘當.md "wikilink")`、`[`陳創`](../Page/陳創.md "wikilink")`、`[`畢大鵬`](../Page/畢大鵬.md "wikilink")`、`[`謝雷`](../Page/謝雷.md "wikilink")`、`[`薛英傑`](../Page/薛英傑.md "wikilink")`、`[`李立群`](../Page/李立群.md "wikilink")`、`[`張國慶`](../Page/張國慶.md "wikilink")`、`[`張建國`](../Page/張建國.md "wikilink")`、`[`王自健`](../Page/王自健.md "wikilink")`、`[`楊全明`](../Page/楊全明.md "wikilink")`、`[`張洪順`](../Page/張洪順.md "wikilink")`、`[`楊曉弟`](../Page/楊曉弟.md "wikilink")`、`[`鄒樹仁`](../Page/鄒樹仁.md "wikilink")`、`[`徐霆雷`](../Page/徐霆雷.md "wikilink")`、`[`魏鵬`](../Page/魏鵬.md "wikilink")`、`[`何沄伟`](../Page/何沄伟.md "wikilink")\[11\]

|-

[7](../Category/相声演员.md "wikilink")
[Category:中国小品演员](../Category/中国小品演员.md "wikilink")
[Category:中國主持人](../Category/中國主持人.md "wikilink")
[Category:中国电视男演员](../Category/中国电视男演员.md "wikilink")
[Category:中国电影男演员](../Category/中国电影男演员.md "wikilink")
[H](../Category/满族人.md "wikilink")
[Y](../Category/侯姓.md "wikilink")

1.
2.  [侯耀华被指家产过亿
    无理由侵占侯耀文遗产(图)_娱乐_凤凰网](http://ent.ifeng.com/special/star/houyaowenyichan/news/200907/0726_7477_1268931.shtml)
3.  [侯耀华不分侯耀文遗产因担心有辱侯宝林名望？_影音娱乐_新浪网](https://ent.sina.com.cn/s/m/2009-07-26/04182625111.shtml)
4.  [网友曝侯耀文遗产纠纷案内幕 侯耀华有难言之隐 - 社会能见度 -
    华声在线](https://www.voc.com.cn/article/200907/200907271040205271.html)
5.  [侯耀文遗产案庭审结束
    双方同意调解(图)-搜狐娱乐](https://yule.sohu.com/20100604/n272576891.shtml)
6.  [侯耀华纪念侯耀文
    称遗产纠纷已过去_影音娱乐_新浪网](https://ent.sina.com.cn/j/2012-06-08/22113653118.shtml)
7.  [那些年，侯耀华代言的那些虚假广告](https://k.sina.com.cn/article_3217975050_bfce670a001001dog.html)，新浪看点
8.  [网曝侯耀华送女徒弟假包
    经纪人发声否认师徒关系](https://ent.sina.com.cn/zz/2017-12-14/doc-ifyptfcn0118984.shtml)
9.  [侯耀华晚节不保？网红女徒弟安娜金澄清：正当的师徒关系](http://new.qq.com/omn/20171213A01AIR.html)
10. [侯耀华儿子疑发声怒喷安娜金是垃圾
    暗讽侯耀华不洁身自好_中华头条_中华网](http://toutiao.china.com/tpsy/ltjsrph/13000457/20171214/31813930.html)
11.