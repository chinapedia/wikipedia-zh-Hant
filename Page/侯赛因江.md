|       Housaiyinjiang Jialili       |
| :--------------------------------: |
|  [简体字](../Page/简体字.md "wikilink")  |
|  [繁体字](../Page/繁体字.md "wikilink")  |
| [汉语拼音](../Page/汉语拼音.md "wikilink") |
|        Haishanjiang Jialili        |
|  [简体字](../Page/简体字.md "wikilink")  |
|  [繁体字](../Page/繁体字.md "wikilink")  |
| [汉语拼音](../Page/汉语拼音.md "wikilink") |
|            Yushanjiang             |
|  [简体字](../Page/简体字.md "wikilink")  |
|  [繁体字](../Page/繁体字.md "wikilink")  |
| [汉语拼音](../Page/汉语拼音.md "wikilink") |
|                                    |

**玉山江中文名**

**侯赛因江·贾里力**（，[拉丁維文](../Page/拉丁維文.md "wikilink")：****，[土耳其语](../Page/土耳其语.md "wikilink")：，，部分文件显示他1955年出生，又翻译为**玉山江**）是一位拥有[加拿大](../Page/加拿大.md "wikilink")[护照的新疆](../Page/护照.md "wikilink")[維吾爾裔人](../Page/維吾爾族.md "wikilink")。他被中國政府視為[疆獨份子](../Page/新疆獨立運動.md "wikilink")。

## 簡歷

侯赛因江出生于[中國](../Page/中華人民共和國.md "wikilink")[新疆](../Page/新疆.md "wikilink")[喀什](../Page/喀什.md "wikilink")。後來移民到加拿大，並在[安大略省](../Page/安大略省.md "wikilink")[伯靈頓居住](../Page/伯靈頓_\(安大略省\).md "wikilink")。

他因為参与[东突厥斯坦伊斯兰运动](../Page/东突厥斯坦伊斯兰运动.md "wikilink")（简称“东伊运”）的[新疆獨立運動被拘捕](../Page/新疆獨立運動.md "wikilink")。之後，他逃离中国，並通过[吉爾吉斯斯坦到達](../Page/吉爾吉斯斯坦.md "wikilink")[土耳其](../Page/土耳其.md "wikilink")。中国政府通过[国际刑警组织发出](../Page/国际刑警组织.md "wikilink")[红色通缉令](../Page/红色通缉令.md "wikilink")。之后玉山江在土耳其[伊斯坦布尔的收容所獲](../Page/伊斯坦布尔.md "wikilink")[联合国难民署鑑別为](../Page/联合国难民署.md "wikilink")[难民](../Page/难民.md "wikilink")。在被授予难民身份以后，通過《[日内瓦公约](../Page/日内瓦公约.md "wikilink")》，得到加拿大认可了他的难民，並接纳了他到當地定居。

侯赛因江在在2001年到達加拿大。2005年加国授予其公民身份。玉山江曾在[哈密尔顿一个](../Page/哈密尔顿_\(安大略\).md "wikilink")[清真寺担当](../Page/清真寺.md "wikilink")[阿訇](../Page/阿訇.md "wikilink")。

2006年在前往[烏孜別克斯坦與妻子探親時被當地的](../Page/烏孜別克斯坦.md "wikilink")[國際刑警拘捕](../Page/國際刑警.md "wikilink")，並移送往中國。自從2006年3月，侯赛因江一直被中國政府拘禁，因为中国政府认为国际刑警组织发出对玉山江的通缉令在先，其变更身份后成为难民在后，等于是借改变身份逃脱国际通缉。故不认可其加拿大国籍身份。在2007年4月19日，被判处[终身监禁](../Page/终身监禁.md "wikilink")。\[1\]

## 請參閱

  - [Omar Khadr](http://en.wikipedia.org/wiki/Omar_Khadr)
  - [新疆独立运动](../Page/新疆独立运动.md "wikilink")

## 參考

<references />

## 外部連結

  - [“玉山江”案绷紧中加政治神经](http://www.westca.com/Forums/viewtopic/t=110060/lang=schinese.html)
  - [玉山江被判无期引爆中加外交口舌战](http://lateline.muzi.net/news/ll/fanti/1465223.shtml?q=&cc=11038&a=on)
  - [哈珀对玉山江和卡塔 (Omar Khadr)
    实行双重标准](http://www.creaders.net/newsViewer.php?id=708282)
  - [欺善怕恶 加人遭美拘押禁见领事
    哈珀不敢提](http://www.looktoronto.com/ss/?viewnews-1126.html)
  - [Liberal MP takes up cause of Canadian accused of terrorism
    charges](http://www.politicswatch.com/celil-june15-2006.htm)
  - [Made By China Blog Support](http://www.madebychina.blogspot.com)
  - [Toronto Star: Wife begs China to release husband facing execution;
    Former activist accused of terrorism; Canadian consulate has had no
    success](https://web.archive.org/web/20100429230621/http://www.uhrp.org/articles/197/1/Wife-begs-China-to-release-husband-facing-execution/Wife-begs-China-to-release-husband-facing-execution.html)
    July 21, 2006

[Category:中华人民共和国罪犯](../Category/中华人民共和国罪犯.md "wikilink")
[Category:中华人民共和国穆斯林](../Category/中华人民共和国穆斯林.md "wikilink")
[Category:中华人民共和国籍难民](../Category/中华人民共和国籍难民.md "wikilink")
[Category:归化加拿大公民的中华人民共和国人](../Category/归化加拿大公民的中华人民共和国人.md "wikilink")
[Category:加拿大穆斯林](../Category/加拿大穆斯林.md "wikilink")
[Category:中华人民共和国维吾尔族政治人物](../Category/中华人民共和国维吾尔族政治人物.md "wikilink")
[Category:东突厥斯坦伊斯兰运动成员](../Category/东突厥斯坦伊斯兰运动成员.md "wikilink")
[Category:喀什人](../Category/喀什人.md "wikilink")

1.  [中国判处加籍维族异议人士终身监禁](http://news.bbc.co.uk/chinese/simp/hi/newsid_6570000/newsid_6571200/6571225.stm)
    BBC