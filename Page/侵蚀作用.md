[Walking_Tree_at_Lookout_Point_at_Grand_Canyon_of_the_Yellowstone.jpg](https://zh.wikipedia.org/wiki/File:Walking_Tree_at_Lookout_Point_at_Grand_Canyon_of_the_Yellowstone.jpg "fig:Walking_Tree_at_Lookout_Point_at_Grand_Canyon_of_the_Yellowstone.jpg")
[NegevWadi2009.JPG](https://zh.wikipedia.org/wiki/File:NegevWadi2009.JPG "fig:NegevWadi2009.JPG")的直接受害者\]\]
**侵蚀作用**（）或**水土流失**（）是[自然界的一种现象](../Page/自然界.md "wikilink")，是指[地球的表面不断受到](../Page/地球.md "wikilink")[风](../Page/风.md "wikilink")、[水的磨损](../Page/水.md "wikilink")。这个过程，陆地除了被侵蚀之外，蚀出的物质還會被带走，堆积在其他地方，主要堆积在[海裡](../Page/海.md "wikilink")。

侵蚀由水流、冰移或风吹造成。

而由[雪](../Page/雪.md "wikilink")、[霜](../Page/霜.md "wikilink")、[阳光](../Page/阳光.md "wikilink")、[雨水对](../Page/雨水.md "wikilink")[地表的作用則叫做](../Page/地表.md "wikilink")[风化作用](../Page/风化作用.md "wikilink")，暴露在[空气中的岩石会受到](../Page/空气.md "wikilink")[天气影响](../Page/天气.md "wikilink")，长期冷热交替，有些岩石会因而开裂。岩石中的水结冰时会膨胀，这个也会使岩石开裂。石头受到重力的挤压后突然减压，也会使岩石碎裂。雨水是稀酸，可溶化或改变岩石内的化学物质。植物的根和穴居动物也会加速风化。

因为风化而碎裂的石块会由水、冰、风带走，而如果水、冰、风内如有石块，即使非常细小，也能大大加强侵蚀的作用，改变地貌。

成因水土流失可能是由于自然环境引起的，譬如地势陡峭，突发大量[降水](../Page/降水.md "wikilink")，或者[地质变化](../Page/地质.md "wikilink")。，譬如过度放牧，开垦土地，[采伐森林](../Page/采伐森林.md "wikilink")，進行各項工程建設等等。

害處侵蚀是自然环境恶化的重要原因。由于水的流动，带走了[地球表面的](../Page/地球.md "wikilink")[土壤](../Page/土壤.md "wikilink")，使得土地变得贫瘠，[岩石裸露](../Page/岩石.md "wikilink")，[植被破坏](../Page/植被.md "wikilink")，[生态恶化](../Page/生态.md "wikilink")。

这种自然现象可以是循环作用的，生态恶化引起水土流失，水土流失又使得生态更加恶化。于是土地退化，无法耕种。植物死亡，地表裸露。恶劣的生态又更导致气候的变化，使得生物可生存的环境变坏。

水土流失还会使得泥沙淤积，河床抬高，引起水流不畅，水质混浊，甚至导致洪水泛滥，河流改道，就如中国的[黄河](../Page/黄河.md "wikilink")。

风化与侵蚀的区别。风化是破坏后改变物体形状，而侵蚀是转移物体，从而改变地貌。

## 正面影響

侵蚀作用的正面作用，如河流将泥沙碎石带入下游，形成平坦的冲积平原，适合于人类和动植物的繁衍生存。

## 河流

河水受重力作用，由高處往低處移動產生動能，對地表進行侵蝕。而河流的侵蝕作用可分為[向下侵蝕](../Page/向下侵蝕.md "wikilink")、[向側侵蝕](../Page/向側侵蝕.md "wikilink")、[向源侵蝕三種](../Page/向源侵蝕.md "wikilink")。

## 參見

  - [侵蝕輪迴說](../Page/侵蝕輪迴說.md "wikilink")
  - [搬運作用](../Page/搬運作用.md "wikilink")
  - [堆積作用](../Page/堆積作用.md "wikilink")
  - [風化作用](../Page/風化作用.md "wikilink")
  - [沙漠化](../Page/沙漠化.md "wikilink")

[侵蚀](../Category/侵蚀.md "wikilink")
[Category:土壤科學](../Category/土壤科學.md "wikilink")
[Category:农学](../Category/农学.md "wikilink")
[Category:产业农业](../Category/产业农业.md "wikilink")
[Category:环境土壤科学](../Category/环境土壤科学.md "wikilink")
[Category:环境问题](../Category/环境问题.md "wikilink")