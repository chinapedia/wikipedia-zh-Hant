[Peter_iredale_sunset_edited1.jpg](https://zh.wikipedia.org/wiki/File:Peter_iredale_sunset_edited1.jpg "fig:Peter_iredale_sunset_edited1.jpg")

**俄勒冈州**（）是[美国的一个](../Page/美国.md "wikilink")[州](../Page/美国州份.md "wikilink")，位于美国[西北的](../Page/美國西北部.md "wikilink")[太平洋沿岸](../Page/美國太平洋沿岸地區.md "wikilink")，西临[太平洋](../Page/太平洋.md "wikilink")、北接[华盛顿州](../Page/华盛顿州.md "wikilink")、东面是[爱德荷州](../Page/爱德荷州.md "wikilink")、南面是[加利福尼亚州和](../Page/加利福尼亚州.md "wikilink")[内华达州](../Page/内华达州.md "wikilink")。2012年人口3,899,353。州别称：**海狸州**，邮编代号**OR**。本州的[行政區劃](../Page/俄勒冈州行政区划.md "wikilink")，共管轄36個[郡](../Page/郡.md "wikilink")。

## 歷史

俄勒岡州早在一萬五千年前已有人類居住的紀錄，人口主要集中在西面近海的[河口](../Page/河口.md "wikilink")[平原](../Page/平原.md "wikilink")。十六世紀時有大量[印第安人居住在俄勒岡州](../Page/印第安人.md "wikilink")。

十八世紀後期[歐洲人開始前來俄勒岡州探險](../Page/歐洲.md "wikilink")，自此俄勒岡州就湧現了不少公司前來開發。1850年後，俄勒岡州人口不斷上升，並有鐵路興建把俄勒岡州的糧食運到東岸。到二十世紀，隨著水利工程的完成，[伐木業的興起](../Page/伐木.md "wikilink")，使工商業開始在這個州份興起。

## 法律與政府

俄勒岡州跟其他美國的州份一樣，都是行政、立法和司法三權分立，州政府沒有國防和外交等層級實權。

[共和黨和](../Page/美國共和黨.md "wikilink")[民主黨在這個州份的政府並沒有絕對優勢](../Page/美國民主黨.md "wikilink")。但自[1988年美國總統選舉起](../Page/1988年美國總統選舉.md "wikilink")，每屆總統大選，這個州份都是由民主黨人勝出。

## 地理

州名[印第安语意为](../Page/印第安语.md "wikilink")“美丽之水”（指[哥伦比亚河](../Page/哥伦比亚河.md "wikilink")）。地形起伏较大，西部有[喀斯喀特山脉和海岸山脉](../Page/喀斯喀特山脉.md "wikilink")，东半部全为[高原](../Page/高原.md "wikilink")。西部沿岸多[雨](../Page/雨.md "wikilink")，东部高原少雨。[森林占全州面积一半](../Page/森林.md "wikilink")，与[华盛顿州同为林业最盛的州](../Page/华盛顿州.md "wikilink")。木材、纸浆、家具制造业很发达。[威拉米特河流域人口密集](../Page/威拉米特河.md "wikilink")，州人口70％集中在这里，农业发达，栽培[小麦](../Page/小麦.md "wikilink")、[燕麦](../Page/燕麦.md "wikilink")、[马铃薯及各种](../Page/马铃薯.md "wikilink")[水果](../Page/水果.md "wikilink")。东部高原主要为[小麦](../Page/小麦.md "wikilink")、肉牛的农牧业区。沿海以[鲑鱼为主的渔业很盛](../Page/鲑鱼.md "wikilink")。工业仅以果品罐头业为重要。重要自然风景区有[火山口湖国家公园和](../Page/火山口湖国家公园.md "wikilink")[富德火山等](../Page/富德火山.md "wikilink")。

## 旅遊與觀光資訊

  - 綺麗湖國家公園（Crater Lake National Park）
  - [哥倫比亞河大峽谷](../Page/哥倫比亞河.md "wikilink")

## 經濟

俄勒冈是美國的一個免稅州，居民年收入约8萬美元，体育用品公司[Nike](../Page/Nike.md "wikilink")、户外运动品牌[哥倫比亞和科技公司Tektronix的全球总部位于该州](../Page/美國哥倫比亞運動服裝公司.md "wikilink")。

## 人口

2012年人口普查，俄勒岡州有3,899,353人。

目前俄勒岡州的族群比例可分為：

  - 83.5%－白人
  - 1.6%－非裔人
  - 8.0%－[拉美裔人](../Page/拉美裔人.md "wikilink")
  - 2.9%－亞裔人
  - 1.3%－[印地安原住民](../Page/印第安人.md "wikilink")
  - 3.2%－混合的族群

俄勒岡州的前五大居民祖先為：[德國人](../Page/德國.md "wikilink")（20.5%）、[英國人](../Page/英國.md "wikilink")（13.2%）、[愛爾蘭人](../Page/愛爾蘭.md "wikilink")（11.9%）、[墨西哥人](../Page/墨西哥.md "wikilink")（6.3%）。

該州有6.5%的人口在5歲以下，24.7%的人口在18歲以下，以及12.8%的人口在65歲以上。女性約占全人口比例50.4%。

### 宗教

[缩略图](https://zh.wikipedia.org/wiki/File:Crater_lake_oregon.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Portland_Skyline-02.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Gresham_Carnegie_Library-1.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Oregon_State_Capitol_1.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:RoseGardenArenaInterior3.jpg "fig:缩略图")
[宗教上](../Page/宗教.md "wikilink")，俄勒岡州居民的信仰比例如下：

  - 67%－基督宗教
      - 47%－[新教](../Page/新教.md "wikilink")
      - 14%－[羅馬天主教](../Page/羅馬天主教.md "wikilink")
      - 6%－其他基督宗教
  - 6%－其他宗教
  - 27%－無宗教信仰

在俄勒岡州的新教徒中，前三大派別為：[福音派](../Page/福音派.md "wikilink")（30%）、[主流新教](../Page/主流新教.md "wikilink")（16%）與其他[新教](../Page/新教.md "wikilink")（1%）。

雖然俄勒岡州大多是新教徒，但此州的教堂數目是相當少的，有可能是美國西海岸各州中最少的（一般皆為80%，但俄勒岡州只有12%）。

## 重要城鎮

州內人口較多的城市依次為：

1.  [波特蘭](../Page/波特蘭_\(奧勒岡州\).md "wikilink") (Portland)

2.  [塞勒姆](../Page/塞勒姆.md "wikilink") (Salem)

3.  [尤金](../Page/尤金_\(俄勒岡州\).md "wikilink") (Eugene)

4.  [葛雷森](../Page/葛雷森.md "wikilink") (Gresham)

5.  [希爾斯伯勒](../Page/希爾斯伯勒_\(俄勒岡州\).md "wikilink") (Hillsboro)，波特蘭都會區

6.  [比佛頓市](../Page/比佛頓市_\(俄勒岡州\).md "wikilink")（Beaverton），波特蘭都會區

7.  [本德](../Page/本德.md "wikilink") (Bend)

8.  [梅德福](../Page/梅德福_\(俄勒岡州\).md "wikilink")（Medford）

9.  [斯普林菲爾德](../Page/斯普林菲爾德_\(俄勒岡州\).md "wikilink")（Springfield）

10. [科瓦利斯](../Page/科瓦利斯_\(俄勒岡州\).md "wikilink") (Corvallis)

11. [奧爾巴尼](../Page/奧爾巴尼_\(俄勒岡州\).md "wikilink") (Albany)

12. [泰格德](../Page/泰格德_\(奧勒岡州\).md "wikilink") (Tigard)

13. [奥斯威戈湖](../Page/奥斯威戈湖.md "wikilink")（Lake Oswego）

14. (Keizer)

15. [格兰茨帕斯](../Page/格兰茨帕斯.md "wikilink") (Grants Pass)

其他城市尚有[庫斯貝](../Page/庫斯貝.md "wikilink")（Coos
Bay）、[达尔斯](../Page/达尔斯.md "wikilink")（The
Dalles）、[圣海伦斯](../Page/圣海伦斯_\(俄勒岡州\).md "wikilink") (St.
Helens)、[蒂拉穆克](../Page/蒂拉穆克.md "wikilink")（Tillamook）、[胡德里弗](../Page/胡德里弗.md "wikilink")（Hood
River）等。

## 教育

### 州立大學

  - [俄勒岡大學](../Page/俄勒岡大學.md "wikilink") (University of Oregon)
  - [俄勒岡州立大學](../Page/俄勒岡州立大學.md "wikilink") (Oregon State University)
  - [波特蘭州立大學](../Page/波特蘭州立大學.md "wikilink")
  - [東俄勒岡州立大學](../Page/東俄勒岡州立大學.md "wikilink")
  - [西俄勒岡州立大學](../Page/西俄勒岡州立大學.md "wikilink")
  - [南俄勒岡大學](../Page/南俄勒岡大學.md "wikilink")
  - [俄勒冈理工学院](../Page/俄勒冈理工学院.md "wikilink")（Oregon Institute of
    Technology）
  - [俄勒冈医科大学](../Page/俄勒冈医科大学.md "wikilink")（Oregon Health Science
    University）

此外，州政府还资助17所社区学院。

### 私立大學

  - [波特蘭大學](../Page/波特蘭大學.md "wikilink")
  - [林菲爾德學院](../Page/林菲爾德學院.md "wikilink")（Linfield College）
  - [路易斯-克拉克學院](../Page/路易斯-克拉克學院.md "wikilink")（Lewis and Clark
    College）
  - [太平洋大学](../Page/太平洋大学_\(俄勒冈州\).md "wikilink")（Pacific University）
  - [瑞德學院](../Page/瑞德學院.md "wikilink")（Reed College）

## 重要體育團體

### 棒球

  - [小聯盟](../Page/小聯盟.md "wikilink")
      - 雅吉玛熊（Yakima
        Bears，短期1A級西北聯盟，母隊：[亞利桑那響尾蛇隊](../Page/亞利桑那響尾蛇隊.md "wikilink")）
      - 蕯勒姆／凱澤火山（Salem-Keizer
        Volcanoes，短期1A級西北聯盟，母隊：[舊金山巨人](../Page/舊金山巨人.md "wikilink")）
      - 尤金翠玉（Eugene
        Emeralds，短期1A級西北聯盟，母隊：[聖地牙哥教士](../Page/聖地牙哥教士.md "wikilink")）

### 籃球

  - [国家篮球协会](../Page/国家篮球协会.md "wikilink")
      - [波特兰开拓者](../Page/波特兰开拓者.md "wikilink")
  - 全国大学生体育协会（[NCAA](../Page/NCAA.md "wikilink"))
      - 美国太平洋大学

### 美式足球

  - 全国大学生体育协会（[NCAA](../Page/NCAA.md "wikilink"))
      - 俄勒冈大学（鸭子）
      - 俄勒冈州立大学（海狸）

## 名人

  - [萊納斯·鮑林](../Page/萊納斯·鮑林.md "wikilink")（1901-1994）：化學家，曾獲[諾貝爾化學獎和](../Page/諾貝爾化學獎.md "wikilink")[和平獎](../Page/諾貝爾和平獎.md "wikilink")
  - [比爾·鮑爾曼](../Page/比爾·鮑爾曼.md "wikilink")（1911-1999）：田徑教練，[Nike品牌的創辦人之一](../Page/Nike.md "wikilink")
  - [喬治·伯納德·丹齊格](../Page/喬治·伯納德·丹齊格.md "wikilink")（1914-2005）：[數學家](../Page/數學家.md "wikilink")，有「線性規劃之父」之稱
  - [理查德·迪本科恩](../Page/理查德·迪本科恩.md "wikilink")（Richard
    Diebenkorn，1922-1993）：畫家
  - [沙可思](../Page/沙可思.md "wikilink")（Mendel Sachs，1927–）：理論物理學家
  - [卡爾·威曼](../Page/卡爾·威曼.md "wikilink")（1951-）：物理學家兼科學教育家，因[玻色–爱因斯坦凝聚的研究而獲](../Page/玻色–爱因斯坦凝聚.md "wikilink")[諾貝爾物理學獎得主](../Page/諾貝爾物理學獎.md "wikilink")

## 其他資訊

### 重要機場

  - [波特蘭國際機場](../Page/波特蘭國際機場.md "wikilink")（PDX）
  - [尤金機場](../Page/尤金機場.md "wikilink")（EUG）

### 重要高速公路

  - [5号州际公路](../Page/5号州际公路俄勒冈州段.md "wikilink")
  - [84号州际公路](../Page/84号州际公路_\(俄勒冈州至犹他州\).md "wikilink")
  - [101号美国国道](../Page/101号美国国道俄勒冈州段.md "wikilink")

## 友好省州

截至2009年11月17日，俄勒冈州的友好省州有：

<table>
<thead>
<tr class="header">
<th><p>友好省州</p></th>
<th><p>结好时间</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/福建省.md" title="wikilink">福建省</a></p></td>
<td><p>1984年[1]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1985年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/富山縣.md" title="wikilink">富山縣</a></p></td>
<td><p>1991年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/全羅南道.md" title="wikilink">全羅南道</a></p></td>
<td><p>1996年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/伊拉克庫德斯坦.md" title="wikilink">伊拉克庫德斯坦</a></p></td>
<td><p>2005年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/天津市.md" title="wikilink">天津市</a></p></td>
<td><p>2014年</p></td>
</tr>
</tbody>
</table>

## 參見

  - [傑佛遜州](../Page/傑佛遜州.md "wikilink")

## 参考文献

## 外部連結

[Category:美国州份](../Category/美国州份.md "wikilink")
[\*](../Category/俄勒冈州.md "wikilink")

1.