**俄勒岡縣**（**Oregon County,
Missouri**）是[美國](../Page/美國.md "wikilink")[密蘇里州南部的一個縣](../Page/密蘇里州.md "wikilink")，南邻[阿肯色州](../Page/阿肯色州.md "wikilink")。面積2,050平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口10,344人。縣治[奧爾頓](../Page/奧爾頓_\(密蘇里州\).md "wikilink")
(Alton)。

成立於1845年，以[俄勒岡準州命名](../Page/俄勒岡準州.md "wikilink")。

[O](../Category/密苏里州行政区划.md "wikilink")