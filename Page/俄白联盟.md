**俄白聯盟**（），前身是[俄白共同体](../Page/俄白共同体.md "wikilink")，是由[俄羅斯和](../Page/俄羅斯.md "wikilink")[白俄羅斯於](../Page/白俄羅斯.md "wikilink")1997年4月2日簽訂《俄羅斯和白俄羅斯聯盟條約》而設立。\[1\]两国公民可依据该条约在俄白两国间自由生活和定居。

## 相關條目

  - [俄白共同体](../Page/俄白共同体.md "wikilink")

## 參考文獻

[Category:俄罗斯联邦外交](../Category/俄罗斯联邦外交.md "wikilink")
[Category:白俄羅斯外交](../Category/白俄羅斯外交.md "wikilink")
[Category:1997年建立](../Category/1997年建立.md "wikilink")

1.