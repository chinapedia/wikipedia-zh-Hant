**卢布**（，**；复数，*rubli*；[符号](../Page/货币符号.md "wikilink")：**₽**；[ISO代码](../Page/ISO_4217.md "wikilink")：**RUB**）是[俄罗斯联邦的法定货币](../Page/俄罗斯联邦.md "wikilink")，由[俄罗斯联邦中央银行发行](../Page/俄罗斯联邦中央银行.md "wikilink")。它也是[阿布哈兹](../Page/阿布哈兹.md "wikilink")、[南奥塞梯](../Page/南奥塞梯.md "wikilink")、[顿涅茨克人民共和国和](../Page/顿涅茨克人民共和国.md "wikilink")[卢甘斯克人民共和国的法定货币](../Page/卢甘斯克人民共和国.md "wikilink")。卢布曾是[俄罗斯帝国和](../Page/俄罗斯帝国.md "wikilink")[苏联的货币](../Page/苏联.md "wikilink")。[白俄罗斯和](../Page/白俄罗斯.md "wikilink")[德涅斯特河沿岸也使用相同的](../Page/德涅斯特河沿岸.md "wikilink")[货币名称](../Page/卢布.md "wikilink")。卢布的辅助单位是“戈比”（，*kopeyka*；复数，*kopeyki*），1卢布
= 100戈比。卢布的[ISO 4217代码是](../Page/ISO_4217.md "wikilink") RUB
或643；而在1998年之前发行的“旧卢布”[货币改值之前的代码是](../Page/货币改值.md "wikilink")
RUR 或810，新旧卢布的兑换比率为1 RUB = 1000 RUR。

## 歷史

### 早期－1921年底

盧布作為俄羅斯的貨幣單位已經大約有500年的歷史。從1710年開始，1盧布再分割100「戈比」。在各時期裡，1盧布硬幣的精確貴金屬含量均有所不同。在1704年的貨幣改革中，[彼得一世規定標準的](../Page/彼得一世.md "wikilink")1盧布含28克[白銀](../Page/白銀.md "wikilink")。雖然一般的盧布硬幣是銀幣，但也有使用黃金或白金鑄造的更高面額硬幣。

到了18世紀末，盧布設置為純金421朵雅純銀（幾乎完全等於18克）或27朵雅（幾乎完全等於1.2克）的純金，兩種金屬的價值大約15:1。1828年，白金硬幣開始使用，1盧布大約有77⅔朵雅（3.451克）。

1885年12月17日，盧布改用新的標準，銀幣成份並未改變，但金幣含量降為1.161克，讓盧布與[法國法郎的兌換率形成](../Page/法國法郎.md "wikilink")1盧布＝4法郎。1897年該兌換率進行修訂，改為1盧布＝2⅔法郎（0.774克黃金）。

隨著[第一次世界大戰的爆發](../Page/第一次世界大戰.md "wikilink")，盧布大幅貶值，1920年代初期更在俄羅斯境內形成惡性的[通貨膨脹](../Page/通貨膨脹.md "wikilink")。1922年[蘇聯成立以後](../Page/蘇聯.md "wikilink")，以[蘇聯盧布取代俄羅斯盧布](../Page/蘇聯盧布.md "wikilink")。

### 1991年－1997年底

1991年[蘇聯解體後](../Page/蘇聯解體.md "wikilink")，盧布仍是俄羅斯聯邦的法定貨幣。1993年俄羅斯銀行發行一組新的鈔票。在1990年代初的高通貨膨脹時期，盧布明顯貶值。

### 新盧布，1998年迄今

1998年1月1日重新發行新的盧布，其兌換率為1000舊盧布兌換1新盧布。新貨幣的面值設定並沒有完全解決當時俄羅斯所面臨的經濟基本問題。1998年俄羅斯金融危機再度造成貨幣貶值。1998年8月，在經過將近6個月的金融危機之後，盧布對美元貶值70％。

2010年11月23日，在[俄羅斯總理](../Page/俄羅斯總理.md "wikilink")[弗拉基米爾·普京和](../Page/弗拉基米爾·普京.md "wikilink")[中國](../Page/中國.md "wikilink")[國務院總理](../Page/國務院總理.md "wikilink")[溫家寶會面時](../Page/溫家寶.md "wikilink")，宣布俄羅斯和中國改用本國貨幣進行雙邊貿易，而不再使用美元。\[1\]\[2\]

## 紙幣

### 第七代盧布

新盧布系列紙幣於1998年1月1日發行，隨後於2001年、2004年、2010年及2014年均有版面上的局部更新。

<table>
<thead>
<tr class="header">
<th><p>1997年系列[3]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>圖像</p></td>
</tr>
<tr class="even">
<td><p>城市</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Banknote_5_rubles_(1997)_front.jpg" title="fig:Banknote_5_rubles_(1997)_front.jpg">Banknote_5_rubles_(1997)_front.jpg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:Banknote_5_rubles_(1997)_back.jpg" title="fig:Banknote_5_rubles_(1997)_back.jpg">Banknote_5_rubles_(1997)_back.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Banknote_10_rubles_2004_front.jpg" title="fig:Banknote_10_rubles_2004_front.jpg">Banknote_10_rubles_2004_front.jpg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:Banknote_10_rubles_2004_back.jpg" title="fig:Banknote_10_rubles_2004_back.jpg">Banknote_10_rubles_2004_back.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Banknote_50_rubles_2004_front.jpg" title="fig:Banknote_50_rubles_2004_front.jpg">Banknote_50_rubles_2004_front.jpg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:Banknote_50_rubles_2004_back.jpg" title="fig:Banknote_50_rubles_2004_back.jpg">Banknote_50_rubles_2004_back.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Russia100rubles04front.jpg" title="fig:Russia100rubles04front.jpg">Russia100rubles04front.jpg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:Russia100rubles04back.jpg" title="fig:Russia100rubles04back.jpg">Russia100rubles04back.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Banknote_500_rubles_2010_front.jpg" title="fig:Banknote_500_rubles_2010_front.jpg">Banknote_500_rubles_2010_front.jpg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:Banknote_500_rubles_2010_back.jpg" title="fig:Banknote_500_rubles_2010_back.jpg">Banknote_500_rubles_2010_back.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Banknote_1000_rubles_2010_front.jpg" title="fig:Banknote_1000_rubles_2010_front.jpg">Banknote_1000_rubles_2010_front.jpg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:Banknote_1000_rubles_2010_back.jpg" title="fig:Banknote_1000_rubles_2010_back.jpg">Banknote_1000_rubles_2010_back.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Banknote_5000_rubles_2010_front.jpg" title="fig:Banknote_5000_rubles_2010_front.jpg">Banknote_5000_rubles_2010_front.jpg</a><br />
<a href="https://zh.wikipedia.org/wiki/File:Banknote_5000_rubles_2010_back.jpg" title="fig:Banknote_5000_rubles_2010_back.jpg">Banknote_5000_rubles_2010_back.jpg</a></p></td>
</tr>
</tbody>
</table>

#### 紀念紙幣

[100_Olympic_rubles.jpg](https://zh.wikipedia.org/wiki/File:100_Olympic_rubles.jpg "fig:100_Olympic_rubles.jpg")在[索契舉行](../Page/索契.md "wikilink")\[4\]\]\]
2013年，為了紀念[冬季奧林匹克運動會在](../Page/冬季奧林匹克運動會.md "wikilink")[索契舉行](../Page/索契.md "wikilink")，發行一組特別的紙幣。

2015年，為了紀念收回[克里米亞一周年](../Page/克里米亞.md "wikilink")，發行一組特別的紙幣。

2018年，為紀念俄羅斯舉辦世界盃，發行一款特別的紙幣，是俄羅斯首款塑膠鈔票。\[5\]

#### 硬幣

俄羅斯硬幣有1, 5, 10, 50戈比 及 1, 2, 5, 10盧布8種面額，目前在俄羅斯境內1及5戈比硬幣較少流通。

#### 印刷

所有俄羅斯盧布紙幣目前都在位於莫斯科的國營工廠印製。該工廠設立於1919年6月6日，並持續運作至今。硬幣則在莫斯科與成立於1724年的鑄造。

## 匯率

## 參考文獻

## 外部連結

  - [俄罗斯硬币（目錄和畫廊）](http://www.ucoin.net/catalog/?country=russia)
  - [俄国的钞票](http://www.bis-ans-ende-der-welt.net/Russland-B-En.htm)

## 参见

  - [卢布](../Page/卢布.md "wikilink")

{{-}}

[Category:卢布](../Category/卢布.md "wikilink")
[Category:流通货币](../Category/流通货币.md "wikilink")
[Category:俄罗斯货币](../Category/俄罗斯货币.md "wikilink")

1.  [China, Russia quit
    dollar](http://www.chinadaily.com.cn/china/2010-11/24/content_11599087.htm)
    [China Daily](../Page/China_Daily.md "wikilink")
2.  [Chinese minister says China-Russia economic, trade co-op at new
    starting
    point](http://news.xinhuanet.com/english2010/indepth/2010-11/25/c_13622058_2.htm)
    [Xinhua News](../Page/Xinhua_News.md "wikilink")
3.
4.
5.  <https://wc2018.hk01.com/article/191467/%E4%B8%96%E7%95%8C%E7%9B%83-%E4%BF%84%E7%BE%85%E6%96%AF%E7%99%BC%E5%B8%83100%E7%9B%A7%E5%B8%83%E4%B8%96%E7%95%8C%E7%9B%83%E7%B4%80%E5%BF%B5%E9%88%94-2000%E8%90%AC%E5%BC%B5%E5%B0%87%E6%B5%81%E8%A1%8C%E5%85%A8%E5%9C%8B>
    【世界盃】俄羅斯發布100盧布世界盃紀念鈔　2000萬張將流行全國，2018年5月23日