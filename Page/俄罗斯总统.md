border |insigniasize = 125px |insigniacaption = 俄羅斯聯邦總統旗 |image =
Vladimir Putin - 2006.jpg |incumbent =
[弗拉基米尔·弗拉基米罗维奇·普京](../Page/弗拉基米尔·弗拉基米罗维奇·普京.md "wikilink")
|incumbentsince = 2018年5月7日 |residence =
[莫斯科](../Page/莫斯科.md "wikilink")[克里姆林宫](../Page/克里姆林宫.md "wikilink")[参议院](../Page/克里姆林宮元老院.md "wikilink")（辦公）
[奧金措夫斯基區](../Page/奧金措夫斯基區.md "wikilink")[Novo-Ogaryovo](../Page/:en:Novo-Ogaryovo.md "wikilink")（官邸）
|succession = [俄罗斯总理](../Page/俄罗斯总理.md "wikilink")（政府主席） |deputy =
[俄罗斯总理](../Page/俄罗斯总理.md "wikilink")（政府主席） |appointer =
公民[直接选举](../Page/直接选举.md "wikilink") |termlength = 6年，可连任一次
|formation = 1991年7月10日 |inaugural =
[鲍里斯·尼古拉耶维奇·葉利欽](../Page/鲍里斯·尼古拉耶维奇·葉利欽.md "wikilink")
|precursor =
[俄罗斯苏维埃联邦社会主义共和国最高苏维埃主席团主席](../Page/俄罗斯苏维埃联邦社会主义共和国最高苏维埃主席团.md "wikilink")
|salary = 360万[卢布](../Page/俄罗斯卢布.md "wikilink")/年 |website =  }}
[Russiaelectedmedved7rub2008.jpg](https://zh.wikipedia.org/wiki/File:Russiaelectedmedved7rub2008.jpg "fig:Russiaelectedmedved7rub2008.jpg")上的俄罗斯[联邦总统旗](../Page/俄罗斯总统.md "wikilink")\]\]

**俄罗斯联邦总统**（）是[俄罗斯联邦的](../Page/俄罗斯联邦.md "wikilink")[国家元首](../Page/国家元首.md "wikilink")。1993年12月12日通过的第一部《[联邦宪法](../Page/俄罗斯联邦宪法.md "wikilink")》，规定俄联邦是“[共和制的民主联邦法制国家](../Page/共和制.md "wikilink")”，确立總統和[联邦政府主席](../Page/俄羅斯總理.md "wikilink")（总理）權力二元結構的政府體制，近似[法兰西第五共和国的](../Page/法兰西第五共和国.md "wikilink")[雙首長制](../Page/雙首長制.md "wikilink")。第三任總統[德米特里·阿納托利耶維奇·梅德韋傑夫](../Page/德米特里·阿納托利耶維奇·梅德韋傑夫.md "wikilink")2008年12月30日簽署憲法修正案，延長總統任期與[國家杜馬](../Page/國家杜馬.md "wikilink")（下議院）任期，總統任期由四年延至六年，國家杜馬任期由四年延至五年。

## 選舉過程

### 資格

依據俄羅斯聯邦憲法，角逐俄羅斯總統的資格，必須為三十五歲以上的俄羅斯公民，並且居住在俄羅斯境內連續超過十年。俄羅斯憲法也同時明文規定，一個人擔任總統之職後再勝選可連任一次，然而擔任兩次之後隔屆再參選並不在此限，換言之並未限制一個人擔任總統的總年限，只要他能夠在選戰中獲勝即可。這是和[美國總統的不同之處](../Page/美國總統.md "wikilink")。

### 選舉

總統選舉的相關規定大都在《俄羅斯總統選舉辦法》（，[缩写](../Page/缩写.md "wikilink")：）、《俄羅斯選舉權保障》（，缩写：）\[1\]，總統選舉採取[兩輪投票制](../Page/兩輪投票制.md "wikilink")，每六年選舉一次，如果沒有候選人在第一回合取得絕對多數的勝利，那麼在第二回合以上一回合得票最多的兩位候選人。目前只曾在1996年俄羅斯總統選舉出現過兩輪投票。

最近一次的總統大選在[2018年](../Page/2018年俄羅斯總統選舉.md "wikilink")。

## 象徵

在總統從大選中獲勝之後，以下象徵將會授予總統，代表總統特殊的地位。

### 項鍊

第一個象徵總統的項鍊，此物的中央是個紅十字勳章，而紅十字徽章上的正是俄羅斯的國徽，"利益、榮譽、榮耀"以交叉的形式出現徽章上，一個金色的花環交叉連接其餘部分，在圈上有十七個徽章，其中九個徽章是俄羅斯國徽，其餘八個徽章是八個花環象徵上述俄羅斯的格言"利益、榮譽、榮耀"，而在普京就職大典上時這個項鍊被放在主席台的左側，根據俄羅斯總統的官方網站介紹，這個項鍊除了在特殊的典禮展示之外，只會存放在克林姆林宮。

### 旗幟

[Flag_of_Commander-in-chief_of_Russia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Commander-in-chief_of_Russia.svg "fig:Flag_of_Commander-in-chief_of_Russia.svg")
[Standard_of_the_President_of_the_Russian_Federation.svg](https://zh.wikipedia.org/wiki/File:Standard_of_the_President_of_the_Russian_Federation.svg "fig:Standard_of_the_President_of_the_Russian_Federation.svg")
這個旗幟成四角型以俄羅斯國旗為底中間是俄羅斯的國徽，而軍徽的雙頭鷹被換成金邊，這個旗幟常被用在克林姆林宮的辦公室以及其他國家的公家單位，而這個旗幟也會隨著總統的駕座飄揚於俄羅斯的境內，一種2:3大小比例的版本則被用在總統巡視海域的船艦上，通常這面旗幟象徵俄羅斯總統的職位。

### 憲法的特別副本

[Inauguration_of_Dmitry_Medvedev,_7_May_2008-7.jpg](https://zh.wikipedia.org/wiki/File:Inauguration_of_Dmitry_Medvedev,_7_May_2008-7.jpg "fig:Inauguration_of_Dmitry_Medvedev,_7_May_2008-7.jpg")

在俄羅斯，總統擁有憲法的特殊副本，而此副本被用在總統的就職大典上，該副本封面採厚板、內頁為紅底金字，上面的國徽則是銀色，這本特殊副本被存放於總統博物館。

### 法律地位

這些象徵以及程序是在公元1996年8月5日的總統令第1138號\[2\]得以確立，其中部分內容在2000年的5月6日的總統令第832號做出修定\[3\]，在新的總統令中憲法的特殊副本被移出俄羅斯總統的第三個象徵，其原因是象徵必須和法律有所區隔，而被移除的憲法副本只用於總統就職大典。

## 官邸

總統主要的工作場所是在莫斯科[克里姆林宮](../Page/克里姆林宮.md "wikilink")（第一樓）\[4\]，總統也可以使用大克里姆林宮（用來接見外賓），也就是通稱的第十四樓\[5\]（備用住所）。

2000年後總統的實際居所被遷到Novo-Ogaryovo（），計畫是在普丁任期結束前將保留高爾基九號（Gorki-9，，也是通稱的*Barvikha*
，），仿效之前保留[葉利欽退休依舊保留官邸的前例](../Page/葉利欽.md "wikilink")。

同時總統也有幾座假日別墅在莫斯科的近郊\[6\]。

## 总统列表

<table>
<thead>
<tr class="header">
<th><p>屆次</p></th>
<th><p>姓名</p></th>
<th><p>任期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/鲍里斯·尼古拉耶维奇·叶利钦.md" title="wikilink">鲍里斯·尼古拉耶维奇·叶利钦</a></p></td>
<td><p>1991年–1995年</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/鲍里斯·尼古拉耶维奇·叶利钦.md" title="wikilink">鲍里斯·尼古拉耶维奇·叶利钦</a><small>（連任）</small><br />
<a href="../Page/弗拉基米尔·弗拉基米罗维奇·普京.md" title="wikilink">弗拉基米尔·弗拉基米罗维奇·普京</a><small>（代理）</small></p></td>
<td><p>1995年–1999年<br />
1999年–2000年</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/弗拉基米尔·弗拉基米罗维奇·普京.md" title="wikilink">弗拉基米尔·弗拉基米罗维奇·普京</a></p></td>
<td><p>2000年–2004年</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/弗拉基米尔·弗拉基米罗维奇·普京.md" title="wikilink">弗拉基米尔·弗拉基米罗维奇·普京</a><small>（連任）</small></p></td>
<td><p>2004年–2008年</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/德米特里·阿纳托利耶维奇·梅德韦杰夫.md" title="wikilink">德米特里·阿纳托利耶维奇·梅德韦杰夫</a></p></td>
<td><p>2008年–2012年</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/弗拉基米尔·弗拉基米罗维奇·普京.md" title="wikilink">弗拉基米尔·弗拉基米罗维奇·普京</a><small>（第二次）</small></p></td>
<td><p>2012年–2018年</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="../Page/弗拉基米尔·弗拉基米罗维奇·普京.md" title="wikilink">弗拉基米尔·弗拉基米罗维奇·普京</a><small>（連任）</small></p></td>
<td><p>2018年至今</p></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部链接

  - [俄罗斯总统官方网站](http://eng.kremlin.ru/)

## 参见

  - [俄羅斯政治](../Page/俄羅斯政治.md "wikilink")
  - [俄罗斯总理](../Page/俄罗斯总理.md "wikilink")
  - [俄罗斯副总统](../Page/俄罗斯副总统.md "wikilink")
  - [蘇聯總統](../Page/蘇聯總統.md "wikilink")
  - [蘇聯共產黨中央委員會總書記](../Page/蘇聯共產黨中央委員會總書記.md "wikilink")
  - [俄罗斯苏维埃联邦社会主义共和国最高苏维埃主席团](../Page/俄罗斯苏维埃联邦社会主义共和国最高苏维埃主席团.md "wikilink")
  - [頭髮交替規則](../Page/頭髮交替規則.md "wikilink")
  - [核手提箱](../Page/核手提箱.md "wikilink")

{{-}}

[RUS](../Category/各国总统.md "wikilink")
[俄罗斯总统](../Category/俄罗斯总统.md "wikilink")
[Category:俄罗斯政治人物列表](../Category/俄罗斯政治人物列表.md "wikilink")
[Category:国家元首列表](../Category/国家元首列表.md "wikilink")
[Category:1991年俄羅斯建立](../Category/1991年俄羅斯建立.md "wikilink")

1.

2.  , № 33, ar. 3976

3.  , № 19, ar. 2068

4.  [The Presidential
    Residences](http://www.kremlin.ru/eng/articles/atributesEng09.shtml)


5.
6.  [Vladimir Putin
    Residences](http://www.kommersant.ru/doc.aspx?DocsID=740687),
    Kommersant, \#18(3594), 7 Feb. 2007