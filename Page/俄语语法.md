**[俄语语法](../Page/俄语.md "wikilink")**（Russian
grammar）中词与词的语法关系和词在句中的语法功能主要通过词形变化来表示。

## 词汇

俄语中的词分为10类：

  - 有词形变化：
      - [名词](../Page/名词.md "wikilink")
      - [形容词](../Page/形容词.md "wikilink")
      - [数词](../Page/数词.md "wikilink")
      - [代词](../Page/代词.md "wikilink")
      - [动词](../Page/动词.md "wikilink")
  - 无词形变化
      - [副词](../Page/副词.md "wikilink")
      - [前置词](../Page/前置词.md "wikilink")
      - [连接词](../Page/连接词.md "wikilink")
      - [语气词](../Page/语气词.md "wikilink")
      - [感叹词](../Page/感叹词.md "wikilink")

俄語詞性有单複數6格：[主格](../Page/主格.md "wikilink")（第1格）、[属格](../Page/属格.md "wikilink")（第2格）、[与格](../Page/与格.md "wikilink")（第3格）、[宾格](../Page/宾格.md "wikilink")（第4格）、[工具格](../Page/工具格.md "wikilink")（第5格）和[前置格](../Page/方位格.md "wikilink")（第6格），动词有"體"的分別，还有[形动词和](../Page/形动词.md "wikilink")[副动词之分](../Page/副动词.md "wikilink")。

## 句子

俄语句子成分可以分为主要成分和次要成分。

主要成分：

  - [主语](../Page/主语.md "wikilink")：表示行为或状态主体，通常为名词或人称代词。
  - [谓语](../Page/谓语.md "wikilink")：表示主语的行为或状态，用动词的变化形式表示。作谓语的动词需要和主语保持性、数的一致。

如果主语和谓语都是名词第一格，那么在主语和谓语之间要加[破折号](../Page/破折号.md "wikilink")“--”。

次要成分：

  - [定语](../Page/定语.md "wikilink")
  - [补语](../Page/补语.md "wikilink")
  - [状语](../Page/状语.md "wikilink")

### 次要成分

作为句子次要成分的定语表示事物的特征。用来说明名词的形容词、[物主代词和](../Page/物主代词.md "wikilink")[指示代词在句中经常作为一致定语](../Page/指示代词.md "wikilink")。

## 動詞

### 形容詞性分詞

俄語形容詞性分詞有主動和被動、完成體和未完成體之分。未完成體可有現在或過去時，而經典語言裏完成體只能有過去時。\[1\]作為形容詞，它們隨格、數、性而變化。若其由[自反動詞衍生](../Page/自反動詞.md "wikilink")，就會有後綴-ся附在最後，此時它不會縮成短形式-сь。分詞常常同動詞衍生的形容詞難於分別（這對[正寫法的某些情形很重要](../Page/俄語正寫法.md "wikilink")）。有的詞顯然是分詞，卻沒有對應的動詞：例如19世紀的司法名詞[вольноопределяющийся並非由不存在的動詞](../Page/wikt:вольноопределяющийся.md "wikilink")\*вольноопределяться衍生，而是副詞[вольно加上определяющийся](../Page/wikt:вольно.md "wikilink")（[определяться的分詞](../Page/wikt:определяться.md "wikilink")）。

#### 主動現在分詞

– **生活**在這城市的人很善良且有責任心。

要建構主動現在分詞，我們應將第三人稱複數現在時動詞末尾的"т"替換為"щ"，並加上形容詞尾：

|                                                            |
| ---------------------------------------------------------- |
| де́лать *（做）* – де́лаю**т** （他們做） – де́лаю**щий** （做的現在分詞） |

|                |                                                                                 |
| -------------- | ------------------------------------------------------------------------------- |
| Masculine form | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ий</span> |
| Feminine form  | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ая</span> |
| Neuter form    | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ее</span> |
| Plural form    | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ие</span> |


注意: 唯有**未完成體** 有主動現在分詞。

<table>
<caption>例子</caption>
<thead>
<tr class="header">
<th><p>不定式</p></th>
<th><p>第三人稱複數<br />
（現在時）</p></th>
<th><p>主動現在分詞</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第一變位</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>име́ть <em>（有）</em></p></td>
<td><p>име́<span style="color:#1976D2">ют</span></p></td>
<td><p>име́<span style="color:#1976D2">ющ</span>ий</p></td>
</tr>
<tr class="odd">
<td><p>писа́ть <em>（寫）</em></p></td>
<td><p>пи́ш<span style="color:#1976D2">ут</span></p></td>
<td><p>пи́ш<span style="color:#1976D2">ущ</span>ий</p></td>
</tr>
<tr class="even">
<td><p>пря́тать <em>（隱藏）</em></p></td>
<td><p>пря́ч<span style="color:#1976D2">ут</span></p></td>
<td><p>пря́ч<span style="color:#1976D2">ущ</span>ий</p></td>
</tr>
<tr class="odd">
<td><p>рисова́ть <em>（畫）</em></p></td>
<td><p>рису́<span style="color:#1976D2">ют</span></p></td>
<td><p>рису́<span style="color:#1976D2">ющ</span>ий</p></td>
</tr>
<tr class="even">
<td><p>вести́ <em>（引導）</em></p></td>
<td><p>вед<span style="color:#1976D2">у́т</span></p></td>
<td><p>вед<span style="color:#1976D2">у́щ</span>ий</p></td>
</tr>
<tr class="odd">
<td><p>печь <em>（烘焙）</em></p></td>
<td><p>пек<span style="color:#1976D2">у́т</span></p></td>
<td><p>пек<span style="color:#1976D2">у́щ</span>ий</p></td>
</tr>
<tr class="even">
<td><p>жить <em>（生活）</em></p></td>
<td><p>жив<span style="color:#1976D2">у́т</span></p></td>
<td><p>жив<span style="color:#1976D2">у́щ</span>ий</p></td>
</tr>
<tr class="odd">
<td><p>люби́ть <em>（愛）</em></p></td>
<td><p>лю́б<span style="color:#1976D2">ят</span></p></td>
<td><p>лю́б<span style="color:#1976D2">ящ</span>ий</p></td>
</tr>
<tr class="even">
<td><p>коло́ть <em>（刺）</em></p></td>
<td><p>ко́л<span style="color:#1976D2">ют</span></p></td>
<td><p>ко́л<span style="color:#1976D2">ющ</span>ий</p></td>
</tr>
<tr class="odd">
<td><p>идти́ <em>（走）</em></p></td>
<td><p>ид<span style="color:#1976D2">у́т</span></p></td>
<td><p>ид<span style="color:#1976D2">у́щ</span>ий</p></td>
</tr>
<tr class="even">
<td><p>пить <em>（喝）</em></p></td>
<td><p>пь<span style="color:#1976D2">ют</span></p></td>
<td><p>пь<span style="color:#1976D2">ю́щ</span>ий</p></td>
</tr>
<tr class="odd">
<td><p>мыть <em>（洗）</em></p></td>
<td><p>мо́<span style="color:#1976D2">ют</span></p></td>
<td><p>мо́<span style="color:#1976D2">ющ</span>ий</p></td>
</tr>
<tr class="even">
<td><p>брить <em>（剃）</em></p></td>
<td><p>бре́<span style="color:#1976D2">ют</span></p></td>
<td><p>бре́<span style="color:#1976D2">ющ</span>ий</p></td>
</tr>
<tr class="odd">
<td><p>петь <em>（唱）</em></p></td>
<td><p>по<span style="color:#1976D2">ю́т</span></p></td>
<td><p>по<span style="color:#1976D2">ю́щ</span>ий</p></td>
</tr>
<tr class="even">
<td><p>дава́ть <em>（給）</em></p></td>
<td><p>да<span style="color:#1976D2">ю́т</span></p></td>
<td><p>да<span style="color:#1976D2">ю́щ</span>ий</p></td>
</tr>
<tr class="odd">
<td><p>жать <em>（壓）</em></p></td>
<td><p>жм<span style="color:#1976D2">ут</span></p></td>
<td><p>жм<span style="color:#1976D2">ущ</span>ий</p></td>
</tr>
<tr class="even">
<td><p>тону́ть <em>（沉）</em></p></td>
<td><p>то́н<span style="color:#1976D2">ут</span></p></td>
<td><p>то́н<span style="color:#1976D2">ущ</span>ий</p></td>
</tr>
<tr class="odd">
<td><p>第二變位</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>слы́шать <em>（聽見）</em></p></td>
<td><p>слы́ш<span style="color:#1976D2">ат</span></p></td>
<td><p>слы́ш<span style="color:#1976D2">ащ</span>ий</p></td>
</tr>
<tr class="odd">
<td><p>сто́ить <em>（花費）</em></p></td>
<td><p>сто́<span style="color:#1976D2">ят</span></p></td>
<td><p>сто́<span style="color:#1976D2">ящ</span>ий</p></td>
</tr>
<tr class="even">
<td><p>стоя́ть <em>（站）</em></p></td>
<td><p>сто<span style="color:#1976D2">я́т</span></p></td>
<td><p>сто<span style="color:#1976D2">я́щ</span>ий</p></td>
</tr>
<tr class="odd">
<td><p>хоте́ть <em>（想要）</em></p></td>
<td><p>хот<span style="color:#1976D2">я́т</span></p></td>
<td><p>хот<span style="color:#1976D2">я́щ</span>ий</p></td>
</tr>
<tr class="even">
<td><p>其他動詞</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>бежа́ть <em>（跑）</em></p></td>
<td><p>бег<span style="color:#1976D2">у́т</span></p></td>
<td><p>бег<span style="color:#1976D2">у́щ</span>ий</p></td>
</tr>
<tr class="even">
<td><p>есть <em>（吃）</em></p></td>
<td><p>ед<span style="color:#1976D2">я́т</span></p></td>
<td><p>ед<span style="color:#1976D2">я́щ</span>ий</p></td>
</tr>
<tr class="odd">
<td><p>быть <em>（是）</em></p></td>
<td><p>*суть</p></td>
<td><p>*су́щий</p></td>
</tr>
</tbody>
</table>

（\*） 注意：現代俄語裏這些形式已廢棄不用，口語中並不用作動詞“是”的變形。

##### 主動現在分詞變格

|         |                                                                                  |                                                                                 |                                                                                  |                                                                           |
| ------- | -------------------------------------------------------------------------------- | :-----------------------------------------------------------------------------: | -------------------------------------------------------------------------------- | :-----------------------------------------------------------------------: |
|         |                                                                                  |                                     **單數**                                      |                                                                                  |                                  **複數**                                   |
| **陽性**  | **陰性**                                                                           |                                     **中性**                                      |                                                                                  |                                                                           |
| **主格**  | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ий</span>  | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ая</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ее</span>  | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ие  |
| **屬格**  | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">его</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ей</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">его</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">их  |
| **與格**  | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ему</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ей</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ему</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">им  |
| **賓格**  | 主格或屬格                                                                            | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ую</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ее</span>  |                                   主格或屬格                                   |
| **工具格** | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">им</span>  | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ей</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">им</span>  | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ими |
| **前置格** | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ем</span>  | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ей</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ем</span>  | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">их  |

де́ла<span style="color:#388E3C">ющ</span>ий - 做

##### 自反動詞範例

|         |                                                                                                                       |                                                                                                                      |                                                                                                                       |                                                                                                                |
| ------- | --------------------------------------------------------------------------------------------------------------------- | :------------------------------------------------------------------------------------------------------------------: | --------------------------------------------------------------------------------------------------------------------- | :------------------------------------------------------------------------------------------------------------: |
|         |                                                                                                                       |                                                        **單數**                                                        |                                                                                                                       |                                                     **複數**                                                     |
| **陽性**  | **陰性**                                                                                                                |                                                        **中性**                                                        |                                                                                                                       |                                                                                                                |
| **主格**  | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ий</span><span style="color:#F44336">ся</span>  | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ая</span><span style="color:#F44336">ся</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ее</span><span style="color:#F44336">ся</span>  | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ие<span style="color:#F44336">ся</span>  |
| **屬格**  | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">его</span><span style="color:#F44336">ся</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ей</span><span style="color:#F44336">ся</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">его</span><span style="color:#F44336">ся</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">их<span style="color:#F44336">ся</span>  |
| **與格**  | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ему</span><span style="color:#F44336">ся</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ей</span><span style="color:#F44336">ся</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ему</span><span style="color:#F44336">ся</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">им<span style="color:#F44336">ся</span>  |
| **賓格**  | 主格或屬格                                                                                                                 | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ую</span><span style="color:#F44336">ся</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ее</span><span style="color:#F44336">ся</span>  |                                                     主格或屬格                                                      |
| **工具格** | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">им</span><span style="color:#F44336">ся</span>  | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ей</span><span style="color:#F44336">ся</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">им</span><span style="color:#F44336">ся</span>  | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ими<span style="color:#F44336">ся</span> |
| **前置格** | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ем</span><span style="color:#F44336">ся</span>  | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ей</span><span style="color:#F44336">ся</span> | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">ем</span><span style="color:#F44336">ся</span>  | де́ла<span style="color:#388E3C">ющ</span><span style="color:#1976D2">их<span style="color:#F44336">ся</span>  |

де́ла<span style="color:#388E3C">ющ</span>ий<span style="color:#F44336">ся</span>
- **被**做

分詞的性、格、數和它修飾的名詞一致：
**<span style="color:#1976D2" lang="ru">ям</span>,
<span style="color:#1976D2" lang="ru">им</span>**  – 我將這首歌獻給住在這座城市的人。

## 参看

  - [俄语正寫法](../Page/俄语正寫法.md "wikilink")

  - [俄语正音法](../Page/俄语正音法.md "wikilink")

  -
  -
## 引用

## 外部連結

  - [Interactive On-line Reference Grammar of
    Russian](http://www.alphadictionary.com/rusgrammar/)
  - [Wikibooks Russian](http://wikibooks.org/wiki/Russian)
  - [Gramota.ru - dictionaries](http://gramota.ru)
  - [Wiktionary has word entries in Cyrillic with meanings and
    grammatical analysis in
    English](http://en.wiktionary.org/wiki/Category:Russian_language)
  - [Russian Wiktionary gives word meanings and grammatical analysis in
    Russian](http://ru.wiktionary.org)
  - [Russian grammar overview with practice
    tests](http://www.practicerussian.com/Grammar/Grammar.aspx)
  - [Over 400 links to Russian Grammar articles around the
    Net](https://web.archive.org/web/20081108070051/http://russianresources.info/links.aspx/grammar)
  - [Free online Russian grammar
    book](http://wikitranslate.org/wiki/Russian_grammar_book) （with
    videos）

[Category:俄語語法](../Category/俄語語法.md "wikilink")

1.  [Classification of
    participles](http://russianlearn.com/grammar/category/classification)