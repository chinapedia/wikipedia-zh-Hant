**保利置業集團有限公司**（簡稱**保利置業**，），前身為「保利（香港）投資有限公司」，是一家[中國的](../Page/中國.md "wikilink")[房地產開發公司](../Page/房地產.md "wikilink")，於1973年當時以「新海康航業投資有限公司」在[香港成立](../Page/香港.md "wikilink")，當時是全球航運集團之一，但同時面對80年代航運危機時期問題，以致生意大減。

1993年，被[中國保利集團公司收購本公司](../Page/中國保利集團公司.md "wikilink")，由原來的航運公司轉營為綜合企業。2005年，股東特別大會中通過公司更名為「保利（香港）投資有限公司」。同年，保利（香港）向保利集團收購上海保利置業，發展中國房地產市場。\[1\]

2009年9月，[中國投資公司以](../Page/中國投資有限責任公司.md "wikilink")4.09億元入股保利（香港），保利（香港）同時向母公司購入位於[蘇州](../Page/蘇州.md "wikilink")、[海南](../Page/海南.md "wikilink")、[上海](../Page/上海.md "wikilink")、[深圳及](../Page/深圳.md "wikilink")[廣州的地產發生項目](../Page/廣州.md "wikilink")。\[2\]

現任董事會主席為[中國保利集團之副總經理](../Page/中國保利集團.md "wikilink")[雪明](../Page/雪明.md "wikilink")。

2014年2月，保利置業以39.23億港元投得香港[啟德發展區一幅地皮](../Page/啟德發展區.md "wikilink")。\[3\]
該項目已命名為[龍譽](../Page/龍譽.md "wikilink")，預計於2018年12月落成。

2015年9月16日，保利置業以17.3億港元投得屯門市地段第542號地皮。\[4\]

2018年8月15日，保利置業以33億港元,每呎樓面地價9256元,投得油塘高超道住宅地皮

## 參考

## 外部連結

  - [polyhongkong.com.hk](https://web.archive.org/web/20080705223026/http://www.polyhongkong.com.hk/)

[Category:香港上市地產公司](../Category/香港上市地產公司.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")
[Category:中國房地產開發公司](../Category/中國房地產開發公司.md "wikilink")
[Category:总部位于北京市的中华人民共和国国有企业](../Category/总部位于北京市的中华人民共和国国有企业.md "wikilink")
[Category:1973年成立的公司](../Category/1973年成立的公司.md "wikilink")
[Category:前恒生香港中資企業指數成份股](../Category/前恒生香港中資企業指數成份股.md "wikilink")
[Category:保利集團](../Category/保利集團.md "wikilink")

1.  [大股東注入土地資源
    香江和保利主業聚焦地產，2009年5月9日](http://www.linkshop.com.cn/web/archives/2008/90931.shtml)
2.  [蘋果日報 -
    中投4億入股保利，2009年9月18日](http://hk.apple.nextmedia.com/financeestate/art/20090918/13221770)
3.  [保利搶貴啟德地兩成 中資再下一城
    未來賣樓呎價至少$15,000](http://hk.apple.nextmedia.com/financeestate/art/20140226/18637854)
    蘋果日報 ，2014年2月26日
4.  <https://www.etnet.com.hk/www/tc/news/categorized_news_detail.php?newsid=ETN250916921&page=1&category=latest>