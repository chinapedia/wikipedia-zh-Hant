**保和**（824年-839年）是[南诏](../Page/南诏.md "wikilink")[勸豐祐的](../Page/勸豐祐.md "wikilink")[年号](../Page/年号.md "wikilink")，共计16年\[1\]。

## 纪年

| 保和                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             | 八年                             | 九年                             | 十年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 824年                           | 825年                           | 826年                           | 827年                           | 828年                           | 829年                           | 830年                           | 831年                           | 832年                           | 833年                           |
| [干支](../Page/干支纪年.md "wikilink") | [甲辰](../Page/甲辰.md "wikilink") | [乙巳](../Page/乙巳.md "wikilink") | [丙午](../Page/丙午.md "wikilink") | [丁未](../Page/丁未.md "wikilink") | [戊申](../Page/戊申.md "wikilink") | [己酉](../Page/己酉.md "wikilink") | [庚戌](../Page/庚戌.md "wikilink") | [辛亥](../Page/辛亥.md "wikilink") | [壬子](../Page/壬子.md "wikilink") | [癸丑](../Page/癸丑.md "wikilink") |
| 保和                               | 十一年                            | 十二年                            | 十三年                            | 十四年                            | 十五年                            | 十六年                            |                                |                                |                                |                                |
| [公元](../Page/公元纪年.md "wikilink") | 834年                           | 835年                           | 836年                           | 837年                           | 838年                           | 839年                           |                                |                                |                                |                                |
| [干支](../Page/干支纪年.md "wikilink") | [甲寅](../Page/甲寅.md "wikilink") | [乙卯](../Page/乙卯.md "wikilink") | [丙辰](../Page/丙辰.md "wikilink") | [丁巳](../Page/丁巳.md "wikilink") | [戊午](../Page/戊午.md "wikilink") | [己未](../Page/己未.md "wikilink") |                                |                                |                                |                                |

## 参见

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [長慶](../Page/長慶.md "wikilink")（821年正月—824年十二月）：[唐穆宗的年號](../Page/唐穆宗.md "wikilink")
      - [大和](../Page/大和_\(唐文宗\).md "wikilink")（827年二月—835年十二月）：[唐文宗的年號](../Page/唐文宗.md "wikilink")
      - [開成](../Page/開成.md "wikilink")（836年正月—840年十二月）：唐文宗的年號
      - [弘仁](../Page/弘仁.md "wikilink")（810年九月十九日至824年正月五日）：平安時代[嵯峨天皇之年號](../Page/嵯峨天皇.md "wikilink")
      - [天長](../Page/天长_\(淳和天皇\).md "wikilink")（824年正月五日至834年正月三日）：[平安時代](../Page/平安時代.md "wikilink")[淳和天皇之年號](../Page/淳和天皇.md "wikilink")
      - [承和](../Page/承和_\(仁明天皇\).md "wikilink")（834年正月三日至848年六月十三日）：平安時代[仁明天皇之年號](../Page/仁明天皇.md "wikilink")
      - [建興](../Page/建興_\(大仁秀\).md "wikilink")（819年至830年）：渤海宣王[大仁秀之年號](../Page/大仁秀.md "wikilink")
      - [咸和](../Page/咸和_\(大彝震\).md "wikilink")（831年至857年）：渤海宣王[大彝震之年號](../Page/大彝震.md "wikilink")
      - [彝泰](../Page/彝泰.md "wikilink")（815年至838年）：[吐蕃可黎可足](../Page/吐蕃.md "wikilink")[贊普](../Page/贊普.md "wikilink")[赤祖德贊之年號](../Page/赤祖德贊.md "wikilink")

## 注释

[Category:南诏年号](../Category/南诏年号.md "wikilink")
[Category:9世纪中国年号](../Category/9世纪中国年号.md "wikilink")
[Category:820年代中国政治](../Category/820年代中国政治.md "wikilink")
[Category:830年代中国政治](../Category/830年代中国政治.md "wikilink")

1.  张增祺. 关于南诏、大理国纪年资料的订正\[J\]. 考古, 1983(1):68-69.