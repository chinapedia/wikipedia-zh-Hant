**保和省**（或稱**薄荷省**）（**Bohol**）是[菲律賓的一個島嶼](../Page/菲律賓.md "wikilink")[省份](../Page/菲律賓省份.md "wikilink")，位於[中米沙鄢](../Page/中米沙鄢.md "wikilink")[政區](../Page/菲律賓政區.md "wikilink")，首府為[塔比拉蘭市](../Page/塔比拉蘭市.md "wikilink")（Tagbilaran
City）。薄荷省所在的[薄荷島是菲律賓第](../Page/薄荷島.md "wikilink")10大島，位於[米沙鄢群島的中心位置](../Page/米沙鄢群島.md "wikilink")。岛上有著名的[巧克力山](../Page/巧克力山.md "wikilink")。岛民喜欢称本岛为薄荷共和国。这里以前短暂独立。

## 參考文獻

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 延伸閱讀

  -
  -
## 外部連接

  - [Official website of the provincial government of
    Bohol](http://www.bohol.gov.ph)
  - [Provincial Planning and Development Office of
    Bohol](https://web.archive.org/web/20170824191139/http://www.ppdobohol.lgu.ph/)
    (includes provincial atlas)

[B](../Category/菲律賓省份.md "wikilink")