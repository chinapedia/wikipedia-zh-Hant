[Bao_Sheng_Temple.jpg](https://zh.wikipedia.org/wiki/File:Bao_Sheng_Temple.jpg "fig:Bao_Sheng_Temple.jpg")
[Arhat_sculpture_in_Bao_Sheng_Temple,_Luzhi,_Suzhou.jpg](https://zh.wikipedia.org/wiki/File:Arhat_sculpture_in_Bao_Sheng_Temple,_Luzhi,_Suzhou.jpg "fig:Arhat_sculpture_in_Bao_Sheng_Temple,_Luzhi,_Suzhou.jpg")
**保圣寺**位于[江苏省](../Page/江苏省.md "wikilink")[蘇州市](../Page/蘇州市.md "wikilink")[吴中区](../Page/吴中区.md "wikilink")，一个有2500年历史的水乡小鎮[甪直](../Page/甪直.md "wikilink")（拼音：Lù
Zhí）。保圣寺原名保圣教寺，建于[梁武帝](../Page/梁武帝.md "wikilink")[天监二年](../Page/天监.md "wikilink")（503年）\[1\]。唐[会昌五年](../Page/会昌_\(唐朝\).md "wikilink")（845年）因[唐武宗灭佛而被毁](../Page/唐武宗灭佛.md "wikilink")。北宋大中[祥符六年](../Page/祥符.md "wikilink")（1013年）重建。[熙宁六年](../Page/熙宁.md "wikilink")（1073年）僧维吉在西庑建白莲讲寺，[宋寧宗](../Page/宋寧宗.md "wikilink")[嘉定十七年](../Page/嘉定_\(宋寧宗\).md "wikilink")（1224年）改为[陆龟蒙祠](../Page/陆龟蒙.md "wikilink")\[2\]。保圣寺最盛时据说有殿堂五千，僧众千人，和杭州的灵隐寺齐名。元末重新衰落，明成化二十三年（公元1487年）重新兴盛。清代名为保圣禅寺。1950年改为保圣寺[博物馆](../Page/博物馆.md "wikilink")。

保圣寺博物馆的著名古迹包括：

  - [中国全国重点文物保护单位](../Page/中国全国重点文物保护单位.md "wikilink")：九尊[保圣寺罗汉塑像](../Page/保圣寺罗汉塑像.md "wikilink")
  - 陆龟蒙墓，陆龟蒙鬥鸭池。
  - [宋代](../Page/宋代.md "wikilink")[斗拱](../Page/斗拱.md "wikilink")\[3\]。
  - 保圣寺大殿保存着宋代石柱础多件，包括铺地莲花柱础，化生柱础\[4\]，天王殿写生华柱础\[5\]、牡丹华柱础\[6\]、宝装莲花柱础，建筑学家[梁思成评曰](../Page/梁思成.md "wikilink")“尤为精美”\[7\]。
  - 宋代幡杆颊，即两片夹旗杆的长石柱，每片高一丈五宽二尺厚一尺二\[8\]。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:苏州博物馆](../Category/苏州博物馆.md "wikilink")
[Category:苏州佛寺](../Category/苏州佛寺.md "wikilink")
[Category:文物博物馆](../Category/文物博物馆.md "wikilink")
[Category:吴中区](../Category/吴中区.md "wikilink")
[Category:6世紀完工建築物](../Category/6世紀完工建築物.md "wikilink")
[Category:11世紀完工建築物](../Category/11世紀完工建築物.md "wikilink")

1.  （清）顾震涛著 《吴门表隐》卷八
2.  （清）徐崧　张大纯撰　《百城烟水·苏州》
3.  梁思成著　《清式营造则例·第三章第一节　斗拱》
4.  \>《梁思成全集》第七卷　56页
5.  \>《梁思成全集》第七卷　57页
6.  \>《梁思成全集》第七卷　52页
7.  [梁思成著](../Page/梁思成.md "wikilink")　《建筑设计参考图集·第七集柱础简说》
8.  《梁思成全集》第七卷　74页