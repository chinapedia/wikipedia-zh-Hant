**保大**（[943年三月](../Page/943年.md "wikilink")-[957年](../Page/957年.md "wikilink")）是南唐元宗[李璟的](../Page/李璟.md "wikilink")[年號](../Page/年號.md "wikilink")，共计15年。楚恭孝王[馬希萼在保大八年至九年用此年号](../Page/馬希萼.md "wikilink")（[950年十二月](../Page/950年.md "wikilink")-[951年十一月](../Page/951年.md "wikilink")）\[1\]。

## 紀年

| 保大                               | 元年                                 | 二年                                 | 三年                                 | 四年                                 | 五年                                 | 六年                                 | 七年                                 | 八年                                 | 九年                                 | 十年                                 |
| -------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- |
| [公元](../Page/公元纪年.md "wikilink") | [944年](../Page/944年.md "wikilink") | [945年](../Page/945年.md "wikilink") | [946年](../Page/946年.md "wikilink") | [947年](../Page/947年.md "wikilink") | [948年](../Page/948年.md "wikilink") | [949年](../Page/949年.md "wikilink") | [950年](../Page/950年.md "wikilink") | [951年](../Page/951年.md "wikilink") | [952年](../Page/952年.md "wikilink") | [953年](../Page/953年.md "wikilink") |
| [干支](../Page/干支纪年.md "wikilink") | [癸卯](../Page/癸卯.md "wikilink")     | [甲辰](../Page/甲辰.md "wikilink")     | [乙巳](../Page/乙巳.md "wikilink")     | [丙午](../Page/丙午.md "wikilink")     | [丁未](../Page/丁未.md "wikilink")     | [戊申](../Page/戊申.md "wikilink")     | [己酉](../Page/己酉.md "wikilink")     | [庚戌](../Page/庚戌.md "wikilink")     | [辛亥](../Page/辛亥.md "wikilink")     | [壬子](../Page/壬子.md "wikilink")     |
| 保大                               | 十一年                                | 十二年                                | 十三年                                | 十四年                                | 十五年                                |                                    |                                    |                                    |                                    |                                    |
| [公元](../Page/公元纪年.md "wikilink") | [954年](../Page/954年.md "wikilink") | [955年](../Page/955年.md "wikilink") | [956年](../Page/956年.md "wikilink") | [957年](../Page/957年.md "wikilink") | [958年](../Page/958年.md "wikilink") |                                    |                                    |                                    |                                    |                                    |
| [干支](../Page/干支纪年.md "wikilink") | [癸丑](../Page/癸丑.md "wikilink")     | [甲寅](../Page/甲寅.md "wikilink")     | [乙卯](../Page/乙卯.md "wikilink")     | [丙辰](../Page/丙辰.md "wikilink")     | [丁巳](../Page/丁巳.md "wikilink")     |                                    |                                    |                                    |                                    |                                    |

## 參看

  - [中國年號索引](../Page/中國年號索引.md "wikilink")
  - 其他政权使用的[保大年号](../Page/保大.md "wikilink")
  - 同期存在的其他政权年号
      - [天福](../Page/天福_\(石敬瑭\).md "wikilink")（936年十一月至944年六月）：[後晉](../Page/後晉.md "wikilink")—[石敬瑭之年號](../Page/石敬瑭.md "wikilink")
      - [開運](../Page/開運_\(石重貴\).md "wikilink")（944年七月至946年）：後晉—[石重貴之年號](../Page/石重貴.md "wikilink")
      - [天福](../Page/天福_\(石敬瑭\).md "wikilink")（947年）：[後漢](../Page/後漢.md "wikilink")—[劉知遠之年號](../Page/劉知遠.md "wikilink")
      - [乾祐](../Page/乾祐_\(劉知遠\).md "wikilink")（948年至956年）：後漢—劉知遠、[劉承祐](../Page/劉承祐.md "wikilink")，[北漢](../Page/北漢.md "wikilink")—[劉旻](../Page/劉旻.md "wikilink")、[劉鈞之年號](../Page/劉鈞.md "wikilink")
      - [天會](../Page/天會_\(劉鈞\).md "wikilink")（957年至973年）：北漢—劉鈞、劉繼恩、劉繼元之年號
      - [天德](../Page/天德_\(王延政\).md "wikilink")（943年二月至945年八月）：[殷](../Page/闽_\(十国\).md "wikilink")—[王延政之年號](../Page/王延政.md "wikilink")
      - [應乾](../Page/應乾.md "wikilink")（943年三月至十月）：南漢—[劉晟之年號](../Page/劉晟.md "wikilink")
      - [乾和](../Page/乾和.md "wikilink")（943年十一月至958年七月）：南漢—劉晟之年號
      - [永樂](../Page/永樂_\(張遇賢\).md "wikilink")（942年七月至943年十月）：南漢—[張遇賢之年號](../Page/張遇賢.md "wikilink")
      - [廣順](../Page/廣順.md "wikilink")（951年至953年）：[後周](../Page/後周.md "wikilink")—[郭威之年號](../Page/郭威.md "wikilink")
      - [顯德](../Page/顯德.md "wikilink")（954年至960年正月）：後周—郭威、柴榮、柴宗訓之年號
      - [廣政](../Page/廣政_\(孟昶\).md "wikilink")（938年正月至965年正月）：[後蜀](../Page/後蜀.md "wikilink")—[孟昶之年號](../Page/孟昶.md "wikilink")
      - [至治](../Page/至治_\(段思良\).md "wikilink")（946年至951年）：大理國—[段思良之年號](../Page/段思良.md "wikilink")
      - [會同](../Page/会同_\(辽朝\).md "wikilink")（938年十一月至947年正月）：契丹—耶律德光之年號
      - [大同](../Page/大同_\(辽朝\).md "wikilink")（947年二月至九月）：遼—耶律德光之年號
      - [天祿](../Page/天祿_\(遼世宗\).md "wikilink")（947年九月至951年九月）：遼—[遼世宗耶律阮之年號](../Page/遼世宗.md "wikilink")
      - [應曆](../Page/應曆.md "wikilink")（951年九月至969年二月）：遼—[遼穆宗耶律璟之年號](../Page/遼穆宗.md "wikilink")
      - [同慶](../Page/同慶_\(尉遲烏僧波\).md "wikilink")（912年至966年）：[于闐](../Page/于闐.md "wikilink")—[尉遲烏僧波之年號](../Page/尉遲烏僧波.md "wikilink")
      - [天慶](../Page/天慶_\(朱雀天皇\).md "wikilink")（938年五月二十二日至947年四月二十二日）：朱雀天皇與[村上天皇之年號](../Page/村上天皇.md "wikilink")
      - [天曆](../Page/天曆_\(村上天皇\).md "wikilink")（947年四月二十二日至957年十月二十七日）：日本村上天皇年號
      - [天德](../Page/天德_\(村上天皇\).md "wikilink")（957年十月二十七日至961年二月十六日）：日本村上天皇年號

## 参考文献

<div class="references-small">

<references />

</div>

  - 南唐年号

<!-- end list -->

  - 马楚年号

[Category:南唐年号](../Category/南唐年号.md "wikilink")
[Category:马楚年号](../Category/马楚年号.md "wikilink")
[Category:940年代中国政治](../Category/940年代中国政治.md "wikilink")
[Category:950年代中国政治](../Category/950年代中国政治.md "wikilink")

1.  李崇智，中国历代年号考，中华书局，2004年12月, 150 ISBN 7101025129