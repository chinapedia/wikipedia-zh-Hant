**保护基**（protecting group、protective
group）是[有机合成上的一个概念](../Page/有机合成.md "wikilink")。是使有机分子中特定[官能团发生希望的反应](../Page/官能团.md "wikilink")，同时抑制其他官能团的反应而通过特定试剂将一部分官能团可逆地转化成惰性基团的策略。有机合成中常需要保护的官能团有[羟基](../Page/羟基.md "wikilink")、[氨基](../Page/氨基.md "wikilink")、[羰基等等](../Page/羰基.md "wikilink")。保护基的使用提高了有机合成的可操作性和准确性，使人们能够合成更加复杂的分子，但是上保护基、去保护基同时也降低了反应的总产率。避免使用保护基一直是合成家的梦想，但是“在可预见的未来，如同死亡和税收一样，保护基的使用是有机合成中不可避免的东西。”

## 常见的保护基

### 保护醇羟基

  - [乙酰基](../Page/乙酰基.md "wikilink")（Ac）
  - 2-甲氧基乙基甲基醚（MEM）
  - [甲氧甲基醚](../Page/甲氧甲基醚.md "wikilink")（MOM）
  - [对甲氧基苄基](../Page/对甲氧基苄基.md "wikilink")（PMB）
  - [甲硫甲基醚](../Page/甲硫甲基醚.md "wikilink")（MTM）
  - [特戊酰基](../Page/特戊酰基.md "wikilink")（Piv）
  - [四氢吡喃](../Page/四氢吡喃.md "wikilink")（THP）
  - [硅醚保护基](../Page/硅醚.md "wikilink")：如[三甲基硅基](../Page/三甲基硅基.md "wikilink")（TMS）、[叔丁基二甲基硅基](../Page/叔丁基二甲基硅基.md "wikilink")（TBDMS）、[三异丙基硅基](../Page/三异丙基硅基.md "wikilink")（TIPS）
  - [甲基醚](../Page/甲氧基.md "wikilink")

### 保护氨基

[Tert-Butoxycarbonyl_protected_Glycine_Structural_Formulae_V.1.png](https://zh.wikipedia.org/wiki/File:Tert-Butoxycarbonyl_protected_Glycine_Structural_Formulae_V.1.png "fig:Tert-Butoxycarbonyl_protected_Glycine_Structural_Formulae_V.1.png")

  - [苄氧羰基](../Page/苄氧羰基.md "wikilink")（Cbz）
  - [叔丁氧羰基](../Page/叔丁氧羰基.md "wikilink")（BOC）
  - [9-芴甲氧羰基](../Page/9-芴甲氧羰基.md "wikilink")（FMOC）
  - [苄基](../Page/苄基.md "wikilink")（Bn）
  - [对甲氧苯基](../Page/对甲氧苯基.md "wikilink")（PMP）

1、 叔丁氧羰基(tert-butoxycarbonyl) 缩写t-Boc 在以下条件稳定：H2/Pd或碱 除去条件：
HBr+CH3COOH或CF3COOH

2、 苄氧羰基(carbobenzoxy) 缩写CBz 在以下条件稳定：CF3COOH或碱 除去条件：HBr+CH3COOH或H2/Pd

3 、2-联苯基-2-丙氧羰基(2-biphenyl-2-propoxycarbonyl) 缩写BPoc 在以下条件稳定：碱
除去条件：CF3COOH或HCOOH，CF3COOH或HBr

4 、邻苯二甲酰亚胺基(phthaloyl) 在以下条件稳定：Na-NH3,H2/Ph,HCl或HBr+CH3COOH
除去条件：NH2NH2-H2O

5 、对甲苯磺酰基(p-toluenesulfonyl) 缩写Tosyl 在以下条件稳定：HBr+CH3COOH或碱 除去条件：Na-NH3

6 、三苯甲基(triphenylmethyl) 缩写Trityl 在以下条件稳定：碱 除去条件：H2/Pd或CF3COOH（80%）

7 、甲酰基(formyl) 在以下条件稳定：H2/Pd或Na-NH3 除去条件：NH2NH2+醇+碱

8 、三氟乙酰基(trifluoroacetyl) 在以下条件稳定： 除去条件：温和碱性条件

### 保护羰基

  - [缩醛与](../Page/缩醛.md "wikilink")[缩酮](../Page/缩酮.md "wikilink")
  - [偕二酸酯](../Page/偕二酸酯.md "wikilink")
  - [二噻烷](../Page/二噻烷.md "wikilink")

### 保护羧基

  - [甲基](../Page/甲基.md "wikilink")[酯](../Page/酯.md "wikilink")
  - [苄基酯](../Page/苄基.md "wikilink")
  - [叔丁基酯](../Page/叔丁基.md "wikilink")
  - [硅基酯](../Page/硅基.md "wikilink")

## 外部連結

  - For introduction of protecting group and mechanism of deprotection
    See : <http://www.biocis.u-psud.fr/spip.php?article332>
  - [Senior undergraduate study notes on this subject, from Prof.
    Rizzo.](http://www.vanderbilt.edu/AnS/Chemistry/Rizzo/chem223/protect.pdf)
  - [A further set of study notes in tutorial form, with guidance and
    comments, from Profs. Grossman and
    Cammers.](http://www.chem.uky.edu/courses/che535/AC/2007.1S/04-2007-S-CHE535.pdf)
  - [A review by Prof.
    Kocienski.](http://www.rsc.org/ej/P1/1998/A803688H.pdf)
  - [A user site excerpting the classic Greene and Wuts text regarding
    stability of a few key groups, from this reference's extensive
    tables.](https://www.organic-chemistry.org/protectivegroups)
  - [protecting
    group](http://www.organic-reaction.com/organic-synthesis/protecting-groups/)
    from organic-reaction.com

[category:有机化学](../Page/category:有机化学.md "wikilink")

[\*](../Category/保护基.md "wikilink")