[Paul_Samuel_Johnson.jpg](https://zh.wikipedia.org/wiki/File:Paul_Samuel_Johnson.jpg "fig:Paul_Samuel_Johnson.jpg")
 **保罗·塞缪尔·利奥·约翰逊**（Paul Samuel Leo（曾用 Levitsky
）Johnson，）是一位美国学者和牧师，他创立了[Layman's
Home Missionary
Movement](../Page/Layman's_Home_Missionary_Movement.md "wikilink")。

约翰逊1873年10月出生于宾夕法尼亚州的Titusville，父母是从波兰逃亡到美国不久的犹太人。他的父亲是一个卓越的希伯来语学者，最后成为当地犹太教堂的会长。在他12岁时，母亲去世，父亲再婚，这两件事对他打击很大，曾经多次离家出走。

最终他归信[基督教](../Page/基督教.md "wikilink")，加入一个[卫理公会教会](../Page/卫理公会.md "wikilink")。

1890年，他进入俄亥俄州首府哥伦布市州立大学[哥伦布](../Page/哥伦布.md "wikilink")，并于1895年以优异成绩毕业；之后他进入俄亥俄[路德教会](../Page/路德教会.md "wikilink")[会合的神学院](../Page/会合.md "wikilink")，并于1898年毕业。

1903年5月，由于信仰的改变，他离开路德教会。与[查尔斯·泰兹·罗素保持着友谊](../Page/查尔斯·泰兹·罗素.md "wikilink")，担任他的个人秘书。在这段时间他成为罗素最信赖的朋友和顾问。1910年他患上[神经衰弱](../Page/神经衰弱.md "wikilink")。

罗素去世后，[约瑟夫·富兰克林·卢瑟福成为主席](../Page/约瑟夫·富兰克林·卢瑟福.md "wikilink")，约翰逊离开了[守望台书社](../Page/守望台书社.md "wikilink")。然后，他在1919年创立了[Layman's
Home Missionary
Movement](../Page/Layman's_Home_Missionary_Movement.md "wikilink")，从1920年开始服务于改组织董事会直到1950年去世。

约翰逊宣称罗素牧师是“[基督再临信使](../Page/基督再临.md "wikilink")”，他自己是“[主显节信使](../Page/主显节.md "wikilink")”。

## 外部链接

  - [传记](https://web.archive.org/web/20070303214229/http://www.epiphanystudies.co.uk/johnson.htm)
  - [不同圣经研究小组历史](https://web.archive.org/web/20110716051854/http://www.heraldmag.org/2004_history/04history_7.htm)，有关于约翰逊神父的不多记录（参看[1](https://web.archive.org/web/20060504011108/http://www.heraldmag.org/2004_history/04history_6.htm)）。

[Category:犹太裔美国作家](../Category/犹太裔美国作家.md "wikilink")
[Category:美國牧師](../Category/美國牧師.md "wikilink")
[Category:耶和华见证人](../Category/耶和华见证人.md "wikilink")