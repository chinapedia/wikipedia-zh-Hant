**保罗·约瑟夫·维兹**（，\[1\]）曾是一位[美国国家航空航天局的](../Page/美国国家航空航天局.md "wikilink")[宇航员](../Page/宇航员.md "wikilink")，执行过[天空實驗室2號以及](../Page/天空實驗室2號.md "wikilink")任务。

## 參考資料

## 外部链接

  - [美国国家航空航天局网站的维兹介绍](http://www.jsc.nasa.gov/Bios/htmlbios/weitz-pj.html)

[Category:美国宇航员](../Category/美国宇航员.md "wikilink")
[Category:阿波罗计划](../Category/阿波罗计划.md "wikilink")
[Category:美國海軍人物](../Category/美國海軍人物.md "wikilink")
[Category:賓夕法尼亞州人](../Category/賓夕法尼亞州人.md "wikilink")
[Category:賓夕法尼亞州立大學校友](../Category/賓夕法尼亞州立大學校友.md "wikilink")

1.