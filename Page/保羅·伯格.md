**保羅·伯格**（，），[美國](../Page/美國.md "wikilink")[生物化學家](../Page/生物化學家.md "wikilink")。

## 生平

1926年出生於[紐約](../Page/紐約.md "wikilink")[布魯克林](../Page/布魯克林.md "wikilink")。他在1980年因為有關核酸的研究貢獻，而與[沃特·吉爾伯特以及](../Page/沃特·吉爾伯特.md "wikilink")[弗雷德里克·桑格共同獲得](../Page/弗雷德里克·桑格.md "wikilink")[諾貝爾化學獎](../Page/諾貝爾化學獎.md "wikilink")。

## 參考來源

## 外部連結

  - Paul Berg narrating "Protein Synthesis: An Epic on the Cellular
    Level" at [Google
    Video](http://video.google.com/videoplay?docid=-2657697036715872139)
  - [Curriculum
    vitae](http://nobelprize.org/chemistry/laureates/1980/berg-cv.html)
    from the Nobel Prize website
  - [2001 article about
    Berg](http://news-service.stanford.edu/news/2001/october3/berg-103.html)
    from a Stanford University website
  - [1](http://nobelprize.org/chemistry/laureates/1980/berg-autobio.html)
    from the Nobel Prize website
  - [2](https://web.archive.org/web/20050418020608/http://nobelprize.org/chemistry/laureates/1980/berg-interview.ram)
    from the Nobel Prize website
  - [Carl Sagan Prize for Science
    Popularization](https://web.archive.org/web/20070807023615/http://www.wonderfest.org/html/sagan_prize_info.html)
    award recipient, Wonderfest 2006.

[Category:美國生物化學家](../Category/美國生物化學家.md "wikilink")
[Category:猶太科學家](../Category/猶太科學家.md "wikilink")
[Category:諾貝爾化學獎獲得者](../Category/諾貝爾化學獎獲得者.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:紐約市人](../Category/紐約市人.md "wikilink")
[Category:法兰西科学院院士](../Category/法兰西科学院院士.md "wikilink")
[Category:拉斯克基礎醫學研究獎得主](../Category/拉斯克基礎醫學研究獎得主.md "wikilink")
[Category:欧洲分子生物学组织会员](../Category/欧洲分子生物学组织会员.md "wikilink")
[Category:史丹佛大學醫學院教師](../Category/史丹佛大學醫學院教師.md "wikilink")
[Category:凱斯西儲大學校友](../Category/凱斯西儲大學校友.md "wikilink")
[Category:賓夕法尼亞州立大學校友](../Category/賓夕法尼亞州立大學校友.md "wikilink")
[Category:美国国家科学奖获奖者](../Category/美国国家科学奖获奖者.md "wikilink")
[Category:盖尔德纳国际奖获得者](../Category/盖尔德纳国际奖获得者.md "wikilink")
[Category:宗座科学院院士](../Category/宗座科学院院士.md "wikilink")