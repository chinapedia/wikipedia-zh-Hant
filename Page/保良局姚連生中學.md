**保良局姚連生中學**（，PLKYLSC）位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[荃灣](../Page/荃灣.md "wikilink")[石圍角邨](../Page/石圍角邨.md "wikilink")，是一所政府資助[中學](../Page/中學.md "wikilink")，於1982年創校。創校初年，位於石圍角邨的校舍尚未落成，因此要借用[保良局李城璧中學的校舍上課](../Page/保良局李城璧中學.md "wikilink")。

## 收生及教學語言

保良局姚連生中學共有24個班別。中一至中六設4個班別（A、B、C、D），每班約35人。

## 獎項

保良局姚連生中學於2006年獲得香港能源效益獎「中學」組別的金獎和創意獎。

## 歷任校長

  - 譚廣正校長（1982年至1990年）
  - 姚啟榮校長（1990年至2003年）
  - 謝伯開校長（2003年至2013年）
  - 戚美玲校長（2013年至現今）

## 著名校友

  - [鍾沛枝](../Page/鍾沛枝.md "wikilink")：2001年[香港小姐](../Page/香港小姐.md "wikilink")[亞軍](../Page/亞軍.md "wikilink")
  - [胡諾言](../Page/胡諾言.md "wikilink")：香港男藝人（1994年中五畢業）
  - 何達鴻（John Ho）：香港著名繪本畫家及平面設計師
  - 貝麗婷（[Mary姐](../Page/wikia:evchk:Mary姐.md "wikilink")）：
    著名[網絡紅人](../Page/網絡紅人.md "wikilink")（Youtuber）
  - [潘耀焯](../Page/潘耀焯.md "wikilink")：香港車路士足球學校教練、香港退役職業足球員
  - [鍾晴](../Page/鍾晴_\(藝人\).md "wikilink")：香港女性模特兒、藝人（2009年中五畢業）
  - [潘敬祖](../Page/潘敬祖.md "wikilink")：香港男歌手（1998年中五畢業）
  - [陳棨豪](../Page/陳棨豪.md "wikilink")：2017國際發呆比賽「國際發呆王」得主

## 鄰近建築

  - [城門谷游泳池](../Page/城門谷游泳池.md "wikilink")
  - [石圍角邨](../Page/石圍角邨.md "wikilink")
  - [城門谷公園](../Page/城門谷公園.md "wikilink")
  - [城門谷運動場](../Page/城門谷運動場.md "wikilink")

## 參見

  - [保良局李城璧中學](../Page/保良局李城璧中學.md "wikilink")
  - [姚連生](../Page/姚連生.md "wikilink")

## 注釋

## 外部連結

  - [保良局姚連生中學校網](http://yls.hkcampus.net/)
  - [香港能源效益獎](https://web.archive.org/web/20070927223050/http://www.eeawards.emsd.gov.hk/tc/winners_list_school.htm)

[Category:石圍角](../Category/石圍角.md "wikilink")
[P](../Category/荃灣區中學.md "wikilink")
[Category:1982年創建的教育機構](../Category/1982年創建的教育機構.md "wikilink")