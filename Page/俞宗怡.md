**俞宗怡**（英文名：，），前任[香港](../Page/香港.md "wikilink")[公務員事務局局長](../Page/公務員事務局.md "wikilink")。

俞宗怡祖籍江蘇,在香港接受教育，中學畢業於[庇理羅士女子中學](../Page/庇理羅士女子中學.md "wikilink")，1974年於[香港大學歷史系畢業](../Page/香港大學.md "wikilink")，同年加入[香港政府擔任](../Page/香港政府.md "wikilink")[行政主任](../Page/行政主任.md "wikilink")，同年轉為[政務主任（AO）](../Page/政務主任.md "wikilink")，曾任職[民政署](../Page/民政署.md "wikilink")、[財政科及](../Page/財政科.md "wikilink")[新界政務署](../Page/新界政務署.md "wikilink")。

## 簡歷

  - 1981年升為高級[政務主任](../Page/政務主任.md "wikilink")，任職[布政司署](../Page/布政司署.md "wikilink")。
  - 1983年年晉升為丙級政務主任，任[布政司總署助理署長](../Page/布政司總署.md "wikilink")。
  - 1984年任首席助理[教育統籌司](../Page/教育統籌司.md "wikilink")。
  - 1985年出任[香港政府駐紐約經貿辦事處副處長](../Page/香港政府駐紐約經貿辦事處.md "wikilink")。
  - 1988年負笈[美國](../Page/美國.md "wikilink")[哈佛大學留學深造](../Page/哈佛大學.md "wikilink")，取得[公共行政管理碩士學位](../Page/公共行政管理.md "wikilink")。
  - 1988年轉往政府[財政科](../Page/財政科.md "wikilink")，擔任首席助理[財政司](../Page/財政司.md "wikilink")。
  - 1991年9月至1992年4月出任[區域市政總署副署長](../Page/區域市政總署.md "wikilink")。
  - **1992年，俞宗怡以40歲之齡成為最年輕的甲級政務官。**
  - 1992年4月至1993年3月任副[工商司](../Page/工商司.md "wikilink")。
  - 1993年5月至1995年9月出任[工業署](../Page/工業署.md "wikilink")（今創新科技署）署長。
  - **1995年11月至1998年3月晉升任[工商局局長](../Page/工商局.md "wikilink")（回歸前称工商司），成爲港府有史以來首位掌管[工商司的女高官](../Page/工商司.md "wikilink")。**
  - **1998年4月至2002年6月出任特區[庫務局局長](../Page/庫務局.md "wikilink")，在[財政司司長離埠期間擔任署理財政司司長](../Page/財政司.md "wikilink")。曾經出現過[政務司](../Page/政務司.md "wikilink")、[財政司](../Page/財政司.md "wikilink")、[律政司三司](../Page/律政司.md "wikilink")，全部由女性掌管的局面。**
  - 2002年7月董建華推出問責制后，俞宗怡獲委任為[工商及科技局](../Page/工商及科技局.md "wikilink")[常任秘書長](../Page/常任秘書長.md "wikilink")（工商），協助制定香港的[外貿政策及](../Page/外貿政策.md "wikilink")[知識產權政策](../Page/知識產權.md "wikilink")，促進外貿關係及本地工商業發展。
  - 2004年8月成為[香港部長級會議統籌辦事處督導委員會主席](../Page/香港部長級會議統籌辦事處督導委員會.md "wikilink")，負責協助特區政府籌辦[世界貿易組織香港部長級會議](../Page/世界貿易組織香港部長級會議.md "wikilink")。
  - 2006年1月24日以停薪留職的方式轉任[公務員事務局局長](../Page/公務員事務局.md "wikilink")，委任期直到2007年6月30日。
  - 2007年7月1日她再獲委任[公務員事務局局長](../Page/公務員事務局.md "wikilink")，任期直到2012年6月30日。

## 職務

  - 1981年：[布政司署高級政務主任](../Page/布政司署.md "wikilink")
  - 1983年：[市政總署助理署長](../Page/市政總署.md "wikilink")
  - 1984年：首席助理[教育統籌司](../Page/教育統籌司.md "wikilink")
  - 1989年：首席助理[經濟司](../Page/經濟局.md "wikilink")
  - 1991年：[區域市政總署副署長](../Page/區域市政總署.md "wikilink")
  - 1992年：[副工商司](../Page/工商及科技局.md "wikilink")
  - 1993年：[工業署署長](../Page/工業署.md "wikilink")
  - 1995年：工商司
  - 1998年：[庫務局局長](../Page/庫務局.md "wikilink")
  - 2002年：[工商及科技局常任秘書長](../Page/工商及科技局.md "wikilink")（工商）
  - 2006年：[公務員事務局局長](../Page/公務員事務局.md "wikilink")

## 審批[梁展文事件失誤](../Page/梁展文事件.md "wikilink")

## 其他

  - 俞宗怡於2001年獲頒[金紫荊星章](../Page/金紫荊星章.md "wikilink")。
  - 俞宗怡是「[手袋黨](../Page/手袋黨.md "wikilink")」成員之一。
  - 俞宗怡曾經和[黎年以及](../Page/黎年.md "wikilink")[李淑儀一同在](../Page/李淑儀.md "wikilink")[香港大學](../Page/香港大學.md "wikilink")[歷史系就讀](../Page/歷史.md "wikilink")。
  - [香港](../Page/香港.md "wikilink")[傳媒暱稱俞小姐為](../Page/傳媒.md "wikilink")「**D姐**」。
  - 俞小姐業餘時間喜歡[讀書](../Page/讀書.md "wikilink")，也喜歡打[高爾夫和](../Page/高爾夫.md "wikilink")[旅遊](../Page/旅遊.md "wikilink")。
  - 俞小姐和前[民政事務局常任秘書長](../Page/民政事務局.md "wikilink")[尤曾家麗女士等人組成馬主團體](../Page/尤曾家麗.md "wikilink")，名為「新秀團體」，擁有一匹名叫「勝任愉快」的馬匹，支持[慈善事業](../Page/慈善.md "wikilink")。\[1\]其後，新秀團體於2008年[香港國際馬匹拍賣會引入一匹名叫](../Page/香港國際馬匹拍賣會.md "wikilink")「勝任有餘」的馬匹，馬主成員亦包括俞宗怡和[商務及經濟發展局局長](../Page/商務及經濟發展局.md "wikilink")[劉吳惠蘭](../Page/劉吳惠蘭.md "wikilink")，[平機會主席](../Page/平機會.md "wikilink")[林煥光等人](../Page/林煥光.md "wikilink")。2014年，新秀團體再度引入自購新馬，名為「勝任如意」，馬主團體成員除了俞宗怡、[劉吳惠蘭外](../Page/劉吳惠蘭.md "wikilink")，還有[何鑄明](../Page/何鑄明.md "wikilink")、[譚榮邦等前政府高官](../Page/譚榮邦.md "wikilink")。

## 參考文獻

[Category:庇理羅士女子中學校友](../Category/庇理羅士女子中學校友.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[Y](../Category/哈佛大學校友.md "wikilink")
[Y](../Category/前香港政府官員.md "wikilink")
[Y](../Category/香港女性政治人物.md "wikilink")
[Category:獲頒授香港金紫荊星章者](../Category/獲頒授香港金紫荊星章者.md "wikilink")
[C](../Category/俞姓.md "wikilink")
[Category:香港建制派人士](../Category/香港建制派人士.md "wikilink")

1.  〈[高官買馬改名「勝任愉快」](http://www1.appledaily.atnext.com//template/apple/art_main.cfm?iss_id=20020716&sec_id=4104&subsec_id=15333&art_id=2715633)
    〉，《香港蘋果日報》，2002年7月16日。