**信天翁属**（屬名：**）在[动物分类学上是](../Page/动物分类学.md "wikilink")[鸟纲](../Page/鸟纲.md "wikilink")[鹱形目](../Page/鹱形目.md "wikilink")[信天翁科中的一个](../Page/信天翁科.md "wikilink")[属](../Page/属.md "wikilink")，分为六种。

## 種\[1\]

  - 以下是《世界年类分类与分布名录》中記載的信天翁屬種類：

<!-- end list -->

  - [漂泊信天翁](../Page/漂泊信天翁.md "wikilink") *D. exulans*
  - [安島信天翁](../Page/安島信天翁.md "wikilink") *D. antipodensis*
  - [阿島信天翁](../Page/阿島信天翁.md "wikilink") *D. amsterdamensis*
  - [特島信天翁](../Page/特島信天翁.md "wikilink") *D. dabbenea*
  - [皇信天翁](../Page/皇信天翁.md "wikilink") *D. epomophora*
  - [加岛信天翁](../Page/加岛信天翁.md "wikilink") *D. irrorata*
  - [短尾信天翁](../Page/短尾信天翁.md "wikilink") *D. albatrus*
  - [黑脚信天翁](../Page/黑脚信天翁.md "wikilink") *D. nigripes*
  - [黑背信天翁](../Page/黑背信天翁.md "wikilink") *D. immutabilis*
  - [黑眉信天翁](../Page/黑眉信天翁.md "wikilink") *D. melanophris*
  - [新西兰信天翁](../Page/新西兰信天翁.md "wikilink") *D. bulleri*
  - [白顶信天翁](../Page/白顶信天翁.md "wikilink") *D. cauta*
  - [黄鼻信天翁](../Page/黄鼻信天翁.md "wikilink") *D. chlororhynchos*
  - [灰头信天翁](../Page/灰头信天翁.md "wikilink") *D. chrysostoma*

<!-- end list -->

  - 經過修改後的信天翁屬種類：

<!-- end list -->

  - [漂泊信天翁](../Page/漂泊信天翁.md "wikilink") *D. exulans*
  - [安島信天翁](../Page/安島信天翁.md "wikilink") *D. antipodensis*
  - [阿島信天翁](../Page/阿島信天翁.md "wikilink") *D. amsterdamensis*
  - [特島信天翁](../Page/特島信天翁.md "wikilink") *D. dabbenea*
  - [北方皇家信天翁](../Page/北方皇家信天翁.md "wikilink") *D. sanfordi*
  - [南方皇家信天翁](../Page/南方皇家信天翁.md "wikilink") *D. epomophora*

<!-- end list -->

  - 已經絕種的信天翁屬種類：
    *D. milleri*
    *D. thyridata*

## 參考資料

<div class="references-small">

<references />

</div>

[Category:信天翁科](../Category/信天翁科.md "wikilink")

1.  郑光美主编 世界年类分类与分布名录 2002年 科学出版社 ISBN 7-03-010294-0