**信息技术基础架构库**（Information Technology Infrastructure
Library，一般情况下使用其缩写**ITIL**，也譯為「**資訊技術基礎建設館**」\[1\]）是用来管理[信息技术的](../Page/信息技术.md "wikilink")[架构设计](../Page/架构.md "wikilink")，研发和操作的一整套概念和思想。
[Itil_service_stages.jpg](https://zh.wikipedia.org/wiki/File:Itil_service_stages.jpg "fig:Itil_service_stages.jpg")
主要精神為和諧推動及持續改善，將服務對象視為客戶，強調End-to-End的服務。

ITIL最初是藉由一套书籍发布。这套书籍的每一本都涵盖一个信息技术的领域。*ITIL*这个名称和*IT Infrastructure
Library*都是[英国](../Page/英国.md "wikilink")[政府商務辦公室](../Page/政府商務辦公室_\(英國\).md "wikilink")
(OGC)的[注册商标](../Page/注册商标.md "wikilink")。藉由为不同的IT组织量身定制一些复杂的清单，任务，流程，ITIL为许多重要的IT时间准则给出了详尽的解释。

## ITIL简介

ITIL是一套主要研究有关IT服务管理的方法。同时其研究的内容主要分成二本书，分别是Service Delivery和Service
Support二大部份。

在了解有关知识之后，可以报名参加一系列复杂的考试，最后获得ITIL官方对于内容掌握的认证。其中两项重要的认证是 ITIL Version 3
and ITIL Version 2 资格认证。

在你通过一系列资格认证之后，你可以获得报名考试其他资格认证的许可，在完成所有的ITIL资历认证的点数之后，你可以获得ITIL专家(expert)和大师(master)的认证。有关学分点数系统可以在它的官方网站找到说明和图片介绍。

## ITIL v.2 资格认证简介

以下作些许的说明: Service Delivery是属于战略性的思考，偏管理规范或协议，共分五大流程，分别是：

  - [服务水平管理](../Page/服务水平管理.md "wikilink")（Service Level Managemnet）
  - [财务管理](../Page/财务管理.md "wikilink")（Financial Management）
  - [可用性管理](../Page/可用性管理.md "wikilink")（Availability Management）
  - [能力管理](../Page/能力管理.md "wikilink")（Capacity Management）
  - [持续性管理](../Page/持续性管理.md "wikilink")（Continuity Management）

Service Support是属于操作面规范，亦分五大流程，分别是

  - [事故管理](../Page/事故管理_\(信息技术服务管理\).md "wikilink")（Incident management）
  - [问题管理](../Page/问题管理.md "wikilink")（Problem management）
  - [配置管理](../Page/配置管理.md "wikilink")（Configuration Management）
  - [变更管理](../Page/变更管理.md "wikilink")（Change Management）
  - [发布管理](../Page/发布管理.md "wikilink")（Release Management）

再加上服务台(Service Desk)及信息安全管理(Security Management)二个流程，即为其全部内容。

## ITIL v.3 资格认证简介

第三版于2007年5月正式改版，结合 IT 服务管理生命周期 (Service Life
Cycle)的观念扩增为５个核心模块，分别是服务策略(Service
Strategy, SS)、服务设计(Service Design, SD)、服务转移(Service Transition,
ST)、服务运营(Service Operation, SO)、持续服务改善(Continual Service
Improvement, CSI)

## 认证

[Wearer_of_an_ITIL_Foundation_Certificate_pin.jpg](https://zh.wikipedia.org/wiki/File:Wearer_of_an_ITIL_Foundation_Certificate_pin.jpg "fig:Wearer_of_an_ITIL_Foundation_Certificate_pin.jpg")

ITIL认证是由ITIL Certification Management Board (ICMB)掌管。这家机构由OGC, IT
Service Management
Forum（[itSMF](../Page/IT_Service_Management_Forum.md "wikilink")）International和如下两家考试机构组成：[EXIN](http://www.exin-exams.com/)（总部设在荷兰），[ISEB](http://www.bcs.org/BCS/Products/Qualifications/ISEB/)（总部设在英国）.

EXIN 和 ISEB
颁发的资格认证分为如下几个级别：基础，使用者以及管理人员。“管理人员”级别又分为‘ITIL服务管理’，‘ITIL应用管理’和‘ICT
Infrastructure 管理’。

在ITIL认证注册的运行下，ITIL认证的使用者级别的用户通常可以自发的进行注册。

ITIL系统不会对组织和管理系统进行“ITIL认证”。但是如果组织或者管理系统积极的实现ITIL框架内的要求，组织和管理单位可以申请[ISO/IEC
20000认证](../Page/ISO/IEC_20000.md "wikilink")。

2006年7月20日，OGC与[APM
Group](http://arquivo.pt/wayback/20160518030526/http%3A//www.apmg%2Dinternational.com/)签订商务合作协议，将从2007年
1月1日起共同进行ITIL认证的认证。

## ITIL的兄弟认证

[信息技术服务管理](../Page/信息技术服务管理.md "wikilink")（ITSM）是一种和ITIL相关但是不完全相同的[概念](../Page/概念.md "wikilink")。ITIL的第二版包括了专门用于定义“IT服务管理”的章节。（第三版的五卷里面目前还没有相关的内容）。

## 参考文献

## 外部連結

  - [官方網站](https://www.axelos.com/best-practice-solutions/itil)

[分类:信息技术服务管理](../Page/分类:信息技术服务管理.md "wikilink")

[Category:資訊科技管理](../Category/資訊科技管理.md "wikilink")

1.