**信江**位于[中國](../Page/中國.md "wikilink")[江西省东北部](../Page/江西省.md "wikilink")，是[鄱阳湖水系五大河流之一](../Page/鄱阳湖.md "wikilink")。

## 流域

发源于[玉山县](../Page/玉山县.md "wikilink")[怀玉山脉](../Page/怀玉山脉.md "wikilink")[三清山的](../Page/三清山.md "wikilink")[玉山水和发源于](../Page/玉山水.md "wikilink")[福建省](../Page/福建省.md "wikilink")[浦城县](../Page/浦城县.md "wikilink")[武夷山脉](../Page/武夷山脉.md "wikilink")[铜钹山的](../Page/铜钹山.md "wikilink")[丰溪](../Page/丰溪.md "wikilink")，在[上饶市区汇合后始称信江](../Page/上饶.md "wikilink")。干流自东向西流向，流经上饶、[铅山](../Page/铅山.md "wikilink")、[弋阳](../Page/弋阳.md "wikilink")、[贵溪](../Page/贵溪.md "wikilink")、[鹰潭](../Page/鹰潭.md "wikilink")、[余江](../Page/余江.md "wikilink")、[余干等县市](../Page/余干.md "wikilink")，在余干县境分为两支注入[鄱阳湖](../Page/鄱阳湖.md "wikilink")，沿途汇纳了石溪水、铅山水、陈坊水、葛溪水、罗塘河、[白塔河等主要支流](../Page/白塔河.md "wikilink")。

信江以上饶和鹰潭为界，分为上游、中游、下游三段。上游沿岸一带以中低山为主，地形起伏较大。中游为信江盆地，其边缘地势由北、东、南三面渐次向中间降低，并向西倾斜。下游为鄱阳湖冲积平原区，地势平坦开阔。

干流全长360.5km\[1\]，流域面积17600平方公里，流域内山地面积占40%，丘陵占35%，平原占25%。现有人口420余万，现有耕地391万亩，其中水田349万亩。

主要支流有：[玉琊溪](../Page/玉琊溪.md "wikilink")、[饶北河](../Page/饶北河.md "wikilink")（灵溪）、[丰溪河](../Page/丰溪.md "wikilink")、[石溪河](../Page/石溪河.md "wikilink")（泸溪）、[铅山河](../Page/铅山河.md "wikilink")、[葛溪河](../Page/葛溪河.md "wikilink")、[罗塘水](../Page/罗塘水.md "wikilink")（须溪、罗塘港）、[白塔河](../Page/白塔河.md "wikilink")、[万年河](../Page/万年河.md "wikilink")。\[2\]

## 气候

信江流域位于[副热带季风气候区](../Page/副热带季风气候区.md "wikilink")。一般从4月开始，雨量逐渐增加；至5～6月份，降雨猛增。流域上游的怀玉山一带，中心年降雨量一般都在2000毫米以上。7～9月常受[副热带高压控制](../Page/副热带高压.md "wikilink")，除地方性雷阵雨和台风雨外，雨水稀少。流域内多年平均气温为18℃，年平均降雨量为1845毫米，降雨分布是东多西少，山区多于平原。年内分布极不均匀，4～6月占全年雨量的50%，7～9月则只占18%，常出现上半年雨多易涝，下半年又雨少易旱的情况。

## 水利工程

流域内建有大型水库三座，其中干流上建有[信州水利枢纽](../Page/信州水利枢纽.md "wikilink")（上饶市）、[界牌航电枢纽](../Page/界牌水库_\(鹰潭\).md "wikilink")（鹰潭市）；支流上有[金沙溪上的](../Page/金沙溪.md "wikilink")[玉山](../Page/玉山.md "wikilink")[七一水库](../Page/七一水库.md "wikilink")（信江流域目前最大的一座水库）、[泸溪河上的](../Page/泸溪河.md "wikilink")[上饶县](../Page/上饶县.md "wikilink")[大坳水库](../Page/大坳水库.md "wikilink")。

引水工程则以余江的[白塔渠为最大](../Page/白塔渠.md "wikilink")，引水流量19立方米每秒，灌田19万亩。

## 污染

信江流域原来工业不发达，主要污染源除了生活污染外，以铅山[永平铜矿和](../Page/永平铜矿.md "wikilink")[贵溪冶炼厂为最大](../Page/贵溪冶炼厂.md "wikilink")。近年来随着流域内经济的发展，信江的污染也越来越重。2005年[浙江省对重污染企业进行清理后](../Page/浙江省.md "wikilink")，江西的地方官员处于招商引资的政绩考虑，争相以优惠措施吸引入驻。这些企业以[造纸](../Page/造纸.md "wikilink")、[冶金](../Page/冶金.md "wikilink")、[化工](../Page/化工.md "wikilink")、[制药类居多](../Page/制药.md "wikilink")，其中尤以[氟化工厂危害性最为严重](../Page/氟.md "wikilink")，继玉山氟化工厂污染后，上饶市又有两个氟化工厂进驻。很多企业没有污水处理设备，如2005年铅山县工业园区引进的恒益纸业和信江纸业，其生产污水不经任何处理，直接往信江排放。\[3\]

## 参考文献

[Category:鄱阳湖水系](../Category/鄱阳湖水系.md "wikilink")
[Category:江西河流](../Category/江西河流.md "wikilink")

1.
2.  [信江流域水文概况](http://www.jxfx.gov.cn/xjfx/docs/xjlyswgs.html)
3.  [信江污染不能承受之重](http://news.xinhuanet.com/environment/2008-02/20/content_7636683.htm)