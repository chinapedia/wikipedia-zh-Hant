**信義松德站**位於[中華民國](../Page/中華民國.md "wikilink")[臺北市](../Page/臺北市.md "wikilink")[信義區](../Page/信義區.md "wikilink")，曾是[臺北捷運](../Page/臺北捷運.md "wikilink")[淡水信義線規劃中的](../Page/淡水信義線.md "wikilink")[捷運車站](../Page/捷運車站.md "wikilink")。因車站用地的居民反對，無法取得用地，[臺北市政府捷運工程局於](../Page/臺北市政府捷運工程局.md "wikilink")[2015年](../Page/2015年.md "wikilink")[3月31日決定廢止此站的設站計劃](../Page/3月31日.md "wikilink")\[1\]。

## 車站概要

車站預計設於信義路下方，在信義路松德路口略東，信義路六段26巷至76巷間。\[2\]
車站位處舊地名「蝴蝶陂」，原有一[埤塘](../Page/埤塘.md "wikilink")，埤塘的所在地現為[臺北市立聯合醫院松德院區](../Page/臺北市立聯合醫院.md "wikilink")。
車站原計劃設於[信義路及](../Page/信義路_\(台北市\).md "wikilink")[松德路路口一帶](../Page/松德路.md "wikilink")。

## 車站週邊

[臺北市松德公園.JPG](https://zh.wikipedia.org/wiki/File:臺北市松德公園.JPG "fig:臺北市松德公園.JPG")

  - 松友公園
  - [臺北市政府松德辦公大樓](../Page/臺北市政府.md "wikilink")
  - 虎林公園
  - [臺北市立聯合醫院松德院區](../Page/臺北市立聯合醫院.md "wikilink")
  - [永春高中](../Page/永春高中.md "wikilink")
  - 永春崗公園
  - 松德公園
  - [臺北市立興雅國民中學](../Page/臺北市立興雅國民中學.md "wikilink")
  - [信義快速道路](../Page/信義快速道路.md "wikilink")（信義路端）
  - [象山](../Page/象山_\(台北市\).md "wikilink")

## 歷史

2012年3月28日，信義區公所召開車站命名會議，會議結論以**信義松德站**、**永春高中站**、**博愛興雅站**為建議名稱，並提報臺北市政府捷運工程局評審小組審議。

曾定2019年12月完工通車。\[3\]2015年3月31日，因車站所在地居民以距離[象山站僅](../Page/象山站.md "wikilink")600公尺等原因反對設站，台北市政府捷運工程局正式決定廢止此站的設站及聯合開發計劃。

## 参考文献

[Category:2012年啟用的鐵路車站](../Category/2012年啟用的鐵路車站.md "wikilink")
[Category:未成站](../Category/未成站.md "wikilink")

1.
2.
3.  [＜終點在廣慈博愛園區＞捷運信義線
    延伸2站-Yahoo\!奇摩新聞](http://tw.news.yahoo.com/article/url/d/a/091224/78/1xjki.html)