[Taipei_Xinyi_District_Skyline.jpg](https://zh.wikipedia.org/wiki/File:Taipei_Xinyi_District_Skyline.jpg "fig:Taipei_Xinyi_District_Skyline.jpg")的天際線\]\]
[Xinyi_District_Night_view_201608.JPG](https://zh.wikipedia.org/wiki/File:Xinyi_District_Night_view_201608.JPG "fig:Xinyi_District_Night_view_201608.JPG")
[市府轉運站&統一國際大樓.jpg](https://zh.wikipedia.org/wiki/File:市府轉運站&統一國際大樓.jpg "fig:市府轉運站&統一國際大樓.jpg")
[2006ComputexDay5-17.jpg](https://zh.wikipedia.org/wiki/File:2006ComputexDay5-17.jpg "fig:2006ComputexDay5-17.jpg")的[信義商圈空橋系統由多座過街天橋連結而成](../Page/信義商圈空橋系統.md "wikilink")，圖中的天橋聯絡了[臺北世界貿易中心與](../Page/臺北世界貿易中心.md "wikilink")[臺北101](../Page/臺北101.md "wikilink")[購物中心](../Page/購物中心.md "wikilink")\]\]
[松智路.jpg](https://zh.wikipedia.org/wiki/File:松智路.jpg "fig:松智路.jpg")

**信義計畫區**是位於[臺灣](../Page/臺灣.md "wikilink")[臺北市](../Page/臺北市.md "wikilink")[信義區的](../Page/信義區_\(臺北市\).md "wikilink")[都市更新區域](../Page/都市更新.md "wikilink")，總面積153[公頃](../Page/公頃.md "wikilink")，1970年代提出副都心計畫，自1980年代開始進行開發，現已成為[臺北首要的](../Page/臺北.md "wikilink")[中心商業區](../Page/中心商業區.md "wikilink")（CBD），[台北101](../Page/台北101.md "wikilink")、[臺北市政府](../Page/臺北市政府.md "wikilink")、[臺北市議會](../Page/臺北市議會.md "wikilink")、[臺北世界貿易中心等重要設施皆位於區內](../Page/臺北世界貿易中心.md "wikilink")，且區內亦匯集眾多[百貨](../Page/百貨.md "wikilink")[商場](../Page/商場.md "wikilink")、[旅館](../Page/旅館.md "wikilink")、[豪宅及企業總部](../Page/豪宅.md "wikilink")。今日該區內之[商業區塊又常被稱為](../Page/商圈.md "wikilink")**信義商圈**。

## 發展背景

信義計畫區該地最早為[四四兵工廠所在](../Page/四四兵工廠.md "wikilink")。其歷史發展起於1976年，翌年副都心計畫提出，[臺北市政府變更](../Page/臺北市政府.md "wikilink")[國父紀念館以東地區為特定專用區作為副都心](../Page/國父紀念館.md "wikilink")，目標為設新市政中心及次商業中心以引導[都市均衡發展](../Page/都市.md "wikilink")，疏解西區舊都心（[臺北車站](../Page/臺北車站.md "wikilink")、[西門町一帶商圈](../Page/西門町.md "wikilink")）的商業擁擠並增進東區繁榮及居民都市生活之便利，配合住宅發展政策提供良好之居住環境，興建完整之示範性新社區；並在第二次通盤檢討案中，採用旅日建築師[郭茂林所規劃的發展架構](../Page/郭茂林.md "wikilink")，計畫導入郭茂林在東京[霞關大樓與](../Page/霞關大樓.md "wikilink")[新宿副都心的規劃經驗](../Page/新宿副都心.md "wikilink")，大幅擴增此計畫區內商業投資誘因，以吸引跨國性的[金融服務或高科技的](../Page/金融.md "wikilink")[資訊產業](../Page/資訊產業.md "wikilink")，因此信義計畫區是臺北市唯一有超大完整街廓設計，具備完善都市規畫設計的商業發展區；而商圈最大的特色是完全針對都市人休閒購物的需求設計，加上市府刻意經營規劃相當多的造景，使得進駐的[百貨商場和企業大樓也都別具特色](../Page/商場.md "wikilink")。

## 範圍

信義計劃區劃定的範圍總面積153公頃，信義計畫區大約以[基隆路一段](../Page/基隆路.md "wikilink")、[信義路五段](../Page/信義路_\(台北市\).md "wikilink")、松德路、[忠孝東路五段為界](../Page/忠孝東路.md "wikilink")。早期此區域的土地皆由[國防部所有](../Page/中華民國國防部.md "wikilink")，[聯勤44兵工廠即座落於此](../Page/聯勤44兵工廠.md "wikilink")，周圍並設有不少[眷村](../Page/眷村.md "wikilink")（如：[四四南村](../Page/四四南村.md "wikilink")、[四四西村等](../Page/四四西村.md "wikilink")）以就地安置在兵工廠工作的軍眷家屬。後來這些單位陸續搬遷後，再經政府規劃之下，依地區未來的用途的不同，規劃成住宅區（如松智路、松勤路一帶）、商業區（松壽路、松高路一帶）、文教區（如[博愛國小](../Page/臺北市信義區博愛國民小學.md "wikilink")、[興雅國中](../Page/臺北市立興雅國民中學.md "wikilink"))，使其成為一個商業金融的中心，同時也成為整個臺北的新都心。

## 發展概況

臺北市政府期望以此計畫區成為臺北的發展脈絡中心，並且成為都市中再中心化的重要場域，為了使整個區域的發展力求一致，本區內的每個開發個案都尚需要經過都市計劃暨土地開發許可審議委員會的審議，使得每個個案的規劃都能符合信義計畫區目標之下的整體發展，企圖規範開發商在豐富都市地景、繁榮地方發展的不同開發中，力求基本的次序、安全與井然有序。

現在的信義計劃區幾乎已開發完成，除了指標性的[臺北世界貿易中心](../Page/臺北世界貿易中心.md "wikilink")、[臺北101之外](../Page/臺北101.md "wikilink")，週邊還有許多企業集團的辦公設施，如[國泰金融中心](../Page/國泰人壽.md "wikilink")、[新光銀行總部大樓](../Page/新光銀行.md "wikilink")、[華南銀行總部大樓](../Page/華南銀行.md "wikilink")、[南山人壽](../Page/南山人壽.md "wikilink")（[臺北南山廣場](../Page/臺北南山廣場.md "wikilink")）、[遠雄集團總部大樓](../Page/遠雄集團.md "wikilink")、[ING安泰人壽](../Page/ING集團.md "wikilink")、[震旦行](../Page/震旦行.md "wikilink")、[群益證券](../Page/群益證券.md "wikilink")、[摩根大通金融大樓](../Page/摩根大通.md "wikilink")、[花旗銀行臺灣總部](../Page/花旗銀行.md "wikilink")、[中油企業總部](../Page/臺灣中油公司.md "wikilink")、[克緹國際大樓等](../Page/克緹大樓.md "wikilink")，未來[富邦集團總部也將遷入此區](../Page/富邦集團.md "wikilink")，將新建一棟52樓，樓高265.5公尺的富邦總部大樓，完工後將成為臺北第四高樓，此外還有高檔的出租公寓如[新光信義傑士堡](../Page/新光信義傑士堡.md "wikilink")，以及不少的[百貨公司和餐飲娛樂設施](../Page/百貨公司.md "wikilink")，如[新光三越信義新天地](../Page/新光三越.md "wikilink")、[信義威秀影城](../Page/威秀影城.md "wikilink")、[ATT
4
FUN](../Page/ATT_4_FUN.md "wikilink")、[統一時代百貨](../Page/統一時代百貨.md "wikilink")、[微風松高](../Page/微風松高.md "wikilink")、[微風信義](../Page/微風信義.md "wikilink")、[寶麗廣場](../Page/BELLAVITA.md "wikilink")、[誠品信義旗艦店及](../Page/誠品書店.md "wikilink")[君悅飯店](../Page/台北君悅酒店.md "wikilink")、[臺北W飯店](../Page/台北W飯店.md "wikilink")、[臺北艾美酒店等](../Page/台北寒舍艾美酒店.md "wikilink")，未來將新增凱悅集團旗下最高檔的酒店Park
Hyatt
Taipei與Andaz，現在已經躍升為臺北主要的商業區之一。而臺北市的行政中樞─臺北市政府與[臺北市議會](../Page/臺北市議會.md "wikilink")，也分別於1994年與1990年搬遷至區內。

目前該區亦是全世界百貨公司最密集的區域，包含[新光三越信義新天地A](../Page/新光三越信義新天地.md "wikilink")11、A8、A9、A4四個館、[臺北101](../Page/臺北101.md "wikilink")、[信義威秀影城](../Page/信義威秀影城.md "wikilink")、[ATT
4
FUN](../Page/ATT_4_FUN.md "wikilink")、[統一時代百貨](../Page/統一時代百貨.md "wikilink")、[BELLAVITA](../Page/BELLAVITA.md "wikilink")、[信義誠品](../Page/信義誠品.md "wikilink")、[微風松高](../Page/微風松高.md "wikilink")、[微風信義](../Page/微風信義.md "wikilink")、[NEO19及](../Page/NEO19.md "wikilink")[微風南山](../Page/微風南山.md "wikilink")；於0.5平方公里內就有14間百貨商場，百貨之間可透過[信義商圈空橋系統或地下通道往來](../Page/信義商圈空橋系統.md "wikilink")，可由[市政府站步行至](../Page/市政府站.md "wikilink")[臺北101/世貿站](../Page/臺北101/世貿站.md "wikilink")。
由於交通量已經達到飽和，除了現有在計畫區北側的[忠孝東路上有](../Page/忠孝東路.md "wikilink")[臺北捷運板南線之外](../Page/臺北捷運板南線.md "wikilink")，在計畫區南側的[信義路上尚有](../Page/信義路_\(台北市\).md "wikilink")[臺北捷運信義線](../Page/臺北捷運信義線.md "wikilink")，已於2013年11月24日通車啟用。

近年將會增加三棟百貨商場，包括原為世貿二館，預計建設48樓的[南山廣場](../Page/南山廣場.md "wikilink")，樓高273.6公尺的摩天大樓，已於2018年底完工，超越同區的國泰置地廣場，成為台北第二高建築。目前已被微風百貨承租，「微風南山」已於2019年1月開幕，將成為信義計畫區內第三間微風百貨。
而信義大遠百，以及中信金總部拆除後再建樓高280公尺的Taipei Sky
Tower，竣工後將成為台北第二高樓與台灣第三高樓，也將使信義區的百貨商場數增添為16棟。

信義計畫區亦為許多駐臺外交機構所在地，包括[韓國](../Page/駐台北韓國代表部.md "wikilink")、[蒙古](../Page/台北烏蘭巴托貿易經濟代表處.md "wikilink")、[印度](../Page/印度—台北協會.md "wikilink")、[土耳其](../Page/駐台北土耳其貿易辦事處.md "wikilink")、[歐盟](../Page/歐洲經貿辦事處.md "wikilink")、[瑞典](../Page/瑞典貿易暨投資委員會台北辦事處.md "wikilink")、[義大利](../Page/義大利經濟貿易文化推廣辦事處.md "wikilink")、[波蘭](../Page/華沙貿易辦事處.md "wikilink")、[斯洛伐克](../Page/斯洛伐克經濟文化辦事處.md "wikilink")、[瑞士](../Page/瑞士商務辦事處.md "wikilink")、[以色列](../Page/駐臺北以色列經濟文化辦事處.md "wikilink")、[阿根廷](../Page/阿根廷商務文化辦事處.md "wikilink")、[墨西哥](../Page/墨西哥商務簽證文件暨文化辦事處.md "wikilink")、[秘魯](../Page/秘魯駐臺北商務辦事處.md "wikilink")、[哥倫比亞](../Page/哥倫比亞商務辦事處.md "wikilink")（國貿大樓）、[英國](../Page/英國在台辦事處.md "wikilink")、[澳洲](../Page/澳洲辦事處.md "wikilink")（統一國際大樓）、[荷蘭](../Page/荷蘭貿易暨投資辦事處處.md "wikilink")（遠雄金融中心）、[德國](../Page/德國在台協會.md "wikilink")（台北101）、[加拿大](../Page/加拿大駐台北貿易辦事處.md "wikilink")、[紐西蘭](../Page/紐西蘭商工辦事處.md "wikilink")（華新麗華大樓）、[智利](../Page/智利商務辦事處.md "wikilink")、[俄羅斯](../Page/莫斯科台北經濟文化合作協調委員會.md "wikilink")（震旦國際大樓）。

[File:Taipei,_Taiwan_CBD_Skyline.jpg|台北信義區天際線](File:Taipei,_Taiwan_CBD_Skyline.jpg%7C台北信義區天際線)
<File:Taipei> Financial
Center.jpg|[臺北世界貿易中心與](../Page/臺北世界貿易中心.md "wikilink")[臺北101](../Page/臺北101.md "wikilink")
<File:Taipei> City Hall
(0097).JPG|信義計畫區內最早的大型建築物-[臺北市政大樓](../Page/臺北市政府.md "wikilink")
<File:Xinyi> District - from Taipei 101
Observatory.jpg|由臺北101鳥瞰[臺北市政府](../Page/臺北市政府.md "wikilink")、[臺北市議會](../Page/臺北市議會.md "wikilink")、[國父紀念館等建築](../Page/國父紀念館.md "wikilink")（[附說明版請點擊此](../Page/:File:Xinyi_District_-_from_Taipei_101_Observatory\(Tagged\).jpg.md "wikilink")）
<File:2010> 07 22160 6923 Xinyi District, Taipei, Buildings, Streets in
Taipei, Taipei 101, Observation towers, Taipei City Hall, Town halls,
Taiwan.JPG|由臺北101俯瞰信義計畫區北側
[File:Taipei_from_101_building.jpg|由](File:Taipei_from_101_building.jpg%7C由)[臺北101鳥瞰信義計畫區東側](../Page/臺北101.md "wikilink")
[File:市府轉運站大樓.jpg|交通樞紐](File:市府轉運站大樓.jpg%7C交通樞紐)[市府轉運站大樓](../Page/市府轉運站.md "wikilink")
<File:Taipei> Rushhour birdseye.JPG|從臺北101觀景臺眺望信義計劃區的夜景
[File:Taipei_Skyscrapers_Aerial_View.jpg|信義區空拍](File:Taipei_Skyscrapers_Aerial_View.jpg%7C信義區空拍)

### 主要建築

[Xinyi_Special_District_core_area_map.svg](https://zh.wikipedia.org/wiki/File:Xinyi_Special_District_core_area_map.svg "fig:Xinyi_Special_District_core_area_map.svg")

<table>
<thead>
<tr class="header">
<th><p>區塊</p></th>
<th><p>建築名稱</p></th>
<th><p>樓層數</p></th>
<th><p>高度(m)</p></th>
<th><p>樓板面積(m<sup>2</sup>)</p></th>
<th><p>狀態</p></th>
<th><p>啟用年度</p></th>
<th><p>用途</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>車站</p></td>
<td><p><a href="../Page/市府轉運站.md" title="wikilink">市府轉運站</a><br />
（統一時代百貨、W Hotel）</p></td>
<td><p>31</p></td>
<td><p>151</p></td>
<td><p>144,037.15</p></td>
<td><p>完工</p></td>
<td><p>2010年</p></td>
<td><p>商場、旅館、交通</p></td>
<td><p>[1]</p></td>
</tr>
<tr class="even">
<td><p>機</p></td>
<td><p><a href="../Page/臺北市市政大樓.md" title="wikilink">臺北市政府大樓</a></p></td>
<td><p>12</p></td>
<td><p>54.42</p></td>
<td><p>196,684.59</p></td>
<td><p>完工</p></td>
<td><p>1994年</p></td>
<td><p>市政</p></td>
<td><p>[2]</p></td>
</tr>
<tr class="odd">
<td><p>機</p></td>
<td><p><a href="../Page/臺北市議會.md" title="wikilink">臺北市議會</a></p></td>
<td><p>10</p></td>
<td><p>35</p></td>
<td><p>46,592.4</p></td>
<td><p>完工</p></td>
<td><p>1990年</p></td>
<td><p>市政</p></td>
<td><p>[3]</p></td>
</tr>
<tr class="even">
<td><p>A1</p></td>
<td><p><a href="../Page/遠雄金融中心.md" title="wikilink">遠雄金融中心</a></p></td>
<td><p>32</p></td>
<td><p>208</p></td>
<td><p>61,147.58</p></td>
<td><p>完工</p></td>
<td><p>2012年</p></td>
<td><p>辦公</p></td>
<td><p>[4]</p></td>
</tr>
<tr class="odd">
<td><p>A2</p></td>
<td><p><a href="../Page/統一國際大樓.md" title="wikilink">統一國際大樓</a><br />
（信義誠品）</p></td>
<td><p>30</p></td>
<td><p>154</p></td>
<td><p>116,773.91</p></td>
<td><p>完工</p></td>
<td><p>2004年</p></td>
<td><p>商場、辦公</p></td>
<td><p>[5]</p></td>
</tr>
<tr class="even">
<td><p>A3</p></td>
<td><p><a href="../Page/國泰置地廣場.md" title="wikilink">國泰置地廣場</a><br />
（微風信義）</p></td>
<td><p>46</p></td>
<td><p>212</p></td>
<td><p>152,488.6</p></td>
<td><p>完工</p></td>
<td><p>2015年</p></td>
<td><p>商場、辦公</p></td>
<td><p>[6]</p></td>
</tr>
<tr class="odd">
<td><p>A4</p></td>
<td><p><a href="../Page/新光三越百貨.md" title="wikilink">信義新天地</a></p></td>
<td><p>9</p></td>
<td><p>49</p></td>
<td><p>87,700</p></td>
<td><p>完工</p></td>
<td><p>2005年</p></td>
<td><p>商場</p></td>
<td><p>[7]</p></td>
</tr>
<tr class="even">
<td><p>A5</p></td>
<td><p><a href="../Page/寶麗廣塲.md" title="wikilink">寶麗廣塲</a></p></td>
<td><p>9</p></td>
<td><p>49.98</p></td>
<td><p>52,636.11</p></td>
<td><p>完工</p></td>
<td><p>2009年</p></td>
<td><p>商場</p></td>
<td><p>[8]</p></td>
</tr>
<tr class="odd">
<td><p>A6</p></td>
<td><p><a href="../Page/華新麗華大樓.md" title="wikilink">華新麗華大樓</a><br />
（花旗信義大樓）</p></td>
<td><p>27</p></td>
<td><p>134.6</p></td>
<td><p>77,824.15</p></td>
<td><p>完工</p></td>
<td><p>2009年</p></td>
<td><p>辦公</p></td>
<td><p>[9]</p></td>
</tr>
<tr class="even">
<td><p>A7</p></td>
<td><p><a href="../Page/Taipei_Sky_Tower.md" title="wikilink">Taipei Sky Tower</a><br />
(柏悅、安達仕酒店)</p></td>
<td><p>46</p></td>
<td><p>280</p></td>
<td><p>87,464.32</p></td>
<td><p>興建中</p></td>
<td><p>2021年</p></td>
<td><p>商場、旅館、演藝廳</p></td>
<td><p>[10]</p></td>
</tr>
<tr class="odd">
<td><p>A8</p></td>
<td><p><a href="../Page/新光三越百貨.md" title="wikilink">信義新天地</a></p></td>
<td><p>9</p></td>
<td><p>39.55</p></td>
<td><p>67,901.18</p></td>
<td><p>完工</p></td>
<td><p>2001年</p></td>
<td><p>商場</p></td>
<td><p>[11]</p></td>
</tr>
<tr class="even">
<td><p>A9</p></td>
<td><p><a href="../Page/新光三越百貨.md" title="wikilink">信義新天地</a></p></td>
<td><p>9</p></td>
<td><p>47.1</p></td>
<td><p>51,854.9</p></td>
<td><p>完工</p></td>
<td><p>2003年</p></td>
<td><p>商場</p></td>
<td><p>[12]</p></td>
</tr>
<tr class="odd">
<td><p>A10</p></td>
<td><p><a href="../Page/寒舍艾麗酒店.md" title="wikilink">寒舍艾麗酒店</a><br />
（微風松高）</p></td>
<td><p>22</p></td>
<td><p>89.95</p></td>
<td><p>54,956.28</p></td>
<td><p>完工</p></td>
<td><p>2013年</p></td>
<td><p>商場、旅館</p></td>
<td><p>[13]</p></td>
</tr>
<tr class="even">
<td><p>A11</p></td>
<td><p><a href="../Page/新光三越百貨.md" title="wikilink">信義新天地</a></p></td>
<td><p>7</p></td>
<td><p>40.7</p></td>
<td><p>62,705.81</p></td>
<td><p>完工</p></td>
<td><p>1997年</p></td>
<td><p>商場</p></td>
<td><p>[14]</p></td>
</tr>
<tr class="odd">
<td><p>A12</p></td>
<td><p><a href="../Page/新光信義金融大樓.md" title="wikilink">新光信義金融大樓</a><br />
（臺北寒舍艾美酒店）</p></td>
<td><p>22</p></td>
<td><p>89.95</p></td>
<td><p>79,689.79</p></td>
<td><p>完工</p></td>
<td><p>2010年</p></td>
<td><p>辦公、旅館</p></td>
<td><p>[15]</p></td>
</tr>
<tr class="even">
<td><p>A13</p></td>
<td><p><a href="../Page/遠東百貨.md" title="wikilink">City遠東百貨信義購物中心</a></p></td>
<td><p>14</p></td>
<td><p>78.95</p></td>
<td><p>78,159.78</p></td>
<td><p>興建中</p></td>
<td><p>2019年</p></td>
<td><p>商場</p></td>
<td><p>[16]</p></td>
</tr>
<tr class="odd">
<td><p>A14</p></td>
<td><p><a href="../Page/ATT_4_FUN.md" title="wikilink">ATT 4 FUN</a></p></td>
<td><p>10</p></td>
<td><p>42</p></td>
<td><p>33,065.65</p></td>
<td><p>完工</p></td>
<td><p>2000年</p></td>
<td><p>商場</p></td>
<td><p>[17]</p></td>
</tr>
<tr class="even">
<td><p>A15、18、20</p></td>
<td><p><a href="../Page/臺北南山廣場.md" title="wikilink">臺北南山廣場</a><br />
（微風南山）</p></td>
<td><p>48</p></td>
<td><p>274</p></td>
<td><p>192,154.99</p></td>
<td><p>完工</p></td>
<td><p>2018年</p></td>
<td><p>商場、辦公</p></td>
<td><p>[18]</p></td>
</tr>
<tr class="odd">
<td><p>A16</p></td>
<td><p><a href="../Page/威秀影城.md" title="wikilink">威秀影城</a></p></td>
<td><p>3</p></td>
<td><p>21.15</p></td>
<td><p>10,944.2</p></td>
<td><p>完工</p></td>
<td><p>1997年</p></td>
<td><p>商場</p></td>
<td><p>[19]</p></td>
</tr>
<tr class="even">
<td><p>A17</p></td>
<td><p><a href="../Page/威秀影城.md" title="wikilink">威秀影城</a></p></td>
<td><p>4</p></td>
<td><p>27.9</p></td>
<td><p>20,674.15</p></td>
<td><p>完工</p></td>
<td><p>1997年</p></td>
<td><p>商場</p></td>
<td><p>[20]</p></td>
</tr>
<tr class="odd">
<td><p>A19</p></td>
<td><p><a href="../Page/Neo_19.md" title="wikilink">Neo 19</a></p></td>
<td><p>12</p></td>
<td><p>43.9</p></td>
<td><p>24,380.69</p></td>
<td><p>完工</p></td>
<td><p>2001年</p></td>
<td><p>商場</p></td>
<td><p>[21]</p></td>
</tr>
<tr class="even">
<td><p>A21</p></td>
<td><p><a href="../Page/台北世界貿易中心.md" title="wikilink">台北世界貿易中心展覽三館</a>（都市更新）</p></td>
<td><p>3</p></td>
<td></td>
<td></td>
<td><p>完工</p></td>
<td><p>2003年</p></td>
<td><p>會展</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>A22、A23</p></td>
<td><p><a href="../Page/台北101.md" title="wikilink">臺北101</a></p></td>
<td><p>101</p></td>
<td><p>509.2</p></td>
<td><p>374,219.95</p></td>
<td><p>完工</p></td>
<td><p>2004年</p></td>
<td><p>商場、辦公</p></td>
<td><p>[22]</p></td>
</tr>
<tr class="even">
<td><p>A24</p></td>
<td><p><a href="../Page/台北世界貿易中心.md" title="wikilink">世貿中心展覽大樓</a></p></td>
<td><p>8</p></td>
<td><p>41</p></td>
<td><p>157,642.34</p></td>
<td><p>完工</p></td>
<td><p>1986年</p></td>
<td><p>會展</p></td>
<td><p>[23]</p></td>
</tr>
<tr class="odd">
<td><p>A24</p></td>
<td><p><a href="../Page/臺北世界貿易中心國際貿易大樓.md" title="wikilink">臺北世界貿易中心國際貿易大樓</a></p></td>
<td><p>34</p></td>
<td><p>142.92</p></td>
<td><p>111,791.52</p></td>
<td><p>完工</p></td>
<td><p>1988年</p></td>
<td><p>辦公</p></td>
<td><p>[24]</p></td>
</tr>
<tr class="even">
<td><p>A24</p></td>
<td><p><a href="../Page/臺北國際會議中心.md" title="wikilink">臺北國際會議中心</a></p></td>
<td><p>10</p></td>
<td><p>48.95</p></td>
<td><p>59,997.45</p></td>
<td><p>完工</p></td>
<td><p>1989年</p></td>
<td><p>會展</p></td>
<td><p>[25]</p></td>
</tr>
<tr class="odd">
<td><p>A24</p></td>
<td><p><a href="../Page/台北君悅酒店.md" title="wikilink">臺北君悅酒店</a></p></td>
<td><p>26</p></td>
<td><p>85.2</p></td>
<td><p>118,505.52</p></td>
<td><p>完工</p></td>
<td><p>1990年</p></td>
<td><p>旅館</p></td>
<td><p>[26]</p></td>
</tr>
<tr class="even">
<td><p>A25</p></td>
<td><p><a href="../Page/富邦A25.md" title="wikilink">富邦A25</a></p></td>
<td><p>52</p></td>
<td><p>265.5</p></td>
<td><p>132,362.85</p></td>
<td><p>興建中</p></td>
<td><p>2020年</p></td>
<td><p>辦公、美術館</p></td>
<td><p>[27][28]</p></td>
</tr>
<tr class="odd">
<td><p>A26</p></td>
<td><p><a href="../Page/信義區.md" title="wikilink">信義區公所</a>（都市更新）</p></td>
<td><p>46</p></td>
<td><p>234</p></td>
<td></td>
<td><p>準備中</p></td>
<td><p>1990年</p></td>
<td><p>市政</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>B1</p></td>
<td><p><a href="../Page/中油大樓.md" title="wikilink">中油大樓</a></p></td>
<td><p>23</p></td>
<td><p>90</p></td>
<td><p>102,920.4</p></td>
<td><p>完工</p></td>
<td><p>2001年</p></td>
<td><p>辦公</p></td>
<td><p>[29]</p></td>
</tr>
<tr class="odd">
<td><p>B2</p></td>
<td><p><a href="../Page/國泰金融中心大樓.md" title="wikilink">國泰金融中心大樓</a></p></td>
<td><p>24</p></td>
<td><p>106.15</p></td>
<td><p>89,902.76</p></td>
<td><p>完工</p></td>
<td><p>2002年</p></td>
<td><p>辦公</p></td>
<td><p>[30]</p></td>
</tr>
<tr class="even">
<td><p>B3</p></td>
<td><p><a href="../Page/宏泰交易廣場.md" title="wikilink">宏泰交易廣場</a></p></td>
<td><p>18</p></td>
<td><p>81.75</p></td>
<td><p>52,801.35</p></td>
<td><p>完工</p></td>
<td><p>1999年</p></td>
<td><p>辦公</p></td>
<td><p>[31]</p></td>
</tr>
<tr class="odd">
<td><p>B4</p></td>
<td><p><a href="../Page/群益金融大樓.md" title="wikilink">群益金融大樓</a></p></td>
<td><p>18</p></td>
<td><p>73.5</p></td>
<td><p>52,938.29</p></td>
<td><p>完工</p></td>
<td><p>1998年</p></td>
<td><p>辦公</p></td>
<td><p>[32]</p></td>
</tr>
<tr class="even">
<td><p>B5</p></td>
<td><p><a href="../Page/克緹大樓.md" title="wikilink">克緹大樓</a></p></td>
<td><p>15</p></td>
<td><p>89.5</p></td>
<td><p>28,823.85</p></td>
<td><p>完工</p></td>
<td><p>2009年</p></td>
<td><p>辦公</p></td>
<td><p>[33]</p></td>
</tr>
<tr class="odd">
<td><p>B6</p></td>
<td><p><a href="../Page/華南銀行總行世貿大樓.md" title="wikilink">華南銀行總行世貿大樓</a></p></td>
<td><p>27</p></td>
<td><p>154.5</p></td>
<td><p>52,131.74</p></td>
<td><p>完工</p></td>
<td><p>2014年</p></td>
<td><p>辦公</p></td>
<td><p>[34]</p></td>
</tr>
<tr class="even">
<td><p>B7</p></td>
<td><p>55Timeless 琢白</p></td>
<td><p>31</p></td>
<td><p>127.23</p></td>
<td><p>25,931.47</p></td>
<td><p>完工</p></td>
<td><p>2018年</p></td>
<td><p>住宅</p></td>
<td><p>[35]</p></td>
</tr>
<tr class="odd">
<td><p>D1</p></td>
<td><p>台北瑰麗酒店</p></td>
<td><p>37</p></td>
<td><p>144.9</p></td>
<td><p>40,329.37</p></td>
<td><p>重新設計</p></td>
<td><p>2024年</p></td>
<td><p>旅館、住宅</p></td>
<td><p>[36]</p></td>
</tr>
<tr class="even">
<td><p>D3</p></td>
<td><p>富創D3</p></td>
<td><p>40</p></td>
<td><p>150.2</p></td>
<td><p>35,220.29</p></td>
<td><p>環評中<br />
(土地法拍)</p></td>
<td></td>
<td><p>住宅</p></td>
<td><p>[37]</p></td>
</tr>
<tr class="odd">
<td><p>D4</p></td>
<td><p><a href="../Page/信義房屋.md" title="wikilink">信義房屋</a>(信義企業集團)</p></td>
<td><p>10</p></td>
<td><p>48.2</p></td>
<td><p>22,771.09</p></td>
<td><p>完工</p></td>
<td><p>1995年</p></td>
<td><p>辦公</p></td>
<td><p>[38]</p></td>
</tr>
<tr class="even">
<td><p>D5</p></td>
<td><p><a href="../Page/國泰信義經貿大樓.md" title="wikilink">國泰信義經貿大樓</a></p></td>
<td><p>12</p></td>
<td><p>49.95</p></td>
<td><p>47,088.36</p></td>
<td><p>完工</p></td>
<td><p>2003年</p></td>
<td><p>辦公</p></td>
<td><p>[39]</p></td>
</tr>
<tr class="odd">
<td><p>E1</p></td>
<td><p>震旦國際大樓</p></td>
<td><p>18</p></td>
<td><p>72.75</p></td>
<td><p>33,747.71</p></td>
<td><p>完工</p></td>
<td><p>1991年</p></td>
<td><p>辦公</p></td>
<td><p>[40]</p></td>
</tr>
<tr class="even">
<td><p>E2</p></td>
<td><p>新光曼哈頓世貿大樓</p></td>
<td><p>16</p></td>
<td><p>66.25</p></td>
<td><p>23,208.43</p></td>
<td><p>完工</p></td>
<td><p>2000年</p></td>
<td><p>辦公</p></td>
<td><p>[41]</p></td>
</tr>
<tr class="odd">
<td><p>E4</p></td>
<td><p>南山金融中心</p></td>
<td><p>17</p></td>
<td><p>69.95</p></td>
<td><p>27,559.93</p></td>
<td><p>完工</p></td>
<td><p>2006年</p></td>
<td><p>辦公</p></td>
<td><p>[42]</p></td>
</tr>
<tr class="even">
<td><p>H</p></td>
<td><p><a href="../Page/陶朱隱園.md" title="wikilink">陶朱隱園</a></p></td>
<td><p>21</p></td>
<td><p>93.2</p></td>
<td><p>42,705.61</p></td>
<td><p>完工</p></td>
<td><p>2018年</p></td>
<td><p>住宅</p></td>
<td><p>[43]</p></td>
</tr>
</tbody>
</table>

## 聯外交通

### 鐵路運輸

  - [捷運系統](../Page/臺北捷運.md "wikilink")
      - [The_seal_of_Department_of_Rapid_Transit_Systems,_Taipei_City_Government_20140108.svg](https://zh.wikipedia.org/wiki/File:The_seal_of_Department_of_Rapid_Transit_Systems,_Taipei_City_Government_20140108.svg "fig:The_seal_of_Department_of_Rapid_Transit_Systems,_Taipei_City_Government_20140108.svg")[臺北捷運](../Page/臺北捷運.md "wikilink")
          - ：[臺北101/世貿站](../Page/臺北101/世貿站.md "wikilink")、[象山站](../Page/象山站.md "wikilink")

          - ：[松山站](../Page/松山車站_\(台灣\).md "wikilink")

          - ：[市政府站](../Page/市政府站_\(臺北市\).md "wikilink")、[永春站](../Page/永春站_\(台北市\).md "wikilink")

          - ：[<font color="#888888">東環段（臺北市東側南北向捷運系統）](../Page/臺北市東側南北向捷運系統.md "wikilink")（可行性評估中）
  - [ROC_Taiwan_Railways_Administration_Logo.svg](https://zh.wikipedia.org/wiki/File:ROC_Taiwan_Railways_Administration_Logo.svg "fig:ROC_Taiwan_Railways_Administration_Logo.svg")[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[縱貫線](../Page/縱貫線_\(北段\).md "wikilink")：[松山車站](../Page/松山車站_\(台灣\).md "wikilink")

### 公路運輸

  - [快速道路系統](../Page/台北交通.md "wikilink")
      - [基隆路車行地下道](../Page/基隆路.md "wikilink")
      - [信義快速道路](../Page/信義快速道路.md "wikilink")
      - [市民大道高架道路](../Page/市民大道.md "wikilink")
      - [環東大道](../Page/環東大道.md "wikilink")
  - [主要聯外道路](../Page/臺北市主要道路列表.md "wikilink")
      - [忠孝東路](../Page/忠孝東路.md "wikilink")（[台5線](../Page/台5線.md "wikilink")）
      - [基隆路](../Page/基隆路.md "wikilink")
      - [信義路](../Page/信義路_\(台北市\).md "wikilink")
  - [長途客運轉乘系統](../Page/市府轉運站.md "wikilink")

## 相關條目

  - [都市計畫](../Page/都市計畫.md "wikilink")、[都市更新](../Page/都市更新.md "wikilink")
      - [七期重劃區](../Page/七期重劃區.md "wikilink")
      - [西區門戶計畫區](../Page/西區門戶計畫區.md "wikilink")
      - [東區門戶計畫區](../Page/東區門戶計畫區.md "wikilink")
      - [水湳經貿園區](../Page/水湳經貿園區.md "wikilink")
      - [亞洲新灣區](../Page/亞洲新灣區.md "wikilink")
      - [高雄多功能經貿園區](../Page/高雄多功能經貿園區.md "wikilink")
  - [台北東區](../Page/台北東區.md "wikilink")、[信義區](../Page/信義區_\(臺北市\).md "wikilink")、[三張犁](../Page/三張犁.md "wikilink")
  - [信義商圈空橋系統](../Page/信義商圈空橋系統.md "wikilink")
  - [都市發展局](../Page/都市發展局.md "wikilink")
  - [臺北市政府都市發展局](../Page/臺北市政府都市發展局.md "wikilink")
  - [高玉樹市府](../Page/高玉樹市府.md "wikilink")

## 註解

## 參考文獻

  - [信義計畫區細部計畫](http://mail.tku.edu.tw/094152/xy2.htm)

## 外部連結

  - [臺北市政府](https://www.gov.taipei/)
  - [信義商圈
    臺北旅遊網](https://www.travel.taipei/zh-tw/attraction/details/1573)
  - [信義區社區規劃服務中心總結成果報告書](http://www.hsiliu.org.tw/green3/index.htm)

{{-}}

[信義計畫區](../Category/信義計畫區.md "wikilink") [Category:信義區
(臺北市)](../Category/信義區_\(臺北市\).md "wikilink")
[Category:郭茂林作品](../Category/郭茂林作品.md "wikilink")
[Category:臺灣經濟政策](../Category/臺灣經濟政策.md "wikilink")
[Category:台灣經濟](../Category/台灣經濟.md "wikilink")
[Category:台灣都市計畫](../Category/台灣都市計畫.md "wikilink")
[Category:台灣都市更新](../Category/台灣都市更新.md "wikilink")
[Category:台北都市計畫](../Category/台北都市計畫.md "wikilink")
[Category:台北市經濟](../Category/台北市經濟.md "wikilink")

1.  99使字第0010號
2.  83使字第0112號
3.  79使字第0470號
4.  101使字第0345號
5.  93使字第0373號
6.  104使字第0094號
7.  101使字第0238號
8.  98使字第0081號
9.  98使字第0108號
10.
11. 90使字第0397號
12. 92使字第0377號
13. 102使字第0155號
14. 86使字第0392號
15. 99使字第0255號
16. 105建字第0129號
17. 89使字第097號
18. 103建字第6688號、103建字第6699號
19. 86使字第447號
20. 86使字第468號
21. 90使字第287號
22. 92使字第0168號、93使字第0353號
23. 74使字第1265號
24. 77使字第0158號、
25. 78使字第0714號
26. 79使字第0052號
27. 106建字第0237號
28.
29. 90使字第090號
30. 91使字第0265號
31. 88使字第0022號
32. 87使字第0484號
33. 98使字第0288號
34. 103使字第0318號
35.
36.
37.
38. 84使字第0388號
39. 92使字第0197號
40. 80使字第0443號
41. 89使字第0324號
42. 95使字第0122號
43. 107使字第6668號