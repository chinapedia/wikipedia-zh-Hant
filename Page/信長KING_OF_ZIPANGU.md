《**信長 KING OF
ZIPANGU**》（****，****），是[NHK在](../Page/NHK.md "wikilink")1992年1月5日—12月13日製播放送的第30部[大河劇](../Page/NHK大河劇.md "wikilink")。

本劇是以[葡萄牙傳教士](../Page/葡萄牙.md "wikilink")[路易斯·弗洛伊斯的角度來闡述織田信長的一生](../Page/路易斯·弗洛伊斯.md "wikilink")，劇中以一個帶有洋腔的外國人用日文來當本劇的旁白，並於每一集終結時說一句
"Ate breve\!
Obrigado\!"（[葡萄牙語](../Page/葡萄牙語.md "wikilink")，約為「下回再見！謝謝！」）。在大河劇歷史上，可算是相當特別。

本劇編劇田向正健之前曾編過「武田信玄」一劇，在當時創下很高的收視率；而本劇的主角演員如緒形直人、菊池桃子等等都是當時偶像級的演員。儘管本劇起用了高收視編劇與人氣偶像，而且劇本較傾向史實而非一般流行的俗說（像是平手政秀切腹一節，並不採通行的死諫說，而是採因過失被命自殺；而扮演信長的緒形直人也是長得較像流傳畫像的信長，也不如一般小說描寫的信長有英氣），以及戲劇中畫面陰暗、描寫虛構術士「隨天」裝神弄鬼部份甚多。因此本劇在一般評價部份，被大打折扣。

台灣於1999年引進本劇，由[緯來日本台播出](../Page/緯來日本台.md "wikilink")，在當時片名被改為《**織田信長**》。

## 製作人員

  - 原作，[田向正健](../Page/田向正健.md "wikilink")
  - 劇本，田向正健
  - 音樂，[毛利藏人](../Page/毛利藏人.md "wikilink")
  - 導演，重光亨彥・小松隆・柴田岳志・加賀田透・岡田健・吉川邦夫
  - 監修，松田穀一
  - 殺陣・武術指導，林邦史朗
  - 振付・所作指導，猿若清三郎
  - 記錄，阿部格・石原美雪
  - 旁白，藍休・克-{裡}-斯多夫
  - 紀行旁白，[加賀美幸子](../Page/加賀美幸子.md "wikilink")

## 出演

### 織田家

  - [織田信長](../Page/織田信長.md "wikilink")：[森田洸輔](../Page/森田洸輔.md "wikilink")→[山根隆明](../Page/山根隆明.md "wikilink")→[緒形直人](../Page/緒形直人.md "wikilink")
  - [歸蝶](../Page/齋藤歸蝶.md "wikilink")：[榎本夕希](../Page/榎本夕希.md "wikilink")→[菊池桃子](../Page/菊池桃子.md "wikilink")
  - [吉乃](../Page/生駒吉乃.md "wikilink")：[高木美保](../Page/高木美保.md "wikilink")
  - [鍋](../Page/興雲院.md "wikilink")：[若村麻由美](../Page/若村麻由美.md "wikilink")
  - [織田信忠](../Page/織田信忠.md "wikilink")：[類家大地](../Page/類家大地.md "wikilink")→[寺澤昌純](../Page/寺澤昌純.md "wikilink")→[青木海](../Page/青木海.md "wikilink")→[藤田哲也](../Page/藤田哲也.md "wikilink")→[東根作壽英](../Page/東根作壽英.md "wikilink")
  - [織田信雄](../Page/織田信雄.md "wikilink")：[田原慎太郎](../Page/田原慎太郎.md "wikilink")→[大泉翼](../Page/大泉翼.md "wikilink")
  - [五德](../Page/織田五德.md "wikilink")：[三海菜穗美](../Page/三海菜穗美.md "wikilink")→[井上亞子](../Page/井上亞子.md "wikilink")→[西村裕子](../Page/西村裕子.md "wikilink")→[成田惠](../Page/成田惠.md "wikilink")
  - [織田信定](../Page/織田信定.md "wikilink")：[船越英二](../Page/船越英二.md "wikilink")
  - [織田信秀](../Page/織田信秀.md "wikilink")：[林隆三](../Page/林隆三.md "wikilink")
  - [琉衣](../Page/土田御前.md "wikilink")：[高橋惠子](../Page/高橋惠子.md "wikilink")
  - [織田信康](../Page/織田信康.md "wikilink")：[加世幸市](../Page/加世幸市.md "wikilink")
  - [織田信廣](../Page/織田信廣.md "wikilink")：[村田泰則](../Page/村田泰則.md "wikilink")→[岸本一人](../Page/岸本一人.md "wikilink")
  - [織田信行](../Page/織田信行.md "wikilink")：[內山真人](../Page/內山真人.md "wikilink")→[大友大輔](../Page/大友大輔.md "wikilink")→[保阪尚輝](../Page/保阪尚輝.md "wikilink")
  - [清](../Page/善應院.md "wikilink")：[吉川十和子](../Page/吉川十和子.md "wikilink")
  - [織田喜六郎](../Page/織田秀孝.md "wikilink")：[谷田真吾](../Page/谷田真吾.md "wikilink")
  - [織田信賢](../Page/織田信賢.md "wikilink")：[沼崎悠](../Page/沼崎悠.md "wikilink")
  - [織田信治](../Page/織田信治.md "wikilink")：[神田雄治](../Page/神田雄治.md "wikilink")
  - [市](../Page/織田市.md "wikilink")：[中野美穗](../Page/中野美穗.md "wikilink")→[鷲尾依沙子](../Page/鷲尾依沙子.md "wikilink")
  - [織田信光](../Page/織田信光.md "wikilink")：[長谷川明男](../Page/長谷川明男.md "wikilink")
  - [織田信時](../Page/織田信時.md "wikilink")：[田中克季](../Page/田中克季.md "wikilink")

### 信長的家臣與相關人士

  - [羽柴秀吉](../Page/羽柴秀吉.md "wikilink")：[仲村亨](../Page/仲村亨.md "wikilink")
  - [寧寧](../Page/高台院.md "wikilink")：[中山美穗](../Page/中山美穗.md "wikilink")
  - [明智光秀](../Page/明智光秀.md "wikilink")：[麥克富岡](../Page/麥克富岡.md "wikilink")
  - [明智秀滿](../Page/明智秀滿.md "wikilink")：[小野了](../Page/小野了.md "wikilink")
  - [德川家康](../Page/德川家康.md "wikilink")：[鄉廣美](../Page/鄉廣美.md "wikilink")
  - [築山殿](../Page/築山殿.md "wikilink")：[島村佳江](../Page/島村佳江.md "wikilink")
  - [松平信康](../Page/松平信康.md "wikilink")：[金子勝](../Page/金子勝.md "wikilink")→[早川亮](../Page/早川亮.md "wikilink")
  - [平手政秀](../Page/平手政秀.md "wikilink")：[二谷英明](../Page/二谷英明.md "wikilink")
  - [平手五郎右衛門](../Page/平手久秀.md "wikilink")：[黑田亞瑟](../Page/黑田亞瑟.md "wikilink")
  - [林通勝](../Page/林通勝.md "wikilink")：[宇津井健](../Page/宇津井健.md "wikilink")
  - [內藤勝介](../Page/內藤勝介.md "wikilink")：[塚本信夫](../Page/塚本信夫.md "wikilink")
  - [佐久間盛重](../Page/佐久間盛重.md "wikilink")：[本鄉功次郎](../Page/本鄉功次郎.md "wikilink")
  - [佐久間信盛](../Page/佐久間信盛.md "wikilink")：[田中健](../Page/田中健.md "wikilink")
  - [柴田勝家](../Page/柴田勝家.md "wikilink")：[瀧田榮](../Page/瀧田榮.md "wikilink")
  - [丹羽長秀](../Page/丹羽長秀.md "wikilink")：[杉本哲太](../Page/杉本哲太.md "wikilink")
  - [瀧川一益](../Page/瀧川一益.md "wikilink")：[柴俊夫](../Page/柴俊夫.md "wikilink")
  - [池田恆興](../Page/池田恆興.md "wikilink")：[阪本德志](../Page/阪本德志.md "wikilink")→[的場浩司](../Page/的場浩司.md "wikilink")
  - [齋藤利三](../Page/齋藤利三.md "wikilink")：[淵野俊太](../Page/淵野俊太.md "wikilink")
  - [前田利家](../Page/前田利家.md "wikilink")：[橋爪淳](../Page/橋爪淳.md "wikilink")
  - [河尻秀隆](../Page/河尻秀隆.md "wikilink")：[森田順平](../Page/森田順平.md "wikilink")
  - [森可成](../Page/森可成.md "wikilink")：[三上真一郎](../Page/三上真一郎.md "wikilink")
  - [森長可](../Page/森長可.md "wikilink")：[野尻忠正](../Page/野尻忠正.md "wikilink")
  - [森蘭丸](../Page/森蘭丸.md "wikilink")：[石野太呂字](../Page/龍小太郎.md "wikilink")
  - [森坊丸](../Page/森坊丸.md "wikilink")：[蘆田昌太郎](../Page/蘆田昌太郎.md "wikilink")
  - [細川藤孝](../Page/細川藤孝.md "wikilink")：[勝野洋](../Page/勝野洋.md "wikilink")
  - [細川忠興](../Page/細川忠興.md "wikilink")：[小林秀樹](../Page/小林秀樹.md "wikilink")
  - [玉](../Page/明智玉子.md "wikilink")：[今村惠子](../Page/今村惠子.md "wikilink")
  - [毛利新助](../Page/毛利新助.md "wikilink")：[青木健](../Page/青木健.md "wikilink")
  - [服部小平太](../Page/服部一忠.md "wikilink")：[堅田宏](../Page/堅田宏.md "wikilink")
  - [高山右近](../Page/高山右近.md "wikilink")：[富家規政](../Page/富家規政.md "wikilink")
  - [不破光治](../Page/不破光治.md "wikilink")：[石山律雄](../Page/石山律雄.md "wikilink")
  - [荒木村重](../Page/荒木村重.md "wikilink")：[本田博太郎](../Page/本田博太郎.md "wikilink")
  - [村井貞勝](../Page/村井貞勝.md "wikilink")：[西田圭](../Page/西田圭.md "wikilink")
  - [水野信元](../Page/水野信元.md "wikilink")：[大林丈史](../Page/大林丈史.md "wikilink")
  - [酒井正親](../Page/酒井正親.md "wikilink")：[大和田伸也](../Page/大和田伸也.md "wikilink")
  - [酒井忠次](../Page/酒井忠次.md "wikilink")：[林邦應](../Page/林邦應.md "wikilink")
  - [石川家成](../Page/石川家成.md "wikilink")：[長棟嘉道](../Page/長棟嘉道.md "wikilink")
  - [平岩親吉](../Page/平岩親吉.md "wikilink")：[小野進也](../Page/小野進也.md "wikilink")
  - [本多重次](../Page/本多重次.md "wikilink")：[高品剛](../Page/高品剛.md "wikilink")
  - [阪井大膳](../Page/阪井大膳.md "wikilink")：[鈴木瑞穗](../Page/鈴木瑞穗.md "wikilink")

### 諸大名・武將

  - [足利義輝](../Page/足利義輝.md "wikilink")：[宮田恭男](../Page/宮田恭男.md "wikilink")
  - [足利義昭](../Page/足利義昭.md "wikilink")：[青山裕一](../Page/青山裕一.md "wikilink")
  - [斯波義統](../Page/斯波義統.md "wikilink")：[山本寛](../Page/山本寛.md "wikilink")
  - [斯波義銀](../Page/斯波義銀.md "wikilink")：[山本耕史](../Page/山本耕史.md "wikilink")
  - [和田惟政](../Page/和田惟政.md "wikilink")：[新井康弘](../Page/新井康弘.md "wikilink")
  - [今川義元](../Page/今川義元.md "wikilink")：[柴田侊彥](../Page/柴田侊彥.md "wikilink")
  - [朝比奈泰能](../Page/朝比奈泰能.md "wikilink")：[俵一](../Page/俵一.md "wikilink")
  - [齋藤道三](../Page/齋藤道三.md "wikilink")：[蘆田伸介](../Page/蘆田伸介.md "wikilink")
  - [齋藤義龍](../Page/齋藤義龍.md "wikilink")：[廣岡瞬](../Page/廣岡瞬.md "wikilink")
  - [齋藤孫四郎](../Page/齋藤孫四郎.md "wikilink")：[永野典勝](../Page/永野典勝.md "wikilink")
  - [齋藤喜平次](../Page/齋藤喜平次.md "wikilink")：[中村久光](../Page/中村久光.md "wikilink")
  - [齋藤龍興](../Page/齋藤龍興.md "wikilink")：[渡浩行](../Page/渡浩行.md "wikilink")
  - [稻葉良通](../Page/稻葉一鉄.md "wikilink")：[篠田三郎](../Page/篠田三郎.md "wikilink")
  - [武田勝賴](../Page/武田勝賴.md "wikilink")：[北谷等](../Page/北谷等.md "wikilink")→[黑田隆哉](../Page/黑田隆哉.md "wikilink")
  - [雪姬](../Page/遠山夫人.md "wikilink")：[品川景子](../Page/品川景子.md "wikilink")
  - [北條夫人](../Page/桂林院.md "wikilink")：[阿部朋子](../Page/阿部朋子.md "wikilink")
  - [武田信勝](../Page/武田信勝.md "wikilink")：[小橋賢兒](../Page/小橋賢兒.md "wikilink")
  - [馬場美濃守](../Page/馬場信春.md "wikilink")：[荒木茂](../Page/荒木茂.md "wikilink")
  - [山縣昌景](../Page/山縣昌景.md "wikilink")：[小山武宏](../Page/小山武宏.md "wikilink")
  - [朝倉義景](../Page/朝倉義景.md "wikilink")：[北村總一朗](../Page/北村總一朗.md "wikilink")
  - [朝倉景鏡](../Page/朝倉景鏡.md "wikilink")：[田島基吉](../Page/田島基吉.md "wikilink")
  - [朝倉景健](../Page/朝倉景健.md "wikilink")：[門田俊一](../Page/門田俊一.md "wikilink")
  - [六角承禎](../Page/六角義賢.md "wikilink")：[平泉成](../Page/平泉成.md "wikilink")
  - [六角義治](../Page/六角義治.md "wikilink")：[山口晃史](../Page/山口晃史.md "wikilink")
  - [淺井久政](../Page/淺井久政.md "wikilink")：[寺田農](../Page/寺田農.md "wikilink")
  - [淺井長政](../Page/淺井長政.md "wikilink")：[辰巳琢郎](../Page/辰巳琢郎.md "wikilink")
  - [茶茶](../Page/淀殿.md "wikilink")：[田原加奈子](../Page/田原加奈子.md "wikilink")
  - [阿江](../Page/崇源院.md "wikilink")：[津川裡奈](../Page/津川裡奈.md "wikilink")
  - [阿閉貞秀](../Page/阿閉貞秀.md "wikilink")：[佐和隆史](../Page/佐和隆史.md "wikilink")
  - [三好長慶](../Page/三好長慶.md "wikilink")：[中丸新將](../Page/中丸新將.md "wikilink")
  - [松永久秀](../Page/松永久秀.md "wikilink")：[清水紘治](../Page/清水紘治.md "wikilink")
  - [小西行長](../Page/小西行長.md "wikilink")：[伊藤秀](../Page/伊藤秀.md "wikilink")

### 其他

  - [今井宗久](../Page/今井宗久.md "wikilink")：[佐藤慶](../Page/佐藤慶.md "wikilink")
  - [千利休](../Page/千利休.md "wikilink")：[伊藤孝雄](../Page/伊藤孝雄.md "wikilink")
  - [津田宗及](../Page/津田宗及.md "wikilink")：[內田稔](../Page/內田稔.md "wikilink")
  - [顯如](../Page/本願寺顯如.md "wikilink")：[伊藤富美也](../Page/伊藤富美也.md "wikilink")
  - [教如](../Page/本願寺教如.md "wikilink")：[原豐](../Page/原豐.md "wikilink")
  - [朝山日乘](../Page/朝山日乘.md "wikilink")：[內田勝正](../Page/內田勝正.md "wikilink")
  - [伊東滿所](../Page/伊東滿所.md "wikilink")：[重富孝](../Page/重富孝.md "wikilink")→[木村直雄樹](../Page/木村直雄樹.md "wikilink")
  - [千千石米格爾](../Page/千千石米格爾.md "wikilink")：[佐藤洋匡](../Page/佐藤洋匡.md "wikilink")→[長崎真純](../Page/長崎真純.md "wikilink")
  - [原馬爾提諾](../Page/原馬爾提諾.md "wikilink")：[柴田宗典](../Page/柴田宗典.md "wikilink")→[植松良介](../Page/植松良介.md "wikilink")
  - [中浦朱裡安](../Page/中浦朱裡安.md "wikilink")：[光行透](../Page/光行透.md "wikilink")
  - [額我略十三世](../Page/額我略十三世.md "wikilink")：-{[安東尼奧・坎特里尼](../Page/安東尼奧・坎特里尼.md "wikilink")}-
  - [沙勿略神父](../Page/方濟各‧沙勿略.md "wikilink")：[泰利・歐布萊恩](../Page/泰利・歐布萊恩.md "wikilink")
  - \-{[亞歷山大・范禮安](../Page/亞歷山大・范禮安.md "wikilink")}-：[布萊恩・布克蓋夫尼](../Page/布萊恩・布克蓋夫尼.md "wikilink")
  - [路易斯‧弗洛伊斯](../Page/路易斯‧弗洛伊斯.md "wikilink")：[亞當・羅賓斯](../Page/亞當・羅賓斯.md "wikilink")→[法蘭克・尼爾](../Page/法蘭克・尼爾.md "wikilink")
  - [奧岡提諾](../Page/格內奇・蘇爾第・奧岡提諾.md "wikilink")：[細川法蘭克](../Page/細川法蘭克.md "wikilink")
  - [雅吉洛](../Page/雅吉洛.md "wikilink")：[龍飛雲](../Page/龍飛雲.md "wikilink")
  - [羅連素](../Page/羅連素.md "wikilink")：[稻川淳二](../Page/稻川淳二.md "wikilink")

### 架空人物

  - \-{加}-納隨天：[平幹二朗](../Page/平幹二朗.md "wikilink")
  - 琉：[黑谷加奈子](../Page/黑谷加奈子.md "wikilink")→[西野麻裡](../Page/西野麻裡.md "wikilink")→[久我美智子](../Page/久我美智子.md "wikilink")→[岡本舞](../Page/岡本舞.md "wikilink")
  - 梶真八郎：[狹間鐵](../Page/狹間鐵.md "wikilink")
  - 松：-{[山本郁子](../Page/山本郁子.md "wikilink")}-
  - 安：[青山知可子](../Page/青山知可子.md "wikilink")
  - 佳奈江：[岩本多代](../Page/岩本多代.md "wikilink")
  - 峰：[山本則子](../Page/山本則子.md "wikilink")

## 各集標題

### 本篇

1.  日本國
2.  父親之死
3.  抗爭的開始
4.  切腹
5.  蝮蛇道三
6.  往大名之路邁進的第一步
7.  以牙還牙
8.  鬼的住所
9.  道三敗死
10. 骨肉之爭
11. 弟弟
12. 尾張統一
13. 桶狹間之戰（前）
14. 桶狹間之戰（後）
15. 家庭的問題
16. 神的武士們
17. 妖怪聖誕節
18. 和平同盟
19. 信長北上
20. 基督徒佛洛伊斯
21. 將軍暗殺
22. 美濃攻略
23. 往京都之道
24. 天下布武
25. 野望
26. 見到信長
27. 對決
28. 人的怨恨
29. 姉川合戰
30. 死神
31. 火燒比叡山
32. 隨天
33. 信長包圍作戰
34. 四面楚歌
35. 足利幕府滅亡
36. 進攻淺井朝倉
37. 取得天下
38. 長篠之戰
39. 家督讓位
40. 夢之城
41. 基督徒決死行
42. 惡劣的謠言
43. 家康的悲劇
44. \-{余}-就是神
45. 地球是圓的
46. 安土山神學校
47. 全國平定作戰
48. 日本國之王
49. 本能寺之變

### 總集篇

1.  日本國
2.  天下布武
3.  本能寺之變

[Category:1992年日本電視劇集](../Category/1992年日本電視劇集.md "wikilink")
[Category:日本小說改編電視劇](../Category/日本小說改編電視劇.md "wikilink")
[Category:日語電視劇](../Category/日語電視劇.md "wikilink")
[Category:織田信長題材作品](../Category/織田信長題材作品.md "wikilink")
[Category:緯來電視外購日劇](../Category/緯來電視外購日劇.md "wikilink")
[Category:兄弟謀殺相關作品](../Category/兄弟謀殺相關作品.md "wikilink")