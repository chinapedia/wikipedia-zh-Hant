**修身堂控股有限公司**（）於2000年由前香港藝人[張玉珊創辦](../Page/張玉珊.md "wikilink")，從事[保健及](../Page/保健.md "wikilink")[美容等業務](../Page/美容.md "wikilink")，包括：銷售保健及美容產品，及提供多類健康及美容服務。

修身堂開辦的纖體中心主要提供[瘦身服務](../Page/瘦身.md "wikilink")，包括全身及局部纖體、控制體重，以及全身護理服務和面部護理服務等。公司亦有開設[瑜伽中心](../Page/瑜伽.md "wikilink")。現時在香港有4家美容修身中心，在[中國的北京](../Page/中國.md "wikilink")(1家)、[上海](../Page/上海.md "wikilink")（2家）及[深圳設有分店](../Page/杭州.md "wikilink")。

修身堂控股有限公司是在[香港創業板上市](../Page/香港創業板.md "wikilink")，於2016年1月,該公司市值大約為2.5億元。

2017年5月15日 - 獨立股評人David Webb「50隻不能買的港股」名單之一。

## 減肥藥事件

2008年1月10日，缳生署人員早前於「修身堂健康纖體服務中心」中環分店內的張英傑醫生診所中搜出大量深綠色、名為「Apisate」的藥丸膠囊。期後化驗結果顯示該減肥膠囊含氟苯丙胺。使用[氟苯丙胺可引致嚴重的副作用如心瓣疾病](../Page/氟苯丙胺.md "wikilink")，本港由一九九八年起已撤銷含該種成份的藥物註冊。\[1\]

2008年6月12日，一名37歲女子在中環修身堂分店獲醫生署方「Soloslim」減肥產品，服食逾一年後，近月出現精神異常，懷疑中毒，上月15日入住東區醫院，留醫個多星期後出院。化驗證實，該產品含沒有標明的西藥成分[西布曲明](../Page/西布曲明.md "wikilink")，衛生署呼籲市民停止服用。\[2\]

## 代言人

修身堂常以知名藝人作為其產品及服務的[代言人](../Page/代言人.md "wikilink")，現時的代言人為曾於1年內減去逾100磅的[鄭欣宜及](../Page/鄭欣宜.md "wikilink")[那英](../Page/那英.md "wikilink")。歷年來的代言人還包括[林穎嫺](../Page/林穎嫺.md "wikilink")、[楊小娟](../Page/楊小娟.md "wikilink")、[余安安](../Page/余安安.md "wikilink")、[薛家燕](../Page/薛家燕.md "wikilink")、[張燊悅](../Page/張燊悅.md "wikilink")、[曹敏莉](../Page/曹敏莉.md "wikilink")、[何超儀](../Page/何超儀.md "wikilink")、[蒙嘉慧](../Page/蒙嘉慧.md "wikilink")、[袁潔瑩](../Page/袁潔瑩.md "wikilink")、[朱茵](../Page/朱茵.md "wikilink")、[章小蕙](../Page/章小蕙.md "wikilink")、[莊泳](../Page/莊泳.md "wikilink")、[恬妞](../Page/恬妞.md "wikilink")、[陳嘉容](../Page/陳嘉容.md "wikilink")、[林熙蕾](../Page/林熙蕾.md "wikilink")、[Danielle](../Page/Danielle.md "wikilink")、[Twins](../Page/Twins.md "wikilink")、[陳法拉](../Page/陳法拉.md "wikilink")、[邵珮詩](../Page/邵珮詩.md "wikilink")、[李蘢怡及唯一男士](../Page/李蘢怡.md "wikilink")[許志安](../Page/許志安.md "wikilink")。

## 注釋

<references />

## 外部链接

  - [修身堂官方網站](http://www.sausantong.com/)

[Category:香港交易所上市公司](../Category/香港交易所上市公司.md "wikilink")
[Category:香港名牌](../Category/香港名牌.md "wikilink")
[Category:美容](../Category/美容.md "wikilink")

1.  [減肥藥被發現含有未註冊藥物](http://www.info.gov.hk/gia/general/200801/10/P200801100238.htm)
2.  [修身堂減肥藥又出事](http://news.sina.com.hk/cgi-bin/nw/show.cgi/2/1/1/769198/1.html)