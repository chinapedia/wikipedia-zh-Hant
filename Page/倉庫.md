[CEDIS_Soriana.jpg](https://zh.wikipedia.org/wiki/File:CEDIS_Soriana.jpg "fig:CEDIS_Soriana.jpg")搬运物品的工人\]\]
**倉庫**（）又名**貨倉**，是一些用作儲存[貨物的](../Page/貨物.md "wikilink")[建築物](../Page/建築物.md "wikilink")。是一种[服務於](../Page/服務.md "wikilink")[生產商](../Page/工廠.md "wikilink")、[商品](../Page/商品.md "wikilink")[供應商](../Page/供應商.md "wikilink")、[物流](../Page/物流.md "wikilink")[組織的建筑](../Page/组织_\(社会学\).md "wikilink")。為方便合作，倉庫通常鄰近[碼頭](../Page/碼頭.md "wikilink")、[火車站](../Page/火車站.md "wikilink")、[飛機場等](../Page/飛機場.md "wikilink")。

倉庫最先被发现在古罗马的Ostia社区，中国隋唐两代政府，在洛阳修建的国家[粮仓](../Page/粮仓.md "wikilink")——[回洛仓](../Page/回洛仓.md "wikilink")、[含嘉仓由于规模巨大形成了独立的](../Page/含嘉仓.md "wikilink")[城池形态](../Page/城池.md "wikilink")。

## 图片

<File:Warehouses> in Chai Wan, Hong
Kong.jpg|thumb|250px|位於[香港](../Page/香港.md "wikilink")[柴灣公眾貨物裝卸區的柴灣货仓](../Page/柴灣.md "wikilink")
[File:GlosDocks.jpg|19世纪在](File:GlosDocks.jpg%7C19世纪在)[英国](../Page/英国.md "wikilink")[格洛斯特的码头](../Page/格洛斯特.md "wikilink"),
原用于存储进口玉米 <File:Horgen> - Sust-Ortsmuseum - Zürichsee IMG 3829.JPG|
一座中世纪的仓库, [豪尔根](../Page/豪尔根.md "wikilink"),
[瑞士](../Page/瑞士.md "wikilink") <File:Atlantic> Dock, Brooklyn,
ca. 1872-1887.
(5833485842).jpg|19世纪大西洋[布鲁克林码头曾经的仓库](../Page/布鲁克林.md "wikilink")
[File:OstiaWarehouses.JPG|古罗马城市](File:OstiaWarehouses.JPG%7C古罗马城市)[Ostia被毁坏的仓库](../Page/Ostia_\(Rome\).md "wikilink")
<File:India> House 5.JPG|英国[曼彻斯特的仓库](../Page/曼彻斯特.md "wikilink")

## 相關

  - [迷你自存倉](../Page/迷你自存倉.md "wikilink")
  - [冷凍倉](../Page/冷凍倉.md "wikilink")
  - [危險倉](../Page/危險倉.md "wikilink")
  - [起重機](../Page/起重機.md "wikilink")
  - [叉車](../Page/叉車.md "wikilink")
  - [海關](../Page/海關.md "wikilink")
  - [空運](../Page/空運.md "wikilink")
  - [船舶](../Page/船舶.md "wikilink")
  - [貨櫃碼頭](../Page/貨櫃碼頭.md "wikilink")
  - [貨櫃船](../Page/貨櫃船.md "wikilink")
  - [載貨汽車](../Page/載貨汽車.md "wikilink")
  - [鐵路列車](../Page/鐵路列車.md "wikilink")
  - [鐵軌](../Page/鐵軌.md "wikilink")
  - [貨櫃](../Page/貨櫃.md "wikilink")

[Category:物流](../Category/物流.md "wikilink")
[Category:製造業](../Category/製造業.md "wikilink")
[倉庫](../Category/倉庫.md "wikilink")