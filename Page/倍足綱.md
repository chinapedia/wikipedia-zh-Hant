**馬陸**（[英文](../Page/英文.md "wikilink")：），又稱**千足虫**、**千腳虫**、**馬䗃**、**馬蚿**、**馬𧏿**(音同“筑”)，為**倍足綱**（[学名](../Page/学名.md "wikilink")：）[節肢動物的通稱](../Page/節肢動物.md "wikilink")，陸生。大多數馬陸的活動速度都比[蜈蚣慢](../Page/蜈蚣.md "wikilink")。身體有多節，[頭部有](../Page/頭部.md "wikilink")[触角](../Page/触角.md "wikilink")，頭四節為[頭胸部](../Page/頭胸部.md "wikilink")，餘下皆為[腹部](../Page/腹部.md "wikilink")\[1\]。生活在[潮濕地方](../Page/潮濕.md "wikilink")，大多以枯枝落葉為食。有時會啃食[植物幼苗](../Page/植物.md "wikilink")。現時本綱已描述的[物種有約](../Page/物種.md "wikilink")8000種，但估計只佔[地球上所有倍足綱物種的十分之一](../Page/地球.md "wikilink")\[2\]。

## 外型構造

[Tachypodoiulus_niger_1.jpg](https://zh.wikipedia.org/wiki/File:Tachypodoiulus_niger_1.jpg "fig:Tachypodoiulus_niger_1.jpg")*\]\]
除第一節及最後一節外，绝大部分[體節各有兩對腳](../Page/體節.md "wikilink")，一般物種大约有從36隻到400隻脚，最大的物种是生活在东非的（*Archispirostreptus
gigas''）。

马陆當中比較較特別的有*[Illacme
plenipes](../Page/Illacme_plenipes.md "wikilink")*和*[Sibiriulus
baigazanensis](../Page/Sibiriulus_baigazanensis.md "wikilink")*。在[俄羅斯研究員](../Page/俄羅斯.md "wikilink")[米洛斯拉夫·薩赫涅維奇於](../Page/米洛斯拉夫·薩赫涅維奇.md "wikilink")[西伯利亞的](../Page/西伯利亞.md "wikilink")[阿爾泰生物保護區首次發現](../Page/阿爾泰生物保護區.md "wikilink")*Sibiriulus
baigazanensis*之前，*Illacme plenipes*是當時世界上腿最多的生物\[3\]。*Sibiriulus
baigazanensis*的身長幾公分到20公分不等，其中最多擁有超過700支腳。相反的，近緣的[球馬陸的身長卻很短小](../Page/球馬陸.md "wikilink")，而且遇襲擊時可把自己蜷縮起來成為球狀。馬陸味臭，令鳥獸都不愛吃。马陆体液及其分泌物含有一种化學物質[苯醌](../Page/苯醌.md "wikilink")（[1,4-苯醌](../Page/1,4-苯醌.md "wikilink")、[1,2-苯醌](../Page/1,2-苯醌.md "wikilink")），可以發揮驱[蚊作用](../Page/蚊.md "wikilink")（曾意外被發現於南美洲的行為）\[4\]\[5\]。
 {{-}}

## 古马陆

已滅絕的古马陆居住在[石炭纪](../Page/石炭纪.md "wikilink")[热带雨林陰暗而潮湿的地面上](../Page/热带雨林.md "wikilink")。躯体由30节体节构成，身长可达2[米](../Page/公尺.md "wikilink")。每节体节上都长着一对足，有如[呼氣蟲的外形](../Page/呼氣蟲.md "wikilink")。

## 分類

[Millipede_collage.jpg](https://zh.wikipedia.org/wiki/File:Millipede_collage.jpg "fig:Millipede_collage.jpg")
[Millipede_order_species_comparison.png](https://zh.wikipedia.org/wiki/File:Millipede_order_species_comparison.png "fig:Millipede_order_species_comparison.png")）和
[Siphoniulida目的](../Page/Siphoniulida.md "wikilink")2個物種\[6\]\]\]
馬陸的生物學及分類學的科學研究被稱為「Diplopodology」：也就是倍足綱物種研究。現時有大約1.2萬個馬陸的物種已被描述，但估計現存在地球的馬陸物種恐達1.5到2.0萬種\[7\]，有文獻甚至估計有8萬種\[8\]。

根據2011年的分類，倍足綱的物種可大致分為三個[亞綱](../Page/亞綱.md "wikilink")，26個[目](../Page/目_\(生物\).md "wikilink")\[9\]（已[滅絕分類群以](../Page/滅絕.md "wikilink")[劍號](../Page/劍號.md "wikilink")(†)標記）：

  - [毛尾馬陸亞綱](../Page/毛尾馬陸亞綱.md "wikilink") Penicillata <small>Latrielle,
    1831</small> = [觸顎亞綱](../Page/觸顎亞綱.md "wikilink")（Pselaphognatha）
      - [毛馬陸目](../Page/毛馬陸目.md "wikilink") Polyxenida <small>Verhoeff,
        1934</small>
  - †[節肋亞綱](../Page/節肋亞綱.md "wikilink") Arthropleuridea
    (在某些文獻中會併入毛尾馬陸亞綱)\[10\]
      - †[節肋目](../Page/節肋目.md "wikilink") Arthropleurida
        <small>Waterlot, 1934</small>
      - †目 [Eoarthropleurida](../Page/Eoarthropleurida.md "wikilink")
        <small> Shear & Selden, 1995</small>
      - †目 [Microdecemplicida](../Page/Microdecemplicida.md "wikilink")
        <small>Wilson & Shear, 2000</small>
  - [唇顎亞綱](../Page/唇顎亞綱.md "wikilink") Chilognatha <small> Latrielle,
    1802</small>\[11\]\[12\]
      - †目 [Zosterogrammida](../Page/Zosterogrammida.md "wikilink")
        <small>Wilson, 2005</small>
        ([地位未定](../Page/地位未定.md "wikilink"))\[13\]
      - [五帶馬陸下綱](../Page/五帶馬陸下綱.md "wikilink") Pentazonia <small>Brandt,
        1833 </small>
          - †目 [Amynilyspedida](../Page/Amynilyspedida.md "wikilink")
            <small>Hoffman, 1969</small>
          - [蚌形總目](../Page/蚌形總目.md "wikilink") Limacomorpha
            <small>Pocock, 1894 </small>
              - [扁形馬陸目](../Page/扁形馬陸目.md "wikilink") Glomeridesmida
                <small>Cook, 1895 </small>
          - [蟠形總目](../Page/蟠形總目.md "wikilink") Oniscomorpha
            <small>Pocock, 1887 </small>
              - [球馬陸目](../Page/球馬陸目.md "wikilink") Glomerida
                <small>Brandt, 1833 </small>
              - [蟠馬陸目](../Page/蟠馬陸目.md "wikilink") Sphaerotheriida
                <small>Brandt, 1833</small>
      - [蠕形馬陸下綱](../Page/蠕形馬陸下綱.md "wikilink") Helminthomorpha
        <small>Pocock, 1887</small>
          - †[原多足總目](../Page/原多足總目.md "wikilink") Archipolypoda
            <small>Scudder, 1882</small>
              - † [Archidesmida目](../Page/Archidesmida.md "wikilink")
                <small>Wilson & Anderson 2004</small>
              - † [Cowiedesmida目](../Page/Cowiedesmida.md "wikilink")
                <small>Wilson & Anderson 2004</small>
              - † [Euphoberiida目](../Page/Euphoberiida.md "wikilink")
                <small>Hoffman, 1969</small>
              - †
                [Palaeosomatida目](../Page/Palaeosomatida.md "wikilink")
                <small>Hannibal & Krzeminski, 2005</small>
          - † [Pleurojulida目](../Page/Pleurojulida.md "wikilink")
            <small>Schneider & Werneburg, 1998</small>
            (可能為畸顎馬陸類的姊妹分類群)\[14\]
          - [畸顎馬陸類](../Page/畸顎馬陸類.md "wikilink") Colobognatha
            <small>Brandt, 1834 </small>
              - [平馬陸目](../Page/平馬陸目.md "wikilink") Platydesmida
                <small>Cook, 1895</small>
              - [多板馬陸目](../Page/多板馬陸目.md "wikilink") Polyzoniida
                <small>Cook, 1895 </small>
              - [Siphonocryptida目](../Page/Siphonocryptida.md "wikilink")
                <small>Cook, 1895</small>
              - [管馬陸目](../Page/管馬陸目.md "wikilink") Siphonophorida
                <small>Newport, 1844</small>
          - [真顎馬陸類](../Page/真顎馬陸類.md "wikilink") Eugnatha <small>Attems,
            1898</small>
              - [馬陸總目](../Page/馬陸總目.md "wikilink") Juliformia
                <small>Attems, 1926</small>
                  - [姬馬陸目](../Page/姬馬陸目.md "wikilink") Julida
                    <small>Brandt, 1833</small>
                  - [山蛩目](../Page/山蛩目.md "wikilink") Spirobolida
                    <small>Cook, 1895</small> =
                    [圓馬陸目](../Page/圓馬陸目.md "wikilink")
                  - [異蛩目](../Page/異蛩目.md "wikilink") Spirostreptida
                    <small>Brandt, 1833</small>\[15\]
                  - †總科
                    [Xyloiuloidea](../Page/Xyloiuloidea.md "wikilink")
                    <small>Cook, 1895</small> (Sometimes aligned with
                    Spirobolida)\[16\]
              - [刺馬陸總目](../Page/刺馬陸總目.md "wikilink") Nematophora
                <small>Verhoeff, 1913 </small>
                  - [美肢馬陸目](../Page/美肢馬陸目.md "wikilink") Callipodida
                    <small>Pocock, 1894</small>
                  - [泡馬陸目](../Page/泡馬陸目.md "wikilink") Chordeumatida
                    <small>Pocock 1894</small>
                  - [斯特馬陸目](../Page/斯特馬陸目.md "wikilink") Stemmiulida
                    <small>Cook, 1895</small>
                  - [Siphoniulida目](../Page/Siphoniulida.md "wikilink")
                    <small>Cook, 1895</small>
              - 總目 [Merocheta](../Page/Merocheta.md "wikilink")
                <small>Cook, 1895</small>
                  - [帶馬陸目](../Page/帶馬陸目.md "wikilink") Polydesmida
                    <small>Pocock, 1887</small>
  - †屬 *[Eileticus](../Page/Eileticus.md "wikilink")*
    ([地位未定](../Page/地位未定.md "wikilink"))

目前[Siphoniulida目和](../Page/Siphoniulida.md "wikilink")[多板馬陸目的分類地位尚未確立](../Page/多板馬陸目.md "wikilink")\[17\]，已滅絕分類群的分類地位也是暫定的。\[18\]\[19\]

## 参见

  - [蜈蚣](../Page/蜈蚣.md "wikilink")

## 参考文献

## 外部連結

  - [Milli-PEET: The Class
    Diplopoda](http://fieldmuseum.org/explore/milli-peet/milli-peet-class-diplopoda)
    – The Field Museum, Chicago
  - [Millipedes of
    Australia](http://www.polydesmida.info/millipedesofaustralia/index.html)
  - [Diplopoda: Guide to New Zealand Soil
    Invertebrates](http://soilbugs.massey.ac.nz/diplopoda.php) – Massey
    University
  - [SysMyr, a myriapod taxonomy
    database](http://www.gbifev2.mwn.de/GloMyrIS/searchh_myr.htm)
  - [British Myriapod & Isopod Group](http://www.bmig.org.uk/)

[馬陸](../Category/馬陸.md "wikilink")
[Category:單肢亞門](../Category/單肢亞門.md "wikilink")

1.

2.
3.
4.

5.

6.
7.

8.

9.

10.

11.

12.

13.
14.
15.

16.

17.
18.
19.