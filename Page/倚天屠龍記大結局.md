為1978年[邵氏電影公司出品](../Page/邵氏電影公司.md "wikilink")，[楚原執導的武俠電影](../Page/楚原.md "wikilink")。改編自[金庸著名](../Page/金庸.md "wikilink")[武俠小說](../Page/武俠小說.md "wikilink")[倚天屠龍記](../Page/倚天屠龍記.md "wikilink")。前作為《[倚天屠龍記](../Page/倚天屠龍記_\(1978年電影\).md "wikilink")》，另有外傳《[魔殿屠龍](../Page/魔殿屠龍.md "wikilink")》。

## 電影內容

劇情以原作故事為主幹，承接上集《[倚天屠龍記](../Page/倚天屠龍記_\(1978年電影\).md "wikilink")》劇情，敘述[張無忌在光明頂一戰接掌](../Page/張無忌.md "wikilink")[明教](../Page/明教.md "wikilink")[教主之位後救出受困於萬安寺六大派高手至少林寺屠獅大會的一段故事](../Page/教主.md "wikilink")。
後接外傳《[魔殿屠龍](../Page/魔殿屠龍.md "wikilink")》。
詳細內容請參閱《[倚天屠龍記](../Page/倚天屠龍記.md "wikilink")》條目。

## 人物角色

| 角色名稱                                                               | 演員                                    | 備註                                                                           |
| ------------------------------------------------------------------ | ------------------------------------- | ---------------------------------------------------------------------------- |
| [張無忌](../Page/張無忌.md "wikilink")                                   | [爾冬陞](../Page/爾冬陞.md "wikilink")      |                                                                              |
| [趙明](../Page/趙明.md "wikilink")                                     | [井莉](../Page/井莉.md "wikilink")        |                                                                              |
| [周芷若](../Page/周芷若.md "wikilink")                                   | [余安安](../Page/余安安.md "wikilink")      |                                                                              |
| [楊逍](../Page/楊逍.md "wikilink")                                     | [王戎](../Page/王戎_\(演員\).md "wikilink") | [明教光明左使](../Page/明教.md "wikilink")                                           |
| [范遙](../Page/范遙.md "wikilink")                                     | [韋弘](../Page/韋弘.md "wikilink")        | 明教光明右使                                                                       |
| [謝遜](../Page/謝遜.md "wikilink")                                     | [羅烈](../Page/羅烈.md "wikilink")        | 金毛獅王                                                                         |
| [殷天正](../Page/殷天正.md "wikilink")                                   | [井淼](../Page/井淼.md "wikilink")        | 白眉鷹王                                                                         |
| [韋一笑](../Page/韋一笑.md "wikilink")                                   | [徐少強](../Page/徐少強.md "wikilink")      | 青翼蝠王                                                                         |
| [冷謙](../Page/冷謙.md "wikilink")                                     | [元華](../Page/元華.md "wikilink")        | 明教[五散人](../Page/五散人.md "wikilink")                                           |
| [說不得](../Page/說不得.md "wikilink")                                   | [楊志卿](../Page/楊志卿.md "wikilink")      | 明教五散人                                                                        |
| [周顛](../Page/周顛.md "wikilink")                                     | [林輝煌](../Page/林輝煌.md "wikilink")      | 明教五散人                                                                        |
| [彭和尚](../Page/彭瑩玉.md "wikilink")                                   | [姜南](../Page/姜南.md "wikilink")        | 明教五散人                                                                        |
| [鐵冠道人](../Page/張中.md "wikilink")                                   | [強漢](../Page/強漢.md "wikilink")        | 明教五散人                                                                        |
| [小昭](../Page/小昭.md "wikilink")                                     | [鄭麗芳](../Page/鄭麗芳.md "wikilink")      |                                                                              |
| [楊不悔](../Page/楊不悔.md "wikilink")                                   | [陳嘉儀](../Page/陳嘉儀.md "wikilink")      |                                                                              |
| [俞蓮舟](../Page/俞蓮舟.md "wikilink")                                   | [陸劍明](../Page/陸劍明.md "wikilink")      | [武當七俠](../Page/武當七俠.md "wikilink")                                           |
| [俞岱岩](../Page/俞岱岩.md "wikilink")                                   | [蕭玉龍](../Page/蕭玉龍.md "wikilink")      | 武當七俠                                                                         |
| [張松溪](../Page/張松溪.md "wikilink")                                   | [吳杭生](../Page/吳杭生.md "wikilink")      | 武當七俠                                                                         |
| [殷利亨](../Page/殷利亨.md "wikilink")                                   | [艾飛](../Page/艾飛.md "wikilink")        | 武當七俠                                                                         |
| [莫聲谷](../Page/莫聲谷.md "wikilink")                                   | [鄧德祥](../Page/鄧德祥.md "wikilink")      | 武當七俠                                                                         |
| [宋青書](../Page/宋青書.md "wikilink")                                   | [顧冠忠](../Page/顧冠忠.md "wikilink")      |                                                                              |
| [滅絕師太](../Page/滅絕師太.md "wikilink")                                 | [王萊](../Page/王萊.md "wikilink")        | [峨嵋派掌門](../Page/峨嵋派.md "wikilink")                                           |
| [朱九真](../Page/朱九真.md "wikilink")                                   | [潘冰嫦](../Page/潘冰嫦.md "wikilink")      | 峨嵋派弟子                                                                        |
| [何太沖](../Page/何太沖.md "wikilink")                                   | [詹森](../Page/詹森_\(演員\).md "wikilink") |                                                                              |
| [斑淑嫻](../Page/斑淑嫻.md "wikilink")                                   | [洪玲玲](../Page/洪玲玲.md "wikilink")      |                                                                              |
| [空聞](../Page/空聞.md "wikilink")                                     | [胡宏達](../Page/胡宏達.md "wikilink")      | 少林寺住持                                                                        |
|                                                                    | [陳國權](../Page/陳國權.md "wikilink")      | 少林弟子                                                                         |
|                                                                    | [杜永亮](../Page/杜永亮.md "wikilink")      | 少林弟子                                                                         |
|                                                                    | [關仁](../Page/關仁.md "wikilink")        | 少林弟子                                                                         |
|                                                                    | [譚寶](../Page/譚寶.md "wikilink")        | 少林弟子                                                                         |
| [王保保](../Page/王保保.md "wikilink")                                   | [于榮](../Page/于榮.md "wikilink")        |                                                                              |
| [鶴筆翁](../Page/鶴筆翁.md "wikilink")                                   | [沈勞](../Page/沈勞.md "wikilink")        |                                                                              |
| [金花婆婆](../Page/金花婆婆.md "wikilink")                                 | [夏萍](../Page/夏萍.md "wikilink")        | 紫杉龍王                                                                         |
| [殷離](../Page/殷離.md "wikilink")（[黃衫女子](../Page/黃衫女子.md "wikilink")） | [文雪兒](../Page/文雪兒.md "wikilink")      | 原著小說中[殷離與](../Page/殷離.md "wikilink")[黃衫女子並非同一人](../Page/黃衫女子.md "wikilink")。 |
| [流雲使](../Page/流雲使.md "wikilink")                                   | [卡樺](../Page/卡樺.md "wikilink")        | 波斯三使                                                                         |
| [妙風使](../Page/妙風使.md "wikilink")                                   | [惠天賜](../Page/惠天賜.md "wikilink")      | 波斯三使                                                                         |
| [輝月使](../Page/輝月使.md "wikilink")                                   | [惠英紅](../Page/惠英紅.md "wikilink")      | 波斯三使                                                                         |
| [圓真](../Page/圓真.md "wikilink")                                     | [田青](../Page/田青.md "wikilink")        |                                                                              |
|                                                                    | [王清河](../Page/王清河.md "wikilink")      | 鄉民                                                                           |
|                                                                    | [呂紅](../Page/呂紅.md "wikilink")        | 村姑                                                                           |
| [史火龍](../Page/史火龍.md "wikilink")                                   | [鐘國仁](../Page/鐘國仁.md "wikilink")      | 丐幫幫主                                                                         |
|                                                                    | [王憾塵](../Page/王憾塵.md "wikilink")      | 丐幫掌棒龍頭                                                                       |
|                                                                    | [馮明](../Page/馮明.md "wikilink")        | 丐幫掌缽龍頭                                                                       |
|                                                                    | [孟海](../Page/孟海.md "wikilink")        | 丐幫弟子                                                                         |
|                                                                    | [袁信義](../Page/袁信義.md "wikilink")      |                                                                              |
|                                                                    | [徐發](../Page/徐發.md "wikilink")        |                                                                              |
|                                                                    | [黃培基](../Page/黃培基.md "wikilink")      |                                                                              |
|                                                                    |                                       |                                                                              |

## 相關條目

  - [金庸](../Page/金庸.md "wikilink")
  - [倚天屠龍記](../Page/倚天屠龍記.md "wikilink")

## 備註

<references/>

## 外部連結

  - {{@movies|fhhk80077665}}

  -
  -
  -
  -
[Category:邵氏電影](../Category/邵氏電影.md "wikilink")
[8](../Category/1970年代香港電影作品.md "wikilink")
[Category:1970年代動作片](../Category/1970年代動作片.md "wikilink")
[Category:香港動作片](../Category/香港動作片.md "wikilink")
[Category:倚天屠龍記改編電影](../Category/倚天屠龍記改編電影.md "wikilink")
[Category:武俠片](../Category/武俠片.md "wikilink")