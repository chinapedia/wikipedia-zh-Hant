**倚天資訊股份有限公司**是一家位於[台灣的](../Page/台灣.md "wikilink")[電子生產商](../Page/電子.md "wikilink")，專於精密的[手持設備](../Page/手持設備.md "wikilink")。

倚天資訊於1986年3月27日在[台北創立](../Page/台北.md "wikilink")，初時因其用於[DOS作業系統電腦的](../Page/DOS.md "wikilink")[中文系統](../Page/中文系統.md "wikilink")「[倚天中文系統](../Page/倚天中文系統.md "wikilink")」而知名。該公司持續發明了多種建基於其表現中文字符經驗的[電腦硬體與](../Page/電腦硬體.md "wikilink")[軟體產品](../Page/軟體.md "wikilink")，推出市面的包括[桌面出版軟件](../Page/桌面出版.md "wikilink")、[雷射印表機與基於](../Page/雷射印表機.md "wikilink")[Microsoft
Windows的輸入系統](../Page/Microsoft_Windows.md "wikilink")。

Windows興盛之後，DOS系統沒落，連帶使中文系統市場萎縮，倚天開始轉型，推出「股博士」[股票分析軟體](../Page/股票.md "wikilink")。在1997年轉向[手持設備](../Page/手持設備.md "wikilink")，發明了可[實時](../Page/實時.md "wikilink")[股票交易](../Page/股票.md "wikilink")「金融[傳呼機](../Page/傳呼機.md "wikilink")」，簡稱「股票機」產品名為「傳訊王」在台灣[金融界相當流行](../Page/金融.md "wikilink")。在21世紀，倚天以[PDA為基礎跨入行動市場](../Page/PDA.md "wikilink")，推出[智慧型手機](../Page/智慧型手機.md "wikilink")。現時該公司集中生產基於[微軟](../Page/微軟.md "wikilink")[Windows
Mobile以及](../Page/Windows_Mobile.md "wikilink")[DAB接收器的先進通訊設備](../Page/數位音訊廣播.md "wikilink")，在2006年10月推出自有品牌「glofiish」。2008年3月3日，[宏碁宣佈以](../Page/宏碁.md "wikilink")[新臺幣](../Page/新臺幣.md "wikilink")90億元、每股1.07元[併購倚天全部](../Page/併購.md "wikilink")[股權](../Page/股權.md "wikilink")，倚天[股東約持有宏碁](../Page/股東.md "wikilink")6%股份；宏碁藉其基礎進軍行動市場。

## 沿革

[ETEN-C16DH.jpg](https://zh.wikipedia.org/wiki/File:ETEN-C16DH.jpg "fig:ETEN-C16DH.jpg")
[Eten_Glofiish_X650_AF.jpg](https://zh.wikipedia.org/wiki/File:Eten_Glofiish_X650_AF.jpg "fig:Eten_Glofiish_X650_AF.jpg")

  - 1985年1月，六位創始[股東成立](../Page/股東.md "wikilink")「凌壹資訊股份有限公司」，開發[微電腦中文系統](../Page/微電腦.md "wikilink")。
  - 1985年11月，第一代卡式中文系統「倚天中文系統ET2416」問世。
  - 1986年3月27日，「倚天資訊股份有限公司」成立，資本額為[新台幣](../Page/新台幣.md "wikilink")200萬元。
  - 1986年8月，倚天資訊推出「飛碟」系列（ET-2416F）。
  - 1987年1月，倚天資訊推出台灣第一套[積木式中文系統](../Page/積木.md "wikilink")「ET-2416B」。
  - 1987年3月，倚天資訊與[佳能企業](../Page/佳能企業.md "wikilink")（Ability Enterprise
    Co., Ltd.）合作開發「天藝影像編輯系統」。
  - 1987年8月，倚天資訊推出第三代中文卡，採用超大型[積體電路](../Page/積體電路.md "wikilink")，是台灣第一套將[顯示卡與](../Page/顯示卡.md "wikilink")[字型卡合一的單片](../Page/字型.md "wikilink")[介面卡設計的中文系統](../Page/介面卡.md "wikilink")（如「閃電一號」、「閃電二號」、「霹靂一號」、「霹靂二號」）。
  - 1988年3月，倚天資訊推出彩色中文系統「彩虹一號」。
  - 1988年4月，倚天資訊推出[桌上排版系統](../Page/桌上排版.md "wikilink")「新翰藝中文[出版系統](../Page/出版.md "wikilink")」。
  - 1988年12月，倚天資訊推出[韓文](../Page/韓文.md "wikilink")、[簡體中文系統](../Page/簡體中文.md "wikilink")，並開發[行列輸入法](../Page/行列輸入法.md "wikilink")。
  - 1989年4月，倚天資訊推出台灣第一套[解析度為](../Page/解析度.md "wikilink")1024\*768的彩色中文系統「彩虹三號」。
  - 1989年5月，倚天資訊推出台灣第一套具備[高速緩存](../Page/高速緩存.md "wikilink")（Cache）與[下載](../Page/下載.md "wikilink")（Download）功能的[雷射印表機](../Page/雷射印表機.md "wikilink")[視頻介面卡](../Page/視頻.md "wikilink")「ETV10」。
  - 1989年6月，倚天資訊推出「漢光雷射印表機」（VLP300/400ST）。
  - 1990年1月，倚天資訊推出首創彩色顯示的桌上排版系統「新翰藝中文出版系統1.50」。
  - 1990年6月，倚天資訊「飛碟四號」獲選為「[IBM PS/55](../Page/IBM_PS/55.md "wikilink")
    Hummingbird」標準配備。
  - 1990年7月，倚天資訊推出台灣第一套採用極限記憶管理技術之中文系統「倚天中文系統2.0」。
  - 1990年11月，倚天資訊推出同時支援簡體中文及[繁體中文的中文系統](../Page/繁體中文.md "wikilink")「倚天簡繁中文系統」。
  - 1991年1月，倚天資訊與[惠普公司合作開發](../Page/惠普公司.md "wikilink")[X
    Window系統專用的中文桌上排版系統](../Page/X_Window系統.md "wikilink")。
  - 1991年4月，倚天資訊代理、生產[MS-DOS](../Page/MS-DOS.md "wikilink") 5.0版。
  - 1991年11月，倚天資訊推出台灣第一個採用繪圖加速[顯示晶片的中文系統](../Page/顯示晶片.md "wikilink")「彩虹五號」。
  - 1992年2月，倚天資訊推出「倚天中文系統3.0」。
  - 1992年9月3日，倚天資訊在[台灣證券交易所](../Page/台灣證券交易所.md "wikilink")[股票上市](../Page/股票上市.md "wikilink")，上市股票代號為2432。
  - 1993年3月，倚天資訊推出「神雕筆手寫辨識系統」。
  - 1993年4月，倚天資訊與惠普公司簽約，成為[HP
    Palmtop台灣總代理](../Page/HP_Palmtop.md "wikilink")。
  - 1993年5月，倚天資訊推出「倚天中文系統3.50」與「倚天[注音中文](../Page/注音.md "wikilink")」。
  - 1993年11月，倚天資訊推出「倚天圖龍[字集](../Page/字集.md "wikilink")」。
  - 1993年12月，倚天資訊推出1024\*768解析度的碟版中文系統「飛碟24」，可在[螢幕上顯示](../Page/螢幕.md "wikilink")24\*24字型。
  - 1994年3月，倚天資訊推出簡繁中文轉換工具軟體「兩岸橋」。
  - 1994年8月，倚天資訊推出「中文系統、防[毒卡](../Page/電腦病毒.md "wikilink")、[VGA卡](../Page/VGA.md "wikilink")」三合一介面卡「倚天彩虹三效卡」。
  - 1994年12月，倚天資訊開發（與佳能企業合作）的中文雷射印表機上市。
  - 1995年6月，倚天資訊推出[DOS版](../Page/DOS.md "wikilink")「倚天中文ET2000」。
  - 1995年12月，倚天資訊推出[Windows
    95中文版專用版](../Page/Windows_95.md "wikilink")「倚天中文ET2000」。
  - 1996年4月，倚天資訊推出[Microsoft
    Windows中文版專用的](../Page/Microsoft_Windows.md "wikilink")[TrueType字集](../Page/TrueType.md "wikilink")。
  - 1996年5月，倚天資訊設立[光碟壓片廠](../Page/光碟.md "wikilink")，並引進第一條[生產線](../Page/生產線.md "wikilink")。
  - 1996年10月，倚天資訊與[Mapinfor簽約](../Page/Mapinfor.md "wikilink")，成為Mapinfor台灣代理商。
  - 1996年12月，倚天資訊推出[海報製作軟體](../Page/海報.md "wikilink")「PrintMagic」與語音軟體「VoiceBank」。
  - 1997年4月，倚天資訊推出台灣第一套利用[網際網路的Windows](../Page/網際網路.md "wikilink")
    95專用即時[證券軟體](../Page/證券.md "wikilink")「股博士即時證券系統」。
  - 1997年6月，倚天資訊推出智慧型[注音輸入法軟體](../Page/注音輸入法.md "wikilink")「倚天[忘形](../Page/忘形輸入法.md "wikilink")97」與手寫輸入設備「倚天神雕小子」。
  - 1997年8月，倚天資訊與[中華電信簽約](../Page/中華電信.md "wikilink")，共同經營「傳訊王」[無線電叫人資訊廣播業務](../Page/無線電叫人資訊廣播.md "wikilink")。
  - 1997年11月，倚天資訊推出「股博士」2.0版。
  - 1997年12月，倚天資訊推出台灣第一部「資訊服務、個人[傳呼](../Page/傳呼.md "wikilink")、[個人數位助理](../Page/個人數位助理.md "wikilink")（PDA）」三合一的智慧型中文[傳呼機](../Page/傳呼機.md "wikilink")「傳訊王加值型金融收信器」。
  - 1998年3月，倚天資訊通過[ISO 9002認證](../Page/ISO_9002.md "wikilink")。
  - 1998年5月，倚天資訊推出可全區接收資訊廣播的「傳訊王」第二代機種。
  - 1998年12月，倚天資訊推出採用[FLEX](../Page/FLEX.md "wikilink")[網路傳輸協議的](../Page/網路傳輸協議.md "wikilink")「傳訊王高智商股票機」，榮獲[台灣精品獎](../Page/台灣精品獎.md "wikilink")。
  - 1999年5月14日，倚天資訊股票在[財團法人中華民國證券櫃檯買賣中心](../Page/財團法人中華民國證券櫃檯買賣中心.md "wikilink")[上櫃](../Page/上櫃.md "wikilink")，上櫃股票代號為5390。
  - 1999年6月，「傳訊王」進軍[中國大陸](../Page/中國大陸.md "wikilink")；「傳訊王」推出「行動號子」功能，跨入[電子商務領域](../Page/電子商務.md "wikilink")。
  - 1999年10月，倚天資訊與[裕隆汽車合作開發](../Page/裕隆汽車.md "wikilink")[汽車專用PDA](../Page/汽車.md "wikilink")「PVA行動秘書」。
  - 1999年11月，倚天資訊推出「傳訊王2000」經典型與豪華型。
  - 1999年12月，倚天資訊總經理[馬惠群榮獲](../Page/馬惠群.md "wikilink")「傑出資訊人才獎」。
  - 2000年2月，倚天資訊與[世華聯合商業銀行及](../Page/世華聯合商業銀行.md "wikilink")[華信商業銀行合作推出](../Page/華信商業銀行.md "wikilink")「隨身銀行」。
  - 2000年9月11日，倚天資訊股票下櫃。
  - 2001年3月，倚天資訊與台灣著名[投資評論家](../Page/投資.md "wikilink")[-{谷}-月涵](../Page/谷月涵.md "wikilink")（Peter
    Kurz）合作提供[月涵股份有限公司](../Page/月涵股份有限公司.md "wikilink")（MrTaiwan.com）的投資[顧問服務](../Page/顧問.md "wikilink")。
  - 2001年7月，倚天資訊推出[全球第一台彩色金融股票機](../Page/全球.md "wikilink")「傳訊王C300」。
  - 2002年6月，倚天資訊推出全球第一台金融PDA「S100」。
  - 2002年7月，倚天資訊推出全球第一台內建[Pocket PC
    2002與](../Page/Pocket_PC_2002.md "wikilink")[GPRS](../Page/GPRS.md "wikilink")[模組的PDA](../Page/模組.md "wikilink")「InfoTouch
    P600」、內建[IEEE
    802.11b](../Page/IEEE_802.11b.md "wikilink")[無線網路模組的PDA](../Page/無線網路.md "wikilink")「InfoTouch
    P610」、以GPRS為基礎的股市報價下單及國際金融行情報價系統「M-Stock」。
  - 2002年8月，倚天資訊推出全球第一台亮彩金融PDA「C350」。
  - 2002年10月，倚天資訊與中華電信聯合發表第二代行動加值服務「[emome](../Page/emome.md "wikilink")333生活宅急便」。
  - 2003年7月，倚天資訊在[台北國際電腦展展示全球第一台內建](../Page/台北國際電腦展.md "wikilink")[Pocket
    PC](../Page/Pocket_PC.md "wikilink") Phone Edition中文版的PDA「Pocket PC
    300/700」。
  - 2003年9月，倚天資訊推出台灣第一支[外匯行動單機](../Page/外匯.md "wikilink")「倚天M-Forex」。
  - 2004年1月，倚天資訊推出第一支內建聲控操作軟體的[智慧型手機](../Page/智慧型手機.md "wikilink")「倚天P300智慧聲控電腦手機」。
  - 2004年9月，倚天資訊推出第一台支援外接[顯示器的股票機](../Page/顯示器.md "wikilink")「倚天C360」。
  - 2005年10月，倚天資訊推出全球最小的內建[Wi-Fi與](../Page/Wi-Fi.md "wikilink")[Skype技術的智慧型手機](../Page/Skype.md "wikilink")「倚天M600」。
  - 2006年5月，倚天資訊推出全球第一支內建[GPS功能的智慧型手機](../Page/GPS.md "wikilink")「倚天藍鑽G500」。
  - 2006年10月，倚天資訊推出自有品牌「glofiish」。
  - 2007年1月31日，倚天資訊、[元大京華證券](../Page/元大京華證券.md "wikilink")、[兆豐證券](../Page/兆豐證券.md "wikilink")、[日盛證券](../Page/日盛證券.md "wikilink")、[-{台証}-綜合證券與](../Page/台証綜合證券.md "wikilink")[台灣網路認證公司](../Page/台灣網路認證公司.md "wikilink")（Taiwan
    Certificate Authority
    Corporation；TWCA）宣佈共同推動台灣證券交易所制定的[公開金鑰憑證](../Page/公開金鑰憑證.md "wikilink")（Certificate
    Authority）。
  - 2007年9月4日，倚天資訊推出採用[Windows
    CE的](../Page/Windows_CE.md "wikilink")[英文學習專用PDA](../Page/英文.md "wikilink")「倚天L610」。
  - 2008年1月21日，倚天資訊推出智慧型手機專用股市看盤軟體「M-Stock touch」。
  - 2008年3月3日，宏碁宣佈併購倚天資訊。
  - 2008年9月5日，倚天資訊股票停止公開發行。
  - 2012年3月24日，倚天資訊總部從台北市內湖區陽光街256號搬遷至台北市內湖區[瑞光路](../Page/瑞光路.md "wikilink")68號6樓。

## 外部連結

  - [倚天資訊](http://www.eten.com.tw/)

[Category:臺灣資訊業公司](../Category/臺灣資訊業公司.md "wikilink")
[E倚](../Category/總部位於臺北市內湖區的工商業機構.md "wikilink")
[E倚](../Category/宏碁.md "wikilink")
[E倚](../Category/1986年成立的公司.md "wikilink")