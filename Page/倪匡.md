**倪匡**（），原名**倪亦明**，后改名为**聰**，[祖籍](../Page/祖籍.md "wikilink")[中國](../Page/中國.md "wikilink")[浙江](../Page/浙江.md "wikilink")[宁波](../Page/宁波.md "wikilink")[鎮海](../Page/镇海县.md "wikilink")，生于[上海](../Page/上海.md "wikilink")，[華東人民革命大學肄業](../Page/華東人民革命大學.md "wikilink")，知名的[香港](../Page/香港.md "wikilink")[作家](../Page/作家.md "wikilink")、[編劇](../Page/編劇.md "wikilink")、節目[主持人](../Page/主持人.md "wikilink")，被喻為「[香港四大才子](../Page/香港四大才子.md "wikilink")」之一，亦是女作家[亦舒的兄長](../Page/亦舒.md "wikilink")，先後使用過的筆名包括：**衛斯理**、**沙翁**、**岳川**、**魏力**、**衣其**、**洪新**、**危龍**。1957年由[中國大陸來到](../Page/中國大陸.md "wikilink")[香港](../Page/香港.md "wikilink")，1986年改奉[基督教](../Page/基督教.md "wikilink")，1992年移居[美國](../Page/美國.md "wikilink")[舊金山](../Page/舊金山.md "wikilink")，2007年返回[香港](../Page/香港.md "wikilink")。

倪匡是[兩岸三地和全球著名的](../Page/兩岸三地.md "wikilink")[科幻小說作家](../Page/科幻小說.md "wikilink")、劇作家、評論家。寫作範圍甚廣，除最主要的[科幻小說系列外](../Page/科幻小說.md "wikilink")，亦曾寫作不少的[靈異故事](../Page/靈異故事.md "wikilink")，與少量[武俠小說與](../Page/武俠小說.md "wikilink")[偵探小說](../Page/偵探小說.md "wikilink")。作品包括《[衛斯理系列](../Page/衛斯理系列.md "wikilink")》
、《[原振俠系列](../Page/原振俠系列.md "wikilink")》、《[女黑俠木蘭花系列](../Page/女黑俠木蘭花系列.md "wikilink")》等。此外，倪匡也創作了超過三百多個[電影](../Page/電影.md "wikilink")[劇本](../Page/劇本.md "wikilink")。另外亦曾在1989年至1990年間，與[黃霑和](../Page/黃霑.md "wikilink")[蔡瀾合作主持](../Page/蔡瀾.md "wikilink")[亞洲電視清談節目](../Page/亞洲電視.md "wikilink")《[今夜不設防](../Page/今夜不設防.md "wikilink")》。\[1\]

## 生平

1951年，16歲的倪匡為了追尋[烏托邦理想](../Page/烏托邦.md "wikilink")，輟學離家，隻身从上海北上[苏州進入](../Page/苏州.md "wikilink")[華東人民革命大學第四部受訓三個月](../Page/華東人民革命大學.md "wikilink")，然後參加[中國人民解放軍华东公安部队成为基层军官](../Page/中國人民解放軍.md "wikilink")，參與過苏南的[土地改革和苏北](../Page/土地改革.md "wikilink")[洪泽湖的](../Page/洪泽湖.md "wikilink")[治理淮河的工程](../Page/新中国治淮.md "wikilink")，具体说参加了双沟引河工程（在[泗洪县](../Page/泗洪县.md "wikilink")[双沟镇](../Page/双沟镇_\(泗洪县\).md "wikilink")，五万人日夜抢工赶在汛期涨水之前挖出一条人工河道，连接淮河和[洪泽湖](../Page/洪泽湖.md "wikilink")，长五十公里，最深处要挖下去二十七公尺）和[南潮河水闸工程](../Page/南潮河.md "wikilink")。1952-1953年间，在苏北[滨海县](../Page/滨海县.md "wikilink")[废黄河边上的](../Page/废黄河.md "wikilink")[七套至](../Page/七套.md "wikilink")[陈家港一带](../Page/陈家港镇.md "wikilink")，开垦盐碱地创办劳改农场：“华东军政委员会公安部新人农场”（该单位1965年受[东京湾事件后的全国形势影响从海防一线内迁](../Page/东京湾事件.md "wikilink")[泗洪县](../Page/泗洪县.md "wikilink")，现名[江苏省监狱局洪泽湖监狱](../Page/江苏省监狱局洪泽湖监狱.md "wikilink")），占地七十万亩容纳五万人，主要种棉花。首先要北起自[陈家港](../Page/陈家港镇.md "wikilink")，南迄[废黄河口](../Page/废黄河.md "wikilink")，建造一道全长超过五十公里高四公尺的海堤，以保护五十万亩即将开垦的荒地不受海水倒灌侵袭。

1955年中，志愿报名去[內蒙古](../Page/內蒙古.md "wikilink")[呼伦贝尔盟](../Page/呼伦贝尔盟.md "wikilink")[扎赉特旗](../Page/扎赉特旗.md "wikilink")[绰儿河畔开辟劳改农场种植水稻](../Page/绰儿河.md "wikilink")：“保安沼机耕农场”（即内蒙古自治区第四劳改管理大队，现为[内蒙古自治区监狱局保安沼监狱](../Page/内蒙古自治区监狱局保安沼监狱.md "wikilink")），带了一批劳改犯人先到[镇江](../Page/镇江.md "wikilink")，换乘火车专列北上，经[郑家屯](../Page/郑家屯站.md "wikilink")、在黑龙江省[泰来县下火车](../Page/泰来县.md "wikilink")，最后步行或乘马车三天到目的地。第四大队的大队长为蒙古族巴图，大队书记为汉族丁建民。争取到多次出差机会，深入大兴安岭林区深处采购基建木材，到东北的大中城市采购物资。他漸漸意識到種種不合理的行為，跟宣傳的[平等世界完全是兩碼事](../Page/平等世界.md "wikilink")，內部就有不少[特權](../Page/特權.md "wikilink")，而且事無大小都要匯報思想、開會檢討（批评与自我批评），令愛好自由的倪匡越發感到不滿和失望，經常跟上級爭拗。

1955-1956年冬，在一次風雪中運煤途上，他和另外幾名士兵把一條木橋拆下來生火取暖，結果被劳改大队[書記指為](../Page/書記.md "wikilink")「破壞交通」，最後罪名竟然是「[反革命](../Page/反革命.md "wikilink")」，遭隔離[軟禁數個月](../Page/軟禁.md "wikilink")。另外，他偷偷飼養的[狼狗把](../Page/狼狗.md "wikilink")[大隊長咬傷](../Page/大隊長.md "wikilink")，這兩罪併發，足以判他[監禁十年](../Page/監禁.md "wikilink")，此時的倪匡手足無措。

1956年5月中旬，倪匡連夜騎馬赶到[泰来县火车站](../Page/泰来县.md "wikilink")，乘坐齐齐哈尔-大连旅客列车到達[大連](../Page/大連.md "wikilink")，再乘船往[上海](../Page/上海.md "wikilink")，但那時沒有人敢接待他，他只好繼續南逃，歷盡艱辛，多次靠吃[老鼠](../Page/老鼠.md "wikilink")、[螞蟻](../Page/螞蟻.md "wikilink")、[棉花充飢](../Page/棉花.md "wikilink")，走了三個月的路到達[廣州](../Page/廣州.md "wikilink")，路上還發揮其刻製[印章的技術瞞過關防人員](../Page/印章.md "wikilink")，才成功逃離[中國大陸](../Page/中國大陸.md "wikilink")，後經[澳門](../Page/澳門.md "wikilink")，於1957年7月成功偷渡到[香港](../Page/香港.md "wikilink")。\[2\][Avenue_of_Stars_Ni_Kuang.JPG](https://zh.wikipedia.org/wiki/File:Avenue_of_Stars_Ni_Kuang.JPG "fig:Avenue_of_Stars_Ni_Kuang.JPG")的手印及簽名\]\]

### 寫作生涯

倪匡的[政治](../Page/政治.md "wikilink")[意識型態和寫作思維](../Page/意識型態.md "wikilink")，跟他年輕時在[中國大陸的經歷有著密切關係](../Page/中國大陸.md "wikilink")。他說：小時候家裡很窮，兄弟姐妹眾多（兩個哥哥、兩個弟弟、一個姊姊、一個妹妹），沒有什麼娛樂，最大的樂趣就是看書，[中國的幾本著名小說](../Page/中國.md "wikilink")，他在十二歲前已讀過，那時最喜歡的書是《[孟子](../Page/孟子.md "wikilink")》，升上中學後就愛看翻譯小說，倪匡認為人腦和[電腦一樣](../Page/電腦.md "wikilink")，都是要先輸入很多材料才會運作，相信童年時從書本的吸收，便成了他日後寫作的資源。

倪匡抵港初期在[工廠當雜工](../Page/工廠.md "wikilink")，晚上在大專院校進修，後來投稿到《[真報](../Page/真報.md "wikilink")》和《[工商日報](../Page/工商日報.md "wikilink")》，不但被採用，還獲《真報》聘用，先後出任[校對](../Page/校對.md "wikilink")、[助理編輯](../Page/助理編輯.md "wikilink")、[記者和](../Page/記者.md "wikilink")[政論專欄作家](../Page/政論專欄.md "wikilink")。他的第一篇小說是寫[中共的](../Page/中共.md "wikilink")[土改故事](../Page/土改.md "wikilink")，叫《活埋》，1957年底發表於《[工商日報](../Page/工商日報.md "wikilink")》。翌年倪匡開始創作[武俠小說](../Page/武俠小說.md "wikilink")，筆名是「岳川」，早期作品包括[女黑俠木蘭花](../Page/女黑俠木蘭花系列.md "wikilink")、[浪子高達的故事](../Page/浪子高達.md "wikilink")、[神仙手高飛的故事以及](../Page/神仙手高飛.md "wikilink")[六指琴魔等](../Page/六指琴魔_\(倪匡小说\).md "wikilink")。1963年他開始用筆名「[衛斯理](../Page/衛斯理.md "wikilink")」寫科幻小說，在《明報》副刊連載，已出版的《衛斯理》系列小說達140多本。他表示除了歌詞與廣告詞之外，什麼文類都寫過。包括小說（武俠、推理、科幻、奇幻、愛情、情色）、散文、雜文、專欄、政論、電影劇本。自言寫作最喜歡玩花樣、變題材，最不喜歡自我重複。倪匡自稱是全世界寫漢字最多和最快的人，因他自進入文壇以來，迄今寫了三十年，一個星期寫足七天，每天寫數萬字。他還透露，當年最高峰時稿費加版稅一年超過二百萬港幣。\[3\]

年過七十，曾經日寫過萬字的倪匡坦言自己已過了寫作的[高峰期](../Page/高峰期.md "wikilink")，不是不想寫，而是「寫不出，配額已用盡了」（配額一詞出自作品《算帳》）。寫作令倪匡名成利就，他也因此而一度意氣風發，沉迷於酒色財氣，1986年信奉[基督教後](../Page/基督教.md "wikilink")，才逐漸擺脫各種生活惡習。2011年加入[香港小說會](../Page/香港小說會.md "wikilink")，成為榮譽會長。\[4\]

### 編寫劇本

1960年代末，香港[武俠片興起](../Page/武俠片.md "wikilink")，倪匡轉而從事[劇本創作](../Page/劇本.md "wikilink")。十多年間，倪匡編寫的電影劇本超過四百部，代表作有[張徹導演的](../Page/張徹.md "wikilink")《[獨臂刀](../Page/獨臂刀.md "wikilink")》。據他自述，他在高峰期時曾一天寫下二萬字，十二份報章刊登其作品\[5\]。

倪匡寫了四、五百套劇本，其中拍出三百套電影。他稱自己當年創作力驚人；三、四天可以寫好一套劇本，同時還在寫七、八篇連載小說。如1972年上映的香港電影《[精武門](../Page/精武門_\(電影\).md "wikilink")》劇本；包括虛構人物主角[陳真和劇中經典的場面](../Page/陳真.md "wikilink")，亦是倪匡所撰寫。\[6\]

2012年4月，倪匡獲得[第31屆香港電影金像獎](../Page/第31屆香港電影金像獎.md "wikilink")[終身成就獎](../Page/香港電影金像獎終身成就獎.md "wikilink")\[7\]。

2018倪匡獲[香港電影編劇家協會頒發編劇會銀禧榮譽大獎](../Page/香港電影編劇家協會.md "wikilink")\[8\]。

### 政治觀點

倪匡說從不掩飾其[反共的立場](../Page/反共.md "wikilink")，2009年9月，接受[香港電台節目](../Page/香港電台.md "wikilink")《薇微語》第一集專訪，談及反[共產黨的原因](../Page/共產黨.md "wikilink")，他認為[一黨專政行不通](../Page/一黨專政.md "wikilink")，權力無限擴張，沒有監督力量，[中國共產黨必然腐化](../Page/中國共產黨.md "wikilink")。節目中他又認為現在的[中華人民共和國是一個](../Page/中華人民共和國.md "wikilink")[官僚](../Page/官僚.md "wikilink")[資本主義的國家](../Page/資本主義.md "wikilink")，[企業都是掌握在](../Page/企業.md "wikilink")[高幹子弟手中](../Page/高幹子弟.md "wikilink")，他說官僚資本主義是最無情的資本主義，他們不會同情老百姓的。並說“我寧願是[毛澤東時代](../Page/毛澤東.md "wikilink")”，亦表示“先讓部分人富起來”的本意應該是“那些部分富起來的人也要照顧未富的人”，而不可以是“富起來的人加倍壓迫窮人”。被問及有人說國家要先有經濟發展才能講[民主](../Page/民主.md "wikilink")；倪匡表示中國不同，他說中國形成了[中產階級後](../Page/中產階級.md "wikilink")，這些有[資產的階級卻不要求國家民主](../Page/資產.md "wikilink")，反而更加投靠[極權政黨](../Page/極權.md "wikilink")，冀望得到更多利益。

2006年9月15日，在一個座談會上，倪匡還說自己親身經歷過中國共產黨很多倒行逆施的事情，共產黨最可怕之處是要[洗腦](../Page/洗腦.md "wikilink")，控制別人的思想意志，人在共產黨的制度裡只會變成完全服從的機械，非常恐怖。1992年移居美國是害怕[中共](../Page/中共.md "wikilink")[收回香港](../Page/香港回歸.md "wikilink")，那時他說過「[共產黨不死光](../Page/共產黨.md "wikilink")，他不會回來」。又言如今違背當初的誓言而回來，是因為太太不適應美國的生活，所以他沒辦法，倪匡說：「我晚節不保就是了！兒女情長一定英雄氣短。」\[9\]\[10\]

2019年4月7日的港台節目「時代的記錄 -
鏗鏘說」中，倪匡接受[石永泰訪問](../Page/石永泰.md "wikilink")，談及曾經寫過一篇具寓言性的科幻小說叫追龍，故事說東方有一個城市將會滅亡，原因不是什麼天災，而是因其優點盡失，坦言故事中的城市就是失去自由的香港，並在節目上澄清無講過「妓女比[共產黨更可信](../Page/共產黨.md "wikilink")」，解釋自己「好尊重妓女」、「呢句話對妓女好侮辱」。\[11\]

## 家庭

倪匡的太太是李果珍。倪匡的弟弟倪亦平的太太是李果珍的妹妹，兩姐妹嫁給了兩兄弟。\[12\]

倪匡的妹妹[亦舒是](../Page/亦舒.md "wikilink")[文藝小說](../Page/文藝小說.md "wikilink")[作家](../Page/作家.md "wikilink")。倪匡的兒子[倪震曾任](../Page/倪震_\(香港\).md "wikilink")[電台主持人](../Page/電台.md "wikilink")，他與香港女星[周慧敏於](../Page/周慧敏.md "wikilink")2008年12月註冊結婚\[13\]\[14\]。倪匡另有一女兒[倪穗](../Page/倪穗.md "wikilink")。

## 軼聞

  - 倪匡喜愛蒐集[貝殼](../Page/貝殼.md "wikilink")，他自稱對貝殼的認識已經達到專家的境界，能為各貝殼分門別類\[15\]。收集期間從1963年至1979年，收集超過六千種貝殼，創組｢香港貝類協會｣，當時外國集貝者到香港拜訪，以倪匡收藏為第一目標；曾和旅港[英國人](../Page/英國人.md "wikilink")｢Rick
    Luther（雷克．路德）｣合著《Cowries and cones of Hong
    Kong（香港的寶貝和芋螺）》一書\[16\]。在「[十年一覺集貝夢](../Page/十年一覺集貝夢.md "wikilink")」一文自述集貝經歷始末\[17\]。
  - 倪匡不懂駕駛，但當他迷上研究汽車時，曾把一部汽車化整為零，再化零為整，裝嵌回原狀。
  - 倪匡曾在報章專欄透露，他也在[1973年香港股災前買入了](../Page/1973年香港股災.md "wikilink")[香港天線](../Page/香港天線.md "wikilink")，結果蝕了不少血汗錢\[18\]。
  - 倪匡亦曾帮助朋友[金庸撰寫在報刊上連載的](../Page/金庸.md "wikilink")[武侠小说](../Page/武侠小说.md "wikilink")《[天龙八部](../Page/天龍八部_\(小說\).md "wikilink")》中的部分章節，倪匡不喜歡書中的角色[阿紫](../Page/阿紫.md "wikilink")\[19\]，故在寫作中把她弄瞎，讓[金庸花了好些時間去令](../Page/金庸.md "wikilink")[阿紫的雙眼復原](../Page/阿紫.md "wikilink")。
  - 倪匡有「智慧老人」這個稱號是[黃毓民替倪匡改的](../Page/黃毓民.md "wikilink")。
  - 知名作家、食家[蔡瀾曾在](../Page/蔡瀾.md "wikilink")《管他的呢，我決定活得有趣：蔡瀾的瀟灑寫意人生》一書中，提到倪匡在移民美國之前打包行李時，不知道要如何處理養了許久的一對巴西烏龜，[蔡瀾等人便打趣道](../Page/蔡瀾.md "wikilink")：「不如用淮山杞子把牠們燉了，最好加幾根冬蟲草。」結果倪匡走進房間，找一把武士刀要來斬人。最後決定由兒子[倪震收留](../Page/倪震.md "wikilink")，並叮嚀「每天要用鮮蝦餵牠們。」[倪震問](../Page/倪震.md "wikilink"):「冷凍的行不行？」倪匡說:「你這衰仔，幾兩蝦又有多少錢？牠們又能吃得了多少？」說完後又回房找武士刀。[倪震落荒而逃](../Page/倪震.md "wikilink")。

## 圖片集

<File:Ni> Kuang.JPG|倪匡(2007) <File:Ni> Kuang 1.JPG|倪匡在香港書展中

## 作品

[NiKuangSciFiction.jpg](https://zh.wikipedia.org/wiki/File:NiKuangSciFiction.jpg "fig:NiKuangSciFiction.jpg")

### [衛斯理系列](../Page/衛斯理系列.md "wikilink")

| 衛斯理系列故事                                  |
| ---------------------------------------- |
| [鑽石花](../Page/鑽石花.md "wikilink")         |
| [透明光](../Page/透明光.md "wikilink")         |
| [原子空間](../Page/原子空間.md "wikilink")       |
| [紅月亮](../Page/紅月亮.md "wikilink")         |
| [屍變](../Page/屍變.md "wikilink")           |
| [再來一次](../Page/再來一次.md "wikilink")       |
| [影子](../Page/影子.md "wikilink")           |
| [古聲](../Page/古聲.md "wikilink")           |
| [環](../Page/環.md "wikilink")             |
| [創造](../Page/創造.md "wikilink")           |
| [地圖](../Page/地圖_\(小說\).md "wikilink")    |
| [新年](../Page/新年_\(小說\).md "wikilink")    |
| [天書](../Page/天書_\(小說\).md "wikilink")    |
| [尋夢](../Page/尋夢.md "wikilink")           |
| [搜靈](../Page/搜靈.md "wikilink")           |
| [洞天](../Page/洞天_\(小說\).md "wikilink")    |
| [十七年](../Page/十七年_\(小說\).md "wikilink")  |
| [遊戲](../Page/遊戲_\(小說\).md "wikilink")    |
| [密碼](../Page/密碼_\(衛斯理小說\).md "wikilink") |
| [招魂](../Page/招魂_\(小說\).md "wikilink")    |
| [錯手](../Page/錯手.md "wikilink")           |
| [怪物](../Page/怪物.md "wikilink")           |
| [烈火女](../Page/烈火女.md "wikilink")         |
| [到陰間去](../Page/到陰間去.md "wikilink")       |
| [許願](../Page/許願.md "wikilink")           |
| [轉世暗號](../Page/轉世暗號.md "wikilink")       |
| [闖禍](../Page/闖禍.md "wikilink")           |
| [爆炸](../Page/爆炸.md "wikilink")           |
| [病毒](../Page/病毒.md "wikilink")           |
| [雙程](../Page/雙程.md "wikilink")           |
| [考驗](../Page/考驗.md "wikilink")           |
| [成精變人](../Page/成精變人.md "wikilink")       |
| [本性難移](../Page/本性難移.md "wikilink")       |
| [異種人生](../Page/異種人生.md "wikilink")       |
| [乾坤挪移](../Page/乾坤挪移.md "wikilink")       |
| [非常遭遇](../Page/非常遭遇.md "wikilink")       |
| [只限老友](../Page/只限老友.md "wikilink")       |

### [原振俠系列](../Page/原振俠系列.md "wikilink")

| 原振俠傳奇系列                                   |
| ----------------------------------------- |
| [天人](../Page/天人_\(小說\).md "wikilink")     |
| [寶狐](../Page/寶狐.md "wikilink")            |
| [鬼界](../Page/鬼界.md "wikilink")            |
| [巫艷](../Page/巫艷.md "wikilink")            |
| [幽靈星座](../Page/幽靈星座.md "wikilink")        |
| [快活秘方](../Page/快活秘方.md "wikilink")        |
| [黑白無常](../Page/黑白無常_\(小說\).md "wikilink") |
| [人鬼疑雲](../Page/人鬼疑雲.md "wikilink")        |

### [亞洲之鷹羅開系列](../Page/亞洲之鷹羅開.md "wikilink")

| 亞洲之鷹羅開系列                              |
| ------------------------------------- |
| [鬼鐘](../Page/鬼鐘.md "wikilink")        |
| [巨龍](../Page/巨龍_\(小說\).md "wikilink") |
| [飛焰](../Page/飛焰.md "wikilink")        |
| [解開死結](../Page/解開死結.md "wikilink")    |

### [非人協會系列](../Page/非人協會.md "wikilink")

| 非人協會系列                                |
| ------------------------------------- |
| [魚人](../Page/魚人_\(小說\).md "wikilink") |
| [泥沼火人](../Page/泥沼火人.md "wikilink")    |

### [年輕人與公主系列](../Page/年輕人與公主.md "wikilink")

| 年輕人與公主系列                              |
| ------------------------------------- |
| [神機](../Page/神機_\(小說\).md "wikilink") |
| [四條金龍](../Page/四條金龍.md "wikilink")    |
| [足球](../Page/足球_\(小說\).md "wikilink") |
| [尺蠖](../Page/尺蠖.md "wikilink")        |

### [女黑俠木蘭花系列](../Page/女黑俠木蘭花系列.md "wikilink")

| 女黑俠木蘭花系列                                |
| --------------------------------------- |
| [巧奪死光錶](../Page/巧奪死光錶.md "wikilink")    |
| [地獄門](../Page/地獄門_\(小說\).md "wikilink") |
| [死亡織錦](../Page/死亡織錦.md "wikilink")      |
| [智擒電子盜](../Page/智擒電子盜.md "wikilink")    |
| [高空喋血](../Page/高空喋血.md "wikilink")      |
| [旋風神偷](../Page/旋風神偷.md "wikilink")      |
| [血濺黃金柱](../Page/血濺黃金柱.md "wikilink")    |
| [潛艇迷宮](../Page/潛艇迷宮.md "wikilink")      |
| [軍械大盜](../Page/軍械大盜.md "wikilink")      |
| [沉船明珠](../Page/沉船明珠.md "wikilink")      |
| [金庫奇案](../Page/金庫奇案.md "wikilink")      |
| [魔畫](../Page/魔畫.md "wikilink")          |
| [地道奇人](../Page/地道奇人.md "wikilink")      |
| [電網火花](../Page/電網火花.md "wikilink")      |
| [生命合同](../Page/生命合同.md "wikilink")      |

### [少年衛斯理系列](../Page/少年衛斯理.md "wikilink")

| 少年衛斯理系列                              |
| ------------------------------------ |
| [少年衛斯理](../Page/少年衛斯理.md "wikilink") |

### [浪子高達系列](../Page/浪子高達.md "wikilink")

| 浪子高達系列                             |
| ---------------------------------- |
| [珍珠蕩婦](../Page/珍珠蕩婦.md "wikilink") |
| [水晶豔女](../Page/水晶豔女.md "wikilink") |

### 倪匡鬼話系列

| 倪匡鬼話系列                               |
| ------------------------------------ |
| [寶寶不要哭](../Page/寶寶不要哭.md "wikilink") |
| [自來鬼](../Page/自來鬼.md "wikilink")     |

### [俠盜影子系列](../Page/俠盜影子系列.md "wikilink")

| 俠盜影子系列                           |
| -------------------------------- |
| [麗人劫](../Page/麗人劫.md "wikilink") |

### [神探高斯系列](../Page/神探高斯系列.md "wikilink")

| 神探高斯系列                             |
| ---------------------------------- |
| [晚禮服](../Page/晚禮服.md "wikilink")   |
| [預告](../Page/預告.md "wikilink")     |
| [未卜先知](../Page/未卜先知.md "wikilink") |
| [擒兇記](../Page/擒兇記.md "wikilink")   |
| [霧夜煞星](../Page/霧夜煞星.md "wikilink") |
| [太陽神](../Page/太陽神.md "wikilink")   |
| [亞洲皇后](../Page/亞洲皇后.md "wikilink") |
| [三與四](../Page/三與四.md "wikilink")   |

### 其它

| 其它                                             |
| ---------------------------------------------- |
| [六指琴魔](../Page/六指琴魔_\(倪匡小说\).md "wikilink")    |
| [蛇王石](../Page/蛇王石.md "wikilink")               |
| [一劍情深](../Page/一劍情深.md "wikilink")             |
| [紅塵白刃](../Page/紅塵白刃.md "wikilink")             |
| [劍亂情迷](../Page/劍亂情迷.md "wikilink")             |
| [一劍動四方](../Page/一劍動四方.md "wikilink")           |
| [虎魄冰魂](../Page/虎魄冰魂.md "wikilink")             |
| [十三太保](../Page/十三太保_\(1970年電影\).md "wikilink") |
| [香港鬼故事](../Page/香港鬼故事.md "wikilink")           |

## 編劇

  - [獨臂刀](../Page/獨臂刀.md "wikilink")（1967）
  - [保鏢](../Page/保鏢.md "wikilink")（1969）
  - [鐵手無情](../Page/鐵手無情.md "wikilink")（1969）
  - [銀姑](../Page/銀姑.md "wikilink")（1969）
  - [十三太保](../Page/十三太保.md "wikilink")（1970）
  - [遊俠兒](../Page/遊俠兒.md "wikilink")（1970）
  - [五虎屠龍](../Page/五虎屠龍.md "wikilink")（1970）
  - [小煞星](../Page/小煞星.md "wikilink")（1970）
  - [報仇](../Page/報仇.md "wikilink")（1970）
  - [鳳飛飛](../Page/鳳飛飛.md "wikilink")（1971）
  - [新獨臂刀](../Page/新獨臂刀.md "wikilink")（1971）
  - [追命槍](../Page/追命槍.md "wikilink")（1971）
  - [影子神鞭](../Page/影子神鞭.md "wikilink")（1971）
  - [火併](../Page/火併.md "wikilink")（1971）
  - [無名英雄](../Page/無名英雄.md "wikilink")（1971）
  - [鏢旗飛揚](../Page/鏢旗飛揚.md "wikilink")（1971）
  - [拳擊](../Page/拳擊.md "wikilink")（1971）
  - [冰天俠女](../Page/冰天俠女.md "wikilink")（1971）
  - [鷹王](../Page/鷹王.md "wikilink")（1971）
  - [雙俠](../Page/雙俠.md "wikilink")（1971）
  - [黑吃黑](../Page/黑吃黑.md "wikilink")（1972）
  - [快活林](../Page/快活林.md "wikilink")（1972）
  - [年輕人](../Page/年輕人.md "wikilink")（1972）
  - [銅頭鐵臂](../Page/銅頭鐵臂.md "wikilink")（1972）
  - [水滸傳](../Page/水滸傳.md "wikilink")（1972）
  - [仇連環](../Page/仇連環.md "wikilink")（1972）
  - [鐵指唐手](../Page/鐵指唐手.md "wikilink")（1972）
  - [馬永貞](../Page/馬永貞.md "wikilink")（1972）
  - [盲拳](../Page/盲拳.md "wikilink")（1972）
  - [亡命徒](../Page/亡命徒.md "wikilink")（1972）
  - [小孩與狗](../Page/小孩與狗.md "wikilink")（1972）
  - [龍兄虎弟](../Page/龍兄虎弟.md "wikilink")（1972）
  - [方世玉](../Page/方世玉.md "wikilink")（1972）
  - [硬碰硬](../Page/硬碰硬.md "wikilink")（1972）
  - [吉祥賭坊](../Page/吉祥賭坊.md "wikilink")（1972）
  - [唐手跆拳道](../Page/唐手跆拳道.md "wikilink")（1972）
  - [威震四方](../Page/威震四方.md "wikilink")（1972）
  - [四騎士](../Page/四騎士.md "wikilink")（1972）
  - [戰北國](../Page/戰北國.md "wikilink")（1972）
  - [惡客](../Page/惡客.md "wikilink")（1972）
  - [唐人客](../Page/唐人客.md "wikilink")（1972）
  - [趕盡殺絕](../Page/趕盡殺絕.md "wikilink")（1973）
  - [黃飛鴻](../Page/黃飛鴻.md "wikilink")（1973）
  - [刺馬](../Page/刺馬.md "wikilink")（1973）
  - [小老虎](../Page/小老虎.md "wikilink")（1973）
  - [雙龍出海](../Page/雙龍出海.md "wikilink")（1973）
  - [怪客](../Page/怪客.md "wikilink")（1973）
  - [大刀王五](../Page/大刀王五.md "wikilink")（1973）
  - [蕩寇誌](../Page/蕩寇誌.md "wikilink")（1973）
  - [男子漢](../Page/男子漢.md "wikilink")（1973）
  - [大小通吃](../Page/大小通吃.md "wikilink")（1973）
  - [江湖行](../Page/江湖行.md "wikilink")（1973）
  - [黑豹](../Page/黑豹.md "wikilink")（1973）
  - [土匪](../Page/土匪.md "wikilink")（1973）
  - [大海盜](../Page/大海盜.md "wikilink")（1973）
  - [憤怒青年](../Page/憤怒青年.md "wikilink")（1973）
  - [龍虎征西](../Page/龍虎征西.md "wikilink")（1973）
  - [逃亡](../Page/逃亡.md "wikilink")（1973）
  - [血証](../Page/血証.md "wikilink")（1973）
  - [警察](../Page/警察.md "wikilink")（1973）
  - [五虎將](../Page/五虎將.md "wikilink")（1973）
  - [叛逆](../Page/叛逆.md "wikilink")（1973）
  - [少林五祖](../Page/少林五祖.md "wikilink")（1974）
  - [鬼馬小天使](../Page/鬼馬小天使.md "wikilink")（1974）
  - [鬼眼](../Page/鬼眼.md "wikilink")（1974）
  - [大鐵牛](../Page/大鐵牛.md "wikilink")（1974）
  - [沖天炮](../Page/沖天炮.md "wikilink")（1974）
  - [電單車](../Page/電單車.md "wikilink")（1974）
  - [四大天王](../Page/四大天王.md "wikilink")（1974）
  - [太極拳](../Page/太極拳.md "wikilink")（1974）
  - [雙龍谷](../Page/雙龍谷.md "wikilink")（1974）
  - [販賣人口](../Page/販賣人口.md "wikilink")（1974）
  - [方世玉與洪熙官](../Page/方世玉與洪熙官.md "wikilink")（1974）
  - [綽頭狀元](../Page/綽頭狀元.md "wikilink")（1974）
  - [少林子弟](../Page/少林子弟.md "wikilink")（1974）
  - [朋友](../Page/朋友.md "wikilink")（1974）
  - [哪吒](../Page/哪吒.md "wikilink")（1974）
  - [吸毒者](../Page/吸毒者.md "wikilink")（1974）
  - [怪人怪事](../Page/怪人怪事.md "wikilink")（1974）
  - [海艷](../Page/海艷.md "wikilink")（1974）
  - [洪拳與詠春](../Page/洪拳與詠春.md "wikilink")（1974）
  - [蛇殺手](../Page/蛇殺手.md "wikilink")（1974）
  - [五大漢](../Page/五大漢.md "wikilink")（1974）
  - [鐵金剛大破紫陽觀](../Page/鐵金剛大破紫陽觀.md "wikilink")（1974）
  - [降頭](../Page/降頭.md "wikilink")（1975）
  - [中國超人](../Page/中國超人.md "wikilink")（1975）
  - [馬哥波羅](../Page/馬哥波羅.md "wikilink")（1975）
  - [後生](../Page/後生.md "wikilink")（1975）
  - [的士大佬](../Page/的士大佬.md "wikilink")（1975）
  - [鐵漢柔情](../Page/鐵漢柔情.md "wikilink")（1975）
  - [惡霸](../Page/惡霸.md "wikilink")（1975）
  - [義劫愛神號](../Page/義劫愛神號.md "wikilink")（1975）
  - [神打](../Page/神打.md "wikilink")（1975）
  - [大老千](../Page/大老千.md "wikilink")（1975）
  - [女逃犯](../Page/女逃犯.md "wikilink")（1975）
  - [洪拳小子](../Page/洪拳小子.md "wikilink")（1975）
  - [金粉神仙手](../Page/金粉神仙手.md "wikilink")（1975）
  - [狼吻](../Page/狼吻.md "wikilink")（1975）
  - [紅孩兒](../Page/紅孩兒.md "wikilink")（1975）
  - [血滴子](../Page/血滴子.md "wikilink")（1975）
  - [七面人](../Page/七面人.md "wikilink")（1975）
  - [密宗聖手](../Page/密宗聖手.md "wikilink")（1976）
  - [方世玉與胡惠乾](../Page/方世玉與胡惠乾.md "wikilink")（1976）
  - [少林寺](../Page/少林寺.md "wikilink")（1976）
  - [陸阿采與黃飛鴻](../Page/陸阿采與黃飛鴻.md "wikilink")（1976）
  - [詠春大兄](../Page/詠春大兄.md "wikilink")（1976）
  - [八道樓子](../Page/八道樓子.md "wikilink")（1976）
  - [勾魂降頭](../Page/勾魂降頭.md "wikilink")（1976）
  - [八國聯軍](../Page/八國聯軍.md "wikilink")（1976）
  - [香港奇案之二《兇殺》](../Page/香港奇案之二《兇殺》.md "wikilink")（1976）
  - [索命](../Page/索命.md "wikilink")（1976）
  - [香港奇案](../Page/香港奇案.md "wikilink")（1976）
  - [蛇王子](../Page/蛇王子.md "wikilink")（1976）
  - [金羅漢](../Page/金羅漢.md "wikilink")（1976）
  - [毒后秘史](../Page/毒后秘史.md "wikilink")（1976）
  - [死囚](../Page/死囚.md "wikilink")（1976）
  - [五毒天羅](../Page/五毒天羅.md "wikilink")（1976）
  - [飛龍斬](../Page/飛龍斬.md "wikilink")（1976）
  - [蔡李佛小子](../Page/蔡李佛小子.md "wikilink")（1976）
  - [流星蝴蝶劍](../Page/流星蝴蝶劍.md "wikilink")（1976）
  - [天涯明月刀](../Page/天涯明月刀.md "wikilink")（1976）
  - [虎鶴雙形](../Page/虎鶴雙形.md "wikilink")（1976）
  - [俏探女嬌娃](../Page/俏探女嬌娃.md "wikilink")（1977）
  - [功夫小子](../Page/功夫小子.md "wikilink")（1977）
  - [決殺令](../Page/決殺令.md "wikilink")（1977）
  - [射鵰英雄傳](../Page/射鵰英雄傳_\(1977年電影\).md "wikilink")（1977）
  - [搏命](../Page/搏命.md "wikilink")（1977）
  - [呂四娘闖少林](../Page/呂四娘闖少林.md "wikilink")（1977）
  - [鐵馬騮](../Page/鐵馬騮.md "wikilink")（1977）
  - [飄香劍雨](../Page/飄香劍雨.md "wikilink")（1977）
  - [洪熙官](../Page/洪熙官.md "wikilink")（1977）
  - [天龍八部](../Page/天龍八部.md "wikilink")（1977）
  - [大武士與小票客](../Page/大武士與小票客.md "wikilink")（1977）
  - [飛虎相爭](../Page/飛虎相爭.md "wikilink")（1977）
  - [猩猩王](../Page/猩猩王.md "wikilink")（1977）
  - [楚留香](../Page/楚留香.md "wikilink")（1977）
  - [江湖漢子](../Page/江湖漢子.md "wikilink")（1977）
  - [唐人街小子](../Page/唐人街小子.md "wikilink")（1977）
  - [肥龍過江](../Page/肥龍過江.md "wikilink")（1978）
  - [十字鎖喉手](../Page/十字鎖喉手.md "wikilink")（1978）
  - [色慾殺人王](../Page/色慾殺人王.md "wikilink")（1978）
  - [南少林與北少林](../Page/南少林與北少林.md "wikilink")（1978）
  - [少林卅六房](../Page/少林卅六房.md "wikilink")（1978）
  - [冷血十三鷹](../Page/冷血十三鷹.md "wikilink")（1978）
  - [五毒](../Page/五毒.md "wikilink")（1978）
  - [雙形鷹爪手](../Page/雙形鷹爪手.md "wikilink")（1978）
  - [中華丈夫](../Page/中華丈夫.md "wikilink")（1978）
  - [射鵰英雄傳續集](../Page/射鵰英雄傳續集.md "wikilink")（1978）
  - [殘缺](../Page/殘缺.md "wikilink")（1978）
  - [貂女](../Page/貂女.md "wikilink")（1978）
  - [大地飛鷹](../Page/大地飛鷹.md "wikilink")（1978）
  - [笑傲江湖](../Page/笑傲江湖.md "wikilink")（1978）
  - [鬼馬功夫](../Page/鬼馬功夫.md "wikilink")（1978）
  - [清宮大刺殺](../Page/清宮大刺殺.md "wikilink")（1978）
  - [第三把飛刀](../Page/第三把飛刀.md "wikilink")（1978）
  - [紮馬](../Page/紮馬.md "wikilink")（1978）
  - [茅山殭屍拳](../Page/茅山殭屍拳.md "wikilink")（1979）
  - [街市英雄](../Page/街市英雄.md "wikilink")（1979）
  - [佛都有火](../Page/佛都有火.md "wikilink")（1979）
  - [踢館](../Page/踢館.md "wikilink")（1979）
  - [一劍刺向太陽](../Page/一劍刺向太陽.md "wikilink")（1979）
  - [賣命小子](../Page/賣命小子.md "wikilink")（1979）
  - [上海灘大亨](../Page/上海灘大亨.md "wikilink")（1979）
  - [形手螳螂腿](../Page/形手螳螂腿.md "wikilink")（1979）
  - [雜技亡命隊](../Page/雜技亡命隊.md "wikilink")（1979）
  - [生死鬥](../Page/生死鬥.md "wikilink")（1979）
  - [廣東十虎與後五虎](../Page/廣東十虎與後五虎.md "wikilink")（1979）
  - [風流斷劍小小刀](../Page/風流斷劍小小刀.md "wikilink")（1979）
  - [金臂童](../Page/金臂童.md "wikilink")（1979）
  - [翡翠狐狸](../Page/翡翠狐狸.md "wikilink")（1979）
  - [瘋猴](../Page/瘋猴.md "wikilink")（1979）
  - [小師傅與大煞星](../Page/小師傅與大煞星.md "wikilink")（1979）
  - [七煞](../Page/七煞.md "wikilink")（1979）
  - [血肉磨坊](../Page/血肉磨坊.md "wikilink")（1979）
  - [爛頭何](../Page/爛頭何.md "wikilink")（1979）
  - [少林英雄榜](../Page/少林英雄榜.md "wikilink")（1979）
  - [教頭](../Page/教頭.md "wikilink")（1979）
  - [情俠追風劍](../Page/情俠追風劍.md "wikilink")（1980）
  - [少林搭棚大師](../Page/少林搭棚大師.md "wikilink")（1980）
  - [連城訣](../Page/連城訣.md "wikilink")（1980）
  - [飛狐外傳](../Page/飛狐外傳.md "wikilink")（1980）
  - [少林與武當](../Page/少林與武當.md "wikilink")（1980）
  - [鐵旗門](../Page/鐵旗門.md "wikilink")（1980）
  - [第三類打鬥](../Page/第三類打鬥.md "wikilink")（1980）
  - [大殺四方](../Page/大殺四方.md "wikilink")（1980）
  - [通天小子紅槍客](../Page/通天小子紅槍客.md "wikilink")（1980）
  - [背叛師門](../Page/背叛師門.md "wikilink")（1980）
  - [少林英雄](../Page/少林英雄.md "wikilink")（1980）
  - [八絕](../Page/八絕.md "wikilink")（1980）
  - [佛掌皇爺](../Page/佛掌皇爺.md "wikilink")（1980）
  - [請帖](../Page/請帖.md "wikilink")（1980）
  - [金劍](../Page/金劍.md "wikilink")（1980）
  - [武館](../Page/武館.md "wikilink")（1981）
  - [目無王法](../Page/目無王法.md "wikilink")（1981）
  - [射鵰英雄傳第三集](../Page/射鵰英雄傳第三集.md "wikilink")（1981）
  - [書劍恩仇錄](../Page/書劍恩仇錄.md "wikilink")（1981）
  - [功夫皇帝](../Page/功夫皇帝.md "wikilink")（1981）
  - [紅粉動江湖](../Page/紅粉動江湖.md "wikilink")（1981）
  - [叉手](../Page/叉手.md "wikilink")（1981）
  - [護花鈴](../Page/護花鈴.md "wikilink")（1981）
  - [再世英雄](../Page/再世英雄.md "wikilink")（1981）
  - [風流彎彎刀](../Page/風流彎彎刀.md "wikilink")（1981）
  - [名劍風流](../Page/名劍風流.md "wikilink")（1981）
  - [飛屍](../Page/飛屍.md "wikilink")（1981）
  - [血鸚鵡](../Page/血鸚鵡.md "wikilink")（1981）
  - [碧血劍](../Page/碧血劍.md "wikilink")（1981）
  - [人皮燈籠](../Page/人皮燈籠.md "wikilink")（1982）
  - [神鵰俠侶](../Page/神鵰俠侶.md "wikilink")（1982）
  - [俠客行](../Page/俠客行.md "wikilink")（1982）
  - [沖宵樓](../Page/沖宵樓.md "wikilink")（1982）
  - [神經大俠](../Page/神經大俠.md "wikilink")（1982）
  - [小子有種](../Page/小子有種.md "wikilink")（1982）
  - [五遁忍術](../Page/五遁忍術.md "wikilink")（1982）
  - [御貓三戲錦毛鼠](../Page/御貓三戲錦毛鼠.md "wikilink")（1982）
  - [三闖少林](../Page/三闖少林.md "wikilink")（1983）
  - [少林醉棍](../Page/少林醉棍.md "wikilink")（1983）
  - [五郎八卦棍](../Page/五郎八卦棍.md "wikilink")（1984）
  - [擂台](../Page/擂台.md "wikilink")（1984）
  - [義膽群英](../Page/義膽群英.md "wikilink")（1989）

## 注釋

## 參考資料

  -
  -
  -
## 外部連結

  - [倪匡真名倪小寶](http://www.ylib.com/class/topic3/show1.asp?No=66203&Object=bid)

  - [百問倪匡](https://web.archive.org/web/20070317034214/http://myweb.hinet.net/home8/nikuang2003/documents/01/01.12.htm)

  - [倪匡看世界(香港電台)](https://web.archive.org/web/20080314224937/http://www.rthk.org.hk/rthk/radio1/MyWay/)

  - [薇微語 第一集 倪匡](http://www.rthk.org.hk/rthk/tv/siumei/20090929.html)

  - [倪匡科幻獎](../Page/倪匡科幻獎.md "wikilink")
    [1](http://sf.nctu.edu.tw/award/index/)

  -
  -
  -
  -
[倪匡](../Category/倪匡.md "wikilink")
[Category:香港宁波人](../Category/香港宁波人.md "wikilink")
[Category:宁波裔上海人](../Category/宁波裔上海人.md "wikilink")
[Category:香港小說家](../Category/香港小說家.md "wikilink")
[Category:科幻小說作家](../Category/科幻小說作家.md "wikilink")
[Category:镇海县人](../Category/镇海县人.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[Category:华裔美国人](../Category/华裔美国人.md "wikilink")
[Category:香港新教徒](../Category/香港新教徒.md "wikilink")
[Category:宁波裔美国人](../Category/宁波裔美国人.md "wikilink")
[Category:左撇子](../Category/左撇子.md "wikilink")
[Category:香港電影金像獎終身成就獎得主](../Category/香港電影金像獎終身成就獎得主.md "wikilink")
[K](../Category/倪匡家族.md "wikilink")
[Category:武侠小说作家](../Category/武侠小说作家.md "wikilink")
[Category:逃港者](../Category/逃港者.md "wikilink")
[Category:归化美国公民](../Category/归化美国公民.md "wikilink")
[Category:中國反共主義者](../Category/中國反共主義者.md "wikilink")
[K](../Category/倪姓.md "wikilink")

1.  [-倪匡五七年歷盡艱辛逃亡香港](http://epochtimes.com/b5/6/10/13/n1485710.htm)【大紀元】
2.  [《薇微語》第一集　倪匡](http://programme.rthk.org.hk/rthk/tv/programme.php?name=tv/siumei&d=2009-09-29&p=4605&e=97959&m=episode)
    香港電台 2009-09-29
3.  [倪匡百問](http://yehleehwa.net/nni11.htm)原載《聯合文學》2002年1月號
4.  [倪匡談共產黨本質未變-在港奮鬥一枝筆名成利就
    -（開放雜誌）２００６年１０月號](http://epochtimes.com/b5/6/10/13/n1485710.htm)【大紀元2006年10月13日訊】
5.  [港台製作數風流人物](../Page/港台.md "wikilink") - 倪匡
6.  [非常人：倪匡　一個劇本換層樓](http://hk.apple.nextmedia.com/supplement/culture/art/20120714/16511377)蘋果日報
    2012年7月14日
7.
8.
9.  [倪匡談共產黨本質未變（開放雜誌）２００６年１０月號](http://epochtimes.com/b5/6/10/13/n1485710.htm)【大紀元2006年10月13日訊】
10. [薇微語第一集 倪匡 (第一節)](https://www.youtube.com/watch?v=jom0KZ_bQ_Q) RTHK
    香港電台《薇微語》 2010年1月
11. [倪匡-無非是這樣](https://www.youtube.com/watch?v=Fy8fvjJwmWA)RTHK
    香港電台《時代的記錄 - 鏗鏘說》 2019年4月
12.
13. [倪震宣布與周慧敏申請結婚](http://hk.news.yahoo.com/article/081218/4/9su2.html)
14. <http://www.vivianchow.asia/zh-cht/notice>
15. 港台製作 - 倪匡專訪
16. [Cowries and cones of Hong Kong:
    Amazon.co.uk](http://www.amazon.co.uk/Cowries-cones-Hong-Kong-Luther/dp/B0006CR2DU)，Publisher:
    Sing Cheong Print. Co (1975)，ASIN: B0006CR2DU
17. ｢十年一覺集貝夢｣ - 臺灣[中國時報](../Page/中國時報.md "wikilink")，倪匡，1981年3月1日
18. [炒股票引發的鬼故事(中)](http://www.am730.com.hk/col_ngai/photo/2008/200801/20080114.jpg)，《am730》，2008年1月14日
19. 2007年1月26日[商業電台](../Page/商業電台.md "wikilink")《[光明頂](../Page/光明頂_\(廣播節目\).md "wikilink")》證實。