**倪瓒**（），字**元镇**，一字玄瑛，号**云林**、幻霞生、荆蛮民、风月主人、萧闲仙卿。[江浙行省](../Page/江浙行省.md "wikilink")[无锡州](../Page/无锡.md "wikilink")（今[江苏省](../Page/江苏省.md "wikilink")[无锡市](../Page/无锡市.md "wikilink")）人。[元代](../Page/元代.md "wikilink")[诗人](../Page/诗人.md "wikilink")、[画家](../Page/画家.md "wikilink")、[书法家](../Page/书法.md "wikilink")、[茶人](../Page/茶人.md "wikilink")，倪瓒是“元四家”之一，也是元代南宗山水画的代表画家，其作品以纸本水墨为主，间用淡色。前景中的树木和空白处的楷书题款几乎成了倪瓒个人的符号。

## 生平

倪瓒家本富有，收藏图书文物甚多，[元末农民起义](../Page/元朝.md "wikilink")，社会动乱，他变卖田产，疏散财物，归隐泛舟五湖，达20年之久，研究[佛学](../Page/佛教.md "wikilink")，后来又入[全真教出家](../Page/全真教.md "wikilink")。多交游博学多才之士，曾向[黄公望请教](../Page/黄公望.md "wikilink")[绘画](../Page/绘画.md "wikilink")，并经常和[王蒙切磋](../Page/王蒙_\(画家\).md "wikilink")。后世将他和黄公望、王蒙、[吴镇并称为](../Page/吴镇.md "wikilink")“元四家”，[明](../Page/明朝.md "wikilink")[清时代受到](../Page/清朝.md "wikilink")[董其昌等人推崇](../Page/董其昌.md "wikilink")，常将他置於其他三人之上。

倪瓚有潔癖傾向，例如“庭前有樹，旦夕汲水揩洗，竟至槁死。”、“拾敗葉上有積垢似啖痕以塞責。倪掩鼻閉目，令持棄三里外”。僕人每日外出打水時，“日汲兩擔，前桶以飲，後桶以濯”，擔心僕人擔水時放屁，所以只用前桶水煎茶，後桶水僅用於洗腳。\[1\]晚年因事入獄，又因潔癖得罪獄卒，或“锁之溺器侧”，出獄後，倪瓚“愤哽竟成脾泄”，\[2\]後“因染痢，秽不可闻”，[洪武七年](../Page/洪武.md "wikilink")（1374年）病死。\[3\]一說“为太祖投溷厕中死”，但不可信。\[4\]

## 繪畫

### 画风

倪瓒善画水墨[山水画](../Page/山水画.md "wikilink")，元朝，
创造“折带皴”，是平远画法的典型，取材于[太湖一带平原风景](../Page/太湖.md "wikilink")，其画萧瑟幽寂，寒林浅水，不见人踪，画出他伤时感事的心情。倪瓒以这种空旷而辽远的太湖意境征服了画坛，以致后世常有“太湖属于倪云林”之说。\[5\]

倪瓒的画“似嫩而苍”，简中寓繁，小中见大，外落寞而内蕴激情。他也善画墨竹，风格“遒逸”，瘦劲开张。画中题咏很多。他的画由于简练，多年来伪作甚多，但不容易仿出他的萧条淡泊的气质。

倪瓒的绘画属于宋代兴起的“文人画”——通过主观的描绘表达更多的作者自身情怀，进入“有我之境”（李泽厚语）。他将自己的画法风格和追求总结成“逸气”一语，
可以说是中国画“笔墨”一说的最好代表。

### 畫作

倪瓒作品有：《江岸望山图》、《竹树野石图》、《溪山图》、《[六君子图](../Page/六君子图.md "wikilink")》、《水竹居图》、《松林亭子图》、《狮子林图》卷、《西林禅室图》、《幽寒松图》、《秋林山色图》、《春雨新篁图》、《小山竹树图》、《容膝斋图》、《修竹图》、《紫兰山房图》、《梧竹秀石图》、《新雁题诗图》等。

[File:Ni_Zan._The_Rongxi_Studio.1372._74,7x35,3._National_Palace_Museum,_Taipei.jpg|容膝齋圖繪於1372年，現藏](File:Ni_Zan._The_Rongxi_Studio.1372._74,7x35,3._National_Palace_Museum,_Taipei.jpg%7C容膝齋圖繪於1372年，現藏)[國立故宮博物院](../Page/國立故宮博物院.md "wikilink")。
[File:Ni_Zan_Water_and_Bamboo_Dwelling.jpg|水竹居图](File:Ni_Zan_Water_and_Bamboo_Dwelling.jpg%7C水竹居图)
<File:Ni> Zan Six Gentlemen.jpg|六君子图 <File:Ni> Zan Autumn Wind in
Gemstones Trees.jpg|琪树秋风图 <File:Ni> Tsan 001.jpg|虞山林壑图 Ни Цзань.Далекий
поток и холодные сосны. Гугун, Пекин.1370е.jpg|

## 书法

倪云林的书法并不显，其小楷，风格清婉妍润，简淡悠远；大字如《淡室诗轴》，用笔爽峻苍健。和画作同样体现了其高逸之气。[徐渭说](../Page/徐渭.md "wikilink")：“瓒书从隶入，辄在钟元常《荐季直表》中夺舍投胎，古而媚，密而疏。”\[6\][董其昌认为](../Page/董其昌.md "wikilink")：“倪从画悟书，因得清洒。人谓倪书有《黄庭》遗意，此论未公。倪自作一种调度，如啖橄榄，时有清律颊耳。”\[7\]

## 逸事

倪是古怪之人，有客來訪時“見其言貌粗率，大怒，掌其頰”、倪瓒好飲茶，特製“清泉白石茶”\[8\]，[趙行恕慕名而來](../Page/趙行恕.md "wikilink")，倪用此等好茶來招待他。趙行恕卻覺得此茶不怎樣。倪生气道：“吾以子为王孙，故出此品，乃略不知风味，真俗物也。”遂與之绝交。

倪氏不僅善畫，更窮治馔羞，其發明之“[雲林鵝](../Page/雲林鵝.md "wikilink")”爲清代[袁枚所推崇](../Page/袁枚.md "wikilink")，\[9\]至今已爲無錫名菜之一。

## 注釋

<small>

<references/>

</small>

## 外部連結

  - 謝正光：〈[倪瓚「霜柯竹石圖」之新贗與舊偽](http://www.litphil.sinica.edu.tw/home/publish/PDF/newsletter/87/87-189-202.pdf)〉。

[4](../Category/元朝畫家.md "wikilink")
[N](../Category/1301年出生.md "wikilink")
[N](../Category/1374年逝世.md "wikilink")
[Category:倪姓](../Category/倪姓.md "wikilink")
[Category:中國茶人](../Category/中國茶人.md "wikilink")

1.  [王錡](../Page/王錡.md "wikilink")《寓圃杂记·云林遗事》
2.  王錡《寓圃杂记·云林遗事》：“元镇因香被执，囚于有司，每传食，命狱卒举案齐眉。卒问其故，不答。旁曰：‘恐汝唾沫及饭耳。’卒怒，锁之溺器侧。众虽为祈免，愤哽竟成脾泄。”
3.  [周南老撰](../Page/周南老.md "wikilink")《元处士云林先生墓志铭》：“洪武甲寅十一月十一日甲子以疾卒，享年七十有四。”王錡《寓圃杂记·云林遗事》：“往游江阴，有习里夏氏舘之，所奉大不如意，因染痢，秽不可近，卒；夏以小棺葬於近地。”
4.  王錡《寓圃杂记·云林遗事》：“后人皆传云林为太祖投溷厕中死，尽恶其太洁而诬之也。”
5.  楚默，《倪云林研究》（百家出版社，2002年），页14。
6.  [徐渭](../Page/徐渭.md "wikilink")：《徐渭集》
7.  [董其昌](../Page/董其昌.md "wikilink")：《画禅室随笔》
8.  《雲林遺事》記載：“倪元鎮素好飲茶，在惠山中，用核桃，松子肉何真粉成小塊如石狀，置於茶中飲之，名曰清泉白石茶。”
9.  袁枚，《随园食单》羽族单。