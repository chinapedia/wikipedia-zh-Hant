**伦敦会**全名**倫敦傳道會**
（），属于[基督教](../Page/基督教.md "wikilink")[新教宗派](../Page/新教.md "wikilink")[公理宗](../Page/公理宗.md "wikilink")。建立於1795年，1977年與-{zh-hans:英联邦;zh-hk:英聯邦;zh-tw:大英國協;}-傳道會（）及英國長老會差傳委員會（）合併為**世界傳道會**（）。总部位於[英國](../Page/英國.md "wikilink")[伦敦](../Page/伦敦.md "wikilink")。

  - 1936年舊址：
  - 現址：

## 在中国之活動

1807年，[蘇格蘭](../Page/蘇格蘭.md "wikilink")[傳教士](../Page/傳教士.md "wikilink")[马礼逊到达](../Page/马礼逊.md "wikilink")[广州廣傳福音](../Page/广州.md "wikilink")。1843年[麦都思和](../Page/麦都思.md "wikilink")[雒魏林医生一同至上海](../Page/雒魏林医生.md "wikilink")。[理雅各把](../Page/理雅各.md "wikilink")[英华书院从](../Page/英华书院.md "wikilink")[马六甲迁到香港](../Page/马六甲.md "wikilink")。1855年[杨格非和](../Page/杨格非.md "wikilink")[韦廉臣同到上海](../Page/韦廉臣.md "wikilink")，韦氏创办[广学会](../Page/广学会.md "wikilink")。1861年[杨格非由上海前往湖北汉口](../Page/杨格非.md "wikilink")，开辟新传教区。[艾约瑟深入中国北方](../Page/艾约瑟.md "wikilink")[直隶地区](../Page/直隶.md "wikilink")，开创传教区。\[1\]

1898年是有伦敦传道会[皮尧士牧师](../Page/皮尧士.md "wikilink")（Rev. T. W.
Pierce）和[道济会堂](../Page/道济会堂.md "wikilink")（现称中华基督教会[合一堂](../Page/合一堂.md "wikilink")）主任王煜初牧师商议发展[新界传道工作](../Page/新界.md "wikilink")，其后亦有西人[香港愉寧堂加入](../Page/香港愉寧堂.md "wikilink")，组织[新界傳道會](../Page/新界傳道會.md "wikilink")，在[新界](../Page/新界.md "wikilink")[屯门](../Page/屯门.md "wikilink")、[荃湾](../Page/荃湾.md "wikilink")、[上水](../Page/上水.md "wikilink")、[大埔及](../Page/大埔_\(香港\).md "wikilink")[离岛各地开基传道](../Page/离岛.md "wikilink")。

### 华南区

[缩略图](https://zh.wikipedia.org/wiki/File:HK_Mid-levels_羅便臣道_80_號_Robinson_Road_old_house_Oct-2010.JPG "fig:缩略图")[羅便臣道](../Page/羅便臣道.md "wikilink")80號[倫敦傳道會大樓](../Page/倫敦傳道會大樓.md "wikilink")\]\]

  - 传教站：[廣州](../Page/廣州.md "wikilink")（1807）、[香港](../Page/香港.md "wikilink")（1843）、[博羅](../Page/博羅.md "wikilink")（1908）
  - 教堂：广州[惠爱堂](../Page/惠爱堂.md "wikilink")、万善堂（1950年起停止聚會，1961年於香港復堂，並改為加入[基督教中國佈道會](../Page/基督教中國佈道會.md "wikilink")）
  - 学校：香港[英華書院及](../Page/英華書院.md "wikilink")[英華女學校](../Page/英華女學校.md "wikilink")，兩校現由屬下的[中華基督教會香港區會管理](../Page/中華基督教會香港區會.md "wikilink")。
  - 医院：广州金利埠（今[六二三路容安街](../Page/六二三路.md "wikilink")）惠爱医院（不存），香港[雅麗氏何妙齡那打素醫院](../Page/雅麗氏何妙齡那打素醫院.md "wikilink")

### 华东区

  - 传教站：[上海](../Page/上海.md "wikilink")（1843-1951）、[南京](../Page/南京.md "wikilink")（1923）
  - 教堂：上海山东路[天安堂](../Page/天安堂.md "wikilink")、[天乐堂](../Page/天乐堂.md "wikilink")、城中堂
  - 学校：上海[麦伦中学](../Page/麦伦中学.md "wikilink")
  - 医院：上海[仁济医院](../Page/仁济医院.md "wikilink")

### 福建区

  - 传教站：[廈門](../Page/廈門.md "wikilink")（1844）、[惠安](../Page/惠安.md "wikilink")（1866）、[漳州](../Page/漳州.md "wikilink")（1888）、[汀州](../Page/汀州.md "wikilink")
    （1907）
  - 教堂：厦门[泰山堂](../Page/泰山堂.md "wikilink")、[关隘内堂](../Page/关隘内堂.md "wikilink")、[鼓浪屿福音堂](../Page/鼓浪屿福音堂.md "wikilink")、鼓浪屿内厝沃公平路（鸡山路）18号[讲道堂](../Page/讲道堂.md "wikilink")
  - 学校：
  - 医院：惠安仁世医院

### 华中区

  - 传教站：[漢口](../Page/漢口.md "wikilink")（1861）、[武昌](../Page/武昌.md "wikilink")（1867）、[孝感](../Page/孝感.md "wikilink")（1880）、[黃陂](../Page/黃陂.md "wikilink")（1898）、[皂市](../Page/皂市.md "wikilink")（1899）
  - 教堂：武昌戈甲营[崇真堂](../Page/崇真堂.md "wikilink")、汉口[格非堂](../Page/格非堂.md "wikilink")
  - 学校：汉口[博学中学](../Page/博学中学.md "wikilink")
  - 医院：汉口[协和医院](../Page/协和医院.md "wikilink")，武昌仁济医院，皂市仁济医院，黄陂仁济医院，孝感仁济医院

### 华北区

  - 传教站：[北京](../Page/北京.md "wikilink")（1861）、[天津](../Page/天津.md "wikilink")（1861）、[枣强县](../Page/枣强县.md "wikilink")[肖张镇](../Page/肖张镇.md "wikilink")（1888）、[沧州](../Page/沧州.md "wikilink")
    （1896）、[濟南](../Page/濟南.md "wikilink") （1918）
  - 教堂：北京[米市堂](../Page/米市堂.md "wikilink")、[缸瓦市堂](../Page/缸瓦市堂.md "wikilink")
  - 学校：天津[新学中学](../Page/新学中学.md "wikilink")
  - 医院：天津[马大夫医院](../Page/马大夫医院.md "wikilink")、沧州博施医院、肖张医院

## 著名人物

  - [马礼逊](../Page/马礼逊.md "wikilink")（Robert Morrison, 1782—1834）
  - [米怜](../Page/米怜.md "wikilink")（William Milne，1785—1822）
  - [麦都思](../Page/麦都思.md "wikilink")（Walter Henry Medhurst，1796—1857）
  - [理雅各](../Page/理雅各.md "wikilink")（James Legge，1815—1897）
  - [合信](../Page/合信.md "wikilink")（Benjamin Hobson，1816—1873）
  - [洪仁玕](../Page/洪仁玕.md "wikilink")（1822-1864）
  - [韦廉臣](../Page/韦廉臣.md "wikilink")（Alexander Williamson, 1829—1890）
  - [杨格非](../Page/杨格非.md "wikilink")（Griffith John，1831－1912）
  - [雒魏林](../Page/雒魏林.md "wikilink")（William Lockhart，1811－1896）
  - [李爱锐](../Page/埃里克·利德爾.md "wikilink")（Eric Henry Liddell，1902－1945）

## 参考文献

## 外部連結

  - [世界傳道會（CWM）網頁](http://www.cwmission.org.uk/)
  - 蘆笛：〈[上海伦敦会早期藏书研究](http://www.nssd.org/articles/Article_Read.aspx?id=665831233)〉。

[Category:公理宗](../Category/公理宗.md "wikilink")
[Category:基督教在华差会](../Category/基督教在华差会.md "wikilink")

1.