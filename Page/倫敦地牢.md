**倫敦地牢** ()
是伦敦一处旅游景点。\[1\]以類似主題公園的做法，用人偶呈現[英國与](../Page/英國.md "wikilink")[歐洲的血腥歷史](../Page/歐洲.md "wikilink")，並以音響、照明等重現逼真的臨場感。參觀內部時，以一個區域為主題，會有專員穿著古裝，為參觀者解說，也會邀請參觀者一起進入當時的活動，可以看到拷打的現場、處置[巫婆](../Page/巫婆.md "wikilink")、[黑死病](../Page/黑死病.md "wikilink")、轟動一時殺人狂[開膛手傑克](../Page/開膛手傑克.md "wikilink")、當時[倫敦大火的情形](../Page/倫敦.md "wikilink")、令人起雞皮疙瘩的展示。

## 历史

1974年开馆，最初设计成一个恐怕历史的博物馆，但逐渐成为一处演员为主导，观众互动参与的体验场馆。伦敦地牢目前是[默林娱乐集团所属](../Page/默林娱乐.md "wikilink")，2013年搬迁至[伦敦眼附近的](../Page/伦敦眼.md "wikilink")[伦敦郡会堂内部](../Page/伦敦郡会堂.md "wikilink")。

## 画廊

<File:2005-07-12> - United Kingdom - England - London - The London
Dungeon.jpg|thumb|伦敦地牢入口 |thumb|upright|伦敦地牢骨架

## 相关景点

[默林娱乐集团在欧洲共有八处以地牢为主题结合所在城市特色的旅游景点](../Page/默林娱乐.md "wikilink")，除了[伦敦地牢之外](../Page/伦敦地牢.md "wikilink")，还有[阿姆斯特丹地牢](../Page/阿姆斯特丹地牢.md "wikilink")，[柏林地牢](../Page/柏林地牢.md "wikilink")，[布莱克浦尔塔地牢](../Page/布莱克浦尔塔.md "wikilink")，[华威城堡地牢](../Page/华威城堡.md "wikilink")，爱丁堡地牢，[汉堡地牢](../Page/汉堡地牢.md "wikilink")，[约克地牢](../Page/约克地牢.md "wikilink")。

## 参考来源

## 外部链接

  - [The London Dungeon](http://www.the-dungeons.co.uk/london/en/)
  - [The Edinburgh Dungeon](http://www.edinburghdungeon.co.uk)

[Category:默林娱乐集团](../Category/默林娱乐集团.md "wikilink")
[L](../Category/英國主題公園.md "wikilink")
[Category:1974年成立的公司](../Category/1974年成立的公司.md "wikilink")
[Category:英国历史博物馆](../Category/英国历史博物馆.md "wikilink")
[Category:伦敦博物馆](../Category/伦敦博物馆.md "wikilink")
[Category:伦敦建筑物](../Category/伦敦建筑物.md "wikilink")
[Category:伦敦旅游景点](../Category/伦敦旅游景点.md "wikilink")

1.