[Senate_House_UoL.jpg](https://zh.wikipedia.org/wiki/File:Senate_House_UoL.jpg "fig:Senate_House_UoL.jpg")

**伦敦大学**（）是由多个行政獨立的院校联合组成的學府（聯邦制大學），亦是世界上规模最大的大学之一。这些院校包括不少世界上最具名望的学校（如[倫敦大學學院](../Page/倫敦大學學院.md "wikilink")、[倫敦政治經濟學院](../Page/倫敦政治經濟學院.md "wikilink")、[倫敦國王學院](../Page/倫敦國王學院.md "wikilink")、[倫敦大學城市学院](../Page/倫敦大學城市学院.md "wikilink")、[倫敦大學亞非學院](../Page/倫敦大學亞非學院.md "wikilink")、[伦敦玛丽王后大学](../Page/伦敦玛丽王后大学.md "wikilink")、[皇家霍洛威學院](../Page/皇家霍洛威學院.md "wikilink")、[倫敦衛生與熱帶醫學院](../Page/倫敦衛生與熱帶醫學院.md "wikilink")、[哲學及神學專科學院等](../Page/哲學及神學專科學院.md "wikilink")）。全[英国有大约](../Page/英国.md "wikilink")5%的学生都在伦敦大学的学院中学习过。大學旗下的學院都擁有高度的自治權，各學院在學術上有各自的領域，例如：[倫敦政治經濟學院負責社會科學](../Page/倫敦政治經濟學院.md "wikilink")、[皇家音樂學院負責音樂](../Page/英國皇家音樂學院.md "wikilink")、[金匠學院負責藝文和傳媒](../Page/金匠學院.md "wikilink")、[哲學及神學專科學院負責哲學和神學](../Page/哲學及神學專科學院.md "wikilink")、[倫敦衛生與熱帶醫學院負責公共衛生與熱帶醫學](../Page/倫敦衛生與熱帶醫學院.md "wikilink")、[科陶德藝術學院爲頂尖藝術史學府](../Page/科陶德藝術學院.md "wikilink")、[亞非學院則為世界最重要的亞洲與非洲區域研究機構](../Page/亞非學院.md "wikilink")，其漢學研究與館藏亦享負盛名。2016年新加入的[城市学院的法学院以及傳播學院在英國相當知名](../Page/伦敦大学城市学院.md "wikilink")，在QS新聞傳播科系類排名在全英國第二位。英國衛報記者Michael
Hann曾將卡迪夫大學與倫敦大學城市學院的新聞科系比做「傳播界的劍橋牛津」。倫敦大學的學院擁有英國最完整的科系，旗下有些獨立院校（如[倫敦大學學院](../Page/倫敦大學學院.md "wikilink")、[倫敦國王學院](../Page/倫敦國王學院.md "wikilink")、[伦敦玛丽王后大学](../Page/伦敦玛丽王后大学.md "wikilink")、[皇家賀洛唯學院](../Page/皇家賀洛唯學院.md "wikilink")、[城市学院](../Page/伦敦大学城市学院.md "wikilink")）的規模相比於其他大型大學無異。而倫敦大學主要行政辦公室位於倫敦[布卢姆茨伯里區](../Page/布卢姆茨伯里.md "wikilink")（Bloomsbury）的[羅素廣場的议事大楼](../Page/羅素廣場.md "wikilink")（Senate
House）之內，该处还建有图书馆和校长的住所。

英國和國外很多大學建校時都是倫敦大學的下屬學院，在授權下頒發倫敦大學的學位。例如，南安普敦大學在1952年獲得皇家特許狀（Royal
Charter）升格為獨立大學之前，也是作为倫敦大學的一個學院提供倫敦大學學位的。近年隨着教育市場的[全球化](../Page/全球化.md "wikilink")，越來越多的海外院校被授權頒發倫敦大學的文憑和學位。

1836年倫敦大學最初建校時，只有[倫敦大學學院和](../Page/倫敦大學學院.md "wikilink")[倫敦國王學院这兩所學院](../Page/倫敦國王學院.md "wikilink")。如今學院有19所，除[倫敦大學學院和](../Page/倫敦大學學院.md "wikilink")[倫敦國王學院之外](../Page/倫敦國王學院.md "wikilink")，[倫敦政治經濟學院](../Page/倫敦政治經濟學院.md "wikilink")、[伦敦玛丽王后大学](../Page/伦敦玛丽王后大学.md "wikilink")、[亞非學院](../Page/亞非學院.md "wikilink")、[皇家賀洛唯學院](../Page/皇家賀洛唯學院.md "wikilink")、[金匠學院](../Page/金匠學院.md "wikilink")、[哲學及神學專科學院](../Page/哲學及神學專科學院.md "wikilink")、[倫敦衛生與熱帶醫學院和](../Page/倫敦衛生與熱帶醫學院.md "wikilink")[倫敦商學院等學院在世界享負盛名](../Page/倫敦商學院.md "wikilink")。各個學院和研究所分散在大倫敦地區，給予高度的自主權。在多數實際場合中，這些學院被作為獨立的大學對待。根據英國法律，這些學院中有些是有權頒發自己的學位的“**認可機構**”（Recognised
bodies），儘管多數學院並不履行這個權利。另外一些則是僅提供倫敦大學學位的課程的“**指定機構**”（Listed
bodies）。[倫敦大學城市學院創建於](../Page/倫敦大學城市學院.md "wikilink")1894年，2016年加入倫敦大學。

[倫敦帝國學院的學生曾經被授予倫敦大學的學位](../Page/倫敦帝國學院.md "wikilink")，倫敦帝國學院在2007年7月8日其一百週年校慶之际，正式宣布脫離倫敦大學。伦敦大學校长[霍华德·戴维斯爵士发表声明](../Page/霍华德·戴维斯.md "wikilink")，表示伦敦政治经济学院、伦敦大学学院和伦敦国王学院决定不会退出伦敦大学，但将对2007年9月入学的新生开始颁发学院自己的学位。

伦敦大学下属眾多学院的学术声望在英国可与牛津、剑桥相比拟，三校被称为[金三角名校](../Page/金三角名校.md "wikilink")。根據最新的全球大學排行榜，英國的劍橋大學、倫敦大學學院、帝國學院和牛津大學位居前十位之內.

## 学院和研究院

倫敦大學的10所大型院校（稱為「學院」）分別為[伯貝克學院](../Page/伯貝克學院.md "wikilink")、[金匠学院](../Page/金匠学院.md "wikilink")、[国王学院](../Page/伦敦国王学院.md "wikilink")、[伦敦商学院](../Page/伦敦商学院.md "wikilink")、[皇家哈洛威學院](../Page/皇家哈洛威學院.md "wikilink")、[伦敦玛丽王后大学](../Page/伦敦玛丽王后大学.md "wikilink")、[亞非學院](../Page/亞非學院.md "wikilink")、[城市學院](../Page/城市學院.md "wikilink")、[政治经济学院及](../Page/伦敦大学政治经济学院.md "wikilink")[伦敦大学学院](../Page/伦敦大学学院.md "wikilink")。其中，部份學院擁有頒發學位的權力，包括：金匠学院、国王学院、政治经济学院、皇家哈洛威學院、伦敦玛丽王后大学、亞非學院、圣乔治学院及大学学院。[倫敦帝國學院於](../Page/倫敦帝國學院.md "wikilink")2007年正式脫離倫敦大學聯邦。

[Founder's_Building,_Royal_Holloway,_south_quad.jpg](https://zh.wikipedia.org/wiki/File:Founder's_Building,_Royal_Holloway,_south_quad.jpg "fig:Founder's_Building,_Royal_Holloway,_south_quad.jpg")\]\]
[Hodgkin_Building.jpg](https://zh.wikipedia.org/wiki/File:Hodgkin_Building.jpg "fig:Hodgkin_Building.jpg")\]\]
[Queen_Mary_&_Westfield_College.jpg](https://zh.wikipedia.org/wiki/File:Queen_Mary_&_Westfield_College.jpg "fig:Queen_Mary_&_Westfield_College.jpg")\]\]
[Heyfront.jpg](https://zh.wikipedia.org/wiki/File:Heyfront.jpg "fig:Heyfront.jpg")\]\]
[Church_of_Christ_the_King,_Bloomsbury.jpg](https://zh.wikipedia.org/wiki/File:Church_of_Christ_the_King,_Bloomsbury.jpg "fig:Church_of_Christ_the_King,_Bloomsbury.jpg")

### 认可机构

| 學院名稱                                                  | 加入年份 |
| ----------------------------------------------------- | ---- |
| [倫敦大學伯貝克學院](../Page/倫敦大學伯貝克學院.md "wikilink")（BBK）     | 1920 |
| [科陶德藝術學院](../Page/科陶德藝術學院.md "wikilink")              | 1932 |
| [倫敦大學金匠學院](../Page/倫敦大學金匠學院.md "wikilink")            | 1904 |
| [哲學及神學專科學院](../Page/哲學及神學專科學院.md "wikilink")（HEY）     | 1971 |
| [癌癥研究院](../Page/癌癥研究院.md "wikilink")（ICR）             | 2003 |
| [倫敦國王學院](../Page/倫敦國王學院.md "wikilink")（KCL）           | 創校學院 |
| [倫敦大學商學院](../Page/倫敦大學商學院.md "wikilink")（LBS）         | 1964 |
| [倫敦政治經濟學院](../Page/倫敦政治經濟學院.md "wikilink")（LSE）       | 1900 |
| [倫敦衛生與熱帶醫學院](../Page/倫敦衛生與熱帶醫學院.md "wikilink")（LSHTM） | 1924 |
| [伦敦玛丽王后大学](../Page/伦敦玛丽王后大学.md "wikilink")（QMUL）      | 1915 |
| [英國皇家音樂學院](../Page/英國皇家音樂學院.md "wikilink")（RAM）       | 2003 |
| [中央演講和戲劇學院](../Page/中央演講和戲劇學院.md "wikilink")（RCSSD）   | 2005 |
| [皇家賀洛唯學院](../Page/皇家賀洛唯學院.md "wikilink")（RHUL）        | 1900 |
| [皇家兽医学院](../Page/皇家兽医学院.md "wikilink")（RVC）           | 1915 |
| [倫敦大學亞非學院](../Page/倫敦大學亞非學院.md "wikilink")（SOAS）      | 1916 |
| [倫敦大學城市學院](../Page/倫敦大學城市學院.md "wikilink")（CUL）       | 2016 |
| [伦敦大学圣乔治学院](../Page/伦敦大学圣乔治学院.md "wikilink")（SGUL）    | 19世紀 |
| [倫敦大學學院](../Page/倫敦大學學院.md "wikilink")（UCL）           | 創校學院 |

### 指定机构

  - 伦敦大学巴黎学院(University of London Institute in Paris)
  - 高级研究学院(School of Advanced Study)，包括以下研究所：
      - 高级法律研究所
      - 古典研究所
      - 英联邦研究所
      - 英语研究所（包括手稿和印刷研究中心）
      - 日尔曼语言研究所
      - 历史研究所
      - 拉丁美洲研究所
      - 罗曼语研究所
      - 美国研究所
      - 沃伯格研究所
  - 米尔港大学海洋生物学研究站

### 不再存在的学院

伦敦大学的一些学院被合并到其他较大的学院或转移到其他地方。这些不复存在的学院包括：

  - [倫敦帝國學院](../Page/倫敦帝國學院.md "wikilink")：於2007年獨立
  - 贝德福德学院：已併入皇家賀洛唯學院
  - 切尔西科技学院：於1985年併入倫敦国王学院
  - 伊丽莎白女王学院：於1985年併入倫敦国王学院
  - 精神病學研究院：於1997年併入倫敦国王学院
  - 韦斯特菲尔德学院：已併入伦敦玛丽王后大学
  - [伦敦大学药学院](../Page/伦敦大学药学院.md "wikilink")：於2012年併入倫敦大學學院
  - [伦敦大学教育研究院](../Page/教育研究院.md "wikilink")：於2014年併入倫敦大學學院

## 知名校友

  - [戴卓爾夫人](../Page/戴卓爾夫人.md "wikilink")，前英國首相
  - [馬斐森](../Page/馬斐森.md "wikilink")，[英國醫學學者](../Page/英國.md "wikilink")、臨床醫學專家、[英國](../Page/英國.md "wikilink")[愛丁堡大學校長](../Page/愛丁堡大學.md "wikilink")﹐曾任[布里斯托大學醫科及牙科學院院長及](../Page/布里斯托大學.md "wikilink")[香港大學校長](../Page/香港大學.md "wikilink")
  - [查尔斯·卡罗尔](../Page/查尔斯·卡罗尔.md "wikilink")，[美國政治家](../Page/美國.md "wikilink"),[美國參議員](../Page/美國參議員.md "wikilink")(《獨立宣言》簽署人中唯一的天主教徒)
  - [約翰·卡羅爾大主教](../Page/約翰·卡羅爾.md "wikilink")，首位[美國](../Page/美國.md "wikilink")[羅馬天主教主教和大主教](../Page/羅馬天主教.md "wikilink")
  - [傑拉爾德·曼利·霍普金斯](../Page/傑拉爾德·曼利·霍普金斯.md "wikilink")，在二十世紀最負盛名的[英國](../Page/英國.md "wikilink")[維多利亞時代詩人](../Page/維多利亞時代.md "wikilink")
  - [嚴肇基](../Page/嚴肇基.md "wikilink")，[澳門副院長](../Page/澳門.md "wikilink")、
    博士。现任[澳門理工學院署理院長](../Page/澳門理工學院.md "wikilink")
  - [余义明](../Page/余义明.md "wikilink")，[新加坡商人](../Page/新加坡.md "wikilink")，現任[余仁生集團總裁](../Page/余仁生.md "wikilink")、企業家
  - [戴耀廷](../Page/戴耀廷.md "wikilink")，香港法律學家、社會運動家
  - [黎棟國](../Page/黎棟國.md "wikilink")，[香港](../Page/香港.md "wikilink")[保安局局長](../Page/保安局局長.md "wikilink")
  - [杨洁篪](../Page/杨洁篪.md "wikilink")，曾任[中華人民共和國外交部部長](../Page/中華人民共和國外交部部長.md "wikilink")、[中国驻美国特命全权大使](../Page/中国驻美国大使.md "wikilink")
  - [鄧厚江](../Page/鄧厚江.md "wikilink")，退休[香港警務處高級助理處長](../Page/香港警務處高級助理處長.md "wikilink")
  - [陳謳明主教](../Page/陳謳明.md "wikilink")，[香港聖公會](../Page/香港聖公會.md "wikilink")[西九龍教區主教](../Page/西九龍教區.md "wikilink")
  - [徐志摩](../Page/徐志摩.md "wikilink")，中國著名新月派現代詩人、散文家
  - [王光亚](../Page/王光亚.md "wikilink")，现任[国务院港澳事务办公室](../Page/国务院港澳事务办公室.md "wikilink")、[中央港澳工作协调小组办公室主任](../Page/中央港澳工作协调小组.md "wikilink")。
  - [圣雄甘地](../Page/圣雄甘地.md "wikilink")—印度国父
  - [弗朗西斯·克里克](../Page/弗朗西斯·克里克.md "wikilink")— DNA雙螺旋之父之一
  - [伊藤博文](../Page/伊藤博文.md "wikilink")—第一任日本首相、[明治维新元老](../Page/明治维新.md "wikilink")
  - [陈忱 (作家)](../Page/陈忱_\(作家\).md "wikilink")
    中国当代女作家，筆名完美災難，90後懸疑第一人，第一個走上戛納電影節紅毯的中國90後編劇
  - [小泉纯一郎](../Page/小泉纯一郎.md "wikilink")—前日本首相
  - [萨道义](../Page/萨道义.md "wikilink")—
    [PC](../Page/PC.md "wikilink")，[GCMG](../Page/GCMG.md "wikilink")，英国驻华公使，代表英国签署《辛丑条约》
  - [哈伊姆·赫尔佐克](../Page/哈伊姆·赫尔佐克.md "wikilink")—前以色列总统
  - [乔莫·肯雅塔](../Page/乔莫·肯雅塔.md "wikilink")—肯尼亚国父、第一任总统
  - [伍廷芳](../Page/伍廷芳.md "wikilink")—中国近代外交家、首位取得英國大律师资格的华人
  - [李福善](../Page/李福善.md "wikilink")—前香港最高法院上诉庭副庭长、首位华人高等法院按察司
  - [楊鐵樑](../Page/楊鐵樑.md "wikilink")—前香港首席大法官
  - [卢嘉锡](../Page/卢嘉锡.md "wikilink")—前中国科学院院长
  - [唐明治](../Page/唐明治.md "wikilink")：1983年至1988年在[香港政府擔任](../Page/香港殖民地時期#香港政府.md "wikilink")[律政司](../Page/律政司.md "wikilink")
  - [李義](../Page/李義.md "wikilink")（）:
    [香港終審法院常任法官](../Page/香港終審法院.md "wikilink")
  - [李業廣](../Page/李業廣.md "wikilink")：[香港行政會議成員](../Page/香港行政會議.md "wikilink")，前任[香港交易所主席](../Page/香港交易所.md "wikilink")，[胡關李羅律師行合夥人](../Page/胡關李羅律師行.md "wikilink")
  - [劉慧卿](../Page/劉慧卿.md "wikilink")：[香港](../Page/香港.md "wikilink")[民主黨主席](../Page/民主黨.md "wikilink")，[香港立法會議員](../Page/香港立法會.md "wikilink")（新界東）
  - [余若薇](../Page/余若薇.md "wikilink")：資深大律師，前香港立法會議員（香港島），[公民黨主席](../Page/公民黨.md "wikilink")
  - [廖長城](../Page/廖長城.md "wikilink")：執業資深大律師，[香港特區政府](../Page/香港特區政府.md "wikilink")[行政會議前非官方成員](../Page/行政會議.md "wikilink")
  - [王易鳴](../Page/王易鳴.md "wikilink")：[香港青年協會總幹事](../Page/香港青年協會.md "wikilink")，前[香港房屋委員會主席](../Page/香港房屋委員會.md "wikilink")
  - [何超蓮](../Page/何超蓮.md "wikilink")：何鴻燊千金，被香港傳媒稱為「全城最索（漂亮）千金」
  - [陶傑](../Page/陶傑.md "wikilink")：[香港著名作家](../Page/香港.md "wikilink")
  - [鄧永鏘](../Page/鄧永鏘.md "wikilink")：中國傳統服裝品牌上海灘，香港、北京及新加坡中國會創辦人
  - [卓振賢](../Page/卓振賢.md "wikilink")：前香港警務處助理處長，在任警司期間因為大力打擊色情事業，獲得「鐵腕警司」綽號
  - [亚历山大·格拉汉姆·贝尔](../Page/亚历山大·格拉汉姆·贝尔.md "wikilink")—电话的发明者
  - [陈占祥](../Page/陈占祥.md "wikilink")—中国著名城市规划师、建筑师
  - [潘宗光](../Page/潘宗光.md "wikilink")—前香港理工大学校长
  - [卢杰](../Page/卢杰.md "wikilink")—国际著名的“长征计划”的发起人和总负责人
  - [泰戈尔](../Page/泰戈尔.md "wikilink")—著名诗人；1913年诺贝尔文学奖得奖者，为首位亚裔诺贝尔奖得主
  - [夏目漱石](../Page/夏目漱石.md "wikilink")—日本“国民大作家”
  - [酷玩乐队](../Page/酷玩乐队.md "wikilink")（Coldplay）
  - [莫文蔚](../Page/莫文蔚.md "wikilink")—香港歌手和演員
  - [徐子淇](../Page/徐子淇.md "wikilink")—前香港知名模特儿、电影演员，现为香港富豪李兆基二子李家诚的妻子
  - [傅斯年](../Page/傅斯年.md "wikilink")—国立台湾大学校长
  - [张道藩](../Page/张道藩.md "wikilink")—中华民国第四任第一届立法院院长
  - [李福善](../Page/李福善.md "wikilink")—香港最高法院法官
  - [克里斯多福·诺兰](../Page/克里斯多福·诺兰.md "wikilink")—英国导演
  - [森有禮](../Page/森有禮.md "wikilink")－
    [日本首任](../Page/日本.md "wikilink")[文部大臣](../Page/文部大臣.md "wikilink")，被稱為日本「明治時期六大教育家」之一和「日本現代教育之父」
  - [羅納德·羅斯](../Page/羅納德·羅斯.md "wikilink")，1902年[諾貝爾生理學或醫學獎](../Page/諾貝爾生理學或醫學獎.md "wikilink")
  - [埃德加·阿德里安](../Page/埃德加·阿德里安.md "wikilink")，1932年諾貝爾生理學或醫學獎
  - [約翰·范恩](../Page/約翰·范恩.md "wikilink")，1982年諾貝爾生理學或醫學獎
  - [約瑟夫·羅特布拉特教授](../Page/約瑟夫·羅特布拉特.md "wikilink")，1995年[諾貝爾和平獎](../Page/諾貝爾和平獎.md "wikilink")
  - [彼得·曼斯菲爾德教授](../Page/彼得·曼斯菲爾德.md "wikilink")，2003年諾貝爾生理學或醫學獎
  - [费孝通](../Page/费孝通.md "wikilink")：第七、八屆全國人民代表大會常務委員會副委員長，中國人民政治協商會議第六屆全國委員會副主席
  - [孟憲承](../Page/孟憲承.md "wikilink")：[華東師範大學首任校長](../Page/華東師範大學.md "wikilink")
  - [謝忻](../Page/謝忻.md "wikilink")：台灣綜藝節目主持人
  - [劉攻芸](../Page/劉攻芸.md "wikilink")：中華民國前[財政部長](../Page/財政部長.md "wikilink")、前[中央銀行總裁](../Page/中央銀行.md "wikilink")
  - [周德偉](../Page/周德偉.md "wikilink")：中華民國財政部關務署署長
  - [俞國華](../Page/俞國華.md "wikilink")：中華民國前[財政部長](../Page/財政部長.md "wikilink")、前[中央銀行總裁](../Page/中央銀行.md "wikilink")、前[行政院院長](../Page/行政院.md "wikilink")
  - [蔡英文](../Page/蔡英文.md "wikilink")：第14任[台灣總統](../Page/台灣.md "wikilink")、前[民主進步黨黨主席](../Page/民主進步黨.md "wikilink")、前[行政院經濟部國際經濟組織首席法律顧問](../Page/行政院.md "wikilink")、前[行政院大陸委員會主任委員](../Page/行政院大陸委員會.md "wikilink")、前全國不分區[立法委員](../Page/立法委員.md "wikilink")、前行政院副院長
  - [賴幸媛](../Page/賴幸媛.md "wikilink")：前行政院陸委會主委、前全國不分區[立法委員](../Page/立法委員.md "wikilink")
  - [李坤儀](../Page/李坤儀.md "wikilink")：前中華民國總統[李登輝之長孫女](../Page/李登輝.md "wikilink")
  - [杭立武](../Page/杭立武.md "wikilink")：中華民國教育家、政治家、外交家、社會活動家
  - [楊念祖](../Page/楊念祖.md "wikilink")：前中華民國[國防部部長](../Page/國防部.md "wikilink")
  - 英國地質學家[查爾斯·萊爾](../Page/查爾斯·萊爾.md "wikilink")
  - 英國科學家，發明電報[Charles
    Wheatsone](../Page/Charles_Wheatsone.md "wikilink")（[韦特斯通](../Page/韦特斯通.md "wikilink")）
  - 英國理論物理與數學家[麦克斯韦](../Page/麦克斯韦.md "wikilink")
  - 英國物理化學家[羅莎林·富蘭克林](../Page/羅莎林·富蘭克林.md "wikilink")
  - 英國外科與解剖學家[Astley Cooper](../Page/Astley_Cooper.md "wikilink")
  - 英國浪漫主義詩人[約翰·濟慈](../Page/約翰·濟慈.md "wikilink")
  - 英國作家，Booker獎得主[Anita Brookner](../Page/Anita_Brookner.md "wikilink")
  - [賽普勒斯總統](../Page/賽普勒斯.md "wikilink")[塔索斯·帕帕佐普洛斯](../Page/塔索斯·帕帕佐普洛斯.md "wikilink")
  - [賽普勒斯總統](../Page/賽普勒斯.md "wikilink")[格拉夫科斯·克莱里季斯](../Page/格拉夫科斯·克莱里季斯.md "wikilink")
  - [約旦總理](../Page/約旦.md "wikilink")[马鲁夫·巴希特](../Page/马鲁夫·巴希特.md "wikilink")
  - [伊拉克總理Abd](../Page/伊拉克.md "wikilink") ar-Rahman al-Bazzaz
  - [烏干達總統Godfrey](../Page/烏干達.md "wikilink") Binaisa
  - [巴哈馬總理Lynden](../Page/巴哈馬.md "wikilink") Pindling
  - [加納總督William](../Page/加納.md "wikilink") Hare, 5th Earl of Listowel
  - [加拿大副總理Anne](../Page/加拿大.md "wikilink") McLellan
  - [新加坡副總理S](../Page/新加坡.md "wikilink"). Rajaratnam
  - 英國外交大臣[大衛·歐文](../Page/大衛·歐文.md "wikilink")
  - 英國内閣部長Lord Passfield
  - 英國曼彻斯特大学校长Dame Nancy Rothwell
  - [黄纬禄](../Page/黄纬禄.md "wikilink")：中国导弹与航天技术的主要开拓者之一，两弹一星功勋奖章获得者。
  - [林建康](../Page/林建康.md "wikilink"): 香港企業家、香港麗新集團董事
  - [郭炳湘](../Page/郭炳湘.md "wikilink"): 香港企業家
  - [刘伟强](../Page/刘伟强.md "wikilink")：
    [马来西亚首相署部长](../Page/马来西亚首相署.md "wikilink")（负责掌管法律事务）

## 参考文献

## 外部链接

  - [伦敦大学网址](https://web.archive.org/web/20101206112952/http://www.lon.ac.uk/)
  - [伦敦大学國際課程](http://www.londoninternational.ac.uk/)

[Category:伦敦大学](../Category/伦敦大学.md "wikilink")
[Category:1836年創建的教育機構](../Category/1836年創建的教育機構.md "wikilink")
[Category:英格蘭大學](../Category/英格蘭大學.md "wikilink")