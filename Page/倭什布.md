**倭什布**（；），**[瓜爾佳氏](../Page/瓜爾佳氏.md "wikilink")**，[清朝政治人物](../Page/清朝.md "wikilink")。

## 生平

[乾隆年間由](../Page/乾隆.md "wikilink")[戶部](../Page/戶部.md "wikilink")[筆帖式外任](../Page/筆帖式.md "wikilink")[四川](../Page/四川.md "wikilink")[理事同知](../Page/理事同知.md "wikilink")，升[保寧府](../Page/保寧府.md "wikilink")[知府](../Page/知府.md "wikilink")。乾隆五十二年（1787年），調任[成都府知府](../Page/成都府.md "wikilink")。五十三年（1788年），升任四川[松茂道](../Page/松茂道.md "wikilink")。五十八年（1793年），擢[廣東](../Page/廣東.md "wikilink")[按察使](../Page/按察使.md "wikilink")。五十九年（1794年），擢[陝西](../Page/陝西.md "wikilink")[布政使](../Page/布政使.md "wikilink")。六十年（1795年），以按察使護理[陝西巡撫](../Page/陝西巡撫.md "wikilink")。[嘉慶元年](../Page/嘉慶.md "wikilink")（1796年）[丁憂](../Page/丁憂.md "wikilink")，同年服闕後署理陝西布政使，兼署陝西按察使。二年（1797年），擢[山西巡撫](../Page/山西巡撫.md "wikilink")。三年（1798年），調任[河南巡撫](../Page/河南巡撫.md "wikilink")。四年（1799年），擢[湖廣總督](../Page/湖廣總督.md "wikilink")。同年因功受賞[雲騎尉世職](../Page/雲騎尉.md "wikilink")。五年（1800年），降為[湖北巡撫](../Page/湖北巡撫.md "wikilink")。六年（1801年），再任湖廣總督，隨即因案降職筆帖式。七年（1802年），授[山東布政使](../Page/山東.md "wikilink")，隨即擢[山東巡撫](../Page/山東巡撫.md "wikilink")。八年（1803年），升調[兩廣總督](../Page/兩廣總督.md "wikilink")。九年（1804年），調[陝甘總督](../Page/陝甘總督.md "wikilink")。十一年（1806年），因案革職，復授筆帖式銜[二等侍衛](../Page/二等侍衛.md "wikilink")，充[科布多參贊大臣](../Page/科布多參贊大臣.md "wikilink")。

## 參考文獻

  - [清史稿](../Page/清史稿.md "wikilink")

[Category:清朝四川理事同知](../Category/清朝四川理事同知.md "wikilink")
[Category:清朝保寧府知府](../Category/清朝保寧府知府.md "wikilink")
[Category:清朝成都府知府](../Category/清朝成都府知府.md "wikilink")
[Category:清朝松茂道](../Category/清朝松茂道.md "wikilink")
[Category:清朝廣東按察使](../Category/清朝廣東按察使.md "wikilink")
[Category:清朝陝西布政使](../Category/清朝陝西布政使.md "wikilink")
[Category:清朝山西巡撫](../Category/清朝山西巡撫.md "wikilink")
[Category:清朝河南巡撫](../Category/清朝河南巡撫.md "wikilink")
[Category:清朝湖廣總督](../Category/清朝湖廣總督.md "wikilink")
[Category:清朝湖北巡撫](../Category/清朝湖北巡撫.md "wikilink")
[Category:清朝山東布政使](../Category/清朝山東布政使.md "wikilink")
[Category:清朝山東巡撫](../Category/清朝山東巡撫.md "wikilink")
[Category:清朝兩廣總督](../Category/清朝兩廣總督.md "wikilink")
[Category:清朝陝甘總督](../Category/清朝陝甘總督.md "wikilink")
[Category:侍衛處二等侍衛](../Category/侍衛處二等侍衛.md "wikilink")
[Category:科布多參贊大臣](../Category/科布多參贊大臣.md "wikilink")
[Category:瓜爾佳氏](../Category/瓜爾佳氏.md "wikilink")
[Category:滿洲旗人](../Category/滿洲旗人.md "wikilink")