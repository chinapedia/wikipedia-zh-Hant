**倭松鼠**（***Microsciurus***）是松鼠中的一屬，生活於[中美洲與](../Page/中美洲.md "wikilink")[南美洲的](../Page/南美洲.md "wikilink")[熱帶區域](../Page/熱帶.md "wikilink")。共有四個物種：

  - [中美倭松鼠](../Page/中美倭松鼠.md "wikilink") *Microsciurus alfari*
  - [西方倭松鼠](../Page/西方倭松鼠.md "wikilink") *Microsciurus mimulus*
  - [黄腹倭松鼠](../Page/黄腹倭松鼠.md "wikilink") *Microsciurus flaviventer*
  - [聖塔倭松鼠](../Page/聖塔倭松鼠.md "wikilink") *Microsciurus santanderensis*

## 參考文獻

  - Ronald M. Nowak: *Walker's Mammals of the World*. Johns Hopkins
    University Press, 1999 ISBN 0-8018-5789-9

[Category:倭松鼠屬](../Category/倭松鼠屬.md "wikilink")