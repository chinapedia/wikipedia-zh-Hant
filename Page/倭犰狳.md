**倭犰狳**（學名：**），也叫**粉毛犰狳**，是[犰狳科最小的一種](../Page/犰狳科.md "wikilink")。體長90-115毫米，不包括尾巴，體色為玫瑰色或粉紅色。生活在[阿根廷中部的乾燥草地](../Page/阿根廷.md "wikilink")、帶刺灌木和[仙人掌的沙地](../Page/仙人掌.md "wikilink")。

倭犰狳主要以[螞蟻為食](../Page/螞蟻.md "wikilink")，偶爾也吃蠕蟲、蝸牛、昆蟲以及各種植物性食物。

## 參考

## 外部連結

  - [Species profile from the U.S. Fish & Wildlife
    Service](https://web.archive.org/web/20040825150842/http://ecos.fws.gov/species_profile/SpeciesProfile?spcode=A00L)
  - [Genus *Chlamyphorus* - Armadillo
    Online](http://www.msu.edu/~nixonjos/armadillo/chlamyphorus.html)

[CT](../Category/IUCN數據缺乏物種.md "wikilink")
[truncatus](../Category/倭犰狳屬.md "wikilink")
[C](../Category/单型的哺乳动物.md "wikilink")