**倭狐猴**（学名 *Microcebus
murinus*）是[倭狐猴属的一种](../Page/倭狐猴属.md "wikilink")。目前为止是最小的一种[灵长目动物](../Page/灵长目.md "wikilink")。它们主要分布在[马达加斯加岛西南部的树林中](../Page/马达加斯加.md "wikilink")。

倭狐猴体毛为红灰色，腹部为白色。它们体长10-14厘米，尾长10-15厘米。体重40-70克。这是一种在夜晚活动的动物，栖息在树的低处，公猴一般单独行动，母猴则会组成一个小的群体。它们主要以水果、花、[昆虫等为食](../Page/昆虫.md "wikilink")。

倭狐猴的孕期为54-69天，每胎2-4仔。平均寿命约为15年。

## 参考

  -
  - [BBC](http://www.bbc.co.uk/nature/wildfacts/factfiles/324.shtml)

[MM](../Category/IUCN無危物種.md "wikilink")
[murinus](../Category/鼠狐猴科.md "wikilink")