**假貓**（*Pseudaelurus*）是生存於約2000-1000萬年前[歐洲](../Page/歐洲.md "wikilink")\[1\]及[北美洲的史前](../Page/北美洲.md "wikilink")[貓科動物](../Page/貓科.md "wikilink")。牠們是現今[家貓](../Page/家貓.md "wikilink")、大型貓科及[劍齒虎亞科的祖先](../Page/劍齒虎亞科.md "wikilink")，並且是[原小熊貓的後裔](../Page/原小熊貓.md "wikilink")。\[2\]假貓的體形纖幼及短，四肢像[靈貓科](../Page/靈貓科.md "wikilink")\[3\]，估計牠們是靈活的攀樹者。\[4\]

假貓的體型大約有[美洲獅的大小](../Page/美洲獅.md "wikilink")。

## 參考

## 外部連結

  - [*Pseudaelurus* at the Paleobiology
    Database](http://paleodb.org/cgi-bin/bridge.pl?action=checkTaxonInfo&taxon_no=41072&is_real_user=0)

[Category:貓科](../Category/貓科.md "wikilink")
[Category:歐洲史前哺乳動物](../Category/歐洲史前哺乳動物.md "wikilink")
[Category:北美洲史前哺乳動物](../Category/北美洲史前哺乳動物.md "wikilink")

1.
2.
3.
4.