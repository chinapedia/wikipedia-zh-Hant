**偉鬣獸**（*Megistotherium*）是巨型的[肉齒目](../Page/肉齒目.md "wikilink")，生存於約2400萬年前[中新世早期](../Page/中新世.md "wikilink")，是當時最大型的陸上[肉食性](../Page/肉食性.md "wikilink")[哺乳動物之一](../Page/哺乳動物.md "wikilink")。

牠們的[化石是在](../Page/化石.md "wikilink")[埃及](../Page/埃及.md "wikilink")\[1\]及[利比亞](../Page/利比亞.md "wikilink")\[2\]發現，肯定是[非洲最大的](../Page/非洲.md "wikilink")[掠食者](../Page/掠食者.md "wikilink")。牠們比[美洲野牛還要大](../Page/美洲野牛.md "wikilink")，超過5-6米長及重約1500公斤\[3\]，[頭顱骨估計超過](../Page/頭顱骨.md "wikilink")1米長。偉鬣獸有可能與[蒙古安氏中獸互相競爭](../Page/蒙古安氏中獸.md "wikilink")。偉鬣獸是肉齒目中體型最大的。

偉鬣獸的[裂肉齒是在第一](../Page/裂肉齒.md "wikilink")[臼齒](../Page/臼齒.md "wikilink")，及與下臼齒重疊成像剪刀般，可產生利害及強大的剪力。雖然牠的體型很大及笨重，牠可以獵食大型的獵物。偉鬣獸有時會吃腐肉。於中新世時[撒哈拉沙漠是較為肥沃](../Page/撒哈拉沙漠.md "wikilink")，大部份地方都是[草原及有豐富的雨量](../Page/草原.md "wikilink")，且有湖及池提供水份予動物群。偉鬣獸及其他的掠食者都有足夠的獵物供應。在偉鬣獸的化石附近亦發現了[乳齒象的骨頭](../Page/乳齒象.md "wikilink")，顯示牠們可能是獵食乳齒象的。\[4\]

偉鬣獸的近親是*Hyainailouros*，在體型及結構上都很相似，如有長的尾巴、短的四肢及粗壯的軀體，並且有可能是屬於同一[屬的](../Page/屬.md "wikilink")。

## 參考

[Category:鬣齒獸科](../Category/鬣齒獸科.md "wikilink")

1.
2.
3.
4.