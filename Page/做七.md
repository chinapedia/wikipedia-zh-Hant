**做七**，又称**作七、作旬**、**烧七**、**斋七**、**理七**，是[東亞的喪殯習俗](../Page/東亞.md "wikilink")，指人死后每隔七天舉行法事一次，七天为一期，最多为七期，七七四十九天才结束。\[1\]道教稱之為**七七追薦，**
[佛教稱之為](../Page/佛教.md "wikilink")**七七齋**、**七七忌**、**累七齋**、**七七日**、**齋七日**。

“七七”为最后一个“七”，称“**断七**”、**尾七**、**滿七**或**圓七**\[2\]。

## 起源

超度亡魂起源于六朝道經中吳葛玄纂集《太上慈悲道場消災九幽懺》十卷及《洞玄靈寶三洞奉道科戒營始》所見的齋儀，以深度懺悔為主，來消災度厄及超度亡魂。而做七的起源最早是出至于南朝的《太上洞玄靈寶業報因緣經卷之八》《生神品第十九》\[3\]。故民間傳說，地府是天庭下轄負責管理人間事務，共有十殿掌管不同事務。一旦人死後第七天，將來到地府第一殿接受審判，如此每逢[做七](../Page/做七.md "wikilink")、百日、對年、三年之期，依序送往地府各殿受審，民間謂為「過王官」、「參詳十王」。\[4\]

北魏時佛教借鑑道教的「七七追薦」改為「七七齋」。《北史．外戚傳》記載外戚胡國珍去世後，北魏孝明帝在七七日中為他設千僧齋，並度七人出家為僧。可知這樣的禮俗應是從佛法的輪迴觀念而來。

民間相傳，此時若無繼嗣為其[作功德](../Page/作功德.md "wikilink")、積[冥福](../Page/冥福.md "wikilink")，在生時又無種下善因，將審定其生前罪業，打入地獄受報；所以，陽世的親屬每隔七日為亡者辦法事、修功德，謂之「七七齋」或「七七追薦」。\[5\]
五代時期開始(一說為唐後期或為宋代)有傳說指[目連尊者暨治下四大判官負責計算死者](../Page/目犍連.md "wikilink")「七七日內，所修佛事[檀施](../Page/布施.md "wikilink")」之功德。

依佛教而言，人死後離魂被稱為「中陰身」，而中陰身的壽命只有七天，那是因為中陰身非常的孱弱，過了七天就會消滅；但是，如果投生到下一生的因緣尚未具足時，中陰身就會死而復生，繼續等待投生的因緣。中陰身在下一生的投生機緣尚未成熟時，會四處尋找有緣的父母，以做為託生的憑藉。若沒有適當的因緣，便會經歷數次的生死，每七日為一期，七次為限，總共七七四十九天為極限，四十九天內一定會被業力牽引而投生，不能逗留。佛教傳[六道輪迴有天](../Page/六道.md "wikilink")、修羅、人、鬼、地獄、傍生。「天道」是大善少惡業者，方可無轉生。「人道」是少善少惡者。「鬼道」是犯重貪惡業者。「地獄道」是犯重嗔惡業者。「傍生道」是犯愚痴重業者。「修羅道」有如天之善者，確有大嗔業，故不得為天道，而生為修羅，在世可享如天之福，善業盡，則墮入地獄。

做七是為了鬼道眾生而作，鬼道是因貪惡業而生，故鬼道多貧苦飢餓，人間子孫若積福迴向，則可使鬼道眾生稍得福報，飽餐幾頓，暫免飢餓之苦。地府神職乃是由有善業的鬼道眾生，不受飢餓之苦，但還是有其他方面苦厄，善惡業是不能相抵的，都必須受報，因此雖有善業，但還是要轉世為鬼道眾生，直到惡業盡。若轉世為其餘五道，無法收受作七積福，不過積福本身也是為自己積福。輪迴轉世是由業力所決定，其為天地大道自發而成，公正無私，非一人所能執掌。

## 頭七

民眾習慣上認為「頭七」指的是人去世後的第六日晚上到第七日早晨。不同的地方有不同的「頭七」習俗，死者[魂魄會於](../Page/魂魄.md "wikilink")「頭七」返家，家人應於魂魄回來前，為死者魂魄預備一頓飯，燒香奉祀。

有人認為，家人之後便須迴避，最好的方法是睡覺，睡不著便躲入被窩；如果死者魂魄看見家人，會令他記掛，便影響他[投胎再世為人](../Page/投胎.md "wikilink")。

亦有人認為人死後魂魄會於「頭七」前到處飄蕩，到了「頭七」當天的子時回家，家人應於家中燒一個梯子形狀的東西，讓魂魄順著這「天梯」到天上。

[閩南人](../Page/閩南人.md "wikilink")、[臺灣人認為](../Page/臺灣人.md "wikilink")，[地府或](../Page/地府.md "wikilink")[城隍的事務繁忙](../Page/城隍.md "wikilink")，除非要即刻審訊此人，否則[黑白無常或](../Page/黑白無常.md "wikilink")[牛頭馬面直接來勾魂的對象較少](../Page/牛頭馬面.md "wikilink")，多數人病死時，渾渾噩噩，沒有[黑白無常接引](../Page/黑白無常.md "wikilink")，所以前六日都不確定自己死了，還四處遊蕩，一心一意要回到自己的軀體，直到第六日的亥時過後，[土地公出現向死者說](../Page/土地公.md "wikilink")：「你已經死了，我帶你回家一趟，看看家人之後，就要去見[十殿閻君](../Page/十殿閻君.md "wikilink")。」通常亡魂不會相信土地公說的話，土地公就會帶亡魂去洗手，亡魂一洗手，發現指甲全部變黑，並且脫落，儼然屍體的樣子，這才相信自己已經死了，然後死者會開始痛哭。此時土地公也會帶著亡魂回家，檢視家中的情況，土地公會帶亡魂到所屬的[城隍廟去報到](../Page/城隍廟.md "wikilink")，[城隍爺初審](../Page/城隍爺.md "wikilink")，並註銷此人陽世的戶籍後，由[牛頭馬面或](../Page/牛頭馬面.md "wikilink")[黑白無常押送到](../Page/黑白無常.md "wikilink")[地府去](../Page/地府.md "wikilink")，面見第一殿[秦廣王](../Page/秦廣王.md "wikilink")。

如果為了細故自殺或者意外[枉死者非常悲慘](../Page/枉死.md "wikilink")，通常土地公也無法前來接引，一種是被當地的風水所困，困在枉死的地方，成為地縛靈，每天重複一次枉死的動作，可能需要[替身代替自己](../Page/替身.md "wikilink")，才能離去。另一種就是被[黑白無常等鬼差勾至地府](../Page/黑白無常.md "wikilink")，集中至[枉死城](../Page/枉死城.md "wikilink")。所以[閩南](../Page/閩南.md "wikilink")、[臺灣的風俗](../Page/臺灣.md "wikilink")，特別重視枉死者的頭七，為其[做功德](../Page/做功德.md "wikilink")，希望枉死者不再痛苦，獲得[冥福](../Page/冥福.md "wikilink")。

## 七七

  - 自隋唐開始，民間在人死後每過七天就舉行一次奠禮，或者頌經設齋，或者建醮拜懺，以追薦亡靈，民間簡稱為“做七”。
  - 道教認為人死後第七天來到地府十殿的第一殿接受審判，如此每逢七日、百日、對年、三年之期，依序送往地府各殿受審，民間謂為「過王官」、「參詳十王」。
    以江浙地區為例： 一七，即死後七日，喪家舉行隆重儀式，設另外、供木主，上香叩拜，燒紙箱，請僧道誦經、拜懺。
  - 舊時杭州的做七風俗是這樣的
      - “一七”一般叫“頭七”或首七，通常在第六天就開始，據《杭俗遺風》上說：做七須在第六日上，故名曰“敲六頭兒”，用土地廟的和尚來做法事，配合音樂。
      - 二七，祭禮從簡，這天，家人備酒饌，供羹飯祭奠，燒紙楮。
      - 三七，亦稱“散七”，這夜，孝子擎香火，到三岔路口呼喊亡人姓名或稱謂，或上墳焚香接亡靈回家，家中設奠。
      - 四七，祭禮從簡，這天，家人備酒饌、供羹飯、焚紙楮進行祭奠。
      - 五七，在七七中，五七祭儀尤重，這天，喪家舉行祭奠，焚楮燒紙，請僧人、道士放焰口。親友也攜紙錢、錫箔元寶（也有送現金）助祭，喪家要辦酒席招待。有的紮紙紮，焚祭亡靈，紙紮有樓閣房宅，內置錫箔元寶；有金山、銀山，山上飾草木鳥獸，祭禮時，將這些紙紮拿到墓前焚化。有的地方這天出嫁女兒挑酒食回娘家祭奠。
      - 六七，祭禮從簡。
      - 七七，“七七”又稱“滿七”、“尾七”等，儀式略同“頭七”。有的地方又稱“七七”為“起服”，即除去孝服，換上吉服。
  - 舉行道場的時間間隔一般最少要七天，最多要做到四十九天為止，也就是說要每隔七天做一次，一共要做七次。
  - 到了宋代以後，民間佛教也為人設道場做七。解釋是，人死後的中陰身，四十九天之內會再度轉生到六道之中。

## 作旬

[漢傳佛教相信](../Page/漢傳佛教.md "wikilink")，[十殿閻羅與](../Page/十殿閻羅.md "wikilink")[目連尊者會在人死後計算其一生的善惡](../Page/目連尊者.md "wikilink")，以給予[報應並決定投生](../Page/報應.md "wikilink")[六道](../Page/六道.md "wikilink")，[閩南人傳說](../Page/閩南人.md "wikilink")，「頭七」到「尾七」是[作功德的最佳時機](../Page/作功德.md "wikilink")，到「尾七」後，要由[目連尊者部下的查察司判官](../Page/目連.md "wikilink")[李玄邃](../Page/李玄邃.md "wikilink")、賞善司判官[楊玄感](../Page/楊玄感.md "wikilink")、罰惡司判官[韓子通](../Page/韓子通.md "wikilink")、陰律司判官[崔子玉等四大](../Page/崔府君.md "wikilink")[判官複審](../Page/判官.md "wikilink")，並由目連尊者判斷死者獲得了多少[冥福](../Page/冥福.md "wikilink")，故富貴[門閥多在](../Page/門閥.md "wikilink")「**尾七**」（「第四十九日」）之後，每一旬（十日）舉行法事一次，稱「作旬」，以祈禱於四判官與[目連尊者](../Page/目連.md "wikilink")。

第五十九日為「初旬」，由陰律司崔判官複審；第六十九日為「二旬」，由查察司李判官複審；第七十九日為「三旬」，由賞善司楊判官複審；第八十九日為「四旬」，由罰惡司韓判官複審；第九十九日為「五旬」，由目連尊者親自複審，但「五旬」通常會與「百日」合併辦理。

[清治時期起](../Page/清治臺灣.md "wikilink")，[臺灣人通常只作法事至](../Page/臺灣漢人.md "wikilink")「尾七」即止，而不「作旬」，直接作「百日」，故[臺灣本省漢族則直接稱](../Page/臺灣漢人.md "wikilink")「作七」為「**作旬**」。

### 圖表

<table>
<thead>
<tr class="header">
<th><p>名號</p></th>
<th><p>畫像</p></th>
<th><p>過王</p></th>
<th><p>簡述與懲處 </p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第一殿秦廣王<a href="../Page/蔣子文.md" title="wikilink">蔣</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:一七秦廣大王.jpg" title="fig:链接=https://zh.wikipedia.org/wiki/File:%E4%B8%80%E4%B8%83%E7%A7%A6%E5%BB%A3%E5%A4%A7%E7%8E%8B.jpg">链接=<a href="https://zh.wikipedia.org/wiki/File:%E4%B8%80%E4%B8%83%E7%A7%A6%E5%BB%A3%E5%A4%A7%E7%8E%8B.jpg">https://zh.wikipedia.org/wiki/File:%E4%B8%80%E4%B8%83%E7%A7%A6%E5%BB%A3%E5%A4%A7%E7%8E%8B.jpg</a></a></p></td>
<td><p>頭七</p></td>
<td><p>姓蔣，民間傳說是東漢時期的<a href="../Page/蔣子文.md" title="wikilink">蔣子文</a>，誕辰農曆二月初一日（一說是農曆二月初二日）。 本殿位居大海之中、沃燋石之外，正西方的黃泉黑路上，專司人間壽夭生死冊籍，管理陰間受刑吉凶。陰魂來到本殿，引上殿右的<a href="../Page/業鏡.md" title="wikilink">孽鏡臺</a>，凡屬善人者，令接引往生天堂或極樂世界；善惡參半者，按其業力送交十殿投胎轉世；惡多善少者，押送其餘各殿審判，令受各種刑罰。</p></td>
</tr>
<tr class="even">
<td><p>第二殿楚江王厲（或歷）</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:二七初江大王.jpg" title="fig:链接=https://zh.wikipedia.org/wiki/File:%E4%BA%8C%E4%B8%83%E5%88%9D%E6%B1%9F%E5%A4%A7%E7%8E%8B.jpg">链接=<a href="https://zh.wikipedia.org/wiki/File:%E4%BA%8C%E4%B8%83%E5%88%9D%E6%B1%9F%E5%A4%A7%E7%8E%8B.jpg">https://zh.wikipedia.org/wiki/File:%E4%BA%8C%E4%B8%83%E5%88%9D%E6%B1%9F%E5%A4%A7%E7%8E%8B.jpg</a></a></p></td>
<td><p>二七</p></td>
<td><p>姓厲（或歷），誕辰農曆三月初一日，亦稱「初江王」。 本殿掌管大海之底、正南方沃燋石下的<a href="../Page/活大地獄.md" title="wikilink">活大地獄</a>，此獄寬廣八千里（五百由旬），並另設十六小地獄，專司在陽間欺佔拐騙、傷人肢體、奸盜殺生等罪。</p></td>
</tr>
<tr class="odd">
<td><p>第三殿宋帝王余</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:三七宋帝大王.jpg" title="fig:链接=https://zh.wikipedia.org/wiki/File:%E4%B8%89%E4%B8%83%E5%AE%8B%E5%B8%9D%E5%A4%A7%E7%8E%8B.jpg">链接=<a href="https://zh.wikipedia.org/wiki/File:%E4%B8%89%E4%B8%83%E5%AE%8B%E5%B8%9D%E5%A4%A7%E7%8E%8B.jpg">https://zh.wikipedia.org/wiki/File:%E4%B8%89%E4%B8%83%E5%AE%8B%E5%B8%9D%E5%A4%A7%E7%8E%8B.jpg</a></a></p></td>
<td><p>三七</p></td>
<td><p>姓余，誕辰農曆二月初八日。 本殿掌管大海之底、東南方沃燋石下的<a href="../Page/黑繩大地獄.md" title="wikilink">黑繩大地獄</a>，此獄寬廣八千里（五百由旬），並另設十六小地獄，專司在陽間忤逆尊長、背信棄義、教唆興訟等罪。</p></td>
</tr>
<tr class="even">
<td><p>第四殿五官王呂</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:四七五官大王.jpg" title="fig:链接=https://zh.wikipedia.org/wiki/File:%E5%9B%9B%E4%B8%83%E4%BA%94%E5%AE%98%E5%A4%A7%E7%8E%8B.jpg">链接=<a href="https://zh.wikipedia.org/wiki/File:%E5%9B%9B%E4%B8%83%E4%BA%94%E5%AE%98%E5%A4%A7%E7%8E%8B.jpg">https://zh.wikipedia.org/wiki/File:%E5%9B%9B%E4%B8%83%E4%BA%94%E5%AE%98%E5%A4%A7%E7%8E%8B.jpg</a></a></p></td>
<td><p>四七</p></td>
<td><p>姓呂，誕辰農曆二月十八日，亦稱「伍官王」、「仵官王」。 本殿掌管大海之底、正東方沃燋石下的<a href="../Page/合大地獄.md" title="wikilink">合大地獄</a>，此獄寬廣八千里（五百由旬），並另設十六小地獄，專司在陽間瞞稅不繳、耍賴欠租、交易欺詐等罪。</p></td>
</tr>
<tr class="odd">
<td><p>第五殿閻羅天子<a href="../Page/包拯.md" title="wikilink">包</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:五七閻羅大王.jpg" title="fig:链接=https://zh.wikipedia.org/wiki/File:%E4%BA%94%E4%B8%83%E9%96%BB%E7%BE%85%E5%A4%A7%E7%8E%8B.jpg">链接=<a href="https://zh.wikipedia.org/wiki/File:%E4%BA%94%E4%B8%83%E9%96%BB%E7%BE%85%E5%A4%A7%E7%8E%8B.jpg">https://zh.wikipedia.org/wiki/File:%E4%BA%94%E4%B8%83%E9%96%BB%E7%BE%85%E5%A4%A7%E7%8E%8B.jpg</a></a></p></td>
<td><p>五七</p></td>
<td><p>姓包，民間傳說是北宋時期的<a href="../Page/包拯.md" title="wikilink">包拯</a>，誕辰農曆正月初八日（一說是農曆六月廿八日），亦稱「閻羅王」、「森羅王」。 本殿掌管大海之底、東北方沃燋石下的<a href="../Page/叫喚大地獄.md" title="wikilink">叫喚大地獄</a>，並另設十六誅心小地獄，專司在陽間不信因果、阻人行善、誹僧謗道等罪。陰魂來到本殿，可以登上望鄉臺，眺望家中情況，一解思鄉之苦。</p></td>
</tr>
<tr class="even">
<td><p>第六殿卞城王畢</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:六七變成王.jpg" title="fig:链接=https://zh.wikipedia.org/wiki/File:%E5%85%AD%E4%B8%83%E8%AE%8A%E6%88%90%E7%8E%8B.jpg">链接=<a href="https://zh.wikipedia.org/wiki/File:%E5%85%AD%E4%B8%83%E8%AE%8A%E6%88%90%E7%8E%8B.jpg">https://zh.wikipedia.org/wiki/File:%E5%85%AD%E4%B8%83%E8%AE%8A%E6%88%90%E7%8E%8B.jpg</a></a></p></td>
<td><p>六七</p></td>
<td><p>姓畢，誕辰農曆三月初八日，亦稱「變成王」、「變性王」、「變城王」。 本殿掌管大海之底、正北方沃燋石下的<a href="../Page/大叫喚大地獄.md" title="wikilink">大叫喚大地獄</a>，此獄寬廣八千里（五百由旬），並另設十六小地獄，專司在陽間怨天尤人、對天溺便、不敬神佛等罪。</p></td>
</tr>
<tr class="odd">
<td><p>第七殿泰山王董</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:七七泰山大王.jpg" title="fig:链接=https://zh.wikipedia.org/wiki/File:%E4%B8%83%E4%B8%83%E6%B3%B0%E5%B1%B1%E5%A4%A7%E7%8E%8B.jpg">链接=<a href="https://zh.wikipedia.org/wiki/File:%E4%B8%83%E4%B8%83%E6%B3%B0%E5%B1%B1%E5%A4%A7%E7%8E%8B.jpg">https://zh.wikipedia.org/wiki/File:%E4%B8%83%E4%B8%83%E6%B3%B0%E5%B1%B1%E5%A4%A7%E7%8E%8B.jpg</a></a></p></td>
<td><p>七七</p></td>
<td><p>姓董，誕辰農曆三月廿七日，亦稱「太山王」。 本殿掌管大海之底、西北方沃燋石下的<a href="../Page/熱惱大地獄.md" title="wikilink">熱惱大地獄</a>，此獄寬廣八千里（五百由旬），並另設十六小地獄，專司在陽間取屍骸做藥、離散他人至親、搬弄是非等罪。</p></td>
</tr>
<tr class="even">
<td><p>第八殿都市王黃</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:周年都市大王.jpg" title="fig:链接=https://zh.wikipedia.org/wiki/File:%E5%91%A8%E5%B9%B4%E9%83%BD%E5%B8%82%E5%A4%A7%E7%8E%8B.jpg">链接=<a href="https://zh.wikipedia.org/wiki/File:%E5%91%A8%E5%B9%B4%E9%83%BD%E5%B8%82%E5%A4%A7%E7%8E%8B.jpg">https://zh.wikipedia.org/wiki/File:%E5%91%A8%E5%B9%B4%E9%83%BD%E5%B8%82%E5%A4%A7%E7%8E%8B.jpg</a></a></p></td>
<td><p>百日</p></td>
<td><p>姓黃，誕辰農曆四月初一日，亦稱「都帝王」、「都弔王」。 本殿掌管大海之底、正西方沃燋石下的<a href="../Page/大熱惱大地獄.md" title="wikilink">大熱惱大地獄</a>，此獄寬廣八千里（五百由旬），並另設十六小地獄，專司在陽間不孝父母翁姑等罪。</p></td>
</tr>
<tr class="odd">
<td><p>第九殿平等王陸</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:百日平等大王.jpg" title="fig:链接=https://zh.wikipedia.org/wiki/File:%E7%99%BE%E6%97%A5%E5%B9%B3%E7%AD%89%E5%A4%A7%E7%8E%8B.jpg">链接=<a href="https://zh.wikipedia.org/wiki/File:%E7%99%BE%E6%97%A5%E5%B9%B3%E7%AD%89%E5%A4%A7%E7%8E%8B.jpg">https://zh.wikipedia.org/wiki/File:%E7%99%BE%E6%97%A5%E5%B9%B3%E7%AD%89%E5%A4%A7%E7%8E%8B.jpg</a></a></p></td>
<td><p>一年 （周歲）</p></td>
<td><p>姓陸，誕辰農曆四月初八日，亦稱「平正王」、「平政王」。 本殿掌管大海之底、西南方沃燋石下的<a href="../Page/阿鼻地獄.md" title="wikilink">阿鼻大地獄</a>，此獄鐵網重疊圍繞，寬廣八千里（五百由旬），並另設十六小地獄，專司在陽間殺人放火、強姦犯毒等極惡之罪。</p></td>
</tr>
<tr class="even">
<td><p>第十殿轉輪王薛</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:三年五道輪轉大王.jpg" title="fig:链接=https://zh.wikipedia.org/wiki/File:%E4%B8%89%E5%B9%B4%E4%BA%94%E9%81%93%E8%BC%AA%E8%BD%89%E5%A4%A7%E7%8E%8B.jpg">链接=<a href="https://zh.wikipedia.org/wiki/File:%E4%B8%89%E5%B9%B4%E4%BA%94%E9%81%93%E8%BC%AA%E8%BD%89%E5%A4%A7%E7%8E%8B.jpg">https://zh.wikipedia.org/wiki/File:%E4%B8%89%E5%B9%B4%E4%BA%94%E9%81%93%E8%BC%AA%E8%BD%89%E5%A4%A7%E7%8E%8B.jpg</a></a></p></td>
<td><p>三年 （大祥）</p></td>
<td><p>姓薛，誕辰農曆四月十七日，亦稱「五道轉輪王」、「輪轉王」。 本殿位居陰間沃燋石之外，正東方直對五濁世界的地方，設有金橋、銀橋、玉橋、石橋、木橋和竹橋，專司地府各殿押解到鬼魂，核查註冊；然後押送到醧忘臺飲<a href="../Page/孟婆湯.md" title="wikilink">孟婆湯</a>，再經轉輪臺發往<a href="../Page/四大部洲.md" title="wikilink">四大部洲投胎</a>。同時，將這些投生的人詳細記載，每月彙報通知第一殿；註冊後，送呈酆都。</p>
<p>本殿並另設轉劫所，寬廣一萬一千二百里（七百由旬），合八十一處共八司，專司管理胎生、卵生、濕生、化生等類生靈，考查陽人在世所犯過錯，分發往畜道受報。每逢歲終彙集受報情況，呈交酆都備案。</p></td>
</tr>
</tbody>
</table>

## 相關条目

  - [送終](../Page/送終.md "wikilink")
  - [上廟](../Page/上廟.md "wikilink")
  - [報喪](../Page/報喪.md "wikilink")
  - [入殮](../Page/入殮.md "wikilink")
  - [送葬](../Page/送葬.md "wikilink")
  - [落葬](../Page/落葬.md "wikilink")
  - [除靈](../Page/除靈.md "wikilink")
  - [擔幡買水](../Page/擔幡買水.md "wikilink")
  - [帛金](../Page/帛金.md "wikilink")

## 參考資料

[Category:道教術語](../Category/道教術語.md "wikilink")
[Category:佛教術語](../Category/佛教術語.md "wikilink")
[Category:東亞傳統祭祀](../Category/東亞傳統祭祀.md "wikilink")
[Category:喪葬](../Category/喪葬.md "wikilink")
[Category:東亞民間信仰](../Category/東亞民間信仰.md "wikilink")
[Category:喪葬](../Category/喪葬.md "wikilink")
[Category:佛教名數7](../Category/佛教名數7.md "wikilink")

1.
2.
3.
4.
5.  《佛學大辭典》：「七七齋，（術語）人命終後未受報之間，是中有也，中有之壽命但極於七日而死，死而復生，未得生緣，則至七七日，七七日，罪業審定，方受其報。」