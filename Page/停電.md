[Calle_Larga_at_night_during_power_cut.jpg](https://zh.wikipedia.org/wiki/File:Calle_Larga_at_night_during_power_cut.jpg "fig:Calle_Larga_at_night_during_power_cut.jpg")電力危機期間，該國大城[昆卡夜間的街景](../Page/昆卡_\(厄瓜多\).md "wikilink")\]\]
[2003_New_York_City_blackout.jpg](https://zh.wikipedia.org/wiki/File:2003_New_York_City_blackout.jpg "fig:2003_New_York_City_blackout.jpg")時紐約市徒步回家的市民。\]\]

**停電**，又名**電力故障**、**電力事故**等。原因是[電力供應出意外](../Page/電源供應.md "wikilink")，或者有計劃施行。停電一般是指有一定的地區無電力供應。

## 類型

  - 計畫施行：由人員有計劃地切斷電力，可能是因為分區限電、電力設備維修、為報復或威脅他人等理由而切斷電力。
  - [自然災害](../Page/自然災害.md "wikilink")：[風暴](../Page/風暴.md "wikilink")、[火災](../Page/火災.md "wikilink")、[地震等令](../Page/地震.md "wikilink")[發電廠及供電設施](../Page/發電廠.md "wikilink")（電塔、[電線杆](../Page/電線.md "wikilink")、[電力變壓站](../Page/變電所.md "wikilink")）受損，或者[短路等](../Page/短路.md "wikilink")。如果[基礎設施大量受損](../Page/基礎設施.md "wikilink")，停電歷時要較久才能修復。
  - 人為疏忽

## 後果

有計劃的停電，廠家及市民有預備，可以安排後備[發電機](../Page/發電機.md "wikilink")、[電筒等](../Page/電筒.md "wikilink")。不過，意外停電則容易引發致命後果，例如[醫院](../Page/醫院.md "wikilink")[手術室](../Page/手術室.md "wikilink")、[航空管制系統](../Page/航空管制.md "wikilink")、[電腦](../Page/電腦.md "wikilink")[伺服器](../Page/伺服器.md "wikilink")、[升降機](../Page/升降機.md "wikilink")、[工廠](../Page/工廠.md "wikilink")[流水式生產線等](../Page/流水式生產.md "wikilink")。

在停電之後，突然回復輸送電力，又會產生意外，損壞電器，不可不防。

## 歷史上的大停電

  - [1965年北美大停電](../Page/1965年北美大停電.md "wikilink")（1965年11月9日） -
    北美約2500萬人受到影響。
  - [1971年九龍大停電](../Page/1971年九龍大停電.md "wikilink")（1971年8月16日） -
    香港的九龍、新九龍及新界全面停電。
  - [1977年紐約市大停電](../Page/1977年紐約市大停電.md "wikilink")（1977年7月13日-14日） -
    紐約市與臨近地區約900萬人受到影響。
  - [729全台大停電](../Page/729全台大停電.md "wikilink")（1999年7月29日） -
    台南以北約846萬人受到影響。
  - [加州電力危機](../Page/加州電力危機.md "wikilink")（2000年6月-2001年5月） -
    多次大小停電，其中2001年3月19日的停電影響約150萬人。
  - [2003年美加大停電](../Page/2003年美加大停電.md "wikilink")（2003年8月14日） -
    北美約5000萬人受到影響。
  - [厄瓜多電力危機](../Page/厄瓜多電力危機.md "wikilink")（2009年11月-2010年1月） -
    多次大小停電，全國70%地區受到直接影響。
  - [東日本大震災](../Page/東日本大震災.md "wikilink")（2011年3月11日）[東北地方](../Page/東北地方.md "wikilink")、[茨城县全境计划停电](../Page/茨城县.md "wikilink")，[关东地方大多数县市计划停电](../Page/关东地方.md "wikilink")。440万人受影响。
  - [2012年7月印度大停電](../Page/2012年7月印度大停電.md "wikilink")（2012年7月30日和31日）-
    [印度東部及北部發生大面積停電事故](../Page/印度.md "wikilink")，超過6億人日常生活受到影響。這也是迄今為止世界最大規模停電事故。
  - [815全台大停電](../Page/815全台大停電.md "wikilink")（2017年8月15日下午接近5時）-
    台灣各地陸續傳出斷電消息，範圍涵括北中南都會區等地，全台約668萬用戶受影響。

## 參見

  - [不间断电源](../Page/不间断电源.md "wikilink")

[停電](../Category/停電.md "wikilink")