**側金盞花屬**（学名：*Adonis*），是大約20至30個[毛茛目開花植物的](../Page/毛茛目.md "wikilink")[屬](../Page/屬.md "wikilink")，生長於[歐洲和](../Page/歐洲.md "wikilink")[亞洲的](../Page/亞洲.md "wikilink")[植物](../Page/植物.md "wikilink")。

側金盞花屬植物生長為10至40高，似[羽毛般](../Page/羽毛.md "wikilink")，精巧分開的葉子；花有[紅色](../Page/紅色.md "wikilink")、[橙色或](../Page/橙色.md "wikilink")[黃色](../Page/黃色.md "wikilink")，大約有5至30個[花瓣](../Page/花瓣.md "wikilink")。

## 種類

  - [夏侧金盏花](../Page/夏侧金盏花.md "wikilink") *Adonis aestivalis*
      - [小侧金盏花](../Page/小侧金盏花.md "wikilink") *Adonis aestivalis* var.
        *parviflora*
  - *Adonis aleppica*
  - [侧金盏花](../Page/侧金盏花.md "wikilink") *Adonis amurensis*
  - [秋侧金盏花](../Page/秋侧金盏花.md "wikilink") *Adonis autumnalis*（异名*Adonis
    annua*）
  - [甘青侧金盏花](../Page/甘青侧金盏花.md "wikilink") *Adonis bobroviana*
  - [短柱侧金盏花](../Page/短柱侧金盏花.md "wikilink") *Adonis brevistyla*
  - [金黄侧金盏花](../Page/金黄侧金盏花.md "wikilink") *Adonis chrysocyathus*
  - [蓝侧金盏花](../Page/蓝侧金盏花.md "wikilink") *Adonis coerulea*
      - [高蓝侧金盏花](../Page/高蓝侧金盏花.md "wikilink") *Adonis coerulea* f.
        *ingegra*
      - [毛蓝侧金盏花](../Page/毛蓝侧金盏花.md "wikilink") *Adonis coerulea* f.
        *puberula*
  - *Adonis cyllenea*
  - [宝兴侧金盏花](../Page/宝兴侧金盏花.md "wikilink") *Adonis davidii*
  - *Adonis distorta*
  - *Adonis flammea*
  - *Adonis microcarpa*
  - *Adonis nepalensis*
  - [辽吉侧金盏花](../Page/辽吉侧金盏花.md "wikilink") *Adonis pseudoamurensis*
  - *Adonis pyrenaica*
  - *Adonis ramosa*
  - [北侧金盏花](../Page/北侧金盏花.md "wikilink") *Adonis sibirica*
  - [蜀侧金盏花](../Page/蜀侧金盏花.md "wikilink") *Adonis sutchuenensis*
  - [天山侧金盏花](../Page/天山侧金盏花.md "wikilink") *Adonis tianschanica*
  - [春側金盞花](../Page/春側金盞花.md "wikilink") *Adonis vernalis*
  - *Adonis volgensis*

## 耕種和用途

耕種於庭院中。

<File:Adonis> annua detalle flor.jpg|*Adonis annua* <File:Adonis>
vernalis.jpg|*Adonis vernalis*

## 外部連結

  -
  - [Flora of China:
    *Adonis*](http://www.efloras.org/florataxon.aspx?flora_id=2&taxon_id=100626)

Herbal information (see [植物學](../Page/植物學.md "wikilink"))

  - [Adonis vernalis (Pheasant's
    Eye)](http://www.henriettesherbal.com/eclectic/kings/adonis.html)
    King's American Dispensatory @ Henriette's Herbal
  - [Hellebore,
    False](http://www.botanical.com/botanical/mgmh/h/helfal15.html) Mrs.
    Grieve's "A Modern Herbal" @ Botanical.com

[側金盞花屬](../Category/側金盞花屬.md "wikilink")
[Category:毛茛科](../Category/毛茛科.md "wikilink")
[Category:花卉](../Category/花卉.md "wikilink")