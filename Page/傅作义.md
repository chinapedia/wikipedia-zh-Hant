**傅作義**，[字](../Page/表字.md "wikilink")**宜生**，是一位生於[山西省](../Page/山西省.md "wikilink")[榮河縣的](../Page/萬榮縣.md "wikilink")[政治人物與军事人物](../Page/政治人物.md "wikilink")。他在加入[中共政權前曾是](../Page/中共.md "wikilink")[中华民国陆军二级上将](../Page/中华民国陆军.md "wikilink")。1949年，他協助[解放軍和平進入北京城](../Page/解放軍.md "wikilink")，並於後來擔任[中华人民共和国首任](../Page/中华人民共和国.md "wikilink")[水利部部长](../Page/中央人民政府水利部.md "wikilink")，晚年任[全国政协副主席](../Page/全国政协副主席.md "wikilink")。

## 生平

### 晋军中的活动

[Fu_Zuoyi2.jpg](https://zh.wikipedia.org/wiki/File:Fu_Zuoyi2.jpg "fig:Fu_Zuoyi2.jpg")的照片\]\]
傅作義出生在一个[富农家庭](../Page/富农.md "wikilink")。1910年（[宣统二年](../Page/宣统.md "wikilink")），进入太原陸軍小学学习。1911年[太原响应](../Page/太原.md "wikilink")[辛亥革命爆发起义之際](../Page/辛亥革命.md "wikilink")，傅作義加入学生軍参加革命\[1\]。

1912年（[民国元年](../Page/民国纪元.md "wikilink")），傅作義转入[北京](../Page/北京市.md "wikilink")[清河鎮的第一陸軍中学学习](../Page/清河街道_\(北京市\).md "wikilink")。1915年，入[保定陸軍軍官学校第](../Page/保定陸軍軍官学校.md "wikilink")5期步兵科。傅作義的学习成績列校内第1名。1918年毕业，回到山西。此后他加入[閻錫山领导的晋軍](../Page/閻錫山.md "wikilink")。1926年，升任第四旅旅長。1927年，升任第四師師長\[2\]。

1927年6月，閻錫山转投[国民政府方面](../Page/国民政府.md "wikilink")。同年10月，傅作義率第4師进攻[涿州](../Page/涿州.md "wikilink")，击退[奉军](../Page/奉军.md "wikilink")。不久，奉军的2個師加2個旅的大部队企图夺回涿州，傅作義指揮部队坚守3个月。1928年1月，傅作義见部队抵抗能力已达到极限，便暂時投降奉军。不久，傅作義秘密逃走，回到山西，被閻錫山任命为[国民革命军第三集团军第五軍团总指揮兼](../Page/国民革命军.md "wikilink")[天津警備司令](../Page/天津市.md "wikilink")\[3\]。

### 抗日作战

[Fu_Zuoyi.JPG](https://zh.wikipedia.org/wiki/File:Fu_Zuoyi.JPG "fig:Fu_Zuoyi.JPG")
[Fu_Zuoyi.jpg](https://zh.wikipedia.org/wiki/File:Fu_Zuoyi.jpg "fig:Fu_Zuoyi.jpg")
1930年5月，[閻錫山发动反](../Page/閻錫山.md "wikilink")[蒋介石的](../Page/蒋介石.md "wikilink")[中原大战](../Page/中原大战.md "wikilink")，傅作義担任第四路軍指揮官。閻錫山敗北之后，[張学良接收傅作義等部](../Page/張学良.md "wikilink")。1931年5月，傅作義被任命为[国民革命军第三十五軍](../Page/国民革命军第三十五軍.md "wikilink")[軍長](../Page/軍長.md "wikilink")。同年8月，他被任命为[綏遠省政府主席](../Page/綏遠省.md "wikilink")。此后傅作義在綏遠省发挥内政手腕，在治安維持、農村基層組織改革、金融整理、城乡建設、教育事业等方面都取得好成績\[4\]\[5\]。当时他赞赏[梁漱溟等人的](../Page/梁漱溟.md "wikilink")[乡村建设学说](../Page/乡村建设运动.md "wikilink")，所以在绥远省推行了教育、生产、武装三位一体的农村制度，即教、养、卫制度\[6\]。

1931年9月，[九一八事变发生](../Page/九一八事变.md "wikilink")，他和[徐永昌](../Page/徐永昌.md "wikilink")、[宋哲元联名发表抗日声明](../Page/宋哲元.md "wikilink")。1933年1月，[日軍进攻](../Page/日軍.md "wikilink")[山海关](../Page/山海关.md "wikilink")，[長城抗战爆发](../Page/長城抗战.md "wikilink")。同年2月，傅作義出任第七[軍团总指揮](../Page/軍团.md "wikilink")。同年4月末，傅作義亲自率领[國民革命軍第五十九軍从](../Page/國民革命軍第五十九軍.md "wikilink")[張家口急赴](../Page/張家口市.md "wikilink")[昌平](../Page/昌平区.md "wikilink")。5月23日，傅作義开始同日軍[第8師团交战](../Page/第8師团.md "wikilink")，双方伤亡都较为惨重。6月末，傅作義率部返回綏遠省\[7\]。

1935年4月，傅作義获晋升陸軍[二級上将](../Page/二級上将.md "wikilink")。1936年11月，[李守信率领的](../Page/李守信.md "wikilink")[蒙古軍与](../Page/蒙古軍.md "wikilink")[王英率领的大漢義軍共同进攻綏遠省](../Page/王英_\(民国\).md "wikilink")，遭到傅作義击退。11月24日，傅作義在[百灵庙击败](../Page/百灵庙.md "wikilink")[蒙古軍](../Page/蒙古軍.md "wikilink")，12月4日完全收复百灵庙，史称[百灵庙大捷](../Page/百灵庙大捷.md "wikilink")。这就是著名的[绥远抗战](../Page/绥远抗战.md "wikilink")\[8\]。

1937年3月9日，行政院決議，嘉獎綏遠抗戰將領，[閻錫山](../Page/閻錫山.md "wikilink")、傅作義敍一等[寶鼎勳章](../Page/寶鼎勳章.md "wikilink")，騎兵司令[趙承綬二等寶鼎勳章](../Page/趙承綬.md "wikilink")\[9\]。5月12日，傅作義至百靈廟視察，5月14日[至鍚拉穆㮙召](../Page/至鍚拉穆㮙召.md "wikilink")（大廟）視察\[10\]。[抗日战争爆发后](../Page/抗日战争.md "wikilink")，傅作義历任[第二战区第七集团軍总司令](../Page/抗日战争第二战区.md "wikilink")、第二战区北路軍总司令（第二战区司令长官为阎锡山），先后参加[忻口战役](../Page/忻口战役.md "wikilink")、[太原保卫战](../Page/太原保卫战.md "wikilink")。

1938年初，傅作義仿照[中国共产党领导的](../Page/中国共产党.md "wikilink")[八路军的建军经验](../Page/八路军.md "wikilink")，成立[第二战区北路军政治工作委员会](../Page/第二战区.md "wikilink")，自兼主任，总部设政治指导室，派[周北峰任委员兼秘书](../Page/周北峰.md "wikilink")，军内各级分别设政治工作机构，不少负责人由[延安派来的干部担任](../Page/延安.md "wikilink")\[11\]\[12\]。傅作义在部队中设立奋斗室，军官家属组成眷属团，军人子弟上奋斗小学、奋斗中学。[中国共产党开展](../Page/中国共产党.md "wikilink")[减租减息](../Page/减租减息.md "wikilink")，傅作义也开展整理土地，将地主的土地清丈以后，限制地租，另外又将领主、地商开垦土地未交地价者收归国有，永远租给佃农\[13\]。傅作義还制定《北路军政治工作守则》，又参照八路军的《[三大纪律、八项注意](../Page/三大纪律八项注意.md "wikilink")》制定《十项纪律》。閻錫山讥称傅作義「把部队带赤化了」、「三十五军已成为七路半了」\[14\]\[15\]。这种情况引起了[蒋介石的警觉](../Page/蒋介石.md "wikilink")，蒋介石特派[中统特务来傅作义部队任政治部主任](../Page/中统.md "wikilink")，并且驱逐[中国共产党党员](../Page/中国共产党.md "wikilink")\[16\]。1938年12月，[国民政府任命傅作義为](../Page/国民政府.md "wikilink")[第八战区副司令长官兼第二战区北路军总司令](../Page/第八战区.md "wikilink")，傅作義自此摆脱阎锡山控制，回到绥远。他先后参加[包头战役](../Page/包头战役.md "wikilink")、[绥西战役](../Page/绥西战役.md "wikilink")、[五原战役等等](../Page/五原战役.md "wikilink")。由于战绩良好，1939年傅作義获国民政府授予[青天白日勋章](../Page/青天白日勋章.md "wikilink")\[17\]\[18\]。

### 受降长官

抗日战争勝利前夕的1945年7月，傅作義任[第十二战区司令長官](../Page/第十二战区.md "wikilink")。抗日战争勝利後，第十二战区司令長官傅作義负责[熱河省](../Page/熱河省.md "wikilink")、[察哈爾省](../Page/察哈爾省.md "wikilink")、[綏遠省的日軍受降事宜](../Page/綏遠省.md "wikilink")\[19\]\[20\]。

日军投降后，大片日本占领区等待接收，当时中国国民党和中国共产党双方对受降权争夺激烈。[八路军在沒有通过前线作战但倚賴蘇聯幫助](../Page/八路军.md "wikilink")，迅速扩大了控制区。[国军部队大多在大后方](../Page/国军.md "wikilink")，难以迅速占领日本占领区。蒋介石命令各地国军迅速抢占地盘，恰好符合傅作义作为中国国民党地方派系的利益。时任第十二战区司令长官的傅作义率6万余人迅速进军[包头](../Page/包头.md "wikilink")、[归绥](../Page/归绥.md "wikilink")。在此之前，中共方面的陕甘宁晋绥五省联防军司令[贺龙已率军包围包头](../Page/贺龙.md "wikilink")、归绥，准备进城接收。傅作义部队突然抵达，强占了这两座城市。傅作义部还远程出击，企图攻占[解放区第二大城市](../Page/解放区.md "wikilink")[张家口](../Page/张家口.md "wikilink")。[八路军贺龙部会攻包头](../Page/八路军.md "wikilink")、归绥，企图从傅作义部手中夺回两城，但战斗多日后，由于缺乏弹药棉衣，被迫撤出战斗\[21\]。

### 国共内战爆发

1946年1月1日，[國民政府令](../Page/國民政府.md "wikilink")：傅作義、[徐堪給一等](../Page/徐堪.md "wikilink")[景星勳章](../Page/景星勳章.md "wikilink")；[董其武](../Page/董其武.md "wikilink")、何文鼎、[楚溪春](../Page/楚溪春.md "wikilink")、[馬占山各給予](../Page/馬占山.md "wikilink")[青天白日勳章](../Page/青天白日勳章.md "wikilink")；[李漢魂](../Page/李漢魂.md "wikilink")、[宋希濂](../Page/宋希濂.md "wikilink")、[陶希聖](../Page/陶希聖.md "wikilink")、陳行、[葉恭綽等黨](../Page/葉恭綽.md "wikilink")、政、軍、文化、學術界967人給予勝利勳章\[22\]。[第二次国共内战爆发后](../Page/第二次国共内战.md "wikilink")，傅作義奉命与[中国共产党领导下的军队在華北展開激战](../Page/中国共产党.md "wikilink")，取得[大同集宁战役的胜利](../Page/大同集宁战役.md "wikilink")，并攻占[张家口等重镇](../Page/张家口.md "wikilink")，使[中国共产党领导下的军队在该时期处于劣势](../Page/中国共产党.md "wikilink")\[23\]\[24\]。

1946年内战爆发后，[贺龙的](../Page/贺龙.md "wikilink")[晋绥军区](../Page/晋绥军区.md "wikilink")、[聂荣臻的](../Page/聂荣臻.md "wikilink")[晋察冀军区会攻](../Page/晋察冀军区.md "wikilink")[大同](../Page/大同市.md "wikilink")，并提出
“进大同吃[月饼](../Page/月饼.md "wikilink")”的口号。驻守大同的[阎锡山部即将溃败](../Page/阎锡山.md "wikilink")，蒋介石随即决定将大同转而划归傅作义的第十二战区。此后，傅作义一方面派[周北峰赴解放区进行假和谈](../Page/周北峰.md "wikilink")，一方面派部队奇袭[集宁](../Page/集宁.md "wikilink")，以支援大同方向的战斗。经过激战，傅作义部攻克集宁，[中国共产党方面的部队不得不从大同撤围](../Page/中国共产党.md "wikilink")\[25\]。

1946年9月，傅作义在他手下的《奋斗日报》上发表《致毛泽东的公开电》，公开“谴责中共发起内战”。1946年9月21日，[南京](../Page/南京.md "wikilink")《[中央日报](../Page/中央日报.md "wikilink")》以《傅作义电劝毛泽东结束战乱参加政府
人民希望在和平中生活违反民意是绝难获成功》为题，全文转载了该电：

[中共中央机关报](../Page/中共中央.md "wikilink")《[解放日报](../Page/解放日报.md "wikilink")》全文转载了此电。[朱德总司令指示西北解放军向连级以上干部宣读此电](../Page/朱德.md "wikilink")，当作反面教材。傅作义并不知道，为自己起草此电的第十二战区长官部新闻处少将副处长、奋斗日报社社长[阎又文](../Page/阎又文.md "wikilink")，其实是一位中国共产党地下党员。阎又文被委派为傅作义起草该公开电后，曾请示中共组织，[周恩来指示他](../Page/周恩来.md "wikilink")：“公开电要骂得狠些，要能够激起解放区军民义愤，要能够导致傅作义部队狂妄自大。”阎又文便按照周恩来的指示起草了这一公开电\[26\]。

1946年10月，傅作义声东击西，巧妙迫使[华北野战军弃守](../Page/华北野战军.md "wikilink")[张家口](../Page/张家口.md "wikilink")。傅作义部开入张家口的次日，蒋介石便在[南京宣布召开中国共产党拒絕参加的](../Page/南京.md "wikilink")[制宪国民大会](../Page/制宪国民大会.md "wikilink")。傅作义赶到南京，受到国大代表热烈欢迎，将傅作义视为中国国民党的党国中兴功臣，并因此获颁发国光勋章以示嘉奖\[27\]。

1947年1月，傅作義转任察哈爾省政府主席（其部下[董其武继任綏遠省政府主席](../Page/董其武.md "wikilink")）\[28\]\[29\]。3月1日，[第二戰區](../Page/抗日戰爭第二戰區.md "wikilink")、[第十一戰區](../Page/抗日戰爭第十一戰區.md "wikilink")、[第十二戰區撤銷](../Page/抗日戰爭第十二戰區.md "wikilink")，成立太原、保定、張垣[綏靖公署](../Page/綏靖公署.md "wikilink")，閻錫山、孫仲連、傅作義任主任\[30\]。7月19日，國民政府派傅作義為察哈爾選舉事務所主任委員；國民政府同時派定其為[國民大會代表](../Page/國民大會代表.md "wikilink")，[立法院](../Page/立法院.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")\[31\]。1947年，他在原籍[山西省](../Page/山西省.md "wikilink")[榮河縣當選為](../Page/榮河縣.md "wikilink")[第一屆國民大會代表](../Page/第一屆國民大會代表.md "wikilink")。

### 北平、绥远失守

1947年12月，[蔣介石統一華北軍事機構](../Page/蔣介石.md "wikilink")，任命傅作義為[華北剿匪總司令部總司令](../Page/華北剿匪總司令部.md "wikilink")，五省軍隊悉數歸其指揮\[32\]。傅作義在張家口就任\[33\]。他以[北平](../Page/北平.md "wikilink")、[天津](../Page/天津.md "wikilink")、[保定三城為犄角防守陣勢](../Page/保定.md "wikilink")，組成津浦、平漢、平綏兵團，實行以主力對主力機動防御，同時以地方武裝固守據點\[34\]。12月10日，傅作義自張家口到北平\[35\]。華北剿匪总司令部即將自張家口移至[豐台](../Page/豐台.md "wikilink")\[36\]。12月22日，[楚溪春到張垣會晤傅作義](../Page/楚溪春.md "wikilink")，當日又返[瀋陽](../Page/瀋陽.md "wikilink")\[37\]。

1948年1月11日，蔣偕[劉斐](../Page/劉斐.md "wikilink")、[范漢杰](../Page/范漢杰.md "wikilink")、[俞濟時等人由](../Page/俞濟時.md "wikilink")[瀋陽同機飛返南京](../Page/瀋陽.md "wikilink")；傅作義飛返北平\[38\]。新編三十二師在傅作義指揮下，於[涞水战役中遭到](../Page/涞水战役.md "wikilink")[中国人民解放軍殲灭](../Page/中国人民解放軍.md "wikilink")，战况没有起色。4月14日，河北國軍攻佔[河間](../Page/河間.md "wikilink")\[39\]。4月15日，河北國軍攻佔[任丘](../Page/任丘.md "wikilink")\[40\]。4月17日，河北國軍再佔[易縣](../Page/易縣.md "wikilink")；察哈爾國軍攻佔[龍關](../Page/龍關.md "wikilink")\[41\]。4月19日，華北剿匪總司令部總司令傅作義、[河北省政府主席](../Page/河北省政府.md "wikilink")[楚溪春飛抵山西太原](../Page/楚溪春.md "wikilink")，晤見[閻錫山](../Page/閻錫山.md "wikilink")，會商華北軍事\[42\]。6月26日，冀東國軍進佔石門\[43\]。7月4日，察南國軍再佔[延慶](../Page/延慶.md "wikilink")\[44\]；冀東國軍進佔[遵化](../Page/遵化.md "wikilink")\[45\]。7月5日，傅作義在河北省參議會講演，稱要貫徹總動員，愛民保政，實施精神動員，造成軍政民整體作風\[46\]。7月6日，傅作義為北平慘案發表明，稱對東北流亡學生生活寄以同情，希望事態勿再擴大，防止暴亂行動\[47\]。9月5日，[华北野战军进攻傅作义的基地](../Page/华北野战军.md "wikilink")[绥远](../Page/绥远.md "wikilink")，迫使傅作义从[北平](../Page/北平.md "wikilink")、张家口分出10个师兵力救援绥远。9月12日，[东北野战军发动](../Page/东北野战军.md "wikilink")[辽沈战役](../Page/辽沈战役.md "wikilink")，傅作义随即面临两线作战的被动局面。11月，蔣接見傅作義，指示華北軍政措施\[48\]。在1948年底至1949年1月的[平津战役中](../Page/平津战役.md "wikilink")，傅作義的部队接连遭受失败。1948年12月下旬，先后丧失[新保安](../Page/新保安.md "wikilink")、张家口\[49\]\[50\]\[51\]。

1948年末，[中國共產黨](../Page/中國共產黨.md "wikilink")、傅作義及社会各方面都开展了和平工作\[52\]。12月23日，解放軍佔領張家口及[察哈爾省全境](../Page/察哈爾省.md "wikilink")\[53\]。12月26日，傅作義談判代表崔載之等從[薊縣八里莊返回北平](../Page/薊縣.md "wikilink")\[54\]。12月30日，傅作義由北平抵達南京，向蔣報告華北軍事情況\[55\]。

1949年1月1日，中共中央電覆平津前線林彪，指示爭取傅作義走和平道路工作要點，通過中共北平市委告訴傅作義：一、傅作義「目前不要發通電」，此電一發，他即沒有合法地位了，他本人和他的部屬，都可能受到蔣系的壓迫，甚至被解決；二、將傅列為戰犯宣布，「傅在蔣介石及蔣系軍面前的地位立即加強了。傅可借此做文章，表示只要有堅持決打下去，除此以外再無出路；但在實際上，則和我們談好，裡應內合，和平地解放北平，或經過不很激烈的戰鬥解放北平。傅氏立此一大功勞，我們就有理由赦免其戰犯罪，並保存其部屬」；三、傅可派一有地位代表及張東蓀出城密談\[56\]。1月8日，傅作義代表[周北峰](../Page/周北峰.md "wikilink")、[張東蓀與解放軍平津前線司令部領導人林彪](../Page/張東蓀.md "wikilink")、聶榮臻、羅榮桓舉行會談。傅方提出北平、[天津](../Page/天津.md "wikilink")、[塘沽](../Page/塘沽.md "wikilink")、綏遠一起解決；軍隊不用投降或在城內繳械方式，而採用以團為單位出城整編。次日，雙方草簽《會談紀要》，並附記：傅方務必於1月14日午夜前答覆\[57\]。

1月15日，天津陷入解放军之手\[58\]。1月21日，傅作義在北平[中南海宣布与中國共產黨方面达成](../Page/中南海.md "wikilink")《关于和平解决北平问题的协议》，决定[北平和平解放](../Page/北平和平解放.md "wikilink")。是日，[中华民国总统蒋中正宣布引退](../Page/中华民国总统.md "wikilink")。1月23日，傅作義投降於中國共產黨，揚言「北平局部和平」，中國共產黨遂入據北平[故宮](../Page/故宮.md "wikilink")\[59\]。1月31日，中国人民解放军和平开入北平\[60\]\[61\]\[62\]。

傅作義在北平和绥远失守前后，一直游走于国共双方之间，两面示好，为今后自身的发展留下较大回旋空间。1949年1月21日，[徐永昌奉蒋中正之命飞抵](../Page/徐永昌.md "wikilink")[北平东单机场](../Page/北平东单机场.md "wikilink")，随后到中南海居仁堂与傅作義、[邓宝珊会晤](../Page/邓宝珊.md "wikilink")，传达蒋中正意旨，傅作義答复很不明确，而且未向徐永昌透露自己已经和中共达成和平协议。1月22日，傅作義部按照和平协议开始撤离北平。当日傍晚，傅作義通过[中央社发布文告](../Page/中央社.md "wikilink")，抢在中共之前公布《关于和平解决北平问题的协议》部分条款，但仅公布了全部协议正文加附件22条中的13条，回避了战败问题，以造成北平和平以傅作义为主的舆论，以求在今后与中共的合作中谈条件\[63\]。

此后，在军事上，傅作义提出[華北剿匪总司令部取消](../Page/華北剿匪总司令部.md "wikilink")，兵团、军、师一律不动的整编方案，最终未被中共接受，中共对傅作义部实行了打散合编的整编方案\[64\]。在政治上，傅作义抓住《关于和平解决北平问题的协议》中“双方派员成立联合办事机构，处理有关军政事宜”以及企业、银行、学校等听候“联合办事机构处理”的条款，力求将这一所谓“联合办事机构”变成自己与中共分享权力的政权。在该协议签订前，毛泽东已于1月15日致电[林彪等人指出](../Page/林彪.md "wikilink")：“北平城内成立联合机构一点，似乎仍有和我分享政权之意。”1月29日，北平联合办事处召开筹备会议，中共代表[叶剑英径直对傅作义方面代表](../Page/叶剑英.md "wikilink")[郭宗汾说](../Page/郭宗汾.md "wikilink")：“此机构是在前线司令部指挥下的工作机关，不是政权机关。”为此，在叶剑英建议下，该机构名称确定为“北平联合接交办事处”。加上“接交”二字，最终从形式及性质上否定了傅作义的任何分权企图\[65\]\[66\]。傅作義在军事及政治上的谋划均迅速遭到挫折\[67\]。1月31日，不愿与中共合作的北平[国军將領](../Page/国军.md "wikilink")[李文](../Page/李文.md "wikilink")、[石覺等人離開北平](../Page/石覺.md "wikilink")，到達[青島](../Page/青島.md "wikilink")\[68\]。

1949年2月22日至2月24日，傅作義、邓宝珊与[颜惠庆](../Page/颜惠庆.md "wikilink")、[邵力子](../Page/邵力子.md "wikilink")、[章士钊等人赴](../Page/章士钊.md "wikilink")[西柏坡](../Page/西柏坡.md "wikilink")，受到[毛泽东等中共领导人接见](../Page/毛泽东.md "wikilink")，邵力子发现傅作義“甚苦闷”，傅作義、邓宝珊此行与中共方面商谈了绥远问题。当时[绥远尚在傅作義部下](../Page/绥远.md "wikilink")[董其武手中](../Page/董其武.md "wikilink")，名义上仍受[中华民国政府节制](../Page/中华民国政府.md "wikilink")。傅作義企圖在3月26日從[北平西苑機場乘飛機赴綏遠](../Page/北京西郊机场.md "wikilink")，不料當天恰逢[中共中央機關自](../Page/中共中央.md "wikilink")[西柏坡遷至北平](../Page/西柏坡.md "wikilink")，並且在北平西苑機場閱兵，傅作義飛赴綏遠的計劃遂告吹。4月1日，傅作義向全国发表通电，靠拢中共\[69\]。

此后，[广州的中华民国政府积极拉拢绥远方面](../Page/广州.md "wikilink")。绥远一直处于政治上在国共之间不明朗的状态。7月14日，傅作義在呈毛泽东的上书中，称蒋中正、阎锡山卖国，幻想[第三次世界大战爆发](../Page/第三次世界大战.md "wikilink")，滥炸人民、封锁海口，没有希望。8月28日，傅作義、邓宝珊等人从北平返回绥远，以完成绥远易帜起义。但傅作義也邀请徐永昌来绥远会面。9月17日，徐永昌、[马鸿宾一行飞抵](../Page/马鸿宾.md "wikilink")[包头](../Page/包头.md "wikilink")，傅作義、邓宝珊、董其武、[孙兰峰等人到机场迎接](../Page/孙兰峰.md "wikilink")。9月18日，绥远起义通电的签字仪式举行，宣布“[绥远和平解放](../Page/绥远和平解放.md "wikilink")”，与此同时，傅作義会见徐永昌，徐永昌企图策反傅作義，但傅作義因为[美国援助无望而不愿在美苏战争](../Page/美国.md "wikilink")（即第三次世界大战）爆发前贸然投向蒋中正。

9月19日，即徐永昌在绥远最后一日，傅作義再度会见徐永昌，对徐痛哭，且为自己预留后路，向徐永昌递交了一份上蒋中正的呈文，在呈文以及和徐永昌的系列谈话中，傅作義称中共卖国，美苏战争的爆发不会远，建议轰炸电厂，认为中共不可能成功，还准备利用合作农场，将自己手下的干部放入农村，以寓兵于农，保存实力，待中共遇到困难时再打出去\[70\]。同日，徐永昌帶上[張慶恩等大小特務於下午二時飞离绥远](../Page/張慶恩.md "wikilink")。下午四時許，傅作義召集幹部開會，宣佈发出通电，「祝賀起义勝利」\[71\]。同日，馬鴻賓得到傅作義鼓勵，自綏遠飛回銀川，即電令其子馬惇靖率領國軍第八十一軍在中衛地區與解放軍簽定和平解放協定\[72\]

9月27日，北平更名為[北京](../Page/北京.md "wikilink")。

1949年9月“[绥远和平解放](../Page/绥远和平解放.md "wikilink")”后，同年12月9日，[中国人民革命军事委员会决定](../Page/中国人民革命军事委员会.md "wikilink")，[绥远的](../Page/绥远.md "wikilink")**傅作义**部的[國軍第九兵團](../Page/國軍.md "wikilink")（[孙兰峰任司令](../Page/孙兰峰.md "wikilink")）等部改編為中國人民解放軍第36軍（[刘万春任军长](../Page/刘万春.md "wikilink")（后来[王建业代理军长](../Page/王建业.md "wikilink")），[康健民任政治委员](../Page/康健民.md "wikilink")）、第37軍（[张世珍任军长](../Page/张世珍.md "wikilink")，[帅荣任政治委员](../Page/帅荣.md "wikilink")），归华北军区建制，由绥远军区指挥。1950年10月，[中国人民志愿军赴朝鲜开始](../Page/中国人民志愿军.md "wikilink")[抗美援朝战争](../Page/抗美援朝战争.md "wikilink")。1950年11月上旬，傅作义向[毛泽东建议](../Page/毛泽东.md "wikilink")，调原绥远起义部队赴[抗美援朝战争前线参战](../Page/抗美援朝战争.md "wikilink")，认为这能使这批部队受新锻炼、新考验，进一步实现其[解放军化](../Page/中国人民解放军.md "wikilink")，也对绥远地方实现[解放区化更有利](../Page/解放区.md "wikilink")。这一建议获得[中共中央](../Page/中共中央.md "wikilink")、毛泽东采纳。1950年12月12日，根据[中国人民革命军事委员会决定](../Page/中国人民革命军事委员会.md "wikilink")，第36军、第37军与騎兵第4師等部組成中國人民解放軍第23兵團（[董其武任兵团司令员](../Page/董其武.md "wikilink")，[高克林任兵团政治委员](../Page/高克林.md "wikilink")），归华北军区建制，1951年9月3日开赴朝鲜，进行了修建机场的工作，同年11月底归国。1952年2月，第36军、第37军番号撤销，第23兵團直辖第107师、第109师。1952年12月，中國人民解放軍第23兵團整編為中國人民解放軍第69軍（[董其武任军长](../Page/董其武.md "wikilink")，[裴周玉任政治委员](../Page/裴周玉.md "wikilink")），归华北军区（后改为[北京军区](../Page/北京军区.md "wikilink")）建制，1985年6月撤销。\[73\]\[74\]

### 人民共和国初期

1949年9月22日，傅作義从绥远回到北平。9月23日，他出席[中国人民政治协商会议第一届全体会议并在大会发言](../Page/中国人民政治协商会议第一届全体会议.md "wikilink")，声称自己坚决拒绝了蒋中正的“亲切的”电报。\[75\]在1949年9月的[中国人民政治协商会议第一届全体会议上](../Page/中国人民政治协商会议第一届全体会议.md "wikilink")，傅作義当选为[第一届全国政协委员](../Page/第一届全国政协.md "wikilink")、[中华人民共和国中央人民政府委员](../Page/中华人民共和国中央人民政府_\(1949-1954\).md "wikilink")。\[76\]

据中共方面当时主管绥远工作的[薄一波记载](../Page/薄一波.md "wikilink")，傅作義从绥远回到北平后随即向薄一波建议：[后套地区](../Page/河套平原.md "wikilink")“可耕地为10万顷，而现耕种面积只4万顷，如果政策允许的话，他愿意在此修水利搞合作农场。”薄一波记载：“我把他的意愿报告党中央，不久，毛主席就在傅的意愿的基础上提名他为[中央人民政府水利部部长](../Page/中央人民政府水利部.md "wikilink")。”这就把傅作義留在了北平。\[77\]时任[中共中央书记处政治秘书室主任的](../Page/中共中央书记处.md "wikilink")[师哲晚年回忆称](../Page/师哲.md "wikilink")：“中央人民政府成立，让傅作義担任了水利部部长。约一年后的一天，他找到毛泽东，给毛泽东说，他还有多少[电台](../Page/电台.md "wikilink")，多少枝枪，存在什么什么地方。”毛泽东回答说：“你留着用吧。”\[78\]\[79\]

[中華人民共和国成立後](../Page/中華人民共和国.md "wikilink")，傅作義历任中华人民共和国中央人民政府委員、[中央人民政府人民革命军事委员会委員](../Page/中央人民政府人民革命军事委员会.md "wikilink")、[中央人民政府水利部](../Page/中央人民政府水利部.md "wikilink")（1954年改为[中华人民共和国水利部](../Page/中华人民共和国水利部.md "wikilink")，后又改为[中华人民共和国水利电力部](../Page/中华人民共和国水利电力部.md "wikilink")）部長、第一、二、三届[全国人民代表大会代表](../Page/全国人民代表大会.md "wikilink")、[中华人民共和国国防委員会副主席](../Page/中华人民共和国国防委員会.md "wikilink")、第二、三届[全国政协常務委員](../Page/全国政协.md "wikilink")、第四届[全国政协副主席](../Page/全国政协副主席.md "wikilink")。特别是水利部長一职，他从[中華人民共和国建国後一直担任至逝世](../Page/中華人民共和国.md "wikilink")，连任25年。\[80\]在水利部长任上，他每年拿出1/4以上时间出北京视察各地，走遍[长江](../Page/长江.md "wikilink")、[黄河](../Page/黄河.md "wikilink")、[淮河](../Page/淮河.md "wikilink")、[珠江](../Page/珠江.md "wikilink")、[黑龙江](../Page/黑龙江.md "wikilink")、[松花江的水利工地](../Page/松花江.md "wikilink")。外出视察时，他总是轻车简从，有时还自带行李。\[81\]

### 争取台湾

[Zhou_Enlai,_Zhang_Zhizhong,_Fu_Zuoyi,_Qu_Wu_in_1962.jpg](https://zh.wikipedia.org/wiki/File:Zhou_Enlai,_Zhang_Zhizhong,_Fu_Zuoyi,_Qu_Wu_in_1962.jpg "fig:Zhou_Enlai,_Zhang_Zhizhong,_Fu_Zuoyi,_Qu_Wu_in_1962.jpg")前夕，[周恩来邀请傅作義](../Page/周恩来.md "wikilink")、[张治中](../Page/张治中.md "wikilink")、[屈武商谈对](../Page/屈武.md "wikilink")[台湾工作问题](../Page/台湾.md "wikilink")。这是会后合影。自左至右：张治中、周恩来、傅作義、屈武\]\]
1962年[春节前夕](../Page/春节.md "wikilink")，[周恩来邀请](../Page/周恩来.md "wikilink")[张治中](../Page/张治中.md "wikilink")、傅作義、[屈武共同商谈台湾问题](../Page/屈武.md "wikilink")，并合影留念。\[82\]據[台湾学者](../Page/台湾.md "wikilink")[郭岱君披露](../Page/郭岱君.md "wikilink")，1963年8月9日[蔣中正日記记载](../Page/蔣中正日記.md "wikilink")：「傅逆作義特以專人帶來其親筆書『悉貢所能』四字密告於余，但其並未具名，其字確是真筆。可知匪共內部已至崩潰在即，有不可想象之勢，否則此種投機分子，絕不敢出此也。」此后8月、9月、10月、11月，蒋介石日记均曾提到傅作義之事，但後來此事無疾而終\[83\]\[84\]。

[文化大革命爆发之初](../Page/文化大革命.md "wikilink")，傅作义便被[周恩来列入](../Page/周恩来.md "wikilink")《[一份应予保护的干部名单](../Page/一份应予保护的干部名单.md "wikilink")》，点名保护起来。1974年初，傅作义因患[癌症住院](../Page/癌症.md "wikilink")。当时周恩来也已住院动手术，但得知傅作义病危，便坚持到[北京医院探望](../Page/北京医院.md "wikilink")，周恩来握住躺在病床上的傅作义的手说：“宜生先生，[毛主席叫我看望你来了](../Page/毛主席.md "wikilink")，毛主席说你为北平的和平解放立了大功！”1974年纪念[台湾](../Page/台湾.md "wikilink")[“二二八”起义座谈会](../Page/二二八事件.md "wikilink")，住院的傅作义无法出席，特别委托[董其武代其宣读书面发言](../Page/董其武.md "wikilink")：“我是1895年出生的人，正是台湾被日本侵占的那一年。现居台湾的许多老年人，都是在那前后出生的。这些人以及晚出生一二十年的人，都受过外人的欺侮凌辱。你们骂我是降将，表示对我的话你们是不屑于听的。但我当时就认为，我做了一件正确的事。二十五年的历史充分证明了，我做的确实是一件最正确的事。我现在仍然要劝说你们……”\[85\]

1974年4月19日，傅作義在北京病逝，享壽78岁\[86\]。

## 逸事

在军队带兵时，傅作义总是十分俭朴，穿着和普通士兵一样的棉布军服，腰扎细皮带，所以人称布衣将军。\[87\]

## 家庭

  - 曾祖父：傅贵德
  - 祖父：傅文鼎，生二子（傅庆泰、傅庆雨）
  - 父：傅庆泰。傅庆泰、傅庆雨共生十子，按照仁、义、礼、智、信、温、良、恭、俭、让顺序排列。其中傅作智、傅作让早夭，傅庆泰生傅作仁、傅作义、傅作良，傅庆雨生傅作礼、傅作信、傅作温、傅作恭、傅作俭。
  - 母：孙氏
  - 继母：张氏
  - 叔父：傅庆雨
  - 兄：傅作仁，字静斋，为母亲孙氏所生
  - 弟：傅作良，为继母张氏所生
  - 妹：傅京子
  - 堂弟：[傅作恭](../Page/傅作恭.md "wikilink")，傅庆雨之子。1957年在[反右中被打成極右分子](../Page/反右.md "wikilink")，開除公職，送到[甘肃省](../Page/甘肃省.md "wikilink")[酒泉](../Page/酒泉.md "wikilink")[夾邊溝農場](../Page/夾邊溝農場.md "wikilink")[勞動教養](../Page/勞動教養.md "wikilink")。1960年死於[三年大饑荒](../Page/三年大饑荒.md "wikilink")。\[88\]\[89\]
  - 大夫人：张金强（？—1994年），1909年结婚，育有一子二女。
      - 长女：[傅冬](../Page/傅冬.md "wikilink")（1924年－2007年）原名傅东菊
          - 女婿：周毅之（？—1997年），傅冬的丈夫
      - 次女：傅西菊
      - 长子：傅瑞元，原名傅印
  - 二夫人：[刘芸生](../Page/刘芸生.md "wikilink")（1910年—2016年）\[90\]，1929年在天津结婚，育有二子六女。
      - 长女：傅克庄（1928年—）\[91\]
      - 长子：[傅亨](../Page/傅亨.md "wikilink")（1929年—1997年）\[92\]又名傅恒，化学家
      - 次女：傅克坚（1933年—）\[93\]
      - 三女：[傅克诚](../Page/傅克诚_\(建筑师\).md "wikilink")（1935年—）\[94\]，建筑师
      - 四女：傅克谨（1936年—）\[95\]
      - 六女：傅克利（1942年—）\[96\]
      - 次子：傅立（1946年—）

## 纪念

  - [傅作义故居](../Page/傅作义故居_\(临猗县\).md "wikilink")：位于[山西省](../Page/山西省.md "wikilink")[临猗县](../Page/临猗县.md "wikilink")[孙吉镇安昌村西部](../Page/孙吉镇.md "wikilink")。由傅作义的祖父傅文鼎建于[清朝](../Page/清朝.md "wikilink")[道光年间](../Page/道光.md "wikilink")，傅作义生于此。\[97\]
  - [杭锦后旗文化科技会展中心](../Page/杭锦后旗文化科技会展中心.md "wikilink")：又名傅作义纪念馆，位于[内蒙古自治区](../Page/内蒙古自治区.md "wikilink")[杭锦后旗人民政府对面的塞上星广场中央](../Page/杭锦后旗.md "wikilink")。\[98\]
  - [傅作义旧宅](../Page/傅作义旧宅.md "wikilink")：位于[北京市](../Page/北京市.md "wikilink")[东城区](../Page/东城区_\(北京市\).md "wikilink")[史家胡同](../Page/史家胡同.md "wikilink")47号（今为32号）。[中华人民共和国成立初期](../Page/中华人民共和国.md "wikilink")，水利部长傅作义一家，以及部分水利部门工作人员居住在此。后来[刘文辉也曾在此居住](../Page/刘文辉.md "wikilink")。\[99\]
  - [傅作义故居](../Page/傅作义故居_\(西城区\).md "wikilink")：[北京市](../Page/北京市.md "wikilink")[西城区](../Page/西城区.md "wikilink")[小酱坊胡同](../Page/小酱坊胡同.md "wikilink")19号（今为27号）寓所。\[100\]从1948年来到北平至[文化大革命](../Page/文化大革命.md "wikilink")，傅作义一直在此居住。后来此处成为[谷牧住宅](../Page/谷牧.md "wikilink")。
  - [傅作义故居](../Page/傅作义故居_\(海淀区\).md "wikilink")：[北京市](../Page/北京市.md "wikilink")[海淀区海淀港沟](../Page/海淀区.md "wikilink")14号别墅。\[101\]后来上交政府，现已无存。

## 參考文獻

## 外部链接

{{-}}

<table>
<thead>
<tr class="header">
<th><p>（<a href="../Page/南京国民政府.md" title="wikilink">南京国民政府</a><a href="../Page/中華民國政府.md" title="wikilink">中華民國政府</a>）  </p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

[Category:第三屆全國人大代表](../Category/第三屆全國人大代表.md "wikilink")
[Category:第二屆全國人大代表](../Category/第二屆全國人大代表.md "wikilink")
[Category:第一屆全國人大代表](../Category/第一屆全國人大代表.md "wikilink")
[Category:第四屆全國政協委員](../Category/第四屆全國政協委員.md "wikilink")
[Category:第三屆全國政協委員](../Category/第三屆全國政協委員.md "wikilink")
[Category:第二屆全國政協委員](../Category/第二屆全國政協委員.md "wikilink")
[Category:第一屆全國政協委員](../Category/第一屆全國政協委員.md "wikilink")
[Category:中国人民政治协商会议第一届全体会议代表](../Category/中国人民政治协商会议第一届全体会议代表.md "wikilink")
[Category:第1屆中華民國國民大會代表](../Category/第1屆中華民國國民大會代表.md "wikilink")
[Category:中華民國陸軍二級上將](../Category/中華民國陸軍二級上將.md "wikilink")
[Category:國共戰爭人物](../Category/國共戰爭人物.md "wikilink")
[Category:中國第二次世界大戰人物](../Category/中國第二次世界大戰人物.md "wikilink")
[Category:中原大戰人物](../Category/中原大戰人物.md "wikilink")
[Category:北伐人物](../Category/北伐人物.md "wikilink")
[Category:辛亥革命人物](../Category/辛亥革命人物.md "wikilink")
[Category:前中國國民黨黨員](../Category/前中國國民黨黨員.md "wikilink")
[Category:中國國民黨革命委員會黨員](../Category/中國國民黨革命委員會黨員.md "wikilink")
[Category:保定陆军军官学校校友](../Category/保定陆军军官学校校友.md "wikilink")
[Category:骨灰盒存于八宝山革命公墓骨灰堂中一室](../Category/骨灰盒存于八宝山革命公墓骨灰堂中一室.md "wikilink")
[傅作义家族](../Category/傅作义家族.md "wikilink")
[Category:万荣县人](../Category/万荣县人.md "wikilink")
[Z作](../Category/傅姓.md "wikilink")

1.

2.
3.
4.
5.

6.
7.
8.
9.

10.
11.
12.
13.

14.
15.
16.
17.
18.
19.
20.
21.
22.
23.
24.
25.
26.
27.
28.
29.
30.
31.
32. 陳布雷等編著：《蔣介石先生年表》，台北：傳記文學出版社，1978年6月1日

33.
34.
35.
36.
37.
38.
39.
40.
41.
42.
43.
44.
45.
46.
47.
48.
49.
50.
51.
52. 12月23日，傅作義致電毛澤東，說：「為求人民迅即得救，擬即通電全國，停止戰鬥，促成全面和平統一」，「細節問題請指派人員在平商談解決」。見李新總主編，中國社會科學院近代史研究所中華民國史研究室編，韓信夫、姜克夫主編：《中華民國史大事記》，北京中華書局2011年7月版，第8758頁

53.
54.
55.
56.
57.
58.
59.
60.
61.
62.
63.
64.
65.
66. 1月22日，「今日北平方面傳來不利的消息：「傅作義與共匪已成立休戰條件，准在城內與共匪成立聯合辦事處，所有我方軍隊，除極少數之維持秩序者外，皆開出郊外整編。」見[蔣經國](../Page/蔣經國.md "wikilink")：〈危急存亡之秋〉，刊《風雨中的寧靜》，[台北](../Page/台北.md "wikilink")：[正中書局](../Page/正中書局.md "wikilink")，1988年，第138頁

67.
68. 1月31日，「北平將領李文、石覺等，直至本日，始得離開北平，到達[青島](../Page/青島.md "wikilink")。傅逆總部亦遷西郊，匪軍已入駐北平城內，並與博部成立十三項協定；傅本人則飛返[绥遠](../Page/绥遠.md "wikilink")，而其覆父親之信，則尚稱「為大局打算」也。前擬空運部隊離平計劃，至此已成泡影。」見蔣經國：〈危急存亡之秋〉，刊《風雨中的寧靜》，台北：正中書局，1988年，第147頁。文中說傅作義飛返綏遠是不對的，當時傅作義一直在北平，直到8月28日才返回綏遠。「十三項協定」是誤記，《關於和平解決北平問題的協議》協議正文加附件共22條，13條是傅作義1月22日通過中央社發布文告時選擇性公布的其中13條，南京方面誤以為這是全部條款。

69.
70. [邓野，傅作义政治转型过程中的双重性，历史研究2005年05期](http://news.ifeng.com/history/zhongguojindaishi/detail_2013_12/26/32486051_0.shtml)

71.

72.
73. [绥远起义部队抗美援朝，内蒙古日报，2011年7月13日](http://szb.northnews.cn/nmgrb/html/2011-07/13/content_845452.htm)


74. [北方新报与内蒙古党委党史研究室影视中心共同寻找23兵团老战士，正北方网，2012年05月30日](http://www.northnews.cn/2012/0530/806319.shtml)


75.
76.
77.
78.
79. 师哲：《我的一生——师哲自述》，北京：人民出版社，2001年

80.
81.
82. [周恩来、张治中、傅作义、屈武，人民网，2002-03-05](http://www.people.com.cn/GB/tupian/70/20020305/679291.html)

83.

84. 朱宗震，傅作义接受北平和平解决的政治性质初探，载 近代中国（第7辑），立信会计出版社，1997年8月

85.
86.
87.
88.

89. 和鳳鳴，經歷：我的一九五七年，敦煌文藝出版社，2006年，ISBN：7805875685

90.
91.
92.
93.
94.

95.
96.
97. [傅作义故居全面修复迎接游客，新华网，2012年06月26日](http://news.xinhuanet.com/2012-06/26/c_112288939.htm)

98. [巴彦淖尔市重点景区情况，巴彦淖尔市新闻门户网站，2009-11-18](http://www.bynrnews.com/news/Doc.jsp?t=loginslypdparamter&docid=logins303paramter990100)


99. [李遥，红门内外:
    史家胡同51号章士钊旧居的故事，文史参考2010年第3期](http://www.people.com.cn/GB/198221/198819/198847/12430607.html)

100. [顾育豹，毛泽东与首任党外部长傅作义，中国共产党新闻网，2010年12月29日](http://dangshi.people.com.cn/GB/85038/13610334.html)

101.