**傅聰**（），生于[中華民國](../Page/中華民國.md "wikilink")[上海](../Page/上海.md "wikilink")，[英國國籍](../Page/英國國籍.md "wikilink")，华裔[鋼琴家](../Page/鋼琴家.md "wikilink")。

## 生平

傅聰於1934年3月10日[上海的花園新村出生](../Page/上海.md "wikilink")，他的父亲是翻译家[傅雷](../Page/傅雷.md "wikilink")。傅聰三、四歲的時候，已經表現出過人的[音樂天份](../Page/音樂.md "wikilink")。他七歲半開始學習[鋼琴](../Page/鋼琴.md "wikilink")，拜[義大利](../Page/義大利.md "wikilink")[指揮家](../Page/指揮家.md "wikilink")、鋼琴家為師。梅百器是[李斯特的再傳弟子](../Page/李斯特.md "wikilink")。傅聰在其門下受教三年。傅聰於1951年再拜蘇籍鋼琴家[勃隆斯丹夫人](../Page/勃隆斯丹夫人.md "wikilink")（Ada
Bronstein）為師。

1952年2月，18歲的傅聰與[上海交響樂團合作](../Page/上海交響樂團.md "wikilink")，作了首次的公開表演，引起了國內音樂界的注意。1953年，傅聰被選中參加在[羅馬尼亞舉行的第四屆世界青年與學生和平友誼聯歡節的鋼琴比賽](../Page/羅馬尼亞.md "wikilink")。同年7月，傅聰首次出國，到羅馬尼亞參賽，得到了三等獎。比賽後，傅聰隨團訪問[德國和](../Page/德國.md "wikilink")[波蘭](../Page/波蘭.md "wikilink")，在波蘭多次演奏蕭邦的作品，得到當地音樂家的認同。1955年，傅聰獲邀參加在華沙舉行的第五屆[蕭邦國際鋼琴比賽](../Page/蕭邦國際鋼琴比賽.md "wikilink")，最後得到第三名和「[瑪祖卡](../Page/瑪祖卡.md "wikilink")」獎，成為首位在國際性鋼琴比賽中獲獎的中國音樂家。
蕭邦國際鋼琴比賽結束後，傅聰留在波蘭學習鋼琴，直到1958年底提前畢業。這期間，傅聰曾於1956年的8至10月返回中國休假，在[北京舉行了個人獨奏會](../Page/北京.md "wikilink")，在上海與上海交響樂團合作，舉行了[莫札特協奏曲音樂會](../Page/莫札特.md "wikilink")。1958年12月傅聰离开波蘭，没有按计划回到中国大陆，而是移居[英國](../Page/英國.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")。这在当时的中国大陆被看作叛逃行为。1981年平反。

在六、七十年代的二十年間，傅聰舉行了約2,400場獨奏音樂會；與包括[梅紐因](../Page/梅紐因.md "wikilink")、[丹尼爾·巴倫博伊姆](../Page/丹尼爾·巴倫博伊姆.md "wikilink")、[鄭京和等在內的許許多多國際著名演奏家合作過](../Page/鄭京和.md "wikilink")；錄製了約50張唱片；擔任過[蕭邦國際鋼琴比賽](../Page/蕭邦國際鋼琴比賽.md "wikilink")、[比利時伊莉莎白皇太后國際音樂比賽以及](../Page/比利時伊莉莎白皇太后國際音樂比賽.md "wikilink")[挪威](../Page/挪威.md "wikilink")、[義大利](../Page/義大利.md "wikilink")、[瑞士](../Page/瑞士.md "wikilink")、[葡萄牙](../Page/葡萄牙.md "wikilink")、[東南亞等國家和地區的音樂比賽的評委](../Page/東南亞.md "wikilink")；演奏的足跡遍及幾乎整個[歐洲](../Page/歐洲.md "wikilink")、[美洲](../Page/美洲.md "wikilink")、[中東](../Page/中東.md "wikilink")、[東南亞](../Page/東南亞.md "wikilink")、[日本](../Page/日本.md "wikilink")、[大洋洲各地](../Page/大洋洲.md "wikilink")。

1979年，傅聰在[中央音樂學院舉行了音樂會](../Page/中央音樂學院.md "wikilink")。之後，他幾乎每年都回國演奏、講學，已經到過[北京](../Page/北京.md "wikilink")、[上海](../Page/上海.md "wikilink")、[西安](../Page/西安.md "wikilink")、[成都](../Page/成都.md "wikilink")、[昆明等地](../Page/昆明.md "wikilink")。主講過[蕭邦](../Page/蕭邦.md "wikilink")、莫札特、[德彪西等專題](../Page/德彪西.md "wikilink")，演奏過這些作曲家的以及[舒伯特等人的作品](../Page/舒伯特.md "wikilink")。他還與中央樂團合作，演奏了[貝多芬的協奏曲](../Page/貝多芬.md "wikilink")；與中央音樂學院大學生樂隊合作，演奏了莫札特的協奏曲，並兼任指揮；還專門指導過中央音樂學院附中室內樂小組的訓練。

## 榮譽

  - [蕭邦國際鋼琴比賽](../Page/蕭邦國際鋼琴比賽.md "wikilink")，第三名和「[瑪祖卡](../Page/瑪祖卡.md "wikilink")」獎
  - 第四屆[世界青年與學生聯歡節鋼琴比賽三等獎](../Page/世界青年與學生聯歡節.md "wikilink")

## 評價

他的豐富的藝術經驗，孜孜不倦的教學態度，博得廣大師生及音樂愛好者的讚揚和尊敬。他以辛勤的勞動贏得了“有分量的巨匠”的評價。時代週刊亦曾譽其為“當今最偉大的中國音樂家”。

## 參見

  - [蕭邦國際鋼琴比賽](../Page/蕭邦國際鋼琴比賽.md "wikilink")
  - [傅雷](../Page/傅雷.md "wikilink")

## 參考

## 參考資料

  - [Program Notes from a recital at the New England Conservatory,
    October 7, 2000](https://web.archive.org/web/20060615021058/http://chineseperformingarts.net/past_event14.htm)
  - [Program Notes from a recital at the Shanghai Concert Hall,
    April 8, 2006](https://web.archive.org/web/20060527215623/http://www.culture.sh.cn/English/product.asp?id=2065)
  - [傅聪:黑白键上50年（名人专访）April 26,2005](http://www.china-embassy.or.jp/chn/whjl/t193179.htm)

[F傅](../Category/中國鋼琴家.md "wikilink")
[F傅](../Category/英国鋼琴家.md "wikilink")
[F傅](../Category/归化英国公民的中国人.md "wikilink")
[F傅](../Category/上海人.md "wikilink") [C聪](../Category/傅姓.md "wikilink")
[Category:香港大學名譽博士](../Category/香港大學名譽博士.md "wikilink")