**傅莹**（），女，[蒙古族](../Page/蒙古族.md "wikilink")，[内蒙古](../Page/内蒙古.md "wikilink")[通辽人](../Page/通辽.md "wikilink")，[中国职业](../Page/中国.md "wikilink")[外交官](../Page/外交官.md "wikilink")。[北京外国语学院英语系毕业](../Page/北京外国语学院.md "wikilink")，[英国肯特大学](../Page/英国.md "wikilink")[国际关系学硕士研究生](../Page/国际关系学.md "wikilink")。曾任[中华人民共和国外交部副部长](../Page/中华人民共和国外交部.md "wikilink")，是继[王海容之后中国第二位女副外长](../Page/王海容.md "wikilink")。先后担任过驻[菲律宾](../Page/菲律宾.md "wikilink")、驻[澳大利亚](../Page/澳大利亚.md "wikilink")、驻[英国等国](../Page/英国.md "wikilink")[特命全权大使](../Page/特命全权大使.md "wikilink")，是[中国第一位少数民族女](../Page/中华人民共和国.md "wikilink")[大使](../Page/大使.md "wikilink")。曾任[第十二届全国人民代表大会常务委员会委员](../Page/第十二届全国人民代表大会常务委员会.md "wikilink")\[1\]兼[全国人大外事委员会主任委员](../Page/全国人民代表大会外事委员会.md "wikilink")。

## 早年经历

傅莹1953年1月出生在[呼和浩特市](../Page/呼和浩特市.md "wikilink")。她的父亲阿民，是著名蒙古族哲学家[艾思奇的学生](../Page/艾思奇.md "wikilink")，曾官至[中共](../Page/中共.md "wikilink")[内蒙古军区宣传部副部长](../Page/内蒙古.md "wikilink")\[2\]。16岁时，傅莹“响应号召”，到[内蒙古的一个农场进行劳动](../Page/内蒙古.md "wikilink")。1970年，傅莹进入当时的内蒙古生产建设兵团广播站工作，并开始自修高中课程。三年后，她作为“工农兵学员”，被推荐到[北京外国语学院英语系学习](../Page/北京外国语学院.md "wikilink")，并选择第二外语为[法语](../Page/法语.md "wikilink")，后又学习[罗马尼亚语](../Page/罗马尼亚语.md "wikilink")。

## 进入外交部

1977年大学毕业后，傅莹就进入[中华人民共和国外交部工作](../Page/中华人民共和国外交部.md "wikilink")，开始了职业外交生涯；曾先后在[中国驻](../Page/中国.md "wikilink")[罗马尼亚](../Page/罗马尼亚.md "wikilink")[大使馆和外交部翻译室工作](../Page/大使馆.md "wikilink")；1985年被派赴[英国肯特大学进修](../Page/英国.md "wikilink")，并获得国际关系硕士学位。回国后，傅莹又回到翻译室工作，并逐步获得提拔；历任三等秘书、二等秘书、培训处副处长、英文处副处长等职。在此期间，她还曾为[邓小平](../Page/邓小平.md "wikilink")、[杨尚昆](../Page/杨尚昆.md "wikilink")、[江泽民](../Page/江泽民.md "wikilink")、[李鹏等中共高级领导人担任翻译工作](../Page/李鹏.md "wikilink")、陪同出访、参加重要会议等\[3\]。

而傅莹获得的一次重要外交历练，就是在调[外交部亚洲司工作后](../Page/中华人民共和国外交部亚洲司.md "wikilink")，她于1992年至1993年间，赴[柬埔寨参加](../Page/柬埔寨.md "wikilink")[联合国的维和工作](../Page/联合国.md "wikilink")。从[柬埔寨回国后](../Page/柬埔寨.md "wikilink")，傅莹继续在亚洲司工作，先后担任综合处处长、亚洲司参赞，并主管综合调研、东盟事务和亚太安全等工作。1997年，她调任中国驻印度尼西亚使馆，任首席馆员。

1998年11月，[江泽民颁布](../Page/江泽民.md "wikilink")[主席令](../Page/中华人民共和国主席令.md "wikilink")，任命傅莹为[中国驻菲律宾第八任大使](../Page/中华人民共和国驻菲律宾大使.md "wikilink")。这使她成为[中国第一位少数民族女大使](../Page/中国.md "wikilink")，也是当时中国最年轻的女大使\[4\]。一年后，傅莹升任[外交部亚洲司司长](../Page/中华人民共和国外交部亚洲司.md "wikilink")，是该司成立以来的第二位女司长。2004年，她再次出使，任[中国驻澳大利亚第十任大使](../Page/中国驻澳大利亚大使列表.md "wikilink")；三年任期结束后，又改任[中国驻英国大使](../Page/中国驻英国大使.md "wikilink")。2007年6月12日，[英国女王](../Page/英国女王.md "wikilink")[伊丽莎白二世在](../Page/伊丽莎白二世.md "wikilink")[白金汉宫接见了傅莹](../Page/白金汉宫.md "wikilink")\[5\]。

2010年1月，傅莹升任外交部副部长，成为中国第二位女性副外长\[6\]。

2013年3月，傅莹被[第十二届全国人大主席团任命为大会副秘书长](../Page/第十二届全国人民代表大会.md "wikilink")、兼大会发言人，成为全国人大第一位女性发言人；\[7\]在这届人大会议上，她当选为全国人大常务委员、并兼任人大外事委员会主任委员。\[8\]

2018年3月，傅莹改任第十三屆[全國人民代表大會外事委員會副主任委員](../Page/全國人民代表大會外事委員會.md "wikilink")。

## 重要外交经历

  - 1992至1993年间，傅莹参与了[联合国五个常任理事国就有关解决](../Page/联合国.md "wikilink")“[柬埔寨问题](../Page/柬埔寨.md "wikilink")”的谈判工作。
  - 担任[外交部亚洲司司长期间](../Page/中华人民共和国外交部亚洲司.md "wikilink")，经历了东帝汶危机、阿富汗战争、中国与东盟建立战略伙伴关系等事件，并参与处理朝鲜半岛核问题、积极推动六方会谈。
  - 在[澳大利亚担任](../Page/澳大利亚.md "wikilink")[大使期间](../Page/大使.md "wikilink")，经历了駐[悉尼领事馆](../Page/悉尼.md "wikilink")[政治参赞](../Page/政治参赞.md "wikilink")[陈用林](../Page/陈用林.md "wikilink")\[9\]以及原[天津市国安局工作人员](../Page/天津市.md "wikilink")[郝凤军](../Page/郝凤军.md "wikilink")\[10\]等人的叛逃事件，努力消减这两起事件给中国造成的负面政治影响，并积极推动中国和澳大利亚的双边关系。
  - 在[2008年西藏骚乱发生后](../Page/2008年西藏骚乱.md "wikilink")，[北京奥运火炬传递在英国受阻](../Page/2008年夏季奧林匹克運動會聖火傳遞.md "wikilink")，傅莹以中国驻英大使的身份在英国《[每日电讯报](../Page/每日电讯报.md "wikilink")》撰文，对示威者试图夺取火炬的暴力行为加以谴责，并抨击西方传媒企图把中国“妖魔化”；同时，她还警告“中西间了解的鸿沟愈来愈大”；而中国则更需要“保持耐性”，“等候世界了解”\[11\]。
  - 2009年4月，傅瑩和[澳大利亚](../Page/澳大利亚.md "wikilink")[總理](../Page/總理.md "wikilink")[陆克文及](../Page/陆克文.md "wikilink")[英國](../Page/英國.md "wikilink")[外交大臣](../Page/外交及聯邦事務大臣.md "wikilink")[米利班德一同参加](../Page/戴维·米利班德.md "wikilink")[英国广播公司的电视访谈节目](../Page/英国广播公司.md "wikilink")，就中国对[2009年伦敦金融峰会的看法和期待进行阐述](../Page/2009年二十国集团伦敦峰会.md "wikilink")，并为中国的立场进行了辩解。她表示“中国还是一个发展中国家；当西方用‘富裕’、‘有钱’和有大量外汇储备等此来描绘中国时候，中国的民众感到这是在吹捧中国，甚至是在忽悠中国”。\[12\]

## 家庭

傅莹的丈夫[郝时远是](../Page/郝时远.md "wikilink")[内蒙古](../Page/内蒙古.md "wikilink")[武川人](../Page/武川.md "wikilink")，曾任[中国社科院民族研究所所长](../Page/中国社科院.md "wikilink")，是位民族问题专家。两人育有一女。\[13\]

## 参考文献

{{-}}

[Category:中华人民共和国蒙古族高级干部](../Category/中华人民共和国蒙古族高级干部.md "wikilink")
[Category:中华人民共和国女大使](../Category/中华人民共和国女大使.md "wikilink")
[Category:中华人民共和国外交部女副部长](../Category/中华人民共和国外交部女副部长.md "wikilink")
[Category:全国人大外事委员会主任委员](../Category/全国人大外事委员会主任委员.md "wikilink")
[Category:第十二届全国人大常委会委员](../Category/第十二届全国人大常委会委员.md "wikilink")
[Category:內蒙古自治區全國人民代表大會代表](../Category/內蒙古自治區全國人民代表大會代表.md "wikilink")
[Category:蒙古族全國人大代表](../Category/蒙古族全國人大代表.md "wikilink")
[Category:女性全國人大代表](../Category/女性全國人大代表.md "wikilink")
[Category:中華人民共和國駐英國大使](../Category/中華人民共和國駐英國大使.md "wikilink")
[Category:中華人民共和國駐澳大利亞大使](../Category/中華人民共和國駐澳大利亞大使.md "wikilink")
[Category:中華人民共和國駐菲律賓大使](../Category/中華人民共和國駐菲律賓大使.md "wikilink")
[Category:北京外国语大学校友](../Category/北京外国语大学校友.md "wikilink")
[F](../Category/肯特大學校友.md "wikilink")
[Category:呼和浩特人](../Category/呼和浩特人.md "wikilink")
[Category:通辽人](../Category/通辽人.md "wikilink")
[Y](../Category/傅姓.md "wikilink")

1.  [十二届全国人大二次会议新闻发佈会](http://www.apdnews.com/news/73468.html)
    ，亚太日报，2014年3月5日
2.  [中国新任驻英大使傅莹——来自大草原的女外交官](http://cn.chinagate.com.cn/profiles/2007-08/19/content_8709517.htm)
    中国发展门户网
3.  [新浪网：从内蒙古走出去的女大使](http://news.sina.com.cn/s/2007-04-23/173112847017.shtml)
    原载《内蒙古晨报》
4.  [中国外交女双杰：傅莹温柔刚毅
    章启月亲切冷峻](http://www.china.com.cn/chinese/zhuanti/232407.htm)
5.  [中国驻英国大使傅莹向英国女王递交国书并举行到任招待会](http://www.fmprc.gov.cn/chn/wjb/zwjg/zwbd/zybd/t330396.htm)

6.  [中国驻英大使傅莹升任外交部副部长](http://realtime.zaobao.com/2010/01/100104_19.shtml)

7.
8.  [新一届全国人大各专门委员会组成人员确定](http://www.chinanews.com/gn/2013/03-16/4649708.shtml)[傅莹回应15个敏感问题
    全国人大首场发布会有哪些干货？|傅莹|人大常委会|strong_新浪财经_新浪网](http://finance.sina.com.cn/roll/2017-03-04/doc-ifycaasy7549039.shtml)
9.
10.
11.
12.
13.