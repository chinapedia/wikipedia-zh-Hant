**杰克·约翰森**（*Jack
Johnson*，）是一位來自[美國](../Page/美國.md "wikilink")[夏威夷的](../Page/夏威夷.md "wikilink")[歌手](../Page/歌手.md "wikilink")、影片製作人和音樂創作者，同時也是[衝浪好手](../Page/衝浪.md "wikilink")。他的音樂在商業方面獲得很大成功，在發行了第一張[專輯](../Page/音樂專輯.md "wikilink")《*Brushfire
Fairytales*》之後就全心投入音樂創作與演唱。至今已發行了多張EP與專輯，並是[輕柔搖滾樂的代表人物之一](../Page/輕柔搖滾.md "wikilink")。

## 生平

### 早期

傑克·強森出生於[夏威夷](../Page/夏威夷.md "wikilink")[歐胡島](../Page/歐胡島.md "wikilink")，並在那裡長大。他是知名[衝浪家](../Page/衝浪家.md "wikilink")[傑夫·強森的兒子](../Page/傑夫·強森.md "wikilink")，很自然的對衝浪產生了興趣與熱情。強森五歲那年，在他父親的朋友艾力克斯·康威爾（*Alex
Conwell*）與薩那姆·布庫司基的監督下，開始學習[衝浪與](../Page/衝浪.md "wikilink")[滑板運動](../Page/滑板運動.md "wikilink")，並常與他最好的朋友班·史考莫一起去衝浪。十七歲時的傑克已經是衝浪界的奇才，並以最年輕參賽者的身份闖入[世界級衝浪大賽](../Page/世界級衝浪大賽.md "wikilink")（*Pipeline
Masters*）決賽。然而，一場意外結束了傑克短暫的衝浪生涯。在那次衝浪意外中，傑克的手腕關節骨折、門牙斷裂，並在前額和嘴部區域縫了超過150針。<small>\[1\]</small>

在意外後，傑克徹底的對職業衝浪界中的競爭感到心灰意冷。「大夥兒為了爭奪下個大浪而互相殘殺，」強森說道，「我常開玩笑說，我是因為當時撞壞了腦袋才會變得如此柔和，但是我真的覺得那次意外讓我變得更成熟了。」<small>\[2\]</small>

[20090120_Jack_Johnson_at_2009_Obama_Home_States_Inaugural_Ball.JPG](https://zh.wikipedia.org/wiki/File:20090120_Jack_Johnson_at_2009_Obama_Home_States_Inaugural_Ball.JPG "fig:20090120_Jack_Johnson_at_2009_Obama_Home_States_Inaugural_Ball.JPG")就職典禮舞會上的表演\]\]

### 大學與音樂生涯

傑克·強森在發生意外、放棄當一位專業的[衝浪者後](../Page/衝浪.md "wikilink")，他利用在病床上休養的所有時間來撰寫歌曲和演奏[吉他](../Page/吉他.md "wikilink")。艾力克斯同時也教他如何演奏吉他和[長笛](../Page/長笛.md "wikilink")。傑克強森在14歲的時候就學會如何演奏吉他，但直到他[加州大學](../Page/加州大學.md "wikilink")（*University
of California*）的[聖塔芭芭拉分校](../Page/聖塔芭芭拉.md "wikilink")（*Santa
Barbara*）就讀期間才認真的在藝術領域中發展，他也在該所大學中取得了[電影的](../Page/電影.md "wikilink")[學位](../Page/學位.md "wikilink")。畢業之後，傑克·強森四處旅行，並且在1997年至1998年之間協助執導與拍攝了衝浪電影《*Thicker
Than Water*》。他也是一名[素食主義者](../Page/素食主義者.md "wikilink")。

## 私人生活

在加州大學[聖塔芭芭拉分校的期間](../Page/聖塔芭芭拉.md "wikilink")，傑克·強森遇見了現任的妻子金，歌曲 *Do You
Remember* 便是獻給妻子的作品。他們並有了兩個兒子。

## 樂團成員

傑克·強森的樂團成員如下：

  - 傑克·強森 - 主唱，[吉他](../Page/吉他.md "wikilink")
  - 亞當·托普 -
    [鼓](../Page/鼓.md "wikilink")，[打擊樂器](../Page/打擊樂器.md "wikilink")
  - 瑪洛·波多維斯基 - [貝斯](../Page/貝斯.md "wikilink")
  - [柴克·基爾](../Page/柴克·基爾.md "wikilink") -
    [鍵盤](../Page/鍵盤.md "wikilink")，合聲

## 作品

### 音樂作品

#### 專輯

  - [虛擬故事](../Page/虛擬故事.md "wikilink")（*Brushfire
    Fairytales*）<small>（2001年）</small>
  - [永不止息](../Page/永不止息.md "wikilink")（*On and
    On*）<small>（2003年）</small>
  - [仲夏夜之夢](../Page/仲夏夜之夢_\(傑克·強森\).md "wikilink")（*In Between
    Dreams*）<small>（2005年）</small>
  - [夢想國度](../Page/Sleep_Through_the_Static.md "wikilink")（*Sleep
    Through the Static*）<small>（2008年）</small>
  - [來去海邊](../Page/來去海邊.md "wikilink")（*To The
    Sea*）<small>（2010年）</small>
  - [此時‧此地‧有你](../Page/此時‧此地‧有你.md "wikilink")（*From Here To Now To
    You*）<small>（2013年）</small>

#### EP

  - 2001年 *Out Cold*
  - 2005年 *Some Live Songs* EP

#### 演唱會DVD

  - 2005年 *Jack Johnson: Live in Japan*
  - 2005年 *Jack Johnson: A Weekend at the Greek*
  - 2009年 *Jack Johnson: En Concert(歐洲巡迴演唱電影)*

#### 虛擬專輯與EP

  - 2004年 *iTunes Originals* - 傑克·強森
  - 2005年 *Sessions@AOL* — EP

#### 電影/演唱會原聲帶

電影:

  - 2000年 [衝浪高手](../Page/衝浪高手.md "wikilink")（*Thicker than Water*）
  - 2002年 [九月逐浪](../Page/九月逐浪.md "wikilink")（*September Sessions*）：
    *Pirate Looks At 40* 及 *F-Stop Blues*
  - 2005年 *Sprout*
  - 2006年 [聽浪的歌](../Page/聽浪的歌.md "wikilink")（*A Brokedown Melody*）：*Let
    It Be Sung* 及 *Home*
  - 2006年 [好奇喬治在唱歌](../Page/好奇喬治在唱歌.md "wikilink")（*Sing-A-Longs and
    Lullabies for the Film Curious George*）
  - 2007年 [巴布狄倫的七段航程](../Page/巴布狄倫的七段航程.md "wikilink")（*I'm Not
    There*）：*Medley: Mama, You've Been on My Mind/A Fraction of Last
    Thoughts on Woody Guthrie*

演唱會:

  - 2009年 [原音精選](../Page/原音精選.md "wikilink")（*En Concert*）

#### 其他歌曲

  - 1999年 G. Love & Special Sauce《*Philadelphonic*》：*Rodeo Clowns*
  - 2002年 《*Hear Music Volume 7: Waking*》：*Fortunate Fool*
  - 2004年 G. Love & Special Sauce《*The Hustle*》：*Give It to You*
  - 2004年 [唐納文·法蘭肯](../Page/唐納文·法蘭肯.md "wikilink")（*Donavon
    Frankenreiter*）的同名專輯：*Free*
  - 2004年 [帥哥名模養成班](../Page/帥哥名模養成班.md "wikilink")（*Handsome Boy
    Modeling School*）《[先生好白](../Page/先生好白.md "wikilink")》（*White
    People*）：*Breakdown*
  - 2005年 《*Look at All the Love We Found*》：*Badfish* / *Boss DJ*
  - 2005年 [黑眼豆豆](../Page/黑眼豆豆.md "wikilink")（*The Black Eyed
    Peas*）《[黑色猴門企業](../Page/黑色猴門企業.md "wikilink")》（*Monkey
    Business (album)|Monkey Business*）：*Gone Going*
  - 2005年 Animal Liberation Orchestra《*Fly Between Falls*》：*Girl I Wanna
    Lay You Down*
  - 2006年 G. Love & Special Sauce《*Lemonade*》："Rainbow"
  - 2006年 [美景合唱團](../Page/美景合唱團.md "wikilink")（*Buena Vista Social
    Club*）《*Rhythms del Mundo: Cuba*》：*Better Together*
  - 2006年 《*The Acoustic Album*》：*Breakdown*
  - 2007年 《*Instant Karma: The Amnesty International Campaign to Save
    Darfur*》：翻唱[約翰·藍儂的](../Page/約翰·藍儂.md "wikilink")*Imagine*
  - 2007年 《*Stockings By the Fire*》：*Rudolph the Red-Nosed Reindeer*
  - 2008年 《*This Warm December: A Brushfire Holiday*》：*Rudolph the Red
    Nosed Reindeer*　及　*Someday at Christmas*

### 電影作品

傑克強森也透過他自己的唱片公司「Moonshine Conspiracy Records」所屬的電影公司「Moonshine
Conspiracy Films」幫忙製作了多部電影。這家公司最近已改名為「WoodShed Films」。他們的作品包括：

  - *Thicker Than Water*
  - *The Seedling*
  - *Sprout*
  - *Shelter*
  - [九月逐浪](../Page/九月逐浪.md "wikilink")（*September Sessions*）
  - [聽浪的歌](../Page/聽浪的歌.md "wikilink")（*A Broke Down Melody*）

## 參考文獻

## 相關網站

  - [傑克強森官方網站](http://www.jackjohnsonmusic.com/)
  - [傑克強森詳細資料](https://web.archive.org/web/20071008020537/http://www.music-city.org/Jack-Johnson/)
  - [Brokedown Melodies](http://www.brokedownmelodies.com/)
  - [傑克強森歌詞](http://www.lyricsdir.com/jack-johnson-lyrics.html)
  - [夏威夷Kokua基金會](http://www.kokuahawaiifoundation.org/)（傑克強森創立的基金會）
  - [Transworld
    Surf雜誌的報導](https://web.archive.org/web/20060820065205/http://www.transworldsurf.com/surf/features/article/0,19929,1033589,00.html)

[Category:美國素食主義者](../Category/美國素食主義者.md "wikilink")
[Category:1975年出生](../Category/1975年出生.md "wikilink")
[Category:美國男歌手](../Category/美國男歌手.md "wikilink")
[Category:美國民謠歌手](../Category/美國民謠歌手.md "wikilink")
[Category:美國搖滾歌手](../Category/美國搖滾歌手.md "wikilink")
[Category:美國作曲家](../Category/美國作曲家.md "wikilink")
[Category:美國吉他手](../Category/美國吉他手.md "wikilink")
[Category:衝浪運動員](../Category/衝浪運動員.md "wikilink")
[Category:夏威夷人](../Category/夏威夷人.md "wikilink")
[Category:創作歌手](../Category/創作歌手.md "wikilink")
[Category:衝浪音樂](../Category/衝浪音樂.md "wikilink")
[Category:烏克麗麗演奏家](../Category/烏克麗麗演奏家.md "wikilink")
[Category:聖塔芭芭拉加州大學校友](../Category/聖塔芭芭拉加州大學校友.md "wikilink")
[Category:日本富士搖滾音樂祭參加歌手](../Category/日本富士搖滾音樂祭參加歌手.md "wikilink")

1.  [專訪傑克·強森](https://web.archive.org/web/20020212140310/http://www.geocities.com/jackjohnsononline/interviews.htm)
2.  [滾石雜誌封面故事:傑克·強森](http://www.rollingstone.com/news/story/18684236/cover_story_jack_johnson__the_dude_abides/5)