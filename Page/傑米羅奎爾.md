[Jamiroquai-JayKay.jpg](https://zh.wikipedia.org/wiki/File:Jamiroquai-JayKay.jpg "fig:Jamiroquai-JayKay.jpg")
[Jay_Kay_Jamiroquai.jpg](https://zh.wikipedia.org/wiki/File:Jay_Kay_Jamiroquai.jpg "fig:Jay_Kay_Jamiroquai.jpg")
**傑米羅奎爾**（[英文](../Page/英文.md "wikilink")：Jamiroquai），[英國流行樂團](../Page/英國.md "wikilink")。在全世界銷售了2000萬張唱片，並且在出道後的14年期間佔據了162周的英國單曲榜名次。據主唱Jay
Kay本人透露，其個人財產高達4000萬英磅（相當25億新台幣），並登上2004年英國Sunday
Times的富豪榜第950名（此榜單為英國國內的富豪榜）。

## Jamiroquai 傑米羅奎爾

在和[Sony簽約前](../Page/Sony.md "wikilink")，傑米羅奎爾發行第一張單曲《When You Gonna
Learn?》，以[Acid
Jazz的曲風在](../Page/Acid_Jazz.md "wikilink")1992出道，席捲了[英國](../Page/英國.md "wikilink")190萬張。新潮又帶點復古的Funk/Acid
Jazz繁複曲風，廣受[英國大眾的喜愛](../Page/英國.md "wikilink")。在第一張成績不俗的單曲之後，得到了[Sony公司的垂青](../Page/Sony.md "wikilink")，和[Sony
BMG Music
Entertainment簽下了八張唱片的合約](../Page/Sony_BMG_Music_Entertainment.md "wikilink")。
第一張由[Sony發行的專輯](../Page/Sony.md "wikilink")《Emergency on Planet
Earth》在1993發行。緊接著1994發行了第二張專輯《The Return of the Space
Cowboy》，其中〈Space Cowboy〉一曲攻進英美排行榜，各舞廳也廣為傳唱，一時知名度大增。

真正讓傑米羅奎爾紅遍全世界的專輯，是在1996發行的第三張《Travelling Without Moving》專輯。其中〈Virtual
Insanity〉和〈Cosmic Girl〉紅遍了全世界。〈Virtual
Insanity〉的MV更是贏得了[MTV錄影帶大獎的](../Page/MTV錄影帶大獎.md "wikilink")【最佳音樂影片】，【最佳特效】，【最佳影視技術】，【最佳突破音樂影片】的殊榮。影片內容有主唱Jay
Kay的精準舞步和卷軸地板創新的電腦特效。[〈Virtual
Insanity〉影片連結（Youtube）](https://www.youtube.com/watch?v=4JkIs37a2JE)

之後1998發行的電影「[酷斯拉](../Page/酷斯拉.md "wikilink")(Godzilla)」主題曲〈Deeper
Underground〉更為英國的音樂排行榜冠軍。另一首單曲〈Canned Heat〉也成為了「舞國英雄傳Center Stage
(2000)」和「拿破崙炸藥Napoleon Dynamite (2004)」兩部電影的主題曲。另外〈Feels Just Like It
Should〉一曲更成為了「FIFA 2006」遊戲的主題曲。

Acid Jazz的曲風在發行前三張專輯後，因為主唱Jay
Kay的偏好下(主唱本身也是個[霹靂舞高手](../Page/霹靂舞.md "wikilink"))，在1999發行的《Synkronized》專輯後漸漸轉向Funk/Jazz的曲風。
有趣的是，這張專輯是在主唱Jay
Kay位在[Buckinghamshire](../Page/Buckinghamshire.md "wikilink")，有500年歷史，11間臥房的豪宅（也可稱作城堡）庭院內錄製，以和大自然最真誠的《Synckronize》。

媒體很喜歡關心主唱Jay
Kay對於名車的收藏和豪宅的購置，但其實他本身更對於環保有相當的投入。除了給[Greenpaece和](../Page/Greenpaece.md "wikilink")[Friends
Of The
Earth等組織大量的金錢奧援](../Page/Friends_Of_The_Earth.md "wikilink")，也投身遊民基金會甚深。

2001年的《A Funk Odyssey》尤其可以感受到濃濃的Funk/Jazz的味道。而這第五張專輯的首支單曲〈Little
L〉更在許多國家搶下了流行音樂排行榜冠軍的冠軍。

第六張《Dynamite》專輯在2005的六月發行，其專輯奪得了[英國流行音樂排行榜第三名](../Page/英國.md "wikilink")。

2006年11月，傑米羅奎爾發行了他們在Sony BMG公司旗下八張專輯的精選集《High Times: Singles
1992-2006》。一發行便直衝[英國流行榜冠軍](../Page/英國.md "wikilink")，在[日本更奪得第四名](../Page/日本.md "wikilink")。這張專輯除了精選以外，更加了兩首新歌〈Runaway〉和〈Radio〉。

在2006的三月，傑米羅奎爾和[Columbia
Records簽約](../Page/Columbia_Records.md "wikilink")，2007以後的專輯將轉到[Columbia
Records發行](../Page/Columbia_Records.md "wikilink")。

## 靈魂人物 主唱Jay Kay生平

[英國](../Page/英國.md "wikilink")[倫敦出生的Jay在](../Page/倫敦.md "wikilink")[猶太裔母親的撫養下長大](../Page/猶太.md "wikilink")，他的母親是一位[英國爵士pub的主唱](../Page/英國.md "wikilink")，甚至還有自己的電視節目。他到2003年，才去看他的葡萄牙親生父親，而從小一直是在繼父James
Royal身邊。就讀於Oakham School in
Rutland，13歲那年還跟繼父到[泰國住了半年](../Page/泰國.md "wikilink")。

當Jay在青少年時期得知他有一個雙胞胎哥哥，在出生後六週就死亡，而且現在看到的父親也並非親生。他的個性開始變得叛逆，在學校惹是生非，甚至在15歲決定翹家。在翹家後，到還沒找到正經工作之前，Jay是個街上的無業遊民，以偷東西和打一些零工為生。

到了1989年，Jay在廢車場找到了一台沒有電系，而且車頂破一個洞的汽車，轉而靠這台破車當起了計程車司機。不過之後他被捲入了一個他沒有參與的竊盜案，並被起訴而且逮捕。

風波過後Jay決定回到他人生的避風港－－家，度過人生的低潮。決定開始著手一些自己有興趣的工作，開始用著陽春的配備寫歌。不久後他加入了「to
be」樂團並改名成「Jamiroquai」。「Jamiroquai」這個團名取自於一個[美國的部落名](../Page/美國.md "wikilink")「[Iroquois](../Page/Iroquois.md "wikilink")」和爵士樂最重要元素－－「Improvisation」（即興），並開啟了長達14年，甚至更多的Funk/Jazz/Disco的傳奇神話。

### Jay Kay和Sony BMG互槓

Jay Kay相當不爽Sony
BMG商業化的作風。甚至在接受[雪梨晨報訪問的時候說](../Page/雪梨.md "wikilink")「這些日子只有18%的心力在音樂上，其他82%都在幹一些無關緊要的狗屁（意指商業活動）」。而2006年發行的精選輯也只是為了完成那份八張專輯的合約罷了。

許許多多的消息都指出Jay
Kay即將在《Dynamite》的巡迴以後有退休的打算，過著平靜的生活，找個老婆、生個小孩、組個家庭，開著他數不清的超級跑車，安享他的下半輩子。

### Jay Kay與媒體互槓

Jay Kay和媒體關係一向很不好，許多例子有跡可循：

  - 2001，他被控告在酒吧外揍了記者一頓，而且扯爛了他的[相機](../Page/相機.md "wikilink")。
  - 2002年，他又在倫敦街上揍了一名記者一頓。
  - 2006年10月，又在[倫敦酒吧外連揍了兩個記者](../Page/倫敦.md "wikilink")。

## Jay Kay與他的超級跑車

　　Jay Kay的興趣主要就是收藏許許多多的超級跑車，目前已經有25輛之多。"Travelling without
Moving"一專輯的封面便是用樂團的標誌牛角人搭配法拉利的標誌組合而成，背後更搭配上[法拉利獨特的碳纖維紋路](../Page/法拉利.md "wikilink")。
〈Cosmic Girl〉這首歌的MV [Cosmic
Girl音樂影片（Youtube）](http://www.youtube.com/watch?v=evSMfV455QM)
更是拍攝了他開Ferrari和Lamborghini的英姿。不過Jay也因愛開跑車，遭到許多次短暫吊銷駕照和付出許多超速罰鍰。

**收藏列表**

  - Aston Martin DB5
  - Audi RS4
  - Audi RS6
  - Ferrari 550
  - Ferrari F355
  - Ferrari Mondial
  - [法拉利F40](../Page/法拉利F40.md "wikilink")
  - Ferrari F50
  - Ferrari Enzo
  - Ferrari La Ferrari
  - Fiat 1000 Abarth (sport version of Fiat 600)
  - Lamborghini Diablo SE30
  - Lamborghini Miura
  - Maserati A6 GCS FRUA
  - Mercedes-Benz G-Class
  - Mercedes-Benz 600 (W100) a large luxury automobile of the 1960s
  - Range Rover
  - Rolls-Royce Phantom

## 歷年單曲排行

| Year | Song                                                                                                                                                                 | <small>[UK singles](../Page/UK_Singles_Chart.md "wikilink")<small> | <small>[U.S. Dance](../Page/Hot_Dance_Club_Play.md "wikilink")<small> | Album                                                                                                                                                                         |
| :--: | :------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :----------------------------------------------------------------: | :-------------------------------------------------------------------: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1992 | "[When you Gonna Learn](../Page/When_you_Gonna_Learn.md "wikilink")"                                                                                                 |                                 52                                 |                                  \-                                   | *[Emergency on Planet Earth](../Page/Emergency_on_Planet_Earth.md "wikilink")*                                                                                                |
| 1993 | "[Too Young to Die](../Page/Too_Young_to_Die_\(Jamiroquai_song\).md "wikilink")"                                                                                     |                                 10                                 |                                  \-                                   | *[Emergency on Planet Earth](../Page/Emergency_on_Planet_Earth.md "wikilink")*                                                                                                |
| 1993 | "[Blow Your Mind](../Page/Blow_Your_Mind_\(Jamiroquai_song\).md "wikilink")"                                                                                         |                                 12                                 |                                  \-                                   | *[Emergency on Planet Earth](../Page/Emergency_on_Planet_Earth.md "wikilink")*                                                                                                |
| 1993 | "[Emergency on Planet Earth (song)](../Page/Emergency_on_Planet_Earth_\(song\).md "wikilink")"                                                                       |                                 32                                 |                                   4                                   | *[Emergency on Planet Earth](../Page/Emergency_on_Planet_Earth.md "wikilink")*                                                                                                |
| 1993 | "[When you Gonna Learn](../Page/When_you_Gonna_Learn.md "wikilink")" *(re-release)*                                                                                  |                                 28                                 |                                  \-                                   | *[Emergency on Planet Earth](../Page/Emergency_on_Planet_Earth.md "wikilink")*                                                                                                |
| 1994 | "[Space Cowboy](../Page/Space_Cowboy.md "wikilink")"                                                                                                                 |                                 17                                 |                                   1                                   | *[The Return of the Space Cowboy](../Page/The_Return_of_the_Space_Cowboy.md "wikilink")*                                                                                      |
| 1994 | "[Half the Man](../Page/Half_the_Man_\(Jamiroquai_song\).md "wikilink")"                                                                                             |                                 15                                 |                                  \-                                   | *[The Return of the Space Cowboy](../Page/The_Return_of_the_Space_Cowboy.md "wikilink")*                                                                                      |
| 1995 | "[Stillness in Time](../Page/Stillness_in_Time.md "wikilink")"                                                                                                       |                                 9                                  |                                  \-                                   | *[The Return of the Space Cowboy](../Page/The_Return_of_the_Space_Cowboy.md "wikilink")*                                                                                      |
| 1995 | "[Light Years](../Page/Light_Years.md "wikilink")"                                                                                                                   |                                 \-                                 |                                   6                                   | *[The Return of the Space Cowboy](../Page/The_Return_of_the_Space_Cowboy.md "wikilink")*                                                                                      |
| 1995 | "[The Kids](../Page/The_Kids_\(Jamiroquai_song\).md "wikilink")"                                                                                                     |                                 \-                                 |                                  \-                                   | *[The Return of the Space Cowboy](../Page/The_Return_of_the_Space_Cowboy.md "wikilink")*                                                                                      |
| 1995 | "[Morning Glory](../Page/Morning_Glory_\(single\).md "wikilink")" *(promo)*                                                                                          |                                 \-                                 |                                  \-                                   | *[The Return of the Space Cowboy](../Page/The_Return_of_the_Space_Cowboy.md "wikilink")*                                                                                      |
| 1996 | "[Do You Know Where You're Coming From](../Page/Do_You_Know_Where_You're_Coming_From.md "wikilink")" *([M-Beat](../Page/M-Beat.md "wikilink") featuring Jamiroquai)* |                                 12                                 |                                  \-                                   | *[The Return of the Space Cowboy \*Bonus Track\*](../Page/The_Return_of_the_Space_Cowboy_*Bonus_Track*.md "wikilink")*                                                        |
| 1997 | "[Virtual Insanity](../Page/Virtual_Insanity.md "wikilink")"                                                                                                         |                                 3                                  |                                  34                                   | *[Travelling Without Moving](../Page/Travelling_Without_Moving.md "wikilink")*                                                                                                |
| 1996 | "[Cosmic Girl](../Page/Cosmic_Girl.md "wikilink")"                                                                                                                   |                                 6                                  |                                   7                                   | *[Travelling Without Moving](../Page/Travelling_Without_Moving.md "wikilink")*                                                                                                |
| 1997 | "[Alright](../Page/Alright_\(Jamiroquai_song\).md "wikilink")"                                                                                                       |                                 6                                  |                                   7                                   | *[Travelling Without Moving](../Page/Travelling_Without_Moving.md "wikilink")*                                                                                                |
| 1996 | "[High Times](../Page/High_Times_\(song\).md "wikilink")"                                                                                                            |                                 20                                 |                                   9                                   | *[Travelling Without Moving](../Page/Travelling_Without_Moving.md "wikilink")*                                                                                                |
| 1998 | "[Deeper Underground](../Page/Deeper_Underground.md "wikilink")"                                                                                                     |                                 1                                  |                                  22                                   | *[Godzilla](../Page/Godzilla_\(1998\).md "wikilink") Soundtrack/[Synkronized](../Page/Synkronized.md "wikilink")/[A Funk Odyssey](../Page/A_Funk_Odyssey.md "wikilink") (JP)* |
| 1999 | "[Canned Heat](../Page/Canned_Heat_\(song\).md "wikilink")"                                                                                                          |                                 4                                  |                                   1                                   | *[Synkronized](../Page/Synkronized.md "wikilink")*                                                                                                                            |
| 1999 | "[Supersonic](../Page/Supersonic_\(Jamiroquai_song\).md "wikilink")"                                                                                                 |                                 22                                 |                                   1                                   | *[Synkronized](../Page/Synkronized.md "wikilink")*                                                                                                                            |
| 1999 | "[King for a Day](../Page/King_for_a_Day_\(song\).md "wikilink")"                                                                                                    |                                 20                                 |                                  \-                                   | *[Synkronized](../Page/Synkronized.md "wikilink")*                                                                                                                            |
| 2001 | "[I'm in the Mood for Love](../Page/I'm_in_the_Mood_for_Love.md "wikilink")" *(with [Jools Holland](../Page/Jools_Holland.md "wikilink"))*                           |                                 29                                 |                                  \-                                   | *-*                                                                                                                                                                           |
| 2001 | "[An Online Odyssey](../Page/An_Online_Odyssey.md "wikilink")" *(10,000 copy non-album promo)*                                                                       |                                 \-                                 |                                  \-                                   | \-''Outtake from [Synkronized](../Page/Synkronized.md "wikilink"), promoting the [A Funk Odyssey](../Page/A_Funk_Odyssey.md "wikilink") album.                                |
| 2001 | "[Little L](../Page/Little_L.md "wikilink")"                                                                                                                         |                                 5                                  |                                   2                                   | *[A Funk Odyssey](../Page/A_Funk_Odyssey.md "wikilink")*                                                                                                                      |
| 2001 | "[You Give me Something](../Page/You_Give_me_Something_\(Jamiroquai_song\).md "wikilink")"                                                                           |                                 16                                 |                                   2                                   | *[A Funk Odyssey](../Page/A_Funk_Odyssey.md "wikilink")*                                                                                                                      |
| 2001 | "[Love Foolosophy](../Page/Love_Foolosophy.md "wikilink")"                                                                                                           |                                 14                                 |                                  \-                                   | *[A Funk Odyssey](../Page/A_Funk_Odyssey.md "wikilink")*                                                                                                                      |
| 2002 | "[Corner of the Earth](../Page/Corner_of_the_Earth.md "wikilink")"                                                                                                   |                                 31                                 |                                  \-                                   | *[A Funk Odyssey](../Page/A_Funk_Odyssey.md "wikilink")*                                                                                                                      |
| 2002 | "[Main Vein](../Page/Main_Vein.md "wikilink")"                                                                                                                       |                                 \-                                 |                                  \-                                   | *[A Funk Odyssey](../Page/A_Funk_Odyssey.md "wikilink")*                                                                                                                      |
| 2005 | "[Feels Just Like It Should](../Page/Feels_Just_Like_It_Should.md "wikilink")"                                                                                       |                                 8                                  |                                   1                                   | *[Dynamite](../Page/Dynamite_\(Jamiroquai_album\).md "wikilink")*                                                                                                             |
| 2005 | "[Seven Days in Sunny June](../Page/Seven_Days_in_Sunny_June.md "wikilink")"                                                                                         |                                 14                                 |                                  \-                                   | *[Dynamite](../Page/Dynamite_\(Jamiroquai_album\).md "wikilink")*                                                                                                             |
| 2005 | "[(Don't) Give Hate a Chance](../Page/\(Don't\)_Give_Hate_a_Chance.md "wikilink")"                                                                                   |                                 27                                 |                                  \-                                   | *[Dynamite](../Page/Dynamite_\(Jamiroquai_album\).md "wikilink")*                                                                                                             |
| 2005 | "Hollywood Swingin'" *([Kool & the Gang](../Page/Kool_&_the_Gang.md "wikilink") featuring Jamiroquai)*                                                               |                                 \-                                 |                                   5                                   | *[Kool & the Gang's The Hits: Reloaded](../Page/Kool_&_the_Gang's_The_Hits:_Reloaded.md "wikilink")*                                                                          |
| 2006 | "[Runaway](../Page/Runaway_\(Jamiroquai_song\).md "wikilink")"                                                                                                       |                                 18                                 |                                   1                                   | *[High Times: Singles 1992-2006](../Page/High_Times:_Singles_1992-2006.md "wikilink")*                                                                                        |
|      |                                                                                                                                                                      |                                                                    |                                                                       |                                                                                                                                                                               |

### 混音專輯

  - "Space Cowboy" (2006) vinyl and download-only remix single, \#71 UK
  - "Deeper Underground" (2006) vinyl and download-only remix single
  - "Cosmic Girl" (2006) vinyl and download-only remix single
  - "Love Foolosophy" (2006) vinyl and download-only remix single, \#166
    UK

## 樂團成員

  - [Jason Kay](../Page/Jason_Kay.md "wikilink") - Vocals
  - [Rob Harris](../Page/Rob_Harris_\(Guitarist\).md "wikilink") -
    Guitar
  - [Derrick McKenzie](../Page/Derrick_McKenzie.md "wikilink") - Drums
  - [Paul Turner](../Page/Paul_Turner.md "wikilink") - Bass
  - [Sola Akingbola](../Page/Sola_Akingbola.md "wikilink") - Percussion
  - [Matt Johnson](../Page/Matt_Johnson_\(keyboardist\).md "wikilink") -
    Keyboards
  - [Lorraine McIntosh](../Page/Lorraine_McIntosh.md "wikilink") -
    Backing Vocals
  - [Hazel Fernandez](../Page/Hazel_Fernandez.md "wikilink") - Backing
    Vocals
  - [Sam Smith](../Page/Sam_Smith.md "wikilink") - Backing Vocals

## 歷屆團員

  - [Toby Smith](../Page/Toby_Smith.md "wikilink") - Keyboards
    (1992-2002)
  - [Wallis Buchanan](../Page/Wallis_Buchanan.md "wikilink") -
    Didgeridoo (1992-2001)
  - [Gavin Dodds](../Page/Gavin_Dodds.md "wikilink") - Guitar
    (1993-1994)
  - [Simon Katz](../Page/Simon_Katz.md "wikilink") - Guitar (1995-2000)
  - [Stuart Zender](../Page/Stuart_Zender.md "wikilink") - Bass
    (1993-1998)
  - [Nick Fyffe](../Page/Nick_Fyffe.md "wikilink") - Bass (1998-2003)
  - [Nick Van Gelder](../Page/Nick_Van_Gelder.md "wikilink") - Drums
    (1993)
  - [Darren Galea](../Page/Darren_Galea.md "wikilink") aka DJ D-Zire -
    Turntables (1993-2001)
  - [Adrian Revell](../Page/Adrian_Revell.md "wikilink") - Flute,
    Saxophone
  - [Winston Rollins](../Page/Winston_Rollins.md "wikilink") - Trombone
  - [Simon Carter](../Page/Simon_Carter.md "wikilink") - Keyboards
    (1999-2001)

特別來賓

  - Andrew Levy - Bass (1992)
  - Simon Bartholomew - Guitar (1992)
  - Martin Shaw - Trumpet
  - [Beverley Knight](../Page/Beverley_Knight.md "wikilink") - Vocals
    (2001)

## 相關網站

  - [英國官方網站－Jamiroquai Website](http://www.jamiroquai.co.uk/)
  - [日本官方網站－JAMIROQUAI OFFICIAL WEBSITE](http://www.jamiroquai.jp/)

[Category:英國歌手](../Category/英國歌手.md "wikilink")
[Category:英國樂團](../Category/英國樂團.md "wikilink")
[Category:電子音樂團體](../Category/電子音樂團體.md "wikilink")