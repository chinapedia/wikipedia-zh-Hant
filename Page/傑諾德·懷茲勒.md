**傑諾德·懷茲勒**（Jerrold Wexler，），是一位美國商人。他是[Haskell
Wexler的哥哥也是](../Page/Haskell_Wexler.md "wikilink")
[戴露·漢娜的繼父](../Page/戴露·漢娜.md "wikilink")。\[1\]

他30年前以建造紀念摩天大樓聞名。\[2\] 自1979年開始他和他的商業伙伴 Edward W. Ross, 買下了
[達克旅館](../Page/:en:Drake_Hotel_\(Chicago\).md "wikilink").\[3\]1980年達克成為美国[國家級歷史性建築](../Page/:en:National_Register_of_Historic_Place.md "wikilink")。1983年
Wexler將興趣移轉到[玩伴女郎大楼](../Page/:en:Playboy_Building.md "wikilink")、湖心塔（[Lake
Point
Tower](../Page/:en:Lake_Point_Tower.md "wikilink")）、[密西根大街的建築房地](../Page/密西根大街_\(芝加哥\).md "wikilink")，數以打計的芝加哥旅館，他還在在紐約市和洛杉磯置產，救助了當時即將破產的
[Goldblatt's](../Page/Goldblatt's.md "wikilink").\[4\] 當[Bertram
Lee](../Page/Bertram_Lee.md "wikilink") 在10%的併購案失敗之後failed to come up
with his ten percent share to purchase the [National Basketball
Association](../Page/National_Basketball_Association.md "wikilink")
[Denver Nuggets](../Page/Denver_Nuggets.md "wikilink") in 1989, [Peter
Bynoe](../Page/Peter_Bynoe.md "wikilink") recruited Jerrold Wexler and
[Jay Pritzker](../Page/Jay_Pritzker.md "wikilink")
讓生意繼續保持活絡。這個團隊在1992年以很大的興趣
[COMSAT](../Page/COMSAT.md "wikilink") 買下了 [Drexel Burnham
Lambert銀行](../Page/Drexel_Burnham_Lambert.md "wikilink"), Bynoe
和他的夥伴們。\[5\]除了他在商業上的興趣之外，他也是一名活耀的政治獻金捐助者。\[6\] In addition此外,
他同時也是John Austin Cheley 基金會的受託人。.\[7\]

## 注釋

<div class="references-small">

<references />

</div>

1.
2.
3.
4.
5.
6.
7.