[Low_Temperature_Oxidation_Catalyst.jpeg](https://zh.wikipedia.org/wiki/File:Low_Temperature_Oxidation_Catalyst.jpeg "fig:Low_Temperature_Oxidation_Catalyst.jpeg")。\]\]
[Activation_energy.svg](https://zh.wikipedia.org/wiki/File:Activation_energy.svg "fig:Activation_energy.svg")
**催化劑**又稱**觸媒**，是能透過提供另一[活化能較低的反應途徑而加快](../Page/活化能.md "wikilink")[化學反應速率](../Page/反應速率.md "wikilink")，而本身的質量、組成和化學性質在參加[化學反應前後保持不變的物質](../Page/化學反應.md "wikilink")。\[1\]例如[二氧化錳可以作為](../Page/二氧化錳.md "wikilink")[過氧化氫](../Page/過氧化氫.md "wikilink")（雙氧水）分解的催化劑。\[2\]與催化劑相反，能減慢反應速率的物質稱為[抑制劑](../Page/反應抑制劑.md "wikilink")。過去曾用的「負催化劑」一詞已不被[國際純粹與應用化學聯合會所接受](../Page/國際純粹與應用化學聯合會.md "wikilink")，而必須改用[抑制劑一詞](../Page/抑制劑.md "wikilink")，催化劑一詞僅指能加快反應速率的物質。\[3\]

## 催化劑與反應

催化劑與反應誘導化學反應發生改變，而使化學反應變快或者在較低的[溫度環境下進行化學反應](../Page/溫度.md "wikilink")。

催化剂加速反应过程一般具有两个途径，一个是增加反应物的活性中心，另一种是改变反应途径。

我們可在[波茲曼分布](../Page/波茲曼分布.md "wikilink")（Boltzmann
distribution）與能量關係圖（energy profile
diagram）中觀察到，催化劑可使化學[反應物在不改變的情形下](../Page/反應物.md "wikilink")，經由只需較少[活化能](../Page/活化能.md "wikilink")（activation
energy）的路徑來進行化學反應。而通常在這種能量下，[分子不是無法完成化學反應](../Page/分子.md "wikilink")，不然就是需要較長[時間來完成化學反應](../Page/時間.md "wikilink")。但在有催化劑的環境下，分子只需較少的能量即可完成化學反應。

## 催化劑的種類

[Zeolite-ZSM-5-vdW.png](https://zh.wikipedia.org/wiki/File:Zeolite-ZSM-5-vdW.png "fig:Zeolite-ZSM-5-vdW.png")的微孔分子結構被利用於煉油廠。\]\]
[Ceolite_nax.JPG](https://zh.wikipedia.org/wiki/File:Ceolite_nax.JPG "fig:Ceolite_nax.JPG")
[Verbrennung_eines_Zuckerwürfels.png](https://zh.wikipedia.org/wiki/File:Verbrennung_eines_Zuckerwürfels.png "fig:Verbrennung_eines_Zuckerwürfels.png")
催化劑分[均相催化劑與](../Page/均相催化劑.md "wikilink")[非均相催化劑](../Page/非均相催化劑.md "wikilink")。非均相催化劑呈現在不同[相](../Page/相態.md "wikilink")（Phase）的反應中（如：[固態催化劑在](../Page/固態.md "wikilink")[液態混合反應和](../Page/液態.md "wikilink")[固態催化劑在](../Page/固態.md "wikilink")[氣態混合反應等](../Page/氣態.md "wikilink")），而均相催化劑則是呈現在同一相的反應（例如：液態催化劑在液態混合反應）。一個簡易的非均相催化反應包含了反應物（或-{zh-hant:[受質](../Page/受質.md "wikilink");
zh-cn:[底物](../Page/底物.md "wikilink");}-）吸附在催化劑的表面，反應物內的[鍵因十分的脆弱而導致新的鍵產生](../Page/化学键.md "wikilink")，但又因產物與催化劑間的鍵並不牢固，而使產物出現。目前已知許多[表反應發生](../Page/表反應.md "wikilink")[吸附反應的不同可能性的結構位置](../Page/吸附.md "wikilink")。一般生活中較常用非均相催化劑，利用不同狀態的特性較好分離出產物與催化物（如汽機車排氣管中的觸媒）。　

## 参阅

  - [催化](../Page/催化.md "wikilink")
  - [相转移催化剂](../Page/相转移催化剂.md "wikilink")

## 参考资料

[\*](../Category/催化剂.md "wikilink")
[Category:化学动力学](../Category/化学动力学.md "wikilink")

1.
2.   p40
3.