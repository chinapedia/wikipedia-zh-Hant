[Value_Chain.png](https://zh.wikipedia.org/wiki/File:Value_Chain.png "fig:Value_Chain.png")
**價值鏈**（Value
chain），又名**價值鏈分析**、**價值鏈模型**等。由[迈克尔·波特在](../Page/迈克尔·波特.md "wikilink")1985年，於《競爭優勢》一書中提出的。波特指出[企業要發展獨特的](../Page/企業.md "wikilink")[競爭優勢](../Page/競爭優勢.md "wikilink")，要為其商品及[服務創造更高附加](../Page/服務.md "wikilink")[價值](../Page/價值.md "wikilink")，[商業](../Page/商業.md "wikilink")[策略是結構企業的經營模式](../Page/戰略.md "wikilink")（流程），成為一系列的增值過程，而此一連串的增值流程，就是「價值鏈」。

一般企業的價值鏈主要分為：

1.  主要活動（Primary Activities），包括企業的核心生產與銷售程序：
      - 進貨[物流](../Page/物流.md "wikilink")（Inbound
        Logistics），即来料储运，缔属[资源市场](../Page/资源市场.md "wikilink")
      - 製造營運（Operations），即[加工生产](../Page/加工生产.md "wikilink")，缔属[制造商市场](../Page/制造商市场.md "wikilink")
      - 出貨物流（Outbound
        Logistics），即成品[储运](../Page/储运.md "wikilink")，缔属[中间商市场](../Page/中间商市场.md "wikilink")
      - [市場行銷](../Page/市場.md "wikilink")（Marketing and
        Sales），即[市场营销](../Page/市场营销.md "wikilink")（[4P](../Page/4P.md "wikilink")），缔属[消费者市场](../Page/消费者市场.md "wikilink")
      - 售後服務（After sales service）
      - 以上为产生[价值的环节](../Page/价值.md "wikilink")。
2.  支援活動（Support Activities），包括支援核心營運活動的其他活動，又稱共同運作環節：
      - [人力資源管理](../Page/人力資源管理.md "wikilink")（Human resources
        management）training
      - 技術發展（Technology
        development），即[技术研发](../Page/技术研发.md "wikilink")（[R\&D](../Page/R&D.md "wikilink")）
      - [採購](../Page/採購.md "wikilink")（Procurement），即采购管理
      - 以上活动利于[资产评估](../Page/资产评估.md "wikilink")，为辅助性增值环节。

## 相關

  - [成本優勢](../Page/成本優勢.md "wikilink")（Cost advantage）
  - [差異化](../Page/差異化.md "wikilink")（Differentiation）
  - [競爭者及潛在競爭者的分析](../Page/競爭者.md "wikilink")
  - [價值系統](../Page/價值系統.md "wikilink")（value system）包括：
      - [供應商價值鏈](../Page/供應商.md "wikilink")
      - 企業價值鏈
      - 通路價值鏈
      - 消費者價值鏈
  - [產業鏈](../Page/產業鏈.md "wikilink")（industry chain）
  - [微笑曲線](../Page/微笑曲線.md "wikilink")
  - 整體產業分析：[PEST分析](../Page/PEST分析.md "wikilink")、[產品生命週期曲線定位](../Page/產品生命週期.md "wikilink")、產業鏈分析、價值系統分析，微笑曲線分析。
  - 個體企業分析：價值鏈分析，[五力分析](../Page/五力分析.md "wikilink")，[BCG矩陣分析](../Page/BCG矩陣.md "wikilink")，[GE矩陣分析](../Page/GE矩陣.md "wikilink")，[SWOT分析](../Page/SWOT分析.md "wikilink")。
  - [外包非核心業務](../Page/外包.md "wikilink")（Outsourcing）
  - 為省成本，必須刪除無增值的環節（Process re-engineering）

## 外部参考

  - [價值鏈、價值系統、產業鏈與微笑曲線](https://web.archive.org/web/20070528043940/http://cdnet.stpi.org.tw/techroom/analysis/pat_A030.htm)

[Category:市場學](../Category/市場學.md "wikilink")
[Category:商業](../Category/商業.md "wikilink")
[Category:管理學](../Category/管理學.md "wikilink")