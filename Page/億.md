**億**是一個[中文數字](../Page/中文數字.md "wikilink")，是[漢字文化圏常用的](../Page/漢字.md "wikilink")[數字](../Page/數字.md "wikilink")[單位](../Page/單位.md "wikilink")。現在於[日本](../Page/日本.md "wikilink")、[中国](../Page/中国.md "wikilink")、[中華民國](../Page/中華民國.md "wikilink")、[南韓和](../Page/南韓.md "wikilink")[北韓使用](../Page/北韓.md "wikilink")，數值相當於100000000（10<sup>8</sup>）。

## 億的定義

中文數字中，億最初是[一](../Page/一.md "wikilink")、[十](../Page/十.md "wikilink")、[百](../Page/百.md "wikilink")、[千](../Page/千.md "wikilink")、[万後續](../Page/万.md "wikilink")10倍的數字單位[下數](../Page/下數.md "wikilink")，「億」的數值本是10<sup>5</sup>。自[漢代起改為](../Page/漢.md "wikilink")10<sup>8</sup>，可見於『[漢書](../Page/漢書.md "wikilink")』律歴志「一億三千四百八萬二千二百九十七」的數值可見。

億亦可稱之為萬萬，數值亦是10<sup>8</sup>。

[越南語的億](../Page/越南語.md "wikilink")（）數值為10<sup>5</sup>的意思。10<sup>8</sup>在越南於為「」（翻譯為一百兆、兆的百萬倍）。

億的單位與上下數可參考下表。

| [數量級](../Page/数量级_\(数\).md "wikilink")         | 十進・下數                                 | 萬進・中數  | 平方・上數  |
| ---------------------------------------------- | ------------------------------------- | ------ | ------ |
| [10<sup>4</sup>](../Page/1_E4.md "wikilink")   | [萬](../Page/10000.md "wikilink")      | 一萬     | 一萬     |
| [10<sup>8</sup>](../Page/1_E8.md "wikilink")   | **億**                                 | 十萬     | 十萬     |
| [10<sup>12</sup>](../Page/1_E12.md "wikilink") | [兆](../Page/兆.md "wikilink")          | 百萬     | 百萬     |
| [10<sup>16</sup>](../Page/1_E16.md "wikilink") | [京](../Page/京_\(數字單位\).md "wikilink") | 千萬     | 千萬     |
| [10<sup>20</sup>](../Page/1_E20.md "wikilink") | 垓                                     | 一**億** | 一**億** |
| [10<sup>24</sup>](../Page/1_E24.md "wikilink") | 秭                                     | 十億     | 十億     |
| [10<sup>28</sup>](../Page/1_E28.md "wikilink") | 穣                                     | 百億     | 百億     |
| [10<sup>32</sup>](../Page/1_E32.md "wikilink") | 溝                                     | 千億     | 千億     |
| [10<sup>36</sup>](../Page/1_E36.md "wikilink") | 澗                                     | 一兆     | 一萬億    |
| [10<sup>40</sup>](../Page/1_E40.md "wikilink") | 正                                     | 十兆     | 十萬億    |
| [10<sup>44</sup>](../Page/1_E44.md "wikilink") | 載                                     | 百兆     | 百萬億    |
| [10<sup>48</sup>](../Page/1_E48.md "wikilink") | 極                                     | 千兆     | 千萬億    |
| [10<sup>52</sup>](../Page/1_E52.md "wikilink") | 恒河沙                                   | 一京     | 一兆     |

## 漢数字的「億」

漢字的「億」是「意」之擬聲字，與「[{{JIS2004フォント](../Page/人部.md "wikilink")」相合。

億本來是一個「很大的數字」，本來沒有一個實際的數值。

## 相關條目

  - [命数法](../Page/命数法.md "wikilink")
  - [數字列表](../Page/數字列表.md "wikilink")
  - [數字比較](../Page/數字比較.md "wikilink")

[中](../Category/数字.md "wikilink")
[Category:東亞傳統數學](../Category/東亞傳統數學.md "wikilink")