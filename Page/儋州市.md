**州市**（[邮政式拼音](../Page/邮政式拼音.md "wikilink")：、）是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[海南省下辖的](../Page/海南省.md "wikilink")[地級市](../Page/地級市.md "wikilink")，位於海南省西北部，瀕臨[北部灣](../Page/北部灣.md "wikilink")，是全省西部的經濟、交通、通信和文化中心。儋州也是中华人民共和国实际管辖的所有地级市中，唯一没有立法权的地级市。

## 历史沿革

[西汉時在此設立](../Page/西汉.md "wikilink")[儋耳郡](../Page/儋耳郡.md "wikilink")；[南朝梁改为义论县](../Page/梁_\(南朝\).md "wikilink")。[唐朝置儋州于今市西北的](../Page/唐朝.md "wikilink")[新州镇](../Page/新州镇_\(儋州市\).md "wikilink")。[北宋改为宜伦县](../Page/北宋.md "wikilink")；[明朝入儋州](../Page/明朝.md "wikilink")。

1912年改为儋县，1959年移置那大镇，1993年3月3日撤銷儋縣並改设县级儋州市。

2015年2月19日，撤銷縣級儋州市並改设地级儋州市，以原县级儋州市的行政区域为地级儋州市的行政区域。\[1\]

## 地理

## 政治

### 现任领导

<table>
<caption>儋州市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党儋州市委员会.md" title="wikilink">中国共产党<br />
儋州市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/儋州市人民代表大会.md" title="wikilink">儋州市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/儋州市人民政府.md" title="wikilink">儋州市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议儋州市委员会.md" title="wikilink">中国人民政治协商会议<br />
儋州市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/袁光平.md" title="wikilink">袁光平</a>[2]</p></td>
<td><p><a href="../Page/王克强.md" title="wikilink">王克强</a>[3]</p></td>
<td><p><a href="../Page/朱洪武_(1971年).md" title="wikilink">朱洪武</a>[4]</p></td>
<td><p><a href="../Page/谢雄峰.md" title="wikilink">谢雄峰</a>[5]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p><a href="../Page/黎族.md" title="wikilink">黎族</a></p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/重庆市.md" title="wikilink">重庆市</a></p></td>
<td><p><a href="../Page/河南省.md" title="wikilink">河南省</a><a href="../Page/桐柏县.md" title="wikilink">桐柏县</a></p></td>
<td><p><a href="../Page/海南省.md" title="wikilink">海南省</a><a href="../Page/澄迈县.md" title="wikilink">澄迈县</a></p></td>
<td><p>海南省<a href="../Page/儋州市.md" title="wikilink">儋州市</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2017年12月</p></td>
<td><p>2016年11月</p></td>
<td><p>2016年11月</p></td>
<td><p>2016年11月</p></td>
</tr>
</tbody>
</table>

### 行政区划

辖16个镇：[那大镇](../Page/那大镇.md "wikilink")、[南丰镇](../Page/南丰镇_\(儋州市\).md "wikilink")、[雅星镇](../Page/雅星镇.md "wikilink")、[和庆镇](../Page/和庆镇.md "wikilink")、[大成镇](../Page/大成镇_\(儋州市\).md "wikilink")、[新州镇](../Page/新州镇_\(儋州市\).md "wikilink")、[光村镇](../Page/光村镇.md "wikilink")、[东成镇](../Page/东成镇_\(儋州市\).md "wikilink")、[中和镇](../Page/中和镇_\(儋州市\).md "wikilink")、[峨蔓镇](../Page/峨蔓镇.md "wikilink")、[兰洋镇](../Page/兰洋镇.md "wikilink")、[王五镇](../Page/王五镇.md "wikilink")、[排浦镇](../Page/排浦镇.md "wikilink")、[海头镇](../Page/海头镇_\(儋州市\).md "wikilink")、[木棠镇](../Page/木棠镇.md "wikilink")、[白马井镇](../Page/白马井镇.md "wikilink")，境内有[洋浦经济开发区及国营八一总场](../Page/洋浦经济开发区.md "wikilink")、国营蓝洋农场、国营西联农场、国营西培农场4个国有农场。

<table>
<thead>
<tr class="header">
<th><p>儋州市行政区划</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><div style="position: relative font-size:70%" class="center">
</div></td>
</tr>
<tr class="even">
<td><p>区划名称</p></td>
</tr>
<tr class="odd">
<td><p>那大镇 {{#tag:ref|包括国营西联农场|name=西联|group=注}}</p></td>
</tr>
<tr class="even">
<td><p>和庆镇</p></td>
</tr>
<tr class="odd">
<td><p>南丰镇</p></td>
</tr>
<tr class="even">
<td><p>大成镇{{#tag:ref|包括国营西培农场和华南热带作物科学研究院|name=西培-华南热作研院|group=注}}</p></td>
</tr>
<tr class="odd">
<td><p>雅星镇{{#tag:ref|包括国营八一农场|name=八一|group=注}}</p></td>
</tr>
<tr class="even">
<td><p>兰洋镇{{#tag:ref|包括国营蓝洋农场|name=蓝洋|group=注}}</p></td>
</tr>
<tr class="odd">
<td><p>光村镇</p></td>
</tr>
<tr class="even">
<td><p>木棠镇</p></td>
</tr>
<tr class="odd">
<td><p>海头镇</p></td>
</tr>
<tr class="even">
<td><p>峨蔓镇</p></td>
</tr>
<tr class="odd">
<td><p>三都镇{{#tag:ref|已整体划入<a href="../Page/洋浦经济开发区.md" title="wikilink">洋浦经济开发区并设立</a><a href="../Page/三都街道.md" title="wikilink">三都街道</a>|name=洋浦|group=注}}</p></td>
</tr>
<tr class="even">
<td><p>王五镇</p></td>
</tr>
<tr class="odd">
<td><p>白马井镇</p></td>
</tr>
<tr class="even">
<td><p>中和镇</p></td>
</tr>
<tr class="odd">
<td><p>排浦镇</p></td>
</tr>
<tr class="even">
<td><p>东成镇</p></td>
</tr>
<tr class="odd">
<td><p>新州镇</p></td>
</tr>
</tbody>
</table>

## 人口

截止2013年，儋州市人口104万，是海南省人口第二大城市。

儋州民族众多，语言复杂。有[汉族](../Page/汉族.md "wikilink")、[黎族](../Page/黎族.md "wikilink")、[苗族等](../Page/苗族.md "wikilink")20多个民族，其中汉族人口占93%。主要语言有[粤语](../Page/粤语.md "wikilink")、[海南话](../Page/海南话.md "wikilink")、[客家话](../Page/客家语.md "wikilink")、[军话](../Page/军家话.md "wikilink")、[临高话等](../Page/臨高語.md "wikilink")，语言交流主要以普通话和粤语[儋州话为主](../Page/儋州话.md "wikilink")。

## 交通

### 公路

  - [G98](../Page/海南地区环线高速公路.md "wikilink")、[225国道](../Page/225国道.md "wikilink")、S308、S306、S307、S315

### 铁路

  - [海南西环铁路](../Page/海南西环铁路.md "wikilink")：儋州站、八一站
  - [海南西环高速铁路](../Page/海南西环高速铁路.md "wikilink")：[银滩站](../Page/银滩站.md "wikilink")、[白马井站](../Page/白马井站.md "wikilink")、[海头站](../Page/海头站.md "wikilink")

### 港口

  - 洋浦港
  - 白马井港

### 机场

  - 儋州西庆通用机场（军用、通用）
  - 儋州国际机场（在建，预计至2018年底建成）

## 教育资源

### 高等院校

  - [海南大学儋州校区](../Page/海南大学.md "wikilink")
  - 中国热带农业科学院

### 中学

  - 海南省洋浦中学
  - 儋州市第一中学
  - 儋州市第二中学
  - 儋州市第三中学
  - 儋州市第四中学
  - 儋州市第五中学
  - 儋州市双龙中学
  - 儋州市洛基中学
  - 儋州市川锦中学
  - 儋州市八一中学
  - 儋州市富克中学
  - 儋州市雅星中学

## 名胜古迹

  - [东坡书院](../Page/东坡书院.md "wikilink")：位于儋州市中和镇东郊，属[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")，是北宋时期[苏轼居儋期间讲学场所](../Page/苏轼.md "wikilink")，历代均有维修或扩建。享有“天南名胜”之美誉。

## 友好关系

儋州市已经在世界上建立了1对友城关系和1对国际友好交流关系\[6\]。

### 友城关系

<table>
<thead>
<tr class="header">
<th><p>城市</p></th>
<th><p>所属<a href="../Page/一级行政区.md" title="wikilink">一级行政区</a></p></th>
<th><p>国家</p></th>
<th><p>结好日期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/莱克伍德_(华盛顿州).md" title="wikilink">莱克伍德市</a>[7]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Washington.svg" title="fig:Flag_of_Washington.svg">Flag_of_Washington.svg</a> <a href="../Page/华盛顿州.md" title="wikilink">华盛顿州</a></p></td>
<td></td>
<td><p>2007.06.28</p></td>
</tr>
</tbody>
</table>

### 国际友好交流关系

<table>
<thead>
<tr class="header">
<th><p>城市</p></th>
<th><p>所属一级行政区</p></th>
<th><p>国家</p></th>
<th><p>结好日期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/下龙市.md" title="wikilink">下龙市</a>[8]</p></td>
<td><p><a href="../Page/广宁省.md" title="wikilink">广宁省</a></p></td>
<td></td>
<td><p>2009.07.09</p></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部链接

  - [儋州市政府网站](http://www.danzhou.gov.cn/)
  - [儋州新闻网](https://web.archive.org/web/20120501185702/http://danzhou365.com/)

[儋州市](../Page/category:儋州市.md "wikilink")
[category:海南地级市](../Page/category:海南地级市.md "wikilink")

[琼](../Category/中国中等城市.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.