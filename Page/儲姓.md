**儲姓**是一個中文姓氏，在《[百家姓](../Page/百家姓.md "wikilink")》中排第211位。
宋朝时期，储姓不足1万，在全国主要分布于福建、江苏、河南、浙江等地，福建为储姓第一大省，约占全国储姓总人口的33%。明朝时期，储姓大约有2万6千人，江苏为储姓第一大省，约占全国储姓总人口的41%。其次分布于安徽、浙江、宁夏、江西、湖北、湖南等地。当代储姓的人口大约有23万，为第二百六十七位大姓姓氏，大约占全国人口的o·018%。储姓在全国的分布目前主要集中于安徽，其次分布于江苏、上海、湖南、浙江、天津、陕西等地。

## 起源

  - 储（Chǔ）姓源出有二：

## 名人

  - [儲敦敘](../Page/儲敦敘.md "wikilink")：北宋晉江人，字彥倫，著有《玉泉集》。
  - [儲光羲](../Page/儲光羲.md "wikilink")：唐代兗州人，任汜水尉，官至監察御史。
  - [儲安平](../Page/儲安平.md "wikilink")：《观察》杂志主编。
  - [儲波](../Page/儲波.md "wikilink")：中國共產黨、中華人民共和國政治人物，曾任湖南省省長、內蒙古自治區黨委書記、自治區人大常委會主任。
  - [儲祥生](../Page/儲祥生.md "wikilink")：中華民國 - 華南投顧董事長，證券投資顧問業人物。
  - [儲用](../Page/儲用.md "wikilink")：宋代学者，在朝廷做官，为官有惠政，曾得朱熹的赞赏。他从书本上得到治理下的道理，又在实践中加以运用，他又一心为民，这样，就取得了很好的政绩，为百姓带来许多好处。当时的大哲学家朱熹，非常夸奖他。
  - [儲珊](../Page/儲珊.md "wikilink")：字朝珍，南直隶颖州（治今安徽省阜阳）人，明弘治十二年（1499年）进士，十七年（1504年）任新乡县（河南省）知县。正德六年（1511年）八月知州储珊主修《颍州志》脱稿，共6卷。
  - [儲欣](../Page/儲欣.md "wikilink")：字同人，清朝宜兴人。自幼好学，精通经史。早年无意仕途，以制艺为业。直到60岁，始领康熙乡荐，一试礼部不遇，遂闭门著书。著有《春秋指掌》30卷，《在陆草堂集》六卷。选编《唐宋十家文全集录》51
    卷。
  - [儲尹之](../Page/儲尹之.md "wikilink")：字思弼，号赤岩，宋元祐三年（1088年）进士，官台州刺史，任满后定居宁海。
  - [儲秘书](../Page/儲秘书.md "wikilink")：宜兴人，清代学者。乾隆皇帝时的进士，在河南郑州任知府，他博览经史，性淡泊，为官顺应天意民心，老百姓感到很自在，他自己也有时间著书立说。

## 地望分佈

  - [江苏省](../Page/江苏省.md "wikilink")[宜兴市一帶](../Page/宜兴市.md "wikilink")
  - [安徽省](../Page/安徽省.md "wikilink")[安庆市](../Page/安庆市.md "wikilink")[潜山县一带](../Page/潜山县.md "wikilink")
  - [山西省](../Page/山西省.md "wikilink")[夏縣一帶](../Page/夏縣.md "wikilink")

[C儲](../Category/漢字姓氏.md "wikilink") [\*](../Category/储姓.md "wikilink")