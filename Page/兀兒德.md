[thumb](../Page/FILE:Urd_magazine.jpg.md "wikilink")
**烏-{}-爾德**（；），又譯**兀-{}-兒德**，是[諾倫三女神其中之一](../Page/諾倫三女神.md "wikilink")（最年長的一位）。她的名字意味著「過去」\[1\]，同时也被叫做死亡的女神（Dís
of Death）。

她的另外两个姐妹分别是[薇兒丹蒂和](../Page/薇兒丹蒂.md "wikilink")[斯庫爾德](../Page/斯庫爾德.md "wikilink")。和[希腊神话中的](../Page/希腊神话.md "wikilink")[命运三女神一样](../Page/命运三女神.md "wikilink")，她们也无法改变命运。例如烏爾德只能警告[奥丁](../Page/奥丁.md "wikilink")，他将会在神之劫难中死于苍狼[芬里厄之手](../Page/芬里厄.md "wikilink")，她们也无法阻止未来即将要发生的事。

## 兀兒德之泉

以兀兒德的名字命名的聖井是[兀兒德之泉](../Page/兀兒德之泉.md "wikilink")，位于[世界之樹](../Page/世界之樹.md "wikilink")（Yggdrasil）的伊格德拉修树根底下，是众神每日聚会的场地。兀兒德之泉是神聖之泉，具有淨化一切的力量，亦可以永生。被認為是源自巨人[密米爾的頭部](../Page/密米爾.md "wikilink")，同時也是世界之樹賴以灌溉的源泉。

## 參見条目

  - [諾倫三女神](../Page/諾倫三女神.md "wikilink")
  - [薇兒丹蒂](../Page/薇兒丹蒂.md "wikilink")
  - [詩寇蒂](../Page/詩寇蒂.md "wikilink")

## 大眾文化

  - [藤島康介的動漫作品](../Page/藤島康介.md "wikilink")《[幸運女神](../Page/幸運女神.md "wikilink")》
  - 手機遊戲《[Puzzle &
    Dragons](../Page/Puzzle_&_Dragons.md "wikilink")》NO.1669
  - 手機遊戲《[神魔之塔](../Page/神魔之塔.md "wikilink")》NO.481
  - 手機遊戲《[旅遊大亨](../Page/旅遊大亨.md "wikilink")》三大時間女神卡片之一

## 参考文献

<references />

[U](../Category/時間與命運之神.md "wikilink")
[U](../Category/北歐女神.md "wikilink")

1.