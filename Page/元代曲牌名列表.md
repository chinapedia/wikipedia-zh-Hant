**曲牌**，是传统填词制谱用的曲调调名的统称，俗称“牌子”。
[元曲共计](../Page/元曲.md "wikilink")15宫，447支曲牌。

曲牌為填寫該散曲所依據的[樂譜及定式](../Page/樂譜.md "wikilink")，決定該曲的曲調、唱法、字數、句數、句子長短、[平仄及](../Page/平仄.md "wikilink")[韻腳等](../Page/韻腳.md "wikilink")。

## [黄钟宫](../Page/黄钟宫.md "wikilink")

  - [醉花阴](../Page/醉花阴.md "wikilink")\[1\]
  - [喜迁莺](../Page/喜迁莺.md "wikilink")\[2\]
  - [出队子](../Page/出队子.md "wikilink")\[3\]
  - [刮地风](../Page/刮地风.md "wikilink")\[4\]
  - [四门子](../Page/四门子.md "wikilink")\[5\]
  - [水仙子](../Page/水仙子.md "wikilink")
  - [塞雁儿](../Page/塞雁儿.md "wikilink")
  - [节节高](../Page/节节高.md "wikilink")
  - [寨儿令](../Page/寨儿令.md "wikilink")
  - [六么令](../Page/六么令.md "wikilink")
  - [九条龙](../Page/九条龙.md "wikilink")

## 正宫

  - [端正好](../Page/端正好.md "wikilink")\[6\]
  - [滚绣球](../Page/滚绣球.md "wikilink")\[7\]
  - [叨叨令](../Page/叨叨令.md "wikilink")\[8\]
  - [倘秀才](../Page/倘秀才.md "wikilink")\[9\]
  - [醉太平](../Page/醉太平.md "wikilink")
  - [呆骨朵](../Page/呆骨朵.md "wikilink")
  - [伴读书](../Page/伴读书.md "wikilink")
  - [笑和尚](../Page/笑和尚.md "wikilink")
  - [芙蓉花](../Page/芙蓉花.md "wikilink")
  - [鹦鹉曲](../Page/鹦鹉曲.md "wikilink")
  - [甘草子](../Page/甘草子.md "wikilink")
  - [汉东山](../Page/汉东山.md "wikilink")

## [仙吕宫](../Page/仙吕宫.md "wikilink")

  - [端正好](../Page/端正好.md "wikilink")
  - [赏花时](../Page/赏花时.md "wikilink")
  - [混江龙](../Page/混江龙.md "wikilink")
  - [油葫芦](../Page/油葫芦.md "wikilink")
  - [天下乐](../Page/天下乐.md "wikilink")
  - [元和令](../Page/元和令.md "wikilink")
  - [上马娇](../Page/上马娇.md "wikilink")
  - [游四门](../Page/游四门.md "wikilink")
  - [哪吒令](../Page/哪吒令.md "wikilink")
  - [鹊踏枝](../Page/鹊踏枝.md "wikilink")
  - [忆王孙](../Page/忆王孙.md "wikilink")
  - [玉花秋](../Page/玉花秋.md "wikilink")
  - [青哥兒](../Page/青哥兒.md "wikilink")

## [南吕宫](../Page/南吕宫.md "wikilink")

  - [一枝花](../Page/一枝花.md "wikilink")
  - [牧羊关](../Page/牧羊关.md "wikilink")
  - [感皇恩](../Page/感皇恩.md "wikilink")
  - [采茶歌](../Page/采茶歌.md "wikilink")
  - [乌夜啼](../Page/乌夜啼.md "wikilink")
  - [贺新郎](../Page/贺新郎.md "wikilink")
  - [四块玉](../Page/四块玉.md "wikilink")
  - [鹌鹑儿](../Page/鹌鹑儿.md "wikilink")
  - [楚天秋](../Page/楚天秋.md "wikilink")
  - [虾蟆序](../Page/虾蟆序.md "wikilink")
  - [醉乡春](../Page/醉乡春.md "wikilink")

## [中吕宫](../Page/中吕宫.md "wikilink")

  - [粉蝶儿](../Page/粉蝶儿.md "wikilink")
  - [醉春风](../Page/醉春风.md "wikilink")
  - [迎仙客](../Page/迎仙客.md "wikilink")
  - [石榴花](../Page/石榴花.md "wikilink")
  - [快活三](../Page/快活三.md "wikilink")
  - [满庭芳](../Page/满庭芳.md "wikilink")
  - [贺圣朝](../Page/贺圣朝.md "wikilink")
  - [红芍药](../Page/红芍药.md "wikilink")
  - [播海令](../Page/播海令.md "wikilink")
  - [古竹马](../Page/古竹马.md "wikilink")

## [道宫](../Page/道宫.md "wikilink")

  - [凭阑人](../Page/凭阑人.md "wikilink")
  - [美中美](../Page/美中美.md "wikilink")
  - [大圣乐](../Page/大圣乐.md "wikilink")

## [大石调](../Page/大石调.md "wikilink")

  - [念奴娇](../Page/念奴娇.md "wikilink")
  - [百字令](../Page/百字令.md "wikilink")
  - [六国朝](../Page/六国朝.md "wikilink")
  - [还京乐](../Page/还京乐.md "wikilink")
  - [催拍子](../Page/催拍子.md "wikilink")
  - [荼縻香](../Page/荼縻香.md "wikilink")
  - [蓦山溪](../Page/蓦山溪.md "wikilink")
  - [女冠子](../Page/女冠子.md "wikilink")
  - [玉翼蝉](../Page/玉翼蝉.md "wikilink")
  - [鹧鸪天](../Page/鹧鸪天.md "wikilink")
  - [喜梧桐](../Page/喜梧桐.md "wikilink")

## [小石调](../Page/小石调.md "wikilink")

  - [恼杀人](../Page/恼杀人.md "wikilink")
  - [伊州遍](../Page/伊州遍.md "wikilink")
  - [青杏儿](../Page/青杏儿.md "wikilink")
  - [天上谣](../Page/天上谣.md "wikilink")

## [般涉调](../Page/般涉调.md "wikilink")

  - [麻婆子](../Page/麻婆子.md "wikilink")
  - [墙头花](../Page/墙头花.md "wikilink")
  - [耍孩儿](../Page/耍孩儿.md "wikilink")
  - [瑶台月](../Page/瑶台月.md "wikilink")

## [商角调](../Page/商角调.md "wikilink")

  - [黄莺儿](../Page/黄莺儿.md "wikilink")
  - [踏莎行](../Page/踏莎行.md "wikilink")
  - [盖天旗](../Page/盖天旗.md "wikilink")
  - [垂丝钓](../Page/垂丝钓.md "wikilink")
  - [应天长](../Page/应天长.md "wikilink")

## [高平调](../Page/高平调.md "wikilink")

  - [木兰花](../Page/木兰花.md "wikilink")
  - [唐多令](../Page/唐多令.md "wikilink")
  - [于飞乐](../Page/于飞乐.md "wikilink")
  - [青玉案](../Page/青玉案.md "wikilink")

## [商调](../Page/商调.md "wikilink")

  - [集贤宾](../Page/集贤宾.md "wikilink")
  - [逍遥乐](../Page/逍遥乐.md "wikilink")
  - [金菊香](../Page/金菊香.md "wikilink")
  - [醋葫芦](../Page/醋葫芦.md "wikilink")
  - [贤圣吉](../Page/贤圣吉.md "wikilink")
  - [满堂春](../Page/满堂春.md "wikilink")

## [越调](../Page/越调.md "wikilink")

  - [半鹌鹑](../Page/半鹌鹑.md "wikilink")
  - [金蕉叶](../Page/金蕉叶.md "wikilink")
  - [小桃红](../Page/小桃红.md "wikilink")
  - [圣药王](../Page/圣药王.md "wikilink")
  - [雪中梅](../Page/雪中梅.md "wikilink")
  - [眉儿弯](../Page/眉儿弯.md "wikilink")
  - [送远行](../Page/送远行.md "wikilink")
  - [黄蔷薇](../Page/黄蔷薇.md "wikilink")
  - [庆元贞](../Page/庆元贞.md "wikilink")
  - [青山口](../Page/青山口.md "wikilink")
  - [看花回](../Page/看花回.md "wikilink")
  - [南乡子](../Page/南乡子.md "wikilink")
  - [梅花引](../Page/梅花引.md "wikilink")
  - [郓州春](../Page/郓州春.md "wikilink")
  - [天淨沙](../Page/天淨沙.md "wikilink")

## [双调](../Page/双调.md "wikilink")

  - [新水令](../Page/新水令.md "wikilink")
  - [驻马听](../Page/驻马听.md "wikilink")
  - [雁儿落](../Page/雁儿落.md "wikilink")
  - [得胜归](../Page/得胜归.md "wikilink")
  - [锦上花](../Page/锦上花.md "wikilink")
  - [碧玉箫](../Page/碧玉箫.md "wikilink")
  - [搅铮琶](../Page/搅铮琶.md "wikilink")
  - [清江引](../Page/清江引.md "wikilink")
  - [步步娇](../Page/步步娇.md "wikilink")
  - [庆宣和](../Page/庆宣和.md "wikilink")
  - [殿前喜](../Page/殿前喜.md "wikilink")
  - [凤引雏](../Page/凤引雏.md "wikilink")
  - [天仙子](../Page/天仙子.md "wikilink")
  - [醉春风](../Page/醉春风.md "wikilink")
  - [快活年](../Page/快活年.md "wikilink")
  - [朝元乐](../Page/朝元乐.md "wikilink")
  - [海天晴](../Page/海天晴.md "wikilink")
  - [楚天遥](../Page/楚天遥.md "wikilink")
  - [川拨棹](../Page/川拨棹.md "wikilink")
  - [七弟兄](../Page/七弟兄.md "wikilink")
  - [梅花酒](../Page/梅花酒.md "wikilink")
  - [小阳关](../Page/小阳关.md "wikilink")
  - [早乡词](../Page/早乡词.md "wikilink")
  - [石竹子](../Page/石竹子.md "wikilink")
  - [庆丰年](../Page/庆丰年.md "wikilink")
  - [秋莲曲](../Page/秋莲曲.md "wikilink")
  - [落风花](../Page/落风花.md "wikilink")
  - [珍珠马](../Page/珍珠马.md "wikilink")
  - [挂打灯](../Page/挂打灯.md "wikilink")

## 參考文獻

[Category:元曲](../Category/元曲.md "wikilink")
[Category:中国艺术列表‎](../Category/中国艺术列表‎.md "wikilink")

1.

2.
3.
4.
5.
6.

7.
8.
9.