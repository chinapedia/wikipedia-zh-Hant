**元凤**（元年：[前80年八月](../Page/前80年.md "wikilink") -
末年：[前75年](../Page/前75年.md "wikilink")）是[汉昭帝的第二个](../Page/汉昭帝.md "wikilink")[年号](../Page/年号.md "wikilink")。汉朝使用元凤这个年号一共六年。

## 大事记

  - 始元七年（[前80年](../Page/前80年.md "wikilink")）八月改元元鳳\[1\]。

## 出生

## 逝世

## 纪年

| 元凤                               | 元年                                 | 二年                                 | 三年                                 | 四年                                 | 五年                                 | 六年                                 |
| -------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- |
| [公元](../Page/公元纪年.md "wikilink") | [前80年](../Page/前80年.md "wikilink") | [前79年](../Page/前79年.md "wikilink") | [前78年](../Page/前78年.md "wikilink") | [前77年](../Page/前77年.md "wikilink") | [前76年](../Page/前76年.md "wikilink") | [前75年](../Page/前75年.md "wikilink") |
| [干支](../Page/干支纪年.md "wikilink") | [辛丑](../Page/辛丑.md "wikilink")     | [壬寅](../Page/壬寅.md "wikilink")     | [癸卯](../Page/癸卯.md "wikilink")     | [甲辰](../Page/甲辰.md "wikilink")     | [乙巳](../Page/乙巳.md "wikilink")     | [丙午](../Page/丙午.md "wikilink")     |

## 文內注釋

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")

[Category:西汉年号](../Category/西汉年号.md "wikilink")
[Category:前1世纪年号](../Category/前1世纪年号.md "wikilink")
[Category:前80年代中国](../Category/前80年代中国.md "wikilink")
[Category:前70年代中国](../Category/前70年代中国.md "wikilink")

1.  [:s:資治通鑑/卷023](../Page/:s:資治通鑑/卷023.md "wikilink")