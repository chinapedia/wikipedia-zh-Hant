**元和**是[日本的](../Page/日本.md "wikilink")[年號之一](../Page/年號.md "wikilink")，在[慶長之後](../Page/慶長.md "wikilink")、[寬永之前](../Page/寬永.md "wikilink")，指的是從[1615年到](../Page/1615年.md "wikilink")[1624年這段期間](../Page/1624年.md "wikilink")。這個時代的[天皇是](../Page/天皇.md "wikilink")[後水尾天皇](../Page/後水尾天皇.md "wikilink")\[1\]，[江戶幕府的將軍是](../Page/江戶幕府.md "wikilink")[德川秀忠](../Page/德川秀忠.md "wikilink")、[德川家光](../Page/德川家光.md "wikilink")。

## 改元

  - 慶長二十年7月13日（[西元](../Page/西元.md "wikilink")[1615年](../Page/1615年.md "wikilink")[9月5日](../Page/9月5日.md "wikilink")）
    因為[後水尾天皇即位與戰亂](../Page/後水尾天皇.md "wikilink")（[大坂之役](../Page/大坂之役.md "wikilink")）等災難而改元。
  - 元和十年2月30日（西元[1624年](../Page/1624年.md "wikilink")[4月17日](../Page/4月17日.md "wikilink")）
    改元寬永。

## 出典

依照[德川家康的命令](../Page/德川家康.md "wikilink")，採用[唐憲宗的年號](../Page/唐憲宗.md "wikilink")[元和](../Page/元和.md "wikilink")。唐憲宗討平[諸侯](../Page/諸侯.md "wikilink")，終結[藩鎮割據](../Page/藩鎮割據.md "wikilink")，史稱「[元和中興](../Page/元和中興.md "wikilink")」，當時大坂之役已經結束，家康的意思是，結束長時間的戰國紛爭。

## 元和年間大事

  - 元年

:\* 7月
制定[武家諸法度](../Page/武家諸法度.md "wikilink")、[禁中並公家諸法度](../Page/禁中並公家諸法度.md "wikilink")。

:\*[7月9日](../Page/七月初九.md "wikilink")（9月1日）：德川家康下令拆卸位於京都的[豐國神社](../Page/豐國神社.md "wikilink")\[2\]。

  - 2年

:\*
[松平忠輝遭到改易](../Page/松平忠輝.md "wikilink")。此外，限制[英國與](../Page/英國.md "wikilink")[荷蘭的船隻的貿易港僅限於](../Page/荷蘭.md "wikilink")[平戶與](../Page/平戶.md "wikilink")[長崎](../Page/長崎.md "wikilink")。

  - 5年

:\*
[福島正則遭到改易](../Page/福島正則.md "wikilink")。設置[大坂城代](../Page/大坂城代.md "wikilink")。[紀伊德川家成立](../Page/紀伊德川家.md "wikilink")。

  - 6年

:\*6月6日，德川秀忠的女兒[德川和子嫁進為](../Page/德川和子.md "wikilink")[後水尾天皇的中宮](../Page/後水尾天皇.md "wikilink")。\[3\]

:\*
[江戶城的](../Page/江戶城.md "wikilink")[本丸](../Page/本丸.md "wikilink")、[北丸進行改建工程](../Page/北丸.md "wikilink")。

  - 8年

:\* [本多正純遭到改易](../Page/本多正純.md "wikilink")。

  - 9年

:\*7月[德川家光就任三代將軍](../Page/德川家光.md "wikilink")。制定[大奧法度](../Page/大奧.md "wikilink")。[松平忠直遭到改易](../Page/松平忠直.md "wikilink")。

  - 10年

:\*[3月24日因為禁教的問題](../Page/三月廿四.md "wikilink")，禁止[西班牙船前往日本](../Page/西班牙.md "wikilink")。

### 出生

  - 元年
      - [11月29日](../Page/十一月廿九.md "wikilink") -
        [松平光長](../Page/松平光長.md "wikilink")、[松平忠直之子](../Page/松平忠直.md "wikilink")。
      - 不明 -
        [野中兼山](../Page/野中兼山.md "wikilink")，[土佐藩](../Page/土佐藩.md "wikilink")[家老](../Page/家老.md "wikilink")。
  - 3年
      - [3月13日](../Page/三月十三.md "wikilink") -
        [慈胤法親王](../Page/慈胤法親王.md "wikilink")，[法親王](../Page/法親王.md "wikilink")，[後陽成天皇父親](../Page/後陽成天皇.md "wikilink")。
      - [10月28日](../Page/十月廿八.md "wikilink") -
        [土佐光起](../Page/土佐光起.md "wikilink")，[土佐派](../Page/土佐派.md "wikilink")[畫家](../Page/畫家.md "wikilink")。
      - 不明 -
        [板倉重矩](../Page/板倉重矩.md "wikilink")，江戶初期[老中](../Page/老中.md "wikilink")，[京都所司代](../Page/京都所司代.md "wikilink")。
  - 4年
      - [12月9日](../Page/十二月初九.md "wikilink") -
        [山崎闇齋](../Page/山崎闇齋.md "wikilink")，江戶初期儒家學者。
  - 5年
      - [3月10日](../Page/三月初十.md "wikilink") -
        [京極高知](../Page/京極高知.md "wikilink")，江戶初期大名。
      - 不明 - [熊澤蕃山](../Page/熊澤蕃山.md "wikilink") -
        江戶初期[陽明學者](../Page/陽明學.md "wikilink")。
  - 6年
      - [11月1日](../Page/十一月初一.md "wikilink") -
        [八条宮智忠親王](../Page/八条宮智忠親王.md "wikilink")，江戶初期[皇族](../Page/皇族.md "wikilink")、[桂宮第](../Page/桂宮.md "wikilink")2代當主。
  - 7年
      - [6月4日](../Page/六月初四.md "wikilink") -
        [木下順庵](../Page/木下順庵.md "wikilink")，江戶初期儒家學者。
      - [11月18日](../Page/十一月十八.md "wikilink") -
        [松平正信](../Page/松平正信.md "wikilink")，江戶初期大名及[奏者番](../Page/奏者番.md "wikilink")。
      - 不明 -
        [寶樹院](../Page/寶樹院.md "wikilink")，[德川家光側室](../Page/德川家光.md "wikilink")，[德川家綱母親](../Page/德川家綱.md "wikilink")。
      - 不明 -
        [伊達宗勝](../Page/伊達宗勝.md "wikilink")，[一關藩初代藩主](../Page/一關藩.md "wikilink")。
      - 不明 -
        [天草四郎](../Page/天草四郎.md "wikilink")，[島原之亂領袖](../Page/島原之亂.md "wikilink")。
  - 8年
      - [7月1日](../Page/七月初一.md "wikilink") -
        [松平賴重](../Page/松平賴重.md "wikilink")，江戶初期[大名](../Page/大名.md "wikilink")，[德川賴房長男](../Page/德川賴房.md "wikilink")。
      - [8月16日](../Page/八月十六.md "wikilink") -
        [山鹿素行](../Page/山鹿素行.md "wikilink")，江戶初期儒學者。
      - 不明 -
        [順性院](../Page/順性院.md "wikilink")，[德川家光側室](../Page/德川家光.md "wikilink")，[德川綱重生母](../Page/德川綱重.md "wikilink")。
  - 9年
      - [11月19日](../Page/十一月十九.md "wikilink") -
        [明正天皇](../Page/明正天皇.md "wikilink")，第109代[天皇](../Page/天皇.md "wikilink")。
      - 不明 -
        [稻葉正則](../Page/稻葉正則.md "wikilink")，[小田原藩主](../Page/小田原藩.md "wikilink")，[老中](../Page/老中.md "wikilink")。

### 逝世

  - 元年
      - [10月14日](../Page/十月十四.md "wikilink")-
        初代[片倉景綱](../Page/片倉景綱.md "wikilink")[白石城主](../Page/白石城.md "wikilink")，[伊達政宗家臣](../Page/伊達政宗.md "wikilink")
  - 2年
      - [4月17日](../Page/四月十七.md "wikilink") -
        [德川家康](../Page/德川家康.md "wikilink")，江戶幕府初代將軍\[4\]。
      - [6月7日](../Page/六月初七.md "wikilink") -
        [本多正信](../Page/本多正信.md "wikilink")，德川家康家臣。
      - [9月11日](../Page/九月十一.md "wikilink") -
        [坂崎直盛](../Page/坂崎直盛.md "wikilink")，[津和野藩初代藩主](../Page/津和野藩.md "wikilink")。
      - [10月12日](../Page/十月十二.md "wikilink") -
        [松前慶廣](../Page/松前慶廣.md "wikilink")，[松前藩初代藩主](../Page/松前藩.md "wikilink")。
  - 3年
      - [3月6日](../Page/三月初六.md "wikilink") -
        [最上家親](../Page/最上家親.md "wikilink")，江戶時代大名，[最上氏第](../Page/最上氏.md "wikilink")12代當主。
      - [7月16日](../Page/七月十六.md "wikilink")
        [芳春院](../Page/芳春院.md "wikilink")，[前田利家正室](../Page/前田利家.md "wikilink")。
      - [8月26日](../Page/八月廿六.md "wikilink") -
        [後陽成天皇](../Page/後陽成天皇.md "wikilink")、第107代[天皇](../Page/天皇.md "wikilink")\[5\]。
  - 4年
      - [6月3日](../Page/六月初三.md "wikilink") -
        [鍋島直茂](../Page/鍋島直茂.md "wikilink")，戰國及江戶時代大名。
  - 5年
      - [3月18日](../Page/三月十八.md "wikilink") -
        [細川興元](../Page/細川興元.md "wikilink")，江戶時代初期大名。
      - [7月21日](../Page/七月廿一.md "wikilink") -
        [島津義弘](../Page/島津義弘.md "wikilink")，[島津家第](../Page/島津家.md "wikilink")17代當主。
      - [9月12日](../Page/九月十二.md "wikilink") -
        [藤原惺窩](../Page/藤原惺窩.md "wikilink")，戰國至江戶時代的儒家學者。
  - 7年
      - [6月5日](../Page/六月初五.md "wikilink") -
        [生駒正俊](../Page/生駒正俊.md "wikilink")，[高松藩第](../Page/高松藩.md "wikilink")3代藩主。
      - [6月12日](../Page/六月十二.md "wikilink") -
        [茶阿局](../Page/茶阿局.md "wikilink")，德川家康[側室](../Page/側室.md "wikilink")。
      - [12月13日](../Page/十二月十三.md "wikilink") -
        [織田長益](../Page/織田長益.md "wikilink")（織田有楽齋），[織田信長弟](../Page/織田信長.md "wikilink")，大名茶人。
  - 8年
      - [6月19日](../Page/六月十九.md "wikilink") -
        [里見忠義](../Page/里見忠義.md "wikilink")，[館山藩第](../Page/館山藩.md "wikilink")2代藩主。
      - [7月3日](../Page/七月初三.md "wikilink") -
        [珠姬](../Page/德川珠姬.md "wikilink")，[前田利常](../Page/前田利常.md "wikilink")[正室](../Page/正室.md "wikilink")，[德川秀忠次女](../Page/德川秀忠.md "wikilink")。
      - [7月11日](../Page/七月十一.md "wikilink") -
        [支倉常長](../Page/支倉常長.md "wikilink")，[伊達政宗](../Page/伊達政宗.md "wikilink")[家臣](../Page/家臣.md "wikilink")[慶長遣歐使節之一](../Page/慶長遣歐使節.md "wikilink")。
      - [8月12日](../Page/八月十二.md "wikilink") -
        [京極高知](../Page/京極高知.md "wikilink")，戰國大名。

## 西元對照表

| 元和                             | 元年                                   | 2年                                   | 3年                                   | 4年                                   | 5年                                   | 6年                                   | 7年                                   | 8年                                   | 9年                                   | 10年                                  |
| ------------------------------ | ------------------------------------ | ------------------------------------ | ------------------------------------ | ------------------------------------ | ------------------------------------ | ------------------------------------ | ------------------------------------ | ------------------------------------ | ------------------------------------ | ------------------------------------ |
| 西元                             | [1615年](../Page/1615年.md "wikilink") | [1616年](../Page/1616年.md "wikilink") | [1617年](../Page/1617年.md "wikilink") | [1618年](../Page/1618年.md "wikilink") | [1619年](../Page/1619年.md "wikilink") | [1620年](../Page/1620年.md "wikilink") | [1621年](../Page/1621年.md "wikilink") | [1622年](../Page/1622年.md "wikilink") | [1623年](../Page/1623年.md "wikilink") | [1624年](../Page/1624年.md "wikilink") |
| [干支](../Page/干支.md "wikilink") | [乙卯](../Page/乙卯.md "wikilink")       | [丙辰](../Page/丙辰.md "wikilink")       | [丁巳](../Page/丁巳.md "wikilink")       | [戊午](../Page/戊午.md "wikilink")       | [己未](../Page/己未.md "wikilink")       | [庚申](../Page/庚申.md "wikilink")       | [辛酉](../Page/辛酉.md "wikilink")       | [壬戌](../Page/壬戌.md "wikilink")       | [癸亥](../Page/癸亥.md "wikilink")       | [甲子](../Page/甲子.md "wikilink")       |

## 參看

  - 其他时期使用的[元和年号](../Page/元和.md "wikilink")

## 参考資料

<div class="references-small">

<references>

</references>

</div>

[Category:17世纪日本年号](../Category/17世纪日本年号.md "wikilink")
[Category:1610年代日本](../Category/1610年代日本.md "wikilink")
[Category:1620年代日本](../Category/1620年代日本.md "wikilink")

1.  Tittsingh, Isaac. (1834). [*Annales des empereurs du japon,* pp.
    410](http://books.google.com/books?id=18oNAAAAIAAJ&pg=PP9&dq=nipon+o+dai+itsi+ran#PRA1-PA410,M1)-411.
2.  Ponsonby-Fane, Richard. (1956). *Kyoto: the Old Capital of Japan,
    794-1869,* p. 317.
3.  Ponsonby-Fane, p. 317; Titsingh,
    [p. 410.](http://books.google.com/books?id=18oNAAAAIAAJ&pg=PP9&dq=nipon+o+dai+itsi+ran#PRA1-PA410,M1)
4.  Titsingh, p. 410.
5.  Titsingh, p. 410.