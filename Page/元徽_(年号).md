**元徽**（[473年正月](../Page/473年.md "wikilink")—[477年七月](../Page/477年.md "wikilink")）是[南朝宋](../Page/南朝宋.md "wikilink")[后废帝刘昱的](../Page/宋后废帝.md "wikilink")[年號](../Page/年號.md "wikilink")，共4年餘。

## 大事记

## 出生

## 逝世

## 纪年

| 元徽                               | 元年                                 | 二年                                 | 三年                                 | 四年                                 | 五年                                 |
| -------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- |
| [公元](../Page/公元纪年.md "wikilink") | [473年](../Page/473年.md "wikilink") | [474年](../Page/474年.md "wikilink") | [475年](../Page/475年.md "wikilink") | [476年](../Page/476年.md "wikilink") | [477年](../Page/477年.md "wikilink") |
| [干支](../Page/干支纪年.md "wikilink") | [癸丑](../Page/癸丑.md "wikilink")     | [甲寅](../Page/甲寅.md "wikilink")     | [乙卯](../Page/乙卯.md "wikilink")     | [丙辰](../Page/丙辰.md "wikilink")     | [丁巳](../Page/丁巳.md "wikilink")     |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [延興](../Page/延兴_\(北魏孝文帝\).md "wikilink")（[471年八月](../Page/471年.md "wikilink")-[476年六月](../Page/476年.md "wikilink")）：[北魏政权](../Page/北魏.md "wikilink")[北魏孝文帝拓跋宏年号](../Page/北魏孝文帝.md "wikilink")
      - [承明](../Page/承明.md "wikilink")（[476年六月](../Page/476年.md "wikilink")-十二月）：[北魏政权](../Page/北魏.md "wikilink")[北魏孝文帝拓跋宏年号](../Page/北魏孝文帝.md "wikilink")
      - [太和](../Page/太和_\(北魏孝文帝\).md "wikilink")（[477年正月](../Page/477年.md "wikilink")-[499年十二月](../Page/499年.md "wikilink")）：[北魏政权](../Page/北魏.md "wikilink")[北魏孝文帝元宏年号](../Page/北魏孝文帝.md "wikilink")
      - [永康](../Page/永康_\(柔然\).md "wikilink")（[464年](../Page/464年.md "wikilink")-[484年](../Page/484年.md "wikilink")）：[柔然政权受罗部真可汗](../Page/柔然.md "wikilink")[予成年号](../Page/予成.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:南朝宋年号](../Category/南朝宋年号.md "wikilink")
[Category:5世纪](../Category/5世纪.md "wikilink")