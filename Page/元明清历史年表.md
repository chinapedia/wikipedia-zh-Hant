**元明清历史年表**為[元明清三朝的历史年表](../Page/元明清.md "wikilink")。

## [蒙古](../Page/蒙古帝国.md "wikilink")（1206－1271）

  -
    1206　太祖（[成吉思汗](../Page/成吉思汗.md "wikilink")，[铁木真](../Page/铁木真.md "wikilink")，1162－1227）即位
    1215　攻取[金朝首都](../Page/金朝.md "wikilink")[中都](../Page/金中都.md "wikilink")
    1217　[中都改名](../Page/金中都.md "wikilink")[燕京](../Page/燕京.md "wikilink")
    1218　灭[西辽](../Page/西辽.md "wikilink")（1132－1218）
    1219　-{[蒙古第一次西征](../Page/蒙古第一次西征.md "wikilink")}-
    1220　灭[花剌子模](../Page/花剌子模.md "wikilink")（1200－1220）
    1223　[木华黎](../Page/木华黎.md "wikilink")（1170－1223）去世
    1224　[哲别](../Page/哲别.md "wikilink")（?－1224?）去世
    1225　[术赤](../Page/术赤.md "wikilink")（1177－1225）去世
    1225　蒙古第一次西征結束
    1227　[长春真人](../Page/长春真人.md "wikilink")（[丘处机](../Page/丘处机.md "wikilink")，1148－1227）逝
    1227　山东博州发行纸币“会子”
    1227　灭[西夏](../Page/西夏.md "wikilink")（1038－1227）
    1227　[拖雷](../Page/拖雷.md "wikilink")（1192－1232）监国
    1229　太宗（窝阔台汗，[窝阔台](../Page/窝阔台.md "wikilink")，1186－1241）
    1233　封[孔子五十一世孙](../Page/孔子.md "wikilink")[孔元措为](../Page/孔元措.md "wikilink")[衍圣公](../Page/衍圣公.md "wikilink")
    1234　联[南宋灭](../Page/南宋.md "wikilink")[金朝](../Page/金朝.md "wikilink")（1115－1234）
    1235　建都[和林](../Page/和林.md "wikilink")
    1235　[蒙古第二次西征](../Page/蒙古第二次西征.md "wikilink")
    1236　发行纸币“交钞”
    1238　第一次[科举](../Page/科举.md "wikilink")“[戊戌选试](../Page/戊戌选试.md "wikilink")”
    1240　征服俄罗斯、乌克兰、白俄罗斯等地
    1241　征服波兰王国、匈牙利王国
    1242　[察合台](../Page/察合台.md "wikilink")（1178?－1242?）去世
    1242　蒙古第二次西征结束
    1242　乃马真后（[昭慈皇后](../Page/昭慈皇后.md "wikilink")，?－1246）称制
    1244　[耶律楚材](../Page/耶律楚材.md "wikilink")（1190－1244）去世
    1246　定宗（贵由汗，[贵由](../Page/贵由.md "wikilink")，1206－1248）
    1247　[凉州会盟](../Page/凉州.md "wikilink")，[吐蕃归附](../Page/吐蕃.md "wikilink")　
    1248　[速不台](../Page/速不台.md "wikilink")（1176－1248）去世　
    1248　海迷失后（[钦淑皇后](../Page/钦淑皇后.md "wikilink")，?－1252）称制
    1251　宪宗（蒙哥汗，[蒙哥](../Page/蒙哥.md "wikilink")，1208－1259）
    1252　“四帝之母”[唆鲁禾帖尼](../Page/唆鲁禾帖尼.md "wikilink")（1192?－1252）皇太后去世
    1252　[蒙古第三次西征](../Page/蒙古第三次西征.md "wikilink")
    1253　设立交钞提举司，专门负责纸币“交钞”的发行
    1253　在[开平修建王府宫殿](../Page/开平.md "wikilink")
    1253　灭[大理国](../Page/大理国.md "wikilink")（937－1253）
    1255　[拔都](../Page/拔都.md "wikilink")（1207－1255）去世
    1256　灭[木剌夷国](../Page/木剌夷国.md "wikilink")
    1257　[元好问](../Page/元好问.md "wikilink")（1190－1257）去世
    1258　灭[阿拔斯王朝](../Page/阿拔斯王朝.md "wikilink")（750－1258）
    1260　击溃[阿尤布王朝](../Page/阿尤布王朝.md "wikilink")，征服[叙利亚全境](../Page/叙利亚.md "wikilink")　
    1260　中统　世祖（薛禅汗，[忽必烈](../Page/忽必烈.md "wikilink")，1215－1294），即皇帝位　
    1260　发布即位诏书《皇帝登宝位诏》，建年号“中统”
    1260　[阿里不哥起兵](../Page/阿里不哥.md "wikilink")，和忽必烈争位　
    1260　发行纸币[中统元宝交钞](../Page/中统元宝交钞.md "wikilink")　
    1260　蒙古第三次西征结束　
    1260　迁都[开平](../Page/开平.md "wikilink")　
    1263　开平改名[上都](../Page/元上都.md "wikilink")　
    1264＝甲子年
    1264　[阿里不哥兵败](../Page/阿里不哥.md "wikilink")，向忽必烈投降
    1264　[燕京改名](../Page/燕京.md "wikilink")[中都](../Page/元中都（北京）.md "wikilink")
    1264　至元
    1265　[旭烈兀](../Page/旭烈兀.md "wikilink")（1217－1265）去世
    1266　[阿里不哥](../Page/阿里不哥.md "wikilink")（1219?－1266）去世
    1267　迁都[中都](../Page/元中都（北京）.md "wikilink")，修建皇城和宫城

## [元](../Page/元朝.md "wikilink")（1271－1368）

  -
    1271　忽必烈将汉语国号“大蒙古国”改为“[大元](../Page/元朝.md "wikilink")”
    1272　[中都改名](../Page/元中都（北京）.md "wikilink")[大都](../Page/元大都.md "wikilink")
    1273　颁布官修农书《[农桑辑要](../Page/农桑辑要.md "wikilink")》
    1274　[刘秉忠](../Page/刘秉忠.md "wikilink")（1216－1274）去世
    1274　第一次征[日](../Page/日本.md "wikilink")
    1275　[马可波罗](../Page/马可波罗.md "wikilink")（1254－1323）到达中国
    1275　[史天泽](../Page/史天泽.md "wikilink")（1202－1275）去世
    1275　[郝经](../Page/郝经.md "wikilink")（1223－1275）去世
    1276　入[临安](../Page/临安.md "wikilink")，灭[南宋](../Page/南宋.md "wikilink")（1127－1276）
    1279　灭南宋海上流亡政权（1276－1279），统一全国
    1280　[张弘范](../Page/张弘范.md "wikilink")（1238－1280）去世
    1281　第二次征日
    1281　[察必](../Page/察必.md "wikilink")（1227?－1281）皇后去世
    1286　[真金](../Page/真金.md "wikilink")（1243－1286）皇太子去世
    1287　发行纸币[至元通行宝钞](../Page/至元通行宝钞.md "wikilink")
    1291　颁布法律《[至元新格](../Page/至元新格.md "wikilink")》
    1292　[马可波罗](../Page/马可波罗.md "wikilink")（1254－1323）离开中国
    1294　成宗（完泽笃汗，[铁穆耳](../Page/铁穆耳.md "wikilink")，1265－1307）
    1295　[伯颜](../Page/伯顏_（元初）.md "wikilink")（1236－1295）去世
    1295　元贞
    1297　大德
    1300　[关汉卿](../Page/关汉卿.md "wikilink")（1220－1300）去世
    1303　[元朝和](../Page/元朝.md "wikilink")[窝阔台汗国](../Page/窝阔台汗国.md "wikilink")、[察合台汗国约和](../Page/察合台汗国.md "wikilink")，四大汗国一直承认元朝皇帝的宗主地位
    1304　[四大汗国彼此之间约和](../Page/四大汗国.md "wikilink")，[蒙古帝国内战彻底结束](../Page/蒙古帝国.md "wikilink")
    1307　武宗（曲律汗，海山，1281－1311）
    1307　在[王忽察都兴建](../Page/王忽察都.md "wikilink")[中都](../Page/元中都遗址.md "wikilink")
    1308　至大
    1309　[窝阔台汗国](../Page/窝阔台汗国.md "wikilink")（1225－1309）灭亡
    1311　仁宗（普颜笃汗，[爱育黎拔力八达](../Page/爱育黎拔力八达.md "wikilink")，1285－1320）
    1312　皇庆
    1313　下诏恢复科举
    1314　延祐
    1316　[郭守敬](../Page/郭守敬.md "wikilink")（1231－1316）逝，计[算数](../Page/算术.md "wikilink")、[天文学](../Page/天文学.md "wikilink")、[地理](../Page/地理.md "wikilink")
    1320　英宗（格坚汗，[硕德八剌](../Page/硕德八剌.md "wikilink")，1303－1323）
    1321　至治
    1321　[马致远](../Page/马致远.md "wikilink")（1250?－1321?）去世
    1322　《[大元圣政国朝典章](../Page/大元圣政国朝典章.md "wikilink")》（《元典章》）修成
    1323　颁布法律《[大元通制](../Page/大元通制.md "wikilink")》，《至元新格》停止使用
    1323　泰定帝（也孙铁木儿汗，[也孙铁木儿](../Page/也孙铁木儿.md "wikilink")，1293－1328）
    1324＝甲子年
    1324　泰定
    1328　致和　
    1328　天顺　 天顺帝（阿剌吉八汗，[阿剌吉八](../Page/元天顺帝.md "wikilink")，1320－1328？）　
    1328　天历　文宗（札牙笃汗，[图帖睦尔](../Page/图帖睦尔.md "wikilink")，1304－1332），第一次在位
    1329　明宗（忽都笃汗，[和世?](../Page/元明宗.md "wikilink")，1300－1329）
    1329　文宗（札牙笃汗，[图帖睦尔](../Page/图帖睦尔.md "wikilink")，1304－1332），第二次在位
    1330　至顺
    1331　《[经世大典](../Page/经世大典.md "wikilink")》修成
    1332　宁宗（懿璘质班汗，[懿璘质班](../Page/懿璘质班.md "wikilink")，1326－1332）
    1333　惠宗（乌哈噶图汗，[妥懽帖睦尔](../Page/妥懽帖睦尔.md "wikilink")，1320－1370）
    1333　元统
    1335　至元
    1335　废止[科举](../Page/科举.md "wikilink")
    1340　恢复科举
    1341　至正
    1344　[辽史](../Page/辽史.md "wikilink")、[金史修成](../Page/金史.md "wikilink")
    1345　[宋史修成](../Page/宋史.md "wikilink")
    1346　颁布法律《[至正条格](../Page/至正条格.md "wikilink")》，《大元通制》停止使用
    1351　[韩山童](../Page/韩山童.md "wikilink")（?－1351）、[刘福通](../Page/刘福通.md "wikilink")（?－1363）[红巾之乱](../Page/红巾之乱.md "wikilink")（1351－1366）起
    1355　[脱脱](../Page/脱脱.md "wikilink")（1314－1355）去世
    1359　红巾军攻陷[上都](../Page/元上都.md "wikilink")，焚毁宫殿后离去
    1368　明军攻陷[大都](../Page/大都.md "wikilink")，[元惠宗北逃](../Page/元惠宗.md "wikilink")，元朝结束

## [北元](../Page/北元.md "wikilink")（1368－1388）

  -
    1368　惠宗（乌哈噶图汗，[妥懽帖睦尔](../Page/妥懽帖睦尔.md "wikilink")，1320－1370），至正二十八年
    1368　迁都[上都](../Page/元上都.md "wikilink")
    1369　迁都[应昌](../Page/应昌.md "wikilink")
    1370　昭宗（必里克图汗，[爱猷识理达腊](../Page/爱猷识理达腊.md "wikilink")，1339－1378）　
    1370　迁都[和林](../Page/和林.md "wikilink")
    1371　宣光　
    1378　天元帝（乌萨哈尔汗，[脱古思帖木儿](../Page/脱古思帖木儿.md "wikilink")，1342?－1388）
    1379　天元
    1392　明军攻取[云南](../Page/云南.md "wikilink")，[元朝梁王](../Page/梁王_\(元朝\).md "wikilink")[把匝剌瓦尔密自杀](../Page/把匝剌瓦尔密.md "wikilink")
    1387　明军攻取东北，元军统帅[纳哈出降明](../Page/纳哈出.md "wikilink")
    1388　天元帝内乱中被杀，此后北元去国号灭亡（《[明史](../Page/明史.md "wikilink")》说法为1402年），为[鞑靼所取代](../Page/鞑靼_\(蒙古\).md "wikilink")，直到1635年[额哲](../Page/额哲.md "wikilink")（[林丹汗之子](../Page/林丹汗.md "wikilink")）将元朝传国玉玺献给[后金](../Page/后金.md "wikilink")[皇太极](../Page/皇太极.md "wikilink")，鞑靼被后金灭亡

## [明](../Page/明朝.md "wikilink")（1368－1644）

  -
    1368　洪武　太祖（[朱元璋](../Page/朱元璋.md "wikilink")，1328－1398）即位
    1369　[倭寇侵山东沿岸](../Page/倭寇.md "wikilink")，倭寇之忧起
    1370　定科举法
    1370　[施耐庵](../Page/施耐庵.md "wikilink")（1296－1370）逝，著有《[水浒传](../Page/水浒传.md "wikilink")》
    1380　[胡惟庸](../Page/胡惟庸.md "wikilink")（ －1380）入狱，废止宰相
    1373　颁布大明律
    1381　行里甲制
    1384＝甲子年
    1384　再定科举取士制
    1385　画家[王蒙](../Page/王蒙_\(画家\).md "wikilink")（1308－1385）逝
    1385　[徐达](../Page/徐达.md "wikilink")（1332－1385）逝
    1387　《[鱼鳞图册](../Page/鱼鳞图册.md "wikilink")》问世
    1398　惠帝（[朱允炆](../Page/朱允炆.md "wikilink")，1377－1402?）
    1399　建文
    1400　[罗贯中](../Page/罗贯中.md "wikilink")（1330?－1400?）逝，著有《[三国志演义](../Page/三国演义.md "wikilink")》
    1402　成祖（[朱棣](../Page/朱棣.md "wikilink")，1360－1424）
    1402　[方孝孺](../Page/方孝孺.md "wikilink")（ －1402）被杀
    1403　永乐
    1405　[郑和](../Page/郑和.md "wikilink")（1371－1435）七次下西洋（ 1405－1433）始
    1407　《[永乐大典](../Page/永乐大典.md "wikilink")》问世
    1420　北京设东厂
    1421　迁都[北京](../Page/北京.md "wikilink")
    1424　仁宗（朱高炽，1378－1425）
    1425　洪熙
    1425　宣宗（[朱瞻基](../Page/朱瞻基.md "wikilink")，1399－1435）
    1426　宣德
    1435　英宗（[朱祁鎮](../Page/朱祁鎮.md "wikilink")，1427－1464）
    1435　宦官专政开始
    1436　正统
    1444＝甲子年
    1449　[土木堡之变](../Page/土木堡之变.md "wikilink")，英宗（1427－1464）被瓦剌军也先（
    －1454）俘
    1449　景帝（[朱祁鈺](../Page/朱祁鈺.md "wikilink")，1428－1457）
    1449　瓦剌军也先（ －1454）攻北京
    1450　景泰
    1457　天顺　英宗（[朱祁镇](../Page/朱祁镇.md "wikilink")，1427－1464）
    1462　画家[戴进](../Page/戴进.md "wikilink")（1389－1462）逝
    1464　宪宗（[朱见深](../Page/朱见深.md "wikilink")，1447－1487）
    1465　成化
    1487　孝宗（[朱祐樘](../Page/朱祐樘.md "wikilink")，1470－1505）
    1488　弘治
    1502　《[大明会典](../Page/大明会典.md "wikilink")》问世
    1504＝甲子年
    1505　武宗（[朱厚照](../Page/朱厚照.md "wikilink")，1491－1521）
    1506　正德
    1506　[王阳明](../Page/王阳明.md "wikilink")（1472－1529）贬贵州
    1509　画家[沈周](../Page/沈周.md "wikilink")（1427－1509）逝
    1510　宦官[刘瑾](../Page/刘瑾.md "wikilink")（ －1510）逝
    1521　世宗（[朱厚熜](../Page/朱厚熜.md "wikilink")，1507－1566）
    1522　嘉靖
    1550　瓦剌軍围北京
    1552　画家[仇英](../Page/仇英.md "wikilink")（ －1552?）逝
    1559　[杨慎](../Page/杨慎.md "wikilink")（1488－1559）逝
    1559　[文徵明](../Page/文徵明.md "wikilink")（1470－1559）逝
    1562　[严嵩遭罢免](../Page/严嵩.md "wikilink")
    1557　[葡萄牙获](../Page/葡萄牙.md "wikilink")[澳门居住权](../Page/澳门.md "wikilink")
    1563　俞大猷（1504－1580），[戚继光](../Page/戚继光.md "wikilink")（1528－1587）于[福建破倭寇](../Page/福建.md "wikilink")
    1564＝甲子年
    1566　穆宗（[朱载垕](../Page/朱载垕.md "wikilink")，1537－1572）
    1566　[海瑞](../Page/海瑞.md "wikilink")（1514－1587）非难世宗（1507－1566）入狱
    1566　葡萄牙人建设澳门
    1567　隆庆
    1570　缔约承认[蒙古對](../Page/蒙古.md "wikilink")[西藏宗主权](../Page/西藏.md "wikilink")
    1570　李攀龙（1514－1557）逝
    1572　神宗（[朱翊鈞](../Page/朱翊鈞.md "wikilink")，1563－1620）
    1572　[张居正](../Page/张居正.md "wikilink")（1525－1582）为首辅，高拱（1512－1578）遭罢免
    1573　万历
    1578　[李时珍](../Page/李时珍.md "wikilink")（1518－1593）著《[本草纲目](../Page/本草纲目.md "wikilink")》（Great
    Pharmacopoeia）问世
    1580　张居正（1525－1582）丈量田亩
    1582　[吴承恩](../Page/吴承恩.md "wikilink")（1510?－1582?）逝，著有《[西游记](../Page/西游记.md "wikilink")》
    1583　[申时行](../Page/申时行.md "wikilink")（1535－1614）任首辅
    1584　[耿定理](../Page/耿定理.md "wikilink")（1534－1584）逝
    1590　[王世贞](../Page/王世贞.md "wikilink")（1526－1590）逝
    1592　日本丰臣秀吉（1536－1598）出兵[朝鲜](../Page/朝鲜.md "wikilink")
    1593　文人徐渭（1521－1593）逝
    1596　耿定向（1524－1596）逝
    1596　日本第二次出兵朝鲜
    1601　[意大利耶稣教会传教士](../Page/意大利.md "wikilink")[利玛窦](../Page/利玛窦.md "wikilink")（1552－1610）到北京
    1602　[李贄](../Page/李贄.md "wikilink")（李卓吾，1527－1602）狱中自杀
    1604　[顾宪成](../Page/顾宪成.md "wikilink")（1550－1612）重建[东林书院](../Page/东林书院.md "wikilink")
    1606　[袁黃](../Page/袁黃.md "wikilink")（袁了凡，1533－1606）逝，著有《[了凡四训](../Page/了凡四训.md "wikilink")》
    1610　[袁宏道](../Page/袁宏道.md "wikilink")（1568－1610）逝
    1615　[爱新觉罗努尔哈赤](../Page/爱新觉罗.md "wikilink")（1559－1626）定八旗制
    1616　[汤显祖](../Page/汤显祖.md "wikilink")（1550－1616）逝
    1618　[湯若望](../Page/湯若望.md "wikilink") 告別歐洲故土，抵達北京，一生再未返回。　
    1620　光宗（朱常洛，1582－1620）
    1620　泰昌　熹宗（[朱由校](../Page/朱由校.md "wikilink")，1605－1627）
    1620　官宦之争起
    1621　天启
    1623　[魏忠贤](../Page/魏忠贤.md "wikilink")（1568－1627）为东厂长官
    1627　思宗（[朱由检](../Page/朱由检.md "wikilink")，1611－1644）
    1628　崇禎　西北大饥荒，[李自成](../Page/李自成.md "wikilink")（1605－1645）乱起
    1630　[袁崇焕](../Page/袁崇焕.md "wikilink")（ －1630）遭磔刑
    1636　[董其昌](../Page/董其昌.md "wikilink")（1555－1636）逝
    1637　[宋应星](../Page/宋应星.md "wikilink")（1587－1666）著《[天工开物](../Page/天工开物.md "wikilink")》
    1642　[洪承疇降清](../Page/洪承疇.md "wikilink")
    1644　李自成（1605－1645）陷北京，明朝灭亡

## [南明](../Page/南明.md "wikilink")（1644－1662）

  -
    1644　[吴三桂](../Page/吴三桂.md "wikilink")（1612－1678）降清
    1644　弘光　安宗（[朱由崧](../Page/朱由崧.md "wikilink")，
    －1646）福王即位[南京](../Page/南京.md "wikilink")
    1645　隆武　绍宗（[朱聿鍵](../Page/朱聿鍵.md "wikilink")，1602－1646）唐王即位[福州](../Page/福州.md "wikilink")
    1646　紹武　唐王（朱聿𨮁，
    －1647）即位于[广州](../Page/广州.md "wikilink")——注：唐王朱聿𨮁即位时改元绍武，以明年为绍武元年。终其朝，仍用隆武纪年。绍武并未使用。但仍称其为绍武帝。
    1646　永历　桂王（[朱由榔](../Page/朱由榔.md "wikilink")，1623－1662）即位[肇庆](../Page/肇庆.md "wikilink")
    1650　[郑成功](../Page/郑成功.md "wikilink")（1624－1662）以[金门](../Page/金门.md "wikilink")、[厦门为据点抗清](../Page/厦门.md "wikilink")
    1658　郑成功（1624－1662）封延平郡王
    1659　桂王（1623－1662）逃[缅甸](../Page/缅甸.md "wikilink")
    1661　郑成功（1624－1662）攻[台湾](../Page/台湾.md "wikilink")，驱[荷兰人](../Page/荷兰.md "wikilink")
    1662　吴三桂（1612－1678）杀桂王（1623－1662）

## [后金](../Page/后金.md "wikilink")（1616－1635）

  -
    1616　[天命](../Page/天命_\(后金\).md "wikilink")　太祖（[努尔哈赤](../Page/努尔哈赤.md "wikilink")，1559－1626）即位，国号大金（后金），国都[兴京](../Page/兴京.md "wikilink")
    1624＝甲子年
    1626　太宗（[皇太极](../Page/皇太极.md "wikilink")，1592－1643）
    1627　[天聪](../Page/天聪.md "wikilink")

## [清](../Page/清朝.md "wikilink")（1636－1912）

  -
    1636　改国号大清，[皇太极称帝](../Page/皇太极.md "wikilink")，[清朝建立](../Page/清朝.md "wikilink")
    1636　[崇德](../Page/崇德.md "wikilink")
    1643　世祖（[福临](../Page/爱新觉罗福临.md "wikilink")，1638－1661）
    1644　[顺治](../Page/顺治.md "wikilink")　灭明（1368－1644）
    1650　摄政王[多尔衮逝](../Page/多尔衮.md "wikilink")（1612－1650）
    1659　[朱舜水](../Page/朱舜水.md "wikilink")（1600－1682）归化[日本](../Page/日本.md "wikilink")，和[水户学派往来](../Page/水户学派.md "wikilink")
    1661　圣祖（[玄烨](../Page/爱新觉罗玄烨.md "wikilink")，1654－1722）
    1662　[康熙](../Page/康熙.md "wikilink")
    1666　[德国耶稣教会传教士](../Page/德国.md "wikilink")[汤若望](../Page/汤若望.md "wikilink")
    逝
    1666　（Johann Adam Schall von Bell，1591－1666）逝
    1671　[耿继茂](../Page/耿继茂.md "wikilink")（ －1671）逝
    1673　[三藩之乱](../Page/三藩之乱.md "wikilink")（1673－1681）起
    1676　[尚可喜](../Page/尚可喜.md "wikilink")（1604－1676）逝
    1678　[吴三桂](../Page/吴三桂.md "wikilink")（1612－1678）逝
    1681　[郑经](../Page/郑经.md "wikilink")（1642－1681）逝
    1682　[顾炎武](../Page/顾炎武.md "wikilink")（1613－1682）逝，著有《[日知录](../Page/日知录.md "wikilink")》
    1683　[施琅攻](../Page/施琅.md "wikilink")[台湾](../Page/台湾.md "wikilink")，郑克塽（
    1670－1707）投降
    1684＝甲子年
    1692　[王夫之](../Page/王夫之.md "wikilink")（王而农，1619－1692）逝
    1695　[黃宗羲](../Page/黃宗羲.md "wikilink")（1610－1695），著有《[明儒学案](../Page/明儒学案.md "wikilink")》
    1705　[八大山人](../Page/八大山人.md "wikilink")（[朱耷](../Page/朱耷.md "wikilink")，1626－1705）逝
    1711　[王士禎](../Page/王士禎.md "wikilink")（1634－1711）逝
    1715　[蒲松龄](../Page/蒲松龄.md "wikilink")（1640－1715）逝，著有《[聊斋志异](../Page/聊斋志异.md "wikilink")》
    1716　《[康熙字典](../Page/康熙字典.md "wikilink")》问世
    1718　画僧[石涛](../Page/石涛.md "wikilink")（1642－1718?）逝
    1721　台湾[朱一贵](../Page/朱一贵.md "wikilink")（1687－1721）举兵反清失败
    1722　世宗（[胤禛](../Page/胤禛.md "wikilink")，1678－1735）
    1723　驱逐各地[基督教传教士](../Page/基督教.md "wikilink")
    1723　[雍正](../Page/雍正.md "wikilink")
    1725　《[古今图书集成](../Page/古今图书集成.md "wikilink")》问世
    1735　高宗（[弘历](../Page/弘历.md "wikilink")，1711－1799）
    1735　《[明史](../Page/明史.md "wikilink")》问世
    1736　[乾隆](../Page/乾隆.md "wikilink")
    1740　《[大清一统志](../Page/大清一统志.md "wikilink")》问世
    1744＝甲子年
    1754　[吴敬梓](../Page/吴敬梓.md "wikilink")（1701－1754）逝，著有《[儒林外史](../Page/儒林外史.md "wikilink")》
    1763　[曹雪芹](../Page/曹雪芹.md "wikilink")（1724－1763）逝，著有《[红楼梦](../Page/红楼梦.md "wikilink")》
    1763　书画家[金农](../Page/金农.md "wikilink")（1687－1763）逝
    1765　[郑板桥](../Page/郑板桥.md "wikilink")（1693－1765）逝
    1766　《[大清会典](../Page/大清会典.md "wikilink")》问世
    1769　诗人[沈德潜](../Page/沈德潜.md "wikilink")（1673－1769）逝
    1780　《四库全书薈要》问世
    1782　《[四库全书](../Page/四库全书.md "wikilink")》问世
    1786　[台湾](../Page/台湾.md "wikilink")[天地会](../Page/天地会.md "wikilink")[林爽文](../Page/林爽文.md "wikilink")（
    －1788）反乱
    1796　[嘉庆](../Page/嘉庆.md "wikilink")　仁宗（[顒琰](../Page/顒琰.md "wikilink")，1760－1820）
    1796　[白莲教起义](../Page/白莲教.md "wikilink")（1796－1804）起
    1797　[袁枚](../Page/袁枚.md "wikilink")（1716－1796）逝
    1799　[和珅](../Page/和珅.md "wikilink")（ －1799）自杀
    1804＝甲子年
    1804　[钱大昕](../Page/钱大昕.md "wikilink")（1728－1804）逝
    1805　[纪昀](../Page/纪昀.md "wikilink")（[纪晓岚](../Page/纪晓岚.md "wikilink")，1724－1805）逝，负责主编《[四库全书](../Page/四库全书.md "wikilink")》
    1811　禁止基督教传教
    1815　禁[鸦片](../Page/鸦片.md "wikilink")
    1820　宣宗（[旻宁](../Page/旻宁.md "wikilink")，1782－1850）
    1821　[道光](../Page/道光.md "wikilink")
    1839　[林则徐](../Page/林则徐.md "wikilink")（1785－1850）[虎门销烟](../Page/虎门销烟.md "wikilink")
    1839　[第一次鸦片战争](../Page/第一次鸦片战争.md "wikilink")（1840－1842）
    1841　[龔自珍](../Page/龔自珍.md "wikilink")（1792－1841）逝，著有《定盦文集》
    1848　徐松（1781－1848）逝，著有《宋会要》
    1850　文宗（[奕詝](../Page/奕詝.md "wikilink")，1831－1861）
    1851　[咸丰](../Page/咸丰_\(年号\).md "wikilink")
    1851　[洪秀全](../Page/洪秀全.md "wikilink")（1814－1864）成立[太平天国](../Page/太平天国.md "wikilink")
    1851　（Heavenly Kingdom of Great Peace，1850－1864）
    1852　[曾国藩](../Page/曾国藩.md "wikilink")（1811－1872）组[湘军](../Page/湘军.md "wikilink")
    1853 　淮北捻军（1853－1868）起
    1856　[英法联军](../Page/英法联军.md "wikilink")，[第二次鸦片战争](../Page/第二次鸦片战争.md "wikilink")（1856－1860）
    1861　[穆宗](../Page/穆宗.md "wikilink")（[载淳](../Page/载淳.md "wikilink")，1856－1875）
    1861　[慈禧](../Page/慈禧.md "wikilink")（1835－1908）垂廉听政开始
    1861　恭亲王[奕訢](../Page/奕訢.md "wikilink")（1832－1898）设总理各国事务衙门
    1862　[同治](../Page/同治.md "wikilink")
    1863　[石达开](../Page/石达开.md "wikilink")（1831－1863）被杀
    1864＝甲子年
    1865　[李鸿章](../Page/李鸿章.md "wikilink")（1823－1901）立江南制造局
    1866　[左宗棠](../Page/左宗棠.md "wikilink")（1812－1885）设福州造船厂
    1874　[德宗](../Page/德宗.md "wikilink")（[載湉](../Page/載湉.md "wikilink")，1871－1908）
    1875　[光绪](../Page/光绪.md "wikilink")
    1876　[沈葆禎](../Page/沈葆禎.md "wikilink")（1820－1879）聘法国工程師于台南建成億载金城
    1885　[刘铭传](../Page/刘铭传.md "wikilink")（1836－1895）任台湾省巡抚（1885－1891）
    1876　[左宗棠讨伐阿古柏](../Page/左宗棠.md "wikilink")。[李鸿章反对无效](../Page/李鸿章.md "wikilink")。
    1877　[左宗棠占和阗](../Page/左宗棠.md "wikilink")，收复除伊犁地区外的[新疆全部领土](../Page/新疆.md "wikilink")。随即上疏建议[新疆改设行省](../Page/新疆.md "wikilink")。
    1879　中俄伊犁交涉。1880年春[左宗棠在新疆部署兵事](../Page/左宗棠.md "wikilink")。
    1879　[日本正式吞并](../Page/日本.md "wikilink")[琉球](../Page/琉球.md "wikilink")，废除其国王，将[琉球改为](../Page/琉球.md "wikilink")[冲绳县](../Page/冲绳.md "wikilink")。
    1881　中俄《[伊犁条约](../Page/伊犁条约.md "wikilink")》签定。
    1889　光绪（1871－1908）亲政开始
    1894　[中日甲午战争](../Page/中日甲午战争.md "wikilink")（1894－1895）
    1894　[唐景崧](../Page/唐景崧.md "wikilink")（1841－1902）任台湾省巡抚
    1895　[丁汝昌](../Page/丁汝昌.md "wikilink")（丁禹廷，1836－1895）自杀
    1898　[谭嗣同](../Page/谭嗣同.md "wikilink")（1865－1898），[康有为](../Page/康有为.md "wikilink")（1858－1927）[戊戌变法](../Page/戊戌变法.md "wikilink")
    1889　[张之洞](../Page/张之洞.md "wikilink")（1837－1909）任湖广总督，推动[洋务运动](../Page/洋务运动.md "wikilink")
    1899　道士王丹籙于[敦煌石窟第](../Page/敦煌石窟.md "wikilink") 17窟发现大量书画经卷
    1900　[义和团起义](../Page/义和团.md "wikilink")
    1900　作家[谢冰心](../Page/谢冰心.md "wikilink")（1900－1999）生
    1905　诗人[黃遵宪](../Page/黃遵宪.md "wikilink")（1848－1905）逝
    1901　废[八股](../Page/八股.md "wikilink")，用策论
    1902　[鲁迅](../Page/鲁迅.md "wikilink")（周作人，1881－1936）留日
    1904　[日俄战争](../Page/日俄战争.md "wikilink")（1904－1905）
    1906　废科举
    1907　徐錫麟（ －1907），马宗汉（ －1907），秋瑾（1875－1907）被捕处死
    1908　[宣统帝](../Page/宣统.md "wikilink")（[溥仪](../Page/溥仪.md "wikilink")，1906－1967）
    1909　[宣统](../Page/宣统.md "wikilink")
    1911　[黃兴](../Page/黃兴.md "wikilink")（1874－1916）广州起义，[黄花岗事件](../Page/黄花岗起义.md "wikilink")
    1911　辛亥革命爆发，南方各省独立，清朝开始瓦解
    1912　清帝退位，清朝正式灭亡

<hr align="center" noshade size="2" width="100%">

<center>

[中国历史年表](../Page/中国历史年表.md "wikilink") |
[中国历史事件列表](../Page/中国历史事件列表.md "wikilink")
| [中国君主列表](../Page/中国君主列表.md "wikilink") |
[中国朝代](../Page/中国朝代.md "wikilink") |
[元朝](../Page/元朝.md "wikilink") | [明朝](../Page/明朝.md "wikilink")
| [清朝](../Page/清朝.md "wikilink")

</center>

[\*](../Category/元明清.md "wikilink")
[Category:元朝年表](../Category/元朝年表.md "wikilink")
[Category:明朝年表](../Category/明朝年表.md "wikilink")
[Category:清朝年表](../Category/清朝年表.md "wikilink")