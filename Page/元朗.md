[Yuen_Long_Skyline_201006.jpg](https://zh.wikipedia.org/wiki/File:Yuen_Long_Skyline_201006.jpg "fig:Yuen_Long_Skyline_201006.jpg")上俯瞰的[元朗市中心面貌](../Page/元朗市中心.md "wikilink")\]\]
[Shap_Pat_Heung_201408.jpg](https://zh.wikipedia.org/wiki/File:Shap_Pat_Heung_201408.jpg "fig:Shap_Pat_Heung_201408.jpg")一帶的貨櫃場及低密度房屋\]\]
**元朗**（；1960年代以前作）是[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[元朗區的主要區域](../Page/元朗區.md "wikilink")，是一個[沖積平原](../Page/沖積平原.md "wikilink")。遠自[宋朝](../Page/宋朝.md "wikilink")，[骨灰](../Page/骨灰.md "wikilink")即已遷入岑田（今日[錦田](../Page/錦田.md "wikilink")）居住，現時人口約53萬。區內也有些漁塘和田地，但因人口愈來愈多，[城市化更趨嚴重](../Page/城市化.md "wikilink")，以致漁塘和田地面積陸續減少。[元朗市中心以](../Page/元朗市中心.md "wikilink")[唐樓居多](../Page/唐樓.md "wikilink")，近年政府致力發展元朗，現在市中心外圍已有較多新住宅大廈落成。

## 语源学

根據《[新安縣誌](../Page/新安縣誌.md "wikilink")》或其他古地圖，元朗最先是寫作**圓蓢**，然後變成**元塱**，再變成今日的寫法。「圓」是完整、豐滿的意思，「塱」則是開朗的土地或是高起之江岸。元朗是指給左起[凹頭的](../Page/凹頭.md "wikilink")[蠔殼山](../Page/蠔殼山.md "wikilink")，右至[屯門的大頭山的一連串](../Page/屯門.md "wikilink")[山丘](../Page/山丘.md "wikilink")，像一個圓圈地圍繞著的平地，從字面上推測，古時元朗是一塊水源充足的[沼澤低地](../Page/沼澤.md "wikilink")。

## 歷史

元朗自[新石器時代已有人居住](../Page/新石器時代.md "wikilink")，[秦朝時已隨香港地區一起併入](../Page/秦朝.md "wikilink")[中國版圖](../Page/中國.md "wikilink")，在歷代行政分區的變易中，隸屬[廣東省之下的一個](../Page/廣東省.md "wikilink")[縣份](../Page/縣.md "wikilink")。現有的原居民有[北宋時期南移的鄧氏](../Page/北宋.md "wikilink")、[南宋末](../Page/南宋.md "wikilink")[文天祥堂弟文天瑞後裔的文氏](../Page/文天祥.md "wikilink")。其中北宋[鄧符協是](../Page/鄧符協.md "wikilink")[江西](../Page/江西.md "wikilink")[吉水人](../Page/吉水.md "wikilink")，是香港元朗區[香港新界鄧氏的四世祖](../Page/香港新界鄧氏.md "wikilink")。[香港新界文氏始祖](../Page/香港新界文氏.md "wikilink")[文天瑞是文天祥的堂弟](../Page/文天瑞.md "wikilink")。

元朗居民過去主要以務[農為生](../Page/農業.md "wikilink")，亦有[漁業和生產](../Page/漁業.md "wikilink")[鹽與](../Page/鹽.md "wikilink")[珍珠](../Page/珍珠.md "wikilink")。元朗過去盛產[稻米](../Page/稻米.md "wikilink")，當地生產的「元朗[絲苗](../Page/絲苗米.md "wikilink")」曾是香港人最愛的米食，主要供應[上水和](../Page/上水.md "wikilink")[沙頭角](../Page/沙頭角.md "wikilink")，甚至遠銷至[南洋](../Page/南洋.md "wikilink")。[明朝](../Page/明朝.md "wikilink")[嘉靖年間](../Page/嘉靖.md "wikilink")，由於[南頭一帶發生饑民搶米](../Page/南頭.md "wikilink")[暴動](../Page/暴動.md "wikilink")，眾多[鄉紳請求在當地建](../Page/鄉紳.md "wikilink")[縣](../Page/縣.md "wikilink")。因此明朝政府於[萬曆元年從](../Page/萬曆.md "wikilink")[東莞縣劃出](../Page/東莞縣.md "wikilink")56里、7608戶，共33,791人，成立[新安縣](../Page/寶安縣.md "wikilink")，縣治設在[南頭](../Page/南頭.md "wikilink")。自此由[明神宗](../Page/明神宗.md "wikilink")[萬曆元年](../Page/萬曆.md "wikilink")（1573年）起，到[清宣宗](../Page/清宣宗.md "wikilink")[道光](../Page/道光.md "wikilink")21年（1841年）成為[英國殖民地為止](../Page/英國殖民地.md "wikilink")，元朗地區一直屬[廣州府](../Page/廣州府.md "wikilink")[新安縣管轄](../Page/新安縣.md "wikilink")。此前，[清初的](../Page/清朝.md "wikilink")[海禁政策和為對付](../Page/海禁.md "wikilink")[鄭成功而實施的](../Page/鄭成功.md "wikilink")[遷界一度使元朗變得荒涼](../Page/遷界.md "wikilink")。

古時[疍家在元朗和流上通南坑其中稱爲](../Page/疍家.md "wikilink")「大樹」的地方建成一小廟，以祭祀[天后](../Page/媽祖.md "wikilink")；這就是今天[十八鄉](../Page/十八鄉.md "wikilink")[大旗嶺](../Page/大旗嶺.md "wikilink")[大樹下天后廟的基礎](../Page/大樹下天后廟.md "wikilink")。由於該廟居於一棵方圓數百尺、濃蔭蔽日的大樹之下，故得此名。大樹下天后廟曾多次重修：清朝[咸豐](../Page/咸豐.md "wikilink")、[光緒時和](../Page/光緒.md "wikilink")1962年[颱風](../Page/颱風.md "wikilink")[溫黛襲港後各大修一次](../Page/颱風溫黛.md "wikilink")。大樹四週本是一片澤國，西面是[蛋家灣](../Page/蛋家灣.md "wikilink")，東面是[蛋家埔](../Page/蛋家埔.md "wikilink")。後來由於人口和商務增多，[元朗舊墟的前身](../Page/元朗舊墟.md "wikilink")[大橋墩墟于焉成立](../Page/大橋墩墟.md "wikilink")。《[新安縣誌](../Page/新安縣誌.md "wikilink")》中清楚紀錄「大橋墩墟：　圓蓢」。

<span style="font-size:smaller;">關於元朗墟的歷史，請參見[元朗墟](../Page/元朗墟.md "wikilink")</span>

## 地理

元朗是[香港十八區中](../Page/香港十八區.md "wikilink")[元朗區的主要部分](../Page/元朗區.md "wikilink")，大致位於[山貝河沿岸的已建區](../Page/山貝河.md "wikilink")，北至[新田和](../Page/新田_\(香港\).md "wikilink")[米埔](../Page/米埔.md "wikilink")，西接[洪水橋和](../Page/洪水橋.md "wikilink")[屏山](../Page/屏山.md "wikilink")，西北接[天水圍](../Page/天水圍.md "wikilink")，南面有[十八鄉和](../Page/十八鄉.md "wikilink")[大棠](../Page/大棠.md "wikilink")，東至[錦田和](../Page/錦田.md "wikilink")[八鄉](../Page/八鄉.md "wikilink")。

## 對外交通

由於地理上的阻隔，過去由元朗來往[九龍市區](../Page/九龍.md "wikilink")，一是取道狹窄的[青山公路](../Page/青山公路.md "wikilink")，繞經[屯門](../Page/屯門.md "wikilink")、[深井和](../Page/深井.md "wikilink")[荃灣](../Page/荃灣.md "wikilink")；一是取道[荃錦公路](../Page/荃錦公路.md "wikilink")，蜿蜒越過[大帽山](../Page/大帽山.md "wikilink")；就算是1980年代，[屯門公路全綫通車](../Page/屯門公路.md "wikilink")，仍不免要繞經屯門。踏入1990年代初，因屯門公路擠塞嚴重，而[荃灣路](../Page/荃灣路.md "wikilink")、[西九龍走廊](../Page/西九龍走廊.md "wikilink")、[紅磡海底隧道的擠塞亦不遑多讓](../Page/紅磡海底隧道.md "wikilink")；前往九龍、[港島的一條相對暢通的路徑](../Page/港島.md "wikilink")，就是取道[新田公路](../Page/新田公路.md "wikilink")、[粉嶺公路](../Page/粉嶺公路.md "wikilink")、[吐露港公路](../Page/吐露港公路.md "wikilink")、[大老山隧道](../Page/大老山隧道.md "wikilink")、[觀塘繞道](../Page/觀塘繞道.md "wikilink")、[東區海底隧道和](../Page/東區海底隧道.md "wikilink")[東區走廊](../Page/東區走廊.md "wikilink")，全程繞經了半個香港，路途遙遠，行車時間十分長。

直到1998年5月，[三號幹綫正式通車](../Page/香港3號幹綫.md "wikilink")，再配合[汀九橋](../Page/汀九橋.md "wikilink")、[西九龍公路及](../Page/西九龍公路.md "wikilink")[西區海底隧道](../Page/西區海底隧道.md "wikilink")，來往九龍、港島和[香港國際機場的路程大大縮短](../Page/香港國際機場.md "wikilink")，巴士路綫版圖大幅擴張，同樣亦減輕大老山隧道、東隧等交通要道的流量負荷；再加上2003年12月[九廣西鐵](../Page/九廣西鐵.md "wikilink")（今「[西鐵綫](../Page/西鐵綫.md "wikilink")」）正式啟用，2009年該綫更延長至[紅磡站](../Page/紅磡站.md "wikilink")，在十數年間，元朗交通已大大改善，進出九龍市區半個小時已可做到。

此外，元朗鄰近[深圳市](../Page/深圳市.md "wikilink")，区內乘過境巴士半小時可以到達[皇崗口岸](../Page/皇崗口岸.md "wikilink")，十分方便。

## 市中心

[Sau_Fu_Street_201706.jpg](https://zh.wikipedia.org/wiki/File:Sau_Fu_Street_201706.jpg "fig:Sau_Fu_Street_201706.jpg")

現時元朗的區域中心位於[青山公路元朗段](../Page/青山公路.md "wikilink")（慣稱「[元朗大馬路](../Page/元朗大馬路.md "wikilink")」）兩側，通稱**元朗市中心**或**元朗市**，所指的是該處的一些已發展區。那裡是元朗最繁盛的地方，商業活動頻繁，亦是區內的交通樞紐，也是[元朗新市鎮的核心部分](../Page/元朗新市鎮.md "wikilink")。著名街道有[教育路和](../Page/教育路_\(香港\).md "wikilink")[又新街](../Page/又新街.md "wikilink")（又稱元朗食街）。[恆香](../Page/恆香餅家.md "wikilink")[老婆餅是知名的地道食品](../Page/老婆餅.md "wikilink")。

隨著[元朗南的發展](../Page/元朗南.md "wikilink")，元朗的範圍還在擴展中。不過，約定俗成的「[元朗市中心](../Page/元朗市中心.md "wikilink")」，仍是指元朗大馬路、教育路、大棠路、谷亭街和安寧路沿線，有時加上[朗屏邨及西鐵綫](../Page/朗屏邨.md "wikilink")[元朗站一帶](../Page/元朗站_\(西鐵綫\).md "wikilink")。而[元朗工業邨](../Page/元朗工業邨.md "wikilink")、元朗東[雞地](../Page/雞地.md "wikilink")、[元朗公園一帶](../Page/元朗公園.md "wikilink")，以及元朗南的新發展區，雖然算是「元朗新市鎮」的構成部份，但一般不會視為「市中心」。

除了元朗市中心外，另外也有一些次區域中心，例如[天水圍新市鎮](../Page/天水圍新市鎮.md "wikilink")、[洪水橋](../Page/洪水橋.md "wikilink")、[屏山](../Page/屏山.md "wikilink")、[流浮山](../Page/流浮山.md "wikilink")、[廈村](../Page/廈村.md "wikilink")、[大棠](../Page/大棠.md "wikilink")、[元朗工業邨和](../Page/元朗工業邨.md "wikilink")[錦田市等等](../Page/錦田.md "wikilink")。

## 新市鎮發展

早於1970年代，[香港政府把元朗一些地方發展為](../Page/香港政府.md "wikilink")[新市鎮](../Page/新市鎮.md "wikilink")。

元朗新市鎮位於[凹頭以西及](../Page/凹頭.md "wikilink")[屏山以東](../Page/屏山.md "wikilink")，現時人口21萬。

## 註釋

## 參考

## 連結

  - [元朗網站 Yuen Long Special](http://www.yl.hk)

  - [元朗遊記 The Incredible Journey of Yuen
    Long](https://web.archive.org/web/20170630235514/http://www.go2yl.com/)

  - [元朗區議會選區分界圖](http://www.eac.gov.hk/pdf/distco/maps/dc2011m.pdf) -
    選舉管理委員會

  - [元朗區議會](https://web.archive.org/web/20130308073035/http://www.districtcouncils.gov.hk/yl/tc/welcome.html)

  -
{{-}}

[元朗](../Category/元朗.md "wikilink")
[Category:元朗區](../Category/元朗區.md "wikilink")