[LRT_Yuen_Long_Terminus.jpg](https://zh.wikipedia.org/wiki/File:LRT_Yuen_Long_Terminus.jpg "fig:LRT_Yuen_Long_Terminus.jpg")
**元朗站**（）是[香港](../Page/香港.md "wikilink")[輕鐵於東北面的終點站](../Page/香港輕鐵.md "wikilink")，代號600，屬單程車票[第5收費區位於](../Page/輕鐵第5收費區.md "wikilink")[新界](../Page/新界.md "wikilink")[元朗市中心東北的](../Page/元朗市中心.md "wikilink")[新元朗中心地面](../Page/新元朗中心.md "wikilink")，與西鐵綫[元朗站相鄰](../Page/元朗站_\(西鐵綫\).md "wikilink")。

## 車站設計

### 車站樓層

元朗站是一個地面車站，位於[西鐵綫](../Page/西鐵綫.md "wikilink")[元朗站及元朗](../Page/元朗站_\(西鐵綫\).md "wikilink")（東）巴士總站之間，屬單程車票第5收費區。西鐵綫元朗站位於本站的北面，乘客可到該站轉乘西鐵綫往返市區及[屯門](../Page/屯門.md "wikilink")。

車站的上蓋物業是[新元朗中心](../Page/新元朗中心.md "wikilink")，共有1個商場及5棟住宅。車站南面是元朗（東）巴士總站、青山公路元朗段及[YOHO
MALL 形點](../Page/YOHO_MALL_形點.md "wikilink")。

<table>
<tbody>
<tr class="odd">
<td><p>上蓋</p></td>
<td><p>物業</p></td>
<td><p><a href="../Page/新元朗中心.md" title="wikilink">新元朗中心</a></p></td>
</tr>
<tr class="even">
<td><p><strong>地面</strong></p></td>
<td></td>
<td><p><a href="../Page/西鐵綫.md" title="wikilink">西鐵綫</a><a href="../Page/元朗站_(西鐵綫).md" title="wikilink">元朗站</a>、<a href="../Page/元朗站公共運輸交匯處.md" title="wikilink">元朗站公共運輸交匯處</a></p></td>
</tr>
<tr class="odd">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/元朗（東）巴士總站.md" title="wikilink">元朗（東）巴士總站</a>、輕鐵客務中心</p></td>
<td></td>
</tr>
</tbody>
</table>

由於元朗總站  號月台進行維修工程，因此  須以  作臨時月台，當月台維修工程完成後，該線路將會返回原本的月台。

### 車站周邊

  - [形點II](../Page/形點II.md "wikilink")
  - YOHO Town（即[新元朗中心)](../Page/新元朗中心\).md "wikilink")
  - [YOHO MALL 形點](../Page/YOHO_MALL_形點.md "wikilink")
  - [元朗舊墟](../Page/元朗舊墟.md "wikilink")
  - [博愛醫院](../Page/博愛醫院_\(香港\).md "wikilink")
  - [青山公路(元朗段)](../Page/青山公路\(元朗段\).md "wikilink")

## 鄰接車站

## 利用狀況

本站於1988年輕鐵啟用以來都是其東北面的終點站，經過本站的路線可前往[屯門](../Page/屯門.md "wikilink")。可是，由於本站距離[元朗市中心較遠](../Page/元朗市中心.md "wikilink")，使用本站的人並不多，主要是新元朗中心的居民及前往其商場的人流。

但自2003年西鐵啟用後，因為本站是元朗市中心接駁西鐵的主要車站，人流迅速增加。

## 接駁交通

<div class="NavFrame collapsed" style="background-color: #FFFF00; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

接駁交通列表

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - [西鐵綫](../Page/西鐵綫.md "wikilink")[元朗站](../Page/元朗站_\(西鐵綫\).md "wikilink")（[八達通轉乘優惠](../Page/八達通.md "wikilink")）

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  -
    八達通、屯門－南昌全月通、屯門－紅磡全月通及屯門－南昌全日通免費轉乘優惠

<!-- end list -->

  - [小巴](../Page/香港小巴.md "wikilink")

<!-- end list -->

  - [的士](../Page/香港的士.md "wikilink")

<!-- end list -->

  - 市區的士站
  - 新界的士站

</div>

</div>

## 歷史

本站曾先後稱為**元朗站**（1989年2月5日前）及**元朗總站**（**Yuen Long
Terminus**），車站是於輕鐵初建造時同步興建，於1988年9月18日啟用。

1988年9月18日，輕鐵的第1期通車、位於元朗東巴士總站南端的露天元朗站同時啟用。當時這個站只設有3個月台。列車進入總站月台之前，需先圍繞新元朗中心地盤以北及東的一段朗日路（該段路軌跟路面汽車共用），然後才進入月台。

1993年隨著新元朗中心落成，位於新元朗中心樓底的地面車站啟用。原來位於元朗東巴士總站南端的露天車站亦隨之停用並拆卸。舊站現址為元朗（東）巴士總站附近。

2010年6月13日起，[港鐵將此站復名為](../Page/港鐵.md "wikilink")**元朗站**，以符合港鐵車站一貫命名方式。

2018年3月1日，[610綫月台進行月台加闊工程](../Page/香港輕鐵610綫.md "wikilink")，所屬綫路的所有列車全部駛入1號月台，直至2018年7月工程完成。

2018年8月10日，[615綫月台進行月台加闊工程](../Page/香港輕鐵615綫.md "wikilink")，所屬綫路的所有列車全部駛入1號月台，直至2018年12月工程完成。

## 外部連結

  - [港鐵公司－輕鐵街道圖](http://www.mtr.com.hk/chi/lr_bus/stmap_index.html)
  - [港鐵公司－輕鐵時間表](http://www.mtr.com.hk/chi/lr_bus/schedule/schedule_index.html)

[zh-yue:元朗站](../Page/zh-yue:元朗站.md "wikilink")

[Category:元朗](../Category/元朗.md "wikilink")
[Category:元朗市中心](../Category/元朗市中心.md "wikilink")
[Category:1988年启用的铁路车站](../Category/1988年启用的铁路车站.md "wikilink")
[Category:以區命名的港鐵車站](../Category/以區命名的港鐵車站.md "wikilink")
[Category:元朗區鐵路車站](../Category/元朗區鐵路車站.md "wikilink")