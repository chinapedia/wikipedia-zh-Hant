[Genkisushi.svg](https://zh.wikipedia.org/wiki/File:Genkisushi.svg "fig:Genkisushi.svg")\]\]
[Genki_Sushi_in_Shibuya_201506.jpg](https://zh.wikipedia.org/wiki/File:Genki_Sushi_in_Shibuya_201506.jpg "fig:Genki_Sushi_in_Shibuya_201506.jpg")[東京](../Page/東京.md "wikilink")[澀谷分店](../Page/澀谷.md "wikilink")\]\]
[Genki_Sushi_Paradise_Mall_branch.jpg](https://zh.wikipedia.org/wiki/File:Genki_Sushi_Paradise_Mall_branch.jpg "fig:Genki_Sushi_Paradise_Mall_branch.jpg")[杏花新城分店](../Page/杏花新城.md "wikilink")\]\]
[Genki_Sushi_in_Amoy_Plaza_2016.jpg](https://zh.wikipedia.org/wiki/File:Genki_Sushi_in_Amoy_Plaza_2016.jpg "fig:Genki_Sushi_in_Amoy_Plaza_2016.jpg")[淘大商場分店](../Page/淘大商場.md "wikilink")\]\]
[Genki_Sushi_concept_store_in_apm_2016.jpg](https://zh.wikipedia.org/wiki/File:Genki_Sushi_concept_store_in_apm_2016.jpg "fig:Genki_Sushi_concept_store_in_apm_2016.jpg")[apm分店採用高速列車送壽司](../Page/Apm_\(香港\).md "wikilink")，食客用[平板電腦點菜](../Page/平板電腦.md "wikilink")\]\]
[HK_Yuen_Long_Plaza_Genki_Sushi_Co_Limited.JPG](https://zh.wikipedia.org/wiki/File:HK_Yuen_Long_Plaza_Genki_Sushi_Co_Limited.JPG "fig:HK_Yuen_Long_Plaza_Genki_Sushi_Co_Limited.JPG")[元朗廣場分店](../Page/元朗廣場.md "wikilink")\]\]
**元氣寿司**株式会社（，[英語譯名](../Page/英語.md "wikilink")：Genki Sushi
CO.,LTD.）是[日本一家](../Page/日本.md "wikilink")[回轉壽司業者](../Page/回轉壽司.md "wikilink")，為[東京証券交易所第一部的](../Page/東京証券交易所.md "wikilink")[上市公司](../Page/上市公司.md "wikilink")（）。現在除了元氣壽司外也開設了壽司温度（）、千両（）、魚米（）、釜屋本舗（）等分店。

## 歷史

1968年12月，當年23歲的壽司師傅[齊藤文男於日本宇都宮市的江野町開設了轉轉元禄](../Page/齊藤文男.md "wikilink")（）壽司店（現稱為東武店）。使用一種名為「[迴轉](../Page/迴轉壽司.md "wikilink")」（Kaiten）的新發明，成為其中一間率先使用迴轉帶的壽司店。迴轉壽司在日本快速增長，原本昂貴的壽司由於使用現代化的機器，價錢變得大眾化可接受。越來越多的迴轉壽司店開設在[栃木縣](../Page/栃木縣.md "wikilink")[宇都宮市](../Page/宇都宮市.md "wikilink")，而[轉轉元禄則在](../Page/轉轉元禄.md "wikilink")[關東地區成為家傳戶曉的名字](../Page/關東.md "wikilink")。2000年代，[日本元氣壽司部份分店由迴轉帶](../Page/日本.md "wikilink")，首創改為[新幹線道具專列直接運送壽司到點單的客人枱號旁](../Page/新幹線.md "wikilink")\[1\]，其後[台灣](../Page/台灣.md "wikilink")、[新加坡及](../Page/新加坡.md "wikilink")[香港個別分店亦陸續轉用火車送遞壽司](../Page/香港.md "wikilink")\[2\]。客人利用店內[平板電腦點菜後](../Page/平板電腦.md "wikilink")，至壽司經專列送抵，平均僅需約30秒時間\[3\]。專列壽司即點即做，質素較從前迴轉形式新鮮。由於火車速度快，為安全起見，湯及熱食類如[味增湯](../Page/味增湯.md "wikilink")、[烏龍麵](../Page/烏龍麵.md "wikilink")，仍會由店員送上\[4\]。

1979年7月，[齊藤文男用了](../Page/齊藤文男.md "wikilink")1000萬日元成立[元禄商事株式会社](../Page/元禄商事株式会社.md "wikilink")（Genroku-Shoji）。配合1980年代飯糰（）製作機器及壽司製作機器發明，可每小時製作1000個飯糰，取代傳統手握飯糰，加快壽司普及化。

1984年，[齊藤文男開始](../Page/齊藤文男.md "wikilink")[元禄寿司的經營權業務](../Page/元禄寿司.md "wikilink")，並於6年後結束。

1990年3月8日，[齊藤文男成立元氣壽司](../Page/齊藤文男.md "wikilink")。

1991年8月，[東京証券交易所上市](../Page/東京証券交易所.md "wikilink")，分店遍佈[日本](../Page/日本.md "wikilink")，包括[栃木](../Page/栃木.md "wikilink")、[福島](../Page/福島.md "wikilink")、[群馬](../Page/群馬.md "wikilink")、[北海道](../Page/北海道.md "wikilink")、[東京](../Page/東京.md "wikilink")。

1992年5月，於[美國](../Page/美國.md "wikilink")[夏威夷開設第一間全資海外分店](../Page/夏威夷.md "wikilink")。

1993年5月，David Ben游說元氣壽司向海外發展，繼而成立Genki Sushi Singapore Ltd，管理亞洲經營權。

1994年5月，[新加坡元氣壽司開業](../Page/新加坡.md "wikilink")。

1995年3月及10月，[香港](../Page/香港.md "wikilink")、[馬來西亞元氣壽司分店開業](../Page/馬來西亞.md "wikilink")。

1997年5月，[台灣元氣壽司開業](../Page/台灣.md "wikilink")。

1998年6月，與[株式会社グルメ杵屋合併](../Page/株式会社グルメ杵屋.md "wikilink")。

2000年2月，[曼谷元氣壽司開業](../Page/曼谷.md "wikilink")。

2005年8月，Burgan Group Holdings
Co與元氣壽司簽訂合約，管理[中東經營權](../Page/中東.md "wikilink")；10月，成立Genki
Sushi Hong Kong Ltd，獨立於新加坡公司，並由[美心持有經營權](../Page/美心.md "wikilink")。

2007年11月，[科威特元氣壽司開業](../Page/科威特.md "wikilink")。

2008年2月，Pt. Ilham Putra
Wicksana與元氣壽司簽訂合約，管理[印尼經營權](../Page/印尼.md "wikilink")；5月，Genki
Sushi Singapore
Ltd合約到期，結束[新加坡及](../Page/新加坡.md "wikilink")[馬來西亞分店](../Page/馬來西亞.md "wikilink")；6月，Genki
Sushi Hong Kong Ltd取得[中國經營權](../Page/中國.md "wikilink")。

2000年代，[日本元氣壽司首創以](../Page/日本.md "wikilink")「火車」道具專列直接運送壽司到點單客人枱號旁，其後[台灣](../Page/台灣.md "wikilink")、[新加坡及](../Page/新加坡.md "wikilink")[香港部份分店亦陸續轉用專列送遞壽司](../Page/香港.md "wikilink")。\[5\]

2010年6月4日，元氣壽司中國大陸首家分店於深圳羅湖喜薈城開業。

2015年7月，[香港元氣壽司](../Page/香港.md "wikilink")[荃灣廣場分店重新裝修後以全新概念店示人](../Page/荃灣廣場.md "wikilink")，由以往的迴轉壽司改為安裝上中下共三層「火車軌」送壽司，成為香港首間使用火車軌的元氣壽司,並重新命名為元氣高速線。\[6\]其後[荃灣](../Page/荃灣.md "wikilink")[綠楊坊](../Page/綠楊坊.md "wikilink")、觀塘[Apm](../Page/Apm_\(香港\).md "wikilink")、[太古康山](../Page/太古.md "wikilink")、[大埔超級城](../Page/大埔超級城.md "wikilink")、[黃大仙中心南館](../Page/黃大仙中心.md "wikilink")、[東港城等分店亦採用此模式](../Page/東港城.md "wikilink")。而大部分新開分店如[屯門市廣場店](../Page/屯門市廣場.md "wikilink")、元朗[形點II](../Page/形點II.md "wikilink")、[元朗廣場店都以元氣高速線營運](../Page/元朗廣場.md "wikilink")。

## 被驗出刺身有寄生蟲

2019年4月，[消費者委員會從市面上抽查](../Page/消費者委員會.md "wikilink")50款三文魚及吞拿魚刺身，發現「元気寿司」太古[康怡廣場分店的吞拿魚刺身發現線蟲及蟲卵](../Page/康怡廣場.md "wikilink")。元気寿司稱不認同測試結果，指食材已通過衛生及食物安全測試，符合本地及國際條例，但未有交代貨源。\[7\]

## 資料來源

## 外部連結

  - [元気寿司（株）](http://www.genkisushi.co.jp/)
  - [元気寿司(香港)](http://www.genkisushi.com.hk/)
  - [（株）グルメ杵屋](http://www.gourmet-kineya.co.jp/)
  - [平田寿司](https://web.archive.org/web/20130606084051/http://hiratasushi.com.tw/)
    (原台灣元氣寿司)

[Category:日本餐饮公司](../Category/日本餐饮公司.md "wikilink")
[Category:日本餐厅](../Category/日本餐厅.md "wikilink")
[Category:美心食品](../Category/美心食品.md "wikilink")
[Category:壽司店](../Category/壽司店.md "wikilink")
[Category:1968年成立的公司](../Category/1968年成立的公司.md "wikilink")
[Category:香港連鎖餐廳](../Category/香港連鎖餐廳.md "wikilink")

1.

2.
3.
4.
5.
6.
7.