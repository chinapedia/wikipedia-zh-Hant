**元江哈尼族彝族傣族自治县**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[云南省](../Page/云南省.md "wikilink")[玉溪市下属的一个](../Page/玉溪市.md "wikilink")[自治縣](../Page/自治縣.md "wikilink")。

## 行政区划

下辖：\[1\] 。

## 参考资料

[元江哈尼族彝族傣族自治县](../Category/元江哈尼族彝族傣族自治县.md "wikilink")
[县/自治县](../Category/玉溪区县.md "wikilink")
[玉溪](../Category/云南省民族自治县.md "wikilink")
[云](../Category/中国哈尼族自治县.md "wikilink")
[云](../Category/中国彝族自治县.md "wikilink")
[云](../Category/中国傣族自治县.md "wikilink")

1.