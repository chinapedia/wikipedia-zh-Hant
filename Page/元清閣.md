[Yuenchingkwok.JPG](https://zh.wikipedia.org/wiki/File:Yuenchingkwok.JPG "fig:Yuenchingkwok.JPG")
[Yuenchingkwokmaingate.JPG](https://zh.wikipedia.org/wiki/File:Yuenchingkwokmaingate.JPG "fig:Yuenchingkwokmaingate.JPG")
[Yuenchingkwokpound.JPG](https://zh.wikipedia.org/wiki/File:Yuenchingkwokpound.JPG "fig:Yuenchingkwokpound.JPG")
[Wong_Tai_Sin_Altar_at_Yuen_Ching_Kwok_Ching_Cheung_Road_Hong_Kong.JPG](https://zh.wikipedia.org/wiki/File:Wong_Tai_Sin_Altar_at_Yuen_Ching_Kwok_Ching_Cheung_Road_Hong_Kong.JPG "fig:Wong_Tai_Sin_Altar_at_Yuen_Ching_Kwok_Ching_Cheung_Road_Hong_Kong.JPG")
[Wong_Cheung_Altar_at_Yuen_Ching_Kwok_Ching_Cheung_Road_Hong_Kong.JPG](https://zh.wikipedia.org/wiki/File:Wong_Cheung_Altar_at_Yuen_Ching_Kwok_Ching_Cheung_Road_Hong_Kong.JPG "fig:Wong_Cheung_Altar_at_Yuen_Ching_Kwok_Ching_Cheung_Road_Hong_Kong.JPG")
[Cheung_Yu_Altar_at_Yuen_Ching_Kwok_Ching_Cheung_Road_Hong_Kong.JPG](https://zh.wikipedia.org/wiki/File:Cheung_Yu_Altar_at_Yuen_Ching_Kwok_Ching_Cheung_Road_Hong_Kong.JPG "fig:Cheung_Yu_Altar_at_Yuen_Ching_Kwok_Ching_Cheung_Road_Hong_Kong.JPG")

**元清閣**（[英文](../Page/英文.md "wikilink")：Yuen Ching
Kok）是[香港一所供奉](../Page/香港.md "wikilink")[黃大仙的](../Page/黃大仙.md "wikilink")[道教](../Page/道教.md "wikilink")[廟宇](../Page/廟宇.md "wikilink")，位於[九龍](../Page/九龍.md "wikilink")[青山道的新圍村](../Page/青山道.md "wikilink")，地址為九龍[蘇屋一段的](../Page/蘇屋.md "wikilink")[呈祥道](../Page/呈祥道.md "wikilink")3738號地段，佔地面積約2公頃，分成高低兩組建築群。南鄰供奉[觀世音菩薩及](../Page/觀世音菩薩.md "wikilink")[孫悟空的](../Page/孫悟空.md "wikilink")[紫陽洞](../Page/紫陽洞.md "wikilink")。

## 歷史

在[香港日治時期](../Page/香港日治時期.md "wikilink")，居住在九龍西部的市民前往[嗇色園參神時屢受到日軍凌辱](../Page/嗇色園.md "wikilink")。那時，[潮州商人周亮星先生得知居民苦況](../Page/潮州.md "wikilink")，便專程在1942年4月15日
（星期二）到嗇色園禮請黃大仙傳聖[土瓜灣](../Page/土瓜灣.md "wikilink")[北帝街](../Page/北帝街.md "wikilink")23號二樓，道壇初名為『駐憇亭』。法壇及後於1942年8月22日（星期六）遷往[九龍](../Page/九龍.md "wikilink")[聯合道](../Page/聯合道.md "wikilink")38號三樓，1942年9月25日（星期五）更名「黃大仙元清閣」，並定[農曆十月初十日為成立日](../Page/農曆.md "wikilink")。爾後，黃大仙屢降乩示，指示當屆理事朝青山道覓地置閣，後蒙潮商陳創穆先生捐地，遂在九龍[青山道的新圍村現址建設了香港另一座供奉黃大仙的](../Page/青山道.md "wikilink")[道觀](../Page/道觀.md "wikilink")。現時的[建築物在](../Page/建築物.md "wikilink")1955年春季落成，1955年4月24日（農曆閏三月初三日，星期日)舉行開光大典。

## 近況

在2018年10月舉行會員大會，並於10月14日董事會經投票選出新任主席何炳堃，副主席周修忠、何鑑周、周全安、彭洪輝。

在2005年9月，該道觀曾試圖進行籌募[經費計劃](../Page/經費.md "wikilink")，重建成一幢現代化的三層道觀，附設[升降機](../Page/升降機.md "wikilink")。

  - 頂層：供奉黃大仙像、「[王章](../Page/王章.md "wikilink")」像、「[張禹](../Page/張禹.md "wikilink")」像。
  - 中層：供善信供奉祖先靈位；
  - 底層：素菜食堂、廚房及[盥洗室](../Page/盥洗室.md "wikilink")；

<!-- end list -->

  - 設有24個位置的無蓋[停車場](../Page/停車場.md "wikilink")。
  - 然而直至2018年仍然未能籌得足夠經費展開該重建計劃。

## 現時建築

  - 大門
      - 橫匾：「黃大仙祠」
      - 對聯：「金生麗水乾卜留聖跡；華光普照坤造別洞天。」
  - 主殿供奉道教的黃大仙像、「王章」像、「張禹」像；
  - 戶外有蓋食堂：在農曆初一日、十五日及農曆8月23日的「黃大仙寶誕」，設有免費[素食招待前來觀光的人士](../Page/素食.md "wikilink")。
  - 龜池：像[新加坡的](../Page/新加坡.md "wikilink")[龜嶼之](../Page/龜嶼.md "wikilink")「大伯公廟」般的池塘，中央亦有一隻石龜，牠的背上有一條[眼鏡蛇](../Page/眼鏡蛇.md "wikilink")。石碑：善因褔果。
  - 辦事處、義診部、[扶乩房](../Page/扶乩.md "wikilink")、善信休憩處、盥洗室。

## 服務

  - 它是目前中唯一提供扶乩服務給民眾的「黃大仙道觀」，客人只需把需要詢問的事情以紅色信箋寫好，在主殿的檀前燒化給黃大仙的聖像，坐在側堂的乩童會受感應寫出四句[七言律詩回答](../Page/七言律詩.md "wikilink")。然而並不是所有信件都必定回覆。

<!-- end list -->

  - 中醫義診暫停

## 交通

  - 乘搭前往[蘇屋邨的](../Page/蘇屋邨.md "wikilink")[巴士](../Page/巴士.md "wikilink")，然後沿[明愛醫院方向前往](../Page/明愛醫院.md "wikilink")，再穿過行人天橋便是。此外亦可乘搭[九巴](../Page/九龍巴士.md "wikilink")[72及](../Page/九龍巴士72線.md "wikilink")[81在](../Page/九龍巴士81線.md "wikilink")[爾登華庭站下車](../Page/爾登華庭.md "wikilink")，然後沿[大埔道步行前往](../Page/大埔道.md "wikilink")。
  - 在農曆初一、十五及農曆8月23日的「黃大仙寶誕」，上午10時30分及11時正，在[美孚新邨](../Page/美孚新邨.md "wikilink")[百老匯街設有兩班免費的客車接載](../Page/百老匯街.md "wikilink")[觀光客前往參觀](../Page/觀光客.md "wikilink")。回程時間在下午1時45分及2時30分。

## 參考書籍

  - 作者：吳麗珍，攝影：林建雄；《香港黃大仙信仰》，三聯書店（香港）有限公司，1997年7月1日版。ISBN 962-04-1389-X
  - 作者：梁鳳縈；《香港靈驗廟宇》，嘉出版有限公司，2009年9月1日版。ISBN 978-962-8955-657

## 參考

<div class="references-small">

<references />

</div>

## 參見

  - 赤松[黃大仙祠](../Page/黃大仙祠.md "wikilink")：[香港的著名](../Page/香港.md "wikilink")[黃大仙道觀](../Page/黃大仙.md "wikilink")
  - [紫陽洞](../Page/紫陽洞.md "wikilink")：與它南鄰供奉[觀世音菩薩及](../Page/觀世音菩薩.md "wikilink")[孫悟空的](../Page/孫悟空.md "wikilink")[道觀](../Page/道觀.md "wikilink")
  - [港九福德念佛社](../Page/港九福德念佛社.md "wikilink")：與它西鄰的佛寺
  - [萬佛寺](../Page/萬佛寺.md "wikilink") - 香港的著名佛寺
  - [2009年6月拍攝黃大仙元清閣的影像](http://www.youtube.com/watch?v=bshqfr8cw-o)。

## 外部連結

  - [黃大仙元清閣的官方網站](http://www.wongtaisin-yck.com/)（中文）。

[Category:蘇屋](../Category/蘇屋.md "wikilink")
[Category:香港道觀](../Category/香港道觀.md "wikilink")