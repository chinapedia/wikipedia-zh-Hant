**元祿**是[日本的](../Page/日本.md "wikilink")[年號之一](../Page/年號.md "wikilink")。在[貞享之後](../Page/貞享.md "wikilink")、[寶永之前](../Page/寶永.md "wikilink")。指1688年到1704年的期間。這個時代的[天皇是](../Page/天皇.md "wikilink")[東山天皇](../Page/東山天皇.md "wikilink")\[1\]。[江戶幕府的將軍是](../Page/江戶幕府.md "wikilink")[德川綱吉](../Page/德川綱吉.md "wikilink")。

## 改元

  - [貞享](../Page/貞享.md "wikilink")5年[9月30日](../Page/九月三十.md "wikilink")（公元1688年10月23日）[東山天皇新皇登基改元](../Page/東山天皇.md "wikilink")
  - **元祿**17年[3月13日](../Page/三月十三.md "wikilink")（公元1704年4月16日） 改元為寶永

## 出典

出自《[文選](../Page/文選.md "wikilink")》的「建立**元**勳、以歷顯**祿**、福之上也」。

## 大事紀

  - 元年
      - 1月：[井原西鶴刊行](../Page/井原西鶴.md "wikilink")《[日本永代藏](../Page/日本永代藏.md "wikilink")》。
      - 11月：[柳澤吉保就任](../Page/柳澤吉保.md "wikilink")[側用人](../Page/側用人.md "wikilink")。
      - 大坂[堂島的新地完成](../Page/堂島.md "wikilink")。
  - 2年
      - 4月：[長崎的](../Page/長崎.md "wikilink")[唐人屋敷建築完成](../Page/唐人屋敷.md "wikilink")。
      - 8月：[松尾芭蕉在大垣終結奧之細道的旅程](../Page/松尾芭蕉.md "wikilink")。
      - 11月：[澀川春海建造展望台](../Page/澀川春海.md "wikilink")。
  - 3年
      - 8月：醫師坎貝爾來到日本，進駐荷蘭商館。
      - 10月：發佈棄嬰禁令。
  - 5年
      - 禁止於[江戶建立寺院](../Page/江戶.md "wikilink")。
      - [善光寺傳聞有一座不存在的秘佛](../Page/善光寺.md "wikilink")，為了查探消息幕府派遣使者調查。
  - 6年
      - 12月：[新井白石成為](../Page/新井白石.md "wikilink")[甲府藩主德川綱豐](../Page/甲府藩.md "wikilink")（[德川家宣](../Page/德川家宣.md "wikilink")）的侍講。
  - 8年
      - 2月：對關東的[天領](../Page/天領.md "wikilink")（德川直轄領地）進行檢地。
      - 8月：開始[元祿金](../Page/元祿小判.md "wikilink")[銀的鑄造](../Page/元祿丁銀.md "wikilink")。
      - 11月：將軍下令將江戶的野狗都收容到設置於中野的狗屋。
  - 9年
      - 幕府禁止渡海到竹島（現在的[鬱陵島](../Page/鬱陵島.md "wikilink")）。（[竹島一件](../Page/竹島一件.md "wikilink")）
  - 13年
      - 8月：設置日光奉行。
      - 11月：訂定金錢銀錢的交換比率。
  - 15年
      - 12月：發生[元祿赤穗事件](../Page/元祿赤穗事件.md "wikilink")（又稱[赤穗浪士報仇](../Page/赤穗浪士報仇.md "wikilink")），事後赤穗藩有不少藩士被處死。
  - 16年
      - 3月：[大石良雄等人被下令切腹](../Page/大石良雄.md "wikilink")。
      - 5月：[近松門左衛門的](../Page/近松門左衛門.md "wikilink")《[曾根崎心中](../Page/曾根崎心中.md "wikilink")》一劇首演。
      - 11月23日：發生[元祿大地震](../Page/元祿大地震.md "wikilink")。

## 出生

  - 2年
      - [9月17日](../Page/九月十七.md "wikilink")（10月29日）-[德川吉通](../Page/德川吉通.md "wikilink")，[尾張藩第四代藩主](../Page/尾張藩.md "wikilink")。
  - 3年
      - [1月3日](../Page/正月初三.md "wikilink")（2月11日）-[正木俊光](../Page/正木俊光.md "wikilink")，兵法家兼劍豪。
  - 5年
      - [2月8日](../Page/二月初八.md "wikilink")（3月25日）-[德川繼友](../Page/德川繼友.md "wikilink")，[尾張藩第六代藩主](../Page/尾張藩.md "wikilink")。
      - [12月16日](../Page/十二月十六.md "wikilink")（1693年1月12日）-[一條兼香](../Page/一條兼香.md "wikilink")，朝廷官員，[關白](../Page/關白.md "wikilink")
  - 6年
      - [2月4日](../Page/二月初四.md "wikilink")（3月10日）-[藤堂高敏](../Page/藤堂高敏.md "wikilink")，[津藩藩主](../Page/津藩.md "wikilink")。
  - 8年
      - 不明-[並木宗輔](../Page/並木宗輔.md "wikilink")，歌舞伎及人形淨琉璃作家。
  - 9年
      - [10月26日](../Page/十月廿六.md "wikilink")（11月20日）-[德川宗春](../Page/德川宗春.md "wikilink")，尾張藩第七代藩主。
  - 10年
      - [3月4日](../Page/三月初四.md "wikilink")（4月24日）-[賀茂真淵](../Page/賀茂真淵.md "wikilink")，[國學者](../Page/日本國學.md "wikilink")、歌人。
  - 13年
      - [6月6日](../Page/六月初六.md "wikilink")（7月21日）-[阿部正福](../Page/阿部正福.md "wikilink")，[福山藩藩主](../Page/福山藩.md "wikilink")。
  - 14年
      - [8月26日](../Page/八月廿六.md "wikilink")（9月28日）-[藤堂高陳](../Page/藤堂高陳.md "wikilink")，[久居藩主](../Page/久居藩.md "wikilink")。
      - [12月7日](../Page/十二月初七.md "wikilink")（1702年1月14日）-慶仁親王（[中御門天皇](../Page/中御門天皇.md "wikilink")），第114代天皇。
  - 15年
      - 不明-[尾崎康工](../Page/尾崎康工.md "wikilink")，俳人。
  - 16年
      - 不明-[安藤昌益](../Page/安藤昌益.md "wikilink")，江戶時代醫師。

## 逝世

  - 2年
      - [7月28日](../Page/七月廿八.md "wikilink")（9月11日）-[松平信平](../Page/松平信平.md "wikilink")，[鷹司松平家初代當主](../Page/鷹司松平家.md "wikilink")。
      - [4月17日](../Page/四月十七.md "wikilink")（6月4日）-[毛利綱廣](../Page/毛利綱廣.md "wikilink")，[長州藩第三任藩主](../Page/長州藩.md "wikilink")。
  - 4年
      - [8月17日](../Page/八月十七.md "wikilink")（9月9日）-[熊澤蕃山](../Page/熊澤蕃山.md "wikilink")，[陽明學者](../Page/陽明學.md "wikilink")。
      - [9月25日](../Page/九月廿五.md "wikilink")（11月14日）-[土佐光起](../Page/土佐光起.md "wikilink")，土佐派畫師。
  - 6年
      - [7月7日](../Page/七月初七.md "wikilink")（8月8日）-[池田光仲](../Page/池田光仲.md "wikilink")，[鳥取藩初代藩主](../Page/鳥取藩.md "wikilink")。
      - [8月10日](../Page/八月初十.md "wikilink")（9月9日）-[井原西鶴](../Page/井原西鶴.md "wikilink")，江戶時代作出、俳人。
      - [9月3日](../Page/九月初三.md "wikilink")（10月2日）-[盤珪永琢](../Page/盤珪永琢.md "wikilink")，臨濟宗佛僧。
  - 7年
      - [6月4日](../Page/六月初四.md "wikilink")（7月25日）-[菱川師宣](../Page/菱川師宣.md "wikilink")，江戶時代繪師，[浮世繪創始人](../Page/浮世繪.md "wikilink")。
      - [10月12日](../Page/十月十二.md "wikilink")（11月28日）-[松尾芭蕉](../Page/松尾芭蕉.md "wikilink")，俳句師被稱為俳聖。
      - [11月29日](../Page/十一月廿九.md "wikilink")（1695年1月14日）-[島津光久](../Page/島津光久.md "wikilink")，[薩摩藩第二代藩主](../Page/薩摩藩.md "wikilink")。
  - 8年
      - [7月15日](../Page/七月十五.md "wikilink")（8月24日）-[圓空](../Page/圓空.md "wikilink")，[天台宗佛僧](../Page/天台宗.md "wikilink")。
  - 9年
      - [11月10日](../Page/十一月初十.md "wikilink")（12月4日）-[明正天皇](../Page/明正天皇.md "wikilink")，第109任天皇。
  - 13年
      - [10月16日](../Page/十月十六.md "wikilink")（11月26日）-[德川光友](../Page/德川光友.md "wikilink")，[尾張藩第](../Page/尾張藩.md "wikilink")2代藩主。
      - [12月6日](../Page/十二月初六.md "wikilink")（1月14日）-[徳川光圀](../Page/徳川光圀.md "wikilink")，[水戶藩第](../Page/水戶藩.md "wikilink")2代藩主。
  - 14年
      - [3月14日](../Page/三月十四.md "wikilink")（4月21日）-[淺野長矩](../Page/淺野長矩.md "wikilink")，[赤穗藩第](../Page/赤穗藩.md "wikilink")3代藩主。
      - [12月15日](../Page/十二月十五.md "wikilink")（1703年1月31日）-[吉良義央](../Page/吉良義央.md "wikilink")，幕府旗本。
  - 15年
      - [2月4日](../Page/二月初四.md "wikilink")（3月20日）-[大石良雄](../Page/大石良雄.md "wikilink")，[赤穗藩家老](../Page/赤穗藩.md "wikilink")。
      - [3月26日](../Page/三月廿六.md "wikilink")（4月22日）-[本因坊道策](../Page/本因坊道策.md "wikilink")，江戶時代圍棋士。

## 西元對照表

| 元祿                             | 元年                             | 2年                             | 3年                             | 4年                             | 5年                             | 6年                             | 7年                             | 8年                             | 9年                             | 10年                            |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| 西元                             | 1688年                          | 1689年                          | 1690年                          | 1691年                          | 1692年                          | 1693年                          | 1694年                          | 1695年                          | 1696年                          | 1697年                          |
| [干支](../Page/干支.md "wikilink") | [戊辰](../Page/戊辰.md "wikilink") | [己巳](../Page/己巳.md "wikilink") | [庚午](../Page/庚午.md "wikilink") | [辛未](../Page/辛未.md "wikilink") | [壬申](../Page/壬申.md "wikilink") | [癸酉](../Page/癸酉.md "wikilink") | [甲戌](../Page/甲戌.md "wikilink") | [乙亥](../Page/乙亥.md "wikilink") | [丙子](../Page/丙子.md "wikilink") | [丁丑](../Page/丁丑.md "wikilink") |
| 元祿                             | 11年                            | 12年                            | 13年                            | 14年                            | 15年                            | 16年                            | 17年                            |                                |                                |                                |
| 西元                             | 1698年                          | 1699年                          | 1700年                          | 1701年                          | 1702年                          | 1703年                          | 1704年                          |                                |                                |                                |
| 干支                             | [戊寅](../Page/戊寅.md "wikilink") | [己卯](../Page/己卯.md "wikilink") | [庚辰](../Page/庚辰.md "wikilink") | [辛巳](../Page/辛巳.md "wikilink") | [壬午](../Page/壬午.md "wikilink") | [癸未](../Page/癸未.md "wikilink") | [甲申](../Page/甲申.md "wikilink") |                                |                                |                                |

## 參見

  - [元祿文化](../Page/元祿文化.md "wikilink")
  - [元祿小判](../Page/元祿小判.md "wikilink")
  - [元祿丁銀](../Page/元祿丁銀.md "wikilink")

## 參考文獻

<div class="references-small">

<references />

</div>

[Genroku](../Category/日本年號.md "wikilink")

1.  Titsingh, Isaac. (1834). *Annales des empereurs du japon,* p. 415.