**元華**（**Yuen
Wah**，），原名**容志**，曾用藝名有**容繼志**、**袁華**，香港[甘草演員](../Page/甘草演員.md "wikilink")，[七小福之一](../Page/七小福_\(演員\).md "wikilink")，師承-{[于占元](../Page/于占元.md "wikilink")}-，為其二弟子。

## 生平

小時候隨與師傅[于占元學習](../Page/于占元.md "wikilink")[功夫](../Page/功夫.md "wikilink")，與[元秋](../Page/元秋.md "wikilink")、[成龍以及](../Page/成龍.md "wikilink")[洪金寶等同為](../Page/洪金寶.md "wikilink")「七小福」成員。

第一次參與演出的電影是《[精武門](../Page/精武門_\(電影\).md "wikilink")》，也是擔任[李小龍](../Page/李小龍.md "wikilink")[翻跟斗的指定](../Page/翻跟斗.md "wikilink")[替身](../Page/替身.md "wikilink")。元華學過[詠春拳](../Page/詠春拳.md "wikilink")、[太極拳](../Page/太極拳.md "wikilink")，後來加入[邵氏電影公司](../Page/邵氏電影公司.md "wikilink")，拍了很多喜劇。

2005年憑電影《[功夫](../Page/功夫_\(電影\).md "wikilink")》奪得[香港電影金像獎及](../Page/香港電影金像獎.md "wikilink")[香港電影金紫荊獎最佳男配角](../Page/香港電影金紫荊獎.md "wikilink")。

2012年，元華離開[無綫電視](../Page/電視廣播有限公司.md "wikilink")，轉投[香港電視網絡](../Page/香港電視網絡.md "wikilink")（HKTV）。離巢原因為元華與無綫電視合約到期，因無綫方面沒有派相關人員洽談續約問題，以致元華離開服務了16年的無綫電視。

## 演出作品

### 電影

|                                                    |                                                                                      |                                     |           |
| -------------------------------------------------- | ------------------------------------------------------------------------------------ | ----------------------------------- | --------- |
| 年份                                                 | 片名                                                                                   | 英文                                  | 角色        |
| 1972年                                              | [精武門](../Page/精武門_\(電影\).md "wikilink")                                              | Fist of Fury/The Chinese Connection |           |
| 1973年                                              | [龍爭虎鬥](../Page/龍爭虎鬥.md "wikilink")                                                   | Enter the Dragon                    |           |
| 1977年                                              | [三少爺的劍](../Page/三少爺的劍_（電影）.md "wikilink")                                            | Death Duel                          |           |
| [多情劍客無情劍](../Page/多情劍客無情劍_（1977年電影）.md "wikilink") | The Sentimental Swordsman                                                            |                                     |           |
| [楚留香](../Page/楚留香_（1977年電影）.md "wikilink")         | Clans of Intrigue                                                                    |                                     |           |
| 1978年                                              | [楚留香之蝙蝠傳奇](../Page/楚留香之蝙蝠傳奇_（電影）.md "wikilink")                                      | Legend of the Bat                   |           |
| [陸小鳳傳奇之綉花大盜](../Page/陸小鳳傳奇之綉花大盜.md "wikilink")     | Clan of Amazons                                                                      | 黑無常                                 |           |
| [倚天屠龍記](../Page/倚天屠龍記_（1978年電影）.md "wikilink")     | Heaven Sword and Dragon Sabre                                                        | 冷謙                                  |           |
| [倚天屠龍記大結局](../Page/倚天屠龍記大結局.md "wikilink")         | Heaven Sword and Dragon Sabre 2                                                      | 冷謙                                  |           |
| 1980年                                              | [英雄無淚](../Page/英雄無淚_（1980年電影）.md "wikilink")                                         | Heroes Shed No Tears                | 木雞        |
| [惡爺](../Page/惡爺.md "wikilink")                     | Coward Bastard                                                                       |                                     |           |
| 1981年                                              | [魔劍俠情](../Page/魔劍俠情.md "wikilink")                                                   | Return of the Sentimental Swordman  | 西門柔       |
| [黑蜥蜴](../Page/黑蜥蜴_（1981年電影）.md "wikilink")         | Black Lizard                                                                         |                                     |           |
| 1983年                                              | [三闖少林](../Page/三闖少林.md "wikilink")                                                   | Shaolin Intruders                   |           |
| [少林傳人](../Page/少林傳人.md "wikilink")                 | Shaolin Prince                                                                       |                                     |           |
| 1985年                                              | [殭屍先生](../Page/殭屍先生.md "wikilink")                                                   | Mr. Vampire                         | 任老太爺 （殭屍） |
| [福星高照](../Page/福星高照.md "wikilink")                 | My Lucky Star                                                                        |                                     |           |
| [龍的心](../Page/龍的心.md "wikilink")                   | Heart of the Dragon                                                                  | 特警隊成員                               |           |
| 1986年                                              | [富貴列車](../Page/富貴列車_\(電影\).md "wikilink")                                            | The Millionaires' Express           |           |
| [霹靂大喇叭](../Page/霹靂大喇叭.md "wikilink")               | Where's Officer Tuba?                                                                |                                     |           |
| 1987年                                              | [東方禿鷹](../Page/東方禿鷹.md "wikilink")                                                   | Eastern Condors                     | 赤柬領導      |
| [最後一戰](../Page/最後一戰.md "wikilink")                 | The Final Test                                                                       |                                     |           |
| 1988年                                              | [亡命鴛鴦](../Page/亡命鴛鴦.md "wikilink")                                                   | On the Run                          |           |
| [殭屍叔叔](../Page/殭屍叔叔.md "wikilink")                 | Mr.Vampire Saga Four                                                                 | 鄔侍郎                                 |           |
| [畫中仙](../Page/畫中仙.md "wikilink")                   | Picture of a Nymph                                                                   |                                     |           |
| [鬼猛腳](../Page/鬼猛腳.md "wikilink") ( 鬼抓人 )           |                                                                                      | 華探長                                 |           |
| [飛龍猛將](../Page/飛龍猛將.md "wikilink")                 | Dragons Forever                                                                      | |-華老闆                               | 1989年     |
| 1990年                                              | [夜魔先生](../Page/夜魔先生.md "wikilink")                                                   | The Nocturnal Demon                 |           |
| [笑傲江湖](../Page/笑傲江湖_（1990年電影）.md "wikilink")       | Swordsman                                                                            | 左冷禪                                 |           |
| [摩登如來神掌](../Page/摩登如來神掌.md "wikilink")             | Kung Fu Vs Acrobatic                                                                 | 天殘                                  |           |
| [皇家女將](../Page/皇家女將.md "wikilink")                 | She Shoots Straight                                                                  |                                     |           |
| [衛斯理之霸王卸甲](../Page/衛斯理之霸王卸甲.md "wikilink")         | Bury Me High                                                                         | 阮文虎                                 |           |
| 1991年                                              | [西藏小子](../Page/西藏小子.md "wikilink")                                                   | Kid From Tibet                      | 包公        |
| [雷霆掃穴](../Page/雷霆掃穴.md "wikilink")                 | Red Shield                                                                           |                                     |           |
| [龍的傳人](../Page/龍的傳人.md "wikilink")                 | Legend Of The Dragon                                                                 | 周飛鴻                                 |           |
| [情聖](../Page/情聖.md "wikilink")                     | The Magnificent Scoundrels                                                           |                                     |           |
| 1992年                                              | [偷神家族](../Page/偷神家族.md "wikilink")                                                   | The Big Deal                        |           |
| [警察故事3：超级警察](../Page/警察故事3：超级警察.md "wikilink")     | Supercop                                                                             | 豹強                                  |           |
| [漫畫威龍](../Page/漫畫威龍.md "wikilink")                 | Fist of Fury 1991 II                                                                 | 郑横刀                                 |           |
| 1993年                                              | 黑豹天下                                                                                 | *The Black Panther Warriors*        | 血狼        |
| 1994年                                              | [新烏龍院](../Page/新烏龍院.md "wikilink")（[笑林小子II─新烏龍院](../Page/笑林小子II─新烏龍院.md "wikilink")） | Shaolin Popey II: Messy Temple      | 古堡白衣杀手    |
| 1995年                                              | [呆佬拜壽](../Page/呆佬拜壽.md "wikilink")（[傻瓜與野丫頭](../Page/傻瓜與野丫頭.md "wikilink")）           | Only Fools Fall in Love             | 阿弟之父      |
| [緝毒先鋒](../Page/緝毒先鋒.md "wikilink")                 | Drugs Fighters                                                                       |                                     |           |
| [怒海威龍](../Page/怒海威龍.md "wikilink")                 | Tough Beauty and the Sloppy Slop                                                     |                                     |           |
| 1997年                                              | [馬永貞](../Page/馬永貞_\(1997年電影\).md "wikilink")                                         | Hero                                |           |
| 1998年                                              | [獵豹行動](../Page/獵豹行動.md "wikilink")                                                   | Leopard Hunting                     | 黑豹        |
| 2000年                                              | [魔影](../Page/魔影.md "wikilink")                                                       | The Devil Shadwo                    |           |
| 2001年                                              | [最後通牒](../Page/最後通牒.md "wikilink")                                                   | Ultimatum                           |           |
| [趕屍先生](../Page/趕屍先生.md "wikilink")                 | Vampire Controller                                                                   |                                     |           |
| 2004年                                              | [功夫](../Page/功夫_\(電影\).md "wikilink")                                                | Kung Fu Hustle                      | 包租公       |
| [追擊八月十五](../Page/追擊八月十五.md "wikilink")             | Hidden Heroes                                                                        | 劉博士                                 |           |
| 2005年                                              | [情癲大聖](../Page/情癲大聖.md "wikilink")                                                   | A Chinese Tall Story                | 龜丞相       |
| 2005年                                              | [雀聖](../Page/雀聖.md "wikilink")                                                       | *Kung Fu Mahjong*                   | 自摸西       |
| [雀聖2自摸天后](../Page/雀聖2自摸天后.md "wikilink")           | *Kung Fu Mahjong* 2                                                                  |                                     |           |
| [愛上屍新娘](../Page/愛上屍新娘.md "wikilink")               | Dating a Vampire                                                                     | M字頭                                 |           |
| [猛龍](../Page/猛龍.md "wikilink")                     | Dragon Squad                                                                         |                                     |           |
| [阿嫂](../Page/阿嫂_\(電影\).md "wikilink")              | Mob Sister                                                                           |                                     |           |
| [野蠻秘笈](../Page/野蠻秘笈.md "wikilink")                 | My Kung Fu Sweetheart                                                                | 上官達華                                |           |
| 2006年                                              | [打雀英雄傳](../Page/打雀英雄傳.md "wikilink")                                                 | Bet To Basic                        | 華三元       |
| [龍虎門](../Page/龍虎門.md "wikilink")                   | Dragon Tiger Gate                                                                    | 王降龍                                 |           |
| [新忠烈圖](../Page/新忠烈圖.md "wikilink")                 | The Valiant Ones New                                                                 |                                     |           |
| 2007年                                              | [雀聖3自摸三百番](../Page/雀聖3自摸三百番.md "wikilink")                                           | Kung Fu Mahjong 3: The Final Duel   | M字頭       |
| [雙子神偷](../Page/雙子神偷.md "wikilink")                 | Twins Mission                                                                        | 常忠/常勇                               |           |
| [僵屍獎門人](../Page/僵屍獎門人.md "wikilink")               | Vampire Super                                                                        |                                     |           |
| [合約情人](../Page/合約情人.md "wikilink")                 | Contract Lover                                                                       |                                     |           |
| 2008年                                              | [澳大利亚](../Page/澳大利亞_\(電影\).md "wikilink")                                            | Australia                           | Sing Song |
| 2009年                                              | [流浪漢世界盃](../Page/流浪漢世界盃.md "wikilink")                                               | Team of Miracle:We Will Rock You    | 老牧師       |
| [寻找成龙](../Page/寻找成龙.md "wikilink")                 | Searching for Jackie Chan（2009）                                                      | 大将军（客串）                             |           |
| 2010年                                              | [越光寶盒](../Page/越光寶盒.md "wikilink")                                                   | Yue Guang Bao He                    |           |
| [功夫詠春](../Page/功夫詠春.md "wikilink")                 | City Under Siege                                                                     |                                     |           |
| [殭屍新戰士](../Page/殭屍新戰士.md "wikilink")               | Vampire Warriors                                                                     |                                     |           |
| [笑詠春](../Page/笑詠春.md "wikilink")\[1\]              | I Love Wing Chun                                                                     |                                     |           |
| 2011年                                              | [蔡李佛](../Page/蔡李佛_\(電影\).md "wikilink")                                              | CHOY LEE FUT                        |           |
| [瘋拳和武](../Page/瘋拳和武.md "wikilink")                 | Carzy to Martial art And Boxing                                                      |                                     |           |
| 2013年                                              | [笑功震武林](../Page/笑功震武林.md "wikilink")                                                 | Princess and Seven KungFu Masters   | 甲乙炳       |
| [詠春小龍](../Page/詠春小龍.md "wikilink")                 | Wing Chun Xiao Long                                                                  | 小龍父親                                |           |
| 2015年                                              | [道士下山](../Page/道士下山.md "wikilink")                                                   | Monk Comes Down the Mountain        | 彭乾吾       |
| 2016年                                              | [特工爺爺](../Page/特工爺爺.md "wikilink")                                                   | The Bodyguard                       |           |
| 2017年                                              | [泡菜愛上小龍蝦](../Page/泡菜愛上小龍蝦.md "wikilink")                                             | My Kitchen Lover                    | 老怪        |
| 2018年                                              | [唐人街探案2](../Page/唐人街探案2.md "wikilink")                                               | Detective Chinatown Vol. 2          | 莫友乾       |
| [低壓槽](../Page/低压槽_\(电影\).md "wikilink")            | The Trough                                                                           |                                     |           |
| [葉問外傳：張天志](../Page/葉問外傳：張天志.md "wikilink")         | Master Z : The Ip Man Legacy                                                         | 廟祝                                  |           |

### 電視劇（[無綫電視](../Page/電視廣播有限公司.md "wikilink")）

|                                               |                                              |                    |
| --------------------------------------------- | -------------------------------------------- | ------------------ |
| 年份                                            | 劇名                                           | 角色                 |
| 1996年                                         | **[殭屍福星](../Page/殭屍福星.md "wikilink")**       | **莊天**             |
| 1997年                                         | **[男人四十打功夫](../Page/男人四十打功夫.md "wikilink")** | **方天龍**            |
| 1998年                                         | [大澳的天空](../Page/大澳的天空.md "wikilink")         | 石福根                |
| 1999年                                         | [茶是故鄉濃](../Page/茶是故鄉濃.md "wikilink")         | 謝虎彪（鷓鴣爺：宋鴻英之入贅丈夫）  |
| [人龍傳說](../Page/人龍傳說.md "wikilink")            | 葉問（第1、9集）                                    |                    |
| 2000年                                         | [撻出愛火花](../Page/撻出愛火花.md "wikilink")         | 羅彬                 |
| [大囍之家](../Page/大囍之家.md "wikilink")            | 梁寬                                           |                    |
| [京城教一](../Page/京城教一.md "wikilink")            | 譚天                                           |                    |
| 2001年                                         | [勇往直前](../Page/勇往直前_\(電視劇\).md "wikilink")   | 甘國華                |
| [封神榜](../Page/封神榜_\(2001年電視劇\).md "wikilink") | [李靖](../Page/李靖_\(神話人物\).md "wikilink")      |                    |
| [酒是故鄉醇](../Page/酒是故鄉醇.md "wikilink")          | 金吉祥（「九舖香」蒸煮房新主管）                             |                    |
| [尋秦記](../Page/尋秦記.md "wikilink")              | 元宗（第2-3集）                                    |                    |
| 2002年                                         | [烈火雄心II](../Page/烈火雄心II.md "wikilink")       | 關安蔭（蝴蝶灣滅火輪消防局之總隊目） |
| [我要Fit一Fit](../Page/我要Fit一Fit.md "wikilink")  | 蘇少林（裁縫師父）                                    |                    |
| [戇夫成龍](../Page/戇夫成龍.md "wikilink")            | 包慶豐（慶豐行[老闆](../Page/老闆.md "wikilink")）       |                    |
| 2003年                                         | [撲水冤家](../Page/撲水冤家.md "wikilink")           | 林火慶                |
| 2004年                                         | [水滸無間道](../Page/水滸無間道.md "wikilink")         | 宋波                 |
| 2005年                                         | [秀才遇著兵](../Page/秀才遇著兵.md "wikilink")         | 陸戰                 |
| [佛山贊師父](../Page/佛山贊師父.md "wikilink")          | 黃華寶                                          |                    |
| 2007年                                         | [天機算](../Page/天機算.md "wikilink")             | 李承天                |
| [緣來自有機](../Page/緣來自有機.md "wikilink")          | 葉長發                                          |                    |
| [鐵咀銀牙](../Page/鐵咀銀牙.md "wikilink")            | 納蘭耿                                          |                    |
| [我外母唔係人](../Page/我外母唔係人.md "wikilink")        | 林水                                           |                    |
| 2011年                                         | [隔離七日情](../Page/隔離七日情.md "wikilink")         | 何仕燦                |
| [蓋世孖寶](../Page/蓋世孖寶.md "wikilink")            | 狄英偉                                          |                    |
| [法證先鋒III](../Page/法證先鋒III.md "wikilink")      | 布順興                                          |                    |

### 電視劇（[香港電視網絡](../Page/香港電視網絡.md "wikilink")）

|       |                                        |     |
| ----- | -------------------------------------- | --- |
| 年份    | 劇名                                     | 角色  |
| 2015年 | [惡毒老人同盟](../Page/惡毒老人同盟.md "wikilink") | 丁　豹 |

### 電視劇（中國大陸）

|       |                                       |                                    |
| ----- | ------------------------------------- | ---------------------------------- |
| 年份    | 劇名                                    | 角色                                 |
| 2016年 | [新萧十一郎](../Page/新萧十一郎.md "wikilink")  | [司空摘星](../Page/司空摘星.md "wikilink") |
| 2016年 | [吉祥天宝](../Page/吉祥天宝.md "wikilink")    | 张半仙                                |
| 2017年 | [木木川](../Page/木木川.md "wikilink")(網絡劇) | 木爸                                 |
| 2018年 | [北國英雄](../Page/北國英雄.md "wikilink")    | 趙太一                                |

### 動作／武術指導

  - [鹿鼎記](../Page/鹿鼎記.md "wikilink") Tales of an Eunuch（1983）
  - [夏日福星](../Page/夏日福星.md "wikilink") Twinkle, Twinkle, Lucky
    Stars（1985）
  - [時來運轉](../Page/時來運轉.md "wikilink") Those Merry Souls（1985）
  - [急凍奇俠](../Page/急凍奇俠.md "wikilink") Iceman Cometh（1985）
  - [摩登如來神掌](../Page/摩登如來神掌.md "wikilink") Kung Fu vs. Acrobatic（1990）
  - [龍的傳人](../Page/龍的傳人.md "wikilink") Legend of the Dragon（1991）

### 音樂錄像

  - 2016年：[李克勤](../Page/李克勤.md "wikilink")《一個都不能少》 MV

## 獎項

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>頒獎典禮</strong></p></td>
<td><p><strong>獎項</strong></p></td>
<td><p><strong>影片</strong></p></td>
<td><p><strong>結果</strong></p></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p><a href="../Page/2005年香港電影金像獎.md" title="wikilink">香港電影金像獎</a></p></td>
<td><p>最佳男配角</p></td>
<td><p>《<a href="../Page/功夫_(電影).md" title="wikilink">功夫</a>》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/香港電影金紫荊獎.md" title="wikilink">香港電影金紫荊獎</a></p></td>
<td><p>最佳男配角</p></td>
<td><p>《<a href="../Page/功夫_(電影).md" title="wikilink">功夫</a>》</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第42屆金馬獎.md" title="wikilink">第42屆金馬獎</a></p></td>
<td><p>最佳男配角</p></td>
<td><p>《<a href="../Page/功夫_(電影).md" title="wikilink">功夫</a>》</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 軼事

  - [洪金寶在](../Page/洪金寶.md "wikilink")2008年「不是帝后不聚首」接受訪問時說，在「中國戲劇研究學院」時期，大師兄洪金寶曾經有次逃跑，元華會偷偷留飯給他吃，後來還代洪金寶挨了70棍（打斷2根藤條），洪金寶卻並未被打，洪金寶不再跟他打架。

<!-- end list -->

  - 在「人皮燈籠」拍攝時，曾千鈞一髮。當時他從地上跳出來，一會意過來竹劍已經到眼前，他趕緊一翻身，竹劍刺中他的脅下，當初不敢把劍拔出，只能先鋸斷。元華回憶，拍電影真的沒什麼防護措施，其實用紙筒代替也可以，實在太危險。

<!-- end list -->

  - 最難忘的經驗是從直昇機跳海，一入海好像大捶頭捶到胸口，話都說不出來，最後喝了2瓶白蘭地才讓血液暢通。

<!-- end list -->

  - 元華說自己自由自在，並未像師兄師弟成立班底（洪家班或成家班）。

<!-- end list -->

  - 在談到當李小龍替身時，他表示李小龍屬於練拳腳功夫，他則是練舞台，拳腳在鏡頭前打不一定漂亮，所以他作為替身處理翻跟斗。

## 资料来源

## 外部連結

  - [演员元华的微博](http://www.weibo.com/u/2311534207)

  -
  -
  -
  -
[Category:香港電影金紫荊獎最佳男配角得主](../Category/香港電影金紫荊獎最佳男配角得主.md "wikilink")
[Wah](../Category/容姓.md "wikilink")
[Category:天津人](../Category/天津人.md "wikilink")
[Category:华裔加拿大人](../Category/华裔加拿大人.md "wikilink")
[Category:20世紀男演員](../Category/20世紀男演員.md "wikilink")
[Category:21世紀男演員](../Category/21世紀男演員.md "wikilink")
[Category:香港男演員](../Category/香港男演員.md "wikilink")
[Category:香港電影男演員](../Category/香港電影男演員.md "wikilink")
[Category:香港武打演員](../Category/香港武打演員.md "wikilink")
[Category:動作片演員](../Category/動作片演員.md "wikilink")
[Category:香港武術家](../Category/香港武術家.md "wikilink")
[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")
[Category:前香港電視網絡藝員](../Category/前香港電視網絡藝員.md "wikilink")
[容](../Category/香港新教徒.md "wikilink")

1.  [笑詠春](http://movie.now.com/home/movie?id=Ilovewingchun)