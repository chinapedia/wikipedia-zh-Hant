[Brother_Hotel,_Taipei_20180505.jpg](https://zh.wikipedia.org/wiki/File:Brother_Hotel,_Taipei_20180505.jpg "fig:Brother_Hotel,_Taipei_20180505.jpg")
[Brother_Hotel_entrance_20170813.jpg](https://zh.wikipedia.org/wiki/File:Brother_Hotel_entrance_20170813.jpg "fig:Brother_Hotel_entrance_20170813.jpg")
**兄弟大飯店**（**Brother
Hotel**），位於[臺灣](../Page/臺灣.md "wikilink")[臺北市](../Page/臺北市.md "wikilink")[松山區](../Page/松山區.md "wikilink")[南京東路](../Page/南京東路.md "wikilink")，為臺北市內緊臨[臺北捷運車站出口的](../Page/臺北捷運.md "wikilink")[五星級觀光飯店之一](../Page/五星級.md "wikilink")，1979年9月13日創立，由[洪騰勝](../Page/洪騰勝.md "wikilink")、[洪騰榮](../Page/洪騰榮.md "wikilink")、[洪瑞麟](../Page/洪瑞麟.md "wikilink")、[洪瑞河](../Page/洪瑞河.md "wikilink")、[洪-{杰}-等五兄弟合資經營](../Page/洪杰.md "wikilink")。早年以接待[日本籍客人為主](../Page/日本.md "wikilink")，餐廳以[臺菜知名](../Page/臺菜.md "wikilink")。曾創建[兄弟大飯店棒球隊與](../Page/兄弟大飯店棒球隊.md "wikilink")[中華職棒](../Page/中華職棒.md "wikilink")[兄弟象隊](../Page/兄弟象隊.md "wikilink")。

## 簡介

兄弟大飯店座落於[南京東路商圈](../Page/南京東路商圈.md "wikilink")，有250間客房和8間各式料理餐廳。周圍環境有眾多[中小企業群](../Page/中小企業.md "wikilink")，鄰近捷運[南京復興站出入口](../Page/南京復興站.md "wikilink")。相較於其他台北市內的觀光飯店，形象較為平價。

## 外部連結

  - [兄弟大飯店](http://www.brotherhotel.com.tw/)
      -
[B](../Category/交通部觀光局評鑑五星級旅館.md "wikilink")
[B](../Category/兄弟象.md "wikilink")
[B](../Category/台北市旅館.md "wikilink")
[B](../Category/1979年完工建築物.md "wikilink")
[B](../Category/總部位於臺北市松山區的工商業機構.md "wikilink")