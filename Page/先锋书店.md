[Librairie_avant-garde.jpg](https://zh.wikipedia.org/wiki/File:Librairie_avant-garde.jpg "fig:Librairie_avant-garde.jpg")
[Xianfengshudian_20160808.jpg](https://zh.wikipedia.org/wiki/File:Xianfengshudian_20160808.jpg "fig:Xianfengshudian_20160808.jpg")
**南京先锋书店**（），为[南京大学写作班的毕业生](../Page/南京大学.md "wikilink")[钱晓华创办于](../Page/钱晓华.md "wikilink")1996年的一家书店，由于其风格独特，在[南京已经颇具影响](../Page/南京.md "wikilink")，甚至被戏称为“南大的第二图书馆”。

先锋书店除了请作家签名售书，还会举办一些文化讲座、展览，乃至邀请乐队前来表演，吸引了不少读者前来参与。平时书店免费提供茶水、沙发、长椅等。

目前先锋书店在南京有五台山、龙江、南京博物院三家书店，店内除书籍外还设有独立先锋创意馆，售卖创意商品，特色饰品等。五台山店有先锋艺术咖啡，供给饮料、点心等，亦设有专区可无线上网。

被美國CNN譽為「中国最美的书店」\[1\]。2014年1月台灣《遠見雜誌》331期報導：「一個破舊的防空洞，竟變成中國最美的書店」\[2\]。

## 参考

  - 钱晓华等，《先锋书店：大地上的异乡者》，广西师范大学出版社，2005年10月，ISBN 7-5633-5722-X/I

## 外部链接

  - [先锋书店：读书人的天堂
    人民网](http://www.people.com.cn/GB/14738/14754/21863/2813821.html)
  - [我所看见的异乡者](http://www.china.com.cn/chinese/archive/184857.htm)

[Category:南京书店](../Category/南京书店.md "wikilink")
[Category:中国内地民营书店](../Category/中国内地民营书店.md "wikilink")
[Category:独立书店](../Category/独立书店.md "wikilink")
[Category:南京文化](../Category/南京文化.md "wikilink")

1.
2.