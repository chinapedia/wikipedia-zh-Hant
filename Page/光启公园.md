[The_tomb_of_Xuguangqi.jpg](https://zh.wikipedia.org/wiki/File:The_tomb_of_Xuguangqi.jpg "fig:The_tomb_of_Xuguangqi.jpg")

**光启公园**位于[上海市](../Page/上海市.md "wikilink")[徐汇区](../Page/徐汇区.md "wikilink")[南丹路](../Page/南丹路.md "wikilink")，原名**南丹公园**，1983年因纪念[徐光启逝世](../Page/徐光启.md "wikilink")350周年而改为现名，园内主体建筑是[徐光启墓](../Page/徐光启墓.md "wikilink")。2003年，上海现存最古老的民居[南春华堂因抢救性保护搬迁于其中](../Page/南春华堂.md "wikilink")，被辟为[徐光启纪念馆](../Page/徐光启纪念馆.md "wikilink")。光启公园地处上海主要商业区之一的[徐家汇](../Page/徐家汇.md "wikilink")，附近有[徐家汇圣依纳爵主教座堂](../Page/徐家汇圣依纳爵主教座堂.md "wikilink")、[天主教上海教区](../Page/天主教上海教区.md "wikilink")[主教府](../Page/主教府.md "wikilink")、[中国科学院上海天文台](../Page/中国科学院上海天文台.md "wikilink")、上海市气象局、[徐汇区政府大楼](../Page/徐汇区.md "wikilink")、[上海轨道交通](../Page/上海轨道交通.md "wikilink")[徐家汇站](../Page/徐家汇站.md "wikilink")。

## 徐光启墓

**徐光启墓**是明末科学家、思想家、政治家、军事家，[天主教](../Page/天主教.md "wikilink")[教友领袖和](../Page/教友.md "wikilink")[护教士](../Page/护教士.md "wikilink")，[礼部尚书兼](../Page/礼部尚书.md "wikilink")[文渊阁大学士](../Page/文渊阁大学士.md "wikilink")[徐光启](../Page/徐光启.md "wikilink")（1562年4月24日－1633年11月10日）及其部分亲属的墓地。

徐光启家族墓地位于2条河流（[肇嘉浜和](../Page/肇嘉浜.md "wikilink")[法华泾](../Page/法华泾.md "wikilink")）的汇合处，附近后来形成村落，称为[徐家汇](../Page/徐家汇.md "wikilink")。1842年天主教[耶稣会重新来华后](../Page/耶稣会.md "wikilink")，在1847年选中徐家汇建立江南传教区的总部，陆续建成一系列教会机构，形成一大片教会区。1914年，徐家汇以东地区并入[上海法租界](../Page/上海法租界.md "wikilink")，因而徐光启墓所在的教会区也就自然融入了[上海市区](../Page/上海市区.md "wikilink")。1990年代以后，紧邻徐光启墓的[徐家汇地区迅速演变为上海最主要的商圈之一](../Page/徐家汇.md "wikilink")，陆续开设许多商店，矗立起许多高层建筑。

崇祯十四年（1641年），徐光启归葬于上海。墓地共十个墓穴，葬有徐光启、夫人吴氏和他的四个孙辈夫妇。墓地附近形成村落，后称为[徐家汇](../Page/徐家汇.md "wikilink")。光绪二十九年（1903年），天主教江南代牧区在徐光启[受洗三百周年之际](../Page/受洗.md "wikilink")，重修了墓地。1933年徐光启逝世三百周年，墓地又获重修。[抗日战争期间](../Page/抗日战争.md "wikilink")，墓地荒废成为[菜畦](../Page/菜畦.md "wikilink")。1957年，[上海市文化局重修徐光启墓](../Page/上海市文化局.md "wikilink")，复建十字架基台，并将其列为[上海市文物保护单位](../Page/上海市文物保护单位.md "wikilink")。“[文化大革命](../Page/文化大革命.md "wikilink")”期间，墓地的牌坊、华表遭到破坏。1978年墓地重辟为南丹公园，此后陆续修建了一系列纪念物。1983年徐光启逝世350周年，**南丹公园**改名为**光启公园**。1988年，徐光启墓被[中华人民共和国国务院列为](../Page/中华人民共和国国务院.md "wikilink")[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")。2003年12月，徐光启墓修复工程竣工，恢复了1903年的墓制。\[1\]

徐光启墓占地300平方米，高2.2米，为椭圆形大墓。墓前立有石碑、[十字架](../Page/十字架.md "wikilink")，石人、石马、华表和石牌坊。墓碑为数学家[苏步青手书](../Page/苏步青.md "wikilink")“明徐光启之墓”。石牌坊刻有[对联](../Page/对联.md "wikilink")：“治历明农百世师经天纬地，出将入相一个臣奋武揆文”。墓东侧为碑廊，刻有徐光启手迹、[查继佐撰](../Page/查继佐.md "wikilink")《徐光启传》及[程十髮临摹的徐光启画像](../Page/程十髮.md "wikilink")。\[2\]

## 南春华堂

[Xu_Guangqi_Memorial_Hall.jpg](https://zh.wikipedia.org/wiki/File:Xu_Guangqi_Memorial_Hall.jpg "fig:Xu_Guangqi_Memorial_Hall.jpg")

上海现存最古老的民居**南春华堂**原位于徐汇区[梅陇路南春华堂](../Page/梅陇路.md "wikilink")5号，2003年经抢救性保护搬迁至光启公园并辟为**徐光启纪念馆**。南春华堂建于明[弘治末年至](../Page/弘治.md "wikilink")[正德年间](../Page/正德.md "wikilink")，距今已有500多年的历史。\[3\]

## 徐光启纪念馆

[Statue_of_Xu_Guangqi.jpg](https://zh.wikipedia.org/wiki/File:Statue_of_Xu_Guangqi.jpg "fig:Statue_of_Xu_Guangqi.jpg")旁边的徐光启纪念馆里的徐光启雕像\]\]
[Preface_of_Euclid's_Elements_by_Xu_Guangqi.jpg](https://zh.wikipedia.org/wiki/File:Preface_of_Euclid's_Elements_by_Xu_Guangqi.jpg "fig:Preface_of_Euclid's_Elements_by_Xu_Guangqi.jpg")》的第一句话（徐光启手迹集字）\]\]

南春华堂院内回廊陈列石刻名人题词和徐光启手迹。陈列室门前有一座徐光启雕像。

### 陈列室展品

<File:Xu> Guangqi.jpg|徐光启画像 <File:Wife> of Xu Guangqi.jpg|徐光启唯一的夫人吴氏的画像

## 参考文献

## 参见

  - [徐家汇](../Page/徐家汇.md "wikilink")：[徐家汇圣依纳爵主教座堂](../Page/徐家汇圣依纳爵主教座堂.md "wikilink")
  - [徐光启](../Page/徐光启.md "wikilink")

{{-}}

[墓](../Category/徐光启.md "wikilink")
[Category:徐家汇](../Category/徐家汇.md "wikilink")
[Category:徐汇区公园](../Category/徐汇区公园.md "wikilink")

1.

2.
3.