**光復會**又稱「**復古會**」，是[清朝末年的反清革命組織](../Page/清朝.md "wikilink")，主要由[江浙人組成](../Page/江浙.md "wikilink")，主要活動於江浙一帶和南洋等地。宗旨為「光復漢族，還我山河，以身許國，功成身退」。1904年11月於上海成立，與[同盟會呈競爭關係](../Page/同盟會.md "wikilink")，辛亥革命後因[孫中山](../Page/孫中山.md "wikilink")、[陳其美等人与](../Page/陳其美.md "wikilink")[陶成章](../Page/陶成章.md "wikilink")、許雪秋等領導人发生冲突，1912年組織解散。

## 歷史

1903年（光绪二十九年）冬由[蒋尊簋](../Page/蒋尊簋.md "wikilink")、[陶成章](../Page/陶成章.md "wikilink")、[魏蘭](../Page/魏蘭.md "wikilink")、[龔寶銓](../Page/龔寶銓.md "wikilink")、王嘉伟等人在东京酝酿协商。

1904年10月，由[軍國民教育會成員陶成章](../Page/軍國民教育會.md "wikilink")、[蔡元培](../Page/蔡元培.md "wikilink")、龔寶銓、魏蘭等發起，在軍國民教育會的基礎上改組擴大而建立。會址設於[上海](../Page/上海.md "wikilink")，推蔡元培為會長，陶成章任副会长。
時在獄中的[章太炎也參與了該會的籌建工作](../Page/章太炎.md "wikilink")。陶成章為該會的中心人物。他聯絡江、浙一帶的會黨作為革命的主要力量，大力發展組織。[徐錫麟](../Page/徐錫麟.md "wikilink")、[秋瑾也相繼入會](../Page/秋瑾.md "wikilink")，並成為該會的領導人。

次年，徐錫麟等在紹興創辦[大通學堂](../Page/大通學堂.md "wikilink")，學生560餘人皆為該會會員。該會之會員以金牌為徽章，中鏤一「復」字[篆文](../Page/篆文.md "wikilink")(旁刻真楷)。其口號為：黃河源溯浙江潮，衛我中華漢族豪。莫使滿胡留片甲，軒轅神胄是天驕。」該學堂專重兵式體操，以為革命之備，遂成為光復會宣傳革命，培養軍事骨幹，聯絡會黨的機關。該會誓詞「光復漢族，還我山河，以身許國，功成身退」為宗旨。首領則鏤一「黃」字，協領鏤一「河」字，分領鏤一「源」字，如是次第，共分為十七部門。\[1\]

1905年[同盟會成立後](../Page/同盟會.md "wikilink")，光復會多數成員加入同盟會，部分會員仍獨立活動。1907年徐錫麟因發動[安慶起義失敗而犧牲](../Page/安慶起義.md "wikilink")，秋瑾也在[紹興遇害](../Page/紹興.md "wikilink")，光復會活動一度停頓。同年3月，陶成章在日本因同盟會內經費問題，與[孫中山發生分歧](../Page/孫中山.md "wikilink")。1908年陶成章到南洋募款又受到當地同盟會員阻止，遂重新以光復會名義進行活動。1910年2月，[章太炎](../Page/章太炎.md "wikilink")、陶成章與同盟會正式分裂，在[東京成立光復會總部](../Page/東京.md "wikilink")。章为会长，陶為副會長。[魏蘭](../Page/魏蘭.md "wikilink")、[李燮和等在南洋組織光復會南洋總部](../Page/李燮和.md "wikilink")，代行東京總部職權。

1911年，該會成員浙江[新军二十一镇八十一标标统](../Page/陆军第二十一镇.md "wikilink")[朱瑞組織](../Page/朱瑞.md "wikilink")[浙軍](../Page/浙軍.md "wikilink")，自任總司令；李燮和任[昊淞光復軍總司令](../Page/昊淞光復軍.md "wikilink")；[許雪秋](../Page/許雪秋.md "wikilink")、[陳芸生任](../Page/陳芸生.md "wikilink")[廣東民軍司令](../Page/廣東民軍.md "wikilink")，響應[武昌起義](../Page/武昌起義.md "wikilink")。1912年1月3日，[章炳麟在上海另組](../Page/章炳麟.md "wikilink")[中華民國聯合會](../Page/中華民國聯合會.md "wikilink")。後因陶成章在上海被[蔣中正刺殺](../Page/蔣中正.md "wikilink")，該會遂解體。

## 会员举录

光绪三十年（1904），光复会成立之初，上海“[复古会](../Page/复古会.md "wikilink")”、“[对俄同志会](../Page/对俄同志会.md "wikilink")”，东京“[浙学会](../Page/浙学会.md "wikilink")”、“[军国民教育会](../Page/军国民教育会.md "wikilink")”，其会员均加入光复会。《紹興市-{志}-》立傳者32人，簡介事跡者69人。\[2\]

### 本志《人物》卷立传32人：

蔡元培、陶成章、徐锡麟、秋瑾（女）、[陈伯平](../Page/陈伯平.md "wikilink")、[王金发](../Page/王金发.md "wikilink")、[谢震](../Page/谢震.md "wikilink")、[尹锐志](../Page/尹锐志.md "wikilink")（女）、[尹维峻](../Page/尹维峻.md "wikilink")（女）、[沈钧业](../Page/沈钧业.md "wikilink")、[陈魏](../Page/陈魏.md "wikilink")、[孙德卿](../Page/孙德卿.md "wikilink")、[王子余](../Page/王子余.md "wikilink")、[周树人](../Page/周树人.md "wikilink")（鲁迅）、[陶冶公](../Page/陶冶公.md "wikilink")、[陈威](../Page/陈威.md "wikilink")、[许寿裳](../Page/许寿裳.md "wikilink")、[裘吉生](../Page/裘吉生.md "wikilink")、[陈仪](../Page/陈仪.md "wikilink")、蒋尊簋、[张伯岐](../Page/张伯岐.md "wikilink")、[王晓籁](../Page/王晓籁.md "wikilink")、[马逢伯](../Page/马逢伯.md "wikilink")、[何燮侯](../Page/何燮侯.md "wikilink")、[蒋智由](../Page/蒋智由.md "wikilink")、[孙世伟](../Page/孙世伟.md "wikilink")、[刘大白](../Page/刘大白.md "wikilink")、[周清](../Page/周清.md "wikilink")、[许铁崖](../Page/许铁崖.md "wikilink")、[张载阳](../Page/张载阳.md "wikilink")、[斯烈](../Page/斯烈.md "wikilink")、[竺绍康](../Page/竺绍康.md "wikilink")。

### 會員表（69人）

<table>
<thead>
<tr class="header">
<th></th>
<th><p>姓名</p></th>
<th><p>戶籍</p></th>
<th><p>個人簡介</p></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/孙晓云.md" title="wikilink">孙晓云</a>（1880～1965）</p></td>
<td><p>上虞崧厦镇</p></td>
<td><p><a href="../Page/陶成章.md" title="wikilink">陶成章之妻</a></p></td>
<td><p>1965年在上海去世。</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/赵汉卿.md" title="wikilink">赵汉卿</a>（1887年－1950年）</p></td>
<td><p>山阴临浦（今萧山）</p></td>
<td><p>上海銀行<a href="../Page/工會.md" title="wikilink">工會幹部</a></p></td>
<td><p>1950年去世。</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/陳捥瀾_(光復會會員).md" title="wikilink">陳捥瀾</a>（1887～1917）</p></td>
<td><p>绍兴平水</p></td>
<td><p><a href="../Page/陈伯平.md" title="wikilink">陈伯平之妹</a>。因<a href="../Page/秋瑾.md" title="wikilink">秋瑾的介绍而入</a><a href="../Page/光复会.md" title="wikilink">光复会</a>。</p></td>
<td><p>1917年去世。</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/王振漢.md" title="wikilink">王振漢</a>（1872～1926）</p></td>
<td><p>绍兴柯桥</p></td>
<td><p>绍兴柯桥王增卿长女。1888年，在东浦与<a href="../Page/徐锡麟.md" title="wikilink">徐锡麟结婚</a>。</p></td>
<td><p>1926年去世。</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/吴惠秋.md" title="wikilink">吴惠秋</a>（1886～1977）</p></td>
<td><p>吴兴县浔溪</p></td>
<td><p>女，原名吴珉，后改名惠秋，吴兴县浔溪人。光绪三十二年（1906）在浔溪女学读书，是秋瑾的学生。因抗婚离家，被秋瑾收留带回绍兴，住在和畅堂秋家，由秋介绍入光复会，成为秋瑾的机要秘书和得力助手。皖案发，即协助秋瑾处置起义计划及枪械等，沉着应变。初六日，秋瑾成仁后，吴历尽艰险逃赴上海，由徐寄尘帮助学医，以护士为掩护进行革命联络工作。宣统三年（1911），参加光复上海之役作救护。辛亥革命后在上海竞雄女学任教。1949年后任上海文史馆馆员。</p></td>
<td><p>1977</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>曹钦熙（1870～1908）</p></td>
<td><p>绍兴东浦</p></td>
<td><p>字荔泉，绍兴东浦人。为<a href="../Page/许仲卿.md" title="wikilink">许仲卿师长</a>。许仲卿出资助徐锡麟，多受钦熙影响。光绪三十一年（1905），大通学堂成立之初，徐锡麟聘钦熙任总理，入光复会。不久随徐锡麟往北京，赴东北考察，帮助徐筹划武装起义。皖浙起义失败，曹遭清政府通缉，避走乡间，不久因忧愤致疾死。</p></td>
<td><p>1908</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考

<references/>

  - 石田米子：〈[光复会与浙江辛亥革命综述](http://www.nssd.org/articles/Article_Read.aspx?id=1002315372)〉。

## 参见

  - [华兴会](../Page/华兴会.md "wikilink")

  - [兴中会](../Page/兴中会.md "wikilink")

  - [同盟会](../Page/同盟会.md "wikilink")

  - [丈夫团](../Page/丈夫团.md "wikilink")

  - [中华革命党](../Page/中华革命党.md "wikilink")

  - [中国国民党](../Page/中国国民党.md "wikilink")

  - [黄花岗起义](../Page/黄花岗起义.md "wikilink")

  - [辛亥革命](../Page/辛亥革命.md "wikilink")

  - [越南光復會](../Page/越南光復會.md "wikilink")

  -
[光復會](../Category/光復會.md "wikilink")
[Category:1904年建立的組織](../Category/1904年建立的組織.md "wikilink")

1.  [曹亞伯](../Page/曹亞伯.md "wikilink")，〈武昌革命真史（十一），載《[春秋雜誌](../Page/春秋雜誌.md "wikilink")》，[香港](../Page/香港.md "wikilink")，1984年6月1日，第646期，第14頁。
2.