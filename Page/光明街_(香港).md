[Kwong_Ming_Street_(clear_view).jpg](https://zh.wikipedia.org/wiki/File:Kwong_Ming_Street_\(clear_view\).jpg "fig:Kwong_Ming_Street_(clear_view).jpg")
**光明街**（）是位於[香港](../Page/香港.md "wikilink")[灣仔區的一條街道](../Page/灣仔區.md "wikilink")，它的左鄰右舍是[日街](../Page/日街.md "wikilink")、[月街和](../Page/月街.md "wikilink")[星街](../Page/星街.md "wikilink")。

從前，光明街只有一柱[街燈](../Page/街燈.md "wikilink")，有[燈蛾飛舞](../Page/燈蛾.md "wikilink")，晚飯之后，有老[街坊拉](../Page/街坊.md "wikilink")[二胡](../Page/二胡.md "wikilink")、唱[粵曲等](../Page/粵曲.md "wikilink")。

舊稱聖嬰孩里(Holy Infant Lane)， 相信是紀念當年天主教會在收養棄嬰的工作。

## 歷史

當年住在灣仔的人多是貧苦大眾，賭博和賣淫業猖獗，許多女子墮入火坑，街上常有嬰兒被遺棄。1848年聖保祿女修會4名修女來港，在進教圍設立孤兒院「聖童之家」收養棄嬰，1853年遷往附近的海旁東（今蘭杜街和晏頓街之間）繼續服務。由於光明街，舊稱聖嬰孩里(Holy
Infant Lane)， 相信是紀念當年天主教會在收養棄嬰的工作。 而後來由於第一間發電廠在附近落成， 為紀錄此事，
街道在1925年6月12日才改名為光明街， 比喻以電力帶來光明。

## 事件

光明街不是一條大街，但是在[市政局舉辦的](../Page/市政局.md "wikilink")「第六屆中文文學創作獎」(1986年)之中，一位参賽者[梁世榮](../Page/梁世榮.md "wikilink")，以描寫[灣仔光明街為題](../Page/灣仔.md "wikilink")，感動了評審團，奪得[散文組第一名](../Page/散文.md "wikilink")。而此文章載於《香港文學展顏:第四輯》。梁世榮[博士是](../Page/博士.md "wikilink")[香港理工大學應用](../Page/香港理工大學.md "wikilink")[社會科學系副](../Page/社會科學.md "wikilink")[教授](../Page/教授.md "wikilink")[1](http://www.dbi.org.hk/xingqing/issue11/focus_3.htm)。

## 鄰近

  - [聖母聖衣堂](../Page/聖母聖衣堂.md "wikilink")
  - [萬豪閣](../Page/萬豪閣.md "wikilink")

## 外部連結

  - [灣仔光明街地圖](http://www.centamap.com/scripts/centamap2.asp?lg=B5&cx=835459&cy=815237&zm=1&mx=835459&my=815237&ms=2&sx=&sy=&ss=0&sl=&vm=&lb=&ly=)

[Category:灣仔街道](../Category/灣仔街道.md "wikilink")