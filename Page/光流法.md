**光流**(Optical flow or optic
flow)是关于视域中的物体[运动检测中的概念](../Page/运动检测.md "wikilink")。用来描述相对于观察者的运动所造成的观测目标、表面或边缘的运动。光流法在[樣型識别](../Page/樣型識别.md "wikilink")、[计算机視覺以及其他](../Page/计算机視覺.md "wikilink")[影像處理領域中非常有用](../Page/影像處理.md "wikilink")，可用于运动检测、物件切割、碰撞时间与物体膨胀的计算、运动补偿编码，或者通过物体表面与边缘进行立体的测量等等。

## 光流的测算

光流法实际是通过检测图像像素点的强度随时间的变化进而推断出物体移动速度及方向的方法。

在 2D+*t* 维的情况下（3D 和更高维度亦然），假设位于 \((x, y, t)\)
的[体素的亮度是](../Page/体素.md "wikilink")
\(I(x, y, t)\)。该体素在两个图像帧之间移动了
\(\Delta x\)、\(\Delta y\)、\(\Delta t\)。于是可以得出一个亮度相同的结论：

\[I(x,y,t) = I(x+\Delta x, y + \Delta y, t + \Delta t)\]

假设该移动很小，那么可以根据[泰勒级数得出](../Page/泰勒级数.md "wikilink")：

\[I(x+\Delta x,y+\Delta y,t+\Delta t) = I(x,y,t) + \frac{\partial I}{\partial x}\Delta x+\frac{\partial I}{\partial y}\Delta y+\frac{\partial I}{\partial t}\Delta t+\][H.O.T.](../Page/摄动理论.md "wikilink")

因此可以推出：

\[\frac{\partial I}{\partial x}\Delta x+\frac{\partial I}{\partial y}\Delta y+\frac{\partial I}{\partial t}\Delta t = 0\]
或

\[\frac{\partial I}{\partial x}\frac{\Delta x}{\Delta t}+\frac{\partial I}{\partial y}\frac{\Delta y}{\Delta t}+\frac{\partial I}{\partial t}\frac{\Delta t}{\Delta t} = 0\]

最终可得出结论：

\[\frac{\partial I}{\partial x}V_x+\frac{\partial I}{\partial y}V_y+\frac{\partial I}{\partial t} = 0\]
这里的 \(V_x,V_y\) 是 \(x\) 和 \(y\) 方向上的速率，或称为 \(I(x,y,t)\) 的光流。而
\(\tfrac{\partial I}{\partial x}\), \(\tfrac{\partial I}{\partial y}\) 和
\(\tfrac{\partial I}{\partial t}\) 则是图像 \((x,y,t)\)
在对应方向上的[偏导数](../Page/偏导数.md "wikilink")。\(I_x\)、\(I_y\)
和 \(I_t\) 的关系可用下式表述：

\[I_xV_x+I_yV_y=-I_t\] 或

\[\nabla I^T\cdot\vec{V} = -I_t\]
这是两个未知数中的一个方程，不能这样求解。这被称为光流算法的孔径问题。为了找到光流，需要另一组方程，由附加的约束给出。所有光流方法都引入了估算实际流量的附加条件.

## 一些求光流的方法

  - [相位相关](../Page/相位相关.md "wikilink")

  - [块相关](../Page/块相关.md "wikilink") (误差绝对值和,
    标准化[互相关](../Page/互相关.md "wikilink"))

  - [梯度约束](../Page/梯度约束.md "wikilink")-相关的对齐

  - [卢卡斯-卡纳德方法](../Page/卢卡斯-卡纳德方法.md "wikilink")（Lucas-Kanade Method）

  - （Horn Schunck Method）

[Category:计算机视觉](../Category/计算机视觉.md "wikilink")