**克倫邦**
（，[斯高克倫語](../Page/斯高克倫語.md "wikilink")：）或譯**卡倫邦**，是[緬甸的一個邦](../Page/緬甸.md "wikilink")，位於該國東南部內陸。面積30,383平方公里，人口1,431,377人。首府[帕安](../Page/帕安.md "wikilink")。下分3縣。

當地的[克倫族](../Page/克倫族.md "wikilink")
（自1948年以來）和其他少數民族正與[緬甸軍政府進行](../Page/緬甸軍政府.md "wikilink")[游擊戰](../Page/游擊戰.md "wikilink")。

## 行政區劃

  - [帕安縣](../Page/帕安縣.md "wikilink")（）
  - [高加力縣](../Page/高加力縣.md "wikilink")（）
  - [苗瓦迪縣](../Page/苗瓦迪縣.md "wikilink")（）

## 參考資料

{{-}}

[Category:緬甸的邦](../Category/緬甸的邦.md "wikilink")