**克利夫蘭·阿貝**（**Cleveland
Abbe**，），[美國](../Page/美國.md "wikilink")[氣象學家](../Page/氣象學家.md "wikilink")，生於[紐約市](../Page/紐約.md "wikilink")。他長期從事[大氣和](../Page/大氣.md "wikilink")[氣候方面的著述](../Page/氣候.md "wikilink")，於1870年開創基于电报系统的國家[氣象服務事業](../Page/氣象服務.md "wikilink")，並引進[標準時間制於](../Page/標準時間.md "wikilink")，1916年卒於[馬利蘭州](../Page/馬利蘭州.md "wikilink")[切維蔡斯](../Page/切維蔡斯.md "wikilink")。

## 生平

阿贝先后在[大卫·斯科特语法学校和](../Page/大卫·斯科特语法学校.md "wikilink")[纽约市立学院接受教育](../Page/纽约市立学院.md "wikilink")\[1\]，曾在[奥利佛·沃尔科特·吉布斯指导下学习](../Page/奥利佛·沃尔科特·吉布斯.md "wikilink").\[2\]。毕业之后他曾在纽约三一学院教授数学，之后在[密歇根大学执教](../Page/密歇根大学.md "wikilink")。美国内战爆发时，他想加入联邦军队，但由于视力不合格未通过。他进入哈佛就读，并担任天文学家本杰明·阿普索普·古尔德的助手\[3\]
。1864年他获得科学学士学位，也不再为美国海岸调查工作。这段时间里他认识了气象学家[威廉·费雷尔](../Page/威廉·费雷尔.md "wikilink")，这可能是他对气象研究兴趣的起源。\[4\]
1864-1866年，阿贝在俄国[普尔科沃天文台](../Page/普尔科沃天文台.md "wikilink")[奥托·威廉·冯·斯特鲁维领导下工作了两年](../Page/奥托·威廉·冯·斯特鲁维.md "wikilink")。回到美国后，他现在美国海军天文台工作，1868年起担任[辛辛那提天文台台长](../Page/辛辛那提天文台.md "wikilink")。\[5\]\[6\]在辛辛那提的几年里，他有感于天气会影响天文学家的工作，开始转向气象学研究</ref>\[7\]。

## 气象学研究

1871年阿贝被任命为气象服务处的首席气象学家。阿贝研究的前提是气象预测应该以尽量小的投入，力争能有收益。他招募了二十名义务气象调查员，又争取到[西联公司的许可](../Page/西联公司.md "wikilink")，使调查员们可以免费收发电报。他也规定了一套密码系统，以减少发送过程中的信息遗漏和损失。通过获取调查员收集的数据，阿贝把天气情况标在地图上，进而作出预测。1871年2月19日阿贝发表了第一份天气预报，之后他一方面扩大数据收集范围，一方面将数据发给各研究机构以扩大影响，还坚持对发布的预告进行复核，在第一年中他们复核了其中69%的预告。1872年阿贝创建了[每月天气评论这一期刊](../Page/每月天气评论.md "wikilink")\[8\]。

1873年，辛辛那提天文台财政紧张，阿贝离开了天文台，开始专门研究气象预报。为了保证各地数据的同时性，他在1879年发表文章《关于标准时间的报告》建议将美国划分为四个时区\[9\]。1884年美国政府正式启用时区系统。1891年该服务处升级为美国气象局，阿贝留任直至去世。

## 脚注

## 参考文献

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
[A](../Category/紐約市人.md "wikilink")
[A](../Category/美國氣象學家.md "wikilink")
[A](../Category/密西根大學校友.md "wikilink")
[Category:气象学史](../Category/气象学史.md "wikilink")

1.

2.

3.
4.
5.
6.

7.

8.
9.