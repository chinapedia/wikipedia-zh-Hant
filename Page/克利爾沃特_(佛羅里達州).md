[Pinellas_County_Florida_Incorporated_and_Unincorporated_areas_Clearwater_Highlighted.svg](https://zh.wikipedia.org/wiki/File:Pinellas_County_Florida_Incorporated_and_Unincorporated_areas_Clearwater_Highlighted.svg "fig:Pinellas_County_Florida_Incorporated_and_Unincorporated_areas_Clearwater_Highlighted.svg")
**清水市**（Clearwater,
Florida）是[美國](../Page/美國.md "wikilink")[佛羅里達州](../Page/佛羅里達州.md "wikilink")[皮尼拉斯縣的縣治](../Page/皮尼拉斯縣_\(佛羅里達州\).md "wikilink")，位於[佛羅里達半島西部](../Page/佛羅里達半島.md "wikilink")[皮尼拉斯半島上](../Page/皮尼拉斯半島.md "wikilink")。中為[沿海水道](../Page/沿海水道.md "wikilink")，西為[墨西哥灣](../Page/墨西哥灣.md "wikilink")。面積97.7平方公里，2006年人口107,742人。\[1\]

1891年設鎮，1915年5月27日建市。

## 姐妹城市

  - [卡拉馬里亞](../Page/卡拉馬里亞.md "wikilink")

  - [長野市](../Page/長野市.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[M](../Category/佛羅里達州城市.md "wikilink")

1.  [Clearwater, Florida - Population Finder - American
    FactFinder](http://factfinder.census.gov/servlet/SAFFPopulation?_event=Search&geo_id=16000US1214400&_geoContext=01000US%7C04000US12%7C16000US1214400&_street=&_county=Clearwater+city&_cityTown=Clearwater+city&_state=04000US12&_zip=&_lang=en&_sse=on&ActiveGeoDiv=geoSelect&_useEV=&pctxt=fph&pgsl=160&_submenuId=population_0&ds_name=null&_ci_nbr=null&qr_name=&reg=%3Anull&_keyword=&_industry=)