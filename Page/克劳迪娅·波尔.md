**克劳迪娅·玛丽亚·波尔·阿伦斯**（Claudia Maria Poll
Ahrens，），[哥斯达黎加著名](../Page/哥斯达黎加.md "wikilink")[游泳运动员](../Page/游泳.md "wikilink")。

波尔的父母是居住在[尼加拉瓜的](../Page/尼加拉瓜.md "wikilink")[德国移民](../Page/德国.md "wikilink")。1972年[马那瓜发生](../Page/马那瓜.md "wikilink")[地震后](../Page/地震.md "wikilink")，波尔一家移居哥斯达黎加。波尔一家均颇具运动天赋，其姐[西尔维娅·波尔是哥斯达黎加首位](../Page/西尔维娅·波尔.md "wikilink")[奥运会游泳奖牌得主](../Page/奥运会.md "wikilink")。

[1996年夏季奥林匹克运动会上](../Page/1996年夏季奥林匹克运动会.md "wikilink")，波尔出人意料地击败了德国名将[弗兰西斯卡·范·阿尔姆西克](../Page/弗兰西斯卡·范·阿尔姆西克.md "wikilink")，夺得200米[自由泳冠军](../Page/自由泳.md "wikilink")。次年，[游泳世界杂志评选其为年度最佳游泳女运动员](../Page/游泳世界.md "wikilink")。

[2000年夏季奥林匹克运动会上](../Page/2000年夏季奥林匹克运动会.md "wikilink")，波尔取得了两枚铜牌。但是，她在2002年被查出使用了[兴奋剂](../Page/兴奋剂.md "wikilink")，被处以四年禁赛。2003年，[国际泳联将其禁赛期限减为两年](../Page/国际泳联.md "wikilink")，使其得以参加[2004年夏季奥林匹克运动会](../Page/2004年夏季奥林匹克运动会.md "wikilink")。在该届奥运会上，波尔取得了400米自由泳第9名，未能进入最后决赛。

[Category:哥斯达黎加游泳运动员](../Category/哥斯达黎加游泳运动员.md "wikilink")
[Category:哥斯达黎加人](../Category/哥斯达黎加人.md "wikilink")
[Category:哥斯達黎加奧林匹克運動會金牌得主](../Category/哥斯達黎加奧林匹克運動會金牌得主.md "wikilink")
[Category:哥斯達黎加奧林匹克運動會銅牌得主](../Category/哥斯達黎加奧林匹克運動會銅牌得主.md "wikilink")
[Category:1996年夏季奧林匹克運動會獎牌得主](../Category/1996年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:2000年夏季奥林匹克运动会奖牌得主](../Category/2000年夏季奥林匹克运动会奖牌得主.md "wikilink")
[Category:1996年夏季奧林匹克運動會游泳運動員](../Category/1996年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:2000年夏季奧林匹克運動會游泳運動員](../Category/2000年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:2004年夏季奧林匹克運動會游泳運動員](../Category/2004年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:奥林匹克运动会游泳铜牌得主](../Category/奥林匹克运动会游泳铜牌得主.md "wikilink")
[Category:世界游泳錦標賽游泳賽事獎牌得主](../Category/世界游泳錦標賽游泳賽事獎牌得主.md "wikilink")
[Category:世界短道游泳錦標賽獎牌得主](../Category/世界短道游泳錦標賽獎牌得主.md "wikilink")
[Category:女子自由式游泳運動員](../Category/女子自由式游泳運動員.md "wikilink")