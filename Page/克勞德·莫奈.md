**奧斯卡-克勞德·莫內**（，；），[法國](../Page/法国.md "wikilink")[畫家](../Page/畫家.md "wikilink")，[印象派代表人物及创始人之一](../Page/印象派.md "wikilink")，「印象」一詞即是源自其名作《[印象·日出](../Page/印象·日出.md "wikilink")》，印象派的理論和實踐大都有其推廣。莫奈擅長光與影的實驗與表現技法，最重要的風格是改變了陰影和輪廓線的畫法，在其畫作中沒有非常明確的陰影，亦無突顯或平塗式的輪廓線。此外，他對於色彩的運用相當細膩，曾长期探索實驗色彩與光的完美表達，常在不同的时间和光线下，对同一对象作多幅描绘。

## 生平

### 早年生涯

莫奈1840年11月14日出生于法国[巴黎](../Page/巴黎.md "wikilink")[第九區拉斐特街](../Page/巴黎第九區.md "wikilink")45號（45
rue Laffitte），\[1\]
是阿道夫和路易斯的次子，雙親均為第二代巴黎人。1841年5月20日，他在当地的[巴黎圣母院教区教堂受洗](../Page/巴黎圣母院教区教堂.md "wikilink")，取名為奧斯卡-克勞德（Oscar-Claude），父母一般只叫他奧斯卡\[2\]\[3\]。雖然[受洗為](../Page/受洗.md "wikilink")[天主教徒](../Page/天主教徒.md "wikilink")，但後來莫奈成為了[無神論者](../Page/無神論者.md "wikilink")。\[4\]\[5\]

1845年全家搬到[诺曼底的](../Page/诺曼底.md "wikilink")[勒阿弗爾后](../Page/勒阿弗爾.md "wikilink")，其父想要他继承家里的船具和杂货业务\[6\]，但莫奈却想做一个艺术家。他作为歌手的母亲决定支持儿子的想法\[7\]。

1851年4月1日莫奈进入阿勒尔弗的一家中学学习艺术，学业之余以一幅10-20法郎的价格售卖自己的炭笔画。他的绘画启蒙老师是[雅克-路易·大卫的学生](../Page/雅克-路易·大卫.md "wikilink")[雅克-弗朗索瓦·奧沙尔](../Page/雅克-弗朗索瓦·奧查德.md "wikilink")（Jacques-François
Ochard）。莫奈于1856年前后在诺曼底的沙滩上结识艺术家[欧仁·布丹](../Page/欧仁·布丹.md "wikilink")，后来他成為了莫奈的良师益友，並教授莫奈繪畫油画及[室外寫生的技法](../Page/室外寫生.md "wikilink")。\[8\]
1857年1月28日，莫奈的母亲去世，他因此退學，與丧偶、无子女的姨妈玛丽－让娜·莱卡德尔（Marie-Jeanne Lecadre）夫人同住。

### 巴黎

[Claude_Monet_-_Camille.JPG](https://zh.wikipedia.org/wiki/File:Claude_Monet_-_Camille.JPG "fig:Claude_Monet_-_Camille.JPG")》，畫中是他的第一任妻子，1866年（藏）\]\]
当莫奈来到[巴黎](../Page/巴黎.md "wikilink")[卢浮宫時](../Page/卢浮宫.md "wikilink")，他亲眼目睹许多画家在模仿著名艺术家的作品，於是他便携带工具和颜料，坐在一扇窗户旁开始画他所看見的景物。\[9\]
他在法国居住期間常於其他年轻画家会面，當中他與後來同為印象派画家的[愛德華·馬奈成為了其好友](../Page/愛德華·馬奈.md "wikilink")。

1860年至1862年期間，莫奈在[阿尔及利亚服役两年](../Page/阿尔及利亚.md "wikilink")，在七年的合同到期前，他以[伤寒為由在列卡德夫人請求下](../Page/伤寒.md "wikilink")（可能受[荷兰画家](../Page/荷兰.md "wikilink")的幫助）脱離部隊，並前往完成大学的艺术课程。

1862年，莫奈在巴黎加入了[夏尔·格莱尔的画室](../Page/夏尔·格莱尔.md "wikilink")。他在那里结认了[皮耶-奧古斯特·雷諾瓦](../Page/皮耶-奧古斯特·雷諾瓦.md "wikilink")、[弗雷德里克·巴齐耶及](../Page/弗雷德里克·巴齐耶.md "wikilink")[阿爾弗雷德·西斯萊等](../Page/阿尔弗雷德·西斯莱.md "wikilink")，並共同创造了一种在户外和自然光线下用浓厚的油彩作画的新艺术手法，后来被称为[印象派](../Page/印象派.md "wikilink")。

1866年，他以[卡米耶·东西厄为模特兒创作了](../Page/卡米耶·东西厄.md "wikilink")《綠衣女子》，使他受到承认；卡米爾後來也成為了他许多作品中的模特。不久之后，卡米爾怀孕，并在1867年誕下長子。

### 普法战争，「印象派」，阿让特伊

[Claude_Monet,_Impression,_soleil_levant.jpg](https://zh.wikipedia.org/wiki/File:Claude_Monet,_Impression,_soleil_levant.jpg "fig:Claude_Monet,_Impression,_soleil_levant.jpg")\]\]
[Monet_Snow_at_Argenteuil_1875.jpg](https://zh.wikipedia.org/wiki/File:Monet_Snow_at_Argenteuil_1875.jpg "fig:Monet_Snow_at_Argenteuil_1875.jpg")\]\]
1870年7月28日，莫奈与卡米爾结婚。同年9月[普法战争期間](../Page/普法战争.md "wikilink")，莫奈到[英国](../Page/大不列颠及爱尔兰联合王国.md "wikilink")[伦敦避难](../Page/伦敦.md "wikilink")，並在該地鑽研[约翰·康斯太布尔和](../Page/约翰·康斯特勃.md "wikilink")[J·M·W·透纳的作品](../Page/J·M·W·透纳.md "wikilink")，激发了他对色彩研究方面的创新。1871年春，英國[皇家艺术学院拒绝将莫奈的作品列入展览](../Page/皇家艺术学院.md "wikilink")。

1871年，他离开[伦敦](../Page/伦敦.md "wikilink")，來到荷兰[赞丹](../Page/赞丹.md "wikilink")，並在那里创作了25幅作品，更使警察怀疑他從事革命活动；他还首次游览了附近的[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")。1871年的10月或11月，他與其妻回到法国，定居於位塞纳河右岸的[阿让特伊至](../Page/阿让特伊.md "wikilink")1878年，曾在1874年時短暂地重返荷兰。在此时期，莫奈创作了大量作品。

1872年，莫奈以[勒阿弗尔的风光为背景创作了](../Page/勒阿弗尔.md "wikilink")《[印象·日出](../Page/印象·日出.md "wikilink")》，在1874年一次画展上首次亮相（如今陈列在[巴黎](../Page/巴黎.md "wikilink")[瑪摩丹美術館](../Page/瑪摩丹美術館.md "wikilink")），卻被艺术评论家以「『印象』派」揶揄他的「離經背道」。

1876年，莫奈夫人染上[肺结核](../Page/肺结核.md "wikilink")。1878年3月17日，次子米歇尔誕生。1879年，莫奈夫人因[子宮癌病逝](../Page/子宮頸癌.md "wikilink")，临终时莫奈为她繪下最后一幅画（《卡米爾安躺病榻上》）。

### 后来的日子

卡米尔過世后，莫奈伤心欲绝，为脫離贫困而创作了數幅名作。
1880年代初，他绘制了几组以风景及海景為背景的作品，以作他在法国乡村生活的记录，後來成為了其系列画作。

1878年，莫奈一家暂居於其好友，百货商店大亨及艺术赞助人在[弗特伊的房子中](../Page/弗特伊.md "wikilink")；兩個家庭在整個夏天共住一室，而莫奈與其妻夫人漸生情愫。同年稍後，奧施地因其[破产而逃至](../Page/破產.md "wikilink")[比利时避債](../Page/比利时.md "wikilink")。1879年，卡米爾逝世后，莫奈依然居於該處，而愛麗絲决定帮助莫奈抚养他的两个孩子，並在其後把他們和自己的六个孩子一同带往巴黎。1880年春，他们从巴黎回来；1881年，莫奈家與奧施地家一同遷往[普瓦西](../Page/普瓦西.md "wikilink")，但莫奈不喜欢那里。1883年4月，在列車上一睹其風光後，他们搬到[上诺曼底](../Page/上诺曼底.md "wikilink")[大区](../Page/大区_\(法国\).md "wikilink")[厄尔省的](../Page/厄尔省.md "wikilink")[吉维尼](../Page/吉维尼.md "wikilink")。他种植了一个大花园，并在那里完成他余生的藝術创作。1891年3月，貧困潦倒、與其妻分居已久的奧施地亡於[痛風](../Page/痛风.md "wikilink")。在出資為他舉殯後，莫奈和愛麗絲於次年[结婚](../Page/繼室.md "wikilink")。

### 在吉维尼

1883年5月初，莫奈一家从吉維尼一位田主手中租下一所房子；这所房子座落在连接[韦尔农與](../Page/韦尔农_\(厄尔省\).md "wikilink")[加斯尼的大路上](../Page/加斯尼.md "wikilink")，有間谷仓被用作画室，同時亦為果园及小花园。房子离當地的学校很近，周围的景观给莫奈的作品提供大量灵感。在一家在此劳作之際，莫奈的作品也被经销商[保罗·杜兰德-鲁埃尔越卖越多](../Page/Paul_Durand-Ruel.md "wikilink")，從而改变了他的命运。及至1890年11月時，莫奈已能买得起他的房子與周边建筑了。1890年代，莫奈興修了创建了一所包含天窗的宽敞建筑，作為一所温室和第二个工作室。

在19世纪80年代至90年代，莫奈开始了系列绘画创作，即在不同的光线和角度下连续画同一个物体，如他的系列作品《[鲁昂主教座堂](../Page/魯昂大教堂_\(莫奈\).md "wikilink")》是在不同的角度和一天中不同的时间来画。1895年，他从20个不同角度对大教堂所繪畫的画在迪朗德—吕埃尔（Durand-Ruel）画廊展出。此外，他还有《[乾草堆](../Page/乾草堆_\(莫奈\).md "wikilink")》、《[白楊樹](../Page/白楊樹_\(莫奈\).md "wikilink")》、《[國會大廈](../Page/國會大廈_\(莫奈\).md "wikilink")》、《[睡蓮](../Page/睡莲_\(莫奈\).md "wikilink")》等系列作。

莫奈喜欢绘制受控状态下的自然，尤其是他在吉维尼的院子及里面的[睡莲與](../Page/睡蓮.md "wikilink")[桥](../Page/橋.md "wikilink")。他还繪画了不少以塞纳河為主題的的画作（如《[塞纳河上的冰破裂了](../Page/塞纳河上的冰破裂了.md "wikilink")》）。他每天给园丁写的指示中包含了精确的种植布局，以及要购买花卉和[植物学书籍](../Page/植物学.md "wikilink")。伴随着财富增长的是其花园的進步，甚至在他已雇用7名园丁时，他仍繼續身體力行建設花園。

1883年至1908年间，莫奈在[地中海画了许多风景画和海景画](../Page/地中海.md "wikilink")。在[意大利](../Page/意大利王國_\(1861年–1946年\).md "wikilink")[威尼斯](../Page/威尼斯.md "wikilink")，他创作了一系列重要画作；而在伦敦，他绘制了两个重要的系列——《国会大厦》系列和《[查林十字桥](../Page/查令十字橋_\(莫奈\).md "wikilink")》系列。1911年，繼室愛麗絲逝世；而莫奈所鍾愛的長子讓，其後亦在1914年去世。妻子逝世后，心力交瘁的莫奈由繼女、讓的妻子而且同為畫家的悉心照顧；这段时间，莫奈身上漸漸出现了[白内障的初步病徵](../Page/白内障.md "wikilink")。

[第一次世界大战期间](../Page/第一次世界大战.md "wikilink")，他次子米歇尔参军，而法国則由他的朋友及仰慕者[乔治·克列孟梭领导](../Page/乔治·克列孟梭.md "wikilink")。莫奈绘制了一系列[垂柳树以表达对法国阵亡将士的敬意](../Page/垂柳.md "wikilink")。1923年，由於在白内障影响下他的画作整体偏红，莫奈決定接受两次[白内障手术](../Page/白内障手术.md "wikilink")，以减小白内障症状對其視力的影响。也許如此，他在手术過后可以看見某些常人難以看见的[紫外线](../Page/紫外线.md "wikilink")，影响他观察到的颜色。在手术后，他甚至重新绘制了部分作品，當中的睡莲更蓝了。

### 逝世

1926年12月5日，莫內因[肺癌不幸病逝](../Page/肺癌.md "wikilink")，享壽八十六岁，安葬於吉维尼教堂的墓地。莫內坚持葬礼仪式要简单，因此只有大约50人出席。\[10\]

他的家、花园和睡莲由唯一继承人次子米歇尔继承，并于1966年捐赠给[法国美术学院](../Page/法国美术学院.md "wikilink")。通过莫奈基金会，[他的房子和花园在](../Page/莫奈花园.md "wikilink")1980年复原后开放参观。\[11\]
除了莫奈的纪念品和他一生中的其他事物，房子中还包含了他收集的[日本](../Page/日本.md "wikilink")[木刻版画](../Page/木刻版畫.md "wikilink")，這房子也成為吉维尼的主要景点之一。

## 作品的销售

[Monet-ArgenteuilBasinWithASingleSailboat.jpg](https://zh.wikipedia.org/wiki/File:Monet-ArgenteuilBasinWithASingleSailboat.jpg "fig:Monet-ArgenteuilBasinWithASingleSailboat.jpg")
2004年，莫奈的*the Parliament*和*Effects of Sun in the
Fog*在伦敦卖出了超过2000万美元。2006年，英国[皇家学报发表文章指出这两幅画作是在](../Page/皇家学报.md "wikilink")[泰晤士河上的](../Page/泰晤士河.md "wikilink")[圣托马斯医院创作的](../Page/圣托马斯医院.md "wikilink")。

他的作品《迪耶普附近的悬崖》被盗两次。一次是在1998年，博物馆的馆长被裁定与两名同伙一起盗窃，因此被判入狱五年零两个月；另一次是在2007年8月，并于2008年找回。他在1873年创作的作品[Le
Pont du chemin de fer à
Argenteuil](../Page/Le_Pont_du_chemin_de_fer_à_Argenteuil.md "wikilink")，描绘了巴黎附近塞纳河上的一座铁路桥。这幅作品2008年5月6日被一个匿名电话竞标者在纽约[佳士得以](../Page/佳士得.md "wikilink")4140万美元竞得。在此之前，他的单幅作品售价的最高记录是3650万美元。仅仅几周之后的2008年6月24日，睡莲系列中的[Le
bassin aux
nymphéas在伦敦佳士得拍出](../Page/Le_bassin_aux_nymphéas.md "wikilink")。落锤价是36,500,000英镑（71,892,376.34美元），算上竞拍费用高达40,921,250英镑（80,451,178美元），是当时最贵的20幅画作之一。

2012年6月29日上午11點，一名叫安德魯·香農（Andrew
Shannon）的男子把[愛爾蘭國家美術館展出的莫奈作品](../Page/愛爾蘭國家美術館.md "wikilink")《阿讓特伊流域的孤舟》一拳打破，估計這幅畫時值一千萬美元\[12\]。該作品在修復後于2014年重新展出，而破壞者則被判五年有期徒刑\[13\]。

## 代表作品

莫奈一生遺留500件素描，2000多幅油畫及2700封信件。足跡由巴黎大街到地中海岸，從法國到倫敦、威尼斯、挪威，在各地旅行寫生，留下無數的作品。

<File:Claude> Monet, Impression, soleil levant,
1872.jpg|《[印象·日出](../Page/印象·日出.md "wikilink")》，1872年，收藏於[巴黎](../Page/巴黎.md "wikilink")[瑪摩丹美術館](../Page/瑪摩丹美術館.md "wikilink")
<File:Monet-montorgueil.JPG>|《[聖德尼街的節日](../Page/聖德尼街的節日.md "wikilink")》(La
Rue Montorgueil à Paris, fête du 30 juin
1878)，1878年，收藏於[巴黎](../Page/巴黎.md "wikilink")[奧賽美術館](../Page/奧賽美術館.md "wikilink")
<File:Claude> Monet -
Argenteuil.jpg|《[紅帆船](../Page/紅帆船.md "wikilink")》(Les
bateaux rouges,
Argenteuil)，1875年，收藏於[巴黎](../Page/巴黎.md "wikilink")[橘園美術館](../Page/橘園美術館.md "wikilink")
<File:Claude> Monet La
Grenouillére.jpg|《[蛙塘](../Page/蛙塘.md "wikilink")》(La
Grenouillère)，1869年，收藏於[大都會美術館](../Page/大都會美術館.md "wikilink")
<File:Bridge> Over a Pond of Water Lilies, Claude Monet
1899.jpg|《[睡蓮池](../Page/睡蓮池.md "wikilink")》(Le Bassin aux
nymphéas)，1899年，收藏於[奧塞美術館](../Page/奧塞美術館.md "wikilink") <File:Claude>
Monet - Le pont de chemin der fer à
Argenteuil.jpg|《[阿讓特伊之橋](../Page/阿讓特伊之橋.md "wikilink")》(Le
Pont de chemin de fer à Argenteuil)，1873年 <File:Claude> Monet - Woman
with a Parasol - Madame Monet and Her Son - Google Art
Project.jpg|《[打阳伞的女人](../Page/打阳伞的女人.md "wikilink")》(Le
promenade)1875年，收藏于[华盛顿国家美术馆](../Page/华盛顿国家美术馆.md "wikilink")
<File:Claude> Monet - The Houses of Parliament,
Sunset.jpg|《[國會大廈_(莫奈)](../Page/國會大廈_\(莫奈\).md "wikilink")》（Série
des Parlements de Londres）1900-1905年

## 参考文献

## 外部链接

  - [National
    Gallery](https://web.archive.org/web/20080501062942/http://www.nationalgallery.org.uk/cgi-bin/WebObjects.dll/CollectionPublisher.woa/wa/artistBiography?artistID=497)
    英国伦敦国家艺术馆收藏
  - [Musée de l’Orangerie](http://www.musee-orangerie.fr/) 法国巴黎橘园博物馆

[M](../Category/印象派畫家.md "wikilink") [M](../Category/法国画家.md "wikilink")
[克洛德·莫奈](../Category/克洛德·莫奈.md "wikilink")
[Category:罹患肺癌逝世者](../Category/罹患肺癌逝世者.md "wikilink")
[Category:法國無神論者](../Category/法國無神論者.md "wikilink")
[Category:葬于法国](../Category/葬于法国.md "wikilink")
[Category:白内障患者](../Category/白内障患者.md "wikilink")

1.  P. Tucker *Claude Monet: Life and Art*, p. 5

2.
3.

4.

5.

6.

7.

8.  [Biography for Claude
    Monet](http://www.guggenheimcollection.org/site/artist_bio_165.html)
     Guggenheim Collection. Retrieved 6 January 2007.

9.  [Gary Tinterow, *Origins of Impressionism*, Metropolitan Museum of
    Art, Jan 1, 1994, ISBN 978-0-87099-717-4,
    9780870997174](http://books.google.es/books?id=kLEpf5a49V0C&pg=PA431&dq=monet+louvre+instead+window+and+paint&hl=en&sa=X&ei=SQ23UpX0NcKL0AX18ICQDg&redir_esc=y#v=onepage&q=monet%20louvre%20instead%20window%20and%20paint&f=false)

10. P. Tucker *Claude Monet: Life and Art*, p.224

11.

12.

13.