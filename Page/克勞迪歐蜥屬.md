**克勞迪歐蜥屬**（屬名：*Claudiosaurus*）是種已滅絕的[雙孔類](../Page/雙孔類.md "wikilink")[爬行動物](../Page/爬行動物.md "wikilink")，生存於[二疊紀晚期的](../Page/二疊紀.md "wikilink")[馬達加斯加](../Page/馬達加斯加.md "wikilink")。
[ClaudiosaurusGermaini-RedpathMuseumMontreal-June6-08.png](https://zh.wikipedia.org/wiki/File:ClaudiosaurusGermaini-RedpathMuseumMontreal-June6-08.png "fig:ClaudiosaurusGermaini-RedpathMuseumMontreal-June6-08.png")
克勞迪歐蜥是最早的[新雙弓類物種之一](../Page/新雙弓類.md "wikilink")，新雙弓類的範圍是大部分的[雙孔亞綱](../Page/雙孔亞綱.md "wikilink")，不包含原始的[纖肢龍目](../Page/纖肢龍目.md "wikilink")。克勞迪歐蜥的身長約60公分，有相當長的身體與頸部。克勞迪歐蜥被推斷是部分海生動物，方活方式類似現代的[海鬣蜥](../Page/海鬣蜥.md "wikilink")。這個理論的主要原因是克勞迪歐蜥身體有大量[軟骨](../Page/軟骨.md "wikilink")，並缺乏堅硬的骨頭，如果牠們生存在陸地上，會在支撐重量上產生問題。牠們的[胸骨發展不良好](../Page/胸骨.md "wikilink")，也妨礙牠們生存於陸地上。牠們可能將四肢緊靠身體，擺動身體與尾巴，以在水中推動前進\[1\]。

## 參考資料

[Category:雙孔亞綱](../Category/雙孔亞綱.md "wikilink")
[Category:二疊紀爬行動物](../Category/二疊紀爬行動物.md "wikilink")
[Category:海生爬行動物](../Category/海生爬行動物.md "wikilink")

1.