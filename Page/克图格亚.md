**克图格亚**（）是美国小说家所创造的[克苏鲁神话中的一个邪恶存在](../Page/克苏鲁神话.md "wikilink")，最早出现在其1944年的短篇小说《黑暗住民》（The
Dweller in Darkness）中。\[1\]

克图格亚的别名又叫“爆燃者（The Burning
One）”或“居于火焰者”，在[奥古斯特·威廉·德雷斯为](../Page/:en:August_Derleth.md "wikilink")[克苏鲁神话构建的体系中](../Page/克苏鲁神话.md "wikilink")，克图格亚是[旧日支配者之一](../Page/旧日支配者.md "wikilink")\[2\]
，象征“火”的存在，是象征“地”的存在的那些旧日支配者和异界诸神们、尤其是[奈亚拉托提普的死敌](../Page/奈亚拉托提普.md "wikilink")。其形象为一巨大、高热的火球或电浆块。[炎之精](../Page/炎之精.md "wikilink")（Fire
vampires）是它的眷族。它至少有一个为人所知的后裔，就是同为旧日支配者的[亚弗姆·扎](../Page/亚弗姆·扎.md "wikilink")。

克图格亚的本体被[旧神禁闭在恒星](../Page/旧神.md "wikilink")[北落师门](../Page/北落师门.md "wikilink")（Fomalhaut）内部\[3\]，這個星體曾在[洛夫克拉夫特的作品中出現](../Page/洛夫克拉夫特.md "wikilink")。\[4\]当夜晚北落师门星升到树梢的时候，连续呼喊三遍咒语“Ph'nglui
mgfw'nafh Cthugha Fomalhaut n'gha-ghaa naf'l thagn\! Ia\!
Cthugha\!”就可以召唤克图格亚在这个世界上现身，这对召唤者而言是极其危险的，因为克图格亚出现的地方会立即发生强烈爆炸，化作一片火海；在[奥古斯特·威廉·德雷斯的短篇小说](../Page/奥古斯特·威廉·德雷斯.md "wikilink")《黑暗住民》（The
Dweller in
Darkness）中，主角就是用这种方法召唤克图格亚以对抗[奈亚拉托提普的](../Page/奈亚拉托提普.md "wikilink")[化身](../Page/化身.md "wikilink")（Avatar），并将[奈亚拉托提普所藏身的](../Page/奈亚拉托提普.md "wikilink")[恩盖伊森林完全炸平](../Page/恩盖伊森林.md "wikilink")。不过，那次他们召唤出来的也可能是[尤玛恩托](../Page/尤玛恩托.md "wikilink")（Yomagn'tho）。\[5\]

## 參考文獻

[C](../Category/克苏鲁神话.md "wikilink") [C](../Category/文学角色.md "wikilink")

1.

2.

3.

4.

5.