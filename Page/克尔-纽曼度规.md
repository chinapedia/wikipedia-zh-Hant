**克尔-纽曼度规**（），简称度规，是描述匀角速度旋转的带[點電荷球体的引力场的](../Page/點電荷.md "wikilink")[度规](../Page/度规.md "wikilink")，其数学表示是：

\[c^{2} d\tau^{2} =
-\left(\frac{dr^2}{\Delta} + d\theta^2 \right) \rho^2 + \left(c \, dt - a \sin^2 \theta \, d\phi \right)^2 \frac{\Delta}{\rho^2} - \left(\left(r^2 + a^2 \right) d\phi - a c\, dt \right)^2 \frac{\sin^2 \theta}{\rho^2}\]

座標(r, θ, ϕ)是[球座標系](../Page/球座標系.md "wikilink")，Q是电荷，且:

\[a = \frac{J}{Mc}\,,\]

\[\ \rho^{2}=r^2+a^2\cos^2\theta\,,\]

\[\ \Delta=r^2-r_sr+a^2+r_Q^2\,,\]

在這裡 *J* 表示黑洞的[角動量](../Page/角動量.md "wikilink")， *r<sub>s</sub>*
是具有質量物體的[史瓦西半徑](../Page/史瓦西半徑.md "wikilink")，其與[質量](../Page/質量.md "wikilink")
*M* 的關係是:

\[r_{s} = \frac{2GM}{c^{2}}\]

其中*G*是[重力常數](../Page/重力常數.md "wikilink"),且 *r*<sub>*Q*</sub>
與[電荷](../Page/電荷.md "wikilink") *Q* 的關係是:

\[r_{Q}^{2} = \frac{Q^{2}G}{4\pi\epsilon_{0} c^{4}}\]

而 1/4π*ε*<sub>0</sub> 為[庫侖常數](../Page/庫侖常數.md "wikilink")。

  - 当\(Q=0\)时，克尔-纽曼度规退化为[克尔度规](../Page/克尔度规.md "wikilink")，所以克尔-纽曼度规是有电荷情况下的克尔度规。
  - 当\(a=0\)时，克尔-纽曼度规退化为[雷斯勒-诺斯特朗姆度规](../Page/雷斯勒-诺斯特朗姆度规.md "wikilink")。
  - 当\(Q=0,\quad a=0\)时，克尔-纽曼度规退化为[史瓦西度规](../Page/史瓦西度规.md "wikilink")。

[K](../Category/廣義相對論的精確解.md "wikilink")
[Category:度规张量](../Category/度规张量.md "wikilink")
[Category:黑洞](../Category/黑洞.md "wikilink")
[Category:广义相对论](../Category/广义相对论.md "wikilink")