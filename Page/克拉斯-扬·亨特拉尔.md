**克拉斯-扬·亨特拉尔**（，）是一位[荷蘭足球運動員](../Page/荷蘭.md "wikilink")，司職[前鋒](../Page/前鋒_\(足球\).md "wikilink")，現時效力[荷甲球会](../Page/荷兰足球甲级联赛.md "wikilink")[阿贾克斯](../Page/阿贾克斯足球俱乐部.md "wikilink")。

## 球會生涯

亨特拉爾原本是效力荷蘭一些小型球會，後來輾轉轉到大球會[PSV燕豪芬的青年隊](../Page/PSV燕豪芬.md "wikilink")。在燕豪芬時代，亨特拉爾已被認為是天才球員，但大部分時間都外借予乙組球會AGOVV阿普多倫(AGOVV
Apeldoorn)。他在2003-04年球季成為荷蘭乙組神射手。由於他後來與燕豪芬不和，結果2004年7月1日轉會到[海伦芬](../Page/海伦芬.md "wikilink")(SC
Heerenveen)，上陣45場聯賽入33球。

2006年1月他轉投阿積士。他在2005-06年裡的荷甲、荷蘭杯、歐洲足協杯和荷蘭青年隊的賽事中，共射入了54球。令他成為五十年裡，第二個荷蘭足球員在一季內射入最多的球員。他為阿積士很重要的球員，並受到一些海外球會的招攬，例如皇家馬德里、曼聯、祖雲達斯等。

他於2009年1月以2700萬[歐元由阿積士轉投](../Page/歐元.md "wikilink")[西甲班霸](../Page/西甲.md "wikilink")[皇家马德里](../Page/皇家马德里足球俱乐部.md "wikilink")，表現不俗。但在2009年夏天皇家马德里重组“银河舰队”后亨特拉尔失去了球队中的位置。最後於8月5日以1500萬[歐元身价转投意甲](../Page/歐元.md "wikilink")[AC米兰](../Page/AC米兰.md "wikilink")。不过在整个赛季里荷兰人的表现被广泛质疑，出场25次只有7球入账。

2010年8月31日，德甲强隊[沙尔克04宣布簽下亨特拉爾](../Page/沙尔克04.md "wikilink")，轉會費約為1300万[欧元](../Page/欧元.md "wikilink")。
亨特拉爾以29球獲得2011-2012球季德甲神射手，不僅成為德甲30年來最強的神射手，也成為德甲歷史上單季入球最多的外援。

## 榮譽

  - 阿積士

<!-- end list -->

  - [荷蘭盃](../Page/荷蘭盃.md "wikilink") 冠軍：2006年、2007年
  - [告魯夫盾](../Page/告魯夫盾.md "wikilink") 冠軍：2006年、2007年

<!-- end list -->

  - 史浩克零四

<!-- end list -->

  - [德國足協盃冠軍](../Page/德國足協盃.md "wikilink")：2011年；

<!-- end list -->

  - 荷蘭國家隊

<!-- end list -->

  - [歐洲U-21足球錦標賽](../Page/歐洲U-21足球錦標賽.md "wikilink") 冠軍：2006年

<!-- end list -->

  - 個人

<!-- end list -->

  - 荷蘭乙組最佳球員：2003/04年
  - 荷蘭乙組神射手：2003/04年
  - 荷蘭甲組最具潛質球員：2005/06年
  - 荷蘭甲組最佳球員：2005/06年
  - 荷蘭甲組神射手：2005/06年、2007/08年
  - [歐洲U-21足球錦標賽最佳球員](../Page/歐洲U-21足球錦標賽.md "wikilink")：2006年
  - [歐洲U-21足球錦標賽神射手](../Page/歐洲U-21足球錦標賽.md "wikilink")：2006年
  - 德國甲組聯賽神射手：2011/2012 (29球)

## 参考資料

## 外部連結

  - [史浩克04檔案
    亨特拉爾檔案](https://web.archive.org/web/20100921004847/http://www.schalke04.de/teams/bundesligakader.html?player=116&cHash=b6d5fef108)

[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")
[Category:2012年歐洲國家盃球員](../Category/2012年歐洲國家盃球員.md "wikilink")
[Category:2014年世界盃足球賽球員](../Category/2014年世界盃足球賽球員.md "wikilink")
[Category:荷蘭國家足球隊球員](../Category/荷蘭國家足球隊球員.md "wikilink")
[Category:荷兰足球运动员](../Category/荷兰足球运动员.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:荷甲球員](../Category/荷甲球員.md "wikilink")
[Category:PSV燕豪芬球員](../Category/PSV燕豪芬球員.md "wikilink")
[Category:迪加史卓普球員](../Category/迪加史卓普球員.md "wikilink")
[Category:阿積士球員](../Category/阿積士球員.md "wikilink")
[Category:皇家馬德里球員](../Category/皇家馬德里球員.md "wikilink")
[Category:AC米蘭球員](../Category/AC米蘭球員.md "wikilink")
[Category:史浩克零四球員](../Category/史浩克零四球員.md "wikilink")
[Category:西班牙外籍足球運動員](../Category/西班牙外籍足球運動員.md "wikilink")
[Category:義大利外籍足球運動員](../Category/義大利外籍足球運動員.md "wikilink")
[Category:德國外籍足球運動員](../Category/德國外籍足球運動員.md "wikilink")
[Category:荷蘭旅外足球運動員](../Category/荷蘭旅外足球運動員.md "wikilink")
[Category:在德國的荷蘭人](../Category/在德國的荷蘭人.md "wikilink")
[Category:在義大利的荷蘭人](../Category/在義大利的荷蘭人.md "wikilink")
[Category:在西班牙的荷蘭人](../Category/在西班牙的荷蘭人.md "wikilink")
[Category:海爾德蘭省人](../Category/海爾德蘭省人.md "wikilink")