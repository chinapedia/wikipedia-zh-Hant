<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><font size="+1"><strong>克拉約瓦<br />
</strong></font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Craiova_in_Romania.png" title="fig:Craiova_in_Romania.png">Craiova_in_Romania.png</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>基本资料</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>國家</strong>：</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>地區</strong>：</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>面積</strong>：</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>人口</strong>：</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>地理位置</strong>：</p></td>
</tr>
</tbody>
</table>

[Parcul_Nicolae_Romanescu01.JPG](https://zh.wikipedia.org/wiki/File:Parcul_Nicolae_Romanescu01.JPG "fig:Parcul_Nicolae_Romanescu01.JPG")
**克拉約瓦**（[羅馬尼亞語](../Page/羅馬尼亞語.md "wikilink")：****）為[羅馬尼亞](../Page/羅馬尼亞.md "wikilink")[多爾日縣的首府](../Page/多爾日縣.md "wikilink")，位於[奧爾特尼亞中部的](../Page/奧爾特尼亞.md "wikilink")[日烏河西岸](../Page/日烏河.md "wikilink")，[東歐](../Page/東歐.md "wikilink")[喀爾巴阡山脈的北方及](../Page/喀爾巴阡山脈.md "wikilink")[多瑙河的南部](../Page/多瑙河.md "wikilink")。而克拉約瓦也是[布加勒斯特西邊的主要](../Page/布加勒斯特.md "wikilink")[商業城市](../Page/商業.md "wikilink")。

克拉約瓦是在古代[達基亞人城市的廢墟上所建立](../Page/達基亞.md "wikilink")，長久以來，它都受到當地的[總督所統治](../Page/總督.md "wikilink")。可是，在1395年，這個地方就是歐洲國家第一次擊退[奧斯曼帝國](../Page/奧斯曼帝國.md "wikilink")[蘇丹](../Page/苏丹_\(称谓\).md "wikilink")[巴耶塞特一世進擊的地方](../Page/巴耶塞特一世.md "wikilink")。而這個地方最早在16世紀被稱作[城市](../Page/城市.md "wikilink")，並開始發展經濟。可是，在1718至1737年間，[奧地利哈布斯堡王室統治此地時](../Page/奧地利哈布斯堡王朝.md "wikilink")，它的經濟開始走下坡。

在1877年至1878年的[俄土戰爭期間](../Page/俄土戰爭_\(1877年-1878年\).md "wikilink")，這個城市因為兩大國的戰爭而從中獲利，經濟及文化急速發展，為今天的發展打下基礎。後來，在羅馬尼亞加入[軸心國以前](../Page/軸心國.md "wikilink")，這個城市更開始[工業化的發展](../Page/工業化.md "wikilink")。

1960年以後，隨著東歐共產政權開始漫延，羅馬尼亞也成為了[共產政權](../Page/罗马尼亚人民共和国.md "wikilink")。在[共產主義政權統治下](../Page/共產主義.md "wikilink")，這個城市開始轉向發展[汽車](../Page/汽車.md "wikilink")、[引擎工業](../Page/引擎.md "wikilink")、以及[航天工業](../Page/航天工業.md "wikilink")，[化學工業](../Page/化學工業.md "wikilink")、[食品工業](../Page/食品工業.md "wikilink")、[工程](../Page/工程.md "wikilink")、[電機工程](../Page/電機工程.md "wikilink")、[採礦業及](../Page/採礦業.md "wikilink")[電力工業等](../Page/電力工業.md "wikilink")。自1989年[東歐革命以來](../Page/1989年羅馬尼亞革命.md "wikilink")，這個城市開始走向產業私有化的道路。直至今天，它成為了[布加勒斯特附近一個最重要的商業城市](../Page/布加勒斯特.md "wikilink")。

## 人口

2002年，克拉約瓦的總人口為302,601人。當地的種族如下（2002年為準）：

  - [羅馬尼亞人](../Page/羅馬尼亞.md "wikilink")：292,487人(96.66%)
  - [罗姆人](../Page/罗姆人.md "wikilink")：8,820人(2.91%)
  - [匈牙利人](../Page/匈牙利.md "wikilink")：218人(0.07%)
  - [希臘人](../Page/希臘.md "wikilink")：188人(0.06%)
  - [義大利人](../Page/義大利.md "wikilink")：178人(0.06%)
  - [德國人](../Page/德國.md "wikilink")：173人(0.06%)
  - [塞爾維亞人](../Page/塞爾維亞.md "wikilink")：34人(0.01%)
  - [烏克蘭人](../Page/烏克蘭.md "wikilink")：32人(0.01%)
  - 其他：471人(0.16%)

## 經濟

自1989年[東歐革命以來](../Page/1989年羅馬尼亞革命.md "wikilink")，這個城市主要發展了[電訊服務](../Page/電訊.md "wikilink")、[銀行業](../Page/銀行業.md "wikilink")、[保險業及](../Page/保險業.md "wikilink")[管理諮詢服務等](../Page/管理諮詢.md "wikilink")。這個城市亦與其他國家如[義大利](../Page/義大利.md "wikilink")、[比利時](../Page/比利時.md "wikilink")、[奧地利](../Page/奧地利.md "wikilink")、[德國](../Page/德國.md "wikilink")、[瑞士](../Page/瑞士.md "wikilink")、[希臘](../Page/希臘.md "wikilink")、[以色列等有貿易往來](../Page/以色列.md "wikilink")。

而這個城市的工作人口大約為110,000人(2002年為準)，當中38%的人任職工業範疇，另外15%受僱於貿易和修理服務，10%從事運輸及庫存，8%工作於教育方面，而醫學的領域中工作的人則佔5.7%。

## 姊妹城市

  - [库奧皮奧](../Page/库奧皮奧.md "wikilink")

  - [南泰尔](../Page/南泰尔.md "wikilink")

  - [里昂](../Page/里昂.md "wikilink")

  - [斯科普里](../Page/斯科普里.md "wikilink")

  - [弗拉察](../Page/弗拉察.md "wikilink")

  - [乌普萨拉](../Page/乌普萨拉.md "wikilink")

## 參考

  - ，1977年

  - ，1982年

  - ，Dolj County Statistical Office，1992年

## 外部連結

  - [克拉約瓦官方網頁](http://www.craiova.ro)

[C](../Category/罗马尼亚城市.md "wikilink")