**克林顿·哈特·梅里厄姆** （[英语](../Page/英语.md "wikilink")：**Clinton Hart
Merriam**，1855年12月5日-1942年3月19日）是美国动物学家、鸟类学家。

## 生平

1855年，梅里厄姆生于[纽约市](../Page/纽约市.md "wikilink")，他的父亲克林顿·李维·梅里厄姆是美国国会议员。他在[耶鲁大学学习生物学与解剖学](../Page/耶鲁大学.md "wikilink")，而后前往[哥伦比亚大学医学院学习](../Page/哥伦比亚大学.md "wikilink")，1879年，获医学博士学位。

1886年，他成为[美国农业部经济鸟类与哺乳动物处](../Page/美国农业部.md "wikilink")(the Division of
Economic Ornithology and
Mammalogy)的首任长官，该部门是国家野生动物研究中心和[美国鱼类及野生動物管理局的前身](../Page/美国鱼类及野生動物管理局.md "wikilink")。他是1888年[国家地理学会的创办者之一](../Page/国家地理学会.md "wikilink")。他发展了[生物区的概念](../Page/生物区.md "wikilink")，以对[北美发现的](../Page/北美.md "wikilink")[生物群系进行分类](../Page/生物群系.md "wikilink")。在晚年，他在美国西部，研究[印第安人部落](../Page/印第安人.md "wikilink")。

1899年，梅里厄姆帮助铁路大亨[爱德华·亨利·哈里曼组织了一次沿](../Page/爱德华·亨利·哈里曼.md "wikilink")[阿拉斯加海岸线的](../Page/阿拉斯加.md "wikilink")[探险航行](../Page/哈里曼阿拉斯加探险.md "wikilink").

一些动物以他的名字来命名，比如梅里厄姆的野火鸡*Meliagris Gallopavo Meriami*，梅里厄姆的麋鹿*Cervus
elaphus merriami*，以及梅里厄姆的金花鼠*Tamias
Merriami*。他的基于细节的分类系统，在现在的哺乳类与鸟类的科学范畴中，仍然具有影响。

1942年，梅里厄姆在[柏克萊去世](../Page/柏克萊.md "wikilink")。

## 外部链接

  - [C. Hart Merriam, *Dawn of the World: Myths and Weird Tales Told by
    the Mewan Indians of
    California*](http://www.yosemite.ca.us/library/dawn_of_the_world/)
    (1910)
  - [C. Hart Merriam, “Indian Village and Camp Sites in Yosemite
    Valley,” *Sierra Club
    Bulletin*](http://www.yosemite.ca.us/library/indian_village_and_camp_sites_in_yosemite_valley.html)
    (1917)
  - [USDA Merriam National Wildlife Research
    Center](https://web.archive.org/web/20041210020027/http://www.aphis.usda.gov/ws/nwrc/hx/merriam.html)

[Merriam](../Category/美国动物学家.md "wikilink")
[Merriam](../Category/美国鸟类学家.md "wikilink") [Merriam,
Clinton Hart](../Category/1855年出生.md "wikilink") [Merriam, Clinton
Hart](../Category/1942年逝世.md "wikilink")