**克氏原螯虾**（[学名](../Page/学名.md "wikilink")：**），原产于美国东南部，又名**美國螯蝦**、**路易斯安那州螯虾**，\[1\][中華人民共和國称](../Page/中華人民共和國.md "wikilink")**[小龙虾](../Page/小龙虾.md "wikilink")**，属[蝲蛄科](../Page/蝲蛄科.md "wikilink")，在[中国北方某些](../Page/中国北方.md "wikilink")[地区被直接称为](../Page/地区.md "wikilink")“[蝲蛄](../Page/蝲蛄.md "wikilink")”，是最具食用价值的[淡水龙虾品种](../Page/淡水龙虾.md "wikilink")，年产量佔整个[淡水龙虾产量的](../Page/淡水龙虾.md "wikilink")70－80%。

## 分布

原產地為从墨西哥北部至佛罗里达州潘汉德尔的[美國墨西哥灣沿岸地區以及美国内陆的](../Page/美國墨西哥灣沿岸地區.md "wikilink")[伊利诺伊州](../Page/伊利诺伊州.md "wikilink")、[俄亥俄州](../Page/俄亥俄州.md "wikilink")\[2\]，後被引進[亞洲](../Page/亞洲.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、[歐洲和美洲其他地区](../Page/歐洲.md "wikilink")，成为一种分布极其广泛的外来入侵物种。在欧洲南部，克氏原螯虾繁衍迅速，挤占了欧洲本地[奧斯塔歐洲螯蝦和](../Page/奧斯塔歐洲螯蝦.md "wikilink")[巨石螯虾的活动空间](../Page/巨石螯虾.md "wikilink")。

## 生態

主要生活於溫暖的[淡水水域](../Page/淡水.md "wikilink")，如流速緩慢的[河流](../Page/河流.md "wikilink")、[沼澤](../Page/沼澤.md "wikilink")、[湖泊和稻田等](../Page/湖泊.md "wikilink")。生長快速，可以忍受無水的旱季達4個月。體重最重可超過50克，體長從5.5公分到12公分不等\[3\]。此物種也可忍受微鹽的水體，這在淡水螯蝦中是很少見的。在濕季，此物種能在乾地橫跨數[英里尋找棲息地](../Page/英里.md "wikilink")。一般來說，可有五年壽命，儘管已知有部分個體在大自然存活超過六年。

## 用途

### 經濟價值

克氏原螯蝦的環境容忍性強，因此造成網路瘋狂謠傳此物種能在含有高污染性[毒素的](../Page/毒素.md "wikilink")[水質下存活](../Page/水質.md "wikilink")\[4\]，事實上，高污染性的水域或不流動而缺乏氧氣的死水灘，此種生物仍不易生存。小龍蝦多數都是杂食动物（如：克氏原螯蝦、[紅螯螯蝦等大型](../Page/紅螯螯蝦.md "wikilink")[淡水龍蝦](../Page/淡水龍蝦.md "wikilink")），不論活體甚至連未消化[糞便或是動物](../Page/糞便.md "wikilink")[屍體](../Page/屍體.md "wikilink")，都可成為其盤中飧，因此不可與其他生物放養在同一水族箱中，此物種顏色多變因此是水族館受歡迎之種類，目前穩定基因的顏色有藍螯、橘螯、白螯而原色物種則被稱為美人蝦販售。

生長快速與高環境忍受度使得此物種成為受歡迎的經濟物種。在[路易斯安那州的農場](../Page/路易斯安那州.md "wikilink")，其養殖地廣達500平方公里，每年可帶來數百萬美元的產值。

在[-{zh:肯亞;zh-hans:肯尼亚;zh-hk:肯尼亞;zh-tw:肯亞}-](../Page/肯亞.md "wikilink")，此螯蝦物種被引進來減少[蝸牛的數量](../Page/蝸牛.md "wikilink")，進而降低[血吸蟲病的發生率](../Page/血吸蟲病.md "wikilink")。

### 食用

克氏原螯蝦因體型比其他淡水蝦類大，頭大身小肉少殼多，肉質具腥味，因而被多製成辣味[料理以掩蓋其本身異味](../Page/料理.md "wikilink")。克氏原螯蝦在[美國是很常見之料理食材](../Page/美國.md "wikilink")，通常和馬鈴薯玉米水煮搭配[卡疆粉](../Page/卡疆粉.md "wikilink")（Cajun）調味。在美國，當地食用的小龍蝦有98%均產自[路易斯安娜州](../Page/路易斯安娜州.md "wikilink")，當中有70%在州內食用，其餘皆出口，佔全世界小龍蝦生產量的90%\[5\]。路易斯安娜州在1983年将克氏原螯虾选为州代表动物，并且每年都举办「龙虾节」。\[6\]。

在中國，克氏原螯蝦于1929年从日本移植到南京一带\[7\]，后经人工养殖和推广，逐渐扩展到北京、天津、河北、山西、河南等省市。在中国的南方和北方可生存和发展，已经成为归化于中国自然水体的一个种群。2000年，江苏省[盱眙县举办了](../Page/盱眙县.md "wikilink")「龙虾节」，此后盱眙龙虾开始扩展到[苏](../Page/江苏.md "wikilink")[浙](../Page/浙江.md "wikilink")[沪及全国](../Page/上海.md "wikilink")。盱眙的「十三香麻辣小龙虾」在中国名声大噪，「十三香调料」是一种约定俗成的叫法，实际上是由20多种香料搭配使用，可分别烹制出多种味型\[8\]\[9\]。2000年代以来，北京簋街的麻辣小龙虾（「麻小兒」）也逐渐成为北京赫赫有名的小吃，\[10\]是常見的餐點，受到食客的普遍歡迎，並且有部份製成龍蝦醬外銷。

台灣的便利商店及火鍋店等製作龍蝦手卷亦有使用。

### 肥料

日本許多大型湖泊裡都會發現美國螯蝦的蹤跡，為嚴控這種外來物種的繁殖數量，防止破壞湖水生態
捕獲的美國螯蝦會被漁民當場「人道毀滅」，直接踩碎當肥料。\[11\]

### 宠物观赏

#### 飼養環境

水温在22－28度最为适合，但是-5至37度的水温下淡水或微鹹水域均可存活。可長時間挨餓及较髒环境下生长，但仍須在流動水流的氧氣充足水域生存。另外，橘色'白色藍色等等顏色，發展出所謂的橘螯蝦'白螯蝦'藍螯蝦等螯蝦'不僅漂亮美觀，加上養殖難度簡易，又目前台灣有穩定市場，但因藍色品系稀有，市面上販售的藍螯蝦大多是佛羅里達州藍螯蝦，而非美國螯蝦.因此目前的橘色跟白色售價約在40元\~80元之間。而藍色或其它新系列的顏色則較貴

## 危害

### 物种入侵

克氏原螯虾的[物种入侵可能對當地生態帶來影響](../Page/物种入侵.md "wikilink")，克氏原螯虾在一个水域的出现，对同一水域的鱼类、甲壳类、水生植物、水稻等造成了很大的威胁，还直接危害了人工养殖的水产。会打乱原本平衡的食物链，改变生态系统的原貌。并且它的食性十分广泛，建立种群的速度极快，易于扩散。此外，克氏原螯虾可能與當地淡水螯蝦物種發生競爭，侵占有属于当地淡水螯蝦的生存空间。2010年1月7日入列中國大陸第二批外來入侵物種名單。[國際自然保護聯盟](../Page/國際自然保護聯盟.md "wikilink")[物種存續委員會的入侵物種專家小組](../Page/物種存續委員會.md "wikilink")（ISSG）列為[世界百大外來入侵種](../Page/世界百大外來入侵種.md "wikilink")。

### 破坏堤坝

克氏原螯虾不仅吃湖中水生植物和小鱼小虾，由于它们生活在江、河、水库、池塘和水田等的边上，螯虾又喜欢钻洞，破坏能力也是十分强大，常会洞穿田埂破坏堤坝，对防汛抗洪会有影响。

## 图库

<center>

<File:Procambarus_clarkii.jpg> <File:Procambarus> clarkii
Bottom.jpg|腹面，♂ <File:Procambarus> clarkii side.jpg

</center>

## 註釋

## 參考文獻

  -
  -
  - [小龙虾千萬不能吃？——瑞昌龙虾节4000多人品龙虾，200多人已被撂倒](http://sinai.blog.163.com/blog/static/1207114882011713115513336/)

  - [認識外來種美國螯蝦](http://web.nchu.edu.tw/~htshih/shrimp/crayfish.pdf) -
    [國立中興大學](../Page/國立中興大學.md "wikilink")

  - [美國螯蝦生活習性與防治](https://web.archive.org/web/20140115074911/http://mail.tmue.edu.tw/~fireant/epaper-10010.html)
    - 黃基森、周建銘 - 教育部防治外來入侵種及植物病蟲害電子報

  - [美國螯蝦對台灣生態迫害究竟有多大](http://kmweb.coa.gov.tw/knowledge/knowledge_cp.aspx?ArticleId=109860&ArticleType=A&CategoryId=C&kpi=0&Order=&dateS=&dateE=)
    - 農業知識入口網 - 小知識串成的大力量 - [行政院農業委員會](../Page/行政院農業委員會.md "wikilink")

[Category:原蝲蛄屬](../Category/原蝲蛄屬.md "wikilink")
[Category:入侵甲殼動物種](../Category/入侵甲殼動物種.md "wikilink")
[Category:食用甲殼類](../Category/食用甲殼類.md "wikilink")

1.

2.
3.

4.  [小龙虾的来历、特征与分布范围](http://m.china.com.cn/yidian/doc_1_80781_443201.html)

5.

6.

7.  [真实第25小时·探秘火爆小龙虾](http://www.bilibili.com/video/av4672040/)，哔哩哔哩弹幕视频网。

8.

9.

10.

11. [震驚中國人　國民美食小龍蝦在日本淪肥料](https://tw.appledaily.com/new/realtime/20170924/1209948/)