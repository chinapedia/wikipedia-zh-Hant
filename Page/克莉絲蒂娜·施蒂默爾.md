**克莉絲蒂娜·施蒂默爾**（**Christina
Stürmer**，）是一位[奧地利](../Page/奧地利.md "wikilink")[Pop與](../Page/Pop音樂.md "wikilink")[搖滾音樂](../Page/搖滾樂.md "wikilink")[歌手](../Page/歌手.md "wikilink")，所屬的[樂團也以她為名](../Page/樂團.md "wikilink")。於2003年出版的第一首歌曲為「Ich
lebe」（我活著），曾在奧地利多項排名中名列第一。

2004年「Vorbei」成為[德國前](../Page/德國.md "wikilink")100名的歌曲。從2005年開始，她在包括[德國](../Page/德國.md "wikilink")、[瑞士及](../Page/瑞士.md "wikilink")[義大利等國的德語地區擁有高知名度](../Page/義大利.md "wikilink")，她的唱片於2006年，在這些地方共賣出超過150萬張。

## 得獎紀錄

  - [Amadeus Austrian Music
    Award](../Page/Amadeus_Austrian_Music_Award.md "wikilink") 2004 for
    "Best Pop/Rock national" and "Newcomer of the Year national"
  - [Amadeus Austrian Music
    Award](../Page/Amadeus_Austrian_Music_Award.md "wikilink") 2005 for
    "Best Pop/Rock national" and "Best Single national"
  - [ECHO](../Page/ECHO_\(music_award\).md "wikilink") 2006 for "Best
    National Female Artist (Rock/Pop)"
  - [Amadeus Austrian Music
    Award](../Page/Amadeus_Austrian_Music_Award.md "wikilink") 2006 for
    "Best Pop/Rock national" and "Best Single national"
  - [Goldene Stimmgabel](../Page/Goldene_Stimmgabel.md "wikilink") 2006
    for "Best German Rock/Pop"

## 外部連結

  - [Official site](http://www.christinaonline.at/)

  -
[Category:奧地利歌手](../Category/奧地利歌手.md "wikilink")