**克里斯蒂安·克林**（[德语](../Page/德语.md "wikilink")：****，），生於[霍恩埃姆斯](../Page/霍恩埃姆斯.md "wikilink")，[奥地利](../Page/奥地利.md "wikilink")[一级方程式赛车运动员](../Page/一级方程式赛车.md "wikilink")，在2010年效力过[伊斯巴尼亞車隊](../Page/伊斯巴尼亞車隊.md "wikilink")。

## F1职业生涯

  - [美洲豹车队](../Page/美洲豹车队.md "wikilink")：2004年
  - [红牛车队](../Page/红牛车队.md "wikilink")：2005年、2006年
  - [伊斯巴尼亞車隊](../Page/伊斯巴尼亞車隊.md "wikilink")：2010年

## 外部链接

  - [克里斯蒂安·克林官方网站](https://web.archive.org/web/20060222231716/http://www.christian-klien.com/)

[Category:紅牛車隊一級方程式車手](../Category/紅牛車隊一級方程式車手.md "wikilink")
[Category:伊斯巴尼亞一級方程式車手](../Category/伊斯巴尼亞一級方程式車手.md "wikilink")
[Category:奧地利一級方程式車手](../Category/奧地利一級方程式車手.md "wikilink")