**克里斯蒂安·惠更斯**（，），[荷兰](../Page/荷兰.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")、[天文学家和](../Page/天文学家.md "wikilink")[数学家](../Page/数学家.md "wikilink")，[土卫六的发现者](../Page/土卫六.md "wikilink")。他还发现了[猎户座大星云和](../Page/猎户座大星云.md "wikilink")[土星光环](../Page/土星光环.md "wikilink")。

## 生平

惠更斯于1629年4月14日出生在[海牙](../Page/海牙.md "wikilink")，是家中的第二个儿子。其父[康斯坦丁·惠更斯](../Page/康斯坦丁·惠更斯.md "wikilink")（Constantijn
Huygens，1596-1687）为外交家，是数学家[笛卡尔的朋友](../Page/笛卡尔.md "wikilink")，家境富裕。其母为苏珊娜·范·巴尔勒（Suzanna
van
Baerle，1599-1637），1627年4月6日与其父结婚，1637年去世。\[1\]惠更斯幼年跟随父亲学习，16岁后进入[莱顿大学学习](../Page/莱顿大学.md "wikilink")[法律与](../Page/法律.md "wikilink")[数学](../Page/数学.md "wikilink")，两年后又转到[布雷达的](../Page/布雷达.md "wikilink")[奥兰治学院](../Page/奥兰治学院.md "wikilink")（Oranjecollege）继续学习。学生时代他接受过[笛卡尔的指导](../Page/笛卡尔.md "wikilink")。1651年发表了第一篇论文，内容为求解曲线所围区域的面积。1655年成为[法学博士](../Page/法学博士.md "wikilink")，1663年成为[英国皇家学会会员](../Page/英国皇家学会.md "wikilink")，1666年成为[荷兰科学院院士](../Page/荷兰科学院.md "wikilink")，同一年在[路易十四的邀请下成为](../Page/路易十四.md "wikilink")[法国皇家科学院院士](../Page/法国皇家科学院.md "wikilink")。\[2\]利用[巴黎天文台](../Page/巴黎天文台.md "wikilink")（1672年竣工），他進一步进行了[天文觀測](../Page/天文学.md "wikilink")。

1678年他介绍荷兰数学家和物理学家[尼古拉斯·哈特苏克](../Page/尼古拉斯·哈特苏克.md "wikilink")（Nicolaas
Hartsoeker）認識了法國科學家如[尼古拉·马勒伯朗士和](../Page/尼古拉·马勒伯朗士.md "wikilink")[乔凡尼·多美尼科·卡西尼](../Page/乔凡尼·多美尼科·卡西尼.md "wikilink")。1684年，他出版了《Astroscopia
Compendiaria》，介绍了他新发明的[空中望远镜](../Page/航空望远镜.md "wikilink")。

1681年，惠更斯在罹患重病後搬回海牙。他試圖在1685年返回法國，但[南特敕令的废止使他未能返回](../Page/南特敕令.md "wikilink")。1695年7月8日，惠更斯死於海牙，被埋葬在[圣雅各堂](../Page/圣雅各堂_\(海牙\).md "wikilink")。\[3\]

他指导过[莱布尼兹学习](../Page/莱布尼兹.md "wikilink")[数学](../Page/数学.md "wikilink")，与[牛顿等人也有交往](../Page/艾薩克·牛頓.md "wikilink")，终生未婚。

## 成就

惠更斯一生研究成果丰富，在多个领域都有所建树，许多重要著作是在他逝世后才发表的，《惠更斯全集》共有22卷，由荷兰科学院编辑出版。\[4\]

### 数学

在[布萊茲·帕斯卡鼓勵他之後](../Page/布萊茲·帕斯卡.md "wikilink")，惠更斯1657年发表了《论赌博中的计算》，被认为是[概率论诞生的标志](../Page/概率.md "wikilink")。同时对[二次曲线](../Page/二次曲线.md "wikilink")、复杂曲线、悬链线、曳物线、[对数螺线等平面曲线都有所研究](../Page/对数螺线.md "wikilink")。发现了旋轮线也是最速降线。\[5\]

### 力学

  - 在《摆式时钟或用于时钟上的摆的运动的几何证明》、《[摆钟论](../Page/摆钟论.md "wikilink")》等论文中提出了钟摆摆动[周期的公式](../Page/周期.md "wikilink")：\(2\pi\sqrt[]{l/g}\)。1656年设计并制造出了利用摆取代重力齿轮的摆钟。
  - 研究了完全弹性[碰撞](../Page/碰撞.md "wikilink")，证明了碰撞前后[能量和](../Page/能量.md "wikilink")[动量的守恒](../Page/动量.md "wikilink")。研究成果在其死后发表于《论物体的碰撞运动》一文中。

### 物理学

  - 创立了光的波动说，把[以太作为光传播的介质](../Page/以太.md "wikilink")，在《光论》一书中提出了[惠更斯原理](../Page/惠更斯原理.md "wikilink")。解释了[冰洲石的](../Page/冰洲石.md "wikilink")[双折射现象](../Page/双折射.md "wikilink")。
  - 他还和[虎克共同测定了温度表的固定点](../Page/罗伯特·虎克.md "wikilink")，即[冰点和](../Page/冰点.md "wikilink")[沸点](../Page/沸点.md "wikilink")。

### 天文学

惠更斯研究了透镜的相关[物理原理](../Page/物理.md "wikilink")，并发明了[惠更斯目镜](../Page/惠更斯目镜.md "wikilink")。1655年惠更斯提出，土星被一個堅硬的環圍住，一個薄又扁，向黃道傾斜的環。他用自製的折射望遠鏡，首次發現了[土星的](../Page/土星.md "wikilink")[衛星](../Page/衛星.md "wikilink")——[土卫六](../Page/土卫六.md "wikilink")，\[6\]同年，惠更斯觀察到了[猎户座大星云並將它畫了下來](../Page/猎户座大星云.md "wikilink")。1659年，他的畫被公佈在Systema
Saturnium裡。\[7\]1659年利用自己磨制的望远镜，发现了土星的[光环](../Page/土星環.md "wikilink")。\[8\]
他用他的現代望遠鏡成功地把星雲分成不同的恆星。惠更斯並發現了幾個星雲和一些雙星。

## 外星生物

惠更斯相信[外星生命的存在](../Page/外星生命.md "wikilink")。1695年，他在過世前完成著作《Cosmotheoros》，惠更斯談論關於他對外星生物的想法。惠更斯覺得其他星球的生物和地球上的[生物極為相似](../Page/生物.md "wikilink")。他認為外星生物需要可用的液態水，因此水的性質會因星球之間的差別而不同，像是地球上的水在[木星會迅速結冰](../Page/木星.md "wikilink")，而在[金星上則會蒸發](../Page/金星.md "wikilink")。惠更斯甚至提及在木星和火星表面上的亮點和令人注意的暗處。他的解釋只成立於有[水和](../Page/水.md "wikilink")[冰的星球](../Page/冰.md "wikilink")。

## 紀念

  - 惠更斯號：登陸在土星衛星[土衛六上](../Page/土衛六.md "wikilink")，是[卡西尼-惠更斯號的一部份](../Page/卡西尼-惠更斯號.md "wikilink")。
  - [小行星2801](../Page/小行星2801.md "wikilink")（惠更斯）：位於木星與火星之間小行星帶的小行星。
  - 惠更斯：一個火星上的隕石坑。
  - 惠更斯山：是在月球[亞平寧山脈上的一座山](../Page/亞平寧山脈_\(月球\).md "wikilink")。
  - 惠更斯軟件（暫譯）：是指一種顯微鏡圖像處理包。
  - 惠更斯目鏡：一種可以修正色差的目鏡。
  - [惠更斯－菲涅耳原理](../Page/惠更斯－菲涅耳原理.md "wikilink")：用一個簡單的模型，研究波傳播的問題。
  - [惠更斯小波](../Page/惠更斯小波.md "wikilink")：是純量繞射的數學基礎。
  - W.I.S.V.惠更斯：位於[代爾夫特理工大學的荷蘭研究協會](../Page/代爾夫特理工大學.md "wikilink")，從事數學和電腦科學的研究。
  - 惠更斯實驗室：總部位於荷蘭[萊頓大學物理系](../Page/萊頓大學.md "wikilink")。
  - 惠更斯[超級電腦](../Page/超級電腦.md "wikilink")：位於荷蘭阿姆斯特丹SARA的國家超級電腦。
  - 惠更斯建築：位於荷蘭[諾德群克](../Page/諾德群克.md "wikilink")，首先被建造在太空商業公園的對面ESTEC。
  - 惠更斯建築：位於荷蘭[奈梅亨](../Page/奈梅亨.md "wikilink")，Radboud大學科學系的主要建築之一。

## 资料来源

## 外部連結

  -   - [Treatise on Light](http://www.gutenberg.org/etext/14725)
        translated into English by Silvanus P. Thompson, Project
        Gutenberg etext.

  - [De Ratiociniis in Ludo Aleae or The Value of all Chances in Games
    of
    Fortune, 1657](http://math.dartmouth.edu/~doyle/docs/huygens/huygens.pdf)
    Christiaan Huygens' book on probability theory. An English
    translation published in 1714. Text pdf file.

  - *[Horologium
    oscillatorium](http://digital.library.cornell.edu/cgi/t/text/text-idx?c=kmoddl;cc=kmoddl;view=toc;subview=short;idno=kmod053)*（German
    translation, pub. 1913）on the pendulum clock

  - *[ΚΟΣΜΟΘΕΩΡΟΣ](http://www.staff.science.uu.nl/~gent0113/huygens/huygens_ct_en.htm)*
    (*Cosmotheoros*). (English translation of Latin, pub. 1698;
    subtitled *The celestial worlds discover'd: or, Conjectures
    concerning the inhabitants, plants and productions of the worlds in
    the planets.*)

  - *[Traité de la lumière](http://www.gutenberg.org/etext/14725)* or
    *[Treatise on
    light](http://www.archive.org/details/treatiseonlight031310mbp)*（English
    translation, pub. 1912 and again in 1962）

  - [Systema Saturnium 1659
    text](http://www.sil.si.edu/DigitalCollections/HST/Huygens/huygens.htm)
    a digital edition of Smithsonian Libraries

  - *[On Centrifugal
    Force](http://www.princeton.edu/~hos/mike/texts/huygens/centriforce/huyforce.htm)*（1703）

  - [Huygens' work at
    WorldCat](http://worldcat.org/identities/find?fullName=christiaan+huygens)

  - [Christiaan Huygens biography and
    achievements](http://www.brighthub.com/science/space/articles/50441.aspx)

  - [Portraits of Christiaan
    Huygens](http://www.leidenuniv.nl/fsw/verduin/stathist/huygens/acad1666/huygpor/)

### 介紹

  - [Huygensmuseum
    Hofwijck](https://web.archive.org/web/20070716001341/http://www.hofwijck.nl/hofwijck/en/)
    in Voorburg, Netherlands, where Huygens lived and worked.
  - [Huygens
    Clocks](http://www.sciencemuseum.org.uk/onlinestuff/stories/huygens_clocks.aspx?keywords=huygens)
    exhibition from the Science Museum, London
  - [LeidenUniv.nl](https://web.archive.org/web/20070623051507/http://bc.ub.leidenuniv.nl/bc/tentoonstelling/Huygens/index.html),
    Exhibition on Huygens in [University Library
    Leiden](../Page/University_Library_Leiden.md "wikilink")

### 其他

  -
  - [Huygens and music
    theory](https://web.archive.org/web/20010307171657/http://www.xs4all.nl/~huygensf/english/huygens.html)
    Huygens–Fokker Foundation—on Huygens' [31 equal
    temperament](../Page/31_equal_temperament.md "wikilink") and how it
    has been used

  - [Christiaan Huygens on the 25 Dutch Guilder banknote of
    the 1950s.](http://www-personal.umich.edu/~jbourj/money1.htm)

  -
  - [How to pronounce
    "Huygens"](https://web.archive.org/web/20030205221441/http://frank.harvard.edu/~paulh/misc/huygens.htm)

[Category:法兰西科学院院士](../Category/法兰西科学院院士.md "wikilink")
[Category:荷兰物理学家](../Category/荷兰物理学家.md "wikilink")
[Category:荷兰天文学家](../Category/荷兰天文学家.md "wikilink")
[Category:荷兰数学家](../Category/荷兰数学家.md "wikilink")
[Category:衛星發現者](../Category/衛星發現者.md "wikilink")
[Category:荷蘭黃金時代人物](../Category/荷蘭黃金時代人物.md "wikilink")
[Category:萊頓大學校友](../Category/萊頓大學校友.md "wikilink")
[Category:海牙人](../Category/海牙人.md "wikilink")

1.  [Constantijn Huygens Lord of Zuilichem
    (1596-1687)](http://www.essentialvermeer.com/history/huygens.html)
2.  [惠更斯（christiaan Huygens）](http://kxj.7456.net/show/2/58/)
3.
4.  [Bibliographical notes on the Oeuvres Complètes
    (1888-1950)](http://www.phys.uu.nl/~huygens/bibl_oeuvrescompletes_en.htm)
5.  [惠更斯与概率论的奠基(1)](http://www.ce86.com/a/lixue/lxqt79/200504/05-35965.html)

6.  [揭秘土星卫星神秘面纱：[土卫六发现湖泊群](../Page/土卫六.md "wikilink")](http://www.sxgov.cn/souqi/souqi_content/2009-03/18/content_51654.htm)

7.  [[猎户座大星云的观测](../Page/猎户座大星云.md "wikilink")
    （图）](http://chro.cpst.net.cn/xxkd/2008_12/229398822.html)
8.  [神奇的[土星光环](../Page/土星環.md "wikilink")](http://www.e-museum.com.cn/dmsa/nature/universe/80010.shtml)