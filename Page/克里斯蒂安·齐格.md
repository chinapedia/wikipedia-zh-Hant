**克里斯蒂安·齐格**（，），是一名前[德国職業足球員](../Page/德国.md "wikilink")，司职[左後衛](../Page/左後衛.md "wikilink")。退役後任职主教練。

## 生平

### 球會

  - 拜仁慕尼黑

薛基是由德國一些小型球會打起。1990年加入[德甲](../Page/德甲.md "wikilink")[拜仁慕尼黑](../Page/拜仁慕尼黑足球俱乐部.md "wikilink")，並於該會打出名堂。早於1994年他就取得生平首個德甲聯賽冠軍，而他更加於英格蘭舉行歐洲國家盃賽事中取得冠軍；1997年也贏得一個德甲冠軍。

  - AC米蘭

1997年[意大利大球會](../Page/意大利.md "wikilink")[AC米蘭遭受十年來最差劣成績](../Page/AC米蘭.md "wikilink")，因而從拜仁收購薛基。然而加盟兩季，薛基都未能適應球會風格。1999年AC米蘭終於贏回意甲冠軍，惟薛基並沒作太大貢獻。

  - 轉戰英超

1999年夏季薛基走到[英超效力](../Page/英超.md "wikilink")[米杜士堡](../Page/米德尔斯堡足球俱乐部.md "wikilink")。此後他狀態回復，一季已上陣29場聯賽。惟球季後薛基受到英超大球會[利物浦招攬](../Page/利物浦足球俱乐部.md "wikilink")，薛基為求轉會竟與米杜士堡鬧翻。轉投利物浦後薛基表現始終未如理想，僅於贏得[英格蘭聯賽盃時作出貢獻](../Page/英格蘭聯賽盃.md "wikilink")。球季下半段薛基更喪失位置。薛基於2001年轉投另一支[英超球會](../Page/英超.md "wikilink")[熱刺](../Page/托特纳姆热刺足球俱乐部.md "wikilink")。起先能取得正選，然而傷病問題一直困擾著薛基，上陣時間因而漸少。

  - 重返德甲

2004年他重返德國效力[慕遜加柏](../Page/门兴格拉德巴赫足球俱乐部.md "wikilink")，並於退役後留在球會任職。

### 國家隊

早於1993年薛基已是國家隊主力，曾代表過國家隊出賽多個主要大賽，其中[1996年歐洲國家杯他贏得了冠軍](../Page/1996年欧洲足球锦标赛.md "wikilink")；[2002年世界杯他亦贏得一個亞軍](../Page/2002年世界杯.md "wikilink")。2004年參加完歐洲國家盃後，宣告退役。

### 教練

2008年10月6日在球隊主教練因戰績欠佳而下课，薜基為慕遜加柏出任臨時教練\[1\]。2010年5月27日轉會至[比勒費爾德出任主教練](../Page/比勒費爾德足球俱樂部.md "wikilink")\[2\]，但球隊開季11戰錄得1勝1和9負的差劣成績，排名墊底，於11月7日被球會解僱\[3\]。

## 荣誉

### 国家队

  - 1996年[欧洲國家杯冠军](../Page/欧洲國家杯.md "wikilink")
  - [2002年世界杯亚军](../Page/2002年世界杯.md "wikilink")

### 俱乐部

  - [拜仁慕尼黑](../Page/拜仁慕尼黑足球俱乐部.md "wikilink")

<!-- end list -->

  - [德甲冠军](../Page/德甲.md "wikilink")（93-94，96-97）
  - [欧洲联盟杯冠军](../Page/欧洲联盟杯.md "wikilink")（95-96）
  - [德国联赛杯](../Page/德国联赛杯.md "wikilink")（97-98）

<!-- end list -->

  - [AC米兰](../Page/米兰足球俱乐部.md "wikilink")

<!-- end list -->

  - [意甲冠军](../Page/意甲.md "wikilink")（98-99)

<!-- end list -->

  - [利物浦](../Page/利物浦足球俱乐部.md "wikilink")

<!-- end list -->

  - [英格兰联赛杯冠军](../Page/英格兰联赛杯.md "wikilink")（00-01）

## 參考資料

## 外部連結

  - [Official website](http://www.christianziege.de)

  - [Yahoo\!
    sport](https://web.archive.org/web/20061202152238/http://uk.sports.yahoo.com/fo/profiles/4272.html)

  - [Borussia
    MG](http://www.borussia.de/CDA/christian_ziege,103477,0,,de.html)

  -
[Category:德國足球運動員](../Category/德國足球運動員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:拜仁慕尼黑球員](../Category/拜仁慕尼黑球員.md "wikilink")
[Category:AC米蘭球員](../Category/AC米蘭球員.md "wikilink")
[Category:米杜士堡球員](../Category/米杜士堡球員.md "wikilink")
[Category:利物浦球員](../Category/利物浦球員.md "wikilink")
[Category:熱刺球員](../Category/熱刺球員.md "wikilink")
[Category:慕遜加柏球員](../Category/慕遜加柏球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:德國足球主教練](../Category/德國足球主教練.md "wikilink")
[Category:比勒費爾德主教練](../Category/比勒費爾德主教練.md "wikilink")
[Category:翁特哈兴主教练](../Category/翁特哈兴主教练.md "wikilink")
[Category:1996年歐洲國家盃球員](../Category/1996年歐洲國家盃球員.md "wikilink")
[Category:1998年世界盃足球賽球員](../Category/1998年世界盃足球賽球員.md "wikilink")
[Category:2000年歐洲國家盃球員](../Category/2000年歐洲國家盃球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")
[Category:歐洲國家盃冠軍隊球員](../Category/歐洲國家盃冠軍隊球員.md "wikilink")
[Category:德國旅外足球運動員](../Category/德國旅外足球運動員.md "wikilink")
[Category:義大利外籍足球運動員](../Category/義大利外籍足球運動員.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:德丙主教练](../Category/德丙主教练.md "wikilink")

1.  [门兴格拉德巴赫五连败 主帅吕许凯下课](http://www.dfo.cn/dfo/show.asp?id=8357)
2.
3.