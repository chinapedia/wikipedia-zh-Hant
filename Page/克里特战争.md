**克里特战争**（公元前205年–公元前200年），是马其顿的国王[腓力五世](../Page/腓力五世_\(马其顿\).md "wikilink")、[埃托利亚同盟](../Page/埃托利亚同盟.md "wikilink")，一些克里特岛城邦如[俄勞斯](../Page/俄勞斯.md "wikilink")（Olous）和[耶拉派特拉](../Page/耶拉派特拉.md "wikilink")，以及斯巴达海盗組成同盟，對抗[罗得岛军队](../Page/罗得岛.md "wikilink")、稍后[帕加马的](../Page/帕加马.md "wikilink")[阿塔罗斯一世](../Page/阿塔罗斯一世.md "wikilink")、[拜占庭](../Page/拜占庭.md "wikilink")、[基濟科斯](../Page/基濟科斯.md "wikilink")、[雅典和](../Page/雅典.md "wikilink")[克诺索斯之間的戰爭](../Page/克诺索斯.md "wikilink")。

马其顿人刚刚结束了[第一次马其顿战争](../Page/第一次马其顿战争.md "wikilink")，而腓力看到了打败罗得岛人的机会，他与埃托利亚同盟和斯巴达海盗结盟，开始袭击罗得岛船只。腓力同时也与几个罗得岛城邦结盟，比方耶拉派特拉和俄勞斯。\[1\]由于罗得岛的海上貿易遭到海盗掠夺，造成经济沉重損害，腓力相信他最終压垮罗得岛的機會即將到来。为了达成他的目标，他与[塞琉古帝国国王](../Page/塞琉古帝国.md "wikilink")[安条克三世缔结了同盟](../Page/安条克三世.md "wikilink")，共同反对埃及的[托勒密五世](../Page/托勒密五世.md "wikilink")（塞琉古帝国和埃及是另外两个[继业者国家](../Page/继业者.md "wikilink")）。腓力开始襲擊[色雷斯和](../Page/色雷斯.md "wikilink")[馬摩拉海的托勒密领土和罗得岛的同盟](../Page/馬摩拉海.md "wikilink")。

公元前202年，罗得岛和他们的盟国帕加马、基濟科斯和拜占庭組成他们的聯合舰队，并在[基俄斯岛海战中打败了腓力](../Page/基俄斯岛海战.md "wikilink")。仅仅数月之后，腓力的舰队在[莱德岛海战打败了罗得岛人](../Page/莱德岛海战_\(前201年\).md "wikilink")。当腓力劫掠帕加马的领土并袭击[卡里亞的城市时](../Page/卡里亞.md "wikilink")，阿塔罗斯一世前往雅典以图與雅典聯合，企圖在那裏建立一個新戰線來牽制馬其頓軍。他成功与雅典人缔结了联盟，雅典人立即向马其顿宣战。腓力五世無法對此保持沉默，他率領海军和一些步兵袭击了雅典。這結果導致罗马人向馬其頓發出警告，要么腓力撤军，要么面对与[罗马的战争](../Page/罗马.md "wikilink")。在被罗得岛和帕加马舰队打败后，腓力撤離雅典，然而不久後腓力袭击了[赫勒斯滂沿岸城市](../Page/达达尼尔海峡.md "wikilink")[阿彼多斯](../Page/阿彼多斯.md "wikilink")（Abydos）。阿彼多斯經獲长时间的围城戰后陷落了，它的大部分居民自杀。罗马再度向馬其頓發出最後通牒，要腓力停止袭击希腊城市，但這次腓力拒絕了，罗马便向马其顿宣战。[第二次馬其頓戰爭爆發導致馬其頓無法再有效支援克里特的同盟](../Page/第二次馬其頓戰爭.md "wikilink")，这使得那些城邦陷于孤立無援，就連克里特最大的城邦[克诺索斯也加入了罗得岛人一方](../Page/克诺索斯.md "wikilink")。面对这个联盟耶拉派特拉（Hierapynta）和俄勞斯（Olous）被迫投降，並且被迫签订了一个对罗得岛和克诺索斯有利的条约。

## 战争序幕

公元前205年，[第一次马其顿战争结束](../Page/第一次马其顿战争.md "wikilink")，参战各方签订了[腓尼基和約](../Page/腓尼基和約.md "wikilink")。該條約不允许马其顿向西扩张，而罗马这时正陷于与[迦太基的战事中](../Page/迦太基.md "wikilink")，腓力五世希望趁著这个机会夺取希腊世界的控制權。他知道与克里特人结盟会帮他实现他的雄心壮志。\[2\]在擊潰小亚细亚的[帕加马王國及馬其頓与](../Page/帕加马王國.md "wikilink")[埃托利亚结盟之后](../Page/埃托利亚同盟.md "wikilink")，現在希腊主要勢力中除了[罗得岛已没有其他势力能反对他](../Page/罗得岛.md "wikilink")。而罗得岛，身為一个掌控地中海东南经济圈，且有不小的影響力的島嶼，它从前曾与腓力结盟，但同样也與馬其頓曾經的敌人[罗马結盟](../Page/罗马共和国.md "wikilink")。\[3\]

## 海盗与战争

[Macedonia_and_the_Aegean_World_chiniese_simplified.gif](https://zh.wikipedia.org/wiki/File:Macedonia_and_the_Aegean_World_chiniese_simplified.gif "fig:Macedonia_and_the_Aegean_World_chiniese_simplified.gif")

腓尼基和約阻止了腓力的势力向西扩张侵入[伊利里亚或](../Page/伊利里亚.md "wikilink")[亚得里亚海](../Page/亚得里亚海.md "wikilink")，所以腓力将他的视线移到东部的[爱琴海](../Page/爱琴海.md "wikilink")，他开始在爱琴海建造一只庞大的舰队。\[4\]

腓力策劃两种动摇罗得岛在爱琴海统治地位的方法：海上劫掠以及战争。他决定双管齐下，他決定鼓励他的盟友從事海盜行為劫掠罗得岛的商船。他说服涉足海盗行为多年的[克里特人](../Page/克里特人.md "wikilink")、埃托利亚人以及斯巴达人参与到海盗行为中。从罗得岛船只上掠取的大量物资对于这些国家来说是个极大的诱惑。\[5\]
他派埃托利亚海盗[狄西阿庫斯](../Page/埃托利亞的狄西阿庫斯.md "wikilink")（Dicaearchus）越过爱琴海参与了以此大规模掠夺式袭击，在此期间他劫掠了[基克拉澤斯群岛和罗得岛的领土](../Page/基克拉澤斯.md "wikilink")。\[6\]

到公元前205年年末，罗得岛已被这些袭击活动大大削弱了，腓力觉得进行他计划中第二部分的时机已成熟，即直接武装对抗。他说服耶拉派特拉、俄勞斯和其他东克里特的城邦向罗得岛宣战。\[7\]

罗得岛对腓力宣战的初期反擊是外交行動，他们请求[罗马帮助他們反对腓力](../Page/罗马.md "wikilink")。然而對罗马人來說因为[第二次布匿战争才刚刚结束](../Page/第二次布匿战争.md "wikilink")，他們對開啟另一场战争顯得很謹慎。在帕加马、基濟科斯、拜占庭都已加入罗得島一方之后，罗马元老院试图说服平民加入战争，但却无法动摇那些已厌倦战争的民众。\[8\]

在这个当口，腓力攻取了[马尔马拉海沿岸的希腊城市](../Page/马尔马拉海.md "wikilink")[基俄斯和](../Page/基俄斯_\(比提尼亞\).md "wikilink")[邁爾里](../Page/阿帕米亞·邁爾里.md "wikilink")（Myrleia）并将它们夷为平地，这一举动进一步激怒了罗得岛人。腓力于是将这两座城市交给他的姐夫[比提尼亚国王](../Page/比提尼亚.md "wikilink")[普魯西阿斯一世](../Page/普魯西阿斯一世.md "wikilink")，他重建了它们并分别依照他和他妻子的名字将它们重命名为普魯西亞斯和阿帕米亞。作为回报普魯西阿斯一世允诺继续在损及帕加马利益的情况下繼續扩张他的帝国（他与帕加马的战争剛剛于前205年结束）。攻取这些城市同样激怒了埃托利亚人，因为这两座城都是[埃托利亚同盟的成员](../Page/埃托利亚同盟.md "wikilink")。埃托利亚与马其顿间的联盟关系仅仅是出于对腓力的恐惧，这次事件更加恶化了本已很脆弱的关系。\[9\]腓力下一步袭击并征服了[利西馬其亞和](../Page/利西馬其亞.md "wikilink")[迦克墩城](../Page/迦克墩.md "wikilink"),他们本也是埃托利亚同盟的成员，最后被迫脱离了埃托利亚。\[10\]

在回程之路上，腓力的舰队停靠在[色雷斯海岸旁的](../Page/色雷斯.md "wikilink")[萨索斯岛](../Page/萨索斯岛.md "wikilink")。腓力的将军梅特羅多勒斯（Metrodorus）去往与岛同名的首府[薩索斯会见该城的外交官](../Page/薩索斯.md "wikilink")。外交官说假如他们马其顿人不驻军、不征兵、不征税、且允许他们继续使用自己的法律的话，他们将交出这座城市。\[11\]
梅特羅多勒斯回复说国王接受了这些条件，于是萨索斯人向马其顿人敞开了大门。然而当马其顿军入城之后，腓力命令他的士兵奴役当地居民，他们之后被买到其他马其顿城市，然后劫掠了整个城市。\[12\]

## 基俄斯島海戰

[Antiochos_III.jpg](https://zh.wikipedia.org/wiki/File:Antiochos_III.jpg "fig:Antiochos_III.jpg")陈列的安条克三世胸像\]\]
腓力趁著[塞琉古帝國國王](../Page/塞琉古帝國.md "wikilink")[安条克三世與托勒密埃及爆發大戰](../Page/安条克三世.md "wikilink")，埃及[托勒密五世無力關注西方戰事和領土](../Page/托勒密五世.md "wikilink")。因此腓力的军队袭击了托勒密在色雷斯的领土。接着，马其顿舰队向南进犯从埃及手里夺取了[萨摩斯](../Page/萨摩斯岛.md "wikilink")，并俘获了停泊在那儿的[埃及舰队](../Page/古埃及.md "wikilink")。\[13\]之后舰队转而向北并封锁了[基俄斯岛](../Page/基俄斯岛海战.md "wikilink")。腓力试图将这个北爱琴海岛屿作为进取罗得岛的跳板。當帕加马的联合舰队、罗得岛与他们的新盟友拜占庭和基濟科斯成功的阻拦了他的舰队時，這次封锁就变得对腓力很不利。\[14\]腓力看到没有其他选择，冒险发动了与联军的战争，[希俄斯島戰役爆發](../Page/希俄斯島戰役.md "wikilink")。\[15\]

马其顿的舰队有大约200隻战船，差不多比联军的舰队多出一倍。\[16\]\[17\]战争以与阿塔罗斯先發起攻擊，他指揮聯合艦隊的左翼，攻向马其顿舰队右翼。同時罗得岛艦隊司令[特俄菲利克斯指挥的联军右翼袭击了马其顿舰队左翼](../Page/特俄菲利克斯.md "wikilink")。联军在他们的左侧处于上风且俘获了腓力左翼指揮官的座舰，腓力的艦隊左翼指揮官[得摩克拉特斯](../Page/得摩克拉特斯.md "wikilink")（Democrates）在战斗中被杀。\[18\]
然而，左翼的马其顿艦隊發動反擊，逐漸把罗得岛人击退。儘管特俄菲利克斯身上三處負傷，依舊在旗艦上繼續戰鬥，並設法激勵他的部下，打敗了來犯的馬其頓水兵。\[19\]

在联军的左侧，阿塔罗斯看到其中一只他的战船已被敌军击沉，附近的一只也处于危险之中。\[20\]他决定派他的旗舰、四条[四列槳座戰船](../Page/四列槳座戰船.md "wikilink")、三艘快速護衛艦（hemioliae）前往营救。
\[21\]但沒想到腓力的旗艦率隊殺來，阿塔罗斯看到強大的腓力舰队接近，出于恐惧開始逃遁，他故譯讓自己的旗艦搁浅，好登陸逃走。他在戰船甲板上留下散落的金幣、紫色長袍和其他貴重物品後，逃往埃利色雷城。當馬其頓人追到岸邊時，他們因此停下來搜刮戰利品。
\[22\]而腓力二世把阿塔羅斯的座艦移到戰場上展示，使剩下的帕加馬艦隊以為他們的國王陣亡，慌忙撤退。\[23\]

联军右侧的戰況因羅德島人英勇奮陣而發生逆转，马其顿艦隊雖然擊敗帕加馬艦隊，但因為羅德島艦隊的表現而被迫撤离，留下罗得岛人将他们被损毁的战船拖拽到基俄斯岛的海港中。與其他聯軍艦隊於基俄斯島會合。\[24\]

这场战斗对腓力来说代价太大了，他有92艘战船被毁，7艘被俘。\[25\]联军一方，帕加马有3艘战船被毁2艘被俘，罗得岛有3艘沉没无船被俘。战争期间马其顿人有6000划手3000水兵阵亡，2000人被俘。联军的伤亡则明显较低，帕加马有70人阵亡，罗得岛60人阵亡，联军共有600人被俘。\[26\]彼得·格林描述这次战败是“一次曲折而昂贵的挫败”。\[27\]这次战败有效削弱了马其顿海军并挽救了爱琴海诸岛免于大规模入侵。

此次战役之后，罗得岛人决定离开基俄斯并反回故里。在回罗得岛途中，罗得岛海军司令特俄菲利克斯由于在基俄斯所负的伤死去，但死前指定[克隆那烏斯作为他的職位](../Page/克隆那烏斯.md "wikilink")。\[28\]当罗得岛舰队航行在[萊德島与小亚细亚岸边的](../Page/萊德島.md "wikilink")[米利都之间的海峡时](../Page/米利都.md "wikilink")，腓力的舰队袭击了他们，發生[萊德島海戰](../Page/萊德島戰役_\(前201年\).md "wikilink")。腓力打败了罗得岛舰队并迫使它撤回罗得岛。米利都人被腓力胜利所感染，在腓力进入米利都领土时献与他[赫拉克利德花环以示欢迎](../Page/赫拉克利德.md "wikilink")。\[29\]

## 小亚细亚战役

帕加马国王阿塔罗斯早在派出艦隊在爱琴海对抗腓力海军的战役之前，就先加固了他首都帕加马的城墙準備抵御袭击。他希望以这些防御措施来阻止腓力在他的领土上取得大量战利品。在擊敗羅德島和帕加马海軍後，此时腓力决定袭击帕加马，并率军来到城下。他看到城里没有足够的守军，所以派了一群標槍兵去攻城，结果他们很容易的被击败了。\[30\]腓力看到城墙过于牢固，于是在摧毁城外几座神殿，其中包括[阿佛罗狄忒神殿和](../Page/阿佛罗狄忒.md "wikilink")[雅典娜尼斯福鲁斯圣殿](../Page/雅典娜.md "wikilink")）后撤退了。
\[31\]在马其顿人夺取[推雅推喇后](../Page/推雅推喇.md "wikilink")，他们前往铁贝（Thebe）周遭进行劫掠，但是掠夺物却没有预期丰厚。\[32\]当他抵达[铁贝](../Page/奇里乞亞·鐵貝.md "wikilink")（Cilician
Thebe）时,他向那一地区的[赛琉古總督](../Page/塞琉古帝国.md "wikilink")[宙克西斯索取食物](../Page/宙克西斯_\(將軍\).md "wikilink")。然而宙克西斯从未打算给腓力充分补给。\[33\]

腓力对[密細亞地區的战利品十分失望](../Page/密細亞.md "wikilink"),于是挥师南下劫掠了[卡里亚的城镇](../Page/卡里亚.md "wikilink")。他包围了帕納塞斯（Prinassus），一开始帕納塞斯勇敢的抵擋了下来，但当腓力装配好攻城武器时，他派了一名外交官进程提议让他们安全离开城市否则会全部被屠杀。帕納塞斯公民们决定放弃这座城。\[34\]眼下腓力的军队已断粮，所以他攻取了[邁烏斯](../Page/邁烏斯.md "wikilink")（Myus）并把它交给[馬格尼西亞人以换取食物补给](../Page/錫皮盧斯山地區的馬格內西亞.md "wikilink")。虽然馬格尼西亞人没有谷物，但腓力对有足够果品来供给他整个军队已感到十分满足。\[35\]腓力接着快速攻取了Iasus,
Bargylia, [Euromus](../Page/Euromus.md "wikilink"),
Pedasa并在那些地方驻军。他接着包围并攻取了罗得岛人控制下的[Peraea城](../Page/:en:Kaunos.md "wikilink")。

当腓力的舰队在Bargylia过冬时，帕加马及罗得岛联合舰队封锁了海港。马其顿阵营的形势开始变得十分严峻，以至于马其顿人几乎到了要投降的地步。\[36\]然而腓力设法用欺诈的方法逃离了。他派了一名埃及流亡者去见阿塔罗斯和罗得岛告诉他们他准备于次日袭击联军。由于得到这个消息，阿塔罗斯和罗得岛人开始准备即将迎战的舰只。\[37\]当联军正做准备时，腓力趁夜色坐船从他们身边溜了过去，并在他的营地处处点上篝火以制造他仍然待在营地的假象。\[38\]

当腓力专注于这场战役时，他的盟友[阿卡納尼亞人陷于反雅典的战争之中](../Page/阿卡納尼亞.md "wikilink")，之前雅典人曾谋杀两名阿卡纳尼亚运动员。\[39\]阿卡纳尼亚人向腓力抱怨这次挑衅，于是他派了[Nicanor
the
Elephant指挥的一只军队在袭击阿提卡的行动中援助他们](../Page/Nicanor_the_Elephant.md "wikilink")。\[40\]马其顿人及其盟军在袭击雅典之前劫掠了阿提卡。\[41\]劫掠一直蔓延到[雅典学院直到城中的罗马特使勒令马其顿人撤退](../Page/雅典学院.md "wikilink")，否则罗马将对其宣战。\[42\]

腓力的舰队刚刚从联军的封锁中逃离，腓力就命令一只分舰队向雅典进发。马其顿海上中队航向[比雷埃夫斯并俘获了四艘雅典战舰](../Page/比雷埃夫斯.md "wikilink")。正当马其顿分舰队撤离时，早已尾随马其顿舰队穿过爱琴海的罗得岛及帕加马的舰队在联军在[埃伊纳岛的基地突然出现并袭击了马其顿人](../Page/埃伊纳岛.md "wikilink")。联军打败了马其顿舰队，他们返回雅典。雅典人十分高兴以至于他们将从前被破坏的亲马其顿部族、Demetrias和Antigonis连带Attalid部族予以恢复，以迎接阿塔罗斯。\[43\]
阿塔罗斯和罗得岛人说服雅典人集合起来向马其顿人宣战。\[44\]

帕加马舰队驶回在埃伊纳岛的基地，罗得岛人开始征服从埃伊纳岛到罗得岛的所有马其顿岛屿，\[45\]
并成功夺取除了[安德罗斯岛](../Page/安德罗斯岛.md "wikilink"),
[帕罗斯岛](../Page/帕罗斯岛.md "wikilink") and
Cythnos之外的所有岛屿。\[46\]腓力命令他在[埃维亚岛上的总督](../Page/埃维亚岛.md "wikilink")[Philoces率](../Page/Philoces.md "wikilink")2000步兵和200骑兵攻击雅典。\[47\]Philoces没能攻取雅典，但劫掠了附近的乡村。\[48\]

## 罗马的干涉

[Thracian_chersonese.png](https://zh.wikipedia.org/wiki/File:Thracian_chersonese.png "fig:Thracian_chersonese.png")

其间罗得岛、帕加马及雅典代表团行至罗马，出现在元老院前。当他们被接见时他们告诉元老院安条克和腓力间的条约并抱怨腓力攻击他们的领土。作为回应罗马派出三位特使，[Marcus
Aemilius
Lepidus](../Page/Marcus_Aemilius_Lepidus_\(187_BC\).md "wikilink")、[盖乌斯·克劳狄乌斯·尼禄和Publius](../Page/盖乌斯·克劳狄乌斯·尼禄.md "wikilink")
Sempronius Tuditanus到埃及与托勒密商量后去往罗得岛。\[49\]

当这些事发生时，腓力攻击并占领了属于托勒密帝国的Maronea, Cypsela, Doriscos,
Serrheum和Aemus。\[50\]随后马其顿人向[Thracian
Chersonese进发](../Page/Thracian_Chersonese.md "wikilink")，在那儿他们夺取了[Perinthus](../Page/Perinthus.md "wikilink")、[Sestos](../Page/Sestos.md "wikilink")、Elaeus、Alopeconnesus、Callipolis和Madytus。\[51\]之后腓力突袭了帕加马和罗得岛守军联防的[阿彼多斯城](../Page/:en:Abydos,_Hellespont.md "wikilink")。腓力开始在陆海两方面封锁该城以防止向城中运送增援补给。满怀信心的阿彼多斯人用[弩炮击退了一部分攻城武器](../Page/弩炮.md "wikilink")，另一部分攻城器械则被守军焚毁。\[52\]
马其顿人开始用那些残破的攻城器械破坏城墙，最终攻破了外城墙。\[53\]

现在形势对守军来说变得严峻，他们派了其中两名最优秀的公民作为与腓力谈判的代表。当见到腓力后，他们提议在如下条件下交出城市：即帕加马和罗得岛守军在休战情况下离开，所有公民被允许穿着自己衣服离开且可以去任何想去的地方。\[54\]腓力答复道他们必需“无条件投降，要么就像男人一样战斗”\[55\]两名大使无能力做的更多，只好带上这个答复回城。

得知这个答复后，城邦的领导人召集了一个特使来决定他们的对策，他们决定解放所有奴隶以确保他们的忠心，将所有孩子和他们的保姆安置在体育馆，所有妇女安置在[阿尔忒密斯神庙](../Page/阿尔忒密斯.md "wikilink")。他们还要求所有人交出他们的黄金和值钱的衣物以便将它们保管在罗得岛和基濟科斯的船上。\[56\]五十名年长可信的人被选出以执行这些任务。之后所有公民进行了宣誓。波利比奥斯写道：

在朗诵了誓言之后，他们让牧师和所有人宣誓他们将要么打败敌人，要么难堪的死去。

内墙倒塌后，男人们尊守了他们的诺言，从废墟中站起英勇的战斗，迫使腓力派他的卫队向前接替前线的战士。夜幕降临前马其顿人撤回了营地。晚上阿彼多斯人下决心保留女人和孩子们，在黎明时他们派了一些牧师和女祭司带着花环穿过马其顿人营地，将该城交给了腓力。\[57\]

其间，阿塔罗斯的船队穿过爱琴海到达[Tenedos岛](../Page/Tenedos.md "wikilink")。最年轻的罗马使节[Marcus
Aemilius
Lepidus在罗得岛听说了阿彼多斯之围](../Page/:en:Marcus_Aemilius_Lepidus_\(187_BC\).md "wikilink")，于是他前往阿彼多斯去找腓力。他在城外会见了腓力，并转告他元老院的意愿。\[58\]
波利比奥斯写道：

当腓力走进阿彼多斯时，他看见人们以剑刺、自焚、上吊、跳井和跳楼诸种方式毁灭自己和他们的家人。腓力十分震惊，发表了一份声明说“他给那些希图自尽的人三天宽恕”\[59\]阿彼多斯人专注于最初的誓言，想到那样是对已逝者的背叛，拒绝在这些条款下苟活。除了那些被囚或者在类似禁锢中的之外，每个家庭都慷慨赴死。\[60\]

腓力随后发动了对雅典的另一次袭击；他的军队没能拿下雅典或[埃莱夫西纳](../Page/埃莱夫西纳.md "wikilink")，但使阿提卡遭受了波斯战争以来的最严重的劫掠。\[61\]作为回应，罗马人向腓力宣战，且侵略了他在[伊利里亚的领土](../Page/伊利里亚.md "wikilink")。为了处理罗马人以及希腊的形势，腓力被迫放弃攻打罗得岛和帕加马。从而[第二次马其顿战争发生了](../Page/第二次马其顿战争.md "wikilink")。

在腓力从与罗得岛的战役中撤退后，罗得岛人便有机会攻击Olous和-{Hierapytna}-和他们的其他[克里特岛盟友](../Page/克里特岛.md "wikilink")。罗得到在克里特搜寻盟友的举措有了成果，[克诺索斯看到战争向罗得岛一方倾斜时决定加入罗得岛阵营](../Page/克诺索斯.md "wikilink")，以图能在克里特岛称雄。\[62\]许多中克里特的其他城市随后也加入了罗得岛和克诺索斯阵营以对抗-{希拉皮特纳}-和Olous。现在-{希拉皮特纳}-面临两面夹攻，最后投降了。\[63\]

## 后果

[Asia_Minor_188_BCE.jpg](https://zh.wikipedia.org/wiki/File:Asia_Minor_188_BCE.jpg "fig:Asia_Minor_188_BCE.jpg")

战争结束后缔结的条约规定-{希拉皮特纳}-断绝与外国势力的所有联系和盟约，并将所有海港和基地交由罗得岛处置。Olous，在废墟中签订了条约，被迫接受罗得岛的统治。\[64\]作为结果。罗得岛在战后对东克里特主要地区实现了有效控制。战争的结束使罗得岛人能够腾出手来在第二次马其顿战争中帮助他们的盟友。

这场战争对克里特岛的其余部分没有多大短期影响。海盗和[雇佣兵仍在那一地区继续活动](../Page/雇佣兵.md "wikilink")。在三年以后的[第二次马其顿期间的](../Page/第二次马其顿.md "wikilink")[库诺斯法莱之战](../Page/库诺斯法莱之战.md "wikilink")，克里特雇佣[射手同时为罗马人和马其顿人作战](../Page/射手.md "wikilink")。\[65\]\[66\]

这场战争对于腓力和马其顿人来说是昂贵的，他们损失了一只舰队，且花了三年时间才重建，由于他们的希腊盟友[埃托利亚同盟和](../Page/埃托利亚同盟.md "wikilink")[亚该亚同盟背叛了他们投向罗马人](../Page/亚该亚同盟.md "wikilink")。战争的直接后果就是[Dardani](../Page/Dardani.md "wikilink")，一个蛮族部落，侵袭马其顿的北方边界，但腓力击退了这次进攻。\[67\]然而在公元前197年，腓力被罗马人在库诺斯法莱打败且被迫投降。
\[68\]这次战败使腓力丧失了马其顿本土之外的绝大多数领土，且他不得不向罗马人支付1000塔兰特的白银。\[69\]

罗得岛人恢复了对基克拉迪群岛的控制，并再次确立了他们在爱琴海的海上霸权。罗得岛对东克里特的控制是的他们能够大举剿灭那一地区的海盗，但使海盗对罗得岛船只的袭击仍然持续并最终引发了[第二次克里特战争](../Page/第二次克里特战争.md "wikilink")。\[70\]阿塔罗斯与前197年去世，他的儿子[歐邁尼斯二世继承了王位并继续了他父王的反马其顿政策](../Page/歐邁尼斯二世.md "wikilink")。帕加马这时从战争中摆脱出来并得到了腓力曾经占领的几个爱琴海岛屿并进一步成为只有安条克能与之抗衡的小西亚的强权势力。\[71\]

## 注释

<references/>

## 参考资料

### 第一手资料

  - [Livy](../Page/Livy.md "wikilink"), translated by Henry Bettison,
    (1976). Rome and the Mediterranean. London: Penguin Classics. ISBN
    0-14-044318-5.
  - [Polybius](../Page/Polybius.md "wikilink"), translated by Frank W.
    Walbank, (1979). The Rise of the Roman Empire. New York: Penguin
    Classics. ISBN 0-14-044362-2.

### 第二手资料

  - Theocharis Detorakis, (1994). A History of Crete. Heraklion:
    Heraklion. ISBN 960-220-712-4.
  - [Peter Green](../Page/Peter_Green_\(historian\).md "wikilink"),
    (1990). Alexander to Actium: The Historical Evolution of the
    Hellenistic Age. Los Angeles: University of California Press. ISBN
    0-500-01485-X.
  - Philip Matyszak (2004). The Enemies of Rome:From Hannibal to Attila.
    London: Thames and Hudson. ISBN 0-500-25124-X.

[Category:馬其頓歷史](../Category/馬其頓歷史.md "wikilink")
[Category:帕加馬戰爭](../Category/帕加馬戰爭.md "wikilink")
[Category:克里特歷史](../Category/克里特歷史.md "wikilink")

1.  Detorakis, *A History of Crete*

2.
3.
4.  Green, *Alexander to Actium: The Historical Evolution of the
    Hellenistic Age*, 305

5.
6.
7.
8.  Matyszak, *The Enemies of Rome: From Hannibal to Attila the Hun*

9.  [Polybius](../Page/#Polybius.md "wikilink")
    [15.23](http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Plb.+15.23)

10.
11. [Polybius](../Page/#Polybius.md "wikilink")
    [15.24](http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Plb.+15.24)

12.
13. Green, *Alexander to Actium: The Historical Evolution of the
    Hellenstic Age*, 306

14. [Polybius](../Page/#Polybius.md "wikilink")
    [16.2](http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Plb.+16.2)

15.
16. Green, *Alexander to Actium: The Historical Evolution of the
    Hellenstic Age*, 307

17.
18. [Polybius](../Page/#Polybius.md "wikilink")
    [16.3](http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Plb.+16.3)

19. [Polybius](../Page/#Polybius.md "wikilink")
    [16.5](http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Plb.+16.5)

20. [Polybius](../Page/#Polybius.md "wikilink")
    [16.6](http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Plb.+16.6)

21.
22.
23.
24.
25. [Polybius](../Page/#Polybius.md "wikilink")
    [16.7](http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Plb.+16.7)

26.
27.
28. [Polybius](../Page/#Polybius.md "wikilink")
    [16.9](http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Plb.+16.9)

29. [Polybius](../Page/#Polybius.md "wikilink")
    [16.15](http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Plb.+16.15)

30. [Polybius](../Page/#Polybius.md "wikilink")
    [16.1](http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Plb.+16.1)

31.
32.
33.
34. [Polybius](../Page/#Polybius.md "wikilink")
    [16.27](http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Plb.+16.27)

35. [Polybius](../Page/#Polybius.md "wikilink")
    [16.24](http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Plb.+16.24)

36.
37.
38.
39. [Livy](../Page/#Livy.md "wikilink")
    [31.14](http://mcadams.posc.mu.edu/txt/ah/Livy/Livy34.html)

40.
41.
42.
43. Green, *Alexander to Actium: The Historical Evolution of the
    Hellenstic Age*. 304–5

44. [Polybius](../Page/#Polybius.md "wikilink")
    [16.26](http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Plb.+16.26)

45. [Livy](../Page/#Livy.md "wikilink")
    [31.15](http://mcadams.posc.mu.edu/txt/ah/Livy/Livy34.html)

46. [Livy](../Page/#Livy.md "wikilink")
    [31.15](http://mcadams.posc.mu.edu/txt/ah/Livy/Livy34.html)

47. [Livy](../Page/#Livy.md "wikilink")
    [31.16](http://mcadams.posc.mu.edu/txt/ah/Livy/Livy34.html)

48.
49. [Livy](../Page/#Livy.md "wikilink")
    [31.14](http://mcadams.posc.mu.edu/txt/ah/Livy/Livy34.html)

50.
51.
52. [Polybius](../Page/#Polybius.md "wikilink")
    [16.30](http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Plb.+16.30)

53.
54.
55.
56. [Polybius](../Page/#Polybius.md "wikilink")
    [16.31](http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Plb.+16.31)

57. [Polybius](../Page/#Polybius.md "wikilink")
    [16.33](http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Plb.+16.33)

58. [Polybius](../Page/#Polybius.md "wikilink")
    [16.34](http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Plb.+16.34)

59.
60.
61. Green, *Alexander to Actium: The Historical Evolution of the
    Hellenstic Age*, 309

62.
63.
64.
65. [Livy](../Page/#Livy.md "wikilink")
    [33.4](http://mcadams.posc.mu.edu/txt/ah/Livy/Livy34.html)

66. [Livy](../Page/#Livy.md "wikilink")
    [33.5](http://mcadams.posc.mu.edu/txt/ah/Livy/Livy34.html)

67.
68. [Livy](../Page/#Livy.md "wikilink")
    [33.11](http://mcadams.posc.mu.edu/txt/ah/Livy/Livy34.html)

69. [Livy](../Page/#Livy.md "wikilink")
    [33.30](http://mcadams.posc.mu.edu/txt/ah/Livy/Livy34.html)

70.
71.