**克雷格黑德縣**（）是[美國](../Page/美國.md "wikilink")[阿肯色州東北部的一個縣](../Page/阿肯色州.md "wikilink")，東北鄰[密蘇里州](../Page/密蘇里州.md "wikilink")。面積1,847平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口82,148人。縣治[萊克城和](../Page/萊克城_\(阿肯色州\).md "wikilink")[瓊斯伯勒](../Page/瓊斯伯勒_\(阿肯色州\).md "wikilink")。

成立於1859年2月19日。縣名紀念[州參議員湯瑪斯](../Page/阿肯色州州參議院.md "wikilink")·克雷格黑德（Thomas
Craighead）。\[1\]本縣為一[禁酒的縣](../Page/禁酒.md "wikilink")。

## 参考文献

[Category:阿肯色州行政區劃](../Category/阿肯色州行政區劃.md "wikilink")
[克雷格黑德縣_(阿肯色州)](../Category/克雷格黑德縣_\(阿肯色州\).md "wikilink")
[Category:瓊斯伯勒都會區](../Category/瓊斯伯勒都會區.md "wikilink")
[Category:1859年阿肯色州建立](../Category/1859年阿肯色州建立.md "wikilink")

1.  <http://local.arkansas.gov/local.php?agency=Craighead%20County>