[Créteil_map.svg](https://zh.wikipedia.org/wiki/File:Créteil_map.svg "fig:Créteil_map.svg")
**克雷泰伊**（[法语](../Page/法语.md "wikilink")：****），又名“克雷特伊”，是[法国](../Page/法国.md "wikilink")[法兰西岛大区](../Page/法兰西岛大区.md "wikilink")[马恩河谷省的省会城市](../Page/马恩河谷省.md "wikilink")，位于[巴黎東南部郊区](../Page/巴黎.md "wikilink")，面积11.43平方公里，人口82,154人（1999年）。18世紀初，這裡開始建設住宅。在歷史上采石業曾十分興盛。[巴黎地鐵8號線在這裡有](../Page/巴黎地鐵8號線.md "wikilink")4個車站。

## 友好城市

  - [苏格兰](../Page/苏格兰.md "wikilink")[福尔柯克](../Page/福尔柯克.md "wikilink")

  - [以色列Kiryat](../Page/以色列.md "wikilink") Yam

## 参考文献

## 外部链接

  - [Official website](http://www.ville-creteil.fr/)
  - [Paris XII- Val-de-Marne University located in
    Créteil](http://www.univ-paris12.fr/)

[C](../Category/马恩河谷省市镇.md "wikilink")