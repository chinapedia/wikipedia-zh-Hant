<table>
<thead>
<tr class="header">
<th><p>克雷西會戰</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Battle_of_crecy_froissart.jpg" title="fig:Battle_of_crecy_froissart.jpg">Battle_of_crecy_froissart.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><strong>日期</strong>: 1346年8月26日</p></td>
</tr>
<tr class="odd">
<td><p><strong>地點</strong>: <a href="../Page/法国.md" title="wikilink">法国</a><a href="../Page/加萊.md" title="wikilink">加萊南方</a></p></td>
</tr>
<tr class="even">
<td><p><strong>結果</strong>: 英国人取得決定性勝利</p></td>
</tr>
<tr class="odd">
<td><p>交戰各方</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Royal_Arms_of_England_(1340-1367).svg" title="fig:Royal_Arms_of_England_(1340-1367).svg">Royal_Arms_of_England_(1340-1367).svg</a> <a href="../Page/英格兰王国.md" title="wikilink">英格兰王国</a><br />
<a href="../Page/丹麦.md" title="wikilink">丹麦和</a><a href="../Page/神圣罗马帝国.md" title="wikilink">神圣罗马帝国的同盟</a><a href="../Page/骑士.md" title="wikilink">骑士</a></p></td>
</tr>
<tr class="odd">
<td><p>指揮官</p></td>
</tr>
<tr class="even">
<td><p>commander1 width="50%"|<a href="https://zh.wikipedia.org/wiki/File:Royal_Arms_of_England_(1340-1367).svg" title="fig:Royal_Arms_of_England_(1340-1367).svg">Royal_Arms_of_England_(1340-1367).svg</a> <a href="../Page/愛德華三世_(英格蘭).md" title="wikilink">愛德華三世</a><br />
<a href="https://zh.wikipedia.org/wiki/File:Royal_Arms_of_England_(1340-1367).svg" title="fig:Royal_Arms_of_England_(1340-1367).svg">Royal_Arms_of_England_(1340-1367).svg</a> <a href="../Page/黑太子爱德华.md" title="wikilink">黑太子爱德华</a></p></td>
</tr>
<tr class="odd">
<td><p>兵力</p></td>
</tr>
<tr class="even">
<td><p>12,000人</p></td>
</tr>
<tr class="odd">
<td><p>傷亡</p></td>
</tr>
<tr class="even">
<td><p>150-1000人</p></td>
</tr>
</tbody>
</table>

**克雷西會戰**（,
），是英法[百年戰爭中的一場戰役](../Page/百年戰爭.md "wikilink")，發生於1346年8月26日，英軍以[英格蘭長弓大破法軍](../Page/英格蘭長弓.md "wikilink")[重騎兵與](../Page/重騎兵.md "wikilink")[弩兵](../Page/弩.md "wikilink")。

## 背景

1337年，[英法百年戰爭爆發](../Page/英法百年戰爭.md "wikilink")。第一場會戰是[斯鲁伊斯海战](../Page/斯鲁伊斯海战.md "wikilink")，發生於1340年6月23日，[英格蘭獲勝](../Page/英格蘭.md "wikilink")，並且獲得[多佛海峽的控制權](../Page/多佛海峽.md "wikilink")。之後，英格蘭國王[愛德華三世企圖藉道](../Page/愛德華三世_\(英格蘭\).md "wikilink")[法蘭德斯入侵法國](../Page/法蘭德斯.md "wikilink")，但由於財政上的困難，加上同盟間的不穩定，使得愛德華三世無力持續作戰。1340年10月30日，愛德華回到了英格蘭。1346年7月，為了支援[法蘭德斯和](../Page/法蘭德斯.md "wikilink")[布列塔尼處境危險的盟軍](../Page/布列塔尼.md "wikilink")，愛德華三世率部從[樸次茅斯出發](../Page/樸次茅斯.md "wikilink")，渡海抵達[法國北岸](../Page/法國.md "wikilink")。在一個月左右的時間內，他穿過法國西北部地區，來到[低地國家背後](../Page/低地國家.md "wikilink")。法王[腓力六世率領的軍隊比英軍強大的多](../Page/腓力六世.md "wikilink")，正緊緊的追趕著前面的敵人。愛德華三世於8月24日在[布朗什塔克淺灘突破法軍的防守](../Page/布朗什塔克戰役.md "wikilink")，經過兩日後抵達克雷西周邊進行布陣。

## 兵力配置

雙方的兵力對比相當的懸殊。當時的法軍兵力大約30,000至40,000名[重騎兵](../Page/重騎兵.md "wikilink")，並且僱用六千名左右的[熱那亞](../Page/熱那亞.md "wikilink")[十字弓手](../Page/十字弓.md "wikilink")[傭兵](../Page/傭兵.md "wikilink")，而英軍只有約7000名[長弓兵](../Page/長弓.md "wikilink")、五千名騎士。

由於英軍明白他們在數量上處於劣勢，故愛德華三世精心的佈置了戰場，希望將自己在兵力上的劣勢，能夠配合地形而安排的[陣形予以補足](../Page/陣形.md "wikilink")。他將其部隊平均的分成了三個部分。右翼部隊部署在靠近克雷西的地方，並且有一條[河流作爲其屏障](../Page/河流.md "wikilink")；左翼部隊則布陣於[瓦迪庫而特村的前方](../Page/瓦迪庫而特.md "wikilink")，有[樹林和](../Page/樹林.md "wikilink")[步兵挖掘的](../Page/步兵.md "wikilink")[防禦工事作爲掩護](../Page/防禦工事.md "wikilink")；右翼則由[黑太子愛德華率領](../Page/黑太子愛德華.md "wikilink")；至於愛德華三世則率軍親自坐陣中央。整個布陣情況呈現出一個指向東面的倒V字型陣式。而在每個部分的中央，則是由大約一千名騎士所組成的[方陣](../Page/方陣.md "wikilink")。

## 戰鬥過程

擺陣完畢後，愛德華騎馬緩行視察部隊，鼓勵人員努力作戰。他們奉命吃[午餐](../Page/午餐.md "wikilink")，當吃飽休息完之後，再列成陣勢，大家坐在地上，將[頭盔和弓弩放在前面](../Page/頭盔.md "wikilink")，所以，當法軍到達前，部隊士兵們的體力和精神都很飽滿。

8月26日下午6時左右，法軍排成冗长的一路行軍縱隊到達了戰場。沒有任何的偵察和警戒就亂哄哄的開始進攻。腓力六世當時還想將部隊集結一下再發動攻擊。於是將十字弩手調到了前面。但是那些妄自尊大、目空一切的法軍騎士們，卻不聽從指揮命令，在弩兵行動後不久就開始進攻。

此時，[熱那亞十字弩手紀律嚴明](../Page/熱那亞.md "wikilink")，他們排成了整齊的隊伍，在距離150碼的地方停了下來，開始向英軍進行射擊。但是，由於英軍位於一個坡地上，同時由於十字弩本身的問題，導致多數的箭都沒有射中目標，對英軍來說可以說是毫髮無傷。於是，熱那亞的十字弩手們又再次向前移動，打算將距離再拉近些。此時，英軍的長弓手開始發射弓箭。

英軍的射擊射出鋪天蓋地的箭雨，使得熱那亞十字弩手無法承受，紛紛向後潰散。但後方的法軍騎兵卻已經等的不耐煩。他們衝上前去，並與熱那亞弓十字弩手發生自相踐踏，形成一片混亂。法軍接下來長達幾小時的戰鬥中，不斷進行一次又一次的衝鋒、突擊，但每一次都被英軍的騎兵分隊化解，皆以失敗收場。戰役結束前，法王腓力六世雖然還有部隊，但是這些七零八落且一身疲憊的散兵，已經不可能挽回敗局。[1](http://www.gamebase.com.tw/forum/content.html?sno=80992255)

## 戰局結果

8月27日上午，愛德華准許他的部隊解散去搜括法軍死者身上的財物。山坡下躺著無數的法軍屍體。在山下的屍體中有1524位勳爵和騎士、約15000名左右的騎兵、十字弩兵和步兵的屍體。其中這時候英軍才發現被殺的人當中，甚至包含[波希米亞國王](../Page/波希米亞國王.md "wikilink")[约翰一世](../Page/约翰一世_\(波希米亚\).md "wikilink")、[洛林公爵](../Page/洛林公爵.md "wikilink")、[法蘭德斯以下的十位](../Page/法蘭德斯.md "wikilink")[伯爵](../Page/伯爵.md "wikilink")。同時還有成千上萬匹的馬做了陪葬。而英軍則傷亡約數百人左右。不過，英王愛德華卻對如此多的貴族騎士陣亡感到十分的後悔。因為這些法軍的死亡，讓他們損失了一大筆的可以到手的[贖金](../Page/贖金.md "wikilink")。

## 影響

克雷西戰役中，英格蘭長弓發揮了重要作用。英軍擊敗了約3倍於自己的敵人，得到相當大的勝利。英國人取勝的關鍵其實是讓下馬作戰的騎兵，與弓箭兵互相掩護，與騎在馬上的騎兵緊密結合，從而把投射兵器的殺傷力、防御的耐久力與機動突擊性靈活的結合起來。

而克雷西戰役後，英軍更加穩固地控制著[加萊](../Page/加萊.md "wikilink")，一直到1558年為止，其間長達二百多年之久。

## 參見

  - [百年戰爭](../Page/百年戰爭.md "wikilink")
  - [普瓦捷戰役](../Page/普瓦捷戰役.md "wikilink")
  - [阿金庫爾戰役](../Page/阿金庫爾戰役.md "wikilink")

## 參考資料

  - 《西洋世界軍事史-卷一：從沙拉米斯會戰到李班多會戰(下)》，作者：[富勒](../Page/富勒.md "wikilink")(J.F.C.
    Fuller)，譯者：[鈕先鍾](../Page/鈕先鍾.md "wikilink")，1996年，麥田出版，頁608\~621
  - [世紀帝國二討論區 -
    遊戲基地](http://www.gamebase.com.tw/forum/content.html?sno=80992255)
  - [古堡殘劍Return to The Middle Age in the dream -
    路西法地獄](http://www.lucifer.tw/swords/Aticls/longbow.htm)

[Category:英國戰役](../Category/英國戰役.md "wikilink")
[Category:法國戰役](../Category/法國戰役.md "wikilink")
[Category:百年戰爭戰役](../Category/百年戰爭戰役.md "wikilink")