[SEM_blood_cells.jpg](https://zh.wikipedia.org/wiki/File:SEM_blood_cells.jpg "fig:SEM_blood_cells.jpg")在[電子顯微鏡下所呈現的](../Page/電子顯微鏡.md "wikilink")[影像](../Page/影像.md "wikilink")。在视野中可见[红血球](../Page/红血球.md "wikilink")、[淋巴细胞](../Page/淋巴细胞.md "wikilink")、[单核细胞](../Page/单核细胞.md "wikilink")、[中性粒细胞](../Page/中性粒细胞.md "wikilink")，和很多[血小板](../Page/血小板.md "wikilink")。\]\]

**免疫**（），指[生物机体识别和排除](../Page/生物.md "wikilink")[抗原物质的一种保护性反应](../Page/抗原.md "wikilink")。其中包括[特异性免疫](../Page/特异性免疫.md "wikilink")（後天免疫系統）与[非特异性免疫](../Page/非特异性免疫.md "wikilink")（先天免疫系統）。

## 词源

### 中国

“免疫”一词，最早见于[中国](../Page/中国.md "wikilink")[明代医书](../Page/明代.md "wikilink")《[免疫类方](../Page/免疫类方.md "wikilink")》，指的是“免除疫疠”，也就是防治传染病的意思。

### 外国

2000多年前，人类就发现曾在瘟疫流行中患过某种疾病的人，对这种疾病的再次感染具有抵抗力，称之为“免疫”。免疫（）这个词是来自罗马时代描述免除个人劳役或对国家义务的一个拉丁文词“”\[1\]。

## 表现

1.  免疫防御（immune
    defense）防止外界病原体的入侵及清除已入侵[病原体](../Page/病原体.md "wikilink")（如[细菌](../Page/细菌.md "wikilink")、[病毒](../Page/病毒.md "wikilink")、[真菌](../Page/真菌.md "wikilink")、[支原体](../Page/支原体.md "wikilink")、[衣原体](../Page/衣原体.md "wikilink")、[寄生虫等](../Page/寄生虫.md "wikilink")）及其他有害物质。免疫防御功能过低或缺路，可发生免疫缺陷病；但若[应答过强或持续时间过长](../Page/应答.md "wikilink")，则在清除病原体的同时，也可导致机体的组织损伤或功能异常，发生[超敏反应](../Page/超敏反应.md "wikilink")。\[2\]
2.  免疫监视（immune
    surveillance）：随时发现和清除体内出现的“非己”成分；如由基因突变而发生的[肿瘤细胞以及衰老](../Page/肿瘤细胞.md "wikilink")、[凋亡细胞](../Page/凋亡.md "wikilink")。[免疫监视功能低下](../Page/免疫监视.md "wikilink")，可能导致[肿瘤的发生及持续性病毒感染](../Page/肿瘤.md "wikilink")。\[3\]
3.  免疫自身稳定（immune
    homeeostasis）：通过自身[免疫耐受和](../Page/免疫耐受.md "wikilink")[免疫调节两种主要的机制来达到免疫系统内的稳定](../Page/免疫调节.md "wikilink")。\[4\]

## 种类

  - 种免疫：个体与生俱有，一般为非特异性免疫，如[吞噬细胞的作用](../Page/吞噬细胞.md "wikilink")。
  - 获得性免疫
      - 自动获得性免疫：一般免疫时间长。可待终身，如[麻疹](../Page/麻疹.md "wikilink")、[天花](../Page/天花.md "wikilink")、[痄腮](../Page/痄腮.md "wikilink")。
          - 自然获得：有被[天花病毒感染發病史的人](../Page/天花.md "wikilink")，一般不会再次感染。
          - 人工获得：如种[牛痘免疫天花](../Page/牛痘.md "wikilink")
      - 被动获得性免疫：免疫时间短，人工获得时，已较少采用。
          - 自然获得：如婴儿在母体胎盘或初乳中获得的免疫
          - 人工获得：如注射具有免疫力的免疫血清，获得免疫，如治疗蛇毒时注射的血清蛋白。

## 作用

一般机体免疫保护机体受到外部损害，对机体有利，但有时会令机体产生[变态反应](../Page/变态反应.md "wikilink")，被认为是对机体不利的。

## 参考文献

## 参见

  - [免疫系统](../Page/免疫系统.md "wikilink")
  - [炎症](../Page/炎症.md "wikilink")

{{-}}

[免疫系統](../Category/免疫系統.md "wikilink")
[Category:免疫学](../Category/免疫学.md "wikilink")

1.  医学免疫学.第五版.第一页
2.  医学免疫学.第五版.第一页
3.  医学免疫学.第五版.第一页
4.  医学免疫学.第五版.第二页