[2009_dd_day_016.00.JPG](https://zh.wikipedia.org/wiki/File:2009_dd_day_016.00.JPG "fig:2009_dd_day_016.00.JPG")
**免费素食主义**（，又譯**飞根主义**）是一种[反消費主義生活方式](../Page/反消費主義.md "wikilink")，指通过[消费已经或将要被其他人](../Page/消费.md "wikilink")（如[超级市场](../Page/超级市场.md "wikilink")）扔掉的[食物的方法来使人对](../Page/食物.md "wikilink")[环境的影响最小化的行为习惯](../Page/环境.md "wikilink")。如果食物的来源不是免费的，这些[环境保护主义者只食用](../Page/环境保护主义.md "wikilink")[素食](../Page/素食主义.md "wikilink")。

## 词语释意

**免费素食者**的英文是Freegan。这个词来源于“免费”（free）和“[纯素食者](../Page/纯素食主义.md "wikilink")”（vegan）\[1\]，尽管不是所有的免费素食者都是纯素食者，但是[纯素食主义的道德价值观与免费素食者相同](../Page/纯素食主义.md "wikilink")。

免费素食主义运动始于1990年代中期，是[反全球化运动和](../Page/反全球化运动.md "wikilink")[环保运动的产物](../Page/环保运动.md "wikilink")。免费素食主义运动的源头可以追溯到1960年代[美国](../Page/美国.md "wikilink")[舊金山的](../Page/舊金山.md "wikilink")[无政府主义](../Page/无政府主义.md "wikilink")[街头剧院](../Page/街头剧院.md "wikilink")；在那里，[嬉皮士们免费派发救济餐](../Page/嬉皮士.md "wikilink")。\[2\]

有些人使用该词来描述某人只吃免费（如通过垃圾箱或赠送而获得）食物的生活方式，因此这个词有时会被当[贬义词使用](../Page/贬义.md "wikilink")，指一个靠揩油过日子的穷人。

## 行为

一个免费素食主义者可能会向[零售商索要那些将要扔掉的食物](../Page/零售.md "wikilink")，或者从[翻垃圾箱获得食物](../Page/垃圾搜尋.md "wikilink")。这样免费素食主义者就没有对[生产过程中使用的](../Page/生产.md "wikilink")[原料和](../Page/原料.md "wikilink")[能量资源产生额外负担](../Page/能量.md "wikilink")。因为当钱与商品交换时，商品已经退出了生产[消費循環](../Page/消費循環.md "wikilink")。

在许多[发达国家](../Page/发达国家.md "wikilink")，[消费者的质量要求和](../Page/消費者_\(經濟\).md "wikilink")[卫生标准相当之高](../Page/卫生.md "wikilink")，很多食物在「[最佳食用日期](../Page/最佳食用日期.md "wikilink")」之后很长一段时间内仍适合食用。在[温带气候的](../Page/温带气候.md "wikilink")[秋天和](../Page/秋天.md "wikilink")[春天](../Page/春天.md "wikilink")，垃圾箱保持着[电冰箱的](../Page/电冰箱.md "wikilink")[温度时](../Page/温度.md "wikilink")，是實行免费素食主义的最好時間。

尽管免费素食主义者通常崇尚[素食主义或](../Page/素食主义.md "wikilink")[纯素食主义理念](../Page/纯素食主义.md "wikilink")，但因为实用、[道德](../Page/道德.md "wikilink")、[哲学或扔掉的](../Page/哲学.md "wikilink")[肉类比](../Page/肉类.md "wikilink")[蔬菜变质更快等原因](../Page/蔬菜.md "wikilink")，免费素食主义自身并不意味某人尊循一个确定的素食主义饮食行为。

免费素食主义也被用来描述那些仅为[素食负费](../Page/素食.md "wikilink")，但如果可以免费得到也吃非素食。不像前面提到的，这些人不是必须吃那些已经扔掉的食物。而是他们宁可吃那些提供给他们的非素食的食物，否则这些食物将被扔掉。

## 批评

纯素食者经常争辩，许多自称免费素食者的人並不严格遵循仅吃那些会被浪费的动物产品。违反免费素食主义原则的可能范围包括从朋友处免费得到食物，甚至因为價格便宜而購买非素食食物。因此，纯素食主义评论家争论「免费素食主义」对很多人来讲更多的是一个[标签或借口](../Page/标签.md "wikilink")，而不是实际遵循的饮食原则。一些免费素食主义反驳此评论，也有很多自称纯素食主义者的人并不细心的遵从他们的食物原则。

## 相关条目

  - [素食主义](../Page/素食主义.md "wikilink")
  - [纯素食主义](../Page/纯素食主义.md "wikilink")
  - [半素食主义](../Page/半素食主义.md "wikilink")

## 参见

  - [反消費主義](../Page/反消費主義.md "wikilink")
  - [反全球化运动](../Page/反全球化运动.md "wikilink")
  - [時代精神運動](../Page/時代精神運動.md "wikilink")
  - [环境保护主义](../Page/环境保护主义.md "wikilink")
  - [环保运动](../Page/环保运动.md "wikilink")

## 索引

## 外部链接

  - [Freegan.info](http://freegan.info)
  - Freegan discussion list at [Yahoo\!
    Groups](../Page/Yahoo!_Groups.md "wikilink"):
    [scavengeuk](http://groups.yahoo.com/group/scavengeuk)
  - [Why
    Freegan](https://web.archive.org/web/20000620185705/http://www.geocities.com/CapitolHill/Lobby/3609/lib_whyfreegan.html)
  - [Green
    Crow](https://web.archive.org/web/20130206183304/http://green-crow.com/)

[Category:素食主義](../Category/素食主義.md "wikilink")

1.

2.