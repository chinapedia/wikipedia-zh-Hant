為依據臺灣作家[黃春明所撰寫的](../Page/黃春明.md "wikilink")[小說](../Page/小說.md "wikilink")《[兒子的大玩偶](../Page/兒子的大玩偶.md "wikilink")》、《[小琪的那頂帽子](../Page/小琪的那頂帽子.md "wikilink")》和《[蘋果的滋味](../Page/蘋果的滋味.md "wikilink")》為基礎，再進行改編的[集錦電影作品](../Page/集錦電影.md "wikilink")。上映前，雖已通過[行政院新聞局審核](../Page/行政院新聞局.md "wikilink")，卻遭保守派的影評人士運用「[中國影評人協會](../Page/中國影評人協會（台灣影評組織）.md "wikilink")」名義書寫黑函密告[中國國民黨中央文化工作會](../Page/中國國民黨中央文化工作會.md "wikilink")，指稱本片貧窮落後及[違章建築的畫面不妥當](../Page/違章建築.md "wikilink")\[1\]，恐有影響國際形象的疑慮；導致屬於黨營企業的[中央電影公司在未經過導演](../Page/中央電影公司.md "wikilink")[萬仁同意下](../Page/萬仁.md "wikilink")，打算逕自修剪《蘋果的滋味》部份片段\[2\]。為此，萬仁透過《[聯合報](../Page/聯合報.md "wikilink")》記者[楊士琪於報紙上披露後](../Page/楊士琪.md "wikilink")，隨即引發臺灣輿論界一陣譁然，紛紛批評中影的官僚作風，迫使中影放棄刪減影片內容。最終，影片逃脫過被刪剪的命運，一刀未剪通過上映，讓此片的創作理念得以保全，而此事件也經常被戲稱為「削蘋果事件」。
[ROC-GIO_movie_playing_license_to_The_Sandwich_Man_19830816.jpg](https://zh.wikipedia.org/wiki/File:ROC-GIO_movie_playing_license_to_The_Sandwich_Man_19830816.jpg "fig:ROC-GIO_movie_playing_license_to_The_Sandwich_Man_19830816.jpg")電影片准演執照\]\]

上映後，寫實傳達臺灣記憶的電影風格，擄獲大多數觀眾口碑，兼具票房收益。至今，本片也被普遍視為「[臺灣新浪潮電影](../Page/台灣新浪潮電影.md "wikilink")」的開端作品之一，佔有著舉足輕重的地位。

## 概述

故事劇情描述早期臺灣民眾艱困的生活環境，從幾位卑微的小人物所面臨的掙扎及挑戰，圍繞在工作和生活的無奈，傳達出[開發中國家的國民在面臨外來](../Page/開發中國家.md "wikilink")[文明引入的那種心酸和無知](../Page/文明.md "wikilink")，亦赤裸裸看到生命中堅毅不拔的韌性。整個舞臺分別在[嘉義縣](../Page/嘉義縣.md "wikilink")[竹崎鄉和](../Page/竹崎鄉.md "wikilink")[布袋鎮](../Page/布袋鎮.md "wikilink")，以及[臺北市](../Page/臺北市.md "wikilink")[林森北路與](../Page/林森北路.md "wikilink")[南京東路上取景](../Page/南京東路_\(台北市\).md "wikilink")，勾勒出早期[臺灣社會面臨轉型與時代變遷之記憶](../Page/臺灣社會.md "wikilink")\[3\]。

## 劇情簡介

  - 《兒子的大玩偶》
    坤樹為求得一家生活溫飽，從事為劇院做活動廣告的工作，經常在身體前後各掛一張電影[海報](../Page/海報.md "wikilink")，打扮成[小丑遊走在大街小巷](../Page/小丑.md "wikilink")，吸引人們的目光。但是，坤樹這份「三明治廣告人」的工作得不到家人及親友的認同，總為此事爭吵不休。返家後，坤樹立刻卸下工作時的小丑裝扮；但年幼的兒子竟然認不出他，放聲大哭起來。自此，坤樹體認到，在家裡也必須保持著小丑的裝扮……

<!-- end list -->

  - 《小琪的那頂帽子》
    王武雄[退伍後找到一份](../Page/退伍.md "wikilink")[推銷員的工作](../Page/推銷員.md "wikilink")，公司遣派他跟隨林再發到沿海城鎮推銷日本生產的[壓力鍋](../Page/壓力鍋.md "wikilink")，他在小鎮內遇到小女孩李小琪。只是，王武雄最感到不解的是，小琪頭上總是緊扣著那頂[帽子](../Page/帽子.md "wikilink")，讓他覺得非常奇怪。同時，鏡頭另一邊的壓力鍋也發生問題。林再發被爆炸的壓力鍋炸傷，生死未卜；他的懷孕的妻子卻寄來一封信，說到自己[流產的消息](../Page/流產.md "wikilink")，希望他能盡速回家……

<!-- end list -->

  - 《蘋果的滋味》
    江阿發在上班的路途上，不幸被[美國海軍陸戰隊](../Page/美國海軍陸戰隊.md "wikilink")[駐臺軍官格雷](../Page/駐臺美軍.md "wikilink")[上校所駕駛的汽車撞倒](../Page/上校.md "wikilink")，因此撞斷了雙腿，住進[臺北市](../Page/臺北市.md "wikilink")[天母的](../Page/天母.md "wikilink")[美國海軍醫院療養](../Page/美國海軍醫院.md "wikilink")。沒料到，阿發的整個家庭沒有因此陷入困境：格雷上校賠償[新臺幣五萬元現金](../Page/新臺幣.md "wikilink")，又願意帶阿發的[啞女兒到美國](../Page/啞.md "wikilink")[留學](../Page/留學.md "wikilink")。前來探望阿發傷勢的老婆、孩子及朋友，見到他能待在豪華的醫院裏，反而對他被撞之事感到羨慕不已。後來，阿發讓老婆和孩子品嚐格雷上校送來作為禮物的美國[蘋果](../Page/蘋果.md "wikilink")，此刻全家人品嘗到天堂的果實……

## 人物角色

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<thead>
<tr class="header">
<th><p>colspan = "3"| 〈兒子的大玩偶〉<br />
The Sandwich Man</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>角色名稱</p></td>
</tr>
<tr class="even">
<td><p>坤樹</p></td>
</tr>
<tr class="odd">
<td><p>阿珠</p></td>
</tr>
<tr class="even">
<td><p>未具名</p></td>
</tr>
<tr class="odd">
<td><p>阿水伯</p></td>
</tr>
<tr class="even">
<td><p>許阿龍</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table></td>
<td><table>
<thead>
<tr class="header">
<th><p>colspan = "3"| 〈小琪的那頂帽子〉<br />
Vicki's Hat</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>角色名稱</p></td>
</tr>
<tr class="even">
<td><p>未具名</p></td>
</tr>
<tr class="odd">
<td><p>王武雄</p></td>
</tr>
<tr class="even">
<td><p>林再發</p></td>
</tr>
<tr class="odd">
<td><p>李小琪</p></td>
</tr>
<tr class="even">
<td><p>美麗</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table></td>
<td><table>
<thead>
<tr class="header">
<th><p>colspan = "3"| 〈蘋果的滋味〉<br />
The Taste of Apple</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>角色名稱</p></td>
</tr>
<tr class="even">
<td><p>江太太</p></td>
</tr>
<tr class="odd">
<td><p>江阿發</p></td>
</tr>
<tr class="even">
<td><p>江阿吉</p></td>
</tr>
<tr class="odd">
<td><p>江阿松</p></td>
</tr>
<tr class="even">
<td><p>未具名</p></td>
</tr>
<tr class="odd">
<td><p>未具名</p></td>
</tr>
<tr class="even">
<td><p>格雷</p></td>
</tr>
<tr class="odd">
<td><p>未具名</p></td>
</tr>
<tr class="even">
<td><p>啞吧</p></td>
</tr>
<tr class="odd">
<td><p>江阿桃</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

## 獎項

  - [第20届金馬獎](../Page/第20届金馬獎.md "wikilink") (1983年)

<table>
<tbody>
<tr class="odd">
<td><p>獎項</p></td>
<td><p>入圍者</p></td>
<td><p>結果</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金馬獎最佳男配角.md" title="wikilink">最佳男配角</a></p></td>
<td><p><strong><a href="../Page/陳博正.md" title="wikilink">陳博正</a></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/金馬獎最佳改編劇本.md" title="wikilink">最佳改編劇本</a></p></td>
<td><p><strong><a href="../Page/吳念真.md" title="wikilink">吳念真</a></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金馬獎最佳童星.md" title="wikilink">最佳童星</a></p></td>
<td><p><a href="../Page/顏正國.md" title="wikilink">顏正國</a></p></td>
<td></td>
</tr>
</tbody>
</table>

  - [曼海姆影展](../Page/曼海姆影展.md "wikilink")

<!-- end list -->

  -
    「佳作」

## 逸事

《兒子的大玩偶》制作前，當時[小野與](../Page/小野.md "wikilink")[吳念真十分明白審查的規則](../Page/吳念真.md "wikilink")，其劇本必需通過中國國民黨中央文化工作會和中央電影公司內部高級行政人員核可，以企劃案的方式進行[意識形態審查](../Page/意識形態.md "wikilink")。為此，小野在繳付《兒子的大玩偶》的企劃書中，他吹噓「為了闡揚國父（[孫中山](../Page/孫中山.md "wikilink")）生前所強調的『為中國蒼生、為亞洲黃種、為世界人道』的崇高理想，藉著三段市井小人物之間誠摯的愛心為出發，同時引發出民族自尊心和對自己同胞的愛」，才得以通過審核並正式拍攝。但是，本片第三段作品《蘋果的滋味》在試映會後，才讓明顯與原企劃案背道而馳的「謊言」被揭穿，遭受到預期之外的「削蘋果事件」衝擊\[4\]。事件中，整個導火線為《蘋果的滋味》片尾內，江阿發一家七口人津津有味地吃著格雷上校探病送的美國蘋果之畫面，加上影片使用方言超過三分之一的限制，招致諸多非議\[5\]。

《蘋果的滋味》在臺北市[林森北路與](../Page/林森北路.md "wikilink")[南京東路上一片違章建築群取景](../Page/南京東路_\(台北市\).md "wikilink")，這處凌亂不堪的生活環境亦遭「削蘋果事件」牽連，差點面臨立刻拆除房屋和住戶搬遷的命運；事件落幕後，才不了了之。1997年3月4日，此處違章建築群被[臺北市政府拆除成為](../Page/臺北市政府.md "wikilink")[十四號及](../Page/林森公園.md "wikilink")[十五號公園預定地](../Page/康樂公園.md "wikilink")（詳見[14、15號公園反拆遷運動](../Page/14、15號公園反拆遷運動.md "wikilink")），萬仁也出現在場弔念，目睹這片經常出現在自己電影中的違建景觀步入歷史，他形容當時的心情是「感觸良多」\[6\]。

《蘋果的滋味》中的美國海軍醫院，於[駐臺美軍撤離後成為](../Page/駐臺美軍.md "wikilink")[臺北榮民總醫院東院區](../Page/臺北榮民總醫院.md "wikilink")。

《聯合報》記者楊士琪不畏懼強權揭露「削蘋果事件」，引起各輿論界的注意與支持，使此片的創作理念得以保全。因此，楊士琪備受新電影導演們感念於心裏，紛紛大力讚揚她對[臺灣新浪潮電影的推動不遺餘力](../Page/台灣新浪潮電影.md "wikilink")、以及對[臺灣電影文化的貢獻](../Page/臺灣電影.md "wikilink")。1984年，楊士琪不幸因為[氣喘病發作而辭世](../Page/氣喘病.md "wikilink")。1985年，導演[楊德昌在他的電影](../Page/楊德昌.md "wikilink")《[青梅竹馬](../Page/青梅竹馬_\(電影\).md "wikilink")》片頭，特地以中文黑底白字並加註英文字幕寫著「獻給楊士琪，感激她生前給我們的鼓勵。——製作全體同仁致敬」，全長六秒鐘。隨後，楊士琪前友人及電影界與新聞界的25位好友聯合倡議組成「楊士琪紀念獎工作委員會」，決定每年選出一位富有道德勇氣並努力開創新局、提升電影地位的傑出人士，頒給「楊士琪電影紀念獎」以資鼓勵，並彰顯楊士琪生前熱愛電影、勇於創新的精神\[7\]。

引發「削蘋果事件」的中國影評人協會無人出面承認發出黑函，但雙方心結已深，彼此間觀點漸趨尖銳，相互杯葛與批評對方。而且，歷經諸多爭論之後，對該協會更是不滿（李幼新，1986，46-48頁）。

## 相關條目

  - [兒子的大玩偶](../Page/兒子的大玩偶.md "wikilink")
  - [台灣新電影](../Page/台灣新電影.md "wikilink")
  - [中國影評人協會](../Page/中國影評人協會.md "wikilink")

## 參考資料

  - [第二十屆金馬獎得獎名單](https://web.archive.org/web/20070926231107/http://student.wtuc.edu.tw/~1093211020/28.htm)
  - 李幼新（2006年後更名為[李幼鸚鵡鵪鶉](../Page/李幼鸚鵡鵪鶉.md "wikilink")），1986年，《電影、電影人、電影刊物》，台北，[自立晚報](../Page/自立晚報.md "wikilink")。
  - 鄭玩香，2001年，《\[<http://tw.wrs.yahoo.com/_ylt=A8tUzIeGi7lGQyAB5XBr1gt>.;_ylu=X3oDMTFhMDQxNjlwBGNvbG8DZQRsA1dTMQRwb3MDNDMEc2VjA3NyBHZ0aWQDdHcwMTAzXzEyMw--/SIG=13k3l1g6a/EXP=1186651398/\*\*http%3A//thesis.lib.ncu.edu.tw/ETD-db/ETD-search/getfile%3Furn=86125013%26%26filename=86125013.pdf
    戰後台灣電影管理體系之研究（1950～1970）\]》，國立中央大學歷史研究所碩士論文。
  - 張世倫，2001年，《[台灣「新電影」論述形構之歷史分析（1965～2000）](https://web.archive.org/web/20071228155507/http://www.benla.mymailer.com.tw/study/study-21-01.htm)》，國立政治大學新聞研究所碩士論文。
  - 陳儒修，2002年4月13日，《[【異言堂之其他】遙遠的凝視：二十年後重看《兒子的大玩偶》](http://movie.cca.gov.tw/Case/Content.asp?ID=212&Year=2002)》，〈[台灣電影筆記](../Page/台灣電影筆記.md "wikilink")〉
  - 葉月瑜、戴樂為，2002年5月30日，《[【異言堂之歷史篇】重訪新電影：小野、吳念真、王童](http://movie.cca.gov.tw/Case/Content.asp?ID=141)》，〈[台灣電影筆記](../Page/台灣電影筆記.md "wikilink")〉
  - 胡清輝，2005年2月26日，《[華語電影參加國際影展得獎紀錄、重要回顧展（1982～2003）](https://web.archive.org/web/20160414112101/http://cinema.nccu.edu.tw/cinemaV2/lwisdominfo.htm?MID=16)》，〈[台灣電影資料庫](../Page/台灣電影資料庫.md "wikilink")〉

## 外部連結

  - {{@movies|fPcmb6061266|兒子的大玩偶}}

  -
  -
  -
  -
  -
  -
  - [兒子的大玩偶（The Sandwich
    Man）](https://web.archive.org/web/20041119084649/http://cinema.nccu.edu.tw/cinemaV2/film_show.htm?SID=1266)，〈[台灣電影資料庫](../Page/台灣電影資料庫.md "wikilink")〉

  - [兒子的大玩偶（The Sandwich
    Man）](http://movie.gio.gov.tw/ct.asp?xItem=17733&ctNode=38)，〈[臺灣電影網](../Page/臺灣電影網.md "wikilink")〉

[3](../Category/1980年代台灣電影作品.md "wikilink")
[Category:台灣劇情片](../Category/台灣劇情片.md "wikilink")
[Category:侯孝賢電影](../Category/侯孝賢電影.md "wikilink")
[Category:台灣小說改編電影](../Category/台灣小說改編電影.md "wikilink")
[Category:中影公司電影](../Category/中影公司電影.md "wikilink")
[Category:臺灣勞工題材作品](../Category/臺灣勞工題材作品.md "wikilink")

1.  [萬仁：政治是虛假的，社會才是真實的。](http://www.newtaiwan.com.tw/bulletinview.jsp?period=355&bulletinid=11982)
    ，〈[新台灣新聞周刊](../Page/新台灣新聞周刊.md "wikilink")〉
2.  [台灣電影史料庫](http://cinema.nccu.edu.tw/cinemaV2/twfilm_s_list.htm?KEYWORD=%A8%E0%A4l%AA%BA%A4j%AA%B1%B0%B8&DATE=&Login=%ACd%B8%DF)，〈[台灣電影資料庫](../Page/台灣電影資料庫.md "wikilink")〉
3.  [〈兒子的大玩偶〉小說與電影的比較](http://www.shs.edu.tw/works/essay/2008/10/2008103115215560.pdf)，[嘉義女中](../Page/嘉義女中.md "wikilink")3年7班
4.  小野〈小小撒個謊：新電影傳奇〉。[楊澤編](../Page/楊澤.md "wikilink")，1999年，《狂飆八O：記錄一個集體發聲的年代》，臺北：[時報文化](../Page/時報文化.md "wikilink")，第132—140頁
5.  蔡明燁，《印象之旅：台灣電影、戲劇和景觀藝術的口述歷史》，2006年。
6.  1997年3月5日，《中國時報》，第22版
7.  [【電影新訊】 楊士琪紀念協會將成立](http://funscreen.com.tw/TaiwanMade.asp?period=)
    ，[國立中央大學](../Page/國立中央大學.md "wikilink")