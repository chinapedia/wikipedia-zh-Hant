{{ multiple image | align = right | direction = vertical | width = 220 |
image1 = Immigration Tower (revised).jpg | caption1 = 入境事務大樓外貌 | image2
= Immigration Tower Entrnace 201808.jpg | caption2 =
[行人天橋入口](../Page/行人天橋.md "wikilink") | image3 =
Immigration Tower access 201808.jpg | caption3 = 大樓通道 | image4 =
Immigration Tower Signage 201605.jpg | caption4 = 部門指示 | image5 = HK Wan
Chai Immagration Bldg Floors The 38th floor Lift Hall.JPG | caption5 =
38樓的轉[升降機指引](../Page/升降機.md "wikilink") }}
**入境事務大樓**（[英語](../Page/英語.md "wikilink")：**Immigration
Tower**），於[香港主權移交前稱為](../Page/香港主權移交.md "wikilink")**人民入境事務大樓**，原名為**灣仔政府綜合大樓第二座**（另兩座分別為[稅務大樓及](../Page/稅務大樓.md "wikilink")[灣仔政府大樓](../Page/灣仔政府大樓.md "wikilink")），位於[香港](../Page/香港.md "wikilink")[灣仔](../Page/灣仔.md "wikilink")[告士打道](../Page/告士打道.md "wikilink")7號，是[香港島島上其中一座政府辦公大樓](../Page/香港島.md "wikilink")，樓高177米，共49層，鄰近[維多利亞港及](../Page/維多利亞港.md "wikilink")[香港會議展覽中心](../Page/香港會議展覽中心.md "wikilink")。大樓內有[入境事務處總部](../Page/入境事務處.md "wikilink")、[創新科技署](../Page/創新科技署.md "wikilink")、[審計署](../Page/審計署_\(香港\).md "wikilink")、[內幕交易審裁處](../Page/內幕交易審裁處.md "wikilink")、[庫務署](../Page/庫務署.md "wikilink")、[運輸署及](../Page/運輸署.md "wikilink")[水務署](../Page/水務署.md "wikilink")。入境處大樓的二樓連接[灣仔行人天橋系統](../Page/灣仔北行人平台.md "wikilink")，連接[灣仔站及鄰近的商業大廈](../Page/灣仔站.md "wikilink")。

2008年，[財政司司長](../Page/財政司司長.md "wikilink")[曾俊華在](../Page/曾俊華.md "wikilink")[財政預算案中披露](../Page/2008年至2009年度香港政府財政預算案.md "wikilink")，研究將佔地1.3[公頃的灣仔海旁](../Page/公頃.md "wikilink")3座政府大樓搬遷，騰出的土地將用以興建甲級[寫字樓](../Page/寫字樓.md "wikilink")。2015年第二季起政府部門將分階段搬遷至[啟德](../Page/啟德發展區.md "wikilink")[工業貿易大樓](../Page/工業貿易大樓.md "wikilink")、[西九龍政府合署](../Page/西九龍政府合署.md "wikilink")、[長沙灣](../Page/長沙灣.md "wikilink")[庫務大樓](../Page/庫務大樓.md "wikilink")、啟德的新税務大樓，以及[將軍澳南的新政府大樓](../Page/將軍澳南.md "wikilink")。當中**入境事務處**則遷往[將軍澳南](../Page/將軍澳南.md "wikilink")，預計最快2023年完成遷出計劃。

## 歷史

灣仔政府綜合大樓第二座於1989年下半年落成，當時人民入境事務處位於[九龍](../Page/九龍.md "wikilink")[尖沙咀冠華中心的總部於](../Page/尖沙咀.md "wikilink")1990年1月22日搬遷入該大樓並正式啟用。1992年，該大樓改名為人民入境事務大樓，至1997年因部門改名而將大樓改為現名。

### 1999年颱風破壞大樓外牆

1999年9月16日[颱風約克襲港](../Page/颱風約克.md "wikilink")，[香港天文台曾懸掛](../Page/香港天文台.md "wikilink")[十號颶風信號](../Page/十號颶風信號.md "wikilink")，入境事務大樓的玻璃外牆被吹倒，以致很多政府文件被吹至地上。

### 2000年縱火案

2000年8月2日，大批聲稱擁有居港權的中國人帶著易燃液體和[打火機到入境事務大樓要求與官員見面](../Page/打火機.md "wikilink")，並威脅[自焚](../Page/自焚.md "wikilink")，期間[施君龍等人在現場縱火](../Page/施君龍.md "wikilink")，事件中高級入境事務主任[梁錦光及爭取居港權人士](../Page/梁錦光.md "wikilink")[林小星被燒死](../Page/林小星.md "wikilink")。

2002年2月首被告施君龍被判[謀殺及](../Page/謀殺.md "wikilink")[縱火罪名成立](../Page/縱火.md "wikilink")，判處[終身監禁](../Page/終身監禁.md "wikilink")，另外6人彭漢坤、傅模、林興鑾、楊義坪、楊義炎及周洪川，則因誤殺及縱火罪判監12至13年。7人其後[上訴](../Page/上訴.md "wikilink")，當中施君龍對謀殺罪判決的上訴得直，被上訴庭改判[誤殺罪](../Page/誤殺.md "wikilink")，監禁14年。7人仍然不服判決，再上訴至[香港終審法院](../Page/香港終審法院.md "wikilink")，由於原審法官錯誤引導[陪審團](../Page/陪審團.md "wikilink")，7名被告獲撤銷所有控罪，發還高等法院[重審](../Page/重審.md "wikilink")。2005年6月16日，施君龍等人在高等法院承認基於[嚴重疏忽誤殺梁錦光和林小星](../Page/嚴重疏忽.md "wikilink")，各人因認罪獲得最少三分一的減刑，施被判入獄8年，其餘6名被告判囚7年4個月；至於縱火罪則不予起訴。

## 樓層

### 二樓　查詢及聯絡組

  - 查詢及表格
  - 收發處

### 三樓　外籍家庭傭工組

  - 續約申請／領取延期逗留標籤
  - 轉換僱主申請
  - 領取入境簽證

### 四樓　旅行證件及國籍（申請）組

  - 申請香港特別行政區護照
  - 有關中國國籍事宜的申請

### 四樓　旅行證件（簽發）組

  - 領取香港特別行政區護照／
  - 簽證身分書

### 五樓　延期逗留組

  - 居港人士／訪港人士
  - 延期逗留／加蓋簽註

### 六樓　優秀人才及內地居民組

  - 輸入內地人才計劃
  - 補充勞工計劃
  - 居港內地居民
  - 優秀人才入境計劃
  - 非本地畢業生留港／回港就業安排

### 六樓　居留權證明書組

  - 入境許可證
  - 居留權證明書（海外申請／基因測試）
  - 輸入中國籍香港永久性居民第二代計劃

### 七樓　其他簽證及入境許可組

  - 受養人及學生簽證／入境許可證
  - 台灣華籍居民來港旅遊入境許可證
  - 亞太經合組織商務旅遊證
  - 香港特別行政區旅遊通行證
  - 資本投資者入境計劃

### 七樓　e- 道登記處

### 八樓　人事登記處—港島辦事處

  - 身分證登記／補領
  - 登記事項證明書／豁免登記證明書
  - 領取身分證

## 交通

<div class="NavFrame collapsed" style="color: black; background: YELLOW; margin: 0 auto; padding: 0 10px">

<div class="NavHead" style="background: YELLOW; margin: 0 auto; padding: 0 10px">

**交通路線列表**

</div>

<div class="NavContent" style="background: YELLOW; margin: 0 auto; padding: 0 10px">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[灣仔站A](../Page/灣仔站.md "wikilink")5出口

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

</div>

</div>

## 軼事

於入境事務大樓內，有一道雙重上鎖的幾吋厚夾萬門，鐵門後是記錄了138年來，香港人的生、死及婚姻紀錄的檔案保險庫。檔案保險庫只有不足20人獲授權內進。\[1\]

## 註腳

## 外部連結

  - [入境事務大樓地圖](http://www.centamap.com/scripts/centamap2.asp?lg=B5&cx=835813&cy=815686&zm=3&mx=835813&my=815686&ms=2&sx=&sy=&ss=0&sl=&vm=&lb=&ly=)

[Category:香港政府部門辦公大樓](../Category/香港政府部門辦公大樓.md "wikilink")
[Category:入境事務處](../Category/入境事務處.md "wikilink")
[Category:灣仔北](../Category/灣仔北.md "wikilink")
[Category:1990年完工建築物](../Category/1990年完工建築物.md "wikilink")
[Category:150米至199米高的摩天大樓](../Category/150米至199米高的摩天大樓.md "wikilink")

1.  [港人生死冊藏138年 入境處首揭神秘面紗
    《明報》 2011年10月9日](http://hk.news.yahoo.com/港人生死冊藏138年-入境處首揭神秘面紗-014452294.html)