**入聲**（checked tone、entering
tone）是[音韻學之概念](../Page/音韻學.md "wikilink")，包括**入聲韻**及**入聲調**。在一般行文中，此二概念往往都混同、省稱為入聲。

入聲韻又稱**促聲韻**，是指傳承自[古代漢語之一類](../Page/古代漢語.md "wikilink")[音節結構](../Page/音節結構.md "wikilink")，其[韻尾以](../Page/韻尾.md "wikilink")[塞音快速結束](../Page/塞音.md "wikilink")，再[無聲除阻](../Page/無聲除阻.md "wikilink")，例如「谷」（[南京話](../Page/南京話.md "wikilink")：gu<sup>5入聲</sup>\[kuʔ
˥\]，[廣州話](../Page/廣州話.md "wikilink")：guk<sup>1上陰入</sup>）相對於「菇」（南京話：gu<sup>1陰平</sup>\[ku
˧˩\]，廣州話：gu<sup>1陰平</sup>）。入聲字是指音節有此類結構之字，此類字之[聲調在](../Page/聲調.md "wikilink")[隋朝](../Page/隋朝.md "wikilink")[韻書](../Page/韻書.md "wikilink")《[切韻](../Page/切韻.md "wikilink")》至[宋朝韻書](../Page/宋朝.md "wikilink")《[廣韻](../Page/廣韻.md "wikilink")》都屬於**入聲調類**，其**入聲聲調**是短而急促。在該時代，入聲都穩定並完整存在於中原語言之中，擁有-p,-t,-k三種輔音韻尾。入聲調根據傳統音韻學屬於「[四聲](../Page/四聲.md "wikilink")」之一，入聲調和非入聲之差別更多來自音質短促而非高低音[調值](../Page/調值.md "wikilink")\[1\]。由於當時之具體調值已經不可考，故無法得知入聲與其他聲調在當時是否擁有相同調值。

中古漢語入聲演變成多種[漢語變體中之入聲調類](../Page/漢語變體.md "wikilink")，各種漢語之入聲調值或有不同，但調型一般都是短促而急收藏\[2\]。在現代漢語中，有不少保留入聲；例如，[粵語及](../Page/粵語.md "wikilink")[客家語完整保留輔音韻尾以及入聲聲調](../Page/客家語.md "wikilink")；[江淮官話已經丟失輔音韻尾](../Page/江淮官話.md "wikilink")，但擁有區別於其它聲調的急促聲調。

## 存在情況

入聲在中國各地的發展情況及表現形式各不相同。[粵語](../Page/粵語.md "wikilink")、[客家語](../Page/客家語.md "wikilink")、[閩南語](../Page/閩南語.md "wikilink")、[赣语](../Page/赣语.md "wikilink")、[壯語](../Page/壯語.md "wikilink")、[日语](../Page/日语.md "wikilink")、[朝鲜语](../Page/朝鲜语.md "wikilink")、[越南語以及](../Page/越南語.md "wikilink")[漢藏語系的其他一些語言中](../Page/漢藏語系.md "wikilink")，入聲字音節以[輔音韻尾](../Page/輔音.md "wikilink")（多為\[p̚\]、\[t̚\]、\[k̚\]）作結，發出明顯的短而急促的[子音](../Page/子音.md "wikilink")，使音節聽起來有一種急促閉塞的頓挫感。

[湘語](../Page/湘語.md "wikilink")、[吳語](../Page/吳語.md "wikilink")、[江淮官话](../Page/江淮官话.md "wikilink")([淮语](../Page/淮语.md "wikilink"))、[闽东语](../Page/闽东语.md "wikilink")、部分[西南官話](../Page/西南官話.md "wikilink")（[岷江话](../Page/岷江话.md "wikilink")）以及部分[晉語也保留入声](../Page/晉語.md "wikilink")，但只帶一個弱喉塞韻尾，或依靠[紧元音韵母来保持音节的頓挫感](../Page/元音松紧对立.md "wikilink")（如[四川](../Page/四川话.md "wikilink")[宜宾话](../Page/宜宾话.md "wikilink")）。大部分的[官話方言](../Page/官話方言.md "wikilink")（除江淮官话以外）中入聲已經不復存在，中原官话、冀鲁官话、胶辽官话的少数方言中尽管保留了入声，却已经變成[舒聲韻](../Page/舒聲韻.md "wikilink")，失去了其原有的促聲韻特点。但是它是何時消失，历来都有不同观点。

漢字大概於[隋](../Page/隋朝.md "wikilink")、[唐時代傳入](../Page/唐朝.md "wikilink")[日本](../Page/日本.md "wikilink")，當時的漢語具有入聲，因而[日語保存入聲的痕跡至今](../Page/日語.md "wikilink")，但日语音系中无闭音节，因而一漢字被分拆成兩音節，其破音音尾已开音节化獨立成另一個音節［通常為ka行、ta行、wa行（
→  →  → ）的音節］。

## 入聲字表

  - **入聲，一屋\[-uk\]** 　　

屋木竹目服福祿熟谷肉咒鹿腹菊陸軸逐牧伏宿讀犢瀆牘櫝黷轂複粥肅育六縮哭幅斛戮僕畜蓄叔淑菽獨卡馥沐速祝麓鏃蹙築穆睦啄覆鶩禿撲鬻輻瀑竺簇暴掬濮鬱矗複塾樸蹴煜謖碌毓舳柚蝠轆夙蝮匐觫囿苜茯髑副孰穀
　　

  - **入聲，二沃\[-uuk\]** 　　

沃俗玉足曲粟燭屬錄辱獄綠毒局欲束鵠蜀促觸續督贖浴酷矚躅褥旭欲淥逯告僕 　　

  - **入聲，三覺\[-eok\]** 　　

覺角桷較岳樂捉朔數卓汲琢剝趵爆駁邈雹璞樸確濁擢鐲濯幄喔藥握搦學 　　

  - **入聲，四質\[-it\]** 　　

質日筆出室實疾術一乙壹吉秩密率律逸佚失漆栗畢恤蜜橘溢瑟膝匹黜弼七叱卒虱悉謐軼詰戌佶櫛昵窒必侄蛭泌秫蟀嫉唧怵帥聿郅桎茁汨尼蒺 　　

  - **入聲，五物\[-ot\]** 　　

物佛拂屈鬱乞掘訖吃紱弗詘崛勿熨厥迄不屹芴倔尉蔚 　　

  - **入聲，六月\[-jat\]** 　　

月骨發闕越謁沒伐罰卒竭窟笏鉞歇突忽勃蹶筏厥蕨掘閥訥歿粵悖兀碣猝樾羯汨咄渤凸滑孛核餑垡閼堀曰訐 　　

  - **入聲，七曷\[-at\]** 　　

曷達末闊活缽脫奪褐割沫拔葛渴撥豁括聒抹秣遏撻薩掇喝跋獺撮剌潑斡捋襪適咄妲 　　

  - **入聲，八黠\[-aet\]** 　　

黠劄拔猾八察殺刹軋刖戛秸嘎瞎刮刷滑 　　

  - **入聲，九屑\[-et\]** 　　

屑節雪絕列烈結穴說血舌潔別裂熱決鐵滅折拙切悅轍訣泄咽噎傑徹別哲設劣碣掣譎竊綴閱抉挈捩楔蹩褻蔑捏竭契癤涅頡擷撤跌蔑浙澈蛭揭啜輟迭呐侄冽掇批橇 　　

  - **入聲，十藥\[-ak\]** 　　

藥薄惡略作樂落閣鶴爵若約腳雀幕洛壑索郭博錯躍若縛酌托削鐸灼鑿卻絡鵲度諾橐漠鑰著虐掠獲泊搏勺酪謔廓綽霍爍莫鑠繳諤鄂亳恪箔攫涸瘧郝駱膜粕礴拓蠖鱷格昨柝摸貉愕柞寞膊魄烙焯厝噩澤矍各獵昔芍踱迮
　　

  - **入聲，十一陌\[-eak\]** 　　

陌石客白澤伯跡宅席策碧籍格役帛戟璧驛麥額柏魄積脈夕液冊尺隙逆畫百辟赤易革脊獲翮屐適劇磧隔益柵窄核擲責惜僻癖辟掖腋釋舶拍擇摘射斥弈奕迫疫譯昔瘠赫炙謫虢臘碩螫藉翟亦鬲骼鯽借嘖蜴幗席貊汐摭咋嚇剌百莫蟈繹霸霹
　　

  - **入聲，十二錫\[-ek\]** 　　

錫壁曆櫪擊績笛敵滴鏑檄激寂翟逖糴析晰溺覓摘狄荻戚滌的吃霹瀝惕踢剔礫櫟適嫡鬩覡淅晰吊霓倜 　　

  - **入聲，十三職\[-jik\]** 　　

職國德食蝕色力翼墨極息直得北黑側飾賊刻則塞式軾域殖植敕飭棘惑默織匿億憶特勒劾仄稷識逼克蜮唧即拭弋陟測冒抑惻肋亟殛忒嶷熄穡嗇匐鯽或愎翌 　　

  - **入聲，十四緝\[-ip\]** 　　

緝輯立集邑急入泣濕習給十拾什襲及級澀粒揖汁蟄笠執隰汲吸熠岌歙熠挹 　　

  - **入聲，十五合\[-op\]** 　　

合塔答納榻雜臘蠟匝闔蛤衲遝鴿踏颯拉盍搭溘嗑 　　

  - **入聲，十六葉\[-ep\]** 　　

葉帖貼牒接獵妾蝶篋涉捷頰楫攝躡諜協俠莢睫懾蹀挾喋燮褶靨燁摺輒撚婕聶霎 　　

  - **入聲，十七洽\[-aep\]** 　　

洽狹峽法甲業鄴匣壓鴨乏怯劫脅插押狎掐夾恰眨呷喋劄鉀

## 音系對比

<div class="overflowbugx" style="overflow:auto;">

<table>
<thead>
<tr class="header">
<th><p>rowspan=2 white-space: nowrap|漢字</p></th>
<th><p>rowspan=2 white-space: nowrap|<a href="../Page/反切.md" title="wikilink">反切</a></p></th>
<th><p>rowspan=2 white-space: nowrap|類型</p></th>
<th><p>rowspan=2 white-space: nowrap|構擬<a href="../Page/上古漢語.md" title="wikilink">上古音</a><br />
（<a href="../Page/鄭張尚芳.md" title="wikilink">鄭張尚芳</a>）</p></th>
<th><p>rowspan=2 white-space: nowrap|構擬<a href="../Page/中古漢語.md" title="wikilink">中古音</a><br />
（<a href="../Page/王力_(语言学家).md" title="wikilink">王力</a>）</p></th>
<th><p>colspan=3 white-space: nowrap|<a href="../Page/官話.md" title="wikilink">官話</a></p></th>
<th><p><a href="../Page/晉语.md" title="wikilink">晉语</a></p></th>
<th><p>rowspan=2 white-space: nowrap|<a href="../Page/閩南語.md" title="wikilink">閩南語泉漳</a><br />
（文 / 白）</p></th>
<th><p><a href="../Page/吳語.md" title="wikilink">吳語</a></p></th>
<th><p><a href="../Page/粵語.md" title="wikilink">粵語</a></p></th>
<th><p>rowspan=2 white-space: nowrap|<a href="../Page/贛語.md" title="wikilink">贛語</a></p></th>
<th><p>rowspan=2 white-space: nowrap|<a href="../Page/客語.md" title="wikilink">客語</a></p></th>
<th><p>colspan=2 white-space: nowrap|<a href="../Page/日語.md" title="wikilink">日語</a></p></th>
<th><p>white-space: nowrap|<a href="../Page/朝鲜语.md" title="wikilink">朝鲜语</a></p></th>
<th><p>colspan=2 white-space: nowrap|<a href="../Page/越南語.md" title="wikilink">越南語</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/北京官話.md" title="wikilink">北京官話</a><br />
<a href="../Page/北京話.md" title="wikilink">北京</a></p></td>
<td><p><a href="../Page/江淮官話.md" title="wikilink">江淮官話</a><br />
<a href="../Page/合肥話.md" title="wikilink">合肥</a></p></td>
<td><p><a href="../Page/西南官话.md" title="wikilink">西南官话</a><br />
<a href="../Page/乐山话.md" title="wikilink">乐山</a></p></td>
<td><p><a href="../Page/太原話.md" title="wikilink">太原</a></p></td>
<td><p><a href="../Page/蘇州話.md" title="wikilink">蘇州</a></p></td>
<td><p><a href="../Page/廣州話.md" title="wikilink">廣州</a></p></td>
<td><p><a href="../Page/吳音.md" title="wikilink">吳音</a></p></td>
<td><p><a href="../Page/漢音.md" title="wikilink">漢音</a></p></td>
<td><p><a href="../Page/首爾.md" title="wikilink">首爾音</a></p></td>
<td><p><a href="../Page/河內.md" title="wikilink">河內音</a></p></td>
<td><p><a href="../Page/胡志明市.md" title="wikilink">西貢音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>合</p></td>
<td><p>侯閤</p></td>
<td><p>全濁入</p></td>
<td><p>* </p></td>
<td></td>
<td><p><br />
</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><br />
(gapu→)</p></td>
</tr>
<tr class="odd">
<td><p>十</p></td>
<td><p>是執</p></td>
<td><p>全濁入</p></td>
<td><p>* </p></td>
<td></td>
<td><p><br />
</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><br />
(jipu→)</p></td>
</tr>
<tr class="even">
<td><p>佛</p></td>
<td><p>符弗</p></td>
<td><p>全濁入</p></td>
<td><p>* <br />
</p>
<div>
<p>（後造字）</p>
</div></td>
<td></td>
<td><p><br />
</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><br />
(butu→butsu)</p></td>
</tr>
<tr class="odd">
<td><p>八</p></td>
<td><p>博拔</p></td>
<td><p>全清入</p></td>
<td><p>* </p></td>
<td></td>
<td><p><br />
</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><br />
(pati→hachi)</p></td>
</tr>
<tr class="even">
<td><p>易</p></td>
<td><p>羊益</p></td>
<td><p>次濁入</p></td>
<td><p>* </p></td>
<td></td>
<td><p><br />
</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><br />
(yaku)</p></td>
</tr>
<tr class="odd">
<td><p>客</p></td>
<td><p>苦格</p></td>
<td><p>次清入</p></td>
<td><p>* </p></td>
<td></td>
<td><p><br />
</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><br />
(kyaku)</p></td>
</tr>
</tbody>
</table>

</div>

<small>※註：「易」僅於「交換、變易」之義時讀為入聲 \[-k̚\]，餘為去聲。</small>

## 漢語各分支中的入聲

### 官話

在無入聲調類的[北京官話](../Page/北京官話.md "wikilink")、[东北官话](../Page/东北官话.md "wikilink")、[胶辽官话](../Page/胶辽官话.md "wikilink")、[冀鲁官话中](../Page/冀鲁官话.md "wikilink")，中古的入聲字被分派入平聲、上聲、去聲中，此現象稱為“**入派三聲**”。

[北京官话中的入声字如何分派有若干規律可循](../Page/北京官话.md "wikilink")（排除少量例外）：

1.  全濁聲母字派入陽平（如：薄、奪、宅、昨、石）；例外：劇、續。
2.  次濁聲母字派入去聲（如：肉、落、日、月、逆）；例外：辱（中國大陸的[普通話中讀上聲](../Page/普通話.md "wikilink")）、額（通常讀陽平）。
3.  清聲母字可能派入陰平、陽平、上聲、去聲之任一類，無固定規律；去声较多，上声较少。

在[中原官话和](../Page/中原官话.md "wikilink")[兰银官话中](../Page/兰银官话.md "wikilink")，中古的入声字被分为两组。古全浊字归阳平；古清声字和古次浊字归入另一组：中原官话中归入阴平，兰银官话中归入去声。

在[西南官话中](../Page/西南官话.md "wikilink")，中古的入声字今声调大都相同，大部分地区读阳平，少数地区仍保留舒聲韻形式的入声調，也有少數地區甚至不只保留入聲調也保留有塞音韻尾的入聲韻。

#### 簡易入聲判別方法

  - 當以下同一橫排條件全符合時，即為入聲字（空白表示無限制）；但是很多入聲字並不符合下列任一排的條件，如“切”、“塔”、“福”、“六”、“刻”、“骨”，這些字還需要強記。

|                                    |                                |                                |                  |
| ---------------------------------- | ------------------------------ | ------------------------------ | ---------------- |
| 北京官話[聲母](../Page/聲母.md "wikilink") | [韻母](../Page/韻母.md "wikilink") | [聲調](../Page/四聲.md "wikilink") | 例外               |
| ㄅ、ㄉ、ㄍ、ㄐ、ㄓ、ㄗ                        | 非鼻音韻尾                          | 陽平                             | 鼻                |
| ㄉ、ㄊ、ㄌ、ㄗ、ㄘ、ㄙ                        | ㄜ                              |                                |                  |
| ㄎ、ㄓ、ㄔ、ㄕ、ㄖ                          | ㄨㄛ                             |                                |                  |
| ㄅ、ㄆ、ㄇ、ㄉ、ㄊ、ㄋ、ㄌ                      | ㄧㄝ                             |                                | 爹、咩              |
| ㄉ、ㄍ、ㄏ、ㄗ、ㄙ                          | ㄟ                              |                                |                  |
| ㄈ                                  | ㄚ、ㄛ                            |                                |                  |
|                                    | ㄩㄝ                             |                                | 靴、瘸、嗟（讀ㄐㄩㄝ／jue時） |

  - 一字有兩讀，[讀音為](../Page/讀音.md "wikilink")[開口呼韻](../Page/開口呼.md "wikilink")，[語音為](../Page/語音.md "wikilink")[齊齒呼](../Page/齊齒呼.md "wikilink")、[合口呼韻](../Page/合口呼.md "wikilink")，必為入聲字。
  - 如一字為鼻音韻尾ㄢ、ㄣ、ㄤ、ㄥ（）時，必**非**入聲字。（例外字只有廿）
  - ㄅ、ㄉ、ㄍ、ㄐ、ㄓ、ㄗ（）等聲母的上聲字，一般非入聲（例外如百、葛、骨、角、窄）。
  - 入聲字的辨識原則可參考：[陳新雄](../Page/陳新雄.md "wikilink")《萬緒千頭次第尋——談讀書指導》

#### 西南官話考量入聲

對於說[西南官話的人來說](../Page/西南官話.md "wikilink")，可以不使用[工具書](../Page/工具書.md "wikilink")，而根據瞭解的中古至今的語音變化規律來辨別大部分古代入聲字：\[3\]

1.  古今聲調演變判斷：
      - 凡普通話讀陰、上、去而西南方言讀陽平的字都是古代入聲字。（例外字：摸、玉）
2.  古今聲母演變規律判斷：
      - 凡不送氣的陽平字是古代入聲字。
      - 凡次濁聲母陽平字都不是古代入聲字。
3.  古今韻母演變規律判斷：
      - 凡鼻音韻尾（陽聲韻）字都不是古代入聲字。（例外字：廿）
      - 凡、、音節的字不是古代入聲字，凡音節的字不是古代入聲字。
      - 凡、韻母的字不是古入聲字。（例外字 ：蟀）
      - 除靴瘸以外的韻母字是古代入聲字。
      - 凡普通話有元音韻尾而西南方言沒有的字是古代入聲字。
      - 凡方言中的韻母字都是古代入聲字。
4.  根據形聲字的聲旁來判斷：
      - 詳見简启贤，《音韵学教程》：188-189．

#### 江淮官話(淮語)

在[淮語中](../Page/淮語.md "wikilink")，中古的入声字仍读入声。其中[洪巢片大部](../Page/洪巢片.md "wikilink")（除[庐江外](../Page/庐江.md "wikilink")）入声不分阴阳，只有一个入声调；[洪巢片](../Page/洪巢片.md "wikilink")[庐江](../Page/庐江.md "wikilink")\[4\]、[泰如片入声按照中古声母的清浊分成阴入和阳入](../Page/泰如片.md "wikilink")，有两个入声调。[黃孝片中也僅有一個入聲調](../Page/黃孝片.md "wikilink")，但部分古全濁入聲字歸入陽平或陽去調（如白、十），部分通[摄入声字的韵尾變為](../Page/攝_\(聲韻學\).md "wikilink")[后鼻音ŋ](../Page/后鼻音.md "wikilink")，但仍保留入聲聲調（如墓、木）。

北方官話与江淮官話的主要入聲歸派規律如下表：

|                                                                       | 古清入 | 古次濁入 | 古全濁入 |
| --------------------------------------------------------------------- | --- | ---- | ---- |
| [中原官话](../Page/中原官话.md "wikilink")                                    | 陰平  | 陰平   | 陽平   |
| [冀魯官话](../Page/冀魯官话.md "wikilink")                                    | 多陰平 | 去    | 陽平   |
| [膠遼官话](../Page/膠遼官话.md "wikilink")                                    | 上   | 去    | 陽平   |
| [蘭銀官话](../Page/蘭銀官话.md "wikilink")                                    | 去   | 去    | 陽平   |
| [西南官话](../Page/西南官话.md "wikilink")                                    | 陽平  | 陽平   | 陽平   |
| [北京官话](../Page/北京官话.md "wikilink")、[東北官话](../Page/東北官话.md "wikilink") | 無規律 | 去    | 多陽平  |
| [江淮官話](../Page/江淮官話.md "wikilink")                                    | 入   | 入    | 入    |

### 晉語

[晉語地區基本上都不同程度的保留有入聲韻](../Page/晉語.md "wikilink")，基本全以[喉塞音](../Page/喉塞音.md "wikilink")\[ʔ\]形式保留，有一部分地区(如[大同](../Page/大同市.md "wikilink"))不少字出現[舒声促化现象](../Page/舒声促化.md "wikilink")。

### 吳語

大多數[吳語收](../Page/吳語.md "wikilink")[喉塞音](../Page/喉塞音.md "wikilink")。但在[温州话中没有入声韵尾](../Page/温州话.md "wikilink")，只有入声声调。温州话仅有的一个韵尾是 -ng。温州话的入声声调在吴语方言里表现得十分奇特。所谓“入声”，就是比舒声要短促；但是温州话的两个入声调值却是 323 和 212，比所有舒声都要长。

### 閩語

[閩南語原只有](../Page/閩南語.md "wikilink")6個辅音韻尾，其中、、是[塞音](../Page/塞音.md "wikilink")，、、是[鼻音](../Page/鼻音_\(辅音\).md "wikilink")。但是在现代闽南语，「古」「咸」（除覃韵）（收）、「山」（收）、「宕」（收）、「梗」「摄」「二」等（收）的[白读音都不念这些韵尾](../Page/白读音.md "wikilink")，这些韵部的韵尾已经弱化为[鼻化元音和](../Page/鼻化元音.md "wikilink")[喉塞韵尾](../Page/喉塞韵尾.md "wikilink")，只有[文读音才念上述辅音韵尾](../Page/文读音.md "wikilink")。此外，[潮汕片韵尾](../Page/潮州話.md "wikilink")已经并入。

在[海南话的方言里](../Page/海南话.md "wikilink")，北部方言与[潮州話比较接近](../Page/潮州話.md "wikilink")，保留有、、三套入声韵尾，但南部方言大多只有喉塞韵尾。海南话、[雷州话都与闽南语有一定亲缘性](../Page/雷州话.md "wikilink")。闽南语念喉塞韵尾的字，在[雷州话念成开韵尾](../Page/雷州话.md "wikilink")。海南话、雷州话把闽南语的鼻化元音都念成口元音。

[莆仙语仅存在喉塞韵尾](../Page/莆仙语.md "wikilink")。此外，莆仙语只有[仙游话存在鼻化元音](../Page/仙游话.md "wikilink")，[莆田话把鼻化元音念成了口元音](../Page/莆田话.md "wikilink")。

[闽东语中存在](../Page/闽东语.md "wikilink")、两套入声韵尾，但在大部分方言中已经混同了，只有[古田话能够明显地区分开来](../Page/古田话.md "wikilink")。

[闽中语和](../Page/闽中语.md "wikilink")[闽北语中](../Page/闽北语.md "wikilink")，都只存在入声调，不存在入声韵尾。

### 粵語

[粵語有](../Page/粵語.md "wikilink")、、，如[廣州話就三種入聲韻尾齊備](../Page/廣州話.md "wikilink")，例子有：納naap，殺saat，福fuk。但各地入聲保存情況不盡相同，如[東莞粵語以收](../Page/東莞.md "wikilink")韻尾為主，還有收[喉塞音](../Page/喉塞音.md "wikilink")和[元音化的](../Page/元音化.md "wikilink")；[寶安區人口主要是外省移民](../Page/寶安區.md "wikilink")，寶安粵語入聲已全部弱化而收[喉塞音](../Page/喉塞音.md "wikilink")韻尾，也有入聲失落輔音韻尾，歸併到[陰平](../Page/陰平.md "wikilink")35調中的。

### 客家語

[客家語的入聲韻尾](../Page/客家語.md "wikilink")、、。一般認為，客家語和後期[中古漢語](../Page/中古漢語.md "wikilink")（[唐宋二代為準](../Page/唐宋音系.md "wikilink")）之間的承襲關係明顯。

### 贛語

[贛語的入聲可以分以下幾類](../Page/贛語.md "wikilink")：

  - 既有入聲調，又有入聲韻母。又可分兩種情況：

<!-- end list -->

1.  只有一個入聲調，調值一般都比較高：[宜豐](../Page/宜豐.md "wikilink")、[上高](../Page/上高.md "wikilink")、[新淦](../Page/新淦.md "wikilink")（有兩個韻尾）；[武寧](../Page/武寧.md "wikilink")、[宜春](../Page/宜春.md "wikilink")、[樟樹](../Page/樟樹.md "wikilink")、樂平、[景德鎮](../Page/景德鎮.md "wikilink")、橫峰、鉛山、進賢、南城、永豐（只有一個韻尾）。
2.  有兩個入聲調。[昌都片](../Page/昌都片.md "wikilink")、[宜瀏片](../Page/宜瀏片.md "wikilink")、[鷹弋片通常为陰入高](../Page/鷹弋片.md "wikilink")、陽入低；[撫廣片通常为陰入低](../Page/撫廣片.md "wikilink")、陽入高。
3.  有三個入聲調。[永修](../Page/永修.md "wikilink")、[德安的入聲依古聲母的清](../Page/德安.md "wikilink")、濁分陰陽，只有一個韻尾。其陰入又根據聲母的送氣與否再分為陰入1和陰入2，因此實際上有三個入聲調。安義的入聲則依聲母的清濁分陰陽兩大類，其陰入又分為兩調因此也有三個入聲韻尾。
4.  有四個入聲調。都昌的入聲依古聲母的清濁分陰陽，有兩個韻尾。其陰入與陽入各根據聲母的送氣與否再分為陰入1、陰入2和陽入1、陽入2，所以實際上有四個入聲調。

<!-- end list -->

  - 有入聲調，無入聲韻母。

<!-- end list -->

1.  只有一個入聲調，讀長音，無塞音韻尾，無入聲韻母，比如[星子](../Page/星子.md "wikilink")、[鄱陽等地](../Page/鄱陽.md "wikilink")。

<!-- end list -->

  - 沒有入聲調，亦沒有入聲韻母。古入聲字派入其他調類。根據派入調類的不同，可以分以下幾種情況：

<!-- end list -->

1.  [湖口](../Page/湖口縣.md "wikilink")、[彭澤按入聲字聲母的清濁分別派入陰去和陽去](../Page/彭澤.md "wikilink")。
2.  分宜、峽江、安福、蓮花、[萍鄉](../Page/萍鄉.md "wikilink")、[寧岡](../Page/寧岡.md "wikilink")、永新、[吉水](../Page/吉水.md "wikilink")、吉安、[泰和通常依入聲字聲母的清濁分別派入陰平和陽去](../Page/泰和.md "wikilink")。
3.  [遂川古入聲清聲母字歸陰去](../Page/遂川.md "wikilink")2，入聲濁聲母字歸陰去1。

### 湘语

湘语中新湘语大部分保留独立入声调，且韵尾消失。老湘语部分地区保留入声调，部分地区入声派入其他声调。

## 漢字文化圈的其他語言

由於古時漢文化大量輸出到東亞各國，[漢字文化圈的其他語言完好而有系統地保留了入聲](../Page/漢字文化圈.md "wikilink")。

### 日語

漢字大概於[隋](../Page/隋.md "wikilink")、[唐時代傳入](../Page/唐.md "wikilink")[日本](../Page/日本.md "wikilink")，當時的漢語具有入聲，因而[日語](../Page/日語.md "wikilink")[音讀多數保留了](../Page/音讀.md "wikilink")[古漢語的入聲](../Page/古漢語.md "wikilink")。但被納入日語體系的入聲發音已經完全不具備入聲的特點，其[塞音音尾已獨立成另一個音節](../Page/塞音.md "wikilink")［通常為<span title="ka">か</span>行、<span title="ta">た</span>行、<span title="ha">は</span>行（後轉變為<span title="wa">わ</span>行、）的音節］。原來為入聲漢字的音讀常擁有兩個音節。

### 朝鲜语

朝鮮半島自古崇尚漢文化，也因此今天的[韓語中](../Page/韓語.md "wikilink")，有超过70%為漢字詞（[漢字語](../Page/汉字词_\(朝鲜文\).md "wikilink")）。朝鲜语中的漢字音完整地保留了[中古漢語的入聲韵尾](../Page/中古漢語.md "wikilink")，例如：入口（입구
），學校（학교 ），五十（오십 ）
。但是，[塞音](../Page/塞音.md "wikilink")韵尾並未直接保留，而是有系统地變化為[流音](../Page/流音.md "wikilink")，例如：一（일
）、八（팔
）。另外，在實際發音時，入聲韻尾有時會根據[朝鲜语的发音规则變為](../Page/朝鮮語音系.md "wikilink")[鼻音或後一個音節的](../Page/鼻音_\(辅音\).md "wikilink")[聲母](../Page/聲母.md "wikilink")，例如：獨立（독립
），業務（업무 ），發言（발언 ），十五（십오 ）。

### 越南語

由于[越南语在古代引入了大量的](../Page/越南语.md "wikilink")[汉语词汇](../Page/汉语.md "wikilink")，所以[汉字的](../Page/汉字.md "wikilink")[古汉语发音在越南语中保留得很好](../Page/古汉语.md "wikilink")。越南語汉字音韵尾仅仅在拉丁字母拼写上出现了4个变异，实际上相较于[中古汉语几乎没有变化](../Page/中古汉语.md "wikilink")。古汉语入聲韵尾收、、的汉字在越南语中依然收、、。例如：
、 、 等。但是在[西貢方言中](../Page/西貢.md "wikilink")，以結尾的音節均發作，而以結尾的音節則發作。

現代越南語按[調值共分六個聲調](../Page/調值.md "wikilink")，即平聲、玄聲、問聲、跌聲、銳聲、重聲。以塞音結尾的音節相當於漢語**入聲**，衹可能是銳聲或者重聲。
[VNtone.jpg](https://zh.wikipedia.org/wiki/File:VNtone.jpg "fig:VNtone.jpg")

<table>
<tbody>
<tr class="odd">
<td><p>序號</p></td>
<td><p>越南語名稱</p></td>
<td><p>漢譯名稱</p></td>
<td><p>漢越語對應<a href="../Page/中古漢語.md" title="wikilink">中古漢語聲調</a></p></td>
<td><p>調值描述</p></td>
<td><p>例字</p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td></td>
<td><p>平聲（陰平）</p></td>
<td><p>清平、次濁平</p></td>
<td><p>44，平，長，類似<a href="../Page/普通話.md" title="wikilink">普通話陰平聲</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td></td>
<td><p>玄聲（陽平）</p></td>
<td><p>全濁平</p></td>
<td><p>31，中降，長</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td></td>
<td><p>問聲（陰上）</p></td>
<td><p>清上</p></td>
<td><p>21(4)，低降或低降後升，長，緊喉，類似普通話上聲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td></td>
<td><p>跌聲（陽上）</p></td>
<td><p>次濁上</p></td>
<td><p>32/4，中，緊喉且中斷</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td></td>
<td><p>銳聲（陰去、陰入）</p></td>
<td><p>清去、清入</p></td>
<td><p>45，高升，短，類似普通話陽平聲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td></td>
<td><p>重聲（陽去、陽入）</p></td>
<td><p>濁去、全濁上、濁入</p></td>
<td><p>21，低降，短</p></td>
<td></td>
</tr>
</tbody>
</table>

{{-}}

## 漢藏語系的其他語言

[漢藏語系其他語言](../Page/漢藏語系.md "wikilink")，包括傳統上歸入其中的[苗瑤語族](../Page/苗瑤語族.md "wikilink")、[壯侗語族](../Page/壯侗語族.md "wikilink")，和漢語有大量的[同源詞](../Page/同源詞.md "wikilink")，很多漢語入聲字在這些語言中的同源詞也保留了塞音韻尾。

舉例：

| 漢語(粵音)      | [藏語](../Page/藏語.md "wikilink")（[拉丁轉寫](../Page/威利轉寫.md "wikilink")） | [緬甸語](../Page/緬甸語.md "wikilink")（[拉丁轉寫](../Page/緬甸語委轉寫系統.md "wikilink")） | [勉語](../Page/勉語.md "wikilink") | [泰語](../Page/泰語.md "wikilink")（[拉丁轉寫](../Page/皇家泰語轉寫通用系統.md "wikilink")） |
| ----------- | ------------------------------------------------------------------ | ------------------------------------------------------------------------ | ------------------------------ | ------------------------------------------------------------------------ |
| 六（lu**k**）  | དྲུག། （dru**g**）                                                   | ခြောက် （hkrau**k**）                                                      | ku**q**                        | หก （ho**k**）                                                             |
| 八（baa**t**） | བརྒྱད། （brgya**d**）                                                | ရှစ် （rha**c**）                                                          | ghje**t**                      | แปด （pae**t**）                                                           |

## 參考文獻

## 參見

  - [無聲除阻](../Page/無聲除阻.md "wikilink")（失去爆破）
  - [四声](../Page/四声.md "wikilink")

## 外部連結

  - [入声字自动标注、查询工具](https://www.xiaohui.com/rsz/)

[Category:漢語音韻學](../Category/漢語音韻學.md "wikilink")

1.  [王力 (語言學家)](../Page/王力_\(語言學家\).md "wikilink")
2.  明朝[釋真空](../Page/釋真空.md "wikilink")：「平聲平道莫低昂，上聲高呼猛烈強，去聲分明哀遠道，入聲短促急收藏。」
3.  简启贤．音韵学教程\[M\]．成都：巴蜀书社，2005：184-189．
4.  孙宜志．安徽江淮官话语音研究\[M\]．合肥：黄山书社，2006．