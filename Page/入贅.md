**入贅**又稱為**招贅**、**入贅婚**，為[婚姻模式一種](../Page/婚姻.md "wikilink")。簡單來说，男子如同古代女子[出嫁般](../Page/出嫁.md "wikilink")，成為女方家庭成員，視[岳父](../Page/岳父.md "wikilink")[母為父母](../Page/岳母.md "wikilink")。入贅的男子稱**贅夫**、**贅婿**，俗稱為**姑爺**。\[1\]

在[中国](../Page/中国.md "wikilink")，入贅俗稱「**倒插門**」、「**上門女婿**」，入赘在不同的地方有不同的风俗，通常子女必須全部[隨母姓](../Page/隨母姓.md "wikilink")，或者僅留下一個子女從父姓；有些地區較寬鬆，是第一个兒子從母姓，其他隨父姓（[閩南語俗稱](../Page/閩南語.md "wikilink")[抽豬母稅](../Page/抽豬母稅.md "wikilink")），部分[門閥的入贅婚](../Page/門閥.md "wikilink")，甚至會有男子必須[冠姓](../Page/冠姓.md "wikilink")（冠上妻家姓氏），不許[納妾及祭拜原族](../Page/納妾.md "wikilink")[祖先](../Page/祖先.md "wikilink")，僅有每年的[正月初二才能回家探望親生父母](../Page/正月初二.md "wikilink")，由於传统[父系社会中](../Page/父系社会.md "wikilink")，一般是男方經濟能力较差，女方富裕且没有兄弟（或兄弟已歿、[出家](../Page/出家.md "wikilink")、殘疾致不能繼後）才會發生入赘的情形，故社會上時常輕視贅婿，認為是無力謀生的男子。

另外，[白族](../Page/白族.md "wikilink")、[彝族](../Page/彝族.md "wikilink")、[瑤族](../Page/瑤族.md "wikilink")、[大和族](../Page/大和族.md "wikilink")、[琉球族還有種以](../Page/琉球族.md "wikilink")[養子名義入贅的約定](../Page/養子.md "wikilink")[習俗](../Page/習俗.md "wikilink")，稱[婿養子](../Page/婿養子.md "wikilink")。這種婚俗中的男子婚後[改用妻姓](../Page/從妻姓.md "wikilink")，依照妻子家中排行，妻子的兄弟姊妹亦視之為兄弟而非「姊夫」或「妹夫」，並可繼承妻家的家業。\[2\]\[3\]

也有一些介乎兩者之間，如[傣族的女婿要在外家生活一段子](../Page/傣族.md "wikilink")，才可視情況決定可否自立門戶。

## 参见

  - [抽豬母稅](../Page/抽豬母稅.md "wikilink")

## 腳注

## 外部連結

  - [地圖會說話:
    臺灣的母系社會遺緒](https://www.webcitation.org/5gN0AXHrh?url=http://richter.pixnet.net/blog/post/22904759)
  - [台灣有關入贅的繼承權](http://www.lawtw.com/article.php?template=article_content&area=free_browse&parent_path=,1,188,&job_id=29600&article_category_id=229&article_id=14737)

[Category:婚姻](../Category/婚姻.md "wikilink")

1.
2.  [日治時期西拉雅家戶的高招贅婚比例—母系繼嗣文化遺留？家中缺乏男嗣？](http://edresource.nmns.edu.tw/ShowObject.aspx?id=0b81d9e9c00b81d9e9dc0b81e6d441)，[.pdf](http://web2.nmns.edu.tw/PubLib/NewsLetter/100/282/a-1.pdf)，陳叔倬，[國立自然科學博物館](../Page/國立自然科學博物館.md "wikilink")
3.  [日治時期新竹地區招贅現象的歷史人口學分析](http://saturn.ihp.sinica.edu.tw/~huangkc/nhist/24-3YCMZWYC.htm)，莊英章、楊文山、張孟珠，《新史學雜誌》，[中央研究院歷史語言研究所](../Page/中央研究院.md "wikilink")