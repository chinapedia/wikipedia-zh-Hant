**入野自由**（）是[日本的男性](../Page/日本.md "wikilink")[演員](../Page/演員.md "wikilink")、[聲優](../Page/聲優.md "wikilink")、[歌手](../Page/歌手.md "wikilink")。[舞台劇](../Page/舞台劇.md "wikilink")[演員出身](../Page/演員.md "wikilink")。1992年加入，2006年退出。2006年起所屬經紀公司為。2009年起所屬唱片公司為[Kiramune](../Page/Kiramune.md "wikilink")，個人應援色為紅色。出生於[東京都](../Page/東京都.md "wikilink")。音域為C～F。

## 人物

2001年，年仅13岁的入野自由开始了自己的声优生涯，在《[神隱少女](../Page/神隱少女.md "wikilink")》，虽然声调平静，但干净透明的童音已经博得不少人的好感。2003年在《[天使怪盗](../Page/天使怪盗.md "wikilink")》中担纲主角，更加引人注目，清亮而未经雕琢的嗓音，令人回味。丹羽大助是可爱的孩子，14岁的孩子，像水果一样漂亮，天空一样明朗。在《[翼·年代记](../Page/翼·年代记.md "wikilink")》与广播剧《[私立堀镡学园](../Page/私立堀镡学园.md "wikilink")》担任小狼与小龙（堀镡学园）的声优，使入野的人气更高。《翼》中的小狼更是给人一种亲切的感觉。在PS2游戏《[王国之心](../Page/王国之心.md "wikilink")》为主角Sora配音，表现十分出色。

在2009年6月24日加入Kiramune以歌手身分出道，應援色為紅色。在[鈴村健一和](../Page/鈴村健一.md "wikilink")[岩田光央的](../Page/岩田光央.md "wikilink")無限期停止演藝活動後，入野成為Kiramume資歷最深年齡最小的人。

  - 曾参加电视剧「[3年B组金八先生](../Page/3年B组金八先生.md "wikilink")」。1999年出演过电影《超人力霸王迪卡・超人力霸王帝納＆超人力霸王蓋亞超時空大決戰》，后来又客串演出了电视剧《[超人力霸王蓋亞](../Page/超人力霸王蓋亞.md "wikilink")》。在孩童時代，他就已参加过TV版动画《[逮捕令](../Page/逮捕令_\(動畫\).md "wikilink")》的配音工作。

<!-- end list -->

  - 在[飛輪少年的動畫化中](../Page/飛輪少年.md "wikilink")，是該動畫化唯一一個在2006年的TV版與2011年的OAD的《黑之羽與沉睡森林
    -Break on the sky-》使用相同的聲優。角色分別為美鞍葛馬（2011年OAD），鵺（2006年TV）。

<!-- end list -->

  - 進行《D.N.ANGEL》配音時，只是個15歲的少年，被一同演出的前輩們疼愛，共演的[淺野真澄也對入野連呼](../Page/淺野真澄.md "wikilink")「かわいい」。
  - [草尾毅屢次稱讚入野為](../Page/草尾毅.md "wikilink")「天才」，但也常常揶揄入野的趣事，但當入野學校成績退步時，也受到草尾的鼓勵。

<!-- end list -->

  - 演出《[TSUBASA翼](../Page/TSUBASA翼.md "wikilink")》的時候被認為聲音與[浪川大輔相似](../Page/浪川大輔.md "wikilink")，曾經和浪川互換過小狼和法伊的配音。

<!-- end list -->

  - 和[宮野真守從劇團ひまわり時代就是朋友](../Page/宮野真守.md "wikilink")，互稱「みゆ」、「まもちゃん」。
      - 2010年3月15～25日與[宮野真守共演舞台劇](../Page/宮野真守.md "wikilink")《Garnet
        Opera》。
      - 2014年與[宮野真守共演舞台劇](../Page/宮野真守.md "wikilink")《最高はひとつじゃない2014》。
      - 2010年10月16日到訪台灣，與[宮野真守以及導演](../Page/宮野真守.md "wikilink")[水島精二一同出席](../Page/水島精二.md "wikilink")《機動戰士鋼彈00劇場版-A
        wakening of the Trailblazer-》電影宣傳見面會。
      - 宮野曾提到印象中自由都是配王道漫畫主人公的角色，很羨慕他擁有絕對閃閃發亮的主角光環。在訪談中也曾稱讚自由一旦做了決定，決心就很強大，會很快付諸行動，並表示自己身邊有個正在走自己無法前進的道路的人，而且不斷進行自我提升，令自己相當憧憬。

<!-- end list -->

  - 與[神谷浩史同屬](../Page/神谷浩史.md "wikilink")[Kiramune。](../Page/Kiramune.md "wikilink")
      - 非常尊敬神谷浩史，認為能與他組成團體，是榮幸。在2010年一起組成了「KAmiYU」團體，並開過4次演唱會「"KAmiYU in
        Wonderland" Talk and Live Event」，其中「KAmiYU in Wonderland
        4」第二日進行全國電影院直播。目前已發售2張單曲「my Proud, my
        Play\!」「REASON」和3張專輯「link-up」「Road to
        Wonderland」「Happy-Go-Lucky」。其中「Happy-Go-Lucky」發售首日為ORICON公信榜專輯部分第五名，首週為第六名。
      - 20歲生日時從神谷那得到一個存錢筒，是[小野大輔在神谷家錄製特別回廣播節目時的道具](../Page/小野大輔.md "wikilink")，後來就擺在神谷家角落，趁入野到神谷家作客時神谷連同裡面數枚500圓硬幣「順手」送給他的。

<!-- end list -->

  - 和[內山昂輝感情很好](../Page/內山昂輝.md "wikilink")，兩人曾一起去旅遊（自由開車）。
  - 和[鈴村健一同屬Kiramune](../Page/鈴村健一.md "wikilink")。
      - 鈴村健一說過「我和自由的緣份很長久」，在兩人初見時，入野還是國一生，過了幾年才終於在廣播節目合作。
      - 在[小松先生](../Page/小松先生.md "wikilink")「えいがのおそ松さん 劇場公開記念特番
        鈴村健一&入野自由のおフランスに行くザンス\!」中與鈴村健一同去法國巴黎為電影進行宣傳活動。
  - 和[吉野裕行同屬Kiramune](../Page/吉野裕行.md "wikilink")。
      - 在《[鋼彈00](../Page/鋼彈00.md "wikilink")》第一次和吉野裕行合作，但在工作外，兩人早已是五人制足球隊的隊友。
      - 2018年8月4日擔任吉野裕行Live Tour 2018“情熱アンソロジー”大阪公演特別嘉賓。
  - 鋼彈00之後為了增強體力開始健身，做了循環訓練以鍛鍊心肺，會跟宮野真守確認鍛鍊狀況。雖然不是注重於壯大體格的人，但也會重視肌肉訓練，還會短跑和蹦床。宮野曾說
    “演技是無形的，但肌肉是摸得著的努力的證據”，自由回應 “肌肉是不會背叛的\!” 成為業界名言。

<!-- end list -->

  - 入野本人曾表示對於稱謂不是那麼在乎。聲優也配，演員也演，歌手也唱，若不是同時擁有這三個身份，就不會構成現在的「入野自由」。並表示「遇到可以前進的方向時，我會毫不猶豫的丟掉稱謂；但如果『稱謂』能幫助我前進，那我會拾回來。」
      - 認為平平都是表演工作，而人們一直在「稱謂」上有意無意的分類這點很奇怪。在未來的道路上，想要努力跨越這條界線。
      - 認為自己是從[聲優界培養起來的](../Page/聲優.md "wikilink")，所以必須努力給予回報，為此必須更加努力。

<!-- end list -->

  - 經常幫自己算命的占卜師表示入野自由會「一生少年」，本人並不否認，並高興的說「大概未來我還是個孩子吧！」。

<!-- end list -->

  - 在15～25歲時，曾想過將來要結婚生子並組幸福的家庭；如今越是能隨心所欲的工作，越是沒了想結婚的念頭，然後心想果然最重要的還是自己啊。

<!-- end list -->

  - 母親曾為[手語相關工作者](../Page/手語.md "wikilink")。為家中獨生子。希望擁有妹妹或弟弟。業界中流傳其父親是冒險家，但被本人否認。

<!-- end list -->

  - 在學生時代，擔任體育祭實行委員。喜歡的運動是足球和籃球。雖然很喜歡足球，但技術不是很好，唯一的特技就是可以帶著球跑很快。

<!-- end list -->

  - 特技是倒立以及雜技。不擅長繞口令。喜歡的水果是西瓜和芒果。曾有段時間到處說自己很喜歡西瓜(但不可以加鹽)。
  - 自認不擅長應付電視節目，面對鏡頭容易緊張，覺得能在鏡頭面前讓氣氛高漲起來的人很厲害。
  - 2011年12月4日參加[LisAni\!](../Page/Lis_Ani!.md "wikilink") LIVE
    2011，在日本武道館演唱 ｢Zero｣。

<!-- end list -->

  - 2015年5月14日上[朝日電視台音樂節目](../Page/朝日電視台.md "wikilink")「BREAK
    OUT」，聊了各種話題。2016年12月8日再度上該節目。
      - 比起描繪巨大的夢想，更專注於一步一步向前邁進。
      - 有忘東忘西的小毛病，想努力根治但似乎是沒有辦法實現的夢。
      - 吃飯時，很容易把米飯掉落在奇怪的地方譬如頭髮上之類的。
      - 容易進入自己的世界，對於沒有興趣的話題，常常跑神。在見面會上也很容易這樣。
      - [天然呆](../Page/天然呆.md "wikilink")，但本人無自覺。

<!-- end list -->

  - 2016年4月9日及2018年4月15日參加共兩次 ｢Friends of Disney Concert｣ 演唱會。
  - 2016年4月30日參加[渡邊直美主持的NHK音樂節目](../Page/渡邊直美.md "wikilink") ｢NAOMIの部屋｣
    ，演唱 ｢嘘と未来と｣。
  - 2016年出演富士電視台8月1日播出的月九劇《[有喜歡的人](../Page/有喜歡的人.md "wikilink")》第四集，為日本人氣聲優首次出演富士台月九劇，預告片反響空前，在推特上成為熱搜話題。

<!-- end list -->

  - 2017年1月起，為海外留学暫時休業，\[1\] 於下半年回歸。
      - 堅持圓了海外留學的夢想。認為如果一直帶著一定要收穫什麼的心情的話會很累，所以只是去各國體會和感受事物，和各種人相遇，讓視野更開闊。期間有在朋友家過夜，結交新朋友，還和最喜歡的前輩一起旅行，聽到許多受益匪淺的話。海外旅行的其中一個目的就是看歐洲各國表演，雖然聽不大懂但仍覺得很有趣。
  - 2017年受邀參加7月1日到7月4日的美國LA漫展「[Anime
    Expo](../Page/Anime_Expo.md "wikilink")
    2017」當嘉賓，首次以個人名義在海外公開亮相，在座談會上分享自己配過的眾多知名角色，並舉辦簽名會。\[2\]
  - 2018年9月5日與跟日本許多名演員合作過的服裝品牌KANGOL
    REWARD共同設計原創帽T，期間限定販售，至10月12日止，為該品牌首次與聲優合作。
  - 2018年9月22日以王國之心3主角聲優的身分參加[東京電玩展](../Page/東京電玩展.md "wikilink")(TOKYO
    GAME SHOW 2018)。
  - 2018年6月出演的舞台劇『銀河鉄道999』〜GALAXY
    OPERA〜與[中川晃教一同獲得](../Page/中川晃教.md "wikilink")2018年音樂劇獎項(All
    Aboutミュージカル・アワード)最佳合作獎(ベスト・フレンズ賞)。\[3\]
  - 2014年2月19日生日當天，發售第四張迷你專輯「E =
    mc2」，登上[ORICON公信榜專輯部分日榜第](../Page/Oricon公信榜.md "wikilink")
    10 名，發售首週為週榜第 17 名。
  - 2015年5月8日為第五張迷你專輯「僕の見つけたもの」發售日，登上ORICON公信榜專輯部分日榜第 1 名，發售首週為週榜第 8 名。
  - 2016年11月30日為第二張專輯「DARE TO DREAM」發售日，登上ORICON公信榜專輯部分日榜第 1 名，發售首週為週榜第
    13 名。
  - 2018年6月20日為第四張單曲「FREEDOM」發售日，登上ORICON公信榜專輯部分日榜第 12 名，發售首週為週榜第 15 名。
  - 2019年3月20日為第六張迷你專輯「Live Your Dream」發售日，登上ORICON公信榜專輯部分日榜第 8
    名，發售首週為週榜第 12 名。

<table>
<caption>歌手活動</caption>
<thead>
<tr class="header">
<th><p>個人演唱會</p></th>
<th><p>時間</p></th>
<th><p>地點</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>入野自由 Birthday Live</p></td>
<td><p>2011年2月19日</p></td>
<td><p>原宿ASTRO HALL（東京）</p></td>
</tr>
<tr class="even">
<td><p>入野自由 2nd Live Tour "見果てぬ世界、繋がる想い"</p></td>
<td><p>2015年6月24日 - 7月26日</p></td>
<td><p>5會場5公演</p>
<ul>
<li>6月24日 新潟LOTS（新潟）</li>
<li>6月28日 Zepp Namba（大阪）</li>
<li>7月5日 豊洲PIT（東京）</li>
<li>7月12日 Zepp Nagoya（愛知）</li>
<li>7月26日 DRUM LOGOS（福岡）</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>入野自由 Live Tour "Enter the New World"</p></td>
<td><p>2017年1月7日 - 12日</p></td>
<td><p>2會場3公演</p>
<ul>
<li>1月7日 大阪府立國際會議場（大阪）</li>
<li>1月11日・12日 舞濱圓形劇場（千葉）</li>
</ul></td>
</tr>
<tr class="even">
<td><p>入野自由 Live Tour 2019 (預定)</p></td>
<td><p>2019年8月3日 - 9月6日</p></td>
<td><p>8會場9公演</p>
<ul>
<li>8月3日 Diamond Hall ( 愛知 )</li>
<li>8月5日・9月6日 新木場STUDIO COAST ( 東京 )</li>
<li>8月11日 Namba Hatch ( 大阪 )</li>
<li>8月18日 DRUM LOGOS ( 福岡 )</li>
<li>8月24日 PENNY LANE 24 ( 北海道 )</li>
<li>8月25日 Rensa ( 宮城 )</li>
<li>8月31日 高松Olive Hall ( 香川 )</li>
<li>9月1日 CLUB QUATTRO ( 廣島 )</li>
</ul></td>
</tr>
</tbody>
</table>

## 參與作品

**粗體字**為主角

### 電視動畫

**1995年**

  - [逮捕令](../Page/逮捕令_\(動畫\).md "wikilink")（阿翔）

**2001年**

  - [パラッパラッパー](../Page/パラッパラッパー.md "wikilink")（**パラッパ**）

**2002年**

  - [攻殼機動隊 STAND ALONE
    COMPLEX](../Page/攻殼機動隊_STAND_ALONE_COMPLEX.md "wikilink")（オンバ）

**2003年**

  - [狼雨](../Page/狼雨.md "wikilink")（ハス）
  - [魁！！天兵高校](../Page/魁！！天兵高校.md "wikilink")（城戶治）
  - [天使怪盜](../Page/天使怪盜.md "wikilink")（**丹羽大助**）

**2004年**

  - [KURAU Phantom
    Memory](../Page/KURAU_Phantom_Memory.md "wikilink")（イヴォン）
  - [次元艦隊](../Page/次元艦隊.md "wikilink")（洋介少年）
  - [蒼穹之戰神](../Page/蒼穹之戰神.md "wikilink")（春日井甲洋）
  - [風人物語](../Page/風人物語.md "wikilink")（润）
  - [MADLAX](../Page/MADLAX.md "wikilink")（クリス）

**2005年**

  - [光速蒙面俠21](../Page/光速蒙面俠21.md "wikilink")（**小早川瀨那**）
  - [星艦駕駛員](../Page/星艦駕駛員.md "wikilink")（Starship Operators）（結城シメイ）
  - [TSUBASA翼](../Page/TSUBASA翼.md "wikilink")（**小狼**）

**2006年**

  - [冬季花園](../Page/冬季花園.md "wikilink")（仙波拓郎）
  - [飛輪少年](../Page/飛輪少年.md "wikilink")（鵺）
  - [银色的奥林西斯](../Page/银色的奥林西斯.md "wikilink")（**トキト・アイザワ**）
  - [D.Gray-man](../Page/D.Gray-man.md "wikilink")（ナレイン）
  - [光之美少女Splash Star](../Page/光之美少女Splash_Star.md "wikilink")（宮迫學）
  - [復甦的天空 -RESCUE
    WINGS-](../Page/復甦的天空_-RESCUE_WINGS-.md "wikilink")（上岡聰）
  - [TSUBASA翼II](../Page/TSUBASA翼.md "wikilink")（**小狼**）

**2007年**

  - [Yes\! 光之美少女5](../Page/Yes!_光之美少女5.md "wikilink")（果果）
  - [機動戰士GUNDAM 00](../Page/機動戰士GUNDAM_00.md "wikilink")（**沙慈·克洛斯羅德**）
  - [驱魔少年](../Page/驱魔少年.md "wikilink")（纳莱茵/27话登场）
  - [黑之契約者](../Page/黑之契約者.md "wikilink")（黑 (小時候)）
  - [鐵子之旅](../Page/鐵子之旅.md "wikilink")（群眾）
  - [ONE PIECE](../Page/ONE_PIECE.md "wikilink")（吉諾）

**2008年**

  - [Yes\! 光之美少女5GoGo](../Page/Yes!_光之美少女5.md "wikilink")（果果）
  - [反斗小王子](../Page/反斗小王子.md "wikilink")（ペレ・サントス）
  - [機動戰士GUNDAM 00
    第二季](../Page/機動戰士GUNDAM_00.md "wikilink")（**沙慈·克洛斯羅德**）
  - [黑塚](../Page/黑塚.md "wikilink")（久遠）
  - [屍姬 赫](../Page/屍姬.md "wikilink")（高遠）
  - [鐵腕女警DECODE](../Page/鐵腕女警.md "wikilink")（**千川勉**）
  - [新‧安琪莉可 Abyss](../Page/新·安琪莉可.md "wikilink")（艾倫弗利德）
  - [新‧安琪莉可 Abyss -Second Age-](../Page/新·安琪莉可.md "wikilink")（艾倫弗利德）

**2009年**

  - [機巧魔神](../Page/機巧魔神.md "wikilink")（**夏目智春**）
  - [機巧魔神2](../Page/機巧魔神#.md "wikilink")（**夏目智春**、夏目直貴）
  - [四葉遊戲](../Page/四葉遊戲.md "wikilink")（**樹多村光**）
  - [07-GHOST神幻拍檔](../Page/07-GHOST.md "wikilink")（修里·歐克）
  - 鐵腕女警 DECODE:02（**千川勉**）
  - [初戀限定。](../Page/初戀限定。.md "wikilink")（財津衛）
  - [Phantom 〜Requiem for the
    Phantom〜](../Page/Phantom_〜Requiem_for_the_Phantom〜.md "wikilink")（**Zwei/吾妻玲二**）
  - [戰鬥司書系列](../Page/戰鬥司書系列.md "wikilink")（克里歐＝東尼斯）
  - [天國少女](../Page/天國少女.md "wikilink")（沙利・哈菲斯）
  - [金属对决 战斗陀螺](../Page/金属对决_战斗陀螺.md "wikilink")（大鳥翼）
  - [空中鞦韆](../Page/空中鞦韆.md "wikilink")（津田雄太）
  - [香格里拉](../Page/香格里拉.md "wikilink")（金絲雀）
  - [料理偶像](../Page/料理偶像.md "wikilink")（安野新）
  - [絕對可愛CHILDREN](../Page/絕對可愛CHILDREN.md "wikilink")（賈利）

**2010年**

  - [戰鬥陀螺 鋼鐵奇兵 爆](../Page/戰鬥陀螺_鋼鐵奇兵.md "wikilink")（**大鳥翼**）
  - [奇蹟少女KOBATO.](../Page/奇蹟少女KOBATO..md "wikilink")（小狼）
  - [大神與七位夥伴](../Page/野狼大神.md "wikilink")（**森野亮士**）
  - [妖怪少爺](../Page/妖怪少爺.md "wikilink")（雞冠丸）
  - [女僕咖啡廳](../Page/女僕咖啡廳_\(漫畫\).md "wikilink")（**真田廣章**）
  - [傳說的勇者的傳說](../Page/傳說的勇者的傳說.md "wikilink")（費歐爾・福克爾）
  - [每日媽媽](../Page/每日媽媽.md "wikilink")（青年）
  - [王牌投手 振臂高揮〜夏季大會篇〜](../Page/王牌投手_振臂高揮#.md "wikilink")（川島公）

**2011年**

  - [電波女&青春男](../Page/電波女&青春男.md "wikilink")（**丹羽真**）
  - [遊戲王ZEXAL](../Page/遊戲王ZEXAL.md "wikilink")（**阿斯特拉爾**、№96 ブラックミスト）
  - [未聞花名](../Page/未聞花名.md "wikilink")（**宿海仁太（小仁）**）
  - [Sacred Seven](../Page/Sacred_Seven.md "wikilink")（**鏡誠**）
  - [少年同盟](../Page/少年同盟.md "wikilink")（**橘千鶴**）
  - [UN-GO](../Page/UN-GO.md "wikilink")（速水星玄）
  - [爆旋陀螺 鋼鐵戰魂 4D](../Page/爆旋陀螺_鋼鐵戰魂#.md "wikilink")（大鳥翼）
  - [STAR DRIVER
    閃亮的塔科特](../Page/STAR_DRIVER_閃亮的塔科特.md "wikilink")（コモリ・ナツオ）
  - [TIGER & BUNNY](../Page/TIGER_&_BUNNY.md "wikilink")（アイザック）
  - [妖怪少爺～千年魔京～](../Page/妖怪少爺#.md "wikilink")（雞冠丸）
  - [最後流亡-銀翼的飛夢-](../Page/最後流亡-銀翼的飛夢-.md "wikilink")（尼可羅）

**2012年**

  - [男子高中生的日常](../Page/男子高中生的日常.md "wikilink")（**忠邦**、豊川）
  - [火影忍者疾風傳](../Page/火影忍者疾風傳.md "wikilink")（波風水門（少年）、第四代水影·櫓）
  - [少年同盟2](../Page/少年同盟.md "wikilink")（**橘千鶴**）
  - [釣球](../Page/釣球.md "wikilink")（**春**）
  - [謎樣女友X](../Page/謎樣女友X.md "wikilink")（**椿明**）
  - [Fate/Zero](../Page/Fate/Zero.md "wikilink")（衛宮切嗣（小時候））
  - [冰菓](../Page/古籍研究社系列#故事簡介.md "wikilink")（杉村二郎）
  - [爆旋陀螺 鋼鐵戰魂 ZERO G](../Page/爆旋陀螺_鋼鐵戰魂#.md "wikilink")（大鳥翼）
  - [遊戲王ZEXALII](../Page/遊戲王ZEXAL#.md "wikilink")（**阿斯特拉爾**）
  - [寵物小精靈超級願望](../Page/寵物小精靈超級願望.md "wikilink")（正臣）
  - [向陽素描×蜂窩](../Page/向陽素描.md "wikilink")（吉野屋的弟弟）

**2013年**

  - [Q弟偵探因幡](../Page/Q弟偵探因幡.md "wikilink")（**野崎圭**）
  - [花牌情緣2](../Page/花牌情緣.md "wikilink")（**筑波秋博**）
  - [黑色嘉年華](../Page/黑色嘉年華.md "wikilink")（夜鷹）
  - [物語系列 第二季](../Page/物語系列_第二季.md "wikilink")（艾比索多）

**2014年**

  - [諸神的惡作劇](../Page/諸神的惡作劇.md "wikilink")（**阿波羅・阿加納・韋萊亞**）
  - [地球隊長](../Page/地球隊長.md "wikilink")（**真夏大地**）
  - [排球少年！！](../Page/排球少年！！.md "wikilink")（**菅原孝支**）

**2015年**

  - [終結的熾天使](../Page/終結的熾天使.md "wikilink")（**百夜優一郎**）
  - [蒼穹之戰神 EXODUS](../Page/蒼穹之戰神.md "wikilink")（春日井甲洋）
  - [終物語](../Page/終物語.md "wikilink")（耶皮索多）
  - 排球少年！！第二季（**菅原孝支**）
  - [小松先生](../Page/小松先生.md "wikilink")（**松野椴松**／門松／トド子／トド美／トティ美、超能貓以外的貓）
  - [終結的熾天使 名古屋決戰篇](../Page/終結的熾天使.md "wikilink")（**百夜優一郎**）

**2016年**

  - [溫暖的印記](../Page/溫暖的印記.md "wikilink")
  - [路人超能100](../Page/路人超能100.md "wikilink")（**影山律**）
  - 排球少年！！烏野高中VS白鳥澤學園高中（**菅原孝支**）

**2017年**

  - 小松先生 第二季（**松野椴松**）

**2018年**

  - [沒有心跳的少女 BEATLESS](../Page/沒有心跳的少女_BEATLESS.md "wikilink")（希金斯）
  - [藍海少女！](../Page/藍海少女！.md "wikilink")～Advance～（彼得）
  - [深夜！天才傻鵬](../Page/深夜！天才傻鵬.md "wikilink")（**傻鵬**）
  - [強風吹拂](../Page/強風吹拂.md "wikilink")（**柏崎茜〈王子〉**）

**2019年**

  - 花牌情緣3（**筑波秋博**）
  - [路人超能100](../Page/路人超能100.md "wikilink") 第二季（**影山律**）
  - 深夜的超自然公務員（**姫塚セオ**）
  - [卡羅爾與星期二](../Page/卡罗尔与星期二.md "wikilink")（**羅迪**）

### OVA、OAD

  - [TSUBASA翼 TOKYO
    REVELATIONS](../Page/TSUBASA翼#OVA.md "wikilink")（**小狼**）
  - TSUBASA翼 春雷纪（**小狼**）
  - [xxxHOLiC 春夢記](../Page/×××HOLiC#xxxHOLiC春夢記.md "wikilink") （**小狼**）
  - [「文學少女」今日的點心 ～初戀～](../Page/文學少女.md "wikilink") （**井上心葉**）
  - 「文學少女」回憶錄 （**井上心葉**）
  - [Code Geass 亡國的AKITO](../Page/Code_Geass_亡國的AKITO.md "wikilink")
    （**日向瑛斗**）
  - [飛輪少年 黑翼與沉睡森林 -Break on the sky-](../Page/飛輪少年#OAD.md "wikilink")
    （**美鞍葛馬**）
  - [怪物王女](../Page/怪物王女.md "wikilink") （**日和見日郎**）
      - 暗闇王女
      - 特急王女
      - 孤島王女
  - [聖鬥士星矢 THE LOST CANVAS
    冥王神話](../Page/聖鬥士星矢_THE_LOST_CANVAS_冥王神話.md "wikilink")（砥草）
  - [限定少女。](../Page/初戀限定。#.md "wikilink")（**財津衛**）
  - [絕對可愛CHILDREN 愛多憎生！被奪走的未來？](../Page/絕對可愛CHILDREN#.md "wikilink")（賈利）
  - [戰場女武神3 為誰而負的槍傷](../Page/戰場女武神3#OVA動畫.md "wikilink") （吉格）
  - [進研研討會高校講座](../Page/進研研討會高校講座.md "wikilink")（**翔太**）
  - [野良劇\!](../Page/野良劇!.md "wikilink")（**好青年**）
  - [模型戰士高達模型大師BEGINNING G](../Page/模型戰士高達模型大師BEGINNING_G.md "wikilink")
    （タツ・シマノ）
  - [奇蹟列車～歡迎來到中央線～](../Page/奇蹟列車.md "wikilink")（吉祥寺拓人）

**2015年**

  - [排球少年！！列夫登場！](../Page/排球少年！！.md "wikilink")（**菅原孝支**）
  - [英雄公司](../Page/英雄公司.md "wikilink")（**天野·銀河**）※漫畫第8卷DVD附贈特裝版
  - [小松先生](../Page/小松君.md "wikilink")（**松野椴松**）

**2016年**

  - [終結的熾天使「吸血鬼沙哈爾」](../Page/終結的熾天使.md "wikilink")（**百夜優一郎**）
  - 排球少年！！「VS不及格」（**菅原孝支**）

**2017年**

  - 排球少年！！「特集！赌在春高排球上的青春」（**菅原孝支**）

### 動畫電影

**2001年**

  - [神隱少女](../Page/神隱少女.md "wikilink")（**白龍**）

**2005年**

  - [TSUBASA翼 鳥籠國的公主](../Page/TSUBASA翼.md "wikilink")（**小狼**）
  - [劇場版×××HOLiC 仲夏夜之夢](../Page/×××HOLiC#.md "wikilink")（**小狼**）

**2006年**

  - [真救世主傳說 北斗之拳 拉歐傳 殉愛之章](../Page/北斗神拳.md "wikilink")（シバ）

**2007年**

  - [Yes\! 光之美少女5
    鏡之國的奇蹟大冒險\!](../Page/Yes!_光之美少女5.md "wikilink")（**果果**）

**2008年**

  - Yes\! 光之美少女5GoGo\! 甜點王國的生日聚會\!（**果果**）

**2009年**

  - [光之美少女 All Stars DX
    大家都是朋友☆奇蹟的全員大集合！](../Page/光之美少女_All_Stars_DX_大家都是朋友☆奇蹟的全員大集合！.md "wikilink")（**果果**）

**2010年**

  - [光之美少女 All Stars DX2
    希望之光☆守護彩虹寶石！](../Page/光之美少女_All_Stars_DX2_希望之光☆守護彩虹寶石！.md "wikilink")（**果果**、宮迫學）
  - [文學少女 劇場版](../Page/文學少女#劇場版.md "wikilink")（**井上心葉**）
  - [爆旋陀螺‧鋼鐵戰魂大電影](../Page/爆旋陀螺_鋼鐵戰魂#劇場版.md "wikilink")（大鳥翼）
  - [劇場版 機動戰士GUNDAM00 -A wakening of the
    Trailblazer-](../Page/劇場版_機動戰士GUNDAM00_-A_wakening_of_the_Trailblazer-.md "wikilink")（沙慈·克洛斯羅德）

**2011年**

  - [光之美少女 All Stars DX3
    傳達到未來！連結世界☆彩虹之花！](../Page/光之美少女_All_Stars_DX3_傳達到未來！連結世界☆彩虹之花！.md "wikilink")（**果果**、宮迫學）
  - [追逐繁星的孩子](../Page/追逐繁星的孩子.md "wikilink")（**瞬**、**心**）
  - [永久之久遠](../Page/永久之久遠.md "wikilink")（千場高雄）
  - [網球王子劇場版 決戰英國網球城](../Page/網球王子_\(動畫\)#第二波.md "wikilink")（彼得）
  - [UN-GO episode:0 因果論](../Page/UN-GO#劇場版.md "wikilink")（速水星玄）

**2012年**

  - [Sacred Seven─銀月之翼─](../Page/Sacred_Seven.md "wikilink")（鏡誠）
  - [Code Geass
    亡國的阿基德第一章](../Page/Code_Geass_亡國的阿基德.md "wikilink")（**日向阿基德**)
  - [劇場版 TIGER & BUNNY -The
    Beginning-](../Page/TIGER_&_BUNNY.md "wikilink")（アイザック）

**2013年**

  - [言葉之庭](../Page/言葉之庭.md "wikilink")（**秋月孝雄**）
  - [劇場版 我們仍未知道那天所看見的花名。](../Page/未聞花名#劇場版.md "wikilink")（**宿海仁太〈小仁〉**）
  - Code Geass 亡國的阿基德 第二章（**日向阿基德**)
  - [魯邦三世VS名偵探柯南 THE
    MOVIE第二章](../Page/魯邦三世VS名偵探柯南_THE_MOVIE.md "wikilink")（艾米利歐·巴瑞帝)

**2015年**

  - Code Geass 亡國的阿基德 第三章（**日向阿基德**)
  - Code Geass 亡國的阿基德 第四章（**日向阿基德**)
  - [百日紅](../Page/百日紅.md "wikilink")（吉彌）
  - [排球少年！！結束與開始](../Page/排球少年.md "wikilink")（**菅原孝支**）
  - 排球少年！！勝利者與失敗者（**菅原孝支**）

**2016年**

  - Code Geass 亡國的阿基德 第五章（**日向阿基德**)
  - [電影 聲之形](../Page/電影版_聲之形.md "wikilink")（**石田將也**）
  - [傷物語](../Page/傷物語.md "wikilink")<II 熱血篇>（耶皮索多）

**2018年**

  - [於離別之朝束起約定之花](../Page/於離別之朝束起約定之花.md "wikilink")（**艾利亞爾**）

**2019年**

  - 電影 [小松先生](../Page/小松先生.md "wikilink") (**松野椴松**）

### 網路動畫

**2006年**

  - ば〜が〜たいむ（**ニエ**)

**2008年**

  - [亡念之扎姆德](../Page/亡念之扎姆德.md "wikilink")（成長後的楊格）

**2012年**

  - [寵物小精靈 黑版2・白版2
    介绍SP動畫](../Page/神奇寶貝_黑2·白2.md "wikilink")（**主人公〈男生〉**)

**2017年**

  - 我透過機器人深愛著你 (**大澤健**)\[4\]

**2018年**

  - 公益廣告 眼鏡的魔法 (**光**)
    [1](https://www.youtube.com/watch?time_continue=1&v=CPvWCdFwx_g)

### 特攝

**2011年**

  - [假面騎士OOO](../Page/假面騎士OOO.md "wikilink")（Ankh的聲音）
  - [超人力霸王外傳キラー ザ
    ビートスター](../Page/超人力霸王外傳キラー_ザ_ビートスター.md "wikilink")（ジャンキラー、ジャンナイン的聲音）
  - [超人力霸王外傳ファイト第二部](../Page/超人力霸王外傳ファイト第二部.md "wikilink")（ジャンナイン的聲音）

### 電影

**1999年**

  - (**平間優**)

**2009年**

  - （一條海人／時空刑事カイト）

  - （**五十嵐廣志**）

### 旁白

**2016年**

  - 日劇《有喜歡的人》介紹節目

**2018年**

  - ＮＨＫ俳句甲子園2018「僕だけの／私だけの17音」

### 吹替

#### 電影

**2018年**

  - [請以你的名字呼喚我](../Page/请以你的名字呼唤我_\(电影\).md "wikilink")（**艾里歐**）

### 電視劇

**1996年**

  - [超人力霸王迪卡](../Page/超人力霸王迪卡.md "wikilink")（孩子）\#8

**2012年**

  - [勇者義彥和惡靈之鑰](../Page/勇者義彥和惡靈之鑰.md "wikilink")（皇帝的聲音）\#11

**2014年**

  - 植物男子ベランダー 多肉愛的劇場（**幻玉**）（聲音出演）

**2016年**

  - [有喜歡的人](../Page/有喜歡的人.md "wikilink")（安西知宏）\#4 \[5\]

**2019年**

  - 絕景偵探 ( 絶景探偵。) (入野自由)\[6\]

### 遊戲

**2002年**

  - [王國之心](../Page/王國之心.md "wikilink")（**Sora**）
  - 塗鴉王國（モノ）

**2003年**

  - [D・N・ANGEL TV Animation
    Series〜紅の翼〜](../Page/天使怪盜#.md "wikilink")（**丹羽大助**）

**2004年**

  - [王國之心 記憶之鏈](../Page/王國之心_記憶之鏈.md "wikilink")（**Sora**）

**2005年**

  - 交響詩篇艾蕾卡7 TR1:NEW WAVE（ムーンドギー〈幼少時〉）
  - [王國之心II](../Page/王國之心II.md "wikilink")（**Sora**）
  - [翼之奇幻旅程](../Page/翼之奇幻旅程.md "wikilink")（**小狼**）
  - [召喚夜響曲 鑄劍物語 \~肇始之石\~](../Page/召喚夜響曲_鑄劍物語_~肇始之石~.md "wikilink")（レミィ）

**2006年**

  - 聖魔戰記3 ～無罪的狂熱～（**ビギナ**）
  - 全民網球（**ユウキ**）

**2007年**

  - [王國之心 Re:COM](../Page/王國之心#.md "wikilink")（**Sora**）
  - [王國之心II FINAL MIX+](../Page/王國之心#.md "wikilink")（**Sora**）
  - 召喚夜想曲雙紀元～精靈們的共鳴～（**亞爾德**）

**2008年**

  - [新·安琪莉可 Special](../Page/新·安琪莉可#.md "wikilink")（**艾倫弗利德**）

**2009年**

  - SD 高達G 世代世紀戰役（HALO〈00〉）
  - 機動戰士高達：Gundam vs Gundam Next Plus（沙慈·克洛斯羅德）
  - [王國之心 358/2days](../Page/王國之心#.md "wikilink")（Sora）
  - [王國之心 coded](../Page/王國之心_coded.md "wikilink")（**Sora(數據型態)**）
  - サイキン恋シテル？（**相羽壯平**）
  - SUNDAY vs. MAGAZINE 集結！頂上大決戰（納茲‧多拉格尼爾）
  - 電擊學園RPG Cross of Venus（夏目智春）
  - VitaminZ（**方丈慧**）
  - [爆旋陀螺 鋼鐵戰魂 全力對戰陀螺盆](../Page/爆旋陀螺_鋼鐵戰魂#遊戲.md "wikilink")（大鳥翼）
  - [爆旋陀螺 鋼鐵戰魂 爆誕！電流天馬](../Page/爆旋陀螺_鋼鐵戰魂#遊戲.md "wikilink")（大鳥翼）
  - 命運之杖（**埃斯特·利諾德**）

**2010年**

  - カエル畑DEつかまえて（**廣瀨優希**）
  - カエル畑DEつかまえて ポータブル（**廣瀨優希**）
  - カエル畑DEつかまえて・夏 千木良参戦\!（**廣瀨優希**）
  - 機動戰士高達 EXTREME VS（沙慈·克洛斯羅德）
  - [王國之心 夢中降生](../Page/王國之心_夢中降生.md "wikilink")（**Vanitas**、Sora）
  - [王國之心 Re:coded](../Page/王國之心#.md "wikilink")（**Sora〈數據型態〉**）
  - 噬神戰士神機解放（カレル・シュナイダー）
  - TAKUYO Mix Box 〜ファーストアニバーサリー〜（廣瀨優希）
  - Happy☆Magic\!（**水無月白夜**）
  - VitaminZ Revolution（**方丈慧**）
  - [爆旋陀螺 鋼鐵戰魂 爆神須佐之男來襲\!](../Page/爆旋陀螺_鋼鐵戰魂#遊戲.md "wikilink")（大鳥翼）
  - [爆旋陀螺 鋼鐵戰魂 頂上決戦\!大爆炸](../Page/爆旋陀螺_鋼鐵戰魂#遊戲.md "wikilink")（大鳥翼）
  - [爆旋陀螺鋼鐵戰魂攜帶版：超絕轉生](../Page/爆旋陀螺_鋼鐵戰魂#遊戲.md "wikilink")（大鳥翼）
  - 恋愛番長 命短し、恋せよ乙女\! Love is Power（風紀番長）
  - 命運之杖 攜帶版（**埃斯特·利諾德**）
  - 命運之杖 ～通向未來的序言～（**埃斯特·利諾德**）
  - 命運之杖 ～通向未來的序言～ 攜帶版（**埃斯特·利諾德**）
  - [七龙珠Heroes](../Page/七龙珠.md "wikilink")（**ビート**）

**2011年**

  - SD Gundam G Generation World（沙慈·克洛斯羅德）
  - カエル畑DEつかまえて・夏 千木良参戦\! ぽーたぶる（**広瀬優希**）
  - [戰場女武神3](../Page/戰場女武神3.md "wikilink")（吉格）
  - [第2次超級機器人大戰Z 破界篇](../Page/第2次超級機器人大戰Z_破界篇.md "wikilink")（HARO）
  - [最終幻想零式](../Page/最終幻想零式.md "wikilink")（**Eight**）
  - Black Robinia（**橘晃太郎**）
  - 遊☆戯☆王ゼアル デュエルターミナル（**阿斯特拉爾**）
  - 命運之杖2～在時空中沉沒的默示錄～（**埃斯特·利諾德**）
  - VitaminXtoZ（**方丈慧**）

**2012年**

  - [未聞花名](../Page/未聞花名.md "wikilink")（**宿海仁太〈小仁〉**）
  - 機動戰士鋼彈極限VS. 火力全開（沙慈·克洛斯羅德）
  - [王國之心 3D〔Dream Drop
    Distance〕](../Page/王國之心#.md "wikilink")（**Sora**、Vanitas）
  - 恋は校則に縛られない\!（**鳴神鴻**）
  - [第2次超級機器人大戰Z 再世篇](../Page/第2次超級機器人大戰Z_再世篇.md "wikilink")（沙慈·克洛斯羅德）
  - 十鬼の絆〜関ヶ原奇譚〜（南雲秀）
  - 百鬼夜行〜怪談ロマンス〜（**椿宗次郎**）
  - [Phantom -PHANTOM OF
    INFERNO-（Xbox360版）](../Page/Phantom_-PHANTOM_OF_INFERNO-#.md "wikilink")（**Zwei/吾妻玲二**）
  - 恋愛番長2 MidnightLesson\!\!\!（**風紀番長**）
  - 命運之杖2FD～獻給你的後記～（**埃斯特·利諾德**）
  - [精靈寶可夢 黑2·白2](../Page/精靈寶可夢_黑2·白2.md "wikilink")(男主人公 恭平)

**2013年**

  - VitaminZ Graduation（**方丈慧**）
  - 超時空要塞30 聯繫銀河的歌聲（**リオン・榊**）
  - [諸神的惡作劇](../Page/諸神的惡作劇.md "wikilink")（**阿波羅・阿加納・韋萊亞**）
  - [王國之心HD 1.5 ReMIX](../Page/王國之心#.md "wikilink")（**Sora**）
  - 百物語〜怪談ロマンス〜（**椿宗次郎**）
  - 十鬼の絆〜花結綴り〜（南雲秀）
  - [火影忍者疾風傳 終極風暴3](../Page/火影忍者疾風傳#.md "wikilink")（第四代水影•櫓）
  - [戰國BASARA4](../Page/戰國BASARA.md "wikilink")（山中鹿之介）

**2015年**

  - 終結的熾天使 BLOODY BLADES (**百夜優一郎**)
  - 終結的熾天使 命運的開端 (**百夜優一郎**)

**2019年**

  - [王國之心III](../Page/王國之心III.md "wikilink")（**Sora**）

## 參考資料

## 外部連結

  - [junction](https://junction99.com/?page_id=141)- 官方網站
  - [入野自由](http://kiramune.jp/artist/irino/) - Kiramune

[Category:日本男性配音員](../Category/日本男性配音員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:日本舞台劇演員](../Category/日本舞台劇演員.md "wikilink")
[Category:日本男性歌手](../Category/日本男性歌手.md "wikilink")

1.
2.
3.
4.
5.
6.