<table>
<thead>
<tr class="header">
<th><p>維也納第一區</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>區徽</p></td>
</tr>
<tr class="even">
<td><center>
<p><a href="https://zh.wikipedia.org/wiki/File:Wien_Wappen_Innere_Stadt.png" title="fig:Wien_Wappen_Innere_Stadt.png">Wien_Wappen_Innere_Stadt.png</a></p>
</center></td>
</tr>
<tr class="odd">
<td><p>名稱:</p></td>
</tr>
<tr class="even">
<td><p>面積:</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/人口.md" title="wikilink">人口</a>:</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/人口密度.md" title="wikilink">人口密度</a>:</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/郵編.md" title="wikilink">郵編</a>:</p></td>
</tr>
<tr class="even">
<td><p>區政府地址:</p></td>
</tr>
<tr class="odd">
<td><p>官方網葉:</p></td>
</tr>
</tbody>
</table>

**内城區**亦被稱為[維也納的](../Page/維也納.md "wikilink")**第一區**，為是[奧地利維也納老城的中心地區](../Page/奧地利.md "wikilink")。在1850年代維也納合併其他城區之前，今日的第一區也就是當時的整個維也納。

第一區有10 0745名就業人口，是維也納最大的提供職業的城區。由於地處市中心，第一區吸引了大量的遊客，也吸引大量企業在此開設業務機構。

[Category:維也納分區](../Category/維也納分區.md "wikilink")
[Category:奥地利世界遗产](../Category/奥地利世界遗产.md "wikilink")
[Category:维也纳建筑物](../Category/维也纳建筑物.md "wikilink")