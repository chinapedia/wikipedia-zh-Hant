[Lough_Neagh_location.png](https://zh.wikipedia.org/wiki/File:Lough_Neagh_location.png "fig:Lough_Neagh_location.png")的位置\]\]
**內湖**（Lough
Neagh），又譯**內伊湖**，是[英国最大的](../Page/英国.md "wikilink")[湖泊](../Page/湖泊.md "wikilink")，位于[北爱尔兰地区中部](../Page/北爱尔兰.md "wikilink")，面积388平方公里。在整个[西欧](../Page/西欧.md "wikilink")，内湖的面积也可排到第三位，仅次于[日内瓦湖和](../Page/日内瓦湖.md "wikilink")[博登湖](../Page/博登湖.md "wikilink")。

## 風景

<File:Loughneagh.jpg>|[蒂龍郡附近的水域](../Page/蒂龍郡.md "wikilink") <File:Lough>
Neagh - geograph.org.uk - 126920.jpg|阿德莫爾 <File:Lough> Neagh at Shane's
Castle - geograph.org.uk -
155427.jpg|[安特里姆郡](../Page/安特里姆郡.md "wikilink")
<File:Lough> Neagh from Gawley's Gate Quay - geograph.org.uk -
59139.jpg|安特里姆郡境內的內伊湖 <File:Maghery> Country Park - geograph.org.uk -
51880.jpg|[阿馬郡的水域](../Page/阿馬郡.md "wikilink") <File:Ballyronan> Picnic
area - geograph.org.uk -
226655.jpg|在[倫敦德里郡的內伊湖](../Page/倫敦德里郡.md "wikilink")

## 外部链接

  - [Lough Neagh Boating Heritage
    Association](http://www.loughneaghboats.org)
  - [Lough Neagh Rescue](http://www.loughneaghrescue.com)
  - [BBC News on
    pollution](http://news.bbc.co.uk/1/hi/northern_ireland/2785671.stm)
  - [BBC News on ownership of Lough
    Neagh](http://212.58.240.132/1/hi/northern_ireland/4491127.stm)
  - [Google卫星地图](http://maps.google.com/maps?ll=54.645996,-6.411896&spn=0.465099,0.617088&t=k&hl=en)
    [贝尔法斯特位于其右手边](../Page/贝尔法斯特.md "wikilink")
  - [Oxford Island National Nature Reserve](http://www.oxfordisland.com)

[Category:北爱尔兰湖泊](../Category/北爱尔兰湖泊.md "wikilink")
[Category:英国地理之最](../Category/英国地理之最.md "wikilink")
[Category:淡水湖](../Category/淡水湖.md "wikilink")