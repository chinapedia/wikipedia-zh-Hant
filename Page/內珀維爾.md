**瑞柏**（）是[美國](../Page/美國.md "wikilink")[伊利諾伊州](../Page/伊利諾伊州.md "wikilink")[杜佩奇县的一個城市](../Page/杜佩奇縣_\(伊利诺伊州\).md "wikilink")，小部分位於[威爾縣](../Page/威尔县_\(伊利诺伊州\).md "wikilink")。面積92.0平方公里，2007年人口為147,779人，是該州第四大城市。\[1\]

1831年開埠，1857年成為建制村，1890年設市。

## 姐妹城市

  - [尼特拉](../Page/尼特拉.md "wikilink")

## 参考文献

[Category:伊利諾伊州城市](../Category/伊利諾伊州城市.md "wikilink") [Category:杜佩奇縣城市
(伊利諾伊州)](../Category/杜佩奇縣城市_\(伊利諾伊州\).md "wikilink") [Category:威爾縣城市
(伊利諾伊州)](../Category/威爾縣城市_\(伊利諾伊州\).md "wikilink")
[Category:芝加哥大都市区](../Category/芝加哥大都市区.md "wikilink")
[Category:1831年建立的聚居地](../Category/1831年建立的聚居地.md "wikilink")
[Category:1831年伊利諾州建立](../Category/1831年伊利諾州建立.md "wikilink")

1.  [Naperville, Illinois - Population Finder - American
    FactFinder](http://factfinder.census.gov/servlet/SAFFPopulation?_event=Search&geo_id=16000US3251800&_geoContext=01000US%7C04000US32%7C16000US3251800&_street=&_county=Naperville+city&_cityTown=Naperville+city&_state=04000US17&_zip=&_lang=en&_sse=on&ActiveGeoDiv=geoSelect&_useEV=&pctxt=fph&pgsl=160&_submenuId=population_0&ds_name=null&_ci_nbr=null&qr_name=&reg=%3Anull&_keyword=&_industry=)