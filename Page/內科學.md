**內科學**是[臨床醫學的專科](../Page/臨床醫學.md "wikilink")，幾乎是所有其他臨床醫學的基礎，亦有[醫學之母之稱](../Page/醫學.md "wikilink")。內容包含了對[疾病的定義](../Page/疾病.md "wikilink")、[病因學](../Page/病因學.md "wikilink")、致病機理、[流行病學](../Page/流行病學.md "wikilink")、自然史、[症狀](../Page/症狀.md "wikilink")、症候、實驗[診斷](../Page/診斷.md "wikilink")、影像檢查、鑑別診斷、診斷、[治療和預後等](../Page/治療.md "wikilink")。

內科學的方法是透過[病史詢問或面談後](../Page/病史.md "wikilink")，進行[病理學檢查](../Page/病理學.md "wikilink")，根據病史與檢查所見做實驗診斷與影像檢查，以期在眾多鑑別診斷中排除可能性較低者，獲得最有可能的診斷；獲得診斷後，內科的治療方法包含追蹤觀察、生活方式、[藥物](../Page/藥物.md "wikilink")、介入性治療（如心導管、[內視鏡](../Page/內視鏡.md "wikilink")）等，根據病人的狀況調整藥物之使用，防止並處理[副作用及](../Page/副作用.md "wikilink")[併發症](../Page/併發症.md "wikilink")。

## 內科學的次專科

[Relación_Médico_Paciente.png](https://zh.wikipedia.org/wiki/File:Relación_Médico_Paciente.png "fig:Relación_Médico_Paciente.png")
內科學包含了依不同器官系統和病因而分類的次專科：[心脏内科](../Page/心脏内科.md "wikilink")、[呼吸科](../Page/呼吸科.md "wikilink")、[肝膽腸胃科](../Page/肝膽腸胃科.md "wikilink")、[腎臟科](../Page/腎臟科.md "wikilink")（泌尿内科）、[血液科](../Page/血液科.md "wikilink")、[腫瘤科](../Page/腫瘤科.md "wikilink")、[內分泌科](../Page/內分泌科.md "wikilink")、[传染病科](../Page/传染病科.md "wikilink")、[免疫科](../Page/免疫科.md "wikilink")、[風濕科](../Page/風濕科.md "wikilink")、[神經科](../Page/神經科.md "wikilink")、[儿科](../Page/儿科.md "wikilink")、[神经内科](../Page/神经内科.md "wikilink")、[老年病科等](../Page/老年病科.md "wikilink")，但各地甚至各醫院對次專科的分類可能有所不同。各次專科多半有成立各自的醫學會，各自審查次專科醫師的資格並舉辦學術活動。廣義的內科學更包含了[皮膚科](../Page/皮膚科.md "wikilink")、[眼科](../Page/眼科学.md "wikilink")、[精神醫學](../Page/精神醫學.md "wikilink")、[復健科](../Page/復健科.md "wikilink")（[康复科](../Page/康复科.md "wikilink")）、[放射科](../Page/影像诊断学.md "wikilink")、[安宁缓和科](../Page/临终关怀.md "wikilink")、[環境及職業病科等非用](../Page/環境及職業病科.md "wikilink")[外科](../Page/外科.md "wikilink")[手术方式治療之專科](../Page/手术.md "wikilink")。

## 参见

  - [内科医师](../Page/内科医师.md "wikilink")

### 受控医学词表

  - [CPT](../Page/CPT.md "wikilink")
  - [ICD](../Page/ICD.md "wikilink")
  - [LOINC](../Page/LOINC.md "wikilink")
  - [MeSH](../Page/MeSH.md "wikilink")
  - [SNOMED](../Page/SNOMED.md "wikilink")
  - [SNOMED RT](../Page/SNOMED_RT.md "wikilink")
  - [SNOMED CT](../Page/SNOMED_CT.md "wikilink")
  - [UMLS](../Page/UMLS.md "wikilink")

[Category:內科學](../Category/內科學.md "wikilink")