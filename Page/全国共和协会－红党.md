**全国共和协会－红党**（），简称**红党**，是[巴拉圭的](../Page/巴拉圭.md "wikilink")[右翼保守派](../Page/右翼.md "wikilink")[政党](../Page/政党.md "wikilink")，曾長期執政。目前该党的主要競爭對手是[自由主義的](../Page/自由主義.md "wikilink")[真正激進自由黨](../Page/真正激進自由黨.md "wikilink")。

## 历史

1887年9月11日創黨，1947年开始執政，一直到2008年才被農民團體、工會與傳統派的自由黨組成中間偏左的[愛國變革聯盟擊敗](../Page/愛國變革聯盟.md "wikilink")，結束其60年的統治。\[1\][2013年巴拉圭大選](../Page/2013年巴拉圭大選.md "wikilink")，紅黨擊敗真正激進自由黨，重掌政權。

## 参考文献

[Category:巴拉圭政黨](../Category/巴拉圭政黨.md "wikilink")
[Category:保守主義政黨](../Category/保守主義政黨.md "wikilink")
[Category:1887年建立的组织](../Category/1887年建立的组织.md "wikilink")

1.  [前主教支持率领先
    连续执政61年巴拉圭红党或败选](http://www.chinadaily.com.cn/hqgj/2008-04/19/content_6628309.htm)