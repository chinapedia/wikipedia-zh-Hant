**全国科学技术名词审定委员会**，简称**全国科技名词委**、**名词委**，是经[中华人民共和国国务院授权](../Page/中华人民共和国国务院.md "wikilink")\[1\]，代表[中华人民共和国进行科技名词审定](../Page/中华人民共和国.md "wikilink")、公布的权威性政府机构。原称**全国自然科学名词审定委员会**，1985年经国务院批准成立。

## 历史沿革

1909年11月2日[清朝](../Page/清朝.md "wikilink")[学部正式成立](../Page/學部_\(清朝\).md "wikilink")**科学名词编订馆**\[2\]。
1932年6月14日[中华民国成立](../Page/中华民国.md "wikilink")**[国立编译馆](../Page/国立编译馆.md "wikilink")**，隶属当时的[教育部](../Page/中华民国教育部.md "wikilink")\[3\]。
1950年[中国科学院编译局接管国立编译馆拟订的各科名词之草案](../Page/中国科学院.md "wikilink")，当年5月，[政务院文化教育委员会成立](../Page/政务院文化教育委员会.md "wikilink")**学术名词统一工作委员会**，下设[自然科学](../Page/自然科学.md "wikilink")、[社会科学](../Page/社会科学.md "wikilink")、[医药](../Page/医药.md "wikilink")[卫生](../Page/卫生.md "wikilink")、[文学](../Page/文学.md "wikilink")[艺术与时事五个组](../Page/艺术.md "wikilink")，并按学科再分小组；中国科学院受政务院文化教育委员会之托负责自然科学组，工作范围涵盖[天文学](../Page/天文学.md "wikilink")、[数学](../Page/数学.md "wikilink")、[物理学](../Page/物理学.md "wikilink")、[化学](../Page/化学.md "wikilink")、[动物学](../Page/动物学.md "wikilink")、[植物学](../Page/植物学.md "wikilink")、[地质学](../Page/地质学.md "wikilink")、[地理学](../Page/地理学.md "wikilink")、[古生物学](../Page/古生物学.md "wikilink")、[考古学](../Page/考古学.md "wikilink")、[心理学](../Page/心理学.md "wikilink")、[语言学](../Page/语言学.md "wikilink")、[地球物理学](../Page/地球物理学.md "wikilink")、[工程与](../Page/工程.md "wikilink")[农学等等](../Page/农学.md "wikilink")。委员会主任委员为[郭沫若](../Page/郭沫若.md "wikilink")，聘任的科学家达150人，包括[严济慈](../Page/严济慈.md "wikilink")、[华罗庚](../Page/华罗庚.md "wikilink")、[钱三强](../Page/钱三强.md "wikilink")、[冯德培](../Page/冯德培.md "wikilink")、[茅以升](../Page/茅以升.md "wikilink")、[吕叔湘](../Page/吕叔湘.md "wikilink")，在编译局下设名词室，负责组织名词编订等工作。[20世纪60年代中期](../Page/20世纪60年代.md "wikilink")，由于[文化大革命](../Page/文化大革命.md "wikilink")，审定工作基本中断。文革后成立全国自然科学名词审定委员会\[4\]，并于1996年更名为全国科学技术名词审定委员会\[5\]。

## 历任主任

1.  [钱三强](../Page/钱三强.md "wikilink")
2.  [卢嘉锡](../Page/卢嘉锡.md "wikilink")
3.  [路甬祥](../Page/路甬祥.md "wikilink")

## 参考文献

## 外部链接

  - [全国科学技术名词审定委员会](http://www.cnctst.cn/)
  - [术语在线](http://www.termonline.cn/)<small>（委员会旗下术语知识服务平台）</small>

## 参见

  - [國立編譯館](../Page/國立編譯館.md "wikilink")

[Category:中华人民共和国科技组织](../Category/中华人民共和国科技组织.md "wikilink")
[Category:中华人民共和国国务院](../Category/中华人民共和国国务院.md "wikilink")
[Category:1950年建立的組織](../Category/1950年建立的組織.md "wikilink")

1.  国函\[1987\]142：“全国自然科学名词审定委员会是经国务院批准成立的。审定、公布各学科名词是该委员会的职权范围，经其审定的自然科学名词具有权威性和约束力；全国各科研、教学、生产、经营、新闻出版等单位应遵照使用。”
2.
3.
4.
5.