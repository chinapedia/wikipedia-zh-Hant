**全州县**（[全州话](../Page/全州话.md "wikilink")：）位于[中国](../Page/中华人民共和国.md "wikilink")[广西东北部](../Page/广西.md "wikilink")，是[桂林市的一個縣](../Page/桂林市.md "wikilink")，毗邻[湖南省](../Page/湖南省.md "wikilink")。南距桂林市125公里，北距湖南永州市79公里。[湘江](../Page/湘江.md "wikilink")、[湘桂铁路](../Page/湘桂铁路.md "wikilink")、[泉南高速公路和](../Page/泉南高速公路.md "wikilink")[322国道穿过](../Page/322国道.md "wikilink")，是中原进入广西的门户。[面积](../Page/面积.md "wikilink")4021[平方千米](../Page/平方千米.md "wikilink")，下辖9镇9乡，[人口](../Page/人口.md "wikilink")82万，是桂林面积最大和人口最多的一个县。[邮政编码](../Page/邮政编码.md "wikilink")541500。县人民政府驻全州镇。

历史上曾经隶属[湖南省](../Page/湖南省.md "wikilink")，历属[零陵郡](../Page/零陵郡.md "wikilink")、[湘源县等](../Page/湘源县.md "wikilink")。[明](../Page/明朝.md "wikilink")[洪武二十七年](../Page/洪武.md "wikilink")（公元1394年），全州被划归广西桂林府。

全州县与临近的湖南省交往密切，属于广西的四大[湘语县之一](../Page/湘语.md "wikilink")。当地语言主要为[湘语](../Page/湘语.md "wikilink")[永全片的](../Page/永全片.md "wikilink")[全州话](../Page/全州话.md "wikilink")，部分乡镇还通行[湘南土话](../Page/湘南土话.md "wikilink")（韶州土话）以及少数民族语言如[瑶语](../Page/瑶语.md "wikilink")。

县内主要景点有：建于唐代的湘山寺、妙明塔、镇湘塔、清代著名画家石涛等的字画石刻等名胜古迹和高山明镜般的天湖水库、亚洲第一高水头（1074米）的天湖电站、龙岩、炎井温泉以及神奇的“风景鱼泉”等。

特色食品有：红油米粉、黑醋血鸭和清代宫廷贡品乌鲤[禾花鱼等](../Page/禾花鱼.md "wikilink")。

## 交通

  - 国道322
  - 省道201
  - 省道303

G72泉南高速

## 行政区划

下辖9镇，7乡、2民族乡：

  - 镇：[全州镇](../Page/全州镇.md "wikilink")、[石塘镇](../Page/石塘鎮_\(全州縣\).md "wikilink")、[绍水镇](../Page/绍水镇.md "wikilink")、[庙头镇](../Page/庙头镇.md "wikilink")、[才湾镇](../Page/才湾镇.md "wikilink")、[黄沙河镇](../Page/黄沙河镇.md "wikilink")、[文桥镇](../Page/文桥镇.md "wikilink")、[大西江镇](../Page/大西江镇.md "wikilink")、[龙水镇](../Page/龙水镇.md "wikilink")。
  - 乡：[枧塘乡](../Page/枧塘乡.md "wikilink")、[永岁乡](../Page/永岁乡.md "wikilink")、[咸水乡](../Page/咸水乡.md "wikilink")、[凤凰乡](../Page/鳳凰鄉_\(全州縣\).md "wikilink")、[安和乡](../Page/安和乡.md "wikilink")、[两河乡](../Page/两河乡.md "wikilink")、[白宝乡](../Page/白宝乡.md "wikilink")。
  - 民族乡：[东山瑶族乡](../Page/东山瑶族乡.md "wikilink")、[蕉江瑶族乡](../Page/蕉江瑶族乡.md "wikilink")。

## 名人

  - [蔣良騏](../Page/蔣良騏.md "wikilink")，[清朝](../Page/清朝.md "wikilink")[乾隆十六年](../Page/乾隆.md "wikilink")（1751年）中辛未進士，歷任[翰林院編修](../Page/翰林院編修.md "wikilink")、[國史館編纂](../Page/國史館.md "wikilink")。[乾隆三十年十月重開國史館於東華門內](../Page/乾隆.md "wikilink")，任纂修官，開始抄寫《[東華錄](../Page/東華錄.md "wikilink")》。官至[通政使](../Page/通政使.md "wikilink")。生前兩袖清風，死後葬於[才灣鎮紗帽嶺](../Page/才灣鎮.md "wikilink")。著有《東華錄》，此外还著有《下学录》、《京门草》、《伤神杂咏》、《釜纪游》等。
  - [舒弘志](../Page/舒弘志.md "wikilink")，自幼聰穎，過目成誦，人稱神童，[明朝](../Page/明朝.md "wikilink")[萬曆十四年](../Page/萬曆.md "wikilink")（1586年）榜眼，年僅十九歲，語多譏刺時政。授[翰林院編修](../Page/翰林院編修.md "wikilink")，有《學始於不欺暗室說》、《真正英雄從戰戰兢兢來論》、《淡泊寧靜說》等文章傳世。

[全州县](../Category/全州县.md "wikilink") [县](../Category/桂林区县.md "wikilink")
[桂林](../Category/广西县份.md "wikilink")