**激光全息存储技术**是一种利用[激光](../Page/激光.md "wikilink")[全息摄影原理将图文等](../Page/全息摄影.md "wikilink")[信息记录在](../Page/信息.md "wikilink")[感光介质上的大容量信息存储技术](../Page/感光介质.md "wikilink")，它有可能取代磁存储和光学存储技术，成为下一代的高容量数据存储技术。传统的存储方式将每一个比特都记为记录介质表面磁或光的变化，而全息存储中将信息记录在介质的体积内，而且利用不同角度的光线可以在同样的区域内记录多个信息图像。

另外，磁存储和光存储每次都只能读写一个比特的信息，而全息存储可以并行的读写数百万比特，这样可以使信号的传输速率大大超过目前光存储的速度\[1\]
。

## 记录数据

全息数据存储在光敏光学材料上通过非光学相干图样来记录信息。一束激光首先被分成两部分，产生暗像素和亮像素。通过调整参考光的角度、波长和介质的位置，理论上可以在同一个空间记录下数千比特张全息图像。数据存储密度的极限是几十TB/立方厘米。2006年，[InPhase科技发表了一份白皮书](../Page/InPhase科技.md "wikilink")，称他们已经实现了500Gb/平方英寸的存储密度。据此我们可以推测出一张普通的光盘（写入半径大约4厘米）可以存储约3896.6Gb的信息\[2\]。

## 读取数据

通过重新产生相通的参考光来重建全息图像可以将存储的数据读出。参考光聚焦在光敏材料之上，照亮了合适的[干涉图样](../Page/干涉图样.md "wikilink")，光线在干涉图样上发生[衍射](../Page/衍射.md "wikilink")，衍射图案投影到检测器上。检测器可以并行的将数据读出，一次就可以读出超过1兆比特的信息，因此数据率非常高。记录在全息驱动器中的文件的访问时间可以做到在200[毫秒以下](../Page/毫秒.md "wikilink")\[3\]。

## 保存寿命

全息数据存储可以为公司提供保存信息的新方法。如果使用一次写入多次读取的方法，可以保证内容的安全，防止存储的的信息被重写或者修改。全息存储制造商认为，这种技术可以提供安全的数据存储方案，储存数据的内容50年也不会发生变化，远远超过当前的数据存储技术。反对观点认为，数据读取技术每十年就会发生巨大的变化，因此尽管有能力将数据保存50-100年，但是很有可能需要用到数据的时候却无法找到合适的读取设备来读取\[4\]。然而，性能很好的存储方案可以持续使用很长的时间，另外，即使技术更新换代了，仍然可能有后向兼容的解决方案，就像现在的[BD技术后向兼容](../Page/BD.md "wikilink")[CD及](../Page/CD.md "wikilink")[DVD技术一样](../Page/DVD.md "wikilink")。

## 常用术语

**敏感性**指的是单位曝光量所能产生的[折射率调制的变化幅度](../Page/折射率.md "wikilink")。衍射效率和[调制指数与有效厚度的平方成正比](../Page/调制指数.md "wikilink")。

**动态范围**决定了在同一个体积内可以存储多少张全息图像。

**[空间光调制器](../Page/空间光调制器.md "wikilink")**是像素化输入设备（液晶面板），用来将数据叠加在物光线上。

## 技术细节

[Hologram_maken_(1).svg](https://zh.wikipedia.org/wiki/File:Hologram_maken_\(1\).svg "fig:Hologram_maken_(1).svg")
[Hologram_lezen.svg](https://zh.wikipedia.org/wiki/File:Hologram_lezen.svg "fig:Hologram_lezen.svg")
一般情况下，全息存储是一种一次写入多次读写的存储技术，这是由于在写入数据的时候，存储介质发生了不可逆的变化。可重写的全息存储技术可以通过晶体的[光致折变效应来实现](../Page/光致折变.md "wikilink")：

  - 从两个光源产生的[相干光可以在介质上产生干涉图样](../Page/相干.md "wikilink")。这两束光分别称为[参考光和](../Page/参考光.md "wikilink")[信号光](../Page/信号光.md "wikilink")。
  - 当出现相长[干涉的时候](../Page/干涉.md "wikilink")，干涉图样显示为亮班，材料中的电子接受了光的能量，可以发生从材料[价带向](../Page/价带.md "wikilink")[导带的](../Page/导带.md "wikilink")[跃迁](../Page/跃迁.md "wikilink")。电子跃迁后价带含有正电，留下的位置称为[空穴](../Page/空穴.md "wikilink")。在可重写的全息图像材料中，要求空穴不可移动。当出现相消干涉的时候，光的能量比较低，因此电子不会发生跃迁。
  - 导带中的电子可以在材料中自由移动。电子的运动受到两种相反的力的作用，第一种是电子和跃迁后留下的空穴之间的库仑力的作用，这个力使得电子难以移动，甚至会将电子拉回空穴。第二个力是[扩散作用产生的](../Page/扩散.md "wikilink")，它使电子移向电子密度较低的地方。如果库仑力不够强，电子就会移动至暗条纹处。
  - 在电子跃迁一开始，电子就有一定的概率与空穴重新结合，回到价带。结合率越高，电子能够移动到暗条纹处的数量就越少。这个速度会影响全息图像的强度。
  - 在一部分电子移动到暗条纹处并与其中的空洞结合以后，在暗区的电子和在亮区的空穴间就会建立一个永久的空间[电场](../Page/电场.md "wikilink")。由于[电光效应](../Page/电光效应.md "wikilink")，这个电场会影响到晶体的[折射率](../Page/折射率.md "wikilink")。

当信息需要从[全息图像中读取出来的时候](../Page/全息图像.md "wikilink")，只需要参考光就可以重建全息图像。参考光以和写入全息图像的时候完全相同的放射照射在材料上。由于写入的时候折射率发生了变化，光线会分裂为两部分，其中之一将会重建存储了信息的信号光。一些检测器比如[电荷耦合元件](../Page/电荷耦合元件.md "wikilink")（CCD）照相机可以用来将信息转化为更容易使用的形式。

理论上一个边长为写入光波波长的立方体可以存储1比特的信息。例如，[氦氖激光器所发出的红色激光波长为](../Page/氦氖激光器.md "wikilink")632.8[纳米](../Page/纳米.md "wikilink")，每立方毫米就可以存储4Gb的数据。实际上，数据密度会远远低于理论密度，主要是由于以下几个原因：

  - 需要使用纠错编码
  - 需要考虑光学系统的缺陷和约束
  - 高密度的记录的成本也会更高，需要考虑到成本和性能的折衷
  - 设计技术的约束（目前磁记录的硬盘已经面临着这个问题了，磁畴结构使得硬盘制造无法达到理论上的极限）

现在的存储技术每次都只能读写一个比特的信息，而全息存储技术可以使用一束光并行的读写数据\[5\]。

## 双色记录

[Holographic_Data_Storage.svg](https://zh.wikipedia.org/wiki/File:Holographic_Data_Storage.svg "fig:Holographic_Data_Storage.svg")

在双色全息记录的过程中，参考光和信号光固定使用一个特殊的波长（绿光、红光、甚至红外光），而敏化/开关光束为一个单独的短波长激光（蓝光或者紫外光）。敏化/开光光束用来使材料在记录过程之前和之后变得敏感，而信息将通过参考光和信号光在[晶体中记录下来](../Page/晶体.md "wikilink")。开光光束在记录过程中会间歇性的照射，以测量衍射光的强度。在读取的过程中，仍然通过单独照射参考光来实现，由于参考光波长较长，它无法在使被束缚的电子在读取阶段激发，因此需要短波长的敏化光来擦除记录的信息。

通常为了进行双色全息记录，需要使用两种不同的掺杂物来增加俘获中心，这两种掺杂物分别是[过渡金属和](../Page/过渡金属.md "wikilink")[稀土元素](../Page/稀土元素.md "wikilink")，他们对特定波长的光波很敏感。通过使用这两种掺杂物，可以在[硝酸锂晶体中产生更多的俘获中心](../Page/硝酸锂.md "wikilink")，准确地说是一个浅陷阱和一个深陷阱。这个概念现在用于使用敏化光将电子从比价带更深的深阱中激发至导带，然后使电子被接近导带的浅陷阱俘获。参考光和信号光用来使电子从浅陷阱中激发并返回深陷阱。这样，信息将被储存在深陷阱中。读取信息的时候仅仅使用参考光就可以了，而由于参考光的波长较长，能量较低，因此不会使电子从深陷阱中激发。

### 硝酸锂晶体双掺杂退火效应

对于双掺杂的硝酸锂晶体，存在一个最佳的[氧化](../Page/氧化.md "wikilink")/[还原的状态](../Page/还原.md "wikilink")，在这里可以达到最好的性能。这个最优点和深陷阱和浅陷阱的掺杂水平和以及晶体样品的[退火条件有关](../Page/退火.md "wikilink")。这个最优的状态一般出现在95%-98%的深陷阱都填满了。在强氧化环境中制备的样品中，全息图像无法很容易的记录下来，衍射效率也非常低。这是由于浅陷阱完全是空的，深陷阱中也缺乏电子。在高还原性环境中制备的样品中，深陷阱完全被填满了，而浅陷阱也几乎被填满了，这使得材料对光非常敏感（可以用于快速记录），而且也有很高的衍射效率。然而在读取阶段，所有的深陷阱很快就被填满了，全息图的产生依赖于浅陷阱中的电子，但是这部分电子会在读取阶段被激发，从而使得信息被删除了。因此在数次度区后，衍射效率会降低至0，而储存的全息图无法恢复了。

## 开发营销

在2002年，全世界主要有三家公司在进行全息存储方面的研究，这些公司是美国的[InPhase科技和](../Page/InPhase科技.md "wikilink")[美国万胜公司](../Page/美国万胜公司.md "wikilink")，以及日本的[Optware公司](../Page/Optware.md "wikilink")\[6\]。尽管全息存储的技术自从二十世纪六十年代就开始讨论\[7\]，而且至少从2001年就开始兜售接近使用的商用方案\[8\]，但是直到现在仍然在试图使人相信这项技术会找到合适的市场\[9\]。从2002年开始，计划中的全息存储产品还并不想与硬盘展开竞争的，而是试图寻找到能够利用到它的特别的优点的市场，如需要很高访问速度的应用等等。

2005年，在[拉斯维加斯举行的国家广播协会会议上](../Page/拉斯维加斯.md "wikilink")，[InPhase科技在](../Page/InPhase科技.md "wikilink")[美国万胜公司展位上公开展示了世界上第一个使用全息存储技术的商业存储设备原型](../Page/美国万胜公司.md "wikilink")。
[InPhase科技在](../Page/InPhase科技.md "wikilink")2006年和2007年数次宣称将会推出其旗舰性产品，然而在不停的延期推迟发布以后，于2010年2月关闭。它的资产由[科罗拉多州没收以偿还欠税](../Page/科罗拉多州.md "wikilink")。这家公司共花费了一亿美元，但是投资者无法再筹集更多的资本了。\[10\]\[11\]

2009年，[通用电气全球研究中心展示了他们自行研究的全息存储材料](../Page/通用电气全球研究中心.md "wikilink")，这种材料可以用于光盘，使用的读取技术和目前的[蓝光光盘播放器类似](../Page/蓝光光盘.md "wikilink")\[12\]。

## 另见

  - [全息万用卡](../Page/全息万用卡.md "wikilink")
  - [全息万用光盘](../Page/全息万用光盘.md "wikilink")
  - [全息内存](../Page/全息内存.md "wikilink")
  - [三维光学数据存储](../Page/三维光学数据存储.md "wikilink")
  - [全息摄影](../Page/全息摄影.md "wikilink")

## 参考文献

## 外部链接

  - [全息存储工作原理](http://computer.howstuffworks.com/holographic-memory.htm)
  - [大宇电子为全息数据存储开发的全世界第一个高精度伺服运动控制系统](http://sine.ni.com/csol/cds/item/vw/p/id/685/nid/124300)
  - [基于页与基于比特的全息存储的比较](https://web.archive.org/web/20110723203652/http://www.media-tech.net/fileadmin/templates/resources/sc06/mtc06_keynote_day2_hesselink_yuzuru.pdf)
  - [万胜全息媒体新闻稿](https://web.archive.org/web/20090212184955/http://www.maxell-usa.com/index.aspx?id=-5;0;246;0&a=read&pid=100)
  - [通用电子全球研究中心开发的Tb光盘和播放器](http://www.technologyreview.com/computing/21507)

[category:電腦儲存](../Page/category:電腦儲存.md "wikilink")

[Category:資訊儲存](../Category/資訊儲存.md "wikilink")

1.

2.

3.  Robinson, T. (2005, June). The race for space. netWorker. 9,2.
    Retrieved April 28, 2008 from ACM Digital Library.

4.
5.  ["Maxell Introduces the Future of Optical Storage Media With
    Holographic Recording Technology", (2005) retrieved
    January 27, 2007](http://www.maxell-usa.com/index.aspx?id=-5;0;158;0&a=read&pid=49)


6.

7.

8.

9.

10. Engadget, [“InPhase delays Tapestry holographic storage solution to
    late 2009”](http://www.engadget.com/2008/11/03/inphase-delays-tapestry-holographic-storage-solution-to-late-200/)

11. Television Broadcast, [“Holographic Storage Firm InPhase
    Technologies Shuts
    Down”](http://www.televisionbroadcast.com/article/94340)

12. [GE Unveils 500-GB, Holographic Disc Storage
    Technology](http://www.crn.com/storage/217200230;jsessionid=PCLSSR1JXVD1OQSNDLOSKHSCJUNN2JVN)