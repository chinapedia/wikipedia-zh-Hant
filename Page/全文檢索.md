從文本或資料庫中，不限定資料欄位，自由地萃取出訊息的技術。

執行全文檢索任務的程式，一般稱作[搜尋引擎](../Page/搜尋引擎.md "wikilink")，它將使用者隨意輸入的文字，試圖從資料庫中，找到符合的內容。

## 全文索引的相關議題

  - 語根處理 (stemming)
  - [符素解析器](../Page/符素.md "wikilink") (token parser) 1-gram, 2-gram ,
    n-gram
  - [斷詞](../Page/斷詞.md "wikilink")/分詞 word segmentation
  - \-{zh-hans:[倒排索引](../Page/倒排索引.md "wikilink");zh-hant:[反向索引](../Page/反向索引.md "wikilink");}-
    inverted index

## 演算法、搜尋策略之模型

  - 布林式 boolean
  - 統計模型 Probabilistic model
  - [向量空間模型](../Page/向量空間模型.md "wikilink") vector base model
  - 隱性語義模型 Latent semantic model

## 評量之準則

  - 查全率(recall rate)，查準率(precision)

## 開放原始碼之全文檢索系統

  - [Apache Solr](../Page/Solr.md "wikilink")
  - [BaseX](../Page/BaseX.md "wikilink")
  - [Clusterpoint Server](../Page/Clusterpoint.md "wikilink") [(freeware
    licence for a
    single-server)](https://web.archive.org/web/20140328121334/http://www.clusterpoint.com/)
  - [DataparkSearch](../Page/DataparkSearch.md "wikilink")
  - [Ferret](../Page/Ferret_search_library.md "wikilink")
  - [Ht-//Dig](../Page/Ht-/Dig.md "wikilink")
  - [Hyper Estraier](../Page/Hyper_Estraier.md "wikilink")
  - [KinoSearch](../Page/KinoSearch.md "wikilink")
  - [Lemur/Indri](../Page/Lemur_Project.md "wikilink")
  - [Lucene](../Page/Lucene.md "wikilink")
  - [mnoGoSearch](../Page/mnoGoSearch.md "wikilink")
  - [Sphinx](../Page/Sphinx_\(search_engine\).md "wikilink")
  - [Swish-e](../Page/Swish-e.md "wikilink")
  - [Xapian](../Page/Xapian.md "wikilink")
  - [ElasticSearch](../Page/ElasticSearch.md "wikilink")

## 和中文有關的議題

  - 斷詞
  - 語法解析
  - 古籍議題
  - 多語言混合

## 優化

  - 剔除字(Stopwords)
  - 詞性標註
  - \-{zh-hans:规范文件;zh-hant:權威檔;}-(authority file)
  - 知識體系，本體論(ontology)
  - 超連結分析(page rank)技術

## 歷史及未來之趨勢

  - 自由語句搜尋

## 參考

  - \-{zh-hans:[信息抽取](../Page/信息抽取.md "wikilink");zh-hant:[資訊擷取](../Page/資訊擷取.md "wikilink");}-
  - [搜尋引擎](../Page/搜尋引擎.md "wikilink")

[Category:搜索](../Category/搜索.md "wikilink")
[Category:文本编辑器功能](../Category/文本编辑器功能.md "wikilink")
[Category:中文信息检索](../Category/中文信息检索.md "wikilink")