**全球華人大遊行**，又稱**五二八大遊行**，是一場於1989年5月28日（星期日）舉行的全球性[遊行活動](../Page/遊行.md "wikilink")，接續前一天周六在[香港舉行的](../Page/香港.md "wikilink")[民主歌聲獻中華](../Page/民主歌聲獻中華.md "wikilink")，以聲援在[北京](../Page/北京.md "wikilink")[天安門廣場](../Page/天安門廣場.md "wikilink")[絕食](../Page/絕食.md "wikilink")[抗議的學生](../Page/八九民運.md "wikilink")。北京的學生為紀念200年前的[法國大革命](../Page/法國大革命.md "wikilink")，呼籲全球[華人在當日串連遊行](../Page/華人.md "wikilink")。

在[香港](../Page/香港.md "wikilink")，當日共有150萬人上街遊行\[1\]\[2\]，不論遊行人數還是全民參與率都是空前的。同日，在北京有大約10萬人遊行，[上海和](../Page/上海.md "wikilink")[澳門有大約](../Page/澳門.md "wikilink")5萬人。此外，在[台北](../Page/台北.md "wikilink")、[高雄](../Page/高雄.md "wikilink")、[大阪](../Page/大阪.md "wikilink")、[三藩市](../Page/三藩市.md "wikilink")、[洛杉磯](../Page/洛杉磯.md "wikilink")、[墨爾本](../Page/墨爾本.md "wikilink")、[悉尼](../Page/悉尼.md "wikilink")、[多倫多](../Page/多倫多.md "wikilink")、[巴黎](../Page/巴黎.md "wikilink")、[柏林](../Page/柏林.md "wikilink")、[倫敦](../Page/倫敦.md "wikilink")、[羅馬等世界各地的大城市也都有大量華人在當地遊行](../Page/羅馬.md "wikilink")\[3\]。

## 背景

北京的[學潮發展到](../Page/學生運動.md "wikilink")1989年5月20日時，[中華人民共和國總理](../Page/中華人民共和國總理.md "wikilink")[李鵬簽署](../Page/李鵬.md "wikilink")[戒嚴令](../Page/戒嚴.md "wikilink")，北京開始戒嚴。北京市人民政府根據戒嚴令，由[陳希同市長簽發了實施戒嚴令的第一](../Page/陳希同.md "wikilink")、第二、第三號令。

北京戒嚴後，外地軍隊接到命令，開赴北京執行戒嚴令。北京市民和學生一起走上街頭，阻止軍隊進城。但此時在北京郊區已經有了些零星的流血衝突。

當時，很多人都認為北京的學潮是一場愛國民主運動，普遍不同意當局的把學潮定性為「動亂」，以及進行戒嚴。在5月20日晚，[颱風布倫達吹襲香港](../Page/颱風布倫達.md "wikilink")，雖然香港掛起[八號風球](../Page/香港熱帶氣旋警告信號.md "wikilink")\[4\]\[5\]，但當晚仍有4萬人冒著颱風威脅在[維多利亞公園集會並遊行到當時在](../Page/維多利亞公園.md "wikilink")[灣仔](../Page/灣仔.md "wikilink")[皇后大道東的](../Page/皇后大道東.md "wikilink")[新華社香港分社](../Page/新華社香港分社.md "wikilink")（今翻新成酒店）。在5月21日，香港更有100萬人遊行（百萬人大遊行）\[6\]\[7\]，[支聯會也在當日成立](../Page/香港市民支援愛國民主運動聯合會.md "wikilink")\[8\]。5月27日，香港演藝界在[跑馬地馬場舉行](../Page/跑馬地馬場.md "wikilink")[民主歌聲獻中華籌款演唱會](../Page/民主歌聲獻中華.md "wikilink")，籌得1,200萬港元，並由[李卓人把款項送到北京](../Page/李卓人.md "wikilink")。在[澳門](../Page/澳門.md "wikilink")，5月20日[澳門氣象台懸掛第二高的](../Page/澳門氣象台.md "wikilink")[九號風球](../Page/九號風球_\(澳門\).md "wikilink")，各界1萬人冒着狂風暴雨大遊行反對北京戒嚴。隨後的遊行人數於5月22日為5萬人、23日達到10萬人、28日有5萬人。

6月4日，民運被[中國人民解放軍以武力血腥鎮壓](../Page/中國人民解放軍.md "wikilink")，澳門高達20萬名市民上街遊行抗議血腥鎮壓\[9\]，創下當地史上最多人遊行集會的記錄，當日[香港亦有](../Page/香港.md "wikilink")100萬人上街抗議\[10\]\[11\]。

## 各地遊行情況

### 香港

在香港，遊行活動由支聯會統籌。遊行人士在當日下午2時由[遮打道行人專用區出發](../Page/遮打道.md "wikilink")，經[灣仔新華社香港分社再遊行至](../Page/灣仔.md "wikilink")[北角](../Page/北角.md "wikilink")[糖水道轉上](../Page/糖水道.md "wikilink")[東區走廊](../Page/東區走廊.md "wikilink")，最後抵達目的地維多利亞公園繼續集會。遊行持續達8小時，直到晚上10時才結束。遊行期間大致和平，並沒有發生不愉快事件。據估計，當日共有150萬人參加遊行\[12\]，為[香港歷史上最大規模的一次群眾運動](../Page/香港歷史.md "wikilink")。

### 澳門

在澳門，各團體動員5萬市民於繁華地段遊行支援學運\[13\]，並遊行至[新華社澳門分社](../Page/新華社澳門分社.md "wikilink")。

### 台灣

在[八九天安門民主運動期間](../Page/六四事件.md "wikilink")，[台灣社會各界也高度關注其發展](../Page/台灣.md "wikilink")，並有團體發起捐助金錢與物資等活動。在[台北](../Page/台北.md "wikilink")，大約1萬名大學生和民眾在[中正紀念堂集會及遊行](../Page/中正紀念堂.md "wikilink")，徹夜聲援以示支持。在高雄，亦有大約1萬人參加遊行活動。

## 參見

  - 1989年4月27日的[四二七遊行](../Page/四二七遊行.md "wikilink")（北京）
  - 1989年5月17日的[五一七大遊行](../Page/五一七大遊行.md "wikilink")（大陆）
  - 1989年5月20日的[五二零遊行](../Page/五二零遊行.md "wikilink")（[港澳](../Page/港澳.md "wikilink")）
  - 1989年5月21日的[五二一大遊行](../Page/五二一大遊行.md "wikilink")（香港）
  - 1989年5月22日的[五二二遊行](../Page/五二二遊行.md "wikilink")（澳門）
  - 1989年5月23日的[五二三大遊行](../Page/五二三大遊行.md "wikilink")（北京及澳門）
  - 1989年5月27日的[民主歌聲獻中華](../Page/民主歌聲獻中華.md "wikilink")
  - [六四事件](../Page/六四事件.md "wikilink")
  - 1989年6月4日的[六四大遊行](../Page/六四大遊行.md "wikilink")（全球）
  - 1990年起每年6月4日舉行的[六四燭光晚會](../Page/六四燭光晚會.md "wikilink")

## 參考文獻

{{-}}

[Category:八九民運](../Category/八九民運.md "wikilink")
[Category:抗议遊行](../Category/抗议遊行.md "wikilink")
[Category:香港遊行](../Category/香港遊行.md "wikilink")
[Category:澳門遊行](../Category/澳門遊行.md "wikilink")
[Category:澳門中外交流史](../Category/澳門中外交流史.md "wikilink")
[Category:1989年香港](../Category/1989年香港.md "wikilink")
[Category:台灣遊行](../Category/台灣遊行.md "wikilink")
[Category:1989年5月](../Category/1989年5月.md "wikilink")
[Category:人道援助](../Category/人道援助.md "wikilink")

1.
2.

3.

4.  [香港天文台警告及信號資料庫](http://www.hko.gov.hk/cgi-bin/hko/warndb_c1.pl?opt=91&tcname=BRENDA&submit=%B7j%B4M)

5.  [1989 Pacific typhoon
    season](../Page/:en:1989_Pacific_typhoon_season#Typhoon_Brenda_\(Bining\).md "wikilink"),
    Wikipedia.

6.

7.

8.  [「六四」是怎樣一回事?
    【字幕版】](http://www.youtube.com/watch?v=ZSia0NcWdVE)，[香港教育專業人員協會](../Page/香港教育專業人員協會.md "wikilink")

9.

10.

11.

12.

13.