[Gylc_gylc.gif](https://zh.wikipedia.org/wiki/File:Gylc_gylc.gif "fig:Gylc_gylc.gif")
**全球青年领袖会议**（，），一个由[国会青年领袖理事会](../Page/国会青年领袖理事会.md "wikilink")（Congressional
Youth Leadership
Council）策划组织的未来领袖培养项目。它将来自全球的16到18岁之间的青年人聚集到一起，来培养他们能够拥有在全球语境中的关键领导技能。

学生们将有机会与一些世界顶尖的商界领袖、政治官员、记者、外交官和学者来学习和探讨各自的想法。

这个项目将会在每年的夏季于如下地点举办，为期12天:

  - [美国](../Page/美国.md "wikilink")
    ([华盛顿和](../Page/华盛顿.md "wikilink")[纽约](../Page/纽约.md "wikilink"))
  - [欧洲](../Page/欧洲.md "wikilink")
    ([维也纳](../Page/维也纳.md "wikilink"),[布达佩斯和](../Page/布达佩斯.md "wikilink")[布拉格](../Page/布拉格.md "wikilink"))
  - [中国](../Page/中国.md "wikilink")
    ([北京](../Page/北京.md "wikilink"),[上海和](../Page/上海.md "wikilink")[杭州](../Page/杭州.md "wikilink"))

## 话题

  - 国际[关系](../Page/关系.md "wikilink")
  - [外交](../Page/外交.md "wikilink")
  - [法律](../Page/法律.md "wikilink")
  - [人权](../Page/人权.md "wikilink")
  - [和平与](../Page/和平.md "wikilink")[安全](../Page/安全.md "wikilink")
  - [经济](../Page/经济.md "wikilink")
  - [联合国的作用](../Page/联合国.md "wikilink")

## 加入

首先需要获得学校或者朋友的提名来成为全球青年领袖会议国际学生，并在截止日期前通过在线注册或邮寄申请表格来正式加入。学费可通过信用卡或电汇来支付。

## 参见

  - [国会青年领袖理事会](../Page/国会青年领袖理事会.md "wikilink")

## 外部链接

  - [官方网站](http://www.cylc.org/gylc/index.cfm)

<!-- end list -->

  - [不只是留学 - 李秉康, 翁燕文 - Google
    图书](https://books.google.com/books?id=uXyvDQAAQBAJ&pg=PT124)
  - [政策月刊 第 54-65 期 - Google
    图书](https://books.google.com/books?id=LxA4AQAAIAAJ&q=%22%E5%85%A8%E7%90%83%E9%9D%92%E5%B9%B4%E9%A2%86%E8%A2%96%E4%BC%9A%E8%AE%AE%22&dq=%22%E5%85%A8%E7%90%83%E9%9D%92%E5%B9%B4%E9%A2%86%E8%A2%96%E4%BC%9A%E8%AE%AE%22)

[Category:非营利组织](../Category/非营利组织.md "wikilink")
[Category:教育组织](../Category/教育组织.md "wikilink")
[Category:青年组织](../Category/青年组织.md "wikilink")