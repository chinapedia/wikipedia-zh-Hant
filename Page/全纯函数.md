[Conformal_map.svg](https://zh.wikipedia.org/wiki/File:Conformal_map.svg "fig:Conformal_map.svg")後的結果（下）\]\]
**全纯函数**（）是[复分析研究的中心对象](../Page/複分析.md "wikilink")；它们是定义在[复平面](../Page/複平面.md "wikilink")\(\mathbb{C}\)的[开子集上的](../Page/开集.md "wikilink")，在复平面**\(\mathbb{C}\)**中取值的，在每点上皆复可微的[函数](../Page/函数.md "wikilink")。这是比实可微强得多的条件，暗示著此函数[无穷可微并可以用](../Page/光滑函数.md "wikilink")[泰勒级数來描述](../Page/泰勒级数.md "wikilink")。

**[解析函数](../Page/解析函数.md "wikilink")**（）一词经常可以和“全纯函数”互相交换使用，虽然前者有几个其他含义。

全纯函数有时称为**正则函数**。在整个复平面上都全纯的函数称为[整函数](../Page/整函数.md "wikilink")（entire
function）。「在一点***\(a\)***全纯」不仅表示在***\(a\)***可微，而且表示在某个中心为***\(a\)***的复平面上的开邻域上可微。

**双全纯**（）表示一个有全纯[逆函数的全纯函数](../Page/逆函数.md "wikilink")。

## 定义

若\(U\)为\(\mathbb{C}\)的开子集，且\(f:U\rightarrow \mathbb{C}\)为一个函数。

  - 我们称\(f\)是在\(U\)中一点\(z_0\)是复可微的（complex
    differentiable）或全纯的，当且仅当该[极限存在](../Page/极限.md "wikilink")：

<center>

\(f'(z_0) = \lim_{z \rightarrow z_0} {f(z) - f(z_0) \over z - z_0 }\)

</center>

  - 若\(f\)在\(U\)上任取一点均全纯，则称\(f\)在\(U\)上**全纯**。
  - 特别地，若函数在整个复平面全纯，我们称这个函数为[**整函数**](../Page/整函数.md "wikilink")。

其中，极限取所有趋向\(z_0\)的复[数列](../Page/数列.md "wikilink")，并对所有这种序列差的商趋向同一个数\(f'(z_0)\)，另外，这个可微性的概念和[实可微性有几个相同性质](../Page/导数.md "wikilink")：它是[线性的](../Page/线性变换.md "wikilink")，并服从乘积，商和[链式法则](../Page/链式法则.md "wikilink")。

下面是一个等价的定义：一个复函数全纯[当且仅当它满足](../Page/当且仅当.md "wikilink")[柯西-黎曼方程](../Page/柯西-黎曼方程.md "wikilink")。

## 范例

### [有理函数](../Page/有理函數.md "wikilink")

  - 所有的复系数[多项式函数为](../Page/多项式函数.md "wikilink")[整函数](../Page/整函数.md "wikilink")

<!-- end list -->

  - 所有复系数的[有理函数](../Page/有理函數.md "wikilink")，在除去[极点以外的区域均为全纯](../Page/极点_\(复分析\).md "wikilink")。例如，函数\(f:z \mapsto \frac{1}{z}\)在\(\mathbb{C^*}\)上为全纯函数。

### 由[幂级数定义的函数](../Page/幂级数.md "wikilink")

若\(\Sigma_{n\geq 0}a_n z^n\)复系数幂级数，且[收敛半径不为零](../Page/收敛半径.md "wikilink")，我们记\(D\)为其收敛区域。

函数

<center>

\(\begin{align} f:D & \rightarrow \mathbb{C} \\ z & \mapsto \Sigma_{z\geq 0}a_n z^n \\ \end{align}\)

</center>

为全纯函数，且任取\(z \in D, f'(z)=\Sigma_{z\geq 1}{n a_nz^{n-1}}\).事实上，这个函数在\(D\)上无穷可导。

指数函数为整函数，同样地，[三角函数](../Page/三角函数.md "wikilink")（可通过指数函数使用[欧拉公式定义](../Page/欧拉公式.md "wikilink")）与[双曲函数同样为整函数](../Page/双曲函数.md "wikilink")。

### [复对数](../Page/複對數.md "wikilink")

若在一个连通集上的函数\(L:U\rightarrow \mathbb{C}\)满足条件：\(\forall z \in D, \exp(L(z))=z\)，则称其为一个[复对数函数](../Page/複對數.md "wikilink")。

另有一等价定义，即若全纯函数\(L\)在\(U\)上以\(z \mapsto 1/z\)为导数，且存在一点\(z_0\)，使得这一点\(\exp(L(z_0))=z_0\)，则称其为一个[复对数函数](../Page/複對數.md "wikilink")。

在\(\mathbb{C^*}\)的任意开子集\(U\)上，若有一个复对数\(L\)，那么任取整数\(k\)，函数\(z \mapsto L(z)+2k\pi i\)也为\(U\)上的复对数函数。

### 幂函数

在\(\mathbb{C^*}\)的任意开子集\(U\)上，若有一个复对数\(L\)，那么任取复数\(a\)，在\(U\)上\(a\)阶幂函数可以定义为\(\forall z \in U, z^a=\exp(aL(z))\)

特别地，任取整数\(n>0\)，有\(z^{1/n}=\exp((1/n)L(z))\)，满足\(\forall z \in U, (z^{1/n})^n=z\)，我们称此表达式为\(U\)上\(n\)阶幂的定义式。另外，记\(\sqrt[n]{z}:=z^{1/n}\)（若对于正实数，这种定义方式可能与其通常含义存在冲突）。

## 性质

因为复微分是线性的，并且服从积、商、链式法则，所以全纯函数的和、积及复合是全纯的，而两个全纯函数的商在所有分母非0的地方全纯。

每个全纯函数在每一点无穷可微。它和它自己的[泰勒级数相等](../Page/泰勒级数.md "wikilink")，而泰勒级数在每个完全位于定义域*\(U\)*内的开圆盘上收敛。泰勒级数也可能在一个更大的圆盘上收敛；例如，对数的泰勒级数在每个不包含0的圆盘上收敛，甚至在复实轴的附近也是如此。证明请参看[证明全纯函数解析](../Page/证明全纯函数解析.md "wikilink")。

若把**\(\mathbb{C}\)**和\(\mathbb{R^2}\)等同起来，则全纯函数和满足[柯西-黎曼方程的双实变量函数相同](../Page/柯西-黎曼方程.md "wikilink")，该方程组含有两个[偏微分方程](../Page/偏微分方程.md "wikilink")。

在非0导数的点的附近，全纯函数是[共形的](../Page/共形映射.md "wikilink")(或称保角的)。因为他们保持了小图形的角度和形状(但尺寸可能改变)。

[柯西积分公式表明每个全纯函数在圆盘内的值由它在盘边界上的取值所完全决定](../Page/柯西积分公式.md "wikilink")。

## 几个变量

[多复變函數的复解析函数定义为在一点全纯和解析](../Page/多複變函數.md "wikilink")，如果它局部可以(在一个[多盘](../Page/多盘.md "wikilink")，也即中心在该点的[圆盘的](../Page/圆盘.md "wikilink")[直积](../Page/直积.md "wikilink"))扩张为收敛的各个变量的幂级数。这个条件比[柯西-黎曼方程要强](../Page/柯西-黎曼方程.md "wikilink")；事实上它可以这样表述：

一个多复变量函数是全纯的当且仅当它满足柯西-黎曼方程并且局部[平方可积](../Page/平方可积.md "wikilink")。

## 扩展到泛函分析

全纯函数的概念可以扩展到[泛函分析中的无穷维空间](../Page/泛函分析.md "wikilink")。[Fréchet导数条目介绍了](../Page/Fréchet导数.md "wikilink")[巴拿赫空间上的全纯函数的概念](../Page/巴拿赫空间.md "wikilink")。

## 参看

  - [亚纯函数](../Page/亚纯函数.md "wikilink")（Meromorphic function）
  - [整函数](../Page/整函数.md "wikilink")
  - [反全纯函数](../Page/反全纯函数.md "wikilink")（Antiholomorphic function）

[Q](../Category/复分析.md "wikilink") [Q](../Category/函数.md "wikilink")