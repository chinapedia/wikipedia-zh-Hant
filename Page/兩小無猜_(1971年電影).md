**兩小無猜**（）是一齣1971年發行關於少年初戀的英國電影。

## 電影情節

兩名初中一年級生－丹尼爾及美樂蒂－向父母宣布要結婚，而且不是在將來而是現在要結婚，大人們試圖勸阻他們，但孩子決定按原定計劃進行。

## 製作

之前已在音樂版*Oliver\!*中亮相的[麥·-{里}-斯德](../Page/麥·里斯德.md "wikilink")（Daniel
Latimer）與[積·懷德](../Page/積·懷德.md "wikilink")（Ornshaw），再加上兒童模特兒 [Tracy
Hyde主演](../Page/Tracy_Hyde.md "wikilink")。其他演員包括[Kate
Williams](../Page/Kate_Williams.md "wikilink")（*Love thy
Neighbour*）飾Melody的母親、[Roy
Kinnear飾她的父親](../Page/Roy_Kinnear.md "wikilink")、 [Sheila
Steafel飾Daniel的母親](../Page/Sheila_Steafel.md "wikilink")、以及 [Ken
Jones](../Page/Ken_Jones.md "wikilink")（*Cuckoo Waltz* and
*Porridge*）和[James Cossins](../Page/James_Cossins.md "wikilink")。

這齣喜劇由[Bee Gees](../Page/Bee_Gees.md "wikilink")（"In the Morning"、"Melody
Fair"、"Give Your Best"、"To Love Somebody"、"First of May"）及Crosby,
Stills, Nash and Young（"Teach Your Children"）配樂。

有趣的是這電影是後來著名的導演[亞倫·帕克所寫的第一個電影劇本](../Page/亞倫·帕克.md "wikilink")。

## 演員陣容

  - [積·懷德飾](../Page/積·懷德.md "wikilink") 奧恩蕭（Ornshaw）
  - [麥·-{里}-斯德飾](../Page/麥·里斯德.md "wikilink") 丹尼爾·拉蒂默（Daniel Latimer）
  - [崔西·海德飾](../Page/崔西·海德.md "wikilink") 美樂蒂·珀金斯（Melody Perkins）
  - [Sheila Steafel飾](../Page/Sheila_Steafel.md "wikilink") 拉蒂默夫人（Mrs.
    Latimer）
  - [基斯·巴隆飾](../Page/基斯·巴隆.md "wikilink") 拉蒂默先生（Mr. Latimer）
  - [羅伊·金尼爾飾](../Page/羅伊·金尼爾.md "wikilink") 珀金斯先生（Mr. Perkins）
  - [希爾達·貝瑞飾](../Page/希爾達·貝瑞.md "wikilink") 珀金斯奶奶（Grandma Perkins）
  - [彼得·華爾頓飾](../Page/彼得·華爾頓_\(演員\).md "wikilink") 芬善（Fensham）
  - [凱·史金納飾](../Page/凱·史金納.md "wikilink") 佩姬（Peggy）
  - [威廉·范德普耶飾](../Page/威廉·范德普耶.md "wikilink") 歐利里（O'Leary）
  - [卡蜜兒·戴維斯飾](../Page/卡蜜兒·戴維斯.md "wikilink") 妙麗葉兒（Muriel）
  - [克萊格·馬略特飾](../Page/克萊格·馬略特.md "wikilink") 達德斯（Dadds）
  - [比利·法蘭克斯飾](../Page/比利·法蘭克斯.md "wikilink") 柏格斯（Burgess）
  - [提姆·威爾頓飾](../Page/提姆·威爾頓.md "wikilink") 費洛斯（Mr. Fellows）
  - [June Jago飾](../Page/June_Jago.md "wikilink") 費爾法克斯小姐（Miss Fairfax）
  - [尼爾·哈雷特](../Page/尼爾·哈雷特.md "wikilink")
  - [肯·瓊斯飾](../Page/肯·瓊斯_\(演員\).md "wikilink") 狄克斯先生（Mr. Dicks）
  - [萊絲莉·羅區飾](../Page/萊絲莉·羅區.md "wikilink") 蘿達（Rhoda）
  - [柯林·貝瑞飾](../Page/柯林·貝瑞.md "wikilink") 錢伯斯（Chambers）
  - [June C. Ellis飾](../Page/June_C._Ellis.md "wikilink") 丁金斯小姐（Miss
    Dimkins）
  - [詹姆斯·柯辛斯飾](../Page/詹姆斯·柯辛斯.md "wikilink") 校長
  - [凱特·威廉斯飾](../Page/凱特·威廉斯.md "wikilink") 珀金斯夫人（Mrs. Perkins）
  - [道恩·霍普飾](../Page/道恩·霍普_\(演員\).md "wikilink") 莫琳（Maureen）
  - [約翰·哥曼飾](../Page/約翰·哥曼.md "wikilink") 男孩的履行團長（Boys' Brigade Captain）
  - [羅賓·杭特飾](../Page/羅賓·杭特.md "wikilink") 喬治（George）
  - [史蒂芬·馬雷特](../Page/史蒂芬·馬雷特.md "wikilink")
  - [愛許莉·奈特飾](../Page/愛許莉·奈特.md "wikilink") 史黛西（Stacey）
  - [崔西·雷德飾](../Page/崔西·雷德_\(演員\).md "wikilink") 醫院中的男女（電視劇）
  - Leonard Brockwell

## 外部連結

  -
  - [Yahoo
    Group](https://web.archive.org/web/20070922002506/http://movies.groups.yahoo.com/group/tracyconstancemargarethyde/)


  - [*Melody* fan site](http://www.tracyhyde.net/)

  - [*Melody* Three Best Child
    Actors](https://archive.is/20121127213721/madaboutasia.blogspot.com/2008/03/melody-1971-mark-lester-jack-wild.html)


[Category:英國電影作品](../Category/英國電影作品.md "wikilink")
[Category:1971年電影](../Category/1971年電影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")