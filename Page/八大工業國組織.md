<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p>七国集团</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:G8_organization.svg" title="fig:G8_organization.svg">G8_organization.svg</a></p>
<hr />
<dl>
<dt></dt>
<dd><a href="../Page/加拿大总理.md" title="wikilink">總理</a> <a href="../Page/賈斯汀·杜魯道.md" title="wikilink">賈斯汀·杜魯道</a>
</dd>
<dt></dt>
<dd><a href="../Page/法国总统.md" title="wikilink">总统</a> <a href="../Page/埃马纽埃尔·马克龙.md" title="wikilink">埃马纽埃尔·马克龙</a>
</dd>
<dt></dt>
<dd><a href="../Page/德国总理.md" title="wikilink">总理</a> <a href="../Page/安格拉·默克爾.md" title="wikilink">安格拉·默克爾</a>
</dd>
<dt></dt>
<dd><a href="../Page/意大利总理.md" title="wikilink">总理</a> <a href="../Page/朱塞佩·孔特.md" title="wikilink">朱塞佩·孔特</a>
</dd>
<dt></dt>
<dd><a href="../Page/日本首相.md" title="wikilink">首相</a> <a href="../Page/安倍晋三.md" title="wikilink">安倍晋三</a>
</dd>
<dt></dt>
<dd><a href="../Page/英国首相.md" title="wikilink">首相</a> <a href="../Page/德蕾莎·梅伊.md" title="wikilink">德蕾莎·梅伊</a>
</dd>
<dt></dt>
<dd><a href="../Page/美国总统.md" title="wikilink">总统</a> <a href="../Page/唐納·川普.md" title="wikilink">唐納·川普</a>
</dd>
<dt> </dt>
<dd><a href="../Page/俄罗斯总统.md" title="wikilink">总统</a> <a href="../Page/弗拉基米尔·普京.md" title="wikilink">弗拉基米尔·普京</a>
</dd>
</dl>
<hr />
<dl>
<dt></dt>
<dd><a href="../Page/欧洲理事会主席.md" title="wikilink">欧洲理事会主席</a> <a href="../Page/唐納德·圖斯克.md" title="wikilink">-{zh-tw:唐納·圖斯克;zh-cn:唐纳德·图斯克;zh-hk:當奴·圖斯克;}-</a>
</dd>
<dd><a href="../Page/欧盟委员会主席.md" title="wikilink">欧盟委员会主席</a> <a href="../Page/让-克洛德·容克.md" title="wikilink">-{zh-tw:尚-克勞德·榮克;zh-cn:让-克洛德·容克;zh-hk:尚-格·容克;}-</a>
</dd>
</dl></td>
</tr>
</tbody>
</table>

[34th_G8_summit_member_20080707.jpg](https://zh.wikipedia.org/wiki/File:34th_G8_summit_member_20080707.jpg "fig:34th_G8_summit_member_20080707.jpg")
[G8_leaders_confer_together.jpg](https://zh.wikipedia.org/wiki/File:G8_leaders_confer_together.jpg "fig:G8_leaders_confer_together.jpg")
**八大工業國組織**（簡稱「G8」，2014年起因俄羅斯會籍被凍結而復稱**七大工業國組織**，亦稱「G7」），為世界上主要的已發展國家所組成的國際建制，創始於1973年，目前成員包括[美國](../Page/美國.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[英國](../Page/英國.md "wikilink")、[法國](../Page/法國.md "wikilink")、[德國](../Page/德國.md "wikilink")、[意大利及](../Page/意大利.md "wikilink")[日本](../Page/日本.md "wikilink")。

該組織源於法國舉行的[第1届六国集团峰会](../Page/第1届六国集团峰会.md "wikilink")。1976年，加拿大加入，改稱七大工業國組織。1997年，俄羅斯加入，於1998年起稱八大工業國組織。

2009年，在[G20美國舉行的](../Page/G20.md "wikilink")[匹茲堡峰會上](../Page/匹茲堡.md "wikilink")，[美國總統](../Page/美國總統.md "wikilink")[巴拉克·歐巴馬宣布](../Page/巴拉克·歐巴馬.md "wikilink")，G20已取代G8的主要功能，成為協調國際經濟合作的重要論壇。\[1\]\[2\]\[3\]\[4\]儘管如此，G8仍繼續舉行。

2014年，俄羅斯於[克-{}-里米亞危機中占領](../Page/2014年克里米亞危機.md "wikilink")[克-{}-里米亞半島及介入](../Page/克里米亞半島.md "wikilink")[頓巴斯戰爭](../Page/頓巴斯戰爭.md "wikilink")，被凍結會籍至今\[5\]\[6\]\[7\]，因而改稱為G7。

## 歷史

七大工業國組織始創於1973年的**五大工業國組織**（簡稱「**G5**」），始創國有5個，包括[美國](../Page/美國.md "wikilink")、[日本](../Page/日本.md "wikilink")、[西德](../Page/西德.md "wikilink")(現今的[德國](../Page/德國.md "wikilink"))、[法國](../Page/法國.md "wikilink")、[英國](../Page/英國.md "wikilink")。[意大利於](../Page/意大利.md "wikilink")1975年加入成為**六大工業國組織**（簡稱「**G6**」）。其後[加拿大於](../Page/加拿大.md "wikilink")1976年加入，成為**七大工業國組織**（簡稱「**G7**」）。

[俄羅斯於](../Page/俄羅斯.md "wikilink")1991年起參與G7峰會的部份會議，直至1997年，被接納成為第八個成員國，正式成為**八国集团**（简称「**G8**」）\[8\]。但因俄羅斯於[2014年克里米亞危機中佔領](../Page/2014年克里米亞危機.md "wikilink")[克里米亞半島及介入](../Page/克里米亞半島.md "wikilink")[頓巴斯戰爭](../Page/頓巴斯戰爭.md "wikilink")，被凍結會籍至今。因此重新恢复為**七大工業國組織**，「G8」亦名存實亡。

[欧盟及其前身](../Page/欧盟.md "wikilink")[歐洲共同體](../Page/歐洲共同體.md "wikilink")，自20世纪80年代开始参与，但不能成为主席国或举办峰会，一次特別的例外是2014年第40届峰会舉行地點原為[俄羅斯的](../Page/俄羅斯.md "wikilink")[索契](../Page/索契.md "wikilink")，但因俄羅斯被凍結會籍，最後改在[比利時的](../Page/比利時.md "wikilink")[布魯塞爾](../Page/布魯塞爾.md "wikilink")（歐盟總部）舉行。成為有史以來欧盟第一次成为主席国并主办峰会。

## 成員

  - （創始成員）

  - （創始成員）

  - （創始成員）

  - （創始成員）

  - （創始成員）

  - （1975年加入至今）

  - （1976年加入至今）

  - ~~~~（1997年加入，2014年会籍被冻结）

  - （非正式成員）

## 被凍結會籍的國家

### （會籍遭凍結）

俄罗斯是当今发展中国家裡工业最发达的国家，也是G8裡唯一的發展中国家，其重工业享誉世界。俄主要工业部门有钢铁、石油、冶金、化工、机械、造船、纺织、木材加工、汽车、食品、核能、宇航等。其中核工业和航空、軍火工业居世界第二位。进入21世纪后，俄罗斯的高新技术产业蓬勃发展，在发展中国家名列前茅。此外，與另外七國相比，俄羅斯屬於[威權主義政體](../Page/威權主義.md "wikilink")，與其他七國均爲民主國家不同。

2014年2月，俄羅斯併吞[烏克蘭](../Page/烏克蘭.md "wikilink")[克里米亞](../Page/克里米亞.md "wikilink")。同年3月2日，G8其餘七國及歐盟同意凍結俄羅斯會籍\[9\]。後來，意大利外交部長[费代丽卡·莫盖里尼及其他意大利官員](../Page/费代丽卡·莫盖里尼.md "wikilink")，還有董事會成員認為應恢复俄羅斯在組織中的會籍。2015年4月，德國外交部長[弗兰克-瓦尔特·施泰因迈尔表示](../Page/弗兰克-瓦尔特·施泰因迈尔.md "wikilink")，如果《[明斯克協議](../Page/明斯克協議.md "wikilink")》得到實施，歡迎俄羅斯回歸。2016年，他補充道“主要國際矛盾如果沒有俄羅斯不能夠解決”，而在2017年，七國集團國家正考慮俄羅斯回歸組織。同年，日本首相[安倍晉三呼籲俄羅斯回歸集團](../Page/安倍晉三.md "wikilink")，表示俄羅斯的參與“對中東多重危機的解決至關重要”。2017年1月，意大利外交部長[安傑利諾·阿爾法諾表示意大利希望](../Page/安傑利諾·阿爾法諾.md "wikilink")“和俄羅斯回復八國集團的模式，結束冷戰氛圍”。2017年1月13日，俄羅斯宣佈永久退出八國集團。儘管如此，[德國自由民主黨黨魁兼](../Page/德國自由民主黨.md "wikilink")[聯邦議院成員](../Page/德國聯邦議院.md "wikilink")[克里斯蒂安·林德納表示應該歡迎普京加入七國集團談判桌](../Page/克里斯蒂安·林德納.md "wikilink")，“和他會談，而不是談論他”，“不能讓所有事情都依賴克里米亞的局勢”。2018年4月，德國政治家、聯邦議院議員[莎拉·瓦根克内希特和](../Page/莎拉·瓦根克内希特.md "wikilink")表示應歡迎俄羅斯回歸組織，出席2018年在加拿大舉行的[峰會](../Page/第44屆七大工業國組織會議.md "wikilink")：“俄羅斯最快應該在6月再次出席峰會”，因為“只有俄羅斯才有可能為歐洲和中東帶來和平”。美國總統[唐納·川普也認為俄羅斯應該回歸八國集團](../Page/唐納·川普.md "wikilink")，他的呼籲受到意大利總理[朱塞佩·孔特支持](../Page/朱塞佩·孔特.md "wikilink")\[10\]\[11\]。後來，七國集團的四名歐洲成員國、加拿大和日本對此並不認同。多個成員國迅速拒絕了川普的建議后，俄羅斯外交部長拉夫羅夫表示俄羅斯聯邦對回歸不感興趣，[二十國集團才是重點](../Page/二十國集團.md "wikilink")\[12\]。2018年加拿大峰會的最終聲明中，七國集團宣佈，因明斯克協定未能完全實施，將恢複制裁，對俄羅斯採取進一步限制\[13\]\[14\]。

目前俄羅斯已視G20作爲主要的國際外交平台，因此現時並無重新加入G8的意願。

## 意義及內容

高峰會的主要目的，是促進每年該七（八）大世界經濟先進國的領袖與[歐洲聯盟官員在](../Page/歐洲聯盟.md "wikilink")[國際貨幣基金和](../Page/國際貨幣基金.md "wikilink")[世界銀行年會前舉行會談](../Page/世界銀行.md "wikilink")。

自1998年後，峰會成為該七（八）國[國家元首或](../Page/國家元首.md "wikilink")[政府首脑的年度高峰會議](../Page/政府首脑.md "wikilink")，由七（八）國輪流擔任主辦國（轮值国），主辦國的領袖成為該年會議的非正式主席（轮值主席）。與會國领袖會在[政治](../Page/政治.md "wikilink")、[經濟](../Page/經濟.md "wikilink")、[軍事等各方面交流意見](../Page/軍事.md "wikilink")。

2009年後，該峰會在經濟事務上的主要功能已被G20峰會取代。\[15\]\[16\]\[17\]\[18\]

## 首脑峰會

每年首腦峰會的主辦國由成員國輪流接任，下表列有過去峰會的舉辦地。

**G6峰會**：1975年；**G7峰會**：1976年至1997年，2014年至今；**G8峰會**：1998年至2013年

<table>
<thead>
<tr class="header">
<th><p>屆次</p></th>
<th><p>舉行日期</p></th>
<th><p>主辦國</p></th>
<th><p>主办国首脑</p></th>
<th><p>主辦地</p></th>
<th><p>官方網頁及备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/第1届六国集团峰会.md" title="wikilink">第1屆</a></p></td>
<td><p>nowrap|1975年11月15日至17日</p></td>
<td></td>
<td><p><a href="../Page/瓦莱里·吉斯卡尔·德斯坦.md" title="wikilink">瓦莱里·吉斯卡尔·德斯坦</a></p></td>
<td><p><a href="../Page/朗布依埃.md" title="wikilink">朗布依埃</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第2届七国集团峰会.md" title="wikilink">第2屆</a></p></td>
<td><p>1976年11月27日至28日</p></td>
<td></td>
<td><p><a href="../Page/杰拉尔德·福特.md" title="wikilink">杰拉尔德·福特</a></p></td>
<td><p><a href="../Page/波多黎各.md" title="wikilink">波多黎各</a><a href="../Page/圣胡安_(波多黎各).md" title="wikilink">圣胡安</a></p></td>
<td><p>註：首次G7峰會</p></td>
</tr>
<tr class="odd">
<td><p>第3屆</p></td>
<td><p>1977年5月7日至8日</p></td>
<td></td>
<td><p><a href="../Page/詹姆斯·卡拉汉.md" title="wikilink">詹姆斯·卡拉汉</a></p></td>
<td><p><a href="../Page/倫敦.md" title="wikilink">倫敦</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第4屆</p></td>
<td><p>1978年7月16日至17日</p></td>
<td></td>
<td><p><a href="../Page/赫尔穆特·施密特.md" title="wikilink">赫尔穆特·施密特</a></p></td>
<td><p><a href="../Page/波恩.md" title="wikilink">波恩</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第5屆</p></td>
<td><p>1979年6月28日至29日</p></td>
<td></td>
<td><p><a href="../Page/大平正芳.md" title="wikilink">大平正芳</a></p></td>
<td><p><a href="../Page/東京.md" title="wikilink">東京</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第6屆</p></td>
<td><p>1980年6月22日至23日</p></td>
<td></td>
<td><p><a href="../Page/弗朗切斯科·科西加.md" title="wikilink">弗朗切斯科·科西加</a></p></td>
<td><p><a href="../Page/威尼斯.md" title="wikilink">威尼斯</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第7屆</p></td>
<td><p>1981年7月20日至21日</p></td>
<td></td>
<td><p><a href="../Page/皮埃尔·特鲁多.md" title="wikilink">皮埃尔·特鲁多</a></p></td>
<td><p><a href="../Page/魁北克省.md" title="wikilink">魁北克省</a><a href="../Page/蒙特贝洛_(魁北克省).md" title="wikilink">-{zh-hans:蒙特贝洛; zh-hant:蒙特堡魯;}-</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第8屆</p></td>
<td><p>1982年6月4日至6日</p></td>
<td></td>
<td><p><a href="../Page/弗朗索瓦·密特朗.md" title="wikilink">弗朗索瓦·密特朗</a></p></td>
<td><p><a href="../Page/凡爾賽.md" title="wikilink">凡爾賽</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第9屆</p></td>
<td><p>1983年5月28日至30日</p></td>
<td></td>
<td><p><a href="../Page/罗纳德·里根.md" title="wikilink">罗纳德·里根</a></p></td>
<td><p><a href="../Page/弗吉尼亞州.md" title="wikilink">弗吉尼亞州</a><a href="../Page/威廉斯堡_(弗吉尼亚州).md" title="wikilink">威廉斯堡</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>nowrap|第10屆</p></td>
<td><p>1984年6月7日至9日</p></td>
<td></td>
<td><p><a href="../Page/玛格丽特·撒切尔.md" title="wikilink">玛格丽特·撒切尔</a></p></td>
<td><p><a href="../Page/倫敦.md" title="wikilink">倫敦</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第11屆</p></td>
<td><p>1985年5月2日至4日</p></td>
<td></td>
<td><p><a href="../Page/赫尔穆特·科尔.md" title="wikilink">赫尔穆特·科尔</a></p></td>
<td><p><a href="../Page/波恩.md" title="wikilink">波恩</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第12屆</p></td>
<td><p>1986年5月4日至6日</p></td>
<td></td>
<td><p><a href="../Page/中曾根康弘.md" title="wikilink">中曾根康弘</a></p></td>
<td><p><a href="../Page/東京.md" title="wikilink">東京</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第13屆</p></td>
<td><p>1987年6月8日至10日</p></td>
<td></td>
<td><p><a href="../Page/阿明托雷·范范尼.md" title="wikilink">阿明托雷·范范尼</a></p></td>
<td><p><a href="../Page/威尼斯.md" title="wikilink">威尼斯</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第14屆</p></td>
<td><p>1988年6月19日至21日</p></td>
<td></td>
<td><p><a href="../Page/布赖恩·马尔罗尼.md" title="wikilink">布赖恩·马尔罗尼</a></p></td>
<td><p><a href="../Page/安大略省.md" title="wikilink">安大略省</a><a href="../Page/多倫多.md" title="wikilink">多倫多</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第15屆</p></td>
<td><p>1989年7月14日至16日</p></td>
<td></td>
<td><p><a href="../Page/弗朗索瓦·密特朗.md" title="wikilink">弗朗索瓦·密特朗</a></p></td>
<td><p><a href="../Page/巴黎.md" title="wikilink">巴黎</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第16屆</p></td>
<td><p>1990年7月9日至11日</p></td>
<td></td>
<td><p><a href="../Page/乔治·赫伯特·沃克·布什.md" title="wikilink">乔治·H·W·布什</a></p></td>
<td><p><a href="../Page/得克萨斯州.md" title="wikilink">得克萨斯州</a><a href="../Page/休斯敦.md" title="wikilink">休斯敦</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第17屆</p></td>
<td><p>1991年7月15日至17日</p></td>
<td></td>
<td><p><a href="../Page/约翰·梅杰.md" title="wikilink">约翰·梅杰</a></p></td>
<td><p><a href="../Page/倫敦.md" title="wikilink">倫敦</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第18屆</p></td>
<td><p>1992年7月6日至8日</p></td>
<td></td>
<td><p><a href="../Page/赫尔穆特·科尔.md" title="wikilink">赫尔穆特·科尔</a></p></td>
<td><p><a href="../Page/慕尼黑.md" title="wikilink">慕尼黑</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第19屆</p></td>
<td><p>1993年7月7日至9日</p></td>
<td></td>
<td><p><a href="../Page/宫泽喜一.md" title="wikilink">宫泽喜一</a></p></td>
<td><p><a href="../Page/東京.md" title="wikilink">東京</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第20屆</p></td>
<td><p>1994年7月8日至10日</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/那不勒斯.md" title="wikilink">-{zh-hans:那不勒斯; zh-hant:拿玻里;}-</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第21屆</p></td>
<td><p>1995年6月15日至17日</p></td>
<td></td>
<td><p><a href="../Page/让·克雷蒂安.md" title="wikilink">让·克雷蒂安</a></p></td>
<td><p><a href="../Page/新斯科舍省.md" title="wikilink">新斯科舍省</a><a href="../Page/哈利法克斯_(新斯科舍省).md" title="wikilink">哈利法克斯</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>非正式</p></td>
<td><p>1996年4月19日至20日</p></td>
<td></td>
<td><p><a href="../Page/鲍里斯·叶利钦.md" title="wikilink">鲍里斯·叶利钦</a></p></td>
<td><p><a href="../Page/莫斯科.md" title="wikilink">莫斯科</a></p></td>
<td><p>註：核子安全特別高峰會議</p></td>
</tr>
<tr class="odd">
<td><p>第22屆</p></td>
<td><p>1996年6月27日至29日</p></td>
<td></td>
<td><p><a href="../Page/雅克·希拉克.md" title="wikilink">雅克·希拉克</a></p></td>
<td><p><a href="../Page/里昂.md" title="wikilink">里昂</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第23屆</p></td>
<td><p>1997年6月20日至22日</p></td>
<td></td>
<td><p><a href="../Page/比尔·克林顿.md" title="wikilink">比尔·克林顿</a></p></td>
<td><p><a href="../Page/科羅拉多州.md" title="wikilink">科羅拉多州</a><a href="../Page/丹佛_(科罗拉多州).md" title="wikilink">丹佛</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第24屆</p></td>
<td><p>1998年5月15日至17日</p></td>
<td></td>
<td><p><a href="../Page/托尼·布莱尔.md" title="wikilink">托尼·布莱尔</a></p></td>
<td><p><a href="../Page/伯明翰.md" title="wikilink">伯明翰</a></p></td>
<td><p>註：首次G8峰會</p></td>
</tr>
<tr class="even">
<td><p>第25屆</p></td>
<td><p>1999年6月18日至20日</p></td>
<td></td>
<td><p><a href="../Page/格哈德·施罗德.md" title="wikilink">格哈德·施罗德</a></p></td>
<td><p><a href="../Page/科隆.md" title="wikilink">科隆</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第26屆</p></td>
<td><p>2000年7月21日至23日</p></td>
<td></td>
<td><p><a href="../Page/森喜朗.md" title="wikilink">森喜朗</a></p></td>
<td><p><a href="../Page/沖繩縣.md" title="wikilink">沖繩縣</a><a href="../Page/名護市.md" title="wikilink">名護市</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第27屆</p></td>
<td><p>2001年7月20日至22日</p></td>
<td></td>
<td><p><a href="../Page/西尔维奥·贝卢斯科尼.md" title="wikilink">西尔维奥·贝卢斯科尼</a></p></td>
<td><p><a href="../Page/熱那亞.md" title="wikilink">熱那亞</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第28屆</p></td>
<td><p>2002年6月26日至27日</p></td>
<td></td>
<td><p><a href="../Page/让·克雷蒂安.md" title="wikilink">让·克雷蒂安</a></p></td>
<td><p><a href="../Page/艾伯塔省.md" title="wikilink">艾伯塔省</a><a href="../Page/卡纳纳斯基斯.md" title="wikilink">卡纳纳斯基斯</a></p></td>
<td><p><a href="https://web.archive.org/web/20061118135534/http://www.g8.gc.ca/"><a href="https://web.archive.org/web/20061118135534/http://www.g8.gc.ca/">https://web.archive.org/web/20061118135534/http://www.g8.gc.ca/</a></a></p></td>
</tr>
<tr class="even">
<td><p>第29屆</p></td>
<td><p>2003年6月2日至3日</p></td>
<td></td>
<td><p><a href="../Page/雅克·希拉克.md" title="wikilink">雅克·希拉克</a></p></td>
<td><p><a href="../Page/埃維昂萊班.md" title="wikilink">埃維昂萊班</a></p></td>
<td><p><a href="https://web.archive.org/web/20110407183144/http://www.g8.fr/"><a href="https://web.archive.org/web/20110407183144/http://www.g8.fr/">https://web.archive.org/web/20110407183144/http://www.g8.fr/</a></a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第30届八国集团首脑会议.md" title="wikilink">第30屆</a></p></td>
<td><p>2004年6月8日至10日</p></td>
<td></td>
<td><p><a href="../Page/乔治·W·布什.md" title="wikilink">乔治·W·布什</a></p></td>
<td><p><a href="../Page/佐治亚州.md" title="wikilink">佐治亚州</a></p></td>
<td><p><a href="https://web.archive.org/web/20050426082900/http://www.g8usa.gov/"><a href="https://web.archive.org/web/20050426082900/http://www.g8usa.gov/">https://web.archive.org/web/20050426082900/http://www.g8usa.gov/</a></a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第31届八国集团首脑会议.md" title="wikilink">第31屆</a></p></td>
<td><p>2005年7月6日至8日</p></td>
<td></td>
<td><p><a href="../Page/托尼·布莱尔.md" title="wikilink">托尼·布莱尔</a></p></td>
<td><p><a href="../Page/蘇格蘭.md" title="wikilink">蘇格蘭</a><a href="../Page/格伦伊格尔斯.md" title="wikilink">格伦伊格尔斯</a></p></td>
<td><p><a href="http://webarchive.loc.gov/all/20080913235039/http%3A//www.g8.gov.uk/servlet/Front?pagename%3DOpenMarket/Xcelerate/ShowPage%26c%3DPage%26cid%3D1078995902703"><a href="http://webarchive.loc.gov/all/20080913235039/http%3A//www.g8.gov.uk/servlet/Front?pagename%3DOpenMarket/Xcelerate/ShowPage%26c%3DPage%26cid%3D1078995902703">http://webarchive.loc.gov/all/20080913235039/http%3A//www.g8.gov.uk/servlet/Front?pagename%3DOpenMarket/Xcelerate/ShowPage%26c%3DPage%26cid%3D1078995902703</a></a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第32屆八國首腦高峰會議.md" title="wikilink">第32屆</a></p></td>
<td><p>2006年7月15至17日</p></td>
<td></td>
<td><p><a href="../Page/弗拉基米尔·普京.md" title="wikilink">弗拉基米尔·普京</a></p></td>
<td><p><a href="../Page/圣彼得堡.md" title="wikilink">圣彼得堡</a></p></td>
<td><p><a href="https://web.archive.org/web/20150421042201/http://en.g8russia.ru/"><a href="https://web.archive.org/web/20150421042201/http://en.g8russia.ru/">https://web.archive.org/web/20150421042201/http://en.g8russia.ru/</a></a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第33届八国集团首脑会议.md" title="wikilink">第33届</a></p></td>
<td><p>2007年6月6日至8日</p></td>
<td></td>
<td><p><a href="../Page/安格拉·默克尔.md" title="wikilink">安格拉·默克尔</a></p></td>
<td><p><a href="../Page/海利根达姆.md" title="wikilink">海利根达姆</a></p></td>
<td><p><a href="http://www.g-8.org"><a href="http://www.g-8.org">http://www.g-8.org</a></a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第34届八国集团首脑会议.md" title="wikilink">第34届</a></p></td>
<td><p>2008年7月7日至9日</p></td>
<td></td>
<td><p><a href="../Page/福田康夫.md" title="wikilink">福田康夫</a></p></td>
<td><p><a href="../Page/北海道.md" title="wikilink">北海道</a><a href="../Page/虻田郡.md" title="wikilink">虻田郡</a><a href="../Page/洞爷湖町.md" title="wikilink">洞爷湖町</a></p></td>
<td><p><a href="https://web.archive.org/web/20080502143616/http://www.g8summit.go.jp/"><a href="http://www.g8summit.go.jp">http://www.g8summit.go.jp</a></a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第35屆八国集团首脑会议.md" title="wikilink">第35届</a></p></td>
<td><p>2009年7月8日至10日</p></td>
<td></td>
<td><p><a href="../Page/西爾維奧·貝盧斯科尼.md" title="wikilink">西爾維奧·貝盧斯科尼</a></p></td>
<td><p><a href="../Page/阿布鲁佐大区.md" title="wikilink">阿布鲁佐大区</a><a href="../Page/拉奎拉.md" title="wikilink">拉奎拉</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第36屆八国集团首脑会议.md" title="wikilink">第36屆</a></p></td>
<td><p>2010年6月25日至26日</p></td>
<td></td>
<td><p><a href="../Page/史蒂芬·哈珀.md" title="wikilink">史蒂芬·哈珀</a></p></td>
<td><p><a href="../Page/安大略省.md" title="wikilink">安大略省</a><a href="../Page/亨茨維爾_(安大略省).md" title="wikilink">亨茨維爾</a></p></td>
<td><p><a href="https://web.archive.org/web/20061118135534/http://www.g8.gc.ca/"><a href="https://web.archive.org/web/20061118135534/http://www.g8.gc.ca/">https://web.archive.org/web/20061118135534/http://www.g8.gc.ca/</a></a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第37屆八国集团首脑会议.md" title="wikilink">第37屆</a></p></td>
<td><p>2011年5月26日至27日</p></td>
<td></td>
<td><p><a href="../Page/萨科齐.md" title="wikilink">萨科齐</a></p></td>
<td><p><a href="../Page/多维尔.md" title="wikilink">多维尔</a></p></td>
<td><p><a href="http://www.g20-g8.com/"><a href="http://www.g20-g8.com/">http://www.g20-g8.com/</a></a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第38屆八大工業國組織會議.md" title="wikilink">第38屆</a></p></td>
<td><p>2012年5月18日至19日</p></td>
<td></td>
<td><p><a href="../Page/贝拉克·奥巴马.md" title="wikilink">贝拉克·奥巴马</a></p></td>
<td><p><a href="../Page/戴维营.md" title="wikilink">戴维营</a></p></td>
<td><p><a href="https://web.archive.org/web/20120516213831/http://www.state.gov/e/eb/ecosum/2012g8/"><a href="https://web.archive.org/web/20120516213831/http://www.state.gov/e/eb/ecosum/2012g8/">https://web.archive.org/web/20120516213831/http://www.state.gov/e/eb/ecosum/2012g8/</a></a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第39屆八大工業國組織會議.md" title="wikilink">第39屆</a></p></td>
<td><p>2013年6月17日至18日</p></td>
<td></td>
<td><p><a href="../Page/戴维·卡梅伦.md" title="wikilink">戴维·卡梅伦</a></p></td>
<td><p><a href="../Page/北愛爾蘭.md" title="wikilink">北愛爾蘭</a><a href="../Page/弗馬納郡.md" title="wikilink">弗馬納郡</a><a href="../Page/恩尼斯基林.md" title="wikilink">恩尼斯基林</a></p></td>
<td><p><a href="https://www.gov.uk/government/topical-events/g8-2013"><a href="https://www.gov.uk/government/topical-events/g8-2013">https://www.gov.uk/government/topical-events/g8-2013</a></a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第40屆七大工業國組織會議.md" title="wikilink">第40屆</a></p></td>
<td><p>2014年6月4日至5日</p></td>
<td><p><br />
（）</p></td>
<td><p><a href="../Page/赫尔曼·范龙佩.md" title="wikilink">赫尔曼·范龙佩</a><br />
<a href="../Page/若泽·曼努埃尔·巴罗佐.md" title="wikilink">若泽·曼努埃尔·巴罗佐</a></p></td>
<td><p><a href="../Page/布鲁塞尔.md" title="wikilink">布鲁塞尔</a></p></td>
<td><p><a href="https://web.archive.org/web/20140609201619/http://www.european-council.europa.eu/g7brussels%3Cbr%3E%E5%8E%9F%E4%B8%BB%E8%BE%A6%E5%9C%B0%E9%BB%9E%E6%98%AF%E4%BF%84%E7%BE%85%E6%96%AF%E7%9A%84%E7%B4%A2%E5%A5%91%EF%BC%8C%E4%BD%86%E4%BF%84%E7%BD%97%E6%96%AF%E5%9B%A0%E9%9D%9E%E6%B3%95%E5%90%9E%E4%BD%B5%E5%85%8B%E9%87%8C%E7%B1%B3%E4%BA%9E%E8%80%8C%E8%A2%AB%E5%87%8D%E7%B5%90%E6%9C%83%E7%B1%8D%EF%BC%8C%3Cbr%3E%E5%9B%A0%E6%AD%A4%E6%94%B9%E5%9C%A8%E6%AD%90%E7%9B%9F%E7%9A%84%E6%89%80%E5%9C%A8%E5%9C%B0">https://web.archive.org/web/20140609201619/http://www.european-council.europa.eu/g7brussels%3Cbr%3E%E5%8E%9F%E4%B8%BB%E8%BE%A6%E5%9C%B0%E9%BB%9E%E6%98%AF%E4%BF%84%E7%BE%85%E6%96%AF%E7%9A%84%E7%B4%A2%E5%A5%91%EF%BC%8C%E4%BD%86%E4%BF%84%E7%BD%97%E6%96%AF%E5%9B%A0%E9%9D%9E%E6%B3%95%E5%90%9E%E4%BD%B5%E5%85%8B%E9%87%8C%E7%B1%B3%E4%BA%9E%E8%80%8C%E8%A2%AB%E5%87%8D%E7%B5%90%E6%9C%83%E7%B1%8D%EF%BC%8C%3Cbr%3E%E5%9B%A0%E6%AD%A4%E6%94%B9%E5%9C%A8%E6%AD%90%E7%9B%9F%E7%9A%84%E6%89%80%E5%9C%A8%E5%9C%B0</a><a href="../Page/比利時.md" title="wikilink">比利時</a><a href="../Page/布鲁塞尔.md" title="wikilink">布鲁塞尔主辦</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第41屆七大工業國組織會議.md" title="wikilink">第41屆</a></p></td>
<td><p>2015年6月7日至8日</p></td>
<td></td>
<td><p><a href="../Page/安格拉·默克尔.md" title="wikilink">安格拉·默克尔</a></p></td>
<td><p>| <a href="../Page/巴伐利亚州.md" title="wikilink">巴伐利亚州</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第42届七国集团会议.md" title="wikilink">第42屆</a></p></td>
<td><p>2016年5月26日至27日</p></td>
<td></td>
<td><p><a href="../Page/安倍晉三.md" title="wikilink">安倍晉三</a></p></td>
<td><p>| <a href="../Page/三重縣.md" title="wikilink">三重縣</a><a href="../Page/志摩市.md" title="wikilink">志摩市</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第43届七国集团会议.md" title="wikilink">第43屆</a></p></td>
<td><p>2017年5月26日至27日</p></td>
<td></td>
<td><p><a href="../Page/保羅·真蒂洛尼.md" title="wikilink">保羅·真蒂洛尼</a></p></td>
<td><p>| <a href="../Page/西西里.md" title="wikilink">西西里</a><a href="../Page/陶尔米纳.md" title="wikilink">陶尔米纳</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第44届七国集团会议.md" title="wikilink">第44屆</a></p></td>
<td><p>2018年6月8日至9日</p></td>
<td></td>
<td><p><a href="../Page/賈斯汀·杜魯多.md" title="wikilink">賈斯汀·杜魯多</a></p></td>
<td><p>| <a href="../Page/魁北克省.md" title="wikilink">魁北克省</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第45屆七大工業國組織會議.md" title="wikilink">第45屆</a></p></td>
<td><p>2019年</p></td>
<td></td>
<td><p><a href="../Page/埃马纽埃尔·马克龙.md" title="wikilink">埃马纽埃尔·马克龙</a></p></td>
<td><p>| <a href="../Page/新亞奎丹大區.md" title="wikilink">新亞奎丹大區</a><a href="../Page/比亞里茨.md" title="wikilink">比亞里茨</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 取消舉行的峰會

<table>
<thead>
<tr class="header">
<th><p>屆次</p></th>
<th><p>原本舉行日期</p></th>
<th><p>原本主辦國</p></th>
<th><p>原本主辦城市</p></th>
<th><p>原因 |-1</p></th>
<th><p>第40屆</p></th>
<th><p>2014年</p></th>
<th></th>
<th><p><a href="../Page/索契.md" title="wikilink">索契</a></p></th>
<th><p>俄羅斯於<a href="../Page/2014年克里米亞危機.md" title="wikilink">克里米亞危機裡的立場和行動被指有違多條</a><a href="../Page/國際法.md" title="wikilink">國際法和條約</a>，會籍被其他會員國凍結，並把峰會搬至<a href="../Page/歐盟.md" title="wikilink">歐盟首府</a><a href="../Page/布魯塞爾.md" title="wikilink">布魯塞爾舉行</a>。</p></th>
</tr>
</thead>
<tbody>
</tbody>
</table>

## 各成員现任君主或國家元首

<File:Queen_Elizabeth_II_in_March_2015.jpg>|
**[英國](../Page/英國.md "wikilink")**
**[加拿大](../Page/加拿大.md "wikilink")**
[女王伊麗莎白二世](../Page/伊麗莎白二世.md "wikilink")
<File:Emperor_Akihito_cropped_2_Barack_Obama_Emperor_Akihito_and_Empress_Michiko_20140424_1.jpg>|
**日本**
[明仁天皇](../Page/明仁.md "wikilink") <File:Frank-Walter> Steinmeier Feb 2014
(cropped).jpg| **[德國](../Page/德國.md "wikilink")**
[弗兰克-瓦尔特·施泰因迈尔](../Page/弗兰克-瓦尔特·施泰因迈尔.md "wikilink") <FILE:Presidente>
Sergio Mattarella.jpg|**[意大利](../Page/意大利.md "wikilink")**
[塞尔焦·马塔雷拉](../Page/塞尔焦·马塔雷拉.md "wikilink")

## 各成员现任領導人物

<File:Justin> Trudeau 2014-1.jpg| **[加拿大](../Page/加拿大.md "wikilink")**
[總理](../Page/加拿大总理.md "wikilink")
[賈斯汀·杜魯道](../Page/賈斯汀·杜魯道.md "wikilink")
<File:Emmanuel> Macron in Tallinn Digital Summit. Welcome dinner hosted
by HE Donald Tusk. Handshake (36669381364) (cropped 2).jpg|
**[法国](../Page/法国.md "wikilink")**
[总统](../Page/法国总统.md "wikilink")
[埃瑪紐耶爾·馬克宏](../Page/埃瑪紐耶爾·馬克宏.md "wikilink")
<File:Angela> Merkel Security Conference February 2015 (cropped).jpg|
**[德国](../Page/德国.md "wikilink")**
[总理](../Page/德国总理.md "wikilink")
[安格拉·默克爾](../Page/安格拉·默克爾.md "wikilink")
<File:Giuseppe_Conte_Official.jpg>|
**[意大利](../Page/意大利.md "wikilink")**
[总理](../Page/意大利总理.md "wikilink") [朱塞佩·孔特](../Page/朱塞佩·孔特.md "wikilink")
<File:Shinzo> Abe cropped.JPG| **[日本](../Page/日本.md "wikilink")**
[首相](../Page/日本内阁总理大臣.md "wikilink") [安倍晋三](../Page/安倍晋三.md "wikilink")
<File:Theresa> May 2015.jpg| **[英国](../Page/英国.md "wikilink")**
[首相](../Page/英国首相.md "wikilink") [德蕾莎·梅伊](../Page/德蕾莎·梅伊.md "wikilink")
<File:Donald> Trump Pentagon 2017.jpg|
**[美国](../Page/美国.md "wikilink")**
[总统](../Page/美国总统.md "wikilink") [唐纳德·川普](../Page/唐纳德·川普.md "wikilink")
<File:Donald> Tusk 2013-12-19.jpg| **[欧盟](../Page/欧盟.md "wikilink")**
[欧洲理事会主席](../Page/欧洲理事会主席.md "wikilink")
[唐納德·圖斯克](../Page/唐納德·圖斯克.md "wikilink")
<File:Ioannes> Claudius Juncker die 7 Martis 2014.jpg|
**[欧盟](../Page/欧盟.md "wikilink")**
[欧盟委员会主席](../Page/欧盟委员会主席.md "wikilink")
[让-克洛德·容克](../Page/让-克洛德·容克.md "wikilink")

### 会籍冻结的成员

Vladimir_Vladimirovich_Putin_(2nd_Presidency).jpg |
**[俄罗斯](../Page/俄罗斯.md "wikilink")**
[总统](../Page/俄罗斯总统.md "wikilink")
[弗拉基米尔·普京](../Page/弗拉基米尔·普京.md "wikilink")

## 注释

## 参考文献

## 参见

  - [二十国集团](../Page/二十国集团.md "wikilink")
  - [上海合作組織](../Page/上海合作組織.md "wikilink")
  - [已開發國家](../Page/已開發國家.md "wikilink")
  - [地域大国](../Page/地域大国.md "wikilink")

{{-}}

[八國集團](../Category/八國集團.md "wikilink")
[Category:国际经济组织](../Category/国际经济组织.md "wikilink")
[Category:國際會議](../Category/國際會議.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.  [G8峰会达成反逃税协定及对叙问题共识](http://www.apdnews.com/news/28605.html)
    ，亚太日报，2013年6月19日

9.

10.

11.

12.

13.

14. <http://tass.com/world/1008994>

15.
16.
17.
18.