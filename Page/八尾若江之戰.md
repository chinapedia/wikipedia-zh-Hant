**八尾・若江之戰**是[大坂夏之陣其中一場戰爭](../Page/大坂夏之陣.md "wikilink")。德川軍和豐臣軍在八尾和若江的軍事衝突。八尾方面由[藤堂高虎與](../Page/藤堂高虎.md "wikilink")[長宗我部盛親交戰](../Page/長宗我部盛親.md "wikilink")。而若江由[井伊直孝對](../Page/井伊直孝.md "wikilink")[木村重成](../Page/木村重成.md "wikilink")。最後木村重成陣亡，而長宗我部盛親給予藤堂高虎不少傷害。此戰役藤堂隊有不少將領是前長宗我部盛親的家臣。

## 八尾之戰

長宗我部勢先鋒吉田重親、進攻藤堂勢中備藤堂高吉。初時，藤堂高虎各隊取得優勢，長宗我部隊先鋒[吉田重親陣亡](../Page/吉田重親.md "wikilink")，幾乎可以殺進長宗我部的本陣。但遭到長宗我部伏兵突襲，長宗我部軍的騎馬者都下馬，持槍埋伏在堤防邊，趁藤堂軍接近時ㄧ齊攻擊，造成左先鋒部隊陷入混亂，藤堂高刑、藤堂氏勝和桑名一孝戰死，然後長宗我部取得優勢，藤堂高吉來援，亦被擊退。在正午時候，長宗我部軍在長瀨川堤防上休整，接獲若江的木村重成戰死的消息後，為免部隊被孤立，長宗我部決定撤退。

## 若江之戰

至於若江方面，兩隊正面交鋒井伊隊先由左先鋒[川上良利及右先鋒](../Page/川上良利.md "wikilink")[庵原朝昌交戰](../Page/庵原朝昌.md "wikilink")，左先鋒的川上良利率先突擊本村本隊，不過失敗而川上陣亡。然後到庵原朝昌上前，在混戰下，[木村重成及部下](../Page/木村重成.md "wikilink")[山口弘定](../Page/山口弘定.md "wikilink")、[內藤長秋均陣亡](../Page/內藤長秋.md "wikilink")，而左先鋒[木村宗明隊見](../Page/木村宗明.md "wikilink")[榊原康勝](../Page/榊原康勝.md "wikilink")、[丹羽長重加入戰團](../Page/丹羽長重.md "wikilink")，指揮剩餘部隊撤退。

## 參考資料

  - （新人物往來社 別冊歷史讀本56）　ISBN 4404030568

  - ，[學習研究社](../Page/學習研究社.md "wikilink")　ISBN 4056022364

[Category:1615年](../Category/1615年.md "wikilink")
[Category:大坂之役](../Category/大坂之役.md "wikilink")
[Category:河內國](../Category/河內國.md "wikilink")
[Category:八尾市](../Category/八尾市.md "wikilink")
[Category:東大阪市](../Category/東大阪市.md "wikilink")
[Category:德川氏](../Category/德川氏.md "wikilink")
[Category:豐臣氏](../Category/豐臣氏.md "wikilink")
[Category:井伊氏](../Category/井伊氏.md "wikilink")
[Category:榊原氏](../Category/榊原氏.md "wikilink")
[Category:仙石氏](../Category/仙石氏.md "wikilink")
[Category:諏訪氏](../Category/諏訪氏.md "wikilink")
[Category:保科氏](../Category/保科氏.md "wikilink")
[Category:藤田氏](../Category/藤田氏.md "wikilink")
[Category:丹羽氏](../Category/丹羽氏.md "wikilink")
[Category:木村氏](../Category/木村氏.md "wikilink")
[Category:長宗我部氏](../Category/長宗我部氏.md "wikilink")
[Category:藤堂氏](../Category/藤堂氏.md "wikilink")
[Category:吉田氏](../Category/吉田氏.md "wikilink")