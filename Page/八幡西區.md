**八幡西區**（）是[福岡縣](../Page/福岡縣.md "wikilink")[北九州市](../Page/北九州市.md "wikilink")7個[行政区之一](../Page/行政区.md "wikilink")，為北九州市人口最多的一區，約佔全市的四分之一。

## 歷史

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：
      - [鞍手郡](../Page/鞍手郡.md "wikilink")：木屋瀨村。
      - [遠賀郡](../Page/遠賀郡.md "wikilink")：黑崎村、上津役村、洞南村、香月村。
  - 1897年4月23日：黑崎村改制為黑崎町。
  - 1898年9月2日：木屋瀨村改制為木屋瀨町。
  - 1905年7月1日：洞南村改名為折尾村。
  - 1918年12月3日：折尾村改制為折尾町。
  - 1926年11月2日：黑崎町被併入八幡市。
  - 1931年4月1日：香月村改制為香月町。
  - 1937年5月5日：上津役村被併入八幡市。
  - 1944年12月8日：折尾町被併入八幡市。
  - 1955年4月1日：木屋瀨町和香月町被併入八幡市。
  - 1963年2月10日：八幡市與[門司市](../Page/門司市.md "wikilink")、[小倉市](../Page/小倉市.md "wikilink")、[戶畑市](../Page/戶畑市.md "wikilink")、[若松市](../Page/若松市.md "wikilink")[合併為](../Page/市町村合併.md "wikilink")[北九州市](../Page/北九州市.md "wikilink")。
  - 1963年4月1日：北九州市成為[政令指定都市](../Page/政令指定都市.md "wikilink")，同時設置[八幡區](../Page/八幡區.md "wikilink")。
  - 1974年4月1日：行政區重劃，八幡區被分割為[八幡東區與](../Page/八幡東區.md "wikilink")**八幡西區**，同時[若松區的淺川地區被改劃入八幡西區](../Page/若松區.md "wikilink")。

## 交通

### 鐵路

  - [九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")
      - [鹿兒島本線](../Page/鹿兒島本線.md "wikilink")：[黑崎車站](../Page/黑崎車站.md "wikilink")
        - [陣原車站](../Page/陣原車站.md "wikilink") -
        [折尾車站](../Page/折尾車站.md "wikilink")
      - [筑豐本線](../Page/筑豐本線.md "wikilink")：[本城車站](../Page/本城車站.md "wikilink")
        - 折尾車站

<!-- end list -->

  - [筑豐電氣鐵道](../Page/筑豐電氣鐵道.md "wikilink")
      - [筑豐電氣鐵道線](../Page/筑豐電氣鐵道線.md "wikilink")：[黑崎車站前站車站](../Page/黑崎車站前站車站.md "wikilink")
        - [西黑崎車站](../Page/西黑崎車站.md "wikilink") -
        [熊西車站](../Page/熊西車站.md "wikilink") -
        [萩原車站](../Page/萩原車站_\(福岡縣\).md "wikilink") -
        [穴生車站](../Page/穴生車站.md "wikilink") -
        [森下車站](../Page/森下車站_\(福岡縣\).md "wikilink") -
        [今池車站](../Page/今池車站_\(福岡縣\).md "wikilink") -
        [永犬丸車站](../Page/永犬丸車站.md "wikilink") -
        [三森車站](../Page/三森車站.md "wikilink") -
        [西山車站](../Page/西山車站_\(福岡縣\).md "wikilink") -
        （[中間市](../Page/中間市.md "wikilink")） -
        [筑豐香月車站](../Page/筑豐香月車站.md "wikilink") -
        [楠橋車站](../Page/楠橋車站.md "wikilink") -
        [新木屋瀨車站](../Page/新木屋瀨車站.md "wikilink") -
        [木屋瀨車站](../Page/木屋瀨車站.md "wikilink")

|                                                                                                                       |                                                                                                                                |
| --------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------ |
| [Kurosaki_Station_P1.jpg](https://zh.wikipedia.org/wiki/File:Kurosaki_Station_P1.jpg "fig:Kurosaki_Station_P1.jpg") | [JRKyushu_Honjo_Station.jpg](https://zh.wikipedia.org/wiki/File:JRKyushu_Honjo_Station.jpg "fig:JRKyushu_Honjo_Station.jpg") |

#### 已停駛鐵路

  - [西日本鐵道](../Page/西日本鐵道.md "wikilink")
      - [北九州線](../Page/北九州線.md "wikilink")：已於2000年11月25日停駛

## 教育

### 大學、短期大學

  - [產業醫科大學](../Page/產業醫科大學.md "wikilink")
  - [九州共立大學](../Page/九州共立大學.md "wikilink")
  - [九州女子大學](../Page/九州女子大學.md "wikilink")、[九州女子短期大學](../Page/九州女子短期大學.md "wikilink")
  - [折尾愛真短期大學](../Page/折尾愛真短期大學.md "wikilink")

### 高等學校

  - [福岡縣立東筑高等學校](../Page/福岡縣立東筑高等學校.md "wikilink")
  - [福岡縣立北筑高等學校](../Page/福岡縣立北筑高等學校.md "wikilink")
  - [福岡縣立八幡南高等學校](../Page/福岡縣立八幡南高等學校.md "wikilink")
  - [福岡縣立八幡中央高等學校](../Page/福岡縣立八幡中央高等學校.md "wikilink")
  - [福岡縣立八幡工業高等學校](../Page/福岡縣立八幡工業高等學校.md "wikilink")
  - [福岡縣立折尾高等學校](../Page/福岡縣立折尾高等學校.md "wikilink")
  - [自由丘高等學校](../Page/自由丘高等學校.md "wikilink")
  - [星琳高等學校](../Page/星琳高等學校.md "wikilink")
  - [折尾愛真高等學校](../Page/折尾愛真高等學校.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>