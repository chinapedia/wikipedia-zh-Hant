《**八月照相館**》（，）是1998年於[韓國發行的一部以愛情為題材的電影](../Page/韓國.md "wikilink")，由[许秦豪執導](../Page/许秦豪.md "wikilink")，[韓石圭及](../Page/韓石圭.md "wikilink")[沈銀河主演](../Page/沈銀河.md "wikilink")。

## 劇情介紹

青年劉正源（韩石圭
饰）身患绝症，但依旧坦然地微笑着面对生活。他开了一家照相馆，并一人负责全部职务。在家里，还耐心的教父亲学遥控器的操作方法。他默默的在为为自己离开人世而“准备”着。正源暗恋青梅竹马的芝泳，但芝泳最终还是选择了别人。这时，女交警德琳（沈银河
饰）由于工作的关系，认识了正源。　　

在正源的眼里德琳是那么天真、那么的可爱，两人在交往过程中，相互感染，逐渐喜欢上了对方。但正源却无法拥有这份难得的幸福，面对死亡，在夜深人静的时侯，他一个人会悄然哭泣。正源终于因病情恶化，住进了医院。德琳下班后照常赶到照相馆，却发现没有开门。　

为了不给德琳带来更多的伤害，正源再也没有出现在德琳的面前。德琳最后一气之下砸碎了照相馆的橱窗。不久，正源便去世了。在雪花纷飞寒冬的一天，德琳再次出现在照相馆的门前……

## 演員陣容

  - [韓石圭](../Page/韓石圭.md "wikilink") 飾 正元
  - [沈銀河](../Page/沈銀河.md "wikilink") 飾 德琳
  - [申　久](../Page/申久.md "wikilink") 飾 正元的父親
  - [吳智慧](../Page/吳智慧.md "wikilink") 飾 正元的妹妹
  - [全美善](../Page/全美善.md "wikilink") 飾 芝泳
  - [李漢偉](../Page/李漢偉.md "wikilink") 飾 哲求

## 獎項

### 入圍

  - 第36屆[大鐘獎](../Page/大鐘獎.md "wikilink")（1999年）\[1\]

:\*「最佳電影獎」

:\*「最佳導演獎」：[許秦豪](../Page/許秦豪.md "wikilink")

:\*「最佳男主角獎」：[韓石圭](../Page/韓石圭.md "wikilink")

:\*「最佳男配角獎」：[申　久](../Page/申久.md "wikilink")

:\*「最佳攝影獎」：[劉永吉](../Page/劉永吉.md "wikilink")

:\*「最佳燈光獎」：[金東浩](../Page/金東浩.md "wikilink")

:\*「最佳音樂獎」：[趙成禹](../Page/趙成禹.md "wikilink")

:\*「最佳企劃獎」：[車勝宰](../Page/車勝宰.md "wikilink")

  - 第19屆[青龍電影獎](../Page/青龍電影獎.md "wikilink")（1998年）\[2\]

:\*「最佳導演獎」：[許秦豪](../Page/許秦豪.md "wikilink")

:\*「最佳男主角獎」：[韓石圭](../Page/韓石圭.md "wikilink")

:\*「最佳劇本獎」：[吳勝旭](../Page/吳勝旭.md "wikilink")

### 獲獎

  - 第36屆[大鐘獎](../Page/大鐘獎.md "wikilink")（1999年）\[3\]

:\*「審查委員特別獎」

:\*「最佳劇本獎」：[吳勝旭](../Page/吳勝旭.md "wikilink")

:\*「最佳新導演獎」：[許秦豪](../Page/許秦豪.md "wikilink")

  - 第19屆[青龍電影獎](../Page/青龍電影獎.md "wikilink")（1998年）

:\*「最佳電影獎」

:\*「最佳女主角獎」：[沈銀河](../Page/沈銀河.md "wikilink")

:\*「最佳攝影獎」：[劉永吉](../Page/劉永吉.md "wikilink")

:\*「最佳新導演獎」：[許秦豪](../Page/許秦豪.md "wikilink")

  - 第34屆[百想藝術大獎](../Page/百想藝術大獎.md "wikilink")（1998年）\[4\]

:\*「電影部門—作品獎」

:\*「電影部門—女子最優秀演技獎」：[沈銀河](../Page/沈銀河.md "wikilink")

  - 第18屆（1998年）

:\*「最佳電影獎」

:\*「最佳導演獎」：[許秦豪](../Page/許秦豪.md "wikilink")

:\*「最佳女主角獎」：[沈銀河](../Page/沈銀河.md "wikilink")

:\*「最佳攝影獎」：[劉永吉](../Page/劉永吉.md "wikilink")

  - 第21屆（1998年）

:\*「最佳新導演獎」：[許秦豪](../Page/許秦豪.md "wikilink")

:\*「製作功勞獎」：[車勝宰](../Page/車勝宰.md "wikilink")

## 參考資料

<div class="references-small">

<references/>

</div>

## 評價

《八月照相館》在韓國國內較受歡迎，有超過40萬名觀眾進入戲院觀看。《八月照相館》亦受到不少國際電影節的垂青，例如[康城電影節](../Page/康城電影節.md "wikilink")、[新加坡國際電影節及](../Page/新加坡國際電影節.md "wikilink")[釜山國際電影節等](../Page/釜山國際電影節.md "wikilink")。[许秦豪导演的電影](../Page/许秦豪.md "wikilink")《春逝》也大受好评。

## 外部連結

  -
  - [本片於韓國電影資料館上頁面](http://www.kmdb.or.kr/movie/md_basic.asp?nation=K&p_dataid=04938)

[C](../Category/1998年电影.md "wikilink")
[C](../Category/韓語電影.md "wikilink")
[C](../Category/1990年代浪漫劇情片.md "wikilink")
[C](../Category/韓國浪漫劇情片.md "wikilink")
[Category:许秦豪电影](../Category/许秦豪电影.md "wikilink")

1.  [第36屆大鐘獎入圍名單](http://www.daejongfilmaward.kr/html/film/info04_36.html)

2.  [第19屆青龍電影獎得獎及入圍名單](http://www.blueaward.co.kr/bbs/board.php?bo_table=awards_news&sca=19)
3.  [第36屆大鐘獎得獎名單](http://www.daejongfilmaward.kr/award05/525)
4.  [第34屆百想藝術大獎得獎名單](http://isplus.live.joins.com/award/bs/2013/history/h_history.asp?menuid=2&R=34)