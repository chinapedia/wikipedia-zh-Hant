[八水绕长安地图.jpg](https://zh.wikipedia.org/wiki/File:八水绕长安地图.jpg "fig:八水绕长安地图.jpg")

**八水绕长安**，指的是[西安城](../Page/西安.md "wikilink")（[长安](../Page/长安.md "wikilink")）四周的八条河流：[渭河](../Page/渭河.md "wikilink")、[泾河](../Page/泾河.md "wikilink")、[沣河](../Page/沣河.md "wikilink")、[涝河](../Page/涝河.md "wikilink")、[潏河](../Page/潏河.md "wikilink")、[滈河](../Page/滈河.md "wikilink")、[浐河](../Page/浐河.md "wikilink")、[灞河](../Page/灞河.md "wikilink")。[渭](../Page/渭河.md "wikilink")、[泾是其中两条大的河流](../Page/泾河.md "wikilink")。

[西汉文学家](../Page/西汉.md "wikilink")[司马相如在](../Page/司马相如.md "wikilink")《上林赋》中，描写[汉代上林苑的美](../Page/汉代.md "wikilink")，写道“荡荡乎八川分流，相背而异态”，以后就有了“八水绕长安”的描述。

“八水”均属[黄河水系](../Page/黄河.md "wikilink")。[渭河汇入](../Page/渭河.md "wikilink")[黄河](../Page/黄河.md "wikilink")，而其他“七水”原本各自直接汇入[渭河](../Page/渭河.md "wikilink")。然而由于时代变迁，[浐河成为了](../Page/浐河.md "wikilink")[灞河的支流](../Page/灞河.md "wikilink")；滈河成为潏河的支流，潏河后又与沣河的交汇。

## “八水”简介

  - **渭河**是[黄河的最大支流](../Page/黄河.md "wikilink")，发源于[甘肃省](../Page/甘肃省.md "wikilink")[渭源县](../Page/渭源县.md "wikilink")，于[陕西](../Page/陕西.md "wikilink")[潼关注入](../Page/潼关.md "wikilink")[黄河](../Page/黄河.md "wikilink")。全长818公里，流域总面积134766平方公里，年径流量102亿立方米。[渭河绕](../Page/渭河.md "wikilink")[西安之北](../Page/西安.md "wikilink")。
  - **泾河**是[渭河的最大支流](../Page/渭河.md "wikilink")，干流发源于[六盘山东麓](../Page/六盘山.md "wikilink")[宁夏回族自治区](../Page/宁夏回族自治区.md "wikilink")[泾源县](../Page/泾源县.md "wikilink")，于[高陵县蒋王村汇入](../Page/高陵县.md "wikilink")[渭河左岸](../Page/渭河.md "wikilink")。全长455公里，流域总面积45421平方公里，年径流量21.4亿立方米。[泾河绕](../Page/泾河.md "wikilink")[西安之北](../Page/西安.md "wikilink")。
  - **沣河**发源于[长安区沣峪](../Page/长安区.md "wikilink")，流至[咸阳市境内汇入](../Page/咸阳市.md "wikilink")[渭河](../Page/渭河.md "wikilink")，全长82公里，总流域面积1460平方公里。
    据载，[大禹曾经治理过沣河](../Page/大禹.md "wikilink")，[西周的丰](../Page/西周.md "wikilink")、镐二京就建在沣河东西两岸。[秦](../Page/秦.md "wikilink")[咸阳](../Page/咸阳.md "wikilink")、[汉](../Page/汉.md "wikilink")[长安也位于沣河](../Page/长安.md "wikilink")、[渭河交汇处](../Page/渭河.md "wikilink")，[汉](../Page/汉.md "wikilink")、[唐时的昆明池也是引沣河水形成的](../Page/唐.md "wikilink")。[沣河绕](../Page/沣河_\(渭河\).md "wikilink")[西安之西](../Page/西安.md "wikilink")。
  - **涝河**古称潦水，源头有两条，东涝河发源于静峪垴，西涝河发源于秦岭梁，两河交汇后北流，最后北经[咸阳流入](../Page/咸阳.md "wikilink")[渭河](../Page/渭河.md "wikilink")。涝河全长82公里，总流域面积663平方公里。
    涝河绕[西安之西](../Page/西安.md "wikilink")。

<!-- end list -->

  - **潏河**发源于[长安区](../Page/长安区.md "wikilink")[秦岭北坡的大峪](../Page/秦岭.md "wikilink")，是西安地区最负盛名的河流。潏河在牛头寺附近分为两支，向北为皂河，向西则与滈河合流汇入沣河。河长67.2公里，流域面积687平方公里。潏河绕[西安之南](../Page/西安.md "wikilink")。

<!-- end list -->

  - **滈河**发源于[长安区石砭峪](../Page/长安区.md "wikilink")，全长46公里，流域面积292平方公里。与潏河在香积寺汇合后向西，在[鄠邑区](../Page/鄠邑区.md "wikilink")[秦渡镇附近注入沣河](../Page/秦渡镇.md "wikilink")。滈河绕[西安之南](../Page/西安.md "wikilink")。

<!-- end list -->

  - **浐河**发源于[蓝田县汤峪](../Page/蓝田县.md "wikilink")，是灞浐水系的最大支流，流经西安东郊纺织城在广太庙附近注入灞河，全长70公里。[浐河绕](../Page/浐河.md "wikilink")[西安之东](../Page/西安.md "wikilink")。

<!-- end list -->

  - **灞河**发源于[蓝田县灞源乡](../Page/蓝田县.md "wikilink")，全长109公里，流域面积2563.7平方公里。据史载，[灞河原名](../Page/灞河.md "wikilink")“滋水”，[春秋时秦穆公为了炫耀其霸业](../Page/春秋.md "wikilink")，改名为[灞河](../Page/灞河.md "wikilink")。[唐在此地设驿站](../Page/唐.md "wikilink")，亲友出行多在这里折柳送行。因沿河岸遍植柳树，春天柳絮纷飞如雪，“[灞柳风雪](../Page/灞柳风雪.md "wikilink")”成为“[长安八景](../Page/长安八景.md "wikilink")”之一。
    [灞河绕](../Page/灞河.md "wikilink")[西安之东](../Page/西安.md "wikilink")。

<!-- end list -->

  - [缩略图](https://zh.wikipedia.org/wiki/File:在富裕路上向北看皂河.jpg "fig:缩略图")**<u>氵皂</u>河**（亦作**皂河**）不应属于“八水”，它原是潏河的古道，河原来沿今之河直接入渭，后于[长安区附近之瓜洲村经人工改造绕经神禾原](../Page/长安区.md "wikilink")。
    目前，<u>氵皂</u>河起自[长安区申店乡潏河北侧](../Page/长安区.md "wikilink")，流经长安区韦曲、杜城、申店进入西安市区，再经丈八沟、北石桥、三桥镇、六村堡至草滩入渭河。全长32公里，西安市区段长27.4公里。<u>氵皂</u>水本名为漕水。据《汉书》记载
    ：“武帝元兴六年（公元前139年）春 ，穿漕渠通渭。” “<u>氵皂</u>”由“漕”演变而来。因本是人工河流
    ，今水源除季节性洪水和潏河的渗水之外，上游主要为稻田的余水，北石桥以下与城市防洪渠汇流以后，排泄城市工业污水为主。最大流量为40立方米每秒。\[1\]

## 污染

随着西安市规模的扩大，“八水”面临着水量锐减、污染严重的问题，河流生态功能的逐步退化，城镇生活污染取代工业污染成为渭河的主要污染源。据统计，2013年渭河流域生活氨氮排放量为2.26万吨，是工业排放量的4.5倍。\[2\]

## 参考文献

[Category:陕西河流](../Category/陕西河流.md "wikilink")
[Category:西安地理](../Category/西安地理.md "wikilink")

1.
2.