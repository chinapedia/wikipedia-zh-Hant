**八珍湯**是[傳統](../Page/傳統.md "wikilink")[中醫流傳下來的](../Page/中醫.md "wikilink")[藥方](../Page/藥方.md "wikilink")，最早出現記載於《[正體類要](../Page/正體類要.md "wikilink")》。八珍湯是由[四君子湯和](../Page/四君子湯.md "wikilink")[四物湯組合而成](../Page/四物湯.md "wikilink")，即[人參](../Page/人參.md "wikilink")、[白朮](../Page/白朮.md "wikilink")、[茯苓](../Page/茯苓.md "wikilink")、[甘草](../Page/甘草.md "wikilink")、[當歸](../Page/當歸.md "wikilink")、[熟地黃](../Page/熟地黃.md "wikilink")、[芍藥及](../Page/芍藥.md "wikilink")[川芎](../Page/川芎.md "wikilink")\[1\]，能夠補氣益血，主治氣[血兩虛](../Page/血.md "wikilink")、面色蒼白或萎黃、頭昏眼花、[四肢倦怠](../Page/四肢.md "wikilink")、氣短懶言、[食慾減退](../Page/食慾下降.md "wikilink")、瘡瘍潰後久不收口、[心悸怔忡](../Page/心悸.md "wikilink")、[月經失調及崩漏不止等](../Page/月經失調.md "wikilink")。廣泛用於各種[慢性疾病](../Page/慢性疾病.md "wikilink")、病後虛弱、[貧血](../Page/貧血.md "wikilink")、[手術後切口長期不癒合者](../Page/手術.md "wikilink")；及功能性子宮出血及頑固性[潰瘍等](../Page/潰瘍.md "wikilink")，屬於氣血兩虛者。

若在八珍湯中加入[黃耆](../Page/黃耆.md "wikilink")、[肉桂即為](../Page/肉桂.md "wikilink")[十全大補湯](../Page/十全大補湯.md "wikilink")。而若抽掉川芎，加入[五味子](../Page/五味子.md "wikilink")、[遠志和](../Page/遠志.md "wikilink")[陳皮](../Page/陳皮.md "wikilink")，便成為[人參養榮湯](../Page/人參養榮湯.md "wikilink")。材料方面，一般來說只要使用合格的廠商生產的藥品便可以安全服用。但痛經女性使用此類補品時，應綜合現代生理醫學觀念考慮，例如因擔心荷爾蒙的作用，補腎藥物等則不建議混合在其中服用。\[2\]

## 參考資料

  - [八珍湯 Ba Zhen
    Tang](http://lib-nt2.hkbu.edu.hk/database/cmed/cmfid/details.asp?lang=cht&id=F00076)
    中藥方劑圖像數據庫 (香港浸會大學中醫藥學院)

<div class="references-small">

<references />

</div>

[Category:方劑](../Category/方劑.md "wikilink")

1.
2.  <https://www.peoplenews.tw/news/f4eaabef-e635-4329-a6bf-71272d4eaa87>