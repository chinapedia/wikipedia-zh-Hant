[HK_Central_Wellington_Street_75_Pat_Chun_Fine_Food_Shop_a.jpg](https://zh.wikipedia.org/wiki/File:HK_Central_Wellington_Street_75_Pat_Chun_Fine_Food_Shop_a.jpg "fig:HK_Central_Wellington_Street_75_Pat_Chun_Fine_Food_Shop_a.jpg")
[Pat_Chun_Building_and_Entrance.jpg](https://zh.wikipedia.org/wiki/File:Pat_Chun_Building_and_Entrance.jpg "fig:Pat_Chun_Building_and_Entrance.jpg")
**八珍甜醋**，又名**八珍國際有限公司**，前者是[品牌](../Page/品牌.md "wikilink")，後者是[老字號](../Page/老字號.md "wikilink")[廠商](../Page/廠商.md "wikilink")。源於1932年在[香港](../Page/香港.md "wikilink")[花園街開辦的一間](../Page/花園街.md "wikilink")[食品](../Page/食品.md "wikilink")[雜貨店](../Page/雜貨店.md "wikilink")，[始創人是](../Page/始創人.md "wikilink")[伍偉森](../Page/伍偉森.md "wikilink")。\[1\]現在香港有四間[專門店](../Page/專門店.md "wikilink")，[中環分店在](../Page/中環.md "wikilink")[威靈頓街](../Page/威靈頓街.md "wikilink")。

八珍甜醋的後人有[政治助理](../Page/政治助理.md "wikilink")[伍潔鏇](../Page/伍潔鏇.md "wikilink")，她的爸爸正是[伍錦康](../Page/伍錦康.md "wikilink")，是第二代董事總經理。

## 產品

  - [甜醋](../Page/甜醋.md "wikilink")
  - [醬油](../Page/醬油.md "wikilink")
  - [喼汁](../Page/喼汁.md "wikilink")
  - [蠔油](../Page/蠔油.md "wikilink")
  - [腐乳](../Page/腐乳.md "wikilink")
  - [花生醬](../Page/花生醬.md "wikilink")
  - [涼果](../Page/涼果.md "wikilink")
  - [芝麻糖](../Page/芝麻糖.md "wikilink")
  - [花生](../Page/花生.md "wikilink")
  - [果仁](../Page/果仁.md "wikilink")
  - [豬腳薑](../Page/豬腳薑.md "wikilink")

## 外部參考

## 外部連結

  - [八珍網頁](http://www.patchun.com/)

[Category:香港食品製造商](../Category/香港食品製造商.md "wikilink")
[Category:香港名牌](../Category/香港名牌.md "wikilink")
[Category:1932年成立的公司](../Category/1932年成立的公司.md "wikilink")

1.  [RTHK-香港故事（第19輯）04-百年商埠-2012-3-12](http://www.youtube.com/watch?v=kXUcO0jFPDQ)