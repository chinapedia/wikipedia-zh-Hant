**八达岭**是位于[北京市](../Page/北京市.md "wikilink")[延庆区内临近](../Page/延庆区.md "wikilink")[居庸关的一个](../Page/居庸关.md "wikilink")[山峰](../Page/山峰.md "wikilink")，最高点1015米。地处于北京西北。

## 历史

八达岭最著名的是它的[长城](../Page/中国长城.md "wikilink")。它是[中国开放最早的一段长城](../Page/中国.md "wikilink")，也是至今为止保护最好，最著名的一段[明代长城](../Page/明代.md "wikilink")。其可行部分全长3741米。它建于1504年，关城有东西二门，东门额题“居庸外镇”，刻于[嘉靖十八年](../Page/嘉靖.md "wikilink")（1539年）；西门额题“北门锁钥”，刻于[万历十年](../Page/万历.md "wikilink")（1582年）。

萬里長城八達嶺段建於1505年（[明朝](../Page/明朝.md "wikilink")[弘治十八年](../Page/弘治_\(明孝宗\).md "wikilink")），東窄西寬，有東西二門，南北兩面各開一豁口，接連關城城牆，臺上四周有磚砌垛口。城牆頂部地面鋪縵方磚，嵌縫密實。內側為宇牆，外側為垛牆，垛牆上方有垛口，下方有射洞。城牆高低寬窄不一，平均高7米多，有些地段高達14米。牆基平均寬6.5米，頂寬5米多，可容五馬並馳或十人並進。

八达岭和附近的长城被称为[燕京八景之一的](../Page/燕京八景.md "wikilink")**居庸叠翠**。

## 八达岭长城图片

[File:Beimensuoyao.JPG|北门锁钥](File:Beimensuoyao.JPG%7C北门锁钥)
[File:Badalingchangcheng.JPG|陡峭的城墙](File:Badalingchangcheng.JPG%7C陡峭的城墙)

## 参见

  - [八达岭火车站](../Page/八达岭站.md "wikilink")
  - [八达岭长城站](../Page/八达岭长城站.md "wikilink")（[京张高铁预计](../Page/京张高铁.md "wikilink")2019年底通车，北京高铁站到长城八达岭20分钟）

[Category:北京市旅游景点](../Category/北京市旅游景点.md "wikilink")
[B](../Category/长城.md "wikilink")
[B](../Category/中国国家重点风景名胜区.md "wikilink")
[Category:中国山峰](../Category/中国山峰.md "wikilink")