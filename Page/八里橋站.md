**八里橋站**位于[北京市](../Page/北京市.md "wikilink")[朝阳区](../Page/朝阳区_\(北京市\).md "wikilink")[管庄地区](../Page/管庄地区.md "wikilink")，是[北京地铁](../Page/北京地铁.md "wikilink")[八通线的車站](../Page/八通线.md "wikilink")，编号是BT07。

## 位置

这个站位于八里橋南街南面的[京通快速路八里桥上下行收费站之间](../Page/京通快速路.md "wikilink")。

## 车站结构

### 车站楼层

<table>
<tbody>
<tr class="odd">
<td><p><strong>地上二层</strong></p></td>
<td><center>
<p><small><a href="../Page/侧式站台.md" title="wikilink">侧式站台</a>，右边车门将会开启</small></p>
</center></td>
</tr>
<tr class="even">
<td><p>西</p></td>
<td><p>列车往方向 <small>（）</small></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>列车往<a href="../Page/土桥站_(中国).md" title="wikilink">土桥方向</a> <small>（）</small></p></td>
</tr>
<tr class="even">
<td><center>
<p><small><a href="../Page/侧式站台.md" title="wikilink">侧式站台</a>，右边车门将会开启</small></p>
</center></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>地上一层</strong></p></td>
<td><p>站厅</p></td>
</tr>
<tr class="even">
<td><p><strong>地下一层</strong></p></td>
<td><p>出入口</p></td>
</tr>
</tbody>
</table>

### 站台

[Inside_Baliqiao_Station_(2).jpg](https://zh.wikipedia.org/wiki/File:Inside_Baliqiao_Station_\(2\).jpg "fig:Inside_Baliqiao_Station_(2).jpg")
八里桥站设有2个侧式站台，位于京通快速路中央。

### 出口

这个地铁站一共有4个出口：西北，西南，東北，東南

## 参见

  - [八里桥](../Page/八里桥.md "wikilink")

[Category:北京市朝阳区地铁车站](../Category/北京市朝阳区地铁车站.md "wikilink")
[Category:2003年启用的铁路车站](../Category/2003年启用的铁路车站.md "wikilink")