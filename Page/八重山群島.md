八重山諸島|kana=やえやまれっとう{{\`}}やえやましょとう|romaji=Yaeyama marettō{{\`}}Yaeyama
shotō}}
**八重山群島**也稱**八重山諸島**（，），為[琉球群島西部的](../Page/琉球群島.md "wikilink")[島嶼群](../Page/島嶼.md "wikilink")，位於[釣魚台群島的南方](../Page/釣魚台群島.md "wikilink")，[宮古群島的西方](../Page/宮古群島.md "wikilink")。其中石垣島是人口最多的島嶼。八重山群島和[宮古群島合稱為](../Page/宮古群島.md "wikilink")[先島群島](../Page/先島群島.md "wikilink")。群島內面積超過100平方公里的島嶼包括西表島和石垣島兩島。

## 概要

八重山群島以[石垣島為中心](../Page/石垣島.md "wikilink")，有人島中以[波照間島為日本最南端](../Page/波照間島.md "wikilink")，[與那國島為日本最西端](../Page/與那國島.md "wikilink")。

「八重山」名稱的由來，有許多種說法。1719年印行的《[中山傳信錄](../Page/中山傳信錄.md "wikilink")》中記載，「八重山，一名北木山，土名彜師加紀，又名爺馬(やま)」。

## 行政劃分

目前八重山群島的行政劃分屬於日本[沖繩縣](../Page/沖繩縣.md "wikilink")，分別屬於[石垣市](../Page/石垣市.md "wikilink")、[八重山郡](../Page/八重山郡.md "wikilink")[竹富町及](../Page/竹富町.md "wikilink")[與那國町](../Page/與那國町.md "wikilink")，另設有[八重山支廳協助辦理縣廳在八重山區域的事務](../Page/八重山支廳.md "wikilink")。

## 島嶼

括號內為該島所屬行政區域。

### 有人島

  - [石垣島](../Page/石垣島.md "wikilink")（石垣市）
  - [西表島](../Page/西表島.md "wikilink")（竹富町）
  - [竹富島](../Page/竹富島.md "wikilink")（竹富町）
  - [小濱島](../Page/小濱島.md "wikilink")（竹富町）
  - [黑島](../Page/黑島_\(八重山群島\).md "wikilink")（竹富町）
  - [鳩間島](../Page/鳩間島.md "wikilink")（竹富町）
  - [新城島](../Page/新城島.md "wikilink")（竹富町）
  - [由布島](../Page/由布島.md "wikilink")（竹富町）
  - [波照間島](../Page/波照間島.md "wikilink")（竹富町）
  - [與那國島](../Page/與那國島.md "wikilink")（與那國町）

### 無人島

  - [大地離島](../Page/大地離島.md "wikilink")（[石垣市](../Page/石垣市.md "wikilink")）
  - [平離島](../Page/平離島.md "wikilink")（石垣市）
  - [川平灣小島](../Page/川平灣小島.md "wikilink")（石垣市）
  - [真謝離](../Page/真謝離.md "wikilink")（石垣市）
  - [嘉彌真島](../Page/嘉彌真島.md "wikilink")（[竹富町](../Page/竹富町.md "wikilink")）
  - [仲之神島](../Page/仲之神島.md "wikilink")（竹富町）
  - [內離島](../Page/內離島.md "wikilink")（竹富町）
  - [外離島](../Page/外離島.md "wikilink")（竹富町）
  - [烏離島](../Page/烏離島.md "wikilink")（竹富町）
  - [鳩離島](../Page/鳩離島.md "wikilink")（竹富町）
  - [赤離島](../Page/赤離島.md "wikilink")（竹富町）
  - [祖納地崎](../Page/祖納地崎.md "wikilink")（[與那國町](../Page/與那國町.md "wikilink")）

[Miyakoshichou_in_Okinawa_Map'.gif](https://zh.wikipedia.org/wiki/File:Miyakoshichou_in_Okinawa_Map'.gif "fig:Miyakoshichou_in_Okinawa_Map'.gif")

## 參考

  - [国立公文書館　デジタル・ギャラリー](https://web.archive.org/web/20100210152855/http://jpimg.digital.archives.go.jp/kouseisai/index.html)
  - [元禄国絵図　琉球国八重山島](https://web.archive.org/web/20071209233304/http://jpimg.digital.archives.go.jp/jpg_prg/jgmWeb?%25TmpFileDisp%25Env=jpeg2k_images%2Fezu%2Fkuniezu_genroku%2F142_ryukyu.env)
  - [天保国絵図　琉球国（八重山島）](http://jpimg.digital.archives.go.jp/jpg_prg/jgmWeb?%TmpFileDisp%env=jpeg2k_images/ezu/kuniezu_tenpo/145_ryukyu.env)
    -
    [江戸幕府の命により](../Page/江戸幕府.md "wikilink")[薩摩藩が](../Page/薩摩藩.md "wikilink")[元禄](../Page/元禄.md "wikilink")・[天保年間に作成した地図](../Page/天保.md "wikilink")。
  - [竹富町観光協会「ぱいぬ島ストーリー」](https://web.archive.org/web/20121017072648/http://painusima.com/)
  - [八重山諸島写真集](https://web.archive.org/web/20140226231159/http://www.panoramio.com/user/3474627/tags/Yaeyama%20Islands%28%E5%85%AB%E9%87%8D%E5%B1%B1%E5%88%97%E5%B3%B6%29)

[八重山群島](../Category/八重山群島.md "wikilink")
[南](../Category/琉球群岛.md "wikilink")