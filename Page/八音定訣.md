《**八音定訣**》為[清朝](../Page/清朝.md "wikilink")（1875年）葉開恩所編著的一部以[廈門音主](../Page/廈門話.md "wikilink")，混合著[漳泉腔的](../Page/漳泉腔.md "wikilink")[閩南語](../Page/閩南語.md "wikilink")[音韻學書籍](../Page/音韻學.md "wikilink")。

## 刊本

  - 八音定訣，廈門倍文齋活版，1875年。
  - 重抄本，1881年，廈大館。
  - 木刻本，甲午端月版，光緒二十年（1894年），手抄本福師大館藏。
  - 廈門會文，1896年（清光緒廿二年）。
  - 福州福靈堂刊，1904年。
  - 廈門倍文齋活版，1904年。
  - 廈門倍文齋活版鉛字排印，1909年（清宣統元年），廈大館。
  - 活版，1910年第3版，廈大館。
  - 會文堂石印本，1940年。

## 參見

  - [拍掌知音](../Page/拍掌知音.md "wikilink")
  - [渡江書十五音](../Page/渡江書十五音.md "wikilink")

## 外部連結

  - [中華閩南文化網](http://www.mnwh.org/main.asp)
  - [中央研究院漢籍電子文獻](http://www.sinica.edu.tw/~tdbproj/handy1/)
  - [中央研究院歷史語言研究所](http://www.ihp.sinica.edu.tw/)

[category:厦门文化](../Page/category:厦门文化.md "wikilink")
[category:厦门历史](../Page/category:厦门历史.md "wikilink")

[Category:閩南語音韻學書籍](../Category/閩南語音韻學書籍.md "wikilink")
[Category:清朝典籍](../Category/清朝典籍.md "wikilink")