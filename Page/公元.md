**西元**（，[縮寫為](../Page/縮寫.md "wikilink")）、**公历纪元**或**公元**是一個被當今[國際社會最廣泛地使用的](../Page/國際.md "wikilink")[紀年標準](../Page/紀年.md "wikilink")。其源自於[歐洲等西方](../Page/歐洲.md "wikilink")[基督教信仰为主國家](../Page/基督教.md "wikilink")，以當時認定的[耶穌出生為紀年的開始](../Page/耶穌.md "wikilink")，原稱[基督纪年](../Page/基督纪年.md "wikilink")。现代学者及科學家基于[世俗化和宗教中立原则](../Page/世俗化.md "wikilink")，加上為免[世俗及非宗教的學術內容範疇涉及](../Page/世俗.md "wikilink")[基督教](../Page/基督教.md "wikilink")（Christ指耶穌基督，Domini指上帝）的人士\[1\]，和避免涉及宗教色彩，而改称公元（C.E.）及公元前（Before
common era, B.C.E.）。\[2\]\[3\]\[4\]

## 歷史

公元紀年起源于[基督教的影響](../Page/基督教.md "wikilink")。公元525年，[基督教](../Page/基督教.md "wikilink")[神學家](../Page/神學家.md "wikilink")[狄奧尼修斯·伊希格斯建議將](../Page/狄奧尼修斯·伊希格斯.md "wikilink")[耶穌生年定為紀元元年](../Page/耶穌.md "wikilink")，以取代當時[羅馬教廷所採用的](../Page/羅馬教廷.md "wikilink")「[戴克里先曆](../Page/戴克里先.md "wikilink")」（以迫害[基督徒著稱的](../Page/基督徒.md "wikilink")[羅馬](../Page/羅馬.md "wikilink")[皇帝戴克里先登基之年](../Page/皇帝.md "wikilink")，即公元284年作為元年，俗稱「殉教紀元」，[Era
of
Martyrs](../Page/w:en:Era_of_Martyrs.md "wikilink")）。伊希格斯並推算[耶穌是生於](../Page/耶穌.md "wikilink")[羅馬建國後](../Page/古羅馬.md "wikilink")754年，但後來的歷史學家發現他的計算有誤，耶穌並非出生于公元1年；現在一般以耶穌誕生在约公元前7年至前4年左右的說法最可靠（[大希律卒于公元前](../Page/大希律.md "wikilink")4年）。

## 英語用法

在[英語裡](../Page/英語.md "wikilink")，傳統上「紀元前」是用「B.C.」來代表，「紀元後」是用「A.D.」代表；「B.C.」是「Before
Christ」（基督前）的[首字母縮寫](../Page/首字母縮寫.md "wikilink")，[基督徒常譯為](../Page/基督徒.md "wikilink")「主前」；而「A.D.」則是[拉丁文](../Page/拉丁文.md "wikilink")「Anno
Domini」（在主之年）的縮寫，[基督徒常譯為](../Page/基督徒.md "wikilink")「[主後](../Page/主後.md "wikilink")××××年」。

現代由於西曆紀元的通用和標準化，為避免非基督徒的反感\[5\]以及文化、意識形態上的爭議，也為了分清基督正確出生年分的訛誤（耶穌並非出生于公元1年），[英語中越來越常用](../Page/英語.md "wikilink")「B.C.E.」和「C.E.」來分別代表「紀元前」和「紀元後」；「C.E.」是「Common
Era」的縮寫，意為「公元」，而「B.C.E.」是「Before the Common
Era」的縮寫，意為「公元前」，但許多人基于各種因素仍然使用「B.C.」，「A.D.」则略去不寫。而「C.E.」也被一些人認為是「Christian
Era」的縮寫。

## 採用

[聯合國採用公元](../Page/聯合國.md "wikilink")（B.C.E.）而非B.C.或A.D.，本身信仰[基督新教的前](../Page/基督新教.md "wikilink")[聯合國秘書長](../Page/聯合國秘書長.md "wikilink")[科菲·安南也支持使用公元作為紀年](../Page/科菲·安南.md "wikilink")，他也指C.E.對基督教人士可以理解為「Christian
Era」。

[中國歷史上最通行的紀年方式是](../Page/中國歷史.md "wikilink")[干支紀年法](../Page/干支.md "wikilink")，即把10[天干和](../Page/天干.md "wikilink")12[地支分別組合起來](../Page/地支.md "wikilink")，每60年為一週期。中國歷史上曾有過的其他紀元方式有：天文紀年法、歷史紀年法、帝王年號紀年法、[黃帝紀年和](../Page/黃帝紀年.md "wikilink")[孔子紀年等等](../Page/孔子紀年.md "wikilink")。1911年10月10日[辛亥革命後](../Page/辛亥革命.md "wikilink")，[湖北軍政府使用黃帝紀年](../Page/湖北軍政府.md "wikilink")。次年（[1912年](../Page/1912年.md "wikilink")），[中華民國臨時政府決定採用国际通用曆法做為國曆](../Page/中華民國臨時政府_\(南京\).md "wikilink")，但紀元部分沿用中華民國傳統，称[民國紀年](../Page/民國紀年.md "wikilink")。

1949年9月27日，[中國人民政治協商會議第一屆全體會議決議](../Page/中國人民政治協商會議第一屆全體會議.md "wikilink")，10月1日後成立的[中華人民共和國放棄使用民國紀年](../Page/中華人民共和國.md "wikilink")，改採用世界通用的公元紀年制度\[6\]。因最初是从西方传入，故译为“西曆”、“西元”，最初作為中華民國官方通用翻譯，如今台灣地区仍沿用，而大陸在中華人民共和國成立後改稱“公元”，以昭明其是“國際共同”，避免“西方獨用”的歧義。

## 參見

  - [0年](../Page/0年.md "wikilink")
  - [闰年](../Page/闰年.md "wikilink")、[闰月](../Page/闰月.md "wikilink")
  - [农历](../Page/农历.md "wikilink")、[生肖](../Page/生肖.md "wikilink")
  - [天干](../Page/天干.md "wikilink")、[地支](../Page/地支.md "wikilink")、[干支](../Page/干支.md "wikilink")
  - [二十四节气](../Page/二十四节气.md "wikilink")、[皇帝年号](../Page/年号.md "wikilink")、[中国历史年表](../Page/中国历史年表.md "wikilink")
  - [公历](../Page/公历.md "wikilink")、[格里历](../Page/格里历.md "wikilink")、[纪元](../Page/纪元.md "wikilink")、[纪年](../Page/纪年.md "wikilink")、[公历纪年](../Page/公历纪年.md "wikilink")
  - [阳历](../Page/阳历.md "wikilink")、[阴历](../Page/阴历.md "wikilink")、[阴阳历](../Page/阴阳历.md "wikilink")、[历法](../Page/历法.md "wikilink")
  - [全新世紀年](../Page/全新世紀年.md "wikilink")

## 参考文献

## 外部链接

  -
[Category:紀元](../Category/紀元.md "wikilink")

1.

2.  Irvin, Dale T.; Sunquist, Scott (2001). History of the World
    Christian Movement. Continuum International Publishing Group. p. xi.
    ISBN 0-567-08866-9. Retrieved 2011-05-18. The influence of western
    culture and scholarship upon the rest of the world in turn led to
    this system of dating becoming the most widely used one across the
    globe today. Many scholars in historical and religious studies in
    the West in recent years have sought to lessen the explicitly
    Christian meaning of this system without abandoning the usefulness
    of a single, common, global form of dating. For this reason the
    terms common era and before the common era, abbreviated as CE and
    BCE, have grown in popularity as designations. The terms are meant,
    in deference to non-Christians, to soften the explicit theological
    claims made by the older Latin terminology, while at the same time
    providing continuity with earlier generations of mostly western
    Christian historical research.

3.

4.

5.
6.  《[關於中華人民共和國國都、紀年、國歌、國旗的決議](../Page/關於中華人民共和國國都、紀年、國歌、國旗的決議.md "wikilink")》