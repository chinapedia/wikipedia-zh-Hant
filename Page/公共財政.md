**公共财政**，或**政府财政**，指[政府的收入和支出](../Page/政府.md "wikilink")，是政府实现[经济政策目标](../Page/经济政策.md "wikilink")、提供[公共产品与](../Page/公共产品.md "wikilink")[服务的主要途径之一](../Page/服务.md "wikilink")。这里[政府可以是代表一个](../Page/政府.md "wikilink")[国家的](../Page/国家.md "wikilink")[中央政府](../Page/中央政府.md "wikilink")，也可以是[地方政府](../Page/地方政府.md "wikilink")。国家或地区每年的财政的支出预算，以及其比例大小，一般可以体现一个国家或地区未来一段时间的重点发展方向。公共财政之外，政府实现[经济政策目标的其他途径包括](../Page/经济政策.md "wikilink")[货币政策](../Page/货币政策.md "wikilink")、[贸易政策](../Page/贸易.md "wikilink")、[法律](../Page/法律.md "wikilink")[法规等](../Page/法规.md "wikilink")。

  - 财政作为对[市场的调节和补充](../Page/市场.md "wikilink")：[市场本身并不能有效地解决某些](../Page/市场.md "wikilink")[经济](../Page/经济.md "wikilink")、[环境或](../Page/环境.md "wikilink")[社会](../Page/社会.md "wikilink")[稳定与](../Page/稳定.md "wikilink")[公正的问题](../Page/公正.md "wikilink")。
  - 财政的特点是：以政府[征税与](../Page/征税.md "wikilink")[立法的权力为基础](../Page/立法.md "wikilink")，具有某种程度上的强制性。
  - 财政的本质和内容是：一种由政府主导的对[资源](../Page/资源.md "wikilink")、[财富](../Page/财富.md "wikilink")、[社会福利的分配关系](../Page/社会福利.md "wikilink")。

## 政府收入

一国的财政的收入，一般包括以下几类：

  - 财政收入
      - [税收收入](../Page/税收.md "wikilink")
      - [非税收收入](../Page/非税收收入.md "wikilink")，如[国有资产收益](../Page/国有资产.md "wikilink")、[收费收入](../Page/收费.md "wikilink")、资产出售收入、[主权财富基金收入](../Page/主权财富基金.md "wikilink")
  - [公债收入](../Page/公债.md "wikilink")
  - 印钞或通胀

政府对金融活动做出的选择很大程度上影响着收入和财富分配，乃至市场的运行效率。税收是如何影响收入分配的问题可以通过[租税负担率来衡量](../Page/租税负担率.md "wikilink")，租税负担率可以显示一国国民的租税负担程度。公共财政的相关研究也对不同类型的税收、公债、行政活动所造成的影响进行分析。

### [税收](../Page/税收.md "wikilink")

税收是现代公共财政的核心部分，其地位之高不仅在于税收是当今财政收入的最主要成分，也因为税收负担日渐加剧的严重性。\[1\]
税收的主要目标在于提高收益，高水平的税收收入对于[福利国家填补](../Page/福利国家.md "wikilink")[债务极为重要](../Page/债务.md "wikilink")。税收的另一个主要目标在于维护社会公平，例如以限制财富来一定程度消除不平等。因此对于现代政府来说，税收不但需要提高财政收入水平以维系日益增长的行政活动，提供公共产品和公共管理，还需要减少社会财富和收入不平等。税收还用于限制资本以防范[通货膨胀](../Page/通货膨胀.md "wikilink")。\[2\]

税收收入既包括国家或相对应政治实体对个人和法人实体征收的收入，又包括地方政府征收的收入。税收内容又可分为[直接税和](../Page/直接税.md "wikilink")[间接税](../Page/间接税.md "wikilink")。

### [公债](../Page/公债.md "wikilink")

与其他经济实体类似，政府也可以贷款、发行[债券](../Page/债券.md "wikilink")、进行投资。公债包括[国债和](../Page/国债.md "wikilink")[地方债等形式](../Page/地方债.md "wikilink")，部分地方政府通过税务机关发行地方债。

## 政府支出

### 转移支付

财政转移支付是上级政府与下级政府之间的财政资金转移，以实现各地公平服务水平均等化的财政举措。

## 盈余与赤字

### 盈余

### 赤字、负债与风险

国际上，评价一国的财政是否有风险，沒有統一的標準。在歐洲國家，政府有以下两个指标：

1.  [赤字率](../Page/赤字率.md "wikilink")，[赤字占](../Page/赤字.md "wikilink")[GDP不超过](../Page/GDP.md "wikilink")3%
2.  [负债率](../Page/负债率.md "wikilink")，[国债余额占GDP不超过](../Page/国债余额.md "wikilink")60%

但這些準則並不適用於全世界，不同國家有不同的財政及貨幣政策。日本的國債已超過GDP的200%。但日本過去長期實施[零利率](../Page/零利率.md "wikilink")，現在更實施[負利率](../Page/負利率.md "wikilink")，使政府即使背負巨大債務，仍有能力支付利息。

### 破产

#### 地方政府破产

城市经济衰落、人口迁徙等因素，可能最终造成地方政府长期财政赤字，加大负债，最终引发破产。

## 参考文献

## 外部链接

  - [UK
    Bankruptcy](https://web.archive.org/web/20090226195529/http://www.clearinsolvency.co.uk/bankruptcy.php)

## 参见

  - [经济政策](../Page/经济政策.md "wikilink")

{{-}}

[公共财政](../Category/公共财政.md "wikilink")
[Category:公共行政](../Category/公共行政.md "wikilink")
[Category:宏观经济学](../Category/宏观经济学.md "wikilink")

1.
2.