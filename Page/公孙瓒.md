[Gongsun_Zan_Qing_illustration.jpg](https://zh.wikipedia.org/wiki/File:Gongsun_Zan_Qing_illustration.jpg "fig:Gongsun_Zan_Qing_illustration.jpg")
**公孙瓒**（），字**伯圭**（出《劉寬碑陰》名字取自圭瓚一詞，文史多作**伯珪**，當取碑），[东汉末期](../Page/东汉.md "wikilink")[人物](../Page/人物.md "wikilink")，[幽州](../Page/幽州.md "wikilink")[辽西](../Page/辽西郡.md "wikilink")[令支人](../Page/令支县.md "wikilink")，曾任[中郎将](../Page/中郎将.md "wikilink")，封[都亭侯](../Page/列侯.md "wikilink")，[幽州](../Page/幽州.md "wikilink")[刺史](../Page/刺史.md "wikilink")。

曾与[刘备和](../Page/刘备.md "wikilink")[刘德然共同师事于](../Page/刘德然.md "wikilink")[卢植](../Page/卢植.md "wikilink")。镇守[辽西时曾与](../Page/辽西郡.md "wikilink")[烏桓](../Page/烏桓.md "wikilink")、[鮮卑等交战](../Page/鮮卑.md "wikilink")，尽选白[马为先锋](../Page/马.md "wikilink")，自号“白马义从”。当时[袁术派遣](../Page/袁术.md "wikilink")[孙坚驻屯于](../Page/孙坚.md "wikilink")[阳城](../Page/阳城.md "wikilink")[以拒董卓](../Page/董卓討伐戰.md "wikilink")，[袁绍派](../Page/袁绍.md "wikilink")[周昂夺取孙坚驻守之处](../Page/周昂.md "wikilink")，袁术便派公孙瓒从弟[公孙越与孙坚一同](../Page/公孙越.md "wikilink")[攻打周昂](../Page/陽城之戰.md "wikilink")，没有获胜，公孙越为流矢所中死。公孙瓚以其弟被杀，将此事归[罪于袁绍](../Page/罪.md "wikilink")，并和袁绍争夺[北方连年交战](../Page/北方.md "wikilink")。

[建安四年](../Page/建安_\(东汉\).md "wikilink")（199年）被袁绍击败，先将其[姊妹](../Page/姊妹.md "wikilink")[妻](../Page/妻.md "wikilink")[儿全部](../Page/儿.md "wikilink")[缢死](../Page/缢死.md "wikilink")，然后引火[自焚](../Page/自焚.md "wikilink")，在公孙瓒自焚时，袁绍[士兵登上公孙瓒所在](../Page/士兵.md "wikilink")[高台并将其](../Page/高台.md "wikilink")[斩首](../Page/斩首.md "wikilink")。

## 生平

### 豪族出身

公孙瓒生年不详，只知年长于161年出生的刘备。

公孫瓚是[豪族子弟](../Page/豪族.md "wikilink")，但因[母親出身低微](../Page/母親.md "wikilink")，只能任書佐。因美貌、聲音洪亮與才智受[太守侯某賞識](../Page/太守.md "wikilink")，被邀請為[女婿](../Page/女婿.md "wikilink")。受[岳父](../Page/岳父.md "wikilink")[幫助曾與](../Page/幫助.md "wikilink")[劉備和](../Page/劉備.md "wikilink")[劉德然共同師事於](../Page/劉德然.md "wikilink")[盧植](../Page/盧植.md "wikilink")。

### 以義顯名

公孫瓚後來在太守劉其（又作劉基）\[1\]下任御車。在劉太守犯法被發配[交州](../Page/交州.md "wikilink")[日南時敢於違法喬裝成士兵沿途護送](../Page/日南郡.md "wikilink")，途中劉其獲赦還。公孫瓚歸來後因此德行被舉[孝廉](../Page/孝廉.md "wikilink")，任為[遼東屬國](../Page/遼東屬國.md "wikilink")[長史](../Page/長史.md "wikilink")。

### 白馬異雄

公孫瓚任長史間以數十[騎兵擊敗數百](../Page/騎兵.md "wikilink")[鮮卑騎兵](../Page/鮮卑.md "wikilink")，升為[涿](../Page/涿州市.md "wikilink")[令](../Page/縣官.md "wikilink")。[光和](../Page/光和.md "wikilink")（[三國志](../Page/三國志.md "wikilink")）或[中平](../Page/中平.md "wikilink")（[後漢書](../Page/後漢書.md "wikilink")）年間，因要討伐[涼州賊被授與](../Page/涼州.md "wikilink")[都督行事傳](../Page/都督.md "wikilink")。後因討伐[漁陽](../Page/漁陽郡.md "wikilink")[張純與遼西](../Page/張純_\(漁陽\).md "wikilink")[烏丸](../Page/烏丸.md "wikilink")[丘力居等有功](../Page/丘力居.md "wikilink")，升為[騎都尉](../Page/騎都尉.md "wikilink")。因[烏丸貪至王率眾人降公孫瓚](../Page/烏丸.md "wikilink")，公孫瓚被升為中郎將，封都亭侯。公孫瓚組織數十擅射[騎兵盡騎](../Page/騎兵.md "wikilink")[白馬](../Page/白馬.md "wikilink")，自號「白馬義從」。[烏桓因此互相告知躲避白馬長史](../Page/烏桓.md "wikilink")，並自此逃離塞外。

### 疏獷趫猛

公孫瓚因曾勸阻[劉虞派兵助](../Page/劉虞.md "wikilink")[袁術而怕袁術怪罪](../Page/袁術.md "wikilink")，所以派其弟[公孫越領千餘人助袁術](../Page/公孫越.md "wikilink")、[孫堅反擊](../Page/孫堅.md "wikilink")[袁绍部下](../Page/袁绍.md "wikilink")[周昂](../Page/周昂.md "wikilink")，於[陽城之戰為流矢所中而死](../Page/陽城之戰.md "wikilink")。公孫瓚以此遷怒袁绍。袁紹初欲以自身[勃海](../Page/勃海郡.md "wikilink")[太守印綬給與公孫瓚弟](../Page/太守.md "wikilink")[公孫範作為巴結](../Page/公孫範.md "wikilink")，但公孫範轉為巴結公孫瓚。[初平二年](../Page/初平.md "wikilink")（191年），公孫範以勃海兵助公孫瓚率二萬人大破青州黃巾軍三十萬人，斬首三萬人，生擒七萬餘人，公孫瓚升為[奮武將軍](../Page/將軍_\(中國古代\).md "wikilink")，封[薊](../Page/北京市.md "wikilink")[侯](../Page/縣侯.md "wikilink")。羅列袁紹十大罪狀，[冀州諸城官員向公孫瓚投降](../Page/冀州_\(古代\).md "wikilink")。公孫瓚自封三州刺史。[嚴綱為](../Page/嚴綱.md "wikilink")[冀州](../Page/冀州_\(古代\).md "wikilink")[刺史](../Page/刺史.md "wikilink")，[田楷為](../Page/田楷.md "wikilink")[青州](../Page/青州_\(古代\).md "wikilink")[刺史](../Page/刺史.md "wikilink")，[單經為](../Page/單經.md "wikilink")[兗州](../Page/兗州_\(古代\).md "wikilink")[刺史](../Page/刺史.md "wikilink")。

  - [初平二年](../Page/初平.md "wikilink")（191年）冬，公孫瓚與[袁紹發生界橋之戰](../Page/袁紹.md "wikilink")，被袁紹將麴義擊敗，嚴綱陣亡。公孫瓚退回[蓟](../Page/北京市.md "wikilink")，屯軍勃海。

<!-- end list -->

  - 初平三年（192年），公孫瓚在界橋之戰後不久就派兵與袁紹在龍湊開戰，被袁紹擊敗，公孫瓚退還[幽州](../Page/幽州.md "wikilink")。

<!-- end list -->

  - 初平三年（192年），袁紹派[崔巨業領兵圍故安](../Page/崔巨業.md "wikilink")，久攻不下。撤退時被公孫瓚派三萬人追擊，在巨馬水大破袁紹軍，殺七八千人。

### 驕奢忘善

劉虞怕公孫瓚而欲先發制人，但被公孫瓚所敗，並在[居庸俘虜劉虞](../Page/居庸.md "wikilink")。[漢獻帝遣使者](../Page/漢獻帝.md "wikilink")[段訓欲增加劉虞的城邑](../Page/段訓.md "wikilink")，升公孫瓚為[前將軍](../Page/前將軍.md "wikilink")，封[易](../Page/易縣.md "wikilink")[侯](../Page/縣侯.md "wikilink")。但公孫瓚誣蔑劉虞意欲謀反以威脅段訓殺劉虞，並封自己為幽州刺史。至此公孫瓚盡有幽州之地。但殺害劉虞對公孫瓚的名聲做成重大的損害，部下向心力大減，對日後部下紛紛向袁紹投降埋下伏線。

### 猶據自全

公孫瓚有幽州後，迫害有名氣及富有的人，任用親信小人，有才能的將領都離開公孫瓚。劉虞舊部[鮮于輔](../Page/鮮于輔.md "wikilink")、[齊周](../Page/齊周.md "wikilink")、[鮮于銀推舉](../Page/鮮于銀.md "wikilink")[閻柔為烏桓](../Page/閻柔.md "wikilink")[司馬與公孫瓚將](../Page/司馬.md "wikilink")[鄒丹戰於](../Page/鄒丹.md "wikilink")[潞河之北](../Page/潞河.md "wikilink")，斬殺鄒丹等四千餘人。烏桓峭王及後與劉虞子[劉和合袁紹兵於興平二年](../Page/劉和.md "wikilink")(195年)，破公孫瓚於鮑丘，殺鄒丹等二萬餘人。公孫瓚所轄各地紛紛殺當地長吏而投奔劉和軍。公孫瓚加強易京的守備，自此只守不攻。建安元年（196年），公孫瓚不援救其部下，被袁紹逐個擊破。

建安四年（199年），袁紹率兵攻公孫瓚。公孫瓚屡战屡败后退守[易京](../Page/易縣.md "wikilink")，与[张燕联合](../Page/張燕_\(東漢\).md "wikilink")，希望夹击袁绍。袁紹将计就计，設下伏兵。公孫瓚中伏後，心知必敗，退回城中自殺。

## 家庭

### 兄弟

  - [公孙越](../Page/公孙越.md "wikilink")：公孫瓚從弟。被公孫瓚派往助袁術與孫堅一同攻打周昂，為流矢所中而死。
  - [公孫範](../Page/公孫範.md "wikilink")：公孫瓚從弟。以勃海兵助公孫瓚。在界橋之戰中與公孫瓚一同敗走。

### 子女

  - [公孙续](../Page/公孙续.md "wikilink")：公孫瓚之子。被公孫瓚派往黑山求張燕來救已遲，後為[屠各所殺](../Page/屠各.md "wikilink")。

## 部下

  - [嚴綱](../Page/嚴綱.md "wikilink")：被公孫瓚任為冀州刺史，界橋之戰中帶領先鋒，被殺。
  - [田楷](../Page/田楷.md "wikilink")：被公孫瓚任為青州刺史，在巨馬水之戰中領軍。
  - [單經](../Page/單經.md "wikilink")：被公孫瓚任為兗州刺史。
  - [鄒丹](../Page/鄒丹.md "wikilink")：在鮑丘之戰中領軍，被殺。
  - [關靖](../Page/關靖.md "wikilink")：長史，勸阻公孫瓚出撃冀州，公孫瓚死後與袁紹軍決戰，戰死。
  - [劉備](../Page/劉備.md "wikilink")：平原令，與田楷共拒袁紹，後與田楷到[徐州救](../Page/徐州.md "wikilink")[陶謙抗拒](../Page/陶謙.md "wikilink")[曹操](../Page/曹操.md "wikilink")，後為陶謙部下。
  - [趙雲](../Page/趙雲.md "wikilink")：隨劉備守平原，領導騎兵。
  - [田豫](../Page/田豫.md "wikilink")：[東州](../Page/東州.md "wikilink")[縣令](../Page/縣令.md "wikilink")，王門叛變時眾人-{欲}-降，田豫說服王門退兵。
  - [王門](../Page/王門.md "wikilink")：叛變獲袁紹給與萬餘人來攻，田豫說服王門退兵。
  - [范方](../Page/范方.md "wikilink")：率領公孫瓚支援[劉岱的部隊](../Page/劉岱.md "wikilink")，後因劉岱聽從[程昱之意見接近袁紹而遭撤回](../Page/程昱.md "wikilink")。
  - [季雍](../Page/季雍.md "wikilink")：公孫瓚麾下鄃縣令。初從袁紹，後雍以鄃縣叛紹而降公孫瓚，瓚遣兵衛之。紹遣朱靈攻之，力戰拔之，生擒雍。
  - [文則](../Page/文則.md "wikilink")：建安四年（199年），黑山帥張燕與公孫續率兵十萬，分三路相救公孫瓚。援兵還沒到，公孫瓚秘密派部下文則送信給公孫續，途中被袁紹劫得了信。
  - [李邵](../Page/李邵.md "wikilink")：鉅鹿太守，在公孫瓚討伐袁紹時倒向公孫瓚，但被董昭用計擺平。
  - [孫伉](../Page/孫伉.md "wikilink")：親公孫瓚一方，在董昭擺平李邵時被殺。
  - [張吉](../Page/張吉.md "wikilink")：公孫瓚的部下，董昭利用他散佈假情報擺平了李邵、孫伉。
  - [劉緯臺](../Page/劉緯臺.md "wikilink")（算命的），[李移子](../Page/李移子.md "wikilink")（賣布的），[樂何當](../Page/樂何當.md "wikilink")（小商販）：公孫瓚拜把兄弟，都受公孫瓚親待。[王粲](../Page/王粲.md "wikilink")《英雄記》有載。
  - [公孫紀](../Page/公孫紀.md "wikilink")：原是劉虞的部下，因公孫瓚以同姓而兄弟相待，而把劉虞要殺公孫瓚的消息告訴公孫瓚。

## 艺术形象

### 演義記載

在演義中，公孫瓚猶如劉備恩人一般的存在。因與劉備有學友之誼，在討伐漁陽[張舉](../Page/張舉.md "wikilink")、[張純之亂時就起用了劉備](../Page/張純.md "wikilink")，後使其駐守[平原](../Page/平原縣.md "wikilink")。

討伐[董卓時](../Page/董卓.md "wikilink")，公孫瓚也參予其中，並接納劉備成為部將，同往會盟。汜水關之戰中，公孫瓚於袁紹面前引薦劉備，稱其為漢室之冑，袁紹乃賜座予劉備；虎牢關之戰中，公孫瓚部隊被指示前往參戰，在關東聯軍折損數將後，麾下劉備、關羽、張飛三人與[呂布於關前大戰](../Page/呂布.md "wikilink")，最後逼退了呂布。

關東聯軍因諸侯產生嫌隙而解散後，公孫瓚與袁紹圖謀合作奪取冀州，不料由袁紹捷足先登。公孫瓚派遣其弟公孫越至袁紹處求取冀州土地，袁紹反悔不與，甚至在其回程中派遣部將假借董卓名義，將之殺害；公孫瓚得知後勃然大怒，遣軍進攻袁紹，對峙於磐河。

序戰中，袁紹部將[麴義驍勇善戰](../Page/麴義.md "wikilink")，一馬當先斬殺公孫瓚部將嚴綱，瓚軍士氣低落而大敗；而後趙雲出現，加上劉備率領援軍適時到來，戰況隨之扭轉。兩軍在長期對峙後，接受了董卓的調停而撤兵。

董卓被[王允與呂布暗殺後](../Page/王允.md "wikilink")，在徐州又爆發曹操為報父仇而興兵的復仇戰，徐州牧陶謙聽從麾下謀士糜竺的建議，請求孔融為援軍，而孔融也推薦平原相劉備，於是劉備以此為契機，向公孫瓚借取趙雲與數千兵馬前往救援徐州。曹操因為長時間於徐州征戰，導致後防空虛，被呂布、陳宮、張邈等人奪取根據地，不得已而撤退，完成了救援使命的趙雲也帶著兵馬返回了公孫瓚處。

此後隨著劉備長期轉戰徐州，幾乎沒有與公孫瓚有任何聯繫。最終由曹操麾下的[滿寵告知劉備](../Page/滿寵.md "wikilink")，公孫瓚已敗於袁紹之手，死於易京的消息。

### 動漫作品

  - [三國演義](../Page/三國演義_\(動畫\).md "wikilink")
  - 《[橫山光輝三國志](../Page/橫山光輝三國志.md "wikilink")》（[橫山光輝](../Page/橫山光輝.md "wikilink")）
  - 《[蒼天航路](../Page/蒼天航路.md "wikilink")》（[王欣太](../Page/王欣太.md "wikilink")）
  - 《[火鳳燎原](../Page/火鳳燎原.md "wikilink")》（[陳某](../Page/陳某.md "wikilink")）

### 影视

  - 中國[中央電視台電視劇](../Page/中央電視台.md "wikilink")《[三國演義](../Page/三国演义_\(电视剧\).md "wikilink")》（1994年）：[杨凡](../Page/杨凡.md "wikilink")
  - 台灣[華視電視劇](../Page/華視.md "wikilink")《[三國英雄傳之關公](../Page/三國英雄傳之關公.md "wikilink")》（1996年）：[管谨宗](../Page/管谨宗.md "wikilink")
  - 中国中央电视台电视剧《[武圣关公](../Page/武圣关公.md "wikilink")》（2004年）：[贺琳](../Page/贺琳.md "wikilink")
  - 中國電視劇《[三国](../Page/三国_\(电视剧\).md "wikilink")》（2010年）：[王宝刚](../Page/王宝刚.md "wikilink")
  - 电视剧《[武神赵子龙](../Page/武神赵子龙.md "wikilink")》（2016年）：[范雨林](../Page/范雨林.md "wikilink")

## 評價

  - [魏攸](../Page/魏攸.md "wikilink")：“瓒，文武才力足恃，虽有小恶，固宜容忍。”（《后汉书·袁绍刘表列传第六十四上》）
  - [袁绍](../Page/袁绍.md "wikilink")：“超然自逸，矜其威诈。”（《后汉书·袁绍刘表列传第六十四上》）
  - 《[三國志](../Page/三國志.md "wikilink")》作者[陳壽](../Page/陳壽.md "wikilink")：“瓒遂骄矜，记过忘善，多所贼害”；「公孙瓚保京，坐待夷灭。」
  - 《[後漢書](../Page/後漢書.md "wikilink")》作者[范曄](../Page/范曄.md "wikilink")：「伯珪疏獷，武才趫猛。」；“瓒恃其才力，不恤百姓，记过忘善，睚眦必报，州里善士名在其右者，必以法害之。”；“自帝室王公之胃，皆生长脂腴，不知稼穑，其能厉行饬身，卓然不群者，或未闻焉。刘虞守道慕名，以忠厚自牧。美哉乎，季汉之名宗子也！若虞、瓒无间，同情共力，纠人完聚，稸保燕、蓟之饶，缮兵昭武，以临群雄之隙，舍诸天运，征乎人文，则古之休烈，何远之有！”
  - [乞伏昙达](../Page/乞伏昙达.md "wikilink")：“昔伯珪凭险，卒有灭宗之祸；韩约肆暴，终受覆族之诛。”（《晋书·卷一百二十五·载记第二十五》）
  - [王勃](../Page/王勃.md "wikilink")：“区区公路，欲据列郡之尊；琐琐伯珪，谓保易京之业。瓒既窘毙，术亦忧终。”（《三国论》）
  - [洪迈](../Page/洪迈.md "wikilink")：“公孙瓒筑京于易，以为足以待天下之变，不知卫梯舞于楼上，城岂可保耶？”（《[容斋随笔](../Page/容斋随笔.md "wikilink")》卷十四）
  - [郝经](../Page/郝经.md "wikilink")：“瓒挟劲气，辄害宗子。百楼虽多，云胡不死。”（《续后汉书》）
  - [王夫之](../Page/王夫之.md "wikilink")：“顽悍而乐杀者公孙瓒，而犹据土以自全。”（《[读通鉴论](../Page/读通鉴论.md "wikilink")》）
  - [柳从辰](../Page/柳从辰.md "wikilink")：“卓虽受诛，豪杰并起，跨州连郡如[刘虞](../Page/刘虞.md "wikilink")、公孙瓒、[陶谦](../Page/陶谦.md "wikilink")、袁绍、[刘表](../Page/刘表.md "wikilink")、[刘焉](../Page/刘焉.md "wikilink")、[袁术](../Page/袁术.md "wikilink")、[吕布者](../Page/吕布.md "wikilink")，皆尝雄视一时，其权力犹足匡正帝室。”
  - [蔡东藩](../Page/蔡东藩.md "wikilink")：“公孙瓒之致死，其失与袁术相同。术死于侈，瓒亦未尝不由侈而死。观其建筑层楼，重门固守，妇女传宣，将士解散，彼且诩诩然自夸得计。一则曰吾有积谷三百万斛，食尽此谷，再觇时变。再则曰当今四方虎争，无一能坐吾城下。谁知绍兵骤至，全城被围，鼓角鸣于地中，柱火焚于楼下，有欲免一死而不可得者，较诸袁术之结局，其惨尤甚！”

## 注释

<references/>

## 参考资料

  - 《[三国志](../Page/三国志.md "wikilink")·[魏书八·二公孙陶四张传第八](../Page/s:三國志/卷08.md "wikilink")》
  - 《[三国志](../Page/三国志.md "wikilink")·[魏書二十六·滿田牽郭傳](../Page/s:三國志/卷26.md "wikilink")》
  - 《[三国志](../Page/三国志.md "wikilink")·[蜀书二·先主传第二](../Page/s:三國志/卷32.md "wikilink")》
  - 《[后汉书](../Page/后汉书.md "wikilink")·[卷七十三·刘虞公孙瓚陶谦列传第六十三](../Page/s:後漢書/卷73.md "wikilink")》
  - 《水经注 卷十四》

[G](../Category/东汉军阀.md "wikilink") [G](../Category/东汉官员.md "wikilink")
[G](../Category/东汉亭侯.md "wikilink")
[S蘇](../Category/東漢軍事人物.md "wikilink")
[S蘇](../Category/東漢政治人物.md "wikilink")
[S蘇](../Category/東漢戰爭身亡者.md "wikilink")
[G](../Category/迁安人.md "wikilink") [Z瓚](../Category/公孫姓.md "wikilink")
[Category:中国自焚人物](../Category/中国自焚人物.md "wikilink")

1.  《太平御览》卷422引《英雄记》：“公孙瓒字伯-{珪}-，为上计吏。郡太守刘基为事被征，伯-{珪}-御重到洛阳，身执徒养。基将徙日南，伯-{珪}-具豚米于北邙上祭先人。