**公孙述**（），[字](../Page/表字.md "wikilink")**子阳**，[扶风](../Page/扶风.md "wikilink")[茂陵](../Page/茂陵.md "wikilink")（今[陕西](../Page/陕西.md "wikilink")[兴平县](../Page/兴平县.md "wikilink")）人。[两汉间政治人物](../Page/两汉.md "wikilink")。曾经割据[蜀郡](../Page/蜀郡.md "wikilink")，並以「[白帝](../Page/白帝.md "wikilink")」自比。

## 生平

[西汉末年](../Page/西汉.md "wikilink")，以父荫为郎，补清水（今属[甘肃](../Page/甘肃.md "wikilink")）县长。他为官有方，一方太平，因而闻名。[王莽篡](../Page/王莽.md "wikilink")[汉后](../Page/汉.md "wikilink")，任导江卒正（即[蜀郡太守](../Page/蜀郡.md "wikilink")）。新朝末年，自称辅汉将军兼任[益州牧](../Page/益州.md "wikilink")，势力大增，自稱為蜀王。

东[汉光武帝](../Page/汉光武帝.md "wikilink")[建武元年](../Page/建武.md "wikilink")（25年）四月，與劉秀同年自立为天子，国号“[成家](../Page/成家.md "wikilink")”，建元龙兴。公孙述迷信讳谶符命之说，废止铜钱，设官铸铁钱，一時間難以流通。好事者竊言“黃牛白腹，五銖當復。”\[1\]建武五年（30年），光武帝派[耿弇等由隴道伐公孫述](../Page/耿弇.md "wikilink")，[隗囂稱臣於公孫述](../Page/隗囂.md "wikilink")，公孫述封其為“朔寧王”。建武十一年（35年），光武帝派兵征讨，不克。

建武十二年冬十一月戊寅（36年12月24日），[东汉大司马](../Page/东汉.md "wikilink")[吴汉](../Page/吳漢_\(漢朝\).md "wikilink")、[臧宫于](../Page/臧宫.md "wikilink")[成都打败了公孙述](../Page/成都.md "wikilink")，公孙述受伤当夜死亡\[2\]。吴汉破屠成都，纵兵大掠，[白帝城化為灰燼](../Page/白帝城.md "wikilink")，漢軍尽诛公孙氏及[延岑](../Page/延岑.md "wikilink")，成家亡，存世凡十二年。應驗了公孫述稱帝前的夢：\[3\]「八厶子系，十二為期。」

## 相关人物

  - 親族

<!-- end list -->

  - 公孫仁（父）
  - 公孫光（弟）为[大司马](../Page/大司马.md "wikilink")
  - 公孫恢（弟）为[大司空](../Page/大司空.md "wikilink")

<!-- end list -->

  - 同盟者

<!-- end list -->

  - [隗囂](../Page/隗囂.md "wikilink")
  - [秦丰](../Page/秦丰.md "wikilink")

<!-- end list -->

  - 軍閥成員

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/延岑.md" title="wikilink">延岑</a></li>
<li><a href="../Page/王元.md" title="wikilink">王元</a></li>
<li>荊邯</li>
<li>伍公</li>
<li>侯丹</li>
</ul></td>
<td><ul>
<li>任満</li>
<li>程烏</li>
<li>程焉</li>
<li>程汎</li>
<li><a href="../Page/田戎.md" title="wikilink">田戎</a></li>
</ul></td>
<td><ul>
<li>李育</li>
<li><a href="../Page/呂鮪.md" title="wikilink">呂鮪</a></li>
<li>楊春卿</li>
</ul></td>
</tr>
</tbody>
</table>

## 注釋

<div class="references-small">

<references />

</div>

## 参考

《[華陽國志](../Page/華陽國志.md "wikilink")·[公孫述劉二牧志](../Page/s:華陽國志/卷五#公孫述.md "wikilink")》

[G公](../Category/新朝民變領袖.md "wikilink")
[Category:東漢被處決者](../Category/東漢被處決者.md "wikilink")
[G公](../Category/中国皇帝.md "wikilink") [S述](../Category/公孫姓.md "wikilink")
[G](../Category/西漢縣令.md "wikilink") [G](../Category/西漢郎.md "wikilink")

1.  《後漢書·五行志》曰：“世祖建武六年，蜀中童謠。是時公孫述僭號於蜀，時人竊言王莽稱黃，述欲繼之，故稱白。五銖，漢家貨，明當複也。述遂誅滅。”
2.  《后汉书》，卷一上‧光武帝纪
3.  《後漢書‧隗囂公孫述列傳》：「述夢有人語之曰：『八厶子系，十二為期。』」