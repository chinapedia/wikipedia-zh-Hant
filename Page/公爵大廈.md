[Edinburgh_Tower.jpg](https://zh.wikipedia.org/wiki/File:Edinburgh_Tower.jpg "fig:Edinburgh_Tower.jpg")
[The_Landmark_Edinburgh_Tower_Lobby_201501.jpg](https://zh.wikipedia.org/wiki/File:The_Landmark_Edinburgh_Tower_Lobby_201501.jpg "fig:The_Landmark_Edinburgh_Tower_Lobby_201501.jpg")
[The_Landmark_Mandarin_Oriental_Hotel_Lobby_2015.jpg](https://zh.wikipedia.org/wiki/File:The_Landmark_Mandarin_Oriental_Hotel_Lobby_2015.jpg "fig:The_Landmark_Mandarin_Oriental_Hotel_Lobby_2015.jpg")
**公爵大廈**（）是[香港的一座商業大廈](../Page/香港.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[中環](../Page/中環.md "wikilink")[皇后大道中](../Page/皇后大道.md "wikilink")15號，樓高44層，於1983年落成，為[置地廣塲的一部份](../Page/置地廣塲.md "wikilink")。

## 歷史

公爵大廈的原址[皇后大道中](../Page/皇后大道.md "wikilink")13-15號是建於1920年代的**公主行**（**Marina
House**）。於1983年改建時與[皇后大道中](../Page/皇后大道.md "wikilink")11-13號的**公爵行**（**Edinburgh
Building**）合併發展，公爵行重建為五層高的置地廣場東翼，而公主行重建為公爵大廈。

## 租戶

公爵大廈的租戶包括第一亞洲金融集團有限公司、[羅兵咸永道會計師事務所](../Page/羅兵咸永道會計師事務所.md "wikilink")、眾達國際法律事務所、[鞍鋼股份有限公司](../Page/鞍鋼股份有限公司.md "wikilink")、[中國匯源果汁集團有限公司](../Page/中國匯源果汁集團有限公司.md "wikilink")、突破文法（香港）有限公司等。

## 樓層分佈

  - 17樓：DLA Piper Hong Kong
  - 18樓：Stephenson Harwood
  - 19樓：霸菱資產管理（亞洲）有限公司（1901室）
  - 21樓：羅兵咸永道會計師事務所
  - 31樓：眾達國際法律事務所
  - 33樓：Morrison & Foerster LLP
  - 35樓：Campbells Law Firm（3507室）、Central Wedding（3510室）
  - 37樓：Seyfarth Shaw LLP（3701室）
  - 39樓：史蒂文生黃律師事務所
  - 40樓：Bank J. Safra Sarasin AG
  - 42樓：Skadden, Arps, Slate, Meagher & Flom LLP
  - 44樓：高蓋茨律師事務所

## 酒店

公爵大廈的最低14層設有一家五星級酒店——置地文華東方酒店，於2005年9月正式開業。[1](http://the-sun.on.cc/channels/fina/20060309/20060309013645_0000_3.html)

## 交通

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")、<font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[中環站](../Page/中環站.md "wikilink")

<!-- end list -->

  - [電車](../Page/香港電車.md "wikilink")

## 參見

  - [置地廣場](../Page/置地廣場.md "wikilink")
  - [告羅士打大廈](../Page/告羅士打大廈.md "wikilink")
  - [遮打大廈](../Page/遮打大廈.md "wikilink")
  - [約克大廈](../Page/約克大廈.md "wikilink")
  - [置地文華東方酒店](../Page/置地文華東方酒店.md "wikilink")

## 外部連結

  - [公爵大廈](https://web.archive.org/web/20070523000753/http://www.hkland.com/commercial_property/hongkong_properties/edinburghtower/index.html)

[Category:香港置地物業](../Category/香港置地物業.md "wikilink")
[Category:香港中環寫字樓](../Category/香港中環寫字樓.md "wikilink")
[Category:中西區寫字樓 (香港)](../Category/中西區寫字樓_\(香港\).md "wikilink")
[Category:中環](../Category/中環.md "wikilink")
[Category:置地廣場](../Category/置地廣場.md "wikilink")