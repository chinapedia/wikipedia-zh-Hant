**公車上書**，即**公車孝廉連署上書**，指[康有为于](../Page/康有为.md "wikilink")[清朝](../Page/清朝.md "wikilink")[光绪二十一年](../Page/光绪.md "wikilink")（1895年）率同[梁启超等](../Page/梁启超.md "wikilink")[公車孝廉](../Page/公車_\(漢朝\).md "wikilink")\[1\]联名向[北京的](../Page/北京.md "wikilink")[光绪皇帝上书](../Page/光绪皇帝.md "wikilink")，反对在[甲午战争中败于](../Page/甲午战争.md "wikilink")[日本的清政府签订丧权辱国的](../Page/日本.md "wikilink")《[马关条约](../Page/马关条约.md "wikilink")》。此舉被认为是[维新派登上历史舞台的标志](../Page/维新派.md "wikilink")，也被认为是中国群众的[政治运动的开端](../Page/政治运动.md "wikilink")。

如[茅海建等不少学者则考証许多文献表示](../Page/茅海建.md "wikilink")，康有为所谓的组织「公車[孝廉連署上書](../Page/孝廉.md "wikilink")」，事实上，只是一次自吹自擂的事件，只有八十個舉人參加[連署](../Page/連署.md "wikilink")，而非康有為所稱的一千多人。而當時仅仅一名廣東舉人[陳景華的上書都有兩百八十人的連署](../Page/陳景華.md "wikilink")，遠勝康有為。其實當時也有真正成功的上書，实由当时的[翁同龢](../Page/翁同龢.md "wikilink")、[李鸿藻](../Page/李鸿藻.md "wikilink")、[汪鸣銮等京城高官发动组织](../Page/汪鸣銮.md "wikilink")，目的是阻挠《[马关条约](../Page/马关条约.md "wikilink")》的签订。\[2\]

## 背景

[缩略图](https://zh.wikipedia.org/wiki/File:Litiefu1904.jpg "fig:缩略图") 布上油畫
70.7×56cm|右\]\]
光緒二十年（1894年）[中日甲午战争](../Page/中日甲午战争.md "wikilink")，中國败于日本。光緒二十一年（1895年）春，参加完乙未科會試的各省[举人雲集](../Page/举人.md "wikilink")[北京](../Page/北京.md "wikilink")，等待發榜。[李鴻章與](../Page/李鴻章.md "wikilink")[伊藤博文簽訂的](../Page/伊藤博文.md "wikilink")《[马关条约](../Page/s:馬關條約.md "wikilink")》內割讓[台灣及](../Page/台灣.md "wikilink")[遼東](../Page/遼東.md "wikilink")，賠款[白銀二萬萬兩的突然消息傳至](../Page/白銀.md "wikilink")，群情激愤。4月22日，康有為、梁啟超寫成一萬八千字的《[上今上皇帝書](../Page/s:上清帝第二書.md "wikilink")》，[內地十八省與](../Page/內地十八省.md "wikilink")[奉天三省](../Page/奉天.md "wikilink")（[中國東北](../Page/中國東北.md "wikilink")）舉人接連響應，共一千二百多人[連署](../Page/連署.md "wikilink")。5月2日，由康、梁二人帶領，各省舉人與數千北京官民集於「[都察院](../Page/都察院.md "wikilink")」門前請代奏[光緒帝](../Page/光緒帝.md "wikilink")。

## 结果及影响

虽然，上书被清朝官方拒绝，但在社会上产生了巨大影响。[康有为等以](../Page/康有为.md "wikilink")“变法图强”为号召，组织[强学会](../Page/强学会.md "wikilink")，在[北京](../Page/北京.md "wikilink")、[上海等地发行](../Page/上海.md "wikilink")[报纸](../Page/报纸.md "wikilink")，宣传维新思想。[严复](../Page/严复.md "wikilink")、[谭嗣同亦在其他地方宣传维新思想](../Page/谭嗣同.md "wikilink")。之后，[光绪帝启用康有为](../Page/光绪帝.md "wikilink")、梁啟超等，史称**[戊戌变法](../Page/戊戌变法.md "wikilink")（或**百日维新'''）。

公车上书和戊戌变法都先后失败，但是维新思想从此唤醒、激励了愈來愈多的中国人，思考中國如何救亡图存於[歐](../Page/歐洲.md "wikilink")[日列強](../Page/日本.md "wikilink")[帝國主義](../Page/帝國主義.md "wikilink")，在中国歷史上有着重要的地位。

## 争议

2005年北京大学历史学教授[茅海建在](../Page/茅海建.md "wikilink")《近代史研究》第3期和第4期上发表《“公车上书”考证补》，根据中国第一历史档案馆所藏档案及其已公开发表过的档案文献，从政治决策高层的角度，重新审视公车上书的背景、运作过程及其影响力。其观点是：“公车上书”是有两个不同的概念，一是由政治高层发动、京官组织的上书，其数量多达31件，签名的举人多达1555人次，且上书已达光緒的[御座](../Page/御座.md "wikilink")；另一个是由康有为组织的「十八行省举人联名上书」，那是一次流产的政治事件。而且康有为组织的「十八行省举人联名上书」，并不是[都察院不收](../Page/都察院.md "wikilink")，而是康有为根本没有去送。由此学术界进行了广泛而激烈的讨论，并且对“公车上书”进行了进一步的审视和研究。根据不少学者的看法，康有为在进行政治宣传以及回忆的时候（最明显如其《康南海自订年谱》）存在着很多不尊重事实的地方，而且又有很多吹牛造假的现象\[3\]。

如[茅海建等不少学者则根据许多文献认为康有为所谓的组织举人联名上书](../Page/茅海建.md "wikilink")，事实上是一次流产的政治事件。公车上书实则是由当时的[翁同龢](../Page/翁同龢.md "wikilink")、[李鸿藻](../Page/李鸿藻.md "wikilink")、[汪鸣銮等京城高官发动组织](../Page/汪鸣銮.md "wikilink")，目的是阻挠《[马关条约](../Page/马关条约.md "wikilink")》的签订。另有研究者认为，当时清政府内部已经趋于求变，即使是保守派的徐桐和[荣禄](../Page/荣禄.md "wikilink")，也曾对变法做过努力。公车上书的时候，十八行省“公车”绝大多数都没有参加康有為组织的[簽名運動](../Page/簽名運動.md "wikilink")，他只征集到80名广东人的[聯署](../Page/聯署.md "wikilink")。而仅仅是另一人[陈景华就鼓动了一场](../Page/陈景华.md "wikilink")280多人签名的「广东公车上书」。\[4\]

## 参见

  - [康有为](../Page/康有为.md "wikilink")
  - [强学会](../Page/强学会.md "wikilink")
  - [戊戌变法](../Page/戊戌变法.md "wikilink")

## \-{注}-解

## 外部链接

  - [康有为公车上书](https://web.archive.org/web/20061017193443/http://cyc6.cycnet.com:8090/xuezhu/llzs/content.jsp?n_id=519)
  - [茅海建《“公车上书”考证补（一）》](http://www.cnki.com.cn/Article/CJFDTOTAL-JDSY200503000.htm)
  - [茅海建《“公车上书”考证补（二）》](http://www.cnki.com.cn/Article/CJFDTOTAL-JDSY200504003.htm)
  - [“公车上书”真相](http://news.qq.com/zt2011/zxzg/1895.htm)
  - [有关公车上书的学术研究与争论](http://www.pep.com.cn/gzls/js/xsjl/zgsyj/201009/t20100926_914357.htm)

[Category:中國學生運動](../Category/中國學生運動.md "wikilink")
[Category:甲午戰爭](../Category/甲午戰爭.md "wikilink")
[Category:戊戌變法](../Category/戊戌變法.md "wikilink")
[Category:1895年中国](../Category/1895年中国.md "wikilink")
[Category:北京政治事件](../Category/北京政治事件.md "wikilink")
[Category:康有為](../Category/康有為.md "wikilink")

1.  [王丹](../Page/王丹.md "wikilink")、[羅文嘉](../Page/羅文嘉.md "wikilink")、[王軍濤](../Page/王軍濤.md "wikilink")《公共知識分子
    No.1: 茉莉花革命》
2.  [茅海建《“公车上书”考证补（一）》](http://www.cnki.com.cn/Article/CJFDTOTAL-JDSY200503000.htm)、[茅海建《“公车上书”考证补（二）》](http://www.cnki.com.cn/Article/CJFDTOTAL-JDSY200504003.htm)
3.  [豆棚闲话：康有为这人近乎骗子](http://www.chinanews.com.cn/hb/news/2009/11-03/1944820.shtml)
4.  [茅海建《“公车上书”考证补（一）》](http://www.cnki.com.cn/Article/CJFDTOTAL-JDSY200503000.htm)、[茅海建《“公车上书”考证补（二）》](http://www.cnki.com.cn/Article/CJFDTOTAL-JDSY200504003.htm)