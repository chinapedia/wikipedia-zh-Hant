**六亚甲基三过氧化二胺**（，**HMTD**）是一种[有机过氧化物](../Page/有机过氧化物.md "wikilink")[起爆藥](../Page/起爆藥.md "wikilink")，由Legler在1885年首先制得。\[1\]

作為一種[有机过氧化物](../Page/有机过氧化物.md "wikilink")，由於分子內[過氧鍵的存在](../Page/過氧鍵.md "wikilink")，HMTD對摩擦、火焰及物理衝擊較敏感，但穩定性和[TATP等相比尚佳](../Page/TATP.md "wikilink")。[紫外線的照射與金屬離子的存在都將敏化HMTD](../Page/紫外線.md "wikilink")。不含[酸](../Page/酸.md "wikilink")、[鹼和金屬離子](../Page/鹼.md "wikilink")、純淨的HMTD有著良好的化學穩定性且不易昇華。

HMTD由[过氧化氢与](../Page/过氧化氢.md "wikilink")[乌洛托品在](../Page/乌洛托品.md "wikilink")[柠檬酸或](../Page/柠檬酸.md "wikilink")[硫酸](../Page/硫酸.md "wikilink")[催化下於水溶液中反应制得](../Page/催化.md "wikilink")，合成較[雷汞簡便](../Page/雷汞.md "wikilink")，在起爆力、穩定性與成本方面亦有優勢，但起爆效果不如[叠氮化铅](../Page/叠氮化铅.md "wikilink")。曾用于矿井中的爆破，\[2\]但已被[特屈儿等更稳定的炸药所取代](../Page/特屈儿.md "wikilink")。

目前HMTD大多用作[自杀式袭击炸弹中](../Page/自杀攻击.md "wikilink")，恐怖分子在[伦敦七七爆炸案](../Page/伦敦七七爆炸案.md "wikilink")\[3\]与[2006年跨大西洋航机恐怖袭击阴谋](../Page/2006年跨大西洋航机恐怖袭击阴谋.md "wikilink")\[4\]时使用的炸药中也可能含有HMTD。

## 参考资料

<div class="references-small">

<references/>

</div>

[Category:有机过氧化物](../Category/有机过氧化物.md "wikilink")
[Category:炸藥](../Category/炸藥.md "wikilink")

1.  Legler, L. *Ber*. **1885**, *18*, 3343-3351.
2.  Taylor, C. A.; Rinkenbach, W. H. *Army Ordnance* **1924**. *5*,
    463-466
3.  ["London bombers used everyday
    materials"](http://www.alertnet.org/thenews/newsdesk/L04442279.htm)
    [Reuters](../Page/Reuters.md "wikilink"), August 4, 2005, retrieved
    April 16, 2006
4.