{{ Galaxy | | image =
[local.group.arp.600pix.jpg](https://zh.wikipedia.org/wiki/File:local.group.arp.600pix.jpg "fig:local.group.arp.600pix.jpg")
|caption= | name = 六分儀座 A | epoch = [J2000](../Page/J2000.md "wikilink")
| type = IBm\[1\] | ra = \[2\] | dec = \[3\] | dist_ly = 4310000 ±
130000 [ly](../Page/光年.md "wikilink")
(1320000 ± 40000[pc](../Page/秒差距.md "wikilink"))\[4\]\[5\]\[6\] | z =
324 ± 2 [km](../Page/公里.md "wikilink")/s\[7\] | appmag_v = 11.9\[8\] |
size_v = 5′.9 × 4′.9\[9\] | constellation name =
[六分儀座](../Page/六分儀座.md "wikilink") | notes = Square in
shape | names = UGCA 205,\[10\]
[DDO](../Page/David_Dunlap_Observatory_Catalogue.md "wikilink")
75,\[11\] [PGC](../Page/Principal_Galaxies_Catalogue.md "wikilink")
29653\[12\] }} **六分儀座
A**是一個在[六分儀座的](../Page/六分儀座.md "wikilink")[不規則矮星系](../Page/不規則矮星系.md "wikilink")，屬於[本星系團的成員之一](../Page/本星系團.md "wikilink")，距離地球有四百萬[光年之遠](../Page/光年.md "wikilink")，而且有5000光年的直徑。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:矮星系](../Category/矮星系.md "wikilink")
[Category:不规则星系](../Category/不规则星系.md "wikilink")
[Category:低表面亮度星系](../Category/低表面亮度星系.md "wikilink")
[Category:六分儀座](../Category/六分儀座.md "wikilink")

1.

2.
3.
4.

5.

6.

7.
8.
9.
10.
11.
12.