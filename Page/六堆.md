**六堆**（客語[南四縣腔](../Page/南四縣腔.md "wikilink")：liugˋ
duiˊ）始自[臺灣](../Page/臺灣.md "wikilink")[朱一貴事件發生後](../Page/朱一貴事件.md "wikilink")，原籍[潮州府之](../Page/潮州府.md "wikilink")[鎮平縣](../Page/蕉嶺縣.md "wikilink")（今[蕉嶺](../Page/蕉嶺.md "wikilink")）、[程鄉縣](../Page/程鄉縣.md "wikilink")（今[梅縣](../Page/梅縣.md "wikilink")）、[平遠縣](../Page/平遠縣.md "wikilink")、[大埔縣](../Page/大埔縣.md "wikilink")，和[汀州府之](../Page/汀州府.md "wikilink")[永定縣](../Page/永定縣.md "wikilink")、[武平縣](../Page/武平縣.md "wikilink")、[上杭縣各縣的客家墾民](../Page/上杭縣.md "wikilink")，在下淡水溪（今[高屏溪](../Page/高屏溪.md "wikilink")）流域以東聯合[客家十三大庄與六十四小庄組成之自衛組織](../Page/客家.md "wikilink")。\[1\]

## 形成背景

[康熙二十五](../Page/康熙.md "wikilink")、六年間，廣東[惠州](../Page/惠州府.md "wikilink")、[潮州二府的客籍墾民渡臺墾殖](../Page/潮州府.md "wikilink")，\[2\]初在臺南東門城外謀生，後至羅漢門南界到林仔邊溪口，沿下淡水、東港兩溪拓墾。至康熙六十年，[朱一貴事變之時](../Page/朱一貴.md "wikilink")，客籍墾民在下淡水溪一地已至少建立了十三大庄與六十四小庄的墾殖規模。
其時在下淡水溪流域一地墾殖的墾民包括了閩、粵二籍。其中閩籍墾民多來自[漳州](../Page/漳州府.md "wikilink")、[泉州二府](../Page/泉州府.md "wikilink")，也包括了少數來自[汀州的客籍墾民](../Page/汀州府.md "wikilink")。粵籍墾民則來自潮州府和惠州府。其中來自潮州府的潮陽、海陽、揭陽三縣的墾民，由於其先祖原來皆是自閩入墾粵東，所以通行[閩南語的](../Page/閩南語.md "wikilink")[潮汕片](../Page/潮州話.md "wikilink")。而來自潮州府的程鄉、平遠、鎮平三縣和惠州府的興寧、長樂二縣的墾民，則為使用[客語的客家墾民](../Page/客語.md "wikilink")。\[3\]因此在清朝[康熙年間臺灣的拓墾歷史上](../Page/康熙.md "wikilink")，來自粵省潮州府的墾民同時包含了使用二種不同方言的人群。\[4\]
同樣地，「粵民」一詞所指也一樣包含了來自潮州通行潮汕片區的墾民以及來自惠州和潮州北部使用客語的墾民。不論來自閩或粵，墾民之間在生活互動上，是以福、客相聚，而不以閩、粵相分。\[5\]同為「粵民」，使用潮汕片的潮民和來自漳、泉的墾民「聲氣相通」，而客家墾民彼此之間又「自為守望」。\[6\]
這種以語言上的差異所形成的認同直接影響了其後六堆民間團練的成員組成。\[7\]

## 參與戰事

### 朱一貴事件

[康熙](../Page/康熙.md "wikilink")60年（1721年），[廣東](../Page/廣東.md "wikilink")[潮州府](../Page/潮州府.md "wikilink")[海陽縣人](../Page/海陽縣.md "wikilink")[杜君英](../Page/杜君英.md "wikilink")，於三月十日豎「清天奪國」旗號，起事於內山。四月十九日，[福建](../Page/福建.md "wikilink")[漳州府](../Page/漳州府.md "wikilink")[長泰縣人](../Page/長泰縣.md "wikilink")[朱一貴](../Page/朱一貴.md "wikilink")，亦舉「大元帥朱」旗於岡山。朱一貴、杜君英在五月一日合力攻下臺灣府城後，朱一貴尊[明為正朔](../Page/明.md "wikilink")，受擁立為「中興王」，國號大明。朱一貴稱王後，朱、杜二人的權力鬥爭轉趨劇烈，再加上杜部軍紀廢弛，導致朱、杜二集團發生內鬨。\[8\]杜君英起事之初，其部眾即包括了粵、閩和臺灣府籍人士，\[9\]其後新園、小琉球陸續有人加入杜君英，此時杜部仍以閩籍之漳、泉和粵籍潮之三陽人士為主。\[10\]
\[11\]待至杜君英和其所屬的粵籍將領以權力分配不公而和朱一貴集團起爭執，\[12\]且朱一貴集團又以整飭紀律為由密謀剿滅杜君英時，雙方遂發生對立。內鬨中，杜君英集團發生閩、粵分裂，原杜君英集團的閩籍將領倒戈投向原即以閩籍為主的朱一貴，和朱一貴集團合攻杜君英。\[13\]\[14\]\[15\]朱、杜起事集團遂分裂為漳、泉和潮之三陽二股勢力，形成閩、粵對立。其後，杜君英戰敗，並率殘部敗走貓兒干（今[雲林縣](../Page/雲林縣.md "wikilink")[崙背鄉](../Page/崙背鄉.md "wikilink")）。
康熙六十年五月，朱一貴、杜君英相爭之際，發生於府城的「閩、粵相爭」引起了下淡水溪不願附眾起事，\[16\]但卻又同樣來自粵屬潮州的客籍墾民的危機感。六堆地區的客籍墾民遂於五月聯合當地十三大庄、六十四小莊一萬餘人，涵蓋來自粵省潮州所屬的鎮平、程鄉、平遠、大埔，和閩省汀州所屬之永定、武平、上杭各縣客籍墾民，\[17\]在[屏東縣萬丹社](../Page/屏東縣.md "wikilink")\[18\]上帝廟（今[萬丹萬泉寺](../Page/萬丹萬泉寺.md "wikilink")\[19\]）集會，分設前、後、左、右、中、巡查營以及先鋒營等七[營衛鄉自保](../Page/部隊.md "wikilink")。\[20\]。並在五月中至六月中之間，以武力清除下淡水溪東岸漳州、泉州籍墾民的勢力，\[21\]嚴防朱一貴勢力的進犯。
朱一貴在擊敗杜君英部後，派二萬餘兵將南下征伐屏東平原的客庄。\[22\]此時，朱一貴北上追討杜君英的本質是閩、粵對立，而南下用兵卻變質為福、客相伐。\[23\]\[24\]\[25\]康熙六十年六月十八、十九日，二軍大戰於下淡水溪，最後以七營大敗朱一貴部隊收場。事件結束後客家人為了保鄉衛土，取消巡查營，將「堆」（意思通「隊」）取代「營」，以和官軍有別，六堆分成前堆、後堆、中堆、左堆、右堆、先鋒堆，各堆設置一名[總理及](../Page/總理.md "wikilink")[監軍](../Page/監軍.md "wikilink")，六堆共同推舉正副「大總理」各一名[統領軍務](../Page/統領.md "wikilink")，由於下淡水溪一帶的客家人生活環境夾在[福佬人](../Page/福佬人.md "wikilink")、[平埔族](../Page/平埔族.md "wikilink")、[高山族聚落之間](../Page/高山族.md "wikilink")，人數居劣勢的客家人遂以常態性的[團練組織維持其族群生存及地方治安](../Page/團練.md "wikilink")。

### 吳福生事件

[雍正十年](../Page/雍正.md "wikilink")，大甲西社平埔族因勞役繁重，聯合臺中、苗栗一帶原住民10社2000餘人起事。此時，[福建](../Page/福建.md "wikilink")[漳州人](../Page/漳州.md "wikilink")[吳福生趁清兵北上之際](../Page/吳福生.md "wikilink")，以「大明得勝」為旗號，舉事於鳳山。吳福生起事後，六堆推舉[侯心富為大總理](../Page/侯心富.md "wikilink")，召集港東、西二里萬餘人，分駐防守上、下淡水、龍肚嶇、冷水坑、搭樓社、篤佳、武洛、羅漢門、巴六焦、阿猴社、三叉河、烏樹港、力力社、新園汛等處，防範吳福生部眾攻掠。侯心富甚至親帶九百餘人渡過下淡水溪大敗吳福生部眾。\[26\]

### 林爽文事件

[乾隆五十一年](../Page/乾隆.md "wikilink")，因[臺灣知府](../Page/臺灣知府.md "wikilink")[孫景燧取締天地會](../Page/孫景燧.md "wikilink")，[彰化大里杙](../Page/彰化.md "wikilink")（今[臺中市](../Page/臺中市.md "wikilink")[大里區](../Page/大里區.md "wikilink")）人[林爽文樹](../Page/林爽文.md "wikilink")「盟主大元帥」大纛，起事於[彰化](../Page/彰化市.md "wikilink")。爽文原籍[福建](../Page/福建.md "wikilink")[漳州府](../Page/漳州府.md "wikilink")[平和縣](../Page/平和縣.md "wikilink")，此時[天地會領袖](../Page/天地會.md "wikilink")，[鳳山縣的](../Page/鳳山縣_\(臺灣\).md "wikilink")[漳州人](../Page/漳州.md "wikilink")[莊大田亦集眾起兵響應](../Page/莊大田.md "wikilink")，並攻陷鳳山，並和林爽文合攻臺灣府城。此時，[屏東平原因朱一貴事件以來的閩](../Page/屏東平原.md "wikilink")、客對立之勢已成，莊大田攻陷鳳山已引起六堆客籍墾民的不安，再加上承平之時對閩籍移民勒索金錢的積怨，\[27\]六堆人民遂齊集忠義亭，推舉人曾中立為總理，選壯丁八千餘人，分為中、左、右，前、後及前敵六堆，對抗莊大田部，保衛六堆地區。其後六堆並選一千三百餘人，協助清軍平定林爽文。\[28\]然而由於閩、客結怨已久，六堆在捍衛鄉土和協助平亂之際，對地方閩庄也有藉機報復之事，閩人被粵人擒殺極多。\[29\]

## 解散

1895年因參加反抗[日本的](../Page/日本.md "wikilink")[乙未戰爭而壯烈犧牲](../Page/乙未戰爭.md "wikilink")，自衛組織被解散，「六堆」一詞已逐漸轉變為該地區客家族群之概念性統稱。

## 六堆範圍

[Liuktoi_Hakka.svg](https://zh.wikipedia.org/wiki/File:Liuktoi_Hakka.svg "fig:Liuktoi_Hakka.svg")

六堆分別指：

  - [右堆](../Page/右堆_\(六堆\).md "wikilink")：[高雄市](../Page/高雄市.md "wikilink")[美濃區](../Page/美濃區.md "wikilink")、[六龜區](../Page/六龜區.md "wikilink")、[杉林區全境或大部分](../Page/杉林區.md "wikilink")，[甲仙區少部分](../Page/甲仙區.md "wikilink")、[旗山區手巾寮](../Page/旗山區.md "wikilink")(今旗山區廣福里)、少部分[屏東縣](../Page/屏東縣.md "wikilink")[高樹鄉](../Page/高樹鄉.md "wikilink")、[里港鄉](../Page/里港鄉.md "wikilink")[武洛](../Page/武洛.md "wikilink")(今里港鄉茄苳村)
      - 右堆附堆有楠仔仙(今六龜區)、莿桐坑(今杉林區)、莿仔寮(今杉林區)、月眉(今杉林區)、叛產厝(今杉林區)、崁頂、新庄、大埔(今高樹鄉大埔村)、九塊厝(今九如鄉)、大路關(今高樹鄉廣福村)、上武洛(今里港鄉)、下武洛(今里港鄉)、鹽樹(今高樹鄉鹽樹村)等十三-{庄}-
  - [左堆](../Page/左堆_\(六堆\).md "wikilink")：[屏東縣](../Page/屏東縣.md "wikilink")[新埤鄉](../Page/新埤鄉.md "wikilink")、[佳冬鄉](../Page/佳冬鄉.md "wikilink")
  - [前堆](../Page/前堆_\(六堆\).md "wikilink")：[屏東縣](../Page/屏東縣.md "wikilink")[長治鄉](../Page/長治鄉.md "wikilink")、[麟洛鄉](../Page/麟洛鄉.md "wikilink")、[屏東市田寮](../Page/屏東市.md "wikilink")、[九如鄉圳寮](../Page/九如鄉.md "wikilink")(今九如鄉玉泉村)
      - 前堆附堆有海豐(今屏東市海豐里)、茄苳仔(今鹽埔鄉洛陽村)兩-{庄}-
  - [後堆](../Page/後堆_\(六堆\).md "wikilink")：西半部[屏東縣](../Page/屏東縣.md "wikilink")[內埔鄉](../Page/內埔鄉.md "wikilink")
  - [中堆](../Page/中堆_\(六堆\).md "wikilink")：[屏東縣](../Page/屏東縣.md "wikilink")[竹田鄉](../Page/竹田鄉.md "wikilink")、[鹽埔鄉七份仔](../Page/鹽埔鄉.md "wikilink")(今鹽埔鄉洛陽村)
  - [先鋒堆](../Page/先鋒堆_\(六堆\).md "wikilink")：[屏東縣](../Page/屏東縣.md "wikilink")[萬巒鄉](../Page/萬巒鄉.md "wikilink")
      - 先鋒堆附堆有潮州、八老爺、力社、佳佐、林后、苦瓜寮、四塊厝等七-{庄}-。\[30\]\[31\]

## 歷屆總理

[內埔鄉豐田村鍾氏祠堂.jpg](https://zh.wikipedia.org/wiki/File:內埔鄉豐田村鍾氏祠堂.jpg "fig:內埔鄉豐田村鍾氏祠堂.jpg")
[內埔鄉豐田村鍾氏祠堂鍾召棠歲進士匾.jpg](https://zh.wikipedia.org/wiki/File:內埔鄉豐田村鍾氏祠堂鍾召棠歲進士匾.jpg "fig:內埔鄉豐田村鍾氏祠堂鍾召棠歲進士匾.jpg")
每遇亂事，六堆便集會推選出大總理、副總理領導應變。歷屆六堆大總理、副總理如下-{表}- \[32\]\[33\]：

|          |
| :------: |
| **六堆領袖** |
|    代     |
|    1     |
|    2     |
|    3     |
|    4     |
|    5     |
|    6     |
|    7     |
|    8     |
|    9     |
|    10    |

## 參見

  - [義民爺](../Page/義民爺.md "wikilink")
  - [義軍](../Page/義軍.md "wikilink")
  - [南四縣腔](../Page/南四縣腔.md "wikilink")
  - [六堆客家文化園區](../Page/六堆客家文化園區.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [屏東縣政府客家事務處](http://www.pthg.gov.tw/planhab/Default.aspx)
  - [六堆客家鄉土文化資訊網](http://liouduai.tacocity.com.tw/)
  - [六堆客家文化園區專題](http://www.ihakka.net/2008ludue/)
  - [六堆客家鄉土誌簡介](http://www.chps.ptc.edu.tw/share/cyber4/chinese/his_pub_intro.htm)
  - [臺灣大百科全書－六堆](http://nrch.culture.tw/twpedia.aspx?id=19472)

[分類:民兵組織](../Page/分類:民兵組織.md "wikilink")

[Category:高雄市歷史](../Category/高雄市歷史.md "wikilink")
[Category:屏東縣歷史](../Category/屏東縣歷史.md "wikilink")
[六堆](../Category/臺灣客家文化重點發展區.md "wikilink")
[Category:已解散軍隊](../Category/已解散軍隊.md "wikilink")
[Category:台湾清治时期军事](../Category/台湾清治时期军事.md "wikilink")
[六堆](../Category/六堆.md "wikilink")

1.  覺羅滿保：〈題義民效力議敘疏〉，《[臺灣文獻叢刊](../Page/臺灣文獻叢刊.md "wikilink")》/一四六
    重修鳳山縣志十二/藝文志/奏疏，「...糾集十三大莊、六十四小莊，合鎮平、程鄉、平遠、永定、武平、大埔、上杭各縣之人...」
2.  見伊能嘉矩，江慶林等譯，「臺灣文化志」。原文中的嘉應州在雍正十一年以前，分屬潮州和惠州管轄，故此處稱潮州和惠州。另鍾任壽編撰的《六堆客家鄉土誌》則認為，六堆的拓墾先民來自康熙二十七年清朝遣臺軍隊中的客籍人士。
3.  宋朝時，鎮平、平遠尚未建縣，其地屬程鄉縣(梅州)轄下的區域，為客語區。到了明洪武二年，程鄉縣被併入通行潮州話的潮州，其地並分別於崇禎七年和嘉靖四十二年，另外再分出鎮平、平遠二縣。
4.  雍正十一年，在廣東總督鄂彌達的建議下，程鄉縣改制為嘉應直隸州，下轄原潮州府的平遠、鎮平二縣和原惠州府的興寧、長樂二縣，全境均屬客語區。自此之後，來自來原潮州府下程鄉、平遠、鎮平三縣的墾民才始稱來自嘉應州。而來自粵省潮州府的墾民，除[大埔](../Page/大埔縣.md "wikilink")、[豐順縣外](../Page/豐順.md "wikilink")，已幾為泛閩南語系的人群。
5.  姚瑩，〈答李信齋論臺灣治事書
    〉，《東槎紀略》（臺北：臺灣銀行經濟研究室，臺灣文獻叢刊第7種），「臺之民不以族分，而以府為氣類；漳人黨漳，泉人黨泉，粵人黨粵，潮雖粵而亦黨漳...」。
6.  覺羅滿保：〈題義民效力議敘疏〉，《重修鳳山縣志》（臺北：臺灣銀行經濟研究室，臺灣文獻叢刊第146
    種），「臺灣鳳山縣屬之南路淡水，歷有漳、泉、汀、潮四府之人，墾田居住。潮屬之潮陽、海陽、揭陽、饒平數縣，與漳、泉之人，語言聲氣相通。而潮屬之鎮平、平遠、程鄉三鄉，則又有汀州之人，自為守望，不與漳、泉之人同夥相雜。」
7.  六堆的成員組成跨越閩、粵二省墾民，而以語言認同來凝聚。
8.  從朱一貴所屬部眾的供詞可知，杜君英部有強娶民女和劫掠村社的事跡。見〈朱一貴謀反殘件〉，《臺案彙錄己集/卷一/三》（臺北：臺灣銀行經濟研究室，臺灣文獻叢刊第191種）。
9.  〈朱一貴謀反殘件〉，《臺案彙錄己集卷一/四》，（臺北：臺灣銀行經濟研究室，臺灣文獻叢刊第191種），「...我依允各自會了我一處民陳伯、莊勳、黃捷、陳會、福建民李國彥、李國恩、陳貴、臺灣府民蕭伯、鄭十三等五十來人...」。
10. 覺羅滿保：〈題義民效力議敘疏〉，《重修鳳山縣志十二/藝文志/奏疏》（臺北：臺灣銀行經濟研究室，臺灣文獻叢刊第146種），「六十年四月二十二日，賊犯杜君英等在南路淡水檳榔林招夥豎旗搶劫新園，北渡淡水溪侵犯南路營，多系潮之三陽及漳、泉人同夥作亂。」
11. 朱一貴事件的原始資料均未見杜君英部隊主以客家墾民為主。只有藍鼎元在〈平臺紀略〉有稱在四月二十一日前，「...君英在下淡水檳榔林招集粵東種地傭工客民...」。但在清一朝的文獻中，客家墾民較多用「客仔/子」稱之，「客民」一詞指「移墾之民」者遠多於指「客家墾民」。如《馬關議和中之伊李問答/第三次問答節略》中「台灣系潮州、漳、泉客民遷往，最為強悍！」、《清初海彊圖稅/附錄臺灣府屬渡口考》中「值耿氏開藩福建，海禁弛；乃招徠客民，漳、泉、惠、潮習水者趨地利，泛海寄居也...」，以及《臺灣私法人事編》等等。
12. 《重修鳳山縣志》/卷十一/雜志/災祥，（臺北：臺灣銀行經濟研究室，臺灣文獻叢刊第146
    種），「粵黨以入府無所獲，且亂自粵莊始，而一貴非粵產，因有異謀」。
13. 如江國論、胡君用均為福建漳州人，分別在下埤頭、林園加入杜君英，朱、杜分裂後均反而追討杜君英。見〈朱一貴謀反殘件〉，《臺案彙錄己集/卷一》（臺北：臺灣銀行經濟研究室，臺灣文獻叢刊第191種）。
14. 《臺灣采訪冊/紀事》（臺北：臺灣銀行經濟研究室，臺灣文獻叢刊第55種），「...朱一貴之亂，有偽封國公杜君英者，粵之潮洲人也。其旗賊眾最雄，閩之賊俱忿恨之。於是，合眾攻君英。諺有云：十八國公滅杜是也...」
15. 林正慧，〈閩客？福客？清代臺灣和人族群新探-以屏東平原為起點〉，《國史館學術集刊第六期》，第45頁。
16. 覺羅滿保：〈題義民效力議敘疏〉，《重修鳳山縣志十二/藝文志/奏疏》（臺北：臺灣銀行經濟研究室，臺灣文獻叢刊第146種），「六十年四月二十二日，賊犯杜君英等在南路淡水檳榔林招夥豎旗搶劫新園，北渡淡水溪侵犯南路營...
    鎮平、程鄉、平遠三縣之民，並無入夥。」此處的鎮平、程鄉、平遠三縣即屬康熙年間潮州府管轄的客語區。
17. 永定、武平、上杭屬福建省汀州府管轄，為閩省的客語區。
18. 《重修鳳山縣志》/卷十二上 藝文志(上)/奏疏 題義民效力議效疏中稱萬丹社。
19.
20. 《重修鳳山縣志》/卷十二上 藝文志(上)/奏疏 題義民效力議效疏　義民內有李直三、侯觀德...
    等謀密起義，誓不從賊；糾集十三大莊、六十四小莊...
    共一萬二千餘名於萬丹社... 遂分設七營，排列淡水河岸，連營固守．每營設立統領二人：先鋒營... 中營... 左營... 右營...
    前營... 後營... 巡查營...
21. 《重修臺灣縣志》/卷十五/雜記，（臺北：臺灣銀行經濟研究室，臺灣文獻叢刊第146種），「自五月中賊黨既分閩、粵、屢相併殺。閩恆散處，粵悉萃居，勢常不敵。南路賴君奏等所糾大莊十三、小莊六十四，並稱客莊，肆毒閩人。而永定、武平、上杭各縣之人，復與粵合，諸泉、漳人多舉家被殺被辱者。」
22. 也有懷疑南下的朱一貴部隊是因清軍登陸而欲逃歸下淡水的閩籍部隊，而非前往征伐的。李文良的《清代南臺灣「客家」的移墾與社會，1680\~1790》即持此觀點。但覺羅滿保的〈題義民效力議敘疏〉、藍鼎元的〈平臺紀略〉和朱一貴謀反殘件中王拔的供詞，都稱是征討，而非回歸。
23. 在朱一貴部眾的供詞中，北上追擊杜君英的皆稱追「杜君英」，而不稱追「客仔」。而王拔在供詞中，則直稱王忠是去南淡水征「客仔」，顯見南、北的征伐性質不同。
24. 李文良的《清代南臺灣「客家」的移墾與社會，1680\~1790》認為往征六堆的朱一貴部隊為起於下淡水的閩籍部眾。而林正慧的〈閩客？福客？清代臺灣和人族群新探-以屏東平原為起點〉也認為朱、杜分裂後，朱一貴集團由閩籍福佬所組成，北逃的杜君英則為粵籍福佬語系部隊（即覺羅滿保所稱的「潮之三陽」），而六堆地區則係客籍集團。
25. 藍鼎元在〈諭閩粵民人〉一文中，也已改採福、客分類，而非閩、粵，稱：「...漳、泉海豐、三陽之人經過客莊，客民經過漳、泉村落，宜各釋前怨，共敦新好...」。見藍鼎元，〈諭閩粵民人〉，《重修臺灣府志/卷二十一/藝文(二)/文移》，（臺北：臺灣銀行經濟研究室，臺灣文獻叢刊第105種）。
26. 《重修鳳山縣志/卷十/人物志》（臺北：臺灣銀行經濟研究室，臺灣文獻叢刊第146種）。
27. 《臺灣采訪冊/紀事》（臺北：臺灣銀行經濟研究室，臺灣文獻叢刊第55種），「其禍自朱逆叛亂以至於今，仇日以結，怨日以深，治時閩欺粵，亂時粵侮閩，率以為常，冤冤相報無已時。」。
28. 《欽定平定臺灣紀略/卷三十六/九月初七日至十二日》（臺北：臺灣銀行經濟研究室，臺灣文獻叢刊第102種）。
29. 《臺灣采訪冊/紀事》（臺北：臺灣銀行經濟研究室，臺灣文獻叢刊第55種），「嗣後地方安靖，閩每欺粵，凡渡船、旅舍、中途多方搜索錢文。粵人積恨難忘，逢叛亂，粵合鄰莊聚類蓄糧，聞警即藉義出莊擾亂閩之街市村莊...
    林爽文反，南路粵人蹂躪莊市尤甚。賊首莊大田、莊錫舍等，合眾力攻粵莊不得入，閩人被粵人擒殺極多。」
30. [六堆客家鄉土文化資訊網](http://liouduai.tacocity.com.tw/)
31. [屏東縣政府－認識屏東－客家人文](http://www.pthg.gov.tw/tw/CP.aspx?s=1018&cp=1&n=10881)
32. 鍾任壽，《六堆客家鄉土誌》，頁178－184
33. 《六堆客家社會文化發展與變遷之研究》第四篇 政事篇，財團法人六堆文化教育基金會，頁8－9