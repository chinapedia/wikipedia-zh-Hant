**六氟化鈾**（）是一种[铀的](../Page/铀.md "wikilink")[化合物](../Page/化合物.md "wikilink")，其[化学式为](../Page/化学式.md "wikilink")。六氟化铀被用于制取[浓缩铀](../Page/浓缩铀.md "wikilink")，因此在[核工业中有很重要的价值](../Page/核工业.md "wikilink")。[标准状况下](../Page/标准状况.md "wikilink")，六氟化铀为灰色的[晶体](../Page/晶体.md "wikilink")。六氟化铀有很强的[毒性](../Page/毒性.md "wikilink")，可与[水剧烈反应](../Page/水.md "wikilink")，并且能腐蚀大多数[金属](../Page/金属.md "wikilink")。它與[鋁反應溫和](../Page/鋁.md "wikilink")，在鋁的表面形成致密的[氟化铝薄膜](../Page/氟化铝.md "wikilink")，阻止反應進一步進行。

## 制取

磨碎鈾礦（[U<sub>3</sub>O<sub>8</sub>](../Page/八氧化三铀.md "wikilink")，或稱“[黃餅](../Page/黃餅.md "wikilink")”），并将其溶于[硝酸](../Page/硝酸.md "wikilink")，產生[硝酸鈾酰UO](../Page/硝酸鈾酰.md "wikilink")<sub>2</sub>(NO<sub>3</sub>)<sub>2</sub>溶液。[萃取纯化后](../Page/萃取.md "wikilink")，使硝酸铀酰与[氨反应](../Page/氨.md "wikilink")，生成[重铀酸铵](../Page/重铀酸铵.md "wikilink")(NH<sub>4</sub>)<sub>2</sub>U<sub>2</sub>O<sub>7</sub>。加[氢](../Page/氢.md "wikilink")[还原得到](../Page/还原.md "wikilink")[二氧化铀](../Page/二氧化铀.md "wikilink")，再与[氢氟酸反应生成](../Page/氢氟酸.md "wikilink")[四氟化铀](../Page/四氟化铀.md "wikilink")。四氟化铀與[氟进行氧化反應產生UF](../Page/氟.md "wikilink")<sub>6</sub>。

## 核燃料循環中的應用

各种主要的铀浓缩方法都需要用到六氟化铀，不论是[气体扩散法](../Page/气体扩散法.md "wikilink")，还是[气体离心法](../Page/气体离心法.md "wikilink")。这是由于六氟化铀的[三相点为](../Page/三相点.md "wikilink")
147 °F （64 °C，337
K），且[气压略高于](../Page/气压.md "wikilink")[大气压](../Page/大气压.md "wikilink")。[氟在自然界只存在一种稳定](../Page/氟.md "wikilink")[核素也是一个原因](../Page/核素.md "wikilink")，因此各种六氟化铀分子的[相对分子质量差异可以完全归咎于各种铀核素](../Page/相对分子质量.md "wikilink")[相对原子质量的差异](../Page/相对原子质量.md "wikilink")。\[1\]
[Uranium_hexafluoride_crystals_sealed_in_an_ampoule.jpg](https://zh.wikipedia.org/wiki/File:Uranium_hexafluoride_crystals_sealed_in_an_ampoule.jpg "fig:Uranium_hexafluoride_crystals_sealed_in_an_ampoule.jpg")
其他所有的氟化鈾將產生固態的[配位聚合物](../Page/配位聚合物.md "wikilink")。

气相扩散法所需用的[能量大约是气体离心法的](../Page/能量.md "wikilink")60倍。尽管如此，其中仅仅有4%的能量被用于制取浓缩铀。

除此之外，[捷克发展了一种循环使用核燃料的方法](../Page/捷克.md "wikilink")：用旧的[氧化物](../Page/氧化物.md "wikilink")[核燃料与氟气反应生成各种](../Page/核燃料.md "wikilink")[氟化物的混合物](../Page/氟化物.md "wikilink")，再用[分馏分离各种组分](../Page/分馏.md "wikilink")。

## 储存

目前大约95%的[贫化铀都以六氟化铀的形式被置于气体](../Page/贫化铀.md "wikilink")[钢瓶中](../Page/钢瓶.md "wikilink")，储存在核工厂的附近。每个钢瓶大约含有12.7吨的固体六氟化铀。仅在美国，1993年至2005年期间，就有686,500吨的六氟化铀（57,122瓶）作为贫化铀的储存形式被生产出来。它们被储存在[俄亥俄州](../Page/俄亥俄州.md "wikilink")[樸茨茅斯](../Page/朴次茅斯_\(俄亥俄州\).md "wikilink")，[田納西州](../Page/田納西州.md "wikilink")[橡樹嶺和](../Page/橡树岭_\(田纳西州\).md "wikilink")[肯塔基州](../Page/肯塔基州.md "wikilink")[帕迪尤卡](../Page/帕迪尤卡_\(肯塔基州\).md "wikilink")。\[2\]\[3\]六氟化铀不很稳定，与潮湿空气接触时即会反应生成极易溶于水并且毒性很大的
[UO<sub>2</sub>F<sub>2</sub>](../Page/铀酰氟.md "wikilink")
和[氟化氢](../Page/氟化氢.md "wikilink")，因此长期储存六氟化铀会对环境及人的健康造成损害，须定期检查是否有裂缝。\[4\]

[DUF6_cylinder_leak.gif](https://zh.wikipedia.org/wiki/File:DUF6_cylinder_leak.gif "fig:DUF6_cylinder_leak.gif")

[美国已经发生过数起涉及六氟化铀的事故](../Page/美国.md "wikilink")。\[5\]\[6\]美国政府将
UF<sub>6</sub>
转化为固态的铀[氧化物回收](../Page/氧化物.md "wikilink")。\[7\]但这种回收总共要花费大约1500万到4.5亿[美元](../Page/美元.md "wikilink")。\[8\]

## 化學性質

固態時的結構由 J.H. Levy、J.C Taylor 和 A.B
Waugh所描述。\[9\]在該論文中，他們利用[中子衍射測定UF](../Page/中子衍射.md "wikilink")<sub>6</sub>、
MoF<sub>6</sub> 和 WF<sub>6</sub> 在77K下的結構。

<center>

[File:Uranium-hexafluoride-crystal-3D-vdW.png|六氟化铀的](File:Uranium-hexafluoride-crystal-3D-vdW.png%7C六氟化铀的)[晶体结构](../Page/晶体结构.md "wikilink")

</center>

六氟化铀可作为[氧化剂或](../Page/氧化剂.md "wikilink")[路易斯酸](../Page/路易斯酸.md "wikilink")，易于[氟离子结合](../Page/氟离子.md "wikilink")。如[氟化铜与六氟化铀在](../Page/氟化铜.md "wikilink")[乙腈中反应会生成](../Page/乙腈.md "wikilink")
Cu\[UF<sub>7</sub>\]<sub>2</sub>.5MeCN。\[10\]

含有有机[阳离子的铀](../Page/阳离子.md "wikilink")（VI）[聚合物已经制得并用](../Page/聚合物.md "wikilink")[X光衍射分析](../Page/X光衍射.md "wikilink")。\[11\]

## 其他铀氟化物

C.J. Howard，J.C. Taylor 和 A.B. Waugh
提出了[五氟化铀](../Page/五氟化铀.md "wikilink")（UF<sub>5</sub>）和[九氟化二铀](../Page/九氟化二铀.md "wikilink")（U<sub>2</sub>F<sub>9</sub>）的结构。\[12\][三氟化铀的结构则由](../Page/三氟化铀.md "wikilink")
J. Laveissiere 阐明。\[13\]

J.H. Levy，J.C. Taylor 和 P.W. Wilson
提出了[四氟氧铀的结构](../Page/四氟氧铀.md "wikilink")。\[14\]

## 参见

  - [贫化铀](../Page/贫化铀.md "wikilink")
  - [铀](../Page/铀.md "wikilink")

## 参考资料

## 延伸阅读

  - x (xstal structure)

  - x (selective oxidant of CFCs)

[Category:六价铀化合物](../Category/六价铀化合物.md "wikilink")
[Category:核能技術](../Category/核能技術.md "wikilink")
[Category:六氟化物](../Category/六氟化物.md "wikilink")
[Category:卤化铀](../Category/卤化铀.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.  x
11.  x
12.  x
13.
14.