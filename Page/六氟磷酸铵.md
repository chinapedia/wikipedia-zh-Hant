**六氟磷酸銨**又稱**氟磷酸銨**，[分子式NH](../Page/分子式.md "wikilink")<sub>4</sub>\[PF<sub>6</sub>\]，是一種白色的[無機化合物](../Page/無機化合物.md "wikilink")。它溶於[水](../Page/水.md "wikilink")、[甲醇](../Page/甲醇.md "wikilink")、[乙醇和](../Page/乙醇.md "wikilink")[丙酮](../Page/丙酮.md "wikilink")，加熱會分解，由[氟化铵与](../Page/氟化铵.md "wikilink")[五氯化磷反应生成六氟磷酸銨](../Page/五氯化磷.md "wikilink")。

## 參見

  -
  - [四丁基六氟磷酸铵](../Page/四丁基六氟磷酸铵.md "wikilink")

  - [氟硅酸铵](../Page/氟硅酸铵.md "wikilink")

## 參考文獻

  -
[Category:銨鹽](../Category/銨鹽.md "wikilink")
[Category:六氟磷酸盐](../Category/六氟磷酸盐.md "wikilink")
[Category:非金属卤化物](../Category/非金属卤化物.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")