[Argon_dimer_potential_and_Lennard-Jones.png](https://zh.wikipedia.org/wiki/File:Argon_dimer_potential_and_Lennard-Jones.png "fig:Argon_dimer_potential_and_Lennard-Jones.png")原子之间作用的兰纳-琼斯势（藍色），与红色的经验公式值相差很小。\]\]

**兰纳-琼斯势**（Lennard-Jones potential），又称**L-J势**, **6-12势**,
或**12-6势**，是用来模拟两个电中性的[分子或](../Page/分子.md "wikilink")[原子间相互作用](../Page/原子.md "wikilink")[势能的一个比较简单的数学模型](../Page/势能.md "wikilink")。最早由数学家于1924年提出。由于其解析形式简单而被广泛使用，特别是用来描述[惰性气体分子间相互作用尤为精确](../Page/稀有氣體.md "wikilink")。

兰纳-琼斯势能以两体距离为唯一变量，包含两个参数。其形式为：

\[V(r) = 4\epsilon \left[ \left(\frac{\sigma}{r}\right)^{12} - \left(\frac{\sigma}{r}\right)^{6} \right]\]

ε等于势能阱的深度，σ是互相作用的势能正好为零时的两体距离。在实际应用中，ε、σ参数往往通过拟合已知实验数据或精确量子计算结果而确定。
另一种写法是：

\[V(r) = \epsilon \left[ \left(\frac{r_{min}}{r}\right)^{12} - 2\left(\frac{r_{min}}{r}\right)^{6} \right]\]

\(r_{min} = 2^{\frac{1}{6}}\sigma\)是在势能阱时底两体间距离。

从物理意义上讲，第一项\(\left(\frac{1}{r}\right)^{12}\)可认为是对应于两体在近距离时以互相排斥为主的作用，第二项\(\left(\frac{1}{r}\right)^{6}\)对应两体在远距离以互相吸引（例如通过[范德瓦耳斯力](../Page/范德瓦耳斯力.md "wikilink")）为主的作用，而此六次方項也的確可以使用以電子-原子核的電偶極矩[微擾展開得到](../Page/微擾理論.md "wikilink")。但讀者尤須記住，兰纳-琼斯势本身只是一个近似公式。

兰纳-琼斯势相应的两体作用[力为](../Page/力.md "wikilink")：

\[\mathbf{F}(r) = - \nabla V(r) = - \frac{d}{dr} V(r) \hat{\mathbf{r}} = 4 \epsilon \left( 12\,{\frac {{\sigma}^{12}}{{r}^{13}}}-6\,{\frac{{\sigma}^{6}}{{r}^{7}}} \right) \hat{\mathbf{r}}\]

## 其他的表示法

以下是幾種常見的李納瓊斯勢的表示法

### AB 表示法

此表示法易於模擬軟體的使用

\[V_\text{LJ}(r) = \frac{A}{r^{12}} - \frac{B}{r^6},\]
其中\(A = 4\varepsilon \sigma^{12}\)，\(B = 4\varepsilon \sigma^6\)。或者\(\sigma = \sqrt[6]{\frac{A}{B}}\)，\(\varepsilon = \frac{B^2}{4A}\)。在此表示法中李納瓊斯勢被寫為12-6勢。\[1\]

數學上更廣義的形式，包含了一個額外的變數*n*

\[V_\text{LJ}(r) = \varepsilon \left(\left(\frac{r_0}{r}\right)^{2n} - 2\left(\frac{r_0}{r}\right)^n\right),\]
其中\(\varepsilon\)是分子間的鍵能（分離原子所需的能量）。

## 参考文献

  - Lennard-Jones, J. E. Cohesion. Proceedings of the Physical Society
    1931, 43, 461-482.

## 参见

  - [莫尔斯势](../Page/莫尔斯势.md "wikilink")

{{-}}

[Category:计算化学](../Category/计算化学.md "wikilink")
[Category:分子间作用力](../Category/分子间作用力.md "wikilink")
[Category:势](../Category/势.md "wikilink")
[Category:化学键](../Category/化学键.md "wikilink")

1.