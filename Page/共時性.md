[CGJung.jpg](https://zh.wikipedia.org/wiki/File:CGJung.jpg "fig:CGJung.jpg")

</center>

\]\]

**共时性**（[英文](../Page/英文.md "wikilink")：**Synchronicity**、又译**同时性**；)），是[瑞士](../Page/瑞士.md "wikilink")[心理学家](../Page/心理学.md "wikilink")[荣格](../Page/荣格.md "wikilink")1920年代提出的理论，指“有意义的巧合”，用于解释[因果律无法解释的现象](../Page/因果律.md "wikilink")，如梦境成真，想到某人某人便出现等。荣格认为，这些表面上无[因果关系的事件之间有着非因果性](../Page/因果关系.md "wikilink")、有意义的联系，这些联系常取决于人的[主观经验](../Page/主观.md "wikilink")。

1952年荣格在《[论共时性](../Page/论共时性.md "wikilink")》（On
Synchronicity）一文中详细定义其所要处理的[概念](../Page/概念.md "wikilink")。他认为共时性是一种巧合现象，并不局限于[心理的领域](../Page/心理.md "wikilink")，可以从“心灵母体内部”与“我们外在世界”，甚或同时从这两方面跨越进入[意识状态](../Page/意识.md "wikilink")。当两者同时发生时便称为“共时性”现象。

## 描述

[Schéma_synchronicité_in_English.png](https://zh.wikipedia.org/wiki/File:Schéma_synchronicité_in_English.png "fig:Schéma_synchronicité_in_English.png")
榮格創造了“共時性(synchronicity)”這個詞彙來描述“在時間上同時或巧合發生的非因果事件”。

## 舉例

[莊周夢蝶](../Page/莊周夢蝶.md "wikilink")

  - 以巧合描述共時性並不恰當，而是有條件限制的巧合，例如兩人相遇，在沒有人為刻意為之之下，本身就是巧合。例如，說曹操，劉備到，這是巧合；說曹操，曹操到，如此才是共時性的巧合，一種多方面同時滿足的心想事成，我想曹操，曹操到來，兩方皆須成立。
  - 1805年，[法國詩人埃米勒](../Page/法國.md "wikilink")·德尚被陌生人Monsieur de
    Fontgibu邀請吃乾果布丁。十年後，他在[巴黎某餐廳點乾果布丁吃](../Page/巴黎.md "wikilink")，侍應告訴他最後一個乾果布丁也給了另一個客人。那個客人就是de
    Fontgibu。
  - 1832年，埃米勒·德尚在飯局上吃乾果布丁，跟朋友談起之前的事。一講曹操，曹操就到——此時曹操來了。

## 迷惘

這個概念很難用科學方法證實。

[預知](../Page/預知.md "wikilink")[夢的發生或](../Page/夢.md "wikilink")[夢遊](../Page/夢遊.md "wikilink")[現象及](../Page/現象.md "wikilink")[靈魂](../Page/靈魂.md "wikilink")[記憶](../Page/記憶.md "wikilink")。

## 共時性相關著作

  - Also included in his *Collected Works* volume 8.

  -
  -
## 参考文献

  - Bernard Beitman, MD,"Connecting with Coincidence--Synchronicity in
    Practice"[1](http://coincider.com/the-book/),Health Communications,
    Inc.,Publication Date: March, 2016.

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - Roth, Remo, F., *Return of the World Soul, Wolfgang Pauli, C.G. Jung
    and the Challenge of Psychophysical Reality \[unus mundus\]*. Pari
    Publishing, 2011

## 參見

  - [意外发现](../Page/意外发现.md "wikilink")
  - [集体无意识](../Page/集体无意识.md "wikilink")
  - [量子脑](../Page/量子脑.md "wikilink")
  - [全像原理](../Page/全像原理.md "wikilink")
  - [胡安·马尔达西那](../Page/胡安·马尔达西那.md "wikilink")

## 外部連結

  - [陰平陽秘，精神乃治—《老子》、《易經》與榮格分析心理](http://www.hkshp.org/humanities/ph105-13.htm)

  - [荣格的中国情结](https://web.archive.org/web/20101226011325/http://www.psychinese.com/Study/Html/2009/03/1177.html)

  - [《共時性：自然與心靈合一的宇宙》](http://www.psychspace.com/psych/viewnews-8026)

  - [Igor V. Limar (2011). "Carl G. Jung’s Synchronicity and Quantum
    Entanglement: Schrödinger’s Cat ‘Wanders’ Between Chromosomes".
    *NeuroQuantology Journal*, 09 (2). pp.
    313–321.](https://www.academia.edu/1248055/Carl_G._Jung_s_Synchronicity_and_Quantum_Entanglement_Schrodinger_s_Cat_Wanders_Between_Chromosomes)

  - [Carl Jung and
    Synchronicity](http://www.carl-jung.net/synchronicity.html)

  -
[Category:心理学术语](../Category/心理学术语.md "wikilink")