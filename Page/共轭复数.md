[Complex_conjugate_picture.svg](https://zh.wikipedia.org/wiki/File:Complex_conjugate_picture.svg "fig:Complex_conjugate_picture.svg")
在[數學中](../Page/數學.md "wikilink")，[複數的](../Page/複數_\(數學\).md "wikilink")**共軛複數**（常簡稱**共軛**）是對虛部變號的運算，因此一個複數

\[z = a + bi \quad (a, b \in \mathbb{R})\]

的複共軛是

\[\overline{z} = a - bi\]

舉例明之：

\[\overline{3 - 2i} = 3 + 2i\]

\[\overline{7} = 7\]

在複數的極坐標表法下，複共軛寫成

\[\overline{re^{i \theta}} = re^{-i \theta}\]

這點可以透過歐拉公式驗證

將複數理解為[複平面](../Page/複平面.md "wikilink")，則複共軛無非是對實軸的[反射](../Page/反射_\(数学\).md "wikilink")。複數\(z\)的複共軛有時也表為\(z^*\)。

## 性質

對於複數\(z, w\)：

\[\begin{array}{l}
\overline{z + w} = \overline{z} + \overline{w} \\
\overline{z - w} = \overline{z} - \overline{w} \\
\overline{zw} = \overline{z} \, \overline{w} \\
\overline{\left( \dfrac{z}{w} \right)} = \dfrac{\overline{z}}{\overline{w}} & (w \ne 0) \\
\overline{z} = z & (z \in \mathbb{R}) \\
\overline{z^n} = \overline{z}^n & (n \in \mathbb{Z}) \\
|\overline{z}| = |z| \\
|\overline{z}|^2 = z\overline{z} \\
\overline{(\overline{z})} = z \\
z^{-1} = \dfrac{\overline{z}}{|z|^2} & (z \ne 0)
\end{array}\]

一般而言，如果複平面上的函數\(\phi\)能表為實係數冪級數，則有：

\[\phi(\overline{z}) = \overline{\phi(z)}\]

最直接的例子是多項式，由此可推得實係數多項式之複根必共軛。此外也可用於複[指數函數與複](../Page/指數函數.md "wikilink")[對數函數](../Page/對數函數.md "wikilink")（取定一分支）：

\[\begin{array}{l}
\exp(\overline{z}) = \overline{\exp(z)} \\
\log(\overline{z}) = \overline{\log(z)} & (z \neq 0)
\end{array}\]

## 其它觀點

複共軛是複平面上的[自同構](../Page/自同構.md "wikilink")，但是並非[全純函數](../Page/全純函數.md "wikilink")。

記複共軛為\(\tau\)，則有\(\operatorname{Gal}(\mathbb{C}/\mathbb{R}) = \{ 1, \tau \}\)。在[代數數論中](../Page/代數數論.md "wikilink")，慣於將複共軛設想為「無窮素數」的[弗羅貝尼烏斯映射](../Page/弗羅貝尼烏斯映射.md "wikilink")，有時記為\(F_\infty\)。

[Category:複數](../Category/複數.md "wikilink")