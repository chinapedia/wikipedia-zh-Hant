[Naval_Jack_of_the_Republic_of_China.svg](https://zh.wikipedia.org/wiki/File:Naval_Jack_of_the_Republic_of_China.svg "fig:Naval_Jack_of_the_Republic_of_China.svg")所設計的[青天白日旗](../Page/青天白日旗.md "wikilink")\]\]

**興中會**，為[孙中山在](../Page/孙中山.md "wikilink")[檀香山成立的革命團體](../Page/檀香山.md "wikilink")，為[中國國民黨的前身組織](../Page/中國國民黨.md "wikilink")，中國國民黨即以其成立日1894年11月24日為建黨日。

## 近代历史

### 檀香山

[興中會墳場大門.JPG](https://zh.wikipedia.org/wiki/File:興中會墳場大門.JPG "fig:興中會墳場大門.JPG")的[興中會墳場](../Page/興中會墳場.md "wikilink")\]\]

1894年11月24日，[孙中山于](../Page/孙中山.md "wikilink")[夏威夷](../Page/夏威夷共和国.md "wikilink")[檀香山創立興中會](../Page/檀香山.md "wikilink")。其以振興中華，挽救危局為宗旨。會員入會時須高舉右手對天宣誓，其誓詞為：「驅除韃虜，恢復中華，創立合眾政府。倘有貳心，神明鑑察」。

永和泰商号[經理](../Page/經理.md "wikilink")[劉祥](../Page/劉祥_\(興中會\).md "wikilink")、卑涉銀行（Bank
of Bishop and
Co.,Ltd.）[華人經理](../Page/華人.md "wikilink")[何寬為首任正副主席](../Page/何寬_\(興中會\).md "wikilink")；然不久，劉祥便退出興中會。

1895年2月21日，[孫中山在](../Page/孫中山.md "wikilink")[香港集合](../Page/香港.md "wikilink")[陈少白](../Page/陈少白.md "wikilink")、[杨鹤龄](../Page/杨鹤龄.md "wikilink")、[尢列](../Page/尢列.md "wikilink")、[陸皓東](../Page/陸皓東.md "wikilink")、[郑士良](../Page/郑士良.md "wikilink")、[程奎光等旧友](../Page/程奎光.md "wikilink")，并與[楊衢雲的香港](../Page/楊衢雲.md "wikilink")[輔仁文社合併](../Page/輔仁文社.md "wikilink")，成立興中會總會，檀香山会为支会。[黃詠商獲選為臨時主席](../Page/黃詠商.md "wikilink")\[1\]。誓詞改為「驅逐韃虜，恢復中華，創立合眾政府」。

总会的办事处，以经营贸易的商號“乾亨行”为名，设在[香港](../Page/香港.md "wikilink")[中环](../Page/中环.md "wikilink")[士丹顿街](../Page/士丹顿街.md "wikilink")13号。总会机构与檀香山支会大同小异，设总办、协办、管库、华文文案、洋文文案各一人，设[董事十人](../Page/董事.md "wikilink")。“会底银”仍为每人五[银元](../Page/银元.md "wikilink")，“银会”的股票仍为每股十银元，于开国之日按股发还本利共一百银元。

### 广州起义

[Sun_Yat_Sen_in_Japan_1898.png](https://zh.wikipedia.org/wiki/File:Sun_Yat_Sen_in_Japan_1898.png "fig:Sun_Yat_Sen_in_Japan_1898.png")的合照。前排左起︰安永東之助、楊衢雲、[平山周](../Page/平山周.md "wikilink")、末永節、[內田良平](../Page/內田良平.md "wikilink")；後排左起︰可兒長一、小山雄太郎、[宮崎寅藏](../Page/宮崎寅藏.md "wikilink")、孫中山（秘書）\[2\]\[3\]、清藤幸七郎、大原義剛。\]\]

1895年10月10日（[阴历八月二十二日](../Page/阴历.md "wikilink")）總會決定[阴历九月初九在](../Page/阴历.md "wikilink")[廣州舉行起義](../Page/廣州.md "wikilink")（第一次[廣州起義](../Page/乙未广州起义.md "wikilink")），同時選舉楊衢雲為會長及合眾政府大總統（當時亦稱總辦或伯理璽天德，即President），孫中山為[秘書](../Page/秘書.md "wikilink")。[廣州起義失败後](../Page/廣州起義.md "wikilink")，[陆皓东被](../Page/陆皓东.md "wikilink")[清廷杀害](../Page/清廷.md "wikilink")。楊衢雲、孫中山被迫逃离[香港](../Page/香港.md "wikilink")。

11月上旬，陳少白在[台灣](../Page/台灣.md "wikilink")[台北創立興中會台灣分會](../Page/台北.md "wikilink")，會員有楊心如、吳文秀、趙滿期、容祺年、莊某等五、六人，並逐漸打開局面。\[4\]

### 惠州革命

[台灣總督](../Page/台灣總督.md "wikilink")[兒玉源太郎並曾應允支援興中會發動](../Page/兒玉源太郎.md "wikilink")1900年的惠州革命，孫中山坐鎮[台北](../Page/台北.md "wikilink")，後因[日本內閣改組](../Page/日本內閣.md "wikilink")，政策轉變，新內閣不願意援助中國革命，惠州革命遂告失敗。\[5\]

1900年1月24日，[楊衢雲辭去興中會會長職務](../Page/楊衢雲.md "wikilink")，讓位给孫中山，之後，興中會多次發動起義，均以失敗告終。

### 与華興會、光復會和其他各會合并

1905年7月30日，興中會与[華興會](../Page/華興會.md "wikilink")、[光復會和其他各會合并為](../Page/光復會.md "wikilink")[同盟會](../Page/中国同盟会.md "wikilink")，孫中山成為同盟會總理。孫中山從此掌控黨的最高權力。

## 興中會組織

| 名稱(地點) | 成立時間     | 發起人 | 職員                                                                                   | 會員人數       | 活動                                                                                                                      |
| ------ | -------- | --- | ------------------------------------------------------------------------------------ | ---------- | ----------------------------------------------------------------------------------------------------------------------- |
| 檀香山興中會 | 1894年11月 | 孫中山 | 主席：[劉祥](../Page/劉祥_\(興中會\).md "wikilink")，副主席：[何寬](../Page/何寬_\(興中會\).md "wikilink") | 約130人      | 有多人參加[乙未廣州之役](../Page/乙未廣州起義.md "wikilink")（第一次廣州起義），孫中山離開後則無活動，於1903年重組，並在[希洛設立分會](../Page/希洛_\(夏威夷州\).md "wikilink")。 |
| 香港興中會  | 1895年2月  | 孫中山 |                                                                                      | 姓名可查者約五十幾人 | 策劃廣州革命，設立農學會                                                                                                            |

## 参见

  - [兴中会坟场](../Page/兴中会坟场.md "wikilink")
  - [中華人民共和國民主黨派](../Page/民主党派.md "wikilink")
  - [中国农工民主党](../Page/中国农工民主党.md "wikilink")
  - [革命党](../Page/革命党.md "wikilink")、[中國同盟會](../Page/中國同盟會.md "wikilink")、[國民黨
    (1912–1913)](../Page/國民黨_\(1912–1913\).md "wikilink")、[中華革命黨](../Page/中華革命黨.md "wikilink")、[五四運動](../Page/五四運動.md "wikilink")、[中國國民黨](../Page/中國國民黨.md "wikilink")

## 注释

## 参考文献

## 延伸閱讀

  - 吳倫霓霞：〈[興中會前期(1894-1900)孫中山革命運動與香港的關係](http://www.mh.sinica.edu.tw/MHDocument/PublicationDetail/PublicationDetail_884.pdf)〉。
  - 李金强：〈[香港兴中会总会的成立及其重要性](http://www.nssd.org/articles/article_read.aspx?id=39699761)〉。
  - 黎东方：《细说民国创立》 上海人民出版社

## 外部链接

  -
{{-}}

[興中會](../Category/興中會.md "wikilink")
[Category:中国国民党历史](../Category/中国国民党历史.md "wikilink")

1.  [巨人身影后的烈士杨衢云](http://informationtimes.dayoo.com/html/2011-12/11/content_1554886.htm)

2.  [FOCUS
    香港烈士楊衢雲被遺忘的辛亥革命](http://www.metrohk.com.hk/index.php?cmd=detail&id=162000),
    [都市日報](../Page/都市日報.md "wikilink")
3.  [回首香江醫療百年](http://www.mingpaomonthly.com/cfm/Archive2.cfm?File=201104/inf/01a.txt)
    , [明報月刊](../Page/明報月刊.md "wikilink"), 二零一一年四月號
4.  [在台興中會的成立](http://sun.yatsen.gov.tw/content.php?cid=S01_05_03_01)
5.  [惠州起義與中國革命](http://edu.ocac.gov.tw/culture/chinese/cul_chculture/vod19html/vod19_05.htm)