**兴平**（486年）是[南朝](../Page/南北朝.md "wikilink")[浙江](../Page/浙江省.md "wikilink")[富阳](../Page/富阳.md "wikilink")、[钱塘的农民起义领袖](../Page/钱塘.md "wikilink")[唐寓之所建立的吴国的](../Page/唐寓之.md "wikilink")[年号](../Page/年号.md "wikilink")，共计1年。

## 大事记

## 出生

## 逝世

## 纪年

  -
    {| border=1 cellspacing=0

|-
style="font-weight:bold;background-color:\#CCCCCC;color:\#000000;text-align:right"
|兴平||元年 |- style="background-color:\#FFFFFF;text-align:center"
|[公元](../Page/公元.md "wikilink")||486年 |-
|[干支](../Page/干支.md "wikilink")||[丙寅](../Page/丙寅.md "wikilink")
|}

## 參看

  - [中国年号列表](../Page/中国年号列表.md "wikilink")
      - 其他使用[兴平年號的政權](../Page/兴平.md "wikilink")
  - 同期存在的其他政权年号
      - [永明](../Page/永明.md "wikilink")（483年正月—493年十二月）：[南朝齊](../Page/南齐.md "wikilink")[齊武帝萧赜的年号](../Page/齐武帝.md "wikilink")
      - [太和](../Page/太和_\(北魏\).md "wikilink")（477年正月-499年十二月）：[北魏政权](../Page/北魏.md "wikilink")[魏孝文帝元宏年号](../Page/魏孝文帝.md "wikilink")
      - [太平](../Page/太平_\(柔然\).md "wikilink")（485年-491年）：[柔然政权伏名敦可汗](../Page/柔然.md "wikilink")[豆崙年号](../Page/豆崙.md "wikilink")

## 參考文獻

  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:南朝民变政权年号](../Category/南朝民变政权年号.md "wikilink")
[Category:5世纪年号](../Category/5世纪年号.md "wikilink")
[Category:480年代中国政治](../Category/480年代中国政治.md "wikilink")
[Category:486年](../Category/486年.md "wikilink")