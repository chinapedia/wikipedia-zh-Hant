[Night_view_of_Osaka_bay.jpg](https://zh.wikipedia.org/wiki/File:Night_view_of_Osaka_bay.jpg "fig:Night_view_of_Osaka_bay.jpg")夜眺神戶市的景致，被譽稱為「百萬夜景」（）、以及「」之一\]\]

**兵库县**（）是[日本](../Page/日本.md "wikilink")[近畿地方的一个](../Page/近畿地方.md "wikilink")[县](../Page/县.md "wikilink")，面積8396.39km²，[縣治為](../Page/都道府縣廳所在地.md "wikilink")[神户市](../Page/神户市.md "wikilink")。总人口約550萬，縣內城市大部分集中在[濑户内海沿岸](../Page/濑户内海.md "wikilink")。其轄區在古代曾分屬[播磨](../Page/播磨.md "wikilink")、[但馬等多個](../Page/但馬.md "wikilink")[令制國](../Page/令制國.md "wikilink")，因此是極具多樣性、也是一體性程度較低的縣。

## 地理

兵库县北临[日本海](../Page/日本海.md "wikilink")，南接[濑户内海](../Page/濑户内海.md "wikilink")，中央部位是[森林和](../Page/森林.md "wikilink")[山地](../Page/山地.md "wikilink")。东面与[京都府](../Page/京都府.md "wikilink")、[大阪府](../Page/大阪府.md "wikilink")，西面与[冈山县](../Page/冈山县.md "wikilink")、[鸟取县接壤](../Page/鸟取县.md "wikilink")。

腹地是肥沃而温暖的[播磨平原](../Page/播磨平原.md "wikilink")，县域包括[淡路岛](../Page/淡路岛.md "wikilink")。

## 历史

“兵庫”一詞的起源來自於在[天智天皇的統治時期這裏有士兵武器倉庫](../Page/天智天皇.md "wikilink")。

  - 1867年 - 兵庫港对外开埠
  - 1868年 - 初步开设兵库县（[伊藤博文担任初代知事](../Page/伊藤博文.md "wikilink")）
  - 1874年 - 神户－大阪間开通铁路
  - 1876年 - 并合播磨、但马、丹波、淡路，大致构成现代兵库县，当时人口为約135万人
  - 1945年 - 尼崎、神户、姬路等市被[美军轰炸](../Page/美军.md "wikilink")
  - 1977年 - 神户[地铁开通](../Page/地铁.md "wikilink")
  - 1981年 - 神户港口岛博覧会
  - 1991年 - 芦屋市选出日本全国第一位女市长
  - 1995年1月17日 - [阪神淡路大地震](../Page/阪神淡路大地震.md "wikilink")、6,000多人丧生
  - 1997年 -
    [神戶市](../Page/神戶市.md "wikilink")[酒鬼薔薇聖斗事件](../Page/酒鬼薔薇聖斗事件.md "wikilink")
  - 1998年 - [明石海峡大桥开通](../Page/明石海峡大桥.md "wikilink")
  - 1999年 - 篠山市成立
  - 2004年 - 養父市、丹波市成立
  - 2005年 - 南淡路市、新丰冈市、宍粟市、淡路市、香美町成立
  - 2005年4月25日 - [JR福知山線出軌事故](../Page/JR福知山線出軌事故.md "wikilink")、100多人丧生

## 观光

日本国宝[姬路城是](../Page/姬路城.md "wikilink")[世界文化遗产之一](../Page/世界文化遗产.md "wikilink")。

1998年开通的[明石海峽大桥横跨](../Page/明石海峽大桥.md "wikilink")[明石海峡連接](../Page/明石海峡.md "wikilink")[本州和](../Page/本州.md "wikilink")[淡路岛](../Page/淡路岛.md "wikilink")，全长3911米，是世界最长的[吊桥](../Page/吊桥.md "wikilink")。

除此之外，包括[濑户内海国立公园](../Page/濑户内海国立公园.md "wikilink")、[山阴海岸国立公园](../Page/山阴海岸国立公园.md "wikilink")、[六甲山](../Page/六甲山.md "wikilink")、被譽稱為「百萬夜景」（）的神戶市[掬星台夜眺](../Page/掬星台.md "wikilink")、[孙中山纪念館](../Page/孙中山纪念館.md "wikilink")「[移情阁](../Page/移情阁.md "wikilink")」、[餘部橋](../Page/餘部橋.md "wikilink")、[城崎溫泉](../Page/城崎溫泉.md "wikilink")、[出石](../Page/出石.md "wikilink")、[湯村溫泉](../Page/湯村溫泉.md "wikilink")、[砥峰高原等](../Page/砥峰高原.md "wikilink")，都是兵庫縣境內頗為知名的觀光勝景。

## 日本国立公园・國定公園

  - [山陰海岸國立公園](../Page/山陰海岸國立公園.md "wikilink")
  - [瀨戶內海國立公園](../Page/瀨戶內海國立公園.md "wikilink")
  - [冰之山後山那岐山國定公園](../Page/冰之山後山那岐山國定公園.md "wikilink")

<File:TakenoBeach> Hyogo
prefecture.jpg|[山陰海岸國立公園](../Page/山陰海岸國立公園.md "wikilink")
（[竹野海濱](../Page/竹野海濱.md "wikilink")） <File:Tajima>
mihonoura39st3200.jpg|[山陰海岸國立公園](../Page/山陰海岸國立公園.md "wikilink")
（[但馬御火灣](../Page/但馬御火灣.md "wikilink")）
<File:Hyonosen02s1760.jpg>|[冰之山後山那岐山國定公園](../Page/冰之山後山那岐山國定公園.md "wikilink")（[冰之山](../Page/冰之山.md "wikilink")）
<File:Cercidiphyllum>
japonicum05s3200.jpg|[冰之山後山那岐山國定公園](../Page/冰之山後山那岐山國定公園.md "wikilink")（[靜川平](../Page/靜川平.md "wikilink")）

## 文化

### 日本國寶

  - [姬路城](../Page/姬路城.md "wikilink")
  - [一乘寺](../Page/一乘寺.md "wikilink")（[加西市](../Page/加西市.md "wikilink")）
  - [淨土寺](../Page/淨土寺_\(小野市\).md "wikilink")（[小野市](../Page/小野市.md "wikilink")）
  - [鶴林寺](../Page/鶴林寺_\(加古川市\).md "wikilink")（[加古川市](../Page/加古川市.md "wikilink")）
  - [太山寺](../Page/太山寺.md "wikilink")（[神户市](../Page/神户市.md "wikilink")）
  - [朝光寺](../Page/朝光寺.md "wikilink")（[加東市](../Page/加東市.md "wikilink")）
  - [長楽寺](../Page/長楽寺.md "wikilink")（[香美町](../Page/香美町.md "wikilink")）

<File:Himeji> Castle The Keep Towers.jpg|姬路城 <File:Jodoji> Ono
Hyogo01n3200.jpg|淨土寺 <File:Ichijoji> Kasai13bs4272.jpg|一乘寺
<File:Kakogawa> Kakurinji12n4592.jpg|鶴林寺 <File:Hyogo-mikata-kami-kawai>
chorakuji-daibutsu.jpg|長楽寺

### 博物馆

  - [兵庫縣立美術館](../Page/兵庫縣立美術館.md "wikilink")（神戶市）
  - [神戶市立博物館](../Page/神戶市立博物館.md "wikilink")（神戶市）
  - [神戶海洋博物館](../Page/神戶海洋博物館.md "wikilink")（神戶市）
  - [姬路市立美術館](../Page/姬路市立美術館.md "wikilink")（姬路市）
  - [蘆屋市立美術博物館](../Page/蘆屋市立美術博物館.md "wikilink")（蘆屋市）
  - [朝來藝術之森](../Page/朝來藝術之森.md "wikilink")（朝來市）

<File:Hyogo> prefectural museum of art08s3200.jpg|兵庫縣立美術館 <File:Kobe>
city museum01 1920.jpg|神戶市立博物館 <File:Kobe> maritime museum2.jpg|神戶海洋博物館
<File:Himeji> City Museum of Art20bs4480.jpg|姬路市立美術館 <File:Asago> Art
Village08n4272.jpg|朝來藝術之森

## 主要城市

  - [神户市](../Page/神户市.md "wikilink")
      - [東灘区](../Page/東灘区.md "wikilink")、[灘区](../Page/灘区.md "wikilink")、[中央区](../Page/中央区_\(神戶市\).md "wikilink")、[兵庫区](../Page/兵庫区.md "wikilink")、[北区](../Page/北区_\(神戶市\).md "wikilink")、[長田区](../Page/長田区.md "wikilink")、[須磨区](../Page/須磨区.md "wikilink")、[垂水区](../Page/垂水区.md "wikilink")、[西区](../Page/西区_\(神戶市\).md "wikilink")
  - [姬路市](../Page/姬路市.md "wikilink")、[西宮市](../Page/西宮市.md "wikilink")、[尼崎市](../Page/尼崎市.md "wikilink")、[芦屋市](../Page/芦屋市.md "wikilink")、[伊丹市](../Page/伊丹市.md "wikilink")、[宝塚市](../Page/宝塚市.md "wikilink")、[川西市](../Page/川西市.md "wikilink")、[豐岡市](../Page/豐岡市.md "wikilink")、[三田市](../Page/三田市.md "wikilink")、[篠山市](../Page/篠山市.md "wikilink")、[丹波市](../Page/丹波市.md "wikilink")、[西脇市](../Page/西脇市.md "wikilink")、[三木市](../Page/三木市.md "wikilink")、[小野市](../Page/小野市.md "wikilink")、[明石市](../Page/明石市.md "wikilink")、[加古川市](../Page/加古川市.md "wikilink")、[加西市](../Page/加西市.md "wikilink")、[加東市](../Page/加東市.md "wikilink")、[高砂市](../Page/高砂市.md "wikilink")、[赤穗市](../Page/赤穗市.md "wikilink")、[相生市](../Page/相生市.md "wikilink")、[朝來市](../Page/朝來市.md "wikilink")、[龍野市](../Page/龍野市.md "wikilink")、[养父市](../Page/养父市.md "wikilink")、[洲本市](../Page/洲本市.md "wikilink")、[淡路市](../Page/淡路市.md "wikilink")、[南淡路市](../Page/南淡路市.md "wikilink")、[宍粟市](../Page/宍粟市.md "wikilink")、[新溫泉町](../Page/新溫泉町.md "wikilink")、[香美町](../Page/香美町.md "wikilink")、[神河町](../Page/神河町.md "wikilink")

<File:Westin> Awaji Island Hotel
06.jpg|[淡路夢舞台](../Page/淡路夢舞台.md "wikilink")，[淡路市](../Page/淡路市.md "wikilink")
<File:Ichikawa> river Ikuno Asago
Hyogo01s5bs4272.jpg|[口銀谷的風景](../Page/口銀谷.md "wikilink")，[朝來市](../Page/朝來市.md "wikilink")
<File:Yumura>
onsen11s1920.jpg|[湯村溫泉](../Page/湯村溫泉.md "wikilink")，[新溫泉町](../Page/新溫泉町.md "wikilink")
<File:Tonomine> highland 01
b.jpg|[砥峰高原](../Page/砥峰高原.md "wikilink")，[神河町](../Page/神河町.md "wikilink")

## 主要大学

  - [神户大学](../Page/神户大学.md "wikilink")
  - [兵库教育大学](../Page/兵库教育大学.md "wikilink")
  - 兵库县立大学
  - 神户市立外语大学
  - [关西学院大学](../Page/关西学院大学.md "wikilink")

## 交通

### 鐵道路線

  - 縣内的連接
      - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）
        [加古川線](../Page/加古川線.md "wikilink")、[播但線](../Page/播但線.md "wikilink")
      - [三木鐵道](../Page/三木鐵道.md "wikilink")
        [三木線](../Page/三木鐵道三木線.md "wikilink")
      - [北条鐵道](../Page/北条鐵道.md "wikilink")
        [北条線](../Page/北条鐵道北条線.md "wikilink")
      - [阪急電鐵](../Page/阪急電鐵.md "wikilink")
        [今津線](../Page/阪急今津線.md "wikilink")、[伊丹線](../Page/阪急今津線.md "wikilink")、[甲陽線](../Page/阪急甲陽線.md "wikilink")、[神戶高速線](../Page/阪急神戶高速線.md "wikilink")
      - [阪神電氣鐵道](../Page/阪神電氣鐵道.md "wikilink")
        [武庫川線](../Page/阪神武庫川線.md "wikilink")、[神戶高速線](../Page/阪神神戶高速線.md "wikilink")
      - [山陽電氣鐵道](../Page/山陽電氣鐵道.md "wikilink")
        [本線](../Page/山陽電氣鐵道本線.md "wikilink")、[網干線](../Page/山陽電氣鐵道網干線.md "wikilink")
      - [神戶電鐵](../Page/神戶電鐵.md "wikilink")
        [有馬線](../Page/神戶電鐵有馬線.md "wikilink")、[三田線](../Page/神戶電鐵三田線.md "wikilink")、[粟生線](../Page/神戶電鐵粟生線.md "wikilink")、[公園都市線](../Page/神戶電鐵公園都市線.md "wikilink")、[神戶高速線](../Page/神戶電鐵神戶高速線.md "wikilink")
      - [北神急行電鐵](../Page/北神急行電鐵.md "wikilink")
        [北神線](../Page/北神急行電鐵北神線.md "wikilink")
      - [能勢電鐵](../Page/能勢電鐵.md "wikilink")
        [日生線](../Page/能勢電鐵日生線.md "wikilink")
      - [神戶新交通](../Page/神戶新交通.md "wikilink")
        [港灣人工島線](../Page/港灣人工島線.md "wikilink")（）、[六甲人工島線](../Page/六甲人工島線.md "wikilink")（）
      - [神戶市營地下鐵](../Page/神戶市營地下鐵.md "wikilink")

<!-- end list -->

  - 近鄰府縣的連接
      - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")
        [福知山線](../Page/福知山線.md "wikilink")、[JR東西線](../Page/JR東西線.md "wikilink")、[赤穂線](../Page/赤穂線.md "wikilink")、[姫新線](../Page/姫新線.md "wikilink")
      - [京都丹後鐵道](../Page/京都丹後鐵道.md "wikilink")
        [宮豐線](../Page/宮津線.md "wikilink")
      - [智頭急行](../Page/智頭急行.md "wikilink")
        [智頭線](../Page/智頭急行智頭線.md "wikilink")
      - [阪急電鐵](../Page/阪急電鐵.md "wikilink")
        [神戶本線](../Page/阪急神戶本線.md "wikilink")、[寶塚本線](../Page/阪急寶塚本線.md "wikilink")
      - [阪神電氣鐵道](../Page/阪神電氣鐵道.md "wikilink")
        [本線](../Page/阪神本線.md "wikilink")、[難波線](../Page/阪神難波線.md "wikilink")
      - [能勢電鐵](../Page/能勢電鐵.md "wikilink")
        [妙見線](../Page/能勢電鐵妙見線.md "wikilink")

<!-- end list -->

  - 廣域的連接
      - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")
        [山陽新幹線](../Page/山陽新幹線.md "wikilink")、[東海道本線](../Page/東海道本線.md "wikilink")、[山陽本線](../Page/山陽本線.md "wikilink")、[山陰本線](../Page/山陰本線.md "wikilink")

### 道路

#### 收費道路

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/高速自動車國道.md" title="wikilink">高速自動車國道</a>
<ul>
<li><a href="../Page/名神高速道路.md" title="wikilink">名神高速道路</a></li>
<li><a href="../Page/中國自動車道.md" title="wikilink">中國自動車道</a></li>
<li><a href="../Page/山陽自動車道.md" title="wikilink">山陽自動車道</a></li>
<li><a href="../Page/舞鶴若狹自動車道.md" title="wikilink">舞鶴若狹自動車道</a></li>
<li><a href="../Page/播磨自動車道.md" title="wikilink">播磨自動車道</a></li>
</ul></li>
</ul></td>
<td><ul>
<li><a href="../Page/自動車専用道路.md" title="wikilink">自動車専用道路</a>
<ul>
<li><a href="../Page/神戶淡路鳴門自動車道.md" title="wikilink">神戶淡路鳴門自動車道</a></li>
<li><a href="../Page/西神自動車道.md" title="wikilink">西神自動車道</a></li>
<li><a href="../Page/北近畿豐岡自動車道.md" title="wikilink">北近畿豐岡自動車道</a></li>
<li><a href="../Page/播但連絡道路.md" title="wikilink">播但連絡道路</a></li>
<li><a href="../Page/阪神高速道路.md" title="wikilink">阪神高速道路</a></li>
<li><a href="../Page/第二神明道路.md" title="wikilink">第二神明道路</a></li>
</ul></li>
</ul></td>
<td><ul>
<li>其他收費道路
<ul>
<li><a href="../Page/六甲北有料道路.md" title="wikilink">六甲北有料道路</a></li>
<li><a href="../Page/六甲隧道.md" title="wikilink">六甲隧道</a></li>
<li><a href="../Page/西神戶有料道路.md" title="wikilink">西神戶有料道路</a></li>
<li><a href="../Page/山麓繞道.md" title="wikilink">山麓繞道</a></li>
<li><a href="../Page/新神戶隧道.md" title="wikilink">新神戶隧道</a></li>
<li><a href="../Page/西宮北道路.md" title="wikilink">西宮北道路</a></li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

#### 國道

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/國道2號_(日本).md" title="wikilink">國道2號</a></li>
<li><a href="../Page/國道9號_(日本).md" title="wikilink">國道9號</a></li>
<li><a href="../Page/國道28號.md" title="wikilink">國道28號</a></li>
<li><a href="../Page/國道29號.md" title="wikilink">國道29號</a></li>
<li><a href="../Page/國道43號.md" title="wikilink">國道43號</a></li>
<li><a href="../Page/國道171號.md" title="wikilink">國道171號</a></li>
<li><a href="../Page/國道173號.md" title="wikilink">國道173號</a></li>
<li><a href="../Page/國道174號.md" title="wikilink">國道174號</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/國道175號.md" title="wikilink">國道175號</a></li>
<li><a href="../Page/國道176號.md" title="wikilink">國道176號</a></li>
<li><a href="../Page/國道178號.md" title="wikilink">國道178號</a></li>
<li><a href="../Page/國道179號.md" title="wikilink">國道179號</a></li>
<li><a href="../Page/國道250號.md" title="wikilink">國道250號</a></li>
<li><a href="../Page/國道312號.md" title="wikilink">國道312號</a></li>
<li><a href="../Page/國道372號.md" title="wikilink">國道372號</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/國道373號.md" title="wikilink">國道373號</a></li>
<li><a href="../Page/國道426號.md" title="wikilink">國道426號</a></li>
<li><a href="../Page/國道427號.md" title="wikilink">國道427號</a></li>
<li><a href="../Page/國道428號.md" title="wikilink">國道428號</a></li>
<li><a href="../Page/國道429號.md" title="wikilink">國道429號</a></li>
<li><a href="../Page/國道436號.md" title="wikilink">國道436號</a></li>
<li><a href="../Page/國道477號.md" title="wikilink">國道477號</a></li>
</ul></td>
</tr>
</tbody>
</table>

#### 縣道

  - [兵庫縣縣道一覽](../Page/兵庫縣縣道一覽.md "wikilink")

#### 其他

  - [山手幹線](../Page/阪神間山手幹線.md "wikilink")

### 機場

  - [大阪國際機場](../Page/大阪國際機場.md "wikilink")（伊丹市）
  - [但馬機場](../Page/但馬飛行場.md "wikilink")（豐岡市）
  - [神戶機場](../Page/神戶機場.md "wikilink")（神戶市・2006年啟用）

### 港灣

[Port_of_Kobe01s3780.jpg](https://zh.wikipedia.org/wiki/File:Port_of_Kobe01s3780.jpg "fig:Port_of_Kobe01s3780.jpg")\]\]

  - [中枢國際港灣](../Page/中枢國際港灣.md "wikilink")：[神戶港](../Page/神戶港.md "wikilink")（神戶市）
  - [特定重要港灣](../Page/特定重要港灣.md "wikilink")：神戶港（神戶市）、[姬路港](../Page/姬路港.md "wikilink")（姬路市）
  - [重要港灣](../Page/重要港灣.md "wikilink")：[尼崎西宮芦屋港](../Page/尼崎西宮芦屋港.md "wikilink")（尼崎市、西宮市、芦屋市）、東播磨港（播磨町、加古川市、高砂市）
  - 其他港灣：兵庫港、長田港、垂水港、明石港、江井島港、二見港、相生港、赤穗港、濱坂港、香住港、津居山港、岩屋港、津名港、洲本港、由良港、福良港、[室津港](../Page/室津.md "wikilink")、[坂越港](../Page/坂越.md "wikilink")、富島港、沼島港

## 特產

<table>
<thead>
<tr class="header">
<th><p>水果、青菜</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/黑大豆.md" title="wikilink">黑豆</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/栃之實.md" title="wikilink">栃之實</a><br />
（）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/無花果.md" title="wikilink">無花果</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/松茸.md" title="wikilink">松茸</a></p></td>
</tr>
<tr class="odd">
<td><p>魚介類、海產</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/松葉蟹.md" title="wikilink">松葉蟹</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/螢烏賊.md" title="wikilink">螢烏賊</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/牡蠣.md" title="wikilink">牡蠣</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鯛.md" title="wikilink">鯛</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/章魚.md" title="wikilink">蛸</a></p></td>
</tr>
<tr class="odd">
<td><p>肉類、乳製品</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/神戶牛.md" title="wikilink">神戶牛</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/但馬牛.md" title="wikilink">但馬牛</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/淡路牛.md" title="wikilink">淡路牛</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/猪肉.md" title="wikilink">猪肉</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/乳製品.md" title="wikilink">乳製品</a></p></td>
</tr>
<tr class="odd">
<td><p>鄉土料理</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/明石燒.md" title="wikilink">明石燒</a></p></td>
</tr>
<tr class="odd">
<td><p>鹽味饅頭</p></td>
</tr>
<tr class="even">
<td><p>神戶葡萄酒</p></td>
</tr>
<tr class="odd">
<td><p>工藝品、民俗藝品</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/丹波立杭燒.md" title="wikilink">丹波立杭燒</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/出石燒.md" title="wikilink">出石燒</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/播州三木打刃物.md" title="wikilink">播州三木打刃物</a></p></td>
</tr>
<tr class="odd">
<td><p>豊岡杞柳細工</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/播州毛鉤.md" title="wikilink">播州毛鉤</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/播州算盤.md" title="wikilink">播州算盤</a></p></td>
</tr>
<tr class="even">
<td><p>參考資料：[1]</p></td>
</tr>
</tbody>
</table>

## 國際交流

### 姐妹城市

  - [廣東省](../Page/廣東省.md "wikilink")（1983年）

  - [海南省](../Page/海南省.md "wikilink")（1990年）

  - [哈巴羅夫斯克邊疆區](../Page/哈巴羅夫斯克邊疆區.md "wikilink")（1969年）

  - [西澳大利亞州](../Page/西澳大利亞州.md "wikilink")（1981年）

  - [華盛頓州](../Page/華盛頓州.md "wikilink")（1963年）

  - （1983年）

  - [巴拉那州](../Page/巴拉那州.md "wikilink")（1970年）

### 交流城市

  - [塞納-馬恩省](../Page/塞納-馬恩省.md "wikilink")、[安德爾-羅亞爾省](../Page/安德爾-羅亞爾省.md "wikilink")（1991年）

  - [什勒斯維希-霍爾斯坦邦](../Page/什勒斯維希-霍爾斯坦.md "wikilink")（1997年）

  - [阿韋龍省](../Page/阿韋龍省.md "wikilink")（2000年）

  - [江蘇省](../Page/江蘇省.md "wikilink")（2006年）

  - [胡志明市](../Page/胡志明市.md "wikilink")（2007年）

  - [慶尚南道](../Page/慶尚南道.md "wikilink")（2012年）

  - [諾爾省](../Page/諾爾省.md "wikilink")（2013年）

  - [同奈省](../Page/同奈省.md "wikilink")（2013年）

  - [古吉拉特邦](../Page/古吉拉特邦.md "wikilink")（2016年）

  - [河南省](../Page/河南省.md "wikilink")（2016年）

  - [隆安省](../Page/隆安省.md "wikilink")、[芹苴市](../Page/芹苴市.md "wikilink")（2017年）

## 参考文献

## 外部链接

  - [兵庫縣](http://web.pref.hyogo.jp/)官方網站
  - [兵庫縣观光联盟](http://www.travelhyogo.org.c.aas.hpcn.transer-cn.com/)

{{-}}

[B兵](../Category/日本都道府縣.md "wikilink")
[兵庫縣](../Category/兵庫縣.md "wikilink")

1.