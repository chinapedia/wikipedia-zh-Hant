**兹韦恩德雷赫特**（）位于[荷兰](../Page/荷兰.md "wikilink")，是[南荷兰省的一个镇](../Page/南荷兰省.md "wikilink")，人口44,445（2011年）。

## 友好城市

  - [諾德施泰特](../Page/諾德施泰特.md "wikilink") 1981年

  - [波普拉德](../Page/波普拉德.md "wikilink") 2000年

  - [兹韦恩德雷赫特](../Page/兹韦恩德雷赫特_\(比利时\).md "wikilink") 2004年

## 外部链接

  - [官方网站](http://www.zwijndrecht.nl)

[Category:南荷兰省城市](../Category/南荷兰省城市.md "wikilink")