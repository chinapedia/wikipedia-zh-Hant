**冀察政务委员会**，是1935年12月到1937年8月之间实际管理[中国](../Page/中国.md "wikilink")[河北](../Page/河北省_\(中華民國\).md "wikilink")、[察哈尔两地之行政机关](../Page/察哈爾省_\(中華民國\).md "wikilink")。它是[中华民国对](../Page/中华民国.md "wikilink")[大日本帝国妥协之产物](../Page/大日本帝国.md "wikilink")，雖隸屬於[国民政府](../Page/国民政府.md "wikilink")，但也為適應華北特殊情況而有很大自主程度\[1\]。

冀察政务委员会在日本人眼里是华北自治政权，在南京眼里是地方政府。由宋哲元亲任委员长，委员会里西北军、东北军和亲日分子三足鼎立。

原[国民政府军事委员会北平军分会和冀察](../Page/国民政府军事委员会.md "wikilink")[绥靖公署被取消](../Page/绥靖公署.md "wikilink")。

## 历史

1935年12月，在國民政府主動提議下，設置「冀察政务委员会」，以[宋哲元为委员长](../Page/宋哲元.md "wikilink")\[2\]。该委员会以宋哲元为委员长，委员16人，包括宋哲元系、[东北系](../Page/东北军.md "wikilink")、旧[皖系](../Page/皖系.md "wikilink")、旧[直系人物及平津士绅](../Page/直系.md "wikilink")，宋与[天津市市长](../Page/天津市市长.md "wikilink")[萧振瀛](../Page/萧振瀛.md "wikilink")、[北平市市长](../Page/北平市市长.md "wikilink")[秦德纯为核心](../Page/秦德纯.md "wikilink")，宋部师长[张自忠任](../Page/张自忠.md "wikilink")[察哈尔省政府主席](../Page/察哈尔省政府.md "wikilink")，宋兼[河北省政府主席](../Page/河北省政府.md "wikilink")。委员中还有亲日分子[王揖唐](../Page/王揖唐.md "wikilink")、[王克敏等人](../Page/王克敏.md "wikilink")。

設立冀察政務委員會，使日本軍方所期盼之「自治」政府完全失去設立之根據，[土肥原賢二擾攘數月之分離華北陰謀只得暫時擱置](../Page/土肥原賢二.md "wikilink")\[3\]。1935年[一二·九运动目标之一](../Page/一二·九运动.md "wikilink")，就是反对成立冀察政务委员会。1937年8月6日，[日军占领下的](../Page/日军.md "wikilink")[北平政府成立后](../Page/北平.md "wikilink")，8月20日该委员会被解散\[4\]

## 组成人员

  - 委员长：[宋哲元](../Page/宋哲元.md "wikilink")（1935年12月11日任）
  - 委员：
      - [宋哲元](../Page/宋哲元.md "wikilink")（1935年12月11日任）
      - [万福麟](../Page/万福麟.md "wikilink")（1935年12月11日任）
      - [王揖唐](../Page/王揖唐.md "wikilink")（1935年12月11日任）
      - [刘哲](../Page/刘哲.md "wikilink")（1935年12月11日任）
      - [李廷玉](../Page/李廷玉.md "wikilink")（1935年12月11日任－1937年4月2日免）
      - [贾德耀](../Page/贾德耀.md "wikilink")（1935年12月11日任）
      - [胡毓坤](../Page/胡毓坤.md "wikilink")（1935年12月11日任）
      - [高凌霨](../Page/高凌霨.md "wikilink")（1935年12月11日任）
      - [王克敏](../Page/王克敏.md "wikilink")（1935年12月11日任－1937年4月2日免）
      - [萧振瀛](../Page/萧振瀛.md "wikilink")（1935年12月11日任）
      - [秦德纯](../Page/秦德纯.md "wikilink")（1935年12月11日任）
      - [张自忠](../Page/张自忠.md "wikilink")（1935年12月11日任）
      - [程克](../Page/程克.md "wikilink")（1935年12月11日任）
      - [周作民](../Page/周作民.md "wikilink")（1935年12月11日任）
      - [门致中](../Page/门致中.md "wikilink")（1935年12月11日任）
      - [石敬亭](../Page/石敬亭.md "wikilink")（1935年12月11日任）
      - [冷家骥](../Page/冷家骥.md "wikilink")（1935年12月11日任－1937年4月2日免）
      - [汤尔和](../Page/汤尔和.md "wikilink")（1936年7月24日任）
      - [曹汝霖](../Page/曹汝霖.md "wikilink")（1936年7月24日任）
      - [戈定远](../Page/戈定远.md "wikilink")（1936年7月24日任）
      - [刘汝明](../Page/刘汝明.md "wikilink")（1936年7月24日任）
      - [李思浩](../Page/李思浩.md "wikilink")（1936年10月21日任）
      - [冯治安](../Page/冯治安.md "wikilink")（1937年4月2日任）
      - [邓哲熙](../Page/邓哲熙.md "wikilink")（1937年4月2日任）
      - [章士钊](../Page/章士钊.md "wikilink")（1937年4月2日任）\[5\]

## 参考文献

## 参见

  - [何梅协定](../Page/何梅协定.md "wikilink")
  - [冀東防共自治政府](../Page/冀東防共自治政府.md "wikilink")

{{-}}

[Category:中华民国大陸時期地方政府](../Category/中华民国大陸時期地方政府.md "wikilink")
[Category:中华民国河北省](../Category/中华民国河北省.md "wikilink")
[Category:察哈尔省](../Category/察哈尔省.md "wikilink")
[Category:河北抗战史](../Category/河北抗战史.md "wikilink")
[Category:内蒙古抗战史](../Category/内蒙古抗战史.md "wikilink")
[Category:华北政治史](../Category/华北政治史.md "wikilink")
[Category:1935年建立政府機構](../Category/1935年建立政府機構.md "wikilink")
[Category:1937年廢除](../Category/1937年廢除.md "wikilink")
[Category:七七事变](../Category/七七事变.md "wikilink")

1.

2.
3.
4.  王秀鑫、郭德宏 主编：《中华民族抗日战争史（1931-1945）》，北京：中共党史出版社，1995年

5.