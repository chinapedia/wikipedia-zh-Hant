**内丘县**是[河北省](../Page/河北省.md "wikilink")[邢台市下辖的一个县](../Page/邢台.md "wikilink")。县人民政府驻内丘镇。西邻[山西省](../Page/山西省.md "wikilink")[晋中市](../Page/晋中市.md "wikilink")[昔阳县](../Page/昔阳县.md "wikilink")，东连[隆尧县](../Page/隆尧县.md "wikilink")、[任县](../Page/任县.md "wikilink")，南近[邢台县](../Page/邢台县.md "wikilink")，北邻[临城县](../Page/临城县.md "wikilink")、[石家庄市](../Page/石家庄市.md "wikilink")[赞皇县](../Page/赞皇县.md "wikilink")。县内河流均为[子牙河支流](../Page/子牙河.md "wikilink")。

## 历史

[西汉置](../Page/西汉.md "wikilink")[中丘县](../Page/中丘县.md "wikilink")，“县西北有蓬山，丘在其间，故名中丘”\[1\]，在[汉高祖时属赵国](../Page/汉高祖.md "wikilink")[邯郸郡](../Page/邯郸郡.md "wikilink")，后分赵国另置[常山国](../Page/常山国.md "wikilink")，中丘县属之，国除后，为[常山郡](../Page/常山郡.md "wikilink")。\[2\]
[晋](../Page/晋.md "wikilink")[太康六年](../Page/太康_\(西晋\).md "wikilink")（285年），升为[中丘郡](../Page/中丘郡.md "wikilink")，后废为县，[北魏属](../Page/北魏.md "wikilink")[殷州](../Page/殷州_\(北魏\).md "wikilink")[南赵郡](../Page/南赵郡.md "wikilink")。[隋](../Page/隋.md "wikilink")[开皇元年](../Page/开皇.md "wikilink")（581年）为避[隋文帝父](../Page/隋文帝.md "wikilink")[杨忠讳](../Page/楊忠_\(北朝\).md "wikilink")，改名[内丘](../Page/内丘.md "wikilink")，属[赵州](../Page/赵州_\(河北\).md "wikilink")，开皇十六年改属[滦州](../Page/滦州.md "wikilink")。[唐属](../Page/唐.md "wikilink")[邢州](../Page/邢州.md "wikilink")，[北宋](../Page/北宋.md "wikilink")[宣和元年](../Page/宣和伐辽.md "wikilink")（1119年）六月，因[宋英宗即位前曾封](../Page/宋英宗.md "wikilink")[钜鹿郡公](../Page/鉅鹿郡.md "wikilink")（封地邢州），升[邢州为](../Page/邢州.md "wikilink")[信德府](../Page/信德府.md "wikilink")，内丘县隶之。[金复邢州](../Page/金朝.md "wikilink")。[元升邢州为](../Page/元朝.md "wikilink")[顺德府](../Page/顺德府.md "wikilink")，内丘县仍隶之。[明因之](../Page/明朝.md "wikilink")。[清](../Page/清朝.md "wikilink")[雍正四年](../Page/雍正.md "wikilink")（1726年）因尊[孔子讳](../Page/孔子.md "wikilink")，曾一度改为**内邱县**。\[3\]

## 行政区划

下辖5个[镇](../Page/行政建制镇.md "wikilink")、4个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 官方网站

  - [内邱之窗](http://www.hbnq.gov.cn/)

## 参考资料

[内丘县](../Page/category:内丘县.md "wikilink")
[县](../Page/category:邢台区县市.md "wikilink")
[邢台](../Page/category:河北省县份.md "wikilink")

1.  [王先谦](../Page/王先谦.md "wikilink").《汉书补注》引《十三州志》
2.  周振鹤.西汉政区地理.人民出版社.1987
3.  中国县情大全.华北卷.p.363-368.