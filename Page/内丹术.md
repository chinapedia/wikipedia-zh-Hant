**内丹术**是[道教一种重要的修炼方法](../Page/道教.md "wikilink")，现在一般視為道教[气功](../Page/气功.md "wikilink")（道家多称“煉氣术”）的一种。内丹术指以“人身是一小天地”的“[天人合一](../Page/天人合一.md "wikilink")”、“[天人相应](../Page/天人相应.md "wikilink")”思想为[理论](../Page/理论.md "wikilink")，进行性命的修炼，以人身为鼎炉，修炼“精、气、神”等而达成强身健体、提高人体的生命功能、延长寿命、乃至成[仙](../Page/仙.md "wikilink")、[长生不老之目的](../Page/长生不老.md "wikilink")。

## 歷史

[NeijingTu1.jpg](https://zh.wikipedia.org/wiki/File:NeijingTu1.jpg "fig:NeijingTu1.jpg")
[中医专著](../Page/中医.md "wikilink")《[黄帝内经](../Page/黄帝内经.md "wikilink")》记载“[真人](../Page/真人.md "wikilink")”、“[至人](../Page/至人.md "wikilink")”、“[圣人](../Page/圣人.md "wikilink")”「[賢人](../Page/賢人.md "wikilink")」的修炼境界，以及「提挈天地，把握[陰陽](../Page/陰陽.md "wikilink")，呼吸精氣，獨立守神，肌肉若一」、「精神不散」、「積精全神」、「移精变气」等修炼、疗病方法。「移精变气」可以看作是后世内丹学「炼精化气」、「炼气化神」一类的方法。记载“[黄帝且战且学仙](../Page/黄帝.md "wikilink")”。

[老子是](../Page/老子.md "wikilink")[先秦道家学派的代表](../Page/先秦.md "wikilink")，其著作《[道德经](../Page/道德经.md "wikilink")》中即有“归根复命”，“深根固柢”的内炼学说，又讲述了“長生久視”、“沒身不殆”的理想，亦讲述了“贵以身为天下者，则可寄于天下；爱以身为天下者，乃可以托于天下”的方法论，亦提到“或嘘或吹”、“绵绵呵其若存”的吐纳功法。《[庄子](../Page/庄子_\(書\).md "wikilink")》中提到“天地与我并生，万物与我为一”，亦有「心斋」、「坐忘」之类的内炼方法。《[管子](../Page/管子_\(書籍\).md "wikilink")》则认为“能守一而弃万疴”。内丹术与源自先秦时期的[行气](../Page/行气.md "wikilink")、[导引等有关](../Page/导引.md "wikilink")。[王乔](../Page/王乔.md "wikilink")、[赤松子均为行气派之祖](../Page/赤松子.md "wikilink")。[春秋末](../Page/春秋.md "wikilink")[战国初的出土文物](../Page/战国.md "wikilink")《[行气玉佩铭](../Page/行气玉佩铭.md "wikilink")》，系统具体地讲述了内气炼养方法。《[楚辞](../Page/楚辞.md "wikilink")·远游》一文中，也有“餐六气而饮沆瀣兮，漱正阳而含朝霞”之句。

早期[道教的一部重要的经书](../Page/道教.md "wikilink")《[太平经](../Page/太平经.md "wikilink")》也有一套自己的[长生不老的修道理论和方法](../Page/长生不老.md "wikilink")，认为需要“爱气尊神重精”、“行善”、“守一”、“食气”等。[东汉](../Page/东汉.md "wikilink")[魏伯阳著](../Page/魏伯阳.md "wikilink")《[周易参同契](../Page/周易参同契.md "wikilink")》，提出了“参同相类”的修炼模式，为丹法之祖书。[东晋](../Page/东晋.md "wikilink")[葛洪是](../Page/葛洪.md "wikilink")[金丹道教的理论家与实践者](../Page/金丹.md "wikilink")。他认为金丹之道，乃仙道之极。葛洪《[抱朴子](../Page/抱朴子.md "wikilink")‧金丹篇》说：“余考览养性之书，鸠集久视之方，曾所披涉篇卷，以千计矣，莫不皆以还丹、金液为大要者焉。然则此二事，盖仙道之极也。服此而不仙，则古来无仙矣。”[魏晋时道教教团重视内炼养生](../Page/魏晋.md "wikilink")，[上清派整理道经](../Page/上清派.md "wikilink")，如[魏华存](../Page/魏华存.md "wikilink")《[黄庭经](../Page/黄庭经.md "wikilink")》、[陶弘景](../Page/陶弘景.md "wikilink")《服气导引法》。

“内丹”一词于已有文献中最早见于题为[东晋](../Page/东晋.md "wikilink")[许逊所作的](../Page/许逊.md "wikilink")《灵剑子·服气诀》：“服气调咽用内丹”。另《灵剑子·松沙记》：“学道之士，初广布阴骘，先行气攻，持内丹长生久视之法。”[南北朝](../Page/南北朝.md "wikilink")[梁代南岳佛教](../Page/南梁.md "wikilink")[天台宗三祖](../Page/天台宗.md "wikilink")[慧思禅师](../Page/慧思.md "wikilink")（515—577）《立警愿文》中说到：“我今入山修习苦行，[忏悔破戒障道罪](../Page/忏悔.md "wikilink")，今身及先身是罪悉忏悔，为护法敌求长寿命，不愿生天及余趣，愿诸贤圣左助我，得好芝草及神丹，疗治众病除饥渴，常得经修行诸[禅](../Page/禅.md "wikilink")，愿得深山静处，神丹药修此愿，借[外丹力修](../Page/外丹.md "wikilink")[内丹](../Page/内丹.md "wikilink")，欲安众生先自安。己身有缚能解他缚，无有是处。”这是最早将外丹、内丹明确划分开的一处著作。(道家外丹也可指“虚空中清灵之气”，前中国道教协会会长[陈撄宁先生云](../Page/陈撄宁.md "wikilink")：“外界资助，当然不可少，却是在虚空中寻求。”)

[隋朝时](../Page/隋朝.md "wikilink")，修道于[罗浮山青霞谷的道士](../Page/罗浮山.md "wikilink")“青霞子”[苏元朗提出](../Page/苏元朗.md "wikilink")“归神丹于心炼”，提倡“[性命双修](../Page/性命双修.md "wikilink")”，强调心身的全面修炼，以此为内丹修炼的核心。“性命双修”一说，进一步推动了内丹术理论的发展。

[唐朝与](../Page/唐朝.md "wikilink")[五代](../Page/五代.md "wikilink")，是内丹之道发展的关键时期，[崔希范撰](../Page/崔希范.md "wikilink")《[入药镜](../Page/入药镜.md "wikilink")》，[司马承祯作](../Page/司马承祯.md "wikilink")《[天隐子](../Page/天隐子.md "wikilink")》、《[坐忘论](../Page/坐忘论.md "wikilink")》、《服气精义论》，[李筌](../Page/李筌.md "wikilink")、[张果等注解](../Page/张果.md "wikilink")《[阴符经](../Page/阴符经.md "wikilink")》，[钟离权著](../Page/钟离权.md "wikilink")《[灵宝毕法](../Page/灵宝毕法.md "wikilink")》、《还丹歌》、《破迷正道歌》，[吕洞宾继承传统丹道](../Page/吕洞宾.md "wikilink")，并作《九真玉书》、《[直指大丹歌](../Page/直指大丹歌.md "wikilink")》、《[指玄篇](../Page/指玄篇.md "wikilink")》、《百字铭》等，[施肩吾撰](../Page/施肩吾.md "wikilink")《[钟吕传道集](../Page/钟吕传道集.md "wikilink")》，[陈抟作](../Page/陈抟.md "wikilink")《[太极图](../Page/太极图.md "wikilink")》、《[无极图](../Page/无极图.md "wikilink")》，使内丹之道的理论与方法进一步完备和发展。宋朝《[云笈七签](../Page/云笈七签.md "wikilink")》中列“诸家气法”及“内丹诀法”，多是唐代作品。

[北宋](../Page/北宋.md "wikilink")[张伯端称](../Page/张伯端.md "wikilink")“感真人授金丹药物火候之诀”，把《道德经》、《[阴符经](../Page/阴符经.md "wikilink")》作为内丹祖书，作《[悟真篇](../Page/悟真篇.md "wikilink")》，融老子内炼思想于《悟真篇》内丹之道中。张伯端《悟真篇》还强调要炼内丹，必先积功德：“德行修逾八百，阴功积满三千。均齐物我与亲冤，始合神仙本愿。”《悟真篇》也继承了道教[上清派的主要经典](../Page/上清派.md "wikilink")《[黄庭经](../Page/黄庭经.md "wikilink")》身体脏腑各有神所主的“五脏神”之说，并结合了东汉魏伯阳《周易参同契》的炼丹之道。

[张伯端一派所传丹法](../Page/张伯端.md "wikilink")，其继承系统为[张伯端](../Page/张伯端.md "wikilink")─[石泰](../Page/石泰.md "wikilink")─[薛道光](../Page/薛道光.md "wikilink")─[陈楠](../Page/陈楠.md "wikilink")─[白玉蟾](../Page/白玉蟾.md "wikilink")，形成[丹道的流派称为内丹](../Page/丹道.md "wikilink")[南宗](../Page/南宗.md "wikilink")。

另有[王重阳开创道教](../Page/王重阳.md "wikilink")[全真派](../Page/全真派.md "wikilink")，称承[钟离权](../Page/钟离权.md "wikilink")、[吕洞宾之真传](../Page/吕洞宾.md "wikilink")，修炼亦以内丹为首务，主张[性命双修](../Page/性命双修.md "wikilink")，明心见性，以修性为先。《[性命圭旨](../Page/性命圭旨.md "wikilink")》提出：「何謂之性？元始真如，一靈炯炯是也。何謂之命？先天至精，一炁氤氳是也。」[王重阳所传丹法流派称内丹](../Page/王重阳.md "wikilink")[北宗](../Page/北宗.md "wikilink")。

以后又有[元代](../Page/元代.md "wikilink")[李道纯所创内丹](../Page/李道纯.md "wikilink")[中派](../Page/中派.md "wikilink")；[明代](../Page/明代.md "wikilink")[陆潜虚所创](../Page/陆潜虚.md "wikilink")[东派](../Page/东派.md "wikilink")；[清代](../Page/清代.md "wikilink")[李涵虚所创](../Page/李涵虚.md "wikilink")[西派](../Page/西派.md "wikilink")。另有明清时[伍守阳](../Page/伍守阳.md "wikilink")、[柳华阳所创的](../Page/柳华阳.md "wikilink")[全真派的分派](../Page/全真派.md "wikilink")“[伍柳派](../Page/伍柳派.md "wikilink")”。元明时代的道士[张三丰](../Page/张三丰.md "wikilink")，丹法据称传自[火龙真人](../Page/火龙真人.md "wikilink")，称为[隐仙派](../Page/隐仙派.md "wikilink")，其法诀采"清修派'与"阴阳派"之长，著有《无根树词》、《大道论》、《玄机直讲》等。另外据说还有不少隐传的内丹流派。隐传的内丹术大多师徒相承，口口相授，外人很难了解。

部分研究人士认为中国古典名著《[西游记](../Page/西游记.md "wikilink")》、《[封神演义](../Page/封神演义.md "wikilink")》不仅是[文学名著](../Page/文学.md "wikilink"),
还是阐述内丹术的"丹经"。

《[四库提要](../Page/四库提要.md "wikilink")》说：“后世神怪之迹，多附于[道家](../Page/道家.md "wikilink")，原其本始，则始于清静自持。其后[长生之说与](../Page/长生.md "wikilink")[神仙家合而为一](../Page/神仙家.md "wikilink")，而[服饵](../Page/服饵.md "wikilink")、[导引入之](../Page/导引.md "wikilink")；[房中一家近于神仙者亦入之](../Page/房中.md "wikilink")。”

## 理論

內丹的一個重要觀念，是要求體液逆流或逆行，尤其是精液與唾液。\[1\]
[_The_Immortal_Soul_of_the_Taoist_Adept.PNG](https://zh.wikipedia.org/wiki/File:_The_Immortal_Soul_of_the_Taoist_Adept.PNG "fig:_The_Immortal_Soul_of_the_Taoist_Adept.PNG")
[Teng_Feng-Chou_2016.jpg](https://zh.wikipedia.org/wiki/File:Teng_Feng-Chou_2016.jpg "fig:Teng_Feng-Chou_2016.jpg")

### 全真教

全真教的內丹術，是冥想[丹田中有一丹爐](../Page/丹田.md "wikilink")，結合上下體液中的要素，產生純陽之氣。要造純陽之氣，要無憂無欲，心無雜念，方能有功。

舌頭攪拌唾液後，形成泡沫，嚥下泡沫，以獲取純陽之氣。唾液稱為「上水」，得和下身產生的「火」結合。精於此道者，可製造兩種不同的唾液，其一結合「肝氣」，其二結合「肺氣」，最後製造出「神水」，當中有「火」的元素。下身生出的「火」，同樣與二氣結合。三爻卦裏的「坎」和「離」，一旦在丹田會合，也可以製造神水和真火。用這種方法所造的純陽之氣，便是以前道士所謂的長生金丹。\[2\]

关于内丹修炼的阶次，各家方法有差，一般可分为[筑基](../Page/筑基.md "wikilink")、炼精化气、炼气化神、炼神还虚几个阶段。元代[陈致虚](../Page/陈致虚.md "wikilink")《[金丹大要](../Page/金丹大要.md "wikilink")》卷四曰：“是皆不外神气精三物，是以三物相感，顺则成人，逆则生丹。何为顺？一生二，二生三，三生万物，故虚化神，神化气，气化精，精化形，形乃成人。何谓逆？万物含三，三归二，二归一，知此道者怡神守形，养形炼精，积精化气，炼气合神，炼神还虚，金丹乃成。”《金丹大要》还说：“求于册者，当以《[阴符](../Page/阴符经.md "wikilink")》、《[道德](../Page/道德经.md "wikilink")》为祖，《金碧》、《[参同](../Page/周易参同契.md "wikilink")》次之。”

据传道家[文始派丹法修炼下手即以最上一层](../Page/文始派.md "wikilink")“炼神还虚”做起。《[文始经](../Page/文始经.md "wikilink")》曰：“能见精神而久生，能忘精神而超生。”《[仙學真詮](../Page/仙學真詮.md "wikilink")》釋之曰:
“盖忘精神者，虚极静笃，而精自然化气，气自然化神，神自然还虚，虚无大道之学也；见精神者，虚静以为本，火符以为用，炼精成气，炼气成神，炼神还虚，此以神御气之术也。学虚无大道者，虽不着于精气，然与道合真，神形俱妙，有无隐显，变化莫测，其寿无量，是了性而自了命者也，举上而兼下也；以神御气，则著于精气矣，然保毓元和，运行不息，冲和之至，薰蒸融液，亦能使形合于神，长生不死，乃了命而性因以存也，自下而做向上去也。此二端虽大小不同，而皆有益于人。”

《[老子想尔注](../Page/老子想尔注.md "wikilink")》中提出：“所以精者，道之别气也。”“万物含道精。”“夫欲宝精，百行当修，万善当著，调和[五行](../Page/五行.md "wikilink")。”《[心印经](../Page/心印经.md "wikilink")》说：“精合其神，神合其气，气合其真，不得其真，皆是强名。”[全真派](../Page/全真派.md "wikilink")《晋真人语录》曰：“若人修行养命，先须积行累功。有功无行，道果难成。功行两全，是谓真人。”“若要真功者，须是澄心定意，打叠精神，无动无作，真清真净，抱元守一，存神固炁，乃真功也。”“若要真行，须要修行蕴德，济贫拔苦，见人患难，常怀拯救之心，或化诱善人入道，修行所为之事，先人后己，与万物无私，乃真行也。”[张三丰以修人道为炼仙道的基础](../Page/张三丰.md "wikilink")，强调只要素行阴德，仁慈悲悯，忠孝信诚，全于人道，离仙道也就自然不远了。他把道家的内炼思想同[儒家学说合在一起](../Page/儒家.md "wikilink")，说：“人能修正身心，则真精、真神聚其中，大才、大德出其中。”

炼内丹方法还有“人元丹法”、「地元丹法」和「天元丹法」之别，“龙凤丹法”与“龙虎丹法”之别，阶次各有不同。

## 西方心理学看法

[德国](../Page/德国.md "wikilink")[卫礼贤](../Page/卫礼贤.md "wikilink")（[Richard
Wilhelm](http://en.wikipedia.org/wiki/Richard_Wilhelm)）曾率先用西方[心理学的概念](../Page/心理学.md "wikilink")—[意识与](../Page/意识.md "wikilink")[无意识](../Page/无意识.md "wikilink")—来阐释内丹学的识神和元神范畴，并认为“性”就是意识与无意识，而“命”与[生理之本能密切相关](../Page/生理.md "wikilink")。[荣格的](../Page/荣格.md "wikilink")[分析心理学旨在消除意识与无意识之间的对立](../Page/分析心理学.md "wikilink")，荣格曾用[集体无意识理论理解了内丹学著作](../Page/集体无意识.md "wikilink")《[太乙金华宗旨](../Page/太乙金华宗旨.md "wikilink")》（《[The
Secret of the Golden
Flower](http://en.wikipedia.org/wiki/The_Secret_of_the_Golden_Flower)》）中的超越现象，视之为一无意识的表征。

## 人物

史书记载，内丹家多高寿，如《[宋史](../Page/宋史.md "wikilink")》载：[陈抟寿长](../Page/陈抟.md "wikilink")118岁，[张无梦](../Page/张无梦.md "wikilink")99岁，[张伯端](../Page/张伯端.md "wikilink")96岁，[石泰](../Page/石泰.md "wikilink")136岁，[薛道光](../Page/薛道光.md "wikilink")113岁，[陈朴](../Page/陈朴.md "wikilink")、[施肩吾](../Page/施肩吾.md "wikilink")、[蓝元道](../Page/蓝元道.md "wikilink")、[陈楠](../Page/陈楠.md "wikilink")、[白玉蟾等内丹家都达到高龄](../Page/白玉蟾.md "wikilink")。

## 参考文献

## 研究書目

  - Joseph Needham（[李約瑟](../Page/李約瑟.md "wikilink")）著，鄒海波 等
    譯：《[中國科學技術史](../Page/中國科學技術史.md "wikilink")》，第5卷第5分冊，「煉丹術的發現和發明：內丹」（北京：科學出版社，2011年）.
  - 謝世維：〈[當代西方對宋元以後內丹研究之回顧](http://www.cl.nthu.edu.tw/ezfiles/278/1278/img/1473/149704406.pdf)〉.

## 参见

  - [道教](../Page/道教.md "wikilink")
  - [外丹术](../Page/外丹术.md "wikilink")
  - [炼金术](../Page/炼金术.md "wikilink")
  - [长生不老](../Page/长生不老.md "wikilink")
  - [修道](../Page/修道.md "wikilink")
  - [隱仙派](../Page/隱仙派.md "wikilink")
  - [太極](../Page/太極.md "wikilink")
  - [無極](../Page/無極.md "wikilink")
  - [全真派](../Page/全真派.md "wikilink")
  - [龙门派](../Page/龙门派.md "wikilink")
  - [華山派](../Page/華山派.md "wikilink")
  - [遇仙派](../Page/遇仙派.md "wikilink")
  - [隨山派](../Page/隨山派.md "wikilink")
  - [清靜派](../Page/清靜派.md "wikilink")
  - [崳山派](../Page/崳山派.md "wikilink")
  - [南無派](../Page/南無派.md "wikilink")
  - [伍柳派](../Page/伍柳派.md "wikilink")
  - [天台宗](../Page/天台宗.md "wikilink")
  - [黄帝内经](../Page/黄帝内经.md "wikilink")

{{-}}

[de:Neidan](../Page/de:Neidan.md "wikilink")

[内丹](../Category/内丹.md "wikilink")
[Category:中國思想](../Category/中國思想.md "wikilink")

1.  余國藩著，李奭學譯：《余國藩西遊記論集》（臺北：聯經出版事業公司，1989），頁209-210。
2.  余國藩：《余國藩西遊記論集》，頁206-207。