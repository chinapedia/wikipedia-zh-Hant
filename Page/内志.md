**内志**（，意为“高地”），又称**纳季德**，是涵蓋[阿拉伯半岛地理中心的地區](../Page/阿拉伯半岛.md "wikilink")，位於[沙烏地阿拉伯境內](../Page/沙烏地阿拉伯.md "wikilink")，是該國首都[利雅德的所在地](../Page/利雅德.md "wikilink")，同時也擁有該國近三分之一的人口\[1\]。

## 地理

### 內志的边界

阿拉伯语內志为高地之意，曾经指阿拉伯半岛的多个地区。然而，其中最著名的是中心地区，其西面是[汉志山脉和](../Page/汉志.md "wikilink")[也门山脉](../Page/也门.md "wikilink")，其东面为历史上的[巴林省](../Page/巴林省.md "wikilink")。

### 地形学

內志为一个[高原](../Page/高原.md "wikilink")，其海拔为762米到1525米，从西向东逐渐下降。东部是绿洲居住区，其余地区零星地居住着[游牧的](../Page/游牧民族.md "wikilink")[贝都因人](../Page/贝都因人.md "wikilink")。主要的地形特点包括北部的Aja山脉和Salma山脉。Tweig山脉从北到南穿过內志中部。

## 主要城市

內志最大的城市为[利雅德](../Page/利雅德.md "wikilink")，也是[沙特阿拉伯最大的城市](../Page/沙特阿拉伯.md "wikilink")，2005年人口超过425万。其他主要城市包括Buraydah（505,000人）、[哈伊勒](../Page/哈伊勒.md "wikilink")（267,005人）和Unaizah（128,930人）。

## 参见

  - [汉志](../Page/汉志.md "wikilink")
  - [阿拉伯高原](../Page/阿拉伯高原.md "wikilink")

[Category:高原](../Category/高原.md "wikilink")
[Category:沙特阿拉伯](../Category/沙特阿拉伯.md "wikilink")
[Category:沙地阿拉伯地理](../Category/沙地阿拉伯地理.md "wikilink")

1.