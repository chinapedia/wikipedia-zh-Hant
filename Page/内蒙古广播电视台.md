**内蒙古广播电视台**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[内蒙古自治区的省级国营广播电视机构](../Page/内蒙古自治区.md "wikilink")，由内蒙古人民广播电台和内蒙古电视台合并组建，为自治区政府直属事业单位，接受中共内蒙古自治区党委宣传部领导。

## 历史与发展

[NMTV_logo.png](https://zh.wikipedia.org/wiki/File:NMTV_logo.png "fig:NMTV_logo.png")
1969年10月1日，内蒙古電視台[漢語節目开播](../Page/漢語.md "wikilink")。

1976年10月2日，内蒙古電視台[蒙古语節目开播](../Page/蒙古语.md "wikilink")。

1979年5月1日，内蒙古電視台開始採用[PAL](../Page/PAL.md "wikilink")-D制式播出[彩色電視節目](../Page/彩色電視.md "wikilink")。

1987年，内蒙古彩电中心落成。由于[内蒙古地域宽广](../Page/内蒙古.md "wikilink")，因此其[微波电路传输干支线也是全国最长的微波电路传输干支线之一](../Page/微波.md "wikilink")。

1997年1月1日，内蒙古電視台蒙古语、汉语卫星频道开通。

2001年5月1日，[内蒙古经济电视台](../Page/内蒙古经济电视台.md "wikilink")、[内蒙古有线电视台被并入内蒙古电视台](../Page/内蒙古有线电视台.md "wikilink")。

2016年6月17日，内蒙古人民广播电台和内蒙古电视台合并为内蒙古广播电视台。

## 频道列表

### 电视服务

内蒙古广播电视台的电视服务源于1960年4月成立的**内蒙古电视台**，1969年10月1日试播，现有9个电视频道。

<table style="width:172%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 80%" />
<col style="width: 10%" />
<col style="width: 17%" />
<col style="width: 13%" />
<col style="width: 16%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><strong>頻道名稱</strong></p></td>
<td><p><strong>语言</strong></p></td>
<td><p><strong>广播格式</strong></p></td>
<td><p><strong>啟播時間</strong></p></td>
<td><p><strong>頻道口號</strong></p></td>
<td><p><strong>频道前身</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/内蒙古卫视.md" title="wikilink">汉语卫视</a></p></td>
<td><p>普通話</p></td>
<td><p>SD: PAL 576i 16:9<br />
HD: 1080i</p></td>
<td><p>标清版：1969年10月1日（开播）<br />
1997年1月1日（上星）<br />
高清版：2017年1月</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>蒙古语卫视</p></td>
<td><p><a href="../Page/蒙古语.md" title="wikilink">蒙古语</a></p></td>
<td><p>SD: PAL 576i 16:9<br />
HD: 1080i</p></td>
<td><p>标清版：1976年10月2日（开播）<br />
1997年1月1日（上星）<br />
高清版：2017年1月</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>新闻综合频道</p></td>
<td><p>普通話</p></td>
<td><p>SD: PAL 576i 4:3</p></td>
<td><p>2004年5月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>文体娱乐频道</p></td>
<td><p>普通話</p></td>
<td><p>SD: PAL 576i 4:3</p></td>
<td><p>2000年10月</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>经济生活频道</p></td>
<td><p>普通话</p></td>
<td><p>SD: PAL 576i 4:3</p></td>
<td><p>1999年10月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>农牧频道</p></td>
<td><p>普通話</p></td>
<td><p>SD: PAL 576i 4:3</p></td>
<td><p>2016年3月1日</p></td>
<td></td>
<td><p>内蒙古电视台影视剧频道</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>少儿频道</p></td>
<td><p>普通話、<a href="../Page/蒙古语.md" title="wikilink">蒙古语</a></p></td>
<td><p>SD: PAL 576i 4:3</p></td>
<td><p>2005年1月1日</p></td>
<td></td>
<td><p>前名“科技教育频道”</p></td>
<td><p>2017年6月18日起，星期日/一的节目以蒙语播放。</p></td>
</tr>
<tr class="odd">
<td><p>蒙古语文化频道</p></td>
<td><p><a href="../Page/蒙古语.md" title="wikilink">蒙古语</a></p></td>
<td><p>SD: PAL 576i 4:3</p></td>
<td><p>2012年1月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>三佳购物频道</p></td>
<td><p>普通话</p></td>
<td><p>SD: PAL 576i 4:3</p></td>
<td></td>
<td></td>
<td></td>
<td><p>天津、河北、内蒙古三省市合办</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 广播服务

内蒙古广播电视台的广播服务源于1950年7月1日成立的**内蒙古人民广播电台**，现有8个广播频率。

|              |                                  |                                           |            |          |          |                       |
| ------------ | -------------------------------- | ----------------------------------------- | ---------- | -------- | -------- | --------------------- |
| **頻道名稱**     | **语言**                           | **频道频率**                                  | **啟播時間**   | **頻道口號** | **频道前身** | **備註**                |
| 蒙语新闻综合广播     | [蒙古语](../Page/蒙古语.md "wikilink") | AM1098 AM1458 SW4785 SW7210 SW9750 FM95.9 | 1950年7月1日  |          |          |                       |
| 汉语新闻综合广播     | 普通話                              | AM675 AM765                               | 1950年7月1日  |          |          | 全天大部分时段和新闻广播同步。       |
| 新闻广播         | 普通话                              | FM95.0                                    | 2007年1月1日  |          |          |                       |
| 交通之声         | 普通话                              | FM105.6                                   | 2003年4月1日  |          |          | 自2010年1月1日起开始24小时播音。  |
| 音乐之声         | 普通话                              | FM93.6                                    | 1999年5月1日  |          |          | 自2010年1月1日起开始24小时播音。  |
| 经济生活广播       | 普通话                              | FM101.4                                   |            |          |          |                       |
| 评书曲艺广播       | 普通话                              | FM102.8                                   | 2006年2月14日 |          |          | 自2010年1月1日起开始24小时播音。  |
| 草原之声         | 蒙语                               | FM91.9                                    | 2008年3月1日  |          |          | 对外宣传广播频率，又名“内蒙古对外广播”。 |
| 农村牧区广播“绿野之声” | 普通话                              | 2008年1月1日                                 |            |          |          |                       |
|              |                                  |                                           |            |          |          |                       |

## 参考文献

  -
## 外部链接

  - [腾格里网 - 内蒙古网络广播电视台](http://www.nmtv.cn/)

  -
{{-}}

[Category:中国各省电视台](../Category/中国各省电视台.md "wikilink")
[Category:内蒙古组织](../Category/内蒙古组织.md "wikilink")
[Category:内蒙古文化](../Category/内蒙古文化.md "wikilink")