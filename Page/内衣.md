[Male_model_A_and_B_and_C_02.JPG](https://zh.wikipedia.org/wiki/File:Male_model_A_and_B_and_C_02.JPG "fig:Male_model_A_and_B_and_C_02.JPG")
**內衣**，指穿在其他衣物內的衣服，通常是直接接觸[皮膚](../Page/皮膚.md "wikilink")，是现代人不可少的[服饰之一](../Page/服饰.md "wikilink")。内衣有吸汗、矯型、承托[身體](../Page/身體.md "wikilink")、保暖及不使身體的分泌物废弃物污穢外衣的作用。天气寒冷时，[长内衣还提供了额外保暖的功能](../Page/长内衣.md "wikilink")。内衣有時會被視為[性徵](../Page/性徵.md "wikilink")。某些內衣更含有[宗教意味](../Page/宗教.md "wikilink")。包括[背心](../Page/背心.md "wikilink")、[汗衫](../Page/汗衫.md "wikilink")、[短裤](../Page/短裤.md "wikilink")、[胸罩等](../Page/胸罩.md "wikilink")。T恤衫既适合做内衣也适合做[外衣](../Page/外衣.md "wikilink")。有些材质的内衣也适合做[泳衣或](../Page/泳衣.md "wikilink")[睡衣](../Page/睡衣.md "wikilink")。

内衣分为穿在躯干部位的与穿在腰以下部位的。

現代的[女性內衣包括遮蔽及保護](../Page/女式内衣.md "wikilink")[乳房的](../Page/乳房.md "wikilink")[胸罩](../Page/胸罩.md "wikilink")（又稱胸圍、bra、brassier）、背心、[女式內褲](../Page/女式內褲.md "wikilink")（panties）；[男性内衣包括贴身背心](../Page/男性.md "wikilink")、[三角褲](../Page/三角褲.md "wikilink")（briefs）、[四角裤](../Page/四角裤.md "wikilink")（boxer
briefs）、大短裤（boxer
shorts）等。現代的內衣除了貼身外，更漸漸發展為[功能內衣](../Page/功能內衣.md "wikilink")，除了保護身體外，還有[保暖及](../Page/保暖.md "wikilink")[塑身](../Page/塑身.md "wikilink")/[矯型的功用](../Page/矯型.md "wikilink")。

某些内衣具有使衣着端庄的目的。例如，女性在半透明织物（sheer）做的裙装时里面穿[衬裙](../Page/衬裙.md "wikilink")，在领口较大或者透明的外衣内穿[吊帶背心](../Page/吊帶背心.md "wikilink")，而在容易被吹起的裙装或者较短的[迷你裙里面穿](../Page/迷你裙.md "wikilink")[安全裤](../Page/安全裤.md "wikilink")。不過有些內衣可能反而是為帶來性衝動而穿著，例如及性感内裤等。

## 歷史

内衣在[汉代被称为](../Page/汉朝.md "wikilink")“[抱腹](../Page/抱腹.md "wikilink")”、“[心衣](../Page/心衣.md "wikilink")”，在[三国和](../Page/三国.md "wikilink")[晋朝时称为](../Page/晋朝.md "wikilink")“[福裆](../Page/福裆.md "wikilink")”，在[隋唐称](../Page/隋唐.md "wikilink")“[内中和](../Page/内中.md "wikilink")[亵衣](../Page/亵衣.md "wikilink")”，在[宋代南方叫](../Page/宋朝.md "wikilink")“[袜肚](../Page/袜肚.md "wikilink")”，[元代北方叫做](../Page/元朝.md "wikilink")“[袜胸](../Page/袜胸.md "wikilink")”，在[明代称为](../Page/明朝.md "wikilink")“[谰裙](../Page/谰裙.md "wikilink")”，在[清代则被称为](../Page/清朝.md "wikilink")“[肚兜](../Page/肚兜.md "wikilink")”\[1\]。

## 不穿内衣

某些人可能出于不同原因而不穿内衣。如出于舒适、使其穿着的外衣（特别是贴身风格的外衣）看起来平整没有内裤痕跡（panty line）或者出于性感。

特定类型的衣物如自行车服（cycling
shorts）、[苏格兰裙](../Page/苏格兰裙.md "wikilink")，[睡衣](../Page/睡衣.md "wikilink")、[泳衣里面一般不穿内衣](../Page/泳衣.md "wikilink")。

## 类型和风格

现代内衣常见的类型与风格列表：

<table style="width:10%;">
<colgroup>
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 4%" />
<col style="width: 2%" />
</colgroup>
<thead>
<tr class="header">
<th><p>类型</p></th>
<th><p>别名</p></th>
<th><p>信息</p></th>
<th><p>变种</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>两性均可穿着</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>全身</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/长内衣.md" title="wikilink">长内衣</a></strong></p>
<p><a href="https://zh.wikipedia.org/wiki/File:LongJohns-20050126.jpg" title="fig:LongJohns-20050126.jpg">LongJohns-20050126.jpg</a></p></td>
<td><p>Long johns, long handles，针织长袖内衣（专业名称）、针织长内裤（专业名称）、秋衣（适用于中国大部），线衣（适用于中国东北）</p></td>
<td><p>两件装的内衣，用于在寒冷天气穿着。上衣袖子有到腕部，下身有裤腿到踝部。</p></td>
<td><ul>
<li>保暖内衣（Thermal underwear）</li>
</ul></td>
</tr>
<tr class="even">
<td><p><strong>上半身</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/背心.md" title="wikilink">背心</a></strong></p>
<p><a href="https://zh.wikipedia.org/wiki/File:Ashirtexample.jpg" title="fig:Ashirtexample.jpg">Ashirtexample.jpg</a></p></td>
<td><p>Tank top, wife beater, singlet</p></td>
<td><p>无领无袖的贴身上衣</p></td>
<td><ul>
<li>A-shirt（<strong>UK</strong>: vest）–即跨栏背心。</li>
</ul></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/T恤衫.md" title="wikilink">T恤衫</a></strong></p>
<p><a href="https://zh.wikipedia.org/wiki/File:T-shirt-black.jpg" title="fig:T-shirt-black.jpg">T-shirt-black.jpg</a></p></td>
<td><p>Tee</p></td>
<td><p>上衣，通常没有口袋，长袖或更常为短袖。套头穿脱。典型的T恤衫是没有领子没有扣子的。但现在也有的具有领子与领扣。</p></td>
<td><ul>
<li>圆领T恤衫</li>
<li>V字领T恤衫</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><strong>下半身</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/比基尼泳衣.md" title="wikilink">比基尼</a></strong></p>
<p><a href="https://zh.wikipedia.org/wiki/File:BikiniBottom-red-20030625.jpg" title="fig:BikiniBottom-red-20030625.jpg">BikiniBottom-red-20030625.jpg</a></p></td>
<td></td>
<td><p>通常内裤的腰带低于人的腰部，内裤侧部不超过腹股沟。男士比基尼短裤没有前开口（fly）。</p></td>
<td><ul>
<li>High-sided bikini underwear</li>
<li>Low-sided bikini underwear</li>
<li><a href="../Page/比基尼泳衣#主要款式.md" title="wikilink">细带比基尼</a> –前后各一块三角形布片，由细绳连接。</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/G弦裤.md" title="wikilink">G弦裤</a></strong></p>
<p><a href="https://zh.wikipedia.org/wiki/File:Underwear_-_triangle_back,_strap_sides.png" title="fig:Underwear_-_triangle_back,_strap_sides.png">Underwear_-_triangle_back,_strap_sides.png</a></p></td>
<td><p>Gee-string, gee string</p></td>
<td><p>属于thong内裤的一种。由两块布片组成，前面的布片较大，后部臀上的布片小，前后布局形似字母G。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/T字裤.md" title="wikilink">T字裤</a></strong></p>
<p><a href="https://zh.wikipedia.org/wiki/File:Underwear_-_string_back,_tie_sides.png" title="fig:Underwear_-_string_back,_tie_sides.png">Underwear_-_string_back,_tie_sides.png</a></p></td>
<td><p>Tee-string, tee string</p></td>
<td><p>属于thong内裤的一种。系带。后部形似字母T</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/V字裤.md" title="wikilink">V字裤</a></strong></p>
<p><a href="https://zh.wikipedia.org/wiki/File:Underwear_-_V_back,_strap_sides.png" title="fig:Underwear_-_V_back,_strap_sides.png">Underwear_-_V_back,_strap_sides.png</a></p></td>
<td><p>Vee-string, Vee string</p></td>
<td><p>属于thong内裤的一种。后部形似字母V</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/C字裤.md" title="wikilink">C字裤</a></strong></p>
<p><a href="https://zh.wikipedia.org/wiki/File:CStringIllustration.svg" title="fig:CStringIllustration.svg">CStringIllustration.svg</a></p></td>
<td></td>
<td><p>属于thong内裤的一种。无腰带。前后布局形似字母C。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>女式内衣</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>上半身</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/胸罩.md" title="wikilink">胸罩</a></strong></p>
<p><a href="https://zh.wikipedia.org/wiki/File:Brassiere-white.jpg" title="fig:Brassiere-white.jpg">Brassiere-white.jpg</a></p></td>
<td><p>Bra，Brassiere</p></td>
<td></td>
<td><ul>
<li><a href="../Page/胸圍設計款式列表.md" title="wikilink">Balconette bra</a></li>
<li>Demi bra</li>
<li>Padded bra</li>
<li><a href="../Page/Push-up_bra.md" title="wikilink">Push-up bra</a></li>
<li><a href="../Page/运动胸罩.md" title="wikilink">运动胸罩</a></li>
<li><a href="../Page/无肩带胸罩.md" title="wikilink">无肩带胸罩</a></li>
<li>T恤胸罩</li>
</ul></td>
</tr>
<tr class="even">
<td><p><strong>下半身</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/平腳褲.md" title="wikilink">平腳褲</a></strong></p>
<p><a href="https://zh.wikipedia.org/wiki/File:Boyshorts_from_rear.jpg" title="fig:Boyshorts_from_rear.jpg">Boyshorts_from_rear.jpg</a></p></td>
<td><p>Booty shorts, boyleg briefs, boy short panties, boys' cut shorts, boyshorts, shorties</p></td>
<td><p>类似于男士四角短裤，但超出腹股沟的部分很短。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/女式内裤.md" title="wikilink">女式内裤</a></strong></p>
<p><a href="https://zh.wikipedia.org/wiki/File:Panties-white-2007.jpg" title="fig:Panties-white-2007.jpg">Panties-white-2007.jpg</a></p></td>
<td><p>Panties，<strong>UK</strong>：<a href="../Page/Knickers.md" title="wikilink">Knickers</a></p></td>
<td><p>通常具有弹性的腰带，会阴部由吸收材质如棉布制成，通常很短没有超出腹股沟的腿的部分。</p></td>
<td><ul>
<li>Control panties –由弹性材料制造，覆盖腰部用于使腰部看起来更苗条。</li>
<li>High cut（French cut）panties.</li>
<li>Hipsters –穿着更低，用腰绳环绕臀部。</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><strong>男士内衣</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>下半身</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/四角裤.md" title="wikilink">四角裤</a></strong></p>
<p><a href="https://zh.wikipedia.org/wiki/File:Menboxer.jpg" title="fig:Menboxer.jpg">Menboxer.jpg</a></p></td>
<td><p>boxer briefs, Trunks</p></td>
<td><p>比大短裤（boxer shorts）更短更贴身。</p></td>
<td><ul>
<li>运动型四角内裤（Athletic-style boxer briefs）–类似于自行车服（bike shorts），非常贴身，通常没有前开口（fly）。</li>
<li>囊袋四角内裤–具有囊袋，可以把<a href="../Page/阴囊.md" title="wikilink">阴囊</a>、<a href="../Page/阴茎.md" title="wikilink">阴茎放入</a>，具有固定、保护、运动作用，以及治疗功能（对<a href="../Page/精索静脉曲张.md" title="wikilink">精索静脉曲张患者</a>）。</li>
</ul></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/大短裤.md" title="wikilink">大短裤</a></strong></p>
<p><a href="https://zh.wikipedia.org/wiki/File:BoxerShorts-20070901.jpg" title="fig:BoxerShorts-20070901.jpg">BoxerShorts-20070901.jpg</a></p></td>
<td><p>Boxers, Boxer shorts</p></td>
<td><p>具有松紧功能的腰带，腰带比一般内裤宽。裤腿至少到大腿中部。具有前开口。外观的花色图案鲜艳、复杂。</p></td>
<td><ul>
<li>Knit boxers</li>
<li>Woven boxers</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/三角裤.md" title="wikilink">三角裤</a></strong></p>
<p><a href="https://zh.wikipedia.org/wiki/File:Ice_blue_fbriefs.jpg" title="fig:Ice_blue_fbriefs.jpg">Ice_blue_fbriefs.jpg</a></p></td>
<td><p>Briefs，<strong>UK</strong>: <a href="../Page/wiktionary:Y-fronts.md" title="wikilink">Y-fronts</a><br />
<strong>US</strong>: jockey shorts, jockeys</p></td>
<td><p>不超过腹股沟，因此没有裤腿。</p></td>
<td><ul>
<li>Traditional briefs – these have a Y-shaped fly.</li>
<li>Diagonal-flap briefs.</li>
<li>Double-seat or double-back briefs.</li>
<li>Low-cut or low-rise briefs</li>
<li>囊袋性三角内裤。</li>
</ul></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/护身三角内裤.md" title="wikilink">护身三角内裤</a></strong></p>
<p><a href="https://zh.wikipedia.org/wiki/File:Jockstrap_Front,_Side_and_Rear.jpg" title="fig:Jockstrap_Front,_Side_and_Rear.jpg">Jockstrap_Front,_Side_and_Rear.jpg</a></p></td>
<td><p>Athletic supporter, jock, strap, supporter</p></td>
<td><p>防止运动损伤</p></td>
<td><ul>
<li>Strapless pouch – consists of a support pouch for the genitals and a waistband, with no securing straps.</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><strong>宗教用途内衣</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>全身</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>s</p>
<p><a href="https://zh.wikipedia.org/wiki/File:Garment.jpg" title="fig:Garment.jpg">Garment.jpg</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>上半身</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>下半身</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Kaccha.JPG" title="fig:Kaccha.JPG">Kaccha.JPG</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考文獻

<small>

<references />

</small>

## 相關條目

  - [內衣外穿](../Page/內衣外穿.md "wikilink")

[内衣](../Category/内衣.md "wikilink")

1.