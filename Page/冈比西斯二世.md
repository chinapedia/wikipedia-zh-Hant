**冈比西斯二世**（[古波斯楔形文字](../Page/古波斯楔形文字.md "wikilink")：
\[1\]；[现代波斯语](../Page/现代波斯语.md "wikilink")：；；）是[波斯](../Page/波斯.md "wikilink")[阿契美尼德王朝的国王](../Page/阿契美尼德王朝.md "wikilink")[居鲁士大帝](../Page/居鲁士大帝.md "wikilink")（前529年—前522年3月在位）的儿子。

前539年[居鲁士大帝擊敗巴比倫王](../Page/居鲁士大帝.md "wikilink")[拿波尼度](../Page/拿波尼度.md "wikilink")，成功奪取巴比倫，岡比西斯大约于前537年在[巴比伦担任居鲁士的全权代理人](../Page/巴比伦.md "wikilink")，前522年[居鲁士大帝戰死後繼承其王位](../Page/居鲁士大帝.md "wikilink")，前525年他率波斯军队入侵[埃及](../Page/埃及.md "wikilink")。[昔兰尼](../Page/昔兰尼.md "wikilink")（希腊在北非的殖民地）和[利比亚自愿臣服冈比西斯](../Page/利比亚.md "wikilink")；埃及在其军队于贝鲁西亚为冈比西斯击败后投降，[法老](../Page/法老.md "wikilink")[普萨美提克三世被俘](../Page/普萨美提克三世.md "wikilink")。前525年5月25日，冈比西斯成为埃及统治者，建立[埃及第二十七王朝](../Page/埃及第二十七王朝.md "wikilink")（波斯第一王朝）。他使用“埃及皇帝，诸国皇帝”为称号。

前524年冈比西斯二世入侵古实（即[努比亚](../Page/努比亚.md "wikilink")）受挫，同年在埃及发生反对波斯统治的暴乱。冈比西斯二世镇压暴乱后又策划征服古实，然而这一计划为波斯本土发生的[高墨塔](../Page/高墨塔.md "wikilink")（伪斯梅尔迪士或伪巴尔迪亚）起义所打断。冈比西斯遂挥师回国。但在途中逝世。

## 脚注

## 外部链接

### 文学

  - Ebers, Georg. [*An Egyptian
    Princess*](http://www.gutenberg.org/etext/5460) 1864. (English
    translation of *Eine ägyptische Königstochter*) at [Project
    Gutenberg](../Page/Project_Gutenberg.md "wikilink").
  - Preston, Thomas.
    [*Cambises*](http://www.umm.maine.edu/faculty/necastro/drama/cambises.txt)
    1667. Plaintext ed. Gerard NeCastro (closer to original spelling) in
    his collection [Medieval and Renaissance
    Drama](http://www.umm.maine.edu/faculty/necastro/drama/).

[Category:法老](../Category/法老.md "wikilink")
[Category:波斯国王](../Category/波斯国王.md "wikilink")

1.