**岡特的約翰**（John of
Gaunt，），是[英格蘭國王](../Page/英格蘭.md "wikilink")[愛德華三世的兒子](../Page/愛德華三世.md "wikilink")，[理查二世的叔叔](../Page/理查二世_\(英格兰\).md "wikilink")，因為侄子年幼，故此在1377年－1399年間代他治理國家。约翰因娶了兰开斯特伯爵的女儿布兰奇而成为兰开斯特公爵，而他的第二任妻子康斯坦萨是[卡斯蒂利亚暴君](../Page/卡斯蒂利亚.md "wikilink")[佩德罗的女儿](../Page/佩德羅一世_\(卡斯蒂利亞\).md "wikilink")，他因此要求卡斯蒂利亚的王位，但未能战胜竞争对手[恩里克二世](../Page/恩里克二世_\(卡斯蒂利亚\).md "wikilink")\[1\]。

约翰死后，理查没收了兰开斯特家的庄园，但约翰的长子亨利于当年兵不血刃夺取了理查的王位，成为[亨利四世](../Page/亨利四世_\(英格兰\).md "wikilink")，建立了[兰开斯特王朝](../Page/兰开斯特王朝.md "wikilink")。[都铎王朝的创始人](../Page/都铎王朝.md "wikilink")[亨利七世则是他第三任妻子](../Page/亨利七世_\(英格兰\).md "wikilink")[凯瑟琳·斯温福德所生的儿子](../Page/凯瑟琳·斯温福德.md "wikilink")[约翰·博福特的后代](../Page/约翰·博福特，第一代索默塞特伯爵.md "wikilink")。

## 家庭

### 祖先

<center>

</center>

## 参考文献

[Category:英国人](../Category/英国人.md "wikilink")

1.