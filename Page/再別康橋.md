[Kings_College_Xu_Zhimo_memorial.jpg](https://zh.wikipedia.org/wiki/File:Kings_College_Xu_Zhimo_memorial.jpg "fig:Kings_College_Xu_Zhimo_memorial.jpg")，紀念[徐志摩](../Page/徐志摩.md "wikilink")《再別康橋》一詩的石头\]\]
《**再別康橋**》是[中國近代](../Page/中國.md "wikilink")[詩人](../Page/詩人.md "wikilink")[徐志摩膾炙人口的](../Page/徐志摩.md "wikilink")[新詩](../Page/新詩.md "wikilink")。1928年秋天，作者最後一次重訪[英國](../Page/英國.md "wikilink")[劍橋](../Page/劍橋.md "wikilink")（舊譯**康橋**），乘船返回[中國](../Page/中國.md "wikilink")，途經中國[南海時](../Page/南海.md "wikilink")，把劍橋的景色和依恋之情融入詩中，表達告別劍橋的淡淡哀愁。\[1\]

該作品在1928年11月6日完成，同年12月10日刊於《[新月](../Page/新月.md "wikilink")》月刊第1-{卷}-第10號，后收入《[猛虎集](../Page/猛虎集.md "wikilink")》。自該新詩出版後，詩被多次譜上樂曲，詩中一句「輕輕的我走了，正如我輕輕的來」也成為中國傳誦一時的名句。

## 背景

[徐志摩一生曾與多間](../Page/徐志摩.md "wikilink")[大學結緣](../Page/大學.md "wikilink")，1915年考入[上海](../Page/上海.md "wikilink")[滬江大學](../Page/滬江大學.md "wikilink")，其後轉讀國立[北洋大學](../Page/北洋大學.md "wikilink")（今[天津大學](../Page/天津大學.md "wikilink")）、國立[北京大學](../Page/北京大學.md "wikilink")（今[北京大學](../Page/北京大學.md "wikilink")）、[美國](../Page/美國.md "wikilink")[克拉克大學](../Page/克拉克大學.md "wikilink")、[哥倫比亞大學](../Page/哥倫比亞大學.md "wikilink")、[英國](../Page/英國.md "wikilink")[倫敦大學](../Page/倫敦大學.md "wikilink")，但直至1922年3月，他與[張幼儀離婚後](../Page/張幼儀.md "wikilink")，轉入[劍橋大學國王學院供讀](../Page/劍橋大學國王學院.md "wikilink")（旁聽）七個月，並结識林徽音及其父親，這一短暫經歷對徐志摩卻影響至深。

他曾說離[美國後](../Page/美國.md "wikilink")，仍一如草包，但住在[劍橋時](../Page/劍橋.md "wikilink")，每天忙著散步、划船、騎自轉車、抽煙、閒談、吃五點鐘[茶](../Page/茶.md "wikilink")、嚐油烤餅、看閒書，回國時發現自己原先只是一肚子顢頇。他在《吸煙與文化》\[2\]
一文曾說：「我的眼是康橋教我睜的，我的求知欲是康橋給我撥動的，我的自我的意識是康橋給我胚胎的。」

劍橋的一景一物，自此成為徐志摩的創作靈感泉源。1922年，徐志摩從劍橋留學歸[中國後](../Page/中國.md "wikilink")，曾作[新詩](../Page/新詩.md "wikilink")《康橋，再會罷！》；1926年，徐志摩再度抵達英國，發表散文《我所知道的康橋》；而1928年他第三次遊歷劍橋後，寫成《再別康橋》，這亦是徐志摩短暫人生中最後一次到訪劍橋，作品完成後3年，他在飛機意外中罹難身亡。

他在撰寫《再別康橋》後似乎意識到那次「再別」，其實是一次訣別。他說：「這是我第三次對康橋表達最深切最難捨離的感情，或許未來的時日裡，她只能永存在我的內心深處了！」他夫子自道，離開劍橋時，雖然仍聽機械的轟鳴和看見不少高樓大廈，但這首詩是特意向雲彩、金柳、柔波、青草和星輝作告別，跳出人與人之間離別的俗套，減少「傷離別」的味道。而全詩中所指的「輕輕」、「悄悄」、「沉默」是要營造寂然無聲的氣氛，減少沉重感，而多一點飄逸。

## 後世引用

  - 此诗作被[人民教育出版社收录在](../Page/人民教育出版社.md "wikilink")《[普通高中课程标准实验教科书·语文·必修1](../Page/普通高中课程标准实验教科书.md "wikilink")》课本中，排序仅次于[毛泽东的](../Page/毛泽东.md "wikilink")《[沁园春·长沙](../Page/沁园春·长沙.md "wikilink")》及[戴望舒的](../Page/戴望舒.md "wikilink")《[雨巷](../Page/雨巷.md "wikilink")》。
  - 此詩作是[香港中學會考中文科舊課程](../Page/香港中學會考.md "wikilink")（1993-2006年期間採用）的指定課文（俗稱範文），公開試試題可能會問及此詩，學生須討論和評價其詩句。
  - [中華民國教育部亦把此詩列為中學](../Page/中華民國教育部.md "wikilink")[國文課本的一部分](../Page/國文.md "wikilink")，收錄在[台灣](../Page/台灣.md "wikilink")[龍騰版高中國文課本第一冊](../Page/龍騰文化.md "wikilink")\[3\]，故詩句在台灣廣為傳誦。
  - 《金韻獎第一集》中收錄改編自此詩的同名歌曲，由李達濤作曲、范廣惠（本名范廣慧）演唱。
  - 台灣專輯《留聲1曠野寄情》中收錄此曲，由[張清芳演唱](../Page/張清芳.md "wikilink")。
  - [蔡琴將此曲收錄於個人專輯](../Page/蔡琴.md "wikilink")《愛像一首歌》中。
  - [萬芳將此歌收錄在](../Page/萬芳.md "wikilink")《林萬芳歌本》中。
  - 填詞人[林振強曾把此詩改編為](../Page/林振強.md "wikilink")[粵語版](../Page/粵語.md "wikilink")，名為《劍橋拜拜》。
  - 女子組合[S.H.E專輯](../Page/S.H.E.md "wikilink")《Play》裡的同名歌曲，歌詞內容取材自[徐志摩的悽美愛情故事](../Page/徐志摩.md "wikilink")，同時回應《再別康橋》原詩。
  - [林宥嘉的專輯](../Page/林宥嘉.md "wikilink")《神秘嘉賓》裡亦收錄同名歌曲，用音樂把這首詩重新演繹。
  - 魔術師[黃柏翰將此詩意境融入其舞台程序](../Page/黃柏翰.md "wikilink")《再別康橋》，並拿下多個獎項，且以此程序代表台灣參加[FISM大賽](../Page/FISM.md "wikilink")。

## 參考

<references />

[Category:中华民国大陆时期现代诗作品](../Category/中华民国大陆时期现代诗作品.md "wikilink")
[Category:英國背景作品](../Category/英國背景作品.md "wikilink")
[Category:1928年作品](../Category/1928年作品.md "wikilink")

1.  詩中的「康橋」即「劍橋」早年的[音譯](../Page/音譯.md "wikilink")。
2.  [徐志摩生平與相關詩作文摘（Powerpoint檔）](http://space.ntcnc.edu.tw/teacher/tww/%E5%86%8D%20%E5%88%A5%20%E5%BA%B7%20%E6%A9%8B%20%20%E5%BE%90%E5%BF%97%E6%91%A9%201928.ppt)

3.