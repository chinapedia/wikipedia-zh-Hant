《**再見亦是老婆**》（又名《**都是有情人**》；[英文](../Page/英文.md "wikilink")：），[香港](../Page/香港.md "wikilink")[無綫電視](../Page/無綫電視.md "wikilink")[翡翠台時裝](../Page/翡翠台.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")，於1994年10月17日至11月11日期間首播，共20集，由[姜大衛及](../Page/姜大衛.md "wikilink")[陳秀雯領衍主演](../Page/陳秀雯.md "wikilink")，並由[周嘉玲](../Page/周嘉玲.md "wikilink")、[鄧萃雯](../Page/鄧萃雯.md "wikilink")、[陳啟泰以及](../Page/陳啟泰.md "wikilink")[林　偉聯合主演](../Page/林偉.md "wikilink")，監製為[李國立](../Page/李國立.md "wikilink")。由於劇情有關當年流行的婚外情話題，此劇甫播出隨即大受歡迎，引起社會熱話，並挽回[無綫電視當時持續低迷的收視](../Page/無綫電視.md "wikilink")，成為當年最高收視港劇。

該劇以細膩手法刻劃夫婦家庭生活，感情親切，故事寫實引起觀眾共鳴，女主角[陳秀雯當年憑](../Page/陳秀雯.md "wikilink")"肥師奶"一角大受歡迎，攀上事業顛峰。該劇講述袁珮珮和丈夫-{于禮和}-原本家庭美滿，育有一對子女。但丈夫被美艷女星卓羚所吸引而拋棄了家庭，最後經歷了各種變故後挽回了夫婦感情。

## 演員表

### 主要演員

  - [姜大偉](../Page/姜大偉.md "wikilink") 飾 -{于}-禮和
  - [陳秀雯](../Page/陳秀雯.md "wikilink") 飾 袁珮珮（-{于}-禮和之妻子）
  - [周嘉玲](../Page/周嘉玲.md "wikilink") 飾 卓　羚（-{于}-禮和的新歡）
  - [鄧萃雯](../Page/鄧萃雯.md "wikilink") 飾 湯敏蓉
  - [林　偉](../Page/林偉_\(演員\).md "wikilink") 飾 洪世亮
  - [陳啟泰](../Page/陳啟泰.md "wikilink") 飾 馬子儒

### 其他演員

  - [陳詠雯](../Page/陳詠雯.md "wikilink") 飾 袁珮珮（童年）
  - [王顗婷](../Page/王顗婷.md "wikilink") 飾 湯敏蓉（童年）
  - [麥皓為](../Page/麥皓為.md "wikilink") 飾 袁珮珮父親
  - [張間森](../Page/張間森.md "wikilink") 飾 袁珮珮母親
  - [黎　宣](../Page/黎宣.md "wikilink") 飾 簡小妹
  - [林芷筠](../Page/林芷筠.md "wikilink") 飾 -{于仲文}-
  - [謝子峰](../Page/謝子峰.md "wikilink") 飾 -{于仲言}-
  - [許紹雄](../Page/許紹雄.md "wikilink") 飾 張樹仁
  - [羅雪玲](../Page/羅雪玲.md "wikilink") 飾 Debby
  - [林嘉麗](../Page/林嘉麗.md "wikilink") 飾 Ruby
  - [陳燕航](../Page/陳燕航.md "wikilink") 飾 Maggie
  - [麥嘉倫](../Page/麥嘉倫.md "wikilink") 飾 Peter
  - [黃啟德](../Page/黃啟德.md "wikilink") 飾 Paul
  - [黃天鐸](../Page/黃天鐸.md "wikilink") 飾 基
  - [蔡易思](../Page/蔡易思.md "wikilink") 飾 豆　豆
  - [劉兆銘](../Page/劉兆銘.md "wikilink") 飾 董立光
  - [李龍基](../Page/李龍基.md "wikilink") 飾 洪有康
  - [謝文軒](../Page/謝文軒.md "wikilink") 飾 洪　日
  - [夏　萍](../Page/夏萍.md "wikilink") 飾 陸　姨
  - [馬海倫](../Page/馬海倫.md "wikilink") 飾 胡梁慧芬
  - [張英才](../Page/張英才.md "wikilink") 飾 岳　叔
  - [戴志偉](../Page/戴志偉.md "wikilink") 飾 C.Y.Tam
  - [李麗麗](../Page/李麗麗.md "wikilink") 飾 May
  - [楊得時](../Page/楊得時.md "wikilink") 飾 歐陽滔
  - [凌　漢](../Page/凌漢.md "wikilink") 飾 老　漢
  - [戴少民](../Page/戴少民.md "wikilink") 飾 蓉同事
  - [亦　羚飾](../Page/亦羚.md "wikilink") Shirley
  - [王翠玉](../Page/王翠玉.md "wikilink") 飾 Benita
  - [容家麗](../Page/容家麗.md "wikilink") 飾 蓉同事
  - [蔡子健](../Page/蔡子健.md "wikilink") 飾 洪文揚
  - [黃文標](../Page/黃文標.md "wikilink") 飾 孖　T
  - [游　飇](../Page/游飇.md "wikilink") 飾 阿　友
  - [張慧儀](../Page/張慧儀.md "wikilink") 飾 歐陽滔太太（Veronica）
  - [黃清榕](../Page/黃清榕.md "wikilink") 飾 胡助手
  - [林家棟](../Page/林家棟.md "wikilink") 飾 Ringo
  - [馬德鐘](../Page/馬德鐘.md "wikilink") 飾 高　俊（Morgan）
  - [溫雙燕](../Page/溫雙燕.md "wikilink") 飾 女演員
  - [廖啟智](../Page/廖啟智.md "wikilink") 飾 馬如龍
  - [馮瑞珍](../Page/馮瑞珍.md "wikilink") 飾 十二家
  - [林尚武](../Page/林尚武.md "wikilink") 飾 胡　生
  - [麥家倫](../Page/麥家倫.md "wikilink") 飾 Jackson
  - [何穗瑩](../Page/何穗瑩.md "wikilink") 飾 海皮（D.J.）
  - [廖麗麗](../Page/廖麗麗.md "wikilink") 飾 師　奶
  - [區　嶽](../Page/區嶽.md "wikilink") 飾 管理員
  - [陸麗燕](../Page/陸麗燕.md "wikilink") 飾 護　士
  - [簡俊然](../Page/簡俊然.md "wikilink") 飾 精神病男子
  - [湯俊明](../Page/湯俊明.md "wikilink") 飾 大漢甲
  - [朱偉達](../Page/朱偉達.md "wikilink") 飾 大漢乙
  - [何美好](../Page/何美好.md "wikilink") 飾 新　抱
  - [林文森](../Page/林文森.md "wikilink") 飾 Anna
  - [沈寶思](../Page/沈寶思.md "wikilink") 飾 Ray
  - [尹健榮](../Page/尹健榮.md "wikilink") 飾 Leon
  - [曾令茵](../Page/曾令茵.md "wikilink") 飾 May
  - [吳列華](../Page/吳列華.md "wikilink") 飾 Maggie
  - [羅　維](../Page/羅維.md "wikilink") 飾 Richard
  - [談佩珊](../Page/談佩珊.md "wikilink") 飾 珠
  - [-{石　云}-](../Page/石云.md "wikilink") 飾 彪
  - [方　傑](../Page/方傑_\(演員\).md "wikilink") 飾 會計師Simon
  - [譚淑梅](../Page/譚淑梅.md "wikilink") 飾 Janet
  - [王啟德](../Page/王啟德.md "wikilink") 飾 Peter
  - \-{[黃鳳琼](../Page/黃鳳琼.md "wikilink")}- 飾 Betty
  - [蘇恩磁](../Page/蘇恩磁.md "wikilink") 飾 梁姑娘
  - [劉美珊](../Page/劉美珊.md "wikilink") 飾 演　員
  - [梁舜燕](../Page/梁舜燕.md "wikilink") 飾 儒　母
  - [文潔雲](../Page/文潔雲.md "wikilink") 飾 儒　姊
  - [陳　銳](../Page/陳銳.md "wikilink") 飾 Annie
  - [鄧汝超](../Page/鄧汝超.md "wikilink") 飾 程律師
  - [蕭玉燕](../Page/蕭玉燕.md "wikilink") 飾 顧　盈
  - [梁建平](../Page/梁建平.md "wikilink") 飾 導　演
  - [黃成想](../Page/黃成想.md "wikilink") 飾 老　板
  - [王偉樑](../Page/王偉樑.md "wikilink") 飾 護衛員
  - [伍文生](../Page/伍文生.md "wikilink") 飾 CID
  - [郭城俊](../Page/郭城俊.md "wikilink") 飾 CID
  - [陳曉雲](../Page/陳曉雲.md "wikilink") 飾 Sales Girl
  - [陳寶靈](../Page/陳寶靈.md "wikilink") 飾 芳　芳
  - [曹　濟](../Page/曹濟.md "wikilink") 飾 工　友
  - [黃煒林](../Page/黃煒林.md "wikilink") 飾 工　友
  - [黃仲匡](../Page/黃仲匡.md "wikilink") 飾 工　友
  - [李綺虹](../Page/李綺虹.md "wikilink") 飾 美　女

## 收視

以下為該劇於[香港無綫電視](../Page/香港無綫電視.md "wikilink")[翡翠台之收視紀錄](../Page/翡翠台.md "wikilink")：

|        |        |                    |                                  |                                       |
| ------ | ------ | ------------------ | -------------------------------- | ------------------------------------- |
| **週次** | **集數** | **日期**             | **平均收視**                         | **备注**                                |
| 1      | 01-05  | 1994年10月17日-10月21日 | 33[點](../Page/收視點.md "wikilink") |                                       |
| 2      | 06-09  | 1994年10月24日-10月28日 | 35[點](../Page/收視點.md "wikilink") |                                       |
| 3      | 10-14  | 1994年10月31日-11月4日  | 35[點](../Page/收視點.md "wikilink") |                                       |
| 4      | 15-20  | 1994年11月7日-11月11日  | 38[點](../Page/收視點.md "wikilink") | 最高收視達43[點](../Page/收視點.md "wikilink") |

  - 本劇平均收視為35[點](../Page/收視點.md "wikilink")。

## 外部連結

  - [無綫電視官方網頁 -
    再見亦是老婆](https://web.archive.org/web/20051107085443/http://tvcity.tvb.com/drama/fate/index.htm)
  - [《再見亦是老婆》 GOTV
    第1集重溫](https://web.archive.org/web/20140222162629/http://gotv.tvb.com/programme/103488/160243/)

[Category:1994年無綫電視劇集](../Category/1994年無綫電視劇集.md "wikilink")
[Category:無綫電視1990年代背景劇集](../Category/無綫電視1990年代背景劇集.md "wikilink")
[Category:外遇題材電視劇](../Category/外遇題材電視劇.md "wikilink")