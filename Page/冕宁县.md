[Voa-chinese_changzheng_yi_minority_yihai_painting_19may10_300.jpg](https://zh.wikipedia.org/wiki/File:Voa-chinese_changzheng_yi_minority_yihai_painting_19may10_300.jpg "fig:Voa-chinese_changzheng_yi_minority_yihai_painting_19may10_300.jpg")
**冕宁县**（）是[中國](../Page/中國.md "wikilink")[四川省](../Page/四川省.md "wikilink")[凉山彝族自治州北部的一个](../Page/凉山彝族自治州.md "wikilink")[县](../Page/县.md "wikilink")。主要有[汉族](../Page/汉族.md "wikilink")、[彝族](../Page/彝族.md "wikilink")、[藏族](../Page/藏族.md "wikilink")、
[回族四种世居民族](../Page/回族.md "wikilink")。

[西昌卫星发射中心](../Page/西昌卫星发射中心.md "wikilink")、[锦屏一级水电站座落于境内](../Page/锦屏一级水电站.md "wikilink")，境内主要河流是[雅砻江及其支流](../Page/雅砻江.md "wikilink")[安宁河](../Page/安宁河.md "wikilink")。当年[红军](../Page/红军.md "wikilink")[长征时的](../Page/长征.md "wikilink")“[彝海结盟](../Page/彝海结盟.md "wikilink")”就发生在此地。

## 历史沿革

[西汉](../Page/西汉.md "wikilink")[元鼎六年](../Page/元鼎.md "wikilink")（前111年）置笮秦、台登县；[明初改置宁潘卫](../Page/明.md "wikilink")；[清](../Page/清.md "wikilink")[雍正六年](../Page/雍正.md "wikilink")（1729年）改为冕宁县。

## 行政区划

冕宁县辖6个镇、31个乡、1个民族乡：

  - 镇：城厢镇、漫水湾镇、大桥镇、复兴镇、泸沽镇、沙坝镇。
  - 乡：回坪乡、回龙乡、哈哈乡、森荣乡、林里乡、石龙乡、铁厂乡、河边乡、河里乡、冶勒乡、拖乌乡、彝海乡、曹古乡、惠安乡、后山乡、宏模乡、先锋乡、泽远乡、里庄乡、金林乡、腊窝乡、联合乡、麦地沟乡、锦屏乡、南河乡、青纳乡、棉沙湾乡、马头乡、窝堡乡、新兴乡、健美乡。
  - 民族乡：和爱藏族乡。

[冕宁县](../Category/冕宁县.md "wikilink")
[Category:凉山县份](../Category/凉山县份.md "wikilink")
[Category:四川省县份](../Category/四川省县份.md "wikilink")