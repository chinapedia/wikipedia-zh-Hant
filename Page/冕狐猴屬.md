**冕狐猴屬**（学名 *Propithecus*）
是[大狐猴科的一属](../Page/大狐猴科.md "wikilink")，与其他狐猴一样，它们也只产于[马达加斯加](../Page/马达加斯加.md "wikilink")。

<div style="padding:5px; margin:10px; align:left; width:525px;">

[Genus_Propithecus_Feet.jpg](https://zh.wikipedia.org/wiki/File:Genus_Propithecus_Feet.jpg "fig:Genus_Propithecus_Feet.jpg")

</div>

[Category:大狐猴科](../Category/大狐猴科.md "wikilink")