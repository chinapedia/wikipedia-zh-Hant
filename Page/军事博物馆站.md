**军事博物馆站**是[北京地铁](../Page/北京地铁.md "wikilink")[1号线和](../Page/北京地铁1号线.md "wikilink")[9号线的一座换乘车站](../Page/北京地铁9号线.md "wikilink")。在1号线中的车站编号是111。1号线军事博物馆站在1969年10月1日随1号线一期建成通车。2012年8月15日9号线军事博物馆站主体完工，受施工进度影响，9号线段军事博物馆站未在2012年年底随9号线北段一同开通。\[1\]
从2013年12月21日起，本站9号线站台以及换乘通道才开始正式启用。\[2\] 在此之前，9号线列车通过不停车。

## 位置

军事博物馆站位于[中国人民革命军事博物馆南侧](../Page/中国人民革命军事博物馆.md "wikilink")。1号线车站沿[复兴路呈东西向布置](../Page/复兴路.md "wikilink")，9号线车站在1号线车站西端，斜跨复兴路与[军博西路](../Page/军博西路.md "wikilink")–[羊坊店路路口](../Page/羊坊店路.md "wikilink")，大致呈南北向布置，两线车站呈“T”形交叉。

这个站周边有中国人民革命军事博物馆、中国有色工程设计研究院、[中国铁路总公司](../Page/中国铁路总公司.md "wikilink")、[中华世纪坛](../Page/中华世纪坛.md "wikilink")、[中央电视台旧台址](../Page/中央电视台.md "wikilink")、[梅地亚中心](../Page/梅地亚中心.md "wikilink")。

## 结构

军事博物馆9号线车站主体下穿既有1号线，1号线车站的西端靠近9号线车站的中间位置，为避开1号线线路采用分离式站厅，1号线车站为地下二层车站，其站台层位于地下二层；9号线车站为地下三层车站，其站台层位于地下三层；换乘厅位于地下一层，两线车站采用换乘厅+换乘通道的换乘方式，通过从9号线两端站厅引出的南、北两条换乘通道连接，每条换乘通道有两个接口，分别连接1号线车站东西两侧的站厅。限于1号线军博站建成时未预留换乘条件，需另建设换乘通道，1号线与9号线的换乘距离仍较远。

1号线和9号线换乘时，乘客由9号线站台至换乘厅，通过换乘通道进入1号线站台；由1号线站台从站厅进入9号线换乘厅，再下至9号线站台。预计高峰时段将通过加设导向标志使南、北两条换乘通道形成单向环形换乘模式。\[3\]

<table>
<tbody>
<tr class="odd">
<td><p><strong>地面</strong></p></td>
<td><p>出入口</p></td>
<td><p>A–H出入口</p></td>
</tr>
<tr class="even">
<td><p><strong>地下一层</strong></p></td>
<td><p>1号线站厅</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>换乘通道</p></td>
<td><p>1号线去往9号线换乘通道</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>地下二层</strong></p></td>
<td><p>9号线站厅</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>换乘通道</p></td>
<td><p>9号线去往1号线换乘通道</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>往<a href="../Page/苹果园站.md" title="wikilink">苹果园站</a><small>（<a href="../Page/公主坟站.md" title="wikilink">公主坟站</a>）（站台2）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>島式月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>往<a href="../Page/四惠东站.md" title="wikilink">四惠东站</a><small>（<a href="../Page/木樨地站.md" title="wikilink">木樨地站</a>）（站台1）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>地下三层</strong></p></td>
<td></td>
<td><p>{{split text|往<a href="../Page/郭公庄站.md" title="wikilink">郭公庄站</a><small>（<a href="../Page/北京西站.md" title="wikilink">北京西站</a>）（站台3）</small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/島式月台.md" title="wikilink">島式月台</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>往<a href="../Page/国家图书馆站.md" title="wikilink">国家图书馆站</a><small>（<a href="../Page/白堆子站.md" title="wikilink">白堆子站</a>）（站台4）</small></p></td>
<td></td>
</tr>
</tbody>
</table>

9号线军事博物馆站的站厅墙上装饰有《[孙子兵法](../Page/孙子兵法.md "wikilink")》中的经典语句，契合军事博物馆的站名。

<File:Military> Museum Station Platform (Line 1) 20131103.jpg|1号线站台
<File:201606> Military Museum Station at 23.00.jpg |1号线站台中央
<File:Transfer> sign of Line 1, Military Museum Station
01.JPG|1号线站台中的站名和换乘标记 <File:Military> Museum Station
Transfer Hall 20181124.jpg|军事博物馆站1号线换乘9号线入口 <File:Platform> of Military
Museum Station (Line 9) 20181124.jpg|9号线站台 <File:Art> decoration in
Military Museum Station, Line 9 (1).JPG|9号线站内《孙子兵法》主题装饰

## 出入口

[201606_Exit_C1_of_Military_Museum_Station.jpg](https://zh.wikipedia.org/wiki/File:201606_Exit_C1_of_Military_Museum_Station.jpg "fig:201606_Exit_C1_of_Military_Museum_Station.jpg")大楼\]\]
军事博物馆站共有9个出入口，E1/E2、A、B出入口位于[复兴路北侧](../Page/复兴路.md "wikilink")，H、D、C1/C2出入口位于复兴路南侧，G出入口位于[羊坊店路东侧](../Page/羊坊店路.md "wikilink")。由于邻近京西宾馆，该站H出入口常年封闭。\[4\]

<table>
<thead>
<tr class="header">
<th><p>编号</p></th>
<th><p>建议前往的目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/中华世纪坛.md" title="wikilink">中华世纪坛</a>、<a href="../Page/梅地亞中心.md" title="wikilink">梅地亞中心</a>、<a href="../Page/中国人民革命军事博物馆.md" title="wikilink">中国人民革命军事博物馆</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/中国人民革命军事博物馆.md" title="wikilink">中国人民革命军事博物馆</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/中国铁路总公司.md" title="wikilink">中国铁路总公司</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>中国有色工程设计研究院</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>中国有色工程设计研究院</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/中央电视台彩色电视中心.md" title="wikilink">中央电视台彩色电视中心</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/中央电视台彩色电视中心.md" title="wikilink">中央电视台彩色电视中心</a>、<a href="../Page/梅地亞中心.md" title="wikilink">梅地亞中心</a>、<a href="../Page/中华世纪坛.md" title="wikilink">中华世纪坛</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>恩菲科技大厦、北京市玉渊潭中学</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/京西宾馆.md" title="wikilink">京西宾馆</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 运营

北京地铁9号线军事博物馆站未启用前，乘坐1号线的乘客需要出站转车前往[北京西站](../Page/北京西站.md "wikilink")，[北京公交320路](../Page/北京公交320路.md "wikilink")（后改为特18）和[北京公交21路一度开行往返军事博物馆站与北京西站之间的区间车](../Page/北京公交21路.md "wikilink")，出口外揽客的“[黑车](../Page/非法營運車輛.md "wikilink")”亦曾长年盘踞，经常有宰客的负面新闻爆出。\[5\]2013年12月21日，9号线军事博物馆站正式启用，实现与1号线的换乘，乘客由1号线换乘9号线可直达北京西站无需再出站，站外的黑车也随之销声匿迹。

## 外部連結

  - [军事博物馆站](http://www.bjsubway.com/station/xltcx/line9/2013-10-26/344.html)－北京地铁官方网站

## 参考资料

[Category:海淀区地铁车站](../Category/海淀区地铁车站.md "wikilink")
[Category:1971年启用的铁路车站](../Category/1971年启用的铁路车站.md "wikilink")

1.  [9号线军博站或甩站通过](http://www.bjnews.com.cn/news/2012/10/25/229663.html)，新京报，2012-10-25。
2.  [9号线军事博物馆站明起可换乘](http://www.bjnews.com.cn/news/2013/12/20/298400.html)，新京报，2013-12-20。
3.  [北京地铁9号线军事博物馆站启用](http://news.xinhuanet.com/photo/2013-12/21/c_125895152.htm)，新华网，2013年12月21日。
4.
5.