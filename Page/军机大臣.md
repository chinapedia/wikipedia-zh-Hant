**軍機大臣**（），雅稱為[樞密](../Page/樞密使.md "wikilink")，負責為[清朝皇帝](../Page/清朝皇帝.md "wikilink")「跪受筆錄」提供建議的職務。中國[清代中樞職務名稱](../Page/清朝.md "wikilink")；該頭銜屬於任務性質，而非定-{}-制官銜，無[品階可言](../Page/品階.md "wikilink")，擔任者亦無[俸祿](../Page/俸祿.md "wikilink")。

## 歷史

[清朝沿襲](../Page/清朝.md "wikilink")[明朝制度](../Page/明朝.md "wikilink")，實行君主集權，不設[宰相](../Page/宰相.md "wikilink")。[雍正帝初為指揮](../Page/雍正帝.md "wikilink")[青海戰事之需要](../Page/青海.md "wikilink")，於1732年設立[軍機處](../Page/軍機處.md "wikilink")；後沿為定制，負責輔佐皇帝處理軍國大政。雍正時首任軍機大臣為[張廷玉與](../Page/張廷玉.md "wikilink")[鄂爾泰](../Page/鄂爾泰.md "wikilink")，恩遇最隆。

自軍機處設立，軍機大臣取代[內閣大學士成為清朝事實上的](../Page/內閣大學士.md "wikilink")[宰相](../Page/宰相.md "wikilink")，但與宰相職務不同的是，此為沒有任何[品階的非定制職銜](../Page/品階.md "wikilink")，職權亦比不上明朝以前的宰相。軍機大臣的權力完全取決於皇帝的信任，由於是非定制官銜，皇帝可隨時撤職，亦因此被認為「非真宰相」。直到清末官制改定，理論上其功能為[新內閣所取代](../Page/慶親王內閣.md "wikilink")，然而清末1911年，首任[內閣總理大臣](../Page/大清國內閣總理大臣.md "wikilink")[慶親王奕劻](../Page/奕劻.md "wikilink")，原亦為領班軍機王大臣。

軍機處的編制，大臣由[親王](../Page/親王.md "wikilink")、[大學士](../Page/大學士.md "wikilink")、[尚書](../Page/尚書.md "wikilink")、[侍郎或](../Page/侍郎.md "wikilink")[京堂充任](../Page/京堂.md "wikilink")，無定員，一般六、七人。其中資歷最深者為「首席軍機大臣」，或稱「領班軍機大臣」，但沒有正式的名義；入值軍機時按資歷深淺的分別稱「軍機處行走」、「軍機大臣上行走」及「軍機大臣上學習行走」三種；「軍機處行走」名目從[嘉慶朝後期起即極少使用](../Page/嘉慶.md "wikilink")，僅[恭親王](../Page/恭親王.md "wikilink")[奕訢以議政王入值軍機時使用過一次](../Page/奕訢.md "wikilink")。另有「軍機[章京上行走](../Page/章京.md "wikilink")」若干人負責協助大臣處理文書事務。

## 历代军机大臣

### [雍正朝](../Page/雍正.md "wikilink")

[怡親王胤祥](../Page/胤祥.md "wikilink") [張廷玉](../Page/張廷玉.md "wikilink")
[马尔赛](../Page/马尔赛.md "wikilink") [蒋廷锡](../Page/蒋廷锡.md "wikilink")
[鄂尔泰](../Page/鄂尔泰.md "wikilink") [哈元生](../Page/哈元生.md "wikilink")
[马兰泰](../Page/马兰泰.md "wikilink") [福彭](../Page/福彭.md "wikilink")
[讷亲](../Page/讷亲.md "wikilink") [班第](../Page/班第.md "wikilink")

### [乾隆朝](../Page/乾隆.md "wikilink")

[鄂尔泰](../Page/鄂尔泰.md "wikilink") [张廷玉](../Page/张廷玉.md "wikilink")
[讷亲](../Page/讷亲.md "wikilink") [班第](../Page/班第.md "wikilink")
[索柱](../Page/索柱.md "wikilink") [丰盛额](../Page/丰盛额.md "wikilink")
[海望](../Page/海望.md "wikilink") [莽鹄立](../Page/莽鹄立.md "wikilink")
[纳延泰](../Page/纳延泰.md "wikilink") [徐本](../Page/徐本.md "wikilink")
[傅恆](../Page/傅恆.md "wikilink") [汪由敦](../Page/汪由敦.md "wikilink")
[高斌](../Page/高斌.md "wikilink") [蒋溥](../Page/蒋溥.md "wikilink")
[陈大受](../Page/陈大受.md "wikilink") [舒赫德](../Page/舒赫德.md "wikilink")
[来保](../Page/来保.md "wikilink") [尹继善](../Page/尹继善.md "wikilink")
[刘纶](../Page/刘纶.md "wikilink") [兆惠](../Page/兆惠.md "wikilink")
[劉統勳](../Page/劉統勳.md "wikilink")
[觉罗雅尔哈善](../Page/觉罗雅尔哈善.md "wikilink")
[阿兰泰](../Page/阿兰泰.md "wikilink") [阿里衮](../Page/阿里衮.md "wikilink")
[裘曰修](../Page/裘曰修.md "wikilink") [梦麟](../Page/梦麟.md "wikilink")
[三泰](../Page/三泰.md "wikilink") [富德](../Page/富德.md "wikilink")
[于敏中](../Page/于敏中.md "wikilink") [阿桂](../Page/阿桂.md "wikilink")
[福隆安](../Page/福隆安.md "wikilink") [索琳](../Page/索琳.md "wikilink")
[温福](../Page/温福.md "wikilink") [丰升额](../Page/丰升额.md "wikilink")
[桂林](../Page/桂林_\(清朝\).md "wikilink") [庆桂](../Page/庆桂.md "wikilink")
[福康安](../Page/福康安.md "wikilink") [袁守侗](../Page/袁守侗.md "wikilink")
[梁国治](../Page/梁国治.md "wikilink") [阿思哈](../Page/阿思哈.md "wikilink")
[和珅](../Page/和珅.md "wikilink") [明亮](../Page/明亮.md "wikilink")
[李侍尧](../Page/李侍尧.md "wikilink") [董诰](../Page/董诰.md "wikilink")
[福长安](../Page/福长安.md "wikilink") [王杰](../Page/王杰.md "wikilink")
[孙士毅](../Page/孙士毅.md "wikilink") [松筠](../Page/松筠.md "wikilink")
[台布](../Page/台布.md "wikilink")

### [嘉庆朝](../Page/嘉庆.md "wikilink")

[阿桂](../Page/阿桂.md "wikilink") [和珅](../Page/和珅.md "wikilink")
[王杰](../Page/王杰.md "wikilink") [福长安](../Page/福长安.md "wikilink")
[董诰](../Page/董诰.md "wikilink") [台布](../Page/台布.md "wikilink")
[沈初](../Page/沈初.md "wikilink") [傅森](../Page/傅森.md "wikilink")
[戴衢亨](../Page/戴衢亨.md "wikilink") [吴熊光](../Page/吴熊光.md "wikilink")
[那彦成](../Page/那彦成.md "wikilink") [成親王永瑆](../Page/永瑆.md "wikilink")
[庆桂](../Page/庆桂.md "wikilink") [成德](../Page/成德.md "wikilink")
[刘权之](../Page/刘权之.md "wikilink") [德瑛](../Page/德瑛.md "wikilink")
[英和](../Page/英和.md "wikilink") [托津](../Page/托津.md "wikilink")
[方维甸](../Page/方维甸.md "wikilink") [盧蔭溥](../Page/盧蔭溥.md "wikilink")
[松筠](../Page/松筠.md "wikilink") [勒保](../Page/勒保.md "wikilink")
[桂芳](../Page/桂芳.md "wikilink") [章煦](../Page/章煦.md "wikilink")
[戴均元](../Page/戴均元.md "wikilink") [和瑛](../Page/和瑛.md "wikilink")
[文孚](../Page/文孚.md "wikilink")

### [道光朝](../Page/道光.md "wikilink")

[曹振镛](../Page/曹振镛.md "wikilink") [黄钺](../Page/黄钺.md "wikilink")
[英和](../Page/英和.md "wikilink") [盧蔭溥](../Page/盧蔭溥.md "wikilink")
[文孚](../Page/文孚.md "wikilink") [松筠](../Page/松筠.md "wikilink")
[长龄](../Page/长龄.md "wikilink") [玉麟](../Page/玉麟.md "wikilink")
[王鼎](../Page/王鼎.md "wikilink") [蒋攸銛](../Page/蒋攸銛.md "wikilink")
[穆彰阿](../Page/穆彰阿.md "wikilink") [潘世恩](../Page/潘世恩.md "wikilink")
[赵盛奎](../Page/赵盛奎.md "wikilink") [赛尚阿](../Page/赛尚阿.md "wikilink")
[奎照](../Page/奎照.md "wikilink") [文庆](../Page/文庆.md "wikilink")
[隆文](../Page/隆文.md "wikilink") [何汝霖](../Page/何汝霖.md "wikilink")
[祁雋藻](../Page/祁雋藻.md "wikilink") [陈孚恩](../Page/陈孚恩.md "wikilink")
[季芝昌](../Page/季芝昌.md "wikilink")

### [咸丰朝](../Page/咸丰.md "wikilink")

[祁雋藻](../Page/祁雋藻.md "wikilink") [赛尚阿](../Page/赛尚阿.md "wikilink")
[何汝霖](../Page/何汝霖.md "wikilink") [季芝昌](../Page/季芝昌.md "wikilink")
[穆蔭](../Page/穆蔭.md "wikilink") [舒兴阿](../Page/舒兴阿.md "wikilink")
[彭蕴章](../Page/彭蕴章.md "wikilink") [邵灿](../Page/邵灿.md "wikilink")
[麟魁](../Page/麟魁.md "wikilink") [恭親王奕訢](../Page/奕訢.md "wikilink")
[瑞麟](../Page/瑞麟.md "wikilink") [杜翰](../Page/杜翰.md "wikilink")
[文庆](../Page/文庆.md "wikilink") [柏葰](../Page/柏葰.md "wikilink")
[匡源](../Page/匡源.md "wikilink") [文祥](../Page/文祥.md "wikilink")
[焦祐瀛](../Page/焦祐瀛.md "wikilink")

### [同治朝](../Page/同治.md "wikilink")

[恭親王奕訢](../Page/奕訢.md "wikilink") [桂良](../Page/桂良.md "wikilink")
[沈兆霖](../Page/沈兆霖.md "wikilink") [宝鋆](../Page/宝鋆.md "wikilink")
[曹毓瑛](../Page/曹毓瑛.md "wikilink") [文祥](../Page/文祥.md "wikilink")
[醇賢親王奕譞](../Page/奕譞.md "wikilink")
[李棠阶](../Page/李棠阶.md "wikilink")
[李鸿藻](../Page/李鸿藻.md "wikilink")
[胡家玉](../Page/胡家玉.md "wikilink")
[汪元方](../Page/汪元方.md "wikilink")
[沈桂芬](../Page/沈桂芬.md "wikilink")

### [光绪朝](../Page/光绪.md "wikilink")

[恭親王奕訢](../Page/奕訢.md "wikilink") [文祥](../Page/文祥.md "wikilink")
[宝鋆](../Page/宝鋆.md "wikilink") [沈桂芬](../Page/沈桂芬.md "wikilink")
[李鸿藻](../Page/李鸿藻.md "wikilink") [景廉](../Page/景廉.md "wikilink")
[王文韶](../Page/王文韶.md "wikilink") [醇賢親王奕譞](../Page/奕譞.md "wikilink")
[左宗棠](../Page/左宗棠.md "wikilink") [翁同龢](../Page/翁同龢.md "wikilink")
[潘祖蔭](../Page/潘祖蔭.md "wikilink") [礼亲王世铎](../Page/世铎.md "wikilink")
[额勒和布](../Page/额勒和布.md "wikilink") [阎敬铭](../Page/阎敬铭.md "wikilink")
[张之万](../Page/张之万.md "wikilink") [孙毓汶](../Page/孙毓汶.md "wikilink")
[许庚身](../Page/许庚身.md "wikilink") [徐用仪](../Page/徐用仪.md "wikilink")
[刚毅](../Page/刚毅.md "wikilink") [錢應溥](../Page/錢應溥.md "wikilink")
[廖寿恆](../Page/廖寿恆.md "wikilink") [裕禄](../Page/裕禄.md "wikilink")
[荣禄](../Page/荣禄.md "wikilink") [啟秀](../Page/啟秀.md "wikilink")
[赵舒翘](../Page/赵舒翘.md "wikilink") [端郡王载漪](../Page/载漪.md "wikilink")
[鹿传霖](../Page/鹿传霖.md "wikilink") [瞿鸿禨](../Page/瞿鸿禨.md "wikilink")
[慶親王奕劻](../Page/奕劻.md "wikilink")
[荣庆](../Page/荣庆.md "wikilink") [徐世昌](../Page/徐世昌.md "wikilink")
[铁良](../Page/铁良.md "wikilink") [世续](../Page/世续.md "wikilink")
[林绍年](../Page/林绍年.md "wikilink") [醇親王載灃](../Page/载沣.md "wikilink")
[张之洞](../Page/张之洞.md "wikilink") [袁世凯](../Page/袁世凯.md "wikilink")
[那桐](../Page/那桐.md "wikilink")

### [宣統朝](../Page/宣統.md "wikilink")

[慶親王奕劻](../Page/奕劻.md "wikilink") [袁世凱](../Page/袁世凱.md "wikilink")
[世续](../Page/世续.md "wikilink") [张之洞](../Page/张之洞.md "wikilink")
[那桐](../Page/那桐.md "wikilink") [鹿傳霖](../Page/鹿傳霖.md "wikilink")
[戴鴻慈](../Page/戴鴻慈.md "wikilink") [吳郁生](../Page/吳郁生.md "wikilink")
[貝勒毓朗](../Page/毓朗.md "wikilink") [徐世昌](../Page/徐世昌.md "wikilink")

## 參見

  - [軍機處](../Page/軍機處.md "wikilink")
  - [军机大臣列表](../Page/军机大臣列表.md "wikilink")
  - [大學士](../Page/大學士.md "wikilink")
  - [宰相](../Page/宰相.md "wikilink")
  - [皇族內閣](../Page/皇族內閣.md "wikilink")

{{-}}

[Category:清朝官制](../Category/清朝官制.md "wikilink")
[軍機大臣](../Category/軍機大臣.md "wikilink")