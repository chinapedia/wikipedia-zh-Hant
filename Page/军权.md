[Xi_Jiping_cropped.jpg](https://zh.wikipedia.org/wiki/File:Xi_Jiping_cropped.jpg "fig:Xi_Jiping_cropped.jpg")；在2012年11月的[中共十八大上](../Page/中共十八大.md "wikilink")[胡锦涛将军委主席的职务交班给新任总书记习近平](../Page/胡锦涛.md "wikilink")，胡锦涛的离任被外界认为是“裸退”。\]\]
**軍權**，通常指指揮[軍隊作戰的](../Page/軍隊.md "wikilink")[權力](../Page/權力.md "wikilink")，也被稱為**兵權**，軍權一般被認為是「[國家](../Page/國家.md "wikilink")」的最高權力，一般由[國家元首或](../Page/國家元首.md "wikilink")[政府首腦掌握](../Page/政府首腦.md "wikilink")，也有像[中華人民共和國這些](../Page/中華人民共和國.md "wikilink")[社會主義國家](../Page/社會主義國家.md "wikilink")，不是由[政府官員掌握](../Page/政府官員.md "wikilink")，而是掌握在[執政黨](../Page/執政黨.md "wikilink")[領袖手上](../Page/政黨領袖.md "wikilink")。在中國，[黨](../Page/中國共產黨.md "wikilink")、[政](../Page/中華人民共和國政府.md "wikilink")、[軍三權之中](../Page/中國人民解放軍.md "wikilink")，軍權最高，[中國共產黨首任中央委員會主席](../Page/中國共產黨.md "wikilink")[毛澤東提出](../Page/毛澤東.md "wikilink")[槍杆子裡面出政權](../Page/槍杆子裡面出政權.md "wikilink")。因此，中共堅持[黨指揮槍](../Page/黨指揮槍.md "wikilink")，以維持[一黨專政](../Page/一黨專政.md "wikilink")。一些政权在移交权利时，首先只移交黨、政两权，如[江泽民卸任](../Page/江泽民.md "wikilink")[中共中央总书记后实际上还掌握了军职和军权](../Page/中共中央总书记.md "wikilink")\[1\]，用两三年观察[胡锦涛能够接任后](../Page/胡锦涛.md "wikilink")，才让胡接任[中央军委主席](../Page/中央军委主席.md "wikilink")\[2\]\[3\]。在[2012年11月的](../Page/2012年11月.md "wikilink")[中共十八大上](../Page/中共十八大.md "wikilink")[胡锦涛将](../Page/胡锦涛.md "wikilink")[中共中央军委主席同时交班给新任中共中央总书记](../Page/中共中央军委主席.md "wikilink")[习近平](../Page/习近平.md "wikilink")，胡锦涛的离任被外界认为是“裸退”\[4\]；习近平以此成为[中华人民共和国最高领导人](../Page/中华人民共和国最高领导人.md "wikilink")。

## 各国的军权

### [泰國](../Page/泰國.md "wikilink")

  - [泰國皇家軍隊發動對總理](../Page/泰國皇家軍隊.md "wikilink")[他信的](../Page/他信.md "wikilink")[軍事政變](../Page/軍事政變.md "wikilink")。

## 注釋

## 参考文献

## 參見

  - [党指挥枪](../Page/党指挥枪.md "wikilink")、[枪杆子里面出政权](../Page/枪杆子里面出政权.md "wikilink")、[军队国家化](../Page/军队国家化.md "wikilink")
  - [垂帘听政](../Page/垂帘听政.md "wikilink")、[政治花瓶](../Page/政治花瓶.md "wikilink")
  - [军政府](../Page/军政府.md "wikilink")、[軍閥](../Page/軍閥.md "wikilink")、[軍事政變](../Page/軍事政變.md "wikilink")
  - [戰爭](../Page/戰爭.md "wikilink")、[起义](../Page/起义.md "wikilink")、[內戰](../Page/內戰.md "wikilink")、[干涉](../Page/干涉_\(國際政治\).md "wikilink")

[J](../Category/军事.md "wikilink") [J](../Category/政治權力.md "wikilink")

1.  [同意江泽民辞去中央军委主席职务决定胡锦涛任中央军委主席](http://news.xinhuanet.com/newscenter/2004-09/19/content_1995216.htm)
2.  [BBC 中文网| 中国报道|
    台湾：江交军权有助两岸关系稳定](http://news.bbc.co.uk/chinese/simp/hi/newsid_3670000/newsid_3671000/3671040.stm)
3.  [世界华人网:“胡锦涛暗逼江核心交军权内幕”](http://www.world-chinese.com/GB/9668.asp)
4.  [军报：全军拥护中央军委权威
    听从习主席指挥](http://mil.sohu.com/20121116/n357763390.shtml)