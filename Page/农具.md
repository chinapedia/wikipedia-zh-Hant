农具是[农业生产中使用的](../Page/农业.md "wikilink")[工具](../Page/工具.md "wikilink")。

| 名称                                       | 类别         | 说明                                                    |
| ---------------------------------------- | ---------- | ----------------------------------------------------- |
| [拖拉机](../Page/拖拉机.md "wikilink")         | 牵引工具       |                                                       |
| [耜](../Page/耜.md "wikilink")             | 耕种工具       |                                                       |
| [耒耜](../Page/耒耜.md "wikilink")           | 耕种工具       |                                                       |
| [犁](../Page/犁.md "wikilink")             | 耕种工具       |                                                       |
| [耖](../Page/耖.md "wikilink")             | 耕种工具       |                                                       |
| [锄](../Page/锄.md "wikilink")             | 耕种工具       |                                                       |
| [锹](../Page/锹.md "wikilink")             | 耕种工具       |                                                       |
| [耙](../Page/耙.md "wikilink")             | 耕种工具       |                                                       |
| [钉齿耙](../Page/钉齿耙.md "wikilink")         | 耕种工具       |                                                       |
| [圆盘耙](../Page/圆盘耙.md "wikilink")         | 耕种工具       |                                                       |
| [播种器](../Page/播种器.md "wikilink")         | 耕种工具       |                                                       |
| [旋耕机](../Page/旋耕机.md "wikilink")         | 耕种工具       |                                                       |
| [铲](../Page/铲.md "wikilink")             | 耕种工具       |                                                       |
| [深耕铲](../Page/深耕铲.md "wikilink")         | 耕种工具       |                                                       |
| [土壤覆盖机](../Page/土壤覆盖机.md "wikilink")     | 播种工具       |                                                       |
| [条播机](../Page/条播机.md "wikilink")         | 播种工具       |                                                       |
| [移栽机](../Page/移栽机.md "wikilink")         | 播种工具       |                                                       |
| [撒肥机](../Page/撒肥机.md "wikilink")         | 肥料及病虫害防治工具 |                                                       |
| [喷雾器](../Page/喷雾器.md "wikilink")         | 肥料及病虫害防治工具 |                                                       |
| [中枢灌溉系统](../Page/中枢灌溉系统.md "wikilink")   | 灌溉工具       |                                                       |
| [镰](../Page/镰.md "wikilink")             | 收割工具       |                                                       |
| [豆类作物收割机](../Page/豆类作物收割机.md "wikilink") | 收割工具       |                                                       |
| [联合收割机](../Page/联合收割机.md "wikilink")     | 收割工具       |                                                       |
| [传送带](../Page/传送带.md "wikilink")         | 收割工具       |                                                       |
| [玉米收割机](../Page/玉米收割机.md "wikilink")     | 收割工具       |                                                       |
| [采棉机](../Page/采棉机.md "wikilink")         | 收割工具       |                                                       |
| [饲料收获机](../Page/饲料收获机.md "wikilink")     | 收割工具       |                                                       |
| [掘薯机](../Page/掘薯机.md "wikilink")         | 收割工具       |                                                       |
| [割谷机](../Page/割谷机.md "wikilink")         | 收割工具       |                                                       |
| [打谷机](../Page/打谷机.md "wikilink")。        | 收割工具       | 手動擊打穀類使殼剝落的[農具叫作](../Page/農具.md "wikilink")「連枷」或「連耞」。 |
| [割草机](../Page/割草机.md "wikilink")         | 割晒干草工具     |                                                       |
| [搂草机](../Page/搂草机.md "wikilink")         | 割晒干草工具     |                                                       |
| [打包机](../Page/打包机.md "wikilink")         | 割晒干草工具     |                                                       |
| [干草架](../Page/干草架.md "wikilink")         | 割晒干草工具     |                                                       |
| [反向铲](../Page/反向铲.md "wikilink")         | 装载工具       |                                                       |
| [翻斗叉车](../Page/翻斗叉车.md "wikilink")       | 装载工具       |                                                       |
| [运粮拖车](../Page/运粮拖车.md "wikilink")       | 装载工具       |                                                       |
| [饲料粉碎机](../Page/饲料粉碎机.md "wikilink")     | 其它工具       |                                                       |

[da:Landbrugsredskaber](../Page/da:Landbrugsredskaber.md "wikilink")
[en:List of agricultural
machinery](../Page/en:List_of_agricultural_machinery.md "wikilink")
[es:Implemento agrícola](../Page/es:Implemento_agrícola.md "wikilink")
[fr:Machinisme agricole](../Page/fr:Machinisme_agricole.md "wikilink")
[it:Macchina agricola](../Page/it:Macchina_agricola.md "wikilink")
[ko:농기구](../Page/ko:농기구.md "wikilink")
[nn:Jordbruksreiskap](../Page/nn:Jordbruksreiskap.md "wikilink")
[no:Liste over
landbruksredskaper](../Page/no:Liste_over_landbruksredskaper.md "wikilink")
[pt:Implemento agrícola](../Page/pt:Implemento_agrícola.md "wikilink")

[農具](../Category/農具.md "wikilink")