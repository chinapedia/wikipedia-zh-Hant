[Coventgarden.jpg](https://zh.wikipedia.org/wiki/File:Coventgarden.jpg "fig:Coventgarden.jpg")[柯芬園菜市](../Page/柯芬園.md "wikilink")\]\]
[The_farmer's_market_near_the_Potala_in_Lhasa.jpg](https://zh.wikipedia.org/wiki/File:The_farmer's_market_near_the_Potala_in_Lhasa.jpg "fig:The_farmer's_market_near_the_Potala_in_Lhasa.jpg")的一个菜市场\]\]
[Carre_Bangkok.jpg](https://zh.wikipedia.org/wiki/File:Carre_Bangkok.jpg "fig:Carre_Bangkok.jpg")的一個室內市場\]\]
**农贸-{}-市场**，又称**农贸自由市场**、**菜-{}-市場**、**街市**或**菜市**或**農墟**，是指在城乡设立的可以进行自由买卖[农产品的](../Page/农产品.md "wikilink")[市场](../Page/市场.md "wikilink")，是一种[传统市场](../Page/传统市场.md "wikilink")。农贸市场在亚洲地区是自古以来就存在的市场。

## 中国大陆

20世纪50年代至70年代，中国大陆使用了大量的[计划经济](../Page/计划经济.md "wikilink")，「統購統銷」，只允許農民保留少許[自留地生產的作物](../Page/自留地.md "wikilink")，阻止了农贸市场的发展。但是农贸自由市场在80年代[改革开放后又逐渐复生](../Page/改革开放.md "wikilink")。

改革开放初期，在农村和城市恢复了曾被视为“[资本主义尾巴](../Page/资本主义.md "wikilink")”的农贸自由市场。饱受[短缺经济困扰和国营菜店冷落的老百姓](../Page/经济短缺.md "wikilink")，以一种欣喜的心情接受农贸自由市场的到来。在农贸自由市场上，可以买到新鲜的[农产品](../Page/农产品.md "wikilink")，允许顾客自己挑选偏好的[农产品](../Page/农产品.md "wikilink")，可以议价；在摊贩之间形成了竞争关系，价格可以随行就市。随着农贸自由市场在全国的发展，很快就把[国营的蔬菜店和副食店挤垮了](../Page/国营.md "wikilink")。农贸市场的大幅扩展也催生了一系列躁动的社会心态，1988年12月首播的相声《[特大新闻](../Page/特大新闻.md "wikilink")》即通过“[天安门广场要改农贸市场](../Page/天安门广场.md "wikilink")”的戏谑性说辞反映了这一趋势。\[1\]

## 参见

  - [传统市场](../Page/传统市场.md "wikilink")
  - [集市](../Page/集市.md "wikilink")
  - [早市](../Page/早市.md "wikilink")
  - [批发市场](../Page/批发.md "wikilink")
  - [超市](../Page/超市.md "wikilink")

## 参考资料

[Category:传统市场](../Category/传统市场.md "wikilink")
[Category:农业](../Category/农业.md "wikilink")

1.