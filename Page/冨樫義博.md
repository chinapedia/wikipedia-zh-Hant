**冨樫義博**\[1\]（），小名為「yoshirin」。在[日本](../Page/日本.md "wikilink")[山形縣](../Page/山形縣.md "wikilink")[新-{庄}-市出生](../Page/新庄市.md "wikilink")，[日本男性](../Page/日本.md "wikilink")[漫画家](../Page/漫画家.md "wikilink")，以漫畫《[幽遊白書](../Page/幽遊白書.md "wikilink")》與《[獵人](../Page/Hunter_×_Hunter.md "wikilink")》而出名。在1999年與[武内直子結婚](../Page/武内直子.md "wikilink")。\[2\]弟弟是成人漫畫家[冨樫秀昭](../Page/冨樫秀昭.md "wikilink")。

## 生平事蹟

1987年發表《FLY
STREET》獲得第34屆[手塚獎](../Page/手塚獎.md "wikilink")，就此出道。1994年因為《[幽遊白書](../Page/幽遊白書.md "wikilink")》大受歡迎。他的繳稅總額高居日本第76位。1997年12月24日與漫畫家[武內直子合出一本](../Page/武內直子.md "wikilink")[同人誌](../Page/同人誌.md "wikilink")，他畫了[武內直子的作品](../Page/武內直子.md "wikilink")《[美少女戰士](../Page/美少女戰士.md "wikilink")》，[武內直子也畫了一些平常不可能畫的東西](../Page/武內直子.md "wikilink")，並且書中還有他們的唇印，附註是「冨樫王子」與「直子公主」的聖誕禮物，自此時起二人的八卦謠言滿天飛，據說是[萩原一至牽線](../Page/萩原一至.md "wikilink")。

1999年1月冨樫義博與武内直子步入禮堂。\[3\]2000年11月夫婦有了一個兒子。2009年12月25日發售的《獵人》第27卷單行本的作者介紹裡明確表示自己有了第二個孩子，而且第二個小孩已經1歲
歲。\[4\]不過並未公布第二子的性別。2012年12月底的JUMP FESTA刊物中留下「女兒好可愛」的訊息。

冨樫以其豐富的想像力和故事情節、有深度的人物塑造而出名，他的漫畫作品很多。目前他的人氣漫畫作品《[HUNTER×HUNTER](../Page/HUNTER×HUNTER.md "wikilink")》（獵人）日本《週刊少年JUMP》連載中。冨樫愛好[棒球](../Page/棒球.md "wikilink")（在《[幽遊白書](../Page/幽遊白書.md "wikilink")》的Free
Talk中多次提到）、搖滾樂、養蛇、玩電玩遊戲\[5\]。

## 作品列表

  - [誇張的生日禮物](../Page/誇張的生日禮物.md "wikilink")（意外的生日禮物）：1987年開始在《[週刊少年JUMP](../Page/週刊少年JUMP.md "wikilink")》連載。
  - [奥卡爾特偵探團](../Page/奥卡爾特偵探團.md "wikilink")：1988年至1989年。
  - [淘氣愛神](../Page/淘氣愛神.md "wikilink")（）全4卷（1989年32号 - 1990年13号、集英社）
  - [狼人我愛你](../Page/狼人我愛你.md "wikilink")（）（1989年20號、集英社）

《[狼人我愛你](../Page/狼人我愛你.md "wikilink")》
《[靈異偵探團](../Page/靈異偵探團.md "wikilink")》
《[恐怖天使](../Page/恐怖天使.md "wikilink")》
《[誇張的生日禮物](../Page/誇張的生日禮物.md "wikilink")》
《[狂飆快速直球](../Page/狂飆快速直球.md "wikilink")》

  - [FLY STREET](../Page/FLY_STREET.md "wikilink")：1989年。
  - [惡魔家庭](../Page/惡魔家庭.md "wikilink")：1989年至1990年。
  - [幽遊白書](../Page/幽遊白書.md "wikilink")（）全19卷（1990年51號 -
    1994年32號、集英社）東立出版
  - [TWO SHOTS](../Page/TWO_SHOTS.md "wikilink")：1992年。
  - [老师比我小](../Page/老师比我小.md "wikilink")：1992年。
  - [LEVEL E](../Page/LEVEL_E.md "wikilink")（レベルE）全3卷 （1995年42號 -
    1997年3・4合併號、集英社） 東立出版
  - [HUNTER×HUNTER](../Page/HUNTER×HUNTER.md "wikilink")（HUNTER×HUNTER）共34卷（1998年14号-連載中、集英社）東立出版

## 休刊爭議

憑藉其作品的人氣，冨樫經常性地以各種理由長時間休刊或拖稿，理由包括：外出取材、家庭旅行、照顧剛出世的孩子\[6\]、沒靈感、生病、腰痛\[7\]等，日本網友將其評為“最常休刊的漫画家”\[8\]，还有部分中國網友戲稱“老贼”\[9\]\[10\]。
台灣網友更稱「富奸」為冨樫義博的綽號 ， 而「富奸」二字也就變成拖稿的代稱。
事實上冨樫義博花很多時間在家用遊戲機上，熱愛[PlayStation](../Page/PlayStation.md "wikilink")。亦經常與朋友打麻將，而在漫畫休刊期間，更前後以自己名義舉辦「冨樫盃」麻將大賽，獎勵是冨樫漫畫繪手稿筆名。\[11\]\[12\]

不過真正的原因是**稅金沉重**。冨樫義博在《週刊少年JUMP》創刊50周年的感想區中表示在自己連載的作品、所賺到的有高達70%用來交稅。這事讓他感到身心困擾，甚至坦言自己根本不在意電動獲得的成就。\[13\]

## 參考資料

## 外部連結

  - [FAN×FUN](http://fxf.cside1.jp/)

[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:山形縣出身人物](../Category/山形縣出身人物.md "wikilink")
[Category:山形大學校友](../Category/山形大學校友.md "wikilink")

1.  注1：中國：「**富坚义博**」。香港：「**富樫義博**」（**冨**為**富**的[異體字](../Page/異體字.md "wikilink")）。
    注2：日本發音為「」的姓氏，可對應「」或「」。而「」是「」的俗寫，但作為姓名和作戶籍登記時，則採用名隨主人政策，本條目依照維基百科[對中文條目命名的規定](../Page/Wikipedia:日語專有名詞的中譯原則.md "wikilink")，以相對應的中文漢字命名。

2.  [『HUNTER×HUNTER』冨樫義博に第2子誕生、休載の原因は育児休暇\!?](http://beauty.oricon.co.jp/trend-culture/trend/news/71932/full/)eltha

3.
4.
5.  [Yoshihiro
    TOGASHI](http://www.animenewsnetwork.com/encyclopedia/people.php?id=1608)[Anime
    News Network](../Page/Anime_News_Network.md "wikilink")

6.
7.  [《獵人暗黑大陸篇連載再開》其實這還不是休刊最久的一次……](https://news.gamme.com.tw/1389892)卡卡洛普

8.

9.

10.

11. [富堅杯麻將大賽是怎麼回事？ 摸幾圈休刊一年](http://news.gamme.com.tw/40378)卡卡洛普

12. [這是真的！休刊拖稿王冨樫《獵人》連載再開！](http://photo.chinatimes.com/20160315003636-260803)中時電子報

13.