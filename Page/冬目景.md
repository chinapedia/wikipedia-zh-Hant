**冬目景**（），[日本女性](../Page/日本.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")、[插畫家](../Page/插畫家.md "wikilink")。[神奈川縣出身](../Page/神奈川縣.md "wikilink")，[多摩美術大學美術學部油繪科畢業](../Page/多摩美術大學.md "wikilink")，與[沙村廣明是學長學妹關係](../Page/沙村廣明.md "wikilink")。

## 作品

### 單行本

  - （）全1冊（1995年、[幻冬舍](../Page/幻冬舍.md "wikilink")）[東立出版](../Page/東立出版.md "wikilink")

  - [變調人生](../Page/變調人生.md "wikilink")（）全1冊（1995年、）東立出版

  - [羔羊之歌](../Page/羔羊之歌.md "wikilink")（）全7冊（1995年－2002年、幻冬舎）東立出版

  - 機械浪劍客－黑鐵（）全5冊（1996年－2003、[講談社](../Page/講談社.md "wikilink")）[尖端出版](../Page/尖端出版.md "wikilink")

  - （2017年5号 - 連載中、[集英社](../Page/集英社.md "wikilink")）未代理

  - [昨日之歌](../Page/昨日之歌.md "wikilink")（）全11卷（1999年－2013年、[集英社](../Page/集英社.md "wikilink")）東立出版

  - （1998年8月 - 2000年4月、講談社）未代理

      - ISBN 4-06-330092-7（2000年5月15日）

      - ISBN 4-06-334956-X（2004年11月22日）

  - （）1冊待續（2002年－中斷連載、エニックス）[台灣角川](../Page/台灣角川.md "wikilink")

  - （）全4冊（2002年－2012年、）[長鴻出版社](../Page/長鴻出版社.md "wikilink")

  - （）港譯《家鼠的時間》全4冊（2004年－2008年、[講談社](../Page/講談社.md "wikilink")（台灣尖端出版、香港[天下文化](../Page/天下文化.md "wikilink")

  - [ACONY永遠的少女](../Page/ACONY永遠的少女.md "wikilink")（ACONY）全3冊（2003年－2010年、講談社月刊）尖端出版

  - [ももんち](../Page/ももんち.md "wikilink") 全1冊（2006年－2009年、小學館週刊[Big Comic
    Spirits](../Page/Big_Comic_Spirits.md "wikilink")）未代理

  - [時空建築幻視譚](../Page/時空建築幻視譚.md "wikilink")（マホロミ
    時空建築幻視譚）全4卷（2010年、小學館週刊Big Comic
    Spirits）東立出版

  - [天電雜音的公主](../Page/天電雜音的公主.md "wikilink")（）（『[月刊バーズ](../Page/月刊バーズ.md "wikilink")』2016年9月号
    - 連載中）

### 未單行本化作品

  - 《》（1997年、講談社週刊増刊第1期、短篇）
  - 《》（1999年，集英社増刊EXTRA、短篇）
  - 《》（2005年、[村田蓮爾責任編集](../Page/村田蓮爾.md "wikilink")「robot 3」、短篇）
  - 《》（2007年、[村田蓮爾責任編集](../Page/村田蓮爾.md "wikilink")「robot 7」、短篇）

### 畫冊

  - 百景【百景／文車館来訪記】（2000年，講談社。ISBN 4-06-330092-7）

  - （2000年，。同時有豪華版與一般版）

  - （2003年，）

  - （2008年，集英社）

### 其他

  - （遊戲，人物設定）

  - （遊戲，人物設定）

  - （電影，概念設計）

  - [三國志大戰Ver](../Page/三國志大戰.md "wikilink").2-R
    [張春華](../Page/張春華.md "wikilink")、Ver3.1-R
    [貂蟬](../Page/貂蟬.md "wikilink")（插畫）

  - （模型，人物設定）

  - PSYCHE（）（小說插畫，原著：唐辺葉介）

## 外部連結

  - 非官方介紹網站

  - 非官方介紹網站

[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:日本插畫家](../Category/日本插畫家.md "wikilink")
[Category:人物設計師](../Category/人物設計師.md "wikilink")
[Category:電子遊戲繪圖及原畫家](../Category/電子遊戲繪圖及原畫家.md "wikilink")
[Category:神奈川縣出身人物](../Category/神奈川縣出身人物.md "wikilink")