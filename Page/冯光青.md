**冯光青**（，），又译**冯光清**，[越南军事人物](../Page/越南.md "wikilink")，生于[河內市](../Page/河內市.md "wikilink")[麊泠縣石柁社](../Page/麊泠縣.md "wikilink")。第十届和十一届[越南共产党中央政治局委员](../Page/越南共产党中央政治局.md "wikilink")，曾任[越共中央军事党委副书记](../Page/越共中央军事党委.md "wikilink")，[越南人民军](../Page/越南人民军.md "wikilink")[大将军衔](../Page/大将.md "wikilink")，2006年6月至2016年4月任[越南国防部部长](../Page/越南国防部.md "wikilink")。

## 简历

冯光青1949年2月2日出生于越南[河內市](../Page/河內市.md "wikilink")[麊泠縣石柁社](../Page/麊泠縣.md "wikilink")，1967年加入[越南人民军](../Page/越南人民军.md "wikilink")，参加过[广治](../Page/广治.md "wikilink")、[南寮的战斗](../Page/南寮.md "wikilink")。1968年，加入[越南共产党](../Page/越南共产党.md "wikilink")。1971年因战功获“武装力量英雄”称号，时任越军320A师64团9营连长。
[Phung_Quang_Thanh_(crop_2).JPG](https://zh.wikipedia.org/wiki/File:Phung_Quang_Thanh_\(crop_2\).JPG "fig:Phung_Quang_Thanh_(crop_2).JPG")
1971年6月，就读于[越南陆军军官学校](../Page/越南陆军军官学校.md "wikilink")。1972年重返战场，任营长。1974年8月，选送就读于军事学院（即今[大叻陆军学院](../Page/大叻陆军学院.md "wikilink")）。1977年至1989年，先后担任团、师级指挥军官。1988年8月至1989年2月任越军312师师长。1989年，派往前苏联[伏罗希洛夫军事学院学习](../Page/伏罗希洛夫军事学院.md "wikilink")，次年回国就读于高级军事学院（即今[越南国防学院](../Page/越南国防学院.md "wikilink")）。

1991年8月至1993年8月，再次担任人民军312师师长。随后调至人民军总参谋部，先后担任总参谋部作战局副局长（1993年）和局长（1995年）。1997年8月，在[越南军事政治学院接受短期培训](../Page/越南军事政治学院.md "wikilink")。1997年12月任人民军第一军区司令。

2001年5月至2006年8月，接替调任人民军政治总局主任的[黎文勇中将出任](../Page/黎文勇.md "wikilink")[越南人民军总参谋长](../Page/越南人民军.md "wikilink")、[越南国防部副部长](../Page/越南国防部.md "wikilink")。2006年接替年届退休的[-{范}-文茶大将出任](../Page/范文茶.md "wikilink")[越共中央军事党委副书记兼越南国防部长](../Page/越共中央军事党委.md "wikilink")。

1994年晋升少将，1999年晋升中将，2003年晋升上将，2007年6月7日晋升大将。越共九届中央委员（2001年），十届中央政治局委员（2006年），十一届中央政治局委员（2011年）。

## 外部連結

[Category:越南大将](../Category/越南大将.md "wikilink")
[Category:馮姓](../Category/馮姓.md "wikilink")
[Category:越南人民軍大將](../Category/越南人民軍大將.md "wikilink")
[Category:河內人](../Category/河內人.md "wikilink")