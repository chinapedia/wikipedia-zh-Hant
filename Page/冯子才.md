**冯子才**，或名**冯子材**，[字](../Page/表字.md "wikilink")**南幹**，[号](../Page/号.md "wikilink")**萃亭**，[清军将领](../Page/清.md "wikilink")，因累官至[太子少保](../Page/太子少保.md "wikilink")，又称**冯宫保**，谥勇毅。

## 生平

生于广东[钦州](../Page/钦州.md "wikilink")（今属广西），自幼父母雙亡，流落街头，跑過[牛幫](../Page/牛幫.md "wikilink")，早年參加過廣東[天地會](../Page/天地會.md "wikilink")，1851年為[江北大營名將](../Page/江北大營.md "wikilink")、[總兵官](../Page/總兵.md "wikilink")，随广西提督[向荣力抗](../Page/向荣.md "wikilink")[太平天國](../Page/太平天國.md "wikilink")，於[一破江南大營時防守建功](../Page/一破江南大營.md "wikilink")，咸丰二年，冯子材改为隶属[张國樑麾下](../Page/张國樑.md "wikilink")，张國樑曾抚其背，称赞说：“子勇，余愧弗如。”张在丹阳南门外落水溺死，冯子材收聚残军，再度建立江南大營。1864年收復[常熟](../Page/常熟.md "wikilink")，領軍駐防[揚州](../Page/揚州.md "wikilink")，平定[太平天國](../Page/太平天國.md "wikilink")。同治元年正月十七（1862年2月15日），由甘肃西宁镇总兵官升任[廣西提督](../Page/廣西提督.md "wikilink")。光绪八年（1882年），乞病家居，以疾退休。光绪九年六月十九（1883年7月22日）病免。贵州代提督江忠义、丁忧副将萧荣芳先后于同治元年十二月至二年十二月代。\[1\]

1884年法军进犯滇桂边境时，两广总督[张之洞奏请起用冯子才](../Page/张之洞.md "wikilink")，称冯“老成宿将，熟习边境军务，威望远播”。光绪十年十月，冯子材率部抵达广西龙州。1885年任[廣西提督领导海外](../Page/廣西提督.md "wikilink")[義勇兵團](../Page/義勇兵.md "wikilink")[黑旗军在](../Page/黑旗军.md "wikilink")[越南](../Page/越南.md "wikilink")，[广西一带於](../Page/广西.md "wikilink")[法军作战](../Page/法國.md "wikilink")，馮部配備新式兵器，且帶領兩個兒子上陣戰鬥，“短衣草履，佩刀督队”，並亲自手挺长矛與法軍東非黑人兵进行肉搏戰，取得[镇南关大战勝利](../Page/镇南关大战.md "wikilink")，连克越南文渊、谅山；后到海南島撫黎，以開山為先，发动黎族青壮年开山修路，設義學館。[光緒十三年](../Page/光緒.md "wikilink")(1887年)在[海南島中部](../Page/海南島.md "wikilink")[五指山馮還書寫](../Page/五指山.md "wikilink")“手壁南荒”巨形山壁古刻；后任过云南、贵州提督，累官至[太子少保](../Page/太子少保.md "wikilink")，86岁卒于军旅，朝廷谥曰勇毅\[2\]。

冯子才逝後葬於欽州，朝廷诏予于钦州城东南隅（今市[粮食局周围](../Page/粮食局.md "wikilink")）建“冯勇毅公专祠”纪念，称“[宫保祠](../Page/宫保祠.md "wikilink")”。[其故居目前为](../Page/冯子材故居.md "wikilink")[爱国主义](../Page/爱国主义.md "wikilink")[教育基地](../Page/教育基地.md "wikilink")。著有《军牍集要》。

## 參考文獻

{{-}}

[Category:中法戰爭人物](../Category/中法戰爭人物.md "wikilink")
[Category:清朝廣西提督](../Category/清朝廣西提督.md "wikilink")
[Category:清朝貴州提督](../Category/清朝貴州提督.md "wikilink")
[Category:钦州人](../Category/钦州人.md "wikilink")
[Z子](../Category/馮姓.md "wikilink")
[Category:清朝雲南提督](../Category/清朝雲南提督.md "wikilink")
[Category:幼年失親者](../Category/幼年失親者.md "wikilink")
[Category:墓穴遭搗毀者](../Category/墓穴遭搗毀者.md "wikilink")

1.
2.  廖宗麟《中国近代民族英雄的杰出代表—冯子材史事撷奇》