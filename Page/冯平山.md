**馮平山**
（），原名**馮朝安**，[廣東](../Page/廣東.md "wikilink")[新會會城高第里人](../Page/新會.md "wikilink")、慈善家。
馮平山的父親[馮洪福](../Page/馮洪福.md "wikilink")，號景堂。

## 生平

馮平山7歲讀《[四書](../Page/四書.md "wikilink")》，15歲隨其六叔往[暹羅學習經商](../Page/泰國.md "wikilink")，來往於[廣東省](../Page/廣東省.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門](../Page/澳門.md "wikilink")，[重慶各地](../Page/重慶.md "wikilink")，在[四川](../Page/四川.md "wikilink")[重慶創辦安記商號](../Page/重慶.md "wikilink")，購銷[陳皮](../Page/陳皮.md "wikilink")、[桂皮等](../Page/桂皮.md "wikilink")[四川](../Page/四川.md "wikilink")[中藥材料](../Page/中藥.md "wikilink")。在1890年，馮平山自編[電報暗碼](../Page/電報.md "wikilink")，通經貿信息，又自置兩艘英國商船，貨運[重慶與](../Page/重慶.md "wikilink")[香港間](../Page/香港.md "wikilink")，10年間，盈利100萬銀圓。

1909年，馮平山在廣州開設[兆豐行從事藥材買賣](../Page/兆豐行.md "wikilink")。1913年遷往香港繼續經營，後來成為[南北行之中的一家著名商號](../Page/南北行.md "wikilink")。馮平山曾在1918年開始，與人合資開設[穗安銀鋪](../Page/穗安銀鋪.md "wikilink")、[岐豐行](../Page/岐豐行.md "wikilink")、[南生行](../Page/南生行.md "wikilink")、[維吉銀號](../Page/維吉銀號.md "wikilink")、[亦安銀號](../Page/亦安銀號.md "wikilink")、[東亞銀號](../Page/東亞銀號.md "wikilink")（後來成為[東亞銀行](../Page/東亞銀行.md "wikilink")）商號等，1922年更與人合辦[華人置業和](../Page/華人置業.md "wikilink")[安榮置業](../Page/安榮置業.md "wikilink")。

馮平山的慈善公益事業，包括廣州[方便醫院](../Page/方便醫院.md "wikilink")、香港1913年成為[東華醫院總理](../Page/東華醫院.md "wikilink")，協辦[大口環義莊](../Page/大口環義莊.md "wikilink")、同年7月[新會](../Page/新會.md "wikilink")[天河圍賑災](../Page/天河圍.md "wikilink")、1917年在新會會城創辦[平山貧兒義塾](../Page/平山貧兒義塾.md "wikilink")、在香港與[孔聖會辦免學費男女](../Page/孔聖會.md "wikilink")[義塾](../Page/義塾.md "wikilink")。1918年加入[新會商會](../Page/新會商會.md "wikilink")、成為[保良局總理](../Page/保良局.md "wikilink")。1919年，在新會捐建[白沙公園](../Page/白沙公園.md "wikilink")、[象山公園](../Page/象山公園.md "wikilink")、[嘉會亭](../Page/嘉會亭.md "wikilink")、[養拙亭及](../Page/養拙亭.md "wikilink")[香港華商總會新樓](../Page/香港華商總會.md "wikilink")。
他也曾擔任[團防局紳](../Page/團防局.md "wikilink")，並出資推動[廣華醫院創辦免費](../Page/廣華醫院.md "wikilink")[留產服務](../Page/留產.md "wikilink")。

馮平山支持中文教育，為[官立漢文中學](../Page/官立漢文中學.md "wikilink")（今[金文泰中學](../Page/金文泰中學.md "wikilink")）的倡議者之一，並協助[香港大學開設中文本科課程及創辦中文學院](../Page/香港大學.md "wikilink")，其兒子[馮秉華和](../Page/馮秉華.md "wikilink")[馮秉芬更為中文學院第一期學生](../Page/馮秉芬.md "wikilink")。馮平山更向香港大學捐出十萬港元興建中文圖書館，校方為其冠名紀念。

1922年，馮平山在新會會城捐建[平山小學](../Page/平山小學.md "wikilink")、新會[景堂圖書館](../Page/景堂圖書館.md "wikilink")、廣州高師附屬高小校舍及獎學金、在香港捐建[香港仔兒童工藝院](../Page/香港仔兒童工藝院.md "wikilink")，[香港華商總會](../Page/香港華商總會.md "wikilink")[圖書館等](../Page/圖書館.md "wikilink")。

馮平山在1925年獲[香港政府的](../Page/香港政府.md "wikilink")[太平紳士榮銜](../Page/太平紳士.md "wikilink")。

1931年8月2日在[香港辭世](../Page/香港.md "wikilink")，享年72歲。

## 以其命名的事物

  - [馮平山圖書館](../Page/馮平山圖書館.md "wikilink")：[香港大學](../Page/香港大學.md "wikilink")[圖書館](../Page/圖書館.md "wikilink")，位於[香港](../Page/香港.md "wikilink")[薄扶林大學本部南面](../Page/薄扶林.md "wikilink")。
  - [馮平山樓](../Page/馮平山樓.md "wikilink")：[香港大學美術博物館建築物之一](../Page/香港大學美術博物館.md "wikilink")。

## 參看

  - [馮平山家族](../Page/馮平山家族.md "wikilink")

## 外部連結

  - [馮平山圖書館](http://lib.hku.hk/fpslib/chi/index.html)
  - [馮平山樓](http://www.hku.hk/hkumag/mc/about_us.html)

[馮平山](../Category/新會人.md "wikilink")
[馮平山](../Category/中国企业家.md "wikilink")
[馮平山](../Category/東亞銀行.md "wikilink")
[P平山](../Category/馮姓.md "wikilink")