**冯潇霆**，出生于[辽宁省](../Page/辽宁省.md "wikilink")[大连市](../Page/大连市.md "wikilink")，足球运动员，现在效力于[广州恒大](../Page/广州恒大足球俱乐部.md "wikilink")
，司职[中后卫](../Page/后卫_\(足球\).md "wikilink")。他是[中国国家足球队的成员](../Page/中国国家足球队.md "wikilink")。拥有良好的头球技术，同时控球和远射能力也相当出色，是一名技术非常全面的球员。

## 俱乐部生涯

他曾在[法国老牌球队](../Page/法国.md "wikilink")[南特试训](../Page/南特足球俱乐部.md "wikilink")，他在效力[四川冠城时还创造了两个](../Page/四川冠城.md "wikilink")[甲A纪录](../Page/甲A.md "wikilink")，一是成为甲A联赛历史上最年轻的射手，二是在[邹侑根缺阵的情况下](../Page/邹侑根.md "wikilink")，冯潇霆戴上了队长袖标，成为[甲A历史上第一个年仅](../Page/甲A.md "wikilink")17岁的场上队长。

2009年1月，韩国[大邱足球俱乐部宣布与冯潇霆签约](../Page/大邱足球俱乐部.md "wikilink")，而之前他所属的大连实德则表示并不知情，于是成为一个月内继[李玮峰之后第二起中国球员试图自由转会国外俱乐部的事件](../Page/李玮峰.md "wikilink")，引起媒体对于[中国足球转会制度漏洞和弊病的广泛关注](../Page/中国足球转会制度.md "wikilink")\[1\]。大连实德曾经试图寻求[中国足协的帮助](../Page/中国足协.md "wikilink")，希望阻止冯潇霆转会，但是此时又发生了[周海滨自由转会的事件](../Page/周海滨.md "wikilink")，中国足协最终表示国际转会必须按照[国际足联的规则](../Page/国际足联.md "wikilink")，于是冯潇霆顺利加盟大邱。

2010年1月29日，冯潇霆和[全北现代签约](../Page/全北现代汽车.md "wikilink")，代表球队参加2010年的[亚足联冠军联赛](../Page/2010年亚足联冠军联赛.md "wikilink")\[2\]。12月25日，[广州恒大召开新闻发布会](../Page/广州恒大足球俱乐部.md "wikilink")，正式宣布冯潇霆加盟球队。

## 国家队生涯

冯潇霆在2003年首次入选国家队，成为第一个国足、国奥和国青的三栖球员。他在2005年[荷兰世青赛中担任中国队主力](../Page/2005年U20世界盃足球賽.md "wikilink")，并引领中国队打进16强，还是FIFA当年评选的14名“U-20新星”中唯一的亚洲人。

## 注释

[Category:2008年夏季奥林匹克运动会足球运动员](../Category/2008年夏季奥林匹克运动会足球运动员.md "wikilink")
[Category:大连籍足球运动员](../Category/大连籍足球运动员.md "wikilink")
[Category:中国海外足球运动员](../Category/中国海外足球运动员.md "wikilink")
[Category:中国国家足球队运动员](../Category/中国国家足球队运动员.md "wikilink")
[Category:大连实德球员](../Category/大连实德球员.md "wikilink")
[Category:四川全兴球员](../Category/四川全兴球员.md "wikilink")
[Category:大邱FC球员](../Category/大邱FC球员.md "wikilink")
[Category:全北现代球员](../Category/全北现代球员.md "wikilink")
[Category:在韓國的中國人](../Category/在韓國的中國人.md "wikilink")
[X](../Category/冯姓.md "wikilink")
[Category:广州足球俱乐部球员](../Category/广州足球俱乐部球员.md "wikilink")
[Category:中超球员](../Category/中超球员.md "wikilink")
[Category:中甲球员](../Category/中甲球员.md "wikilink")
[Category:韩职球员](../Category/韩职球员.md "wikilink")
[Category:韓國外籍足球運動員](../Category/韓國外籍足球運動員.md "wikilink")
[Category:中国奥运足球运动员](../Category/中国奥运足球运动员.md "wikilink")

1.
2.