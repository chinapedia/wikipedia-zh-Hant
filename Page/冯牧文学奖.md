**冯牧文学奖**，是为纪念中国著名文学[评论家](../Page/评论家.md "wikilink")[冯牧](../Page/冯牧.md "wikilink")，扶植新人、促进文学事业繁荣发展的遗愿而设，由中华文学基金会冯牧文学专项基金理事会设立的全国性文学奖项，该奖设青年批评家、文学新人和军旅文学三个奖项，首届冯牧文学奖于2000年颁发。

根据评奖规则该奖每年举办一次，并在2000年首届之后，于2001年、2002年又连续举办第二届和第三届，但2003年第三届之后不明原因停办14年。2016年举办第四届。\[1\]2018年举办第五届。\[2\]

## 历届获奖名单

### 第一届冯牧文学奖

  - 青年批评家奖：
      - [李洁非](../Page/李洁非.md "wikilink")（[北京](../Page/北京.md "wikilink")）
      - [洪治纲](../Page/洪治纲.md "wikilink")（[浙江](../Page/浙江.md "wikilink")）
      - [李敬泽](../Page/李敬泽.md "wikilink")（[北京](../Page/北京.md "wikilink")）

<!-- end list -->

  - 文学新人奖：
      - [红柯](../Page/红柯.md "wikilink")（[陕西](../Page/陕西.md "wikilink")）
      - [徐坤](../Page/徐坤.md "wikilink")（[北京](../Page/北京.md "wikilink")）

<!-- end list -->

  - 军旅文学奖：
      - [朱苏进](../Page/朱苏进.md "wikilink")（[南京军区](../Page/南京.md "wikilink")）
      - [邓一光](../Page/邓一光.md "wikilink")（[湖北](../Page/湖北.md "wikilink")）
      - [柳建伟](../Page/柳建伟.md "wikilink")（[河南](../Page/河南.md "wikilink")）

### 第二届冯牧文学奖

  - 青年批评家获奖者：
      - [何向阳](../Page/何向阳.md "wikilink")
      - [闫晶明](../Page/闫晶明.md "wikilink")
      - [谢有顺](../Page/谢有顺.md "wikilink")

<!-- end list -->

  - 文学新人奖获奖者：
      - [刘亮程](../Page/刘亮程.md "wikilink")
      - [毕飞宇](../Page/毕飞宇.md "wikilink")
      - [祁智](../Page/祁智.md "wikilink")

<!-- end list -->

  - 军旅文学创作奖获奖者：
      - [莫言](../Page/莫言.md "wikilink")
      - [乔良](../Page/乔良.md "wikilink")
      - [朱秀海](../Page/朱秀海.md "wikilink")

### 第三届冯牧文学奖

  - 青年批评家奖：
      - [郜元宝](../Page/郜元宝.md "wikilink")
      - [吴俊](../Page/吴俊.md "wikilink")
      - [李建军](../Page/李建军.md "wikilink")　　

<!-- end list -->

  - 文学新人奖：
      - [雪漠](../Page/雪漠.md "wikilink")
      - [周晓枫](../Page/周晓枫.md "wikilink")
      - [孙惠芬](../Page/孙惠芬.md "wikilink")

<!-- end list -->

  - 军旅文学创作奖：
      - [周大新](../Page/周大新.md "wikilink")
      - [李鸣生](../Page/李鸣生.md "wikilink")
      - [苗长水](../Page/苗长水.md "wikilink")

### 第四届冯牧文学奖

  - [魏微](../Page/魏微.md "wikilink")
  - [徐则臣](../Page/徐则臣.md "wikilink")
  - [杨庆祥](../Page/杨庆祥.md "wikilink")

### 第五届冯牧文学奖

  - [石一枫](../Page/石一枫.md "wikilink")
  - [鲁敏](../Page/鲁敏.md "wikilink")
  - [李云雷](../Page/李云雷.md "wikilink")

## 参考资料

[Category:中华人民共和国文学奖](../Category/中华人民共和国文学奖.md "wikilink")
[Category:以人名命名的奖项](../Category/以人名命名的奖项.md "wikilink")
[Category:2000年建立的奖项](../Category/2000年建立的奖项.md "wikilink")

1.
2.