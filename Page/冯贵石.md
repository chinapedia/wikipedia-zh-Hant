**冯贵石**（**Francesca Law
French**，1871年12月12日-1960年8月2日）是一位[英国](../Page/英国.md "wikilink")
[内地会派往中国的女传教士](../Page/内地会.md "wikilink")。

冯贵石出生于[比利时](../Page/比利时.md "wikilink")[布鲁日](../Page/布鲁日.md "wikilink")，是家中的次女
，在[瑞士](../Page/瑞士.md "wikilink")[日内瓦接受中学教育](../Page/日内瓦.md "wikilink")。她的姐姐[冯贵珠](../Page/冯贵珠.md "wikilink")（Evangeline
French）于1893年前往中国的山西霍州传教，尽管冯贵石也想前往中国，还是留下来照顾家庭。直到1908年母亲去世，冯贵石也前往中国传教。

1923年，冯贵石、冯贵珠和[盖群英](../Page/盖群英.md "wikilink")（Alice Mildred
Cable）3位女传教士离开山西霍州，以甘肃肃州（酒泉）为基地，开始对新疆的巡回布道，直到1950年代被迫离开。

1960年8月2日，冯贵石在[英国](../Page/英国.md "wikilink")[伦敦去世](../Page/伦敦.md "wikilink")。

[Category:内地会传教士](../Category/内地会传教士.md "wikilink")
[Category:英國基督徒](../Category/英國基督徒.md "wikilink")
[Category:1871年出生](../Category/1871年出生.md "wikilink")
[Category:1960年逝世](../Category/1960年逝世.md "wikilink")