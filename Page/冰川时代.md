《**冰原歷險記**》（）是2002年[美國動畫](../Page/美國動畫.md "wikilink")[電影](../Page/電影.md "wikilink")，由[藍天工作室製作](../Page/藍天工作室.md "wikilink")，[二十世紀福斯發行](../Page/二十世紀福斯.md "wikilink")，導演為[克里斯·威巨](../Page/克里斯·威巨.md "wikilink")（Chris
Wedge）和[卡洛斯·沙丹哈](../Page/卡洛斯·沙丹哈.md "wikilink")（Carlos Saldanha）聯合執導。

目前推出續集，2006年的《[冰原歷險記2](../Page/冰原歷險記2.md "wikilink")》、2009年的《[冰原歷險記3：恐龍現身](../Page/冰原歷險記3：恐龍現身.md "wikilink")》、2012年《[冰原歷險記4：板塊漂移](../Page/冰原歷險記4：板塊漂移.md "wikilink")》。2016年《[冰原歷險記：笑星撞地球](../Page/冰原歷險記：笑星撞地球.md "wikilink")》。

電影原先預定由[唐·布魯斯](../Page/唐·布魯斯.md "wikilink")（Don
Bluth）和[葛瑞·古德曼](../Page/葛瑞·古德曼.md "wikilink")（Gary
Goldman）執導，且為福斯動畫工作室（Fox Animation
Studios）製作2D動畫，由於[CGI動畫的興起](../Page/CGI.md "wikilink")，加上福斯傳統動畫電影《冰凍星球》（[Titan
A.E.](../Page/:en:Titan_A.E..md "wikilink")）的失敗，於是把製作轉向給專職CGI電腦動畫的藍天工作室。

## 動畫角色

動畫角色全都是[史前動物](../Page/史前動物.md "wikilink")。電影將這些動物[擬人化](../Page/擬人化.md "wikilink")，會彼此溝通與思考，如同許多史前電影，時代考究較為寬鬆不嚴謹，許多影片裡出現的生物物種，並非真正生存在同一時空，都將之歸類在同一史前時代裡，如幾萬年前的人種[尼安德塔和滅亡至今數千萬年的](../Page/尼安德塔人.md "wikilink")[恐龍即不可能出現同一時代](../Page/恐龍.md "wikilink")。

**鼠奎特**（Scrat）\[1\]

是一隻冰河時代的小型松鼠動物，特色是喜愛[橡子](../Page/橡子.md "wikilink")（acorn），且為了藏橡子常常搞出大型災變，行为搞笑，大抢主角镜头。

## 配音員

<table>
<thead>
<tr class="header">
<th><p>配音</p></th>
<th><p>角色</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>美國</p></td>
<td><p>台灣</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雷·羅曼諾.md" title="wikilink">雷·羅曼諾</a></p></td>
<td><p><a href="../Page/趙樹海.md" title="wikilink">趙樹海</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/丹尼斯·利瑞.md" title="wikilink">丹尼斯·利瑞</a></p></td>
<td><p><a href="../Page/李立群.md" title="wikilink">李立群</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/約翰·李古查摩.md" title="wikilink">約翰·李古查摩</a></p></td>
<td><p><a href="../Page/唐從聖.md" title="wikilink">唐從聖</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>使用原音</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/李立英.md" title="wikilink">李立英</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/傑克·布萊克.md" title="wikilink">傑克·布萊克</a></p></td>
<td><p><a href="../Page/李香生.md" title="wikilink">李香生</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/狄爾里奇·巴德.md" title="wikilink">狄爾里奇·巴德</a></p></td>
<td><p><a href="../Page/沈光平.md" title="wikilink">沈光平</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿蘭·圖代克.md" title="wikilink">艾倫·圖代克</a></p></td>
<td><p><a href="../Page/雷威遠.md" title="wikilink">雷威遠</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/陳宗岳.md" title="wikilink">陳宗岳</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/斯蒂芬·魯特.md" title="wikilink">斯蒂芬·魯特</a></p></td>
<td><p><a href="../Page/孫德成.md" title="wikilink">孫德成</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/魏晶琦.md" title="wikilink">魏晶琦</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/陳美貞.md" title="wikilink">陳美貞</a></p></td>
</tr>
</tbody>
</table>

## 史前動物

以下列出電影裡出現過的史前生物，再將之分為在影片中生存著與已故被冰凍的生物。

  - 已滅絕的生物

<!-- end list -->

  - [漸雷獸](../Page/漸雷獸.md "wikilink")
  - [恐狼](../Page/恐狼.md "wikilink")
  - [渡渡鳥](../Page/渡渡鳥.md "wikilink")
  - [雕齒獸](../Page/雕齒獸.md "wikilink")
  - [暴龍](../Page/暴龍.md "wikilink")
  - [重爪龍](../Page/重爪龍.md "wikilink")
  - [長頸駝](../Page/長頸駝.md "wikilink")
  - [長毛象](../Page/長毛象.md "wikilink")
  - [尼安德塔人](../Page/尼安德塔人.md "wikilink")
  - [古獸馬](../Page/古獸馬.md "wikilink")
  - [老撾石鼠](../Page/老撾石鼠.md "wikilink")
  - [劍齒虎](../Page/劍齒虎.md "wikilink")

<!-- end list -->

  - 尚未滅絕的生物

<!-- end list -->

  - [土豚](../Page/土豚.md "wikilink")
  - [河鱸](../Page/河鱸.md "wikilink")
  - [樹懶](../Page/樹懶.md "wikilink")
  - [食人魚](../Page/食人魚.md "wikilink")
  - [變形蟲](../Page/變形蟲.md "wikilink")
  - [牙形石](../Page/牙形石.md "wikilink")
  - [蚓螈](../Page/蚓螈.md "wikilink")

<!-- end list -->

  - 未知

<!-- end list -->

  - [飛碟](../Page/冰原歷險記：笑星撞地球.md "wikilink")

## 反響

### 評價

[爛番茄新鮮度](../Page/爛番茄.md "wikilink")77%，基於165條評論，平均分為6.8/10，而在[Metacritic上得到](../Page/Metacritic.md "wikilink")60分，[IMDB上得](../Page/IMDB.md "wikilink")7.6分，獲得普遍好評。

## 註釋

<div class="references-small">

<references />

</div>

## 外部連結

  -
  -
  -
  -
  -
  -
  - {{@movies|fien50268380}}

  -
  -
  -
[Category:冰原歷險記](../Category/冰原歷險記.md "wikilink")
[Category:2002年美國動畫電影](../Category/2002年美國動畫電影.md "wikilink")
[Category:2002年電腦动画电影](../Category/2002年電腦动画电影.md "wikilink")
[Category:導演處女作](../Category/導演處女作.md "wikilink")
[Category:公路電影](../Category/公路電影.md "wikilink")
[I](../Category/藍天工作室動畫電影.md "wikilink")

1.  中國大陸影碟版譯「斯克莱特」。