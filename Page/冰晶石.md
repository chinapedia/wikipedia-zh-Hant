**冰晶石**（[英語](../Page/英語.md "wikilink")：*Cryolite*）一种[矿物](../Page/矿物.md "wikilink")，主要成分为[六氟铝酸钠](../Page/六氟铝酸钠.md "wikilink")（Na<sub>3</sub>AlF<sub>6</sub>），[白色](../Page/白色.md "wikilink")[单斜晶系](../Page/单斜晶系.md "wikilink")（109摄氏度），微溶于[水](../Page/水.md "wikilink")，能溶于[氧化铝](../Page/氧化铝.md "wikilink")，在[电解铝工业作](../Page/电解.md "wikilink")[助熔剂](../Page/助熔剂.md "wikilink")、制造乳白色[玻璃和](../Page/玻璃.md "wikilink")[搪瓷的](../Page/搪瓷.md "wikilink")[遮光剂](../Page/遮光剂.md "wikilink")。[格陵蘭西海岸的](../Page/格陵蘭.md "wikilink")[伊維圖特](../Page/伊維圖特.md "wikilink")（）是冰晶石的主要產地，此矿于1987年开采完毕。现时多以[萤石](../Page/萤石.md "wikilink")[人工合成六氟铝酸钠供工业使用](../Page/人工合成.md "wikilink")。

## 使用

冰晶石可用作在[霍尔－埃鲁法中還原](../Page/霍尔－埃鲁法.md "wikilink")[氧化鋁之電解質](../Page/氧化鋁.md "wikilink")，能溶于氧化铝，在[电解铝工业作助熔剂](../Page/霍尔－埃鲁法.md "wikilink")，被用于[铝的提炼](../Page/铝.md "wikilink")。冰晶石可用作陶瓷、殺蟲劑\[1\]、研磨料結合劑、電子絕緣、光亮劑等。可由[氟化鈉与](../Page/氟化鈉.md "wikilink")[氟化鋁熔融反应制得](../Page/氟化鋁.md "wikilink")。

## 參考

  - [冰晶石 - Webmineral](http://webmineral.com/data/Cryolite.shtml)
  - [冰晶石 - Mindat](http://www.mindat.org/min-1161.html)

[Category:含铝矿物](../Category/含铝矿物.md "wikilink")
[Category:鹵化物礦物](../Category/鹵化物礦物.md "wikilink")
[Category:格陵兰历史](../Category/格陵兰历史.md "wikilink")
[Category:格陵兰自然史](../Category/格陵兰自然史.md "wikilink")
[Category:烟火着色剂](../Category/烟火着色剂.md "wikilink")
[Category:含钠矿物](../Category/含钠矿物.md "wikilink")
[Category:单斜晶系矿物](../Category/单斜晶系矿物.md "wikilink")

1.  EPA R.E.D. FACTS Cryolite