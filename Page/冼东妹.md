**冼東妹**（），[廣東](../Page/廣東.md "wikilink")[四會人](../Page/四會.md "wikilink")，前[中國女子](../Page/中國.md "wikilink")[柔道運動員](../Page/柔道.md "wikilink")。现任中国柔道协会主席、中国国家柔道队总教练。

## 生涯

冼東妹於年青時入讀广东省四会市业余体校，一年后，冼東妹进入广东省体校練習[摔跤](../Page/摔跤.md "wikilink")，之後的1年轉到广东省体校练[柔道](../Page/柔道.md "wikilink")。3年後，冼東妹入选国家隊。

冼東妹還是在練習角力時，曾獲得了全国女子摔跤锦标赛亞軍。其後在練柔道後的兩年贏得了全国青年赛冠军。於1995年的[亚洲锦标赛中獲亞](../Page/亚洲.md "wikilink")，1997年奪得了[八運會的](../Page/八運會.md "wikilink")[金牌](../Page/金牌.md "wikilink")。

於2001年至2002年先後奪得了[世界大学生運動會](../Page/世界大学生運動會.md "wikilink")[金牌](../Page/金牌.md "wikilink")、[九運會](../Page/九運會.md "wikilink")[金牌](../Page/金牌.md "wikilink")、[釜山亞運會](../Page/2002年亞洲運動會.md "wikilink")[銀牌](../Page/銀牌.md "wikilink")。兩年後，先後贏得了[法國公開賽中的冠军](../Page/法國.md "wikilink")、亚洲锦标赛亚军，又在[德國的世界杯賽事中奪取冠軍](../Page/德國.md "wikilink")。

## 奥运冠军

[雅典奥运会上](../Page/2004年夏季奧林匹克運動會.md "wikilink")，冼东妹一路杀进决赛，在决赛中仅用时1分鐘07秒一本淘汰[日本選手橫澤由貴](../Page/日本.md "wikilink")，贏得了女子52公斤级的[柔道](../Page/柔道.md "wikilink")[金牌](../Page/金牌.md "wikilink")，該面金牌是[中國在](../Page/中國.md "wikilink")[雅典的第](../Page/雅典.md "wikilink")5面金牌。

在[2008年北京奥运会中](../Page/2008年北京奥运会.md "wikilink")，她在此晋级決賽。在决赛中击败[朝鮮的选手](../Page/朝鮮.md "wikilink")[安琴愛](../Page/安琴愛.md "wikilink")，成功衛冕柔道金牌，成為首位妈妈级的中國奥运冠军。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [冼东妹](http://www.020vips.com/mingrentang/180.html) 广州名人网

[冼東妹](../Category/肇慶人.md "wikilink")
[Category:中国柔道运动员](../Category/中国柔道运动员.md "wikilink")
[Dongmei](../Category/冼姓.md "wikilink")
[Category:中国奥林匹克运动会金牌得主](../Category/中国奥林匹克运动会金牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會金牌得主](../Category/2004年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:2008年夏季奧林匹克運動會獎牌得主](../Category/2008年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會柔道運動員](../Category/2004年夏季奧林匹克運動會柔道運動員.md "wikilink")
[Category:2008年夏季奧林匹克運動會柔道運動員](../Category/2008年夏季奧林匹克運動會柔道運動員.md "wikilink")
[Category:奧林匹克運動會柔道獎牌得主](../Category/奧林匹克運動會柔道獎牌得主.md "wikilink")
[Category:2002年亞洲運動會銀牌得主](../Category/2002年亞洲運動會銀牌得主.md "wikilink")
[Category:中国奥运柔道运动员](../Category/中国奥运柔道运动员.md "wikilink")
[Category:中国柔道协会主席](../Category/中国柔道协会主席.md "wikilink")

1.