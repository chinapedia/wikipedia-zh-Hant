**准噶尔汗国**（；；[维吾尔语](../Page/维吾尔语.md "wikilink")：），是[卫拉特](../Page/卫拉特.md "wikilink")[蒙古的一支](../Page/蒙古.md "wikilink")[准噶尔部建立的一个](../Page/准噶尔部.md "wikilink")[汗国](../Page/汗国.md "wikilink")。17世纪到18世纪中叶，准噶尔部控制[天山南北](../Page/天山.md "wikilink")，在西起[巴尔喀什湖](../Page/巴尔喀什湖.md "wikilink")，北越[阿尔泰山](../Page/阿尔泰山.md "wikilink")，东到[吐鲁番](../Page/吐鲁番.md "wikilink")，西南至[楚河](../Page/楚河.md "wikilink")、[塔拉斯河的广大地区](../Page/塔拉斯河.md "wikilink")，建立史上最后的[游牧帝国](../Page/游牧.md "wikilink")。由于此时期是[卫拉特](../Page/卫拉特.md "wikilink")[蒙古历史上的全盛时期](../Page/蒙古.md "wikilink")，卫拉特蒙古常常就以准噶尔的名称出现于史料中。[宗教上他们信奉](../Page/宗教.md "wikilink")[藏传佛教](../Page/藏传佛教.md "wikilink")，对[西藏也有一定的影响力](../Page/西藏.md "wikilink")。18世纪在[清朝几次军事行动中灭亡](../Page/清朝.md "wikilink")。

## 兴亡历史

### 興起

[明末清初](../Page/明.md "wikilink")，准噶尔首领最早是[哈剌忽剌](../Page/哈剌忽剌.md "wikilink")，後[巴图尔珲台吉](../Page/巴图尔珲台吉.md "wikilink")1635年即珲台吉位，**准噶尔汗国**正式成立，以[伊犁为根据地兼并](../Page/伊犁.md "wikilink")[厄鲁特蒙古各部](../Page/厄鲁特.md "wikilink")，压迫西方的[哈萨克族](../Page/哈萨克族.md "wikilink")。1670年巴图尔珲台吉的儿子[僧格被杀后](../Page/僧格.md "wikilink")，他的异母弟[噶尔丹夺得了准噶尔的统治权](../Page/噶尔丹.md "wikilink")。1678年，西藏的[五世达赖赐噶尔丹](../Page/五世达赖.md "wikilink")[可汗号](../Page/可汗.md "wikilink")（噶尔丹为准噶尔汗国统治者中唯一一位称汗者）。此年噶尔丹出兵南疆，占领[叶尔羌汗国](../Page/叶尔羌汗国.md "wikilink")，把广大[维吾尔族地区置于其统治之下](../Page/维吾尔族.md "wikilink")。1688年噶尔丹突然率兵越过[杭爱山](../Page/杭爱山.md "wikilink")，大举进攻[土谢图汗](../Page/土谢图汗.md "wikilink")，迫使[喀尔喀蒙古诸部南迁](../Page/喀尔喀.md "wikilink")。1690年6月，噶尔丹又向漠南喀尔喀蒙古进攻，俘掠人口，抢劫牲畜。同年，[康熙帝决计亲征](../Page/康熙帝.md "wikilink")，组织左右两路大军，分别出[古北口和](../Page/古北口.md "wikilink")[喜峰口](../Page/喜峰口.md "wikilink")。8月1日，双方大战于[乌兰布通](../Page/乌兰布通之戰.md "wikilink")（今[内蒙古](../Page/内蒙古.md "wikilink")[克什克腾旗境内](../Page/克什克腾.md "wikilink")）。清军大败噶尔丹军，噶尔丹乘夜向北溃逃。

噶尔丹战败后因原根据地为[策妄阿拉布坦占据](../Page/策妄阿拉布坦.md "wikilink")，不得不滞留[科布多地区](../Page/科布多.md "wikilink")，集合残部，休养生息，以期东山再起，不断骚扰边地安宁。1696年2月，康熙帝发兵10万，分三路大举出击。5月13日，西路军在[昭莫多](../Page/昭莫多战役.md "wikilink")（今[蒙古国](../Page/蒙古国.md "wikilink")[乌兰巴托以南的](../Page/乌兰巴托.md "wikilink")[宗莫德](../Page/宗莫德.md "wikilink")）大败噶尔丹军。1697年2月，康熙帝發動第三次遠征，命[费扬古](../Page/费扬古.md "wikilink")、[马恩哈分别统率两路大军](../Page/马恩哈.md "wikilink")，共6000人，由[宁夏出发](../Page/宁夏.md "wikilink")，进剿噶尔丹残部。4月康熙帝亲赴宁夏，指挥这次军事行动。正当清军进发之时，噶尔丹众叛亲离，军队只剩下五六百人，遂饮药自尽（又说被人杀害）。

噶尔丹败亡后，他的侄子[策妄阿拉布坦继续任准噶尔部台吉](../Page/策妄阿拉布坦.md "wikilink")，准噶尔部又逐渐强大起来，和清朝再次发生矛盾冲突。1716年，他派兵于[西藏](../Page/西藏.md "wikilink")，占领[拉萨](../Page/拉萨.md "wikilink")。1718年清朝由[青海路出兵入藏](../Page/青海.md "wikilink")，不过全军覆没。1720年清朝第二次出兵才赶走准噶尔军。清朝和准噶尔军在[吐鲁番方面也有战斗](../Page/吐鲁番.md "wikilink")。

### 滅亡

1755年春，清军5万人分西北两路向[伊犁进军](../Page/伊犁.md "wikilink")。不到100天就到达伊犁。[达瓦齐率兵](../Page/达瓦齐.md "wikilink")6000人扼守伊犁西南的格登山（今[新疆昭苏县境内](../Page/新疆.md "wikilink")），清军以25人夜袭达瓦齐大营，达瓦齐军惊溃，不战自降。达瓦齐仅带少数人仓皇南逃。此后，[阿睦尔撒纳自任领袖继续反对清朝](../Page/阿睦尔撒纳.md "wikilink")。1757年初，清政府开始新的军事行动，分两路推进。准噶尔军在清军的追剿下，全线溃败。阿睦尔撒纳投奔[俄罗斯帝国](../Page/俄罗斯帝国.md "wikilink")。

1759年8月，清军抵达[喀什噶尔](../Page/喀什噶尔.md "wikilink")，平定了支持阿睦尔撒纳的大和卓[波罗尼都和](../Page/波罗尼都.md "wikilink")[小和卓霍集占兄弟的](../Page/小和卓霍集占.md "wikilink")[叛乱](../Page/大小和卓之乱.md "wikilink")。这年秋天，[天山南路完全平定](../Page/天山.md "wikilink")，清朝完全控制今日的[新疆地区](../Page/新疆.md "wikilink")。由于准噶尔部反叛无常，此后清军在[兆惠等的带领下](../Page/兆惠.md "wikilink")，按照[乾隆帝的旨意消灭了剩余的准噶尔人](../Page/乾隆帝.md "wikilink")\[1\]，除病死或逃往国外的以外，數十萬準噶爾人遭到[屠殺](../Page/屠殺.md "wikilink")。据[魏源記載](../Page/魏源.md "wikilink")：「計數十萬戶中，先痘死者十之四，繼竄入俄羅斯哈薩克者十之二，卒殲於大兵者十之三。除婦孺充賞外，至今惟來降受屯之厄魯特若干戶，編設佐領昂吉，此外數千里間，無瓦剌一氊帳。」\[2\][昭槤的描寫更加誇張](../Page/昭槤.md "wikilink")：「凡病死者十之三；逃入俄羅斯、哈薩克者十之三，為我兵殺者十之五，數千里內遂無一人」\[3\]。留下的空地由大量移民填充。\[4\]

## 历代准噶尔大汗

  - 准噶尔[大汗](../Page/蒙古大汗.md "wikilink")（1634年－1757年）

<!-- end list -->

1.  [巴圖爾琿台吉](../Page/巴圖爾琿台吉.md "wikilink") （19）
    [甲戌](../Page/甲戌.md "wikilink") 1634年
2.  [僧格](../Page/僧格.md "wikilink") （18） [癸巳](../Page/癸巳.md "wikilink")
    1653年
3.  [索诺木阿拉布坦](../Page/索诺木阿拉布坦.md "wikilink") （2）
    [辛亥](../Page/辛亥.md "wikilink") 1671年
4.  [噶尔丹](../Page/噶尔丹.md "wikilink") （16） [癸丑](../Page/癸丑.md "wikilink")
    1673年
5.  [策妄阿拉布坦](../Page/策妄阿拉布坦.md "wikilink") （30）
    [丁丑](../Page/丁丑.md "wikilink") 1697年
6.  [噶尔丹策零](../Page/噶尔丹策零.md "wikilink") （18）
    [丁未](../Page/丁未.md "wikilink") 1727年
7.  [策妄多爾濟那木扎爾](../Page/策妄多爾濟那木扎爾.md "wikilink") （5）
    [乙丑](../Page/乙丑.md "wikilink") 1745年
8.  [喇嘛达尔扎](../Page/喇嘛达尔扎.md "wikilink") （3）
    [庚申](../Page/庚申.md "wikilink") 1750年
9.  [达瓦齐](../Page/达瓦齐.md "wikilink") （2） [癸酉](../Page/癸酉.md "wikilink")
    1753年
10. [阿睦尔撒纳本](../Page/阿睦爾撒纳.md "wikilink") （2）
    [乙亥](../Page/乙亥.md "wikilink") 1755年

## 参考文献

## 研究書目

  - 宮脇淳子 著，曉克 譯：《最後的游牧民族：準噶爾部的興亡》（呼和浩特：內蒙古人民出版社，2005）。
  - Thomas Barfield 著，袁劍 譯：《危險的邊疆：游牧帝國與中國》（南京：江蘇人民出版社，2011）。

{{-}}

[Category:蒙古汗国](../Category/蒙古汗国.md "wikilink")
[准噶尔汗国](../Category/准噶尔汗国.md "wikilink")
[Category:清朝蒙古](../Category/清朝蒙古.md "wikilink")

1.  《大清高宗纯皇帝实录》乾隆二十四年：“此次进兵非同一般，各将厄鲁特彻底剿灭，永绝根株”
2.  [魏源](../Page/魏源.md "wikilink")，《聖武記》，卷四，一一至一二。
3.  [昭梿](../Page/昭梿.md "wikilink")，《啸亭杂录》，卷三。
4.  [勒内·格鲁塞](../Page/勒内·格鲁塞.md "wikilink")·《[草原帝国](../Page/草原帝国.md "wikilink")》：“清朝以来自各地的移民充实其地，其中有吉尔吉斯—哈萨克人，来自喀什噶尔的[塔兰奇人](../Page/塔兰奇.md "wikilink")，或穆斯林，来自甘肃的[东-{干}-人](../Page/東干人.md "wikilink")（或回民），[察哈尔和](../Page/察哈尔.md "wikilink")[喀尔喀居民](../Page/喀尔喀.md "wikilink")，[图瓦族的兀良哈人](../Page/图瓦族.md "wikilink")（或称索约特人），甚至有来自满洲的[锡伯族和](../Page/锡伯族.md "wikilink")[高丽族移民](../Page/高丽族.md "wikilink")”