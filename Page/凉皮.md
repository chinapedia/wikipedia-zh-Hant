[thumb](../Page/文件:汉中面皮.jpg.md "wikilink")
**凉皮**是[中国西部众多地区的风味](../Page/中国西部.md "wikilink")[小吃](../Page/小吃.md "wikilink")，被许多人认为是[陕西小吃中最受欢迎的品种](../Page/陕西.md "wikilink")。
[Wuhan-Entering-Jiangxia-from-Hongshan-4265.jpg](https://zh.wikipedia.org/wiki/File:Wuhan-Entering-Jiangxia-from-Hongshan-4265.jpg "fig:Wuhan-Entering-Jiangxia-from-Hongshan-4265.jpg")
凉皮，又称**皮子**、**酿皮子**、**麵皮**，据说是从[唐代](../Page/唐代.md "wikilink")“冷淘麵”演变而来，以“白、薄、光、软、酿、香”而闻名。凉皮一年四季都可以吃到，因为“凉”，所以在夏天吃的人更多。

凉皮的种类繁多，做法各具特色，口味也不尽相同。从制作方法上可大体分为：蒸麵皮、擀麵皮和烙麵皮。蒸麵皮的主要制作方法是由[面粉](../Page/面粉.md "wikilink")（将[面筋洗出](../Page/麩質.md "wikilink")）或米粉加水均匀搅拌製成糊状，盛入圆形平底的金属容器，摇摆凉皮容器使麵/米糊平展得铺在容器底，然后放入开水锅或蒸笼内蒸製（蒸熟后的圆形整张皮子大约0.5厘米厚，直径近1米）。随后把凉皮过凉水冷却，用近1米长、20余厘米宽的大铡刀切成宽0.5厘米至2厘米的长条（根据製作材料不同，颜色有稍许不同），调拌时可根据不同风味加入[盐](../Page/食盐.md "wikilink")、[醋](../Page/醋.md "wikilink")、[芝麻酱](../Page/芝麻酱.md "wikilink")、[辣椒油](../Page/辣椒.md "wikilink")、[麵筋](../Page/麵筋_\(食品\).md "wikilink")、[黄瓜丝](../Page/黄瓜.md "wikilink")、[豆芽](../Page/豆芽.md "wikilink")、[蒜汁和时令](../Page/蒜.md "wikilink")[蔬菜等](../Page/蔬菜.md "wikilink")。

現在凉皮是[北京等大城市的街头快餐之一](../Page/北京.md "wikilink")。

## 皮子种类

[Shanxi_Liangpi_陕西_凉皮.jpg](https://zh.wikipedia.org/wiki/File:Shanxi_Liangpi_陕西_凉皮.jpg "fig:Shanxi_Liangpi_陕西_凉皮.jpg")\]\]
从吃法上分为凉皮和热皮。从原料上分为米皮（大米）和麵皮（小麦）\[1\]。

再次从口味及配料上可分为：

### 面筋凉皮

盛行于[关中地区](../Page/关中.md "wikilink")，以面筋为主要辅料。调制时，须加入面筋块、时令蔬菜；调味品有[醋](../Page/醋.md "wikilink")、酱、蒜汁、[味精](../Page/味精.md "wikilink")、[盐](../Page/食盐.md "wikilink")、[辣油](../Page/辣椒油.md "wikilink")、[香油等](../Page/香油.md "wikilink")。

### 麻酱凉皮

以[芝麻酱做调拌而得名](../Page/芝麻酱.md "wikilink")，为[清真食品](../Page/清真菜.md "wikilink")。调制时一般加辅料为黄瓜丝，并调入盐、醋、酱、芝麻酱、粗辣椒油（有辣椒粒）等。

### 濮阳凉皮

以[一机厂凉皮](http://ha.people.com.cn/n/2014/0807/c351638-21908214.html)为代表，有调凉皮和裹凉皮两种，加入辅料黄瓜丝和面筋，调味品有醋、酱、芝麻酱、盐等。[LIANG_PI.jpg](https://zh.wikipedia.org/wiki/File:LIANG_PI.jpg "fig:LIANG_PI.jpg")

### 秦镇凉皮

因产于[户县的](../Page/户县.md "wikilink")[秦镇](../Page/秦渡镇.md "wikilink")，又以大米粉为原料製成，也叫秦镇米皮。制作时把米粉加水搅拌为糊状，平铺在多层竹蒸笼内，旺火蒸熟，切细。调制时加入辅料青菜、小豆芽等；调入佐料较少，重点为辣椒油（制作较为讲究，辣椒麵加入花椒、茴香等大料小火反复在上等油中熬制，无辣椒粒）。调好的凉皮呈红色，辣且香\[2\]。
2007年5月，**秦镇米皮制作技艺**被列入[陕西省第一批非物质文化遗产名录](../Page/陕西省.md "wikilink")\[3\]。

### 汉中麵皮

因产于[汉中地区而得名](../Page/汉中.md "wikilink")。面皮是当地的习惯称呼，实际是由大米制成而非面粉。由于加工时用小石磨加水将米磨製成米粉浆，又叫水磨面皮。制作时将磨制成的米粉浆平铺於竹蒸笼上蒸制。加入调料，主要是蒜汁、辣椒油，口味则是酸辣之中透着蒜香。

### 岐山擀麵皮

以[岐山县製作的为最佳](../Page/岐山县.md "wikilink")。製作时，先用将小麦粉用水洗，洗出“麵水”，也就是淀粉水，上蒸笼蒸製，然后用幹杖幹好。调料以当地特别酿製的粮食醋和辣椒油为主，并辅以[小麦粉洗出的麵筋丝](../Page/小麦.md "wikilink")，[蛋白质含量较高](../Page/蛋白质.md "wikilink")，擀面皮的麵筋丝和蒸麵皮不同，在于“上上”用水煮，而非上笼蒸，煮熟后，然后用手撕开，在小锅（有锅无灶）内调拌均匀。其主要特点是凉皮为半透明状，筋道而伴有麦香，口味为酸、辣、香。

岐山麵皮种类繁多，分为擀、蒸、烙多种。

相传[康熙年间](../Page/康熙.md "wikilink")，岐山城北八亩沟村民**王同仁**在[御膳房专做面皮](../Page/御膳房.md "wikilink")，因用面粉制作，又是京城御膳，故称“御京粉”。王同仁年老还乡后，专事经营面皮小吃，逐渐传播关中，成为[宝鸡名吃](../Page/宝鸡.md "wikilink")\[4\]。

除以上各具风味的凉皮之外，还有[扶风县的烙麵皮](../Page/扶风县.md "wikilink")、汉中的[魔芋凉皮](../Page/魔芋.md "wikilink")、[黑米凉皮](../Page/黑米.md "wikilink")、[陕北](../Page/陕北.md "wikilink")[绿豆凉皮](../Page/绿豆.md "wikilink")、[汉中熱麵皮等](../Page/汉中.md "wikilink")。

## 参考资料

## 外部链接

  - [凉皮儿制作过程图解](https://web.archive.org/web/20130629225655/http://food.gog.com.cn/system/2009/03/10/010509760.shtml)
  - [秦朝贡品：辣香诱人的陕西凉皮](http://bj.sina.com.cn/20021029/28716.shtml)

[Category:陕西小吃](../Category/陕西小吃.md "wikilink")
[Category:涼麵](../Category/涼麵.md "wikilink")
[Category:西安小吃](../Category/西安小吃.md "wikilink")

1.
2.
3.
4.