**凌兆新村站**位于[上海](../Page/上海.md "wikilink")[浦东新区](../Page/浦东新区.md "wikilink")[济阳路](../Page/济阳路.md "wikilink")[凌兆路](../Page/凌兆路.md "wikilink")，为[上海轨道交通8号线二期地下岛式车站](../Page/上海轨道交通8号线.md "wikilink")，于2009年7月5日启用。

## 车站出口

  - 1号口：济阳路西侧，凌兆路北
  - 2号口：济阳路西侧，凌兆路与济阳路丁字路口处

注：2号口设有无障碍电梯及洗手间

## 公交换乘

583、755、761、873、955、973、976、978、986

## 参考资料

1.  [上海市城市规划管理局－建设用地规划许可证－凌兆路站](https://web.archive.org/web/20070927193101/http://www.shghj.gov.cn/Ct_3.aspx?ct_id=00060307E03060)
2.  [上海市城市规划管理局－建设工程竣工规划验收合格证－凌兆新村站](https://web.archive.org/web/20070927192431/http://www.shghj.gov.cn/Ct_4.aspx?ct_id=00070725D11753)
3.  [东方网上海频道(2007年12月11日) - 浦东轨交"新四线"2009年底全建成
    动迁已基本完成](http://xwwb.eastday.com/x/20071211/u1a382283.html)
4.  [文新传媒：轨交8号线还要往南延伸到航天公园站](http://topic.news365.com.cn/jjstxg_4090/200712/t20071205_1675008.htm)

[Category:浦东新区地铁车站](../Category/浦东新区地铁车站.md "wikilink")
[Category:2007年启用的铁路车站](../Category/2007年启用的铁路车站.md "wikilink")