**凌海市**是[中国](../Page/中国.md "wikilink")[辽宁省](../Page/辽宁省.md "wikilink")[锦州市代管的一个](../Page/锦州市.md "wikilink")[县级市](../Page/县级市.md "wikilink")，[大凌河和](../Page/大凌河.md "wikilink")[小凌河横贯](../Page/小凌河.md "wikilink")。

## 历史

[辽朝始设锦州临海军](../Page/辽朝.md "wikilink")，置永乐县，属京中道；[清朝康熙元年七月](../Page/清朝.md "wikilink")（1662年）改锦州为锦县，隶属[奉天府](../Page/奉天府.md "wikilink")；1993年11月16日撤县建市，为凌海市。

## 行政区划

凌海市现辖2个[街道](../Page/街道.md "wikilink")，11个[镇](../Page/镇.md "wikilink")，8个[乡](../Page/乡.md "wikilink")，1个农场。下设440个[村](../Page/村.md "wikilink")、32个[社区](../Page/社区.md "wikilink")。

  - 2个街道为：大凌河街道、金城街道。
  - 11个镇为：石山镇、余积镇、双羊镇、班吉塔镇、沈家台镇、三台子镇、右卫满族镇、闫家镇、新庄子镇、娘娘宫镇、翠岩镇。
  - 8个乡为：大业乡、西八千乡、建业乡、温滴楼满族乡、白台子乡、谢屯乡、安屯乡、板石沟乡。
  - 1个农场为：大有农场。

## 参考文献

[凌海市](../Category/凌海市.md "wikilink")
[市](../Category/锦州区县市.md "wikilink")
[锦州](../Category/辽宁县级市.md "wikilink")