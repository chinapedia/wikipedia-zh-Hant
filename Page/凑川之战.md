**凑川之战**（），是[日本历史上重要](../Page/日本历史.md "wikilink")[战役之一](../Page/战役.md "wikilink")。為[後醍醐天皇與](../Page/後醍醐天皇.md "wikilink")[足利尊氏的決戰](../Page/足利尊氏.md "wikilink")，以[足利氏的勝利告終](../Page/足利氏.md "wikilink")。湊川為[日本古地名](../Page/日本.md "wikilink")，即今[兵庫縣](../Page/兵庫縣.md "wikilink")[神戶市](../Page/神戶市.md "wikilink")。著名將領[楠木正成在此戰中陣亡](../Page/楠木正成.md "wikilink")。

## 簡介

[日本](../Page/日本.md "wikilink")[後醍醐天皇](../Page/後醍醐天皇.md "wikilink")[建武三年](../Page/建武.md "wikilink")(1336年)1月，[足利尊氏及弟](../Page/足利尊氏.md "wikilink")[足利直義從](../Page/足利直義.md "wikilink")[京都敗走](../Page/京都.md "wikilink")，翌月逃至[九州](../Page/九州.md "wikilink")，4月時在[博多重新集結大軍分海陸二路向京都進兵](../Page/博多.md "wikilink")。尊氏率領海路並與[細川氏統率的](../Page/細川氏.md "wikilink")[四國地區兵力在海上會合](../Page/四國.md "wikilink")，直義則率領陸路，一同進逼[京畿](../Page/京畿.md "wikilink")。

[楠木正成見足利軍勢龐大](../Page/楠木正成.md "wikilink")，京都現有兵力並不足以對抗，便建議後醍醐天皇臨幸[行在暫避軍勢](../Page/行在.md "wikilink")，他也可回自己[河內](../Page/河內國.md "wikilink")、[和泉二處領地招募兵力來對抗足利大軍](../Page/和泉國.md "wikilink")。但[公家大臣仍沉溺在年初收復京都的勝利中](../Page/公家.md "wikilink")，認為足利軍不可怕，託言官軍有帝祖[天照大神庇護](../Page/天照大神.md "wikilink")，必能戰勝敵人，反對[遷都要求迎戰](../Page/遷都.md "wikilink")，[天皇聽信讒言](../Page/天皇.md "wikilink")，強令楠木正成與[新田義貞二人出戰](../Page/新田義貞.md "wikilink")。

楠木與新田二人無奈率兵出擊，缺乏[水師的楠木新田軍在陸上佈陣迎擊足利軍](../Page/水師.md "wikilink")，新田軍在湊川佈陣，楠木軍則在湊川以西的西野宿佈陣，而楠木軍兵力僅數百人左右。戰役之初，足利海路東進軍[細川定禪在湊川附近的](../Page/細川定禪.md "wikilink")[生田森林登陸](../Page/生田.md "wikilink")，此時新田義貞感到自己後路有被截斷之虞連忙退兵，獨留楠木正成迎戰足利直義的數萬足利陸路軍。楠木正成雖兵力居於劣勢，仍舊奮戰到底，但寡不敵眾兵敗湊川。

足利直義遣使招降[楠木正成](../Page/楠木正成.md "wikilink")，但被拒絕。楠木正成見大勢已去又不願投降，便對其弟[楠木正季說](../Page/楠木正季.md "wikilink")：「我願七生轉世報效國家。」與正季對刺[自殺殉國](../Page/自殺.md "wikilink")。

楠木正成死前遺言「七生報國」後來成為[第二次世界大戰](../Page/第二次世界大戰.md "wikilink")[日軍的口號](../Page/日軍.md "wikilink")，也是[神風特攻隊的精神訓令](../Page/神風特攻隊.md "wikilink")，特攻隊員頭上綁著寫有「七生報國」的頭巾，駕駛[飛機執行](../Page/飛機.md "wikilink")[自殺式攻擊任務](../Page/自殺式攻擊.md "wikilink")。

## 關連項目

  - [湊川神社](../Page/湊川神社.md "wikilink")

[Category:南北朝時代戰役 (日本)](../Category/南北朝時代戰役_\(日本\).md "wikilink")
[Category:攝津國](../Category/攝津國.md "wikilink") [Category:中央區
(神戶市)](../Category/中央區_\(神戶市\).md "wikilink")
[Category:新田氏](../Category/新田氏.md "wikilink")
[Category:大館氏](../Category/大館氏.md "wikilink")
[Category:菊池氏](../Category/菊池氏.md "wikilink")
[Category:足利氏](../Category/足利氏.md "wikilink")
[Category:高氏](../Category/高氏.md "wikilink")
[Category:少貳氏](../Category/少貳氏.md "wikilink")
[Category:赤松氏](../Category/赤松氏.md "wikilink")
[Category:奧州細川氏](../Category/奧州細川氏.md "wikilink")
[Category:斯波氏](../Category/斯波氏.md "wikilink")
[Category:兵庫區](../Category/兵庫區.md "wikilink")