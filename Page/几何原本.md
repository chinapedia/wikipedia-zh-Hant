[Ricci1.jpg](https://zh.wikipedia.org/wiki/File:Ricci1.jpg "fig:Ricci1.jpg")和[徐光启](../Page/徐光启.md "wikilink")\]\]
[Preface_of_Euclid's_Elements_by_Xu_Guangqi.jpg](https://zh.wikipedia.org/wiki/File:Preface_of_Euclid's_Elements_by_Xu_Guangqi.jpg "fig:Preface_of_Euclid's_Elements_by_Xu_Guangqi.jpg")手书《刻〈几何原本〉序》\]\]

《**几何原本**》（，）是[古希腊数学家](../Page/古希腊数学.md "wikilink")[欧几里得所著的一部](../Page/欧几里得.md "wikilink")[数学著作](../Page/数学.md "wikilink")，共13卷。这本著作是现代[数学的基础](../Page/数学.md "wikilink")，据估计在[西方是仅次于](../Page/西方.md "wikilink")《[圣经](../Page/圣经.md "wikilink")》的出版版本最多的书籍\[1\]。在[四庫全書中歸賴於子部天文演算法算書類](../Page/四庫全書.md "wikilink")。

## 章节大纲

[欧几里得所著的](../Page/欧几里得.md "wikilink")《**几何原本**》共分13卷。\[2\]

第一卷至第六卷的内容主要为[平面几何](../Page/平面几何.md "wikilink")。

  - 第一卷：几何基础。本卷确立了基本定义、[公设和](../Page/公设.md "wikilink")[公理](../Page/公理.md "wikilink")，还包括一些关于[全等形](../Page/全等.md "wikilink")、[平行线和](../Page/平行线.md "wikilink")[直线形的熟知的](../Page/直线形.md "wikilink")[定理](../Page/定理.md "wikilink")。

<!-- end list -->

  - 第二卷：几何与代数。该卷主要讨论的是[毕达哥拉斯学派的几何代数学](../Page/毕达哥拉斯.md "wikilink")，主要包括大量[代数定理的几何](../Page/代数.md "wikilink")[证明](../Page/证明.md "wikilink")。

<!-- end list -->

  - 第三卷：圆与角。本卷阐述了[圆](../Page/圆.md "wikilink")、[弦](../Page/弦.md "wikilink")、[割线](../Page/割线.md "wikilink")、[切线](../Page/切线.md "wikilink")、[圆心角](../Page/圆心角.md "wikilink")、[圆周角的一些](../Page/圆周角.md "wikilink")[定理](../Page/定理.md "wikilink")。

<!-- end list -->

  - 第四卷：圆与正多边形。本卷讨论了已知圆的某些[内接和](../Page/内接.md "wikilink")[外切](../Page/外切.md "wikilink")[正多边形的](../Page/正多边形.md "wikilink")[尺规作图问题](../Page/尺规作图.md "wikilink")。

<!-- end list -->

  - 第五卷：比例。本卷对[欧多克索斯的](../Page/欧多克索斯.md "wikilink")[比例理论进行阐述](../Page/比例論.md "wikilink")，

<!-- end list -->

  - 第六卷：相似。本卷阐述了[比例的属性](../Page/比例.md "wikilink")，以及[相似形的概念](../Page/相似.md "wikilink")，包括了[泰勒斯定理](../Page/泰勒斯定理.md "wikilink")。

第七卷至第九卷主要阐述了[数论](../Page/数论.md "wikilink")。

  - 第七卷：数论（一）。本卷内容包括[整除性](../Page/整除性.md "wikilink")、[质数](../Page/质数.md "wikilink")、[最大公约数](../Page/最大公约数.md "wikilink")、[最小公倍数等](../Page/最小公倍数.md "wikilink")[初等数论内容](../Page/初等数论.md "wikilink")。

<!-- end list -->

  - 第八卷：数论（二）。本卷继续讨论[初等数论](../Page/初等数论.md "wikilink")，包括欧几里得[辗转相除法](../Page/辗转相除法.md "wikilink")、各种[数的关系](../Page/数.md "wikilink")（如[质数](../Page/质数.md "wikilink")、[合数](../Page/合数.md "wikilink")、[平方数](../Page/平方数.md "wikilink")、[立方数等](../Page/立方数.md "wikilink")）。

<!-- end list -->

  - 第九卷：数论（三）。本卷设计了[比例](../Page/比例.md "wikilink")、[几何级数](../Page/几何级数.md "wikilink")，给出了许多重要的[初等数论定理](../Page/初等数论.md "wikilink")。

第十卷讨论了[无理数](../Page/无理数.md "wikilink")。

  - 第十卷：无理数。本卷定义了[无理量](../Page/无理数.md "wikilink")（即[不可公约量](../Page/通約性.md "wikilink")），并蕴含了[极限思想](../Page/极限.md "wikilink")（如[穷竭法](../Page/穷竭法.md "wikilink")）。本卷篇幅最大，也较不易理解。

第11卷至第13卷主要讨论[立体几何](../Page/立体几何.md "wikilink")。

  - 第11卷：立体几何。本卷论述立体几何；将第一卷至第六卷的主要内容推广至[立体](../Page/立体几何.md "wikilink")，如[平行](../Page/平行.md "wikilink")、[垂直以及立体图形的](../Page/垂直.md "wikilink")[体积](../Page/体积.md "wikilink")。

<!-- end list -->

  - 第12卷：立体的测量。本卷重在讨论立体图形的体积，例如[棱柱](../Page/棱柱.md "wikilink")、[棱锥](../Page/棱锥.md "wikilink")、[圆柱](../Page/圆柱.md "wikilink")、[圆锥以至](../Page/圆锥.md "wikilink")[球体的体积](../Page/球_\(数学\).md "wikilink")。

<!-- end list -->

  - 第13卷：建正多面体。本卷重点研究[正多面体的作图](../Page/正多面体.md "wikilink")。包含了五种[正多面体的作图](../Page/正多面体.md "wikilink")，并证明了不存在更多的[正多面体](../Page/正多面体.md "wikilink")。

## 歷史

[Jiheyuanben.JPG](https://zh.wikipedia.org/wiki/File:Jiheyuanben.JPG "fig:Jiheyuanben.JPG")
几何原本被很多学者认为是欧几里得把很多前人所证明的原理以及自己的一些原创证明汇集在一起的著作，古希腊的一名历史学家[普罗克洛就这样认为](../Page/普罗克洛.md "wikilink")。

歐幾里得約於西元前300年寫成《几何原本》。

它翻譯成[阿拉伯文](../Page/阿拉伯文.md "wikilink")，然後再二手翻譯成[拉丁文](../Page/拉丁文.md "wikilink")。最先的印制本出現於1482年。希臘語版本仍然存在於各地，如[梵蒂岡教廷圖書館或](../Page/梵蒂岡教廷圖書館.md "wikilink")[牛津大學的](../Page/牛津大學.md "wikilink")[博德利圖書館](../Page/博德利圖書館.md "wikilink")。遺憾的是這些現存手抄本品質參差而不完整。

[中国最早的译本是](../Page/中国.md "wikilink")1607年[意大利传教士](../Page/意大利.md "wikilink")[利玛窦和中国学者](../Page/利玛窦.md "wikilink")[徐光启根据](../Page/徐光启.md "wikilink")[德国神父](../Page/德国.md "wikilink")[克里斯托弗·克拉维乌斯校订增补的拉丁文本](../Page/克里斯托弗·克拉维乌斯.md "wikilink")《欧几里得原本》（15卷）合译的，定名为《几何原本》，[几何的中文名称就是由此而得来的](../Page/几何.md "wikilink")。他们只翻译了前6卷，后9卷由[英国人](../Page/英国.md "wikilink")[伟烈亚力和中国科学家](../Page/伟烈亚力.md "wikilink")[李善兰在](../Page/李善兰.md "wikilink")1857年译出。

[元朝](../Page/元朝.md "wikilink")[波斯人](../Page/波斯人.md "wikilink")[札马鲁丁在](../Page/札马鲁丁.md "wikilink")[秘书监时](../Page/秘书监.md "wikilink")，引进波斯文和阿拉伯文的各種數學與科學書籍，包括欧几里得《几何原本》，命名為《四擘算法段数》。但它大概並未翻譯成中文，也沒有引起學者注意。。

## 参见

  - [古希腊数学](../Page/古希腊数学.md "wikilink")
      - [欧几里得](../Page/欧几里得.md "wikilink")
  - [欧几里得几何](../Page/欧几里得几何.md "wikilink")
  - [文藝復興](../Page/文藝復興.md "wikilink")
  - [西学东渐](../Page/西学东渐.md "wikilink")
      - [利玛窦](../Page/利玛窦.md "wikilink")、[徐光启](../Page/徐光启.md "wikilink")
      - [偉烈亞力](../Page/偉烈亞力.md "wikilink")、[李善兰](../Page/李善兰.md "wikilink")

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - 《[续修四库全书](../Page/续修四库全书.md "wikilink")》. 1300年,
    [子部](../Page/子部.md "wikilink")·西学译著类 \[专著\] /
    [顾廷龙](../Page/顾廷龙.md "wikilink") 主编 ; 《续修四库全书》编纂委员会 编. -- .
    -- \[影印本\]. -- . -- 上海 : [上海古籍出版社](../Page/上海古籍出版社.md "wikilink"),
    2002. -- 866页 : 图 ; 26cm.

## 外部链接

  - [Euclid's
    Elements](http://aleph0.clarku.edu/~djoyce/java/elements/toc.html)（《**原本**》的在线英文版@美国[克拉克大学](../Page/克拉克大学.md "wikilink")）

  -
  - 陳方正：〈[《幾何原本》在不同文明之翻譯及命運初探](http://www.cuhk.edu.hk/ics/journal/articles/v48p193.pdf)〉。

{{-}}

[Category:欧几里得](../Category/欧几里得.md "wikilink")
[Category:數學書籍](../Category/數學書籍.md "wikilink")
[Category:明朝西方科技译著](../Category/明朝西方科技译著.md "wikilink")
[Category:徐光启译著](../Category/徐光启译著.md "wikilink")
[Category:利玛窦译著](../Category/利玛窦译著.md "wikilink")
[Category:清朝西方科技译著](../Category/清朝西方科技译著.md "wikilink")
[Category:李善兰](../Category/李善兰.md "wikilink")
[Category:南京书籍](../Category/南京书籍.md "wikilink")
[Category:前3世紀書籍](../Category/前3世紀書籍.md "wikilink")

1.
2.