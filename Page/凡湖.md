**凡湖**（，，\[1\]\[2\]）为一[鹹水湖](../Page/鹹水湖.md "wikilink")，位於[土耳其东部](../Page/土耳其.md "wikilink")、[阿勒山西南](../Page/阿勒山.md "wikilink")，是土耳其最大的[内陆湖](../Page/内陆湖.md "wikilink")。

面积3,755平方公里，长119公里，\[3\]
岸线长430公里；湖面海拔1,646米，平均深度约25米，最深处在100米以上。湖盆由[断层陷落再经过火山熔岩阻塞](../Page/断层.md "wikilink")[河谷而成](../Page/河谷.md "wikilink")。

該湖為一內流湖，形成於[更新世的火山活動](../Page/更新世.md "wikilink")。自1995年起當地傳出有[湖怪的消息](../Page/湖怪.md "wikilink")。

[凡城省首府](../Page/凡城省.md "wikilink")[凡城位于湖东岸](../Page/凡城.md "wikilink")。

## 参考文献

[Category:土耳其鹹水湖](../Category/土耳其鹹水湖.md "wikilink")
[Category:內流湖](../Category/內流湖.md "wikilink")

1.  [Rojnameya
    AVESTA](http://www.avestakurd.net/arshiv/Avesta%2053/metran%20isa.htm)
    . Avestakurd.net. Retrieved on 27 September 2010.
2.  [Kîne em ? –
    CD 5](http://www.institutkurde.org/products/details/143/).
    Institutkurde.org. Retrieved on 27 September 2010.
3.