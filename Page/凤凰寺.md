**凤凰寺**为中国东南沿海[伊斯兰教四大](../Page/中国伊斯兰教.md "wikilink")[清真古寺之一](../Page/清真寺.md "wikilink")（另三处为[扬州](../Page/扬州.md "wikilink")[仙鹤寺](../Page/仙鹤寺.md "wikilink")、[泉州](../Page/泉州.md "wikilink")[清净寺和](../Page/清净寺.md "wikilink")[广州](../Page/广州.md "wikilink")[怀圣寺](../Page/怀圣寺.md "wikilink")），位于[浙江省](../Page/浙江省.md "wikilink")[杭州市](../Page/杭州市.md "wikilink")[上城区中山中路西侧](../Page/上城区.md "wikilink")223号。该寺约始建于唐或宋，原名**真教寺**，宋末遭毁，元代得西域伊斯兰教大师[阿老丁](../Page/阿老丁.md "wikilink")（Ala
al-Din）捐资重建。民国十七年（1928）原照壁、寺门、望月楼和长廊等因道路拓宽拆除。1953年大修，至今保存较好\[1\]。因其建筑群形似[凤凰](../Page/凤凰.md "wikilink")，故清代时又称凤凰寺。

今全寺平面略呈矩形，由门厅（上建望月楼，为2009年重建）、礼拜殿、碑廊和其他附属建筑组成，其中主体建筑礼拜殿建于元世祖[至元十八年](../Page/至元_\(忽必烈\).md "wikilink")（1281），面阔三间，内以圆拱门相通，采用砖石结构[无梁殿](../Page/无梁殿.md "wikilink")，内壁于上端转角处作菱角牙子叠涩逐层收缩形成穹窿顶，其上绘有精美的彩画，以暗红、棕色等蔓卷纹为主，外观成三个[攒尖顶](../Page/攒尖顶.md "wikilink")，中间为重檐八角，两侧为单檐六角\[2\]。明间紧贴后墙中部有读经台，其上倚墙立有明[景泰二年](../Page/景泰.md "wikilink")（1451）木质红漆凹壁经版“天经一函”，上雕[阿拉伯文](../Page/阿拉伯文.md "wikilink")[古兰经](../Page/古兰经.md "wikilink")。大殿建筑风格不似传统伊斯兰清真寺，带有江南建筑独有的质朴和秀气。寺中还有阿老丁墓碑，墓志铭以阿拉伯文书写。

现凤凰寺依然是杭州[穆斯林的聚集中心](../Page/穆斯林.md "wikilink")，寺外有清真饭店。现任教长为[冶曼苏](../Page/冶曼苏.md "wikilink")\[3\]

## 图集

{{-}}  {{-}}

## 参考资料

[国](../Page/category:杭州文物保护单位.md "wikilink")
[category:杭州清真寺](../Page/category:杭州清真寺.md "wikilink")
[category:元朝建筑](../Page/category:元朝建筑.md "wikilink")

[Category:上城区](../Category/上城区.md "wikilink")

1.
2.
3.  [杭州政协新闻网](http://hzzx.gov.cn/zxgk/content/2014-01/28/content_5112340.htm)