**凤城市**位于[中国](../Page/中国.md "wikilink")[辽宁省东部](../Page/辽宁省.md "wikilink")，是[丹东市代管的一个](../Page/丹东市.md "wikilink")[县级市](../Page/县级市.md "wikilink")，中国第一个由少数民族[自治县撤县建市](../Page/自治县.md "wikilink")，并保留民族自治地方的待遇和各项优惠民族政策的城市。\[1\]

## 历史

[明朝后期将定辽右卫从辽阳迁于凤凰堡](../Page/明朝.md "wikilink")。[清朝](../Page/清朝.md "wikilink")[光绪二年](../Page/光绪.md "wikilink")（1876年）设立凤凰直隶厅，辖岫岩州、安东县、宽甸县。[民国二年](../Page/民国.md "wikilink")（1913年）改凤凰县；因与湖南省凤凰县同名，1914年改为凤城县。1985年撤销凤城县，设立凤城满族自治县；1994年撤销凤城满族自治县，设立凤城市。
[Dacheng_Pavillion,_Fenghuacheng.jpg](https://zh.wikipedia.org/wiki/File:Dacheng_Pavillion,_Fenghuacheng.jpg "fig:Dacheng_Pavillion,_Fenghuacheng.jpg")

## 行政区划

下辖3个[街道办事处](../Page/街道办事处.md "wikilink")、17个[镇](../Page/镇_\(中华人民共和国\).md "wikilink")、1个[民族乡](../Page/民族乡.md "wikilink")\[2\]：

  - 街道：凤凰城街道、凤山街道、草河街道。
  - 镇：青城子镇、通远堡镇、爱阳镇、赛马镇、鸡冠山镇、边门镇、红旗镇、弟兄山镇、大堡镇、东汤镇、刘家河镇、[宝山镇](../Page/宝山镇_\(凤城市\).md "wikilink")、蓝旗镇、白旗镇、四门子镇、石城镇、沙里寨镇、大兴镇。
  - 民族乡：大堡[蒙古族乡](../Page/蒙古族.md "wikilink")。

## 交通

凤城交通便捷，有[沈丹客运专线](../Page/沈丹客运专线.md "wikilink")、[沈丹铁路](../Page/沈丹铁路.md "wikilink")、[凤上铁路](../Page/凤上铁路.md "wikilink")、[304国道](../Page/304国道.md "wikilink")、[丹阜高速公路贯穿](../Page/丹阜高速公路.md "wikilink")。

## 旅游

凤凰山为国家风景名胜区，有“泰山之雄伟，华山之险峻，庐山之幽静，黄山之奇观，峨眉之秀美”。除外，凤城市境内还有玉龙湖、浦石河原始森林、大梨树花果山、赛马溶洞群、爱河鸽子洞、东汤温泉和古遗址、古城堡等旅游景点。

## 参考文献

[凤城市](../Category/凤城市.md "wikilink")
[市](../Category/丹东区县市.md "wikilink")
[丹东](../Category/辽宁县级市.md "wikilink")

1.
2.   2009年丹东市行政区划统计表