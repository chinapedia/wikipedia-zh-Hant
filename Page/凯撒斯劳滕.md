**凯撒斯劳滕**\[1\]（德语：Kaiserslautern），又译作**凯泽斯劳滕**，是位于[德国西南](../Page/德国.md "wikilink")[莱茵兰-普法尔茨州中部的一座城市](../Page/莱茵兰-普法尔茨州.md "wikilink")，本市目前是[信息技术和](../Page/信息技术.md "wikilink")[通讯技术的中心](../Page/通訊技術.md "wikilink")，同时也是工业城市和大学城，人口約10萬人，另有為數5萬名的[北約軍事人員居住在本市及其周邊的](../Page/北約.md "wikilink")[凱撒斯勞滕縣內](../Page/凯撒斯劳滕县.md "wikilink")，每年約替地方經濟注入10億美元的增額，當中的是[美國海外最大的](../Page/美國.md "wikilink")[美國人聚落](../Page/美國人.md "wikilink")\[2\]。

凱撒斯勞滕的历史最早可以追溯到公元前800年[凯尔特人在此处的定居](../Page/凯尔特人.md "wikilink")。凯撒斯劳滕曾被一条名为[劳特](../Page/劳特河.md "wikilink")（Lauter）的河流所环绕。1155到1190年间统治[神圣罗马帝国的](../Page/神圣罗马帝国.md "wikilink")[腓特烈一世为这个城市取了现在的名称](../Page/腓特烈一世.md "wikilink")。其中文意思是“皇帝的劳特河”。[腓特烈一世的](../Page/腓特烈一世.md "wikilink")[城堡遗迹在市政厅的门前仍然可以看到](../Page/城堡.md "wikilink")。

[德国足球丙级联赛](../Page/德国足球丙级联赛.md "wikilink")[凯撒斯劳滕足球俱乐部位于该市](../Page/凯撒斯劳滕足球俱乐部.md "wikilink")，同时凯撒斯劳滕也是[2006年世界杯足球赛的举办城市之一](../Page/2006年世界杯足球赛.md "wikilink")。凯撒斯劳滕距离[巴黎](../Page/巴黎.md "wikilink")459公里，[卢森堡](../Page/卢森堡.md "wikilink")159公里。位于国家间的交互区。

## 友好城市

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p><a href="../Page/美国.md" title="wikilink">美国</a><a href="../Page/达文波特_(艾奥瓦州).md" title="wikilink">达文波特</a></p></li>
<li><p><a href="../Page/法国.md" title="wikilink">法国</a><a href="../Page/杜济.md" title="wikilink">杜济</a></p></li>
<li><p><a href="../Page/法国.md" title="wikilink">法国</a><a href="../Page/圣康坦_(埃纳省).md" title="wikilink">圣康坦</a></p></li>
<li><p><a href="../Page/英国.md" title="wikilink">英国</a><a href="../Page/纽汉.md" title="wikilink">纽汉</a></p></li>
<li><p><a href="../Page/日本.md" title="wikilink">日本</a><a href="../Page/東京都.md" title="wikilink">東京都</a><a href="../Page/文京区.md" title="wikilink">文京区</a></p></li>
<li><p><a href="../Page/德国.md" title="wikilink">德国</a><a href="../Page/勃兰登堡.md" title="wikilink">勃兰登堡</a></p></li>
<li><p><a href="../Page/保加利亚.md" title="wikilink">保加利亚</a><a href="../Page/普列文.md" title="wikilink">普列文</a></p></li>
</ul></td>
<td><p> </p></td>
<td><ul>
<li><p><a href="../Page/美国.md" title="wikilink">美国</a><a href="../Page/哥伦比亚_(南卡罗来纳州).md" title="wikilink">哥伦比亚</a></p></li>
<li><p><a href="../Page/丹麦.md" title="wikilink">丹麦</a><a href="../Page/锡尔克堡.md" title="wikilink">锡尔克堡</a></p></li>
<li><p><a href="../Page/葡萄牙.md" title="wikilink">葡萄牙</a><a href="../Page/吉马朗伊斯.md" title="wikilink">吉马朗伊斯</a></p></li>
<li><p><a href="../Page/波斯尼亚和黑塞哥维那.md" title="wikilink">波斯尼亚和黑塞哥维那</a><a href="../Page/巴尼亚卢卡.md" title="wikilink">巴尼亚卢卡</a></p></li>
<li><p><a href="../Page/马其顿.md" title="wikilink">马其顿</a><a href="../Page/比托拉.md" title="wikilink">比托拉</a></p></li>
<li><p><a href="../Page/西班牙.md" title="wikilink">西班牙</a><a href="../Page/伊瓜拉达.md" title="wikilink">伊瓜拉达</a></p></li>
<li><p><a href="../Page/英国.md" title="wikilink">英国</a><a href="../Page/罗瑟勒姆.md" title="wikilink">罗瑟勒姆</a></p></li>
</ul></td>
</tr>
</tbody>
</table>

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [凯撒斯劳滕官方网站](http://www.kaiserslautern.de/)
  - [凯撒斯劳滕工业大学网站](http://www.tu-kaiserslautern.de/)
  - [凱澤斯勞騰華人社區網站](http://www.china-kl.de/)

[K](../Category/莱茵兰-普法尔茨州市镇.md "wikilink")

1.  [Britannica Online Traditional Chinese
    Edition](http://wordpedia.eb.com/tbol/article?i=039295&db=big5&q=kaiserslautern)
2.