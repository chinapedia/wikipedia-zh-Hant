**凯文·洛奇**（，\[1\]），[美国當代](../Page/美国.md "wikilink")[建筑师](../Page/建筑师.md "wikilink")，1982年第四届[普利兹克奖得主](../Page/普利兹克奖.md "wikilink")。

凯文生于爱尔兰[都柏林](../Page/都柏林.md "wikilink")，1948年移民美国。1964年成为美国公民。

他去美国时，起意环球旅行10年，每年为不同的建筑师工作。[伊利诺理工学院的研究生课程成为他的第一站](../Page/伊利诺理工学院.md "wikilink")，当时，[密斯正执教于斯](../Page/密斯.md "wikilink")。

1951年，他加入位于密歇根州Bloomfield
Hills的[沙里宁事务所](../Page/沙里宁.md "wikilink")。他未来的合伙人[詹·丁克路](../Page/詹·丁克路.md "wikilink")（John
Dinkeloo）亦于同年加入。自1954年至Eero·沙里宁1961年辞世，凯文·洛奇是其最主要的设计成员。

## 参考资料

## 外部链接

  - [Profile.](http://archiseek.com/2009/kevin-roche-born-1922)
    archiseek.com
  - [New Haven Coliseum
    infosite.](https://web.archive.org/web/20070930081958/http://www.yurgeles.net/Stein_Website/HTML/index.html)
    yurgeles.net
  - [Kevin Roche
    profile.](https://web.archive.org/web/20070925192107/http://www.pritzkerprize.com/roche.htm)
    PritzkerPrize.com
  - [Yale School of Architecture Honors Kevin Roche with Exhibition,
    Symposium.](http://news.yale.edu/2011/01/20/yale-school-architecture-honors-kevin-roche-exhibition-symposium)

[Category:美国建筑师](../Category/美国建筑师.md "wikilink")
[Category:普利兹克奖得主](../Category/普利兹克奖得主.md "wikilink")

1.