**凯莱蒂·阿格奈什**（，，生于[匈牙利](../Page/匈牙利.md "wikilink")[布达佩斯](../Page/布达佩斯.md "wikilink")），[匈牙利女子](../Page/匈牙利.md "wikilink")[体操运动员](../Page/体操.md "wikilink")，共获得过10枚[奥运会奖牌](../Page/奥运会.md "wikilink")（5金3银2铜）。

凯莱蒂4岁开始练习体操，第一次获得奥运奖牌时已31岁，获最后一块时更是已年高35岁了。

1956年參加[墨爾本奧運時](../Page/1956年夏季奥林匹克运动会.md "wikilink")，正值[蘇聯入侵匈牙利](../Page/匈牙利十月事件.md "wikilink")，她與多位匈牙利運動員決定留在澳洲申請[政治庇護](../Page/政治庇護.md "wikilink")。她於1957年移居[以色列](../Page/以色列.md "wikilink")。

2002年，凯莱蒂入选[国际体操名人堂](../Page/国际体操名人堂.md "wikilink")。

## 参考资料

1.

## 外部链接

  - [国际体操联合会相关资料](https://web.archive.org/web/20070310222021/http://www.fig-gymnastics.com/events/athletes/bio.jsp?ID=4889)

[Category:匈牙利体操运动员](../Category/匈牙利体操运动员.md "wikilink")
[Category:匈牙利奧林匹克運動會金牌得主](../Category/匈牙利奧林匹克運動會金牌得主.md "wikilink")
[Category:匈牙利奧林匹克運動會銀牌得主](../Category/匈牙利奧林匹克運動會銀牌得主.md "wikilink")
[Category:匈牙利奧林匹克運動會銅牌得主](../Category/匈牙利奧林匹克運動會銅牌得主.md "wikilink")
[Category:1952年夏季奧林匹克運動會獎牌得主](../Category/1952年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:1956年夏季奧林匹克運動會獎牌得主](../Category/1956年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:奧林匹克運動會體操獎牌得主](../Category/奧林匹克運動會體操獎牌得主.md "wikilink")
[Category:世界体操锦标赛奖牌得主](../Category/世界体操锦标赛奖牌得主.md "wikilink")