**凱倫·馮·白列森-菲尼克男爵夫人**（**Baronesse Karen von Blixen-Finecke**,
），筆名**伊莎·丹尼森**（**Isak
Dinesen**），[丹麥著名的现代](../Page/丹麥.md "wikilink")[作家](../Page/作家.md "wikilink")，寫作上使用丹麥語、法語、英語，但主要以英語寫作再譯為[母语](../Page/丹麦语.md "wikilink")。成名作《七个奇幻的故事》。

白列森出生于北西兰岛rungstedlund的一个人丁兴旺的富裕家庭，她有四个兄弟姐妹，两个姐姐和两个弟弟。从小由于家境富足，他们一直接受的是私立教育，家里聘请了家庭教师，也有仆人和园丁。她十岁的时候，家里发生了重大变故，父亲自杀了。但在母亲和外祖母的精心照顾下，凯伦还是度过了这一困难时期，童年总体说来还算是幸福的。1912年她和表兄馮·白列森-菲尼克男爵订婚，并于1914年结婚并移居到东非肯尼亚。1925年受经济危机影响两人离婚。
白列森曾居住在東非的[肯尼亞](../Page/肯尼亞.md "wikilink")，其生平故事被拍攝成電影《[-{zh-tw:遠離非洲;
zh-hk:非洲之旅;
zh-cn:走出非洲;}-](../Page/走出非洲.md "wikilink")》，獲1985年[奧斯卡金像獎最佳影片](../Page/奧斯卡金像獎.md "wikilink")。其作品《[芭比的盛宴](../Page/芭比的盛宴.md "wikilink")》（Babette's
Feast）也拍攝成同名電影，獲得1987年奧斯卡金像獎最佳外國影片。

丹尼森多次被提名[諾貝爾文學獎](../Page/諾貝爾文學獎.md "wikilink")，其中於1959年更一度被宣布獲獎，但諾貝爾獎委員會卻把獎頒給意大利作家[薩瓦多爾·誇西莫多](../Page/薩瓦多爾·誇西莫多.md "wikilink")，原因竟是「太多北歐地區人士獲獎」。最終丹尼森於1962年逝世，終年77歲，永遠無緣諾貝爾獎。1954年文學獎得主[歐內斯特·海明威於授獎時曾說](../Page/歐內斯特·海明威.md "wikilink")：「如果把此獎頒給美麗的丹尼森女士，我會更高興。」

## 外部链接

  -
  -
  -
[Category:丹麦作家](../Category/丹麦作家.md "wikilink")
[Category:回忆录作家](../Category/回忆录作家.md "wikilink")
[Category:20世纪女性作家](../Category/20世纪女性作家.md "wikilink")
[Category:使用筆名的作家](../Category/使用筆名的作家.md "wikilink")
[Category:歐洲紙幣上的人物](../Category/歐洲紙幣上的人物.md "wikilink")