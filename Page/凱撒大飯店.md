**凱撒大飯店**（）為[台灣的](../Page/台灣.md "wikilink")[五星級](../Page/五星級.md "wikilink")[飯店之一](../Page/飯店.md "wikilink")，也是台灣知名的[旅館系統之一](../Page/旅館.md "wikilink")，由[宏國關係事業經營](../Page/宏國關係事業.md "wikilink")。

目前凱撒大飯店在[台北市](../Page/台北市.md "wikilink")（[台北車站](../Page/台北車站.md "wikilink")）、[新北市](../Page/新北市.md "wikilink")[新板特區](../Page/新板橋車站特定專用區.md "wikilink")、[屏東縣](../Page/屏東縣.md "wikilink")（[墾丁](../Page/墾丁.md "wikilink")）設有據點。另外[臺南市](../Page/臺南市.md "wikilink")[曾文水庫的趣淘漫旅](../Page/曾文水庫.md "wikilink")
Hotel Cham Cham 亦由凱撒團隊經營管理。2017年凱撒與希爾頓全球酒店集團(HILTON
WORLDWIDE)正式簽訂合作加盟意向書，計畫讓希爾頓飯店連鎖品牌重返台灣\[1\]
。預估凱撒飯店連鎖未來旗下會有10間飯店，所屬飯店房間數將達3,600間，躋身為全台最大連鎖飯店\[2\] 。

## 飯店據點

[Caesar_Park_Taipei_20131107.jpg](https://zh.wikipedia.org/wiki/File:Caesar_Park_Taipei_20131107.jpg "fig:Caesar_Park_Taipei_20131107.jpg")

  - 台北凱撒大飯店 (Caesar Park Hotel Taipei)：台北市忠孝西路一段38號
  - 板橋凱撒大飯店 (Ceasar Park Hotel Banqiao): 新北市板橋區縣民大道二段8號
  - 墾丁凱撒大飯店 (Caesar Park Hotel Kenting)：屏東縣恆春鎮墾丁路6號
  - 台北趣淘漫旅 (Hotel Cham Cham Taipei)：新北市板橋區中山路一段139號
  - 台南趣淘漫旅 (Hotel Cham Cham Tainan)：台南市楠西區密枝里密枝102之5號（原山芙蓉渡假大酒店\[3\]）　
  - 阿樹國際旅店 (ArTree Hotel)：台北市松山區八德路三段76號
  - 台北凱達大飯店 (Caesar Metro Hotel Taipei)：台北市萬華區艋舺大道167號
  - 內湖凱旋酒店 (Just Palace Hotel)：台北市內湖區江南街55號

## 樓層簡介

  - 台北凱撒大飯店

<!-- end list -->

  - 3F 王朝餐廳
  - 2F Checkers自助餐
  - B1-1F 凱撒美食精品館

## 興建中飯店

  - 南港台肥案: 飯店名稱未定 \[預計2020年開幕\]

## 參考資料

## 外部連結

官網:

  - [台北凱撒大飯店](http://taipei.caesarpark.com.tw/)
  - [墾丁凱撒大飯店](http://kenting.caesarpark.com.tw/)
  - [板橋凱撒大飯店](http://banqiao.caesarpark.com.tw/)
  - [趣淘漫旅 Hotel Cham Cham](http://www.chamcham.com.tw/)

Facebook粉絲專頁:

  -
  -
  -
{{-}}

[Category:台北市旅館](../Category/台北市旅館.md "wikilink")
[Category:台南市旅館](../Category/台南市旅館.md "wikilink")
[Category:屏東縣旅館](../Category/屏東縣旅館.md "wikilink")
[屏東](../Category/交通部觀光局評鑑五星級旅館.md "wikilink")
[Category:總部位於臺北市中正區的工商業機構](../Category/總部位於臺北市中正區的工商業機構.md "wikilink")
[Category:宏國集團](../Category/宏國集團.md "wikilink")

1.
2.
3.