**凱撒琳·丹尼芙**（，）[法國女影星](../Page/法國.md "wikilink")，從1960年代活躍至今。

出生於法國[巴黎](../Page/巴黎.md "wikilink")，13歲從影。有法國第一美人之稱。成名作電影是「[秋水伊人](../Page/秋水伊人.md "wikilink")」，代表作是闡釋家庭主婦不滿平常生活「[青樓怨婦](../Page/青樓怨婦.md "wikilink")」（[布紐爾執導](../Page/布紐爾.md "wikilink")）與1981年「[最後地下鐵](../Page/最後地下鐵.md "wikilink")」（Le
Dernier Metro），2006年擔任[威尼斯影展評審團主席](../Page/威尼斯影展.md "wikilink")。

前夫为英國攝影師David
Bailey，與她維持7年婚姻關係後離異，竟形容與凱結婚像是「開慣[福特平常車種想改駕駛](../Page/福特.md "wikilink")[義大利超級](../Page/義大利.md "wikilink")[跑車](../Page/跑車.md "wikilink")[瑪莎拉蒂結果可想而知](../Page/瑪莎拉蒂.md "wikilink")\[1\]。」讓人聞之莞爾。

## 電影作品

<table>
<thead>
<tr class="header">
<th><p>年分</p></th>
<th><p>電影</p></th>
<th><p>導演</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1964</p></td>
<td><p>《》<br />
Les Parapluies de Cherbourg</p></td>
<td><p><a href="../Page/雅克·德米.md" title="wikilink">賈克·德米</a></p></td>
<td><p><a href="../Page/坎城影展.md" title="wikilink">第17屆坎城影展金棕櫚獲獎影片</a></p></td>
</tr>
<tr class="even">
<td><p>《》<br />
L'homme qui vendit la Tour Eiffel</p></td>
<td><p><a href="../Page/克勞德·夏布洛.md" title="wikilink">克勞德·夏布洛</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1965</p></td>
<td><p>《》<br />
Repulsion</p></td>
<td><p><a href="../Page/羅曼·波蘭斯基.md" title="wikilink">羅曼·波蘭斯基</a></p></td>
<td><p><br />
提名-<a href="../Page/紐約影評人協會獎最佳女主角.md" title="wikilink">紐約影評人協會獎最佳女主角</a>(第3名)</p></td>
</tr>
<tr class="even">
<td><p>《》<br />
Le Chant du monde</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1966</p></td>
<td><p>《》<br />
Les Créatures</p></td>
<td><p><a href="../Page/阿涅斯·瓦尔达.md" title="wikilink">安妮·華達</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1967</p></td>
<td><p>《》<br />
Les Demoiselles de Rochefort</p></td>
<td><p><a href="../Page/雅克·德米.md" title="wikilink">賈克·德米</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》<br />
Belle de jour</p></td>
<td><p><a href="../Page/路易斯·布紐爾.md" title="wikilink">路易斯·布紐爾</a></p></td>
<td><p>{<a href="../Page/威尼斯影展.md" title="wikilink">第28屆威尼斯影展正式競賽片</a>}}<br />
<a href="../Page/威尼斯影展.md" title="wikilink">第28屆威尼斯影展最佳女主角</a><br />
提名-<a href="../Page/英國電影學院獎最佳女主角.md" title="wikilink">英國電影學院獎最佳女主角</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1969</p></td>
<td><p>《》<br />
La sirène du Mississipi</p></td>
<td><p><a href="../Page/法蘭索瓦·杜魯福.md" title="wikilink">法蘭索瓦·楚浮</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1970</p></td>
<td><p>《<a href="../Page/特莉絲坦娜.md" title="wikilink">特莉絲坦娜</a>》<br />
Tristana</p></td>
<td><p><a href="../Page/路易斯·布紐爾.md" title="wikilink">路易斯·布紐爾</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1972</p></td>
<td><p>《》<br />
Un flic</p></td>
<td><p><a href="../Page/讓-皮埃爾·梅爾維爾.md" title="wikilink">尚-皮耶·梅爾維爾</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1975</p></td>
<td><p>《》<br />
Hustle</p></td>
<td><p><a href="../Page/罗伯特·奥尔德里奇.md" title="wikilink">羅伯·阿德力區</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1979</p></td>
<td><p>《》<br />
À nous deux</p></td>
<td><p><a href="../Page/克勞德·雷路許.md" title="wikilink">克勞德·雷路許</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1980</p></td>
<td><p>《》<br />
Le Dernier Métro</p></td>
<td><p><a href="../Page/法蘭索瓦·杜魯福.md" title="wikilink">法蘭索瓦·楚浮</a></p></td>
<td><p><a href="../Page/凱薩獎最佳女主角獎.md" title="wikilink">凱薩獎最佳女主角獎</a><br />
<a href="../Page/義大利電影金像獎.md" title="wikilink">大衛獎最佳外國女演員獎</a></p></td>
</tr>
<tr class="even">
<td><p>1981</p></td>
<td><p>《》<br />
Hôtel des Amériques</p></td>
<td><p><a href="../Page/安德烈·泰希內.md" title="wikilink">安德烈·泰希內</a></p></td>
<td><p>提名-<a href="../Page/凱薩獎最佳女主角獎.md" title="wikilink">凱薩獎最佳女主角獎</a></p></td>
</tr>
<tr class="odd">
<td><p>1986</p></td>
<td><p>《》<br />
Le Lieu du crime</p></td>
<td><p><a href="../Page/安德烈·泰希內.md" title="wikilink">安德烈·泰希內</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1988</p></td>
<td><p>《》<br />
Drôle d'endroit pour une rencontre</p></td>
<td></td>
<td><p>提名-<a href="../Page/凱薩獎最佳女主角獎.md" title="wikilink">凱薩獎最佳女主角獎</a></p></td>
</tr>
<tr class="odd">
<td><p>1992</p></td>
<td><p>《<a href="../Page/印度支那_(電影).md" title="wikilink">印度支那</a>》<br />
Indochine</p></td>
<td></td>
<td><p><a href="../Page/凱薩獎最佳女主角獎.md" title="wikilink">凱薩獎最佳女主角獎</a><br />
提名-<a href="../Page/奧斯卡最佳女主角獎.md" title="wikilink">奧斯卡最佳女主角獎</a></p></td>
</tr>
<tr class="even">
<td><p>1993</p></td>
<td><p>《》<br />
Ma saison préférée</p></td>
<td><p><a href="../Page/安德烈·泰希內.md" title="wikilink">安德烈·泰希內</a></p></td>
<td><p><br />
提名-<a href="../Page/凱薩獎最佳女主角獎.md" title="wikilink">凱薩獎最佳女主角獎</a></p></td>
</tr>
<tr class="odd">
<td><p>1995</p></td>
<td><p>《》<br />
Les cent et une nuits de Simon Cinéma</p></td>
<td><p><a href="../Page/阿涅斯·瓦尔达.md" title="wikilink">安妮·華達</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/愛慾修道院.md" title="wikilink">愛慾修道院</a>》<br />
L'amore molesto</p></td>
<td><p><a href="../Page/曼諾·迪·奧利維拉.md" title="wikilink">曼諾·迪·奧利維拉</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1996</p></td>
<td><p>《》<br />
Les Voleurs</p></td>
<td><p><a href="../Page/安德烈·泰希內.md" title="wikilink">安德烈·泰希內</a></p></td>
<td><p><br />
提名-<a href="../Page/凱薩獎最佳女主角獎.md" title="wikilink">凱薩獎最佳女主角獎</a></p></td>
</tr>
<tr class="even">
<td><p>1997</p></td>
<td><p>《》<br />
Généalogies d'un crime</p></td>
<td><p><a href="../Page/劳尔·鲁伊斯.md" title="wikilink">拉烏·盧伊茲</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1998</p></td>
<td><p>《》<br />
Place Vendôme</p></td>
<td><p><a href="../Page/妮可·賈西亞.md" title="wikilink">妮可·賈西亞</a></p></td>
<td><p><a href="../Page/威尼斯影展.md" title="wikilink">第55屆威尼斯影展正式競賽片</a><br />
<a href="../Page/威尼斯影展最佳女演員獎.md" title="wikilink">威尼斯影展最佳女演員獎</a><br />
提名-<a href="../Page/凱薩獎最佳女主角獎.md" title="wikilink">凱薩獎最佳女主角獎</a></p></td>
</tr>
<tr class="even">
<td><p>1999</p></td>
<td><p>《》<br />
Pola X</p></td>
<td><p><a href="../Page/李歐·卡霍.md" title="wikilink">李歐·卡霍</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/追憶似水年華_(電影).md" title="wikilink">追憶似水年華</a>》<br />
Le Temps retrouvé</p></td>
<td><p><a href="../Page/劳尔·鲁伊斯.md" title="wikilink">拉烏·盧伊茲</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》<br />
Le vent de la nuit</p></td>
<td><p><a href="../Page/菲利普·卡黑.md" title="wikilink">菲利普·卡黑</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td><p>《<a href="../Page/黑暗中的舞者.md" title="wikilink">在黑暗中漫舞</a>》<br />
Dancer in the Dark</p></td>
<td><p><a href="../Page/拉斯·馮·提爾.md" title="wikilink">拉斯·馮·提爾</a></p></td>
<td><p><br />
提名-<a href="../Page/衛星獎最佳女配角.md" title="wikilink">衛星獎戲劇類最佳女配角獎</a></p></td>
</tr>
<tr class="even">
<td><p>2001</p></td>
<td><p>《》<br />
Vou para Casa</p></td>
<td><p><a href="../Page/曼諾·迪·奧利維拉.md" title="wikilink">曼諾·迪·奧利維拉</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002</p></td>
<td><p>《<a href="../Page/八美圖.md" title="wikilink">八美圖</a>》<br />
8 femmes</p></td>
<td><p><a href="../Page/法蘭索瓦·歐容.md" title="wikilink">法蘭索瓦·歐容</a></p></td>
<td><p><br />
<a href="../Page/柏林影展.md" title="wikilink">柏林影展</a><a href="../Page/銀熊獎.md" title="wikilink">傑出藝術成就銀熊獎</a><br />
<a href="../Page/歐洲電影獎.md" title="wikilink">歐洲電影獎最佳女演員</a></p></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p>《》<br />
Rois et reine</p></td>
<td><p><a href="../Page/阿諾·戴普勒尚.md" title="wikilink">阿諾·戴普勒尚</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》<br />
Les Temps qui changent</p></td>
<td><p><a href="../Page/安德烈·泰希內.md" title="wikilink">安德烈·泰希內</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p>《》<br />
Un conte de Noël</p></td>
<td><p><a href="../Page/阿諾·戴普勒尚.md" title="wikilink">阿諾·戴普勒尚</a></p></td>
<td><p><br />
提名-<a href="../Page/衛星獎最佳女主角.md" title="wikilink">衛星獎最佳女主角</a></p></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p>《》<br />
La fille du RER</p></td>
<td><p><a href="../Page/安德烈·泰希內.md" title="wikilink">安德烈·泰希內</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td><p>《》<br />
Potiche</p></td>
<td><p><a href="../Page/法蘭索瓦·歐容.md" title="wikilink">法蘭索瓦·歐容</a></p></td>
<td><p><br />
提名-<a href="../Page/凱薩獎最佳女主角獎.md" title="wikilink">凱薩獎最佳女主角獎</a></p></td>
</tr>
<tr class="odd">
<td><p>2011</p></td>
<td><p>《》<br />
Les Bien-aimés</p></td>
<td><p><a href="../Page/克里斯多福·歐諾黑.md" title="wikilink">克里斯多福·歐諾黑</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p>《》<br />
Linhas de Wellington</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p>《<a href="../Page/她的搖擺上路.md" title="wikilink">她的搖擺上路</a>》<br />
Elle s'en va</p></td>
<td><p><a href="../Page/艾曼紐·貝考.md" title="wikilink">艾曼紐·貝考</a></p></td>
<td><p><br />
提名-<a href="../Page/凱薩獎最佳女主角獎.md" title="wikilink">凱薩獎最佳女主角獎</a><br />
提名-<a href="../Page/盧米埃獎.md" title="wikilink">盧米埃獎最佳女演員獎</a></p></td>
</tr>
<tr class="even">
<td><p>2014</p></td>
<td><p>《》<br />
L'Homme qu'on aimait trop</p></td>
<td><p><a href="../Page/安德烈·泰希內.md" title="wikilink">安德烈·泰希內</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》<br />
Trois coeurs</p></td>
<td><p><a href="../Page/班諾·賈克.md" title="wikilink">班諾·賈克</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015</p></td>
<td><p>《》<br />
La Tête haute</p></td>
<td><p><a href="../Page/艾曼紐·貝考.md" title="wikilink">艾曼紐·貝考</a></p></td>
<td><p>提名-<a href="../Page/凱薩獎最佳女主角獎.md" title="wikilink">凱薩獎最佳女主角獎</a></p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/死期大公開.md" title="wikilink">死期大公開</a>》<br />
Le Tout Nouveau Testament</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

[Catherine_deneuve2.jpg](https://zh.wikipedia.org/wiki/File:Catherine_deneuve2.jpg "fig:Catherine_deneuve2.jpg")

### 廣告

2007年為法國品牌[路易威登拍攝一輯廣告](../Page/路易威登.md "wikilink")。

## 參註

## 外部連結

  - [Film.guardian.uk
    interview](http://film.guardian.co.uk/interview/interviewpages/0,,1577158,00.html)

  - <http://www.myspace.com/catherine_deneuve> The Myspace Catherine
    Deneuve Fan Site.

  -
  - [Music Video "Catherine Deneuve" By Kelly & The
    KellyGirls](http://www.youtube.com/watch?v=QTPD7RZFaw8)

[Category:凱撒電影獎最佳女主角獲得者](../Category/凱撒電影獎最佳女主角獲得者.md "wikilink")
[Category:凱撒電影獎獲得者](../Category/凱撒電影獎獲得者.md "wikilink")
[Category:法国女演员](../Category/法国女演员.md "wikilink")
[Category:法國電影女演員](../Category/法國電影女演員.md "wikilink")
[Category:法國電視演員](../Category/法國電視演員.md "wikilink")
[Category:法國天主教徒](../Category/法國天主教徒.md "wikilink")
[Category:坎城影展獲獎者](../Category/坎城影展獲獎者.md "wikilink")
[Category:威尼斯影展獲獎者](../Category/威尼斯影展獲獎者.md "wikilink")
[Category:威尼斯电影节评审团主席](../Category/威尼斯电影节评审团主席.md "wikilink")
[Category:巴黎人](../Category/巴黎人.md "wikilink")
[Category:20世纪演员](../Category/20世纪演员.md "wikilink")
[Category:高松宫殿下纪念世界文化奖获得者](../Category/高松宫殿下纪念世界文化奖获得者.md "wikilink")

1.