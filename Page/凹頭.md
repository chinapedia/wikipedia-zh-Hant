[Au_Tau_201707.jpg](https://zh.wikipedia.org/wiki/File:Au_Tau_201707.jpg "fig:Au_Tau_201707.jpg")
**凹頭**（）（亦寫作**坳頭**，「凹」依古時的讀音，粵音「坳」，國音āu）是位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[元朗區](../Page/元朗區.md "wikilink")[元朗市中心東面的一個](../Page/元朗市中心.md "wikilink")[山坳](../Page/山坳.md "wikilink")\[1\]，古時是[元朗和](../Page/元朗.md "wikilink")[錦田的分界線](../Page/錦田.md "wikilink")，現時也作爲附近一帶地方的地名。凹頭由[青山公路分開南北](../Page/青山公路.md "wikilink")，[博愛醫院位於北面](../Page/博愛醫院_\(香港\).md "wikilink")，[朗善邨位於南面](../Page/朗善邨.md "wikilink")；其東面為[錦田及](../Page/錦田.md "wikilink")[錦田河](../Page/錦田河.md "wikilink")，其西面則是[十八鄉](../Page/十八鄉.md "wikilink")。

## 接駁交通

凹頭、牛潭尾、新田三站均會在[北環綫興建時預留](../Page/北環綫.md "wikilink")，但不會與[北環綫同時啟用](../Page/北環綫.md "wikilink")。

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 註腳

## 參見

  - [博愛醫院](../Page/博愛醫院_\(香港\).md "wikilink")

{{-}}

[凹頭](../Category/凹頭.md "wikilink")
[Category:香港山坳](../Category/香港山坳.md "wikilink")

1.