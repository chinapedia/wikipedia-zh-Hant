**出島停留場**（）是一位於[日本](../Page/日本.md "wikilink")[長崎縣](../Page/長崎縣.md "wikilink")[長崎市出島町](../Page/長崎市.md "wikilink")，由[長崎電氣軌道所經營的](../Page/長崎電氣軌道.md "wikilink")[路面電車停車站](../Page/路面電車.md "wikilink")。出島車站是長崎電氣軌道所屬的兩條路線系統、[1系統與](../Page/長崎電氣軌道1系統.md "wikilink")[2系統的共用車站之一](../Page/長崎電氣軌道2系統.md "wikilink")，也是最靠近[出島](../Page/出島.md "wikilink")、[長崎縣美術館與](../Page/長崎縣美術館.md "wikilink")[長崎水邊之森公園](../Page/長崎水邊之森公園.md "wikilink")（）等景點的一站。

## 通過路線

  - [長崎電氣軌道](../Page/長崎電氣軌道.md "wikilink")
      - [1號系統](../Page/長崎電氣軌道1號系統.md "wikilink")
      - [2號系統](../Page/長崎電氣軌道2號系統.md "wikilink")

## 參考文獻

  - [電車路線圖（長崎電氣軌道）](https://web.archive.org/web/20091216082604/http://www.naga-den.com/kikaku/zikoku/jikoku_kikaku.html)

[Jima](../Category/日本鐵路車站_De.md "wikilink")
[Category:長崎縣鐵路車站](../Category/長崎縣鐵路車站.md "wikilink")
[Category:長崎電氣軌道本線車站](../Category/長崎電氣軌道本線車站.md "wikilink")
[Category:1915年启用的铁路车站](../Category/1915年启用的铁路车站.md "wikilink")
[Category:長崎市建築物](../Category/長崎市建築物.md "wikilink")
[Category:長崎電氣軌道2號系統車站](../Category/長崎電氣軌道2號系統車站.md "wikilink")