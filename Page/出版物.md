**出版物**（**出版**、**出版品**）指以传播[文化和](../Page/文化.md "wikilink")[知识为目的的各种](../Page/知识.md "wikilink")[产品包括](../Page/产品.md "wikilink")[印刷品](../Page/印刷品.md "wikilink")、电子产品的总称，属于传播文化知识的[媒体](../Page/传播媒体.md "wikilink")。分为[书籍](../Page/图书.md "wikilink")、[期刊](../Page/期刊.md "wikilink")、[报纸和电子传播产品](../Page/报纸.md "wikilink")（[电子出版物或稱](../Page/电子出版.md "wikilink")[電子書](../Page/電子書.md "wikilink")）等种类。

  - **书籍**，传统意义上指传播各种知识为主的出版物，通常为[纸张合订的](../Page/纸张.md "wikilink")[印刷品](../Page/印刷品.md "wikilink")；现代意义包括[电子书籍](../Page/电子书籍.md "wikilink")。
  - **刊物**，常为书籍以外的出版物，分为定期和不定期发行刊物，有的刊物属于书籍的范畴如[杂志](../Page/杂志.md "wikilink")。包括[报纸](../Page/报纸.md "wikilink")、[杂志](../Page/杂志.md "wikilink")、[专刊](../Page/专业刊物.md "wikilink")、电子刊物（含各种电子专辑如[音乐专辑](../Page/音乐专辑.md "wikilink")）等。根据其[发行对象](../Page/出版发行.md "wikilink")（受众）来划分，分为[内部刊物和公开发行两种](../Page/内部刊物.md "wikilink")。
  - **期刊**，属于定期发行的刊物。
  - **书刊**，为图书和刊物的合称。
  - **电子出版物**，包括[磁带](../Page/磁带.md "wikilink")、[唱片](../Page/唱片.md "wikilink")、[VCD](../Page/VCD.md "wikilink")、[DVD](../Page/DVD.md "wikilink")、[光碟](../Page/光碟.md "wikilink")、[电子书籍](../Page/电子书籍.md "wikilink")、[电子辞典等](../Page/电子辞典.md "wikilink")，广义上还包括[影视作品的电子拷贝](../Page/影视作品.md "wikilink")。
  - **未刊本**，作者在世时未出版的书籍、簽名本、畫集、圖片集等作品。\[1\]\[2\]但因作者的影響力，作者过世后民眾有意購買或收藏。亦有一些密書或[禁書被列為未刊本](../Page/禁書.md "wikilink")。\[3\]\[4\]

## 法律定义

1906年，[清朝发布](../Page/清朝.md "wikilink")《大清印刷物件专律》，其中使用了印刷物件和记载物件的概念，但并未出现出版物的概念\[5\]。

1914年，[中华民国北洋政府公布](../Page/中华民国北洋政府.md "wikilink")《[出版法](../Page/出版法.md "wikilink")》，使用了出版物的概念，但是未直接对出版物作解释，只提供了[出版的概念](../Page/出版.md "wikilink")\[6\]。

1930年，[中华民国政府公布的](../Page/中华民国政府.md "wikilink")《出版法》，使用了出版品的类似概念：“本法称出版品者，谓用机械或化学方法所引述，而供出售或散步之文书图画。”\[7\]

1997年，[中华人民共和国政府颁布](../Page/中华人民共和国政府.md "wikilink")《[出版管理条例](../Page/:s:出版管理条例_\(1997年\).md "wikilink")》，将出版物定义为包括报纸、期刊、图书、音像制品、电子出版物等。

## 特点

  - 以出版为主的[生产或](../Page/生产.md "wikilink")[产业领域](../Page/产业.md "wikilink")，称为**出版业**。
  - 以[商业利益为目的](../Page/商业.md "wikilink")，面向社会公众发行的出版物，均受[版权](../Page/版权.md "wikilink")（[著作权](../Page/著作权.md "wikilink")）的制约。
  - 任何社会形态和政治制度的政府或政权，出版物的生产、发行，均受到管制，其行政管理部门多为[版权局](../Page/版权局.md "wikilink")。

## 参考文献

## 参见

  - [出版](../Page/出版.md "wikilink")

[出版品](../Category/出版品.md "wikilink")

1.  [新一批哈佛大学图书馆馆藏未刊中国旧海关史料发布](http://www.chinanews.com/cul/2016/02-27/7775685.shtml)

2.  [惠州首部大型乡邦文献总集《惠州文征》首发](http://www.hznews.com/xw/hzxw/201509/t20150925_1048476.html)

3.

4.  [《丁玲传》作者：她的命运跟革命紧紧相连](http://cul.qq.com/a/20150710/022250.htm)

5.

6.
7.