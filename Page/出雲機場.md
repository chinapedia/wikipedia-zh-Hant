**出雲機場**（，），位於[島根縣](../Page/島根縣.md "wikilink")[出雲市](../Page/出雲市.md "wikilink")，屬[第三種機場](../Page/日本機場.md "wikilink")。設有「出雲結緣機場」（）的愛稱。

## 航空管制

| 種類  | 電波頻率              |
| --- | ----------------- |
| RDO | 122.7MHz,126.2MHz |

  -
    航空管制國土交通省大阪航空局出雲空港出張所擔當

## 航空保安無線設施

|         |     |           |      |
| ------- | --- | --------- | ---- |
| 局名      | 種類  | 電波頻率      | 識別信號 |
| 出雲      | VOR | 113.4MHz  | XZE  |
| 出雲      | DME | 1168.0MHz | XZE  |
| 出雲R/W25 | ILS | 111.7MHz  | IXZ  |

## 航空公司與航點

## 巴士交通

  - [出雲-出雲空港線](../Page/出雲-出雲空港線.md "wikilink")（[出雲一畑交通](../Page/出雲一畑交通.md "wikilink")）
      - [出雲市駅](../Page/出雲市駅.md "wikilink")・体育館前-上成橋-大津小学校前-富-[直江駅入口](../Page/直江駅.md "wikilink")-斐川西中入口-小原-直江町東-斐川役場前-斐川東中入口-出雲空港
  - [大社-出雲空港-玉造線](../Page/大社-出雲空港-玉造線.md "wikilink")（出雲一畑交通）
      - [出雲大社](../Page/出雲大社.md "wikilink")・[出雲大社前駅](../Page/出雲大社前駅.md "wikilink")-出雲空港
      - 出雲大社・出雲大社前駅←出雲空港←[玉造温泉](../Page/玉造温泉.md "wikilink")
  - [松江-出雲空港線](../Page/松江-出雲空港線.md "wikilink")（[松江一畑交通](../Page/松江一畑交通.md "wikilink")）

## 參考來源

  -

<div class="references-small">

<references />

</div>

## 外部連結

  - [官方網站](http://www.izumo-airport.co.jp/)
  - [出雲機場管理事務所](http://www.pref.shimane.lg.jp/izumokuko/)
  - [國土交通省大阪航空局介紹](http://ocab.mlit.go.jp/about/jurisdiction/izumo/)

{{-}}

[Category:中國地方機場](../Category/中國地方機場.md "wikilink")
[Category:島根縣交通](../Category/島根縣交通.md "wikilink")
[Category:出雲市](../Category/出雲市.md "wikilink")