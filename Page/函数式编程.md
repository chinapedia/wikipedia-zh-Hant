**函数式编程**（）或称**函数程序设计**、**泛函编程**，是一种[编程范式](../Page/编程范式.md "wikilink")，它将[电脑运算视为](../Page/电脑运算.md "wikilink")[函数运算](../Page/函数.md "wikilink")，并且避免使用[程式状态以及](../Page/程式状态.md "wikilink")[易变物件](../Page/不可变物件.md "wikilink")。其中，[λ演算](../Page/λ演算.md "wikilink")（lambda
calculus）为该语言最重要的基础。而且，λ演算的函数可以接受函数当作输入（引数）和输出（传出值）。

比起[指令式編程](../Page/指令式編程.md "wikilink")，函數式編程更加強調程序执行的结果而非执行的过程，倡导利用若干简单的执行单元让计算结果不断渐进，逐层推导复杂的运算，而不是设计一个复杂的执行过程。

## 典型的函数式编程语言

### 純函数式编程语言

  - 強靜態類型
      - [Concurrent Clean](../Page/Concurrent_Clean.md "wikilink")
      - [Haskell](../Page/Haskell.md "wikilink")
      - [Miranda](../Page/Miranda_\(编程语言\).md "wikilink")

<!-- end list -->

  - 弱類型
      - [Lazy K](../Page/Lazy_K.md "wikilink")

### 非純函数式编程语言

  - 強靜態類型
      - [F\#](../Page/F♯.md "wikilink")
      - [ML](../Page/ML語言.md "wikilink")
      - [OCaml](../Page/OCaml.md "wikilink")
      - [Scala](../Page/Scala.md "wikilink")

<!-- end list -->

  - 強動態類型
      - [Clojure](../Page/Clojure.md "wikilink")
      - [Erlang](../Page/Erlang.md "wikilink")
      - [Lisp](../Page/Lisp.md "wikilink")
      - [LOGO](../Page/Logo_\(程序语言\).md "wikilink")
      - [Mathematica](../Page/Mathematica.md "wikilink")
      - [R](../Page/R語言.md "wikilink")
      - [Scheme](../Page/Scheme.md "wikilink")

<!-- end list -->

  - 弱類型
      - [Unlambda](../Page/Unlambda.md "wikilink")

### 其他函数式编程语言

  - [APL語言](../Page/APL語言.md "wikilink")
  - [XSLT](../Page/XSL_Transformations.md "wikilink")

## 歷史

函數式編程中最古老的例子莫過於1958年被創造出來的[Lisp了](../Page/Lisp.md "wikilink")。函數式編程更加現代一些的例子包括[Clean](../Page/Concurrent_Clean.md "wikilink")、[Clojure](../Page/Clojure.md "wikilink")、[Erlang](../Page/Erlang.md "wikilink")、[Haskell](../Page/Haskell.md "wikilink")、[Miranda](../Page/Miranda_\(編程語言\).md "wikilink")、[Scheme等](../Page/Scheme.md "wikilink")。

雖然[λ演算並非設計來於計算機上執行](../Page/λ演算.md "wikilink")，但它可以被視作第一個函數式編程語言。1980年代末期，集函數式編程研究成果於大成的Haskell發佈。

基于[JavaScript的某些开发理念强调函数式的实现方法](../Page/JavaScript.md "wikilink")。

## 速度和空間上的顧慮

函數式編程常被認為嚴重耗費CPU和記憶體資源。主因有二：

  - 在實現早期的函數式編程語言時並沒有考慮過效率問題。
  - 面向函数式编程特性（如保证函数参数不变性等）的独特数据结构和算法。

## 参考文献

## 外部連結

  - [Why Functional Programming
    Matters](http://www.cse.chalmers.se/~rjmh/Papers/whyfp.html)

{{-}}

[Category:編程典範](../Category/編程典範.md "wikilink")
[Category:函數式編程](../Category/函數式編程.md "wikilink")
[函数式编程语言](../Category/函数式编程语言.md "wikilink")