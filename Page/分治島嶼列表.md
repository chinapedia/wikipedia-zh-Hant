世界上絕大多數的[島嶼都由一個國家統治](../Page/島嶼.md "wikilink")，它們若非成為[島國](../Page/島國.md "wikilink")，就是成為大陸沿海國的部分領土。本列表列出了較為少見的**多國分治島嶼**（**跨國島嶼**）。這些島嶼通常較大，且當中亦有因為多國分治而陷於國際糾紛者。

## 當前的列表

### 海島

<table>
<thead>
<tr class="header">
<th><p>島嶼名稱</p></th>
<th><p>面積<br />
km<sup>2</sup></p></th>
<th><p>國家（擁有面積占比）</p></th>
<th><p>地圖（島嶼地理位置）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/新幾內亞島.md" title="wikilink">新幾內亞島</a></p></td>
<td><p>785,753[1]</p></td>
<td><p>（53.52%）<small><a href="../Page/巴布亞省.md" title="wikilink">巴布亞省</a>、<a href="../Page/西巴布亞省.md" title="wikilink">西巴布亞省</a></small><br />
（46.48%）</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:LocationNewGuinea.svg" title="fig:LocationNewGuinea.svg">LocationNewGuinea.svg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/婆羅洲.md" title="wikilink">婆羅洲</a> </p></td>
<td><p>748,168[2]</p></td>
<td><p>（73%）<small><a href="../Page/中加里曼丹省.md" title="wikilink">中加里曼丹省</a>、<a href="../Page/東加里曼丹省.md" title="wikilink">東加里曼丹省</a>、<a href="../Page/北加里曼丹省.md" title="wikilink">北加里曼丹省</a>、<a href="../Page/南加里曼丹省.md" title="wikilink">南加里曼丹省</a>、<a href="../Page/西加里曼丹省.md" title="wikilink">西加里曼丹省</a></small><br />
（26%）<small><a href="../Page/沙巴.md" title="wikilink">沙巴</a>、<a href="../Page/砂拉越.md" title="wikilink">砂拉越</a></small><br />
（1%）</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:LocationBorneo.svg" title="fig:LocationBorneo.svg">LocationBorneo.svg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛爾蘭島.md" title="wikilink">愛爾蘭島</a></p></td>
<td><p>81,638[3]</p></td>
<td><p>（83%）、（17%）<small><a href="../Page/北愛爾蘭.md" title="wikilink">北愛爾蘭</a></small></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Map_of_Ireland_in_Europe.svg" title="fig:Map_of_Ireland_in_Europe.svg">Map_of_Ireland_in_Europe.svg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/伊斯帕尼奧拉島.md" title="wikilink">伊斯帕尼奧拉島</a></p></td>
<td><p>73,929[4]</p></td>
<td><p>（65%）、（35%）</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:LocationHispaniola.svg" title="fig:LocationHispaniola.svg">LocationHispaniola.svg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/火地島.md" title="wikilink">火地島</a></p></td>
<td><p>47,992[5]</p></td>
<td><p>（56%）<small><a href="../Page/火地省_(智利).md" title="wikilink">火地省</a></small>、（44%）<small><a href="../Page/火地省_(阿根廷).md" title="wikilink">火地省</a></small></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tierra_del_Fuego_adm_location_map.svg" title="fig:Tierra_del_Fuego_adm_location_map.svg">Tierra_del_Fuego_adm_location_map.svg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/帝汶島.md" title="wikilink">帝汶島</a></p></td>
<td><p>28,418[6]</p></td>
<td><p>（53%）<small><a href="../Page/東努沙登加拉省.md" title="wikilink">東努沙登加拉省</a></small>、（47%）</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Timor-map.png" title="fig:Timor-map.png">Timor-map.png</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/賽普勒斯島.md" title="wikilink">賽普勒斯島</a></p></td>
<td><p>9,234[7]</p></td>
<td><p><strong><em><a href="../Page/按照法律的.md" title="wikilink">法律</a><strong>'' （97%）<br />
（3%）<small><a href="../Page/亞克羅提利與德凱利亞.md" title="wikilink">亞克羅提利與德凱利亞</a>、<a href="../Page/英屬基地區.md" title="wikilink">英屬基地區</a></small><br />
</strong></em><a href="../Page/De_facto.md" title="wikilink">實際</a></strong>''<br />
（58%）<br />
（37%）<br />
（3%）<small><a href="../Page/英屬基地區.md" title="wikilink">英屬基地區</a></small><br />
<a href="../Page/賽普勒斯聯合國緩衝區.md" title="wikilink">緩衝區</a>（2%）</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:LocationCyprus.svg" title="fig:LocationCyprus.svg">LocationCyprus.svg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/塞巴蒂克島.md" title="wikilink">塞巴蒂克島</a></p></td>
<td><p>452.2[8]</p></td>
<td><p><small><a href="../Page/北加里曼丹省.md" title="wikilink">北加里曼丹省</a></small>、<small><a href="../Page/沙巴.md" title="wikilink">沙巴</a></small></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Sebatik_Island.png" title="fig:Sebatik_Island.png">Sebatik_Island.png</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/烏瑟多姆島.md" title="wikilink">烏瑟多姆島</a></p></td>
<td><p>445[9]</p></td>
<td><p>（79%）[10] <small></small>、（21%）<small><a href="../Page/西波美拉尼亞省.md" title="wikilink">西波美拉尼亞省</a></small></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Baltic_Sea_map_Usedom_location.png" title="fig:Baltic_Sea_map_Usedom_location.png">Baltic_Sea_map_Usedom_location.png</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/聖馬丁島.md" title="wikilink">聖馬丁島</a></p></td>
<td><p>91.9[11]</p></td>
<td><p>（61%）<small></small>、（39%）<small></small></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Saint_martin_map.PNG" title="fig:Saint_martin_map.PNG">Saint_martin_map.PNG</a></p></td>
</tr>
<tr class="odd">
<td><p>（包括伊納卡里島Inakari）</p></td>
<td><p>0.71</p></td>
<td><p>、[12]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Sweden_location_map_(Nord).svg" title="fig:Sweden_location_map_(Nord).svg">Sweden_location_map_(Nord).svg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/法赫德國王大橋.md" title="wikilink">法赫德國王大橋</a>4號堤</p></td>
<td><p>0.66[13]</p></td>
<td><p>、</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Map_of_Bahrain.svg" title="fig:Map_of_Bahrain.svg">Map_of_Bahrain.svg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬凱特島.md" title="wikilink">馬凱特島</a>（）</p></td>
<td><p>0.03</p></td>
<td><p>（50%）<small></small>、（50%）<small><a href="../Page/烏普薩拉省.md" title="wikilink">烏普薩拉省和</a><a href="../Page/斯德哥爾摩省.md" title="wikilink">斯德哥爾摩省</a></small></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Märket_Island_map.svg" title="fig:Märket_Island_map.svg">Märket_Island_map.svg</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>&lt;0.03</p></td>
<td><p>（&gt;50%）、（&lt;50%）[14]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Карта_острова_Койлуото.svg" title="fig:Карта_острова_Койлуото.svg">Карта_острова_Койлуото.svg</a></p></td>
</tr>
</tbody>
</table>

  - <small>如果不把[賽普勒斯島上事實存在的](../Page/賽普勒斯島.md "wikilink")[北賽普勒斯政權看作是一個](../Page/北賽普勒斯.md "wikilink")[國家](../Page/國家.md "wikilink")，[婆羅洲是世界上唯一一分屬三國的島嶼](../Page/婆羅洲.md "wikilink")。</small>

  - <small>北賽普勒斯是一個尚未受到國際承認的政治實體；[英國佔有兩小塊](../Page/英國.md "wikilink")[賽普勒斯英屬基地區](../Page/賽普勒斯英屬基地區.md "wikilink")。</small>

### 湖島

  - 、和之間：

      - ，[三邊交界處的](../Page/三邊交界.md "wikilink")，距Goldajärvi/Koltajauri湖岸10米，因此可以說是極小的人工島。约14平方米。

  - 和之間：

      - 處的湖島，[育空](../Page/育空.md "wikilink")-[阿拉斯加公路的](../Page/阿拉斯加公路.md "wikilink")以北

      - 處的湖島，育空-阿拉斯加公路的邊境口岸以北

      - 處的湖島，靠近育空-阿拉斯加公路的邊境口岸北側

      - 處小湖中的兩個島，育空-阿拉斯加公路的邊境口岸以南8.2公里

      - 處小湖中的幾個岩石小島，位于[華盛頓州與](../Page/華盛頓州.md "wikilink")[不列顛哥倫比亞省交界處的](../Page/不列顛哥倫比亞.md "wikilink")[奧卡諾根縣](../Page/奧卡諾根縣.md "wikilink")

      - 處小湖中的一個島，位于華盛頓州與不列顛哥倫比亞省交界處的奧卡諾根縣

      - 處小湖中的一個島，毗鄰[蒙大拿州與](../Page/蒙大拿州.md "wikilink")[艾伯塔省邊交界的](../Page/艾伯塔.md "wikilink")

      - 處鹽湖（鹼湖，Salt Lake (Alkali
        Lake)）中的一個島，位于蒙大拿州與[薩斯喀徹溫省交界處的](../Page/薩斯喀徹溫.md "wikilink")[謝里敦縣](../Page/謝里敦縣_\(蒙大拿州\).md "wikilink")

      - 處中的一個[島](http://1.usa.gov/17q8yXx)，\[15\]，位于[北達科他州與薩斯喀徹溫省交界處](../Page/北達科他州.md "wikilink")

      - 處中的兩個島\[16\]，位于北達科他州與[曼尼托巴省交界處](../Page/曼尼托巴.md "wikilink")

      - 處中的一個[島](http://1.usa.gov/12esyDi)\[17\]，位于北達科他州與曼尼托巴省交界處

      - 處東部的一個小島\[18\]，位于北達科他州與曼尼托巴省交界處的[博蒂諾縣](../Page/博蒂諾縣_\(北達科他州\).md "wikilink")

      - 處小湖中的一個島\[19\]，位于北達科他州與曼尼托巴省交界處的[羅利特縣](../Page/羅利特縣_\(北達科他州\).md "wikilink")

      - 處小湖中的一個島\[20\]，位于北達科他州與曼尼托巴省交界處的[陶納縣](../Page/陶納縣_\(北達科他州\).md "wikilink")

      - 處小湖中的一個島\[21\]，位于北達科他州與曼尼托巴省交界處的[卡弗利爾縣](../Page/卡弗利爾縣_\(北達科他州\).md "wikilink")

      - 處小水庫（充滿時）中的一個[小島](http://1.usa.gov/13W4LMM)\[22\]，靠近北達科他州與曼尼托巴省交界處上的[口岸東側](../Page/口岸.md "wikilink")

      - 處小湖中的一個島，位于[緬因州與](../Page/緬因州.md "wikilink")[新布藍茲維省交界處的](../Page/新布藍茲維.md "wikilink")

      - 中的[普羅文斯島](../Page/普羅文斯島.md "wikilink")，位于[魁北克省](../Page/魁北克.md "wikilink")（91%）和[佛蒙特州](../Page/佛蒙特州.md "wikilink")（9%）交界處\[23\]

  - 和之間：

      - Klistervatn湖中的Store Grenenholmen<ref>

— 界碑\#169-172 </ref>

  -   - Grensevatn湖中的Korkeasaari和一個無名小島<ref>

— 界碑\#12-13（Korkeasaari）& \#14（無名小島） </ref>

  - 和之間：

      - 中的Äikkäänniemi\[24\]

      - Yla-Tirja湖中的和一個較小的島\[25\]

      - Melaselänjärvi湖中的Tarraassiinsaari、Härkäsaari和Kiteensaari\[26\]\[27\]

      - （Virmajärvi）中的一個島

      - Kokkojärvi湖中的\[28\]

      - 中的Kalmasaari\[29\]

      - Hietajärvi湖中的Varposaari\[30\]

      - Parvajärvi湖中的Parvajärvensaari\[31\]

      - Pukarijärvi / Ozero Pyukharin中的Keuhkosaari\[32\]\[33\]

      - （Onkamojärvi / Ozero
        Onkamo）中的Siiheojansuusaari和Tossensaari\[34\]\[35\]

  - 和之間：

      - Kivisarijärvi/Keđgisualuijävri湖中的一個島\[36\]
      - 界碑347A東南方湖中的一個島\[37\]

  - 和之間：\[38\]

      - Sör Vammsjön/Vammen Søndre湖中的一個島\[39\]

      - Norra Kornsjön/Nordre Kornsjø湖中的Hisön/Hisøya（≈0,09 km²）\[40\]

      - （Södra Boksjön/Søndre Boksjø）中的Kulleholmen/Kalholmen（≈5500
        m²）和Tagholm/Tåkeholmen（≈600 m²）\[41\]

      - 中的Salholmen和Trollön\[42\]

      - Tannsjøen/Tannsjön湖中的一個島\[43\]

      - Helgesjö湖中的Linneholmene\[44\]

      - Holmsjøen湖中的Jensøya\[45\]

      - 中的Storøya\[46\]

      - （Fallsjøen / Nordre Røgden）中的Fallsjøholmen\[47\]

      - Kroksjøen湖中的一個島\[48\]

      - 中的一個島\[49\]

      - Skurdalssjøen/Kruehkiejaevrie湖中的一個島\[50\]

      - Gihcijoka河海拔710米處湖中的一個島\[51\]

      - 中的三個島\[52\]

  - 和之間：

      - 湖中的Sosnovec和另一個無名島\[53\]\[54\]\[55\]\[56\]

  - 和之間：

      - Vearty湖中的Pollatawny\[57\]

  - 和之間：

      - [阿貝湖Aleilou角的一個島](../Page/阿貝湖.md "wikilink")\[58\]

  - 和之間的邊界跨過[新錫德爾湖](../Page/新錫德爾湖.md "wikilink")，有時因水位波動會露出跨越邊界的島嶼。

### 河島

  - 、和之間：

      - 一個小島（*Staustufe
        Apach*），在[摩塞爾河中](../Page/摩塞爾河.md "wikilink")[申根附近](../Page/申根.md "wikilink")，主要位于法國，河中一角由盧森堡和德國[共管](../Page/共管_\(國際法\).md "wikilink")。\[59\]

  - 和之間：

      - [黑瞎子島](../Page/黑瞎子島.md "wikilink")，在[烏蘇里江和](../Page/烏蘇里江.md "wikilink")[黑龍江交匯處](../Page/黑龍江.md "wikilink")。\[60\]\[61\]
      - [阿巴該圖洲渚](../Page/阿巴該圖洲渚.md "wikilink")，在[額爾古納河中](../Page/額爾古納河.md "wikilink")。\[62\]

  - 和之間：

      - [科羅科羅島](../Page/科羅科羅島.md "wikilink")，在[巴里馬河](../Page/巴里馬河.md "wikilink")[三角洲處](../Page/三角洲.md "wikilink")。\[63\]

  - 和之間：

      - 聖何塞島，在[內格羅河中](../Page/內格羅河_\(亞馬遜河\).md "wikilink")。\[64\]

  - 和之間：

      - [馬丁加西亞島](../Page/馬丁加西亞島.md "wikilink")，在[拉普拉塔河中](../Page/拉普拉塔河.md "wikilink")。\[65\]

  - 和之間：

      - [恒河](../Page/恒河.md "wikilink")、\[66\][緹斯塔河](../Page/緹斯塔河.md "wikilink")\[67\]和[布拉馬普特拉河](../Page/布拉馬普特拉河.md "wikilink")\[68\]下游接近[恒河三角洲處形成](../Page/恒河三角洲.md "wikilink")，含有大量泥沙島（稱為chars）。\[69\]這些島都足夠大且可以定居，但它們可能在任何時間與印度（[阿薩姆邦和](../Page/阿薩姆邦.md "wikilink")[西孟加拉邦](../Page/西孟加拉邦.md "wikilink")）和孟加拉國的邊界相接，而不再成為一個島，儘管兩國邊界還未完全劃清。

  - 和之間：

      - 標為“Q”的一個島，在[馬里查河中](../Page/馬里查河.md "wikilink")。\[70\]

  - 和之間：

      - 一個小島，在Uutuanjoki河中，位于。

  - 和之間：

      - 一個小島，在Tunnsjø附近的Vadet中，位于。

  - 和之間：

      - 一個小島，在跨越芬蘭[托爾尼奧和瑞典](../Page/托爾尼奧.md "wikilink")[哈帕蘭達的高爾夫球場西側](../Page/哈帕蘭達.md "wikilink")，位于.

  - 和之間：

      - Nokiel和另一無名小島，在[杜納耶茨河中](../Page/杜納耶茨河.md "wikilink")，位于。\[71\]

  - 和之間：

      - 共四個島，在[豐塞卡灣中](../Page/豐塞卡灣.md "wikilink")三角洲的直線邊界處。

  - 和之間：

      - 中連續9個島嶼，在附近的，最北端位于，最南端位于。

      - 阿拉斯加州東北部中的一個或多個島嶼，位于。

      - 阿拉斯加州北極村東北部[曼查溪](../Page/曼查溪.md "wikilink")（Mancha
        Creek）中的一個島，位于。

      - 阿拉斯加州北極村東北部[弗斯河岔流中的](../Page/:File:Firth_river_Arctic_national_wildlife_refuge.jpg.md "wikilink")6個（或更多）島嶼，位于。

      - [舊克羅下游處Sunaghun溪匯入](../Page/舊克羅.md "wikilink")[豪豬河河口附近的一個島](../Page/豪豬河.md "wikilink")，位于。

      - 阿拉斯加州東部、育空地區[藍魚湖](../Page/藍魚湖.md "wikilink")（Bluefish
        Lake）西西西南方向15公里處，[藍魚河](../Page/藍魚河.md "wikilink")（Bluefish
        River）分水嶺的一個島，位于。

      - 阿拉斯加州查爾基齊克東部Fort溪中的一個島，位于。

      - 阿拉斯加州查爾基齊克東部（Black River / Salmon Fork）中的一個島，位于。

      - 育空地區[河狸溪西南部](../Page/河狸溪.md "wikilink")中的一個島，位于。

      - 阿拉斯加州（Klukwan）附近中的兩個島，位于。

      - 吉爾伯特灣上游匯入的岔流中的11個（或更多）島嶼，位于。

      - 育空地區下游中的兩個島，位于。

      - [華盛頓州](../Page/華盛頓州.md "wikilink")/[不列顛哥倫比亞省](../Page/不列顛哥倫比亞.md "wikilink")（Waneta
        border crossing）處[哥倫比亞河中的一個島](../Page/哥倫比亞河.md "wikilink")，位于。

      - [蒙大拿州](../Page/蒙大拿州.md "wikilink")/[艾伯塔省](../Page/艾伯塔.md "wikilink")中的一個島，位于。

      - 蒙大拿州/亞伯達省[李溪](../Page/李溪_\(亞伯達-蒙大拿州\).md "wikilink")（Lee
        Creek）分支上的一個島，位于。

      - 蒙大拿州/亞伯達省[邊境溪](../Page/邊境溪.md "wikilink")（Boundary
        Creek）中的一個小島，位于。

      - 蒙大拿州/亞伯達省中的一個島，位于。

      - 蒙大拿州/亞伯達省中的一個小島，位于。

      - 蒙大拿州/[薩斯喀徹溫省](../Page/薩斯喀徹溫.md "wikilink")[蒙代爾溪](../Page/蒙代爾溪.md "wikilink")（Mundell
        Creek）中的一個島，位于。

      - 蒙大拿州/薩斯喀徹溫省[邁克易切恩溪](../Page/邁克易切恩溪.md "wikilink")（McEachern
        Creek）中的一個島，位于。

      - [北達科他州](../Page/北達科他州.md "wikilink")/[曼尼托巴省](../Page/曼尼托巴.md "wikilink")[博蒂諾縣的](../Page/博蒂諾縣_\(北達科他州\).md "wikilink")中的一個小島\[72\]，位于。

      - 北達科他州/曼尼托巴省中的一個島\[73\]，位于。

      - [緬因州](../Page/緬因州.md "wikilink")/[魁北克省](../Page/魁北克.md "wikilink")支流中的一個島，位于。

      - 緬因州/[新布藍茲維省](../Page/新布藍茲維.md "wikilink")中的一個島，位于。

      - 東南部的緬因州/新布藍茲維省邊境處，自緬因州溫柔湖（Gentle Lake）流入米迪斯尼克河北支（North
        Branch–Meduxnekeag River）的岔流中形成的兩個島（和）。

## 歷史上的列表

另一些島嶼在過去屬於多個國家，但現在已統一。

現代[民族國家的明確邊界劃分並不適用於其他形式的社會組織](../Page/民族國家.md "wikilink")，故被“分割”的島嶼可能並不太值得一提。例如，在[古希臘](../Page/古希臘.md "wikilink")，[优卑亚岛分屬多個](../Page/优卑亚岛.md "wikilink")[城邦](../Page/城邦.md "wikilink")，包括[哈爾基斯和](../Page/哈爾基斯.md "wikilink")[埃雷特里亞](../Page/埃雷特里亞.md "wikilink")；歐洲人移民[塔斯馬尼亞島之前](../Page/塔斯馬尼亞島.md "wikilink")，該島也分屬九個[土著部落](../Page/塔斯馬尼亞原住民.md "wikilink")。

戰爭時期的島嶼可能會分屬侵略方和保衛方，例如，分屬[奧斯曼帝國和](../Page/奧斯曼帝國.md "wikilink")[威尼斯共和國](../Page/威尼斯共和國.md "wikilink")。

曾被分割的島嶼包括：

  - **[科西嘉島](../Page/科西嘉島.md "wikilink")**：依據1132年教皇[諾森二世的裁決](../Page/諾森二世.md "wikilink")，被[比薩共和國和](../Page/比薩共和國.md "wikilink")[熱那亞共和國分割](../Page/熱那亞共和國.md "wikilink")，直到1284年的[梅洛里亞海戰](../Page/梅洛里亞海戰.md "wikilink")。此後科西嘉島依次歸屬熱那亞、[亞拉岡](../Page/阿拉贡王国.md "wikilink")、熱那亞、[科西嘉共和國](../Page/科西嘉共和國.md "wikilink")、[法蘭西王國](../Page/法蘭西王國.md "wikilink")、，最終由[法國管轄至今](../Page/法國.md "wikilink")。
  - **[撒丁岛](../Page/撒丁岛.md "wikilink")**：自公元900年之前直到1420年[阿波利亞王國滅亡期間分屬數個王國](../Page/阿波利亞王國.md "wikilink")（）。此後薩丁尼亞島依次歸屬[亞拉岡](../Page/阿拉贡王国.md "wikilink")、[西班牙帝國](../Page/西班牙帝國.md "wikilink")、[薩丁尼亞王國](../Page/薩丁尼亞王國.md "wikilink")、[義大利王國](../Page/意大利王國_\(1861–1946\).md "wikilink")，最終由[義大利共和國管轄至今](../Page/意大利.md "wikilink")。
  - **[薩列馬島](../Page/薩列馬島.md "wikilink")**（1237–1570）和**[希烏馬島](../Page/希烏馬島.md "wikilink")**（1254–1563）：分屬[利沃尼亚骑士团和](../Page/利沃尼亚骑士团.md "wikilink")[Bishopric
    of
    Ösel-Wiek](../Page/Bishopric_of_Ösel-Wiek.md "wikilink")（1560年後的[丹麥王國](../Page/丹麥王國.md "wikilink")）。此後依次歸屬丹麥（僅薩列馬島）、[瑞典王國](../Page/瑞典王國.md "wikilink")、[俄羅斯帝國](../Page/俄羅斯帝國.md "wikilink")、[愛沙尼亞](../Page/愛沙尼亞.md "wikilink")、[蘇聯](../Page/蘇聯.md "wikilink")、[納粹德國](../Page/納粹德國.md "wikilink")（1941-1944）、蘇聯，最終在1991年蘇聯解體後重歸於愛沙尼亞。
  - **[多巴哥島](../Page/多巴哥島.md "wikilink")**：自1654至1659年，該島上有[庫爾蘭和瑟米加利亞公國和](../Page/庫爾蘭和瑟米加利亞公國.md "wikilink")[荷蘭共和國的兩個殖民地](../Page/荷蘭共和國.md "wikilink")，但均經歷了經濟失敗而被放棄。<ref>

</ref>此後依次歸屬[法蘭西殖民帝國](../Page/法蘭西殖民帝國.md "wikilink")、[大不列顛王國](../Page/大不列顛王國.md "wikilink")（1706）、[大不列顛及愛爾蘭聯合王國](../Page/大不列顛及愛爾蘭聯合王國.md "wikilink")、今天的[英國](../Page/英國.md "wikilink")，最終由獨立後的[千里達及托巴哥管轄至今](../Page/千里達及托巴哥.md "wikilink")。

  - **[德那第島](../Page/德那第.md "wikilink")**：自1607至1663年，分屬與[蒂多雷結盟的](../Page/蒂多雷.md "wikilink")[西班牙帝國和與德那第的蘇丹結盟的](../Page/西班牙帝國.md "wikilink")[荷蘭共和國](../Page/荷蘭共和國.md "wikilink")。此後依次歸屬[荷蘭](../Page/荷蘭.md "wikilink")、[大日本帝國](../Page/大日本帝國.md "wikilink")（1942–1945）、荷蘭，1949年後由獨立的[印尼管轄至今](../Page/印尼.md "wikilink")。
  - **[紐約長島](../Page/長島_\(紐約\).md "wikilink")**：分屬荷蘭共和國和[英格蘭王國](../Page/英格蘭王國.md "wikilink")，自1640年（*[實際](../Page/De_facto.md "wikilink")*，成立）或1650年（*[法律](../Page/按照法律的.md "wikilink")*，依照）起，直到1664年[新尼德蘭向](../Page/新尼德蘭.md "wikilink")[英國陸軍投降](../Page/英國陸軍.md "wikilink")。此後依次歸屬英格蘭王國、[英國](../Page/英國.md "wikilink")，1781年後由[美國管轄至今](../Page/美國.md "wikilink")。長島自1781起為[紐約州的一部分](../Page/紐約州.md "wikilink")，且是[美國本土](../Page/美國本土.md "wikilink")（48州）的最大島嶼。

<!-- end list -->

  - **[大不列顛島](../Page/大不列顛島.md "wikilink")**：早期分屬三個或更多王國，包括[英格蘭](../Page/英格蘭.md "wikilink")、[威爾士和](../Page/威爾士.md "wikilink")[蘇格蘭](../Page/蘇格蘭.md "wikilink")，有時也成為[羅馬帝國和](../Page/羅馬帝國.md "wikilink")[丹麥帝國的一部分](../Page/丹麥殖民地.md "wikilink")。到1707年之前，後來減少到了兩個，[英格蘭王國和](../Page/英格蘭王國.md "wikilink")[蘇格蘭王國](../Page/蘇格蘭王國.md "wikilink")，並最終制定了[1707年聯合法令](../Page/1707年聯合法令.md "wikilink")，建立單一的[君主制和](../Page/君主制.md "wikilink")[議會](../Page/議會.md "wikilink")。自1707年起，英格蘭、蘇格蘭和威爾士依次歸屬[大不列顛王國](../Page/大不列顛王國.md "wikilink")、[大不列顛及愛爾蘭聯合王國和大不列顛及北愛爾蘭聯合王國](../Page/大不列顛及愛爾蘭聯合王國.md "wikilink")，即今天的[英國](../Page/英國.md "wikilink")。

<!-- end list -->

  - **[紐芬蘭島](../Page/紐芬蘭島.md "wikilink")**：早期分屬英國和法蘭西殖民帝國，直到1713年《[烏得勒支和約](../Page/烏得勒支和約.md "wikilink")》規定其屬於大英帝國。隨後該島又成為部分獨立的[紐芬蘭自治領](../Page/紐芬蘭自治領.md "wikilink")，最終在1949年成為[加拿大的一個省](../Page/加拿大.md "wikilink")。
  - **[聖基茨島](../Page/聖基茨島.md "wikilink")**：自1626年起至1713年《烏得勒支和約》為止，分屬[大不列顛和法蘭西殖民帝國](../Page/大不列顛.md "wikilink")。隨後250年屬大英帝國，最終由獨立的[圣基茨和尼维斯管轄](../Page/圣基茨和尼维斯.md "wikilink")。無論1713前後，在數次[加勒比海上的戰爭中](../Page/加勒比海.md "wikilink")，法國和英國均曾佔領過聖基茨全島。
  - **[厄爾巴島](../Page/厄爾巴島.md "wikilink")**：島上[費拉約港自](../Page/費拉約港.md "wikilink")1548年起屬[佛羅倫斯公爵](../Page/佛羅倫斯公國.md "wikilink")（後為[托斯卡纳大公国所取代](../Page/托斯卡纳大公国.md "wikilink")）管轄，直至1802年由《[亞眠和約](../Page/亞眠和約.md "wikilink")》割讓與法國；[波爾托阿祖羅自](../Page/波爾托阿祖羅.md "wikilink")1557年起先後歸[西班牙帝國的從屬國](../Page/西班牙帝國.md "wikilink")，和[那不勒斯王國管轄](../Page/那不勒斯王國.md "wikilink")，直至1801年由《》割讓與法國。該島其餘部分屬於，直至為[法蘭西殖民帝國擊敗後](../Page/法蘭西殖民帝國.md "wikilink")，[拿破仑一世於](../Page/拿破仑一世.md "wikilink")1802年將厄爾巴島納入[伊特魯里亞王國](../Page/伊特魯里亞王國.md "wikilink")。該島此後依次歸屬法蘭西殖民帝國、[埃莉薩·波拿巴之下的皮奧恩比諾公國](../Page/埃莉薩·波拿巴.md "wikilink")、[拿破崙主權之下](../Page/拿破仑一世#戰敗與流放.md "wikilink")（依據1814年《[楓丹白露和約](../Page/楓丹白露和約_\(1814年\).md "wikilink")》條款）、再歸[托斯卡尼](../Page/托斯卡尼.md "wikilink")、[義大利王國](../Page/意大利王國_\(1861–1946\).md "wikilink")、[義大利社會共和國](../Page/義大利社會共和國.md "wikilink")（1943–1944），最終由[義大利共和國管轄至今](../Page/意大利.md "wikilink")。\[74\]\[75\]
  - **[埃法特島](../Page/埃法特島.md "wikilink")**：1889年的數個月間分屬和[新赫布里底群島共管地](../Page/新赫布里底群島共管地.md "wikilink")，然後屬英法聯合海軍委員會。
  - **[庫頁島](../Page/庫頁島.md "wikilink")**，俄語名**薩哈林島**（****），日語名**樺太岛**（****）：1905年《[樸茨茅斯條約](../Page/樸茨茅斯條約.md "wikilink")》規定以[北緯50度線為界](../Page/北緯50度線.md "wikilink")，北部先後屬[俄羅斯帝國和](../Page/俄羅斯帝國.md "wikilink")[蘇聯](../Page/蘇聯.md "wikilink")，南部屬[大日本帝國](../Page/大日本帝國.md "wikilink")[樺太廳](../Page/樺太廳.md "wikilink")。[第二次世界大戰結束後](../Page/第二次世界大戰.md "wikilink")，[日本投降并放棄島上主權](../Page/日本投降.md "wikilink")，自此全島**事實上**歸蘇聯及後來的[俄羅斯聯邦管轄](../Page/俄羅斯.md "wikilink")。日本部分人士仍將該島南半部主權視為“未定”，許多日本地圖上標註該島南部為“[三不管地帶](../Page/无主地.md "wikilink")”。
  - ****：自[1867年英屬北美法令確立加拿大成立起](../Page/1867年英屬北美法令.md "wikilink")，分屬[加拿大和](../Page/加拿大.md "wikilink")、後來的[紐芬蘭自治領](../Page/紐芬蘭自治領.md "wikilink")，直至1949年將該島并入加拿大。
  - ****：在[委內瑞拉與英屬蓋亞那](../Page/委內瑞拉.md "wikilink")（今[圭亚那](../Page/圭亚那.md "wikilink")）邊境上的[庫尤尼河中](../Page/庫尤尼河.md "wikilink")。
  - **[香山島](../Page/香山島.md "wikilink")**：位於[珠江三角洲中](../Page/珠江三角洲.md "wikilink")，自1862年[天津條約起至](../Page/天津條約.md "wikilink")1999年[澳門回歸期間分屬](../Page/澳門回歸.md "wikilink")[中國和](../Page/中國.md "wikilink")[澳門](../Page/澳葡政府.md "wikilink")。

以下曾分屬多國的島嶼由於水位變化而已消失：

  - **沃兹罗日杰尼耶岛**（又名**復興島**）：自1991年[蘇聯解體後分屬獨立的](../Page/蘇聯解體.md "wikilink")和。2002年由於[鹹海水面減小而成為](../Page/鹹海.md "wikilink")[半島](../Page/半島.md "wikilink")，半島仍為兩國共有。
  - [乍得湖Bogomerom群島中的小島](../Page/乍得湖.md "wikilink")：曾分屬和\[76\]，但乍得湖水位在歷史上差異很大，目前已下降至使這些小島成為[非洲大陸一部分](../Page/非洲.md "wikilink")。

## 分屬一國之不同行政區

有些島嶼位於同一國家，但屬於多個不同的省或州，例如：

  - ：

      - ****：[紐芬蘭與拉布拉多省](../Page/紐芬蘭與拉布拉多.md "wikilink")、[努納武特](../Page/努納武特.md "wikilink")。
      - **[梅爾維爾島](../Page/梅爾維爾島_\(加拿大\).md "wikilink")**和**[維多利亞島](../Page/維多利亞島.md "wikilink")**：努納武特、[西北地區](../Page/西北地区_\(加拿大\).md "wikilink")。

  - ：

      - ****：[塔斯马尼亚州](../Page/塔斯马尼亚州.md "wikilink")、[維多利亞州](../Page/維多利亞州.md "wikilink")。

  - ：

      - **[埃利斯島](../Page/埃利斯島.md "wikilink")**：本身位於[新澤西州](../Page/新澤西州.md "wikilink")，包括一塊[紐約州的飛地](../Page/紐約州.md "wikilink")。

  - ：

      - **[香山島](../Page/香山島.md "wikilink")**：[广东省](../Page/广东省.md "wikilink")、。
      - **[崇明岛](../Page/崇明岛.md "wikilink")**：绝大部分在[上海市](../Page/上海市.md "wikilink")，小部分归属[江苏省](../Page/江苏省.md "wikilink")。

## 參見

  - [島嶼列表](../Page/島嶼列表.md "wikilink")

  - [島嶼國家](../Page/島嶼國家.md "wikilink")

  -
  - [共管
    (國際法)](../Page/共管_\(國際法\).md "wikilink")：一塊政治領土由兩個或以上的主權國家共治。例如[雉島](../Page/雉島.md "wikilink")（Pheasant
    Island），由和以六個月為週期交替管轄）。

  - [:分類:有爭議的島嶼和](../Page/:分類:有爭議的島嶼.md "wikilink")[世界主權爭端領土列表](../Page/世界主權爭端領土列表.md "wikilink")：列舉了許多多國宣稱擁有主權，但實際為一國管轄的島嶼。

  - [關塔那摩灣海軍基地](../Page/關塔那摩灣海軍基地.md "wikilink")：保留主權，無限期租用。

  - [朝韩非军事区](../Page/朝韩非军事区.md "wikilink")：包括數個較小的島嶼。

  - [漢斯島](../Page/漢斯島.md "wikilink")：2012年4月和（代表）談判，尚未最終確定，意在促成兩國共管或各擁有一半主權。\[77\]

## 參考

[\*](../Category/跨國島嶼.md "wikilink")
[島嶼列表](../Category/島嶼列表.md "wikilink")
[\*](../Category/分治.md "wikilink")

1.

2.
3.
4.
5.
6.
7.
8.
9.

10.
11.
12. [伊納卡里島（卡塔亚岛的芬蘭部分）地圖](http://kansalaisen.karttapaikka.fi/linkki?text=divided+island%3F&scale=16000&y=7289500&x=2507935&lang=en-GB)


13.

14. [科伊羅托島芬蘭部分地圖](http://kansalaisen.karttapaikka.fi/kartanhaku/osoitehaku.html?e=542347&n=6707960&scale=16000&tool=suurenna&styles=normal&lang=fi&tool=suurenna&lang=fi&map.x=308&map.y=294)


15.

16.

17.

18.

19.

20.

21.

22.

23.

24. [標記Äikkäänniemi的Nuijamaanjärvi湖部分](http://kansalaisen.karttapaikka.fi/linkki?text=%C3%84ikk%C3%A4%C3%A4nniemi+&scale=16000&y=6761563&x=4421353&lang=en-GB)，芬蘭公民地圖網站

25. [標記93（蘇爾島）和94（較小的島）號被分割島嶼的Yla-Tirja湖部分](http://kansalaisen.karttapaikka.fi/linkki?text=Divided+island&scale=16000&y=6835484&x=4489222&lang=en-GB)，芬蘭公民地圖網站

26. [顯示Tarraassiinsaari和Härkäsaari的Melaselänjärvi湖部分](http://kansalaisen.karttapaikka.fi/linkki?text=H%C3%A4rk%C3%A4saari&scale=16000&y=6929156&x=4560278&lang=en-GB)，芬蘭公民地圖網站

27. [顯示Kiteensaari的Melaselänjärvi湖部分](http://kansalaisen.karttapaikka.fi/linkki?text=Kiteensaari&scale=16000&y=6931328&x=4560650&lang=en-GB)，芬蘭公民地圖網站

28. [顯示拉亞島的Kokkojärvi湖部分](http://kansalaisen.karttapaikka.fi/linkki?text=Rajasaari&scale=16000&y=7033916&x=4539523&lang=en-GB)，芬蘭公民地圖網站

29. [顯示Kalmasaari的武奥基湖部分](http://kansalaisen.karttapaikka.fi/linkki?text=Kalmasaari&scale=16000&y=7172329&x=4506712&lang=en-GB)，芬蘭公民地圖網站

30. [顯示Varposaari的Hietajärvi湖部分](http://kansalaisen.karttapaikka.fi/linkki?text=Varposaari&scale=16000&y=7173201&x=4506184&lang=en-GB)，芬蘭公民地圖網站

31. [顯示Parvajärvensaari的Parvajärvi部分](http://kansalaisen.karttapaikka.fi/linkki?text=Parvaj%C3%A4rvensaari&scale=16000&y=7288902&x=4500529&lang=en-GB)，芬蘭公民地圖網站

32.

33. [標記Keuhkosaari的Pukarijärvi部分](http://kansalaisen.karttapaikka.fi/linkki?text=Keuhkosaari&scale=16000&y=7319252&x=4501271&lang=en-GB)，芬蘭公民地圖網站

34. International 國際邊界研究第74號，第22頁. ""The frontier follows the creek down
    to Lake Onkamojarvi, intersects the small island of Siiheojansuusaai
    and proceeds in a straight line to the small island of Tossensaari."

35. [Onkamojärvi湖部分](http://kansalaisen.karttapaikka.fi/linkki?text=Siiheojansuusaari-Tossonsaari+line&scale=16000&y=7411169&x=4461276&lang=en-GB)，芬蘭公民地圖網站（Siiheojansuusaari是IV/179；Tossonsaari是IV/180）

36. [Portion of Kivisarijärvi with divided island
    marked](http://kansalaisen.karttapaikka.fi/linkki?text=divided+island&scale=16000&y=7740042&x=4456945&lang=en-GB)
    from Citizen's Mapsite of Finland.

37. [標記被分割島嶼的界碑347A附近區域](http://kansalaisen.karttapaikka.fi/linkki?text=divided+island&scale=16000&y=7757408&x=3550112&lang=en-GB)，芬蘭公民地圖網站


38. 經[挪威國家測繪局網站](http://ngis2.statkart.no/norgesglasset/default.html) 驗證

39.

40. Hisøya：

41. Søndre Boksjø：

42. Salholmen：；Trollön：

43. Tannsjøen島（("Nr 54" on Norwegian map):

44. Linneholmene：

45. Jensøya：

46. Storøya：

47. Fallsjøholmen：

48. Kroksjøen湖中島：

49. 沃恩湖中島：

50. Skurdalssjøen湖中島：

51. Gihcijoka河的湖中島：

52. 科爾維湖中島：最大：；中：；南：

53. 此地圖上國界已標出。

54.

55.  此地圖上有Sosnovec名稱。

56. Drūkšiai湖坐標：

57. 經[多尼戈爾郡托貝爾](../Page/多尼戈爾郡.md "wikilink")（Tober）6英寸的地圖驗證；1905年05月05日調查。坐標：
    ：[G996663](http://getamap.ordnancesurvey.co.uk/getamap/frames.htm?mapAction=gaz&gazName=g&gazString=G996663)

58.

59. . Coordinates:

60. [Дополнительное соглашение между Российской Федерацией и Китайской
    Народной Республикой о российско-китайской государственной границе
    на ее Восточной
    части](http://bestpravo.ru/fed2004/data03/tex14451.htm)  от 14
    октября 2004 года.

61. 中國曾經對該島全島有主權要求，但該島一直被俄羅斯實際控制，根據2004年《[中華人民共和國和俄羅斯聯邦關於中俄國界東段的補充協定](../Page/中華人民共和國和俄羅斯聯邦關於中俄國界東段的補充協定.md "wikilink")》，中國放棄黑瞎子島東半部的主權，從而得以收回該島的西半部。

62.
63. 科羅科羅島的北岸位于開放海面，但它并非真正的海島，因為南部邊界在一條[淡水海峽中](../Page/淡水.md "wikilink")。委內瑞拉聲稱對全島擁有主權。

64. 坐標：

65. [Barros, Vicente (Coordinator) Impact Of Global Change On The
    Coastal Areas Of The Rio De La Plata: Sea Level Rise And
    Meteorological Effects.
    Page 7](ftp://ftp.agu.org/aiacc/Archieved%20Files/Abridged%20Proposals/Abridged_LA26)

66. 參見孟加拉國[諾瓦布甘傑縣的](../Page/諾瓦布甘傑縣.md "wikilink")[地圖](http://www.banglapedia.org/Maps/MN_0140.GIF)
    、[拉傑沙希縣的](../Page/拉傑沙希縣.md "wikilink")[地圖](http://www.banglapedia.org/Maps/MR_0079.GIF)
    及[庫什蒂亞縣的](../Page/庫什蒂亞縣.md "wikilink")[Daulatpur
    upazila地圖](http://www.banglapedia.org/Maps/MD_0071.GIF) 。

67. 參見孟加拉國[尼爾帕馬里縣的](../Page/尼爾帕馬里縣.md "wikilink")[Dilma
    upazila地圖](http://www.banglapedia.org/Maps/MD_0231.GIF) 。

68. 參見孟加拉國[古里格拉姆縣的](../Page/古里格拉姆縣.md "wikilink")[地圖](http://www.banglapedia.org/Maps/MK_0322.GIF)
    。

69.

70.

71.

72.

73.

74.

75.

76.

77.