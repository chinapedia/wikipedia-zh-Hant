[Fra_Angelico_009.jpg](https://zh.wikipedia.org/wiki/File:Fra_Angelico_009.jpg "fig:Fra_Angelico_009.jpg")，1432-1435\]\]
**区分绵羊和山羊**是[耶稣在](../Page/耶稣.md "wikilink")[橄榄山讲论中用来說明](../Page/橄榄山讲论.md "wikilink")[祂再來时的事的一个](../Page/耶稣再临.md "wikilink")[比喻](../Page/耶稣的比喻.md "wikilink")，或被称为“**对万国/列族/众民的审判**（****）”，被记载于。

## 內容

在這个比喻中，耶穌把信徒分為兩邊，一邊是能得救的[綿羊](../Page/綿羊.md "wikilink")，一邊是不能得救的[山羊](../Page/山羊.md "wikilink")，不信的人不包括在內。綿羊能得救不是因為他們單遵守道理沒有犯[死罪](../Page/死罪.md "wikilink")，而是他們能以耶穌的愛實行主的新命令——彼此相[愛](../Page/愛.md "wikilink")，不是單愛那愛自己的人，且愛那些身心窮困而饑餓，乾渴，沒衣服穿，沒地方住，或遇患難，受[逼迫](../Page/逼迫.md "wikilink")，生病的同靈。山羊雖遵守神的[律法](../Page/律法.md "wikilink")，沒有違背[十誡](../Page/十誡.md "wikilink")，但他們可能只愛那愛他們的人，不能用耶穌的愛，愛那些患難中窮困的同靈,
因此違背了耶穌最後賜下的最重要的命令——[彼此相愛](../Page/爱#基督宗教.md "wikilink")。

## 参考文献

## 外部連結

  - [John Gill's
    commentary](http://westover.searchgodsword.org/com/geb/view.cgi?book=mt&chapter=025&verse=035)

## 参见

  - [橄榄山讲论](../Page/橄榄山讲论.md "wikilink")、[最后的审判](../Page/最后的审判.md "wikilink")
  - [基督教末世论](../Page/基督教末世论.md "wikilink")、[基督教救赎论](../Page/基督教救赎论.md "wikilink")
  - [撒网的比喻](../Page/撒网的比喻.md "wikilink")、[稗子的比喻](../Page/稗子的比喻.md "wikilink")
  - [公绵羊和公山羊的异象](../Page/公绵羊和公山羊的异象.md "wikilink")
  - [十个童女的比喻](../Page/十个童女的比喻.md "wikilink")

{{-}}  {{@Bible|马太福音|25|31-36}}

[Category:以動物作比喻](../Category/以動物作比喻.md "wikilink")