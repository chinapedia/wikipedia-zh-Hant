在[抽象代数中](../Page/抽象代数.md "wikilink")，**分配律**是[二元运算的一个性质](../Page/二元运算.md "wikilink")，它是[基本代数中的分配律的推广](../Page/基本代数.md "wikilink")。

## 定義

設\(*\)及\(+\)是定义在[集合](../Page/集合.md "wikilink")\(S\)上的兩個[二元運算](../Page/二元運算.md "wikilink")，我們說

  - \(*\)对于\(+\)满足左分配律，如果：

<!-- end list -->

  -

      -
        \(\forall x,y,z \in S, x * (y+z) = (x*y)+(x*z)\);

<!-- end list -->

  - \(*\)对于\(+\)满足右分配律，如果：

<!-- end list -->

  -

      -
        \(\forall x,y,z \in S, (y+z)*x = (y*x)+(z*x)\);

<!-- end list -->

  - 如果\(*\)对于\(+\)同時满足左分配律和右分配律，那么我們說\(*\)对于\(+\)满足分配律。

如果\(*\)满足[交换律](../Page/交换律.md "wikilink")，那么以上三条语句在邏輯上是[等价的](../Page/等价.md "wikilink")。

## 例子

  - 除了实数以外，[自然数](../Page/自然数.md "wikilink")、[复数和](../Page/复数.md "wikilink")[基数中的乘法都对加法满足分配律](../Page/基数.md "wikilink")。
  - 然而，[序数的乘法对加法只满足左分配律](../Page/序数.md "wikilink")，不满足右分配律。
  - [矩阵乘法对](../Page/矩阵乘法.md "wikilink")[矩阵加法满足分配律](../Page/矩阵加法.md "wikilink")（但不满足交换律）。
  - [集合的](../Page/集合.md "wikilink")[并集对](../Page/并集.md "wikilink")[交集满足分配律](../Page/交集.md "wikilink")，交集对并集也满足分配律。另外，交集对[对称差也满足分配律](../Page/对称差.md "wikilink")。
  - [逻辑析取对](../Page/逻辑析取.md "wikilink")[逻辑合取满足分配律](../Page/逻辑合取.md "wikilink")，逻辑合取对逻辑析取也满足分配律。另外，逻辑合取对[逻辑异或也满足分配律](../Page/逻辑异或.md "wikilink")。
  - 对于[实数](../Page/实数.md "wikilink")（或任何[全序集合](../Page/全序集合.md "wikilink")），最大值对最小值满足分配律，反之亦然：

<!-- end list -->

  -

      -
        \(\operatorname{max}(a,\operatorname{min}(b,c)) = \operatorname{min}(\operatorname{max}(a,b),\operatorname{max}(a,c))\)
        \(\operatorname{min}(a,\operatorname{max}(b,c)) = \operatorname{max}(\operatorname{min}(a,b),\operatorname{min}(a,c))\)。

<!-- end list -->

  - 对于[整数](../Page/整数.md "wikilink")，[最大公因子对](../Page/最大公因子.md "wikilink")[最小公倍数满足分配律](../Page/最小公倍数.md "wikilink")，反之亦然：

<!-- end list -->

  -

      -
        \(\operatorname{gcd}(a,\operatorname{lcm}(b,c)) = \operatorname{lcm}(\operatorname{gcd}(a,b),\operatorname{gcd}(a,c))\)

<!-- end list -->

  -

      -
        \(\operatorname{lcm}(a,\operatorname{gcd}(b,c)) = \operatorname{gcd}(\operatorname{lcm}(a,b),\operatorname{lcm}(a,c))\)。

<!-- end list -->

  - 对于[实数](../Page/实数.md "wikilink")，加法对最大值满足分配律，对最小值也满足分配律：

<!-- end list -->

  -

      -
        \(a + \operatorname{max}(b,c) = \operatorname{max}(a+b,a+c)\)

<!-- end list -->

  -

      -
        \(a + \operatorname{min}(b,c) = \operatorname{min}(a+b,a+c)\)。

## 环的分配律

分配律在[环和](../Page/环_\(代数\).md "wikilink")[分配格中很常见](../Page/分配格.md "wikilink")。

一个环有两个二元运算（通常称为\(+\)和\(*\)），其中一个要求是\(*\)必须对\(+\)满足分配律。

[格是另外一种具有两个二元运算](../Page/格_\(数学\).md "wikilink")\(\wedge\)和\(\vee\)的[代数结构](../Page/代数结构.md "wikilink")。如果这两个运算中的任何一个（例如\(\wedge\)）对另外一个（\(\vee\)）满足分配律，则\(\vee\)对\(\wedge\)也一定满足分配律，这时这个格便称为分配格。

## 參見

  - [交換律](../Page/交換律.md "wikilink")
  - [結合律](../Page/結合律.md "wikilink")
  - [遞移關係](../Page/遞移關係.md "wikilink")

[F](../Category/初等代数.md "wikilink")