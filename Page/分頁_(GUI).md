[Firefox_2_Tabbed_Browsing_GNU-Linux.png](https://zh.wikipedia.org/wiki/File:Firefox_2_Tabbed_Browsing_GNU-Linux.png "fig:Firefox_2_Tabbed_Browsing_GNU-Linux.png")，在網址列下方有個分頁列，可以用來切換多個網頁，節省螢幕空間，擺脫開新多視窗的困擾。\]\]

**分頁**，又稱為**頁籤**，Windows內建的瀏覽器又稱為**索引標籤**。指的是在帳冊或是資料夾邊緣用來索引用的[標籤](../Page/標籤.md "wikilink")，抓住的話可以來快速檢索特定頁面，分頁瀏覽器就是以此為概念製作成[圖形使用者界面](../Page/圖形使用者界面.md "wikilink")。

早期瀏覽器原則上一個視窗就-{只}-能顯示一個頁面，要開啟新的頁面-{只}-能使用現有的視窗或是開啟新的視窗，同時要看多個網頁也就-{只}-能開啟多個視窗，這樣下去整個畫面都是瀏覽器的視窗變得不容易管理，並且會使用大量的[記憶體造成電腦效能降低](../Page/記憶體.md "wikilink")。

為了解決這個問題，使用了[MDI](../Page/MDI.md "wikilink")（Multi-Document
Interface）的原理設計出了分頁瀏覽器。僅使用一個視窗就可以用分頁來呈現多個網頁，另外因為多個網頁已經集中於一個視窗，因此不需要特別去調整視窗，記憶體也相對用量較少。如果碰到會開啟多個視窗的Script也只需要關掉一個視窗即可，同時為了解決前述這個問題，瀏覽器也相對提升了功能，例如提供了阻擋[彈出式廣告的特色](../Page/彈出式廣告.md "wikilink")。

## 代表的分頁瀏覽器

以下依照[HTML排版引擎的種類分類](../Page/HTML排版引擎.md "wikilink")

### [KHTML](../Page/KHTML.md "wikilink")（[Apple WebKit](../Page/WebKit.md "wikilink")）

  - [Safari](../Page/Safari.md "wikilink")
  - [OmniWeb](../Page/OmniWeb.md "wikilink")
  - [Konqueror](../Page/Konqueror.md "wikilink")
  - [Shiira](../Page/Shiira.md "wikilink")
  - [Maxthon 3](../Page/Maxthon.md "wikilink")

### [Gecko](../Page/Gecko.md "wikilink")

  - [Epiphany](../Page/Epiphany.md "wikilink")
  - [Galeon](../Page/Galeon.md "wikilink")
  - [Mozilla Suite](../Page/Mozilla.md "wikilink")
  - [Mozilla Firefox](../Page/Mozilla_Firefox.md "wikilink")
  - [SeaMonkey](../Page/SeaMonkey.md "wikilink")
  - [Netscape 6](../Page/Netscape_6.md "wikilink")、[Netscape
    7](../Page/Netscape_7.md "wikilink")、[Netscape
    Browser](../Page/Netscape_Browser.md "wikilink")、[Netscape Navigator
    9](../Page/Netscape_Navigator_9.md "wikilink")
  - [Sylera](../Page/Sylera.md "wikilink")
  - [K-Meleon](../Page/K-Meleon.md "wikilink")
  - [Camino](../Page/Camino.md "wikilink")

### [Presto](../Page/Presto.md "wikilink")

  - [Opera](../Page/Opera.md "wikilink")

### [Trident](../Page/Trident_\(排版引擎\).md "wikilink")

  - Windows版[Internet
    Explorer](../Page/Internet_Explorer.md "wikilink")\[1\]
  - [Avant Browser](../Page/Avant_Browser.md "wikilink")
  - [Sleipnir](../Page/Sleipnir.md "wikilink")\[2\]
  - [Grani](../Page/Grani.md "wikilink")
  - [Lunascape](../Page/Lunascape.md "wikilink")\[3\]
  - [Maxthon](../Page/Maxthon.md "wikilink")（原本的MyIE2）
  - [NetCaptor](../Page/NetCaptor.md "wikilink")
  - [Picea](../Page/Picea.md "wikilink")

### [w3m](../Page/w3m.md "wikilink")（文字瀏覽器）

  - [w3m](../Page/w3m.md "wikilink")

## 註解

<references />

[it:Navigazione a schede](../Page/it:Navigazione_a_schede.md "wikilink")
[ja:タブブラウザ](../Page/ja:タブブラウザ.md "wikilink") [ru:Многодокументный
интерфейс со
вкладками](../Page/ru:Многодокументный_интерфейс_со_вкладками.md "wikilink")

[Category:網頁瀏覽器](../Category/網頁瀏覽器.md "wikilink")
[Category:圖形用戶界面](../Category/圖形用戶界面.md "wikilink")

1.  \-{只}-有在2006年登場的7.0版直接具備分頁瀏覽機能，5.0以上的版本可以用裝MSN工具列的方式導入分頁瀏覽，但是以機能性來說不如其他分頁瀏覽器，另外MSN工具列會將數個視窗同時以分頁的方式顯示，並不能節省記憶體。[1](http://level.s69.xrea.com/mozilla/index.cgi?id=20050609_IE6_Tab)（日文說明）
2.  Sleipnir 1.60之後的版本也可以使用Gecko排版引擎。但是使用Sleipnir 1.6x和Sleipnir
    2.x的archive版本的時候需要另外安裝Gecko排版引擎的插件。
3.  Lunascape2之後的版本也可以使用Gecko排版引擎。