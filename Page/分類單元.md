**分類單元**（**分類群**，）是指[分類學上的一個群體](../Page/分類學.md "wikilink")\[1\]\[2\]，不管處哪一個**[分類階層](../Page/分類階層.md "wikilink")**（taxonomic
rank），稱此群體為**分類群**。

## 範圍與命名

各個**分類階層**皆可能具多個分類群，而其整體亦為一個分類群。分類群可以給定一個名字，但若指整個分類階層，則其命名便要受國際命名法規規範，例如種應有[種的](../Page/種.md "wikilink")[學名](../Page/學名.md "wikilink")、[綱需有綱的](../Page/綱.md "wikilink")[學名](../Page/學名_\(植物\).md "wikilink")。

以[槐為例](../Page/槐树.md "wikilink")，以下列出十個分類階層，主要的分階層（principal ranks）\[3\]
指界、門、綱、目、科、屬、種等七個，若有需要可以在七個主要的階層之上或之下，再加合適的次級階層（secondary
ranks，例如科與屬之間的族、組、系，種下的亞，或是亞科、亞屬等等）\[4\]。每個階層皆有其特定的學名，學名則受到[命名法規的規範](../Page/國際植物命名法規.md "wikilink")。

  -
    域： [真核域](../Page/真核域.md "wikilink") Eukarya
      -
        界： [植物界](../Page/植物界.md "wikilink") Plantae
          -
            門： [被子植物門](../Page/被子植物門.md "wikilink") Magnoliophyta
              -
                綱：[雙子葉植物綱](../Page/雙子葉植物綱.md "wikilink") Magnoliopsida
                  -
                    目： [豆目](../Page/豆目.md "wikilink") Fabales
                      -
                        科： [豆科](../Page/豆科.md "wikilink") Fabaceae
                          -
                            亞科： [蝶型花亞科](../Page/蝶型花亞科.md "wikilink")
                            Faboideae Lindl.
                              -
                                族：[槐族](../Page/槐族.md "wikilink")
                                Sophoreae
                                  -
                                    屬：[槐屬](../Page/槐屬.md "wikilink")
                                    *Styphnolobium*
                                      -
                                        種：槐 *Styphnolobium japonicum*
                                        (L.) Schott

## 參見

  - [植物分類學](../Page/植物分類學.md "wikilink")
  - [生物分類法](../Page/生物分類法.md "wikilink")
  - [支序分類學](../Page/支序分類學.md "wikilink")
  - [單系群](../Page/單系群.md "wikilink")

## 注釋

## 參考文獻

  - McNeill, J. et al. 2006. International Code of Botanical
    Nomenclature (Vienna Code). Regnum Vegetabile 146. A.R.G. Gantner
    Verlag KG. ISBN 0080-0694. [***Vienna Code** on-line
    version*](https://web.archive.org/web/20121006231936/http://ibot.sav.sk/icbn/main.htm)
  - [INTERNATIONAL CODE OF ZOOLOGICAL NOMENCLATURE (**ICZN**)
    online](http://www.nhm.ac.uk/hosted-sites/iczn/code/)，retrieved
    2011-06-16.

[Category:生物分類學](../Category/生物分類學.md "wikilink")
[Category:植物分類學](../Category/植物分類學.md "wikilink")
[Category:生物学命名法](../Category/生物学命名法.md "wikilink")

1.  [Vienna Code](http://ibot.sav.sk/icbn/main.htm)
    ，「[國際植物命名法規中對此字的釋義較簡單](../Page/國際植物命名法規.md "wikilink")，不管何種[階層](../Page/分類階層.md "wikilink")，是分類學上的一個群體」。
2.  [ICZN / Glossary:
    **taxon**](http://www.nhm.ac.uk/hosted-sites/iczn/code/)，[國際動物命名規約對於分類群的解釋較多](../Page/國際動物命名規約.md "wikilink")，定義分類為「一個分類學上的單元，不管是否具有學名；一個被認為具親緣關係的物種之一個[族群或多個族群](../Page/族群.md "wikilink")，具有能分別出相似的其他一群之共同的[特徵之單元](../Page/特徵.md "wikilink")。一個分類群包含了較低階層的所有分類群及其中的[個體](../Page/生物.md "wikilink")。」。
3.  [Vienna
    Code 3.1](http://ibot.sav.sk/icbn/frameset/0007Ch1Art003.htm)
4.  [Vienna
    Code 4.1, 4.2](http://ibot.sav.sk/icbn/frameset/0008Ch1Art004.htm)