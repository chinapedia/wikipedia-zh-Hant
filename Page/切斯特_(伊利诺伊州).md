**切斯特**（）位于[美国](../Page/美国.md "wikilink")[伊利诺伊州](../Page/伊利诺伊州.md "wikilink")[密西西比河谷](../Page/密西西比河.md "wikilink")，是[兰道夫县的县治所在](../Page/兰道夫县_\(伊利诺伊州\).md "wikilink")，根據[2010年美國人口普查](../Page/2010年美國人口普查.md "wikilink")，此城市人口有8,586人\[1\]。

## 人口統計

根據[2010年美國人口普查](../Page/2010年美國人口普查.md "wikilink")，切斯特人口有8,586人，其中男性有6,091人，女性有2,495人，[人口密度為每平方公里](../Page/人口密度.md "wikilink")569.37人，住房計2,193棟，住戶計2,001戶。城市內的[白人有](../Page/美國白人.md "wikilink")5,798人，[非裔美國人有](../Page/非裔美國人.md "wikilink")2,397人，[美國原住民有](../Page/美國原住民.md "wikilink")12人，[亞裔美國人有](../Page/亞裔美國人.md "wikilink")26人，太平洋島裔美國人有1人，其他種族有274人，[混血種族則有](../Page/混血兒.md "wikilink")78人。此外城市內有521人為西班牙裔或拉丁裔美國人。此城市之年齡中位數為37.2歲，而男性年齡中位數為35.7歲，女性年齡中位數為43.8歲。\[2\]

## 參考資料

## 外部連結

  - [City of Chester official website](http://www.chesterill.com)
  - *[Randolph County Herald
    Tribune](http://randolphcountyheraldtribune.com/)*
  - *[Sun Times News
    Online](https://web.archive.org/web/20081207010431/http://www.suntimesnews.com/2/index.html)*
  - [618 Football - Chester Yellow
    Jackets](https://web.archive.org/web/20081025025958/http://www.618football.com/teams/sirr-mississippi/chester-yellowjackets/)

[Category:伊利诺伊州城市](../Category/伊利诺伊州城市.md "wikilink")
[Category:伊利诺伊州县城](../Category/伊利诺伊州县城.md "wikilink")
[Category:密西西比河聚居地](../Category/密西西比河聚居地.md "wikilink")
[Category:1829年建立的聚居地](../Category/1829年建立的聚居地.md "wikilink")
[Category:1829年伊利諾州建立](../Category/1829年伊利諾州建立.md "wikilink")

1.
2.