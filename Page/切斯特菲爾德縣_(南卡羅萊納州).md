**切斯特菲爾德縣** （**Chesterfield County, South
Carolina**）是[美國](../Page/美國.md "wikilink")[南卡羅萊納州東北部的一個縣](../Page/南卡羅萊納州.md "wikilink")，北鄰[北卡羅萊納州](../Page/北卡羅萊納州.md "wikilink")。面積2,087平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口42,768人
（2005年人口43,435人）。\[1\]縣治[切斯特菲爾德](../Page/切斯特菲爾德_\(南卡羅萊納州\).md "wikilink")（Chesterfield
）。

成立於1785年3月12日。縣名來自[第四代切斯特菲爾德伯爵](../Page/第四代切斯特菲爾德伯爵.md "wikilink")。\[2\]

## 参考文献

<div class="references-small">

<references />

</div>

[C](../Category/南卡罗来纳州行政区划.md "wikilink")

1.  <http://www.census.gov/popest/counties/tables/CO-EST2005-01-45.xls>
2.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.