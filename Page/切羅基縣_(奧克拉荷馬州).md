**切羅基縣**（）是[美國](../Page/美國.md "wikilink")[奧克拉荷馬州東北部的一個縣](../Page/奧克拉荷馬州.md "wikilink")，面積2,011平方公里。根據[2010年人口普查](../Page/美國2010年人口普查.md "wikilink")，本縣共有人口46,987人\[1\]\[2\]。本縣縣治為[塔勒闊](../Page/塔勒闊_\(愛阿華州\).md "wikilink")（），而塔勒闊同時也是[切羅基國](../Page/切羅基國.md "wikilink")（）的首府。\[3\]

## 歷史

[Tahlequah,_Oklahoma.jpg](https://zh.wikipedia.org/wiki/File:Tahlequah,_Oklahoma.jpg "fig:Tahlequah,_Oklahoma.jpg")
[Seminary_Hall.jpg](https://zh.wikipedia.org/wiki/File:Seminary_Hall.jpg "fig:Seminary_Hall.jpg")
根據美國地名索引指出，本縣是於1907年成立。\[4\]
可是，《俄克拉何馬州歷史和文化百科全書》（）卻指出本縣是於1906年成立的。\[5\]本縣縣名取自[切羅基族](../Page/切羅基族.md "wikilink")。

於1830年，美國通過《[印第安人迁移法案](../Page/印第安人迁移法案.md "wikilink")》，迫使大量切羅基人遷往本縣，即[美国西进运动](../Page/美国西进运动.md "wikilink")。本縣內首次大型遷徒的目的地是[帕克希爾](../Page/帕克希爾_\(俄克拉何馬州\).md "wikilink")。後來，其中一個遷徒目的地塔勒闊成為了本縣縣治。可是，[美國內戰促使切羅基部落分裂](../Page/美國內戰.md "wikilink")，並導致許多早期的切羅基族建築物被破壞。在1870年代中期，白人開始非法遷入本縣，並於1890年代取代切羅基人成為本縣的主要種族。\[6\]

於1851年，切羅基男學院在塔勒闊正式開設，而切羅基女學院則在帕克希爾開設。後者於1887年被燒毀，並於塔勒闊重建。於1910年，一場大火燒毀了男學院。之後，女學院變成了東北州立師範學校，並於現代成為[東北州立大學的一部份](../Page/東北州立大學.md "wikilink")。另外，東北州立大學也是位於本縣內。\[7\]

在1901年至1903年期間，歐薩克-切諾基中央鐵路（後來變成了[聖路易斯 -
舊金山鐵路](../Page/聖路易斯_-_舊金山鐵路.md "wikilink")）成為了本縣的首條鐵路。該鐵路在1920年代大大地提升了本縣的農業產品出貨量，但出貨量在[大萧条时期中下跌了](../Page/大萧条时期.md "wikilink")。隨著農業在本縣式微，所有鐵路服務在1942年被終止了。\[8\]

## 地理

[Illinois_River_Oklahoma.jpg](https://zh.wikipedia.org/wiki/File:Illinois_River_Oklahoma.jpg "fig:Illinois_River_Oklahoma.jpg")
[USACE_Tenkiller_Lake_and_Dam.jpg](https://zh.wikipedia.org/wiki/File:USACE_Tenkiller_Lake_and_Dam.jpg "fig:USACE_Tenkiller_Lake_and_Dam.jpg")

根據[2000年人口普查](../Page/2000年美国人口普查.md "wikilink")，切羅基縣的總面積為，其中有，即96.73%為陸地；，即3.27%為水域。\[9\]

本縣位於歐薩克山脈的山麓地帶。幾乎整個[滕基勒费里水库和部份](../Page/滕基勒费里水库.md "wikilink")[吉布森堡湖都位於本縣內](../Page/吉布森堡湖.md "wikilink")。本縣的主要河流是[伊利諾伊河](../Page/伊利諾伊河_\(俄克拉何馬州\).md "wikilink")，而[格蘭德河則形成了本縣的西部邊界](../Page/格蘭德河_\(俄克拉何馬州\).md "wikilink")。\[10\]

### 主要公路

  - [US_62.svg](https://zh.wikipedia.org/wiki/File:US_62.svg "fig:US_62.svg")
    [美國62號公路](../Page/美國62號公路.md "wikilink")
  - [Oklahoma_State_Highway_10.svg](https://zh.wikipedia.org/wiki/File:Oklahoma_State_Highway_10.svg "fig:Oklahoma_State_Highway_10.svg")
    [10號高速公路](../Page/10號高速公路_\(奧克拉荷馬州\).md "wikilink")
  - [Oklahoma_State_Highway_51.svg](https://zh.wikipedia.org/wiki/File:Oklahoma_State_Highway_51.svg "fig:Oklahoma_State_Highway_51.svg")
    [51號高速公路](../Page/51號高速公路_\(奧克拉荷馬州\).md "wikilink")
  - [Oklahoma_State_Highway_82.svg](https://zh.wikipedia.org/wiki/File:Oklahoma_State_Highway_82.svg "fig:Oklahoma_State_Highway_82.svg")
    [82號高速公路](../Page/82號高速公路_\(奧克拉荷馬州\).md "wikilink")

### 毗鄰縣

所有馬斯科吉縣的毗鄰縣都是奧克拉荷馬州的縣份。

  - [特拉華縣](../Page/特拉華縣_\(奧克拉荷馬州\).md "wikilink")：北方
  - [亞代爾縣](../Page/亞代爾縣_\(奧克拉荷馬州\).md "wikilink")：東方
  - [塞闊雅縣](../Page/塞闊雅縣_\(奧克拉荷馬州\).md "wikilink")：南方
  - [馬斯科吉縣](../Page/馬斯科吉縣_\(奧克拉荷馬州\).md "wikilink")：西南方
  - [瓦戈納縣](../Page/瓦戈納縣_\(奧克拉荷馬州\).md "wikilink")：西方
  - [梅斯縣](../Page/梅斯縣_\(奧克拉荷馬州\).md "wikilink")：西北方

## 人口

[USA_Cherokee_County,_Oklahoma_age_pyramid.svg](https://zh.wikipedia.org/wiki/File:USA_Cherokee_County,_Oklahoma_age_pyramid.svg "fig:USA_Cherokee_County,_Oklahoma_age_pyramid.svg")

根據[2000年人口普查](../Page/2000年美國人口普查.md "wikilink")，切羅基縣擁有42,521居民、16,175住戶和11,079家庭。\[11\]其[人口密度為每平方英里](../Page/人口密度.md "wikilink")57居民（每平方公里23居民）。\[12\]本縣擁有19,499間房屋单位，其密度為每平方英里26間（每平方公里10間）。\[13\]而人口是由56.41%[白人](../Page/歐裔美國人.md "wikilink")、1.2%[黑人](../Page/非裔美國人.md "wikilink")、32.42%[土著](../Page/美國土著.md "wikilink")、0.27%[亞洲人](../Page/亞裔美國人.md "wikilink")、0.04%[太平洋岛民](../Page/太平洋岛民.md "wikilink")、2.1%其他[種族和](../Page/種族.md "wikilink")7.56%[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人佔了人口](../Page/拉丁美洲人.md "wikilink")4.14%。92.7%人口的母語為[英語](../Page/英語.md "wikilink")、3.8%為[西班牙語](../Page/西班牙語.md "wikilink")、以及2.7%為[切羅基語](../Page/切羅基語.md "wikilink")。\[14\]

在16,175住户中，有32.7%擁有一個或以上的兒童（18歲以下）、52.5%為夫妻、11.9%為單親家庭、31.5%為非家庭、25.3%為獨居、9%住戶有同居長者。平均每戶有2.52人，而平均每個家庭則有3.04人。在42,521居民中，有26.3%為18歲以下、14.6%為18至24歲、25.7%為25至44歲、21.5%為45至64歲以及12%為65歲以上。人口的年齡中位數為32歲，女子對男子的性別比為100：96.3。成年人的性別比則為100：92.1。\[15\]

本縣的住戶收入中位數為$26,536，而家庭收入中位數則為$32,369。男性的收入中位數為$25,993，而女性的收入中位數則為$21,048，[人均收入為](../Page/人均收入.md "wikilink")$13,436。約17%家庭和22.9%人口在[貧窮線以下](../Page/貧窮線.md "wikilink")，包括28.4%兒童（18歲以下）及13.8%長者（65歲以上）。\[16\]

## 參考文獻

## 外部連結

  - [Encyclopedia of Oklahoma History and Culture - Cherokee
    County](https://web.archive.org/web/20130727162732/http://digital.library.okstate.edu/encyclopedia/entries/C/CH017.html)
  - [Oklahoma Digital Maps: Digital Collections of Oklahoma and Indian
    Territory](https://web.archive.org/web/20121024013411/http://www.library.okstate.edu/okmaps/)

[C](../Category/俄克拉何马州行政区划.md "wikilink")

1.  [CensusViewer: Population of Cherokee County,
    Oklahoma](http://censusviewer.com/county/OK/Cherokee)

2.  [2010 census data](http://2010.census.gov/2010census/data/)

3.  Burnett, Amanda. *Encyclopedia of Oklahoma History and Culture*.
    "Cherokee County."

4.  *Columbia-Lippincott Gazeteer*. p. 386

5.
6.
7.
8.
9.

10.
11. [Population Profile of the United
    States: 2000](http://www.census.gov/population/www/pop-profile/profile2000.html)

12. [Statistical profile of Alger County,
    Michigan](http://censtats.census.gov/data/MI/05026003.pdf) , United
    States Census Bureau, Census 2000

13. [State and County QuickFacts](http://quickfacts.census.gov/qfd/)

14. [Census 2000 gateway](http://www.census.gov/main/www/cen2000.html)

15. [American FactFinder](http://factfinder.census.gov/)

16.