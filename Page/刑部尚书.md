**刑部尚书**，[中国古代](../Page/中国.md "wikilink")[官职](../Page/官职.md "wikilink")，主要负责[司法和](../Page/司法.md "wikilink")[牢狱](../Page/牢狱.md "wikilink")，雅稱為[大司寇](../Page/大司寇.md "wikilink")、**秋官卿**。

## 沿革

中国古代官署[刑部的主官](../Page/刑部.md "wikilink")。**刑部尚书**的官职最早出现于[隋](../Page/隋.md "wikilink")[五省六曹制](../Page/五省六曹制.md "wikilink")，後來的[明](../Page/明.md "wikilink")、[清两代沿袭此制](../Page/清.md "wikilink")。

[唐高宗](../Page/唐高宗.md "wikilink")[龍朔二年](../Page/龍朔.md "wikilink")（662年），改為司刑太常伯。[咸亨元年](../Page/咸亨.md "wikilink")（670年），復為刑部尚書。[光宅元年](../Page/光宅.md "wikilink")（684年），改為秋官尚書。[神龍元年](../Page/神龍.md "wikilink")（705年），復為刑部尚書。[天寶十一載](../Page/天宝_\(唐朝\).md "wikilink")（752年），改為憲部尚書。[至德二載](../Page/至德_\(唐朝\).md "wikilink")（757年），復為刑部尚書。\[1\]

在清时，由于统治者是来自[山海关外的](../Page/山海关.md "wikilink")[满族人](../Page/满族.md "wikilink")，所以为了维护满族的统治地位，清初各部主官均为[满人](../Page/满人.md "wikilink")。[顺治五年](../Page/顺治.md "wikilink")（1648年），[清世祖在](../Page/清世祖.md "wikilink")[六部改设两位主官](../Page/六部.md "wikilink")，满[汉各一](../Page/汉.md "wikilink")。刑部也由此首次迎来两位尚书，称“**刑部**[满尚书](../Page/满尚书.md "wikilink")”和“**刑部**[汉尚书](../Page/汉尚书.md "wikilink")”。名义上，二者在行使职权时不分级别高低，完全平等，然而由于满人在整个社会处于统治地位，所以部内权力基本上掌握在[满尚书手中](../Page/满尚书.md "wikilink")。

清[光绪三十二年](../Page/光绪.md "wikilink")（1906年），清政府宣布“[仿行宪政](../Page/仿行宪政.md "wikilink")”，改刑部为“法部”，“刑部尚书”之职正式从中国历史上消失。

## 刑部尚書名人

### 唐朝

  - 高祖

[萧造](../Page/萧造.md "wikilink") [皇甫无逸](../Page/皇甫无逸.md "wikilink")
[窦诞](../Page/窦诞.md "wikilink") [刘政会](../Page/刘政会.md "wikilink")
[沈叔安](../Page/沈叔安.md "wikilink") [屈突通](../Page/屈突通.md "wikilink")
[郑善果](../Page/郑善果.md "wikilink")

  - 太宗

[郑善果](../Page/郑善果.md "wikilink") [李靖](../Page/李靖.md "wikilink")
[韩仲良](../Page/韩仲良.md "wikilink") [李道宗](../Page/李道宗.md "wikilink")
[刘德威](../Page/刘德威.md "wikilink") [韦挺](../Page/韦挺.md "wikilink")
[张亮](../Page/张亮.md "wikilink")

  - 高宗

[张行成](../Page/张行成.md "wikilink") [唐临](../Page/唐临.md "wikilink")
[长孙祥](../Page/长孙祥.md "wikilink") [刘祥道](../Page/刘祥道.md "wikilink")
[源直心](../Page/源直心.md "wikilink") [李乾祐](../Page/李乾祐.md "wikilink")
[卢承庆](../Page/卢承庆.md "wikilink")

  - 武后

[裴居道](../Page/裴居道.md "wikilink") [李崇晦](../Page/李崇晦.md "wikilink")
[张楚金](../Page/张楚金.md "wikilink") [袁智弘](../Page/袁智弘.md "wikilink")
[娄师德](../Page/娄师德.md "wikilink") [豆卢钦望](../Page/豆卢钦望.md "wikilink")
[杜景俭](../Page/杜景俭.md "wikilink") [邓恽](../Page/邓恽.md "wikilink")
[李怀远](../Page/李怀远.md "wikilink") [韦安石](../Page/韦安石.md "wikilink")

  - 中宗

[韦安石](../Page/韦安石.md "wikilink") [祝钦明](../Page/祝钦明.md "wikilink")
[韦巨源](../Page/韦巨源.md "wikilink") [裴谈](../Page/裴谈.md "wikilink")

  - 睿宗

[岑羲](../Page/岑羲.md "wikilink") [杨元琰](../Page/杨元琰.md "wikilink")
[萧至忠](../Page/萧至忠.md "wikilink") [郭元振](../Page/郭元振.md "wikilink")

  - 玄宗

[郭元振](../Page/郭元振.md "wikilink") [李日知](../Page/李日知.md "wikilink")
[赵彦昭](../Page/赵彦昭.md "wikilink") [薛谦光](../Page/薛谦光.md "wikilink")
[李乂](../Page/李乂.md "wikilink") [宋璟](../Page/宋璟.md "wikilink")
[王志愔](../Page/王志愔.md "wikilink") [陆象先](../Page/陆象先.md "wikilink")
[韦抗](../Page/韦抗.md "wikilink") [卢从愿](../Page/卢从愿.md "wikilink")
[崔隐甫](../Page/崔隐甫.md "wikilink") [崔琳](../Page/崔琳.md "wikilink")
[李适之](../Page/李适之.md "wikilink") [裴敦复](../Page/裴敦复.md "wikilink")
[韦坚](../Page/韦坚.md "wikilink") [萧炅](../Page/萧炅.md "wikilink")
[张均](../Page/张均.md "wikilink") [苗晋卿](../Page/苗晋卿.md "wikilink")

  - 肃宗

[李巨](../Page/李巨.md "wikilink") [李麟](../Page/李麟.md "wikilink")
[颜真卿](../Page/颜真卿.md "wikilink") [李齐物](../Page/李齐物.md "wikilink")
[王玙](../Page/王玙.md "wikilink")

  - 代宗

[房琯](../Page/房琯.md "wikilink") [颜真卿](../Page/颜真卿.md "wikilink")
[路嗣恭](../Page/路嗣恭.md "wikilink") [蒋涣](../Page/蒋涣.md "wikilink")
[王昂](../Page/王昂.md "wikilink") [崔挹](../Page/崔挹.md "wikilink")
[徐坚](../Page/徐坚.md "wikilink")

  - 德宗

[关播](../Page/关播.md "wikilink") [李勉](../Page/李勉.md "wikilink")
[刘滋](../Page/刘滋.md "wikilink") [王锷](../Page/王锷.md "wikilink")

  - 顺宗

[高郢](../Page/高郢.md "wikilink")

  - 宪宗

[郑元](../Page/郑元.md "wikilink") [李鄘](../Page/李鄘.md "wikilink")
[赵宗儒](../Page/赵宗儒.md "wikilink") [张弘靖](../Page/张弘靖.md "wikilink")
[权德舆](../Page/权德舆.md "wikilink") [王播](../Page/王播.md "wikilink")
[李愿](../Page/李愿.md "wikilink")

  - 穆宗

[郭钊](../Page/郭钊.md "wikilink") [王播](../Page/王播.md "wikilink")
[李逊](../Page/李逊.md "wikilink") [崔植](../Page/崔植.md "wikilink")
[段文昌](../Page/段文昌.md "wikilink")

  - 敬宗

[段文昌](../Page/段文昌.md "wikilink")

  - 文宗

[柳公绰](../Page/柳公绰.md "wikilink") [崔弘礼](../Page/崔弘礼.md "wikilink")
[韦弘景](../Page/韦弘景.md "wikilink") [殷侑](../Page/殷侑.md "wikilink")
[高瑀](../Page/高瑀.md "wikilink") [郑覃](../Page/郑覃.md "wikilink")
[王源中](../Page/王源中.md "wikilink") [郑澣](../Page/郑澣.md "wikilink")
[崔珙](../Page/崔珙.md "wikilink")

  - 武宗

[崔珙](../Page/崔珙.md "wikilink") [杨汝士](../Page/杨汝士.md "wikilink")
[崔元式](../Page/崔元式.md "wikilink")

  - 宣宗

[崔元式](../Page/崔元式.md "wikilink") [白敏中](../Page/白敏中.md "wikilink")
[周墀](../Page/周墀.md "wikilink") [李玭](../Page/李玭.md "wikilink")
[韦损](../Page/韦损.md "wikilink") [崔璪](../Page/崔璪.md "wikilink")
[蒋系](../Page/蒋系.md "wikilink") [柳仲郢](../Page/柳仲郢.md "wikilink")

  - 懿宗

[夏侯孜](../Page/夏侯孜.md "wikilink") [蒋伸](../Page/蒋伸.md "wikilink")
[徐商](../Page/徐商.md "wikilink") [李福](../Page/李福.md "wikilink")
[杨收](../Page/杨收.md "wikilink") [路岩](../Page/路岩.md "wikilink")
[刘瞻](../Page/刘瞻.md "wikilink") [王铎](../Page/王铎.md "wikilink")

  - 僖宗

[刘瞻](../Page/刘瞻.md "wikilink") [郑从谠](../Page/郑从谠.md "wikilink")
[崔彦昭](../Page/崔彦昭.md "wikilink") [卢携](../Page/卢携.md "wikilink")
[牛蔚](../Page/牛蔚.md "wikilink") [李当](../Page/李当.md "wikilink")
[孔纬](../Page/孔纬.md "wikilink") [裴瓒](../Page/裴瓒.md "wikilink")

  - 昭宗

[郑延昌](../Page/郑延昌.md "wikilink") [崔凝](../Page/崔凝.md "wikilink")
[张袆](../Page/张袆.md "wikilink") [杨授](../Page/杨授.md "wikilink")
[裴贽](../Page/裴贽.md "wikilink") [长孙凝](../Page/长孙凝.md "wikilink")
[郑元规](../Page/郑元规.md "wikilink")

  - 昭宣帝

[张袆](../Page/张袆.md "wikilink") [裴迪](../Page/裴迪.md "wikilink")\[2\]

### 明朝

### 清朝

## 註釋

{{-}}

[刑部尚书](../Category/刑部尚书.md "wikilink")

1.  後周、北宋[王溥](../Page/王溥.md "wikilink")，《[唐会要](../Page/唐会要.md "wikilink")》[卷第59](../Page/:s:唐會要/卷059.md "wikilink")·尚書省諸司下
2.  [严耕望](../Page/严耕望.md "wikilink")《唐仆尚丞郎表》