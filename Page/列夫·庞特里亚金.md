**列夫·庞特里亚金**（，），[苏联](../Page/苏联.md "wikilink")[数学家](../Page/数学家.md "wikilink")。他生于[莫斯科](../Page/莫斯科.md "wikilink")，并在14岁时因为爆炸中失明。1924年進入[莫斯科國立大學](../Page/莫斯科國立大學.md "wikilink")，1928年畢業，1935獲得同校數學、物理博士學位。虽然双目失明，他在母亲塔季扬娜·安德烈耶夫娜（，）的帮助下成为数学家，是她为他阅读数学书籍。他在很多数学领域作出了巨大的贡献，包括[代数拓扑与](../Page/代数拓扑.md "wikilink")[微分拓扑](../Page/微分拓扑.md "wikilink")，1938年發表多項重要論文，獲得[列宁奖](../Page/列宁奖.md "wikilink")（1962）、[苏联国家奖](../Page/苏联国家奖.md "wikilink")（1975）等前蘇聯高等榮譽。

## 贡献

他在还是学生时就研究了[同调的](../Page/同调.md "wikilink")[对偶理论](../Page/对偶.md "wikilink")。他继续了这个研究，并为[傅立叶变换的抽象理论打下基础](../Page/傅立叶变换.md "wikilink")，现在该理论被称为[庞特里亚金对偶性](../Page/庞特里亚金对偶性.md "wikilink")。在拓扑学中，他提出了的基本问题。这导致在1940年左右引入了1个现在被称为的，当它位于一个作为[边界的](../Page/边界_\(拓扑学\).md "wikilink")[流形上时会消失为](../Page/流形.md "wikilink")0。

在其学术生涯后期，他研究了[优化控制理论](../Page/优化控制.md "wikilink")。他的[最小化原理对于该课题的现代理论是基础性的](../Page/庞特里亚金最小化原理.md "wikilink")。他也在该领域引入了[起停式控制的思想](../Page/起停式控制.md "wikilink")，用于描述一些需在最大驅動力或是無驅動力二個條件下選擇的系统。

## 外部链接

  -
  -
  -
[Category:社會主義勞動英雄](../Category/社會主義勞動英雄.md "wikilink")
[Category:列寧勳章獲得者](../Category/列寧勳章獲得者.md "wikilink")
[Category:十月革命勳章獲得者](../Category/十月革命勳章獲得者.md "wikilink")
[Category:勞動紅旗勳章獲得者](../Category/勞動紅旗勳章獲得者.md "wikilink")
[Category:榮譽徽章勳章獲得者](../Category/榮譽徽章勳章獲得者.md "wikilink")
[Category:列寧百年誕辰紀念勳章獲得者](../Category/列寧百年誕辰紀念勳章獲得者.md "wikilink")
[Category:莫斯科建城800週年紀念勳章獲得者](../Category/莫斯科建城800週年紀念勳章獲得者.md "wikilink")
[Category:蘇聯國家獎獲得者](../Category/蘇聯國家獎獲得者.md "wikilink")
[Category:列寧獎獲得者](../Category/列寧獎獲得者.md "wikilink")
[Category:斯大林獎獲得者](../Category/斯大林獎獲得者.md "wikilink")
[Category:蘇聯科學院院士](../Category/蘇聯科學院院士.md "wikilink")
[Category:苏联数学家](../Category/苏联数学家.md "wikilink")
[Category:莫斯科国立大学校友](../Category/莫斯科国立大学校友.md "wikilink")
[Category:莫斯科人](../Category/莫斯科人.md "wikilink")
[Category:俄罗斯盲人](../Category/俄罗斯盲人.md "wikilink")
[Category:安葬於新聖女公墓者](../Category/安葬於新聖女公墓者.md "wikilink")