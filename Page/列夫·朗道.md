**列夫·达维多维奇·朗道**（，，），前[蘇聯知名](../Page/蘇聯.md "wikilink")[物理學家](../Page/物理學家.md "wikilink")，[凝聚态物理学的奠基人](../Page/凝聚态物理学.md "wikilink")，苏联科学领军人之一\[1\]，在[理論物理裡多個領域都有重大貢獻](../Page/理論物理.md "wikilink")。他标新立异，爱打破传统，并以精炼的著作风格和高傲的性格而为人熟知。他由於「關於凝聚態物質的開創性理論，特別是[液氦](../Page/液氦.md "wikilink")」獲得1962年的[諾貝爾物理學獎](../Page/諾貝爾物理學獎.md "wikilink")\[2\]。1962年，仍活跃于研究前沿的朗道发生严重车祸，智力和记忆力均受损，身体状况大不如前，6年后去世。

朗道去世多年后，[阿列克谢·阿布里科索夫及](../Page/阿列克谢·阿布里科索夫.md "wikilink")[维塔利·金兹堡凭借早年与朗道共同建立的](../Page/维塔利·金兹堡.md "wikilink")[超导体和](../Page/超导体.md "wikilink")[超流体理论也于](../Page/超流体.md "wikilink")2003年获得诺贝尔物理学奖。

## 生平

[Landau1910.jpg](https://zh.wikipedia.org/wiki/File:Landau1910.jpg "fig:Landau1910.jpg")

### 早年

1908年1月22日，列夫·朗道出生于俄国[里海边上的石油城](../Page/里海.md "wikilink")[巴库](../Page/巴库.md "wikilink")（今[亞塞拜然共和国首都](../Page/亞塞拜然共和国.md "wikilink")）。“列夫”（[Лев](../Page/wikt:Лев.md "wikilink")）这个名字在[俄语里是](../Page/俄语.md "wikilink")“雄狮”的意思。他的父亲达维特·勒沃维奇·朗道（Давид
Львович
Ландау\[3\]）是[犹太人](../Page/犹太人.md "wikilink")，為一名當地油田上的工程师；母親柳波芙·维尼娅米诺夫娃·嘎尔卡维-朗道（Любовь
Вениаминовна
Гаркави-Ландау\[4\]）則是一名醫師。朗道小时候身体瘦弱，性格高傲倔强，而且表现出很高的[數學天赋](../Page/數學.md "wikilink")。13岁时，朗道中学毕业。父母认为他年龄还不够上大学，便让他入读[巴库经济专科学校](../Page/巴库经济专科学校.md "wikilink")。

1922年，他年满14岁，被[巴庫國立大學录取](../Page/巴庫國立大學.md "wikilink")，成为该校年龄最小的学生。他同時攻讀兩個科系：數學和[物理學系和](../Page/物理.md "wikilink")[化學系](../Page/化學.md "wikilink")。雖然後來不再繼續學習化學，但仍一直維持對這門學科的興趣。1924年，朗道转至[列寧格勒國立大學](../Page/列寧格勒國立大學.md "wikilink")，在這兒他第一次認識到真正的[理論物理](../Page/理論物理.md "wikilink")，並且決定全心投入這方面的研究。朗道在列宁格勒大学时常与和[乔治·伽莫夫交流物理前沿的学习心得](../Page/乔治·伽莫夫.md "wikilink")，这三人被称为该校的新锐少年“[三剑客](../Page/三剑客.md "wikilink")”\[5\]。（有的说法把也算进去了。）1927年從该校畢業後，他進入[列寧格勒物理技術學院](../Page/列寧格勒物理技術學院.md "wikilink")，並在1929年（21歲）取得[副博士學位](../Page/副博士學位.md "wikilink")。同年，在蘇聯政府和[洛克菲勒基金會的資助下](../Page/洛克菲勒基金會.md "wikilink")，朗道得以遊歷歐洲作研究。

### 游学欧洲

朗道此次游学欧洲收获丰富。在[哥廷根](../Page/哥廷根.md "wikilink")（有[马克斯·玻恩在](../Page/马克斯·玻恩.md "wikilink")）和[莱比锡](../Page/莱比锡.md "wikilink")（有[维尔纳·海森堡在](../Page/维尔纳·海森堡.md "wikilink")）短暂地停留之后，朗道来到[哥本哈根](../Page/哥本哈根.md "wikilink")[玻尔研究所](../Page/玻尔研究所.md "wikilink")，跟从著名物理学家[尼爾斯·玻爾等人研究](../Page/尼爾斯·玻爾.md "wikilink")[量子力學](../Page/量子力學.md "wikilink")。在那里，朗道参加了玻尔主持的理论物理讨论班，初步展露出才华。22岁的朗道提出了金属电子的[抗磁性理论](../Page/抗磁性.md "wikilink")。爱因斯坦有一次作演讲时被年轻的朗道敏锐地指出了一处推理漏洞，转而对着黑板思索后说：“后面那位年轻人说得完全正确，诸位可以把我今天讲的完全忘掉\[6\]。”玻尔在物理方面的直觉也令朗道佩服不已，后来朗道时常提到自己是玻尔的学生，虽然这次他只在玻尔研究所工作了4个月（他在1933年和1934年又来到哥本哈根）。之后，朗道还访问了[劍橋和](../Page/劍橋.md "wikilink")[蘇黎世](../Page/蘇黎世.md "wikilink")（有[沃尔夫冈·泡利在](../Page/沃尔夫冈·泡利.md "wikilink")）。瑞士苏黎世读博士后的同学皮尔斯（Rudolph
Reierls）回忆说：“我还清晰记得朗道1929年在苏黎世出现在泡利的系里时，给我们留下的深刻印象。
...没过多久就能发现他对现代物理学的深刻认识和他解决基础问题的技巧。他很少详细阅读理论物理学的论文，只是大概看看问题是否有趣，如果有趣，作者的方法是什么。然后他开始自己计算，如果答案和作者的一致，他就赞同这篇文章。\[7\]”他在劍橋時，參訪了[歐尼斯特·拉塞福主持的](../Page/歐尼斯特·拉塞福.md "wikilink")[卡文迪许实验室](../Page/卡文迪许实验室.md "wikilink")，并在那里结识了另一位苏联物理学家[彼得·卡皮查](../Page/彼得·卡皮查.md "wikilink")。

### 回国

1931年，朗道回到蘇聯。1932年-1937年，他担任的理论部主任\[8\]，在此遇上了得意门生[叶夫根尼·利夫希茨](../Page/叶夫根尼·利夫希茨.md "wikilink")（又译为“栗弗席兹”）\[9\]，并一起开始筹划编写名著《[理论物理学教程](../Page/理论物理学教程.md "wikilink")》\[10\]。“朗道学派”（Landau
school）在此期间开始萌芽\[11\]，哈爾科夫理工學院成为苏联物理学的研究中心\[12\]。1934年，他未经答辩便直接获得物理与数学科学[全博士学位](../Page/全博士.md "wikilink")\[13\]。1937年，朗道前往[莫斯科](../Page/莫斯科.md "wikilink")，在卡皮查的[物理问题研究所担任理论部主任](../Page/物理问题研究所.md "wikilink")。

### 入狱及中后期研究

在[大清洗期間](../Page/大清洗.md "wikilink")，朗道受到官方調查。1938年4月28日，朗道和他的两位友人因「煽动[反革命罪](../Page/反革命.md "wikilink")」被捕，囚禁在[內務人民委員部的監獄](../Page/內務人民委員部.md "wikilink")。為了把朗道保释出来，卡皮查私底下寫信給[史達林擔保朗道的行事作為](../Page/史達林.md "wikilink")，并亲赴莫斯科[克里姆林宫为朗道说情](../Page/克里姆林宫.md "wikilink")\[14\]。卡皮察担保全苏联只有朗道才能解决自己一年前在实验中所发现的[液氦](../Page/液氦.md "wikilink")[超流态的物理机制问题](../Page/超流体.md "wikilink")\[15\]，从而为苏联在国际上争光\[16\]。高层人物[贝利亚相信了卡皮察关于朗道的能力对国家有利用价值的说法](../Page/拉夫连季·帕夫洛维奇·贝利亚.md "wikilink")\[17\]，并为释放朗道打通关系。在卡皮查等人的努力下，朗道一年后获释。朗道不负众望，成功建立了液氦超流性的理论\[18\]。

1946年，仍有政治问题嫌疑的朗道在贝利亚的许可下当上了[苏联科学院院士](../Page/苏联科学院.md "wikilink")\[19\]。
朗道曾经参与[苏联的核武器研制计划](../Page/苏联原子弹计划.md "wikilink")，在其中进行数值计算方面的工作，并因此2次获得[史達林獎](../Page/史達林獎.md "wikilink")，还在1954年被授予「社会主义劳动英雄」称号。

1956年，他见到了小自己一辈的百科全才[默里·盖尔曼](../Page/默里·盖尔曼.md "wikilink")\[20\]。盖尔曼和朗道一样都是看不起应用研究的理论物理名家。

### 车祸与晚年

1962年1月7日，朗道出了一次严重的[车祸](../Page/车祸.md "wikilink")，震动了整个物理学界。众多苏联物理学家聚集到朗道的病房，在医院的长廊点上烛光为他祈祷。玻尔亲自安排了一流的医生前往莫斯科。著名[神经外科专家](../Page/神经外科.md "wikilink")[怀尔德·潘菲尔德都有参与救治朗道](../Page/怀尔德·潘菲尔德.md "wikilink")。车祸严重损害了朗道的身体健康。在昏迷了大约两个月后，朗道醒来，但智力已经发生了严重的退化。这年年底，[诺贝尔物理学奖授予朗道](../Page/诺贝尔物理学奖.md "wikilink")，表彰他在液態[氦的](../Page/氦.md "wikilink")[超流體理论方面作出的贡献](../Page/超流體.md "wikilink")\[21\]。由于健康原因，奖项破例由[瑞典驻苏联大使在莫斯科代为颁发](../Page/瑞典.md "wikilink")\[22\]\[23\]。

1968年4月1日，朗道因於車禍後健康逐漸惡化去世，享年60歲。死後其遺體下葬在[莫斯科](../Page/莫斯科.md "wikilink")[新處女公墓](../Page/新處女公墓.md "wikilink")\[24\]。

## 學術研究

### 朗道十诫

朗道是[凝聚态物理学的主要开拓者](../Page/凝聚态物理学.md "wikilink")。该学科在朗道的推动下快速发展起来，现在已经形成一个热门且应用价值广泛的物理学主要分支，涵盖[固体物理](../Page/固体物理.md "wikilink")、金属物理、粘稠流体、[超导物理](../Page/超导.md "wikilink")、物态及[相变](../Page/相变.md "wikilink")、[介观物理学](../Page/介观物理学.md "wikilink")、[巨磁阻效应等多个大的子研究领域](../Page/巨磁阻效应.md "wikilink")，其中有数项重要观点和概念都是朗道第一个提出的。1958年，为了庆祝朗道的50岁寿辰，苏联原子能研究所赠送给他两块青石板，上面仿照《[圣经](../Page/圣经.md "wikilink")》中的“[摩西十诫](../Page/摩西十诫.md "wikilink")”，刻着朗道在物理学中作出的最重要的十项贡献，被称为“朗道十诫”\[25\]：

1.  [量子力学中的](../Page/量子力学.md "wikilink")[密度矩阵和](../Page/密度矩阵.md "wikilink")[统计物理学](../Page/统计物理学.md "wikilink")（1927年）；
2.  自由电子的抗磁性量子理论（1930年）；
3.  二级[相变的研究](../Page/相变.md "wikilink")（1936～1937年）；
4.  [铁磁性的](../Page/铁磁性.md "wikilink")[磁畴理论和](../Page/磁畴.md "wikilink")[反铁磁性的理论解释](../Page/反铁磁性.md "wikilink")（1935年）；
5.  [超导体的混合态理论](../Page/超导体.md "wikilink")（1934年）；
6.  [原子核的几率理论](../Page/原子核.md "wikilink")（1937年）；
7.  氦Ⅱ[超流性的量子理论](../Page/超流性.md "wikilink")（1940～1941年）；
8.  [基本粒子的电荷约束理论](../Page/基本粒子.md "wikilink")（1954年）；
9.  [费米液体的量子理论](../Page/费米液体.md "wikilink")（1956年）；
10. [弱相互作用的](../Page/弱相互作用.md "wikilink")[CP不变性](../Page/CP对称.md "wikilink")（1957年）。

朗道的数学功底非常扎实，喜欢用简单而深刻的物理模型说明问题。这种风格受到了[玻尔的影响](../Page/尼尔斯·玻尔.md "wikilink")，也深深影响了后世的苏联物理学家。

### 物态相变研究

### 固体物理

在[固体物理学方面](../Page/固体物理学.md "wikilink")，朗道提出了著名的[元激发](../Page/元激发.md "wikilink")(elementary
excitation)，引入了[声子的概念](../Page/声子.md "wikilink")。

### 超导领域

在超导领域，朗道将自己提出的[二级相变观点推广到超导现象的量子机制研究中](../Page/二级相变.md "wikilink")，得到了[金兹堡-朗道方程](../Page/金兹堡-朗道方程.md "wikilink")\[26\]。金兹堡-朗道方程是对[伦敦方程的进一步发展](../Page/伦敦方程.md "wikilink")，也为后来更著名的超导[BCS理论理论做好了铺垫](../Page/BCS理论.md "wikilink")\[27\]\[28\]。金兹堡-朗道理论还可以用于超流体、非线性波动、模式形成（pattern
formation）、[液晶现象](../Page/液晶.md "wikilink")、[超对称](../Page/超对称.md "wikilink")[共形场论等一大批物理课题](../Page/共形场论.md "wikilink")\[29\]，其核心思想还在有关[希格斯粒子的热门研究中获得广泛应用](../Page/希格斯粒子.md "wikilink")\[30\]。

朗道与金兹堡的超导理论也是[唯象论的代表成就](../Page/粒子物理现象学.md "wikilink")。

### 粒子物理学

### 核武器

朗道从事应用方面的研究时也喜欢玩高观点、搞新技术\[31\]。应用研究很少见于朗道的论文集\[32\]，参与核武器研发就是他研究生涯中少有的被迫从事应用类研究的例子。朗道在[雅可夫·泽尔多维奇的带领下负责数值计算工作](../Page/雅可夫·泽尔多维奇.md "wikilink")，但朗道并不像美国对手们一样使用计算机解决计算计算量很大的难题，这说明他发明了更厉害的新计算方法以绕过传统方法的计算困难\[33\]。他此期间的工作后来编成了1958年出版的讲义《一种基于网格法的偏微分方程数值方法》（*Numerical
Methods of an Integration of Partial Equations by a Method of
Grids*）\[34\]。

他还曾与其它几位苏联专家一起论证了从美国对手[爱德华·泰勒那边秘密搞过来的一种设计方案的不可行性](../Page/爱德华·泰勒.md "wikilink")。等到[约瑟夫·斯大林一去世](../Page/约瑟夫·斯大林.md "wikilink")，朗道就设法找机会脱离了苏联的核计划\[35\]。

### 宇宙学

在宇宙学方面，朗道早在1931年春天同[尼尔斯·玻尔](../Page/尼尔斯·玻尔.md "wikilink")、[莱昂·罗森菲尔德等人讨论时](../Page/莱昂·罗森菲尔德.md "wikilink")，就猜想宇宙中会存在特别致密的天体，但其论文到1932年才发表\[36\]。1932年，中子被发现后，朗道随即大胆地提出了“[中子星](../Page/中子星.md "wikilink")”一词\[37\]（更确切地说，朗道称其为“中子核”\[38\]）。朗道在只有4页纸的简短论文中，独立推导出了著名的[钱德拉塞卡极限](../Page/钱德拉塞卡极限.md "wikilink")\[39\]。朗道发表的致密星体研究和命名中子星的举动为当时因巨大质量星体演化理论不被认可而遭到孤立的印度青年天体物理学家[苏布拉马尼扬·钱德拉塞卡提供了支持](../Page/苏布拉马尼扬·钱德拉塞卡.md "wikilink")。朗道还曾半开玩笑地提到过，也许世界上还会出现核心密度大到“连常规量子力学定律都不再适用”（"the
laws of ordinary quantum mechanics break down..."）的奇怪天体\[40\]。

美国物理学家[罗伯特·奥本海默当时也关注了朗道的论文](../Page/罗伯特·奥本海默.md "wikilink")，并从中获得灵感\[41\]。奥本海默后来算出了同样知名的[奥本海默极限](../Page/欧本海默极限.md "wikilink")。

## 教育工作

朗道对学生的要求极其严格，他的学生要做大量的习题，毕业之前还要通过朗道难度极大的考试。他和学生[叶夫根尼·利夫希茨编写的](../Page/叶夫根尼·利夫希茨.md "wikilink")《[理论物理学教程](../Page/理论物理学教程.md "wikilink")》深度和难度都很大，被奉为20世纪物理学的经典著作\[42\]。按照利夫希茨的最后筹划，整套书俄文版总篇幅超过4600页\[43\]。朗道的学生在进行科研工作之前都要通读此书，并通过基于这些教材的困难考试。朗道称通过其考试只是“理论最低要求”（theoretical
minimum），学生则用双关语戏称其为「朗道[势垒](../Page/势垒.md "wikilink")」\[44\]\[45\]。朗道在考试时会离开教室，然后每隔20分钟巡视一次，遇到解題思路不对的学生会在其身旁发出声响作为暗示。从1934年到朗道出车祸前的1961年，在这近30年的时间内仅有43人成功通过考试\[46\]\[47\]。这43人中至少有18人后来成为苏联或加盟共和国的科学院院士或通讯院士，有1位获得[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")\[48\]，还有一部分人因之前备考压力过大而在通过考试后丧失了对物理的兴趣\[49\]。除此之外，朗道还要求学生务必熟练掌握英文和德文。

朗道是苏联理论物理学界“朗道学派”的领袖\[50\]\[51\]\[52\]，与他好友[伊戈尔·塔姆带领的](../Page/伊戈尔·叶夫根耶维奇·塔姆.md "wikilink")“塔姆学派”及[尼古拉·博戈柳博夫的学派都是苏联理论物理研究的主力军](../Page/尼古拉·博戈柳博夫.md "wikilink")\[53\]。朗道和塔姆在学风和对核计划的态度上完全不同，但是在生活里走得很近，以至于苏联中央委员会认为“塔姆-朗道”集团是串通好了要一起将党的影响力排斥在科学研究之外\[54\]。

苏联早期的物理学界可以粗略分为2类派系：发展西方新物理学（相对论和量子论）的进步派和不支持（甚至是抵制）新物理学的保守派。矛盾的产生是由于[辩证唯物主义已经成为苏联的意识形态基础](../Page/辩证唯物主义.md "wikilink")，但[量子论和](../Page/量子论.md "wikilink")[相对论中的许多概念经常被一些不了解的人误指为是一种](../Page/相对论.md "wikilink")[唯心主义](../Page/唯心主义.md "wikilink")。进步派在人数比例上占优，但是经常需要应对来自政府官员的质疑。朗道、伊戈尔·塔姆，以及更早一辈的（常简称为“约飞”）、、[德米特里·罗日杰斯特文斯基等人都属于进步的一派](../Page/德米特里·谢尔盖耶维奇·罗日杰斯特文斯基.md "wikilink")。\[55\]

## 政治倾向

关于入狱一事，据后来解密的[克格勃文件显示](../Page/克格勃.md "wikilink")，朗道经查实确实有秘密加入反政府团体\[56\]，以及在反政府游行的传单上签名，并准备散布出去\[57\]。这可能是由于朗道对于斯大林政府的所作所为感到失望和不满\[58\]。（朗道有不少同事都在先前的政治运动中遭受冲击，甚至被处死。例如以及[伊戈尔·塔姆的](../Page/伊戈尔·塔姆.md "wikilink")1个弟弟\[59\]都没能保住性命，朗道的好友也于1938年锒铛入狱\[60\]。）不过由于好友[彼得·卡皮查全力担保朗道是](../Page/彼得·卡皮查.md "wikilink")“因平时不积口德、不会处理人际关系而被人诬陷”，以及朗道本人确实拥有出众的科研实力，[斯大林最后宽容了他](../Page/约瑟夫·斯大林.md "wikilink")。朗道出狱后迅速建立了描述[液氦](../Page/液氦.md "wikilink")[超流态的理论](../Page/超流态.md "wikilink")\[61\]，为苏联在国际上争了光，并在此后极少提及自己在监狱的经历。苏联的[克格勃机关则没有放松对他的监视](../Page/克格勃.md "wikilink")，指控也没有撤销掉\[62\]。也有说法支持朗道是清白无辜的\[63\]。

朗道是理想主义者，倾向于共产主义\[64\]，自认为是[世界公民](../Page/世界公民.md "wikilink")\[65\]，也是爱国者\[66\]，追求平稳缓和的政治演变，但是不喜欢专制极权，也不想看到社会出现混乱和动荡，对民族主义有着警惕的心，认为苏联出于政治需要的过度爱国主义宣传“妨碍了我国的科研工作”\[67\]。他认为苏联的制度问题起源于[列宁所犯的错误](../Page/列宁.md "wikilink")，而且觉得苏联的这条路走不长久\[68\]。

1930年，朗道的父亲曾因从事破坏活动被捕。\[69\]

## 个人生活

### 性格

朗道性格高傲，脾气暴躁，经常出言不逊，批评其同事和老一辈物理学家，辱骂学生，这使他一生树敌众多\[70\]。朗道有一定的精神洁癖，看待问题容易绝对化\[71\]。据说自从和大学时代的好友\[72\]因政治方面的原因而决裂后，他就把对方说得一无是处，而且再也没把和对方合作过的作品收录于自己的选辑中\[73\]。只是在朗道面前提起伊万连科的名字都会导致和朗道处不好关系\[74\]。有人曾在他的办公室门上写“当心！此君咬人！”（Beware\!
He
bites\!）\[75\]。他在列宁格勒物理研究所因为得罪了老师而离开，后来又在哈尔科夫研究所因为得罪院长而离开\[76\]。其好友说朗道在成名后，锋芒有所收敛\[77\]。

有很多涉及朗道傲慢自大一面的传闻在其同事们和徒子徒孙中口口相传，其中不可避免地会有一定程度的扭曲、杜撰的情况存在。这些传闻有不少已经收录进入正式的文献资料，所以有一部分历史真相变得难以考证了。\[78\]

朗道与另一位苏联理论物理大师[雅可夫·泽尔多维奇本来也是好朋友](../Page/雅可夫·泽尔多维奇.md "wikilink")。朗道在核武器研究期间就是在泽尔多维奇带领的小组工作。泽尔多维奇视朗道为老师，朗道则推荐泽尔多维奇入选[科学院](../Page/俄罗斯科学院.md "wikilink")（要成为苏联的院士先要有通讯院士的资格）。不过从50年代开始，朗道对泽尔多维奇不再友好。因为泽尔多维奇总是想拉朗道入伙去从事机密的工作，但是朗道很不情愿。\[79\]

据物理学者[管惟炎回忆](../Page/管惟炎.md "wikilink")，朗道是一个不怎么注意个人外在形象的人\[80\]。朗道爱标新立异，常穿一身搭配十分奇怪的装束上班，这多半是为了出风头\[81\]。

### 婚姻

1937年，朗道同孔克尔迪亚·德罗班特塞娃(，，1908–1984)结婚。其独生子伊戈尔(，，1946–2011\[82\])后来成为[实验物理工作者](../Page/实验物理.md "wikilink")。\[83\]

### 朗道的排名

朗道爱以[对数尺度给各种事物](../Page/对数尺度.md "wikilink")（包括物理学家、科学文章和女性等等）作高低排名\[84\]。

朗道作了一張[物理學家名字的列表](../Page/物理學家.md "wikilink")，上頭以數值 0 到 5
作了分等级的排名。排在最前面的是[艾薩克·牛頓](../Page/艾薩克·牛頓.md "wikilink")，数值为0，其后是[愛因斯坦](../Page/阿尔伯特·爱因斯坦.md "wikilink")，為
0.5 。數值 1
則獻給了[量子力學的奠基者](../Page/量子力學.md "wikilink")（[尼爾斯·玻爾](../Page/尼爾斯·玻爾.md "wikilink")、[維爾納·海森堡](../Page/維爾納·海森堡.md "wikilink")、[保羅·狄拉克](../Page/保羅·狄拉克.md "wikilink")、[埃爾溫·薛定谔](../Page/埃爾溫·薛定谔.md "wikilink")）以及与他同时代的天才[尤金·維格納等](../Page/尤金·維格納.md "wikilink")。朗道将自己列为2.5级，後來又将自己提升到2级。\[85\]\[86\]\[87\]\[88\]

### 逸闻

朗道喜欢让自己的学生和关系亲近的人称呼他为“道”(，）而非“朗道”\[89\]。据说这个绰号是给他起的\[90\]。

朗道曾在课堂黑板上方悬有一幅油画《牧人吹笛羊群吃草》\[91\]。朗道自认为是画中的牧人，而学生是羊，上课如同吹着笛子催羊吃草\[92\]。英文版的《理论物理学教程》还附有一幅流传广泛的“对驴讲经图”，暗示他的学生普遍被他轻视，给学生上课在他看来有几分对牛弹琴的意味。\[93\]

记得朗道于1929年时，曾失望地对他说：“正如所有的漂亮妹子不是已经和人家拍拖就是已经被人家娶走，所有的好问题也都被解决了……我都怀疑自己还能不能在剩下的东西里面发现任何有价值的课题。”然而就在第2年，朗道还是撞到了1个机会。\[94\]\[95\]

传说有一次[尼尔斯·玻尔发](../Page/尼尔斯·玻尔.md "wikilink")[电报问他如何评价](../Page/电报.md "wikilink")[保罗·狄拉克提出的](../Page/保罗·狄拉克.md "wikilink")[正电子](../Page/正电子.md "wikilink")（也有说法是说“粒子空穴”\[96\]\[97\]）这个新点子，他毫不客气地只回覆了1个词——“垃圾”()\[98\]。不过由于当时的物理学观点争论很频繁，他这种说话口气还算不上得罪人\[99\]。不过正电子不久后真的被[赵忠尧和](../Page/赵忠尧.md "wikilink")[卡尔·安德森先后发现了](../Page/卡尔·戴维·安德森.md "wikilink")\[100\]。

有一次玻尔在苏联一所中学里对学生们讲话时说“我是不会不好意思说自己笨的”，朗道却的学生[叶夫根尼·利夫希茨将其翻译成了](../Page/叶夫根尼·利夫希茨.md "wikilink")“我是不会不好意思说你们笨的”\[101\]。现场能听懂玻尔原话的人则哈哈大笑\[102\]。

朗道有个比方，说人有3类，分别用三角形、倒三角形和菱形来代表。三角形([拉普拉斯算子](../Page/拉普拉斯算子.md "wikilink"))表示基础广博，头脑敏锐，玻尔当然是这一类；倒三角形([竖琴算子](../Page/nabla算子.md "wikilink"))表示没有基础，又头脑迟钝，这种人当然无缘作物理学家；朗道谦称自己是菱形，基础不够广博，头脑尚称敏锐\[103\]。也有说法补充说还有一种形状是正立的正方形([达朗贝尔算子](../Page/达朗贝尔算子.md "wikilink"))，代表头脑笨但坐得住者\[104\]。

朗道厌恶罗列因素、不突重点的物理理论，明确区分“技术问题”和“理论物理”，把只谈“技术”的人从讨论班的讲台上撵下去\[105\]。他轻视头脑笨的同行，晚年却因车祸受伤失去了过人的智力，最后又是在西方的[愚人节去世](../Page/愚人节.md "wikilink")。

与多数苏联人不同，朗道对酒不感兴趣。\[106\]\[107\]

朗道不喜欢语文，这导致他的著作大多都是同其他人合作完成的。\[108\]

朗道年轻时曾当面大胆地指出[爱因斯坦演讲中的错误](../Page/阿尔伯特·爱因斯坦.md "wikilink")\[109\]，但他也没能说服爱因斯坦相信[量子力学的正确性](../Page/量子力学.md "wikilink")\[110\]。另有传闻说朗道曾在演讲时遇到同样很爱挑刺的前辈[沃尔夫冈·泡利](../Page/沃尔夫冈·泡利.md "wikilink")，并向对方示弱，但此事可能只是杜撰的\[111\]。

中国[量子场论学者](../Page/量子场论.md "wikilink")[段一士](../Page/段一士.md "wikilink")1956年在莫斯科大学就读期间，曾给朗道做助教。朗道夸段一士是“最聪明的中国青年”。有一次朗道和[弗拉基米尔·福克在争论广义相对论的一个问题](../Page/弗拉基米尔·福克.md "wikilink")，朗道问段一士说：“你站在哪一边？”段只回答：“我站在真理一边！”\[112\]

## 评价

朗道数学基本功和对物理的直觉都很好。但他也因此自负且偏执\[113\]。朗道的一大失误是在“θ-τ之谜”研究热潮期间，轻率地丢弃了由本国人沙皮罗(I.
S.
Shapiro)写的尝试说明[宇称不守恒的重要论文](../Page/宇称不守恒.md "wikilink")，导致同样提出此理论的[杨振宁](../Page/杨振宁.md "wikilink")、[李政道和](../Page/李政道.md "wikilink")[吴健雄等人组成的小组抢到了诺贝尔奖](../Page/吴健雄.md "wikilink")。\[114\]\[115\]\[116\]不过朗道不久后又提出了“CP守恒”的崭新观点以挽回面子，这引出了后来[CP并不守恒的新发现以及终极补救方案](../Page/CP不守恒.md "wikilink")[CPT对称的提出](../Page/CPT对称.md "wikilink")。

朗道虽然对自己的理论水平颇为满意，但是也同时表示自己对于从事实验研究并不拿手。朗道说自己是“最后一个全能物理学家”\[117\]。但更确切地说，他是一个“全能理论物理学家”\[118\]。

[尼尔斯·玻尔曾评价青年朗道](../Page/尼尔斯·玻尔.md "wikilink")：“他一来就给了我们深刻的印象。他对物理课题的洞察力，以及对人类生活的强烈见解，使许多次讨论会的水平上升了。”\[119\]

苏联原子弹计划领导者[伊戈尔·库尔恰托夫院士写信请求苏联中央政府让朗道加入原子弹计划时](../Page/伊格爾·瓦西里耶維奇·庫爾恰托夫.md "wikilink")，敬称朗道是“我国最伟大的理论物理学家”\[120\]。朗道的好友[彼得·卡皮查说](../Page/彼得·卡皮查.md "wikilink")：“朗道在整个理论物理学领域都做了工作，所有这些工作都可以用一个词来描述——卓越\[121\]。”卡皮查还说朗道是“简单化作风和民主作风、无限偏执和过分自信的奇妙混合体\[122\]”。

苏联氢弹计划领导者[安德烈·萨哈罗夫曾在](../Page/安德烈·萨哈罗夫.md "wikilink")1947年说自己对于从事纯理论研究很不拿手，并说自己在这方面连朗道的徒弟[伊萨克·波梅兰丘克都比不上](../Page/伊萨克·波梅兰丘克.md "wikilink")，并用公式写下3人的理论研究功底排名（用姓氏英文拼写的首字母代表每个人）：L
\> P \>
S\[123\]。1953年，当朗道听说萨哈罗夫入选苏联科学院时，直接表示萨哈罗夫不是理论学者，而只是个学物理的发明家\[124\]。

美国广义相对论学者[基普·索恩](../Page/基普·索恩.md "wikilink")：“在20年代，西欧是理论物理学的圣地，几乎是每一个知名的理论物理学家的故乡。苏联的领导者们为了显示他们的恩赐，要把西方的理论物理灌输到苏联，没有别的选择，只好将他们年轻的理论家送到那儿去培养，也顾不上[精神污染的危险了](../Page/精神污染.md "wikilink")。在经历过到列宁格勒，然后去西欧，然后回苏联的年轻苏维埃理论家中，朗道在物理学界是最有影响的。”\[125\]

苏联领袖数学家[弗拉基米尔·阿诺尔德](../Page/弗拉基米尔·阿诺尔德.md "wikilink")：“一个数学教师，如果至今还没有掌握至少几卷朗道和[利夫希茨著的](../Page/叶夫根尼·利夫希茨.md "wikilink")[物理学教程](../Page/理论物理学教程.md "wikilink")，他（她）必将成为一个数学界的希罕的残存者，就好似如今一个仍不知道[开集与](../Page/开集.md "wikilink")[闭集差别的人](../Page/闭集.md "wikilink")。”\[126\]

## 荣誉

1962年，朗道受重伤醒来后，被授予诺贝尔物理学奖。颁奖委员会的人担心他身体撑不住长途旅行，特别破例为他在[莫斯科举行了颁奖仪式](../Page/莫斯科.md "wikilink")。

## 紀念

[Stamp_of_Azerbaijan_834.jpg](https://zh.wikipedia.org/wiki/File:Stamp_of_Azerbaijan_834.jpg "fig:Stamp_of_Azerbaijan_834.jpg")发行的纪念朗道诞辰100年的邮票。朗道的出生地就是“火之国”阿塞拜疆的首都[巴库](../Page/巴库.md "wikilink")。\]\]
1965年，朗道之前的學生與同事成立了[朗道理論物理研究所](../Page/朗道理論物理研究所.md "wikilink")，地點位於靠近莫斯科的[切爾諾戈洛夫卡](../Page/切爾諾戈洛夫卡.md "wikilink")。之後的30年由主持研究所。

蘇聯[天文學家](../Page/天文學家.md "wikilink")[柳德米拉·雀尼克在](../Page/柳德米拉·切尔尼赫.md "wikilink")1972年發現的[小行星2142以朗道命名](../Page/小行星2142.md "wikilink")\[127\]。月球上的[朗道环形山也是以之命名](../Page/朗道环形山.md "wikilink")。

## 著作

朗道堪称著作等身，一生完成的著作多达120余部。\[128\]

### 《理论物理学教程》

朗道和其学生[叶夫根尼·利夫希茨的](../Page/叶夫根尼·利夫希茨.md "wikilink")《理论物理学教程》以行文简练而又洞察深刻的风格著称于世。这套教程对读者的数学基本功要求比较高（一些他认为明显的步骤和次要的细节情形就会跳过），但又不迷失于琐碎繁难的各种数学细节，每一本书都有清晰的物理思想作为贯穿前后的指引，贯彻“直觉先行”的特点。整套书以统一的“[理论物理式](../Page/理论物理.md "wikilink")”风格，描述了一个理论物理工作者应当具备的基础知识\[129\]。朗道展示了以最少的基本信息，由[第一性原理出发](../Page/第一性原理.md "wikilink")，快速猜测和推导出重要公式的“物理直觉”。只凭少量原理和直觉指引就能得出涉及自然规律的大量重要结论是理论物理学的一大特色，也是[还原论思想在科学领域中的充分体现](../Page/还原论.md "wikilink")。书中有许多习题本身就曾是研究成果\[130\]，甚至有朗道以前从未公开发表的研究。

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
其中《流体动力学》和《弹性理论》在1954年之前原为一册《连续介质力学》\[131\]。

### 其它教材

  -   - 英文版：

  -
### 大众读物

朗道也参与创作了不少[科学普及读物](../Page/科学普及.md "wikilink")。这些书籍或是小册子属于扫盲级别的读物，配有不少卡通插图，适合没接触过物理或缺乏物理基础教育的读者\[132\](如了解[三角函数的中学生](../Page/三角函数.md "wikilink"))：

  -   - 英译版：
      - 汉译版： (统一书号: 13031·2958)

  -   - 英译版：
      - 汉译版： (统一书号: 13031·1738)

  -
注：《大众物理学》(，)一共有4卷，但有朗道作为作者署名的仅有前2卷，而第3卷《电子》(，)和第4卷《光子和原子核》(，)仅是由亚历山大·基泰戈罗特斯基1人所著。

## 传记

  - （有13小时时长的俄语[有声读物版发行](../Page/有声读物.md "wikilink")）（注：书名中的“Роман”在俄语里有“浪漫爱情”或“小说”之意）

      - 汉译版：\[133\]

  -
  - (作者是女性物理学者，可在线阅读)

      - 英译版：

  - (朗道妻子写的回忆录)

## 文化影响

### 文学

  - 俄国作家的名作《》中的主角维克托·施特鲁姆（，）是一名[核物理学者](../Page/核物理.md "wikilink")。该书的英译者罗伯特·钱德勒（Robert
    Chandler）指出此人物形象源于朗道。

## 参閱

  - [朗道理論物理研究所](../Page/朗道理論物理研究所.md "wikilink")

## 参考资料

### 文内引用

### 补充来源

  -
  -
  -
  -
  -
  -
  -
  -
  -
### 延伸阅读

  -
  - (这是[美国物理协会的一份会议内容幻灯片](../Page/美国物理协会.md "wikilink")，其中第9页包含通过“朗道势垒”的全部学生姓氏。)

## 外部链接

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
[Category:诺贝尔物理学奖获得者](../Category/诺贝尔物理学奖获得者.md "wikilink")
[Category:俄羅斯/蘇聯諾貝爾獎獲得者](../Category/俄羅斯/蘇聯諾貝爾獎獲得者.md "wikilink")
[Category:苏联诺贝尔奖得主](../Category/苏联诺贝尔奖得主.md "wikilink")
[Category:猶太諾貝爾獎獲得者](../Category/猶太諾貝爾獎獲得者.md "wikilink")
[Category:馬克斯·普朗克獎章獲得者](../Category/馬克斯·普朗克獎章獲得者.md "wikilink")
[Category:社會主義勞動英雄](../Category/社會主義勞動英雄.md "wikilink")
[Category:勞動紅旗勳章獲得者](../Category/勞動紅旗勳章獲得者.md "wikilink")
[Category:列寧勳章獲得者](../Category/列寧勳章獲得者.md "wikilink")
[Category:斯大林獎獲得者](../Category/斯大林獎獲得者.md "wikilink")
[Category:列寧獎獲得者](../Category/列寧獎獲得者.md "wikilink")
[Category:俄羅斯科學院院士](../Category/俄羅斯科學院院士.md "wikilink")
[Category:英國皇家學會院士](../Category/英國皇家學會院士.md "wikilink")
[Category:美国国家科学院院士](../Category/美国国家科学院院士.md "wikilink")
[Category:苏联物理学家](../Category/苏联物理学家.md "wikilink")
[Category:理論物理學家](../Category/理論物理學家.md "wikilink")
[Category:猶太科學家](../Category/猶太科學家.md "wikilink")
[Category:蘇聯發明家](../Category/蘇聯發明家.md "wikilink")
[Category:猶太發明家](../Category/猶太發明家.md "wikilink")
[Category:莫斯科國立大學教師](../Category/莫斯科國立大學教師.md "wikilink")
[Category:聖彼得堡國立大學校友](../Category/聖彼得堡國立大學校友.md "wikilink")
[Category:教科书作家](../Category/教科书作家.md "wikilink")
[Category:蘇聯猶太人](../Category/蘇聯猶太人.md "wikilink")
[Category:阿塞拜疆猶太人](../Category/阿塞拜疆猶太人.md "wikilink")
[Category:俄國犹太人](../Category/俄國犹太人.md "wikilink")
[Category:巴庫人](../Category/巴庫人.md "wikilink")
[Category:安葬於新聖女公墓者](../Category/安葬於新聖女公墓者.md "wikilink")

1.  这是[安德烈·萨哈罗夫给的评价](../Page/安德烈·萨哈罗夫.md "wikilink")，英文原文为“In his
    influence on the development of science in our country and the
    entire world, Landau is one of the top figures.”出自
    (位于该书第14章“从军工物理到和平的宇宙学”(From Military
    Physics To Peaceful Cosmology)第3节“宇宙中的物质与反物质”(Matter and Antimatter
    in the Universe)。

2.

3.
4.
5.  见。

6.
7.  见 (位于该书第5章“坍缩是必然的”第2节“朗道”)。

8.
9.
10.
11.
12.
13.
14.
15.
16. 见 (位于该书第5章“坍缩是必然的”第2节“朗道”)。

17. 见 (位于该书第7章“在贝利亚指挥下的核子物理”(Nuclear Physics Under Beria's
    Command)第2节“库恰托夫，‘大外交官’”(Kurchatov, the "Great Diplomat")。

18.
19.  (位于该书第7章“在贝利亚指挥下的核子物理”(Nuclear Physics Under Beria's
    Command)第2节“库恰托夫，‘大外交官’”(Kurchatov, the "Great
    Diplomat")。

20. 见第6章。

21.
22.
23.
24.

25.
26.
27.
28.
29.
30.
31. 见 (位于该书第12章“苏联培养下的理论物理学者”(Theoretical Physicists in Soviet
    Practice)第1节“塔姆，朗道，以及‘缘由’”(Tamm, Landau, and the "Cause")。

32. 见 (位于该书第10章“动工”(The Installation)第3节“制作‘千层糕’”(making sloyka)。

33. 见 (位于该书第10章“动工”(The Installation)第3节“制作‘千层糕’”(making sloyka)。

34. 见 (位于该书第10章“动工”(The Installation)第3节“制作‘千层糕’”(making sloyka)。

35. 见 (位于该书第12章“苏联培养下的理论物理学者”(Theoretical Physicists in Soviet
    Practice)第1节“塔姆，朗道，以及‘缘由’”(Tamm, Landau, and the "Cause")。

36.
37.
38.
39.
40.
41. 见 (位于该书第5章“坍缩是必然的”第3节“奥本海默”)。

42. 见。

43. 见。

44. 见。

45.
46. 见。

47.
48. 见。

49.
50.
51.
52. 见。

53. 见。

54. 见 (位于该书第12章“苏联培养下的理论物理学者”(Theoretical Physicists in Soviet
    Practice)第1节“塔姆，朗道，以及‘缘由’”(Tamm, Landau, and the "Cause")。

55.
56.
57.
58.
59. 见 (位于该书第5章“安德烈·萨哈罗夫，塔姆的研究生”(Andrei Sakharov, Tamm's Graduate
    Student)第3节“原子之内，核，与热核”(intra-atomic, nuclear, and thermonuclear)。

60. 见 (位于该书第5章“安德烈·萨哈罗夫，塔姆的研究生”(Andrei Sakharov, Tamm's Graduate
    Student)第3节“原子之内，核，与热核”(intra-atomic, nuclear, and thermonuclear)。

61.
62. 见 (位于该书第12章“苏联培养下的理论物理学者”(Theoretical Physicists in Soviet
    Practice)第1节“塔姆，朗道，以及‘缘由’”(Tamm, Landau, and the "Cause")。

63. 见。

64. 见 (位于该书第5章“坍缩是必然的”第2节“朗道”)。

65.
66. 见 (位于该书第5章“坍缩是必然的”第2节“朗道”)。

67.
68.
69.
70.

71. 见 (位于原文第5节“‘废话’与‘病态’”)。

72. 见。

73.
74. 见 (位于原文第5节“‘废话’与‘病态’”)。

75.
76. 见 (位于原文第2节“列宁格勒——哈尔科夫——莫斯科”)。

77. 见 (位于第8章“物理学者们忙于工作且认为这样再正常不过了”(Physicists Went About Their Work and
    Regarded It As Perfectly Normal))，摘录如下：“It is said of Landau's
    character in his youthful years that he was cocky and outspoken in
    his judgements to the point of deliberate eccentricity. These traits
    reminded me of the young Mayakovsky when he... I think that such
    exaltation of the ego is characteristic of geniuses who are still
    seeking a position which is worthy of their talents. When Mayakovsky
    won general recognition he softened, became more approachable and
    kinder. Landau followed the same pattern. When universal recognition
    came to him - both at home and abroad - he stopped being arrogant.”

78. 见 (位于第8章“物理学者们忙于工作且认为这样再正常不过了”(Physicists Went About Their Work and
    Regarded It As Perfectly Normal))，摘录如下：“There are many stories about
    Landau's arrogance. His friends, pupils and pupils' pupils hand on
    these legends from mouth to mouth and in them there clearly appears
    an inevitable element of fabrication. We shall not "enrich" the
    distortions which, unfortunately, already exist in the literature
    about Landau. There are, however, also well-documented facts.”

79. 见 (位于该书第12章“苏联培养下的理论物理学者”(Theoretical Physicists in Soviet
    Practice)第1节“塔姆，朗道，以及‘缘由’”(Tamm, Landau, and the "Cause")。

80. 见。

81. 见 (位于原文第5节“‘废话’与‘病态’”)。

82.

83.
84. 见。

85.

86.

87.
88. 见。

89.
90. 见 (位于原文第1节“神童和活跃分子”)。

91.
92.
93.

94. 见 (位于第8章“物理学者们忙于工作且认为这样再正常不过了”(Physicists Went About Their Work and
    Regarded It As Perfectly Normal))，摘录如下：“Pavel Sigismundovich
    Ehrenfest introduced me to Landau at a conference on theoretical
    physics in Berlin at the very end of 1929. Landau said to me
    regretfully: 'Just as all the pretty girls are all spoken for and
    married, so are all the best problems already solved ... I doubt if
    I shall find anything worthwhile among those that remain.' But he
    did find something. In January 1930, while staying with Pauli in
    Zurich, he discovered what he called the last of the great problems
    - the analysis in quantum terms of the movement of electrons in a
    constant magnetic field. He solved this problem in the spring of
    that year while in Cambridge with Rutherford. So it was that
    Landau's diamagnetism and Pauli's paramagnetism came upon the scene
    at the same moment in the history of physics ...”

95. 见 (位于该书第5章“坍缩是必然的”第2节“朗道”)。

96.
97. 见 (位于原文第5节“‘废话’与‘病态’”)。

98.
99. 见 (位于第8章“物理学者们忙于工作且认为这样再正常不过了”(Physicists Went About Their Work and
    Regarded It As Perfectly Normal))，摘录如下：“A typical example is
    Landau's telegram to Niels Bohr. In 1931, when Landau was in
    England, Dirac gave an account at a seminar of the work which had
    led to his famous equation. He tried to make sense of it in terms of
    "the positively charged electron" (the future positron\!). Bohr held
    Landau's work in high estimation and sent him Dirac's paper, asking
    him his opinion. He soon received a telegram from Landau in which he
    gave his assessment of Dirac's ideas. The telegram was short and
    unambiguous: "Quatsch" - i. e., rubbish. This is yet another proof
    of the difficulty with which even brilliant minds bring themselves
    to accept new ideas. But no one in that age of heated argument took
    offence at such attacks.”

100.

101.
102.
103.
104. 见。

105. 见。

106. 见 (位于原文第1节“神童和活跃分子”)。

107.
108.
109.
110. 见 (位于原文第1节“神童和活跃分子”)。

111.

112.

113. 见 (位于原文第5节“‘废话’与‘病态’”)。

114.
115.
116. 见 (位于原文第5节“‘废话’与‘病态’”)。

117.
118.
119. 见 (位于原文第1节“神童和活跃分子”)。

120. 见。

121.
122.
123. 见 (位于该书第10章“动工”(The Installation)第3节“制作‘千层糕’”(making sloyka)。

124. 见 (位于该书第14章“从军工物理到和平的宇宙学”(From Military Physics To Peaceful
     Cosmology)第1节“发明家还是理论家？”(Inventor or Theorist?)。

125. 见 (位于该书第5章“坍缩是必然的”第2节“朗道”)。

126.

127.

128.
129. 见。

130. 见。

131. 见。

132. 见 (位于“致俄语第4版前言”)。

133.