**列格尼卡战役**（，），亦称**列格尼兹战役**或**莱格尼茨战役**等，爆发于1241年4月9日，地点在现[波兰境内](../Page/波兰.md "wikilink")[列格尼卡的一座小村莊](../Page/萊格尼察.md "wikilink")。蒙古軍在此戰擊敗了波德聯軍。

[Mongoltroops.jpg](https://zh.wikipedia.org/wiki/File:Mongoltroops.jpg "fig:Mongoltroops.jpg")
交战双方是[拔都统率](../Page/拔都.md "wikilink")，[速不台指挥的](../Page/速不台.md "wikilink")[蒙古军队与](../Page/蒙古.md "wikilink")[西里西亚](../Page/西里西亚.md "wikilink")[公爵](../Page/公爵.md "wikilink")[亨利二世率领下的波兰军队](../Page/亨里克二世_\(西里西亚\).md "wikilink")。兵力方面，蒙古軍大約有8,000
-
20,000人（2個[圖們](../Page/萬戶制.md "wikilink")）；波蘭聯軍則大約有25,000人。這支聯軍的士兵分別來自波蘭自身的軍隊、以及少量[聖殿騎士團](../Page/聖殿騎士團.md "wikilink")、[摩拉维亚](../Page/摩拉维亚.md "wikilink")。傷亡人數方面，蒙古軍死傷人數暫時無法得知。

蒙古人取得了此战的胜利，令歐洲各國對此大為震驚，但是由于[窝阔台汗去世的消息传来](../Page/窝阔台汗.md "wikilink")，他们不得不折返回东方去选出新任[大汗](../Page/大汗.md "wikilink")，于是列格尼卡也成为[蒙古西征中所到达的最西方](../Page/蒙古西征.md "wikilink")。

## 参考來源

[Category:波蘭戰役](../Category/波蘭戰役.md "wikilink")
[Category:蒙古帝國戰役](../Category/蒙古帝國戰役.md "wikilink")
[Category:金帳汗國](../Category/金帳汗國.md "wikilink")
[Category:1241年](../Category/1241年.md "wikilink")