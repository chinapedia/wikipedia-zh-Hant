**利威尔-奇维塔联络**（****），在[黎曼几何中](../Page/黎曼几何.md "wikilink")，
是[切丛上的无](../Page/切丛.md "wikilink")[挠率](../Page/聯絡的撓率.md "wikilink")[联络](../Page/联络_\(数学\).md "wikilink")，它保持[黎曼度量](../Page/黎曼度量.md "wikilink")(或[伪黎曼度量](../Page/伪黎曼流形.md "wikilink"))不变。因[意大利数学家](../Page/意大利.md "wikilink")[图利奥·列维-奇维塔而得名](../Page/图利奥·列维-奇维塔.md "wikilink")。

[黎曼几何基本定理表明存在唯一联络满足这些属性](../Page/黎曼几何基本定理.md "wikilink")。

在[黎曼流形和](../Page/黎曼流形.md "wikilink")[伪黎曼流形的理论中](../Page/伪黎曼流形.md "wikilink")，[共变导数一词经常用于列维](../Page/共变导数.md "wikilink")-奇维塔联络。联络的坐标空间的表达式称为[克里斯托费尔符号](../Page/克里斯托费尔符号.md "wikilink")。

## 形式化定义

设 \((M,g)\)
为一[黎曼流形](../Page/黎曼流形.md "wikilink")（或[伪黎曼流形](../Page/伪黎曼流形.md "wikilink")），则[仿射联络](../Page/仿射联络.md "wikilink")
\(\nabla\) 在满足以下条件时是列维-奇维塔联络。

1.  无[挠率](../Page/聯絡的撓率.md "wikilink")：也就是，对任何向量场 \(X, Y\) 我们有
    \(\nabla_XY-\nabla_YX=[X,Y]\)，其中 \([X,Y]\)
    是[向量场](../Page/向量场.md "wikilink") \(X\) 和 \(Y\)
    的[李括号](../Page/李导数.md "wikilink")。

<!-- end list -->

1.  与度量相容：也就是,对任何向量场 \(X, Y, Z\)我们有
    \(Xg(Y,Z)=g(\nabla_X Y,Z)+g(Y,\nabla_X Z)\)，其中 \(Xg(Y,Z)\) 表示函数
    \(g(Y,Z)\) 沿向量场 \(X\) 的导数。

## 沿曲线的导数

列维-奇维塔联络也定义了一个沿曲线的导数，通常用 \(D\) 表示。

给定一个在 \((M,g)\) 上的光滑[曲线](../Page/曲线.md "wikilink") \(\gamma\)和\(\gamma\)
上的一个[向量场](../Page/向量场.md "wikilink") \(V\)，其导数定义如下

\[\frac{D}{dt}V=\nabla_{\dot\gamma(t)}V.\]

## 外部链接

  - [MathWorld: Levi-Civita
    Connection](http://mathworld.wolfram.com/Levi-CivitaConnection.html)
  - [PlanetMath: Levi-Civita
    Connection](https://web.archive.org/web/20041026085619/http://planetmath.org/encyclopedia/LeviCivitaConnection.html)

[L](../Category/黎曼几何.md "wikilink") [L](../Category/联络.md "wikilink")