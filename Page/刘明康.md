[Liu_Mingkang_-_Annual_Meeting_of_the_New_Champions_Tianjin_2008.jpg](https://zh.wikipedia.org/wiki/File:Liu_Mingkang_-_Annual_Meeting_of_the_New_Champions_Tianjin_2008.jpg "fig:Liu_Mingkang_-_Annual_Meeting_of_the_New_Champions_Tianjin_2008.jpg")
**刘明康**（），生于[上海](../Page/上海.md "wikilink")\[1\]，[籍贯](../Page/籍贯.md "wikilink")[福建省](../Page/福建省.md "wikilink")[福州市](../Page/福州市.md "wikilink")\[2\]\[3\]，1988年2月加入中国共产党，1965年9月参加工作。曾任[中国银行行长](../Page/中国银行.md "wikilink")，[中国银行业监督管理委员会主席](../Page/中国银行业监督管理委员会.md "wikilink")。中共第十六届中央候补委员，第十七届中央委员。

刘明康曾在1985年1月到1987年2月在[英国](../Page/英国.md "wikilink")[倫敦城市大學](../Page/倫敦城市大學.md "wikilink")[卡斯商学院学习](../Page/卡斯商学院.md "wikilink")，获[工商管理学硕士学位](../Page/工商管理学硕士.md "wikilink")；2001年11月又在英国[倫敦城市大學获得名誉博士学位](../Page/倫敦城市大學.md "wikilink")。

## 官方简历

  - 1965年9月至1976年1月在[江苏省](../Page/江苏省.md "wikilink")[丹阳市](../Page/丹阳市.md "wikilink")[河阳乡](../Page/河阳乡.md "wikilink")[插队](../Page/插队落户.md "wikilink")。
  - 1976年1月至1979年6月在江苏省丹阳市轧钢厂当工艺员。
  - 1979年6月至10月任江苏省丹阳市总[工会](../Page/工会.md "wikilink")[干部](../Page/干部.md "wikilink")。
  - 1979年10月至1984年7月任[中国银行江苏省分行干部](../Page/中国银行.md "wikilink")。
  - 1984年7月至1987年5月任中国银行[伦敦分行部门经理](../Page/伦敦.md "wikilink")。
  - 1987年5月至1988年9月任中国银行江苏省分行[信托投资公司总经理](../Page/信托投资公司.md "wikilink")。
  - 1988年9月至1992年11月任中国银行[福州市分行副行长](../Page/福州市.md "wikilink")。
  - 1992年11月至1993年1月任中国银行[福建省分行行长](../Page/福建省.md "wikilink")、[党组书记](../Page/党组书记.md "wikilink")。
  - 1993年1月至1994年1月任福建省副[省长兼省政府秘书长](../Page/省长.md "wikilink")。
  - 1994年1月至1998年3月任[国家开发银行副行长](../Page/国家开发银行.md "wikilink")。
  - 1998年3月至1999年3月任[中国人民银行副行长](../Page/中国人民银行.md "wikilink")，1999年3月起兼任党委副书记。
  - 1999年7月至2000年6月任[中国光大集团](../Page/中国光大集团.md "wikilink")[董事长](../Page/董事长.md "wikilink")、党组书记。
  - 2000年2月至2003年3月任[中国银行董事长](../Page/中国银行.md "wikilink")、[行长](../Page/行长.md "wikilink")、党委书记。
  - 2003年3月至2005年4月任中国银行业监督管理委员会主席、党委副书记。
  - 2005年4月至2011年10月任中国银行业监督管理委员会主席、党委书记。
  - 2011年12月至今任中山大学岭南（大学）学院名誉院长。

## 所获荣誉

  - 2010年 第一财经金融价值榜 年度金融思想家\[4\]
  - 2011年 第一财经金融价值榜 年度金融风云人物\[5\]

## 参考文献

{{-}}

[Category:中国银行业监督管理委员会主席](../Category/中国银行业监督管理委员会主席.md "wikilink")
[Category:中國銀行行長](../Category/中國銀行行長.md "wikilink")
[Category:中国光大集团董事长](../Category/中国光大集团董事长.md "wikilink")
[Category:中国共产党党员
(1988年入党)](../Category/中国共产党党员_\(1988年入党\).md "wikilink")
[L](../Category/中国共产党第十六届中央委员会候补委员.md "wikilink")
[L](../Category/中国共产党第十七届中央委员会委员.md "wikilink")
[Category:伦敦城市大学校友](../Category/伦敦城市大学校友.md "wikilink")
[Category:卡斯商学院校友](../Category/卡斯商学院校友.md "wikilink")
[Category:香港公開大學榮譽博士](../Category/香港公開大學榮譽博士.md "wikilink")
[Category:香港嶺南大學榮譽博士](../Category/香港嶺南大學榮譽博士.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[Category:福州人](../Category/福州人.md "wikilink")
[M](../Category/刘姓.md "wikilink")
[Category:国家开发银行副行长](../Category/国家开发银行副行长.md "wikilink")
[Category:中国人民银行副行长](../Category/中国人民银行副行长.md "wikilink")
[Category:中华人民共和国福建省副省长](../Category/中华人民共和国福建省副省长.md "wikilink")

1.  [刘明康同志简历](http://politics.people.com.cn/GB/shizheng/252/9667/9683/6592152.html)
2.  [刘明康：银行业应服务实体经济并不断创新](http://www.apdnews.com/news/34133.html)
    ，亚太日报，2013年9月10日
3.  [传记从美中贸易全国委员会](http://www.uschina.org/public/china/govstructure/bio/liumingkang.html)
    ，英文 (2011-6-17)
4.
5.