**劉春紅**（），[籍贯](../Page/籍贯.md "wikilink")[山東](../Page/山東.md "wikilink")[烟台](../Page/烟台.md "wikilink")，[中國女子](../Page/中國.md "wikilink")[舉重運動員](../Page/舉重.md "wikilink")。

## 生涯

劉春紅本是從事[柔道運動](../Page/柔道.md "wikilink")，但於1996年轉為接受舉重運動。兩年後入選山東隊。4年後成為了國家隊的成員之一。

劉春紅在加入國家隊後的次年，就在[九運會奪得了女子](../Page/九運會.md "wikilink")69公斤级舉重賽事中的[金牌](../Page/金牌.md "wikilink")。1年後，在[亞運奪得一面](../Page/2002年亞洲運動會.md "wikilink")[金牌](../Page/金牌.md "wikilink")。於2003年分別奪得了世锦赛和亚锦赛[冠軍](../Page/冠軍.md "wikilink")。

2004年，蟬聯了世锦赛的冠軍，更於[雅典奥运為](../Page/2004年夏季奧林匹克運動會.md "wikilink")[中國摘下第](../Page/中國.md "wikilink")13面[金牌](../Page/金牌.md "wikilink")。

2005年，在[世界舉重錦標賽女子](../Page/世界舉重錦標賽.md "wikilink")75公斤組，以[挺舉](../Page/挺舉.md "wikilink")159公斤與總合285公斤雙破世界新紀錄。但是，抓舉項目不敵[俄國的](../Page/俄國.md "wikilink")[札波羅娜娃](../Page/札波羅娜娃.md "wikilink")，而位居亞軍，札波羅娜娃兩度把劉春紅刷新的世界紀錄再次改寫，最後以130公斤的世界新紀錄摘金，舊紀錄125公斤也是她所締造的。

2008年8月13日，劉春紅在[北京奧運會女子舉重](../Page/北京奧運會.md "wikilink")69公斤級以總成績286公斤（抓舉128公斤、挺舉158公斤）打破抓舉、挺舉及總成績的三項[奧運及世界紀錄](../Page/奧運.md "wikilink")，並奪得冠軍。在抓举部分，刘春红先以125公斤打破原為123公斤的抓举世界记录，再以128公斤打破自己刚刚创造的世界记录，把世界紀錄推至128公斤。在挺举部分，刘春红先舉起145公斤，第二举則以149公斤打破总成绩的世界记录，最后一举，刘春红舉起158公斤成功，打破挺舉及總成绩的世界记录，先後五次打破三项的世界记录。刘春红夺金後表示：「自己能以破纪录夺金的成绩向[胡锦涛总书记汇报](../Page/胡锦涛.md "wikilink")，没有让[总书记和全国人民失望](../Page/总书记.md "wikilink")。」\[1\]

2016年8月，国际奥委会在对2008年北京奥运会运动员的再次尿检中发现刘春红的样本中发现促生长激素释放肽并且[西布曲明呈现阳性](../Page/西布曲明.md "wikilink")\[2\]，2017年1月12日被國際奧委會宣布剥夺金牌。2017年8月，[国际体育仲裁庭駁回其上訴](../Page/国际体育仲裁院.md "wikilink")\[3\]。

## 参考资料

[Category:中国举重运动员](../Category/中国举重运动员.md "wikilink")
[Category:中国奥运举重运动员](../Category/中国奥运举重运动员.md "wikilink")
[Category:體育世界紀錄保持者](../Category/體育世界紀錄保持者.md "wikilink")
[Category:中国奥林匹克运动会金牌得主](../Category/中国奥林匹克运动会金牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會金牌得主](../Category/2004年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:烟台籍运动员](../Category/烟台籍运动员.md "wikilink")
[C春红](../Category/刘姓.md "wikilink")
[Category:2004年夏季奧林匹克運動會舉重運動員](../Category/2004年夏季奧林匹克運動會舉重運動員.md "wikilink")
[Category:2008年夏季奧林匹克運動會舉重運動員](../Category/2008年夏季奧林匹克運動會舉重運動員.md "wikilink")
[Category:奧林匹克運動會舉重獎牌得主](../Category/奧林匹克運動會舉重獎牌得主.md "wikilink")
[Category:亞洲運動會舉重獎牌得主](../Category/亞洲運動會舉重獎牌得主.md "wikilink")
[Category:2002年亞洲運動會舉重運動員](../Category/2002年亞洲運動會舉重運動員.md "wikilink")
[Category:2010年亞洲運動會舉重運動員](../Category/2010年亞洲運動會舉重運動員.md "wikilink")
[Category:2002年亞洲運動會金牌得主](../Category/2002年亞洲運動會金牌得主.md "wikilink")
[Category:2010年亚洲运动会金牌得主](../Category/2010年亚洲运动会金牌得主.md "wikilink")

1.
2.  [PUBLIC
    DISCLOSURES](http://www.iwf.net/2016/08/24/public-disclosures-5/).IWF.2016-08-24.\[2016-08-24\].
3.