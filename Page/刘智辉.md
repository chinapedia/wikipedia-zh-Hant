[CaoBinpoetry.jpg](https://zh.wikipedia.org/wiki/File:CaoBinpoetry.jpg "fig:CaoBinpoetry.jpg")
[Fzyyylj.jpg](https://zh.wikipedia.org/wiki/File:Fzyyylj.jpg "fig:Fzyyylj.jpg")
[DuFu'spoems.jpg](https://zh.wikipedia.org/wiki/File:DuFu'spoems.jpg "fig:DuFu'spoems.jpg")

**劉智輝**（），字書庵，號無極。1975年拜[劉炳森為師](../Page/劉炳森.md "wikilink")，1982年加入[中國書法家協會](../Page/中國書法家協會.md "wikilink")。他亦為[北京市書法家協會宣傳工作委員會副主任](../Page/北京市.md "wikilink")，[中國書法培訓中心教授](../Page/中國書法培訓中心.md "wikilink")，[中國國際書畫藝術研究會顧問等](../Page/中國國際書畫藝術研究會.md "wikilink")。他自10歲開始習字。他的作品被[中國美術館](../Page/中國.md "wikilink")、[北京圖書館](../Page/北京.md "wikilink")、對外藝術展覽公司收藏，並被[新加坡](../Page/新加坡.md "wikilink")、[日本](../Page/日本.md "wikilink")、[加拿大等國家和](../Page/加拿大.md "wikilink")[台灣](../Page/台灣.md "wikilink")、[香港等地藝術機構收藏](../Page/香港.md "wikilink")。另外，作品亦被國内多間博物館及碑林收藏。劉智輝以行書、草書名世，其書法作品遍佈世界各地，以[日本](../Page/日本.md "wikilink")、[韓國](../Page/韓國.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[香港和](../Page/香港.md "wikilink")[台灣流傳最為廣泛](../Page/台灣.md "wikilink")。

| 1993年獲「書壇青年百傑」的稱號。                                                                                                                                                                                                                |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1997年獲「世界功勳藝術家」的稱號。                                                                                                                                                                                                               |
| 1998年獲「中國百名藝術家」的稱號。                                                                                                                                                                                                               |
| 2000年成為第二批通過ISO2000標準價值評定的書法家。                                                                                                                                                                                                    |
| 2005年12月1日《劉智輝先生書法作品展》在[湖南省博物館開幕](../Page/湖南省.md "wikilink")，原省政協主席[王克英](../Page/王克英.md "wikilink")、省政協副主席[文選德](../Page/文選德.md "wikilink")、[湖南省書法家協會主席](../Page/湖南省.md "wikilink")[何滿宗以及書畫界同仝参加了開幕儀式](../Page/何滿宗.md "wikilink")。 |
|                                                                                                                                                                                                                                   |

## 主要作品

## 外部連結

  - [劉智輝](https://web.archive.org/web/20080110133508/http://www.wujidaoren.com/)
  - [湖南省博物館書法作品展](http://www.wujidaoren.com/2007/03/01/hunan-museum-exhibition-of-calligraphy/)
  - [天賦地造心似有苦寒寡欲情自來———劉智輝和他的書法藝術](http://www.caacjournal.com/2006journal/327/6327c1.asp)
  - [神州墨海弄潮兒---劉智輝書法藝術談](http://www.wujidaoren.com/2003/08/15/mexican-china-sea-pioneer/)
  - [走筆山河大地
    情動香草松煙——小叙書法家劉智輝先生](http://www.wujidaoren.com/2005/12/01/article-of-mountains-rivers-the-earth-moving-situation-vanilla-breathing/)
  - [書法作品欣賞](http://sf.wujidaoren.com/)

[L](../Page/category:中國新詩作家.md "wikilink")

[L劉](../Category/中華人民共和國書法家.md "wikilink")
[L劉](../Category/北京人.md "wikilink")
[L劉](../Category/1949年出生.md "wikilink")