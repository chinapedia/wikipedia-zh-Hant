**刘永川**（）是一位“研究方法和[数据科学](../Page/数据科学.md "wikilink")”的知名学者，被广泛称为“大数据分析与机器学习应用于商业与社会研究”的顶尖专家。刘永川也是[中国](../Page/中国.md "wikilink")1980年代至90年代的留学生代表人物。

## 职业简历

刘永川1982年获得中国[西北工业大学的航空机械学士](../Page/西北工业大学.md "wikilink")、1986年获得[北京大学的社会学硕士](../Page/北京大学.md "wikilink")、1993年获得美国[斯坦福大学的统计计算硕士和社会学博士](../Page/斯坦福大学.md "wikilink")。

刘博士1993年开始在[斯坦福大学亚太研究中心从事医疗服务的比较研究](../Page/斯坦福大学.md "wikilink")、之后在美国[IBM研究院研究因果分析的方法和算法](../Page/IBM研究院.md "wikilink")、再到[圣地牙哥加利福尼亚大学瑞帝管理学院研究落后地区创新创业的社会环境](../Page/圣地牙哥加利福尼亚大学.md "wikilink")、并担任研究员。2002年至2011年,刘永川曾在[南加州大学马歇尔商学院和](../Page/南加州大学.md "wikilink")[尔湾加州大学保罗梅拉吉商学院教授博士研究生的高级社会研究方法](../Page/尔湾加州大学.md "wikilink")，并曾到南亚的斯里兰卡、中亚的哈萨克斯坦与吉尔吉斯斯坦和非洲的肯尼亚从事扶贫项目评估方法和创业发展援助的项目。刘博士2009年创建“研究方法与数据科学”全球协会并担任负责人，自2010年起曾担任多家跨国公司如
TRG 和 RS 的首席数据科学家，自2013年起担任 IBM 大数据分析的首席数据科学家,
于2018年获得[IBM和开源行业协会的](../Page/IBM.md "wikilink")"数据科学"思想领袖证书。

## 成长经历

刘永川出生于中国、江西、[井冈山](../Page/井冈山.md "wikilink")\[1\]，在1977年的高考中考入西北工业大学，是在中国引入数量社会科学的先驱之一\[2\]
\[3\]。他于1986年赴美国留学，1987年担任斯坦福大学[中国学生学者联合会会长](../Page/中国学生学者联合会.md "wikilink")，1988年担任加州中国学生学者联合会主席，1989年7月经当时在美国的4万多中国留学生的代表选举担任[全美中国学生学者自治联合会首届主席](../Page/全美中国学生学者自治联合会.md "wikilink")\[4\]，于1990年8月经选举担任[全球中国学联首届总召集人](../Page/全球中国学联.md "wikilink")。1995年至1997年，刘博士曾在美国硅谷主办
“华裔美国人创业高峰会议”，和“中美兵兵外交25周年纪念”\[5\]等。刘博士于2000年加入[美国国籍](../Page/美国国籍.md "wikilink")，2004年成为[基督徒](../Page/基督徒.md "wikilink")\[6\]
\[7\]。2006年至2011年，刘博士提出[精神资本主导的四资本发展理论和相关的整体方法论](../Page/精神资本.md "wikilink")\[8\]，近年专注于研究方法的中西比较分析。

## 代表著作

  - [Apache Spark 机器学习 机械工业出版社 2017](https://item.jd.com/12161918.html)
  - [Apache Spark Machine Learning Blueprints Packt
    Publishing 2016](https://www.amazon.com/Apache-Spark-Machine-Learning-Blueprints/dp/178588039X)
  - \[<http://onlinelibrary.wiley.com/doi/10.1002/9781118900772.etrds0325/abstract;jsessionid=07D9B9EEB7E4DEA36C8F8E0C74DAECDC.f04t03>?
    结构方程模型与潜在变量方法 约翰威立国际出版公司 2015\]
  - [社会科学的结构方程模型建立(教科书)
    RM出版社 2009](https://www.amazon.com/Building-Structural-Equation-Science-ebook/dp/B002C74J1U/)
  - [社会科学的回归模型建立(教科书)
    RM出版社 2009](https://www.amazon.com/Building-Regression-Models-Science-ebook/dp/B002BNMMPA/)
  - [日本全国健康保险业中费用共付与使用的分析
    国际健康研究期刊 1995](http://journals.sagepub.com/doi/abs/10.2190/CU5H-CNQ9-HUW6-993F)
  - [第三次民主浪潮的模式和结果，美国大学出版社 1993](https://www.amazon.com/Patterns-Results-Third-Democratization-Wave/dp/0819191248)
  - [协调发展与综合决策
    《科学．经济．社会》1987](http://www.cqvip.com/QK/94401X/198703/1003091278.html)
  - [数理社会学
    《国外社会科学》1985](http://mall.cnki.net/magazine/article/GWSH198510023.htm)

## 参考报道

  - [星岛日报
    刘博士谈人工智能 2017](https://www.singtaousa.com/%E5%8D%97%E5%8A%A0%E6%96%B0%E8%81%9E_la/360808-%E6%B4%9B%E6%9D%89%E7%A3%AF%E5%A4%A7%E6%95%B8%E6%93%9A%E8%AB%96%E5%A3%87%E2%80%82%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E6%99%82%E4%BB%A3%E5%8B%A2%E4%B8%8D%E5%8F%AF%E6%93%8B/)
  - [中新网
    刘博士谈人工智能 2017](http://www.chinanews.com/hr/2017/11-06/8369665.shtml)
  - [SCIENCE 科学杂志
    谈刘博士 2014](http://science.sciencemag.org/content/344/6187/953)
  - [世界日报
    谈刘博士 2004](http://hx.cnd.org/2004/09/07/%E7%BE%8E%E5%9B%BD%E7%89%B9%E6%AE%8A%E7%A7%BB%E6%B0%91%E7%BE%A4%E4%BD%93%E2%80%95%E2%80%95%E5%85%AD%E5%9B%9B%E7%BB%BF%E5%8D%A1%E5%A4%A7%E5%86%9B/)

## 外部链接

  - [刘永川博士 - “研究方法和数据科学”专家](http://www.researchmethods.org/alexc.htm)
  - [刘永川博士的代表著述](http://www.researchmethods.org/alexc_writings.htm)
  - [刘永川博士领英档案 LinkedIn Profiel](https://www.linkedin.com/in/alexyliu/)
  - [刘永川博士ResearchGate
    档案](https://www.researchgate.net/profile/Alex_Liu29)
  - [刘永川博士的 IBM 博客](http://www.ibmbigdatahub.com/blog/author/alex-liu)
  - [刘永川博士 -
    四资本理论](http://www.researchmethods.org/4capitalc/b4capital.htm)
  - [刘永川博士谈反恐](http://www.researchmethods.org/liu-75xinjiang)

## 参考资料

[Category:史丹佛大学校友](../Category/史丹佛大学校友.md "wikilink")
[Category:北京大学校友](../Category/北京大学校友.md "wikilink")
[Category:西北工业大学校友](../Category/西北工业大学校友.md "wikilink")
[Category:归化美国公民的中华人民共和国人](../Category/归化美国公民的中华人民共和国人.md "wikilink")
[Yong Chuan](../Category/刘姓.md "wikilink")
[Category:井冈山人](../Category/井冈山人.md "wikilink")
[Category:中国社会学家](../Category/中国社会学家.md "wikilink")
[Category:美國客家人](../Category/美國客家人.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.