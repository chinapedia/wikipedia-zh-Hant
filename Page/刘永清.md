[Laura_Bush_and_Liu_Yongqing_(April_20,_2006).jpg](https://zh.wikipedia.org/wiki/File:Laura_Bush_and_Liu_Yongqing_\(April_20,_2006\).jpg "fig:Laura_Bush_and_Liu_Yongqing_(April_20,_2006).jpg")[劳拉·布什在](../Page/劳拉·布什.md "wikilink")[白宫](../Page/白宫.md "wikilink")\]\]

**刘永清**（），前中共中央总书记、国家主席、中央军委主席[胡锦涛的](../Page/胡锦涛.md "wikilink")[夫人](../Page/夫人.md "wikilink")，[中国](../Page/中国.md "wikilink")[重庆人](../Page/重庆.md "wikilink")。曾担任[北京市城乡规划委员会副主任](../Page/北京市.md "wikilink")。

刘永清毕业于[重庆](../Page/重庆.md "wikilink")[巴蜀中学](../Page/巴蜀中学.md "wikilink")，后考入[清华大学水利系](../Page/清华大学.md "wikilink")1959年级学习，是胡锦涛大学期间同班同学，后来结为连理。大学毕业后先于胡锦涛分配到当时位于[甘肃](../Page/甘肃.md "wikilink")[兰州的](../Page/兰州.md "wikilink")[水利部第四工程局](../Page/水利部.md "wikilink")。
1970年2月与胡锦涛结婚。在1971年、1972年先后生下一子一女\[1\]。

## 家族

  - 丈夫：[胡锦涛](../Page/胡锦涛.md "wikilink")（1942年12月25日－）。
      - 儿子：[胡海峰](../Page/胡海峰.md "wikilink")（1972年11月
        －）。现任[中国共产党](../Page/中国共产党.md "wikilink")[浙江省](../Page/浙江省.md "wikilink")[丽水市市委书记](../Page/丽水市.md "wikilink")。
          - 儿媳：[王珺](../Page/王珺.md "wikilink")（1973年－），胡海峰的大学同学，1995年毕业于北方交通大学。1998年获得北京大学经济学硕士。2008年获得清华大学经济学博士。2009年6月，出任由前总理朱镕基担任创院院长的清华大学经济管理学院院长助理。
      - 女儿：[胡海清](../Page/胡海清.md "wikilink")（1973年12月 －）。
          - 女婿：[茅道临](../Page/茅道临.md "wikilink")（1963年1月10日－），曾任[新浪网CEO](../Page/新浪网.md "wikilink")。
  - 姐姐：[刘永平](../Page/刘永平.md "wikilink")。
      - 姐夫：[王彦峰](../Page/王彦峰.md "wikilink")。堂侄子刘剑

## 参考文献

## 外部链接

## 参见

  - [中华人民共和国主席配偶](../Page/中华人民共和国主席配偶.md "wikilink")
  - [胡锦涛](../Page/胡锦涛.md "wikilink")

{{-}}

[L](../Category/中国共产党中央委员会总书记夫人.md "wikilink")
[5](../Category/中华人民共和国主席夫人.md "wikilink")
[L劉](../Category/清華大學校友.md "wikilink")
[Category:重庆巴蜀中学校友](../Category/重庆巴蜀中学校友.md "wikilink")
[Category:胡锦涛家族](../Category/胡锦涛家族.md "wikilink")
[L劉](../Category/重庆人.md "wikilink")
[Y永](../Category/劉姓.md "wikilink")

1.