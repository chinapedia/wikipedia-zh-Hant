**劉璇**（），[湖南](../Page/湖南.md "wikilink")[長沙人](../Page/長沙.md "wikilink")，[2000年悉尼奧運](../Page/2000年夏季奧林匹克運動會體操比賽.md "wikilink")[平衡木金牌](../Page/平衡木.md "wikilink")、體操個人全能銅牌得主。此前於[1996年亞特蘭大奧運團體全能賽](../Page/1996年夏季奧林匹克運動會.md "wikilink")，她開始[高低杠動作不久就失誤墮地得](../Page/高低杠.md "wikilink")0分，而全隊隊員在各項目均獲8.9分以上，最終中國隊總分385，差4分就獲金牌，僅得第4名，此失誤令她為中國人熟悉，至2000年才一吐烏氣。有「璇美人」稱号。

退役後，[北京大學新聞系畢業](../Page/北京大學.md "wikilink")\[1\]，曾任體育記者、節目主持、電視劇演員等。因為是眾多國家運動員的大師姐，憑良好人脈，常被派往採訪體動員。[2008年奧運受聘香港TVB任賽場記者](../Page/2008年奧運.md "wikilink")，自此長留香港發展，並通過[香港優才計劃](../Page/香港優才計劃.md "wikilink")。

## 個人简介

### 運動員生涯

劉璇於6歲湖南長沙
小學時就因為家长想讓劉璇學會自立而送到業餘體育學校接受體操訓練，後來年僅8歲就入[湖南省体操中心专业队](../Page/湖南省.md "wikilink")，5年後（13岁）入選國家隊。

劉璇的首個具重要的[冠軍是在全國賽中奪冠](../Page/冠軍.md "wikilink")。一年後於[七運會的女子團體項目奪得](../Page/七運會.md "wikilink")[金牌](../Page/金牌.md "wikilink")。又於1994年在[廣島](../Page/廣島.md "wikilink")[亞運會上獲得高低杠項目](../Page/1994年亞洲運動會.md "wikilink")[銀牌與女團項目的](../Page/銀牌.md "wikilink")[金牌](../Page/金牌.md "wikilink")。

一年之後，劉璇於[世界錦標賽的女團賽中得亞軍](../Page/世界.md "wikilink")。1996年，劉璇於世界盃的平衡木賽事中得到季軍，又參加了[亞特蘭大奧運](../Page/1996年夏季奧林匹克運動會.md "wikilink")，可惜只在女團項目中取得第4名，未能得到獎牌。一年後，劉璇在世界錦標賽的女團賽得到季軍。

1998年，劉璇在世界盃中贏得首個冠軍，她在平衡木比賽中奪冠，又獲高低杠季軍。2000年，劉璇先後得到了全國錦標賽的平衡木賽冠軍與全能賽[亞軍](../Page/亞軍.md "wikilink")，更於[悉尼奧運贏得了女子平衡木項目的](../Page/悉尼奧運.md "wikilink")[金牌](../Page/金牌.md "wikilink")。同年，劉璇得到了[全國十佳運動員的獎項](../Page/全國十佳運動員.md "wikilink")。

### 退役生涯

刘璇於2001年退役，进入[北京大学新闻与传播学院](../Page/北京大学.md "wikilink")，2005年7月5日获得新闻學系学士学位。刘璇退役后有意向[演艺界发展](../Page/演艺界.md "wikilink")，同时熱心[公益](../Page/公益.md "wikilink")。

劉璇在2007年5月11日於[广东](../Page/广东.md "wikilink")[佛山的全國體操冠軍賽中作為平衡木和跳馬兩個環節的裁判](../Page/佛山.md "wikilink")，是她首次的體操裁判工作。

2008年，應[香港](../Page/香港.md "wikilink")[無綫電視](../Page/電視廣播有限公司.md "wikilink")（TVB）邀請，擔任[北京奧運會的體操專業評述](../Page/北京奧運會.md "wikilink")，後簽約成為TVB經理人合約藝員，同年通過[优才计划成為](../Page/优才计划.md "wikilink")[香港居民](../Page/香港.md "wikilink")。

2010年，刘璇首度拍攝TVB民初武打[电视劇集](../Page/电视劇集.md "wikilink")
《[女拳](../Page/女拳.md "wikilink")》擔正第一女主角，2011年推出首張個人專輯《美麗的樣子》。

2012年，刘璇与男友[王弢结束](../Page/王弢.md "wikilink")11年爱情长跑，成为王太太；王弢是四川足球元老[王茂俊的独生子](../Page/王茂俊.md "wikilink")，1978年生于[成都](../Page/成都.md "wikilink")。1995年考入[中央音乐学院管弦系](../Page/中央音乐学院.md "wikilink")。2002年成为中央音乐学院的第一个单簧管专业硕士、中国第一个单簧管研究生。

2013年，正式进入娱乐圈，加盟[银润影视传媒集团](../Page/银润影视传媒集团.md "wikilink")。参加中央电视塔制作的舞蹈节目[舞出我人生](../Page/舞出我人生.md "wikilink")，并和[韩艺博一起夺得冠军](../Page/韩艺博.md "wikilink")。并参演《[我和我的小伙伴们](../Page/我和我的小伙伴们.md "wikilink")》、《[穿警服的那些女孩儿](../Page/穿警服的那些女孩儿.md "wikilink")》等影视剧，并为动画电影《[我是狼](../Page/我是狼.md "wikilink")》配音。在12月7日香港大婚前，推出全新婚礼单曲暨公益单曲《[无添加的爱](../Page/无添加的爱.md "wikilink")》。

2013年12月7日，刘璇与男友王弢在[香港賽馬會和游轮举行浪漫婚礼](../Page/香港賽馬會.md "wikilink")。\[2\]2015年10月，劉璇在[香港產下一子](../Page/香港.md "wikilink")。

## 演出作品

### 電影

  - 2002年 《[我的美麗鄉愁](../Page/我的美麗鄉愁.md "wikilink")》
  - 2009年 《[东风雨](../Page/东风雨.md "wikilink")》
  - 2013年 《[我和我的小伙伴们](../Page/我和我的小伙伴们.md "wikilink")》
  - 2013年 《[我是狼](../Page/我是狼.md "wikilink")》（配音）
  - 2016年 《[看见我和你](../Page/看见我和你.md "wikilink")》饰 黄小逸

### 電視劇

  - 2002年 《[我和我的父亲](../Page/我和我的父亲.md "wikilink")》饰 高文君
  - 2003年 《[终极目标](../Page/终极目标.md "wikilink")》饰 廖静仪
  - 2004年 《[夜半歌声](../Page/夜半歌声.md "wikilink")》饰 田菲
  - 2012年 《[Strangers6](../Page/Strangers6.md "wikilink")》饰 文靜
  - 2016年 《[东方战场](../Page/东方战场.md "wikilink")》饰
    [婉容](../Page/婉容.md "wikilink")
  - 2016年 《[穿警服的那些女孩儿](../Page/穿警服的那些女孩儿.md "wikilink")》饰 凌麦穗
  - 2018年 《[扶摇](../Page/扶摇.md "wikilink")》饰 非烟

### 綜藝節目（中國大陸）

  - 2005年 《[浅蓝深蓝](../Page/浅蓝深蓝.md "wikilink")》
  - 2006年 《[舞林大会](../Page/舞林大会.md "wikilink")》
  - 2007年《[名声大震](../Page/名声大震.md "wikilink")》
  - 2013年《[舞出我人生](../Page/舞出我人生.md "wikilink")》
  - 2014年《[百变大咖秀](../Page/百变大咖秀.md "wikilink")》
  - 2018年《[爸媽學前班](../Page/爸媽學前班.md "wikilink")》

### 綜藝節目（[無綫電視](../Page/無綫電視.md "wikilink")）

  - 2008年 《[TVB8全球華人新秀歌唱大賽](../Page/TVB8全球華人新秀歌唱大賽.md "wikilink")》
  - 2010年 《[上海世博行](../Page/上海世博行.md "wikilink")》

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

|        |                                |        |        |
| ------ | ------------------------------ | ------ | ------ |
| **首播** | **劇名**                         | **角色** | **性質** |
| 2011年  | [女拳](../Page/女拳.md "wikilink") | 莫桂蘭    | 第一女主角  |

## 音樂

### 唱片

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1st</p></td>
<td style="text-align: left;"><p><a href="../Page/Beautiful_Faces_美麗的樣子.md" title="wikilink">Beautiful Faces 美麗的樣子</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/星娛樂.md" title="wikilink">星娛樂</a></p></td>
<td style="text-align: left;"><p>2011年2月18日</p></td>
<td style="text-align: left;"><p><strong>CD</strong></p>
<ol>
<li>羅馬陽光</li>
<li>愛上你就是唯一（光良/劉璇）</li>
<li>往事剪接手</li>
<li>Cry For You</li>
<li>心浪</li>
<li>麻</li>
<li>美麗的樣子</li>
<li>証明</li>
<li>抓不住的愛情</li>
<li>Happy Day</li>
</ol>
<p><strong>DVD</strong></p>
<ol>
<li>羅馬陽光MV</li>
<li>往事剪接手MV</li>
</ol></td>
</tr>
</tbody>
</table>

### 其他歌曲

  - 2008年：甜上心頭
  - 2008年：出發
  - 2009年：I wish for you
  - 2011年：回見（電視劇《[女拳](../Page/女拳.md "wikilink")》片尾曲）
  - 2013年：无添加的爱（关爱自闭症三棵柚公益基金主题歌）

### 派台歌曲

| **派台歌曲成績**                                                           |
| -------------------------------------------------------------------- |
| 唱片                                                                   |
| **2010**                                                             |
| [Beautiful Faces 美麗的樣子](../Page/Beautiful_Faces_美麗的樣子.md "wikilink") |
| Beautiful Faces 美麗的樣子                                                |
| **2011**                                                             |
| Beautiful Faces 美麗的樣子                                                |
| [Love TV 情歌精選 3](../Page/Love_TV_情歌精選_3.md "wikilink")               |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **0**       |

## 個人著作

  - 《璇木》（ISBN
    9787544732154，[译林出版社](../Page/译林出版社.md "wikilink")：个人自传，讲述了刘璇从5岁开始训练，到20岁退役的体操生涯。）\[3\]

## 獎項

  - 2010年：TVB8金曲榜頒獎典禮 - 金曲獎 《往事剪接手》
  - 2011年：[新城國語力頒獎禮](../Page/新城國語力頒獎禮.md "wikilink") - 新城國語力合唱歌曲
    《愛上你就是唯一》（註：和光良合唱）

## 參考資料

## 外部連結

  - [劉璇官方論壇](http://www.liu-xuan.com/)

  - [劉璇全球個人官方網站](https://web.archive.org/web/20080825005812/http://liuxuan.sports.cn/)

  -
[category:前無綫電視女藝員](../Page/category:前無綫電視女藝員.md "wikilink")

[L刘](../Category/长沙籍运动员.md "wikilink")
[Category:中国体操运动员](../Category/中国体操运动员.md "wikilink")
[Xuan](../Category/刘姓.md "wikilink")
[Category:中国奥林匹克运动会金牌得主](../Category/中国奥林匹克运动会金牌得主.md "wikilink")
[Category:中国奥林匹克运动会铜牌得主](../Category/中国奥林匹克运动会铜牌得主.md "wikilink")
[Category:2000年夏季奧林匹克運動會金牌得主](../Category/2000年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:中國女歌手](../Category/中國女歌手.md "wikilink")
[Category:北京大学校友](../Category/北京大学校友.md "wikilink")
[Category:1996年夏季奧林匹克運動會體操運動員](../Category/1996年夏季奧林匹克運動會體操運動員.md "wikilink")
[Category:2000年夏季奧林匹克運動會體操運動員](../Category/2000年夏季奧林匹克運動會體操運動員.md "wikilink")
[Category:奧林匹克運動會體操獎牌得主](../Category/奧林匹克運動會體操獎牌得主.md "wikilink")
[Category:1994年亚洲运动会金牌得主](../Category/1994年亚洲运动会金牌得主.md "wikilink")
[Category:1994年亞洲運動會銀牌得主](../Category/1994年亞洲運動會銀牌得主.md "wikilink")
[Category:1998年亞洲運動會金牌得主](../Category/1998年亞洲運動會金牌得主.md "wikilink")
[Category:世界体操锦标赛奖牌得主](../Category/世界体操锦标赛奖牌得主.md "wikilink")
[Category:取得香港特別行政區居留權人士](../Category/取得香港特別行政區居留權人士.md "wikilink")

1.  \[<http://women.sohu.com/20050706/n226197250.shtml>
    劉璇在北大的學業大揭底，[搜狐新聞](../Page/搜狐.md "wikilink")，2005年7月6日。
2.  [刘璇大婚 在港举行浪漫“邮轮婚礼”](http://www.apdnews.com/photo/52761.html)
    ，亚太日报，2013年12月9日
3.  [刘璇自传《璇木》出版](http://news.ifeng.com/gundong/detail_2012_09/24/17836719_0.shtml?_from_ralated)