**刘福通**（），[颍州](../Page/潁州_\(北魏\).md "wikilink")（今安徽阜阳界首市）人。[元末民变领袖](../Page/元末民变.md "wikilink")。

## 生平

[至正十一年](../Page/至正.md "wikilink")（1351年）五月，刘福通與[韓山童率](../Page/韓山童.md "wikilink")[白莲教教众和被征挖](../Page/白莲教.md "wikilink")[黄河河道的河工起事](../Page/黄河.md "wikilink")，在[潁州](../Page/潁州.md "wikilink")[潁上發動起義](../Page/潁上.md "wikilink")，部眾以紅巾為號，信奉[彌勒佛](../Page/彌勒佛.md "wikilink")，组织“[红巾军](../Page/红巾军.md "wikilink")”，对抗元军，攻克[安徽](../Page/安徽.md "wikilink")、[河南地區](../Page/河南.md "wikilink")，开始了元末农民运动。

至正十五年（1355年）二月迎[韓山童之子](../Page/韓山童.md "wikilink")[韓林兒為皇帝](../Page/韓林兒.md "wikilink")，稱小明王，国号宋，定都[亳州](../Page/亳州.md "wikilink")，建元[龙凤](../Page/龙凤.md "wikilink")。他为[枢密院平章](../Page/枢密院.md "wikilink")，旋改任丞相。

至正十七年（1357年）二月，龙凤将领[毛贵浮海破](../Page/毛贵.md "wikilink")[胶州](../Page/胶州.md "wikilink")；三月，陷[莱州](../Page/莱州.md "wikilink")，据[益都](../Page/益都.md "wikilink")。龙凤将领[李武](../Page/李武.md "wikilink")、[崔德绕过](../Page/崔德.md "wikilink")[潼关](../Page/潼关.md "wikilink")，夺[七盘](../Page/七盘.md "wikilink")，进据[蓝田](../Page/蓝田.md "wikilink")，直趋[奉元路](../Page/奉元路.md "wikilink")。六月，刘福通自帅一军攻[汴梁](../Page/汴梁.md "wikilink")，余军分三路北伐：[關鐸](../Page/關鐸.md "wikilink")、[潘誠](../Page/潘誠.md "wikilink")、[沙劉二等攻](../Page/沙劉二.md "wikilink")[怀庆](../Page/怀庆.md "wikilink")，深入[晋](../Page/晋.md "wikilink")[冀](../Page/冀.md "wikilink")，[白不信](../Page/白不信.md "wikilink")、[大刀敖等西取](../Page/大刀敖.md "wikilink")[关中](../Page/关中.md "wikilink")；[毛贵自](../Page/毛贵.md "wikilink")[山东北上](../Page/山东.md "wikilink")。

至正十八年（1358年）二月，[毛贵攻占](../Page/毛贵.md "wikilink")[济南](../Page/济南.md "wikilink")。三月，毛贵北攻[蓟州](../Page/蓟州.md "wikilink")、[漷州](../Page/漷州.md "wikilink")，进逼[枣林](../Page/枣林.md "wikilink")，距[大都一百二十里](../Page/大都.md "wikilink")，战失利，退回[济南](../Page/济南.md "wikilink")。五月，刘福通攻破[汴梁](../Page/汴梁.md "wikilink")，自[安丰](../Page/安丰.md "wikilink")（今安徽寿县）迎[韩林儿](../Page/韩林儿.md "wikilink")，定为国都。龙凤政权中央分设[六部](../Page/六部.md "wikilink")、[御史等诸官属](../Page/御史.md "wikilink")；在[山东](../Page/山东.md "wikilink")、[江南等地分设](../Page/江南.md "wikilink")[行省](../Page/行省.md "wikilink")。

至正十九年（1359年）正月，[關鐸](../Page/關鐸.md "wikilink")、[潘誠](../Page/潘誠.md "wikilink")、[沙劉二东攻](../Page/沙劉二.md "wikilink")[全宁路](../Page/全宁路.md "wikilink")，焚鲁王府宫阙。再破[元上都](../Page/元上都.md "wikilink")，焚之。进破[辽阳](../Page/辽阳.md "wikilink")，入[高丽境](../Page/高丽.md "wikilink")。八月，[汴梁被](../Page/汴梁.md "wikilink")[察罕帖木儿攻破](../Page/察罕帖木儿.md "wikilink")，刘福通与[韩林儿退据](../Page/韩林儿.md "wikilink")[安丰](../Page/安丰.md "wikilink")（今安徽寿县）。

至正二十三年（1363年）二月，[張士誠遣](../Page/張士誠.md "wikilink")[呂珍圍攻](../Page/呂珍.md "wikilink")[安豐](../Page/安豐.md "wikilink")，杀劉福通\[1\]\[2\]。[朱元璋前往救援](../Page/朱元璋.md "wikilink")，打败[呂珍](../Page/呂珍.md "wikilink")，迎[韓林兒到](../Page/韓林兒.md "wikilink")[滁州](../Page/滁州.md "wikilink")。

## 参考文献

[L刘](../Category/元朝民變領袖.md "wikilink")
[Category:元朝白蓮教人物](../Category/元朝白蓮教人物.md "wikilink")
[Category:紅巾軍人物](../Category/紅巾軍人物.md "wikilink")
[L刘](../Category/元朝戰爭身亡者.md "wikilink")
[L刘](../Category/界首人.md "wikilink")
[Category:劉姓](../Category/劉姓.md "wikilink")

1.  [权衡的](../Page/权衡.md "wikilink")《庚申外史》记录是刘福通在至正二十六年与韩林儿同死于瓜步沉船，“小明王与刘太保（即刘福通）至瓜州渡（江苏六合县东南），遇风浪掀舟没，刘太保小明王俱亡。”《四库全书总目》认为，《庚申外史》“所言多与《元史》合”。[吴晗写](../Page/吴晗.md "wikilink")《朱元璋》也沿用这种说法。一說刘福通被杀于安丰，[高岱](../Page/高岱.md "wikilink")《鸿猷录》卷二《宋事始末》云：“张士诚遣将吕珍率兵攻安丰，福通遣使诣建康求救，上自率诸将救之。未至，吕珍攻破安丰，杀福通，据其城。三月，上至安丰击吕珍，大破之，珍弃城走。上遂以宋主韩林儿归金陵。”谷应泰《明史纪事本末》卷四《太祖平吴》亦稱被殺於安丰，[邵元平](../Page/邵元平.md "wikilink")《元史类编》、[夏燮](../Page/夏燮.md "wikilink")《明通鉴》、[毕沅](../Page/毕沅.md "wikilink")《续资治通鉴》、[翦伯赞](../Page/翦伯赞.md "wikilink")《中国史纲要》、[邱树森](../Page/邱树森.md "wikilink")《元朝史话》亦赞同被殺於安豐
2.  另據元末明初人劉辰《國初事蹟》中記載，呂珍未能殺死劉福通，劉福通退到滁州。