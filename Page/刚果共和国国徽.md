**[刚果共和国國徽](../Page/刚果共和国.md "wikilink")**為一黃地盾徽。繪有一隻呈忿怒狀、右爪持火炬的紅色綠爪綠舌[獅子](../Page/獅子.md "wikilink")，中有綠色波浪紋。上有一頂書有國名的王冠。盾徽由兩隻[象守護](../Page/非洲象.md "wikilink")。飾帶以[法語書上國家格言](../Page/法語.md "wikilink")「正義、勞動、進步」。
该国徽于1963年启用，1970年废除，2003年再度启用。

[Category:剛果共和國](../Category/剛果共和國.md "wikilink")
[C](../Category/國徽.md "wikilink")