{{ Distinguish|剛果共和國}}

**刚果民主共和国**（，簡寫為），是位於[非洲](../Page/非洲.md "wikilink")[中部的](../Page/中部非洲.md "wikilink")[國家](../Page/國家.md "wikilink")，簡稱**民主剛果**（）、**剛果（金）**（）。陸地[面积約](../Page/面积.md "wikilink")234.5万[平方公里](../Page/平方公里.md "wikilink")，是非洲第2大（僅次於[阿爾及利亞](../Page/阿爾及利亞.md "wikilink")）、暨世界第11大的国家。人口超过8100万\[1\]，是世界人口第16多、非洲人口第4多的国家，同時是世界人口最多的[法語國家](../Page/法语国家和地区列表.md "wikilink")。首都暨最大都市為[金沙薩](../Page/金沙薩.md "wikilink")。

民主剛果的前身為[剛果自由邦與](../Page/剛果自由邦.md "wikilink")[比屬剛果](../Page/比屬剛果.md "wikilink")，1960年6月30日獨立建國。由於建國時與鄰近稍早獨立的[原法屬剛果同樣以](../Page/刚果共和国.md "wikilink")「剛果共和國」做為國號，國際社會在兩國名稱後括注首都名稱以作區別，因此又別名为**刚果共和国（利奥波德维尔）**。1964年8月1日改國名为**剛果民主共和國**\[2\]。1971年10月27日，時任總統[蒙博托為](../Page/蒙博托·塞塞·塞科.md "wikilink")[去殖民化而將國名更改為](../Page/去殖民化.md "wikilink")**[薩伊](../Page/薩伊.md "wikilink")**（），1997年5月17日，[洛朗·卡比拉领导的](../Page/洛朗·卡比拉.md "wikilink")攻占金沙薩推翻蒙博托政權，并恢復国名为刚果民主共和国至今。

1998年开始的[第二次刚果战争](../Page/第二次刚果战争.md "wikilink")，让这个国家满目疮痍。因为这场战争涉及了9个非洲国家和大约20个武装势力，因此也被称为“非洲的[世界大战](../Page/世界大战.md "wikilink")”\[3\]。尽管在2003年签署了和平协定，但战斗仍在在该国东部地区继续。在刚果东部，[强奸和其他](../Page/强奸.md "wikilink")[性暴力的发生率被認為是世界上最高的](../Page/性暴力.md "wikilink")\[4\]。这场战争自1998年以来造成540万人死亡\[5\]\[6\]，其中绝大多数死于[疟疾](../Page/疟疾.md "wikilink")、[腹泻](../Page/腹泻.md "wikilink")、[肺炎和](../Page/肺炎.md "wikilink")[营养不良](../Page/营养不良.md "wikilink")\[7\]。

## 历史

[Mercator_Congo_map.jpg](https://zh.wikipedia.org/wiki/File:Mercator_Congo_map.jpg "fig:Mercator_Congo_map.jpg")

现被称为[剛果民主共和國的地区](../Page/剛果民主共和國.md "wikilink")，在8万年前即有人类居住。早期的历史开始于刚果[班图人的迁移浪潮](../Page/班图人.md "wikilink")，他們在公元前2000年到公元前500年从西北迁移到盆地地区，期间包括前殖民地时期至推翻殖民者时期。班图人的迁徙取代了本地土著[俾格米人](../Page/俾格米人.md "wikilink")，他們將其文化融合进现代刚果南部地区。班图人从[西非地区学习了](../Page/西非.md "wikilink")[农业和](../Page/农业.md "wikilink")[铁器的使用方法](../Page/铁器.md "wikilink")，以刚果的[班图语系語言为主要方言](../Page/班图语.md "wikilink")。

人類随后从[达尔富尔和](../Page/达尔富尔.md "wikilink")[苏丹](../Page/苏丹.md "wikilink")[科尔多凡地区迁移进入刚果北部](../Page/科尔多凡.md "wikilink")，东非人也迁移进入刚果东部，加速了民族的融合。人类遷移使技术從[石器时代过渡到](../Page/石器时代.md "wikilink")[铁器时代](../Page/铁器时代.md "wikilink")。南部和西南部人大部分是狩猎、采集者，幾乎不含金属技术的使用。金属工具在这段时间内的发展，彻底改变了[农业和](../Page/农业.md "wikilink")[畜牧业](../Page/畜牧业.md "wikilink")。这导致东部和东南部的狩猎采集者被班图人取代。10世纪的班图人完成了最后一次向中部非洲西部的扩张。人口的增长使交易网络迅速形成，[商業交易主要为盐](../Page/商業.md "wikilink")、铁和铜。

在公元5世纪，一个氏族社会开始沿[烏朋巴湖與](../Page/烏朋巴湖.md "wikilink")[加丹加的](../Page/加丹加.md "wikilink")[卢阿拉巴河两岸发展](../Page/卢阿拉巴河.md "wikilink")。此文明被称为[乌彭巴](../Page/乌彭巴.md "wikilink")，最终演变成[卢巴王国](../Page/卢巴王国.md "wikilink")（Kingdom
of Luba）以及[隆达王国](../Page/隆达王国.md "wikilink")（Kingdom of
Lunda）。原始的乌彭巴社会演變成卢巴王国的过程是渐进和复杂的。这种转变不断進行，发展出几种与乌彭巴文化不同的[社会文化](../Page/社会.md "wikilink")。每一个社会都来源发展自其之前社会文化（与[罗马文化借鉴](../Page/罗马文化.md "wikilink")[希腊文化相似](../Page/希腊.md "wikilink")）。

公元5世纪的社会演变在卡兰巴（Kamilamba）周围地区发展，然後由一些在[桑加和](../Page/桑加.md "wikilink")[加丹加周边的文化所取替](../Page/加丹加.md "wikilink")。在刚果地区出现的是这些特别丰富的矿石，除了在象牙和其他商品的交易外，开始制定文明并实施钢铁、铜技术。乌彭巴因自己对金属工艺的大量需求建立了一套大型商路（超过1500公里的商业网络，直到印度洋）。此外，该地区拥有良好的农业条件和丰富的鱼类和野味。其强大的经济和粮食储备，使该地区变得非常富有。城邦和中央政府建立在酋长制度上，成为普遍接受的政治制度，统治者变得越来越强大，特别是在16世纪。

班图人曾经于13世纪末14世纪初在这个地区建立过刚果王国，领土包括现在的安哥拉、加蓬和两刚果的疆域。15世纪晚期，欧洲人来到这里开始奴隶贸易。到19世纪早期奴隶贸易结束的时候，这些班图王国都衰落了。

1482年[葡萄牙的航海家](../Page/葡萄牙.md "wikilink")[狄亞哥最先發現](../Page/狄亞哥.md "wikilink")[剛果河](../Page/剛果河.md "wikilink")、並且在河口立碑為誌後，在以後的三世紀中，歐洲人才和[剛果開始作初步的接觸](../Page/剛果.md "wikilink")；有的人來此傳教，有的人開始在瀕[大西洋的河口建立貿易站](../Page/大西洋.md "wikilink")，用以販賣奴隸和其他物品。但是因為內部蠻荒多阻，深入剛果內部的歐洲人十分稀少。

1816年，英國也對剛果發生興趣，英國海軍派遣[詹姆斯·亨斯顿·杜凱](../Page/詹姆斯·亨斯顿·杜凱.md "wikilink")（James
Hingston
Tuckey）上校，率領裝備精良的探險隊溯河深入探測，終於因為河流湍急、氣候不適，杜凱一行人終告齎志以殁，他們的遺體現在還埋藏在[博马附近的太子島上](../Page/博马_\(刚果民主共和国\).md "wikilink")。

剛果和文明世界的接觸以及有計劃的開發，始於1874年到1877年。[英國探險家](../Page/英國.md "wikilink")[李文斯頓冒險深入剛果境內](../Page/大衛·李文斯頓_\(探險家\).md "wikilink")，但他將剛果河誤認為是[尼羅河的源流](../Page/尼羅河.md "wikilink")。1874年，新聞記者出身的英國探險家[史丹利](../Page/亨利·莫頓·史丹利.md "wikilink")，組織了一支包括342人的大探險隊，才認清楚了剛果河真貌。史丹利先向英國政府報告這塊還沒開發的財富之地，但是沒有得到當時英國政府的資助支持，所以轉向[比利時](../Page/比利時.md "wikilink")。

1876年，比利時國王[雷奧波爾特二世在](../Page/雷奧波爾特二世.md "wikilink")[布鲁塞尔召開國際開發非洲會議](../Page/布鲁塞尔.md "wikilink")，斯丹利在非洲探險和經營的成就，深獲比王的重視，於是就以國際開發名義極力資助斯丹利委託開發，使他重返非洲探險。斯丹利在逗留剛果5年期間，先後在各地建立了22處貿易站，同時在魯巴那河上流行駛汽船，並且在斯丹利瀑布附近建築鐵路，替比利時國王建立了「剛果獨立區」，並且在1885年成立[剛果自由邦](../Page/剛果自由邦.md "wikilink")。同年獲得在[柏林會議中的列強所承認](../Page/柏林會議.md "wikilink")，使比利時國王成為剛果自由邦的主人，1908年就將這個地方正名為「[比屬剛果](../Page/比屬剛果.md "wikilink")」。

刚果自由邦是一个通过殖民控制的非政府组织，由[比利时](../Page/比利时.md "wikilink")[利奥波德二世国王个人控制](../Page/利奥波德二世.md "wikilink")。利奥波德是董事长和唯一的股东，他越来越多地在上刚果地区攫取[象牙](../Page/象牙.md "wikilink")、[橡胶和矿产在世界市场上销售](../Page/橡胶.md "wikilink")（虽然其名义上的目的是为了提升当地人民生活和该地区的发展）。国家包括现代刚果民主共和国整个地区，存在于1885年至1908年期间。刚果自由邦最终因对当地人民日益残酷的虐待和掠夺自然资源而声名狼藉，导致比利时政府将其废除，并于1908年接管。

[Congo_belge_campagne_1918.jpg](https://zh.wikipedia.org/wiki/File:Congo_belge_campagne_1918.jpg "fig:Congo_belge_campagne_1918.jpg")（Force
Publique）士兵於[比屬剛果](../Page/比屬剛果.md "wikilink")，攝於1918年，在公共力量的巔峰時期，共有420名白人軍官領導19000名士兵\]\]

比利时在1908年举行了有关刚果的会议，尽管比利时最初不同意，但迫于国际压力（尤其是来自英国），最终接管了比利时国王的刚果自由邦。从此，它被称为比利时属刚果殖民地，在比利时政府的统治下直到1960年。

[PatricelumumbaIISG.jpg](https://zh.wikipedia.org/wiki/File:PatricelumumbaIISG.jpg "fig:PatricelumumbaIISG.jpg")\]\]

在[二次世界大戰後](../Page/二次世界大戰.md "wikilink")，刚果民族意識普遍覺醒，[民族運動逐漸醞釀](../Page/民族運動.md "wikilink")。1960年，由[帕特里斯·卢蒙巴领导的](../Page/帕特里斯·卢蒙巴.md "wikilink")[刚果民族运动党](../Page/刚果民族运动党.md "wikilink")（MNC）赢得了议会选举，卢蒙巴被任命为总理。议会选举刚果人同盟（ABAKO）的[约瑟夫·卡萨武布为总统](../Page/约瑟夫·卡萨武布.md "wikilink")。其他出现的政党包括Antoine
Gizenga领导的[非洲团结党](../Page/非洲团结党.md "wikilink")（PSA），和Albert
Delvaux与Laurent
Mbariko领导的国家人民党（PNP）。比属刚果在1960年6月30日独立，改名为“刚果共和国”。独立后不久，[加丹加省](../Page/加丹加省.md "wikilink")（由[莫伊兹·冲伯领导](../Page/莫伊兹·冲伯.md "wikilink")）和[南开赛省针对新领导人开展分裂斗争導致](../Page/南开赛省.md "wikilink")[聯合國剛果行動](../Page/聯合國剛果行動.md "wikilink")。在独立后留下的100,000名欧洲人大多逃离了这个国家，为刚果人取代欧洲军事、行政精英铺平了道路。

刚果独立后與鄰國同稱剛果共和國，在兩國名稱後括注首都名稱以作區別。\[8\]1964年8月1日改國名为剛果民主共和國。\[9\]1971年10月27日，時任總統[蒙博托為](../Page/蒙博托·塞塞·塞科.md "wikilink")[去殖民化而將國名更改為](../Page/去殖民化.md "wikilink")**[扎伊尔共和国](../Page/扎伊尔共和国.md "wikilink")**（）。1997年5月17日，[洛朗-德西雷·卡比拉领导的](../Page/洛朗-德西雷·卡比拉.md "wikilink")[刚果解放民主力量同盟的](../Page/刚果解放民主力量同盟.md "wikilink")[武装部队攻占首都金沙薩推翻蒙博托政權](../Page/武装部队.md "wikilink")，宣布就任[总统](../Page/总统.md "wikilink")，并恢復国名为刚果民主共和国至今。

[第二次刚果战争从](../Page/第二次刚果战争.md "wikilink")1998年开始，让这个国家满目疮痍，也被称为“非洲的[世界大战](../Page/世界大战.md "wikilink")”，因为涉及了9个非洲国家和大约20个武装势力。\[10\]尽管在2003年签署了和平协定，战斗仍在在该国东部地区继续。在刚果东部，[强奸和其他](../Page/强奸.md "wikilink")[性暴力的发生率被描述为世界上最高的](../Page/性暴力.md "wikilink")。\[11\]此次战争是自[第二次世界大战以来全世界最激烈的](../Page/第二次世界大战.md "wikilink")，1998年以来540万人死亡。\[12\]\[13\]
绝大多数死于[疟疾](../Page/疟疾.md "wikilink")、[腹泻](../Page/腹泻.md "wikilink")、[肺炎和营养不良](../Page/肺炎.md "wikilink")。\[14\]

## 地理

[Dem_congo_sat.jpg](https://zh.wikipedia.org/wiki/File:Dem_congo_sat.jpg "fig:Dem_congo_sat.jpg")

民主剛果位于中部非洲西部，[赤道横贯其中北部](../Page/赤道.md "wikilink")，东接[乌干达](../Page/乌干达.md "wikilink")、[卢旺达](../Page/卢旺达.md "wikilink")、[布隆迪](../Page/布隆迪.md "wikilink")、[坦桑尼亚](../Page/坦桑尼亚.md "wikilink")，北邻[南苏丹](../Page/南苏丹.md "wikilink")、[中非共和国](../Page/中非共和国.md "wikilink")，西界[刚果共和国](../Page/刚果共和国.md "wikilink")，南界[安哥拉](../Page/安哥拉.md "wikilink")、[赞比亚](../Page/赞比亚.md "wikilink")。[剛果河自东向西流贯全境](../Page/剛果河.md "wikilink")，另外有[乌班吉河](../Page/乌班吉河.md "wikilink")、[卢阿拉巴河等重要支流](../Page/卢阿拉巴河.md "wikilink")。东部边界自北向南有[艾伯特湖](../Page/艾伯特湖.md "wikilink")、[爱德华湖](../Page/爱德华湖.md "wikilink")、[基伍湖](../Page/基伍湖.md "wikilink")、[坦噶尼喀湖和](../Page/坦噶尼喀湖.md "wikilink")[姆韦鲁湖等](../Page/姆韦鲁湖.md "wikilink")[湖泊](../Page/湖泊.md "wikilink")。

### 位置與地形

[Cg-map.png](https://zh.wikipedia.org/wiki/File:Cg-map.png "fig:Cg-map.png")《[世界概況](../Page/世界概況.md "wikilink")》\]\]
[Congo_Kinshasa_Topography.png](https://zh.wikipedia.org/wiki/File:Congo_Kinshasa_Topography.png "fig:Congo_Kinshasa_Topography.png")

剛果民主共和國的位置在非洲中部，位於非洲赤道心臟地帶，地大物博，為非洲僅次於[阿爾及利亞的第](../Page/阿爾及利亞.md "wikilink")2大國。全國總面積2344858平方公里\[15\]。北鄰[中非共和國](../Page/中非共和國.md "wikilink")，東北和[南蘇丹接壤](../Page/南蘇丹.md "wikilink")。東鄰[東非地塹](../Page/東非地塹.md "wikilink")，峭壁深谷，水深奇秀，與[烏干達](../Page/烏干達.md "wikilink")、[坦尚尼亞](../Page/坦尚尼亞.md "wikilink")、[盧安達和](../Page/盧安達.md "wikilink")[蒲隆地相接](../Page/蒲隆地.md "wikilink")。南與[安哥拉毗連](../Page/安哥拉.md "wikilink")，東南界[尚比亞](../Page/尚比亞.md "wikilink")，西界[剛果](../Page/剛果.md "wikilink")。西南部[剛果河下游一隅瀕大西洋](../Page/剛果河.md "wikilink")，海岸線只有37公里長\[16\]。

這個橫跨赤道的國家，國境佔[剛果盆地的絕大部份](../Page/剛果盆地.md "wikilink")，並且被支流密佈的剛果河所灌溉；除中下游有較大的平原外，其餘東、北、南三面都是被高原和山地所環繞，其中又以南方和東南方的山嶺最為高峻。平均海拔由600公尺到1500公尺。

### 水系

[Congo_maluku.jpg](https://zh.wikipedia.org/wiki/File:Congo_maluku.jpg "fig:Congo_maluku.jpg")\]\]

剛果民主共和國以剛果河為主流，其他均為支流。剛果河發源於尚比亞境內，流長四千六百六十九公里，為世界第十長河。流域面積廣達三百六十七萬七千八百平方公里，其流量之大和流域之廣，僅次於[拉丁美洲的](../Page/拉丁美洲.md "wikilink")[亞馬孫河](../Page/亞馬孫河.md "wikilink")，位列全球第二，且流量十分穩定。這條河浩蕩西流，經安哥拉注入大西洋。它的重要支流有[乌班吉河及](../Page/乌班吉河.md "wikilink")[卡賽河](../Page/卡賽河.md "wikilink")，另有次要支流十六條，可供航行的有13000公里\[17\]。

### 氣候

剛果民主共和國大部份地區位置在北緯4度到南緯4度間，屬赤道氣候區，全年氣溫在攝氏21.1度（華氏70度）以上，平均溫度是攝氏27.8度（華氏82度），濕熱多雨。

以全境來說，全年分乾、雨兩季；每年十一月到第二年五月是雨季，氣候濕熱；六月到十月是乾季，氣候溫和。但是在夏天，中部和東部高原地區常有陣雨或豪雨，不失為解暑的天然良劑；並且中部山地海拔有1000公尺到2000公尺，高處山頂終年被雪掩蓋，通常被叫做「月球之山」。

### 地理區

其氣候由於地形的關係分成三個地帶\[18\]：

#### 高原地區

[Nyiragongo2004.jpg](https://zh.wikipedia.org/wiki/File:Nyiragongo2004.jpg "fig:Nyiragongo2004.jpg")

東部和南部高原地帶終年涼爽，雨量適中。

#### 金夏沙及以南地區

金夏沙市及其以南地帶每年六月到九月是乾季，氣候也很涼爽；每年十月到第二年二月是小雨季，三月到五月是大雨季，雨量甚豐，年雨量可達2000公釐。

#### 金夏沙以北地區

金夏沙市以北為赤道地帶，乾雨兩季分明；乾季白天氣溫可達攝氏40度（華氏104度），夜晚則只有攝氏18.3度（華氏65度），全年雨量約在1200到2000公釐間。

## 政治

2001年1月18日，[洛朗-德西雷·卡比拉被下属军官拉什迪](../Page/洛朗-德西雷·卡比拉.md "wikilink")·米泽勒（Rashidi
Mizele）刺杀身亡，其子[约瑟夫·卡比拉回国后接任总统](../Page/约瑟夫·卡比拉.md "wikilink")，执政至2019年1月24日。

2006年7月30日，刚果（金）举行总统和国民议会选举。\[19\]8月20日，独立选举委员会宣布，总统选举中无人获得50%以上的选票，[约瑟夫·卡比拉和](../Page/约瑟夫·卡比拉.md "wikilink")[让-皮埃尔·本巴进入第二轮选举](../Page/让-皮埃尔·本巴.md "wikilink")。\[20\]10月29日，第二轮总统选举举行。\[21\]11月15日，独立选举委员会宣布，卡比拉当选总统。\[22\]12月30日，[安托万·基赞加被任命为总理](../Page/安托万·基赞加.md "wikilink")。\[23\]

2011年12月16日，剛果最高法院裁定，在2011年11月28日舉行的總統選舉投票中，卡比拉獲得48.95％的選票，在所有候選人中得票最高。按照選舉法規定，卡比拉獲得簡單多數，當選總統獲得連任。卡比拉的主要競爭對手民主與社會進步聯盟主席齊塞凱迪獲得32.33％的選票，位居第二。這一數據和此前剛果獨立選舉委員會11月9日公佈的數據完全相同。

2016年12月31日，卡比拉與反對派達成協議，总统选举推迟至2017年底舉行\[24\]，等到了承諾日期选举官员卻再次表示選舉延後至2019年，該表態引發群眾強烈不滿\[25\]，隨後爆發示威遊行，遭剛果民主共和國政府镇压，造成8人死亡，十余名抗议者遭捕，剛果民主共和國政府下令由于「国家安全原因」，全国即日起无限期关闭網路与訊息服务\[26\]。

2019年1月10日，选委会宣布[齐塞克迪击败另一反对派候选人马丁](../Page/菲利克斯·齊塞克迪.md "wikilink")·法尤卢和获总统[约瑟夫·卡比拉支持的伊曼纽尔](../Page/约瑟夫·卡比拉.md "wikilink")·拉马扎尼·沙达利，当选成为刚果民主共和国总统。得票第二多的法尤卢拒绝承认落败，批评选举舞弊并挑战选举结果\[27\]。1月19日，民主刚果宪法法院裁定败诉，齐塞克迪正式成为候任总统\[28\]。

2019年1月24日，费利克斯·齐塞克迪宣誓就任总统\[29\]。

## 經濟

[Kinshasa_Congo.jpg](https://zh.wikipedia.org/wiki/File:Kinshasa_Congo.jpg "fig:Kinshasa_Congo.jpg")
[DRC_Rwanda_line.jpg](https://zh.wikipedia.org/wiki/File:DRC_Rwanda_line.jpg "fig:DRC_Rwanda_line.jpg")
[Democratic_Republic_of_the_Congo_GDP_evolution-fr.svg](https://zh.wikipedia.org/wiki/File:Democratic_Republic_of_the_Congo_GDP_evolution-fr.svg "fig:Democratic_Republic_of_the_Congo_GDP_evolution-fr.svg")
[DRC_classroom.jpg](https://zh.wikipedia.org/wiki/File:DRC_classroom.jpg "fig:DRC_classroom.jpg")
[Matadi_city_port_1965.JPG](https://zh.wikipedia.org/wiki/File:Matadi_city_port_1965.JPG "fig:Matadi_city_port_1965.JPG")
[Kisangani_rond-point_Cathédrale_et_Congo_Palace.jpg](https://zh.wikipedia.org/wiki/File:Kisangani_rond-point_Cathédrale_et_Congo_Palace.jpg "fig:Kisangani_rond-point_Cathédrale_et_Congo_Palace.jpg")\]\]

自1980年代中期以後，刚果的矿产资源已经大幅减少，雖然在1960年独立后是继[南非之后非洲第二個](../Page/南非.md "wikilink")[工业化国家](../Page/工业.md "wikilink")，但是它夸大了“高速”发展中的采矿业和农业部门的劳动生产率。第一次和第二次刚果战争及1996年开始的冲突\[30\]大大地衰弱了刚果的国民产值和财政收入，並给国库增加了高额外债。数次战争共造成500多万人丧生，其引发的[饥荒和疾病導致該国约三分之二的](../Page/饥荒.md "wikilink")[人口营养不良](../Page/人口.md "wikilink")。

由于冲突以及缺乏基础设施、腐败、通货膨胀等基本问题，外国企业已经减少了在刚果的投资。刚果民主共和国的低公开性、政府的经济政策與金融运作\[31\]以及困难的经营环境，均加剧了法律基礎結構的不确定性。近年来，由于自2002年底起入侵的外国军队大部分撤出，经济条件有所改善，国际货币基金組織和世界银行的特派团已經与該國政府方面接触，帮助其制定一项协调一致的经济计划\[32\]，总统约瑟夫卡比拉已开始实施改革。尽管如此，许多经济活动仍然只能由外部的[GDP数据反映](../Page/GDP.md "wikilink")。根据联合国[人类发展指数报告显示](../Page/人类发展指数.md "wikilink")，刚果民主共和国的人类發展指數是数十年来表现最差的国家之一。

身為非洲第二大国家，刚果民主共和国在[经济上严重依赖于采矿业](../Page/经济.md "wikilink")。然而，许多经济活动发生在非正规部门，不能在国内生产总值数据中實際反映出来。刚果是世界上最大的[钴矿产地](../Page/钴.md "wikilink")，及[铜和工业](../Page/铜.md "wikilink")[钻石的主要产地](../Page/钻石.md "wikilink")。刚果还拥有世界矿石种类的70％，拥有超过的30％世界钻石储量\[33\]，并具有重要的[钽矿床](../Page/钽.md "wikilink")，这是一种用在电脑和手机使用的电子元件制造上的矿物。2002年，该国东部发现[锡矿](../Page/锡.md "wikilink")，但是迄今为止，矿山只得到小规模的开发\[34\]。[走私矿物](../Page/走私.md "wikilink")、矿石和锡石（特别是钽和锡等矿石）的非法开采极其严重，其带来的利益冲突，甚至导致在刚果东部的[战争不断加剧](../Page/战争.md "wikilink")。[加丹加矿业有限公司是一家总部位于](../Page/加丹加.md "wikilink")[伦敦的公司](../Page/伦敦.md "wikilink")，拥有的卢伊卢河冶金厂可年产175,000吨铜、8000吨钴，使它成为世界上最大的钴精炼厂。经过重大的调整计划，该公司的冶金廠分別於2007年12月及2008年5月重新启动铜及钴的精鍊生产。刚果民主共和国也拥有[非洲的](../Page/非洲.md "wikilink")[森林中百分之五十的河流系统](../Page/森林.md "wikilink")\[35\]，可以提供水电到整个大陆，根据[联合国对国家战略意义的报告](../Page/联合国.md "wikilink")，刚果民主共和国在[非洲中部有做為经济大国的潜力](../Page/非洲中部.md "wikilink")\[36\]，不过[清廉指数中的排名为倒数](../Page/清廉指数.md "wikilink")20名。

### 農業

剛果民主共和國是一個農業國家，農業發展具有很大潛力。由於境內氣候溫濕，土地肥沃，因此農產品十分豐富，大多數為熱帶農作物，主要的農產品有[棕櫚樹](../Page/棕櫚樹.md "wikilink")，其所產製的[棕櫚油佔重要地位](../Page/棕櫚油.md "wikilink")。此外也盛產[咖啡](../Page/咖啡.md "wikilink")、[可可](../Page/可可.md "wikilink")、[椰子](../Page/椰子.md "wikilink")、[棉花](../Page/棉花.md "wikilink")、[菸葉](../Page/菸葉.md "wikilink")、[稻米](../Page/稻米.md "wikilink")、[蔬菜](../Page/蔬菜.md "wikilink")、[鳳梨](../Page/鳳梨.md "wikilink")、[玉蜀黍](../Page/玉蜀黍.md "wikilink")、[甘蔗](../Page/甘蔗.md "wikilink")、[落花生](../Page/落花生.md "wikilink")、[橡膠](../Page/橡膠.md "wikilink")、[香蕉和](../Page/香蕉.md "wikilink")[茶葉等](../Page/茶葉.md "wikilink")。剛果民主共和國有1329707平方公里的森林，森林覆蓋率約58.7%，森林面積為世界第七大\[37\]。境內熱帶森林到處可見，產有許多良材，原始森林中有許多野獸出沒。剛果民主共和國還產有世界上一些獨有的奇特花草，尤其是熱帶花卉品種極多，十分鮮艷。

### 畜牧業

畜牧業也很發達，多以[牛和](../Page/牛.md "wikilink")[羊為主](../Page/羊.md "wikilink")；飼養[豬](../Page/豬.md "wikilink")、[雞以及](../Page/雞.md "wikilink")[鴨等](../Page/鴨.md "wikilink")，也是一般農家的主要副業。

### 漁業

剛果民主共和國漁業以[淡水魚獲為主](../Page/淡水魚.md "wikilink")，[海水魚獲為輔](../Page/海水魚.md "wikilink")。

### 礦業

剛果民主共和國素以天然資源豐富見稱，在礦產方面包括[鐳](../Page/鐳.md "wikilink")、錫、[鈾](../Page/鈾.md "wikilink")、工業用[鑽石](../Page/鑽石.md "wikilink")、[鋅](../Page/鋅.md "wikilink")、[金](../Page/金.md "wikilink")、[銀](../Page/銀.md "wikilink")、[鋁](../Page/鋁.md "wikilink")、[鐵](../Page/鐵.md "wikilink")、[錳](../Page/錳.md "wikilink")、[鎢和](../Page/鎢.md "wikilink")[煤等](../Page/煤.md "wikilink")，但尚未充分開發。

民主剛果矿产资源极为丰富，素有“原料仓库”之称。据可靠的勘探数据，民主剛果钴和钽的储量居世界第一位，其中，钴的储量占世界总储量的2／3；作为核原料的铀238的储量居世界第一位；钻石储量近2亿克拉，居世界第二位；铜储量为7500万吨，居世界第六位。此外，该国其他[有色金属以及](../Page/有色金属.md "wikilink")[石油](../Page/石油.md "wikilink")、[天然气的储量也相当可观](../Page/天然气.md "wikilink")，森林覆盖率更是高达53％，面积约1.25亿公顷，占非洲热带森林面积的一半。

### 工業

剛果民主共和國工業也有相當發展，目前主要的工业有糖、菸酒、飲料、棕櫚油、[肥皂](../Page/肥皂.md "wikilink")、磨粉、紡織、化學原料、[水泥](../Page/水泥.md "wikilink")、煉鐵、鳳梨[罐頭](../Page/罐頭.md "wikilink")、造船、紙、[機車](../Page/機車.md "wikilink")、[肥料](../Page/肥料.md "wikilink")、印刷、成衣和發電等。由於境內河流遍佈，且多瀑布和急流，因此工業發電所必須的水力十分充足。

## 人口

[Congo_dem_demographie.png](https://zh.wikipedia.org/wiki/File:Congo_dem_demographie.png "fig:Congo_dem_demographie.png")

民主剛果共分有254个[部族](../Page/部族.md "wikilink")，以[班图语系為語言的约占总人口的](../Page/班图语支.md "wikilink")84％，主要为[刚果族](../Page/刚果人.md "wikilink")、[恩加拉族](../Page/恩加拉族.md "wikilink")、[卢巴族](../Page/卢巴族.md "wikilink")、[蒙戈-恩库恩杜族](../Page/蒙戈-恩库恩杜族.md "wikilink")、[隆达族](../Page/隆达族.md "wikilink")、[苏古族等](../Page/苏古族人.md "wikilink")，其余为讲[苏丹语的](../Page/苏丹语.md "wikilink")[阿赞德族和](../Page/阿赞德族.md "wikilink")[尼洛特语系的](../Page/尼洛特语系.md "wikilink")[阿卢尔族和](../Page/阿卢尔族.md "wikilink")[俾格米人等](../Page/俾格米人.md "wikilink")；官方语言为[法语](../Page/法语.md "wikilink")，主要语言有[林加拉语](../Page/林加拉语.md "wikilink")、[基刚果语](../Page/基刚果语.md "wikilink")、[契卢巴语和](../Page/契卢巴语.md "wikilink")[斯瓦希里语](../Page/斯瓦希里语.md "wikilink")。居民中55％信仰[天主教](../Page/天主教.md "wikilink")，35％为[新教](../Page/新教.md "wikilink")，4％信仰原始宗教。

### 主要城市

| 城市                                   | 人口（2008年） |
| ------------------------------------ | --------- |
| [金夏沙](../Page/金夏沙.md "wikilink")     | 7,500,000 |
| [姆布吉馬伊](../Page/姆布吉馬伊.md "wikilink") | 2,500,000 |
| [盧本巴希](../Page/盧本巴希.md "wikilink")   | 1,700,000 |
| [卡南加](../Page/卡南加.md "wikilink")     | 1,400,000 |
| [基桑加尼](../Page/基桑加尼.md "wikilink")   | 1,200,000 |
| [科盧韋齊](../Page/科盧韋齊.md "wikilink")   | 1,100,000 |
| [姆班達卡](../Page/姆班達卡.md "wikilink")   | 850,000   |
| [利卡西](../Page/利卡西.md "wikilink")     | 600,000   |
| [波馬](../Page/波馬.md "wikilink")       | 600,000   |

## 國際貿易

### 出口貿易

在對外貿方面，剛果民主共和國2007年出口值61億美元\[38\]，主要輸出品有銅、鈷、工業鑽石、咖啡和木材等，以[中國](../Page/中國.md "wikilink")、[比利時](../Page/比利時.md "wikilink")、[芬蘭](../Page/芬蘭.md "wikilink")、[美國和](../Page/美國.md "wikilink")[尚比亞為主要輸出對象](../Page/尚比亞.md "wikilink")\[39\]。

### 進口貿易

剛果民主共和國2007年輸入總值52億美元\[40\]，主要輸入品以石油、食品、金屬製品、紡織品、化學品、交通器材、電器和電力設施等為主，主要輸入國是[南非](../Page/南非.md "wikilink")、比利時、尚比亞、[辛巴威](../Page/辛巴威.md "wikilink")、中國、[肯亞](../Page/肯亞.md "wikilink")、[法國等](../Page/法國.md "wikilink")\[41\]。

## 行政区劃

[Provinces_de_la_République_démocratique_du_Congo_-_2005.svg](https://zh.wikipedia.org/wiki/File:Provinces_de_la_République_démocratique_du_Congo_-_2005.svg "fig:Provinces_de_la_République_démocratique_du_Congo_-_2005.svg")

民主剛果全国划分为26个[省](../Page/省.md "wikilink")（Province）。刚独立时全国划分为8个省，1972年7月，省改为区（）；1996年前后基伍区划分为马尼埃马、南基伍和北基伍三区。

1997年5月17日，[洛朗·卡比拉政府成立后](../Page/洛朗·卡比拉.md "wikilink")，国家一级区划“区”改称为“省”，并将三省改名：下扎伊尔（Bas-Zaïre）更名为下刚果（Bas-Congo）；上扎伊尔（Haut-Zaïre）更名为上刚果（Haut-Congo），旋再改东方（Orientale）；沙巴更名为加丹加（Katanga）。當時的11個省：

  - [班顿杜](../Page/班顿杜省.md "wikilink")（Bandundu）：人口数5,201,000人，面积295,658平方公里，首府[班顿杜](../Page/班顿杜.md "wikilink")（Bandundu）。
  - [下刚果](../Page/下刚果.md "wikilink")（Bas-Congo）：人口数2,835,000人，面积53,920平方公里，首府[马塔迪](../Page/马塔迪.md "wikilink")（Matadi）。
  - [赤道](../Page/赤道省.md "wikilink")（Équateur）：人口数4,820,000，面积403,292平方公里，首府[姆班达卡](../Page/姆班达卡.md "wikilink")（Mbandaka）。
  - [西开赛](../Page/西开赛省.md "wikilink")（Kasai
    Occidental）：人口数3,337,000人，面积154,742平方公里，首府[卡南加](../Page/卡南加.md "wikilink")（Kananga）。
  - [东开赛](../Page/东开赛省_\(旧省\).md "wikilink")（Kasai
    Oriental）：人口数3,830,000，人面积170,302平方公里，首府[姆布吉马伊](../Page/姆布吉马伊.md "wikilink")（Mbuji-Mayi）。
  - [加丹加](../Page/加丹加.md "wikilink")（Katanga）：人口数4,125,000人，面积496,877平方公里，首府[卢本巴希](../Page/卢本巴希.md "wikilink")。
  - [金沙萨直辖市](../Page/金夏沙.md "wikilink")：人口数6,541,300人，面积9,965平方公里。
  - [马尼埃马](../Page/马尼埃马.md "wikilink")（Maniema）：人口数1,246,787人，面积132,250平方公里，首府[金杜](../Page/金杜.md "wikilink")（Kindu）。
  - [北基伍](../Page/北基伍.md "wikilink")（Nord-Kivu）：人口数3,564,434人，面积59,483平方公里，首府[戈马](../Page/戈马.md "wikilink")（Goma）。
  - [东方](../Page/东方省.md "wikilink")（Orientale）：人口数5,566,000人，面积503,239平方公里，首府[基桑加尼](../Page/基桑加尼.md "wikilink")。
  - [南基伍](../Page/南基伍.md "wikilink")（Sud-Kivu）：人口数2,837,779人，面积65,070平方公里，首府[布卡武](../Page/布卡武.md "wikilink")（Bukavu）。

2005年剛果民主共和國又根据新宪法，將全國劃為26个省。

<table>
<tbody>
<tr class="odd">
<td><table>
<thead>
<tr class="header">
<th></th>
<th><p>省</p></th>
<th><p>首府</p>
</tr></th>
<th><p>1.</p></th>
<th><p><a href="../Page/金沙萨.md" title="wikilink">金沙萨</a></p></th>
<th><p><a href="../Page/金沙萨.md" title="wikilink">金沙萨</a></p>
</tr></th>
<th><p>2.</p></th>
<th><p><a href="../Page/中刚果省.md" title="wikilink">中刚果省</a></p></th>
<th><p><a href="../Page/马塔迪.md" title="wikilink">马塔迪</a></p>
</tr></th>
<th><p>3.</p></th>
<th><p><a href="../Page/宽果省.md" title="wikilink">宽果省</a></p></th>
<th><p><a href="../Page/根盖.md" title="wikilink">根盖</a></p>
</tr></th>
<th><p>4.</p></th>
<th><p><a href="../Page/奎卢省.md" title="wikilink">奎卢省</a></p></th>
<th><p><a href="../Page/基奎特.md" title="wikilink">基奎特</a></p>
</tr></th>
<th><p>5.</p></th>
<th><p><a href="../Page/马伊恩东贝省.md" title="wikilink">马伊恩东贝省</a></p></th>
<th><p><a href="../Page/伊农戈.md" title="wikilink">伊农戈</a></p>
</tr></th>
<th><p>6.</p></th>
<th><p><a href="../Page/开赛省.md" title="wikilink">开赛省</a></p></th>
<th><p><a href="../Page/鲁耶波.md" title="wikilink">鲁耶波</a></p>
</tr></th>
<th><p>7.</p></th>
<th><p><a href="../Page/卢卢阿省.md" title="wikilink">卢卢阿省</a></p></th>
<th><p><a href="../Page/卡南加.md" title="wikilink">卡南加</a></p>
</tr></th>
<th><p>8.</p></th>
<th><p><a href="../Page/东开赛省.md" title="wikilink">东开赛省</a></p></th>
<th><p><a href="../Page/姆布吉马伊.md" title="wikilink">姆布吉马伊</a></p>
</tr></th>
<th><p>9.</p></th>
<th><p><a href="../Page/洛马米省.md" title="wikilink">洛马米省</a></p></th>
<th><p><a href="../Page/卡宾达.md" title="wikilink">卡宾达</a></p>
</tr></th>
<th><p>10.</p></th>
<th><p><a href="../Page/桑库鲁省.md" title="wikilink">桑库鲁省</a></p></th>
<th><p><a href="../Page/洛贾.md" title="wikilink">洛贾</a></p>
</tr></th>
<th><p>11.</p></th>
<th><p><a href="../Page/马涅马省.md" title="wikilink">马涅马省</a></p></th>
<th><p><a href="../Page/金杜.md" title="wikilink">金杜</a></p>
</tr></th>
<th><p>12.</p></th>
<th><p><a href="../Page/南基伍省.md" title="wikilink">南基伍省</a></p></th>
<th><p><a href="../Page/布卡武.md" title="wikilink">布卡武</a></p>
</tr></th>
<th><p>13.</p></th>
<th><p><a href="../Page/北基伍省.md" title="wikilink">北基伍省</a></p></th>
<th><p><a href="../Page/戈马.md" title="wikilink">戈马</a></p>
</tr></th>
</tr>
</thead>
<tbody>
</tbody>
</table></td>
<td><table>
<thead>
<tr class="header">
<th></th>
<th><p>省</p></th>
<th><p>首府</p>
</tr></th>
<th><p>14.</p></th>
<th><p><a href="../Page/伊图利省.md" title="wikilink">伊图利省</a></p></th>
<th><p><a href="../Page/布尼亚.md" title="wikilink">布尼亚</a></p>
</tr></th>
<th><p>15.</p></th>
<th><p><a href="../Page/上韦莱省.md" title="wikilink">上韦莱省</a></p></th>
<th><p><a href="../Page/伊西罗.md" title="wikilink">伊西罗</a></p>
</tr></th>
<th><p>16.</p></th>
<th><p><a href="../Page/乔波省.md" title="wikilink">乔波省</a></p></th>
<th><p><a href="../Page/基桑加尼.md" title="wikilink">基桑加尼</a></p>
</tr></th>
<th><p>17.</p></th>
<th><p><a href="../Page/下韦莱省.md" title="wikilink">下韦莱省</a></p></th>
<th><p><a href="../Page/布塔.md" title="wikilink">布塔</a></p>
</tr></th>
<th><p>18.</p></th>
<th><p><a href="../Page/北乌班吉省.md" title="wikilink">北乌班吉省</a></p></th>
<th><p><a href="../Page/戈巴多莱.md" title="wikilink">戈巴多莱</a></p>
</tr></th>
<th><p>19.</p></th>
<th><p><a href="../Page/蒙加拉省.md" title="wikilink">蒙加拉省</a></p></th>
<th><p><a href="../Page/利萨拉.md" title="wikilink">利萨拉</a></p>
</tr></th>
<th><p>20.</p></th>
<th><p><a href="../Page/南乌班吉省.md" title="wikilink">南乌班吉省</a></p></th>
<th><p><a href="../Page/盖梅纳.md" title="wikilink">盖梅纳</a></p>
</tr></th>
<th><p>21.</p></th>
<th><p><a href="../Page/赤道省.md" title="wikilink">赤道省</a></p></th>
<th><p><a href="../Page/姆班达卡.md" title="wikilink">姆班达卡</a></p>
</tr></th>
<th><p>22.</p></th>
<th><p><a href="../Page/楚阿帕省.md" title="wikilink">楚阿帕省</a></p></th>
<th><p><a href="../Page/博恩代.md" title="wikilink">博恩代</a></p>
</tr></th>
<th><p>23.</p></th>
<th><p><a href="../Page/坦噶尼喀省.md" title="wikilink">坦噶尼喀省</a></p></th>
<th><p><a href="../Page/卡莱米.md" title="wikilink">卡莱米</a></p>
</tr></th>
<th><p>24.</p></th>
<th><p><a href="../Page/上洛马米省.md" title="wikilink">上洛马米省</a></p></th>
<th><p><a href="../Page/卡米纳.md" title="wikilink">卡米纳</a></p>
</tr></th>
<th><p>25.</p></th>
<th><p><a href="../Page/卢阿拉巴省.md" title="wikilink">卢阿拉巴省</a></p></th>
<th><p><a href="../Page/科卢韦齐.md" title="wikilink">科卢韦齐</a></p>
</tr></th>
<th><p>26.</p></th>
<th><p><a href="../Page/上加丹加省.md" title="wikilink">上加丹加省</a></p></th>
<th><p><a href="../Page/卢本巴希.md" title="wikilink">卢本巴希</a></p>
</tr></th>
</tr>
</thead>
<tbody>
</tbody>
</table></td>
</tr>
</tbody>
</table>

## 艺术

在[金夏沙美术学院](../Page/金夏沙美术学院.md "wikilink")，有多国的艺术家在此教授艺术。著名艺术家亦任教于金沙萨美术学院。他开设的私人画室，是许多在非洲华人的常临之所。其他的知名艺术家有画家Lema
Kusa、Roger Botembe、Claudy Khan、Mavinga Nshole等；雕刻家Alfred Liyolo、Freddy
Tsimba等。

## 注释

## 参考文献

## 外部链接

  - 政府

<!-- end list -->

  - [Chief of State and Cabinet
    Members](https://web.archive.org/web/20090506023504/https://www.cia.gov/library/publications/world-leaders-1/world-leaders-c/congo-democratic-republic-of-the.html)

<!-- end list -->

  - 概况

<!-- end list -->

  - [Country
    Profile](http://news.bbc.co.uk/2/hi/africa/country_profiles/1076399.stm)
    from the [BBC News](../Page/BBC_News.md "wikilink")

  -
  - [Democratic Republic of the
    Congo](https://web.archive.org/web/20080727023932/http://ucblibraries.colorado.edu/govpubs/for/DRCongo.htm)
    from *UCB Libraries GovPubs*

  -
  -
  - [Democratic Republic of
    Congo](http://world.wikia.com/wiki/Democratic_Republic_of_Congo) at
    WorldWikia

<!-- end list -->

  - 人道危机

<!-- end list -->

  - [Crisis briefing on
    Congo](http://members.alertnet.org/db/crisisprofiles/ZR_CON.htm?v=in_detail)
    from [Reuters AlertNet](../Page/Reuters_AlertNet.md "wikilink")
  - [Humanitarian information coverage on
    ReliefWeb](http://www.reliefweb.int/rw/dbc.nsf/doc104?OpenForm&rc=1&cc=cod)
  - [The Democratic Republic of Congo from Global
    Issues](http://www.globalissues.org/article/87/the-democratic-republic-of-congo)

<!-- end list -->

  - 旅游

<!-- end list -->

  -
  - [Official Website of Virunga National
    Park](https://web.archive.org/web/20081003023434/http://gorilla.cd/)

<!-- end list -->

  - 有关冲突的新闻报道

<!-- end list -->

  - [BBC DR Congo: Key
    facts](http://news.bbc.co.uk/2/shared/spl/hi/guides/456900/456977/html/nn1page1.stm)
  - [BBC Q\&A: DR Congo
    conflict](http://news.bbc.co.uk/2/hi/africa/3075537.stm)
  - [BBC Timeline: Democratic Republic of
    Congo](http://news.bbc.co.uk/2/hi/africa/country_profiles/1072684.stm)
  - [BBC In pictures: Congo
    crisis](http://news.bbc.co.uk/2/hi/in_pictures/7702602.stm)
  - ["Rape of a Nation" by Marcus Bleasdale (VII) on
    MediaStorm](http://www.mediastorm.org/0022.htm)
  - [Maps of
    Congo](https://web.archive.org/web/20120216011222/http://www.stiopka.com/cartographie.html)
    before and after independence

<!-- end list -->

  - 其他

<!-- end list -->

  - [Living in Congo by Myron
    Buck](http://www.getlostmagazine.com/features/2004/0401congo/congo1.html)

## 参见

  - [刚果](../Page/刚果.md "wikilink")
  - [比属刚果](../Page/比属刚果.md "wikilink")、[扎伊尔](../Page/扎伊尔.md "wikilink")
  - [刚果共和国](../Page/刚果共和国.md "wikilink")（[布拉柴维尔](../Page/布拉柴维尔.md "wikilink")）

{{-}}

[剛果民主共和國](../Category/剛果民主共和國.md "wikilink")
[Category:中非](../Category/中非.md "wikilink")
[Category:非洲国家](../Category/非洲国家.md "wikilink")
[Category:大西洋國家](../Category/大西洋國家.md "wikilink")
[Category:民主共和国](../Category/民主共和国.md "wikilink")
[Category:前比利時殖民地](../Category/前比利時殖民地.md "wikilink")
[Category:法語國家地區](../Category/法語國家地區.md "wikilink")
[Category:班圖語國家地區](../Category/班圖語國家地區.md "wikilink")
[Category:斯瓦希里語國家地區](../Category/斯瓦希里語國家地區.md "wikilink")
[Category:1960年建立的國家或政權](../Category/1960年建立的國家或政權.md "wikilink")

1.

2.  ["Zaire: Post-Independent Political
    Development"](http://lcweb2.loc.gov/cgi-bin/query/r?frd/cstdy:@field\(DOCID+zr0146\)),
    Library of Congress

3.  See ["Rumblings of war in heart of
    Africa"](http://www.csmonitor.com/2004/0623/p01s04-woaf.html) by
    Abraham McLaughlin and Duncan Woodside *The Christian Science
    Monitor* 23 June 2004 and ["World War
    Three"](http://www.mydd.com/story/2006/7/24/135222/827)  by Chris
    Bowers *My Direct Democracy* 24 July 2006

4.

5.

6.

7.  [Report on the
    crisis](http://www.rescue.org/special-reports/congo-forgotten-crisis)
    Full report at [IRC Mortality
    Facts](http://www.rescue.org/sites/default/files/resource-file/IRC_DRCMortalityFacts.pdf)
    from the [International Rescue
    Committee](../Page/International_Rescue_Committee.md "wikilink")

8.  兩國1960年加入聯合國決議案，英語國名分別為The Republic of the Congo
    (Leopoldville)（Leopoldville為金沙薩舊名利奥波德维尔）和The Republic of the Congo
    (Brazzaville)。[UN admission
    resolutions](http://www.un.org/documents/ga/res/15/ares15.htm)

9.  ["Zaire: Post-Independent Political
    Development"](http://lcweb2.loc.gov/cgi-bin/query/r?frd/cstdy:@field\(DOCID+zr0146\)),
    Library of Congress

10. See ["Rumblings of war in heart of
    Africa"](http://www.csmonitor.com/2004/0623/p01s04-woaf.html) by
    Abraham McLaughlin and Duncan Woodside *The Christian Science
    Monitor* 23 June 2004 and ["World War
    Three"](http://www.mydd.com/story/2006/7/24/135222/827)  by Chris
    Bowers *My Direct Democracy* 24 July 2006

11.

12.

13.

14. [Report on the
    crisis](http://www.rescue.org/special-reports/congo-forgotten-crisis)
    Full report at [IRC Mortality
    Facts](http://www.rescue.org/sites/default/files/resource-file/IRC_DRCMortalityFacts.pdf)
    from the [International Rescue
    Committee](../Page/International_Rescue_Committee.md "wikilink")

15.

16.

17. 改寫自《非洲列國誌》--賴比瑞亞，頁223,1981年出版

18.
19.

20.

21.

22.

23.

24. [联合国秘书长吁刚果（金）总统按约去职](http://www.xinhuanet.com/world/2018-01/03/c_129781160.htm).新華網.\[2017-01-03\].

25.

26. [總統大選推遲引示威遊行
    至少8人死亡、剛果政府宣布無限期關閉網路](https://cnews.com.tw/002180102-03/).匯流新聞網.\[2018-01-02\].

27.

28.

29.

30. Elle“[pouvait se prévaloir](http://www.societecivile.cd/node/4243)

31. [Dublin - Research and
    Markets](http://www.researchandmarkets.com/research/e074e9/democratic_republi)

32.

33. "[DR Congo poll crucial for
    Africa](http://news.bbc.co.uk/2/hi/africa/5209428.stm)". BBC News.
    November 16, 2006.

34.

35.

36. [DR Congo economic and strategic
    significance](http://www.stripes.com/article.asp?section=104&article=69718)

37.

38.

39.

40.

41.