由[长春电影制片厂于](../Page/长春电影制片厂.md "wikilink")1974年摄制，是一部反映石油工人奋斗精神的电影。影片摄制于[中华人民共和国](../Page/中华人民共和国.md "wikilink")[文化大革命时期](../Page/文化大革命.md "wikilink")，描述[中华人民共和国成立后的二十世纪五](../Page/中华人民共和国.md "wikilink")、六十年代中国石油工人发扬“[铁人](../Page/王进喜.md "wikilink")”精神，克服重重困难，使中国实现原油自给的事迹。是“文革”中摄制的为数不多的[故事片之一](../Page/故事片.md "wikilink")，上映后引起了[中华人民共和国全国性轰动](../Page/中华人民共和国.md "wikilink")。

该片的拍摄和上映过程十分曲折，由於反映了“文革”前石油工業的成就，遭到[江青的責難](../Page/江青.md "wikilink")，[毛泽东曾亲自就该片作批示](../Page/毛泽东.md "wikilink")「此片無大錯，建議通過發行。」\[1\]

## 剧情梗概

## 制作人员

  - 编剧：大庆 长影《创业》创作组集体创作，[张天民执笔](../Page/张天民.md "wikilink")。
  - 导演：[于彦夫](../Page/于彦夫.md "wikilink")
  - 摄影：[王雷](../Page/王雷.md "wikilink")
  - 美术：[王崇](../Page/王崇.md "wikilink")
  - 作曲：[秦咏诚](../Page/秦咏诚.md "wikilink")
  - 演奏 演唱：[长影乐团](../Page/长影乐团.md "wikilink")
    [辽宁省歌舞团](../Page/辽宁省歌舞团.md "wikilink")
  - 独唱：[边桂荣](../Page/边桂荣.md "wikilink")
  - 指挥：[尹升山](../Page/尹升山.md "wikilink")
  - [大庆油田](../Page/大庆油田.md "wikilink")
    [玉门石油管理局](../Page/玉门石油管理局.md "wikilink")
    [中国人民解放军](../Page/中国人民解放军.md "wikilink")[兰州部队](../Page/兰州军区.md "wikilink")、[沈阳部队协助拍摄](../Page/沈阳军区.md "wikilink")

## 主要演员

  - [张连文](../Page/张连文.md "wikilink") 饰 周挺杉
  - [李仁堂](../Page/李仁堂.md "wikilink") 饰 华程
  - [陈　颖](../Page/陈颖.md "wikilink") 饰 章易之
  - [朱德承](../Page/朱德承.md "wikilink") 饰 油娃
  - [宫喜斌](../Page/宫喜斌.md "wikilink") 饰 秦发奋
  - [章　杰](../Page/章杰.md "wikilink") 饰 许光发
  - [王者兰](../Page/王者兰.md "wikilink") 饰 姚云明
  - [邵德兴](../Page/邵德兴.md "wikilink") 饰 范师傅
  - [迟志强](../Page/迟志强.md "wikilink") 饰 魏国华
  - [欧阳如秋](../Page/欧阳如秋.md "wikilink") 饰 周大娘
  - [李　瑛](../Page/李瑛.md "wikilink") 饰 陈淑芬
  - [张冲霄](../Page/张冲霄.md "wikilink") 饰 老周师傅
  - [浦　克](../Page/浦克.md "wikilink") 饰 田大爷
  - [刘庆生](../Page/刘庆生.md "wikilink") 饰 赵春生
  - [刘树纲](../Page/刘树纲.md "wikilink") 饰 王副指挥
  - [刘国祥](../Page/刘国祥.md "wikilink") 饰 冯超
  - [庞万灵](../Page/庞万灵.md "wikilink") 饰 美国顾问

## 参考文献

## 外部連結

  -
  -
  -
## 参见

  - [中华人民共和国石油工业部](../Page/中华人民共和国石油工业部.md "wikilink")
  - [玉门油田](../Page/玉门油田.md "wikilink")
  - [大庆油田](../Page/大庆油田.md "wikilink")
      - [大庆铁人第一口油井](../Page/大庆铁人第一口油井.md "wikilink")
      - [五面红旗](../Page/五面红旗.md "wikilink")
          - [王进喜](../Page/王进喜.md "wikilink")

[Category:1974年电影](../Category/1974年电影.md "wikilink")
[Category:1974年中国作品](../Category/1974年中国作品.md "wikilink")
[Category:1970年代中国电影作品](../Category/1970年代中国电影作品.md "wikilink")
[Category:黑龙江背景电影](../Category/黑龙江背景电影.md "wikilink")
[C创](../Category/中國劇情片.md "wikilink")
[Category:中华人民共和国石油工业](../Category/中华人民共和国石油工业.md "wikilink")
[Category:大庆油田](../Category/大庆油田.md "wikilink")
[Category:长春电影制片厂电影](../Category/长春电影制片厂电影.md "wikilink")

1.  [1975年2月11日
    毛澤東支持電影《創業》公映](http://cpc.people.com.cn/GB/64162/64165/77552/77563/5327972.html).
    中國共產黨新聞