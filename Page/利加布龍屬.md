**利加布龍屬**（[學名](../Page/學名.md "wikilink")：*Ligabuesaurus*）是[蜥腳下目](../Page/蜥腳下目.md "wikilink")[泰坦巨龍類](../Page/泰坦巨龍類.md "wikilink")[恐龍的一屬](../Page/恐龍.md "wikilink")，生存於[白堊紀早期](../Page/白堊紀.md "wikilink")（約1億2500萬至1億年前）的[阿根廷](../Page/阿根廷.md "wikilink")。[模式種為](../Page/模式種.md "wikilink")**黎氏利加布龍**（*L.
leanzai*），是由[約瑟·波拿巴](../Page/約瑟·波拿巴.md "wikilink")（Jose
Bonaparte）、Gonzalez
Riga與[塞巴斯蒂安·阿派斯特圭](../Page/塞巴斯蒂安·阿派斯特圭.md "wikilink")（Sebastián
Apesteguía）在2006年根據一個部份身體骨骼所描述、命名的。種名是以發現化石的地質學家Héctor A. Leanza為名。

## 參考資料

  - José F. Bonaparte, Bernardo J. González Riga and Sebastián
    Apesteguía 2006. *Ligabuesaurus leanzai* gen. et sp. nov.
    (Dinosauria, Sauropoda), a new titanosaur from the Lohan Cura
    Formation (Aptian, Lower Cretaceous) of Neuquén, Patagonia,
    Argentina. Cretaceous Research 27(3): 364-376.
  - [Dinosaur Mailing List entry which announces the discovery (includes
    complete abstract)](http://dml.cmnh.org/2006May/msg00104.html)

[Category:泰坦巨龍類](../Category/泰坦巨龍類.md "wikilink")
[Category:南美洲恐龍](../Category/南美洲恐龍.md "wikilink")
[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")