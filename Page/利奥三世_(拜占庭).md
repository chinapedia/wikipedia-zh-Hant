**伊苏里亚的利奥三世**
（，，又譯**李奧三世**，約），717年建立[伊苏里亚王朝](../Page/伊苏里亚王朝.md "wikilink")，717年至741年在位為[东罗马帝国皇帝](../Page/东罗马帝国.md "wikilink")。

## 早年

利奥三世原是有着叙利亚血统的[小亚细亚牧民](../Page/小亚细亚.md "wikilink")，家乡为马拉什，通[阿拉伯语和](../Page/阿拉伯语.md "wikilink")[希腊语](../Page/希腊语.md "wikilink")。早年曾協助[查士丁尼二世復位](../Page/查士丁尼二世.md "wikilink")，向政府进献多頭生畜，得到賞識而获得官职，开始了他的政治生涯，官至[安纳托利亚军区司令](../Page/安纳托利亚.md "wikilink")\[1\]。

## 與阿拉伯關係

716年，利奥三世自立為王，但實際上仍未成為[拜占廷的真正統治者](../Page/拜占廷.md "wikilink")，[君士坦丁堡仍然在](../Page/君士坦丁堡.md "wikilink")[狄奥多西三世的統治下](../Page/狄奥多西三世_\(拜占庭\).md "wikilink")，因此利奧三世勾結[阿拉伯勢力](../Page/阿拉伯.md "wikilink")，接受阿拉伯的條件，以爭取其支持，聯合出兵攻佔君士坦丁堡，717年利奧三世率軍進入君士坦丁堡，正式揭開伊苏里亚王朝的序幕。但利奧三世建國後，並沒有履行對阿拉伯的承諾，阿拉伯因此率兵攻擊君士坦丁堡，阿拉伯兵的[陸軍及](../Page/陸軍.md "wikilink")[海軍分別取道](../Page/海軍.md "wikilink")[小亞細亞及](../Page/小亞細亞.md "wikilink")[博斯普魯斯海峽](../Page/博斯普魯斯海峽.md "wikilink")\[2\]進攻。阿拉伯圍城近一年後，利奥三世借用新武器[希臘火的威力](../Page/希臘火.md "wikilink")\[3\]，終于718年击败[阿拉伯军队](../Page/阿拉伯.md "wikilink")，暂时抑制阿拉伯的威脅。

## 破坏圣像运动

利奧三世執政期間，阿拉伯人佔領了帝國大部分的領土，令佔領地內的[基督教](../Page/基督教.md "wikilink")[修士回流到东罗马帝国境內](../Page/修士.md "wikilink")，導致境內的修士人數大增。修士及教會在帝國境內一向享有眾多的特權\[4\]，加上教會以拜祭聖像等名目搜括人民資產，令宗教階層成為當時社會的一大負擔。於是利奧三世在726-730年間\[5\]，兩度宣佈反對供奉聖像的詔令，是為破坏圣像运动，同時沒收教會的田產及土地，逼令修士還俗，從而減輕社會負擔，[教皇](../Page/教皇.md "wikilink")[圣格列高利二世對此表達強烈抗議](../Page/额我略二世.md "wikilink")，727年，[拉文納更因此脫離帝國的統治](../Page/拉文納.md "wikilink")，不過都無阻破坏圣像运动的進行，破坏圣像运动大大打擊了基督教會的勢力，最重要的是利奧三世將沒收了的田產及土地，分配給軍隊和貴族，從而堅固了利奧三世的統治。

## 內政

利奧三世在位期間，重新編修了[查士丁尼一世的法典](../Page/查士丁尼一世.md "wikilink")，是為埃克洛加(Ecloga)\[6\]，當中涵蓋了婚禮、遺囑、奴隸及私有制等[法律內容](../Page/法律.md "wikilink")，同時在法典中強調君權的重要性\[7\]，鞏固了伊蘇里亞王朝的統治。

## 參考文獻

<div class="references-small">

<references />

</div>

  - 圖說天下·世界歷史系列編委會 (2009)，《海盜傳奇》，長春：吉林出版集團

[Category:拜占庭皇帝](../Category/拜占庭皇帝.md "wikilink")
[Category:罹患水腫病逝世者](../Category/罹患水腫病逝世者.md "wikilink")

1.  [第七十七位 利奥三世](http://tech.163.com/06/0820/11/2OVD5CEO0009205I.html)
2.   Guilland, Rodolphe. "L'expédition de Maslama contre Constantinople
    (717 - 718)" in *Études Byzantines*. Paris: Presses universitaires
    de France, 1959, pp. 109-133.
3.  Treadgold. *History of the Byzantine State*, p. 347.
4.  [从拜占廷帝国毁坏圣像运动中期的变化看皇权的加强](http://cbs.nku.cn/info/8.htm)
5.  Cf. (ed.) F. GIOIA, *The Popes - Twenty Centuries of History*,
    Libreria Editrice Vaticana (2005), p. 40.
6.  [拜占庭与西欧中世纪:詹姆斯·霍华德-约翰斯通教授的拜占庭史观](http://cbs.nku.cn/info/50.htm)
7.  [LEO III AND THE BEGINNINGS OF
    ICONOCLASM](http://isthmia.osu.edu/teg/50501/20.htm)