[教宗](../Page/教宗.md "wikilink")**良十三世**\[1\]（，），本名**文森佐·焦阿基諾·拉斐爾·路易吉·佩西**（），於1878年2月20日至1903年7月20日出任教宗。

[Coat_of_arms_of_Pope_Leo_XIII.svg](https://zh.wikipedia.org/wiki/File:Coat_of_arms_of_Pope_Leo_XIII.svg "fig:Coat_of_arms_of_Pope_Leo_XIII.svg")\]\]
[TOMB_LEO_xiii.jpg](https://zh.wikipedia.org/wiki/File:TOMB_LEO_xiii.jpg "fig:TOMB_LEO_xiii.jpg")的紀念碑和陵墓\]\]

## 参考文献

{{-}}

[L](../Category/教宗.md "wikilink")
[Category:義大利出生的教宗](../Category/義大利出生的教宗.md "wikilink")

1.  譯名：良十三世：[天主教香港教區禮儀委員會：禧年專頁](http://catholic-dlc.org.hk/2000.htm)、[香港天主教教區檔案　歷任教宗](http://archives.catholic.org.hk/popes/index.htm)
    作良。利奧十三世：[《大英簡明百科知識庫》2005年版](http://wordpedia.britannica.com/concise/quicksearch.aspx?keyword=leo&mode=3)作利奧。利奧十三世、列奧十三世或萊奧十三世：《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作利奧。雷歐十三世：[國立編譯舘作雷歐](../Page/國立編譯舘.md "wikilink")。