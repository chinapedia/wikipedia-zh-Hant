**利昂·库珀**（**Leon
Cooper**，1930年2月28日[纽约](../Page/纽约.md "wikilink")），[美国](../Page/美国.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")，[布朗大學物理系教授](../Page/布朗大學.md "wikilink")。1972年，因為與[約翰·巴丁](../Page/約翰·巴丁.md "wikilink")、[约翰·施里弗聯合創立了](../Page/约翰·施里弗.md "wikilink")[超導微觀理論](../Page/超導.md "wikilink")，即常說的[BCS理論](../Page/BCS理論.md "wikilink")，共同榮获[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")。

## 参考资料

  - [诺贝尔官方网站关于利昂·库珀简介](http://nobelprize.org/nobel_prizes/physics/laureates/1972/cooper-bio.html)

[C](../Category/1930年出生.md "wikilink")
[C](../Category/美国物理学家.md "wikilink")
[C](../Category/诺贝尔物理学奖获得者.md "wikilink")
[C](../Category/美國諾貝爾獎獲得者.md "wikilink")
[C](../Category/猶太諾貝爾獎獲得者.md "wikilink")
[C](../Category/哥倫比亞大學校友.md "wikilink")