[缩略图](https://zh.wikipedia.org/wiki/File:Flag_of_Levi.svg "fig:缩略图")

## 譯名

**利未**(，[提比利安發音](../Page/提比利安發音.md "wikilink")：**Lēwî**；英文：**Levi**，天主教譯：**肋未**)是[以色列](../Page/以色列.md "wikilink")[利未支派的祖先](../Page/利未支派.md "wikilink")，他是[雅各和](../Page/雅各.md "wikilink")[利亚的第三个儿子](../Page/利亚.md "wikilink")。

## 名字意義

**利未**的意思是“聯合”。母親利亞相信第三個兒子的降生將使她與丈夫聯合，將丈夫的心從[拉結身上奪回](../Page/拉結.md "wikilink")。

## 作爲

[圣经描写利未性情暴烈](../Page/圣经.md "wikilink")，当[示剑](../Page/示剑.md "wikilink")[玷污了利未的妹妹](../Page/玷污.md "wikilink")[底拿](../Page/底拿.md "wikilink")，示剑人以此脅迫求婚，利未和哥哥[西缅表面上同意了婚事](../Page/西缅.md "wikilink")，条件是所有的男丁都受[割礼](../Page/割礼.md "wikilink")。示剑人同意了。但第三天，西缅和利未趁着众人正在疼痛的时候，率領部落把示剑一切男丁都杀了，其後把底拿从示剑家里带出来走了。(创世纪34章)

他參與謀害其同父异母弟弟[约瑟](../Page/約瑟_\(舊約聖經\).md "wikilink")。

雅各临终前的祝福：西緬和利未是弟兄；他們的刀劍是殘忍的器具。我的靈啊，不要與他們同謀；我的心哪，不要與他們聯絡；因為他們趁怒殺害人命，任意砍斷牛腿大筋。他們的怒氣暴烈可咒；他們的忿恨殘忍可詛。我要使他們分居在雅各家裏，散住在以色列地中。(创世纪49:5)

利未有三个儿子：[革顺](../Page/革顺.md "wikilink")、[哥辖和](../Page/哥辖.md "wikilink")[米拉利](../Page/米拉利.md "wikilink")。按照圣经的記載，利未活了137岁。(出埃及记6:16)

[摩西的祝福](../Page/摩西.md "wikilink")：论利未说，耶和华阿，愿你的[土明和烏陵都在你的虔诚人那里](../Page/烏陵與土明.md "wikilink")；你在玛撒曾试验他，在米利巴水曾与他争论。他论自己的父母说，我没有看见；他不承认自己的弟兄，也不认识自己的儿女；这是因利未人谨守你的话，护卫你的约。他们要将你的典章指教雅各，将你的律法指教以色列；他们要把香焚在你面前，把全牲的[燔祭献在你的坛上](../Page/燔祭.md "wikilink")。耶和华阿，求你赐福给他的能力，悦纳他手中的工作；那些起来攻击他和恨恶他的人，愿你刺透他们的腰，使他们不得再起来。([申命记](../Page/申命记.md "wikilink")33:8-11)

## 支派

利未的后代称为利未支派，被分别出来专门管理[会幕和后来的](../Page/会幕.md "wikilink")[圣殿](../Page/圣殿.md "wikilink")，没有分土地。其中最著名的是摩西，带领几百万犹太人出[埃及](../Page/埃及.md "wikilink")，并在[西乃山从神领受](../Page/西乃山.md "wikilink")[律法和建造会幕的蓝图](../Page/律法.md "wikilink")([出埃及记](../Page/出埃及记.md "wikilink"))。他的哥哥[亚伦是以色列第一位大](../Page/亚伦.md "wikilink")[祭司](../Page/祭司.md "wikilink")，并且只有[亚伦的后代才能做祭司事奉](../Page/亞倫子孫.md "wikilink")[耶和华](../Page/耶和华.md "wikilink")。

## 参见

  - [Levi
    (disambiguation)](../Page/Levi_\(disambiguation\).md "wikilink")
  - [Levy](../Page/Levy.md "wikilink"),
    [Levin](../Page/Levin.md "wikilink"),
    [Lewy](../Page/Lewy.md "wikilink"),
    [Lewi](../Page/Lewi.md "wikilink")
  - [Alexander Levi](../Page/Alexander_Levi.md "wikilink")

{{-}}

[Category:雅各的儿子](../Category/雅各的儿子.md "wikilink")