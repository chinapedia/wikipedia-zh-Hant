《**利未記**》（，**；）是[摩西五经中的第三本](../Page/摩西五经.md "wikilink")。這本書的英文名字（Leviticus）採自[希臘文](../Page/希臘文.md "wikilink")《[七十士譯本](../Page/七十士譯本.md "wikilink")》所用的希臘字利未提綱（Leu·i·ti·kon′）和《[通俗拉丁文本聖經](../Page/通俗拉丁文本聖經.md "wikilink")》的“利未提格斯”（Leviticus）\[1\]。整本書的內容主要是記述有關選自利未族的祭司團所需謹守的一切律例。在[希伯來文聖經裏](../Page/旧约圣经.md "wikilink")，此書的名字取自書中開首的一句，韋依格拉（Wai·yiq·ra’），實際的意思是“他呼叫”\[2\]。後來[猶太人也把此書稱為祭司的手冊](../Page/猶太人.md "wikilink")。\[3\]

[利未人是](../Page/利未人.md "wikilink")[雅各与](../Page/雅各.md "wikilink")[利亚的第三子](../Page/利亚.md "wikilink")[利未的后人](../Page/利未.md "wikilink")，负责[以色列人的祭祀工作](../Page/以色列人.md "wikilink")，不参与分配土地，不算入以色列[十二支派之一](../Page/十二支派.md "wikilink")，利未人對神忠心，被[耶和華揀選作為事奉祂的支派](../Page/耶和華.md "wikilink")。所有的[祭司都屬於利未支派](../Page/祭司.md "wikilink")，他們的工作是協助料理會幕，並向百姓講解律法。

## 关于作者

本書有56次提到：“[耶和華對摩西說](../Page/耶和華.md "wikilink")”，利未记的結語説：“這就是耶和華……吩咐摩西的命令。”（利未记27：34）利未記26：46也有類似的表示。故本書是摩西寫的。
以往用來支持創世記和[出埃及記是由摩西執筆的證據也適用於利未記](../Page/出埃及記.md "wikilink")，因為摩西五經可能原是合成一卷的。

此外，[出埃及記的結束提到摩西按照耶和華的吩咐建好](../Page/出埃及記.md "wikilink")[會幕](../Page/會幕.md "wikilink")，利未記接續出埃及記教導以色列人如何遵守有關[會幕的規矩及條例](../Page/會幕.md "wikilink")。這些律法的頒佈時間和地點，書中詳細地說明是從以色列民安營在[西奈山開始](../Page/西奈山.md "wikilink")。利未記的原文以“此後”作起首語，把它跟前兩本書連接起來。明显的證據是，耶穌及耶和华的其它圣经执笔者時常引用載於利未記的律法和原則，並且表示它們是摩西寫的。\[4\]

## 写作背景

### 写作时间

以色列戰勝[亞瑪力人之後](../Page/亞瑪力人.md "wikilink")，上帝吩咐摩西將他的判決寫下，摩西留意將各事記下。書中的若干資料也顯示它是在頗早的時期寫成的。例如，以色列人奉命把用作肉食的牲畜帶到會幕門口宰殺。這項命令是在設立祭司團之後不久被記錄下來的。許多訓示都是為了指引在曠野上路的以色列人而頒下的。\[5\]

### 主要目的

耶和華定意要揀選一個神聖的國家，一個聖潔的民族，把她分别開來以便事奉他。自[亞伯起](../Page/亞伯.md "wikilink")，上帝的忠僕一直向耶和華獻祭，但從以色列國開始，耶和華才詳細規定有關獻贖罪供物及其他祭物的安排。利未記對這些安排詳加論述，目的是要提醒以色列人罪的嚴重性，使他們知道罪令他們在耶和華眼中變得十分可憎。基督新教的部份觀點認為，這些規條是律法的一部分，它們好像訓蒙的師傅一般把猶太人帶到基督那裏，表明他們需要有一位救主，同時也幫助他們成為一個與世俗保持分離的國家。有關潔淨之例的上帝律法特别能够成就後者的目的。\[6\]

## 主題特色

本書直接的目的就是頒佈上帝的律法和原則，叫以色列人生活得像神的子民，又教導他們如何過潔淨的生活，以及親近這位聖潔的耶和華神。

基督新教的部份觀點認為，利未記記下許多律法條文，對於子民全部的生活都有原則或律例加以管制。另一方面，在[舊約書卷中](../Page/舊約.md "wikilink")，没有一本書像利未記一樣，把在[基督裡的救贖意義表現得那麼清楚](../Page/基督.md "wikilink")。

基督新教的部份觀點認為，這是[舊約表達救贖的說法](../Page/舊約.md "wikilink")；這救贖藉着獻祭的禮儀(特別是贖罪日的禮儀)，清楚勾劃出來。而[耶穌基督降世](../Page/耶穌基督.md "wikilink")，一切獻上成了永遠的贖罪祭，使[新約信徒得以事奉永生上帝](../Page/新約.md "wikilink")，不再需要像以色列人那樣獻祭。

## 主要内容

利未記主要的內容與律法有關，其中有頗大部分且含有預言性。全書大致根據一個主題綱領而發展。它可以分成八個部分，以合乎邏輯的次序排列。

### 獻祭之例

以色列人須藉著祭物才能來到神面前。祭禮主要分五種：[燔祭](../Page/燔祭.md "wikilink")、[素祭](../Page/素祭.md "wikilink")、[平安祭](../Page/平安祭.md "wikilink")、[贖罪祭](../Page/贖罪祭.md "wikilink")、[贖愆祭五種](../Page/贖愆祭.md "wikilink")。希望以色列人能通過這五祭遠離罪惡，更接近上帝。

### 設立祭司團

（覆盖-）。以色列國一個重大的場合——設立祭司團的日子——臨到了。摩西完全依照耶和華的詳細指示而行。“於是亞倫和他兒子行了耶和華藉着[摩西所吩咐的一切事](../Page/摩西.md "wikilink")。”（）經過七天的任職禮之後，會衆得以目睹一件强化信心的奇事發生。當時全會衆都在場。祭司剛獻完祭物。亞倫和摩西為百姓祝福。接着，看哪！“耶和華的榮光就向衆民顯現。有火從耶和華面前出來，在壇上燒盡燔祭和脂油。衆民一見，就都歡呼，俯伏在地。”（）

然而，也有些違背律法的事發生。例如，亞倫的兒子拿答和亞比户擅自獻上耶和華沒有吩咐的凡火。“就有火從耶和華面前出來，把他們燒滅，他們就死在耶和華面前。”（）若要所獻的祭物蒙耶和華悦納，百姓和祭司均必須遵照耶和華的訓示而行。耶和華隨即吩咐祭司在進會幕服務時不可飲[酒](../Page/酒.md "wikilink")，這暗示醉酒可能是促使亞倫的兩個兒子犯罪的原因。

### 潔淨之例

（覆盖-）這部分記載有關禮儀上及[衛生上的潔淨](../Page/衛生.md "wikilink")。某些家畜及野生動物是不潔之物。所有死屍都是不潔的，觸摸它們的人也變成不潔。生孩子會使婦人變成不潔，因此要隔離一段日子，並且獻上特别的祭物。

若干種[皮膚病](../Page/皮膚病.md "wikilink")，例如[痲瘋病](../Page/痲瘋.md "wikilink")，也造成禮儀上的不潔，患者不單要自潔，連他的衣物房舍都要加以清潔。病者亦必須受檢疫隔離。漏症、[行經和](../Page/月经.md "wikilink")[遺精都會使人成為不潔](../Page/遺精.md "wikilink")。這類不潔的人也要隔離，此外復原之後要沐浴或獻祭，或兩件事都要做。

### 贖罪日

（覆盖）[贖罪日在七月初十日](../Page/贖罪日.md "wikilink")。這日百姓要刻苦己心（很可能藉着禁食），並且甚麽工都不可作。
首先祭司要獻上一隻公牛犢，為[亞倫自己和本家的利未人贖罪](../Page/亞倫.md "wikilink")，然後獻上一隻公山羊為其餘的國人贖罪。燒過香之後，大祭司把祭牲的血依次帶進帳幕的至聖所內，將血彈灑在[法櫃上的施恩座前](../Page/法櫃.md "wikilink")。
稍後必須把祭牲的[屍體帶到營外燒去](../Page/屍體.md "wikilink")。此外，在這日也要把一頭活的公山羊在耶和華面前奉獻，祭司要按手在羊頭上，承認以色列人的一切罪孽過犯，然後派人把羊送到曠野去。在此之後，祭司要獻上兩隻公綿羊作燔燒供物，一隻為亞倫和他的家族，另一隻則為以色列全會衆獻上。

### 律例

（覆盖-）聖經在這裏詳列了許多律例，包括禁戒吃血。（）血可以用在祭壇上，但卻不可吃；嚴禁人犯亂倫和獸交等惡行；保護窮人、卑微的人及寄居的外人的規條，吩咐人説：“要愛人如己，我是耶和華。”（）律法使國人在社會和經濟上的[福利受到保障](../Page/福利.md "wikilink")；崇拜[摩洛及行邪術的人會被治死](../Page/摩洛.md "wikilink")，為要防止人陷入屬靈的危險裏。上帝再次强調他的百姓要與列國保持分離：“你們要歸我為聖，因為我——耶和華是聖的，並叫你們與萬民有分别，使你們作我的民。”（）

### 祭司團與節期

（覆盖-）这三章主要論及以色列的正式崇拜：祭司必須遵守的規條，他們在身體上必須符合的條件，結婚的對象，誰可以吃聖物及祭牲必須健全無疵等各項規定。律法制定了三個全國的節期，好使百姓“在耶和華——你們的上帝面前歡樂”。（）這樣，全國可以齊心一意把一切注意力、讚美和崇拜全歸給耶和華，從而加强與他的關係。這些都是向耶和華所守的節期，是一年一度的聖會。[逾越節和](../Page/逾越節.md "wikilink")[無酵餅節於初春舉行](../Page/無酵餅節.md "wikilink")，[五旬節](../Page/五旬節.md "wikilink")（七七節）於春末時分舉行，贖罪日及為期八天的[住棚節](../Page/住棚節.md "wikilink")（收藏節）則在秋季舉行。

第24章所載的是關於在帳幕中所用的油和陳設餅的訓示。在接着所載的事件中，耶和華規定任何人若咒詛“聖名”就必須被人用石頭打死。他跟着定出“以眼還眼，以牙還牙”的懲罰之例。\[7\]

第25章記述有關持續一年的安息，亦即每七年舉行的[安息年](../Page/安息年.md "wikilink")，以及第50年的[禧年之例](../Page/禧年.md "wikilink")。在第50年的禧年裏，要在遍地向一切居民宣告自由，所有在過去49年內賣去或交出的祖業田産均要歸回本家。律法也定出保障窮人和奴僕之例。在聖經的這部分，“七”這個數字再三出現——第七日、第七年、共長七日的節期、共長七週的時期，以及禧年在七個七年之後來臨。

### 服從和忤逆的結果迥異

（覆盖）耶和華在這裏列出服從的獎賞和忤逆的懲罰。他同時指出以色列人若虚心受教，便會享有光明的希望：“我卻要為他們的緣故記念我與他們先祖所立的約。他們的先祖是我在列邦人眼前、從埃及地領出來的，為要作他們的上帝。我是耶和華。”（）

### 其他律例

（覆盖）利未記的最後部分論述有關許願供物之例、頭生的人畜歸於耶和華及[什一捐之例](../Page/什一捐.md "wikilink")。最後來到全書的簡短結語：“這就是耶和華在西奈山為以色列人所吩咐摩西的命令。”（27：34）

## 注释与参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [基督教文藝出版社藏書](http://lib-nt2.hkbu.edu.hk/sca_fb/org/bhk01_cclc.html)
    - 華人基督宗教文獻保存計劃
  - [思高聖經學會藏書](http://lib-nt2.hkbu.edu.hk/sca_fb/org/bhk05_sbofm_2.html)
    - 華人基督宗教文獻保存計劃

### 《利未记》在线译本

#### [犹太教](../Page/犹太教.md "wikilink") 译本

  - [Leviticus at
    Mechon-Mamre](http://www.mechon-mamre.org/e/et/et0301.htm) (Jewish
    Publication Society翻译)
  - [Leviticus (The Living
    Torah)](http://bible.ort.org/books/pentd2.asp?ACTION=displaypage&BOOK=3&CHAPTER=1)
    拉比Aryeh Kaplan的翻译和评论 Ort.org
  - [Vayikra - Levitichius (Judaica
    Press)](http://www.chabad.org/article.asp?aid=8162)
    翻译，附[Rashi的评论](../Page/Rashi.md "wikilink") Chabad.org
  - [ויקרא *Vayikra* -
    Leviticus](http://www.mechon-mamre.org/p/pt/pt0301.htm)
    ([Hebrew](../Page/Hebrew_language.md "wikilink") - English at
    Mechon-Mamre.org)

#### [基督教英文译本](../Page/基督教.md "wikilink")

  - [*在线圣经*
    GospelHall.org](http://www.gospelhall.org/bible/bible.php?passage=Leviticus+1)
    ([英王詹姆士欽定本](../Page/英王詹姆士欽定本.md "wikilink"))
  - [*oremus 圣经浏览器*](http://bible.oremus.org/?ql=56782100)
    ([新修订版圣经](../Page/新修订版圣经.md "wikilink"))
  - [*oremus 圣经浏览器*](http://bible.oremus.org/?ql=56782135) (*英语话*
    新修订版圣经)

#### [基督教中文译本](../Page/基督教.md "wikilink")

  - [利未記](http://www.o-bible.com/cgibin/ob.cgi?version=hb5&book=lev&chapter=1)

  - [利未记](http://www.o-bible.com/cgibin/ob.cgi?version=hgb&book=lev&chapter=1)


### 相关内容

  - [利未记条目](http://www.jewishencyclopedia.com/view.jsp?artid=301&letter=L&search=Leviticus)
    (犹太百科全书)
  - [The Literary Structure of
    Leviticus](http://chaver.com/Torah/The%20Literary%20Structure%20of%20Leviticus%20\(TBH\).pdf?artid=301&letter=L&search=Leviticus)
    (chaver.com)
  - [Leviticus in Skeptic's Annotated
    Bible](http://www.skepticsannotatedbible.com/lev/intro.html)

免费在线图书馆：利未记

  - [BiBIL](https://wwwdbunil.unil.ch/bibil/?MIval=/bi/en/bibilhome&BiMenu=10&BiMain=21&BiTypeRech=5&WINSIZE=50&affiche=2&Submit=1&rech=vedid=2522)

<!-- end list -->

  - [Lego reenactments of key passages of
    Leviticus](http://www.thebricktestament.com/the_law/index.html)

## 参见

  - [利未](../Page/利未.md "wikilink")、[利未支派](../Page/利未支派.md "wikilink")
  - [摩西](../Page/摩西.md "wikilink")、[摩西五经](../Page/摩西五经.md "wikilink")、[摩西律法](../Page/摩西律法.md "wikilink")
  - [安息日](../Page/安息日.md "wikilink")、[禧年](../Page/禧年.md "wikilink")
  - [逾越节](../Page/逾越节.md "wikilink")、[住棚节](../Page/住棚节.md "wikilink")、[五旬节](../Page/五旬节.md "wikilink")
  - [什一税](../Page/什一税.md "wikilink")

[利未记](../Category/利未记.md "wikilink") [3](../Category/摩西五经.md "wikilink")
[3](../Category/被认为是摩西所写的经籍.md "wikilink")
[Category:前7世紀書籍](../Category/前7世紀書籍.md "wikilink")
[Category:前6世紀書籍](../Category/前6世紀書籍.md "wikilink")
[Category:前5世紀書籍](../Category/前5世紀書籍.md "wikilink")
[Category:前4世紀書籍](../Category/前4世紀書籍.md "wikilink")

1.  [这个词的拼写直接照搬自拉丁语，来源于希腊语](http://www.jewishjournal.com/you_tai_ren/item/pentateuch_20100328/)
2.  [第三卷书的名称是Vayyiqra（瓦伊克辣），词义“他呼唤”，确实是这卷书的第一个字](http://www.jewishjournal.com/you_tai_ren/item/the_first_word_and_the_name_of_each_book_20100321/)
3.  [旧约纵览：利未記](http://book.edzx.com/look_book.asp?id=419)  赖特著 。
4.  参看，-；-；-；-；-。
5.  参看；；。
6.  参看；。
7.  参看，