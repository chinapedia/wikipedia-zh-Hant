[Lei_Tung_Estate_Coverway_access_201612.jpg](https://zh.wikipedia.org/wiki/File:Lei_Tung_Estate_Coverway_access_201612.jpg "fig:Lei_Tung_Estate_Coverway_access_201612.jpg")
[Lei_Tung_Estate_Playground_(brighter).jpg](https://zh.wikipedia.org/wiki/File:Lei_Tung_Estate_Playground_\(brighter\).jpg "fig:Lei_Tung_Estate_Playground_(brighter).jpg")
**利東邨**（）是[香港公共屋邨](../Page/香港公共屋邨.md "wikilink")，位於[香港](../Page/香港.md "wikilink")[南區](../Page/南區_\(香港\).md "wikilink")[鴨脷洲東部](../Page/鴨脷洲.md "wikilink")[玉桂山](../Page/玉桂山.md "wikilink")（島上居民多將之簡稱為「東邨」，而將[鴨脷洲邨](../Page/鴨脷洲邨.md "wikilink")[返稱為](../Page/返璞词.md "wikilink")「西邨」）。於1987至1988年分期入伙，其後於2004年6月在[租者置其屋計劃](../Page/租者置其屋.md "wikilink")（第六期甲）把單位出售給所屬租戶，現已成立[業主立案法團](../Page/業主立案法團.md "wikilink")，自2006年起由康業服務有限公司負責屋邨管理。現任區議員為[民主黨](../Page/民主黨_\(香港\).md "wikilink")[羅健熙及](../Page/羅健熙.md "wikilink")[區諾軒](../Page/區諾軒.md "wikilink")。

利東邨是鴨脷洲島上第二個公營房屋發展計劃，建於陡峭山坡之上，佔地15.02公頃，提供約7500伙租住公屋單位，能容納約29000人及1960伙出售居屋單位，能容納約7000人。

## 土地開拓

由於地勢陡峭，所以要開拓數個平台，才能建屋，房屋署花30個月時間，為利東邨地盤移除了200萬立方米沙石，並運到[鋼綫灣作填海之用](../Page/鋼綫灣.md "wikilink")。此項爆破工程由[禮頓建築承建](../Page/禮頓建築.md "wikilink")。

## 出租公屋部分

利東邨出租公屋部分由8座35層高[Y型樓宇組成](../Page/Y型大廈.md "wikilink")：Y1型佔2幢、Y2型佔6幢；Y型大廈雖然是房屋署的標準設計，但運用在利東邨時卻要因為遷就陡峭的地形而略作改動。而樓宇安排亦盡量使住客看到最多的海景。

## 簡介及歷史

利東邨原址前身是1980年代初設立的[利東臨時房屋區](../Page/臨時房屋區#南區.md "wikilink")，後來臨時房屋區拆卸，並改建為標準[公共屋邨](../Page/公共屋邨.md "wikilink")。

由於利東邨位於[玉桂山旁的半山](../Page/玉桂山.md "wikilink")，北面有整個景色美麗及遊客常到的[香港仔港口](../Page/香港仔.md "wikilink")，東面有豪華遊艇聚集的深灣。邨內各座大廈皆處於不同的海拔高度的平台之上，因此各座大樓均有有蓋通道連接上下山，從利東邨的不同樓宇居高臨下，可飽覽香港仔及[東博寮海峽海景](../Page/東博寮海峽.md "wikilink")，更可遠眺南丫島及鄰近島嶼，盡覽香港大都會以外的大自然美景。

利東邨工程分三期興建。有多種不同面積的出租單位，適合一至九人家庭居住。此邨第一期工程於1987年完竣，包括兩座35層高Y（一）型大廈（東興樓、東茂樓）及兩座35層高Y（二）型大廈（東昌樓及東業樓），由[中國海外房屋工程有限公司承建](../Page/中國海外房屋工程有限公司.md "wikilink")；其中約500個為單身人士宿舍。第二期有4座35層高Y（二）型大廈（東昇樓、東平樓、東安樓及東逸樓）。第三期為居屋計劃樓宇—漁安苑，包括7座33層高彈性十字三型大廈。

### 食水含鉛超標

## 教育及康樂

利東邨設有[基督教宣道會利東幼兒學校](http://www.leitung.cmasshk.edu.hk/)、[聖文嘉中英文幼稚園](http://www.stmonicalt.edu.hk)、[路德會利東幼兒園](http://leitung-nursery.hklss.hk)三間幼兒園暨幼稚園、[鴨脷洲街坊學校](http://www.akps.edu.hk)、[聖伯多祿天主教小學兩間小學](../Page/聖伯多祿天主教小學.md "wikilink")、[香港仔浸信會呂明才書院](../Page/香港仔浸信會呂明才書院.md "wikilink")、[香港真光書院兩間中學](../Page/香港真光書院.md "wikilink")。康樂設施方面，有3個籃球場、2個足球場（其中一個內有[跑步徑](../Page/跑步徑.md "wikilink")）、排球場、羽毛球場、乒乓球枱、緩跑徑及兒童遊樂場等等。

Lei Tung Estate Playground (2).jpg|兒童遊樂場（2） Lei Tung Estate Playground
(3, brighter).jpg|兒童遊樂場（3）

## 社會服務設施

利東邨設有[社區會堂](../Page/社區會堂.md "wikilink")、青年中心、幼兒中心、老人社區中心、老人宿舍。

## 屋邨資料

### 樓宇

| **樓宇名稱（座別）** | **樓宇類型**                        | **落成年份** |
| ------------ | ------------------------------- | -------- |
| 東興樓 (C座)     | [Y1型](../Page/Y型.md "wikilink") | 1987（一期） |
| 東茂樓 (D座)     |                                 |          |
| 東昌樓 (A座)     | [Y2型](../Page/Y型.md "wikilink") |          |
| 東業樓 (B座)     |                                 |          |
| 東昇樓 (E座)     | 1988（二期）                        |          |
| 東平樓 (F座)     |                                 |          |
| 東安樓 (G座)     |                                 |          |
| 東逸樓 (H座)     |                                 |          |

## 利東商場

[Lei_Tung_Commercial_Centre_Phase_1.jpg](https://zh.wikipedia.org/wiki/File:Lei_Tung_Commercial_Centre_Phase_1.jpg "fig:Lei_Tung_Commercial_Centre_Phase_1.jpg")
[Lei_Tung_Commercial_Centre_Phase_1_1st_floor_(2).jpg](https://zh.wikipedia.org/wiki/File:Lei_Tung_Commercial_Centre_Phase_1_1st_floor_\(2\).jpg "fig:Lei_Tung_Commercial_Centre_Phase_1_1st_floor_(2).jpg")
[Lei_Tung_Commercial_Centre_Phase_1_2nd_floor_in_2014.jpg](https://zh.wikipedia.org/wiki/File:Lei_Tung_Commercial_Centre_Phase_1_2nd_floor_in_2014.jpg "fig:Lei_Tung_Commercial_Centre_Phase_1_2nd_floor_in_2014.jpg")
[Lei_Tung_Commercial_Centre_Phase_2_Atrium_201612.jpg](https://zh.wikipedia.org/wiki/File:Lei_Tung_Commercial_Centre_Phase_2_Atrium_201612.jpg "fig:Lei_Tung_Commercial_Centre_Phase_2_Atrium_201612.jpg")
利東商場樓高5層，分為兩期，約6萬平方呎，設有[停車場](../Page/停車場.md "wikilink")，亦有[大家樂](../Page/大家樂.md "wikilink")、[麥當勞](../Page/麥當勞.md "wikilink")、[肯德基及](../Page/肯德基.md "wikilink")[一粥麵等連鎖快餐店進駐](../Page/一粥麵.md "wikilink")。而商場頂層曾為酒樓，但酒樓於2009年10月結業，其後由同商場內的[百佳超級市場及名苑代替而遷入](../Page/百佳超級市場.md "wikilink")。頂層平台設花園及兒童遊樂場。

利東商場設有以下商店：

  - [KFC](../Page/KFC.md "wikilink")
  - [億世家](../Page/億世家.md "wikilink")
  - [7-11便利店](../Page/7-11便利店.md "wikilink")
  - [家品店](../Page/家品店.md "wikilink")
  - [日本城](../Page/日本城.md "wikilink")（現址為前房屋署分區保養辦事處）
  - [屈臣氏](../Page/屈臣氏.md "wikilink")
  - [759阿信屋](../Page/759阿信屋.md "wikilink")
  - [大家樂](../Page/大家樂.md "wikilink")
  - [一粥麵](../Page/一粥麵.md "wikilink")
  - [凱施餅店](../Page/凱施餅店.md "wikilink")
  - [民主黨](../Page/民主黨_\(香港\).md "wikilink")[南區區議員](../Page/南區區議會.md "wikilink")[區諾軒](../Page/區諾軒.md "wikilink")、[羅健熙聯合辦事處](../Page/羅健熙.md "wikilink")
  - [民建聯南區支部](../Page/民建聯.md "wikilink")、[立法會主席](../Page/香港立法會.md "wikilink")[曾鈺成](../Page/曾鈺成.md "wikilink")、立法會議員[李慧琼](../Page/李慧琼.md "wikilink")、[鍾樹根聯合辦事處](../Page/鍾樹根.md "wikilink")
  - [南區區議會委任區議員](../Page/南區區議會.md "wikilink")[廖漢輝議員辦事處](../Page/廖漢輝.md "wikilink")
  - [診所](../Page/診所.md "wikilink")、[雜誌店](../Page/雜誌店.md "wikilink")、[理髮店](../Page/理髮店.md "wikilink")、[文具店及](../Page/文具店.md "wikilink")[餐廳等](../Page/餐廳.md "wikilink")

<!-- end list -->

  - 利東商場一期

Lei Tung Commercial Centre Phase 1 GF 201612.jpg|地下南面商店 Lei Tung
Commercial Centre Phase 1 ground floor (2).jpg|地下北面商店 Lei Tung
Commercial Centre Phase 1 1st
floor.jpg|1樓[大家樂](../Page/大家樂.md "wikilink") Lei Tung
Commercial Centre Phase 1 2nd
floor.jpg|2016年的2樓南面（左）及北面（右），左邊寢具店及右方[波仔店已結業](../Page/波仔.md "wikilink")，並於右方加建一間商舖（理髮店）
Lei Tung Commercial Centre Phase 1 2nd floor (2).jpg|2樓西面 Lei Tung
Commercial Centre Phase 1 3rd floor (2).jpg|3樓「名苑」

  - 利東商場二期

Lei Tung Commercial Centre Phase 2 B1
JHC.jpg|地庫1樓[日本城](../Page/日本城.md "wikilink") Lei Tung
Commercial Centre Phase 2 ground
floor.jpg|地下[凱施餅店及](../Page/凱施餅店.md "wikilink")[麥當勞](../Page/麥當勞.md "wikilink")
Lei Tung Commercial Centre Phase 2 2nd
floor.jpg|2樓[康年護老院](../Page/康年護老院.md "wikilink")（現址為前房屋署辦事處）
Lei Tung Commercial Centre Phase 2 3rd
floor.jpg|3樓設議員辦事處及[診所](../Page/診所.md "wikilink")

## 利東街市

[Lei_Tung_Market.JPG](https://zh.wikipedia.org/wiki/File:Lei_Tung_Market.JPG "fig:Lei_Tung_Market.JPG")
[Lei_Tung_Market_view_201612.jpg](https://zh.wikipedia.org/wiki/File:Lei_Tung_Market_view_201612.jpg "fig:Lei_Tung_Market_view_201612.jpg")
[Lei_Tung_Estate_Cooked_Food_Stall_(brighter).jpg](https://zh.wikipedia.org/wiki/File:Lei_Tung_Estate_Cooked_Food_Stall_\(brighter\).jpg "fig:Lei_Tung_Estate_Cooked_Food_Stall_(brighter).jpg")熟食檔（已結業）\]\]

利東街市於2016年完成翻新，由大判管理公司宏集策劃有限公司作街市管理。\[1\]

Lei Tung Market in 2016.jpg|入口 Interior of Lei Tung Market (2).jpg|菜檔
Interior of Lei Tung Market (3).jpg|水果與[海味檔](../Page/海味.md "wikilink")
Interior of Lei Tung Market.jpg|魚檔 Interior of Lei Tung Market
(4).jpg|雜貨、[紙紮及凍肉檔](../Page/紙紮.md "wikilink") Interior of Lei Tung
Market (5).jpg|[燒味檔](../Page/燒味.md "wikilink") Interior of Lei Tung
Market (6).jpg|水電五金檔 Interior of Lei Tung Market
(7).jpg|翻新後初期的利東街市仍有不少檔位未租出 Lei Tung
Market view2 201612.jpg|小食店（場外）

## 利東海鮮食街

[Lei_Tung_Food_Mart_201612.jpg](https://zh.wikipedia.org/wiki/File:Lei_Tung_Food_Mart_201612.jpg "fig:Lei_Tung_Food_Mart_201612.jpg")
利東海鮮食街於2016年11月開業，原有9個舖位，7間食肆經營，並交由「皇昌有限公司」判上判形式管理。不過由於人流不足，大多經營不足4個月已結業，到2017年3月只剩一間店舖經營。該店被通知於同年4月1日收回舖位，商戶大嘆40萬元血本無歸，不能得到賠償。\[2\]

## 美食薈超級街市

美食薈超級街市於2017年12月開業，於2018年8月13日裝修再次停止營業，再於8月23日重開。

## 名人住客

  - [陳志健](../Page/陳志健.md "wikilink")：香港男演員\[3\]

<!-- end list -->

  - [鄧美欣](../Page/鄧美欣.md "wikilink")：2018香港小姐

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{南港島綫色彩}}">█</font><a href="../Page/南港島綫.md" title="wikilink">南港島綫</a>：<a href="../Page/利東站.md" title="wikilink">利東站</a></li>
</ul>
<dl>
<dt><a href="../Page/利東邨巴士總站.md" title="wikilink">利東邨巴士總站</a></dt>

</dl>
<dl>
<dt><a href="../Page/利東邨道.md" title="wikilink">利東邨道</a>/<a href="../Page/利東邨通道.md" title="wikilink">利東邨通道</a></dt>

</dl>
<dl>
<dt><a href="../Page/漁安苑道.md" title="wikilink">漁安苑道</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 資料來源

## 外部連結

  - [房委會：利東邨簡介](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=2758)
  - [屋苑簡介-利東邨](http://home.hongyip.com/lt/Home)

[Category:玉桂山](../Category/玉桂山.md "wikilink")
[Category:租者置其屋計劃屋邨](../Category/租者置其屋計劃屋邨.md "wikilink")

1.
2.
3.