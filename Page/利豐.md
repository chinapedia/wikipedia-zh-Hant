[HK_LiFungTower.JPG](https://zh.wikipedia.org/wiki/File:HK_LiFungTower.JPG "fig:HK_LiFungTower.JPG")利豐大廈\]\]
[HK_Li_Fung_Centre.jpg](https://zh.wikipedia.org/wiki/File:HK_Li_Fung_Centre.jpg "fig:HK_Li_Fung_Centre.jpg")[石門利豐中心](../Page/石門_\(香港\).md "wikilink")\]\]

**利豐有限公司**（，）是一家在[香港交易所上市的綜合企業公司](../Page/香港交易所.md "wikilink")，主要業務是消費產品之出口貿易，消費產品包括[成衣](../Page/成衣.md "wikilink")、[時尚](../Page/時尚.md "wikilink")[飾物](../Page/飾物.md "wikilink")、[玩具與](../Page/玩具.md "wikilink")[遊戲](../Page/遊戲.md "wikilink")、運動用品、[傢具](../Page/傢具.md "wikilink")、[手工藝品](../Page/手工藝.md "wikilink")、[鞋類與](../Page/鞋.md "wikilink")[餐具等](../Page/餐具.md "wikilink")。公司在[百慕達註冊](../Page/百慕達.md "wikilink")，董事局榮譽主席為[馮國經](../Page/馮國經.md "wikilink")，董事局主席[馮國綸](../Page/馮國綸.md "wikilink")。

利豐集團及部分附屬公司總部設於[長沙灣道](../Page/長沙灣道.md "wikilink")888號利豐大廈。

自2017年2月10日，利豐被剔出[恒生指數成分股](../Page/恒生指數.md "wikilink")。

## 歷史

利豐公司由[李道明和](../Page/李道明.md "wikilink")[馮柏燎於](../Page/馮柏燎.md "wikilink")1906年在[中國](../Page/中國.md "wikilink")[廣州創立](../Page/廣州.md "wikilink")，是當時中國第一家華資的對外貿易出口商。1937年12月28日，馮柏燎的第三子[馮漢柱在香港成立分公司利豐](../Page/馮漢柱.md "wikilink")（1937）有限公司。

1943年利豐創辦人之一馮柏燎去世後，業務移交馮氏家族第二代。其後向來不參與業務管理的合夥人李道明，退休後將股份全數賣給[馮柏燎家族](../Page/馮柏燎家族.md "wikilink")，但公司名稱未有變更，「利豐」二字，其中藏有兩位創辦人的[姓氏](../Page/姓氏.md "wikilink")（[李姓及](../Page/李姓.md "wikilink")[馮姓](../Page/馮姓.md "wikilink")）。

藉著1950年代香港成為以製造生產主導的勞工密集市場經濟，利豐最初的出口產品類別包括[服裝](../Page/服裝.md "wikilink")、[玩具](../Page/玩具.md "wikilink")、電子及塑膠花等，馮漢柱將利豐發展成為香港以現金計算的最大出口商之一。

1970年代初，馮氏家族第三代[馮國經和](../Page/馮國經.md "wikilink")[馮國綸自](../Page/馮國綸.md "wikilink")[美國留學回港](../Page/美國.md "wikilink")，加入利豐家族業務。1973年利豐在[香港聯交所上市](../Page/香港聯交所.md "wikilink")，獲超額認購113倍，正式成為香港[上市公司](../Page/上市公司.md "wikilink")。1989年利豐為重整家族股權架構及專注核心業務發展將公司進行私有化，由管理層收購全部股權，將公司集中在出口貿易及零售兩項業務。1992年利豐出口貿易業務以「利豐有限公司」名稱在香港聯交所上市。

利豐透過[收購手段將競爭對手一一收歸旗下](../Page/收購.md "wikilink")，1995年收購[英之傑採購服務](../Page/英之傑.md "wikilink")（Inchcape
Buying
Services，前身為[天祥](../Page/天祥.md "wikilink")）；1999年收購[太古集團旗下的](../Page/太古集團.md "wikilink")[太古貿易有限公司](../Page/太古貿易有限公司.md "wikilink")（Swire
& Maclaine Ltd.）和金巴莉有限公司（Camberley Enterprises
Ltd.）；2000年和2002年分別收購香港採購出口集團由[盛智文擁有的](../Page/盛智文.md "wikilink")「Colby
Group」及美利洋行（Janco Oversea
Limited）。[2009年10月20日以](../Page/2009年10月20日.md "wikilink")31.34億港元（4.018億美元），收購美國童裝及男士服飾銷售公司Wear
Me Apparel。

2010年5月起，利豐收購了以香港為總部、香水及個人護理產品企業Jackel Group；6月又收購香港牛仔服裝設計HTP
Group，以及美加領先的男女配飾設計及分銷公司Cipriani Accessories Inc.及其聯營公司The
Max Leather
Group絕大部份資產。[2012年3月27日利豐透過先舊後新方式](../Page/2012年3月27日.md "wikilink")，按每股18.52至18.82元，配股集資5億美元（約39億港元），所得將作一般營業資金。

2012年8月20日，利豐母公司利豐集團，這個月開始改名為馮氏集團。

2012年12月16日，利豐以代價5400萬美元（約4.212億港元），向主要股東馮氏控股（1937-HK）出售目標公司利豐亞洲（台灣）全部權益，以轉型作品牌特許授權業務，主要從事「Roots」品牌服裝及配飾零售業務。

2013年1月15日。利豐以1.9億美元，收購個人護理品牌Lornamead Acquisitions
Limited及其附屬公司（“Lornamead”）。Lornamead擁有及管理多個美國、德國及英國個人護理品牌，如Finesse,
CD, Aqua Net,
Yardley及Lypsyl，產品種類包括美發、身體護理、護膚及口腔護理。是項收購並不包括Lornamead資產負債表上之現金，但收購不包括美國以外Yardley業務等。（Lornamead已於此收購項目前分開賣出）

2013年1月31日，意大利高級皮革品牌Furla夥拍利豐（00494）母公司馮氏集團，成立合營公司管理旗下在中、港及澳門的分銷業務，為未來四年在大中華區開設超過100間分店鋪路。

2013年9月22日大股東馮氏家族的投資基金Fung Capital，透過歐洲分部Fung Capital
Europe（FCE），向迪拜集團JMH Group收購英國男士服裝品牌Kilgour，將與Hardy
Amies組成兩大成衣品牌系列發展，在高端百貨公司銷售。

2015年11月7日利標品牌以不超過1.3億美元（約10.14億港元）向PS
Brands收購其授權、品牌及私人商標襪類產品之製造、設計、開發、採購、入口、推廣、銷售及分銷業務

2016年5月3日，[大昌行以](../Page/大昌行.md "wikilink")3.5億美元（逾27.3億港元）現金，收購利豐旗下亞洲消費品及健康保健用品分銷業務（LF
Asia Distribution）全部股權。是項協議並不包括利豐的物流及美容產品業務。

2017年2月10日，利豐被剔出[恆生指數成份股](../Page/恆生指數成份股.md "wikilink")，由[吉利汽車取代](../Page/吉利汽車.md "wikilink")。\[1\]

2017年5月3日馮氏零售集團與玩具反斗城亞洲簽訂一項協議，將合併[玩具反斗城日本與大中華區及東南亞的業務](../Page/玩具反斗城.md "wikilink")。在加入玩具反斗城日本後，馮氏零售集團將擁有亞洲合資企業玩具反斗城亞洲
(Toys"R"Us Asia Ltd)約15%股權。

2017年12月14日馮氏控股1937及弘毅基金分別佔55%和45%股權的合資公司True Sage
Limited以11億美元（約85.36億港元）現金，收購利豐旗下傢具、美容和毛衣產品業務，在出售完成後，會向股東派發每股0.0614美元(折合約0.476港元)的特別股息，總值5.2億美元

## 馮氏控股（1937）有限公司

馮氏控股（1937）有限公司（前稱：利豐（1937）有限公司），是馮氏集團（前稱：利豐集團）的主要股東。其核心業務包括貿易、物流、分銷及零售。

集團旗下上市業務包括利豐有限公司（股票代號：00494）、利和經銷有限公司（當時股票代號：02387，但已私有化）、利亞零售有限公司（股票代號：08052）及新近上市的利邦控股有限公司（股票代號：00891）。

非上市的零售業務包括經營時裝零售業務的Branded Lifestyle以及經營連鎖玩具店「玩具"反"斗城」的利童反斗城（亞洲）有限公司。

馮氏集團成員公司：

  - 利豐有限公司（），集團之出口貿易業務 -
    利豐有限公司是全球最具規模的供應鏈管理企業之一，運用供應鏈管理的概念，提供高增值及高流量消費品的原料採購、制造及出口統籌服務；業務網絡遍布全球40個經濟體系。

<!-- end list -->

  - [利和經銷集團有限公司](../Page/利和經銷集團有限公司.md "wikilink")（當時股票代號：02387，但已私有化），集團之經銷業務由利和經銷主理，為亞太地區客戶提供全方位配套服務，核心業務包括生產制造、物流管理及品牌推廣等。

<!-- end list -->

  - [利亞零售有限公司](../Page/利亞零售.md "wikilink")()，於香港及華南地區經營「[OK便利店](../Page/OK便利店.md "wikilink")」及「[聖安娜餅屋](../Page/聖安娜餅屋.md "wikilink")」之連鎖零售業務，總店舖數目逾500家。

<!-- end list -->

  - [利邦集團](http://www.trinity-limited.com)()，乃中國高級奢華男服零售市場主要零售集團之一，管理多個國際著名男服品牌，包括：Kent
    & Curwen、Gieves & Hawkes、Cerruti
    1881、D'urban、Intermezzo及Altea；零售網絡覆蓋各大城市之高端購物中心及商場。

<!-- end list -->

  - [利標品牌](../Page/利標品牌.md "wikilink")()，從利豐分拆，主要為授權品牌及拥控品牌，將於2014年7月9日以介紹形式在[聯交所上市](../Page/聯交所.md "wikilink")。

私人全資擁有業務：

  - [利豐零售](../Page/利豐零售.md "wikilink")，經營零售業務；

<!-- end list -->

  - Branded Lifestyle，經營時裝零售業務；

<!-- end list -->

  - 利童反斗城（亞洲）有限公司，經營連鎖玩具店[玩具反斗城](../Page/玩具反斗城.md "wikilink")。

<!-- end list -->

  - 广州正佳反斗城

<!-- end list -->

  - 南韓兒童服飾生產商Suhyang Networks70%權益

<!-- end list -->

  - 利和醫療集團

<!-- end list -->

  - [微醫利和醫療中國有限公司](../Page/微醫.md "wikilink")

董事局榮譽主席為[馮國經](../Page/馮國經.md "wikilink")、董事局主席為[馮國綸](../Page/馮國綸.md "wikilink")，利豐集團旗下共有逾35,000名僱員遍佈全球40個經濟體系。

## 股份分拆

2011年3月24日，利豐（）宣布將股份拆細，一拆二股，拆細後繼續以每手二千股為買賣單位，以降低入場費及增加該股市場流通量，從而吸引更多投資者及擴大其股東基礎，於2011年5月19日正式生效。

## 外部連結

  - [馮氏集團](http://www.funggroup.com/big5/global/home.php)
  - [利豐網站](http://www.lifung.com)
  - [利豐集團網站](http://www.lifunggroup.com)
  - [利邦集團網站](http://www.trinity-limited.com)

[Category:香港交易所上市公司](../Category/香港交易所上市公司.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")
[Category:香港上市綜合企業公司](../Category/香港上市綜合企業公司.md "wikilink")
[Category:香港物流公司](../Category/香港物流公司.md "wikilink")
[Category:香港贸易公司](../Category/香港贸易公司.md "wikilink")
[利豐](../Category/利豐.md "wikilink")
[Category:前恒生指數成份股](../Category/前恒生指數成份股.md "wikilink")

1.  [恒指成分股加入吉利汽車
    剔出利豐](http://www2.hkej.com/instantnews/stock/article/1490056/%E6%81%92%E6%8C%87%E6%88%90%E5%88%86%E8%82%A1%E5%8A%A0%E5%85%A5%E5%90%89%E5%88%A9%E6%B1%BD%E8%BB%8A+%E5%89%94%E5%87%BA%E5%88%A9%E8%B1%90)