**利默里克** （Limerick ；[愛爾蘭語](../Page/愛爾蘭語.md "wikilink")：
Luimneach）\[1\]是[愛爾蘭西部的一個城市](../Page/愛爾蘭共和國.md "wikilink")，位於[香農河河口](../Page/香農河.md "wikilink")。是[利默里克郡郡治](../Page/利默里克郡.md "wikilink")。2006年人口90,778人。

## 参考文献

<div class="references-small">

<references />

</div>

[L](../Category/愛爾蘭城市.md "wikilink")
[L](../Category/愛爾蘭行政區劃.md "wikilink")

1.  原文是loimeanach，是「荒蕪的沼澤」的意思，指的是城外的河灘。