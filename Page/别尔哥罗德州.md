**别尔哥罗德州**（）位於[俄羅斯西南部](../Page/俄羅斯.md "wikilink")[頓河](../Page/頓河_\(俄羅斯\).md "wikilink")—[第聶伯河中間的丘陵](../Page/第聶伯河.md "wikilink")，南部、西部與[烏克蘭接壤](../Page/烏克蘭.md "wikilink")。是[俄羅斯聯邦主體之一](../Page/俄羅斯聯邦主體.md "wikilink")，屬[中央聯邦管區](../Page/中央聯邦管區.md "wikilink")。面積27,100平方公里，人口1,549,581
(2016年)。首府[别尔哥罗德](../Page/别尔哥罗德.md "wikilink")，距離首都[莫斯科](../Page/莫斯科.md "wikilink")695公里。

## 历史

17世纪之交，该地区建成了一条坚实的军事设防线，延伸了近800公里。
由于贵族压迫和税务负担而搬到这里的乌克兰[哥萨克人负责线路防御](../Page/哥萨克.md "wikilink")。
在[哥萨克酋长国](../Page/哥萨克酋长国.md "wikilink")（1659-1679）时期，更多的哥萨克人在[赫梅尔尼茨基起义](../Page/赫梅尔尼茨基起义.md "wikilink")（1648-1657）和相互间的战争中移动到该地区。
别尔哥罗德成为军事和行政中心，起源于俄罗斯南部边界的前哨。

在[波尔塔瓦会战之后](../Page/波尔塔瓦会战.md "wikilink")，[彼得一世授予大别尔哥罗德的士兵团旗](../Page/彼得大帝.md "wikilink")。

## 行政区划

### 区域

| 区旗                                                                                                                                                                                                    | 中文名                                           | 俄文名                     | 行政中心                                     |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------- | ----------------------- | ---------------------------------------- |
| [Flag_of_Alexeyevsky_rayon.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Alexeyevsky_rayon.svg "fig:Flag_of_Alexeyevsky_rayon.svg")                                                              | [阿列克谢耶夫卡区](../Page/阿列克谢耶夫卡区.md "wikilink")    | Алексеевский район      | [阿列克谢耶夫卡](../Page/阿列克谢耶夫卡.md "wikilink") |
| [Flag_of_Belgorod_rayon.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Belgorod_rayon.svg "fig:Flag_of_Belgorod_rayon.svg")                                                                       | [别尔哥罗德区](../Page/别尔哥罗德区.md "wikilink")        | Белгородский район      | [迈斯基](../Page/迈斯基.md "wikilink")         |
| [Flag_of_Borisovsky_rayon.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Borisovsky_rayon.svg "fig:Flag_of_Borisovsky_rayon.svg")                                                                 | [鲍里索夫卡区](../Page/鲍里索夫卡区.md "wikilink")        | Борисовский район       | [鲍里索夫卡](../Page/鲍里索夫卡.md "wikilink")     |
| [Flag_of_Valuysky_rayon.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Valuysky_rayon.svg "fig:Flag_of_Valuysky_rayon.svg")                                                                       | [瓦卢伊基区](../Page/瓦卢伊基区.md "wikilink")          | Валуйский район         | [瓦卢伊基](../Page/瓦卢伊基.md "wikilink")       |
| [Flag_of_Veydelevsky_rayon.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Veydelevsky_rayon.svg "fig:Flag_of_Veydelevsky_rayon.svg")                                                              | [韦伊杰列夫卡区](../Page/韦伊杰列夫卡区.md "wikilink")      | Вейделевский район      | [韦伊杰列夫卡](../Page/韦伊杰列夫卡.md "wikilink")   |
| [Flag_of_Volokonovsky_rayon.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Volokonovsky_rayon.svg "fig:Flag_of_Volokonovsky_rayon.svg")                                                           | [沃洛科诺夫卡区](../Page/沃洛科诺夫卡区.md "wikilink")      | Волоконовский район     | [沃洛科诺夫卡](../Page/沃洛科诺夫卡.md "wikilink")   |
| [Flag_of_Grayvoronsky_rayon_(Belgorod_oblast).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Grayvoronsky_rayon_\(Belgorod_oblast\).svg "fig:Flag_of_Grayvoronsky_rayon_(Belgorod_oblast).svg") | [格赖沃龙区](../Page/格赖沃龙区.md "wikilink")          | Грайворонский район     | [格赖沃龙](../Page/格赖沃龙.md "wikilink")       |
| [Flag_of_Ivnyansky_rayon_(Belgorod_oblast).png](https://zh.wikipedia.org/wiki/File:Flag_of_Ivnyansky_rayon_\(Belgorod_oblast\).png "fig:Flag_of_Ivnyansky_rayon_(Belgorod_oblast).png")          | [伊夫尼亚区](../Page/伊夫尼亚区.md "wikilink")          | Ивнянский район         | [伊夫尼亚](../Page/伊夫尼亚.md "wikilink")       |
| [Flag_of_Korochansky_rayon.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Korochansky_rayon.svg "fig:Flag_of_Korochansky_rayon.svg")                                                              | [科罗恰区](../Page/科罗恰区.md "wikilink")            | Корочанский район       | [科罗恰](../Page/科罗恰.md "wikilink")         |
| [Flag_of_Krasnensky_rayon.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Krasnensky_rayon.svg "fig:Flag_of_Krasnensky_rayon.svg")                                                                 | [克拉斯诺耶区](../Page/克拉斯诺耶区.md "wikilink")        | Красненский район       | [克拉斯诺耶](../Page/克拉斯诺耶.md "wikilink")     |
| [Flag_of_Krasnogvardeysky_rayon.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Krasnogvardeysky_rayon.svg "fig:Flag_of_Krasnogvardeysky_rayon.svg")                                               | [赤卫军区](../Page/赤卫军区_\(别尔哥罗德州\).md "wikilink") | Красногвардейский район | [比留奇](../Page/比留奇.md "wikilink")         |
| [Flag_of_Krasnoyaruzhsky_rayon.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Krasnoyaruzhsky_rayon.svg "fig:Flag_of_Krasnoyaruzhsky_rayon.svg")                                                  | [红亚鲁加区](../Page/红亚鲁加区.md "wikilink")          | Краснояружский район    | [红亚鲁加](../Page/红亚鲁加.md "wikilink")       |
| [Flag_of_Novooskolsky_rayon.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Novooskolsky_rayon.svg "fig:Flag_of_Novooskolsky_rayon.svg")                                                           | [新奥斯科尔区](../Page/新奥斯科尔区.md "wikilink")        | Новооскольский район    | [新奥斯科尔](../Page/新奥斯科尔.md "wikilink")     |
| [Flag_of_Prohorovsky_rayon_(Belgorod_oblast).png](https://zh.wikipedia.org/wiki/File:Flag_of_Prohorovsky_rayon_\(Belgorod_oblast\).png "fig:Flag_of_Prohorovsky_rayon_(Belgorod_oblast).png")    | [普洛霍罗夫卡区](../Page/普洛霍罗夫卡区.md "wikilink")      | Прохоровский район      | [普洛霍罗夫卡](../Page/普洛霍罗夫卡.md "wikilink")   |
| [Flag_of_Rakityansky_rayon.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Rakityansky_rayon.svg "fig:Flag_of_Rakityansky_rayon.svg")                                                              | [拉基特诺耶区](../Page/拉基特诺耶区.md "wikilink")        | Ракитянский район       | [拉基特诺耶](../Page/拉基特诺耶.md "wikilink")     |
| [Flag_of_Rovensky_rayon.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Rovensky_rayon.svg "fig:Flag_of_Rovensky_rayon.svg")                                                                       | [罗韦尼基区](../Page/罗韦尼基区.md "wikilink")          | Ровеньский район        | [罗韦尼基](../Page/罗韦尼基.md "wikilink")       |
| [Flag_of_Chernyansky_rayon.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Chernyansky_rayon.svg "fig:Flag_of_Chernyansky_rayon.svg")                                                              | [切尔尼扬卡区](../Page/切尔尼扬卡区.md "wikilink")        | Чернянский район        | [切尔尼扬卡](../Page/切尔尼扬卡.md "wikilink")     |
| [Flag_of_Shebekino.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Shebekino.svg "fig:Flag_of_Shebekino.svg")                                                                                       | [舍别基诺区](../Page/舍别基诺区.md "wikilink")          | Шебекинский район       | [舍别基诺](../Page/舍别基诺.md "wikilink")       |
| [Flag_of_Yakovlevsky_rayon.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Yakovlevsky_rayon.svg "fig:Flag_of_Yakovlevsky_rayon.svg")                                                              | [雅科夫列沃区](../Page/雅科夫列沃区.md "wikilink")        | Яковлевский район       | [斯特罗伊捷利](../Page/斯特罗伊捷利.md "wikilink")   |

## 注释

## 参考文献

## 外部連結

  - [Official website of Belgorod Oblast](http://www.belregion.ru/)

  - [Belgorod Oblast sceneries
    photos](http://russiatrek.org/belgorod-oblast)

[\*](../Category/別爾哥羅德州.md "wikilink")
[Category:中央聯邦管區](../Category/中央聯邦管區.md "wikilink")
[Category:俄罗斯州份](../Category/俄罗斯州份.md "wikilink")