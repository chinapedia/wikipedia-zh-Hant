[制度](../Page/制度.md "wikilink")，指[人际交往中的规则及](../Page/人际交往.md "wikilink")[社会组织的结构和机制](../Page/社会组织.md "wikilink")。**制度经济学**（）是把制度作为研究对象的一门[经济学分支](../Page/经济学.md "wikilink")。它研究制度对于经济行为和经济发展的影响，以及经济发展如何影响制度的演变。美國經濟學家[托斯丹·范伯倫被推崇為這個學派的創始者](../Page/托斯丹·范伯倫.md "wikilink")，在1919年發表於《[美國經濟評論](../Page/美國經濟評論.md "wikilink")》的論文中歸納了先前的研究成果，並提出這個名稱。

[新制度经济学的研究始于](../Page/新制度经济学.md "wikilink")[科斯](../Page/罗纳德·科斯.md "wikilink")《企业之性质》，科斯的贡献在于的将[交易成本这一概念引入了经济学的分析中并指出企业和市场在经济交往中的不同作用](../Page/交易成本.md "wikilink")。
[奥利弗·威廉姆森](../Page/奥利弗·威廉姆森.md "wikilink")、等人对于这么新兴学科作出了重大的贡献。近30年，[新制度经济学是蓬勃发展的经济学的一个分支](../Page/新制度经济学.md "wikilink")。

## 外部链接

  - [Chang, Ha-Joon "Breaking the mould: an institutionalist political
    economy alternative to the neo-liberal theory of the market and the
    state" Cambridge Journal of Economics 26:539-559
    (2002)](http://cje.oxfordjournals.org/cgi/content/abstract/26/5/539)
  - [American Institutional
    School](https://web.archive.org/web/20090319115301/http://cepa.newschool.edu/het/schools/institut.htm)
  - [Thorstein Veblen, Articels by
    Veblen](http://www.mnc.net/norway/veblen.html)
  - [T.Veblen:Leisure
    Class](https://web.archive.org/web/20070609124553/http://socserv2.mcmaster.ca/%7Eecon/ugcm/3ll3/veblen/leisure/index.html)
  - [Li,Rita Yi Man “Everyday Life Application of Neo-institutional
    Economics: A Global
    Perspective”](http://www.amazon.com/Everyday-Life-Application-Neo-institutional-Economics/dp/384439270X)

[Category:经济学](../Category/经济学.md "wikilink")
[Category:非主流經濟學](../Category/非主流經濟學.md "wikilink")
[Category:制度經濟學](../Category/制度經濟學.md "wikilink")