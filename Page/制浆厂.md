[Zellstoffwerk_Rosenthal_4.jpg](https://zh.wikipedia.org/wiki/File:Zellstoffwerk_Rosenthal_4.jpg "fig:Zellstoffwerk_Rosenthal_4.jpg")
**制浆厂**，是能将木材脆片变为厚的[纤维板的制造设施](../Page/纤维板.md "wikilink")，纤维板可以被运到[造纸厂进行进一步的製程](../Page/造纸厂.md "wikilink")。木浆可以使用机械的、半化学的或全化学的方法，如[硫酸盐制浆法制造](../Page/硫酸盐制浆法.md "wikilink")。根据客户的需求，成品可以是[漂白的或非漂白的](../Page/漂白.md "wikilink")。

## 概述

制浆的过程始于碎木片堆，在那里，刨机木碎片和[锯木厂木碎片存放一个或两个月进行机械制浆前的风干](../Page/锯木厂.md "wikilink")。

树木的木质除了水以外包含三种主要成分：[纤维素聚合物纤维](../Page/纤维素.md "wikilink")，造纸所需要的原材料；[木质素](../Page/木质素.md "wikilink")，一种三维聚合物，能将纤维素纤维结合在一起产生树木或木材的内在强度；以及[半纤维素](../Page/半纤维素.md "wikilink")，短一些的多聚糖。化学制浆的目的就是在于将有用的纤维素纤维与木质素和半纤维素分离。这需要在取出所有木质素和避免降低纤维素纤维的强度之间保持平衡。

化学制浆法，如[硫酸盐制浆法和](../Page/硫酸盐.md "wikilink")[亚硫酸盐制浆法](../Page/亚硫酸盐.md "wikilink")，去除了大量的半纤维素和木质素。在保持纤维素纤维的强度上，硫酸盐纸浆法比亚硫酸盐制浆法更为有效。化学制浆法结合使用高温和[酸的](../Page/酸.md "wikilink")（硫酸盐）或[碱的](../Page/碱.md "wikilink")（亚硫酸盐）化学制剂来品打开木质素的[化学键](../Page/化学键.md "wikilink")。

有不同的[机械制浆法](../Page/机械.md "wikilink")，如研磨制浆法和盘磨机制浆法，机械性地将纤维素纤维撕开。大量的木质素依然粘着在纤维上。强度被消弱因为，纤维可能被切断。

同时还有许多结合了化学和[热处理的混合制浆法](../Page/热处理.md "wikilink")：开始进行缩短的化学处理，接着立即进行机械处理以分离纤维。这些混合方法包括热磨机械制浆法（TMP）和化学热磨机械制浆法（CTMP）。化学处理和热处理能够降低机械处理的能耗，同时也降低了纤维遭受的[强度损失量](../Page/强度.md "wikilink")。

碎木片开始进入工厂的筛选车间，在那里碎木片被分类并筛去木屑。过大尺寸的碎木片被进一步破碎或用作[燃料](../Page/燃料.md "wikilink")。经过筛选后，碎木片开始进入[蒸煮器](../Page/蒸煮器.md "wikilink")，在那里碎木片在一个大容器内与[氢氧化钠和](../Page/氢氧化钠.md "wikilink")[硫化钠液体混合并用来自](../Page/硫化钠.md "wikilink")[锅炉的](../Page/锅炉.md "wikilink")[蒸汽加热](../Page/蒸汽.md "wikilink")。

经过几个小时的蒸煮，碎木片分解为燕麦粥样的浓度并在通过蒸煮器的出口的气闸时被挤压。压力的突然变化使得碎木片以类似[爆米花的方式膨胀](../Page/爆米花.md "wikilink")，因此进一步分解了木质纤维。生成的纤维在水溶液的悬浮液被称为[木浆](../Page/木浆.md "wikilink")。

随后，木质纤维被清洗，去除所有化学残余物，这些化学残余物将在工厂中进行恢复和[回收](../Page/回收.md "wikilink")。纤维现可以进行[漂白或不进行漂白](../Page/漂白.md "wikilink")。木浆被喷到木浆机器网上，排水并进行[干燥](../Page/干燥.md "wikilink")。此刻的纤维就可以再改变为非直线的材料。这种状态下的纤维的挤压和干燥引起了纤维间强连接的结合而不是网状结构。在干燥机出口处的叠制装置将干燥的纸浆切割、堆叠并打包。然后，木浆被装上[火车](../Page/火车.md "wikilink")、[汽车或](../Page/汽车.md "wikilink")[轮船运到](../Page/轮船.md "wikilink")[造纸厂](../Page/造纸厂.md "wikilink")。

## 参见

  - [木浆](../Page/木浆.md "wikilink")
  - [造纸厂](../Page/造纸厂.md "wikilink")

## 外部链接

  - [Integrated Pulp Mill - Forest Biorefinery - pdf
    file](https://web.archive.org/web/20070213075137/http://www.tri-inc.net/Biorefinery%20Business%20Case-June1%20_2_.pdf)
  - [Pulp and Paper
    terminology](http://www.unionmillwright.com/paper4.html)

[Category:造纸](../Category/造纸.md "wikilink")
[Category:造纸业](../Category/造纸业.md "wikilink")
[Category:林业](../Category/林业.md "wikilink")