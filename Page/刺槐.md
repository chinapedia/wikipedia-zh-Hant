**刺槐**又名**洋槐**，是原生于[北美洲的一个](../Page/北美洲.md "wikilink")[树](../Page/树.md "wikilink")[种](../Page/种.md "wikilink")，但现在被广泛引种到[亚洲](../Page/亚洲.md "wikilink")、[欧洲等地](../Page/欧洲.md "wikilink")。汉语名称取自其与同属[蝶形花亚科的](../Page/蝶形花亚科.md "wikilink")[槐属的相似度](../Page/槐属.md "wikilink")（[刺槐族仅在美洲存在分布](../Page/刺槐族.md "wikilink")）；拉丁学名中的种名“pseudoacacia”是假[金合欢或假](../Page/金合欢属.md "wikilink")[相思树之意](../Page/相思树属.md "wikilink")，尽管两属同属[蘇木亞科与洋槐关系甚远](../Page/蘇木亞科.md "wikilink")。

## 形态

刺槐可以长到高达27[米](../Page/米.md "wikilink")，树干[直径到](../Page/直径.md "wikilink")1.6米，[树皮厚](../Page/树皮.md "wikilink")，暗色，纹裂多；树[叶长](../Page/叶.md "wikilink")25厘米，羽状复叶，由9-19个小叶组成，每个树叶根部有一对1-2毫米长的刺；[花为](../Page/花.md "wikilink")[白色](../Page/白色.md "wikilink")，有香味，穗状花序；[果实为](../Page/果实.md "wikilink")[荚果](../Page/荚果.md "wikilink")，每个果荚中有4-10粒[种子](../Page/种子.md "wikilink")。刺槐树叶类似[槐树](../Page/槐树.md "wikilink")，但叶比较薄，半透明，有刺，花色不同，是两者的主要区别。

## 生态

刺槐喜光，喜温暖湿润气候。在中国[黄河中下游](../Page/黄河.md "wikilink")、[淮河流域和](../Page/淮河.md "wikilink")[黄土高原背风沟谷](../Page/黄土高原.md "wikilink")、土石山坡的沟谷地、河滩等都能很好生长。从气候上，年降水量600～1200毫米，年均气温10～14℃比较适合生长。

## 用途

刺槐对[土壤要求不高](../Page/土壤.md "wikilink")，[根部有](../Page/根.md "wikilink")[根瘤菌](../Page/根瘤菌.md "wikilink")，有固[氮能力](../Page/氮.md "wikilink")，并可以抵抗污染，所以被广泛引种，作为城市行道树，或防风固沙树种。刺槐也是一种重要的[蜜源](../Page/蜜.md "wikilink")[植物](../Page/植物.md "wikilink")。刺槐的[木材坚硬](../Page/木材.md "wikilink")，耐腐蚀，木质中含有的类黄酮可以使其在地下埋藏达100年之久，不会腐烂。\[1\]
刺槐木材燃烧缓慢，只有少量的烟，热值高，所以在[欧](../Page/欧洲.md "wikilink")[美也适宜用做壁炉燃料](../Page/美洲.md "wikilink")，但必须先干燥2至3年。刺槐花可以生吃，或者做成燜飯。但刺槐可以成为[蚜虫的宿主](../Page/蚜虫.md "wikilink")，必须注意防范。

## 外部链接

  - [刺槐图片](https://web.archive.org/web/20080512151958/http://www.cas.vanderbilt.edu/bioimages/species/frame/rops.htm)

  - [刺槐](http://www.na.fs.fed.us/pubs/silvics_manual/volume_2/robinia/pseudoacacia.htm)

  - [作为入侵物种的刺槐](http://www.nps.gov/plants/ALIEN/fact/rops1.htm)

  - [刺槐
    Cihuai](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00572)
    藥用植物圖像資料庫 (香港浸會大學中醫藥學院)

[File:Robinia-pseudoacacia.JPG|花](File:Robinia-pseudoacacia.JPG%7C花)
<File:Robinia> pseudoacacia - Ardenne.JPG|花序 <File:Esneux>
AR7aJPG.jpg|秋季的刺槐 <File:Black> Locust Endgrain.jpg|木材年轮

## 参考文献

[樹](../Category/藥用植物.md "wikilink")
[Category:刺槐属](../Category/刺槐属.md "wikilink")
[Category:树](../Category/树.md "wikilink")

1.