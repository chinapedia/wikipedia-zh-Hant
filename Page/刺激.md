在[神经生物学中](../Page/神经生物学.md "wikilink")，**刺激**（stimulus、stimuli）指的是[细胞膜的超过阈值的](../Page/细胞膜.md "wikilink")[去极化过程](../Page/去极化.md "wikilink")，它能激发[动作电位](../Page/动作电位.md "wikilink")。这种去极化通常能够是细胞外因素引起的。刺激通常被描述為具有**正效價**或**負效價**。

如果一种[物理或](../Page/物理.md "wikilink")[化学刺激能在](../Page/化学.md "wikilink")[感受器](../Page/感受器.md "wikilink")（例如在[感受细胞上的](../Page/感受细胞.md "wikilink")）引起[兴奋](../Page/兴奋.md "wikilink")，就被称之为“适宜刺激”（Aqequate
Stimulus）。感受器会因应刺激而传递信号（如[动作电位](../Page/动作电位.md "wikilink")）。有时这些信号可被生物體作为一种[感觉接受](../Page/感觉.md "wikilink")，如[视觉](../Page/视觉.md "wikilink")。

生物体不但对外界刺激，同时对内在的刺激也会作出反应。一个适宜刺激之后是一个反应。（但这种反应可以被后续的调节所压制）。这条规则不但适用于个体中器官之间，也适用于环境中个体与个体之间。在个体中，神经元是该过程的体现者，它们通过[突触与中枢和外周](../Page/突触.md "wikilink")[神经节相联系](../Page/神经节.md "wikilink")。在那刺激会被分析整合，并触发反应。

在[植物中](../Page/植物.md "wikilink")，信号的传递靠的仅仅是化学反应。光对植物来说是非常重要的刺激，其他的刺激分别为温度，化学物质，[重力等](../Page/重力.md "wikilink")。这种影响导致反应发生。在整合的过程中，不同的刺激会相互影响。但[反射却引发的反应总是无意识的](../Page/反射_\(生理学\).md "wikilink")。

感觉会对刺激的[光谱和强度](../Page/光谱.md "wikilink")（[听阈](../Page/听阈.md "wikilink")）的反映。人类对于下列刺激会有如下感觉：

  - [触觉](../Page/触觉.md "wikilink")（[皮肤](../Page/皮肤.md "wikilink")）-
    压/痛觉，温度
  - [味觉](../Page/味觉.md "wikilink")（[舌头](../Page/舌头.md "wikilink")）-
    咸，酸,甜，苦
  - [嗅觉](../Page/嗅觉.md "wikilink")（[鼻](../Page/鼻.md "wikilink")）-
    气味分子（部分与味觉重合）
  - [视觉](../Page/视觉.md "wikilink")（[眼](../Page/眼.md "wikilink")）- 光和颜色
  - [听觉](../Page/听觉.md "wikilink")（[耳](../Page/耳.md "wikilink")）-
    [声波](../Page/声波.md "wikilink")

除此之外还有很多其他刺激，如[磁场和](../Page/磁场.md "wikilink")[超声](../Page/超声.md "wikilink")，但不能为人类所感觉得到。

## 社会刺激

**社会刺激**在[社会心理学中指的是在不同的社会成员能引起不同感受的刺激](../Page/社会心理学.md "wikilink")。最知名的例子為[香港社会](../Page/香港社会.md "wikilink")\[1\]，故需十分[密切朋友或](../Page/密切朋友.md "wikilink")[親屬](../Page/親屬.md "wikilink")。

另例是一枚硬币，**[穷人](../Page/穷人.md "wikilink")**会在物理方面更好的感受到它的存在，但仍然不会放下[社会上的](../Page/社会.md "wikilink")**炫耀**刺激。\[2\]

## 概念区分：刺激和兴奋

刺激（如热，压力和痛等）是外在的，例如它会在皮肤的感觉细胞（感受器）发生作用。刺激会在神经细胞引起[电位变化](../Page/电位.md "wikilink")，这宗电位变化被称之为**冲动**。心臟的兴奋或者是兴奋的传导却不需要刺激的存在。只有[电信号](../Page/电信号.md "wikilink")（但不是电刺激\!）才能为神经纤维所传导。

## 相反

  - [心境](../Page/心境.md "wikilink")
  - [人情味](../Page/人情味.md "wikilink")
  - [包容性](../Page/包容性.md "wikilink")

## 參见

  - [道德](../Page/道德.md "wikilink")
  - [情緒](../Page/情緒.md "wikilink")

## 參考文獻

[pl:Bodziec
(psychologia)](../Page/pl:Bodziec_\(psychologia\).md "wikilink")

[Category:動物行為學](../Category/動物行為學.md "wikilink")
[Category:神經科學](../Category/神經科學.md "wikilink")

1.  [:香港學生自殺事件列表](../Page/:香港學生自殺事件列表.md "wikilink")
2.  [社会刺激与社会行为之间的中介过程是什么？](https://m.iask.sina.com.cn/b/L0MiZDDrxV.html)