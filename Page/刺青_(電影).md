是[周美玲導演的一部](../Page/周美玲.md "wikilink")[台灣電影](../Page/台灣電影.md "wikilink")，[三映電影製作](../Page/三映電影.md "wikilink")，由[楊丞琳](../Page/楊丞琳.md "wikilink")、[梁洛施領銜主演](../Page/梁洛施.md "wikilink")，是一部以[刺青為象徵](../Page/刺青.md "wikilink")，探討愛情、親情、友情的[女同性戀電影](../Page/女同性戀.md "wikilink")。其獲得2005年[中華民國](../Page/中華民國.md "wikilink")[行政院新聞局](../Page/行政院新聞局.md "wikilink")[輔導金補助](../Page/國片製作輔導金.md "wikilink")。

## 角色

| 演員                               | 角色    | 背景                                                                               |
| -------------------------------- | ----- | -------------------------------------------------------------------------------- |
| [楊丞琳](../Page/楊丞琳.md "wikilink") | 小綠    | 白天是學生，晚上是視訊情人。表面看來膚淺的女孩實則內心複雜。                                                   |
| [梁洛施](../Page/梁洛施.md "wikilink") | 竹子    | [刺青師傅](../Page/刺青.md "wikilink")。因為[地震的意外而將自己的內心封閉起來](../Page/地震.md "wikilink")。 |
| [陳意涵](../Page/陳意涵.md "wikilink") | 真真    | 竹子的初戀情人，竹子傷痛的一段過往。                                                               |
| [是元介](../Page/是元介.md "wikilink") | 阿東    | 小混混，常找竹子刺青，希望藉由刺青而獲得力量。                                                          |
| [沈建宏](../Page/沈建宏.md "wikilink") | 阿青    | 竹子的弟弟，因為地震而失去了有關家的記憶，患得[解離症](../Page/解離症.md "wikilink")。                         |
| [謝曜仲](../Page/謝曜仲.md "wikilink") | 大宇    | [網路警察](../Page/網路警察.md "wikilink")，喜歡上當視訊情人的小綠。                                  |
| [米七偶](../Page/米七偶.md "wikilink") | 竹子的師父 | 應竹子要求在她的手臂上刺下彼岸花刺青，並傳授竹子刺青技術及真義。                                                 |

## 劇情

## 宣傳與票房

票房表現部分，在電影上映前，導演及演員即到各地進行密集宣傳，加上電影主題與演員陣容搭配，吸引不少注意。[台灣在](../Page/台灣.md "wikilink")2007年3月30日上映後，第一個星期即開出440萬[新台幣的票房佳績](../Page/新台幣.md "wikilink")，隨後在口碑及媒體持續宣傳下，台灣票房不久超過1000萬。到一線電影院全部下片為止，大約有1420萬的票房成績。[香港在](../Page/香港.md "wikilink")4月12日上映後，也開出不錯的成績，至一線電影院下片為止，共累積超過315萬[港幣](../Page/港幣.md "wikilink")，以匯率1:4.26換算後，香港票房也超過1300萬[新台幣](../Page/新台幣.md "wikilink")。刺青成為少數能在台灣和香港票房都超過1300萬的[台灣電影](../Page/台灣電影.md "wikilink")。

## 得獎

1.  獲2007年[柏林影展](../Page/柏林影展.md "wikilink")「[泰迪熊獎](../Page/泰迪熊獎.md "wikilink")」最佳影片
2.  香港電影節
3.  都靈同志國際影展
4.  米蘭同志國際影展

## 電影配樂

由台灣[後搖滾樂團](../Page/後搖滾.md "wikilink")[甜梅號](../Page/甜梅號.md "wikilink")（[Sugar
Plum
Ferry](../Page/Sugar_Plum_Ferry.md "wikilink")）吉他手**[昆蟲白](../Page/昆蟲白.md "wikilink")**（insecteens）擔綱製作，收錄**[昆蟲白](../Page/昆蟲白.md "wikilink")**、**陳建騏**、**張見宇**為《刺青》所創作的十六首配樂作品。主題曲「**小茉莉**」（作詞：林書宇
/
[周美玲](../Page/周美玲.md "wikilink")，作曲：[昆蟲白](../Page/昆蟲白.md "wikilink")）於2007年**入圍**第44屆**[金馬獎](../Page/金馬獎.md "wikilink")**「**最佳原創電影歌曲**」。

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯封面</p></th>
<th style="text-align: left;"><p>專輯資訊</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p><a href="https://zh.wikipedia.org/wiki/File:Spider_Lilies_Soundtrack.jpg" title="fig:Spider_Lilies_Soundtrack.jpg">Spider_Lilies_Soundtrack.jpg</a></p></td>
<td style="text-align: left;"><p>《<a href="../Page/刺青_/_電影音樂概念專輯.md" title="wikilink">刺青 / 電影音樂概念專輯</a>》</p>
<ul>
<li>創作人：<a href="../Page/昆蟲白.md" title="wikilink">昆蟲白</a> / 陳建騏 / 張見宇</li>
<li>發行日期：2007年4月27日(臺灣)</li>
<li>發行公司：喜瑪拉雅唱片</li>
</ul></td>
</tr>
</tbody>
</table>

## 周邊商品

  - 電影書
  - 電影小說
  - 電影劇本書
  - 海報
  - [原聲CD](../Page/刺青_/_電影音樂概念專輯.md "wikilink")
  - 珍藏版DVD

## 得獎紀錄

  - 2007年[柏林影展](../Page/第57届柏林国际电影节.md "wikilink")
    入圍電影大觀(Panorama)單元、獲同志電影「[泰迪熊獎](../Page/泰迪熊獎.md "wikilink")」

## 外部連結

  - [刺青官方部落格](http://www.wretch.cc/blog/spiderlily)

  -
  - {{@movies|fstw20891457|刺青}}

  -
  -
  -
  -
  -
  -
[Category:2007年電影](../Category/2007年電影.md "wikilink")
[Category:台灣LGBT相關電影](../Category/台灣LGBT相關電影.md "wikilink")
[Category:女同性戀相關電影](../Category/女同性戀相關電影.md "wikilink")
[Category:台灣浪漫劇情片](../Category/台灣浪漫劇情片.md "wikilink")
[Category:2000年代浪漫劇情片](../Category/2000年代浪漫劇情片.md "wikilink")
[7](../Category/2000年代台灣電影作品.md "wikilink")