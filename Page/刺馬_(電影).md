是[香港](../Page/香港.md "wikilink")[邵氏公司](../Page/邵氏公司.md "wikilink")1973年拍攝有關[馬新貽案的電影](../Page/馬新貽.md "wikilink")。

## 故事大綱

張文祥（[-{姜}-大衛飾](../Page/姜大衛.md "wikilink")）、黃縱（[陳觀泰飾](../Page/陳觀泰.md "wikilink")）與黃妻（[井莉飾](../Page/井莉.md "wikilink")）因長於亂世，故同淪為草莽，張文祥、黃縱與馬新貽（[狄龍飾](../Page/狄龍.md "wikilink")）在巧合下，成為異姓兄弟，三人同為軍隊效命，當中馬更拜[兩江总督銜](../Page/兩江总督.md "wikilink")，但馬對黃縱作出「[勾義嫂](../Page/勾義嫂.md "wikilink")」行為，並暗殺黃縱，當張文祥得悉後，計劃於校場殺馬為義兄報仇...
本片劇情根柢《[清末四大奇案](../Page/清末四大奇案.md "wikilink")》《[刺馬案](../Page/刺馬案.md "wikilink")》，清末其他奇案為《[楊乃武與小白菜](../Page/楊乃武與小白菜.md "wikilink")》、《[楊月樓奇案](../Page/楊月樓奇案.md "wikilink")》、《[太原奇案](../Page/太原奇案.md "wikilink")》。

## 獎項

榮獲[第11屆金馬獎優秀演技特別獎](../Page/第11屆金馬獎.md "wikilink")（狄龍）

## 相關

  - [馬新貽](../Page/馬新貽.md "wikilink")
  - [投名狀](../Page/投名狀.md "wikilink")

## 外部連結

  - {{@movies|fbhk20065045|刺馬}}

  -
  -
  -
  -
  -
[3](../Category/1970年代香港電影作品.md "wikilink")
[Category:邵氏電影](../Category/邵氏電影.md "wikilink")
[Category:1970年代動作片](../Category/1970年代動作片.md "wikilink")
[Category:香港動作片](../Category/香港動作片.md "wikilink")
[Category:同治時期背景電影](../Category/同治時期背景電影.md "wikilink")
[Category:张彻电影](../Category/张彻电影.md "wikilink")