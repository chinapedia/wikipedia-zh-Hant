**刺鳅科**是[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[合鰓目的其中一科](../Page/合鰓目.md "wikilink")。

## 分類

**刺鳅科**下分3個屬，如下：

### 吻棘鰍屬（*Macrognathus*）

  -   - [長吻棘鰍](../Page/長吻棘鰍.md "wikilink")（*Macrognathus aculeatus*）。
      - [大吻棘鰍](../Page/大吻棘鰍.md "wikilink")（*Macrognathus aral*）
      - [金色吻棘鰍](../Page/金色吻棘鰍.md "wikilink")（*Macrognathus aureus*）
      - [睛尾吻棘鰍](../Page/睛尾吻棘鰍.md "wikilink")（*Macrognathus
        caudiocellatus*）
      - [環帶吻棘鰍](../Page/環帶吻棘鰍.md "wikilink")（*Macrognathus
        circumcinctus*）
      - [背眼吻棘鰍](../Page/背眼吻棘鰍.md "wikilink")（*Macrognathus
        dorsiocellatus*）
      - [岡氏吻棘鰍](../Page/岡氏吻棘鰍.md "wikilink")（*Macrognathus guentheri*）
      - [基氏吻棘鰍](../Page/基氏吻棘鰍.md "wikilink")（*Macrognathus keithi*）
      - [線斑吻棘鰍](../Page/線斑吻棘鰍.md "wikilink")（*Macrognathus
        lineatomaculatus*）
      - [斑吻棘鰍](../Page/斑吻棘鰍.md "wikilink")（*Macrognathus maculatus*）
      - [馬拉巴吻棘鰍](../Page/馬拉巴吻棘鰍.md "wikilink")（*Macrognathus
        malabaricus*）
      - [泰國吻棘鰍](../Page/泰國吻棘鰍.md "wikilink")（*Macrognathus
        meklongensis*）
      - [莫里亨吻棘鰍](../Page/莫里亨吻棘鰍.md "wikilink")（*Macrognathus
        morehensis*）
      - [暗體吻棘鰍](../Page/暗體吻棘鰍.md "wikilink")（*Macrognathus obscurus*）
      - [大頭吻棘鰍](../Page/大頭吻棘鰍.md "wikilink")（*Macrognathus pancalus*）
      - [孔雀吻棘鰍](../Page/孔雀吻棘鰍.md "wikilink")（*Macrognathus pavo*）
      - [潘氏吻棘鰍](../Page/潘氏吻棘鰍.md "wikilink")（*Macrognathus
        pentophthalmos*）
      - [半斑吻棘鰍](../Page/半斑吻棘鰍.md "wikilink")（*Macrognathus
        semiocellatus*）
      - [曼谷吻棘鰍](../Page/曼谷吻棘鰍.md "wikilink")（*Macrognathus siamensis*）
      - [腹紋吻棘鰍](../Page/腹紋吻棘鰍.md "wikilink")（*Macrognathus
        taeniagaster*）
      - [濘棲吻棘鰍](../Page/濘棲吻棘鰍.md "wikilink")（*Macrognathus tapirus*）
      - [斑紋吻棘鰍](../Page/斑紋吻棘鰍.md "wikilink")（*Macrognathus zebrinus*）

### 刺鰍屬（*Mastacembelus*）

  -   - [白點刺鰍](../Page/白點刺鰍.md "wikilink")（*Mastacembelus alboguttatus*）
      - [白斑刺鰍](../Page/白斑刺鰍.md "wikilink")（*Mastacembelus
        albomaculatus*）
      - [安氏刺鰍](../Page/安氏刺鰍.md "wikilink")（*Mastacembelus ansorgii*）
      - [缺鰭刺鰍](../Page/缺鰭刺鰍.md "wikilink")（*Mastacembelus apectoralis*）
      - [大刺鰍](../Page/大刺鰍.md "wikilink")（*Mastacembelus armatus*）
      - [烏首刺鰍](../Page/烏首刺鰍.md "wikilink")（*Mastacembelus aviceps*）
      - [短吻刺鳅](../Page/短吻刺鳅.md "wikilink")（*Mastacembelus brachyrhinus*）
      - [盲刺鳅](../Page/盲刺鳅.md "wikilink")（*Mastacembelus brichardi*）
      - [卡氏刺鳅](../Page/卡氏刺鳅.md "wikilink")（*Mastacembelus catchpolei*）
      - [康吉刺鳅](../Page/康吉刺鳅.md "wikilink")（*Mastacembelus congicus*）
      - [厚身刺鳅](../Page/厚身刺鳅.md "wikilink")（*Mastacembelus crassus*）
      - [隱棘刺鳅](../Page/隱棘刺鳅.md "wikilink")（*Mastacembelus
        cryptacanthus*）
      - [坎寧頓刺鳅](../Page/坎寧頓刺鳅.md "wikilink")（*Mastacembelus
        cunningtoni*）
      - [戴氏刺鰍](../Page/戴氏刺鰍.md "wikilink")（*Mastacembelus dayi*）
      - [德氏刺鰍](../Page/德氏刺鰍.md "wikilink")（*Mastacembelus decorsei*）
      - [奠邊刺鰍](../Page/奠邊刺鰍.md "wikilink")（*Mastacembelus
        dienbienensis*）
      - [不育刺鰍](../Page/不育刺鰍.md "wikilink")（*Mastacembelus ellipsifer*）
      - [紅紋刺鰍](../Page/紅紋刺鰍.md "wikilink")（*Mastacembelus
        erythrotaenia*）
      - [網紋刺鰍](../Page/網紋刺鰍.md "wikilink")（*Mastacembelus favus*）
      - [黃體刺鰍](../Page/黃體刺鰍.md "wikilink")（*Mastacembelus flavidus*）
      - [長尾刺鰍](../Page/長尾刺鰍.md "wikilink")（*Mastacembelus frenatus*）
      - [格氏刺鰍](../Page/格氏刺鰍.md "wikilink")（*Mastacembelus greshoffi*）
      - [幾內亞刺鰍](../Page/幾內亞刺鰍.md "wikilink")（*Mastacembelus
        kakrimensis*）
      - [潛刺鰍](../Page/潛刺鰍.md "wikilink")（*Mastacembelus latens*）
      - [賴比瑞亞刺鰍](../Page/賴比瑞亞刺鰍.md "wikilink")（''Mastacembelus
        liberiensis ''）
      - [洛氏刺鰍](../Page/洛氏刺鰍.md "wikilink")（''Mastacembelus loennbergii
        ''）
      - [馬氏刺鰍](../Page/馬氏刺鰍.md "wikilink")（''Mastacembelus marchei ''）
      - [真刺鰍](../Page/真刺鰍.md "wikilink")（''Mastacembelus mastacembelus
        ''）
      - [細胸刺鰍](../Page/細胸刺鰍.md "wikilink")（''Mastacembelus micropectus
        ''）
      - [莫湖刺鰍](../Page/莫湖刺鰍.md "wikilink")（''Mastacembelus moeruensis
        ''）
      - [摩氏刺鰍](../Page/摩氏刺鰍.md "wikilink")（*Mastacembelus moorii*）
      - [黑刺鰍](../Page/黑刺鰍.md "wikilink")（*Mastacembelus niger*）
      - [黑緣刺鰍](../Page/黑緣刺鰍.md "wikilink")（''Mastacembelus
        nigromarginatus ''）
      - [背眼刺鰍](../Page/背眼刺鰍.md "wikilink")（*Mastacembelus
        notophthalmus*）
      - [因萊湖刺鰍](../Page/因萊湖刺鰍.md "wikilink")（*Mastacembelus oatesii*）
      - [蜥形刺鰍](../Page/蜥形刺鰍.md "wikilink")（''Mastacembelus ophidium ''）
      - [白腹刺鰍](../Page/白腹刺鰍.md "wikilink")（''Mastacembelus pantherinus
        ''）
      - [少鱗刺鰍](../Page/少鱗刺鰍.md "wikilink")（''Mastacembelus paucispinis
        ''
      - [橫口刺鰍](../Page/橫口刺鰍.md "wikilink")（''Mastacembelus plagiostomus
        ''）
      - [扁體刺鰍](../Page/扁體刺鰍.md "wikilink")（''Mastacembelus platysoma ''）
      - [波氏刺鰍](../Page/波氏刺鰍.md "wikilink")（''Mastacembelus polli ''）
      - [帕倫刺鰍](../Page/帕倫刺鰍.md "wikilink")（''Mastacembelus praensis ''）
      - [雷氏刺鰍](../Page/雷氏刺鰍.md "wikilink")（''Mastacembelus reygeli ''）
      - [勞氏刺鰍](../Page/勞氏刺鰍.md "wikilink")（''Mastacembelus robertsi ''）
      - [薩氏刺鰍](../Page/薩氏刺鰍.md "wikilink")（''Mastacembelus sanagali ''）
      - [塞氏刺鰍](../Page/塞氏刺鰍.md "wikilink")（''Mastacembelus seiteri ''）
      - [喀麥隆刺鰍](../Page/喀麥隆刺鰍.md "wikilink")（''Mastacembelus
        sexdecimspinus ''）
      - [剛果刺鰍](../Page/剛果刺鰍.md "wikilink")（''Mastacembelus
        shiloangoensis ''）
      - [馬拉維刺鰍](../Page/馬拉維刺鰍.md "wikilink")（''Mastacembelus shiranus
        ''）
      - [小眼刺鰍](../Page/小眼刺鰍.md "wikilink")（''Mastacembelus signatus ''）
      - [辛比刺鰍](../Page/辛比刺鰍.md "wikilink")（*Mastacembelus simbi*）
      - [斯氏刺鰍](../Page/斯氏刺鰍.md "wikilink")（*Mastacembelus stappersii*）
      - [尖頭刺鰍](../Page/尖頭刺鰍.md "wikilink")（*Mastacembelus strigiventus*）
      - [太安刺鰍](../Page/太安刺鰍.md "wikilink")（*Mastacembelus taiaensis*）
      - [坦干伊喀刺鰍](../Page/坦干伊喀刺鰍.md "wikilink")（*Mastacembelus
        tanganicae*）
      - [塔平刺鰍](../Page/塔平刺鰍.md "wikilink")（*Mastacembelus thacbaensis*）
      - [廷氏刺鰍](../Page/廷氏刺鰍.md "wikilink")（*Mastacembelus tinwini*）
      - [特氏刺鰍](../Page/特氏刺鰍.md "wikilink")（*Mastacembelus traversi*）
      - [三疣刺鰍](../Page/三疣刺鰍.md "wikilink")（*Mastacembelus triolobus*）
      - [三棘刺鰍](../Page/三棘刺鰍.md "wikilink")（*Mastacembelus trispinosus*）
      - [波狀刺鰍](../Page/波狀刺鰍.md "wikilink")（*Mastacembelus undulatus*）
      - [單色刺鰍](../Page/單色刺鰍.md "wikilink")（*Mastacembelus unicolor*）
      - [眼斑刺鰍](../Page/眼斑刺鰍.md "wikilink")（*Mastacembelus vanderwaali*）
      - [條紋刺鰍](../Page/條紋刺鰍.md "wikilink")（*Mastacembelus zebratus*）

### 中華刺鰍屬（*Sinobdella*）

  -   - [中華刺鰍](../Page/中華刺鰍.md "wikilink")（*Sinobdella sinensis*）

[\*](../Category/刺鳅科.md "wikilink")