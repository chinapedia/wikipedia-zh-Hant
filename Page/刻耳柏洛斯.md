\-{H|zh-cn:赫剌克勒斯;zh-tw:海克力斯;zh-hk:海格力斯}-
-{H|zh-cn:百变小樱;zh-tw:庫洛魔法使;zh-hk:百變小櫻}-
[Cerberus-Blake.jpeg](https://zh.wikipedia.org/wiki/File:Cerberus-Blake.jpeg "fig:Cerberus-Blake.jpeg")

**刻耳柏洛斯**（，*Kerberos*，，，），也稱**克伯羅斯**、**基路比羅斯**、**塞爾伯洛斯**、**賽伯拉斯**、**色柏洛斯**，[希腊神话中看守](../Page/希腊神话.md "wikilink")[冥界入口的](../Page/冥界_\(古希臘\).md "wikilink")[恶犬](../Page/狗.md "wikilink")，所以也譯為**地獄犬**。[赫西俄德在](../Page/赫西俄德.md "wikilink")《[神谱](../Page/神谱.md "wikilink")》中說此犬有50个头，而后来的一些艺术作品则大多表现它有3个头（可能是为了便于雕刻所致）；因此在[漢語語境裡](../Page/漢語.md "wikilink")（尤其是通俗文化中）也常称这怪物为**地獄三頭犬**。其名在古希腊语中的意思是为“黑暗中的恶魔”\[1\]。

## 传说

[Peter_Paul_Rubens_-_Hercules_and_Cerberus,_1636.jpg](https://zh.wikipedia.org/wiki/File:Peter_Paul_Rubens_-_Hercules_and_Cerberus,_1636.jpg "fig:Peter_Paul_Rubens_-_Hercules_and_Cerberus,_1636.jpg")，1636\]\]
按照傳統說法，刻耳柏洛斯为眾妖之祖[堤丰與蛇身女怪](../Page/堤丰.md "wikilink")[厄客德娜所生](../Page/厄客德娜.md "wikilink")。这怪物的外形凶恶可怕：它有许多个頭，還長著[蛇的尾巴](../Page/蛇.md "wikilink")，脖子上也盤繞著毒蛇。[海克力士把這狗從冥界带到地上後](../Page/海克力士.md "wikilink")，它在狂吠時從嘴里喷出毒液，毒液所落之處生出了[烏頭属植物](../Page/烏頭属.md "wikilink")（[奥维德在](../Page/奥维德.md "wikilink")《[變形記](../Page/變形記.md "wikilink")》裡描寫，[美狄亞用烏頭的汁液製成毒劑](../Page/美狄亞.md "wikilink")）。

刻耳柏洛斯住在[冥河岸邊](../Page/冥河.md "wikilink")（在希臘神話中，死人在進入冥界時要先乘坐[卡戎](../Page/卡戎.md "wikilink")-{划}-的船渡渡冥河），為冥王[哈帝斯看守冥界的大門](../Page/哈帝斯.md "wikilink")。刻耳柏洛斯允許每一個死者的[靈魂進入冥界](../Page/靈魂.md "wikilink")，但不讓任何人出去（同時也不允許活人出入）。因此在希腊神话與[羅馬神話中只有寥寥數人從冥界生還](../Page/羅馬神話.md "wikilink")。當中包括：

  - [阿耳戈船英雄中的](../Page/阿耳戈船英雄.md "wikilink")[俄耳甫斯](../Page/俄耳甫斯.md "wikilink")，他为了把死去的妻子從冥界带回而進入地府，用美妙的歌声使刻耳柏洛斯入睡，因而得以走出冥界；
  - [特洛伊英雄](../Page/特洛伊.md "wikilink")[埃涅阿斯为了解自己的命运而下冥界时](../Page/埃涅阿斯.md "wikilink")，[库迈的西彼拉](../Page/库迈的西彼拉.md "wikilink")（一个女先知）诱使刻耳柏洛斯吃了含有催眠草的饼，令它昏睡过去（[维吉尔](../Page/维吉尔.md "wikilink")，《[埃涅阿斯纪](../Page/埃涅阿斯纪.md "wikilink")》）；
  - 不过最有名的还要数[海克力士](../Page/海克力士.md "wikilink")，他奉[歐律斯透斯之命把刻耳柏洛斯從冥界抓了出来](../Page/歐律斯透斯.md "wikilink")，後来又按照歐律斯透斯的命令把它放回。活捉刻耳柏洛斯是海克力士的十二项功绩之一。

另外，[奥林匹斯十二主神中的](../Page/奥林匹斯十二主神.md "wikilink")[赫耳墨斯曾用冥河的河水使刻耳柏洛斯入睡](../Page/赫耳墨斯.md "wikilink")；在一个著名的关于爱神[厄罗斯的故事中](../Page/厄罗斯.md "wikilink")，厄罗斯的情人[賽姬也曾用含有催眠药的饼使刻耳柏洛斯昏睡过去](../Page/賽姬.md "wikilink")（[阿普列尤斯](../Page/阿普列尤斯.md "wikilink")，《[金驴记](../Page/金驴记.md "wikilink")》）。

古希腊人有在死者的[棺材里放一块蜜饼的习俗](../Page/棺材.md "wikilink")，据说就是为了讨好刻耳柏洛斯，他们希望死人可以把这块饼送给恶犬。

在[意大利南部](../Page/意大利.md "wikilink")[库迈地方对一座古代神殿](../Page/库迈.md "wikilink")（供奉哈得斯和[珀耳塞福涅](../Page/珀耳塞福涅.md "wikilink")）的[考古发掘中](../Page/考古学.md "wikilink")，考古学家发现神殿入口旁的墙上有用于拴狗的铁链。这些狗在古代可能是用来代表刻耳柏洛斯的。

在惡魔學中被視為在[所羅門七十二柱魔神中排第](../Page/所羅門七十二柱魔神.md "wikilink")24位的魔神[納貝流士的原型](../Page/納貝流士.md "wikilink")。

## 大眾文化

  - [哈利波特神祕魔法石中的三頭犬毛毛](../Page/哈利波特.md "wikilink")。
  - [靈異教師神眉裡的地獄三頭犬](../Page/靈異教師神眉.md "wikilink")。
  - [金田一少年之事件簿的](../Page/金田一少年之事件簿.md "wikilink")《魔犬森林殺人事件》單元，兇手的代號。
  - [鬼燈的冷徹裡的地獄三頭犬](../Page/鬼燈的冷徹.md "wikilink")。
  - [地獄三頭犬的日常的刻耳柏洛斯](../Page/地獄三頭犬的日常.md "wikilink")。
  - [海賊王的地獄三頭犬](../Page/海賊王.md "wikilink")
  - [神魔之塔的封印卡No](../Page/神魔之塔.md "wikilink").852
  - [幻想神域的源神賽伯拉斯](../Page/幻想神域.md "wikilink")
  - [庫洛魔法使的小可](../Page/庫洛魔法使.md "wikilink")（可魯貝洛斯，Cerberus）
  - [闇影詩章的傳說卡凱爾貝洛斯](../Page/闇影詩章.md "wikilink")。
  - [戰鬥陀螺 鋼鐵奇兵的陀螺地獄魔犬](../Page/戰鬥陀螺_鋼鐵奇兵.md "wikilink") BD145DS（地獄犬座）

## 注释

## 资料来源

  - 《世界神话辞典》，辽宁人民出版社，ISBN 7-205-00960-X
  - 《神话辞典》，（[苏联](../Page/苏联.md "wikilink")）M·H·鲍特文尼克等，统一书号：2017·304

## 外部链接

  - [Theoi Project,
    Cerberus](http://www.theoi.com/Ther/KuonKerberos.html)
  - [The Dog of Hades](http://www.gutenberg.org/etext/19119)
  - [Cerberus Capital Management, L.P.](http://www.cerberuscapital.com)
  - [An Engraving of
    Cerebus](http://www.vampyra.com/demons/cerberus.htm)

[Category:希腊神话生物](../Category/希腊神话生物.md "wikilink")
[Category:神話傳說中的狗](../Category/神話傳說中的狗.md "wikilink")
[Category:有複數生物特徵的傳說生物](../Category/有複數生物特徵的傳說生物.md "wikilink")
[Category:有複數頭部的傳說生物](../Category/有複數頭部的傳說生物.md "wikilink")
[Category:怪物](../Category/怪物.md "wikilink")

1.  （“惡運、死亡”）＋（“黑暗”），兩個字合起來似乎沒有什麼涵義，不過分開的字對照的都是冥界的現象，死亡和黑暗，所以刻耳柏洛斯理所當然的出現在冥界。雖然神話中並無記載為刻耳柏洛斯會守衛在冥界。