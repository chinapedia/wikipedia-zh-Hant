前2200年至前2101年的这一段期间被称为**前22世纪**。

## 重要事件、发展与成就

  - **科学技术**

<!-- end list -->

  - **战争与政治**
      - 約前2193年：[古提](../Page/古提.md "wikilink")（Guti）人滅[阿卡德](../Page/阿卡德.md "wikilink")，統治[美索不達米亞南部](../Page/美索不達米亞.md "wikilink")，建立[古提姆](../Page/古提姆.md "wikilink")（Gutium）政權。美索不達米亞南部之蘇美爾-阿卡德時代結束。)
      - 約前2185年：[華夏部落聯盟盟主](../Page/華夏.md "wikilink")[堯以](../Page/堯.md "wikilink")[舜為攝政](../Page/舜.md "wikilink")，以便在其死後由[舜繼立](../Page/舜.md "wikilink")。其後舜也以相同方法選立禹為其繼承人，史稱「禪讓」制度。
      - 約前2181年：[古埃及](../Page/古埃及.md "wikilink")[古王國時代結束](../Page/古王國時代.md "wikilink")，進入[第一中間時期](../Page/第一中間時期.md "wikilink")（第七王朝（約前2173年）—第十一王朝（約前1991年）。在此時代埃及陷入政治分裂，各州州長互相爭雄，爆發[第一次埃及奴隸貧民大起义](../Page/第一次埃及奴隸貧民大起义.md "wikilink")。[底比斯和](../Page/底比斯.md "wikilink")[赫拉克里奧波利斯](../Page/赫拉克里奧波利斯.md "wikilink")（Heracleopolis）逐漸崛起，分別成為南方和北方之政治中心。
      - 約前2150年：[古地亞繼為](../Page/古地亞.md "wikilink")[拉加什國王](../Page/拉加什.md "wikilink")。在位期間向[古提姆繳納大量貢賦](../Page/古提姆.md "wikilink")，取得獨立地位；並推行社會改革，復興[拉加什國勢](../Page/拉加什.md "wikilink")
      - 約前2133年：古埃及南部统治者[曼圖霍特普一世和](../Page/曼圖霍特普一世.md "wikilink")[因提夫一世割據底比斯](../Page/因提夫一世.md "wikilink")，战胜统一北部的[希拉克利奥波里](../Page/希拉克利奥波里.md "wikilink")，建立古埃及第十一王朝。埃及之[中王國時代開始](../Page/中王國時代.md "wikilink")。
      - 約前2120年：烏圖-赫加爾（Utu-hengal）繼任[烏魯克國王](../Page/烏魯克.md "wikilink")，率军打败库提王[梯雷根](../Page/梯雷根.md "wikilink")（Tirigan），將古提人逐出[美索不達米亞南部](../Page/美索不達米亞.md "wikilink")，推翻古提姆。
      - 約前2113年：烏爾-納姆(Ur-Nammu)击败[乌鲁克](../Page/乌鲁克.md "wikilink")，统一[两河流域南部](../Page/两河流域.md "wikilink")，建立[烏爾第三王朝](../Page/烏爾第三王朝.md "wikilink")。在位期間稱霸[美索不達米亞南部諸城邦](../Page/美索不達米亞.md "wikilink")，並頒佈《烏爾-納姆法典》。烏爾-納姆擊敗烏圖-赫加爾，滅烏魯克。此间，两河流域南部已经进入[青铜时代](../Page/青铜时代.md "wikilink")，[私有经济发展](../Page/私有经济.md "wikilink")、中央集权加强，并出现[奴隶制社会第一部法典](../Page/奴隶制社会.md "wikilink")《[乌尔纳姆法典](../Page/乌尔纳姆法典.md "wikilink")》，后为[埃兰人和](../Page/埃兰人.md "wikilink")[阿摩利人所灭](../Page/阿摩利人.md "wikilink")。

<!-- end list -->

  - **公元前22世纪的天灾人祸**

<!-- end list -->

  - **文化娱乐**
      -
<!-- end list -->

  - **疾病与医学**

<!-- end list -->

  - **环境与自然资源**

## 重要人物

### 世界領導人

### 科学家

### 军事领袖

### 艺术家

  -
### 哲学家

[前22世纪](../Category/前22世纪.md "wikilink")
[-22](../Category/世纪.md "wikilink")
[Category:前3千纪](../Category/前3千纪.md "wikilink")