<table>
<tbody>
<tr class="odd">
<td><p><small><strong><a href="../Page/世纪.md" title="wikilink">世纪</a>：</strong></small></p></td>
<td style="text-align: center;"><p><small> <a href="../Page/前9世纪.md" title="wikilink">前9世纪</a> | <strong><a href="../Page/前8世纪.md" title="wikilink">前8世纪</a></strong> | <a href="../Page/前7世纪.md" title="wikilink">前7世纪</a></small><br />
</p></td>
</tr>
<tr class="even">
<td><p><small><strong><a href="../Page/年代.md" title="wikilink">年代</a>：</strong></small></p></td>
<td style="text-align: center;"><p><small> <a href="../Page/前720年代.md" title="wikilink">前720年代</a> <a href="../Page/前710年代.md" title="wikilink">前710年代</a> | <em>' <a href="../Page/前700年代.md" title="wikilink">前700年代</a></em>' | <a href="../Page/前690年代.md" title="wikilink">前690年代</a> <a href="../Page/前680年代.md" title="wikilink">前680年代</a></small><br />
</p></td>
</tr>
<tr class="odd">
<td><p><small><strong><a href="../Page/年.md" title="wikilink">年份</a>：</strong></small></p></td>
<td style="text-align: center;"><p><small><a href="../Page/前711年.md" title="wikilink">前711年</a> <a href="../Page/前710年.md" title="wikilink">前710年</a> <a href="../Page/前709年.md" title="wikilink">前709年</a> <a href="../Page/前708年.md" title="wikilink">前708年</a> <a href="../Page/前707年.md" title="wikilink">前707年</a> | <strong>前706年</strong> | <a href="../Page/前705年.md" title="wikilink">前705年</a> <a href="../Page/前704年.md" title="wikilink">前704年</a> <a href="../Page/前703年.md" title="wikilink">前703年</a> <a href="../Page/前702年.md" title="wikilink">前702年</a> <a href="../Page/前701年.md" title="wikilink">前701年</a> </small><br />
</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td style="text-align: center;"><p> </p></td>
</tr>
<tr class="odd">
<td><p><strong>中国传统纪年：</strong></p></td>
<td style="text-align: center;"><p><small>'''<a href="../Page/周桓王.md" title="wikilink">周桓王林十四年</a>；<a href="../Page/鲁桓公.md" title="wikilink">鲁桓公允六年</a>；<a href="../Page/齐僖公.md" title="wikilink">齐僖公禄父二十五年</a>；<a href="../Page/晋小子侯.md" title="wikilink">晋小子侯三年</a>；<a href="../Page/曲沃武公.md" title="wikilink">曲沃武公称十年</a>；<a href="../Page/卫宣公.md" title="wikilink">卫宣公晋十三年</a>；<a href="../Page/蔡桓侯.md" title="wikilink">蔡桓侯封人九年</a>；<a href="../Page/郑庄公.md" title="wikilink">郑庄公寤生三十八年</a>；<a href="../Page/曹桓公.md" title="wikilink">曹桓公终生五十一年</a>；<a href="../Page/燕宣侯.md" title="wikilink">燕宣侯五年</a>；<a href="../Page/陈厉公.md" title="wikilink">陈厉公躍元年</a>；<a href="../Page/杞武公.md" title="wikilink">杞武公四十五年</a>；<a href="../Page/宋庄公.md" title="wikilink">宋庄公冯四年</a>；<a href="../Page/秦宁公.md" title="wikilink">秦宁公十年</a>；<a href="../Page/楚武王.md" title="wikilink">楚武王熊通三十五年</a></small><br />
</p></td>
</tr>
</tbody>
</table>

-----

## 大事记

  - [蔡桓侯殺](../Page/蔡桓侯.md "wikilink")[陳侯佗立](../Page/陳佗.md "wikilink")[陳厲公躍](../Page/陳厲公.md "wikilink")。
  - [楚国国君熊通攻隨](../Page/楚国.md "wikilink")（[湖北](../Page/湖北.md "wikilink")[隨州](../Page/隨州.md "wikilink")），[隨國求和](../Page/隨國.md "wikilink")，並請[周桓王封楚為王](../Page/周桓王.md "wikilink")，周桓王不允\[1\]。
  - [北戎](../Page/北戎.md "wikilink")（河北東北部）攻齊，鄭[太子忽救齊](../Page/郑昭公.md "wikilink")，大敗北戎。
  - [紀國](../Page/紀國.md "wikilink")（山東壽光）受齊侵略，國危，紀侯赴魯朝覲，請轉乞[周桓王](../Page/周桓王.md "wikilink")，下令齊國與紀國和解。[魯桓公告以不能](../Page/魯桓公.md "wikilink")。

## 出生

## 逝世

## 参考

[前706年](../Category/前706年.md "wikilink")
[年6](../Category/前700年代.md "wikilink")
[Category:前8世纪各年](../Category/前8世纪各年.md "wikilink")

1.