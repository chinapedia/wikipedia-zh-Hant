在**前7千纪**（或称**前第7个千年**），[農業由](../Page/農業.md "wikilink")[安那托利亞傳播到](../Page/安那托利亞.md "wikilink")[巴爾幹半島](../Page/巴爾幹半島.md "wikilink")。

[世界人口維持](../Page/世界人口.md "wikilink")500萬人，世界各地的人類仍與狩獵為生。在中東的農業社會，牛被人類馴服，陶瓷開始常見，開始傳播到歐洲及南亞，第一個金屬制品制成。

## 文化

[CatalHoyukSouthArea.JPG](https://zh.wikipedia.org/wiki/File:CatalHoyukSouthArea.JPG "fig:CatalHoyukSouthArea.JPG")

  - **前7000年** —
    在[克里特島](../Page/克里特島.md "wikilink")[愛琴海發現使用陶瓷的新石器時代種族](../Page/愛琴海.md "wikilink")。
  - **前7000年** — 中國開始[裴李崗文化](../Page/裴李崗文化.md "wikilink")。
  - **前7000年** — [巴布亞人開始在新畿內亞發展農業](../Page/巴布亞人.md "wikilink")。\[1\]
  - **前7000年**–[前600年](../Page/前600年.md "wikilink") —
    [埃蘭](../Page/埃蘭.md "wikilink").
  - **前7000年** — [埃蘭成為農業地區](../Page/埃蘭.md "wikilink")。
  - **前6500年** —
    中國[舊石器時代結束](../Page/舊石器時代.md "wikilink")，進入[新石器時代](../Page/新石器時代.md "wikilink")。
  - **前6500年** — 中國出現[后李文化](../Page/后李文化.md "wikilink")。
  - **前6500年** — 中國開始[興隆洼文化](../Page/興隆洼文化.md "wikilink")。
  - **前6500年** — 中國開始[磁山文化](../Page/磁山文化.md "wikilink")。

## 生活習慣改變

  - **前7000年** — 中石器時代遺跡[Lepenski
    Vir在今日的](../Page/Lepenski_Vir.md "wikilink")[塞爾維亞發現](../Page/塞爾維亞.md "wikilink")。
  - **前7000年** — 非洲近東出現[陶器](../Page/陶器.md "wikilink")。
  - **前7000年** — [埃蘭成為農業地區](../Page/埃蘭.md "wikilink")。
  - **前7000年** —
    [中國開始種植](../Page/中國.md "wikilink")[白米](../Page/白米.md "wikilink")、[小麥](../Page/小麥.md "wikilink")、[小米及](../Page/小米.md "wikilink")[山药](../Page/山药.md "wikilink")(1990
    Rand McNally Atlas)。
  - **前7000年** —
    [辣椒](../Page/辣椒.md "wikilink")、[葫蘆](../Page/葫蘆.md "wikilink")、[鳄梨及](../Page/鳄梨.md "wikilink")[南瓜開始在](../Page/南瓜.md "wikilink")[危地馬拉](../Page/危地馬拉.md "wikilink")[太平洋海洋種植](../Page/太平洋海洋.md "wikilink")。
    (Bailey 1973)
  - **前6500年** -
    在斯堪的纳维亚發現兩頭犬。畜豬在[Jarmo出現](../Page/Jarmo.md "wikilink")。[土耳其出現](../Page/土耳其.md "wikilink")[歐洲牛](../Page/歐洲牛.md "wikilink")。
  - **前6000年** — [半坡定居點在中國出現](../Page/半坡遗址.md "wikilink")。
  - [農業開始在巴爾幹半島發展](../Page/農業.md "wikilink")。
  - 首次有[養蜂的記錄](../Page/養蜂.md "wikilink")。石畫在[非洲及](../Page/非洲.md "wikilink")[西班牙東部採集](../Page/西班牙.md "wikilink")[蜜糖](../Page/蜜糖.md "wikilink")，畫像表示蜂蜜在樹或石頭上，而蜜蜂則飛來飛土去
    —
    畫壁在[西班牙](../Page/西班牙.md "wikilink")[華倫西亞附近](../Page/華倫西亞.md "wikilink")。
  - [Pastoralism](../Page/Pastoralism.md "wikilink") and cultivation of
    [cereals](../Page/cereal.md "wikilink") (East
    [Sahara](../Page/Sahara.md "wikilink"))(  )
  - 開始使用[金和原](../Page/金.md "wikilink")[銅](../Page/銅.md "wikilink")( })
  - [中東](../Page/中東.md "wikilink"): 牛被家養化。 (  )
  - [北美洲](../Page/北美洲.md "wikilink"):
    [美國原住民開始使用石頭打磨食物及佞狩獵](../Page/美國原住民.md "wikilink")[美洲野牛及更細少的動物](../Page/美洲野牛.md "wikilink")。
  - [北美洲落葉植物首次在](../Page/北美洲.md "wikilink")[長島出現](../Page/長島.md "wikilink")。
  - [北美洲](../Page/北美洲.md "wikilink"):[北大西洋水溫約](../Page/北大西洋.md "wikilink")3–6 °C足夠融化。(
     })
  - [墨西哥](../Page/墨西哥.md "wikilink") — 開始有耕作。
  - [地中海東部](../Page/地中海東部.md "wikilink") — Forms of
    [pottery](../Page/pottery.md "wikilink") become decoration.
  - Animal figures of
    [Estuarine](../Page/Estuarine.md "wikilink")-period rock painting in
    [Australia](../Page/Australia.md "wikilink") include saltwater
    [fish](../Page/fish.md "wikilink") and
    [crocodiles](../Page/crocodile.md "wikilink")
    [Australia](../Page/Australia.md "wikilink").

## 環境轉變

  - **前7000年** —
    野馬數量在歐洲減少，馬匹在不列顛島發現，但從未出現於[愛爾蘭](../Page/愛爾蘭.md "wikilink")。
    (Horse & Man, Clutton-Brock)
  - **前7000年** — [英倫海峽形成](../Page/英倫海峽.md "wikilink")\[2\]
  - **前7000年** — [新石器時代氣候形成](../Page/新石器時代.md "wikilink")
  - **前6400年± 25年** —
    [俄羅斯](../Page/俄羅斯.md "wikilink")[堪察加半島](../Page/堪察加半島.md "wikilink")[Kurile火山達到](../Page/Kurile_Lake.md "wikilink")[VEI7級火山爆發](../Page/Volcanic_Explosivity_Index.md "wikilink")。是[全新紀最大規模的一次](../Page/全新紀.md "wikilink")。
  - **前6100年** —
    [海底崩移在](../Page/海底崩移.md "wikilink")[挪威海造成](../Page/挪威海.md "wikilink")[大海嘯](../Page/大海嘯.md "wikilink")。
  - **前6000年** —
    [托雷斯海峡水位上升](../Page/托雷斯海峡.md "wikilink")，將[澳洲及](../Page/澳洲.md "wikilink")[新畿內亞分離](../Page/新畿內亞.md "wikilink")。
  - **前6000年** — 由公元前12000元至前5000年，大量內陸浮起。

## 參考資料

[-07](../Category/千纪.md "wikilink")
[前7千纪](../Category/前7千纪.md "wikilink")

1.  [大英百科全書](../Page/大英百科全書.md "wikilink"), "Melanesian cultures"
2.  Roberts, J: "History of the World." Penguin, 1994.