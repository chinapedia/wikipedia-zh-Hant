[Dante_Gabriel_Rossetti_-_Proserpine.JPG](https://zh.wikipedia.org/wiki/File:Dante_Gabriel_Rossetti_-_Proserpine.JPG "fig:Dante_Gabriel_Rossetti_-_Proserpine.JPG")，由但丁·加百利·羅塞蒂所繪\]\]

**前拉斐爾派**（**Pre-Raphaelite
Brotherhood**），又常譯為**前拉斐爾兄弟會**，是1848年開始的一個[藝術團體](../Page/藝術運動.md "wikilink")（也是藝術運動），由3名年輕的[英國畫家所發起](../Page/英國.md "wikilink")—[約翰·艾佛雷特·米萊](../Page/約翰·艾佛雷特·米萊.md "wikilink")、[但丁·加百利·羅塞蒂和](../Page/但丁·加百利·羅塞蒂.md "wikilink")[威廉·霍爾曼·亨特](../Page/威廉·霍爾曼·亨特.md "wikilink")。

他們的目的是為了改變當時的藝術潮流，反對那些在[米开朗基罗和](../Page/米开朗基罗.md "wikilink")[拉斐尔的時代之後在他們看來偏向了機械論的](../Page/拉斐尔.md "wikilink")[風格主義畫家](../Page/風格主義.md "wikilink")。他們認為拉斐爾時代以前古典的姿勢和優美的繪畫成分已經被[學院藝術派的教學方法所腐化了](../Page/學院藝術.md "wikilink")，因此取名為前拉斐爾派。他們尤其反對由[約書亞·雷諾茲爵士所創立的](../Page/約書亞·雷諾茲.md "wikilink")[英國皇家藝術學院的畫風](../Page/英國皇家藝術學院.md "wikilink")，認為他的作畫技巧只是懶散而公式化的學院風格主義\[1\]。他們主張回歸到15世紀[義大利文藝復興初期的](../Page/義大利文藝復興.md "wikilink")，畫出大量細節、並運用強烈色彩的畫風。

前拉斐爾派常被看作是藝術中的前衛派運動，不過他們否認這種描述，因為他們仍然以古典歷史和神話作為繪畫題材以及[模仿的藝術態度](../Page/模仿.md "wikilink")，或者是以模擬自然的狀態，來作為他們藝術的目的。不過，前拉斐爾派毫無疑問將自身視為藝術界的改革運動，為他們的運動取了名稱以做區別，並且也出版了他們的期刊*The
Germ*，以宣揚他們的概念<ref name=McGann>McGann, Jerome J. *The Complete Writings
and Pictures of Dante Gabriel Rossetti, NINES consortium, Creative
Commons License; <http://www.rossettiarchive.org/docs/s40.rap.html>
retrieved 16 Dec. 2012.</ref>。有關他們的討論則紀錄在*Pre-Raphaelite Journal''中。

## 畫派的開端

[Germ.jpg](https://zh.wikipedia.org/wiki/File:Germ.jpg "fig:Germ.jpg")
1848年，前拉斐爾派在約翰·米萊於[倫敦的家中發起了](../Page/倫敦.md "wikilink")。在開始的會面中有[約翰·艾佛雷特·米萊](../Page/約翰·艾佛雷特·米萊.md "wikilink")、[但丁·加百利·羅塞蒂和](../Page/但丁·加百利·羅塞蒂.md "wikilink")[威廉·霍爾曼·亨特三人](../Page/威廉·霍爾曼·亨特.md "wikilink")。羅塞蒂和亨特是[皇家藝術學院的學生](../Page/皇家藝術學院.md "wikilink")，他們之前也曾在其他組織中會面過。羅塞蒂是[福特·馬多克斯·布朗](../Page/福特·馬多克斯·布朗.md "wikilink")（Ford
Madox Brown）的學徒\[2\]，他曾前往觀賞亨特依據[濟慈的詩](../Page/约翰·济慈.md "wikilink")*The
Eve of St
Agnes*所繪的作品，羅塞蒂想要将[浪漫詩和繪畫连接起来](../Page/浪漫主義.md "wikilink")。到了那年秋天他們已經累積至7人，畫派逐漸形成\[3\]。他們是[威廉·邁克爾·羅塞蒂](../Page/威廉·邁克爾·羅塞蒂.md "wikilink")（William
Michael Rossetti,
但丁羅塞蒂的弟弟）、[托馬斯·伍爾納](../Page/托馬斯·伍爾納.md "wikilink")（Thomas
Woolner）、[詹姆士·柯林](../Page/詹姆士·柯林.md "wikilink")（James
Collinson）、[弗雷德里克·喬治·史蒂芬](../Page/弗雷德里克·喬治·史蒂芬.md "wikilink")（Frederic
George
Stephens）。他們也邀請福特·布朗加入，不過被他拒絕了，但他的畫風還是跟他們相當接近。一些年輕的畫家和雕刻家也和他們有緊密關係。他們將畫派的成立作為秘密，沒有讓皇家學院的其他人知道。

## 早期原則

前拉斐爾派的原則為以下四條：

1.  要有真正想要表達的理念。
2.  要仔細地研究自然，從而得以知道怎樣表達它們(理念)。
3.  要對以前的藝術中直接、認真而真誠的部份感同身受，並排斥那些陳腐的、自我模仿的、死記硬背的部份。
4.  以及，最必不可少的一點，要創作出徹頭徹尾得好的畫和雕像。

這些原則相當謹慎地避免教條化，因為他們希望強調藝術家個人的責任，去決定他們繪畫的觀點和方法。他们受到了浪漫主義的影響，將自由和個人責任視為不可分離的两个概念。不過，他們尤其著迷於[中世紀的文化](../Page/中世紀.md "wikilink")，相信中世紀文化有著後來的時代所失去的正直精神和創造性。後來，強調中世紀文化的觀點與強調獨立觀察自然狀態的[現實主義產生了衝突](../Page/現實主義.md "wikilink")。原本拉斐爾畫派裡認為這兩者是能互相配合的，但在衝突產生後畫派一分為二，現實主義派由亨特和米萊領導，中世紀派則由羅塞蒂和[威廉·莫里斯等追隨者領導](../Page/威廉·莫里斯.md "wikilink")。不過衝突並不是完全的，兩派都相信藝術的實質是心靈上的，反對[库尔贝](../Page/古斯塔夫·库尔贝.md "wikilink")[唯物的現實主義以及](../Page/唯物主义.md "wikilink")[印象派](../Page/印象派繪畫.md "wikilink")。

為了復興15世紀藝術光輝的色彩風格，亨特和米萊發展了一種繪畫的方法，用稀薄的透明顏料（Glaze）覆蓋在潮濕的白色表面上，以此讓顏色保持如寶石一般的透明度和清晰度。這種色彩鮮明的強調是為了對比早期那些過度使用[瀝青的英國畫家](../Page/瀝青.md "wikilink")，瀝青會產生出渾濁而不固定的黑暗區塊，而這正是前拉斐爾派所輕視的。

從歷史角度來看，前拉斐爾派在前衛的同時也是中世紀的，它綜合了浪漫理想主義、科學理性主義、和一種在當時屬於前衛的道德觀。如同後來[維多利亞时期的藝術一般](../Page/維多利亞.md "wikilink")，這種結合在當今看來充滿了矛盾，但它卻也反映出了後來維多利亞時期的走向\[4\]。

## 引起的爭議

1849年，前拉斐爾派的作品被首次展覽。米萊所繪的《伊莎贝拉》(1848–1849)和亨特的《Rienzi》(1848–1849)在皇家學院進行了展示，而羅塞蒂的《Girlhood
of Mary Virgin》則在倫敦海德公園的街角自由展示。畫派裡所有成員已達成共識，在他們作品的簽名旁邊留下前拉斐爾派的縮寫—
**PRB** 的記號。在1850年1月至4月間他們也發行了雜誌，名為*The
Germ*。但丁·羅塞蒂的弟弟威廉·羅塞蒂負責編輯雜誌，發行了包括羅塞蒂、托馬斯·伍爾納和詹姆士·柯林的詩，以及其他有關藝術與文學間的親前拉斐爾派的論文，如[考文垂·巴特摩爾](../Page/考文垂·巴特摩爾.md "wikilink")（Coventry
Patmore）的文章。但這也顯現了為何前拉斐爾派僅維持短暫時間的原因，雜誌並不能長時間的維持畫派的氣勢。\[5\]

[Sir_John_Everett_Millais_002.jpg](https://zh.wikipedia.org/wiki/File:Sir_John_Everett_Millais_002.jpg "fig:Sir_John_Everett_Millais_002.jpg")
在1850年，在米萊的《基督在父母家中》（Christ In the House of His
Parents）進行展覽後，前拉斐爾派引起了不小的爭議。畫中描繪基督一家人在滿地木屑的雜亂木匠房裡工作，包括[查尔斯·狄更斯在內的許多人批評那是一種對基督的](../Page/查尔斯·狄更斯.md "wikilink")[褻瀆](../Page/褻瀆.md "wikilink")\[6\]。他們的中世紀畫風被批評為保守倒退\[7\]，而詳盡的描繪細節則被批評为醜陋而刺眼。狄更斯批評米萊將基督一家人描繪的像是酗酒者和貧民窟，而“中世紀”的姿勢則荒謬而扭曲。另一個名為“[The
Clique](../Page/The_Clique.md "wikilink")”的畫派也極力批評前拉斐爾派。皇家學院的主席[查理斯·洛克·伊斯特萊克](../Page/查理斯·洛克·伊斯特萊克.md "wikilink")（Charles
Lock Eastlake）也公開批評前拉斐爾派主張的原則。

不過，前拉斐爾派得到了評論家[约翰·拉斯金的支持](../Page/约翰·拉斯金.md "wikilink")，他讚揚前拉斐爾派對於自然狀態的描繪以及否定了傳統繪畫的方法。他繼續在经济上和他的寫作上支援前拉斐爾派，比如在[泰晤士報發表了支持前拉斐爾派的評論](../Page/泰晤士報.md "wikilink")，同時他還僱請米萊到蘇格蘭去替他畫了一幅肖像\[8\]，這趟旅程使得米萊認識了拉斯金的妻子埃菲，兩人接著發展出戀情，導致後來埃菲與拉斯金離婚而改嫁米萊\[9\]，成為了一場被大幅報導的醜聞。

在經過這些爭議之後，詹姆士·柯林離開了畫派。他們集合起來討論應該由誰來取代他的位置，但最後卻無法達成決定。同時米萊在結婚後也逐漸放棄前拉斐爾派的畫風。於是畫派便解散了，不過他們繼續發揮著影響力，畫家仍然繼續用這些風格作畫，只是他們不再於作品上簽下“PRB”了。儘管與米萊的決裂，拉斯金仍然繼續支持羅塞蒂和亨特的畫作，並且鼓勵羅塞蒂的妻子伊麗莎白·西德爾進行更多繪畫。

## 後來的發展及影響

[Helen_of_Troy.jpg](https://zh.wikipedia.org/wiki/File:Helen_of_Troy.jpg "fig:Helen_of_Troy.jpg")。伊芙琳·摩根所繪，1889年。\]\]
受到前拉斐爾派影響的畫家包括了[亞瑟·休斯](../Page/亞瑟·休斯.md "wikilink")（Arthur
Hughes）、[弗雷德里克·桑迪斯](../Page/弗雷德里克·桑迪斯.md "wikilink")（Frederic
Sandys）、[伊芙琳·摩根](../Page/伊芙琳·摩根.md "wikilink")（Evelyn De
Morgan）等人\[10\]，以及[福特·馬多克斯·布朗](../Page/福特·馬多克斯·布朗.md "wikilink")，布朗雖然沒有加入畫派，但他的畫風卻被認為是最接近前拉斐爾派原則的。畫派成員之一的[奧伯利·比亞茲萊後來則發展出他獨特的畫風](../Page/奧伯利·比亞茲萊.md "wikilink")\[11\]。

1856年後，但丁·羅塞蒂成了前拉斐爾派裡中世紀派畫風的領導人，他的作品也影響了[威廉·莫里斯](../Page/威廉·莫里斯.md "wikilink")，他們兩人成為夥伴，不過羅塞蒂也因此和莫里斯的妻子—模特兒[珍·莫里斯](../Page/珍·莫里斯.md "wikilink")（Jane
Morris）發生緋聞。福特·馬多克斯·布朗與畫家[愛德華·伯納-瓊斯也成了夥伴](../Page/愛德華·伯納-瓊斯.md "wikilink")。藉由莫里斯的關係，前拉斐爾派的概念影響了許多室內設計師和建築師，利用中世紀的風格做建築設計，以及其他裝飾品的設計。這也直接引導了莫里斯所發動的[工艺美术运动](../Page/工艺美术运动.md "wikilink")，霍爾曼·亨特也參與了這項運動。

在1850年後，由於現實主義和科學觀點上的著重，亨特和米萊都已經不再直接模仿中世紀藝術。但亨特繼續強調心靈在藝術上的重要性，試圖利用準確的觀察和研究來調和信仰與科學兩者，而前往[以色列和](../Page/以色列.md "wikilink")[埃及以聖經故事作為繪畫的題材](../Page/埃及.md "wikilink")。相較之下，米萊於1860年後拋棄了前拉斐爾派原則，而重採皇家藝術學院創始人雷諾茲那種廣泛而鬆散的風格。莫里斯和其他人則極力批評米萊的這種改變。

前拉斐爾派持續影響許多英國畫家直至20世紀。羅塞蒂後來成為了歐洲[象徵主義的先驅](../Page/象徵主義.md "wikilink")。

在英國伯明罕市的[伯明罕博物館和藝術畫廊](../Page/伯明罕博物館和藝術畫廊.md "wikilink")（Birmingham
Museum & Art
Gallery）收藏著許多世界知名的前拉斐爾派畫作，這些畫作也對在當地長大的作家[J.R.R.托尔金產生極大的影響](../Page/约翰·罗纳德·瑞尔·托尔金.md "wikilink")\[12\]。

20世紀時畫家的觀點大量改變，由於攝影技術的發達，藝術的目的逐漸遠離了重現實際的狀態。因為前拉斐爾派主要專注於將事物描繪的如同攝影般逼真，儘管他們也特殊的專注於詳細描繪表面圖案上，他們的作品仍被許多批評家所貶低。不過自從1960年代以來，前拉斐爾派的作品開始重新受到重視，比如1984年倫敦[泰特美術館的展覽](../Page/泰特美術館.md "wikilink")，確立前拉斐爾派在藝術史上的地位\[13\]。在2012年9月泰特美術館又再次的對前拉斐爾派的畫作進行展覽\[14\]。

## 畫家列表

### 前拉斐爾派成員

[John_Everett_Millais_-_Ophelia_-_Google_Art_Project.jpg](https://zh.wikipedia.org/wiki/File:John_Everett_Millais_-_Ophelia_-_Google_Art_Project.jpg "fig:John_Everett_Millais_-_Ophelia_-_Google_Art_Project.jpg")所繪。\]\]

  - [詹姆士·柯林](../Page/詹姆士·柯林.md "wikilink")（James Collinson） 畫家
  - [威廉·霍爾曼·亨特](../Page/威廉·霍爾曼·亨特.md "wikilink")
    畫家《[伊莎貝拉與香草罐](../Page/伊莎貝拉與香草罐.md "wikilink")》《[登上英國海岸](../Page/登上英國海岸.md "wikilink")》
  - [約翰·艾佛雷特·米萊](../Page/約翰·艾佛雷特·米萊.md "wikilink")
    畫家《[歐菲麗亞](../Page/歐菲麗亞.md "wikilink")》
  - [但丁·加百利·羅塞蒂](../Page/但丁·加百利·羅塞蒂.md "wikilink")
    畫家、詩人《[碧兒翠絲](../Page/碧兒翠絲.md "wikilink")》《[報佳音](../Page/報佳音.md "wikilink")》
  - [威廉·邁克爾·羅塞蒂](../Page/威廉·邁克爾·羅塞蒂.md "wikilink")（William Michael
    Rossetti）評論家
  - [弗雷德里克·喬治·史蒂芬](../Page/弗雷德里克·喬治·史蒂芬.md "wikilink")（Frederic George
    Stephens）評論家
  - [托馬斯·伍爾納](../Page/托馬斯·伍爾納.md "wikilink")（Thomas Woolner）雕刻家、詩人
  - [比爾茲萊](../Page/比爾茲萊.md "wikilink")《[莎樂美](../Page/莎樂美.md "wikilink")》

### 有關聯的畫家與人物

  - [查尔斯·奥尔斯顿·柯林斯画家](../Page/查尔斯·奥尔斯顿·柯林斯.md "wikilink")
  - [福特·馬多克斯·布朗](../Page/福特·馬多克斯·布朗.md "wikilink")（Ford Madox Brown）
    畫家、設計師
  - [愛德華·伯納-瓊斯](../Page/愛德華·伯納-瓊斯.md "wikilink")（Edward Burne-Jones）
    畫家、設計師
  - [湯瑪斯·庫伯·高奇](../Page/湯瑪斯·庫伯·高奇.md "wikilink")（Thomas Cooper Gotch） 畫家
  - [亞瑟·休斯](../Page/亞瑟·休斯.md "wikilink")（Arthur Hughes） 畫家、插圖畫家
  - [珍·莫里斯](../Page/珍·莫里斯.md "wikilink")（Jane Morris） 模特兒
  - [梅·莫里斯](../Page/梅·莫里斯.md "wikilink")（May Morris） 刺繡師、設計師
  - [克莉斯緹娜·羅塞蒂](../Page/克莉斯緹娜·羅塞蒂.md "wikilink")（Christina Rossetti） 詩人
  - [约翰·拉斯金](../Page/约翰·拉斯金.md "wikilink")
    評論家《[威尼斯之石](../Page/威尼斯之石.md "wikilink")》
  - [弗雷德里克·雷頓](../Page/弗雷德里克·雷頓.md "wikilink") 畫家
  - [伊麗莎白·西德爾](../Page/伊麗莎白·西德爾.md "wikilink")（Elizabeth Siddal）
    畫家、詩人和模特兒
  - [阿爾吉儂·斯文本恩](../Page/阿爾吉儂·斯文本恩.md "wikilink")（Algernon Swinburne） 詩人
  - [约翰·威廉姆·沃特豪斯](../Page/约翰·威廉姆·沃特豪斯.md "wikilink") 畫家
  - [威廉·莫里斯](../Page/威廉·莫里斯.md "wikilink")
    設計師、作家《[金盞花紋飾](../Page/金盞花紋飾.md "wikilink")》
  - [約翰·柯里爾](../Page/約翰·柯里爾.md "wikilink")《[戈黛娃夫人](../Page/戈黛娃夫人.md "wikilink")》

## 畫作收藏

英國的[泰特美術館](../Page/泰特美術館.md "wikilink")、[维多利亚和阿尔伯特博物馆](../Page/维多利亚和阿尔伯特博物馆.md "wikilink")、曼徹斯特藝術畫廊等地都收藏了大量的前拉斐爾派畫作，而在英國以外，美國的德拉威藝術博物館也有許多收藏。
許多地點的[國家名勝古蹟信託也都收藏了前拉斐爾派作品](../Page/國家名勝古蹟信託.md "wikilink")。

[安德魯·洛伊·韋伯是前拉斐爾派的狂熱收藏家](../Page/安德魯·洛伊·韋伯.md "wikilink")，至今已經收藏了300件該畫派的作品，他還在2003年由[皇家藝術學院展出了他的收藏](../Page/皇家藝術學院.md "wikilink")。

## 參考文獻

### 引用

### 來源

  -
  - Bucher, Gregory (2004).
    "[Review](https://web.archive.org/web/20091231160442/http://moses.creighton.edu/JRS/2004/2004-r3.html)
    of Matthew Dickerson. 'Following Gandalf. Epic Battles and Moral
    Victory in The Lord of the Rings'", *Journal of Religion & Society*,
    **6**, ISSN 1522-5658, webpage accessed 13 October 2007

  -
  - Dickerson, Matthew (2003). *Following Gandalf : epic battles and
    moral victory in the Lord of the rings*, Grand Rapids, Mich. :
    Brazos Press, ISBN 978-1-58743-085-5

  -
  -
  -
  - Ramm, John (2003). "The Forgotten Pre-Raphaelite: Henry Wallis",
    *Antique Dealer & Collectors Guide*, **56** (Mar/April), pp. 8–9.

## 外部連結

  - [收藏的連結](http://www.dlc.fi/~hurmari/preraph.htm)
  - [伯明罕藝術博物館和藝術畫廊](http://www.bmagic.org.uk/results?s=pre-raphaelite+)
  - 利物浦[步行者畫廊](../Page/步行者畫廊.md "wikilink")[藏品](https://web.archive.org/web/20040416083323/http://www.liverpoolmuseums.org.uk/walker/pre-raphaelites/index.asp)
  - [前拉斐爾派的人物和原則](http://www.angelfire.com/wy2/preraph/page2.html)

{{-}}

[前拉斐爾派](../Category/前拉斐爾派.md "wikilink")

1.  Hilton, Timothy (1970). *The Pre-Raphaelites*, p. 46. Oxford
    University Press.

2.
3.  Hilton (1970), pp. 28–33.

4.  Wood, Christopher. (1981) *The Pre-Raphaelites*, p. 12. Weidenfeld
    and Nicolson.

5.  Daly, 1989.

6.  Slater, Michael (2009). *Charles Dickens*, p. 309. Yale University
    Press.

7.  The Times, Saturday, May 3, 1851; pg. 8; Issue 20792: Exhibition of
    the Royal Academy. (Private View.)

8.  Dearden, James S. (1999). *John Ruskin: A Life in Pictures*, pp.
    36–37. Sheffield Academic Press.

9.  Dearden (1999), p. 43.

10. Hilton ( 1970), pp. 202–05

11.
12. 參見Bucher (2004)討論前拉斐爾派對托爾金的影響。

13. Barringer, Tim (1999). *Reading the Pre-Raphaelites*, p. 17. Yale
    University Press.

14.