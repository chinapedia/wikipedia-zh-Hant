[Maeda_Keijirō.jpg](https://zh.wikipedia.org/wiki/File:Maeda_Keijirō.jpg "fig:Maeda_Keijirō.jpg")
**前田利益**（）（另有生於天文10年（1541年）一說），[日本戰國時代中後期的名將](../Page/日本戰國時代.md "wikilink")，幼名**宗兵衛**，本名**利益**（**利太**），通稱**前田慶次/慶次郎**，[瀧川一益之兄](../Page/瀧川一益.md "wikilink")[瀧川益氏之庶男](../Page/瀧川益氏.md "wikilink")（另有一益之子一說）。生於[尾張海東郡荒子部的寒村](../Page/尾張.md "wikilink")，因前田家家督[前田利久體弱無子](../Page/前田利久.md "wikilink")，幼年過繼給利久當養子，雖然因此成為[前田利家之侄子](../Page/前田利家.md "wikilink")，但其實慶次年齡比利家大了許多。

前田慶次以其奇裝異行著稱，是[傾奇者的代表性人物](../Page/傾奇者.md "wikilink")，沒有血緣關係的叔父利家年輕時也是一名傾奇者。

## 青年時期

[永祿十年](../Page/永祿.md "wikilink")（1567年），[織田信長命令](../Page/織田信長.md "wikilink")[前田利久讓出家督之位給](../Page/前田利久.md "wikilink")[前田利家](../Page/前田利家.md "wikilink")，並在命令書上寫道：「利久子嗣中有傾奇者（慶次），對將來繼承家督無所用，又左衛門（利家）自幼追隨我學習武士道，而且立功無數，家督之位由又左衛門繼承才符合正理。」雖然利久本人沒有異議，但此人事命令一出馬上激化利久家人家臣與利家的衝突，個性溫厚的利久決定離開織田家，利益也跟著養父一起離開。

根據《米澤人國記》的記載，利益於（1567年-1582年）期間到了京都與關白[一條兼冬及右大臣](../Page/一條兼冬.md "wikilink")[西園寺公朝的屋敷活動](../Page/西園寺公朝.md "wikilink")，學習文學、音樂，又聽學大納言[三條公光講解源氏物語及](../Page/三條公光.md "wikilink")[伊勢物語](../Page/伊勢物語.md "wikilink")、向名茶道家[千利休學者茶道](../Page/千利休.md "wikilink")，更學懂亂舞、猿樂、笛吹、太鼓的舞技，且向連歌第一大師紹巴學習連歌、俳句和歌等藝文。以此可研判利久一家最後定居京都，而且過著相當優渥的生活(不然慶次也沒辦法去學這些風雅事物)，有說法認為信長有派發補償金或利家暗中接濟讓利久一家老小衣食無憂。

[天正十年](../Page/天正.md "wikilink")（1583年），成為加賀國主的利家把利久請回前田家照顧，利家給與利久七千石知行，而利久將其中五千石分予利益。同年，利益獲利家封為[阿娓城城主](../Page/阿娓城.md "wikilink")。

天正十九年（1591年），因與利家不和及養父利久去世數年之故，利益決定出走，離開時妻子兒子都沒帶上獨自一人離開。利家得知後大為憤怒，並揚言追殺利益。離家後的利益再入京都，於各大名及貴族的屋敷間出入，同時在京都遇上了一生中的摯友[直江兼續](../Page/直江兼續.md "wikilink")，兩人一見如故，不時往來。

## 晚年時期

[MaedaKeiji-memorialstone.jpg](https://zh.wikipedia.org/wiki/File:MaedaKeiji-memorialstone.jpg "fig:MaedaKeiji-memorialstone.jpg")堂森善光寺の供養塔\]\]
[慶長三年](../Page/慶長.md "wikilink")（1598年），[直江兼續極力推薦利益予](../Page/直江兼續.md "wikilink")[上杉景勝](../Page/上杉景勝.md "wikilink")，據《**上杉將士書‧上**》記載，當時的利益自稱名為「**穀藏院了齋**」，有趣的是當時為盛夏，利益卻穿著厚衣，據說當時景勝見到後，驚言：「此果為[傾奇者也](../Page/傾奇者.md "wikilink")！」。景勝以低於慶次過往俸祿之一千石聘用之，利益慨然受之。

慶長三年（1598年）[八月](../Page/八月.md "wikilink")，[豐臣秀吉病死](../Page/豐臣秀吉.md "wikilink")，[德川家康受秀吉托孤輔政](../Page/德川家康.md "wikilink")，居五大老之首，權傾天下並逐漸掌握天下號令之實。慶長五年（1600年），[德川家康向上杉家用兵](../Page/德川家康.md "wikilink")（用兵之意有為誘使石田一派明反之說），大軍直指會津，但由於[石田三成於畿內起兵](../Page/石田三成.md "wikilink")，家康遂於九月十五日引兵返關原與石田一派所組之西軍決戰（是為[關原之戰](../Page/關原之戰.md "wikilink")），而上杉家則趁勢轉向對[最上義光用兵期欲擴大版圖](../Page/最上義光.md "wikilink")，就在包圍長谷堂城時，收到東軍勝利的消息，此時在攻擊[最上義光的](../Page/最上義光.md "wikilink")[直江兼續不得不撤退](../Page/直江兼續.md "wikilink")（長谷堂城撤退戰）。總大將[直江兼續命](../Page/直江兼續.md "wikilink")[前田慶次與](../Page/前田慶次.md "wikilink")[水原親憲擔任上杉家殿後軍以換取撤退時間](../Page/水原親憲.md "wikilink")。由於[直江兼續精妙的指揮調度](../Page/直江兼續.md "wikilink")，幾番交鋒後，成功抵禦最上軍追擊。

[關原之戰後](../Page/關原之戰.md "wikilink")，上杉家降伏於家康，領地由原有的一百二十萬石被减封僅餘米澤三十萬石，據聞當時利益與景勝、兼續上京，並得到家康的接見。另據聞當時諸多大名有意招攬利益，惟皆一一被利益所拒。上杉家由於領地大減，無力支付高俸，僅以五百石俸給聘用利益，利益不以為意。慶長六年（1601年），利益到達上杉領地米澤，於途中完成了描寫民風民俗的「前田慶次道中日記」，成為研究當時民情風俗非常有用的史料。利益移往米澤後，定居於堂森山東北的無苦庵，終日看花賞月，與近鄰住民相處融洽，不時參加宴會祭典。其間作成「無苦庵記」，記末利益寫道「當生活時當生活，當要死時當點綴，不為煩惱動一眉，不為俗事怨一言。」，可見其處世之人生觀。

慶長十七年（1612年）六月四日，一代戰國[傾奇者於](../Page/傾奇者.md "wikilink")[米澤病逝](../Page/米澤.md "wikilink")，葬於堂森善光寺。保留甲胄、朱槍及和歌五首，收錄於「**龜岡文殊奉納詩歌百首**」中。其子正虎未與利益一同出奔繼續留在前田家，從利家家臣到江戶時代成為加賀藩藩士。利益雖為[傾奇者](../Page/傾奇者.md "wikilink")，然實為文武雙全之才，連歌、音樂等藝術才華皆名冠當世。縱然其行為特立獨行，然獲景勝讚以「大剛之大將」之稱，足以表達利益一生真實的寫照。

## 系譜

  - 妻：前田安勝女
  - 子：前田正虎

## 相關作品

[Gomai-Do_Armor_by_MAEDA_Keiji.jpg](https://zh.wikipedia.org/wiki/File:Gomai-Do_Armor_by_MAEDA_Keiji.jpg "fig:Gomai-Do_Armor_by_MAEDA_Keiji.jpg")

  - 小說

<!-- end list -->

  - 一夢庵風流記（[讀賣新聞](../Page/讀賣新聞.md "wikilink")、[隆慶一郎著](../Page/隆慶一郎.md "wikilink")）
  - 風流戰國武士（文松堂、[海音寺潮五郎著](../Page/海音寺潮五郎.md "wikilink")）
  - 戰國風流（[PHP研究所](../Page/PHP研究所.md "wikilink")、著）
  - 叛旗兵（[角川書店](../Page/角川書店.md "wikilink")、[山田風太郎著](../Page/山田風太郎.md "wikilink")）
  - 傍若無人劍（[河出書房新社](../Page/河出書房新社.md "wikilink")、著）
  - 丹前屏風（啓明社、[大佛次郎著](../Page/大佛次郎.md "wikilink")）
  - 前田慶次郎：天下無雙的傾奇者（PHP研究所、著）
  - 慶長疾風錄（[學研](../Page/學研.md "wikilink")、著）
  - 怒濤之慶次（[寶島社](../Page/寶島社.md "wikilink")、中村朋臣著）

<!-- end list -->

  - 影視劇

<!-- end list -->

  - あばれ大名（1959年、東映、演：[市川右太衛門](../Page/市川右太衛門.md "wikilink")）

  - [利家與松](../Page/利家與松.md "wikilink")（2002年、[NHK大河劇](../Page/NHK大河劇.md "wikilink")、演：[及川光博](../Page/及川光博.md "wikilink")）

  - （2015年、NHK、演：）

<!-- end list -->

  - 舞台劇

<!-- end list -->

  - 風流夢大名（1995年、、演：）
  - 花之武將 前田慶次（2010年、、演：[片岡愛之助](../Page/片岡愛之助_\(第六代\).md "wikilink")）
  - 一夢庵風流記
    前田慶次（2014年、[寶塚](../Page/寶塚.md "wikilink")[雪組公演](../Page/雪組.md "wikilink")、演：[壯一帆](../Page/壯一帆.md "wikilink")）

<!-- end list -->

  - 漫畫

<!-- end list -->

  - [花之慶次](../Page/花之慶次.md "wikilink")（[集英社](../Page/集英社.md "wikilink")、[隆慶一郎作](../Page/隆慶一郎.md "wikilink")・[原哲夫畫](../Page/原哲夫.md "wikilink")）
  - [義風堂堂
    直江兼續～前田慶次月語～](../Page/義風堂堂！！兼續和慶次.md "wikilink")（[新潮社](../Page/新潮社.md "wikilink")、原哲夫、作・畫）
  - [女忍紅騎兵](../Page/山風短.md "wikilink")（[講談社](../Page/講談社.md "wikilink")、山田風太郎作・畫）

<!-- end list -->

  - 遊戲

<!-- end list -->

  - [信長之野望系列](../Page/信長之野望系列.md "wikilink")（[光榮開發](../Page/光榮.md "wikilink")）
  - [戰國無雙系列](../Page/戰國無雙系列.md "wikilink")（2004年、光榮開發、聲：[上田祐司](../Page/上田祐司.md "wikilink")）
  - [無双OROCHI系列](../Page/無双OROCHI系列.md "wikilink")（2007年、光榮開發、聲：[上田祐司](../Page/上田祐司.md "wikilink")）
  - [太閤立志傳系列](../Page/太閤立志傳系列.md "wikilink")（光榮開發）
  - 信喵之野望（光榮開發）
  - [戰國BASARA](../Page/戰國BASARA.md "wikilink")（2006年、[CAPCOM開發](../Page/CAPCOM.md "wikilink")、聲：[森田成一](../Page/森田成一.md "wikilink")）
  - [貓咪大戰爭](../Page/貓咪大戰爭.md "wikilink")

## 相關連結

  - [直江兼續](../Page/直江兼續.md "wikilink")
  - [前田利家](../Page/前田利家.md "wikilink")
  - [芳春院](../Page/芳春院.md "wikilink")（阿松）
  - [上杉景勝](../Page/上杉景勝.md "wikilink")

[Category:加賀前田氏](../Category/加賀前田氏.md "wikilink")
[Category:瀧川氏](../Category/瀧川氏.md "wikilink")
[Category:戰國武將](../Category/戰國武將.md "wikilink")
[Category:尾張國出身人物](../Category/尾張國出身人物.md "wikilink")