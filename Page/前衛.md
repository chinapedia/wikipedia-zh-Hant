**先锋派**（Avant-garde）原本是[法語詞](../Page/法語.md "wikilink")，譯成[英文即](../Page/英文.md "wikilink")*front
guard*，*advance
guard*，或*vanguard*，“前衛”是其直譯之意。人們經常用這詞指涉新穎的或實驗性的作品或人物，尤其是對於[藝術](../Page/藝術.md "wikilink")、[文化及](../Page/文化.md "wikilink")[政治的層面](../Page/政治.md "wikilink")。

前衛已被接受為規範藝術、文化、現實界線向前推進，由於前衛的心靈相信事物只會在現實的前沿（leading edge of reality）產生。

軍隊的先鋒是一小批高度訓練的士兵，探索大片地域並且計畫軍隊的行進方向。這個意義幾近於近代小群的知識份子與藝術家所達致的工作，開創新的文化或政治領域並且給予社會指導。由於[軍事術語的暗示意義](../Page/軍事術語.md "wikilink")，有些人認為前衛即是[菁英主義](../Page/菁英主義.md "wikilink")，尤其當它用來形容文化運動時。

因此，前衛的音樂即是指涉[即興音樂的極端形式](../Page/即興音樂.md "wikilink")，由此他們並不在乎既存的[和聲結構或是節奏等等的知識](../Page/和聲.md "wikilink")。

這個用詞也用來指涉社會進步和重塑的促進，各個運動的不同目標發佈為公眾宣示的表現，稱作「宣言」（manifestos）——最有名的例子：[共產黨宣言](../Page/共產黨宣言.md "wikilink")（The
Communist
Manifestos）。隨著時間，前衛逐漸與「[為藝術而藝術](../Page/為藝術而藝術.md "wikilink")」的運動連結在一起，主要的目標在擴展[美學經驗的新領域](../Page/美學.md "wikilink")，而不是社會重造的運動。

此一法國術語在藝術上的使用始於1863年5月17日巴黎「[落選者沙龍](../Page/落選者沙龍.md "wikilink")」開幕。此一組織的組成者是那些畫作被年度[巴黎沙龍退回的](../Page/巴黎沙龍.md "wikilink")[畫家們](../Page/畫家.md "wikilink")。之後這個活動又分別在1874年、1875年和1886年舉辦。在十九世紀後期至二十世紀中葉，**前衛**是一種在藝術、建築、態度與想法上，與既有典範背道而馳的原創與實驗。\[1\]

由於一些建議，前衛藝術應該包括[街頭藝術](../Page/街頭藝術.md "wikilink")，例如[塗鴉藝術或是任何一種把藝術前緣往前推的運動](../Page/塗鴉.md "wikilink")。應注意的是前衛不只是一種藝術風格，像是[超現實主義或](../Page/超現實主義.md "wikilink")[立體主義等詞](../Page/立體主義.md "wikilink")，它也不等於當代發生的任何事件。

例如：[杜象的](../Page/杜象.md "wikilink")[尿壺也許在當時是前衛的](../Page/噴泉_\(杜象\).md "wikilink")，今日假使有人重新舉行這一創作，即不屬前衛，因為已有了前例。因而前衛一詞的涵意通常是暫時的，並且與藝術的持續解構有關。它可以用來指所有領域的先行者。無論如何，杜象創作這一行為仍維持前衛的，因為他推動藝術並且開啟了和藝術自我定義的新對話。

## 關聯

前衛與藝術相關主要因為除去這些運動，藝術本身會變得迂腐而沒有活力，並且僅僅會是一項手藝，不斷的重複歷史的風格與手法。這個詞最常被用於[視覺藝術](../Page/視覺藝術.md "wikilink")、[時尚](../Page/時尚.md "wikilink")、[電影](../Page/電影.md "wikilink")、和文學，但也同樣用於[音樂](../Page/音樂.md "wikilink")、[烹飪](../Page/烹飪.md "wikilink")、[政治或](../Page/政治.md "wikilink")[文化的知識份子或新方法](../Page/文化.md "wikilink")。

## 前衛藝術運動

  - [抽象表現主義](../Page/抽象表現主義.md "wikilink")
  - [CoBrA](../Page/CoBrA前衛藝術運動.md "wikilink")
  - [建構主義](../Page/建構主義.md "wikilink")
  - [立體主義](../Page/立體主義.md "wikilink")
  - [達達主義](../Page/達達主義.md "wikilink")
  - [道格瑪95](../Page/道格瑪95.md "wikilink")
  - [表現主義](../Page/表現主義.md "wikilink")
  - [未來主義](../Page/未來主義.md "wikilink")
  - [Fluxus](../Page/Fluxus.md "wikilink")
  - [印象主義](../Page/印象主義.md "wikilink")
  - [Incoherents](../Page/Incoherents.md "wikilink")
  - [Lettrisme](../Page/Lettrisme.md "wikilink")
  - [Mail art](../Page/Mail_art.md "wikilink")
  - [現代主義](../Page/現代主義.md "wikilink")
  - [Neoism](../Page/Neoism.md "wikilink")
  - [No Wave](../Page/No_Wave.md "wikilink")
  - [原始主義](../Page/原始主義.md "wikilink")
  - [Free Jazz](../Page/Free_Jazz.md "wikilink")
  - [普普藝術](../Page/普普藝術.md "wikilink")
  - [Situationist](../Page/Situationist.md "wikilink")
  - [社會現實主義](../Page/社會現實主義.md "wikilink")
  - [Spart](../Page/Spart.md "wikilink")
  - [超現實主義](../Page/超現實主義.md "wikilink")
  - [觀念藝術](../Page/觀念藝術.md "wikilink")
  - [拼貼畫](../Page/拼貼畫.md "wikilink")

## 前衛的其他領域

  - [實驗電影](../Page/實驗電影.md "wikilink")
  - [實驗音樂](../Page/實驗音樂.md "wikilink")
  - [實驗劇場](../Page/實驗劇場.md "wikilink")
  - [時代精神](../Page/時代精神.md "wikilink")
  - [Molecular gastronomy](../Page/Molecular_gastronomy.md "wikilink")
  - [Demoscene](../Page/Demoscene.md "wikilink")

## 同見

  - [知識份子](../Page/知識份子.md "wikilink")

## 外部連結

  - [cinema avant garde](http://www.exprmntl.net) (en, fr, es, de)
  - [Iranian Avant Garde Media](http://www.kolahstudio.com) Iranian
    Underground Art Community

[Category:社会哲学](../Category/社会哲学.md "wikilink")
[Category:文化運動](../Category/文化運動.md "wikilink")
[Category:前衛藝術](../Category/前衛藝術.md "wikilink")

1.  [藝術與建築索引典](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300055775)
    於2011年3月31日查閱