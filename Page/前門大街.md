[View_from_the_Chienmen_Gate_to_the_Kaiser-street.jpg](https://zh.wikipedia.org/wiki/File:View_from_the_Chienmen_Gate_to_the_Kaiser-street.jpg "fig:View_from_the_Chienmen_Gate_to_the_Kaiser-street.jpg")
[The_Ch'ien-men_Great_Street.jpg](https://zh.wikipedia.org/wiki/File:The_Ch'ien-men_Great_Street.jpg "fig:The_Ch'ien-men_Great_Street.jpg")

**前門大街**，是[北京市](../Page/北京市.md "wikilink")[东城区的一條大街](../Page/东城区_\(北京市\).md "wikilink")，位于[北京中轴线上](../Page/北京中轴线.md "wikilink")。前门大街是北京的传统商业街，毗邻[天安门广场](../Page/天安门广场.md "wikilink")。前门大街及其两侧的[大栅栏街道](../Page/大栅栏街道.md "wikilink")、[前门街道辖区内有许多](../Page/前门街道.md "wikilink")[中華老字号](../Page/中華老字号.md "wikilink")，如[同仁堂](../Page/同仁堂.md "wikilink")、[全聚德](../Page/全聚德.md "wikilink")、[张一元等](../Page/张一元.md "wikilink")。

## 簡介

前門大街北起[正阳门](../Page/正阳门.md "wikilink")（前门）箭楼，南至[天桥路口](../Page/天桥.md "wikilink")，与[天桥南大街连接](../Page/天桥南大街.md "wikilink")。

前门大街从2004年开始大修。2007年5月9日，前门大街前门至[珠市口段开始改造](../Page/珠市口.md "wikilink")，2008年5月28日赶在[北京奥运会前完工](../Page/2008年北京奥运会.md "wikilink")\[1\]，同年8月7日对外开放\[2\]。经过改造后，前门大街前门至[珠市口段成为一条步行街](../Page/珠市口.md "wikilink")，并重现了前门大街清末民初的建筑风格。此次改造恢复了前门大街前门至[珠市口段](../Page/珠市口.md "wikilink")[过去的有轨电车](../Page/北京有轨电车.md "wikilink")“铛铛车”，于2009年元旦开始运营\[3\]。

从2007年至2008年的改造，改变了前门大街的整体面貌。改造前，前门大街到处立有凌乱的广告牌，许多商铺出售假冒伪劣商品，食品安全无法保证。2007年，北京市[崇文区人民政府启动前门大街改造工程后](../Page/崇文区.md "wikilink")，该工程交给了隶属崇文区国资委的天街公司运作，并代表政府持有前门大街的产权。天街公司采用市场化运作，引入[潘石屹的](../Page/潘石屹.md "wikilink")[SOHO中国参与改造工程](../Page/SOHO中国.md "wikilink")。在项目动工前，潘石屹就专门为此注册成立北京丹石投资管理有限公司（以下简称“北京丹石公司”）。由于[中共北京市委](../Page/中共北京市委.md "wikilink")、[北京市人民政府考虑到前门不能搞房地产炒作](../Page/北京市人民政府.md "wikilink")，乃终止了和SOHO中国的合作。SOHO中国成为前门大街改造工程中没有“开发商”名分的实际操作者。经过两年波折，2009年SOHO中国通过潘石屹控股的北京丹石公司，收购了北京前门项目中的部分商业物业，成为业主。\[4\]

前门大街改造后，新開設了不少國際品牌連鎖店，包括[Uniqlo](../Page/Uniqlo.md "wikilink")、[ZARA](../Page/ZARA.md "wikilink")、[H\&M時裝店](../Page/H&M.md "wikilink")；[劳力士](../Page/劳力士.md "wikilink")、[Swatch鐘錶店及](../Page/Swatch.md "wikilink")[周大福等](../Page/周大福.md "wikilink")。食肆包括[全聚德](../Page/全聚德.md "wikilink")、[Starbucks](../Page/Starbucks.md "wikilink")、[KFC及](../Page/KFC.md "wikilink")[麥當勞快餐店等](../Page/麥當勞.md "wikilink")。但改造后的前门大街因市场定位不准，生意清淡，既不受本地居民欢迎，也不能满足游客需要。2012年，据中国商报记者统计，前门大街共有25家店面关门；前门大街管委会介绍说，整个前门大街约有185家商户。\[5\]

2013年，前门大街十分冷清。大街两侧的110多家商铺中，关门的商铺已有大约30多家。沿街各店的经营内容五花八门，有[全聚德等老字号](../Page/全聚德.md "wikilink")，有[ZARA等国际服装品牌](../Page/ZARA.md "wikilink")，有国内运动品牌、家具、首饰、快餐连锁店等等，其中服装品牌最多。无论商品价格高低，许多店面内都空无一人。\[6\]

<File:Quanjude> Qianmen Dajie Store
2010.jpg|前门大街[全聚德分店](../Page/全聚德.md "wikilink")
<File:Zhang> Yiyuan tea
Company.JPG|前门大街[张一元茶庄](../Page/张一元.md "wikilink")
[File:北京前门大街东来顺.JPG|前门大街](File:北京前门大街东来顺.JPG%7C前门大街)[东来顺](../Page/东来顺.md "wikilink")
<File:Rolex> in Beijing.JPG|前门大街[劳力士](../Page/劳力士.md "wikilink")
<File:Qianmen> Dajie Tram.jpg|正在前门大街行驶的“铛铛车”

## 参考文献

## 外部链接

  -
## 参见

  - [北京中轴线](../Page/北京中轴线.md "wikilink")
      - [正陽門](../Page/正陽門.md "wikilink")
      - [正阳桥坊](../Page/正阳桥坊.md "wikilink")
      - [珠市口](../Page/珠市口.md "wikilink")
  - [大栅栏](../Page/大栅栏.md "wikilink")
  - [全聚德](../Page/全聚德.md "wikilink")
  - [鲜鱼口胡同](../Page/鲜鱼口胡同.md "wikilink")
  - [西興隆街](../Page/西興隆街.md "wikilink")
  - [打磨廠街](../Page/打磨廠街.md "wikilink")
  - [西河沿街](../Page/西河沿街.md "wikilink")

{{-}}

[Category:北京市道路](../Category/北京市道路.md "wikilink")
[Category:北京中轴线](../Category/北京中轴线.md "wikilink")
[Category:北京市东城区](../Category/北京市东城区.md "wikilink")

1.  [前门大街改造完工，千龙网，2008-05-28](http://beijing.qianlong.com/3825/2008/05/28/3562@4464640.htm)

2.  [前门大街8月7日开街
    当当车暂不对游客开放，搜狐，2008-07-29](http://news.sohu.com/20080729/n258450772.shtml)

3.  [前门当当车元旦开放，新华网，2008-12-29](http://www.bj.xinhuanet.com/bjpd_sdwm/2008-12/29/content_15313137.htm)


4.  [前门大街
    天安门前的商业烦恼，搜狐，2012-11-22](http://roll.sohu.com/20121122/n358362516.shtml)

5.
6.  [北京前门商业冷清
    店铺频唱空城计，襄阳房产热线，2013年8月24日](http://www.xffcol.com/News_View.Asp?Xid=62284)