[Kate_and_William,_Canada_Day,_2011,_Ottawa,_Ontario,_Canada.jpg](https://zh.wikipedia.org/wiki/File:Kate_and_William,_Canada_Day,_2011,_Ottawa,_Ontario,_Canada.jpg "fig:Kate_and_William,_Canada_Day,_2011,_Ottawa,_Ontario,_Canada.jpg")
**劍橋公爵**（，又譯**坎布里奇公爵**）為其中一種，也是[英國王室的一種特別等級](../Page/英國王室.md "wikilink")。此頭銜（以[英格蘭](../Page/英格蘭.md "wikilink")[劍橋為名](../Page/劍橋.md "wikilink")）可經由[長子繼承制](../Page/長子繼承制.md "wikilink")，由[男性後裔繼承](../Page/父系制度.md "wikilink")，並已授予多位英國王室成員。劍橋公爵的配偶則稱作劍橋公爵夫人（）。

劍橋公爵的頭銜可追溯至17世紀，取代了早期的[劍橋伯爵頭銜](../Page/劍橋伯爵.md "wikilink")。此頭銜曾數次消失，甚至100多年來都沒有使用，直到2011年4月29日[威廉王子與](../Page/威廉王子.md "wikilink")[凱薩琳·密道頓結婚時才重新授予](../Page/劍橋公爵夫人凱薩琳.md "wikilink")。

## 歷史

劍橋公爵的頭銜第1次授予約克公爵（後來的[詹姆斯二世](../Page/詹姆斯二世_\(英格蘭\).md "wikilink")）的長子（1660年─1661年），但由於他在6個月大時過世，而無法正式設立。1664年，此頭銜第1次正式設立並列入名單內，授予約克公爵的次子，但他在1667年時過世，得年僅僅3歲，因此該頭銜就被廢棄。隨後在當年度，此頭銜再次設立並授予約克公爵的三子，但他在1671年時過世，得年也僅僅3歲，該頭銜遂再度廢棄。1677年，約克公爵與第2任妻子的長子也曾在1677年獲授劍橋公爵爵位，但在大約1個月後就過世，因此也無法正式設立該爵位。

1706年，此頭銜被重新設立並授予漢諾威選帝侯（後來的[喬治一世](../Page/喬治一世_\(大不列顛\).md "wikilink")）之子[喬治·奧古斯圖斯](../Page/喬治二世_\(大不列顛\).md "wikilink")。當喬治·奧古斯圖斯於1727年繼承王位成為喬治二世時，爵位併入王位\[1\]。

後來劍橋公爵頭銜重新設立並授予[喬治三世的七子](../Page/喬治三世.md "wikilink")[阿道弗斯王子](../Page/阿道弗斯王子_\(剑桥公爵\).md "wikilink")，列入名單\[2\]。1850年，此頭銜由其獨子[喬治親王繼承](../Page/喬治親王_\(劍橋公爵\).md "wikilink")。喬治親王與結婚並育有3名兒子。但由於《》，此段婚姻無法傳承頭銜給下一代，因此喬治親王過世後，他的所有頭銜均被廢棄\[3\]。

1999年，在[伊莉莎白二世女王最小兒子](../Page/伊莉莎白二世.md "wikilink")[愛德華王子的婚禮期間](../Page/威塞克斯伯爵愛德華王子.md "wikilink")，諸專家推測其最有可能會獲得劍橋公爵或[薩塞克斯公爵](../Page/薩塞克斯公爵.md "wikilink")，隨後《》當時報導，將會頒贈愛德華王子劍橋公爵爵位\[4\]。結果，愛德華王子獲頒[威塞克斯伯爵](../Page/威塞克斯伯爵.md "wikilink")，並宣布他將會繼承[父親的](../Page/愛丁堡公爵菲利普親王.md "wikilink")[愛丁堡公爵爵位](../Page/愛丁堡公爵.md "wikilink")\[5\]。

2011年4月29日，也就是[威廉王子與](../Page/威廉王子.md "wikilink")[凱薩琳·密道頓](../Page/劍橋公爵夫人凱薩琳.md "wikilink")[結婚當日](../Page/威廉王子與凱特·米德爾頓的婚禮.md "wikilink")，宣布授予威廉王子劍橋公爵、與等3個頭銜\[6\]。2011年5月26日，這些頭銜以[英國國璽鈐印之](../Page/英國國璽.md "wikilink")[君主制誥發佈](../Page/專利特許證.md "wikilink")\[7\]。

## 劍橋公爵列表

### 1660年授而未設

| ****
[斯圖亞特王朝](../Page/斯圖亞特王朝.md "wikilink")
1660年─1661年 | 無肖像 | 1660年10月22日
[倫敦](../Page/倫敦.md "wikilink")
[詹姆斯二世與](../Page/詹姆斯二世_\(英格蘭\).md "wikilink")之子 | 未婚 | 1661年5月5日
倫敦[懷特霍爾宮](../Page/懷特霍爾宮.md "wikilink") |- |}

### 1664年第1次設立

| ****
[斯圖亞特王朝](../Page/斯圖亞特王朝.md "wikilink")
1664年─1667年
<small>同時擁有：[劍橋伯爵與](../Page/劍橋伯爵.md "wikilink") （1664年）</small> |
[James,_Duke_of_Cambridge_-_Wright_1666-7.jpg](https://zh.wikipedia.org/wiki/File:James,_Duke_of_Cambridge_-_Wright_1666-7.jpg "fig:James,_Duke_of_Cambridge_-_Wright_1666-7.jpg")
| 1663年7月12日
倫敦[聖詹姆士宮](../Page/聖詹姆士宮.md "wikilink")
[詹姆斯二世與](../Page/詹姆斯二世_\(英格蘭\).md "wikilink")之子 | 未婚 | 1667年6月20日
倫敦 |- |}

### 1667年第2次設立

| ****
[斯圖亞特王朝](../Page/斯圖亞特王朝.md "wikilink")
1667年─1671年
<small>同時擁有：[劍橋伯爵與](../Page/劍橋伯爵.md "wikilink")（1667年）</small> | 無肖像 |
1667年9月14日
倫敦[聖詹姆士宮](../Page/聖詹姆士宮.md "wikilink")
[詹姆斯二世與](../Page/詹姆斯二世_\(英格蘭\).md "wikilink")之子 | 未婚 | 1671年6月8日
倫敦 |- |}

### 1677年授而未設

| ****
[斯圖亞特王朝](../Page/斯圖亞特王朝.md "wikilink")
1677年─1677年 | 無肖像 | 1677年11月7日
倫敦[聖詹姆士宮](../Page/聖詹姆士宮.md "wikilink")
[詹姆斯二世與](../Page/詹姆斯二世_\(英格蘭\).md "wikilink")之子 | 未婚 | 1677年12月12日
倫敦[聖詹姆士宮](../Page/聖詹姆士宮.md "wikilink") |- |}

### 1706年第3次設立

| **[喬治王子](../Page/喬治二世_\(大不列顛\).md "wikilink")**
[漢諾威王朝](../Page/漢諾威王朝.md "wikilink")
1706年─1727年
<small>同時擁有：、、與（1706年─1714年）；
[威爾斯親王](../Page/威爾斯親王.md "wikilink")、[康沃爾公爵與](../Page/康沃爾公爵.md "wikilink")[羅撒西公爵](../Page/羅撒西公爵.md "wikilink")（1714年）</small>
|
[George_II.jpg](https://zh.wikipedia.org/wiki/File:George_II.jpg "fig:George_II.jpg")
| 1683年9月9日/10月30日
[漢諾威](../Page/漢諾威.md "wikilink")
[喬治一世與](../Page/喬治一世_\(大不列顛\).md "wikilink")[索菲亞·多魯西亞之子](../Page/索菲亞·多魯西亞.md "wikilink")
| 1705年8月22日
[安斯巴赫的卡羅琳](../Page/卡羅琳_\(安斯巴赫\).md "wikilink")
10名小孩 | 1760年10月25日
倫敦[肯辛頓宮](../Page/肯辛頓宮.md "wikilink") |- |
colspan=5|喬治王子因1727年[父親過世而繼承王位](../Page/喬治一世_\(大不列顛\).md "wikilink")，也就是**喬治二世**，其公爵頭銜被併入王位當中。
|- |}

### 1801年第4次設立

| **[阿道弗斯王子](../Page/阿道弗斯王子_\(劍橋公爵\).md "wikilink")**
[漢諾威王朝](../Page/漢諾威王朝.md "wikilink")
1801年─1850年
<small>同時擁有：與（1801年）</small> |
[Adolphus_Frederick_Duke_of_Cambridge.JPG](https://zh.wikipedia.org/wiki/File:Adolphus_Frederick_Duke_of_Cambridge.JPG "fig:Adolphus_Frederick_Duke_of_Cambridge.JPG")
| 1774年2月24日
倫敦[白金漢宮](../Page/白金漢宮.md "wikilink")
[喬治三世與](../Page/喬治三世.md "wikilink")[夏綠蒂王后之子](../Page/梅克倫堡-施特雷利茨的夏綠蒂.md "wikilink")
| 1818年6月18日

3名小孩 | 1850年7月8日
倫敦 |- | **[喬治親王](../Page/喬治親王_\(劍橋公爵\).md "wikilink")**
[漢諾威王朝](../Page/漢諾威王朝.md "wikilink")
1850年─1904年
<small>同時擁有：與（1801年）</small> |
[George_2nd_Cambridge.png](https://zh.wikipedia.org/wiki/File:George_2nd_Cambridge.png "fig:George_2nd_Cambridge.png")
| 1819年3月26日
[漢諾威劍橋廳](../Page/漢諾威.md "wikilink")（）
[阿道弗斯王子與](../Page/阿道弗斯王子_\(劍橋公爵\).md "wikilink")之子 | 1847年1月8日

3名小孩 | 1904年3月17日
倫敦劍橋廳 |- |
colspan=5|喬治親王與結婚並育有3名兒子。但由於《》，此段婚姻無法傳承頭銜給下一代，因此喬治親王過世後，他的所有頭銜均被廢棄。
|- |}

### 2011年第5次設立

| **[威廉王子](../Page/劍橋公爵威廉王子.md "wikilink")**
[溫莎王朝](../Page/溫莎王朝.md "wikilink")
2011年至今
<small>同時擁有：與（2011年）</small> |
[Prince_William_February_2015.jpg](https://zh.wikipedia.org/wiki/File:Prince_William_February_2015.jpg "fig:Prince_William_February_2015.jpg")
| 1982年6月21日
[倫敦](../Page/倫敦.md "wikilink")[聖瑪麗醫院](../Page/聖瑪麗醫院.md "wikilink")
[查爾斯王子與](../Page/威爾斯親王查爾斯.md "wikilink")[黛安娜王妃之子](../Page/威爾斯王妃黛安娜.md "wikilink")
| 2011年4月29日
[劍橋公爵夫人凱薩琳](../Page/劍橋公爵夫人凱薩琳.md "wikilink")
3名小孩 |  |- |}

## 繼承順位

1.  [劍橋的喬治王子](../Page/劍橋的喬治王子.md "wikilink")（2013年出生）
2.  [劍橋的路易王子](../Page/劍橋的路易王子.md "wikilink")（2018年出生）

## 參考文獻

{{-}}

[Cambridge](../Category/英国公爵.md "wikilink")
[剑桥公爵](../Category/剑桥公爵.md "wikilink")

1.

2.

3.

4.

5.

6.
7.