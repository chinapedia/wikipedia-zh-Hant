[Side_effects_of_nicotine.svg](https://zh.wikipedia.org/wiki/File:Side_effects_of_nicotine.svg "fig:Side_effects_of_nicotine.svg")的副作用。\]\]

在[醫學中](../Page/醫學.md "wikilink")，**副作用**（英語：**side
effect**）是指藥品往往有多種作用，作用於不同身體部位[受體](../Page/受體.md "wikilink")，治療時利用其一種或一部分受體作用，其他作用或是受體產生作用即變成為副作用。雖然副作用一詞常被用來形容
[不良反应
(医学)](../Page/不良反应_\(医学\).md "wikilink")，但事實上副作用也可以指那些**「有益處、意料之外」**的效果。

有時候，一些藥物的副作用反而成為醫生處方那些藥物的目的；在這種情況下，那些藥物的副作用不再是副作用，反而是藥物的主作用了。
例如：[X輻射線/X光一直被用做](../Page/X射线.md "wikilink")[醫學影像用途](../Page/醫學影像.md "wikilink")，人們原本把它的[輻射線對人體產生的效果當成是副作用](../Page/輻射.md "wikilink")。但自從人們發現[X輻射線/X光能夠用來治療](../Page/X射线.md "wikilink")[腫瘤後](../Page/腫瘤.md "wikilink")，[輻射線被應用為](../Page/輻射.md "wikilink")[放射線療法](../Page/放射線療法.md "wikilink")。在醫學影像領域中被當成副作用的輻射線效果，在[癌症治療上反而成了消滅](../Page/癌症.md "wikilink")[贅生物的正作用了](../Page/贅生物.md "wikilink")。

## 副作用出現頻率的术语

通常以下列幾個術語來表示副作用出現的[機率](../Page/機率.md "wikilink")： \[1\]\[2\]

  - 非常常見（Very common）\>=1/10
  - 常見（ Common（frequent））\>=1/100 & \<1/10
  - 不常見（Uncommon（infrequent））\>=1/1000 & \<1/100
  - 罕見（Rare）\>=1/10000 & \<1/1000
  - 非常罕見（Very rare）\<1/10000

## 有治療作用的「副作用」

[Side_effects_of_nicotine.png](https://zh.wikipedia.org/wiki/File:Side_effects_of_nicotine.png "fig:Side_effects_of_nicotine.png")可能的副作用\]\]

  - [安维汀](../Page/安维汀.md "wikilink")，原本用來減緩血管的生成，後來也被用來治療[黃斑部病變](../Page/黃斑部病變.md "wikilink")、
    [糖尿病视网膜病变和](../Page/糖尿病视网膜病变.md "wikilink")引起的[黃斑水腫](../Page/黃斑水腫.md "wikilink")。\[3\]

  - 曾於198年至1995年被試驗發現對於治療嚴重、頑固性的憂鬱是有療效的。\[4\]\[5\]

  - [安非他酮](../Page/安非他酮.md "wikilink")，一種[抗憂鬱劑](../Page/抗憂鬱劑.md "wikilink")，但後來也被用來治療[菸癮](../Page/菸癮.md "wikilink")。

  - [卡马西平被核可於治療](../Page/卡马西平.md "wikilink")[躁鬱症和](../Page/躁鬱症.md "wikilink")
    [癲癇發作](../Page/癲癇發作.md "wikilink")。但Carbamazepine的副作用對於治療下列症狀也很有效：[注意力不足過動症](../Page/注意力不足過動症.md "wikilink")
    (ADHD)、[思覺失調症](../Page/思覺失調症.md "wikilink")、[幻肢](../Page/幻肢.md "wikilink")、[阵发性剧痛症](../Page/阵发性剧痛症.md "wikilink")、、和
    [創傷後壓力症候群](../Page/創傷後壓力症候群.md "wikilink")。\[6\]

  - [地塞米松是一种人工合成的](../Page/地塞米松.md "wikilink")[皮質類固醇](../Page/皮質類固醇.md "wikilink")\[7\]\[8\]

  - 具有強效[抗組織胺的特性被用來治療](../Page/抗組織胺.md "wikilink")和嚴重[過敏](../Page/過敏.md "wikilink")。\[9\]

  - [加巴喷丁](../Page/加巴喷丁.md "wikilink")，被批准用來治療[癲癇綜合症和成人](../Page/癲癇綜合症.md "wikilink")。Gabapentin的副作用也可以用來治療[躁鬱症](../Page/躁鬱症.md "wikilink")、、[潮熱](../Page/潮熱.md "wikilink")、預防[偏頭痛](../Page/偏頭痛.md "wikilink")、
    綜合症、幻肢綜合症（phantom limb syndrome）、和
    [不寧腿症候群](../Page/不寧腿症候群.md "wikilink")。\[10\]

  - 具有強效[抗組織胺的特性被用來治療](../Page/抗組織胺.md "wikilink")和嚴重[過敏](../Page/過敏.md "wikilink")。\[11\]

  - [加巴喷丁](../Page/加巴喷丁.md "wikilink")，被批准用來治療[癲癇綜合症和成人](../Page/癲癇綜合症.md "wikilink")。Gabapentin的副作用也可以用來治療[躁鬱症](../Page/躁鬱症.md "wikilink")、、[潮熱](../Page/潮熱.md "wikilink")、預防[偏頭痛](../Page/偏頭痛.md "wikilink")、
    綜合症、幻肢綜合症、和 [不寧腿症候群](../Page/不寧腿症候群.md "wikilink")。\[12\]

  - [羟嗪](../Page/羟嗪.md "wikilink")
    是一種[抗組織胺也被用作](../Page/抗組織胺.md "wikilink")。\<\!--

  - [硫酸镁](../Page/硫酸镁.md "wikilink") in obstetrics for [premature
    labor](../Page/早產.md "wikilink") and
    [preeclampsia](../Page/妊娠毒血症.md "wikilink").\[13\]--\>

  - [氨甲蝶呤被批准用於治療](../Page/氨甲蝶呤.md "wikilink")[绒毛膜癌](../Page/绒毛膜癌.md "wikilink")；但實際上MTX也常配用來治療未破裂的（unruptured）[異位妊娠](../Page/異位妊娠.md "wikilink")\[14\]。

  - [选择性5-羟色胺再摄取抑制剂類藥物](../Page/选择性5-羟色胺再摄取抑制剂.md "wikilink")[舍曲林被批准為](../Page/舍曲林.md "wikilink")[抗憂鬱劑](../Page/抗憂鬱劑.md "wikilink")。它的副作用是延後男性性愛時的[射精](../Page/射精.md "wikilink")，因此也被用來治療[早發性射精](../Page/早發性射精.md "wikilink")\[15\]

  - [氨甲蝶呤被批准用於治療](../Page/氨甲蝶呤.md "wikilink")[绒毛膜癌](../Page/绒毛膜癌.md "wikilink")；但實際上MTX也常配用來治療未破裂的（unruptured）[異位妊娠](../Page/異位妊娠.md "wikilink")\[16\]。

  - [西地那非](../Page/西地那非.md "wikilink")
    原本研發的目的是治療[肺高壓](../Page/肺高壓.md "wikilink")，後來被發現能產生[勃起](../Page/勃起.md "wikilink")。

  - （一種 ）被批准用於治療「良性攝護腺肥大」（benign prostatic hyperplasia (enlarged
    prostate)）和[高血壓](../Page/高血壓.md "wikilink")。但同時也是數種以的方式處方來治療良性[diaphoresis和](../Page/汗液.md "wikilink")[多汗症的藥物之一](../Page/多汗症.md "wikilink")。\[17\]\[18\]

## 範例：不被喜歡的副作用

  - [紫錐花屬](../Page/紫錐花屬.md "wikilink") – 已被報告超過20種不同類型的反應，
    比如說：[氣喘](../Page/氣喘.md "wikilink")、[流產](../Page/流產.md "wikilink")、[荨麻疹](../Page/荨麻疹.md "wikilink")、、[肌肉痛](../Page/肌肉痛.md "wikilink")、[腸胃炎](../Page/腸胃炎.md "wikilink")。
    \[19\]
  - [短舌匹菊](../Page/短舌匹菊.md "wikilink") –
    [懷孕的婦女應該避免使用此](../Page/懷孕.md "wikilink")[草藥](../Page/草藥.md "wikilink")，因為它能誘發[子宮收縮](../Page/子宮.md "wikilink")。在動物試驗中[短舌匹菊被發現會導致](../Page/短舌匹菊.md "wikilink")[流產](../Page/流產.md "wikilink")。
  - [菊科植物](../Page/菊科.md "wikilink") –
    包含[短舌匹菊](../Page/短舌匹菊.md "wikilink")、[紫錐花屬](../Page/紫錐花屬.md "wikilink")、[蒲公英和](../Page/蒲公英.md "wikilink")。副作用包括[過敏性皮膚炎和](../Page/過敏性皮膚炎.md "wikilink")[花粉引起的過敏性鼻炎](../Page/過敏性鼻炎.md "wikilink")。

## 相關條目

  - [不良反應](../Page/不良反應.md "wikilink") （adverse effect）

## 參考資料

## 備註

## 外部連結

  - [衛生福利部](https://www.mohw.gov.tw/mp-1.html)

  - [MedEffect Canada (Health
    Canada)](http://www.hc-sc.gc.ca/dhp-mps/medeff/index-eng.php)

  - [definitions.pdf](http://www.who.int/medicines/areas/quality_safety/safety_efficacy/trainingcourses/definitions.pdf)

歡迎蒞臨相關的中文維基百科主題：

[Category:藥理學](../Category/藥理學.md "wikilink")

1.
2.  <http://www.who.int/medicines/areas/quality_safety/safety_efficacy/trainingcourses/definitions.pdf>
3.
4.
5.
6.  [Mood Stabilizers for Bipolar Disorder (Manic
    Depressive)](http://www.leeheymd.com/charts/dep4_1.html).
    Leeheymd.com (2003-08-01). Retrieved on 2011-08-17.
7.
8.
9.
10. [Off-label Use of
    Gabapentin](http://idahodur.isu.edu/leaflets/2004/GABAPENTIN%20EDU%20LEAFLET%202004.pdf)
    , Idaho Drug Utilization Review, educational leaflet, 2004.
11.
12. [Off-label Use of
    Gabapentin](http://idahodur.isu.edu/leaflets/2004/GABAPENTIN%20EDU%20LEAFLET%202004.pdf)
    , Idaho Drug Utilization Review, educational leaflet, 2004.
13.
14.
15.
16.
17.
18.
19.