[ ](https://zh.wikipedia.org/wiki/File:ClimateMapWorld.png "fig: ")
**副热带季风气候**（Subtropical monsoon
climate），又称为**亚热带季风气候**或**中國型氣候**（China
type climate），其特点是夏季高温並多雨，冬季较同纬度地区温暖湿润。主要分布于纬度在20°\~35° 之间的大陆东部。

## 分布

位于[欧亞大陆的副热带东部](../Page/欧亞大陆.md "wikilink")，北至[秦岭-淮河](../Page/秦岭-淮河线.md "wikilink")、[朝鲜半岛南端和](../Page/朝鲜半岛.md "wikilink")[日本](../Page/日本.md "wikilink")[本州島](../Page/本州島.md "wikilink")[关东地区](../Page/关东.md "wikilink")，南至[雷州半岛](../Page/雷州半岛.md "wikilink")（不包括）、台湾岛北回归线附近，包括[四国岛](../Page/四国岛.md "wikilink")、[九州岛](../Page/九州岛.md "wikilink")、[琉球群岛](../Page/琉球群岛.md "wikilink")、[臺灣島](../Page/臺灣島.md "wikilink")（[臺南市以北地區](../Page/臺南市.md "wikilink")）、[港澳](../Page/港澳.md "wikilink")。

## 成因

位于[最大的大陆与](../Page/亞歐大陸.md "wikilink")[最大的大洋之间](../Page/太平洋.md "wikilink")，[海陆热力性质差异显著](../Page/海陆热力性质差异.md "wikilink")。夏季亚欧大陆[低压槽连成一片](../Page/低压槽.md "wikilink")，海洋上[副熱帶高氣壓西伸北进](../Page/副熱帶高氣壓.md "wikilink")，从北[太平洋副熱帶高氣壓散发出来的东南季风带来丰沛的降水](../Page/太平洋.md "wikilink")；冬季强大的[蒙古高压散发出来的偏北风影响本地](../Page/蒙古高压.md "wikilink")。因风向切变符合[季风要求](../Page/季风.md "wikilink")，故为季风气候。

## 特徵

[Fz_weather.GIF](https://zh.wikipedia.org/wiki/File:Fz_weather.GIF "fig:Fz_weather.GIF")(26.06N,
119.28E)月均降水柱状图及气温曲线图\]\]

夏季高溫多雨：夏季太陽高角度大，氣溫較高，[季風槽北進](../Page/季風槽.md "wikilink")，從副熱帶高壓盤踞的熱海洋吹來的東南季風帶來豐沛的降水。夏秋季常有[熱帶氣旋來襲](../Page/熱帶氣旋.md "wikilink")。

冬季溫和少雨：最冷的月份平均溫度在18℃以下，0℃以上，冬季較溫和，因為本地緯度較低，受黑潮影響，離冬季季候風源地遠，地形起伏使冬季季候風受削弱。尤在台灣北部與日本群島南部、琉球群島一帶，因冬季風（[極地大陸氣團](../Page/極地大陸氣團.md "wikilink")）過海變性，變得較為溫和濕潤，故冬季氣候比起相同緯度的沿海城市顯得特別溫和多雨的型態。

## 自然带

为[副热带常绿阔叶林带](../Page/副热带常绿阔叶林带.md "wikilink")。

## 区内农业景观

  - 粮食：夏季种植[水稻为主](../Page/水稻.md "wikilink")，冬季种植[小麦](../Page/小麦.md "wikilink")、[地瓜等](../Page/地瓜.md "wikilink")，一年两到三熟
  - 糖料：[甘蔗](../Page/甘蔗.md "wikilink")
  - 油料：[花生](../Page/花生.md "wikilink")、[油菜](../Page/油菜.md "wikilink")
  - 纤维：[棉花](../Page/棉花.md "wikilink")
  - 水果：[柑橘](../Page/柑橘.md "wikilink")、[枇杷](../Page/枇杷.md "wikilink")、[荔枝](../Page/荔枝.md "wikilink")、[龙眼](../Page/龙眼.md "wikilink")、[香蕉](../Page/香蕉.md "wikilink")、[芒果等](../Page/芒果.md "wikilink")
  - 花卉：[水仙花等](../Page/水仙花.md "wikilink")
  - 其他：[毛竹](../Page/毛竹.md "wikilink")、[茶叶等](../Page/茶叶.md "wikilink")

## 区内主要城市

  - [东京](../Page/东京.md "wikilink")

  - [名古屋](../Page/名古屋.md "wikilink")

  - [大阪](../Page/大阪.md "wikilink")

  - [广岛](../Page/广岛.md "wikilink")

  - [福岡](../Page/福岡.md "wikilink")

  - [那霸](../Page/那霸.md "wikilink")

  - [济州](../Page/济州.md "wikilink")

  - [釜山](../Page/釜山.md "wikilink")

  - [臺北](../Page/臺北.md "wikilink")

  - [新北](../Page/新北.md "wikilink")

  - [桃園](../Page/桃園.md "wikilink")

  - [新竹](../Page/新竹.md "wikilink")

  - [臺中](../Page/臺中.md "wikilink")

  - [臺南](../Page/臺南.md "wikilink")

  - [河内](../Page/河内.md "wikilink")

  - [特別行政區](../Page/香港特別行政區.md "wikilink")

  - [特別行政區](../Page/澳門特別行政區.md "wikilink")

  - [上海](../Page/上海.md "wikilink")

  - [深圳](../Page/深圳.md "wikilink")

  - [重庆](../Page/重庆.md "wikilink")

  - [广州](../Page/广州.md "wikilink")

  - [厦门](../Page/厦门.md "wikilink")

  - [南京](../Page/南京.md "wikilink")

  - [武汉](../Page/武汉市.md "wikilink")

## 参看

  - [副熱帶濕潤氣候](../Page/副熱帶濕潤氣候.md "wikilink")
  - [季风](../Page/季风.md "wikilink")
  - [气候](../Page/气候.md "wikilink")
  - [大气环流](../Page/大气环流.md "wikilink")
  - [温带季风气候](../Page/温带季风气候.md "wikilink")
  - [热带季风气候](../Page/热带季风气候.md "wikilink")
  - [东亚季风区](../Page/东亚季风区.md "wikilink")

[category:季風](../Page/category:季風.md "wikilink")