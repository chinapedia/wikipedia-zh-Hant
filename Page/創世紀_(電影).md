[RussianArk.png](https://zh.wikipedia.org/wiki/File:RussianArk.png "fig:RussianArk.png")
《**俄罗斯方舟**》（《**創世紀**》）\[1\]（），是由[俄國](../Page/俄國.md "wikilink")[導演](../Page/導演.md "wikilink")[亚历山大·蘇古諾夫於](../Page/亚历山大·蘇古諾夫.md "wikilink")2002年所執導的[電影](../Page/電影.md "wikilink")，为[康城影展主竞赛片](../Page/康城影展.md "wikilink")。片中他「扮演」一位匿名、無影像的[旁白](../Page/旁白.md "wikilink")，隨著片中由[德萊堅](../Page/德萊堅.md "wikilink")（Sergei
Dreiden）飾演的法國文人[古斯丁](../Page/古斯丁.md "wikilink")（Marquis de
Custine），漫步在[俄國](../Page/俄國.md "wikilink")[聖彼得堡的](../Page/聖彼得堡.md "wikilink")[冬宮](../Page/冬宮.md "wikilink")（）。片中蘇古諾夫與古斯丁不僅穿梭於冬宮，在時間上甚至跨越300餘年的俄國歷史。

古斯丁在歷史上真有其人。他於1839年造訪俄國，回國寫了本遊記，本片也編入了古斯丁真實的經驗。古斯丁本人信仰虔誠，在該遊記《1839年的俄國》（**）中，他表示縱使俄國強調西化，但卻是[歐洲皮](../Page/歐洲.md "wikilink")[亞洲骨](../Page/亞洲.md "wikilink")。也因此俄國歷史是個大劇場，他在俄國碰到的人們各個都是演員。《創世紀》讓古斯丁這個角色引領鏡頭的考量便有引入歐洲角色，帶出檢視俄國自身歷史的意味。

《創世紀》錄製於未[壓縮的](../Page/壓縮.md "wikilink")[高解析度影像](../Page/高解析度影像.md "wikilink")（High
Definition
Video），並輔以[攝影機穩定器](../Page/攝影機穩定器.md "wikilink")（Steadicam）。該片穿梭於冬宮33個房間，動用800名以上的演員，全片90分鐘只是用了一个[长镜头从头到尾完成拍摄](../Page/长镜头.md "wikilink")，是電影技巧上的一個里程碑。

## 名言

片尾旁白

It's a pity you're not here with me. you would understand everything.
look,the sea is all around. We are destined to sail forever,to live
forever.

真可惜，你現在不在這裡。你終將明白所有事情。看哪，四周都是海，我們命中注定要永遠航行，永遠活下去。

## 註解

<references />

## 外部連結

  - [IMDB链接](http://us.imdb.com/Title?0318034)
  - [*Russian Ark* 官方网站](http://www.russianark.spb.ru/eng/)
  - <https://web.archive.org/web/20030603185816/http://www.wellspring.com/russianark/production.html>

[Category:2002年电影](../Category/2002年电影.md "wikilink")
[Category:俄罗斯电影作品](../Category/俄罗斯电影作品.md "wikilink")
[Category:亚历山大·索科洛夫电影](../Category/亚历山大·索科洛夫电影.md "wikilink")
[Category:俄语电影](../Category/俄语电影.md "wikilink")
[Category:藝術電影](../Category/藝術電影.md "wikilink")
[Category:俄罗斯帝国背景电影](../Category/俄罗斯帝国背景电影.md "wikilink")
[Category:圣彼得堡背景电影](../Category/圣彼得堡背景电影.md "wikilink")
[Category:历史电影](../Category/历史电影.md "wikilink")
[Category:埃尔米塔日博物馆](../Category/埃尔米塔日博物馆.md "wikilink")
[Category:2000年代歷史電影](../Category/2000年代歷史電影.md "wikilink")

1.  為台灣譯名，英譯名為*Russian Ark*