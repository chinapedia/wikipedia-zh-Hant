**劉偉強**（英文名：，\[1\]），[香港電影](../Page/香港.md "wikilink")[導演以及](../Page/導演.md "wikilink")[攝影師](../Page/攝影師.md "wikilink")。

## 生平

劉偉強自小對拍攝[電影產生興趣](../Page/電影.md "wikilink")。\[2\]父親劉國榮是一名建築公司的規劃師。而胞弟劉偉民則是一名設計師。劉偉強早年在[新界](../Page/新界.md "wikilink")[元朗大旗嶺村生活](../Page/元朗.md "wikilink")，並在村內的嶺文學校讀小學。後來一家移居[香港島](../Page/香港島.md "wikilink")[銅鑼灣禮頓道嘉柏大廈](../Page/銅鑼灣.md "wikilink")。\[3\]他曾就讀[聖貞德中學](../Page/聖貞德中學.md "wikilink")\[4\]
和[嶺南中學](../Page/嶺南中學_\(香港\).md "wikilink")（1975年入讀），至1980年中學畢業\[5\]。在校時曾是游泳學會和攝影學會成員，又為校內的話劇比賽做配樂。他之後到[中環一所交易行當了一年黃金買賣員](../Page/中環.md "wikilink")。\[6\]劉偉強在1981年進入[邵氏公司](../Page/邵氏公司.md "wikilink")，由攝影小工做起，慢慢地升至助理攝影師及副攝影師。最後在1985年正式成為攝影師，1988年為[王家衛負責](../Page/王家衛.md "wikilink")《[旺角卡門](../Page/旺角卡門.md "wikilink")》的攝影工作。1990年擔任導演，1995年與[文雋](../Page/文雋.md "wikilink")、[王晶合組](../Page/王晶_\(導演\).md "wikilink")[最佳拍檔電影公司](../Page/最佳拍檔電影公司.md "wikilink")，並把漫畫[古惑仔改編成一系列古惑仔電影](../Page/古惑仔.md "wikilink")，獲得票房佳績。\[7\]\[8\]其後劉偉強籌組[基本映畫製作公司](../Page/基本映畫.md "wikilink")\[9\]，拍攝改編自其他漫畫作品的電影，包括《[風雲雄霸天下](../Page/風雲雄霸天下.md "wikilink")》及《[中華英雄](../Page/中華英雄_\(1999年電影\).md "wikilink")》等。\[10\]

2002年，由商人[林建岳投資](../Page/林建岳.md "wikilink")，劉偉強與[麥兆輝及編劇](../Page/麥兆輝.md "wikilink")[莊文強合作編導製作的](../Page/莊文強.md "wikilink")[警匪片](../Page/警匪片.md "wikilink")《[無間道](../Page/無間道.md "wikilink")》，再在香港、[中國等地取得票房佳績](../Page/中國.md "wikilink")，并获得香港金像与台湾金马的最佳电影和最佳导演等多项大奖\[11\]，其後並拍攝《[無間道II](../Page/無間道II.md "wikilink")》及《[無間道III](../Page/無間道III.md "wikilink")》等續集。故事版權獲得[荷里活片商垂青](../Page/荷里活.md "wikilink")，並由導演-{zh:[馬田·史高西斯](../Page/馬田·史高西斯.md "wikilink");zh-hans:[马丁·斯科塞斯](../Page/马丁·斯科塞斯.md "wikilink");zh-hk:[馬田·史高西斯](../Page/馬田·史高西斯.md "wikilink");zh-tw:[馬丁·史柯西斯](../Page/馬丁·史柯西斯.md "wikilink");}-重新拍攝西片版本《[無間道風雲](../Page/無間道風雲.md "wikilink")》。\[12\]\[13\]2005年執導改編自日本賽車漫畫的電影《[頭文字D](../Page/頭文字D_\(電影\).md "wikilink")》。

在2005年第二十四屆香港電影金像獎頒獎典禮頒發最佳視覺效果獎項時，劉偉強作為頒獎嘉賓，竟在台上將得獎者之一洪毓良的名字讀成「洪唔知咩野良」（洪「不知什麼」良），被批評有欠莊重，以及中文程度欠佳。\[14\]\[15\]

2006年，劉偉強首次執導[韓國電影](../Page/韓國.md "wikilink")《[雛菊](../Page/雛菊_\(電影\).md "wikilink")》，3月9日在南韓上映，其後在香港、中國大陸及[台灣上映](../Page/台灣.md "wikilink")。\[16\]2007年，劉偉強進軍美國荷里活，執導由-{zh-hans:[理查·基尔](../Page/理查·基尔.md "wikilink");zh-hk:[李察·基爾](../Page/李察·基爾.md "wikilink");zh-tw:[李察·吉爾](../Page/李察·吉爾.md "wikilink");}-主演的電影《[強捕犯](../Page/強捕犯.md "wikilink")》。\[17\]\[18\]

2008年，劉偉強接替[黃岳泰成為](../Page/黃岳泰.md "wikilink")[香港專業電影攝影師學會會長](../Page/香港專業電影攝影師學會.md "wikilink")\[19\]。

2014年推出第二部美国电影《[青龙复仇](../Page/青龙复仇.md "wikilink")》，[马丁·斯科塞斯任监制](../Page/马丁·斯科塞斯.md "wikilink")，影片取材于真实事件，讲述两个中国兄弟在[纽约市](../Page/纽约市.md "wikilink")[黑帮里挣扎的故事](../Page/黑帮.md "wikilink")。\[20\]

2015年3月，劉偉強接替[爾冬陞成為](../Page/爾冬陞.md "wikilink")[香港導演協會會長](../Page/香港導演協會.md "wikilink")\[21\]。代表香港專業電影攝影師學會任香港電影金像獎董事局副主席。

## 電影作品

### 1980年代

<table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>|電影名稱</p></th>
<th><p><a href="../Page/電影監製.md" title="wikilink">監製</a></p></th>
<th><p><a href="../Page/導演.md" title="wikilink">導演</a></p></th>
<th><p>攝影</p></th>
<th><p>演員</p></th>
<th><p>角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1986</p></td>
<td><p><a href="../Page/殭屍家族.md" title="wikilink">殭屍家族</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/霹靂大喇叭.md" title="wikilink">霹靂大喇叭</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/富貴列車_(電影).md" title="wikilink">富貴列車</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1987</p></td>
<td><p><a href="../Page/龍兄虎弟_(1987年電影).md" title="wikilink">龍兄虎弟</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/龍虎風雲.md" title="wikilink">龍虎風雲</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/靈幻先生.md" title="wikilink">靈幻先生</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/橫財三千萬.md" title="wikilink">橫財三千萬</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1988</p></td>
<td><p><a href="../Page/天羅地網_(1988年電影).md" title="wikilink">天羅地網 (1988年電影)</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/霸王花_(電影).md" title="wikilink">霸王花</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/旺角卡門.md" title="wikilink">旺角卡門</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1989</p></td>
<td><p><a href="../Page/小小小警察.md" title="wikilink">小小小警察</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/神行太保.md" title="wikilink">神行太保</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/轟天龍虎會.md" title="wikilink">轟天龍虎會</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/伴我闖天涯.md" title="wikilink">伴我闖天涯</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第一繭.md" title="wikilink">第一繭</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 1990年代

<table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>|電影名稱</p></th>
<th><p><a href="../Page/電影監製.md" title="wikilink">監製</a></p></th>
<th><p><a href="../Page/導演.md" title="wikilink">導演</a></p></th>
<th><p>攝影</p></th>
<th><p>演員</p></th>
<th><p>角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1990</p></td>
<td><p><a href="../Page/鐵血騎警II朋黨.md" title="wikilink">朋黨</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿飛正傳.md" title="wikilink">阿飛正傳</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/風雨同路_(1990年電影).md" title="wikilink">風雨同路</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/再戰江湖.md" title="wikilink">再戰江湖</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/咖喱辣椒.md" title="wikilink">咖喱辣椒</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/勇闖天下.md" title="wikilink">勇闖天下</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1991</p></td>
<td><p><a href="../Page/殭屍至尊.md" title="wikilink">殭屍至尊</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/五億探長雷洛傳.md" title="wikilink">五億探長雷洛傳</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/豪門夜宴_(1991年電影).md" title="wikilink">豪門夜宴</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/五億探長雷洛傳II之父子情仇.md" title="wikilink">五億探長雷洛傳II之父子情仇</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/龍的傳人_(電影).md" title="wikilink">龍的傳人</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1992</p></td>
<td><p><a href="../Page/伴我縱橫.md" title="wikilink">伴我縱橫</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/亞飛與亞基.md" title="wikilink">亞飛與亞基</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/伙頭福星.md" title="wikilink">伙頭福星</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/妖獸都市.md" title="wikilink">妖獸都市</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1993</p></td>
<td><p><a href="../Page/人皮燈籠.md" title="wikilink">人皮燈籠</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/香港奇案之強姦.md" title="wikilink">香港奇案之強姦</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/射鵰英雄傳之東成西就.md" title="wikilink">射鵰英雄傳之東成西就</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/至尊三十六計之偷天換日.md" title="wikilink">至尊三十六計之偷天換日</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/超級學校霸王.md" title="wikilink">超級學校霸王</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新難兄難弟.md" title="wikilink">新難兄難弟</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/正牌韋小寶之奉旨溝女.md" title="wikilink">正牌韋小寶之奉旨溝女</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃飛鴻之三：獅王爭霸.md" title="wikilink">黃飛鴻之三：獅王爭霸</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1994</p></td>
<td><p><a href="../Page/香港淪陷.md" title="wikilink">香港淪陷</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/戀愛的天空.md" title="wikilink">戀愛的天空</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/新邊緣人.md" title="wikilink">新邊緣人</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/重慶森林.md" title="wikilink">重慶森林</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/東邪西毒.md" title="wikilink">東邪西毒</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1995</p></td>
<td><p><a href="../Page/不道德的禮物.md" title="wikilink">不道德的禮物</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/賭聖2之街頭賭聖.md" title="wikilink">賭聖2之街頭賭聖</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/百變星君.md" title="wikilink">百變星君</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/廟街故事.md" title="wikilink">廟街故事</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/慈禧秘密生活.md" title="wikilink">慈禧秘密生活</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td><p><a href="../Page/怪談協會.md" title="wikilink">怪談協會</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/人細鬼大.md" title="wikilink">人細鬼大</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/紅燈區_(電影).md" title="wikilink">紅燈區</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/百分百感覺.md" title="wikilink">百分百感覺</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/古惑仔之人在江湖.md" title="wikilink">古惑仔之人在江湖</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/古惑仔2之猛龍過江.md" title="wikilink">古惑仔2之猛龍過江</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/古惑仔3之隻手遮天.md" title="wikilink">古惑仔3之隻手遮天</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/飛虎雄心2之傲氣比天高.md" title="wikilink">飛虎雄心2之傲氣比天高</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997</p></td>
<td><p><a href="../Page/愛上100%英雄.md" title="wikilink">愛上100%英雄</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/97古惑仔戰無不勝.md" title="wikilink">97古惑仔戰無不勝</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1998</p></td>
<td><p><a href="../Page/新古惑仔之少年激鬥篇.md" title="wikilink">新古惑仔之少年激鬥篇</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/98古惑仔之龍爭虎鬥.md" title="wikilink">98古惑仔之龍爭虎鬥</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/風雲雄霸天下.md" title="wikilink">風雲雄霸天下</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999</p></td>
<td><p><a href="../Page/烈火戰車2極速傳說.md" title="wikilink">烈火戰車2極速傳說</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中華英雄_(1999年電影).md" title="wikilink">中華英雄</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 2000年代

<table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>|電影名稱</p></th>
<th><p><a href="../Page/電影監製.md" title="wikilink">監製</a></p></th>
<th><p><a href="../Page/導演.md" title="wikilink">導演</a></p></th>
<th><p>攝影</p></th>
<th><p>演員</p></th>
<th><p>角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2000</p></td>
<td><p><a href="../Page/決戰紫禁之巔.md" title="wikilink">決戰紫禁之巔</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/一見鍾情_(2000年電影).md" title="wikilink">一見鍾情</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/勝者為王_(古惑仔).md" title="wikilink">勝者為王</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2001</p></td>
<td><p><a href="../Page/不死情謎.md" title="wikilink">不死情謎</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛君如夢.md" title="wikilink">愛君如夢</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/拳神.md" title="wikilink">拳神</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002</p></td>
<td><p><a href="../Page/走火槍.md" title="wikilink">走火槍</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/當男人變成女人.md" title="wikilink">當男人變成女人</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/衛斯理藍血人.md" title="wikilink">衛斯理藍血人</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/無間道.md" title="wikilink">無間道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003</p></td>
<td><p><a href="../Page/無間道II.md" title="wikilink">無間道II</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/咒樂園.md" title="wikilink">咒樂園</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/無間道Ⅲ：終極無間.md" title="wikilink">無間道Ⅲ：終極無間</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/老鼠愛上貓.md" title="wikilink">老鼠愛上貓</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1:99電影行動.md" title="wikilink">1:99電影行動</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
<td><p><a href="../Page/頭文字D_(電影).md" title="wikilink">頭文字D</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/情義我心知.md" title="wikilink">情義我心知</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p><a href="../Page/雛菊_(電影).md" title="wikilink">雛菊</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國電影</a>，</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/傷城.md" title="wikilink">傷城</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
<td><p><a href="../Page/危險人物_(香港電影).md" title="wikilink">危險人物</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/校墓處.md" title="wikilink">校墓處</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/人在江湖_(香港電影).md" title="wikilink">人在江湖</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/小心眼.md" title="wikilink">小心眼</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/強捕犯.md" title="wikilink">強捕犯</a> (The Flock)</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/美國電影.md" title="wikilink">美國電影</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008</p></td>
<td><p><a href="../Page/阿飛_(2008年香港電影).md" title="wikilink">阿飛</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><p><a href="../Page/游龍戲鳳_(2009年電影).md" title="wikilink">游龍戲鳳</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 2010年代

<table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>|電影名稱</p></th>
<th><p><a href="../Page/電影監製.md" title="wikilink">監製</a></p></th>
<th><p><a href="../Page/導演.md" title="wikilink">導演</a></p></th>
<th><p>攝影</p></th>
<th><p>演員</p></th>
<th><p>角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2010</p></td>
<td><p><a href="../Page/精武風雲.md" title="wikilink">精武風雲</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td><p><a href="../Page/不再讓你孤單.md" title="wikilink">不再讓你孤單</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/親密敵人.md" title="wikilink">親密敵人</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/桃姐.md" title="wikilink">桃姐</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>本人</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td><p><a href="../Page/太極1從零開始.md" title="wikilink">太極1從零開始</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>楊父</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/血滴子_(2012年电影).md" title="wikilink">血滴子</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/雍正.md" title="wikilink">雍正</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大上海.md" title="wikilink">大上海</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/時間檔案館.md" title="wikilink">時間檔案館</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/微電影.md" title="wikilink">微電影</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p><a href="../Page/天台_(電影).md" title="wikilink">天台</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>市長</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014</p></td>
<td><p><a href="../Page/救火英雄.md" title="wikilink">救火英雄</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>消防處處長</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/一個人的武林.md" title="wikilink">一個人的武林</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>便衣探員</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/賭城風雲_(2014年電影).md" title="wikilink">賭城風雲</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/青龍復仇.md" title="wikilink">青龍復仇</a> (Revenge of the Green Dragons)</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/美國電影.md" title="wikilink">美國電影</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015</p></td>
<td><p><a href="../Page/賭城風雲II.md" title="wikilink">賭城風雲II</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/我是路人甲.md" title="wikilink">我是路人甲</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>他自己</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陀地驅魔人.md" title="wikilink">陀地驅魔人</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>導演</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016</p></td>
<td><p><a href="../Page/賭城風雲III.md" title="wikilink">賭城風雲III</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017</p></td>
<td><p><a href="../Page/建軍大業.md" title="wikilink">建軍大業</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2018</p></td>
<td><p><a href="../Page/武林怪獸.md" title="wikilink">武林怪獸</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2019</p></td>
<td><p><a href="../Page/使徒行者：諜影行動.md" title="wikilink">使徒行者：諜影行動</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>待上映</p></td>
<td><p><a href="../Page/烈火英雄.md" title="wikilink">烈火英雄</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>待播</p></td>
<td><p><a href="../Page/中國機長.md" title="wikilink">中國機長</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

[HK_KT_HKFDG_關信輝_Adrian_Kwan_Shun-Fai_劉偉強_Andrew_Lau_Wai-Keung_郭子健_Derek_Kwok_Chi-Kin_n_李光耀_導演_David_Lee_Kwong_Yiu_Dec-2017_IX1.jpg](https://zh.wikipedia.org/wiki/File:HK_KT_HKFDG_關信輝_Adrian_Kwan_Shun-Fai_劉偉強_Andrew_Lau_Wai-Keung_郭子健_Derek_Kwok_Chi-Kin_n_李光耀_導演_David_Lee_Kwong_Yiu_Dec-2017_IX1.jpg "fig:HK_KT_HKFDG_關信輝_Adrian_Kwan_Shun-Fai_劉偉強_Andrew_Lau_Wai-Keung_郭子健_Derek_Kwok_Chi-Kin_n_李光耀_導演_David_Lee_Kwong_Yiu_Dec-2017_IX1.jpg")

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部連結

  -
  -
  -
[Category:金馬獎最佳導演獲得者](../Category/金馬獎最佳導演獲得者.md "wikilink")
[Category:香港电影导演](../Category/香港电影导演.md "wikilink")
[Category:香港電影监制](../Category/香港電影监制.md "wikilink")
[Category:香港電影攝影師](../Category/香港電影攝影師.md "wikilink")
[W](../Category/劉姓.md "wikilink")
[Category:聖貞德中學校友](../Category/聖貞德中學校友.md "wikilink")
[Category:香港電影金像獎最佳攝影得主](../Category/香港電影金像獎最佳攝影得主.md "wikilink")
[Category:嶺南中學校友 (香港)](../Category/嶺南中學校友_\(香港\).md "wikilink")
[Category:香港電影金紫荊獎最佳導演得主](../Category/香港電影金紫荊獎最佳導演得主.md "wikilink")
[Category:無間道](../Category/無間道.md "wikilink")
[Category:香港客家人](../Category/香港客家人.md "wikilink")

1.

2.

3.

4.

5.  [嶺南中學《校園通訊》第29期，2015年](http://www.lingnan.edu.hk/com/dl/1516/no29/index.pdf)

6.  [嶺南中學《校園通訊》第29期，2015年](http://www.lingnan.edu.hk/com/dl/1516/no29/index.pdf)

7.
8.
9.

10.
11.

12.
13.

14. <http://paper.wenweipo.com/2005/03/28/EN0503280003.htm>

15.
16.

17.
18.
19.

20.

21.