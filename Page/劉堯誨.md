**劉堯誨**（），字君纳，号凝斋，[湖廣](../Page/湖廣.md "wikilink")[臨武縣人](../Page/臨武縣.md "wikilink")。[明朝政治人物](../Page/明朝.md "wikilink")。嘉靖進士。以[新喻縣知縣歷官](../Page/新喻縣.md "wikilink")[給事中](../Page/給事中.md "wikilink")，累升[福建巡撫](../Page/福建巡撫.md "wikilink")、[兩廣總督等職](../Page/兩廣總督.md "wikilink")。官至[南京](../Page/南京.md "wikilink")[兵部尚書](../Page/兵部尚書.md "wikilink")。

## 生平

[嘉靖三十二年](../Page/嘉靖.md "wikilink")（1553年）进士。授江西[新喻縣知縣](../Page/新喻縣.md "wikilink")，以治行高等，擢南京刑科[给事中](../Page/给事中.md "wikilink")。[萬曆初年](../Page/萬曆.md "wikilink")，以僉都御史任[巡撫福建](../Page/福建巡撫.md "wikilink")，與廣東提督[殷正茂和西班牙聯軍剿滅巨寇](../Page/殷正茂.md "wikilink")[林鳳](../Page/林鳳_\(潮州饒平\).md "wikilink")\[1\]。此後改[江西巡撫](../Page/江西巡撫.md "wikilink")，升任[兩廣總督](../Page/兩廣總督.md "wikilink")，官至[南京兵部尚書](../Page/南京兵部尚書.md "wikilink")，参赞機要事務。致仕归鄉而卒。\[2\]\[3\]\[4\]

## 著作

著有《虚籁集》十四卷。\[5\]另有《奏议》、《岭南议》、《左传评林》、《宙园吟稿》，纂嘉靖《临武县志》等。

## 參考文獻

  - 《明隆萬之際粵東巨盜林鳳事蹟詳考——以劉堯誨《督撫疏議》中林鳳史料為中心》，《歷史研究》，2012年第6期

## 外部連接

  - [刘尧诲](http://www.czs.gov.cn/html/zjcz/czrw/content_243989.html) 郴州人物

{{-}}

[Category:明朝新喻縣知縣](../Category/明朝新喻縣知縣.md "wikilink")
[Category:明朝給事中](../Category/明朝給事中.md "wikilink")
[Category:明朝福建巡抚](../Category/明朝福建巡抚.md "wikilink")
[Category:明朝江西巡抚](../Category/明朝江西巡抚.md "wikilink")
[Category:明朝兩廣總督](../Category/明朝兩廣總督.md "wikilink")
[Category:明朝兵部尚書](../Category/明朝兵部尚書.md "wikilink")
[Category:臨武人](../Category/臨武人.md "wikilink")
[Y堯](../Category/劉姓.md "wikilink")

1.  《福建省志》，福建省地方誌編纂委員會編，1992年。
2.  《漢籍電子文獻資料庫-明人傳記資料索引》8767：劉堯晦，字君納，號凝齋，臨武人。嘉靖三十二年進士，歷官僉都御史、巡撫福建。平巨冦林鳳，擒倭朶麻里，奏設總兵鎮南澳島。移撫江西，陞兩廣總制。平鬱林、木頭等十砦，晉南京兵部尚書，參賛機務，致仕歸卒，年六十四。有虛籟集。
3.  [王世貞](../Page/王世貞.md "wikilink")《弇山堂別集·[卷六十四](../Page/s:弇山堂别集_\(四庫全書本\)/卷064.md "wikilink")》：劉堯誨，湖廣臨武人，嘉靖癸丑進士，萬厯六年以兵左侍任加右都御史。
4.  《臨武縣志·卷之三十三·人物志十三》：劉堯誨，字君納，號凝齋，生有異徵，美髯修貌，沉毅寡言。弱冠舉於鄉……。
5.  《欽定續文獻通考·卷一百九十三》：劉堯誨《虛籟集》十四卷。堯誨，號凝齋，臨武人，嘉靖進士，官至南京兵部尚書。