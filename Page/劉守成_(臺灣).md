<table>
<thead>
<tr class="header">
<th><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 學歷</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li>輔仁大學哲學研究所博士班研究</li>
<li>輔仁大學哲學研究所碩士</li>
<li>國立政治大學哲學系畢業</li>
<li><a href="../Page/台灣省立宜蘭中學.md" title="wikilink">台灣省立宜蘭中學高中部畢業</a>（1970年）</li>
<li>宜蘭縣立宜蘭初級中學畢業（1967年）</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 經歷</p></td>
</tr>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li>八十年代雜誌執行主編</li>
<li>噶瑪蘭週刊總編輯</li>
<li>民主進步黨黨外宜蘭公政會憲政小組召集人</li>
<li>民主進步黨宜蘭縣黨部主任委員</li>
<li>民主進步黨中央黨部（第五屆）中常委</li>
<li>臺灣省議會（第九屆）議員<br />
<span style="color: blue;">(1989年-1994年)</span></li>
<li>臺灣省議會（第十屆）議員<br />
<span style="color: blue;">(1994年-1997年)</span></li>
<li>宜蘭縣（第十三屆）縣長<br />
<span style="color: blue;">(1997年12月20日-2001年12月20日)</span></li>
<li>宜蘭縣（第十四屆）縣長<br />
<span style="color: blue;">(2001年12月20日-2005年12月20日)</span></li>
<li>考試院保訓會 主任委員<br />
<span style="color: blue;">(2006年-2008年)</span></li>
</ul>
</div></td>
</tr>
<tr class="even">
<td><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 著作</p></td>
</tr>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li><span style="color: green;">《萊爾「心靈概念之研究」》</span><br />
（碩士論文），柏克萊，時報文化出版</li>
</ul>
</div></td>
</tr>
</tbody>
</table>

**劉守成**（），[中華民國](../Page/中華民國.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")，[宜蘭縣人](../Page/宜蘭縣.md "wikilink")，[民主進步黨籍](../Page/民主進步黨.md "wikilink")，妻子[田秋堇為現任](../Page/田秋堇.md "wikilink")[監察委員](../Page/監察委員.md "wikilink")。[政治大學](../Page/政治大學.md "wikilink")[哲學](../Page/哲學.md "wikilink")[學士](../Page/學士.md "wikilink")、[輔仁大學](../Page/輔仁大學.md "wikilink")[哲學](../Page/哲學.md "wikilink")[碩士](../Page/碩士.md "wikilink")。曾任噶瑪蘭週刊總編輯、[臺灣省議會](../Page/臺灣省議會.md "wikilink")[議員](../Page/議員.md "wikilink")、[宜蘭縣](../Page/宜蘭縣.md "wikilink")[縣長](../Page/縣長.md "wikilink")、[考試院](../Page/考試院.md "wikilink")[公務人員保障暨培訓委員會主任委員等](../Page/公務人員保障暨培訓委員會.md "wikilink")。

## 噶瑪蘭週刊

1986年5月11日《噶瑪蘭週刊》創辦，劉守成先生擔任總編輯，以「宜蘭縣民的雜誌」自許，以「真實而深入的報導、強硬而理性的批判、誠摯而開放的服務」為辦報原則。

## 選舉

### 1997宜蘭縣長選舉

| 1997年[宜蘭縣縣長選舉結果](../Page/宜蘭縣縣長.md "wikilink") |
| --------------------------------------------- |
| 號次                                            |
| 1                                             |
| 2                                             |
| **選舉人數**                                      |
| **投票數**                                       |
| **有效票**                                       |
| **無效票**                                       |
| **投票率**                                       |

### 2001宜蘭縣長選舉

| 2001年[宜蘭縣縣長選舉結果](../Page/宜蘭縣縣長.md "wikilink") |
| --------------------------------------------- |
| 號次                                            |
| 1                                             |
| 2                                             |
| 3                                             |
| 4                                             |
| **選舉人數**                                      |
| **投票數**                                       |
| **有效票**                                       |
| **無效票**                                       |
| **投票率**                                       |

## 外部連結

|- |colspan="3"
style="text-align:center;"|**[宜蘭縣政府](../Page/宜蘭縣政府.md "wikilink")**
|-       [category:宜蘭縣縣長](../Page/category:宜蘭縣縣長.md "wikilink")
[category:宜蘭人](../Page/category:宜蘭人.md "wikilink")

[Category:中華民國公務人員保障暨培訓委員會主任委員](../Category/中華民國公務人員保障暨培訓委員會主任委員.md "wikilink")
[Category:第10屆臺灣省議員](../Category/第10屆臺灣省議員.md "wikilink")
[Category:第9屆臺灣省議員](../Category/第9屆臺灣省議員.md "wikilink")
[Category:民主進步黨黨員](../Category/民主進步黨黨員.md "wikilink")
[Category:輔仁大學校友](../Category/輔仁大學校友.md "wikilink")
[Category:國立政治大學校友](../Category/國立政治大學校友.md "wikilink")
[Category:國立宜蘭高級中學校友](../Category/國立宜蘭高級中學校友.md "wikilink")
[Shou守](../Category/劉姓.md "wikilink")