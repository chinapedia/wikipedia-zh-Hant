**劉宗敏**（），[明末民變](../Page/明末民變.md "wikilink")[李自成軍主將](../Page/李自成.md "wikilink")。[陝西](../Page/陝西.md "wikilink")[藍田人](../Page/藍田.md "wikilink")，[鍛工出身](../Page/鍛.md "wikilink")。

## 生平

崇禎十一年（1638年），[李自成軍在](../Page/李自成.md "wikilink")[陝西](../Page/陝西.md "wikilink")[潼關遭官軍合擊](../Page/潼關.md "wikilink")，他與李自成率餘眾18人騎突圍，隱至[商洛山](../Page/商洛山.md "wikilink")。次年再起。[崇禎十三年助李自成突圍](../Page/崇禎.md "wikilink")[巴西](../Page/巴郡.md "wikilink")、魚腹諸山，入[河南災區](../Page/河南.md "wikilink")，聲勢大振。十六年任[權將軍](../Page/權將軍.md "wikilink")。次年，李自成軍兵分南、北兩路，東渡黃河，直趨[燕京](../Page/燕京.md "wikilink")。後宗敏協助李自成率北路軍經[大同](../Page/大同市.md "wikilink")（今屬山西）、[宣府](../Page/宣府.md "wikilink")（河北[宣化](../Page/宣化.md "wikilink")），至北京城下。三月十七日夜，指揮北路軍攻佔外城。十九日，攻克京城。[崇禎帝於](../Page/崇禎帝.md "wikilink")[煤山](../Page/煤山.md "wikilink")[自縊](../Page/自縊.md "wikilink")。

李自成進[京師後](../Page/京師.md "wikilink")，實施“助餉”政策，設立“[比餉鎮撫司](../Page/比餉鎮撫司.md "wikilink")”，以劉宗敏、[李過主之](../Page/李過.md "wikilink")，搜刮京城官员大户的财产。規定助餉額為“中堂十萬，部院京堂錦衣七萬或五萬三萬，道科吏部五萬三萬，翰林三萬二萬一萬，部屬而下則各以千計”
(《[甲申核真略](../Page/甲申核真略.md "wikilink")》)。\[1\]

《[鹿樵紀聞](../Page/綏寇紀略.md "wikilink")》記載，吳三桂本欲归顺李自成，但听说自己的父亲被比餉鎮撫司拷打，而且劉宗敏強佔自己的愛妾[陳圓圓](../Page/陳圓圓.md "wikilink")，暴怒，拒绝归顺李自成。明末清初詩人[吳偉業](../Page/吳偉業.md "wikilink")《圓圓曲》寫道“痛哭三軍俱縞素，衝冠一怒為紅顏。”小說家[姚雪垠在](../Page/姚雪垠.md "wikilink")《論＜圓圓曲＞》一文称，据他考据，陳圓圓當時不在[北京](../Page/北京.md "wikilink")，而在[寧遠](../Page/寧遠.md "wikilink")（[興城](../Page/興城.md "wikilink")，在[錦州南邊](../Page/錦州.md "wikilink")），不久病死\[2\]。

三月二十六日行劝进之礼时，刘宗敏竟对众官说：“我与他同作响马，何故拜他?”\[3\]。5月17日，李自成在[武英殿召集軍事會議](../Page/武英殿.md "wikilink")，擬派大將劉宗敏出兵山海關，讨伐吴三桂。刘竟顶撞说：「大家都是做贼的，凭甚么你在京城享受，卻让我去前线卖命？」\[4\]李自成无奈，只好率队亲征。刘宗敏不好再推托，隨李自成討吳三桂。

5月26日，大顺军向[山海关猛攻](../Page/山海关.md "wikilink")，[吴三桂拼死抗拒](../Page/吴三桂.md "wikilink")，寡不敌众。後吳三桂降清，与清军联合大败大顺军。闖軍撤退，败出北京。清順治二年（1645年）四月下旬，清军在距[江西](../Page/江西.md "wikilink")[九江四十里处攻入大顺军老营](../Page/九江.md "wikilink")，劉宗敏在[通山](../Page/通山.md "wikilink")（今屬[湖北](../Page/湖北.md "wikilink")）作戰中同李自成的两位叔父赵侯和襄南侯被清軍俘獲殺死\[5\]。

## 注釋

<div class="references-small">

<references />

</div>

[L刘](../Category/明朝民變人物.md "wikilink")
[L刘](../Category/蓝田人.md "wikilink")
[Z宗](../Category/劉姓.md "wikilink")
[L刘](../Category/被處決的中國人.md "wikilink")
[Category:大顺军事人物](../Category/大顺军事人物.md "wikilink")

1.  《[國榷](../Page/國榷.md "wikilink")》卷一百一：“（李自成）賞各將百金，各兵十金。吏卒大失望，更賞卒白布四丈，青布八丈，皆市𠪨奪取之。時都人大失望，[牛金星](../Page/牛金星.md "wikilink")、[顧君恩以告](../Page/顧君恩.md "wikilink")。劉宗敏曰：‘今但畏軍變，不畏民變。……且軍兵日弗萬金，若不強取，從何而給？”
2.  《[文學遺產](../Page/文學遺產.md "wikilink")》季刊1980年第一期
3.  《[甲申纪事](../Page/甲申纪事.md "wikilink")》
4.  《[再生纪略](../Page/再生纪略.md "wikilink")》：“逆闯每欲僭位，其下即相对偶语云：‘以响马拜响马，谁甘屈膝。’又云：‘我辈血汗杀来天下，不是他的本事。’繁言喷喷，逆闯心不甚安。”
5.  《[清世祖实录](../Page/清世祖实录.md "wikilink")》卷十八