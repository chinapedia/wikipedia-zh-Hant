[HK_Pokfulam_Water_Street_Lau_Ka_Workshop.JPG](https://zh.wikipedia.org/wiki/File:HK_Pokfulam_Water_Street_Lau_Ka_Workshop.JPG "fig:HK_Pokfulam_Water_Street_Lau_Ka_Workshop.JPG")的音樂工作室\]\]

**劉家昌**（）或（），出生于[滿洲國](../Page/滿洲國.md "wikilink")[哈爾濱市](../Page/哈爾濱市.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")、[填詞人](../Page/填詞人.md "wikilink")、[電影導演](../Page/電影導演.md "wikilink")。

## 生平

劉家昌童年時因[中國抗日戰爭戰亂動盪而移居遷往朝鮮日殖時期的](../Page/中國抗日戰爭.md "wikilink")[仁川](../Page/仁川.md "wikilink")，中學時代當職業演唱，[新竹中學畢業](../Page/新竹中學.md "wikilink")，1962年當僑生到[中華民國唸](../Page/中華民國.md "wikilink")[國立政治大學政治學系肄業](../Page/國立政治大學.md "wikilink")。1966年與台灣知名女星[江青結婚](../Page/江青_\(演員\).md "wikilink")，兩人生下一子劉繼成（後改名為劉繼鵬），不過旋即於1970年離婚。之後，他於中華民國演藝圈創作大量[流行曲](../Page/流行曲.md "wikilink")，[電影](../Page/電影.md "wikilink")[配樂及當演員](../Page/配樂.md "wikilink")、[電影導演等](../Page/電影導演.md "wikilink")。
1972年他以《[晚秋](../Page/晚秋.md "wikilink")》電影成名，1975年的愛國電影《[梅花](../Page/梅花_\(電影\).md "wikilink")》更讓他於中華民國家喻戶曉。之後，他開始追求電影界亞洲影后[甄珍](../Page/甄珍.md "wikilink")。劉家昌與甄珍即於美國拉斯維加斯結婚，但當時甄珍的父母反對，因此女方全家及親友都未參加他們的婚禮。

藝人[費貞綾曾先後介紹弟弟](../Page/費貞綾.md "wikilink")[費玉清與](../Page/費玉清.md "wikilink")[張菲給劉家昌認識](../Page/張菲.md "wikilink")，希望劉家昌提拔他們進入歌壇。劉家昌在[中天綜合台](../Page/中天綜合台.md "wikilink")[談話性節目](../Page/談話性節目.md "wikilink")《[康熙來了](../Page/康熙來了.md "wikilink")》回憶，費貞綾帶費玉清去[錄音室見他](../Page/錄音室.md "wikilink")，他看出費玉清適合當歌星，於是收費玉清為徒；之後費貞綾帶張菲去錄音室見他，他看出張菲在歌壇發展不會比費玉清好，於是他說「去去去，怎麼都是你們家的人」趕走張菲，張菲頗有怨言。

1979年，劉家昌因[谷名倫墜樓事件疑點重重而離華定居](../Page/谷名倫.md "wikilink")[美國](../Page/美國.md "wikilink")。之後表示：谷名倫墜樓身亡是因試戲不慎墜樓，並非自殺。根據報導谷名倫曾遭受劉家昌多次辱罵而自殺。其中真相不得而知。\[1\]

1980年代，創辦[歐帝威唱片](../Page/歐帝威唱片.md "wikilink")，發行自己的專輯《在雨中》等。

1982年在美國與妻子甄珍合作經營旅館，1999年返華再發展有線電視頻道業者[八大電視與](../Page/八大電視.md "wikilink")[博新多媒體](../Page/博新多媒體.md "wikilink")，並參與[中國國民黨的政黨活動](../Page/中國國民黨.md "wikilink")。2000年，他再因掏空媒體爭議，前往中國大陸從事酒店旅館業務。

2001年，劉家昌獲頒[第12屆金曲獎](../Page/第12屆金曲獎.md "wikilink")[特別貢獻獎](../Page/特別貢獻獎_\(金曲獎\).md "wikilink")；該屆的特別貢獻獎共有兩位得主，另一位是音樂家[許常惠](../Page/許常惠.md "wikilink")。

2006年6月，劉家昌於[香港舉行](../Page/香港.md "wikilink")《往事只能回味：劉家昌音樂會》。

2007年4月8日晚，[中國](../Page/中國.md "wikilink")[北京第](../Page/北京.md "wikilink")7届[音乐风云榜颁奖盛典上](../Page/音乐风云榜.md "wikilink")，刘家昌获颁终身成就奖。

2009年，因淘空國民黨黨營事業，遭臺北地檢署依背信罪名起訴並被限制出境。本人過去曾表示：「我不會回台灣，直到陳水扁下台。」並嘲笑檢方抓不到他，但於2009年2月28日返台投案，稱自己為國民黨處理黨產之人頭。\[2\]

2010年，事涉法律案件有轉寰空間，劉家昌回台發展。4月3日、4月4日，在[台北小巨蛋](../Page/台北小巨蛋.md "wikilink")，及9月17、9月18日晚上在台中[國立中興大學蕙蓀堂舉辦](../Page/國立中興大學.md "wikilink")《往事只能回味：劉家昌音樂會》。

2011年8月，其子[劉子千唱](../Page/劉子千.md "wikilink")《唸你》的鴨子嗓被廣大網友批評和惡搞，劉家昌對於外界批評歸咎是技術性問題，他說：「我們母帶聲音沒有問題，因為聲音的問題是在剪接師過帶的時候出現的，電腦低音沒打開，所以會犯這樣的錯。」\[3\]

2014年9月20日，劉家昌在[新加坡室內體育館舉行](../Page/新加坡.md "wikilink")《往事只能回味》音樂會。

2015年5月21日，甄珍自爆，其實她跟劉家昌婚姻關係只維持了9年，在台灣歌手兒子[劉子千](../Page/劉子千.md "wikilink")1歲時就離婚，為了給兒子完整幸福快樂的家庭，2人才決定保密，劉子千也是直到2年前才知道這件事，至於離婚原因是什麼並不願多說，只希望兩人以後能互相尊重。\[4\]

2017年5月22日，劉家昌在臉書上辱罵[甄珍](../Page/甄珍.md "wikilink")、[劉子千母子](../Page/劉子千.md "wikilink")，痛斥其「無恥、吃我的喝我的，跟我作對。」等不堪字眼。同年7月5日，甄珍批露劉家昌擅自干涉劉子千音樂事業、強迫其做出《唸你》專輯，「讓他現在看到吉他都反感」，並持續向她索取金援的多年痛苦經歷。\[5\]

2017年11月3日，台塑集團[王永慶三房之女王瑞華因愛慕他](../Page/王永慶.md "wikilink")，以極大筆資金投資劉家昌的地產，完成他在[江西打造的世界畫牆在華泉小鎮開幕](../Page/江西.md "wikilink")。12月2日，新聞刊登劉家昌曾密戀[張艾嘉兩人同居兩年](../Page/張艾嘉.md "wikilink")。老製片發行人江文雄提證他兩人居住在他房子。導演王時正也証明此新聞真實性。據說，劉家昌在演藝圈曾與多位藝人鬧上新聞版面，有聲音如鄧麗君的某小調歌星。

2018年初兩人家庭矛盾升級，劉家昌指控畢生所得且經過法院認可的財產約20億台幣，全被[甄珍](../Page/甄珍.md "wikilink")、[劉子千私吞](../Page/劉子千.md "wikilink")，自己一毛無得，且六年多來母子兩人還發動媒體戰意圖奪得輿論制高點，將他鬥臭鬥垮，華泉小鎮開幕後從未來過的母子兩人跑去[江西第一件事就是去告他](../Page/江西.md "wikilink")，認為他還有財產沒交出，其指控「母子加起來過百歲，沒做過一天事，到今天吃我喝我，卻置我於死，這種家人有人敢要嗎？人怎沒有感情親情，沒有愛呢？太貪得無厭了」，甚至數落前妻，「你把我兒子怎麼糟蹋的，你借著保護兒子在媒體悔（毀）謗我。」並決定使用法律反擊。\[6\]

## 提拔過及合作的藝人

| 藝人                                         | 歌曲                                                       |
| ------------------------------------------ | -------------------------------------------------------- |
| [甄妮](../Page/甄妮.md "wikilink")             | 誓言、晚秋、我家在哪裡、雲河                                           |
| [鳳飛飛](../Page/鳳飛飛.md "wikilink")           | 敲敲門、小貝殼、星語                                               |
| [尤雅](../Page/尤雅.md "wikilink")             | 往事只能回味、在雨中                                               |
| [鄧麗君](../Page/鄧麗君.md "wikilink")           | 獨上西樓、詩意                                                  |
| [崔苔菁](../Page/崔苔菁.md "wikilink")           | 落葉的煩惱                                                    |
| [江蕾](../Page/江蕾.md "wikilink")             | [煙雨斜陽](../Page/煙雨斜陽.md "wikilink")                       |
| [高凌風](../Page/高凌風.md "wikilink")           | 不一樣                                                      |
| [劉文正](../Page/劉文正.md "wikilink")           | 諾言、小雨打在我身上                                               |
| [蕭孋珠](../Page/蕭孋珠.md "wikilink")           | [一簾幽夢](../Page/一簾幽夢.md "wikilink")                       |
| [張俐敏](../Page/張俐敏.md "wikilink")           | 楓林小雨                                                     |
| [費玉清](../Page/費玉清.md "wikilink")           | 白雲長在天、晚安曲、[中華民國頌](../Page/中華民國頌.md "wikilink")           |
| [林青霞](../Page/林青霞.md "wikilink")           |                                                          |
| [劉子千](../Page/劉子千.md "wikilink")           | 唸你                                                       |
| [乾德門](../Page/乾德門.md "wikilink")           |                                                          |
| [黃鶯鶯](../Page/黃鶯鶯.md "wikilink")           | 我心深處                                                     |
| [千百惠](../Page/千百惠.md "wikilink")           |                                                          |
| [朱海君](../Page/朱海君.md "wikilink")           |                                                          |
| [鄭嘉穎](../Page/鄭嘉穎.md "wikilink")           |                                                          |
| [鄭欣宜](../Page/鄭欣宜.md "wikilink")（Joyce）    | 連心                                                       |
| [謝金燕](../Page/謝金燕.md "wikilink")           | 你真酷                                                      |
| [張帝](../Page/張帝.md "wikilink")             |                                                          |
| [張魁](../Page/張魁_\(藝人\).md "wikilink")      | 小丑                                                       |
| [陽帆](../Page/陽帆.md "wikilink")             | 揚帆                                                       |
| [沈雁](../Page/沈雁.md "wikilink")             |                                                          |
| [楊林](../Page/楊林.md "wikilink")             |                                                          |
| [潘安邦](../Page/潘安邦.md "wikilink")           |                                                          |
| [翁倩玉](../Page/翁倩玉.md "wikilink")           | 海鷗                                                       |
| [陳盈潔](../Page/陳盈潔.md "wikilink")           | 誓言(1987同名電影主題曲)                                          |
| [王海玲](../Page/王海玲.md "wikilink")           |                                                          |
| [林靈](../Page/林靈.md "wikilink")             |                                                          |
| [陳昇](../Page/陳昇.md "wikilink")             |                                                          |
| [楊宗憲](../Page/楊宗憲.md "wikilink")           |                                                          |
| 王電                                         |                                                          |
| [姚贝娜](../Page/姚贝娜.md "wikilink")           |                                                          |
| [宋文](../Page/宋文.md "wikilink")（Ariel Sung） | 你我的家 、不要你㸃頭 、能不能 、真話 、風裡的擁抱 、去而復返、那個是你那個是我 、我的心裡不像外表 、江水 |

## 合作過的作詞人

| 作詞人                                | 歌名                                          | 原唱                               | 備註          |
| ---------------------------------- | ------------------------------------------- | -------------------------------- | ----------- |
| [瓊瑤](../Page/瓊瑤.md "wikilink")     | [庭院深深](../Page/庭院深深.md "wikilink")          | [歸亞蕾](../Page/歸亞蕾.md "wikilink") | 電影《庭院深深》主題曲 |
| [一簾幽夢](../Page/一簾幽夢.md "wikilink") | [蕭孋珠](../Page/蕭孋珠.md "wikilink")            | 電影《一簾幽夢》歌曲                       |             |
| [失意](../Page/失意.md "wikilink")     |                                             |                                  |             |
| [秋歌](../Page/秋歌.md "wikilink")     | [甄妮](../Page/甄妮.md "wikilink")              | 電影《秋歌》歌曲                         |             |
| 秋天在我們手裡                            |                                             |                                  |             |
| 我倆在一起\[7\]                         | \-                                          |                                  |             |
| [莊奴](../Page/莊奴.md "wikilink")     | [煙雨斜陽](../Page/煙雨斜陽.md "wikilink")          | [江蕾](../Page/江蕾.md "wikilink")   | 電影《煙雨斜陽》主題曲 |
| [林煌坤](../Page/林煌坤.md "wikilink")   | [往事只能回味](../Page/往事只能回味.md "wikilink")\[8\] | [尤雅](../Page/尤雅.md "wikilink")   |             |

## 評價

  - [尤雅說](../Page/尤雅.md "wikilink")，劉家昌很疼新人，但也很會罵人，只是她並不怕劉家昌。
  - [李敖評](../Page/李敖.md "wikilink")：「理想的民族主義者」\[9\] （主張未來兩岸統一）。
  - [甄珍](../Page/甄珍.md "wikilink")：「古人說『虎毒不食子』，劉家昌卻為了操控兒子，不惜殘忍傷害兒子名譽」\[10\]

## 電影作品

  - 1968年《[二十年代](../Page/二十年代_\(電影\).md "wikilink")》
  - 1968年「春夢了無痕」 艾黎、唐威、巴戈、古軍、曹健
  - 1970年《[家花總比野花香](../Page/家花總比野花香.md "wikilink")》
  - 1971年《[問白雲](../Page/問白雲.md "wikilink")》
  - 1971年《[往事只能回味](../Page/往事只能回味.md "wikilink")》 尤雅，唐威　 　
  - 1971年《[有我就有你](../Page/有我就有你.md "wikilink")》
  - 1971年《[老爺車](../Page/老爺車_\(電影\).md "wikilink")》
  - 1972年《[只要為你活一天](../Page/只要為你活一天.md "wikilink")》
  - 1972年《[四季發財](../Page/四季發財.md "wikilink")》 韓湘琴，唐威
  - 1972年 「晚秋」劉家昌、唐寶雲 文藝劇情片 　
  - 1973年《[初戀在台北](../Page/初戀在台北.md "wikilink")》 劉家昌 　
  - 1973年《[愛的天地](../Page/愛的天地.md "wikilink")》　 翁倩玉 　 劇情片
  - 1973年《[一家人](../Page/一家人.md "wikilink")》 歸亞蕾，唐寶雲，翁倩玉，湯蘭花 劇情片
  - 1973年《[雷風雨](../Page/雷風雨.md "wikilink")》
  - 1973年《[串串風鈴聲](../Page/串串風鈴聲.md "wikilink")》
  - 1973年《[雲飄飄](../Page/雲飄飄.md "wikilink")》 林青霞，唐寶雲，康家珍，谷名倫 愛情片
  - 1974年《[雪花片片](../Page/雪花片片.md "wikilink")》 秦祥林，胡錦，湯蘭花，張沖 劇情片
  - 1974年《[雲河](../Page/雲河.md "wikilink")》
  - 1974年《[十七十七十八](../Page/十七十七十八.md "wikilink")》
  - 1974年《[純純的愛](../Page/純純的愛.md "wikilink")》 林青霞，秦祥林 劇情片
  - 1975年《[梅花](../Page/梅花_\(電影\).md "wikilink")》 恬妞，張艾嘉，柯俊雄，谷名倫 劇情片
  - 1975年《[楓紅層層](../Page/楓紅層層.md "wikilink")》
  - 1975年《[小女兒的心愿](../Page/小女兒的心愿.md "wikilink")》 魏蘇，徐康泰，高怡文，劉秦雨 劇情片
  - 1975年《[少女的祈禱](../Page/少女的祈禱_\(電影\).md "wikilink")》 谷名倫，岳陽 劇情片
  - 1975年《[煙雨](../Page/煙雨.md "wikilink")》
  - 1976年《[田園](../Page/田園_\(電影\).md "wikilink")》
  - 1976年《[星語](../Page/星語.md "wikilink")》
  - 1976年「溫暖在秋天」張艾嘉、劉文正、谷名倫、胡茵夢、劉秦雨、徐康泰 文藝劇情片
  - 1977年《[黃埔軍魂](../Page/黃埔軍魂.md "wikilink")》 向華強，甄珍，胡茵夢，柯俊雄，谷名倫 劇情片
  - 1977年《[秋詩篇篇](../Page/秋詩篇篇.md "wikilink")》
  - 1977年《[日落北京城](../Page/日落北京城.md "wikilink")》
  - 1977年《[台北66](../Page/台北66.md "wikilink")》
  - 1978年《[白雲長在天](../Page/白雲長在天.md "wikilink")》
  - 1978年《[楓林小雨](../Page/楓林小雨.md "wikilink")》
  - 1981年《[背國旗的人](../Page/背國旗的人.md "wikilink")》
  - 1983年《[風水二十年](../Page/風水二十年.md "wikilink")》
  - 1984年《[聖戰千秋](../Page/聖戰千秋.md "wikilink")》
  - 1984年《[洪隊長](../Page/洪隊長.md "wikilink")》慕思成，方芳，石雋 紀錄片
  - 1984年《[江秋水](../Page/江秋水.md "wikilink")》
  - 1986年《[我是中國人](../Page/我是中國人.md "wikilink")》
  - 1994年《[梅珍](../Page/梅珍_\(電影\).md "wikilink")》

## 歌曲作品

  - 《一簾幽夢》
  - 《我心深處》
  - 《我找到自己》
  - 《楓林小雨》
  - 《[黃埔軍魂](../Page/黃埔軍魂.md "wikilink")》
  - 《秋詩篇篇》
  - 《日落北京城》
  - 《[我是中國人](../Page/我是中國人.md "wikilink")》
  - 《誓言》
  - 《往事只能回味》
  - 《[煙雨斜陽](../Page/煙雨斜陽.md "wikilink")》
  - 《[庭院深深](../Page/庭院深深.md "wikilink")》
  - 《小-{丑}-》
  - 《[美好的今天](../Page/美好的今天.md "wikilink")》（[中視早期開播曲](../Page/中視.md "wikilink")，由[蕭孋珠主唱](../Page/蕭孋珠.md "wikilink")）
  - 《[晚安曲](../Page/晚安曲.md "wikilink")》（中視早期收播曲，由[費玉清主唱](../Page/費玉清.md "wikilink")）
  - 《街燈下》
  - 《不一樣》
  - 《雲河》
  - 《秋歌》
  - 《海鷗》
  - 《梅花》
  - 《秋纏》
  - 《只要為你活一天》（2004年電影《[功夫](../Page/功夫_\(電影\).md "wikilink")》主題曲）
  - 《溫暖的秋天》
  - 《我家在那裡》
  - 《[中華民國頌](../Page/中華民國頌.md "wikilink")》
  - 《敲敲門》（[鳳飛飛早期成名曲](../Page/鳳飛飛.md "wikilink")）
  - 《[國家](../Page/國家.md "wikilink")》
  - 《民國六十六年在台北》
  - 《楓紅層層》
  - 《小女兒的心願》
  - 《諾言》
  - 《小雨打在我身上》
  - 《有真情有活力》
  - 《在雨中》
  - 《獨上西樓》
  - 《胭脂淚》
  - 《梅蘭梅蘭我愛你》
  - 《揚帆》
  - 《天真活潑又美麗》
  - 《[大中華](../Page/大中華_\(歌曲\).md "wikilink")》
  - [你我的家](../Page/你我的家.md "wikilink")
  - [不要你㸃頭](../Page/不要你㸃頭.md "wikilink")
  - 《連心》
  - 《[唸你](../Page/唸你.md "wikilink")》（親自為兒子[劉子千打造的歌曲](../Page/劉子千.md "wikilink")，在網路上引起極大的迴響，評價兩極化。）

## 獎項紀錄

### 金馬獎

|- |1973年 |愛的天地 |[第11屆金馬獎](../Page/第11屆金馬獎.md "wikilink") 優等劇情片 | |-
|1973年 |愛的天地 | [第11屆金馬獎](../Page/第11屆金馬獎.md "wikilink") 最佳電影原創音樂 | |-
|1976年 | [梅花](../Page/梅花_\(電影\).md "wikilink") |
[第13屆金馬獎](../Page/第13屆金馬獎.md "wikilink") 最佳影片 | |-
|1976年 | 梅花 | [第13屆金馬獎](../Page/第13屆金馬獎.md "wikilink") 最佳電影原創音樂 | |-
|1978年 |日落北京城 |[第15屆金馬獎](../Page/第15屆金馬獎.md "wikilink") 優等劇情片 | |-
|1978年 |[日落北京城](../Page/日落北京城.md "wikilink") |
[第15屆金馬獎](../Page/第15屆金馬獎.md "wikilink") 最佳劇本獎 | |-
|1979年 |[黃埔軍魂](../Page/黃埔軍魂.md "wikilink") |
[第16屆金馬獎](../Page/第16屆金馬獎.md "wikilink") 優等劇情片 | |-
|}

### 金曲獎

|- |2001年 | | [第12屆金曲獎](../Page/第12屆金曲獎.md "wikilink") 特別貢獻獎 | |- |}

## 注释

## 参考文献

## 外部連結

  -
  -
  -
  -
  - [的音樂世界](http://ent.sina.com.cn/y/2006-06-01/09561105328.html)

  - [中文電影資料庫─劉家昌](http://www.dianying.com/ft/person/LiuJiachang)

  - [2100全民開講─劉家昌、甄珍夫婦專訪](http://www.tudou.com/programs/view/pYeHe4B6dzs/)

  - [難忍劉子千遭罵畜生，甄珍怒揭劉家昌假面目](http://www.youtube.com/watch?feature=youtu.be&v=8XQJdhvw85Y/)

  - [劉家昌 - 財團法人國家電影中心 -
    台灣電影數位典藏中心](http://www.ctfa.org.tw/filmmaker/content.php?cid=2&id=559)

{{-}}

[劉家昌](../Category/劉家昌.md "wikilink")
[Category:金馬獎最佳電影音樂獲獎者](../Category/金馬獎最佳電影音樂獲獎者.md "wikilink")
[Category:臺灣劇情片導演](../Category/臺灣劇情片導演.md "wikilink")
[Category:台灣男歌手](../Category/台灣男歌手.md "wikilink")
[Category:台灣創作歌手](../Category/台灣創作歌手.md "wikilink")
[Category:華語歌手](../Category/華語歌手.md "wikilink")
[L](../Category/臺灣電影男演員.md "wikilink")
[Category:台灣作曲家](../Category/台灣作曲家.md "wikilink")
[Category:國立新竹高級中學校友](../Category/國立新竹高級中學校友.md "wikilink")
[Category:國立政治大學校友](../Category/國立政治大學校友.md "wikilink")
[Category:台灣戰後東北移民](../Category/台灣戰後東北移民.md "wikilink")
[Category:烟台人](../Category/烟台人.md "wikilink")
[Category:哈尔滨人](../Category/哈尔滨人.md "wikilink")
[J家昌](../Category/劉姓.md "wikilink")
[Category:金曲獎特別貢獻獎獲得者](../Category/金曲獎特別貢獻獎獲得者.md "wikilink")

1.  [谷名倫- 台灣Wiki](http://www.twwiki.com/wiki/谷名倫)

2.  <https://tw.appledaily.com/headline/daily/20090228/31429742>

3.  <http://hk.apple.nextmedia.com/entertainment/art/20110802/15484897>
    親唱《唸你》晒家傳唱腔 劉家昌憂子千：紅過頭

4.  [假面夫妻28年！甄珍自爆跟劉家昌早離婚，yam天空新聞，2015年05月21日](http://video.n.yam.com/20150521046020/%E5%81%87%E9%9D%A2%E5%A4%AB%E5%A6%BB28%E5%B9%B4%EF%BC%81%E3%80%80%E7%94%84%E7%8F%8D%E8%87%AA%E7%88%86%E8%B7%9F%E5%8A%89%E5%AE%B6%E6%98%8C%E6%97%A9%E9%9B%A2%E5%A9%9A)


5.  [甄珍臉書全文，2017年07月05日](https://www.facebook.com/misschiachen/photos/a.301172790333461.1073741827.301136443670429/315067778943962/?type=3)

6.  [劉家昌再控7宗罪 怒轟甄珍](https://stars.udn.com/star/story/10088/3003293)

7.

8.
9.

10.