**劉家輝**（****，）本名**冼錦熙**。1970年代末到1980年代間，曾是[邵氏電影公司力捧的](../Page/邵氏電影公司.md "wikilink")[功夫片男星](../Page/功夫片.md "wikilink")，也曾做過[導演](../Page/導演.md "wikilink")、[監製與](../Page/監製.md "wikilink")[武術指導](../Page/武術指導.md "wikilink")，也曾经是[無綫電視旗下的演員](../Page/無綫電視.md "wikilink")。

劉家輝是[劉湛的](../Page/劉湛_\(武術家\).md "wikilink")[乾兒子](../Page/乾兒子.md "wikilink")，與劉湛兩個[兒子](../Page/兒子.md "wikilink")，[劉家良](../Page/劉家良.md "wikilink")、[劉家榮並無](../Page/劉家榮.md "wikilink")[血緣關係](../Page/血緣關係.md "wikilink")。劉湛是[洪拳名家](../Page/洪拳.md "wikilink")[林世榮](../Page/林世榮.md "wikilink")（師承[黃飛鴻](../Page/黃飛鴻.md "wikilink")）的[弟子](../Page/弟子.md "wikilink")，劉家輝自小接受[劉家良嚴格的正統](../Page/劉家良.md "wikilink")[武術](../Page/武術.md "wikilink")[訓練](../Page/訓練.md "wikilink")，奠定後來成為武打明星的基礎。

## 簡歷

劉家輝1955年在[廣東出生](../Page/廣東.md "wikilink")，8歲時拜劉湛為師（但實際上大多是由劉家良教導），在其經營的[洪拳武館習武](../Page/洪拳.md "wikilink")；1971年中學畢業後，白天當[會計](../Page/會計.md "wikilink")，晚上則在劉湛的[武館教拳](../Page/武館.md "wikilink")。

1973年，劉家輝參與劉家良自資的電影《[殺出重圍](../Page/殺出重圍.md "wikilink")》而進入[電影圈](../Page/電影圈.md "wikilink")，之後在劉家良的介紹下進入在[張徹導演在台灣開設的](../Page/張徹.md "wikilink")[長弓電影公司工作](../Page/長弓電影公司.md "wikilink")，但由於當時張徹手上已經有[姜大衛](../Page/姜大衛.md "wikilink")、[狄龍等紅星](../Page/狄龍.md "wikilink")，所以他一直沒有機會演出重要的角色，1976年與長弓電影簽的三年約滿，回到香港，在由劉家良執導的《[陸阿-{采}-與黃飛鴻](../Page/陸阿采與黃飛鴻.md "wikilink")》中首次擔任主角，不過這沒有讓他獲得很大的成功，直到1978年的《[少林三十六房](../Page/少林三十六房.md "wikilink")》才真正讓他聲名大噪。這部電影大為成功，它不但是當年香港的十大賣座片，也在國外獲得不錯的迴響，甚至在[日本有人為他成立](../Page/日本.md "wikilink")[影友會](../Page/影友會.md "wikilink")。劉家輝在於片中飾演的[三德大師讓觀眾印象深刻](../Page/三德大師.md "wikilink")，**San
Te**（三德的英文音譯）與**Master Killer**（《少林三十六房》在英文片名）成為**Gordon
Liu**（劉家輝的英文名字）在某些[外國](../Page/外國.md "wikilink")[影迷心中的](../Page/影迷.md "wikilink")[代名詞](../Page/代名詞.md "wikilink")，也讓[光頭成為他的](../Page/光頭.md "wikilink")[註冊標記](../Page/註冊商標符號.md "wikilink")。後來在[邵氏拍的幾十部影片裡](../Page/邵氏.md "wikilink")，他幾乎都是飾演[武僧](../Page/武僧.md "wikilink")。

當[功夫片熱潮消退](../Page/功夫片.md "wikilink")、邵氏不再拍攝[電影之後](../Page/電影.md "wikilink")，劉家輝加入[無綫電視轉向](../Page/無綫電視.md "wikilink")[電視圏發展](../Page/電視圏.md "wikilink")。少了邵氏公司的幫助，他的[知名度不如以往](../Page/知名度.md "wikilink")，逐漸只能在在[電視劇或電影中扮演](../Page/電視劇.md "wikilink")[配角或者是](../Page/配角.md "wikilink")[反派](../Page/反派.md "wikilink")，直至90年代，劉家輝轉向演出[詼諧角色](../Page/詼諧角色.md "wikilink")。

2003年，劉家輝受名導演[-{zh-hans:昆廷·塔伦蒂诺;zh-hk:昆頓塔倫天奴;zh-tw:昆汀·塔倫提諾;}-之邀在](../Page/昆廷·塔伦蒂诺.md "wikilink")《[-{zh-hans:標殺令;zh-hk:標殺令;zh-tw:追殺比爾;}-](../Page/標殺令.md "wikilink")》电影系列中演出，分別在第一集中飾演瘋狂88的殺手[強尼莫](../Page/強尼莫.md "wikilink")，第二集中飾演[武術高人](../Page/武術高人.md "wikilink")[白眉](../Page/白眉.md "wikilink")。

2005年3月20日，在[廣州舉辦邵氏名片海報劇照展覽時](../Page/廣州.md "wikilink")，劉家輝收了第一個徒弟，知名女星[鄭佩佩之女](../Page/鄭佩佩.md "wikilink")[原子鏸](../Page/原子鏸.md "wikilink")，到8月22日生日時，又多收了13名徒弟。

由於劉家輝五官清秀，頗具男子氣概，成為不少少女們心目中的[偶像](../Page/偶像.md "wikilink")。更有甚者，將其[布娃娃取名為](../Page/布娃娃.md "wikilink")[蹼熊](../Page/蹼熊.md "wikilink")，也是喜好劉家輝的一種表現。

劉家輝曾於2009年初到台灣拜訪嶺南拳術委員會，以了解洪拳於台灣發展現況，他認為[國術是中國的國粹](../Page/國術.md "wikilink")，也是[中華民族的文化](../Page/中華民族.md "wikilink")，社會一天一天在進步，不可走回頭路，必須思考如何延續傳統國術存在的價值。而他也為[傳統武術做了最佳的詮釋](../Page/傳統武術.md "wikilink")：本身本人才能練就是傳統功夫。

2011年8月17日，劉家輝中風失衡跌倒，腦部撞傷致半身不遂，其演藝生涯需要暫停。劉家輝因中風住進養老院，好友[曾志偉探訪送他一部](../Page/曾志偉.md "wikilink")[iPad](../Page/iPad.md "wikilink")，教他上網看以前的作品及和朋友玩視訊。據港媒報導，他中風後妻子並未照顧，反倒教唆子女分父親家產，兒女更向他索取每月7000元家用，迫使他的傷殘津貼都要給他們，劉家輝因此事憔悴暴瘦。除了女助手每日探望，先後兩任妻子都不曾到院，與前妻所出的一對21歲及18歲的子女也未曾探望，及後跟前女助手又有錢銀官司令他更憔悴\[1\]。但得[樊亦敏義務協助](../Page/樊亦敏.md "wikilink")，成為其委託人打點一切。

## 音樂

劉家輝在[演戲之餘](../Page/演戲.md "wikilink")，最大興趣是[音樂](../Page/音樂.md "wikilink")，他在[小學六年級時就開始練](../Page/小學.md "wikilink")[吉他](../Page/吉他.md "wikilink")，並且表示最喜歡[披頭四的音樂](../Page/披頭四.md "wikilink")。2001年，他與一群志同道合的朋友組了一個[樂團](../Page/樂團.md "wikilink")，每星期固定練習；值得一提的是，當初《少林三十六房》在日本上映時，日方還特地讓劉家輝灌錄了一張日語專輯《熱風傳說》，這也是他唯一一張正式的[音樂專輯](../Page/音樂專輯.md "wikilink")。

## 事件

據2011年10月某期《壹週刊》報道，8月初劉家輝在土瓜灣夾BAND期間，跣倒撞傷頭，並停止了所有工作。後來媒體拍到的照片卻顯示劉家輝是中風。劉家輝接受手術後，右邊手腳行動不便及口齒不清，需要接受長期康復治療。劉家輝變得意志消沉，一度拒絕物理治療。後經朋友開解，才開始重新接受治療，並進行了中醫針灸，漸有改善。

2012年8月，劉家輝捲入妻兒分產被棄而不顧的傳聞。\[2\]\[3\]\[4\]

2014年4月，劉家輝成功向前女助手馮映華索回約120萬人民幣（約160萬港幣）。\[5\]

## 幕後作品

### 導演

  - [少林與武當](../Page/少林與武當.md "wikilink")（1981）
  - [殺出重圍](../Page/殺出重圍.md "wikilink")（1973）

### 監製

  - [妙探孖寶](../Page/妙探孖寶.md "wikilink")（1985）

### 武術指導

  - [一膽二力三功夫](../Page/一膽二力三功夫.md "wikilink")（1980）
  - [USA Ninja](../Page/USA_Ninja.md "wikilink")（1985）

## 演出作品

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

<table>
<tbody>
<tr class="odd">
<td><p><strong>首播</strong></p></td>
<td><p><strong>劇名</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>合作演員</strong></p></td>
</tr>
<tr class="even">
<td><p>1988年</p></td>
<td><p><a href="../Page/兵權_(電視劇).md" title="wikilink">兵權</a></p></td>
<td><p><a href="../Page/趙匡胤.md" title="wikilink">趙匡胤</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1990年</p></td>
<td><p><a href="../Page/劍魔獨孤求敗.md" title="wikilink">劍魔獨孤求敗</a></p></td>
<td><p>江震雄</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1991年</p></td>
<td><p><a href="../Page/日月神劍.md" title="wikilink">日月神劍</a></p></td>
<td><p>燕翩遷</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1992年</p></td>
<td><p><a href="../Page/捉妖奇兵.md" title="wikilink">捉妖奇兵</a></p></td>
<td><p>燕翩遷</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大地飛鷹.md" title="wikilink">大地飛鷹</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/水滸英雄傳.md" title="wikilink">水滸英雄傳</a></p></td>
<td><p>武松</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1993年</p></td>
<td><p><a href="../Page/射鵰英雄傳之九陰真經.md" title="wikilink">射鵰英雄傳之九陰真經</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/如來神掌再戰江湖.md" title="wikilink">如來神掌再戰江湖</a></p></td>
<td><p><a href="../Page/火雲邪神.md" title="wikilink">敖千山</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/精武五虎.md" title="wikilink">精武五虎</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1994年</p></td>
<td><p><a href="../Page/螳螂小子.md" title="wikilink">螳螂小子</a></p></td>
<td><p>姜震北</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1995年</p></td>
<td><p><a href="../Page/神鵰俠侶_(1995年電視劇).md" title="wikilink">神鵰俠侶</a></p></td>
<td><p><a href="../Page/金輪法王.md" title="wikilink">金輪法王</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/刑事偵緝檔案II.md" title="wikilink">刑事偵緝檔案II</a></p></td>
<td><p>程根（檔案四：豪門綁架）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996年</p></td>
<td><p><a href="../Page/900重案追兇.md" title="wikilink">900重案追兇</a></p></td>
<td><p>高輝</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大刺客.md" title="wikilink">大刺客之魚腸劍</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/五個醒覺的少年.md" title="wikilink">五個醒覺的少年</a></p></td>
<td><p>呂立剛</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/闔府統請.md" title="wikilink">闔府統請</a></strong></p></td>
<td><p><strong>羅四季</strong></p></td>
<td><p><strong><a href="../Page/陳秀珠.md" title="wikilink">陳秀珠</a></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/西遊記_(1996年電視劇).md" title="wikilink">西遊記</a></strong></p></td>
<td><p><strong><a href="../Page/牛魔王.md" title="wikilink">牛魔王</a></strong></p></td>
<td><p><strong><a href="../Page/陳秀珠.md" title="wikilink">陳秀珠</a></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1997年</p></td>
<td><p><a href="../Page/醉打金枝_(無綫電視劇).md" title="wikilink">醉打金枝</a></p></td>
<td><p>八王爺</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/壹號皇庭V.md" title="wikilink">壹號皇庭V</a></p></td>
<td><p>歐國堅</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/苗翠花_(電視劇).md" title="wikilink">苗翠花</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/男人四十打功夫.md" title="wikilink">男人四十打功夫</a></p></td>
<td><p>聶萬鈞</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/香港人在廣州.md" title="wikilink">香港人在廣州</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1998年</p></td>
<td><p><a href="../Page/聊齋_(貳).md" title="wikilink">聊齋 (貳)</a></p></td>
<td><p>高漸耳</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/西遊記(貳).md" title="wikilink">西遊記(貳)</a></strong></p></td>
<td><p><strong><a href="../Page/牛魔王.md" title="wikilink">牛魔王</a></strong>／金角大王</p></td>
<td><p><strong><a href="../Page/陳秀珠.md" title="wikilink">陳秀珠</a></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td><p><a href="../Page/雪山飛狐_(1999年電視劇).md" title="wikilink">雪山飛狐</a></p></td>
<td><p><a href="../Page/石萬嗔.md" title="wikilink">石萬嗔</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/刑事偵緝檔案IV.md" title="wikilink">刑事偵緝檔案IV</a></p></td>
<td><p>洛鎮彪（檔案十三：愛的代價）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/千里姻緣兜錯圈.md" title="wikilink">千里姻緣兜錯圈</a></p></td>
<td><p>謝蝦</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/狀王宋世傑(貳).md" title="wikilink">狀王宋世傑(貳)</a></p></td>
<td><p>江總兵</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/騙中傳奇.md" title="wikilink">騙中傳奇</a></p></td>
<td><p><a href="../Page/和珅.md" title="wikilink">和珅</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/人龍傳說.md" title="wikilink">人龍傳說</a></p></td>
<td><p>龍王（第1、5-7、11-18、20集）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/洗冤錄.md" title="wikilink">洗冤錄</a></p></td>
<td><p>聶人龍</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td><p><a href="../Page/金裝四大才子.md" title="wikilink">金裝四大才子</a></p></td>
<td><p>李龍</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/京城教一.md" title="wikilink">京城教一</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/雷霆第一關.md" title="wikilink">雷霆第一關</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/FM701.md" title="wikilink">FM701</a></p></td>
<td><p>林鑫淼</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/倚天屠龍記_(2001年電視劇).md" title="wikilink">倚天屠龍記</a></p></td>
<td><p><a href="../Page/成崑.md" title="wikilink">成崑</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陀槍師姐II.md" title="wikilink">陀槍師姐II</a></p></td>
<td><p>蒲勇</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><a href="../Page/美味情緣.md" title="wikilink">美味情緣</a></p></td>
<td><p>封三刀</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/尋秦記_(電視劇).md" title="wikilink">尋秦記</a></p></td>
<td><p>曹秋道（第8-9、12、32、37集）、神秘人（第19集）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p><a href="../Page/情事緝私檔案.md" title="wikilink">情事緝私檔案</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/烈火雄心II.md" title="wikilink">烈火雄心II</a></p></td>
<td><p>鍾廣成<br />
（鍾茵怡之父）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/轉世驚情.md" title="wikilink">轉世驚情</a></p></td>
<td><p>畢仕仁（於2006年播映）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p><a href="../Page/九五至尊.md" title="wikilink">九五至尊</a></p></td>
<td><p>向陽</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/帝女花_(無綫電視劇).md" title="wikilink">帝女花</a></p></td>
<td><p>田弘遇</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金牌冰人.md" title="wikilink">金牌冰人</a></p></td>
<td><p>屈仁</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/英雄·刀·少年.md" title="wikilink">英雄·刀·少年</a></p></td>
<td><p>林軒源</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/俗世情真.md" title="wikilink">俗世情真</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p><a href="../Page/怪俠一枝梅_(無綫電視劇集).md" title="wikilink">怪俠一枝梅</a></p></td>
<td><p>大師</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陀槍師姐IV.md" title="wikilink">陀槍師姐IV</a></p></td>
<td><p>任重遠</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/追魂交易.md" title="wikilink">追魂交易</a></p></td>
<td><p>湯錦輝</p></td>
<td><p><a href="../Page/陳秀珠.md" title="wikilink">陳秀珠</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大唐雙龍傳_(電視劇).md" title="wikilink">大唐雙龍傳</a></p></td>
<td><p>李孝常</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/水滸無間道.md" title="wikilink">水滸無間道</a></p></td>
<td><p>黃錦仁</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
<td><p><a href="../Page/我師傅係黃飛鴻.md" title="wikilink">我師傅係黃飛鴻</a></p></td>
<td><p>莫天龍</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西廂奇緣.md" title="wikilink">西廂奇緣</a></p></td>
<td><p>天竺</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/御用閒人.md" title="wikilink">御用閒人</a></p></td>
<td><p>年茂林</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/情迷黑森林.md" title="wikilink">情迷黑森林</a></p></td>
<td><p>唐森</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/我的野蠻奶奶.md" title="wikilink">我的野蠻奶奶</a></p></td>
<td><p>田滿<br />
（田力之父）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/佛山贊師父.md" title="wikilink">佛山贊師父</a></p></td>
<td><p>連勇</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/窈窕熟女.md" title="wikilink">窈窕熟女</a></p></td>
<td><p>關師父</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006</p></td>
<td><p><a href="../Page/施公奇案_(無綫電視劇).md" title="wikilink">施公奇案</a></p></td>
<td><p>司馬追風</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/覆雨翻雲_(電視劇).md" title="wikilink">覆雨翻雲</a></p></td>
<td><p><a href="../Page/厲若海.md" title="wikilink">厲若海</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p><a href="../Page/迎妻接福.md" title="wikilink">迎妻接福</a></p></td>
<td><p>曾大力</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/通天幹探.md" title="wikilink">通天幹探</a></p></td>
<td><p>黎振祥<br />
（黎家派詠春拳之掌門人）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/學警出更.md" title="wikilink">學警出更</a></p></td>
<td><p>袁滿</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008年</p></td>
<td><p><a href="../Page/千謊百計.md" title="wikilink">千謊百計</a></p></td>
<td><p>孫虎成<br />
（<a href="../Page/無綫電視.md" title="wikilink">無綫電視收費劇集台</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/少年四大名捕_(無綫電視劇).md" title="wikilink">少年四大名捕</a></p></td>
<td><p>藍破天</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p><a href="../Page/幕後大老爺.md" title="wikilink">幕後大老爺</a></p></td>
<td><p>戴有恭</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p><strong><a href="../Page/施公奇案II.md" title="wikilink">施公奇案II</a></strong></p></td>
<td><p><strong>司馬追風</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/女人最痛.md" title="wikilink">女人最痛</a></p></td>
<td><p>吳立秋</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/誘情轉駁.md" title="wikilink">誘情轉駁</a></p></td>
<td><p>林中豹</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011年</p></td>
<td><p><a href="../Page/洪武三十二.md" title="wikilink">洪武三十二</a></p></td>
<td><p>嚴進</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/萬凰之王.md" title="wikilink">萬凰之王</a></strong></p></td>
<td><p><strong>佟佳·舒明阿</strong></p></td>
<td><p><strong><a href="../Page/陳秀珠.md" title="wikilink">陳秀珠</a></strong></p></td>
<td></td>
</tr>
</tbody>
</table>

### 無綫綜藝節目

|        |                                          |                                       |
| ------ | ---------------------------------------- | ------------------------------------- |
| **年份** | **節目**                                   | 參演人員                                  |
| 2008   | [鐵甲無敵獎門人](../Page/鐵甲無敵獎門人.md "wikilink") | 第35集、[陳秀珠](../Page/陳秀珠.md "wikilink") |
| 2010   | [超級遊戲獎門人](../Page/超級遊戲獎門人.md "wikilink") | 第13集、[陳秀珠](../Page/陳秀珠.md "wikilink") |

### 電影

#### 1970年代

  - [殺出重圍](../Page/殺出重圍.md "wikilink")（1973）
  - [洪拳與詠春](../Page/洪拳與詠春.md "wikilink")（1974） 飾 何振剛
  - [少林五祖](../Page/少林五祖.md "wikilink")（1974）
  - [馬哥波羅](../Page/馬哥波羅_\(電影\).md "wikilink")（1975）
  - [陸阿采與黃飛鴻](../Page/陸阿采與黃飛鴻.md "wikilink")（1976）飾 黃飛鴻
  - [八道樓子](../Page/八道樓子.md "wikilink")（1976）
  - [八國聯軍](../Page/八國聯軍_\(電影\).md "wikilink")（1976）
  - [洪熙官](../Page/洪熙官_\(電影\).md "wikilink")（1977）飾 童千斤
  - [功夫小子](../Page/功夫小子.md "wikilink")（1977）
  - [螳螂](../Page/螳螂_\(電影\).md "wikilink")（1978）
  - [少林三十六房](../Page/少林三十六房.md "wikilink")（1978）飾 劉裕德／三德
  - [中華丈夫](../Page/中華丈夫.md "wikilink")（1979）飾 何濤
  - [茅山殭屍拳](../Page/茅山殭屍拳.md "wikilink")（1979）
  - [爛頭何](../Page/爛頭何.md "wikilink")（1979）飾 勤親王
  - [少林真英雄](../Page/少林真英雄.md "wikilink")（1979）

#### 1980年代

  - [一膽二力三功夫](../Page/一膽二力三功夫.md "wikilink")（1980）
  - [少林搭棚大師](../Page/少林搭棚大師.md "wikilink")（1980）飾 周仁傑
  - [洪文定三破白蓮教](../Page/洪文定三破白蓮教.md "wikilink")（1980）飾 洪文定
  - [武館](../Page/武館_\(電影\).md "wikilink")（1981）飾 黃飛鴻
  - [少林與武當](../Page/少林與武當.md "wikilink")（1981）
  - [長輩](../Page/長輩.md "wikilink")（1981）
  - [御貓三戲錦毛鼠](../Page/御貓三戲錦毛鼠.md "wikilink")（1982）
  - [少年蘇乞兒](../Page/少年蘇乞兒.md "wikilink")（1982）
  - [龍虎少爺](../Page/龍虎少爺.md "wikilink")（1982）
  - [十八般武藝](../Page/十八般武藝.md "wikilink")（1982）
  - [八十二家房客](../Page/八十二家房客.md "wikilink")（1982）飾 的士輝
  - [搏盡](../Page/搏盡.md "wikilink")（又名《黃埔灘頭》）（1982）
  - [少林醉八拳](../Page/少林醉八拳.md "wikilink")（1982）
  - [大兄出道](../Page/大兄出道.md "wikilink")（1982）
  - [五郎八卦棍](../Page/五郎八卦棍.md "wikilink")（1983）飾 楊五郎
  - [少林小子](../Page/少林小子.md "wikilink")（1983）
  - [鹿鼎記](../Page/鹿鼎記_\(1983年電影\).md "wikilink")（1983）飾 康熙
  - [弟子也瘋狂](../Page/弟子也瘋狂.md "wikilink")（1983）
  - [掌門人](../Page/掌門人_\(電影\).md "wikilink")（1983）
  - [破戒大師](../Page/破戒大師.md "wikilink")（1984）
  - [有牌流氓](../Page/有牌流氓.md "wikilink") (1985)
  - [霹靂十傑](../Page/霹靂十傑.md "wikilink")（1985）飾 三德
  - [弟子也瘋狂](../Page/弟子也瘋狂.md "wikilink")（1985）
  - [妙探孖寶](../Page/妙探孖寶.md "wikilink")（1985）
  - [殺手蝴蝶夢](../Page/殺手蝴蝶夢.md "wikilink")（1987）
  - [國父孫中山與開國英雄](../Page/國父孫中山與開國英雄.md "wikilink")（又名：國父傳）The Story of
    Dr Sun Yat Sen（1987）飾 史堅如
  - [老虎出更](../Page/老虎出更.md "wikilink")（1988）
  - [孔雀王](../Page/孔雀王.md "wikilink")（1988）
  - [猛鬼舞廳](../Page/猛鬼舞廳.md "wikilink")（1989）
  - [猛龍行動](../Page/猛龍行動.md "wikilink")（1989）
  - [富貴兵團](../Page/富貴兵團.md "wikilink")（1989）
  - [龍虎家族](../Page/龍虎家族.md "wikilink")（1989）
  - [猛虎發火](../Page/猛虎發火.md "wikilink")（1989）

#### 1990年代

  - [列血風雲](../Page/列血風雲.md "wikilink")（1990）
  - [老虎出更二](../Page/老虎出更二.md "wikilink")（1990）飾 輝少
  - [怒火威龍](../Page/怒火威龍.md "wikilink")（1991）
  - [雲雨第六感](../Page/雲雨第六感.md "wikilink")（1992）
  - [走佬威龍](../Page/走佬威龍.md "wikilink")（1993）
  - [笑俠楚留香](../Page/笑俠楚留香.md "wikilink")（1993）
  - [黃飛鴻：鐵雞鬥蜈蚣](../Page/黃飛鴻：鐵雞鬥蜈蚣.md "wikilink")（1993）
  - [唐伯虎點秋香](../Page/唐伯虎點秋香.md "wikilink")（1993）飾 奪命書生
  - [倫文敘老點柳先開](../Page/倫文敘老點柳先開.md "wikilink")（1993）
  - [獵豹行動](../Page/獵豹行動.md "wikilink")（1993）
  - [少林活寶貝](../Page/少林活寶貝.md "wikilink")（1994）
  - [花旗少林](../Page/花旗少林.md "wikilink")（1994）飾 空智方丈
  - [醉拳三](../Page/醉拳三.md "wikilink")（1994）
  - [Lethal Girls 2](../Page/Lethal_Girls_2.md "wikilink")（1995）
  - [有時跳舞](../Page/有時跳舞.md "wikilink")（1999）
  - [一代梟雄：曹操](../Page/一代梟雄：曹操.md "wikilink")（1999）飾
    [周瑜](../Page/周瑜.md "wikilink")

#### 2000年代

  - [有時跳舞](../Page/有時跳舞.md "wikilink")（2000）
  - [醉馬騮](../Page/醉馬騮.md "wikilink")（台灣稱《醉猴》）（2002）飾 洪一虎
  - [-{zh-hans:杀死比尔;zh-hk:標殺令;zh-tw:追殺比爾;}-](../Page/追殺比爾.md "wikilink")（2003）飾
    強尼莫
  - [少年阿虎](../Page/少年阿虎.md "wikilink")（2003）
  - [-{zh-hans:杀死比尔2;zh-hk:標殺令2;zh-tw:追殺比爾2;}-](../Page/追殺比爾2：愛的大逃殺.md "wikilink")（2004）飾
    白眉
  - [野蠻秘笈](../Page/野蠻秘笈.md "wikilink")（2004）
  - [霍元甲之精武真英雄](../Page/霍元甲之精武真英雄.md "wikilink")（2004）飾
    [霍元甲](../Page/霍元甲.md "wikilink")
  - [猛龍](../Page/猛龍_\(電影\).md "wikilink")（2005）
  - [蛇咒](../Page/蛇咒.md "wikilink")（2005）
  - [情顛大聖](../Page/情顛大聖.md "wikilink")（2005）飾 玉帝
  - [少林殭屍](../Page/少林殭屍.md "wikilink")（2005）飾 白道長
  - [少林殭屍](../Page/少林殭屍.md "wikilink")2-[天極](../Page/天極.md "wikilink")（2006）飾
    白道長（周遊）
  - [野蠻秘笈](../Page/野蠻秘笈.md "wikilink")（2006）
  - [三分鐘先生](../Page/三分鐘先生.md "wikilink")（2006）飾 光頭叔叔
  - [從印度到中國](../Page/從印度到中國.md "wikilink")（2009）飾 Hojo
  - [蘇乞兒](../Page/蘇乞兒.md "wikilink")（2010）飾 白鬚翁
  - [星光18借](../Page/星光18借.md "wikilink")（2010）飾 劉武
  - [龍門飛甲](../Page/龍門飛甲.md "wikilink")（2011）飾 萬喻樓
  - [大追捕](../Page/大追捕.md "wikilink")（2012）
  - [畫皮II](../Page/畫皮II.md "wikilink")（2012）
  - [刺客攻略](../Page/刺客攻略.md "wikilink") （2012） 飾 和尚 Monk
  - 戰牆（2016）(網絡電影)

### 電視劇（[香港電台](../Page/香港電台.md "wikilink")）

  - [有房出租](../Page/有房出租_\(香港電台劇集\).md "wikilink")（2009）飾 吳奚
  - [火速救兵](../Page/火速救兵.md "wikilink") - 勇士首部曲（2010）飾 連永堅
  - [火速救兵II](../Page/火速救兵II.md "wikilink")（2012）

### 電視劇（台灣[華視](../Page/華視.md "wikilink")）

  - [飛象過河](../Page/飛象過河.md "wikilink")
  - [少林弟子](../Page/少林弟子.md "wikilink")
  - [新西螺七劍](../Page/新西螺七劍.md "wikilink")（1985年）飾 洪善（阿善師）

### 電視劇（其他）

  - 《[千秋家園夢](../Page/千秋家園夢.md "wikilink")》
  - 《[中華大丈夫](../Page/中華大丈夫.md "wikilink") 》飾
    [容百川](../Page/容百川.md "wikilink")
  - 2008年《[仙剑奇侠传三](../Page/仙剑奇侠传三_\(电视剧\).md "wikilink")》飾 邪剑仙
  - 《[聊斋2](../Page/聊斋_\(2007年电视剧\).md "wikilink")》之婴宁 饰 五行尊者
  - 《[黄飞鸿与十三姨](../Page/黄飞鸿与十三姨.md "wikilink")》 饰 黄飞鸿

### 電視電影（[無綫電視](../Page/無綫電視.md "wikilink")）

  - [劍客與靈童](../Page/劍客與靈童.md "wikilink")（1989）
  - [孽海狂花](../Page/孽海狂花.md "wikilink")

### 廣告

  - [定期檢修大廈供氣喉](../Page/定期檢修大廈供氣喉.md "wikilink")　機電工程署宣傳短片(2010)

## 參考文獻

<references/>

## 外部連結

  -
  -
  -
  - [政府宣傳短片：定期檢收大廈供氣喉](http://www.isd.gov.hk/chi/tvapi/07_eg55.html)

[category:香港武打演員](../Page/category:香港武打演員.md "wikilink")

[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")
[Category:動作片演員](../Category/動作片演員.md "wikilink")
[Category:香港武術家](../Category/香港武術家.md "wikilink")
[Category:广东人](../Category/广东人.md "wikilink")
[Chia](../Category/冼姓.md "wikilink")

1.
2.  [劉家輝中風跌倒 半身不遂](http://www.ihktv.com/gordon-liu-stroke-falls.html)
3.  [港打星劉家輝 中風癱瘓
    慘遭妻棄](http://news.wenweipo.com/2012/08/19/IN1208190066.htm)
4.  [邵音音撐劉家輝父慈子孝](https://www.youtube.com/watch?v=2XsjkVSQpWU)
5.