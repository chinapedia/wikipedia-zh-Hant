**劉岱**（），字**公山**，[東漢](../Page/東漢.md "wikilink")[東萊郡](../Page/東萊郡.md "wikilink")[牟平縣](../Page/牟平縣.md "wikilink")（今[山東省](../Page/山東省.md "wikilink")[煙台市](../Page/煙台市.md "wikilink")[福山區西北](../Page/福山區.md "wikilink")）人。他是[西漢齊孝王](../Page/西漢.md "wikilink")[劉將閭之子牟平共侯](../Page/劉將閭.md "wikilink")[刘渫的后代](../Page/刘渫.md "wikilink")\[1\]，[山陽](../Page/山陽郡.md "wikilink")[太守](../Page/太守.md "wikilink")[劉輿](../Page/劉輿.md "wikilink")（又名[劉方](../Page/劉方.md "wikilink")）的兒子，[揚州牧](../Page/揚州.md "wikilink")[劉繇之兄](../Page/劉繇.md "wikilink")。

## 生平

劉岱的伯父[劉寵曾經擔任過](../Page/刘宠_\(东莱\).md "wikilink")[東漢的](../Page/東漢.md "wikilink")[太尉](../Page/太尉.md "wikilink")，劉岱本人則在漢朝歷任[侍中](../Page/侍中.md "wikilink")、侍御史、[兗州](../Page/兗州.md "wikilink")[刺史](../Page/刺史.md "wikilink")。

中平二年（185年），中常侍[张让](../Page/张让.md "wikilink")、[赵忠说服](../Page/赵忠.md "wikilink")[汉灵帝收天下田花大钱修宫殿](../Page/汉灵帝.md "wikilink")，铸就铜人，樂安太守[陆康上表劝阻](../Page/陆康.md "wikilink")，激怒灵帝。灵帝欲治陆康大不敬之罪，辛亏当时担任侍御史的劉岱上表解释才使得陆康逃过杀身之祸。后董卓掌政，听从[伍琼](../Page/伍琼.md "wikilink")、[周毖之言以刘岱为兖州刺史](../Page/周毖.md "wikilink")。

[初平元年](../Page/初平.md "wikilink")（190年）春正月，劉岱依從[袁紹](../Page/袁紹.md "wikilink")、[曹操起兵反對](../Page/曹操.md "wikilink")[董卓](../Page/董卓.md "wikilink")，加入[討伐董卓之戰](../Page/討伐董卓之戰.md "wikilink")。後來劉岱與[東郡](../Page/東郡.md "wikilink")[太守](../Page/太守.md "wikilink")[橋瑁不和](../Page/橋瑁.md "wikilink")，最終劉岱殺了橋瑁，改任王肱為東郡太守。

劉岱又與袁紹、[公孫瓚和親](../Page/公孫瓚.md "wikilink")，袁紹讓自己的家眷居住在劉岱的居所，公孫瓚也派部下的[從事](../Page/從事.md "wikilink")[范方率軍協助劉岱](../Page/范方.md "wikilink")。此後袁紹與公孫瓚有了矛盾，公孫瓚在擊破袁紹軍後便讓劉岱與袁紹斷絕來往，又對范方說：「假如劉岱不把袁紹的家眷趕走的話，你就率軍回來。等我滅了袁紹，就要攻打劉岱了。」劉岱為此連日不能決斷，他的[別駕](../Page/別駕.md "wikilink")[王彧告訴劉岱不如去詢問](../Page/王彧.md "wikilink")[程昱](../Page/程昱.md "wikilink")。於是劉岱召見了程昱，向他詢問計策，程昱勸說劉岱不如幫助袁紹，劉岱聽從了他的計策，果然不久之後公孫瓚就被袁紹打敗了。

初平三年（192年），青州的百萬黃巾黨入侵了兗州，轉入[東平縣](../Page/東平縣.md "wikilink")。劉岱想要攻打黃巾黨，[鮑信卻勸他最好固守](../Page/鮑信.md "wikilink")。劉岱不聽鮑信的勸告，與黃巾黨交戰，兵敗被殺。

## 評價

陶丘洪：「若明使君用公山于前，擢正礼于后，所谓御二龙于长涂，骋骐骥于千里，不亦可乎?」

《续汉书》：「岱、繇皆有隽才。」

《英雄记》：「岱孝悌仁恕，以虚己受人。」

## 參考資料

  - 《[三國志](../Page/三國志.md "wikilink")·魏書·武帝紀》
  - 《三國志·吳書·劉繇傳》註引《續漢書》
  - 《[三國志·魏志十四·程昱傳](../Page/s:三國志/卷14#程昱.md "wikilink")》

[D岱](../Page/category:劉姓.md "wikilink")

[L劉](../Category/漢朝宗室.md "wikilink")
[L劉](../Category/東漢政治人物.md "wikilink")
[Category:三國戰爭身亡者](../Category/三國戰爭身亡者.md "wikilink")

1.  《后汉书·卷七十六·循吏列传第六十六》：刘宠字祖荣，东莱牟平人，齐悼惠王之后也。悼惠王子孝王将闾，将闾少子封牟平侯，子孙家焉。