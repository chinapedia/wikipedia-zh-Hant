**劉必榮**（），生於[台北](../Page/台北.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")[政治學](../Page/政治學.md "wikilink")[學者](../Page/學者.md "wikilink")，[東吳大學政治學系](../Page/東吳大學.md "wikilink")[教授](../Page/教授.md "wikilink")，學術專長為[國際政治](../Page/國際政治.md "wikilink")、[國際衝突](../Page/國際衝突.md "wikilink")、[談判理論](../Page/談判理論.md "wikilink")。

## 生平

1979年劉必榮在[國立政治大學外交學系以第一名的成績畢業](../Page/國立政治大學.md "wikilink")，後又於[中華民國教育部公費考試再度以第一名的成績取得資格](../Page/中華民國教育部.md "wikilink")[留學](../Page/留學.md "wikilink")[美國](../Page/美國.md "wikilink")，獲得[約翰霍普金斯大學](../Page/約翰霍普金斯大學.md "wikilink")[國際關係](../Page/國際關係.md "wikilink")[碩士與](../Page/碩士.md "wikilink")[維吉尼亞大學國際關係](../Page/維吉尼亞大學.md "wikilink")[博士學位](../Page/博士.md "wikilink")。於1986年畢業後，返國任教於[東吳大學政治學系迄今](../Page/東吳大學.md "wikilink")。

劉必榮除國際政治、[外交政策與](../Page/外交政策.md "wikilink")[東南亞研究等課程外](../Page/東南亞.md "wikilink")，專研談判理論、[孫子兵法與衝突解決](../Page/孫子兵法.md "wikilink")，致力於談判觀念與藝術之推廣，長期為[中華民國外交部](../Page/中華民國外交部.md "wikilink")、[中華民國經濟部](../Page/中華民國經濟部.md "wikilink")、[中華民國外貿協會及國內外民間機構講授談判課程](../Page/中華民國外貿協會.md "wikilink")。劉必榮曾任[中國時報總主筆](../Page/中國時報.md "wikilink")、[行政院顧問](../Page/行政院.md "wikilink")，及[公共電視文化事業基金會](../Page/公共電視文化事業基金會.md "wikilink")、[年代新聞台](../Page/年代新聞台.md "wikilink")、[大愛電視等之國際新聞節目之](../Page/大愛電視.md "wikilink")[製作人](../Page/製作人.md "wikilink")、解析員、[主播及](../Page/主播.md "wikilink")[主持人](../Page/主持人.md "wikilink")；現並為[台北市談判研究發展協會理事長與](../Page/台北市談判研究發展協會.md "wikilink")[和風談判學院主持人](../Page/和風談判學院.md "wikilink")。

## 著作

  - [時報文化](../Page/時報文化.md "wikilink")

<!-- end list -->

  - 《談判》，1989年

<!-- end list -->

  - 商周出版

<!-- end list -->

  - 《談判Q\&A》，1992年
  - 《談判聖經》，1996年5月

<!-- end list -->

  - [希代書版集團](../Page/希代書版集團.md "wikilink")

<!-- end list -->

  - 《雄辯天下》，1996年
  - 《劉必榮談談判藝術》（原名《生活談判》），2000年

<!-- end list -->

  - [先覺出版社](../Page/先覺出版社.md "wikilink")

<!-- end list -->

  - 《三頂帽子哲學：劉必榮的談判人生》，2002年，ISBN 9576078202
  - 《談判，無所不在：你不能不懂的協商智慧》，2003年，ISBN 9576079128
  - 《談判兵法：孫子兵學的謀略智慧》，2004年，ISBN 9861340130
  - 《新世紀談判全攻略》，2007年，ISBN 9861340726
  - 《國際觀的第一本書：看世界的方法》，2008年，ISBN 9789861341163
  - 《商務談判高級兵法》，2008年，ISBN 9787301136201
  - 《世界真的變了！10個你必須知道的未來》，2009年，ISBN 9789861341446
  - 《世界地圖就是你的財富版圖：掌握國際觀，獲利更可觀》(與林志昊合著)，2011年，ISBN 9789861341705

<!-- end list -->

  - [清涼音文化](../Page/清涼音文化.md "wikilink")

<!-- end list -->

  - 《上班族的談判技巧》（[有聲書](../Page/有聲書.md "wikilink")），2002年
  - 《大戰略：談判孫子兵法》（[有聲書](../Page/有聲書.md "wikilink")），2002年
  - 《生活談判》（[有聲書](../Page/有聲書.md "wikilink")），2002年
  - 《中國人、韓國人、日本人的談判謀略與性格》（[有聲書](../Page/有聲書.md "wikilink")），2002年
  - 《從大長今與明成皇后－看韓國人的談判謀略與性格》([有聲書](../Page/有聲書.md "wikilink"))，2008年
  - 《從織田信長與德川家康－看日本人的談判謀略與性格》([有聲書](../Page/有聲書.md "wikilink"))，2008年
  - 《從李鴻章與周恩來－看中國人的談判謀略與性格》([有聲書](../Page/有聲書.md "wikilink"))，2008年
  - 《孫子兵法－從談判角度活用孫子兵法》([有聲書](../Page/有聲書.md "wikilink"))，2010年
  - 《看山、看海、讀詩、論劍－談判的藝術與禪意》([有聲書](../Page/有聲書.md "wikilink"))，2010年
  - 《明朝劉伯溫－郁離子裡的管理智慧》([有聲書](../Page/有聲書.md "wikilink"))，2014年
  - 《日本宮本武藏－五輪書裡的兵法世界》([有聲書](../Page/有聲書.md "wikilink"))，2014年
  - 《清朝恭親王－大清王爺的內外世界》([有聲書](../Page/有聲書.md "wikilink"))，2014年
  - 《元朝成吉思汗－帝國崛起的大謀略》([有聲書](../Page/有聲書.md "wikilink"))，2014年

<!-- end list -->

  - [北京大學出版社](../Page/北京大學出版社.md "wikilink")

<!-- end list -->

  - 《攻守之道：談判高手的100個錦囊》，2006年，ISBN 7301102054
  - 《達成交易的完美談判》，2007年，ISBN 9787301127681
  - 《辦公室裡的溝通談判術》，2008年，ISBN 9787301132951
  - 《中國式商務談判》，2011年，ISBN 9787301157008

<!-- end list -->

  - [文經社](../Page/文經社.md "wikilink")

<!-- end list -->

  - 《學會談判：從兩敗到雙贏的溝通模式》，2006年，ISBN 9789576634666
  - 《學會溝通：創造雙贏的協調技巧》，2011年，文經社，ISBN 9789576636387

<!-- end list -->

  - [國語日報社](../Page/國語日報.md "wikilink")

<!-- end list -->

  - 《孩子的第一個國際觀學校》，2012年

## 主持

  - 公共電視文化事業基金會《公視新聞深度報導》國際新聞分析員（1999年12月6日—已停播）
  - 公共電視文化事業基金會《[七點看世界](../Page/七點看世界.md "wikilink")》主播兼解析員（已停播）
  - 公共電視文化事業基金會《[全球現場](../Page/全球現場.md "wikilink")》第一任主播兼解析員（2002年—2003年2月）
  - 年代新聞台《[年代看世界](../Page/年代看世界.md "wikilink")》解析員
  - 大愛電視《大愛寰宇新聞》製作人兼主持人

## 電台訪談

常是臺灣、新加坡兩地廣播電台的特約電話訪問對象。

## 外部連結

  - [劉必榮教授 Facebook粉絲專頁](http://www.facebook.com/LiuBiRong)
  - [東吳大學政治系-師資介紹](http://www2.scu.edu.tw/politics/default.asp?Subject=Teacher)
  - [和風談判學院](https://web.archive.org/web/20071023063311/http://www.negotiation.com.tw/ezpor/front/bin/home.phtml)
  - [劉必榮無名小站部落格](http://www.wretch.cc/blog/Birong)
  - [劉必榮鳳凰博報部落格](https://web.archive.org/web/20090420193612/http://blog.ifeng.com/1325388.html)
  - [《大眾時代》劉必榮文集](http://mass-age.com/wpmu/blog/tag/%E5%8A%89%E5%BF%85%E6%A6%AE/)
  - [Fathom Training](http://www.fathomtraining.com)

[Category:台灣政治學家](../Category/台灣政治學家.md "wikilink")
[Category:台灣新聞節目主持人](../Category/台灣新聞節目主持人.md "wikilink")
[Category:東吳大學教授](../Category/東吳大學教授.md "wikilink")
[Category:維吉尼亞大學校友](../Category/維吉尼亞大學校友.md "wikilink")
[Category:約翰·霍普金斯大學校友](../Category/約翰·霍普金斯大學校友.md "wikilink")
[Category:國立政治大學校友](../Category/國立政治大學校友.md "wikilink")
[Category:福州裔台灣人](../Category/福州裔台灣人.md "wikilink")
[Category:台北市人](../Category/台北市人.md "wikilink")
[Bi必](../Category/劉姓.md "wikilink")
[Category:臺北市私立再興高級中學校友](../Category/臺北市私立再興高級中學校友.md "wikilink")