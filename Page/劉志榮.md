**劉志榮**（英文名：**David Lau Chi
Wing**，），祖籍[廣西](../Page/廣西.md "wikilink")[玉林](../Page/玉林.md "wikilink")，是[華南電影及](../Page/華南.md "wikilink")[粵劇著名演員](../Page/粵劇.md "wikilink")[劉克宣之子](../Page/劉克宣.md "wikilink")。劉志榮子承父業，於1973年加入[麗的映聲後經歷](../Page/麗的映聲.md "wikilink")[麗的電視及](../Page/麗的電視.md "wikilink")[亞洲電視](../Page/亞洲電視.md "wikilink")，並成為其「鎮台之寶」至1997年才離開[亞洲電視](../Page/亞洲電視.md "wikilink")；後來轉而從政，曾任[廣西政協委員](../Page/廣西政協.md "wikilink")、[香港](../Page/香港.md "wikilink")[油尖旺區議會議員和](../Page/油尖旺區議會.md "wikilink")[民主建港協進聯盟中央委員](../Page/民主建港協進聯盟.md "wikilink")。

## 簡歷

劉志榮從事演藝工作三十多年，先後主演數十部電視連續劇，如《[變色龍](../Page/變色龍.md "wikilink")》、《[浮生六劫](../Page/浮生六劫.md "wikilink")》、《[浴血太平山](../Page/浴血太平山.md "wikilink")》、《[精武門](../Page/精武門.md "wikilink")》等，齣齣膾炙人口。劉志榮劇中百變形象，廣為[香港](../Page/香港.md "wikilink")、國內及海外觀眾津津樂道，尤其是在《[大地恩情](../Page/大地恩情.md "wikilink")》中飾演的楊九斤。他曾五度榮獲香港十大電視明星金球獎，是[麗的電視全盛時期的代表人物](../Page/麗的電視.md "wikilink")。除演出劇集外，電視台凡製作大型節目，都多由劉志榮擔正主持。自1985年[亞洲電視取得國際亞太小姐選美大會四年主辦權後](../Page/亞洲電視.md "wikilink")，劉志榮即連續四屆出任大會司儀。其他由劉志榮主持的節目，不論是娛樂、綜藝性的（如《[開枱](../Page/開枱.md "wikilink")》和《[歷史也瘋狂](../Page/歷史也瘋狂.md "wikilink")》），或時事、資訊性的（如《十一億人民》），皆備受讚賞。

劉志榮口才了得，被譽為「金牌司儀」，因此不少商會盛宴，甚至政府的回歸紀念晚會，都會邀其擔當主持。正因劉志榮在演戲和主持方面都表現出色，即使從麗的歲月到[亞洲電視該台曾多次變換股權](../Page/亞洲電視.md "wikilink")，劉志榮在每個年代都同樣深受器重，奠定其「鎮台之寶」的地位。由於他言行溫文、處事得體，待人熱情友善，甚具大腕之風，同行便為他起了「劉翁」這綽號。

在電視演出同時，劉志榮亦有參演多部台灣及香港電影，如《翦翦風》、《大毒后》、《[警察故事](../Page/警察故事.md "wikilink")》等；其後更有自資出品及監製電影，包括《[卿本佳人](../Page/卿本佳人.md "wikilink")》、《女人檔案》等。劉志榮到外地登台，在星、馬、泰等地大受歡迎，1988年更獲[泰國公主頒發](../Page/泰國.md "wikilink")[皇室榮譽獎章](../Page/皇室.md "wikilink")。而他在演員生涯中，從未在[無綫電視亮相](../Page/無綫電視.md "wikilink")，直至1999年參選區議會，接受[無線節目](../Page/無綫新聞.md "wikilink")《[新聞透視](../Page/新聞透視.md "wikilink")》訪問，首次在無綫露面。

劉志榮積極參與社會事務，除了是廣西政協委員之外，還身兼多個[廣西鄉誼組織的主席職務](../Page/廣西.md "wikilink")。當香港第一屆行政長官選舉舉行，他亦是四百名推選委員之一。為慶祝[香港回歸](../Page/香港回歸.md "wikilink")，劉志榮在回歸前夕主辦中國名家精品匯展，邀請[程十髮](../Page/程十髮.md "wikilink")、錢君匋、宋文治、吳青霞等十位當代著名畫家，首次蒞港獻技。1997年7月1日，劉志榮更獲北京邀請擔任在[北京舉辦之](../Page/北京.md "wikilink")「首都各界慶祝香港回歸大會」主持。

1999年[香港特區舉行第一屆](../Page/香港特區.md "wikilink")[區議會選舉](../Page/區議會.md "wikilink")，劉志榮代表[民主建港聯盟出戰](../Page/民主建港聯盟.md "wikilink")[油尖旺區](../Page/油尖旺區.md "wikilink")[佐敦選區](../Page/佐敦.md "wikilink")，並得到1,142票而當選\[1\]。2003年，[中聯辦籌組](../Page/中聯辦.md "wikilink")「[香港電影電視演藝界訪京團](../Page/香港.md "wikilink")」，劉志榮獲任命為團隊的秘書長，得到國家副主席[曾慶紅與商務部副部長](../Page/曾慶紅.md "wikilink")[安民接見](../Page/安民.md "wikilink")。在同年的第二屆區選及07年的第三屆選舉中，劉志榮再度勝出。他在民建聯居中常委位置，也是黨內[九龍西支部副主席](../Page/九龍西.md "wikilink")。

2008年5月15日，劉志榮因肺積水於伊利沙伯醫院逝世，享年55歲。劉志榮治喪委員會由政、商、演藝及各界人士組成，政府、[中聯辦](../Page/中聯辦.md "wikilink")、[民建聯](../Page/民建聯.md "wikilink")、[亞洲電視及一眾地區團體均有代表參加](../Page/亞洲電視.md "wikilink")，包括[民建聯主席](../Page/民建聯.md "wikilink")[譚耀宗](../Page/譚耀宗.md "wikilink")、[民政事務局前局長](../Page/民政事務局.md "wikilink")[曾德成](../Page/曾德成.md "wikilink")、[立法會議員](../Page/立法會議員.md "wikilink")[霍震霆](../Page/霍震霆.md "wikilink")、前[亞視老闆](../Page/亞視.md "wikilink")[邱德根](../Page/邱德根.md "wikilink")、[其士集團創辦人](../Page/其士集團.md "wikilink")[周亦卿](../Page/周亦卿.md "wikilink")、[八和會館主席](../Page/八和會館.md "wikilink")[汪明荃](../Page/汪明荃.md "wikilink")、資深傳媒人[甘國亮等](../Page/甘國亮.md "wikilink")。

同年10月，他在[油尖旺區議會的民選議席由](../Page/油尖旺區議會.md "wikilink")[民建聯的](../Page/民建聯.md "wikilink")[葉傲冬奪得](../Page/葉傲冬.md "wikilink")。

2012年7月，在《亞洲電視55周年頒獎禮》中，劉志榮獲追頒『ATV 55年終生成就男藝人大獎』。

## 家庭

劉志榮父親是老牌影視明星及[粵劇名伶](../Page/粵劇.md "wikilink")[劉克宣](../Page/劉克宣.md "wikilink")\[2\]。劉志榮於1986年與同台藝人[梁淑莊結婚](../Page/梁淑莊.md "wikilink")，行內人稱之為「模範夫婦」，育有一子劉健樂\[3\]。他也是藝人[楊羚的舅父](../Page/楊羚.md "wikilink")\[4\]。

## 曾任公職

  - [中國人民政治協商會議委員](../Page/中國人民政治協商會議.md "wikilink")
    ([廣西](../Page/廣西.md "wikilink") )
  - 第九、十、十一屆[全國人民代表大會代表選舉會議成員](../Page/全國人民代表大會.md "wikilink")
  - [香港特別行政區第一屆政府推選委員](../Page/香港特別行政區.md "wikilink")
  - 世界[廣西同鄉聯誼會第二屆](../Page/廣西.md "wikilink")[主席](../Page/主席.md "wikilink")
  - [香港](../Page/香港.md "wikilink")[廣西聯誼會創會](../Page/廣西.md "wikilink")[主席](../Page/主席.md "wikilink")
  - [香港](../Page/香港.md "wikilink")[廣西社團總會名譽會長](../Page/廣西.md "wikilink")
  - [香港](../Page/香港.md "wikilink")[廣西同鄉聯誼會第八屆](../Page/廣西.md "wikilink")[理事長](../Page/理事長.md "wikilink")
  - [香港各界慶祝回歸活動委員會委員](../Page/香港.md "wikilink")
  - [香港特別行政區](../Page/香港特別行政區.md "wikilink")[油尖旺區議會](../Page/油尖旺區議會.md "wikilink")([佐敦選區](../Page/佐敦道.md "wikilink"))民選[區議員](../Page/區議員.md "wikilink")
  - [民建聯中央委員會委員暨](../Page/民建聯.md "wikilink")[九龍西支部](../Page/九龍西.md "wikilink")[副主席](../Page/副主席.md "wikilink")
  - [華南電影工作者聯合會副理事長](../Page/華南.md "wikilink")
  - [香港](../Page/香港.md "wikilink")[中華國際電影電視有限公司](../Page/中華.md "wikilink")（已結業）[總經理](../Page/總經理.md "wikilink")

## 參演劇集

### 電視劇([香港電台](../Page/香港電台.md "wikilink"))

  - 1974年 [獅子山下](../Page/獅子山下.md "wikilink") 回巢 飾 陳永

### 電視劇([麗的電視](../Page/麗的電視.md "wikilink")/[亞洲電視](../Page/亞洲電視.md "wikilink"))

  - 1974年 [金粉世家](../Page/金粉世家.md "wikilink")
  - 1974年 [紫杜鵑](../Page/紫杜鵑.md "wikilink")
  - 1974年 [殞星](../Page/殞星.md "wikilink")
  - 1974年 [梁天來](../Page/梁天來.md "wikilink") 飾 梁天來
  - 1975年 [母親](../Page/母親_\(1975年電視劇\).md "wikilink") 飾 李家錫
  - 1975年 [海角風雲](../Page/海角風雲.md "wikilink")
  - 1975年 [疑雲](../Page/疑雲.md "wikilink")
  - 1976年 [三國春秋](../Page/三國春秋.md "wikilink") 飾 關雲長
  - 1977年 [浪淘沙](../Page/浪淘沙\(電視劇\).md "wikilink") 飾 陸偉基
  - 1978年 [變色龍](../Page/變色龍_\(電視劇\).md "wikilink") 飾 賀升
  - 1979年 [沈勝衣之白蜘蛛](../Page/沈勝衣.md "wikilink") 飾 偉七
  - 1979年[新變色龍](../Page/新變色龍.md "wikilink") 飾 賀升
  - 1980年 [浮生六劫](../Page/浮生六劫.md "wikilink") 飾 趙振鵬
  - 1980年 [大地恩情](../Page/大地恩情.md "wikilink") 飾 楊九斤
  - 1981年 [浴血太平山](../Page/浴血太平山.md "wikilink") 飾 周亞瑞
  - 1981年 [再會太平山](../Page/再會太平山.md "wikilink") 飾 周亞瑞
  - 1982年 [愛情跑道](../Page/愛情跑道.md "wikilink") 飾 林亞倫
  - 1982年 [天梯](../Page/天梯_\(1982年電視劇\).md "wikilink")
  - 1982年 [凹凸神探](../Page/凹凸神探.md "wikilink") 飾 劉家浩
  - 1982年 [家春秋](../Page/家春秋_\(電視劇\).md "wikilink")
  - 1982年 [雄霸天下](../Page/雄霸天下.md "wikilink")
  - 1983年 [八美圖](../Page/八美圖〈電視劇〉.md "wikilink")
  - 1983年 [世界仔](../Page/世界仔.md "wikilink")
  - 1984年 [表錯七段情](../Page/表錯七段情.md "wikilink")
  - 1984年 [霍東閣](../Page/霍東閣.md "wikilink") 飾 陳公哲
  - 1985年 [諜網迷情](../Page/諜網迷情.md "wikilink")
  - 1985年 [魅力九龍塘](../Page/魅力九龍塘.md "wikilink")
  - 1986年 [時光倒流七十年](../Page/時光倒流七十年.md "wikilink")
  - 1987年 [紅塵](../Page/紅塵_\(電視劇\).md "wikilink") 飾 呂志滔
  - 1987年 [梟雄](../Page/梟雄.md "wikilink")
  - 1988年 [賭徒](../Page/賭徒.md "wikilink")
  - 1990年 [Q表姐](../Page/Q表姐.md "wikilink")
  - 1991年 [觸電情緣](../Page/觸電情緣.md "wikilink")
  - 1995年 [精武門](../Page/精武門_\(電視劇\).md "wikilink") 飾 蔡六斤
  - 1996年 [親親，親人](../Page/親親，親人.md "wikilink") 飾 常大智
  - 1996年 [一人有一個理想](../Page/一人有一個理想.md "wikilink")
  - 1997年 [97變色龍](../Page/97變色龍.md "wikilink") 飾 賀升
  - 1997年 [國際刑警](../Page/國際刑警.md "wikilink") 飾 楊碧開
  - 2000年 [與狼共枕](../Page/與狼共枕.md "wikilink") 飾 孔競庭
  - 2000年 [電視風雲](../Page/電視風雲.md "wikilink") 飾 李富

## 參演電影

  - [我愛金龜婿](../Page/我愛金龜婿.md "wikilink")(1971)
  - [大殺手](../Page/大殺手.md "wikilink")(1972)
  - [娃娃夫人](../Page/娃娃夫人.md "wikilink")(1972)
  - [女魔](../Page/女魔.md "wikilink")(1974)
  - [翦翦風](../Page/翦翦風.md "wikilink")(1974)
  - [社女](../Page/社女.md "wikilink")(1975)
  - [遊戲人間三百年](../Page/遊戲人間三百年.md "wikilink")(1975)
  - [男人四十一條龍](../Page/男人四十一條龍.md "wikilink")(1975)
  - [蠱惑女光棍才](../Page/蠱惑女光棍才.md "wikilink")(1975)
  - [花心三少騷銀姐](../Page/花心三少騷銀姐.md "wikilink")(1976)
  - [新蘇小妹三難新郎](../Page/新蘇小妹三難新郎.md "wikilink")(1976)
  - [大毒后](../Page/大毒后.md "wikilink")(1976)
  - [投胎人](../Page/投胎人.md "wikilink")(1976)
  - [香港艾曼妞](../Page/香港艾曼妞.md "wikilink")(1977)
  - [風飄飄鬼飄飄](../Page/風飄飄鬼飄飄.md "wikilink")(1977)
  - [架步串女古惑仔](../Page/架步串女古惑仔.md "wikilink")(1977)
  - [邪魔煞](../Page/邪魔煞.md "wikilink")(1977)
  - [馬場風暴](../Page/馬場風暴.md "wikilink")(1978)
  - [廟街女強人](../Page/廟街女強人.md "wikilink")(1978)
  - [發財埋便](../Page/發財埋便.md "wikilink")(1978)
  - [I.Q. 爆棚](../Page/I.Q._爆棚.md "wikilink")(1981)
  - [女人檔案](../Page/女人檔案.md "wikilink")(1982)
  - [狼來了](../Page/狼來了.md "wikilink")(1982)
  - [龍潭大鱷](../Page/龍潭大鱷.md "wikilink")(1985)
  - [警察故事](../Page/警察故事.md "wikilink")(1985)
  - [開心鬼精靈](../Page/開心鬼精靈.md "wikilink")(1986)
  - [惡男](../Page/惡男.md "wikilink")(1986)
  - [龍虎智多星](../Page/龍虎智多星.md "wikilink")(1988)
  - [龍之爭霸](../Page/龍之爭霸.md "wikilink")(1989)
  - [鐵漢柔情](../Page/鐵漢柔情.md "wikilink")(1990)
  - [摩登如來神掌](../Page/摩登如來神掌.md "wikilink")(1990)
  - [皇家女將](../Page/皇家女將.md "wikilink")(1990)
  - [四大家族之龍虎兄弟](../Page/四大家族之龍虎兄弟.md "wikilink")(1991)
  - [鐳射劇場之偏門家族](../Page/鐳射劇場之偏門家族.md "wikilink")(1992)
  - [浪子殺手霸王花](../Page/浪子殺手霸王花.md "wikilink")(1992)

## 主持節目

  - [亞太小姐選美大會](../Page/亞太小姐選美大會.md "wikilink")
  - [亞洲小姐競選](../Page/亞洲小姐競選.md "wikilink")
  - [廣州小姐競芳華](../Page/廣州小姐競芳華.md "wikilink")
  - [慶節煙花匯演](../Page/慶節煙花匯演.md "wikilink")
  - [亞視台慶](../Page/亞視台慶.md "wikilink")
  - [開枱](../Page/開枱.md "wikilink")
  - [冧莊](../Page/冧莊.md "wikilink")
  - [物有所值](../Page/物有所值.md "wikilink")
  - [盤滿砵滿](../Page/盤滿砵滿_\(電視節目\).md "wikilink")
  - [歷史也瘋狂](../Page/歷史也瘋狂.md "wikilink")
  - [十一億人民](../Page/十一億人民.md "wikilink")
  - [九七會係點](../Page/九七會係點.md "wikilink")
  - [首都各界慶祝香港回歸祖國大會](../Page/首都各界慶祝香港回歸祖國大會.md "wikilink")

## 個人╱合集唱片

  - 《[再會太平山](http://www.vinylparadise.com/4pop_can/2/255LCW0A.htm)》(1981)
  - 《[亞洲電視劇集主題曲大全'86](http://www.vinylparadise.com/4pop_can/2/255LCW0A.htm)》(1986)

## 参考資料

  - <https://web.archive.org/web/20080521020104/http://hk.news.yahoo.com/080517/60/2u63r.html>
  - <http://www.mtime.com/person/1016383/>
  - <http://www.usqiaobao.com:81/qiaobaoweek/html/2008-05/25/content_46210.htm>
  - <https://web.archive.org/web/20090524025040/http://www.mingpaony.com/htm/News/20080606/HK-maa1.htm>
  - <http://paper.wenweipo.com/2008/05/18/HK0805180024.htm>
  - <http://www.hkcd.com.hk/content/2008-06/06/content_2098107.htm>
  - <http://hm.people.com.cn/GB/83174/7348674.html>
  - <http://hkmdb.com/db/people/view.mhtml?id=4060&display_set=big5>

[Category:前香港區議員](../Category/前香港區議員.md "wikilink")
[Category:前麗的電視藝員](../Category/前麗的電視藝員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:藝人出身的政治人物](../Category/藝人出身的政治人物.md "wikilink")
[C](../Category/劉姓.md "wikilink")
[Category:民主建港協進聯盟成員](../Category/民主建港協進聯盟成員.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:玉林人](../Category/玉林人.md "wikilink")
[Category:香港男演員](../Category/香港男演員.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:香港電視演員](../Category/香港電視演員.md "wikilink")
[Category:香港天主教徒](../Category/香港天主教徒.md "wikilink")

1.
2.
3.
4.