**漢世祖劉旻**（），[並州](../Page/並州.md "wikilink")[晉陽](../Page/晉陽.md "wikilink")（今[山西](../Page/山西.md "wikilink")[太原](../Page/太原.md "wikilink")）人，[沙陀族](../Page/沙陀.md "wikilink")，原名**刘彦崇**、**劉崇**，[五代十國時期](../Page/五代十國.md "wikilink")[北漢開國皇帝](../Page/北漢.md "wikilink")，為後漢高祖[劉知遠之弟](../Page/劉知遠.md "wikilink")，父[劉琠](../Page/劉琠.md "wikilink")。

## 生平

劉崇年輕時喜歡飲酒[賭博](../Page/賭博.md "wikilink")，曾經於臉上[刺青從軍](../Page/刺青.md "wikilink")。劉知遠於[後晉任河東節度使時](../Page/後晉.md "wikilink")，他擔任都指揮使。劉知遠建[後漢之後](../Page/後漢.md "wikilink")，任[太原尹](../Page/太原尹.md "wikilink")，後漢隱帝[劉承祐在位時任河東節度使](../Page/劉承祐.md "wikilink")（位於太原），鎮守河東地區。

後漢[乾祐三年](../Page/乾祐_\(后汉\).md "wikilink")（950年），[樞密使](../Page/樞密使.md "wikilink")[郭威為劉承祐逼反](../Page/郭威.md "wikilink")，進軍後漢都城[大梁](../Page/大梁.md "wikilink")（今[河南](../Page/河南.md "wikilink")[開封](../Page/開封.md "wikilink")），劉承祐逃亡中為下屬郭允明所殺，郭威遂控制朝政。劉崇此時原欲舉兵南下，但聽到郭威計畫迎立劉崇之長子武寧節度使[劉贇為帝](../Page/劉贇.md "wikilink")，遂打消此意。然而不久郭威被黃旗加身後自登帝位，建立[後周](../Page/後周.md "wikilink")，改元[廣順](../Page/廣順.md "wikilink")，並殺劉贇，當時為951年。因此劉崇隨即亦在[太原](../Page/太原.md "wikilink")（今[山西省](../Page/山西省.md "wikilink")[太原市](../Page/太原市.md "wikilink")）登帝位，延續[後漢](../Page/後漢.md "wikilink")，改名劉旻，仍維持乾祐年號，稱乾祐四年。後世把劉崇稱帝後的政權稱作[北漢](../Page/北漢.md "wikilink")。

北漢地小民貧，又以興復後漢為業，遂向[遼國乞援](../Page/遼國.md "wikilink")，與遼國約為父子之國，由劉旻稱遼帝為叔，而自稱姪皇帝；遼國則封劉旻為大漢神武皇帝。北漢因遼國的援助，而與後周進行了不少戰爭，但仍勝少敗多。北漢乾祐七年（954年），趁郭威去世之際，聯合遼國南攻後周，然為後周世宗柴榮率軍[敗於高平](../Page/高平之戰.md "wikilink")，劉旻穿著農人的衣服隨百餘騎逃走，途中一度迷路，劉旻年老力衰，差點無法支撐回到太原。

經此一役，北漢元氣大傷，無力南下，而劉旻亦憂憤成疾，不久去世，廟號世祖，次子[劉承鈞繼位](../Page/劉承鈞.md "wikilink")。

## 家庭

### 后妃

  - [皇后某氏](../Page/北汉世祖皇后.md "wikilink")
  - 王氏（915年—971年），赠太惠妃

### 子女

#### 子

1.  湘陰公 [劉贇](../Page/劉贇.md "wikilink")
2.  睿宗 [劉鈞](../Page/刘钧_\(北汉\).md "wikilink")
3.  [劉鎬](../Page/刘镐_\(北汉\).md "wikilink")
4.  [劉錫](../Page/刘承锡.md "wikilink")
5.  [劉鍇](../Page/刘锴_\(北汉\).md "wikilink")
6.  [劉銑](../Page/劉銑.md "wikilink")
7.  （失名）
8.  （失名）
9.  （失名）

#### 女

  - 有史可查者一人

<!-- end list -->

1.  [刘氏](../Page/劉氏_\(劉旻女\).md "wikilink")，下嫁薛钊、何某，生子[劉繼恩](../Page/劉繼恩.md "wikilink")、[劉繼忠](../Page/劉繼忠.md "wikilink")、[劉繼元](../Page/劉繼元.md "wikilink")。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:北漢皇帝](../Category/北漢皇帝.md "wikilink")
[Category:后汉宗室](../Category/后汉宗室.md "wikilink")
[Category:后汉太原尹](../Category/后汉太原尹.md "wikilink")
[Category:開國君主](../Category/開國君主.md "wikilink")
[M](../Category/刘姓.md "wikilink")