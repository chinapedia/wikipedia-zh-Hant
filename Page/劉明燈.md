**劉明燈**（1838年—1895年），[字](../Page/表字.md "wikilink")**照遠**，[號](../Page/號.md "wikilink")**簡青**，[中國](../Page/中國.md "wikilink")[清朝](../Page/清朝.md "wikilink")[湖南](../Page/湖南.md "wikilink")[大庸](../Page/大庸.md "wikilink")（今[張家界](../Page/張家界.md "wikilink")[永定](../Page/永定.md "wikilink")）人，武官，曾任[台灣總兵](../Page/台灣總兵.md "wikilink")、[甘南](../Page/甘肅.md "wikilink")[提督](../Page/提督.md "wikilink")，長於[書法](../Page/書法.md "wikilink")。

## 生平

[劉明燈統帥過福安村題名碑.jpg](https://zh.wikipedia.org/wiki/File:劉明燈統帥過福安村題名碑.jpg "fig:劉明燈統帥過福安村題名碑.jpg")，劉明燈勒石於[車城庄福德廟](../Page/車城福安宮.md "wikilink")\[1\]。\]\]
劉明燈[咸豐年間中](../Page/咸豐.md "wikilink")[武舉](../Page/武舉.md "wikilink")，1861年率所部三千人入[左宗棠](../Page/左宗棠.md "wikilink")[湘軍](../Page/湘軍.md "wikilink")[楚勇部](../Page/楚勇.md "wikilink")，劉明燈與其弟劉明珍均是左宗棠麾下僅次劉典的將領，於由於作戰英勇升至[福寧總兵](../Page/福寧總兵.md "wikilink")。[清穆宗](../Page/清穆宗.md "wikilink")[同治五年](../Page/同治.md "wikilink")（1866年）奉旨接任[曾元福擔任](../Page/曾元福.md "wikilink")[台灣總兵](../Page/台灣總兵.md "wikilink")，率所部楚軍新左營弁勇赴台，可視為[左宗棠調其親信湘軍入台之始](../Page/左宗棠.md "wikilink")。\[2\]
之後雖亦有少數淮軍入台，但是直到劉銘傳時代，湘軍一直是台灣駐軍的主力。

現在[新北市名勝](../Page/新北市.md "wikilink")[雄鎮蠻煙碑](../Page/雄鎮蠻煙碑.md "wikilink")、[虎字碑](../Page/虎字碑.md "wikilink")、[金字碑](../Page/金字碑.md "wikilink")，皆是劉明燈巡防[台灣所立的](../Page/台灣.md "wikilink")，並且是他親筆墨寶，[書法一絕](../Page/書法.md "wikilink")。另外，台灣與[美國的知名外交事件](../Page/美國.md "wikilink")－[羅發號事件也是在劉明燈任台灣總兵時所發生](../Page/羅發號事件.md "wikilink")。

明燈後任甘南[提督](../Page/提督.md "wikilink")，[清德宗](../Page/清德宗.md "wikilink")[光緒四年](../Page/光緒.md "wikilink")（1878年）以[丁憂故解甲歸田](../Page/丁憂.md "wikilink")，在故里作慈善活動，興學、立坊，修橋、鋪路、興水利、設義渡等。

據[宜蘭](../Page/宜蘭縣.md "wikilink")[舉人](../Page/舉人.md "wikilink")[林步瀛等](../Page/林步瀛.md "wikilink")15人所撰文，他們對劉明燈評價相當高，「揆文奮武，兼詞章篆隸以名家；移孝作忠，歷皖翻閩江而奏績」。有「輕裘緩帶，[羊叔子之高風](../Page/羊叔子.md "wikilink")；羽扇綸巾，[武鄉侯之雅度](../Page/諸葛亮.md "wikilink")」。

在左宗棠奏摺「揀員調補台灣鎮總兵摺」中，稱許劉明燈「謀勇兼資，廉幹而善拊循，朴質而通方略，可望成一名將。」\[3\]

## 事件

  - [羅發號事件](../Page/羅發號事件.md "wikilink")
    ：1867年（同治六年）三月（陽曆），美國商船羅發號（Rover，又譯羅妹號）遭風浪漂流至屏東七星岩觸礁沉沒，船長亨特·漢特（J.
    W.
    Hunt）夫婦等十三人遭「龜仔甪社」「出草」殺害。同年4月19日臺灣總兵劉明燈、兵備道[吳大廷接見李仙得時](../Page/吳大廷.md "wikilink")，出示照會時言：「臺地生番穴處猱居，不載版圖，為聲教所不及，今該船遭風誤陷絕地，為思慮防範所不到，苟可盡力搜捕，無不飛速檄行，無煩合眾國兵力相幫辦理」\[4\]
  - [安平砲擊事件](../Page/安平砲擊事件.md "wikilink"):安平砲擊事件發生於1868年11月25日到12月2日，事件成因複雜，以樟腦糾紛為主的通商問題與埤頭教案為主。英國軍艦於11月25日下午砲擊安平，26日凌晨襲擊台灣水師協官署，清兵11名陣亡6名受傷，水師協署副將[江國珍受傷藏匿民家](../Page/江國珍.md "wikilink")，後服毒自盡\[5\]
    。27日與自台灣府趕來支援的清兵發生戰鬥，清兵的火藥庫遭英軍放火。劉明燈以英方強橫企圖再戰，但為地方仕紳以「從前粵東洋務，前事可鑒」所勸阻而停戰談和。\[6\]

## 文化資產

劉明燈在台的有形文化資產有:

  - [坪林茶業博物館之](../Page/坪林茶業博物館.md "wikilink")[虎字碑](../Page/虎字碑_\(坪林\).md "wikilink")
  - [貢寮區](../Page/貢寮區.md "wikilink")[草嶺古道之](../Page/草嶺古道.md "wikilink")[虎字碑](../Page/虎字碑_\(貢寮\).md "wikilink")
  - [雄鎮蠻煙碑](../Page/雄鎮蠻煙碑.md "wikilink")
  - [金字碑](../Page/金字碑.md "wikilink")
  - [礁溪勅建協天廟匾](../Page/礁溪.md "wikilink")\[7\]
  - [新埔劉氏家廟本支百世匾](../Page/新埔.md "wikilink")
  - [車城](../Page/車城.md "wikilink")[福安宮劉明燈統帥過福安村題名碑](../Page/福安宮.md "wikilink")

## 參考文獻

  - 書目

<!-- end list -->

  - 劉寧顏編，《重修台灣省通志》，台北市，台灣省文獻委員會，1994年。

<!-- end list -->

  - 引用

[Category:台灣鎮總兵](../Category/台灣鎮總兵.md "wikilink")
[Category:土家族人](../Category/土家族人.md "wikilink")
[Category:張家界人](../Category/張家界人.md "wikilink")
[Category:劉姓家族](../Category/劉姓家族.md "wikilink")

1.

2.  [郭廷以《郭廷以先生百歲冥誕紀念史學論文集:　左宗棠與台灣》](https://books.google.com.tw/books?id=V0wFAzs3UDMC&pg=PA182&dq=%E5%8A%89%E6%98%8E%E7%87%88+++%E9%83%AD%E5%BB%B7%E4%BB%A5%E5%85%88%E7%94%9F%E7%99%BE%E6%AD%B2%E5%86%A5%E8%AA%95%E7%B4%80%E5%BF%B5%E5%8F%B2%E5%AD%B8%E8%AB%96%E6%96%87%E9%9B%86&hl=zh-TW&sa=X&ved=0ahUKEwiLk5_k6eLUAhVBEpQKHZhTDZEQ6AEIJDAA#v=onepage&q=%E5%8A%89%E6%98%8E%E7%87%88%20%20%20%E9%83%AD%E5%BB%B7%E4%BB%A5%E5%85%88%E7%94%9F%E7%99%BE%E6%AD%B2%E5%86%A5%E8%AA%95%E7%B4%80%E5%BF%B5%E5%8F%B2%E5%AD%B8%E8%AB%96%E6%96%87%E9%9B%86&f=false)

3.
4.  [Edward H.
    House《征臺紀事：牡丹社事件始末》五南出版，2015](http://www.wunan.com.tw/www2/download/preview/8V11.PDF)

5.  [陳良智
    《清末政教關係：以清法戰爭後馬偕與劉銘傳的交涉為例》2013](http://ir.taitheo.org.tw:8080/ir/bitstream/987654321/5473/2/MDIV2013-6_%E9%99%B3%E8%89%AF%E6%99%BA.pdf)

6.

7.  [宜蘭縣勅建礁溪協天廟《神威顯靈責難欽差官》](http://www.sttemple.org/index/index-04-3.htm)