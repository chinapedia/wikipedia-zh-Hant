**劉涌**（），[中國](../Page/中國.md "wikilink")[辽宁](../Page/辽宁省.md "wikilink")[沈阳人](../Page/沈阳市.md "wikilink")，[黑社會頭目](../Page/黑社會.md "wikilink")，被控为與沈陽市主要领导[慕绥新](../Page/慕绥新.md "wikilink")、[马向东等人相互勾結而被](../Page/马向东.md "wikilink")[中華人民共和國政府](../Page/中華人民共和國政府.md "wikilink")[处决](../Page/死刑.md "wikilink")。

## 简历

刘涌生于辽宁省沈阳市，[汉族](../Page/汉族.md "wikilink")，[初中学历](../Page/初级中学.md "wikilink")。原任[沈阳嘉阳集团董事长](../Page/沈阳嘉阳集团.md "wikilink")，[和平区政协委员](../Page/和平区_\(瀋陽市\).md "wikilink")，[中国致公党沈阳市直属支部主委](../Page/中国致公党.md "wikilink")。1997年12月当选为第十二届[沈阳市人民代表大会代表](../Page/沈阳市人民代表大会.md "wikilink")。1994年曾因犯伤害罪被沈阳市公安局收容审查一次。

2000年7月3日，沈阳市公安局对刘涌及其四名同伙发起[通緝](../Page/通緝.md "wikilink")\[1\]\[2\]，刘涌当天乘出租车从沈阳前往铁岭，其后辗转长春、哈尔滨等地，7月8日到达[黑龙江省](../Page/黑龙江省.md "wikilink")[黑河市](../Page/黑河市.md "wikilink")\[3\]。7月11日下午，刘涌在黑河出境未遂，在乘车逃亡途中被当地警方抓获\[4\]\[5\]\[6\]，其后被[沈阳市公安局](../Page/沈阳市公安局.md "wikilink")[刑事拘留](../Page/拘留.md "wikilink")，同年8月10日经[沈阳市人民检察院批准逮捕](../Page/沈阳市人民检察院.md "wikilink")。2002年4月17日被判死刑，2003年8月15日改判死刑缓期两年执行。2003年12月20日，[中华人民共和国最高人民法院在没有有关当事人的申诉或最高人民检察院的抗诉的情况下](../Page/中华人民共和国最高人民法院.md "wikilink")\[7\]，再审改判死刑，[剥夺政治权利终身](../Page/剥夺政治权利.md "wikilink")。2003年12月22日在[锦州市被处以](../Page/锦州市.md "wikilink")[死刑](../Page/死刑.md "wikilink")。\[8\]\[9\]

## 影视形象

曾饰演过[白眉大侠的演员](../Page/白眉大侠_\(电视剧\).md "wikilink")[赵恒煊在电视剧](../Page/赵恒煊.md "wikilink")《[大江东去](../Page/大江东去_\(2003年电视剧\).md "wikilink")》和《人大主任》中饰演的黑道富豪现实原型皆为刘涌。

## 参考文献

[Category:被中华人民共和国以故意伤害罪为名处决的死刑犯](../Category/被中华人民共和国以故意伤害罪为名处决的死刑犯.md "wikilink")
[L](../Category/中華人民共和國企業家.md "wikilink")
[L](../Category/中国致公党党员.md "wikilink")
[Category:慕马案](../Category/慕马案.md "wikilink")
[Category:黑社會成員](../Category/黑社會成員.md "wikilink")
[Category:2003年被中华人民共和国处决的死刑犯](../Category/2003年被中华人民共和国处决的死刑犯.md "wikilink")
[L](../Category/沈阳人.md "wikilink") [Y](../Category/刘姓.md "wikilink")

1.
2.
3.
4.
5.
6.
7.  <https://news.sohu.com/2003/12/22/12/news217271207.shtml>
8.  [最高人民法院再审刘涌案刑事判决书(全文)](http://news.sohu.com/2003/12/23/45/news217334582.shtml)
9.