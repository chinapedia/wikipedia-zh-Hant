**劉金標**（**King
Liu**，）是台灣知名的企業家，[台中](../Page/台中.md "wikilink")[沙鹿出身](../Page/沙鹿.md "wikilink")，是巨大機械、[捷安特](../Page/捷安特.md "wikilink")、微笑單車公司的創辦人暨董事長。以推廣自行車運動而聞名，因為他的推廣，也讓台灣在每年的五月都有專屬的「台灣自行車月」。2006年獲選為Discovery頻道《台灣人物誌》的6名主角之1。\[1\]

## 參考

[Category:自行車](../Category/自行車.md "wikilink")
[Category:台灣企業家](../Category/台灣企業家.md "wikilink")
[Category:沙鹿人](../Category/沙鹿人.md "wikilink")
[Category:中華民國總統府資政](../Category/中華民國總統府資政.md "wikilink")
[Category:國立臺中高級工業職業學校校友](../Category/國立臺中高級工業職業學校校友.md "wikilink")
[Jin金](../Category/劉姓.md "wikilink")
[創辦人](../Category/巨大集團.md "wikilink")

1.  [劉金標歡慶80歲
    二度單車環島](https://tw.news.yahoo.com/%E5%8A%89%E9%87%91%E6%A8%99%E6%AD%A1%E6%85%B680%E6%AD%B2-%E4%BA%8C%E5%BA%A6%E5%96%AE%E8%BB%8A%E7%92%B0%E5%B3%B6-215919338--finance.html)