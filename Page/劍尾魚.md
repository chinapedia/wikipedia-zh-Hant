**劍尾魚**（[學名](../Page/學名.md "wikilink")：），為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鳉形目](../Page/鳉形目.md "wikilink")[花鳉科的其中一種](../Page/花鳉科.md "wikilink")，分布於[中美洲](../Page/中美洲.md "wikilink")[墨西哥Nantla至](../Page/墨西哥.md "wikilink")[宏都拉斯西北部的淡水流域](../Page/宏都拉斯.md "wikilink")，並引進到許多國家，體長可達14公分，棲息在水流快速、植被生長的溪流或湖泊、池塘，屬雜食性，主要以[甲殼類](../Page/甲殼類.md "wikilink")、[蠕蟲](../Page/蠕蟲.md "wikilink")、[昆蟲等為食](../Page/昆蟲.md "wikilink")，人工飼養可投為飼料，适应能力强，性情温和，不易生病，可以和其他无攻击性的[热带鱼混养](../Page/热带鱼.md "wikilink")。但劍尾魚喜欢跳跃，因此魚缸要加盖。否则容易跳出导致死亡。雌魚要比雄魚体形要大，雄鱼尾部有延长的剑状突出，雌魚尾部也有劍狀突出但與雄魚相較下較短，有单剑，也有双剑。

[Xiphophorus_helleri.jpg](https://zh.wikipedia.org/wiki/File:Xiphophorus_helleri.jpg "fig:Xiphophorus_helleri.jpg")
红剑鱼是一种人工培育的品种，是劍尾魚经[杂交选育的](../Page/混種.md "wikilink")，通体红色，尾剑一般镶有黑边，十分美丽。

## 參考

  -
  - （中文）[xiphophorus.net](http://www.xiphophorus.net/)

[XH](../Category/觀賞魚.md "wikilink")
[hellerii](../Category/剑尾鱼属.md "wikilink")