**劍淵町**（）是位於[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[上川綜合振興局中部](../Page/上川綜合振興局.md "wikilink")[名寄盆地裡的一個町](../Page/名寄盆地.md "wikilink")。東西兩側皆為南北走向的丘陵地帶，南鄰[和寒町](../Page/和寒町.md "wikilink")，北部以[犬牛別川與](../Page/犬牛別川.md "wikilink")[士別市相隔](../Page/士別市.md "wikilink")。[天鹽川的支流](../Page/天鹽川.md "wikilink")[劍淵川留過轄區中央](../Page/劍淵川.md "wikilink")。

屬於內陸性氣候，夏季由於日照多，氣溫較高，但從夏末到秋季則日照天數較少；春季少雨，秋季多雨，冬天則會有大雪。\[1\]

名稱源自[阿伊努語的](../Page/阿伊努語.md "wikilink")「kene-pet-put-i」或「kene-put-i」，意思是「[赤楊多的河流的河口](../Page/赤楊.md "wikilink")」\[2\]。

## 歷史

  - 1897年：設置劍淵村。
  - 1899年：設置劍淵戶長役場。
  - 1902年：與上名寄3村戶長役場（現在的[名寄市](../Page/名寄市.md "wikilink")）分離。
  - 1906年4月1日：成為北海道二級村。
  - 1915年4月1日：和寒村從劍淵村中分村。
  - 1927年10月1日：溫根別村（後已合併為[士別市](../Page/士別市.md "wikilink")）從劍淵村中分村。
  - 1962年1月1日：改制為劍淵町。

## 交通

### 機場

  - [旭川機場](../Page/旭川機場.md "wikilink")（位於[東神樂町](../Page/東神樂町.md "wikilink")）

### 鐵路

  - [北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")
      - [宗谷本線](../Page/宗谷本線.md "wikilink")：[東六線車站](../Page/東六線車站.md "wikilink")
        - [劍淵車站](../Page/劍淵車站.md "wikilink") -
        [北劍淵車站](../Page/北劍淵車站.md "wikilink")

### 道路

[Ehonnosato_Kenbuchi_Michinoeki_Hokkaido_Japan.JPG](https://zh.wikipedia.org/wiki/File:Ehonnosato_Kenbuchi_Michinoeki_Hokkaido_Japan.JPG "fig:Ehonnosato_Kenbuchi_Michinoeki_Hokkaido_Japan.JPG")[繪本之鄉劍淵](../Page/繪本.md "wikilink")\]\]

<table style="width:70%;">
<colgroup>
<col style="width: 35%" />
<col style="width: 35%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>高速道路</dt>

</dl>
<ul>
<li><a href="../Page/道央自動車道.md" title="wikilink">道央自動車道</a>：<a href="../Page/士別劍淵交流道.md" title="wikilink">士別劍淵交流道</a></li>
</ul>
<dl>
<dt><a href="../Page/一般國道.md" title="wikilink">一般國道</a></dt>

</dl>
<ul>
<li><a href="../Page/國道40號.md" title="wikilink">國道40號</a></li>
<li><a href="../Page/國道239號.md" title="wikilink">國道239號</a></li>
</ul></td>
<td><dl>
<dt><a href="../Page/都道府縣道.md" title="wikilink">道道</a></dt>

</dl>
<ul>
<li>北海道道205號上士別線</li>
<li>北海道道251號雨龍旭川線</li>
<li>北海道道293號溫根別劍淵停車場線</li>
<li>北海道道536號劍淵原野士別線</li>
<li>北海道道545號三和劍淵線</li>
<li>北海道道984號溫根別線</li>
</ul></td>
</tr>
</tbody>
</table>

## 觀光景點

### 景點

  - 繪本之館

## 教育

### 高等學校

  - [道立北海道劍淵高等學校](http://business4.plala.or.jp/kenko/)

### 中學校

  - 劍淵町立劍淵中學校

### 小學校

  - 劍淵町立劍淵小學校

## 姊妹市・友好都市

### 日本

  - [大門町](../Page/大門町.md "wikilink")（[富山縣](../Page/富山縣.md "wikilink")）：現已合併為[射水市](../Page/射水市.md "wikilink")。
  - [志度町](../Page/志度町.md "wikilink")（[香川縣](../Page/香川縣.md "wikilink")）：現已合併為[讚岐市](../Page/讚岐市.md "wikilink")。

## 參考資料

## 外部連結

  - [繪本之館](http://www.ehon-yakata.com/)

  - [劍淵商工會](https://web.archive.org/web/20061220111639/http://www.eolas.co.jp/hokkaido/biba/)

  - [道北観光連盟-劍淵町](https://web.archive.org/web/20070711113157/http://www15.ocn.ne.jp/~douhoku/kenbuchi.htm)

<!-- end list -->

1.  劍淵町官方網站 - 劍淵町的沿革和自然
    <http://www.town.kembuchi.hokkaido.jp/cgi-bin/odb-get.exe?WIT_template=AC02000&Cc=7D31458EA3&DM=&Tp=&IM=>

2.