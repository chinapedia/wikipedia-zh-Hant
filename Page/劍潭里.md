**劍潭里**位於[台灣](../Page/台灣.md "wikilink")[台北市](../Page/台北市.md "wikilink")[劍潭](../Page/劍潭.md "wikilink")，是歸於台北市[中山區的](../Page/中山區_\(臺北市\).md "wikilink")[里行政區劃之一](../Page/里.md "wikilink")。地點約在臺北市[明水路與](../Page/明水路.md "wikilink")[中山北路四段範圍內](../Page/中山北路.md "wikilink")，面積廣達千公頃左右。

劍潭里全區於[日治時期屬台北市](../Page/台灣日治時期.md "wikilink")，稱為[大宮町](../Page/大宮町_\(臺北市\).md "wikilink")。1949年，[國共內戰爆發](../Page/國共內戰.md "wikilink")，國府遷至台灣後，該里成為[大台北地區之](../Page/大台北地區.md "wikilink")[眷村聚集地之一](../Page/眷村.md "wikilink")，遷徙大量[中國大陸](../Page/中國大陸.md "wikilink")[新移民](../Page/新移民.md "wikilink")，至今該移民仍佔眷村成員六成左右。其中，[中華民國前總統](../Page/中華民國.md "wikilink")[蔣經國](../Page/蔣經國.md "wikilink")、中華民國前副總統[李元簇及如](../Page/李元簇.md "wikilink")[劉和謙等知名政治人物](../Page/劉和謙.md "wikilink")，其眷舍亦設於該里。

劍潭里因為包含部分劍潭風景區，佔地廣闊且風景優美。因此除[劍潭山內](../Page/劍潭山.md "wikilink")[圓山大飯店外](../Page/圓山大飯店.md "wikilink")，亦有[中華民國空軍總司令部](../Page/中華民國空軍.md "wikilink")、[中華民國海軍總司令部設立於此](../Page/中華民國海軍.md "wikilink")，據悉，台灣國防部亦將於2010年代左右自[博愛特區遷徙至該里](../Page/博愛特區.md "wikilink")。

除此，因為山坡地形因素，劍潭里另一特色為佛道寺、神壇頗多，包含[圓通巖](../Page/圓通巖.md "wikilink")、[福正宮等十數家宗教建築](../Page/福正宮.md "wikilink")。該等建築存廢及管理常引起部分媒體關注。除此，台北市政府設立的[劍潭山親山步道大部分區域亦在該里](../Page/親山步道.md "wikilink")。

## 參考資料

  - [中山區劍潭里介紹](https://web.archive.org/web/20070927050115/http://www.taipeilink.net/cgi-bin/SM_theme?page=43795c04)

[J](../Category/劍潭.md "wikilink") [中山區](../Category/臺北市的里.md "wikilink")