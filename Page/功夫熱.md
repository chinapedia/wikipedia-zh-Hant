《**功夫熱**》（）是[香港](../Page/香港.md "wikilink")[電視廣播有限公司拍攝製作的時裝劇集](../Page/電視廣播有限公司.md "wikilink")，全劇共20集，每集長約30分鐘，以[彩色製作](../Page/彩色.md "wikilink")。編導[朱克](../Page/朱克.md "wikilink")。[石堅擔任該劇的主角](../Page/石堅.md "wikilink")。其他主要演員有[梁淑卿](../Page/梁淑卿.md "wikilink")、[石修](../Page/石修.md "wikilink")、[蘇杏璇](../Page/蘇杏璇.md "wikilink")、[汪明荃](../Page/汪明荃.md "wikilink")、[劉麗詩等](../Page/劉麗詩.md "wikilink")。

## 故事大綱

柳仲元師父是一個盛名一時的武術家，他早已告老歸田，與家人隱居鄉間，閒時為村民醫治跌打，甚得人敬重。柳師父有一子兩女。長女柳鳳已出嫁，由於丈夫赴英謀生，她與幼女回娘家居住，每天在父親的醫館幫忙。次女柳儀，性格爽朗，就讀大學的她，與自幼跟隨父親習武的哥哥柳龍相反，對習武完全提不起興趣。

柳龍中學畢業後再無心向學，终日游手好閒，更恃懂得武功，不時生事與人打架令柳師父甚為擔心。在柳鳳的苦勸下，柳師父決定開設武館授徒，希望柳龍有了工作寄托，不再出外生事。無奈柳龍貪慕虛榮，欲加入演藝圈成為武打明星，因此被黑社會利用，更連累父親被要脅，究竟柳師父最後能否憑他的一身武功及仁義之心與黑勢力對抗，令兒子及時浪子回頭呢？

## 演員表

### 主要演員

  - [石　堅](../Page/石堅.md "wikilink") 飾 柳仲元
  - [梁淑卿](../Page/梁淑卿.md "wikilink") 飾 柳　妻
  - [蘇杏璇](../Page/蘇杏璇.md "wikilink") 飾 柳　鳳
  - [石　修](../Page/石修.md "wikilink") 飾 柳　龍
  - [汪明荃](../Page/汪明荃.md "wikilink") 飾 柳　儀
  - [劉麗詩](../Page/劉麗詩.md "wikilink") 飾 唐　雯

### 其他演員

  - [陳　東](../Page/陳東_\(演員\).md "wikilink") 飾 田　叔
  - [梁　愛](../Page/梁愛.md "wikilink") 飾 四　嬸
  - [譚芳妮](../Page/譚芳妮.md "wikilink") 飾 張安妮
  - [嚴　浩](../Page/嚴浩.md "wikilink") 飾 張　培
  - [陳立品](../Page/陳立品.md "wikilink") 飾 老　婦
  - [吳殷志](../Page/吳殷志.md "wikilink") 飾 大-{胆}-成
  - [陳劍雲](../Page/陳劍雲.md "wikilink") 飾 木　叔
  - [招浩東](../Page/招浩東.md "wikilink") 飾 馬　仔
  - [胡其剛](../Page/胡其剛.md "wikilink") 飾 馬　仔
  - [徐英龍](../Page/徐英龍.md "wikilink") 飾 馬仔寅/黑　仔
  - [盧大偉](../Page/盧大偉.md "wikilink") 飾 黃製片
  - [梁秋媚](../Page/梁秋媚.md "wikilink") 飾 吧　女/亞　嬌
  - [謝麗霞](../Page/謝麗霞.md "wikilink") 飾 侍　女/同學乙
  - [梁碧玲](../Page/梁碧玲.md "wikilink") 飾 舞　客
  - [劉仕裕](../Page/劉仕裕.md "wikilink") 飾 舞　客/伙　記/侍　仔
  - [張美琳](../Page/張美琳.md "wikilink") 飾 舞　客/安　娜
  - [賴水清](../Page/賴水清.md "wikilink") 飾 舞　客/徒弟乙
  - [黃　新](../Page/黃新.md "wikilink") 飾 莫　雄
  - [歐陽慎瑩](../Page/歐陽慎瑩.md "wikilink") 飾 泉　嫂
  - [羅　佳](../Page/羅佳.md "wikilink") 飾 占　士
  - [蘇鴻遠](../Page/蘇鴻遠.md "wikilink") 飾 飛　仔
  - [鍾濟深](../Page/鍾濟深.md "wikilink") 飾 李師傅
  - [張活游](../Page/張活游.md "wikilink") 飾 蘇　森
  - [鄭少萍](../Page/鄭少萍.md "wikilink") 飾 師　奶
  - [楊炎棠](../Page/楊炎棠.md "wikilink") 飾 大　衛
  - [蘇應波](../Page/蘇應波.md "wikilink") 飾 飛仔乙
  - [陳喜蓮](../Page/陳喜蓮.md "wikilink") 飾 姬絲汀
  - [黃建勳](../Page/黃建勳.md "wikilink") 飾 同學甲
  - [談泉慶](../Page/談泉慶.md "wikilink") 飾 徒弟甲
  - [李國麟](../Page/李國麟_\(演員\).md "wikilink") 飾 徒弟丙
  - [張光銳](../Page/張光銳.md "wikilink") 飾 徒弟丁
  - [黃寶玲](../Page/黃寶玲.md "wikilink") 飾 按摩女郎
  - [杜琪峯](../Page/杜琪峯.md "wikilink") 飾 店　員
  - [鄧英敏](../Page/鄧英敏.md "wikilink") 飾 雜　差
  - [徐桂香](../Page/徐桂香.md "wikilink") 飾 露　絲
  - [徐英龍](../Page/徐英龍.md "wikilink") 飾 黑　仔
  - [張　雷](../Page/張雷.md "wikilink") 飾 馬　仔
  - [黃美華](../Page/黃美華.md "wikilink") 飾 蘭　絲
  - [潔　瑩](../Page/潔瑩.md "wikilink") 飾 街坊甲/榮　嬸
  - 王　克 飾 街坊乙
  - [關偉倫](../Page/關偉倫.md "wikilink") 飾 馬　仔
  - [何禮男](../Page/何禮男.md "wikilink") 飾 男　仔
  - [何廣倫](../Page/何廣倫.md "wikilink") 飾 馬　仔

## 外部連結

  - [《功夫熱》 GOTV
    第1集重溫](https://web.archive.org/web/20140222163917/http://gotv.tvb.com/programme/103317/155296/)

[Category:1975年無綫電視劇集](../Category/1975年無綫電視劇集.md "wikilink")
[Category:無綫電視民初背景劇集](../Category/無綫電視民初背景劇集.md "wikilink")