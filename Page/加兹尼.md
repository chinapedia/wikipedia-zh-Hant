**加兹尼**（，中國宋朝《[诸番志](../Page/诸番志.md "wikilink")》作**吉慈尼**）是[阿富汗东部一城市](../Page/阿富汗.md "wikilink")，[加兹尼省省会](../Page/加兹尼省.md "wikilink")，位于[海拔](../Page/海拔.md "wikilink")2219米的[高原上](../Page/高原.md "wikilink")，接近[巴基斯坦边界](../Page/巴基斯坦.md "wikilink")，人口约141000人，[塔吉克族占](../Page/塔吉克族.md "wikilink")50%，[普什图族占](../Page/普什图族.md "wikilink")25%，[哈扎拉族占](../Page/哈扎拉族.md "wikilink")20%，另有少部分[印度人](../Page/印度.md "wikilink")。[喀布尔在其东北](../Page/喀布尔.md "wikilink")。

加兹尼古为[佛教中心](../Page/佛教.md "wikilink")，683年为[阿拉伯帝国攻占](../Page/阿拉伯帝国.md "wikilink")，从此[伊斯兰化](../Page/伊斯兰化.md "wikilink")。在994年至1160年间，以加兹尼为首都的伽色尼王国强盛一时，其领土范围最大时包括今巴基斯坦、阿富汗、[大呼罗珊和](../Page/大呼罗珊.md "wikilink")[波斯](../Page/波斯.md "wikilink")，伊斯兰教即是通过[伽色尼王国的征战而传入北印度](../Page/伽色尼王国.md "wikilink")。

1151年，加兹尼被[古尔王朝首次攻陷](../Page/古尔王朝.md "wikilink")，1173年，该城成为古尔王朝的陪都，1221年被[窝阔台率领的](../Page/窝阔台.md "wikilink")[蒙古帝国大军攻占并彻底屠杀和毁坏](../Page/蒙古帝国.md "wikilink")，其后虽然得以重建，但从此再也未能恢复到原有的繁盛。

1839年7月23日，[英军攻占该城](../Page/英军.md "wikilink")，拉开了[第一次英阿战争的序幕](../Page/英阿战争.md "wikilink")。20世纪末，该城又成为[塔利班和](../Page/塔利班.md "wikilink")[北方联盟争夺的焦点之一](../Page/北方聯盟_\(阿富汗\).md "wikilink")。

## 友好城市

  - [海沃德](../Page/海沃德_\(加利福尼亞州\).md "wikilink")

## 参考文献

## 参见

  - [伽色尼的马哈茂德](../Page/伽色尼的马哈茂德.md "wikilink")
  - [加兹尼战役](../Page/加兹尼战役.md "wikilink")

[Category:阿富汗城市](../Category/阿富汗城市.md "wikilink")
[Category:古都](../Category/古都.md "wikilink")