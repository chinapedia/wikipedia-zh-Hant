[Caribbean_Plate_map-fr.png](https://zh.wikipedia.org/wiki/File:Caribbean_Plate_map-fr.png "fig:Caribbean_Plate_map-fr.png")

**加勒比板块**是一个大部分为[大洋地壳的](../Page/大洋地壳.md "wikilink")[板块](../Page/板块.md "wikilink")，位于[中美洲和](../Page/中美洲.md "wikilink")[南美洲北岸以外的](../Page/南美洲.md "wikilink")[加勒比海](../Page/加勒比海.md "wikilink")。在1968年[勒皮雄首次提出的六大板块中](../Page/勒皮雄.md "wikilink")，它是美洲板块的一部分。

加勒比板块的面积大约是320万平方千米，它与[北美洲板块](../Page/北美洲板块.md "wikilink")、[南美洲板块和](../Page/南美洲板块.md "wikilink")[科科斯板块接界](../Page/科科斯板块.md "wikilink")。这些边界都是[地震活动密集区](../Page/地震.md "wikilink")，频繁发生地震，偶然还会发生[海啸](../Page/海啸.md "wikilink")；在边界上还有[火山喷发](../Page/火山.md "wikilink")。

## 边界类型

加勒比板块和北美洲板块相接的北界是一个[转形断层](../Page/转形断层.md "wikilink")，沿中美洲[伯利兹](../Page/伯利兹.md "wikilink")、[危地马拉和](../Page/危地马拉.md "wikilink")[洪都拉斯的边境地区](../Page/洪都拉斯.md "wikilink")，向东经[开曼海槽达](../Page/开曼海沟.md "wikilink")[古巴东南海岸以南](../Page/古巴.md "wikilink")，再至[伊斯帕尼奥拉岛](../Page/伊斯帕尼奥拉岛.md "wikilink")、[波多黎各岛和](../Page/波多黎各.md "wikilink")[维尔京群岛以北不远处](../Page/维尔京群岛.md "wikilink")。[大西洋中最深的](../Page/大西洋.md "wikilink")[波多黎各海沟](../Page/波多黎各海沟.md "wikilink")（约8400米深）有一部分也是沿这个边界延伸。波多黎各海沟是一个从其南部的[聚合边界向西部的](../Page/聚合板塊邊緣.md "wikilink")[转形边界过渡的复杂海底地貌单元](../Page/转形断层.md "wikilink")。

加勒比板块的东界是一个[隱沒带](../Page/隱沒带.md "wikilink")，但因为在大西洋上北美洲板块和南美洲板块的边界还没有得到明确认定，目前尚不知是其中哪一个板块在这个消减带处俯冲到加勒比板块之下，也许二者都有。这一消减作用造就了从北部的维尔京群岛直至南部靠近[委内瑞拉海岸诸岛的](../Page/委内瑞拉.md "wikilink")[小安的列斯](../Page/小安的列斯群岛.md "wikilink")[岛弧](../Page/岛弧.md "wikilink")。在这个边界上，一共有17座[活火山](../Page/活火山.md "wikilink")，其中最有名的是[蒙特塞拉特岛上的](../Page/蒙特塞拉特岛.md "wikilink")[苏弗里耶尔火山](../Page/苏弗里耶尔火山.md "wikilink")、[马提尼克岛上的](../Page/马提尼克岛.md "wikilink")[培雷火山](../Page/培雷火山.md "wikilink")、[瓜德罗普岛上的](../Page/瓜德罗普岛.md "wikilink")、[圣文森特岛上的](../Page/圣文森特岛.md "wikilink")及位于[格林纳达岛以北](../Page/格林纳达岛.md "wikilink")10千米处的海下火山。

[CaribbeanVolcanoMap.gif](https://zh.wikipedia.org/wiki/File:CaribbeanVolcanoMap.gif "fig:CaribbeanVolcanoMap.gif")

沿着加勒比板块具有复杂地质构造的南界，它和南美洲板块的相互作用形成了[巴巴多斯岛](../Page/巴巴多斯岛.md "wikilink")、[特立尼达岛](../Page/特立尼达岛.md "wikilink")（这二者都位于南美洲板块上）和[多巴哥岛](../Page/多巴哥岛.md "wikilink")（位于加勒比板块上），以及委内瑞拉海岸外的岛屿（包括[背风安的列斯群岛](../Page/背风安的列斯群岛.md "wikilink")）和[哥伦比亚](../Page/哥伦比亚.md "wikilink")。这一边界部分为转换断层，沿此断层发生逆冲和一定程度的消减。委内瑞拉储油丰富的[油田可能就是由这种复杂的板块作用造就的](../Page/油田.md "wikilink")。

加勒比板块的西部为中美洲占据。[太平洋中的科科斯板块在中美洲西海岸以外不远处向加勒比板块之下消减](../Page/太平洋.md "wikilink")。这一消减形成了危地马拉、[萨尔瓦多](../Page/萨尔瓦多.md "wikilink")、[尼加拉瓜和](../Page/尼加拉瓜.md "wikilink")[哥斯达黎加的火山](../Page/哥斯达黎加.md "wikilink")，並且形成[中美洲火山弧及](../Page/中美洲火山弧.md "wikilink")。

## 起源

加勒比板块被认为是一个[大火成岩省](../Page/大火成岩省.md "wikilink")，在几千万年前的[古太平洋中形成](../Page/古太平洋.md "wikilink")。当[大西洋开始扩张](../Page/大西洋.md "wikilink")，北美洲和南美洲即被向西推动，于是太平洋洋盆开始在美洲大陆的西缘之下消减。由于加勒比板块较太平洋海底的其余部位为厚，地势也较高，因此它并没有像其他太平洋海底那样消减，而是相对北美洲和南美洲向东运动，仰冲于大西洋海底之上，并在300万年前[巴拿马地峡形成之后](../Page/巴拿马地峡.md "wikilink")，最终完全和太平洋失去了联系。

据推断，南美洲板块的西向运动使分别与其北端和南端相邻的加勒比板块和[斯科舍板块均受到挤压](../Page/斯科舍板块.md "wikilink")。这两个板块因而具有相似的外形，其东界都是南美洲板块的消减带。[1](https://web.archive.org/web/20070515045138/http://www.geophysics.rice.edu/department/research/alan1/SECAR/Plate_Tectonic_Evolution_.htm)

## 参考來源

<references/>

## 外部链接

  - [NOAA Ocean
    Explorer](http://oceanexplorer.noaa.gov/explorations/03trench/welcome.html)
  - [Caribbean Plate formation PDF
    file](http://www.geo.tu-freiberg.de/~merkel/vorlesung/OS2001/bachmann_raik.pdf)

[Category:板块](../Category/板块.md "wikilink")
[Category:加勒比地区地理](../Category/加勒比地区地理.md "wikilink")
[Category:中美洲自然史](../Category/中美洲自然史.md "wikilink")
[Category:加勒比地区自然史](../Category/加勒比地区自然史.md "wikilink")