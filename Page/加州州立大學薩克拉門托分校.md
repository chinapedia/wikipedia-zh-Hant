[Sac_State_North_Entrance.jpg](https://zh.wikipedia.org/wiki/File:Sac_State_North_Entrance.jpg "fig:Sac_State_North_Entrance.jpg")
**沙加緬度加利福尼亞州立大學**
(****；常用簡稱****、****、**沙加緬度加州州大**；又常被譯為**加州州立大學沙加緬度分校**)
是一所位於[美國](../Page/美國.md "wikilink")[加州首府](../Page/加州.md "wikilink")[沙加緬度的公立大學](../Page/沙加緬度.md "wikilink")。該校屬於[加利福尼亞州立大學系統](../Page/加利福尼亞州立大學.md "wikilink")。2015年在《[美國新聞與世界報道](../Page/美國新聞與世界報道.md "wikilink")》“西部地區大學排名”中排在第58位。\[1\]

## 院系

沙加緬度加州州大设有文学院、商学院、教育学院、工程與计算机学院、健康与护理学院、自然科学与数学学院、社会科学与交叉科学学院以及继续教育学院，每年颁发60种本科学位及40种研究生学位，并与[太平洋大学联合颁发法律博士学位](../Page/太平洋大学.md "wikilink")。教育学院近年新开设教育学博士学位。Sac
State每年均被《[美国新闻与世界报道](../Page/美国新闻与世界报道.md "wikilink")》评为西部最佳大学之一。

## 著名校友

### 影劇界與媒體

  - 影帝[汤姆·汉克斯](../Page/汤姆·汉克斯.md "wikilink")（Tom Hanks）
  - 演员[卡洛斯·阿拉兹拉奎](../Page/卡洛斯·阿拉兹拉奎.md "wikilink")（Carlos
    Alazraqui，「Reno 911」主演）
  - 演员[里克·洛索维奇](../Page/里克·洛索维奇.md "wikilink")（Rick Rossovich）
  - 导演[喬·卡纳汗](../Page/喬·卡纳汗.md "wikilink")（Joe Carnahan）
  - 新闻播音员[吉塞爾·费尔南德斯](../Page/吉塞爾·费尔南德斯.md "wikilink")（Giselle Fernandez）
  - 新闻播音员[莱斯特·霍尔特](../Page/莱斯特·霍尔特.md "wikilink")（Lester Holt）
  - 新闻播音员[胡安·伦登](../Page/胡安·伦登.md "wikilink")（Joan Lunden）
  - 新闻播音员[芮妮·赛勒](../Page/芮妮·赛勒.md "wikilink")（Rene Syler）
  - 艺术家[格利高里·孔多斯](../Page/格利高里·孔多斯.md "wikilink")（Gergory Kondos）
  - 艺术家[维恩·赛鲍德](../Page/维恩·赛鲍德.md "wikilink")（Wayne Thiebaud）
  - 音乐家[博比·麦克费林](../Page/博比·麦克费林.md "wikilink")（Bobby
    McFerrin，[格莱美奖得主](../Page/格莱美奖.md "wikilink")）
  - 歌星[辛蒂·尼尔森](../Page/辛蒂·尼尔森.md "wikilink")（Cindy Nelson）
  - 作家[理查德·梅布里](../Page/理查德·梅布里.md "wikilink")（Richard Maybury）
  - 美国偶像[斯帝維·斯科特](../Page/斯帝維·斯科特.md "wikilink")（Stevie Scott）

### 法界與政界

  - 联邦法院法官：Janice Rogers Brown
  - [西沙加緬度市长](../Page/西沙加緬度.md "wikilink")：Christopher Cabaldon
  - 斯托克顿市市长：Edward Chavez
  - 前州长夫人：Sharon Davis
  - 前[加州副州长和](../Page/加州.md "wikilink")[众议员](../Page/众议员.md "wikilink")：Mervyn
    Dymally
  - 加州议员：Bill Emmerson
  - 前[众议员](../Page/众议员.md "wikilink")：Vic Fazio
  - 地区检察官：Bradford Fenocchio
  - 沙加緬度女议员：Lauren Hammond
  - 众议员 Wally Herger
  - 参议员：Bill Leonard
  - 加州议员：Lloyd Levine
  - 沙加緬度议员：Kevin McCarty
  - 前[国家安全局助理主任](../Page/国家安全局.md "wikilink")：Ray Shaddick
  - [澳門特別行政區行政長官](../Page/澳門特別行政區行政長官.md "wikilink")：[崔世安](../Page/崔世安.md "wikilink")

### 商界

  - 「Sleep Train Mattress Center」创始人：Dale Carlsen
  - 餐馆大亨：Randy Paragary
  - 地产大亨：Angelo Tsakopoulos
  - 「Java City」咖啡连锁店创始人：Tom Weborg

### 體育界

  - 橄榄球教练：Greg Knapp
  - 橄榄球明星[四分卫](../Page/四分卫.md "wikilink")：Aaron Garcia
  - 橄榄球明星四分卫：Ricky Ray
  - 橄榄球明星：Otis Amey
  - 橄榄球明星：Mike Black
  - 橄榄球明星：Mike Carter
  - 橄榄球明星：Dan Chamberlain
  - 橄榄球明星：John Farley
  - 橄榄球明星：Rob Harrison
  - 橄榄球明星：Jon Kirksey
  - 橄榄球明星：Marko Cavka
  - 橄榄球明星：Tony Cobrin
  - 橄榄球明星：John Gesek
  - 橄榄球明星：Lorenzo Lynch
  - 橄榄球明星：Al Nicholas
  - 橄榄球明星：Lonie Paxton
  - 橄榄球明星：Greg Robinson
  - 橄榄球明星：Charles Roberts
  - 橄榄球明星：Daimon Shelton
  - 橄榄球明星：Lindy Winkler
  - [冰球明星](../Page/冰球.md "wikilink")：Mike Lange
  - [棒球明星](../Page/棒球.md "wikilink")：Buck Martinez
  - [篮球明星](../Page/篮球.md "wikilink")：Jameel Pugh
  - [篮球明星](../Page/篮球.md "wikilink")：Greg Reed
  - [拳击明星](../Page/拳击.md "wikilink")：Norm Tavalero

## 參考文獻

## 外部連結

  - [Official website](http://www.csus.edu/)
  - [Official athletics website](http://www.hornetsports.com)

[Sacramento](../Category/加利福尼亚州立大学.md "wikilink")
[Category:1947年創建的教育機構](../Category/1947年創建的教育機構.md "wikilink")
[Category:加利福尼亚州中央谷地](../Category/加利福尼亚州中央谷地.md "wikilink")

1.