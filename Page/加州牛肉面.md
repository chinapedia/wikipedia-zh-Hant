[MrLeeNanchang.jpg](https://zh.wikipedia.org/wiki/File:MrLeeNanchang.jpg "fig:MrLeeNanchang.jpg")\]\]
[Californiabeefnoodle.jpg](https://zh.wikipedia.org/wiki/File:Californiabeefnoodle.jpg "fig:Californiabeefnoodle.jpg")\]\]
**北京李先生加州牛肉面大王公司**
(**李先生**)，旧名字**美国加州牛肉面**，又称“**加州牛肉面**”，包括若干家互相指责侵权的[中国](../Page/中华人民共和国.md "wikilink")[连锁](../Page/连锁.md "wikilink")[快餐店](../Page/快餐.md "wikilink")。面条制作方法为纯粹的[中式面条](../Page/中式面条.md "wikilink")，与台湾的[牛肉面几乎完全相同](../Page/牛肉面.md "wikilink")。

## 历史

在美国从事餐饮业的[华侨](../Page/华侨.md "wikilink")[吴京红](../Page/吴京红.md "wikilink")1985年回中国大陸创办了第一家“美国加州牛肉面大王”。1993年在中国大陸注册了[商标](../Page/商标.md "wikilink")。1996年在[美国注册了商标](../Page/美国.md "wikilink")，归吴京红所拥有的美国鸿利国际公司\[1\]。

另一家“李先生加州牛肉面馆”的创办人是[重庆人美国加州华侨](../Page/重庆.md "wikilink")[李北祺](../Page/李北祺.md "wikilink")。他在1972年到1974年间以“牛肉面大王”为号，[洛杉矶地区的](../Page/洛杉矶.md "wikilink")[唐人街](../Page/唐人街.md "wikilink")、[蒙特利公園](../Page/蒙特利公園.md "wikilink")、[阿罕布拉开了](../Page/阿罕布拉.md "wikilink")3家“牛肉面大王”餐馆，到1979年扩展到7家。之后在中国大陸以“美国加州牛肉面大王”为商号，开办专营店
\[2\]。

一般認為“美国加州牛肉面”其實就是台灣牛肉麵，台灣牛肉麵傳到美國，之後再有人到其他地方(台灣、中國大陸等)開店，便以“美国加州牛肉麵”為名称。

## 诉讼

在中国“美国加州牛肉面”很多。美国鸿利国际公司成功起诉了[天津](../Page/天津.md "wikilink")“金筷子美国加州牛肉面”，并在2007年开始起诉“李先生加州牛肉面馆”连锁店。同时吴京红的“美国加州牛肉面大王”也因为不是来自美国加州而被律师指出涉嫌欺骗\[3\]。

## 参考文献

## 外部链接

  - [李先生](http://www.mrlee.com.cn/)
  - [加州牛肉面](http://web.archive.org/web/*/http://www.cb-noodleking.com/)
    (Archive)

[J](../Category/牛肉面.md "wikilink")
[Category:中国快餐餐厅](../Category/中国快餐餐厅.md "wikilink")
[Category:面馆](../Category/面馆.md "wikilink")

1.  [加州牛肉面并非出自加州
    被指涉嫌欺骗！](http://news.xinhuanet.com/food/2007-03/13/content_5838644.htm)
    新华网，2007年03月13日，2007年06月07日造访

2.  [“加州牛肉面”称品牌来自加州原料等出自国内](http://www.pp168.com/news/newnews/ppdt/2007/423/07423817583E40D995270KKJ2DK5C0.html)
    中华人民共和国品牌网，2007年06月07日造访

3.