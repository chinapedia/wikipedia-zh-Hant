**加拿大传爱医援会**（Evangelical Medical Aids Society，简称
EMAS）是[加拿大](../Page/加拿大.md "wikilink")[基督教慈善医疗援助协会](../Page/基督教.md "wikilink")，创立于1948年,总部设于加拿大[多伦多](../Page/多伦多.md "wikilink")[密西沙加市](../Page/密西沙加.md "wikilink")。其宗旨是为[发展中国家的贫穷乡村地区提供医疗援助](../Page/发展中国家.md "wikilink")，包括[脑科](../Page/脑科.md "wikilink")[儿科](../Page/儿科.md "wikilink")、[妇产科](../Page/妇产科.md "wikilink")、[牙科等各科门诊与治疗](../Page/牙科.md "wikilink")，建立乡镇医院，建立活水井，培训乡镇医生、[护士](../Page/护士.md "wikilink")、[接生员](../Page/接生员.md "wikilink")、[药剂师等爱心服务](../Page/药剂师.md "wikilink")，另外还开办[英语培训班](../Page/英语.md "wikilink")。

加拿大传爱医援会包括以下14个传爱医援组:

  - 华东华北团：现有27名医务人员，对[江苏省](../Page/江苏省.md "wikilink")[苏州市](../Page/苏州市.md "wikilink")、[淮安县](../Page/淮安县.md "wikilink")，[福建省](../Page/福建省.md "wikilink")[厦门市](../Page/厦门市.md "wikilink")，东北[大连市等地方提供医援](../Page/大连市.md "wikilink")。
  - 华西团：也称EMAS宁夏传爱医援会，团长为[加拿大](../Page/加拿大.md "wikilink")[温哥华马叔和医生](../Page/温哥华.md "wikilink")，团员包括来自加拿大各地区的医生、专科医生、牙医等四十余人。成立后15年来每年都到[银川大学附属医院开办讲座](../Page/银川大学.md "wikilink")、在宁夏卫生局指定乡村，建立自来水工程和地方医院；在宁夏南部乡镇如吴旗县、新寨乡、王洼子等地开门诊。
  - 华南团，为[云南省乡镇提供医援](../Page/云南省.md "wikilink")。
  - [哥斯达黎加团](../Page/哥斯达黎加.md "wikilink")
  - [厄瓜多尔团](../Page/厄瓜多尔.md "wikilink")
  - [加纳团](../Page/加纳.md "wikilink")
  - [海地团](../Page/海地.md "wikilink")
  - [印度团](../Page/印度.md "wikilink")
  - [牙买加团](../Page/牙买加.md "wikilink")
  - [马拉维团](../Page/马拉维.md "wikilink")
  - [尼日利亚团](../Page/尼日利亚.md "wikilink")
  - [菲律宾团](../Page/菲律宾.md "wikilink")
  - [罗马尼亚团](../Page/罗马尼亚.md "wikilink")
  - [乌克兰团](../Page/乌克兰.md "wikilink")

## 外部链接

  - [加拿大传爱医援会](http://www.cmds-emas.ca)
  - [“加拿大医援会传爱行动”15年10万人受益](http://ningxia.cistc.gov.cn/districtmember/browse.asp?id=56471&site=3038&column=3041)
  - [加拿大传爱医援会赴云南省开展医疗服务](http://www.ynredcross.org/Show.aspx?id=262)
  - [杏林传爱在山区](http://xiwen.godblog.cn/user1/410/archives/2005/1037.html)

[Category:加拿大慈善機構](../Category/加拿大慈善機構.md "wikilink")
[Category:加拿大组织](../Category/加拿大组织.md "wikilink")