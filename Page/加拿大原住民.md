**加拿大原住民**，他们是在1982年[宪政法案第](../Page/宪政法案.md "wikilink")25和35节中所认定的原住民族群，分別是[第一民族](../Page/第一民族.md "wikilink")、[因纽特人以及](../Page/因纽特人.md "wikilink")[梅蒂人](../Page/梅蒂人.md "wikilink")。

根据2006年的[人口普查](../Page/人口普查.md "wikilink")，加拿大总人口超过33,570,000人，其中包含3.8%的原住民[人口](../Page/人口.md "wikilink")。这些人中有698,025名[第一民族子嗣](../Page/第一民族.md "wikilink")，389,785名梅蒂斯人和50,485名因纽特人。加拿大的国家代表人员包含[原住民国族联合议会](../Page/原住民国族联合议会.md "wikilink")、[因纽特团结组织](../Page/因纽特团结组织.md "wikilink")、[梅蒂斯部族议会](../Page/梅蒂斯部族议会.md "wikilink")、[加拿大原住民妇女协会](../Page/加拿大原住民妇女协会.md "wikilink")、[全国友谊中心协会和](../Page/全国友谊中心协会.md "wikilink")[原住民族国会](../Page/原住民族国会.md "wikilink")。在做为当地原住民的意见代表上，某些人並不认可他們。那些当地人比较倾向依赖自己的[传统律法以及](../Page/传统律法.md "wikilink")[管理](../Page/管理.md "wikilink")，並且依传统法規行事。

一些原住民宣称他們的[主权从未消失过](../Page/主权.md "wikilink")，并且表明，在1982年加拿大宪政法案第25节提到的1763年[皇家宣言](../Page/皇家宣言.md "wikilink")、英属北美法及加拿大和英国签署的1969年[维也纳公约](../Page/维也纳公约.md "wikilink")，全部支持这一说法。

[皇家原住民委员会是一个在](../Page/皇家原住民委员会.md "wikilink")1990年代，由加拿大政府所掌管的重要委员会。它评判過去政府对于原住民族群的政策，比如[寄宿學校](../Page/加拿大印地安人寄宿學校系統.md "wikilink")，並且为政府提供提关于政策上的建议；然而，大部份皇家原住民族委員會提出的建议到目前為止都沒有被联邦政府应用。

在就业等法之下，原住民族是一個被指定为包含女人、可见的少数民族和弱势族群的团体，但他们在法案下和在[加拿大统计局的观点里并不被认为是一个可见的少数族群](../Page/加拿大统计局.md "wikilink")，因而有相当多的权益受损。

## 原住民語

[Langs_N.Amer.png](https://zh.wikipedia.org/wiki/File:Langs_N.Amer.png "fig:Langs_N.Amer.png")
目前原住民使用超過30種以上不同的語言，大多只在加拿大使用，但是日漸衰微。[奧傑布瓦語和](../Page/奧傑布瓦語.md "wikilink")[克里語](../Page/克里語.md "wikilink")，加起來共有15萬以上的使用者；[伊努克梯圖語](../Page/伊努克梯圖語.md "wikilink")（Inuktitut），在[西北地区](../Page/西北地区_\(加拿大\).md "wikilink")、[努納福特地區](../Page/努納福特.md "wikilink")、[努納維克地方](../Page/努納維克.md "wikilink")（北魁北克）和[努納特西亞烏特地方](../Page/努納特西亞烏特.md "wikilink")（北[拉布拉多](../Page/拉布拉多.md "wikilink")）約有29,000名使用者；以及[密卡茂語](../Page/密卡茂語.md "wikilink")（Mi'kmaq），大約8,500名使用者，多在加拿大東部。

加拿大有兩個地區給予原住民語官方地位。在[努納福特地區](../Page/努納福特.md "wikilink")，伊努克梯圖語（Inuktitut）和Inuinnaqtun語與英文及法文皆是官方語言，而且伊努克梯圖語是政府機關的通用語言。在[西北地区](../Page/西北地区_\(加拿大\).md "wikilink")，官方語言法（Official
Languages
Act）承認11種官方語言：[奇標揚語](../Page/奇標揚語.md "wikilink")（Chipewyan）、[克里語](../Page/克里語.md "wikilink")（Cree）、[哥威迅語](../Page/哥威迅語.md "wikilink")（Gwich'in）、Inuinnaqtun語、伊努克梯圖語（Inuktitut）、Inuvialuktun語、[北斯拉維語](../Page/北斯拉維語.md "wikilink")（North
Slavey）、[南斯拉維語](../Page/南斯拉維語.md "wikilink")（South
Slavey）、[道格瑞普語](../Page/道格瑞普語.md "wikilink")（Tłįchǫ）、[英語和法語](../Page/英語.md "wikilink")，其他語言非政府通用語言；但其官方地位允許公民得要求以這些語言與政府接洽公務。

## 命名爭議

“印第安”這個字雖然在加拿大國會裡是合法的，但它的用法在外頭被認為是冒犯的。這個困惑可以追溯到歐洲探險家[克里斯多福‧哥倫布](../Page/哥倫布.md "wikilink")，他完全堅信了他發現一條到印度的新航路（當年哥倫布以為他到了[東印度群島](../Page/東印度群島.md "wikilink")，所以將島的人命名為印第安，即[印度人之意](../Page/印度人.md "wikilink"))。
它通常被引用於加拿大原住民的自我認同，但是他們從未接受他們的主權權利或者土地所有權的消失。

[第一民族和第一國族曾經是同義字](../Page/第一民族.md "wikilink")，並且偶爾會被美國當地原住民在聲援他們加拿大的親屬時，當作描述性術語來使用。

### 大寫化

政策的使用關於「Aboriginal」這個字的大寫化，從組織到組織間而有所不同。[加拿大印第安北方事務部建議這個名詞應該一直都要被大寫化](../Page/加拿大印第安北方事務部.md "wikilink")，並且只能做為一個形容詞使用，不是名詞。並且在[加拿大英國國會議事錄裡被大寫化](../Page/加拿大英國國會議事錄.md "wikilink")，議會的手鈔本認為它反映了良好的加拿大英文風格。牛津加拿大詞典給大寫的「Aboriginal」作為中心詞詞條──意味著它代表著最普遍的加拿大的用法──而且提供小寫的「Aboriginal」作為一個不同的拼寫。

## 人口

加拿大統計局紀錄在加拿大人口調查中，表態具有原住民身份的人。

| 省/州                                                                                                                                                                                    | 人數        | 人口的百分比 | 全國原住民人口的百分比 |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------- | ------ | ----------- |
| [紐芬蘭和拉布拉多](../Page/紐芬蘭和拉布拉多.md "wikilink")                                                                                                                                             | 23,450    | 5      | 2           |
| [愛德華王子島省](../Page/愛德華王子島省.md "wikilink")                                                                                                                                               | 1,730     | 1      | 0.1         |
| [新斯科舍省](../Page/新斯科舍省.md "wikilink")                                                                                                                                                   | 24,175    | 3      | 2           |
| [新不倫瑞克省](../Page/新不倫瑞克省.md "wikilink")                                                                                                                                                 | 17,655    | 2      | 2           |
| [魁北克省](../Page/魁北克省.md "wikilink")                                                                                                                                                     | 108,430   | 1      | 9           |
| [安大略省](../Page/安大略省.md "wikilink")                                                                                                                                                     | 242,495   | 2      | 21          |
| [馬尼托巴省](../Page/馬尼托巴省.md "wikilink")                                                                                                                                                   | 175,395   | 15     | 15          |
| [薩斯喀徹溫省](../Page/薩斯喀徹溫省.md "wikilink")                                                                                                                                                 | 141,890   | 15     | 12          |
| [亞伯達省](../Page/亞伯達省.md "wikilink")                                                                                                                                                     | 188,365   | 6      | 16          |
| [不列顛哥倫比亞省](../Page/不列顛哥倫比亞省.md "wikilink")                                                                                                                                             | 196,075   | 5      | 17          |
| [育空地區](../Page/育空地區.md "wikilink")                                                                                                                                                     | 7,580     | 25     | 0.6         |
| [西北地區](../Page/西北地區_\(加拿大\).md "wikilink")                                                                                                                                             | 20,635    | 50     | 2           |
| [努納武特](../Page/努納武特.md "wikilink")                                                                                                                                                     | 24,920    | 85     | 2           |
| [加拿大](../Page/加拿大.md "wikilink")                                                                                                                                                       | 1,172,790 | 4      | 100.0       |
| 資料來源：2006年人口普查[1](http://www12.statcan.ca/english/census06/analysis/aboriginal/tables/table2.htm), [2](http://www12.statcan.ca/english/census06/analysis/aboriginal/charts/chart1.htm) |           |        |             |

## 國家原住民節

自從1996年之後，[加拿大全國原住民日](../Page/加拿大全國原住民日.md "wikilink")（6月21日）已成為全國慶祝的正式節日。

## 重要日期

1763年皇室宣言：确定第一民族的权利。

1763-1921：政府与第一民族陆续签定条约（Treaties）以分享土地及资源。

1867年宪法（91条24款）：要求联邦政府保障印地安并划设印地安保留区（Lands Reserved for Indians）。

1876年[印地安法](../Page/印地安法.md "wikilink")（Indian Act）：架构国家与印地安的管理关系。

1982年宪法（35条）：确定原住民族权及条约权（Treaty
Rights）。（所谓「条约权」，包括对于过去以签定条约的承认及未来订定条约的权利）。

補充：最高法院的判决不断重新定义彼此关系的实质内涵，使其更宽广且更具实质内涵（例如以下判决均具有指标性意义：1973年承认原住民族对传统土地的权利、1985年信托责任、1990年原住民权、1998
年原住民命名权、1999年选举权、2000年历史条约的承认等等）。

## 原住民族国

  - [齐佩瓦族国](../Page/齐佩瓦族国.md "wikilink")（Chippewa/Ojibwe/Ashinabe First
    Nation）
  - [克里族国](../Page/克里族国.md "wikilink")（Cree First Nation）
  - [甸尼族国](../Page/甸尼族国.md "wikilink")（Dene First Nation）
  - [海达族国](../Page/海达族国.md "wikilink")（Haida First Nation）
  - [伊努族國](../Page/伊努族國.md "wikilink")（Innu First Nation）
  - [夸奇乌托族国](../Page/夸奇乌托族国.md "wikilink")/[夸夸嘉夸族国](../Page/夸夸嘉夸族国.md "wikilink")（Kwakuitl/Kwakwaka'wakw
    First Nation）
  - [肋筐恩族国](../Page/肋筐恩族国.md "wikilink")/[桑吉斯族國](../Page/桑吉斯族國.md "wikilink")（Lekwungen/Songhees
    First Nation）
  - [卢比肯族国](../Page/卢比肯族国.md "wikilink")（Lubicon First Nation）
  - [密克马克族国](../Page/密克马克族国.md "wikilink")（Micmac First Nation）
  - [摩和克族国](../Page/摩和克族国.md "wikilink")（Mohawk First Nation）
  - [玛斯昆族国](../Page/玛斯昆族国.md "wikilink")（Musqueam First Nation）
  - [尼斯卡国族国](../Page/尼斯卡国族国.md "wikilink")（Nisga'a First Nation）
  - [努特卡族国](../Page/努特卡族国.md "wikilink")（Nootka/Nuu-chah-nulth First
    Nation）
  - [薩利希族國](../Page/薩利希族國.md "wikilink")（Salish First Nation）
  - [蕭尼族國](../Page/蕭尼族國.md "wikilink")（Shwanee First Nation）
  - [苏夸密许族国](../Page/苏夸密许族国.md "wikilink")（Squamish First Nation）
  - [特林基特族国](../Page/特林基特族国.md "wikilink")（Tlingit First Nation）

## 知名原住民

  - [基努·里维斯](../Page/基努·里维斯.md "wikilink") 夏威夷人 演员

  - [仙妮亞·唐恩](../Page/仙妮亞·唐恩.md "wikilink") 齐佩瓦人 歌手

  - 克里人 歌手

  - 撒利希人 演员

  - 蕭尼人 军人

  - [路易斯·瑞尔](../Page/路易斯·瑞尔.md "wikilink") 梅蒂人(Metis) 政治家

  - 克里人 政治家

  - [格雷厄姆·格林](../Page/格雷厄姆·格林.md "wikilink") 摩和克人 演员

  - 伊努人 音乐家

  - 甸尼人 歌手

  - 海达人 艺术家

  - 甸尼人 政治家

## 參見

  - [The Canadian Crown and First Nations, Inuit and
    Métis](http://en.wikipedia.org/wiki/The_Canadian_Crown_and_First_Nations,_Inuit_and_M%C3%A9tis)

## 參考

  - Katherine Barber, editor (2004). *The Canadian Oxford Dictionary,
    Second Edition.* Toronto, Oxford University Press. ISBN
    0-19-541816-6.

## 外部連結

  - [The Canadian Museum of Civilization - First Peoples
    Section](https://web.archive.org/web/20071009061924/http://www.civilisations.ca/aborig/aborige.asp)
  - [Aboriginal Canada
    Portal](https://web.archive.org/web/20100208214231/http://www.aboriginalcanada.gc.ca/acp/site.nsf/en/index.html)
  - [Comprehensive Claims (Modern Treaties) in Canada - Indian and
    Northern Affairs
    Canada](https://web.archive.org/web/20080622024346/http://www.ainc-inac.gc.ca/pr/info/trty_e.html)
  - [*Aboriginal Perspectives* educational Web
    site](https://web.archive.org/web/20080730212028/http://nfb.ca/enclasse/doclens/visau/index.php?language=english)
  - [Report of the Royal Commission on Aboriginal
    Peoples](http://www.ainc-inac.gc.ca/ch/rcap/sg/sgmm_e.html)
  - [Naming
    guidelines](http://www.ainc-inac.gc.ca/pr/pub/wf/index_e.html) of
    the [Government of
    Canada](http://en.wikipedia.org/wiki/Government_of_Canada)'s
    [Department of Indian and Northern
    Affairs](http://en.wikipedia.org/wiki/Department_of_Indian_and_Northern_Affairs_\(Canada\))
  - [Map of historical territory treaties with Aboriginal peoples in
    Canada](https://web.archive.org/web/20060422194149/http://atlas.gc.ca/site/english/maps/historical/indiantreaties)
  - [CBC Digital Archives - The Battle for Aboriginal Treaty
    Rights](http://archives.cbc.ca/IDD-1-73-1238/politics_economy/aboriginal_treaty_rights/)
  - Collection of Historical Images of the Canadian
    [kwakiutl](https://web.archive.org/web/20061107080817/http://www.picture-history.com/kwakiutl-index-001.htm)
    Natives
  - [A History of Aboriginal Treaties and Relations in
    Canada](http://www.canadiana.org/citm/themes/aboriginals_e.html)
    This site includes links to digitized primary sources and summaries
    of primary source documents, such as treaties.
  - [Chart of Aboriginal population according to their percentage of the
    total population in Canada, provinces and
    territories](http://www12.statcan.ca/english/census01/products/analytic/companion/abor/charts/abshare.cfm)
    - 2001 Census, Statistics Canada
  - [First Nations Studies
    Essays](https://web.archive.org/web/20080225231505/http://www.egwald.com/ubcstudent/aboriginal/)
  - [First Nations Seeker](http://www.firstnationsseeker.ca/)
  - [Aboriginal Virtual Exhibits from Canadian
    Museums](https://web.archive.org/web/20080926120049/http://www.virtualmuseum.ca/AboriginalArtCultureandTraditions.html)

[加拿大原住民](../Category/加拿大原住民.md "wikilink")
[Category:原住民政治](../Category/原住民政治.md "wikilink")