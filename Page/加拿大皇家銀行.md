[Royal_Bank_of_Canada_in_Fairview_Broadway_2018.jpg](https://zh.wikipedia.org/wiki/File:Royal_Bank_of_Canada_in_Fairview_Broadway_2018.jpg "fig:Royal_Bank_of_Canada_in_Fairview_Broadway_2018.jpg")Fairview的分行\]\]
**加拿大皇家銀行**（[英文](../Page/英文.md "wikilink")：**Royal Bank of
Canada**，[法文](../Page/法文.md "wikilink")：**Banque Royale du
Canada**，，）是[加拿大最大的](../Page/加拿大.md "wikilink")[銀行](../Page/銀行.md "wikilink")\[1\]。截至2014年，根据[市值规模排名世界第](../Page/市值.md "wikilink")15大银行\[2\]。

## 历史

加拿大皇家銀行始創於1864年，於[哈利法克斯開業](../Page/哈利法克斯.md "wikilink")，初時的名稱為Merchants'
Bank，於1901年易名並沿用至今，时當地加拿大華僑稱之为**賚路銀行**\[3\]\[4\]。1907年，該行總部遷往[蒙特婁](../Page/蒙特婁.md "wikilink")。

2011年6月20日，PNC金融服務集團20日發表聲明說，已與加拿大皇家銀行達成協議，將出資34.5億美元收購後者在美國的零售銀行業務。
加拿大皇家銀行（美國）的總部位於北卡羅來納州，資產大約為250億美元。該銀行在美國6個州擁有424家分行。在交易完成後，加上PNC金融服務集團現有的服務網絡，該銀行的分行數量將達到2870家，屆時將成為美國第五大銀行。

## 经营状况

現時加拿大皇家銀行總部位於[蒙特利尔的玛丽城中心](../Page/蒙特利尔.md "wikilink")，运营中心位于[多伦多](../Page/多伦多.md "wikilink")，有1400間分行，遍佈[北美和全球](../Page/北美.md "wikilink")30多個國家，客戶多達1200多萬名。
加拿大皇家银行亦拥有世界排名第12的投资银行，。

## 参见

  - [道明加拿大信託銀行](../Page/道明加拿大信託銀行.md "wikilink")
  - [滿地可銀行](../Page/滿地可銀行.md "wikilink")
  - [加拿大豐業銀行](../Page/加拿大豐業銀行.md "wikilink")
  - [加拿大中央銀行](../Page/加拿大中央銀行.md "wikilink")

## 備註及參考文獻

<references/>

## 外部連結

  - [加拿大皇家銀行](http://www.rbcroyalbank.com/)

  - [加拿大皇家銀行中文網站](http://www.rbc.com/chinese/canada/)

[Category:加拿大銀行](../Category/加拿大銀行.md "wikilink")
[Category:1864年成立的公司](../Category/1864年成立的公司.md "wikilink")

1.  <http://www.rbc.com/investorrelations/pdf/ar_2009_e.pdf> RBC Annual
    Report 2009
2.
3.  [1929年5月10日加拿大大漢公報第一頁左下角【賚路銀行】廣告](http://www.multiculturalcanada.ca/node/114705)

4.  [2019年3月加拿大SFU大學圖書舘電子存檔的1929年5月10日加拿大大漢公報第一頁左下角【賚路銀行】廣告](http://chinesetimes.lib.sfu.ca/ctimes-35512/page-1)