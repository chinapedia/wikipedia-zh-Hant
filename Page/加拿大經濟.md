|                    <big>[加拿大經濟](../Page/加拿大.md "wikilink")</big>                     |
| :----------------------------------------------------------------------------------: |
| [200px](../Page/file:flag_of_Canada.svg.md "wikilink"){{Photomontage|position=center |
|                                          通貨                                          |
|                                         財政年度                                         |
|                                         貿易組織                                         |
|         統計[1](http://www.cia.gov/cia/publications/factbook/geos/ks.html#top)         |
|                          [GDP排名](../Page/GDP.md "wikilink")                          |
|                                         GDP                                          |
|                                        GDP增長                                         |
|                                        人均GDP                                         |
|                                       各產業GDP比重                                       |
|                           [通脹率](../Page/通脹.md "wikilink")                            |
|                         [貧窮線下人口](../Page/貧窮線.md "wikilink")                          |
|                                         勞動人口                                         |
|                                       各產業勞動人口                                        |
|                           [失業率](../Page/失業.md "wikilink")                            |
|                                         主要工業                                         |
|  貿易夥伴 [2](https://www.cia.gov/library/publications/the-world-factbook/geos/ja.html)  |
|                                          出口                                          |
|                                         主要夥伴                                         |
|                                          入口                                          |
|                                         主要夥伴                                         |
|  公共財政 [3](https://www.cia.gov/library/publications/the-world-factbook/geos/ja.html)  |

**[加拿大](../Page/加拿大.md "wikilink")**是世界最富裕的國家之一，為世界第七大經濟體。為[OECD和](../Page/OECD.md "wikilink")[G8成員](../Page/G8.md "wikilink")。加拿大經濟以服務業為主，全國民眾有四分之三都從事服務業，在先進國家中位居前列。制造業則主要集中在東部地區。[美國是加拿大最大的貿易伙伴](../Page/美國.md "wikilink")。

在[美國傳統基金會的](../Page/美國傳統基金會.md "wikilink")[經濟自由度指數排名中](../Page/經濟自由度指數.md "wikilink")，加拿大排名第7。高于大部分西歐國家，但低于美國。加拿大的[人類發展指數亦居世界前列](../Page/人類發展指數.md "wikilink")。

## 產業

加拿大的[石油行業一直是經濟增長的主要動力](../Page/石油.md "wikilink")，推動加國貿易轉虧為盈，並有很大量的投資。跟很多先進經濟體一樣，精煉石油產品的國內需求平穩，但原油生產商仍可透過加東的[煉油廠](../Page/煉油廠.md "wikilink")，擴大加國境內的市場。加拿大可說是原油淨出口國，但東部省份的煉油廠向來依賴[進口石油](../Page/進口.md "wikilink")，因為在全國各地運送加西原油的成本更高。儘管美國增產[頁岩油然而由於加國接近美國](../Page/頁岩油.md "wikilink")，並有龐大的管道網絡，對[美國的出口仍有增長](../Page/美國.md "wikilink")，2014年原油佔加國貨物出口約18%。\[1\]

加拿大聯邦和各省政府經營多種國際[保險業務成為經濟一大亮點](../Page/保險.md "wikilink")，包括出口信用保險和投資保險。早在1945年制定、頒佈了出口信用保險法；在1947年建立了出口信用保險公司，保險監管採取的是兩級監管的模式，即聯邦和省兩級，保護加拿大本國的海外投資者的投資資本、投資收入，彌補因各種商業和政治風險造成的出口收匯和[資本及收益的損失](../Page/資本.md "wikilink")，有力地促進了加拿大國際[貿易發展和本國保險人在國際市場上的競爭地位](../Page/貿易.md "wikilink")。保險業資產位居加拿大金融業第二位。目前加拿大非壽險公司有近400家。\[2\]\[3\]

农业食品业是加拿大经济重要的组成部分，占其国内生产总值的8%。加拥有耕地面积4600万公顷，主要在西部，占国土总面积的5%，產物主要有：小麦、[燕麦](../Page/燕麦.md "wikilink")、大豆、油菜籽、[大麦](../Page/大麦.md "wikilink")、红肉类（牛、猪和羊）、水果、[蔬菜](../Page/蔬菜.md "wikilink")、酒类、[烟草](../Page/烟草.md "wikilink")、饮料等，出口美國占大宗，約占总量的60%。安大略省和[魁北克省主产红肉类和乳制品](../Page/魁北克.md "wikilink")。

## 参考文献

  - [北美自由貿易協議](../Page/北美自由貿易協議.md "wikilink")

[\*](../Category/加拿大經濟.md "wikilink")

1.  "Canada vs. The OECD: An Environmental Comparison".
    Environmentalindicators.com. Retrieved 2011-02-22.
2.  沈婷著.國際保險.格致出版社,2010.04.
3.  FTDWebMaster, \[Name of person creating HTML\]. "FTD - Statistics -
    Trade Highlights - Top Trading Partners". Census.gov. Retrieved
    2011-02-22.