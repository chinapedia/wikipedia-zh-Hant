[Air_Canada_HQ.jpg](https://zh.wikipedia.org/wiki/File:Air_Canada_HQ.jpg "fig:Air_Canada_HQ.jpg")

**加拿大航空**（[英語](../Page/英語.md "wikilink")、[法語](../Page/法語.md "wikilink")：****，****，简称**加航**，中文前稱“楓葉航空”）是[加拿大的](../Page/加拿大.md "wikilink")[國家航空公司](../Page/國家航空公司.md "wikilink")，總部設在[加拿大](../Page/加拿大.md "wikilink")[魁北克省](../Page/魁北克省.md "wikilink")[蒙特利尔](../Page/蒙特利尔.md "wikilink")。加拿大航空提供客運及貨運定期及包機服務，到超過240個目的地、90個渡假目的地。亦為其他航空公司提供維修、地勤及訓練服務。加拿大航空的主要航空樞紐在[多倫多國際機場](../Page/多倫多國際機場.md "wikilink")，在[蒙特利爾特鲁多國際機場及](../Page/蒙特利爾特鲁多國際機場.md "wikilink")[溫哥華國際機場亦設有樞紐](../Page/溫哥華國際機場.md "wikilink")。

## 歷史

加拿大航空的前身——環加拿大航空（Trans-Canada Air
Lines），創立於1937年4月10日，同年9月1日以[洛歇10A進行首次飛航](../Page/洛歇10A.md "wikilink")，由[溫哥華飛往](../Page/溫哥華.md "wikilink")[西雅圖](../Page/西雅圖.md "wikilink")，並載有兩名乘客與郵件。其後環加拿大航空發展成加拿大的國家航空公司。1964年，當時未成為[首相的](../Page/首相.md "wikilink")[克雷蒂安在國會要求把航空公司名稱易名為Air](../Page/克雷蒂安.md "wikilink")
Canada，1965年1月1日，正式易名Air Canada。1970年代末，Air
Canada成為獨立的[國營企業](../Page/國營企業.md "wikilink")（Crown
corporation）。

1989年，Air Canada完成[私有化](../Page/私有化.md "wikilink")。由於Air
Canada把業務重點放在國內線、[歐洲線和](../Page/歐洲.md "wikilink")[美國線](../Page/美國.md "wikilink")，故一直沒有正式中文名稱，直至1994年，Air
Canada開通溫哥華—[香港航線](../Page/香港.md "wikilink")，始注冊中文名。惟當時“加拿大航空”一名已被早在1950年代開通香港經[東京往溫哥華線的競爭對手](../Page/東京.md "wikilink")**Canadian
Airlines**（另譯名[加拿大國際航空](../Page/加拿大國際航空.md "wikilink")）注冊，因此Air
Canada把中文名定為“楓葉航空”。

1997年5月，楓葉航空成為[星空聯盟創始成員](../Page/星空聯盟.md "wikilink")。1998年9月2日，楓葉航空[機師發起首次](../Page/機師.md "wikilink")[罷工行動](../Page/罷工.md "wikilink")。

2000年1月，楓葉航空收購破產的加拿大航空（Canadian Airlines）並宣佈合併，成為世界第12大航空公司。

2003年4月1日，加拿大航空申請破產保護，其間擁有加拿大國籍的香港商人[李澤鉅曾作深入的獨家拯救收購談判](../Page/李澤鉅.md "wikilink")，後李因無法與[工會達成協議而退出](../Page/工會.md "wikilink")。2004年9月30日撤銷破產保護，改組後ACE航空控股（ACE
Aviation Holdings）成為加拿大航空的[母公司](../Page/母公司.md "wikilink")。

## 現代化

加航於2004年10月19日公佈全新的飛機塗裝及前線人員的制服。在新聞發佈會上，一架[波音767-300客機被漆上了全新青藍色塗裝](../Page/波音767.md "wikilink")，加上新的字體，及把原來墨綠色的機尾塗裝改成青藍色加上紅點襯拓的[楓葉標誌](../Page/楓葉.md "wikilink")。

於2005年11月9日，加航與[波音公司簽署翻新其](../Page/波音公司.md "wikilink")[寬體客機的同意書](../Page/寬體客機.md "wikilink")，當中條件包括購買18架[波音777](../Page/波音777.md "wikilink")（其中10架為-300ER型，6架-200LR型，2架貨機），與14架[波音787-8](../Page/波音787.md "wikilink")。加航亦表示有另外18架波音777及46架波音787-8和787-9的額外訂購權。[1](http://www.sedar.com/GetFile.do?lang=EN&docClass=13&issuerNo=00020954&fileName=/csfsprod/data63/filings/00880502/00000001/x%3A%5CSedar%5C2006%5CACEAviation%5C18jan%5CAIRCANADA.pdf)
以上的波音777將全部配置[通用電氣](../Page/通用電氣.md "wikilink")[GE90-115B引擎](../Page/通用电气GE90发动机.md "wikilink")，而787-8則配置[通用電氣GEnx引擎](../Page/通用电气GEnx发动机.md "wikilink")。[2](http://www.geae.com/aboutgeae/presscenter/genx/genx_20051109.html)
首架波音777於2007年3月交付加航，波音787則最初預期於2010年交付，實際上首架787于2014年5月交付。加航計劃以波音777取代現有的[空中巴士A340機隊](../Page/空中巴士A340.md "wikilink")，而波音787將取代波音767及[空中巴士A330機隊](../Page/空中巴士A330.md "wikilink")。[3](https://web.archive.org/web/20091007204352/http://www.boeing.com/news/releases/2005/q4/nr_051109g.html)
加航接收波音777-200超長程型後，隨即於2007年8月6日起，於往來[香港至](../Page/香港.md "wikilink")[多倫多的不停站直航航班AC](../Page/多倫多.md "wikilink")15/16服役，取代原有的A340-500，成為目前唯一一條採用波音777-200超長程型客機往來香港的航空公司。

由於燃油價格高企，加航於2005年11月試行減輕飛機重量以節省燃油開支。曾把一架波音767-200客機去除機身塗裝並加以打磨，使其有反光的金屬機身。但此計劃其後遭擱置，原因是打磨機身的成本比燃油更貴，再者全金屬銀色的機身塗裝被認為不夠美觀。

加航於2007年4月29日再訂購更多波音787客機，落實訂單總數為60架，使加航將成為北美洲最大及全球第二最大的波音787營運者（繼澳洲航空公司之後）。同時，加航亦取消原有的2架波音777貨機訂單，使其波音777訂單總數為16架，並有另外18架的購買權。[4](http://active.boeing.com/commercial/orders/index.cfm?content=displaystandardreport.cfm&pageid=m25064&RequestTimeout=20000)

2014年5月18日，加拿大航空公司接收其首架波音787型飛機（787-8）\[1\]，并表示在2014年年底之前將會有六架全新787加入機隊。加拿大航空公司的第一架波音787飛機于2014年五月二十三日開始執行多伦多至哈利法克斯（Halifax,
Nova
Scotia）的國內航線，接下來還將實驗性的投入到一些跨大西洋航線中，在大約一個月的磨合期之後，將投入到熱門國際航線中，例如多倫多–[東京羽田](../Page/東京羽田機場.md "wikilink")、多倫多-[特拉維夫等](../Page/特拉維夫.md "wikilink")。

短程及地區型飛機方面，加航已陸續接收已訂購的[巴西航空工業的ERJ](../Page/巴西航空工業.md "wikilink")-175及ERJ-190型客機，並擁有60架ERJ-190型客機的購買權。這些飛機用以飛行加拿大國內及往來美國之航班。ERJ-190型飛機將取代部份較舊的[空中巴士A319及](../Page/空中巴士A319.md "wikilink")[空中巴士A320型](../Page/空中巴士A320.md "wikilink")。

2017年2月開始，加航開始陸續為其所有機型啟用全新涂装，在全新的涂装中，機尾換回了原來的墨綠色，其中点綴着加航的楓葉圓圈標誌，機身則改為白色，且在機身前部的舷窗上方標示2004年啟用的Air
Canada字樣，而原本在字樣左側的楓葉圓圈標誌則移至下方。

## 機隊

[C-GHPQ_(38497035285).jpg](https://zh.wikipedia.org/wiki/File:C-GHPQ_\(38497035285\).jpg "fig:C-GHPQ_(38497035285).jpg")客機\]\]
[C-FNND@HKG_(20181006132028).jpg](https://zh.wikipedia.org/wiki/File:C-FNND@HKG_\(20181006132028\).jpg "fig:C-FNND@HKG_(20181006132028).jpg")客機\]\]
[Air.canada.b767-300.c-ggfj.arp.jpg](https://zh.wikipedia.org/wiki/File:Air.canada.b767-300.c-ggfj.arp.jpg "fig:Air.canada.b767-300.c-ggfj.arp.jpg")客機\]\]
[Air_Canada_Airbus_A340-300_(C-FYKZ)_touching_down.jpg](https://zh.wikipedia.org/wiki/File:Air_Canada_Airbus_A340-300_\(C-FYKZ\)_touching_down.jpg "fig:Air_Canada_Airbus_A340-300_(C-FYKZ)_touching_down.jpg")客機\]\]
[Air_Canada_Boeing_787_C-GHPT.jpg](https://zh.wikipedia.org/wiki/File:Air_Canada_Boeing_787_C-GHPT.jpg "fig:Air_Canada_Boeing_787_C-GHPT.jpg")客機\]\]
[Air_Canada_Boeing_747-400_KvW.jpg](https://zh.wikipedia.org/wiki/File:Air_Canada_Boeing_747-400_KvW.jpg "fig:Air_Canada_Boeing_747-400_KvW.jpg")客機\]\]
[Air_Canada_Boeing_727-233F_C-GAAL_02.jpg](https://zh.wikipedia.org/wiki/File:Air_Canada_Boeing_727-233F_C-GAAL_02.jpg "fig:Air_Canada_Boeing_727-233F_C-GAAL_02.jpg")客機\]\]
[Air_Canada_Boeing_747-233_C-GAGA_07.jpg](https://zh.wikipedia.org/wiki/File:Air_Canada_Boeing_747-233_C-GAGA_07.jpg "fig:Air_Canada_Boeing_747-233_C-GAGA_07.jpg")客機\]\]
[EnRoute_System_on_B787-8.JPG](https://zh.wikipedia.org/wiki/File:EnRoute_System_on_B787-8.JPG "fig:EnRoute_System_on_B787-8.JPG")
[AC_Inflight_Meal-1.JPG](https://zh.wikipedia.org/wiki/File:AC_Inflight_Meal-1.JPG "fig:AC_Inflight_Meal-1.JPG")\]\]
截至2018年3月，加拿大航空的機隊平均機齡為13.7年\[2\]，拥有180架飞机\[3\]\[4\]，詳情如下：

<center>

| <span style="color:black;">机型                                         | <span style="color:black;">数量  | <span style="color:black;">预定权                        | <span style="color:black;">载客量                           | <span style="color:black;">备注                         |
| --------------------------------------------------------------------- | ------------------------------ | ----------------------------------------------------- | -------------------------------------------------------- | ----------------------------------------------------- |
| <span style="color:black;">所有布局                                       | <span style="color:black;">各布局 | <span style="color:black;"><abbr title="商务舱">J</abbr> | <span style="color:black;"><abbr title="超级经济舱">PY</abbr> | <span style="color:black;"><abbr title="经济舱">Y</abbr> |
| [空中巴士A220-300](../Page/空中巴士A220.md "wikilink")                        | －                              | 45                                                    | 有待确定                                                     | 將於2019年交付                                             |
| [空中巴士A319-113/114](../Page/空中巴士A320.md "wikilink")                    | 18                             | —                                                     | 14                                                       | —                                                     |
| [空中巴士A320-211/214](../Page/空中巴士A320.md "wikilink")                    | 42 \[5\] \[6\] \[7\]           | —                                                     | 14                                                       | —                                                     |
| [空中巴士A321-211](../Page/空中巴士A320.md "wikilink")                        | 15                             | —                                                     | 16                                                       | —                                                     |
| [空中巴士A330-343X](../Page/空中巴士A330.md "wikilink")                       | 8                              | —                                                     | 27                                                       | 21                                                    |
| [波音737MAX 8](../Page/波音737MAX.md "wikilink")                          | 10                             | 40                                                    | 16                                                       | —                                                     |
| [波音737MAX 9](../Page/波音737MAX.md "wikilink")                          | —                              | 11                                                    | 有待确定                                                     |                                                       |
| [波音767-333ER/375ER/38EER](../Page/波音767.md "wikilink")                | 7                              | —                                                     | 24                                                       | —                                                     |
| [波音777-233LR](../Page/波音777.md "wikilink")                            | 6                              | —                                                     | 40                                                       | 24                                                    |
| [波音777-333ER](../Page/波音777.md "wikilink")                            | 19                             | 11                                                    | —                                                        | 40                                                    |
| 8                                                                     | —                              | 28                                                    | 24                                                       | 398                                                   |
| [波音787-8](../Page/波音787.md "wikilink")                                | 8                              | 0\[8\]                                                | 20                                                       | 21                                                    |
| [波音787-9](../Page/波音787.md "wikilink")                                | 25                             | 4\[9\]                                                | 30                                                       | 21                                                    |
| [Embraer 190](../Page/Embraer-170系列.md "wikilink")                    | 25                             | —                                                     | 9                                                        | —                                                     |
| 主线                                                                    | 183                            | 100                                                   |                                                          |                                                       |
| <span style="color:#df0000;">[加拿大快运航空](../Page/加拿大快运航空.md "wikilink") |                                |                                                       |                                                          |                                                       |
| [Embraer 175](../Page/Embraer-170系列.md "wikilink")                    | 25                             | —                                                     | 12                                                       | —                                                     |
| [CRJ900](../Page/龐巴迪CRJ.md "wikilink")                                | 21                             | —                                                     | 12                                                       | —                                                     |
| [CRJ100/200](../Page/龐巴迪CRJ.md "wikilink")                            | 24                             | —                                                     | —                                                        | —                                                     |
| [Q400](../Page/龐巴迪Dash_8.md "wikilink")                               | 44                             | —                                                     | —                                                        | —                                                     |
| [Q300](../Page/龐巴迪Dash_8.md "wikilink")                               | 26                             | —                                                     | —                                                        | —                                                     |
| [Q100](../Page/龐巴迪Dash_8.md "wikilink")                               | 16                             | —                                                     | —                                                        | —                                                     |
| Express                                                               | 156                            | 0                                                     |                                                          |                                                       |
| <span style="color:#df0000;">[加拿大胭脂航空](../Page/加拿大胭脂航空.md "wikilink") |                                |                                                       |                                                          |                                                       |
| [空中巴士A319-100](../Page/空中巴士A320.md "wikilink")                        | 20                             | —                                                     | 12                                                       | —                                                     |
| [空中巴士A321-200](../Page/空中巴士A320.md "wikilink")                        | 5                              | —                                                     | 16                                                       | —                                                     |
| [波音767-33AER](../Page/波音767.md "wikilink")                            | 25                             | —                                                     | 24                                                       | —                                                     |
| rouge                                                                 | 50                             | 0                                                     |                                                          |                                                       |
| 总数                                                                    | 389                            | 100                                                   |                                                          |                                                       |

**加拿大航空机队**

</center>

### 退役机型

<center>

<table>
<tbody>
<tr class="odd">
<td><table>
<caption><strong>加拿大航空退役机型</strong></caption>
<thead>
<tr class="header">
<th><p><strong><span style="color:black;">机型</strong></p></th>
<th><p><strong><span style="color:black;">年份</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/道格拉斯DC-8.md" title="wikilink">道格拉斯DC-8-40/50/60/70</a></p></td>
<td><p>1960-1983[10]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/道格拉斯DC-9.md" title="wikilink">道格拉斯DC-9-10/30</a></p></td>
<td><p>1966-2002[11]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音747.md" title="wikilink">波音747-133</a></p></td>
<td><p>1971-1998[12]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/洛克希德L-1011.md" title="wikilink">洛克希德L-1011-1/15/100/500</a></p></td>
<td><p>1973-1996[13]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音727.md" title="wikilink">波音727-233</a></p></td>
<td><p>1974-1992[14]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音747.md" title="wikilink">波音747-233BM/238BM</a></p></td>
<td><p>1975-1999[15]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音737.md" title="wikilink">波音737-275</a></p></td>
<td><p>2001-2004[16]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音767.md" title="wikilink">波音767-233/233ER/209ER</a></p></td>
<td><p>1983-2008[17]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/福克F28.md" title="wikilink">福克F28</a></p></td>
<td><p>1986-2004[18]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BAe_146.md" title="wikilink">英国航太146-200</a></p></td>
<td><p>1990-2005[19]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音747-475.md" title="wikilink">波音747-475</a></p></td>
<td><p>2001-2003[20]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音747.md" title="wikilink">波音747-433M</a></p></td>
<td><p>1990-2004[21]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/空中巴士A340.md" title="wikilink">空中巴士A340-313X</a></p></td>
<td><p>1995-2008[22]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/道格拉斯DC-10.md" title="wikilink">道格拉斯DC-10-30</a></p></td>
<td><p>2001[23]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/空中巴士A340.md" title="wikilink">空中巴士A340-541</a></p></td>
<td><p>2004-2007[24]</p></td>
</tr>
</tbody>
</table></td>
<td><table>
<caption><strong>加拿大航空退役机型</strong></caption>
<thead>
<tr class="header">
<th><p><strong><span style="color:black;">机型</strong></p></th>
<th><p><strong><span style="color:black;">年份</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/斯蒂尔曼4.md" title="wikilink">斯蒂尔曼4-EM Senior Speedmail</a></p></td>
<td><p>1937-1939[25]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/洛克希德Model_10_Electra.md" title="wikilink">洛克希德Model 10 Electra</a></p></td>
<td><p>1937-1941[26]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/洛克希德Model_14_Super_Electra.md" title="wikilink">洛克希德Model 14 Super Electra</a></p></td>
<td><p>1941-1949[27]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/洛克希德Lodestar.md" title="wikilink">Lockheed Model 18 Lodestar</a></p></td>
<td><p>1941-1949[28]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Avro_Lancastrian.md" title="wikilink">Avro Lancastrian</a></p></td>
<td><p>1943-1947[29]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/道格拉斯DC-3.md" title="wikilink">道格拉斯DC-3</a></p></td>
<td><p>1945-1963[30]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Canadair_North_Star.md" title="wikilink">Canadair North Star</a></p></td>
<td><p>1946-1961[31]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Bristol_Freighter.md" title="wikilink">Bristol Freighter</a></p></td>
<td><p>1953-1955[32]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/洛克希德L-1049_Super_Constellation.md" title="wikilink">洛克希德L-1049 Super Constellation</a></p></td>
<td><p>1954-1963[33]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/维克斯子爵.md" title="wikilink">维克斯子爵</a></p></td>
<td><p>1955-1974[34]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/维克斯Vanguard.md" title="wikilink">维克斯Vanguard</a></p></td>
<td><p>1961-1972[35]</p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

</center>

## 航点

[Air_Canada_Destinations.svg](https://zh.wikipedia.org/wiki/File:Air_Canada_Destinations.svg "fig:Air_Canada_Destinations.svg")
加拿大航空拥有21个国内航点和全球33个国家81个国际航点（不包括非洲）。\[36\]

加航为[星空联盟創始成員](../Page/星空联盟.md "wikilink")，并已经与以下航空公司签订了[代码共享](../Page/代码共享.md "wikilink")：


  - [中国国际航空](../Page/中国国际航空.md "wikilink") \*
  - [新西兰航空](../Page/新西兰航空.md "wikilink") \*
  - [全日空](../Page/全日空.md "wikilink") \*
  - [韩亚航空](../Page/韩亚航空.md "wikilink") \*
  - [联合航空](../Page/联合航空.md "wikilink") \*

|

  - [奥地利航空](../Page/奥地利航空.md "wikilink") \*
  - [哥伦比亚航空](../Page/哥伦比亚航空.md "wikilink") \*
  - [布鲁塞尔航空](../Page/布鲁塞尔航空.md "wikilink") \*
  - [埃及航空](../Page/埃及航空.md "wikilink") \*
  - [哥伦比亚航空](../Page/哥伦比亚航空.md "wikilink") \*

|

  - [TAP葡萄牙航空](../Page/TAP葡萄牙航空.md "wikilink") \*
  - [波兰航空](../Page/波兰航空.md "wikilink") \*
  - [汉莎航空](../Page/汉莎航空.md "wikilink") \*
  - [瑞士国际航空](../Page/瑞士国际航空.md "wikilink") \*
  - [北欧航空](../Page/北欧航空.md "wikilink") \*

|

  - [南非航空](../Page/南非航空.md "wikilink") \*
  - [土耳其航空](../Page/土耳其航空.md "wikilink") \* \[37\]
  - [泰国国际航空](../Page/泰国国际航空.md "wikilink") \*
  - [捷特航空](../Page/捷特航空.md "wikilink")
  - [阿提哈德航空](../Page/阿提哈德航空.md "wikilink") \[38\]

|

  - [長榮航空](../Page/長榮航空.md "wikilink") \*

|

  - [中东航空](../Page/中东航空.md "wikilink")（[天合联盟](../Page/天合联盟.md "wikilink")）
  - [LATAM巴西航空](../Page/LATAM巴西航空.md "wikilink")（[寰宇一家](../Page/寰宇一家.md "wikilink")）
  - [斯里兰卡航空](../Page/斯里兰卡航空.md "wikilink")（寰宇一家）
  - [國泰航空](../Page/國泰航空.md "wikilink")\[39\]（寰宇一家）
  - [國泰港龍航空](../Page/國泰港龍航空.md "wikilink")（寰宇一家）

**\***：星空联盟成员航空公司

## 意外事件

1978年6月26日，加拿大一架[DC-9在执行](../Page/DC-9.md "wikilink")[加拿大航空189号班机时](../Page/加拿大航空189号班机空难.md "wikilink")，于[多伦多皮尔逊国际机场降落时冲出跑道并坠毁在一个深沟中](../Page/多伦多皮尔逊国际机场.md "wikilink")，导致2名乘客死亡。

1983年6月2日，[加拿大航空797號班機由一架](../Page/加拿大航空797號班機.md "wikilink")[DC-9客機執行任務](../Page/DC-9.md "wikilink")，從[美國](../Page/美國.md "wikilink")[達拉斯前往](../Page/達拉斯.md "wikilink")[多倫多途中](../Page/多倫多.md "wikilink")，機艙洗手間突然冒煙。機組人員曾嘗試滅火，但濃煙越來越多。機長決定緊急降落[辛辛那提機場](../Page/辛辛那提.md "wikilink")。飛機降落後不久火勢一發不可收拾，當機上46名乘客及機員疏散了一半時，機艙突然發生[閃燃](../Page/閃燃.md "wikilink")。結果這次事件共造成23人死亡，當中包括加拿大新晉歌手[史丹‧羅傑斯](../Page/史丹‧羅傑斯.md "wikilink")（Stan
Rogers）。經此事件後，全球所有民航客機的洗手間都強制安裝煙霧感應器，及全面改良疏散通道的照明系統，以便在機艙漆黑一片時，乘客仍可找到緊急出口逃生。

1983年7月23日，[加拿大航空143號班機由](../Page/加拿大航空143號班機.md "wikilink")[滿地可經](../Page/滿地可.md "wikilink")[渥太華至](../Page/渥太華.md "wikilink")[愛民頓的一架](../Page/愛民頓.md "wikilink")[波音767-200途中燃料耗盡](../Page/波音767.md "wikilink")，最後飛機以滑翔方式安全降落在[曼尼托巴省吉姆利](../Page/曼尼托巴省.md "wikilink")（Gimli）一個被改為賽車場的前空軍基地內，無人傷亡。肇事客機於修復後繼續復役，並被命名為「基米尼滑翔機」。

2015年3月29日，一架由多伦多飞往[哈利法克斯](../Page/哈利法克斯.md "wikilink")，由一架[空中客车A320-200值飞的](../Page/空中客车A320.md "wikilink")[624号航班在哈利法克斯机场着陆时冲出跑道](../Page/加拿大航空624号班机事故.md "wikilink")。所幸无人死亡或重伤。\[40\]

2017年7月7日，[加拿大航空759號班機的](../Page/加拿大航空759号班机降落失误事件.md "wikilink")[空中客车A320-200在](../Page/空中客车A320.md "wikilink")[三藩市國際機場降落时](../Page/三藩市國際機場.md "wikilink")，錯誤地對準了28R跑道右邊的滑行道，最终由于控制塔及時下令飛行員重飞，才及时避免了同时与四架等待起飞的客机连环相撞的事故，据悉该加航飞机上共有140人，其余四架飞机每架共计200至480人不等。如果此次空难发生，将直接取代[特内里费空难成为史上死伤人数最多的空难](../Page/特内里费空难.md "wikilink")。\[41\]

2017年10月24日，[加拿大航空781號航班的一架](../Page/加拿大航空781號航班.md "wikilink")[空中巴士A320在舊金山國際機場降落时](../Page/空中巴士A320.md "wikilink")，控制塔在飛機即將著陸時多次要求飛行員重飛，但飛行員依然進行著陸。事後飛行員表示：他們是在得到控制塔批准後才進行著陸的，但由於駕駛艙通訊系統失靈而並沒有收到控制塔的重飛指示。事件引發美國聯邦航空局啟動調查\[42\]。

2018年2月17日，[加拿大航空快运8585号班机事故中](../Page/加拿大航空快运8585号班机事故.md "wikilink")，一架隶属于加拿大航空快运的[庞巴迪Dash
8
Q400在萨斯卡通起飞后引擎突然起火](../Page/庞巴迪Dash_8.md "wikilink")，最终紧急返回萨斯卡通并成功迫降，机上人员无一伤亡\[43\]。

2018年12月11日，[加拿大航空015號航班](../Page/加拿大航空015號航班.md "wikilink")，一架載有376名乘客和17名機組人員，註冊編號為CFITW的[波音777-333ER由](../Page/波音777.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")[多倫多](../Page/多倫多.md "wikilink")[皮爾遜國際機場起飛](../Page/皮爾遜國際機場.md "wikilink")，前往[香港國際機場](../Page/香港國際機場.md "wikilink")。降落時，由在12月6日才獲簽發駕駛B777型飛機的資格，並首次實際駕駛並降落B777型飛機的副機長負責降落程序，發生機尾着地事故，機身嚴重損壞，無人受傷。事發當日，香港機場吹每小時12海里偏北風，沒有陣風，能見度為10公里，涉事客機在側風下着陸07R跑道。涉事客機降落時，降落軌迹略高於下滑道，但副機長在機長監督下仍穩定地將客機駛近跑道。當客機降至400英尺，副機長按航空公司程序更改為手動操控，機長則繼續在旁口頭指導。但當客機在跑道上方約200英尺時，突然出現一連串輕微的左右傾側，副機長即時使用大幅度操控以制止飛機傾側，機身先向左，再向右明顯傾側。客機降落前，機翼向右傾側，着地時機身右側起落架率先接觸跑道。由於客機以高速及機頭高仰的姿勢粗猛着陸，機身後方下側觸及跑道表面，繼而飛機彈起，再返回跑道中心線，最後駛向停機位讓乘客下機。檢查後發現，機身後方底部損毀，受影響部件需重大維修或更換，客機暫時不能使用。民航意外調查機構正詳細分析收集到的數據和資料，確定意外情況和因由，料調查需時12個月\[44\]。

## 相關條目

  - [加拿大越洋航空](../Page/加拿大越洋航空.md "wikilink")
  - [西捷航空](../Page/西捷航空.md "wikilink")
  - [波特航空](../Page/波特航空.md "wikilink")

## 參考來源

## 外部連結

  - [加拿大航空](http://www.aircanada.com/)

[加拿大航空_(公司)](../Category/加拿大航空_\(公司\).md "wikilink")
[Category:加拿大航空公司](../Category/加拿大航空公司.md "wikilink")
[Category:1937年成立的航空公司](../Category/1937年成立的航空公司.md "wikilink")

1.

2.  [Air Canada fleet
    age](http://www.planespotters.net/Airline/Air-Canada) Retrieved 13
    November 2014

3.

4.

5.

6.

7.

8.

9.

10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.
22. [Air Canada Historical
    Fleet](http://www.aircanada.com/en/about/fleet/historical.html) Date
    accessed: 27 January 2009

23.
24. [Air Canada -
    A340-500](http://www.aircanada.com/en/about/fleet/a340-500.html)
    Date accessed: 15 September 2009

25.
26.
27.
28.
29.
30.
31.
32.
33.
34.
35.
36.

37.

38.

39. [5](https://www.cathaypacific.com/cx/zh_HK/about-us/press-room/press-release/2016/Cathay-Pacific-and-Air-Canada-to-introduce-codeshare-services-and-reciprocal-mileage-accrual-and-redemption-benefits-in-strategic-co-operation.html)

40. <http://www.cbc.ca/news/canada/nova-scotia/air-canada-flight-624-crash-landing-in-halifax-1.3014060>
    Air Canada Flight 624 crash landing in Halifax

41.  中文|accessdate=2017-09-01|language=zh-CN}}

42.

43.

44.