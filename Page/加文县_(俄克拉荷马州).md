**加文县**（**Garvin County,
Oklahoma**）是[美國](../Page/美國.md "wikilink")[奧克拉荷馬州中部的一個縣](../Page/奧克拉荷馬州.md "wikilink")。面積2107平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口27210人。縣治[保尔斯谷](../Page/保尔斯谷.md "wikilink")（Pauls
Valley）。

## 参考文献

<div class="references-small">

<references />

</div>

[G](../Category/俄克拉何马州行政区划.md "wikilink")