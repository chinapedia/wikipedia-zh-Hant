[車容坊加油站鳳鳴站（2）.jpg](https://zh.wikipedia.org/wiki/File:車容坊加油站鳳鳴站（2）.jpg "fig:車容坊加油站鳳鳴站（2）.jpg")加盟的車容坊加油站鳳鳴站\]\]
**加油站**供[車輛及](../Page/車輛.md "wikilink")[船隻需要補充油料时使用](../Page/船隻.md "wikilink")。油料存在油庫（或油槽、油池）裡，通過管道連接到加油機器，通過加油槍加到車輛的[油箱裡](../Page/油箱.md "wikilink")。

## 型態

### 人工服務加油站

在世界上大多数地区，如[中國大陆及](../Page/中國大陆.md "wikilink")[台灣](../Page/台灣.md "wikilink")、非洲、拉美等，服務員會幫司機操作油槍加油及收費。

### 自助加油站

這些加油站在一些[歐洲以及](../Page/歐洲.md "wikilink")[北美洲國家](../Page/北美洲.md "wikilink")（比如[瑞士](../Page/瑞士.md "wikilink")、[美國](../Page/美國.md "wikilink")）、[日本比較常見](../Page/日本.md "wikilink")，加油站沒有服務員會幫司機操作油槍加油，使用者必須先向收費人員繳費後，由收費人員開啟加油機。或是通過自助收費機器（可以使用[紙鈔](../Page/紙鈔.md "wikilink")、[現金卡或](../Page/現金卡.md "wikilink")[信用卡](../Page/信用卡.md "wikilink")）自行加油。雖沒有服務員，但有的自助加油站會有站員駐守，以協助排除加油設備故障等問題。在[台灣](../Page/台灣.md "wikilink")，[台灣中油](../Page/台灣中油.md "wikilink")、[台塑](../Page/台塑.md "wikilink")[全國加油站](../Page/全國加油站.md "wikilink")、福懋、台亞、台糖也廣設自助加油，並給予折扣優惠以鼓勵顧客使用，而加油站可節省人力成本開銷。

### 聽裝油料供應站

[CPC_Corporation,_Taiwan_Yanping_Filling_station_06.jpg](https://zh.wikipedia.org/wiki/File:CPC_Corporation,_Taiwan_Yanping_Filling_station_06.jpg "fig:CPC_Corporation,_Taiwan_Yanping_Filling_station_06.jpg")延平聽裝油料供應站\]\]
聽裝站為加油站的一種形式之一，其得名原因為1950年至1970年代[美軍駐臺撤離前](../Page/駐台美軍.md "wikilink")，遺留下許多馬口鐵桶，而[美軍當時都以一只十公升馬口鐵桶為一](../Page/美軍.md "wikilink")「Tin」[汽油單位](../Page/汽油.md "wikilink")，直譯中文字為「聽」，由於該形式的馬口鐵桶油氣不逸散、拋擲也不損壞等因素，故加油站不普及的年代臺灣中油公司即利用美軍的馬口鐵桶當成運補偏遠鄉鎮的汽油桶\[1\]。而這些加油站點即稱為「聽裝站」。臺灣自2009年[八八水災](../Page/八八水災.md "wikilink")[高雄市](../Page/高雄市.md "wikilink")[桃源區](../Page/桃源區.md "wikilink")，臺灣中油桃源聽裝油料供應站裁撤後，僅存唯一一間[臺東縣](../Page/臺東縣.md "wikilink")[延平鄉](../Page/延平鄉.md "wikilink")[延平聽裝油料供應站](../Page/延平聽裝油料供應站.md "wikilink")\[2\]。

## 設置地點

加油站經常在普通公路以及[高速公路出現](../Page/高速公路.md "wikilink")，有些地方市區內亦有。[公路服務區也常設有加油站](../Page/服務區_\(公路\).md "wikilink")。

## 複合式經營

有些加油站採取[複合式經營](../Page/複合式經營.md "wikilink")，結合了洗車、車輛保養、代收款項（停車費、交通罰單等），部份加油站還設有[餐廳](../Page/餐廳.md "wikilink")、[速食店](../Page/速食店.md "wikilink")、[便利商店](../Page/便利商店.md "wikilink")、[郵局等](../Page/郵局.md "wikilink")。

## 加氣站

加油站內有時會附設供瓦斯或氫燃料車使用的加氣站，同時提供加油及加氣服務。亦有單獨設立的加氣站。

## 參見

  - [嫌惡設施](../Page/嫌惡設施.md "wikilink")

## 資料來源

## 外部連結

  -
  -
  -
  - [NPN Station Count
    (USA 1996-2006)](https://web.archive.org/web/20080624230423/http://www.npnweb.com/uploads/researchdata/2006/USAnnualStationCount/06-stationcount.pdf)

  - [An interview with architectural historian Jim Draeger about the
    history of filling stations from Wisconsin Public
    Television](https://web.archive.org/web/20110610054818/http://www.wpt.org/blog/2007/07/wpt-be-more-tuned-in-podcast.html)

[加油站](../Category/加油站.md "wikilink")
[Category:零售商](../Category/零售商.md "wikilink")
[Category:石油基础设施](../Category/石油基础设施.md "wikilink")
[Category:交通建築物](../Category/交通建築物.md "wikilink")
[Category:汽车保养](../Category/汽车保养.md "wikilink")
[Category:公路基础设施](../Category/公路基础设施.md "wikilink")

1.
2.