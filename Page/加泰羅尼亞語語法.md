**加泰羅尼亞語語法**（Catalan grammar）為[加泰罗尼亚语之語法](../Page/加泰罗尼亚语.md "wikilink")。

## 冠词与名词

在加泰罗尼亚语里非人类名词和抽象名词有阳性和阴性两种形式: *el llibre* (书，阳性), *la taula* (桌子，阴性).

冠词有下列形式:

### [定冠词](../Page/定冠词.md "wikilink")

| [名词](../Page/名词.md "wikilink")       | 阳性     | 阴性     |
| ------------------------------------ | ------ | ------ |
| [单数](../Page/數_\(語法\).md "wikilink") | el, l' | la, l' |
| [复数](../Page/數_\(語法\).md "wikilink") | els    | les    |

### [不定冠词](../Page/不定冠词.md "wikilink")

| [名词](../Page/名词.md "wikilink")       | 阳性  | 阴性   |
| ------------------------------------ | --- | ---- |
| [单数](../Page/數_\(語法\).md "wikilink") | un  | una  |
| [复数](../Page/數_\(語法\).md "wikilink") | uns | unes |

同一个词因为性的不同，意义也发生了改变.例如:

  - *el cap* (头脑), *la capa* (斗篷，披风)
  - *el roc* (石头), *la roca* (岩石).

有些名词由单数变复数时，词形不发生改变.例如:

  - *la pols* (脉搏) y *les pols* (脉搏)
  - *la tos* (咳嗽) y *les tos* (咳嗽)

非人的動物名詞的性變化可以分為四種情況.例如:

  - 陽性名詞，用來表示雄性動物的名詞:
      - *un cocodril* (鱷魚)
      - *un rossinyol* (夜鶯)
  - 隂性名詞，用來表示雌性動物的名詞:
      - *una guilla* (雌狐)
      - *una sargantana* (小雌蜥蜴)
  - 雄性和雌性詞形各不相同:
      - *un cavall* (公馬), *una egua* (母馬)
      - *un brau* (公牛), *una vaca* (母牛)
  - 具有同一詞根的:
      - *un gos* (公狗), *una gossa* (母狗)
      - *un lleó*(雄獅), *una lleona* (雌獅)

指人的名詞的性變化可分以下幾組:

  - 只有陽性:
      - *un fuster* (木匠)
      - *un paleta* (泥瓦工)
  - 只有隂性:
      - ''una *pentinadora* (梳毛機)
  - 隂陽性通用的:
      - *un modista* (女服裁縫), *una modista* (女服裁縫)
      - *un serpent* (蛇), *una serpent* (蛇)
  - 擁有同樣和相似詞根的:
      - *un cuiner* (廚師), *una cuinera* (女廚師)
      - *un mestre* (老師), *una mestra* (女老師)
  - 具有不同詞根的:
      - *un home* (男人), ''una *dona* (女人)
      - *un amo* (主人), una *mestressa* (主婦)
  - 變隂性時有不同詞尾變化的:
      - *un actor* (演員), *una actriu* (女演員)
      - *un metge* (醫生), *una metgessa* (女醫生)
  - 兩種可能:
      - *un advocat* (律師), *una advocada* 或 *una advocadessa* (女律師)
      - *un poeta* (詩人), *una poeta* 或 *una poetessa* (女詩人)
      - *un deu* (神), *una dea* 或 *una deessa* (女神)
  - 以 *-iu* 結尾的陽性名詞變隂性時，變為 *-iva*

## 參考文獻

  - \[Facsimile published in 1995\]

  -
  -
  -
  -
  -
  -
  -
## 外部連接

  - [A good English-language site about Catalan
    grammar](https://web.archive.org/web/20040616051717/http://www.orbilat.com/Languages/Catalan/Grammar/index.html)

[Category:特定語言語法](../Category/特定語言語法.md "wikilink")
[Category:安道爾](../Category/安道爾.md "wikilink")
[Category:加泰羅尼亞語](../Category/加泰羅尼亞語.md "wikilink")
[Category:羅曼語族](../Category/羅曼語族.md "wikilink")