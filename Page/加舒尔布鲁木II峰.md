**加舒尔布鲁木II峰**（**Gasherbrum
II**）位于[中国和](../Page/中国.md "wikilink")[巴控克什米尔边境的](../Page/巴控克什米尔.md "wikilink")[喀喇昆仑山脉](../Page/喀喇昆仑山脉.md "wikilink")，距离[乔戈里峰约](../Page/乔戈里峰.md "wikilink")26公里，海拔8,035-{zh-hans:米;
zh-hant:公尺;}-，是喀喇昆仑山脉的第四高峰，世界第十三高峰。

## 参见

  - [山峰列表](../Page/山峰列表.md "wikilink")

[Category:八千米高山](../Category/八千米高山.md "wikilink")
[Category:新疆山峰](../Category/新疆山峰.md "wikilink")
[Category:巴基斯坦山峰](../Category/巴基斯坦山峰.md "wikilink")
[Category:喀喇昆仑山脉](../Category/喀喇昆仑山脉.md "wikilink")