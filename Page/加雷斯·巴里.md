**加里夫·巴利**（，）是一名[英格蘭足球運動員](../Page/英格蘭.md "wikilink")。初出道時是一名中堅，後來改打左後衛，其後更憑著出色的助攻能力被改造為左中場及中路中場，更是球隊的[十二碼劊子手](../Page/十二碼.md "wikilink")。巴利曾效力[英超球會](../Page/英超.md "wikilink")[阿士東維拉長達](../Page/阿士東維拉足球俱樂部.md "wikilink")12年，於2009年夏季轉投[曼城](../Page/曼城足球俱乐部.md "wikilink")。他目前效力於[英超球隊](../Page/英超.md "wikilink")[西布罗姆维奇](../Page/西布罗姆维奇足球俱乐部.md "wikilink")。

巴利是曾為[英格蘭國家隊成員](../Page/英格蘭國家隊.md "wikilink")，及曾成為[18歲以下國家隊隊長](../Page/英格蘭18歲以下足球代表隊.md "wikilink")。他曾是[21歲以下國家隊上陣次數的紀錄保持者](../Page/英格蘭21歲以下足球代表隊.md "wikilink")，他合共代表球隊上陣21次，與[利物浦副隊長](../Page/利物浦.md "wikilink")[加歷查共同保持這項紀錄](../Page/加歷查.md "wikilink")。直至[利物浦守門員](../Page/利物浦.md "wikilink")[卡臣近年把他們的紀錄打破](../Page/卡臣.md "wikilink")。

## 生平

### 球會

  - 阿士東維拉

巴利原本是在[英格蘭小型球會](../Page/英格蘭.md "wikilink")[白禮頓受訓](../Page/布莱顿足球俱乐部.md "wikilink")，後於1997年以學徒身份加盟屬[英超的](../Page/英超.md "wikilink")[阿士東維拉](../Page/阿士東維拉足球會.md "wikilink")，於[1997-98年球季季末首次代表維拉上陣](../Page/1997年至1998年英格蘭超級聯賽.md "wikilink")。

巴利早於[1998-99年球季起已取得正選](../Page/1998年至1999年英格蘭超級聯賽.md "wikilink")，並由原來的中堅轉為左後衛。這名鬥志旺盛的球員常能在比賽劣勢下策動攻勢，而其熟練的左腳無論在盤球、傳送及射門，都經常能於左路帶來動力。由於英超基本上都是由常勝軍[曼聯所壟斷](../Page/曼聯.md "wikilink")，巴利並未獲得[英超聯賽冠軍](../Page/英超.md "wikilink")，唯一的佳績是2000年英格蘭足總盃打入了決賽，但敗於當屆冠軍[車路士](../Page/車路士.md "wikilink")。

由於具攻擊能力，巴利開始由後衛轉任為中場，成為球隊的進攻重心，為球隊中路製造創造力。巴利於[2006-07年球季開始前接任](../Page/2006年至2007年英格蘭超級聯賽.md "wikilink")[隊長一職](../Page/隊長_\(足球\).md "wikilink")。該年是巴利效力維拉的第十個年頭，維拉更頒贈獎章以表揚巴利對維拉之貢獻。

[2007-08年球季](../Page/2007年至2008年英格蘭超級聯賽.md "wikilink")，[英超四強之一的](../Page/英超.md "wikilink")[利物浦公開表示希望以超過](../Page/利物浦足球俱乐部.md "wikilink")1,000萬[英鎊轉會費買下巴利](../Page/英鎊.md "wikilink")<small>\[1\]</small>。這時巴利已27歲，曾公開稱渴求參戰[歐冠盃](../Page/歐冠盃.md "wikilink")，因此亟欲加盟利物浦，因此表示對維拉把過高標價加之於他甚為不滿，並與教練鬧翻。巴利對維拉公開衝突，使得維拉對他處以兩週週薪的罰款，並禁止他進入練習場地<small>\[2\]</small>。對效力超過十年之老臣公開跟球會鬧翻，維拉球迷都站在球會一方<small>\[3\]</small>。更可惜者，利物浦領隊跟利物浦班主，始終無法對巴利之交易達成一致意見。利物浦班主認為巴利根本不值1,800萬鎊身價<small>\[4\]</small>。轉會的糾紛不斷傳出，一直到季末<small>\[5\]</small>。最後基於轉會幾乎不可能下，巴利透過經紀人表示，[08-09年賽季將繼續留效維拉](../Page/2008年至2009年英格蘭超級聯賽.md "wikilink")<small>\[6\]</small>。

  - 曼城

巴利於各項維拉的賽事合共上陣超過400場賽事及射入52球。至2009年6月2日，巴利以1,200萬[英鎊身價加盟](../Page/英鎊.md "wikilink")[曼城](../Page/曼城足球俱乐部.md "wikilink")，簽約五年<small>\[7\]</small>。

2013年9月3日，[埃弗顿從](../Page/埃弗顿足球俱乐部.md "wikilink")[曼城借入將於](../Page/曼城足球俱乐部.md "wikilink")2014年6月約滿的巴利，借用期至球季結朿<small>\[8\]</small>。

  - 愛華頓

2014年7月8日，愛華頓官方宣布巴利加盟並簽約3年。同年12月，巴利成為首位英超歷史上得黃牌最多的球員。他也是英超歷史上出場次數超過500場的球員當中最年輕的一位。

2015年10月24日，愛華頓作客[阿仙奴的賽事中](../Page/阿仙奴.md "wikilink")，巴利在67分鐘領取在英超的第106面黃牌，成為英超史上領取黃牌最多的球員，及後在補時階段再領取本場第二張黃牌，兩黃一紅被逐，最後球隊以1:2不敵[阿仙奴](../Page/阿仙奴.md "wikilink")。

  - 西布朗

2017年9月25日，西布朗作客[阿仙奴的賽事中](../Page/阿仙奴.md "wikilink")，巴利成為英超史上球員最多上陣次數紀錄者(633場)。

### 國家隊

巴利曾代表[英格蘭U-21出賽](../Page/英格蘭21歲以下足球代表隊.md "wikilink")27場，曾與[加歷查一同保持最多代表次數的紀錄](../Page/加歷查.md "wikilink")，現已被[卡臣及](../Page/斯科特·卡森.md "wikilink")[詹姆斯·米尔纳分別打破](../Page/詹姆斯·米尔纳.md "wikilink")。巴利首次晉升為大國腳為1999年，當時曾參加[2000年歐洲國家盃](../Page/2000年歐洲國家盃.md "wikilink")，但沒上過陣。而真正以正選身份代表上陣，是於2000年9月對法國的賽事擔任左翼衛。而英格蘭最後以1比1逼和該支[1998年世界盃冠軍球隊](../Page/1998年世界盃.md "wikilink")。

巴利自2003年起已長時間沒有入選[英格蘭](../Page/英格蘭足球代表隊.md "wikilink")，要等到2007年再獲選出戰對[西班牙的友誼賽](../Page/西班牙國家足球隊.md "wikilink")\[9\]。9月8日以正選身份代表英格蘭上陣對以色列的歐洲國家盃外圍賽賽事，貢獻出兩個助攻，球隊以3:0擊敗對手，奪得重要的三分。3日後對俄羅斯的賽事，交出一個助攻，英格蘭最後以3:0勝出。兩場賽事中巴利的表現獲得很好的評價。\[10\]
他出色的表現，逼使[車路士的中場大將](../Page/車路士.md "wikilink")[林柏特於](../Page/林柏特.md "wikilink")07年10月份的兩場英格蘭國家隊重要賽事退居後備，亦是該名車路士中場自2004年後，首次因非傷患被其他球員奪去正選位置。

2008年6月初對千里達的國際友誼賽，巴利攻進其首個國家隊入球，協助球隊勝出3:0。2011年11月15日，在主場對[瑞典的友誼賽中](../Page/瑞典國家足球隊.md "wikilink")，射進全場唯一進球，以1-0擊敗對手，這是英格蘭歷史上第2,000個國際賽入球\[11\]。

## 参考資料

## 外部連結

  -
  - [[英格蘭足總](../Page/英格蘭足總.md "wikilink")：-{巴里}-國際賽紀錄](http://www.thefa.com/England/SeniorTeam/Archive/?pf=p&i=5463&ap=p&searchname=barry)

  - [阿士東維拉：巴利簡歷](https://web.archive.org/web/20081217193154/http://www.avfc.premiumtv.co.uk/page/PlayerProfiles/0%2C%2C10265~5502%2C00.html)

[Category:英格蘭足球運動員](../Category/英格蘭足球運動員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:白禮頓球員](../Category/白禮頓球員.md "wikilink")
[Category:阿士東維拉球員](../Category/阿士東維拉球員.md "wikilink")
[Category:曼城球員](../Category/曼城球員.md "wikilink")
[Category:愛華頓球員](../Category/愛華頓球員.md "wikilink")
[Category:西布朗球員](../Category/西布朗球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:2000年歐洲國家盃球員](../Category/2000年歐洲國家盃球員.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.  [新面孔加入英格蘭](http://news.bbc.co.uk/sport2/hi/football/internationals/6331715.stm)
10. [巴利交出兩個助攻](http://www.thefa.com/England/SeniorTeam/NewsAndFeatures/Postings/2007/08/England_vIsrael_Barry.htm)
11. [England end 43-year
    hoodoo](http://www.thefa.com/England/News/match-centre/2011/England-v-Sweden/England-Sweden-report-151111)
    thefa.com