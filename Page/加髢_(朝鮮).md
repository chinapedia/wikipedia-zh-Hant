[Seoul-Gyeongbokgung-Chinjamrye-16.jpg](https://zh.wikipedia.org/wiki/File:Seoul-Gyeongbokgung-Chinjamrye-16.jpg "fig:Seoul-Gyeongbokgung-Chinjamrye-16.jpg")時期王族婦女的加髢樣式（今人模擬）\]\]
**加髢**是[朝鮮古代](../Page/朝鮮半島.md "wikilink")[婦女佩戴的一種](../Page/婦女.md "wikilink")[假髻](../Page/假髻.md "wikilink")，一般是把[假髮編成髮](../Page/假髮.md "wikilink")[辮](../Page/辮.md "wikilink")，弄成盤狀戴上，有多種不同的樣式。

## 歷史

[Seoul-Gyeongbokgung-Chinjamrye-14a.JPG](https://zh.wikipedia.org/wiki/File:Seoul-Gyeongbokgung-Chinjamrye-14a.JPG "fig:Seoul-Gyeongbokgung-Chinjamrye-14a.JPG")
加髢的本義是[假髮](../Page/假髮.md "wikilink")，[高麗王朝時](../Page/高麗王朝.md "wikilink")，[忠烈王下令高麗全國穿](../Page/忠烈王.md "wikilink")[蒙古服](../Page/蒙古.md "wikilink")、留蒙古髮髻（編髮），女性開始使用加髢。後來[朝鮮太祖李成桂建立](../Page/朝鮮太祖.md "wikilink")[朝鮮王朝](../Page/朝鮮王朝.md "wikilink")，採「男降女不降」政策，男性恢復漢制，女性則「蒙漢並行」，之後加髢再發展成後來樣式，成為李氏朝鮮的代表服飾之一。

在李氏朝鮮的前期至中期，已婚婦女、[妓生](../Page/妓生.md "wikilink")、高級[女官](../Page/女官.md "wikilink")（[尚宮](../Page/尚宮.md "wikilink")）均會戴上加髢。加髢也是[身份](../Page/身份.md "wikilink")、[財富的象徵](../Page/財富.md "wikilink")，有錢人、貴族婦女和妓生的加髢可以很大，再加上各種飾物如[簪](../Page/簪.md "wikilink")、[釵](../Page/釵.md "wikilink")、[花](../Page/花.md "wikilink")、[玉板等](../Page/玉板_\(頭飾\).md "wikilink")，有些會綴上[絲帶或布條](../Page/絲帶.md "wikilink")。有些加髢是編成一條大髮辮再盤起來，有些則是盤成一條較幼的髮辮再盤成幾個圈，一些加髢甚至超過三圈。加髢是[牒紙的一種](../Page/牒紙.md "wikilink")，宮廷[女官服裝和](../Page/女官.md "wikilink")[命婦](../Page/命婦.md "wikilink")、[王族婦女禮服的加髢樣式有嚴格規範](../Page/王族.md "wikilink")，尚宮和王族婦女會在加髢上加上[子供枕](../Page/子供枕.md "wikilink")，王族婦女的加髢正面正上方有玉板一個，左右各有[花簪作為頭飾](../Page/花簪.md "wikilink")，通稱為「鳳首」。婚服中的加髢一般較大和加上較多飾物。後來宮中再發展出一種叫「[舉頭美](../Page/舉頭美.md "wikilink")」（）的[木頭假髻](../Page/木頭.md "wikilink")，於重大日子加在加髢上。

## 禁加髢

[Korea-Kisaeng-03a.jpg](https://zh.wikipedia.org/wiki/File:Korea-Kisaeng-03a.jpg "fig:Korea-Kisaeng-03a.jpg")的小妓生\]\]
[Old_korean_official_wife.jpg](https://zh.wikipedia.org/wiki/File:Old_korean_official_wife.jpg "fig:Old_korean_official_wife.jpg")
由於大加髢是身份、財富象徵，婦女的加髢越來越大，也越來越重，形成[奢侈之風氣](../Page/奢侈.md "wikilink")，一個加髢動輒百金\[1\]，後來更有婦女因加髢過重-{折}-斷[頸項至死](../Page/頸項.md "wikilink")，[英祖三年](../Page/朝鮮英祖.md "wikilink")（1737年）曾下令宮中加髢所用的[假髮每個由](../Page/假髮.md "wikilink")50束減至20束，以改善宮中風俗，英祖二十三年（1747年）時，他又與群臣商議以[花冠代替加髢](../Page/花冠.md "wikilink")，但仍未有共識。有些人認為即使改用花冠，如果飾以[珠](../Page/珍珠.md "wikilink")[玉](../Page/玉.md "wikilink")、[金](../Page/金.md "wikilink")[貝](../Page/貝殼.md "wikilink")，花費比加髢更多。

據《朝鮮王朝實錄·英祖實錄》及《正祖實錄》載，英祖三十二年（1756年），[領議政](../Page/領議政.md "wikilink")[金尚喆向英祖轉述](../Page/金尚喆.md "wikilink")[儒生](../Page/儒生.md "wikilink")[宋德相禁髢髮的仰請](../Page/宋德相.md "wikilink")，英祖認為要袪奢崇儉、移風易俗要從在上位者開始\[2\]
，於是在農曆[正月十六日](../Page/正月.md "wikilink")，下令禁[士族婦女加髢](../Page/士族.md "wikilink")，改戴稱為[簇頭里](../Page/簇頭里.md "wikilink")（）的小花冠\[3\]。直至英祖三十三年，宮中及[士族婦女正式禁用加髢](../Page/士族.md "wikilink")，改為在腦後梳髻\[4\]
，只容許平民和賤民女性加髢\[5\]。李氏朝鮮[禮曹所編的](../Page/禮曹.md "wikilink")《[特教定式](../Page/特教定式.md "wikilink")》（，）記載，英祖三十九年，朝廷又再討論禁加髢之事，英祖當時表示髢髻弊處多，認為真髮漂亮，不需要戴假髻\[6\]。後來已婚婦女就改為只把辮子盤成髮髻並插上[髮簪而不戴加髢](../Page/髮簪.md "wikilink")。妓生則仍然流行佩戴加髢。

到後期，官員妻子、王族婦女穿著[圓衫](../Page/圓衫.md "wikilink")（一種小禮服）時或在一些正式場合戴加髢。

## 圖庫

Image:754px-Hyewon-Dano.pungjeong2.JPG|脫下加髢（左）和戴着加髢的婦女
Image:Hyewon-Iseung.yeonggi.jpg|戴加髢並用[袿衣遮蓋頭部的婦女](../Page/袿衣.md "wikilink")（中）
Image:Hyewon-Ssanggeum.daemu-detail-01.jpg|戴加髢、帽子跳[劍舞的妓生](../Page/劍舞.md "wikilink")
Image:Danwon-Umulga2.jpg|平民婦女的加髢
Image:-Hyewon-Samchu.gayeon3.JPG|加髢較小的平民婦女
Image:Gache3.jpg|在整理加髢的妓生
Image:Hyewon-Tanhyeon.jpg|戴加髢彈[玄琴的婦人與梳辮子的](../Page/玄琴.md "wikilink")[女孩](../Page/女孩.md "wikilink")
Image:Gache002.jpg|以布帶裝飾加髢的女子 Image:Gache004.jpg|對鏡整理加髢的女子

## 參見

  - [韓服](../Page/韓服.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [加髢介紹](http://100.empas.com/dicsearch/pentry.html?i=110011)

  - [朝鮮古代髮式介紹](http://hair.culturecontent.com/)

  - [朝鮮內外命婦假髻介紹](http://blog.yam.com/ypzrgy1225/article/1725846)

<!-- end list -->

  - [王族婦女加髢樣式示意圖](http://hair.culturecontent.com/mall/pop_img.asp?c_id=cp0413220003&c_name)
  - [特教定式](http://e-kyujanggak.snu.ac.kr/_Print/GDS/GDS_VIEW.jsp?ptype=list&subtype=wg&bookid=GR36069_00IB&volnum=0001&articleid=024&dtype=2&savetype=save&savesize=)

[Category:韓服首服](../Category/韓服首服.md "wikilink")
[Category:高麗服飾](../Category/高麗服飾.md "wikilink")
[Category:朝鮮王朝服飾](../Category/朝鮮王朝服飾.md "wikilink")
[Category:假髮](../Category/假髮.md "wikilink")

1.  《朝鮮王朝實錄·英祖實錄》：「婦人一髢髻，輒費累百金。」
2.  《朝鮮王朝實錄·正祖實錄》：「上（英祖）曰：『儒賢所達，寔出袪奢崇儉之意。然必得其永久可行之制……大抵末世移風，莫非在上者之責。』」
3.  《朝鮮王朝實錄·英祖實錄》：「禁士族婦女加髢，代以俗名簇頭里。」
4.  《朝鮮王朝實錄·英祖實錄》：「命禁（宮）中外婦女髢髮，代以後髻。」
5.  《朝鮮王朝實錄·英祖實錄》：「常賤人則仍用髢。」
6.  《特教定式·禁髢髻》：「鬒髮如雲，不屑髢也。」