[Lorenz_attractor_yb.svg](https://zh.wikipedia.org/wiki/File:Lorenz_attractor_yb.svg "fig:Lorenz_attractor_yb.svg")，一个动态系统的研究中出现[洛伦茨吸引子](../Page/洛伦茨吸引子.md "wikilink")。\]\]

**动态系统**（****）是[数学上的一个概念](../Page/数学.md "wikilink")。動態系统是一种固定的规则，它描述一个给定空间（如某个物理系统的[状态空间](../Page/状态空间.md "wikilink")）中所有点随时间的变化情况。例如描述钟摆晃动、管道中水的流动，或者湖中每年春季鱼类的数量，凡此等等的[数学模型都是動態系统](../Page/数学模型.md "wikilink")。

在動態系统中有所谓**状态**的概念，状态是一组可以被确定下来的[实数](../Page/实数.md "wikilink")。状态的微小变动对应这组实数的微小变动。这组实数也是一种[流形的几何空间坐标](../Page/流形.md "wikilink")。動態系统的演化规则是一组[函数的](../Page/函数.md "wikilink")[固定规则](../Page/固定规则.md "wikilink")，它描述未来状态如何依赖于当前状态的。这种规则是[确定性的](../Page/确定性.md "wikilink")，即对于给定的时间间隔內，从现在的状态只能演化出一个未来的状态。

若只是在一系列不连续的时间点考察系统的状态，则这个動態系统为**离散動態系统**；若时间连续，就得到一个**连续動態系统**。如果系统以一种连续可微的方式依赖于时间，我们就称它为一个**光滑動態系统**。

## 参见

  - [混沌理论](../Page/混沌理论.md "wikilink")
  - [洛伦兹系统](../Page/洛伦兹系统.md "wikilink")
  - [複變動態系統](../Page/複變動態系統.md "wikilink")
  - [线性动态系统](../Page/线性动态系统.md "wikilink")

## 参考书籍

  - [Geometrical theory of dynamical
    systems](http://arxiv.org/pdf/math.HO/0111177) Nils Berglund's
    lecture notes for a course at [ETH](../Page/ETH.md "wikilink") at
    the advanced undergraduate level.
  - [Dynamical systems](http://www.ams.org/online_bks/coll9/). George D.
    Birkhoff's 1927 book already takes a modern approach to dynamical
    systems.
  - [Chaos: classical and
    quantum](https://web.archive.org/web/20051104051610/http://alf.nbi.dk/ChaosBook/).
    An introduction to dynamical systems from the periodic orbit point
    of view.
  - [Introduction to Social
    Macrodynamics](http://urss.ru/cgi-bin/db.pl?cp=&page=Book&id=34250&lang=en&blang=en&list=Found).
    Mathematical models of the World System development
  - Differential Equations, Dynamical Systems, and an Introduction to
    Chaos 微分方程、动力系统与混沌导论

## 延伸閱讀

Works providing a broad coverage:

  - (available as a reprint: )

  - *Encyclopaedia of Mathematical Sciences* () has a sub-series on
    dynamical systems with reviews of current research.

  -
  -
Introductory texts with a unique perspective:

  -
  -
  -
  -
  -
Textbooks

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
Popularizations:

  -
  -
  -
  -
## 外部链接

  - [動態系統介紹](http://www.math.nctu.edu.tw/Dynamics/dynamic%20intro/dynamic%20intro.htm)
  - [Dynamical systems at
    SUNY](https://web.archive.org/web/20050702083555/http://www.math.sunysb.edu/dynamics/)：紐約州立大學石溪校區研究小組的網站，有會議、研究者和未解決問題等資料（英）
  - [Oliver
    Knill](http://www.dynamical-systems.org)：以JavaScript說明一些動態系統的例子（英）
  - [Arxiv preprint
    server](http://www.arxiv.org/list/math.DS/recent)：關於此範疇每日的新論文（英）
  - [Chaos @ UMD](http://www-chaos.umd.edu/)：主攻應用層面（英）
  - [動態系統的穩定性分析](http://web.phys.ntu.edu.tw/physhistory/space_time/vol_28/v28_p77.pdf)

[Category:系統理論](../Category/系統理論.md "wikilink")
[动力系统](../Category/动力系统.md "wikilink")