《**动物庄园**》（），也译作《**動物農場**》、《**動物-{zh-hans:农庄;
zh-hant:莊園;}-**》、《**一脸猪相**》，[英国著名作家](../Page/英国.md "wikilink")[喬治·奧威爾編寫用以反对](../Page/喬治·奧威爾.md "wikilink")[斯大林的小說](../Page/斯大林.md "wikilink")\[1\]。1945年首次岀版英文版，目前有多个中文译本。

据乔治·奥威尔所说，这本书反映了[苏联从](../Page/苏联.md "wikilink")[十月革命到斯大林时期的历史事件](../Page/十月革命.md "wikilink")。奥威尔是一位[民主社会主义者](../Page/民主社会主义.md "wikilink")，但他对斯大林和[斯大林主义持批判态度](../Page/斯大林主义.md "wikilink")。奥威尔参加过[西班牙内战](../Page/西班牙内战.md "wikilink")，正是这段经历让他对斯大林主义获得更深入的了解并对其进行批判。他认为，苏联已是惨无人道的[独裁统治](../Page/独裁统治.md "wikilink")，而这种统治手段建立在对斯大林的[个人崇拜和](../Page/个人崇拜.md "wikilink")[大清洗上](../Page/大清洗.md "wikilink")。

乔治·奥威尔于1943年11月至1944年2月间创作本书，当时[英国同苏联是战时盟友](../Page/英国.md "wikilink")，而英国的百姓和知识分子都对斯大林评价很高，这一现象让奥威尔十分厌恶。本书的手稿，起初被许多英美出版商拒绝出版。但本书问世以後获得了巨大的成功，其原因部分是因為原来同英国是战时盟友的苏联与西方世界开始了[冷战](../Page/冷战.md "wikilink")。

《[时代杂志](../Page/时代杂志.md "wikilink")》将本书评选为1923-2005年100佳英文小说。

## 故事概要

庄园农场（Manor Farm）里的一头老公猪“老少校”（Old
Major）将动物们召集起来开会，宣布人类是动物的敌人，并教会了动物们一首歌《英格兰的生灵》（Beasts
of
England）。老少校死后，两头年轻的猪，分别叫做雪球（Snowball）和拿破仑（Napoleon），成为了动物们的领袖，并开始准备起义。

起义成功了，动物们把终日酗酒、不负责任的农场主琼斯（Mr. Jones）赶出了农场，然后把农场更名为“动物农场”（Animal
Farm）。他们通过了“动物主义七戒“（ the Seven Commandments of
Animalism），其中最重要的一条是，“所有动物一律平等。”

雪球教动物们读写，而拿破仑教小狗们动物主义的规则。动物们食物充足，农场运营良好。猪们把自己提拔到了领导的位置，并给自己留出了特别的食物供应，显然，这是为了他们自己的健康和享受。

一段时间之后，一些人攻击了动物农场。琼斯和他手下的人尝试着重新夺回农场，同时，其他农场主因为害怕相似的动物起义发生在自己的农场，也来帮助琼斯。雪球和其他动物先是躲藏起来，随即发动了突然袭击，把刚刚进入农场的人类打得落花流水。动物们胜利了。雪球的支持率猛增，这场战斗被正式命名为“牛棚战役”。在动物起义的周年纪念日，动物们决定鸣枪纪念。

雪球和拿破仑为了最高领导人的地位互相竞争。雪球有一个计划，那就是兴建[风车](../Page/风车.md "wikilink")，让农场变得现代化。当雪球宣布他的的计划时，拿破仑让他喂养的狗驱逐了雪球，并宣布他自己就是领导人。

拿破仑改变了农场的领导结构。他取消了动物大会，取而代之的是一个由猪组成的委员会，由猪来运营农场。通过一头叫做吱嘎（Squealer）的年轻的猪，拿破仑宣布了兴建风车的计划。动物们工作得十分卖力，因为拿破仑承诺，风车建成后动物们的生活将会变得更加轻松。一场猛烈的暴风雨后，动物们发现风车倒塌了。拿破仑和吱嘎随即让动物们相信，这是雪球在蓄意破坏他们的工程。自从雪球成为替罪羊后，拿破仑开始利用他手下的狗来清洗农场，杀死那些被他指控同雪球有私交的动物。当一些动物回忆起牛棚战役时，拿破仑不断污蔑说雪球是琼斯的通敌，并把他自己荒唐地当成这场战斗中英雄的代表，（而实际上拿破仑自己在这场战斗中躲藏得无影无踪。）“英格兰的生灵”这首歌被一首赞颂拿破仑的歌曲所取代，拿破仑却开始像人类一样生活。动物们仍然相信他们的生活比在琼斯统治下的生活好多了。

[缩略图](https://zh.wikipedia.org/wiki/File:Animal_Farm_artwork.jpg "fig:缩略图")

弗雷德里克先生（Mr.
Frederick）是附近一个农场的农场主。他攻击了动物农场，并用火药炸毁了重建好的风车。尽管动物们在战斗中获胜，但他们付出了巨大代价，包括一匹叫做拳击手（Boxer）的驮马也受伤了。但拳击手不顾伤口，仍然不辞辛苦地工作，直到他在建造风车的过程中累倒。拿破仑派了一辆货车送拳击手去看兽医，并解释说，这样拳击手可以得到更好的护理。一头叫本杰明（Benjamin）的愤世嫉俗的驴子能够像猪一样阅读，而他却发现，这是辆屠马场的货车，并徒劳地尝试着救下拳击手。很快，吱嘎就让动物们相信，这辆车是动物医院从屠马场买来的，而屠马场的标识并没有被涂改。随后，吱嘎告诉动物们拳击手在动物医院安详去世。猪们在拳击手死后举行了一天的节日庆典，来深刻赞扬动物农场取得的光辉成就，并要求动物们像拳击手一样刻苦地工作。然而，事实却是拿破仑把拳击手卖给了屠马场，赚来钞票好给统治集团裡的猪们买威士忌喝。（在20世纪40年代的英国，农场可以通过将大型动物卖给屠宰场赚钱，而屠宰场会把动物杀死，并将它们的残骸煮沸，制成动物胶。）

许多年过去了，风车已被重建，同时另一座风车在建，农场的收入非常可观。但是，雪球曾经提及的，包括给畜栏挂上电灯，供应热水和自来水的设想已被忘记，而拿破仑不断鼓吹说，最快乐的动物应当过着简单的生活。拳击手死后，许多参加过动物起义的动物也已死去，琼斯也客死他乡。猪们越来越与人类相似，他们直立行走、手持鞭子、身着衣服。“七戒”被简化为一句简单的话：“所有动物一律平等，但一些动物比其他动物更加平等。”拿破仑举办了一场晚餐宴会，猪们和当地的农场主庆祝着他们的刚刚建立的盟友关系。拿破仑废除了革命传统，并将农场重新命名为“庄园农场”。动物们看着与人类平起平坐的猪，再看看人类，这才意识到，他们再也不能辨别哪个是猪，哪个是人了。

## 角色

一种常见的解读是，动物庄园的故事以[俄国十月革命到苏联](../Page/俄国十月革命.md "wikilink")1940年代这段历史为蓝本而构建，因此故事中出现的角色也都有着相应的原型。

### 动物角色

  - [猪](../Page/猪.md "wikilink")
      - **老少校**（Old
        Major），提出了动物主义的思想，影射[馬克思和](../Page/卡尔·马克思.md "wikilink")[列宁](../Page/列宁.md "wikilink")。
      - **雪球**（Snowball），[动物庄园](../Page/动物.md "wikilink")[革命的领导者之一](../Page/革命.md "wikilink")，后被拿破仑驱逐出境并宣布为革命的敌人，影射[托洛茨基](../Page/托洛茨基.md "wikilink")。
      - **拿破崙**（Napoleon），动物庄园革命的领导者之一，后来通过暴力政变成为庄园的领袖，影射[斯大林](../Page/斯大林.md "wikilink")。
      - **尖叫者**（Squealer），拿破崙的忠實支持者，擅於言辭。影射[莫洛托夫](../Page/莫洛托夫.md "wikilink")。
  - [马](../Page/马.md "wikilink")
      - **[拳師](../Page/鲍克斯.md "wikilink")**（Boxer），动物主义理念的忠实追随者，时刻积极响应革命领袖的号召，勤劳肯干。积劳成疾后被拿破崙卖给宰马商，象征着相信“革命理论”的广大[無產階級群眾](../Page/無產階級.md "wikilink")。
      - **珂蘿薇**（Clover），動物主義的追隨者。與拳擊手一樣勤勞，而且十分關心別人。同樣象徵[無產階級](../Page/無產階級.md "wikilink")。
      - **莫麗**（Mollie），愚蠢、貪慕虛榮、物質主義者。對於革命沒有興趣，只關心自己的利益，後來逃離了動物農莊。代表俄國的[資產階級](../Page/資產階級.md "wikilink")。
  - 其他
      - **摩西**（Moses），[烏鴉](../Page/烏鴉.md "wikilink")，被農場主人瓊斯馴服。一開始與瓊斯一同逃離農莊，後來回到農莊。經常大談蜜糖山的美夢。代表[東正教會](../Page/俄罗斯东正教会.md "wikilink")。
      - **班杰明**（Benjamin），[驴](../Page/驴.md "wikilink")，对拿破仑的所作所为始终抱有怀疑但明哲自保，象征有独立思想对[极权主义有所怀疑但明哲保身的](../Page/极权主义.md "wikilink")[知识分子](../Page/知识分子.md "wikilink")。（[乔治·奥威尔宣称本杰明是影射自己的](../Page/乔治·奥威尔.md "wikilink")）
      - **羊群**，盲目追隨拿破崙，阻止反對拿破崙的聲音。代表盲從政治宣傳的無知民眾。
      - **无名[犬们](../Page/犬.md "wikilink")**(包括**傑西**、**品撤**)，拿破崙在动物庄园实施暴力统治的工具，象征极权主义[国家内如](../Page/国家.md "wikilink")[KGB之流的各种](../Page/KGB.md "wikilink")[暴力机构](../Page/秘密警察.md "wikilink")。

### 人類角色

  - **琼斯先生**（Mr.
    Jones）庄园农场的旧主人，影射俄罗斯沙皇[尼古拉二世](../Page/尼古拉二世.md "wikilink")。
  - **皮尔京顿先生**（Mr.
    Pilkington）福克斯伍德农场的主人，影射西方国家（如[英国的](../Page/英国.md "wikilink")[丘吉尔和](../Page/丘吉尔.md "wikilink")[美国的](../Page/美国.md "wikilink")[罗斯福](../Page/富兰克林·德拉诺·罗斯福.md "wikilink")）。
  - **弗雷德里克先生**（Mr.
    Frederick）平彻菲尔德农场的主人，與皮尔京顿先生關係很差，無法合作；很常有他喜歡虐待動物的傳聞；曾经与动物庄园达成买卖协议，后来毁约並入侵动物庄园。影射打壓共產主義的[軸心國](../Page/軸心國.md "wikilink")（如[纳粹德国的](../Page/纳粹德国.md "wikilink")[希特勒](../Page/希特勒.md "wikilink")、[義大利的](../Page/義大利王國_\(1861年–1946年\).md "wikilink")[墨索里尼及](../Page/墨索里尼.md "wikilink")[日本的](../Page/大日本帝國.md "wikilink")[東條英機](../Page/東條英機.md "wikilink")）。
  - **温普先生**（Mr.
    Whymper）动物庄园与外界的联系人，影射西方的[左派人士](../Page/左派.md "wikilink")（如[萧伯纳](../Page/萧伯纳.md "wikilink")）。

## 英格兰之兽和七诫之曲

英格兰之兽是革命时所唱的歌曲，有着鼓舞人心的作用，影射了[国际歌](../Page/国际歌.md "wikilink")。七诫则是[宪法性质的基本条律](../Page/宪法.md "wikilink")，影射了[共产党宣言](../Page/共产党宣言.md "wikilink")。七誡包括：

1.  凡靠兩條腿行走的，全是仇敵。
2.  凡靠四條腿行走，或者長著翅膀的，全是朋友。
3.  所有動物不能穿衣服。
4.  所有動物不能睡床鋪。
5.  所有動物不能喝酒。
6.  所有動物不能傷害其他動物。
7.  所有動物一律平等。

## 与历史事件对应的情节

以下为读者整理，为本书中可能与现实历史事件相对应的书中情节。

### 第一章

  - 老少校向动物传道：[马克思](../Page/马克思.md "wikilink")、[恩格斯创立](../Page/恩格斯.md "wikilink")[共产主义学说](../Page/共产主义.md "wikilink")，并在各国宣传，在其影响下形成了最早的一批共产主义者。
  - “四条腿好，两条腿坏”、“所有动物都是同志”：马克思恩格斯的[无产阶级国际主义思想](../Page/无产阶级国际主义.md "wikilink")、[正义者同盟的](../Page/正义者同盟.md "wikilink")“四海皆兄弟”这一口号。
  - “英格兰兽”及其反响：失败的欧洲工人[1848年革命](../Page/1848年革命.md "wikilink")/19世纪末沙俄各派社会主义者的行动。

### 第二章

  - 拿破仑、斯诺鲍发展动物主义：[列宁发展](../Page/列宁.md "wikilink")[马克思主义并将其现实化](../Page/马克思主义.md "wikilink")/苏共的发展。
  - 琼斯先生的困境：[沙俄被](../Page/沙俄.md "wikilink")[日本打敗](../Page/日本.md "wikilink")/卷入[一战无法自拔](../Page/一战.md "wikilink")。
  - 动物叛亂：[无政府主义式的](../Page/无政府主义.md "wikilink")[俄国二月革命](../Page/俄国二月革命.md "wikilink")。
  - 七诫的书写：[十月革命以及](../Page/十月革命.md "wikilink")[俄共（布）的建立](../Page/苏联共产党.md "wikilink")。

### 第三章

  - 收割牧草：苏联的第一个五年计划/列宁的“[新经济政策](../Page/新经济政策.md "wikilink")”。
  - 各个委员会的成立：“[苏维埃](../Page/苏维埃.md "wikilink")”的成立。

### 第四章

  - 动物庄园声名的传播：十月革命后到20世纪20年代初西方各国对苏俄的影响。
  - “牛棚战役”：肃清[白俄](../Page/俄國白軍.md "wikilink")，并成功抵御[多国干涉的](../Page/协约国武装干涉俄国内战.md "wikilink")[俄国内战](../Page/俄国内战.md "wikilink")。

### 第五章

  - 改进生产工具：苏联第二、第三两个[五年计划中对苏联经济结构的改造](../Page/五年计划.md "wikilink")。
  - 斯诺鲍的理想和风车计划：[托洛茨基的超工业化思想和](../Page/托洛茨基.md "wikilink")[全球革命论](../Page/全球革命论.md "wikilink")。
  - 九只狗的培养：[秘密警察](../Page/秘密警察.md "wikilink")（如:[契卡](../Page/契卡.md "wikilink")、[内务人民委员会](../Page/内务人民委员会.md "wikilink")、[克格勃等](../Page/克格勃.md "wikilink")）的建立。
  - 驅逐斯诺鲍：斯大林上台后对托洛茨基以及[托派的全面](../Page/托派.md "wikilink")[封杀](../Page/封杀.md "wikilink")，迫使[托洛茨基流亡至墨西哥](../Page/托洛茨基.md "wikilink")。

### 第六章

  - 风车建设：经济建设中对重工业化的偏向，以及对关系民生问题的轻工业的忽视。（註：风车象征着苏联的工业化发展。）
  - 温普尔的到来：苏联通过西方左派知识分子（如萧伯纳）向外传达信息，与西方一定程度地达成和解。
  - 猪的享乐化：苏联高层的腐化。
  - 风车被毁：苏联的经济危机。

### 第七章

  - 饥荒：苏联的[农业集体化对农业生产的损害及其引发的](../Page/农业集体化.md "wikilink")[大饥荒](../Page/1932年苏联大饥荒.md "wikilink")。
  - 鸡被迫出售鸡蛋导致造反：苏联对农业的榨取。
  - 批判斯诺鲍：清洗[托派的延续并通过树立敌人转移矛盾](../Page/托派.md "wikilink")。
  - 大屠杀：斯大林发动的[大清洗](../Page/大清洗.md "wikilink")。
  - “英格兰兽”被禁止唱歌：苏联／[斯大林主义者篡改](../Page/斯大林主义.md "wikilink")[社会主义学说](../Page/科学社会主义.md "wikilink")

### 第八章

  - 篡改“七诫”：[篡改](../Page/修正主义_\(马克思主义\).md "wikilink")[共产主义学说](../Page/共产主义.md "wikilink")。
  - 梅尼缪斯的歌颂诗歌：对斯大林的[个人崇拜](../Page/个人崇拜.md "wikilink")。
  - 煽动对斯诺鲍以及人类的恐惧心理：各国常用政治手腕：通过对敌人的恐惧（xenophobia）来转移舆论注意力。
  - 出卖木料：（英法）《[慕尼黑协定](../Page/慕尼黑协定.md "wikilink")》/《[苏德互不侵犯条约](../Page/苏德互不侵犯条约.md "wikilink")》。
  - 出卖木料被骗：德国撕毁《苏德互不侵犯条约》。
  - 与其它庄园的关系：20世纪三十年代后期的国际政治格局。
  - 風車戰役（对弗雷德里克战斗）：[二战](../Page/二战.md "wikilink")（苏联[史達林格勒战役](../Page/史達林格勒战役.md "wikilink")、[卫国战争](../Page/卫国战争.md "wikilink")）。
  - 猪饮酒：苏联高层更加腐化，特权普遍化。

### 第九章

  - 乌鸦“摩西”的归来：苏联对[宗教管制的放松](../Page/宗教管制.md "wikilink")。
  - [鲍克斯的受伤与被杀](../Page/鲍克斯.md "wikilink")：战后苏联人民生活的困难与被利用。
  - 猪狗数量增长：苏联高层官员数目膨胀。

### 第十章

  - 猪篡改“动物主义”理论，猪变成人：苏联披着“社会主义”的表皮，使剥削公开化合法化。
  - 猪和人类的和解，人猪宴会：英美和苏联的战时合作，[雅爾達會議](../Page/雅爾達會議.md "wikilink")、[德黑兰会议](../Page/德黑兰会议.md "wikilink")。
  - 猪和人类同时打出黑桃A，互相指责对方作弊：苏联和西方国家战后敌对，[冷战开始](../Page/冷战.md "wikilink")。

## 名言

  - 「四條腿好，兩條腿壞」（Four legs good, two legs bad.）
  - 「四條腿好，兩條腿更好」（Four legs good, two legs better.）
  - 「所有動物都是同志」（All animals are comrades.）
  - 「所有動物生來平等，但有些動物比其他動物更平等。」（All animals are equal, but some animals
    are more equal than others.）

## 創作及出版歷程

喬治·奧威爾從1943年冬天起開始執筆撰寫《动物庄园》。書成之後，他向許多家出版社一一投稿，但都遭退回，其中一家出版社還寫信回應表示：「我認為以豬影射統治階層會得罪很多人，尤其是像蘇聯這樣難搞的政府」。後來，奧威爾找到一家小型獨立出版社Secker
and
Warburg的老闆，對方終於願意出版此書。於是《动物庄园》於1945年8月在英國正式出版，很快成為[暢銷書](../Page/暢銷書.md "wikilink")，初版的四千五百本在幾天內銷售一空\[2\]。

## 中文版

自1948年[商务印书馆出版任稚羽翻译的中文版起](../Page/商务印书馆.md "wikilink")，《动物庄园》被多次译为中文版，以下列出一些常见中文版本的[ISBN](../Page/ISBN.md "wikilink")：

  - ISBN 7-208-00425-0 （[上海人民出版社](../Page/上海人民出版社.md "wikilink")）
  - ISBN 7-5327-2935-4 （[上海译文出版社](../Page/上海译文出版社.md "wikilink")）
  - ISBN 978-7-5327-4857-0 （上海译文出版社中英双语版）
  - ISBN 7-80514-445-1 （[上海翻译出版公司](../Page/上海翻译出版公司.md "wikilink")）
  - ISBN 7-5004-4041-3 （[中国社会科学出版社](../Page/中国社会科学出版社.md "wikilink")）
  - ISBN 7-80096-750-6 （[中国致公出版社](../Page/中国致公出版社.md "wikilink")）
  - ISBN 978-7-2010-7762-8 （[天津人民出版社](../Page/天津人民出版社.md "wikilink")）

## 脚注

## 参考资料

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - ([Bernard Crick](../Page/Bernard_Crick.md "wikilink")'s preface
    quotes Orwell writing to T. S. Eliot about Cape's suggestion to find
    another animal than pigs to represent the Bolsheviks)

  -
  -
## 外部链接

  - [動物農莊線上閱讀](http://books.google.com/books?id=AQ-df8o2LhoC&dq=%E5%8B%95%E7%89%A9%E8%BE%B2%E8%8E%8A&printsec=frontcover&source=bn&hl=zh-TW&ei=yt5JS9CAO8qHkQXMpvD_Ag&sa=X&oi=book_result&ct=result&resnum=4&ved=0CBsQ6AEwAw#v=onepage&q=&f=false)
  - [动物庄园电影Animal Farm (1954)](http://www.imdb.com/title/tt0047834/)
  - [动物庄园电视Animal Farm (1999)
    (TV)](http://www.imdb.com/title/tt0204824/)
  - [*Animal Farm* full text at
    eBooks@Adelaide](http://ebooks.adelaide.edu.au/o/orwell/george/o79a/index.html)
  - [*Animal Farm* Audio
    Book](https://archive.org/details/GeorgeOrwellsanimalFarmRadioAudio)
    (web archive)
  - [*Animal Farm* Book Notes from
    Literapedia](http://literapedia.wikispaces.com/Animal+Farm)
  - [Excerpts from Orwell's letters to his agent concerning *Animal
    Farm*](https://web.archive.org/web/20051024060250/http://www.netcharles.com/orwell/essays/letters-agent-af.htm)
  - [Literary Journal
    review](https://web.archive.org/web/20070108225314/http://www.antigonishreview.com/bi-111/111-pyle.html)
  - [Orwell's original preface to the
    book](http://home.iprimus.com.au/korob/Orwell.html)
  - [*Animal Farm
    Revisited*](https://www.marxists.org/history/etol/writers/molyneux/1989/xx/orwell.html)
    by [John
    Molyneux](../Page/John_Molyneux_\(academic\).md "wikilink"),
    *International Socialism*, 44 (1989)
  - [*Animal Farm*](http://www.bl.uk/works/animal-farm) at the British
    Library

[A](../Category/1945年長篇小說.md "wikilink")
[A](../Category/英国小说.md "wikilink")
[A](../Category/諷刺小說.md "wikilink")
[Category:反烏托邦小說](../Category/反烏托邦小說.md "wikilink")
[A](../Category/猪主角故事.md "wikilink")
[A](../Category/20世紀百大英文小說.md "wikilink")
[A](../Category/時代雜誌百大英文小說.md "wikilink")
[A](../Category/英格蘭小說.md "wikilink")
[A](../Category/動物文學.md "wikilink")
[A](../Category/改編成電影的英國小說.md "wikilink")

1.  [Davison 2000](../Page/#Dav.md "wikilink")
2.