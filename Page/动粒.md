[Kinetochore.jpg](https://zh.wikipedia.org/wiki/File:Kinetochore.jpg "fig:Kinetochore.jpg")

**动粒\[1\]**或**着丝点**（）是[真核细胞](../Page/真核细胞.md "wikilink")[染色体中位于](../Page/染色体.md "wikilink")[着丝粒两侧的](../Page/着丝粒.md "wikilink")3层盘状特化结构，其化学本质为[蛋白质](../Page/蛋白质.md "wikilink")，是非染色体性质物质附加物。

动粒与染色体的移动有关。在[细胞分裂](../Page/细胞分裂.md "wikilink")（包括[有丝分裂和](../Page/有丝分裂.md "wikilink")[减数分裂](../Page/减数分裂.md "wikilink")）的[前](../Page/细胞分裂前期.md "wikilink")、[中](../Page/细胞分裂中期.md "wikilink")、[后期等几个阶段](../Page/细胞分裂后期.md "wikilink")，[纺锤体的](../Page/纺锤体.md "wikilink")[纺锤丝](../Page/纺锤丝.md "wikilink")（或[星射线](../Page/星射线.md "wikilink")）需附着在染色体的动粒上（而非着丝粒上），牵引染色体移动、将染色体拉向细胞两极。

动粒在[真核生物中形成并在着丝粒上组装](../Page/真核生物.md "wikilink")。在有丝分裂和减数分裂期间，丝点将染色体连接到[微管聚合物上](../Page/微管聚合物.md "wikilink")。

动粒和[着丝粒并非同一结构](../Page/着丝粒.md "wikilink")，它们的功能也不同，但这两种结构的位置关系是固定的。在非正规场合中，有时也用动粒或着丝粒泛指它们所在的染色体[主缢痕位置](../Page/主缢痕.md "wikilink")。

## 区域

着丝粒包括两个区域：一个[内动粒](../Page/内动粒.md "wikilink")，该区域用于与[DNA着丝粒紧密连接](../Page/DNA.md "wikilink")；一个[外动粒](../Page/外动粒.md "wikilink")，用于和微管发生作用。

[单着丝粒生物](../Page/单着丝粒生物.md "wikilink")（包括[脊椎动物](../Page/脊椎动物.md "wikilink")、[霉菌和众多](../Page/霉菌.md "wikilink")[植物](../Page/植物.md "wikilink")）在每个染色体中有一个单独的动粒区，联合起来组成一个动粒。[全着丝粒生物](../Page/全着丝粒生物.md "wikilink")（包括[线虫和](../Page/线虫.md "wikilink")[蛔虫等](../Page/蛔虫.md "wikilink")）顺着染色体的延伸方向组装动粒。

## 作用

在细胞[有丝分裂S期期间](../Page/有丝分裂S期.md "wikilink")，染色体自我复制，两个姐妹染色单体由各自的方向相反的动粒结合在一起。在[分裂中期到](../Page/分裂中期.md "wikilink")[分裂后期的转变中](../Page/分裂后期.md "wikilink")，姐妹染色单体各自分离，各染色单体上的独立动粒驱动它们向纺锤体的两极运动，形成两个新的子细胞。因此动粒是经典有丝分裂和减数分裂中染色体分离必不可少的要素。

## 组成

即使是最简单的动粒也包括超过45种不同的蛋白质，其中大部分存在于真核细胞中，包括一类专用的[组蛋白H3变种](../Page/组蛋白H3.md "wikilink")（称为“CENP-A”或“CenH3”）。这些蛋白质在动粒和DNA连接中起辅助作用。动粒中的其他蛋白质使动粒附着于有丝分裂纺锤体的微管上。同时还需[蛋白质发动机](../Page/蛋白质发动机.md "wikilink")（如[动力蛋白和](../Page/动力蛋白.md "wikilink")[驱动蛋白](../Page/驱动蛋白.md "wikilink")）为有丝分裂中染色体的运动提供动力。其他一些蛋白（如MAD2）监测微管的附着情况及[姐妹动粒的张力大小](../Page/姐妹动粒.md "wikilink")，并在这两项中任意一项出现问题时激活[纺锤体检查点来阻止](../Page/纺锤体检查点.md "wikilink")[细胞复制的循环周期](../Page/细胞复制.md "wikilink")。

## 染色

在[染色体被](../Page/染色体.md "wikilink")[碱性染料染色后](../Page/碱性染料.md "wikilink")，由于动粒几乎把着丝粒覆盖，所以染色后观察染色体的外形时在动粒部分染色很浅或几乎观察不到着色。

## 外部链接

  -
## 参考文献

[Category:細胞器](../Category/細胞器.md "wikilink")

1.  [1](http://www.term.gov.cn/pages/homepage/result2.jsp?id=171683&subid=10000633&subject=%E5%8C%BB%E5%AD%A6%E9%81%97%E4%BC%A0%E5%AD%A6&subsys=%E5%8C%BB%E5%AD%A6)