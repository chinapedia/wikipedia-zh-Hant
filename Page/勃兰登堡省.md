<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p><strong>Provinz Brandenburg</strong><br />
<strong>勃兰登堡省</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Flagge_Preußen_-_Provinz_Brandenburg.svg" title="fig:Flagge_Preußen_-_Provinz_Brandenburg.svg">Flagge_Preußen_-_Provinz_Brandenburg.svg</a></p></td>
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Wappen_Preußische_Provinzen_-_Brandenburg.png" title="fig:Wappen_Preußische_Provinzen_-_Brandenburg.png">Wappen_Preußische_Provinzen_-_Brandenburg.png</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><small>[ <a href="../Page/旗帜.md" title="wikilink">旗帜</a> ]</small></p></td>
<td style="text-align: center;"><p><small>[ <a href="../Page/徽章.md" title="wikilink">徽章</a> ]</small></p></td>
</tr>
</tbody>
</table></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>存在时期</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong><a href="../Page/首府.md" title="wikilink">首府</a></strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong><a href="../Page/人口.md" title="wikilink">人口</a></strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong><a href="../Page/人口密度.md" title="wikilink">人口密度</a></strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>版图</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:German_Empire_-_Prussia_-_Brandenburg_(1871).svg" title="fig:German_Empire_-_Prussia_-_Brandenburg_(1871).svg">German_Empire_-_Prussia_-_Brandenburg_(1871).svg</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1871年德意志帝国时期，普鲁士王国内的勃兰登堡省</p></td>
</tr>
</tbody>
</table>

**勃兰登堡省**
（）是1815年至1946年间[普鲁士和之后的](../Page/普鲁士.md "wikilink")[普鲁士自由邦的一个省分](../Page/普鲁士自由邦.md "wikilink")。省的首府原本是[波茨坦](../Page/波茨坦.md "wikilink")，1827年迁到[柏林](../Page/柏林.md "wikilink")，1843年又迁回波茨坦，最后1918年迁到[夏洛特堡](../Page/夏洛特堡.md "wikilink")。

## 历史

勃兰登堡省主要由前[勃兰登堡侯国的领土组成](../Page/勃兰登堡侯国.md "wikilink")，统治侯国的[霍亨索伦王朝](../Page/霍亨索伦王朝.md "wikilink")1618年继承了[普鲁士公国](../Page/普鲁士公国.md "wikilink")，成为了[勃兰登堡-普鲁士](../Page/勃兰登堡-普鲁士.md "wikilink")。省主要的领土变更是省的西部边界延伸到[易北河以东](../Page/易北河.md "wikilink")，易北河以西的[阿尔特马克](../Page/阿尔特马克.md "wikilink")（老边境）地区则并入新组成的[萨克森省](../Page/萨克森省.md "wikilink")。[奥得河东的](../Page/奥得河.md "wikilink")[纽马克](../Page/纽马克.md "wikilink")（新边境）地区和下[卢萨蒂亚都保留到勃兰登堡之内](../Page/卢萨蒂亚.md "wikilink")。

1920年的[大柏林法案令](../Page/大柏林法案.md "wikilink")[柏林脱离勃兰登堡](../Page/柏林.md "wikilink")，并组成独立的一个省。法案亦扩张了柏林市的版图，并入了邻近的大量地区和市镇，组成了大柏林地区（）。

1946年[二次大战之后](../Page/二次大战.md "wikilink")，[奥德河-尼斯河线以东的勃兰登堡地区被割让到波兰](../Page/奥德河-尼斯河线.md "wikilink")，以组成[波兹南省](../Page/波兹南省.md "wikilink")。剩余的地区成为苏联占领区，1949年成为[东德一部分](../Page/东德.md "wikilink")。1990年[两德统一后](../Page/两德统一.md "wikilink")[勃兰登堡重新成为](../Page/勃兰登堡.md "wikilink")[联邦德国的](../Page/联邦德国.md "wikilink")[联邦州](../Page/联邦州.md "wikilink")。

## 另見

  - [勃兰登堡](../Page/勃兰登堡.md "wikilink")
  - [勃兰登堡侯国](../Page/勃兰登堡侯国.md "wikilink")
  - [普鲁士](../Page/普鲁士.md "wikilink")

[Category:普鲁士历史](../Category/普鲁士历史.md "wikilink")
[Category:1815年建立的國家或政權](../Category/1815年建立的國家或政權.md "wikilink")
[Category:1946年終結的國家或政權](../Category/1946年終結的國家或政權.md "wikilink")
[Category:1946年德國廢除](../Category/1946年德國廢除.md "wikilink")