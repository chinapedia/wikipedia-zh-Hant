**勃印曩**（；），《[明史](../Page/明史.md "wikilink")》称之为**莽應龍**，意为「王兄」\[1\]\[2\]，本名**耶圖**（）\[3\]。緬甸[東吁王朝的統治者之一](../Page/東吁王朝.md "wikilink")，[德彬瑞蒂的繼承者](../Page/德彬瑞蒂.md "wikilink")。1551年至1581年在位。1551年勃印曩打败自己的弟弟东吁王明康，即位于东吁，两年后灭亡了[孟族政权](../Page/孟族.md "wikilink")，建都于[汉达瓦底](../Page/勃固.md "wikilink")。勃印曩在[緬甸廣泛受到崇敬](../Page/緬甸.md "wikilink")，被冠以「大帝」的尊稱。在位期間，將[撣邦](../Page/撣邦.md "wikilink")、[寮國與](../Page/寮國.md "wikilink")[蘭納併入緬甸領土](../Page/蘭納.md "wikilink")。曾编制律书法典，统一度量衡，发展冶铁、炼铜等手工业，对当时经济文化有发展，但因征战频繁，人民遭受深重灾难。1581年，他去世於[下緬甸地區的](../Page/下緬甸.md "wikilink")[汉达瓦底](../Page/勃固.md "wikilink")。

## 參見

  - [東吁王朝](../Page/東吁王朝.md "wikilink")

## 參考文獻

  - 引用

<!-- end list -->

  - 書籍

<!-- end list -->

  -
  -
  -
  - 大野徹，謎の仏教王国パガン，NHKブックス，2002年

[Category:緬甸歷史](../Category/緬甸歷史.md "wikilink")
[Category:緬甸君主](../Category/緬甸君主.md "wikilink")

1.  《緬甸史》，戈·埃·哈威著，姚梓良譯，371頁
2.  Hmannan Yazawin 2003，第二卷，193頁
3.  Thaw Kaung 2010，102–103頁