**勒蘇維斯龍屬**（[學名](../Page/學名.md "wikilink")：*Lexovisaurus*）是最早被發現的該時代歐洲[恐龍之一](../Page/恐龍.md "wikilink")，屬於[劍龍科](../Page/劍龍科.md "wikilink")，生存於[侏羅紀中至晚期](../Page/侏羅紀.md "wikilink")（約1億6500萬年前）的[歐洲](../Page/歐洲.md "wikilink")，是以[法國古代的部落Lexovi為名](../Page/法國.md "wikilink")。牠的[化石是一些骨甲及四肢骨頭](../Page/化石.md "wikilink")，發現於[法國及](../Page/法國.md "wikilink")[英格蘭北部](../Page/英格蘭.md "wikilink")。
[Lexovisaurus.jpg](https://zh.wikipedia.org/wiki/File:Lexovisaurus.jpg "fig:Lexovisaurus.jpg")
法國標本顯示勒蘇維斯龍可能很像[劍龍](../Page/劍龍.md "wikilink")。在牠的肩上或臀部可能有一對長刺，背部及尾巴都有扁平骨甲及圓尖刺。勒蘇維斯龍可能有5米長。[模式種是](../Page/模式種.md "wikilink")*L.
durobrivensis*，是由Hoffstetter於1957年描述及命名的。[模式標本原先被分類在](../Page/模式標本.md "wikilink")*Omosaurus*（現為[銳龍](../Page/銳龍.md "wikilink")）之中。

近年的重新研究顯示勒蘇維斯龍沒有可鑑定特徵，因此部份化石被建立為新屬，[鎧甲龍](../Page/鎧甲龍.md "wikilink")\[1\]。

## 相關條目

  - [鎧甲龍](../Page/鎧甲龍.md "wikilink")

## 參考書籍

  - [*Lexovisaurus*](https://web.archive.org/web/20090729092426/http://www.thescelosaurus.com/stegosauria.htm)
    at *Thescelosaurus*\!
  - Benton, Michael. 1992. *Dinosaur and other prehistoric animal Fact
    Finder* 1992

## 外部連結

  - <https://web.archive.org/web/20090729092426/http://www.thescelosaurus.com/stegosauria.htm>
  - <https://web.archive.org/web/20080113185734/http://www.dinoruss.org/de_4/5a77866.htm>
  - [恐龍博物館──勒蘇維斯龍](https://web.archive.org/web/20071109033949/http://www.dinosaur.net.cn/museum/Lexovisaurus.htm)

[Category:劍龍下目](../Category/劍龍下目.md "wikilink")
[Category:歐洲恐龍](../Category/歐洲恐龍.md "wikilink")
[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")

1.