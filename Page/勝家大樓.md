**勝家大樓**（[英语](../Page/英语.md "wikilink")：Singer Building）
位於[美國](../Page/美國.md "wikilink")[紐約市](../Page/紐約市.md "wikilink")[曼哈頓](../Page/曼哈頓.md "wikilink")[百老匯大街和自由街](../Page/百老匯.md "wikilink")（Liberty
Street）的路口，建於1908年，是美國[胜家公司的總部](../Page/胜家公司.md "wikilink")。大樓樓高612英呎（187米），樓高47層，建成後曾為世界最高[摩天大樓](../Page/摩天大樓.md "wikilink")，直到翌年（1909年）被同樣位於曼哈頓的[大都會人壽保險大樓取代](../Page/大都會人壽保險大樓.md "wikilink")。最後於1968年拆除，成為世界最高已拆除摩天大樓，直到2001年9月11日，[世界貿易中心](../Page/世界貿易中心.md "wikilink")[倒塌為止](../Page/九一一襲擊事件.md "wikilink")。

## 參見

  - [胜家公司](../Page/胜家公司.md "wikilink")
  - [美國摩天大樓](../Page/美國摩天大樓.md "wikilink")

## 外部連結

  - [Old postcard view of the Singer Building on
    bc.edu](http://www.bc.edu/bc_org/avp/cas/fnart/fa267/20th/singer1.jpg)

[Category:曼哈頓摩天大樓](../Category/曼哈頓摩天大樓.md "wikilink")
[Category:美国建筑的世界之最](../Category/美国建筑的世界之最.md "wikilink")
[Category:前世界最高建築](../Category/前世界最高建築.md "wikilink")
[Category:150米至199米高的摩天大樓](../Category/150米至199米高的摩天大樓.md "wikilink")
[Category:已不存在的摩天大樓](../Category/已不存在的摩天大樓.md "wikilink")
[Category:1908年完工建築物](../Category/1908年完工建築物.md "wikilink")
[Category:1908年紐約州建立](../Category/1908年紐約州建立.md "wikilink")