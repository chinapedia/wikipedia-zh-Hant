**勝興車站**位於[台灣](../Page/台灣.md "wikilink")[苗栗縣](../Page/苗栗縣.md "wikilink")[三義鄉](../Page/三義鄉_\(台灣\).md "wikilink")，原為[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[舊山線](../Page/舊山線.md "wikilink")（臺中線舊線）的[鐵路車站](../Page/鐵路車站.md "wikilink")，原已因鐵路改線而裁撤，後於2010年6月5日舊山線復駛而重新設站，現為苗栗縣著名觀光景點。

相對於臺中線（山線）之新線，原廢止線習稱舊山線，參見[舊山線條目](../Page/舊山線.md "wikilink")。

## 車站構造

  - 台鐵[海拔最高的車站](../Page/海拔.md "wikilink")，標高402.326公尺，並在車站內軌道側豎有一石造紀念碑標示出車站高度。
  - 木造站房。
  - 岸式月台、島式月台各一座。

## 利用狀況

<File:TRA> Shengsing Station Birdview
south.jpg|從高處俯瞰，可以看得出勝興車站是位在一個[山坳之中](../Page/山坳.md "wikilink")
<File:TRA> Shengsing Station
Birdview.jpg|勝興車站的鐵軌側，路線旁的石坡上還可以見到車站的舊名「十六份」之拼字

目前為觀光用途，且為苗栗縣縣定古蹟，周邊含車站本體、廣場、山洞、月台間之平行主副線鐵軌、倉庫、宿舍、道班房與油庫等公有設施則被文化局登錄為交通類文化景觀。

## 車站周邊

  - [勝興老街](../Page/勝興老街.md "wikilink")
  - 挑柴古道
  - [苗49線](../Page/苗49線.md "wikilink")
  - [苗49-1線](../Page/苗49-1線.md "wikilink")

## 歷史

  - 1907年4月1日：設置**十六份信號場**（[號誌站](../Page/號誌站.md "wikilink")）。\[1\]
  - 1930年4月1日：升為驛（[車站](../Page/車站.md "wikilink")）。\[2\]
  - 1935年4月21日：[新竹台中地震](../Page/1935年新竹台中地震.md "wikilink")，站房無礙。
  - 1958年2月10日：與[南州](../Page/南州車站.md "wikilink")、[港嘴](../Page/港嘴車站.md "wikilink")（廢止）、[內壢等車站同時改稱今名](../Page/內壢車站.md "wikilink")。\[3\]
  - 1998年9月23日：改線廢止。\[4\]
  - 1999年4月16日：登錄為苗栗縣縣定古蹟。
  - 2010年6月5日：[舊山線復駛](../Page/舊山線.md "wikilink")，僅行駛少數列次，並以[CK124蒸氣火車行駛睽違](../Page/CK124.md "wikilink")12年的[舊山線](../Page/舊山線.md "wikilink")，同時慶祝6月9日123週年[鐵路節暨](../Page/鐵路節.md "wikilink")[舊山線ROT暖身](../Page/舊山線.md "wikilink")，以帶動鐵道觀光旅遊新風潮，開行「CK124舊山線風情半日遊」懷舊郵輪式列車。預計於同年9月公開招商交由民間營運。

## 鄰近車站

## 相關條目

  - [舊山線](../Page/舊山線.md "wikilink")

## 參考文獻

## 外部連結

  - [勝興車站（臺灣鐵路管理局）](https://web.archive.org/web/20100710184510/http://service.tra.gov.tw/Taichung-Transportation/CP/11209/stations33.aspx)
  - [記憶苗栗舊山線](http://lib.mlc.gov.tw/webmlr/)
  - [文化部文化資產局-勝興火車站](https://nchdb.boch.gov.tw/assets/advanceSearch/monument/19990416000001)
  - [勝興車站(VR
    實景旅遊網)](http://www.vrwalker.net/tw/scenery_view.php?tbname=scenerys&serno=42)
  - [勝興車站](http://bluezz.tw/c.php?id=1007)
  - [勝興站 - Google
    Search](https://www.google.com.tw/maps/preview/uv?hl=en&pb=!1s0x3469042cfabb8913:0xf8a628ca575dc296!2m5!2m2!1i80!2i80!3m1!2i100!3m1!7e1!4shttp://www.panoramio.com/photo/3559662!5s%E5%8B%9D%E8%88%88%E7%AB%99+-+Google+Search&sa=X&ei=_L5rU_yOH4_k8AXqnICwCg&ved=0CNABEKIqMBA)

[Category:苗栗縣鐵路車站](../Category/苗栗縣鐵路車站.md "wikilink")
[三](../Category/苗栗縣旅遊景點.md "wikilink")
[苗](../Category/台灣鐵道旅遊景點.md "wikilink")
[三](../Category/苗栗縣古蹟.md "wikilink")
[苗](../Category/台灣文化景觀.md "wikilink") [Category:三義鄉
(台灣)](../Category/三義鄉_\(台灣\).md "wikilink")
[Category:1907年啟用的鐵路車站](../Category/1907年啟用的鐵路車站.md "wikilink")
[Category:台灣日治時期交通建築](../Category/台灣日治時期交通建築.md "wikilink")
[苗](../Category/臺灣鐵路文化資產.md "wikilink")

1.

2.
3.
4.