**勞勃·韋納**（，）是一位[美國家喻戶曉的演技派知名電視](../Page/美國.md "wikilink")、電影演員；他因演出三部著名美國全國聯播[連續劇電視影集成名美國藝壇](../Page/連續劇.md "wikilink")30年；現年事已大，已處於半[退休狀態](../Page/退休.md "wikilink")，但仍為各國影迷津津樂道回憶著他；他自言自己在1950年代初踏入影壇時模仿及崇拜[偶像是](../Page/偶像.md "wikilink")[好萊塢影星](../Page/好萊塢.md "wikilink")[史賓塞·屈賽](../Page/史賓塞·屈賽.md "wikilink")。

## 早年

  - 出生於美國[密西根州](../Page/密西根州.md "wikilink")[底特律](../Page/底特律.md "wikilink")，父親是鐵公司職員；在韋納7歲時全家搬遷到[加州](../Page/加州.md "wikilink")[洛杉磯](../Page/洛杉磯.md "wikilink")；他相當早青年時就參加演員演出，都是小角色，特別是演出像[克拉克蓋博這樣類型紳士演出](../Page/克拉克蓋博.md "wikilink")。
  - 有天勞勃韋納在好萊塢[比佛利山莊某家餐廳用餐時被星探邀請演出](../Page/比佛利山莊.md "wikilink")，終於演男主角：1950年第一次電影演出《快樂年代》；之後他又演出數部配角的[戰爭片](../Page/戰爭片.md "wikilink")，直到1952年又演出[傳記式電影](../Page/傳記.md "wikilink")《With
    a Song in My
    Heart》成名，此片女主角影星[蘇珊海華還安排勞勃韋納與](../Page/蘇珊海華.md "wikilink")[福斯電影公司簽演員約](../Page/福斯電影公司.md "wikilink")。
  - 在福斯時代他還認識女星[琼·柯林斯及](../Page/琼·柯林斯.md "wikilink")[黛比·雷诺斯](../Page/黛比·雷诺斯.md "wikilink")，至今50年之紅粉知交。

## 部份影視作品

### 電影

  - 第一次電影演出《快樂年代》The Happy Years1950年
  - 《With a Song in My Heart》1952年 成名作
  - 1953年《[鐵達尼號](../Page/鐵達尼號_\(1953年電影\).md "wikilink")》
  - 1954年《[斷戈浴血記](../Page/斷戈浴血記.md "wikilink")》（*[Broken
    Lance](../Page/:en:Broken_Lance.md "wikilink")*）
  - 1962年《[碧血長天](../Page/碧血長天.md "wikilink")》（*The Longest Day*）
  - 1963年《[粉紅豹](../Page/粉紅豹.md "wikilink")》（*he Pink Panther*）
  - 1983年《[粉紅豹之探長的詛咒](../Page/粉紅豹之探長的詛咒.md "wikilink")》（*Curse of the
    Pink Panther*）
  - 1993年《[李小龍傳](../Page/李小龍傳_\(1993年美國電影\).md "wikilink")》
  - 1997年《[王牌大賤諜](../Page/王牌大賤諜.md "wikilink")》

### 電視

  - 演出數部偵探影集

妙賊

## 花邊新聞

  - 與影星[娜妲麗華離婚又復合](../Page/娜妲麗華.md "wikilink")，共15年的婚姻，頗為知名，1981年11月29日娜妲麗華溺水身亡，罗伯特此后成为了长期被调查对象。

## 獎項

2007 Man in the Chair

## 参考资料

## 外部链接

  -
  -
  -
  -
[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:21世紀美國男演員](../Category/21世紀美國男演員.md "wikilink")
[Category:21世纪美国作家](../Category/21世纪美国作家.md "wikilink")
[Category:美國電影男演員](../Category/美國電影男演員.md "wikilink")
[Category:美國電視男演員](../Category/美國電視男演員.md "wikilink")
[Category:美国回忆录撰写人](../Category/美国回忆录撰写人.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:挪威裔美國人](../Category/挪威裔美國人.md "wikilink")
[Category:洛杉矶市男演员](../Category/洛杉矶市男演员.md "wikilink")