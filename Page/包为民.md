**包为民**（）原籍[浙江省](../Page/浙江省.md "wikilink")[镇海](../Page/镇海.md "wikilink")，生于[黑龙江省](../Page/黑龙江省.md "wikilink")[哈尔滨](../Page/哈尔滨市.md "wikilink")。制导与控制专家，[中国科学院院士](../Page/中国科学院院士.md "wikilink")。

## 生平

1982年8月，毕业于[西北电讯工程学院电子工程系信息处理专业](../Page/西安电子科技大学.md "wikilink")。他是[中国航天运载器总体及控制系统领域的学术带头人](../Page/中国.md "wikilink")，国家重点工程的总[设计师](../Page/设计师.md "wikilink")。2005年，当选为[中国科学院院士](../Page/中国科学院.md "wikilink")\[1\]。2018年1月，当选[第十三届全国政协委员](../Page/中国人民政治协商会议第十三届全国委员会委员名单.md "wikilink")\[2\]。

## 参考文献

## 外部链接

  - [制导与控制专家
    包为民院士](http://www.zhxww.net/zhnews401/zh2/zh02/20070615145015.htm)

[Category:黑龙江科学家](../Category/黑龙江科学家.md "wikilink")
[Category:浙江科学家](../Category/浙江科学家.md "wikilink")
[Category:西安电子科技大学校友](../Category/西安电子科技大学校友.md "wikilink")
[Category:哈尔滨人](../Category/哈尔滨人.md "wikilink")
[Category:镇海县人](../Category/镇海县人.md "wikilink")
[Category:第十三届全国政协委员](../Category/第十三届全国政协委员.md "wikilink")
[W为民](../Category/包姓.md "wikilink")

1.  [1](http://www.casad.ac.cn/showuserinfo.asp?userid=3933&act=info)
2.