在[數學裡](../Page/數學.md "wikilink")，若*A*為*B*的[子集](../Page/子集.md "wikilink")，則其**包含映射**為一[函數](../Page/函數.md "wikilink")，其將*A*的每一元素映射至*B*內的同一元素：

  -
    *i*:*A* → *B*, *i*(*x*) = *x*.

「有鉤箭頭」\(\hookrightarrow\)有時被用來標記一內含映射。

此一及其他類似的由子結構映射的[單射函數有時會被稱為](../Page/單射.md "wikilink")***自然單射***。

給定任一於[對象](../Page/範疇論.md "wikilink")*X*和*Y*之間的[態射](../Page/態射.md "wikilink")，若存在一映射至其[定義域的內含映射](../Page/定義域.md "wikilink")*i*:*A*→*X*，則可形成一*f*的[限制](../Page/函數#限制及擴張.md "wikilink")*\(/fi\)*:*A*→*Y*。在許多的例子內，亦可以建立一映射至[陪域的內含映射](../Page/陪域.md "wikilink")*R*→*Y*，其中*R*為*f*[值域的子集](../Page/值域.md "wikilink")。

## 內含映射

內含映射傾向於[代數結構的](../Page/代數結構.md "wikilink")[同態](../Page/同態.md "wikilink")；更精確地說，給定一於某些運算下封閉的子結構，其內含映射將會是一個同態，因為由其定義可得出的一當然原因。例如，一二元運算
，其需要有

\[\iota(x\star y)=\iota(x)\star \iota(y)\]

因為
在子模型和大模型裡的運算一致。在[一元運算的情況下也是類似的](../Page/一元運算.md "wikilink")；但也要注意零元運算，其給出一*常數*元素。這裡的重點在於其[封閉性](../Page/封閉性.md "wikilink")，表示其常數必須於子結構內。

[微分幾何中有多種不同的的內含映射](../Page/微分幾何.md "wikilink")，例如[子流形的嵌入](../Page/子流形.md "wikilink")；由此可導出某些反變對象（例如[微分形式](../Page/微分形式.md "wikilink")）的「限制映射」，其方向恰好相反。在[代數幾何中的內含映射則稍複雜](../Page/代數幾何.md "wikilink")，此時不僅須考慮底層拓撲空間的映射，也須考慮結構層的同態，例如以下兩個[交換環譜的包含映射](../Page/交換環譜.md "wikilink")

\[\operatorname{Spec}\left(R/I\right) \to \operatorname{Spec}(R)\]

\[\operatorname{Spec}\left(R/I^2\right) \to \operatorname{Spec}(R)\]
儘管拓撲上一致，卻是不同的映射；其中  是[交換環而](../Page/環.md "wikilink")
是其[理想](../Page/理想_\(環論\).md "wikilink")。

## 另見

  - [恆等函數](../Page/恆等函數.md "wikilink")

[pl:Inkluzja
(matematyka)](../Page/pl:Inkluzja_\(matematyka\).md "wikilink")

[B](../Category/集合論基本概念.md "wikilink")
[Category:函数](../Category/函数.md "wikilink")