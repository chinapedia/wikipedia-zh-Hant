**包拯**（），[字](../Page/表字.md "wikilink")**希仁**，[廬州](../Page/廬州.md "wikilink")[合肥](../Page/合肥.md "wikilink")（今[安徽省](../Page/安徽省.md "wikilink")[合肥市](../Page/合肥市.md "wikilink")[肥东](../Page/肥东.md "wikilink")）人，[北宋人](../Page/北宋.md "wikilink")，官至[枢密副使](../Page/枢密副使.md "wikilink")、[朝散大夫](../Page/朝散大夫.md "wikilink")、[给事中](../Page/给事中.md "wikilink")、[上轻车都尉](../Page/上轻车都尉.md "wikilink")，封[东海郡](../Page/东海郡.md "wikilink")[开国侯](../Page/开国侯.md "wikilink")、食邑一千八百户、食实封四百户，赐紫色金魚袋。赠[礼部尚书](../Page/礼部尚书.md "wikilink")、谥号“孝肃”。包拯以清廉公正聞名於世，被后世稱譽为「**包青天**」，將他奉為神明崇拜。[中國民間信仰傳其為](../Page/中國民間信仰.md "wikilink")[文曲星轉世](../Page/文曲星.md "wikilink")，死後成為[地獄](../Page/地獄.md "wikilink")[第五殿閻羅王之一](../Page/十殿閻羅王.md "wikilink")，亦稱[森羅殿主及](../Page/森羅殿主.md "wikilink")[閻羅天子](../Page/閻羅天子.md "wikilink")、[包府千歲](../Page/包府千歲.md "wikilink")、[文曲星君](../Page/文曲星君.md "wikilink")。

## 别名

[BaoZheng_detail.jpg](https://zh.wikipedia.org/wiki/File:BaoZheng_detail.jpg "fig:BaoZheng_detail.jpg")
包拯曾任[天章閣](../Page/天章閣.md "wikilink")[待制](../Page/待制.md "wikilink")，人稱「**包待制**」；后进为[龙图阁](../Page/龙图阁.md "wikilink")[直学士](../Page/直学士.md "wikilink")，故后人亦称「**包龙图**」。此外，其形象传说为黑面，故此亦被称为「**包黑子**」、「**包黑炭**」。

## 生平

[宋真宗](../Page/宋真宗.md "wikilink")[咸平二年](../Page/咸平.md "wikilink")（999年），包拯生于[庐州](../Page/庐州.md "wikilink")[合肥](../Page/合肥.md "wikilink")。[仁宗](../Page/宋仁宗.md "wikilink")[天圣五年](../Page/天圣.md "wikilink")（1027年）中丁卯科[进士](../Page/进士.md "wikilink")\[1\]，授大理评事（从八品下），知建昌县；以父母年老，没有赴任。又监和州税收，其父母不想让他离开，于是一直在家乡侍奉父母。几年之后，他的父母亲相继去世，包拯在双亲的墓旁筑起草庐，直到守丧期满\[2\]。调知[天长县](../Page/天长县.md "wikilink")。任滿後，調任[端州](../Page/端州.md "wikilink")（[廣東](../Page/廣東.md "wikilink")[肇慶](../Page/肇慶.md "wikilink")）[知州](../Page/知州.md "wikilink")。回京任[監察御史](../Page/監察御史.md "wikilink")[里行](../Page/里行.md "wikilink")，又改[監察御史](../Page/監察御史.md "wikilink")，為「[言事官](../Page/言事官.md "wikilink")」（對處事不當、行事不法的官僚，都可以進行[彈劾](../Page/彈劾.md "wikilink")）。為懲治貪官，[宋仁宗](../Page/宋仁宗.md "wikilink")[慶曆四年](../Page/慶曆.md "wikilink")（1044年），他向宋仁宗上書《乞不用贓吏》，認為清廉是人們的表率，而貪贓則是「民賊」。包拯七次上書彈奏江西轉運使[王逵](../Page/王逵_\(宋朝\).md "wikilink")，揭露他「心同蛇蠍」、殘害百姓，並嚴厲批評宋廷的任官制度。宋仁宗[皇祐二年](../Page/皇祐.md "wikilink")（1050年）至三年間，包拯[知諫院](../Page/知諫院.md "wikilink")，三次彈劾[外戚](../Page/外戚.md "wikilink")[張堯佐](../Page/張堯佐.md "wikilink")，稱其“真清朝之秽污，白昼之魑魅”，又審清妖人[冷青冒充皇子案](../Page/冷青.md "wikilink")，震動朝野。

宋仁宗嘉祐二年12月（1057年1月），包拯任[權知開封府](../Page/開封府尹.md "wikilink")，\[3\]至嘉祐三年六月離任，前後只有一年有餘。但在這短短的時間內，把號稱難治的[開封府](../Page/開封府.md "wikilink")，治理得井井有條。敢於懲治權貴們的不法行為，堅決抑制開封府吏的驕橫之勢，並能夠及時懲辦誣賴刁民。包拯公正廉明、明察秋毫、鐵面無私、斷案如神，因此受人敬仰。

宋仁宗嘉祐六年（1061年），他進入「[二府](../Page/二府.md "wikilink")」成為北宋最高決策機關成員（[樞密副使](../Page/樞密副使.md "wikilink")）後，衣著飲食和器具依然「如布衣時」，是[古代中国清官的典型代表](../Page/古代中国.md "wikilink")，民間[諺語有](../Page/諺語.md "wikilink")-{云}-：“关节不到，有[閻羅包老](../Page/閻羅.md "wikilink")。”包拯為人嚴正，殊少笑容，時人以“[黃河清](../Page/黃河.md "wikilink")”比喻包拯之笑。曾與包拯同朝為官的[歐陽修](../Page/歐陽修.md "wikilink")、[司馬光](../Page/司馬光.md "wikilink")，乃至後世如[朱熹](../Page/朱熹.md "wikilink")、[劉敞等](../Page/劉敞.md "wikilink")，對包拯皆有正面評價。

包公於[宋仁宗趙禎](../Page/宋仁宗趙禎.md "wikilink")[嘉祐七年](../Page/嘉祐.md "wikilink")（1062年）农历五月二十四日（7月3日），病殁於[開封](../Page/開封.md "wikilink")。[宋仁宗趙禎加封包公為東海郡開國侯](../Page/宋仁宗趙禎.md "wikilink")，贈官禮部尚書，諡孝肅，還根據包公「少有孝行，聞於鄉里；晚有直節，著於朝廷。」這句話追諡包公為**孝肅**，妻子董氏把包拯生前奏議底稿交付至門生[張田輯錄成](../Page/張田.md "wikilink")《孝肅包公奏議》（即《包拯集》）傳世，次年歸葬合肥，墓誌銘由同為[樞密副使的騎都尉](../Page/樞密.md "wikilink")、濮陽縣開國子[吳奎撰寫](../Page/吳奎.md "wikilink")、朝奉郎、上騎都尉[楊南仲書寫以及甥將仕郎](../Page/楊南仲.md "wikilink")、守溫州里安縣令[文勳篆蓋](../Page/文勳.md "wikilink")（現存[安徽省博物館](../Page/安徽省博物館.md "wikilink")）。

## 家族與遺跡

[Baogongmu2.jpg](https://zh.wikipedia.org/wiki/File:Baogongmu2.jpg "fig:Baogongmu2.jpg")
包拯父包令儀為太平興國八年進士，官至刑部侍郎。祖父包士通是平民。\[4\]\[5\]據出土於1973年包公墓的包公墓铭記載：包拯先後有三妻，分別為張氏、董氏和媵孫氏（媵指隨嫁之侍婢，或可指妾侍）\[6\]。妻子董氏於[宋神宗趙頊](../Page/宋神宗趙頊.md "wikilink")[熙寧元年](../Page/熙寧.md "wikilink")（1068年）病逝於[合肥](../Page/合肥.md "wikilink")，與包拯合葬。

包拯與董氏生有一子[包繶](../Page/包繶.md "wikilink")，但他婚後兩年染病身亡，其子（即包拯孫子）[包文輔於五歲時夭折](../Page/包文輔.md "wikilink")，長媳崔氏則於[宋哲宗](../Page/宋哲宗.md "wikilink")[紹聖元年](../Page/紹聖.md "wikilink")（1094年）過世，享年62歲。

後來媵孫氏懷孕，包拯將其遣送回家，媳婦崔氏得悉後妥為照顧。1058年媵孫氏為包拯生下一子，家族得以繁衍，拯替他取名包綖，崔氏幫他改名為[包綬](../Page/包綬.md "wikilink")。包綬成過兩次親，第一次是娶包公門生、做過[廬州知州的](../Page/廬州知州.md "wikilink")[張田的女兒](../Page/張田.md "wikilink")，張氏早包綬而死；第二任妻子是宰相[文彥博的小女兒](../Page/文彥博.md "wikilink")，出身相門的文氏，並不是一位雍容華貴的嬌小姐，她恬靜寡欲，生活儉樸，待人和善，從不以勢自居，見他人有難，還樂於慷慨接濟。她經常[吃素](../Page/素食主义.md "wikilink")，與丈夫包綬一同受過道教的洗禮，視富貴如糞土，在北宋百餘年的太平時代下，包綬夫妻二人嚴守父命，看重節操。文氏早包綬四年去世，於[宋徽宗趙佶](../Page/宋徽宗趙佶.md "wikilink")[建中靖國元年病故](../Page/建中靖國.md "wikilink")，享年僅三十多歲；包公另有兩女。

### 文化大革命的破壞

[Baogongmu1.jpg](https://zh.wikipedia.org/wiki/File:Baogongmu1.jpg "fig:Baogongmu1.jpg")

包公病歿後一年1063年歸葬於今日合肥市東郊大興集，該墓曾於1199年由[淮西路官員重修](../Page/淮西路.md "wikilink")。[文革時期批臭清官的謬論風潮盛起](../Page/文革.md "wikilink")，清官被視為「比貪官更壞」，「具更大欺騙性」，「鞏固封建主義」的批鬥對象，故而清官形象代表人物包公成為眾矢之的，遺址文物受到嚴重的破壞，合肥市包河公园中的包公祠被洗劫一空，包公塑像被粉身碎骨，包公后裔世代相传保存下来的包公画像和《包氏宗谱》付之一炬。

1973年3月時包公墓及包氏宗族墓群因當時合肥市[革命委員會冶金建设指挥部以徵用工廠的建設用地的理由被強行](../Page/革命委員會.md "wikilink")「遷墳」，而在無法阻止的情況下，相關人員為搶救文物成立一个「包公墓清理發掘领导小组」進行包公墓的挖掘與清理，發掘出包公遺骨及新發現的兩塊墓銘石《宋枢密副使赠礼部尚书孝肃包公墓铭》、《宋故永康郡夫人董氏墓志铭》，並發現包公墓實為包公和董氏（第二任妻子）合葬墓，而且曾因受破壞而遷移，另外也挖掘與清理了長子包紹夫婦墓、次子包綬夫婦墓、孫子包永年墓。

但之後包公及其家人的遺骨因族人害怕文革迫害（例如：當時当地的[公社](../Page/人民公社.md "wikilink")[书记以](../Page/党委书记.md "wikilink")「搞封建宗教活动，大包村的土地不能让封建社会的孝子贤孙给抹黑」为由不允许包公的遺骨下葬，否则立即销毁），另行偷偷安葬而不知所終，僅留下曾送往北京進行鑑證的34块包公遺骨，現存於新建的包公墓園\[7\]。

而另一相關古跡「包公井」原址亦搶救未果，於1986年成為[合肥市第四中學](../Page/合肥市第四中學.md "wikilink")（[合肥四中](../Page/合肥四中.md "wikilink")）的一幢五層樓[宿舍](../Page/宿舍.md "wikilink")。1985年重建包公墓园於合肥市內包河南畔林區內，保留了舊墓群的包公遺骨及文物，1987年落成，與包公祠緊緊相連。

### 後世子孫

  - 8世孙：[包遜](../Page/包遜.md "wikilink")
  - 9世孙：[包恢](../Page/包恢.md "wikilink")
  - 27世孙：[包方务](../Page/包方务.md "wikilink")
  - 28世孫：[包兆龍](../Page/包兆龍.md "wikilink")
  - 29世孫：[包玉剛](../Page/包玉剛.md "wikilink")、[包玉書](../Page/包玉書.md "wikilink")、[包德明](../Page/包德明.md "wikilink")
  - 30世孫：[包陪慶](../Page/包陪慶.md "wikilink")
  - 32世孫：[包振銘](../Page/包振銘.md "wikilink")
  - 33世孫：[包华成](../Page/包华成.md "wikilink")、[包华章](../Page/包华章.md "wikilink")、[包华兵](../Page/包华兵.md "wikilink")、[包华军](../Page/包华军.md "wikilink")、[包华秀](../Page/包华秀.md "wikilink")、[包胜东](../Page/包胜东.md "wikilink")、包大銘、[包偉銘](../Page/包偉銘.md "wikilink")、[包小松](../Page/包小松.md "wikilink")、[包小柏](../Page/包小柏.md "wikilink")
  - 34世孫：[包庭政](../Page/包庭政.md "wikilink")、[包玺](../Page/包玺.md "wikilink")、[包丹](../Page/包丹.md "wikilink")、[包慧芳](../Page/包慧芳.md "wikilink")、[包勇](../Page/包勇.md "wikilink")、[包遵信](../Page/包遵信.md "wikilink")

## 評價

包拯在當朝以清正剛直著稱，直到後世仍然被後人所尊敬。

  - [朱熹稱包公](../Page/朱熹.md "wikilink")：「復為京尹，令行禁止，立朝剛毅。」
  - [欧阳修稱包公](../Page/欧阳修.md "wikilink")：「清節美行，著自貧賤，讜言正論，聞於朝廷。」
  - [劉敞稱包公](../Page/劉敞.md "wikilink")：「識清氣勁，直而不撓；凜乎有歲寒之操。」
  - 奉旨編集《[資治通鑑](../Page/資治通鑑.md "wikilink")》的[司馬光也稱讚包公](../Page/司馬光.md "wikilink")：「仁宋時，包拯最名正直。」
  - 《[宋史](../Page/宋史.md "wikilink")》：「公性峭直，惡吏苛刻，務敦厚，雖其嫉忠，而未嘗不推以忠恕也。與人不苟合，不為辭色悅人，平居無私書，故人、親黨皆絕之。雖貴，衣服、器用、飲食如布衣時。」
  - [胡适說包公](../Page/胡适.md "wikilink")“是一個箭垛式的人物”，民間的傳說將各種各樣的斷案故事都射到他身上。胡适在《[三侠五义](../Page/三侠五义.md "wikilink")·序》说《宋史》只记包拯“
    割牛舌” 一案图。

正史中的包拯雖然清直，但是以敦厚著稱。後世傳說，包拯審案時，好用[酷刑](../Page/酷刑.md "wikilink")，這可能是與他的九世孫[包恢的事蹟混淆所致](../Page/包恢.md "wikilink")。

## 傳說形象

[开封包公祠.JPG](https://zh.wikipedia.org/wiki/File:开封包公祠.JPG "fig:开封包公祠.JPG")

[四大小說之一的](../Page/四大小說.md "wikilink")《[水滸傳](../Page/水滸傳.md "wikilink")》曾指出包公是[文曲星的](../Page/文曲星.md "wikilink")[轉世](../Page/轉世.md "wikilink")，他與[狄青一文一武開創了北宋的全盛時期](../Page/狄青.md "wikilink")。

古代小說《[七俠五義](../Page/七俠五義.md "wikilink")》以包公為主人公。相傳為[文曲星轉世](../Page/文曲星.md "wikilink")，因其大公無私，擁有一副鐵面如墨的臉孔以鎮懾佞臣，額上掛有一彎蒼白明月，故有「包黑子」稱號。但是在歷史中，他的皮膚並不黑，只是為了塑造鐵面無私的形象，劇中才把它的臉變黑，到了現在許多人反而認為他原本就是黑臉。

民間傳說包公任開封府尹期間，得到「四大名捕」（王朝、馬漢、張龍、趙虎）與有足智多謀的[公孫策和](../Page/公孫策.md "wikilink")「御貓」之稱的御前四品帶刀護衛之南俠[展昭為左右文武助手](../Page/展昭.md "wikilink")，辦案既明查又暗訪，執法既嚴謹又不失人情。在皇室，有[八賢王](../Page/八賢王.md "wikilink")[趙德芳撐腰](../Page/趙德芳.md "wikilink")；在朝廷，有丞相王齡為其後盾，並擁有太祖（[趙匡胤](../Page/趙匡胤.md "wikilink")）御賜[尚方寶劍](../Page/尚方寶劍.md "wikilink")，有「先斬後奏」之權，連當朝皇帝[宋仁宗](../Page/宋仁宗.md "wikilink")[趙禎都得畏他三分](../Page/趙禎.md "wikilink")，因此不論皇家貴族、朝野官宦，乃至平民百姓、販夫走卒，均使正義得以伸張，真正落實「太子犯法與庶民同罪」。

開封府大堂，有常設三口鍘刀：[龍頭鍘](../Page/龍頭鍘.md "wikilink")（[火龍鍘](../Page/火龍鍘.md "wikilink")）專鍘無道謀逆之皇親國戚，[虎頭鍘專鍘文武貪濁污惡之百官](../Page/虎頭鍘.md "wikilink")，而[狗頭鍘](../Page/狗頭鍘.md "wikilink")（[犬頭鍘](../Page/犬頭鍘.md "wikilink")）專鍘窮凶極惡之平民百姓，三口鍘刀亦有先斬後奏之權。傳頌的案件包括《[鍘美案](../Page/陳世美.md "wikilink")（鍘[駙馬](../Page/駙馬.md "wikilink")）》、《[狸貓換太子](../Page/狸貓換太子.md "wikilink")（[打龍袍](../Page/打龍袍.md "wikilink")）》、《[鍘龐昱](../Page/鍘龐昱.md "wikilink")》、《[烏盆案](../Page/烏盆案.md "wikilink")》、《[鍘包勉](../Page/鍘包勉.md "wikilink")》、《[鍘判官](../Page/鍘判官.md "wikilink")》、《[陳州糶米](../Page/陳州糶米.md "wikilink")》等。故事中百姓向包公申冤的方法有：攔路喊冤（趁包公坐[轎出巡時攔路向包公申冤](../Page/轎.md "wikilink")）、擊鼓鳴冤（傳說開封府公堂門前有一鼓，任何人擊鼓後，包公便會升堂聆聽喊冤者的冤情）。\[8\]

傳說包拯審案不分晝夜：夜審陰，日審陽，亦即其晚上[靈魂出竅](../Page/靈魂出竅.md "wikilink")，到[陰司審判鬼魂](../Page/陰司.md "wikilink")，所以民間流傳的包拯肖像，都在額上畫一枚[弦月](../Page/弦月.md "wikilink")。[華南地區有傳說謂包拯死後被封為十殿](../Page/華南.md "wikilink")[閻羅王中的](../Page/閻羅王.md "wikilink")[五殿森羅王](../Page/十殿閻羅.md "wikilink")。

### 軼聞

[北宋民間筆記](../Page/北宋.md "wikilink")《[聞見前錄](../Page/聞見前錄.md "wikilink")》有孤證紀載，考獲進士的[章惇寄居在長輩家中候任期間](../Page/章惇.md "wikilink")，與長輩之妾私通而被撞破，翻牆而出時踩傷一名老婦，而被捉往告狀。包拯見章惇為新科進士，憐惜他寒窗苦讀多年，就無定其罪，只著章惇賠償老婦私了。只問踩人案，不問通姦案。根據宋律，私通長輩妻妾至少定罪兩年[徒刑](../Page/徒刑.md "wikilink")。另記，有位深得包拯任用的下屬王尚恭在開封府下轄武陽縣擔任知縣。一次數名百姓上訪，控訴[王尚恭徇私害民](../Page/王尚恭.md "wikilink")，請求包拯作主。包拯得知為狀告者為王尚恭後，不知怎麼最後判斷認為告訴者無理，就派人將百姓驅逐，內情無人知亦無其他史料記載，只有該書孤證。\[9\]。
[The_Memorial_Temple_of_Bao_Zheng_in_Hefei_2012-06.JPG](https://zh.wikipedia.org/wiki/File:The_Memorial_Temple_of_Bao_Zheng_in_Hefei_2012-06.JPG "fig:The_Memorial_Temple_of_Bao_Zheng_in_Hefei_2012-06.JPG")

### 包公廟宇

在[中國大陸](../Page/中國大陸.md "wikilink")[安徽省](../Page/安徽省.md "wikilink")[合肥市](../Page/合肥市.md "wikilink")[包公祠旁的包公墓](../Page/包公祠.md "wikilink")，於1987年10月1日重修對外開放。[开封包公祠则是国内外规模最大](../Page/开封包公祠.md "wikilink")、资料最全、影响最广的专门纪念包公的场所。而[兩岸三地均有大小不等之包公廟](../Page/兩岸三地.md "wikilink")，在[臺灣主要的包公祖廟為](../Page/臺灣.md "wikilink")[雲林縣](../Page/雲林縣.md "wikilink")[四湖鄉](../Page/四湖鄉.md "wikilink")[三條崙海清宮](../Page/三條崙海清宮.md "wikilink")；其他較著名的包公廟有[臺灣](../Page/臺灣.md "wikilink")[南投縣](../Page/南投縣.md "wikilink")[埔里鎮](../Page/埔里鎮.md "wikilink")「青天堂」\[10\]、南投縣[竹山鎮德興里](../Page/竹山鎮_\(台灣\).md "wikilink")「包青宮」、[高雄市](../Page/高雄市.md "wikilink")[大寮區大寮](../Page/大寮區.md "wikilink")-{里}-「開封宮」\[11\]、[台南市府城「](../Page/臺南市.md "wikilink")[三協境下南河南沙宮」及](../Page/臺南南沙宮.md "wikilink")[苗栗縣](../Page/苗栗縣.md "wikilink")[苑裡鎮](../Page/苑裡鎮.md "wikilink")「包公壇」……等。

[廣東省](../Page/廣東省.md "wikilink")[肇慶市端州區的](../Page/肇慶市.md "wikilink")「包公祠」[澳門](../Page/澳門.md "wikilink")[鏡湖醫院附近的包公廟](../Page/鏡湖醫院.md "wikilink")、[湖南省](../Page/湖南省.md "wikilink")[郴州市宜章栗源镇的包公廟等](../Page/郴州市.md "wikilink")。[香港一部份的廟宇以及圍村的神廳內亦有祀奉包公](../Page/香港.md "wikilink")，如[大角咀洪聖廟](../Page/大角咀洪聖廟.md "wikilink")、[上環文武廟及](../Page/上環文武廟.md "wikilink")[灣仔玉虛宮](../Page/灣仔玉虛宮.md "wikilink")、[深水埗三太子廟](../Page/深水埗三太子廟.md "wikilink")\[12\]。[菲律宾包公庙位于](../Page/菲律宾.md "wikilink")[马尼拉](../Page/马尼拉.md "wikilink")[计顺市的南洋天地宫](../Page/计顺市.md "wikilink")。
[臺南南沙宮_包府千歲.jpg](https://zh.wikipedia.org/wiki/File:臺南南沙宮_包府千歲.jpg "fig:臺南南沙宮_包府千歲.jpg")[下南河南沙宮](../Page/臺南南沙宮.md "wikilink")
包府千歲 \]\]

## 流行文化

### 電影

[Bao_Zheng_in_Beijing_opera.JPG](https://zh.wikipedia.org/wiki/File:Bao_Zheng_in_Beijing_opera.JPG "fig:Bao_Zheng_in_Beijing_opera.JPG")中的包拯形象\]\]

  - 《[七俠五義 (1967年電影)](../Page/七俠五義_\(1967年電影\).md "wikilink")》
  - 《[五鼠鬧東京 (1992年電影)](../Page/五鼠鬧東京_\(1992年電影\).md "wikilink")》
  - 《[九品芝麻官之白](../Page/九品芝麻官.md "wikilink")-{面}-包青天》（1994年）：[王晶](../Page/王晶.md "wikilink")[編劇](../Page/編劇.md "wikilink")、[導演](../Page/導演.md "wikilink")，[周星馳](../Page/周星馳.md "wikilink")、[吳孟達主演](../Page/吳孟達.md "wikilink")，電影配樂使用了華視1993年版《包青天》的配樂。此片并非正式的包青天故事，香港本名为《[九品芝麻官](../Page/九品芝麻官.md "wikilink")》，並無指有包拯等人物。台灣譯為《九品芝麻官之白-{面}-包青天》只是華視1993年版《包青天》在受歡迎後自創的惡搞名稱，其中「白-{面}-包青天」是指角色設定中包龍星（[周星馳飾](../Page/周星馳.md "wikilink")）為包拯之後代。
  - 《[老鼠愛上貓](../Page/老鼠愛上貓.md "wikilink")》
  - 《[麻辣白玉堂](../Page/麻辣白玉堂.md "wikilink")》

### 電視劇

  - 《七侠五义》（1967年）
  - 《新包公案》（1972年）：[台灣](../Page/台灣.md "wikilink")[中國電視公司](../Page/中國電視公司.md "wikilink")（中視）於1972年9點檔時段所播出的閩南語連續劇，由柯佑民（飾包拯）、劉林、張育安、小戽斗等主演。
  - 《[包青天](../Page/包青天_\(1974年電視劇\).md "wikilink")》（1974年）：1974年4月8日至1975年11月3日，[台灣](../Page/台灣.md "wikilink")[中華電視台](../Page/中華電視台.md "wikilink")（華視，今[中華電視公司](../Page/中華電視公司.md "wikilink")）將包拯審案故事改編成電視連續劇，由[田文光](../Page/田文光.md "wikilink")、[邊啟明製作](../Page/邊啟明.md "wikilink")，[儀銘](../Page/儀銘.md "wikilink")（飾演包拯）、[古錚](../Page/古錚.md "wikilink")（飾演[公孫策](../Page/公孫策.md "wikilink")）、[田鵬](../Page/田鵬.md "wikilink")（飾演展昭）、[韓湘琴](../Page/韓湘琴.md "wikilink")、[唐威等主演](../Page/唐威.md "wikilink")，總共播出350集，創下當時台灣電視連續劇播映集數最高紀錄（目前紀錄為[三立台灣台的](../Page/三立台灣台.md "wikilink")《[鳥來伯與十三姨](../Page/鳥來伯與十三姨.md "wikilink")》〔後改名為《[幸福白馬-{里}-](../Page/鳥來伯與十三姨.md "wikilink")》〕）。由[孫儀作詞](../Page/孫儀.md "wikilink")、[楊秉忠作曲](../Page/楊秉忠.md "wikilink")、[蔣光超主唱的主題曲](../Page/蔣光超.md "wikilink")〈包青天〉，隨之成為在台灣家喻戶曉的名曲。
  - 《[南侠展昭](../Page/南侠展昭.md "wikilink")》（1976年）
  - 《[御猫三戏锦毛鼠](../Page/御猫三戏锦毛鼠.md "wikilink")》（1982年）
  - 《[鐵-{面}-包公](../Page/鐵面包公.md "wikilink")》（1985年）：[TVB](../Page/TVB.md "wikilink")
    [盧海鵬](../Page/盧海鵬.md "wikilink") 飾演包公
  - 《[楊家將](../Page/楊家將.md "wikilink")》（1985年）：[TVB](../Page/TVB.md "wikilink")
    18周年台慶劇 [盧海鵬](../Page/盧海鵬.md "wikilink") 飾演包公
  - 《[包公](../Page/包公_\(1986年電視劇\).md "wikilink")》（1986年）：[河南電視台](../Page/河南電視台.md "wikilink")
  - 《[包公](../Page/包公.md "wikilink")》（1987年）：[亞洲電視](../Page/亞洲電視.md "wikilink")
    [王偉](../Page/王偉.md "wikilink") 飾演包公
  - 《[三侠五义](../Page/三侠五义.md "wikilink")》（1991年）
  - 《[包青天](../Page/包青天_\(1993年电视剧\).md "wikilink")》（1993年）：1993年2月23日，華視推出新版《包青天》，由[趙大深製作](../Page/趙大深.md "wikilink")，[金超群擔綱主演](../Page/金超群.md "wikilink")，原計畫製作15集，但播出後[收視率出乎意料之佳](../Page/收視率.md "wikilink")，製作單位[開全傳播公司乃不斷延長故事](../Page/開全傳播公司.md "wikilink")，結果總共[跨年製播](../Page/跨年.md "wikilink")236集，至1994年1月18日方告下檔。主題曲是[詹森雄編曲](../Page/詹森雄.md "wikilink")、[胡瓜主唱的](../Page/胡瓜.md "wikilink")〈包青天〉，是把蔣光超主唱的原曲加快[節奏而成](../Page/節奏.md "wikilink")。此劇[香港播映成績亦極佳](../Page/香港.md "wikilink")，香港兩個免費電視台（[無綫電視與](../Page/無綫電視.md "wikilink")[亞洲電視](../Page/亞洲電視.md "wikilink")）均購入此劇，作為同時段收視爭奪之對戰武器，被观众称为「[双包案](../Page/双包案.md "wikilink")」（双包案原是《包青天》中其中一个案件）。無綫所用之主題曲為[林子祥主唱](../Page/林子祥.md "wikilink")、[黃霑作詞的](../Page/黃霑.md "wikilink")《願世間有青天》；而亞視則以華視原版、胡瓜主唱之《包青天》。其後，兩電視台又分別以包青天為題材，各自拍攝電視劇（[無綫電視由](../Page/無綫電視.md "wikilink")[狄龍飾演](../Page/狄龍.md "wikilink")
    [包青天](http://www.ihkmusic.com/detail.asp?PRODUCT_ID=VCD050016&txtParam=1&txtCat=Leo)，沿用臺灣華視版《包青天》在TVB播出時所使用的主題曲《願世間有青天》，[亞洲電視則聯合中國大陸的](../Page/亞洲電視.md "wikilink")[河北電視臺邀請台灣華視](../Page/河北電視臺.md "wikilink")《包青天》原演員班底赴港拍攝《新包青天》）。此戲使“龍/虎/狗頭鍘侍候”（剧中包拯着劊子手處死犯人時說的一句話）在港成為一時金句。往後相繼出現以包青天為背景的電視劇，如《七俠五義》（1994年，台灣[華視](../Page/華視.md "wikilink")）、《碧血青天楊家將》（1994年，香港[亞洲電視](../Page/亞洲電視.md "wikilink")）、
    《新新包青天》（1998年，台湾[台视與香港](../Page/台视.md "wikilink")[亞洲電視合拍](../Page/亞洲電視.md "wikilink")）、《[包公出巡](../Page/包公出巡.md "wikilink")》（2000年，台湾[台视](../Page/台视.md "wikilink")）、《[五鼠鬥御貓](../Page/五鼠鬥御貓.md "wikilink")》（2005年，香港）與《[情逆三世緣](../Page/情逆三世緣.md "wikilink")》（2013年，香港[TVB](../Page/TVB.md "wikilink")）等。
  - 《[俠義包公](../Page/俠義包公.md "wikilink")》（1994年）：由[新加坡](../Page/新加坡.md "wikilink")[新加坡廣播局第](../Page/新加坡廣播局.md "wikilink")8頻道（今[新傳媒電視8頻道](../Page/新傳媒電視8頻道.md "wikilink")）製作播出，由飾演包拯，不過，該劇不是當官審案的中年包公，而是少年包公。
  - 《[七侠五義](../Page/七侠五義_\(1994年電視劇\).md "wikilink")》（1994年）
  - 《[侠義見青天](../Page/侠義見青天.md "wikilink")》（1994年）：台灣劇集
  - 《[碧血青天杨家将](../Page/碧血青天杨家将.md "wikilink")》（1994年）
  - 《[碧血青天珍珠旗](../Page/碧血青天珍珠旗.md "wikilink")》（1994年）
  - 《新七侠五义》（1994年）
  - 《[南侠展昭](../Page/南侠展昭.md "wikilink")》（1994年）
  - 《[包青天](../Page/包青天_\(無綫電視劇\).md "wikilink")》（1995年）
  - 《[新包青天](../Page/包青天_\(亞視電視劇\).md "wikilink")》（1995年）
  - 《[新新包青天](../Page/新新包青天.md "wikilink")》（1998年）
  - 《[包公生死劫](../Page/包公生死劫.md "wikilink")》（2000年）：[廣東電視台](../Page/廣東電視台.md "wikilink")
  - 《[包公出巡](../Page/包公出巡.md "wikilink")》（2000年）
  - 《[包公奇案](../Page/包公奇案.md "wikilink")》（2000年）
  - 《[少年包青天](../Page/少年包青天.md "wikilink")》（2000年）
  - 《[少年包青天II](../Page/少年包青天.md "wikilink")》（2001年）
  - 《[凌雲状志包青天](../Page/剑临天下.md "wikilink")》（2004年）
  - 《[新铡美案](../Page/新铡美案.md "wikilink")》（2004年）
  - 《[包青天之白玉堂傳奇](../Page/包青天之白玉堂傳奇.md "wikilink")》（2005年）
  - 《[狸猫换太子传奇](../Page/狸猫换太子传奇.md "wikilink")》（2005年）
  - 《[少年包青天III之天芒传奇](../Page/少年包青天.md "wikilink")》（2006年）
  - 《[包青天](../Page/包青天_\(2009年電視劇\).md "wikilink")》（2009年）：《包青天》是一部由[上海飞迈影视制作有限公司在](../Page/上海飞迈影视制作有限公司.md "wikilink")2008年製作的[電視劇](../Page/電視劇.md "wikilink")，在[上海的播出期間為](../Page/上海.md "wikilink")2009年7月22日至8月6日。因由[台湾](../Page/台湾.md "wikilink")[中华电视公司](../Page/中华电视公司.md "wikilink")（華視）1993年播出的电视剧《[包青天](../Page/包青天_\(1993年電視劇\).md "wikilink")》的原班人马出演，有时被誤會是華視1993年版《包青天》，同時也為了與亞洲電視與河北電視台合拍的《新包青天》以作区别。故又称《新包青天》2009版。全劇以[單元劇的方式呈現](../Page/單元劇.md "wikilink")，總集數為61集，但上海首播时缩减至47集。台灣首播權由愛爾達電視取得，2010年4月7日起在[愛爾達綜合台首播](../Page/愛爾達綜合台.md "wikilink")；而[香港則在](../Page/香港.md "wikilink")[高清翡翠台播映](../Page/高清翡翠台.md "wikilink")，主題曲為[林峰主唱的](../Page/林峰.md "wikilink")《值得流淚》。
  - 《[包青天之七侠五義](../Page/包青天之七侠五義.md "wikilink")》（2010年）：由**金超群**自編自導自演，續做包青天一系列電視劇。
  - 《[带刀女捕快](../Page/带刀女捕快.md "wikilink")》（2011年）
  - 《[秦香蓮](../Page/秦香蓮_\(電視劇\).md "wikilink")》（2011年）
  - 《[包青天之碧血丹心](../Page/包青天之碧血丹心.md "wikilink")》（2012年）：由**金超群**自編自導自演，續做包青天一系列電視劇。
  - 《[包青天之開封奇案](../Page/包青天之開封奇案.md "wikilink")》（2012年）：由**金超群**自編自導自演，續做包青天一系列電視劇。
  - 《[七侠五義人間道](../Page/七侠五義人間道.md "wikilink")》（2012年）：由中國製作，在台灣[中視首播](../Page/中視.md "wikilink")，由[萬梓良飾演包拯](../Page/萬梓良.md "wikilink")、[趙文卓飾演](../Page/趙文卓.md "wikilink")[展昭](../Page/展昭.md "wikilink")，其餘的演員分別為[王同輝](../Page/王同輝.md "wikilink")、[霍政諺](../Page/霍政諺.md "wikilink")、
    [寇振海](../Page/寇振海.md "wikilink") 、[秦焰等人](../Page/秦焰.md "wikilink")。
  - 《[包公判阴阳](../Page/包公判阴阳.md "wikilink")》（2013年）
  - 《[情逆三世緣](../Page/情逆三世緣.md "wikilink")》（第1－13集）（2013年）：由[電視廣播有限公司播映](../Page/電視廣播有限公司.md "wikilink")。並非全部描述包拯。其中有歷史被改寫。
  - 《[虎头铡](../Page/虎头铡_\(电视剧\).md "wikilink")》（2015年）：中國大陸電視劇
  - 《[神探包青天](../Page/神探包青天.md "wikilink")》（2015年）：新麗電視文化投資有限公司出品的古裝懸疑劇，由錢雁秋執導，張子健、於震、淳于珊珊、錢雁秋、曲柵柵、鍾衛華、靳德茂、張樹平、韓含、於函冰、梁凱主演。該劇根據武俠小說《三俠五義》改編而成，講述的是“包青天”包拯的斷案傳奇。
  - 《[五鼠鬧東京](../Page/五鼠鬧東京_\(電視劇\).md "wikilink")》（2016年）
  - 《[包青天](../Page/包青天_\(2017年电视剧\).md "wikilink")》（2017年）
  - 《[开封府](../Page/开封府_\(电视剧\).md "wikilink")》（2017年）
  - 《[小戏骨：包青天之秦香莲与陈世美](../Page/小戏骨.md "wikilink")》（2018年）
  - 《[包青天再起風雲](../Page/包青天再起風雲.md "wikilink")》（未定）

### 其他電視劇

传说包公首任定远知县，但是宋史中并未有相应记载，可能为演绎小说，也有人认为当时包拯属于举荐代理、未满一任的临时性质任职（一年左右时间），国史馆没有任命资料；包公后任天长知县才属于朝廷任命、已满一任的正式官员，国史馆保存有档案材料。而宋史编撰时只参考了国史馆藏档案，而没有考察地方志。但是《少年包青天》等当代影视剧都只描述了包拯担任定远县令而没有担任天长县令的相关细节。而唯一记载于正式的包拯断牛舌案发生在包拯担任天长县令的三年多内，并成为后世所有包公戏的雏形。

  - 《[天师钟馗](../Page/天师钟馗.md "wikilink")》：有借用包拯一角。
      - 《[天师钟馗之三会包青天](../Page/天师钟馗之三会包青天.md "wikilink")》
      - 《[天师钟馗之包公三请钟馗](../Page/天师钟馗之包公三请钟馗.md "wikilink")》
      - 《[天师锺馗之美丽传说](../Page/天师锺馗之美丽传说.md "wikilink")》
  - 《[追魚傳奇](../Page/追魚傳奇.md "wikilink")》《[無雙譜](../Page/無雙譜.md "wikilink")》：借用包拯一角。
  - 《[新神探联盟之包大人来了](../Page/新神探联盟之包大人来了.md "wikilink")》
  - 《[包三姑外传](../Page/包三姑外传.md "wikilink")》

### 布袋戲

  - 《[包公俠義傳](../Page/包公俠義傳.md "wikilink")》：2007年[天地多媒體推出以包青天為背景的](../Page/天地多媒體.md "wikilink")[黃俊雄布袋戲](../Page/黃俊雄布袋戲.md "wikilink")《[包公俠義傳](../Page/包公俠義傳.md "wikilink")》，以[黃海岱所寫的](../Page/黃海岱.md "wikilink")[七俠五義所改編](../Page/七俠五義.md "wikilink")，配音和編導由[黃俊雄之幼子](../Page/黃俊雄.md "wikilink")[黃立綱擔綱](../Page/黃立綱.md "wikilink")，[西卿和](../Page/西卿.md "wikilink")[黃鳳儀擔任幕後主唱](../Page/黃鳳儀.md "wikilink")，首播頻道為[公視主頻道](../Page/公視主頻道.md "wikilink")。[客家電視台以](../Page/客家電視台.md "wikilink")[客家語](../Page/客家語.md "wikilink")[配音重播](../Page/配音.md "wikilink")。

### 漫畫

  - 《[包青天](../Page/包青天.md "wikilink")》：香港漫畫家[李志清武俠類漫畫](../Page/李志清_\(香港\).md "wikilink")
  - 《[北宋風雲錄](../Page/北宋風雲錄.md "wikilink")》：由日本漫畫家[瀧口琳琳參照](../Page/瀧口琳琳.md "wikilink")《[七俠五義](../Page/七俠五義.md "wikilink")》改編成的少女漫畫
  - 《[开封奇谈-这个包公不太行](../Page/开封奇谈-这个包公不太行.md "wikilink")》：中国漫画家[晓晨兽的少女漫畫](../Page/晓晨兽.md "wikilink")

### 紀錄片

  - 2016年五月，中央電視台紀實台製作播映紀錄片「千年包公」，共三集。\[13\]

### 改編歌曲

  - [亞洲電視節目街坊大為營片頭曲](../Page/亞洲電視.md "wikilink")
  - 1998[香港申訴專員公署廣告歌曲](../Page/香港申訴專員公署.md "wikilink")

### 兒童節目

  - 《[快樂童盟](../Page/快樂童盟.md "wikilink")》 : (2016-今)
    香港[viuTV每星期一辯論公堂環節](../Page/viuTV.md "wikilink")
    Emma Patarini,Asha Cuthbert(徐㴓喬),林玉兒(Yeni Lee)

## 注释

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - 《包公傳》程如峰 著，[黃山書社](../Page/黃山書社.md "wikilink")1994年初版，ISBN
    7-80535-562-2
  - 《包拯集校注》，包拯 著，程如峰補遺，楊國宜[校正](../Page/校正.md "wikilink")，黃山書社1999年6月初版
  - 《[包公遺骨記](https://web.archive.org/web/20070304231738/http://www.books.com.tw/exep/prod/booksfile.php?item=0010354645)》，[陳桂棣](../Page/陳桂棣.md "wikilink")、[吳春桃著](../Page/吳春桃.md "wikilink")，北京[人民文學出版社](../Page/人民文學出版社.md "wikilink")2005年5月初版；ISBN
    7-02-005194-4

## 外部連結

  - 伊维德：〈[罪恶与清官：说唱词话中的包公](http://www.nssd.org/articles/article_read.aspx?id=669051623)〉。
  - [包青天](http://www.wrsn.com.tw/z/zz07.htm)
  - [民間傳奇之包公案](http://www.rthk.org.hk/radiodrama/4tales/)（[廣播劇](../Page/廣播劇.md "wikilink")）

## 参见

  - [开封府](../Page/开封府.md "wikilink")、[开封府尹](../Page/开封府尹.md "wikilink")、[龍圖閣直学士](../Page/龍圖閣.md "wikilink")
  - [三司使](../Page/三司.md "wikilink")、[枢密副使](../Page/枢密副使.md "wikilink")
  - [包公戏](../Page/包公戏.md "wikilink")、[包青天](../Page/包青天.md "wikilink")、[尚方宝剑](../Page/尚方宝剑.md "wikilink")
  - [三侠五义](../Page/三侠五义.md "wikilink")、[展昭](../Page/展昭.md "wikilink")
  - [狄仁杰](../Page/狄仁杰.md "wikilink")、[宋慈](../Page/宋慈.md "wikilink")
  - [寇准](../Page/寇准.md "wikilink")、[狄青](../Page/狄青.md "wikilink")

[包拯](../Category/包拯.md "wikilink")
[Category:北宋官员](../Category/北宋官员.md "wikilink")
[Category:天聖五年丁卯科進士](../Category/天聖五年丁卯科進士.md "wikilink")
[Category:智慧之神](../Category/智慧之神.md "wikilink")
[Category:中国人物神](../Category/中国人物神.md "wikilink")
[Category:中國民間信仰](../Category/中國民間信仰.md "wikilink")
[Category:台灣民間信仰](../Category/台灣民間信仰.md "wikilink")
[Category:包青天角色](../Category/包青天角色.md "wikilink")
[Category:楊家將角色](../Category/楊家將角色.md "wikilink")
[Category:合肥人](../Category/合肥人.md "wikilink")
[Category:諡孝肅](../Category/諡孝肅.md "wikilink")
[Z](../Category/包姓.md "wikilink")

1.  《包拯墓志铭》：“公讳拯，字希仁，庐州合肥人。天圣五年进士甲科。”
2.  《宋史·包拯传》：“始举进士，除大理评事，出知建昌县。以父母皆老，辞不就。得监和州税，父母又不欲行，拯即解官归养。后数年，亲继亡，拯庐墓终丧，犹裴徊不忍去，里中父老数来劝勉。”
3.  《續資治通鑑長編》卷一百八十四，嘉祐元年十二月壬子條
4.
5.
6.  [包公遗骨记 第七章 一个真实的包公（2）
    作者：陈桂棣、春桃](http://book.sina.com.cn/nzt/his/baogong/13.shtml)
7.  [包公遗骨记](http://www.clght.com/show.aspx?id=6748&cid=3)，大宋名臣包拯墓发现始末
8.
9.  [歷史人物︰通姦案令包青天枉法](http://www.bastillepost.com/hongkong/6-%E5%A4%A7%E8%A6%96%E9%87%8E/623284-%E6%AD%B7%E5%8F%B2%E4%BA%BA%E7%89%A9%EF%B8%B0%E9%80%9A%E5%A7%A6%E6%A1%88%E4%BB%A4%E5%8C%85%E9%9D%92%E5%A4%A9%E6%9E%89%E6%B3%95)
    《巴士的報》 2015年4月23日
10.
11. [開封宮（包公廟）](http://www.zzn.ks.edu.tw/dalouw/house-2.htm)
12. [福山堂](http://www.fushantang.com/1005/e1020.html)
13.