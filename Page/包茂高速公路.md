**包头－茂名高速公路**，简称**包茂高速**，[中国国家高速公路网编号为](../Page/中国国家高速公路网.md "wikilink")**G65**，起点在[包头](../Page/包头.md "wikilink")，途经[鄂尔多斯](../Page/鄂尔多斯.md "wikilink")、[榆林](../Page/榆林.md "wikilink")、[延安](../Page/延安.md "wikilink")、[黄陵](../Page/黄陵.md "wikilink")、[铜川](../Page/铜川.md "wikilink")、[西安](../Page/西安.md "wikilink")、[安康](../Page/安康.md "wikilink")、[达州](../Page/达州.md "wikilink")、[重庆](../Page/重庆.md "wikilink")、[南川](../Page/南川.md "wikilink")、[黔江](../Page/黔江.md "wikilink")、[吉首](../Page/吉首.md "wikilink")、[怀化](../Page/怀化.md "wikilink")、[桂林](../Page/桂林.md "wikilink")、[梧州](../Page/梧州.md "wikilink")，终点在[茂名](../Page/茂名.md "wikilink")，全长3098公里。\[1\]

## 分省介绍

[G65_Huaihua_Direction_close_to_G56.jpg](https://zh.wikipedia.org/wiki/File:G65_Huaihua_Direction_close_to_G56.jpg "fig:G65_Huaihua_Direction_close_to_G56.jpg")段（近[杭瑞高速](../Page/杭瑞高速.md "wikilink")）\]\]
[Aizhai_Bridge-1.jpg](https://zh.wikipedia.org/wiki/File:Aizhai_Bridge-1.jpg "fig:Aizhai_Bridge-1.jpg")的[矮寨大桥](../Page/矮寨大桥.md "wikilink")\]\]

  - [内蒙古段](../Page/内蒙古.md "wikilink")：全通。
  - [陕西段](../Page/陕西.md "wikilink")：全通。
  - [四川段](../Page/四川.md "wikilink")：全通。
  - [重庆段](../Page/重庆.md "wikilink")：全通。
  - [湖南段](../Page/湖南.md "wikilink")：全通。
  - [广西段](../Page/广西.md "wikilink")：全通。

## [广东段](../Page/广东.md "wikilink")

## 注释

## 参见

  - [中国公路命名与编号](../Page/中国公路命名与编号.md "wikilink")
  - [中国国家高速公路网](../Page/中国国家高速公路网.md "wikilink")
  - [矮寨大桥](../Page/矮寨大桥.md "wikilink")
  - [秦岭终南山公路隧道](../Page/秦岭终南山公路隧道.md "wikilink")

[Category:国家高速公路网纵向线](../Category/国家高速公路网纵向线.md "wikilink")
[G](../Category/内蒙古自治区高速公路.md "wikilink")
[G](../Category/陕西省高速公路.md "wikilink")
[G](../Category/四川省高速公路.md "wikilink")
[G](../Category/重庆市高速公路.md "wikilink")
[G](../Category/湖南省高速公路.md "wikilink")
[G](../Category/广西壮族自治区高速公路.md "wikilink")
[G](../Category/广东省高速公路.md "wikilink")

1.  [中华人民共和国交通运输部国家高速公路网布局方案](http://www.mot.gov.cn/images/luwang2009.jpg)