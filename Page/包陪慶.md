**包陪庆**（），[香港](../Page/香港.md "wikilink")[企业家](../Page/企业家.md "wikilink")、社会活动家、[慈善家](../Page/慈善家.md "wikilink")、[第十二屆全國政協委員](../Page/中國人民政治協商會議全國委員會香港地區委員.md "wikilink")。中國[北宋名臣](../Page/北宋.md "wikilink")[包拯](../Page/包拯.md "wikilink")30世孫。

## 简介

[籍贯](../Page/籍贯.md "wikilink")[浙江省](../Page/浙江省.md "wikilink")[宁波镇海县](../Page/宁波.md "wikilink")（现[镇海区](../Page/镇海区_\(宁波市\).md "wikilink")），1945年[抗日战争勝利時出生於](../Page/抗日战争.md "wikilink")[重庆](../Page/重庆.md "wikilink")，故取名为包陪庆。为已故船王[包玉刚長女](../Page/包玉刚.md "wikilink")，于1969年嫁予[奥地利人](../Page/奥地利.md "wikilink")[苏海文](../Page/苏海文.md "wikilink")（曾任[立法局議員](../Page/立法局.md "wikilink")、现为世界船王）。曾先后就读于[美国](../Page/美国.md "wikilink")[普渡大学](../Page/普渡大学.md "wikilink")，[芝加哥大学](../Page/芝加哥大学.md "wikilink")，[加拿大](../Page/加拿大.md "wikilink")[麦吉尔大学](../Page/麦吉尔大学.md "wikilink")，[英国](../Page/英国.md "wikilink")[伦敦大学等世界著名学府](../Page/伦敦大学.md "wikilink")。通晓[英语](../Page/英语.md "wikilink")、[法语](../Page/法语.md "wikilink")、[德语](../Page/德语.md "wikilink")、[宁波](../Page/宁波.md "wikilink")/[上海话](../Page/上海话.md "wikilink")、[粤语等多种](../Page/粤语.md "wikilink")[语言](../Page/语言.md "wikilink")[方言](../Page/方言.md "wikilink")。

是环球航运有限公司董事，并兼任环球水火保险有限公司主席，香港升华服务有限公司主席，并是[香港中華廠商聯合會名誉会长](../Page/香港中華廠商聯合會.md "wikilink")。曾担任[香港演艺学院董事会主席](../Page/香港演艺学院.md "wikilink")。

2002年，当选为浙江省政协委员。2004年，捐资在[浙江大学成立](../Page/浙江大学.md "wikilink")“包玉刚国际基金”，在此之前，创立了“包玉刚奖学金”。
2004年，被[黑龙江省政府聘为高级经济顾问](../Page/黑龙江省.md "wikilink")。2005年2月，当选为黑龙江省[政协常委](../Page/政协.md "wikilink")。

在香港创立[伸手助人协会](../Page/伸手助人协会.md "wikilink")，照顾孤寡老人。创立肝寿基金，致力于[肝脏医疗保健事业](../Page/肝脏.md "wikilink")。创立香港汉基国际学校，为[普通话与](../Page/普通话.md "wikilink")[英语并重的教学教育机构](../Page/英语.md "wikilink")。曾担任香港康智儿童基金会的主席，并担任华夏基金会主席，[上海宋庆龄福利基金会的理事等职务](../Page/上海.md "wikilink")。

## 荣誉

  - 2005年，[香港特别行政区政府](../Page/香港特别行政区政府.md "wikilink")[银紫荆星章](../Page/银紫荆星章.md "wikilink")
  - 2004年，香港舞蹈聯盟[香港舞蹈年獎傑出成就獎](../Page/香港舞蹈年獎.md "wikilink")
  - 2004年，[美国国际标准舞师生锦标赛](../Page/美国.md "wikilink")[冠军](../Page/冠军.md "wikilink")
  - 2004年，香港港联盟会卓越成就奖
  - 2004年，[美国](../Page/美国.md "wikilink")[普渡大学名誉](../Page/普渡大学.md "wikilink")[博士](../Page/博士.md "wikilink")[学位](../Page/学位.md "wikilink")
  - 2000年，世界杰出女企业家奖
  - 1999年，香港杰出女企业家奖
  - 1994年，Mont Blanc国际文化奖

## 参考

  - [包陪庆
    简介](https://web.archive.org/web/20070923173922/http://paoscholarship.zju.edu.cn/bpqjj.jsp)
  - [包陪庆：航运王国的女主人](http://info.china.alibaba.com/news/detail/v5003414-d1000510704.html)
  - [包陪庆：快乐女船王启航新副业](http://www.cnelder.com/2007/5-28/94856.shtml)

[Category:中華人民共和國女企業家](../Category/中華人民共和國女企業家.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:第十二屆全國政協委員](../Category/第十二屆全國政協委員.md "wikilink")
[Category:第十一届全国政协委员](../Category/第十一届全国政协委员.md "wikilink")
[Category:全国政协香港委员](../Category/全国政协香港委员.md "wikilink")
[Category:黑龙江省政协常务委员](../Category/黑龙江省政协常务委员.md "wikilink")
[Category:浙江省政协委員](../Category/浙江省政协委員.md "wikilink")
[Category:芝加哥大学校友](../Category/芝加哥大学校友.md "wikilink")
[Category:普渡大学校友](../Category/普渡大学校友.md "wikilink")
[Category:英華女學校校友](../Category/英華女學校校友.md "wikilink")
[Category:镇海区人](../Category/镇海区人.md "wikilink")
[Category:香港寧波人](../Category/香港寧波人.md "wikilink")
[P](../Category/包玉刚家族.md "wikilink") [P](../Category/包姓.md "wikilink")
[Category:香港演藝學院榮譽博士](../Category/香港演藝學院榮譽博士.md "wikilink")
[Category:商人出身的政治人物](../Category/商人出身的政治人物.md "wikilink")