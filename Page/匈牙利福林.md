**福林**是[匈牙利貨幣](../Page/匈牙利.md "wikilink")，縮寫**Ft**。福林分硬幣和紙幣兩種，硬幣有八個面值，分別是1、2（兩者均於2008年3月停止流通）、5、10、20、50、100、200（於2009年1月推出）；紙幣有七個面值，分別是200（於2010年開始被硬幣取代）、500、1000、2000、5000、10000、20000。匈牙利于1946年8月实行币值改革，以新币福林代替旧币[帕戈](../Page/帕戈.md "wikilink")。福林的名稱來自於[意大利城市](../Page/意大利.md "wikilink")[佛罗伦萨](../Page/佛罗伦萨.md "wikilink")。1福林=100菲勒。取消1、2福林硬幣後，商品價格並未因此而有所調整，所以將會向上或向下取至最近的5福林。
匈牙利已经决定在不久的将来用[欧元取代福林作为法定货币](../Page/欧元.md "wikilink")，但是日期并未最终确定，曾经在2012年[元旦进行这一转换的计划迄今尚未完成](../Page/元旦.md "wikilink")。

## 流通中的貨幣

### 硬幣

  - 5 福林
  - 10 福林
  - 20 福林
  - 50 福林
  - 100 福林
  - 200 福林

### 紙幣

[缩略图](https://zh.wikipedia.org/wiki/File:Hungarian_forint_banknote_set.jpg "fig:缩略图")

  - 500 福林
      - 正面：[拉科齊·費倫茨二世](../Page/拉科齊·費倫茨二世.md "wikilink")
      - 背面：[沙羅什保陶克城堡](../Page/沙羅什保陶克.md "wikilink")
  - 1000 福林
      - 正面：[马加什一世](../Page/马加什一世.md "wikilink")
      - 背面：[維謝格拉德城堡的大力神](../Page/維謝格拉德.md "wikilink")[噴泉](../Page/噴泉.md "wikilink")
  - 2000 福林
      - 正面：[拜特倫·伽柏](../Page/拜特倫·伽柏.md "wikilink")
      - 背面：維克托·馬德華斯的畫作
  - 5000 福林
      - 正面：[塞切尼·伊斯特凡](../Page/伊斯特凡·塞切尼.md "wikilink")
      - 背面：塞切尼豪宅
  - 10000 福林
      - 正面：[伊什特万一世](../Page/伊什特万一世.md "wikilink")
      - 背面：[埃斯泰尔戈姆的景色](../Page/埃斯泰尔戈姆.md "wikilink")
  - 20000 福林
      - 正面：[戴阿克·费伦茨](../Page/戴阿克·费伦茨.md "wikilink")
      - 背面：在[佩斯的老房子](../Page/佩斯.md "wikilink")

## 匯率

## 外部連結

  - [匈牙利硬币 (目錄和畫廊)](http://en.ucoin.net/catalog/?country=hungary)

[Category:各國貨幣](../Category/各國貨幣.md "wikilink")
[Category:匈牙利经济](../Category/匈牙利经济.md "wikilink")
[Category:1946年面世的貨幣](../Category/1946年面世的貨幣.md "wikilink")