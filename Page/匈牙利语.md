**匈牙利语**又称**马扎尔语**（匈牙利语：**magyar**），是一種[乌拉尔语系](../Page/乌拉尔语系.md "wikilink")[芬兰-乌戈尔语族](../Page/芬兰-乌戈尔语族.md "wikilink")[乌戈尔语支的語言](../Page/乌戈尔语支.md "wikilink")，为[马扎尔人的语言](../Page/马扎尔人.md "wikilink")。使用者主要分布在[匈牙利](../Page/匈牙利.md "wikilink")，是该国的[官方语言](../Page/官方语言.md "wikilink")，也是[欧盟](../Page/欧盟.md "wikilink")24个官方语言之一。此外匈牙利语还分布在[罗马尼亚](../Page/罗马尼亚.md "wikilink")、[斯洛伐克](../Page/斯洛伐克.md "wikilink")、[乌克兰](../Page/乌克兰.md "wikilink")、[塞尔维亚](../Page/塞尔维亚.md "wikilink")、[克罗地亚](../Page/克罗地亚.md "wikilink")、[奥地利](../Page/奥地利.md "wikilink")、[斯洛文尼亚等国家](../Page/斯洛文尼亚.md "wikilink")。
由於沒有其他语言能夠与匈牙利语沟通，匈牙利语也视作世界上数一数二最孤立的语言之一。

## 字母

曾使用[古匈牙利字母](../Page/古匈牙利字母.md "wikilink")，於[基督教傳入](../Page/基督教.md "wikilink")[匈牙利王國後改用](../Page/匈牙利王國.md "wikilink")[拉丁字母](../Page/拉丁字母.md "wikilink")，不過20世紀後[古匈牙利字母在該國重新復甦](../Page/古匈牙利字母.md "wikilink")，並和[拉丁字母一起並列使用](../Page/拉丁字母.md "wikilink")。匈牙利语字母表由拉丁字母组成，大匈牙利字母如下：

“Q”、“W”、“X”、“Y”只在外语词中出现（[匈牙利人名中常见](../Page/匈牙利人名.md "wikilink")“W”和“Y”，分别读作“V”和“(J)I”）。有时人们会使用大匈牙利字母和小匈牙利字母分别代表有和没有“Q”、“W”、“X”、“Y”的字母表。

## 元音

匈牙利語共有14個元音音位。元音和元音可以相互組合成數對長元音。他們大多數的差別只在持續長度長或短，但a/á 和 e/é
同時在開口大小與長短上都不相同。

### 后元音

  - a \[ɒ\]：圓唇。和[英式英语](../Page/英式英语.md "wikilink")“o”的短音基本一样。
  - á \[aː\]：非圓唇。a的长音
  - o \[o\]
  - ó \[oː\] o的長音
  - u \[u\]
  - ú \[uː\] u的長音

### 前元音

  - e \[ɛ\]
  - é \[eː\]：e（及ë）的长音
  - i \[i\]
  - í \[iː\]：i的長音
  - ö \[ø\]
  - ő \[øː\]：ö的長音
  - ü \[y\]
  - ű \[yː\]：ü的長音

### 其他

  - ë \[e\]：仅在方言中出现。标准语（[布达佩斯方言](../Page/布达佩斯.md "wikilink")）做短音e。

### 元音和谐

于词后加后缀时，匈语遵守[元音和谐规则](../Page/元音和谐律.md "wikilink")。即根据词的元音，加入不同的后缀。

## 輔音

匈牙利語共有25個輔音音位。与元音相似，匈牙利语中辅音也可以[延长](../Page/輔音延長.md "wikilink")。比如fenn读作\[fɛnː\]。

|                                       | [唇](../Page/唇音.md "wikilink") | [齒齦](../Page/齒齦音.md "wikilink") | [齒齦後](../Page/齒齦後音.md "wikilink") | [硬顎](../Page/硬顎音.md "wikilink") | [軟齶](../Page/軟齶音.md "wikilink") | [聲門](../Page/聲門音.md "wikilink") |
| ------------------------------------- | ----------------------------- | ------------------------------- | --------------------------------- | ------------------------------- | ------------------------------- | ------------------------------- |
| [鼻音](../Page/鼻音_\(辅音\).md "wikilink") |                               | **m**                           |                                   | **n**                           |                                 |                                 |
| [塞音](../Page/塞音.md "wikilink")        | **p**                         | **b**                           | **t**                             | **d**                           |                                 | **ty**                          |
| [塞擦音](../Page/塞擦音.md "wikilink")      |                               | **c**                           | **dz**                            | **cs**                          | **dzs**                         |                                 |
| [擦音](../Page/擦音.md "wikilink")        | **f**                         | **v**                           | **sz**                            | **z**                           | **s**                           | **zs**                          |
| [顫音](../Page/顫音.md "wikilink")        |                               |                                 | **r**                             |                                 |                                 |                                 |
| [近音](../Page/近音.md "wikilink")        |                               |                                 | **l**                             |                                 |                                 | **j**                           |

匈牙利語 輔音\[1\]

## 语法

### 名词

匈牙利語屬[黏著語](../Page/黏著語.md "wikilink")，並且有[元音和谐律現象](../Page/元音和谐律.md "wikilink")，和同属于[芬兰-乌戈尔语族的](../Page/芬兰-乌戈尔语族.md "wikilink")[芬蘭語有很多相似性](../Page/芬蘭語.md "wikilink")。匈牙利語名詞有格的變化，並且格的變化多達20多種。[主格没有后缀](../Page/主格.md "wikilink")；[宾格由结尾字母](../Page/宾格.md "wikilink")-t表示；[与格用后缀](../Page/与格.md "wikilink")-nak、-nek表示；[入格以后缀](../Page/入格.md "wikilink")-ba、-be作为标志；[在内格以](../Page/在内格.md "wikilink")-ben、-ban为后缀；[从格以](../Page/从格.md "wikilink")-ból、-ből为后缀；[向格以](../Page/向格.md "wikilink")-hoz
-hez -höz表示；[位置格以后缀](../Page/位置格.md "wikilink")-nál
-nél为结尾；[离格用](../Page/离格.md "wikilink")-tól
-től作为后缀；另外表示所有格的詞接在名詞詞尾并表示所有者，舉例如下：

  - ház　（家）
  - ház-am　（我家）
  - ház-ad　（你家）
  - ház-a　（他\[她\]家）
  - ház-unk　（我們家）
  - ház-atok　（你們家）
  - ház-uk　（他\[她\]們家）

### 动词

[動詞變位分為定變位和不定變位](../Page/動詞變位.md "wikilink")，如[賓語是第三人稱或](../Page/賓語.md "wikilink")[賓語有定冠詞](../Page/賓語.md "wikilink")，動詞應定變位，其他情況動詞要不定變位。

### 介词

匈牙利语没有[介词](../Page/介词.md "wikilink")，取而代之的是后置的[词缀](../Page/词缀.md "wikilink")，有着与介词相同的功能。比如名词的入格、在内格。

### 动词

匈语的动词依照两个时态（过去与现在）、三个语气（陈述、条件、命令-虚拟语气）、两个数（单、复数）、三个人称，以及定式或不定式而变化，而助词[fog用来表示未來式](../Page/wiktionary:fog#Hungarian.md "wikilink")。

## 参考资料

## 外部链接

  - [Hungarian - A Strange Cake on the Menu - *article by Nádasdy
    Ádám*](https://web.archive.org/web/20060421044724/http://www.filolog.com/languageStrangeCake.html)
  - [Ethnologue report for
    Hungarian](https://web.archive.org/web/20050306085430/http://www.ethnologue.com/show_language.asp?code=HNG)
  - [Numerals of some Uralic
    languages](https://web.archive.org/web/20051210180034/http://www.ut.ee/Ural/num.html)
  - [Uralic
    page](https://web.archive.org/web/20060216005753/http://www.geocities.com/Athens/Acropolis/3093/finnugor.html)
  - [Introduction to
    Hungarian](https://web.archive.org/web/20041010065220/http://impulzus.sch.bme.hu/info/magyar.shtml)
  - [Hungarian
    Profile](https://web.archive.org/web/20050614081328/http://www.lmp.ucla.edu/profiles/profh02.htm)
  - ["The Hungarian Language: A Short Descriptive
    Grammar"](http://www.speech.kth.se/~bea/hungarian.pdf) by Beáta
    Megyesi (PDF document)
  - [The old site of the Indiana University Institute of Hungarian
    Studies (various
    resources)](http://www.indiana.edu/~iuihsl/homethisisoldbutgoodone.html)
  - [Let's try to learn Hungarian (Magyar) and
    Turkish\!](https://web.archive.org/web/20060619150421/http://miejipang.homestead.com/untitled18.html)
  - [Grammar, phonology and syntax plus some history of the
    language](https://web.archive.org/web/20051028164541/http://www.nyariegyetem.hu/regi/hlga/ang1.html)
  - [Hungarian Language Learning
    References](http://www.rpi.edu/~sofkam/magyar.html) on the Hungarian
    Language Page (short reviews of useful books)
  - [Debrecen Summer School](http://www.nyariegyetem.hu/) (with
    Hungarian Language and Culture Courses)
  - [One of the oldest Hungarian texts - A Halotti Beszéd (The Funeral
    Oration)](https://web.archive.org/web/20060210154349/http://users.tpg.com.au/etr/oldhu/halotti.html)

### 《匈牙利人百科全书》中的语言章节 (1–5)

  - [Introduction to the History of the Language; The Pre-Hungarian
    Period; The Early Hungarian Period; The Old Hungarian
    Period](http://mek.oszk.hu/01900/01993/html/index2.html)
  - [The Linguistic Records of the Early Old Hungarian Period; The
    Linguistic System of the
    Age](http://mek.oszk.hu/01900/01955/html/index2.html)
  - [The Old Hungarian Period; The System of the Language of the Old
    Hungarian Period](http://mek.oszk.hu/01900/01949/html/index2.html)
  - [The Late Old Hungarian Period; The System of the
    Language](http://mek.oszk.hu/01900/01919/html/index2.html)
  - [The First Half of the Middle Hungarian Period; Turkish Loan
    Words](http://mek.oszk.hu/01900/01911/html/index2.html)
  - (The English translations of volumes 6 to 9 are in preparation.)

### 词典

  - [Hungarian-English-Hungarian](http://dict.sztaki.hu/english-hungarian)

### 网上语言课堂

  - [A Hungarian Language
    Course](http://www.personal.psu.edu/faculty/a/d/adr10/hungarian.html)
    by Aaron Rubin
  - [Hungarian 101](http://www.101languages.net/hungarian/) Learn
    Hungarian online
  - [Online course
    hungarotips.com](http://www.hungarotips.com/hungarian/b/)
  - [Magyaróra: New paths to the Hungarian
    language](http://www.magyarora.com/)
  - [Hungarian Language Lessons - Puzzles, Quizzes, Sound
    Files](http://www.hungarotips.com/hungarian/)

[匈牙利语](../Category/匈牙利语.md "wikilink")
[Category:芬兰-乌戈尔语族](../Category/芬兰-乌戈尔语族.md "wikilink")
[Category:元音和諧語言](../Category/元音和諧語言.md "wikilink")

1.