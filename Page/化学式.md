**化學式**（／），是一種用來表示化學物質（也可能為[元素或](../Page/元素.md "wikilink")[化合物](../Page/化合物.md "wikilink")）組成的式子。

一般情況下，由[元素符號](../Page/元素符號.md "wikilink")、[數字或其他符號組成](../Page/數字.md "wikilink")；這些符號單一行列，被限制在一個[排版](../Page/排版.md "wikilink")，並會出現[上標和](../Page/上标.md "wikilink")[下標](../Page/下标.md "wikilink")。

下為常用符號：

  - `-` 連字符號 （）
  - `()` 括號（）
  - `[]` 中括號（）
  - `{}` 大括號（）
  - `+` 正號（）
  - `−` 負號（）

## 讀法

化學式的通則讀法，一般是**從右向左**、**由正向負**，讀作**「X化X」**，如**「**ZnO**」**讀作**「[氧化鋅](../Page/氧化鋅.md "wikilink")」**；**「**HCl**」**讀作**「[氯化氫](../Page/氯化氢.md "wikilink")」**
。

當一個分子中原子個數不止一個時，還要讀出**[粒子個數](../Page/粒子數.md "wikilink")**，如「V<sub>2</sub>O<sub>5</sub>」讀作**「[五氧化二釩](../Page/五氧化二钒.md "wikilink")
」**。

帶有酸原子團的要讀成**「O酸X」**，如**「Na<sub>2</sub>SO<sub>4</sub>」**讀作**「[硫酸鈉](../Page/硫酸钠.md "wikilink")
」**。

另外，有的要讀「氫氧化X」，如「NaOH」叫**「[氫氧化鈉](../Page/氢氧化钠.md "wikilink")」** 。

而在讀含多種[氧化數的化合物時還要注意其化合價數](../Page/氧化數.md "wikilink")，例如「Fe<sup>3+</sup>」讀作**「鐵離子」**（如「FeCl<sub>3</sub>」讀作「氯化鐵」），**「Fe<sup>2+</sup>」**讀作「**亞鐵離子」**（如**「FeCl<sub>2</sub>」**讀作**「氯化亞鐵」**）。

若含結晶水應先讀出結晶水的個數讀作“X水合X”，如“CuSO<sub>4</sub>
·5H<sub>2</sub>O”讀作“五水合[硫酸銅](../Page/硫酸銅.md "wikilink")”（注：結晶水個數為1，也應讀出個數，讀作“一水合X”）。

## 化學式的分類

**化學式主要**分為下列幾種：

  - [實驗式](../Page/实验式.md "wikilink")（最簡式）
  - [分子式](../Page/分子式.md "wikilink")
  - [示性式](../Page/结构简式.md "wikilink")（結構簡式）
  - [結構式](../Page/結構式.md "wikilink")
  - [鍵線式](../Page/鍵線式.md "wikilink")
  - [電子點式](../Page/路易斯結構.md "wikilink")

### 最簡式

**最簡式**（），又稱**實驗式**、**簡式**，是用元素符號表示化合物分子中各元素的原子個數比的最簡關係式。

最簡式一般都是通過分析化學的元素分析法（）獲得的，尤其是通過有機化學中燃燒法測定化合物中碳氫比，因此最簡式又稱實驗式。過氧化氫（H<sub>2</sub>O<sub>2</sub>）以最簡式僅記作HO，儘管它的一個分子中含有兩個氫原子與兩個氧原子。另一個例子是正己烷（C<sub>6</sub>H<sub>14</sub>），它以最簡式記作C<sub>3</sub>H<sub>7</sub>。

最簡式無法表示分子內原子的具體個數，亦無法表示分子結構，但在分析物質中某種元素的比例時起到重要作用。像是乙炔、苯的實驗式均為CH，但乙炔的分子式是C<sub>2</sub>H<sub>2</sub>，苯的分子式是C<sub>6</sub>H<sub>6</sub>
。

### 分子式

**分子式**（），僅適用於分子化合物。非分子型化合物不宜用分子式表示，只能用實驗式表示。

例如：氧氣、二氧化碳、硫酸、乙烯等可用分子式O<sub>2</sub> 、CO<sub>2</sub>
、H<sub>2</sub>SO<sub>4</sub> 、C<sub>2</sub>H<sub>4</sub> 等表示。

當分子式相同時，也有可能不是一種物質 ，它們有可能是同分異構物 。

例如：乙醇和甲醚分子式相同（皆是C<sub>2</sub>H<sub>6</sub>O），但二者為不同物質。

### 示性式（结构简式）

**[示性式](../Page/结构简式.md "wikilink")，或稱结构简式，**一般用來表示有機物，其分子中所含[官能基的簡化結構式](../Page/官能基.md "wikilink")。

例如：乙醇和甲醚分子式皆相同（皆是C<sub>2</sub>H<sub>6</sub>O），如何區分二者，就須看其示性式。乙醇示性式為C<sub>2</sub>H<sub>5</sub>OH；二甲醚為CH<sub>3</sub>OCH<sub>3</sub>。

### 鍵線式

[Escitalopram-skeletal.png](https://zh.wikipedia.org/wiki/File:Escitalopram-skeletal.png "fig:Escitalopram-skeletal.png")[艾司西酞普兰的](../Page/艾司西酞普兰.md "wikilink")**键线式**结构。
只有[杂原子](../Page/杂原子.md "wikilink")、[碳-碳键和](../Page/碳-碳键.md "wikilink")[立体化学特征表现出来](../Page/立体化学.md "wikilink")，所有的碳原子、氢原子和[碳-氢键都被省略](../Page/碳-氢键.md "wikilink")，且容易书写。\[1\]
\]\]**鍵線式**（），是在紙面表示[分子結構的最常用的方法](../Page/分子结构.md "wikilink")，在表示[有機化合物的立體結構時尤其常用](../Page/有机化合物.md "wikilink")。用鍵線式表示的結構簡明易懂，並且容易書寫。

### 結構式

**結構式**（）是用元素符號和短線表示化合物（或單質）分子中原子的排列和結合方式的式子。是一簡單描述分子式的方法，常用於有機化合物。

以乙醇為例，舉例如下:

[Ethanol-structure.svg](https://zh.wikipedia.org/wiki/File:Ethanol-structure.svg "fig:Ethanol-structure.svg")

### 路易斯結構式

**路易斯結構式**（），又稱為**電子點式**，可以表達分子中各原子之间的[化学键以及每個可能的](../Page/化学键.md "wikilink")[孤对电子對](../Page/電子.md "wikilink")。

## 有機化合物

表示[有機化合物的化學式時](../Page/有機化合物.md "wikilink")，常有兩種方法：[分子式與](../Page/分子式.md "wikilink")[結構式](../Page/結構式.md "wikilink")。\[2\]\[3\]

  - 分子式僅表示一種物質的組成成份，如[乙烯的分子式](../Page/乙烯.md "wikilink")：

<!-- end list -->

  -
    C<sub>2</sub>H<sub>4</sub>

<!-- end list -->

  - 結構式則亦表示了一種物質的組成結構。

<!-- end list -->

  -
    通常來說，分子式可能會受[同分異構體的困擾](../Page/同分異構體.md "wikilink")，而結構式則不會。

<!-- end list -->

  - 如乙烯的結構式：

<!-- end list -->

  -
    H<sub>2</sub>C=CH<sub>2</sub>

注意這裡的兩個碳原子間用雙橫線連接以示雙鍵。表示三鍵時，則可用三橫線。單鍵的橫線通常不寫，但在可能存在歧義時應書寫。

此外，在書寫分子較大的有機物的示性式時，可以將重複的部分簡記，如正十八烷記作：

  -
    CH<sub>3</sub>(CH<sub>2</sub>)<sub>16</sub>CH<sub>3</sub>

有時也用n作為未知數代表通式，如[烷烴通式C](../Page/烷烴.md "wikilink")<sub>n</sub>H<sub>2n+2</sub>。

  - [芳香烴的結構式通常用](../Page/芳香烴.md "wikilink")[苯環表示](../Page/苯環.md "wikilink")。如[2,4,6-三硝基甲苯](../Page/三硝基甲苯.md "wikilink")：

<!-- end list -->

  -
    [Trinitrotoluene.svg](https://zh.wikipedia.org/wiki/File:Trinitrotoluene.svg "fig:Trinitrotoluene.svg")

## 參看

  - [化学方程式](../Page/化学方程式.md "wikilink")
  - [离子方程式](../Page/离子方程式.md "wikilink")

## 注釋

[Category:化学式](../Category/化学式.md "wikilink")

1.  *General, Organic, and Biological Chemistry*, H. Stephen Stoker 2012
2.  [第五章化學反應](http://www.mingdao.edu.tw/physics/pdf/matter_05.pdf),物質科學化學篇(上)講義.
    編者：陳義忠, 1986
3.  [分子式（Molecular Formula）與結構式（Structural
    Formula）](http://case.ntu.edu.tw/hs/wordpress/?p=4510),
    國科會高瞻自然科學教學資源平台