**反应速率**（英語：Rate of
reaction）是在化學反應中，[反應物轉變成](../Page/反应物.md "wikilink")[生成物的速度](../Page/生成物.md "wikilink")。不同反應的速率有所不同。例如鐵的[生鏽](../Page/生銹.md "wikilink")（氧化）過程的需時以年來計算；在火中燃燒[纖維素](../Page/纤维素.md "wikilink")，卻只需要數秒鐘的時間。對於大部分的反應，反應速率隨時間而減少。

反應速率可以用這種方式來表達：

\(\Delta v(A)=\frac{\Delta [A]}{\Delta t}\)

反应速率的单位为mol/(L·s)或mol/(L·min)。浓度单位一般用[莫耳](../Page/摩尔_\(单位\).md "wikilink")·升<sup>-1</sup>，时间单位用秒、分或小时。反应速率分为平均速率（一定时间间隔裡平均反应速率）和瞬时速率（给定某时刻的反应速率），可通过实验测定。

## 碰撞学说

碰撞理论，是由[德国的Max](../Page/德国.md "wikilink")
Trautz及[英国的William](../Page/英国.md "wikilink") Lewis
在1916年及1918年分别提出的。

1.  [碰撞学说](../Page/碰撞学说.md "wikilink")：任何化学反应的发生，必需反应粒子互相接近碰撞，则反应速率与碰撞次数成正比。
2.  [活化能](../Page/活化能.md "wikilink")：所谓活化能就是能使粒子发生反应的最低能量。
3.  [有效碰撞](../Page/有效碰撞.md "wikilink")：所谓有效碰撞是指碰撞的粒子其能量超过活化能,且碰撞方向(位向)要正確（发生化学反应所需的能量）。

## 影響反應速率的因素

反应物本身的性质，外界因素：[温度](../Page/温度.md "wikilink")，[浓度](../Page/浓度.md "wikilink")，[壓力](../Page/壓力.md "wikilink")，[催化剂](../Page/催化剂.md "wikilink")，[光](../Page/光.md "wikilink")，[激光](../Page/激光.md "wikilink")，反应物颗粒大小，反应物之间的接触面积和反应物状态，x射线，γ射线，固体物质的表面积，与反应物的接触面积，反应物的浓度也会影响化学反应速率。

增加反应物的浓度，即增加了单位体积内[活化分子的数目](../Page/活化分子.md "wikilink")，从而增加了单位时间内反应物[分子的有效碰撞的次数](../Page/分子.md "wikilink")，导致反应速率加快。提高反应温度，即增加了活化分子的百分数，也增加了单位时间内反应物分子有效碰撞的次数，导致反应速率加快。使用正催化剂，改变了反应历程，降低了反应所需的活化能，使反应速率加快。

在[化工生产中](../Page/化工.md "wikilink")，常控制[反应条件来加快反应速率](../Page/反应条件.md "wikilink")，以增加产量。有时也要采取减慢反应速率的措施，以延长产品的使用时间。

[fr:Cinétique chimique\#Vitesse de
réaction](../Page/fr:Cinétique_chimique#Vitesse_de_réaction.md "wikilink")
[th:อัตราการเกิดปฏิกิริยาเคมี](../Page/th:อัตราการเกิดปฏิกิริยาเคมี.md "wikilink")

[F](../Category/化學反應.md "wikilink")
[Category:化学动力学](../Category/化学动力学.md "wikilink")
[Category:化學反應工程](../Category/化學反應工程.md "wikilink")