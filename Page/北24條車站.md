**北24條車站**（）是一位於[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[札幌市](../Page/札幌市.md "wikilink")[北區北](../Page/北區_\(札幌市\).md "wikilink")23條西4丁目，隸屬於[札幌市交通局的](../Page/札幌市交通局.md "wikilink")[地下鐵車站](../Page/鐵路車站.md "wikilink")。北24條車站是[札幌市營地下鐵南北線的沿線車站之一](../Page/札幌市營地下鐵南北線.md "wikilink")，車站編號N03。

在北24條車站現址以西約100公尺處的地面上，曾經存在過一個與本站同名的[路面電車車站](../Page/路面電車.md "wikilink")。該站是已廢線的[札幌市電](../Page/札幌市電.md "wikilink")[鐵北線沿線車站之一](../Page/鐵北線.md "wikilink")，在1971年地下鐵南北線通車之後，由於路面電車線的運輸能力已由地下鐵所取代，而在1974年5月時遭到撤除。北24條車站在1978年北24條－[麻生間的延伸路段通車之前](../Page/麻生車站.md "wikilink")，曾一度是南北線最北端的起點站。

## 車站結構

地下1樓為閘口，地下2樓為1面2線島式月台，月台南側設有升降機。出口共4個，通往巴士總站內的樓梯共3個。地面的升降機設於3號出口。

### 月台配置

| 月台    | 路線                                                                                                                             | 目的地                                                                                                                     |
| ----- | ------------------------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------- |
| **1** | [Subway_SapporoNamboku.svg](https://zh.wikipedia.org/wiki/File:Subway_SapporoNamboku.svg "fig:Subway_SapporoNamboku.svg") 南北線 | [札幌](../Page/札幌站_\(札幌市營地下鐵\).md "wikilink")、[大通](../Page/大通站_\(北海道\).md "wikilink")、[真駒內方向](../Page/真駒內站.md "wikilink") |
| **2** | [麻生方向](../Page/麻生站.md "wikilink")                                                                                              |                                                                                                                         |

## 相鄰車站

  - [ST_Logo.svg](https://zh.wikipedia.org/wiki/File:ST_Logo.svg "fig:ST_Logo.svg")
    札幌市營地下鐵
    [Subway_SapporoNamboku.svg](https://zh.wikipedia.org/wiki/File:Subway_SapporoNamboku.svg "fig:Subway_SapporoNamboku.svg")
    南北線
      -
        [北34條](../Page/北34條站.md "wikilink")（N02）－**北24條（N03）**－[北18條](../Page/北18條站.md "wikilink")（N04）

[Tanijuuyojou](../Category/日本鐵路車站_Ki.md "wikilink") [Category:北區鐵路車站
(札幌市)](../Category/北區鐵路車站_\(札幌市\).md "wikilink") [Category:南北線車站
(札幌市營地下鐵)](../Category/南北線車站_\(札幌市營地下鐵\).md "wikilink")
[Category:1971年啟用的鐵路車站](../Category/1971年啟用的鐵路車站.md "wikilink")
[Category:以街道命名的鐵路車站](../Category/以街道命名的鐵路車站.md "wikilink")