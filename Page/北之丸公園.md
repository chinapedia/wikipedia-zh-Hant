**北之丸公園**（）是[日本](../Page/日本.md "wikilink")[東京都](../Page/東京都.md "wikilink")[千代田区的町名之一](../Page/千代田区.md "wikilink")，名稱源自下文介紹的同名[公園](../Page/公園.md "wikilink")。[郵遞區號為](../Page/郵遞區號.md "wikilink")102-0091。

## 北之丸公園（公園）

**北之丸公園**（）是位於[日本](../Page/日本.md "wikilink")[東京都](../Page/東京都.md "wikilink")[千代田区北之丸公園](../Page/千代田区.md "wikilink")（地區）的[國民公園](../Page/國民公園.md "wikilink")。如其名所言，位於[江戶城北之丸](../Page/江戶城.md "wikilink")（）範圍。公園有大量舊江戶城遺跡（如田安門、清水門等[重要文化財產](../Page/重要文化財產.md "wikilink")），與[皇居外苑和](../Page/皇居外苑.md "wikilink")[日比谷公園並稱為都會的綠洲](../Page/日比谷公園.md "wikilink")。

### 歷史

最初為[太田道灌建築江戶城時](../Page/太田道灌.md "wikilink")，祭祀關東守護神（舊田安明神）的舊址，之後在[德川家康入府時](../Page/德川家康.md "wikilink")，本地為關東代官的屋敷所在地，因而稱為「代官町」。

之後為[德川忠長與](../Page/德川忠長.md "wikilink")[德川綱重屋敷地](../Page/德川綱重.md "wikilink")，和[德川氏](../Page/德川氏.md "wikilink")[御三卿](../Page/御三卿.md "wikilink")[田安德川家與](../Page/田安德川家.md "wikilink")[清水德川家上屋敷用地](../Page/清水德川家.md "wikilink")，敷地内有倉庫用地、植池御用地、馬場等設施。

維新後，[明治政府在此設置](../Page/明治.md "wikilink")[近衛師團營地](../Page/近衛師團.md "wikilink")（現在的[東京國立近代美術館工藝館改建自近衛師團司令部](../Page/東京國立近代美術館.md "wikilink")）。

戰後，1946年（[昭和](../Page/昭和.md "wikilink")21年）東京特別都市計劃決定整備皇居周邊的綠地，拆除舊近衛連隊等多項建物。1963年（昭和38年），建設省整修此地為森林公園，1967年（昭和42年）實施住居表示改稱為「北之丸公園」。1969年（昭和44年）[昭和天皇為慶祝六十大壽開園](../Page/昭和天皇.md "wikilink")。之後由[厚生省管理](../Page/厚生省.md "wikilink")，現在為[環境省管理的國民公園](../Page/環境省.md "wikilink")。

## 北之丸公園（町名）

### 地理

  - 河川、橋

<!-- end list -->

  - [竹橋](../Page/竹橋.md "wikilink")

<!-- end list -->

  - 坂名

<!-- end list -->

  - 紀伊國坂

### 歷史

過去稱為代官町，1967年（昭和42年）實施住居表示，正式町名為「千代田區北之丸公園」。代官町的名稱目前仍存在於首都高速道路出入口名。

### 町名由來

來自此地的「北之丸公園」。

### 地域

西側為遊艇碼頭與千鳥淵公園（千鳥淵戰死者墓苑在三番町）。住居表示的街区分為1番至6番，其中4番內有警視廳第一機動隊，5番內有宮内廳代官町宿舎，6番有皇宮警察宿舎。2015年2月1日止的人口為646人\[1\]。

  - [氣象廳](../Page/氣象廳.md "wikilink")[百葉箱](../Page/百葉箱.md "wikilink")（預定2013年從大手町廳舍轉移設備）

<!-- end list -->

  - 機關

<!-- end list -->

  - [警視廳](../Page/警視廳.md "wikilink") 第一
  - [皇宮警察](../Page/皇宮警察本部.md "wikilink") 北之丸宿舎
  - [國立公文書館](../Page/國立公文書館.md "wikilink")

### 觀光

  - 景點、古蹟

<!-- end list -->

  -
  - [東京國立近代美術館](../Page/東京國立近代美術館.md "wikilink")

  - [日本武道館](../Page/日本武道館.md "wikilink")

  - [吉田茂銅像](../Page/吉田茂.md "wikilink")\[2\]

### 交通

  - 首都高速道路、出入口

<!-- end list -->

  - [首都高速都心環狀線](../Page/首都高速都心環狀線.md "wikilink")
  - [北之丸出口](../Page/北之丸出口.md "wikilink")
  - [代官町出入口](../Page/代官町出入口.md "wikilink")

## 交通方式

  - [竹橋站步行](../Page/竹橋站.md "wikilink")5分，[九段下站步行徒歩](../Page/九段下站.md "wikilink")5分。
  - 停車場（小型車3小時400圓，之後每1小時100圓。夜間關閉）

<!-- end list -->

  -
    第一停車場 - 小型車145台
    第二停車場 - 小型車107台
    第三停車場 - 小型車260台

## 關連項目

  -
  - [竹橋](../Page/竹橋.md "wikilink")

## 照片

<file:01> Zosterops japonicus.jpg|北之丸公園[梅花](../Page/梅.md "wikilink")
<file:Kitanomaru> Park 01.jpg|田安門遙望[九段會館](../Page/九段會館.md "wikilink")
<file:Kitanomaru>
park01s3072.jpg|北之丸公園内[B-29超級堡壘轟炸機迎撃用高射炮台座跡](../Page/B-29超級堡壘轟炸機.md "wikilink")
<file:Cherry> Blossom -Kitanomaru
Park.jpg|千鳥淵附近的[樱花](../Page/樱.md "wikilink") <File:A>
sparrow in Kitanomaru Park.jpg|[麻雀](../Page/麻雀.md "wikilink")
<File:Kitanomaru> park02s3072.jpg <File:Green> Alley -Kitanomaru
Park.jpg|[夏天的北之丸](../Page/夏天.md "wikilink") <File:Kitanomaru>
park03s3072.jpg <File:Kitanomaru> park04s3072.jpg <File:Kitanomaru.jpg>
<File:Kitashiragawa> near Imperial Palace
Tokyo.jpg|[北白川宮能久親王銅像](../Page/北白川宮能久親王.md "wikilink")
<File:Signboard> of Kitanomaru Park.JPG <File:Sparrows> -Kitanomaru
Park.jpg|[麻雀](../Page/麻雀.md "wikilink") <File:Takebashi>
tokyo.jpg|[竹桥](../Page/竹桥.md "wikilink")

## 備註

## 外部連結

  - [皇居外苑](https://web.archive.org/web/20061106054223/http://www.env.go.jp/nature/nationalgardens/kohkyo/index.html)
  - [東京國立近代美術館](http://www.momat.go.jp/)
  - [築土神社（北之丸公園的氏神）](http://www.tsukudo.jp/)
  - [日本武道館](http://www.nipponbudokan.or.jp/)

[Category:東京都公園](../Category/東京都公園.md "wikilink")
[Category:東京都區部地區](../Category/東京都區部地區.md "wikilink")
[Category:千代田區町名](../Category/千代田區町名.md "wikilink")
[Category:江戶城](../Category/江戶城.md "wikilink")

1.  [町丁別世帯数および人口（住民基本台帳）：平成27年2月1日現在-千代田区総合ホームページ](http://www.city.chiyoda.lg.jp/koho/kuse/toke/cho-setai/cho-setaih2702.html)
2.  [吉田茂像（北の丸公園）](http://chiyoda-tokyo.sakura.ne.jp/pic-htm/yosidasg.htm)
    千代田遺産