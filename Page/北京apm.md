[Beijing_apm,_Wangfujing,_2008-07-14.jpg](https://zh.wikipedia.org/wiki/File:Beijing_apm,_Wangfujing,_2008-07-14.jpg "fig:Beijing_apm,_Wangfujing,_2008-07-14.jpg")

**北京apm**，原名**新东安市场**、**新东安广场**，是位于[北京市](../Page/北京市.md "wikilink")[王府井大街北段的一座商业](../Page/王府井大街.md "wikilink")[大厦](../Page/大厦.md "wikilink")。

## 简介

[Beijing_apm,_2008-10-19.jpg](https://zh.wikipedia.org/wiki/File:Beijing_apm,_2008-10-19.jpg "fig:Beijing_apm,_2008-10-19.jpg")

1993年开始由北京[东安集团和](../Page/东安集团.md "wikilink")[香港](../Page/香港.md "wikilink")[新鸿基地产有限公司合资兴建](../Page/新鸿基地产.md "wikilink")，在[东安市场原址建设](../Page/东安市场.md "wikilink")，1998年竣工开业，名为新东安市场，\[1\]
获得1999年度建筑工程[鲁班奖](../Page/鲁班奖.md "wikilink")。

该建筑地上11层，地下3层，具有购物、餐饮、娱乐和办公多种功能。[新东安影城](../Page/新东安影城.md "wikilink")、[东来顺新东安总店均位于此处](../Page/东来顺.md "wikilink")。2005年11月开始，原新东安市场进行升级改造，改建成高档购物场所\[2\]。2008年4月20日正式更名为**北京apm**。\[3\]
名字乃參考位於香港[觀塘](../Page/觀塘.md "wikilink")，同屬新鴻基地產的[創紀之城購物商場](../Page/創紀之城.md "wikilink")
[apm](../Page/apm_\(香港\).md "wikilink")\[4\]。

## 参见

  - [apm (香港)](../Page/apm_\(香港\).md "wikilink")
  - [东安市场](../Page/东安市场.md "wikilink")
  - 上海[环贸iapm商场](../Page/环贸iapm商场.md "wikilink")

## 参考文献

## 外部链接

  - [北京apm官方网站](http://www.beijingapm.cn/)

[Category:北京市商场](../Category/北京市商场.md "wikilink")
[Category:新鴻基地產物業](../Category/新鴻基地產物業.md "wikilink")
[Category:鲁班奖获奖工程](../Category/鲁班奖获奖工程.md "wikilink")
[Category:1998年完工的辦公建築物](../Category/1998年完工的辦公建築物.md "wikilink")
[Category:1998年中國建立](../Category/1998年中國建立.md "wikilink")

1.
2.
3.
4.