[Beijingbelltower1.jpg](https://zh.wikipedia.org/wiki/File:Beijingbelltower1.jpg "fig:Beijingbelltower1.jpg")
[Beijing_drum_tower.JPG](https://zh.wikipedia.org/wiki/File:Beijing_drum_tower.JPG "fig:Beijing_drum_tower.JPG")
[Beijing_hongzhou_Road,_jingshan_to_di'anmen.jpg](https://zh.wikipedia.org/wiki/File:Beijing_hongzhou_Road,_jingshan_to_di'anmen.jpg "fig:Beijing_hongzhou_Road,_jingshan_to_di'anmen.jpg")
[Forbidden_city_01.jpg](https://zh.wikipedia.org/wiki/File:Forbidden_city_01.jpg "fig:Forbidden_city_01.jpg")
[Gugun_panorama-2005-1.jpg](https://zh.wikipedia.org/wiki/File:Gugun_panorama-2005-1.jpg "fig:Gugun_panorama-2005-1.jpg")
[钦安殿.JPG](https://zh.wikipedia.org/wiki/File:钦安殿.JPG "fig:钦安殿.JPG")
[乾清宮.JPG](https://zh.wikipedia.org/wiki/File:乾清宮.JPG "fig:乾清宮.JPG")
[Forbiddencitybeijing.jpg](https://zh.wikipedia.org/wiki/File:Forbiddencitybeijing.jpg "fig:Forbiddencitybeijing.jpg")
[Forbidden_city_05.jpg](https://zh.wikipedia.org/wiki/File:Forbidden_city_05.jpg "fig:Forbidden_city_05.jpg")
[Forbidden_City1.JPG](https://zh.wikipedia.org/wiki/File:Forbidden_City1.JPG "fig:Forbidden_City1.JPG")
[Gate_of_Supreme_Harmony.JPG](https://zh.wikipedia.org/wiki/File:Gate_of_Supreme_Harmony.JPG "fig:Gate_of_Supreme_Harmony.JPG")
[Meridan_Gate.jpg](https://zh.wikipedia.org/wiki/File:Meridan_Gate.jpg "fig:Meridan_Gate.jpg")
[Forbiddencityviewpic10.jpg](https://zh.wikipedia.org/wiki/File:Forbiddencityviewpic10.jpg "fig:Forbiddencityviewpic10.jpg")
[Tiananmen.JPG](https://zh.wikipedia.org/wiki/File:Tiananmen.JPG "fig:Tiananmen.JPG")
[Zhengyangmen_menlou.JPG](https://zh.wikipedia.org/wiki/File:Zhengyangmen_menlou.JPG "fig:Zhengyangmen_menlou.JPG")
[Qianmenpic1.jpg](https://zh.wikipedia.org/wiki/File:Qianmenpic1.jpg "fig:Qianmenpic1.jpg")
[CR400AF-0208_at_Yongdingmen_(20180301155615).jpg](https://zh.wikipedia.org/wiki/File:CR400AF-0208_at_Yongdingmen_\(20180301155615\).jpg "fig:CR400AF-0208_at_Yongdingmen_(20180301155615).jpg")

**北京中轴线**是自[元大都](../Page/元大都.md "wikilink")、[明清北京城以来](../Page/明清北京城.md "wikilink")[北京城东西对称布局](../Page/北京.md "wikilink")[建筑物的](../Page/建筑物.md "wikilink")[对称轴](../Page/对称轴.md "wikilink")，北京市诸多其他建筑物亦位于此条轴线上。北京中轴线是世界文化遗产储备项目，北京市已于2018年开启对中轴线的全面整治计划。

## 元

在元代，[元大都城墙即为左右对称](../Page/元大都.md "wikilink")，南边正门[丽正门即在中轴线上](../Page/丽正门.md "wikilink")。皇城坐落在中轴线上，也是左右对称的。

但自皇城以北，中轴线向西偏斜2°。在这个偏斜中轴线上的[地安门向西偏离子午线](../Page/地安门.md "wikilink")200多米，而元大都的[钟鼓楼已向西偏离子午线近](../Page/钟鼓楼.md "wikilink")300米。这个偏斜的中轴线顺延270余公里后，即为[忽必烈入主中原前的国都](../Page/忽必烈.md "wikilink")[元上都](../Page/元上都.md "wikilink")（今[锡林郭勒](../Page/锡林郭勒.md "wikilink")[正蓝旗](../Page/正蓝旗.md "wikilink")[兆奈曼苏默](../Page/兆奈曼苏默.md "wikilink")）。

## 明清

除[西直门以北](../Page/西直门.md "wikilink")[北京城墙的一角外](../Page/北京城墙.md "wikilink")，[明清北京城基本按元代的中轴线对称](../Page/明清北京城.md "wikilink")（从卫星地图上看北京故宫可以明显发现其西斜），但北部偏斜的中轴线被矫正过来。

明清北京城的中轴线上建筑从南往北依次为：[永定门箭楼](../Page/永定门.md "wikilink")（1957年拆除）、[永定门城楼](../Page/永定门.md "wikilink")（1957年拆除，2005年重建）、[天桥](../Page/北京天桥.md "wikilink")（1934年拆除，后重建）、[正阳桥坊](../Page/正阳桥坊.md "wikilink")（五牌楼）、[正阳门](../Page/正阳门.md "wikilink")（[前门](../Page/前门.md "wikilink")）箭楼，[正阳门城楼](../Page/正阳门.md "wikilink")、[中华门](../Page/中华门_\(北京\).md "wikilink")（明称大明门，清称大清门，[民国时改为中华门](../Page/中华民国.md "wikilink")，1954年拆除）、[天安门](../Page/天安门.md "wikilink")、[端门](../Page/端门.md "wikilink")、[午门](../Page/午门.md "wikilink")、[太和门](../Page/太和门.md "wikilink")、[太和殿](../Page/太和殿.md "wikilink")、[中和殿](../Page/中和殿.md "wikilink")、[保和殿](../Page/保和殿.md "wikilink")、[乾清门](../Page/乾清门.md "wikilink")、[乾清宫](../Page/乾清宫.md "wikilink")、[交泰殿](../Page/交泰殿.md "wikilink")、[坤宁宫](../Page/坤宁宫.md "wikilink")、[坤宁门](../Page/坤宁门.md "wikilink")、[御花园](../Page/御花园.md "wikilink")、[钦安殿](../Page/钦安殿.md "wikilink")、[顺贞门](../Page/顺贞门.md "wikilink")、[神武门](../Page/神武门.md "wikilink")、[北上门](../Page/北上门.md "wikilink")（1956年拆除）、[景山门](../Page/景山门.md "wikilink")、[绮望楼](../Page/绮望楼.md "wikilink")、[万春亭](../Page/万春亭.md "wikilink")、[寿皇门](../Page/寿皇门.md "wikilink")、[寿皇殿](../Page/寿皇殿.md "wikilink")、[地安门](../Page/地安门.md "wikilink")（1954年拆除）、[万宁桥](../Page/万宁桥.md "wikilink")、[鼓楼和钟楼](../Page/北京鼓楼和钟楼.md "wikilink")。

从北往南依次为：[钟楼](../Page/北京鼓楼和钟楼.md "wikilink")、[鼓楼](../Page/北京鼓楼和钟楼.md "wikilink")、[万宁桥](../Page/万宁桥.md "wikilink")、[地安门](../Page/地安门.md "wikilink")、[景山](../Page/景山公园.md "wikilink")（[寿皇殿](../Page/寿皇殿.md "wikilink")、[寿皇门](../Page/寿皇门.md "wikilink")、[万春亭](../Page/万春亭.md "wikilink")、[绮望楼](../Page/绮望楼.md "wikilink")、[景山门](../Page/景山门.md "wikilink")、[北上门](../Page/北上门.md "wikilink")）、[故宫](../Page/故宫.md "wikilink")（[神武门](../Page/神武门.md "wikilink")、[顺贞门](../Page/顺贞门.md "wikilink")、[钦安殿](../Page/钦安殿.md "wikilink")、[御花园](../Page/御花园.md "wikilink")、[坤宁门](../Page/坤宁门.md "wikilink")、[坤宁宫](../Page/坤宁宫.md "wikilink")、[交泰殿](../Page/交泰殿.md "wikilink")、[乾清宫](../Page/乾清宫.md "wikilink")、[乾清门](../Page/乾清门.md "wikilink")、[保和殿](../Page/保和殿.md "wikilink")、[中和殿](../Page/中和殿.md "wikilink")、[太和殿](../Page/太和殿.md "wikilink")、[太和门](../Page/太和门.md "wikilink")、[午门](../Page/午门.md "wikilink")）、[端门](../Page/端门.md "wikilink")、[天安门](../Page/天安门.md "wikilink")、[中华门](../Page/中华门_\(北京\).md "wikilink")、[正阳门城楼](../Page/正阳门.md "wikilink")、[正阳门箭楼](../Page/正阳门箭楼.md "wikilink")、[正阳桥坊](../Page/正阳桥坊.md "wikilink")、[天桥](../Page/北京天桥.md "wikilink")、[永定门城楼](../Page/永定门.md "wikilink")、永定门箭楼。

从这条中轴线的南端永定门起，就有[天坛](../Page/天坛.md "wikilink")─[先农坛](../Page/先农坛.md "wikilink")、[东便门](../Page/东便门.md "wikilink")─[西便门](../Page/西便门.md "wikilink")、[崇文门](../Page/崇文门.md "wikilink")─[宣武门](../Page/宣武门.md "wikilink")、[太庙](../Page/太庙.md "wikilink")─[社稷坛](../Page/社稷坛.md "wikilink")、[东三座门](../Page/东三座门.md "wikilink")─[西三座门](../Page/西三座门.md "wikilink")、[长安左门](../Page/长安左门.md "wikilink")─[长安右门](../Page/长安右门.md "wikilink")、[东华门](../Page/东华门.md "wikilink")─[西华门](../Page/西华门.md "wikilink")、[东直门](../Page/东直门.md "wikilink")─[西直门](../Page/西直门.md "wikilink")、[安定门](../Page/安定门_\(北京\).md "wikilink")─[德胜门以中轴线为轴对称分布](../Page/德胜门.md "wikilink")。中国著名建筑大师[梁思成先生曾经说](../Page/梁思成.md "wikilink")：“北京的独有的壮美秩序就由这条中轴线的建立而产生。”

[故宫的建筑多数东西对称](../Page/故宫.md "wikilink")。[太和殿等主要建筑坐落在中轴线上](../Page/太和殿.md "wikilink")。

人们在[后门桥的河泥裡发现石老鼠](../Page/后门桥.md "wikilink")（[地支子鼠](../Page/地支.md "wikilink")），在前门附近的河里发现石马（[地支午马](../Page/地支.md "wikilink")），据说这是北京中轴线（子午线）的标志物。

## [中华民国](../Page/中华民国.md "wikilink")

民國時期，北京（[北平](../Page/北平.md "wikilink")）中軸線並無太大變動，[北洋政府時期拆除](../Page/北洋政府.md "wikilink")[天安門前的千步廊及](../Page/天安門.md "wikilink")[前門](../Page/前門.md "wikilink")[甕城](../Page/甕城.md "wikilink")。

## [中华人民共和国](../Page/中华人民共和国.md "wikilink")

### 中軸線的破壞

在建國初拆除北京城的運動中，中軸線的[永定門城樓](../Page/永定門.md "wikilink")（已复建）及甕城（计划复建）、[天橋](../Page/天橋.md "wikilink")（已复建）、正陽橋牌樓（前門五牌樓，已复建）、正陽橋、[中華門](../Page/中华门_\(北京\).md "wikilink")、北上門、[地安門](../Page/地安門.md "wikilink")（其雁翅楼已复建）均被拆除。

### 改造[天安门广场](../Page/天安门广场.md "wikilink")

1949年[开国大典上使用的](../Page/开国大典.md "wikilink")[旗杆即在中轴线上](../Page/旗杆.md "wikilink")。

1952年，北京拆除长安左门、长安右门，1954年，北京拆除[中华门](../Page/中华门_\(北京\).md "wikilink")，使得[天安门广场扩大到现在的规模](../Page/天安门广场.md "wikilink")。广场中央建造[人民英雄纪念碑](../Page/人民英雄纪念碑.md "wikilink")，东西两侧则是基本对称的[人民大会堂](../Page/人民大会堂.md "wikilink")（西侧）、[中国国家博物馆](../Page/中国国家博物馆.md "wikilink")（东侧）（原为[中国革命博物馆及](../Page/中国革命博物馆.md "wikilink")[中国历史博物馆](../Page/中国历史博物馆.md "wikilink")）。

1976年[毛泽东逝世之后](../Page/毛泽东.md "wikilink")，在原中华门的位置建造[毛主席纪念堂](../Page/毛主席纪念堂.md "wikilink")。

### 中轴线北延

北京申办[1990年亚运会成功之后](../Page/1990年亚运会.md "wikilink")，为了连接城市中心和亚运村，北京在[二环路](../Page/北京二环路.md "wikilink")[钟鼓楼桥引出](../Page/钟鼓楼桥.md "wikilink")[鼓楼外大街](../Page/鼓楼外大街.md "wikilink")，向北至[三环后改名为](../Page/北京三环路.md "wikilink")[北辰路](../Page/北辰路_\(北京\).md "wikilink")，这条路成为北京中轴线的延伸。西边建造[中华民族园](../Page/中华民族园.md "wikilink")，东边则是[奥体中心](../Page/奥林匹克体育中心_\(北京\).md "wikilink")。

### [奥林匹克公园](../Page/奥林匹克公园_\(北京\).md "wikilink")

北京申奥成功后，中轴线再次向北延长，成为奥林匹克公园的轴线。东边建造[国家体育场“鸟巢”](../Page/国家体育场.md "wikilink")，西边则是[国家游泳中心“水立方”](../Page/国家游泳中心.md "wikilink")。这两个建筑一圆一方，体现中国古代[天圆地方的思想](../Page/天圆地方.md "wikilink")。

再向北，穿过奥林匹克公园，到达[奥林匹克森林公园](../Page/奥林匹克森林公园.md "wikilink")，该公园中间的[仰山](../Page/仰山.md "wikilink")、[奥海均在中轴线上](../Page/奥海.md "wikilink")。

奥林匹克公园中轴线地下已有[北京地铁8号线](../Page/北京地铁8号线.md "wikilink")。该线将延长，成为贯穿北京南北中轴线地下交通大动脉。

中轴线再进行北延的设想已经出现，计划把奥林匹克森林公园和往北的一系列森林公园连片，形成一个新的奥林匹克森林公园，一直向北延伸到北六环。但具体规划尚未出台。

### 南中轴治理计划

2003年，北京市启动南中轴治理计划。对前门以南的道路进行改造。\[1\]

2008年，南中轴[快速公交全线贯通](../Page/快速公交.md "wikilink")，自前门至德茂庄，全程15.8公里。\[2\]

2018年，北京市面向全球征集南中轴线规划方案。

### 前门大街改造

2007年5月9日，前门大街前门至珠市口段开始改造，2008年5月28日完工\[3\]，同年8月7日对外开放\[4\]。改造工程重现前门大街明末清初的建筑风格。改造恢复前门大街的有轨电车“当当车”，2009年元旦开始运营\[5\]。

### 中轴线的申遗

截至2018年6月2日，《北京中轴线申遗保护规划》、《北京中轴线申报世界遗产文本》、《北京中轴线申遗综合整治规划纲要》（后文简称《纲要》）已编制完成，而据《纲要》确定的《2018年中轴线申遗综合整治重点任务》提出了文物腾退类11项、文物修缮类7项、风貌整治类8项、文化设施类1项的任务清单。目前市文物局正在编制《北京中轴线申遗综合整治规划实施计划》，将对中轴线遗产点和遗产环境内的文保单位保护、道路景观塑造、历史水系恢复、视廊对景保护、天际线控制、色彩体量和屋顶形式控制、历史街区风貌提升与民生改善、文化展示、遗产管理等方面按照近、中、远期提出不同阶段的整治和实施要求。刚刚启动编制的《北京中轴线申遗风貌设计管理导则》将对重要节点、沿街立面、整改地块和缓冲区通则提出设计、管理方案，并将加强对老城中轴线申遗核心区内新建项目外形、体量、业态的把控。\[6\]

## 中轴线道路

  - [北辰路](../Page/北辰路_\(北京\).md "wikilink") 至
    [奥林匹克森林公园](../Page/奥林匹克森林公园.md "wikilink")
  - [鼓楼外大街](../Page/鼓楼外大街.md "wikilink")
  - [地安门外大街](../Page/地安门外大街.md "wikilink") 至
    [鼓楼和钟楼](../Page/北京鼓楼和钟楼.md "wikilink")，到[二环路](../Page/北京二环路.md "wikilink")
  - [地安门内大街](../Page/地安门内大街.md "wikilink")
    至[天安门广场](../Page/天安门广场.md "wikilink")、[故宫](../Page/故宫.md "wikilink")、[景山](../Page/景山.md "wikilink")
  - [前门大街](../Page/前门大街.md "wikilink")
  - [天桥南大街](../Page/天桥南大街.md "wikilink")
  - [永定门内大街](../Page/永定门内大街.md "wikilink")
  - [永定门外大街](../Page/永定门外大街.md "wikilink")
  - [南苑路](../Page/南苑路_\(北京\).md "wikilink")

## 注释

## 参考文献

## 外部链接

  - [中軸線怎是歪的?\!元代建北京城時為何修歪了?](http://cul.china.com.cn/lishi/2013-04/02/content_5847765.htm)

  -
## 参见

  - [元大都](../Page/元大都.md "wikilink")、[明清北京城](../Page/明清北京城.md "wikilink")
  - [对称轴](../Page/对称轴.md "wikilink")
  - [广州城市中轴线](../Page/广州城市中轴线.md "wikilink")

{{-}}

[Category:城市中轴线](../Category/城市中轴线.md "wikilink")
[北京中轴线](../Category/北京中轴线.md "wikilink")
[Category:北京市地理](../Category/北京市地理.md "wikilink")

1.  [整治历史环境
    再现名城景观—关于恢复南中轴路传统景观与复建永定门城楼及其意义](http://www.bjww.gov.cn/2004/7-28/3170.shtml)
2.  [北京南中轴路快速公交全线贯通](http://bbs.tranbbs.com/dispbbs.asp?boardid=45&Id=15627)

3.  [前门大街改造完工](http://beijing.qianlong.com/3825/2008/05/28/3562@4464640.htm)
4.  [前门大街8月7日开街
    当当车暂不对游客开放](http://news.sohu.com/20080729/n258450772.shtml)
5.  [前门当当车元旦开放](http://www.bj.xinhuanet.com/bjpd_sdwm/2008-12/29/content_15313137.htm)

6.  [2018年北京中轴线遗产区将腾退11处文物](http://beijing.qianlong.com/2018/0602/2611510.shtml)