**北京四合院**是位于[中国](../Page/中国.md "wikilink")[北京的](../Page/北京.md "wikilink")[四合院建筑](../Page/四合院.md "wikilink")。由于北京是四合院最常见也最有特色的一个城市，因此人们提起四合院常常就是指北京四合院。北京的四合院现在已经和北京[胡同一起](../Page/胡同.md "wikilink")，成为北京传统[文化和民俗的代表](../Page/文化.md "wikilink")，成为北京城市的城市[名片之一](../Page/名片.md "wikilink")。

[北京城保持了](../Page/北京城.md "wikilink")[元大都时期的很多街道和建筑](../Page/元大都.md "wikilink")，其中就包括了四合院。因此可以说，四合院积淀了几百年来深厚的北京文化。从空中俯瞰北京城，可以看到一片灰瓦的房屋围着一个四方的院子。院子里绿葱葱的[树木给灰色的房屋做点缀](../Page/树木.md "wikilink")，也给四合院院里的人们提供了树荫。

## 特征

[Meilanfangguju.JPG](https://zh.wikipedia.org/wiki/File:Meilanfangguju.JPG "fig:Meilanfangguju.JPG")正房\]\]
与其他省份的四合院一样，北京四合院的基本结构也是四面住房包围中央的庭院，但在很多细节方面具有自己的特色。北京的胡同大多为东西走向，因而四合院多为南北向，这一特点在[北京内城尤为明显](../Page/北京内城.md "wikilink")。四合院大门通常开在四合院的东南方向，而不与正房相对。\[1\]
如果四合院位于胡同（胡同通常东西向）南侧，院南边不临街，开不了院门，则在北房西边开门。

北京四合院中央的院落非常宽敞，且比较方正，这一点与[山西四合院有着显著的区别](../Page/山西四合院.md "wikilink")。包围庭院的四组建筑相互独立，仅仅有[廊联系](../Page/廊.md "wikilink")，这也是北京四合院的特点。\[2\]
此外，[门楼](../Page/门楼.md "wikilink")、[影壁](../Page/影壁.md "wikilink")、[门墩等建筑细部也都有浓郁的北京特色](../Page/门墩.md "wikilink")。四合院大门有[广亮大门](../Page/广亮大门.md "wikilink")、[金柱大门](../Page/金柱大门.md "wikilink")、[蛮子门](../Page/蛮子门.md "wikilink")、[如意门等](../Page/如意门.md "wikilink")，房屋多用[清水脊](../Page/清水脊.md "wikilink")。

## 历史

北京四合院建筑规格早在[辽代时就已经初步形成](../Page/辽代.md "wikilink")。[元朝当时世祖](../Page/元朝.md "wikilink")[忽必烈](../Page/忽必烈.md "wikilink")“诏旧城居民之过京城老，以赀高（有钱人）及居职（在朝廷供职）者为先，乃定制以地八亩为一分”，分给前往[大都的富商](../Page/元大都.md "wikilink")、官员建造住宅，由此开始了北京传统四合院住宅大规模形成时期。\[3\]
1970年代初，北京[后英房胡同出土的](../Page/后英房胡同.md "wikilink")[元代四合院遗址](../Page/元代.md "wikilink")，可视为北京四合院的雏形。\[4\]
后经明、清历代完善，逐渐形成北京特有的建筑风格。现在，四合院和胡同一起被认为是北京市井文化的象征。

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，特别是[文化大革命以后](../Page/文化大革命.md "wikilink")，随着四合院原主人被打倒和抄家，以及那个时代的住房压力，四合院被强占，或重新分配给多户人家。由于不适应多户人家，因此各家在院子里盖起了[厨房](../Page/厨房.md "wikilink")、储物间等建筑，四合院也沦落成了[大杂院](../Page/大杂院.md "wikilink")。

除了一些成为[大杂院的四合院外](../Page/大杂院.md "wikilink")，一些保留完好的四合院通常成为[公司和组织单位办公的场所](../Page/公司.md "wikilink")，失去了居住的意义。有的甚至在四合院里办起了[宾馆](../Page/宾馆.md "wikilink")，如[史家胡同的好园宾馆](../Page/史家胡同.md "wikilink")，[后圆恩寺的友好宾馆等等](../Page/后圆恩寺.md "wikilink")。

由于四合院不再属于一户人家，以及维修不善，造成了北京老城区出现了很多危房。
除了危房的问题，还有老城区基础设施的陈旧，以及人口增长带来的住房压力的问题，北京市开始了[危旧房改造](../Page/危旧房改造.md "wikilink")（简称危改）的工程，政府把危改和房改结合在一起，引入了房地产商进行商业开发。

[File:Siheyuanxianzhuang1.JPG|四合院里搭满了小屋](File:Siheyuanxianzhuang1.JPG%7C四合院里搭满了小屋)
[File:Siheyuanxianzhuang2.JPG|破旧失修的四合院](File:Siheyuanxianzhuang2.JPG%7C破旧失修的四合院)
[File:Siheyuanxianzhuang4.JPG|依稀可见的](File:Siheyuanxianzhuang4.JPG%7C依稀可见的)[垂花门早已失去了当年的光彩](../Page/垂花门.md "wikilink")
[File:Siheyuanxianzhuang5.JPG|大门内的](File:Siheyuanxianzhuang5.JPG%7C大门内的)[影壁已经被小屋遮盖住](../Page/影壁.md "wikilink")
[File:Siheyuanxianzhuang6.JPG|大门内安装的](File:Siheyuanxianzhuang6.JPG%7C大门内安装的)[电表](../Page/电表.md "wikilink")
<File:Siheyuanxianzhuang7.JPG>|[广亮大门外又安装了铁](../Page/广亮大门.md "wikilink")[栅栏](../Page/栅栏.md "wikilink")

除此以外，加密路网、拓宽道路、增建绿地等工程，都在挤压着四合院的生存空间。从此，四合院开始成片成片地被拆毁，如1998年拆除[康有为的](../Page/康有为.md "wikilink")[粤东新馆](../Page/粤东新馆.md "wikilink")，2000年拆除[赵紫宸故居](../Page/赵紫宸.md "wikilink")，2004年拆除[孟端胡同](../Page/孟端胡同.md "wikilink")45号的清代[果郡王府](../Page/果郡王府.md "wikilink")，2005年拆除[曹雪芹故居](../Page/曹雪芹.md "wikilink")，2006年拆除[唐绍仪故居](../Page/唐绍仪.md "wikilink")。\[5\]
与此同时，一些四合院被列入北京市和各区县级的保护院落，但即使保存下来的四合院，也在高楼大厦四面的围合之中，成为现代化城市中的孤岛。

随着老城区保护的开展，一些原有四合院进行了改造，例如1990年由[清华大学](../Page/清华大学.md "wikilink")[吴良镛教授主持的对北京](../Page/吴良镛.md "wikilink")[菊儿胡同四合院的危改项目](../Page/菊儿胡同.md "wikilink")，在保留院落结构的基础上，将原有四合院的平房改为楼房，增加了厨房、[卫生间等设施](../Page/卫生间.md "wikilink")。\[6\]该改造工程获得了[联合国的](../Page/联合国.md "wikilink")[世界人居奖](../Page/世界人居奖.md "wikilink")。\[7\]
北京[南池子危改中](../Page/南池子.md "wikilink")，也将部分四合院房屋改成两层，并修建了地下车库。\[8\]2006年，北京市公布了《[北京四合院建筑要素图](../Page/北京四合院建筑要素图.md "wikilink")》，作为对四合院保护、修缮、翻建、改建的参考依据。\[9\]

## 著名四合院

游览北京的胡同和四合院已经成为北京旅游的一个独具特色的内容之一。但是绝大多数四合院都是宿舍和单位，谢绝参观，一些开放的名人故居成为了解四合院的主要途径。

  - [茅盾故居](../Page/茅盾故居.md "wikilink")，位于交道口后圆恩寺胡同13号。
  - [鲁迅故居](../Page/北京鲁迅故居.md "wikilink")，位于[阜成门内西三条](../Page/阜成门.md "wikilink")21号鲁迅纪念馆院内。
  - [老舍故居](../Page/北京老舍故居.md "wikilink")，位于东城区[灯市口西街丰富胡同](../Page/灯市口西街.md "wikilink")19号。
  - [郭沫若故居](../Page/郭沫若故居.md "wikilink")，位于[什刹海前海西街](../Page/什刹海.md "wikilink")。
  - [梅兰芳故居](../Page/北京梅兰芳故居.md "wikilink")，位于[西城区](../Page/西城区.md "wikilink")[护国寺街九号](../Page/护国寺.md "wikilink")

另外，还有一批四合院被列为[文物保护单位](../Page/文物保护单位.md "wikilink")：

  - [西城区](../Page/西城区.md "wikilink")[西交民巷87号四合院](../Page/西交民巷87号四合院.md "wikilink")
  - [北新华街112号四合院](../Page/北新华街112号四合院.md "wikilink")
  - 西城区[西四北大街六条23号四合院](../Page/西四北大街六条23号四合院.md "wikilink")
  - 西城区[西四北三条11号四合院](../Page/西四北三条11号四合院.md "wikilink")
  - 西城区[西四北三条19号四合院](../Page/西四北三条19号四合院.md "wikilink")
  - 西城区[前公用胡同15号四合院](../Page/前公用胡同15号四合院.md "wikilink")
  - [东城区](../Page/东城区_\(北京市\).md "wikilink")[东四六条63—65号四合院](../Page/东四六条63—65号四合院.md "wikilink")
  - 东城区[礼士胡同129号四合院](../Page/礼士胡同129号四合院.md "wikilink")
  - 东城区[内务部街11号四合院](../Page/内务部街11号四合院.md "wikilink")
  - 东城区[后圆恩寺胡同7号四合院](../Page/后圆恩寺胡同7号四合院.md "wikilink")
  - 东城区[国祥胡同2号四合院](../Page/国祥胡同2号四合院.md "wikilink")
  - 东城区[方家胡同13、15号四合院](../Page/方家胡同13、15号四合院.md "wikilink")
  - 东城区[府学胡同36号四合院](../Page/府学胡同36号四合院.md "wikilink")（包括交道口南大街小136号）
  - 东城区[新开路24号四合院](../Page/新开路24号四合院.md "wikilink")
  - [西堂子胡同25-37号四合院](../Page/西堂子胡同25-37号四合院.md "wikilink")
  - [帽儿胡同5号四合院](../Page/帽儿胡同5号四合院.md "wikilink")
  - [帽儿胡同11号四合院](../Page/帽儿胡同11号四合院.md "wikilink")
  - [美术馆东街25号四合院](../Page/美术馆东街25号四合院.md "wikilink")
  - [东棉花胡同15号院及拱门砖雕](../Page/东棉花胡同15号院及拱门砖雕.md "wikilink")
  - [前鼓楼苑胡同7、9号四合院](../Page/前鼓楼苑胡同7、9号四合院.md "wikilink")
  - [鼓楼东大街255号四合院](../Page/鼓楼东大街255号四合院.md "wikilink")
  - [陈独秀旧居](../Page/陈独秀旧居.md "wikilink")
  - [黑芝麻胡同13号四合院](../Page/黑芝麻胡同13号四合院.md "wikilink")
  - [前永康胡同7号四合院](../Page/前永康胡同7号四合院.md "wikilink")
  - [沙井胡同15号四合院等等](../Page/沙井胡同15号四合院.md "wikilink")。

## 注释

<div class="references-small">

<references />

</div>

## 外部链接

  - [北京四合院](http://www.china-culture.com.cn/ww/gs/1.htm)

[北京四合院](../Category/北京四合院.md "wikilink")

1.  [北京四合院的特点](http://house.qianlong.com/31278/2006/06/19/2530@3248375.htm)
    ，[千龙网](../Page/千龙网.md "wikilink")2006年6月19日。
2.  [老北京四合院的历史文化](http://xiuxian.ynet.com/article.jsp?oid=6913355&pageno=2)
    ，[北青网](../Page/北青网.md "wikilink")2005年11月1日。
3.  [老北京四合院的历史文化](http://xiuxian.ynet.com/article.jsp?oid=6913355)
    ，北青网，2005年11月1日。
4.  高巍 等，《四合院》，[学苑出版社](../Page/学苑出版社.md "wikilink")2007年第一版，第3-4页。ISBN
    7-5077-1827-1
5.  [北京胡同迅速消亡：梅兰芳故居被大楼埋葬](http://history.163.com/06/0607/13/2J15AHS500011247.html)
    ，[网易](../Page/网易.md "wikilink")2006年6月7日。
6.  孔繁峙，[危房改造方式与传统四合院保护](http://www.bjww.gov.cn/2004/12-7/3717.shtml)，北京文博2004年12月7日。
7.  [伤心四合院——拆掉“第二座城墙”](http://www.jianzhuw.com/article/jzarticle/jianzhulishi/20070722/1185113294247.html)
    ，[中国建筑网](../Page/中国建筑网.md "wikilink")2007年7月22日。
8.  [北京：四合院地下有车库](http://news.xinhuanet.com/house/2003-08/18/content_1030858.htm)，[新华网](../Page/新华网.md "wikilink")2003年8月18日，文章来源于《[北京青年报](../Page/北京青年报.md "wikilink")》。
9.  [《北京四合院建筑要素图》正式对外公布](http://www.bj.xinhuanet.com/bjpd_bjzq/2006-06/25/content_7345599.htm)
    ，[新华网](../Page/新华网.md "wikilink")2006年6月25日。