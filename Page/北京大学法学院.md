**北京大学法学院**（Peking University Law School、PKU
LAW、PKULS），原為**北京大学法律系**，前身是[京師大學堂於](../Page/京師大學堂.md "wikilink")1904年成立的[法律學科](../Page/法律.md "wikilink")，是[中国历史最悠久的正規法律教育機構](../Page/中国.md "wikilink")（其後為1909年創立的[清華學堂法律學科](../Page/清華學堂.md "wikilink")、1912年創立的[朝陽大學法律系及](../Page/朝陽大學.md "wikilink")1915年創立的[東吳大學法學院](../Page/東吳大學.md "wikilink")）。早年由於社會條件及國策方向所限，1948年中國的205所大學中僅有29所設有法學教育單位，學生人數最多的依次為[朝陽大學](../Page/朝陽大學.md "wikilink")、[東吳大學](../Page/東吳大學.md "wikilink")、[北京大學](../Page/北京大學.md "wikilink")、[清華大學](../Page/清華大學.md "wikilink")、[武漢大學](../Page/武漢大學.md "wikilink")、[中山大學等](../Page/中山大學.md "wikilink")，位於北京的[北京大學與](../Page/北京大學.md "wikilink")[朝陽大學法律系](../Page/朝陽大學.md "wikilink")、[清華學校](../Page/清華學校.md "wikilink")（前）法律學科同為京派法學的重要代表。北京大學法學院實力雄厚，在法學界聲名斐然。

## 歷史沿革

[北京大學前身為](../Page/北京大學.md "wikilink")[京師大學堂](../Page/京師大學堂.md "wikilink")，自創校起已設有[法律學科](../Page/法律.md "wikilink")，其時為「高等政治學」中的一個科目，1902年清政府《[欽定京師大學堂章程](../Page/欽定京師大學堂章程.md "wikilink")》規定，在大學專門分科[政治科內設](../Page/政治.md "wikilink")[法律學目](../Page/法律.md "wikilink")，正式規範了[法律學在中國](../Page/法律.md "wikilink")[高等教育中的地位](../Page/高等教育.md "wikilink")。1904年1月修訂大學堂章程，改大學專門分科為分科大學堂，在政法科大學堂內設[法律學門](../Page/法律.md "wikilink")，其時[法律學門的學制為四年](../Page/法律.md "wikilink")。

1911年[蔡元培改革中國](../Page/蔡元培.md "wikilink")[教育制度](../Page/教育制度.md "wikilink")，[京師大學堂改為](../Page/京師大學堂.md "wikilink")[北京大學](../Page/北京大學.md "wikilink")，改大學堂總監督為大學[校長](../Page/校長.md "wikilink")，由[嚴復擔任](../Page/嚴復.md "wikilink")。1913年2月，改政法科為法科，以[孫祥齡為學長](../Page/孫祥齡.md "wikilink")。1917年1月[蔡元培出任](../Page/蔡元培.md "wikilink")[北京大學](../Page/北京大學.md "wikilink")[校長](../Page/校長.md "wikilink")，在全面改革[北京大學舊制的同時](../Page/北京大學.md "wikilink")，對法科也進行了一系列改革，在[法律學門設立了](../Page/法律.md "wikilink")[法律學門研究所](../Page/法律.md "wikilink")，聘有學者專職研究各個法學領域。1919年，法科[法律學門改為](../Page/法律.md "wikilink")[法律學系](../Page/法律.md "wikilink")，同時允許[法律學系學生兼修其他學系課程](../Page/法律.md "wikilink")，也容許其他學生兼修[法律學系課程](../Page/法律.md "wikilink")，引起了法學思潮的激蕩，[北京大學法律學系成為了中國法學研究和討論的中心](../Page/北京大學.md "wikilink")。

1927年，[張作霖取消](../Page/張作霖.md "wikilink")[北京大學](../Page/北京大學.md "wikilink")，將北京九間高等院校合組為京師大學校，[北京大學](../Page/北京大學.md "wikilink")[法律學系與](../Page/法律.md "wikilink")[北京政法大學合併](../Page/北京政法大學.md "wikilink")，並被名為法科第二院，1928年6月[南京](../Page/南京.md "wikilink")[國民政府將京師大學校改為](../Page/國民政府.md "wikilink")[中華大學](../Page/中華大學.md "wikilink")，8月又將之改為[北平大學](../Page/北平大學.md "wikilink")。在原[北京大學師生的強烈抗爭下](../Page/北京大學.md "wikilink")，南京政府恢復國立[北京大學的名稱](../Page/北京大學.md "wikilink")，其[法律學系亦重新成為](../Page/法律.md "wikilink")[北京大學](../Page/北京大學.md "wikilink")[法律學系](../Page/法律.md "wikilink")，[北京政法大學則成為](../Page/北京政法大學.md "wikilink")[北平大學法學院](../Page/北平大學.md "wikilink")。

1930年12月，[蔣夢麟出任](../Page/蔣夢麟.md "wikilink")[北京大學](../Page/北京大學.md "wikilink")[校長](../Page/校長.md "wikilink")，仿效美國改革[教育制度](../Page/教育制度.md "wikilink")，[法律學系改為法學院](../Page/法律.md "wikilink")，下設[政治系](../Page/政治.md "wikilink")、[經濟系和](../Page/經濟.md "wikilink")[法律系](../Page/法律.md "wikilink")，學生畢業後可得[學士](../Page/學士.md "wikilink")[學位](../Page/學位.md "wikilink")。1934年，[北京大學法學院法科研究所成立](../Page/北京大學.md "wikilink")，開始法學研究生的培育。

1937年[平津淪陷](../Page/平津淪陷.md "wikilink")。[北京大學](../Page/北京大學.md "wikilink")、[清華大學](../Page/清華大學.md "wikilink")、[南開大學奉命南遷](../Page/南開大學.md "wikilink")，於長沙組成[長沙臨時大學](../Page/長沙臨時大學.md "wikilink")。1938年春，[長沙臨時大學遷往昆明](../Page/長沙臨時大學.md "wikilink")，改校名為國立[西南聯合大學](../Page/西南聯合大學.md "wikilink")。在此期間，原[北京大學](../Page/北京大學.md "wikilink")[法律學系的部分師生奔赴抗日前線](../Page/法律.md "wikilink")，部分師生則編入[西南聯合大學法商學院](../Page/西南聯合大學.md "wikilink")。法商學院由[法律學系](../Page/法律.md "wikilink")、[政治學系](../Page/政治.md "wikilink")、[經濟學系](../Page/經濟.md "wikilink")、[社會學系](../Page/社會.md "wikilink")、商學系組成。由於組成[西南聯合大學的三所學校中](../Page/西南聯合大學.md "wikilink")，只有[北京大學有](../Page/北京大學.md "wikilink")[法律學系](../Page/法律.md "wikilink")，因此西南聯大法商學院的[法律學系即為原](../Page/法律.md "wikilink")[北京大學](../Page/北京大學.md "wikilink")[法律學系](../Page/法律.md "wikilink")。直至[八年抗戰終結](../Page/八年抗戰.md "wikilink")，[西南聯合大學解散](../Page/西南聯合大學.md "wikilink")，[北京大學](../Page/北京大學.md "wikilink")、[清華大學](../Page/清華大學.md "wikilink")、[南開大學各自復校](../Page/南開大學.md "wikilink")，[北京大學法學院亦重新復員](../Page/北京大學.md "wikilink")。

1949年[中華人民共和國成立](../Page/中華人民共和國.md "wikilink")。

1952年，中國實行[院系調整](../Page/院系調整.md "wikilink")，[北京大學](../Page/北京大學.md "wikilink")[法律學系被併入](../Page/法律.md "wikilink")[北京政法學院](../Page/北京政法學院.md "wikilink")。1954年，在政務院副總理、中央政法委員會主任[董必武的直接指示下](../Page/董必武.md "wikilink")，[北京大學](../Page/北京大學.md "wikilink")[法律學系得以重建恢復](../Page/法律.md "wikilink")。[司法部教育司司長](../Page/司法部.md "wikilink")[陳守一則出任](../Page/陳守一.md "wikilink")[法律學系重建後的第一屆系主任](../Page/法律.md "wikilink")。在1954年至1966年，[北京大學](../Page/北京大學.md "wikilink")[法律學系作出了多番教學計劃](../Page/法律.md "wikilink")，其中較為突出的，分別是蘇聯法學的課程比重增加和學制由四年改為五年。

在1966年，中國發生了[文化大革命](../Page/文化大革命.md "wikilink")，[法律學系在](../Page/法律.md "wikilink")1970年被宣佈取消，但由於法學院師生團結爭取，[法律學系最終得以保留](../Page/法律.md "wikilink")，和[吉林大學法學院成為全國僅存的兩個政法院系之一](../Page/吉林大學.md "wikilink")。教職員基本沒有分散，主要圖書資料沒有流失，法學研究的工作也沒有中斷過。1966年至1971年6年間[法律學系沒有招生](../Page/法律.md "wikilink")，因此也沒有正規教學。1972年，在一批教師在剛剛得到平反之後，即致力恢復正常的教學工作。1973年[中國人民大學](../Page/中國人民大學.md "wikilink")[法律學系的三十六名教職員被合併到](../Page/法律.md "wikilink")[北京大學](../Page/北京大學.md "wikilink")[法律學系](../Page/法律.md "wikilink")，1978年[中國人民大學](../Page/中國人民大學.md "wikilink")[法律系復系時](../Page/法律.md "wikilink")，他們中絕大多數又回到[中國人民大學](../Page/中國人民大學.md "wikilink")。

1977年，中國[高等院校的招生制度恢復正常](../Page/高等院校.md "wikilink")，[北京大學及其](../Page/北京大學.md "wikilink")[法律學系亦得以正常發展](../Page/法律.md "wikilink")。

[北京大學法學院于](../Page/北京大學.md "wikilink")1999年6月26日正式成立\[1\]。作为歷史悠久、桃李滿天下的学院，畢業生多有從事[律師](../Page/律師.md "wikilink")、司法工作者，亦有在海內外著名法學院任教者。

至今，[北京大學法學院共設有多個法學研究機構和多部法學研究的學術期刊](../Page/北京大學.md "wikilink")，由於其學術成果和畢業生們的傑出成就，[北京大學法學院多年來皆獲社會及學術界公認為中國最好的法學院之一](../Page/北京大學.md "wikilink")。

## 重点学科

北大法学院有[法理学](../Page/法理学.md "wikilink")、[宪法与](../Page/宪法.md "wikilink")[行政法](../Page/行政法.md "wikilink")、[经济法](../Page/经济法.md "wikilink")、[刑法四个](../Page/刑法.md "wikilink")[国家重点学科](../Page/国家重点学科.md "wikilink")\[2\]。此外，北大法学院在国际法学、民商法学、诉讼法学、法律史、环境法等学科也有很强的实力。

北大法学院有经济法研究所、国际法研究所、国际经济法研究所、劳动法与社会保障法研究所、刑事法理论研究所、世界贸易组织法律研究中心、公法研究中心、税法研究中心、金融法研究中心、人权研究中心、法制信息中心等30多个研究机构。

北大法学院所属的法律图书馆馆藏量和自动化水平在综合大学法律院系中处于领先地位。

《[中外法学](../Page/中外法学.md "wikilink")》是北大法学院主办的[法学期刊](../Page/法学期刊.md "wikilink")，创刊于1989年。

## 學歷課程

北京大學法學院提供以下課程：

| 學位                                                                | 英文名稱                                             | 英文縮寫   | 學制         | 簡述                                                          |
| ----------------------------------------------------------------- | ------------------------------------------------ | ------ | ---------- | ----------------------------------------------------------- |
| [法學士](../Page/法學士.md "wikilink")                                  | Bachelor of Laws                                 | LL.B.  | 四年全日制      | 法學本科課程，學生須修畢指定必修科、一定數量的選修科和完成畢業論文。                          |
| [知識產權法](../Page/知識產權.md "wikilink")[學士](../Page/學士.md "wikilink") | Bachelor of Science in Intellectual Property Law | \-     | (本科在讀兼修兩年) | 專修知識產權的雙學位課程。                                               |
| [法律碩士](../Page/法律碩士.md "wikilink")                                | Juris Master                                     | J.M.   | 三年全日制      | 供非法學本科畢業的大學畢業生所修讀的法律專業學位，學生須修畢指定必修科、一定數量的選修科和完成畢業論文。        |
| 法學[碩士](../Page/碩士.md "wikilink")                                  | Master of Laws                                   | LL.M.  | 三年全日制      | 法學研究生課程，學生須按研究方向修畢指定學科，並完成研究論文。                             |
| 中國法法學[碩士](../Page/碩士.md "wikilink")                               | Master of Laws in Chinese Law                    | LL.M.  | 兩年全日制      | 主修中國法的法學研究生課程，以英語授課，只錄取沒有主修中國法學位的法學畢業生。學生須按規定修畢指定學科，並完成實習。  |
| [人權學](../Page/人權.md "wikilink")[碩士](../Page/碩士.md "wikilink")     | Master of Human Rights                           | M.H.R. | (在讀兼修)     | 專修人權的雙碩士學位課程，與瑞典隆德大學羅爾．瓦倫堡人權與人道法研究所合辦。                      |
| 法學[博士](../Page/博士.md "wikilink")                                  | Doctor of Philosophy in Law                      | Ph.D.  | 四年全日制      | 法學研究生課程，學生須按研究方向修畢指定學科，並完成研究論文。如學生屬於「碩博連讀」生，博士階段的修業期可縮減至三年。 |

## 部份著名學者及校友

（註：以下之學歷僅羅列北京大學法學院所頒授之學歷。）

  - **著名學者**

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>簡述</p></th>
<th><p>北大法學院淵源</p></th>
<th><p>其他</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>國際法、人權法學家。</p></td>
<td><p>教授，法學碩士、博士畢業</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/白建軍.md" title="wikilink">白建軍</a></p></td>
<td><p>犯罪學家。</p></td>
<td><p>教授，法學碩士畢業</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蔡定劍.md" title="wikilink">蔡定劍</a></p></td>
<td><p>憲政學家。</p></td>
<td><p>法學博士畢業</p></td>
<td><p>已故</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳寶音.md" title="wikilink">陳寶音</a></p></td>
<td><p>憲法學家。</p></td>
<td><p>教授</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳瑞華.md" title="wikilink">陳瑞華</a></p></td>
<td><p>刑法學家。</p></td>
<td><p>教授，法學博士畢業</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳守一.md" title="wikilink">陳守一</a></p></td>
<td><p>曾任國家司法部教育司司長，對中國的法學教育貢獻良多。</p></td>
<td><p>教授、前法律學系系主任</p></td>
<td><p>已故</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳忠林.md" title="wikilink">陳忠林</a></p></td>
<td><p>重慶大學法學院院長，西南政法大學法學院前院長</p></td>
<td><p>1982年法學士</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳興良.md" title="wikilink">陳興良</a></p></td>
<td><p>刑法學家，美國法律社會學協會「國際學術獎」得獎者。</p></td>
<td><p>教授</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馮象.md" title="wikilink">馮象</a></p></td>
<td><p>知識產權學家，<a href="../Page/清華大學.md" title="wikilink">清華大學法學院講座教授</a>、<a href="../Page/香港大學.md" title="wikilink">香港大學法學院前副院長</a></p></td>
<td><p>兼職教授</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/付子堂.md" title="wikilink">付子堂</a></p></td>
<td><p><a href="../Page/西南政法大学.md" title="wikilink">西南政法大学副校长</a>。</p></td>
<td><p>法学博士</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/甘雨沛.md" title="wikilink">甘雨沛</a></p></td>
<td><p>中國刑法學科的創始人，東北大學（<a href="../Page/吉林大學.md" title="wikilink">吉林大學前身</a>）法律系創系系主任。</p></td>
<td><p>1932年本科畢業、64年任教授</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/龔刃韌.md" title="wikilink">龔刃韌</a></p></td>
<td><p>國際法、人權法學家。</p></td>
<td><p>教授、<a href="../Page/北京大學法學院人權及人道法研究中心.md" title="wikilink">人權及人道法研究中心主任</a>，法學士畢業</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/龔祥瑞.md" title="wikilink">龔祥瑞</a></p></td>
<td><p>憲法、行政法、比較法學家。</p></td>
<td><p>教授、</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鞏獻田.md" title="wikilink">鞏獻田</a></p></td>
<td><p>曾公開指《物權法》草案違憲引發廣泛爭論。</p></td>
<td><p>教授，法學碩士畢業</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/顾功耘.md" title="wikilink">顾功耘</a></p></td>
<td><p><a href="../Page/華東政法大學.md" title="wikilink">華東政法大學副校长</a>。</p></td>
<td><p>77级法律系</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭明瑞.md" title="wikilink">郭明瑞</a></p></td>
<td><p><a href="../Page/烟台大学.md" title="wikilink">烟台大学校长</a>、山东省法学会副会长。</p></td>
<td><p>77级法律系</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/海子.md" title="wikilink">海子</a></p></td>
<td><p>詩人，原名查海生，前<a href="../Page/中國政法大學.md" title="wikilink">中國政法大學講師</a>，其自殺事件曾引起社會關注。</p></td>
<td><p>法學士畢業</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/何勤華.md" title="wikilink">何勤華</a></p></td>
<td><p><a href="../Page/華東政法大學.md" title="wikilink">華東政法大學校長</a>。</p></td>
<td><p>法學士</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/賀衛方.md" title="wikilink">賀衛方</a></p></td>
<td><p>在中國社會上矚目的著名法學家、批評家，曾停招碩士生引起爭議。</p></td>
<td><p>教授</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/姜明安.md" title="wikilink">姜明安</a></p></td>
<td><p>行政法學家</p></td>
<td><p>教授、法學士畢業</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/強世功.md" title="wikilink">強世功</a></p></td>
<td><p>法理學家</p></td>
<td><p>教授、法學碩士、法學博士畢業</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/劉劍文.md" title="wikilink">劉劍文</a></p></td>
<td><p>前<a href="../Page/武漢大學.md" title="wikilink">武漢大學法學院副院長</a>，稅法學家。</p></td>
<td><p>教授</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李浩培.md" title="wikilink">李浩培</a></p></td>
<td><p>國際法學家，海牙戰爭罪行法庭創建成員，曾任<a href="../Page/聯合國.md" title="wikilink">聯合國</a><a href="../Page/國際刑事法庭.md" title="wikilink">國際刑事法庭首席法官</a>，退休前被一名任職護士、有精神病的連續殺人犯毒殺。</p></td>
<td><p>教授</p></td>
<td><p>已故</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李力.md" title="wikilink">李力</a></p></td>
<td><p><a href="../Page/中国青年政治学院.md" title="wikilink">中国青年政治学院法律系主任</a>。</p></td>
<td><p>85年法学士、88年法学硕士</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/凌兵.md" title="wikilink">凌兵</a></p></td>
<td><p><a href="../Page/香港中文大學.md" title="wikilink">香港中文大學</a><a href="../Page/香港中文大學法律學院.md" title="wikilink">法律學院教授</a>、副院長。</p></td>
<td><p>法學士畢業</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅豪才.md" title="wikilink">羅豪才</a></p></td>
<td><p>曾任北京大學副校長、法律學系系主任、最高人民法院副院長，現時是全國<a href="../Page/人民政治協商會議.md" title="wikilink">人民政治協商會議</a>（<a href="../Page/政協.md" title="wikilink">政協</a>）常務委員會委員。</p></td>
<td><p>退休教授，法學士畢業</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/梅汝璈.md" title="wikilink">梅汝璈</a></p></td>
<td><p><a href="../Page/聯合國.md" title="wikilink">聯合國</a><a href="../Page/遠東國際軍事法庭.md" title="wikilink">遠東國際軍事法庭法官</a>。</p></td>
<td><p><a href="../Page/西南聯合大學.md" title="wikilink">西南聯合大學法學教授</a></p></td>
<td><p>已故</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/錢端升.md" title="wikilink">錢端升</a></p></td>
<td><p>比較憲法學家、政治學家、教育家、外交家，<a href="../Page/中國政法大學.md" title="wikilink">北京政法學院創校校長</a>。</p></td>
<td><p>教授</p></td>
<td><p>已故</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/瞿同祖.md" title="wikilink">瞿同祖</a></p></td>
<td><p>著名中國法律史學者。</p></td>
<td><p>教授</p></td>
<td><p>已故</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/芮沐.md" title="wikilink">芮沐</a></p></td>
<td><p>首位在中國開設經濟法學和國際經濟法學兩個專業範疇的學者。</p></td>
<td><p>退休教授</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/盛杰民.md" title="wikilink">盛杰民</a></p></td>
<td><p>著名競爭法、反壟斷法學家，曾揭發國外企業在中國的壟斷情況。</p></td>
<td><p>退休教授，法學士畢業</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沈四宝.md" title="wikilink">沈四宝</a></p></td>
<td><p><a href="../Page/对外经济贸易大学.md" title="wikilink">对外经济贸易大学法学院院长</a>。</p></td>
<td><p>70年法学士、79年法学硕士</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/沈宗靈.md" title="wikilink">沈宗靈</a></p></td>
<td><p>著名法理學家。</p></td>
<td><p>退休教授</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/田飛龍.md" title="wikilink">田飛龍</a></p></td>
<td><p>憲法學家。</p></td>
<td><p>2008年法學碩士、2012年法學博士</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王晨光.md" title="wikilink">王晨光</a></p></td>
<td><p>前<a href="../Page/清华大学.md" title="wikilink">清华大学法学院院长</a>。</p></td>
<td><p>1983年法学硕士</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王寵惠.md" title="wikilink">王寵惠</a></p></td>
<td><p>民國時期曾任外交總長、<a href="../Page/大理院.md" title="wikilink">大理院院長</a>、教育總長、代總理、司法部長、<a href="../Page/海牙.md" title="wikilink">海牙</a><a href="../Page/國際法院.md" title="wikilink">國際法院法官</a>、<a href="../Page/復旦大學.md" title="wikilink">復旦大學副校長</a>。在維護中國主權、外交、制憲、制定政府架構、創建<a href="../Page/復旦大學.md" title="wikilink">復旦大學</a>、制定<a href="../Page/聯合國.md" title="wikilink">聯合國憲章等歷史上幾乎都發揮了重要甚至關鍵之作用</a>。</p></td>
<td><p>1913年起任研究員、1918年起任兼任教授</p></td>
<td><p>已故</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王貴國.md" title="wikilink">王貴國</a></p></td>
<td><p><a href="../Page/香港城市大學.md" title="wikilink">香港城市大學</a><a href="../Page/香港城市大學法律學院.md" title="wikilink">法律學院講座教授</a>、前院長。</p></td>
<td><p>法學碩士畢業</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/汪勁.md" title="wikilink">汪勁</a></p></td>
<td><p>醫生、環境法學家，積極於環境保護活動。</p></td>
<td><p>教授，法學博士畢業</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王磊.md" title="wikilink">王磊</a></p></td>
<td><p>憲法學家、港澳基本法學家、中國憲法司法化學派的代表人物。</p></td>
<td><p>教授，法學士、法學碩士、法學博士畢業</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王世洲.md" title="wikilink">王世洲</a></p></td>
<td><p>刑法學家。</p></td>
<td><p>教授，法學士、法學碩士畢業</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王世杰_(中华民国政治家).md" title="wikilink">王世杰</a></p></td>
<td><p>憲法學家、教育家。曾任<a href="../Page/武漢大學.md" title="wikilink">武漢大學首任校長</a>、南京國民政府法制局局長、教育部長、外交部長、台灣[中央研究院]院長等要職。</p></td>
<td><p>教授</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王鐵崖.md" title="wikilink">王鐵崖</a></p></td>
<td><p>國際法學權威，曾任<a href="../Page/聯合國.md" title="wikilink">聯合國</a><a href="../Page/國際刑事法庭.md" title="wikilink">國際刑事法庭法官</a>。</p></td>
<td><p>教授</p></td>
<td><p>已故</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王小能.md" title="wikilink">王小能</a></p></td>
<td><p>民商法學家，曾經是社會上矚目的女性法律學者，現已出家。</p></td>
<td><p>前副教授，法學士、法學碩士畢業</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/武樹臣.md" title="wikilink">武樹臣</a></p></td>
<td><p>中國法律思想史學者，現為北京市第二中級人民法院副院長。</p></td>
<td><p>教授，法學士畢業</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吳志攀.md" title="wikilink">吳志攀</a></p></td>
<td><p>北京大學副校長。</p></td>
<td><p>教授、前院長，法學士、法學碩士、法學博士畢業</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蕭蔚雲.md" title="wikilink">蕭蔚雲</a></p></td>
<td><p><a href="../Page/香港.md" title="wikilink">香港</a>《<a href="../Page/基本法.md" title="wikilink">基本法</a>》起草委員，曾就<a href="../Page/釋法.md" title="wikilink">釋法事件在</a><a href="../Page/香港.md" title="wikilink">香港引起爭議</a>。</p></td>
<td><p>教授</p></td>
<td><p>已故</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/孫東東.md" title="wikilink">孫東東</a></p></td>
<td><p>因<a href="../Page/三鹿奶粉污染事件.md" title="wikilink">三鹿奶粉污染事件及</a><a href="../Page/上訪.md" title="wikilink">上訪事件失言而備受關注</a></p></td>
<td><p>北京大學司法鑒定中心主任，司法精神病学专家</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/魏振瀛.md" title="wikilink">魏振瀛</a></p></td>
<td><p>民法學家。</p></td>
<td><p>退休教授、前法律學系系主任，法學士畢業</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/倪慧芳.md" title="wikilink">倪慧芳</a></p></td>
<td><p><a href="../Page/云南大学.md" title="wikilink">云南大学副校长</a>。</p></td>
<td><p>1986年至1988年法理学研究生</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吴英荃.md" title="wikilink">吴英荃</a></p></td>
<td><p>1946年任国防部政工局少将处长</p></td>
<td><p>法律系毕业、教授、西南聯大時期擔任工商法律學院院長</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/薛捍勤.md" title="wikilink">薛捍勤</a></p></td>
<td><p><a href="../Page/聯合國.md" title="wikilink">聯合國</a><a href="../Page/國際法院.md" title="wikilink">國際法院法官</a>。</p></td>
<td><p>1981年至1982年國際法研究生</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/袁曙宏.md" title="wikilink">袁曙宏</a></p></td>
<td><p><a href="../Page/国家行政学院.md" title="wikilink">国家行政学院副院长</a>。</p></td>
<td><p>90级法学博士</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張千帆.md" title="wikilink">張千帆</a></p></td>
<td><p>北京大學法學院及<a href="../Page/北京大學政府管理學院.md" title="wikilink">政府管理學院雙聘教授</a>，兼任南京大學法學院教授。</p></td>
<td><p>教授</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張守文.md" title="wikilink">張守文</a></p></td>
<td><p>經濟法學家</p></td>
<td><p>教授、院長，法學士、法學碩士、法學博士畢業</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/趙理海.md" title="wikilink">趙理海</a></p></td>
<td><p>國際法學家，曾任<a href="../Page/聯合國.md" title="wikilink">聯合國</a><a href="../Page/國際海洋法庭.md" title="wikilink">國際海洋法庭法官</a>。</p></td>
<td><p>教授</p></td>
<td><p>已故</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/焦津洪.md" title="wikilink">焦津洪</a></p></td>
<td><p><a href="../Page/对外经济贸易大学.md" title="wikilink">对外经济贸易大学法学院副院长</a>。</p></td>
<td><p>80年法学士</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭成思.md" title="wikilink">鄭成思</a></p></td>
<td><p><a href="../Page/中國社會科學院.md" title="wikilink">中國社會科學院教授</a>，知識產權法學權威。</p></td>
<td><p>兼職教授</p></td>
<td><p>已故</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄭天錫.md" title="wikilink">鄭天錫</a></p></td>
<td><p>曾任北京大學、<a href="../Page/朝陽大學.md" title="wikilink">朝陽大學</a>、<a href="../Page/東吳大學.md" title="wikilink">東吳大學等校之法學院兼職教授及</a><a href="../Page/聯合國.md" title="wikilink">聯合國</a><a href="../Page/常設國際法庭.md" title="wikilink">常設國際法庭法官等職</a>。</p></td>
<td><p>兼職教授</p></td>
<td><p>已故</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曾东红.md" title="wikilink">曾东红</a></p></td>
<td><p><a href="../Page/中山大学.md" title="wikilink">中山大学法学院副院长</a>。</p></td>
<td><p>1988年法学硕士、1999年法学博士</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/周鯁生.md" title="wikilink">周鯁生</a></p></td>
<td><p>著名國際法學家、外交史家、教育家。</p></td>
<td><p>1922-1927年任教授</p></td>
<td><p>已故</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/周旺生.md" title="wikilink">周旺生</a></p></td>
<td><p>著名立法學家。</p></td>
<td><p>教授，法學碩士畢業</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/周振想.md" title="wikilink">周振想</a></p></td>
<td><p><a href="../Page/中国青年政治学院.md" title="wikilink">中国青年政治学院副院长</a>。</p></td>
<td><p>77级法律系</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/朱啟超.md" title="wikilink">朱啟超</a></p></td>
<td><p>民法學家。</p></td>
<td><p>退休教授，法學士畢業</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/朱蘇力.md" title="wikilink">朱蘇力</a></p></td>
<td><p>北京大學法學院前院長。</p></td>
<td><p>教授、前院長，法學士畢業</p></td>
<td></td>
</tr>
</tbody>
</table>

  - **著名校友**
      - [岑浩辉](../Page/岑浩辉.md "wikilink")　–　澳门特别行政区终审法院院长（法律系毕业）
      - [何超明](../Page/何超明.md "wikilink")　–　澳门特别行政区检察院检察长（经济法博士研究生）
      - [曹康泰](../Page/曹康泰.md "wikilink")　–　[国务院法制办主任](../Page/国务院.md "wikilink")（法律系83级经济法干部专修科毕业）
      - [奚晓明](../Page/奚晓明.md "wikilink")　–　最高人民法院副院长（法律系毕业）
      - [江必新](../Page/江必新.md "wikilink")　–　湖南省高级人民法院院长、原最高人民法院副院长（2004年行政法博士）
      - [黄尔梅](../Page/黄尔梅.md "wikilink")　–　最高人民法院审判委员会委员、立案庭庭长（法律系79级）
      - [马迎新](../Page/马迎新.md "wikilink")　–　最高人民法院立案庭副庭长（法律系77级）
      - [罗东川](../Page/罗东川.md "wikilink")　–　最高人民法院审判员、“全国十大人民满意好法官”（法律系82级）
      - [董天平](../Page/董天平.md "wikilink")　–　最高人民法院知识产权庭副庭长（法律系80级）
      - [俞灵雨](../Page/俞灵雨.md "wikilink")　–　最高人民法院审判委员会委员、执行办主任、原民四庭庭长（法律系86级）
      - [孙华璞](../Page/孙华璞.md "wikilink")　–　最高人民法院审判委员会委员、原民一庭庭长（政法系统在职攻读法律硕士）
      - [宋凯楚](../Page/宋凯楚.md "wikilink")　–　湖南省高级人民法院副院长（法律系77级）
      - [鲍圣庆](../Page/鲍圣庆.md "wikilink")　–　湖南省高级人民法院副院长（挂职）、最高人民法院办公厅副主任（法律系79届）
      - [石时态](../Page/石时态.md "wikilink")　–　湖南省检察院副院长（法律系毕业）
      - [傅长禄](../Page/傅长禄.md "wikilink")　–　上海市高级人民法院副院长（法律系77级）
      - [汪利民](../Page/汪利民.md "wikilink")　–　安徽省高级人民法院副院长（法律系80级）
      - [鲍国友](../Page/鲍国友.md "wikilink")　–　安徽省人民检察院副检察长（法律系78级）
      - [童兆洪](../Page/童兆洪.md "wikilink")　–　浙江省高级人民法院副院长（法律系80届）
      - [吕忠梅](../Page/吕忠梅.md "wikilink")　–　湖北省高级人民法院副院长（法律系80级）
      - [景汉朝](../Page/景汉朝.md "wikilink")　–　河北省高级人民法院党组副书记、常务副院长（法学院96届硕士）
      - [甄树清](../Page/甄树清.md "wikilink")　–　河北省高级人民法院副院长（法律系79级）
      - [康为民](../Page/康为民.md "wikilink")　–　江西省高级人民法院院长（中国高级法官培训中心北京大学经济法专业首届学员）
      - [周溯](../Page/周溯.md "wikilink")　　–　安徽省高级人民法院院长（中国高级法官培训中心北京大学经济法专业首届学员，法律系硕士）
      - [吴合振](../Page/吴合振.md "wikilink")　–　河南省高级人民法院副院长（中国高级法官培训中心北京大学经济法专业首届学员）
      - [张居胜](../Page/张居胜.md "wikilink")　–　福建省高级人民法院副院长（中国高级法官培训中心北京大学经济法专业首届学员）
      - [李琦](../Page/李琦.md "wikilink")　　–　广东省高级人民法院副院长（中国高级法官培训中心北京大学经济法专业首届学员）
      - [李毅峰](../Page/李毅峰.md "wikilink")　–　广东省高级人民法院副院长（中国高级法官培训中心北京大学经济法专业首届学员）
      - [孙纬](../Page/孙纬.md "wikilink")　　–　黑龙江省高级人民法院副院长（中国高级法官培训中心北京大学经济法专业首届学员）
      - [潘熙](../Page/潘熙.md "wikilink")　　–　[香港](../Page/香港.md "wikilink")[資深大律師](../Page/資深大律師.md "wikilink")，專攻公法及人權法（曾任客席講師、1998年法學士畢業）
      - [范鴻齡](../Page/范鴻齡.md "wikilink")　–　[香港](../Page/香港.md "wikilink")[大律師](../Page/大律師.md "wikilink")，[中信泰富董事總經理](../Page/中信泰富.md "wikilink")，[香港行政會議成員](../Page/香港行政會議.md "wikilink")（法學士畢業）
      - [开来](../Page/开来.md "wikilink")　　–　著名女律师（1985年法律系毕业）
      - [白仁杰](../Page/白仁杰.md "wikilink")　–　美国高特兄弟律师事务所合伙人
      - [陶景洲](../Page/陶景洲.md "wikilink")　–　美国高特兄弟律师事务所合伙人、北京办事处首席代表（1981年毕业，数年时间为中国打赢了数以亿万美元计的涉外经济官司而著名）
      - [陈大刚](../Page/陈大刚.md "wikilink")　–　中国证监会首席律师、法律部主任（1978级本科）
      - [白涛](../Page/白涛.md "wikilink")　　–　中国足协法律顾问（毕业于法律系）
      - [許志永](../Page/許志永.md "wikilink")　–　中國民間維權組織「公盟」創辦人之一。（2002年法學博士）
      - [邓焕香](../Page/邓焕香.md "wikilink")　–　1929年赴越南定居，参加了越法、越日、越美战争，曾任团长、师长、军长、南方海陆军三军副司令，官至中將，1975年為救援友軍犧牲（法律系毕业，已故）
      - [陈克华](../Page/陈克华.md "wikilink")　–　抗日战争時任国民党陆军160师少将师长，1946年任第九军官总队中将总副队长（法律系肄业）
      - [李润沂](../Page/李润沂.md "wikilink")　–　1949年任国民党军法处少将处长，后任台湾“立法院”大法官（法律系毕业，已故）
      - [许惠东](../Page/许惠东.md "wikilink")　–　民国時期官員，抗日战争時出任陆军少将（政法系毕业，已故）
      - [朱光沐](../Page/朱光沐.md "wikilink")　–　民国东北军军法处少将处长（法科毕业）

## 参考资料

## 外部链接

  - [北京大学法学院](http://www.law.pku.edu.cn/)

{{-}}

[Category:北京大學學院系所](../Category/北京大學學院系所.md "wikilink")
[Category:中國法學院](../Category/中國法學院.md "wikilink")

1.

2.