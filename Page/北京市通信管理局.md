**北京市通信管理局**是隶属于[北京市人民政府和](../Page/北京市人民政府.md "wikilink")[信息产业部的通信行业管理部门](../Page/中华人民共和国信息产业部.md "wikilink")。它负责对[北京市辖境内的通信行业特别是](../Page/北京市.md "wikilink")[电信市场的管理和监督](../Page/电信.md "wikilink")。

## 职能

北京市通信管理局的主要职能如下（管辖范围仅限于北京市行政区划内）：

  - 统筹管理本地[电信网规划](../Page/电信网.md "wikilink")
  - 分配及管理[电话号码资源](../Page/电话号码.md "wikilink")
  - 监督管理电信服务质量及价格
  - 审核电信运营商的业务范围，发放业务经营许可证
  - 审核发放电信设备入网许可证
  - 监督电信网建设的工程质量
  - 其他行政性职能：如组织职业技能鉴定和[工程师职称评审等](../Page/工程师.md "wikilink")

## 管辖范围

北京市电信管理局位于[北京市](../Page/北京.md "wikilink")[宣武区马连道路](../Page/宣武区.md "wikilink")4号（[邮编](../Page/邮政编码.md "wikilink")：100055），管理范围在行政区划上包括北京市及所属的[东城](../Page/东城区.md "wikilink")、[西城](../Page/西城区.md "wikilink")、[崇文](../Page/崇文区.md "wikilink")、[宣武](../Page/宣武区.md "wikilink")、[朝阳](../Page/朝阳区_\(北京市\).md "wikilink")、[海淀](../Page/海淀区.md "wikilink")、[丰台](../Page/丰台区.md "wikilink")、[石景山](../Page/石景山区.md "wikilink")、[房山](../Page/房山区.md "wikilink")、[通州](../Page/通州区.md "wikilink")、[顺义](../Page/顺义区.md "wikilink")、[昌平](../Page/昌平区.md "wikilink")、[大兴](../Page/大兴区.md "wikilink")、[怀柔](../Page/怀柔区.md "wikilink")、[平谷](../Page/平谷区.md "wikilink")、[门头沟](../Page/门头沟区.md "wikilink")、[密云及](../Page/密云县.md "wikilink")[延庆](../Page/延庆县.md "wikilink")。
在此范围内注册的与通信相关的产业均由该局监督管理。此外，如果电信设备物理位置位于北京而拥有该设备产权的公司不在北京市注册，设备仍然会受到北京市通信管理局管理。

主要的管理对象：

  - 电信网运营商：如[中国电信](../Page/中国电信.md "wikilink")、[中国联通](../Page/中国联通.md "wikilink")、[中国移动等公司在北京实际运营的分公司](../Page/中国移动.md "wikilink")
  - 电信设备生产厂商：如首信[诺基亚等公司](../Page/诺基亚.md "wikilink")
  - 互联网数据中心（IDC）：如万网等提供[因特网服务器托管存放的公司](../Page/因特网.md "wikilink")
  - 互联网内容提供商（ICP）：如[新浪网等提供因特网服务内容的公司或个人](../Page/新浪网.md "wikilink")

## 参考文献

## 外部链接

  - [北京市通信管理局官方主页](http://www.bca.gov.cn/)

[Category:中华人民共和国工业和信息化部](../Category/中华人民共和国工业和信息化部.md "wikilink")
[Category:北京市人民政府](../Category/北京市人民政府.md "wikilink")