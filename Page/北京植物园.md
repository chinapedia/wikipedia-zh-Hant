[Beijing_Botanical_Garden.JPG](https://zh.wikipedia.org/wiki/File:Beijing_Botanical_Garden.JPG "fig:Beijing_Botanical_Garden.JPG")
[Beijing_Botanical_Garden_-_Oct_09_-_IMG_1120.jpg](https://zh.wikipedia.org/wiki/File:Beijing_Botanical_Garden_-_Oct_09_-_IMG_1120.jpg "fig:Beijing_Botanical_Garden_-_Oct_09_-_IMG_1120.jpg")
[Beijing_Botanical_Garden_-_Oct_09_-_IMG_1161.jpg](https://zh.wikipedia.org/wiki/File:Beijing_Botanical_Garden_-_Oct_09_-_IMG_1161.jpg "fig:Beijing_Botanical_Garden_-_Oct_09_-_IMG_1161.jpg")
[BeijingBotanical2.jpg](https://zh.wikipedia.org/wiki/File:BeijingBotanical2.jpg "fig:BeijingBotanical2.jpg")

**北京植物园**位于[中國](../Page/中國.md "wikilink")[北京的西北郊](../Page/北京.md "wikilink")[香山公园和](../Page/香山公园.md "wikilink")[玉泉山之间](../Page/玉泉山.md "wikilink")，坐落在[寿安山南麓](../Page/寿安山.md "wikilink")，占地400[公顷](../Page/公顷.md "wikilink")。北京植物园建于1955年，栽培了萬余种（含品种）150余万株各类植物。

植物展览区分为观赏植物区、树木园和温室区三部分。观赏植物区由专类园组成，主要有月季园、桃花园、牡丹园、芍药园、丁香园、海棠栒子园、木兰园、集秀园（竹园）、宿根花卉园和梅园。月季园是我国目前规模最大的月季专类园，栽培了近1000个月季品种。桃花园是世界上收集桃花品种最多的专类园。树木园由银杏松柏区、槭树蔷薇区、椴树杨柳区、木兰小檗区、悬铃木麻栎区和泡桐白蜡区组成。热带植物展览温室是植物园的亮点，有四个展区，热带雨林展区，四季花园展区，沙漠植物展区，专类植物展区，分别展示了不同气候条件下典型的植物景观。

植物园栽培的植物包括许多稀有物种。比如[水杉](../Page/水杉.md "wikilink")1941年在[湖北](../Page/湖北.md "wikilink")、[四川第一次被中国科学家发现](../Page/四川.md "wikilink")。由于水杉当时被认为已经在[新生代](../Page/新生代.md "wikilink")[第三纪](../Page/第三纪.md "wikilink")（6,500百万年前）就[灭绝了](../Page/灭绝.md "wikilink")，其活标本在中国的发现震动了植物学界。

植物园中的其他植物包括能捕食昆虫的[猪笼草](../Page/猪笼草.md "wikilink")，[美国前总统](../Page/美国.md "wikilink")[尼克松赠送的](../Page/尼克松.md "wikilink")[美洲红杉](../Page/美洲红杉.md "wikilink")，[日本前首相](../Page/日本.md "wikilink")[田中角荣赠送的](../Page/田中角荣.md "wikilink")[樱花](../Page/樱花.md "wikilink")，[菲律宾前总统](../Page/菲律宾.md "wikilink")[马科斯及夫人赠送的](../Page/费迪南德·马科斯.md "wikilink")[金蝶兰](../Page/文心兰.md "wikilink")，[斯里兰卡前总理](../Page/斯里兰卡.md "wikilink")[西丽玛沃·班达拉奈克赠送的](../Page/西丽玛沃·班达拉奈克.md "wikilink")[菩提树](../Page/菩提树.md "wikilink")，佛祖曾在菩提树下成道。

北京植物园内还有[卧佛寺](../Page/十方普觉寺.md "wikilink")、樱桃沟、[曹雪芹纪念馆](../Page/曹雪芹.md "wikilink")、[一二九运动纪念亭](../Page/一二九运动.md "wikilink")、北纬40度地理标志雕塑、[梁启超墓](../Page/梁启超墓.md "wikilink")、[孙传芳墓](../Page/孙传芳.md "wikilink")、[王锡彤墓等景观](../Page/王锡彤.md "wikilink")。

## 花卉

Image:BeijingBotanical3.jpg| Image:Bitaoyuan at Beijing Botanical
Garden.jpg| Image:Beijingbotanical6.jpg| Image:Gazania_(beijing).jpg|
Image:Beijingbotanical8.jpg| Image:Beijingbotanical7.jpg|
Image:Beijingbotanical9.jpg| Image:Beijingbotanical10.jpg|
Image:P1090677.JPG|[一二·九运动纪念亭](../Page/一二·九运动.md "wikilink")

## 外部链接

  - [北京市植物园](http://www.beijingbg.com/)
  - [中国科学院植物研究所北京植物园](http://garden.ibcas.ac.cn/)

[category:中國植物園](../Page/category:中國植物園.md "wikilink")

[Category:1955年建立](../Category/1955年建立.md "wikilink")
[Category:北京市公园](../Category/北京市公园.md "wikilink")