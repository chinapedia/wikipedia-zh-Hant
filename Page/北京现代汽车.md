[Hyundai_Elantra_silver_vr.jpg](https://zh.wikipedia.org/wiki/File:Hyundai_Elantra_silver_vr.jpg "fig:Hyundai_Elantra_silver_vr.jpg")
**北京现代汽车有限公司**简称**北京现代**，成立于2002年，\[1\]是由[北京汽车投资有限公司和](../Page/北京汽车投资有限公司.md "wikilink")[韩国](../Page/韩国.md "wikilink")[现代汽车股份有限公司共同出资建立的汽车制造公司](../Page/现代汽车股份有限公司.md "wikilink")。

根据[中国汽车工业协会统计](../Page/中国汽车工业协会.md "wikilink")，北京现代2005年轿车销量为22.47万辆，列全国第四位。截至2019年3月，北京現代汽車在北京有3家整车生产工厂，年生产能力为105万辆。\[2\]

## 主要产品

  - 索纳塔(Sonata)
  - 伊兰特(Elantra)
  - 途胜(Tucson)
  - NF御翔(NF)
  - 雅绅特(Accent)
  - IX35(ix35)
  - 瑞纳(verna)
  - 新胜达（santa fe）
  - 名图（mistra）
  - 菲斯塔(LAFESTA)
  - 名驭
  - 领翔
  - 悦动

## 参考文献

## 外部链接

  - [北京现代汽车有限公司官方网站](http://www.beijing-hyundai.com.cn)

[Category:北汽集团](../Category/北汽集团.md "wikilink")
[Category:现代汽车](../Category/现代汽车.md "wikilink")
[Category:中國汽車公司](../Category/中國汽車公司.md "wikilink")
[Category:中国汽车品牌](../Category/中国汽车品牌.md "wikilink")
[Category:北京市公司](../Category/北京市公司.md "wikilink")
[Category:2002年成立的公司](../Category/2002年成立的公司.md "wikilink")

1.

2.