**北京莫斯科餐厅**建于1954年，由[苏联中央设计院设计](../Page/苏联中央设计院.md "wikilink")，属于[北京展览馆建筑群之一](../Page/北京展览馆.md "wikilink")，主要经营[俄式](../Page/俄羅斯飲食.md "wikilink")[西餐](../Page/西餐.md "wikilink")。北京人昵称其为“[老莫](../Page/老莫.md "wikilink")”。该餐厅是中苏建交后为了体现[社会主义同盟兄弟国家的友谊而建设的餐厅](../Page/社会主义同盟.md "wikilink")，两国首都[北京和](../Page/北京.md "wikilink")[莫斯科分别建有](../Page/莫斯科.md "wikilink")“莫斯科餐厅”和“[北京餐厅](../Page/北京餐厅.md "wikilink")”。

## 历史

[Entrance_to_Moscow_Restaurant_(20151118175713).jpg](https://zh.wikipedia.org/wiki/File:Entrance_to_Moscow_Restaurant_\(20151118175713\).jpg "fig:Entrance_to_Moscow_Restaurant_(20151118175713).jpg")
餐厅的早期服务对象主要为[苏联援华专家](../Page/苏联援华专家.md "wikilink")、驻华官员和赴俄留学归来的[知识分子](../Page/知识分子.md "wikilink")。\[1\]之后因价格较高，主要是干部及其子弟光顾，所以，作为早期为数不多的西餐厅，老莫对一些人来说具有别样的意义。莫斯科餐厅目睹了[中华人民共和国和](../Page/中华人民共和国.md "wikilink")[苏联的蜜月时期](../Page/苏联.md "wikilink")、[裂痕时期](../Page/中苏交恶.md "wikilink")，旁观了[苏联解体](../Page/苏联解体.md "wikilink")，是[中苏关系变迁的见证](../Page/中苏关系.md "wikilink")。

[文化大革命中餐厅曾被改为](../Page/文化大革命.md "wikilink")[食堂并更名为](../Page/食堂.md "wikilink")“北京展览馆餐厅”\[2\]，1968年甚至只供应[中餐](../Page/中餐.md "wikilink")，1969年才恢复供应西餐，其后一度是北京唯一对外营业的西餐厅。\[3\][改革开放后](../Page/改革开放.md "wikilink")，餐厅面临市场压力和挑战，逐渐改换[体制经营](../Page/体制.md "wikilink")，1984年11月7日改回原名。由于其历史与精湛的[厨艺](../Page/厨艺.md "wikilink")，在北京餐饮业中十分著名。

餐厅曾于2000年扩建并重新装修，但风格与1954年开业时不同。为恢复原有风格，餐厅于2009年6月10日再度停业装修\[4\]，直至2009年9月8日重张开业。\[5\]

## 简介

餐厅环境典雅，充满俄式风情。菜肴以俄式西餐为主，[红菜汤](../Page/红菜汤.md "wikilink")、罐焖牛肉、[奶油蘑菇汤](../Page/奶油蘑菇汤.md "wikilink")、奶油烤鱼等都是不错的选择，还有俄式饮料[格瓦斯](../Page/格瓦斯.md "wikilink")。总体价格较为昂贵。

## 参考资料

## 外部链接

  - [莫斯科餐厅网站](http://www.bjmskct.com/)

[Category:北京市餐廳](../Category/北京市餐廳.md "wikilink")
[Category:西城区](../Category/西城区.md "wikilink")
[Category:中俄关系](../Category/中俄关系.md "wikilink")
[Category:1954年成立的公司](../Category/1954年成立的公司.md "wikilink")

1.
2.  [再见老莫-纪念北京1954莫斯科餐厅开业50周年](http://news.sina.com.cn/c/2004-10-14/11244582434.shtml).2004年10月14日.新浪新闻.\[2013-11-14\].
3.
4.
5.