[West_Beijing_Road_Shanghai.JPG](https://zh.wikipedia.org/wiki/File:West_Beijing_Road_Shanghai.JPG "fig:West_Beijing_Road_Shanghai.JPG")
[Avenue_Apartments_on_West_Beijing_Road_Shanghai.JPG](https://zh.wikipedia.org/wiki/File:Avenue_Apartments_on_West_Beijing_Road_Shanghai.JPG "fig:Avenue_Apartments_on_West_Beijing_Road_Shanghai.JPG")
[Lane_1394_West_Beijing_Road_Shanghai.JPG](https://zh.wikipedia.org/wiki/File:Lane_1394_West_Beijing_Road_Shanghai.JPG "fig:Lane_1394_West_Beijing_Road_Shanghai.JPG")
**北京西路**是[中国](../Page/中国.md "wikilink")[上海市](../Page/上海市.md "wikilink")[静安区及](../Page/静安区.md "wikilink")[黄浦区境内的一条东西走向的干道](../Page/黄浦区.md "wikilink")，在租界时代的路名为[爱文义路](../Page/爱文义路.md "wikilink")(Avenue
Road)。该路东起[西藏中路](../Page/西藏中路.md "wikilink")，西至[乌鲁木齐北路](../Page/乌鲁木齐北路.md "wikilink")。全长3502米，宽21～27米。中间与之交汇的南北向道路甚多，自东向西有长沙路、温州路、[黄河路](../Page/黄河路_\(上海\).md "wikilink")、[新昌路](../Page/新昌路_\(上海\).md "wikilink")、[成都北路](../Page/成都北路_\(上海\).md "wikilink")、[大田路](../Page/大田路.md "wikilink")、慈溪路、[石门二路](../Page/石门二路.md "wikilink")、[泰兴路](../Page/泰兴路_\(上海\).md "wikilink")、[江宁路](../Page/江宁路_\(上海\).md "wikilink")、[陕西北路](../Page/陕西北路_\(上海\).md "wikilink")、[西康路](../Page/西康路_\(上海\).md "wikilink")、[铜仁路](../Page/铜仁路.md "wikilink")、[常德路](../Page/常德路_\(上海\).md "wikilink")、[胶州路](../Page/胶州路_\(上海\).md "wikilink")、[万航渡路等](../Page/万航渡路.md "wikilink")。北京西路大致与南侧的[南京西路和北侧的](../Page/南京西路_\(上海\).md "wikilink")[新闸路平行](../Page/新闸路.md "wikilink")。

北京西路的东端，派克路(黄河路)以东段修筑于1887年，名平桥路，为[上海公共租界越界筑路之一](../Page/上海公共租界越界筑路.md "wikilink")。1899年，向西延伸至[赫德路](../Page/赫德路.md "wikilink")(今常德路)，命名为爱文义路，成为上海公共租界西区主要的东西走向的干道之一。其中赫德路到卡德路(今石门二路)段，铺设了上海最早的电车轨道。1918年，平桥路亦更名为爱文义路。1922年，继续向西延伸至极司非而路(今万航渡路)。1943年，汪精卫政权接收租界，爱文义路更名为大同路。1945年改名北京西路。1988年，北京西路又向西延伸延伸至乌鲁木齐北路。

目前北京西路上矗立有多幢现代高楼，如高235.5米的华敏帝豪大厦等。但同时，北京西路的石门二路以西段，沿线分布有不少花园住宅或公寓，已经被列为[南京西路历史文化风貌区的景观道路](../Page/南京西路历史文化风貌区.md "wikilink")。[静安雕塑公园也位于北京西路上](../Page/静安雕塑公园.md "wikilink")。

## 歷史建筑

  - 北京西路239弄平和里27号——[张闻天旧居](../Page/张闻天.md "wikilink")
  - 北京西路304号——[陈鹤琴旧居](../Page/陈鹤琴.md "wikilink")
  - 北京西路1094弄2号——原[伍廷芳住宅](../Page/伍廷芳.md "wikilink")，[大新烟草公司大楼](../Page/大新烟草公司大楼.md "wikilink")，约建于1910年
  - 北京西路1220弄2号——[望德堂](../Page/望德堂_\(上海\).md "wikilink")（今为民宅），建于1930年
  - 北京西路1301號——[贝宅](../Page/贝轩大公馆.md "wikilink")（今为贝轩大公馆酒店），建于1934年
  - 北京西路1314-1320号——[雷士德医学院](../Page/雷士德医学院.md "wikilink")（今为医药工业研究院），建于1932年
  - 铜仁路333号（北京西路[铜仁路口](../Page/铜仁路.md "wikilink")）——[吴同文住宅](../Page/吴同文住宅.md "wikilink")，建于1938年
  - 北京西路1341-1383号——[联华公寓](../Page/联华公寓.md "wikilink")（原名爱文义公寓），1932年
  - 北京西路1394弄2号——[花园住宅](../Page/北京西路1394弄住宅.md "wikilink")（今为上海电气进出口公司），建于1929年
  - 北京西路1400弄——觉园（今为民宅）
      - 1400弄24号——[上海市儿童医院](../Page/上海市儿童医院.md "wikilink")（原难童医院），建于1937年
  - 北京西路1510号——纪氏花园（静安区文化局），[陈咏仁住宅](../Page/陈咏仁.md "wikilink")，建于1929年

## 交汇道路(东向西)

  - [西藏中路](../Page/西藏中路.md "wikilink")
  - [长沙路](../Page/长沙路.md "wikilink")
  - [温州路](../Page/温州路.md "wikilink")
  - [黄河路](../Page/黄河路.md "wikilink")
  - [新昌路](../Page/新昌路.md "wikilink")
  - [成都北路](../Page/成都北路.md "wikilink")
  - [大田路](../Page/大田路.md "wikilink")
  - [石门二路](../Page/石门二路.md "wikilink")
  - [泰兴路](../Page/泰兴路.md "wikilink")
  - [南汇路](../Page/南汇路.md "wikilink")
  - [江宁路](../Page/江宁路.md "wikilink")
  - [陕西北路](../Page/陕西北路.md "wikilink")
  - [西康路](../Page/西康路.md "wikilink")
  - [铜仁路](../Page/铜仁路.md "wikilink")
  - [常德路](../Page/常德路.md "wikilink")
  - [胶州路](../Page/胶州路.md "wikilink")
  - [万航渡路](../Page/万航渡路.md "wikilink")
  - [乌鲁木齐北路](../Page/乌鲁木齐北路.md "wikilink")、[愚园路](../Page/愚园路.md "wikilink")

## 参见

  - [北京东路](../Page/北京东路_\(上海\).md "wikilink")

## 参考资料

[Category:静安区](../Category/静安区.md "wikilink")
[Category:黄浦区](../Category/黄浦区.md "wikilink")
[B](../Category/上海道路.md "wikilink")
[Category:上海市风貌保护道路](../Category/上海市风貌保护道路.md "wikilink")