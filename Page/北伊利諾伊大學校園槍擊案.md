**北伊利诺伊大学校园枪击案**是[美国](../Page/美国.md "wikilink")[伊利诺伊州](../Page/伊利诺伊州.md "wikilink")[迪卡柏](../Page/迪卡尔布_\(伊利诺伊州\).md "wikilink")[北伊利诺伊大学于](../Page/北伊利诺伊大学.md "wikilink")2008年2月14日发生的一起校园枪击事件。事件是在下午大約3點05分時發生於學校的Cole
Hall大堂。\[1\]事件發生後學校宣佈封鎖校區並立即停課，措施延續到隔天結束為止。師生們皆被請求避難至安全處所尋求保護，避免前往事發的現場或臨近大樓。\[2\]
截至2008年2月15日，造成包括枪手在内的6人死亡，16人受伤。枪手在开枪扫射其教师和同学后，开枪自杀。

發生槍擊案的 Cole Hall大堂自案件發生後一直被暫時關閉，直至翻新後於2012年秋季新學年重開。

## 参见

  - [北伊利诺伊大学](../Page/北伊利诺伊大学.md "wikilink")

## 參考資料

<div class="references-small">

<references/>

</div>

## 類似事件

[Category:2008年美国事件](../Category/2008年美国事件.md "wikilink")
[Category:美国大学枪击事件](../Category/美国大学枪击事件.md "wikilink")
[Category:美国谋杀－自杀案](../Category/美国谋杀－自杀案.md "wikilink")
[Category:伊利诺伊州历史](../Category/伊利诺伊州历史.md "wikilink")
[Category:2008年2月](../Category/2008年2月.md "wikilink")

1.
2.