《**北史**》，[唐朝](../Page/唐朝.md "wikilink")[李延寿撰](../Page/李延寿.md "wikilink")。[纪传体](../Page/纪传体.md "wikilink")，共100卷，含[本纪十二卷](../Page/本纪.md "wikilink")，[列传八十八卷](../Page/列传.md "wikilink")，上起[北魏](../Page/北魏.md "wikilink")[登国元年](../Page/登国.md "wikilink")（386），下迄[隋](../Page/隋朝.md "wikilink")[义宁二年](../Page/义宁.md "wikilink")（618），记[北朝北魏](../Page/北朝.md "wikilink")、[西魏](../Page/西魏.md "wikilink")、[东魏](../Page/东魏.md "wikilink")、[北周](../Page/北周.md "wikilink")、[北齐及隋六代二百三十三年史事](../Page/北齐.md "wikilink")。《[南史](../Page/南史.md "wikilink")》與《北史》是唐初史学家李延寿的作品，《北史》主要在《[魏](../Page/魏書.md "wikilink")》、《[齊](../Page/北齊書.md "wikilink")》、《[周](../Page/周書.md "wikilink")》、《[隋](../Page/隋書.md "wikilink")》四書基礎上刪訂改編而成，並參考各種[雜史](../Page/雜史.md "wikilink")，“鸠聚遗逸，以广异闻”，“除其冗长，捃其菁华”，體例完整，文字優美。他撰写这两部书是为了“追终先志”，继承父亲[李大师](../Page/李大师.md "wikilink")（570－628）未完成的事业。

## 撰述

李延寿在《北史·序传》中自述：“从此八代正史外，更勘杂史，于正史所无者一千余卷，皆以编入。”《北史》內容遠較《南史》佳，這是李大師、李延壽父子均為北方人，因此，對北朝的歷史、掌故、風俗、人情都較熟悉。[王鸣盛说](../Page/王鸣盛.md "wikilink")：“观《北史》高洋纪，其穷凶极恶，赖《北史》得著，此李延寿之功。”\[1\][司馬光在給](../Page/司馬光.md "wikilink")[劉道原信中談到](../Page/劉道原.md "wikilink")：“乃知李延壽之書，亦近世之佳史也。雖于祥詼嘲小事，無所不載，然敘事簡徑，比于南北正史，無煩冗蕪穢之辭。竊謂陳壽之後，惟延壽可以亞之也。”唐穆宗长庆三年（823），《南史》、《北史》被列为科考的项目之一。自有南北二史之後，《宋書》、《南齊書》、《魏書》、《梁書》、《陳書》、《北齊書》、《周書》、《隋書》被稱為八書，史稱「二史八書」。

《北史》也有缺點，如完全不載《魏书·李孝伯附李安世传》中关于“均田”的奏疏，南北交兵事，尤多删削\[2\]。另外，南、北二史多記[迷信](../Page/迷信.md "wikilink")[異端](../Page/異端.md "wikilink")，被指為“好述妖异、兆祥、谣谶，特为繁猥”。同時《北史》、《南史》內容亦有重複現象。[朱熹說](../Page/朱熹.md "wikilink")：“南北史除了通鑑所取者，其餘只是一部好笑底小說。”\[3\]

## 内容

### 本紀

1.  魏本紀第一 -
    [太祖道武帝](../Page/北魏道武帝.md "wikilink")・[太宗明元帝](../Page/北魏明元帝.md "wikilink")
2.  魏本紀第二 -
    [世祖太武帝](../Page/北魏太武帝.md "wikilink")・[恭宗景穆帝](../Page/拓跋晃.md "wikilink")・[高宗文成帝](../Page/北魏文成帝.md "wikilink")・[显祖献文帝](../Page/北魏献文帝.md "wikilink")
3.  魏本紀第三 - [高祖孝文帝](../Page/元宏.md "wikilink")
4.  魏本紀第四 -
    [世宗宣武帝](../Page/元恪.md "wikilink")・[肅宗孝明帝](../Page/北魏孝明帝.md "wikilink")
5.  魏本紀第五 -
    [敬宗孝庄帝](../Page/北魏孝莊帝.md "wikilink")・[節閔帝](../Page/元恭.md "wikilink")・[废帝](../Page/元朗.md "wikilink")・[孝武帝](../Page/北魏孝武帝.md "wikilink")・[西魏文帝](../Page/西魏文帝.md "wikilink")・[西魏废帝](../Page/元钦.md "wikilink")・[西魏恭帝](../Page/西魏恭帝.md "wikilink")・[東魏孝静帝](../Page/東魏孝静帝.md "wikilink")
6.  齐本紀上第六 -
    [高祖神武帝](../Page/高欢.md "wikilink")・[世宗文襄帝](../Page/高澄.md "wikilink")
7.  齐本紀中第七 -
    [显祖文宣帝](../Page/高洋.md "wikilink")・[废帝](../Page/高殷.md "wikilink")・[孝昭帝](../Page/北齐孝昭帝.md "wikilink")
8.  齐本紀下第八 -
    [世祖武成帝](../Page/北齐武成帝.md "wikilink")・[後主](../Page/北齐後主.md "wikilink")・[幼主](../Page/北齐幼主.md "wikilink")
9.  周本紀上第九 -
    [太祖文帝](../Page/宇文泰.md "wikilink")・[孝閔帝](../Page/北周孝閔帝.md "wikilink")・[世宗明帝](../Page/北周明帝.md "wikilink")
10. 周本紀下第十 -
    [高祖武帝](../Page/北周武帝.md "wikilink")・[宣帝](../Page/北周宣帝.md "wikilink")・[静帝](../Page/北周静帝.md "wikilink")
11. 隋本紀上第十一 - [高祖文帝](../Page/楊堅.md "wikilink")
12. 隋本紀下第十二 -
    [煬帝](../Page/煬帝.md "wikilink")・[恭帝](../Page/楊侑.md "wikilink")

### 列传

1.  列传第一 后妃上 -
    [魏神元皇后窦氏](../Page/神元皇后.md "wikilink")・[文帝皇后封氏](../Page/封皇后.md "wikilink")・[桓皇后惟氏](../Page/祁皇后.md "wikilink")・[平文皇后王氏](../Page/王皇后_\(代平文帝\).md "wikilink")・[昭成皇后慕容氏](../Page/慕容皇后_\(拓跋什翼犍\).md "wikilink")・[献明皇后贺氏](../Page/賀皇后_\(拓跋寔\).md "wikilink")・[道武皇后慕容氏](../Page/慕容皇后_\(拓跋珪\).md "wikilink")・[道武宣穆皇后刘氏](../Page/刘贵人_\(北魏道武帝\).md "wikilink")・[明元昭哀皇后姚氏](../Page/昭哀皇后.md "wikilink")・[明元密皇后杜氏](../Page/杜夫人_\(北魏明元帝\).md "wikilink")・[太武皇后赫连氏](../Page/太武皇后.md "wikilink")・[太武敬哀皇后贺氏](../Page/贺夫人_\(拓跋焘\).md "wikilink")・[景穆恭皇后郁久闾氏](../Page/恭皇后_\(北魏\).md "wikilink")・[文成文明皇后冯氏](../Page/文成文明皇后.md "wikilink")・[文成元皇后李氏](../Page/文成元皇后.md "wikilink")・[献文思皇后李氏](../Page/李夫人_\(北魏獻文帝\).md "wikilink")・[孝文贞皇后林氏](../Page/孝文貞皇后.md "wikilink")・[孝文废皇后冯氏](../Page/孝文废皇后.md "wikilink")・[孝文幽皇后冯氏](../Page/孝文幽皇后.md "wikilink")・[孝文文昭皇后高氏](../Page/高照容.md "wikilink")・[宣武顺皇后于氏](../Page/順皇后_\(北魏\).md "wikilink")・[宣武皇后高氏](../Page/高英.md "wikilink")・[宣武灵皇后胡氏](../Page/宣武靈皇后.md "wikilink")・[孝明皇后胡氏](../Page/胡皇后_\(北魏孝明帝\).md "wikilink")・[孝武皇后高氏](../Page/高皇后_\(北魏孝武帝\).md "wikilink")・[文帝文皇后乙弗氏](../Page/乙弗皇后.md "wikilink")・[文帝悼皇后郁久闾氏](../Page/郁久閭皇后_\(西魏文帝\).md "wikilink")・[废帝皇后宇文氏](../Page/宇文皇后.md "wikilink")・[恭帝皇后若干氏](../Page/若干皇后.md "wikilink")・[孝静皇后高氏](../Page/高皇后_\(東魏孝靜帝\).md "wikilink")
2.  列传第二 后妃下 -
    [齐武明皇后娄氏](../Page/娄昭君.md "wikilink")・[蠕蠕公主郁久闾氏](../Page/蠕蠕公主.md "wikilink")・[大尔朱氏](../Page/大尔朱氏.md "wikilink")・[小尔朱氏](../Page/爾朱皇后_\(北魏長廣王\).md "wikilink")・[上党太妃韩氏](../Page/韩娘_\(北齐神武帝\).md "wikilink")・[冯翊太妃郑氏](../Page/鄭大車.md "wikilink")・[高阳太妃游氏](../Page/游娘_\(北齐神武帝\).md "wikilink")・[冯娘](../Page/冯娘_\(北齐神武帝\).md "wikilink")・[李娘](../Page/李娘_\(高欢\).md "wikilink")・[文襄敬皇后元氏](../Page/元仲华.md "wikilink")・[琅邪公主](../Page/元玉儀.md "wikilink")・[文宣皇后李氏](../Page/李祖娥.md "wikilink")・[段昭仪](../Page/段昭仪_\(北齐\).md "wikilink")・[王嬪](../Page/王嫔_\(高洋\).md "wikilink")・[薛嬪](../Page/薛嬪.md "wikilink")・[孝昭皇后元氏](../Page/元皇后_\(北齊孝昭帝\).md "wikilink")・[武成皇后胡氏](../Page/胡皇后_\(北齐武成帝\).md "wikilink")・[弘德李夫人](../Page/李夫人_\(北齐武成帝\).md "wikilink")・[后主皇后斛律氏](../Page/斛律皇后.md "wikilink")・[后主皇后胡氏](../Page/胡皇后_\(北齐后主\).md "wikilink")・[后主皇后穆氏](../Page/穆黃花.md "wikilink")・[冯淑妃](../Page/冯小怜.md "wikilink")・[周文皇后元氏](../Page/元夫人_\(北周文帝\).md "wikilink")・[文宣皇后叱奴氏](../Page/叱奴太后.md "wikilink")・[孝闵皇后元氏](../Page/元胡摩.md "wikilink")・[明敬皇后独孤氏](../Page/明敬皇后_\(北周\).md "wikilink")・[武成皇后阿史那氏](../Page/阿史那皇后.md "wikilink")・[武皇后李氏](../Page/李娥姿.md "wikilink")・[宣皇后杨氏](../Page/楊麗華.md "wikilink")・[宣皇后朱氏](../Page/朱滿月.md "wikilink")・[宣皇后陈氏](../Page/陳月儀.md "wikilink")・[宣皇后元氏](../Page/元樂尚.md "wikilink")・[宣皇后尉迟氏](../Page/尉遲熾繁.md "wikilink")・[静皇后司马氏](../Page/司馬令姬.md "wikilink")・[隋文献皇后独孤氏](../Page/獨孤伽羅.md "wikilink")・[宣华夫人陈氏](../Page/宣華夫人.md "wikilink")・[容华夫人蔡氏](../Page/容華夫人.md "wikilink")・[炀愍皇后萧氏](../Page/愍皇后_\(隋朝\).md "wikilink")
3.  列传第三 魏諸[宗室](../Page/宗室.md "wikilink") -
4.  列传第四 道武七王・明元六王・太武五王 -
    [清河王紹](../Page/拓跋紹.md "wikilink")・[陽平王熙](../Page/拓跋熙.md "wikilink")・[河南王曜](../Page/拓跋曜.md "wikilink")・[長乐王处文](../Page/拓跋处文.md "wikilink")・[广平王連](../Page/拓跋連.md "wikilink")・[京兆王黎](../Page/拓跋黎.md "wikilink")・[乐平王丕](../Page/拓跋丕.md "wikilink")・[安定王彌](../Page/拓跋弥.md "wikilink")・[乐安王範](../Page/拓跋範.md "wikilink")・[永昌王健](../Page/拓跋健.md "wikilink")・[建寧王崇](../Page/拓跋崇_\(建宁王\).md "wikilink")・[新興王俊](../Page/拓跋俊.md "wikilink")・[晋王伏羅](../Page/拓跋伏羅.md "wikilink")・[東平王翰](../Page/拓跋翰_\(东平王\).md "wikilink")・[臨淮王譚](../Page/拓跋譚.md "wikilink")・[广陽王建](../Page/拓跋建.md "wikilink")・[南安王余](../Page/拓跋余.md "wikilink")
5.  列传第五 景穆十二王上 -
    [元新成](../Page/元新成.md "wikilink")・[元子推](../Page/元子推.md "wikilink")・[元小新成](../Page/元小新成.md "wikilink")・[元天賜](../Page/元天賜.md "wikilink")・[元万寿](../Page/元万寿.md "wikilink")・[元洛侯](../Page/元洛侯.md "wikilink")
6.  列传第六 景穆十二王下 -
    [元雲](../Page/元雲.md "wikilink")・[元楨](../Page/元楨.md "wikilink")・[元長寿](../Page/元長寿.md "wikilink")・[元太洛](../Page/元太洛.md "wikilink")・[元胡儿](../Page/元胡儿.md "wikilink")・[元休](../Page/元休.md "wikilink")
7.  列传第七 文成五王・献文六王・孝文六王 -
    [安乐王長乐](../Page/元長乐.md "wikilink")・[广川王略](../Page/元略.md "wikilink")・[齐郡王簡](../Page/元簡.md "wikilink")・[河間王若](../Page/元若.md "wikilink")・[安丰王猛](../Page/元猛.md "wikilink")・[咸陽王禧](../Page/元禧.md "wikilink")・[趙郡王幹](../Page/元幹.md "wikilink")・[广陵王羽](../Page/元羽.md "wikilink")・[高陽王雍](../Page/元雍.md "wikilink")・[北海王詳](../Page/元詳.md "wikilink")・[彭城王勰](../Page/元勰.md "wikilink")・[废太子恂](../Page/元恂.md "wikilink")・[京兆王愉](../Page/元愉.md "wikilink")・[清河王懌](../Page/元懌.md "wikilink")・[广平王怀](../Page/元怀.md "wikilink")・[汝南王悦](../Page/元悦.md "wikilink")
8.  列传第八 -
    [衛操](../Page/衛操.md "wikilink")・[莫含](../Page/莫含.md "wikilink")・[劉庫仁](../Page/劉庫仁.md "wikilink")・[尉古真](../Page/尉古真.md "wikilink")・[穆崇](../Page/穆崇.md "wikilink")・[奚斤](../Page/奚斤.md "wikilink")・[叔孫建](../Page/叔孫建.md "wikilink")・[安同](../Page/安同.md "wikilink")・[庾業延](../Page/庾業延.md "wikilink")・[王建](../Page/王建_\(北魏\).md "wikilink")・[羅結](../Page/羅結.md "wikilink")・[楼伏連](../Page/楼伏連.md "wikilink")・[閭大肥](../Page/閭大肥.md "wikilink")・[奚牧](../Page/奚牧.md "wikilink")・[和跋](../Page/和跋.md "wikilink")・[莫題](../Page/莫題.md "wikilink")・[賀狄干](../Page/賀狄干.md "wikilink")・[李栗](../Page/李栗.md "wikilink")・[奚眷](../Page/奚眷.md "wikilink")
9.  列传第九 -
    [燕鳳](../Page/燕鳳.md "wikilink")・[許謙](../Page/許謙.md "wikilink")・[崔宏](../Page/崔宏.md "wikilink")・[張袞](../Page/張袞.md "wikilink")・[鄧彦海](../Page/鄧彦海.md "wikilink")
10. 列传第十 -
    [長孫嵩](../Page/長孫嵩.md "wikilink")・[長孫道生](../Page/長孫道生.md "wikilink")・[長孫肥](../Page/長孫肥.md "wikilink")
11. 列传第十一 - -{[于栗磾](../Page/于栗磾.md "wikilink")}--{}-
12. 列传第十二 -
    [崔逞](../Page/崔逞.md "wikilink")・[王憲](../Page/王宪_\(北魏\).md "wikilink")・[封懿](../Page/封懿.md "wikilink")
13. 列传第十三 -
    [古弼](../Page/古弼.md "wikilink")・[張黎](../Page/張黎_\(北魏\).md "wikilink")・[劉潔](../Page/劉潔.md "wikilink")・[丘堆](../Page/丘堆.md "wikilink")・[娥清](../Page/娥清.md "wikilink")・[伊馛](../Page/伊馛.md "wikilink")・[乙瓌](../Page/乙瓌.md "wikilink")・[周幾](../Page/周幾.md "wikilink")・[豆代田](../Page/豆代田.md "wikilink")・[車伊洛](../Page/車伊洛.md "wikilink")・[王洛儿](../Page/王洛儿.md "wikilink")・[車路頭](../Page/車路頭.md "wikilink")・[盧魯元](../Page/盧魯元.md "wikilink")・[陳建](../Page/陳建.md "wikilink")・[来大干](../Page/来大干.md "wikilink")・[宿石](../Page/宿石.md "wikilink")・[万安国](../Page/万安国.md "wikilink")・[周观](../Page/周观.md "wikilink")・[尉撥](../Page/尉撥.md "wikilink")・[陸真](../Page/陸真.md "wikilink")・[呂洛拔](../Page/呂洛拔.md "wikilink")・[薛彪子](../Page/薛彪子.md "wikilink")・[尉元](../Page/尉元.md "wikilink")・[慕容白曜](../Page/慕容白曜.md "wikilink")・[和其奴](../Page/和其奴.md "wikilink")・[苟頽](../Page/苟頽.md "wikilink")・[宇文福](../Page/宇文福.md "wikilink")
14. 列传第十四 -
    [宋隐](../Page/宋隐.md "wikilink")・[許彦](../Page/許彦.md "wikilink")・[刁雍](../Page/刁雍.md "wikilink")・[辛紹先](../Page/辛紹先.md "wikilink")・[韋閬](../Page/韋閬.md "wikilink")・[杜銓](../Page/杜銓.md "wikilink")
15. 列传第十五 -
    [屈遵](../Page/屈遵.md "wikilink")・[張蒲](../Page/張蒲.md "wikilink")・[谷渾](../Page/谷渾.md "wikilink")・[公孫表](../Page/公孫表.md "wikilink")・[張济](../Page/張济_\(北魏\).md "wikilink")・[李先](../Page/李先.md "wikilink")・[賈彝](../Page/賈彝.md "wikilink")・[竇瑾](../Page/竇瑾.md "wikilink")・[李訢](../Page/李䜣_\(北魏\).md "wikilink")・[韓延之](../Page/韓延之.md "wikilink")・[袁式](../Page/袁式.md "wikilink")・[毛修之](../Page/毛修之.md "wikilink")・[唐和](../Page/唐和_\(南北朝\).md "wikilink")・[寇讃](../Page/寇讃.md "wikilink")・[郦范](../Page/郦范.md "wikilink")・[韓秀](../Page/韓秀.md "wikilink")・[堯暄](../Page/堯暄.md "wikilink")・[柳崇](../Page/柳崇.md "wikilink")
16. 列传第十六 -
    [陸俟](../Page/陸俟.md "wikilink")・[源賀](../Page/源賀.md "wikilink")・[劉尼](../Page/刘尼_\(北魏\).md "wikilink")・[薛提](../Page/薛提.md "wikilink")
17. 列传第十七 -
    [司馬休之](../Page/司馬休之.md "wikilink")・[司馬楚之](../Page/司馬楚之.md "wikilink")・[劉昶](../Page/劉昶.md "wikilink")・[蕭宝寅](../Page/蕭宝寅.md "wikilink")・[蕭正表](../Page/蕭正表.md "wikilink")・[蕭祗](../Page/蕭祗.md "wikilink")・[蕭退](../Page/蕭退.md "wikilink")・[蕭泰](../Page/蕭泰.md "wikilink")・[蕭撝](../Page/蕭撝.md "wikilink")・[蕭圆肅](../Page/蕭圆肅.md "wikilink")・[蕭大圜](../Page/蕭大圜.md "wikilink")
18. 列传第十八 -
    [盧玄](../Page/盧玄.md "wikilink")・[盧柔](../Page/盧柔.md "wikilink")・[盧观](../Page/盧观.md "wikilink")・[盧同](../Page/盧同.md "wikilink")・[盧誕](../Page/盧誕.md "wikilink")
19. 列传第十九 -
    [高允](../Page/高允.md "wikilink")・[高祐](../Page/高祐.md "wikilink")・[盧曹](../Page/盧曹.md "wikilink")
20. 列传第二十 -
    [崔鑑](../Page/崔鑑.md "wikilink")・[崔辯](../Page/崔辯.md "wikilink")・[崔挺](../Page/崔挺.md "wikilink")
21. 列传第二十一 -
    [李灵](../Page/李灵.md "wikilink")・[李順](../Page/李順.md "wikilink")・[李孝伯](../Page/李孝伯.md "wikilink")・[李裔](../Page/李裔.md "wikilink")・[李義深](../Page/李義深.md "wikilink")
22. 列传第二十二 -
    [游雅](../Page/游雅.md "wikilink")・[高閭](../Page/高閭.md "wikilink")・[趙逸](../Page/趙逸.md "wikilink")・[胡叟](../Page/胡叟.md "wikilink")・[胡方回](../Page/胡方回.md "wikilink")・[張湛](../Page/張湛_\(北魏\).md "wikilink")・[段承根](../Page/段承根.md "wikilink")・[闞駰](../Page/闞駰.md "wikilink")・[劉延明](../Page/劉延明.md "wikilink")・[趙柔](../Page/趙柔.md "wikilink")・[索敞](../Page/索敞.md "wikilink")・[宋繇](../Page/宋繇.md "wikilink")・[江式](../Page/江式.md "wikilink")
23. 列传第二十三 -
    [王慧龍](../Page/王慧龍.md "wikilink")・[鄭羲](../Page/鄭羲.md "wikilink")
24. 列传第二十四 -
    [薛辯](../Page/薛辯.md "wikilink")・[薛寘](../Page/薛寘.md "wikilink")・[薛憕](../Page/薛憕.md "wikilink")
25. 列传第二十五 -
    [韓茂](../Page/韓茂.md "wikilink")・[皮豹子](../Page/皮豹子.md "wikilink")・[封敕文](../Page/封敕文.md "wikilink")・[呂羅漢](../Page/呂羅漢.md "wikilink")・[孔伯恭](../Page/孔伯恭.md "wikilink")・[田益宗](../Page/田益宗.md "wikilink")・[孟表](../Page/孟表.md "wikilink")・[奚康生](../Page/奚康生.md "wikilink")・[楊大眼](../Page/楊大眼.md "wikilink")・[崔延伯](../Page/崔延伯.md "wikilink")・[李叔仁](../Page/李叔仁.md "wikilink")
26. 列传第二十六 -
    [裴駿](../Page/裴駿.md "wikilink")・[裴延儁](../Page/裴延儁.md "wikilink")・[裴佗](../Page/裴佗.md "wikilink")・[裴果](../Page/裴果.md "wikilink")・[裴宽](../Page/裴宽.md "wikilink")・[裴侠](../Page/裴侠.md "wikilink")・[裴文举](../Page/裴文举.md "wikilink")・[裴仁基](../Page/裴仁基.md "wikilink")
27. 列传第二十七 -
    [薛安都](../Page/薛安都.md "wikilink")・[劉休賓](../Page/劉休賓.md "wikilink")・[房法寿](../Page/房法寿.md "wikilink")・[畢衆敬](../Page/畢衆敬.md "wikilink")・[羊祉](../Page/羊祉.md "wikilink")
28. 列传第二十八 -
    [韓麒麟](../Page/韓麒麟.md "wikilink")・[程駿](../Page/程駿.md "wikilink")・[李彪](../Page/李彪.md "wikilink")・[高道悦](../Page/高道悦.md "wikilink")・[甄琛](../Page/甄琛.md "wikilink")・[張纂](../Page/張纂.md "wikilink")・[高聪](../Page/高聪.md "wikilink")
29. 列传第二十九 -
    [楊播](../Page/楊播.md "wikilink")・[楊敷](../Page/楊敷.md "wikilink")
30. 列传第三十 -
    [王肅](../Page/王肅_\(北魏\).md "wikilink")・[劉芳](../Page/劉芳.md "wikilink")・[常爽](../Page/常爽.md "wikilink")
31. 列传第三十一 -
    [郭祚](../Page/郭祚.md "wikilink")・[張彝](../Page/張彝.md "wikilink")・[邢巒](../Page/邢巒.md "wikilink")・[李崇](../Page/李崇.md "wikilink")
32. 列传第三十二 -
    [崔光](../Page/崔光.md "wikilink")・[崔亮](../Page/崔亮.md "wikilink")
33. 列传第三十三 -
    [裴叔業](../Page/裴叔業.md "wikilink")・[夏侯道遷](../Page/夏侯道遷.md "wikilink")・[李元護](../Page/李元護.md "wikilink")・[席法友](../Page/席法友.md "wikilink")・[王世弼](../Page/王世弼.md "wikilink")・[江悦之](../Page/江悦之.md "wikilink")・[淳于誕](../Page/淳于誕.md "wikilink")・[沈文秀](../Page/沈文秀.md "wikilink")・[張讜](../Page/張讜.md "wikilink")・[李苗](../Page/李苗.md "wikilink")・[劉藻](../Page/劉藻.md "wikilink")・[傅永](../Page/傅永.md "wikilink")・[傅竪眼](../Page/傅竪眼.md "wikilink")・[張烈](../Page/張烈.md "wikilink")・[李叔彪](../Page/李叔彪.md "wikilink")・[路恃慶](../Page/路恃慶.md "wikilink")・[房亮](../Page/房亮.md "wikilink")・[曹世表](../Page/曹世表.md "wikilink")・[潘永基](../Page/潘永基.md "wikilink")・[朱元旭](../Page/朱元旭.md "wikilink")
34. 列传第三十四 -
    [孫紹](../Page/孫紹_\(北魏\).md "wikilink")・[張普惠](../Page/張普惠.md "wikilink")・[成淹](../Page/成淹.md "wikilink")・[范紹](../Page/范紹.md "wikilink")・[劉桃符](../Page/劉桃符.md "wikilink")・[鹿悆](../Page/鹿悆.md "wikilink")・[張燿](../Page/張燿.md "wikilink")・[劉道斌](../Page/劉道斌.md "wikilink")・[董紹](../Page/董紹.md "wikilink")・[馮元興](../Page/馮元興.md "wikilink")
35. 列传第三十五 -
    [袁翻](../Page/袁翻.md "wikilink")・[陽尼](../Page/陽尼.md "wikilink")・[賈思伯](../Page/賈思伯.md "wikilink")・[祖瑩](../Page/祖瑩.md "wikilink")
36. 列传第三十六 - [爾朱荣](../Page/爾朱荣.md "wikilink")
37. 列传第三十七 -
    [朱瑞](../Page/朱瑞_\(北魏\).md "wikilink")・[叱列延慶](../Page/叱列延慶.md "wikilink")・[斛斯椿](../Page/斛斯椿.md "wikilink")・[賈显度](../Page/賈显度.md "wikilink")・[樊子鵠](../Page/樊子鵠.md "wikilink")・[侯深](../Page/侯深.md "wikilink")・[賀拔允](../Page/賀拔允.md "wikilink")・[侯莫陳悦](../Page/侯莫陳悦.md "wikilink")・[念賢](../Page/念賢.md "wikilink")・[梁览](../Page/梁览.md "wikilink")・[雷紹](../Page/雷紹.md "wikilink")・[毛遐](../Page/毛遐.md "wikilink")・[乙弗朗](../Page/乙弗朗.md "wikilink")
38. 列传第三十八 -
    [辛雄](../Page/辛雄.md "wikilink")・[楊機](../Page/楊機.md "wikilink")・[高道穆](../Page/高道穆.md "wikilink")・[綦儁](../Page/綦儁.md "wikilink")・[山偉](../Page/山偉.md "wikilink")・[宇文忠之](../Page/宇文忠之.md "wikilink")・[費穆](../Page/費穆.md "wikilink")・[孟威](../Page/孟威.md "wikilink")
39. 列传第三十九 齐宗室諸王上 -
    [趙郡王琛](../Page/高琛.md "wikilink")・[清河王岳](../Page/高岳.md "wikilink")・[广平公盛](../Page/高盛.md "wikilink")・[陽州公永乐](../Page/高永乐.md "wikilink")・[襄乐王显国](../Page/高显国.md "wikilink")・[上洛王思宗](../Page/高思宗.md "wikilink")・[平秦王归彦](../Page/高归彦.md "wikilink")・[長乐太守灵山](../Page/高灵山.md "wikilink")
40. 列传第四十 齐宗室諸王下 -
    [河南王孝瑜](../Page/高孝瑜.md "wikilink")・[广寧王孝珩](../Page/高孝珩.md "wikilink")・[河間王孝琬](../Page/高孝琬.md "wikilink")・[蘭陵王長恭](../Page/高長恭.md "wikilink")・[安德王延宗](../Page/高延宗.md "wikilink")・[漁陽王紹信](../Page/高紹信.md "wikilink")・[太原王紹德](../Page/高紹德.md "wikilink")・[范陽王紹義](../Page/高紹義.md "wikilink")・[西河王紹仁](../Page/高紹仁.md "wikilink")・[隴西王紹廉](../Page/高紹廉.md "wikilink")・[乐陵王百年](../Page/高百年.md "wikilink")・[汝南王彦理](../Page/高彦理.md "wikilink")・[南陽王綽](../Page/高綽.md "wikilink")・[琅邪王儼](../Page/高儼.md "wikilink")・[齐安王廓](../Page/高廓.md "wikilink")・[東平王恪](../Page/高恪.md "wikilink")
41. 列传第四十一 -
    [万俟普](../Page/万俟普.md "wikilink")・[可朱渾元](../Page/可朱渾元.md "wikilink")・[劉丰](../Page/劉丰.md "wikilink")・[破六韓常](../Page/破六韓常.md "wikilink")・[金祚](../Page/金祚.md "wikilink")・[劉貴](../Page/劉貴.md "wikilink")・[蔡儁](../Page/蔡儁.md "wikilink")・[韓賢](../Page/韓賢.md "wikilink")・[尉長命](../Page/尉長命.md "wikilink")・[王怀](../Page/王怀.md "wikilink")・[任祥](../Page/任祥.md "wikilink")・[莫多婁貸文](../Page/莫多婁貸文.md "wikilink")・[厙狄迴洛](../Page/厙狄迴洛.md "wikilink")・[厙狄盛](../Page/厙狄盛.md "wikilink")・[張保洛](../Page/張保洛.md "wikilink")・[侯莫陳相](../Page/侯莫陳相.md "wikilink")・[薛孤延](../Page/薛孤延.md "wikilink")・[斛律羌举](../Page/斛律羌举.md "wikilink")・[張瓊](../Page/張瓊.md "wikilink")・[宋显](../Page/宋显.md "wikilink")・[王則](../Page/王則.md "wikilink")・[慕容紹宗](../Page/慕容紹宗.md "wikilink")・[叱列平](../Page/叱列平.md "wikilink")・[步大汗薩](../Page/步大汗薩.md "wikilink")・[薛修義](../Page/薛修義.md "wikilink")・[慕容儼](../Page/慕容儼.md "wikilink")・[潘乐](../Page/潘乐.md "wikilink")・[彭乐](../Page/彭乐.md "wikilink")・[暴显](../Page/暴显.md "wikilink")・[皮景和](../Page/皮景和.md "wikilink")・[綦連猛](../Page/綦連猛.md "wikilink")・[元景安](../Page/元景安.md "wikilink")・[独孤永業](../Page/独孤永業.md "wikilink")・[鮮于世荣](../Page/鮮于世荣.md "wikilink")・[傅伏](../Page/傅伏.md "wikilink")
42. 列传第四十二 -
    [孫騰](../Page/孫騰.md "wikilink")・[高隆之](../Page/高隆之.md "wikilink")・[司馬子如](../Page/司馬子如.md "wikilink")・[竇泰](../Page/竇泰.md "wikilink")・[尉景](../Page/尉景.md "wikilink")・[婁昭](../Page/婁昭.md "wikilink")・[厙狄干](../Page/厙狄干.md "wikilink")・[韓軌](../Page/韓軌.md "wikilink")・[段荣](../Page/段荣.md "wikilink")・[斛律金](../Page/斛律金.md "wikilink")
43. 列传第四十三 -
    [孫搴](../Page/孫搴.md "wikilink")・[陳元康](../Page/陳元康.md "wikilink")・[杜弼](../Page/杜弼.md "wikilink")・[房謨](../Page/房謨.md "wikilink")・[張纂](../Page/張纂.md "wikilink")・[張亮](../Page/張亮_\(北齐\).md "wikilink")・[張曜](../Page/張曜_\(北齐\).md "wikilink")・[王峻](../Page/王峻.md "wikilink")・[王紘](../Page/王紘.md "wikilink")・[敬显儁](../Page/敬显儁.md "wikilink")・[平鑑](../Page/平鑑.md "wikilink")・[唐邕](../Page/唐邕.md "wikilink")・[白建](../Page/白建.md "wikilink")・[元文遙](../Page/元文遙.md "wikilink")・[趙彦深](../Page/趙彦深.md "wikilink")・[赫連子悦](../Page/赫連子悦.md "wikilink")・[馮子琮](../Page/馮子琮.md "wikilink")・[郎基](../Page/郎基.md "wikilink")
44. 列传第四十四 -
    [魏收](../Page/魏收.md "wikilink")・[魏長賢](../Page/魏長賢.md "wikilink")・[魏季景](../Page/魏季景.md "wikilink")・[魏蘭根](../Page/魏蘭根.md "wikilink")
45. 列传第四十五 周宗室 -
    [邵惠公顥](../Page/宇文顥.md "wikilink")・[杞簡公連](../Page/宇文連.md "wikilink")・[莒庄公洛生](../Page/宇文洛生.md "wikilink")・[虞国公仲](../Page/宇文仲.md "wikilink")・[广川公測](../Page/宇文測.md "wikilink")・[東平公神举](../Page/宇文神举.md "wikilink")
46. 列传第四十六 周室諸王 -
    [宋献公震](../Page/宇文震.md "wikilink")・[衛剌王直](../Page/宇文直.md "wikilink")・[齐煬王憲](../Page/宇文憲.md "wikilink")・[趙僭王招](../Page/宇文招.md "wikilink")・[譙孝王俭](../Page/宇文俭.md "wikilink")・[陳惑王純](../Page/宇文純.md "wikilink")・[越野王盛](../Page/宇文盛.md "wikilink")・[代奰王達](../Page/宇文達.md "wikilink")・[冀康公通](../Page/宇文通.md "wikilink")・[滕聞王逌](../Page/宇文逌.md "wikilink")・[紀厲王康](../Page/宇文康.md "wikilink")・[畢剌王賢](../Page/宇文賢.md "wikilink")・[酆王貞](../Page/宇文貞.md "wikilink")・[漢王賛](../Page/宇文賛.md "wikilink")・[萊王衎](../Page/宇文衎.md "wikilink")
47. 列传第四十七 -
    [寇洛](../Page/寇洛.md "wikilink")・[趙貴](../Page/趙貴.md "wikilink")・[李賢](../Page/李贤_\(北周\).md "wikilink")・[梁禦](../Page/梁禦.md "wikilink")
48. 列传第四十八 -
    [李弼](../Page/李弼.md "wikilink")・[宇文貴](../Page/宇文貴.md "wikilink")・[侯莫陳崇](../Page/侯莫陳崇.md "wikilink")・[王雄](../Page/王雄.md "wikilink")
49. 列传第四十九 -
    [王盟](../Page/王盟.md "wikilink")・[独孤信](../Page/独孤信.md "wikilink")・[竇熾](../Page/竇熾.md "wikilink")・[賀蘭祥](../Page/賀蘭祥.md "wikilink")・[叱列伏龟](../Page/叱列伏龟.md "wikilink")・[閻慶](../Page/閻慶.md "wikilink")・[史寧](../Page/史寧.md "wikilink")・[权景宣](../Page/权景宣.md "wikilink")
50. 列传第五十 -
    [王羆](../Page/王羆.md "wikilink")・[王思政](../Page/王思政.md "wikilink")・[尉迟迥](../Page/尉迟迥.md "wikilink")・[王軌](../Page/王軌.md "wikilink")
51. 列传第五十一 -
    [周惠達](../Page/周惠達.md "wikilink")・[馮景](../Page/馮景.md "wikilink")・[蘇綽](../Page/蘇綽.md "wikilink")
52. 列传第五十二 -
    [韋孝宽](../Page/韋孝宽.md "wikilink")・[韋瑱](../Page/韋瑱.md "wikilink")・[柳虯](../Page/柳虯.md "wikilink")
53. 列传第五十三 -
    [達奚武](../Page/達奚武.md "wikilink")・[若干惠](../Page/若干惠.md "wikilink")・[怡峰](../Page/怡峰.md "wikilink")・[劉亮](../Page/劉亮.md "wikilink")・[王德](../Page/王德.md "wikilink")・[赫連達](../Page/赫連達.md "wikilink")・[韓果](../Page/韓果.md "wikilink")・[蔡祐](../Page/蔡祐.md "wikilink")・[常善](../Page/常善.md "wikilink")・[辛威](../Page/辛威.md "wikilink")・[厙狄昌](../Page/厙狄昌.md "wikilink")・[梁椿](../Page/梁椿.md "wikilink")・[梁台](../Page/梁台.md "wikilink")・[田弘](../Page/田弘.md "wikilink")
54. 列传第五十四 -
    [王傑](../Page/王傑_\(北周\).md "wikilink")・[王勇](../Page/王勇_\(北周\).md "wikilink")・[宇文虯](../Page/宇文虯.md "wikilink")・[耿豪](../Page/耿豪.md "wikilink")・[高琳](../Page/高琳.md "wikilink")・[李和](../Page/李和.md "wikilink")・[伊婁穆](../Page/伊婁穆.md "wikilink")・[達奚寔](../Page/達奚寔.md "wikilink")・[劉雄](../Page/劉雄.md "wikilink")・[侯植](../Page/侯植.md "wikilink")・[李延孫](../Page/李延孫.md "wikilink")・[韦法保](../Page/韦法保.md "wikilink")・[陳欣](../Page/陳欣.md "wikilink")・[魏玄](../Page/魏玄.md "wikilink")・[泉仚](../Page/泉仚.md "wikilink")・[李遷哲](../Page/李遷哲.md "wikilink")・[楊乾運](../Page/楊乾運.md "wikilink")・[扶猛](../Page/扶猛.md "wikilink")・[陽雄](../Page/陽雄.md "wikilink")・[席固](../Page/席固.md "wikilink")・[任果](../Page/任果.md "wikilink")
55. 列传第五十五 -
    [崔彦穆](../Page/崔彦穆.md "wikilink")・[楊纂](../Page/楊纂.md "wikilink")・[段永](../Page/段永.md "wikilink")・[令狐整](../Page/令狐整.md "wikilink")・[唐永](../Page/唐永.md "wikilink")・[柳敏](../Page/柳敏.md "wikilink")・[王士良](../Page/王士良.md "wikilink")
56. 列传第五十六 -
    [豆盧寧](../Page/豆盧寧.md "wikilink")・[楊紹](../Page/楊紹_\(北周\).md "wikilink")・[王雅](../Page/王雅_\(北周\).md "wikilink")・[韓雄](../Page/韓雄.md "wikilink")・[賀若敦](../Page/賀若敦.md "wikilink")
57. 列传第五十七 -
    [申徽](../Page/申徽.md "wikilink")・[陸通](../Page/陸通.md "wikilink")・[厙狄峙](../Page/厙狄峙.md "wikilink")・[楊荐](../Page/楊荐.md "wikilink")・[王慶](../Page/王慶_\(北周\).md "wikilink")・[趙剛](../Page/趙剛.md "wikilink")・[趙昶](../Page/趙昶.md "wikilink")・[王悦](../Page/王悦.md "wikilink")・[趙文表](../Page/趙文表.md "wikilink")・[元定](../Page/元定.md "wikilink")・[楊摽](../Page/楊摽.md "wikilink")
58. 列传第五十八 -
    [韓褒](../Page/韓褒.md "wikilink")・[趙肅](../Page/趙肅.md "wikilink")・[張軌](../Page/張軌_\(北周\).md "wikilink")・[李彦](../Page/李彦.md "wikilink")・[郭彦](../Page/郭彦.md "wikilink")・[梁昕](../Page/梁昕.md "wikilink")・[皇甫璠](../Page/皇甫璠.md "wikilink")・[辛慶之](../Page/辛慶之.md "wikilink")・[王子直](../Page/王子直.md "wikilink")・[杜杲](../Page/杜杲.md "wikilink")・[呂思礼](../Page/呂思礼.md "wikilink")・[徐招](../Page/徐招.md "wikilink")・[檀翥](../Page/檀翥.md "wikilink")・[孟信](../Page/孟信.md "wikilink")・[宗懍](../Page/宗懍.md "wikilink")・[劉璠](../Page/劉璠.md "wikilink")・[柳遐](../Page/柳遐.md "wikilink")
59. 列传第五十九 隋宗室諸王 -
    [蔡景王整](../Page/楊整.md "wikilink")・[滕穆王瓚](../Page/楊瓚.md "wikilink")・[道宣王嵩](../Page/楊嵩.md "wikilink")・[衛昭王爽](../Page/楊爽.md "wikilink")・[河間王弘](../Page/楊弘.md "wikilink")・[義城公处綱](../Page/楊处綱.md "wikilink")・[離石太守子崇](../Page/楊子崇.md "wikilink")・[房陵王勇](../Page/杨勇_\(隋\).md "wikilink")・[秦王俊](../Page/楊俊.md "wikilink")・[庶人秀](../Page/楊秀.md "wikilink")・[庶人諒](../Page/楊諒.md "wikilink")・[元德太子昭](../Page/楊昭.md "wikilink")・[齐王暕](../Page/楊暕.md "wikilink")・[趙王杲](../Page/楊杲.md "wikilink")
60. 列传第六十 -
    [高熲](../Page/高熲.md "wikilink")・[牛弘](../Page/牛弘.md "wikilink")・[李德林](../Page/李德林.md "wikilink")
61. 列传第六十一 -
    [梁士彦](../Page/梁士彦.md "wikilink")・[元諧](../Page/元諧.md "wikilink")・[虞慶則](../Page/虞慶則.md "wikilink")・[元胄](../Page/元胄.md "wikilink")・[達奚長儒](../Page/達奚長儒.md "wikilink")・[賀婁子幹](../Page/賀婁子幹.md "wikilink")・[史萬歲](../Page/史萬歲.md "wikilink")・[劉方](../Page/劉方.md "wikilink")・[杜彦](../Page/杜彦.md "wikilink")・[周搖](../Page/周搖.md "wikilink")・[独孤楷](../Page/独孤楷.md "wikilink")・[乞伏慧](../Page/乞伏慧.md "wikilink")・[張威](../Page/張威.md "wikilink")・[和洪](../Page/和洪.md "wikilink")・[陰寿](../Page/陰寿.md "wikilink")・[楊義臣](../Page/楊義臣.md "wikilink")
62. 列传第六十二 -
    [劉昉](../Page/劉昉.md "wikilink")・[柳裘](../Page/柳裘.md "wikilink")・[皇甫績](../Page/皇甫績.md "wikilink")・[郭衍](../Page/郭衍.md "wikilink")・[張衡](../Page/張衡_\(隋\).md "wikilink")・[楊汪](../Page/楊汪.md "wikilink")・[裴蘊](../Page/裴蘊.md "wikilink")・[袁充](../Page/袁充.md "wikilink")・[李雄](../Page/李雄_\(隋\).md "wikilink")
63. 列传第六十三 -
    [趙煚](../Page/趙煚.md "wikilink")・[趙芬](../Page/趙芬.md "wikilink")・[王韶](../Page/王韶.md "wikilink")・[元岩](../Page/元岩.md "wikilink")・[宇文恺](../Page/宇文恺.md "wikilink")・[伊婁謙](../Page/伊婁謙.md "wikilink")・[李圆通](../Page/李圆通.md "wikilink")・[郭荣](../Page/郭荣.md "wikilink")・[龐晃](../Page/龐晃.md "wikilink")・[李安](../Page/李安_\(隋\).md "wikilink")・[楊尚希](../Page/楊尚希.md "wikilink")・[張煚](../Page/張煚.md "wikilink")・[蘇孝慈](../Page/蘇孝慈.md "wikilink")・[元寿](../Page/元寿_\(隋\).md "wikilink")
64. 列传第六十四 -
    [段文振](../Page/段文振.md "wikilink")・[来護儿](../Page/来護儿.md "wikilink")・[樊子蓋](../Page/樊子蓋.md "wikilink")・[周羅睺](../Page/周羅睺.md "wikilink")・[周法尚](../Page/周法尚.md "wikilink")・[衛玄](../Page/衛玄.md "wikilink")・[劉权](../Page/劉权.md "wikilink")・[李景](../Page/李景.md "wikilink")・[薛世雄](../Page/薛世雄.md "wikilink")
65. 列传第六十五 -
    [裴政](../Page/裴政.md "wikilink")・[李諤](../Page/李諤.md "wikilink")・[鮑宏](../Page/鮑宏.md "wikilink")・[高構](../Page/高構.md "wikilink")・[荣毗](../Page/荣毗.md "wikilink")・[陸知命](../Page/陸知命.md "wikilink")・[梁毗](../Page/梁毗.md "wikilink")・[柳彧](../Page/柳彧.md "wikilink")・[趙綽](../Page/趙綽.md "wikilink")・[杜整](../Page/杜整.md "wikilink")
66. 列传第六十六 -
    [張定和](../Page/張定和.md "wikilink")・張奫・[麦铁杖](../Page/麦铁杖.md "wikilink")・[权武](../Page/权武.md "wikilink")・[王仁恭](../Page/王仁恭.md "wikilink")・[吐万緒](../Page/吐万緒.md "wikilink")・[董純](../Page/董純.md "wikilink")・[魚俱羅](../Page/魚俱羅.md "wikilink")・[王辯](../Page/王辩_\(隋朝\).md "wikilink")・[陳稜](../Page/陳稜.md "wikilink")・[趙才](../Page/趙才.md "wikilink")
67. 列传第六十七 -
    [宇文述](../Page/宇文述.md "wikilink")・[王世充](../Page/王世充.md "wikilink")
68. 列传第六十八 外戚 -
69. 列传第六十九 儒林上 -
70. 列传第七十 儒林下 -
71. 列传第七十一 文苑 -
    [温子昇](../Page/温子昇.md "wikilink")・[荀济](../Page/荀济.md "wikilink")・[祖鴻勋](../Page/祖鴻勋.md "wikilink")・[李广](../Page/李广_\(北齐\).md "wikilink")・[樊遜](../Page/樊遜.md "wikilink")・[荀士遜](../Page/荀士遜.md "wikilink")・[王褒](../Page/王褒.md "wikilink")・[庾信](../Page/庾信.md "wikilink")・[顔之推](../Page/顔之推.md "wikilink")・[虞世基](../Page/虞世基.md "wikilink")・柳䛒・[許善心](../Page/許善心.md "wikilink")・[李文博](../Page/李文博.md "wikilink")・[明克讓](../Page/明克讓.md "wikilink")・[劉臻](../Page/劉臻.md "wikilink")・[諸葛潁](../Page/諸葛潁.md "wikilink")・[王貞](../Page/王貞.md "wikilink")・[虞綽](../Page/虞綽.md "wikilink")・[王胄](../Page/王胄.md "wikilink")・[庾自直](../Page/庾自直.md "wikilink")・[潘徽](../Page/潘徽.md "wikilink")
72. 列传第七十二 孝行 -
73. 列传第七十三 節義 -
74. 列传第七十四 循吏 -
75. 列传第七十五 酷吏 -
    [于洛侯](../Page/于洛侯.md "wikilink")・[胡泥](../Page/胡泥.md "wikilink")・[李洪之子](../Page/李洪之.md "wikilink")[李神](../Page/李神.md "wikilink")・[张赦提](../Page/张赦提.md "wikilink")・[赵霸](../Page/赵霸.md "wikilink")・[崔暹](../Page/崔暹.md "wikilink")・[邸珍](../Page/邸珍.md "wikilink")・[田式](../Page/田式.md "wikilink")・[燕荣](../Page/燕荣.md "wikilink")・[元弘嗣](../Page/元弘嗣.md "wikilink")・[王文同](../Page/王文同.md "wikilink")
76. 列传第七十六 隐逸 -
    [眭夸](../Page/眭夸.md "wikilink")・[冯亮](../Page/冯亮.md "wikilink")・[郑修](../Page/郑修.md "wikilink")・[崔廓子](../Page/崔廓.md "wikilink")[崔赜](../Page/崔赜.md "wikilink")・[徐则](../Page/徐则.md "wikilink")・[张文诩](../Page/张文诩.md "wikilink")
77. 列传第七十七 艺術上 -
    [晁崇](../Page/晁崇.md "wikilink")・[张深](../Page/张深.md "wikilink")・[殷绍](../Page/殷绍.md "wikilink")・[王早](../Page/王早.md "wikilink")・[耿玄](../Page/耿玄.md "wikilink")・[刘灵助](../Page/刘灵助.md "wikilink")・沙门[灵远](../Page/灵远.md "wikilink")・李顺兴[檀特师](../Page/檀特师.md "wikilink")・[由吾](../Page/由吾.md "wikilink")・[道荣](../Page/道荣.md "wikilink")・[张远游](../Page/张远游.md "wikilink")・[颜恶头](../Page/颜恶头.md "wikilink")・[王春](../Page/王春.md "wikilink")・[信都芳](../Page/信都芳.md "wikilink")・[宋景业](../Page/宋景业.md "wikilink")・[许遵](../Page/许遵.md "wikilink")・[麹绍](../Page/麹绍.md "wikilink")・吴遵世・[赵辅和](../Page/赵辅和.md "wikilink")・[皇甫玉](../Page/皇甫玉.md "wikilink")・[解法选](../Page/解法选.md "wikilink")・[魏宁](../Page/魏宁.md "wikilink")・[綦母怀文](../Page/綦母怀文.md "wikilink")・[张子信](../Page/张子信.md "wikilink")・[陆法和](../Page/陆法和.md "wikilink")・[蒋升](../Page/蒋升.md "wikilink")・[强练](../Page/强练.md "wikilink")・[庾季才子](../Page/庾季才.md "wikilink")[庾质](../Page/庾质.md "wikilink")・[卢太翼](../Page/卢太翼.md "wikilink")・[耿询](../Page/耿询.md "wikilink")・[来和](../Page/来和.md "wikilink")・[萧吉](../Page/萧吉.md "wikilink")・[杨伯丑](../Page/杨伯丑.md "wikilink")・[临孝恭](../Page/临孝恭.md "wikilink")・[刘祐](../Page/刘祐.md "wikilink")・[张胄玄](../Page/张胄玄.md "wikilink")
78. 列传第七十八 艺術下 -
    [周澹](../Page/周澹.md "wikilink")・[李修](../Page/李修.md "wikilink")・[徐謇从孙](../Page/徐謇.md "wikilink")[徐之才](../Page/徐之才.md "wikilink")・[王显](../Page/王显.md "wikilink")・[马嗣明](../Page/马嗣明.md "wikilink")・[姚僧垣](../Page/姚僧垣.md "wikilink")・[褚该](../Page/褚该.md "wikilink")・[许智藏](../Page/许智藏.md "wikilink")・[万宝常](../Page/万宝常.md "wikilink")・[蒋少游](../Page/蒋少游.md "wikilink")・[何稠](../Page/何稠.md "wikilink")
79. 列传第七十九 列女 -
    魏崔览妻封氏・封卓妻刘氏・魏溥妻房氏・胡长命妻张氏・平原女子孙氏・房爱亲妻崔氏・泾州贞女兒氏・姚氏妇杨氏・张洪祁妻刘氏・董景起妻张氏・阳尼妻高氏・史映周妻耿氏・任城国太妃孟氏・苟金龙妻刘氏・贞孝女宗・河东姚氏女・刁思遵妻鲁氏・西魏孙道温妻赵氏・孙神妻陈氏・隋[兰陵公主](../Page/兰陵公主.md "wikilink")・[南阳公主](../Page/南阳公主.md "wikilink")・襄城王恪妃・华阳王楷妃・[谯国夫人洗氏](../Page/冼夫人.md "wikilink")・郑善果母崔氏・孝女王舜・[韩觊妻于氏](../Page/于茂德.md "wikilink")・陆让母冯氏・刘昶女・钟士雄母蒋氏・孝妇覃氏・元务光母卢氏・裴伦妻柳氏・赵元楷妻崔氏
80. 列传第八十
    恩幸・-[王睿](../Page/王睿.md "wikilink")・[王仲兴](../Page/王仲兴.md "wikilink")・[寇猛](../Page/寇猛.md "wikilink")・[赵修](../Page/赵修.md "wikilink")・[茹皓](../Page/茹皓.md "wikilink")・[赵邕](../Page/赵邕.md "wikilink")・[侯刚](../Page/侯刚.md "wikilink")・[徐纥](../Page/徐纥.md "wikilink")・[宗爱](../Page/宗爱.md "wikilink")・[仇洛齐](../Page/仇洛齐.md "wikilink")・[段霸](../Page/段霸.md "wikilink")・[王琚](../Page/王琚.md "wikilink")・[赵默](../Page/赵默.md "wikilink")・[孙小](../Page/孙小.md "wikilink")・[张宗之](../Page/张宗之.md "wikilink")・[剧鹏](../Page/剧鹏.md "wikilink")・[张祐](../Page/张祐.md "wikilink")・[抱嶷](../Page/抱嶷.md "wikilink")・[王遇](../Page/王遇.md "wikilink")・[苻承祖](../Page/苻承祖.md "wikilink")・[王质](../Page/王质.md "wikilink")・[李坚](../Page/李坚.md "wikilink")・[秦松](../Page/秦松.md "wikilink")・[白整](../Page/白整.md "wikilink")・[刘腾](../Page/刘腾.md "wikilink")・[贾粲](../Page/贾粲.md "wikilink")・[杨范](../Page/杨范.md "wikilink")・[成轨](../Page/成轨.md "wikilink")・[王温](../Page/王温.md "wikilink")・[孟栾](../Page/孟栾.md "wikilink")・[平季](../Page/平季.md "wikilink")・[封津](../Page/封津.md "wikilink")・[刘思逸](../Page/刘思逸.md "wikilink")・[张景嵩](../Page/张景嵩.md "wikilink")・[毛暢](../Page/毛暢.md "wikilink")・[郭秀](../Page/郭秀.md "wikilink")・[和士开](../Page/和士开.md "wikilink")・[穆提婆](../Page/穆提婆.md "wikilink")・[高阿那肱](../Page/高阿那肱.md "wikilink")・[韩凤](../Page/韩凤.md "wikilink")・齐诸宦者
81. 列传第八十一 僭偽附庸 -
    [夏](../Page/夏_\(十六国\).md "wikilink")・[燕](../Page/後燕.md "wikilink")・[後秦](../Page/後秦.md "wikilink")・[北燕](../Page/北燕.md "wikilink")・[西秦](../Page/西秦.md "wikilink")・[北涼](../Page/北涼.md "wikilink")・[後梁](../Page/後梁_\(南朝\).md "wikilink")
82. 列传第八十二 -
    [高麗](../Page/高句麗.md "wikilink")・[百济](../Page/百济.md "wikilink")・[新羅](../Page/新羅.md "wikilink")・[勿吉](../Page/勿吉.md "wikilink")・[奚](../Page/奚.md "wikilink")・[契丹](../Page/契丹.md "wikilink")・[室韋](../Page/室韋.md "wikilink")・[豆莫婁](../Page/豆莫婁.md "wikilink")・[地豆干](../Page/地豆于.md "wikilink")・[烏洛侯](../Page/烏洛侯.md "wikilink")・[流求](../Page/流求.md "wikilink")・[倭](../Page/倭.md "wikilink")
83. 列传第八十三 -
    蛮・獠・[林邑](../Page/林邑.md "wikilink")・[赤土](../Page/赤土国.md "wikilink")・[真臘](../Page/真臘.md "wikilink")・[婆利](../Page/婆利.md "wikilink")
84. 列传第八十四 -
    [氐](../Page/氐.md "wikilink")・[吐谷渾](../Page/吐谷渾.md "wikilink")・[宕昌](../Page/宕昌.md "wikilink")・[鄧至](../Page/鄧至.md "wikilink")・[党項](../Page/党項.md "wikilink")・[附国](../Page/附国.md "wikilink")・[稽胡](../Page/稽胡.md "wikilink")
85. 列传第八十五 西域 -
    [鄯善](../Page/鄯善.md "wikilink")・[于闐](../Page/于闐.md "wikilink")・[高昌](../Page/高昌.md "wikilink")・[焉耆](../Page/焉耆.md "wikilink")・[龟茲](../Page/龟茲.md "wikilink")・[烏孫](../Page/烏孫.md "wikilink")・[疏勒](../Page/疏勒.md "wikilink")・[悦般](../Page/悦般.md "wikilink")・[破洛那](../Page/破洛那.md "wikilink")・[粟特](../Page/粟特.md "wikilink")・[波斯](../Page/波斯.md "wikilink")・[大月氏](../Page/大月氏.md "wikilink")・[安息](../Page/安息_\(国家\).md "wikilink")・[条支](../Page/条支.md "wikilink")・[大秦](../Page/大秦.md "wikilink")
86. 列传第八十六 -
    [蠕蠕](../Page/柔然.md "wikilink")・[匈奴宇文莫槐](../Page/宇文莫圭.md "wikilink")・[徒何段就六眷](../Page/段疾陸眷.md "wikilink")・[高車](../Page/高車.md "wikilink")
87. 列传第八十七 -
    [突厥](../Page/突厥.md "wikilink")・[铁勒](../Page/铁勒.md "wikilink")
88. 列传第八十八 序传 - [涼武昭王李暠](../Page/李暠.md "wikilink")

## 參見

  - [北史人物列表](../Page/北史人物列表.md "wikilink")

## 注釋

## 參考書目

  - 《北史識小錄》

## 外部链接

  - [《北史》全文(繁体)](http://www.sidneyluo.net/a/a15/a15.htm)
  - [《北史》全文(简体)](http://www.guoxue.com/shibu/24shi/beishi/beisml.htm)

[Category:纪传体](../Category/纪传体.md "wikilink")
[Category:二十四史](../Category/二十四史.md "wikilink")
[Category:史部正史類](../Category/史部正史類.md "wikilink")
[Category:7世紀書籍](../Category/7世紀書籍.md "wikilink")

1.  《[十七史商榷](../Page/十七史商榷.md "wikilink")》卷66《取北史补北齐书》
2.  《[廿二史札记](../Page/廿二史札记.md "wikilink")》卷13《南北史两国交兵不详载》
3.  《朱子語類》卷第一百三十四