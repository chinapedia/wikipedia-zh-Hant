[Outline_map_of_KAREN_railway_stations.PNG](https://zh.wikipedia.org/wiki/File:Outline_map_of_KAREN_railway_stations.PNG "fig:Outline_map_of_KAREN_railway_stations.PNG")
**北埔車站**位於[台灣](../Page/台灣.md "wikilink")[花蓮縣](../Page/花蓮縣.md "wikilink")[新城鄉](../Page/新城鄉_\(台灣\).md "wikilink")，為[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[北迴線](../Page/北迴線.md "wikilink")、[花蓮臨港線的](../Page/花蓮臨港線.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。

## 車站構造

  - [島式月台一座](../Page/島式月台.md "wikilink")(第二月台)。
  - 設有多卡通刷卡機。

## 利用狀況

  - 目前為[三等站](../Page/臺灣鐵路管理局車站等級#三等站.md "wikilink")。
  - [莒光號](../Page/莒光號列車.md "wikilink")：停靠北上(逆行)653次/6653，南下(順行)642/640、624次。
  - [復興號](../Page/復興號列車.md "wikilink")：停靠南下(順行)688/6688次，北上(逆行)689次。
  - [區間快車](../Page/臺鐵區間車.md "wikilink")：停靠南下(順行)4032次、北上(逆行)4033次。
  - [區間車](../Page/臺鐵區間車.md "wikilink")：皆停靠。
  - 本站開放使用[悠遊卡](../Page/悠遊卡.md "wikilink")、[一卡通及](../Page/一卡通_\(台灣\).md "wikilink")[icash
    2.0付費](../Page/icash.md "wikilink")。

| 年度   | 日均進站旅次（人） |
| ---- | --------- |
| 1998 | 79        |
| 1999 | 85        |
| 2000 | 105       |
| 2001 | 124       |
| 2002 | 94        |
| 2003 | 88        |
| 2004 | 83        |
| 2005 | 87        |
| 2006 | 97        |
| 2007 | 114       |
| 2008 | 131       |
| 2009 | 126       |
| 2010 | 156       |
| 2011 | 284       |
| 2012 | 501       |
| 2013 | 903       |
| 2014 | 658       |
| 2015 | 363       |
| 2016 | 244       |

歷年旅客人次紀錄

## 車站周邊

  - 教育單位

<!-- end list -->

  - 花蓮縣北埔國民小學
  - 花蓮縣新城國民中學
  - [大漢技術學院](../Page/大漢技術學院.md "wikilink")

<!-- end list -->

  - 政府單位

<!-- end list -->

  - [新城鄉公所](../Page/新城鄉_\(台灣\).md "wikilink")
  - 花蓮縣警察局新城分局北埔派出所
  - 花蓮縣消防局北埔分隊
  - 花蓮縣衛生局新城鄉衛生所
  - 花蓮縣新城鄉戶政事務所
  - 花蓮縣新城鄉立圖書館
  - [國軍北埔營區](../Page/國軍.md "wikilink")
  - [行政院](../Page/行政院.md "wikilink")[海巡署](../Page/行政院海岸巡防署.md "wikilink")[海岸巡防總局](../Page/行政院海岸巡防署海岸巡防總局.md "wikilink")[東部地區巡防局](../Page/東部地區巡防局.md "wikilink")[第八三岸巡大隊](../Page/第八三岸巡大隊.md "wikilink")
  - 新城鄉代會

<!-- end list -->

  - 醫院

<!-- end list -->

  - [國軍花蓮總醫院](../Page/國軍花蓮總醫院.md "wikilink")（原八〇五總醫院）

<!-- end list -->

  - 設施

<!-- end list -->

  - [花蓮機場](../Page/花蓮機場.md "wikilink")

  - [中華電信新城服務中心](../Page/中華電信股份有限公司.md "wikilink")

  - [台灣中油北埔站](../Page/台灣中油股份有限公司.md "wikilink")

  - [台灣中油花蓮供油服務中心](../Page/台灣中油股份有限公司.md "wikilink")(北埔庫區)

  - [台電北埔服務所](../Page/台灣電力公司.md "wikilink")

<!-- end list -->

  - 其他

<!-- end list -->

  - [家樂福花蓮店](../Page/家樂福.md "wikilink")
  - [特力和樂花蓮店](../Page/特力和樂.md "wikilink")
  - [85度C新城北埔店](../Page/85度C.md "wikilink")
  - [八方雲集北埔店](../Page/八方雲集.md "wikilink")
  - [7-eleven](../Page/7-eleven.md "wikilink")
  - [全家](../Page/全家.md "wikilink")

## 公車資訊

  - [花蓮客運](../Page/花蓮客運.md "wikilink")：
      - **<font color="red">1126</font>** 花蓮新站－洛韶
      - **<font color="red">1129</font>** 花蓮－太魯閣
      - **<font color="red">1132</font>** 花蓮－崇德
      - **<font color="red">1133
        [台灣好行太魯閣線](../Page/台灣好行.md "wikilink")</font>**
        花蓮－天祥
      - **<font color="red">1141</font>** 花蓮－梨山

## 歷史

  - 1975年7月26日：設站。
  - 1975年9月：開始營運。
  - 2016年3月1日：啟用多卡通刷卡機。

## 攝影集

<File:TRA> BeiPu Station Panorama.jpg|北埔車站全景
[File:Taiwan_BeiPu_Railway_Station.JPG|北埔車站](File:Taiwan_BeiPu_Railway_Station.JPG%7C北埔車站)
[File:Taiwan_BeiPu_Railway_Station_2.JPG|北埔車站月台](File:Taiwan_BeiPu_Railway_Station_2.JPG%7C北埔車站月台)

## 鄰近車站

|-
<small>直通運行至[花蓮車站](../Page/花蓮車站.md "wikilink")</small>

## 參考文獻

## 外部連結

[Category:北迴線車站](../Category/北迴線車站.md "wikilink")
[Category:花蓮縣鐵路車站](../Category/花蓮縣鐵路車站.md "wikilink")
[Category:新城鄉 (台灣)](../Category/新城鄉_\(台灣\).md "wikilink")