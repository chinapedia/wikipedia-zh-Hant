**北大西洋公约**（）是1949年4月4日在美國[华盛顿哥伦比亚特区由](../Page/华盛顿哥伦比亚特区.md "wikilink")12国联合签署的条约，最初的12个缔约国即是[北大西洋公约组织的创始国](../Page/北大西洋公约组织.md "wikilink")。

## 缔约国

[Location_NATO.svg](https://zh.wikipedia.org/wiki/File:Location_NATO.svg "fig:Location_NATO.svg")
[Map_of_NATO_chronological.gif](https://zh.wikipedia.org/wiki/File:Map_of_NATO_chronological.gif "fig:Map_of_NATO_chronological.gif")

<table>
<tbody>
<tr class="odd">
<td><ul>
<li></li>
<li></li>
<li></li>
<li><p>第四共和国</p></li>
<li></li>
<li><p>共和国</p></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li><p>第二共和国</p></li>
<li></li>
<li></li>
</ul></td>
</tr>
</tbody>
</table>

後續签署国包括：

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p>王国（1952年）</p></li>
<li><p>（1952年）</p></li>
<li><p>（1955年）</p></li>
<li><p>王国（1982年）</p></li>
<li><p>共和国（1999年）</p></li>
<li><p>第三共和国（1999年）</p></li>
<li><p>第三共和国（1999年）</p></li>
<li><p>（2004年）</p></li>
</ul></td>
<td><ul>
<li><p>（2004年）</p></li>
<li><p>（2004年）</p></li>
<li><p>（2004年）</p></li>
<li><p>（2004年）</p></li>
<li><p>（2004年）</p></li>
<li><p>（2004年）</p></li>
<li><p>（2009年）</p></li>
<li><p>（2009年）</p></li>
</ul></td>
</tr>
</tbody>
</table>

## 历史

在[美国参议院](../Page/美国参议院.md "wikilink")，北大西洋公约于1949年7月21日以82票对13票通过。

当1990年[两德统一后](../Page/两德统一.md "wikilink")，前東德區域自动成为北大西洋公约组织的涵蓋區。

在2008年4月的首脑会议上，[克罗地亚和](../Page/克罗地亚.md "wikilink")[阿尔巴尼亚被正式邀请加入北约](../Page/阿尔巴尼亚.md "wikilink")，他们在较后都签署了该条约，并于2009年4月1日正式加入北约。

该份条约的关键部分是第五条款（）要求簽署国承诺“针对任何一个成员国发动的武装攻击应被视为是对全体成员国发动的武装攻击”，该条款的用意主要是防范苏联对西欧的大规模武装攻击，但直到冷战结束，第五条款一直都没有被动用过，它被首次动用，是在2001年9月11日对[世界贸易中心和](../Page/世界贸易中心.md "wikilink")[五角大楼袭击事件后发起的](../Page/五角大楼.md "wikilink")[鹰援行动](../Page/鹰援行动.md "wikilink")（）。

该条约的第四条款：要求成员国承诺“当北约成员国在领土完整、政治独立或安全受到威胁时，在任何一个成员国的提议下，北约各国将进行紧急磋商”。第四条款的第一次使用是在2003年[伊拉克战争期间由土耳其提出](../Page/伊拉克战争.md "wikilink")；第二次使用是在2012年，因为一架土耳其[F-4军机在土耳其叙利亚边境被](../Page/F-4.md "wikilink")[叙利亚防空系统击落再次由土耳其提出](../Page/叙利亚.md "wikilink")。

## 另见

  - [北大西洋公约组织](../Page/北大西洋公约组织.md "wikilink")
  - [华沙条约组织](../Page/华沙条约组织.md "wikilink")

## 外部链接

  - [北大西洋公约官方文件](http://www.nato.int/cps/en/natolive/official_texts_17120.htm)

[Category:冷战条约](../Category/冷战条约.md "wikilink")
[Category:北大西洋公约组织](../Category/北大西洋公约组织.md "wikilink")
[Category:政治宪章](../Category/政治宪章.md "wikilink") [Category:Treaties
concluded in 1949](../Category/Treaties_concluded_in_1949.md "wikilink")
[Category:Treaties entered into force in
1949](../Category/Treaties_entered_into_force_in_1949.md "wikilink")
[Category:比利时条约](../Category/比利时条约.md "wikilink")
[Category:加拿大条约](../Category/加拿大条约.md "wikilink")
[Category:捷克共和国条约](../Category/捷克共和国条约.md "wikilink")
[Category:丹麦条约](../Category/丹麦条约.md "wikilink")
[Category:爱沙尼亚条约](../Category/爱沙尼亚条约.md "wikilink")
[Category:冰岛条约](../Category/冰岛条约.md "wikilink")
[Category:拉脱维亚条约](../Category/拉脱维亚条约.md "wikilink")
[Category:立陶宛条约](../Category/立陶宛条约.md "wikilink")
[Category:卢森堡条约](../Category/卢森堡条约.md "wikilink")
[Category:挪威条约](../Category/挪威条约.md "wikilink")
[Category:罗马尼亚条约](../Category/罗马尼亚条约.md "wikilink")
[Category:斯洛伐克条约](../Category/斯洛伐克条约.md "wikilink")
[Category:斯洛文尼亚条约](../Category/斯洛文尼亚条约.md "wikilink")
[Category:土耳其条约](../Category/土耳其条约.md "wikilink")
[Category:英国条约](../Category/英国条约.md "wikilink")
[Category:美国条约](../Category/美国条约.md "wikilink")
[Category:法国条约](../Category/法国条约.md "wikilink")
[Category:德国条约](../Category/德国条约.md "wikilink")
[Category:波兰条约](../Category/波兰条约.md "wikilink")
[Category:意大利条约](../Category/意大利条约.md "wikilink")
[Category:葡萄牙条约](../Category/葡萄牙条约.md "wikilink")
[Category:西班牙条约](../Category/西班牙条约.md "wikilink")
[Category:荷兰条约](../Category/荷兰条约.md "wikilink")
[Category:匈牙利条约](../Category/匈牙利条约.md "wikilink")
[Category:保加利亚条约](../Category/保加利亚条约.md "wikilink")
[Category:希腊条约](../Category/希腊条约.md "wikilink")
[Category:克罗地亚条约](../Category/克罗地亚条约.md "wikilink")
[Category:阿尔巴尼亚条约](../Category/阿尔巴尼亚条约.md "wikilink")