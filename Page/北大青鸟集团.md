\-{H|zh-hant:軟件;zh-hans:软件;}-
[Beida_jade_bird_group.png](https://zh.wikipedia.org/wiki/File:Beida_jade_bird_group.png "fig:Beida_jade_bird_group.png")
**北大青鸟集团**是[北京大学下属的一个大型国有控股高科技企业集团](../Page/北京大学.md "wikilink")，它成立于1994年11月19日，它由计算机专家、北京大学计算机系的[杨芙清院士创办](../Page/杨芙清.md "wikilink")，前身是中国国家支持的“计算机软件重大科技攻关项目**青鸟工程**”。至2000年底，北大青鸟集团已经拥有\[1\]其中，[北大青鸟环宇科技](../Page/北大青鸟环宇科技.md "wikilink")（）是首家在[香港创业板上市的大陆公司](../Page/香港.md "wikilink")。集團的主控股公司為**北京北大青鳥軟件**。

北大青鸟集团曾經擁有[北京天桥北大青鸟科技](../Page/北京天桥北大青鸟科技.md "wikilink")
（，現更名[信達地產](../Page/信達地產.md "wikilink")）。

## 北大软件中心、公司

**北大软件工程国家工程研究中心**于1996年在国家支持下成立，依托北京大学。

**北京北大软件工程发展有限公司**成立于2000年12月，它由[863计划](../Page/863计划.md "wikilink")“青鸟CASE系统”起家，业务陆续扩展到[系统集成](../Page/系统集成.md "wikilink")、和行业软件和[中间件开发](../Page/中间件.md "wikilink")。\[2\]

## 北大青鸟APTECH

[Jb-aptech_logo.png](https://zh.wikipedia.org/wiki/File:Jb-aptech_logo.png "fig:Jb-aptech_logo.png")
**北大青鸟APTECH**（全称**北京阿博泰克北大青鸟信息技术有限公司**），是由北大青鸟集团与印度阿博泰克公司（）合资的一家IT职业教育公司。它成立于2001年1月，是中国最大的IT培训公司。\[3\]

中国各地的北大青鸟培训公司均为北大青鸟APTECH许可加盟经营的外包公司。

## 子公司

  - [北京市北大青鳥軟件系統](../Page/北京市北大青鳥軟件系統.md "wikilink")
  - [北京北大青鳥](../Page/北京北大青鳥.md "wikilink")
  - [北京市北大宇環微電子系統工程](../Page/北京市北大宇環微電子系統工程.md "wikilink")
  - [北大青鸟环宇科技](../Page/北大青鸟环宇科技.md "wikilink")

## 參股公司

  - [博雅软件](../Page/博雅软件.md "wikilink") (北京北大青鸟国际软件技术)\[4\]\[5\]

## 参考

## 外部链接

  - [北大青鸟集团官方网站](http://www.jadebird.com.cn/)
  - [北京北大软件工程发展有限公司官方网站](http://www.beidasoft.com/)
  - [北大青鸟企业站](http://qy.bdqn.cn/)
  - [北大青鸟官方网站](http://www.bdqn.cn/)

[Category:中国民营企业](../Category/中国民营企业.md "wikilink")
[Category:北京大學校辦產業](../Category/北京大學校辦產業.md "wikilink")
[Category:中國軟件公司](../Category/中國軟件公司.md "wikilink")
[Category:北京市公司](../Category/北京市公司.md "wikilink")
[Category:1994年成立的公司](../Category/1994年成立的公司.md "wikilink")

1.
2.
3.
4.  [青鸟软件股份有限公司在京创立](http://www.boyasoftware.com/article-199.html)
5.  [青鸟软件正式更名博雅软件](http://www.boyasoftware.com/article-359.html)(cnet科技资讯网)