**北山龍屬**（屬名：*Peishansaurus*）是種[草食性恐龍](../Page/草食性.md "wikilink")，生存於晚[白堊紀](../Page/白堊紀.md "wikilink")[坎潘階](../Page/坎潘階.md "wikilink")，約9750萬年前到6500萬年前。

北山龍是由Birger
Bohlin在1953年所敘述、命名，化石發現於[中國](../Page/中國.md "wikilink")[新疆](../Page/新疆.md "wikilink")，包含頜部碎片與牙齒。[模式種是](../Page/模式種.md "wikilink")**薄甲北山龍**（*P.
philemys*），屬名是以新疆[北山山脈](../Page/甘肅北山山脈.md "wikilink")，為名。目前不確定牠們為[甲龍類或是](../Page/甲龍類.md "wikilink")[厚頭龍類](../Page/厚頭龍類.md "wikilink")。

## 參考資料

  - [Dinosauria Translation and Pronunciation
    Guide](https://web.archive.org/web/20070928010616/http://www.dinosauria.com/dml/names/dinop.htm)
  - \[<https://web.archive.org/web/20181003215106/http://www.thescelosaurus.com/ornithischia>*Thescelosaurus*\!
    page containing information about *Peishansaurus*\]

## 外部链接

  - [恐龙.NET](https://web.archive.org/web/20170527222928/http://dinosaur.net.cn/)——中国古生物门户网站。

[Category:鳥臀目](../Category/鳥臀目.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")