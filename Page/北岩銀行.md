[Northern_Rock_House_1.JPG](https://zh.wikipedia.org/wiki/File:Northern_Rock_House_1.JPG "fig:Northern_Rock_House_1.JPG")
**北岩銀行**（****，）是一家[英國](../Page/英國.md "wikilink")[銀行](../Page/銀行.md "wikilink")，於1997年在[倫敦證券交易所上市](../Page/倫敦證券交易所.md "wikilink")，2000年加入[富時100指數成份股](../Page/富時100指數.md "wikilink")，但於2007年12月24日從富時100指數中除名\[1\]。北岩銀行是英國第五大貸款機構，佔英國貸款市場的18.3%。

## 擠提事件

一般銀行的資金來自客戶的存款，而北岩銀行則主要從其它金融機構借貸，然後再轉借給買房子的人。北岩銀行是英國[次級按揭市場的貸款大戶](../Page/次級按揭.md "wikilink")，給購房者的貸款多達十足房價，有時超出購房者收入的5-6倍，還給那些購房出租的投資者大量貸款。

2007年，北岩銀行因[次按風暴導致國際融資市場出現停滯](../Page/2007年美國次級房屋信貸風暴.md "wikilink")，難以取得資金支持其業務，為了維持銀行體系的穩定，[英倫銀行決定介入](../Page/英倫銀行.md "wikilink")，注資北岩銀行以解決流動資金短缺的危機\[2\]。

9月14日北岩銀行股票價格一度下跌三成，出現[擠兌現象](../Page/擠兌.md "wikilink")，各分行門前排滿等待提款的儲戶。大批儲戶向北岩銀行各網點或通過網絡服務提款，由於登入人數太多，銀行網站的[伺服器無法負荷](../Page/伺服器.md "wikilink")，網上存取款服務嚴重受阻。

9月17日股價再跌四成\[3\]，儲戶擠兌持續。因为根据英國自2001年制定的《存款補償計劃》，只有存款2,000[英鎊以下的儲戶在銀行破產時可收回全部存款](../Page/英鎊.md "wikilink")，超出2,000英鎊的31,000英鎊，儲戶能夠收回90%。此时，財政大臣[阿利斯泰尔·达林](../Page/戴理德.md "wikilink")（Alistair
Darling）提出保證北岩銀行儲戶的全部存款，才导致挤兑暂时得以缓解。\[4\]。

9月18日在存款獲得保證後，北岩銀行擠兌稍為舒緩，期間儲戶共提走20億英鎊的存款，相當於北岩銀行存款的8%，而股價亦回升16%。10月19日北岩銀行董事長里德利（Matt
Ridley）辭職下台，由前保柏（BUPA）及[渣打銀行董事長桑德森](../Page/渣打銀行.md "wikilink")（Bryan
Sanderson）接任。

<File:Birmingham> Northern Rock bank run
2007.jpg|[伯明罕分行門口的](../Page/伯明罕.md "wikilink")[擠兌人潮](../Page/擠兌.md "wikilink")
<File:Northern> Rock
Queue.jpg|[布赖顿分行門口排隊的](../Page/布赖顿-霍夫.md "wikilink")[擠兌人龍](../Page/擠兌.md "wikilink")

### 收購建議

2007年10月12日[維珍集團組建包括](../Page/維珍集團.md "wikilink")[香港第一東方投資集團](../Page/香港.md "wikilink")（First
Eastern Investment）、美國[AIG保險公司](../Page/美國國際集團.md "wikilink")、私人股權公司WL
Ross及倫敦的投機性投資公司Toscafund等的財團建議收購北岩銀行。

2011年11月18日[維珍金融](../Page/維珍金融.md "wikilink")（Virgin
Money）以7.47億英鎊(約合12億美元)收購北岩銀行。

### 國有化

2008年2月18日英國財政大臣-{zh-cn:阿利斯泰尔‧达林; zh-tw:達林;
zh-hk:戴理德;}-宣佈北岩銀行將被暫時國有化\[5\]，英國著名的保險公司勞合社（Lloyd's
of London）的前舵手桑德勒（Ron Sandler）接掌國有化後的北岩\[6\]。

## 贊助

北岩銀行贊助多個當地體育俱乐部及項目，包括[纽卡斯尔联](../Page/纽卡斯尔联足球俱乐部.md "wikilink")（Newcastle
United，[足球隊](../Page/足球.md "wikilink")）\[7\]、（Newcastle
Falcons，球隊）、（Newcastle
Eagles，[籃球隊](../Page/籃球.md "wikilink")）、及木球會，職業[高爾夫球員](../Page/高爾夫球.md "wikilink")（Paul
Eales）及「Northern Rock Cyclone」自行車節\[8\]。

[-{zh:纽卡斯尔联足球俱乐部;zh-hans:纽卡斯尔联足球俱乐部;zh-hk:紐卡素足球隊;zh-tw:紐卡斯爾聯足球俱樂部;}-的贊助始於](../Page/纽卡斯尔联足球俱乐部.md "wikilink")2003年，直到2010年才結束。

## 資料來源

## 參考資料

  - [Timeline: Northern Rock bank
    crisis](http://news.bbc.co.uk/2/hi/business/7007076.stm)

## 外部連結

  - [北岩銀行](http://www.northernrock.co.uk)

[category:英國銀行](../Page/category:英國銀行.md "wikilink")

[Category:次級房屋信貸危機](../Category/次級房屋信貸危機.md "wikilink")

1.  [Northern Rock drops from
    FTSE 100](http://news.bbc.co.uk/2/hi/business/7139241.stm)
2.  [Liquidity Support Facility for Northern Rock
    plc](http://www.bankofengland.co.uk/publications/news/2007/090.htm)
3.  [Northern Rock besieged by
    savers](http://newsvote.bbc.co.uk/2/hi/business/6997765.stm)
4.  [Northern Rock deposits
    guaranteed](http://news.bbc.co.uk/2/hi/business/6999615.stm)
5.  [Northern Rock to be
    nationalised](http://news.bbc.co.uk/2/hi/business/7249575.stm)
6.  [Rock recovery is Sandler's
    goal](http://news.bbc.co.uk/2/hi/business/7249858.stm)
7.  [Magpies extend sponsorship
    deal](http://news.bbc.co.uk/1/hi/england/tyne/3643581.stm)
8.  [northernrockcyclone.co.uk](http://northernrockcyclone.co.uk)