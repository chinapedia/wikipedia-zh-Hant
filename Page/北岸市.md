**北岸市**（[英文](../Page/英文.md "wikilink")：**North Shore
City**；通稱**北岸**、**北港**或**北奧克蘭**）是[新西蘭數個在](../Page/新西蘭.md "wikilink")[奧克蘭大區內獲官方認可的](../Page/奧克蘭大區.md "wikilink")[城市之一](../Page/城市.md "wikilink")，也是該國第四大的城市。奧克蘭集合城市([conurbation](../Page/:en:conurbation.md "wikilink"))常被市民視為單一的城市，但北岸市市民最不接受這看法。

北岸市被南面的[Waitemate港和東面的](../Page/:en:Waitemata_Harbour.md "wikilink")[豪拉基湾](../Page/豪拉基湾.md "wikilink")[Rangitoto海峽環繞](../Page/:en:Rangitoto_Channel.md "wikilink")，經[奥克兰](../Page/奥克兰.md "wikilink")[港港大橋連接Waitemate港以南的](../Page/港港大橋.md "wikilink")[奧克蘭市](../Page/奧克蘭市.md "wikilink")。[Devonport是](../Page/:en:Devonport,_New_Zealand.md "wikilink")[纽西蘭皇家海軍主基地的所在地](../Page/纽西蘭皇家海軍.md "wikilink")，[奧克蘭理工大學](../Page/奧克蘭理工大學.md "wikilink")[AUT在北岸市Akoranga校區](../Page/AUT.md "wikilink")
設有公共衛生學院以及教育學院,[梅西大學Albany分校也設於北岸市](../Page/梅西大學.md "wikilink")[Albany](../Page/:en:Albany,_New_Zealand.md "wikilink")。

北岸市議會坐落於[Takapuna](../Page/:en:Takapuna.md "wikilink")。北岸市劃爲三個行政區：港區、北區和中區，每個行政區再細分為兩個社區委員會。

內城區包括：\*Milford、Takapuna、貝爾蒙、Devonport、貝斯沃特、[Northcote](../Page/:en:Northcote,_New_Zealand.md "wikilink")、[Birkenhead](../Page/:en:Birkenhead,_New_Zealand.md "wikilink")、海布瑞、希爾科瑞斯特、[Glenfield](../Page/:en:Glenfield,_New_Zealand.md "wikilink")、懷勞谷、West
Lake以及弗瑞斯特山。外城區包括：伯克代爾、North_Harbour、[North
Harbour](../Page/:en:North_Harbour,_New_Zealand.md "wikilink")、[Albany](../Page/:en:Albany,_New_Zealand.md "wikilink")、\*Long
Bay、\*托貝、\*懷艾克、\*Brown's
Bay、\*羅特西灣、\*墨累灣、\*[Mairangi灣](../Page/:en:Mairangi_Bay,_New_Zealand.md "wikilink")、\*坎貝爾灣和卡斯特灣。標有星號的地區，與其他少數豪拉基灣海岸
的海灘，總稱為[East Coast](../Page/East_Coast.md "wikilink")。

東岸海灣有幾處[纽西蘭最昂貴的房地產](../Page/纽西蘭.md "wikilink")。從Takapuna
Beach北至Milford，這段海岸通稱為「百萬元[英里](../Page/英里.md "wikilink")」，多数房產价值數百萬[纽西兰元](../Page/纽西兰元.md "wikilink")，因為那裏擁有優美的海灘、受歡迎的學校和購物中心。

穿梭北岸市不難，但是，往來奧克蘭市和奧克蘭港灣大橋，往往要面臨嚴重的交通擠塞。另一條貫穿西部城區的路線，在高峰時間也水泄不通。大奧克蘭地區的交通問題，是全國和地區政府商討的重點。沿南部[高速公路伸延的新](../Page/高速公路.md "wikilink")[巴士線道](../Page/巴士.md "wikilink")，連同Albany新公園和遊樂設施的工程，將於2006年下旬落成，預料可改善擠塞情況。

近五年來[Albany大為發展](../Page/:en:Albany,_New_Zealand.md "wikilink")。這昔日寧靜的郊區，已成為北岸市的商業中心。一些零售商在該地區設立了大型商店，預期將持續發展、擴充。同時，廉價房屋落成，無數的農地也變成了小型城區，內有許多設計相若的住宅。因此，[Albany吸引了數億元的投資金額](../Page/Albany.md "wikilink")。

北岸市繼續向北擴展。Orewa鎮區北距Takapuna25[公里](../Page/公里.md "wikilink")，昔日曾是渡假勝地；現在，北部高速公路連接了Orewa鎮區，該镇最終将与向北發展的北岸市连为一体。

## 姐妹城市

  - [中華民國](../Page/中華民國.md "wikilink")－[臺中市](../Page/臺中市.md "wikilink")

  - [中華人民共和國](../Page/中華人民共和國.md "wikilink")－[青島市](../Page/青島市.md "wikilink")

## 外部連結

  - [北岸市議會網站](https://web.archive.org/web/20060808214531/http://www.northshorecity.govt.nz/)
  - [Wikivoyage
    北岸市頁面](https://en.wikivoyage.org/wiki/North_Shore_\(New_Zealand\))

[category:紐西蘭城市](../Page/category:紐西蘭城市.md "wikilink")

[Category:奧克蘭 (紐西蘭)](../Category/奧克蘭_\(紐西蘭\).md "wikilink")