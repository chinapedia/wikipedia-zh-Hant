**北岛**（[英语](../Page/英语.md "wikilink")：**North
Island**；[毛利语](../Page/毛利语.md "wikilink")：'''Te Ika-a-Māui
'''）与[南岛组成](../Page/南岛_\(新西兰\).md "wikilink")[新西兰的主体](../Page/新西兰.md "wikilink")，是[世界第十四大](../Page/世界.md "wikilink")[岛屿](../Page/岛屿.md "wikilink")。

  - 面積：113,729 km²
  - 人口：3,596,200 (June 2016)

76%的新西兰人口居住于北岛，而其中大半则居住在最大的城市[奥克兰](../Page/奥克兰_\(新西兰\).md "wikilink")，及首都[惠灵顿](../Page/惠灵顿.md "wikilink")。北岛亦是著名的旅游胜地，著名的旅游胜地包括[島灣](../Page/島灣.md "wikilink")，[陶波湖和](../Page/陶波湖.md "wikilink")[羅托魯瓦等等](../Page/羅托魯瓦.md "wikilink")。

[毛利人传说北岛和](../Page/毛利人.md "wikilink")[南岛来自于一位叫毛伊的先祖和他弟弟在海上钓鱼的故事](../Page/南岛_\(新西兰\).md "wikilink")。
南岛为毛伊当时所乘坐的船只，北岛则是毛伊钓上的大鱼，[毛利语对北岛的名称](../Page/毛利语.md "wikilink")**Te-Ika-a-Maui**（毛伊的鱼）则由此而来。

## 主要城鎮

  - [奥克兰](../Page/奥克兰_\(新西兰\).md "wikilink")
  - [劍橋](../Page/劍橋_\(紐西蘭\).md "wikilink")
  - [科羅曼德](../Page/科羅曼德.md "wikilink")
  - [福斯頓](../Page/福斯頓.md "wikilink")
  - [吉斯本](../Page/吉斯本.md "wikilink")
  - [漢米爾頓](../Page/漢米爾頓.md "wikilink")
  - [哈斯丁斯](../Page/哈斯丁斯.md "wikilink")
  - [納皮爾](../Page/納皮爾.md "wikilink")
  - [新普利茅斯](../Page/新普利茅斯.md "wikilink")
  - [北帕默斯頓](../Page/北帕默斯頓.md "wikilink")
  - [羅托魯瓦](../Page/羅托魯瓦.md "wikilink")
  - [陶波](../Page/陶波.md "wikilink")
  - [陶蘭加](../Page/陶蘭加.md "wikilink")
  - [惠灵顿](../Page/惠灵顿.md "wikilink")

## 地理特點

  - [雷因格角](../Page/雷因格角.md "wikilink")
  - [島灣](../Page/島灣.md "wikilink")
  - [庫克海峽](../Page/庫克海峽.md "wikilink")
  - [陶波湖](../Page/陶波湖.md "wikilink")
  - [艾格蒙特山國家公園](../Page/艾格蒙特山國家公園.md "wikilink")
  - [東格里羅國家公園](../Page/東格里羅國家公園.md "wikilink")
  - [懷卡托河](../Page/懷卡托河.md "wikilink")
  - [懷波瓦考里松林](../Page/考里松.md "wikilink")
  - [懷托摩螢火蟲洞](../Page/懷托摩螢火蟲洞.md "wikilink")
  - [塔乌玛塔法卡塔尼哈娜可阿乌阿乌欧塔玛提亚坡凯费努啊奇塔娜塔胡](../Page/塔乌玛塔法卡塔尼哈娜可阿乌阿乌欧塔玛提亚坡凯费努啊奇塔娜塔胡.md "wikilink")

## 参考资料

[Category:新西兰岛屿](../Category/新西兰岛屿.md "wikilink")