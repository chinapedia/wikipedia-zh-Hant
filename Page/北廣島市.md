**北廣島市**（）位於[北海道](../Page/北海道.md "wikilink")[石狩振興局東南部](../Page/石狩振興局.md "wikilink")。緊鄰[札幌市東南側](../Page/札幌市.md "wikilink")。原本是農村，隨著札幌的開發，成為札幌的[通勤城鎮](../Page/通勤城市.md "wikilink")。

1884年（明治17年），[廣島縣的移民到此定居開墾](../Page/廣島縣.md "wikilink")，當時名為「廣島村」，以示不忘家鄉，1995年改制為市的時候，命名為北廣島市。每年[廣島市都會招待北廣島的中小學生參加](../Page/廣島市.md "wikilink")[原子彈被爆的紀念活動](../Page/廣島原爆.md "wikilink")。廣島市遭受嚴重自然災害時，北廣島市民都會捐款相助。2018年3月，職棒[日本火腿隊決定在北廣島市興建新球場](../Page/日本火腿隊.md "wikilink")，部份球迷因而誤以為球隊要離開北海道遷往廣島縣，因此北廣島市政府製作印有「別、別搞錯了\!
是北海道啦！」標語及歷史人物圖像的[團扇做為宣傳](../Page/團扇.md "wikilink")\[1\]。

在[廣島縣](../Page/廣島縣.md "wikilink")[山縣郡有同樣以北廣島為名的](../Page/山縣郡.md "wikilink")[北廣島町](../Page/北廣島町.md "wikilink")。

## 地理

  - 東部為石狩平原，緊鄰札幌市的西側為[台地](../Page/台地.md "wikilink")。
  - 北部有跨越札幌市和[江別市轄區的野幌森林公園](../Page/江別市.md "wikilink")。

## 歷史

  - 1873年：[中山久藏從](../Page/中山久藏.md "wikilink")[千歲郡](../Page/千歲郡.md "wikilink")[島松村轉移到此地進行開墾](../Page/島松村.md "wikilink")。
  - 1884年：[和田郁次郎從](../Page/和田郁次郎.md "wikilink")[廣島縣段原村](../Page/廣島縣.md "wikilink")（現在的廣島市[南區段原](../Page/南區_\(廣島市\).md "wikilink")）帶領25戶開墾者集體前來進行開墾，因此命名為廣島。
  - 1894年：廣島村從月寒村分割獨立設置，並設置廣島村戶長役場。
  - 1902年：成為二級村
  - 1921年：成為一級村。
  - 1968年：改制為廣島町。
  - 1996年9月1日：改制為市，但為避免與原本的[廣島市混淆](../Page/廣島市.md "wikilink")，經過公開徵求命名後，決定改名為北廣島市。
  - 2004年：人口突破六萬人。
  - 2018年3月：日本職棒[日本火腿隊決定在北廣島市興建新球場](../Page/日本火腿隊.md "wikilink")，預定2023年完工遷入\[2\]。

## 行政

現任市長：[上野正三](../Page/上野正三.md "wikilink")

## 交通

### 機場

  - [新千歲機場](../Page/新千歲機場.md "wikilink")（位於[千歲市](../Page/千歲市.md "wikilink")）
      - 轉搭[千歲線的快速列車](../Page/千歲線.md "wikilink")，可在20分鐘抵達[北廣島車站](../Page/北廣島車站.md "wikilink")。

### 鐵路

  - [北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")
      - [千歲線](../Page/千歲線.md "wikilink")：[北廣島車站](../Page/北廣島車站.md "wikilink")
        - [西之里號誌站](../Page/西之里號誌站.md "wikilink")

### 道路

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>高速道路</dt>

</dl>
<ul>
<li><a href="../Page/道央自動車道.md" title="wikilink">道央自動車道</a>：<a href="../Page/北廣島交流道.md" title="wikilink">北廣島交流道</a></li>
</ul>
<dl>
<dt><a href="../Page/一般國道.md" title="wikilink">一般國道</a></dt>

</dl>
<ul>
<li><a href="../Page/國道36號.md" title="wikilink">國道36號</a></li>
<li><a href="../Page/國道274號.md" title="wikilink">國道274號</a></li>
</ul>
<dl>
<dt><a href="../Page/主要地方道.md" title="wikilink">主要地方道</a></dt>

</dl>
<ul>
<li>北海道道46號江別惠庭線</li>
</ul></td>
<td><dl>
<dt><a href="../Page/道道.md" title="wikilink">道道</a></dt>

</dl>
<ul>
<li>北海道道371號北廣島停車場線</li>
<li>北海道道790號仁別大曲線</li>
<li>北海道道1080號栗山北廣島線</li>
<li>北海道道1147號大曲工業區美しが丘線</li>
<li>北海道道1148號札幌惠庭自行車道線</li>
</ul></td>
</tr>
</tbody>
</table>

## 觀光景點

  - 轄區有八個[高爾夫球場](../Page/高爾夫球場.md "wikilink")，合計面積超過轄區總面積的一成。
      - 札幌高爾夫球俱樂部 輪厚コース
      - 札幌國際鄉村俱樂部 島松コース
      - 札幌不二羅亞爾高爾夫球俱樂部
      - 克拉克鄉村俱樂部
      - 札幌後樂園鄉村俱樂部
      - 廣濟堂札幌鄉村俱樂部
      - 札幌北廣島王子高爾夫球場
      - 太陽公園札幌高爾夫球場
  - [舊島松驛遞所](../Page/舊島松驛遞所.md "wikilink")：國家指定史蹟，[克拉克博士紀念碑](../Page/克拉克博士.md "wikilink")，寒地稻作發祥地，是1877年4月16日[札幌農業中等專科學校](../Page/札幌農業學校.md "wikilink")（[北海道大學的前身](../Page/北海道大學.md "wikilink")）的首任教頭[威廉·史密司·克拉克](../Page/威廉·史密司·克拉克.md "wikilink")（William
    Smith Clark）與前來送行學生和學校職員們分手的地方。
  - [和平燈公園](../Page/和平燈公園.md "wikilink")：來自廣島市[廣島和平紀念公園分火所點燃的和平燈](../Page/廣島和平紀念公園.md "wikilink")。
  - 休憩森林：位於市中心西側，設有原野設施，有部份區域因有蜂巢而禁止進入。
  - 自然森林露營區：鄰近國道36號。
  - 竹山高原溫泉

## 教育

### 大學

  - [道都大學](../Page/道都大學.md "wikilink")（學校法人北海道櫻井產業學園）

### 高等學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>道立北海道北廣島高等學校</li>
<li>道立北海道北廣島西高等學校</li>
</ul></td>
<td><ul>
<li>札幌日本大學高等學校（學校法人<a href="../Page/札幌日本大學.md" title="wikilink">札幌日本大學學園</a>）</li>
</ul></td>
</tr>
</tbody>
</table>

### 中學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>北廣島市立東部中學校</li>
<li>北廣島市立西部中學校</li>
<li>北廣島市立大曲中學校</li>
<li>北廣島市立西之里中學校</li>
</ul></td>
<td><ul>
<li>北廣島市立廣葉中學校</li>
<li>北廣島市立綠陽中學校</li>
<li>札幌日本大學中學校（學校法人札幌日本大學學園）</li>
</ul></td>
</tr>
</tbody>
</table>

### 小學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>北廣島市立東部小學校</li>
<li>北廣島市立西部小學校</li>
<li>北廣島市立大曲小學校</li>
<li>北廣島市立西之里小學校</li>
<li>北廣島市立廣葉小學校</li>
</ul></td>
<td><ul>
<li>北廣島市立若葉小學校</li>
<li>北廣島市立高台小學校</li>
<li>北廣島市立綠陽小學校</li>
<li>北廣島市立北之台小學校</li>
<li>北廣島市立大曲東小學校</li>
</ul></td>
</tr>
</tbody>
</table>

### 特殊學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>札幌養護學校共榮分校</li>
</ul></td>
<td><ul>
<li>道立北海道白樺高等養護學校</li>
</ul></td>
</tr>
</tbody>
</table>

## 姊妹市・友好都市

### 日本

  - [東廣島市](../Page/東廣島市.md "wikilink")（[廣島縣](../Page/廣島縣.md "wikilink")）：1980年7月19日締結\[3\]

### 海外

  - [薩克屯](../Page/薩克屯.md "wikilink")（[加拿大](../Page/加拿大.md "wikilink")
    [薩克其萬省](../Page/薩克其萬省.md "wikilink")）

## 本地出身的名人

  - [大澤逸美](../Page/大澤逸美.md "wikilink")：[演員](../Page/演員.md "wikilink")，出身於北廣島高校等。

  - ：演員，出身於北廣島西高等學校。

  - [梅村禮](../Page/梅村禮.md "wikilink")：[桌球選手](../Page/桌球.md "wikilink")，出身於廣葉中學校，曾出賽2004年[雅典奧林匹克運動會女子單打和雙打](../Page/雅典奧林匹克運動會.md "wikilink")。

## 參考資料

## 外部連結

1.  [北廣島市不在廣島縣\!
    市府宣傳團扇標語引爆話題](https://www.asahichinese-f.com/cool_japan/life/11738961)
2.  [日職棒火腿隊新球場
    落腳北廣島市](http://www.cna.com.tw/news/aspt/201803260273-1.aspx)
3.