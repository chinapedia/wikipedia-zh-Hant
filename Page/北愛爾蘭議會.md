**北愛爾蘭議會**為[北愛爾蘭](../Page/北愛爾蘭.md "wikilink")[分權自治政府之](../Page/權力下放.md "wikilink")[立法機關](../Page/立法.md "wikilink")。\[1\]有权能於[英国国会成文法外另行立法](../Page/英国国会.md "wikilink")，並負責任命各[北愛爾蘭行政委員會](../Page/北愛爾蘭行政委員會.md "wikilink")（北愛自治政府）內閣要員。坐落於[貝爾法斯特](../Page/貝爾法斯特.md "wikilink")[維多利亞選區的](../Page/維多利亞選區.md "wikilink")[議會大廈內](../Page/議會大廈_\(北愛爾蘭\).md "wikilink")。

## 陣營

根據[貝爾法斯特協議](../Page/貝爾法斯特協議.md "wikilink")，北愛爾蘭議會内的政黨必須從「聯合派」（即支持北愛留在英國的所謂親英派）、「民族派」（支持和[愛爾蘭共和國統一的派系](../Page/愛爾蘭共和國.md "wikilink")）或「其他」三大陣營中擇一加入，而重要議案必須獲得跨陣營支持。

## 參考資料

## 外部連結

  - [Official website](http://www.niassembly.gov.uk)
  - [The St Andrews'
    Agreement](https://web.archive.org/web/20080623085217/http://www.standrewsagreement.org/)
    The latest attempt to restore devolution to Northern Ireland.

[Category:北愛爾蘭政府](../Category/北愛爾蘭政府.md "wikilink")
[Category:英国议会](../Category/英国议会.md "wikilink")
[Category:一院制立法機構](../Category/一院制立法機構.md "wikilink")

1.