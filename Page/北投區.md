**北投區**是[臺灣](../Page/臺灣.md "wikilink")[臺北市最北邊](../Page/臺北市.md "wikilink")、以及面積第二大的行政區，著名的[北投溫泉及關渡風景區也位在此區](../Page/北投溫泉.md "wikilink")。\[1\]\[2\]臺灣温泉史源頭在北投，於1896年由[日本](../Page/日本.md "wikilink")[大阪商人平田源吾在此設立臺灣第一家温泉旅館](../Page/大阪.md "wikilink")「[天狗庵](../Page/天狗庵.md "wikilink")」。\[3\]。其東側以磺溪上接[中山樓右側山脊至](../Page/中山樓_\(陽明山\).md "wikilink")[馬槽橋沿山溝](../Page/馬槽.md "wikilink")、南側以[基隆河與](../Page/基隆河.md "wikilink")[士林區相望](../Page/士林區.md "wikilink")，北側鄰[新北市](../Page/新北市.md "wikilink")[三芝區與](../Page/三芝區.md "wikilink")[金山區](../Page/金山區_\(台灣\).md "wikilink")，西側與新北市[淡水區為鄰](../Page/淡水區.md "wikilink")。

此區[自然資源豐富](../Page/自然資源.md "wikilink")，[陽明山國家公園即位於此區及](../Page/陽明山國家公園.md "wikilink")[士林區](../Page/士林區.md "wikilink")。此區醫療資源亦豐，許多醫療中心、醫護院校均位於本區，是故未來本區欲開發之[士林北投科技園區將以生技醫藥產業為主軸](../Page/士林北投科技園區.md "wikilink")。2010年[臺北市政府舉辦花卉博覽會而制定區花](../Page/臺北市政府.md "wikilink")，北投區為[櫻花](../Page/櫻花.md "wikilink")，臺北市政府民政局也特別製作區花版門牌。

## 歷史

[Scenery_of_Hsin_Beitou.jpg](https://zh.wikipedia.org/wiki/File:Scenery_of_Hsin_Beitou.jpg "fig:Scenery_of_Hsin_Beitou.jpg")
[臺灣民主&獨立運動先驅蘇東啟(左)與蘇洪月嬌(中)於1956臺北北投Taiwanese_Democracy_Activists_Su,_Tong-ch'i_(left)_&_Hung,_Yüeh-chiao_(center).jpg](https://zh.wikipedia.org/wiki/File:臺灣民主&獨立運動先驅蘇東啟\(左\)與蘇洪月嬌\(中\)於1956臺北北投Taiwanese_Democracy_Activists_Su,_Tong-ch'i_\(left\)_&_Hung,_Yüeh-chiao_\(center\).jpg "fig:臺灣民主&獨立運動先驅蘇東啟(左)與蘇洪月嬌(中)於1956臺北北投Taiwanese_Democracy_Activists_Su,_Tong-ch'i_(left)_&_Hung,_Yüeh-chiao_(center).jpg")先驅[蘇東啟](../Page/蘇東啟.md "wikilink")（左）與[蘇洪月嬌](../Page/蘇洪月嬌.md "wikilink")（中）等在1956年合影於北投，兩人所抱者有一人係次女[蘇治芬](../Page/蘇治芬.md "wikilink")\]\]

  - 昔日為平埔族[北投社](../Page/北投社.md "wikilink")（[巴賽語](../Page/巴賽語.md "wikilink")：，意為「[巫女](../Page/巫女.md "wikilink")」）之地，[康熙中葉後](../Page/康熙.md "wikilink")，漸有[漢人](../Page/漢人.md "wikilink")[屯墾當地](../Page/屯墾.md "wikilink")。
  - 1697年[郁永河來北投開採硫磺](../Page/郁永河.md "wikilink")，紀錄於「[裨海紀遊](../Page/裨海紀遊.md "wikilink")」一書中。
  - 1875年北投隸屬於淡水縣芝蘭二堡。
  - 1920年臺灣地方改制，實行街-{庄}-制，北投隸[台北州](../Page/臺北州.md "wikilink")[七星郡](../Page/七星郡.md "wikilink")，設「[北投-{庄}-](../Page/北投庄.md "wikilink")」。
  - 1913年[臺灣總督府為迎接皇太子裕仁](../Page/臺灣總督府.md "wikilink")，臺北廳廳長[井村大吉興建](../Page/井村大吉.md "wikilink")[北投溫泉公共浴場](../Page/北投溫泉公共浴場.md "wikilink")，同時整建成[北投公園](../Page/北投公園.md "wikilink")。
  - 1946年[臺北縣政府成立](../Page/新北市政府.md "wikilink")，隸屬臺北縣七星區北投鎮，1947年七星區併入淡水區，改隸臺北縣淡水區北投鎮
  - 1949年時改隸[草山管理局北投鎮](../Page/陽明山管理局.md "wikilink")。
  - 1967年7月1日臺北市升格院轄市時未併入。直到1968年7月1日隨臨近之景美鎮、南港鎮、木柵鄉、內湖鄉、暨同屬陽明山管理局之士林鎮等鄉鎮劃入臺北市，成為北投區。
  - 1988年7月16日[臺鐵](../Page/臺灣鐵路管理局.md "wikilink")[淡水線因改建捷運停駛](../Page/臺鐵淡水線.md "wikilink")。
  - 1997年[臺北捷運](../Page/臺北捷運.md "wikilink")[淡水線及](../Page/台北捷運淡水線.md "wikilink")[新北投支線通車](../Page/台北捷運新北投支線.md "wikilink")，其中[唭哩岸站站名譯音自](../Page/唭哩岸站.md "wikilink")[凱達格蘭族語之](../Page/凱達格蘭族.md "wikilink")。
  - 2006年[北投空中纜車動工](../Page/北投空中纜車.md "wikilink")，原預計2007年7月通車，但開工後因為爆發弊案及一些開發爭議，也受到當地居民反對。目前確定停建，[臺北市政府與廠商協調中](../Page/臺北.md "wikilink")。
  - 2016年日式老車站[新北投站火車站預計回歸到](../Page/新北投站.md "wikilink")[七星公園](../Page/七星公園.md "wikilink")，但是確切位置仍在討論中。

[Beitou_Hotspring_Museum_2015.jpg](https://zh.wikipedia.org/wiki/File:Beitou_Hotspring_Museum_2015.jpg "fig:Beitou_Hotspring_Museum_2015.jpg")）\]\]

**北投區**位於[臺灣](../Page/臺灣.md "wikilink")[臺北市最北邊](../Page/臺北市.md "wikilink")，與[士林區用](../Page/士林區.md "wikilink")[淡水河下游來劃分它是全臺灣面積第十二大的區](../Page/淡水河.md "wikilink")，著名的[陽明山國家公園及](../Page/陽明山國家公園.md "wikilink")[臺北榮民總醫院也在該區](../Page/臺北榮民總醫院.md "wikilink")。北投區的名字是因為早期[巴賽族認為](../Page/巴賽族.md "wikilink")[地熱谷是女巫施法才會冒煙](../Page/地熱谷.md "wikilink")，所以就用[巴賽語裡的女巫](../Page/巴賽語.md "wikilink")，念起來是「PATAW（巴島）」與北投的[臺語發音很像](../Page/臺灣閩南語.md "wikilink")，故命名為「北投」。

**歷年所屬行政區列表**

|           |               |
| --------- | ------------- |
| 起訖年份      | 行政區           |
| 1901～1920 | 臺北廳士林支廳北投區    |
| 1920～1940 | 臺北州七星郡北投-{庄}- |
| 1940～1945 | 臺北州七星郡北投街     |
| 1945～1947 | 臺北縣七星區北投鎮     |
| 1947～1949 | 臺北縣淡水區北投鎮     |
| 1949～1950 | 草山管理局北投鎮      |
| 1950～1968 | 陽明山管理局北投鎮     |
| 1968～1974 | 臺北市陽明山管理局北投區  |
| 1974～     | 臺北市北投區        |

## 行政區域

全區共分為42里，並編列有七個次分區，次分區及各里名稱如下：

關渡次分區：八仙、豐年、稻香、桃源、一德、關渡共六里。

陽明山次分區：泉源、湖山、湖田共三里。

舊北投次分區：奇岩、清江、中央、大同 共四里。

新北投次分區：長安、溫泉、林泉、中心 共四里。

石牌次分區：建民、文林、石牌、福興、榮光、榮華、裕民、振華、永欣、永和、洲美共十一里。

唭哩岸次分區：永明、東華、吉利、吉慶、尊賢、立賢、立農 共七里。

大屯次分區：中庸、開明、中和、大屯、智仁、秀山、文化 共七里。

## 人口

## 歷任首長

### 北投鎮鎮長時期

|     |         |   |          |                                  |                                        |
| --- | ------- | - | -------- | -------------------------------- | -------------------------------------- |
| 屆   | 上任      |   | 卸任       | 鎮長                               | 備註                                     |
| 第一任 | 1946年1月 | ～ | 1946年4月  | [周碧](../Page/周碧.md "wikilink")   | 官派                                     |
| 第二任 | 1947年1月 | ～ | 1947年3月  | [鄭如松](../Page/鄭如松.md "wikilink") | 鎮民代表[間接選舉](../Page/間接選舉.md "wikilink") |
| 第三任 | 1947年3月 | ～ | 1948年4月  | [陳振榮](../Page/陳振榮.md "wikilink") | 鎮民代表[間接選舉](../Page/間接選舉.md "wikilink") |
| 第四任 | 1948年4月 | ～ | 1948年12月 | [吳著錄](../Page/吳著錄.md "wikilink") | 民選                                     |
| 第五任 | 1949年1月 | ～ | 1951年6月  | [廖樹](../Page/廖樹.md "wikilink")   | 民選                                     |
| 第六任 | 1951年7月 | ～ | 1953年6月  | [廖樹](../Page/廖樹.md "wikilink")   | 民選                                     |
| 第七任 | 1953年7月 | ～ | 1956年6月  | [廖樹](../Page/廖樹.md "wikilink")   | 民選                                     |
| 第八任 | 1956年7月 | ～ | 1960年1月  | [廖樹](../Page/廖樹.md "wikilink")   | 民選                                     |
| 第九任 | 1960年1月 | ～ | 1964年2月  | [李德財](../Page/李德財.md "wikilink") | 民選                                     |
| 第十任 | 1964年3月 | ～ | 1968年6月  | [李德財](../Page/李德財.md "wikilink") | 民選                                     |

### 北投區區長時期

|      |         |   |         |     |    |
| ---- | ------- | - | ------- | --- | -- |
| 屆    | 上任      |   | 卸任      | 區長  | 備註 |
| 第一任  | 1968.7月 | ～ | 1977.7月 | 張道炎 | 官派 |
| 第二任  | 1977.7月 | ～ | 1982.8月 | 許培聰 | 官派 |
| 第三任  | 1982.8月 | ～ | 1990.3月 | 林錫可 | 官派 |
| 第四任  | 1990.3月 | ～ | 1993.9月 | 葉良增 | 官派 |
| 第五任  | 1993.9月 | ～ | 1996.2月 | 盤治郎 | 官派 |
| 第六任  | 1996.2月 | ～ | 1998.2月 | 楊勝雄 | 官派 |
| 第七任  | 1998.2  | ～ | 1999.3  | 陳壽寶 | 官派 |
| 第八任  | 1999.3  | ～ | 2000.7  | 劉錦興 | 官派 |
| 第九任  | 2000.7  | ～ | 2007.10 | 張義芳 | 官派 |
| 第十任  | 2007.10 | ～ | 2016.4  | 李美麗 | 官派 |
|      |         |   |         |     |    |
| 第十一任 | 2016.4  | ～ | 現任      | 陳銘國 | 官派 |
|      |         |   |         |     |    |

## 醫療機構

  - [臺北榮民總醫院](../Page/臺北榮民總醫院.md "wikilink")
  - [振興醫院](../Page/振興醫院.md "wikilink")
  - [三軍總醫院北投分院](../Page/三軍總醫院北投分院.md "wikilink")（前國軍北投醫院，俗稱818醫院）
  - [辜公亮基金會](../Page/辜公亮基金會.md "wikilink")[和信醫院](../Page/和信醫院.md "wikilink")
  - [關渡醫院](../Page/關渡醫院.md "wikilink")
  - [國立中國醫藥研究所](../Page/國立中國醫藥研究所.md "wikilink")
  - [臺北市立聯合醫院](../Page/臺北市立聯合醫院.md "wikilink")（陽明院區）附設北投門診部（位於北投行政中心5樓）

## 教育機構

### 大專院校

  - [國立陽明大學](../Page/國立陽明大學.md "wikilink")
  - [國立臺北藝術大學](../Page/國立臺北藝術大學.md "wikilink")
  - [國立臺北護理健康大學](../Page/國立臺北護理健康大學.md "wikilink")（校本部）
  - [臺北城市科技大學](../Page/臺北城市科技大學.md "wikilink")
  - [馬偕醫護管理專科學校](../Page/馬偕醫護管理專科學校.md "wikilink")
  - [國防大學（政治作戰學院、管理學院）](../Page/國防大學_\(臺灣\).md "wikilink")

### 高級中等學校

  - [臺北市立中正高級中學](../Page/臺北市立中正高級中學.md "wikilink")（原為**臺北市立士林高中**）
  - [臺北市立復興高級中學](../Page/臺北市立復興高級中學.md "wikilink")
  - [臺北市私立薇閣高級中學](../Page/臺北市私立薇閣高級中學.md "wikilink")
  - [臺北市私立十信高級中學](../Page/臺北市私立十信高級中學.md "wikilink")
  - [臺北市私立奎山實驗高級中學](../Page/臺北市私立奎山實驗高級中學.md "wikilink")
  - [臺北市私立惇敘高級工商職業學校](../Page/臺北市私立惇敘高級工商職業學校.md "wikilink")

### 國民中學

  - [臺北市立北投國民中學](../Page/臺北市立北投國民中學.md "wikilink")
  - [臺北市立明德國民中學](../Page/臺北市立明德國民中學.md "wikilink")
  - [臺北市立桃源國民中學](http://www.tyjh.tp.edu.tw/)
  - [臺北市私立薇閣高級中學附設國民中學](../Page/臺北市私立薇閣高級中學.md "wikilink")
  - [臺北市立石牌國民中學](../Page/臺北市立石牌國民中學.md "wikilink")
  - [臺北市私立奎山實驗高級中學](../Page/臺北市私立奎山實驗高級中學.md "wikilink")
  - [臺北市立新民國民中學](http://www.hmjh.tp.edu.tw/)
  - [臺北市立關渡國民中學](http://www.ktjhs.tp.edu.tw/)

### 國民小學

  - [臺北市北投區北投國民小學](http://www.ptes.tp.edu.tw/)
  - [臺北市北投區文化國民小學](http://www.whps.tp.edu.tw/)
  - [臺北市北投區石牌國民小學](http://www.spps.tp.edu.tw/)
  - [臺北市立立農國民小學](../Page/臺北市立立農國民小學.md "wikilink")
  - [臺北市北投區明德國民小學](http://www.mdes.tp.edu.tw/)
  - [臺北市北投區洲美國民小學](http://www.cmps.tp.edu.tw/)
  - [臺北市北投區桃源國民小學](http://www.tyues.tp.edu.tw/)
  - [臺北市北投區湖山國民小學](http://www.hses.tp.edu.tw/)
  - [臺北市北投區逸仙國民小學](http://www.ysps.tp.edu.tw/)
  - [臺北市私立薇閣國民小學](http://www.wgps.tp.edu.tw/)
  - [臺北市北投區大屯國民小學](http://www.dtps.tp.edu.tw/)
  - [臺北市北投區文林國民小學](http://www.wles.tp.edu.tw/)
  - [臺北市北投區立農國民小學](http://www.lnes.tp.edu.tw/)
  - [臺北市私立奎山實驗高級中學附屬國民小學](../Page/臺北市私立奎山實驗高級中學.md "wikilink")
  - [臺北市北投區泉源國民小學](http://www.cyps.tp.edu.tw/)
  - [臺北市北投區清江國民小學](https://web.archive.org/web/20070805104606/http://www.cjps.tp.edu.tw/)
  - [臺北市北投區湖田國民小學](http://www.htes.tp.edu.tw/)
  - [臺北市北投區義方國民小學](http://www.yfes.tp.edu.tw/)
  - [臺北市北投區關渡國民小學](http://www.kdps.tp.edu.tw/)

### 社會教育

  - [北投法鼓山社會大學](../Page/北投法鼓山社會大學.md "wikilink")、北投社區大學

## 臺北市立圖書館（北投區）

  - 北投分館
  - 稻香分館
  - 石牌分館
  - 清江分館
  - 吉利分館
  - 永明民眾閱覽室
  - 秀山民眾閱覽室（含秀山書閣）

## 商業設施

  - [家樂福](../Page/家樂福.md "wikilink")-北投店
  - [好市多](../Page/好市多.md "wikilink")-北投店
  - [燦坤3C](../Page/燦坤3C.md "wikilink")-北投新旗艦店
  - [燦坤3C](../Page/燦坤3C.md "wikilink")-新北投店
  - [燦坤3C](../Page/燦坤3C.md "wikilink")-石牌店
  - [全國電子](../Page/全國電子.md "wikilink")
  - [聚廠樂活運動俱樂部](../Page/聚廠樂活運動俱樂部.md "wikilink")

## 政府（民間）機構與設施

  - [北投垃圾焚化廠](../Page/北投垃圾焚化廠.md "wikilink")-又稱「北投焚化爐」，位於北投區洲美街，由臺北市政府環境保護局管理，主要負責垃圾收受及處理、燃燒空氣、底渣、飛灰、廢金屬、蒸汽、凝結水、廢氣處理、廢水流程等等工作。廠內並附設「旋轉餐廳」（摘星樓-星月360度旋轉景觀餐廳，位於廠內120公尺高處），為全球首座煙囪上之觀景旋轉餐廳，提供民眾用餐與觀看360度窗外美景的地方。
  - 北投區農會
  - [臺北市立浩然敬老院](../Page/臺北市立浩然敬老院.md "wikilink")

## 知名企業

  - [華碩電腦](../Page/華碩電腦.md "wikilink")：總部設立於北投區立德路15號
  - [和碩聯合科技](../Page/和碩聯合科技.md "wikilink")：總部設立於北投區立功街96號
  - [華擎科技](../Page/華擎科技.md "wikilink")：總部設立於北投區中央南路二段37號
  - [大愛電視](../Page/大愛電視.md "wikilink")：總部設立於北投區立德路2號
  - [大同公司北投廠](../Page/大同公司.md "wikilink")：位於北投區承德路七段，為北投區重要地標，已廢廠。
  - [台灣森永製菓](../Page/台灣森永製菓.md "wikilink")：總部設立於北投區中央南路二段
  - [一之鄉蜂蜜蛋糕](../Page/一之鄉.md "wikilink")：總公司設立於北投區北投路一段
  - 原動力文化發展事業有限公司：位於北投區明德路，以宗教心靈音樂為主的唱片公司。（如知名的[伊藤佳代宗教心靈系列音樂](../Page/伊藤佳代.md "wikilink")）

## 旅遊

[硫磺谷地熱景觀區.jpg](https://zh.wikipedia.org/wiki/File:硫磺谷地熱景觀區.jpg "fig:硫磺谷地熱景觀區.jpg")地熱景觀區。\]\]
[龍鳳谷地熱景觀區.jpg](https://zh.wikipedia.org/wiki/File:龍鳳谷地熱景觀區.jpg "fig:龍鳳谷地熱景觀區.jpg")地熱景觀區。\]\]
[梅庭南側.jpg](https://zh.wikipedia.org/wiki/File:梅庭南側.jpg "fig:梅庭南側.jpg")。\]\]
[Peitou_Hot_spring_Museum-Taipei-Taiwan-P1010094.JPG](https://zh.wikipedia.org/wiki/File:Peitou_Hot_spring_Museum-Taipei-Taiwan-P1010094.JPG "fig:Peitou_Hot_spring_Museum-Taipei-Taiwan-P1010094.JPG")
[缩略图](https://zh.wikipedia.org/wiki/File:Tatung_Co._Peitou_Plant_20090823.jpg "fig:缩略图")

### 名勝

  - [北投溫泉](../Page/北投溫泉.md "wikilink")
  - [紗帽山溫泉風景區](../Page/紗帽山.md "wikilink")
  - [行天宮](../Page/行天宮.md "wikilink")（行天宮北投分宮）
  - [石牌福星宮](../Page/石牌福星宮.md "wikilink")
  - [照明淨寺](../Page/照明淨寺.md "wikilink")（舊稱：北投情人廟）
  - [凱達格蘭文化館](../Page/凱達格蘭文化館.md "wikilink")
  - [關渡宮](../Page/關渡宮.md "wikilink")
  - [北投慈-{后}-宮](../Page/北投慈后宮.md "wikilink")
  - [北投法藏寺](../Page/北投法藏寺.md "wikilink")
  - 北投中和禪寺&章嘉活佛舍利塔
  - [地熱谷](../Page/地熱谷.md "wikilink")
  - [善光寺](../Page/善光寺_\(台北市\).md "wikilink")
  - [臺北市立圖書館](../Page/臺北市立圖書館.md "wikilink")[北投分館](../Page/臺北市立圖書館北投分館.md "wikilink")（[臺北市立圖書館](../Page/臺北市立圖書館.md "wikilink")[北投分館](../Page/臺北市立圖書館北投分館.md "wikilink")、[北投公園以及](../Page/北投公園.md "wikilink")[北投溫泉博物館最近的車站爲](../Page/北投溫泉博物館.md "wikilink")*[捷運新北投站](../Page/新北投站.md "wikilink")*。）

### 古蹟

  - [周氏節孝坊](../Page/周氏節孝坊.md "wikilink") ，三級古蹟，1861年建立。
  - [北投溫泉浴場](../Page/北投溫泉浴場.md "wikilink")（[北投溫泉博物館](../Page/北投溫泉博物館.md "wikilink")），三級古蹟，1913年建立。
  - [陽明山中山樓](../Page/陽明山中山樓.md "wikilink")，直轄市定古蹟，1965年建立。
  - [石牌漢番界碑](../Page/石牌漢番界碑.md "wikilink")
  - [草山教師研習中心](../Page/草山教師研習中心.md "wikilink")（原[草山公共浴場](../Page/草山公共浴場.md "wikilink")），直轄市定古蹟，1929年建立。
  - [草山水道系統](../Page/草山水道系統.md "wikilink")，直轄市定古蹟，約1928年建立。
  - [前日軍衛戍醫北投分院](../Page/前日軍衛戍醫北投分院.md "wikilink")，直轄市定古蹟，1910年代年建立。
  - [長老教會北投教堂](../Page/長老教會北投教堂.md "wikilink")，直轄市定古蹟，1912年建立。
  - [吟松閣](../Page/吟松閣.md "wikilink")，直轄市定古蹟，1934年建立。
  - [北投普濟寺](../Page/北投普濟寺.md "wikilink")，直轄市定古蹟，1905年建立。
  - [北投臺灣銀行舊宿舍](../Page/北投台灣銀行舊宿舍.md "wikilink")，直轄市定古蹟，1920年代年建立。
  - [北投不動明王石窟](../Page/北投不動明王石窟.md "wikilink")，直轄市定古蹟，1925年建立。
  - [北投文物館](../Page/北投文物館.md "wikilink")（原佳山旅館），直轄市定古蹟，約1925年建立。
  - [北投穀倉](../Page/北投穀倉.md "wikilink")，直轄市定古蹟，1938年建立。
  - [章嘉活佛舍利塔塔蹟](../Page/章嘉活佛舍利塔塔蹟.md "wikilink")，19世章嘉活佛與達賴、班禪、哲布尊丹巴並稱四大活佛，是佛教界重要之領導人物，曾封為蒙旂宣化使，任國大代表、總統府資政，是西藏黃教格魯派活佛中，1949年後唯一來臺定居者。章嘉活佛推動藏傳佛教在臺發展與安定當時人心有顯著貢獻。1957年3月4日大師因病捨報示寂。1960年代興建的舍利塔，屬印度佛塔形，造型具藏式舍利塔之重要特徵。目前塔內舍利子已取出，塔上供有活佛照片及碑文等。\[4\]

### 公園

  - [陽明山國家公園](../Page/陽明山國家公園.md "wikilink")
  - [新北投公園](../Page/新北投公園.md "wikilink")（手機遊戲《[精靈寶可夢GO](../Page/精靈寶可夢GO.md "wikilink")》最熱門的抓寶聖地）
  - [關渡自然公園](../Page/關渡自然公園.md "wikilink")
  - [貴子坑水土保持園區](../Page/貴子坑水土保持園區.md "wikilink")

### 步道

  - 忠義山步道\[5\]
  - 下青礐步道
  - 郵政訓練所步道
  - 中正山親山步道\[6\]
  - 軍艦岩步道\[7\]
  - 水尾巴拉卡步道
  - 泉源里紗帽步道

### 自然保留區

  - [北投石自然保留區](../Page/北投石自然保留區.md "wikilink")

### 休閒景點

  - [北投櫻花隧道](../Page/北投櫻花隧道.md "wikilink")：位於北投復興三路，北部熱門賞櫻秘境。\[8\]

### 剪影

Image:Beitou District Office, Taipei City 20050702.jpg|北投區行政中心
Image:Hsin Beitou-1.jpg|[新北投](../Page/新北投.md "wikilink")（在前景可看見泉源路）
Image:Taipei City Hospital Beitou Clinic 20060221.jpg|新北投（左為中和街）
Image:Hsin Beitou-3.jpg|新北投（右為中山路，可通往[地熱谷](../Page/地熱谷.md "wikilink")）
Image:Beitou Park bus stop boards on Kuang Ming Road
20060221.jpg|新北投（光明路） Image:Beitou Post Office and Danan
Bus AH-900 20060221.jpg|北投（光明路北投郵局） Image:Hsin Beitou-4.jpg|新北投（新北投購物中心）
Image:Hsin Beitou-5.jpg|北投（遠方為[大屯山](../Page/大屯山.md "wikilink")）
Image:Beitou Market-1.jpg|北投（北投傳統市場）
Image:BeitouLibrary.JPG|[臺北市立圖書館北投分館](../Page/臺北市立圖書館.md "wikilink")

## 宗教信仰與慶典

  - [關渡宮](../Page/關渡宮.md "wikilink")：每年舉辦元宵節燈會
  - [北投行天宮](../Page/北投行天宮.md "wikilink")：提供收驚服務
  - [照明淨寺](../Page/照明淨寺.md "wikilink")：求桃花姻緣知名的北投情人廟
  - [法鼓山北投分院](../Page/法鼓山.md "wikilink")、中華佛教文化館、農禪寺、雲來寺：台灣佛教四大山頭之一
  - 佛教[慈濟人文志業中心](../Page/慈濟人文志業中心.md "wikilink")：台灣佛教四大山頭之一
  - 北投石牌吉慶社區「聖誕巷」：每年聖誕節會舉辦大型慶祝活動
  - 北投石牌福興「聖誕公園」：每年聖誕節會舉辦大型慶祝活動
  - 北投[石牌福星宮](../Page/石牌福星宮.md "wikilink")：每年農立3月媽祖繞境由北投關渡宮至石牌福星宮
  - 石牌福星宮、北投福慶宮、嘎嘮別桃源福德宮：合稱北投區三大福德正神廟
  - [番仔厝保德宮](../Page/番仔厝保德宮.md "wikilink")
  - [屈原宮](../Page/屈原宮.md "wikilink")

## 交通

### 捷運

  - [台北捷運](../Page/台北捷運.md "wikilink")

<!-- end list -->

  - ：[關渡站](../Page/關渡站.md "wikilink") -
    [忠義站](../Page/忠義站_\(臺北市\).md "wikilink") -
    [復興崗站](../Page/復興崗站.md "wikilink") -
    [北投站](../Page/北投站.md "wikilink") -
    [奇岩站](../Page/奇岩站.md "wikilink") -
    [唭哩岸站](../Page/唭哩岸站.md "wikilink") -
    [石牌站](../Page/石牌站_\(臺北市\).md "wikilink") -
    [明德站](../Page/明德站.md "wikilink")

  - ：[新北投站](../Page/新北投站.md "wikilink") -
    [北投站](../Page/北投站.md "wikilink")

### 道路

  - [TW_PHW2a.svg](https://zh.wikipedia.org/wiki/File:TW_PHW2a.svg "fig:TW_PHW2a.svg")[台2甲](../Page/台2線#甲線.md "wikilink")：陽金公路
  - [TW_PHW2b.svg](https://zh.wikipedia.org/wiki/File:TW_PHW2b.svg "fig:TW_PHW2b.svg")[台2乙](../Page/台2線#乙線.md "wikilink")：承德路、大度路、北淡公路
  - [TW_CHW101a.svg](https://zh.wikipedia.org/wiki/File:TW_CHW101a.svg "fig:TW_CHW101a.svg")[市道101號](../Page/市道101號.md "wikilink")：巴拉卡公路
  - [洲美快速道路](../Page/洲美快速道路.md "wikilink")

### 主要道路

**東西向**

  - [中央北路一段](../Page/中央北路_\(北投\).md "wikilink")
  - 中央北路二段
  - 中央北路三段
  - 中央北路四段
  - [TW_PHW2b.svg](https://zh.wikipedia.org/wiki/File:TW_PHW2b.svg "fig:TW_PHW2b.svg")[台2乙](../Page/台2線#乙線.md "wikilink")：[大度路](../Page/大度路.md "wikilink")

**南北向**

  - 行義路
  - [TW_PHW2b.svg](https://zh.wikipedia.org/wiki/File:TW_PHW2b.svg "fig:TW_PHW2b.svg")[台2乙](../Page/台2線#乙線.md "wikilink")：承德路

### 公車、客運

#### [大南汽車](../Page/大南汽車.md "wikilink")

  - 216 新北投-捷運劍潭站
  - 216 (區) 新北投-榮總院區
  - 218 新北投-萬華
  - 218直 新北投-承德路-萬華
  - 218區 新北投-捷運奇岩
  - 223 關渡-青年公園
  - 230 捷運北投站-陽明山
  - 288 兒童新樂園-吳興街
  - 302 關渡宮-萬華
  - 550 關渡宮-洲美運動公園
  - 558 新北投-經中央北路-榮總院區
  - 602 北投-天母（與[中興巴士聯營](../Page/中興巴士.md "wikilink")）
  - 小6 北投-清天宮
  - 小7 北投-嶺腳
  - 小8 捷運石牌站-竹子湖
  - 小9 北投-竹子湖
  - 小14 北投-照明寺
  - 小21 北投-八仙里
  - 小22 捷運北投站-新民泉源路
  - 小23 北投國小-關渡宮
  - 小25 捷運北投站-六窟
  - 小26 北投-頂湖
  - 小36 捷運石牌站-六窟
  - 市民小巴2路 捷運北投-溫泉路
  - 紅35 關渡碼頭-台北藝術大學
  - 紅55 捷運關渡-臺北藝術大學
  - 紅55區 捷運關渡-臺北城市科技大學
  - 內科16 新北投-內湖科學園區
  - 南軟北投 新北投-南港軟體園區
  - 承德幹線 新北投-捷運市政府（原266路，107年4月改號）

#### [淡水客運](../Page/淡水客運.md "wikilink")

  - 631 淡水-北投
  - 682 八里-社子
  - 紅13 八里-捷運關渡站
  - 紅22 八里-捷運關渡站
  - 864 三芝-捷運劍潭站

#### [中興巴士](../Page/中興巴士.md "wikilink")

  - 602 天母-北投（與[大南汽車聯營](../Page/大南汽車.md "wikilink")）
  - 821 三芝-市立天文館
  - 紅12 市立天文館-捷運石牌站

#### [光華巴士](../Page/光華巴士.md "wikilink")

  - 224 天母-天母西路-捷運石牌站
  - 267 天母-捷運大湖公園
  - 紅19 天母-天母北路-捷運石牌站

#### [指南客運](../Page/指南客運.md "wikilink")

  - 308 淡江大學-捷運劍潭
  - 838 泰山-捷運關渡
  - 902 萬芳社區-榮總
  - 757 淡海-北門
  - 1505 淡海-中和
  - 756 淡大-北門

#### [大都會客運](../Page/大都會客運.md "wikilink")

  - 108 陽明山遊園公車
  - 109 萬芳社區-陽明山

假日休閒公車

  - 260（含區間車） 陽明山-（[臺北車站](../Page/臺北車站.md "wikilink")）-東園
  - 277 松德-榮總
  - 敦化幹線 麟光新村-榮總
  - 508區 蘆洲-惇敘工商（與[三重客運聯營](../Page/三重客運.md "wikilink")）
  - 重慶幹線 天母-東園
  - 606 萬芳社區-榮總
  - 紅5 陽明山-捷運劍潭站

#### [欣欣客運](../Page/欣欣客運.md "wikilink")

  - 快速公車 景美女中-榮總

#### [首都客運](../Page/首都客運.md "wikilink")

  - 68 洲美里-捷運劍潭站
  - 536 中國海專-大同之家

#### [三重客運](../Page/三重客運.md "wikilink")

  - 111 新莊-陽明山
  - 508區 蘆洲-惇敘工商（與[大都會客運聯營](../Page/大都會客運.md "wikilink")）
  - 508全 泰山-惇敘工商
  - 645 舊庄-榮總

#### [東南客運](../Page/東南客運.md "wikilink")

  - 612 松德站-惇敘工商

#### [國光客運](../Page/國光客運.md "wikilink")

  - 1801國立護專-基隆

### 未來

#### 輕軌

  - [台北捷運](../Page/台北捷運.md "wikilink")

<!-- end list -->

  - <font color={{台北捷運色彩|社東}}>█</font> </small>東西線（規劃中）：[<font color="#888888">洲美站</font>](../Page/洲美站.md "wikilink")
    - [<font color="#888888">軟橋站</font>](../Page/軟橋站.md "wikilink")
  - <font color={{台北捷運色彩|社南}}>█</font> </small>南北線（規劃中）：[<font color="#888888">洲美站</font>](../Page/洲美站.md "wikilink")
    - [<font color="#888888">五分港站</font>](../Page/五分港站.md "wikilink") -
    [<font color="#888888">頂八仙站</font>](../Page/頂八仙站.md "wikilink") -
    [<font color="#888888">農禪寺站</font>](../Page/農禪寺站.md "wikilink") -
    [<font color="#888888">嗄嘮別站</font>](../Page/嗄嘮別站.md "wikilink")

## 名人

  - [陳信宏](../Page/陳信宏.md "wikilink")：臺灣音樂天團[五月天主唱](../Page/五月天.md "wikilink")。
  - [李宗盛](../Page/李宗盛.md "wikilink")：臺灣歌手音樂創作人。
  - [李威](../Page/李威.md "wikilink")：臺灣藝人。
  - [陳明章](../Page/陳明章.md "wikilink")：臺灣[音樂創作人](../Page/音樂.md "wikilink")。
  - [楊丞琳](../Page/楊丞琳.md "wikilink")：臺灣歌手。
  - [余天](../Page/余天.md "wikilink")：臺灣藝人，曾任立法委員。
  - [許孟哲](../Page/許孟哲.md "wikilink")：臺灣藝人。
  - [施易男](../Page/施易男.md "wikilink")：臺灣藝人，媽媽是臺灣[明霞歌劇團的團長歌仔戲名人](../Page/明霞歌劇團.md "wikilink")[小明明](../Page/小明明.md "wikilink")。
  - [小明明](../Page/小明明.md "wikilink")：臺灣[明霞歌劇團的團長](../Page/明霞歌劇團.md "wikilink")。
  - [黃鴻升](../Page/黃鴻升.md "wikilink")：臺灣藝人。
  - [陳博正](../Page/陳博正.md "wikilink")：臺灣藝人，阿西。
  - [陳建銘](../Page/陳建銘.md "wikilink")：臺北市議員。
  - [陳重文](../Page/陳重文.md "wikilink")：臺北市議員。
  - [吳思瑤](../Page/吳思瑤.md "wikilink")：臺北市立法委員。
  - [洪文棟](../Page/洪文棟.md "wikilink")：前[台北榮總知名骨科醫師](../Page/台北榮總.md "wikilink")、新光集團大股東，1983年、1986年曾任二屆立委，妻子是台灣歌仔戲演員[楊麗花](../Page/楊麗花.md "wikilink")。
  - [楊麗花](../Page/楊麗花.md "wikilink")：臺灣[楊麗花歌仔戲團長](../Page/楊麗花.md "wikilink")，丈夫是前[台北榮總知名骨科醫師](../Page/台北榮總.md "wikilink")，1983年、1986年曾任二屆立委、新光集團大股東[洪文棟](../Page/洪文棟.md "wikilink")。
  - [陳亞蘭](../Page/陳亞蘭.md "wikilink")：臺灣楊麗花歌仔戲團演員和[陳亞蘭歌仔戲團長](../Page/陳亞蘭.md "wikilink")
  - [紀麗如](../Page/紀麗如.md "wikilink")：臺灣楊麗花歌仔戲團演員
  - [潘麗麗](../Page/潘麗麗.md "wikilink")：臺灣楊麗花歌仔戲團演員
  - [許秀年](../Page/許秀年.md "wikilink")：臺灣楊麗花歌仔戲團演員
  - [李如麟](../Page/李如麟.md "wikilink")：臺灣楊麗花歌仔戲團前演員
  - [李宣榕](../Page/李宣榕.md "wikilink")：臺灣歌手和陳亞蘭歌仔戲團演員
  - [杨奇煜](../Page/杨奇煜.md "wikilink")：臺湾团体[Lollipop@F成员](../Page/Lollipop@F.md "wikilink")。
  - [邰智源](../Page/邰智源.md "wikilink")：臺灣主持人、演員，尤其專精於政治戲仿節目。
  - [張菲](../Page/張菲.md "wikilink")：臺灣綜藝節目主持人。
  - [張喬菲](../Page/張喬菲.md "wikilink")：臺灣藝人，半導體千金。
  - [李易](../Page/李易.md "wikilink")：臺灣藝人
  - [管仁健](../Page/管仁健.md "wikilink")：作家、文史工作者。
  - [洪德仁](../Page/洪德仁.md "wikilink")：耳鼻喉科醫生、北投社區醫師、北投社區工作者、北投文化基金會創辦人
    、中華民國社區營造學會理事長。

## 關於北投的更多資訊

  - [臺語歌手](../Page/臺語.md "wikilink")[黃克林以前有首](../Page/黃克林.md "wikilink")[臺語老歌叫做](../Page/臺語.md "wikilink")「倒退嚕」，歌詞中的第一段「耶\~拜請
    拜請拜請 東海岸西海岸 北投西帽山 鶯歌出土炭 草山底勒出溫泉」歌詞中提到的北投西帽山=北投紗帽山。
  - [破壞之王](../Page/破壞之王.md "wikilink")：是[周星馳](../Page/周星馳.md "wikilink")1994年的香港喜劇片，電影內容（台灣國語配音版）第一段中，何金銀送外賣回來，對榮記冰室的老闆說：哎，老闆啊，下次再到「北投」那麼遠的地方，千萬不要再叫我去了！那兒有一個斜坡那麼斜，我差點騎不上去啊！老闆對何金銀說：天將降大任於斯人也，必先勞其筋骨，餓其體膚，苦其心志，這一次啊送去「萬華」→電影中有提到「北投」。

## 相關條目

  - [北投石](../Page/北投石.md "wikilink")
  - [硫磺](../Page/硫磺.md "wikilink")
  - [竹子湖](../Page/竹子湖.md "wikilink")

## 参考文献

## 外部連結

  - [北投區公所](http://www.btdo.taipei.gov.tw/)
  - [北投區戶政事務所](http://www.bthr.taipei.gov.tw/)
  - [北投社區大學](http://www.btcc.org.tw/)
  - [微笑台灣：迷路，是認識北投最美的起點](https://smiletaiwan.cw.com.tw/article/774)

[\*](../Category/北投區.md "wikilink")
[Category:臺北市行政區劃](../Category/臺北市行政區劃.md "wikilink")
[Category:源自台灣原住民語言的台灣地名](../Category/源自台灣原住民語言的台灣地名.md "wikilink")
[Beitou](../Category/1968年建立的行政區劃.md "wikilink")

1.  [陳姿安,
    北投有愛光明在望：最有故事的溫泉廊道, 2008](http://books.google.com.tw/books?id=VhAfQwAACAAJ&dq=%E5%8C%97%E6%8A%95&hl=zh-TW&sa=X&ei=CYPeUL3TPIKNmQXbnIDACw&redir_esc=y)
2.  [戀戀北投溫泉](http://books.google.com.tw/books?id=3URbAAAACAAJ&dq=%E5%8C%97%E6%8A%95&hl=zh-TW&sa=X&ei=QoPeUMuwKZCamQWCm4HYCQ&redir_esc=y)
3.  [協合力-中衛體系提升企業經營綜效
    作者：蘇錦夥](http://books.google.com.tw/books?id=jlpQILx3hSwC&pg=PA254&dq=%E5%8C%97%E6%8A%95+%E6%BA%AB%E6%B3%89&hl=zh-TW&sa=X&ei=iYTeUPCyLerOmAXNg4DICA&redir_esc=y#v=onepage&q=%E5%8C%97%E6%8A%95%20%E6%BA%AB%E6%B3%89&f=false)
4.
5.  [大屯山系-忠義山親山步道
    臺北旅遊網](https://www.travel.taipei/zh-tw/attraction/details/13)
6.  [大屯山系-中正山親山步道
    臺北旅遊網](https://www.travel.taipei/zh-tw/attraction/details/15)
7.  [大屯山系_軍艦岩親山步道
    臺北旅遊網](https://www.travel.taipei/zh-tw/attraction/details/16)
8.  [復興三路櫻花隧道
    臺北旅遊網](https://www.travel.taipei/zh-tw/attraction/details/375)