《**北方回声报**》（[英语](../Page/英语.md "wikilink")：）是[英格兰东北部的一份地区](../Page/英格兰.md "wikilink")[报纸](../Page/报纸.md "wikilink")，总部设于[达灵顿](../Page/达灵顿.md "wikilink")。《北方回声报》由约翰·希斯洛普·贝尔在[皮斯家族的支持下创立](../Page/皮斯家族_\(达灵顿\).md "wikilink")，在1870年1月1日首次出版，立场较当时的其他报纸更倾向[自由主义](../Page/自由主义.md "wikilink")。\[1\]

现在《北方回声报》由Newsquest拥有。根据[发行公信会的统计](../Page/发行公信会.md "wikilink")，《北方回声报》每天的销售数量约为51,000份，\[2\]分别有蒂赛德、北约克郡、北达勒姆、西南达勒姆和达灵顿5个版本。

2006年1月14日，原为大报的《北方回声报》跟随《[泰晤士报](../Page/泰晤士报.md "wikilink")》、《[独立报](../Page/独立报.md "wikilink")》和《[卫报](../Page/卫报.md "wikilink")》等尝试改以[小报的形式出版](../Page/小报.md "wikilink")。\[3\]\[4\]试行1年多后在2007年2月26日完全转为小报。\[5\]

## 参考资料

## 外部链接

  - [《北方回声报》网站](http://www.thenorthernecho.co.uk/)

[Category:英國報紙](../Category/英國報紙.md "wikilink")
[Category:1870年建立的出版物](../Category/1870年建立的出版物.md "wikilink")

1.
2.
3.
4.
5.