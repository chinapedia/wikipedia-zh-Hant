**北梭魚**，又称多骨鱼，屬於[北梭魚科](../Page/北梭魚科.md "wikilink")，是[美國](../Page/美國.md "wikilink")[佛羅里達州](../Page/佛羅里達州.md "wikilink")、[巴哈馬等地釣魚運動中常用的魚種](../Page/巴哈馬.md "wikilink")。

## 概述

北梭魚與[海鏈有很近的親緣關係](../Page/海鏈.md "wikilink")。北梭魚的體色為銀白色，帶著細長的藍色或綠色條紋。嘴部位於吻狀突出上。背鰭和尾鰭的顏色為黑色。北梭魚的平均大小約3至5磅，最大的長席可到76厘米，最重可達6.4公斤。

## 棲地

北梭魚為海水魚，棲習於熱帶海沿岸和島嶼淺水區。最切是發現在[佛羅里達州的](../Page/佛羅里達州.md "wikilink")[比斯坎海灣](../Page/比斯坎海灣.md "wikilink")。牠們生活在近陸地的淺水域。可在有水草生長的白沙地找到牠們。

## 繁殖與習性

北梭魚主要在冬末春初生下子代，主要在淺沙和淤泥處覓食。牠們利用突出的吻狀的嘴掘取生活在海底的[蠕蟲](../Page/蠕蟲.md "wikilink")、[軟體體物](../Page/軟體動物門.md "wikilink")、小蝦、[蟹類](../Page/蟹.md "wikilink")。

## 外部連結

  - [臺灣地區北梭魚科](https://web.archive.org/web/20050308060231/http://fishdb.sinica.edu.tw/2001new/fishfind.asp?outline=065)
    -- [中央研究院台灣魚類資料庫](../Page/中央研究院.md "wikilink")
  - [Albula
    glossodonta](http://www.tfrin.gov.tw/friweb/index.php?func=collection&act=ShowForm&num=936&PHPSESSID=66213033ac89201aa89073be0ccb647e)
    中華民國行政院水產試驗所，水產數位典藏。

[vulpes](../Category/北梭魚屬.md "wikilink")