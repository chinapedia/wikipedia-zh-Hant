**太子**（**γ Ursae Minoris**、縮寫為**Gamma UMi**、**γ
UMi**），也稱為**北極一**（**Pherkad**）\[1\]，是位在北半球[小熊座的](../Page/小熊座.md "wikilink")[拱極星](../Page/拱極星.md "wikilink")。太子與[帝](../Page/北極二.md "wikilink")（小熊座β，Kochab）一起組成"小北斗"尾端的勺口。小北斗[星群組成小熊的尾巴](../Page/星群.md "wikilink")。

## 命名

*小熊座γ*（[拉丁文是](../Page/拉丁名字.md "wikilink")*Gamma Ursae
Minoris*）是[拜耳命名法的恆星名稱](../Page/拜耳命名法.md "wikilink")。

它過去的傳統名稱是*Pherkad*，源自阿拉伯文*فرقد*，簡稱為*farqad*、"calf"，全名為 *aḫfa al
farkadayn*，意思是"昏暗中的兩隻牛犢"，也就是太子（Pherkad）與帝（Kochab），而全稱的傳統上是[勾陳四](../Page/勾陳四.md "wikilink")（小熊座ζ）。太子有時被稱為*Pherkad
Major*，以與天床增二（Pherkad
Minor，小熊座11，小熊座γ1）有所區別。在2016年，[國際天文學聯合會組織下的](../Page/國際天文學聯合會.md "wikilink")（WGSN）\[2\]規範了恆星傳統專有名稱的目錄。WGSN於2016年8月21日批准這顆恒星的名稱*Pherkad*，並收入IAU的恆星目錄中\[3\]。

[恆星光譜分類為A](../Page/恆星光譜.md "wikilink")3II-III型的白色巨星或亮巨星。虽然有资料将其归纳为超巨星，但是对比超巨星它显然太小，亮度只有普通A型超巨星的10%。[視星等](../Page/視星等.md "wikilink")3.00等，距離[地球](../Page/地球.md "wikilink")480[光年](../Page/光年.md "wikilink")。北極一的亮度超過[太陽的](../Page/太陽.md "wikilink")1100倍，质量约是太阳的5倍，直徑超過太陽的15倍\[4\]。北極一在公元前1900年至公元500年間與[北極二一起成为](../Page/北極二.md "wikilink")[北極星](../Page/北極星.md "wikilink")。

## 參考資料

[Category:小熊座](../Category/小熊座.md "wikilink") [小熊座
γ](../Category/拜耳天體.md "wikilink")
[Category:變星](../Category/變星.md "wikilink")
[Category:巨星](../Category/巨星.md "wikilink")
[Category:亮巨星](../Category/亮巨星.md "wikilink")
[太子](../Category/太子_\(星官\).md "wikilink")
[Category:紫微垣](../Category/紫微垣.md "wikilink")

1.

2.

3.
4.