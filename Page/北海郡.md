**北海郡**，[中國古代的](../Page/中國.md "wikilink")[郡](../Page/郡.md "wikilink")。

[西漢置](../Page/西漢.md "wikilink")，[治所在](../Page/治所.md "wikilink")[营陵县](../Page/营陵县.md "wikilink")（今[山東](../Page/山东省.md "wikilink")[昌樂縣東南五十里](../Page/昌樂縣.md "wikilink")）。[东汉时](../Page/东汉.md "wikilink")，徙治[劇縣](../Page/劇縣.md "wikilink")（在今山东省[壽光市東南三十一里](../Page/壽光市.md "wikilink")），後其封其諸王在此，改稱為「[北海國](../Page/北海國.md "wikilink")」。[南齊时](../Page/南齊.md "wikilink")，治所在[都昌县](../Page/都昌县_\(南齐\).md "wikilink")（在今[江蘇省](../Page/江蘇省.md "wikilink")[東海縣東北](../Page/東海縣.md "wikilink")），[南朝宋及](../Page/南朝宋.md "wikilink")[北魏时](../Page/北魏.md "wikilink")，治所在[平寿县](../Page/平寿县.md "wikilink")（在今山东省[潍坊市西南](../Page/潍坊市.md "wikilink")）。[北周置](../Page/北周.md "wikilink")[总管府](../Page/总管府.md "wikilink")。

[隋朝](../Page/隋朝.md "wikilink")[开皇十四年](../Page/开皇.md "wikilink")，废总管府。改為[高陽郡](../Page/高陽郡.md "wikilink")。後廢郡改置[青州](../Page/青州_\(刘宋\).md "wikilink")，[大业初年置北海郡](../Page/大业_\(年号\).md "wikilink")。户十四万七千八百四十五，下辖十县：[益都县](../Page/益都县.md "wikilink")、[临淄县](../Page/临淄县.md "wikilink")、[千乘县](../Page/千乘县.md "wikilink")、[博昌县](../Page/博昌县.md "wikilink")、[寿光县](../Page/寿光县.md "wikilink")、[临朐县](../Page/临朐县.md "wikilink")、都昌县、[北海县](../Page/北海县.md "wikilink")、[营丘县](../Page/营丘县.md "wikilink")、[下密县](../Page/下密县.md "wikilink")。

[唐朝](../Page/唐朝.md "wikilink")[武德四年](../Page/武德.md "wikilink")（621年），復稱青州，[天宝元年](../Page/天宝_\(唐朝\).md "wikilink")（742年）复称北海郡，[乾元时又改為青州](../Page/乾元_\(唐朝\).md "wikilink")，[宋朝稱青州北海郡](../Page/宋朝.md "wikilink")，[金朝改為](../Page/金朝.md "wikilink")[益都府](../Page/益都府.md "wikilink")，即今山東益都縣治。

## 行政区划

### 隋朝

| 县名                               | 现在地名                                | 简介                                                                                                                                                                                             | 备注 |
| -------------------------------- | ----------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -- |
| [益都县](../Page/益都县.md "wikilink") |                                     | 旧置[齐郡](../Page/齐郡.md "wikilink")，开皇初废，大业初置北海郡。有尧山、鋋山。                                                                                                                                          |    |
| [临淄县](../Page/临淄县.md "wikilink") |                                     | 北齐废临淄县及[东安平县](../Page/东安平县.md "wikilink")、[西安县](../Page/西安县_\(西汉\).md "wikilink")。开皇十六年又置临淄及[时水县](../Page/时水县.md "wikilink")。大业初废[高阳县](../Page/高阳县_\(刘宋\).md "wikilink")、时水县入临淄县。有社山、葵丘、牛山、稷山。 |    |
| [千乘县](../Page/千乘县.md "wikilink") |                                     | 旧置[乐安郡](../Page/樂安郡.md "wikilink")，开皇初郡废。                                                                                                                                                      |    |
| [博昌县](../Page/博昌县.md "wikilink") |                                     | 旧曰[乐安县](../Page/乐安县.md "wikilink")，开皇十六年改焉。又十八年析置[新河县](../Page/新河县.md "wikilink")，大业初废入焉。                                                                                                      |    |
| [寿光县](../Page/寿光县.md "wikilink") | 山东省[寿光市](../Page/寿光市.md "wikilink") | 开皇十六年置[闾丘县](../Page/闾丘县.md "wikilink")，大业初废入焉。                                                                                                                                                 |    |
| [临朐县](../Page/临朐县.md "wikilink") |                                     | 旧曰昌国。开皇六年改为逢山，又置[般阳县](../Page/般阳县.md "wikilink")。大业初改曰临朐县，并废般阳县入。有逢山、沂山、穆陵山、大岘山。有汶水、浯水。                                                                                                        |    |
| [都昌县](../Page/都昌县.md "wikilink") |                                     | 有箕山、阜山、白狼山。                                                                                                                                                                                    |    |
| [北海县](../Page/北海县.md "wikilink") |                                     | 旧曰[下密县](../Page/下密县.md "wikilink")，置北海郡。北齐改郡曰高阳郡，开皇初郡废。十六年分置[潍州](../Page/潍州.md "wikilink")，大业初州废，县改名焉。                                                                                         |    |
| [营丘县](../Page/营丘县.md "wikilink") |                                     | 北齐废，开皇十六年复。有丛角山、女节山。                                                                                                                                                                           |    |
| [下密县](../Page/下密县.md "wikilink") |                                     | 北魏曰[胶东县](../Page/胶东县_\(北魏\).md "wikilink")，北齐废。开皇六年复，改为[潍水县](../Page/潍水县.md "wikilink")。大业初改名焉。有铁山。有溉水。                                                                                        |    |
|                                  |                                     |                                                                                                                                                                                                |    |

## 参考资料

  - 《[隋书](../Page/隋书.md "wikilink") 志第二十五 地理中》

[Category:汉朝的郡](../Category/汉朝的郡.md "wikilink")
[Category:刘宋的郡](../Category/刘宋的郡.md "wikilink")
[Category:南齐的郡](../Category/南齐的郡.md "wikilink")
[Category:北魏的郡](../Category/北魏的郡.md "wikilink")
[Category:东魏的郡](../Category/东魏的郡.md "wikilink")
[Category:北齐的郡](../Category/北齐的郡.md "wikilink")
[Category:北周的郡](../Category/北周的郡.md "wikilink")
[Category:隋朝的郡](../Category/隋朝的郡.md "wikilink")
[Category:唐朝的郡](../Category/唐朝的郡.md "wikilink")
[Category:山东的郡](../Category/山东的郡.md "wikilink")
[Category:江苏的郡](../Category/江苏的郡.md "wikilink")
[Category:潍坊行政区划史](../Category/潍坊行政区划史.md "wikilink")
[Category:淄博行政区划史](../Category/淄博行政区划史.md "wikilink")
[Category:连云港行政区划史](../Category/连云港行政区划史.md "wikilink")