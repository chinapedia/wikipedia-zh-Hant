**北港溪**位於[台灣中部](../Page/台灣.md "wikilink")，為[烏溪的主流上游](../Page/烏溪.md "wikilink")，流域面積536.33平方公里\[1\]，分佈於[南投縣北部及](../Page/南投縣.md "wikilink")[台中市一小部分](../Page/台中市.md "wikilink")。主流發源於標高2,532公尺之[更孟山南側](../Page/更孟山.md "wikilink")\[2\]\[3\]，向南南西流至經翠巒、[紅香](../Page/紅香.md "wikilink")、溪門，於馬家附近匯集[瑞岩溪](../Page/瑞岩溪.md "wikilink")，續流至瑞岩後轉西偏南，經萱野、新生社
(眉原)、互助、清流、梅子林、北港、國姓，於柑子林與[南港溪會合](../Page/南港溪_\(南投縣\).md "wikilink")，改稱[烏溪](../Page/烏溪.md "wikilink")。

## 糯米橋

[Old_Beigang_River_Bridge.jpg](https://zh.wikipedia.org/wiki/File:Old_Beigang_River_Bridge.jpg "fig:Old_Beigang_River_Bridge.jpg")\]\]
在南投縣國姓鄉[北港村往](../Page/北港村.md "wikilink")[惠蓀林場方向](../Page/惠蓀國家森林遊樂區.md "wikilink")，過烏溪上游的，有一座跨越北港溪的**糯米橋**（即[國姓鄉北港溪石橋](../Page/國姓鄉北港溪石橋.md "wikilink")）。監造人為林龍先生，出生於西元1898年12月27日，台灣彰化竹圍人。糯米橋是利用糯米、黑糖、石灰，代替水泥作為固定石塊的材料，因而得名。2004年糯米橋被公告定為三級古蹟，是第一座被定為古蹟的橋樑。

## 北港溪主要支流

  - **北港溪**：[南投縣](../Page/南投縣.md "wikilink")、[台中市](../Page/台中市.md "wikilink")
      - [水長流溪](../Page/水長流溪.md "wikilink")：南投縣[國姓鄉](../Page/國姓鄉.md "wikilink")、台中市[新社區](../Page/新社區.md "wikilink")、[和平區](../Page/和平區_\(臺中市\).md "wikilink")
          - [水流東溪](../Page/水流東溪.md "wikilink")：南投縣國姓鄉
          - [二櫃溪](../Page/二櫃溪.md "wikilink")：台中市新社區
          - [牛坪坑溪](../Page/牛坪坑溪.md "wikilink")：台中市和平區
          - [包安溪](../Page/包安溪.md "wikilink")：台中市和平區
          - [包拉五溪](../Page/包拉五溪.md "wikilink")：台中市和平區
      - [眉原溪](../Page/眉原溪.md "wikilink")：南投縣[仁愛鄉](../Page/仁愛鄉.md "wikilink")
      - [黃肉溪](../Page/黃肉溪.md "wikilink")：南投縣仁愛鄉
      - [楊岸溪](../Page/楊岸溪.md "wikilink")：南投縣仁愛鄉
      - [關刀溪](../Page/關刀溪.md "wikilink")：南投縣仁愛鄉
      - [尾敏溪](../Page/尾敏溪.md "wikilink")：南投縣仁愛鄉
      - [九仙溪](../Page/九仙溪.md "wikilink")：南投縣仁愛鄉
      - [東峰溪](../Page/東峰溪.md "wikilink")：南投縣仁愛鄉
      - [椿谷溪](../Page/椿谷溪.md "wikilink")：南投縣仁愛鄉
      - [合水溪](../Page/合水溪.md "wikilink")：南投縣仁愛鄉
      - [布布爾溪](../Page/布布爾溪.md "wikilink")：南投縣仁愛鄉
      - [發祥溪](../Page/發祥溪.md "wikilink")：南投縣仁愛鄉
      - [帖比倫溪](../Page/帖比倫溪.md "wikilink")：南投縣仁愛鄉
      - [瑞岩溪](../Page/瑞岩溪.md "wikilink")：南投縣仁愛鄉

## 相關條目

  - [烏溪](../Page/烏溪.md "wikilink")
  - [台灣河流列表](../Page/台灣河流列表.md "wikilink")

## 參考資料

<div class="references-small">

<references/>

</div>

[Category:烏溪水系](../Category/烏溪水系.md "wikilink")
[Category:台中市河川](../Category/台中市河川.md "wikilink")
[Category:南投縣河川](../Category/南投縣河川.md "wikilink")

1.  [臺中縣環保局 烏溪流域環境資訊網](http://bumf.teepb.gov.tw/river/link1-22.html)
2.  [東海山社溯溪館：烏溪](http://web.thu.edu.tw/deborah/www/index2/stream/i_stream.htm)
3.  一說發源於[中央山脈的](../Page/中央山脈.md "wikilink")[合歡山西側](../Page/合歡山.md "wikilink")，亦即以[瑞岩溪為主流](../Page/瑞岩溪.md "wikilink")，[經濟部水利署水利櫥窗：讓我們看河去(重要河川)--烏溪](http://www.wra.gov.tw/ct.asp?xItem=20075&CtNode=4356)