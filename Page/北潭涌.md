[HK_Sai_Kung_Country_Park_Visitor_Centre_1.JPG](https://zh.wikipedia.org/wiki/File:HK_Sai_Kung_Country_Park_Visitor_Centre_1.JPG "fig:HK_Sai_Kung_Country_Park_Visitor_Centre_1.JPG")
[Pak_Tam_Chung_Creek.jpg](https://zh.wikipedia.org/wiki/File:Pak_Tam_Chung_Creek.jpg "fig:Pak_Tam_Chung_Creek.jpg")
**北潭涌**（）位於[新界東部](../Page/新界.md "wikilink")，是[西貢郊野公園的中轉站和就近馬鞍山市區的郊遊遠足之地](../Page/西貢郊野公園.md "wikilink")，亦是[麥理浩徑的起點](../Page/麥理浩徑.md "wikilink")，而大網仔路可沿迴旋處直達馬鞍山、九龍和香港島。昔日的北潭涌有零星的村落。但除著[鄉城遷移](../Page/鄉城遷移.md "wikilink")，原有村落多已衰微。如北潭涌的上窰村乃19世紀由[黃氏](../Page/黃姓.md "wikilink")（落擔祖：黃發升及其數兄弟）建立之[客家村落](../Page/客家.md "wikilink")，主要以燒[石灰為生](../Page/石灰.md "wikilink")，在荒廢多年後，在1981年被列為[香港法定古蹟](../Page/香港法定古蹟.md "wikilink")，1984年成為今日的[上窰民俗文物館](../Page/上窰民俗文物館.md "wikilink")。北潭涌原指[龍坑](../Page/龍坑.md "wikilink")，是流經[北潭的一條](../Page/北潭.md "wikilink")[溪澗](../Page/溪澗.md "wikilink")。

自從1976年[香港政府把](../Page/香港殖民地時期#香港政府.md "wikilink")[西貢半島劃作郊野公園後](../Page/西貢半島.md "wikilink")，位於[西貢東郊野公園及](../Page/西貢東郊野公園.md "wikilink")[西貢西郊野公園中間的北潭涌](../Page/西貢西郊野公園.md "wikilink")，已建成西貢郊野公園遊客中心，專門介紹昔日西貢鄉村習俗、生態環境及風景區等等。不久，[保良局在北潭涌設置](../Page/保良局.md "wikilink")[保良局羅傑承北潭涌渡假營供宿營遊客逗留](../Page/保良局羅傑承北潭涌渡假營.md "wikilink")，同時亦有官方[康樂及文化事務署管理的](../Page/康樂及文化事務署.md "wikilink")[麥理浩夫人度假村和香港小童群益會管理的](../Page/麥理浩夫人度假村.md "wikilink")[白普理營](../Page/白普理營.md "wikilink")，3個渡假營申請使用者眾，排期需時。

除了渡假營外，北潭涌還設有幾個郊遊徑：

  - [北潭涌樹木研習徑](../Page/北潭涌樹木研習徑.md "wikilink")
  - [北潭涌自然教育徑](../Page/北潭涌自然教育徑.md "wikilink")
  - [上窰家樂徑](../Page/上窰家樂徑.md "wikilink")
  - [北潭涌家樂徑](../Page/北潭涌家樂徑.md "wikilink")
  - [北潭郊遊徑](../Page/北潭郊遊徑.md "wikilink")
  - [鹿湖郊遊徑](../Page/鹿湖郊遊徑.md "wikilink")
  - [上窰郊遊徑](../Page/上窰郊遊徑.md "wikilink")

## 氣候

<div style="width:80%;">

## 保良局羅傑承北潭涌渡假營

2010年2月23日，出席「保良局羅傑承北潭涌渡假營」命名儀式的[羅傑承證實](../Page/羅傑承.md "wikilink")，獲[香港賽馬會慈善信託基金的資助](../Page/香港賽馬會.md "wikilink")，渡假營將會興建一個面積95米X65米的標準足球場，現正探討就保養問題而使用第4代仿真草的可能性。據了解，訓練基地今後將全權由保良局管理，除代表隊，亦會接受市民、團體及職業球隊租用。\[1\]
2019年2月18日，保良局賽馬會北潭涌渡假營的第4代人造草運動場正式落成和使用。運動場面積61乘37米，斥資700萬包括鋪設人造草地、草地疏水系統、鞏固附近斜坡等。球場只需要每周使用疏草機打理一次，管理方便。\[2\]

[File:HK_SheungYiuFamilyWalk.JPG|上窰家樂徑](File:HK_SheungYiuFamilyWalk.JPG%7C上窰家樂徑)

## 交通

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

北潭涌設有公共停車場，供郊遊人士泊車轉乘公共交通工具前往郊野公園私家車禁區範圍各處，減少私家車駛入郊野公園，收保護環境之效。

## 區議會議席分佈

北潭涌由於屬於[鄉村地帶](../Page/鄉村.md "wikilink")，人口不多，所以往往加入其他地方而組合為[西貢離島選區](../Page/西貢離島.md "wikilink")，選出一名區議員。

<table>
<thead>
<tr class="header">
<th><p>年度/範圍</p></th>
<th><p>2000-2003</p></th>
<th><p>2004-2007</p></th>
<th><p>2008-2011</p></th>
<th><p>2012-2015</p></th>
<th><p>2016-2019</p></th>
<th><p>2020-2023</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/西貢郊野公園.md" title="wikilink">西貢郊野公園管理站</a><a href="../Page/大網仔道.md" title="wikilink">大網仔道一帶</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

註：以上主要範圍尚有其他細微調整（包括編號），請參閱有關區議會選舉選區分界地圖及條目。

## 參考資料

## 外部参考

  - [西貢郊野公園遊客中心及地圖](http://www.afcd.gov.hk/tc_chi/country/cou_lea/cou_lea_ven/saikung.html)

[Category:西貢半島](../Category/西貢半島.md "wikilink")
[北潭涌](../Category/北潭涌.md "wikilink")
[Category:西貢區鄉村](../Category/西貢區鄉村.md "wikilink")
[Category:香港客家鄉村](../Category/香港客家鄉村.md "wikilink")

1.  [北潭涌變身港隊基地](http://www1.hk.apple.nextmedia.com/template/apple/art_main.php?iss_id=20100224&sec_id=25391&art_id=13756571)
    *蘋果日報*. 2010年02月24日.
2.