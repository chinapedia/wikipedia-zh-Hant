《**北齐书**》，[唐朝史学家](../Page/唐朝.md "wikilink")[李百药撰](../Page/李百药.md "wikilink")，原名《**齊書**》，因[宋朝時為区别于](../Page/宋朝.md "wikilink")[萧子显](../Page/萧子显.md "wikilink")《[南齐书](../Page/南齐书.md "wikilink")》與[南齊歷史區別而更名](../Page/南齊.md "wikilink")，属[纪传体](../Page/纪传体.md "wikilink")[断代史](../Page/断代史.md "wikilink")，共50卷，纪8卷，列传42卷，记载上起[北魏分裂前十年左右](../Page/北魏.md "wikilink")，接續北魏分裂、[东魏立国](../Page/东魏.md "wikilink")、[北齐取代东魏](../Page/北齐.md "wikilink")，下迄北齐亡國，前後约五十餘年史實，而以记北齐历史为主。同时期成书还有《[梁书](../Page/梁书.md "wikilink")》、《[陈书](../Page/陈书.md "wikilink")》、《[周书](../Page/周书.md "wikilink")》、《[隋书](../Page/隋书.md "wikilink")》。

## 流傳經過

唐太宗頗器重李百藥的才學，百藥先後於[唐太宗](../Page/唐太宗.md "wikilink")[貞觀元年](../Page/貞觀_\(唐\).md "wikilink")（627年）和三年（629年）兩次奉詔繼續完成父撰《齊書》遺稿，並參考了隋朝史家[王劭所撰編年體](../Page/王劭_\(隋朝\).md "wikilink")《[齊志](../Page/齊志.md "wikilink")》。《齊書》成書於貞觀十年（636年），經歷了三個朝代（北齊、隋、唐）、共六十多年時間。

北齊同[西魏和](../Page/西魏.md "wikilink")[北周并立](../Page/北周.md "wikilink")，另一方面，它们又一道跟南朝的梁、陈对峙。因此阅读《北齐书》的同時應和《周书》对照着读，另一方面再和《梁书》、《陈书》对照着读。李百藥撰文善用事例與言語來描述史實，並收錄了當時的流行、通俗語句，使得人物生動鮮明。如帝紀第一收錄將當時傳唱的歌謠：「銅拔打鐵拔，元家世將末。」，當時有好事者以銅拔與鐵拔代表拓拔、賀拔二人，斷言是元家將要衰敗的徵兆。書中亦存在有神異的描寫，如寫[高洋出生前有赤光照室](../Page/高洋.md "wikilink")，出生時“鱗身、重踝”，即位時“京師獲赤雀”等等，是本書的缺失。

後來《北齐书》遭受嚴重散佚。到[南宋時](../Page/南宋.md "wikilink")，五十卷的《北齐书》僅剩一卷帝纪、十六卷列传是李百药的原文；其餘各卷，都是後人根據唐代史家[李延寿所撰](../Page/李延寿.md "wikilink")《[北史](../Page/北史.md "wikilink")》抄补修成的。由於《北史》當時並没有遭到散佚，故後人根据《北史》以補寫《北齐书》，在文字上非尽属百药原文，但在内容上則不失真實（因《北史》的北齐史部分多採自《北齐书》）。

《北齊書》無志及表，後人據相關史料補作志表，清人[萬斯同](../Page/萬斯同.md "wikilink")《[歷代史表](../Page/歷代史表.md "wikilink")》有〈北齊諸王世表〉、〈北齊異姓諸王世表〉、〈北齊將相大臣年表〉各1卷，近人[施和金](../Page/施和金.md "wikilink")《北齊地理志》5卷，[蒙傳銘](../Page/蒙傳銘.md "wikilink")《補北齊書藝文志》不分卷，佚名《補北齊書疆域志》不分卷。1931年，刘盼遂发表了《齐书宗室世系表》，\[1\]1945年，徐仁辅发表了《补〈北齐书·艺文志〉》，\[2\]1947
年，严敦杰发表了《北齐历考》，均为补缺之作。

## 内容

注：有\*者為李氏原文

### 帝紀

1.  帝紀第一 - [神武帝上](../Page/高欢.md "wikilink")
2.  帝紀第二 - 神武帝下
3.  帝紀第三 - [文襄帝](../Page/高澄.md "wikilink")
4.  帝紀第四\* - [文宣帝](../Page/高洋.md "wikilink")
5.  帝紀第五 - [废帝](../Page/高殷.md "wikilink")
6.  帝紀第六 - [孝昭帝](../Page/北齐孝昭帝.md "wikilink")
7.  帝紀第七 - [武成帝](../Page/北齐武成帝.md "wikilink")
8.  帝紀第八 -
    [後主](../Page/北齐後主.md "wikilink")・[幼主](../Page/北齐幼主.md "wikilink")

### 列传

1.  列传第一 - 神武婁后・文襄元后・文宣李后・孝昭元后・武成胡后・後主斛律后・胡后・穆后
2.  列传第二 - 高祖十一王 -
    [永安簡平王浚](../Page/高浚.md "wikilink")・[平陽靖翼王淹](../Page/高淹.md "wikilink")・彭城景思王浟・上党剛肅王渙・襄城景王淯・任城王湝・高陽康穆王湜・[博陵文簡王济](../Page/高济.md "wikilink")・[華山王凝](../Page/高凝.md "wikilink")・[馮翊王潤](../Page/高潤.md "wikilink")・[漢陽敬怀王洽](../Page/高洽.md "wikilink")
3.  列传第三 - 文襄六王-
    [河南康舒王孝瑜](../Page/高孝瑜.md "wikilink")・广寧王孝珩・[河間王孝琬](../Page/高孝琬.md "wikilink")・[蘭陵忠武王孝瓘](../Page/高長恭.md "wikilink")・[安德王延宗](../Page/安德王.md "wikilink")・[漁陽王紹信](../Page/高紹信.md "wikilink")
4.  列传第四 - 文宣四王・孝昭六王・武成十二王・後主五男
5.  列传第五\* -
    [趙郡王琛](../Page/高琛.md "wikilink")・[清河王岳](../Page/高岳.md "wikilink")
6.  列传第六 -
    [广平公盛](../Page/高盛.md "wikilink")・[陽州公永乐](../Page/高永乐.md "wikilink")・[襄乐王显国](../Page/高显国.md "wikilink")・[上洛王思宗](../Page/高思宗.md "wikilink")・[平秦王归彦](../Page/高归彦.md "wikilink")・[武興王普](../Page/高普.md "wikilink")・[長乐太守灵山](../Page/高灵山.md "wikilink")
7.  列传第七 -
    [竇泰](../Page/竇泰.md "wikilink")・[尉景](../Page/尉景.md "wikilink")・[婁昭](../Page/婁昭.md "wikilink")・[厙狄干](../Page/庫狄干.md "wikilink")・[韓軌](../Page/韓軌.md "wikilink")・[潘乐](../Page/潘乐.md "wikilink")
8.  列传第八\* - [段荣](../Page/段荣.md "wikilink")
9.  列传第九\* - [斛律金](../Page/斛律金.md "wikilink")
10. 列传第十\* -
    [孫騰](../Page/孫騰.md "wikilink")・[高隆之](../Page/高隆之.md "wikilink")・[司馬子如](../Page/司馬子如.md "wikilink")
11. 列传第十一\* -
    [賀拔允](../Page/賀拔允.md "wikilink")・[蔡儁](../Page/蔡儁.md "wikilink")・[韓賢](../Page/韓賢.md "wikilink")・[尉長命](../Page/尉長命.md "wikilink")・[王怀](../Page/王怀.md "wikilink")・[劉貴](../Page/劉貴.md "wikilink")・[任延敬](../Page/任祥_\(北朝\).md "wikilink")・[莫多婁貸文](../Page/莫多婁貸文.md "wikilink")・[高市貴](../Page/高市貴.md "wikilink")・[厙狄迴洛](../Page/庫狄迴洛.md "wikilink")・[厙狄盛](../Page/庫狄盛.md "wikilink")・[薛孤延](../Page/薛孤延.md "wikilink")・[張保洛](../Page/張保洛.md "wikilink")・[侯莫陳相](../Page/侯莫陳相.md "wikilink")
12. 列传第十二\* -
    [張瓊](../Page/張瓊_\(北朝\).md "wikilink")・[斛律羌举](../Page/斛律羌举.md "wikilink")・[堯雄](../Page/堯雄.md "wikilink")・[宋显](../Page/宋显.md "wikilink")・[王則](../Page/王則.md "wikilink")・[慕容紹宗](../Page/慕容紹宗.md "wikilink")・[薛修義](../Page/薛修義.md "wikilink")・[叱列平](../Page/叱列平.md "wikilink")・[步大汗薩](../Page/步大汗薩.md "wikilink")・[慕容儼](../Page/慕容儼.md "wikilink")
13. 列传第十三\* -
    [高-{乾}-](../Page/高乾_\(北魏\).md "wikilink")・[封隆之](../Page/封隆之.md "wikilink")
14. 列传第十四\* -
    [李元忠](../Page/李元忠.md "wikilink")・[卢文伟](../Page/卢文伟.md "wikilink")・[李義深](../Page/李義深.md "wikilink")
15. 列传第十五\* -
    [魏蘭根](../Page/魏蘭根.md "wikilink")・[崔悛](../Page/崔悛.md "wikilink")
16. 列传第十六\* -
    [孫搴](../Page/孫搴.md "wikilink")・[陳元康](../Page/陳元康.md "wikilink")・[杜弼](../Page/杜弼.md "wikilink")
17. 列传第十七\* -
    [張纂](../Page/張纂_\(代郡\).md "wikilink")・[張亮](../Page/張亮_\(北齐\).md "wikilink")・[張耀](../Page/張耀.md "wikilink")・[趙起](../Page/趙起.md "wikilink")・[徐遠](../Page/徐遠.md "wikilink")・[王峻](../Page/王峻.md "wikilink")・[王紘](../Page/王紘.md "wikilink")
18. 列传第十八 -
    [薛琡](../Page/薛琡.md "wikilink")・[敬显儁](../Page/敬显儁.md "wikilink")・[平鑑](../Page/平鑑.md "wikilink")
19. 列传第十九 -
    [万俟普](../Page/万俟普.md "wikilink")・[可朱渾元](../Page/可朱渾元.md "wikilink")・[刘丰](../Page/刘丰.md "wikilink")・[破六韓常](../Page/破六韓常.md "wikilink")・[金祚](../Page/金祚.md "wikilink")・[韋子粲](../Page/韋子粲.md "wikilink")
20. 列传第二十 -
    [元坦](../Page/元坦.md "wikilink")・[元斌](../Page/元斌_\(北齐\).md "wikilink")・[元孝友](../Page/元孝友.md "wikilink")・[元暉業](../Page/元暉業.md "wikilink")・[元弼](../Page/元弼.md "wikilink")・[元韶](../Page/元韶.md "wikilink")
21. 列传第二十一 -
    [李渾](../Page/李渾_\(北魏\).md "wikilink")・[李璵](../Page/李璵.md "wikilink")・[鄭述祖](../Page/鄭述祖.md "wikilink")
22. 列传第二十二 -
    [崔暹](../Page/崔暹.md "wikilink")・[高德政](../Page/高德政.md "wikilink")・[崔昂](../Page/崔昂.md "wikilink")
23. 列传第二十三 - [王昕](../Page/王昕.md "wikilink")
24. 列传第二十四 -
    [陸法和](../Page/陸法和.md "wikilink")・[王琳](../Page/王琳_\(会稽\).md "wikilink")
25. 列传第二十五 -
    [蕭明](../Page/萧渊明.md "wikilink")・[蕭祗](../Page/蕭祗.md "wikilink")・[蕭退](../Page/蕭退.md "wikilink")・[蕭放](../Page/蕭放.md "wikilink")・[徐之才](../Page/徐之才.md "wikilink")
26. 列传第二十六 - [楊愔](../Page/楊愔.md "wikilink")
27. 列传第二十七 -
    [裴讓之](../Page/裴讓之.md "wikilink")・[皇甫和](../Page/皇甫和.md "wikilink")・[李構](../Page/李構.md "wikilink")・[張宴之](../Page/張宴之.md "wikilink")・[陸卬](../Page/陸卬.md "wikilink")・[王松年](../Page/王松年_\(北朝\).md "wikilink")・[劉禕](../Page/劉禕_\(北朝\).md "wikilink")
28. 列传第二十八 - [邢邵](../Page/邢邵.md "wikilink")
29. 列传第二十九 - [魏收](../Page/魏收.md "wikilink")
30. 列传第三十 -
    [辛術](../Page/辛術.md "wikilink")・[元文遙](../Page/元文遙.md "wikilink")・[趙彦深](../Page/趙彦深.md "wikilink")
31. 列传第三十一 -
    [崔季舒](../Page/崔季舒.md "wikilink")・[祖珽](../Page/祖珽.md "wikilink")
32. 列传第三十二 -
    [尉瑾](../Page/尉瑾.md "wikilink")・[馮子琮](../Page/馮子琮.md "wikilink")・[赫連子悦](../Page/赫連子悦.md "wikilink")・[唐邕](../Page/唐邕.md "wikilink")・[白建](../Page/白建.md "wikilink")
33. 列传第三十三\* -
    [暴显](../Page/暴显.md "wikilink")・[皮景和](../Page/皮景和.md "wikilink")・[鮮于世荣](../Page/鮮于世荣.md "wikilink")・[綦連猛](../Page/綦連猛.md "wikilink")・[元景安](../Page/元景安.md "wikilink")・[独孤永業](../Page/独孤永業.md "wikilink")・[傅伏](../Page/傅伏.md "wikilink")・[高保寧](../Page/高保寧.md "wikilink")
34. 列传第三十四\* -
    [陽斐](../Page/陽斐.md "wikilink")・[盧潜](../Page/盧潜.md "wikilink")・[崔劼](../Page/崔劼.md "wikilink")・[盧叔武](../Page/盧叔武.md "wikilink")・[陽休之](../Page/陽休之.md "wikilink")・[袁聿修](../Page/袁聿修.md "wikilink")
35. 列传第三十五\* -
    [李稚廉](../Page/李稚廉.md "wikilink")・[封述](../Page/封述.md "wikilink")・[許夏惇](../Page/許夏惇.md "wikilink")・[羊烈](../Page/羊烈.md "wikilink")・[源彪](../Page/源彪.md "wikilink")
36. 列传第三十六\* 儒林 -
37. 列传第三十七\* 文苑 -
    [祖鴻勋](../Page/祖鴻勋.md "wikilink")・[李广](../Page/李广_\(北齐\).md "wikilink")・[樊遜](../Page/樊遜.md "wikilink")・[劉逖](../Page/劉逖.md "wikilink")・[荀士遜](../Page/荀士遜.md "wikilink")・[顔之推](../Page/顔之推.md "wikilink")
38. 列传第三十八 循吏 -
39. 列传第三十九 酷吏 -
40. 列传第四十 外戚 -
    [趙猛](../Page/趙猛.md "wikilink")・[婁叡](../Page/婁叡.md "wikilink")・[尒朱文暢](../Page/尒朱文暢.md "wikilink")・[鄭仲礼](../Page/鄭仲礼.md "wikilink")・[李祖昇](../Page/李祖昇.md "wikilink")・[元蛮](../Page/元蛮.md "wikilink")
41. 列传第四十一 方伎 -
42. 列传第四十二 恩倖 -
    [郭秀](../Page/郭秀.md "wikilink")・[和士開](../Page/和士開.md "wikilink")・[穆提婆](../Page/穆提婆.md "wikilink")・[高阿那肱](../Page/高阿那肱.md "wikilink")・[韓鳳](../Page/韓鳳.md "wikilink")・[韓宝業](../Page/韓宝業.md "wikilink")

## 参考文献

## 外部链接

  - [中華典藏：二十五史全文](http://www.hoolulu.com/zh/)
  - [《北齊书》全文](http://www.guoxue.com/shibu/24shi/beiqishu/beiqiml.htm)

[Category:北齐](../Category/北齐.md "wikilink")
[Category:史部正史類](../Category/史部正史類.md "wikilink")
[Category:纪传体](../Category/纪传体.md "wikilink")
[Category:二十四史](../Category/二十四史.md "wikilink")
[Category:7世紀書籍](../Category/7世紀書籍.md "wikilink")
[Category:唐朝典籍](../Category/唐朝典籍.md "wikilink")

1.  《学文》第1卷第3期
2.  《志学月刊》第 19、20 期合刊