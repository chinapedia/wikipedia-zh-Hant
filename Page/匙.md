[Chinese_spoons.jpg](https://zh.wikipedia.org/wiki/File:Chinese_spoons.jpg "fig:Chinese_spoons.jpg")\]\]
**匙**，也称**[汤匙](../Page/汤匙.md "wikilink")**、**匙子**、**勺**、**勺子**、**汤勺**、**調羹**或**匙羹**，是一种餐具、量具或工具，由带有凹陷的头部和连接的柄构成。用来装液体和小块固体。一般把體積較小、用於進餐的稱為「匙」或「羹」(spoon)，體積較大用於烹飪和把食物撈起到碗或盤子裡的的稱為「勺」(ladle)。

## 饮食工具

[Tibetan_spoon_2005.jpg](https://zh.wikipedia.org/wiki/File:Tibetan_spoon_2005.jpg "fig:Tibetan_spoon_2005.jpg")式样的匙子\]\]

[烹饪过程中](../Page/烹饪.md "wikilink")，匙可以用来量和取原料。“[餐匙](../Page/餐匙.md "wikilink")”（tablespoon）和“[茶匙](../Page/茶匙.md "wikilink")”（teaspoon）是欧美一些国家[菜谱中常见的](../Page/菜谱.md "wikilink")[体积单位](../Page/体积.md "wikilink")。一汤匙（又称大匙）目前在[美国法定为](../Page/美国.md "wikilink")15[毫升](../Page/毫升.md "wikilink")，英国则为14.2-17.8毫升不等。一茶匙在美国的标准为4.93毫升，[英制单位则为](../Page/英制单位.md "wikilink")3.55毫升（=1液量打兰）。

可以用做搅动工具来充分混合流质和半流质的[食品](../Page/食品.md "wikilink")，如[汤或](../Page/汤.md "wikilink")[粥](../Page/粥.md "wikilink")。在煮汤等食物的时候，表面的泡沫可以用扁平的匙撇去。此外，带洞的大勺子可以捞取锅里的固体食物，而留下流质部分。

烹饪完毕的流质、半流质食品用大型的匙从锅转移到[碗等进餐器皿后上桌](../Page/碗.md "wikilink")。进餐者用匙来取食这些食品的详情见下。

### 中餐汤匙

[中国菜的传统餐具为](../Page/中国菜.md "wikilink")[筷子和用来喝汤的汤匙](../Page/筷子.md "wikilink")。汤匙平底，深而高帮，柄与匙体合为一体，有时配有“凹”形的小架子用来在桌上暂时架汤匙。因为主要用来喝热汤，为了隔热多为瓷制，少数为金属制作。

正式使用礼仪如下（非正式场合则比较随便，下面西方亦然）：

  - 不应该和筷子同握。
  - 汤匙从远侧向**就餐者方向**舀汤。
  - 碗里的汤很少的时候可以向**就餐者方向**倾斜以便于舀取。
  - 汤太热的时候可以小口吹气加速其冷却。
  - 喝的时候汤匙不进嘴而是倾斜汤匙从边上喝。
  - 咂嘴出声音或吹气动静太大被认为是不礼貌的表现。
  - 赴宴的时候，不应提前于主人动筷子或汤匙。

[茶道中所用的](../Page/茶道.md "wikilink")[茶匙](../Page/茶匙_\(點茶茶具\).md "wikilink")，是泡完茶后从壶中取出茶叶用的。

### 西餐餐匙

[Silver_spoon.jpeg](https://zh.wikipedia.org/wiki/File:Silver_spoon.jpeg "fig:Silver_spoon.jpeg")

[西餐的传统餐具为](../Page/西餐.md "wikilink")[餐刀](../Page/餐刀.md "wikilink")，[餐叉和餐匙](../Page/餐叉.md "wikilink")，一般为金属制，头部较小而且扁平，有长柄。除了喝汤之外还可以用来舀调味汁及吃甜品。没有中国的匙架，不用的时候放在自己的盘子里或桌布上。

喝汤的礼仪和中餐类似。主要不同为舀汤和倾斜方向与中餐相反（**背向就餐者方向**），而且肘部应尽量紧挨身体。另外，正式宴会上就餐者从第一道汤到最后的甜品会先后使用大小、功能不同的很多匙子；有时候是餐前按一定顺序在座位前摆好，有的是在撤、上菜的时候更换餐具。

西餐中用来搅加了[糖](../Page/糖.md "wikilink")、[牛奶的](../Page/牛奶.md "wikilink")[咖啡或](../Page/咖啡.md "wikilink")[茶使其均匀](../Page/茶.md "wikilink")；但按进餐礼仪来说，搅完应该把匙子拿出来放在一边，而不是用它来舀咖啡或茶喝。

在西餐中常用的餐匙有以下幾種：

  - [湯匙](../Page/湯匙.md "wikilink")（）：匙斗成圓形，用來喝[湯](../Page/湯.md "wikilink")。

  - （）：匙斗成橢圓形，和湯匙大小相約，用來吃[甜品](../Page/甜品.md "wikilink")。一般中國人拿來吃飯，反而用茶匙吃甜品。

  - 茶匙（）：外形與甜品匙相似，但比它小很多，用來攪拌[紅茶](../Page/紅茶.md "wikilink")。

  - [咖啡匙](../Page/咖啡匙.md "wikilink")（）：匙斗較短略呈圓形，用來攪拌咖啡。

  - [聖代匙](../Page/聖代匙.md "wikilink")（）：外形與茶匙相似，但匙柄長度與湯匙相約，用來吃[聖代](../Page/聖代.md "wikilink")、[雪糕等](../Page/雪糕.md "wikilink")。

  - 餐匙（）：外形與甜品匙相似，但匙斗較尖較大，用來吃飯或意粉等，不能用來喝湯，必須用Soup Spoon。

### 其他国家的餐匙

在[日本料理中](../Page/日本料理.md "wikilink")，汤传统上不配汤匙，而是直接举小碗到嘴边喝。匙子只是在茶道中使用，或者作为藥粉的量具。[明治维新之后](../Page/明治维新.md "wikilink")，随着[咖喱饭等舶来食物的流行](../Page/咖喱饭.md "wikilink")，日本人更常使用匙子飲食。食用[日本拉麵时](../Page/日本拉麵.md "wikilink")，食客通常左手拿匙，右手拿筷子。日本人除了使用[金属和](../Page/金属.md "wikilink")[瓷匙之外](../Page/瓷.md "wikilink")，也使用木匙。

在[朝鲜半岛](../Page/朝鲜半岛.md "wikilink")，进餐用筷子和长柄扁平的匙子。匙子是主要餐具，用来盛汤、喝汤（包括捞汤里的固体食品）和吃[饭](../Page/饭.md "wikilink")。筷子只是用来夾菜的。

[Absinthe_spoons.jpg](https://zh.wikipedia.org/wiki/File:Absinthe_spoons.jpg "fig:Absinthe_spoons.jpg")用的匙子\]\]

### 其他饮食用途

其他饮食用途还有：

  - 从容器里舀[冰淇淋一般使用特殊的勺子](../Page/冰淇淋.md "wikilink")，食用的时候用餐匙或甜点匙。
  - [甜瓜等质软的](../Page/甜瓜.md "wikilink")[水果可以用勺子挖取成块而不用刀切](../Page/水果.md "wikilink")。一些带核的水果如[梨需要去核的时候也可以用勺子挖](../Page/梨.md "wikilink")。
  - 苦艾酒饮用的时候配有特制的雕花匙子。饮者将匙子架于酒杯上，上置方糖，倒水在方糖之上，糖水流入酒杯，原来澄清的酒变成浑浊液体，然后饮用。

## 其他用途

[Spork.png](https://zh.wikipedia.org/wiki/File:Spork.png "fig:Spork.png")（spork）\]\]

  - [化学实验中](../Page/化学.md "wikilink")，从容器中取固体化学品用特制的[药匙](../Page/药匙.md "wikilink")。
  - 上瘾者吸食[海洛因等毒品的时候](../Page/海洛因.md "wikilink")，经常将毒品放在匙子里，在下方点火加热，融化后注射到体内。
  - [中医用汤匙的邊缘来](../Page/中医.md "wikilink")[刮痧](../Page/刮痧.md "wikilink")。
  - 按中国民俗，给满周岁的女孩[抓周的时候](../Page/抓周.md "wikilink")，有象征炊具的小匙子或铲子。抓到的被认为孩子长大后会善于烹饪。
  - 称为“[挖耳勺](../Page/挖耳勺.md "wikilink")”的小匙子传统上用来在耳道内清除[耳垢](../Page/耳垢.md "wikilink")。但使用的时候如不小心，有伤害[耳膜的危险](../Page/耳膜.md "wikilink")。
  - 贵重或制作精美的匙子被用来做纪念物。例如西方国家[教父、教母或其他人送给新生](../Page/教父_\(基督宗教\).md "wikilink")[嬰兒的常见礼物为](../Page/嬰兒.md "wikilink")[银做的小匙子](../Page/银.md "wikilink")。由此产生了“口含银匙出生”（）的俗语，意谓出生在富贵人家。
  - 金属勺子可以用做[打击乐器](../Page/打击乐器.md "wikilink")。
  - [超能力者會以折彎湯匙來展現超能力](../Page/超能力者.md "wikilink")。

## 参见

  - [餐叉](../Page/叉_\(餐具\).md "wikilink")
  - [餐刀](../Page/刀_\(餐具\).md "wikilink")
  - [天鹅勺](../Page/天鹅勺.md "wikilink")

[de:Essbesteck\#Löffel](../Page/de:Essbesteck#Löffel.md "wikilink")

[匙](../Category/匙.md "wikilink")
[Category:饮食工具](../Category/饮食工具.md "wikilink")