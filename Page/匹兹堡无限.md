**匹兹堡无限队**(**Pittsburgh
Xplosion**)是美国[大陆篮球协会的一支球队](../Page/大陆篮球协会.md "wikilink")，始建于2004年

但是球队拥有[ABA的球队参赛资格用来取代](../Page/美国篮球协会_\(21世纪\).md "wikilink")2000-2004间存在而现在已经解散了的[匹兹堡硬帽](../Page/匹兹堡硬帽.md "wikilink")(Pittsburgh
Hardhats)。在这一个联盟中匹兹堡球队是有历史传统的，1960年代到1970年代中的[匹兹堡风笛手队和之后的秃鹫队就出现在旧美国篮球协会中](../Page/匹兹堡风笛手队.md "wikilink")，得到过1967/68赛季的旧ABA联盟总冠军。、匹兹堡无限队的主场设在梅隆球场(Mellon
Arena)和[匹兹堡大学校区内的佩特森娱乐中心](../Page/匹兹堡大学.md "wikilink")，这两个球场都在[宾西法尼亚州的](../Page/宾西法尼亚州.md "wikilink")[匹兹堡](../Page/匹兹堡.md "wikilink")。

在他们第一个赛季期间，无限队取得了18胜12负的成绩，名列Freddie
Lewis分区的第二位。匹兹堡无限随后在季候赛中被[贝林汉姆爆扣队](../Page/贝林汉姆爆扣队.md "wikilink")(Bellingham
Slam)所淘汰。

最初只有几百位观众，但最终平均每场都能涌入1,600位观众观看球队的比赛。

在2006年赛季后，球队决定转移到[大陆篮球协会](../Page/大陆篮球协会.md "wikilink")。
[1](http://www.cbahoopsonline.com/news_detail.html?newsid=796).
在2006年的选秀中球队选中了多位在大学篮球赛中表现出色的球员，比如:Carl Krauser, Kevin
Pittsnogle 和 Jai Lewis。

## 外部链接

  - [官方网站](http://www.pittsburghxplosion.com/)
  - ["Join the Hooters
    Rooters"](http://mondesishouse.blogspot.com/2006/09/join-hooters-rooters_21.html)

[Category:美国大陆篮球协会球队](../Category/美国大陆篮球协会球队.md "wikilink")
[P](../Category/匹兹堡体育组织.md "wikilink")
[P](../Category/2004年建立.md "wikilink")