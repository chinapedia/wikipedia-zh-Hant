**医圣祠**位于[河南省](../Page/河南省.md "wikilink")[南阳市中心城区医圣祠路](../Page/南阳市.md "wikilink")，是为了纪念[东汉时期的](../Page/东汉.md "wikilink")[医圣](../Page/医圣.md "wikilink")[张仲景而修建的大型祠堂园林](../Page/张仲景.md "wikilink")。[文化大革命时期](../Page/文化大革命.md "wikilink")，张仲景的塑像被摧毁，墓亭、石碑被砸，“张仲景纪念馆”的展览品被洗劫一空，目前的医圣祠是在文革后修建的，目前是[全国重点文物保护单位和国家级](../Page/全国重点文物保护单位.md "wikilink")4A景区。

## 文物

医圣祠大门具有汉代建筑风格，其“医圣祠”三字为[郭沫若于](../Page/郭沫若.md "wikilink")1959年12月所题写。大门前双[阙并立](../Page/阙.md "wikilink")，名为“子母门阙”，其中母阙较高，子阙稍低，就像一位母亲怀抱[婴儿](../Page/婴儿.md "wikilink")。中国汉朝的建筑群时常可以见到这种建筑设计，可以作为瞭望哨和[烽火台](../Page/烽火台.md "wikilink")，也可以起到很好的装饰作用。大门上有一对[青铜](../Page/青铜.md "wikilink")“[铺首衔环](../Page/铺首衔环.md "wikilink")”，为虎嘴中叨一圆环，重约三百公斤，是目前世界上最重的铺首衔环。\[1\]

门庭内有一个巨大的石屏，是一块完整的石料制成的，长高各为3．5米，重达6吨，是中国当代碑林一绝。其正面雕刻的是《张仲景传》，为已故名医[黄竹斋先生撰写](../Page/黄竹斋.md "wikilink")，描述了张仲景的一生。两侧是由中国著名的老中医[任应秋题写的对联](../Page/任应秋.md "wikilink")：“阴阳有三，\[2\]辩病还须辩证；医相无二，活国在于活人”。背面雕刻的是张仲景的《[伤寒杂病论序](../Page/伤寒杂病论.md "wikilink")》，说明了他自己从医的原因。门庭内还有中国不同历史时期的四位大医学家的塑像，分别是[医和](../Page/医和.md "wikilink")、[王叔和](../Page/王叔和.md "wikilink")、[华佗](../Page/华佗.md "wikilink")、[李时珍](../Page/李时珍.md "wikilink")。

医圣祠后花园有喷水石龙，另外还有羊头四角砖墓。

## 注释

<div class="references-small">

<references />

</div>

[Category:河南的祠](../Category/河南的祠.md "wikilink")
[Category:河南墓葬](../Category/河南墓葬.md "wikilink")
[Category:南阳景点](../Category/南阳景点.md "wikilink")
[Category:南阳建筑物](../Category/南阳建筑物.md "wikilink")

1.  [河南南阳. 医 圣 祠
    ——导游词](http://www.kftour.com/news/2004/10-25/122913.shtml)
    ，开封旅游网2004年10月25日。
2.  “阴阳有三”是指三阴（少阴、太阴、阙阴）和三阳（少阳、太阳、阳明）。