《**十七史商榷**》，共一百卷，清代[王鳴盛](../Page/王鳴盛.md "wikilink")（1722年－1797年）著。在清代與[錢大昕](../Page/錢大昕.md "wikilink")《[廿二史考異](../Page/廿二史考異.md "wikilink")》、[趙翼](../Page/趙翼.md "wikilink")《[廿二史劄記](../Page/廿二史劄記.md "wikilink")》齊名。

## 內容架構

《十七史商榷》所包括者，其實是十九史，所謂“十七史”，是沿用了宋時彙刻[十七史之名](../Page/十七史.md "wikilink")（十七史又多了《[舊唐書](../Page/舊唐書.md "wikilink")》、《[舊五代史](../Page/舊五代史.md "wikilink")》兩種）。內容分《[史記](../Page/史記.md "wikilink")》六卷、《[漢書](../Page/漢書.md "wikilink")》二十二卷、《[後漢書](../Page/後漢書.md "wikilink")》十卷、《[三國志](../Page/三國志.md "wikilink")》四卷、《[晉書](../Page/晉書.md "wikilink")》十卷、《[南史](../Page/南史.md "wikilink")》、《[宋書](../Page/宋書.md "wikilink")》、《[齊書](../Page/南齊書.md "wikilink")》、《[梁書](../Page/梁書.md "wikilink")》、《[陳書](../Page/陳書.md "wikilink")》共十二卷、《[北史](../Page/北史.md "wikilink")》、《[魏書](../Page/魏書.md "wikilink")》、《[齊書](../Page/北齊書.md "wikilink")》、《[周書](../Page/周書.md "wikilink")》、《[隋書](../Page/隋書.md "wikilink")》共四卷、《[新唐書](../Page/新唐書.md "wikilink")》與《[舊唐書](../Page/舊唐書.md "wikilink")》二十四卷、《[新五代史](../Page/新五代史.md "wikilink")》與《[舊五代史](../Page/舊五代史.md "wikilink")》六卷，另有〈綴言〉二卷。

## 考據治史

王鳴盛治史重考據，反對議論褒貶的[春秋筆法](../Page/春秋筆法.md "wikilink")，對於[儒家對歷史的歪曲](../Page/儒家.md "wikilink")，皆據實予以反駁；他認為：「大抵史家所記，典制有得有失，讓史者不必橫生意見，馳騁議論，以明法戒也，但當考其典制之實，俾數千百年建置沿革，瞭如指掌，而或宜法，或宜戒，待人之自擇焉可矣。其事蹟則有美有惡，讀史者不必強立文法，擅加與奪，以為褒貶也，但當考其事蹟之實，俾年經事緯，部居州次，紀載之異同，見解之離合，一一條析無疑，而若者可褒，若者可貶，聽之天下之公論焉可矣。書生胸臆，每患迂愚，即使考之已詳，而議論褒貶，猶恐末當，況其考之未確者哉！蓋學問之道，求於虛不如求於實，議論褒貶，皆虛文耳。作史者之所記錄，讀史者之所考核，總期於能得其實焉而已矣，外此又何多求耶？」

## 評價

[谭其骧以](../Page/谭其骧.md "wikilink")《十七史商榷》中“许邺洛三都”条做分析稱：“王氏此条，无一语不误”\[1\]。

## 参考文献

[Category:二十四史](../Category/二十四史.md "wikilink")
[Category:清朝典籍](../Category/清朝典籍.md "wikilink")
[Category:中国通史性历史著作](../Category/中国通史性历史著作.md "wikilink")

1.  《历史地理》第7辑