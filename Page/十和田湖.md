**十和田湖**（）是橫跨[日本](../Page/日本.md "wikilink")[青森縣](../Page/青森縣.md "wikilink")[十和田市](../Page/十和田市.md "wikilink")、[秋田縣](../Page/秋田縣.md "wikilink")[鹿角郡](../Page/鹿角郡.md "wikilink")[小坂町的一個](../Page/小坂町.md "wikilink")[湖泊](../Page/湖泊.md "wikilink")。位于[十和田八幡平國立公園内](../Page/十和田八幡平國立公園.md "wikilink")，是由[十和田火山噴發而形成的二重](../Page/十和田火山.md "wikilink")[火山湖](../Page/火山湖.md "wikilink")。十和田湖面積61.1平方公里，是日本排名第12大的湖泊，而其327公尺的最大深度僅居於[田澤湖](../Page/田澤湖.md "wikilink")（423公尺，秋田縣）與[支笏湖](../Page/支笏湖.md "wikilink")（363公尺，[北海道](../Page/北海道.md "wikilink")）之後，名列日本第三。

## 相關條目

  - [和井内貞行](../Page/和井内貞行.md "wikilink")

## 外部連結

  - [十和田湖觀光情報](http://www.expandbiz.net/towada/)

[Category:日本火山湖](../Category/日本火山湖.md "wikilink")
[Category:青森縣地理](../Category/青森縣地理.md "wikilink")
[Category:秋田縣地理](../Category/秋田縣地理.md "wikilink")
[Category:十和田市](../Category/十和田市.md "wikilink")
[Category:小坂町](../Category/小坂町.md "wikilink")
[Category:平成百景](../Category/平成百景.md "wikilink")
[Category:日本地質百選](../Category/日本地質百選.md "wikilink")
[Category:秋田縣觀光地](../Category/秋田縣觀光地.md "wikilink")