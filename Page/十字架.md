[Marco_palmezzano,_crocifissione_degli_Uffizi.jpg](https://zh.wikipedia.org/wiki/File:Marco_palmezzano,_crocifissione_degli_Uffizi.jpg "fig:Marco_palmezzano,_crocifissione_degli_Uffizi.jpg"))\]\]

**十字架**曾作為一種古代[死刑的](../Page/死刑.md "wikilink")[刑具](../Page/刑具.md "wikilink")。《[新約聖經](../Page/新約聖經.md "wikilink")》希臘文版聖經記載[耶稣曾被](../Page/耶稣.md "wikilink")[猶太教宗教領袖拘送到](../Page/猶太教.md "wikilink")[羅馬帝國駐](../Page/羅馬帝國.md "wikilink")[猶太](../Page/猶太.md "wikilink")[總督](../Page/總督.md "wikilink")[彼拉多](../Page/彼拉多.md "wikilink")，之後被判處此刑。所以[基督十字也是基督教重要的象徵](../Page/十字架_\(基督教\).md "wikilink")。在[歐美文學中](../Page/歐美.md "wikilink")，一般用十字架比喻苦難。

作為刑具的十字架，曾流行使用于[巴比倫](../Page/巴比倫.md "wikilink")[波斯帝國](../Page/波斯帝國.md "wikilink")、[大馬士革王國](../Page/大馬士革.md "wikilink")、[猶大王國](../Page/猶大王國.md "wikilink")、[以色列王國](../Page/以色列王國.md "wikilink")、[迦太基和](../Page/迦太基.md "wikilink")[古羅馬等地](../Page/古羅馬.md "wikilink")，常用以處死[叛逆者](../Page/謀反.md "wikilink")、[異教徒](../Page/異教徒.md "wikilink")、[奴隸和沒有](../Page/奴隸.md "wikilink")[公民權的人](../Page/公民權.md "wikilink")。在當時的社會，這種死刑方式是一種忌諱。由於消耗的資源很大，一年通常只會處死數人，對象是極度重犯。西元337年，[羅馬皇帝](../Page/羅馬皇帝.md "wikilink")[君士坦丁大帝下令禁用此刑具](../Page/君士坦丁大帝.md "wikilink")。現除了每年在[菲律賓的](../Page/菲律賓.md "wikilink")[復活節](../Page/復活節.md "wikilink")，願體驗耶穌被釘死的信徒還使用此刑具作为一种仪式。

## 十字架的行刑方式

刑具的[形狀是兩條架成十字形的原木](../Page/形狀.md "wikilink")。行刑前，犯人會先行背著十字架的橫木遊街，直至走到行刑場所。行刑方法是先把犯人的雙[手打橫張開](../Page/手.md "wikilink")，並用長釘穿過[前臂兩條骨之間](../Page/桡骨.md "wikilink")，把手臂釘在一條橫木上，再把橫木放在一條[垂直的木上](../Page/垂直.md "wikilink")，再把雙腳釘在直木上面，然後把十字架豎起來，任他慢慢死去。若時限到了之前犯人還未死去，看守者會把犯人的雙腿打斷，加速犯人的死亡。因為釘在十字架上的死囚由於[背部緊貼十字架](../Page/背部.md "wikilink")，無法令[肺部有充足擴張的空間納入新鮮空氣](../Page/肺部.md "wikilink")，故呼吸時需依靠雙腿撐起全身，令背部稍微離開十字架。雙腿若被打斷，犯人亦無法撐起身體，最終因[缺氧](../Page/缺氧.md "wikilink")[窒息而死亡](../Page/窒息.md "wikilink")。\[1\]\[2\]\[3\]

## [天主教](../Page/天主教.md "wikilink")

## 十字架的意義

源於聖經中，耶穌屈尊就卑、紆尊降貴、捨己為人，經歷種種苦難，犠牲自己為人類受苦受難至死，為紀念此事，紀念衪對人類的愛，並警醒世人要因為自己的罪而悔改，因此以衪受刑的刑具作整個教會的標誌，亦指出教會應該以謙遜、刻苦的生活去成聖自己、聖化他人及轉化世界。

## 放置十字架的原則

就天主教而言，一般放置十字架會有的原則： a.放於當眼位置 b.每個空間一個 c.十字架通常置於每個空間最高及最中心的位置

## 每間教堂的十字架的樣式

外圍以簡約為主，室內以有耶穌像的苦架為主，然而，決定使用甚麼樣式的十字存是由該堂區的設計師與堂區神父擬定。

## 設計十字架的理念

如神父或堂區沒有特別要求，都會以傳統的拉丁十字架作標誌。

## 聖堂包含十字架的物品

1.  聖體櫃
2.  聖體聖血布、九摺布
3.  聖體
4.  聖體盒
5.  聖體皓光
6.  坐櫈
7.  14處苦路
8.  讀經台
9.  祭台布

## 日本

在[日本](../Page/日本.md "wikilink")，使用的十字架称之为（**磔**）。用该[刑具執行的](../Page/刑具.md "wikilink")[死刑被稱為](../Page/死刑.md "wikilink")**[磔刑](../Page/磔刑.md "wikilink")**。而磔在中国指的是[凌迟](../Page/凌迟.md "wikilink")。

### 磔刑的種類

日本[江戶時代中期以後開始有十字架处死的磔刑和称为](../Page/江戶時代.md "wikilink")[鋸挽](../Page/鋸挽.md "wikilink")（）的情況。磔刑適用於犯人破壞關所、偽造貨幣、傷害主人和親人等的情況，鋸挽適用於犯人殺死主人和親人的情況。受刑者會從[小傳馬町的牢房中被帶出並遊街示眾](../Page/小傳馬町.md "wikilink")。磔刑的情況是遊街後被帶往刑場。鋸挽的情況是把頭以外的部份埋在地下兩日，之後再執行磔刑。

### 執行磔刑的方法

處刑是公開執行的，牢内的犯人如果在認罪後在獄中死去，則會以鹽漬的方式保存屍體，經判決後继续執行处刑。

首先，在刑場脫去犯人的衣服，把手、足、胸、腰部等用繩索綑綁在刑柱上（為了讓長槍能刺到身體，會露出兩乳和腹部，把一部份衣服剝去，用布束著身體中央），然後把刑柱豎立在地面。刑柱的形狀根據性別而有所不同，男性用「キ」（卄轉90度）形的柱子，而女性用「十」字形的柱子；男性用股間部來承重，而女性用腳下的承重台來承重。

檢察使的[與力](../Page/與力.md "wikilink")（辅佐官）收到的（小吏）的準備和報告後，任命[同心](../Page/同心_\(官職\).md "wikilink")（治安官）確認受刑者是本人。

持長槍的死刑执行者属于[非人身分](../Page/非人.md "wikilink")，與手代一起站在磔柱的左右，最初把長槍交叉放在受刑者的面前（被稱為「」）。隨著「阿利也阿利也（）」的大叫聲，把長槍扭刺入右腹至左肩，不斷穿刺受刑者的身體（正式的做法是把槍刃從肩部突出一尺），之後再貫穿左腹至右肩，不斷以同樣的次順左右交互地用長槍貫穿。受刑者主要會因為大量出血而陷入[休克狀態](../Page/休克.md "wikilink")，被刺兩至三次就會死亡，但是屍體仍然會被重複穿刺30多次。情況相當凄慘。與西洋的十字架刑的過程和方式完全不同，事實上是以長槍執行的[死刑](../Page/死刑.md "wikilink")。通過對[消化器官至](../Page/消化器官.md "wikilink")[肺等的範圍](../Page/肺.md "wikilink")[內臟給予損傷](../Page/內臟.md "wikilink")，而且長槍會刺到骨和令到受刑者呼吸困難，比起[斬首的痛苦大得多](../Page/斬首.md "wikilink")。

最後用長柄連上罪人的[髷並令面部向上](../Page/髷.md "wikilink")，用長槍由右至左貫穿受刑者喉嚨（被稱為「」）。屍體被放置三日曝曬後，非人會將其放入洞穴中。之後屍體會被鳥和野犬等處理。

在[東京都](../Page/東京都.md "wikilink")[品川區的](../Page/品川區.md "wikilink")[鈴森刑場遺跡中](../Page/鈴森刑場.md "wikilink")，有為了豎立磔柱而使用的基石殘留。

### 其他磔刑

[江戶時代前期之前](../Page/江戶時代.md "wikilink")，執行磔刑的方法有很多，[藩和大名的執行方法有相當大的差異](../Page/藩.md "wikilink")。有「[串刺刑](../Page/穿刺.md "wikilink")」等名稱在記錄中，實際就是磔刑。但是在小說中描述，用長槍刺入[肛門的方法](../Page/肛門.md "wikilink")，在現實中技術上的執行相當困難。

在海岸滿潮時，把受刑者的頭放入海中的磔刑被稱為「[水磔](../Page/水磔.md "wikilink")」，但是那是倒吊受刑者，另一種「磔刑」。

## 伊斯蘭教

《[古蘭經](../Page/古蘭經.md "wikilink")》第5章33節：「敵對真主和使者，而且擾亂地方的人，他們的報酬，只是處以死刑，或釘死在十字架上，或把手腳交互著割去，或驅逐出境。這是他們在今世所受的凌辱；他們在後世，將受重大的刑罰。」

21世紀興起的恐怖組織[伊斯蘭國也使用釘十字架作為處決方式之一](../Page/伊斯蘭國.md "wikilink")。\[4\]

## 著名受刑者

普遍认同的受刑者包括：

  - [耶穌](../Page/耶穌.md "wikilink")
  - [耶穌的十二使徒](../Page/耶穌的十二使徒.md "wikilink")：[彼得與](../Page/彼得.md "wikilink")[安得烈](../Page/安得烈.md "wikilink")

斯巴達克斯起義失敗後，被克拉蘇俘虜的六千名奴隸，被釘死在從卡普亞通往羅馬城的阿庇亞大道沿線的十字架上。
另外，基督教非傳統教派之一[耶和華見證人](../Page/耶和華見證人.md "wikilink")，則引證考古文獻認為耶穌其實是死在一根直立的木柱上。

## 相關條目

  - [十字架 (基督教)](../Page/十字架_\(基督教\).md "wikilink")
  - [聖彼得十字](../Page/聖彼得十字.md "wikilink")
  - [十字](../Page/十字.md "wikilink")
  - [T型十字架](../Page/T型十字架.md "wikilink")
  - [踏十字架](../Page/踏十字架.md "wikilink")

## 參考文獻

## 外部連結

  - [十字架：它的起源和意義](http://www.ccg.org/chinese/z/p039z.htm)
  - [《大英百科全書》11部，第7版，第505頁](http://www.britannica.com/)
  - [石頭還在呼喊！(新約篇)：十字架！十字架！永是我的榮耀！](http://www.pcchong.com/Stones2/Stone2_13.htm)
  - [1](http://www.churchinhk.org.hk/Se_Content.asp?CID=566)

[Category:十字架](../Category/十字架.md "wikilink")
[Category:基督教术语](../Category/基督教术语.md "wikilink")
[Category:死刑](../Category/死刑.md "wikilink")
[Category:酷刑](../Category/酷刑.md "wikilink")
[Category:刑具](../Category/刑具.md "wikilink")
[刑](../Category/古羅馬.md "wikilink")
[刑](../Category/酷刑.md "wikilink")
[刑](../Category/姿势.md "wikilink")

1.  [Columbia University page of Pierre Barbet on
    Crucifixion](http://www.columbia.edu/cu/augustine/arch/barbet.html)
2.
3.
4.