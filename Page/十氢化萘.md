**十氢化萘**，又名**十氢萘**或**萘烷**，[分子式为](../Page/分子式.md "wikilink")，由两个[环己烷并合形成](../Page/环己烷.md "wikilink")，是有芳香气味的无色液体。它用作工业[溶剂](../Page/溶剂.md "wikilink")，可以溶解许多[树脂](../Page/树脂.md "wikilink")。十氢化萘可看作[萘完全](../Page/萘.md "wikilink")[氢化的产物](../Page/氢化.md "wikilink")，并且也可通过萘的[催化氢化制备](../Page/催化.md "wikilink")。空气存在时，十氢化萘很容易生成[爆炸性的](../Page/爆炸性.md "wikilink")[有机过氧化物](../Page/有机过氧化物.md "wikilink")。

萘的位号沿用至十氢化萘中，而且中间的两个碳原子中，离8号位近的定为9号碳，另一个为10号。

## 异构体

十氢化萘有[顺反异构体](../Page/顺反异构.md "wikilink")，顺式和反式异构体都为两个[椅型环己烷并合而成](../Page/环己烷构象.md "wikilink")，但反式邻交叉的[相互作用较少](../Page/空间相互作用.md "wikilink")，更加稳定。此外，反式没有[构象异构体](../Page/构象异构体.md "wikilink")，但顺式十氢化萘存在两个构象异构体。

<File:Cis-decalin-3D-balls.png>|

<center>

顺式十氢化萘的[球棍模型](../Page/球棍模型.md "wikilink")

</center>

<File:Trans-decalin-3D-balls.png>|

<center>

反式十氢化萘

</center>

## 参考资料

  - 邢其毅等。《基础有机化学》第三版上册。北京：高等教育出版社，2005年。ISBN 7-04-016637-2

[Category:环烷烃](../Category/环烷烃.md "wikilink")
[Category:烃类溶剂](../Category/烃类溶剂.md "wikilink")
[Category:萘的衍生物](../Category/萘的衍生物.md "wikilink")
[Category:六元环](../Category/六元环.md "wikilink")
[Category:萘烷](../Category/萘烷.md "wikilink")