**千屈菜科**（[学名](../Page/学名.md "wikilink")：），
包括32[属大约](../Page/属.md "wikilink")500-600[种](../Page/种.md "wikilink")[植物](../Page/植物.md "wikilink")，绝大部分是[草本](../Page/草本.md "wikilink")，也有少数[灌木或](../Page/灌木.md "wikilink")[乔木](../Page/乔木.md "wikilink")。广泛分布在全世界[热带和](../Page/热带.md "wikilink")[温带地区](../Page/温带.md "wikilink")。

2003年的[APG II
分类法将](../Page/APG_II_分类法.md "wikilink")[石榴科和菱科劃歸成為千屈菜科的亞科](../Page/石榴科.md "wikilink")。

## 属

  - **千屈菜亚科** Lythroideae
    <small>[Juss.](../Page/Antoine_Laurent_de_Jussieu.md "wikilink") ex
    [Arn.](../Page/Arn..md "wikilink") 1832</small> = 28 属：
      - *[Adenaria](../Page/Adenaria.md "wikilink")*
      - [水苋菜属](../Page/水苋菜属.md "wikilink") *Ammannia*
      - *[Capuronia](../Page/Capuronia.md "wikilink")*
      - *[Crenea](../Page/Crenea.md "wikilink")*
      - [萼距花属](../Page/萼距花属.md "wikilink") *Cuphea*
      - *[Decodon](../Page/Decodon.md "wikilink")*
      - [牛顿草属](../Page/牛顿草属.md "wikilink") *Didiplis*
      - *[Diplusodon](../Page/Diplusodon.md "wikilink")*
      - *[Galpinia](../Page/Galpinia.md "wikilink")*
      - *[Ginoria](../Page/Ginoria.md "wikilink")*
      - *[Haitia](../Page/Haitia.md "wikilink")*
      - [黄薇属](../Page/黄薇属.md "wikilink") *Heimia*
      - *[Hionanthera](../Page/Hionanthera.md "wikilink")*
      - *[Koehneria](../Page/Koehneria.md "wikilink")*
      - [丽薇属](../Page/丽薇属.md "wikilink") *Lafoensia*
      - [紫薇属](../Page/紫薇属.md "wikilink") *Lagerstroemia*
      - [指甲花属](../Page/指甲花属.md "wikilink") *Lawsonia*
      - *[Lourtella](../Page/Lourtella.md "wikilink")*
      - [千屈菜属](../Page/千屈菜属.md "wikilink") *Lythrum*
      - [非洲红柳属](../Page/非洲红柳属.md "wikilink") '' Nesaea''
      - *[Pehria](../Page/Pehria.md "wikilink")*
      - [水芫花属](../Page/水芫花属.md "wikilink") *Pemphis*
      - [荸艾属](../Page/荸艾属.md "wikilink") *Peplis*
      - *[Physocalymma](../Page/Physocalymma.md "wikilink")*
      - *[Pleurophora](../Page/Pleurophora.md "wikilink")*
      - [节节菜属](../Page/节节菜属.md "wikilink") *Rotala*
      - *[Tetrataxis](../Page/Tetrataxis.md "wikilink")*
      - [虾子花属](../Page/虾子花属.md "wikilink") *Woodfordia*
  - **石榴亚科** Punicoideae <small>([Horan.](../Page/Horan..md "wikilink")
    1834) S. A. Graham, Thorne & Reveal 1998</small> =
    '[石榴科](../Page/石榴科.md "wikilink")'
      - [石榴属](../Page/石榴属.md "wikilink") *Punica*
  - **海桑亚科** Sonneratioideae
    <small>([Engl.](../Page/Adolf_Engler.md "wikilink") &
    [Gilg](../Page/Gilg.md "wikilink") 1924) S. A. Graham, Thorne &
    Reveal 1998</small>
      - [海桑属](../Page/海桑属.md "wikilink") *Sonneratia*
  - **八宝树亚科** Duabangoideae
    <small>([Takht.](../Page/Takht..md "wikilink") 1986) S. A. Graham,
    Thorne & Reveal 1998</small> = '[八宝树科](../Page/八宝树科.md "wikilink")'
      - [八宝树属](../Page/八宝树属.md "wikilink") *Duabanga*
  - **菱亚科** Trapoideae <small>[Voigt](../Page/Voigt.md "wikilink")
    1845</small> = '[菱科](../Page/菱科.md "wikilink")'
      - [菱属](../Page/菱属.md "wikilink") *Trapa*

<!-- end list -->

  - 紫薇属
      - [九芎](../Page/九芎.md "wikilink")
      - [大花紫薇](../Page/大花紫薇.md "wikilink")
      - [紫薇](../Page/紫薇.md "wikilink")

## 参考文献

  - 林春吉，《台灣的水生與溼地植物》，2005，台北，綠世界出版社。

  -
  -
  - L. Watson和M.J. Dallwitz
    (1992年)《[显花植物分类](https://web.archive.org/web/20070103200438/http://delta-intkey.com/angio/)》中的[千屈菜科](https://web.archive.org/web/20050424135704/http://delta-intkey.com/angio/www/lythrace.htm)

[\*](../Category/千屈菜科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")