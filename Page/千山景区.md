[Qianshan_National_Park_1,_China.jpg](https://zh.wikipedia.org/wiki/File:Qianshan_National_Park_1,_China.jpg "fig:Qianshan_National_Park_1,_China.jpg")
**千山景区**位于[辽宁省](../Page/辽宁省.md "wikilink")[鞍山市中心东南约](../Page/鞍山市.md "wikilink")25公里处，是[千山山脉的精华部分](../Page/千山山脉.md "wikilink")。为中国政府在1982年公布的首批[国家级风景名胜区之一](../Page/国家级风景名胜区.md "wikilink")，亦是[中国国家5A级旅游景区](../Page/中国国家5A级旅游景区.md "wikilink")。千山主峰[仙人台海拔](../Page/仙人台.md "wikilink")708.5米（游人可及的最高点）。千山景区面积约有50平方公里，以[弥勒峰为核心共分为四大景区](../Page/弥勒峰.md "wikilink")（北、中、南、西），大体上北西两区多寺庙古迹，中南两区多重山密林。千山虽地处塞外，但山体挺拔秀丽景色优美。素有**东北明珠**和**辽东第一山**之美誉。景区内既有七寺、十二观、九宫、十庵、唐代古城遗址等约两百多处人文景观和遗迹，又有御笔山、小黄山、仙人台国家森林公园等自然风光。千山诸峰集险、峻、秀、奇、幽等壮美景色于一体。1996年，原全国人大常委会副委员长、当代著名社会学家[费孝通先生游览千山后留下了](../Page/费孝通.md "wikilink")**“南有黄山，北有千山”**的题词。
[Qianshan_National_Park_2,_China.jpg](https://zh.wikipedia.org/wiki/File:Qianshan_National_Park_2,_China.jpg "fig:Qianshan_National_Park_2,_China.jpg")

## 景区景点

### 宗教

千山本集[佛教和](../Page/佛教.md "wikilink")[道教于一山](../Page/道教.md "wikilink")，在景区范围内和周边散落着数十座佛道寺庙（全盛时期约有大小庙宇近百座，现存的约有四十余处），几乎每一道山谷沟壑之中都能听到钟罄之声。

#### 佛教

千山的主要佛寺有[龙泉寺](../Page/龙泉寺.md "wikilink")、[祖越寺](../Page/祖越寺.md "wikilink")、[香岩寺](../Page/香岩寺.md "wikilink")、[中会寺](../Page/中会寺.md "wikilink")、[大安寺](../Page/大安寺.md "wikilink")（这五座佛教[禅宗寺庙也被称为千山的](../Page/禅宗.md "wikilink")**五大禅林**，它们在明代以前就已经很有名气），此外还有[灵岩寺](../Page/灵岩寺.md "wikilink")、[大佛寺](../Page/大佛寺.md "wikilink")、[慈航寺](../Page/慈航寺.md "wikilink")、[皈源寺](../Page/皈源寺.md "wikilink")（龙泉庵）、[朝阳寺](../Page/朝阳寺.md "wikilink")、[鎏金庵](../Page/鎏金庵.md "wikilink")、[南泉庵](../Page/南泉庵.md "wikilink")、[木鱼庵等寺庙](../Page/木鱼庵.md "wikilink")。在千山北部景区内，有一座山峰呈天然的弥勒佛形状，被称为[弥勒峰俗称](../Page/弥勒峰.md "wikilink")[千山大佛](../Page/千山大佛.md "wikilink")（它与[中会寺铜佛和](../Page/中会寺.md "wikilink")[玉佛苑的玉佛一起](../Page/玉佛苑.md "wikilink")，成为鞍山所特有的“三大佛”）。

#### 道教

道教大致在明清时期开始大规模的进入千山，现在景区内尚存有多座著名的道观，其中以[无量观的规模最为宏大](../Page/无量观.md "wikilink")，同时它也是[道教全国重点宫观](../Page/道教全国重点宫观.md "wikilink")。无量观又名“无梁观”，相传是因为道观初建时无梁而得名，始建于隋唐盛于明清。现在的无量观是清康熙六年(1667年)，由道教[全真道](../Page/全真道.md "wikilink")[龙门派第八代弟子郭守真的徒弟刘太琳所创建](../Page/龙门派.md "wikilink")。此外，千山风景区内还有[圆通观](../Page/圆通观.md "wikilink")、[凤朝观](../Page/凤朝观.md "wikilink")、[普安观](../Page/普安观.md "wikilink")、[五龙宫](../Page/五龙宫.md "wikilink")、[太和宫](../Page/太和宫.md "wikilink")、[东极宫等道观也非常值得游览](../Page/东极宫.md "wikilink")。

### 古树

千山的植被茂密物种非常丰富，风景区内有许多著名的古树奇松，其中比较有代表性的有：[香岩寺内的](../Page/香岩寺.md "wikilink")[蟠龙松](../Page/蟠龙松.md "wikilink")，九重天附近的[探海松](../Page/探海松.md "wikilink")，一步登天下面的[可怜松等](../Page/可怜松.md "wikilink")。

### 怪石

千山也有很多造型怪异栩栩如生的奇石，如只有身材苗条的人才能通过的[夹扁石](../Page/夹扁石.md "wikilink")、敲击时能发出跟木鱼一样声音的[木鱼石](../Page/木鱼石.md "wikilink")。此外，还有[卧龙石](../Page/卧龙石.md "wikilink")、[寿星石](../Page/寿星石.md "wikilink")、[巨人石](../Page/巨人石.md "wikilink")、[鹦鹉石和](../Page/鹦鹉石.md "wikilink")[无根石等等](../Page/无根石.md "wikilink")。

### 其他景点

此外，著名的景点还有[五佛顶](../Page/五佛顶.md "wikilink")（游人可到的第二高峰，海拔554.1米）、[夕霞峰](../Page/夕霞峰.md "wikilink")（海拔590米，只能远观不能登临）、[弥勒峰](../Page/弥勒峰.md "wikilink")、[卧虎峰](../Page/卧虎峰.md "wikilink")、[佛手峰](../Page/佛手峰.md "wikilink")、[玉霞关](../Page/玉霞关.md "wikilink")、[一线天](../Page/一线天.md "wikilink")、[天上天](../Page/天上天.md "wikilink")、[天外天](../Page/天外天.md "wikilink")、[罗汉洞等](../Page/罗汉洞.md "wikilink")。一年四季都可游览千山，不同的季节会领略不同韵味的千山。春天来到千山，可观赏遍布山谷的梨花盛开；在盛夏，千山则是一处避暑胜地，无论烈日如何炽热山中却只见无边无际的翠绿，耳畔只能闻听到泉水叮咚；清秋时节漫山遍野是深红色的枫叶，其景堪比北京香山；冬季，千山又是赏雪和玩雪的绝佳之地，每年山内约有近五个月的积雪期，这是关内名山所无法比拟的。

## 文化

### 节日

  - **大佛节**，大致在每年的六月六日(东南亚上座部佛教的世界佛陀日前后)。
  - **春节庙会**，从每年的大年初一开始。

### 历代名人

历史上到过千山的名人不少。相传[唐太宗](../Page/唐太宗.md "wikilink")[李世民东征时曾到过千山并住在](../Page/李世民.md "wikilink")[大安寺](../Page/大安寺.md "wikilink")，北沟景区还有据说是他当年曾抖过战袍的振衣冈。[金世宗](../Page/金世宗.md "wikilink")[完颜雍曾到过千山](../Page/完颜雍.md "wikilink")[灵岩寺探母](../Page/灵岩寺.md "wikilink")（其生母贞懿皇后李氏）。清代的[康熙](../Page/康熙.md "wikilink")、[乾隆](../Page/乾隆.md "wikilink")、[嘉庆三帝都曾到过千山或在](../Page/嘉庆.md "wikilink")[辽阳城头远望过千山](../Page/辽阳.md "wikilink")。[曹雪芹祖父](../Page/曹雪芹.md "wikilink")[曹寅的籍贯就在千山](../Page/曹寅.md "wikilink")，著名的关东才子[王尔烈曾在千山](../Page/王尔烈.md "wikilink")[龙泉寺读书](../Page/龙泉寺.md "wikilink")，该寺至今还保留着他的书房。从唐宋到明清不少官宦和名人都曾到千山游览过，他们还都留下过不少摩崖题刻、匾额和诗词，如[陆游的](../Page/陆游.md "wikilink")《[沁园春.有感](../Page/沁园春.有感.md "wikilink")》、[康熙的](../Page/康熙.md "wikilink")《[入千山](../Page/入千山.md "wikilink")》等。

### 影视、歌曲

千山曾经是1987年版电视剧《[红楼梦](../Page/红楼梦_\(1987年电视剧\).md "wikilink")》，还有[电影](../Page/电影.md "wikilink")《[古刹钟声](../Page/古刹钟声.md "wikilink")》的重要外景地。歌曲《[木鱼石的传说](../Page/木鱼石的传说.md "wikilink")》的渊源也来自千山的[木鱼石](../Page/木鱼石.md "wikilink")。

### 出版物

  - [中国国家邮政局于](../Page/中国国家邮政局.md "wikilink")2002年4月26日发行了《千山》[特种邮票](../Page/特种邮票.md "wikilink")1套4枚，编号为2002-8。四枚[邮票分别为](../Page/邮票.md "wikilink")：（4-1）无量观，面值80分；（4-2）弥勒峰，面值80分；（4-3）龙泉寺，面值80分；（4-4）仙人台，面值2.80元。

## 特色特产

### 温泉

[千山温泉在千山正门以北约两公里处](../Page/千山温泉.md "wikilink")，（乘8路温泉站下车）此处建有多家假日酒店，这里的浴池、游泳馆以及所有客房的热水均来自地下的[温泉](../Page/温泉.md "wikilink")。千山温泉出水温度高，对关节炎和皮肤病有一定治疗功效。

### 南果梨

[南果梨是千山特有的一种水果](../Page/南果梨.md "wikilink")，比正常的梨要小，但味道独特口感甜美。由于该梨极难保存，因此推广难度很大外地人也并不知晓。据考证，南果梨的祖树位于千山西麓的上对桩石村。[鞍山市](../Page/鞍山市.md "wikilink")[千山区的东部山区是南果梨的主要产地](../Page/千山区.md "wikilink")，每年中秋节前后是南果梨味道最好的时候。近年来随着保鲜技术和交通条件的改善，南果梨也会出现在[北京](../Page/北京.md "wikilink")、[上海和](../Page/上海.md "wikilink")[广州这样的大城市](../Page/广州.md "wikilink")，不过数量还是很稀少价格也较贵。

## 交通

### 对外交通

#### 航空

距离千山最近的[机场为](../Page/机场.md "wikilink")[鞍山腾鳌机场](../Page/鞍山腾鳌机场.md "wikilink")，距离千山正门约有30多公里，目前已经开通有往返于[北京首都国际机场](../Page/北京首都国际机场.md "wikilink")、[上海浦东国际机场](../Page/上海浦东国际机场.md "wikilink")、[广州新白云国际机场](../Page/广州新白云国际机场.md "wikilink")、[南京禄口国际机场和](../Page/南京禄口国际机场.md "wikilink")[成都双流国际机场的定期航班](../Page/成都双流国际机场.md "wikilink")（其中北京、上海每天一班，南京、广州1、3、5、日，成都3、5、日一班）。游客从鞍山机场出发，可搭乘机场专线到五一路公交中转枢纽转乘8路公共汽车到达千山。离千山最近的[国际机场是](../Page/国际机场.md "wikilink")[沈阳桃仙国际机场](../Page/沈阳桃仙国际机场.md "wikilink")，距千山约100公里，可直达国内各主要城市以及日韩、[东南亚和](../Page/东南亚.md "wikilink")[欧洲等地](../Page/欧洲.md "wikilink")。推荐乘坐市际机场大巴到达鞍山城市候机楼再乘坐38路转8路，若机场大巴终点为鞍山西客站，也可乘坐126路转8路。

#### 铁路与公交

距千山最近的火车站是[鞍山火车站](../Page/鞍山火车站.md "wikilink")，距离景区17公里，可抵达东北各地以及[北京](../Page/北京.md "wikilink")、[上海](../Page/上海.md "wikilink")、[深圳等国内主要大城市](../Page/深圳.md "wikilink")。从鞍山火车站出站后向北步行，可搭乘8路公共汽车到达千山北门（正门）车程约40分钟（途中可经过鞍山另一处著名风景区[玉佛苑](../Page/玉佛苑.md "wikilink")）。也可以乘坐10路转45路公共汽车抵达南部景区[香岩寺附近的上石桥](../Page/香岩寺.md "wikilink")（下车后继续步行），从这里是攀登主峰仙人台（鞍山城区的海拔最高点）最近的出发地。哈大高铁开通后经停[鞍山西站](../Page/鞍山西站.md "wikilink")，从这可搭乘高铁直达[沈阳](../Page/沈阳.md "wikilink")、[大连](../Page/大连.md "wikilink")、[哈尔滨等诸多东北重要城市](../Page/哈尔滨.md "wikilink")，也可以快速抵达北京和上海，去西站需要在鞍山站前倒车（22路转8路）才能到达。

#### 公路

[沈大高速公路G](../Page/沈大高速公路.md "wikilink")15、[丹海高速公路G](../Page/丹海高速公路.md "wikilink")16、[辽中环线高速公路G](../Page/辽中环线高速公路.md "wikilink")91和众多国道、省道（G202、S316、S101等）通往[沈阳](../Page/沈阳市.md "wikilink")、[大连](../Page/大连市.md "wikilink")、[本溪](../Page/本溪市.md "wikilink")、[丹东](../Page/丹东市.md "wikilink")、[盘锦](../Page/盘锦市.md "wikilink")、[营口](../Page/营口市.md "wikilink")、[岫岩](../Page/岫岩满族自治县.md "wikilink")、[海城](../Page/海城市.md "wikilink")、[辽阳等许多市县的公路经过](../Page/辽阳市.md "wikilink")[鞍山周边](../Page/鞍山.md "wikilink")。

#### 水运

营口的[鲅鱼圈港是离](../Page/鲅鱼圈.md "wikilink")[鞍山](../Page/鞍山.md "wikilink")（**千山**）最近的港口（约100公里），从该港可乘坐开往环[渤海以及日韩地区的游轮](../Page/渤海.md "wikilink")。此外，[大连港距](../Page/大连港.md "wikilink")[鞍山](../Page/鞍山.md "wikilink")（**千山**）约320公里，在那里可以搭乘汽车轮渡到达山东半岛，是[华东和](../Page/华东.md "wikilink")[东北两大区域之间的一条交通捷径](../Page/东北.md "wikilink")。

### 山内交通

景区内有数量众多的山内旅游车往返于各景区之间（夏季）。在北部景区有天上天、五佛顶和千山大佛等三条登山[索道](../Page/索道.md "wikilink")，通往各主要山峰景点的山路均有比较平整的石阶。

## 参考文献

  - 鞍山地方志：《鞍山市志》、《千山区志》（原〈旧堡区志〉）

## 外部链接

  - [千山--首页](http://www.qianshan.ln.cn/)

{{-}}

[Category:中國國家級風景名勝區](../Category/中國國家級風景名勝區.md "wikilink")
[Q](../Category/鞍山旅游景点.md "wikilink")
[Category:道教名山](../Category/道教名山.md "wikilink")