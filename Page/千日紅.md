**千日紅**（學名：*Gomphrena
globosa*），又名**圓仔花**（）\[1\]、**百日红**（广州）、**火球花**（北京）、**紅繡球**，原產於熱帶[美洲各國內地](../Page/美洲.md "wikilink")，一般在春天播種繁殖。千日紅的[花語是](../Page/花語.md "wikilink")“永恆、不變的愛”。分布于美洲热带以及中国大陆的南北各省等地，目前已由人工引种栽培。

## 特征

[2228_-_Salzburg_-_Flowers.JPG](https://zh.wikipedia.org/wiki/File:2228_-_Salzburg_-_Flowers.JPG "fig:2228_-_Salzburg_-_Flowers.JPG")
一年生草本，高20～60公分，全株密被[白色長毛](../Page/白色.md "wikilink")。有栽培有高性和矮性品種，莖直立，有分枝，近四稜形，具溝紋，節部膨大，帶紫紅色，密被白色柔毛。單葉對生，葉柄長約1公分，上端葉幾無柄；葉片長圓形至橢圓形，長5～10公分，寬2～4公分，先端鈍或尖，基部楔形，全緣，兩面被白色長柔毛和緣毛。

花期夏季，開紫紅色、白色或淡紅色花，頭狀花序球形或長圓形。

千日紅屬[陽生植物](../Page/陽生植物.md "wikilink")，日照需充足，日照不足时不易開花或疏少。

## 用途

可治[支氣管](../Page/支氣管.md "wikilink")[哮喘](../Page/哮喘.md "wikilink")，急、慢性[支氣管炎之咳嗽](../Page/支氣管炎.md "wikilink")、哮喘，百日咳，肺結核咯血，頭暈，視物模糊，痢疾，肝熱目痛，小便不利，瘰歷，瘡瘍，跌打損傷，以及小兒癲癇，腹脹，驚風，夜啼。

## 参考文献

<div class="references-small">

  -

</div>

## 外部連結

  - [千日紅
    Qianrihong](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00736)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[globosa](../Category/千日红属.md "wikilink")
[Category:球根花卉](../Category/球根花卉.md "wikilink")

1.  [圓仔花－臺灣閩南語常用詞辭典](http://twblg.dict.edu.tw/holodict_new/result_detail.jsp?n_no=9395&source=9&level1=4&level2=15&level3=0&curpage=0&sample=0&radiobutton=0&querytarget=0&limit=1&pagenum=0&rowcount=0&cattype=1)