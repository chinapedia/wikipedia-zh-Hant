《**千機變**》（英文名：），2003年製作的[香港電影](../Page/香港電影.md "wikilink")。這是一套為兩位女主角、當時香港的青春偶像[Twins度身訂做的電影](../Page/Twins.md "wikilink")。

## 劇情簡介

圣典“Day for
Night”是[僵尸皇族的珍宝](../Page/僵尸.md "wikilink")，传说它拥有摧毁人类世界的力量，为Kazaf（[陈冠希](../Page/陈冠希.md "wikilink")）收藏，僵尸伯爵对此虎视眈眈。Kazaf與管家Prada（[黃秋生](../Page/黃秋生.md "wikilink")）經地產經紀Momoko（[蔣雅文](../Page/蔣雅文.md "wikilink")）介紹下居住於一間無人教堂內。[僵尸猎人Reeve](../Page/僵尸猎人.md "wikilink")（[郑伊健](../Page/郑伊健.md "wikilink")）到处游历，拍档Lila（[何超儀](../Page/何超儀.md "wikilink")）遇害后，为了对抗僵尸伯爵，Reeve训练Lila之妹Gypsy（[鍾欣潼](../Page/鍾欣潼.md "wikilink")）增强实力，但Reeve的妹妹Helen（[蔡卓妍](../Page/蔡卓妍.md "wikilink")）视Gypsy为入侵者。

僵尸王子Kazaf邂逅Helen后希望能过人类的生活，却发生了一些不同寻常的事：Kazaf被僵尸伯爵追杀，Helen冒死相救并把他藏在家中，被Gypsy碰见；Reeve追捕僵尸时反中埋伏成为伯爵的猎物…………

## 角色列表

|                                                        |             |
| ------------------------------------------------------ | ----------- |
| **演　員**                                                | **角　色**     |
| [郑伊健](../Page/郑伊健.md "wikilink")                       | Reeve       |
| [蔡卓妍](../Page/蔡卓妍.md "wikilink")                       | Helen       |
| [鍾欣潼](../Page/鍾欣潼.md "wikilink")                       | Gypsy       |
| [陈冠希](../Page/陈冠希.md "wikilink")                       | Kazaf       |
| [黃秋生](../Page/黃秋生.md "wikilink")                       | Prada       |
| [蔣雅文](../Page/蔣雅文.md "wikilink")                       | Momoko      |
| [何超儀](../Page/何超儀.md "wikilink")                       | Lila        |
| [劉思惠](../Page/劉思惠.md "wikilink")                       | Maggie      |
| [成　龍](../Page/成龍.md "wikilink")                        | Jackie Fong |
| [莫文蔚](../Page/莫文蔚.md "wikilink")                       | Ivy         |
| [杜汶澤](../Page/杜汶澤.md "wikilink")                       | Ken         |
| [鄒凱光](../Page/鄒凱光.md "wikilink")                       | 司儀          |
| [張達明](../Page/張達明.md "wikilink")                       | 伴郎          |
| [林尚義](../Page/林尚義.md "wikilink")                       | Jackie父親    |
| [伍允龍](../Page/伍允龍.md "wikilink")                       | 殭屍          |
| [Mark Strange](../Page/:en:Mark_Strange.md "wikilink") | 殭屍          |
| [Bey Logan](../Page/:en:Bey_Logan.md "wikilink")       | 殭屍          |
| [Don Ferguson](../Page/:en:Don_Ferguson.md "wikilink") | 殭屍          |

## 制作人员

  - 導演：[林超賢](../Page/林超賢.md "wikilink")
  - 聯合導演、動作指導：[甄子丹](../Page/甄子丹.md "wikilink")
  - 編劇：[陳慶嘉](../Page/陳慶嘉.md "wikilink")、[吳煒倫](../Page/吳煒倫.md "wikilink")
  - 監製：[張承勷](../Page/張承勷.md "wikilink")
  - 攝影指導：[張文寶](../Page/張文寶.md "wikilink") (HKSC)
  - 剪接：[陳祺合](../Page/陳祺合.md "wikilink") (HKSE)
  - 燈光：[伍文拯](../Page/伍文拯.md "wikilink") (HKSC)
  - 原創音樂：[陳光榮](../Page/陳光榮.md "wikilink")
  - 助理動作指導：[谷垣健治](../Page/谷垣健治.md "wikilink")、[黃偉亮](../Page/黃偉亮.md "wikilink")
  - 美術指導：[雷楚雄](../Page/雷楚雄.md "wikilink")
  - 服裝指導：戴美玲、[歐陽霞](../Page/歐陽霞.md "wikilink")
  - 造型設計：[奚仲文](../Page/奚仲文.md "wikilink")
  - 音響設計師：[曾景祥](../Page/曾景祥.md "wikilink")
  - 視覺效果監督：[黃宏顯](../Page/黃宏顯.md "wikilink")
  - 視覺效果製作：[萬寬電腦藝術設計有限公司](../Page/萬寬電腦藝術設計有限公司.md "wikilink")
  - 出品人：[楊受成](../Page/楊受成.md "wikilink")
  - 行政策劃：[Bey Logan](../Page/Bey_Logan.md "wikilink")
  - 策劃：[梁雲傑](../Page/梁雲傑.md "wikilink")
  - 製片：彭玉琳
  - 統籌：林錦波

## 獎項

<table style="width:100%;">
<colgroup>
<col style="width: 19%" />
<col style="width: 12%" />
<col style="width: 16%" />
<col style="width: 52%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獎項</p></th>
<th><p>類別</p></th>
<th><p>名字</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/第40屆金馬獎.md" title="wikilink">第40屆金馬獎</a></p></td>
<td><p><a href="../Page/金馬獎最佳視覺效果.md" title="wikilink">最佳視覺效果</a></p></td>
<td><p>黃宏顯</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金馬獎最佳動作設計.md" title="wikilink">最佳動作設計</a></p></td>
<td><p><a href="../Page/甄子丹.md" title="wikilink">甄子丹</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第23屆香港電影金像獎.md" title="wikilink">第23屆香港電影金像獎</a></p></td>
<td><p>最佳動作設計</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳剪接</p></td>
<td><p>陳祺合</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳美術指導</p></td>
<td><p>雷楚雄</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳服裝造型設計</p></td>
<td><p><a href="../Page/奚仲文.md" title="wikilink">奚仲文</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳音響效果</p></td>
<td><p>曾景祥</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/香港電影金像獎最佳女配角.md" title="wikilink">最佳女配角</a></p></td>
<td><p><a href="../Page/何超儀.md" title="wikilink">何超儀</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第9屆香港電影金紫荊獎.md" title="wikilink">第9屆香港電影金紫荊獎</a></p></td>
<td><p>最佳攝影</p></td>
<td><p>張文寶</p></td>
<td></td>
</tr>
</tbody>
</table>

## 另見

  - [千機變II之花都大戰](../Page/千機變II之花都大戰.md "wikilink")

## 外部連結

  - [千機變製片商的網頁](https://web.archive.org/web/20030328025909/http://twinseffect.emg.com.hk/)

  - {{@movies|fthk30351887}}

  -
  -
  -
  -
  -
  -
[Category:續集電影](../Category/續集電影.md "wikilink")
[3](../Category/2000年代香港電影作品.md "wikilink")
[Category:吸血鬼題材電影](../Category/吸血鬼題材電影.md "wikilink")
[Category:林超賢電影](../Category/林超賢電影.md "wikilink")
[Category:中国奇幻电影](../Category/中国奇幻电影.md "wikilink")
[Category:中国动作片](../Category/中国动作片.md "wikilink")
[Category:香港奇幻電影](../Category/香港奇幻電影.md "wikilink")
[Category:香港電影金像獎最佳剪接獲獎電影](../Category/香港電影金像獎最佳剪接獲獎電影.md "wikilink")
[Category:香港電影金像獎最佳視覺效果獲獎電影](../Category/香港電影金像獎最佳視覺效果獲獎電影.md "wikilink")
[Category:香港電影金像獎最佳音響效果獲獎電影](../Category/香港電影金像獎最佳音響效果獲獎電影.md "wikilink")
[Category:香港電影金像獎最佳動作設計獲獎電影](../Category/香港電影金像獎最佳動作設計獲獎電影.md "wikilink")
[Category:香港電影金像獎最佳美術指導獲獎電影](../Category/香港電影金像獎最佳美術指導獲獎電影.md "wikilink")
[Category:香港電影金像獎最佳服裝造型設計獲獎電影](../Category/香港電影金像獎最佳服裝造型設計獲獎電影.md "wikilink")
[Category:金馬獎最佳視覺效果獲獎電影](../Category/金馬獎最佳視覺效果獲獎電影.md "wikilink")
[Category:金马奖最佳动作设计获奖电影](../Category/金马奖最佳动作设计获奖电影.md "wikilink")
[Category:陈光荣配乐电影](../Category/陈光荣配乐电影.md "wikilink")