**千纪**\[1\]，又称**千年纪**、**千年**，是跨越一千年的时间跨度。在[公元纪年中](../Page/西元紀年.md "wikilink")，如果一个年份可以被1000整除就被称为千年（或者那个年份之后的一年），如[2000年或](../Page/2000年.md "wikilink")[2001年都可被认为是新千年](../Page/2001年.md "wikilink")。

在2000年全球庆祝新千纪到来时，就发生了有关到底2000年还是2001年才是新千纪开始的争论。一派观点认为，由于阳历历法从公元[1年开始算起](../Page/1年.md "wikilink")，因此公元1年至1000年是1千纪，而2千纪开始于1001年，这样推算下去，2001年才是新千纪的开始。

## 史前

  - [前18千紀](../Page/前18千紀.md "wikilink")
    [前11千紀](../Page/前11千紀.md "wikilink")
    [前10千纪](../Page/前10千纪.md "wikilink")
    [前9千纪](../Page/前9千纪.md "wikilink")
    [前8千纪](../Page/前8千纪.md "wikilink")
    [前7千纪](../Page/前7千纪.md "wikilink")
    [前6千纪](../Page/前6千纪.md "wikilink")
    [前5千纪](../Page/前5千纪.md "wikilink")
    [前4千纪](../Page/前4千纪.md "wikilink")

## 公元前

  - [前3千纪](../Page/前3千纪.md "wikilink")
      - [前21世纪](../Page/前21世纪.md "wikilink") -
        [前22世纪](../Page/前22世纪.md "wikilink") -
        [前23世纪](../Page/前23世纪.md "wikilink") -
        [前24世纪](../Page/前24世纪.md "wikilink") -
        [前25世纪](../Page/前25世纪.md "wikilink")
      - [前26世纪](../Page/前26世纪.md "wikilink") -
        [前27世纪](../Page/前27世纪.md "wikilink") -
        [前28世纪](../Page/前28世纪.md "wikilink") -
        [前29世纪](../Page/前29世纪.md "wikilink") -
        [前30世纪](../Page/前30世纪.md "wikilink")
  - [前2千纪](../Page/前2千纪.md "wikilink")
      - [前11世纪](../Page/前11世纪.md "wikilink") -
        [前12世纪](../Page/前12世纪.md "wikilink") -
        [前13世纪](../Page/前13世纪.md "wikilink") -
        [前14世纪](../Page/前14世纪.md "wikilink") -
        [前15世纪](../Page/前15世纪.md "wikilink")
      - [前16世纪](../Page/前16世纪.md "wikilink") -
        [前17世纪](../Page/前17世纪.md "wikilink") -
        [前18世纪](../Page/前18世纪.md "wikilink") -
        [前19世纪](../Page/前19世纪.md "wikilink") -
        [前20世纪](../Page/前20世纪.md "wikilink")
  - [前1千纪](../Page/前1千纪.md "wikilink")
      - [前1世纪](../Page/前1世纪.md "wikilink") -
        [前2世纪](../Page/前2世纪.md "wikilink") -
        [前3世纪](../Page/前3世纪.md "wikilink") -
        [前4世纪](../Page/前4世纪.md "wikilink") -
        [前5世纪](../Page/前5世纪.md "wikilink")
      - [前6世纪](../Page/前6世纪.md "wikilink") -
        [前7世纪](../Page/前7世纪.md "wikilink") -
        [前8世纪](../Page/前8世纪.md "wikilink") -
        [前9世纪](../Page/前9世纪.md "wikilink") -
        [前10世纪](../Page/前10世纪.md "wikilink")

## 公元后

  - [1千纪](../Page/1千纪.md "wikilink")
      - [1世纪](../Page/1世纪.md "wikilink") -
        [2世纪](../Page/2世纪.md "wikilink") -
        [3世纪](../Page/3世纪.md "wikilink") -
        [4世纪](../Page/4世纪.md "wikilink") -
        [5世纪](../Page/5世纪.md "wikilink")
      - [6世纪](../Page/6世纪.md "wikilink") -
        [7世纪](../Page/7世纪.md "wikilink") -
        [8世纪](../Page/8世纪.md "wikilink") -
        [9世纪](../Page/9世纪.md "wikilink") -
        [10世纪](../Page/10世纪.md "wikilink")
  - [2千纪](../Page/2千纪.md "wikilink")
      - [11世纪](../Page/11世纪.md "wikilink") -
        [12世纪](../Page/12世纪.md "wikilink") -
        [13世纪](../Page/13世纪.md "wikilink") -
        [14世纪](../Page/14世纪.md "wikilink") -
        [15世纪](../Page/15世纪.md "wikilink")
      - [16世纪](../Page/16世纪.md "wikilink") -
        [17世纪](../Page/17世纪.md "wikilink") -
        [18世纪](../Page/18世纪.md "wikilink") -
        [19世纪](../Page/19世纪.md "wikilink") -
        [20世纪](../Page/20世纪.md "wikilink")
  - [3千纪](../Page/3千纪.md "wikilink")
      - [21世纪](../Page/21世纪.md "wikilink") -
        [22世纪](../Page/22世纪.md "wikilink") -
        [23世纪](../Page/23世纪.md "wikilink") -
        [24世纪](../Page/24世纪.md "wikilink") -
        [25世纪](../Page/25世纪.md "wikilink")
      - [26世纪](../Page/26世纪.md "wikilink") -
        [27世纪](../Page/27世纪.md "wikilink") -
        [28世纪](../Page/28世纪.md "wikilink") -
        [29世纪](../Page/29世纪.md "wikilink") -
        [30世纪](../Page/30世纪.md "wikilink")
  - [4千纪](../Page/4千纪.md "wikilink")
  - [5千纪](../Page/5千纪.md "wikilink")
  - [6千纪](../Page/6千纪.md "wikilink")
  - [7千纪](../Page/7千纪.md "wikilink")
  - [8千纪](../Page/8千纪.md "wikilink")
  - [9千纪](../Page/9千纪.md "wikilink")
  - [10千纪](../Page/10千纪.md "wikilink")
  - [11千纪及以后](../Page/11千纪及以后.md "wikilink")

## 参考文献

## 参见

  - [年代](../Page/年代.md "wikilink") - [世纪](../Page/世纪.md "wikilink") -
    [千禧年主义](../Page/千禧年主义.md "wikilink")

{{-}}

[Q](../Category/时间单位.md "wikilink") [千纪](../Category/千纪.md "wikilink")
[Q](../Category/年代学.md "wikilink")

1.