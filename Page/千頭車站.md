**千頭車站**（）是一位於[日本](../Page/日本.md "wikilink")[靜岡縣](../Page/靜岡縣.md "wikilink")[榛原郡](../Page/榛原郡.md "wikilink")[川根本町千頭](../Page/川根本町.md "wikilink")，由[大井川鐵道所經營的](../Page/大井川鐵道.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。千頭車站是大井川鐵道所擁有的兩條鐵路線、[大井川本線與](../Page/大井川本線.md "wikilink")[井川線的交會車站](../Page/井川線.md "wikilink")，也是附近著名的觀光名勝[寸又峽溫泉的主要進出門戶](../Page/寸又峽溫泉.md "wikilink")。位於山區的車站標高299.8公尺，並入選為[中部車站百選](../Page/中部車站百選.md "wikilink")（）名單之一。

由於是兩條鐵路線的起點站，千頭車站內採的是[港灣式月台設計](../Page/港灣式月台.md "wikilink")。大井川本線與井川線雖然都是一樣的軌距（1067mm窄軌），但卻是採用截然不同的軌道設計。其中井川線是日本僅有的一條[齒軌鐵路](../Page/齒軌鐵路.md "wikilink")（採[Abt式設計](../Page/Abt式齒軌鐵路.md "wikilink")），行駛於大井川本線的列車在抵達本站後，會換上齒軌鐵路專用的機車頭，再繼續駛入井川線。除了列車的動力變更外，站區內另有佔地不小的車庫設施，與過去供[蒸汽機車掉頭用的](../Page/蒸汽機車.md "wikilink")[轉車台](../Page/轉車台.md "wikilink")，曾由[日本國鐵使用過的](../Page/日本國鐵.md "wikilink")49616號蒸氣機車頭（一輛[日本國鐵9600型蒸汽機車](../Page/日本國鐵9600型蒸汽機車.md "wikilink")），目前是保存在千頭車站內作為靜態展示使用。

## 通過路線

  - [大井川鐵道](../Page/大井川鐵道.md "wikilink")
      - [大井川本線](../Page/大井川本線.md "wikilink")
      - [井川線](../Page/井川線.md "wikilink")

[Nzu](../Category/日本鐵路車站_Se.md "wikilink")
[Category:靜岡縣鐵路車站](../Category/靜岡縣鐵路車站.md "wikilink")
[Category:大井川本線車站](../Category/大井川本線車站.md "wikilink")
[Category:川根本町](../Category/川根本町.md "wikilink")
[Category:1931年启用的铁路车站](../Category/1931年启用的铁路车站.md "wikilink")
[Category:井川線車站](../Category/井川線車站.md "wikilink")