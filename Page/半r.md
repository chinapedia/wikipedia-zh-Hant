[Half_r_from_malmesbury_bible.jpg](https://zh.wikipedia.org/wiki/File:Half_r_from_malmesbury_bible.jpg "fig:Half_r_from_malmesbury_bible.jpg")
**半r** (, R rotunda) 是字母**[R](../Page/R.md "wikilink")**的古老的变体。
从中世纪到现在，许多字符的不同写法都消失了。这些消失了的[字符有连字符的一个不同的写法](../Page/字符.md "wikilink")、[连体字](../Page/连体字.md "wikilink")、书写用[缩写词](../Page/缩写词.md "wikilink")、一些[花体字的突出部分](../Page/花体字.md "wikilink")，还有带连字符的[长s](../Page/长s.md "wikilink")，半r等等。与很多被列出的惯例一样，这些字符的变体原本的设计目的要不是为了节省在昂贵的羊皮纸上的书写位置，要不就是为了让字母更有美感。

这个“[r](../Page/r.md "wikilink")”型字母最初只跟在字母“[O](../Page/O.md "wikilink")”的後面，继而发展到跟在任何一个带有弯曲笔画字母的後面。从此，半r被用在字母"[b](../Page/b.md "wikilink")","[o](../Page/o.md "wikilink")"和"[p](../Page/p.md "wikilink")"的后面，有时也会用在字母"[d](../Page/d.md "wikilink")"的后面，因为在许多古老的字体中"[d](../Page/d.md "wikilink")"的垂直的笔画通常是向左弯曲的（在[冰岛语中](../Page/冰岛语.md "wikilink")，这种字母"d"的变体成为了字母[ð](../Page/ð.md "wikilink")）。因此，半r从来不会出现在词首。这个字符来自几种不同的字形，这些字形都符合[X高度](../Page/X高度.md "wikilink")。

## 原形

这个字符在[黑体字体](../Page/黑体字.md "wikilink")“Textualis”和[圆体字中使用](../Page/黑体字.md "wikilink")。由于在圆体字中，这个字符看上去像大写字母"[R](../Page/R.md "wikilink")"的右半部分，所以被称作**“半r”**。另外，半r与[阿拉伯数字](../Page/阿拉伯数字.md "wikilink")“[2](../Page/2.md "wikilink")”有一点相似。

[Jupiter_symbol.svg](https://zh.wikipedia.org/wiki/File:Jupiter_symbol.svg "fig:Jupiter_symbol.svg")这个字符同样在普通抄写工作中作为一个缩写词。如果把半r的尾巴延长，并且加上一条竖线与之垂直，那么就变成了[木星的符号](../Page/木星.md "wikilink")。同时，半r在拉丁文中可以作为音节**的缩写，也可以当作属格复数词的后缀**或**。当半r作为缩写词使用的时候，可以跟随任何字母。

## 其他写法

在一些以Textura字体书写的手稿中发现"半R"的另一种非常狭窄的变体，这种写法是在一个垂直的笔画顶上标两个实心菱形。

有一种在黑体字中的写法与当前流通的英镑的符号差不多，只是没有两个横杠。不过这个符号上面没有一个环，下面的笔划也没有那么长。

还有一种在以往的德语中的写法，就是一个相当像"c"的字符上加上一个[長s](../Page/長s.md "wikilink")。这一种写法用在跟随字母"e"的一对"r"中的第二个"r"。

## 斜体写法

[Half-r-comparison.png](https://zh.wikipedia.org/wiki/File:Half-r-comparison.png "fig:Half-r-comparison.png")
第五种写法，这种写法在18世纪的一些法语斜体字体中出现，这种写法可能是由Schrift字体中的花体小字"r"或别的类似的自体中派生出来。这个字符像一个向后的"J"放置在一个旋转180度的同样形状的字符后面。这两个字符被一个小于笔画宽度的空格在中间隔开，并且整个字符倾侧尽管两个部件的笔迹是连在一起的。这个字符用在前面有一个竖笔向左倾的字母"d"的后面。
这个字符的宽度与一个正规的字母"r"相等。在过去，尽管这个字符比较复杂，但由于这个字符显得使用者比较高雅，或者让人记起一些享有声望的旧书法，因而这个字符被人使用。

## 半r的消亡

由于除了在黑体字以外都没有被广泛应用，所以當16世纪[罗马字体在英语中变得更加有地位时](../Page/罗马字体.md "wikilink")，半r在英語的使用消亡了。

## 編碼

半r過往只包含在中世纪字体 (MUFI)
中的私人使用域中，编码为：xF20E。这个字母已经加入了5.1版的[Unicode当中](../Page/Unicode.md "wikilink")（A75A和A75B）。

## 參看

  - [preliminary
    proposal](http://std.dkuug.dk/jtc1/sc2/wg2/docs/n2957.pdf) to be
    added to Unicode

[R半](../Category/中古拉丁文字變體.md "wikilink")