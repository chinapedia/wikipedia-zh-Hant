[Middle_Hill_Residentional_View_201504.jpg](https://zh.wikipedia.org/wiki/File:Middle_Hill_Residentional_View_201504.jpg "fig:Middle_Hill_Residentional_View_201504.jpg")
[Middle_Hill_Residentional_view_from_The_Peak_2009.jpg](https://zh.wikipedia.org/wiki/File:Middle_Hill_Residentional_view_from_The_Peak_2009.jpg "fig:Middle_Hill_Residentional_view_from_The_Peak_2009.jpg")
**半山區**（）是[香港島一個形容住宅區的統稱](../Page/香港島.md "wikilink")，當中有廣義及狹義兩個意思。當中狹義**半山區**位於[太平山山頂及](../Page/太平山山頂.md "wikilink")[中環之間](../Page/中環.md "wikilink")。由於鄰近[核心商業區](../Page/核心商業區.md "wikilink")、交通方便，是多個世界最昂貴的[高尚住宅所在地](../Page/高尚住宅.md "wikilink")。

廣義半山區可細分為東、中、北、西四部分：[中環](../Page/中環.md "wikilink")、[上環](../Page/上環.md "wikilink")、[西環的為](../Page/西環.md "wikilink")「西半山」（當中鄰近中環、上環為狹義的**半山區**)；[麥當奴道](../Page/麥當奴道.md "wikilink")／[堅尼地道及](../Page/堅尼地道.md "wikilink")[寶雲道等近](../Page/寶雲道.md "wikilink")[灣仔的為](../Page/灣仔.md "wikilink")「中半山」；而[銅鑼灣一帶](../Page/銅鑼灣.md "wikilink")（包括[渣甸山及](../Page/渣甸山.md "wikilink")[畢拉山](../Page/畢拉山.md "wikilink")）稱作「東半山」，而[北角的即](../Page/北角.md "wikilink")[寶馬山稱之為](../Page/寶馬山.md "wikilink")「北半山」，根據2011年香港人口普查結果，東半山最富有，其次為中半山及北半山，西半山居民以較高收入的中產爲主，當中前三者收入相若，後者（中環半山）則只有前三者的60－70%，但仍略高於部份的中產區如太古城西的62,370\[1\]。

## 特色

[Middle_Hill_Residential_near_Sai_Ying_Pun_201709.jpg](https://zh.wikipedia.org/wiki/File:Middle_Hill_Residential_near_Sai_Ying_Pun_201709.jpg "fig:Middle_Hill_Residential_near_Sai_Ying_Pun_201709.jpg")看半山區住宅群\]\]
[Dynasty_Court_Towers,_Mid-levels,_Hong_Kong.jpg](https://zh.wikipedia.org/wiki/File:Dynasty_Court_Towers,_Mid-levels,_Hong_Kong.jpg "fig:Dynasty_Court_Towers,_Mid-levels,_Hong_Kong.jpg")裕景花園(左)及帝景園(右)\]\]
半山區亦是香港的大單位集中地，雖然呎價低於市區一定多的比例，如[銅鑼灣市區](../Page/銅鑼灣.md "wikilink")、[中環市區](../Page/中環.md "wikilink")，甚至北角、上環，但在大量的大單位支持下，半山區是香港較富有的地區，除了較早開發的西半山以外，其餘東半山、北半山、中半山的的樓宇平均樓齡遠低於市區，而且較少家庭收入[中位數較低的唐樓社群](../Page/中位數.md "wikilink")。而西半山因為較早開發的關係，亦有很多唐樓的社區，使該區的收入中位數比其餘的半山區低。西半山亦有不少樓齡超過50年的唐樓被陸續收購而被發展商重建，使西半山新樓舊樓兩者互存。

半山區為香港中產聚居地之一，半山區除東半山以外絕大多數為分層單位，香港富豪則絕大多數居住在[山頂以及](../Page/山頂_\(香港\).md "wikilink")[深水灣](../Page/深水灣.md "wikilink")、[清水灣沿海的一萬呎以上的大宅中](../Page/清水灣半島.md "wikilink")，部份亦居住於東半山一萬呎以上的大宅中，以及市區頂層連小型泳池的單位。此外，香港的九龍、新界亦有很多獨立屋、村屋以及三層的七百呎丁屋，這些住戶的收入比不少住在半山區以及港島市區一千呎大單位的住客都低。香港的半山區大多為2,000呎以下的單位。

2011年家庭收入中位數：

  - 東半山（銅鑼灣半山，渣甸山選區）：115,000\[2\]
  - 中半山（灣仔半山，司徒拔道選區）：107,080\[3\]
  - 北半山（北角半山，寶馬山選區）：96,740\[4\]
  - 西半山（中環半山，半山東選區）：70,000\[5\]

## 西半山

半山區東由[灣仔區與](../Page/灣仔區.md "wikilink")[中西區的邊界開始](../Page/中西區_\(香港\).md "wikilink")、西至[薄扶林道以東](../Page/薄扶林道.md "wikilink")、南至[薄扶林郊野公園以北](../Page/薄扶林郊野公園.md "wikilink")、北至[堅道及](../Page/堅道.md "wikilink")[般咸道以南](../Page/般咸道.md "wikilink")。香港開埠初期，[山頂是西方的達官貴人以及富有商人居住地](../Page/山頂.md "wikilink")，中環及中環半山一帶則華洋混居，而[上環](../Page/上環.md "wikilink")、[西環及上環以及西環半山一帶則以華人為主](../Page/西環.md "wikilink")。西半山區的位置，早期能看到[維多利亞港的景色](../Page/維多利亞港.md "wikilink")，但隨住市區的發展，西半山景觀亦被同區密集的高樓以及中環、上環大量極高的商業大廈阻擋，但在大廈中間或能看到海的藍色。而中環半山鄰近中環及[金鐘這些商業區](../Page/金鐘.md "wikilink")，方便上班一族。另一方面，半山區有[樹木和其他](../Page/树.md "wikilink")[植物](../Page/植物.md "wikilink")，提供新鮮[空氣](../Page/空氣.md "wikilink")，唯一的問題是交通略差，影響了此區的樓價。西半山區擁有濃厚的[殖民地色彩](../Page/殖民地.md "wikilink")，有不少具歷史特色的校舍，例如西半山和薄扶林交界的[香港大學](../Page/香港大學.md "wikilink")，[英皇書院等](../Page/英皇書院.md "wikilink")。此外，半山區也有一些以[香港總督命名的街道](../Page/香港總督.md "wikilink")，例如[寶雲道](../Page/寶雲道.md "wikilink")、[麥當勞道](../Page/麥當勞道.md "wikilink")、[羅便臣道](../Page/羅便臣道.md "wikilink")、[堅尼地道及](../Page/堅尼地道.md "wikilink")[般咸道](../Page/般咸道.md "wikilink")。此外，半山區的治安在香港也是相對較好，因爲較少非法暴力社團在此區活動，和油尖旺區有極大對比。

西半山在2012年時尺價平均在8千-9千元，既有2,000呎以上的大單位住宅（但不能和[山頂價值數億低密度](../Page/太平山_\(香港\).md "wikilink")[豪宅比較](../Page/豪宅.md "wikilink")），1,000呎左右的辦公室人員住宅，但亦有不少400呎左右的小單位上車盤\[6\]，豐儉由人，大單位的住客以行政人員為主，小單位的住客以上班一族以及家庭住戶為主，根據最新的2006年分段家庭收入中位數數據，在同一地段，不同大廈的家庭住戶收入中位數可相距極遠。加上西半山有優良校網，亦吸引到不少新界以九龍外區客為子女升學而搬入此區，但大多只可搬入比原新界以及九龍較細的單位，因為此區鄰近中環辦公室，有大量上班一族的上車盤需求，使此區有大量的細單位，令中環半山成為港島半山區家庭收入最低的地區，其餘的半山區則有較多大單位。

## 著名地點、街道及建築物

### 地點

  - [姻緣石](../Page/姻緣石.md "wikilink")
  - [馬己仙峽](../Page/馬己仙峽.md "wikilink")

### 主要街道

  - [干德道](../Page/干德道.md "wikilink")
  - [羅便臣道](../Page/羅便臣道.md "wikilink")
  - [堅道](../Page/堅道.md "wikilink")
  - [些利街](../Page/些利街.md "wikilink")
  - [般咸道](../Page/般咸道.md "wikilink")
  - [西摩道](../Page/西摩道.md "wikilink")
  - [衛城道](../Page/衛城道.md "wikilink")
  - [列堤頓道](../Page/列堤頓道.md "wikilink")
  - [旭龢道](../Page/旭龢道.md "wikilink")
  - [紅棉路](../Page/紅棉路.md "wikilink")
  - [花園道](../Page/花園道.md "wikilink")
  - [馬己仙峽道](../Page/馬己仙峽道.md "wikilink")
  - [堅尼地道](../Page/堅尼地道.md "wikilink")
  - [梅道](../Page/梅道.md "wikilink")
  - [麥當勞道](../Page/麥當勞道.md "wikilink")
  - [寶雲道](../Page/寶雲道.md "wikilink")
  - [司徒拔道](../Page/司徒拔道.md "wikilink")

### 建築物

[天主教座堂的屋頂是十字架形狀.JPG](https://zh.wikipedia.org/wiki/File:天主教座堂的屋頂是十字架形狀.JPG "fig:天主教座堂的屋頂是十字架形狀.JPG")

  - [香港大學](../Page/香港大學.md "wikilink")
  - [山頂纜車站](../Page/山頂纜車.md "wikilink")
  - [英皇書院](../Page/英皇書院.md "wikilink")
  - [中環至半山自動扶梯系統](../Page/中環至半山自動扶梯系統.md "wikilink")
  - [天主教主教座堂](../Page/聖母無原罪主教座堂.md "wikilink")
  - [明愛徐誠斌學院](../Page/明愛徐誠斌學院.md "wikilink")
  - [舊英童學校](../Page/舊英童學校.md "wikilink")
  - [孫中山紀念館 (香港)](../Page/孫中山紀念館_\(香港\).md "wikilink")
  - [聖若瑟書院](../Page/聖若瑟書院.md "wikilink")

### 住宅

  - [雅賓利](../Page/雅賓利.md "wikilink")（[雅賓利道](../Page/雅賓利道.md "wikilink")1號）：[長實執行董事](../Page/長實.md "wikilink")[趙國雄](../Page/趙國雄.md "wikilink")、藝人[孫佳君近年購入該廈單位](../Page/孫佳君.md "wikilink")；[金管局前總裁](../Page/金管局.md "wikilink")[任志剛](../Page/任志剛.md "wikilink")，[唐英年家族](../Page/唐英年.md "wikilink")，[和黃執行董事](../Page/和黃.md "wikilink")[黎啟明](../Page/黎啟明.md "wikilink")，[呂麗君等](../Page/呂麗君.md "wikilink")，都先後為該廈業主
  - [愛都大廈](../Page/愛都大廈.md "wikilink")（[花園道](../Page/花園道.md "wikilink")55號）：演藝界巨星[張學友於此大廈擁有產業](../Page/張學友.md "wikilink")、藝人[吳千語亦曾租住於此](../Page/吳千語.md "wikilink")
  - [嘉園](../Page/嘉園.md "wikilink")（[大坑道](../Page/大坑道.md "wikilink")345號）：演藝界天王[郭富城居住於此](../Page/郭富城.md "wikilink")
  - [花園臺](../Page/花園臺.md "wikilink")（[舊山頂道](../Page/舊山頂道.md "wikilink")8A號）：[利孝和夫人](../Page/利孝和夫人.md "wikilink")、藝人[張智霖及](../Page/張智霖.md "wikilink")[袁詠儀等擇居於此](../Page/袁詠儀.md "wikilink")；[長實執行董事](../Page/長實.md "wikilink")[趙國雄](../Page/趙國雄.md "wikilink")、[創興銀行](../Page/創興銀行.md "wikilink")[家族](../Page/家族.md "wikilink")，[龐維新家族等便持有該單位收租](../Page/龐維新家族.md "wikilink")，曾為藝人的[鄺美雲曾是屋苑業主](../Page/鄺美雲.md "wikilink")
  - [嘉慧園](../Page/嘉慧園.md "wikilink")（[馬己仙峽道](../Page/馬己仙峽道.md "wikilink")3號）：前[特首](../Page/特首.md "wikilink")[董建華居所](../Page/董建華.md "wikilink")
  - [澄碧閣](../Page/澄碧閣.md "wikilink")（[馬己仙峽道](../Page/馬己仙峽道.md "wikilink")5-7號）：[李柱銘](../Page/李柱銘.md "wikilink")、[葉劉淑儀居住於此](../Page/葉劉淑儀.md "wikilink")
  - [帝景园](../Page/帝景园.md "wikilink")（[舊山頂道](../Page/舊山頂道.md "wikilink")17-23號）：艺人[林峰居住于其中一个单位](../Page/林峰.md "wikilink")
  - [嘉林閣](../Page/嘉林閣.md "wikilink")（[薄扶林道](../Page/薄扶林道.md "wikilink")137號）：艺人[林峰居住于其中一个單位](../Page/林峰.md "wikilink")
  - [高雲大厦](../Page/高雲大厦.md "wikilink")（[麥當勞道](../Page/麥當勞道.md "wikilink")114-116號）：艺人[吳千語曾租住於此](../Page/吳千語.md "wikilink")
  - [金櫻閣](../Page/金櫻閣.md "wikilink")（[堅尼地道](../Page/堅尼地道.md "wikilink")58-60號）：艺人[吳千語的父母曾租住於此](../Page/吳千語.md "wikilink")
  - [君德閣](../Page/君德閣.md "wikilink")（[干德道](../Page/干德道.md "wikilink")20號）
  - [康苑](../Page/康苑.md "wikilink")（[干德道](../Page/干德道.md "wikilink")17-25號）
  - [慧豪閣](../Page/慧豪閣.md "wikilink")（[干德道](../Page/干德道.md "wikilink")22號）：藝人[陳法拉居住於此](../Page/陳法拉.md "wikilink")
  - [聯邦花園](../Page/聯邦花園.md "wikilink")（[干德道](../Page/干德道.md "wikilink")41號）
  - [樂信臺](../Page/樂信臺.md "wikilink")（[羅便臣道](../Page/羅便臣道.md "wikilink")8號）
  - [嘉兆臺](../Page/嘉兆臺.md "wikilink")（[羅便臣道](../Page/羅便臣道.md "wikilink")10號）
  - [薈萃苑](../Page/薈萃苑.md "wikilink")（[羅便臣道](../Page/羅便臣道.md "wikilink")9號）
  - [雍景臺](../Page/雍景臺.md "wikilink")（[羅便臣道](../Page/羅便臣道.md "wikilink")70號）
  - [雅苑](../Page/雅苑.md "wikilink")（[羅便臣道](../Page/羅便臣道.md "wikilink")82號）
  - [殷樺花園](../Page/殷樺花園.md "wikilink")（[羅便臣道](../Page/羅便臣道.md "wikilink")95號）：藝人[薛家燕居住於此](../Page/薛家燕.md "wikilink")
  - [瑧環](../Page/瑧環.md "wikilink")（[堅道](../Page/堅道.md "wikilink")38號）
  - [御景臺](../Page/御景臺.md "wikilink")（[堅道](../Page/堅道.md "wikilink")46號）
  - [應彪大廈](../Page/應彪大廈.md "wikilink")（[半山卑利士道](../Page/半山卑利士道.md "wikilink")1號）
  - [羅便臣道31號](../Page/羅便臣道31號.md "wikilink")（[羅便臣道](../Page/羅便臣道.md "wikilink")31號）
  - [衛城閣](../Page/衛城閣.md "wikilink")（[衛城道](../Page/衛城道.md "wikilink")6號）：名模[Maggie
    Q](../Page/Maggie_Q.md "wikilink")、藝人[林德信曾居住於此](../Page/林德信.md "wikilink")

## 區議會議席分佈

為方便比較，以下列表會以西半山為主，當中東至[紅棉道](../Page/紅棉道.md "wikilink")，北至[下亞厘畢道](../Page/下亞厘畢道.md "wikilink")、[雲咸街](../Page/雲咸街.md "wikilink")、[荷李活道](../Page/荷李活道.md "wikilink")、[樓梯街](../Page/樓梯街.md "wikilink")、[堅道](../Page/堅道.md "wikilink")、[般咸道](../Page/般咸道.md "wikilink")、[高街](../Page/高街.md "wikilink")、[薄扶林道](../Page/薄扶林道.md "wikilink")，西至[香港大學](../Page/香港大學.md "wikilink")，南至[干德道及](../Page/干德道.md "wikilink")[寶珊道沿線為範圍](../Page/寶珊道.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>年度/範圍</p></th>
<th><p>2000-2003</p></th>
<th><p>2004-2007</p></th>
<th><p>2008-2011</p></th>
<th><p>2012-2015</p></th>
<th><p>2016-2019</p></th>
<th><p>2020-2023</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/鴨巴甸街.md" title="wikilink">鴨巴甸街</a>、<a href="../Page/衛城道.md" title="wikilink">衛城道</a>、<a href="../Page/西摩道.md" title="wikilink">西摩道以東</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/柏道.md" title="wikilink">柏道</a>、<a href="../Page/羅便臣道.md" title="wikilink">羅便臣道及</a><a href="../Page/羅便臣道.md" title="wikilink">羅便臣道往</a><a href="../Page/干德道.md" title="wikilink">干德道天橋以東至</a><a href="../Page/鴨巴甸街.md" title="wikilink">鴨巴甸街</a>、<a href="../Page/衛城道.md" title="wikilink">衛城道</a>、<a href="../Page/西摩道.md" title="wikilink">西摩道以西</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/高街.md" title="wikilink">高街以南</a>、<a href="../Page/般咸道.md" title="wikilink">般咸道以北</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/般咸道.md" title="wikilink">般咸道以南</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

註：以上主要範圍尚有其他細微調整（包括編號），請參閱有關區議會選舉選區分界地圖及條目。

## 參見

  - [中西區](../Page/中西區_\(香港\).md "wikilink")
  - [太平山](../Page/太平山_\(香港\).md "wikilink")

## 参考资料

{{-}}

[Category:中西區 (香港)](../Category/中西區_\(香港\).md "wikilink")
[半山區](../Category/半山區.md "wikilink")
[Category:紅色公共小巴禁區](../Category/紅色公共小巴禁區.md "wikilink")

1.
2.  [1](http://www.census2011.gov.hk/tc/district-profiles/ca/wan-chai/b06.html)
3.  [2](http://www.census2011.gov.hk/tc/district-profiles/ca/wan-chai/b09.html)
4.  [3](http://www.census2011.gov.hk/tc/district-profiles/ca/eastern/c15.html)
5.  [4](http://www.census2011.gov.hk/tc/district-profiles/ca/central-and-western/a02.html)
6.