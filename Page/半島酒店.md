[The_Peninsula_Hong_Kong_Night_View_2006.jpg](https://zh.wikipedia.org/wiki/File:The_Peninsula_Hong_Kong_Night_View_2006.jpg "fig:The_Peninsula_Hong_Kong_Night_View_2006.jpg")
[The_Peninsula_Hong_Kong_Arcade.jpg](https://zh.wikipedia.org/wiki/File:The_Peninsula_Hong_Kong_Arcade.jpg "fig:The_Peninsula_Hong_Kong_Arcade.jpg")
[HKPeninsulahotel.jpg](https://zh.wikipedia.org/wiki/File:HKPeninsulahotel.jpg "fig:HKPeninsulahotel.jpg")
[The_Peninsula_HK_hallway_1.JPG](https://zh.wikipedia.org/wiki/File:The_Peninsula_HK_hallway_1.JPG "fig:The_Peninsula_HK_hallway_1.JPG")

**香港半島酒店**（[英文](../Page/英文.md "wikilink")：**The Peninsula Hong
Kong**），為[香港上海大酒店有限公司](../Page/香港上海大酒店有限公司.md "wikilink")（）的[旗艦](../Page/旗艦.md "wikilink")[酒店](../Page/酒店.md "wikilink")，亦是集團首間命名為「半島」的酒店，位於[九龍](../Page/九龍.md "wikilink")[尖沙咀](../Page/尖沙咀.md "wikilink")[梳士巴利道](../Page/梳士巴利道.md "wikilink")22號，是[香港現存歷史最悠久及最著名的](../Page/香港酒店列表.md "wikilink")[甲級高價酒店](../Page/甲級高價酒店.md "wikilink")，也是全球最著名及最豪華的酒店之一。基於其極為珍貴的歷史及建築價值，酒店建築被[古物古蹟辦事處評審為](../Page/古物古蹟辦事處.md "wikilink")[一級歷史建築](../Page/一級歷史建築.md "wikilink")。

## 概覽

半島酒店面對[維多利亞港](../Page/維多利亞港.md "wikilink")，中高層享有海景。酒店有300間客房，客房面積較香港一般酒店大。設施包括羅馬宮廷風格的游泳池及健身俱樂部，地下商場多是名牌店。酒店於1970年購入[勞斯萊斯豪華轎車作為車隊](../Page/勞斯萊斯.md "wikilink")，現時所用的14輛[勞斯萊斯幻影轎車是全球最大的勞斯萊斯車隊](../Page/勞斯萊斯幻影.md "wikilink")，並創下勞斯萊斯有史以來最昂貴的訂單紀錄。

酒店由英籍[猶太裔的](../Page/猶太裔.md "wikilink")[嘉道理家族擁有及經營](../Page/嘉道理.md "wikilink")，由香港上市公司香港上海大酒店有限公司持有。

半島酒店曾被選為全球十大知名酒店，曾入住的名人包括[美國前總統](../Page/美國總統.md "wikilink")[-{zh-hans:理查德·尼克松;zh-hk:尼克遜;zh-tw:理察·米爾豪斯·尼克森;}-](../Page/理查德·尼克松.md "wikilink")、影星[-{zh-hans:克拉克·盖博;zh-hk:奇勒·基寶;zh-tw:克拉克·蓋博;}-](../Page/克拉克·盖博.md "wikilink")、[NBA球星](../Page/NBA.md "wikilink")[-{zh-hans:迈克尔·乔丹;zh-hk:米高佐敦;zh-tw:麥可·喬丹;}-等](../Page/迈克尔·乔丹.md "wikilink")，[英女王](../Page/英女王.md "wikilink")[-{zh-hans:伊丽莎白二世;zh-hk:伊利沙伯二世;zh-tw:伊麗莎白二世;}-亦指定下榻](../Page/伊丽莎白二世.md "wikilink")。其他曾入住酒店之名人包括[伊莉沙伯·泰萊及](../Page/伊莉沙伯·泰萊.md "wikilink")[楊虎城](../Page/楊虎城.md "wikilink")。

半島酒店是參與[幻彩詠香江匯演的建築物之一](../Page/幻彩詠香江.md "wikilink")。

## 歷史

半島酒店由1924年酒店動工興建，至1926年至1927年期間竣工，並於1928年12月11日正式開業。開業前的1927年2月，[英軍派遣部隊到](../Page/英軍.md "wikilink")[上海英租界](../Page/上海.md "wikilink")，曾一度進駐作為臨時軍營。

酒店樓高七層，為當時香港最高的建築，也是1950年代前九龍最高的建築\[1\]。以[巴洛克復興風格设计及建造](../Page/巴洛克建筑.md "wikilink")。建築物立面有大量古典特徵作裝飾，如石雕刻、拱窗、[拱心石等](../Page/拱心石.md "wikilink")，強烈突出的屋簷為建築物帶來意大利式豪華建築外觀。正門前小廣場原設有車房，其後拆卸並改為[大理石](../Page/大理石.md "wikilink")[噴水池](../Page/噴水池.md "wikilink")。建築物內部有大量[新古典及](../Page/新古典主義建築.md "wikilink")[巴洛克風格的裝飾](../Page/巴洛克建筑.md "wikilink")，裝飾相當華麗。當時酒店有「遠東貴婦」的稱號，是當時全[亞洲最先進及豪華的酒店之一](../Page/亞洲.md "wikilink")，旋即成為富豪及社會精英的熱門社交場所，亦吸引多位外國名人入住。首位入住的[英國王室人員是](../Page/英國王室.md "wikilink")1929年入住的[告羅士打公爵](../Page/亨利王子_\(告羅士打公爵\).md "wikilink")。

1941年12月6日，半島酒店曾舉行慈善舞會為[中國抗日戰爭籌款](../Page/中國抗日戰爭.md "wikilink")，惟兩日後[香港保衛戰旋即爆發](../Page/香港保衛戰.md "wikilink")。[日軍於](../Page/日軍.md "wikilink")12月12日進駐九龍半島，半島酒店升起了[日本國旗](../Page/日本國旗.md "wikilink")，並成為其臨時總部。1941年12月25日傍晚，[香港總督](../Page/香港總督.md "wikilink")[楊慕琦及](../Page/楊慕琦.md "wikilink")[駐港英軍將領](../Page/駐港英軍.md "wikilink")[莫德庇乘](../Page/莫德庇.md "wikilink")[天星小輪到半島酒店](../Page/天星小輪.md "wikilink")336室\[2\]與日軍簽署投降書。在[日佔時期初期至](../Page/香港日佔時期.md "wikilink")1942年2月20日，半島酒店曾被[日軍徵用作為戰爭司令部及軍政廳行政總部](../Page/日軍.md "wikilink")，及後第一任日佔時期總督[磯谷廉介亦曾在半島酒店短暫居住兩個月才遷往](../Page/磯谷廉介.md "wikilink")[港督府](../Page/港督府.md "wikilink")。1942年4月23日，酒店名稱被日方改為**東亞酒店**，至[香港重光為止](../Page/香港重光.md "wikilink")。重光初期，政府使用該酒店收容無家可歸者，至1946年交回酒店原主。

1950年代起，半島酒店有「影人茶座」之稱，因不少電影明星都愛在半島酒店喝[下午茶](../Page/下午茶.md "wikilink")。1960年，酒店進行大規模重修，並安裝[空調設施](../Page/空調.md "wikilink")。1980年代，[張國榮](../Page/張國榮.md "wikilink")、[鍾楚紅](../Page/鍾楚紅.md "wikilink")、[張曼玉等藝人曾是酒店常客](../Page/張曼玉.md "wikilink")。[-{zh-hans:好莱坞;zh-hk:荷里活;zh-tw:好萊塢;}-影星](../Page/好莱坞.md "wikilink")[-{zh-hans:汤姆·克鲁斯;
zh-hk:湯告魯斯;}-](../Page/汤姆·克鲁斯.md "wikilink")、[-{zh-hans:阿诺·施瓦辛格;
zh-hk:阿諾舒華辛力加;}-以及](../Page/阿诺·施瓦辛格.md "wikilink")[日本藝人](../Page/日本.md "wikilink")[酒井法子也曾入住](../Page/酒井法子.md "wikilink")。

酒店其後於1988年再度維修，並於1991年起進行擴建工程，在北面加建一幢30層的新翼，於1994年落成。新翼北面為辦公大樓，並於中間道設置獨立大堂，頂樓設有[直昇機場](../Page/直昇機.md "wikilink")，方便重要貴賓直接使用直昇機往來[香港國際機場或暢遊香港上空](../Page/香港國際機場.md "wikilink")。

<File:Salisbury> Road in 1930s.jpg|1930年代半島酒店 <File:TST>
aerophoto.jpg|從高空看半島酒店（1931年）File:Hong Kong - The
Peninsula.jpg|1931年的半島酒店

半島酒店擁有多家餐廳和酒吧，[吉地士](../Page/吉地士.md "wikilink")\[3\]（Gaddi's）被認為是全亞洲最佳的[法國餐廳之一](../Page/法國.md "wikilink")。位於28樓的[Felix](../Page/Felix_\(餐廳\).md "wikilink")\[4\]則提供太平洋沿岸美食，於1994年開始營業，由著名法籍設計師[菲利普·斯塔克設計](../Page/菲利普·斯塔克.md "wikilink")。半島的大堂茶座（The
Lobby）\[5\]以[下午茶知名](../Page/下午茶.md "wikilink")，常見不少香港本地名人；[嘉麟樓](../Page/嘉麟樓.md "wikilink")\[6\]（Spring
Moon）提供精緻的[廣東菜](../Page/廣東菜.md "wikilink")，並曾與[國泰航空合作推出空中特色](../Page/國泰航空.md "wikilink")[飛機餐](../Page/飛機餐.md "wikilink")。另外二樓供應瑞士餐的「瑞樵閣」（Chesa）\[7\]也享負盛名。在大堂供應的英式下午茶也是十分受歡迎，每逢假日必定人潮如鯽。

2011年11月底，宣布2012年將不再提供有[魚翅的料理](../Page/魚翅.md "wikilink")，以響應全球[鯊魚保育議題](../Page/鯊魚.md "wikilink")。

<File:Afternoon> Tea in the Peninsula, Hong Kong.jpg|半島酒店供應的英式下午茶
[File:RRPhantomEWBHK.JPG|半島酒店的](File:RRPhantomEWBHK.JPG%7C半島酒店的)[勞斯萊斯幻影豪華轎車](../Page/勞斯萊斯幻影_\(2003版\).md "wikilink")

## 途經之公共交通服務

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{荃灣綫色彩}}">█</font><a href="../Page/荃灣綫.md" title="wikilink">荃灣綫</a>：<a href="../Page/尖沙咀站.md" title="wikilink">尖沙咀站E出口</a></li>
<li><font color="{{西鐵綫色彩}}">█</font><a href="../Page/西鐵綫.md" title="wikilink">西鐵綫</a>：<a href="../Page/尖東站.md" title="wikilink">尖東站L</a>3出口</li>
</ul>
<dl>
<dt><a href="../Page/香港巴士.md" title="wikilink">巴士</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 參考

<references />

## 外部链接

  - [半島酒店](http://hongkong.peninsula.com/zh/default)

[Category:香港上海大酒店](../Category/香港上海大酒店.md "wikilink")
[Category:尖沙咀酒店](../Category/尖沙咀酒店.md "wikilink")
[Category:香港歷史建築](../Category/香港歷史建築.md "wikilink")
[Category:彌敦道](../Category/彌敦道.md "wikilink")
[Category:幻彩詠香江](../Category/幻彩詠香江.md "wikilink")
[Category:香港一級歷史建築](../Category/香港一級歷史建築.md "wikilink")
[Category:香港上海大酒店](../Category/香港上海大酒店.md "wikilink")
[Category:香港摩天大樓](../Category/香港摩天大樓.md "wikilink")
[Category:1928年建立](../Category/1928年建立.md "wikilink")

1.
2.  [酒店大堂將時光倒流半島80周年茶舞再現](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20080109&sec_id=4104&subsec_id=11869&art_id=10623529)
    《[蘋果日報 (香港)](../Page/蘋果日報_\(香港\).md "wikilink")》2008年1月9日A10
3.  [半島酒店 -
    吉地士](http://www.foodeasy.com/hk/rest_detail.php?id_rest=1501)
4.  [半島酒店 -
    Felix](http://www.foodeasy.com/hk/rest_detail.php?id_rest=451)
5.  [半島酒店 -
    大堂茶座](http://www.foodeasy.com/hk/rest_detail.php?id_rest=3922)

6.  [半島酒店 -
    嘉麟樓](http://www.foodeasy.com/hk/rest_detail.php?id_rest=1451)
7.  [半島酒店 -
    瑞樵閣](http://www.foodeasy.com/hk/rest_detail.php?id_rest=1488)