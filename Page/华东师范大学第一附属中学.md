**华东师范大学第一附属中学**，通常被简称为**华师大一附中**、“师大附中”、“师大一附中”，位于[中国](../Page/中国.md "wikilink")[上海市](../Page/上海市.md "wikilink")，是首批被列为[上海市](../Page/上海市.md "wikilink")[重点中学的学校](../Page/重点中学.md "wikilink")。2005年2月，又被命名为首批上海市[实验性示范性高级中学](../Page/实验性示范性高级中学.md "wikilink")。

## 学校历史

华东师范大学第一附属中学前身是创建于1925年的[光华大学附中和](../Page/光华大学.md "wikilink")[大夏大学附中](../Page/大夏大学附中.md "wikilink")。

1951年因[华东师范大学的组建](../Page/华东师范大学.md "wikilink")，两校附中合并为华东师范大学附属中学，校址为原[工部局立华童公学](../Page/工部局立华童公学.md "wikilink")。

1958年因华东师大新成立了[二附中](../Page/华东师范大学第二附属中学.md "wikilink")，改称为华东师范大学第一附属中学。

2005年学校启用了坐落于上海市[北外滩附近的](../Page/北外滩.md "wikilink")[瑞虹新城中的新校区](../Page/瑞虹新城.md "wikilink")。

## 著名校友

  - [乔石](../Page/乔石.md "wikilink")
  - [姚依林](../Page/姚依林.md "wikilink")
  - [尉健行](../Page/尉健行.md "wikilink")
  - [方成](../Page/方成.md "wikilink")
  - [陈凯先](../Page/陈凯先.md "wikilink")
  - [叶澜](../Page/叶澜.md "wikilink")
  - [朱践耳](../Page/朱践耳.md "wikilink")
  - [舒適](../Page/舒適_\(演員\).md "wikilink")
  - [谢晋](../Page/谢晋.md "wikilink")
  - [赵长天](../Page/赵长天.md "wikilink")
  - [叶惠贤](../Page/叶惠贤.md "wikilink")
  - [钱文忠](../Page/钱文忠.md "wikilink")
  - [骆利群](../Page/骆利群.md "wikilink")，[中国科学技术大学](../Page/中国科学技术大学.md "wikilink")81少/818校友，现任[美国](../Page/美国.md "wikilink")[斯坦福大学生物系教授](../Page/斯坦福大学.md "wikilink")、霍华德·休斯医学研究所研究员。

## 参看

  - [华东师范大学附属学校列表](../Page/华东师范大学附属学校列表.md "wikilink")
  - [华东师范大学](../Page/华东师范大学.md "wikilink")
  - [光华大学](../Page/光华大学.md "wikilink")
  - [大夏大学](../Page/大夏大学.md "wikilink")

## 参考资料

  - 学校介绍[1](https://web.archive.org/web/20070704172720/http://www.sdfz.sh.cn/sdfzweb/introduce/index.htm)
  - [光华大学变迁记](https://web.archive.org/web/20091122153351/http://www.dag1.ecnu.edu.cn/xiaoshi/guanghua/bianqian-guanghua.htm)

## 外部链接

  - [华东师范大学第一附属中学主页](http://www.sdfz.sh.cn/)

[H](../Category/上海中等教育.md "wikilink")
[Category:虹口区教育](../Category/虹口区教育.md "wikilink")
[Category:华东师范大学附属学校](../Category/华东师范大学附属学校.md "wikilink")
[Category:华东师范大学基础教育集团成员校](../Category/华东师范大学基础教育集团成员校.md "wikilink")
[Category:1951年创建的教育机构](../Category/1951年创建的教育机构.md "wikilink")