**华坪县**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[云南省](../Page/云南省.md "wikilink")[丽江市下属的一个县](../Page/丽江市.md "wikilink")。

## 矿产资源

拥有[煤](../Page/煤.md "wikilink")、[石灰石等矿产资源](../Page/石灰石.md "wikilink")。水能资源丰富，2011年开建[观音岩水电站](../Page/观音岩水电站.md "wikilink")（投资230亿）。

## 农业

华坪种植业以[芒果为主](../Page/芒果.md "wikilink")，年产值达2亿人民币。

## 民族文化

华坪的傈僳族分为花傈僳和黑傈僳，花傈僳的服饰光彩耀人。

彝族的分支——[水田彝族](../Page/水田彝族.md "wikilink")（约8000人），他留彝族。水田彝族最热情好客，勤劳勇敢。他们酿造的腊渣酒独具特色，口感纯正。

## 行政区划

下辖：\[1\] 。

## 参考资料

[华坪县](../Category/华坪县.md "wikilink") [县](../Category/丽江区县.md "wikilink")
[丽江](../Category/云南省县份.md "wikilink")

1.