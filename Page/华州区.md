**华州区**，前稱**華縣**，古稱[鄭縣](../Page/郑县_\(秦国\).md "wikilink")，位于[中华人民共和国](../Page/中华人民共和国.md "wikilink")[陕西省东部](../Page/陕西省.md "wikilink")、[渭河下游南岸](../Page/渭河.md "wikilink")，[少華山之陰](../Page/少華山.md "wikilink")，是[渭南市所辖的一个](../Page/渭南市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")。[面积为](../Page/面积.md "wikilink")1127[平方公里](../Page/平方公里.md "wikilink")，2002年[人口为](../Page/人口.md "wikilink")34万人。

## 历史

[春秋时](../Page/春秋.md "wikilink")，[秦武公十一年](../Page/秦武公.md "wikilink")（前687年）秦国在此设置[郑县](../Page/郑县_\(秦国\).md "wikilink")；[南北朝西魏时](../Page/南北朝.md "wikilink")，因處在[少華山之陰](../Page/少華山.md "wikilink")，華陰縣（太[華山之陰](../Page/華山.md "wikilink")）附近，设置[华州](../Page/华州_\(西魏\).md "wikilink")。元朝后撤鄭縣，唯稱華州。1556年1月23日，華縣發生約8級的[大地震](../Page/嘉靖大地震.md "wikilink")，全國死亡人數佔計高達82萬，是[中國歷史以至全球史上傷亡最為慘重的一次地震](../Page/中國歷史.md "wikilink")。

中华民国二年(1913年)改华州为华县。2015年10月，国务院批复同意撤销华县，设立渭南市华州区\[1\]。

## 行政区划

下辖1个[街道办事处](../Page/街道办事处.md "wikilink")、9个[镇](../Page/镇.md "wikilink")：

。

## 资源

华州区矿产资源丰富，[钼资源储量居](../Page/钼.md "wikilink")“世界第三、亚洲第一”，荣获“中国钼业之都”称号。

## 知名人物

  - [胡琏](../Page/胡琏.md "wikilink")，[中华民国时期](../Page/中华民国.md "wikilink")，[国共内战后期知名人物](../Page/国共内战.md "wikilink")，重要将领，随[国民政府迁往](../Page/国民政府.md "wikilink")[台湾后以镇守](../Page/台湾.md "wikilink")[金门著称](../Page/金门.md "wikilink")。\[2\]

<!-- end list -->

  - [史恒丰](../Page/史恒丰.md "wikilink")，[中华民国陆军军官随](../Page/中华民国陆军.md "wikilink")[国民政府迁往](../Page/国民政府.md "wikilink")[台湾后以镇守](../Page/台湾.md "wikilink")[金门之](../Page/金门.md "wikilink")[大胆岛著称](../Page/大胆岛.md "wikilink")。曾于1950年担任营长期间驻防于[大胆岛担任南山指挥官期间指挥](../Page/大胆岛.md "wikilink")[大胆岛战役](../Page/大胆岛战役.md "wikilink")。1975年以少将军阶退伍。2011年11月10日病逝，享年91岁。

## 参考文献

## 外部链接

  - [渭南市华州区人民政府网站](http://www.huaxian.gov.cn/)

{{-}}

[华州区](../Category/华州区.md "wikilink")
[区](../Category/渭南区县市.md "wikilink")
[渭南](../Category/陕西市辖区.md "wikilink")

1.
2.  [历史风云人物之胡璉將軍 -
    金門的現代「恩主公」](http://video.sina.com.cn/v/b/53451438-2149380552.html)