[Warner_Bros.png](https://zh.wikipedia.org/wiki/File:Warner_Bros.png "fig:Warner_Bros.png")
**华纳兄弟影业**是[华纳兄弟娱乐公司的主要](../Page/华纳兄弟.md "wikilink")[电影](../Page/电影.md "wikilink")[制作部门](../Page/电影制作.md "wikilink")，创立于1926年，總部位於[加州](../Page/加州.md "wikilink")[洛杉磯縣的](../Page/洛杉磯縣.md "wikilink")[荷里活](../Page/荷里活.md "wikilink")。

## 著名电影

以下是华纳兄弟影业制作、合拍或者发行的部分电影列表：

### 1930年代

  - [跳舞吧宝贝](../Page/跳舞吧宝贝.md "wikilink")（Dancing Sweeties）（1930）
  - [三对佳偶](../Page/三对佳偶.md "wikilink")（Three on a Match）（1932）
  - [扮演上帝的男人](../Page/扮演上帝的男人.md "wikilink")（The Man Who Played
    God）（1932）
  - [异国情缘](../Page/异国情缘.md "wikilink")（Ever in My Heart）（1933）
  - [56号大街上的房子](../Page/56号大街上的房子.md "wikilink")（The House on 56th
    Street）（1933）
  - [圣路易斯的孩子](../Page/圣路易斯的孩子.md "wikilink")（The St. Louis Kid）（1934）
  - [神探南茜和隐蔽的楼梯](../Page/神探南茜和隐蔽的楼梯.md "wikilink")（Nancy Drew and the
    Hidden Staircase）（1939）

### 1940年代

  - [一切成真](../Page/一切成真.md "wikilink")（It All Came True）（1940）
  - [讲话太多的男人](../Page/讲话太多的男人.md "wikilink")（The Man Who Talked Too
    Much）（1940）
  - [伦敦能坚持下去](../Page/伦敦能坚持下去.md "wikilink")（London Can Take It\!）（1940）
  - [夜的布鲁斯](../Page/夜的布鲁斯.md "wikilink")（Blues in the Night）（1941）
  - [伟大的谎言](../Page/伟大的谎言.md "wikilink")（The Great Lie）（1941）
  - [三个人的蜜月](../Page/三个人的蜜月.md "wikilink")（Honeymoon for Three）（1941）
  - [渡过黑暗](../Page/渡过黑暗.md "wikilink")（All Through the Night）（1941）
  - [今晚的目标](../Page/今晚的目标.md "wikilink")（Target for Tonight）（1941）
  - [龟兔赛跑](../Page/龟兔赛跑.md "wikilink")（Tortoise Wins by a Hare）（1943）
  - [汤姆·塔克与达菲鸭](../Page/汤姆·塔克与达菲鸭.md "wikilink")（Tom Turk and
    Daffy）（1944）
  - [他们让我变成亡命徒](../Page/他们让我变成亡命徒.md "wikilink")（They Made Me a
    Fugitive）（1947）

### 1950年代

  - [发生什么了，博士？](../Page/发生什么了，博士？.md "wikilink")（What's Up Doc?）（1950）
  - [秘密行动](../Page/秘密行动.md "wikilink")（1952）
  - [囚笼之鸟崔弟](../Page/囚笼之鸟崔弟.md "wikilink")（1952）
  - [发大水了](../Page/发大水了.md "wikilink")（1952）
  - [兔八哥遇上大嘴怪](../Page/兔八哥遇上大嘴怪.md "wikilink")（1954）

### 1960年代

  - [魔鬼、肉体和宽恕](../Page/魔鬼、肉体和宽恕.md "wikilink") （1961）
  - [兰闺惊变](../Page/兰闺惊变.md "wikilink") （1962）
  - [窈窕淑女](../Page/窈窕淑女.md "wikilink") （1964）
  - [巴塞洛缪与车轮](../Page/巴塞洛缪与车轮.md "wikilink") （1964）
  - [哈姆雷特](../Page/哈姆雷特.md "wikilink") （1964）
  - [坦克大決戰](../Page/坦克大決戰_\(電影\).md "wikilink") （1965）
  - [双重间谍网](../Page/双重间谍网.md "wikilink") （1966）

### 1970年代

  - [發條橙](../Page/發條橙.md "wikilink") （1971）
  - [驅魔人](../Page/驅魔人.md "wikilink") （1973）
  - [驅魔人續集](../Page/驅魔人續集.md "wikilink") （1977）
  - [超人](../Page/超人_\(電影\).md "wikilink") （1978, distribution only）

### 1980年代

  - [戰火屠城](../Page/戰火屠城.md "wikilink") （1984）
  - [小魔怪](../Page/小魔怪.md "wikilink") （1984） （co-production with [Amblin
    Entertainment](../Page/Amblin_Entertainment.md "wikilink")）
  - [紫雨](../Page/紫雨.md "wikilink") （1984）
  - [學警出更](../Page/學警出更.md "wikilink") （1984）
  - [學警出更2](../Page/學警出更2.md "wikilink") （1985）
  - [學警出更3](../Page/學警出更3.md "wikilink") （1986）
  - [轟天砲](../Page/轟天砲.md "wikilink") （1987）
  - [學警出更4](../Page/學警出更4.md "wikilink") （1987）
  - [太陽帝國](../Page/太陽帝國.md "wikilink") （1987） （co-production with
    [Amblin Entertainment](../Page/Amblin_Entertainment.md "wikilink")）
  - [學警出更5](../Page/學警出更5.md "wikilink") （1988）
  - [蝙蝠俠](../Page/蝙蝠俠_\(1989年電影\).md "wikilink") （1989） （co-production
    with [PolyGram Pictures](../Page/PolyGram_Pictures.md "wikilink")）
  - [轟天砲續集](../Page/轟天砲續集.md "wikilink") （1989）

### 1990年代

  - [四海好傢伙](../Page/四海好傢伙.md "wikilink") （1990）
  - [轟天炮3](../Page/轟天炮3.md "wikilink") （1992）
  - [蝙蝠俠再戰風雲](../Page/蝙蝠俠再戰風雲.md "wikilink") （1992） （co-production with
    [PolyGram Pictures](../Page/PolyGram_Pictures.md "wikilink")）
  - [夜訪吸血鬼](../Page/夜訪吸血鬼.md "wikilink") （1994）
  - [太空也入樽](../Page/太空也入樽.md "wikilink") （1996）
  - [蝙蝠俠3：蝙蝠俠與羅賓](../Page/蝙蝠俠3：蝙蝠俠與羅賓.md "wikilink") （1997）
    （co-production with [PolyGram
    Pictures](../Page/PolyGram_Pictures.md "wikilink")）
  - [轟天炮4](../Page/轟天炮4.md "wikilink") （1998）
  - [國王與我](../Page/國王與我.md "wikilink") （1999）
  - [大开眼戒](../Page/大开眼戒.md "wikilink") （1999）
  - [駭客任務](../Page/駭客任務.md "wikilink")([22世紀殺人網絡](../Page/22世紀殺人網絡.md "wikilink"))
    （1999） （co-production with [Village Roadshow
    Pictures](../Page/Village_Roadshow_Pictures.md "wikilink")）

### 2000年代

  - [人工智能](../Page/人工智能_\(电影\).md "wikilink") （2001, with
    [DreamWorks](../Page/DreamWorks.md "wikilink")）
  - [哈利波特－神秘的魔法石](../Page/哈利波特－神秘的魔法石_\(電影\).md "wikilink") （2001）
  - [十一罗汉](../Page/十一罗汉_\(2001年电影\).md "wikilink") （2001） （2004年推出续集）
  - [哈利·波特与密室](../Page/哈利·波特与密室_\(电影\).md "wikilink") （2002）
  - [史酷比](../Page/史酷比.md "wikilink")（真人电影）（2002）
  - [时间机器](../Page/时间机器_\(电影\).md "wikilink")
    （2002，与[梦工厂合拍](../Page/梦工厂.md "wikilink")）
  - [最后的武士](../Page/最后的武士.md "wikilink") （2003）
  - [駭客任務2：重裝上陣](../Page/駭客任務2：重裝上陣.md "wikilink") （2003）
  - [华纳巨星总动员](../Page/华纳巨星总动员.md "wikilink") （2003）
  - [駭客任務3：最後戰役](../Page/駭客任務3：最後戰役.md "wikilink") （2003）
  - [爱是妥协](../Page/爱是妥协.md "wikilink") （2003）
    （与[哥伦比亚影业合拍](../Page/哥伦比亚影业.md "wikilink")）
  - [哈利·波特与阿兹卡班的囚徒](../Page/哈利·波特与阿兹卡班的囚徒_\(电影\).md "wikilink") （2004）
  - [歌剧魅影](../Page/歌剧魅影_\(2004年电影\).md "wikilink") （2004）
  - [极地特快](../Page/极地特快_\(电影\).md "wikilink")
    （2004）（与[城石娱乐合拍](../Page/城石娱乐.md "wikilink")）
  - [飞行者](../Page/飞行者.md "wikilink")
    （2004，与[米拉麦克斯影片合拍](../Page/米拉麦克斯影片.md "wikilink")）
  - [特洛伊](../Page/特洛伊_\(电影\).md "wikilink") （2004）
  - [十二罗汉](../Page/十二罗汉.md "wikilink") （2004）
  - [蝙蝠侠：侠影之谜](../Page/蝙蝠侠：侠影之谜.md "wikilink") （2005）
  - [逃出克隆岛](../Page/逃出克隆岛.md "wikilink") （2005, with
    [DreamWorks](../Page/DreamWorks.md "wikilink")）
  - [查理与巧克力工厂](../Page/查理与巧克力工厂_\(电影\).md "wikilink") （co-production
    with [Village Roadshow
    Pictures](../Page/Village_Roadshow_Pictures.md "wikilink")）
  - [帝企鹅日记](../Page/帝企鹅日记.md "wikilink") （2005）
  - [魔間行者](../Page/魔間行者.md "wikilink") （2005）
  - [哈利·波特与火焰杯](../Page/哈利·波特与火焰杯_\(电影\).md "wikilink") （2005）
  - [恐怖蜡像馆](../Page/恐怖蜡像馆.md "wikilink") （2005）
  - [水中的女人](../Page/水中的女人.md "wikilink") （2006, with [Legendary
    Pictures](../Page/Legendary_Pictures.md "wikilink")）
  - [海神号](../Page/海神号_\(电影\).md "wikilink") （2006）
  - [超人归来](../Page/超人归来.md "wikilink")
    （2006，与[威秀影业和](../Page/威秀影业.md "wikilink")[DC漫画合拍](../Page/DC漫画.md "wikilink")）
  - [别惹蚂蚁](../Page/别惹蚂蚁.md "wikilink") （2006, with [Legendary
    Pictures](../Page/Legendary_Pictures.md "wikilink") and [Playtone
    Pictures](../Page/Playtone_Pictures.md "wikilink")）
  - [快樂腳](../Page/快樂腳.md "wikilink") （2006, with [Village Roadshow
    Pictures](../Page/Village_Roadshow_Pictures.md "wikilink")）
  - [無間道風雲](../Page/無間道風雲.md "wikilink") （2006）
  - [V煞](../Page/V煞.md "wikilink") （2006） （co-production with [Virtual
    Studios](../Page/Virtual_Studios.md "wikilink"), [Vertigo DC
    Comics](../Page/Vertigo_DC_Comics.md "wikilink") and [Silver
    Pictures](../Page/Silver_Pictures.md "wikilink")）
  - [跳越時空的情書](../Page/跳越時空的情書.md "wikilink") （2006）
  - [父辈的旗帜](../Page/父辈的旗帜.md "wikilink") （2006, with
    [DreamWorks](../Page/DreamWorks.md "wikilink") and [Amblin
    Entertainment](../Page/Amblin_Entertainment.md "wikilink")）
  - [300壯士：斯巴達的逆襲](../Page/300壯士：斯巴達的逆襲.md "wikilink") （2007）
  - [十三罗汉](../Page/十三罗汉.md "wikilink") （2007）
  - [血鑽](../Page/血鑽_\(電影\).md "wikilink") （2007）
  - [哈利波特－鳳凰會的密令](../Page/哈利波特－鳳凰會的密令_\(電影\).md "wikilink")（Harry Potter
    and the Order of the Phoenix） （2007）
  - [哈利波特－混血王子的背叛](../Page/哈利波特－混血王子的背叛_\(電影\).md "wikilink") （2008）
  - [蝙蝠侠：黑夜之神](../Page/蝙蝠侠：黑夜之神.md "wikilink")（2008）

### 2010年代

  - [全面啟動](../Page/全面啟動.md "wikilink")（2010）
  - [哈利波特－死神的聖物](../Page/哈利波特—死神的聖物_\(電影\).md "wikilink") （2010/2011）
  - [銀魂劇場版:新譯紅櫻篇](../Page/銀魂劇場版_新譯紅櫻篇.md "wikilink")（2010）
  - [銀魂完結篇 永遠的萬事屋](../Page/銀魂劇場版_完結篇_永遠的萬事屋.md "wikilink")（2013）

[Hua](../Category/美国电影制作公司.md "wikilink")
[Category:1926年成立的公司](../Category/1926年成立的公司.md "wikilink")