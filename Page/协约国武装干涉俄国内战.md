**协约国武装干涉俄国内战**（**[俄语](../Page/俄语.md "wikilink")：**）是指在1918年到1920年期间，[英國](../Page/不列颠帝国.md "wikilink")、[法國](../Page/法兰西第三共和国.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[美国](../Page/美利坚合众國.md "wikilink")、[日本](../Page/大日本帝国.md "wikilink")、[中国和其他一些](../Page/中华民国.md "wikilink")[第一次世界大战中的](../Page/第一次世界大战.md "wikilink")[协约国](../Page/协约国.md "wikilink")，对**[俄國內戰](../Page/俄國內戰.md "wikilink")**进行的武装干涉。

在协约国武装干涉期间，外国军队的军事存在被[布尔什维克有效地用于爱国宣传](../Page/布尔什维克.md "wikilink")，影响了俄国人民，最终赢得了内战的胜利。

## 协约国干涉的动机

1917年3月发生了许多改变[第一次世界大战进程的事件](../Page/第一次世界大战.md "wikilink")。随着[俄罗斯帝国](../Page/俄罗斯帝国.md "wikilink")[罗曼诺夫王朝皇帝](../Page/罗曼诺夫王朝.md "wikilink")[尼古拉二世的退位和](../Page/尼古拉二世.md "wikilink")[俄罗斯临时政府的成立](../Page/俄罗斯临时政府.md "wikilink")，美国总统[伍德罗·威尔逊對加入](../Page/伍德罗·威尔逊.md "wikilink")[协约国一方参战的最后保留不再存在](../Page/协约国.md "wikilink")。因此美国参战，反对新成立的[蘇俄政府](../Page/蘇俄.md "wikilink")，而[亚历山大·克伦斯基领导的俄国临时政府保证继续在东部战线与](../Page/亚历山大·克伦斯基.md "wikilink")[德意志帝国战斗](../Page/德意志帝国.md "wikilink")。为了使俄国临时政府能够完成其继续同德意志帝国战争的保证，美利坚合众国开始向其提供经济和技术援助。

在[东部战线](../Page/東方戰線_\(第一次世界大戰\).md "wikilink")，俄国军队被证实不是德国和奥匈帝国军队的对手。1917年6月18日俄国人的攻势被德意志帝国军的反攻压倒性地击败。士气受挫的俄国军队，深受兵变和士兵逃跑的困扰，东部战线迅速崩溃。

1917年10月[十月革命推翻了克伦斯基的临时政府](../Page/十月革命.md "wikilink")，5个月后[俄罗斯苏维埃联邦社会主义共和国与德国签署了](../Page/俄罗斯苏维埃联邦社会主义共和国.md "wikilink")[布列斯特-立陶夫斯克条约](../Page/布列斯特-立陶夫斯克条约.md "wikilink")，正式在东方战线结束了战争。这使得德国人可以重新部署军力，调往[西部战线](../Page/西方戰線_\(第一次世界大戰\).md "wikilink")，在那里耗尽了英国和法国军队，当时尚未得到的支援。

[日本在這次戰爭的介入有其複](../Page/大日本帝國.md "wikilink")1的因素：日本在這次戰爭派出的军队人数是各國中最多的，在吞併了[大韓帝國](../Page/大韓帝國.md "wikilink")、[內滿洲及部分中國的領土後](../Page/內滿洲.md "wikilink")，日本有非常強烈興趣去增強其在[遠東的影響力](../Page/遠東.md "wikilink")、甚或再擴展其版圖。

## 协约国军队

以下數量的外國士兵進軍俄羅斯：

  - 28,000 日本（後來增兵至70,000）
  - 50,000 捷克斯洛伐克
  - 24,000 希臘
  - 40,000 英国
  - 13,000 美国
  - 12,000 法国
  - 12,000 波兰
  - 4,000 加拿大
  - 4,000 塞尔维亚
  - 4,000 罗马尼亚
  - 2,500 意大利
  - 2,000 中国
  - 150 澳大利亞

## 协约国在俄国北部的干涉

[Mark_in_Arkhangelsk_RU.JPG](https://zh.wikipedia.org/wiki/File:Mark_in_Arkhangelsk_RU.JPG "fig:Mark_in_Arkhangelsk_RU.JPG")缴获的。\]\]

1918年11月底，英军少将威廉·埃蒙德·艾恩赛德任协约国北俄干涉军总司令。

  - [英国陆军](../Page/英国陆军.md "wikilink"): , , 第10营, 一部, )

  - [英国皇家海军](../Page/英国皇家海军.md "wikilink")
    ([美国海军第](../Page/美国海军.md "wikilink")53分遣队的水兵–包括 –
    巡洋舰USS Olympia C-6)

  - [英国皇家空军](../Page/英国皇家空军.md "wikilink") 与水上飞机，以及战斗机)\[1\]

  - [法国陆军](../Page/法国陆军.md "wikilink") (第21殖民地营)

  - 第16旅第67、第68炮兵营

  - (SBAL)英国训练的反布尔什维克武装，包括

  - [俄国白军](../Page/俄国白军.md "wikilink")
    ：[克伦斯基临时政府的军队](../Page/克伦斯基.md "wikilink")，由自称“北俄罗斯总督”的[葉夫根尼·米勒中将领导指挥](../Page/葉夫根尼·米勒.md "wikilink")。

  - 美国陆军, 编成 (即)辖：, （“底特律自己的”部队）, ,
    )，共计近5500人。[西班牙流感暴发流行](../Page/西班牙流感.md "wikilink")。244人阵亡或者其他亡，305人受伤，超过100人死于流感，1人自杀。

  - 美国陆军,  、，1919年4月派来维护摩尔曼斯克-彼得格勒铁路线。共计500人。

  - 波兰军队

  - 塞尔维亚军队

  - 意大利军队

  - ，1919年5月抵达接受美军撤走后留下的防务。

## 协约国在西伯利亚的干涉

<center>

<File:Wladiwostok> Parade
1918.jpg|1918年[海参崴的协约国部队](../Page/海参崴.md "wikilink")
[File:The_Illustration_of_The_Siberian_War,_No._16.__The_Japanese_Army_Occupied_Vragaeschensk_(Blagoveshchensk).jpg|一张日本宣传海报，表现日本军队入侵](File:The_Illustration_of_The_Siberian_War,_No._16.__The_Japanese_Army_Occupied_Vragaeschensk_\(Blagoveshchensk\).jpg%7C一张日本宣传海报，表现日本军队入侵)[海兰泡](../Page/海兰泡.md "wikilink")。

</center>

## 协约国在俄罗斯西北部的干涉

## 协约国在南俄罗斯与乌克兰的干涉

## 协约国在比萨拉比亚的干涉

## 协约国在高加索的干涉

## 协约国在外里海（中亚）的干涉

## 參見

  - [远东共和国](../Page/远东共和国.md "wikilink")

## 參考文獻

[Category:俄國內戰](../Category/俄國內戰.md "wikilink")
[Category:俄羅斯歷史](../Category/俄羅斯歷史.md "wikilink")
[Category:俄羅斯戰爭](../Category/俄羅斯戰爭.md "wikilink")
[Category:20世纪美国军事](../Category/20世纪美国军事.md "wikilink")
[Category:1918年冲突](../Category/1918年冲突.md "wikilink")
[Category:1919年冲突](../Category/1919年冲突.md "wikilink")
[Category:1920年冲突](../Category/1920年冲突.md "wikilink")

1.  [British Military Aviation in 1918 -
    Part 2](http://www.rafmuseum.org/milestones-of-flight/british_military/1918_2.cfm)
     Royal Air Force Museum