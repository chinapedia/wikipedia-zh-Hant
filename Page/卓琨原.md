**卓琨原**（1969年10月12日－）為[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，曾經效力於[中華職棒](../Page/中華職棒.md "wikilink")[時報鷹隊](../Page/時報鷹.md "wikilink")，守備位置為[投手](../Page/投手.md "wikilink")，後來因為[職棒簽賭案而遭到](../Page/職棒簽賭案.md "wikilink")[中華職棒終身禁賽](../Page/中華職棒.md "wikilink")。

## 經歷

  - [嘉義縣朴子國小少棒隊](../Page/嘉義縣.md "wikilink")
  - [台南縣官田國中青少棒隊](../Page/台南縣.md "wikilink")
  - [臺北縣板橋國中青少棒隊](../Page/新北市.md "wikilink")
  - [臺北縣中華中學青棒隊](../Page/新北市.md "wikilink")
  - 榮工棒球隊
  - 陸光棒球隊
  - [時報鷹棒球隊](../Page/時報鷹.md "wikilink")
  - [中華職棒](../Page/中華職棒.md "wikilink")[時報鷹隊](../Page/時報鷹.md "wikilink")

## 職棒生涯成績

| 年度    | 球隊                               | 出賽  | 勝投 | 敗投 | 中繼 | 救援 | 完投 | 完封 | 四死  | 三振  | 責失  | 投球局數  | 防禦率  |
| ----- | -------------------------------- | --- | -- | -- | -- | -- | -- | -- | --- | --- | --- | ----- | ---- |
| 1993年 | [時報鷹](../Page/時報鷹.md "wikilink") | 36  | 5  | 5  | 0  | 4  | 2  | 0  | 60  | 53  | 42  | 106.0 | 3.57 |
| 1994年 | [時報鷹](../Page/時報鷹.md "wikilink") | 29  | 6  | 3  | 0  | 0  | 0  | 0  | 32  | 37  | 29  | 75.2  | 3.45 |
| 1995年 | [時報鷹](../Page/時報鷹.md "wikilink") | 34  | 3  | 3  | 0  | 1  | 1  | 0  | 22  | 28  | 29  | 79.0  | 3.08 |
| 1996年 | [時報鷹](../Page/時報鷹.md "wikilink") | 26  | 1  | 2  | 0  | 1  | 0  | 0  | 22  | 21  | 39  | 57.0  | 6.16 |
| 合計    | 4年                               | 125 | 15 | 13 | 0  | 6  | 3  | 0  | 136 | 139 | 137 | 317.2 | 3.88 |

## 特殊事蹟

## 外部連結

[K](../Category/卓姓.md "wikilink") [Z](../Category/在世人物.md "wikilink")
[Z](../Category/1969年出生.md "wikilink")
[Z](../Category/台灣棒球選手.md "wikilink")
[Z](../Category/時報鷹隊球員.md "wikilink")
[Category:中華職棒終身禁賽名單](../Category/中華職棒終身禁賽名單.md "wikilink")