<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p><strong>南下加利福尼亞州<br />
Baja California Sur</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>位置</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Baja_California_Sur_en_México.svg" title="fig:Baja_California_Sur_en_México.svg">Baja_California_Sur_en_México.svg</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>基本資料</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>首府</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>面積</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>人口<br />
<small></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>州長<br />
<small>（2005-2011）</small></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/墨西哥眾議院.md" title="wikilink">眾議院議席數</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/墨西哥參議院.md" title="wikilink">參議院議席數</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/ISO_3166-2.md" title="wikilink">ISO 3166-2</a><br />
<small>郵政簡寫</small></p></td>
</tr>
</tbody>
</table>

**南下加利福尼亞州**（），簡稱**南下加州**，是[墨西哥三十一個](../Page/墨西哥.md "wikilink")[州之一](../Page/墨西哥行政區劃.md "wikilink")，位於[下加利福尼亞半島南部](../Page/下加利福尼亞半島.md "wikilink")，西臨[太平洋](../Page/太平洋.md "wikilink")，東鄰[下加利福尼亞灣](../Page/下加利福尼亞灣.md "wikilink")，北以北緯28度與[下加利福尼亞州分開](../Page/下加利福尼亞州.md "wikilink")。首府[拉巴斯](../Page/拉巴斯_\(南下加利福尼亞州\).md "wikilink")。

1888年成立準州，1974年9月24日建州。

[\*](../Category/南下加利福尼亚州.md "wikilink")