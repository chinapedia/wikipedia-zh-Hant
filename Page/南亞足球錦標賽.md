**南亞足球錦標賽**是由[南亞足球協會兩年一度舉辦的一個大型男子](../Page/南亞足球協會.md "wikilink")[足球比賽](../Page/足球.md "wikilink")。參賽國家包括[阿富汗](../Page/阿富汗國家足球隊.md "wikilink")、[孟加拉](../Page/孟加拉國家足球代表隊.md "wikilink")、[不丹](../Page/不丹國家足球隊.md "wikilink")、[印度](../Page/印度國家足球隊.md "wikilink")、[馬爾代夫](../Page/馬爾代夫國家足球隊.md "wikilink")、[尼泊爾](../Page/尼泊爾國家足球隊.md "wikilink")、[巴基斯坦和](../Page/巴基斯坦國家足球隊.md "wikilink")[斯里蘭卡](../Page/斯里蘭卡國家足球隊.md "wikilink")。

## 歷屆賽事

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 9%" />
<col style="width: 1%" />
<col style="width: -6%" />
<col style="width: 1%" />
<col style="width: -6%" />
<col style="width: 1%" />
<col style="width: 6%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>主辦國</p></th>
<th></th>
<th><p>決賽</p></th>
<th></th>
<th><p>季軍戰</p></th>
<th></th>
<th><p>參賽<br />
隊數</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>冠軍</p></td>
<td><p>比數</p></td>
<td><p>亞軍</p></td>
<td><p>季軍</p></td>
<td><p>比數</p></td>
<td><p>殿軍</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1993<br />
<em><a href="../Page/1993年南亞足球錦標賽.md" title="wikilink">詳細</a></em></p></td>
<td></td>
<td><p><strong></strong></p></td>
<td><p><strong>小組賽</strong></p></td>
<td></td>
<td></td>
<td><p><strong>小組賽</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1995<br />
<em><a href="../Page/1995年南亞足球錦標賽.md" title="wikilink">詳細</a></em></p></td>
<td></td>
<td><p><strong></strong></p></td>
<td><p><strong>1–0</strong><br />
<small>（黃金入球）</small></p></td>
<td></td>
<td></td>
<td><p><strong>小組賽</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997<br />
<em><a href="../Page/1997年南亞足球錦標賽.md" title="wikilink">詳細</a></em></p></td>
<td></td>
<td><p><strong></strong></p></td>
<td><p><strong>5–1</strong></p></td>
<td></td>
<td></td>
<td><p><strong>1–0</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999<br />
<em><a href="../Page/1999年南亞足球錦標賽.md" title="wikilink">詳細</a></em></p></td>
<td></td>
<td><p><strong></strong></p></td>
<td><p><strong>2–0</strong></p></td>
<td></td>
<td></td>
<td><p><strong>2–0</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003<br />
<em><a href="../Page/2003年南亞足球錦標賽.md" title="wikilink">詳細</a></em></p></td>
<td></td>
<td><p><strong></strong></p></td>
<td><p><strong>1–1</strong><small>（<a href="../Page/加時賽.md" title="wikilink">加時</a>）<br />
<strong>5–3</strong>（<a href="../Page/互射十二碼.md" title="wikilink">十二碼</a>）</small></p></td>
<td></td>
<td></td>
<td><p><strong>2–1</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005<br />
<em><a href="../Page/2005年南亞足球錦標賽.md" title="wikilink">詳細</a></em></p></td>
<td></td>
<td><p><strong></strong></p></td>
<td><p><strong>2–0</strong></p></td>
<td></td>
<td><p>及 </p></td>
<td><p>8</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008<br />
<em><a href="../Page/2008年南亞足球錦標賽.md" title="wikilink">詳細</a></em></p></td>
<td><p><br />
</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>1–0</strong></p></td>
<td></td>
<td><p>及 </p></td>
<td><p>8</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009<br />
<em><a href="../Page/2009年南亞足球錦標賽.md" title="wikilink">詳細</a></em></p></td>
<td></td>
<td><p><strong></strong></p></td>
<td><p><strong>0–0</strong><small>（<a href="../Page/加時賽.md" title="wikilink">加時</a>）<br />
<strong>3–1</strong>（<a href="../Page/互射十二碼.md" title="wikilink">十二碼</a>）</small></p></td>
<td></td>
<td><p>及 </p></td>
<td><p>8</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011<br />
<em><a href="../Page/2011年南亞足球錦標賽.md" title="wikilink">詳細</a></em></p></td>
<td></td>
<td><p><strong></strong></p></td>
<td><p><strong>4–0</strong></p></td>
<td></td>
<td><p>及 </p></td>
<td><p>8</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013<br />
<em><a href="../Page/2013年南亞足球錦標賽.md" title="wikilink">詳細</a></em></p></td>
<td></td>
<td><p><strong></strong></p></td>
<td><p><strong>2–0</strong></p></td>
<td></td>
<td><p>及 </p></td>
<td><p>8</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015<br />
<em><a href="../Page/2015年南亞足球錦標賽.md" title="wikilink">詳細</a></em></p></td>
<td></td>
<td><p><strong></strong></p></td>
<td><p><strong>2–1</strong><small>（<a href="../Page/加時賽.md" title="wikilink">加時</a>）</small></p></td>
<td></td>
<td><p>及 </p></td>
<td><p>7</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2018<br />
<em><a href="../Page/2018年南亞足球錦標賽.md" title="wikilink">詳細</a></em></p></td>
<td></td>
<td></td>
<td><p><strong>–</strong></p></td>
<td></td>
<td></td>
<td><p>7</p></td>
<td></td>
</tr>
</tbody>
</table>

## 球隊成績

<table>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p>冠軍</p></th>
<th><p>亞軍</p></th>
<th><p>季軍</p></th>
<th><p>殿軍</p></th>
<th><p>準決賽負方</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><strong>7</strong> (1993, 1997, 1999, 2005, 2009, 2011, 2015)</p></td>
<td><p><strong>3</strong> (1995, 2008, 2013)</p></td>
<td><p><strong>1</strong> (2003)</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><strong>1</strong> (2008)</p></td>
<td><p><strong>3</strong> (1997, 2003, 2009)</p></td>
<td><p><strong>1</strong> (1999)</p></td>
<td></td>
<td><p><strong>4</strong> (2005, 2011, 2013, 2015)</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><strong>1</strong> (2003)</p></td>
<td><p><strong>2</strong> (1999, 2005)</p></td>
<td><p><strong>1</strong> (1995)</p></td>
<td></td>
<td><p><strong>1</strong> (2009)</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><strong>1</strong> (2013)</p></td>
<td><p><strong>2</strong> (2011, 2015)</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><strong>1</strong> (1995)</p></td>
<td><p><strong>1</strong> (1993)</p></td>
<td></td>
<td><p><strong>1</strong> (1997)</p></td>
<td><p><strong>3</strong> (2008, 2009, 2015)</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td><p><strong>1</strong> (1993)</p></td>
<td><p><strong>2</strong> (1995, 1999)</p></td>
<td><p><strong>2</strong> (2011, 2013)</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td><p><strong>1</strong> (1997)</p></td>
<td><p><strong>2</strong> (1993, 2003)</p></td>
<td><p><strong>1</strong> (2005)</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><strong>1</strong> (2008)</p></td>
</tr>
</tbody>
</table>

## 相關條目

  - [亞洲足協](../Page/亞洲足協.md "wikilink")
  - [亞洲盃](../Page/亞洲盃足球賽.md "wikilink")
  - [東亞足球錦標賽](../Page/東亞足球錦標賽.md "wikilink")
  - [東南亞足球錦標賽](../Page/東南亞足球錦標賽.md "wikilink")

## 外部連結

  - [RSSSF Page on the South Asian Federation
    Cup](http://www.rsssf.com/tabless/saffcup.html)

[Category:亞洲國家隊足球賽事](../Category/亞洲國家隊足球賽事.md "wikilink")