[Nankin_1912.jpg](https://zh.wikipedia.org/wiki/File:Nankin_1912.jpg "fig:Nankin_1912.jpg")
**南京城**是[明代修筑的](../Page/明代.md "wikilink")[都城](../Page/都城.md "wikilink")，延续至今即[南京市](../Page/南京市.md "wikilink")[主城区所在](../Page/主城区.md "wikilink")，是[南京历史上修筑的等级最高的](../Page/南京历史.md "wikilink")[城池之一](../Page/城池.md "wikilink")。

南京有[六朝古都之称](../Page/六朝古都.md "wikilink")，明代之前有数个政权定都于此，但至明朝才第一次成为全国性政权的都城。

## 明代

[元朝末年](../Page/元朝.md "wikilink")，[朱元璋建立吴政权](../Page/朱元璋.md "wikilink")，初改[集庆路为](../Page/集庆路.md "wikilink")[应天府](../Page/应天府_\(明朝\).md "wikilink")。原[金陵城东的](../Page/金陵城.md "wikilink")[燕雀湖被填平](../Page/燕雀湖.md "wikilink")，以建吴王新宫，即[明故宫](../Page/明故宫.md "wikilink")。有说填湖泥土来自城南的[三山](../Page/三山.md "wikilink")，故有“移三山，填燕雀”的说法\[1\]。吴元年（1367年），吴王新宫建成。[洪武元年](../Page/洪武.md "wikilink")（1368年）八月，建都，曰南京。洪武二年（1369年）九月，在[金陵城的基础上建南京城](../Page/金陵城.md "wikilink")。洪武六年八月，城池建成。与中国城池的传统正方形或矩形平面布局不同，南京城布局为不规则形状。由四重城墙构建出不同的城池区域，由内而外，依次是[宫城](../Page/宫城_\(中国\).md "wikilink")、[皇城](../Page/皇城.md "wikilink")、内城和外城。洪武十一年（1378年），曰京师。[永乐元年](../Page/永乐_\(明朝\).md "wikilink")（1403年），仍称南京。后[明朝政府](../Page/明朝政府.md "wikilink")[迁都北京](../Page/永乐迁都.md "wikilink")。南京城逐为[留都](../Page/留都.md "wikilink")。而应天府下辖的[上元县](../Page/上元县.md "wikilink")、[江宁县亦为](../Page/江宁县.md "wikilink")[倚郭县](../Page/倚郭县.md "wikilink")。两县依旧沿袭以[秦淮河为界](../Page/秦淮河.md "wikilink")，分南京城，同城而治的管辖状态。

## 清代

[清朝](../Page/清朝.md "wikilink")[顺治初年](../Page/顺治.md "wikilink")，[清军攻占南京](../Page/清军.md "wikilink")，圈占[明故宫](../Page/明故宫.md "wikilink")，以南京城东部置[江宁满城](../Page/江宁满城.md "wikilink")。

[咸丰三年](../Page/咸丰_\(年号\).md "wikilink")（1853年），[太平军攻克江宁城](../Page/太平军.md "wikilink")，定都于此，改名天京，筑[天王府](../Page/天王府.md "wikilink")。[同治三年](../Page/同治.md "wikilink")（1864年），[湘军攻克天京](../Page/湘军.md "wikilink")，[屠城并焚毁天王府](../Page/屠城.md "wikilink")。

## 民国时期

民国十六年（1927年），北伐军攻占南京。其后，设立[南京特别市](../Page/南京特别市.md "wikilink")，后成为[中华民国首都](../Page/中华民国首都.md "wikilink")。1927年后，[国民政府颁布](../Page/国民政府.md "wikilink")[首都计划](../Page/首都计划.md "wikilink")，开启了南京城现代化之路。1928年，南京铺设了第一条柏油马路——中山大道。从1929年至1937年间，在南京进行了大规模的首都建设。但城区并未突破明代南京城墙范围。[通济门](../Page/通济门.md "wikilink")、[尧化门](../Page/尧化门.md "wikilink")、[高桥门等地仍存在大片农田](../Page/高桥门.md "wikilink")，并以各类[土特产知名](../Page/土特产.md "wikilink")\[2\]。

## 参考文献

{{-}}

[Category:明朝都城](../Category/明朝都城.md "wikilink")
[Category:南京城池](../Category/南京城池.md "wikilink")
[Category:南京明代建筑物](../Category/南京明代建筑物.md "wikilink")

1.
2.