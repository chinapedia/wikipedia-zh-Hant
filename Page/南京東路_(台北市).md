<div class="notice metadata" id="disambig">

[Disambig.svg](https://zh.wikipedia.org/wiki/File:Disambig.svg "fig:Disambig.svg")
<small>本條目敘述[台北的南京東路](../Page/台北市.md "wikilink")。關於[上海的同名道路](../Page/上海.md "wikilink")，請參見**[南京東路
(上海)](../Page/南京東路_\(上海\).md "wikilink")**。</small>

<div style="font-size: small;">

</div>

</div>

[台北市南京東路二段_-_panoramio_(31).jpg](https://zh.wikipedia.org/wiki/File:台北市南京東路二段_-_panoramio_\(31\).jpg "fig:台北市南京東路二段_-_panoramio_(31).jpg")
[MRT_Nanjing_East_Road_Station,_Brother_Hotel_and_Sincere_Department_Store_20071227.jpg](https://zh.wikipedia.org/wiki/File:MRT_Nanjing_East_Road_Station,_Brother_Hotel_and_Sincere_Department_Store_20071227.jpg "fig:MRT_Nanjing_East_Road_Station,_Brother_Hotel_and_Sincere_Department_Store_20071227.jpg")（南京東路三段）\]\]
[台北小巨蛋_Taipei_Arena_-_panoramio.jpg](https://zh.wikipedia.org/wiki/File:台北小巨蛋_Taipei_Arena_-_panoramio.jpg "fig:台北小巨蛋_Taipei_Arena_-_panoramio.jpg")（南京東路四段）\]\]
**南京東路**為[臺北市的重要幹道](../Page/臺北市.md "wikilink")，大致呈東西向，西於[中山北路口接](../Page/中山北路_\(台北市\).md "wikilink")[南京西路](../Page/南京西路_\(台北市\).md "wikilink")，東於塔悠路口接麥帥一橋，最後在內湖接上國道一號高速公路，全程共有六段，其中南京東路一段至五段路中央布設[公車專用道](../Page/公車專用道.md "wikilink")。此外，[\<font
color={{台北捷運色彩](../Page/台北捷運松山線.md "wikilink")（[南京三民站](../Page/南京三民站.md "wikilink")-[松江南京站段](../Page/松江南京站.md "wikilink")）即沿著南京東路一段至五段，並在此路下設置4座車站，還有2段[自行車道及](../Page/自行車道.md "wikilink")[台北市公共自行車租賃系統](../Page/台北市公共自行車租賃系統.md "wikilink")5個租賃站。

臺灣最早的一條[高速公路](../Page/高速公路.md "wikilink")——[麥克阿瑟公路](../Page/麥克阿瑟公路.md "wikilink")（1964年5月2日—1977年7月10日）臺北市區路段，與今日的南京東路四段\[1\]－六段路線相同。

## 行經行政區域

（由西至東）

  - [中山區](../Page/中山區_\(臺北市\).md "wikilink")（一段、二段、三段復興北路以西）
  - [松山區](../Page/松山區_\(臺北市\).md "wikilink")（三段復興北路以東、四段、五段）
  - [內湖區](../Page/內湖區.md "wikilink")（六段）

## 歷史

南京東路原為水田，[臺灣日治時代](../Page/台灣日治時期.md "wikilink")1939年所訂定的都市計劃中已包括了南京東路的興建，在計畫中編為2號道路，預定全線路寬為40公尺。戰後[中華民國政府於](../Page/中華民國政府.md "wikilink")1951年闢建為防空疏散道路，自1956年起，[臺北市政府接受](../Page/臺北市政府.md "wikilink")[美援作為工程費](../Page/美援.md "wikilink")，並自籌土地徵收、拆遷房屋及青苗等補償費，同時成立[美援道路工程處](../Page/美援道路工程處.md "wikilink")，隔年（1957年），該路以[南京市命名為](../Page/南京市_\(中華民國\).md "wikilink")「南京東路」，利用中美基金貸款新鋪[柏油路面並拓延至五段](../Page/柏油.md "wikilink")\[2\]，而成為當時的九條[美援道路之一](../Page/美援.md "wikilink")\[3\]。美援道路的完成，使得以[大稻埕為首的資本和人口才開始湧進這塊地區](../Page/大稻埕.md "wikilink")，形成了日後[南京東路商圈的主流](../Page/南京東路商圈.md "wikilink")\[4\]

1960年代至今，南京東路一帶的金融機構、保險公司、商業辦公大樓、飯店及百貨公司如雨後春筍般冒出，南京商圈因而建立優越的商業地位，並躍升臺北市財經中心區。而現在的南京東路二段、三段、四段、五段一帶，更由於銀行等各類金融機構密集，而有「台北[華爾街](../Page/華爾街.md "wikilink")」（Taipei
Wall Street）之稱\[5\]。

1980年，已大部份併入[中山高速公路之](../Page/中山高速公路.md "wikilink")[麥帥公路僅存麥帥一橋至高速公路內湖交流道路段](../Page/麥帥公路.md "wikilink")，於1980年納入市區道路系統，命名為南京東路六段（路兩側房屋之門牌至2009年始改編為南京東路六段），路段長僅餘二公里。

## 分段

南京東路共分六段。

  - 一段：西於[中山南北路與](../Page/中山南路_\(台北市\).md "wikilink")[南京西路相接](../Page/南京西路_\(台北市\).md "wikilink")，東於[新生北路與南京東路二段相接](../Page/新生北路.md "wikilink")。
  - 二段：西於[新生北路與南京東路相接](../Page/新生北路.md "wikilink")，東於[建國北路與南京東路三段相接](../Page/建國高架道路.md "wikilink")。
  - 三段：西於[建國北路與南京東路二段相接](../Page/建國南北路_\(臺北市\).md "wikilink")，東於[敦化北路與南京東路四段相接](../Page/敦化北路.md "wikilink")。
  - 四段：西於[敦化北路與南京東路三段相接](../Page/敦化北路.md "wikilink")，東於[光復北路與南京東路五段相接](../Page/光復北路.md "wikilink")。
  - 五段：西於[光復北路與南京東路四段相接](../Page/光復北路.md "wikilink")，東於[塔悠路口與](../Page/塔悠路.md "wikilink")[麥帥一橋相接](../Page/麥帥一橋.md "wikilink")。
  - 六段：西起麥帥一橋與南京東路五段相接，平面部份東於成功路上方高架橋與[中山高速公路](../Page/中山高速公路.md "wikilink")（國道一號）相接，高架部份則於舊宗路口接上[環東大道](../Page/環東快速道路.md "wikilink")，往[南港方向及](../Page/南港區.md "wikilink")[福爾摩沙高速公路](../Page/福爾摩沙高速公路.md "wikilink")（國道三號）。

## 道路設計

### 車道數

  - 一段\~四段：雙向各四車道，最內側均為公車專用道。
  - 五段（光復北路-南京東路五段66巷）：雙向各四車道，最內側均為公車專用道。
  - 五段（南京東路五段66巷-三民路）：雙向各四車道，東向西最內側為公車專用道。
  - 五段（三民路-麥帥一橋）：雙向各四車道。
  - 六段：平面內側雙向各三車道，平面外側雙向各1\~2車道；高架雙向各二車道。

### 號碼

  - 一段：單號1\~31，雙號2\~140。
  - 二段：單號1\~167，雙號2\~224。
  - 三段：單號1\~337，雙號2\~350。
  - 四段：單號1\~199，雙號2\~186。
  - 五段：單號1\~411，雙號2\~392。
  - 六段：單號1\~149，雙號2\~352。

## 沿線設施

（由西到東）

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li>一段
<ul>
<li><a href="../Page/台北大倉久和大飯店.md" title="wikilink">台北大倉久和大飯店</a>（9號）</li>
<li><a href="../Page/康樂公園.md" title="wikilink">康樂公園</a>、<a href="../Page/林森公園.md" title="wikilink">林森公園</a>（35號）</li>
</ul></li>
<li><a href="../Page/台北市公共自行車租賃系統.md" title="wikilink">台北市公共自行車租賃系統</a><a href="../Page/林森公園.md" title="wikilink">林森公園站</a></li>
<li><a href="../Page/林森公園.md" title="wikilink">林森公園地下停車場</a>
<ul>
<li>國王大飯店（118號）</li>
<li><a href="../Page/神旺商務酒店.md" title="wikilink">神旺商務酒店</a>（128號）</li>
</ul></li>
<li>中山北路口
<ul>
<li><a href="../Page/臺北市政府警察局.md" title="wikilink">臺北市政府警察局中山分局</a>（中山北路二段1號）</li>
</ul></li>
</ul>
<ul>
<li>二段
<ul>
<li><a href="../Page/法務部行政執行署.md" title="wikilink">法務部行政執行署台北分署</a>（1號）</li>
<li>再保大樓（53號）</li>
<li><a href="../Page/第一華僑大飯店.md" title="wikilink">第一華僑大飯店</a>（63號）</li>
<li><a href="../Page/台灣肥料公司.md" title="wikilink">台灣肥料公司</a>（88號）</li>
<li>亞洲信託大樓（116號，改建中）</li>
<li><a href="../Page/松江南京站.md" title="wikilink">捷運松江南京站</a>(5號出口、6號出口)</li>
<li>新光人壽大樓(123號，<a href="../Page/新光集團.md" title="wikilink">新光集團舊總部</a>)</li>
<li><a href="../Page/文水藝文中心.md" title="wikilink">文水藝文中心</a>(124號9樓)</li>
<li><a href="../Page/聯邦企業集團.md" title="wikilink">聯邦商業大樓</a>（137號）</li>
</ul></li>
</ul>
<ul>
<li>三段
<ul>
<li><a href="https://zh.wikipedia.org/wiki/File:China_Airlines.svg" title="fig:China_Airlines.svg">China_Airlines.svg</a>華航大樓（131號，<a href="../Page/中華航空.md" title="wikilink">中華航空舊總部</a>）</li>
<li><a href="../Page/財政部台北國稅局.md" title="wikilink">財政部台北國稅局松山分局</a>（131號，華航大樓內）</li>
<li><a href="../Page/六福皇宮.md" title="wikilink">六福皇宮</a>（133號）</li>
<li><a href="../Page/南京復興站.md" title="wikilink">捷運南京復興站</a>（復興北路口，253號）</li>
</ul></li>
<li><a href="../Page/台北市公共自行車租賃系統.md" title="wikilink">台北市公共自行車租賃系統</a><a href="../Page/捷運南京復興站.md" title="wikilink">捷運南京復興站</a>(5號出口)
<ul>
<li><a href="../Page/兄弟大飯店.md" title="wikilink">兄弟大飯店</a>（255號）</li>
<li><a href="../Page/臺北市立敦化國民中學.md" title="wikilink">敦化國中</a>（300號）</li>
<li><a href="../Page/微風南京.md" title="wikilink">微風南京</a>（337號）</li>
<li><a href="https://zh.wikipedia.org/wiki/File:Republic_of_China_Military_Police_(ROCMP)_Logo.svg" title="fig:Republic_of_China_Military_Police_(ROCMP)_Logo.svg">Republic_of_China_Military_Police_(ROCMP)_Logo.svg</a><a href="../Page/中華民國憲兵.md" title="wikilink">台北憲兵隊東區分隊</a>（340號，荒廢中）</li>
</ul></li>
<li>敦化北路口
<ul>
<li>臺北金融中心大樓（敦化北路88、88之1號）</li>
</ul></li>
</ul></td>
<td><ul>
<li>四段
<ul>
<li><a href="../Page/臺北小巨蛋.md" title="wikilink">臺北小巨蛋</a>（2號）</li>
<li><a href="../Page/臺北體育館.md" title="wikilink">臺北體育館</a>（10號）</li>
<li>臺北市政府警察局松山分局（12號）及<a href="../Page/台北市政府捷運工程局.md" title="wikilink">台北市政府捷運工程局中區工程處</a>（12之1號）辦公大樓</li>
<li><a href="../Page/臺北小巨蛋站.md" title="wikilink">捷運臺北小巨蛋站</a></li>
</ul></li>
<li><a href="../Page/台北市公共自行車租賃系統.md" title="wikilink">台北市公共自行車租賃系統</a><a href="../Page/臺北小巨蛋站.md" title="wikilink">捷運台北小巨蛋站</a>(5號出口)
<ul>
<li><a href="../Page/大塊文化.md" title="wikilink">大塊文化</a>(25號11F)</li>
<li><a href="../Page/圓神出版事業機構.md" title="wikilink">圓神出版事業機構</a>(50號6F-1)</li>
</ul></li>
<li><a href="../Page/寧安街.md" title="wikilink">寧安街</a>
<ul>
<li><a href="../Page/育達高職.md" title="wikilink">育達高職</a>（寧安街12號）</li>
</ul></li>
</ul>
<ul>
<li>五段
<ul>
<li>新光前瞻金融大樓（1至3號）
<ul>
<li><a href="../Page/花旗（台灣）商業銀行.md" title="wikilink">花旗（台灣）商業銀行松山分行</a>（3號1至2樓）</li>
</ul></li>
<li>台北馥敦飯店南京館（32號）</li>
<li><a href="../Page/金寶電子.md" title="wikilink">金寶大樓</a>（99號）</li>
<li><a href="../Page/中華開發金融控股公司.md" title="wikilink">中華開發</a>（125、127號）</li>
<li><a href="../Page/南京三民站.md" title="wikilink">捷運南京三民站</a></li>
<li><a href="../Page/吉野家.md" title="wikilink">吉野家</a> 南京三民店</li>
</ul></li>
<li><a href="../Page/台北市公共自行車租賃系統.md" title="wikilink">台北市公共自行車租賃系統</a><a href="../Page/捷運南京三民站.md" title="wikilink">捷運南京三民站</a>(1號出口)</li>
</ul>
<ul>
<li>六段
<ul>
<li><a href="../Page/台北企業總部園區.md" title="wikilink">台北企業總部園區</a></li>
<li><a href="../Page/星宇航空.md" title="wikilink">星宇航空</a></li>
<li>麥帥新城-基河一期國宅</li>
</ul></li>
<li><a href="../Page/台北市公共自行車租賃系統.md" title="wikilink">台北市公共自行車租賃系統基河一期國宅站</a>
<ul>
<li>環東快速道路</li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

## 參見

  - [臺北市主要道路列表](../Page/臺北市主要道路列表.md "wikilink")
  - [台北捷運](../Page/台北捷運.md "wikilink")[松山線](../Page/松山線.md "wikilink")
  - [麥克阿瑟公路](../Page/麥克阿瑟公路.md "wikilink")、[環東大道](../Page/環東大道.md "wikilink")（南京東路六段原有路基）

## 注釋及参考文献

[Category:臺北市街道](../Category/臺北市街道.md "wikilink")

1.  麥帥公路的西端起點位置，即為臺北體育館前的南京東路四段
2.  [南京東路──台北華爾街](http://blog.udn.com/kurich/1158819).kurich 的網誌- udn部落格
3.  美援道路有拓寬有新築者，皆以40公尺為計畫寬度，共計九條：[羅斯福路](../Page/羅斯福路_\(台北市\).md "wikilink")、南京東路、[敦化北路](../Page/敦化北路.md "wikilink")、[仁愛路三段及四段](../Page/仁愛路_\(台北市\).md "wikilink")、[新生北路](../Page/新生北路.md "wikilink")、[重慶北路](../Page/重慶北路_\(台北市\).md "wikilink")、[松江路](../Page/松江路.md "wikilink")、[民權東](../Page/民權東路.md "wikilink")[西路](../Page/民權西路.md "wikilink")（[延平北路以東](../Page/延平北路.md "wikilink")）。
4.  eslitebear.[中山區散步](http://blog.pixnet.net/eslitebear/post/7245184).PIXNET
    BLOG
5.  [捷運松山線動工](http://www.gamebase.com.tw/forum/content.html?sno=73824325).綜合鐵道討論-鐵道世界討論區內頁-遊戲基地