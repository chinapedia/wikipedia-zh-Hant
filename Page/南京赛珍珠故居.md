**南京赛珍珠故居**位于[南京市](../Page/南京市.md "wikilink")[鼓楼区](../Page/鼓楼区_\(南京市\).md "wikilink")[南京大学北园内](../Page/南京大学.md "wikilink")，是美国作家[赛珍珠](../Page/赛珍珠.md "wikilink")（Pearl
S.
Buck）在南京的故居，为两层砖混结构的洋楼。[1](https://commons.wikimedia.org/wiki/File:Pearl_Buck_Former_Residence_in_Nanjing_University.jpg)

1919年起，赛珍珠和她的丈夫、农业经济学家约翰·洛辛·布克（John Lossing
Buck）及家人居住于此，直至1934年。在这里度过的十多年间，她完成了处女作《放逐》和后来获得[诺贝尔文学奖的小说](../Page/诺贝尔文学奖.md "wikilink")《[大地](../Page/大地_\(小说\).md "wikilink")》等许多作品。

1998年，美国前总统[乔治·赫伯特·沃克·布什探访了赛珍珠故居](../Page/乔治·赫伯特·沃克·布什.md "wikilink")。两年后的2000年5月，南京大学正式给赛珍珠故居挂牌，[赛珍珠国际基金会](http://www.pearl-s-buck.org)主席梅瑞狄斯·理查森女士、赛珍珠曾就读的美国[伦道夫·梅肯女子学院](https://web.archive.org/web/20071031083839/http://www.rmwc.edu/)的院长夫妇率领的代表团出席了揭幕典礼。2012年5月19日，南京大学110周年校庆之际，南京大学“赛珍珠纪念馆”正式揭牌，赛珍珠故居修缮后首次面向公众开放。\[1\]

## 参考

## 链接

  - [镇江赛珍珠故居](../Page/镇江赛珍珠故居.md "wikilink")
  - [宿州赛珍珠故居](../Page/宿州赛珍珠故居.md "wikilink")
  - [庐山赛珍珠故居](../Page/庐山赛珍珠故居.md "wikilink")

{{-}}

[宁](../Category/江苏省文物保护单位.md "wikilink")
[Category:南京文物保护单位](../Category/南京文物保护单位.md "wikilink")
[Category:南京博物馆](../Category/南京博物馆.md "wikilink")
[Category:南京名人故居](../Category/南京名人故居.md "wikilink")
[Category:南京大学鼓楼校区](../Category/南京大学鼓楼校区.md "wikilink")
[Category:南京市鼓楼区](../Category/南京市鼓楼区.md "wikilink")

1.