**南京长江三桥**为一条位于[中国](../Page/中国.md "wikilink")[江苏省](../Page/江苏省.md "wikilink")[南京市的跨](../Page/南京市.md "wikilink")[长江公路桥](../Page/长江.md "wikilink")。该桥位于[南京长江大桥上游约](../Page/南京长江大桥.md "wikilink")19公里处的[大胜关](../Page/大胜关.md "wikilink")，东距长江入海口约350公里，是[上海-成都高速公路的重要组成部分](../Page/沪蓉高速公路.md "wikilink")。大桥全长约15.6公里
，其中跨江大桥长4744米，主桥为跨径648米（在同类型大桥中排名世界第四）的双塔双索-{面}-钢塔钢箱梁[斜拉桥](../Page/斜拉桥.md "wikilink")。下横梁当以上部分的索塔采用钢结构，在国内尚属首次。南引桥长680米，北引桥长2780米。南岸接线长3.083公里，北岸接线长7.773公里。工程投资概算29.6亿元。该桥设计为6车道高速公路，桥面铺装层跟采用环氧沥青。桥下最大通航净高24米。

[Nanjing-3-bridge.svg](https://zh.wikipedia.org/wiki/File:Nanjing-3-bridge.svg "fig:Nanjing-3-bridge.svg")
{{-}}

## 历程

该桥于2003年8月29日正式动工，2005年5月22日合龙，该桥在[2005年10月通车迎接](../Page/2005年10月.md "wikilink")[十运会](../Page/十运会.md "wikilink")，比原计划提前1年。

## 参考资料

  -
## 参见

[苏](../Page/category:长江大桥.md "wikilink")

[Category:中国斜拉桥](../Category/中国斜拉桥.md "wikilink")
[Category:南京跨江桥梁](../Category/南京跨江桥梁.md "wikilink")
[Category:2005年完工橋梁](../Category/2005年完工橋梁.md "wikilink")