## [中央部属高校](../Page/中央部属高校.md "wikilink")

### [教育部直属院校](../Page/中华人民共和国教育部.md "wikilink")

  - [南京大学](../Page/南京大学.md "wikilink")
  - [东南大学](../Page/东南大学.md "wikilink")
  - [南京农业大学](../Page/南京农业大学.md "wikilink")
  - [河海大学](../Page/河海大学.md "wikilink")
  - [中国药科大学](../Page/中国药科大学.md "wikilink")

### [工业和信息化部直属院校](../Page/工业和信息化部.md "wikilink")

  - [南京航空航天大学](../Page/南京航空航天大学.md "wikilink")
  - [南京理工大学](../Page/南京理工大学.md "wikilink")

### [自然资源部](../Page/自然资源部.md "wikilink")[国家林业和草原局所属院校](../Page/国家林业和草原局.md "wikilink")

  - [南京森林警察学院](../Page/南京森林警察学院.md "wikilink")

### [国家宗教事务局所属院校](../Page/国家宗教事务局.md "wikilink")

  - [金陵协和神学院](../Page/金陵协和神学院.md "wikilink")

## [军事院校](../Page/军事院校.md "wikilink")

### [中央军委联合参谋部直属院校](../Page/中央军委联合参谋部.md "wikilink")

  - [解放军理工大学](../Page/解放军理工大学.md "wikilink")
  - [陆军指挥学院](../Page/陆军指挥学院.md "wikilink")
  - [海军指挥学院](../Page/海军指挥学院.md "wikilink")
  - [炮兵学院南京分院](../Page/炮兵学院南京分院.md "wikilink")
  - [解放军国际关系学院](../Page/解放军国际关系学院.md "wikilink")

### [中央军委政治工作部直属院校](../Page/中央军委政治工作部.md "wikilink")

  - [南京政治学院](../Page/南京政治学院.md "wikilink")

## 江苏省[直属院校](../Page/地方所属高校.md "wikilink")

  - [南京师范大学](../Page/南京师范大学.md "wikilink")
  - [南京工业大学](../Page/南京工业大学.md "wikilink")
  - [南京医科大学](../Page/南京医科大学.md "wikilink")
  - [南京邮电大学](../Page/南京邮电大学.md "wikilink")
  - [南京林业大学](../Page/南京林业大学.md "wikilink")
  - [南京财经大学](../Page/南京财经大学.md "wikilink")
  - [南京中医药大学](../Page/南京中医药大学.md "wikilink")
  - [南京信息工程大学](../Page/南京信息工程大学.md "wikilink")
  - [南京审计大学](../Page/南京审计大学.md "wikilink")
  - [南京艺术学院](../Page/南京艺术学院.md "wikilink")
  - [南京体育学院](../Page/南京体育学院.md "wikilink")
  - [江苏警官学院](../Page/江苏警官学院.md "wikilink")
  - [南京工程学院](../Page/南京工程学院.md "wikilink")
  - [南京晓庄学院](../Page/南京晓庄学院.md "wikilink")
  - [金陵科技学院](../Page/金陵科技学院.md "wikilink")

## [江苏省教育厅直属院校和](../Page/江苏省教育厅.md "wikilink")[高等职业院校](../Page/高等职业院校.md "wikilink")

  - [三江学院](../Page/三江学院.md "wikilink")
  - [江苏经贸职业技术学院](../Page/江苏经贸职业技术学院.md "wikilink")
  - [江苏联合职业技术学院](../Page/江苏联合职业技术学院.md "wikilink")
  - [江苏城市职业学院](../Page/江苏城市职业学院.md "wikilink")
  - [江苏海事职业技术学院](../Page/江苏海事职业技术学院.md "wikilink")
  - [南京高等职业技术学校](../Page/南京高等职业技术学校.md "wikilink")
  - [南京铁道职业技术学院](../Page/南京铁道职业技术学院.md "wikilink")
  - [南京特殊教育职业技术学院](../Page/南京特殊教育职业技术学院.md "wikilink")
  - [南京视觉艺术职业技术学院](../Page/南京视觉艺术职业技术学院.md "wikilink")
  - [南京机电职业技术学院](../Page/南京机电职业技术学院.md "wikilink")
  - [南京交通职业技术学院](../Page/南京交通职业技术学院.md "wikilink")
  - [南京信息职业技术学院](../Page/南京信息职业技术学院.md "wikilink")
  - [南京化工职业技术学院](../Page/南京化工职业技术学院.md "wikilink")
  - [南京工业职业技术学院](../Page/南京工业职业技术学院.md "wikilink")
  - [钟山职业技术学院](../Page/钟山职业技术学院.md "wikilink")
  - [正德职业技术学院](../Page/正德职业技术学院.md "wikilink")
  - [培尔职业技术学院](../Page/培尔职业技术学院.md "wikilink")
  - [应天职业技术学院](../Page/应天职业技术学院.md "wikilink")
  - [金肯职业技术学院](../Page/金肯职业技术学院.md "wikilink")

## [独立学院](../Page/独立学院.md "wikilink")

  - [南京大学金陵学院](../Page/南京大学金陵学院.md "wikilink")
  - [东南大学成贤学院](../Page/东南大学成贤学院.md "wikilink")
  - [南京理工大学紫金学院](../Page/南京理工大学紫金学院.md "wikilink")
  - [南京航空航天大学金城学院](../Page/南京航空航天大学金城学院.md "wikilink")
  - [南京师范大学中北学院](../Page/南京师范大学中北学院.md "wikilink")
  - [南京工业大学浦江学院](../Page/南京工业大学浦江学院.md "wikilink")
  - [南京医科大学康达学院](../Page/南京医科大学康达学院.md "wikilink")
  - [南京中医药大学翰林学院](../Page/南京中医药大学翰林学院.md "wikilink")
  - [南京信息工程大学滨江学院](../Page/南京信息工程大学滨江学院.md "wikilink")
  - [南京邮电大学通达学院](../Page/南京邮电大学通达学院.md "wikilink")
  - [南京财经大学红山学院](../Page/南京财经大学红山学院.md "wikilink")
  - [南京审计学院金审学院](../Page/南京审计学院金审学院.md "wikilink")
  - [南京工程学院康尼学院](../Page/南京工程学院康尼学院.md "wikilink")
  - [中国传媒大学南广学院](../Page/中国传媒大学南广学院.md "wikilink")

## 其它院校

  - [江苏教育学院](../Page/江苏教育学院.md "wikilink")
  - [江苏省广播电视大学](../Page/江苏省广播电视大学.md "wikilink")
  - [江苏省省级机关管理干部学院](../Page/江苏省省级机关管理干部学院.md "wikilink")
  - [江苏建康职业学院](../Page/江苏建康职业学院.md "wikilink")
  - [南京城市职业学院](../Page/南京城市职业学院.md "wikilink")
  - [南京人口管理干部学院](../Page/南京人口管理干部学院.md "wikilink")
  - [南京电子工业职工大学](../Page/南京电子工业职工大学.md "wikilink")
  - [南京联合职工大学](../Page/南京联合职工大学.md "wikilink")
  - [空军第一职工大学](../Page/空军第一职工大学.md "wikilink")

## 大学城

[\*](../Category/南京高等院校.md "wikilink")
[Category:南京列表](../Category/南京列表.md "wikilink")
[Category:江苏学校列表](../Category/江苏学校列表.md "wikilink")
[Category:中国大学列表](../Category/中国大学列表.md "wikilink")