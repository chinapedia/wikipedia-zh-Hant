**南佳也**（），[日本](../Page/日本.md "wikilink")[石川縣](../Page/石川縣.md "wikilink")[金澤市出身的](../Page/金澤市.md "wikilink")[男性色情片演員](../Page/男性色情片演員.md "wikilink")。

從小就對演藝事業充滿興趣的南佳也，從金澤市的[高中和](../Page/高中.md "wikilink")[東京的美容學校畢業之後](../Page/東京.md "wikilink")，到片場當[臨時演員](../Page/臨時演員.md "wikilink")。受到知名[色情片導演](../Page/色情片.md "wikilink")[長崎南的賞識](../Page/長崎南.md "wikilink")，有機會參加演出色情片，並且因此出名。

身高180公分的南佳也，長头发，面容俊美而且肌肉結實，是色情片界中第一次以[女異性戀者及](../Page/女異性戀者.md "wikilink")[男同性戀者族群作為主打市場的色情片男演員](../Page/男同性戀者.md "wikilink")，也因此被認為是引領女性開始欣賞色情片的關鍵人物，甚至有女人說：「只要知道有南佳也的存在，誰還會理會[木村拓哉呢](../Page/木村拓哉.md "wikilink")？」

南佳也與其他色情男演員不同，以溫柔、愛撫的方式作前奏引入[性交](../Page/性交.md "wikilink")。

至今南佳也已拍攝過上千部作品，合作過的[女演員不計其數](../Page/女演員.md "wikilink")，包括著名的[高树玛丽亚](../Page/高树玛丽亚.md "wikilink")、[美竹凉子](../Page/美竹凉子.md "wikilink")、[大石彩香](../Page/大石彩香.md "wikilink")、[苍井遥](../Page/苍井遥.md "wikilink")、[古都光](../Page/古都光.md "wikilink")、[爱田由](../Page/爱田由.md "wikilink")、[樱朱音](../Page/樱朱音.md "wikilink")、[小森美王](../Page/小森美王.md "wikilink")、[白鸟樱](../Page/白鸟樱.md "wikilink")、[北島優](../Page/北島優.md "wikilink")、[早川桃華](../Page/早川桃華.md "wikilink")、[星野流宇](../Page/星野流宇.md "wikilink")、[葵實野理](../Page/葵實野理.md "wikilink")、[上原あやか](../Page/上原あやか.md "wikilink")、[松島楓](../Page/松島楓.md "wikilink")、[神谷姬](../Page/神谷姬.md "wikilink")、[相崎琴音](../Page/相崎琴音.md "wikilink")、[伊東玲](../Page/伊東玲.md "wikilink")、[西野翔和](../Page/西野翔.md "wikilink")[花野真衣等](../Page/花野真衣.md "wikilink")。同時南佳也亦曾出演過不少[男男色情片](../Page/男男色情片.md "wikilink")。

**南佳也**有一齣突破性的色情片，是由三名女演員對被綑綁著的南佳也施暴的影片，名叫《[屋形船大宴會](../Page/屋形船大宴會.md "wikilink")》，颠覆了一般以男性为本位的南佳也的演出風格。

2004年南佳也一度在[香港](../Page/香港.md "wikilink")[無線電視的深夜節目出出現](../Page/電視廣播有限公司.md "wikilink")，並且獲邀至台灣宣傳。

## 關連項目

  - [男性色情片演員](../Page/男性色情片演員.md "wikilink")
  - [加藤鷹](../Page/加藤鷹.md "wikilink")
  - [向山裕](../Page/向山裕.md "wikilink")

## 參考資料

  -
  -
[Category:日本男性色情演員](../Category/日本男性色情演員.md "wikilink")
[Category:日本男同性戀色情片演員](../Category/日本男同性戀色情片演員.md "wikilink")
[Category:金澤市出身人物](../Category/金澤市出身人物.md "wikilink")