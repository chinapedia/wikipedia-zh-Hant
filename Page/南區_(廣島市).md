**南区**（）是[廣島市的](../Page/廣島市.md "wikilink")[行政區之一](../Page/行政區.md "wikilink")，於1980年廣島市升格為[政令指定都市的同時成立](../Page/政令指定都市.md "wikilink")。位於廣島灣的似島、金輪島、峠島都屬於南區的轄區。

## 交通

### 鐵路

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")
      - [山陽新幹線](../Page/山陽新幹線.md "wikilink")：[廣島車站](../Page/廣島車站.md "wikilink")
      - [山陽本線](../Page/山陽本線.md "wikilink")：[天神川車站](../Page/天神川車站.md "wikilink")
        - 廣島車站
      - [藝備線](../Page/藝備線.md "wikilink")：廣島車站
  - [廣島電鐵](../Page/廣島電鐵.md "wikilink")
      - [本線 (廣島電鐵)](../Page/本線_\(廣島電鐵\).md "wikilink")
      - [皆實線](../Page/皆實線.md "wikilink")
      - [宇品線](../Page/宇品線.md "wikilink")

## 外部連結