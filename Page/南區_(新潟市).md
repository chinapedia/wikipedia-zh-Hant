**南區**（）為日本[新潟縣](../Page/新潟縣.md "wikilink")[新潟市的](../Page/新潟市.md "wikilink")[行政区之一](../Page/行政区.md "wikilink")。誕生于2007年4月1日新潟市成為[政令指定都市之際](../Page/政令指定都市.md "wikilink")。范圍包括旧[白根市](../Page/白根市.md "wikilink")、旧[味方村](../Page/味方村.md "wikilink")、旧[月潟村](../Page/月潟村.md "wikilink")。区公所位于旧白根支所。面積100.54平方公里，總人口47,588人。

## 外部連結

  - [新潟市](http://www.city.niigata.jp/)
  - [新潟市 南区](http://www.city.niigata.jp/info/minami/)