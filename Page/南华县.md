**南华县**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[云南省](../Page/云南省.md "wikilink")[楚雄彝族自治州下属的一个县](../Page/楚雄彝族自治州.md "wikilink")，位于楚雄州西部，县城[龙川镇东距州府](../Page/龙川镇_\(南华县\).md "wikilink")[楚雄市](../Page/楚雄市.md "wikilink")33公里、省会[昆明市](../Page/昆明市.md "wikilink")192公里，西距[大理市](../Page/大理市.md "wikilink")175公里。东接[牟定县](../Page/牟定县.md "wikilink")，东南连楚雄市，南连[景东县](../Page/景东县.md "wikilink")，西邻[弥渡县](../Page/弥渡县.md "wikilink")，北毗[祥云县与](../Page/祥云县.md "wikilink")[姚安县](../Page/姚安县.md "wikilink")。南华县是[国家扶贫开发工作重点县之一](../Page/国家扶贫开发工作重点县.md "wikilink")，同时也是云南第二次国共内战时期的47个革命老区县之一。

## 历史

[汉代时属](../Page/汉代.md "wikilink")[益州郡地](../Page/益州郡_\(西汉\).md "wikilink")，[三国](../Page/三国.md "wikilink")[蜀汉为](../Page/蜀汉.md "wikilink")[益州](../Page/益州.md "wikilink")[云南郡地](../Page/云南郡.md "wikilink")。[西晋置为](../Page/西晋.md "wikilink")[宁州云南郡](../Page/宁州.md "wikilink")，[东晋至](../Page/东晋.md "wikilink")[南朝](../Page/南朝.md "wikilink")[梁为宁州](../Page/梁_\(南朝\).md "wikilink")[兴宁郡地](../Page/兴宁郡.md "wikilink")，后属[北周](../Page/北周.md "wikilink")[南宁州云南郡](../Page/南宁州.md "wikilink")。[隋代属南宁州总管府](../Page/隋代.md "wikilink")。[唐朝初置](../Page/唐朝.md "wikilink")[丘州](../Page/丘州.md "wikilink")，[南诏国设](../Page/南诏国.md "wikilink")[俗富郡](../Page/俗富郡.md "wikilink")，属[弄栋节度](../Page/弄栋节度.md "wikilink")。[大理国设](../Page/大理国.md "wikilink")[石鼓赕](../Page/石鼓赕.md "wikilink")，属[威楚府](../Page/威楚府.md "wikilink")。[元代](../Page/元代.md "wikilink")[宪宗七年](../Page/蒙哥.md "wikilink")（1257年）设[欠舍千户](../Page/欠舍千户.md "wikilink")，[至元十二年](../Page/至元_\(忽必烈\).md "wikilink")（1275年）改为[镇南州](../Page/镇南州.md "wikilink")，治今[沙桥镇](../Page/沙桥镇.md "wikilink")，属[威楚路](../Page/威楚路.md "wikilink")。[明代沿为镇南州](../Page/明代.md "wikilink")，迁治今龙川镇，属楚雄府。[清代沿明制](../Page/清代.md "wikilink")。

[民国](../Page/民国纪年.md "wikilink")2年（1913年）4月，镇南州改为镇南县。1950年镇南县属楚雄专区。1954年6月30日，镇南县更名为南华县，寓意为“西南美丽的地方”。1957年设楚雄彝族自治州，南华县属楚雄州。1960年撤销南华县，并归楚雄县。1962年又重新恢复南华县。\[1\]

## 自然地理

南华县境东西横距64.6公里，南北纵距71.7公里。地势呈西北高东南低，西南群山纵横，东北丘陵起伏，间有少量平坝和狭谷。县城所在地龙川镇海拔1857米，境内最高点为[红土坡镇龙潭山的烧香寺山](../Page/红土坡镇.md "wikilink")，海拔2861.1米，最低点位于[马街镇威车村倒座窑](../Page/马街镇_\(南华县\).md "wikilink")，海拔963米，海拔高差1898.1米。林业资源丰富，森林覆盖率达65.86％。境内[亚热带至中温带的气候兼有](../Page/亚热带.md "wikilink")，形成“一山分四季，谷坡两重天”的立体气候。[320国道](../Page/320国道.md "wikilink")、省道217线（南永二级公路）和[广大铁路](../Page/广大铁路.md "wikilink")、[楚大高速通过县境](../Page/楚大高速.md "wikilink")，南华历来是川、黔、滇东通往滇西、[缅甸](../Page/缅甸.md "wikilink")、[印度等国家和地区的咽喉要塞](../Page/印度.md "wikilink")，有“九府通衢”之称。\[2\]

## 区划人口

南华县目前下辖6镇4乡128个村（居）委会：[龙川镇](../Page/龙川镇_\(南华县\).md "wikilink")，[沙桥镇](../Page/沙桥镇.md "wikilink")，[五街镇](../Page/五街镇.md "wikilink")，[红土坡镇](../Page/红土坡镇.md "wikilink")，[马街镇](../Page/马街镇_\(南华县\).md "wikilink")，[兔街镇](../Page/兔街镇.md "wikilink")，[雨露白族自治乡](../Page/雨露白族自治乡.md "wikilink")，[一街乡](../Page/一街乡.md "wikilink")，[罗武庄乡](../Page/罗武庄乡.md "wikilink")，[五顶山乡](../Page/五顶山乡.md "wikilink")。

全县总人口23.7万人，4个主体民族为[汉族](../Page/汉族.md "wikilink")、[彝族](../Page/彝族.md "wikilink")、[白族](../Page/白族.md "wikilink")、[回族](../Page/回族.md "wikilink")，少数民族人口占总人口的39.1％，农业人口占89.5％，

## 经济

南华县经济以农业为主，有“中国野生菌之乡”的美誉。

## 参考资料

[南华县](../Category/南华县.md "wikilink") [县](../Category/楚雄县市.md "wikilink")
[楚雄](../Category/云南省县份.md "wikilink")
[Category:国家级贫困县](../Category/国家级贫困县.md "wikilink")

1.
2.