**南原清隆**（），生於[日本](../Page/日本.md "wikilink")[香川縣](../Page/香川縣.md "wikilink")[高松市](../Page/高松市.md "wikilink")，日本演藝人物、搞笑藝人、演員、落語師。與[內村光良組成](../Page/內村光良.md "wikilink")[小內小南](../Page/小內小南.md "wikilink")（）組合。曾與[徐若瑄](../Page/徐若瑄.md "wikilink")、[天野博之](../Page/天野博之.md "wikilink")、[丁愷蒂組成](../Page/丁愷蒂.md "wikilink")[黑色餅乾](../Page/黑色餅乾.md "wikilink")，使用的藝名是**南南見狂也**。

此外，他與[九世野村萬藏合組](../Page/九世野村萬藏.md "wikilink")[現代狂言](../Page/現代狂言.md "wikilink")，進行[狂言演出](../Page/狂言.md "wikilink")。

## 生平

1985年，與[內村光良組成](../Page/內村光良.md "wikilink")[漫才搭擋](../Page/漫才.md "wikilink")，進入演藝圈。

## 出演作品

  - [義經 (大河劇)](../Page/義經_\(大河劇\).md "wikilink")
  - [NANDA\!?](../Page/NANDA!?.md "wikilink")
  - [賣成](../Page/賣成.md "wikilink")
  - [渥美清物語](../Page/渥美清物語.md "wikilink")
  - [L change the world](../Page/死亡筆記_\(電影\).md "wikilink")

## 電影

  - [UDON](../Page/UDON.md "wikilink")
  - ナトゥ\!\!踊る忍者伝説

## 參考來源

[黑色餅乾](../Page/黑色餅乾.md "wikilink")

  - [經紀公司個人介紹頁](http://www.maseki.co.jp/talent/uchan_nanchan)

[Category:日本男性搞笑藝人](../Category/日本男性搞笑藝人.md "wikilink")
[Category:日本男演員](../Category/日本男演員.md "wikilink")
[Category:香川縣出身人物](../Category/香川縣出身人物.md "wikilink")
[Category:日本電影學院獎新人獎得主](../Category/日本電影學院獎新人獎得主.md "wikilink")
[Category:報知金酸莓獎獲獎者](../Category/報知金酸莓獎獲獎者.md "wikilink")
[Category:模仿藝人](../Category/模仿藝人.md "wikilink")