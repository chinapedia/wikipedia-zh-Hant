**南大路**為[臺灣](../Page/臺灣.md "wikilink")[新竹市東北](../Page/新竹市.md "wikilink")-西南向的市區道路，全線隸屬於[縣道117號的一部份](../Page/縣道117號.md "wikilink")。因其位於新竹市街之南，故命名為南大路。東北起於[光復路二段](../Page/光復路_\(新竹市\).md "wikilink")，西南止於[明湖路](../Page/明湖路_\(新竹市\).md "wikilink")。

## 歷史

1905年（[明治三十八年](../Page/明治.md "wikilink")）實施的[新竹市街改正計畫](../Page/市區改正.md "wikilink")，在市街的外圍興築四條大道，作為棋盤狀式市街的外圍。1921年（[大正十年](../Page/大正.md "wikilink")）築成[烏崩坎](../Page/烏崩坎.md "wikilink")（[振興里](../Page/振興里.md "wikilink")）到[東南街的路段](../Page/東南街.md "wikilink")\[1\]，1937年向北延伸到今光復路\[2\]。
[二戰後](../Page/臺灣戰後時期.md "wikilink")，南大路沿[客雅溪向南伸延築到](../Page/客雅溪.md "wikilink")[御史崎](../Page/御史崎.md "wikilink")（[光鎮里](../Page/光鎮里.md "wikilink")），其沿線也在市區擴張後成為綿密的[住宅區](../Page/住宅區.md "wikilink")。

## 沿經行政區域

  - [東區](../Page/東區_\(新竹市\).md "wikilink")

## 車道配置

  - 全線：雙向各一車道

## 沿線設施

(由東北至西南)

  - [勞工保險局新竹市辦事處](../Page/勞工保險局.md "wikilink")（42號）
  - [美廉社新竹南大二店](../Page/美廉社.md "wikilink")（273號）
  - [麥當勞新竹南大店](../Page/麥當勞.md "wikilink")（337號）
  - [凱基商業銀行南大分行](../Page/凱基商業銀行.md "wikilink")（339號）
  - [中華郵政新竹西大路郵局](../Page/中華郵政.md "wikilink")（428號）
  - [新竹市立新竹國民小學](../Page/新竹市立新竹國民小學.md "wikilink")（興學街106號）
  - [國立清華大學南大校區](../Page/國立清華大學南大校區.md "wikilink")（521號）
  - [新竹市立育賢國民中學](http://www.ysjh.hc.edu.tw/home/web/index.php)（569號）
  - [中華郵政新竹南大路郵局](../Page/中華郵政.md "wikilink")（621號）
  - [新竹市農會](http://www.hccfa.org.tw/)新興辦事處（880號）

## 参考文献

## 外部連結

[Category:新竹市街道](../Category/新竹市街道.md "wikilink")

1.  新竹州報，1921年：41號
2.  新竹州報，1937年：1155號