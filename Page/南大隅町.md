**南大隅町**（）是位於[日本](../Page/日本.md "wikilink")[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")[大隅半島南部的一個](../Page/大隅半島.md "wikilink")[城鎮](../Page/城鎮.md "wikilink")，屬[肝属郡](../Page/肝属郡.md "wikilink")。成立于2005年3月31日，由舊[根占町與](../Page/根占町.md "wikilink")[佐多町合併而成](../Page/佐多町.md "wikilink")。

[九州最南端的](../Page/九州_\(日本\).md "wikilink")[佐多岬位於轄區內](../Page/佐多岬.md "wikilink")，轄區的西側海岸也是[霧島屋久國立公園的範圍](../Page/霧島屋久國立公園.md "wikilink")。

町名是由當地町民以[公投決定](../Page/公投.md "wikilink")。

## 歷史

### 年表

  - 1889年4月1日：實施町村制，現在的轄區在當時分屬[南大隅郡小根占村和佐多村](../Page/南大隅郡.md "wikilink")。
  - 1897年4月1日：南大隅郡被併入[肝屬郡](../Page/肝屬郡.md "wikilink")。
  - 1941年1月1日：根占村改制為[根占町](../Page/根占町.md "wikilink")。
  - 1947年9月1日：小佐多村改制並改名為[佐多町](../Page/佐多町.md "wikilink")。
  - 2005年3月31日：根占町和佐多町[合併為](../Page/市町村合併.md "wikilink")**南大隅町**。\[1\]

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年-1910年</p></th>
<th><p>1910年-1930年</p></th>
<th><p>1930年-1950年</p></th>
<th><p>1950年-1970年</p></th>
<th><p>1970年-1990年</p></th>
<th><p>1990年-現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>南大隅郡<br />
小根占村</p></td>
<td><p>1896年3月29日<br />
肝屬郡小根占村</p></td>
<td><p>1941年1月1日<br />
改制並改名為根占町</p></td>
<td><p>2005年3月31日<br />
合併為南大隅町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>南大隅郡<br />
佐多村</p></td>
<td><p>1896年3月29日<br />
肝屬郡佐多村</p></td>
<td><p>1947年9月5日<br />
改制為佐多町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 觀光資源

[Satamisaki.jpg](https://zh.wikipedia.org/wiki/File:Satamisaki.jpg "fig:Satamisaki.jpg")

  - [佐多岬](../Page/佐多岬.md "wikilink")：[九州最南端](../Page/九州_\(日本\).md "wikilink")
  - [佐多舊藥園](../Page/佐多舊藥園.md "wikilink")：國指定史跡

## 教育

### 高等學校

  - [鹿兒島縣立南大隅高等學校](../Page/鹿兒島縣立南大隅高等學校.md "wikilink")

### 中學校

<table style="width:60%;">
<colgroup>
<col style="width: 30%" />
<col style="width: 30%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>南大隅町立根占中學校</li>
</ul></td>
<td><ul>
<li>南大隅町立第一佐多中學校</li>
</ul></td>
</tr>
</tbody>
</table>

### 小學校

<table style="width:60%;">
<colgroup>
<col style="width: 19%" />
<col style="width: 19%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>南大隅町立神山小學校</li>
<li>南大隅町立宮田小學校</li>
<li>南大隅町立登尾小學校</li>
<li>南大隅町立滑川小學校</li>
</ul></td>
<td><ul>
<li>南大隅町立城內小學校</li>
<li>南大隅町立佐多小學校</li>
<li>南大隅町立大泊小學校</li>
<li>南大隅町立竹之浦小學校</li>
</ul></td>
<td><ul>
<li>南大隅町立郡小學校</li>
<li>南大隅町立大中尾小學校</li>
<li>南大隅町立邊塚小學校</li>
</ul></td>
</tr>
</tbody>
</table>

## 本地出身之名人

  - [愛華みれ](../Page/愛華みれ.md "wikilink")：[女演員](../Page/女演員.md "wikilink")、前[寶塚歌劇團成員](../Page/寶塚歌劇團.md "wikilink")
  - [內薗直樹](../Page/內薗直樹.md "wikilink")：[職業棒球選手](../Page/職業棒球.md "wikilink")

## 參考資料

## 外部連結

  - [南大隅探訪](http://www.minamiosumi-tanbou.com/)

  - [南大隅町教育委員會](http://blog.canpan.info/minao/)

  - [根占町商工會](http://nejime.kashoren.or.jp/)

<!-- end list -->

1.