**南宮崎車站**（）是一位於[日本](../Page/日本.md "wikilink")[宮崎縣](../Page/宮崎縣.md "wikilink")[宮崎市東大淀](../Page/宮崎市.md "wikilink")、隸屬於[九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")（JR九州）的[鐵路車站](../Page/鐵路車站.md "wikilink")。南宮崎車站是九州島東岸的南北縱貫幹線[日豐本線與](../Page/日豐本線.md "wikilink")[地方支線](../Page/地方支線.md "wikilink")[日南線的交會車站](../Page/日南線.md "wikilink")，除此之外從隔鄰的[宮崎車站欲發往](../Page/宮崎車站.md "wikilink")[宮崎機場](../Page/宮崎機場.md "wikilink")（）的列車也必須由南宮崎這裡進入日南線之後，再由鄰站[田吉轉入](../Page/田吉車站.md "wikilink")[宮崎空港線](../Page/宮崎空港線.md "wikilink")。站內設置有兩座[島式月台共四條乘車線](../Page/島式月台.md "wikilink")，除此之外還配置了多達七條的停車用[側線](../Page/側線.md "wikilink")。

## 車站構造

[島式月台](../Page/島式月台.md "wikilink")2面4線與7條車輛留置用側線的[地面車站](../Page/地面車站.md "wikilink")，[鋼筋混凝土](../Page/鋼筋混凝土.md "wikilink")2層的[跨站式站房](../Page/跨站式站房.md "wikilink")\[1\]。1樓為[食堂](../Page/食堂.md "wikilink")、[廁所](../Page/廁所.md "wikilink")，2樓為閘口、[商店](../Page/商店.md "wikilink")，月台以跨線天橋與[升降機聯絡](../Page/升降機.md "wikilink")。

### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>方向</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1、2</strong></p></td>
<td><p>日豐本線</p></td>
<td><p>下行</p></td>
<td><p>、<a href="../Page/都城站.md" title="wikilink">都城</a>、<a href="../Page/鹿兒島中央站.md" title="wikilink">鹿兒島中央方向</a></p></td>
</tr>
<tr class="even">
<td><p>上行</p></td>
<td><p><a href="../Page/宮崎站.md" title="wikilink">宮崎</a>、、<a href="../Page/大分站.md" title="wikilink">大分方向</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>3、4</strong></p></td>
<td><p>日南線</p></td>
<td></td>
<td><p>、、<a href="../Page/志布志站.md" title="wikilink">志布志方向</a></p></td>
</tr>
<tr class="even">
<td><p>宮崎機場線</p></td>
<td></td>
<td><p>方向</p></td>
<td></td>
</tr>
</tbody>
</table>

## 相鄰車站

  - 九州旅客鐵道
    日豐本線
      - 特急「[日輪](../Page/日輪號列車.md "wikilink")」「日輪Seagaia」「」「[霧島](../Page/霧島號列車.md "wikilink")」「[海幸山幸](../Page/海幸山幸號列車.md "wikilink")」停靠站（「日輪」「日向」的一部分此站到發）
    <!-- end list -->
      -

        普通

          -
            [宮崎](../Page/宮崎站.md "wikilink")－**南宮崎**－
    日南線、宮崎機場線（宮崎機場線至田吉站為日南線）
      - 特急「日輪」「日輪Seagaia」「日向」「海幸山幸」停靠站
    <!-- end list -->
      -

        快速「日南Marine號」

          -
            宮崎－**南宮崎**－

        普通（約半數列車直通宮崎站為至）

          -
            （宮崎－）**南宮崎**－田吉

## 參考資料

[NamiMiyazaki](../Category/日本鐵路車站_Mi.md "wikilink")
[Category:宮崎縣鐵路車站](../Category/宮崎縣鐵路車站.md "wikilink")
[Category:日豐本線車站](../Category/日豐本線車站.md "wikilink")
[Category:日南線車站](../Category/日南線車站.md "wikilink")
[Category:宮崎市](../Category/宮崎市.md "wikilink")
[Category:1913年啟用的鐵路車站](../Category/1913年啟用的鐵路車站.md "wikilink")

1.