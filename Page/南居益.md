**南居益**（），[字](../Page/表字.md "wikilink")**思受**，[號](../Page/號.md "wikilink")**二太**，[陕西](../Page/陕西.md "wikilink")[渭南人](../Page/渭南.md "wikilink")，[明朝政治人物](../Page/明朝.md "wikilink")，[進士出身](../Page/進士.md "wikilink")。

## 生平

[南京禮部尚書](../Page/南京禮部尚書.md "wikilink")[南师仲之子](../Page/南师仲.md "wikilink")，[万历二十九年](../Page/万历.md "wikilink")（1601年）成辛丑科[进士](../Page/进士.md "wikilink")，授[刑部](../Page/刑部.md "wikilink")[主事](../Page/主事.md "wikilink")，后迁[广平](../Page/广平.md "wikilink")[知府](../Page/知府.md "wikilink")，擢[山西](../Page/山西.md "wikilink")[提学副使](../Page/提学.md "wikilink")、[雁门](../Page/雁门.md "wikilink")[参政](../Page/参政.md "wikilink")，历任[按察使](../Page/按察使.md "wikilink")、左右[布政使等職](../Page/布政使.md "wikilink")。[天启二年](../Page/天启_\(明朝\).md "wikilink")（1622年）任[太仆寺卿](../Page/太仆寺卿.md "wikilink")，次年擢為[右副都御史](../Page/右副都御史.md "wikilink")。荷蘭人佔據[澎湖](../Page/澎湖.md "wikilink")，[福建巡抚](../Page/福建巡抚.md "wikilink")[商周祚私下答應荷蘭舰队司令雷约兹](../Page/商周祚.md "wikilink")（Cornelis
Rijersz
Schoonhoffman），只要撤出澎湖，就允許他們在台灣貿易，但雙方一直無法取得共識。最後商周祚命令南路副總兵[張嘉策強行要求](../Page/張嘉策.md "wikilink")[荷蘭人撤返](../Page/荷蘭人.md "wikilink")，“但师行粮从，无饷则无兵。”御史[游凤翔痛批](../Page/游凤翔.md "wikilink")：“非惟船不回、城不拆，且来者日多。”最後张嘉策被朝廷革职查办，改由抗[倭名将](../Page/倭.md "wikilink")[俞大猷之子](../Page/俞大猷.md "wikilink")[俞咨皋接任](../Page/俞咨皋.md "wikilink")。天启三年二月，朝廷以南居益取代商周祚出任福建巡抚，南居益“未出都门，辄痛心切齿，毅然以必诛[红夷为己任](../Page/红夷.md "wikilink")”，集结一万名士兵与两百艘船，包括运兵船、炮船与火船。天啟四年（1624年）二月，南居益親自乘船到[金門](../Page/金門.md "wikilink")，下令渡海出擊收復澎湖。福建[總兵俞咨皋](../Page/總兵.md "wikilink")、守備[王夢熊](../Page/王夢熊.md "wikilink")，率領兵船至澎湖，登陸[白沙島](../Page/白沙島.md "wikilink")，與荷軍接戰。但[荷蘭軍隊依仗堅固的工事與戰艦頑抗](../Page/荷蘭軍隊.md "wikilink")，澎湖久攻不下。是年九月，總兵俞咨皁統率三軍與荷軍苦戰七個月，炸毀[紅毛城](../Page/紅毛城.md "wikilink")，終於收復澎湖，荷將高文律（Kobenloet）等十二人被補。
[天启五年](../Page/天启_\(明朝\).md "wikilink")（1625年）升任[工部右侍郎](../Page/工部右侍郎.md "wikilink")，[总督河道](../Page/河道总督.md "wikilink")，因被[魏忠贤](../Page/魏忠贤.md "wikilink")、[黄承昊等人劾奏而被罢官](../Page/黄承昊.md "wikilink")。[崇祯元年](../Page/崇祯.md "wikilink")(1628年)被重新起用，任[户部右侍郎](../Page/户部右侍郎.md "wikilink")，曾提议增加军饷\[1\]。工部尚书[张凤翔因军械未能齐备而下狱](../Page/张凤翔.md "wikilink")，南居益代理[工部尚书](../Page/工部尚书.md "wikilink")。不久，[兵部尚书](../Page/兵部尚书.md "wikilink")[梁廷栋彈劾](../Page/梁廷栋.md "wikilink")[郎中](../Page/郎中.md "wikilink")[王守履失职](../Page/王守履.md "wikilink")，南居益上疏说情，被[崇禎帝以徇私削籍遣还](../Page/崇禎帝.md "wikilink")。不久，又因叙守城功复职。[崇祯十六年](../Page/崇祯.md "wikilink")（1643年）[李自成攻破](../Page/李自成.md "wikilink")[渭南](../Page/渭南.md "wikilink")，招降居益，不从。次年正月，與族弟南居业绝食而死\[2\]。

## 参考文献

<div class="references-small">

<references>

</references>

</div>

## 延伸阅读

  - 《福建省志》，福建省地方誌編纂委員會 編，1992年.

{{-}}

[Category:明朝刑部主事](../Category/明朝刑部主事.md "wikilink")
[Category:明朝廣平府知府](../Category/明朝廣平府知府.md "wikilink")
[Category:明朝山西按察使](../Category/明朝山西按察使.md "wikilink")
[Category:明朝山西布政使](../Category/明朝山西布政使.md "wikilink")
[Category:明朝太僕寺卿](../Category/明朝太僕寺卿.md "wikilink")
[Category:明朝福建巡抚](../Category/明朝福建巡抚.md "wikilink")
[Category:明朝工部侍郎](../Category/明朝工部侍郎.md "wikilink")
[Category:明朝工部尚書](../Category/明朝工部尚書.md "wikilink")
[Category:渭南人](../Category/渭南人.md "wikilink")
[J居](../Category/南姓.md "wikilink")

1.  《[明季北略](../Page/明季北略.md "wikilink")》（卷5）：“三月二十八日，陕西户部侍郎南居益奏曰：[九边要害](../Page/九边.md "wikilink")，半在[关中](../Page/关中.md "wikilink")，故蒭饷之需，独倍他省。迩因宇内多事，[司农告匮](../Page/司农.md "wikilink")，[延绥](../Page/延绥.md "wikilink")[宁](../Page/寧夏.md "wikilink")[固三镇](../Page/固原.md "wikilink")，额粮缺至三十六月矣。去岁阖省荒旱，室若磬悬，野无青草，边方斗米，贵至四钱。军民交困，嚣然丧其乐生之心，穷极思乱，大盗蜂起，劫杀之变，在在告闻。适青黄不接，匮乏难支，狡寇逃丁，互相煽动，狂锋愈逞，带甲鸣锣，驮驰控弦者，千百成群，横行于[西安境内](../Page/西安.md "wikilink")。[耀州](../Page/耀州.md "wikilink")、[泾阳](../Page/泾阳.md "wikilink")、[三原](../Page/三原.md "wikilink")、[富平](../Page/富平.md "wikilink")、[淳化](../Page/淳化.md "wikilink")、[韩城](../Page/韩城.md "wikilink")、[蒲城之间](../Page/蒲城.md "wikilink")，所过放火杀人，劫财掠畜，庐舍成墟，鸡犬一空。泾、富二邑，被祸尤酷。屠掠滛污，惨不忍言，即有存者，骇鹤惊风，扶老携幼，逃窜无门。时势至此，百二河山，危若累卵，揆厥所由皆缘饥军数数鼓噪城中亡命之徒，揭竿相向，数载以来，养成燎原之势，遂至不可响迩。为今之计，欲剿贼必先稽离伍之军，欲查军必先给积逋之饷。饷如不足，则士不宿饱，马无余蒭，枵腹荷戈，即慈父不能保其子，而抚镇又安能制此汹汹骄悍之卒哉！今惟发三十万饷以给之，庶可弭脱巾之祸于旦夕。不然崤函以西。且溃散而不可收拾，关中一变，川、蜀、晋、楚，唇齿俱为摇动，天下事尚忍言哉。”
2.  [清](../Page/清.md "wikilink").[張廷玉等](../Page/張廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷264）：“十六年，李自成陷渭南，責南氏餉百六十萬。企仲年八十三矣，遇害。誘降居益及企仲子禮部主事居業，皆不從。明年正月，賊遣兵擁之去，加炮烙。二人終不屈，絕食七日而死。”