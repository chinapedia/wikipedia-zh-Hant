**南山**可以指：

## 山脉或山峰

  - [终南山](../Page/终南山.md "wikilink")，又称南山，指[秦岭山脉中段](../Page/秦岭.md "wikilink")[中国](../Page/中国.md "wikilink")[陕西省境内西起](../Page/陕西省.md "wikilink")[武功县](../Page/武功县.md "wikilink")，东到[蓝田县的部分](../Page/蓝田县.md "wikilink")。
  - [祁连山](../Page/祁连山.md "wikilink")，别称南山，中国[青藏高原北缘的一片山脉](../Page/青藏高原.md "wikilink")。
  - [衡山](../Page/衡山.md "wikilink")，位於湖南省，稱為中國的南岳，亦有稱為南山。
  - [昆仑山](../Page/昆仑山.md "wikilink")，位于中国新疆省与西藏省交界处，在汉代被称作南山。
  - [三亚南山](../Page/三亚南山.md "wikilink")，位于中国[海南省](../Page/海南省.md "wikilink")[三亚市境内的一处山脉](../Page/三亚市.md "wikilink")。
  - [深圳南山](../Page/大南山_\(深圳\).md "wikilink")，位于中国[广东省](../Page/广东省.md "wikilink")[深圳市](../Page/深圳市.md "wikilink")[南山区境内的一处山脉](../Page/南山区_\(深圳市\).md "wikilink")。
  - [镇江南山](../Page/镇江南山.md "wikilink")，位于中国[江苏省](../Page/江苏省.md "wikilink")[镇江市境内的一处山脉](../Page/镇江市.md "wikilink")。
  - [重庆南山](../Page/重庆南山.md "wikilink")，位于中国[重庆市](../Page/重庆市.md "wikilink")[南岸区境内的](../Page/南岸区.md "wikilink")[真武山山脉中几座山峰的合称](../Page/真武山.md "wikilink")。
  - [南山摩崖造像](../Page/南山摩崖造像.md "wikilink")，[世界文化遗产](../Page/世界文化遗产.md "wikilink")[大足石刻的组成部分](../Page/大足石刻.md "wikilink")，位于[重庆市](../Page/重庆市.md "wikilink")[大足区](../Page/大足区.md "wikilink")。
  - [绵阳南山](../Page/绵阳南山.md "wikilink")，位于中国[四川省](../Page/四川省.md "wikilink")[绵阳市](../Page/绵阳市.md "wikilink")[涪城区的一座山](../Page/涪城区.md "wikilink")。在其周边有南山公园，南山寺，[南山中学等以南山为名的地方](../Page/绵阳南山中学.md "wikilink")。
  - [香港的數個名為南山的山丘](../Page/香港.md "wikilink")：
      - [南山 (大嶼山)](../Page/南山_\(大嶼山\).md "wikilink")
      - [南山 (大鴉洲)](../Page/南山_\(大鴉洲\).md "wikilink")
      - [南山 (八仙嶺)](../Page/南山_\(八仙嶺\).md "wikilink")
      - [南山 (馬鞍山)](../Page/南山_\(馬鞍山\).md "wikilink")
      - [南山 (西貢)](../Page/南山_\(西貢\).md "wikilink")
  - [南山
    (首爾)](../Page/南山_\(首爾\).md "wikilink")，位於[大韓民國](../Page/大韓民國.md "wikilink")[首爾特別市的](../Page/首爾特別市.md "wikilink")[龍山區](../Page/龍山區_\(首爾\).md "wikilink")。

## 行政区

  - [南山区
    (消歧义)](../Page/南山区_\(消歧义\).md "wikilink")，多处地方有名为「南山區」的[市辖区或](../Page/市辖区.md "wikilink")[县辖区](../Page/縣轄區_\(中華人民共和國\).md "wikilink")。
  - [南山縣](../Page/南山縣.md "wikilink")，[中华民国](../Page/中华民国.md "wikilink")[广东省的一个县](../Page/广东省_\(中华民国\).md "wikilink")，治[兩英鎮](../Page/兩英鎮.md "wikilink")（今[汕頭市](../Page/汕頭市.md "wikilink")[潮南區](../Page/潮南區.md "wikilink")[兩英鎮](../Page/兩英鎮.md "wikilink")）
    (潮循道)
    國府當局為加強控制位於[潮陽](../Page/潮陽.md "wikilink")、[普寧等縣境內的大南山地區](../Page/普寧.md "wikilink")，於民國24年（1935）10月設立縣級南山管理局。37年（1948）3月核准改制為縣，但實際未及成立縣政府。
  - [南山街道](../Page/南山街道.md "wikilink")，多处地方有名为“南山街道”的行政区。
  - [南山镇](../Page/南山镇.md "wikilink")，多处地方有名为“南山镇”的行政区。
  - [南山乡](../Page/南山乡.md "wikilink")，多处地方有名为“南山乡”的行政区。
  - 南山，指沖繩島南部地區，即[島尻](../Page/島尻.md "wikilink")。

## 牧场

  - [南山牧场
    (乌鲁木齐)](../Page/南山牧场_\(乌鲁木齐\).md "wikilink")，位于中国[新疆维吾尔自治区](../Page/新疆维吾尔自治区.md "wikilink")[乌鲁木齐市境内的一个牧场](../Page/乌鲁木齐市.md "wikilink")。
  - [南山牧场
    (城步)](../Page/南山牧场_\(城步\).md "wikilink")，位于中国[湖南省](../Page/湖南省.md "wikilink")[城步苗族自治县境内的一个牧场](../Page/城步苗族自治县.md "wikilink")。

## 学校

### 中学

参见：[南山中学](../Page/南山中学.md "wikilink")

  - [绵阳南山中学](../Page/绵阳南山中学.md "wikilink")，位于中国[四川省](../Page/四川省.md "wikilink")[绵阳市的一所中学](../Page/绵阳市.md "wikilink")，在[四川省](../Page/四川省.md "wikilink")[遂宁市设有分校](../Page/遂宁市.md "wikilink")。
  - [新北市私立南山高級中學](../Page/新北市私立南山高級中學.md "wikilink")，位於[台灣](../Page/台灣.md "wikilink")[新北市](../Page/新北市.md "wikilink")[中和區的一所](../Page/中和區.md "wikilink")[完全中學](../Page/完全中學.md "wikilink")。

### 大学

  - [南山大学](../Page/南山大学.md "wikilink")，位于[日本](../Page/日本.md "wikilink")[名古屋市](../Page/名古屋市.md "wikilink")[昭和区的一所](../Page/昭和区.md "wikilink")[大学](../Page/大学.md "wikilink")。

## 房屋

  - [南山邨](../Page/南山邨.md "wikilink")，香港[九龍](../Page/九龍.md "wikilink")[石硤尾的一个](../Page/石硤尾.md "wikilink")[公共屋邨](../Page/公共屋邨.md "wikilink")。

## 壽險

  - [南山人壽](../Page/南山人壽.md "wikilink")，台灣的保險公司。

## 人名

  - [戴南山](../Page/戴南山.md "wikilink")，[清朝](../Page/清朝.md "wikilink")[文學家](../Page/文學家.md "wikilink")、[政治人物](../Page/政治人物.md "wikilink")。[安徽](../Page/安徽.md "wikilink")[桐城人](../Page/桐城.md "wikilink")，因[南山案](../Page/南山案.md "wikilink")[文字獄遭](../Page/文字獄.md "wikilink")[康熙帝](../Page/康熙帝.md "wikilink")[斬首](../Page/斬首.md "wikilink")。
  - [钟南山](../Page/钟南山.md "wikilink")，[医学家](../Page/医学家.md "wikilink")。[福建](../Page/福建.md "wikilink")[厦门人](../Page/厦门.md "wikilink")。[中国工程院院士](../Page/中国工程院.md "wikilink")，[中华医学会会长](../Page/中华医学会.md "wikilink")。

## 古国名

  - [南山国](../Page/南山国.md "wikilink")（1337年-1429年），[琉球群岛](../Page/琉球群岛.md "wikilink")[三山时代的一个王国](../Page/三山时代.md "wikilink")，后为[中山国所灭](../Page/中山国.md "wikilink")。

## 船只

  - [美国海军南山号运煤船](../Page/美国海军南山号运煤船.md "wikilink")

## 飞机

  - [晴岚攻击机的教练机版本](../Page/晴岚攻击机#M6A1-K_南山.md "wikilink")

[es:Namsan](../Page/es:Namsan.md "wikilink")
[fr:nanshan](../Page/fr:nanshan.md "wikilink")