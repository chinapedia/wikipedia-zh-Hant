**南岳大庙**
位于[南岳衡山的山脚下的南岳古镇北端](../Page/南岳衡山.md "wikilink")，是衡山最大的古庙，也是[五岳中规模最大](../Page/五岳.md "wikilink")、最完整的宫殿式[庙宇](../Page/庙宇.md "wikilink")。大庙是衡山[国家重点风景名胜区的一部分](../Page/国家重点风景名胜区.md "wikilink")。

## 历史

南岳大庙初为**司天霍王庙**，后改为**南天真君祠**。大庙的始建年代不详。最早的文字记载是在[唐朝](../Page/唐朝.md "wikilink")[开元十三年](../Page/开元.md "wikilink")（725年）修建，历经[宋](../Page/宋朝.md "wikilink")[元](../Page/元朝.md "wikilink")[明](../Page/明朝.md "wikilink")[清的六次大火和十六次大修](../Page/清朝.md "wikilink")。[同治](../Page/同治.md "wikilink")12年寺庙在雷火中被烧毁。清[光绪八年](../Page/光绪.md "wikilink")（1882年）建筑群按照[北京故宫的样式重建](../Page/北京故宫.md "wikilink")，因此也有“江南小故宫”之称，并且大部分建筑一直保存至今。在[文化大革命中](../Page/文化大革命.md "wikilink")，庙宇被视为“四旧”而遭到了破坏，碑林、匾额、塑像和经书等悉数被毁。1980年代开始大庙开始了持续的整修，损坏的建筑和神像等都已经复原。

## 建筑

[nanyuedamiao-kuixingge.jpg](https://zh.wikipedia.org/wiki/File:nanyuedamiao-kuixingge.jpg "fig:nanyuedamiao-kuixingge.jpg")总占地面积为98500多平方米。中轴线上主体建筑分九进，由南至北依次为棂星门、奎星阁、正川门、御碑亭、嘉应门、御书楼、正殿、寝宫和北后门。东西两厢分别是道教八观和佛教八寺。

[棂星门的](../Page/棂星门.md "wikilink")“棂星”意思是人才辈出，为国所用。一般来说棂星不会命以庙门。以棂星命名门的庙宇只有两个，另一个是[孔庙](../Page/孔庙.md "wikilink")。奎星阁又名盘龙亭，是[重檐歇山顶建筑](../Page/重檐歇山顶.md "wikilink")，面积139平方米。其作用是戏台。

御碑亭内有[康熙帝御制的](../Page/康熙帝.md "wikilink")《重修南岳庙碑记》碑刻279字，首句为“南岳为天南巨镇，上应北斗玉衡，亦名寿岳。”这也是南岳为“寿岳”的御定。康熙的手迹在[文化大革命中遭到损毁](../Page/文化大革命.md "wikilink")。嘉应门爲明代建築，是迎接宾客的礼仪门，皇帝和官员等来南岳祭祀时，地方官员在此迎接。门宽36.8米，深16米，高18米，为[单檐歇山七开间建筑](../Page/单檐歇山.md "wikilink")，是大庙最宽的地方。東西角門爲[硬山建築](../Page/硬山頂.md "wikilink")，部分斗栱帶有宋代風格。

[nanyuedamiao-yubeiting.jpg](https://zh.wikipedia.org/wiki/File:nanyuedamiao-yubeiting.jpg "fig:nanyuedamiao-yubeiting.jpg")
圣帝殿又名正殿，是仿造[北京故宫](../Page/北京故宫.md "wikilink")[太和殿所建造](../Page/太和殿.md "wikilink")。正殿为重檐歇山式建筑，殿基长35.3米，宽53.68米，高31.11米，是南岳古镇最高的建筑。殿内外共有七十二根石柱，象征着南岳七十二峰。正殿前的寶庫是焚化香烛祭祀祈福的香炉。正殿的十六级台阶中间造有一条汉白玉游龙，与故宫各大殿前的龙凤石雕十分相似。汉白玉石栏上刻有浮雕，内容多为中国古代的神话传说。正殿供奉的是南岳司天昭圣帝，即[祝融火神](../Page/祝融.md "wikilink")。

## 文化

南岳大庙是[佛教](../Page/佛教.md "wikilink")、[道教和](../Page/道教.md "wikilink")[儒教三教并存的寺庙](../Page/儒教.md "wikilink")。道教八观、佛教八寺和御书楼等建筑代表了三教合一的性质。大庙的香火十分旺盛，每年在重要的佛教日子都有盛大的庙会。朝拜的人们不仅来自于[湖南](../Page/湖南.md "wikilink")、[广东等附近省市](../Page/广东.md "wikilink")，也有[港](../Page/香港.md "wikilink")[澳人士](../Page/澳門.md "wikilink")、[东南亚](../Page/东南亚.md "wikilink")[华人和](../Page/华人.md "wikilink")[日本人](../Page/日本.md "wikilink")。正殿中原来设有岳神座位，历代统治者对岳神都加赐封号。如[唐初封为](../Page/唐朝.md "wikilink")“司天霍王”，唐开元年间又封为“南岳真君”，[宋代加封为](../Page/宋朝.md "wikilink")“司天昭圣帝”等。如今的“南岳圣帝”是1983年复制的，原像毁于文革期间。正殿的两旁的两座宝库，从正殿往下看，左边是供在世的人们烧香祈福用的，右边是供奉已经逝世的先人。在衡山主峰[祝融峰顶也有供奉](../Page/祝融峰.md "wikilink")[祝融火神的](../Page/祝融.md "wikilink")[祝融殿](../Page/祝融殿.md "wikilink")。

“道教东八观”。即：老铨德观、新铨德观、老三元宫、新三元宫、万寿宫、清和宫、仁寿阁、玉虚阁。南岳最早的道教活动系[东汉](../Page/东汉.md "wikilink")，据明《衡岳志》记载：“天师[张道陵](../Page/张道陵.md "wikilink")，尝自天目山游南岳，谒青玉、光天二坛，礼祝融群君祠。”晋泰始四年（268），道士陈兴明于南岳[喜阳峰兴建南岳历史上第一座道观](../Page/喜阳峰.md "wikilink")，此时应为道教传入南岳之始。晋[太康间](../Page/太康.md "wikilink")，[魏华存来南岳修道](../Page/魏华存.md "wikilink")，在此注疏道教重要经典《[黄庭经](../Page/黄庭经.md "wikilink")》、《元始大洞真经》，后开创[上清派](../Page/上清派.md "wikilink")。

1990年[中国国家邮政局发行的一套T](../Page/中国国家邮政局.md "wikilink")155《衡山》邮票，4张中的第一张为《大庙巍峨》，即以南岳大庙为景。

## 参见

  - [祝融](../Page/祝融.md "wikilink")
  - [衡山](../Page/衡山.md "wikilink")
  - [北嶽廟](../Page/北嶽廟.md "wikilink")
  - [岱廟](../Page/岱廟.md "wikilink")、[東嶽廟](../Page/東嶽廟.md "wikilink")
  - [西嶽廟](../Page/西嶽廟.md "wikilink")
  - [中嶽廟](../Page/中嶽廟.md "wikilink")

## 外部链接

  - [南岳大庙](https://web.archive.org/web/20060102202807/http://www.zjjok.com/hengyang/nyd.htm)
  - [中国南岳衡山旅游网](https://web.archive.org/web/20060513223811/http://www.nanyue.net.cn/navigate/navintro.asp?id=21)
  - [中国佛道共存奇观——南岳大庙](http://www.huaxia.com/20040819/00234202.html)

[N](../Category/衡阳宗教建筑.md "wikilink")
[N](../Category/中國傳統建築.md "wikilink")
[Category:衡阳文物保护单位](../Category/衡阳文物保护单位.md "wikilink")
[Category:南岳区](../Category/南岳区.md "wikilink")