**南宁至广州铁路**\[1\]，简称**南广铁路**，是连接中国[广西壮族自治区](../Page/广西壮族自治区.md "wikilink")[南宁市与](../Page/南宁市.md "wikilink")[广东省](../Page/广东省.md "wikilink")[广州市的](../Page/广州市.md "wikilink")[高速铁路](../Page/高速铁路.md "wikilink")，是以客运为主、客货共线的铁路干线，由[南宁铁路局管轄](../Page/南宁铁路局.md "wikilink")。工程2008年11月9日起动工，2014年4月18日广西段竣工通车，同年12月26日全线通车。由[铁道部与广西壮族自治区政府](../Page/中华人民共和国铁道部.md "wikilink")、广东省政府合资建设，总投资约410亿元。\[2\]

## 路线

铁路走向沿[珠江水道的](../Page/珠江.md "wikilink")[西江方向](../Page/西江.md "wikilink")，从[南宁市出发](../Page/南宁市.md "wikilink")，与[柳南城际铁路并行至](../Page/柳南城际铁路.md "wikilink")[宾阳县](../Page/宾阳县.md "wikilink")[黎塘镇后向东延伸](../Page/黎塘镇.md "wikilink")，经[贵港](../Page/贵港.md "wikilink")，跨[郁江](../Page/郁江.md "wikilink")，过[桂平](../Page/桂平.md "wikilink")、[平南](../Page/平南.md "wikilink")、[藤县至](../Page/藤县.md "wikilink")[梧州进入](../Page/梧州.md "wikilink")[广东](../Page/广东.md "wikilink")，经[郁南](../Page/郁南.md "wikilink")、[云浮至](../Page/云浮.md "wikilink")[肇庆](../Page/肇庆.md "wikilink")[肇庆东站后与](../Page/肇庆东站.md "wikilink")[贵广线一起平行延伸至](../Page/贵广线.md "wikilink")[广州南站](../Page/广州南站.md "wikilink")。全长576公里，其中广西境内349公里，广东境内227公里。

南广铁路线路设站23个，分别为：[南宁](../Page/南宁站.md "wikilink")、[南宁东](../Page/南宁东站.md "wikilink")、五塘、[宾阳](../Page/宾阳站.md "wikilink")、覃塘北、根竹、[贵港](../Page/贵港站.md "wikilink")、厚禄、[桂平](../Page/桂平站.md "wikilink")、[平南南](../Page/平南南站.md "wikilink")、永康、[藤县](../Page/藤县站.md "wikilink")、[梧州南](../Page/梧州南站.md "wikilink")、[郁南](../Page/郁南站.md "wikilink")、[南江口](../Page/南江口站.md "wikilink")、[云浮东](../Page/云浮东站.md "wikilink")、大湾、[肇庆东](../Page/肇庆东站.md "wikilink")、[三水南](../Page/三水南站.md "wikilink")、[佛山西](../Page/佛山西站.md "wikilink")、奇槎、三眼桥、[广州南](../Page/广州南站.md "wikilink")。其中五塘、覃塘北、根竹、厚禄、永康、大湾、奇槎和[三眼桥不办理客运业务](../Page/三眼桥.md "wikilink")。

## 主要技术标准

南广铁路按双线[电气化标准建设](../Page/电气化.md "wikilink")，设计时速200公里，预留250公里的设计时速。

  - [铁路等级](../Page/铁路等级.md "wikilink")：国铁Ⅰ级；
  - 正线数目：双线；
  - 旅客列车设计行车速度：200km/h，预留提速至250km/h的条件；
  - [最小曲线半径](../Page/最小曲线半径.md "wikilink")：3500m；
  - [限制坡度](../Page/限制坡度.md "wikilink")：南宁至贵港12‰，贵港至广州南6‰；
  - 到发线有效长度：850m，只办理客车的车站650m；
  - [牵引种类](../Page/牵引种类.md "wikilink")：电力牵引；
  - [牵引质量](../Page/牵引质量.md "wikilink")：4000t；
  - [闭塞方式](../Page/闭塞方式.md "wikilink")：自动闭塞；
  - 建筑限界：满足开行双层集装箱列车运输要求。

## 历史背景

[广西壮族自治区](../Page/广西壮族自治区.md "wikilink")、[广东省毗邻而居](../Page/广东省.md "wikilink")，两省区间的产业合作、经贸往来、人员交流由来已久，因此自古便有“共饮一江水、[两广一家亲](../Page/两广.md "wikilink")”之说。经济发展，交通现行。然而2014年底前，两广[省会与](../Page/省会.md "wikilink")[首府间仍未有一条直通的](../Page/首府.md "wikilink")[铁路线](../Page/铁路.md "wikilink")。由[广西](../Page/广西.md "wikilink")[南宁出发](../Page/南宁.md "wikilink")，必须向南绕道[贵港](../Page/贵港.md "wikilink")、[玉林](../Page/玉林.md "wikilink")，再经[茂名](../Page/茂名.md "wikilink")、[肇庆等地才能到达](../Page/肇庆.md "wikilink")[广州](../Page/广州.md "wikilink")，全程800多[公里](../Page/公里.md "wikilink")，需要耗时13个多[小时](../Page/小时.md "wikilink")。单线、绕行、旅途时间长等铁路因素，一定程度上制约了两省区间经济社会的进一步深度融合。南广铁路的开通运营，有效补充了铁路交通通道能力方面的不足，由[南宁横向穿过](../Page/南宁.md "wikilink")[贵港](../Page/贵港.md "wikilink")、[梧州](../Page/梧州.md "wikilink")、[肇庆直达](../Page/肇庆.md "wikilink")[广州](../Page/广州.md "wikilink")，570公里的运输距离较过去缩短230公里，旅途时间压缩8小时，大大节约了人们出行的时间成本，同时为两广加快区域经济一体化建设提供了一条人流、物流、资金流快速交换通道。

## 建设进程

  - 2008年11月9日动工。
  - 2009年10月10日，首座隧道贯通。
  - 2011年6月22日，南广铁路关键控制性工程——花培岭隧道提前贯通，成为南广铁路全线四大控制性工程中第一座提前贯通的隧道，是南广铁路建设的里程碑。\[3\]
  - 2012年5月22日，南广铁路松根隧道顺利贯通，松根隧道位于广东省云浮市郁南县建城镇与宝珠镇交界处，全长3123米。
  - 2014年3月4日，南廣鐵路北嶺山隧道順利貫通，它是南廣鐵路線上最后一座貫通的隧道，北嶺山隧道位於廣東省肇慶境內，全長12438米，是南廣鐵路全線最長的一座隧道。\[4\]
  - 2014年4月18日，南广铁路广西段开通。南宁到梧州南仅需2小时半。\[5\]
  - 2014年7月7日，梧州至肇庆段铺接到肇庆东站，南广铁路实现全线贯通。\[6\]
  - 2014年11月8日，南广铁路最后一段进入全线联调联试，为14年年底全线通车奠定基础。\[7\]
  - 2014年12月26日，南广铁路全线通车。\[8\]

## 现状

南广铁路自2014年12月26日全线开通运营以来，客流持续处于旺盛的状态。铁路客流统计数据显示，南广铁路全线营运仅18天，已累计运送旅客69.5万人次，动车全程客座平均利用率达到92.5%，位居全国营运中各条高铁路线前列。2015年[春运期间](../Page/春运.md "wikilink")，部分原先结伴骑[摩托車从广东返回广西的](../Page/摩托車.md "wikilink")[民工改乘南广动车组列车返乡](../Page/民工.md "wikilink")。\[9\]\[10\]

## 長遠發展

根據規劃，南廣鐵路將直接接入[東盟的](../Page/東盟.md "wikilink")[泛亞鐵路網絡](../Page/泛亚铁路_\(中国\).md "wikilink")，屆時，從[廣州坐火車出發](../Page/廣州.md "wikilink")，經[廣西可前往](../Page/廣西.md "wikilink")[老撾](../Page/老撾.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[新加坡等國家](../Page/新加坡.md "wikilink")。

南廣鐵路是《中長期鐵路網規劃》的重點項目。這條鐵路能夠在[中國西南](../Page/中國西南.md "wikilink")、[華南間形成一條最便捷](../Page/華南.md "wikilink")、最快速的鐵路運輸通道，把[北部灣經濟區與](../Page/北部灣經濟區.md "wikilink")[珠三角地區更加緊密地聯繫在一起](../Page/珠三角地區.md "wikilink")。並將形成由中國東南沿海通往東盟國家的國際運輸大通道，加強中國與東盟國家間的經貿等各領域的交流與合作。南廣鐵路通車後，旅客可直接從[廣州乘車至](../Page/廣州.md "wikilink")[越南](../Page/越南.md "wikilink")，最終組成[泛亞鐵路東線方案](../Page/泛亞鐵路.md "wikilink")，連接到[新加坡](../Page/新加坡.md "wikilink")。\[11\]

## 參考文獻

[Category:中国高速铁路线](../Category/中国高速铁路线.md "wikilink")
[Category:广西壮族自治区铁路线](../Category/广西壮族自治区铁路线.md "wikilink")
[Category:广东铁路](../Category/广东铁路.md "wikilink")
[Category:2014年啟用的鐵路線](../Category/2014年啟用的鐵路線.md "wikilink")

1.  [新建南宁至广州铁路项目建议书通过批复](http://www.ndrc.gov.cn/fzgggz/nyjt/zhdt/200803/t20080314_197592.html)
2.  [南宁至广州铁路今日开工
    投资总额410亿元](http://news.sina.com.cn/c/2008-11-09/174716618528.shtml)
3.
4.
5.
6.  [南广铁路铺轨接入肇庆东站](http://www.gx.xinhuanet.com/dtzx/wzswz/2014-07/10/c_1111554197.htm)

7.  [南广铁路最后一段进入全线联调联试](http://news.163.com/14/1109/16/AAKDGPKU00014AED.html)
8.  [贵广、南广高铁正式开通](http://news.xinhuanet.com/photo/2014-12/26/c_127337917.htm)
9.
10.
11.