[Théophile_Verbist.jpg](https://zh.wikipedia.org/wiki/File:Théophile_Verbist.jpg "fig:Théophile_Verbist.jpg")
**南怀义**（），原名**泰奧菲勒·維比斯特**，又稱**小南怀仁**，[比利时](../Page/比利时.md "wikilink")[安特衛普人](../Page/安特衛普.md "wikilink")，1862年在[布鲁塞尔郊外的](../Page/布鲁塞尔.md "wikilink")[斯格脱](../Page/斯格脱.md "wikilink")（Scheut）创办了天主教外方传教会[圣母圣心会](../Page/圣母圣心会.md "wikilink")（[拉丁语简称C](../Page/拉丁语.md "wikilink").I.C.M.）。1865年亲自率首批该会的[韩默理等](../Page/韩默理.md "wikilink")4名热切献身于传教使命的传教士到达[内蒙古](../Page/内蒙古.md "wikilink")，从[遣使会手中接管了蒙古代牧区](../Page/遣使会.md "wikilink")。他们以[长城外侧的](../Page/长城.md "wikilink")[西湾子村](../Page/西湾子村.md "wikilink")（后来发展成河北省[崇礼县城](../Page/崇礼县.md "wikilink")）为基地，开始了在中國的宣教过程。1868年2月23日，南怀义在前往老虎沟的途中感染[斑疹伤寒蒙主寵召](../Page/斑疹伤寒.md "wikilink")。

## 身后

南懷義的寵召，使聖母聖心會的傳教工作一度陷入困難，但他所帶來的其他傳教士，繼續地在內蒙古進行宣教工作，規模也逐步擴大。此后陆续在塞外开辟了7个教区：[西湾子教区](../Page/西湾子教区.md "wikilink")、[绥远教区](../Page/绥远教区.md "wikilink")、[热河教区](../Page/热河教区.md "wikilink")、[宁夏教区](../Page/宁夏教区.md "wikilink")（包括后来陕北的三边地带）、[大同教区](../Page/大同教区.md "wikilink")、[集宁教区和](../Page/集宁教区.md "wikilink")[赤峰教区](../Page/赤峰教区.md "wikilink")。[甘肃和](../Page/甘肃.md "wikilink")[新疆也曾是他们的传教区](../Page/新疆.md "wikilink")，后来让给了[圣言会](../Page/圣言会.md "wikilink")。

1955年最后一名会士离开中国时，圣母圣心会共有神职人员239人（前后共有679人来华），教徒23．5万；创办中小学校960所，孤儿院、育婴堂19所，养老院1l所，诊疗所24处。该会还创办了比利时[魯汶大學的中文系](../Page/魯汶大學.md "wikilink")，培训来华的传教土。

2000年，世界各地有1,202位该会传教士，遍布于[非洲](../Page/非洲.md "wikilink")、[亚洲](../Page/亚洲.md "wikilink")、[北美洲](../Page/北美洲.md "wikilink")、[南美洲及](../Page/南美洲.md "wikilink")[欧洲](../Page/欧洲.md "wikilink")，在28个国家和地區生活和工作。

  - 亚洲：[台湾](../Page/台湾.md "wikilink")、[蒙古](../Page/蒙古.md "wikilink")、[香港](../Page/香港.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[菲律宾](../Page/菲律宾.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[日本](../Page/日本.md "wikilink")
  - 非洲：[刚果](../Page/刚果.md "wikilink")、[喀麦隆](../Page/喀麦隆.md "wikilink")、[赞比亚](../Page/赞比亚.md "wikilink")、[塞内加尔](../Page/塞内加尔.md "wikilink")、[安哥拉](../Page/安哥拉.md "wikilink")
  - 美洲：[海地](../Page/海地.md "wikilink")、[多米尼加共和国](../Page/多米尼加共和国.md "wikilink")、[危地马拉](../Page/危地马拉.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[墨西哥](../Page/墨西哥.md "wikilink")、[美国](../Page/美国.md "wikilink")
  - 欧洲：[比利时](../Page/比利时.md "wikilink")、[荷兰](../Page/荷兰.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[法国](../Page/法国.md "wikilink")、[德国](../Page/德国.md "wikilink")

由于南怀义和许多年轻的传教士均因感染流行于蒙古地区的[斑疹伤寒去世](../Page/斑疹伤寒.md "wikilink")，也促成了针对此疾病疫苗的问世，拯救了许多年轻的生命。

[N](../Category/比利時人.md "wikilink")
[N](../Category/在华天主教传教士.md "wikilink")
[N](../Category/1823年出生.md "wikilink")
[N](../Category/1868年逝世.md "wikilink")