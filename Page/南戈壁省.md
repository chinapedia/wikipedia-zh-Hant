[Mongolia_Omnogovi_sum_map.png](https://zh.wikipedia.org/wiki/File:Mongolia_Omnogovi_sum_map.png "fig:Mongolia_Omnogovi_sum_map.png")
[Map_mn_umnugobi_aimag.png](https://zh.wikipedia.org/wiki/File:Map_mn_umnugobi_aimag.png "fig:Map_mn_umnugobi_aimag.png")

**南戈壁省**（），是[蒙古國南部的一個](../Page/蒙古國.md "wikilink")[省](../Page/省_\(蒙古國\).md "wikilink")，南接[中國](../Page/中華人民共和國.md "wikilink")[內蒙古自治區](../Page/內蒙古自治區.md "wikilink")。省會[达兰扎达嘎德](../Page/达兰扎达嘎德.md "wikilink")。南戈壁省的總面積有十六萬五千四百平方公里，多沙漠，人口僅有61,314人
(2011年統計)，地廣人稀。

該省盛產[銅](../Page/銅.md "wikilink")、[煤](../Page/煤.md "wikilink")、[金](../Page/金.md "wikilink")、[鉛等](../Page/鉛.md "wikilink")。

{{-}}

[Ömnögovi](../Category/蒙古国省份.md "wikilink")
[南戈壁省](../Category/南戈壁省.md "wikilink")