**南方丝绸之路**，古称**[蜀身毒道](../Page/蜀身毒道.md "wikilink")**，也称**交广印度两道**是一条经[交州](../Page/交州.md "wikilink")、[广州](../Page/广州.md "wikilink")，通[西域](../Page/西域.md "wikilink")、[印度](../Page/印度.md "wikilink")，远至[巴格达的中外丝绸贸易孔道道](../Page/巴格达.md "wikilink")\[1\]。其总长有大約2000公里，是中国最古老的**国际通道**之一，包括陆道和海道，主要出口商品为[丝绸](../Page/丝绸.md "wikilink")。

### 漢代

公元前126年，在外漂泊13年的[张骞回到](../Page/张骞.md "wikilink")[长安](../Page/长安.md "wikilink")，向[汉武帝汇报](../Page/汉武帝.md "wikilink")[西域的情报](../Page/西域.md "wikilink")，促使漢武帝大破[匈奴](../Page/匈奴.md "wikilink")。同时张骞还汇报说，在[汉帝国西南可能有一条途经](../Page/漢朝.md "wikilink")[身毒](../Page/身毒.md "wikilink")（今[印度](../Page/印度.md "wikilink")）的秘道，通往[大夏](../Page/大夏.md "wikilink")（今[阿富汗](../Page/阿富汗.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")）。这一消息引起了汉武帝重视，于是派出4路人马前往探索，却被藏在深山峻岭中的当地部族阻挡。其中一路幸运来到[滇池](../Page/滇池.md "wikilink")。[滇王热情款待了远方来客](../Page/滇國.md "wikilink")，并留他们一住就是十来年。其间帮助他们西行，却为[昆明人所阻](../Page/昆明人.md "wikilink")，终未能完成对身毒的探险。

這就是南方絲綢之路的由來。中国于1950年代从古滇[墓葬遗址中出土的](../Page/墓葬.md "wikilink")[文物中](../Page/文物.md "wikilink")，發現部份出土文物來自西域遠至今阿富汗的地方。由此証明南方絲綢之路當年已存在。

### 唐代

《[新唐书](../Page/新唐书.md "wikilink")·艺文志》记有地理学家[贾耽所著的](../Page/贾耽.md "wikilink")《皇华四达记》，全书已失，原七道只剩下五：安西入[西域道](../Page/西域.md "wikilink")、六：[安南通](../Page/越南.md "wikilink")[天竺道和七](../Page/天竺.md "wikilink")：[广州通海夷道](../Page/广州.md "wikilink")，详细记录了唐代由中国经[交州](../Page/交州.md "wikilink")、广州通西域、印度，远至[巴格达的通路](../Page/巴格达.md "wikilink")：

1.  安南-[交趾](../Page/交趾.md "wikilink")-[太平](../Page/太平_\(交趾\).md "wikilink")-[峰州](../Page/峰州.md "wikilink")-南田-忠城-多利州-朱贵州-丹棠州-古涌步-汤泉州-曲江-[剑南](../Page/剑南.md "wikilink")-[通海镇](../Page/通海.md "wikilink")-安宁故城-灵南城-白崖城-[蒙舍城](../Page/蒙舍.md "wikilink")-龙尾城-太和城-[永昌郡](../Page/永昌郡.md "wikilink")-[诸葛亮城](../Page/诸葛亮.md "wikilink")-悉利城-[骠国](../Page/骠国.md "wikilink")-黑山-东天竺-伽罗都河-奔那伐檀那-中天竺-[恒河](../Page/恒河.md "wikilink")-摩格陀（[摩揭陀](../Page/摩揭陀.md "wikilink")）
2.  诸葛亮城-腾充城-丽水城-龙泉水-安西城-弥诺江-[大秦](../Page/大秦.md "wikilink")[婆罗门国](../Page/婆罗门.md "wikilink")-大岭

最早研究南方丝绸之路是[法国](../Page/法国.md "wikilink")[汉学家](../Page/汉学家.md "wikilink")[伯希和](../Page/伯希和.md "wikilink")，曾著《交广印度两道考》对南方丝绸之路的陆道和海道有深入的研究。

## 相關條目

  - [丝绸之路](../Page/丝绸之路.md "wikilink")
  - [海上絲綢之路](../Page/海上絲綢之路.md "wikilink")

## 参考資料

  - 《交广印度两道考》(法)伯希和著，冯承钧译 中华书局 ISBN 7-101-03511-6

[Category:中国对外贸易史](../Category/中国对外贸易史.md "wikilink")
[Category:絲綢之路](../Category/絲綢之路.md "wikilink")

1.  《交广印度两道考》(法)伯希和著，冯承钧译 中华书局 ISBN 7-101-03511-6