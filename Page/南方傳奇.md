《**南國傳奇**》（***Southland
Tales***）是一部2007年[科幻](../Page/科幻片.md "wikilink")[劇情](../Page/戲劇.md "wikilink")[電影](../Page/電影.md "wikilink")，由[李察·凱利](../Page/李察·凱利.md "wikilink")（Richard
Kelly）編劇和執導。片名中的「南方」（Southland），是當地居民對於[南加州和](../Page/南加州.md "wikilink")[大洛杉磯](../Page/大洛杉磯區.md "wikilink")（Greater
Los
Angeles）的稱呼。電影的背景設定在近未來，主要內容在描寫[洛杉磯](../Page/洛杉磯.md "wikilink")，並評判了美國的[軍事工業複合體以及新聞綜藝娛樂化現象](../Page/軍事工業複合體.md "wikilink")。本片的演員包括[巨石強森](../Page/巨石強森.md "wikilink")（Dwayne
Johnson）、[西恩·威廉·史考特和](../Page/西恩·威廉·史考特.md "wikilink")[莎拉·邁克爾·蓋勒](../Page/莎拉·邁克爾·蓋勒.md "wikilink")，此外[米蘭達·李察森](../Page/米蘭達·李察森.md "wikilink")、[查特·歐泰瑞](../Page/查特·歐泰瑞.md "wikilink")、[克里斯多福·蘭柏特](../Page/克里斯多福·蘭柏特.md "wikilink")、[曼蒂·摩爾](../Page/曼蒂·摩爾.md "wikilink")、[賈斯汀](../Page/賈斯汀·提姆布萊克.md "wikilink")，以及電影導演[凱文·史密斯等人也特別參與演出](../Page/凱文·史密斯.md "wikilink")。電影的配樂由[魔比](../Page/魔比.md "wikilink")（Moby）負責製作。[美國電影協會](../Page/美國電影協會.md "wikilink")（MPAA）將本片定為限制級（Restricted，R），由於電影中包含的[褻瀆語言](../Page/髒話.md "wikilink")、性愛和暴力場片，以及部份[毒品使用](../Page/藥物.md "wikilink")\[1\]。

## 劇情概要

[美國](../Page/美國.md "wikilink")[德州](../Page/德州.md "wikilink")[艾爾帕索和](../Page/艾爾帕索_\(德克薩斯州\).md "wikilink")[阿比林兩地在](../Page/阿比林_\(德克薩斯州\).md "wikilink")2005年7月4日遭到[核彈攻擊](../Page/核彈.md "wikilink")，這場突如其來的大災難將美國帶往戰爭之路。[美國愛國者法案經過升級後成為一個新的部門](../Page/美國愛國者法案.md "wikilink")「US-IDENT」，甚至對網際網路進行監視和審查，而使用電腦和銀行帳戶都需要經過指紋驗證，而為了解決能源危機，一間名為「Treer」的德國公司設計出了一種裝置能夠利用海洋推進力發電，但發電機擾亂了洋流讓地球的旋轉失控，使得地球結構上出現了空間和時間的裂縫。

故事發生的地點在即將陷入混亂的洛杉磯。在一個晚上，受傷失憶的動作演員霸克斯·聖塔洛斯（Boxer
Santaros，[巨石強森飾](../Page/巨石強森.md "wikilink")）、希望再振作復出的過氣情色女星克莉絲塔·娜歐（Krysta
Now，[莎拉·邁克爾·蓋勒飾](../Page/莎拉·邁克爾·蓋勒.md "wikilink")），以及雙胞胎兄弟羅蘭和羅納杜·塔瓦納（Roland
and Ronald
Taverner，皆由[西恩·威廉·史考特飾演](../Page/西恩·威廉·史考特.md "wikilink")），他們四人掌握了全人類的命運關鍵，

## 演員與角色

  - **[巨石強森](../Page/巨石強森.md "wikilink")**（本名杜恩·強森，**Dwayne
    Johnson**）飾演**霸克斯·聖塔洛斯**（**Boxer
    Santaros**）：一個失去記憶的動作片明星，他的生活和克莉絲塔·娜歐有關\[2\]。聖塔洛斯的妻子是某個參議員的女兒\[3\]。
  - **[西恩·威廉·史考特](../Page/西恩·威廉·史考特.md "wikilink")**（**Seann William
    Scott**）飾演**羅蘭·塔瓦納**、**羅納杜·塔瓦納**（**Roland Taverner** / **Ronald
    Taverner**）
  - **[莎拉·蜜雪兒·吉蘭](../Page/莎拉·蜜雪兒·吉蘭.md "wikilink")**（**Sarah Michelle
    Gellar**）飾演**克莉絲塔·娜歐**（**Krysta Now**）
  - **[米蘭達·李察森](../Page/米蘭達·李察森.md "wikilink")**（**Miranda
    Richardson**）飾演**娜娜·梅·佛斯特**（**Nana Mae
    Frost**）：充滿野心的反派角色，是聖塔洛斯的岳母，在
    US-IDENT 工作。
  - **[賈斯汀](../Page/賈斯汀·提姆布萊克.md "wikilink")**（**Justin
    Timberlake**）飾演**阿比連恩機長**（**Pilot
    Abilene**）：受傷的伊拉克戰爭退役軍人。他也負責電影中的旁白，並在音樂方面演出\[4\]。

## 資料來源

## 外部連結

  - [官方往暫](http://www.southlandtales.com/)

  - [USIDent
    機構網站](https://web.archive.org/web/20071007113135/http://usident.org/)

  - [霸克斯·聖塔洛斯的 MySpace 網頁](http://www.myspace.com/boxersantaros)

  - [克莉絲塔·娜歐網站](http://www.krysta-now.com/)

  - [Treer Products 網站](http://www.treer-products.com/)

  - [官方 [MySpace](../Page/MySpace.md "wikilink")
    網頁](http://www.myspace.com/southlandtalesofficial)

  -
[Category:2007年電影](../Category/2007年電影.md "wikilink")
[Category:2000年代喜劇劇情片](../Category/2000年代喜劇劇情片.md "wikilink")
[Category:美國喜劇劇情片](../Category/美國喜劇劇情片.md "wikilink")
[Category:德國喜劇劇情片](../Category/德國喜劇劇情片.md "wikilink")
[Category:2000年代驚悚片](../Category/2000年代驚悚片.md "wikilink")
[Category:美國驚悚片](../Category/美國驚悚片.md "wikilink")
[Category:德國驚悚片](../Category/德國驚悚片.md "wikilink")
[Category:2000年代科幻动作片](../Category/2000年代科幻动作片.md "wikilink")
[Category:美国科幻动作片](../Category/美国科幻动作片.md "wikilink")
[Category:德国科幻动作片](../Category/德国科幻动作片.md "wikilink")
[Category:2000年代動作驚悚片](../Category/2000年代動作驚悚片.md "wikilink")
[Category:美國動作驚悚片](../Category/美國動作驚悚片.md "wikilink")
[Category:德國動作驚悚片](../Category/德國動作驚悚片.md "wikilink")
[Category:美國LGBT相關電影](../Category/美國LGBT相關電影.md "wikilink")
[Category:德國LGBT相關電影](../Category/德國LGBT相關電影.md "wikilink")
[Category:英语电影](../Category/英语电影.md "wikilink")
[Category:末日題材電影](../Category/末日題材電影.md "wikilink")
[Category:雙性戀相關電影](../Category/雙性戀相關電影.md "wikilink")
[Category:新澤西州取景電影](../Category/新澤西州取景電影.md "wikilink")
[Category:加利福尼亞州背景電影](../Category/加利福尼亞州背景電影.md "wikilink")
[Category:加利福尼亞州取景電影](../Category/加利福尼亞州取景電影.md "wikilink")
[Category:2008年背景電影](../Category/2008年背景電影.md "wikilink")
[Category:洛杉磯取景電影](../Category/洛杉磯取景電影.md "wikilink")
[Category:時間旅行電影](../Category/時間旅行電影.md "wikilink")
[Category:反烏托邦電影](../Category/反烏托邦電影.md "wikilink")
[Category:平行世界題材電影](../Category/平行世界題材電影.md "wikilink")
[Category:環球影業電影](../Category/環球影業電影.md "wikilink")

1.  <http://www.comingsoon.net/films.php?id=13204>

2.
3.
4.