**南方十字**是指以在[南半球可以看見的星座](../Page/南半球.md "wikilink")，**[南十字座](../Page/南十字座.md "wikilink")**，為圖案，放置在[國旗](../Page/國旗.md "wikilink")、[旗幟](../Page/旗幟.md "wikilink")、[徽章等](../Page/徽章.md "wikilink")，以做為[國家](../Page/國家.md "wikilink")、地方政府象徵的標誌實體。這個星座在南半球各地都很容易看見，因此位於南半球的地區喜愛使用它作為象徵。

*南方十字*也可以指在[南北戰爭中](../Page/南北戰爭.md "wikilink")，位處南方的[美利堅聯盟國上使用的各種不同的藍色](../Page/美利堅聯盟國.md "wikilink")[聖安德魯十字](../Page/聖安德魯十字.md "wikilink")，包括[美利堅聯盟國國旗和軍旗](../Page/美利堅聯盟國國旗.md "wikilink")。

這是一份不完整的清單，而且在清單中的一些標誌可能不具有官方地位。同時，也請注意，不同的[旗幟比例可能就是不同的旗幟](../Page/旗幟學用語.md "wikilink")，也可能是同一旗幟在不同時代的版本。

## 在南半球國家的國旗

## 澳大利亞大英國協的其它旗幟

<table>
<thead>
<tr class="header">
<th><p>类型</p></th>
<th><p>蓝</p></th>
<th><p>红</p></th>
<th><p>白</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/Pantone.md" title="wikilink">Pantone</a></p></td>
<td><p>280 C</p></td>
<td><p>185 C</p></td>
<td><p>Safe</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/RGB.md" title="wikilink">RGB</a><br />
(<a href="../Page/Hex_triplet.md" title="wikilink">Hex</a>)</p></td>
<td><p>0-0-139<br />
(#00008B)</p></td>
<td><p>255–0–0<br />
(#FF0000)</p></td>
<td><p>255-255-255<br />
(#FFFFFF)</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/CMYK.md" title="wikilink">CMYK</a></p></td>
<td><p>100%–80%-0%-0%</p></td>
<td><p>0%–100%-100%-0%</p></td>
<td><p>0%–0%-0%-0%</p></td>
</tr>
</tbody>
</table>

## 历代国旗

### 旗帜统览

### 澳洲历史旗帜

| 旗帜                                                                                                                                                     | 日期          | 用途         | 设计                                                                                                                                                         | 叙述                                                                                                                                                                        |
| ------------------------------------------------------------------------------------------------------------------------------------------------------ | ----------- | ---------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Flag_of_Australia_(1901–1903).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Australia_\(1901–1903\).svg "fig:Flag_of_Australia_(1901–1903).svg") | 1901年－1903年 | 澳大利亚国旗     |                                                                                                                                                            |                                                                                                                                                                           |
| [Flag_of_Australia_(1903–1908).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Australia_\(1903–1908\).svg "fig:Flag_of_Australia_(1903–1908).svg") | 1903年－1908年 | 澳大利亚国旗     |                                                                                                                                                            |                                                                                                                                                                           |
| [Flag_of_Australia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Australia.svg "fig:Flag_of_Australia.svg")                                        | 1908年至今     | **澳大利亚国旗** | 为蓝底、有一颗白七角星及五颗白南十字星、左上角为英国国旗。蓝色代表环绕澳洲的大海。白色七角星被称为「澳大利亚联邦之星」，其七个角代表澳洲的六个州与一个首都区，右边白色的五颗星则代表[南十字星座](../Page/南十字星座.md "wikilink")。国旗的左上角为英国国旗图案，表明澳洲与英国的历史关系。 | 现为[澳大利亚联邦的国旗](../Page/澳大利亚联邦.md "wikilink")。这面旗帜是在1904年的一次设计比赛中，由英国君主选出来的。最初只用於军舰旗，到1954年英国女王访问澳洲时正式成为澳洲的国旗。但直到1960年代，这面旗帜才完全取代[米字旗](../Page/英国国旗.md "wikilink")，成为国家的国旗。 |

### 地方旗帜

<File:Flag> of Queensland.svg|[昆士兰州旗](../Page/昆士兰州旗.md "wikilink")
<File:Flag> of New South
Wales.svg|[新南威尔士州旗](../Page/新南威尔士州旗.md "wikilink")
<File:Flag> of Victoria
(Australia).svg|[维多利亚州旗](../Page/维多利亚州旗.md "wikilink")
<File:Flag> of South
Australia.svg|[南澳大利亚州旗](../Page/南澳大利亚州旗.md "wikilink")
<File:Flag> of Western
Australia.svg|[西澳大利亚州旗](../Page/西澳大利亚州旗.md "wikilink")
<File:Flag> of Tasmania.svg|[塔斯马尼亚州旗](../Page/塔斯马尼亚州旗.md "wikilink")
<File:Flag> of the Australian Antarctic Territory
(unofficial).svg|[澳大利亚南极领地](../Page/澳大利亚南极领地.md "wikilink")
<File:Flag> of the Australian Capital
Territory.svg|[澳大利亚首都领地旗](../Page/澳大利亚首都领地旗.md "wikilink")
<File:Flag> of the Northern
Territory.svg|[北领地旗](../Page/北领地旗.md "wikilink")
<File:Flag> of Christmas Island.svg|[圣诞岛旗帜](../Page/圣诞岛旗帜.md "wikilink")
<File:Flag> of the Cocos (Keeling)
Islands.svg|[科科斯（基林）群岛](../Page/科科斯（基林）群岛.md "wikilink")

[Category:旗幟](../Category/旗幟.md "wikilink")