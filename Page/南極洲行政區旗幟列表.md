  - 由於南極洲無正式代表，所以南極洲旗幟為個人設計的非正式旗幟。

## 南極洲旗幟

|                                                                                                                    |                                                        |                                                                                                                                               |                                                        |
| ------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------ |
| [Flag_of_Antarctica.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Antarctica.svg "fig:Flag_of_Antarctica.svg") | [南極洲](../Page/南極洲.md "wikilink")<small>（非正式旗幟）</small> | [Flag_of_Antarctica_(Smith).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Antarctica_\(Smith\).svg "fig:Flag_of_Antarctica_(Smith).svg") | [南極洲](../Page/南極洲.md "wikilink")<small>（非正式旗幟）</small> |
|                                                                                                                    |                                                        |                                                                                                                                               |                                                        |

## 各國已凍結的南極洲領地之旗幟

根據1961年6月通過的《[南極條約](../Page/s:南极条约.md "wikilink")》，規定南極只用於和平目的:「她的主權不屬於任何一個國家，而是屬於全人類。」所以除了挪威對[布威島與](../Page/布威島.md "wikilink")[彼得一世島的主權外](../Page/彼得一世島.md "wikilink")，各國對南極洲的主權與領地均凍結。

|                                                                                                                                                                                                    |                                            |                                                                                                                                                                                                                                         |                                              |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------- |
| [Bandera_de_la_Provincia_de_Tierra_del_Fuego.svg](https://zh.wikipedia.org/wiki/File:Bandera_de_la_Provincia_de_Tierra_del_Fuego.svg "fig:Bandera_de_la_Provincia_de_Tierra_del_Fuego.svg") | [阿根廷南极属地](../Page/阿根廷南极属地.md "wikilink")（） | [Flag_of_the_Australian_Antarctic_Territory_(unofficial).svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Australian_Antarctic_Territory_\(unofficial\).svg "fig:Flag_of_the_Australian_Antarctic_Territory_(unofficial).svg") | [澳大利亞南極領地](../Page/澳大利亞南極領地.md "wikilink")（） |
| [Flag_of_the_Ross_Dependency_(unofficial).svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Ross_Dependency_\(unofficial\).svg "fig:Flag_of_the_Ross_Dependency_(unofficial).svg")          | [羅斯屬地](../Page/羅斯屬地.md "wikilink")（）       |                                                                                                                                                                                                                                         |                                              |
| [Flag_of_Magallanes,_Chile.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Magallanes,_Chile.svg "fig:Flag_of_Magallanes,_Chile.svg")                                                           | [智利南極領地](../Page/智利南極領地.md "wikilink")（）   | [Flag_of_the_French_Southern_and_Antarctic_Lands.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_French_Southern_and_Antarctic_Lands.svg "fig:Flag_of_the_French_Southern_and_Antarctic_Lands.svg")                          | [阿黛利地](../Page/阿黛利地.md "wikilink")（）         |
| [Flag_of_the_British_Antarctic_Territory.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_British_Antarctic_Territory.svg "fig:Flag_of_the_British_Antarctic_Territory.svg")               | [英屬南極領地](../Page/英屬南極領地.md "wikilink")（）   | [Flag_of_Norway.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Norway.svg "fig:Flag_of_Norway.svg")                                                                                                                                  | [毛德皇后地](../Page/毛德皇后地.md "wikilink")（）       |
| [Flag_of_Norway.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Norway.svg "fig:Flag_of_Norway.svg")                                                                                             | [布威島](../Page/布威島.md "wikilink")（）         | [Flag_of_Norway.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Norway.svg "fig:Flag_of_Norway.svg")                                                                                                                                  | [彼得一世島](../Page/彼得一世島.md "wikilink")（）       |

## 主權國家曾經宣稱擁有的南極領土之旗幟

|                                                                                                                                                  |                                      |                                                                                                                                                                                                                        |                                      |
| ------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------ |
| [Flag_of_Germany_(1935–1945).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Germany_\(1935–1945\).svg "fig:Flag_of_Germany_(1935–1945).svg") | [新士瓦本](../Page/新士瓦本.md "wikilink")（） | [Flag_of_the_Imperial_Japanese_Antarctic_Expedition.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Imperial_Japanese_Antarctic_Expedition.svg "fig:Flag_of_the_Imperial_Japanese_Antarctic_Expedition.svg") | [大和雪原](../Page/大和雪原.md "wikilink")（） |
|                                                                                                                                                  |                                      |                                                                                                                                                                                                                        |                                      |

## 條約之旗幟

|                                                                                                                                                    |                                                |
| -------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------- |
| [Flag_of_the_Antarctic_Treaty.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Antarctic_Treaty.svg "fig:Flag_of_the_Antarctic_Treaty.svg") | [南极洲与南极领地旗帜](../Page/南极洲与南极领地旗帜.md "wikilink") |
|                                                                                                                                                    |                                                |

## 相關條目

  - [旗幟列表](../Page/旗幟列表.md "wikilink")

[Category:行政區旗幟](../Category/行政區旗幟.md "wikilink")
[Category:旗幟列表](../Category/旗幟列表.md "wikilink")
[Category:南極洲](../Category/南極洲.md "wikilink")