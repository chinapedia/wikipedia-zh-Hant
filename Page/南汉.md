**南汉**（917年－971年）是[五代十国时期的地方政权之一](../Page/五代十国.md "wikilink")，位于现[广东](../Page/广东省.md "wikilink")、[广西](../Page/广西壮族自治区.md "wikilink")、[海南三省及](../Page/海南省.md "wikilink")[越南北部](../Page/越南.md "wikilink")（后失）。971年为[北宋所灭](../Page/北宋.md "wikilink")。

## 建国

[唐朝末年](../Page/唐朝.md "wikilink")，[刘谦在](../Page/刘谦_\(唐朝\).md "wikilink")[岭南的封州](../Page/岭南.md "wikilink")（今[广东](../Page/广东省.md "wikilink")[封开](../Page/封开县.md "wikilink")）任[刺史](../Page/刺史.md "wikilink")，拥兵过万，战舰百餘。刘谦死后，其长子[刘隐继承父职](../Page/刘隐.md "wikilink")。[唐](../Page/唐朝.md "wikilink")[天祐二年](../Page/天祐_\(唐朝\).md "wikilink")（905年），刘隐任清海军（[岭南东道](../Page/岭南节度使.md "wikilink")）[节度使](../Page/节度使.md "wikilink")。907年，[刘隐受](../Page/刘隐.md "wikilink")[后梁封为彭郡王](../Page/后梁.md "wikilink")，909年改封为南平王，次年又改封为南海王。刘隐死后，其弟[劉龑](../Page/劉龑.md "wikilink")（又名刘岩）袭封南海王。劉龑凭借父兄在岭南的基业，于[后梁](../Page/后梁.md "wikilink")[贞明三年](../Page/贞明_\(后梁\).md "wikilink")（917年）在[番禺](../Page/番禺_\(古代\).md "wikilink")（今[广州](../Page/广州市.md "wikilink")，号兴王府）称帝，国号「[大越](../Page/大越.md "wikilink")」。次年，劉龑以[汉朝刘氏后裔的身份改国号为](../Page/汉朝.md "wikilink")「漢」\[1\]，史称南汉，以别于[北汉](../Page/北汉.md "wikilink")\[2\]。

## 政治

刘龑迷信，他非常喜欢《[周易](../Page/易经.md "wikilink")》，[年号的改变](../Page/年号.md "wikilink")，以及名字的变动，原因都是算卦所致。南汉、[南唐曾经是友好邻邦](../Page/南唐.md "wikilink")，[潘佑](../Page/潘佑.md "wikilink")《为李后主与南汉后主书》称两国“情若弟兄，义同交契”。南漢末年政治黑暗，“作燒煮剝剔、刀山劍樹之刑，或令罪人鬥虎抵象。又賦斂煩重，人不聊生。”\[3\]，邕州“民入城者，人输一钱”\[4\]，“置[媚川都](../Page/大埔_\(香港\).md "wikilink")，定其課，令入海五百尺采珠。所居宮殿以珠、[玳瑁飾之](../Page/玳瑁.md "wikilink")。[陳延壽作諸淫巧](../Page/陳延壽.md "wikilink")，日費數萬金。”\[5\]

[大有十年](../Page/大有_\(年號\).md "wikilink")（937年），南汉的[交州发生兵变](../Page/交州.md "wikilink")，属将[矯公羨杀死了主管官员](../Page/矯公羨.md "wikilink")，割据一方，另一属将[吳權领兵攻打矯公羨](../Page/吳權.md "wikilink")，矯公羨便求救于[劉龑](../Page/劉龑.md "wikilink")，刘龑封儿子[刘弘操为交王](../Page/刘弘操.md "wikilink")，然后领兵进攻吴权，结果被吴权打败，刘弘操阵亡。吴权从此占有了交州，吴权建立的王朝即[越南](../Page/越南.md "wikilink")[吳朝](../Page/吳朝.md "wikilink")，越南由此正式从中国独立出去。

## 经济

南汉是一个商业气息浓郁的国家，“岭北行商至国都，必召示之夸其富”\[6\]，“每见北人，盛夸岭海之强”\[7\]。[马端临谓](../Page/马端临.md "wikilink")：“宋兴，而吴、蜀、江南、荆湖、南粤，皆号富强”。钱多为主，钱少为奴视为一般的准则。

## 文化

[劉鋹时](../Page/劉鋹.md "wikilink")，日与[波斯女等大宫中游宴](../Page/波斯.md "wikilink")，“无名之费，日有千万”，其官制则有特殊的规定，[科举被录取者](../Page/科举.md "wikilink")，若要做官必须先[净身](../Page/閹割.md "wikilink")，也就是[閹割](../Page/閹割.md "wikilink")。在劉鋹看来，百官们有家有室，有妻儿老小，肯定不能对皇上尽忠。

## 君主

### 君主列表

### 君主世系图

## 相關條目

  - [靜海軍節度使](../Page/靜海軍節度使.md "wikilink")（[交趾政權](../Page/交趾政權.md "wikilink")）

## 參考資料

{{-}}

[SH](../Category/已不存在的亞洲君主國.md "wikilink")
[南汉](../Category/南汉.md "wikilink")
[SH](../Category/五代十国.md "wikilink")
[Category:中国历史政权](../Category/中国历史政权.md "wikilink")
[Category:廣東歷史政權](../Category/廣東歷史政權.md "wikilink")
[Category:廣州政治史](../Category/廣州政治史.md "wikilink")
[Category:越南歷史政權](../Category/越南歷史政權.md "wikilink")
[Category:歷史上的王國](../Category/歷史上的王國.md "wikilink")

1.  《资治通鉴·卷二百七十·后梁纪五》
2.
3.  《東都事略》卷二三
4.  《南汉书》
5.  《宋史》卷481
6.  《南汉书·卷三·本纪·第三高祖纪二》
7.  陶谷《清异录》谓：“南汉地狭力贫，不自揣度，有欺四方傲中国之志。每见北人，盛夸岭海之强。世宗遣使入岭，馆接者遗茉莉，文其名曰‘小南强’”