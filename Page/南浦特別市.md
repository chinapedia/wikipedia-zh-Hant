[缩略图](https://zh.wikipedia.org/wiki/File:Chongsan-ri_Farm,_North_Korea..jpg "fig:缩略图")
**南浦特別市**（）是[朝鮮的一个](../Page/朝鮮.md "wikilink")[特別市](../Page/特別市.md "wikilink")，是平壤重要的貿易港口、工業都市。人口366,815（2008年人口普查）。

## 地理環境

**南浦**是一個港灣都市，距離[大同江河口約](../Page/大同江.md "wikilink")39[公里](../Page/公里.md "wikilink")。北面為[平壤直轄市](../Page/平壤直轄市.md "wikilink")，南面為[黃海北道](../Page/黃海北道.md "wikilink")。
[125.39255E_38.73470N_Port_of_Nampho.png](https://zh.wikipedia.org/wiki/File:125.39255E_38.73470N_Port_of_Nampho.png "fig:125.39255E_38.73470N_Port_of_Nampho.png")

## 歷史

**南浦**在[朝鮮王朝時代只是一個小漁村](../Page/朝鮮王朝.md "wikilink")，1894年的[甲午戰爭成為](../Page/甲午戰爭.md "wikilink")[日本軍的後勤基地](../Page/日本.md "wikilink")。1908年至1945年改名為鎮南浦、歸屬[平安南道](../Page/平安南道.md "wikilink")。

## 工業

從殖民地時代開始，南浦市已經是朝鮮北部最大的貿易港口，貿易總額僅次於[釜山](../Page/釜山.md "wikilink")、[仁川](../Page/仁川.md "wikilink")，居全國第三位。各種冶煉廠、玻璃廠、造船廠、化學工業等工業相繼發展起來，而[漁業亦十分興旺](../Page/漁業.md "wikilink")。現在南浦市是全國[玻璃](../Page/玻璃.md "wikilink")、[有色金屬的冶煉中心](../Page/有色金屬.md "wikilink")。

和[韓國的合資的企業](../Page/韓國.md "wikilink")「[平和自動車](../Page/平和自動車.md "wikilink")」有[汽車生產](../Page/汽車.md "wikilink")。

## 交通

作為[平壤市的海上門戶](../Page/平壤市.md "wikilink")，南浦市的[海運](../Page/海運.md "wikilink")、[河運](../Page/河運.md "wikilink")、[鐵路](../Page/鐵路.md "wikilink")、[公路十分發達](../Page/公路.md "wikilink")，是朝鲜的對外貿易中心。

### 鐵路

  - [平南線](../Page/平南線.md "wikilink")(平壤―南浦)
  - [平安線](../Page/平安線.md "wikilink")(平壤―溫泉)

### 高速公路

南浦市有[高速公路和平壤連接](../Page/青年英雄公路.md "wikilink")。

## 高等院校

  - [南浦大學](../Page/南浦大學.md "wikilink")
  - [西海大學](../Page/西海大學.md "wikilink")
  - [中央體育學院](../Page/中央體育學院.md "wikilink")

## 遺跡

  - [新石器時代的](../Page/新石器時代.md "wikilink")[巨石墓](../Page/巨石墓.md "wikilink")
  - [高句麗時代的](../Page/高句麗.md "wikilink")[藥水里壁畫古墓](../Page/藥水里壁畫古墓.md "wikilink")
    ([聯合國教科文組織宣佈為](../Page/聯合國教科文組織.md "wikilink")[世界遺產](../Page/世界遺產.md "wikilink"))。

## 關於南浦直轄市

  - 1980年，[平安南道的南浦市](../Page/平安南道.md "wikilink")、大安市、龍岡郡（當時龍岡郡的全境）合併為獨立於平安南道的南浦直轄市。
  - 1991年，浦市面積大約為1000[平方公里](../Page/平方公里.md "wikilink")，人口約807000。

由於朝鮮於2004年1月9日重編國家行政區劃，新增了「[特級市](../Page/特級市.md "wikilink")」（隸屬於道），南浦市改制为特级市。2010年，南浦市升格为特別市建制，并将江西、大安、温泉、龙岗、千里马五郡劃入。合计1市5郡总人口为98万3660（2008年人口普查值）。

## 照片

<File:NK-Hafen> Nampo.jpg|西海水閘 <file:Chinnampo> Lighthouse.JPG|灯塔
摄于[朝鲜日據时期](../Page/朝鲜日據时期.md "wikilink") <File:Hero> Youth
Highway in DPRK.jpg|青年英雄公路[平壤到南浦](../Page/平壤.md "wikilink")
[File:Nampocity.jpg|市區](File:Nampocity.jpg%7C市區) <File:Nampo> west sea
barrage.jpg|金日成紀念堂

## 参见

  - [朝鮮八道](../Page/朝鮮八道.md "wikilink")
  - [平安道](../Page/平安道.md "wikilink")
  - [二十三府制](../Page/二十三府制.md "wikilink")

{{-}}

[南浦特別市](../Category/南浦特別市.md "wikilink")
[Category:朝鲜民主主义人民共和国城市](../Category/朝鲜民主主义人民共和国城市.md "wikilink")
[Category:黃海沿海城市](../Category/黃海沿海城市.md "wikilink")
[Category:朝鲜一级行政区](../Category/朝鲜一级行政区.md "wikilink")