**南浦火車站**位于[上海市](../Page/上海市.md "wikilink")[徐汇区](../Page/徐汇区.md "wikilink")[龍華地區](../Page/龙华街道_\(上海市\).md "wikilink")[兆丰路](../Page/兆丰路.md "wikilink")200號，是一座已被拆除的綜合[一等站](../Page/一等站_\(中鐵\).md "wikilink")，車站同時負責管理[松江火車站等客運站](../Page/松江站_\(上海市\).md "wikilink")\[1\]及部分貨運站。

## 历史

南浦火車站始建于[清](../Page/清.md "wikilink")[光緒三十三年](../Page/光緒.md "wikilink")（1907年），當時稱爲**日暉港貨棧**。日晖港货栈是当时上海地区唯一自备专用[码头的](../Page/码头.md "wikilink")[铁路车站](../Page/铁路车站.md "wikilink")，因此主要負責[黃浦江上貨物的裝卸](../Page/黃浦江.md "wikilink")，并和[上海南站分別負擔著](../Page/上海南站_\(1937年以前\).md "wikilink")[滬杭鐵路的貨運和客運業務](../Page/滬杭鐵路.md "wikilink")。1908年4月20日，滬杭鐵路首列上海南站至[松江站的客车开行](../Page/松江站_\(上海市\).md "wikilink")，上海南站和日晖港站正式同時開通运营。[民國](../Page/中華民國.md "wikilink")26年，[八一三事變爆發](../Page/八一三事變.md "wikilink")，[上海南站被炸毀](../Page/上海南站_\(1937年以前\).md "wikilink")，**日暉港貨棧**接收貨運業務和小部分客運業務，改稱**日暉港站**。1958年更名為**上海南站**，1978年起因[上海火車站](../Page/上海站.md "wikilink")（新客站）的建設，上海南站又接收了[原上海東站的水陸聯運等貨運業務](../Page/上海站.md "wikilink")。
\[2\]1983年，上海南站的浦江码头进行了大规模改建，令码头能够停泊[排水量达](../Page/排水量.md "wikilink")2000吨级的[货轮](../Page/货轮.md "wikilink")。

2000年1月，隨著[新上海南站啟用](../Page/上海南站.md "wikilink")，原**上海南站**更名為**南浦站**。\[3\]\[4\]

由於**南浦站**站址位于[2010年上海世界博覽會規劃展區範圍内](../Page/2010年上海世界博覽會.md "wikilink")，因此車站已經在2009年6月28日关闭，其货场设备及直属站建制迁至[闵行站](../Page/闵行站.md "wikilink")\[5\]。

## 参见

  - [徐汇滨江地区](../Page/徐汇滨江地区.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

{{-}}

[Category:上海铁路废站](../Category/上海铁路废站.md "wikilink")
[废](../Category/徐汇区铁路车站.md "wikilink")
[Category:1907年啟用的鐵路車站](../Category/1907年啟用的鐵路車站.md "wikilink")
[Category:2009年關閉的鐵路車站](../Category/2009年關閉的鐵路車站.md "wikilink")

1.  [大雪无情人有情
    铁路南浦站妥善安置滞留旅客](http://sh.eastday.com/qtmt/20080202/u1a398661.html)
2.  [上海鐵路志](http://www.shtong.gov.cn/newsite/node2/node2245/node68716/node68721/node68760/node68832/userobject1ai66561.html)

3.  [南浦站：为世博会谢幕](http://www.tieliu.com.cn/zhishi/2008/200809/2008-09-08/20080908130347_156505.html)
4.
5.