**南灣湖人**（）[美國](../Page/美國.md "wikilink")[NBA发展联盟的参赛球隊](../Page/NBA发展联盟.md "wikilink")，隶属于[洛杉矶湖人队](../Page/洛杉矶湖人队.md "wikilink")，湖人队也成为NBA历史上第一支單獨擁有一支NBDL小联盟球队的NBA球队。球队最初的主場球館是与[洛杉矶湖人队相同的](../Page/洛杉矶湖人队.md "wikilink")[斯台普斯中心](../Page/斯台普斯中心.md "wikilink")，直至2007年迁往新落成的在2007年。

2010-11赛季，球队曾退出NBDL联盟。

## 历届成绩

<table>
<thead>
<tr class="header">
<th><p>赛季</p></th>
<th><p>分区</p></th>
<th><p>排名</p></th>
<th><p>胜</p></th>
<th><p>负</p></th>
<th><p>胜率</p></th>
<th><p>季后赛成绩</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><font color="gold"> <strong>南灣湖人</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006–07</p></td>
<td><p>西区</p></td>
<td><p>第5</p></td>
<td><p>23</p></td>
<td><p>27</p></td>
<td><p>.460</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007–08</p></td>
<td><p>西区</p></td>
<td><p>第2</p></td>
<td><p>32</p></td>
<td><p>18</p></td>
<td><p>.640</p></td>
<td><p>第一轮 淘汰(<a href="../Page/科罗拉多14人队.md" title="wikilink">科罗拉多</a>) 102-95<br />
半决赛 输(<a href="../Page/爱达荷奔腾队.md" title="wikilink">爱达荷</a>) 97-90</p></td>
</tr>
<tr class="even">
<td><p>2008–09</p></td>
<td><p>西区</p></td>
<td><p>第5</p></td>
<td><p>19</p></td>
<td><p>31</p></td>
<td><p>.380</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009–10</p></td>
<td><p>西区</p></td>
<td><p>第9</p></td>
<td><p>16</p></td>
<td><p>34</p></td>
<td><p>.320</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>常规赛</p></td>
<td><p>90</p></td>
<td><p>110</p></td>
<td><p>.450</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>季后赛</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>.500</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  - [官方網站](http://www.nba.com/dleague/losangeles/)

[Los Angeles D-Fenders](../Category/NBA發展聯盟球隊.md "wikilink")
[Category:2006年所創體育俱樂部](../Category/2006年所創體育俱樂部.md "wikilink")
[Category:2006年加利福尼亞州建立](../Category/2006年加利福尼亞州建立.md "wikilink")