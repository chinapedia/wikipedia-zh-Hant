[Sør-Trøndelag_kart.png](https://zh.wikipedia.org/wiki/File:Sør-Trøndelag_kart.png "fig:Sør-Trøndelag_kart.png")
**南特伦德拉格**（
）是[挪威中部的一个](../Page/挪威.md "wikilink")[郡](../Page/郡.md "wikilink")，首府及最大城市为[特隆赫姆](../Page/特隆赫姆.md "wikilink")。南特伦德拉格郡面积18,848[平方公里](../Page/平方公里.md "wikilink")，人口278,836（2007年），是挪威人口第5大郡。

## 自治市

南特伦德拉格郡有25个自治市，如下：

<table>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Sor-Trondelag_Municipalities.png" title="fig:Sor-Trondelag_Municipalities.png">Sor-Trondelag_Municipalities.png</a></p></td>
<td><ol>
<li><a href="../Page/奥菲尤尔.md" title="wikilink">奥菲尤尔</a>（）</li>
<li><a href="../Page/阿格德內斯.md" title="wikilink">阿格德內斯</a>（）</li>
<li><a href="../Page/比于恩.md" title="wikilink">比于恩</a>（）</li>
<li><a href="../Page/弗爾島_(南特倫德拉格郡).md" title="wikilink">弗爾島</a>（）</li>
<li><a href="../Page/海姆內.md" title="wikilink">海姆內</a>（）</li>
<li><a href="../Page/希特拉島_(自治區).md" title="wikilink">-{希特拉}-島</a>（）</li>
<li><a href="../Page/霍爾托倫.md" title="wikilink">霍爾托倫</a>（）</li>
<li><a href="../Page/克萊比.md" title="wikilink">克萊比</a>（）</li>
<li><a href="../Page/馬爾維克.md" title="wikilink">馬爾維克</a>（）</li>
<li><a href="../Page/梅爾達爾.md" title="wikilink">梅爾達爾</a>（）</li>
<li><a href="../Page/梅尔许斯.md" title="wikilink">梅尔许斯</a>（）</li>
<li><a href="../Page/中蓋于爾達爾.md" title="wikilink">中蓋于爾達爾</a>（）</li>
<li><a href="../Page/奥普达尔.md" title="wikilink">奥普达尔</a>（）</li>
<li><a href="../Page/奧克達爾.md" title="wikilink">奧克達爾</a>（）</li>
<li><a href="../Page/歐蘭.md" title="wikilink">歐蘭</a>（）</li>
<li><a href="../Page/乌森.md" title="wikilink">乌森</a>（）</li>
<li><a href="../Page/倫內比.md" title="wikilink">倫內比</a>（）</li>
<li><a href="../Page/里薩_(挪威).md" title="wikilink">里薩</a>（）</li>
<li><a href="../Page/魯安.md" title="wikilink">魯安</a>（）</li>
<li><a href="../Page/勒罗斯.md" title="wikilink">勒罗斯</a>（）</li>
<li><a href="../Page/塞爾比.md" title="wikilink">塞爾比</a>（）</li>
<li><a href="../Page/斯凱于恩.md" title="wikilink">斯凱于恩</a>（）</li>
<li><a href="../Page/斯尼爾菲尤爾.md" title="wikilink">斯尼爾菲尤爾</a>（）</li>
<li><a href="../Page/特隆赫姆.md" title="wikilink">特隆赫姆</a>（）</li>
<li><a href="../Page/蒂達爾.md" title="wikilink">蒂達爾</a>（）</li>
</ol></td>
</tr>
</tbody>
</table>

## 外部链接

  - [南特伦德拉格郡官方网站](https://web.archive.org/web/20170603213159/https://www.stfk.no/)

[\*](../Category/南特伦德拉格郡.md "wikilink")