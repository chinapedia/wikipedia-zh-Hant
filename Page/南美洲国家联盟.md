**南美洲国家联盟**（；；；）是根据《[库斯科宣言](../Page/库斯科宣言.md "wikilink")》于2004年12月8日成立的主权国家联盟，目前共有成员国12个，观察员国2个。联盟原名**南美洲国家共同体**，2007年4月16日改为现名\[1\]。

联盟将合并現有的两个自由贸易组织[南方共同市场和](../Page/南方共同市场.md "wikilink")[安第斯山国家共同体](../Page/安第斯山国家共同体.md "wikilink")，建立起一个涵蓋[南美洲的](../Page/南美洲.md "wikilink")[自由贸易区](../Page/自由贸易区.md "wikilink")，分别于2014年和2019年前取消非敏感商品和敏感商品的关税。联盟总部将设于[厄瓜多首都](../Page/厄瓜多.md "wikilink")[基多](../Page/基多.md "wikilink")、議會設於[玻利維亞的](../Page/玻利維亞.md "wikilink")[科恰班巴](../Page/科恰班巴.md "wikilink")，而南美洲银行将设于[委內瑞拉首都](../Page/委內瑞拉.md "wikilink")[加拉加斯](../Page/加拉加斯.md "wikilink")。

## 概述

[Unasul_12.jpg](https://zh.wikipedia.org/wiki/File:Unasul_12.jpg "fig:Unasul_12.jpg")
在2004年12月8日举行的[第三次南美洲首脑峰会上](../Page/2004年南美洲首脑峰会.md "wikilink")，来自南美12个国家的与会总统或代表签署了《[库斯科宣言](../Page/库斯科宣言.md "wikilink")》，宣布成立南美洲国家共同体。[巴拿马作为观察员参加了签字仪式](../Page/巴拿马.md "wikilink")。

会议宣布将效仿[欧盟](../Page/欧盟.md "wikilink")，建立起一个包括共同货币、议会和护照的新共同体。据安第斯国家共同体前秘书长[阿兰·瓦格纳·蒂松描述](../Page/阿兰·瓦格纳·蒂松.md "wikilink")，一个类似于欧盟的完整联盟可能将于2019年形成。

## 起源

[西蒙·玻利瓦尔在](../Page/西蒙·玻利瓦尔.md "wikilink")19世纪早期直接领导了[委内瑞拉](../Page/委内瑞拉.md "wikilink")、[哥伦比亚](../Page/哥伦比亚.md "wikilink")、[厄瓜多尔](../Page/厄瓜多尔.md "wikilink")、[秘鲁和](../Page/秘鲁.md "wikilink")[玻利维亚的民族独立运动](../Page/玻利维亚.md "wikilink")，他的雕像几乎在每一个[拉丁美洲国家的首都都能看到](../Page/拉丁美洲.md "wikilink")。玻利瓦尔计划在这些独立后的国家基础上建立起一个联邦国家以保证繁荣和安全，但直到死也未能实现目标。

## 成员国

南美洲国家联盟共有12个成员国：
[Flag_of_the_Union_of_South_American_Nations.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Union_of_South_American_Nations.svg "fig:Flag_of_the_Union_of_South_American_Nations.svg")

  - [安第斯山国家共同体成员国](../Page/安第斯山国家共同体.md "wikilink")：
      -
      -
      -
      - \-{zh-cn:[南美洲国家联盟.svg](https://zh.wikipedia.org/wiki/File:南美洲国家联盟.svg "fig:南美洲国家联盟.svg");
        zh-tw:[Union_of_South_American_Nations_zh.svg](https://zh.wikipedia.org/wiki/File:Union_of_South_American_Nations_zh.svg "fig:Union_of_South_American_Nations_zh.svg")}-
  - [南方共同市场成员国](../Page/南方共同市场.md "wikilink")：
      -
      -
      -
      -
      -
  - 其它成员国：
      -
      -
      -
  - 观察员国：
      -
      -
以下是未参加联盟的南美洲国家和地区：

  -
## 参考文献

## 外部链接

  - [SACN information page on Andean Community
    website](https://web.archive.org/web/20120630173459/http://www.comunidadandina.org/ingles/sudamerican.htm)
  - [BBC - S America Creates Single
    Market](http://news.bbc.co.uk/2/hi/business/4079505.stm)
  - [Do the Mercosur Countries Form an Optimum Currency
    Area?](http://mitglied.lycos.de/mercosur/)
  - [MercoPress - S. American Community Comes to Light
    December 9](http://www.falkland-malvinas.com/Detalle.asp?NUM=4589)
  - [Regional rivalries are obstacle to S. American 12-nation bloc:
    analysts](https://web.archive.org/web/20050428151339/http://www.eubusiness.com/afp/041210002837.bbw02djv/)
    (EU business)
  - [News of
    CSN](http://www.notisur.com/nts/index.php?s=CSN&searchbutton=IR)
  - [BBC News (2006-12-10): Lat-Am leaders end second
    summit](http://news.bbc.co.uk/1/hi/world/americas/6166047.stm)
  - [AP: South American Leaders Dream of Integration, Continental
    Parliament (2006
    Dec 09)](http://www.iht.com/articles/ap/2006/12/10/america/LA_GEN_Bolivia_Summit.php)

## 参见

  - [里约集团](../Page/里约集团.md "wikilink")

  - [加勒比共同體](../Page/加勒比共同體.md "wikilink")

  - [拉美及加勒比國家共同體](../Page/拉美及加勒比國家共同體.md "wikilink")

  - [美洲玻利瓦爾同盟](../Page/美洲玻利瓦爾同盟.md "wikilink")

  - [南美洲](../Page/南美洲.md "wikilink")

  - [拉丁美洲](../Page/拉丁美洲.md "wikilink")

  - [南美洲经济](../Page/南美洲经济.md "wikilink")

  - [美洲自由贸易区](../Page/美洲自由贸易区.md "wikilink")

  -   - [非洲联盟](../Page/非洲联盟.md "wikilink")
      - [欧洲联盟](../Page/欧洲联盟.md "wikilink")
      - [北美聯盟](../Page/北美聯盟.md "wikilink")

  - [貿易集團](../Page/貿易集團.md "wikilink")

{{-}}

[Category:南美洲国家政府间组织](../Category/南美洲国家政府间组织.md "wikilink")
[南美洲国家联盟](../Category/南美洲国家联盟.md "wikilink")
[Category:區域主義 (國際關係)](../Category/區域主義_\(國際關係\).md "wikilink")
[Category:2008年建立的組織](../Category/2008年建立的組織.md "wikilink")

1.  ["Chávez: Presidentes acordaron llamar Unasur a integración política
    regional"](http://www.latercera.cl/medio/articulo/0,0,3255_5702_263339850,00.html)
     *[La Tercera](../Page/La_Tercera.md "wikilink")*, accesed on [April
    16](../Page/April_16.md "wikilink") 2007