**南茅部町**（）是過去位於[北海道](../Page/北海道.md "wikilink")[渡島支廳東南部的一個以](../Page/渡島支廳.md "wikilink")[漁業為主](../Page/漁業.md "wikilink")[城鎮](../Page/城鎮.md "wikilink")，北面[內浦灣](../Page/內浦灣.md "wikilink")，南部為山地；已於2004年12月1日與[戶井町](../Page/戶井町.md "wikilink")、[惠山町](../Page/惠山町.md "wikilink")、[椴法華村同時](../Page/椴法華村.md "wikilink")[併入](../Page/市町村合併.md "wikilink")[函館市](../Page/函館市.md "wikilink")，過去的轄區相當於現在的函館市南茅部支所的負責範圍。

町名源自[阿伊努語的](../Page/阿伊努語.md "wikilink")「kaya-un-pes」，意思是像帆船的懸崖。

## 歷史

### 沿革

  - 1906年4月1日：[茅部郡臼尻村](../Page/茅部郡.md "wikilink")、熊泊村合併為[臼尻村](../Page/臼尻村.md "wikilink")，並成為北海道二級村。\[1\]
  - 1919年4月1日：[椴法華村](../Page/椴法華村.md "wikilink")（現在的椴法華支所轄區）從[尾札部村分村](../Page/尾札部村.md "wikilink")，並成為北海道二級村。
  - 1959年5月1日：尾禮部村、臼尻村合併為南茅部町。
  - 2004年12月1日：與[龜田郡](../Page/龜田郡.md "wikilink")[戶井町](../Page/戶井町.md "wikilink")、[惠山町](../Page/惠山町.md "wikilink")、[椴法華村一同被併入](../Page/椴法華村.md "wikilink")[函館市](../Page/函館市.md "wikilink")。

## 交通

### 航空

  - [函館機場](../Page/函館機場.md "wikilink")

### 道路

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>一般國道</dt>

</dl>
<ul>
<li>國道278號</li>
</ul></td>
<td><dl>
<dt><a href="../Page/主要地方道.md" title="wikilink">主要地方道</a></dt>

</dl>
<ul>
<li>北海道道83號函館南茅部線</li>
</ul>
<dl>
<dt><a href="../Page/都道府縣道.md" title="wikilink">道道</a></dt>

</dl>
<ul>
<li>北海道道980號臼尻豐崎線</li>
</ul></td>
</tr>
</tbody>
</table>

## 觀光點

  - 川汲公園
  - 川汲溫泉
  - 大船溫泉
  - 大船遺跡
  - 臼尻劇場

## 教育

### 高等學校

  - [道立北海道南茅部高等學校](http://www.minamikayabe.hokkaido-c.ed.jp/)

### 中學校

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>南茅部町立臼尻中學校</li>
</ul></td>
<td><ul>
<li>南茅部町立尾札部中學校</li>
</ul></td>
</tr>
</tbody>
</table>

### 小學校

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>南茅部町立臼尻小學校</li>
<li>南茅部町立木直小學校</li>
</ul></td>
<td><ul>
<li>南茅部町立大船小學校</li>
</ul></td>
<td><ul>
<li>南茅部町立磨光小學校</li>
</ul></td>
</tr>
</tbody>
</table>

## 姊妹市・友好都市

### 日本

  - [佐井村](../Page/佐井村.md "wikilink")（[青森縣](../Page/青森縣.md "wikilink")[下北郡](../Page/下北郡.md "wikilink")）

## 本地出身的名人

  - [野村伸](../Page/野村伸.md "wikilink")（漫畫家）

## 外部連結

  - [函館市南茅部支所](https://web.archive.org/web/20071224235911/http://www.city.hakodate.hokkaido.jp/minamikayabe/)

## 參考資料

<div class="references-small">

<references />

[Category:渡島管內](../Category/渡島管內.md "wikilink")
[Category:函館市](../Category/函館市.md "wikilink")

1.