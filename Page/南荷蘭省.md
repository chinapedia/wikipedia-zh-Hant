[Map_South_Holland.png](https://zh.wikipedia.org/wiki/File:Map_South_Holland.png "fig:Map_South_Holland.png")
**南荷兰**（[荷兰语](../Page/荷兰语.md "wikilink"):
）是[荷兰的一个省](../Page/荷兰.md "wikilink")，省会城市是[海牙](../Page/海牙.md "wikilink")。南荷兰省位于该国的西部，西临[北海](../Page/北海_\(大西洋\).md "wikilink")，与之毗邻的省份有[北荷兰省](../Page/北荷兰省.md "wikilink")、[西兰省](../Page/西兰省.md "wikilink")、[乌特勒支省](../Page/乌特勒支省.md "wikilink")、[北布拉班特省和](../Page/北布拉班特省.md "wikilink")[海尔德兰省](../Page/海尔德兰省.md "wikilink")。南荷兰省是荷兰高人口密度最稠密和工业化程度最高的地区。

南荷兰省的主要城市有[海牙](../Page/海牙.md "wikilink") (*Den Haag* or *'s-Gravenhage*)
（荷兰政府和国际法庭的所在地皆位于该市）、[鹿特丹](../Page/鹿特丹.md "wikilink")。[莱顿](../Page/莱顿.md "wikilink")、[代尔夫特](../Page/代尔夫特.md "wikilink")、[豪达的市中心拥有许多](../Page/豪达.md "wikilink")17世纪时的建筑物。[卡特韦克的市中心位于海边](../Page/卡特韦克.md "wikilink")，[鹿特丹港是全世界最大的港口之一](../Page/鹿特丹港.md "wikilink")，新兴城市[祖特爾梅爾是](../Page/祖特爾梅爾.md "wikilink")1970年以后才发展起来的。
其他新兴的城镇：[艾瑟尔河畔卡佩勒](../Page/艾瑟尔河畔卡佩勒.md "wikilink")（Capelle aan den
IJssel）、[赫勒富茨劳斯](../Page/赫勒富茨劳斯.md "wikilink")（Hellevoetsluis）、[斯派克尼瑟](../Page/斯派克尼瑟.md "wikilink")（Spijkenisse）均座落于[鹿特丹附近](../Page/鹿特丹.md "wikilink")。

南荷兰省的[ISO代码为](../Page/国际标准化组织.md "wikilink")**ISO 3166-2:NL**-ZH.

## 历史

南荷兰省于1840年设立，当时的[荷兰省被划分为](../Page/荷兰省.md "wikilink")[北荷兰省和](../Page/北荷兰省.md "wikilink")[南荷兰省](../Page/南荷兰省.md "wikilink")，而现在的南荷兰省就是当时的荷兰省南部。之后南荷兰省的三个市镇[奥德瓦特Oudewater](../Page/奥德瓦特.md "wikilink")(1970)、[武尔登Woerden](../Page/武尔登.md "wikilink")(1989)和[菲亚嫩Vianen](../Page/菲亚嫩.md "wikilink")(2002)又先后被划归[乌得勒支省管辖](../Page/乌得勒支省.md "wikilink")。2011年[蘭斯台德地區有合併建省的計畫](../Page/蘭斯台德.md "wikilink")，位於南荷蘭省的蘭斯台德地區也有意加入此新的省份建置\[1\]。

### 1795年前

今日南荷蘭在古代長期都屬於[荷蘭地區](../Page/荷蘭地區.md "wikilink")。

9世紀到16世紀之間荷蘭隸屬於[神聖羅馬帝國並由其設置的荷蘭縣管轄](../Page/神聖羅馬帝國.md "wikilink")，縣治的範圍因荷蘭地區的實力而可以統治其他地區，之後這塊地區被[哈布斯堡王朝統治到](../Page/哈布斯堡王朝.md "wikilink")[荷蘭革命為止](../Page/荷蘭革命.md "wikilink")。

16世紀到1795年一直是七省聯盟和[荷蘭共和國的重要省份](../Page/荷蘭共和國.md "wikilink")，作為國內最富有以及最有實力的省分，荷蘭有效的主宰聯盟，在這段期間荷蘭已經出現南北之分，北方稱作Noorderkwartier，而南方稱作Zuiderkwartier，這塊地區已經粗略形成現代省分劃分的雛形。

### 1795年至1840年

南荷蘭省最早的雛形在1795年至1813年法治時期出現，這段期也是荷蘭省份大幅變更的時代，1795年[巴達維亞共和國成立](../Page/巴達維亞共和國.md "wikilink")，1798年4月23日憲法生效舊有疆界遭到變更，新的共和國依據人口平均的設置八個行省([英文](../Page/英文.md "wikilink")：departments；[法文](../Page/法文.md "wikilink")：*département*)，荷蘭地區(亦即不包含[南尼德蘭](../Page/南尼德蘭.md "wikilink"))設有五個行省，如下：特克塞爾(Texel)、阿姆斯特(Amstel)、地洛夫(Delf)、斯凱爾特與默茲(Schelde
en
Maas)以及萊茵(Rijn)，前三個行省位於今日的[荷蘭地區](../Page/荷蘭地區.md "wikilink")，後兩個則是今日不同的省分，1801年荷蘭舊的疆界都重新區劃，這次曇花一現的重組使荷蘭一向凌駕其他地區的狀況不再。

1807年再次的重組，荷蘭全境被統合成兩個行省－默茲蘭(Maasland，約為今日南荷蘭省地區)以及阿姆斯特蘭(Amstelland，約為今日北荷蘭省地區)，這次重組也歷時不久而在1810年荷蘭全部的省分全部被化威诶[法蘭西第一帝國](../Page/法蘭西第一帝國.md "wikilink")，麥斯蘭改名為麥登逢地默茲([荷蘭文](../Page/荷蘭文.md "wikilink")：Monden
van de
Maas；[法文](../Page/法文.md "wikilink")：Bouches-de-la-Meuse)，阿姆斯特蘭以及[烏特勒支合併為須得利行省](../Page/烏特勒支.md "wikilink")([荷蘭語](../Page/荷蘭語.md "wikilink")："Zuiderzee"；[法文](../Page/法文.md "wikilink")：Zuyderzée)。

隨著1813年法國遭受戰敗，這些建置也在一年內回復到原狀，次年原本的憲法恢復效力，荷蘭全國改組省份以及區域(*landschappen*)，麥登逢地默茲以及須得利合併為荷蘭省。

然而有些事情已經生米煮成熟飯，在1814年荷蘭省要重新建立之際，該省出現了兩名總督，一個是前默茲蘭另一個則是阿姆斯特蘭的總督，儘管兩地重新回歸，但是這兩個地方持續出現歧異威脅著荷蘭省繼續存續。

1840年荷蘭修憲決議再次分割荷蘭，從此和蘭分成南、北荷蘭省，以[阿姆斯特丹為主的北荷蘭省](../Page/阿姆斯特丹.md "wikilink")，在1838年依舊存有不滿向[荷蘭最高法院控訴南荷蘭省分離之舉](../Page/荷蘭最高法院.md "wikilink")。

### 1840年至今

1840年後南荷蘭省陸續分割了三個[市鎮給](../Page/荷蘭市鎮政治.md "wikilink")[烏特勒支](../Page/烏特勒支.md "wikilink")，分別是：1970年奧德瓦特(Oudewater)、1989年武爾登(Woerden)以及2002年菲亞嫩(Vianen)。

2004年到2006年之間也有以下市鎮重組。

  - 2004年1月1日德利爾(De
    Lier)、斯格逢洛迪('s-Gravenzande)、蒙斯特(Monster)、諾德維克(Naaldwijk)以及懷特分(Wateringen)被合併成一個新市鎮-[韋斯特蘭](../Page/韋斯特蘭_\(荷蘭\).md "wikilink")；另外默茲蘭(Maasland)和史區蘭(Schipluiden)則另組一個新市鎮-[中代爾夫蘭](../Page/中代爾夫蘭.md "wikilink")
  - 2006年1月1日薩森海姆(Sassenheim)、福爾豪特(Voorhout)以及瓦爾蒙德(Warmond)被併入[泰靈恩](../Page/泰靈恩.md "wikilink")；蘭斯伯格(Rijnsburg)和法爾肯堡(Valkenburg)被併入[卡特韋克](../Page/卡特韋克.md "wikilink")。
  - 2007年1月1日特阿爾(Ter
    Aar)和利梅爾(Liemeer)被併入[新科普](../Page/新科普.md "wikilink")；布萊斯維克(Bleiswijk)、布蘭克與洛德斯(Berkel
    en
    Rodenrijs)以及柏格史區漢克(Bergschenhoek)被合併成[蘭辛厄爾蘭](../Page/蘭辛厄爾蘭.md "wikilink")；斯格逢洛迪被併入[比嫩馬斯](../Page/比嫩馬斯.md "wikilink")。
  - 2010年3月18日諾茲伯格(Rozenburg)被併入[鹿特丹](../Page/鹿特丹.md "wikilink")。

## 基层政权

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/阿尔布拉瑟丹.md" title="wikilink">阿尔布拉瑟丹Alblasserdam</a></li>
<li><a href="../Page/阿尔布兰茨瓦尔德.md" title="wikilink">阿尔布兰茨瓦尔德Albrandswaard</a></li>
<li><a href="../Page/莱茵河畔阿尔芬.md" title="wikilink">莱茵河畔阿尔芬</a></li>
<li><a href="../Page/巴伦德雷赫特.md" title="wikilink">巴伦德雷赫特Barendrecht</a></li>
<li><a href="../Page/拜尔赫阿姆巴赫特.md" title="wikilink">拜尔赫阿姆巴赫特Bergambacht</a></li>
<li><a href="../Page/拜尔尼瑟.md" title="wikilink">拜尔尼瑟</a></li>
<li><a href="../Page/比嫩马斯.md" title="wikilink">比嫩马斯Binnenmaas</a></li>
<li><a href="../Page/博德赫拉芬－雷韦克.md" title="wikilink">博德赫拉芬－雷韦克Bodegraven</a>-Reeuwijk</li>
<li><a href="../Page/博斯科普.md" title="wikilink">博斯科普Boskoop</a></li>
<li><a href="../Page/布里勒.md" title="wikilink">布里勒Brielle</a></li>
<li><a href="../Page/艾瑟尔河畔卡佩勒.md" title="wikilink">艾瑟尔河畔卡佩勒Capelle</a> aan den IJssel</li>
<li><a href="../Page/克罗姆斯特赖恩.md" title="wikilink">克罗姆斯特赖恩Cromstrijen</a></li>
<li><a href="../Page/代尔夫特.md" title="wikilink">代尔夫特Delft</a></li>
<li><a href="../Page/迪尔克斯兰.md" title="wikilink">迪尔克斯兰Dirksland</a></li>
<li><a href="../Page/多德雷赫特.md" title="wikilink">多德雷赫特Dordrecht</a></li>
<li><a href="../Page/希森兰登.md" title="wikilink">希森兰登Giessenlanden</a></li>
<li><a href="../Page/胡德雷德.md" title="wikilink">胡德雷德Goedereede</a></li>
<li><a href="../Page/霍林赫姆.md" title="wikilink">霍林赫姆Gorinchem</a></li>
<li><a href="../Page/豪达.md" title="wikilink">豪达Gouda</a></li>
<li><a href="../Page/赫拉夫斯特罗姆.md" title="wikilink">赫拉夫斯特罗姆</a></li>
<li><a href="../Page/斯赫拉芬哈赫.md" title="wikilink">斯赫拉芬哈赫</a><a href="../Page/海牙.md" title="wikilink">'s-Gravenhage</a></li>
<li><a href="../Page/哈尔丁斯费尔德－希森丹.md" title="wikilink">哈尔丁斯费尔德－希森丹Hardinxveld</a>-Giessendam</li>
<li><a href="../Page/赫勒富茨劳斯.md" title="wikilink">赫勒富茨劳斯Hellevoetsluis</a></li>
<li><a href="../Page/亨德里克－伊多－阿姆巴赫特.md" title="wikilink">亨德里克－伊多－阿姆巴赫特Hendrik</a>-Ido-Ambacht</li>
<li><a href="../Page/希勒霍姆.md" title="wikilink">希勒霍姆Hillegom</a></li>
<li><a href="../Page/卡赫和布拉瑟姆.md" title="wikilink">卡赫和布拉瑟姆Kaag</a> en Braassem</li>
<li><a href="../Page/卡特韦克.md" title="wikilink">卡特韦克</a></li>
<li><a href="../Page/科伦代克.md" title="wikilink">科伦代克Korendijk</a></li>
<li><a href="../Page/艾瑟尔河畔克林彭.md" title="wikilink">艾瑟尔河畔克林彭Krimpen</a> aan den IJssel</li>
<li><a href="../Page/兰辛厄尔兰.md" title="wikilink">兰辛厄尔兰Lansingerland</a></li>
<li><a href="../Page/莱尔丹.md" title="wikilink">莱尔丹Leerdam</a></li>
<li><a href="../Page/莱顿.md" title="wikilink">莱顿Leiden</a></li>
<li><a href="../Page/萊德多普.md" title="wikilink">萊德多普Leiderdorp</a></li>
<li><a href="../Page/莱德斯亨丹－福尔堡.md" title="wikilink">莱德斯亨丹－福尔堡Leidschendam</a>-Voorburg</li>
<li><a href="../Page/利斯费尔德.md" title="wikilink">利斯费尔德Liesveld</a></li>
<li><a href="../Page/利瑟.md" title="wikilink">利瑟Lisse</a></li>
<li><a href="../Page/马斯劳斯.md" title="wikilink">马斯劳斯Maassluis</a></li>
<li><a href="../Page/米德尔哈尼斯.md" title="wikilink">米德尔哈尼斯Middelharnis</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/中代爾夫蘭.md" title="wikilink">中代爾夫蘭Midden</a>-Delfland</li>
<li><del><a href="../Page/莫德雷赫特.md" title="wikilink">莫德雷赫特</a></del></li>
<li><a href="../Page/内德莱克.md" title="wikilink">内德莱克Nederlek</a></li>
<li><a href="../Page/新莱克兰.md" title="wikilink">新莱克兰</a></li>
<li><del><a href="../Page/艾瑟尔河畔尼沃凯尔克.md" title="wikilink">艾瑟尔河畔尼沃凯尔克</a></del></li>
<li><a href="../Page/新科普.md" title="wikilink">新科普Nieuwkoop</a></li>
<li><a href="../Page/诺德韦克.md" title="wikilink">诺德韦克</a></li>
<li><a href="../Page/诺德韦克豪特.md" title="wikilink">诺德韦克豪特Noordwijkerhout</a></li>
<li><a href="../Page/乌赫斯特海斯特.md" title="wikilink">乌赫斯特海斯特Oegstgeest</a></li>
<li><a href="../Page/东弗拉凯.md" title="wikilink">东弗拉凯Oostflakkee</a></li>
<li><a href="../Page/旧拜耶兰.md" title="wikilink">旧拜耶兰Oud</a>-Beijerland</li>
<li><a href="../Page/奥德凯尔克.md" title="wikilink">奥德凯尔克Ouderkerk</a></li>
<li><a href="../Page/帕彭德雷赫特.md" title="wikilink">帕彭德雷赫特Papendrecht</a></li>
<li><a href="../Page/派纳克－诺特多普.md" title="wikilink">派纳克－诺特多普Pijnacker</a>-Nootdorp</li>
<li><del><a href="../Page/雷韦克.md" title="wikilink">雷韦克</a></del></li>
<li><a href="../Page/里德凯尔克.md" title="wikilink">里德凯尔克Ridderkerk</a></li>
<li><a href="../Page/莱茵沃德.md" title="wikilink">莱茵沃德Rijnwoude</a></li>
<li><a href="../Page/赖斯韦克.md" title="wikilink">赖斯韦克</a></li>
<li><a href="../Page/鹿特丹.md" title="wikilink">鹿特丹Rotterdam</a></li>
<li><del><a href="../Page/罗曾堡.md" title="wikilink">罗曾堡</a></del></li>
<li><a href="../Page/斯希丹.md" title="wikilink">斯希丹Schiedam</a></li>
<li><a href="../Page/斯洪霍芬.md" title="wikilink">斯洪霍芬Schoonhoven</a></li>
<li><a href="../Page/斯利德雷赫特.md" title="wikilink">斯利德雷赫特Sliedrecht</a></li>
<li><a href="../Page/斯派克尼瑟.md" title="wikilink">斯派克尼瑟Spijkenisse</a></li>
<li><a href="../Page/斯特赖恩.md" title="wikilink">斯特赖恩Strijen</a></li>
<li><a href="../Page/泰灵恩.md" title="wikilink">泰灵恩</a></li>
<li><a href="../Page/弗拉尔丁恩.md" title="wikilink">弗拉尔丁恩Vlaardingen</a></li>
<li><a href="../Page/弗利斯特.md" title="wikilink">弗利斯特Vlist</a></li>
<li><a href="../Page/福爾斯霍滕.md" title="wikilink">福爾斯霍滕Voorschoten</a></li>
<li><a href="../Page/瓦丁斯芬.md" title="wikilink">瓦丁斯芬Waddinxveen</a></li>
<li><a href="../Page/瓦瑟訥爾.md" title="wikilink">瓦瑟訥爾</a></li>
<li><a href="../Page/韦斯特兰_(荷兰).md" title="wikilink">韦斯特兰</a></li>
<li><a href="../Page/西福尔讷.md" title="wikilink">西福尔讷Westvoorne</a></li>
<li><a href="../Page/泽德里克.md" title="wikilink">泽德里克Zederik</a></li>
<li><del><a href="../Page/泽芬豪曾－莫尔卡佩勒.md" title="wikilink">泽芬豪曾－莫尔卡佩勒</a></del></li>
<li><a href="../Page/祖特爾梅爾.md" title="wikilink">祖特爾梅爾Zoetermeer</a></li>
<li><a href="../Page/祖特爾烏德.md" title="wikilink">祖特爾烏德Zoeterwoude</a></li>
<li><a href="../Page/南普拉斯.md" title="wikilink">南普拉斯</a></li>
<li><a href="../Page/兹韦恩德雷赫特.md" title="wikilink">兹韦恩德雷赫特Zwijndrecht</a></li>
</ul></td>
</tr>
</tbody>
</table>

[2011-P08-Zuid-Holland-b54.jpg](https://zh.wikipedia.org/wiki/File:2011-P08-Zuid-Holland-b54.jpg "fig:2011-P08-Zuid-Holland-b54.jpg")

## 根据地理位置罗列的主要的市镇位置

`                    `[`希勒霍姆`](../Page/希勒霍姆.md "wikilink")
`                 `[`利瑟`](../Page/利瑟.md "wikilink")
`          `[`诺德韦克`](../Page/诺德韦克.md "wikilink")
`      `[`卡特韦克`](../Page/卡特韦克.md "wikilink")
`         `[`莱顿`](../Page/莱顿.md "wikilink")`              `[`莱茵河畔阿尔芬`](../Page/莱茵河畔阿尔芬.md "wikilink")` `
` `[`瓦瑟讷尔`](../Page/瓦瑟讷尔.md "wikilink")` `[`福爾斯霍滕`](../Page/福爾斯霍滕.md "wikilink")`                          `[`博德赫拉芬－雷韦克`](../Page/博德赫拉芬－雷韦克.md "wikilink")
`          `[`莱德斯亨丹－福尔堡`](../Page/莱德斯亨丹－福尔堡.md "wikilink")
[`海牙`](../Page/海牙.md "wikilink")` `[`祖特尔梅尔`](../Page/祖特尔梅尔.md "wikilink")`                   `[`豪达`](../Page/豪达.md "wikilink")
`   `[`赖斯韦克`](../Page/赖斯韦克.md "wikilink")` `[`派纳克－诺特多普`](../Page/派纳克－诺特多普.md "wikilink")
`       `[`代尔夫特`](../Page/代尔夫特.md "wikilink")`            `[`艾瑟尔河畔卡佩勒`](../Page/艾瑟尔河畔卡佩勒.md "wikilink")
[`韦斯特兰`](../Page/韦斯特兰_\(荷兰\).md "wikilink")` `[`弗拉尔丁恩`](../Page/弗拉尔丁恩.md "wikilink")` `[`斯希丹`](../Page/斯希丹.md "wikilink")` `[`鹿特丹`](../Page/鹿特丹.md "wikilink")
` `[`赫勒富茨劳斯`](../Page/赫勒富茨劳斯.md "wikilink")` `[`斯派克尼瑟`](../Page/斯派克尼瑟.md "wikilink")` `[`巴伦德雷赫特`](../Page/巴伦德雷赫特.md "wikilink")
`                            `[`兹韦恩德雷赫特`](../Page/兹韦恩德雷赫特_\(荷兰\).md "wikilink")
`                             `[`多德雷赫特`](../Page/多德雷赫特.md "wikilink")` `[`斯利德雷赫特`](../Page/斯利德雷赫特.md "wikilink")` `[`霍林赫姆`](../Page/霍林赫姆.md "wikilink")

## 岛屿

[Keukenhof-Szmurlo.jpg](https://zh.wikipedia.org/wiki/File:Keukenhof-Szmurlo.jpg "fig:Keukenhof-Szmurlo.jpg")公园(Keukenhof)\]\]
(from north to south and from west to east, with municipalities)

  - [Rozenburg](../Page/Rozenburg.md "wikilink")
      - Rozenburg
        [1](https://web.archive.org/web/20030815094746/http://www.voorne-putten.nl/rozenburg/)
      - part of [Rotterdam](../Page/Rotterdam.md "wikilink") (Botlek
        [2](https://web.archive.org/web/20070830065047/http://www.voorne-putten.nl/botlek/),
        [Europoort](../Page/Europoort.md "wikilink")
        [3](https://web.archive.org/web/20031226015808/http://www.voorne-putten.nl/europoort/),
        Maasvlakte
        [4](https://web.archive.org/web/20031226020107/http://www.voorne-putten.nl/maasvlakte/))
  - [IJsselmonde](../Page/IJsselmonde.md "wikilink")
      - part of [Rotterdam](../Page/Rotterdam.md "wikilink")
      - [Barendrecht](../Page/Barendrecht.md "wikilink")
        [5](http://www.plattegronden.nl/gemeentebarendrecht/)
      - [Ridderkerk](../Page/Ridderkerk.md "wikilink")
        [6](http://www.plattegronden.nl/ridderkerk/)
      - [Hendrik-Ido-Ambacht](../Page/Hendrik-Ido-Ambacht.md "wikilink")
        [7](http://www.plattegronden.nl/hendrik-ido-ambacht/)
      - [Zwijndrecht](../Page/Zwijndrecht,_Netherlands.md "wikilink")
        [8](http://www.plattegronden.nl/zwijndrecht/),
        [9](http://www.plattegronden.nl/heerjansdam/)
  - [Voorne-Putten](../Page/Voorne-Putten.md "wikilink")
      - [Westvoorne](../Page/Westvoorne.md "wikilink")
        [10](http://www.plattegronden.nl/gemeentewestvoorne/)
      - [Brielle](../Page/Brielle.md "wikilink")
        [11](http://www.plattegronden.nl/gemeentebrielle/)
      - [Hellevoetsluis](../Page/Hellevoetsluis.md "wikilink")
        [12](http://www.plattegronden.nl/gemeentehellevoetsluis/)
      - [Bernisse](../Page/Bernisse.md "wikilink")
        [13](http://www.plattegronden.nl/gemeentebernisse)
      - [Spijkenisse](../Page/Spijkenisse.md "wikilink")
        [14](http://www.plattegronden.nl/spijkenisse/)
  - [Hoeksche Waard](../Page/Hoeksche_Waard.md "wikilink")
      - [Oud-Beijerland](../Page/Oud-Beijerland.md "wikilink")
        [15](http://www.plattegronden.nl/gemeenteoud-beijerland/)
      - [Binnenmaas](../Page/Binnenmaas.md "wikilink")
      - part of [Korendijk](../Page/Korendijk.md "wikilink")
        [16](http://www.plattegronden.nl/gemeenteoud-beijerland/)
      - [Cromstrijen](../Page/Cromstrijen.md "wikilink")
        [17](http://www.plattegronden.nl/gemeentecromstrijen/)
      - [Strijen](../Page/Strijen.md "wikilink")
        [18](http://www.plattegronden.nl/gemeentestrijen/)
  - Eiland van [Dordrecht](../Page/Dordrecht.md "wikilink")
    [19](https://web.archive.org/web/20050405225424/http://www.svd-stadsvervoer.nl/kaart/stadskaart.html)
      - Dordrecht
  - [Goeree-Overflakkee](../Page/Goeree-Overflakkee.md "wikilink")
    [20](http://www.plattegronden.nl/goeree-overflakkee/)
      - [Goedereede](../Page/Goedereede.md "wikilink")
      - [Dirksland](../Page/Dirksland.md "wikilink")
      - [Middelharnis](../Page/Middelharnis.md "wikilink")
      - [Oostflakkee](../Page/Oostflakkee.md "wikilink").
  - Tiengemeten
      - part of [Korendijk](../Page/Korendijk.md "wikilink")
        [21](http://www.plattegronden.nl/gemeenteoud-beijerland/)

## 其他相关条目

  - [荷蘭 (地區)](../Page/荷蘭_\(地區\).md "wikilink")
  - [荷蘭省級政治](../Page/荷蘭省級政治.md "wikilink")
      - [北荷蘭省](../Page/北荷蘭省.md "wikilink")

## 相關連結

<references/>

## 外部链接

  - [Official
    site](https://web.archive.org/web/20060905180812/http://www.pzh.nl/)
      - [List of municipalities with population and area
        figures](https://web.archive.org/web/20060117070000/http://www.zuid-holland.nl/algemeen/provincie/gemeenten.jsp)
        (hard to find on the site, because not under *Gemeenten* but
        under *Over de provincie*\!)
  - [Basic
    information](http://www.sdu.nl/staatscourant/scdata/prov/zuid-holland.htm)
  - [Lists of towns and villages, with
    municipality](http://home.wxs.nl/~pagklein/gemeente/zuidholland.html)
  - Province maps showing subdivision in municipalities:
      - [small version linking for each municipality to basic data page
        in
        Dutch](http://www.sdu.nl/staatscourant/gemeentes/gemprovin.htm#ZH)
        (2006)
  - [Map
    of 1853](http://members.lycos.nl/netherworld/maps_pocket_1853/pocket_1853_04_l.jpg)
  - [Tourist
    information](http://www.zuid-holland.com/zuid-hollandinfo/welcome.asp?lang=0)
    - The province is subdivided in eight regions, not corresponding to
    those above. The maps show villages, not municipalities.
  - [Tourist information for a region called Zuid-Holland Zuid (South
    Holland South), but different from the one in the previous section:
    less on the west, more on the
    east](https://web.archive.org/web/20070311105204/http://www.vvvzhz.nl/engels/krt_eng.html);
    it is the region called "Dutch river area" in the website of the
    previous link.
  - <https://web.archive.org/web/20051120143347/http://www.atlasgeo.ch/fotw/flags/nl-zh-.html>
    - various info on the province and its municipalities, in particular
    flags
  - [Deltaworks Online - Flood protection of South Holland and
    Deltaregion](http://www.deltaworks.org)

Entries for South Holland in worldwide gazetteers:
[Alexandria Digital
Library](https://web.archive.org/web/20070310233913/http://middleware.alexandria.ucsb.edu/client/gaz/adl/sessionRenderReportID.jsp?identifier=adlgaz-1-3096966-05)
| [Getty
Thesaurus](http://www.getty.edu/vow/TGNFullDisplay?find=leiden&place=&nation=Netherlands&english=Y&subjectid=7003632)
|
[ESRI](http://maps.esri.com/scripts/esrimap.dll?name=gaz_avd&cmd=SetExtent&extent=3.168846,50.560789,7.410874,53.745591&fid=2586&flay=ADMIN&Draw_0=ON&Draw_1=ON&Draw_2=ON&Draw_3=ON&Draw_4=ON&Draw_5=ON&Draw_6=ON&Draw_7=ON&Draw_8=ON&SelLyr=nil&Size=422.000000,345.000000)
| [Falling Rain](http://www.fallingrain.com/world/NL/11/) |
[Tageo](http://www.tageo.com/index-e-nl-v-11.htm) | [World
Gazetteer](https://archive.is/20130105080327/http://www.world-gazetteer.com/wg.php?x=1109495534&men=gpro&lng=en&gln=xx&dat=32&geo=-2723&srt=npan&col=aohdq)

[\*](../Category/南荷兰省.md "wikilink")

1.  <http://www.nrc.nl/nieuws/2011/02/04/randstadprovincies-onderzoeken-fusie/>