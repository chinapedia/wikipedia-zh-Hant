**南谯区**是[中國](../Page/中國.md "wikilink")[安徽省](../Page/安徽省.md "wikilink")[滁州市下辖的一个区](../Page/滁州.md "wikilink")。面积1273平方千米，人口27万。邮政编码239000。南谯区原为[寄治区](../Page/寄治区.md "wikilink")，曾驻[琅琊区](../Page/琅琊区.md "wikilink")[北门街道](../Page/北门街道_\(滁州市\).md "wikilink")。现驻**南谯区**[乌衣镇政务新区](../Page/乌衣镇.md "wikilink")。下辖1个街道、8个镇。

## 古代行政区划

名称来源于[三国时在此设立](../Page/三国.md "wikilink")[侨州侨置](../Page/侨州.md "wikilink")“南谯州”，中原地区的[谯州治今安徽](../Page/谯州.md "wikilink")[亳州市](../Page/亳州市.md "wikilink")[谯城区](../Page/谯城区.md "wikilink")。

[隋](../Page/隋.md "wikilink")[开皇初改](../Page/开皇.md "wikilink")[南谯州置](../Page/南谯州.md "wikilink")，“因滁水为名”（《太平寰宇记》）。治[新昌县](../Page/新昌县.md "wikilink")（后改[清流县](../Page/清流县.md "wikilink")）。

## 行政区划

下辖1个街道办事处、8个镇。

  - 街道办事处：大王街道办事处。
  - 镇：乌衣镇、沙河镇、章广镇、黄泥岗镇、大柳镇、腰铺镇、珠龙镇、施集镇。

[滁州](../Page/category:安徽市辖区.md "wikilink")

[南谯区](../Category/南谯区.md "wikilink")
[区](../Category/滁州区县市.md "wikilink")