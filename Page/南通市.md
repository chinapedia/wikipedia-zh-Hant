**南通市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[江苏省下辖的](../Page/江苏省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于江苏省东南部，东临[黄海](../Page/黄海.md "wikilink")，南临[长江](../Page/长江.md "wikilink")，与[上海的](../Page/上海.md "wikilink")[崇明岛及](../Page/崇明岛.md "wikilink")[苏州市隔江相望](../Page/苏州市.md "wikilink")。南通是中国近代工业的发源地之一，经济以纺织、石化、港口、机械、电子、化工、建筑等较发达，南通是中国首批[对外开放的14个沿海城市之一](../Page/沿海开放城市.md "wikilink")。市区有[狼山](../Page/狼山_\(江苏\).md "wikilink")、[濠河等名胜](../Page/濠河.md "wikilink")。

2004年9月26日，首届世界大城市带发展高层论坛在南通举办。2005年[GDP增长](../Page/GDP.md "wikilink")15.4％，增速位居江苏第一，2017年GDP总量7734.6亿元，居全国主要城市第18位，同比2016年前进3位，GDP居全国地级市第6位，同比2017年前进2位。其下属县市均为全国百强县。

## 历史

南通位處於[長江下游](../Page/長江.md "wikilink")，是一處由長江的沙泥沉積而成的陸地，大約在6500年前成陆。据古文化遗址考证，远在5000多年前的[新石器时期](../Page/新石器时期.md "wikilink")，境内海安青墩地区就有原始[氏族部落繁衍生息](../Page/氏族部落.md "wikilink")。今市区一带，五代设[军](../Page/军_\(行政区划\).md "wikilink")，称静海。[后周](../Page/后周.md "wikilink")[显德五年](../Page/显德.md "wikilink")（958年）筑城，定名[通州](../Page/通州_\(江蘇\).md "wikilink")。[北宋](../Page/北宋.md "wikilink")[天圣元年](../Page/天圣.md "wikilink")（1023年），改称崇州，又名崇川，后复称通州。[清朝时](../Page/清朝.md "wikilink")，升为[通州直隶州](../Page/通州直隶州.md "wikilink")，辖境除今南通市外，含[泰兴市](../Page/泰兴市.md "wikilink")。

[清末](../Page/清末.md "wikilink")[状元](../Page/状元.md "wikilink")、实业家、教育家海门人[张謇开风气之先](../Page/张謇.md "wikilink")，在南通创办全国第一所师范学校、[第一座民间博物苑](../Page/南通博物苑.md "wikilink")、第一所纺织学校、第一所刺绣学校、第一所戏剧学校、第一所盲哑学校和第一所气象站等，并由中国人自己按照近代理念全面规划建设城市，南通因此堪称“中国近代第一城”。[辛亥革命后](../Page/辛亥革命.md "wikilink")，全国[废府州厅改县](../Page/废府州厅改县.md "wikilink")；為与[河北的](../Page/河北.md "wikilink")[通州](../Page/通州区_\(北京市\).md "wikilink")（今属北京）区分，因位于南方而改名称[南通县](../Page/南通县.md "wikilink")。

1949年2月，中共佔領南通全境后，划城区、狼山、天生港一带建南通市，南通县政府迁往金沙镇。同年设置[南通专区](../Page/南通专区.md "wikilink")，驻南通市。1953年时，南通专区下领七县：[海安县](../Page/海安县.md "wikilink")、[如东县](../Page/如东县.md "wikilink")、[如皋县](../Page/如皋县.md "wikilink")、南通县、[海门县](../Page/海门县.md "wikilink")、[启东县](../Page/启东县.md "wikilink")、[崇明县](../Page/崇明县.md "wikilink")。南通市则领六区：[城东区](../Page/城东区_\(南通市\).md "wikilink")、[城西区](../Page/城西区_\(南通市\).md "wikilink")、[钟秀区](../Page/钟秀区.md "wikilink")、[唐闸区](../Page/唐闸区.md "wikilink")、[狼山区](../Page/狼山区.md "wikilink")、[芦泾区](../Page/芦泾区.md "wikilink")\[1\]。1962年改为[省辖市](../Page/省辖市.md "wikilink")。1983年，[南通地区与南通市合并](../Page/南通地区.md "wikilink")，实行市管县体制。

2009年4月17日，[中国国务院作出](../Page/中华人民共和国国务院.md "wikilink")《关于同意江苏省调整南通市部分行政区划的批复》，[江苏省人民政府下发了](../Page/江苏省人民政府.md "wikilink")《关于撤销通州市设立南通市通州区的通知》。经国务院批准，撤销通州市，设立南通市通州区，以原通州市的行政区域为通州区的行政区域，区人民政府驻金沙镇。调整后的通州区下辖19个镇和一个省级经济开发区，分别是：金沙镇、西亭镇、二甲镇、五甲镇、东社镇、三余镇、十总镇、骑岸镇、石港镇、四安镇、刘桥镇、平潮镇、平东镇、五接镇、兴仁镇、兴东镇、川姜镇、先锋镇、张芝山镇和通州经济开发区。全区面积1351平方公里，总人口约124万。

## 地理

属于[副热带季风气候](../Page/副热带季风气候.md "wikilink")。

## 政治

### 现任领导

<table>
<caption>南通市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党南通市委员会.md" title="wikilink">中国共产党<br />
南通市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/南通市人民代表大会.md" title="wikilink">南通市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/南通市人民政府.md" title="wikilink">南通市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议南通市委员会.md" title="wikilink">中国人民政治协商会议<br />
南通市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/陆志鹏.md" title="wikilink">陆志鹏</a>[2]</p></td>
<td><p><a href="../Page/徐惠民.md" title="wikilink">徐惠民</a>[3]</p></td>
<td><p><a href="../Page/黄巍东.md" title="wikilink">黄巍东</a>[4]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/河南省.md" title="wikilink">河南省</a><a href="../Page/孟津县.md" title="wikilink">孟津县</a></p></td>
<td><p><a href="../Page/江苏省.md" title="wikilink">江苏省</a><a href="../Page/苏州市.md" title="wikilink">苏州市</a><a href="../Page/吴江区.md" title="wikilink">吴江区</a></p></td>
<td><p>江苏省<a href="../Page/启东市.md" title="wikilink">启东市</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2016年1月</p></td>
<td><p>候任</p></td>
<td><p>2017年2月</p></td>
<td></td>
</tr>
</tbody>
</table>

### 行政区划

现辖3个[市辖区](../Page/市辖区.md "wikilink")、1个[县](../Page/县_\(中华人民共和国\).md "wikilink")，代管4个[县级市](../Page/县级市.md "wikilink")。

  - 市辖区：[崇川区](../Page/崇川区.md "wikilink")、[港闸区](../Page/港闸区.md "wikilink")、[通州区](../Page/通州区_\(南通市\).md "wikilink")
  - 县级市：[启东市](../Page/启东市.md "wikilink")、[如皋市](../Page/如皋市.md "wikilink")、[海门市](../Page/海门市.md "wikilink")、[海安市](../Page/海安市.md "wikilink")
  - 县：[如东县](../Page/如东县.md "wikilink")

此外，[南通经济技术开发区是南通市设立的](../Page/南通经济技术开发区.md "wikilink")[国家级经济技术开发区](../Page/国家级经济技术开发区.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p><strong>南通市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[5]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>320600</p></td>
</tr>
<tr class="odd">
<td><p>320602</p></td>
</tr>
<tr class="even">
<td><p>320611</p></td>
</tr>
<tr class="odd">
<td><p>320612</p></td>
</tr>
<tr class="even">
<td><p>320623</p></td>
</tr>
<tr class="odd">
<td><p>320681</p></td>
</tr>
<tr class="even">
<td><p>320682</p></td>
</tr>
<tr class="odd">
<td><p>320684</p></td>
</tr>
<tr class="even">
<td><p>320685</p></td>
</tr>
<tr class="odd">
<td><p>注：崇川区数字包含南通经济技术开发区所辖4街道。</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

### 领土争端

位于南通外海的[苏岩礁](../Page/苏岩礁.md "wikilink")，在低潮時仍處在水面以下4.6米，目前由[韓國和中國實際控制](../Page/韓國.md "wikilink")。2002年，[韓國開始在此興建](../Page/韓國.md "wikilink")「韓國離於島綜合海洋科學基地」。中國認為蘇岩礁並非島嶼，由於位於東海大陸架上，因此應該屬於中國的專屬經濟區，但韓國否认中方的诉求。

## 人口

<table>
<caption><strong>南通市各区（县、市）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[6]（2010年11月）</p></th>
<th><p>户籍人口[7]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>南通市</p></td>
<td><p>7283622</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>崇川区</p></td>
<td><p>869049</p></td>
<td><p>11.93</p></td>
</tr>
<tr class="even">
<td><p>港闸区</p></td>
<td><p>266326</p></td>
<td><p>3.66</p></td>
</tr>
<tr class="odd">
<td><p>通州区</p></td>
<td><p>1138738</p></td>
<td><p>15.63</p></td>
</tr>
<tr class="even">
<td><p>海安市</p></td>
<td><p>866337</p></td>
<td><p>11.89</p></td>
</tr>
<tr class="odd">
<td><p>如东县</p></td>
<td><p>995983</p></td>
<td><p>13.67</p></td>
</tr>
<tr class="even">
<td><p>启东市</p></td>
<td><p>972525</p></td>
<td><p>13.35</p></td>
</tr>
<tr class="odd">
<td><p>如皋市</p></td>
<td><p>1267066</p></td>
<td><p>17.40</p></td>
</tr>
<tr class="even">
<td><p>海门市</p></td>
<td><p>907598</p></td>
<td><p>12.46</p></td>
</tr>
<tr class="odd">
<td><p>注：崇川区的常住人口数据中包含南通经济技术开发区182104人。</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

根据[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")7282835人\[8\]。其中，男性人口为3443066人，占47.28%；女性人口为3839769人，占52.72%。总人口性别比（以女性为100）为89.67。0－14岁人口为774003人，占10.63%；15－64岁人口为5306345人，占72.86%；65岁及以上人口为1202487人，占16.51%。

在人口结构方面，南通市的老龄化程度为全省最高。\[9\]

截至2017年末，南通市常住人口730.5万人，其中，城镇人口达到482.4万人，增长2.6%，城镇化率66.03%，比2016年提高1.66个百分点。截至2017年末，南通户籍人口764.5万人，人口出生率7.99‰，人口死亡率10.68‰，人口自然增长率-2.69‰。

## 经济

[Nantong_map.gif](https://zh.wikipedia.org/wiki/File:Nantong_map.gif "fig:Nantong_map.gif")
港口业是一直以来南通的主要支柱产业，纺织业、加工业也是重要的收入来源。临港重工业特别是造船业近年发展相当迅速。南通私营经济正处于蓬勃发展的阶段。2009年5月，苏通科技产业园项目正式启动，标志着与新加坡跨国合作的深化，苏州新加坡工业园将实现跨江发展。

### 工业园区

### 南通滨海园区

[南通滨海园区成立于](../Page/南通滨海园区.md "wikilink")2012年1月，是南通市政府直属、按照国家级开发区标准建立的重点开发园区，总规划控制面积达820平方公里、近期代管范围面积585平方公里，目标建成港口、产业、新城三位一体的现代化国际滨海新城。南通滨海园区具有港口资源优、发展空间广、地理位置佳、联运条件好等综合优势，其产业定位：中国东部沿海大型临港产业基地，长三角北翼综合性物流基地和重型装备制造业集聚区，江苏沿海地区重要能源基地，江苏沿海国家级生态旅游度假区。

### 南通经济技术开发区

[缩略图](https://zh.wikipedia.org/wiki/File:China_Abacuses_Museum_04_2013-01.JPG "fig:缩略图")，清代南通周懋琦製算盤。\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:House_of_the_Ming_Dynasty_in_Dinggujiao_of_Nantong_02_2013-01.JPG "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Historic_Site_of_the_North_Gate_of_Nantong_01_2013-01.JPG "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Model_of_Nantong_City_in_Nantong_Museum_2013-01.JPG "fig:缩略图")
南通经济技术开发区成立于1984年12月，是中国最早设立的14个国家级开发区之一，区内五大特色鲜明的专业园区包括：电子工业暨数字健康科技园；新材料工业园；海洋工程船舶装备制造工业园；出口加工区；能达商务区。鼓励产业：电子信息、生物医药、软件及集成电路、新材料、新能源、现代服务业。\[10\]

### 苏通科技产业园

苏通科技产业园是江苏省沿海开发和跨江联动开发的重点项目，是苏州、南通两市跨江联动开发、推动区域共同发展的合作园区，是苏州工业园区成功经验推广辐射的创新之区。园区规划面积50平方公里，一期开发面积9.5平方公里。综合分析苏通科技产业园，可概括为“区位独特，规划一流，开发科学，产业先进，生态优先”五个特点。\[11\]

### 南通崇川经济开发区

江苏南通崇川经济开发区成立于1993年，位于南通主城区——崇川区。总规划面积25.86平方公里，分为东、南两区崇川经济开发区主导产业包括：数字视讯电子、新能源、船舶工业研发、现代物流、服务外包。主要特色园区有：节能环保园、海工机电园、电子信息园、科技创意园、塑胶工业园、汽车产贸园、清之华园软件园等。\[12\]

### 南通港闸经济开发区

江苏省南通港闸经济开发区位于南通市区西北翼，是1993年江苏省人民政府批准设立的省级开发区，是南通北翼新城的重要现代工业基地和外资集聚高地，也是全省沿江开发的十五大园区之一。现规划面积40平方公里。\[13\]

## 交通

南通已形成较便捷的水陆空立体交通网络。

### 航空

南通地区最早的机场，可追溯至1936年建成的段家坝飞机场。1930年，[江苏省政府要求](../Page/江苏省政府.md "wikilink")[南通县报备飞机场](../Page/南通县.md "wikilink")，南通县长张栋报备狼山路第二体育场为飞机场。1933年，国民政府交通部派人筹建南通机场，这是为南下渡江飞机准备的备用机场。1936年建成，为泥地机场，仅起降小型飞机。为[教练机飞行训练使用](../Page/教练机.md "wikilink")，偶有[运输机起降](../Page/运输机.md "wikilink")。机场在1949年后荒废，可能沿用至1957年后。1950、70年代曾修建过两个农用飞机场地，即通州三余农用飞机场、南通农场农用飞机场\[14\]。[江苏省交通厅在](../Page/江苏省交通厅.md "wikilink")1950年代末在南通县晏复公社投资兴建了兴仁飞机场（有称兴东机场\[15\]），开通至南京的航线，1962年关闭\[16\]\[17\]。

1964年开工建设了军用机场——[如皋机场](../Page/如皋机场.md "wikilink")，曾改为[军民两用机场](../Page/军民两用机场.md "wikilink")，现仍做为军用机场使用\[18\]。1983年，[地市合并后](../Page/地市合并.md "wikilink")，南通市人民政府在次年提交报告，要求在南通县兴东镇兴建机场。1990年，获得[国家计委批准](../Page/国家计委.md "wikilink")\[19\]，最终，[南通兴东机场在](../Page/南通兴东机场.md "wikilink")1992年底竣工。机场位于今南通市东北方向，通州区[兴东街道境内](../Page/兴东街道_\(南通市\).md "wikilink")，距南通市区约9.8公里，占地面积约3000亩，1993年投入使用，距离上海约100公里，在机场西南端3.5公里处有宁启高速和沿海高速入口，贯通苏通大桥和崇启大桥。地面交通设施较为发达，出入机场比较便捷\[20\]。2000年，[启东直升机场开通至上海的航线](../Page/启东直升机场.md "wikilink")，但在两年后停运\[21\]。

### 公路

有长途客运班车通往省内大部分和省外主要城市。世界第一大跨径双塔双索面[斜拉桥](../Page/斜拉桥.md "wikilink")[苏通长江大桥和盐通](../Page/苏通长江大桥.md "wikilink")[高速公路已建成](../Page/高速公路.md "wikilink")，已于2008年4月通车。市内有几十条[公交线路](../Page/公交.md "wikilink")。自从70年代以来南通的经济发展一直受制于陆路交通，尤其是与长江南岸的经济区之间的交通仅维系于[轮渡和水运](../Page/轮渡.md "wikilink")，因此加大了[苏南](../Page/苏南.md "wikilink")[苏北在近](../Page/苏北.md "wikilink")30年的经济差异。而目前南通正致力于水陆空交通网的建设，尤其是苏通长江大桥以及经[崇明达](../Page/崇明.md "wikilink")[上海的火车线路的开通必将大大推动未来南通经济的发展](../Page/上海.md "wikilink")。

### 铁路

[宁启铁路和](../Page/宁启铁路.md "wikilink")[新长铁路支线交汇于](../Page/新长铁路.md "wikilink")[海安](../Page/海安.md "wikilink")，有客运火车通往[宁启铁路沿线](../Page/宁启铁路.md "wikilink")[泰州](../Page/泰州.md "wikilink")、[扬州](../Page/扬州.md "wikilink")、[南京和](../Page/南京.md "wikilink")[新长铁路沿线](../Page/新长铁路.md "wikilink")[淮安等城市](../Page/淮安.md "wikilink")，目前已开辟南通至北京的直达特快专线，以及至[重庆](../Page/重庆.md "wikilink")、[温州](../Page/温州.md "wikilink")、[太原](../Page/太原.md "wikilink")、[衢州的快速客运线路](../Page/衢州.md "wikilink")，跨越长江的沪通高铁目前在建，预计2019年通车。[南通火车站自](../Page/南通火车站.md "wikilink")2005年起开始扩建。

### 轨道交通

据最新消息，南通未来将会有8条地铁线路贯通南通市区与县区，目前[南通地铁一号线已全面开工](../Page/南通地铁.md "wikilink")，预计2020年试运行，2022年正式开通。2号线也在建罗密鼓的筹划中，预计二号线将于2018年底开建。

### 水运

[南通港是长江下游重要港口](../Page/南通港.md "wikilink")，现有[天生](../Page/天生.md "wikilink")、南通、[狼山](../Page/狼山.md "wikilink")3个港区和[青龙](../Page/青龙.md "wikilink")、[启东两个港站](../Page/启东.md "wikilink")。南通港现有码头岸线总长4110.8米，有万吨级以上货运码头7座（内货主3座），千吨级以上货运码头19座（内货主15座），客运码头5座（内货主3座）港作船和其他非生产性码头5座，内河1000吨级以下码头18座。另有装卸平台2座，1座为卸油平台，1座为卸矿平台，浮筒泊位10个。

## 旅游景点

[狼山风景区](../Page/狼山_\(江苏\).md "wikilink")、[濠河风景区](../Page/濠河.md "wikilink")、[张謇纪念馆](../Page/张謇.md "wikilink")、[军山](../Page/军山.md "wikilink")、[啬园](../Page/啬园.md "wikilink")、蛎岈山生态旅游、三星绣品城、圆陀角风景区、[吕四风情区](../Page/吕四.md "wikilink")、[徽派园林孤本水绘园](../Page/徽派园林.md "wikilink")、[南通博物苑](../Page/南通博物苑.md "wikilink")、千年古刹[定慧禅寺](../Page/定慧禅寺.md "wikilink")

| <font color="#000000">南通的全国重点文物保护单位</font>            |
| ----------------------------------------------------- |
| [南通博物苑](../Page/南通博物苑.md "wikilink")                  |
| [水绘园](../Page/水绘园.md "wikilink")（如皋）                  |
| **南通的世界级人类口头非物质文化遗产**                                 |
| [南通仿真绣](../Page/南通仿真绣.md "wikilink")（属“中国蚕桑丝织技艺”项目）   |
| [南通缂丝织造技艺](../Page/南通缂丝.md "wikilink")（属“中国蚕桑丝织技艺”项目） |
| [南通梅庵琴派](../Page/南通梅庵琴派.md "wikilink")（属“中国古琴艺术”项目）   |
| **南通的国家级非物质文化遗产**                                     |
| [古琴艺术](../Page/古琴艺术.md "wikilink")                    |
| [南通蓝印花布印染技艺](../Page/南通蓝印花布印染技艺.md "wikilink")        |
| [童子戏](../Page/童子戏.md "wikilink")（南通，通州）               |

方特乐园将落户[南通](../Page/南通.md "wikilink")，同时南通正在兴建“近代第一城”影视基地。
[缩略图](https://zh.wikipedia.org/wiki/File:Nantong_Confucian_Temple_02_2013-01.JPG "fig:缩略图")\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Temple_of_Mystery_of_Nantong_01_2013-01.JPG "fig:缩略图")現作為南通文物商店\]\]

## 文化

### 方言

南通地区方言极其复杂，这和南通地区逐渐由水成陆，各时期接受了各地移民的历史有关。

  - 西部、北部的海安、如皋以及如东的大部分地区的方言属于[江淮官话](../Page/江淮官话.md "wikilink")[通泰片](../Page/通泰片.md "wikilink")，其中可以分如东东部地区的如东口音和其他地区的如海口音两种，可以分别称为如东话和如海话。
  - 南通市区及通州区中西部的方言为[南通话](../Page/南通话.md "wikilink")，也常被归入江淮官话通泰片，但和前者几乎无法交流（故也有人认为应该单独成片），实际上南通话是吴方言毗陵片向江淮方言的过渡方言。
  - 通州区东部主要说另外两种[吴語方言](../Page/吴語方言.md "wikilink")，[通东话](../Page/通东话.md "wikilink")（亦可叫古常州语，启海人称江北话）和[金沙话](../Page/金沙话.md "wikilink")，其中金沙话是通东话和南通话的过渡方言，和南通话可以互通，语言学家顾黔认为金沙话属南通话系统，金沙话和其他二种[吴语很难交流](../Page/吴语.md "wikilink")，同时通东话和启海话（沙地话）也很难交流。
  - 东部启东海门的南部以及如东的部分东部沿海地区、通州部分地区主要说属于吴语苏沪嘉小片的[启海话](../Page/启海话.md "wikilink")（本地称
    沙地话）。
  - 启东海门的北部和通州东部二甲东社一部分地区说通东话。通东话被归入吴语毗陵小片，是南通东部最初的方言的继承者。
  - 南通话形成于古壶豆洲上是江南吴語和淮語（江淮官話）的混全体，南通话浊音消失，金沙话保留浊流，通东话主要由吴語毗陵片和基它方言组成，通东话形成于东部洲上，沙地话（启海南部）人群主要由清代中后期江南各地移民组成，沙地话和江南各地方言能互通。

在上述各种方言交汇的地区，一个人会多种方言，交流中使用对方方言母语的情形并不罕见。

### 长寿之乡

[如皋](../Page/如皋.md "wikilink")，长寿之乡

### 遊戲

[南通長牌](../Page/南通長牌.md "wikilink")，掼蛋（牌类游戏）

## 特产

  - 石港[烤虾](../Page/烤虾.md "wikilink")
  - 白蒲[茶干](../Page/茶干.md "wikilink")
  - 水明楼[黄酒](../Page/黄酒.md "wikilink")
  - 嵌桃[麻糕](../Page/麻糕.md "wikilink")
  - 正场熏糕
  - 西亭[脆饼](../Page/脆饼.md "wikilink")
  - 南通[风筝](../Page/风筝.md "wikilink")
  - [南通缂丝？](../Page/南通缂丝？.md "wikilink")
  - 南通[盆景](../Page/盆景.md "wikilink")
  - [蓝印花布](../Page/蓝印花布.md "wikilink")
  - [红木](../Page/红木.md "wikilink")[雕刻](../Page/雕刻.md "wikilink")
  - [薄荷脑](../Page/薄荷脑.md "wikilink")
  - [王氏保赤丸](../Page/王氏保赤丸.md "wikilink")
  - [季德胜蛇药](../Page/季德胜蛇药.md "wikilink")
  - [石港乳腐](../Page/石港乳腐.md "wikilink")
  - [刘桥菜刀](../Page/刘桥菜刀.md "wikilink")
  - [刘桥南通长牌](../Page/刘桥南通长牌.md "wikilink")
  - [兴仁猪头肉](../Page/兴仁猪头肉.md "wikilink")
  - [金沙茶食（寸金糖等）](../Page/金沙茶食（寸金糖等）.md "wikilink")
  - [狼山鸡](../Page/狼山鸡.md "wikilink")
  - [南通鹞子](../Page/南通鹞子.md "wikilink")
  - [通绣](../Page/通绣.md "wikilink")
  - [海安糯米陈酒](../Page/海安糯米陈酒.md "wikilink")
  - [海安冰雪酒](../Page/海安冰雪酒.md "wikilink")
  - 铁板文蛤

## 教育

### 小学

**崇川区**

  - 江苏省南通师范学校第一附属小学
  - 江苏省南通师范学校第二附属小学
  - 南通师范第三附属小学
  - 南通市实验小学
  - 南通市虹桥小学校
  - 城中小学
  - 南通市城南小学校
  - 新桥小学
  - 南通市郭里园新村小学校
  - 南通市城西小学校
  - 南通市城西小学校天元校区
  - 南通市城西小学校城港校区
  - 南通市虹桥第二小学
  - 虹桥三小
  - 南通市西郊小学校
  - 南通市陆洪闸小学校
  - 南通市三里墩小学校
  - 南通市文亮小学
  - 南通市钟秀小学
  - 南通市五山小学
  - 五山教学点
  - 南通市八一小学
  - 南通市文华小学
  - 南通市韩湘坝小学
  - 南通市雁滨小学
  - 南通市红旗小学
  - 南通市启秀小学
  - 南通市跃龙桥小学

**港闸区**

  - 实验小学
  - 二三工小(已与唐闸小学合并)
  - 唐闸小学
  - 永兴小学
  - 曙光小学
  - 十里坊小学
  - 五接桥小学
  - 龙潭小学
  - 九圩港小学
  - 五里树小学
  - 陈桥小学
  - 鹤涛小学
  - 沿河桥小学
  - 费桥小学

**开发区**

  - 南通经济技术开发区实验小学
  - 南通市小海小学
  - 南通市竹行小学
  - 南通市江海小学

**南通滨海园区**

  - 三余小学
  - 恒兴小学
  - 海晏小学
  - 东余小学
  - 东凌小学
  - 北兴桥小学

**海门**

  - 海门实验小学

### 中学

  - [江苏省南通中学](../Page/江苏省南通中学.md "wikilink")
  - [江苏省如皋中学](../Page/江苏省如皋中学.md "wikilink")
  - [江苏省启东中学](../Page/江苏省启东中学.md "wikilink")
  - [江苏省海安高级中学](../Page/江苏省海安高级中学.md "wikilink")
  - [江苏省如东高级中学](../Page/江苏省如东高级中学.md "wikilink")
  - [江苏省海门中学](../Page/江苏省海门中学.md "wikilink")
  - [江苏省通州高级中学](../Page/江苏省通州高级中学.md "wikilink")（1994年前称之为[南通县高级中学](../Page/南通县高级中学.md "wikilink")）
  - [江苏省南通第一中学](../Page/江苏省南通第一中学.md "wikilink")
  - [江苏省白蒲高级中学](../Page/江苏省白蒲高级中学.md "wikilink")
  - [南通市第三中学](../Page/南通市第三中学.md "wikilink")
  - [南通市启秀中学](../Page/南通市启秀中学.md "wikilink")
  - [江苏省海安曲塘中学](../Page/江苏省海安曲塘中学.md "wikilink")
  - [江苏省平潮高级中学](../Page/江苏省平潮高级中学.md "wikilink")
  - [南通市新桥中学](../Page/南通市新桥中学.md "wikilink")
  - [南通市天星湖中学](../Page/南通市天星湖中学.md "wikilink")
  - [南通市天生港中学](../Page/南通市天生港中学.md "wikilink")
  - [南通市第二中学](../Page/南通市第二中学.md "wikilink")
  - [南通市西公园中学](../Page/南通市西公园中学.md "wikilink")（2005年4月并入[南通市启秀中学后改西公园校区](../Page/南通市启秀中学.md "wikilink")）
  - [南通市第一初级中学](../Page/南通市第一初级中学.md "wikilink")
  - [南通大学附属中学](../Page/南通大学附属中学.md "wikilink")（原南通市启秀中学高中部）
  - 通州[兴仁高级中学](../Page/兴仁高级中学.md "wikilink")
  - [江苏省西亭高级中学](../Page/江苏省西亭高级中学.md "wikilink")
  - [南通市小海中学](../Page/南通市小海中学.md "wikilink")
  - [通州通海高级中学](../Page/通州通海高级中学.md "wikilink")
  - [通州二爻高级中学](../Page/通州二爻高级中学.md "wikilink")
  - [通州石港高级中学](../Page/通州石港高级中学.md "wikilink")
  - [通州金沙高级中学](../Page/通州金沙高级中学.md "wikilink")
  - [通州姜灶高级中学](../Page/通州姜灶高级中学.md "wikilink")
  - [通州刘桥高级中学](../Page/通州刘桥高级中学.md "wikilink")
  - [通州二甲高级中学](../Page/通州二甲高级中学.md "wikilink")
  - [江苏省栟茶中学](../Page/江苏省栟茶中学.md "wikilink")

[南通中学.JPG](https://zh.wikipedia.org/wiki/File:南通中学.JPG "fig:南通中学.JPG")

### 高校

  - [南通大学](../Page/南通大学.md "wikilink")：2004年5月由原南通医学院、南通工学院（原[南通纺织工学院](../Page/南通纺织工学院.md "wikilink")）、南通师范学院合并组建而成的综合性大学。
  - 南通职业大学（2004年2月[南通技师学院并入](../Page/南通技师学院.md "wikilink")）
  - 江苏工程职业技术学院（2014年南通纺织职业技术学院更名）
  - 南通航运职业技术学院
  - 南通开放大学（江苏城市职业学院南通办学点，原南通广播电视大学）
  - 南通师范高等专科学校(2014年由南通高等师范学校升格改名)
  - [南通理工学院](../Page/南通理工学院.md "wikilink")（原紫琅职业学院）
  - 南通科技职业学院（原南通农业职业技术学院）
  - 南通商贸高等职业学校（前身为[南通供销学校](../Page/南通供销学校.md "wikilink")）
  - 江苏电大通州学院（前身为南通广播电视大学通州分院）

## 名人

### 学术界

#### 中国科学院院士

  - [保铮](../Page/保铮.md "wikilink")，雷达及电子专家
  - [蔡金涛](../Page/蔡金涛.md "wikilink")，电讯工程学家
  - [管惟炎](../Page/管惟炎.md "wikilink")，物理学家
  - [胡济民](../Page/胡济民.md "wikilink")，理论核物理学家
  - [黄耀曾](../Page/黄耀曾.md "wikilink")，化学家
  - [李大潜](../Page/李大潜.md "wikilink")，数学家
  - [马瑾](../Page/马瑾.md "wikilink")，构造物理与构造地质学家
  - [沈其韩](../Page/沈其韩.md "wikilink")，地质学家
  - [施雅风](../Page/施雅风.md "wikilink")，地理学、冰川学家
  - [王曦](../Page/王曦.md "wikilink")
  - [魏建功](../Page/魏建功.md "wikilink")
  - [徐冠仁](../Page/徐冠仁.md "wikilink")，核农学家
  - [闵乃本](../Page/闵乃本.md "wikilink")，物理学家，材料学家
  - [严志达](../Page/严志达.md "wikilink")，数学家
  - [杨乐](../Page/杨乐.md "wikilink")，数学家
  - [印象初](../Page/印象初.md "wikilink")，昆虫学家
  - [袁翰青](../Page/袁翰青.md "wikilink")，有机化学家、化学史家
  - [王贻芳](../Page/王贻芳.md "wikilink")，实验粒子物理学家

#### 中国工程院院士

  - [韩德馨](../Page/韩德馨.md "wikilink")，煤炭资源与勘查专家
  - [黄先祥](../Page/黄先祥.md "wikilink")，导弹发射及运用技术专家
  - [林祥棣](../Page/林祥棣.md "wikilink")
  - [吴慰祖](../Page/吴慰祖.md "wikilink")
  - [杨裕生](../Page/杨裕生.md "wikilink")，核试验技术、分析化学专家
  - [姚穆](../Page/姚穆.md "wikilink")
  - [周成虎](../Page/周成虎.md "wikilink")，地理信息、遥感专家

#### 其他

  - [闵乃大](../Page/闵乃大.md "wikilink")，中国计算机科学先驱，德籍华裔
  - [易作霖](../Page/易作霖.md "wikilink")，民国初期著名学者、[语言学家](../Page/语言学家.md "wikilink")、[教育家](../Page/教育家.md "wikilink")、[慈善家](../Page/慈善家.md "wikilink")
  - [袁运开](../Page/袁运开.md "wikilink")
  - [庄小威](../Page/庄小威.md "wikilink")

### 文艺界

  - [范楊](../Page/范楊.md "wikilink")，[国画家](../Page/国画.md "wikilink")
  - [范曾](../Page/范曾.md "wikilink")，国画家、书法家
  - [王个簃](../Page/王个簃.md "wikilink")，国画家
  - [袁运甫](../Page/袁运甫.md "wikilink")，艺术家
  - [袁运生](../Page/袁运生.md "wikilink")，艺术家
  - [赵丹](../Page/赵丹.md "wikilink")，表演艺术家

### 商界

  - [徐旭東](../Page/徐旭東.md "wikilink")，台灣企業家，[遠東集團總裁](../Page/遠東集團.md "wikilink")
  - [徐有庠](../Page/徐有庠.md "wikilink")，台灣遠東集團創辦人
  - [张志熔](../Page/张志熔.md "wikilink")，中国熔盛重工集团控股有限公司、恒盛地产控股有限公司的董事会主席及执行董事
  - [张謇](../Page/张謇.md "wikilink")，清末状元、实业家、教育家，晚清君主立宪派领袖

### 政界

  - [张佑才](../Page/张佑才.md "wikilink")，财政部副部长
  - [曹建明](../Page/曹建明.md "wikilink")，[最高人民检察院检察长](../Page/最高人民检察院.md "wikilink")
  - [顾秀莲](../Page/顾秀莲.md "wikilink")，[全国人大常委会副委员长](../Page/全国人大常委会副委员长.md "wikilink")
  - [李金华](../Page/李金华_\(审计长\).md "wikilink")，[全国政协副主席](../Page/全国政协.md "wikilink")
  - [李盛霖](../Page/李盛霖.md "wikilink")，[中华人民共和国](../Page/中华人民共和国.md "wikilink")[交通运输部部长](../Page/交通运输部.md "wikilink")
  - [刘延东](../Page/刘延东.md "wikilink")，[中共中央政治局委員](../Page/中共中央政治局.md "wikilink")、[國務委員](../Page/國務委員.md "wikilink")
  - [丁薛祥](../Page/丁薛祥.md "wikilink")，[中共中央政治局委員](../Page/中共中央政治局.md "wikilink")、[中共中央办公厅主任](../Page/中共中央办公厅主任.md "wikilink")
  - [徐守盛](../Page/徐守盛.md "wikilink")，[湖南省省委书记](../Page/湖南省.md "wikilink")
  - [张志军](../Page/张志军.md "wikilink")，中共國臺辦主任。

### 军界

  - [陈炳德](../Page/陈炳德.md "wikilink")，[中央军委委员](../Page/中央军委委员.md "wikilink")，[中国人民解放军总参谋长](../Page/中国人民解放军总参谋长.md "wikilink")

### 宗教

  - [聖嚴法師](../Page/聖嚴法師.md "wikilink")，[台灣佛教界人士](../Page/台灣.md "wikilink")，[法鼓山創辦人](../Page/法鼓山.md "wikilink")

### 体育运动员

  - [周玲美](../Page/周玲美.md "wikilink")

<!-- end list -->

  -
    [自行车](../Page/自行车.md "wikilink")（两次打破[世界纪录](../Page/世界纪录.md "wikilink")）

<!-- end list -->

  - [张洁云](../Page/张洁云.md "wikilink")

<!-- end list -->

  -
    1981年第三届[世界杯女子](../Page/世界杯.md "wikilink")[排球比赛](../Page/排球.md "wikilink")[团体冠军](../Page/团体冠军.md "wikilink")

<!-- end list -->

  - [吴健秋](../Page/吴健秋.md "wikilink")

<!-- end list -->

  -
    1984年第十届国际[羽毛球女团锦标赛女团冠军](../Page/羽毛球.md "wikilink")
    1986年第十一届[世界羽毛球锦标赛女团冠军](../Page/世界羽毛球锦标赛.md "wikilink")

<!-- end list -->

  - [殷勤](../Page/殷勤.md "wikilink")

<!-- end list -->

  -
    1985年第四届[世界杯女子排球赛团体冠军](../Page/世界杯女子排球赛.md "wikilink")
    1986年第十届世界女排锦标赛团体冠军

<!-- end list -->

  - [赵剑华](../Page/赵剑华.md "wikilink")

<!-- end list -->

  -
    1987年第七届[世界杯羽毛球赛男团冠军](../Page/世界杯羽毛球赛.md "wikilink")
    1988年第十五届国际男团锦标赛冠军
    1990年第十六届国际男团锦标赛冠军
    1991年第七届世界锦标赛男单冠军

<!-- end list -->

  - [林莉](../Page/林莉_\(游泳員\).md "wikilink")

<!-- end list -->

  -
    1991年第六届[世界游泳锦标赛女](../Page/世界游泳锦标赛.md "wikilink")400m个人混合泳冠军、200m个人混合泳冠军
    1992年世界杯短池总决赛女100m个人混合泳、女200m个人混合泳、400m个人混合泳冠军
    1992年第二十五届[奥运会女](../Page/奥运会.md "wikilink")200m个人混合泳冠军
    1993年世界杯短池游泳赛女200m个人混合泳冠军

<!-- end list -->

  - [葛菲](../Page/葛菲.md "wikilink")

<!-- end list -->

  -
    1995年第九届世界羽毛球锦标赛女团冠军
    1996年第二十六届奥运会女双冠军
    1996年第十六届世界杯羽毛球赛女双冠军
    1997年第五届[苏迪曼杯赛混合团体](../Page/苏迪曼杯.md "wikilink")
    1997年第十届世界锦标赛混合双打、女子双打冠军
    1997年世界杯羽毛球比赛混双、女双冠军
    1998年第十七届[尤伯杯赛女团冠军](../Page/尤伯杯.md "wikilink")
    1999年第六届苏迪曼杯混合团体冠军
    1999年第十一届世锦赛女双冠军
    2000年第二十七届奥运会（悉尼）女双冠军

<!-- end list -->

  - [李菊](../Page/李菊.md "wikilink")

<!-- end list -->

  -
    1997年第四十四届世界锦标赛团体冠军
    1999年第四十五届世锦赛女双冠军
    2000年第四届世界杯女子[单打冠军](../Page/单打.md "wikilink")
    2000年第二十七届奥运会（悉尼）女双冠军
    2001年第四十六届世锦赛女双、团体两枚金牌

<!-- end list -->

  - [黄旭](../Page/黄旭.md "wikilink")

<!-- end list -->

  -
    1997年第三十三届世界体操锦标赛男子团体冠军
    1999年第三十四届世界体操锦标赛男子团体冠军
    2000年第二十七届奥运会（悉尼）男子团体冠军
    2003年第三十六届世界体操锦标赛男子团体冠军
    2008年[第二十九届夏季奥林匹克运动会体操男子团体冠军](../Page/第二十九届夏季奥林匹克运动会.md "wikilink")

<!-- end list -->

  - [季磊](../Page/季磊.md "wikilink")

<!-- end list -->

  -
    1999年第十六届[世界技巧锦标赛男子四人项目的第一套](../Page/世界技巧锦标赛.md "wikilink")、第二套、第三套和全能共四枚金牌

<!-- end list -->

  - [胡欣](../Page/胡欣.md "wikilink")

<!-- end list -->

  -
    2000年第十七届世界技巧锦标赛男子四人项目的第一套、第二套和全能共三枚金牌

<!-- end list -->

  - [陆斌](../Page/陆斌.md "wikilink")

<!-- end list -->

  -
    2002年体操世界杯总决赛跳马冠军
    2004年第十二届世界杯总决赛（英国）跳马金牌

<!-- end list -->

  - [赵婷婷](../Page/赵婷婷.md "wikilink")

<!-- end list -->

  -
    2004年第二十届尤伯杯女团冠军

<!-- end list -->

  - [陈玘](../Page/陈玘.md "wikilink")

<!-- end list -->

  -
    2004年第二十八届奥运会（雅典）男乒双打冠军

：2007年萨格勒布世乒赛男乒双打冠军（陈玘/马琳）

  - [陈若琳](../Page/陈若琳.md "wikilink")

<!-- end list -->

  -
    2006年第十五届世界杯跳水赛女子双人10米跳台冠军
    2008年第二十九届奥运会（北京）女子双人10米跳台冠军

<!-- end list -->

  - [仲满](../Page/仲满.md "wikilink")

<!-- end list -->

  -
    2008年第二十九届奥运会（北京）男子佩剑个人奥运金牌，也是亚洲男子击剑百年来第一枚奥运金牌

### 其他

  - [李昌钰](../Page/李昌钰.md "wikilink")，国际著名刑侦专家
  - [朱戬](../Page/朱戬.md "wikilink")，演员

## 国际友好城市

  - [斯旺西市](../Page/斯旺西.md "wikilink") 1987年4月10日

  - [丰桥市](../Page/丰桥市.md "wikilink") 1987年5月26日

  - [和泉市](../Page/和泉市.md "wikilink") 1993年4月24日

  - [泽西市](../Page/泽西市.md "wikilink") 1996年4月2日

  - [特罗斯多夫市](../Page/特羅斯多夫.md "wikilink") 1997年4月8日

  - [金堤市](../Page/金堤市.md "wikilink") 1997年10月22日

  - [奇维塔韦基亚市](../Page/奇维塔韦基亚.md "wikilink") 1999年12月1日

  - [里穆斯基市](../Page/里穆斯基.md "wikilink") 2003年9月8日

## 参考文献

## 外部链接

  - [南通市政府](http://www.nantong.gov.cn)

{{-}}

[Category:江苏地级市](../Category/江苏地级市.md "wikilink")
[南通](../Category/南通.md "wikilink")
[Category:长江沿岸城市](../Category/长江沿岸城市.md "wikilink")
[Category:中国沿海开放城市](../Category/中国沿海开放城市.md "wikilink")
[Category:长江三角洲城市](../Category/长江三角洲城市.md "wikilink")
[苏](../Category/国家历史文化名城.md "wikilink")
[2](../Category/全国文明城市.md "wikilink")
[苏](../Category/国家卫生城市.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10. [开发区|南通经济技术开发区](http://rightsite.asia/zh-hans/gongyeyuanqu/nantongjingjijizhukaifaqu/)

11. [工业园区|苏通科技产业园](http://rightsite.asia/zh-hans/gongyeyuanqu/sutongkejichanyeyuan/)

12. [开发区|南通崇川经济开发区](http://rightsite.asia/zh-hans/gongyeyuanqu/jiangsunantongchongchuanjingjikaifaqu/)

13. [开发区|南通港闸经济开发区](http://rightsite.asia/zh-hans/gongyeyuanqu/jiangsunantonggangzhajingjikaifaqu/)

14.
15.
16.
17.
18.

19.

20. [南通兴东机场简介](http://www.ntcaac.com/gywm.asp)

21.