**南靖县**（）位于中国[福建省南部](../Page/福建省.md "wikilink")，是[漳州市下辖的一个县](../Page/漳州市.md "wikilink")。面积1962平方公里，人口34万人。

## 历史沿革

「南」者即「地处福建之南」，「靖」者取「安靖」之义，至今已有六百多年历史。

[南北朝](../Page/南北朝.md "wikilink")[梁](../Page/梁.md "wikilink")[天监年间](../Page/天监.md "wikilink")(502年～518年)，析晋安县地置兰水县，今南靖属之。[隋](../Page/隋.md "wikilink")[开皇十二年](../Page/开皇.md "wikilink")(592年)，兰水县并入[龙溪县](../Page/龙溪县.md "wikilink")。

[唐](../Page/唐.md "wikilink")[垂拱二年](../Page/垂拱.md "wikilink")(686年)，析龙溪县南部原[绥安县地域建置漳州](../Page/绥安县.md "wikilink")，辖[漳浦县和怀恩县](../Page/漳浦县.md "wikilink")。今南靖县地域多属漳浦县、[龙溪县](../Page/龙溪县.md "wikilink")。[大历十二年](../Page/大历.md "wikilink")(777年)，[汀州](../Page/汀州.md "wikilink")[龙岩县划归漳州](../Page/龙岩县.md "wikilink")，漳州辖漳浦、龙溪、龙岩3县。

[元](../Page/元.md "wikilink")[至治二年](../Page/至治.md "wikilink")（1322年），析龙溪、龙岩、漳浦交界地域置南胜县（今南靖县、[平和县全境及漳浦县部分](../Page/平和县.md "wikilink")），隶漳州路，县治设于九围矾山东麓（今平和县南胜镇人民政府所在地）。[至元六年](../Page/至元.md "wikilink")（1340年），县治迁至官山（今平和县[小溪镇旧县村](../Page/小溪镇.md "wikilink")）。[至正十六年](../Page/至正.md "wikilink")（1355年），南胜县改名为南靖县，意“南方安靖”，並把县政府迁至靖城。

明[洪武元年](../Page/洪武.md "wikilink")（1368年），漳州路改为漳州府，南靖属之。[正德十三年](../Page/正德.md "wikilink")，划清宁、新安2[里共](../Page/乡里制.md "wikilink")12[图置](../Page/图.md "wikilink")[平和县](../Page/平和县.md "wikilink")。同年，龙溪县划二十一都7图、二十五都5图（今[龙海市](../Page/龙海市.md "wikilink")[程溪镇和漳浦县南浦](../Page/程溪镇.md "wikilink")、马苑、龙溪墟）归南靖县管辖。

[清](../Page/清.md "wikilink")[雍正十三年](../Page/雍正.md "wikilink")（1735年），南靖县居仁里第一图车田总十五保划归漳浦县。

[中华民国初年](../Page/中华民国.md "wikilink")，南靖县先属[西路道](../Page/西路道.md "wikilink")，后属[汀漳道](../Page/汀漳道.md "wikilink")。民国14年（1925年），直接隶属福建省。民国22年，属[龙汀省](../Page/龙汀省.md "wikilink")。民国24年，中西坪、大坪划给漳浦县管辖。民国27年7月，县政府迁至山城。民国35年，划归福建省[第六行政督察区](../Page/行政督察区.md "wikilink")，后又改属第五行政区。

中华人民共和国成立后，南靖县行政区划变动较多。1949年10月20日，原第五行政区改为第六行政区，南靖属之。1954年12月，平和县金竹、葛竹、大岭、北坑划归南靖县。1956年8月5日，[科岭乡都宁头村划归龙岩县](../Page/科岭乡.md "wikilink")。11月6日，[版寮乡杨厝村划归平和县管辖](../Page/版寮乡.md "wikilink")。1957年2月17日，程溪区外云、白云、程溪、塔潭、粗座、人家、官园、洋奎等8个乡划给龙溪县，南浦、马苑、龙溪墟划给漳浦县管辖。同年，华安县蓬莱、迎新、迎富划归南靖县。1960年，龙岩县长塔、泉坑、都宁头划归南靖县管辖。

## 行政区划

南靖县现共辖11个镇，分别是：\[1\] 。

## 人口

南靖县人口主要为[汉族](../Page/汉族.md "wikilink")，1990年的统计表明汉族占总人口的99.83%，此外[畲族占总人口的](../Page/畲族.md "wikilink")0.13%。

大多为[闽南人](../Page/闽南人.md "wikilink")、少數[客家人](../Page/客家人.md "wikilink")，海外华裔3万多人，祖籍南靖的[台灣漢族有](../Page/台灣漢族.md "wikilink")100万人左右，是漳州市重点侨乡和[台灣本省漢族祖籍地之一](../Page/台灣漢人.md "wikilink")。

### 方言

南靖县主要通行[闽南话](../Page/闽南话.md "wikilink")。约有4%的南靖人口使用客家话，主要分布在[梅林镇和](../Page/梅林镇_\(南靖县\).md "wikilink")[书洋镇](../Page/书洋镇.md "wikilink")\[2\]。
人口34.5万

## 交通

南靖段原称漳龙公路，全线完工于民国22年5月20日。1950年改称厦隘线，1985年改称。

南靖境内还有省道山长线等。

[15px](../Page/file:China_Railways.svg.md "wikilink")[龙厦铁路](../Page/龙厦铁路.md "wikilink")

## 经济

南靖县为农业大县，主产有[茶叶](../Page/茶叶.md "wikilink")、[兰花](../Page/兰花.md "wikilink")、[香蕉和](../Page/香蕉.md "wikilink")[食用菌等](../Page/食用菌.md "wikilink")\[3\]，被[中国农学会特产经济委员会授予](../Page/中国农学会.md "wikilink")“中国兰花之乡”、“中国香蕉之乡”称号。2007年4月，天宝香蕉获得[产品地理标志保护](../Page/中国地理标志产品.md "wikilink")。

## 矿产

  - [彩玉石](../Page/彩玉石.md "wikilink")
  - [稀土](../Page/稀土.md "wikilink")
  - [高岭土](../Page/高岭土.md "wikilink")
  - [地热](../Page/地热.md "wikilink")

## 旅游景点

  - [田螺坑土楼群](../Page/田螺坑土楼群.md "wikilink")
  - [云水谣古镇](../Page/云水谣古镇.md "wikilink")
  - [乐土亚热带原始雨林](../Page/乐土亚热带原始雨林.md "wikilink")
  - [鹅仙洞亚热带原始雨林](../Page/鹅仙洞.md "wikilink")
  - [虎伯寮亚热带原始雨林](../Page/虎伯寮.md "wikilink")
  - [文昌塔](../Page/文昌塔.md "wikilink")
  - [道源亭](../Page/道源亭.md "wikilink")
  - [慈济行宫](../Page/慈济行宫.md "wikilink")

## 教育

### 高中

  - [南靖一中](../Page/南靖一中.md "wikilink")
  - [南靖二中](../Page/南靖二中.md "wikilink")
  - [南靖三中](../Page/南靖三中.md "wikilink")
  - [南靖四中](../Page/南靖四中.md "wikilink")
  - [南靖五中](../Page/南靖五中.md "wikilink")（湖美中学）
  - [龙山中学](../Page/龙山中学.md "wikilink")
  - [山城中学](../Page/山城中学.md "wikilink")

## 参考文献

## 外部链接

  - [南靖县政府网站](http://www.fjnj.gov.cn/)
  - [南靖县志](http://www.fjsq.gov.cn/ShowBook.asp?BookName=%C4%CF%BE%B8%CF%D8%D6%BE)
  - [漳臺客家淵源](https://web.archive.org/web/20120111072154/http://big5.chinataiwan.org/zppd/zpgc/200809/t20080918_748604.htm)

[Category:福建省县份](../Category/福建省县份.md "wikilink")

1.
2.  [南靖县志
    卷四十二　方　言](http://www.fjsq.gov.cn:88/ShowText.asp?ToBook=3205&index=2228&)
3.  [南靖县情](http://www.fjnj.gov.cn/ViewNews.asp?NewsId=91)