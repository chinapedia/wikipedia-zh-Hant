**蘭特**（South African
Rand，原符号：R；标准符号：ZAR，又稱**-{zh-cn:锾;zh-tw:蘭特;zh-hk:鍰}-**），為[南非所通用的](../Page/南非.md "wikilink")[貨幣](../Page/貨幣.md "wikilink")。紙幣面額有10、20、50、100、200蘭特。硬幣面值有5、10、20、50分和1、2蘭特、5蘭特。1蘭特=100分。南非兰特由南非储备银行（中央银行）发行。

南非蘭特管理行雙重匯率體系。官方固定匯率為1蘭特兌換定額[美元](../Page/美元.md "wikilink")，由此基礎上行浮動商業匯率，適用於外貿、官方資本轉移及經常項目支付。

## 流通中的貨幣

### 硬幣

  - 10 分
  - 20 分
  - 50 分
  - 1 蘭特
  - 2 蘭特
  - 5 蘭特

### 紙幣

#### 1961年

[缩略图](https://zh.wikipedia.org/wiki/File:South_African_rand_banknote_set_1961.jpg "fig:缩略图")

  - 1 兰特
      - 正面：[扬·范·里贝克](../Page/扬·范·里贝克.md "wikilink")
      - 背面：[獅子](../Page/獅子.md "wikilink")
  - 2 兰特
      - 正面：扬·范·里贝克
      - 背面：獅子
  - 10 兰特
      - 正面：扬·范·里贝克
      - 背面：[船舶](../Page/船舶.md "wikilink")
  - 20 兰特
      - 正面：扬·范·里贝克
      - 背面：[礦](../Page/礦.md "wikilink")

#### 1970年

[缩略图](https://zh.wikipedia.org/wiki/File:South_African_rand_banknote_set_1970.jpg "fig:缩略图")

  - 2 兰特
      - 正面：[扬·范·里贝克](../Page/扬·范·里贝克.md "wikilink")、基礎設施
      - 背面：[採礦](../Page/採礦.md "wikilink")
  - 5 兰特
      - 正面：扬·范·里贝克、[鑽石](../Page/鑽石.md "wikilink")
      - 背面：採礦
  - 10 兰特
      - 正面：扬·范·里贝克、[山龙眼](../Page/山龙眼.md "wikilink")
      - 背面：農業
  - 20 兰特
      - 正面：扬·范·里贝克、格魯特康斯坦
      - 背面：扬·范·里贝克的登陸部隊、[南非国徽](../Page/南非国徽.md "wikilink")
  - 50 兰特
      - 正面：扬·范·里贝克、[獅子](../Page/獅子.md "wikilink")
      - 背面：自然環境

#### 1992年

[缩略图](https://zh.wikipedia.org/wiki/File:South_African_rand_banknote_set_1992.jpg "fig:缩略图")

  - 10 兰特
      - 正面：[犀牛](../Page/犀牛.md "wikilink")
      - 背面：[農業](../Page/農業.md "wikilink")
  - 20 兰特
      - 正面：[大象](../Page/大象.md "wikilink")
      - 背面：[採礦](../Page/採礦.md "wikilink")
  - 50 兰特
      - 正面：[獅子](../Page/獅子.md "wikilink")
      - 背面：[製造業](../Page/製造業.md "wikilink")
  - 100 兰特
      - 正面：[非洲水牛](../Page/非洲水牛.md "wikilink")
      - 背面：[旅遊](../Page/旅遊.md "wikilink")
  - 200 兰特
      - 正面：[豹](../Page/豹.md "wikilink")
      - 背面：[交通和](../Page/交通.md "wikilink")[通訊](../Page/通訊.md "wikilink")

#### 2005年

[缩略图](https://zh.wikipedia.org/wiki/File:South_African_rand_banknote_set_2005.jpg "fig:缩略图")

  - 10 兰特
      - 正面：[犀牛](../Page/犀牛.md "wikilink")
      - 背面：[農業](../Page/農業.md "wikilink")
  - 20 兰特
      - 正面：[大象](../Page/大象.md "wikilink")
      - 背面：[採礦](../Page/採礦.md "wikilink")
  - 50 兰特
      - 正面：[獅子](../Page/獅子.md "wikilink")
      - 背面：[製造業](../Page/製造業.md "wikilink")
  - 100 兰特
      - 正面：[非洲水牛](../Page/非洲水牛.md "wikilink")
      - 背面：[旅遊](../Page/旅遊.md "wikilink")
  - 200 兰特
      - 正面：[豹](../Page/豹.md "wikilink")
      - 背面：[交通和](../Page/交通.md "wikilink")[通訊](../Page/通訊.md "wikilink")

#### 2012年

[缩略图](https://zh.wikipedia.org/wiki/File:South_African_rand_banknote_set_2012.jpg "fig:缩略图")

  - 10 兰特
      - 正面：[纳尔逊·曼德拉](../Page/纳尔逊·曼德拉.md "wikilink")
      - 背面：[犀牛](../Page/犀牛.md "wikilink")
  - 20 兰特
      - 正面：纳尔逊·曼德拉
      - 背面：[大象](../Page/大象.md "wikilink")
  - 50 兰特
      - 正面：纳尔逊·曼德拉
      - 背面：[獅子](../Page/獅子.md "wikilink")
  - 100 兰特
      - 正面：纳尔逊·曼德拉
      - 背面：[非洲水牛](../Page/非洲水牛.md "wikilink")
  - 200 兰特
      - 正面：纳尔逊·曼德拉
      - 背面：[豹](../Page/豹.md "wikilink")

## 匯率

## 註釋

<references group="注" />

## 参考文献

## 外部參考

  - [南非貨幣匯率管理簡史](https://web.archive.org/web/20081011182210/http://www.51766.com/www/detailhtml/1100052151.html)
  - [標準銀行](../Page/標準銀行.md "wikilink")

[Category:南非货币](../Category/南非货币.md "wikilink")
[Category:流通货币](../Category/流通货币.md "wikilink")