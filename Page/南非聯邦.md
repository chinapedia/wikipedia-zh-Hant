**南非联邦**（，，）为現今[南非共和國的前身](../Page/南非.md "wikilink")。1910年5月31日，四个[英国殖民地](../Page/直辖殖民地.md "wikilink")——[开普殖民地](../Page/开普殖民地.md "wikilink")、、及合并成为南非联邦。其包含以前屬於[南非共和國和](../Page/德蘭士瓦共和國.md "wikilink")[奧蘭治自由邦的領土](../Page/奧蘭治自由邦.md "wikilink")。

[第一次世界大战后](../Page/第一次世界大战.md "wikilink")，南非联邦在[国际联盟委任下开始对前](../Page/国际联盟.md "wikilink")[德属西南非洲进行](../Page/德属西南非洲.md "wikilink")[托管](../Page/国际联盟托管地.md "wikilink")。西南非洲事实上为联邦另一个省份，但法理上从未被正式纳入。

與[加拿大和](../Page/加拿大.md "wikilink")[澳大利亞一樣](../Page/澳大利亞.md "wikilink")，南非联邦为[大英帝国的](../Page/大英帝国.md "wikilink")[自治领之一](../Page/自治领.md "wikilink")。于1931年12月11日通过的《[威斯敏斯特法令](../Page/1931年西敏法令.md "wikilink")》使南非成为主权国家。其实行[君主立宪制](../Page/君主立宪制.md "wikilink")，王权由南非总督代表。隨著《》的订立，联邦宣告终结；同年5月31日，南非改行[共和制並暫時退出](../Page/共和制.md "wikilink")[英联邦](../Page/英联邦.md "wikilink")。

## 南非联邦的宪法

### 主要特点

與加拿大和澳大利亚不同，南非联邦是一个单一制国家，而非如其名字那样的一个联邦制国家。原来各个组成南非联邦殖民地的议会都被废除，并由各级省议会取代\[1\]。而且中央成立了参众两院，其成员主要由该国的少数民族选举产生\[2\]。「议会至上」是继承自联合王国的宪法共识，
除了针对特许经营权和通用语言部分（通过数则刚性条款）的程序保障外，法院无法干预议会的决定。

## 參見

  - [南非總督列表](../Page/南非總督列表.md "wikilink")

## 註釋

<references group="lower-alpha" />

## 參考文獻

## 延伸閱讀

  - Beck, Roger B. *The History of South Africa* (Greenwood, 2000).

  - Davenport, Thomas, and Christopher Saunders. *South Africa: A modern
    history* (Springer, 2000).

  - Eze, M. *Intellectual history in contemporary South Africa*
    (Springer, 2016).

  -
  - Ross, Robert. *A Concise History of South Africa* (2009)

  - Thompson, Leonard, and Lynn Berat. *A History of South Africa* (4th
    ed. 2014)

  - Thompson, Leonard. *The Unification of South Africa 1902 – 1910*
    (Oxford UP, 1960).

  - Welsh, Frank. *A History of South Africa* (2000).

## 外部連結

  - [World Statesmen.org: South
    Africa](http://worldstatesmen.org/South_Africa.html#South-Africa)

  -
[Category:南非](../Category/南非.md "wikilink")
[Category:已不存在的非洲國家](../Category/已不存在的非洲國家.md "wikilink")
[Category:已不存在的非洲君主國](../Category/已不存在的非洲君主國.md "wikilink")
[Category:冷戰時期的歷史政權](../Category/冷戰時期的歷史政權.md "wikilink")
[Category:南非歷史](../Category/南非歷史.md "wikilink")
[Category:1910年建立的國家或政權](../Category/1910年建立的國家或政權.md "wikilink")
[Category:1961年終結的國家或政權](../Category/1961年終結的國家或政權.md "wikilink")
[Category:前聯合國會員國](../Category/前聯合國會員國.md "wikilink")

1.
2.