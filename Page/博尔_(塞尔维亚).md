[Bor_-_panorama.jpg](https://zh.wikipedia.org/wiki/File:Bor_-_panorama.jpg "fig:Bor_-_panorama.jpg")
[Serbia_Bor.png](https://zh.wikipedia.org/wiki/File:Serbia_Bor.png "fig:Serbia_Bor.png")
**博尔**（[塞尔维亚语](../Page/塞尔维亚语.md "wikilink")：****）是位于[塞尔维亚东部的](../Page/塞尔维亚.md "wikilink")[博尔州](../Page/博尔州.md "wikilink")[博尔县的城镇](../Page/博尔县.md "wikilink")，这也被称为蒂莫斯卡克拉伊纳地区的主要城镇。根据2011年的人口普查，有34160人（根据1991年的人口普查，共有40668人），并在[扎耶查尔之后这部分塞尔维亚的人口幅度下降最大](../Page/扎耶查尔.md "wikilink")。

**博尔**是[采矿和工业城市](../Page/采矿.md "wikilink")，发展[有色冶金](../Page/有色冶金.md "wikilink")。博尔拥有的[铜矿是](../Page/铜矿.md "wikilink")[欧洲最大的铜矿之一](../Page/欧洲.md "wikilink")。

该城始建于1945年，只有经历于1800年左右的地方。定居规划在[南斯拉夫的熟练劳动力问题](../Page/南斯拉夫.md "wikilink")，因此有不同的种族构成。

## 地理

**博尔**位于蒂莫斯卡克拉伊纳地区而接近与布雷斯托瓦茨克水疗中心，也接近于博尔湖和斯托尔山。博尔拥有的[铜矿是](../Page/铜矿.md "wikilink")[欧洲最大的铜矿之一](../Page/欧洲.md "wikilink")。

**博尔**有一个民用机场，这在近几年来是不可营业。最近的民用机场是[贝尔格莱德和](../Page/贝尔格莱德.md "wikilink")[尼什](../Page/尼什.md "wikilink")。从2009年12月25日始，尼什[君士坦丁大帝国际机场执行定期航班飞往](../Page/君士坦丁大帝国际机场.md "wikilink")[博洛尼亚](../Page/博洛尼亚.md "wikilink")，并有季节性的航班飞往[安塔利亚](../Page/安塔利亚.md "wikilink")，[蒂瓦特和](../Page/蒂瓦特.md "wikilink")[波德戈里察](../Page/波德戈里察.md "wikilink")。尼古拉特斯拉贝尔格莱德国际机场执行航班的目的地多数飞往国外。博尔至尼什和扎耶查尔可通过长达128公里的[欧洲高速公路E](../Page/欧洲高速公路.md "wikilink")771号线前往[克尼亞熱瓦茨](../Page/克尼亞熱瓦茨.md "wikilink")，而博尔至尼什可通过长达192公里欧洲高速公路[E761号线和](../Page/E761号线.md "wikilink")[欧洲E75公路前往](../Page/欧洲E75公路.md "wikilink")[帕拉欽](../Page/帕拉欽.md "wikilink")。**博尔**可通过长达272公里的欧洲高速公路[E761号线和](../Page/E761号线.md "wikilink")[欧洲E75公路前往贝尔格莱德](../Page/欧洲E75公路.md "wikilink")。

**博尔**与扎耶查尔距离约30公里，距[內戈廷约](../Page/內戈廷.md "wikilink")60公里，距[馬伊丹佩克约](../Page/馬伊丹佩克.md "wikilink")120公里，距[克拉多沃约](../Page/克拉多沃.md "wikilink")60公里。

最近的边境口岸有克拉多沃附近的捷尔达普水电1号站的铁门位于罗马尼亚，扎耶查尔附近的CuKα的铁门位于保加利亚。

## 历史

[Bor_Dom_kulture.JPG](https://zh.wikipedia.org/wiki/File:Bor_Dom_kulture.JPG "fig:Bor_Dom_kulture.JPG")
作为[罗马帝国的一部分](../Page/罗马帝国.md "wikilink")，**博尔**是[上默西亚省其中的首都维米纳库姆](../Page/上默西亚省.md "wikilink")（今[科斯托拉茨](../Page/科斯托拉茨.md "wikilink")）的一部分。从第四到六世纪，这城市也是[东罗马帝国](../Page/东罗马帝国.md "wikilink")（[拜占庭帝国](../Page/拜占庭帝国.md "wikilink")）的一部分，这片领土接着改变了许多杰出人才。首先，他们赢得了[格皮德人](../Page/格皮德人.md "wikilink")，并在他们身上一直保持到[保加利亚的征服](../Page/保加利亚.md "wikilink")。然后再次成为拜占庭帝国的一部分，之后成为[匈牙利王国](../Page/匈牙利王国.md "wikilink")。

博尔度假村是在1903年已于乔治·魏弗特建立与开通博尔的铜矿。此后，博尔开始突然发展壮大。在此期间1933至1940年博尔有了新的村庄，新（南部）的殖民地，建立一所医院和一所新学校，而且**博尔**已经发展为欧洲最大的之一。在1931年，博尔曾有4749个居民。
**博尔**这个城市在第二次世界大战之后尽收了现状，在1947年5月30日博尔有11000个居民，而今天它拥有约34000个居民与约20个民族。

## 人口

在**博尔**有30895个成年人，该村平均年龄为37.4岁（男性为36.5岁和女性为38.2岁）。该村有14044户，每户成员的平均数是2.80人。
这个村主要是由[塞尔维亚人](../Page/塞尔维亚人.md "wikilink")（根据2002年的人口普查）居住，但有大量的少数民族的存在。

## 经济

[Dumper_in_Bor.jpg](https://zh.wikipedia.org/wiki/File:Dumper_in_Bor.jpg "fig:Dumper_in_Bor.jpg")
**博尔**在城市的经济基础上来源是基于[采矿业](../Page/采矿业.md "wikilink")。随着采矿业的发展，全镇迅速发展。人口不断增加，发生与博尔相关其他有害工厂的事故（铜丝厂为例）。然后，博尔开发了其他行业（如南斯拉夫联合银行博尔分行）。这个城市的经济顶峰是20世纪80年代，当时有一个担忧，曾在德国和美国设有分公司。
20世纪八九十年代，随着[南斯拉夫社会主义联邦共和国解体的担忧来自于经济困难的形势](../Page/南斯拉夫社会主义联邦共和国.md "wikilink")，其中直到2010年。在2000年至2010年，进行了与国内外其他公司的合作项目一系列的事件是为了达到经济稳定。在同一时期，[国有企业关切的是尝试了两次](../Page/国有企业.md "wikilink")，以通过私有企业具有相同的目标销售集团。在2010年初，该集团仍然由国有企业管理。

## 友好城市

  - [黑山](../Page/黑山.md "wikilink")[巴尔](../Page/巴尔.md "wikilink")

  - [法国](../Page/法国.md "wikilink")[克勒兹](../Page/克勒兹.md "wikilink")

  - [乌克兰](../Page/乌克兰.md "wikilink")[赫梅利尼茨基](../Page/赫梅利尼茨基.md "wikilink")

  - [赞比亚](../Page/赞比亚.md "wikilink")[基特韦](../Page/基特韦.md "wikilink")

  - [保加利亚](../Page/保加利亚.md "wikilink")[弗拉察](../Page/弗拉察.md "wikilink")

## 外部链接

  - [Offical Web Site of the
    Municipality](https://web.archive.org/web/20091125030147/http://www.opstinabor.org.yu/)
  - [Bor 030 - unofficial Web Site](http://www.bor030.net)
  - [Timočka
    Krajina](https://web.archive.org/web/20081231044116/http://www.etos.co.yu/)