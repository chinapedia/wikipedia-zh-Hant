**博尔塔拉蒙古自治州**，简称**博尔塔拉州**、**博州**，是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[新疆维吾尔自治区下辖的](../Page/新疆维吾尔自治区.md "wikilink")[自治州](../Page/自治州.md "wikilink")，位于新疆西北部。面积24,934.33平方公里。[博乐市为州府所在地](../Page/博乐市.md "wikilink")。博尔塔拉係[蒙古语](../Page/蒙古语.md "wikilink")，意为银灰色的草原。博州[汉族人口比例约](../Page/汉族.md "wikilink")65%，[蒙古族人口比例约](../Page/蒙古族.md "wikilink")6%。

## 历史

博尔塔拉曾为[塞人游牧地](../Page/塞迦.md "wikilink")。以后，北方[遊牧民族活动区域](../Page/遊牧民族.md "wikilink")。

[汉朝属](../Page/汉朝.md "wikilink")[西域都護府](../Page/西域都護府.md "wikilink")。[唐朝为](../Page/唐朝.md "wikilink")[西突厥游牧地](../Page/西突厥.md "wikilink")，朝廷在此地设[双河都督府](../Page/雙河都督府.md "wikilink")。[元](../Page/元朝.md "wikilink")[明期間为](../Page/明朝.md "wikilink")[卫拉特蒙古游牧地](../Page/瓦剌.md "wikilink")。

[清朝](../Page/清朝.md "wikilink")[乾隆年间](../Page/乾隆.md "wikilink")，从察哈爾调[八旗察哈尔兵丁进驻屯垦戍边](../Page/八旗察哈爾.md "wikilink")，后又有从[伏尔加河流域回归的](../Page/伏尔加河.md "wikilink")[舊土爾扈特部西旗落驻牧于精河](../Page/舊土爾扈特部.md "wikilink")，稱晶土爾扈特。

1954年7月13日成立博尔塔拉蒙古自治区，1955年2月改自治州。

## 地理

位于新疆[准噶尔盆地西南部](../Page/准噶尔盆地.md "wikilink")，北部和西部与[哈萨克斯坦共和国接壤](../Page/哈萨克斯坦.md "wikilink")，边境线长达385公里；东部与[塔城地区](../Page/塔城地区.md "wikilink")[乌苏县](../Page/乌苏.md "wikilink")、东北部与[托里县相连](../Page/托里县.md "wikilink")；南部与[伊犁哈萨克自治州的](../Page/伊犁哈萨克自治州.md "wikilink")[尼勒克县](../Page/尼勒克县.md "wikilink")、[伊宁县](../Page/伊宁县.md "wikilink")、[霍城县三县相邻](../Page/霍城县.md "wikilink")。

## 政治

### 现任领导

<table>
<caption>博尔塔拉蒙古自治州四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党博尔塔拉蒙古自治州委员会.md" title="wikilink">中国共产党<br />
博尔塔拉蒙古自治州<br />
委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/博尔塔拉蒙古自治州人民代表大会.md" title="wikilink">博尔塔拉蒙古自治州<br />
人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/博尔塔拉蒙古自治州人民政府.md" title="wikilink">博尔塔拉蒙古自治州<br />
人民政府</a><br />
<br />
州长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议博尔塔拉蒙古自治州委员会.md" title="wikilink">中国人民政治协商会议<br />
博尔塔拉蒙古自治州<br />
委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/邱树华.md" title="wikilink">邱树华</a>（女）[1]</p></td>
<td><p><a href="../Page/巴德玛拉.md" title="wikilink">巴德玛拉</a>[2]</p></td>
<td><p><a href="../Page/刘自重.md" title="wikilink">刘自重</a>[3]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p><a href="../Page/蒙古族.md" title="wikilink">蒙古族</a></p></td>
<td><p>汉族</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/河南省.md" title="wikilink">河南省</a><a href="../Page/商丘市.md" title="wikilink">商丘市</a></p></td>
<td><p><a href="../Page/新疆维吾尔自治区.md" title="wikilink">新疆维吾尔自治区</a><a href="../Page/温泉县.md" title="wikilink">温泉县</a></p></td>
<td><p><a href="../Page/陕西省.md" title="wikilink">陕西省</a><a href="../Page/旬阳县.md" title="wikilink">旬阳县</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2015年12月</p></td>
<td><p>2016年1月</p></td>
<td><p>2016年1月</p></td>
<td><p>2017年1月</p></td>
</tr>
</tbody>
</table>

### 行政区划

下辖2个[县级市](../Page/县级市.md "wikilink")、2个[县](../Page/县_\(中华人民共和国\).md "wikilink")。

  - 县级市：[博乐市](../Page/博乐市.md "wikilink")、[阿拉山口市](../Page/阿拉山口市.md "wikilink")
  - 县：[精河县](../Page/精河县.md "wikilink")、[温泉县](../Page/温泉县.md "wikilink")

另外有[新疆生产建设兵团农五师及其](../Page/新疆生产建设兵团.md "wikilink")11个团场。

<table>
<thead>
<tr class="header">
<th><p><strong>博尔塔拉蒙古自治州行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[4]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>652700</p></td>
</tr>
<tr class="odd">
<td><p>652701</p></td>
</tr>
<tr class="even">
<td><p>652702</p></td>
</tr>
<tr class="odd">
<td><p>652722</p></td>
</tr>
<tr class="even">
<td><p>652723</p></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>博尔塔拉蒙古自治州各市（县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[5]（2010年11月）</p></th>
<th><p>户籍人口[6]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>博尔塔拉蒙古自治州</p></td>
<td><p>443680</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>博乐市</p></td>
<td><p>235585</p></td>
<td><p>53.10</p></td>
</tr>
<tr class="even">
<td><p>精河县</p></td>
<td><p>141593</p></td>
<td><p>31.91</p></td>
</tr>
<tr class="odd">
<td><p>温泉县</p></td>
<td><p>66502</p></td>
<td><p>14.99</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全州[常住人口为](../Page/常住人口.md "wikilink")443680人\[7\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，10年共增加19640人，增长4.63%。年平均增长率为0.45%。其中，男性人口为229399人，占51.70%；女性人口为214281人，占48.3%。总人口性别比（以女性为100）为107.06。0—14岁人口为79559人，占17.93%；15—64岁人口为332185人，占74.87%；65岁及以上人口为31936人，占7.2%。

### 民族

常住人口中，[汉族人口](../Page/汉族.md "wikilink")288220人，占总人口的64.96%，各[少数民族人口](../Page/少数民族.md "wikilink")155460人，占总人口的35.04%。全州有35个民族成份，[蒙古族是自治民族](../Page/蒙古族.md "wikilink")。超过万人的少数民族有蒙古族、[维吾尔族](../Page/维吾尔族.md "wikilink")、[哈萨克族](../Page/哈萨克族.md "wikilink")、[回族等](../Page/回族.md "wikilink")。

{{-}}

| 民族名称         | [汉族](../Page/汉族.md "wikilink") | [维吾尔族](../Page/维吾尔族.md "wikilink") | [哈萨克族](../Page/哈萨克族.md "wikilink") | [蒙古族](../Page/蒙古族.md "wikilink") | [回族](../Page/回族.md "wikilink") | [东乡族](../Page/东乡族.md "wikilink") | [壮族](../Page/壮族.md "wikilink") | [锡伯族](../Page/锡伯族.md "wikilink") | [藏族](../Page/藏族.md "wikilink") | [土家族](../Page/土家族.md "wikilink") | 其他民族 |
| ------------ | ------------------------------ | ---------------------------------- | ---------------------------------- | -------------------------------- | ------------------------------ | -------------------------------- | ------------------------------ | -------------------------------- | ------------------------------ | -------------------------------- | ---- |
| 人口数          | 288220                         | 59106                              | 44417                              | 25125                            | 23180                          | 1455                             | 345                            | 273                              | 271                            | 267                              | 1021 |
| 占总人口比例（%）    | 64.96                          | 13.32                              | 10.01                              | 5.66                             | 5.22                           | 0.33                             | 0.08                           | 0.06                             | 0.06                           | 0.06                             | 0.23 |
| 占少数民族人口比例（%） | \---                           | 38.02                              | 28.57                              | 16.16                            | 14.91                          | 0.94                             | 0.22                           | 0.18                             | 0.17                           | 0.17                             | 0.66 |

**博尔塔拉蒙古自治州民族构成（2010年11月）**\[8\]

## 经济

2004年全州实现[国内生产总值](../Page/国内生产总值.md "wikilink")36．9亿元(含农五师)，比上年增长11.9%。其中地方生产总值25.5亿元，增长11.7%。全年对外进出口贸易总额为5.54亿美元，比上年增长96.8%。在岗职工年平均工资1.1万元，比上年增长7.6%；农牧民人均纯收入3904元，比上年增加382元，增长10.8%。

[阿拉山口口岸是中国西北地区唯一的](../Page/阿拉山口口岸.md "wikilink")[铁路](../Page/铁路.md "wikilink")、[公路并举的](../Page/公路.md "wikilink")[国家一类口岸](../Page/国家一类口岸.md "wikilink")。过货量占全疆16个口岸总量的90%，在全国陆路口岸中仅次于[滿洲里口岸](../Page/滿洲里.md "wikilink")，已连续8年居全国陆路口岸的第二位。

## 参考文献

{{-}}

[博尔塔拉](../Category/博尔塔拉.md "wikilink")
[Category:新疆自治州](../Category/新疆自治州.md "wikilink")
[新](../Category/蒙古族自治州.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.