**財團法人博幼社會福利基金會**（簡稱**博幼基金會**），（[英語](../Page/英語.md "wikilink")：BOYO
SOCIAL WELFARE
FOUNDATION）是[台灣一個](../Page/台灣.md "wikilink")[慈善](../Page/慈善.md "wikilink")[基金會](../Page/基金會.md "wikilink")，由前[暨大校長](../Page/國立暨南國際大學.md "wikilink")[李家同於](../Page/李家同.md "wikilink")2002年在[南投縣](../Page/南投縣.md "wikilink")[埔里鎮創立](../Page/埔里鎮.md "wikilink")。原名「財團法人南投縣博幼社會福利慈善事業基金會」，最初工作內容是聯合南投縣境內[國中](../Page/國中.md "wikilink")、[國小](../Page/國小.md "wikilink")、[教會以及暨大](../Page/教會.md "wikilink")[學生](../Page/學生.md "wikilink")，提供南投縣弱勢[家庭學童](../Page/家庭.md "wikilink")[課後輔導](../Page/課後輔導.md "wikilink")，目前已將服務範圍擴張至全台各地。另外也有與[TVBS合作](../Page/TVBS.md "wikilink")「山中[流動圖書館](../Page/流動圖書館.md "wikilink")」計畫。

## 外部連結

  - [財團法人博幼社會福利基金會](http://www.boyo.org.tw/)

  -
## 參考

  - [李家同：提升全民教育，邀您一起改變台灣！](http://magazine.chinatimes.com/moneyweekly/20160905005078-300201)
  - [李家同博幼基金會15年 畢業生脫貧回偏鄉課輔](https://udn.com/news/story/6886/2430258)

[Category:臺灣兒童慈善組織](../Category/臺灣兒童慈善組織.md "wikilink")
[Category:臺灣教育組織](../Category/臺灣教育組織.md "wikilink")
[Category:台灣基金會](../Category/台灣基金會.md "wikilink")
[Category:2002年建立的組織](../Category/2002年建立的組織.md "wikilink")
[Category:南投縣組織](../Category/南投縣組織.md "wikilink")
[Category:埔里鎮](../Category/埔里鎮.md "wikilink")