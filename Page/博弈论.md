**賽局理論**（），又譯為**对策论**，或者**-{zh-hans:赛局理论;zh-hk:賽局理論;zh-tw:博弈論;}-**，[经济学的一个分支](../Page/经济学.md "wikilink")，1944年[馮·諾伊曼與](../Page/馮·諾伊曼.md "wikilink")合著《博弈論與經濟行為》，標誌著現代系統博弈理論的的初步形成，因此他被稱為「博弈論之父」。博弈論被認為是20世紀經濟學最偉大的成果之一。目前在[生物学](../Page/生物学.md "wikilink")、[经济学](../Page/经济学.md "wikilink")、[国际关系](../Page/国际关系.md "wikilink")、[计算机科学](../Page/计算机科学.md "wikilink")、[政治学](../Page/政治学.md "wikilink")、[军事战略和其他很多学科都有广泛的应用](../Page/军事战略.md "wikilink")。主要研究公式化了的激励结构（**游戏**或者**博弈**）间的相互作用。是研究具有斗争或竞争性质现象的数学理论和方法。也是[運籌學的一个重要学科](../Page/運籌學.md "wikilink")。

## 概述

博弈论考虑游戏中的个体的预测行为和实际行为，并研究它们的优化策略。表面上不同的相互作用可能表现出相似的激励结构（incentive
structure），所以它们是同一个游戏的特例。其中一個有名有趣的應用例子是[囚徒困境](../Page/囚徒困境.md "wikilink")。

具有竞争或对抗性质的行为称为博弈行为。在这类行为中，参加斗争或竞争的各方各自具有不同的目标或利益。为了达到各自的目标和利益，各方必须考虑对手的各种可能的行动方案，并力图选取对自己最为有利或最为合理的方案。比如日常生活中的下棋，打牌等。博弈论就是研究博弈行为中斗争各方是否存在着最合理的行为方案，以及如何找到这个合理的行为方案的数学理论和方法。

生物学家使用博弈理论来理解和预测演化（论）的某些结果。例如，John Maynard Smith和George R.
Price在1973年发表于《自然》杂志上的论文中提出的「evolutionarily stable
strategy」的这个概念就是使用了博弈理论。还可以参见[演化博弈理论和行为生态学](../Page/进化博弈理论.md "wikilink")（behavioral
ecology）。

博弈论也应用于数学的其他分支，如[概率](../Page/概率.md "wikilink")、[统计和](../Page/统计.md "wikilink")[线性规划等](../Page/线性规划.md "wikilink")。

## 数学定义

对于“博弈”（game）有不少可以互换的定义。这里给出简短的介绍和相互关系的说明。

### 范式博弈（Normal form game）

范式博弈又被译为[正則形式的博弈](../Page/正則形式的博弈.md "wikilink")、策略型賽局或標準型賽局。

设定\(\mathrm{N}\)是一个「參與者」（players）的集合。对于每一个「參與者」\(i \in \mathrm{N}\)都有一个给定的“策略”集合\(\Sigma\ ^i\)**博弈（游戏）**是一个[函数](../Page/函数.md "wikilink")，定义为：

\[\pi\ : \prod_{i\in \mathrm{N}} \Sigma\ ^i \to \mathbb{R}^\mathrm{N}\]也就是说，如果我们知道了参与者的策略集合是什么，那么就可以有一个实数值与之对应。我们可以把上面的[方程拆成两个方程来进一步把它一般化](../Page/方程.md "wikilink")。一个方程是正則形式（Normal
form
game）的参与者方程，描述策略规定结果的方式。另外一个方程描写参与者对于结果（outcome）集合的偏好（preference）。也就是：

\[\pi\ : \prod_{i \in \mathrm{N}} \Sigma\ ^i \to \Gamma\\]
这里\(\Gamma\\)是游戏（博弈）的*结果集合*（outcome
set）。对于每一个参与者\(i\in \mathrm{N}\)都有一个*偏好函数*（*preference
function*）

\[\nu\ ^i : \Gamma\ \to \mathbb{R}\].

### 展开形式的博弈（Extensive form game）

展开形式的博弈又可译为[擴展形式的博弈](../Page/擴展形式的博弈.md "wikilink")、擴展式賽局或擴展型賽局。

正则形式的定义为数学家们提供了“均衡”（equilibria）问题的研究一个容易使用的表达式。因为它避免了怎么计算“策略”的问题，也就是说游戏是怎么进行的问题。

若要考慮遊戲是如何進行的，展开形式的博弈是一个比较方便的表达式。这个形式与[组合博弈论关系密切](../Page/组合博弈论.md "wikilink")。这个定义通过一个[树的形式给定](../Page/树_\(图论\).md "wikilink")。在树的每一个节点（vertex），不同的参与者选择一个边（edge）。

## 博弈论简史

对于博弈论的研究开始于[恩斯特·策梅洛](../Page/恩斯特·策梅洛.md "wikilink")（1913）、[埃米尔·博雷尔](../Page/埃米尔·博雷尔.md "wikilink")（1921）及[冯·诺伊曼](../Page/冯·诺伊曼.md "wikilink")（1928），后来由[冯·诺伊曼和](../Page/冯·诺伊曼.md "wikilink")[奥斯卡·摩根斯坦](../Page/奥斯卡·摩根斯坦.md "wikilink")（1944，1947）首次將其系统化和形式化（参照Myerson,
1991）。随后[约翰·福布斯·纳什](../Page/约翰·福布斯·纳什.md "wikilink")（1950，1951）利用不动点定理证明了均衡点的存在，为博弈论的一般化奠定了坚实的基础。

[约翰·福布斯·纳什](../Page/约翰·福布斯·纳什.md "wikilink")、[约翰·C·海萨尼及](../Page/约翰·C·海萨尼.md "wikilink")[萊因哈德·澤爾騰因为他们对博弈论的突出贡献而获得](../Page/萊因哈德·澤爾騰.md "wikilink")1994年的[瑞典銀行經濟學獎](../Page/瑞典銀行經濟學獎.md "wikilink")。[罗伯特·J·奥曼](../Page/罗伯特·J·奥曼.md "wikilink")、[肯·宾摩尔](../Page/肯·宾摩尔.md "wikilink")、[戴维·克瑞普斯及](../Page/戴维·克瑞普斯.md "wikilink")[阿里尔·鲁宾斯坦對於博弈论也做出重大貢獻](../Page/阿里尔·鲁宾斯坦.md "wikilink")。

## 博弈分类

博弈的分类根据不同的基准也有不同的分类。一般认为，博弈主要可以分为[合作博弈和](../Page/合作博弈.md "wikilink")[非合作博弈](../Page/非合作博弈.md "wikilink")。它们的区别在于相互发生作用的当事人之间有没有一个具有约束力的协议，如果有，就是合作博弈，如果没有，就是非合作博弈。

从行为的时间序列性，博弈论进一步分为两类：[静态博弈是指在博弈中](../Page/静态博弈.md "wikilink")，参与人同时选择或虽非同时选择但后行动者并不知道先行动者采取了什么具体行动；[动态博弈是指在博弈中](../Page/动态博弈.md "wikilink")，参与人的行动有先后顺序，且后行动者能够观察到先行动者所选择的行动。通俗的理解：「[囚徒困境](../Page/囚徒困境.md "wikilink")」就是同时决策的，属于静态博弈；而棋牌类游戏等决策或行动有先后次序的，属于动态博弈。

按照参与人对其他参与人的了解程度分为[完全信息博弈和](../Page/完全信息博弈.md "wikilink")[不完全信息博弈](../Page/不完全信息博弈.md "wikilink")。完全博弈是指在博弈过程中，每一位参与人对其他参与人的特征、策略空间及收益函数有准确的信息。如果参与人对其他参与人的特征、策略空间及收益函数信息了解的不够准确、或者不是对所有参与人的特征、策略空间及收益函数都有准确的准确信息，在这种情况下进行的博弈就是不完全信息博弈。

目前经济学家们现在所谈的博弈论一般是指非合作博弈，由于合作博弈论比非合作博弈论复杂，在理论上的成熟度远远不如非合作博弈论。非合作博弈又分为：完全信息静态博弈，完全信息动态博弈，不完全信息静态博弈，不完全信息动态博弈。与上述四种博弈相对应的均衡概念为：[纳什均衡](../Page/纳什均衡.md "wikilink")、、[贝叶斯纳什均衡](../Page/贝叶斯纳什均衡.md "wikilink")、精炼贝叶斯纳什均衡（perfect
Bayesian Nash equilibrium）。

博弈论还有很多分类，比如：以博弈进行的次数或者持续长短可以分为有限博弈和无限博弈；以表现形式也可以分为一般型（战略型）或者展开型，等等。

## 博弈论相关概念

  - [纳什均衡](../Page/纳什均衡.md "wikilink")
  - [囚徒困境](../Page/囚徒困境.md "wikilink")
  - [重复博弈](../Page/重复博弈.md "wikilink")
  - [大眾定理](../Page/無名氏定理.md "wikilink")
  - [信息](../Page/信息.md "wikilink")
  - [帕累托最优](../Page/帕累托最优.md "wikilink")
  - [零和博弈](../Page/零和博弈.md "wikilink")
  - [非零和博弈](../Page/非零和博弈.md "wikilink")
  - [微分包含式](../Page/微分包含式.md "wikilink")
  - [拍卖博弈](../Page/拍卖博弈.md "wikilink")

## 参考书目

1.  Harold W. K.(editor), 1997, *Classics in Game theory*, Princeton,
    NJ:Princeton University Press ISBN 0-691-01193-1
2.  Myerson, R., 1991, *Game Theory: Analysis of Conflict*. Cambridge
    and London: Harvard University Press.
3.  Osborne, M. and A. Rubinstein，1994，*A Course in Game Theory*,
    Cambridge and London: The MIT Press.
4.  岡田章,1996,『ゲーム理論』東京：有斐閣 ISBN 4-641-06794-5
5.  金子守 『ゲーム理論と蒟蒻問答』 日本評論社、2003年4月。ISBN 4-535-55288-6
6.  川西諭 『ゲーム理論の思考法』 中経出版、2009年9月。ISBN 978-4-8061-3470-1

<!-- end list -->

1.  Axelrod, Robert: The Evolution of Cooperation, 1985, ISBN
    0-465-02121-2
2.  Axelrod, Robert: The Complexity of Cooperation - Agent-Based Models
    of Competition and Collaboration, 1997, ISBN 0-691-01567-8
3.  Dixit, Avinash K./ Skeath, Susan: Games of Strategy, 1999, ISBN
    0-393-97421-9
4.  Eigen, Manfred / Winkler, Ruthild: Das Spiel, 1976, ISBN
    3-492-02151-4
5.  Hargreaves Heap, Shaun P. / Varoufakis, Yanis: Game Theory - A
    Critical Text, 2004, ISBN 0-415-25095-1
6.  Kelly, Anthony: Decision Making Using Game Theory - An Introduction
    for Managers, 2003, ISBN 0-521-81462-6
7.  Schlee, Welter: Einführung in die Spieltheorie, 2004, ISBN
    3-528-03214-6

## 外部链接

  - [联盟博弈论在通信系统中的应用](http://www.supelec.fr/d2ri/flexibleradio/cours/coalition.pdf)-
    Tutorial written by Prof. Debbah, head of the Alcatel-Lucent Chair
    on flexible radio
  - [Economics and Language](http://arielrubinstein.tau.ac.il/el.html),
    by [Ariel Rubinstein](http://arielrubinstein.tau.ac.il/)
  - [Bargaining and
    Markets](http://ww2.economics.utoronto.ca/osborne/bm/), by [Osborne,
    M.](http://www.economics.utoronto.ca/osborne/) and [A.
    Rubinstein](http://arielrubinstein.tau.ac.il/)

{{-}}

[category:数理经济学](../Page/category:数理经济学.md "wikilink")

[博弈论](../Category/博弈论.md "wikilink")
[Category:社會學方法論](../Category/社會學方法論.md "wikilink")
[Category:数理与定量方法 (经济学)](../Category/数理与定量方法_\(经济学\).md "wikilink")
[Category:人工智能](../Category/人工智能.md "wikilink")