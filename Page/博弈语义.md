**博弈语义**是一种基于[博弈论定义](../Page/博弈论.md "wikilink")[真或](../Page/真.md "wikilink")[有效性等逻辑概念的形式语义](../Page/有效性.md "wikilink")，比如游戏者的赢策略。[保尔·洛伦茨首先在](../Page/保尔·洛伦茨.md "wikilink")1950年代晚期为逻辑引入了博弈语义。此后在逻辑中已经研究了很多不同的博弈语义。博弈语义也已经应用于[编程语言的](../Page/编程语言.md "wikilink")[形式语义](../Page/形式语义.md "wikilink")。

## 直觉主义逻辑，指称语义，线性逻辑

Lorenzen和Kuno
Lorenz的主要动机是为[直觉主义逻辑找到一种博弈论](../Page/直觉主义逻辑.md "wikilink")（他们的术语是"对话式"
*Dialogische
Logik*）语义。[Blass](http://www.math.lsa.umich.edu/~ablass/)首先指出在博弈语义和[线性逻辑之间的联系](../Page/线性逻辑.md "wikilink")。这个路线进一步由[Samson
Abramsky](../Page/Samson_Abramsky.md "wikilink")、[Radhakrishnan
Jagadeesan](../Page/Radhakrishnan_Jagadeesan.md "wikilink")、[Pasquale
Malacaria和独立的由](../Page/Pasquale_Malacaria.md "wikilink")[Martin
Hyland和](../Page/Martin_Hyland.md "wikilink")[Luke
Ong发展](../Page/Luke_Ong.md "wikilink")，对组合性加以特别强调，就是递归的在语法上定义策略。使用博弈语义，上面提及的作者们解决了长期存在的为[可计算函数的编程语言定义](../Page/可计算函数的编程语言.md "wikilink")[完全抽象模型的问题](../Page/完全抽象.md "wikilink")。于是，在博弈语义的基础上，产生了为各种编程语言提供了完全抽象的语义模型，以及由此产生了用软件[模型检测进行软件验证的新的语义导向的方法](../Page/模型检测.md "wikilink")。

## 量词

博弈语义的基础性考虑被[Jaakko
Hintikka和Gabriel](../Page/Jaakko_Hintikka.md "wikilink")
Sandu更加强调，特别是为[Independence
Friendly逻辑](../Page/Independence_Friendly逻辑.md "wikilink")（IF逻辑，更加新近地Information-friendly逻辑）提供语义。IF逻辑是带有[分枝量词的逻辑](../Page/分枝量词.md "wikilink")，一度，[组合原则被认为对IF逻辑不成立](../Page/组合原则.md "wikilink")，所以无法利用Tarski的[真理定义为此逻辑定义合适的语义](../Page/真理的语义理论.md "wikilink")。为解决这个问题，量词被赋予博弈论的意义，特别的，这种方法和经典命题逻辑的博弈论语义类似，区别仅在于博弈者并非总是掌握对弈者前面的行动的完全信息。[Wilfrid
Hodges给出了](../Page/Wilfrid_Hodges.md "wikilink")[组合语义](../Page/组合语义.md "wikilink")，并证明了它和IF-逻辑的博弈语义等价。基础性考虑已经推动了其他的工作，比如[Japaridze](http://www.csc.villanova.edu/~japaridz/)的[可计算性逻辑](../Page/可计算性逻辑.md "wikilink")。

## 参见

  - [Independence
    Friendly逻辑](../Page/Independence_Friendly逻辑.md "wikilink")
  - [直觉主义逻辑](../Page/直觉主义逻辑.md "wikilink")
  - [线性逻辑](../Page/线性逻辑.md "wikilink")
  - [可计算性逻辑](../Page/可计算性逻辑.md "wikilink")
  - [交互式计算](../Page/交互式计算.md "wikilink")

## 引用

  - Krabbe, E. C. W., 2001. "Dialogue Foundations: Dialogue Logic
    Revisited," *Supplement to the Proceedings of The Aristotelian
    Society 75*: 33-49.
  - K. Lorenz, P. Lorenzen: *Dialogische Logik*, Darmstadt 1978
  - P. Lorenzen: *Lehrbuch der konstruktiven Wissenschaftstheorie*,
    Stuttgart 2000 ISBN 3-476-01784-2
  - R. Inhetveen: *Logik. Eine dialog-orientierte Einführung.*, Leipzig
    2003 ISBN 3-937219-02-1

## 外部链接

  - [GALOP: Workshop on Games for Logic and Programming
    Languages](http://www.cs.bham.ac.uk/galop)
  - [Stanford Encyclopedia of Philosophy entry on Logic and
    Games](http://plato.stanford.edu/entries/logic-games/)

[Category:語義學](../Category/語義學.md "wikilink")
[Category:计算机逻辑](../Category/计算机逻辑.md "wikilink")