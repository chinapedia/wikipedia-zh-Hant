[PD-2006-06-23-台北市博愛路街照.jpg](https://zh.wikipedia.org/wiki/File:PD-2006-06-23-台北市博愛路街照.jpg "fig:PD-2006-06-23-台北市博愛路街照.jpg")
[Taipei_North_Gate,_2014.JPG](https://zh.wikipedia.org/wiki/File:Taipei_North_Gate,_2014.JPG "fig:Taipei_North_Gate,_2014.JPG")
**博愛路**是位在[台灣](../Page/台灣.md "wikilink")[台北市](../Page/台北市.md "wikilink")[中正區的一條街道](../Page/中正區_\(台北市\).md "wikilink")，今日多被俗稱為「相機街」或「攝影器材專賣街」，附近的[漢口街也是以](../Page/漢口街.md "wikilink")[攝影器材商家的聚集而聞名](../Page/攝影器材.md "wikilink")，兩街又合稱為「博漢區」。因為博愛路的路口為過去[台北府城北門位置](../Page/台北府城北門.md "wikilink")，所以此區也被稱為「北門相機街」。

## 台北府城到台北市都

[Japanese_Soldier_Entering_Taipei(1895).jpg](https://zh.wikipedia.org/wiki/File:Japanese_Soldier_Entering_Taipei\(1895\).jpg "fig:Japanese_Soldier_Entering_Taipei(1895).jpg")
博愛路這條道路早在1880年（[光緒六年](../Page/光緒.md "wikilink")）就已經開闢，不過當時並沒有路名，且台北也尚未建城。台北建城後，該街的路口正對著[北門](../Page/台北府城北門.md "wikilink")，因此被命名為「北門街」；然而也因為北門街是當時[清朝官員進入台北府城後的第一條街道](../Page/清朝.md "wikilink")，所以也是台北府城內最早繁榮興盛的街道之一。1895年，從北門進入台北市的[日軍](../Page/日軍.md "wikilink")，首先經過的街道也是北門街。

到了[日據時代](../Page/台灣日據時期.md "wikilink")1922年（[大正十一年](../Page/大正.md "wikilink")），博愛路一帶依據日本政府的[行政劃分被稱為](../Page/町名改正.md "wikilink")「[京町](../Page/京町.md "wikilink")」，並在1925年（大正十四年）後開始大幅改建此處的街道建築，改成具[文藝復興風格的設計](../Page/文藝復興.md "wikilink")，這也是今日該區仍保有諸多古蹟等級建物的一項原因，例如[台北郵局等](../Page/台北郵局.md "wikilink")。而正式命名為博愛路則是在[中華民國時期才有的改稱](../Page/中華民國.md "wikilink")。

## 需求與地利逐漸成形

博愛路之所以會成為相機街，其實有其地緣因素與都市機能需求。博愛路除了接鄰台北城內的各政府辦公機構與報社外，也與[台北火車站相當靠近](../Page/台北火車站.md "wikilink")，如此的形勢構成了往後攝影器材商家進駐的絕佳位置與吸引條件。到了今日，博愛路一帶已開設了約五十家的攝影器材店。

除了政府機構及火車交通外，另一個促成攝影專賣街的要素是附近的[台北中山堂](../Page/台北中山堂.md "wikilink")，中山堂為當時台北市較大的公眾活動中心，許多民間文化活動都在該處舉辦，而活動也需要拍攝存影留念，如此也就強化博愛路在攝影用品上的就近服務倚賴。一直到[台北國父紀念館落成後](../Page/台北國父紀念館.md "wikilink")，民眾才有比中山堂更大的文化活動舉辦場所。

## 影音相漸融合

[PD-2006-07-25-Bo-ai_Road_Map_Taipei_City.png](https://zh.wikipedia.org/wiki/File:PD-2006-07-25-Bo-ai_Road_Map_Taipei_City.png "fig:PD-2006-07-25-Bo-ai_Road_Map_Taipei_City.png")
若說博愛路是靜態攝影的商店街，那麼與博愛路僅隔一條[延平南路的](../Page/延平南路.md "wikilink")[中華路則是](../Page/中華路_\(台北市\).md "wikilink")[音響的專賣集散地](../Page/音響.md "wikilink")。與博愛路類似的，中華路聚集了數十家的音響店家，由於[群聚效應使博愛路包辦了消費性攝影與專業攝影等產品](../Page/群聚效應.md "wikilink")，而中華路也同樣包辦了消費性音響與專業音響的供應，再加上漢口街，如此形成了一個三角區域。

在這三角區域內，由於科技的不斷進步，靜態攝影也逐漸跨足延伸到動態錄像，使博愛路、漢口街一帶也開始提供消費性錄影、專業錄影等相關設備，而音響也逐漸走向影音合一的共享共娛新趨勢，所以在這個區域內，已經逐漸走向靜態影像、動態影像、聲光影音、專業與消費性等共融的一體性消費區。

## 附註

1.  \-
    在台灣有許多鄉鎮都市都有街道被命名為**博愛路**，例如[高雄市](../Page/高雄市.md "wikilink")、[台東市等](../Page/台東市.md "wikilink")，其他各地也類似，在此難以逐一詳列。

2.  \-[台北畫報](http://www.tpml.edu.tw/blind/cgi/dlpic.php?filename=9208010.txt)

## 沿線設施

  - 國際電化大樓（57號，[國際電化商品總部](../Page/國際電化商品.md "wikilink")）
  - [邱氏鼎食品](../Page/邱氏鼎食品.md "wikilink")（[大黑松小倆口](../Page/大黑松小倆口.md "wikilink")）總部（99號）
  - [臺北市政府警察局中正第一分局博愛路派出所](../Page/臺北市政府警察局中正第一分局.md "wikilink")（119號）
  - [國防部憲兵指揮部憲兵二一一營](../Page/國防部憲兵指揮部.md "wikilink")（123號）
  - [臺灣高等法院刑事庭大廈](../Page/臺灣高等法院.md "wikilink")（127號）
  - [臺灣臺北地方法院](../Page/臺灣臺北地方法院.md "wikilink")、[臺灣臺北地方法院檢察署](../Page/臺灣臺北地方法院檢察署.md "wikilink")（131號）
  - [北美事務協調委員會](../Page/北美事務協調委員會.md "wikilink")（133號）
  - [菊元百貨](../Page/菊元百貨.md "wikilink")（148、150號）
  - [帝國生命會社舊廈](../Page/帝國生命會社舊廈.md "wikilink")（162號）
  - 國防部博愛大樓（164、166號）
  - [中華電信博愛路服務中心](../Page/中華電信.md "wikilink")（168號，原[臺灣總督府電話交換局](../Page/臺灣總督府電話交換局.md "wikilink")）
  - [中華郵政國史館郵局](../Page/中華郵政.md "wikilink")（168號之1）
  - [國防部後備指揮部](../Page/國防部後備指揮部.md "wikilink")、臺北市後備指揮部、[國防安全研究院](../Page/國防安全研究院.md "wikilink")（172號，國防部忠愛營區）

## 相關參見

  - [台北城](../Page/台北城.md "wikilink")
  - [書院町](../Page/書院町.md "wikilink")
  - [台北郵局](../Page/台北郵局.md "wikilink")
  - [台北公會堂](../Page/台北公會堂.md "wikilink")
  - [台北府城北門](../Page/台北府城北門.md "wikilink")
  - [台北府城西門](../Page/台北府城西門.md "wikilink")
  - [捷運西門站](../Page/捷運西門站.md "wikilink")
  - [中華商場](../Page/中華商場.md "wikilink")

## 外部連結

  - [台北相機街](http://camstreet.com.tw) -**（正體中文）**

[B博](../Category/台北市街道.md "wikilink")