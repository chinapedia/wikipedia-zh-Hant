[缩略图](https://zh.wikipedia.org/wiki/File:Boracity.jpg "fig:缩略图")
**博拉馬**（）是一個[索馬利蘭西部的都市](../Page/索馬利蘭.md "wikilink")，隸屬[奧達勒州](../Page/奧達勒州.md "wikilink")[博拉馬區](../Page/博拉馬區_\(索馬利亞\).md "wikilink")，是該州的首府和經濟中心，2001年統計該地約有20萬人口。其位於丘陵上多山環繞的美麗景色已經成為該國的景點之一，當地有著綠草地和原野成為了該國一個重要的動物棲地。

博拉馬同時也是一個教育中心，戰後第一座高等學府－[阿蒙德大學](../Page/阿蒙德大學.md "wikilink")（）即座落於此，該城同時也有著索馬利蘭第一所的聾啞學校。

[Category:索马里城市](../Category/索马里城市.md "wikilink")