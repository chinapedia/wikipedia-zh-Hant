|date_event3 = 1994年 |event_end = 解散 |year_end = 1994年 |date_end =
4月27日 |p1 = 南非 |flag_p1 = Flag of South Africa (1928-1994).svg |s1 =
南非 |flag_s1 = Flag of South Africa.svg |image_flag = Flag of
Bophuthatswana.svg |image_coat = Bophuthatswana COA.png |flag =
|image_map = Bophuthatswana in South Africa.svg |image_map_caption =
博普塔茨瓦纳在南非的位置 |image_map2 = LocationBophuthatswana.svg
|image_map_caption2 = 博普塔茨瓦纳在南非的位置 |capital = 姆马巴托 |title_leader = 領袖
|national_motto = [博普塔茨瓦纳語](../Page/博普塔茨瓦纳語.md "wikilink")：
|national_anthem = [博普塔茨瓦纳語](../Page/博普塔茨瓦纳語.md "wikilink")："Lefatshe
leno la bo-rrarona" <sup>b</sup><small>
[英語](../Page/英語.md "wikilink")："This Land of our Forefathers"</small>

|common_languages = [博普塔茨瓦纳語](../Page/博普塔茨瓦纳語.md "wikilink")（官方語言）
[英文](../Page/英文.md "wikilink") |stat_year1 = 1980年\[1\] |stat_area1 =
44109 |stat_pop1 = 1323315 |stat_year2 = 1991\[2\] |stat_area2 =
|stat_pop2 = 1478950 |religion = |currency =
[南非蘭特](../Page/南非蘭特.md "wikilink") |footnote_a =
[Bophuthatswana](http://flagspot.net/flags/za-bw.html) at Flags of the
World. |footnote_b = [Constitution of the Republic of
Bophuthatswana](http://www.worldstatesmen.org/Bophuthatswana_Constitution.pdf)
as amended in 1984, Schedule 1. |footnote_c = *ibid.*, Chapter 5.
|footnote_d = Appointed. |footnote_e = With or without citizenship. }}

[Bophuthatswana_in_South_Africa.svg](https://zh.wikipedia.org/wiki/File:Bophuthatswana_in_South_Africa.svg "fig:Bophuthatswana_in_South_Africa.svg")
**博普塔茨瓦纳**（），又譯**-{zh-hant:博普塔茨瓦纳;
zh-hans:波布那}-**，[南非白人種族主義者建立的一個](../Page/南非.md "wikilink")[班圖斯坦](../Page/班圖斯坦.md "wikilink")，位於南非北部，由七塊[飛地組成](../Page/飛地.md "wikilink")。面積約40,000
平方公里，1983年人口1,430,000
人，以[茨瓦納族為主](../Page/茨瓦納族.md "wikilink")。首府[姆馬巴托](../Page/姆馬巴托.md "wikilink")。

## 歷史

1971年「自治」，1977年12月6日獨立。1994年重返南非：其中五塊飛地歸[西北省](../Page/西北省_\(南非\).md "wikilink")，一塊
（[塔巴恩丘](../Page/塔巴恩丘.md "wikilink")）歸[自由邦](../Page/自由邦_\(南非\).md "wikilink")，另一塊歸[普馬蘭加省](../Page/普馬蘭加省.md "wikilink")。

## 經濟

有[鈾礦](../Page/鈾.md "wikilink")。南非唯一的賭場[太陽城位於這裡](../Page/太陽城_\(南非\).md "wikilink")。

[Category:已不存在的共和國](../Category/已不存在的共和國.md "wikilink")
[Category:已不存在的非洲國家](../Category/已不存在的非洲國家.md "wikilink")
[Category:南非歷史](../Category/南非歷史.md "wikilink")
[Category:南非班圖斯坦](../Category/南非班圖斯坦.md "wikilink")
[Category:1972年建立](../Category/1972年建立.md "wikilink")

1.
2.