**博爱县**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[河南省](../Page/河南省.md "wikilink")[焦作市下属的一个县](../Page/焦作市.md "wikilink")。位于[太行山南麓](../Page/太行山.md "wikilink")，与[山西省交界](../Page/山西省.md "wikilink")。面积492平方公里，2002年人口42万。[邮政编码](../Page/邮政编码.md "wikilink")454450，县政府驻清化镇。目前下辖：[清化镇街道](../Page/清化镇街道.md "wikilink")、[鸿昌街道](../Page/鸿昌街道.md "wikilink")、[许良镇](../Page/许良镇.md "wikilink")、[月山镇](../Page/月山镇_\(博爱县\).md "wikilink")、[柏山镇](../Page/柏山镇.md "wikilink")、[磨头镇](../Page/磨头镇_\(博爱县\).md "wikilink")、[孝敬镇](../Page/孝敬镇.md "wikilink")、[金城乡和](../Page/金城乡_\(博爱县\).md "wikilink")[寨豁乡](../Page/寨豁乡.md "wikilink")。

## 人口

### 人口迁徙

博爱县人口多来自于[明朝初年的人口迁徙](../Page/明朝.md "wikilink")。[元朝末年的战乱使得博爱县一带的人口锐减](../Page/元朝.md "wikilink")。[朱元璋为使博爱一带的肥沃土地得到耕种](../Page/朱元璋.md "wikilink")，下令使山西省大槐树乡一带的青壮年迁徙至博爱。现在博爱县仍有许多地名保留的当年的移民痕迹\[1\]。

## 名人

### 古代

  - [李商隐](../Page/李商隐.md "wikilink")

<!-- end list -->

  -
    2014年，博爱县民政局组织召开了学术研讨会，专家们经过实事求是的考察与论证，得出结论：李商隐祖籍、祖茔、坟墓在河南省博爱县的许良镇江陵堡村\[2\]。

<!-- end list -->

  - [竹林七贤](../Page/竹林七贤.md "wikilink")

## 参考资料与注释

[博爱县](../Category/博爱县.md "wikilink")
[县](../Category/焦作区县市.md "wikilink")
[焦作](../Category/河南省县份.md "wikilink")

1.
2.