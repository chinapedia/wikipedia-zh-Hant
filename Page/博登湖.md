**博登湖**（），也称**康斯坦茨湖**（），位于[瑞士](../Page/瑞士.md "wikilink")、[奥地利和](../Page/奥地利.md "wikilink")[德国三国交界处](../Page/德国.md "wikilink")，由三国共同管理，湖区景色优美，风景迷人。博登湖由三部分：上湖（Obersee）、下湖（Untersee）和Seerhein组成。

博登湖面积536平方公里，最深处254公尺，是德语区最大的[淡水湖](../Page/淡水湖.md "wikilink")，每年为当地450万居民提供1800万立方公尺饮用水。

## 名字的历史

早在罗马时期，已将“上湖”、“下湖”命名。古老的作家们将“下湖”称为“拉库斯·阿克努斯*Lacus
Acronius*”，“上湖”为“拉库斯·不列坦努斯Lacus
Brigantinus”（凯尔特Brigantiern），“拉库斯·韦内图斯*Lacus Venetus*”（Pomponius
Mela）或者“拉库斯·康斯坦丁努斯*Lacus
Constantinus*”（名来自于小城[康士坦茲](../Page/康士坦茲.md "wikilink")）。中世纪时，称呼为“拉库斯·波达米修斯*Lacus
Bodamicus*”。但其他欧洲语言仍旧保持“拉库斯·康斯坦丁努斯Lacus Constantinus”（法语：Lac de
Constance，英语：Lake Constance，意大利语：Lago di Costanza，葡萄牙语：Lago de
Constança，西班牙语：Lago de Constanza）。“拉库斯·波达米修斯*Lacus
Bodamicus*”称呼被德语（博登湖，Bodensee）及荷兰（Bodenmeer）所接受。何时以及为何将“下湖”（和Seerhein）也称为博登湖，还不得而知。

## 參看

[瑞士湖泊列表](../Page/瑞士湖泊列表.md "wikilink")

[File:Karte_Bodensee.png|博登湖地圖](File:Karte_Bodensee.png%7C博登湖地圖)
<File:Lindau> Lake Constance 002.jpg|德国巴伐利亚博登湖港口入口
<File:Schleienloecher2-1-> C.jpg|奥地利哈尔德的博登湖自然保护区

[Category:萊茵河](../Category/萊茵河.md "wikilink")
[Category:歐洲跨國湖泊](../Category/歐洲跨國湖泊.md "wikilink")
[Category:瑞士湖泊](../Category/瑞士湖泊.md "wikilink")
[Category:德國湖泊](../Category/德國湖泊.md "wikilink")
[Category:奧地利湖泊](../Category/奧地利湖泊.md "wikilink")
[Category:德奧邊界](../Category/德奧邊界.md "wikilink")
[Category:德國-瑞士邊界](../Category/德國-瑞士邊界.md "wikilink")
[Category:奧地利-瑞士邊界](../Category/奧地利-瑞士邊界.md "wikilink")