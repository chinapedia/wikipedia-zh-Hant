**博科尼大学**全称Università Commerciale Luigi Bocconi（英语：Bocconi
University），博科尼大学是意大利第一家教授经济学高等教育的学院。该大学也是世界最重要的商学院之一，它的金融学，法律，政治学和管理学专业在欧洲享有很高的声望。博科尼大学是一家私立大学，但是凭借实行和发扬*机会均等*的教育政策，它的资助计划使得一届又一届的家庭经济劣势的优秀学生能够在这所著名的私立大学深造。它的第一任校长兼教导主任，[莱昻帕多·撒巴蒂尼](../Page/莱昻帕多·撒巴蒂尼.md "wikilink")，一位著名的民权律师，提出了博科尼大学的使命是“促进学术与人民生活的融合”。

## 历史

于1902年在[米兰](../Page/米兰.md "wikilink")（[意大利](../Page/意大利.md "wikilink")）由富商[费迪南多·博科尼捐款创立](../Page/费迪南多·博科尼.md "wikilink")。是为了纪念[费迪南多·博科尼先生的独子在殖民战争的](../Page/费迪南多·博科尼.md "wikilink")[阿多瓦战役中牺牲](../Page/阿多瓦战役.md "wikilink")（[埃塞俄比亚](../Page/埃塞俄比亚.md "wikilink")，1896年5月1日）。
2010年金融时报欧洲商学院排名第10位。2011年金融时报欧洲商学院排名第7位。

## 课程

以下是该大学的本科教育课程名单：

  - 8门关于管理学领域的课程
      - 综合管理
      - 销售管理
      - 组织学和信息科技系统
      - 企业金融和会计控制
      - 公共管理和国际机构管理
      - 金融机构管理
      - 商业法
      - 艺术文化传播管理

<!-- end list -->

  - 2门关于经济学领域的课程
      - 经济社会科学
      - 国际金融和经济管理

<!-- end list -->

  - 1门关于法律领域的课程
      - 法学

博科尼大学，和SDA
Bocconi商学院提供针对企业管理执行层的职业培训课程。该大学为世界工商管理协会[AMBA会员](../Page/Association_of_MBAs.md "wikilink")。

## 參考文獻

<references/>

## 外部链接

  - [Bocconi University Website](http://www.unibocconi.eu) *(English
    homepage)*
  - [SDA Bocconi School of
    Management](http://www.sdabocconi.it/home/en/index.html) *(English
    homepage)*
  - [Bocconi Alumni Association New York
    Website](https://web.archive.org/web/20171007202614/http://www.alumnibocconi.com/)
    *(English homepage)*
  - [Campus Bocconi - Official
    Site](https://web.archive.org/web/20150925123017/http://campus-university.virtualbocconi.com/)
    *(English homepage)*
  - [International Students at Bocconi Website](http://www.isatb.com/)
    Practical information for incoming students *(in English)*
  - [Bocconi Chinese Students Association
    Website](http://www.bocconicsa.com) *(English)*

[Category:意大利大学](../Category/意大利大学.md "wikilink")
[博科尼大学](../Category/博科尼大学.md "wikilink")
[Category:1902年创建的教育机构](../Category/1902年创建的教育机构.md "wikilink")