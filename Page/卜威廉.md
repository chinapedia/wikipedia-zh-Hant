[Williambooth.jpg](https://zh.wikipedia.org/wiki/File:Williambooth.jpg "fig:Williambooth.jpg")
**卜維廉**（**William
Booth**，），[救世軍創始人和第一任](../Page/救世軍.md "wikilink")[救世軍大將](../Page/救世軍大將.md "wikilink")（1878—1912）。

1829年4月10日，卜維廉生於[英國](../Page/英國.md "wikilink")[諾丁漢的一個貧窮家庭](../Page/諾丁漢.md "wikilink")。13歲輟學到一貧民窟中的當鋪當學徒；這令他良心非常不安。不久父親去世。當他與一位表兄接近後，隨他去循道會聽道，隨後開始在貧民窟熱切的從事街頭佈道，勸人離棄罪惡。

1865年，卜維廉夫婦進入倫敦東區碼頭地區的貧民窟傳道，取得極大成功。他於是組織基督徒復興會。1878年正式定名為救世軍，向罪惡、貧困、疾病展開挑戰。他們採用軍事化的管理方式，以及「軍區」、「部隊」、「軍官」、「軍兵」、「戰役」、「募兵」、「軍兵戰條」的稱呼，有制服，軍旗，樂隊，聚會，強調聖潔的生活。軍旗以紅黃藍顏色代表[耶穌基督的](../Page/耶穌基督.md "wikilink")[寶血](../Page/血.md "wikilink")、[聖靈的火與聖潔](../Page/聖靈.md "wikilink")。

1880年，救世軍傳入[美國的](../Page/美國.md "wikilink")[費城](../Page/費城.md "wikilink")，從此迅速發展成為國際性組織，擴展到世界許多國家：[澳大利亞](../Page/澳大利亞.md "wikilink")、[印度](../Page/印度.md "wikilink")、[法國](../Page/法國.md "wikilink")、[加拿大等等](../Page/加拿大.md "wikilink")。1912年8月20日，83歲的卜維廉在[倫敦家中去世](../Page/倫敦.md "wikilink")。4年之後，救世軍進入[中國的](../Page/中國.md "wikilink")[北京](../Page/北京.md "wikilink")。

1912年8月28日，卜維廉的追思聚會在倫敦奧林比亞（Olympia）展覽館舉行，有四萬人趕來參加。[瑪麗王后也在其中](../Page/瑪麗王后_\(英國\).md "wikilink")，她按著次序坐在一個衣冠襤褸的婦人旁邊，那婦人向瑪麗王后述說，她曾是一個妓女，是因著救世軍，才脫離苦海，獲得新生。卜維廉還曾對她說，在天上將有她的位置，正像[抹大拉的馬利亞一樣](../Page/抹大拉的馬利亞.md "wikilink")。

卜維廉去世後，兒子[卜邦衛和女兒卜婉懿曾先後接任救世軍大將](../Page/卜邦衛.md "wikilink")。

## 参考文献

## 參見

  - [救世軍大將](../Page/救世軍大將.md "wikilink")
  - [救世軍](../Page/救世軍.md "wikilink")

{{-}}

[01](../Category/救世军大将.md "wikilink")
[Category:英格蘭新教徒](../Category/英格蘭新教徒.md "wikilink")