<div class="notice metadata" id="disambig" style="font-size:smaller;">

[Nuvola_apps_important.svg](https://zh.wikipedia.org/wiki/File:Nuvola_apps_important.svg "fig:Nuvola_apps_important.svg")
**注意**：本條目名及內文是以常用的誤植漢字來表示寫法近似的日文假名，詳細源由請參考內文。

</div>

**卜字高雄**（，），[新潟縣出身的](../Page/新潟縣.md "wikilink")[日本](../Page/日本.md "wikilink")[演員](../Page/演員.md "wikilink")。本名**平井高雄**的卜字，雖然自1990年起就已參與[電視連續劇的演出](../Page/電視連續劇.md "wikilink")，但卻一直擔任些客串演出的位置，從來沒有真正演出過主角或配角等級的角色。

其藝名中第一個字原本應該是[日文](../Page/日文.md "wikilink")[片假名](../Page/片假名.md "wikilink")「」（To）而非[漢字](../Page/漢字.md "wikilink")「卜」，但因為兩個字極為相似而經常在中文語境中習慣性地被誤植。卜字的本名「平井高雄」在日文中的發音「」（Hiraitakao）與「開朗的面容」（；Hiraita
Kao）發音相同，因此他將「」的反意詞「」（閉上的）作為自己的藝名並取其同音字，而變成「」。

## 演出作品

### 電影

  - 《[死亡筆記本](../Page/死亡筆記本.md "wikilink")》（，2006年）

### 電視劇

  - 《[宛如飛翔](../Page/宛如飛翔.md "wikilink")》（），[NHK](../Page/日本放送協會.md "wikilink")，1990年
  - 《[世界奇妙物語](../Page/世界奇妙物語.md "wikilink")》（），[富士電視台](../Page/富士電視台.md "wikilink")，1991年開始陸續參與演出

<!-- end list -->

  - 《[織田信長](../Page/信長_KING_OF_ZIPANGU.md "wikilink")》（），NHK，1992年
  - 《[隔世情未了](../Page/隔世情未了.md "wikilink")》（），[富士電視台](../Page/富士電視台.md "wikilink")，1992年
  - 《[高校教師](../Page/高校教師.md "wikilink")》，[TBS](../Page/東京廣播公司.md "wikilink")，1993年
  - 《[琉球之風](../Page/琉球之風.md "wikilink")》（），NHK，1993年
  - 《[花梨](../Page/花梨_\(電視劇\).md "wikilink")》（），NHK，1993年至1994年
  - 《[甜蜜家庭](../Page/甜蜜家庭.md "wikilink")》（），TBS，1994年
  - 《[古畑任三郎](../Page/古畑任三郎.md "wikilink")》，第7集，富士電視台，1994年
  - 《[發達之路](../Page/發達之路.md "wikilink")》（），1994年
  - 《[手機刑警
    錢形泪](../Page/手機刑警_錢形泪.md "wikilink")》（），[BS-i](../Page/BS-i.md "wikilink")，2004年
  - 《[美食偵探王](../Page/美食偵探王_\(電視劇\).md "wikilink")》（），[日本電視台](../Page/日本電視台.md "wikilink")，2006年
  - 《[主播台女王](../Page/主播台女王.md "wikilink")》（），2006年 - 飾演 蟹原三郎
  - 《[嘻哈迷情](../Page/嘻哈迷情.md "wikilink")》（），2006年 - 飾演 校長
  - 《[丘比特的惡作劇～虹玉](../Page/丘比特的惡作劇～虹玉.md "wikilink")》（），2006年 - 飾演 睦月清正
  - 《[敬啟，父親大人](../Page/敬啟，父親大人.md "wikilink")》（），2007年
  - 《[求婚大作戰](../Page/求婚大作戰.md "wikilink")》（），第1集，2007年 - 飾演
    [甲子園預賽主審](../Page/甲子園.md "wikilink")

## 外部連結

  - [經紀公司官方網站介紹](https://web.archive.org/web/20130612011514/http://www.kira-boshi.com/prof-tojitakao.html)

[Category:日本男演員](../Category/日本男演員.md "wikilink")
[Category:新潟縣出身人物](../Category/新潟縣出身人物.md "wikilink")