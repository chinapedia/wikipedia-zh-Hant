**卞貞秀**（，），[韓國](../Page/韓國.md "wikilink")[女演員](../Page/女演員.md "wikilink")、[主持人](../Page/主持人.md "wikilink")。妹妹為演員[卞貞敏](../Page/卞貞敏.md "wikilink")。

## 演出作品

### 電視劇

  - 2002年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[射星](../Page/射星.md "wikilink")》
  - 2003年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《我的麻煩男友》
  - 2003年：MBC《前屋女子》
  - 2003年：SBS《[初戀](../Page/初戀_\(2003年電視劇\).md "wikilink")》
  - 2004年：MBC《[想結婚的女子](../Page/想結婚的女子.md "wikilink")》飾演 張勝利
  - 2004年：SBS《妻子的背叛》
  - 2004年：SBS《並非獨自一人》
  - 2005年：[KBS](../Page/韓國放送公社.md "wikilink")《愛情能再觸電嗎？》
  - 2007年：SBS《[不良情侶](../Page/不良情侶.md "wikilink")》飾演 羅多順
  - 2008年：MBC《[我人生最後的緋聞](../Page/我人生最後的緋聞.md "wikilink")》飾演 李娜允
  - 2010年：MBC《[Pasta](../Page/Pasta_\(電視劇\).md "wikilink")》飾演 金江
  - 2011年：[tvN](../Page/tvN.md "wikilink")《[Manny](../Page/型男保姆到我家.md "wikilink")》飾演
    珍妮絲
  - 2011年：MBC《[愛情萬萬歲](../Page/愛情萬萬歲_\(韓國電視劇\).md "wikilink")》飾演申秀晶 ／卞珠莉
  - 2013年：KBS《[她們的完美一天](../Page/她們的完美一天.md "wikilink")》飾演 道恩母
  - 2013年：MBC《[女王的教室](../Page/女王的教室_\(韓國電視劇\).md "wikilink")》飾演 娜莉母
  - 2013年：KBS《[紅寶石戒指](../Page/紅寶石戒指_\(電視劇\).md "wikilink")》飾演 鄭初霖
  - 2014年：MBC《[湔雪的魔女](../Page/湔雪的魔女.md "wikilink")》飾演 馬珠蘭
  - 2015年：SBS《[我有愛人了](../Page/我有愛人了.md "wikilink")》
  - 2015年：MBC《[最佳戀人](../Page/最佳戀人.md "wikilink")》飾演 高興慈
  - 2017年：SBS《[姐姐風采依舊](../Page/姐姐風采依舊.md "wikilink")》飾演 具必順

### 電影

  - 2003年：《[If You Were Me](../Page/If_You_Were_Me.md "wikilink")》
  - 2011年：《[白色：詛咒的旋律](../Page/白色：詛咒的旋律.md "wikilink")》

## 外部連結

  -
  - [EPG](http://epg.epg.co.kr/star/profile/index.asp?actor_id=3659)

  - [官網介紹](https://web.archive.org/web/20160110073018/http://atreemedia.com/menu20/menu1.html)

[B](../Category/韩国女演员.md "wikilink")
[B](../Category/韓國電視演員.md "wikilink")
[B](../Category/韓國電視主持人.md "wikilink")
[B](../Category/暻園大學校友.md "wikilink")
[B](../Category/卞姓.md "wikilink")