**卡亞俄**
（[西班牙語](../Page/西班牙語.md "wikilink")：**Callao**，[當地華人稱之](../Page/秘魯華人.md "wikilink")**介休港**）
是[秘魯最大](../Page/秘魯.md "wikilink")、最重要的港口，與**卡亞俄省**同域，是[卡亞俄大區唯一的](../Page/卡亞俄大區.md "wikilink")[省](../Page/省.md "wikilink")。卡亞俄位於首都[利馬的西邊](../Page/利馬.md "wikilink")，屬於[利馬都會區的一部分](../Page/利馬都會區.md "wikilink")，人口約占秘魯的1/3。面积147km²。

[Category:秘魯城市](../Category/秘魯城市.md "wikilink")
[C](../Category/太平洋沿海城市.md "wikilink")