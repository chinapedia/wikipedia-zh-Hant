**卡利克斯河**（瑞典语：，北萨米语：。在下游地区被称为，上游称为）是[瑞典北部的](../Page/瑞典.md "wikilink")[諾爾蘭的四大河流之一](../Page/諾爾蘭_\(瑞典\).md "wikilink")，有着得天独厚的水力资源。它全长461公里，流向[凯布讷山下的](../Page/凯布讷山.md "wikilink")[基律纳市](../Page/基律纳市.md "wikilink")。往东南方向它流经[拉普兰](../Page/拉普兰.md "wikilink")，然后往南穿过[北博滕省](../Page/北博滕省.md "wikilink")，最后流入[波的尼亚湾](../Page/波的尼亚湾.md "wikilink")。

卡利克斯河是北博滕省第三长河流。第一是[托尔讷河](../Page/托尔讷河.md "wikilink")，全长522公里。卡利克斯河僅比全长460.81公里的[吕勒河略短一些](../Page/吕勒河.md "wikilink")。

## 參考資料

[K](../Category/瑞典河流.md "wikilink")