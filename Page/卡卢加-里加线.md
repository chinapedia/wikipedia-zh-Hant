**卡卢加－里加线**（**6号线**，）为[莫斯科地铁中第](../Page/莫斯科地铁.md "wikilink")5條開通的路线，于1958年投入营运，地图中以橙色线为标识，卡卢加－里加线全长37.6公里，全线一共24车站。

## 历史

6號線原是兩條建於[環狀線圈外的鐵路](../Page/環狀線_\(莫斯科地鐵\).md "wikilink")，分別是1958年通車北方的「里加线」（）和1962年南部啟用的「卡卢加线」（），1970年末里加线伸延進環狀線圈內中區的中國城，與7號線交匯成為莫斯科地鐵第一個[跨月台轉車站](../Page/跨月台轉車站.md "wikilink")，1971年末兩線正式相連貫通南北。

由於路線建於[尼基塔·赫魯曉夫提倡的](../Page/尼基塔·赫魯曉夫.md "wikilink")[去斯大林化時期](../Page/去斯大林化.md "wikilink")，以前在莫斯科地鐵站洋溢的[斯大林式建筑風格不再在](../Page/斯大林式建筑.md "wikilink")6號線的車站復見，與同期通車的[4號線類似都是盡量以節省建築成本和時間為主](../Page/菲利線.md "wikilink")。

## 站名沿革

<table>
<thead>
<tr class="header">
<th><p>現在名稱</p></th>
<th><p>舊名稱</p></th>
<th><p>舊名年期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>国家经济成就展</p></td>
<td><p>泛聯邦農業展（）</p></td>
<td><p>1958-1959</p></td>
</tr>
<tr class="even">
<td><p>阿列克谢</p></td>
<td><p>和平（）</p></td>
<td><p>1958-1966</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/亞歷山大·謝爾巴科夫.md" title="wikilink">謝爾巴科夫</a>（）</p></td>
<td><p>1966-1990</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>和平大道</p></td>
<td><p>植物園（）</p></td>
<td><p>1958-1966</p></td>
</tr>
<tr class="odd">
<td><p>特列季亚科夫</p></td>
<td><p><a href="../Page/新库兹涅茨克.md" title="wikilink">新库兹涅茨克</a>（）</p></td>
<td><p>1970-1983</p></td>
</tr>
<tr class="even">
<td><p>中国城</p></td>
<td><p>廣場（）</p></td>
<td><p>1970-1990</p></td>
</tr>
<tr class="odd">
<td><p>苏哈列夫</p></td>
<td><p><a href="../Page/苏联农业集体化.md" title="wikilink">集體農場</a>（）</p></td>
<td><p>1971-1990</p></td>
</tr>
<tr class="even">
<td><p>新亚先涅沃</p></td>
<td><p>比察公園（）</p></td>
<td><p>1990-2009</p></td>
</tr>
</tbody>
</table>

## 外部連結

  - [莫斯科地鐵官方網頁——卡卢加－里加线](https://web.archive.org/web/20101013123147/http://old.mosmetro.ru/pages/page_5.php?id_page=46)

[卡卢加－里加线](../Category/卡卢加－里加线.md "wikilink")
[Category:1958年啟用的鐵路線](../Category/1958年啟用的鐵路線.md "wikilink")