**卡哈帕奇**（[猶加敦馬雅語](../Page/猶加敦馬雅語.md "wikilink")：）是一個[瑪雅遺址](../Page/瑪雅文明.md "wikilink")，靠近[貝里斯](../Page/貝里斯.md "wikilink")[卡約區的](../Page/卡約區.md "wikilink")[聖伊格納西奧鎮](../Page/聖伊格納西奧.md "wikilink")(San
Ignacio)。這個遺址的歷史可以上溯至西元400年，當時這裡是一個馬雅家庭的居所，後來逐漸發展為一個中型的馬雅城市。這個遺址位於[瑪寇河谷](../Page/瑪寇河.md "wikilink")(Macal
River)邊，共有34棟建築物，其中最高的有25公尺高。這個遺址在大約西元九世紀時荒廢，原因不明。

遺址的名字，意思是「[扁虱之地](../Page/扁虱.md "wikilink")」。這是因為當考古學家門在1950年代初次探勘此處時，此地還是個牧場的緣故。

## 圖片

Image:Cahal Pech Belize.jpg|主庭院 Image:Cahal Pech Belize - main courtyard
1.jpg|主庭院 Image:Cahal Pech Belize - main courtyard 2.jpg|主庭院 Image:Cahal
Pech Belize - ball court.jpg|球場的遺跡

## 相关条目

  - [奎略](../Page/奎略_\(伯利兹\).md "wikilink")

  -
## 参考文献

  -

[Category:玛雅文明](../Category/玛雅文明.md "wikilink")
[Category:貝里斯](../Category/貝里斯.md "wikilink")