[Carlstumpf.jpg](https://zh.wikipedia.org/wiki/File:Carlstumpf.jpg "fig:Carlstumpf.jpg")
**卡尔·斯图姆夫**（**Carl
Stumpf**，）是一位[德国哲学家和](../Page/德国.md "wikilink")[心理学家](../Page/心理学.md "wikilink")。

## 生平

卡尔·斯图姆夫出生在德国[巴伐利亚](../Page/巴伐利亚.md "wikilink")[维森泰德](../Page/维森泰德.md "wikilink")，师从[弗朗兹·布伦塔诺和](../Page/弗朗兹·布伦塔诺.md "wikilink")[鲁道夫·赫尔曼·陆宰](../Page/鲁道夫·赫尔曼·陆宰.md "wikilink")。他对他指导的博士生——现代[现象学的创始人](../Page/现象学.md "wikilink")[埃德蒙德·胡塞尔](../Page/埃德蒙德·胡塞尔.md "wikilink")，[格式塔心理学的三位创始人](../Page/格式塔心理学.md "wikilink")[马科斯·韦特墨](../Page/马科斯·韦特墨.md "wikilink")、[沃尔夫冈·苛勒和](../Page/沃尔夫冈·苛勒.md "wikilink")[科特·考夫卡](../Page/科特·考夫卡.md "wikilink")，以及奥地利著名小说家Robert
Musil都产生了重要影响。斯图姆夫还将[事态](../Page/事态.md "wikilink")（*Sachverhalt*)的概念引进了现代哲学，这个概念后来通过胡塞尔的著作得到普及。他还是由13位杰出的科学家组成的“汉斯委员会”成员之一，研究一匹能够进行计算的名叫“[聪明的汉斯](../Page/聪明的汉斯.md "wikilink")”的马。最终心理学家证实这匹马不能真正地进行计算。

斯图姆夫是布伦塔诺最早的学生之一，而且始终非常接近于他的早期教导。他写作论文在陆宰的指导下在[格丁根大学](../Page/格丁根大学.md "wikilink")（1868年），也在那里habilitation（1870年）。后来他越来越感兴趣于将经验主义方法运用于实验心理学，并成为这一学科的先驱之一。他任教于[格丁根大学](../Page/格丁根大学.md "wikilink")，然后成为[维尔茨堡教授](../Page/维尔茨堡.md "wikilink")，后来先后去了[布拉格](../Page/布拉格.md "wikilink")、[哈勒](../Page/哈勒.md "wikilink")、[慕尼黑](../Page/慕尼黑.md "wikilink")，最终来到[柏林](../Page/柏林.md "wikilink")，在那里创立了实验心理学的[柏林学派](../Page/柏林学派.md "wikilink")，后来[格式塔心理学在此基础上发展起来](../Page/格式塔心理学.md "wikilink")。斯图姆夫与当时德国（也是全世界）实验心理学最权威的人物[威廉·冯特之间的争执非常有名](../Page/威廉·冯特.md "wikilink")。斯图姆夫与美国心理学家、哲学家[威廉·詹姆斯是频繁通信的好友](../Page/威廉·詹姆斯.md "wikilink")，詹姆斯与冯特之间也有争议。

## 参见

  - [布伦塔诺学派](../Page/布伦塔诺学派.md "wikilink")

## 外部链接

  - [Picture, short biography, and
    bibliography](http://vlp.mpiwg-berlin.mpg.de/people/data?id=per307)
    in the Virtual Laboratory of the [Max Planck Institute for the
    History of
    Science](../Page/Max_Planck_Institute_for_the_History_of_Science.md "wikilink")
  - [Autobiography](http://psychclassics.yorku.ca/Stumpf/murchison.htm)
    from *History of Psychology in Autobiography* Vol. 1 (1930), p.
    389-441, at [York University](../Page/York_University.md "wikilink")
    "Classics in the History of Psychology"

[Category:德国心理学家](../Category/德国心理学家.md "wikilink")
[Category:德國哲學家](../Category/德國哲學家.md "wikilink")
[Category:20世紀哲學家](../Category/20世紀哲學家.md "wikilink")
[Category:19世紀哲學家](../Category/19世紀哲學家.md "wikilink")
[Category:柏林洪堡大學教師](../Category/柏林洪堡大學教師.md "wikilink")
[Category:慕尼黑大學教師](../Category/慕尼黑大學教師.md "wikilink")
[Category:哈雷-維滕貝格大學教師](../Category/哈雷-維滕貝格大學教師.md "wikilink")
[Category:維爾茨堡大學教師](../Category/維爾茨堡大學教師.md "wikilink")
[Category:哥廷根大學校友](../Category/哥廷根大學校友.md "wikilink")
[Category:巴伐利亞人](../Category/巴伐利亞人.md "wikilink")