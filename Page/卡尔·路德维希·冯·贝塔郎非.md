**卡爾·路德維希·馮·貝塔郎非**(**Karl Ludwig von
Bertalanffy**)，[奧地利](../Page/奧地利.md "wikilink")[生物學家](../Page/生物學家.md "wikilink")，[一般系統論創始人](../Page/一般系統論.md "wikilink")。他生於[奧地利首都](../Page/奧地利.md "wikilink")[維也納附近的](../Page/維也納.md "wikilink")[阿茨格斯多夫](../Page/阿茨格斯多夫.md "wikilink")，曾經在[倫敦](../Page/倫敦.md "wikilink")、[加拿大及](../Page/加拿大.md "wikilink")[美國等地工作](../Page/美國.md "wikilink")。

## 生平

貝塔郎非出身自一個有很多學者及法官的不平凡家族。\[1\]貝塔郎非家族是16世紀匈牙利貴族的後裔。他曾祖父[查理·約瑟夫·馮·貝塔郎非](../Page/查理·約瑟夫·馮·貝塔郎非.md "wikilink")(Charles
Joseph von
Bertalanffy)在奧地利定居，擔任奧地利南部的克拉根福(Klagenfurt)劇院院長。他外曾祖父則是[維也納出版商及皇家顧問](../Page/維也納.md "wikilink")－[約瑟夫·佛格爾](../Page/約瑟夫·佛格爾.md "wikilink")。\[2\].

1926年，貝塔朗非在[維也納大學考獲](../Page/維也納大學.md "wikilink")[哲學博士學位](../Page/哲學.md "wikilink")，畢業後便在該校任教。1937年前往美國[芝加哥大學任教](../Page/芝加哥大學.md "wikilink")。1948年，轉投[加拿大](../Page/加拿大.md "wikilink")[渥太華大學擔任醫療系任系主任及教授](../Page/渥太華大學.md "wikilink")。1954年至1955年，在[行为科学高等研究中心作学者](../Page/行为科学高等研究中心.md "wikilink")。1954年，建立一般系統論研究會並且出版了《[行為科學](../Page/行為科學.md "wikilink")》雜誌和《[一般系統年鑑](../Page/一般系統年鑑.md "wikilink")》。

## 研究及貢獻

他在20世紀人類智慧歷史中佔了一個重要的地位。他的貢獻超越了生物學的範疇，更觸及[控制論](../Page/控制論.md "wikilink")、[教育](../Page/教育.md "wikilink")、[歷史](../Page/歷史.md "wikilink")、[哲學](../Page/哲學.md "wikilink")、[精神科](../Page/精神科.md "wikilink")、[心理學及](../Page/心理學.md "wikilink")[社會學](../Page/社會學.md "wikilink")。推崇貝塔郎非的人士相信[一般系統論能夠為所有領域提供概念性架構](../Page/一般系統論.md "wikilink")。\[3\]

1934年，他發表了[個體成長模型](../Page/個體成長模型.md "wikilink")，闡述了生物學模型並且衍生幾個版本。

在簡單版本中，成長方程式是在時間(*t*)以微分方程式長度(*L*)來表達:

\(L'(t) = r_B \left( L_\infty - L(t) \right)\)

\(r_B\)是貝塔郎非的成長率而\(L_\infty\)則是個體終極長度。這個模型早見於1920年由**Pütter**倡議\[4\]。

## 参考文献

<div class="references-small">

<references />

</div>

## 參見

  - [人口動態](../Page/人口動態.md "wikilink")

## 外部連結

  - [International Society for the Systems
    Sciences'](http://www.isss.org/lumLVB.htm) biography of Ludwig von
    Bertalanffy
  - [Bertalanffy Center for the Study of Systems
    Science](https://web.archive.org/web/20140121231639/http://www.bertalanffy.org/)
    BCSSS in Vienna.

[Category:奥地利生物学家](../Category/奥地利生物学家.md "wikilink")
[Category:奥匈帝国科学家](../Category/奥匈帝国科学家.md "wikilink")
[Category:系统生物学家](../Category/系统生物学家.md "wikilink")
[Category:系统科学家](../Category/系统科学家.md "wikilink")
[Category:理论生物学家](../Category/理论生物学家.md "wikilink")
[Category:維也納大學校友](../Category/維也納大學校友.md "wikilink")
[Category:纳粹党党员](../Category/纳粹党党员.md "wikilink")
[Category:移民美国的奥地利人](../Category/移民美国的奥地利人.md "wikilink")
[Category:芝加哥大学教师](../Category/芝加哥大学教师.md "wikilink")
[Category:阿尔伯塔大学教师](../Category/阿尔伯塔大学教师.md "wikilink")
[Category:蒙特利尔大学教师](../Category/蒙特利尔大学教师.md "wikilink")
[Category:渥太华大学教师](../Category/渥太华大学教师.md "wikilink")
[Category:匈牙利裔奥地利人](../Category/匈牙利裔奥地利人.md "wikilink")
[Category:奥地利贵族](../Category/奥地利贵族.md "wikilink")
[Category:行为科学高等研究中心学者](../Category/行为科学高等研究中心学者.md "wikilink")

1.  Weckowicz p.2

2.  Davidson p.49

3.
4.  *Arch. Gesamte Physiol. Mench. Tiere*, **180**: 298-340