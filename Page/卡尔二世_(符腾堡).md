**卡尔·玛利亚·彼得·斐迪南·菲利普·阿尔布雷希特·约瑟夫·米夏埃尔·庇护·康拉德·罗伯特·乌尔里希**（***Carl Maria
Peter Ferdinand Philipp Albrecht Joseph Michael Pius Konrad Robert
Ulrich***，），生于[德国](../Page/德国.md "wikilink")[腓特烈港](../Page/腓特烈港.md "wikilink")，已被[废黜的](../Page/废黜.md "wikilink")[符腾堡公爵](../Page/符腾堡统治者列表.md "wikilink")，符腾堡王位继承人，符腾堡王室家族首领。如果[德国仍是一个](../Page/德国.md "wikilink")[君主制国家](../Page/君主制.md "wikilink")，他将成为[符腾堡](../Page/符腾堡.md "wikilink")[国王卡尔二世](../Page/国王.md "wikilink")。

## 生平

祖父[阿尔布雷希特是](../Page/阿尔布雷希特_\(符腾堡\).md "wikilink")[符腾堡王国末代](../Page/符腾堡王国.md "wikilink")[王储](../Page/王储.md "wikilink")（[推定继承人](../Page/推定继承人.md "wikilink")）。他是符腾堡公爵[菲利普·阿尔布雷希特与](../Page/菲利普·阿尔布雷希特_\(符腾堡\).md "wikilink")[托斯卡纳大公继承人](../Page/托斯卡纳大公.md "wikilink")[彼得·斐迪南的次女罗萨的次子](../Page/彼得·斐迪南_\(托斯卡纳\).md "wikilink")。

1959年，卡尔的长兄路德维希因与博德曼女[男爵阿德莱德](../Page/男爵.md "wikilink")（*Adelheid Frn
von
Bodman*）结婚而放弃王位继承权。他成为父亲符腾堡公爵菲利普的继承人，并在1975年父亲去世后继承了符腾堡公爵的头衔，成为符腾堡王室家族首领。

卡尔于1960年7月娶远房表妹法兰西[奥尔良支王位继承人巴黎伯爵](../Page/法国王位继承人.md "wikilink")[亨利](../Page/亨利_\(巴黎伯爵\).md "wikilink")（称“亨利六世”）的第四女戴安娜·弗朗索瓦丝·玛利亚·达·格洛利娅（*Diane
Françoise Maria da Glória*），二人都是法国国王路易·菲利普一世的后裔；婚后有四子二女：

  - 长子**[弗里德里希·菲利普·卡尔·弗朗茨·玛利亚](../Page/弗里德里希二世_\(符腾堡\).md "wikilink")**（*Friedrich
    Philipp Carl Franz
    Maria*），1961年6月1日生于[腓特烈港](../Page/腓特烈港.md "wikilink")，2018年5月9日逝世。1993年娶[维德的威廉明妮公主](../Page/维德侯爵.md "wikilink")（*Wilhelmine
    Friederike Pauline Elisabeth Marie*），有一子二女。
  - 长女**玛蒂尔德·玛丽-安托瓦内特·罗萨·伊莎贝尔**（*Mathilde Marie-Antoinette Rosa
    Isabelle*），1962年7月11日生于[腓特烈港](../Page/腓特烈港.md "wikilink")，1988年嫁给[瓦尔德堡-载尔-特劳赫堡侯爵世子](../Page/瓦尔德堡侯爵.md "wikilink")[埃里希](../Page/埃里希_\(瓦尔德堡-载尔-特劳赫堡\).md "wikilink")，现有五女。
  - 次子**埃伯哈德·尼库劳斯·海因里希·约翰内斯·玛利亚**（*Eberhard Alois Nikolaus Heinrich
    Johannes
    Maria*），1963年6月20日生于[腓特烈港](../Page/腓特烈港.md "wikilink")，2011年与露西亚·德西蕾·科普夫结婚，有一子。
  - 三子**菲利普·阿尔布雷希特·克里斯托弗·乌尔里希·玛利亚**（*Philipp Albrecht Christoph Ulrich
    Maria*），1964年11月1日生于[腓特烈港](../Page/腓特烈港.md "wikilink")，1991年娶[巴伐利亚的](../Page/巴伐利亚.md "wikilink")[马克斯·埃曼努埃尔公爵的次女玛利亚](../Page/马克斯·埃曼努埃尔_\(巴伐利亚\).md "wikilink")·卡罗琳娜，现有一子三女。
  - 四子**米夏埃尔·海因里希·阿尔伯特·亚历山大·玛利亚**（*Michael Heinrich Albert Alexander
    Maria*），1965年12月1日生于[腓特烈港](../Page/腓特烈港.md "wikilink")，2006年7月8日娶平民尤里娅·施托尔茨（*Julia
    Storz*）。
  - 次女**埃利奥诺尔·弗洛尔·胡安尼塔·夏洛特·尤多克茜·玛丽-昂内**（*Eleonore Fleur Juanita
    Charlotte Eudoxie
    Marie-Agne*），1977年11月4日生于[阿特斯豪森](../Page/阿特斯豪森.md "wikilink")，2003年嫁给高厄斯的莫里茨·路易伯爵（*Moritz
    Louis Gf v.Goëß*）。

卡尔目前是一个成功的企业家。

[Category:符腾堡王室后裔](../Category/符腾堡王室后裔.md "wikilink")
[Category:德国企业家](../Category/德国企业家.md "wikilink")
[Category:巴登-符騰堡人](../Category/巴登-符騰堡人.md "wikilink")