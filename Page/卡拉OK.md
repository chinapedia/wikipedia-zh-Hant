[卡拉OK.jpg](https://zh.wikipedia.org/wiki/File:卡拉OK.jpg "fig:卡拉OK.jpg")

**卡拉OK**（）是
源自[日本的娛樂性質](../Page/日本.md "wikilink")[歌唱活動](../Page/歌唱.md "wikilink")，通常是在播放預錄在[錄影帶之類儲存媒介上](../Page/錄影帶.md "wikilink")、沒有主唱人聲的[音樂伴奏同時](../Page/音樂.md "wikilink")，在[電視](../Page/電視.md "wikilink")[螢幕上同步播放有著節拍提示的歌詞](../Page/螢幕.md "wikilink")，然後由參與者邊看著歌詞邊持[麥克風歌唱](../Page/麥克風.md "wikilink")。其日語原名意為「無人樂隊」。自從1971年[井上大佑發明了最早的音樂伴唱帶之後](../Page/井上大佑.md "wikilink")，它已成為現今最受歡迎的大眾休閒娛樂方式之一。

## 簡介

[KaraokeMachine.JPG](https://zh.wikipedia.org/wiki/File:KaraokeMachine.JPG "fig:KaraokeMachine.JPG")
卡拉OK的名字源自於[日文](../Page/日文.md "wikilink")，其中卡拉（）是漢字「空」之[訓讀](../Page/訓讀.md "wikilink")，OK（）則是[管弦樂團](../Page/管弦樂團.md "wikilink")（，源自英语）之諧音，合起來意指唱歌時沒有真正的[樂隊伴唱](../Page/樂隊.md "wikilink")，只有影音伴唱。但與華語圈的習慣相反，日本的[卡拉OK厅并不播放原声原影的卡拉ok伴唱帶](../Page/KTV.md "wikilink")\[1\]。[JVC曾於](../Page/JVC.md "wikilink")1980年代在中國大陸以「」註冊產品商標。\[2\][Carolok.jpg](https://zh.wikipedia.org/wiki/File:Carolok.jpg "fig:Carolok.jpg")

## 歷史

### 1970年代

1971年，[日本](../Page/日本.md "wikilink")[神戶市的音樂家](../Page/神戶市.md "wikilink")[井上大佑發明了第一部卡拉OK機器](../Page/井上大佑.md "wikilink")\[3\]（另有說法為1967年日電工業的根岸重一最早销售，或是1968年浜崎厳）。\[4\]\[5\]

### 1990年代

卡拉OK面世之後，由於卡拉OK器材和伴唱器材的商業化，以及歌曲著作權等問題的阻礙，沉寂了很長時間，未能得以普及，而井上大佑也未能因發明卡拉OK得到專利權而獲利。

1980年代中後期，有選曲功能的CD音響存儲形式和VCD音影存儲形式的成熟，取代錄音錄影帶的捲帶搜索功能，卡拉OK才能得到實質性發展。

在日本大規模流行之後，很快，卡拉OK也在1989年傳到了東亞以及東南亞，接著傳到了世界的其他地方。

卡拉OK的發展普及，有賴於字幕VCD的大量生產。VCD生產線及技術當時由日本壟斷。簡體中文的卡拉OK用VCD碟的生產基地集中在台灣、香港、華南地區的合資工廠。

1989年中國大陸因反對資產階級自由化，卡拉OK器材和字幕VCD碟的生產和進口受到嚴格限制。是年6月開始，中國大陸面臨經濟制裁，進口停頓。1990年下半年開始，形勢漸漸改善。1991年以後，卡拉OK在大陸才有迅速發展的機會。

### 2010年代

隨著寬頻網路與手持設備的硬體更加成熟，利用雲端技術將卡拉ok網路化，透過手持設備即可下載影音伴唱檔案，使用者可以透過電腦、行動電話或是平板電腦，搭配KTV應用程式進行歌唱。

KTV
APP與卡拉ok最大的相異之處，在於使用的麥克風差別，受限於電腦與手持設備僅提供小電容式的麥克風，因此收音效果無法與卡拉OK相比，卡拉OK所採用的收音設備為動圈式麥克風，電腦與手持設備必須透過動圈式麥克風轉接器，才能搭配動圈式使用，讓收音效果與卡拉OK一樣。

## 卡拉OK店的運作

卡拉OK除了在日本本土非常流行之外，也進一步地推廣到其他許多地區，包括[台灣](../Page/台灣.md "wikilink")、[香港及](../Page/香港.md "wikilink")[中国大陆](../Page/中国大陆.md "wikilink")。1988年卡拉OK傳入香港並迅速發展。由於大批香港商人赴[中國大陸地區投資經商](../Page/中國大陸.md "wikilink")，也將卡拉OK文化傳播開來。中國大陸第一間卡拉OK是
1990年在海南省以中外合資形式出現。隨著翻版碟大量生產和音響器材走私進口，卡拉OK瞬間開遍中國大陸所有大中城市。並於1991年中起，向縣城以下地區發展，成為色情活動的主要場所，也有些發展成夜總會。

台灣有商人更進一步將卡拉OK結合當時市面上非常流行的[MTV](../Page/影音包廂.md "wikilink")，而變成改良式包廂型態的卡拉OK，稱為「KTV」。KTV的概念在傳回日本本土後也逐漸受到歡迎，成為近年來主流的經營形式，稱為「」（<span lang="ja">カラオケボックス</span>）。

隨著[互聯網以及](../Page/互聯網.md "wikilink")[Youtube](../Page/Youtube.md "wikilink")、[优酷](../Page/优酷.md "wikilink")、[土豆网等媒體分享網站普及](../Page/土豆网.md "wikilink")，有網民會把KTV片段放至網上，或為沒有卡啦OK字幕的MV加上字幕，甚至出現網上卡啦OK系統（如[巨星](../Page/巨星_\(游戏\).md "wikilink")、[酷我K歌](../Page/酷我K歌.md "wikilink")、[呱呱K歌伴侣](../Page/呱呱K歌伴侣.md "wikilink")、[爱吼网](../Page/爱吼网.md "wikilink")、[哆来咪发KTV](../Page/哆来咪发KTV.md "wikilink")、[K米网](../Page/K米网.md "wikilink")、[爱卡拉](../Page/爱卡拉.md "wikilink")、[爱唱久久](../Page/爱唱久久.md "wikilink")、[CarolOK雲端行動KTV等](../Page/CarolOK雲端行動KTV.md "wikilink")）。但此類作法大都違反了著作權法規或是遊走於合法邊緣。

另外大型卡拉OK店與投幣式卡拉OK店兩者間的消費價位相差甚距，最主要的原因是房間裝潢設計的質感、歌曲的新鮮度與客人歌唱音量彼此間的-{干}-擾程度，大型卡拉OK店偏向多機密閉式裝潢，投幣式卡拉OK店偏向單機半開放式廣場。

### 香港

在[香港](../Page/香港.md "wikilink")，1988年開始，有伴唱女郎的卡拉OK迅速發展並取代夜總會，成為夜生活的主要形式。近年卡拉OK幾乎操控了全港的[流行音樂的品味](../Page/流行音樂.md "wikilink")，形成[香港人俗稱的](../Page/香港人.md "wikilink")「[K歌文化](../Page/K歌.md "wikilink")」，即大部份歌曲皆為卡拉OK量身訂做，音域遷就大眾避免極端高音；事實上，近年香港絕大部份流行歌曲都先被安排在卡拉OK盒子讓顧客試唱，然後才安排在大眾媒體上播放。直至2005年，這個情況才因香港「卡拉OK盒子」市場的發展到達極限，以及獨立音樂、政治諷刺歌曲和其他非主流音樂開始隨著[寬頻](../Page/寬頻.md "wikilink")[網際網路發達而有所改變](../Page/網際網路.md "wikilink")。

香港加州紅於黃埔的Yo-Park也曾提供同時提供一般包廂式唱K，及少量投幣式單機卡啦OK，又提供表演場地予非主流音樂\[6\]，但隨著被[Neway收購而消失](../Page/Neway.md "wikilink")。

近年香港的卡拉OK開始著重食物質素，推出和其他餐廳價錢相約之K-Lunch（午膳時段）及K-Buffet（卡拉OK
同時享用自助餐），款式也貼近市場其他口味，例如壽司、刺身、火鍋、朱古力噴泉、西式甜品等，務求和其他餐廳競爭。

### 台灣

在[台灣約](../Page/台灣.md "wikilink")2000年開始，製作人歌曲若在電視媒體、街頭演藝、網路試聽反應口碑不錯，會先賣版權至大型卡拉OK店，如[錢櫃KTV](../Page/錢櫃KTV.md "wikilink")、[好樂迪供民眾團聚歌唱](../Page/好樂迪.md "wikilink")；若該歌曲市場漸趨冷淡才會下賣到投幣式卡拉OK主機供應商並再轉賣小型餐館供民眾點播歌唱。

## 著名的連鎖卡拉OK店

[Karaoke-Big_echo-P5262150.jpg](https://zh.wikipedia.org/wiki/File:Karaoke-Big_echo-P5262150.jpg "fig:Karaoke-Big_echo-P5262150.jpg")
[Taipei_KTV-Eingangshalle.jpg](https://zh.wikipedia.org/wiki/File:Taipei_KTV-Eingangshalle.jpg "fig:Taipei_KTV-Eingangshalle.jpg")
[Karaoke-Harbin6303733.jpg](https://zh.wikipedia.org/wiki/File:Karaoke-Harbin6303733.jpg "fig:Karaoke-Harbin6303733.jpg")

### 日本

  - [必愛歌](../Page/必愛歌.md "wikilink")（，Big Echo）

### 台灣

  - [錢櫃KTV](../Page/錢櫃KTV.md "wikilink")
  - [好樂迪](../Page/好樂迪.md "wikilink")
  - [凱悅yes KTV](../Page/凱悅yes_KTV.md "wikilink")
  - [星聚點](../Page/星聚點.md "wikilink")
  - [銀櫃KTV](../Page/銀櫃KTV.md "wikilink")
  - [笑傲江湖(KTV)](../Page/笑傲江湖\(KTV\).md "wikilink")
  - [享溫馨(KTV)](../Page/享溫馨\(KTV\).md "wikilink")
  - [U2KTV](../Page/U2KTV.md "wikilink")

### 香港

  - [Neway](../Page/Neway.md "wikilink")
      - [加州紅](../Page/加州紅.md "wikilink")：已被Neway收購
  - [紅人派對](../Page/紅人派對.md "wikilink")（Red Mr）

### 中國大陸

  - [錢櫃KTV](../Page/錢櫃KTV.md "wikilink")
  - [糖果KTV](../Page/糖果KTV.md "wikilink")
  - [好乐迪](../Page/好乐迪_\(中國大陸\).md "wikilink")
  - [麥樂迪](../Page/麥樂迪.md "wikilink")
  - [游歌KTV](../Page/游歌KTV.md "wikilink")（BMB）
  - [Neway](../Page/Neway.md "wikilink")

### 馬來西亞

  - [Redbox](../Page/加州紅.md "wikilink")
  - [Neway](../Page/Neway.md "wikilink")

## 相關條目

  - [K壓](../Page/K壓.md "wikilink")
  - [迷你KTV](../Page/迷你KTV.md "wikilink")

## 參考文獻

<references/>

[Category:卡拉OK](../Category/卡拉OK.md "wikilink")
[Category:歌唱](../Category/歌唱.md "wikilink")
[Category:娛樂場所](../Category/娛樂場所.md "wikilink")
[Category:日本發明](../Category/日本發明.md "wikilink")
[Category:日語詞彙](../Category/日語詞彙.md "wikilink")

1.
2.  [客乐我歌](http://www.searchtmr.com/details?trqi=cb40615033fff349)
3.  [Who Invented the Karaoke
    Machine?](http://www.events-in-music.com/who-invented-the-karaoke-machine.html)
     Events-in-Music.com
4.  烏賀陽弘道 『カラオケ秘史―創意工夫の世界革命―』（新潮社、2008年）、ISBN 978-4106102929
5.  野口恒 『カラオケ文化産業論』（PHP研究所、2005年）、ISBN 978-4569642222
6.  [YO PARK
    轉手引起樂迷關注](http://bitetone.com/2010/04/07/%E9%A6%99%E6%B8%AF-yo-park-%E8%BD%89%E6%89%8B%E6%8E%80%E8%B5%B7%E6%A8%82%E8%BF%B7%E9%97%9C%E6%B3%A8/)