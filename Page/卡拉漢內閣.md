## 內閣

<table>
<tbody>
<tr class="odd">
<td><p><strong>官職</strong></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/英國首相.md" title="wikilink">首相</a>（Prime Minister）<br />
<a href="../Page/第一財政大臣.md" title="wikilink">第一財政大臣</a>（First Lord of the Treasury）</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/財政大臣.md" title="wikilink">財政大臣</a>（Chancellor of the Exchequer）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/英國大法官.md" title="wikilink">大法官</a>（Lord Chancellor）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/樞密院議長.md" title="wikilink">樞密院議長</a>（Lord President of the Council）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/掌璽大臣.md" title="wikilink">掌璽大臣</a>（Lord Privy Seal）</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/外務及英聯邦事務大臣.md" title="wikilink">外務大臣</a>（Foreign Secretary）</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/內務大臣.md" title="wikilink">內務大臣</a>（Home Secretary）</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/國防大臣.md" title="wikilink">國防大臣</a>（Secretary of State for Defence）</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/教育及科學大臣.md" title="wikilink">教育及科學大臣</a>（Secretary of State for Education and Science）</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/就業事務大臣.md" title="wikilink">就業事務大臣</a>（Secretary of State for Employment）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/能源大臣.md" title="wikilink">能源大臣</a>（Secretary of State for Energy）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/環境事務大臣.md" title="wikilink">環境事務大臣</a>（Secretary of State for the Environment）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/社會服務大臣.md" title="wikilink">社會服務大臣</a>（Secretary of State for Social Services）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/工業大臣.md" title="wikilink">工業大臣</a>（Secretary of State for Industry）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/海外發展部長.md" title="wikilink">海外發展部長</a>（Minister for Overseas Development）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/價格及消費者權益大臣.md" title="wikilink">價格及消費者權益大臣</a>（Secretary of State for Prices and Consumer Protection）</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/貿易及工業大臣.md" title="wikilink">貿易大臣</a>（Secretary of State for Trade）</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/交通大臣.md" title="wikilink">交通大臣</a>（Secretary of State for Transport）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蘇格蘭事務大臣.md" title="wikilink">蘇格蘭事務大臣</a>（Secretary of State for Scotland）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/威爾斯事務大臣.md" title="wikilink">威爾斯事務大臣</a>（Secretary of State for Wales）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/北愛爾蘭事務大臣.md" title="wikilink">北愛爾蘭事務大臣</a>（Secretary of State for Northern Ireland）</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蘭卡斯特公爵領地總裁.md" title="wikilink">蘭卡斯特公爵領地總裁</a>（Chancellor of the Duchy of Lancaster）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/財政部秘書長.md" title="wikilink">財政部秘書長</a>（Chief Secretary to the Treasury）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/農業、魚業及食品部長.md" title="wikilink">農業、魚業及食品部長</a>（Minister of Agriculture, Fisheries and Food）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/藝術部長.md" title="wikilink">藝術部長</a>（Minister of Arts）</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/社會保障部長.md" title="wikilink">社會保障部長</a>（Minister for Social Security）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/地方政府及規劃部長.md" title="wikilink">地方政府及規劃部長</a>（Minister for Local Government and Planning）</p></td>
</tr>
</tbody>
</table>

[C](../Category/英國內閣.md "wikilink")