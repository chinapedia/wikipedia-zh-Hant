**卡普空株式会社**（*CAPCOM CO., LTD.
株式会社カプコン*）是一家日本视频游戏开发商与发行商。创始于1979年，以日本街机制造商开始\[1\]，发展成如今在日本，美国，欧洲，亚洲都设有事务所的国际公司\[2\]。据官网所称，其目标是：“通过游戏创造‘游文化’，成为带给大众感动的‘感性开发企业’”\[3\]。其最新作是2019年3月发售的《鬼泣5》。

## 历史

日本人辻本宪三( Tsujimoto
Kenzō)1979年5月30日\[4\]创立了卡普空的前身——IRM公司，他同时还是Irem公司的董事长。直到1983年离开卡普空前辻本宪三同时任职于两家公司。

IRM公司及其子公司Japan Capsule Computers Co.,
Ltd,都致力于电子游戏机的制造和分销。\[5\]这两家公司1981年9月时更名为Sambi
Co., Ltd.，\[6\]随后1983年6月11日\[7\]辻本宪三为了接管公司内部销售部门建立了Capcom Co., Ltd。\[8\]

1994年卡普空将旗下的格斗游戏《[街头霸王](../Page/街头霸王.md "wikilink")》系列改编成同名电影，并在商业上取得成功，2002年的《生化危机》系列的电影改编也是如此，卡普空视其为一种增加游戏销量的好手段。2000年10月，卡普空在东京证券交易所第一部上市。\[9\]

2014年7月22日，卡普空宣布加速推进在中国和其它亚洲地区的在线游戏业务，并和[奇虎360及](../Page/三六零.md "wikilink")[腾讯合作开发和运营](../Page/腾讯.md "wikilink")。\[10\]

2018年8月10日，卡普空官方网上商店e-Capcom进驻[京东](../Page/京東_\(網站\).md "wikilink")，主要销售面向作品粉丝的周边商品。\[11\]

## 作品

卡普空创造了许多数百万销量的游戏系列，其中最成功的是《[生化危机](../Page/生化危機系列.md "wikilink")》系列。\[12\]

主要作品有《[快打旋風](../Page/快打旋風系列.md "wikilink")》系列、《[街頭快打](../Page/街頭快打系列.md "wikilink")》系列、[洛克人系列](../Page/洛克人系列.md "wikilink")、《[惡靈古堡](../Page/惡靈古堡.md "wikilink")》、《[惡魔獵人](../Page/惡魔獵人.md "wikilink")》、《[魔物獵人](../Page/魔物獵人.md "wikilink")》、《[戰國BASARA](../Page/戰國BASARA.md "wikilink")》、《[鬼武者](../Page/鬼武者.md "wikilink")》、[逆转裁判系列等](../Page/逆转裁判系列.md "wikilink")。

| 百萬暢銷游戏销量（至2018年12月10日）\[13\]                       |
| -------------------------------------------------- |
| 名次                                                 |
| 1                                                  |
| 2                                                  |
| 3                                                  |
| 4                                                  |
| 5                                                  |
| 6                                                  |
| 7                                                  |
| 8                                                  |
| 9                                                  |
| 10                                                 |
| 11                                                 |
| 12                                                 |
| 13                                                 |
| 14                                                 |
| 15                                                 |
| 16                                                 |
| 17                                                 |
| 18                                                 |
| 19                                                 |
| 20                                                 |
| 21                                                 |
| 22                                                 |
| 23                                                 |
| 24                                                 |
| 25                                                 |
| 26                                                 |
| 27                                                 |
| 28                                                 |
| 29                                                 |
| 30                                                 |
| 31                                                 |
| 32                                                 |
| 33                                                 |
| 34                                                 |
| 35                                                 |
| 36                                                 |
| 37                                                 |
| 38                                                 |
| 39                                                 |
| 40                                                 |
| 41                                                 |
| 42                                                 |
| 43                                                 |
| 44                                                 |
| 45                                                 |
| 46                                                 |
| 47                                                 |
| 48                                                 |
| 49                                                 |
| 50                                                 |
| 51                                                 |
| 52                                                 |
| 53                                                 |
| 54                                                 |
| 55                                                 |
| 56                                                 |
| 57                                                 |
| 58                                                 |
| 59                                                 |
| 60                                                 |
| 61                                                 |
| 62                                                 |
| 63                                                 |
| 64                                                 |
| 65                                                 |
| 66                                                 |
| 67                                                 |
| 68                                                 |
| 69                                                 |
| 70                                                 |
| 71                                                 |
| 72                                                 |
| 73                                                 |
| 74                                                 |
| 75                                                 |
| 76                                                 |
| 77                                                 |
| 78                                                 |
| 79                                                 |
| 80                                                 |
| 81                                                 |
| 82                                                 |
| 83                                                 |
| 84                                                 |
| 85                                                 |
| 86                                                 |
| 87                                                 |
| 其中DL代表數位下載版，包含PSN、Xbox Live還有Wii和Wii U、PC等數位平台銷售版本 |
|                                                    |

## 参考文献

## 外部链接

  - [卡普空官方网站](http://www.capcom.co.jp)
  - [卡普空台湾官方网站](http://ctc.capcom.com.tw/)
  - [Capcom香港官方网站](http://www.capcomasia.com.hk)
  - [卡普空中国大陆官方网站](http://www.capcom.co.jp/ir/chinese/)

[Category:1983年開業電子遊戲公司](../Category/1983年開業電子遊戲公司.md "wikilink")
[Category:日本電子遊戲公司](../Category/日本電子遊戲公司.md "wikilink")
[Category:日本品牌](../Category/日本品牌.md "wikilink")
[Category:電子遊戲開發公司](../Category/電子遊戲開發公司.md "wikilink")
[Category:電子遊戲發行商](../Category/電子遊戲發行商.md "wikilink")
[Category:大阪府公司](../Category/大阪府公司.md "wikilink")
[卡普空](../Category/卡普空.md "wikilink")
[Category:國際遊戲開發者協會成員](../Category/國際遊戲開發者協會成員.md "wikilink")
[Category:中央區 (大阪市)](../Category/中央區_\(大阪市\).md "wikilink")

1.

2.

3.   致投资家们/公司信息|accessdate=2019-03-12|language=zh-CN}}

4.

5.
    History|url=[http://www.capcom.co.jp/ir/english/company/history.html|work=CAPCOM](http://www.capcom.co.jp/ir/english/company/history.html%7Cwork=CAPCOM)
    IR|accessdate=2019-02-15|language=en}}

6.
7.
8.
    カプコンの歴史|url=[http://www.capcom.co.jp/ir/company/history.html|work=株式会社カプコン](http://www.capcom.co.jp/ir/company/history.html%7Cwork=株式会社カプコン){{\!}}
    投資家の皆さまへ / 企業情報|accessdate=2019-02-15|language=ja}}

9.
    カプコンの歴史|url=[http://www.capcom.co.jp/ir/company/history.html|work=株式会社カプコン](http://www.capcom.co.jp/ir/company/history.html%7Cwork=株式会社カプコン){{\!}}
    投資家の皆さまへ / 企業情報|accessdate=2019-02-15|language=ja}}

10.

11.

12.  Total Sales Units
    Data|url=[https://web.archive.org/web/20140327174103/http://www.capcom.co.jp/ir/english/business/salesdata.html|work=web.archive.org|date=2014-03-27|accessdate=2019-02-15](https://web.archive.org/web/20140327174103/http://www.capcom.co.jp/ir/english/business/salesdata.html%7Cwork=web.archive.org%7Cdate=2014-03-27%7Caccessdate=2019-02-15)}}

13. [總銷售統計（Capcom投資人情報官方頁面）](http://www.capcom.co.jp/ir/finance/million.html)