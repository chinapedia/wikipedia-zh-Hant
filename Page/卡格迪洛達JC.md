**卡格迪洛達JC**（**Roda JC
Kerkrade**），一般稱為**洛達**，是一間[荷蘭](../Page/荷蘭.md "wikilink")[足球會](../Page/足球會.md "wikilink")，位於[克克拉德](../Page/科尔克拉德.md "wikilink")，位於荷蘭南部，鄰近[德國](../Page/德國.md "wikilink")。球會現在於[荷蘭甲組足球聯賽角逐](../Page/荷蘭甲組足球聯賽.md "wikilink")。

## 歷史

洛達是由多間當地球會合併而成。在1954年，1926年成立的SV克克拉德（），和1914年成立的，合併成為洛達體育會（）。同年，當年成立的快速隊54（），和1910年成立的業餘球隊合併成快速隊（），快速隊在1956年贏得聯賽冠軍。而在1962年6月27日，快速隊和洛達體育會合併成現在的洛達。結果，球會成為了不少於五間球會參與合併中，最複雜的一次。而當球會在1973年升上荷蘭甲組足球聯賽時，球會在第二年沒有降班。而球會在1997年和2000年在球會五次參與[荷蘭盃的決賽中](../Page/荷蘭盃.md "wikilink")，這兩年他們贏得了冠軍。而2004/05球季的平均入座率為12,700人。

最後一個荷蘭煤礦在六十年代關閉，但在最南部的省林堡（Limberg），到今天依然被喻為採礦區。這個區域比較多山，和國家其他地區不同。在區域層面上和老礦工的眼中，採煤仍然是一種工業。他們依然在回憶光輝不再的工業，所以他們已經視南林堡為自己的家園，也因此洛達成了他們的球隊。

洛達在荷蘭被視為「礦工隊」。球會的球迷，來自世俗的省首府[馬斯垂克](../Page/馬斯垂克.md "wikilink")（），都十分尊敬球會。而在克克拉德和周邊地區，更是把球會視為榮耀。所以洛達毫無疑問地，成為了當地第一球會。而現在位於林堡的同市宿敵,

和，都只在乙組角逐。洛達的榮耀還來自七次打入歐洲賽和五次打入荷蘭杯決賽，而近兩次(1997年和2000年)更是獲得冠軍。洛達的其中一個前身快速隊，是1956年的聯賽冠軍，當時的正選中，有十個人是礦工。

荷蘭有幾間球會好像洛達一樣，擁有複習的合併歷史。洛達的全名是。故事是這樣的：克克拉德足球會（）在1926年成立，而則在1914年成立。這兩間球會在1954年合併為洛達體育會（）。同年，1910年成立的和快速隊'54合併成快速隊（）。這兩宗合併，只是維持了八年，到1962年，他們合併成洛達。其後，洛達在1973年升上甲組，而且自此之後沒有降班。今日，洛達的主場位於[林堡公園球場](../Page/林堡公園球場.md "wikilink")，是荷甲眾球場中最大和最多設施的球場之一。

自從1973年升上甲組以來，球會超過二十次能在頭十名完成。因此，洛達獲得「三強之外最佳球會」（）之名。因為他們屬少數能定期於歐洲賽登場的球會，和在三強中突出自己。洛達在1994/95球季是歷史中最光輝的時刻，他們成為了唯一一支對當年歐洲冠軍和聯賽冠軍[阿積士時](../Page/阿積士.md "wikilink")，能保持不敗的球隊。這季他們兩次的對賽都打成1-1，而這季他們以第二名完成聯賽，是他們有史以來最佳的成績。

球會最值得回味的歐洲賽賽季是1988/89球季，他們在[歐洲杯賽冠軍杯出線](../Page/歐洲杯賽冠軍杯.md "wikilink")，後來在淘汰賽階段才被兩名[保加利亞球會](../Page/保加利亞.md "wikilink")[索菲亞中央陸軍射手](../Page/索菲亞中央陸軍.md "wikilink")：史岱捷哥夫（）和哥斯達甸洛夫（），淘汰出局。而後來他們都成為了超級球星。而最值得回味的賽事，則是在2002年，洛達對到訪的[意大利球會](../Page/意大利.md "wikilink")[AC米蘭](../Page/AC米蘭.md "wikilink")，不幸地以0-1落敗，但後來他們以同樣比數在AC米蘭主場[聖西路球場贏回對手](../Page/聖西路球場.md "wikilink")。當時此舉令米蘭人十分震驚，令比賽最後要互射十二碼分勝負，而洛達更曾經領先對手，但最後只以一球十二碼見負，球會最終無法出線。

以[荷蘭盃的成績來演繹洛達在荷蘭的地位](../Page/荷蘭盃.md "wikilink")，實在最適合不過。洛達曾經五次進入決賽，但頭三次的對手是荷蘭三強，而洛達最後也只取得銀牌。頭兩次在1976年和1988年對[燕豪芬](../Page/燕豪芬.md "wikilink")，第三次在1992年對[飛燕諾](../Page/飛燕諾.md "wikilink")。但後來的兩次，對手都不是荷蘭三強
，這兩次都能奪金而回，分別是1997年的[海倫芬](../Page/海倫芬.md "wikilink")，和2000年的[奈梅亨](../Page/奈梅亨.md "wikilink")。由此可知，洛達雖然不及荷蘭三強，但比其他眾多對手都要強。

即使在2002年至2003年遇上財政困難，洛達依然能保持水準，而且也沒有不忠實於洛達的精神。基本上，球會的死忠球迷，都是礦工家族們的後代。洛達的歷史如果沒有這個名字就會變得不完整：，他們球會八十年代到九十年代初期的隊長，到現在仍被視為林堡煤礦區的草根英雄。

## 球場

自從球會成立後，球會就在""這個能容納21,500人的球場作為主場。現在的主場位於[林堡公園球場](../Page/林堡公園球場.md "wikilink")，在2000年8月15日開幕，擁有20,000個座位，首場的比賽是和[薩拉戈薩比賽](../Page/薩拉戈薩.md "wikilink")。

## 成就

  - '''[荷蘭甲組足球聯賽](../Page/荷蘭甲組足球聯賽.md "wikilink") '''
      - **冠軍:** 1955/1956 快速隊
      - **亞軍:** 1958/1959 快速隊
      - **亞軍:** 1994/1995 洛達

<!-- end list -->

  - '''[荷蘭乙組足球聯賽](../Page/荷蘭乙組足球聯賽.md "wikilink") '''
      - **冠軍:** 1972/1973 洛達

<!-- end list -->

  - **[荷蘭盃](../Page/荷蘭盃.md "wikilink")**
      - **冠軍:** 1997, 2000 洛達
      - **亞軍:** 1976, 1988, 1992 洛達

<!-- end list -->

  - **[荷蘭超級盃](../Page/荷蘭超級盃.md "wikilink")**
      - **亞軍:** 1997, 2000 洛達

## 著名球員

<table style="width:75%;">
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/Dick_Advocaat.md" title="wikilink">Dick Advocaat</a></li>
<li><a href="../Page/Tijani_Babangida.md" title="wikilink">Tijani Babangida</a></li>
<li><a href="../Page/Stanley_Bish.md" title="wikilink">Stanley Bish</a></li>
<li><a href="../Page/Joop_Dacier.md" title="wikilink">Joop Dacier</a></li>
<li><a href="../Page/Leo_Degens.md" title="wikilink">Leo Degens</a></li>
<li><a href="../Page/Leo_Ehlen.md" title="wikilink">Leo Ehlen</a></li>
<li><a href="../Page/John_Eriksen.md" title="wikilink">John Eriksen</a></li>
<li><a href="../Page/Henk_Fräser.md" title="wikilink">Henk Fräser</a></li>
<li><a href="../Page/Bram_Geilman.md" title="wikilink">Bram Geilman</a></li>
<li><a href="../Page/Nol_Hendriks.md" title="wikilink">Nol Hendriks</a></li>
<li><a href="../Page/Ruud_Hesp.md" title="wikilink">Ruud Hesp</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/Gene_Hanssen.md" title="wikilink">Gene Hanssen</a></li>
<li><a href="../Page/Marco_van_Hoogdalem.md" title="wikilink">Marco van Hoogdalem</a></li>
<li><a href="../Page/Bert_Jacobs.md" title="wikilink">Bert Jacobs</a></li>
<li><a href="../Page/Ron_Jans.md" title="wikilink">Ron Jans</a></li>
<li><a href="../Page/Jan_Jongbloed.md" title="wikilink">Jan Jongbloed</a></li>
<li><a href="../Page/Johan_de_Kock.md" title="wikilink">Johan de Kock</a></li>
<li><a href="../Page/Jens_Kolding.md" title="wikilink">Jens Kolding</a></li>
<li><a href="../Page/Arouna_Kone.md" title="wikilink">Arouna Kone</a></li>
<li><a href="../Page/Adri_Koster.md" title="wikilink">Adri Koster</a></li>
<li><a href="../Page/Gerard_van_der_Lem.md" title="wikilink">Gerard van der Lem</a></li>
<li><a href="../Page/John_van_Loen.md" title="wikilink">John van Loen</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/Eric_van_der_Luer.md" title="wikilink">Eric van der Luer</a></li>
<li><a href="../Page/Mark_Luijpers.md" title="wikilink">Mark Luijpers</a></li>
<li><a href="../Page/Dick_Nanninga.md" title="wikilink">Dick Nanninga</a></li>
<li><a href="../Page/Theo_Pickee.md" title="wikilink">Theo Pickee</a></li>
<li><a href="../Page/Huub_Stevens.md" title="wikilink">Huub Stevens</a></li>
<li><a href="../Page/Wilbert_Suvrijn.md" title="wikilink">Wilbert Suvrijn</a></li>
<li><a href="../Page/Rene_Trost.md" title="wikilink">Rene Trost</a></li>
<li><a href="../Page/Joos_Valgaeren.md" title="wikilink">Joos Valgaeren</a></li>
<li><a href="../Page/Pierre_Vermeulen.md" title="wikilink">Pierre Vermeulen</a></li>
<li><a href="../Page/Peter_de_Wit.md" title="wikilink">Peter de Wit</a></li>
<li><a href="../Page/Sten_Ziegler.md" title="wikilink">Sten Ziegler</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 贊助商

  - [Umbro](../Page/Umbro.md "wikilink")

## 外部連結

  - [官方網站](http://www.rodajc.nl/)
  - [介紹球員網站](http://www.rodajcspelers.nl/)
  - [球迷網站](http://www.rodajcfans.nl/)
  - [球迷網站](http://www.rodaworld.nl/)
  - [球迷網站](http://www.koempel.nl/)
  - [1st
    官方球迷會網站](https://web.archive.org/web/20050401234225/http://www.sv-online.info/)
  - [球迷網站](http://www.fanproject.nl/)

[R](../Category/荷蘭足球俱樂部.md "wikilink")
[Category:1962年建立的足球會](../Category/1962年建立的足球會.md "wikilink")