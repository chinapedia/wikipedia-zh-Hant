**卡梅隆·杰罗梅**（，）[英格蘭職業足球員](../Page/英格蘭.md "wikilink")，司職[前鋒](../Page/前鋒_\(足球\).md "wikilink")，效力[英冠球會](../Page/英冠.md "wikilink")[打比郡](../Page/德比郡足球俱乐部.md "wikilink")。

## 生平

### 球會

謝路美於本地球隊Westend
Juniors開始足球訓練，後來轉至[哈德斯菲爾德](../Page/哈德斯菲爾德足球俱樂部.md "wikilink")，亦曾於[甘士比和](../Page/格林斯比镇足球俱乐部.md "wikilink")[錫周三接受足球訓練](../Page/錫周三.md "wikilink")\[1\]。2003年，謝路美在[英超球隊](../Page/英超.md "wikilink")[米杜士堡踢球](../Page/米杜士堡足球會.md "wikilink")，後來被球隊放棄，在[英冠球隊](../Page/英冠.md "wikilink")[卡迪夫城獲得合約](../Page/卡迪夫城足球俱樂部.md "wikilink")，得以繼續足球事業，更被[BBC形容為](../Page/BBC.md "wikilink")「卡迪夫城明日之星」，謝路美於2004-05賽季上陣32場射入7球，2005-06賽季更射入20球成為卡迪夫城的首席射手。因此獲[伯明翰城青睞](../Page/伯明翰城足球俱樂部.md "wikilink")。

2006年5月31日，謝路美通過體檢正式與[伯明翰城簽約](../Page/伯明翰城足球俱樂部.md "wikilink")，轉會費350萬[英鎊](../Page/英鎊.md "wikilink")\[2\]，[錫菲聯](../Page/錫菲聯.md "wikilink")、[西布朗](../Page/西布朗.md "wikilink")、[诺里奇城亦曾表示對謝路美有興趣](../Page/诺里奇城足球俱乐部.md "wikilink")。2006年9月12日，謝路美對[昆士柏流浪為伯明翰城射入首個入球](../Page/昆士柏流浪.md "wikilink")。謝路美助伯明翰城升上英超，於2007年8月25日對[打比郡僅](../Page/德比郡足球俱樂部.md "wikilink")32秒即射入首個英超入球，助伯明翰贏得[該季首場英超賽事](../Page/2007年至2008年英格蘭超級聯賽.md "wikilink")\[3\]。

2011年謝路美隨同伯明翰降班[英冠](../Page/英冠.md "wikilink")，於[新球季為球隊以後補身上陣](../Page/2011年至2012年英格蘭足球冠軍聯賽.md "wikilink")1場後，在8月31日[轉會窗關閉前一天轉投](../Page/轉會窗.md "wikilink")[英超球會](../Page/英超.md "wikilink")[史篤城](../Page/斯托克城足球俱乐部.md "wikilink")，簽約四年\[4\]。2013年9月3日於[轉會窗關閉前被借用到](../Page/轉會窗.md "wikilink")[水晶宮一整個球季](../Page/水晶宫足球俱乐部.md "wikilink")\[5\]。

2014年8月20日，謝路美轉投剛降班[英冠的](../Page/英冠.md "wikilink")[诺里奇城](../Page/诺里奇城足球俱乐部.md "wikilink")，簽約三年\[6\]。

### 國家隊

謝路美是[英格蘭21歲以下足球代表隊成員](../Page/英格蘭21歲以下足球代表隊.md "wikilink")，可參加2009年的[歐洲U-21足球錦標賽](../Page/歐洲U-21足球錦標賽.md "wikilink")。

## 榮譽

  - 伯明翰城

<!-- end list -->

  - [英格蘭聯賽盃](../Page/英格蘭聯賽盃.md "wikilink")：[2010/11年](../Page/2011年英格蘭聯賽盃決賽.md "wikilink")；

## 參考資料

## 外部連結

  - [soccerbase
    謝路美數據](https://web.archive.org/web/20080512011457/http://www.soccerbase.com/players_details.sd?playerid=38709)
  - [伯明翰城官方網站
    謝路美資料](https://web.archive.org/web/20081220053046/http://www.blues.premiumtv.co.uk/page/ProfilesDetail/0%2C%2C10412~30567%2C00.html)
  - [myspace
    謝路美網站](https://web.archive.org/web/20081114180429/http://profile.myspace.com/index.cfm?fuseaction=user.viewprofile)

[Category:西約克郡人](../Category/西約克郡人.md "wikilink")
[Category:英格蘭足球運動員](../Category/英格蘭足球運動員.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:米杜士堡球員](../Category/米杜士堡球員.md "wikilink")
[Category:卡迪夫城球員](../Category/卡迪夫城球員.md "wikilink")
[Category:伯明翰城球員](../Category/伯明翰城球員.md "wikilink")
[Category:史篤城球員](../Category/史篤城球員.md "wikilink")
[Category:諾域治球員](../Category/諾域治球員.md "wikilink")
[Category:打比郡球員](../Category/打比郡球員.md "wikilink")
[Category:英格蘭足球聯賽球員](../Category/英格蘭足球聯賽球員.md "wikilink")

1.
2.
3.
4.
5.
6.