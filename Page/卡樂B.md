[Calbee_logo.svg](https://zh.wikipedia.org/wiki/File:Calbee_logo.svg "fig:Calbee_logo.svg")
[Calbee_Hiroshima_Factory.JPG](https://zh.wikipedia.org/wiki/File:Calbee_Hiroshima_Factory.JPG "fig:Calbee_Hiroshima_Factory.JPG")[廿日市市的一座工場](../Page/廿日市市.md "wikilink")\]\]

**卡樂B**（，；）是[日本一家以](../Page/日本.md "wikilink")[零食為主力的](../Page/零食.md "wikilink")[食品](../Page/食品.md "wikilink")[生產商](../Page/生產商.md "wikilink")，於1949年在[廣島市宇品以松尾糧食工業株式會社的名稱成立](../Page/廣島市.md "wikilink")，1955年改為現名。

## 歷史

1964年起，卡樂B推出以[瀨戶內海小](../Page/瀨戶內海.md "wikilink")[蝦所製成的卡樂B](../Page/蝦.md "wikilink")[蝦條](../Page/蝦條.md "wikilink")，受到市場歡迎，令卡樂B的名字開始著名。1973年，卡樂B把總社由廣島遷移至東京。現在本社位於[東京都](../Page/東京都.md "wikilink")[千代田區](../Page/千代田區.md "wikilink")。亦有贊助[J
League球隊](../Page/J_League.md "wikilink")[廣島三箭](../Page/廣島三箭.md "wikilink")。

卡樂B名稱是由[鈣質與](../Page/鈣.md "wikilink")[維他命B1的的日文](../Page/維他命B1.md "wikilink")[片假名字母合併而成](../Page/片假名.md "wikilink")。

1976年，卡樂B與香港[四洲集團合作](../Page/四洲集團.md "wikilink")，引入卡樂B蝦條、薯片及各款小食。1994年成立卡樂B四洲有限公司，設廠於[西貢](../Page/西貢區.md "wikilink")，2000年遷往[將軍澳工業邨至今](../Page/將軍澳工業邨.md "wikilink")。

2011年3月11日，卡樂B株式會社正式於[東京證券交易所股票上市](../Page/東京證券交易所.md "wikilink")。

2012年6月，卡樂比與台灣[味全合資設立台北卡樂比食品股份有限公司](../Page/味全.md "wikilink")，卡樂比持股51%、味全持股49%，並在味全斗六廠生產產品；但於2016年4月結束合作並解散台北卡樂比食品\[1\]。卡樂比於2016年8月獨資設立台灣卡樂比股份有限公司\[2\]。

2012年8月，卡樂比、[伊藤忠商事](../Page/伊藤忠商事.md "wikilink")、康師傅方便食品投資（中國）有限公司合資成立卡樂（杭州）食品有限公司。2015年11月4日，[康師傅控股有限公司公告](../Page/康師傅控股有限公司.md "wikilink")，由於經營理念、見解不同，康師傅決定收購卡樂比與伊藤忠商事持有的卡樂（杭州）食品有限公司所有股權，並承接現有業務繼續經營\[3\]。

### 產品回收事件

2012年11月20日，卡樂B於日本官方網站上發表聲明指出懷疑有玻璃碎片混入薯片中，決定回收有關批貨 (5,345,000包)
及向消費者道歉。公司表示，於此前接收到兩宗消費者投訴，表達於進食卡樂B薯片時被薯片割傷了嘴部，公司後來調查發現日本湖南工場的照明設備的保護蓋部分破損，因而懷疑可能有玻璃碎混入產品內，最終作出了是次決定\[4\]。

## 香港發售之產品類別

  - 卡樂B蝦條
  - 卡樂B[馬鈴薯片](../Page/馬鈴薯片.md "wikilink")
  - 卡樂B粟一燒
  - 卡樂B香脆小食
  - 卡樂B宅卡B（Jagabee）薯條（杯裝及袋裝）
  - 卡樂B卡樂脆薯條

## 參考注釋

## 外部連結

  - [日本卡樂B](http://www.calbee.co.jp/)
  - [香港卡樂B](http://www.calbee.com.hk/)

[K](../Category/日本食品公司.md "wikilink")
[Category:馬鈴薯食品](../Category/馬鈴薯食品.md "wikilink")

1.  [薯條先生不走囉　日零食龍頭Calbee　擬在台獨資建廠](http://www.appledaily.com.tw/realtimenews/article/new/20160514/861176/)
2.  [經濟部商業司─公司資料查詢](http://gcis.nat.gov.tw/pub/cmpy/cmpyInfoAction.do?method=detail&banNo=52233162)
3.  [康師傅與卡樂比分手
    味全：台灣合作不受影響](http://www.chinatimes.com/newspapers/20151105000229-260206)
4.  [卡樂B薯片玻璃碎食損嘴](http://orientaldaily.on.cc/cnt/news/20121122/00176_025.html)
    《東方日報》 2012年11月22日