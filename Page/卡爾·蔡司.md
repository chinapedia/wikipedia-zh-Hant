[Carl_Zeiss_47.png](https://zh.wikipedia.org/wiki/File:Carl_Zeiss_47.png "fig:Carl_Zeiss_47.png")[Microscope_Zeiss_1879.jpg](https://zh.wikipedia.org/wiki/File:Microscope_Zeiss_1879.jpg "fig:Microscope_Zeiss_1879.jpg")
**卡爾·蔡司**（**Carl
Zeiss**，），[光學仪器企业家](../Page/光學.md "wikilink")，以其创立的[蔡司公司闻名于世](../Page/蔡司公司.md "wikilink")，本人對於現代[透鏡的製造生產也貢獻良多](../Page/透鏡.md "wikilink")。蔡司在[德國](../Page/德國.md "wikilink")[威瑪起家](../Page/威瑪.md "wikilink")，到了1840年代，已成為知名的透鏡製作者，他制造的高品质透镜以孔径大、成像清晰著称。他在[耶拿自设工场](../Page/耶拿.md "wikilink")，開始他的制造透鏡的生涯，起初制造[显微镜镜头](../Page/显微镜.md "wikilink")，后来制造高品质[照相机镜头](../Page/照相机.md "wikilink")，1888年在開始透鏡生涯的耶拿逝世。

## 生平

蔡司在[德國威瑪的語言學校學習](../Page/德國.md "wikilink")，並师从弗德烈·科爾納博士（Dr.Friedrich
Körne）習藝，當時科爾納正在研究用於[望遠鏡目鏡的玻璃](../Page/望遠鏡.md "wikilink")。他随后在[耶拿大學學習](../Page/耶拿大學.md "wikilink")[數學](../Page/數學.md "wikilink")、[實驗物理學](../Page/實驗物理學.md "wikilink")、[人類學](../Page/人類學.md "wikilink")、[礦物學](../Page/礦物學.md "wikilink")、和光學。七年後，他自己创立一個小工作室，當時工作室中工具十分貧乏。他做出許多透鏡，但只得到一點認同，直到1847年他雇用他的第一個學徒。同年他的前老師科爾納去世，促使蔡司毕生致力于顯微鏡事业。

1847年蔡司開始將全部时间放在制造顯微鏡上。他的第一项革新是制造只用一片单透鏡的簡單型顯微鏡，適合用於解剖的工作。在第一年生產這批顯微鏡時，他賣了大約23台。他很快意識到他需要新挑戰，因此開始研發[複合式顯微鏡](../Page/複合式顯微鏡.md "wikilink")。隨後，他創造了Stand
I，並於1857年打進了銷售市場。

在1861年凭他的設計，在圖林根州工業展览會上獲頒一枚金牌。这些顯微鏡被認為是德國最佳的科學儀器。这时候他有大約20名員工，他的事業仍然持續成長。1866年蔡司工廠賣出第1000台顯微鏡。同年物理學家[恩斯特·阿贝博士以研究導師的身份加入蔡司工作室](../Page/恩斯特·阿贝.md "wikilink")，兩人一同研究光學產品的科學基礎原理，1872年他們聯合製作出了複合式顯微鏡，這台複合式顯微鏡是現代所有複合式顯微鏡的始祖。

在這個期間，蔡司做出至此为止最好的透鏡。理論上，[阿贝正弦条件能大大改善透鏡品質](../Page/阿贝正弦条件.md "wikilink")，但問題在于当时沒有足夠強度的玻璃來測試這個學說。

所幸阿贝博士認識了[奥托·肖特](../Page/奥托·肖特.md "wikilink")。30歲的肖特是位剛获得博士學位的玻璃化學家。1879年他們合作，很快在1886年生產出新型玻璃，能充分地表現阿贝正弦學說。這種新型玻璃為新顯微鏡的[物鏡開闢出一條新的道路](../Page/物鏡.md "wikilink")：[消色差物鏡](../Page/消色差.md "wikilink")（apochromatic
lens）。

肖特專門生產用於新型卡爾·蔡司顯微鏡的玻璃，1884年建立了一個全面的工廠，其所有權屬於蔡司、阿贝、肖特，稱為Jena Schott &
Genossen光學工廠。耶拿玻璃也因此成為世界上最有名的玻璃。

蔡司在創造出劃時代的顯微鏡后不久後的1888年去世。蔡司在遺囑中將股權轉移給兒子洛迪里克（Roderick）。洛迪里克將股權出售給阿贝，1889年，阿贝成立卡爾·蔡司基金會。該基金會又成立了一個新的集團，作為蔡司公司的所有者。

## 参看

  - [肖特玻璃厂](../Page/肖特玻璃厂.md "wikilink")

## 外部連結

  - [蔡司公司在網頁上對公司歷史的介紹](http://www.zeiss.com/C12567BE0045ACF1/allBySubject/20D1FA535B9D8B08C1256C020024B193)

[Z](../Category/德国发明家.md "wikilink")
[Z](../Category/德国企业家.md "wikilink")
[Z](../Category/德國配鏡師.md "wikilink")
[Z](../Category/显微镜学家.md "wikilink")
[Z](../Category/圖林根人.md "wikilink")
[Z](../Category/耶拿大學校友.md "wikilink")