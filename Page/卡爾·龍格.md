**卡爾·龍格**（**Carl Runge**
）是一位[德國](../Page/德國.md "wikilink")[數學家](../Page/數學家.md "wikilink")、
[物理學家](../Page/物理學家.md "wikilink")、[光譜學家](../Page/光譜學.md "wikilink")。在[數值分析學裏](../Page/數值分析.md "wikilink")，他是[龍格－庫塔法的共同發明者與共同命名者](../Page/龍格－庫塔法.md "wikilink")。

幼时，龍格在[古巴](../Page/古巴.md "wikilink")[哈瓦那度過了几年](../Page/哈瓦那.md "wikilink")；在那期間，他的父親**尤利烏斯·龍格**是駐[古巴的](../Page/古巴.md "wikilink")[丹麥外交官](../Page/丹麥.md "wikilink")。之后全家回到了[不來梅](../Page/不來梅.md "wikilink")。他父亲于1864年早逝。

1880年，他在[柏林大學获取數學博士](../Page/柏林大學.md "wikilink")，导师是被譽為「現代[分析之父](../Page/數學分析.md "wikilink")」的著名德國數學家[卡爾·魏爾施特拉斯](../Page/卡爾·魏爾施特拉斯.md "wikilink")。1886年，他迁至[漢諾威](../Page/漢諾威.md "wikilink")，成為[漢諾威大學的教授](../Page/漢諾威大學.md "wikilink")。

他的興趣包括數學，[光譜學](../Page/光譜學.md "wikilink")，[大地測量學](../Page/大地測量學.md "wikilink")，與[天體物理學](../Page/天體物理學.md "wikilink")。除了純數學以外，他也從事很多涉及實驗的工作。他跟[海因里希·凱瑟一同研究各種元素的](../Page/海因里希·凱瑟.md "wikilink")[譜線](../Page/譜線.md "wikilink")，又將研究的結果應用在[天體](../Page/天文學.md "wikilink")[光譜學](../Page/光譜學.md "wikilink")。

1904年，受[哥廷根大學教授](../Page/哥廷根大學.md "wikilink")[菲利克斯·克萊因的主動邀請](../Page/菲利克斯·克萊因.md "wikilink")，他同意去那裡教書。1925
年，他在[哥廷根大學退休](../Page/哥廷根大學.md "wikilink")。

[月球的龍格](../Page/月球.md "wikilink")[隕石坑](../Page/隕石坑.md "wikilink") ()
是因他而命名的。

## 參閱

  - [龍格現象](../Page/龍格現象.md "wikilink")
  - [龍格－庫塔法](../Page/龍格－庫塔法.md "wikilink")
  - [拉普拉斯-龍格-冷次向量](../Page/拉普拉斯-龍格-冷次向量.md "wikilink")

## 著作

  - [Ueber die Krümmung, Torsion und geodätische Krümmung der auf einer
    Fläche gezogenen
    Curven](http://www-gdz.sub.uni-goettingen.de/cgi-bin/digbib.cgi?PPN317854747)
    (博士論文, Friese, 1880)
  - [Analytische Geometrie der
    Ebene](http://name.umdl.umich.edu/ABN6667.0001.001) (B.G. Teubner,
    Leipzig, 1908)
  - [圖解方法：在哥倫比亞大學一個課程的講課， 1909 年10 月至 1910年 1
    月](http://www.archive.org/details/graphmethods00rungrich)
    (Columbia University Press, New York, 1912)
  - Carl Runge und Hermann König [Vorlesungen über numerisches
    Rechnen](http://www-gdz.sub.uni-goettingen.de/cgi-bin/digbib.cgi?PPN373207646)
    (Springer, Heidelberg, 1924)
  - [Graphischen
    Methoden](http://www.archive.org/details/graphischenmetho003786mbp)
    (Teubner, 1928)

## 自傳

  - F. Paschen:
    \[<http://adsabs.harvard.edu//full/seri/ApJ>../0069//0000317.000.html
    "卡爾·龍格"\], *Astrophysical Journal* **69**:317–321, 1929. 。
  - Iris Runge: *Carl Runge und sein wissenschaftliches Werk*,
    Vandenhoeck & Ruprecht, Göttingen 1949。

## 外連

  -
  - [卡爾·龍格自傳](http://numericalmethods.eng.usf.edu/anecdotes/runge.html)

  -
[Category:20世紀數學家](../Category/20世紀數學家.md "wikilink")
[Category:19世紀數學家](../Category/19世紀數學家.md "wikilink")
[Category:德國數學家](../Category/德國數學家.md "wikilink")
[Category:德國物理學家](../Category/德國物理學家.md "wikilink")
[Category:哥廷根大學教師](../Category/哥廷根大學教師.md "wikilink")
[Category:漢諾瓦大學教師](../Category/漢諾瓦大學教師.md "wikilink")
[Category:柏林洪堡大學校友](../Category/柏林洪堡大學校友.md "wikilink")
[Category:不來梅人](../Category/不來梅人.md "wikilink")