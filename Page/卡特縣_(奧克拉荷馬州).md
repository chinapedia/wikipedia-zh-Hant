**卡特县**（**Carter County,
Oklahoma**）是[美國](../Page/美國.md "wikilink")[奧克拉荷馬州南部的一個縣](../Page/奧克拉荷馬州.md "wikilink")。面積2,159平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口45,621人。縣治[阿德摩爾](../Page/阿德摩爾_\(奧克拉荷馬州\).md "wikilink")（Ardmore）。

成立於1907年。縣名是紀念當地一個顯要家族。

[C](../Category/俄克拉何马州行政区划.md "wikilink")