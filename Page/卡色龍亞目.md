**卡色龍亞目**（Caseasauria）是早期[合弓動物的兩個主要演化支之一](../Page/合弓動物.md "wikilink")，另一演化支是[真盤龍亞目](../Page/真盤龍亞目.md "wikilink")。牠們現在只發現於[二疊紀](../Page/二疊紀.md "wikilink")，包括兩個外表上不同的科：小型、[食蟲性或](../Page/食蟲性.md "wikilink")[肉食性的](../Page/肉食性.md "wikilink")[始蜥龍科](../Page/始蜥龍科.md "wikilink")，以及大型[草食性的](../Page/草食性.md "wikilink")[卡色龍科](../Page/卡色龍科.md "wikilink")。這兩科動物擁有許多相同的獨有特徵，這些特徵是關於口鼻部與外鼻孔的型態，卡色龍科可能從[始蜥龍科演化而來](../Page/始蜥龍科.md "wikilink")。

卡色龍類的祖先，可回溯至[石炭紀晚期](../Page/石炭紀.md "wikilink")（[賓夕法尼亞紀](../Page/賓夕法尼亞紀.md "wikilink")）的某種外表類似[蜥蜴的食蟲性或](../Page/蜥蜴.md "wikilink")[雜食性合弓動物](../Page/雜食性.md "wikilink")，例如[始祖單弓獸](../Page/始祖單弓獸.md "wikilink")（已知年代最早的合弓類動物）。在早[二疊紀的末期](../Page/二疊紀.md "wikilink")，卡色龍類是大量且成功的動物。到了中[二疊紀](../Page/二疊紀.md "wikilink")，更為先進、成功的[獸孔目動物的出現](../Page/獸孔目.md "wikilink")，而導致卡色龍類數量變少，最後滅絕，牠們沒有存活到晚二疊紀\[1\]。

合弓綱的早期物種，目前很難建立明確的[種系發生學分析](../Page/種系發生學.md "wikilink")，包含卡色龍亞目。在一個1983年的研究中，[蜥代龍科被歸類於卡色龍亞目](../Page/蜥代龍科.md "wikilink")，而卡色龍亞目是合弓綱的最原始旁支。數年後，蜥代龍科被移除於卡色龍亞目，被歸類到更衍化的位置。大部分研究根據頭顱骨特徵，而將卡色龍亞目視為合弓綱的最原始物種之一。在2012年，R.J.
Benson提出不同看法，他根據身體骨骼特徵，而非頭顱骨特徵，而將[蛇齒龍科](../Page/蛇齒龍科.md "wikilink")、蜥代龍科是最原始的合弓類動物，而卡色龍類位於更衍化的位置。

以下[演化樹來自於R](../Page/演化樹.md "wikilink").J. Benson的2012年研究\[2\]︰

## 參考資料

  - Robert Reisz, 1986, *Handbuch der Paläoherpetologie – Encyclopedia
    of Paleoherpetology, Part 17A Pelycosauria* Verlag Friedrich Pfeil,
    ISBN 3-89937-032-5
  - Michel Laurin and Reisz, R. R., 1997, [Autapomorphies of the main
    clades of
    synapsids](http://tolweb.org/accessory/Synapsid_Classification_&_Apomorphies?acc_id=466)

## 外部連結

  - [Palaeos Vertebrates 390.100
    Synapsida](https://web.archive.org/web/20060503193950/http://www.palaeos.com/Vertebrates/Units/Unit390/100.html)

[\*](../Category/卡色龍亞目.md "wikilink")

1.  Maddin, H.C., Sidor, C.A. & Reisz, R.R. 2008. Cranial anatomy of
    Ennatosaurus tecton (Synapsida: Caseidae) from the Middle Permian of
    Russia and the evolutionary relationships of Caseidae. Journal of
    Vertebrate Paleontology (28): 160-180
2.