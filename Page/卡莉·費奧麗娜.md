**卡莉·費奧莉娜**（**Carly**
**Fiorina**，）生於[美國](../Page/美國.md "wikilink")[德州](../Page/得克萨斯州.md "wikilink")[奧斯丁](../Page/奧斯丁.md "wikilink")，曾任[惠普公司](../Page/惠普公司.md "wikilink")[董事長兼](../Page/董事長.md "wikilink")[CEO](../Page/CEO.md "wikilink")，是美國商業及科技界中最具影響力的女性之一。費奧莉娜于2015年5月4日宣布在美国共和党内争取[2016年大选总统候选人提名](../Page/2016年美國總統選舉.md "wikilink")，并在退选后直至2016年5月3日短暂成为了[泰德·克鲁兹的竞选搭档](../Page/泰德·克鲁兹.md "wikilink")。

## 個人生涯

費奧麗娜1976年畢業於[史丹佛大學](../Page/史丹佛大學.md "wikilink")，主修西洋[中古史與](../Page/中古史.md "wikilink")[哲學](../Page/哲學.md "wikilink")，隨後同年曾就讀[加州大學洛杉磯分校法學院](../Page/加州大學洛杉磯分校.md "wikilink")，但一個學期後就退學\[1\]，轉去地產公司當了六個月接待員，升至經紀後，離開到[意大利](../Page/意大利.md "wikilink")[博洛尼亞教英語](../Page/博洛尼亞.md "wikilink")。\[2\]她懂得說意大利語。\[3\]

1980年又在[馬利蘭大學學院市分校](../Page/馬利蘭大學學院市分校.md "wikilink")取得[市場學的](../Page/市場學.md "wikilink")[企業管理碩士](../Page/企業管理碩士.md "wikilink")，及1989年在[MIT史隆管理學院的](../Page/MIT史隆管理學院.md "wikilink")計劃下取得[管理學的](../Page/管理學.md "wikilink")[理学碩士学位](../Page/理学碩士.md "wikilink")。

費奧麗娜在1999年7月進入惠普公司擔任董事長及CEO；在此之前，她曾服務於美國AT\&T及擔任[朗訊全球服務事業群總裁](../Page/朗訊科技.md "wikilink")，並主導朗訊从AT\&T的分拆\[4\]。

費奧麗娜曾經連續六年被美國[財星雜誌評選為美國商業界最具影響力的女性](../Page/財星雜誌.md "wikilink")。惟她在2002年主導惠普公司與對手[康柏的合併案後](../Page/康柏.md "wikilink")，沖淡了惠普原先在影像事業部門的獲利，而讓原先穩健經營的惠普開始在季報中出現不穩定的情況，遂與惠普董事會漸行漸遠，並逐漸被董事會削減職務。2005年2月9日，她在董事會的通知下，同時被免去董事長及CEO兩項職務。

費奧麗娜曾經2度拜訪[台灣](../Page/中華民國.md "wikilink")。2004年拜訪台灣時，曾經蒙[總統](../Page/總統.md "wikilink")[陳水扁接見](../Page/陳水扁.md "wikilink")，並受婉轉請求將台灣在[三通議題上的誠意轉達給](../Page/三通.md "wikilink")[中華人民共和國政府](../Page/中華人民共和國.md "wikilink")，但隨後經惠普公司以新聞稿的方式否認。

費奧麗娜曾在[2008年美国大选表態支持](../Page/2008年美国大选.md "wikilink")[共和党總統參選人](../Page/共和黨_\(美國\).md "wikilink")[約翰·麥凱恩](../Page/約翰·麥凱恩.md "wikilink")。

## 參考文獻

## 外部链接

  - [卡莉·費奧麗娜](https://carlyforpresident.com/)

  -
  -
[Category:21世纪美国作家](../Category/21世纪美国作家.md "wikilink")
[Category:21世纪女性作家](../Category/21世纪女性作家.md "wikilink")
[Category:美國基督徒](../Category/美國基督徒.md "wikilink")
[Category:美国回忆录撰写人](../Category/美国回忆录撰写人.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:美國慈善家](../Category/美國慈善家.md "wikilink")
[Category:加州企業家](../Category/加州企業家.md "wikilink")
[Category:加利福尼亚州共和党人](../Category/加利福尼亚州共和党人.md "wikilink")
[Category:惠普人物](../Category/惠普人物.md "wikilink")
[Category:AT\&T人物](../Category/AT&T人物.md "wikilink")
[Category:奧斯汀人](../Category/奧斯汀人.md "wikilink")
[Category:史丹佛大學校友](../Category/史丹佛大學校友.md "wikilink")
[Category:馬里蘭大學校友](../Category/馬里蘭大學校友.md "wikilink")
[Category:麻省理工學院校友](../Category/麻省理工學院校友.md "wikilink")
[Category:2016年美國總統選舉候選人](../Category/2016年美國總統選舉候選人.md "wikilink")
[Category:美國共和黨黨員](../Category/美國共和黨黨員.md "wikilink")

1.
2.  Fiorina, *Tough Choices*, p. 21.
3.  <https://books.google.com/books?id=SE99BAAAQBAJ&pg=PT89&dq=carly+fiorina+spoke+italian&hl=en&sa=X&ved=0CDEQ6AEwAGoVChMIy_nUoIX1xwIVBjKICh2MRAIQ#v=onepage&q=carly%20fiorina%20spoke%20italian&f=false>
4.  《勇敢抉擇：Carly Fiorina回憶錄》，天下雜誌社，2006年10月25日初版，ISBN 9789866948244