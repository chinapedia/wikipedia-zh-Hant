**卡萊爾县**（**Carlisle County,
Kentucky**）是[美國](../Page/美國.md "wikilink")[肯塔基州西南部的一個縣](../Page/肯塔基州.md "wikilink")，西隔[密西西比河與](../Page/密西西比河.md "wikilink")[密蘇里州相望](../Page/密蘇里州.md "wikilink")。面積515平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口5,351人。縣治[巴德韋爾](../Page/巴德韋爾_\(肯塔基州\).md "wikilink")（Bardwell）。

成立於1886年4月3日。縣名紀念[聯邦眾議員](../Page/美國眾議院.md "wikilink")、[議長](../Page/美國眾議院議長.md "wikilink")、[參議員](../Page/美國參議院.md "wikilink")、[財政部長](../Page/美國財政部長.md "wikilink")[約翰·G·卡萊爾](../Page/約翰·G·卡萊爾.md "wikilink")
（John Griffin Carlisle）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[C](../Category/肯塔基州行政區劃.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.