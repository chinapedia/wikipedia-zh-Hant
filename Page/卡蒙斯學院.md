**賈梅士學院**（）是[葡萄牙對外推廣葡萄牙](../Page/葡萄牙.md "wikilink")[語言和](../Page/語言.md "wikilink")[文化的官方機構](../Page/文化.md "wikilink")，名稱從葡萄牙的愛國[詩人](../Page/詩人.md "wikilink")[賈梅士的姓氏而來](../Page/賈梅士.md "wikilink")。

該學院的前身是“葡萄牙語言文化學會”，成立於1928年。1992年，賈梅士學院成立，取代葡萄牙語言文化學會。該學院原屬葡萄牙教育部管轄，於1994年起轉由外交部管轄，但享有行政自治。

該學院旨在於高等教育層次，向世界推廣葡萄牙語言和文化，並促進與第三世界國家在[教育](../Page/教育.md "wikilink")、[科技](../Page/科技.md "wikilink")、[文化](../Page/文化.md "wikilink")、[體育](../Page/體育.md "wikilink")、[青年和](../Page/青年.md "wikilink")[大眾媒體事務簽訂文化合作協議的工作](../Page/大眾媒體.md "wikilink")。

與其他推廣語言文化的官方機構（例如[英國文化協會](../Page/英國文化協會.md "wikilink")、[歌德学院](../Page/歌德学院.md "wikilink")）不同，賈梅士學院不直接開設固定的語言學校教授葡語，而是間接地透過由世界各地[大學的葡語系和講師等所組成的網絡](../Page/大學.md "wikilink")，由學院冠以“葡語中心”（Centro
de Língua
Portuguesa）等各種名義來進行。學院宣稱目前該網絡共有175名成員，共3萬名學生，並對200名葡語教師支付薪酬和提供支援，同時又發放90份獎學金給學生進修葡萄牙語言和文化。

另一方面，該學院在15個國家開設了15所文化中心和4所地區中心，其中7所設於葡語國家，其餘設於葡萄牙[移民較集中的國家](../Page/移民.md "wikilink")，或在歷史上與葡萄牙較有淵源的國家，例如[巴西](../Page/巴西.md "wikilink")、[中國](../Page/中國.md "wikilink")、[日本和](../Page/日本.md "wikilink")[泰國](../Page/泰國.md "wikilink")。

在中國，與賈梅士學院和東方葡萄牙學會合作開設葡語課程的高等院校包括：[暨南大學](../Page/暨南大學.md "wikilink")、[香港大學](../Page/香港大學.md "wikilink")、[北京外國語大學](../Page/北京外國語大學.md "wikilink")、[中國傳媒大學](../Page/中國傳媒大學.md "wikilink")、[上海外國語大學和](../Page/上海外國語大學.md "wikilink")[華僑大學](../Page/華僑大學.md "wikilink")（目前華僑大學之教席從缺）。所有這些院校都以“賈梅士學院
− 東方葡萄牙學會教學網絡”（Rede de Docência
IC/IPOR）的名義來舉辦葡語課程。而在[澳門的](../Page/澳門.md "wikilink")[東方葡萄牙學會](../Page/東方葡萄牙學會.md "wikilink")，其語言中心也是賈梅士學院的冠名機構。但是，賈梅士學院只是東方葡萄牙學會的組織成員之一，而並非該學院的附屬機構，兩者並無直接的關係。

## 另見條目

  - [東方葡萄牙學會](../Page/東方葡萄牙學會.md "wikilink")

## 外部連結

  - [賈梅士學院網站（葡文）](http://www.instituto-camoes.pt/)
  - [東方葡萄牙學會網站（葡文）](http://www.ipor.org.mo/)

[Category:葡萄牙語](../Category/葡萄牙語.md "wikilink")
[Category:葡萄牙文化](../Category/葡萄牙文化.md "wikilink")
[Category:阿斯图里亚斯亲王奖获得者](../Category/阿斯图里亚斯亲王奖获得者.md "wikilink")
[Category:文化推广组织](../Category/文化推广组织.md "wikilink")