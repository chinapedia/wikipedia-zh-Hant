在[托爾金](../Page/托爾金.md "wikilink")（J. R. R.
Tolkien）奇幻小說的[中土大陸裡](../Page/中土大陸.md "wikilink")，**卡蘭拉斯山**（Caradhras），又稱**紅角**（Redhorn），是[迷霧山脈](../Page/迷霧山脈.md "wikilink")（Misty
Mountains）其中一個最巨大的山峰\[1\]。托爾金曾表示它的高度為17,500英尺\[2\]。

卡蘭拉斯山的**紅角隘口**（Redhorn
Pass）連接西面[諾多精靈的舊地](../Page/諾多精靈.md "wikilink")[伊瑞詹](../Page/伊瑞詹.md "wikilink")（Eregion）及東面的[安都因河](../Page/安都因河.md "wikilink")（Anduin）河谷。[凱薩督姆](../Page/凱薩督姆.md "wikilink")（Khazad-dûm）被廢棄後，紅角隘口被[精靈用作來往](../Page/精靈_\(中土大陸\).md "wikilink")[羅斯洛立安](../Page/羅斯洛立安.md "wikilink")（Lórien）及[伊利雅德](../Page/伊利雅德.md "wikilink")（Eriador）兩地。

卡蘭拉斯山（[矮人語稱為](../Page/矮人語.md "wikilink")**巴拉辛巴爾**
*Barazinbar*）是[摩瑞亞山脈](../Page/摩瑞亞.md "wikilink")（Mountains
of
Moria）的其中一個山峰。其下則是[矮人領地凱薩督姆的所在](../Page/矮人_\(中土大陸\).md "wikilink")。卡蘭拉斯山蘊含[秘銀](../Page/秘銀.md "wikilink")（mithril），採礦者喚醒了這裡的[炎魔](../Page/炎魔.md "wikilink")（Balrog）。

卡蘭拉斯山被矮人形容為殘酷，長久以來負有不良聲譽。紅角隘口亦以危險見稱。[愛隆](../Page/愛隆.md "wikilink")（Elrond）之妻[凱勒布理安](../Page/凱勒布理安.md "wikilink")（Celebrían）在紅角隘口遭[半獸人攻擊](../Page/半獸人_\(中土大陸\).md "wikilink")。這通道也被[哈比人用以由](../Page/哈比人.md "wikilink")[格拉頓平原](../Page/格拉頓平原.md "wikilink")（Gladden
Fields）遷至伊利雅德。

[魔戒遠征隊曾嘗試穿越紅角隘口](../Page/魔戒遠征隊.md "wikilink")，但被暴風雪所組，以失敗告終，最後不得不從地底的摩瑞亞礦坑越過迷霧山脈。[金靂認為是卡蘭拉斯山的力量阻擾他們通過該處](../Page/金靂.md "wikilink")。

## 改編

在[彼得·傑克森的電影](../Page/彼得·傑克森.md "wikilink")《[魔戒首部曲：魔戒現身](../Page/魔戒首部曲：魔戒現身.md "wikilink")》，魔戒遠征隊越過卡蘭拉斯山時，是[薩魯曼在遠方招喚暴風雪阻擾他們](../Page/薩魯曼.md "wikilink")，並非卡蘭拉斯山本身的力量。

## 參考資料

<references />

[de:Regionen und Orte in Tolkiens
Welt\#Nebelgebirge](../Page/de:Regionen_und_Orte_in_Tolkiens_Welt#Nebelgebirge.md "wikilink")
[nl:Roodhoornpas\#Caradhras](../Page/nl:Roodhoornpas#Caradhras.md "wikilink")
[pl:Lista gór i wzgórz
Śródziemia\#Caradhras](../Page/pl:Lista_gór_i_wzgórz_Śródziemia#Caradhras.md "wikilink")
[pt:Hithaeglir\#Geografia](../Page/pt:Hithaeglir#Geografia.md "wikilink")
[sv:Platser i Tolkiens
värld\#Caradhras](../Page/sv:Platser_i_Tolkiens_värld#Caradhras.md "wikilink")

[C](../Category/中土大陸的地理.md "wikilink")

1.
2.  [Wayne G. Hammond](../Page/韋恩·G·哈蒙德.md "wikilink") & Christina Scull
    (1995), *J. R. R. Tolkien: Artist and Illustrator*, HarperCollins,
    figure 158 (p.163) & p.167;