**卢多维奇·马格宁**（**Ludovic
Magnin**，），[瑞士足球運動員](../Page/瑞士.md "wikilink")，擔任[左閘](../Page/左閘.md "wikilink")，2010年冬季[轉會窗重返](../Page/轉會窗.md "wikilink")[瑞士加盟](../Page/瑞士.md "wikilink")[蘇黎世](../Page/蘇黎世足球俱樂部.md "wikilink")，亦是[瑞士國家足球隊的主力](../Page/瑞士國家足球隊.md "wikilink")。麥連的罰球技術不俗，經常擔任主罰者。

2008年2月4日，麥連與[史特加續約至](../Page/史特加足球會.md "wikilink")2010年\[1\]。

自2000年入選國家隊至今，麥連共代表瑞士53次，曾參加[2004年歐洲國家盃](../Page/2004年歐洲國家盃.md "wikilink")、[2006年世界盃和](../Page/2006年世界盃.md "wikilink")[2008年歐洲國家盃](../Page/2008年歐洲國家盃.md "wikilink")。他在[2008年歐洲國家盃中接替受傷的](../Page/2008年歐洲國家盃.md "wikilink")[阿歷山大·費爾擔任隊長](../Page/阿歷山大·費爾.md "wikilink")。

著名足球評述人黃興桂對他冠以假洛賓的稱號。

## 榮譽

  - 雲達不來梅

<!-- end list -->

  - [德國甲組聯賽冠軍](../Page/德國甲組聯賽.md "wikilink")：2003-04賽季
  - [德國足協杯](../Page/德國足協杯.md "wikilink")：2004年

<!-- end list -->

  - 史特加:

<!-- end list -->

  - [德國甲組聯賽冠軍](../Page/德國甲組聯賽.md "wikilink")：2006-07賽季

## 參考資料

## 外部連結

  - [fussballdaten.de
    麥連職業數據](http://www.fussballdaten.de/spieler/magninludovic/)

[Category:瑞士足球運動員](../Category/瑞士足球運動員.md "wikilink")
[Category:雲達不來梅球員](../Category/雲達不來梅球員.md "wikilink")
[Category:史特加球員](../Category/史特加球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")

1.