**卢恰诺·弗洛里迪**（**Luciano
Floridi**，[罗马大学](../Page/罗马大学.md "wikilink")、[华威大学](../Page/华威大学.md "wikilink")[哲学](../Page/哲学.md "wikilink")[硕士和](../Page/硕士.md "wikilink")[博士](../Page/博士.md "wikilink")、[牛津大学](../Page/牛津大学.md "wikilink")[文学硕士](../Page/文学硕士.md "wikilink")）是意大利科技哲学和伦理学方面
最有影响的思想家之一。\[1\]
他在[怀疑论方面的研究很有成果并且是](../Page/怀疑论.md "wikilink")[信息哲学和](../Page/信息哲学.md "wikilink")[信息伦理的创始人](../Page/信息伦理.md "wikilink")。

## 参考

## 外部链接

  - [个人主页及论文](http://www.philosophyofinformation.net/)
  - [《哲学和计算机时事通讯》采访卢恰诺·弗洛里迪](https://web.archive.org/web/20080613181938/http://www.philosophyofinformation.net/pdf/apapaci.pdf)
  - [个人传记](https://web.archive.org/web/20080513154630/http://www.blackwellpublishing.com/pci/author.htm)
  - [个人传记](https://web.archive.org/web/20080907203824/http://www.philosophyofinformation.net/pdf/auto.pdf)

[Category:意大利哲學家](../Category/意大利哲學家.md "wikilink")
[Category:羅馬大學校友](../Category/羅馬大學校友.md "wikilink")
[Category:華威大學校友](../Category/華威大學校友.md "wikilink")

1.  Carl Mitcham, 《Encyclopedia of Science, Technology, and Ethics》
    (Macmillan, 2005), 网页 [*Italian
    Perspectives*](http://www.bookrags.com/Italian_people).