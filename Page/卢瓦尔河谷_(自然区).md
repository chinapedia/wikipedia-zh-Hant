[Chateau_Valencay_20050726.jpg](https://zh.wikipedia.org/wiki/File:Chateau_Valencay_20050726.jpg "fig:Chateau_Valencay_20050726.jpg")
**卢瓦尔河谷地区**（）是一个[法国的自然区](../Page/法国自然区.md "wikilink")，被称为“法国的花园”和“法语的摇篮”，以其高质量的建筑遗产著称。这些建筑不仅分布在[昂布瓦斯](../Page/昂布瓦斯.md "wikilink")、[昂热](../Page/昂热.md "wikilink")、[布卢瓦](../Page/布卢瓦.md "wikilink")、[希农](../Page/希农.md "wikilink")、[南特](../Page/南特.md "wikilink")、[奥尔良](../Page/奥尔良.md "wikilink")、[索米尔和](../Page/索米尔.md "wikilink")[图尔等历史名城](../Page/图尔.md "wikilink")，尤其以[盧瓦爾河谷城堡群闻名世界](../Page/盧瓦爾河谷城堡群.md "wikilink")。

[卢瓦尔河流域的风景](../Page/卢瓦尔河.md "wikilink")，尤其是众多的文化建筑，清楚表明了西欧[文艺复兴和](../Page/文艺复兴.md "wikilink")[启蒙时代的思想和建筑设计理念](../Page/启蒙运动.md "wikilink")。

2000年12月2日，[联合国教科文组织将](../Page/联合国教科文组织.md "wikilink")[卢瓦尔河流域的中游部分](../Page/卢瓦尔河.md "wikilink")，从[曼恩河到叙利](../Page/曼恩河.md "wikilink")，列入[世界遗产名单](../Page/世界遗产.md "wikilink")\[1\]。这一地区涉及[卢瓦雷省](../Page/卢瓦雷省.md "wikilink")、[卢瓦尔-谢尔省](../Page/卢瓦尔-谢尔省.md "wikilink")、[安德尔-卢瓦尔省和](../Page/安德尔-卢瓦尔省.md "wikilink")[曼恩-卢瓦尔省](../Page/曼恩-卢瓦尔省.md "wikilink")，评审委员会称卢瓦尔河流域是：
“拥有极其美丽的杰出的人文景观，历史名城和村庄，大量壮丽的历史建筑（城堡），几个世纪以来人类和自然环境相互交融而形成的土地”。卢瓦尔河流域亦包括在1981年被單獨列入世界遗产目录中著名的[香波尔城堡](../Page/香波尔城堡.md "wikilink")。

## 城堡

城堡的总数超过300个，其建造开始于10世纪，当时还只是必要的防御工事，但是到500年后修建了一批豪华的城堡。法国国王开始在此建造巨大的城堡后，贵族们不希望远离权力中心，也纷纷仿效修建城堡。它们出现在这片土壤肥沃、气候温和的河谷，开始吸引最好的风景设计师。

到16世纪中叶，法国国王[弗朗索瓦一世](../Page/弗朗索瓦一世.md "wikilink")，将法国的权力中心从卢瓦尔河迁回到古代首都巴黎，建筑大师也随同迁走。不过大部分法国国王多半时间仍然在卢瓦尔河流域度过。法国国王[路易十四于](../Page/路易十四.md "wikilink")17世纪中叶建造[凡尔赛宫](../Page/凡尔赛宫.md "wikilink")，使巴黎成为王室的永久驻地。不过，那些国王的宠臣和富有的[资产阶级](../Page/资产阶级.md "wikilink")，继续修复已有的城堡，或者在此新建夏季别墅。

在[法国革命中](../Page/法国革命.md "wikilink")，许多被废黜的贵族被送上[断头台](../Page/断头台.md "wikilink")，他们的城堡也被洗劫并摧毁。在两次世界大战期间，一些城堡被征用作为军事指挥部，其中有些在二战结束后仍然作此用途使用。

今天，这些私人拥有的城堡用作住宅，少数向游客开放，还有一些辟为旅馆。许多城堡被当地政府接收，而[香波尔城堡等大型城堡属于法国中央政府拥有和管理](../Page/香波尔城堡.md "wikilink")，是重要的名胜古迹，每年吸引无数游客。

[替代=Château de Montsoreau,
Maine-et-Loire](https://zh.wikipedia.org/wiki/File:Château_de_Montsoreau\(Maine-et-Loire\).jpg "fig:替代=Château de Montsoreau, Maine-et-Loire")\]\]

## 参考文献

## 外部链接

  - [The Chateau
    File](http://www.frenchentree.com/france-pays-de-la-loire)
  - [Loire Valley guide on
    Wikivoyage](../Page/voy:en:Loire_Valley.md "wikilink")
  - [Tourist Office Board Loire
    Valley](http://www.loirevalleytourism.com/)
  - [Short breaks in Loire Valley
    Châteaux](https://web.archive.org/web/20081014032219/http://short-breaks.by-loire-valley.com/)
  - [Directory of Loire Valley
    Châteaux](http://www.chateaux-de-la-loire.fr/chateaux_of_the_loire.htm)

{{-}}

[@](../Category/法國城堡.md "wikilink")
[Category:法國世界遺產](../Category/法國世界遺產.md "wikilink")

1.  [1](http://whc.unesco.org/en/list/933)