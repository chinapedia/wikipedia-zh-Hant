**卤化物**指含有呈负价[卤素的](../Page/卤素.md "wikilink")[化合物](../Page/化合物.md "wikilink")，通常为二元化合物。**卤离子**则指相应-1价的卤素离子。根据具体卤素的不同，卤化物可分为：

  - [氟化物](../Page/氟化物.md "wikilink") — F<sup>−</sup>
  - [氯化物](../Page/氯化物.md "wikilink") — Cl<sup>−</sup>
  - [溴化物](../Page/溴化物.md "wikilink") — Br<sup>−</sup>
  - [碘化物](../Page/碘化物.md "wikilink") — I<sup>−</sup>
  - [-{zh-hans:砹;zh-hk:砈;zh-tw:砈;}-化物](../Page/砹.md "wikilink") —
    At<sup>−</sup>

[拟卤素生成的](../Page/拟卤素.md "wikilink")-1价化合物称为**拟卤化物**\[1\]，主要有[叠氮化物N](../Page/叠氮化合物.md "wikilink")<sub>3</sub><sup>−</sup>、[氰化物CN](../Page/氰化物.md "wikilink")<sup>−</sup>、[硫氰酸盐SCN](../Page/硫氰酸盐.md "wikilink")<sup>−</sup>和[氰酸盐OCN](../Page/氰酸盐.md "wikilink")<sup>−</sup>等。

[互卤化物则指只由卤素构成的二元化合物](../Page/互卤化物.md "wikilink")，如[ICl](../Page/一氯化碘.md "wikilink")、[ClF<sub>3</sub>](../Page/三氟化氯.md "wikilink")、[IF<sub>5</sub>等](../Page/五氟化碘.md "wikilink")。

[有机化学中](../Page/有机化学.md "wikilink")，有时也用卤化物来指[卤代烃](../Page/卤代烃.md "wikilink")，简写为R-X。根据具体卤素的不同亦可分类为[氟代烃](../Page/氟代烃.md "wikilink")、[氯代烃](../Page/氯代烃.md "wikilink")、[溴代烃和](../Page/溴代烃.md "wikilink")[碘代烃](../Page/碘代烃.md "wikilink")。二卤代烷常用于环的构建，参见[卡宾](../Page/卡宾.md "wikilink")。

## 鉴定

通常以[硝酸银溶液鉴定卤离子](../Page/硝酸银.md "wikilink")（氟离子除外），其[相应沉淀的颜色分别为](../Page/卤化银.md "wikilink")：

  - [AgCl](../Page/氯化银.md "wikilink") — 白色
  - [AgBr](../Page/溴化银.md "wikilink") — 淡黄色
  - [AgI](../Page/碘化银.md "wikilink") — 黄色

[AgF可溶于水](../Page/氟化银.md "wikilink")，因此一般不用此法。

## 例子

常见的卤化物有：

  - [氯化钠](../Page/氯化钠.md "wikilink") — NaCl
  - [氯化钾](../Page/氯化钾.md "wikilink") — KCl
  - [碘化钾](../Page/碘化钾.md "wikilink") — KI
  - [三氯化磷](../Page/三氯化磷.md "wikilink") — PCl<sub>3</sub>
  - [氯仿](../Page/氯仿.md "wikilink") — CHCl<sub>3</sub>

## 参见

  - [盐度](../Page/盐度.md "wikilink")
  - [金属卤化物](../Page/金属卤化物.md "wikilink")

## 参考资料

[Category:按阴离子分类的化合物](../Category/按阴离子分类的化合物.md "wikilink")
[L](../Category/官能团.md "wikilink") [\*](../Category/卤化物.md "wikilink")

1.  [International Union of Pure and Applied
    Chemistry](../Page/IUPAC.md "wikilink") (1995).
    "[pseudohalide](http://goldbook.iupac.org/P04930.html)". *Compendium
    of Chemical Terminology* Internet edition.