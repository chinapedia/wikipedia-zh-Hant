**卫媪**，[西汉人](../Page/西汉.md "wikilink")，本姓本名不详，[卫子夫](../Page/卫子夫.md "wikilink")、[卫青生母](../Page/卫青.md "wikilink")，[霍去病外祖母](../Page/霍去病.md "wikilink")。

## 身世

她的身份，《[史记](../Page/史记.md "wikilink")》\[1\]记为平阳侯“妾”，《[汉书](../Page/汉书.md "wikilink")》\[2\]则记为平阳侯“家僮”。

**妾**在[先秦和](../Page/先秦.md "wikilink")[秦汉时可指女奴](../Page/秦汉.md "wikilink")，并不等于现代汉语中的**[妾](../Page/妾.md "wikilink")**。卫青对自己的身份的描述，则是“人奴之生”\[3\]。据《史记》记叙，卫子夫“其家号曰卫氏”\[4\]，而媪则是用于妇人的[称谓](../Page/称谓.md "wikilink")。卫媪的身份应是平阳侯家的奴仆。

另“平阳侯妾”中，平阳侯可能指曹时。[汉高祖时](../Page/汉高祖.md "wikilink")[曹参封平阳侯](../Page/曹参.md "wikilink")\[5\]，为第一代平阳侯。[汉惠帝六年十月](../Page/汉惠帝.md "wikilink")，[曹窋为第二代平阳侯](../Page/曹窋.md "wikilink")。[汉文帝二十年](../Page/汉文帝.md "wikilink")（公元前160年），[曹奇为第三代平阳侯](../Page/曹奇.md "wikilink")。[汉景帝四年](../Page/汉景帝.md "wikilink")（公元前154年），曹时为第四代平阳侯。卫媪的第二女卫少儿身份明确，她为平阳侯曹时家中的侍者\[6\]。而其妹[卫子夫](../Page/卫子夫.md "wikilink")（卫媪的第四孩子）于[建元二年春](../Page/建元_\(汉武帝\).md "wikilink")（公元前139年）入汉武帝后宫<ref>

  - 建元二年春，青姊子夫得入宫幸上。</ref>。

## 家族

<center>

</center>

卫媪，先生有一子三女。长子卫长君，长女卫孺（[公孙贺妻](../Page/公孙贺.md "wikilink")，《[史记](../Page/史记.md "wikilink")》中作“卫孺”，《[汉书](../Page/汉书.md "wikilink")》中作“卫君孺”），次女卫少儿（[霍去病母](../Page/霍去病.md "wikilink")），三女卫子夫。后与郑季生一子[卫青](../Page/卫青.md "wikilink")，其后又生两子卫步、卫广\[7\]。

卫子夫在平阳公主府上被[汉武帝看中接入宫中](../Page/汉武帝.md "wikilink")，后来成为孝武皇后。卫青原来姓郑，便改姓卫，得到皇帝的重用，官拜[大将军](../Page/大将军.md "wikilink")。步、广也改姓卫。

少儿先和平阳侯府的“[给事](../Page/给事.md "wikilink")”（管理仆役的基层管理人员）[霍仲孺](../Page/霍仲孺.md "wikilink")（又作霍中孺）私通，生[霍去病](../Page/霍去病.md "wikilink")。后嫁与[陈平的曾孙](../Page/陈平_\(汉朝\).md "wikilink")[詹事](../Page/詹事.md "wikilink")[陈掌](../Page/陈掌.md "wikilink")。霍仲孺后回乡娶妻生[霍光](../Page/霍光.md "wikilink")。

卫子夫贵为皇后，卫青、霍去病、霍光都曾位极人臣。

## 参考资料

  -
  -
  - 《[汉书](../Page/汉书.md "wikilink") 卷九十七 外戚传第六十七》

  - 《[汉书](../Page/汉书.md "wikilink") 卷五十五 卫青霍去病传第二十五》

## 注释

<div class="references-small">

<references />

</div>

[W](../Category/西汉女性人物.md "wikilink")
[W](../Category/姓名失傳者.md "wikilink")

1.  其父郑季，为吏，给事平阳侯家，与侯妾卫媪通，生青。
2.  《[汉书](../Page/汉书.md "wikilink") 卷五十五 卫青霍去病传第二十五》季与主家僮卫媪通，生青。
3.  青笑曰：“人奴之生，得毋笞骂即足矣，安得封侯事乎！”
4.  卫皇后字子夫，生微矣。盖其家号曰卫氏，出平阳侯邑。
5.
6.  《[汉书](../Page/汉书.md "wikilink") 卷六十八
    霍光金日磾传第三十八》父中孺，[河东平阳人也](../Page/河东.md "wikilink")，以县[吏给事平阳侯家](../Page/吏.md "wikilink")，与侍者卫少儿私通而生[去病](../Page/霍去病.md "wikilink")。
7.  《史记》卷一百一十一·卫将军骠骑列传第五十一：“后子夫男弟步广皆冒卫氏”，皆，故为二人。