[Crystal_Project_starthere.png](https://zh.wikipedia.org/wiki/File:Crystal_Project_starthere.png "fig:Crystal_Project_starthere.png")

**衛星導航系統**是覆蓋全球的自主地利空間定位的衛星系統，允許小巧的[電子接收器確定它的所在位置](../Page/電子.md "wikilink")（[經度](../Page/經度.md "wikilink")、[緯度和](../Page/緯度.md "wikilink")[高度](../Page/高度.md "wikilink")），並且經由[衛星廣播沿著視線方向傳送的時間信號精確到](../Page/衛星.md "wikilink")10米的範圍內。接收機計算的精確時間以及位置，可以作為科學實驗的參考。

截至2012年，只有[美國的](../Page/美國.md "wikilink")[全球定位系統](../Page/全球定位系統.md "wikilink")
（；共由24顆衛星組成）及[俄罗斯的](../Page/俄罗斯.md "wikilink")[格洛納斯系統](../Page/格洛納斯系統.md "wikilink")（）是完全覆蓋全球的定位系統。[中國的](../Page/中國.md "wikilink")[北斗衛星導航系統](../Page/北斗衛星導航系統.md "wikilink")（）則於[2012年12月開始服務於](../Page/2012年12月.md "wikilink")[亞太區](../Page/亞太區.md "wikilink")（共由16顆衛星組成），預計於約2020年覆蓋全球\[1\]。[歐洲聯盟的](../Page/歐洲聯盟.md "wikilink")[伽利略定位系統則為在初期部署階段的全球導航衛星系統](../Page/伽利略定位系統.md "wikilink")，預定最早到2020年才能夠充分的運作\[2\]。一些國家，包括法國、日本和印度\[3\]，都在發展區域導航系統。

每個覆蓋全球的系統通常都是由20-30顆衛星組成的[衛星集群](../Page/衛星集群.md "wikilink")，以[中地球軌道分布在幾個軌道平面上](../Page/中地球軌道.md "wikilink")。實際的系統各自不同，但是使用的軌道傾斜都大於50°，和[軌道週期大約都是](../Page/軌道週期.md "wikilink")12小時（高度大約）。

## 分類

### 全球卫星导航系统国际委员会認定系統

  - [美國](../Page/美國.md "wikilink")：[全球定位系統](../Page/全球定位系統.md "wikilink")（）
  - [俄羅斯](../Page/俄羅斯.md "wikilink")：[全球導航衛星系統](../Page/格洛納斯系統.md "wikilink")（）
  - [中國](../Page/中國.md "wikilink")：[北斗衛星導航系統](../Page/北斗衛星導航系統.md "wikilink")
    （）
  - [歐盟](../Page/歐盟.md "wikilink")：[伽利略定位系統](../Page/伽利略定位系統.md "wikilink")，至2020年前，該系統的30顆衛星發射完成。

### 區域型衛星導航系統

  - [中國](../Page/中國.md "wikilink")：[中国区域定位系统](../Page/中国区域定位系统.md "wikilink")
    （）
  - [印度](../Page/印度.md "wikilink")：[印度區域導航衛星系統](../Page/印度區域導航衛星系統.md "wikilink")（），此系統包含7顆衛星及地面設施，於2017年8月完成衛星發射部署，為印度自主的導航系統。

### 輔助型衛星導航系統

  - [日本](../Page/日本.md "wikilink")：[準天頂衛星系統](../Page/準天頂衛星系統.md "wikilink")（）是美國的輔助系統，只為服務日本地區，但在東亞能觀測到該衛星的地區皆可利用。系統由4枚衛星組成，於2018年11月1日正式啟用。

## 參考資料

<references />

[Category:衛星導航系統](../Category/衛星導航系統.md "wikilink")
[Category:航空儀表](../Category/航空儀表.md "wikilink")

1.
2.
3.