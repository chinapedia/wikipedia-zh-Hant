**卢比**（，简作**Rp**）是[印度尼西亚的](../Page/印度尼西亚.md "wikilink")[法定货币](../Page/法定货币.md "wikilink")，[当地华人习称](../Page/印尼华人.md "wikilink")-{**印尼盾**}-，由[印度尼西亚银行发行和控制](../Page/印度尼西亚银行.md "wikilink")。印尼卢比的[ISO
4217代碼為](../Page/ISO_4217.md "wikilink")“IDR”。每卢比可分为100[分](../Page/分_\(貨幣\).md "wikilink")（sen），但由于[通货膨胀](../Page/通货膨胀.md "wikilink")，印尼盾所有的纸币和硬币都已经不以“分”为单位了。

[廖内群岛以及](../Page/廖内群岛省.md "wikilink")[新几内亚岛的印尼部分](../Page/新几内亚岛.md "wikilink")（[西伊里安](../Page/西伊里安.md "wikilink")）曾分别拥有自己的卢比货币，即和，但分别在1964年和1971年被纳入了国家卢比。

## 名稱

“卢比”（**Rupiah**）一名源自[印度斯坦语词语](../Page/印度斯坦语.md "wikilink")“rupiyaa”（,
），最源头则是表示“锻打的白银”（wrought
silver）的[梵文词汇](../Page/梵文.md "wikilink")“rupya”（）。印尼人通常也用“perak”来代指卢比，这个词在印尼语中表示白银。印尼錢幣上鑄有[印尼國徽和](../Page/印尼國徽.md "wikilink")“Rupiah”字樣。由於印尼被荷蘭殖民期間使用的貨幣稱為「」，現時部分華人地區仍使用「印尼-{}-盾」稱呼印尼-{}-盧比。

## 歷史

印尼在1610年至1817年間曾使用荷屬東印度盾作為貨幣，在[二戰由](../Page/二戰.md "wikilink")[日本所佔領的时期開始出現Rupiah](../Page/日本.md "wikilink")，至二戰結束由[爪哇盧比](../Page/爪哇盧比.md "wikilink")（Java
Rupiah）取代。

印尼盧比於1949年11月2日首度發行，當時的[廖內群島和](../Page/廖內群島.md "wikilink")[新幾內亞仍使用非標準的貨幣](../Page/新幾內亞.md "wikilink")——[廖內盧比](../Page/廖內盧比.md "wikilink")（Riau
rupiah）與[西伊里安卢比](../Page/西伊里安卢比.md "wikilink")（West Irian
rupiah），它們分別於1964年和1971年轉用印尼盧比。至1965年12月13日為應付[通貨膨脹](../Page/通貨膨脹.md "wikilink")，當局發行新的印尼盧比，每新盧比兌1000舊盧比。

在1997年至1998年的[亞洲金融風暴](../Page/亞洲金融風暴.md "wikilink")，印尼盧比貶值了35%，成為了[蘇哈托被推翻的主要因素之一](../Page/蘇哈托.md "wikilink")。當時每一[美元相等於](../Page/美元.md "wikilink")2000至3000盧比，至1998年6月更跌至每美元對16800盧比。

印尼盧比的[輔幣分別有Rp](../Page/輔幣.md "wikilink")5、Rp25、Rp50、Rp100、Rp500等幾種面額。而[紙幣則有Rp](../Page/紙幣.md "wikilink")100、Rp500、Rp1,000、Rp5,000、Rp10,000、Rp20,000、Rp50,000、Rp100,000等面額。

## 流通中的貨幣

### 硬幣

  - 25 盾
      - 正面：印尼國徽
      - 背面：[果實](../Page/果實.md "wikilink")、面值
  - 50 盾
      - 正面：印尼國徽
      - 背面：[黑枕黃鸝](../Page/黑枕黃鸝.md "wikilink")、面值
  - 100 盾
      - 正面：印尼國徽
      - 背面：[棕树凤头鹦鹉](../Page/棕树凤头鹦鹉.md "wikilink")、面值
  - 200 盾
      - 正面：印尼國徽
      - 背面：長冠八哥、面值
  - 500 盾
      - 正面：印尼國徽
      - 背面：[茉莉花](../Page/茉莉花.md "wikilink")、面值
  - 1000 盾
      - 正面：印尼國徽、面值
      - 背面：樂器、政府辦事處

### 紙幣

|                                                                                                                                      |
| ------------------------------------------------------------------------------------------------------------------------------------ |
| 2016年版印尼盾鈔票（民族英雄系列）                                                                                                                  |
| 圖片                                                                                                                                   |
| 正面                                                                                                                                   |
| [Indonesia_2016_1000r_o.jpg](https://zh.wikipedia.org/wiki/File:Indonesia_2016_1000r_o.jpg "fig:Indonesia_2016_1000r_o.jpg")      |
| [Indonesia_2016_2000r_o.jpg](https://zh.wikipedia.org/wiki/File:Indonesia_2016_2000r_o.jpg "fig:Indonesia_2016_2000r_o.jpg")      |
| [Indonesia_2016_5000r_o.jpg](https://zh.wikipedia.org/wiki/File:Indonesia_2016_5000r_o.jpg "fig:Indonesia_2016_5000r_o.jpg")      |
| [Indonesia_2016_10000r_o.jpg](https://zh.wikipedia.org/wiki/File:Indonesia_2016_10000r_o.jpg "fig:Indonesia_2016_10000r_o.jpg")   |
| [Indonesia_2016_20000r_o.jpg](https://zh.wikipedia.org/wiki/File:Indonesia_2016_20000r_o.jpg "fig:Indonesia_2016_20000r_o.jpg")   |
| [Indonesia_2016_50000r_o.jpg](https://zh.wikipedia.org/wiki/File:Indonesia_2016_50000r_o.jpg "fig:Indonesia_2016_50000r_o.jpg")   |
| [Indonesia_2016_100000IDR.jpg](https://zh.wikipedia.org/wiki/File:Indonesia_2016_100000IDR.jpg "fig:Indonesia_2016_100000IDR.jpg") |
|                                                                                                                                      |

## 匯率

## 注释

## 参考

## 外部链接

  - [印尼钞票影像](http://www.currencymuseum.net/indonesia.htm)
  - [印尼盧比硬幣圖片](http://worldcoingallery.com/countries/circ_sets/Indonesia.jpg)
  - [印度尼西亚的钞票](http://www.bis-ans-ende-der-welt.net/Indonesien-B-En.htm)


[Category:各國貨幣](../Category/各國貨幣.md "wikilink")
[Category:印尼经济](../Category/印尼经济.md "wikilink")