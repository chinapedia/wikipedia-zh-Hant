[Indian_Plate_map-fr.png](https://zh.wikipedia.org/wiki/File:Indian_Plate_map-fr.png "fig:Indian_Plate_map-fr.png")
[Australian_Plate_map-fr.png](https://zh.wikipedia.org/wiki/File:Australian_Plate_map-fr.png "fig:Australian_Plate_map-fr.png")

**印度-澳洲板塊**（英語：**Indo-Australian
Plate**，或**印澳板塊**）是兩塊[板塊的合稱](../Page/板塊.md "wikilink")，其中包含了[澳洲大陸及周圍海域](../Page/澳洲大陸.md "wikilink")，並向西北延伸，涵蓋[印度次大陸與附近水域](../Page/印度次大陸.md "wikilink")。此板塊可分成較大的澳洲板塊與較小的[印度板塊](../Page/印度板塊.md "wikilink")，兩者之間為一道低度活動邊界。兩個板塊在5千5百萬到5千萬年前融合在一起，在此以前兩者為分離狀態。

於澳洲西南部Mount Barren群地層南緣的[尤岡克拉通](../Page/尤岡克拉通.md "wikilink")（Yilgarn
Craton，[克拉通是大陸內部古老且穩定的陸塊](../Page/克拉通.md "wikilink")）所做的[鋯石定年分析顯示](../Page/鋯石.md "wikilink")，大約在1696±7百萬年前，皮爾巴拉-尤岡（Pilbara-Yilgarn）與尤岡-高勒（Yilgarn-Gawler）兩個克拉通發生碰撞，聚集成為原始的澳洲大陸\[1\]。

[印度](../Page/印度.md "wikilink")、[澳洲大陸](../Page/澳洲大陸.md "wikilink")（包括[澳大利亞](../Page/澳大利亞.md "wikilink")、[新幾內亞](../Page/新幾內亞.md "wikilink")、[塔斯馬尼亞](../Page/塔斯馬尼亞.md "wikilink")、[紐西蘭與](../Page/紐西蘭.md "wikilink")[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")）原來皆是[岡瓦那大陸的一部分](../Page/岡瓦那大陸.md "wikilink")，後來[海底擴張使這些陸地分隔](../Page/海底擴張.md "wikilink")，但由於分隔邊界逐漸失去活動性，因此這些陸地可視為融合成單一板塊。於澳洲所做的[GPS的計算確認板塊的移動方向為北](../Page/GPS.md "wikilink")35度東，速度為每年67公厘。

## 參考文獻

{{-}}

[Category:板塊](../Category/板塊.md "wikilink")
[Category:澳大利亞地理](../Category/澳大利亞地理.md "wikilink")
[Category:印度地理](../Category/印度地理.md "wikilink")
[Category:太平洋地质学](../Category/太平洋地质学.md "wikilink")

1.