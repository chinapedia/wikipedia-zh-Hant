[Rigveda_MS2097.jpg](https://zh.wikipedia.org/wiki/File:Rigveda_MS2097.jpg "fig:Rigveda_MS2097.jpg")手抄本）\]\]
[Krishna,_Kottangal_padayani.JPG](https://zh.wikipedia.org/wiki/File:Krishna,_Kottangal_padayani.JPG "fig:Krishna,_Kottangal_padayani.JPG")
**印度教神话**的形成与其本身的[历史关系密切](../Page/印度历史.md "wikilink")。大约公元前3000年左右，[印度河流域出现](../Page/印度河.md "wikilink")[印度河文明亦即哈拉帕文明](../Page/印度河文明.md "wikilink")，大約公元前2000年左右，自[亞利安人南遷入](../Page/亞利安人.md "wikilink")[印度後](../Page/印度.md "wikilink")，经过无数次戰爭和[文化融合](../Page/文化融合.md "wikilink"),
逐漸取代原有印度河流域的文明並从印度河流域擴展至[恒河流域以及整個印度次大陸](../Page/恒河.md "wikilink"),
最終形成今天的印度文明。

公元前1500年至前600年左右，《[吠陀经](../Page/吠陀经.md "wikilink")》问世，这是[印欧语系诸民族中最为古老的一部](../Page/印欧语系.md "wikilink")[文学著作](../Page/文学.md "wikilink")，在其中，印度神话初次较有系统组合起来。吠陀神话裡所描述的最大的神是因陀罗，他是天帝，众神之首。据记载，因陀罗原本是带领雅利安人入侵印度的英雄，死后成为神，其神格化可以看作是吠陀诗人对于权力的一种附会。吠陀文化后期，印度产生了[婆罗门教](../Page/婆罗门教.md "wikilink")，而[种姓制度的出现則是权力更为集中的体现](../Page/种姓制度.md "wikilink")。

公元前六世纪左右，在各方面快速发展的印度进入列国时代，这一时期，旧的神话不断被编辑，新的神话又不断产生，宗教方面，出现了[佛教与](../Page/佛教.md "wikilink")[耆那教](../Page/耆那教.md "wikilink")，而这两大教派又各自繁衍着不同的神话。

公元前四世纪之前，印度最大的两部[史诗出现](../Page/史诗.md "wikilink")——《[罗摩衍那](../Page/罗摩衍那.md "wikilink")》和《[摩诃婆罗多](../Page/摩诃婆罗多.md "wikilink")》，在这里，因陀罗等神的地位被削弱，印度神话基本回到最初的体系。

## 吠陀神话中主要的神

1.  [因陀罗](../Page/因陀罗.md "wikilink")：天帝。
2.  [阿耆尼](../Page/阿耆尼.md "wikilink")：火神。
3.  [伐楼那](../Page/伐楼那.md "wikilink")：水神。
4.  [阎摩](../Page/阎摩.md "wikilink")：死神。
5.  [苏利耶](../Page/苏利耶.md "wikilink")：太阳神。
6.  [樓陀羅](../Page/樓陀羅.md "wikilink")：暴風神。

<File:Tiruchchirappalli> painting Indra.jpg|因陀罗 <File:Agni> deva.JPG|阿耆尼
<File:Varuna> makara.jpg|伐楼那

## 史诗中的神祇

### 主要神

1.  [梵天](../Page/梵天.md "wikilink")：创造之神。
2.  [毗湿奴](../Page/毗湿奴.md "wikilink")：保护神。
3.  [湿婆](../Page/湿婆.md "wikilink")：音乐、舞蹈和毁灭之神。
4.  [阿修罗](../Page/阿修罗.md "wikilink")：恶魔。
5.  [迦楼罗](../Page/迦楼罗.md "wikilink")：金翅神鸟，于烈火中涅盘而得永生。因与中国传统神话中的[凤凰在外形上有些相似](../Page/凤凰.md "wikilink")，但其实两者相差甚大。

<File:A> roundel of Brahma.jpg|梵天
[File:Vishnu_Kumartuli_Park_Sarbojanin_Arnab_Dutta_2010.JPG|毗湿奴](File:Vishnu_Kumartuli_Park_Sarbojanin_Arnab_Dutta_2010.JPG%7C毗湿奴)
|湿婆 <File:Thailand> Bangkok Barge Museum Garuda.jpg|迦楼罗

### 次要神

世界守護神指的是以天神之王因陀羅為首的八位主神，他們大多數都是原來的吠陀主神，在印度教神話中地位或多或少都有所降低，和吠陀神話相比他們的力量也有所削弱。

1.  雷電之神[因陀羅](../Page/因陀羅.md "wikilink")
2.  死神[閻摩](../Page/閻摩.md "wikilink")
3.  海洋之神[伐樓那](../Page/伐樓那.md "wikilink")
4.  風神[伐由](../Page/伐由.md "wikilink")
5.  酒神[蘇摩](../Page/蘇摩.md "wikilink")
6.  太陽神[蘇利耶](../Page/蘇利耶.md "wikilink")
7.  火神[阿耆尼](../Page/阿耆尼.md "wikilink")
8.  財富之神[俱比羅](../Page/俱比羅.md "wikilink")

<File:The> deity Yama with fangs and holding a daṇḍa (a rod).jpg|閻摩
<File:Vayu> deva.JPG|伐由 <File:Shri> Surya Bhagvan bazaar art,
c.1940's.jpg|蘇利耶 <File:Kubera> 1842.jpg|俱比羅

## 相關

  - [印度教](../Page/印度教.md "wikilink")
  - [婆羅門教](../Page/婆羅門教.md "wikilink")

[印度教神話](../Category/印度教神話.md "wikilink")