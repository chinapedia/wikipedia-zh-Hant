**印第安纳州**（）是[美国的一个州](../Page/美国.md "wikilink")，它的首府是[印第安纳波利斯](../Page/印第安纳波利斯.md "wikilink")。印第安纳原意是[印第安人的土地的意思](../Page/印第安人.md "wikilink")。它的缩写是IN。在美国，一个来自印第安纳州的人不被称为印第安纳州人（Indianer），而被称为胡希尔人（Hoosier），在中文裡，这个词一般被译为印第安纳州人。美国海军有多艘战舰以印第安纳州命名。印第安纳州是美国50个州中按面积第38大，按人口第15大的州。印第安纳州也是美国本土阿巴拉契亚山脉以西面积最小的州。印第安纳州的州府和最大城市是印第安纳波利斯，它是美国第二大州府和密西西比河以东最大的州府。

在印第安纳领地建制之前，不同文化的原住民已经在印第安纳这片土地上生活了数千年。是美国保存最好的古代土木工程之一，位于印第安纳州西南[埃文斯維爾附近](../Page/埃文斯維爾.md "wikilink")。印第安纳州的居民称为[胡希尔人](../Page/胡希尔.md "wikilink")（The
Hoosier）。该词来源有争议，但主流看法是该词来源于美国南方高山地区，是一不敬的俚语“土包子”的意思。该看法也为印第安纳州历史局和印第安纳州历史协会认可。

印第安纳州的州名意为印第安人的土地。此名称最早可以追溯到1760年代，当1800年印第安纳领地从[西北领地中分离出来单独建制的时候](../Page/西北领地.md "wikilink")，第一次在[美国国会使用](../Page/美国国会.md "wikilink")。自从印第安纳领地的建制，居民来源就反映出美国东部的地区文化格局。州北部的居民主要来自[新英格兰和](../Page/新英格兰.md "wikilink")[纽约州](../Page/纽约州.md "wikilink")，州中部的居民主要来自中大西洋各州和毗邻的[俄亥俄州](../Page/俄亥俄州.md "wikilink")，而南部居民则主要来自南部诸州，主要是[肯塔基州和](../Page/肯塔基州.md "wikilink")[田纳西州](../Page/田纳西州.md "wikilink")。

印第安纳州有非常多元化的经济，2005年州内生产总值为2140亿美元。印第安纳州有数个人口在10万以上的都会区和许多较小的工业城镇。美国2012年人口估算显示，该州总人口数为653.7万。印第安纳州有数个体育球队和体育赛事，包括[国家橄榄球联盟的](../Page/国家橄榄球联盟.md "wikilink")[印第安纳波利斯小马队](../Page/印第安纳波利斯小马.md "wikilink")，[国家篮球联盟的](../Page/国家篮球联盟.md "wikilink")[印第安纳步行者队](../Page/印第安纳步行者.md "wikilink")，国家女子篮球联盟的印第安纳热度队，和[印第安納波利斯500赛车](../Page/印第安納波利斯500.md "wikilink")，Brickyard
400赛车。印第安纳州有数个大学入选[美国新闻与世界报道最好大学](../Page/美国新闻与世界报道.md "wikilink")2011年排行榜。[普渡大学](../Page/普渡大学.md "wikilink")、[印第安纳大学和](../Page/印第安纳大学.md "wikilink")[圣母大学为全美前](../Page/圣母大学.md "wikilink")50名的大学，而[巴特勒大學](../Page/巴特勒大學.md "wikilink")、[瓦爾帕萊索大學和](../Page/瓦爾帕萊索大學.md "wikilink")[伊凡斯維爾大學为美国中西部地区大学前](../Page/伊凡斯維爾大學.md "wikilink")10名的大学。

## 6种中文译法

「Indiana」中的「Indi」有「印第」、「印弟」和「印地」3种译法；「ana」則有「安纳」和「安那」，所以2种译法，因此其组合共有3×2=6种：-{zh-hans:;zh-hant:}-、-{zh-hans:;zh-hant:}-、-{zh-hans:;zh-hant:}-、、、。当地唯一的中文报纸印州华报译为州。\[1\]

## 历史

印第安纳州最早的居民是古[印第安人](../Page/印第安人.md "wikilink")，大约公元前8000年，[冰河時期结束](../Page/冰河時期.md "wikilink")[冰川融化后](../Page/冰川.md "wikilink")，进入本州。分化为小的部落，古印第安人是游牧民族，他们围捕大型动物，例如[乳齒象](../Page/乳齒象.md "wikilink")。他们用燧石，通过打凿，敲击和切薄打造石器工具。下一个本地印第安人的时期叫做旧时期，开始于约公元前5000年至公元前4000年。和之前的古印第安人的不同之处在于，他们用新的工具和技术准备食物。这些新的工具包括不同类型的长矛，有不同槽口的刀具。他们也适用基础的石器工具，例如石斧，木工工具和磨石。本时期的后期，土丘和垃圾堆出现，揭示他们的定居变得更加的永久。旧时期结束于大约公元前1500年，但是也有部分旧时期时代的印第安人持续到公元前700年左右。在印第安纳，紧接着的是[伍德兰期](../Page/伍德兰期.md "wikilink")，各种新的文化产生。在这一时期，随着种植业的大量出现，瓷器和陶器出现。一个早期的伍德兰部落[Adena人](../Page/Adena.md "wikilink")，有着别致的安葬仪式，在土丘之下安置木制的坟墓。在伍德兰时期的中期，[Hopewell人已经开始尝试长距离的物资交易](../Page/Hopewell.md "wikilink")。在伍德兰时期结束前，大量的农作物开始培育生产，比如[玉米和](../Page/玉米.md "wikilink")[西葫芦](../Page/西葫芦.md "wikilink")。伍德兰时期结束于大约公元1000年。接下来的时期称为[密西西比文化时期](../Page/密西西比文化.md "wikilink")，从公元1000年持续到15世纪欧洲人到来之前。在这一时期，大型的定居点，类似城镇出现，例如[Angel
Mounds](../Page/Angel_Mounds.md "wikilink")。他们拥有大片的公共区域，例如广场和土丘的平顶，定居点的领导人物居住于此，或者在此举行仪式。印第安纳的密西西比文化在15世纪中期瓦解，原因未明。

法国探险家[René-Robert Cavelier, Sieur de La
Salle在](../Page/René-Robert_Cavelier,_Sieur_de_La_Salle.md "wikilink")1679年抵达今日圣约瑟夫河畔的[南本德后](../Page/南本德.md "wikilink")，成为第一个穿越印第安纳的欧洲人。次年，他带着北印第安纳的信息归程。同时，加拿大的皮毛交易商也带着毛毯，珠宝，工具，威士忌和武器，和印第安纳的土著居民交换皮毛。到1702年，在Vincennes附近，[Sieur
Juchereau建立了第一个交易点](../Page/Sieur_Juchereau.md "wikilink")。1715年，[Sieur
de
Vincennes在](../Page/Sieur_de_Vincennes.md "wikilink")[Kekionga](../Page/Kekionga.md "wikilink")（今[韦恩堡](../Page/韦恩堡.md "wikilink")）建造了迈阿密堡（[Fort
Miami](../Page/Fort_Miami.md "wikilink")）。1717年，另一个加拿大人[Picote de
Beletre在](../Page/Picote_de_Beletre.md "wikilink")[瓦萨比河建立](../Page/瓦萨比河.md "wikilink")[Ouiatenon堡](../Page/Ouiatenon.md "wikilink")，尝试控制从[伊利湖到](../Page/伊利湖.md "wikilink")[密西西比河的美国土著的交易路线](../Page/密西西比河.md "wikilink")。1732年，Sieur
de
Vincennes在Vincennes建立了第二个皮毛交易点。早期那些由于敌意而离开的加拿大定居者，有大批返回。然而一些年后，英国人也到达印第安纳，他们尝试和加拿大人争夺获利丰厚的皮毛交易的控制权。这种争夺最终导致了1750年代的加拿大人和英国人之间的战争。\[2\]

在[法國印第安戰爭中](../Page/法國印第安戰爭.md "wikilink")，印第安纳的土著部落支持加拿大人。到战争结束的1763年，法国人丢失了[十三个殖民地以西的所有土地](../Page/十三个殖民地.md "wikilink")，控制权转手到英国王室。印第安纳周围的印第安部落并未放弃，在1763年的[Pontiac's
Rebellion中](../Page/Pontiac's_Rebellion.md "wikilink")，摧毁了[Ouiatenon堡和迈阿密堡](../Page/Ouiatenon.md "wikilink")。1763年英国王室不得不宣布[阿巴拉契亚山脉以西土地为印第安人的使用](../Page/阿巴拉契亚山脉.md "wikilink")，并成为印第安领地。1775年，[美国独立战争爆发](../Page/美国独立战争.md "wikilink")，殖民地居民寻求从英国人统治下独立。大多数的战争发生在东部地区，但是军官[George
Rogers
Clark征召军队在西部和英国人抗争](../Page/George_Rogers_Clark.md "wikilink")。1779年2月25日，在几次大捷后，Clark的军队占领[Vincennes和](../Page/Vincennes.md "wikilink")[Fort
Sackville](../Page/Fort_Sackville.md "wikilink")。在战争中，Clark尝试阻截从西部进攻殖民地的英国部队。他的成功现在被认为改变了美国独立战争的进程。在独立战争末期，通过[巴黎条约](../Page/巴黎条约.md "wikilink")，英国王室割让[五大湖地区以南的土地给新成立的](../Page/五大湖地区.md "wikilink")[美利坚合众国](../Page/美利坚合众国.md "wikilink")。

1787年，今日的印第安纳州成为[西北领地的一部分](../Page/西北领地.md "wikilink")。1800年，国会把[俄亥俄州从西边领地中分离出去](../Page/俄亥俄州.md "wikilink")，剩余部分土地组成[印第安纳领地](../Page/印第安纳领地.md "wikilink")。[托马斯·杰佛逊总统指认](../Page/托马斯·杰佛逊.md "wikilink")[William
Henry
Harrison为领地总督](../Page/William_Henry_Harrison.md "wikilink")，[Vincennes成为领地的首府](../Page/Vincennes.md "wikilink")。在[密歇根州分离出去和](../Page/密歇根州.md "wikilink")[伊利诺伊领地形成后](../Page/伊利诺伊领地.md "wikilink")，印第安纳领地形成当今印第安纳州的大小。1810年，[Shawnee部落的领袖](../Page/Shawnee.md "wikilink")[Tecumseh和他的兄弟](../Page/Tecumseh.md "wikilink")[Tenskwatawa鼓动其他的部落抵制欧洲定居者进入印第安领地](../Page/Tenskwatawa.md "wikilink")。双方的紧张态势不断升高，Harrison被授权先发制人打击[Tecumseh邦联](../Page/Tecumseh.md "wikilink")。1811年11月7日，美国军队在[Tippecanoe战役中获胜](../Page/Tippecanoe.md "wikilink")。Tecumseh在1813年的[Thames战役中被杀](../Page/Thames.md "wikilink")。在他死后，武装抵抗美国对该区域的控制结束。在1820和1830年代，通过协议购买土地，大多数的美国土著居民离开了本州。

[5NationsExpansion.jpg](https://zh.wikipedia.org/wiki/File:5NationsExpansion.jpg "fig:5NationsExpansion.jpg")
conquests during the [Beaver Wars](../Page/Beaver_Wars.md "wikilink")
(mid 1600's), which largely depopulated [Ohio
River](../Page/Ohio_River.md "wikilink") valley.\[3\]\]\]

1813年12月，[科里登成为印第安纳领地的第二个首府](../Page/科里登_\(印第安纳州\).md "wikilink")。2年后，领地众议会通过成立州的申请，并送往国会。之后，[Enabling
Act通过](../Page/Enabling_Act.md "wikilink")，选举代表撰写印第安纳宪法。1816年6月10日，代表们聚集首府科里登开始撰写宪法，并于19日后完成。1816年12月11日，[詹姆斯·麦迪逊总统批准了印第安纳作为第](../Page/詹姆斯·麦迪逊.md "wikilink")19个州加入联邦。1825年，州府由[科里登迁至](../Page/科里登_\(印第安纳州\).md "wikilink")[印地安纳波利斯](../Page/印地安纳波利斯.md "wikilink")。26年后，第二部宪法被采纳。随着州的建立，新政府设立了一个雄心勃勃的机会，把印第安纳州从一个荒蛮的[美國舊西部打造为一个发展的](../Page/美國舊西部.md "wikilink")，人口蓬勃的繁荣之州，进行一系列的人口和经济的改变。州的建立者开始建造公路，运河，铁路和公立学校的项目。该项目导致了州的破产和金融危机。但是，土地和产品的价值也翻了4倍多。19世纪初，大量的移民涌入印第安纳州。最大的移民群体为[德国人](../Page/德国.md "wikilink")，也有大量的来自[爱尔兰和](../Page/爱尔兰.md "wikilink")[英国的移民](../Page/英国.md "wikilink")。同时，也有来自[纽约州](../Page/纽约州.md "wikilink")，[新英格兰和](../Page/新英格兰.md "wikilink")[宾夕法尼亚州的英国人后裔移入印第安纳州](../Page/宾夕法尼亚州.md "wikilink")。

在[美国内战期间](../Page/美国内战.md "wikilink")，印第安纳州扮演者举足轻重的角色，对全国事务有着重要影响。作为第一个进行战争动员的西部州，印第安纳州的士兵出现在战争中所有的重要战场上。

## 法律和政治

[缩略图](https://zh.wikipedia.org/wiki/File:Lafayette_IN_3rd_and_Main.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Monarch_Wine.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Simon_HQs.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Case_IH_combine_harvesting_soybeans.jpg "fig:缩略图")
印第安纳州在美国國會中代表的参议员是[丹·寇茨](../Page/丹·寇茨.md "wikilink")（[共和党](../Page/共和党.md "wikilink")）和[理查德·鲁阁](../Page/理查德·鲁阁.md "wikilink")（[共和党](../Page/共和党.md "wikilink")）；並有9位代表的眾議員。
印第安纳州的政府也依三權分立的原則組成，行政權由州长行使，目前是[共和党的](../Page/共和党.md "wikilink")[埃瑞克·霍科姆](../Page/埃瑞克·霍科姆.md "wikilink")。立法權由州議會行使，分成有50位代表的州參議會和100位代表的州眾議會。司法權由州最高法院行使，共有5位法官。

### 行政劃分

印第安纳州共有92个[县](../Page/县_\(美国\).md "wikilink")。

### 政治

从1880年至1924年，除一届总统选举外，每一届总统选举都有一位来自印第安纳州的人士参与。1880年的总统大选，印第安纳州众议员[威廉·海登·英格利希被提名为副总统候选人](../Page/威廉·海登·英格利希.md "wikilink")，和[溫菲爾德·斯科特·漢考克一起竞选副总统和总统](../Page/溫菲爾德·斯科特·漢考克.md "wikilink")。1884年前印第安纳州州长[托馬斯·A·亨德里克斯成为总统](../Page/托馬斯·A·亨德里克斯.md "wikilink")[格罗弗·克利夫兰的副总统](../Page/格罗弗·克利夫兰.md "wikilink")，他于1885年11月25日在任内去世。1888年，印第安纳州参议员[本杰明·哈里森被选为](../Page/本杰明·哈里森.md "wikilink")[美国总统](../Page/美国总统.md "wikilink")，在任一届。他是迄今为止唯一一位来自印第安纳州的总统。1904年，印第安纳州参议员[查爾斯·W·費爾班克斯当选为](../Page/查爾斯·W·費爾班克斯.md "wikilink")[西奥多·罗斯福总统的副总统](../Page/西奥多·罗斯福.md "wikilink")，在任至1913年。查爾斯·W·費爾班克斯在1912年搭档[查爾斯·埃文斯·休斯再次参选副总统一职](../Page/查爾斯·埃文斯·休斯.md "wikilink")，但是竞选失败。同年，[伍德罗·威尔逊和印第安纳州州长](../Page/伍德罗·威尔逊.md "wikilink")[托马斯·R·马歇尔当选为总统和副总统](../Page/托马斯·R·马歇尔.md "wikilink")。湯瑪斯·R·馬歇爾的副总统任期从1913年至1921年。之后直到1988年，印第安纳州参议员[丹·奎尔成为](../Page/丹·奎尔.md "wikilink")[乔治·赫伯特·沃克·布什总统的副总统](../Page/乔治·赫伯特·沃克·布什.md "wikilink")，在任一届。

印第安纳州长期以来被认为是[共和党占有强有力的支持](../Page/共和党.md "wikilink")，尤其是在总统选举中。但是目前Cook
Partisan Voting Index
(CPVI)仅把印第安纳州评为R+5，比28个[红州中](../Page/红州.md "wikilink")20个的共和党的优势还要小。印第安纳州是1940年总统选举中支持共和党候选人[溫德爾·威爾基的](../Page/溫德爾·威爾基.md "wikilink")10个仅有的州中的一个。在14次总统选举中，共和党候选人以2位数的差距赢得本州，其中6次，差距超过了20%。尽管2000年和2004年的总统选举，在全国范围而言，两党候选人差距很小，共和党候选人[乔治·沃克·布什仍以极大优势赢得印第安纳州](../Page/乔治·沃克·布什.md "wikilink")。从1900年以来，[民主党仅赢得本州](../Page/民主党.md "wikilink")5次。1912年，[伍德罗·威尔逊以](../Page/伍德罗·威尔逊.md "wikilink")43%的选票为民主党第一次赢得印第安纳州。20年后，[富兰克林·德拉诺·罗斯福以](../Page/富兰克林·德拉诺·罗斯福.md "wikilink")55%的选票在印第安纳州战胜在任共和党总统[赫伯特·胡佛](../Page/赫伯特·胡佛.md "wikilink")。1936年罗斯福再次赢得印第安纳州。1964年，民主党候选人[林登·约翰逊以](../Page/林登·约翰逊.md "wikilink")56%的选择战胜[巴里·戈德华特](../Page/巴里·戈德华特.md "wikilink")，赢得印第安纳州。44年后，民主党候选人[巴拉克·奥巴马以](../Page/巴拉克·奥巴马.md "wikilink")50%对49%的极小差距战胜共和党候选人[约翰·麦凯恩](../Page/约翰·麦凯恩.md "wikilink")，赢得印第安纳州。

## 地理

印第安纳州面积为36,418平方英里（94,320平方公里），为[美国第](../Page/美国.md "wikilink")38大州。该州南北最长为250英里（400公里），东西最宽为145英里（233公里）。印第安纳州北滨[密歇根湖和](../Page/密歇根湖.md "wikilink")[密歇根州](../Page/密歇根州.md "wikilink")，东临[俄亥俄州](../Page/俄亥俄州.md "wikilink")，南隔[俄亥俄河與](../Page/俄亥俄河.md "wikilink")[肯塔基州相望](../Page/肯塔基州.md "wikilink")，西连[伊利诺州](../Page/伊利诺州.md "wikilink")。[瓦伯西河由東北向西南貫穿本州](../Page/瓦伯西河.md "wikilink")，注入[俄亥俄河](../Page/俄亥俄河.md "wikilink")。本州為中央及北部为平原地形，南部为丘陵地形，最高點僅海拔1257呎（約380公尺，位於韋恩郡富蘭克林鎮，Franklin
Township of Wayne
County）。该州仅有2,850平方英里（7,400平方公里）区域海拔高于1,000英尺（300米），该区域位于14个县。大约4,700平方英里（12,000平方公里）的区域海拔低于500英尺（150米）。

## 气候

印第安纳州为[温带大陆性湿润气候](../Page/温带大陆性湿润气候.md "wikilink")，冬季寒冷，夏季温暖湿润。州最南端部分区域属于[副热带湿润气候](../Page/副热带湿润气候.md "wikilink")，比本州其他地方有更多降雨量。气温南北有差异。隆冬时节，最北的平均高温和低温为30华氏度（-1摄氏度）和15华氏度（-10摄氏度），而最南的平均高温和低温为39华氏度（4摄氏度）和22华氏度（-6摄氏度）。仲夏时节全州范围内气温差异较小。最北的平均高温和低温为84华氏度（29摄氏度）和64华氏度（18摄氏度），而最南的平均高温和低温为90华氏度（32摄氏度）和69华氏度（21摄氏度）。州历史最高温度为116华氏度（47摄氏度）出现在1936年7月14日，位于Collegeville。州历史最低温度为-36华氏度（-38摄氏度）出现在1994年1月19日，位于New
Whiteland。农作物生长期北方为155天，南方为185天。

## 经济

1999年印第安纳州的总产值为1820亿美元，在美国列第15位，人均收入为27,011美元。主要农产品有谷物、大豆、猪肉、牛、奶制品和蛋。工业产品有钢、电气设备、运输设备、化学产品、油和煤产物和机械设备。著名金融公司和制药公司[礼来公司的总部和](../Page/礼来公司.md "wikilink")[罗氏公司的美国总部在印第安纳州](../Page/罗氏.md "wikilink")。

## 人口

[Indiana_population_map.png](https://zh.wikipedia.org/wiki/File:Indiana_population_map.png "fig:Indiana_population_map.png")
[USA_Indiana_age_pyramid.svg](https://zh.wikipedia.org/wiki/File:USA_Indiana_age_pyramid.svg "fig:USA_Indiana_age_pyramid.svg")

2011年7月1日，印第安纳州的人口估计为6,516,922人，人口自[2010年美國人口普查后增长](../Page/2010年美國人口普查.md "wikilink")0.51%。印第安纳州的大多数城市是中小城市，其最大的城市和首府是[印第安纳波利斯](../Page/印第安纳波利斯.md "wikilink")。

截至2010年，印第安納州有6,483,802人。人口密度为181人每平方英里。目前印第安納州的種族比例可分為：

  - 84.3%是[白人](../Page/白人.md "wikilink")（81.5%非[拉美裔人](../Page/拉美裔人.md "wikilink")）
  - 9.1%[非裔美國人](../Page/非裔美國人.md "wikilink")
  - 6.0%是[拉美裔人](../Page/拉美裔人.md "wikilink")
  - 1.6%是[亞裔美國人](../Page/亞裔美國人.md "wikilink")
  - 0.3%是[印地安原住民](../Page/印第安人.md "wikilink")
  - 2.0 %混合的種族

[拉美裔人是印第安纳州增长最快的少数族裔](../Page/拉美裔人.md "wikilink")。印第安纳州24.9%的人口为18岁以下，6.9%为5岁以下，12.8%为65岁以上。年龄中位数为36.4岁。2005年，77.7%的印第安纳居民居住于都会区，16.5%居住于人口较集中的县，5.9%居住于非人口集中县。

印第安納州的前五大居民祖先為：

  - [德國人](../Page/德國.md "wikilink")（22.7%）
  - 美國本土化血統者（12%）
  - [愛爾蘭人](../Page/愛爾蘭.md "wikilink")（10.8%）
  - [英國人](../Page/英國.md "wikilink")（8.9%）
  - [波兰人](../Page/波兰.md "wikilink")（3.0%）

大多数自认为祖先是美国人的人，其实为英国人后裔，只是他们移民美国太早，很多情况下为早期殖民年代，他们自我识别为美国人。

印第安纳州的人口中心位于[汉密尔顿县的Sheridan镇](../Page/汉密尔顿县.md "wikilink")。印第安纳州人口增长自1990年开始，主要集中于[印地安纳波利斯周围诸县](../Page/印地安纳波利斯.md "wikilink")，在印第安纳州人口增长最快的5个县中，包括了4个：[汉密尔顿县](../Page/汉密尔顿县.md "wikilink")，[亨德里克斯县](../Page/亨德里克斯县.md "wikilink")，[约翰逊县和](../Page/约翰逊县.md "wikilink")[汉考克县](../Page/汉考克县.md "wikilink")。另外一个县为[迪尔伯恩县](../Page/迪尔伯恩县.md "wikilink")，临近[辛辛那提](../Page/辛辛那提.md "wikilink")。

2005年，印第安纳州居民户收入中位数为43,993美元。接近498,700居民，大约20%户，户收入在50,000美元到74,999美元。[汉密尔顿县的户收入中位数比印第安纳州平均高](../Page/汉密尔顿县.md "wikilink")35,000美元，大约为78,932美元，在全美人口少于25万的县中，排名第七。

### 宗教

[宗教上](../Page/宗教.md "wikilink")，印第安納州大多是新教徒，雖然也有五分之一的羅馬天主教徒。此州天主教所辦的[聖母大學相當著名](../Page/聖母大學.md "wikilink")，本州另有[路德會所属的](../Page/路德會.md "wikilink")[瓦爾帕萊索大學](../Page/瓦爾帕萊索大學.md "wikilink")。各人口信仰比例為

  - 67%新教徒
  - 20% [羅馬天主教徒](../Page/羅馬天主教.md "wikilink")
  - 1%其他基督教派的教徒
  - 1%其他宗教教徒
  - 8%無信仰者

印第安納州前三大新教徒是：[浸禮會](../Page/浸禮會.md "wikilink")（17%）、[衛理公會](../Page/衛理公會.md "wikilink")（10%）、[路德會](../Page/路德會.md "wikilink")（5%）。

## 重要城鎮

[Daleville-indiana-from-above.jpg](https://zh.wikipedia.org/wiki/File:Daleville-indiana-from-above.jpg "fig:Daleville-indiana-from-above.jpg")
[Chase_Building,_Lafayette,_IN.jpg](https://zh.wikipedia.org/wiki/File:Chase_Building,_Lafayette,_IN.jpg "fig:Chase_Building,_Lafayette,_IN.jpg")\]\]

人口在100万以上的（城市居民）

  - [印第安纳波利斯](../Page/印第安纳波利斯.md "wikilink")，首府，位于州中央

人口在10万以上的（城市人口）

  - [韋恩堡](../Page/韦恩堡_\(印第安纳州\).md "wikilink")，位于东北
  - [南本德](../Page/南本德_\(印地安納州\).md "wikilink")，靠近密歇根边境，[圣母大学所在地](../Page/圣母大学.md "wikilink")
  - [埃文斯維爾](../Page/埃文斯維爾_\(印地安納州\).md "wikilink")，位于西南，[俄亥俄河边](../Page/俄亥俄河.md "wikilink")
  - [加里](../Page/加里_\(印地安納州\).md "wikilink")，位于西北，[芝加哥近郊](../Page/芝加哥.md "wikilink")

縣

| city_1 = 印第安納波利斯{{\!}}印第安納波利斯 | div_1 = 馬里昂縣 (印地安納州){{\!}}馬里昂縣 |
pop_1 = 863,002 | img_1 = Indianapolis-1872528.jpg

| city_2 = 韋恩堡 (印第安納州){{\!}}韋恩堡 | div_2 = 艾倫縣 (印第安納州){{\!}}艾倫縣 |
pop_2 = 265,904 | img_2 = Downtown Fort Wayne, Indiana Skyline from
Old Fort, May 2014.jpg

| city_3 = 埃文斯維爾 (印第安納州){{\!}}伊凡斯維爾 | div_3 = 范德堡縣 (印第安納州){{\!}}范德堡縣 |
pop_3 = 118,930 | img_3 = Evilleriverfront.jpg

| city_4 = 南本德 (印第安納州){{\!}}南本德 | div_4 = 聖約瑟夫縣 (印第安納州){{\!}}聖約瑟夫縣 |
pop_4 = 102,245 | img_4 = South-bend-indiana-courthouse.jpg

| city_5 = 卡梅爾 (印第安納州){{\!}}卡梅爾 | div_5 = 漢密爾頓縣_(印地安納州){{\!}}漢密爾頓縣 |
pop_5 = 92,198 | img_5 = Carmel-indiana-art-and-design-district.jpg

| city_6 = 費希爾斯 (印第安納州){{\!}}費希爾斯 | div_6 = 漢密爾頓縣_(印地安納州){{\!}}漢密爾頓縣
| pop_6 = 91,832 | img_6 =

| city_7 = 布盧明頓 (印第安納州){{\!}}布盧明頓 | div_7 = 門羅縣 (印第安納州){{\!}}門羅縣 |
pop_7 = 85,071 | img_7 =

| city_8 = 哈蒙德 (印第安納州){{\!}}哈蒙德 | div_8 = 萊克縣 (印第安納州){{\!}}萊克縣 |
pop_8 = 76,618 | img_8 =

| city_9 = 加里 (印第安納州){{\!}}加里 | div_9 = 萊克縣 (印第安納州){{\!}}萊克縣 | pop_9
= 76,008 | img_9 =

| city_10 = 拉法葉 (印第安納州){{\!}}拉法葉 | div_10 = 蒂珀卡努縣{{\!}}蒂珀卡努縣 | pop_10
= 72,390 | img_10 = Chase Building, Lafayette, IN.jpg }}

## 教育

### 大學與學院

<table>
<tbody>
<tr class="odd">
<td><p><small> 公立學校</p>
<ul>
<li><a href="../Page/州立博爾大學.md" title="wikilink">州立博爾大學</a>（Ball State University）</li>
<li><a href="../Page/印第安納州立大學.md" title="wikilink">印第安納州立大學</a>（Indiana State University）</li>
<li><a href="../Page/印第安納大學.md" title="wikilink">印第安納大學</a>（Indiana University System）
<ul>
<li><a href="../Page/印第安納大學.md" title="wikilink">伯明頓主校區</a>（Indiana University at Bloomington）</li>
<li><a href="../Page/印第安納大學東部分校.md" title="wikilink">東部分校</a>（Indiana University East）</li>
<li><a href="../Page/印第安納大學科克摩分校.md" title="wikilink">科克摩分校</a>（Indiana University at Kokomo）</li>
<li><a href="../Page/印第安納大學西北分校.md" title="wikilink">西北分校</a>（Indiana University Northwest）</li>
<li><a href="../Page/印第安納大學南彎分校.md" title="wikilink">南彎分校</a>（Indiana University at South Bend）</li>
<li><a href="../Page/印第安納大學東南分校.md" title="wikilink">東南分校</a>（Indiana University Southeast）</li>
<li><a href="../Page/印第安納大學與普渡大學哥倫布聯合分校.md" title="wikilink">印第安納大學與普渡大學哥倫布聯合分校</a>（Indiana University Purdue University at Columbus）</li>
<li><a href="../Page/印第安納大學與普渡大學韋恩堡聯合分校.md" title="wikilink">印第安納大學與普渡大學韋恩堡聯合分校</a>（Indiana University Purdue University at Fort Wayne, IPFW）</li>
<li><a href="../Page/印第安納大學與普渡大學印第安納波利斯聯合分校.md" title="wikilink">印第安納大學與普渡大學印第安納波利斯聯合分校</a>（Indiana University Purdue University at Indianapolis, IUPUI）</li>
</ul></li>
<li><a href="../Page/普度大學.md" title="wikilink">普度大學</a>（Purdue University System）
<ul>
<li><a href="../Page/普度大學.md" title="wikilink">西拉法葉主校區</a>（Purdue University, West Lafayette）</li>
<li><a href="../Page/普度大學卡羅美分校.md" title="wikilink">卡羅美分校</a>（Purdue University Calumet）</li>
<li><a href="../Page/普度大學卡羅美分校.md" title="wikilink">中北部分校</a>（Purdue University North Central）</li>
<li><a href="../Page/印第安納大學─普度大學哥倫布分校.md" title="wikilink">印第安納大學─普度大學哥倫布分校</a>（Indiana University Purdue University at Columbus）</li>
<li><a href="../Page/印第安納大學─普度大學韋恩堡分校.md" title="wikilink">印第安納大學─普度大學韋恩堡分校</a>（Indiana University Purdue University at Fort Wayne, IPFW）</li>
<li><a href="../Page/印第安納大學─普度大學印第安納波利斯分校.md" title="wikilink">印第安納大學─普度大學印第安納波利斯分校</a>（Indiana University Purdue University at Indianapolis, IUPUI）</li>
<li>普度大學技術學院（Purdue University School of Technology）
<ul>
<li><a href="../Page/普度大學技術學院安德生分校.md" title="wikilink">安德生分校</a>（Purdue University School of Technology at Anderson）</li>
<li><a href="../Page/普度大學技術學院哥倫布分校.md" title="wikilink">哥倫布分校</a>（Purdue University School of Technology at Columbus）</li>
<li><a href="../Page/普度大學技術學院印第安納波利斯分校.md" title="wikilink">印第安納波利斯分校</a>（Purdue University School of Technology at Indianapolis）</li>
<li><a href="../Page/普度大學技術學院科克摩分校.md" title="wikilink">科克摩分校</a>（Purdue University School of Technology at Kokomo）</li>
<li><a href="../Page/普度大學技術學院曼希分校.md" title="wikilink">曼希分校</a>（Purdue University School of Technology at Muncie）</li>
<li><a href="../Page/普度大學技術學院新亞伯尼分校.md" title="wikilink">新亞伯尼分校</a>（Purdue University School of Technology at New Albany）</li>
<li><a href="../Page/普度大學技術學院裡奇蒙分校.md" title="wikilink">裡奇蒙分校</a>（Purdue University School of Technology at Richmond）</li>
<li><a href="../Page/普度大學技術學院南彎─艾克哈分校.md" title="wikilink">南彎─艾克哈分校</a>（Purdue University School of Technology at South Bend/Elkhart）</li>
<li><a href="../Page/普度大學技術學院佛塞雷斯分校.md" title="wikilink">佛塞雷斯分校Purdue</a> University School of Technology at Versailles</li>
</ul></li>
</ul></li>
<li><a href="../Page/南印第安納大學.md" title="wikilink">南印第安納大學</a>（University of Southern Indiana）</li>
</ul>
<p></small></p></td>
<td><p><small> 私立學校</p>
<ul>
<li><a href="../Page/安德生大學.md" title="wikilink">安德生大學</a>（Anderson University）</li>
<li><a href="../Page/貝瑟學院_印第安納.md" title="wikilink">貝瑟學院</a>（Bethel College, Indiana）</li>
<li><a href="../Page/巴特勒大學.md" title="wikilink">巴特勒大學</a>（Butler University）</li>
<li><a href="../Page/聖約瑟夫卡羅美學院.md" title="wikilink">聖約瑟夫卡羅美學院</a>（Calumet College of St. Joseph）</li>
<li><a href="../Page/基督學院.md" title="wikilink">基督學院</a>（Christian Theological Seminary）</li>
<li><a href="../Page/韋恩堡協同神學院.md" title="wikilink">韋恩堡協同神學院</a>（Concordia Theological Seminary Fort Wayne）</li>
<li><a href="../Page/德葩大学.md" title="wikilink">德葩大学</a> （DePauw University）</li>
<li><a href="../Page/厄爾漢學院.md" title="wikilink">厄爾漢學院</a>（Earlham College）</li>
<li><a href="../Page/富蘭克林學院.md" title="wikilink">富蘭克林學院</a>（Franklin College）</li>
<li><a href="../Page/高盛學院.md" title="wikilink">高盛學院</a>（Goshen College）</li>
<li><a href="../Page/葛雷斯學院.md" title="wikilink">葛雷斯學院</a>（Grace College）</li>
<li><a href="../Page/漢諾佛學院.md" title="wikilink">漢諾佛學院</a>（Hanover College）</li>
<li><a href="../Page/聖十字學院.md" title="wikilink">聖十字學院</a>（Holy Cross College）</li>
<li><a href="../Page/杭廷頓學院.md" title="wikilink">杭廷頓學院</a>（Huntington College）</li>
<li><a href="../Page/印第安納理工學院.md" title="wikilink">印第安納理工學院</a>（Indiana Institute of Technology）</li>
<li><a href="../Page/印第安納魏斯理大學.md" title="wikilink">印第安納魏斯理大學</a>（Indiana Wesleyan University）</li>
<li><a href="../Page/曼徹斯特學院.md" title="wikilink">曼徹斯特學院</a>（Manchester College）</li>
<li><a href="../Page/馬利安學院.md" title="wikilink">馬利安學院</a>（Marian College）</li>
<li><a href="../Page/馬丁大學.md" title="wikilink">馬丁大學</a>（Martin University）</li>
<li><a href="../Page/奧克蘭市大學.md" title="wikilink">奧克蘭市大學</a>（Oakland City University）</li>
<li><a href="../Page/羅斯─豪曼理工學院.md" title="wikilink">羅斯─豪曼理工學院</a>（Rose-Hulman Institute of Technology）</li>
<li><a href="../Page/聖約瑟夫學院_印第安納.md" title="wikilink">聖約瑟夫學院</a>（Saint Joseph's College, Indiana）</li>
<li><a href="../Page/森林聖瑪麗學院.md" title="wikilink">森林聖瑪麗學院</a>（Saint Mary-of-the-Woods College）</li>
<li><a href="../Page/聖瑪麗學院_印第安納.md" title="wikilink">聖瑪麗學院</a>（Saint Mary's College, Indiana）</li>
<li><a href="../Page/泰勒大學.md" title="wikilink">泰勒大學</a>（Taylor University）</li>
<li><a href="../Page/三州大學.md" title="wikilink">三州大學</a>（Tri-State University）</li>
<li><a href="../Page/伊凡斯維爾大學.md" title="wikilink">伊凡斯維爾大學</a>（University of Evansville）</li>
<li><a href="../Page/印第安納波利斯大學.md" title="wikilink">印第安納波利斯大學University</a> of Indianapolis</li>
<li><a href="../Page/聖母大學.md" title="wikilink">聖母大學</a>（University of Notre Dame）</li>
<li><a href="../Page/聖法蘭西斯大學.md" title="wikilink">聖法蘭西斯大學</a>（University of Saint Francis）</li>
<li><a href="../Page/瓦爾帕萊索大學.md" title="wikilink">瓦爾帕萊索大學</a>（Valparaiso University）</li>
<li><a href="../Page/瓦伯西學院.md" title="wikilink">瓦伯西學院</a>（Wabash College）</li>
</ul>
<p></small></p></td>
</tr>
</tbody>
</table>

## 交通

印第安納州被譽為“美國的十字路口”，多條鐵路，高速公路在印第安納交匯。

### 鐵路運輸

印第安納波利斯曾是歷史上美國中西部地區第二大鐵路樞紐，僅次於芝加哥。[印第安納波利斯聯合車站為州內最大](../Page/印第安納波利斯聯合車站.md "wikilink")，也是美國歷史上第一座聯合車站。目前[美國國鐵經營旅客列車中](../Page/美國國鐵.md "wikilink")，有[綠水號](../Page/綠水號.md "wikilink")，[狼獾號](../Page/狼獾號.md "wikilink")，[佩雷\`馬凱特號號](../Page/佩雷\`馬凱特號號.md "wikilink")，[首都特快](../Page/首都特快.md "wikilink")，[湖畔特快](../Page/湖畔特快.md "wikilink")，[紅衣主教號和](../Page/紅衣主教號.md "wikilink")[印第安那州號經過印第安納州](../Page/印第安那州號.md "wikilink")。其中除[紅衣主教號和](../Page/紅衣主教號.md "wikilink")[印第安那州號通過印第安納波利斯外](../Page/印第安那州號.md "wikilink")，其餘線路均服務于北印第安納州。

芝加哥Metra[通勤列車也延伸至北印第安納](../Page/通勤列車.md "wikilink")[南本德市](../Page/南本德_\(印地安納州\).md "wikilink")。

### 重要機場

  - [印第安纳波利斯國際機場](../Page/印第安纳波利斯國際機場.md "wikilink")（IND）

### 重要高速公路

  - [I-64.svg](https://zh.wikipedia.org/wiki/File:I-64.svg "fig:I-64.svg")[64號州際公路](../Page/64號州際公路.md "wikilink")：東西向，東接[肯塔基州](../Page/肯塔基州.md "wikilink")[路易維爾](../Page/路易維爾.md "wikilink")，西續行至[伊利諾州南部與](../Page/伊利諾州.md "wikilink")[密蘇里州](../Page/密蘇里州.md "wikilink")[聖路易](../Page/聖路易.md "wikilink")。
  - [I-65.svg](https://zh.wikipedia.org/wiki/File:I-65.svg "fig:I-65.svg")[65號州際公路](../Page/65號州際公路.md "wikilink")：西北-東南向，西北與[I-80.svg](https://zh.wikipedia.org/wiki/File:I-80.svg "fig:I-80.svg")[I-90.svg](https://zh.wikipedia.org/wiki/File:I-90.svg "fig:I-90.svg")[印州收費公路交接](../Page/印州收費公路.md "wikilink")，東南續行至[肯塔基州](../Page/肯塔基州.md "wikilink")。
  - [I-69.svg](https://zh.wikipedia.org/wiki/File:I-69.svg "fig:I-69.svg")[69號州際公路](../Page/69號州際公路.md "wikilink")：東北-西南向，東北續行至[密西根州](../Page/密西根州.md "wikilink")，西南續行至[肯塔基州](../Page/肯塔基州.md "wikilink")。部分路段尚在興建中。
  - [I-70.svg](https://zh.wikipedia.org/wiki/File:I-70.svg "fig:I-70.svg")[70號州際公路](../Page/70號州際公路.md "wikilink")：東西向，東續行至[俄亥俄州](../Page/俄亥俄州.md "wikilink")，往西續行至[伊利諾州](../Page/伊利諾州.md "wikilink")、[密蘇里州](../Page/密蘇里州.md "wikilink")[聖路易](../Page/聖路易.md "wikilink")。
  - [I-74.svg](https://zh.wikipedia.org/wiki/File:I-74.svg "fig:I-74.svg")[74號州際公路](../Page/74號州際公路.md "wikilink")：西北西-東南東向，西接[伊利諾州](../Page/伊利諾州.md "wikilink")，東接[俄亥俄州](../Page/俄亥俄州.md "wikilink")[辛辛那提](../Page/辛辛那提.md "wikilink")。
  - [I-80.svg](https://zh.wikipedia.org/wiki/File:I-80.svg "fig:I-80.svg")[I-90.svg](https://zh.wikipedia.org/wiki/File:I-90.svg "fig:I-90.svg")[印州收費公路](../Page/印州收費公路.md "wikilink")：位於全州最北端，為東西向收費道路，西接[伊利諾州](../Page/伊利諾州.md "wikilink")，東接[俄亥俄州](../Page/俄亥俄州.md "wikilink")。
  - [I-94.svg](https://zh.wikipedia.org/wiki/File:I-94.svg "fig:I-94.svg")[94號州際公路](../Page/94號州際公路.md "wikilink")：位於全州最北端，為東西向道路，西接[伊利諾州](../Page/伊利諾州.md "wikilink")[芝加哥](../Page/芝加哥.md "wikilink")，東接[密西根州](../Page/密西根州.md "wikilink")。
  - [I-265.svg](https://zh.wikipedia.org/wiki/File:I-265.svg "fig:I-265.svg")[265號州際公路](../Page/265號州際公路.md "wikilink")：環繞[路易維爾的州際公路](../Page/路易維爾.md "wikilink")，僅一小段在印第安納州境內。
  - [I-275.svg](https://zh.wikipedia.org/wiki/File:I-275.svg "fig:I-275.svg")[275號州際公路](../Page/275號州際公路.md "wikilink")：環繞[辛辛那提的州際公路](../Page/辛辛那提.md "wikilink")，僅一小段在印第安納州境內。
  - [I-465.svg](https://zh.wikipedia.org/wiki/File:I-465.svg "fig:I-465.svg")[465號州際公路](../Page/465號州際公路.md "wikilink")：環繞[印第安納波利斯的州際公路](../Page/印第安納波利斯.md "wikilink")。
  - [I-469.svg](https://zh.wikipedia.org/wiki/File:I-469.svg "fig:I-469.svg")[469號州際公路](../Page/469號州際公路.md "wikilink")：環繞[韋恩堡的州際公路](../Page/韋恩堡.md "wikilink")。
  - [I-865.svg](https://zh.wikipedia.org/wiki/File:I-865.svg "fig:I-865.svg")[865號州際公路](../Page/865號州際公路.md "wikilink")：銜接[I-65.svg](https://zh.wikipedia.org/wiki/File:I-65.svg "fig:I-65.svg")[65號州際公路與](../Page/65號州際公路.md "wikilink")[I-465.svg](https://zh.wikipedia.org/wiki/File:I-465.svg "fig:I-465.svg")[465號州際公路的補助線](../Page/465號州際公路.md "wikilink")。

## 重要体育团体

### 美式足球

  - [NFL](../Page/NFL.md "wikilink")
      - [印第安納波利斯小馬](../Page/印第安納波利斯小馬.md "wikilink")（Indianapolis Colts）
  - [NCAA](../Page/NCAA.md "wikilink")
      - [圣母大学](../Page/圣母大学.md "wikilink")（Notre Dame）(Fighting Irish）
      - [普渡大学](../Page/普渡大学.md "wikilink")（Purdue
        University）（Boilermakers）
      - [印第安納大學](../Page/印第安納大學.md "wikilink")（Indiana
        University）（Hoosiers）
      - Ball State University（Cardinals）
      - Valparaiso University（Crusaders）

### 棒球

  - [小聯盟](../Page/小聯盟.md "wikilink")
      - 印第安納波利斯印地安人（Indianapolis
        Indians，3A級國際聯盟，母隊：[匹茲堡海盗](../Page/匹茲堡海盗.md "wikilink")）
      - 韋恩堡巫師（Fort Wayne
        Wizards，1A級中西部聯盟，母隊：[聖地牙哥教士](../Page/聖地牙哥教士.md "wikilink")）
      - 南彎銀鷹（South Bend Silver
        Hawks，1A級中西部聯盟，母隊：[亞利桑那響尾蛇](../Page/亞利桑那響尾蛇.md "wikilink")）

### 籃球

  - [NBA](../Page/NBA.md "wikilink")
      - [印第安纳步行者](../Page/印第安纳步行者.md "wikilink")（Indiana Pacers）
  - [WNBA](../Page/WNBA.md "wikilink")
      - [印第安納狂热](../Page/印第安納狂热.md "wikilink")（Indiana Fever）
  - [NCAA](../Page/NCAA.md "wikilink")
      - [印第安納大學](../Page/印第安納大學.md "wikilink")（Indiana
        University)（Hoosiers）
      - [普渡大学](../Page/普渡大学.md "wikilink")（Purdue
        University)（Boilermakers）
      - Indiana State University (Sycamores)
      - [圣母大学](../Page/圣母大学.md "wikilink")（Notre Dame) (Fighting Irish）
      - [印第安納大學─普度大學印第安納波利斯分校](../Page/印第安納大學─普度大學印第安納波利斯分校.md "wikilink")（IUPUI）（Jaguars）
      - [Ball State
        University](../Page/Ball_State_University.md "wikilink")
        (Cardinals)
      - 印第安纳大学－普度大学韦恩堡分校 (IPFW) (Masterdons)
      - 法尔帕拉索大学Valparaiso University (Crusaders)
      - \*\*普渡大学 (Purdue University) (Lady Boilermakers)
      - [印第安納大學](../Page/印第安納大學.md "wikilink")（Lady Hoosiers）
      - Indiana State University (Lady Sycamores)
      - [Ball State
        University](../Page/Ball_State_University.md "wikilink") (Lady
        Cardinals)
      - 圣母大学 (Notre Dame) (Fighting Lady Irish)
      - [印第安納大學─普度大學印第安納波利斯分校](../Page/印第安納大學─普度大學印第安納波利斯分校.md "wikilink")
        (Lady Jaguars)
      - 印第安纳大学－普度大学韦恩堡分校 (Lady Masterdons)
      - 法尔帕拉索大学Valparaiso University (Lady Crusaders)

### 賽車

  - [印第安納波利斯500](../Page/印第安納波利斯500.md "wikilink")
  - [F1方程式賽車](../Page/F1.md "wikilink")[美國大獎賽](../Page/美國大獎賽.md "wikilink")（已取消）

## 其他資訊

### 印第安納時間（2006年起廢止）

美國絕大多數地區實施[日光節約時間](../Page/日光節約時間.md "wikilink")，印第安納州由於地處美東時區西緣，州民決議不使用日光節約時間，以免夏季日落太晚，但西北角[芝加哥](../Page/芝加哥.md "wikilink")（[伊利諾州](../Page/伊利諾州.md "wikilink")）附近（Jasper,
Lake, LaPorte, Newton,
Porter等五郡）及西南角[伊凡斯維爾附近](../Page/伊凡斯維爾.md "wikilink")（Gibson,
Posey, Spencer, Vanderburgh,
Warrick等五郡）和美中時區一致，東南[辛辛那堤](../Page/辛辛那堤.md "wikilink")（[俄亥俄州](../Page/俄亥俄州.md "wikilink")）附近（Dearborn及Ohio等二郡）及[路易維爾](../Page/路易維爾.md "wikilink")（[肯塔基州](../Page/肯塔基州.md "wikilink")）附近（Clark,
Floyd,
Harrison三郡）則和美東時區一致，意即在大城市附近的郡和大城市一起作息。因此，全州分成三個時區：1.美東時間：辛辛那堤及路易維爾附近；2.美中時間：芝加哥及伊凡斯維爾附近及3.印第安納時間：其他地區（冬季與美東時間一致，夏季與美中時間一致）

2005年4月28日，印第安納州議會決議自2006年4月開始實施日光節約時間，原本使用印第安納時間的地區納入美東時區。

## 著名人物

### 娛樂界

  - [麥可傑克森](../Page/麥可傑克森.md "wikilink")，美國著名唱片[歌手](../Page/歌手.md "wikilink")，出生於[加里](../Page/加里_\(印第安納州\).md "wikilink")。
  - [珍娜·傑克森](../Page/珍妮·杰克逊.md "wikilink")，美國著名唱片[歌手與](../Page/歌手.md "wikilink")[女演員](../Page/女演員.md "wikilink")，，出生於[加里](../Page/加里_\(印第安納州\).md "wikilink")。
  - [史提夫·麥昆](../Page/史提夫·麥昆.md "wikilink")，美國著名[好萊塢](../Page/好萊塢.md "wikilink")[動作片](../Page/動作片.md "wikilink")[男演員](../Page/男演員.md "wikilink")。
  - [埃克索爾·羅斯](../Page/埃克索爾·羅斯.md "wikilink")，美國著名唱片[歌手](../Page/歌手.md "wikilink")，[硬式搖滾樂團](../Page/硬式搖滾.md "wikilink")[槍與玫瑰原始成員](../Page/槍與玫瑰.md "wikilink")，出生於[拉法葉](../Page/拉斐特_\(印第安纳州\).md "wikilink")。
  - [亞當·蘭伯特](../Page/亞當·蘭伯特.md "wikilink")，美國[歌手](../Page/歌手.md "wikilink")。
  - [吉姆·戴維斯](../Page/吉姆·戴維斯_\(漫畫家\).md "wikilink")，漫畫家，[加菲貓系列連載漫畫的作者](../Page/加菲貓.md "wikilink")。

### 政治界

  - [班傑明·哈瑞森](../Page/本杰明·哈里森.md "wikilink")，[美國第二十三任](../Page/美國.md "wikilink")[總統](../Page/總統.md "wikilink")，也是目前唯一印第安納州出身的美國總統（哈瑞森自21歲起至逝世的主要活動均在印第安納州）。在[印第安納波利斯有其故居可供參觀](../Page/印第安納波利斯.md "wikilink")。
  - [托馬斯·A·亨德里克斯](../Page/托馬斯·A·亨德里克斯.md "wikilink")，[美國第二十一任](../Page/美國.md "wikilink")[副總統](../Page/副總統.md "wikilink")。曾任[印第安納州州長](../Page/印第安納州州長.md "wikilink")。
  - [查爾斯·W·費爾班克斯](../Page/查爾斯·W·費爾班克斯.md "wikilink")，[美國第二十六任](../Page/美國.md "wikilink")[副總統](../Page/副總統.md "wikilink")。
  - [湯瑪斯·R·馬歇爾](../Page/托马斯·R·马歇尔.md "wikilink")，[美國第二十八任](../Page/美國.md "wikilink")[副總統](../Page/副總統.md "wikilink")，出生於[沃巴什郡](../Page/沃巴什縣_\(印地安納州\).md "wikilink")。曾任[印第安納州州長](../Page/印第安納州州長.md "wikilink")。
  - [丹·奎爾](../Page/丹·奎爾.md "wikilink")，[美國第四十四任](../Page/美國.md "wikilink")[副總統](../Page/副總統.md "wikilink")，出生於[印第安納波利斯](../Page/印第安納波利斯.md "wikilink")。
  - [麥克·彭斯](../Page/迈克·彭斯.md "wikilink")，[美國第四十八任](../Page/美國.md "wikilink")（現任）[副總統](../Page/副總統.md "wikilink")，出生於[哥倫布](../Page/哥倫布_\(印第安納州\).md "wikilink")。曾任[印第安納州州長](../Page/印第安納州州長.md "wikilink")、眾議院共和黨黨團會議主席等。

### 商業界

  - [哈兰德·桑德斯](../Page/哈兰德·桑德斯.md "wikilink")，肯德基创始人，出生於[亨利維爾](../Page/亨利維爾_\(印第安那州\).md "wikilink")。
  - [約翰·迪林傑](../Page/約翰·迪林傑.md "wikilink")，是二十世紀三十年代大蕭條時期中，活躍於美國中西部的銀行搶匪和美國黑幫的一員。

### 科學界

  - [弗蘭克·博爾曼](../Page/弗蘭克·博爾曼.md "wikilink")：太空人，出生於[加里](../Page/加里_\(印第安納州\).md "wikilink")。

### 學術界

  - [保羅·薩繆爾森](../Page/保羅·薩繆爾森.md "wikilink")：經濟學家，1970年[諾貝爾經濟學獎得主](../Page/諾貝爾經濟學獎.md "wikilink")，出生於[加里](../Page/加里_\(印第安納州\).md "wikilink")。
  - [約瑟夫·史迪格里茲](../Page/约瑟夫·斯蒂格利茨.md "wikilink")：經濟學家，2001年[諾貝爾經濟學獎得主](../Page/諾貝爾經濟學獎.md "wikilink")，出生於[加里](../Page/加里_\(印第安納州\).md "wikilink")。

## 參見

## 外部連結

  - [州政府官方網站](http://www.in.gov)
  - [Indiana time
    zone](http://www.timetemperature.com/tzus/indiana_time_zone.shtml)

[Category:美国州份](../Category/美国州份.md "wikilink")
[\*](../Category/印第安纳州.md "wikilink")
[Category:美国中西部](../Category/美国中西部.md "wikilink")

1.

2.
3.  <http://www.nps.gov/neri/parkmgmt/upload/NERI-GARI-final-2-CA-2011.pdf>