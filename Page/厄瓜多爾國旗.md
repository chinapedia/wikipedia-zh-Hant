**厄瓜多國旗**為一面三色旗，纵横比例為2:3，由闊度比例為2:1:1的黃、藍、紅三條橫條
（即[米蘭達三色旗](../Page/米蘭達三色旗.md "wikilink")）組成。中間有[國徽者為政府旗及軍旗](../Page/厄瓜多爾國徽.md "wikilink")，無者為民用旗。

目前的厄瓜多尔國旗色彩採用於1860年9月26日，1900年12月5日旗帜中央增添[国徽](../Page/厄瓜多尔国徽.md "wikilink")，2009年11月16日将纵横比例由1:2改为2:3。

三色的解釋為：黃色象徵黃金、農業和礦業資源；藍色代表天空、海洋和赤道；紅色代表為自由和獨立犧牲者的血。\[1\]

## 历史国旗

[Municipal_Flag_of_Ecuador.svg](https://zh.wikipedia.org/wiki/File:Municipal_Flag_of_Ecuador.svg "fig:Municipal_Flag_of_Ecuador.svg")
[Flag_of_Ecuador_(1900-2009).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Ecuador_\(1900-2009\).svg "fig:Flag_of_Ecuador_(1900-2009).svg")

### 其他旗幟

### 相似国旗

## 参考文献

<div class="references-small">

<references />

</div>

[Category:厄瓜多爾](../Category/厄瓜多爾.md "wikilink")
[E](../Category/國旗.md "wikilink")

1.  [厄瓜多爾總統府](http://www.presidencia.gov.ec/modulos.asp?id=19)