**NBA总冠军球队大名单**，包括历届取得最终[NBA总冠军球队的球员和主教练](../Page/NBA总冠军.md "wikilink")，此名单仅列出参加最后季后赛的球员名单，在赛季中被转会交易或未能入选季后赛大名单的球员不在此名单中。

## 参看

  - [NBA总决赛](../Page/NBA总决赛.md "wikilink"),[NBA](../Page/NBA.md "wikilink")

[category:NBA](../Page/category:NBA.md "wikilink")
[category:NBA总决赛](../Page/category:NBA总决赛.md "wikilink")