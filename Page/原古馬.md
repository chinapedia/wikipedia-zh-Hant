**原古馬**（[學名](../Page/學名.md "wikilink")*Propalaeotherium*）是一[屬早期的](../Page/屬.md "wikilink")[奇蹄目](../Page/奇蹄目.md "wikilink")，是[馬的祖先](../Page/馬.md "wikilink")。牠的學名意思是「[古獸馬之前](../Page/古獸馬.md "wikilink")」，可見牠是古獸馬的祖先。雖然原古馬及古獸馬都是由[始祖馬](../Page/始祖馬.md "wikilink")[演化而來](../Page/演化.md "wikilink")，但牠們並非現今馬的祖先。牠們於4千5百萬年前[滅絕](../Page/滅絕.md "wikilink")，並沒有後繼的[物種](../Page/物種.md "wikilink")。

原古馬是細少的[動物](../Page/動物.md "wikilink")，肩高30-60厘米。牠們的外貌很像非常細少的[貘](../Page/貘.md "wikilink")。牠們沒有蹄，只有幾根細少像釘的小蹄。牠們是[草食性的](../Page/草食性.md "wikilink")，而在[德國](../Page/德國.md "wikilink")[麥塞爾發現的](../Page/麥塞爾.md "wikilink")[化石顯示牠們是吃](../Page/化石.md "wikilink")[漿果及葉子](../Page/漿果.md "wikilink")。

## 外部連結

  - [BBC Online Science and
    Nature](http://www.bbc.co.uk/beasts/evidence/prog1/page3_2.shtml)
  - [馬的演化](http://www.talkorigins.org/faqs/horses/horse_evol.html)

[Category:古獸馬科](../Category/古獸馬科.md "wikilink")
[Category:歐洲史前哺乳動物](../Category/歐洲史前哺乳動物.md "wikilink")
[Category:亞洲史前哺乳動物](../Category/亞洲史前哺乳動物.md "wikilink")