**原始吶喊**是提名全英音樂獎的非主流樂隊，1982年成立於[蘇格蘭](../Page/蘇格蘭.md "wikilink")[格拉斯哥](../Page/格拉斯哥.md "wikilink")。目前成員有主唱
[Bobby Gillespie](../Page/Bobby_Gillespie.md "wikilink")、吉他手 [Andrew
Innes](../Page/Andrew_Innes.md "wikilink")、鍵盤手 [Martin
Duffy](../Page/Martin_Duffy.md "wikilink")、貝斯手 [Gary "Mani"
Mounfield](../Page/Gary_"Mani"_Mounfield.md "wikilink") 以及鼓手 [Darrin
Mooney](../Page/Darrin_Mooney.md "wikilink")，而 [Barrie
Coddigan](../Page/Barrie_Coddigan.md "wikilink") 在2006年的巡迴之中短期擔任替代原吉他手
[Robert "Throb" Young](../Page/Robert_"Throb"_Young.md "wikilink") 的位置，而
Young 的正式歸隊聲明尚未宣佈。至今他們已經突破銷售了千萬張的銷售量。

樂隊在1982-1984年間進行表演，但是他們的表演事業並未因而起飛，直到主唱離開樂隊加入 [Jesus And Mary
Chain](../Page/Jesus_And_Mary_Chain.md "wikilink")
擔任鼓手，該樂隊在[八零年代中期的](../Page/八零年代.md "wikilink")[獨立音樂圈具有相當地位](../Page/獨立音樂.md "wikilink")。樂隊捨棄了初期的
jangly sound
後，漸漸轉向[迷幻以及](../Page/迷幻.md "wikilink")[車庫搖滾的走向](../Page/車庫搖滾.md "wikilink")，同時音樂中也加入了許多[舞曲元素](../Page/舞曲.md "wikilink")。樂隊
1991 年的專輯 吶喊症候群 ([Screamadelica](../Page/Screamadelica.md "wikilink"))
突破了自我正式進入主流圈。儘管樂隊成員有多次改組，樂隊確實得到了市場上的成功並且至今仍持續錄製新作品以及巡迴表演當中。

## 樂隊歷史

### 初期組成 (1982 - 1985)

Bobby Gillespie 搬到格拉斯哥東南方的 Mount Florida，在就讀 Kings Park Secondary School
時初遇 Robert Young。另一位朋友 Alan McGee 帶 Gillespie 到他的首場現場表演，由 Thin Lizzy
演出。兩人深受[龐克音樂影響而在](../Page/龐克.md "wikilink") 1978
年加入當地的[龐克樂隊](../Page/龐克.md "wikilink") The
Drains，該樂隊吉他手為十五歲的 Andrew Innes。該樂隊並不長壽，當 Gillespie
選擇留在[格拉斯哥的同時](../Page/格拉斯哥.md "wikilink") Innes 以及
McGee 則前往[倫敦](../Page/倫敦.md "wikilink")。

[龐克浪潮結束後](../Page/龐克.md "wikilink")，Gillespie
對主流的[新浪潮音樂感到灰心](../Page/新浪潮.md "wikilink")。他與初識的朋友
Jim Beattie 分享他的看法並且錄製 "elemental noise tapes"，裡頭的 Gillespie 打著兩個垃圾箱蓋而
Beattie 則彈奏 Fuzz-guitar。在開始寫自己的歌前他們翻唱[地下絲絨](../Page/地下絲絨.md "wikilink")
(The Velvet Underground) 及[伯茲合唱團](../Page/伯茲合唱團.md "wikilink") (The
Byrds) 的歌曲，主要以 [Jah Wobble](../Page/Jah_Wobble.md "wikilink") 以及 [Peter
Hook](../Page/Peter_Hook.md "wikilink")
的[貝斯線為主](../Page/貝斯.md "wikilink")。 Gillespie 之後說這個樂團
"並無實質上的存在，他們只是想在每天晚上找點事情做。" 他們為自己命名原始吶喊，本意是一種使用在 Janovian
精神治療療法中的哭聲。本質上只屬合作關係，樂隊首次在1982年進行第一場表演。

### 首張專輯 (1986 - 1989)

### 成名之作 (1990 - 1992)

### 第四張專輯 (1992 - 1995)

### 第五張專輯 (1996 - 1998)

### 第六及第七張專輯 (1999 - 2005)

### 第八及第九張專輯 (2006 - 目前)

[Category:英國搖滾樂團](../Category/英國搖滾樂團.md "wikilink")
[Category:另類搖滾樂團](../Category/另類搖滾樂團.md "wikilink")
[Category:日本富士搖滾音樂祭參加歌手](../Category/日本富士搖滾音樂祭參加歌手.md "wikilink")