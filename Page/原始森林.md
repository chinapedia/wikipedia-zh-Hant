[Creek_and_old-growth_forest-Larch_Mountain.jpg](https://zh.wikipedia.org/wiki/File:Creek_and_old-growth_forest-Larch_Mountain.jpg "fig:Creek_and_old-growth_forest-Larch_Mountain.jpg")Larch
Mountain的原始森林\]\]
[Biogradska_suma.jpg](https://zh.wikipedia.org/wiki/File:Biogradska_suma.jpg "fig:Biogradska_suma.jpg")森林，位于[黑山](../Page/黑山.md "wikilink")国家公园\]\]

**原始森林**又稱**原生林**，是指一个[森林已经达到非常长久的年龄而没有遭到显著的干扰](../Page/森林.md "wikilink")，从而表现出独特的[生态特征](../Page/生态.md "wikilink")，并可能被归类为[顶极群落](../Page/顶极群落.md "wikilink")。\[1\]原生特性包括多样化的树有关的结构，提供多样化的野生动物[栖息地](../Page/栖息地.md "wikilink")，增加了森林生态系统的生物多样性。多样化的树结构的概念包括多层树冠和树冠间隙，很大变化的[树的高度和直径](../Page/树.md "wikilink")，多样的树种和纲，和多种大小的木质残体。

原始森林是[陆地](../Page/陆地.md "wikilink")[生态系统中核心](../Page/生态系统.md "wikilink")，具有保持[生态平衡](../Page/生态平衡.md "wikilink")、涵养[水份](../Page/水份.md "wikilink")、调节[气候](../Page/气候.md "wikilink")、净化[空气等作用](../Page/空气.md "wikilink")。原始森林不仅仅是高大的[树木](../Page/树木.md "wikilink")，而是一个综合的生态系统，包[动](../Page/动物.md "wikilink")[植物间的](../Page/植物.md "wikilink")[食物链关系](../Page/食物链.md "wikilink")，在原始森林中，某一物种的减少，可以影响其他物种的生存，这种情况在[热带雨林中体现更为突出](../Page/热带雨林.md "wikilink")。人们在破坏原始森林以后，即使人工补种了大量的[树林](../Page/树林.md "wikilink")，也无法弥补森林被破坏对生态带来的影响。

## 生态意义

原始森林能给[野生动物提供广阔的生存空间](../Page/野生动物.md "wikilink")，维持[生物的多样性](../Page/生物多样性.md "wikilink")，保持[生态平衡的作用](../Page/生态平衡.md "wikilink")；同时原始森林还能净化[空气](../Page/空气.md "wikilink")、调节[气候](../Page/气候.md "wikilink")、涵养[水份](../Page/水份.md "wikilink")、保持水土、增强[土壤肥力](../Page/土壤.md "wikilink")、减轻自然灾害等。

## 全球分布

全球的原始森林主要分布在[西伯利亚](../Page/西伯利亚.md "wikilink")、[中国的西南](../Page/中国.md "wikilink")、[东南亚](../Page/东南亚.md "wikilink")、[北美的北部](../Page/北美.md "wikilink")、[北欧](../Page/北欧.md "wikilink")、[亚马孙河流域](../Page/亚马孙河.md "wikilink")、[刚果盆地等地区](../Page/刚果盆地.md "wikilink")。

## 参看

## 参考文献

## 外部連結

  - [綠色和平：世界原始森林現狀](http://www.greenpeace.org/china/ch/campaigns/forests/crisis/world)

  - [明尼苏达州自然资源部](http://www.dnr.state.mn.us/forests_types/oldgrowth/importance.html)

  - [原始森林网络](http://oldgrowthforest.net)

[Category:各类型的正式指定森林](../Category/各类型的正式指定森林.md "wikilink")
[\*](../Category/原始森林.md "wikilink")
[Category:自然保育](../Category/自然保育.md "wikilink")

1.