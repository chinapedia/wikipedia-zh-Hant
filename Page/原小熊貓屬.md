**原小熊貓**（[學名](../Page/學名.md "wikilink")：**），又名**原貓**或**始貓**，是一類史前的[貓科動物](../Page/貓科.md "wikilink")，生存於2500萬年前的[歐洲](../Page/歐洲.md "wikilink")。牠們是現今貓科的祖先。

原小熊貓是細小的[動物](../Page/動物.md "wikilink")，只較現今的[家貓大少許](../Page/家貓.md "wikilink")，重約9公斤。牠們的尾巴很長，有大[眼睛](../Page/眼睛.md "wikilink")，鋒利的爪及[牙齒](../Page/牙齒.md "wikilink")，與[靈貓科有相似的比例](../Page/靈貓科.md "wikilink")。牠們可能像靈貓科般是棲於樹上的。\[1\]

原小熊貓之後出現的是生存於2000-1000萬年前的[假貓](../Page/假貓.md "wikilink")。

## 參考

[Category:古動物](../Category/古動物.md "wikilink")
[Category:中新世動物](../Category/中新世動物.md "wikilink")
[Category:原小熊貓亞科](../Category/原小熊貓亞科.md "wikilink")
[Category:漸新世哺乳類](../Category/漸新世哺乳類.md "wikilink")
[Category:中新世哺乳類](../Category/中新世哺乳類.md "wikilink")
[Category:歐洲史前哺乳動物](../Category/歐洲史前哺乳動物.md "wikilink")

1.