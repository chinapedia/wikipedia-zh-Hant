**原櫛龍屬**（屬名：*Prosaurolophus*）又名**原蜥冠龍**、**原蜥嵴龍**、**原蜥冠鱷**，意為「原始櫛龍」，是因冠飾與較晚期的[櫛龍類似而得名](../Page/櫛龍.md "wikilink")。原櫛龍是種[鴨嘴龍類](../Page/鴨嘴龍類.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生存於晚[白堊紀的](../Page/白堊紀.md "wikilink")[北美洲](../Page/北美洲.md "wikilink")。目前已經發現2個種與至少25個個體，包含[頭顱骨與骨骼](../Page/頭顱骨.md "wikilink")，但化石整體仍不明確。原櫛龍身長約9公尺，化石發現於[加拿大](../Page/加拿大.md "wikilink")[亞伯達省的](../Page/亞伯達省.md "wikilink")[恐龍公園組與](../Page/恐龍公園組.md "wikilink")[美國](../Page/美國.md "wikilink")[蒙大拿州的](../Page/蒙大拿州.md "wikilink")[雙麥迪遜組](../Page/雙麥迪遜組.md "wikilink")，年代為上[白堊紀的晚](../Page/白堊紀.md "wikilink")[坎潘階](../Page/坎潘階.md "wikilink")，約7600萬到7500萬年前\[1\]。原櫛龍最顯著的特徵是由[鼻骨形成的小型堅硬冠飾](../Page/鼻骨.md "wikilink")，在眼睛前方突出。

[模式種是](../Page/模式種.md "wikilink")**巨原櫛龍**（*P.
maximus*），是由[巴納姆·布郎](../Page/巴納姆·布郎.md "wikilink")（Barnum
Brown）在1916年所敘述、命名。第二個種是**黑腳原櫛龍**（*P.
blackfeetensis*），是由[傑克·霍納](../Page/傑克·霍納.md "wikilink")（Jack
Horner）在1992年所敘述、命名。兩個種的主要差異在頭冠的大小，與頭骨的比例。

## 敘述

[Prosaurolophus.jpg](https://zh.wikipedia.org/wiki/File:Prosaurolophus.jpg "fig:Prosaurolophus.jpg")\]\]
原櫛龍是種有大型頭部的鴨嘴龍類，最完整標本的身長為8.5公尺，而頭顱骨長度約0.9公尺\[2\]。原櫛龍的眼部前方擁有小型、低矮、三角形的冠飾，兩側有凹處。[肱骨相當短](../Page/肱骨.md "wikilink")\[3\]。

除了冠飾以外，原櫛龍是種不顯眼的[鴨嘴龍亞科恐龍](../Page/鴨嘴龍亞科.md "wikilink")。如同其他鴨嘴龍科，原櫛龍的頭部前段平坦、寬廣，具有喙嘴，適合咬斷樹葉與樹枝。嘴部的後段有數千顆牙齒，適合磨碎植物，再行吞食。

原櫛龍的兩個種差別在於冠飾特徵與輪廓，黑腳原櫛龍的面部比巨原櫛龍的還要陡峭、高。黑腳原櫛龍的冠飾邊緣在成長期間往眼睛後方生長，至少黑腳原櫛龍擁有這樣的成長模式\[4\]。

## 分類

[ROM-HadrosaurSkeleton.png](https://zh.wikipedia.org/wiki/File:ROM-HadrosaurSkeleton.png "fig:ROM-HadrosaurSkeleton.png")[皇家安大略博物館](../Page/皇家安大略博物館.md "wikilink")\]\]
因為名稱的緣故，原櫛龍經常與[櫛龍聯想在一起](../Page/櫛龍.md "wikilink")。然而這是有爭議的，有些研究人員認為牠們有接近親緣關係\[5\]\[6\]；其他研究人員則認為沒有，而提出原櫛龍的近親為：[冠長鼻龍](../Page/冠長鼻龍.md "wikilink")、[埃德蒙頓龍](../Page/埃德蒙頓龍.md "wikilink")、-{[格里芬龍](../Page/格里芬龍.md "wikilink")}-、[慈母龍](../Page/慈母龍.md "wikilink")\[7\]。原櫛龍屬於[鴨嘴龍亞科](../Page/鴨嘴龍亞科.md "wikilink")，意指牠們缺乏中空冠飾。

右方演化樹是根據[傑克·霍納](../Page/傑克·霍納.md "wikilink")（Jack
Horner）、[大衛·威顯穆沛](../Page/大衛·威顯穆沛.md "wikilink")（David
B. Weishampel）、凱薩琳·福斯特（Catherine Forster）等人在2004年所者的《*The
Dinosauria*》一書第二版\[8\]。但這只是眾多鴨嘴龍科演化樹的一個版本。

## 歷史

著名[古生物學家](../Page/古生物學家.md "wikilink")[巴納姆·布郎](../Page/巴納姆·布郎.md "wikilink")（Barnum
Brown）在1915年於[亞伯達省Steveville鎮附近的](../Page/亞伯達省.md "wikilink")[紅鹿河發現一個](../Page/紅鹿河.md "wikilink")[鴨嘴龍類頭顱骨](../Page/鴨嘴龍類.md "wikilink")（編號AMNH
5836）。他在1916年述敘這個標本，並以較早命名的櫛龍為對比，櫛龍擁有類似但較長的冠飾，他將這個標本命名為**原櫛龍屬**\[9\]。頭顱骨的鼻口部遭到損害，並認為以過長的模樣重建\[10\]，但在1924年發現的一個接近完整標本與頭顱骨，是由[威廉·帕克斯](../Page/威廉·帕克斯.md "wikilink")（William
Parks）所敘述，則發現原本標本是以真實的外型來重建\[11\]。原櫛龍的化石已被發現20到25個個體，包含7個連接者身體的頭顱骨\[12\]。

第二個種黑腳原櫛龍是根據編號MOR
454標本，是由另一著名古生物學家[傑克·霍納](../Page/傑克·霍納.md "wikilink")（Jack
Horner）命名。這個標本以及其他3到4個個體化石，是在[蒙大拿州](../Page/蒙大拿州.md "wikilink")[冰川縣附近發現的](../Page/冰川郡_\(蒙大拿州\).md "wikilink")\[13\]。這些標本來自於一個由原櫛龍化石所構成的[屍骨層](../Page/屍骨層.md "wikilink")，這顯示這群動物在一年中的至少某個部份是群居的。這個屍骨層反映了一群動物在乾旱時期聚集在水源附近\[14\]。雖然大部分的鴨嘴龍類物種已被確認，但黑腳原櫛龍是在最近幾年的研究才被認為是有效屬\[15\]。

## 古生態學

[Prosaurolophus_maximus1.JPG](https://zh.wikipedia.org/wiki/File:Prosaurolophus_maximus1.JPG "fig:Prosaurolophus_maximus1.JPG")
巨原櫛龍的化石發現於恐龍公園組，恐龍公園組是個遍佈[河流的](../Page/河流.md "wikilink")[氾濫平原](../Page/氾濫平原.md "wikilink")，隨者[西部內陸海道的往西海侵](../Page/西部內陸海道.md "wikilink")，恐龍公園組有更多[沼澤](../Page/沼澤.md "wikilink")，氣候更受[海洋影響](../Page/海洋.md "wikilink")\[16\]。當地的氣候較今日的[亞伯達省溫暖](../Page/亞伯達省.md "wikilink")，沒有霜害，但有乾季與濕季變化。[針葉林是當地的優勢大型植物](../Page/針葉林.md "wikilink")，[植被的下層則是](../Page/植被.md "wikilink")[蕨類](../Page/蕨類.md "wikilink")、[樹蕨](../Page/樹蕨.md "wikilink")、以及[被子植物](../Page/被子植物.md "wikilink")\[17\]。

巨原櫛龍的化石只發現於恐龍公園組的上層，上層較下層受到更多的海洋影響。原櫛龍是該地最常見的[鴨嘴龍亞科恐龍](../Page/鴨嘴龍亞科.md "wikilink")，生存年代約7600萬年前到7400萬年前\[18\]。恐龍公園組的其他恐龍則有：[角龍科的](../Page/角龍科.md "wikilink")[尖角龍](../Page/尖角龍.md "wikilink")、[戟龍](../Page/戟龍.md "wikilink")、[開角龍](../Page/開角龍.md "wikilink")；鴨嘴龍科的-{[格里芬龍](../Page/格里芬龍.md "wikilink")}-、[冠龍](../Page/冠龍.md "wikilink")、[賴氏龍](../Page/賴氏龍.md "wikilink")、[副櫛龍](../Page/副櫛龍.md "wikilink")；[暴龍科的](../Page/暴龍科.md "wikilink")[蛇髮女怪龍](../Page/蛇髮女怪龍.md "wikilink")；[甲龍下目的](../Page/甲龍下目.md "wikilink")[埃德蒙頓甲龍與](../Page/埃德蒙頓甲龍.md "wikilink")[包頭龍](../Page/包頭龍.md "wikilink")\[19\]。

黑腳原櫛龍的化石發現於[雙麥迪遜組](../Page/雙麥迪遜組.md "wikilink")，年代大約是相同時期，該地發現許多恐龍蛋巢、蛋、與幼體化石。雙麥迪遜組的其他恐龍有：[斯氏亞冠龍](../Page/亞冠龍.md "wikilink")、[慈母龍](../Page/慈母龍.md "wikilink")、[傷齒龍科的傷齒龍](../Page/傷齒龍科.md "wikilink")、暴龍科的[懼龍](../Page/懼龍.md "wikilink")、[近頜龍科的](../Page/近頜龍科.md "wikilink")[纖手龍](../Page/纖手龍.md "wikilink")、[馳龍科的](../Page/馳龍科.md "wikilink")[斑比盜龍與](../Page/斑比盜龍.md "wikilink")[蜥鳥盜龍](../Page/蜥鳥盜龍.md "wikilink")、愛德蒙頓龍與包頭龍、[稜齒龍類的](../Page/稜齒龍類.md "wikilink")[奔山龍](../Page/奔山龍.md "wikilink")、角龍科的[河神龍](../Page/河神龍.md "wikilink")、[短角龍](../Page/短角龍.md "wikilink")、[野牛龍與](../Page/野牛龍.md "wikilink")[卵圓戟龍](../Page/戟龍.md "wikilink")\[20\]。雙麥迪遜組離西部內陸海道更遠，地勢更高，氣候更乾燥，受更多陸地影響\[21\]。

## 古生物學

[Prosaurolophus_maximus.JPG](https://zh.wikipedia.org/wiki/File:Prosaurolophus_maximus.JPG "fig:Prosaurolophus_maximus.JPG")
如同其他[鴨嘴龍科](../Page/鴨嘴龍科.md "wikilink")，原櫛龍是種大型、[草食性恐龍](../Page/草食性.md "wikilink")，可採二足或四足方式行走，擁有複雜的頭顱骨，可做出類似咀嚼的磨輾動作。原櫛龍的牙齒是不斷生長、替換的，形成數百顆牙齒所構成的[齒系](../Page/齒系.md "wikilink")，但同一時間只會使用少部份牙齒。原櫛龍使用牠們的寬廣喙狀嘴來切斷植物，然後將植物置於頰部空間咀嚼。原櫛龍的進食範圍大約是地面以上4公尺的範圍內\[22\]。在2011年，科學家比較恐龍、現代[鳥類與](../Page/鳥類.md "wikilink")[爬行動物的](../Page/爬行動物.md "wikilink")[鞏膜環大小](../Page/鞏膜環.md "wikilink")，提出原櫛龍可能屬於無定時活躍性的動物，覓食、移動行為跟白天黑夜沒有正相關，只休息短暫時間\[23\]。

### 社會行為

原櫛龍的[屍骨層證明牠們至少一年的某些部份是群居生活的](../Page/屍骨層.md "wikilink")。\[24\]此外，原櫛龍可能有數種展示方式以達到溝通目的。骨質冠飾是可能的方式之一，冠飾內部可能擁有[鼻憩室](../Page/鼻憩室.md "wikilink")，與膨脹的[鼻囊軟組織](../Page/鼻囊.md "wikilink")，以及延長的洞可容納鼻孔。這些組織可當作視覺與聽覺辨認功能\[25\]。

## 參考資料

## 外部链接

  - [恐龙.NET](https://web.archive.org/web/20170527222928/http://dinosaur.net.cn/)——中国古生物门户网站。
  - [*Prosaurolophus*](https://web.archive.org/web/20070905202317/http://www.users.qwest.net/~jstweet1/hadrosaurinae.htm)
    at *Thescelosaurus*\! (scroll down to Hadrosaurinae)
  - [*Prosaurolophus*](https://web.archive.org/web/20080910114740/http://internt.nhm.ac.uk/jdsml/nature-online/dino-directory/detail.dsml?Genus=Prosaurolophus),
    Natural History Museum
  - [Hadrosaurinae](https://web.archive.org/web/20080927073559/http://www.palaeos.com/Vertebrates/Units/320Ornithischia/320.750.html#Hadrosaurinae)
    from Palaeos.com (technical)

[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")
[Category:櫛龍亞科](../Category/櫛龍亞科.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.
9.

10.

11.

12.
13.
14.

15.
16. Eberth, David A. 2005. "The geology", in *Dinosaur Provincial Park*,
    pp. 54–82.

17. Braman, Dennis R., and Koppelhus, Eva B. 2005. "Campanian
    palynomorphs", in *Dinosaur Provincial Park*, pp. 101–130.

18.

19. Weishampel, David B.; Barrett, Paul M.; Coria, Rodolfo A.; Le
    Loeuff, Jean; Xu Xing; Zhao Xijin; Sahni, Ashok; Gomani, Elizabeth,
    M.P.; and Noto, Christopher R. (2004). "Dinosaur Distribution", in
    *The Dinosauria* (2nd), pp. 517–606.

20.
21.
22.
23.

24.
25.