[228_Incident_h.jpg](https://zh.wikipedia.org/wiki/File:228_Incident_h.jpg "fig:228_Incident_h.jpg")
**原辰馬商會本町店鋪**，建於1929年，最初為原辰馬商會本町店鋪，該三層樓的建築曾被刊載於《[臺灣建築會誌](../Page/臺灣建築會誌.md "wikilink")》裡，之後被改為[專賣局的](../Page/專賣局.md "wikilink")**[台北](../Page/台北.md "wikilink")[本町分局](../Page/本町.md "wikilink")**。該局統籌負責[大台北地區的](../Page/大台北地區.md "wikilink")[鴉片](../Page/鴉片.md "wikilink")、[樟腦](../Page/樟腦.md "wikilink")、[酒類等專賣業務](../Page/酒類.md "wikilink")。亦為著名[二二八事件的民眾首波抗議地點](../Page/二二八事件.md "wikilink")。

## 建築使用沿革

建物本體始建於1929年（昭和4年）由日人河東富次起造興建，同年落成使用，並保存登記完成。1935年移轉予株式會社辰馬商會。1945年台灣進入[中華民國時期](../Page/中華民國.md "wikilink")，[台灣省行政長官公署循例](../Page/台灣省行政長官公署.md "wikilink")，仍由專賣局負責台北的菸酒專賣，其名則更改為台灣省專賣局台北分局。1951年收歸為國有財產，編入管理機關為[臺灣省公產管理處](../Page/臺灣省公產管理處.md "wikilink")。1968年3月1日移轉予[第一產物保險](../Page/第一產物保險.md "wikilink")，同年5月17日移轉予[彰化銀行臺北分行使用](../Page/彰化銀行.md "wikilink")。

## 二二八事件

1947年2月27日，台灣省專賣局台北分局專員[傅學通與葉得根](../Page/傅學通.md "wikilink")、盛鐵夫、鐘延洲、趙子健、劉超群等六人（均為外省籍）及四名警察（均為本省籍）以查緝員身分前往[臺北市](../Page/臺北市.md "wikilink")[大稻埕一帶取締私菸](../Page/大稻埕.md "wikilink")。公務執勤間，該查緝隊伍於臺北市[南京西路](../Page/南京西路.md "wikilink")[天馬茶房](../Page/天馬茶房.md "wikilink")（約位於今[延平北路附近](../Page/延平北路.md "wikilink")）前發現[寡婦林江邁](../Page/寡婦.md "wikilink")（40歲，育有一子一女）販賣私菸，查緝員將林婦所販賣的香菸及身上所有的錢財均沒收。林婦以家計困難，跪地求饒，希望查緝員至少歸還那些經過繳稅的公菸；不料查緝員卻堅持將她手上的錢財與所有菸品都沒收，連已繳稅的菸也一樣沒收，愈來愈多民眾聚集觀看，林婦被一名查緝員葉得根以[槍托擊傷頭部](../Page/槍托.md "wikilink")，當場血流滿面昏迷倒地。

民眾目睹此景後極為憤怒，乃將查緝員包圍，傅學通開槍警告，不幸誤殺當時在自宅樓下觀看熱鬧的市民陳文溪。激憤的群眾於當晚包圍[台北市警察局總局](../Page/台北市警察局.md "wikilink")，要求懲兇，但得不到滿意的答覆。

隔天，2月28日，臺北市各區[罷工](../Page/罷工.md "wikilink")、[罷市](../Page/罷市.md "wikilink")，不少市民前往肇事查緝員所任職的台灣省專賣局台北分局抗議。雖然該局早就預作準備，也配置多位警力。不過因為示威群眾眾多，不久後仍發生焚燒事件。該焚燒不但造成該局嚴重損失，也在兩造衝突下打傷三名職員（一說打死一人）。

該分局於事件後仍然負責台北地區的專賣，至1966年遷至中山北路；位於[重慶南路的本建築於](../Page/重慶南路.md "wikilink")1986年變成同為公營機構的[彰化銀行台北分行營業處所](../Page/彰化銀行.md "wikilink")。彰化銀行[民營化之後](../Page/民營化.md "wikilink")，台北分行仍於該址營業。

## 相關條目

  - [二二八事件](../Page/二二八事件.md "wikilink")
  - [天馬茶房](../Page/天馬茶房.md "wikilink")

## 外部連結

  - [文化部文化資產局 -
    原辰馬商會本町店鋪(彰化銀行臺北分行)](https://nchdb.boch.gov.tw/assets/advanceSearch/historicalBuilding/20121029000001)

[category:二二八事件](../Page/category:二二八事件.md "wikilink")

[Category:中正區 (臺北市)](../Category/中正區_\(臺北市\).md "wikilink")
[Category:台灣菸酒公司](../Category/台灣菸酒公司.md "wikilink")
[Category:臺北市歷史建築](../Category/臺北市歷史建築.md "wikilink")
[Category:1929年完工建築物](../Category/1929年完工建築物.md "wikilink")
[Category:台灣日治時期產業建築](../Category/台灣日治時期產業建築.md "wikilink")
[Category:彰化銀行](../Category/彰化銀行.md "wikilink")