**縣令**，尊稱**縣太爺**、**縣大老爺**，雅稱為**百里侯**、**邑侯**、**邑宰**。是中國古代[縣級行政區劃的最高官員名稱](../Page/縣.md "wikilink")，掌握所管轄區域的[行政](../Page/行政.md "wikilink")、[司法](../Page/司法.md "wikilink")、[審判](../Page/審判.md "wikilink")、[稅務](../Page/稅務.md "wikilink")、[兵役等大權](../Page/兵役.md "wikilink")。县令下设[县丞](../Page/县丞.md "wikilink")、[主簿](../Page/主簿.md "wikilink")、[縣尉](../Page/縣尉.md "wikilink")、[典史等](../Page/典史.md "wikilink")。

## 簡介

[戰國時](../Page/戰國.md "wikilink")[秦國](../Page/秦國.md "wikilink")[商鞅變法](../Page/商鞅.md "wikilink")，將許多小鄉合併為县，併县後的行政長官便稱為「令」。

最初，县令直接由[國君任免](../Page/國君.md "wikilink")，但在戰國末年，地方行政制度形成郡县兩級制，县隷屬於[郡](../Page/郡.md "wikilink")，县令就成了郡守的下屬。[秦](../Page/秦.md "wikilink")[汉时](../Page/汉.md "wikilink")，法令規定根据县的人口多寡，县官的职称也不同，一万户以上称**县令**，以下称**[县长](../Page/县长.md "wikilink")**。

[秦](../Page/秦.md "wikilink")[汉时](../Page/汉.md "wikilink")，县令[品秩八百](../Page/品秩.md "wikilink")[石至千石](../Page/石.md "wikilink")，考绩优良者可辟为[府椽](../Page/府椽.md "wikilink")、甚至可以升为[郡守](../Page/郡守.md "wikilink")。所以县令颇为人所重，朝廷也深重其选。[魏晋](../Page/魏晋.md "wikilink")[南北朝时期](../Page/南北朝.md "wikilink")，县令多以年老[胥吏或退役的下级](../Page/胥吏.md "wikilink")[军官充任](../Page/军官.md "wikilink")，品秩既低，所任之人又及其颟頇、贪暴，为[士人所不齿](../Page/士人.md "wikilink")，地方亦多不治。而到[宋代](../Page/宋代.md "wikilink")
，遣[京朝官分知县事](../Page/京朝官.md "wikilink")，简称**知縣**。[元朝時改為](../Page/元朝.md "wikilink")**縣尹**；到[明清時恢復](../Page/明清.md "wikilink")。

縣令古稱[百里侯](../Page/百里侯.md "wikilink")\[1\]，所谓“万事胚胎，皆在州县”\[2\]，“养鳏寡，恤孤穷。审察冤屈，躬亲狱讼，务知百姓之疾苦”\[3\]，其責任不可謂不大。宋朝以後的縣治卻成為“官冗于上，吏肆于下”\[4\]。[袁宏道在给友人](../Page/袁宏道.md "wikilink")[丘长孺的信中说](../Page/丘长孺.md "wikilink")：“弟作令，备极丑态，不可名状。大约遇上官则奴，候过客则妓，治钱谷则仓老人，……苦哉！毒哉！”\[5\]。

1912年[宣統退位](../Page/宣統退位.md "wikilink")，[中华民国政府成立](../Page/中华民国政府.md "wikilink")，初改县[知事](../Page/知事.md "wikilink")，后改[县长](../Page/县长.md "wikilink")，1949年後[中華人民共和國因襲之](../Page/中華人民共和國.md "wikilink")。

## 任职回避

明清两朝，规定县令不得在籍贯所在的本省任职，也不得在家乡500里之内的他省任职。近亲、近姻亲不得在同一省任职。

《[中华人民共和国公务员法](../Page/中华人民共和国公务员.md "wikilink")》、[中共中央组织部颁发的](../Page/中共中央组织部.md "wikilink")《党政领导干部选拔任用工作条例》第五十三条也规定了[职务回避与](../Page/职务回避.md "wikilink")[地域回避](../Page/地域回避.md "wikilink")，包括县长（[县级市市长](../Page/县级市.md "wikilink")）的任职回避。

## 参见

  - [縣官](../Page/縣官.md "wikilink")

## 参考文献

[縣令](../Category/縣令.md "wikilink")

1.  《漢書·百官公卿表》：“縣大率方百里，其民稠则减，稀则旷。”
2.  [汪辉祖](../Page/汪辉祖.md "wikilink")《[学治说赘](../Page/学治说赘.md "wikilink")》
3.  《旧唐书·卷四十四·职官三》
4.  《元史·卷八十五·百官一》
5.  《袁宏道集笺校·卷五》