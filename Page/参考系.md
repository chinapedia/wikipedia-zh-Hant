**参考系**（又称**参照系**、**参考坐标**），在[物理學中指用以測量並記錄位置](../Page/物理學.md "wikilink")、[定向以及其他物體屬性的](../Page/定向_\(几何\).md "wikilink")[坐標系](../Page/坐標系.md "wikilink")；或指與[觀測者的運動狀態相關的觀測參考系](../Page/觀測者.md "wikilink")；又或同指兩者。

## 各種參考系

參考系有許多種，所以在提到參考系時，常會在前面加上字詞指定是哪一種參考系，如[笛卡儿坐标系](../Page/笛卡儿坐标系.md "wikilink")。人們也會指定參考系的屬性：[旋轉參考系強調參考系的運動狀態](../Page/旋轉參考系.md "wikilink")，伽利略參考系強調系與系之間的[變換法](../Page/伽利略變換.md "wikilink")，而宏觀或微觀參考系則強調參考系的尺度大小。

在本條目中，「觀測者參考系」強調的是運動的狀態，而非某種特定坐標系的選擇，或是用於觀測的儀器。這種用法能夠研究觀測者的運動對坐標系的影響，無論觀測者使用的坐標系是哪一種。另一方面，當觀測者的運動狀態並非主要的針對點時，不同的「參考系」能夠利用不同系統的對稱性，來簡化計算的過程。更廣義的來說，許多物理學中的問題都用到[廣義坐標](../Page/廣義坐標.md "wikilink")、[實模態或](../Page/實模態.md "wikilink")[特徵向量](../Page/特徵向量.md "wikilink")，這些都和時間和空間沒有直接的關係。下文因此有必要分開敘述各種參考系，把觀測者參考系、坐標系及觀測儀器作為獨立的概念來看，如下：

  - 觀測者參考系（如[慣性參考系或](../Page/慣性參考系.md "wikilink")[非慣性參考系](../Page/非慣性參考系.md "wikilink")）是與運動狀態有關的物理概念。
  - 坐標系為一個數學概念，是用於描述物理問題一種語言。所以，在一個觀測者參考系中的觀測者可以選用各種各樣的坐標系（笛卡爾坐標系、極坐標系、曲線坐標系、廣義坐標系等等）來描述從該參考系中觀測到的現象。坐標系的改變並不影響觀測者本身的運動，也就不會影響這個觀測者的「觀測者參考系」<ref name=Johansson>

</ref>。某一些坐標系比另一些更適合描述特定的物理問題，在同一個觀測者參考系中，可以任選其一。

  - 對量度或觀測工具的選擇獨立於觀測者的運動狀態和其選用的坐標系。

### 坐標系

[Reference_frame_and_observer.svg](https://zh.wikipedia.org/wiki/File:Reference_frame_and_observer.svg "fig:Reference_frame_and_observer.svg")

「坐標」一詞的意義有時是非專業性的（特別在物理學中），然而它在數學中卻具有準確的意義。

數學中的坐標系是一個[幾何學和](../Page/幾何學.md "wikilink")[代數學用到的概念](../Page/代數學.md "wikilink")\[1\]\[2\]，一般作為[流形的一種特性](../Page/流形.md "wikilink")（如物理學中的[位形空間和](../Page/位形空間.md "wikilink")[相空間](../Page/相空間.md "wikilink")）<ref name=Hawking>According
to Hawking and Ellis: "A manifold is a space locally similar to
Euclidean space in that it can be covered by coordinate patches. This
structure allows differentiation to be defined, but does not distinguish
between different coordinate systems. Thus, the only concepts defined by
the manifold structure are those that are independent of the choice of a
coordinate system."  A mathematical definition is: ''A connected
[Hausdorff space](../Page/Hausdorff_space.md "wikilink") *M* is called
an *n*-dimensional manifold if each point of *M* is contained in an open
set that is homeomorphic to an open set in Euclidean *n*-dimensional
space.*</ref>\[3\]。一個點**r**在*n''維空間中的[坐標表達方式為](../Page/笛卡爾坐標系.md "wikilink")[*n*-元組](../Page/n_元組.md "wikilink")：\[4\]\[5\]

  -
    \(\mathbf{r} =[x^1,\ x^2,\ \dots\ ,  x^n] \ .\)

在廣義的[巴拿赫空間中](../Page/巴拿赫空間.md "wikilink")，這些數字能夠是諸如[傅立葉級數等函數展開式中的系數](../Page/傅立葉級數.md "wikilink")。在物理問題中，它們可以是[時空坐標或](../Page/時空.md "wikilink")[實模態振幅](../Page/實模態.md "wikilink")。當用在[機器人設計時](../Page/機器人.md "wikilink")，它們可以是相對旋轉的角度、直線平移或關節的變形度等\[6\]。在此我們假設這些坐標能夠以[笛卡爾坐標系中的一組函數表示](../Page/笛卡爾坐標系.md "wikilink")：

  -
    \(x^j = x^j (x,\  y,\  z,\  \dots)\ ,\)   
    \(j = 1, \ \dots \ , \ n\\)

其中*x*、*y*、*z*等等為該點的*n*個笛卡爾坐標數。給定這些函數，定義**坐標面**為以下關係：

  -
    \(x^j (x, y, z, \dots) =\) 常數    \(j = 1, \ \dots \ , \ n\  .\)

這些面的相交處定義為**坐標線**。在任何一點上，與相交的坐標線相切的所有切線組成一組在那一點的**基向量**：{**e**<sub>1</sub>,
**e**<sub>2</sub>, …, **e**<sub>n</sub>}。也就是：\[7\]

\[\mathbf{e}_i(\mathbf{r}) =\lim_{\epsilon \rightarrow 0} \frac{\mathbf{r}\left(x^1,\  \dots,\  x^i+\epsilon,\  \dots ,\  x^n \right) - \mathbf{r}\left(x^1,\  \dots,\  x^i,\  \dots ,\  x^n \right)}{\epsilon }\ ,\]

這能夠歸一化為單位長度。

坐標面、坐標線以及[基向量組成一個](../Page/基_\(線性代數\).md "wikilink")**坐標系**\[8\]。如果基向量在每一點上都兩兩正交，則該坐標系稱為[正交坐標系](../Page/正交坐標系.md "wikilink")。

坐標系中一個重要的方面在於其[度量](../Page/度量.md "wikilink")*g*<sub>ik</sub>，它在坐標系中以一組坐標表達[弧長](../Page/弧長.md "wikilink")*ds*：\[9\]

  -
    \((ds)^2 = g_{ik}\ dx^i\ dx^k \ ,\)

並求和所有重復的索引。

根據上文可以看出，參考系其實是一個[數學模型](../Page/模型論.md "wikilink")，屬於[公理系統的一部分](../Page/公理系統.md "wikilink")。參考系和物體運動實際上並沒有關係，但在加上時間作為又一個坐標後，它就能夠描述運動。所以，[洛倫茲變換及](../Page/洛倫茲變換.md "wikilink")[伽利略變換可以被視為](../Page/伽利略變換.md "wikilink")[坐標轉換](../Page/坐標系#坐標轉換.md "wikilink")。

### 觀測者參考系

[Minkowski_diagram_-_3_systems.svg](https://zh.wikipedia.org/wiki/File:Minkowski_diagram_-_3_systems.svg "fig:Minkowski_diagram_-_3_systems.svg")

**觀測者參考系**，或一般只稱為參考系，是與[觀測者以及其運動狀態相關的物理概念](../Page/觀測者.md "wikilink")。在本文中所指的，是只和運動狀態有關的參考系\[10\]。不過，人們對此觀點並无共識。在狹義相對論中，「觀測者」和「參考系」一般是有分別的。這一觀點認為，參考系是觀測者加上一個右手正交坐標系，該坐標系由一組正交的空間向量和與其垂直的一個時間向量組成\[11\]。本文並不使用這種狹義的觀點\[12\]\[13\]。在[廣義相對論中](../Page/廣義相對論.md "wikilink")，廣義坐標系的使用是很常見的（參見獨立球體外的引力場的[史瓦西解](../Page/卡爾·史瓦西.md "wikilink")\[14\]）。

觀測者參考系有兩種：[慣性與](../Page/慣性參考系.md "wikilink")[非慣性參考系](../Page/非慣性參考系.md "wikilink")。慣性參考系中的物理定律都處於最為簡單的形式。在[狹義相對論中](../Page/狹義相對論.md "wikilink")，這種參考系通過[洛倫茲變換相互變換](../Page/洛倫茲變換.md "wikilink")，其參數為[快度](../Page/快度.md "wikilink")。在牛頓力學中，慣性參考系定義為[牛頓第一定律必須成立的參考系](../Page/牛頓第一定律.md "wikilink")，也就是在這種參考系中的[自由粒子要麼以](../Page/自由粒子.md "wikilink")[直線恒速運行](../Page/直線.md "wikilink")，要麼保持靜止。它們之間以[伽利略變換互相轉換](../Page/伽利略變換.md "wikilink")。

與之相對的是非慣性參考系，當中的物理現象必須用到[假想力才能解釋](../Page/假想力.md "wikilink")。其中一個例子為位於地球表面的參考系。該參考系圍繞地球中心旋轉，因此造成一系列的假想力，如[科里奧利力](../Page/科里奧利力.md "wikilink")、[離心力和](../Page/離心力.md "wikilink")[引力](../Page/引力.md "wikilink")。（這些力，包括引力在內，都是在真正的慣性參考系——自由落體——中不存在的。）

### 量度儀器

參考系的其中一方面在於，加載與參考系上的[量度儀器有關的](../Page/度量衡學.md "wikilink")（如鐘或長桿等）到底具有甚麼樣的角色。本文不討論這一問題，而這是在[量子力學中牽涉到觀測者與測量之間關係的一個課題](../Page/量子測量.md "wikilink")。

在物理實驗中，實驗室量度儀器靜止位處的參考系稱為[實驗室參考系](../Page/實驗室參考系.md "wikilink")。某些實驗中的實驗室參考系是慣性參考系，而另一些則不是（如在地球表面的大部分實驗室都是非慣性參考系）。在粒子物理學中，一個常見的做法是把實驗室參考系中的能量與動量轉換到[質心系中](../Page/質心系.md "wikilink")，這樣可以簡化計算過程。

在[思想實驗中用到的鐘或長桿等觀測者的量度儀器](../Page/思想實驗.md "wikilink")，在實際實驗中是以非常複雜的儀器取代，從而間接地做出測量的。這些儀器用到[真空的屬性](../Page/真空.md "wikilink")，其[原子鐘根據](../Page/原子鐘.md "wikilink")[標準模型運作](../Page/標準模型.md "wikilink")，時間也必須根據[引力時間膨脹做出調整](../Page/引力時間膨脹.md "wikilink")\[15\]。

其實，[愛因斯坦認為鐘和長桿應該由更基礎的物體取代](../Page/愛因斯坦.md "wikilink")，如原子和分子等\[16\]。

## 慣性參考系

[Two_reference_frames.PNG](https://zh.wikipedia.org/wiki/File:Two_reference_frames.PNG "fig:Two_reference_frames.PNG")
兩輛車以不同的均速在路面行駛（見圖）。在某一時刻，它們間隔200米。前方的那輛車以每秒22米的速度行駛，隨後的那輛車以每秒30米的速度行駛。要計算第二輛車在多久後會趕上第一輛車，我們可以使用三個參考系的其中一個。

首先，我們可以從路邊觀測兩輛車。定義路邊的參考系為*S*：計時器在第二輛車經過路邊觀測者時開始，這時兩車相距*d* = 200
*m*。兩輛車都以均速運行，所以我們可以用以下的公式表述它們的位置：\(x_1(t)\)為第一輛車在時間*t*秒後的位置，而\(x_2(t)\)為第二輛車在時間*t*秒後的位置。

\[x_1(t)= d + v_1 t = 200\ + \ 22t\ ; \quad x_2(t)=  v_2 t = 30t\]

當時間為*t* = 0 *s*時，第一輛車位於200米處，而第二輛車位置為零，這符合實際情況。我們要設 \(x_1=x_2\)，並求\(t\)：

\[200 + 22 t = 30t \quad\]

\[8t = 200 \quad\]

\[t = 25 \quad \mathrm{s}\]

或者我們可以選擇位於第一輛車上的參考系''S'
''。在這個參考系裏，第一輛車是靜止的，而第二輛車跟隨在後，速度為<span style="font-family: Bookman Old Style; font-size:100%;font-style:italic;" >v<sub>2</sub>
− v<sub>1</sub></span> = 8 *m / s*。趕上前一輛車所需的時間為*d*
/(<span style="font-family: Bookman Old Style; font-size:100%;font-style:italic;" >
v<sub>2</sub> − v<sub>1</sub></span>) = 200 / 8
*s*，也就是25秒，同上。使用這個參考系比上一個參考系簡單得多。第三種做法是，取位於第二輛車上的參考系。這和以上的例子相似，但這次第二輛車為靜止，而第一輛車以每秒8米的速度向後退。

另外我們也可以使用旋轉或加速的參考系，但這樣會不必要地把問題複雜化了。值得注意的是，在任何參考系中作出的測量都可以換算成其他的參考系。

### 備註

以上的例子作了一些假設。比如牛頓使用的是世界時，因此兩個相互以高速均速運動的鐘的時間流逝率永遠是一樣的。他認為一個參考系中的時間流逝率應該和所有其他參考系中的一樣。也就是說，所有參考系的時間流逝率都和一個絕對的世界時相同，並不取決於參考系的位置和速率。愛因斯坦於1905年在他的[狹義相對論中延伸了這一概念](../Page/狹義相對論.md "wikilink")，並假設所有物理定律在所有的慣性參考系中都相同（包括光在真空中的速度），在這種原理下參考系之間的變換方法稱為[洛倫茲變換](../Page/洛倫茲變換.md "wikilink")。

另外，[慣性參考系的定義並不局限於三維](../Page/慣性參考系.md "wikilink")[歐幾里得空間](../Page/歐幾里得空間.md "wikilink")。牛頓所用的為簡單的歐幾里得空間，但[廣義相對論則用一種更為廣義的幾何](../Page/廣義相對論.md "wikilink")。就拿橢球體的幾何為例，其中的自由粒子定義為沿著[測地線均速移動或靜止不動](../Page/測地線.md "wikilink")。兩個自由粒子可以在表面上的同一點開始，以均速向不同方向運行。一段時間後，兩個粒子會在橢球體的另一邊會和相撞。粒子均以均速運行，符合沒有外在施力的定義；沒有加速度，也就符合了牛頓第一定律。因此這兩個粒子位於慣性參考系當中。它們最後的相撞是橢球體的幾何造成的。類似地，人們現在相信存在一種稱為[時空的四維幾何](../Page/時空.md "wikilink")，而這種幾何能夠解釋為甚麼兩個有質量的物體在沒有外力的情況下會互相靠近。時空的曲率取代了牛頓力學和狹義相對論中的引力。

## 非慣性參考系

非慣性參考系和慣性參考系之間的分別在於，在用到非慣性參考系時，必須用到假想力。

加速參考系一般以撇號標記，所有與其相關的變量都加以撇號：''x' *、*y' *、*a' ''等。

某慣性參考系和非慣性參考系之間的距離一般記為**R**。取同時存在於兩個參考系中的任意點，從慣性參考系原點指向該點的向量長度為**r**，而從非慣性參考系原點指向該點的向量長度為**r**'。以下的關係成立：

  -
    \(\mathbf r = \mathbf R + \mathbf r'\)

取一階及二階導數後得：

  -
    \(\mathbf v = \mathbf V + \mathbf v'\)
    \(\mathbf a = \mathbf A + \mathbf a'\)

其中**V**和**A**分別為相對非慣性參考系的速率和加速度，而**v**和**a**分別為相對慣性參考系的速率和加速度。

利用這些公式，我們能夠在兩種參考系之間變換。比如，[牛頓第二定律現在可以寫作](../Page/牛頓第二定律.md "wikilink")：

  -
    \(\mathbf F = m\mathbf a = m\mathbf A + m\mathbf a'\)

當從旋轉參考系等加速參考系來看時，慣性似乎表現為一種力（旋轉參考系中有[離心力及與垂直於物體移動路徑的](../Page/離心力.md "wikilink")[科里奧利力](../Page/科里奧利力.md "wikilink")）。

其中一種常見的加速參考系為同時旋轉並平移的參考系（如固定在搬動並運作中的播放機裏的光碟上的參考系）。如此的參考系有以下的方程：

  -
    \(\mathbf a = \mathbf a' + \dot{\boldsymbol\omega} \times \mathbf r' + 2\boldsymbol\omega \times \mathbf v' + \boldsymbol\omega \times (\boldsymbol\omega \times \mathbf r') + \mathbf A_0\)

或求相對加速參考系的物體加速度：

  -
    \(\mathbf a' = \mathbf a - \dot{\boldsymbol\omega} \times \mathbf r' - 2\boldsymbol\omega \times \mathbf v' - \boldsymbol\omega \times (\boldsymbol\omega \times \mathbf r') - \mathbf A_0\)

兩邊乘以質量*m*得

  -
    \(\mathbf F' = \mathbf F_\mathrm{physical} + \mathbf F'_\mathrm{Euler} + \mathbf F'_\mathrm{Coriolis} + \mathbf F'_\mathrm{centripetal} - m\mathbf A_0\)

其中

  -
    \(\mathbf F'_\mathrm{Euler} = -m\dot{\boldsymbol\omega} \times \mathbf r'\)（[歐拉力](../Page/歐拉力.md "wikilink")）

<!-- end list -->

  -
    \(\mathbf F'_\mathrm{Coriolis} = -2m\boldsymbol\omega \times \mathbf v'\)（[科里奧利力](../Page/科里奧利力.md "wikilink")）

<!-- end list -->

  -
    \(\mathbf F'_\mathrm{centrifugal} = -m\boldsymbol\omega \times (\boldsymbol\omega \times \mathbf r')=m(\omega^2 \mathbf r'- (\boldsymbol\omega \cdot \mathbf r')\boldsymbol\omega)\)（[離心力](../Page/離心力.md "wikilink")）

## 常用的特定參考系

  - [國際地表參考系統](../Page/國際地表參考系統.md "wikilink")
  - [國際天球參考系統](../Page/國際天球參考系統.md "wikilink")

## 參見

  - [分析力學](../Page/分析力學.md "wikilink")
  - [工程力學](../Page/工程力學.md "wikilink")
  - [笛卡爾坐標系](../Page/笛卡爾坐標系.md "wikilink")
  - [離心力](../Page/離心力.md "wikilink")
  - [向心力](../Page/向心力.md "wikilink")
  - [經典力學](../Page/經典力學.md "wikilink")
  - [科里奧利力](../Page/科里奧利力.md "wikilink")
  - [曲線坐標系](../Page/曲線坐標系.md "wikilink")
  - [圓柱坐標系](../Page/圓柱坐標系.md "wikilink")
  - [動力學](../Page/動力學.md "wikilink")
  - [假想力](../Page/假想力.md "wikilink")
  - [弗萊納公式](../Page/弗萊納公式.md "wikilink")
  - [伽利略不變性](../Page/伽利略不變性.md "wikilink")
  - [伽利略變換](../Page/伽利略變換.md "wikilink")
  - [廣義相對論](../Page/廣義相對論.md "wikilink")
  - [廣義坐標](../Page/廣義坐標.md "wikilink")
  - [廣義力](../Page/廣義力.md "wikilink")
  - [慣性參考系](../Page/慣性參考系.md "wikilink")
  - [運動學](../Page/運動學.md "wikilink")
  - [洛倫茲變換](../Page/洛倫茲變換.md "wikilink")
  - [馬赫原理](../Page/馬赫原理.md "wikilink")
  - [正交坐標系](../Page/正交坐標系.md "wikilink")
  - [相對性原理](../Page/相對性原理.md "wikilink")
  - [狹義相對論](../Page/狹義相對論.md "wikilink")
  - [球坐標系](../Page/球坐標系.md "wikilink")
  - [相對論](../Page/相對論.md "wikilink")
  - [圓環坐標系](../Page/圓環坐標系.md "wikilink")

## 注释

## 参考文献

## 外部連結

  - [Cultural Frame of
    Reference](http://jbd.sagepub.com/cgi/content/abstract/16/3/483)
  - [Philosophical Frame of
    Reference](http://links.jstor.org/sici?sici=0003-049X\(19621212\)106%3A6%3C467%3ATAOC%3E2.0.CO%3B2-1)

{{-}}

[參考系](../Category/參考系.md "wikilink")
[Category:相對論](../Category/相對論.md "wikilink")

1.
2.
3.
4.
5.  見[Encarta上的定義](http://encarta.msn.com/encyclopedia_761579532/Coordinate_System_\(mathematics\).html)。[存檔](http://www.webcitation.org/5kwcKb20f)
    2009-10-31.
6.
7.
8.
9.
10. See
11. .
12. For example, Møller states: "Instead of Cartesian coordinates we can
    obviously just as well employ general curvilinear coordinates for
    the fixation of points in physical space.…we shall now introduce
    general "curvilinear" coordinates *x*<sup>i</sup> in four-space…."
13.
14.
15.
16. See .