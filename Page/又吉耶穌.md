**又吉耶穌**（，），戶籍名及本名為**又吉光雄**，在[日本](../Page/日本.md "wikilink")[沖繩縣](../Page/沖繩縣.md "wikilink")[宜野灣市出生](../Page/宜野灣市.md "wikilink")，日本[政治家](../Page/政治家.md "wikilink")，自稱**唯一神[基督](../Page/基督.md "wikilink")[耶穌](../Page/耶穌.md "wikilink")**又吉光雄（日語：）。現時是[世界經濟共同體黨的代表](../Page/世界經濟共同體黨.md "wikilink")。父親為[又吉清真](../Page/又吉清真.md "wikilink")，母親為[又吉牛](../Page/又吉牛.md "wikilink")（）。

## 進入政壇前

1966年在沖繩縣的[中央大學的](../Page/日本中央大學.md "wikilink")[商學部畢業](../Page/商學部.md "wikilink")。之後，在一家汽車公司成為了職員、當過[補習教師以及在世界](../Page/補習教師.md "wikilink")[基督教會當過](../Page/基督教.md "wikilink")[牧師](../Page/牧師.md "wikilink")。1996年在[秘魯游擊隊佔領日本大使館事件對游擊隊和政府進行遊說](../Page/秘魯.md "wikilink")。1997年以耶穌的名義成立了[世界經濟同體黨](../Page/世界經濟同體黨.md "wikilink")，並且成為該黨的黨代表。

## 選舉

他的第一次參選在1997年[沖繩縣](../Page/沖繩縣.md "wikilink")[宜野灣市](../Page/宜野灣市.md "wikilink")，其後嘗試參選沖繩縣[眾議院選舉兩次沖繩縣](../Page/眾議院.md "wikilink")[知事](../Page/知事.md "wikilink")，名護市長選舉，還有一次在[宜野灣市的選舉](../Page/宜野灣市.md "wikilink")，在這幾次選舉當中，全部以最後一名落選，當中他在2002年沖繩縣知事選舉表明：「如果是次選舉落選的話，我會放棄沖繩移往東京。」之後，他將政黨的本部遷往東京，開始在東京參選的生涯。

## 東京選舉

在東京的三次選舉中，他的宣傳方法已經非常出位，儘管以前使用過這個方法，但是在[東京選舉更指名該人的名字](../Page/東京.md "wikilink")，在他的宣傳海報中，除了公開競爭對手的「罪狀」，還有「命令」他的對手以及支持對手候選人的投票者[切腹自盡和下](../Page/切腹.md "wikilink")「[地獄](../Page/地獄.md "wikilink")」接受永恆之火的燃燒，藉此換取本人成功當選。然而這種特別的宣傳使他在[日本網路上出現了爆紅現象](../Page/日本.md "wikilink")，出現了不少[戲仿的對象](../Page/戲仿.md "wikilink")，網路上有關他的[Flash動畫流傳不少](../Page/Flash.md "wikilink")。當中對象包括了前[日本首相](../Page/日本首相.md "wikilink")[小泉純一郎](../Page/小泉純一郎.md "wikilink")。2003年11月，首次在東京眾議院出選，但是大敗收場，目前在三次的選舉中，全部是最後一名而落選。

## 政綱

## 參選選舉和所得票數

| 日期          | 選舉         | 票數（得票率）       | 與第一名差距      |
| ----------- | ---------- | ------------- | ----------- |
| 1997年7月13日  | 宜野灣市長選舉    | 516票（1.85%）   | 18,082票 |-  |
| 1998年11月15日 | 沖繩縣知事選舉    | 2,649票（0.37%） | 372,184票 |- |
| 2002年2月3日   | 名護市長選舉     | 80票（0.25%）    | 20,276票 |-  |
| 2003年11月9日  | 眾議院選舉・東京1區 | 698票（0.29%）   | 104,525票 |- |
| 2005年9月11日  | 眾議院選舉・東京1區 | 1,557票（0.60%） | 148,337票    |

\[1\]

## 命令切腹主要的指名人士

  - [稻嶺直一](../Page/稻嶺直一.md "wikilink")
  - [與謝野馨](../Page/與謝野馨.md "wikilink")
  - [新垣繁信](../Page/新垣繁信.md "wikilink")
  - [堀江泰信](../Page/堀江泰信.md "wikilink")
  - [小泉純一郎](../Page/小泉純一郎.md "wikilink")

## 退出政壇及去世

2018年6月30日，以健康狀態惡化為由，於官網上宣布退出政治活動。\[2\]。

同年7月20日因左腎癌去世，享壽74歲。

## 参考文献

## 外部連結

  - [世界經濟共同體黨網站](http://www.matayoshi.org/)

  - [支持者網站](http://yes.kazamidori.net/)

  - [又吉耶穌宣傳海報（有英語翻譯）](https://web.archive.org/web/20051125135648/http://cgunson.com/extras/matayoshi.html)

[Category:日本基督徒](../Category/日本基督徒.md "wikilink")
[Category:沖繩縣出身人物](../Category/沖繩縣出身人物.md "wikilink")
[Category:網路迷因](../Category/網路迷因.md "wikilink")
[Category:琉球族政治人物](../Category/琉球族政治人物.md "wikilink")
[Category:日本中央大学校友](../Category/日本中央大学校友.md "wikilink")

1.  參考來源 [1](http://yes.kazamidori.net/election/okinawa.html)
    [2](http://yes.kazamidori.net/election/tokyo.html)
2.