[Avenida_da_Amizade_01.jpg](https://zh.wikipedia.org/wiki/File:Avenida_da_Amizade_01.jpg "fig:Avenida_da_Amizade_01.jpg")一段的友誼大馬路\]\]
[Macau_Avenida_da_Amizade_New_Yaohan.JPG](https://zh.wikipedia.org/wiki/File:Macau_Avenida_da_Amizade_New_Yaohan.JPG "fig:Macau_Avenida_da_Amizade_New_Yaohan.JPG")一段的友誼大馬路\]\]
**友誼大馬路**（）位於[澳門半島東南](../Page/澳門半島.md "wikilink")，東北起[新口岸水塘東北角](../Page/新口岸.md "wikilink")，西南至[亞馬喇前地](../Page/亞馬喇前地.md "wikilink")，長約2860米。每年[澳門格蘭披治大賽車舉行時](../Page/澳門格蘭披治大賽車.md "wikilink")，該路會成為用作[賽車跑道用途](../Page/東望洋跑道.md "wikilink")。

## 歷史

最初稱**薩拉沙博士大馬路**（參見葡萄牙獨裁者[薩拉查](../Page/薩拉查.md "wikilink")），東南為[海](../Page/海.md "wikilink")，北為[新口岸菜地](../Page/新口岸.md "wikilink")。

1974年，[葡萄牙](../Page/葡萄牙.md "wikilink")[四二五革命後不久](../Page/四二五革命.md "wikilink")，[澳門市政廳把該馬路改名為友誼大馬路](../Page/澳門市政廳.md "wikilink")。

1989年，[葡萄牙總統](../Page/葡萄牙總統.md "wikilink")[蘇亞利斯訪問澳門後](../Page/马里奥·苏亚雷斯.md "wikilink")，市政廳又把該馬路的西南端瀕臨[南灣的一段另外命名為](../Page/南灣_\(澳門\).md "wikilink")[蘇亞利斯博士大馬路](../Page/蘇亞利斯博士大馬路.md "wikilink")。

## 整治工程

由於城市發展需要，新口岸的道路使用量日益增加，[澳門政府於](../Page/澳門政府.md "wikilink")2005年開展[新口岸的交通整治工程](../Page/新口岸.md "wikilink")，工程範圍包括[亞馬喇前地](../Page/亞馬喇前地.md "wikilink")、[藝園](../Page/藝園.md "wikilink")、**友誼大馬路**、[城市日大馬路](../Page/城市日大馬路.md "wikilink")、[仙德麗街](../Page/仙德麗街.md "wikilink")、[何賢公園等](../Page/何賢公園.md "wikilink")。整治工程的另一個目的是為了避免每年一度的[澳門格蘭披治大賽車所引致的塞車問題](../Page/澳門格蘭披治大賽車.md "wikilink")。
2006年整治工程完成後，**友誼大馬路**的行車方向有所更改。\[1\]

## 沿途重要地點

**由東至西**

  - [新口岸水塘](../Page/新口岸水塘.md "wikilink")
  - 澳門格蘭披治大賽車大樓（控制中心）
  - [外港客運碼頭](../Page/外港客運碼頭.md "wikilink")（港澳碼頭）
  - ~~[皇宮娛樂場](../Page/皇宮娛樂場.md "wikilink")~~（已停業並被移往筷子基北灣停泊）
  - [海立方](../Page/海立方.md "wikilink")（前[新八佰伴所在地](../Page/新八佰伴.md "wikilink")）
  - [澳門保安部隊高等學校分教處](../Page/澳門保安部隊高等學校.md "wikilink")（前身為治安警察局出入境事務廳大樓）
  - 澳門國際中心
  - [澳門漁人碼頭](../Page/澳門漁人碼頭.md "wikilink")
  - [金龍酒店及金龍娛樂場](../Page/金龍酒店.md "wikilink")
  - [金沙娛樂場](../Page/澳門金沙.md "wikilink")
  - [金蓮花廣場](../Page/金蓮花廣場.md "wikilink")
  - [澳門華都酒店及銀河華都娛樂場](../Page/澳門華都酒店.md "wikilink")
  - [澳門金麗華酒店](../Page/澳門金麗華酒店.md "wikilink")（前[文華東方酒店](../Page/澳門文華東方酒店.md "wikilink")）
  - [澳門世界貿易中心](../Page/澳門世界貿易中心.md "wikilink")
  - 新華大廈
  - [何賢公園](../Page/何賢公園.md "wikilink")
  - [藝園](../Page/藝園.md "wikilink")
  - [宋玉生廣場](../Page/宋玉生廣場.md "wikilink")
  - [皇朝廣場](../Page/皇朝廣場.md "wikilink")
  - [澳門置地廣場](../Page/澳門置地廣場.md "wikilink")
  - [澳門星際酒店](../Page/澳門星際酒店.md "wikilink")
  - [總統酒店](../Page/澳門總統酒店.md "wikilink")
  - [金碧娛樂場](../Page/金碧娛樂場.md "wikilink")
  - [葡京彎](../Page/葡京彎.md "wikilink")
  - [永利澳門](../Page/永利澳門.md "wikilink")（位於仙德麗街）
  - [葡京酒店](../Page/葡京酒店.md "wikilink")[葡京娛樂場](../Page/葡京娛樂場.md "wikilink")

## 參考文獻

<div class="references-small">

<references />

</div>

## 相關條目

  - [東望洋跑道](../Page/東望洋跑道.md "wikilink")
  - [澳門格蘭披治大賽車](../Page/澳門格蘭披治大賽車.md "wikilink")
  - [1998年澳門東湖海鮮酒家爆炸事件](../Page/1998年澳門東湖海鮮酒家爆炸事件.md "wikilink")
  - [蘇亞利斯博士大馬路](../Page/蘇亞利斯博士大馬路.md "wikilink")

[Category:澳門街道](../Category/澳門街道.md "wikilink")

1.  澳門日報 2006年9月2日，友誼馬路周一大改道