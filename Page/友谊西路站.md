**友谊西路站**位于[上海](../Page/上海.md "wikilink")[宝山区](../Page/宝山区.md "wikilink")[杨行镇](../Page/杨行镇.md "wikilink")[蕰川路](../Page/蕰川路.md "wikilink")[友谊西路](../Page/友谊路_\(上海\).md "wikilink")，为[上海轨道交通1号线](../Page/上海轨道交通1号线.md "wikilink")「北北延伸」的高架[侧式车站](../Page/侧式站台.md "wikilink")，于2007年12月29日启用。

## 公交换乘

172、552、钱泰线、宝山7路

## 车站出口

  - 1号口：蕰川路东侧，湄星路南
  - 2号口：蕰川路西侧，湄星路南

## 邻近车站

[Category:上海市宝山区地铁车站](../Category/上海市宝山区地铁车站.md "wikilink")
[Category:2007年启用的铁路车站](../Category/2007年启用的铁路车站.md "wikilink")
[Category:以街道命名的中國鐵路車站](../Category/以街道命名的中國鐵路車站.md "wikilink")