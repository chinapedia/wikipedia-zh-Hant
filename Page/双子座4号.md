**双子座4号**（）是[双子座计划中的第二次载人飞行任务](../Page/双子座计划.md "wikilink")，在这次任务中，宇航员[爱德华·怀特执行了美国航天史上第一次](../Page/爱德华·怀特.md "wikilink")（人类历史上第二次）[太空行走](../Page/舱外活动.md "wikilink")。

## 任务成员

  - **[詹姆斯·麦克迪维特](../Page/詹姆斯·麦克迪维特.md "wikilink")**（，曾执行双子座4号以及[阿波罗9号任务](../Page/阿波罗9号.md "wikilink")），指令飞行员
  - **[爱德华·怀特](../Page/爱德华·怀特.md "wikilink")**（，曾执行双子座4号以及[阿波罗1号任务](../Page/阿波罗1号.md "wikilink")），飞行员

### 替补成员

<small>替补成员同样接受任务训练，在主力成员因各种原因无法执行任务时接替。</small>

  - **[弗兰克·博尔曼](../Page/弗兰克·博尔曼.md "wikilink")**（，曾执行[双子座7号以及](../Page/双子座7号.md "wikilink")[阿波罗8号任务](../Page/阿波罗8号.md "wikilink")），指令飞行员
  - **[詹姆斯·洛威尔](../Page/吉姆·洛威尔.md "wikilink")**（，曾执行[双子座7号](../Page/双子座7号.md "wikilink")、[12号](../Page/双子座12号.md "wikilink")、[阿波罗8号以及](../Page/阿波罗8号.md "wikilink")[阿波罗13号任务](../Page/阿波罗13号.md "wikilink")），飞行员

[Category:双子座计划任务](../Category/双子座计划任务.md "wikilink")
[Category:1965年佛罗里达州](../Category/1965年佛罗里达州.md "wikilink")
[Category:1965年美国事件](../Category/1965年美国事件.md "wikilink")
[Category:1965年科學](../Category/1965年科學.md "wikilink")
[Category:1965年6月](../Category/1965年6月.md "wikilink")