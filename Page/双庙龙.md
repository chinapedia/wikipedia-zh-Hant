**雙廟龍**（屬名：*Shuangmiaosaurus*）是[鳥臀目](../Page/鳥臀目.md "wikilink")[禽龍類](../Page/禽龍類.md "wikilink")[恐龍的一屬](../Page/恐龍.md "wikilink")，是種[草食性恐龍](../Page/草食性.md "wikilink")，生存於晚[白堊紀](../Page/白堊紀.md "wikilink")（[阿爾比階](../Page/阿爾比階.md "wikilink")）的[中國](../Page/中國.md "wikilink")，約1億年前\[1\]。

[模式種是](../Page/模式種.md "wikilink")**吉氏雙廟龍**（*S.
gilmorei*），是由[尤海魯](../Page/尤海魯.md "wikilink")、[季強等中國科學家在](../Page/季強.md "wikilink")2003年敘述、命名。屬名是以化石發現處的[遼寧省](../Page/遼寧省.md "wikilink")[北票市下府鄉的雙廟村為名](../Page/北票市.md "wikilink")；種名則是以[美國古生物學家](../Page/美國.md "wikilink")[查爾斯·惠特尼·吉爾摩](../Page/查爾斯·惠特尼·吉爾摩.md "wikilink")（Charles
Whitney Gilmore）為名，他曾在中國多年進行恐龍研究\[2\]。

[正模標本](../Page/正模標本.md "wikilink")（編號LPM
0165）是一個部分左[前上頜骨](../Page/前上頜骨.md "wikilink")、左[上頜骨](../Page/上頜骨.md "wikilink")、[淚骨](../Page/淚骨.md "wikilink")、與[齒骨](../Page/齒骨.md "wikilink")。化石發現於[孫子家灣組](../Page/孫子家灣組.md "wikilink")，最初被估計約[白堊紀晚期的](../Page/白堊紀.md "wikilink")[森諾曼階到](../Page/森諾曼階.md "wikilink")[土侖階交界](../Page/土侖階.md "wikilink")，目前被認為地質年代應該較古老。

雙廟龍是種大型[鳥腳類恐龍](../Page/鳥腳類.md "wikilink")。在2010年，[葛瑞格利·保羅](../Page/葛瑞格利·保羅.md "wikilink")（Gregory
S. Paul）估計其身長約7.5公尺，體重約2.5公噸\[3\]。

尤海魯等人起初將雙廟龍歸為[鴨嘴龍超科的基礎物種](../Page/鴨嘴龍超科.md "wikilink")，是[鴨嘴龍科的近親](../Page/鴨嘴龍科.md "wikilink")。在2004年，[劍橋大學的](../Page/劍橋大學.md "wikilink")[大衛·諾曼](../Page/大衛·諾曼.md "wikilink")（David
B. Norman）認為雙廟龍是種基礎禽龍類，不屬於鴨嘴龍超科\[4\]。

## 參考資料

## 外部連結

  - [禽龍類](https://web.archive.org/web/20090425151715/http://www.thescelosaurus.com/iguanodontia.htm)

[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:禽龍類](../Category/禽龍類.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")

1.  Carpenter, K. and Ishida, Y. (2010). "[Early and “Middle” Cretaceous
    Iguanodonts in Time and
    Space.](http://www.ucm.es/info/estratig/JIG/vol_content/vol_36_2/36_2_145_164_Carpenter.pdf)"
    *Journal of Iberian Geology*, **36** (2): 145-164
2.  H. You, Q. Ji, J. Li and Y. Li, 2003, "A new hadrosauroid dinosaur
    from the mid-Cretaceous of Liaoning, China", *Acta Geologica Sinica*
    **77**(2): 148-154
3.  Paul, G.S., 2010, *The Princeton Field Guide to Dinosaurs*,
    Princeton University Press p. 294
4.  Norman, D.B. (2004). "Basal Iguanodontia." In D. B. Weishampel, P.
    Dodson, and H. Osmólska (eds.), *The Dinosauria* (2nd edition).
    University of California Press, Berkeley 413-437