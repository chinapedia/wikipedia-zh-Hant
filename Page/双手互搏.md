**雙手互搏**（亦稱**左右互搏**）為[金庸](../Page/金庸.md "wikilink")[武俠小說](../Page/武俠小說.md "wikilink")《[射鵰英雄傳](../Page/射鵰英雄傳.md "wikilink")》中「[老頑童](../Page/老頑童.md "wikilink")」[周伯通](../Page/周伯通.md "wikilink")（也就是《[神雕俠侶](../Page/神雕俠侶.md "wikilink")》裡的「[中頑童](../Page/中頑童.md "wikilink")」）被「[東邪](../Page/東邪.md "wikilink")」[黃藥師困於](../Page/黃藥師.md "wikilink")[桃花島時所創](../Page/桃花島.md "wikilink")。

## 概述

據《[神鵰俠侶](../Page/神鵰俠侶.md "wikilink")》原文：
其實這左右互搏之技，關鍵訣竅全在「分心二用」四字。凡是聰明智慧的人，心思繁複，
一件事沒想完，第二件事又湧上心頭。例如[三國時](../Page/三國.md "wikilink")[曹子建](../Page/曹子建.md "wikilink")（即[曹植](../Page/曹植.md "wikilink")）[七步成詩](../Page/七步成詩.md "wikilink")，[五代間](../Page/五代.md "wikilink")[劉鄩用兵](../Page/劉鄩.md "wikilink")\[1\]，一步百計，這等人要他學那左右互搏的功夫，便是要殺他的頭也學不會的。

關鍵訣竅全在「分心二用」，因此要習此門功夫，須做到心無雜念。根據[周伯通的說法](../Page/周伯通.md "wikilink")，若能左手畫方，右手畫圓，方能修習此法。《[射鵰英雄傳](../Page/射鵰英雄傳.md "wikilink")》中世上僅有[周伯通及](../Page/周伯通.md "wikilink")[郭靖能使用](../Page/郭靖.md "wikilink")，而在《[神鵰俠侶](../Page/神鵰俠侶.md "wikilink")》中[小龍女以養蜂術與](../Page/小龍女.md "wikilink")[周伯通交換雙手互搏之後便能一人使出](../Page/周伯通.md "wikilink")「[玉女素心劍法](../Page/玉女素心劍法.md "wikilink")」，成為第三個懂得雙手互搏的人。周伯通，郭靖與小龍女皆是心思純樸之人，心無雜念，尤其小龍女自幼便學習寡欲，學習雙手互搏並非難事。

在[倚天屠龍記中](../Page/倚天屠龍記.md "wikilink")「崑崙三聖」[何足道亦曾以左手凌厲攻敵](../Page/何足道.md "wikilink")、右手舒緩撫琴。儘管不及雙手分使兩般武功，卻已是[射鵰三部曲中極少數能分心二用的人](../Page/射鵰三部曲.md "wikilink")。

另外，在《[碧血劍](../Page/碧血劍.md "wikilink")》中，主角[袁承志為圓溫青青一時口舌之快](../Page/袁承志.md "wikilink")，於擊敗仙都派兩大弟子後，現學現賣，當場使出需兩人才有辦法施展的「兩儀劍法」，書中有云：「只見他雙劍舞了開來，左攻右守，右擊左拒，一招一式，果然與兩儀劍法毫無二致。劍招繁複，變化多端，洞玄和閔子華適才分別使出，人人都已親見，此時見他一人雙劍竟囊括仙都派二大弟子的劍招，盡皆相顧駭然。
」，可見[袁承志亦是少數能分心二用之人](../Page/袁承志.md "wikilink")。

## 參見

  - [周伯通](../Page/周伯通.md "wikilink")
  - [郭靖](../Page/郭靖.md "wikilink")
  - [小龍女](../Page/小龍女.md "wikilink")
  - 《[射鵰英雄傳](../Page/射鵰英雄傳.md "wikilink")》
  - 《[神鵰俠侶](../Page/神鵰俠侶.md "wikilink")》

## 註解

<div class="references-small">

<references>

\[2\]

</references>

</div>

[Category:金庸筆下武功](../Category/金庸筆下武功.md "wikilink")

1.
2.  [《舊版神鵰俠侶．第六十九回　日夜跟蹤》](http://www.cognitiohk.edu.hk/chilit/Friction/Contemporary%20Friction/JinYong/JiuBanZhenDiaoXiaLu/JiuBanZhenDiaoXiaLu069.htm)作「劉鄖」（yún），五代名將只有[劉鄩](../Page/劉鄩.md "wikilink")（xún，一步百計者），無[劉鄖](../Page/劉鄖.md "wikilink")。