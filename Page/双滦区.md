**双滦区**是[河北省](../Page/河北省.md "wikilink")[承德市下辖的一个市辖区](../Page/承德.md "wikilink")。以汉族为多，另有满族、蒙古族、回族等少数民族。

## 气候

为暖温带阔叶林向[温带草原过度地带](../Page/温带草原.md "wikilink")，属暖温和中温带半湿润大陆季风型气候，年平均气温8.9℃，无霜期168天。

## 行政区划

下辖3个[街道办事处](../Page/街道办事处.md "wikilink")、4个[镇](../Page/行政建制镇.md "wikilink")、1个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")、1个[民族乡](../Page/民族乡.md "wikilink")：

。

## 官方网站

  - [双滦区政府信息公开平台](http://218.11.132.47:81/)
  - [承德市双滦区商务之窗](https://web.archive.org/web/20100523064032/http://shuangluanqu.mofcom.gov.cn/)

[双滦区](../Page/category:双滦区.md "wikilink")
[区](../Page/category:承德区县.md "wikilink")

[承德](../Category/河北市辖区.md "wikilink")