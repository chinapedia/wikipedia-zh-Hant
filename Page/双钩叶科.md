**双钩叶科**（）共有3[属](../Page/属.md "wikilink")3[种](../Page/种.md "wikilink")（3个单种属），皆生长在[非洲西部](../Page/非洲.md "wikilink")[热带地区的雨林中](../Page/热带.md "wikilink")。

本科[植物为](../Page/植物.md "wikilink")[藤本植物](../Page/藤本植物.md "wikilink")；单[叶互生](../Page/叶.md "wikilink")，无托叶，革质，叶子中脉长出两个类似钩子的器官，借以攀爬；[花中等](../Page/花.md "wikilink")，[花瓣](../Page/花瓣.md "wikilink")5瓣；[果实为](../Page/果实.md "wikilink")[蒴果](../Page/蒴果.md "wikilink")；本科最為人知的成員是[盾籽穗葉藤](../Page/盾籽穗葉藤.md "wikilink")，僅有該種是[食肉植物](../Page/食肉植物.md "wikilink")，該種的嫩芽和幼枝可以形成捕捉器官，分泌酸性黏液。本科另外2種：[道氏盾籽藤](../Page/道氏盾籽藤.md "wikilink")、[托倫雙鉤葉](../Page/托倫雙鉤葉.md "wikilink")，它們的知名度皆遠遠不如盾籽穗葉藤。

1981年的[克朗奎斯特分类法将其列在](../Page/克朗奎斯特分类法.md "wikilink")[堇菜目](../Page/堇菜目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG分类法认为应该放在](../Page/APG分类法.md "wikilink")[石竹目中](../Page/石竹目.md "wikilink")，2003年经过修订的[APG
II分类法维持原分类](../Page/APG_II分类法.md "wikilink")。

## 種系發生學

<small>參考[1](https://archive.is/20070611/http://www-organik.chemie.uni-wuerzburg.de/ak_bring/research-garden.html)[2](http://www.mobot.org/MOBOT/Research/APweb/orders/Introstudent.html)[3](http://www.mobot.org/MOBOT/Research/APweb/orders/caryophyllalesweb.htm)</small>

## 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](https://web.archive.org/web/20070103200438/http://delta-intkey.com/angio/)中的[Dioncophyllaceae
    双钩叶科](http://delta-intkey.com/angio/www/dioncoph.htm)
  - [NCBI中的双钩叶科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=63072&lvl=3&p=mapview&p=has_linkout&p=blast_url&p=genome_blast&lin=f&keep=1&srchmode=1&unlock)

[\*](../Category/双钩叶科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")