《**反俄狄浦斯：資本主義與精神分裂**》（）是[法國](../Page/法國.md "wikilink")[哲学家](../Page/哲学家.md "wikilink")[吉尔·德勒兹](../Page/吉尔·德勒兹.md "wikilink")（）與[心理学家](../Page/心理学家.md "wikilink")[菲力斯·伽塔利](../Page/伽塔利.md "wikilink")（）于1972年合作出版的一本書。

该书对于70年代学术界对[弗洛伊德及](../Page/西格蒙德·弗洛伊德.md "wikilink")[拉康的](../Page/拉康.md "wikilink")[精神分析学理论具有明确的](../Page/精神分析学.md "wikilink")「[解构](../Page/解构.md "wikilink")」意味。指出弗洛伊德的分析体系是建立在男性话语权力结构基础上的二元对立体系，不利于多元文化社会的建构与研究。并因此将[社会科学与](../Page/社会科学.md "wikilink")[分析心理学分别推向了](../Page/分析心理学.md "wikilink")「后结构主义」及「精神分裂分析学」的新高度。现在这个概念，一般被用作与经典精神分析的区别研究以及后结构與[后现代主义的个性文化及心理研究](../Page/后现代主义.md "wikilink")。

[de:Schizoanalyse](../Page/de:Schizoanalyse.md "wikilink")
[es:Esquizoanálisis](../Page/es:Esquizoanálisis.md "wikilink")
[pt:Esquizoanálise](../Page/pt:Esquizoanálise.md "wikilink")

[Category:哲学书籍](../Category/哲学书籍.md "wikilink")
[Category:後現代主義](../Category/後現代主義.md "wikilink")
[Category:心理學](../Category/心理學.md "wikilink")
[Category:1972年出版](../Category/1972年出版.md "wikilink")