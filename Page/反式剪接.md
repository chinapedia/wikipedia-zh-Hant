**反式剪接**指的是两条不同的[mRNA的](../Page/mRNA.md "wikilink")[外显子连接到一起](../Page/外显子.md "wikilink")。与正常的顺式[剪接不同](../Page/剪接.md "wikilink")，这里的两段外显子是来自不同的[RNA的](../Page/RNA.md "wikilink")，但却可能来自同一[基因](../Page/基因.md "wikilink")。“经典”反式剪接见于[锥虫和](../Page/锥虫.md "wikilink")[线虫](../Page/线虫.md "wikilink")，近期在[人类身上也发现了反式剪接](../Page/人类.md "wikilink")。

## 锥虫和线虫的“经典”反式剪接

[动质体目动物](../Page/动质体目.md "wikilink")，如引起非洲[昏睡症](../Page/昏睡症.md "wikilink")，[那加那病和](../Page/那加那病.md "wikilink")[Chagas病的](../Page/Chagas病.md "wikilink")[病原体](../Page/病原体.md "wikilink")，特别是布氏布氏锥虫（*Trypanosoma
brucei
brucei*），狭义上是反式剪接的经典模型生物。这些生物在[转录过程中](../Page/转录.md "wikilink")，和[细菌一样从单一一个](../Page/细菌.md "wikilink")[基因中产生一个](../Page/基因.md "wikilink")[多顺反子的抄本](../Page/顺反子.md "wikilink")，之后经过反式剪接加工等步骤被释放。这步反应发生在[剪接体上](../Page/剪接体.md "wikilink")，U1-[snRNA会被所谓的反式剪接引导序列](../Page/snRNA.md "wikilink")“Spliced-Leader”
(SL) snRNA替换。和U1 [snRNA不同](../Page/snRNA.md "wikilink")，SL
RNA会在剪接过程中被消耗，其5'端有一迷你外显子，它会和一段多顺反子的抄本中的[外显子组成成熟mRNA](../Page/外显子.md "wikilink")，(见旁边的插图)。SL
RNA的迷你外显子含有ATG-启动子，它是完整mRNA拥有正确的开放阅读框（open reading frame）的前提。
锥虫中，除了反式剪接外，还有一经典的顺式剪接，它发生于为多聚A多聚酶编码的基因上。锥虫也有U1-snRNA，只是为数极少，(最近其序列已被分析)。

狭义上，反式剪接在[线虫上也会发生](../Page/线虫.md "wikilink")，但其它生物都没见有反式剪接的存在。

## 人类的反式剪接

在人体中也有反式剪接被发现的报道。但与锥虫不同，在人类的反式剪接中不存在SL-snRNA，也就是并非狭义上的反式剪接。这些报道中指的情况，是同一基因转录得出的两条序列相同的前体mRNA交错加工(也属于反式剪接，因为根据概念，两条独立的RNA被交错剪接)。这造成后来成熟RNA中一个外显子的重复出现。但这种人类反式剪接极罕见(目前有两例)，但却引起了热烈的评论，可不可以用这种方法对[遗传病做出治疗](../Page/遗传病.md "wikilink")（科学家已经尝试对[tau蛋白编码基因使用该方法](../Page/tau蛋白.md "wikilink")，tau蛋白是[痴呆如](../Page/痴呆.md "wikilink")[阿茲海默症中扮演重要角色](../Page/阿茲海默症.md "wikilink"))。

[蓝藻中也见反式剪接](../Page/蓝藻.md "wikilink")。

## 外部連結

  - [寄生蟲學-protozoa(原蟲)-Trypanosoma
    spp.(錐形蟲)](http://smallcollation.blogspot.com/2013/02/protozoa-trypanosoma-spp.html)
  - [反式剪接反应](http://www.biox.cn/content/20050707/24976.htm)

[category:遗传学](../Page/category:遗传学.md "wikilink")