[W87_MX_Missile_schematic.jpg](https://zh.wikipedia.org/wiki/File:W87_MX_Missile_schematic.jpg "fig:W87_MX_Missile_schematic.jpg")的多彈頭核子彈\]\]
[Peacekeeper_in_silo_1987.jpg](https://zh.wikipedia.org/wiki/File:Peacekeeper_in_silo_1987.jpg "fig:Peacekeeper_in_silo_1987.jpg")
**《反彈道飛彈條約》**（**Anti-Ballistic Missile
Treaty**），全称**《美利坚合众国与苏维埃社会主义共和国联盟关于限制反弹道导弹系统的条约》**，是由[苏联與](../Page/苏联.md "wikilink")[美国在](../Page/美国.md "wikilink")1972年簽署限制**反彈道飛彈**部署數量的条约。這項條約附属在第一階段**限制戰略武器談判**（**Strategic
Arms Limitation Talks**， **SALT**）協議之下。

條約中的主要規定包括：

  - [蘇聯與](../Page/蘇聯.md "wikilink")[美國可以各自選擇兩處分散的地點](../Page/美國.md "wikilink")，各部署100枚反彈道飛彈防禦重要目標。這項規定使得雙方無法利用部署地點或者是數量形成一個完整的全國性彈道飛彈防禦網。

<!-- end list -->

  - 簽署雙方必須提供對方國家自由檢查相關設施與部署的權力。也就是說美蘇雙方可以經由[間諜衛星蒐集與判斷對方遵循這項條約的狀態與行動](../Page/間諜衛星.md "wikilink")。

<!-- end list -->

  - 美蘇將共同成立一個協調會來處理與本條約有關的規定與進行的細節。

1974年蘇聯與美國同意調整雙方對於系統部署的數量，其中部署地點降為一處，飛彈數量降為100枚。蘇聯選擇[莫斯科做為部署地點](../Page/莫斯科.md "wikilink")，美國選擇[北達科塔州的](../Page/北達科塔州.md "wikilink")[義勇兵彈道飛彈基地部署他們的系統](../Page/義勇兵彈道飛彈.md "wikilink")。到了1976年，美國將該處反彈道飛彈基地關閉，蘇聯仍繼續使用。

2001年12月13日，美国宣布將退出美苏1972年签署的《反弹道导弹条约》。根据该条约规定，布什的这项决定将在6个月后正式生效。这是美国在现代史上首次退出一项主要国际协议。

## 反彈道飛彈的需求

[二次世界大戰期間](../Page/二次世界大戰.md "wikilink")[德國首先利用](../Page/德國.md "wikilink")[彈道飛彈攻擊地面目標的構想](../Page/彈道飛彈.md "wikilink")，受到包括[蘇聯與](../Page/蘇聯.md "wikilink")[美國在內的國家的重視](../Page/美國.md "wikilink")。當[彈道飛彈的射程足以到達另外一個大陸之後](../Page/彈道飛彈.md "wikilink")，[洲際彈道飛彈取代](../Page/洲際彈道飛彈.md "wikilink")[轟炸機成為](../Page/轟炸機.md "wikilink")[核子武器時代的](../Page/核子武器.md "wikilink")[戰略力量投射主流](../Page/戰略.md "wikilink")。

即使擁有配備核子彈頭的洲際彈道飛彈並不能抵擋來襲的飛彈，即使在毫無預警下的發射也未必能夠將對方的[戰略武器消滅殆盡](../Page/戰略武器.md "wikilink")。因此發展防禦彈道飛彈的系統就成為許多國家研究的方向，其中又以蘇聯與美國成功的發展出相關的技術與系統。

## 反彈道飛彈技術

[PAC-3_Interception.jpg](https://zh.wikipedia.org/wiki/File:PAC-3_Interception.jpg "fig:PAC-3_Interception.jpg")

要形成實用化的反彈道飛彈系統，需要兩個條件。第一是必須擁有[早期預警系統](../Page/早期預警.md "wikilink")，以提供[彈道飛彈來襲的預警和進入接戰程序](../Page/彈道飛彈.md "wikilink")。這些預警的系統包含在地面上或者是特殊船隻的大型[雷達](../Page/雷達.md "wikilink")，以及在太空中的預警[衛星](../Page/衛星.md "wikilink")。[蘇聯與](../Page/蘇聯.md "wikilink")[美國都大量興建](../Page/美國.md "wikilink")[早期預警系統](../Page/早期預警.md "wikilink")，並且與中央作戰管制中心以及戰略飛彈指揮系統連線。

第二個條件就是負責攔截彈道飛彈的武器。早期的發展只能在彈道飛彈重返[大氣層的最後階段進行攔截](../Page/大氣層.md "wikilink")，攔截的武器以[飛彈為主](../Page/飛彈.md "wikilink")。到了1980年代美國[雷根政府提出的](../Page/雷根.md "wikilink")[星戰計畫還包括在太空中利用各類飛彈或者是能量武器進行攔截](../Page/星戰計畫.md "wikilink")。現在的研究與發展還將發射階段的攔截方式也包含在內。彈道飛彈攔截最大的難題之一是目標速度非常的高，加上[核子彈頭不需要在低空才引爆](../Page/核子彈.md "wikilink")。負責攔截的反彈道飛彈的反應時間短，必須以很高的[加速率在核子彈頭引爆前摧毀目標](../Page/加速率.md "wikilink")。可是早期的飛彈對這種高速目標的攔截精確度無法使用傳統[彈頭](../Page/彈頭.md "wikilink")，這個缺點導致反彈道飛彈也得要使用核子彈頭擴大殺傷範圍以達到目的。

這項缺點以及在大氣層內引爆可能導致的災難性影響，讓反彈道飛彈成為不太受歡迎的攔截系統，在美國遭到計劃部署早期預警或者是飛彈系統地區附近居民的強烈反對。這個問題直到1992年[波斯灣戰爭期間](../Page/波斯灣戰爭.md "wikilink")，[愛國者飛彈的實戰經驗證實技術上以傳統彈頭攔截彈道飛彈是可行的](../Page/愛國者飛彈.md "wikilink")，才算是讓反彈道飛彈的發展進入另外一個階段。

## 参考文献

## 外部連結

  - [《美苏关于限制反弹道导弹系统条约》全文](http://www.people.com.cn/GB/junshi/61/20010809/531407.html)
  - [US Announcement of withdrawal
    (2001)](https://web.archive.org/web/20020223065455/http://www.state.gov/t/ac/rls/fs/2001/6848.htm)
  - [Global Security
    Institute](https://web.archive.org/web/20070529212930/http://www.gsinstitute.org/index.html)

{{-}}

[Category:1972年蘇聯](../Category/1972年蘇聯.md "wikilink")
[Category:1972年美国](../Category/1972年美国.md "wikilink")
[Category:飛彈防禦](../Category/飛彈防禦.md "wikilink")
[Category:冷战](../Category/冷战.md "wikilink")
[Category:1991年后的美国历史](../Category/1991年后的美国历史.md "wikilink")
[Category:美國條約](../Category/美國條約.md "wikilink")
[Category:蘇聯條約](../Category/蘇聯條約.md "wikilink")
[Category:美蘇關係](../Category/美蘇關係.md "wikilink")