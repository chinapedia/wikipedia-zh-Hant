在[粒子物理學裡](../Page/粒子物理學.md "wikilink")，**反物質**（英语：**antimatter**）是[反粒子概念的延伸](../Page/反粒子.md "wikilink")，反物質是由反粒子構成的，如同普通物質是由普通粒子所构成的。例如一顆[反質子和一顆反電子](../Page/反質子.md "wikilink")〈[正電子](../Page/正電子.md "wikilink")〉能形成一個反氫原子，如同[電子和](../Page/電子.md "wikilink")[質子形成一般物質的](../Page/質子.md "wikilink")[氫原子](../Page/氫原子.md "wikilink")。此外，物質與反物質的結合，會如同粒子與反粒子結合一般，導致兩者[湮滅](../Page/湮滅.md "wikilink")，且因而釋放出高能[光子](../Page/光子.md "wikilink")（[伽瑪射線](../Page/伽瑪射線.md "wikilink")）或是其他能量較低的正反[粒子對](../Page/粒子.md "wikilink")。正反[物質湮滅所造成的粒子](../Page/物质.md "wikilink")，賦予的[動能等同於原始正反物質對的動能](../Page/動能.md "wikilink")，加上原物質[靜止質量與生成粒子](../Page/靜止質量.md "wikilink")[靜質量的差](../Page/靜質量.md "wikilink")，後者通常佔大部分。（[愛因斯坦](../Page/阿尔伯特·爱因斯坦.md "wikilink")[相對論指出](../Page/相对论.md "wikilink")，[質量與](../Page/质量.md "wikilink")[能量是](../Page/能量.md "wikilink")[等價的](../Page/等价关系.md "wikilink")。）

反物質無法在[自然界找到](../Page/自然.md "wikilink")，除非是在稍縱即逝的少量存在（例如因[放射衰變或](../Page/放射性衰變.md "wikilink")[宇宙射線等現象](../Page/宇宙射線.md "wikilink")）。這是由於反物質若非存在於像物理實驗室的人工環境下，則無可避免地隨即與自然界的物質發生碰觸並湮滅。反粒子和一些穩定的反物質（例如[反氫](../Page/反氫.md "wikilink")）可以人工製造出極少量，但卻不足以達到可對這些物質驗證其理論性的程度。

在[科學或](../Page/科學.md "wikilink")[科幻方面](../Page/科幻.md "wikilink")，焦點環繞在為何所見的宇宙幾乎充滿了物質、是否有其他地方則是幾乎充滿了反物質以及是否能夠駕馭反物質，在現今可見的宇宙範圍中明顯的[正反物質不對稱性成了](../Page/CP破壞.md "wikilink")[物理的最大難題之一](../Page/物理.md "wikilink")。許多可能的物理過程都是在探究[重子時所發現](../Page/重子.md "wikilink")。

## 歷史

[缩略图](https://zh.wikipedia.org/wiki/File:Antimatter.jpg "fig:缩略图")
1927年12月，英国物理学家[保罗·狄拉克提出了](../Page/保罗·狄拉克.md "wikilink")[電子的](../Page/電子.md "wikilink")[相對論方程式](../Page/相對論.md "wikilink")，即[狄拉克方程式](../Page/狄拉克方程式.md "wikilink")\[1\]。有趣的是，等式中發現除了一般正能量之外的負能量結果。這顯示出一個問題，當電子趨向於朝著最低可能的能階躍遷時；負無限大的能量是毫無意義的。但為了要彌補這條件，狄拉克提出[真空狀態中是充滿了負能量電子的](../Page/真空.md "wikilink")「海」，稱作[狄拉克之海](../Page/狄拉克之海.md "wikilink")。任何真實的電子因此會填補這些海中具有正能量的部分。

衍伸這個想法，狄拉克發現海中的這些「洞」則具有正電荷。起初他認為這是[質子](../Page/質子.md "wikilink")，但[赫爾曼·外爾指出這些洞應該是具有和電子相同的質量](../Page/赫爾曼·外爾.md "wikilink")。1932年由美国物理学家[卡尔·安德森在實驗中證實了](../Page/卡尔·安德森.md "wikilink")[正电子的存在](../Page/正电子.md "wikilink")。在此期間，反物質有時也常被稱作「**反地物質**」。雖然狄拉克自己沒有使用反物質這個術語，但是後來的科學家將[反質子等粒子稱呼為反物質](../Page/反質子.md "wikilink")\[2\]。完整的反物質[元素週期表由](../Page/元素週期表.md "wikilink")[查爾斯·珍妮特](../Page/查爾斯·珍妮特.md "wikilink")（Charles
Janet）於1929年完成\[3\]。

## 性質

[反质子](../Page/反质子.md "wikilink")、[反中子和](../Page/反中子.md "wikilink")[反电子如果像](../Page/反电子.md "wikilink")[质子](../Page/质子.md "wikilink")、[中子](../Page/中子.md "wikilink")、[电子那样结合起来就形成了](../Page/电子.md "wikilink")[反原子](../Page/反原子.md "wikilink")。

反物质和物质一旦相遇，就相互吸引、碰撞並完全转化为光并释放出的巨大的能量，这个过程叫做**[湮灭](../Page/湮灭.md "wikilink")**。湮灭过程会释放出正、反物质中蕴涵的所有静质量能，根据爱因斯坦著名的质能关系式──[E=mc²](../Page/E=mc².md "wikilink")，一种在科学界受到普遍认同的理论认为，[宇宙大爆炸早期曾产生了数量相当的物质和反物质](../Page/宇宙大爆炸.md "wikilink")，随后发生的物质和反物质的湮灭消耗掉了绝大部分的正、反物质，遗留下的少部分正物质构成了现如今的物质世界。理論上[宇宙大爆炸時所產生的粒子與反粒子應該數量相同](../Page/宇宙大爆炸.md "wikilink")，但是为什么現今所遺留下來的絕大多數都是正粒子，這即所谓的“正反物质对称性破壞”（對稱破缺），雖然在幾個粒子对撞试验中，都發現了正粒子與反粒子的衰變略有不同，即所謂的電荷宇稱不守恆（[CP破壞](../Page/CP破壞.md "wikilink")），但在數量上仍不足以解釋為何現今反物質消失的問題，這在[粒子物理学上仍是一大未解决的问题](../Page/粒子物理学.md "wikilink")。

尽管在人们已经在实验室中制造出了为数众多的反原子，然而目前在自然界中尚没有发现反物质。一种观点认为即使自然界中存在反物质，它也很快会和正物质发生湮灭。

## 時光倒流

[費曼曾經研究](../Page/理查德·費曼.md "wikilink")[狄拉克方程而發現](../Page/狄拉克方程.md "wikilink")，假如將時間方向逆轉同時將電子電荷顛倒，則方程依然成立。即是說，一粒時光順流的[電子](../Page/電子.md "wikilink")，其實等同一粒[時光倒流的](../Page/時光倒流.md "wikilink")[反電子](../Page/反電子.md "wikilink")。因此所謂的反物質，其實就是倒著時間方向的正物質\[4\]，是為[CPT對稱](../Page/CPT對稱.md "wikilink")。

縱使如此，反物質的運動，並不違反因果律。當[電磁波的](../Page/電磁波.md "wikilink")[超前波和](../Page/超前波.md "wikilink")[延遲波相加時](../Page/延遲波.md "wikilink")，違反因果律的項次會互相抵消。如果沒有了反物質，因果律反而會瓦解\[5\]。

費曼和他的老師[惠勒進一步假設](../Page/約翰·惠勒.md "wikilink")，整個宇宙其實也許只是相同一粒電子在時間軸中從[大霹靂到宇宙末日之間來來回回地走動](../Page/大霹靂.md "wikilink")，這也是為何所有電子彼此之間毫無分別\[6\]。

## 應用

[缩略图](https://zh.wikipedia.org/wiki/File:Antimatter_Rocket.jpg "fig:缩略图")
因為物質與反物質的[湮滅時質量可完全轉換成能量](../Page/湮滅.md "wikilink")，帶來最大的能源效率，且單位產量是[核能的千百倍或常規燃料的億兆倍](../Page/核能.md "wikilink")，所以一直有人研究其作為新能源的可行性，主要用於很難補給燃料的[航天用](../Page/航天.md "wikilink")，甚至作為[反物質武器](../Page/反物質武器.md "wikilink")。但是由於目前人為製造反物質的方式，是由加速粒子打擊固定靶產生反粒子，再減速合成的。此過程所需要的能量遠大於湮滅作用所放出的能量，且生成反物質的速率極低，因此尚不具有經濟價值。此外，反物质与物质相遇会发生湮灭，保存上也是一大問題。

## 參考資料

## 延伸閱讀

  -
## 外部連結

  - [Freeview Video 'Antimatter' by the Vega Science Trust and the
    BBC/OU](http://www.vega.org.uk/video/programme/14)
  - [CERN Webcasts (RealPlayer
    required)](https://web.archive.org/web/20000620014355/http://livefromcern.web.cern.ch/livefromcern/antimatter/webcast/AM-webcast06.html)
  - [What is
    Antimatter?](https://web.archive.org/web/20051028075601/http://www.positron.edu.au/faq.html)
    (from the Frequently Asked Questions at the Center for
    Antimatter-Matter Studies)
  - [FAQ from
    CERN](https://web.archive.org/web/20140327204326/http://public.web.cern.ch/public/en/Spotlight/SpotlightAandD-en.html)
    with lots of information about antimatter aimed at the general
    reader, posted in response to antimatter's fictional portrayal in
    Angels & Demons
  - [What is direct
    CP-violation?](https://web.archive.org/web/20140503090147/http://www2.slac.stanford.edu/tip/special/cp.htm)''
  - [Animated illustration of antihydrogen production at
    CERN](http://www.exploratorium.edu/origins/cern/tools/animation.html)
    from the Exploratorium.

[F](../Category/量子场论.md "wikilink") [\*](../Category/反物質.md "wikilink")
[Category:物理学中未解决的问题](../Category/物理学中未解决的问题.md "wikilink")
[Category:虚构力量源](../Category/虚构力量源.md "wikilink")

1.
2.
3.
4.  加來道雄 《電影中不可能的物理學》,世茂出版社 (2009): P. 345 - 346.
5.  加來道雄 《電影中不可能的物理學》,世茂出版社 (2009): P. 348.
6.  加來道雄 《電影中不可能的物理學》,世茂出版社 (2009): P. 347.