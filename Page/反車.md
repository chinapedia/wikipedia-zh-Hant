**反車**是[日本將棋的棋子之一](../Page/日本將棋.md "wikilink")。有在[中將棋](../Page/中將棋.md "wikilink")、[大將棋](../Page/大將棋.md "wikilink")、[大大將棋](../Page/大大將棋.md "wikilink")、[天竺大將棋](../Page/天竺大將棋.md "wikilink")、[摩訶大大將棋](../Page/摩訶大大將棋.md "wikilink")、[泰將棋和](../Page/泰將棋.md "wikilink")[大局將棋出現](../Page/大局將棋.md "wikilink")。

在[平安大將棋之中](../Page/平安大將棋.md "wikilink")，名為「奔車」（）的棋子之存在已經受到確認，在大將棋之後「奔車」則被記載為「反車」。

在有反車存在的日本將棋變體當中，反車的開局位置必定位於[香車的前方](../Page/香車_\(日本將棋\).md "wikilink")。

## 奔車在平安大將棋

可升級成[金將](../Page/金將.md "wikilink")（）。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
<th><p>升級棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>奔車</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>│</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>│</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p><strong>反</strong></p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>│</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>│</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
</tbody>
</table></td>
<td><p>可循縱向自由行走。不得越棋。</p></td>
<td><p>金將</p></td>
</tr>
</tbody>
</table>

## 反車在中將棋、大將棋、天竺大將棋、大局將棋

在中將棋簡稱為**反**。可升級成[鯨鯢](../Page/鯨鯢.md "wikilink")（）。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
<th><p>升級棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>反車</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>│</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p> </p></td>
<td><p>│</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p><strong>反</strong></p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p> </p></td>
<td><p>│</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p>│</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table></td>
<td><p>可循縱向自由行走。不得越棋。</p></td>
<td><p>鯨鯢</p></td>
</tr>
</tbody>
</table>

## 反車在大大將棋

不得升級。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
<th><p>升級棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>反車</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>│</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p> </p></td>
<td><p>│</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p><strong>反</strong></p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p> </p></td>
<td><p>│</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p>│</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table></td>
<td><p>可循縱向自由行走。不得越棋。</p></td>
<td><p>-</p></td>
</tr>
</tbody>
</table>

## 反車在摩訶大大將棋、泰將棋

可升級成[金將](../Page/金將.md "wikilink")（）。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
<th><p>升級棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>反車</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>│</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>│</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p><strong>反</strong></p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>│</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>│</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
</tbody>
</table></td>
<td><p>可循縱向自由行走。不得越棋。</p></td>
<td><p>金將</p></td>
</tr>
</tbody>
</table>

## 參見

  - [日本將棋](../Page/日本將棋.md "wikilink")
  - [日本將棋變體](../Page/日本將棋變體.md "wikilink")
  - [中將棋](../Page/中將棋.md "wikilink")
  - [大將棋](../Page/大將棋.md "wikilink")
  - [大大將棋](../Page/大大將棋.md "wikilink")
  - [天竺大將棋](../Page/天竺大將棋.md "wikilink")
  - [摩訶大大將棋](../Page/摩訶大大將棋.md "wikilink")
  - [泰將棋](../Page/泰將棋.md "wikilink")
  - [大局將棋](../Page/大局將棋.md "wikilink")
  - [平安大將棋](../Page/平安大將棋.md "wikilink")

[Category:日本將棋棋子](../Category/日本將棋棋子.md "wikilink")
[Category:中將棋棋子](../Category/中將棋棋子.md "wikilink")
[Category:大將棋棋子](../Category/大將棋棋子.md "wikilink")
[Category:大大將棋棋子](../Category/大大將棋棋子.md "wikilink")
[Category:天竺大將棋棋子](../Category/天竺大將棋棋子.md "wikilink")
[Category:摩訶大大將棋棋子](../Category/摩訶大大將棋棋子.md "wikilink")
[Category:泰將棋棋子](../Category/泰將棋棋子.md "wikilink")
[Category:大局將棋棋子](../Category/大局將棋棋子.md "wikilink")
[Category:平安大將棋棋子](../Category/平安大將棋棋子.md "wikilink")