**叛國**，在[法律上](../Page/法律.md "wikilink")，指一個人對其所屬於的[國家不](../Page/國家.md "wikilink")[忠誠](../Page/忠誠.md "wikilink")；違背其效忠[宣言或與其國家的](../Page/宣言.md "wikilink")[敵人合作的人會被稱為](../Page/敵人.md "wikilink")**叛徒**。《法律詞典》（）（1983年版）對叛國的定義是：「一個[公民協助外國](../Page/公民.md "wikilink")[政府推翻或嚴重侵害他所屬的國家](../Page/政府.md "wikilink")，或慫恿外國對他所屬的國家[宣戰](../Page/宣戰.md "wikilink")。」

站在本国执政者（尤屬長期執政的[獨裁者](../Page/獨裁者.md "wikilink")）的立场上来看，叛國的相似詞是[革命](../Page/革命.md "wikilink")，因為革命幾乎會利用國內外一切可用勢力來從事推翻原政府政權的行為，叛國與革命的認定則由觀點立場的不同而有差異，進行革命的人通常會被原始政府視為叛國或密謀顛覆等，被捕後通常會以前述之罪名予以處決、監禁或是驅逐出境（有外國勢力介入者的可能性較大）。但是也有观点认为二者可从受支持的普遍程度上来划分。

在一些奉行[民族主義的](../Page/民族主義.md "wikilink")[專制政權](../Page/專制政權.md "wikilink")，「叛國」經常被用作消弭[改革派或異見分子的藉口](../Page/改革派.md "wikilink")，被指控叛國的人未必會被提上法庭上，根據客觀的案情證據接受公平審議。這些國家會將不同程度的[異見人士動輒標籤為叛國](../Page/異見人士.md "wikilink")、「賣國賊」，而被國家[秘密警察](../Page/秘密警察.md "wikilink")[私下緝拿](../Page/私刑.md "wikilink")，或動用輿論壓力以達至[政治迫害](../Page/政治迫害.md "wikilink")、噤聲等鞏固政權的效果。

## 中國

[古中國叛國的刑罰非常重](../Page/古中國.md "wikilink")。[車裂](../Page/車裂.md "wikilink")、[凌遲等極刑都是用來處死叛逆者的](../Page/凌遲.md "wikilink")（一般的形式是[斬首或](../Page/斬首.md "wikilink")[絞刑](../Page/絞刑.md "wikilink")），而且很有可能牽連親人，導致[滅族](../Page/滅族.md "wikilink")；也是[十惡之一](../Page/十惡_\(法律\).md "wikilink")。

### 元朝

元朝正式将「凌迟」明定为刑法的一种死刑，並视为对汉人宣扬威权的工具，元朝法律和南宋末年一样，将凌迟予以合法化。主要用来惩罚三大类的犯罪：

  - 谋逆君主之罪：重大暴动、叛国、谋取皇位。
  - [逆伦之罪](../Page/逆伦.md "wikilink")：子女谋害父母、弟幼谋害兄长，妻子谋害丈夫、奴僕谋害主人。
  - 凶残与不人道之罪：活生生砍断他人四肢（施以巫术）；杀害同一家族三人以上；結為[恐怖組織](../Page/恐怖組織.md "wikilink")。

### 明朝

  - 根據[明大诰](../Page/明大诰.md "wikilink")、[大明律](../Page/大明律.md "wikilink")，
  - 凡**謀反**者，處以[磔刑](../Page/磔刑.md "wikilink")（凌遲），如：明英宗手下的太监[曹吉祥](../Page/曹吉祥.md "wikilink")1561年因率军谋反而获此刑。
  - 凡**皇帝怀疑其谋反**者，如[袁崇煥](../Page/袁崇煥.md "wikilink")：崇禎年間知名将领，因崇祯皇帝怀疑其谋反，以“通虏谋叛”被凌迟处死，據說當時京中百姓爭啖其肉。

### 清朝

根據[大清律例](../Page/大清律例.md "wikilink")，舉凡**叛國**、**謀反**、**製造內亂**、**製造內亂起事者之子\[1\]**、**謀殺皇帝**、**奸淫多名幼女**、**搶劫**、**殺人**、**革命\[2\]**之徒，最高刑罰為凌迟处死，最低刑罰是處以監禁直到[清朝滅亡才得以釋放](../Page/清朝滅亡.md "wikilink")。

### 中華民國

《[中華民國刑法](../Page/中華民國刑法.md "wikilink")》中有制定**內亂罪**（[第100條至第](../Page/中華民國刑法第一百條.md "wikilink")102條）、**外患罪**（第103條至第115條）\[3\]以及**妨害國交罪**（第116條至第119條）\[4\]，其中[刑法第一百條已修改為限以暴力脅迫者有罪](../Page/中華民國刑法第一百條.md "wikilink")\[5\]。

### 中華人民共和國

[中華人民共和國初期](../Page/中華人民共和國.md "wikilink"),有幾項罪名均與叛國罪相類，如[反革命](../Page/反革命.md "wikilink")（1997年廢除）、[背叛国家罪](../Page/背叛国家罪.md "wikilink")、[分裂国家罪](../Page/分裂国家罪.md "wikilink")、[顛覆國家政權罪](../Page/顛覆國家政權罪.md "wikilink")、[煽動顛覆國家政權罪](../Page/煽動顛覆國家政權罪.md "wikilink")、[间谍罪等](../Page/间谍罪.md "wikilink")。這些罪行的刑罰由[有期徒刑至](../Page/有期徒刑.md "wikilink")[死刑不等](../Page/死刑.md "wikilink")。

## 英國

[英國在](../Page/英國.md "wikilink")1350年開始把叛國和其他輕叛逆罪分開處理。「輕叛逆罪」指某人[謀殺一個在法律上地位比該人為高的人](../Page/謀殺.md "wikilink")，例如[妻子殺死](../Page/妻子.md "wikilink")[丈夫](../Page/丈夫.md "wikilink")、[奴隸殺害](../Page/奴隸.md "wikilink")[主人等](../Page/主人.md "wikilink")。叛國罪則指任何能威脅國家穩定或存續的行為，如殺害國王、製作[偽幣以資助反對國家的戰爭等](../Page/偽幣.md "wikilink")。一項在18世紀的條文界定四種叛國的類型：

1.  促成或想像[國王](../Page/國王.md "wikilink")（或[女王](../Page/女王.md "wikilink")）、-{[王后](../Page/王后.md "wikilink")}-（或[王夫](../Page/王夫.md "wikilink")）或[王儲的死亡](../Page/王儲.md "wikilink")；
2.  對國王的隨從、最年長而未嫁的[公主](../Page/公主.md "wikilink")、[儲妃等人使用暴力](../Page/儲妃.md "wikilink")；
3.  發動反對國王的戰爭；
4.  在國境之內對國王的敵人表示擁護，或在任何地方對國王的敵人提供任何援助。

叛國罪的處罰常常是長而且殘忍的死亡（[掛拉分](../Page/掛拉分.md "wikilink")）。[英國政府曾以該法律對付](../Page/英國政府.md "wikilink")[異議分子至](../Page/異議分子.md "wikilink")19世紀。

1945年英國以叛國罪将“哈哈勋爵”[威廉·乔伊斯判处](../Page/威廉·乔伊斯_\(播音员\).md "wikilink")[死刑](../Page/死刑.md "wikilink")，並於1946年把該犯人[绞死](../Page/絞刑.md "wikilink")，这也是英国历史上最後一次以叛国罪处死犯人。此後[英國理論上仍可判處叛國者死刑](../Page/英國.md "wikilink")，直至1998年修例後把有關最高刑罰改為[終身監禁](../Page/終身監禁.md "wikilink")。

## 法國

在[法國大革命時](../Page/法國大革命.md "wikilink")，很多人動輒被定叛國罪而送上[斷頭台](../Page/斷頭台.md "wikilink")，包括[路易十六在內](../Page/路易十六.md "wikilink")。但至20世紀，叛國罪的死刑以[槍決而非](../Page/槍決.md "wikilink")[斷頭台執行](../Page/斷頭台.md "wikilink")。[維奇法國的領導人](../Page/維奇法國.md "wikilink")[貝當](../Page/菲利普·貝當.md "wikilink")[將軍最初便是判](../Page/將軍.md "wikilink")[槍決的](../Page/槍決.md "wikilink")，後來新政府的[戴高樂表示因他年老](../Page/夏爾·戴高樂.md "wikilink")（貝當被審判時已屆90歲高齡）而改判[終身監禁](../Page/終身監禁.md "wikilink")。

法國在1981年廢除一般[死刑時](../Page/死刑.md "wikilink")，並沒有廢除叛國罪的死刑（叛國罪的死囚是被槍決的，而不是以斷頭台[斬首](../Page/斬首.md "wikilink")）。

## 美國

為了避免[英國的相關法律被濫用](../Page/英國.md "wikilink")，[美國是在](../Page/美國.md "wikilink")[憲法中定義叛國罪的相關條文](../Page/美國憲法.md "wikilink")。它的憲法第三條定義叛國為「發動反對美國的戰爭，或擁護美國的敵人、並給予他們任何形式的援助」。另外，法例亦規定需要至少兩名[證人目擊被告在進行叛國行為](../Page/證人.md "wikilink")，且兩人所目擊的行為必須相同，或被告在[法庭認罪方能判被告罪名成立](../Page/法庭.md "wikilink")。

另一方面，[美國國會是可以定義一些類似叛國的罪名](../Page/美國國會.md "wikilink")（如[暴動](../Page/暴動.md "wikilink")、藏有對國家不利的武器等）。這些罪名毋需至少兩名證人以令被告罪名成立，而罪名包括的範圍亦比憲法第三條的叛國廣得多。一些[間諜便被定](../Page/間諜.md "wikilink")「間諜活動」罪而非叛國罪。

根據美國法律，叛國的刑罰可以高至[死刑](../Page/死刑.md "wikilink")，但也有較輕的罰則，如「判5年或以上[有期徒刑](../Page/有期徒刑.md "wikilink")、罰款至少10,000[美元](../Page/美元.md "wikilink")，且終身不得在[美國擔任任何](../Page/美國.md "wikilink")[公職](../Page/公職.md "wikilink")」。

在[美國](../Page/美國.md "wikilink")，由於避免政治逼害，叛國的指控不易入罪。自其開國至今，有關叛國的起訴不超過40宗。前[美國副总统](../Page/美國副总统.md "wikilink")[阿龙·伯尔是因为这个罪名被](../Page/阿龙·伯尔.md "wikilink")[托马斯·杰斐逊起訴而結束了政治生涯](../Page/托马斯·杰斐逊.md "wikilink")。

## 範例

  - 1914年，[奥匈帝国的](../Page/奥匈帝国.md "wikilink")[塞尔维亚族人](../Page/塞尔维亚人.md "wikilink")[加夫里洛·普林西普因为刺杀奥匈帝国王继承人](../Page/加夫里洛·普林西普.md "wikilink")[弗朗茨·斐迪南大公而被判](../Page/弗朗茨·斐迪南大公.md "wikilink")[有期徒刑](../Page/有期徒刑.md "wikilink")。
  - 1910年，簽訂[日韓合併條約的八位韓國內閣大臣](../Page/日韓合併條約.md "wikilink")，被韓國人普遍認定是賣國賊。
  - [汪精衛與日本合作組建](../Page/汪精衛.md "wikilink")[汪兆銘政權](../Page/汪兆銘政權.md "wikilink")，被中國人普遍認定是[賣國賊](../Page/賣國賊.md "wikilink")。
  - [维德孔·吉斯林协助纳粹德国入侵挪威](../Page/维德孔·吉斯林.md "wikilink"),并于1942年起担任纳粹占领下的挪威政府首脑，他的姓氏"吉斯林"在斯堪迪纳维亚语系和英语中成为"叛国者"的代名词。
  - [殷汝耕出任日本扶植的](../Page/殷汝耕.md "wikilink")[冀東防共自治政府行政長官](../Page/冀東防共自治政府.md "wikilink")，1947年被[重慶國民政府以](../Page/重慶國民政府.md "wikilink")[漢奸的罪名槍決](../Page/漢奸.md "wikilink")。
  - [西藏的](../Page/西藏.md "wikilink")[第十四世达赖喇嘛](../Page/第十四世达赖喇嘛.md "wikilink")1959年被迫逃离西藏流亡[印度](../Page/印度.md "wikilink")，[中华人民共和国聲稱第十四达赖喇嘛组织](../Page/中华人民共和国.md "wikilink")[藏独](../Page/藏独.md "wikilink")，是叛国者；2008年，第十四世达赖喇嘛宣称[阿魯納恰爾邦](../Page/阿魯納恰爾邦.md "wikilink")（中国称[藏南](../Page/藏南.md "wikilink")）属于印度。\[6\]
  - 1971年，在中共黨章中被定為「[毛澤東的接班人](../Page/毛澤東.md "wikilink")」的[林彪等人](../Page/林彪.md "wikilink")[出逃坠机](../Page/九一三事件.md "wikilink")，被中华人民共和国認為是叛國行為。\[7\]

## 參見

  - [通敵](../Page/通敵.md "wikilink")
  - [謀叛](../Page/謀叛.md "wikilink")
  - [漢奸](../Page/漢奸.md "wikilink")
  - [越奸](../Page/越奸.md "wikilink")
  - [台奸](../Page/台奸.md "wikilink")

## 参考文献

[叛國](../Category/叛國.md "wikilink")
[Category:罪名](../Category/罪名.md "wikilink")
[Category:国家安全](../Category/国家安全.md "wikilink")
[Category:欺瞞](../Category/欺瞞.md "wikilink")

1.  太平天國的洪天貴福(15歲)
2.  [光復會的](../Page/光復會.md "wikilink")[徐錫麟](../Page/徐錫麟.md "wikilink")
3.  [中華民國刑法-全國法規資料庫入口網站](http://law.moj.gov.tw/LawClass/LawSearchNo.aspx?PC=C0000001&DF=&SNo=103-115)
4.  [中華民國刑法-全國法規資料庫入口網站](http://law.moj.gov.tw/LawClass/LawSearchNo.aspx?PC=C0000001&DF=&SNo=116-119)
5.  [中華民國刑法-全國法規資料庫入口網站](http://law.moj.gov.tw/LawClass/LawSearchContent.aspx?PC=C0000001&K1=%E5%85%A7%E4%BA%82%E7%BD%AA)
6.
7.