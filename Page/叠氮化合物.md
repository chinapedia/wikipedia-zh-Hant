[叠氮根离子.png](https://zh.wikipedia.org/wiki/File:叠氮根离子.png "fig:叠氮根离子.png")。\]\]
**叠氮化合物**在[无机化学中](../Page/无机化学.md "wikilink")，指的是含有叠氮根离子的化合物（）；在[有机化学中](../Page/有机化学.md "wikilink")，则指含有[叠氮基](../Page/叠氮基.md "wikilink")（-N<sub>3</sub>）的化合物。

## 叠氮根离子

叠氮根离子为並非直线型结构，不論是離子型態抑或是有機物皆有微微彎曲(約172°)，属 D<sub>∞h</sub>
[点群](../Page/点群.md "wikilink")，价电子数为16，和NCN<sup>2−</sup>离子、CNO<sup>−</sup>离子、NCO<sup>−</sup>离子，CO<sub>2</sub>分子是[等电子体](../Page/等电子体.md "wikilink")。叠氮根离子的化学性质类似于[卤离子](../Page/卤离子.md "wikilink")，例如白色的
[AgN<sub>3</sub>](../Page/叠氮化银.md "wikilink") 和
[Pb(N<sub>3</sub>)<sub>2</sub>](../Page/叠氮化铅.md "wikilink")
难溶于水。作为[配体能和金属离子形成一系列](../Page/配体.md "wikilink")[配合物](../Page/配合物.md "wikilink")，如Na<sub>2</sub>\[Sn(N<sub>3</sub>)<sub>6</sub>\]、Cu(N<sub>3</sub>)<sub>2</sub>(NH<sub>3</sub>)<sub>2</sub>等\[1\]。

## 无机叠氮化物

绝大多数叠氮化物进行[爆炸分解](../Page/爆炸.md "wikilink")，但也可通过[热化学](../Page/热化学.md "wikilink")、[光化学或放电法使其缓慢分解](../Page/光化学.md "wikilink")。爆炸分解的结果是产生相应的[单质](../Page/单质.md "wikilink")，分解热即相当于该化合物的[标准生成焓](../Page/标准生成焓.md "wikilink")。

有些分解产生[氮化物](../Page/氮化物.md "wikilink")：

  -
    3LiN<sub>3</sub> → Li<sub>3</sub>N + 4N<sub>2</sub>↑

[叠氮化氢的热分解若在](../Page/叠氮化氢.md "wikilink")1000<sup>o</sup>C及低压条件下进行，产物收集在用[液氮冷却的表面上](../Page/液氮.md "wikilink")，则反应为：

  -
    6HN<sub>3</sub> → 7N<sub>2</sub> + H<sub>2</sub> + (NH)<sub>4</sub>

[碱金属叠氮并不爆炸](../Page/碱金属.md "wikilink")，只是缓慢分解：

  -
    2NaN<sub>3</sub> → 2Na + 3N<sub>2</sub>↑

[重金属叠氮化物的分解是由于叠氮根离子的激发](../Page/重金属.md "wikilink")，结果一个电子跃迁到[导带](../Page/导带.md "wikilink")，产生叠氮基。[基态的叠氮基解离成基态的N和N](../Page/基态.md "wikilink")<sub>2</sub>是[选律禁阻的](../Page/选律禁阻.md "wikilink")，解离成[激发态的N和N](../Page/激发态.md "wikilink")<sub>2</sub>虽是[选律允许的](../Page/选律允许.md "wikilink")，但需要259kJ/mol的能量，因此在常温下并不重要。两个叠氮基之间的相互作用也是选律允许的，并且是个放热的的过程，因此可以认为这一步在固体离子型叠氮化物的分解中是重要的一步。

叠氮化物迅速分解能导致爆炸点火或[起爆](../Page/起爆.md "wikilink")，但原因尚不清楚。

[叠氮化钠被用于汽车的](../Page/叠氮化钠.md "wikilink")[安全气囊内](../Page/安全气囊.md "wikilink")，叠氮化铅被用作[起爆剂](../Page/起爆剂.md "wikilink")。

## 叠氮根配位化合物

[Zn(N3)2(C5H5N)2.jpg](https://zh.wikipedia.org/wiki/File:Zn\(N3\)2\(C5H5N\)2.jpg "fig:Zn(N3)2(C5H5N)2.jpg")
叠氮根离子可以以[端基或](../Page/端基.md "wikilink")[橋基的方式与金属离子相结合](../Page/橋接配體.md "wikilink")：

  -

      -
        [叠氮根配位.png](https://zh.wikipedia.org/wiki/File:叠氮根配位.png "fig:叠氮根配位.png")

已知端基配位的较多，桥式的较少。

## 有机叠氮化合物

[Azide-2D.png](https://zh.wikipedia.org/wiki/File:Azide-2D.png "fig:Azide-2D.png")

有机叠氮化合物的端基碳具有[亲核性](../Page/亲核性.md "wikilink")，并且不稳定容易放出[氮气](../Page/氮气.md "wikilink")。[Curtius重排反应即经过](../Page/Curtius重排反应.md "wikilink")[酰基叠氮中间体](../Page/酰基叠氮.md "wikilink")。

有些叠氮化合物是[1,3-偶极体](../Page/1,3-偶极体.md "wikilink")，可进行[环加成反应](../Page/环加成反应.md "wikilink")。

## 参见

  - [疊氮酸](../Page/疊氮酸.md "wikilink")
  - [叠氮化钠](../Page/叠氮化钠.md "wikilink")

## 参考资料

<references />

<div class="references-small">

  - 宋天佑等.《无机化学》下册.北京：高等教育出版社，2004年. ISBN 7-04-015582-6
  - 张青莲等.《无机化学丛书》第四卷，氮磷砷分族.北京：科学出版社，1984年.

</div>

## 外部链接

  - [有机叠氮化合物的合成（英文）](https://www.organic-chemistry.org/synthesis/C1N/azides/index.shtm)
  - [关于有机叠氮化合物（英文）](http://www.ehs.ucsb.edu/units/labsfty/labrsc/factsheets/Azides_FS26.pdf)
  - [酰基叠氮（英文）](http://pubs.acs.org/cgi-bin/abstract.cgi/joceah/asap/abs/jo070162e.html)
  - [叠氮化合物的合成和还原（英文）](http://designer-drugs.com/pte/12.162.180.114/dcd/chemistry/azide.rxns.html)

[D](../Category/官能团.md "wikilink") [\*](../Category/叠氮化合物.md "wikilink")
[Category:爆炸物](../Category/爆炸物.md "wikilink")

1.  刘新锦等.无机元素化学(第二版) 氮族元素.北京:科学出版社.2010.01 ISBN 978-7-03-026399-5