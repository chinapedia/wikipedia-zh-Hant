[Lapiz_labial.jpg](https://zh.wikipedia.org/wiki/File:Lapiz_labial.jpg "fig:Lapiz_labial.jpg")
**-{口}-紅**，又称**-{脣膏}-**，是一種主要使用於[嘴脣上並可以增加嘴脣的色澤或改變嘴脣的顏色的](../Page/嘴脣.md "wikilink")[化妝品](../Page/化妝品.md "wikilink")。口红是彩妝中重要的化妝品之一；主要成份包含了[蠟](../Page/蠟.md "wikilink")、[油和](../Page/油.md "wikilink")[食用色素](../Page/食用色素.md "wikilink")。最初的口紅是膏狀，與現今的[唇蜜相似](../Page/唇蜜.md "wikilink")，子彈型的口紅是在[第一次世界大戰時才發明的](../Page/第一次世界大戰.md "wikilink")。

許多化妝品廠商會以顏色或流行的名稱來為口紅命名，第一個不以口紅顏色為名而以號碼行銷的品牌是[伊夫·圣罗兰](../Page/伊夫·圣罗兰_\(品牌\).md "wikilink")。

## 口紅的歷史

考古學家發現，世界上的第一支口紅在[蘇美人的城市](../Page/蘇美人.md "wikilink")[烏爾被發現](../Page/烏爾.md "wikilink")，約五千年前。[古埃及人會使用黑色](../Page/古埃及.md "wikilink")、橘色、紫紅色的唇膏，男性也會使用；古羅馬時代一種名為Fucus的口紅是以紫紅色含[水銀的植物染液和](../Page/水銀.md "wikilink")[紅酒沉澱物所製成](../Page/紅酒.md "wikilink")。男性也會塗口紅。中國[唐朝貴族婦女和](../Page/唐朝.md "wikilink")[教坊](../Page/教坊.md "wikilink")[歌妓喜歡以檀色](../Page/歌妓.md "wikilink")（赭紅）注唇，後世沿用。在[維多利亞女王時期](../Page/維多利亞女王.md "wikilink")，口紅被視為是[妓女的用品](../Page/妓女.md "wikilink")，使用口紅是一種[禁忌](../Page/禁忌.md "wikilink")。根據文獻記載，[伊莉莎白一世以口紅抹粉來對抗死亡](../Page/伊莉莎白一世.md "wikilink")。中國古代稱為**口脂**，有製成筒狀，也有些是粉狀，粉狀口紅的用法是將色素塗於[紙的兩面](../Page/紙.md "wikilink")，用嘴唇抿住後，顏色自然會附於唇上。

大約在1660-1789年，[歐洲的](../Page/歐洲.md "wikilink")[法國和](../Page/法國.md "wikilink")[英國男士間流行塗口紅](../Page/英國.md "wikilink")。

[十八世紀美國的](../Page/十八世紀.md "wikilink")[清教徒移民並不流行塗口紅](../Page/清教徒.md "wikilink")，愛美的[女人會趁人不注意時以絲帶摩擦嘴唇](../Page/女人.md "wikilink")，以增加紅潤，這樣的情形直到[十九世紀](../Page/十九世紀.md "wikilink")，[十九世紀流行蒼白](../Page/十九世紀.md "wikilink")，口紅和化妝品被視為禁忌，而轉為以藥品型式販售。

[法國嬌蘭](../Page/法國.md "wikilink")(Guerlain)將管狀口紅引進美國，販售對象主要為少數貴族，第一支金屬管口紅是由[美國](../Page/美國.md "wikilink")[康乃狄克州](../Page/康乃狄克州.md "wikilink")[沃特伯里的毛里斯李維和史柯維爾製造公司](../Page/沃特伯里_\(康涅狄格州\).md "wikilink")(Maurice
Levy and the Scovil Manufacturing Company)於1915年製造，屬於大眾化產品。

1912年[紐約市婦女參政權論者的示威活動中](../Page/紐約.md "wikilink")，著名的[女性主義者都抹上口紅](../Page/女性主義者.md "wikilink")，把口紅示為婦女解放的象徵。1920年代的美國，由於[电影的流行](../Page/电影.md "wikilink")，也帶動了口紅的流行，其後各類口紅顏色的流行都會受到影視明星的影響，而帶動風潮。但當時的口紅是以[肥皂為基底](../Page/肥皂.md "wikilink")，使用起來並不讓人感到舒適。

1940年代的美國女性受到戰爭的影響，會以化妝來保持好臉色，當時最大的口紅廠商之一Tangee，曾推出一個名為「[戰爭](../Page/戰爭.md "wikilink")、[女人和口紅](../Page/女人.md "wikilink")」的[廣告](../Page/廣告.md "wikilink")。

1950年戰爭結束，女星們帶動將唇形顯得飽滿、魅惑的流行。1960年代，由於流行[白色與](../Page/白色.md "wikilink")[銀色等淺色的口紅妝](../Page/銀色.md "wikilink")，魚鱗被用來製造出閃動的效果。1970年[-{zh-cn:迪斯科;
zh-hk:的士高;}-流行時](../Page/迪斯可.md "wikilink")，[紫色是一種流行的口紅顏色](../Page/紫色.md "wikilink")，而[龐克族喜愛的口紅顏色為](../Page/龐克.md "wikilink")[黑色](../Page/黑色.md "wikilink")。一些新世紀奉行者(New
Ager)開始將自然植物的成份帶入口紅之中。接著搖滾樂手的裝扮帶動了男人使用口紅的風潮。特別是1980年代的[喬治男孩樂團](../Page/喬治男孩樂團.md "wikilink")。1990年代的口紅開始出現[咖啡色的口紅](../Page/咖啡色.md "wikilink")，在某些搖滾樂團中也出現了使用黑、藍色的唇色。1990年代末期，[維他命](../Page/維他命.md "wikilink")、[藥草](../Page/藥草.md "wikilink")、[香料等材料被大量添加於口紅之中](../Page/香料.md "wikilink")。2000年後，潮流走向展現自然美感，較常選用[珍珠色及淡紅色](../Page/珍珠.md "wikilink")，顏色不誇張，色彩自然而有光澤。

## 口紅的成份

[Lipsticks.jpg](https://zh.wikipedia.org/wiki/File:Lipsticks.jpg "fig:Lipsticks.jpg")

  - 基底：[油](../Page/油.md "wikilink")、[蠟](../Page/蠟.md "wikilink")、[軟化劑](../Page/軟化劑.md "wikilink")，使口紅能夠凝固、持久
      - 蠟：[蜂蠟](../Page/蜂蠟.md "wikilink")、[棕櫚蠟](../Page/棕櫚蠟.md "wikilink")、[蜜蠟最常被使用的](../Page/蜜蠟.md "wikilink")，棕櫚蠟較不易熔化
      - 油：[礦物油](../Page/礦物油.md "wikilink")、[蓖麻油](../Page/蓖麻油.md "wikilink")、[羊毛脂](../Page/羊毛脂.md "wikilink")，[石蠟油](../Page/石蠟油.md "wikilink")([凡士林](../Page/凡士林.md "wikilink"))
      - 軟化劑可增加顏色附著到唇上的能力，也可滋潤

唇蜜含有較多的油，較少的蠟。

  - 著色劑:含有顏料或染料，口紅使用的顏料須為較細的顆粒，才能均勻附著於唇上。
  - 香料和調味，可掩去上述成份的味道。

化學家認為口紅的六種基本成分為蜜蠟、棕櫚蠟、堪地里拉蠟(candelilla wax，一種植物蠟)、蓖麻油、羊毛脂、石蠟。

成分比為：蓖麻油65%，蜜蠟15%，棕櫚蠟8-10%，羊毛脂5%，些許著色劑和香味。

一些具閃爍效果的口紅也包含了[雲母](../Page/雲母.md "wikilink")、[氧化鐵](../Page/氧化鐵.md "wikilink")、[二氧化鈦等成份](../Page/二氧化鈦.md "wikilink")。某些較鮮紅或暗紅效果的口紅，成份中可能含有寄生在仙人掌上乾胭脂蟲(cochineal)，也包含了一些防腐的成份。

## 使用方法

  - 直接塗於唇上
  - 在唇的中間點上幾點顏色，用手指向兩側推向嘴角。
  - 先上粉底，以唇線筆勾出輪廓，塗上口紅，以面紙抿去多餘的顏色，在唇上抹粉、再塗一層口紅，以面紙抿一下，再用唇線修整、塗上唇彩。
  - 使用唇刷沾口紅顏色，擦於唇上，在唇刷保養得當的情形下，這是一種比較衛生的做法。

## 口紅的保存

口紅可保存於常溫之下，存放地點應避免高溫。當口紅破裂時，可以用[打火機將斷裂的兩截修復](../Page/打火機.md "wikilink")，然後冰於[冰箱中](../Page/冰箱.md "wikilink")，直到變硬。為了保持口紅的衛生，可以用少量的酒精清洗口紅。口紅的保存期限約一至兩年，當口紅的顏色改變或發出異味時，應停止使用。

[Category:嘴](../Category/嘴.md "wikilink")
[Category:唇部化妝品](../Category/唇部化妝品.md "wikilink")