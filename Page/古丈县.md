**古丈县**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[湖南省](../Page/湖南省.md "wikilink")[湘西土家族苗族自治州下辖的一个县](../Page/湘西土家族苗族自治州.md "wikilink")，辖域面积1297.45平方公里，[国内生产总值](../Page/国内生产总值.md "wikilink")39,071万元（西元2004年），总人口为137,637人（西元2004年），少数民族人口117,066人，占总人口的85.1
%，其中[土家族](../Page/土家族.md "wikilink")54,169人，[苗族](../Page/苗族.md "wikilink")62,683人
[1](https://web.archive.org/web/20051126233702/http://www.hntj.gov.cn/tjgb/xqgb/xxgb/default.htm)。

其著名产品有[古丈毛尖](../Page/古丈毛尖.md "wikilink")。

## 历史

[汉朝时为酉阳县地](../Page/汉朝.md "wikilink")。梁至[隋朝](../Page/隋朝.md "wikilink")，为大乡县地。唐代后为溪州地。五代晋天福五年（940）溪州治所迁今会溪坪，改名下溪州，又称誓下州。元为保靖州地。明为[永顺宣慰司地](../Page/永顺宣慰司地.md "wikilink")。清初为[永顺县地](../Page/永顺县.md "wikilink")。[道光二年析置古丈坪厅](../Page/道光二年.md "wikilink")，治古丈坪（今古阳镇），厅因坪得名。1913年起改古丈县。

## 地理

古丈位于[湖南西部](../Page/湖南.md "wikilink")、[湘西州中部偏东](../Page/湘西.md "wikilink")。

[武陵山脉斜贯全境](../Page/武陵山脉.md "wikilink")，最高海拔1146米，最低海拔147米；主要河流为[酉水](../Page/酉水_\(沅水支流\).md "wikilink")。相邻的[县级行政区](../Page/县级行政区.md "wikilink")，北接[永顺](../Page/永顺县.md "wikilink")，东临[沅陵](../Page/沅陵县.md "wikilink")，南界[泸溪和](../Page/泸溪县.md "wikilink")[吉首](../Page/吉首市.md "wikilink")，西部和[花垣相连](../Page/花垣县.md "wikilink")。

## 行政区划

古丈县辖有7个镇：

  - 镇：古阳镇、高峰镇、岩头寨镇、坪坝镇、断龙山镇、红石林镇、默戎镇

## 人物

著名女歌唱家、苗族人[宋祖英出生于古丈县](../Page/宋祖英.md "wikilink")。

## 官方网站

  - [湖南省湘西古丈县人民政府网](http://www.gzx.gov.cn/)

[古丈县](../Category/古丈县.md "wikilink") [县](../Category/湘西县市.md "wikilink")
[湘西](../Category/湖南省县份.md "wikilink")
[Category:土家族](../Category/土家族.md "wikilink")
[Category:苗族](../Category/苗族.md "wikilink")