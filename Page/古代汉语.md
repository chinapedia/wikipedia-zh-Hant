**古代漢語**是与[现代漢语相对而言的](../Page/现代漢语.md "wikilink")，它是[汉族祖先及其后代在古代的语言](../Page/汉族.md "wikilink")。一般人心目中的古代汉语的面貌在语音上为唐诗宋词等[韵文](../Page/韵文.md "wikilink")，文字上体现为[王羲之](../Page/王羲之.md "wikilink")、[欧阳询](../Page/欧阳询.md "wikilink")、[柳公权](../Page/柳公权.md "wikilink")、[颜真卿等人的书法作品](../Page/颜真卿.md "wikilink")，词汇和语法则为先秦到明末清初的[文言文](../Page/文言文.md "wikilink")。

## 時期

直到現在，還沒有對古代漢語和[近代漢語定下公認明顯的劃分界線](../Page/近代漢語.md "wikilink")。一般都以「[五四運動](../Page/五四運動.md "wikilink")」/「[白話文運動](../Page/白話文運動.md "wikilink")」為[分水嶺](../Page/分水嶺.md "wikilink")。

古代的汉语有三个明显的发展阶段：以[先秦时代为核心的](../Page/先秦.md "wikilink")[上古漢语](../Page/上古漢语.md "wikilink")，其语音体系一般稱[上古音](../Page/上古汉语.md "wikilink")，以诗经和形声字的[古韻推测](../Page/古韻.md "wikilink")，文字为[甲骨文](../Page/甲骨文.md "wikilink")、[金文和](../Page/金文.md "wikilink")[篆书](../Page/篆书.md "wikilink")；以[唐代和](../Page/唐代.md "wikilink")[宋代为核心的](../Page/宋代.md "wikilink")[中古漢语](../Page/中古漢语.md "wikilink")，其语音体系一般稱[中古音](../Page/中古音.md "wikilink")，为[等韻和](../Page/等韻.md "wikilink")[韵图所记录](../Page/韵图.md "wikilink")，文字为楷书；以明末[清初为核心的](../Page/清朝.md "wikilink")[近代漢语](../Page/近代漢语.md "wikilink")。亦有人分成先秦、兩漢[六朝](../Page/六朝.md "wikilink")、唐宋、元明清四個階段。古代汉语在不同阶段最明显的差别是语音，而[上古漢语还有文字也与后世不同](../Page/上古漢语.md "wikilink")。狭义的古代汉语的面貌大体上和[中古漢语一致](../Page/中古漢语.md "wikilink")，因为这是古代汉语发展最繁荣、影响力最大、产生最多代表性作品的时期。

## 文字

[漢字以紀錄](../Page/漢字.md "wikilink")[語素和](../Page/語素.md "wikilink")[音節方式紀錄文字](../Page/音節.md "wikilink")，稱為「音節語素」文字。意思是一個文字符號紀錄音節和具獨立意義的語素。

漢朝[許慎師承](../Page/許慎.md "wikilink")[劉歆](../Page/劉歆.md "wikilink")，将漢字的構造和使用規律分為六種，稱為「[六書](../Page/六書.md "wikilink")」：即[象形](../Page/象形.md "wikilink")、[指事](../Page/指事.md "wikilink")、[會意](../Page/會意.md "wikilink")、[形聲](../Page/形聲.md "wikilink")、[轉注](../Page/轉注.md "wikilink")、[假借](../Page/假借.md "wikilink")。前四者是造字規律，而後兩者是用字規律。

一曰指事。指事者，視而可識，察而見意，上、下是也。 二曰象形。象形者，畫成其物，隨體詰詘，日、月是也。
三曰形聲。形聲者，以事為名，取譬相成，江、河是也。
四曰會意。會意者，比類合宜，以見指撝，武信是也。 五曰轉注。轉注者，建類一首，同意相受，考老是也。
六曰假借。假借者，本無其字，依聲託事，令長是也。」

### 不足

「六書」的不足之處在于早期漢字以一個文字符號紀錄一個語素，依據「因義造意，據意構形」的理論造字，即是依語素意義，定立意圖，最後構字形，這是象形字。

## 字體

[夏朝](../Page/夏朝.md "wikilink")、[商朝](../Page/商朝.md "wikilink")、[周朝主要使用的文字是](../Page/周朝.md "wikilink")[甲骨文](../Page/甲骨文.md "wikilink")、[金文](../Page/金文.md "wikilink")。[春秋戰國時期](../Page/春秋戰國.md "wikilink")，各個[諸侯發展自己的書寫方式](../Page/諸侯.md "wikilink")
。

到了[秦朝](../Page/秦朝.md "wikilink")，文字書寫方式才得到統一，以[小篆作为通用的字体](../Page/小篆.md "wikilink")，並將[戰國時不少秦國工匠為書寫方便無意間創造之](../Page/戰國時代.md "wikilink")[隸書標準化](../Page/隸書.md "wikilink")，作為對[小篆之简化书写](../Page/小篆.md "wikilink")。

实际上，由于[汉字发展的师承关系](../Page/汉字.md "wikilink")，地域不同，实际书写也有所不同。所以一部分[古籍和](../Page/古籍.md "wikilink")[字画中使用了不同写法的](../Page/字画.md "wikilink")[汉字](../Page/汉字.md "wikilink")。

## 語法

**古代汉语**的语法主要体现在词汇使用和句子结构上。

### 词汇使用

[汉语没有词性变化](../Page/汉语.md "wikilink")，直接利用别的词性的词汇来活用（这-{只}-是用现代的概念来说，不是说，中国古代汉语有这些概念）。比如名词动用，动词名词化，动词形容词化等。

### 句子结构

大量使用省略句式，倾向于省略主语，所以大量句子看不到主语的存在，只能靠上下文的意思来推定。现代汉语大体上保留了这种习惯，尤其是口语。

否定句式中将宾语提前。例如：“时不我與”是典型的古汉语结构的一个成语，把“我”这个宾语提在“與”的前面。这种用法在现代汉语普通话和各种方言中都完全消失。

## 音韻

在[周朝之前](../Page/周朝.md "wikilink")，中國境內沒有統一的音韻
。隨著周朝國力日增，各地的音韻慢慢以首都的口音為標準，稱為雅言。[爾雅就是](../Page/爾雅.md "wikilink")[周朝第一本把字詞分類的著作](../Page/周朝.md "wikilink")。「爾」的意思是近/接近
。「雅」的意思是[雅言](../Page/雅言.md "wikilink")，即是正音 。

## 拼音系統

早期汉语的拼音系统为「[讀若法](../Page/讀若法.md "wikilink")」或「[直注法](../Page/直注法.md "wikilink")」，即用相同或相似读音的字来标注别的字的读音。

後來發明了「[反切](../Page/反切.md "wikilink")」：一般是用兩個的漢字音韻合併，用前者的聲母跟後者的韻母連在一起念。隨著時代變化，古代音韻跟現代音韻差別不同。

## 被採用的地區

因為中國古代國力鼎盛，所以影響鄰近地區的民族採納或者參考漢語及漢字作為當地的語音及文字。

文字方面，例如：[朝鮮半島從](../Page/朝鮮半島.md "wikilink")[朝鲜三国时期是直接採用](../Page/朝鲜三国时期.md "wikilink")[漢字](../Page/漢字.md "wikilink")，有學說指在[古朝鮮時亦有使用](../Page/古朝鮮.md "wikilink")[燕](../Page/燕國.md "wikilink")、[齊一帶通行的文字](../Page/田齊.md "wikilink")。[遼朝根據漢字來自創](../Page/遼朝.md "wikilink")[契丹大字](../Page/契丹大字.md "wikilink")（[契丹小字为依照](../Page/契丹小字.md "wikilink")[回鹘文创建的字母拼写文字](../Page/回鹘文.md "wikilink")）。[金朝](../Page/金朝.md "wikilink")（[女真](../Page/女真.md "wikilink")）大字根据契丹大字结合汉字为基础再次改造出新变种[方块字](../Page/方块字.md "wikilink")。西夏文亦是根据汉字笔画改造而成。

語音方面，例如：現代[日語的汉字](../Page/日本語.md "wikilink")[音读保留了六朝江南音和唐朝长安音的一些特征](../Page/音读.md "wikilink")，現代[朝鲜语和现代](../Page/朝鲜语.md "wikilink")[越南语的汉语词都保留了唐朝长安音的一些特征](../Page/越南语.md "wikilink")。詳見[日本漢字音](../Page/日本漢字音.md "wikilink")、[朝鮮漢字音和](../Page/朝鮮漢字音.md "wikilink")[越汉音](../Page/越汉音.md "wikilink")。

## 相關影片資料

  - [李白 月下獨酌 中古漢語朗讀](http://www.youtube.com/watch?v=F-z1fCIE8Mc)
  - [李紳
    憫農（鋤禾日當午）中古漢語朗讀](http://www.youtube.com/watch?v=zAQeEzDL7-A&feature=related)
  - [古代汉语--语法](https://web.archive.org/web/20110527014614/http://v.ku6.com/show/9qeVjt1baBNohhX8.html)
  - [南师大古代汉语国家精品课程－董志翘《古代汉语通论·诗律》](http://v.youku.com/v_show/id_XMjYxNzg0MDA=.html)

## 参见

  - [文言文](../Page/文言文.md "wikilink")
  - [上古汉语](../Page/上古汉语.md "wikilink")
  - [中古汉语](../Page/中古汉语.md "wikilink")
  - [近代汉语](../Page/近代汉语.md "wikilink")
  - [切韵音](../Page/切韵音.md "wikilink")

## 參考文獻

## 外部連結

  - [先秦兩漢原典資料庫](http://ctext.org/pre-qin-and-han/zh) -
    [中國哲學書電子化計劃](../Page/中國哲學書電子化計劃.md "wikilink")
    （[繁體](http://ctext.org/pre-qin-and-han/zh)／[簡體](http://ctext.org/pre-qin-and-han/zhs)）

{{-}}

[古代漢語](../Category/古代漢語.md "wikilink")