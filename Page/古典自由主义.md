**古典自由主义**可以指：

  - 早期的[自由主義](../Page/自由主義.md "wikilink")，從[啟蒙時代開始直到](../Page/啟蒙時代.md "wikilink")[约翰·斯图尔特·密尔為止的自由主義](../Page/约翰·斯图尔特·密尔.md "wikilink")，中文因为共产与社会主义学说对它的批评，采用了强调“古典”的名称，这个译法让中文界普遍把经典或是直称自由主义的学说，与下面的另一种自由主义混为一谈。
  - 在20世紀復甦的相同政治哲學，追溯上述的早期自由主義，亦即[自由意志主義](../Page/自由意志主義.md "wikilink")，英文为了区分其在经济与道德观上同[共产主义当时已经反对的](../Page/共产主义.md "wikilink")[自由主义](../Page/自由主义.md "wikilink")(严格说，译为经典自由主义更加接近)，自称为"自由(意志)者的(东西)"(英文Liberation按语言直译如此)，当然，它的中文如前，较另一个自由主义在译名贴近原文。\[1\]\[2\]。

<!-- end list -->

  -
    *而本條目所述及的即是第一種早期的自由主義。*

**经典自由主義**（**Classical
liberalism**）是一種支持個人先於國家存在的政治哲學，強調個人的權利、私有財產，並主張[自由放任的經濟政策](../Page/自由放任.md "wikilink")\[3\]，認為政府存在的目的僅在於保護每個個體的自由。古典自由主義發源於17世紀和18世紀，也因此，它通常被視為由於工業革命和隨後的資本主義體制而產生的一種意識形態。[言論自由](../Page/言論自由.md "wikilink")、信仰自由、思想自由、自我負責，和自由市場等概念最先也是由古典自由主義所提出，後來才陸續被其他政治意識形態所採納。古典自由主義反對當時絕大多數較早期的政治學說，例如[君權神授說](../Page/君權神授說.md "wikilink")、[世襲制度和](../Page/世襲制度.md "wikilink")[國教制度](../Page/国教.md "wikilink")，強調個人的自由、理性、正義和寬容\[4\]。[美國革命和](../Page/美國革命.md "wikilink")[法国大革命都受到了古典自由主義的影響](../Page/法国大革命.md "wikilink")。

古典自由主義认为一国的真正[财富不是金银总量的增加](../Page/财富.md "wikilink")，而是其国民创造的[商品](../Page/商品.md "wikilink")[服务的数量](../Page/服务.md "wikilink")。[政府的干预经常是阻碍了](../Page/政府.md "wikilink")[经济的增长](../Page/经济.md "wikilink")，因此主张将政府排除出经济领域，让经济生活自行其事，让追求自身[利益的无数的个体](../Page/利益.md "wikilink")[理性的计算来引导经济生活的调整](../Page/理性.md "wikilink")，即“社會應當盡量從政府干預中擺脫出来，盡量的自由”，“管得最少的政府是最好的政府”。

## 概觀

古典自由主義哲學特別重視[個人的主權](../Page/自我所有權.md "wikilink")，個人財產的[所有权被視為個人自由最重要的部分](../Page/所有权.md "wikilink")，強調[自由放任的政策](../Page/自由放任.md "wikilink")。古典自由主義並不必然支持民主的原理，這是因為尊重和保護個人財產權的法律，比民主裡的多數決原則還要重要\[5\]。舉例而言，[詹姆斯·麦迪逊主張](../Page/詹姆斯·麦迪逊.md "wikilink")[共和立憲制以保護個人的自由](../Page/共和立憲制.md "wikilink")，他擔心[純粹民主制可能會造成](../Page/直接民主.md "wikilink")「公共的情緒和利益被多數派掌控，而卻沒有半點避免少數派被犧牲的機制存在」\[6\]。在經濟上，古典自由主義堅持一個「不受管制的自由市場」才能有效滿足人類的需求、並且將資源分配至最合適的地方。他們對於自由市場的支持是因為「假定個人都是理性的、追求私利的、並且會有計畫的追求他們各自目標的。」\[7\]他們不相信個人權利是由政府所「創造」的（在道德層次上），而相信道德權利是獨立於政府之外存在的。[托马斯·杰斐逊稱呼這些是](../Page/托马斯·杰斐逊.md "wikilink")「無法被分割的權利」，並且也指出古典自由主義所相信的理念：亦即權利並非來自法律、相反的法律的唯一目的便是用以保護個人的權利，他宣稱「正當的自由，指的是個人有絕對權利依照他們自己的意志做出任何行動，唯一的限制便是不違反其他人的相同權利。我不會加上『以法律為限』，因為法律經常只是專制者的工具，這在法律侵犯個人權利時尤其明顯。」\[8\]對於古典自由主義者而言，個人的權利是**消極本質**—亦即權利是以不受其他人（以及政府）侵犯的個人自由為基準。相反的，[社會自由主義](../Page/社會自由主義.md "wikilink")（又常稱為「現代自由主義」）則主張權利是由其他人提供的某些利益或服務所構成的。因此古典自由主義在本質上是徹底反對[福利國家等政策的](../Page/福利國家.md "wikilink")。古典自由主義強調「法律之前人人平等」，但卻不主張在物質上的平等。古典自由主義認為社會自由主義所追求的那些「積極權利」反而會侵蝕原本消極的個人權利\[9\]。因此，古典自由主義支持以憲法保護個人的自由和財產權免受多數統治的干擾，並認為人民投票僅僅是為了選出官員，而不是為了創造法律。

[弗里德里克·哈耶克指出古典自由主義有著兩種不同的流派](../Page/弗里德里克·哈耶克.md "wikilink")：「英國的流派」以及「法國的流派」。哈耶克認為英國哲學家[大卫·休谟](../Page/大卫·休谟.md "wikilink")、[亚当·斯密](../Page/亚当·斯密.md "wikilink")、[埃德蒙·伯克等人代表了](../Page/埃德蒙·伯克.md "wikilink")[经验主义流派的思想](../Page/经验主义.md "wikilink")，注重[普通法](../Page/英美法系.md "wikilink")，並且遵循長期自然發展下來的傳統和思想。而法國的流派如[卢梭](../Page/让-雅克·卢梭.md "wikilink")、[百科全书派](../Page/百科全书派.md "wikilink")、以及[重农主义則相信理性主義](../Page/重农主义.md "wikilink")、認為理性的力量是毫無限制的，並且有時會對傳統和宗教表現出敵意。哈耶克承認國籍的分類是和流派的分類不同的，有些哲學家並不一定符合國籍的分類：哈耶克認為法國的[孟德斯鳩屬於](../Page/孟德斯鳩.md "wikilink")「英國的流派」，而英國的[托马斯·霍布斯](../Page/托马斯·霍布斯.md "wikilink")、[威廉·戈德温](../Page/威廉·戈德温.md "wikilink")、[约瑟夫·普利斯特里](../Page/约瑟夫·普利斯特里.md "wikilink")、和[托马斯·潘恩則屬於](../Page/托马斯·潘恩.md "wikilink")「法國的流派」\[10\]。哈耶克也否認「[laissez
faire](../Page/自由放任.md "wikilink")」這一詞是源於法國的傳統，並將其歸功於英國的休谟、斯密、和伯克。

## 起源

古典自由主義是政治和經濟的一種哲學，最早的根源可以溯及[古希腊](../Page/古希腊.md "wikilink")。在16世紀初[啟蒙時代由西班牙的](../Page/啟蒙時代.md "wikilink")[薩拉曼卡學派提出早期的論述](../Page/薩拉曼卡學派.md "wikilink")。而在[蘇格蘭哲學家](../Page/蘇格蘭.md "wikilink")[亚当·斯密所著的](../Page/亚当·斯密.md "wikilink")[國富論](../Page/國富論.md "wikilink")（1776）中，他反對了主張由國家干預經濟並實行[貿易保護的](../Page/貿易保護主義.md "wikilink")[重商主义](../Page/重商主义.md "wikilink")，認為重商主義只富裕了那些擁有特權的菁英份子，而沒有顧及廣大平民。[芬蘭議員](../Page/芬蘭.md "wikilink")[安德斯·屈德紐斯](../Page/安德斯·屈德紐斯.md "wikilink")（Anders
Chydenius）也是另一個提出早期古典自由主義論述的重要人物。古典自由主義試著限制政治權力的界限，以保護個人的自由和財產權。古典自由主義一詞也常被使用於描述早期的[自由主義](../Page/自由主義.md "wikilink")，以免與[新自由主義](../Page/社會自由主義.md "wikilink")（[社會自由主義](../Page/社會自由主義.md "wikilink")）混淆。

[AdamSmith.jpg](https://zh.wikipedia.org/wiki/File:AdamSmith.jpg "fig:AdamSmith.jpg")\]\]

[亚当·斯密於](../Page/亚当·斯密.md "wikilink")1776年所出版的[國富論成了自由主義的理論根基](../Page/國富論.md "wikilink")，亚当·斯密提出了關於自由主義和經濟的解釋，在法律和哲學上的理解則經由[约翰·洛克](../Page/约翰·洛克.md "wikilink")、[托马斯·杰斐逊](../Page/托马斯·杰斐逊.md "wikilink")、[詹姆斯·麦迪逊等人](../Page/詹姆斯·麦迪逊.md "wikilink")。而在[伊曼努尔·康德的著作](../Page/伊曼努尔·康德.md "wikilink")，《[永久和平](../Page/永久和平.md "wikilink")》（Perpetual
Peace）中，他假設了一種國際性的自由體制，以維持世界的和平。

「自由主義」一詞在此時開始出現分歧（大約在18和19世紀）。原先的自由主義主張個人自由、經濟自由（包括自由市場），和有一定權力限制的[代議制政府](../Page/代議制.md "wikilink")。到了18世紀這層原先代表的意義僅在少數國家仍然完整存在，大多數國家裡自由主義一詞都已經偏離了最初的軌道（例如社會福利、關稅、政府對經濟的介入和規定、薪水和物價的控制）。在許多國家自由主義一詞大多用以形容處在古典自由主義至[美國自由主義之間的立場](../Page/美國自由主義.md "wikilink")，只有少數幾個主要政黨仍然支持古典自由主義，大多數的自由主義政黨都接受了政府對經濟進行干預的概念。

## 古典自由主義與現代自由主義

近代[工业革命大幅提升了人類的物質文明](../Page/工业革命.md "wikilink")，但也造成許多社會問題浮上檯面，例如[污染](../Page/污染.md "wikilink")、[童工](../Page/童工.md "wikilink")，和都市人口過於擁擠等。物質和科學上的進步增長了人類的壽命，減少了死亡率，也因此人口爆炸性的增長。而這造成了勞工過多的問題，減低了平均的工資。[米爾頓·佛利民指出這個時代造成的現象不是貧窮人口增加](../Page/米爾頓·佛利民.md "wikilink")，而是“貧窮現象更顯而易見”了。古典自由主義的經濟學家，如[约翰·洛克](../Page/约翰·洛克.md "wikilink")、[亚当·斯密](../Page/亚当·斯密.md "wikilink")、[威廉·馮·洪堡則認為這些問題將會由工業社會自身進行修正](../Page/威廉·馮·洪堡.md "wikilink")，而無須政府的干預。

在19世紀，多數的民主國家都延伸了[選舉權](../Page/選舉權.md "wikilink")，而這些新獲得選舉權的公民往往傾向支持[政府干預的政策](../Page/政府干預.md "wikilink")，由[識字率的提高和知識傳播的發達產生了在社會上各種形式的](../Page/識字率.md "wikilink")[行動主義](../Page/行動主義.md "wikilink")。19世紀產生的[社會自由主義](../Page/社會自由主義.md "wikilink")（Social
liberalism）成了第一個從古典自由主義裡分裂出來的重要流派，社會自由主義主張立法禁止[童工和規定勞工的最低工資](../Page/童工.md "wikilink")，而這些都是主張自由放任經濟的自由主義者所視為妨礙自由的政策，更認為這些政策會影響經濟的發展。

到了19世紀末，這些從自由主義產生的分裂已經逐漸擴大，他們認為為了要達成自由的目標，個人必須得到為達成目標的條件，包括[教育和免於受剝削的保護](../Page/教育.md "wikilink")。在1911年由[霍布豪斯](../Page/霍布豪斯.md "wikilink")（Leonard
Trelawny Hobhouse）所著的《自由主義》一書裡，他總結了「新自由主義」（New liberalism，现多称Social
Liberalism（[社会自由主义](../Page/社会自由主义.md "wikilink")），以与[Neoliberalism区分](../Page/新自由主义.md "wikilink")）的概念，也就是主張政府介入經濟，並應確保每個人在貿易時的平等權利。由於霍布豪斯的「自由主義」與古典自由主義的差異實在太大，[哈耶克甚至指出那應該被稱為](../Page/弗里德里克·哈耶克.md "wikilink")「社會主義」比較正確\[11\]（霍布豪斯的確曾自稱他的理論為「自由社會主義」）。

古典自由主義相信自由的哲學應該在每個領域都同等擴大（而不是在某些領域受到侷限），他們非常反對現代自由主義的政策，如[槍枝管理法](../Page/槍枝管理法.md "wikilink")、[平權法案](../Page/平權法案.md "wikilink")（Affirmative
action,
鼓勵錄用女性和少數族群的政策）、[高稅收](../Page/稅賦.md "wikilink")、社會福利和支持[公立學校](../Page/公立學校.md "wikilink")，這些都是古典自由主義者所認為侵犯了個人自由的政策。

在[美國](../Page/美國.md "wikilink")「自由主義」一詞早已改變了原意，[哈耶克認為涵義的改變是從](../Page/弗里德里克·哈耶克.md "wikilink")[羅斯福任內開始的](../Page/富兰克林·德拉诺·罗斯福.md "wikilink")，羅斯福實行的[新政在當時被貼上](../Page/罗斯福新政.md "wikilink")[社會主義和](../Page/社會主義.md "wikilink")[左翼的標籤](../Page/左翼.md "wikilink")，由於擔心這些標籤的負面影響，羅斯福於是改自稱為自由主義者。自從那時開始，「自由主義」一詞在美國改變了涵義，與原本18和19世紀的自由主義完全不同了。

## 自由主義對抗極權主義

自由主義一向定義自身為自由的一方，而不是隨心所欲和处心积虑的專制和[极权主义](../Page/极权主义.md "wikilink")。極權主義這一名稱是由義大利哲學家[喬瓦尼·秦梯利](../Page/喬瓦尼·秦梯利.md "wikilink")（Giovanni
Gentile）最先使用的，用以描述[贝尼托·墨索里尼所建立的法西斯政治和社會系統](../Page/贝尼托·墨索里尼.md "wikilink")。[史達林](../Page/史達林.md "wikilink")、史達林主义下的修正主义苏联，毛泽东、毛泽东思想下的社会主义中国和[希特勒](../Page/希特勒.md "wikilink")、[德國](../Page/德國.md "wikilink")[納粹黨也同樣適用此名稱](../Page/納粹黨.md "wikilink")，在戰後此名稱成了對[法西斯主義和西方资本主义政權普遍的形容詞](../Page/法西斯主義.md "wikilink")。旧極權主義政權主張並且實行對整個社會的中央集權控制，以達成理想中繁榮和穩定的目標，而新的正在存在的极权主义则是通过大资本家财阀门对社会生产、舆论等方面的操纵，达到操纵国家政权以对整个社会进行集权控制。而对于社会中的普通民众，大资本家财阀们总是善于用在让民众做一道在他们的操纵下的“有极大限制的选择题”来愚弄民众，让民众认为自己擁有民主权力。極權主義通常宣稱其國家和人民正受到其他威脅，以替獨裁政權辯護。對極權主義的反抗成了自由和民主思想的重要部份，極權主義政權也常被描繪成試圖摧毀自由民主。

在意大利和德國，極端民族主義的政府連結了國家與大企業間的關係，並且宣傳自身國家民族在文化和種族上的優秀，而侵略他國將能使他們獲得應得的"陽光之土地"。在這些極權主義國家裡，宣傳機器通常宣稱民主是軟弱而無法達成大成就的，只有非凡的領導人才能帶來他們必要的紀律。

極權主義的崛起成了自由主義的反照面，許多自由主義者開始分析極權主義及其追隨者的信仰和理論，他們最後推論出，極權主義能崛起的原因在於人們身處太過惡劣的環境，因此轉向支持極權主義以尋求解決辦法。由於這樣的結論，一些人開始認為國家應該有保護經濟情況的職責，如同[以赛亚·伯林所說的](../Page/以赛亚·伯林.md "wikilink")：“狼群的自由就意味著羊群的死亡”，也因此越來越多自由主義者開始主張政府應該扮演穩定經濟發展的角色。

其他支持自由市場的古典自由主義者，則主張極權主義的崛起正是因為越來越多政府-{管制}-造成的。哈耶克在他的著作《[通往奴役之路](../Page/通往奴役之路.md "wikilink")》一書中，主張極權主義獨裁者的崛起是由於政府對市場進行了太多干預和管制，造成政治和公民自由的喪失而導致的。哈耶克也對英國和美國因為受到凱恩斯學派影響而建立的經濟控制制度提出警告，認為那將會導致相同的極權主義政府產生—而那正是凱恩斯學派所極力避免的。哈耶克認為極權主義政權如法西斯、納粹、和共產主義都是同樣的極權主義流派；因為這些政權都試著抹滅經濟的自由。對哈耶克而言他認為抹滅經濟的自由即代表抹滅政治的自由，因此他相信納粹和共產主義間的差別僅僅是名稱上的不同罷了。

弗里德里克·哈耶克和米爾頓·佛利民主張，經濟上的自由是公民和政治自由所不可或缺的要件。哈耶克認為，只要政府試著以政策來控制個人的自由（如杜威、凱恩斯和羅斯福主張的政策），相同的極權主義後果也可能發生在英國（或任何其他地方）。

## 古典自由主義和新古典主義經濟學

[奧地利經濟學派和](../Page/奧地利經濟學派.md "wikilink")[芝加哥經濟學派的擁護者](../Page/芝加哥經濟學派.md "wikilink")，如[米爾頓·佛利民](../Page/米爾頓·佛利民.md "wikilink")、[路德維希·馮·米塞斯和](../Page/路德維希·馮·米塞斯.md "wikilink")[弗里德里克·哈耶克對於現代自由主義的反駁](../Page/弗里德里克·哈耶克.md "wikilink")，即代表了古典自由主義的持續發展—也就是所謂的[新古典主义经济学](../Page/新古典主义经济学.md "wikilink")\[12\]
\[13\] 。佛利民說：

也因此新古典自由主義者認為他們才是真的古典自由主義的繼承人。舉例而言，哈耶克主張他並不是所謂的[保守主義](../Page/保守主義者.md "wikilink")，反而他才是真的自由主義者，而那些新自由主義者只是冒充者，他也因此一直拒絕放棄這種稱呼。

[约瑟夫·熊彼特則指出](../Page/约瑟夫·熊彼特.md "wikilink")：「最重要的是，如果這不是一場預謀，那麼私人企業制度的敵人們或許會覺得他們盜用了這一稱呼是相當明智的舉動」，意味著現代自由主義者們「偷走了」這一詞、並且還將這一詞冠上與之原義完全相反的解釋。

### 對新古典主義經濟學的批評

不過，有些人反對新古典主義經濟學的主張，仍稱之為「右翼經濟的自由主義者」、「自由保守主義」或直接稱之「右翼」，並認為他們自行增添其他稱呼的做法是無視於古典自由主義在政治面的部分，而且他們只專注於[斯密和](../Page/亚当·斯密.md "wikilink")[李嘉圖的經濟學說](../Page/大卫·李嘉图.md "wikilink")。\[14\]除此之外，他們認為哈耶克對於古典自由主義的看法相當奇怪："他忽略了其他更早的傑出思想家例如[洛克和](../Page/约翰·洛克.md "wikilink")[密爾](../Page/约翰·斯图尔特·密尔.md "wikilink")"\[15\]。

## 古典自由主義與自由意志主義

[自由意志主義者通常認為](../Page/自由意志主義.md "wikilink")「古典自由主義」和「自由意志主義」兩詞是可以互換的。例如美國的[卡托研究所認為古典自由主義](../Page/卡托研究所.md "wikilink")、自由主義和自由意志主義三者都是源於同一意識形態組群\[16\]。卡托研究所更喜歡自稱為「自由主義者」，因其自認為他們才是正當的自由主義繼承者。自由意志主義確實與古典自由主義有非常多相似處，包括哲學、政治、經濟方面，同樣主張自由放任的政府、自由市場、和個人的自由。古典自由主義一直主張為了保護個人的自由，必須盡量限制政府的權力。自由意志主義政黨則進一步主張對政府權力更多的限制。

比薩大學政治學系的教授Raimondo
Cubeddu就說：「我們很難分辨『自由意志主義』和『古典自由主義』之間到底有何差別。這兩種稱呼在用以形容那些「自由意志主義」的「[小政府主義](../Page/小政府主義.md "wikilink")」流派時幾乎是可以替換的：也就是用以稱呼那些認同[洛克和](../Page/约翰·洛克.md "wikilink")[羅伯特·諾齊克](../Page/羅伯特·諾齊克.md "wikilink")，認為國家的存在目的就是為了保護個人財產的學派。」\[17\]

自由意志主義認為他們繼承了古典自由主義。不過，一些人仍然認為兩者存在著分離、甚至互相衝突的理論。美國大法官[撒母耳·佛里曼·米勒](../Page/撒母耳·佛里曼·米勒.md "wikilink")（Samuel
Freeman
Miller）便認為：「自由意志主義只是表面上類似自由主義。自由意志主義者拒絕了自由主義所必要的制度。正確的說，自由意志主義假冒了一種觀點，而那種觀點正是自由主義依據歷史角度所反對的—那就是認為個人的政治權利處於封建制度之下。如同封建主義者，自由意志主義者構想出一種架構在個人契約連結上的政治權力。而自由主義理念所不可或缺的，便是主張政治權力乃是公眾的權力，應該公平的為公眾的利益所服務。」\[18\]

強調兩者間存在差異的人則主張，自由意志主義和古典自由主義間有著根本的矛盾，因自由主義裡為了檢查和平衡而使用的手段，與自由意志主義所支持的完全解除經濟管制有著衝突。\[19\]

## 參考文獻

## 參見

  - [資本主義](../Page/資本主義.md "wikilink")
  - [自由意志主義](../Page/自由意志主義.md "wikilink")
  - [自由放任](../Page/自由放任.md "wikilink")
  - [自由市場](../Page/自由市場.md "wikilink")
  - [私有化](../Page/私有化.md "wikilink")
  - [自由化](../Page/自由化.md "wikilink")
  - [自由主義](../Page/自由主義.md "wikilink")

{{-}}

[he:ליברליזם קלאסי](../Page/he:ליברליזם_קלאסי.md "wikilink")

[Category:主义](../Category/主义.md "wikilink")
[Category:自由主义](../Category/自由主义.md "wikilink")
[Category:自由意志主義](../Category/自由意志主義.md "wikilink")

1.  Vallentyne, Peter. [*Libertarianism and the
    State*](http://www.missouri.edu/~klinechair/Vita_Revised.htm#Articles)
    , Social Philosophy and Policy, 24 (forthcoming, 2007).
2.  www.cato.org/about/label.html
3.  Sturgis, Amy H. [*The Rise, Decline, and Reemergence of Classical
    Liberalism*](http://www.belmont.edu/lockesmith/essay.html) ,
    Lockesmith Institute, 1994.
4.  Heywood, A. (1998) Political Ideologies: An Introduction, Macmillan
    Press page 27
5.  Ryan, Alan. *Liberalism*. A Companion to Contemprary Political
    Philosophy, editors Goodin, Robert E. and Pettit, Philip. Blackwell
    Publishing, 1995, p.293.
6.  Madison, James. Federalist Paper no. 10, 1787
7.  Drilane, Robert and Parkinson, Gary. [*Online Dictionary of the
    Social
    Sciences*](http://bitbucket.icaap.org/dict.pl?term=CLASSICAL%20LIBERALISM).
8.  Jefferson, Thomas. Letter to Isaac H. Tiffany, 1819
9.  Duncan-Aimone, Katherine and Evans, Mark, ''Edinburgh Companion to
    Contemporary Liberalism: Evidence and Experience, ISBN
    1-57958-339-3, Routledge (UK), 2001, p. 55
10. Hayek, F. A. *The Constitution of Liberty* (London: Routledge,
    1976), pp. 55-6.
11. Hayek, F. A., *The Fatal Conceit: The Errors of Socialism*
    (University of Chicago Press, 1991), p. 110.
12. Kohl, B. and Warner, M., *Scales of Neoliberalism* International
    Journal of Urban and Regional Research Volume 28 (2004) pg1
13. Heywood, A. (1998) *Political Ideologies: An Introduction* Macmillan
    Press pg93
14. Lessnoff, M. H. (1999) *Political Philosophers of the Twentieth
    Century* Blackwell; Heywood, A. (1998) *Political Ideologies: An
    Introduction* Macmillan Press pg155; Festenstein, M. and Kenny, M.
    (2005) *Political Ideologies* Oxford University Press
15. Gamble, A. (1996) "Hayek: The Iron Cage of Liberty" Blackwell
    Publishers pg 106
16. <http://www.cato.org/about/about.html>
17. <http://www.univ.trieste.it/~etica/2003_2/>
18. Freeman, S., Illiberal Libertarians: Why Libertarianism Is Not a
    Liberal View Philosophy & Public Affairs Volume 30 (2001) pg3
19. Haworth, A. (1994) *Anti-libertarianism. Markets, philosophy and
    Myth* Routledge pg 27