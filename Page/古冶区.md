**古冶区**是[唐山市七区之一](../Page/唐山市.md "wikilink")，地处唐山市中东部地区，东临[滦县](../Page/滦县.md "wikilink")，西接[开平区](../Page/开平区.md "wikilink")，人口36万。是一个产煤区，著名的[开滦公司下设五个矿在本区](../Page/开滦公司.md "wikilink")。经济以煤炭为主。

## 行政区划

下辖5个[街道办事处](../Page/街道办事处.md "wikilink")、2个[镇](../Page/行政建制镇.md "wikilink")、3个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 人口

古冶区总人口36.2万，其中：城市人口27.1万，农业人口9.1万\[1\]。2011年，古冶区人口出生率5.59‰，政策生育率98.14%，性别比104，已连续12年实现人口负增长\[2\]。

### 民族

有汉、回、满、蒙古、土家、朝鲜、藏、壮、苗、彝、布依、维吾尔、侗、达斡尔、锡伯、俄罗斯、佤、畲等18个民族，汉族人口占总人口的98.4%。

## 宗教

### 庙

多为明清时所建，有文字可考者共有13处\[3\]：

  - [真武庙](../Page/真武庙.md "wikilink")
  - [三官庙](../Page/三官庙.md "wikilink")
  - [观音庙](../Page/观音庙.md "wikilink")
  - [蚕姑庙](../Page/蚕姑庙.md "wikilink")
  - [娘娘庙](../Page/娘娘庙.md "wikilink")
  - [关帝庙](../Page/关帝庙.md "wikilink")

### 佛教

  - [多宝佛塔](../Page/多宝佛塔.md "wikilink")\[4\]

### 伊斯兰教

  - [古冶清真寺](../Page/古冶清真寺.md "wikilink")，始建于1876年\[5\]\[6\]。

### 基督教

  - [唐家庄基督教堂](../Page/唐家庄基督教堂.md "wikilink")\[7\]

## 当地名人

  - [岳美中](../Page/岳美中.md "wikilink")，中医学家
  - [张广厚](../Page/张广厚.md "wikilink")，数学家
  - [节振国](../Page/节振国.md "wikilink")

## 参考文献

## 官方網站

  - [唐山市古冶區人民政府](http://www.guye.gov.cn/)

[古冶区](../Category/古冶区.md "wikilink")
[区](../Category/唐山区县市.md "wikilink")
[唐山](../Category/河北市辖区.md "wikilink")

1.
2.
3.
4.
5.
6.
7.