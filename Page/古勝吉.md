**古國謙**（），為[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，曾經效力於[中華職棒](../Page/中華職棒.md "wikilink")[時報鷹](../Page/時報鷹.md "wikilink")，後來因為[職棒簽賭案而遭到](../Page/職棒簽賭案.md "wikilink")[中華職棒終身禁賽](../Page/中華職棒.md "wikilink")。

## 經歷

  - [高雄市信義國小少棒隊](../Page/高雄市.md "wikilink")
  - [屏東縣美和中學青少棒隊](../Page/屏東縣.md "wikilink")
  - [屏東縣美和中學青棒隊](../Page/屏東縣.md "wikilink")
  - [合作金庫棒球隊](../Page/合作金庫.md "wikilink")
  - 陸光棒球隊
  - [中華職棒](../Page/中華職棒.md "wikilink")[時報鷹隊](../Page/時報鷹.md "wikilink")
  - [高雄市前金國中青少棒隊總教練](../Page/高雄市.md "wikilink")

## 職棒生涯成績

| 年度    | 球隊                               | 出賽  | 打數   | 安打  | 全壘打 | 打點  | 盜壘 | 四死 | 三振  | 壘打數 | 雙殺打 | 打擊率   |
| ----- | -------------------------------- | --- | ---- | --- | --- | --- | -- | -- | --- | --- | --- | ----- |
| 1993年 | [時報鷹](../Page/時報鷹.md "wikilink") | 86  | 307  | 79  | 3   | 28  | 20 | 11 | 25  | 103 | 7   | 0.257 |
| 1994年 | [時報鷹](../Page/時報鷹.md "wikilink") | 65  | 221  | 53  | 1   | 30  | 10 | 12 | 29  | 70  | 4   | 0.240 |
| 1995年 | [時報鷹](../Page/時報鷹.md "wikilink") | 84  | 273  | 78  | 0   | 26  | 16 | 16 | 32  | 100 | 1   | 0.286 |
| 1996年 | [時報鷹](../Page/時報鷹.md "wikilink") | 83  | 296  | 43  | 0   | 31  | 16 | 21 | 43  | 95  | 2   | 0.247 |
| 合計    | 4年                               | 315 | 1097 | 283 | 4   | 133 | 6  | 60 | 129 | 368 | 14  | 0.258 |

## 特殊事蹟

## 外部連結

[Shengji](../Category/古姓.md "wikilink")
[Category:台灣棒球選手](../Category/台灣棒球選手.md "wikilink")
[Category:時報鷹隊球員](../Category/時報鷹隊球員.md "wikilink")
[Category:中華成棒隊球員](../Category/中華成棒隊球員.md "wikilink")
[Category:高雄市人](../Category/高雄市人.md "wikilink")
[Category:美和中學青少棒隊](../Category/美和中學青少棒隊.md "wikilink")
[Category:台灣奧林匹克運動會銀牌得主](../Category/台灣奧林匹克運動會銀牌得主.md "wikilink")
[Category:1992年夏季奧林匹克運動會獎牌得主](../Category/1992年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:中華職棒終身禁賽名單](../Category/中華職棒終身禁賽名單.md "wikilink")