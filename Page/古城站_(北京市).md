**古城站**是[北京地铁的一个车站](../Page/北京地铁.md "wikilink")，编号是104，在[1号线上](../Page/北京地铁1号线.md "wikilink")。本站原名**古城路站**\[1\]，后更改为现在的站名。

## 位置

这个站位于[古城小街和](../Page/古城小街.md "wikilink")[石景山路交汇处](../Page/石景山路.md "wikilink")。

## 出口

这个地铁站一共有4个出口。

<table>
<thead>
<tr class="header">
<th><p>出口編號</p></th>
<th><p>建議前往的目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>星座商厦</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>北京市古城中学</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>长庚医院、首钢篮球中心体育馆</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>长安家园</p></td>
</tr>
</tbody>
</table>

## 车站结构

[North_concourse_entrance_of_Gucheng_Station_(20171225154811).jpg](https://zh.wikipedia.org/wiki/File:North_concourse_entrance_of_Gucheng_Station_\(20171225154811\).jpg "fig:North_concourse_entrance_of_Gucheng_Station_(20171225154811).jpg")
该站于两个行驶方向各设一个大堂，两个大堂不相通，因此乘客必须出站才能换向。

<table>
<tbody>
<tr class="odd">
<td><p><strong>地下一层</strong></p></td>
<td><p>大堂</p></td>
<td><p>A、B出入口、售票机、问询处</p></td>
</tr>
<tr class="even">
<td><center>
<p><small><a href="../Page/側式月台.md" title="wikilink">側式月台</a>，右邊車門將會開啟</small></p>
</center></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>列車往<a href="../Page/苹果园站.md" title="wikilink">苹果园方向</a> <small>（<a href="../Page/苹果园站.md" title="wikilink">苹果园</a>）</small></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>列車往<a href="../Page/四惠东站.md" title="wikilink">四惠东方向</a> <small>（<a href="../Page/八角游乐园站.md" title="wikilink">八角游乐园</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p><small><a href="../Page/側式月台.md" title="wikilink">側式月台</a>，右邊車門將會開啟</small></p>
</center></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>大堂</p></td>
<td><p>C、D出入口、售票机、问询处</p></td>
<td></td>
</tr>
</tbody>
</table>

## 临近车站

## 参考

[Category:石景山区地铁车站](../Category/石景山区地铁车站.md "wikilink")
[Category:1971年启用的铁路车站](../Category/1971年启用的铁路车站.md "wikilink")

1.