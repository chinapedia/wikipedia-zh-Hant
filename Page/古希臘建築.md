[Stoa_in_Athens.jpg](https://zh.wikipedia.org/wiki/File:Stoa_in_Athens.jpg "fig:Stoa_in_Athens.jpg")[柱廊](../Page/柱廊.md "wikilink")\]\]
從[邁錫尼時期](../Page/邁錫尼文明.md "wikilink")（約在西元前1200年）到西元前7世紀間的希臘建築已不復存在，而當時的城市的生活和繁榮剛好足夠承擔公共建築。但是許多在殖民時期（西元前8－6世紀）的古希臘建築物都是用[木材或](../Page/木材.md "wikilink")[泥磚或者](../Page/磚.md "wikilink")[黏土造的](../Page/黏土.md "wikilink")，除了少數的地基幾乎沒有任何建築被留到現在，對於這些剛萌芽的建築也幾乎沒有文獻記載和描述。

一些常見的古希臘建築材料像是[木材](../Page/木材.md "wikilink")，用來支撐和當作屋梁；特別是許多民宅，將未燒結的磚用來築牆；而[石灰岩和](../Page/石灰岩.md "wikilink")[大理石被用來做寺廟和公共建築的](../Page/大理石.md "wikilink")[柱子](../Page/柱子.md "wikilink")，牆壁和上半部份的建築；[陶瓦](../Page/陶瓦.md "wikilink")（terracotta）用做屋瓦和裝飾；而金屬，特別是[青銅](../Page/青銅.md "wikilink")，被用在裝飾的細節部份。古風和古典時期的[建築師使用這些材料來建造宗教](../Page/建築師.md "wikilink")、公共、民生、喪葬或休憩，這五種類型的建築。

## 歷史

大約在西元前600年，[奧林匹亞古老的](../Page/奧林匹亞.md "wikilink")[赫拉神廟的木造](../Page/赫拉神廟.md "wikilink")[柱子經歷了稱為](../Page/柱子.md "wikilink")[石化](../Page/石化.md "wikilink")（petrification）的材料變革，所有的柱子都被換成[石造的](../Page/岩石.md "wikilink")。神廟的其他部件也很大程度的被石化。

大部分我們對於古希臘建築的了解都是來自於[古風時期](../Page/古風時期.md "wikilink")（Archaic
period）晚期（西元前550－500年）、[伯里克利時代](../Page/伯里克利時代.md "wikilink")（Periclean
age，西元前450－430年）和[古典時期](../Page/希臘古典時期.md "wikilink")（Classical
period，西元前430－400年）之間。[希臘化時代和](../Page/希臘化時代.md "wikilink")[古羅馬時期](../Page/古羅馬.md "wikilink")（[古羅馬建築大量複製自希臘](../Page/古羅馬建築.md "wikilink")），和後來的文獻來源，例如[維特魯威](../Page/馬爾庫斯·維特魯威·波利奧.md "wikilink")（Vitruvius，西元1世紀）。結果讓建築強烈傾向[神廟](../Page/神廟.md "wikilink")─唯一留存下來一些數量的建築物。

就如希臘的[繪畫和](../Page/繪畫.md "wikilink")[雕塑](../Page/雕塑.md "wikilink")，前半[古典時期的希臘](../Page/古典時期.md "wikilink")[建築在現代看來並不是](../Page/建築.md "wikilink")[為藝術而藝術](../Page/為藝術而藝術.md "wikilink")（art
for art's
sake）的行為。建築師被政府或是富裕的私人客戶僱請。建築師同時也是建築承包商，當時還沒有區分這兩個職位。建築師必須設計建築物，並且雇用工人和工匠建造，還要負責預算並且在一定時間內完成。相對於現在的公共建築師，當時的建築師並不享有任何崇高的地位。甚至在5世紀前的建築師的名字都不為人知。例如設計[帕德嫩神廟的](../Page/帕德嫩神廟.md "wikilink")[伊克提諾斯](../Page/伊克提諾斯.md "wikilink")（Iktinos），在今日可能會被當做像是天才一樣的來看待，但是在他終其一生被對待不過像個非常有價值的工匠罷了。

## 神廟構造與風格

[TempleHephaistosAthens.jpg](https://zh.wikipedia.org/wiki/File:TempleHephaistosAthens.jpg "fig:TempleHephaistosAthens.jpg")[赫菲斯托斯神廟](../Page/赫菲斯托斯神廟.md "wikilink")：西面
|200px\]\]

像是在[雅典的](../Page/雅典.md "wikilink")[帕德嫩神廟和](../Page/帕德嫩神廟.md "wikilink")[赫菲斯托斯神廟](../Page/赫菲斯托斯神廟.md "wikilink")，與在[阿格里真托的](../Page/阿格里真托.md "wikilink")[賽利農特](../Page/賽利農特.md "wikilink")（Selinus）神廟群和聖所，都是為人所知留存的標準類型的希臘公共建築物的例子。大部分的建築物都是矩形的，並且用[石灰岩或是](../Page/石灰岩.md "wikilink")[凝灰岩建造的](../Page/凝灰岩.md "wikilink")。這類的石材在希臘相當豐富，會先被切成大石塊並且稍做修飾。[大理石在希臘是相當昂貴的建材](../Page/大理石.md "wikilink")：高品質的大理石必須從[阿提卡的](../Page/阿提卡.md "wikilink")[潘提里山](../Page/潘提里.md "wikilink")（Pentelicus）或一些島嶼例如[帕羅斯島](../Page/帕羅斯島.md "wikilink")（Paros）取得，而運送這些大石塊並不容易。大部分的大理石都用做雕塑裝飾，不會用來建築，除非是在古典時期的偉大建築，像是[帕德嫩神廟](../Page/帕德嫩神廟.md "wikilink")。

例如[帕德嫩神廟的矩形基底四面圍繞的列柱稱為](../Page/帕德嫩神廟.md "wikilink")[圍柱列](../Page/列柱中庭.md "wikilink")（peristyle）；而只有前後，兩旁沒有列柱的建築稱為（amphiprostyle），像是[雅典娜勝利女神殿](../Page/雅典娜勝利女神殿.md "wikilink")；有些建築物前面突出的柱列稱為（prostyle）；而有些從建築物正面延伸到中庭的列柱稱為[門廊](../Page/門廊.md "wikilink")（pronaos）。希臘人會在屋頂上木頭橫樑，並且鋪上[陶瓦](../Page/陶瓦.md "wikilink")，或者偶爾鋪上[大理石板](../Page/大理石.md "wikilink")。他們知道石[拱的原理但甚少使用](../Page/拱.md "wikilink")，也沒有將[圓頂蓋在他們的建築物上](../Page/圓頂.md "wikilink")；這些東西後來留給了羅馬人使用。

低矮的山形屋頂會在建築物的末端留下三角形的剖面，[山形牆](../Page/山形牆.md "wikilink")（pediment）通常會用雕塑裝飾。在屋頂與列柱[楣樑間形成部份稱為](../Page/楣樑.md "wikilink")[檐部](../Page/檐部.md "wikilink")（entablature）。過梁外露的部份提供的雕塑的空間叫做[帶飾](../Page/帶飾.md "wikilink")（frieze）。帶飾的裝飾由[排檔間飾和](../Page/排檔間飾.md "wikilink")[三槽線飾帶構成](../Page/三槽線飾帶.md "wikilink")。沒有任何古希臘建築上的雕塑還完好如初，但是在現今許多仿造古希臘的建築上還看的到這種裝飾。

## 希臘公共建築

[神廟算的上是最普遍而且為人所知的希臘公共建築](../Page/希臘神廟.md "wikilink")。但神廟的功能和現今的教堂不同，祭壇通常就直接設在神廟後方的戶外。神廟就像個儲藏室一樣，放置著許多和求神問卜有關的寶物還有例如雕像、頭盔和武器這些的（votive
offering）。而神廟內的房間[內殿](../Page/內殿.md "wikilink")（cella）則主要當作保險庫或是儲藏庫，通常沿著列柱排列。

其他希臘人使用的建築結構包括[圓頂和](../Page/圓頂.md "wikilink")[圓形建築](../Page/圓形建築.md "wikilink")，例如位於[德爾斐供奉](../Page/德爾斐.md "wikilink")[雅典娜的](../Page/雅典娜.md "wikilink")[泰奧多勒斯圓形建築](../Page/泰奧多勒斯圓形建築.md "wikilink")（Tholos
of
Theodorus）；[山門](../Page/山門.md "wikilink")（propylon）則構成了神殿的出口（目前保存最完善的例子在[雅典衛城](../Page/雅典衛城.md "wikilink")）；噴泉屋是婦女在他們的花瓶裝水的地方。

## 参见

  - [古希臘神廟](../Page/古希臘神廟.md "wikilink")
  - [拜占庭式建築](../Page/拜占庭式建築.md "wikilink")
  - [古希腊科技](../Page/古希腊科技.md "wikilink")

## 注釋

<div class="references-small">

<references />

</div>

## 參考資料

  - *Greek architecture* 1965年版[大英百科全書](../Page/大英百科全書.md "wikilink")
  - *Greece: From Mycenae to the Parthenon*, Henri Stierlin,
    [TASCHEN](../Page/Taschen.md "wikilink"), 2004

**Re-invention of roof tiles**

  - Marilyn Y. Goldberg, “Greek Temples and Chinese Roofs,” *American
    Journal of Archaeology*, Vol. 87, No. 3. (Jul., 1983), pp. 305-310
  - Orjan Wikander, “Archaic Roof Tiles the First Generations,”
    *Hesperia*, Vol. 59, No. 1. (Jan. - Mar., 1990), pp. 285-290
  - William Rostoker; Elizabeth Gebhard, “The Reproduction of Rooftiles
    for the Archaic Temple of Poseidon at Isthmia, Greece,” *Journal of
    Field Archaeology*, Vol. 8, No. 2. (Summer, 1981), pp. 211-2

[Category:古希臘](../Category/古希臘.md "wikilink")
[Category:建筑史](../Category/建筑史.md "wikilink")