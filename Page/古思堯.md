[Koo_Sze_Yiu_em_23_Nov_2008_em_Macau.jpg](https://zh.wikipedia.org/wiki/File:Koo_Sze_Yiu_em_23_Nov_2008_em_Macau.jpg "fig:Koo_Sze_Yiu_em_23_Nov_2008_em_Macau.jpg")立法\]\]
**古思堯**（**Koo Sze
Yiu**，），外號**長鬚**，原籍[廣東](../Page/廣東.md "wikilink")[中山](../Page/中山市.md "wikilink")[石歧](../Page/石歧.md "wikilink")，
[香港街頭](../Page/香港.md "wikilink")[示威活躍份子](../Page/示威.md "wikilink")，[社會民主連線成員](../Page/社會民主連線.md "wikilink")。

## 生平

他自稱前半生是[毛派信徒](../Page/毛派.md "wikilink")，且是[澳門工會聯合總會核心成員](../Page/澳門工會聯合總會.md "wikilink")，並在[一二三事件中衝擊葡警](../Page/一二三事件.md "wikilink")。1989年[天安門事件之後開始成為香港示威積極份子](../Page/六四事件.md "wikilink")，與外號長毛的[梁國雄為伍](../Page/梁國雄.md "wikilink")。其間因參與[社會運動而多次被檢控](../Page/社會運動.md "wikilink")，甚至入獄。

古思堯過去經常與梁國雄一起於[遊行時抬](../Page/遊行.md "wikilink")[棺材示威而為香港人所認知](../Page/棺材.md "wikilink")，然而他卻並非[四五行動成員之一](../Page/四五行動.md "wikilink")。\[1\]

2007年5月15日曾在《[蘋果日報](../Page/香港蘋果日報.md "wikilink")》訪問中稱：「香港人太斯文，遊行好像嘉年華，應該像韓農那樣。」（指[2005年香港反對世貿遊行衝突組織到香港衝擊](../Page/2005年香港反對世貿遊行衝突.md "wikilink")[香港會議展覽中心的](../Page/香港會議展覽中心.md "wikilink")[韓國農業協會之示威者](../Page/韓國農業協會.md "wikilink")）

古思堯亦與[南方民主同盟的](../Page/南方民主同盟.md "wikilink")[龍緯汶友善](../Page/龍緯汶.md "wikilink")，常出席有關的活動。古思堯謂希望[香港人要起來](../Page/香港人.md "wikilink")，反對[中共](../Page/中共.md "wikilink")，結束[一党專政](../Page/一党專政.md "wikilink")。

2012年8月15日，古思堯與其它香港保釣行動委員會成員一同登上[釣魚島](../Page/釣魚島.md "wikilink")。\[2\]8月17日，團隊抵港開簡單記招時，在電視台直播期間爆出「喂喂喂，企開啲啦，[屌你老母](../Page/屌.md "wikilink")」\[3\]的粵語[粗口](../Page/粗口.md "wikilink")，全句完整地在免費電視中清晰播出，引起市民嘩然\[4\]。

古思堯的妻兒在大陆，會於2012年8月19日到[香港探望他](../Page/香港.md "wikilink")\[5\]。他有一子一女。

## 早年生活

古思堯早年曾在[澳門船廠學師及參與當地](../Page/澳門.md "wikilink")[左派陣營](../Page/左派和右派.md "wikilink")，在1966年「[一二·三事件](../Page/一二·三事件.md "wikilink")」曾衝擊[澳葡政府](../Page/澳葡政府.md "wikilink")，包括衝入市政廳，把銅像、報告板等砸毀，並把文件和傢俬由二樓拋到街上。後被戰友出賣，輾轉逃到香港定居\[6\]。

## 入獄事件

1998年，與[曾健成](../Page/曾健成.md "wikilink")（阿牛）、[梁國雄](../Page/梁國雄.md "wikilink")、[劉山青趁時任](../Page/劉山青.md "wikilink")[國家主席的](../Page/中華人民共和國主席.md "wikilink")[江澤民訪港](../Page/江澤民訪港.md "wikilink")，到[香港會議展覽中心門外示威](../Page/香港會議展覽中心.md "wikilink")、燒[棺材](../Page/棺材.md "wikilink")。結果他們被裁定在公眾地方擾亂秩序，被罰款3,000元。

2000年，他更因為兩度在[立法會會議期間示威被檢控](../Page/香港立法會.md "wikilink")，判入獄14日。

2008年12月，古思堯在一次[領匯示威中被指襲擊立會保安](../Page/領匯.md "wikilink")。2009年6月，香港法院判罪成，[罰款](../Page/罰款.md "wikilink")2千港元或[坐牢](../Page/坐牢.md "wikilink")7天。結果，古思堯選擇坐牢。\[7\]

2013年2月18日，他先後在抗議中國內地民運人士[李旺陽](../Page/李旺陽.md "wikilink")「被自殺」及在[元旦遊行中展示塗污的](../Page/元旦.md "wikilink")[中國國旗](../Page/中國國旗.md "wikilink")、[香港區旗及焚燒國旗](../Page/香港區旗.md "wikilink")，被裁定4項侮辱國旗及區旗罪成立，判囚9個月。他當日在法院外戴上寫有「我非常榮幸第三次入獄」的綵帶。\[8\]

2018年3月27日，古思堯在[東區裁判法院被裁定涉於](../Page/東區裁判法院.md "wikilink")2017年7月15日悼念[劉曉波的燭光遊行及](../Page/劉曉波.md "wikilink")10月1日國慶遊行時倒置及塗污中國國旗，以及在2018年1月1日元旦遊行中塗污香港區旗，被裁定2項侮辱國旗罪及1項侮辱區旗罪罪成，每項控罪各判監2個月，同期執行，合共2個月監禁。當裁判官問古會否自行求情時，古回答：「求情不必要的啦，法律歸法律，政治歸政治」，又稱「任何後果都無所謂」，會「屢敗屢戰，坐監完再抗爭」，而他稱侮辱國旗屬他的個人抗爭行為以示不滿中共，但不希望戰友模仿。古思堯在庭外頸掛「打倒中國新帝制」和「無悔第六次坐監」字句，並祝賀自己再入獄及與支持者舉杯暢飲，在進入囚室時高叫「民主萬歲」、「人權萬歲」和「反對中國共產黨」口號。\[9\]\[10\]\[11\]

## 參見

  - [古思堯及另一人對香港特別行政區行政長官案](../Page/古思堯及另一人對香港特別行政區行政長官案.md "wikilink")

## 參考來源

### 內文腳註

### 外部連結

  - [希望之聲國際廣播電台：香港民主人士玷污国旗被判入狱9个月](http://big5.soundofhope.org/node/314663)
  - [希望之聲國際廣播電台：港民主人士古思堯與大陸妻子團聚](http://big5.soundofhope.org/node/179823)
  - [希望之聲國際廣播電台：港民間捍衛法輪功遊行
    抗中共禍港](http://big5.soundofhope.org/node/305839)
  - [蘋果日報：古思堯專訪](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20070515&sec_id=4104&subsec_id=11867&art_id=7103032)

[Category:社會民主連線成員](../Category/社會民主連線成員.md "wikilink")
[Category:香港社會運動人士](../Category/香港社會運動人士.md "wikilink")
[Category:四五行動成員](../Category/四五行動成員.md "wikilink")
[Category:保釣人士](../Category/保釣人士.md "wikilink")
[Category:香港曾入獄者](../Category/香港曾入獄者.md "wikilink")
[S](../Category/古姓.md "wikilink")

1.  [《蘋果日報》「隔牆有耳」專欄：《棺材戰士憎人遲到》](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20070530&sec_id=4104&subsec_id=15333&art_id=7156170&cat_id=45&coln_id=20)

2.
3.
4.
5.  \[二零一二年八月十九日, 香港《明報》,『古思堯﹕決心「撞散船都要搶灘」』。\]
6.  [參與澳門騷亂反遭批鬥
    古思堯：堅決擁共變反共](http://hk.apple.nextmedia.com/news/art/20160523/19623615)
7.  [明報:古思堯襲擊罪成
    寧坐監不交罰款，案件編號﹕ESCC2501/09](http://news.sina.com.hk/cgi-bin/nw/show.cgi/2/1/1/1183950/1.html)
8.  [明報:燒國旗辱區旗
    古思堯囚9月](http://finance.sina.com.hk/news/-1-5633561/1.html)
9.
10.
11.