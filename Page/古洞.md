[Kwu_Tung_2017.jpg](https://zh.wikipedia.org/wiki/File:Kwu_Tung_2017.jpg "fig:Kwu_Tung_2017.jpg")，右下方大型屋苑爲[天巒](../Page/天巒.md "wikilink")，兩者之間的建築物爲[古洞街市購物中心](../Page/古洞街市購物中心.md "wikilink")，將照片分割爲上下兩部分的馬路是[粉嶺公路和](../Page/粉嶺公路.md "wikilink")[青山公路](../Page/青山公路.md "wikilink")，歐意花園上方泥地爲已清拆多年的[意大利農場村舊址](../Page/意大利農場村.md "wikilink")，右側整齊排列的房屋爲[石仔嶺花園](../Page/石仔嶺花園.md "wikilink")，其上方大片寮屋群落爲[古洞東方區](../Page/古洞東方區.md "wikilink")，其後方小山爲[大石磨](../Page/大石磨.md "wikilink")，大石磨後方的大片高樓建築爲[深圳市](../Page/深圳市.md "wikilink")，在照片右半部蜿蜒的河流爲[雙魚河](../Page/雙魚河.md "wikilink")。\]\]
[Kwu_tung_village_office.JPG](https://zh.wikipedia.org/wiki/File:Kwu_tung_village_office.JPG "fig:Kwu_tung_village_office.JPG")
**古洞**（，舊稱：\[1\]），[戰前曾有別稱](../Page/二戰.md "wikilink")**七鄉**（\[2\]），地處[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[北區](../Page/北區_\(香港\).md "wikilink")[上水西部](../Page/上水.md "wikilink")，[落馬洲之東南](../Page/落馬洲.md "wikilink")，爲[雙魚河流經的一處地方](../Page/雙魚河.md "wikilink")，具體位置爲雙魚河的西岸，由於該段[青山公路以](../Page/青山公路.md "wikilink")「古洞段」命名，故部分人甚至政府部門會將「古洞」視爲覆蓋[金錢](../Page/金錢_\(香港\).md "wikilink")、[坑頭](../Page/坑頭.md "wikilink")、[蕉徑等地的地區名稱](../Page/蕉徑.md "wikilink")\[3\]，但傳統上古洞的核心區僅指金錢和[河上鄉以西](../Page/河上鄉.md "wikilink")、[白石凹以東](../Page/白石凹.md "wikilink")、[馬草壟以南和](../Page/馬草壟.md "wikilink")[麒麟山以北的範圍](../Page/麒麟山_\(香港\).md "wikilink")。

自1945年，古洞成為圍頭人（包括同操[粵語](../Page/粵語.md "wikilink")[莞寶片之](../Page/莞寶片.md "wikilink")[東莞人](../Page/東莞人.md "wikilink")）、[客家人與少數](../Page/客家人.md "wikilink")[潮州人之聚居地](../Page/潮州人.md "wikilink")，從內地南下的難民在古洞建立了多條寮屋村落，青山公路於古洞的一段更曾發展出規模接近[墟市的大馬路市場](../Page/墟市.md "wikilink")，但自1978年[粉嶺／上水新市鎮開始發展後](../Page/粉嶺／上水新市鎮.md "wikilink")，古洞的墟市活動逐漸式微，加上不斷有地產發展商收購土地，使古洞原來的村落人口不斷流失。古洞南近白石凹最早於1995年建成小型別墅項目[歐意花園](../Page/歐意花園.md "wikilink")，後於2009年由發展商利用青山公路以南至麒麟山的整片古洞南區域建成大型豪宅項目[天巒](../Page/天巒.md "wikilink")。至2010年代，香港特區政府提出發展[古洞北新市鎮](../Page/古洞北新市鎮.md "wikilink")，相關工程將於2018年展開。

## 名稱來源

史籍記載曰：古越蠻居處多以洞或峒為地名，《集韻》載云：蠻人所居曰峒，使古洞被相信曾有古越蠻人居住。\[4\]

## 地理

[HK_KuTungPublicOiWahSchool2.jpg](https://zh.wikipedia.org/wiki/File:HK_KuTungPublicOiWahSchool2.jpg "fig:HK_KuTungPublicOiWahSchool2.jpg")
古洞南北兩邊各有一座山，分別是[麒麟山與](../Page/麒麟山.md "wikilink")[大石磨](../Page/大石磨.md "wikilink")。

[麒麟山內有座名為](../Page/麒麟山.md "wikilink")[古洞水塘的小型](../Page/古洞水塘.md "wikilink")[農業用](../Page/農業.md "wikilink")[水庫](../Page/水庫.md "wikilink")，約有三個[北區公園一期](../Page/北區公園.md "wikilink")[人工湖的面積](../Page/人工湖.md "wikilink")，曾經發生溺斃或投塘自盡事件，可由[古洞街市](../Page/古洞街市.md "wikilink")[十字路口](../Page/十字路口.md "wikilink")（[九龍巴士76K線](../Page/九龍巴士76K線.md "wikilink")[巴士站](../Page/巴士站.md "wikilink")）進入[古洞南路前往](../Page/古洞南路.md "wikilink")。經過長瀝（長壢）村與雙魚河畔（[客家話](../Page/客家話.md "wikilink")：河唇），可從西北方觀望位於[麒麟山內的古洞水塘](../Page/麒麟山.md "wikilink")[水壩](../Page/水壩.md "wikilink")（天朗氣清時候，亦可從[上水市中心向西南方眺望](../Page/上水.md "wikilink")）。

自2003年[嚴重急性呼吸系統綜合症](../Page/嚴重急性呼吸系統綜合症.md "wikilink")（[SARS](../Page/SARS.md "wikilink")）疫災過後，麒麟山的古洞水塘水壩成為香港行山人士的旅程途經地點，可往來古洞街市、[錦田](../Page/錦田.md "wikilink")[八鄉](../Page/八鄉.md "wikilink")、[新田](../Page/新田.md "wikilink")[米埔](../Page/米埔.md "wikilink")、[河上鄉](../Page/河上鄉.md "wikilink")、[塱原濕地](../Page/塱原濕地.md "wikilink")、[蓬瀛仙館](../Page/蓬瀛仙館.md "wikilink")（[蓬瀛僊館](../Page/蓬瀛僊館.md "wikilink")）、[和合石](../Page/和合石.md "wikilink")、[林錦公路](../Page/林錦公路.md "wikilink")[白牛石](../Page/白牛石.md "wikilink")[嘉道理農場等地點](../Page/嘉道理農場.md "wikilink")，連繫多條縱橫交錯的行山路徑，組成有一定規模的山徑網絡。[大石磨則成為行山愛好者新興線性行程的途經地點](../Page/大石磨.md "wikilink")，可往來[落馬洲](../Page/落馬洲.md "wikilink")、[河上鄉](../Page/河上鄉.md "wikilink")、上水[華山](../Page/華山_\(香港\).md "wikilink")、[打鼓嶺等地](../Page/打鼓嶺.md "wikilink")。

## 住宅

[Kwu_Tung_South_2017.jpg](https://zh.wikipedia.org/wiki/File:Kwu_Tung_South_2017.jpg "fig:Kwu_Tung_South_2017.jpg")（前）和坑頸村（後）\]\]

### 鄉村村落

古洞一帶原來只有一些規模細小的村落，自1945年後建立多條非原居民[寮屋村落](../Page/寮屋.md "wikilink")，合稱「古洞村」；[粉嶺公路從中穿過分為南北兩區](../Page/粉嶺公路.md "wikilink")\[5\]：

  - [煙寮村](../Page/煙寮_\(古洞\).md "wikilink")（現稱「古洞煙寮區」）
  - [塘角村](../Page/塘角_\(古洞\).md "wikilink")（現稱「古洞塘角區」）
  - [聯和農場](../Page/聯和農場.md "wikilink")（現稱「古洞聯和區」）
  - [鳳崗村](../Page/鳳崗山.md "wikilink")（現稱「古洞鳳崗區」）
  - [東方農場](../Page/東方農場.md "wikilink")（現稱「古洞東方區」）
  - [石仔嶺村](../Page/石仔嶺.md "wikilink")
  - [意大利農場](../Page/意大利農場_\(香港\).md "wikilink")（於1970年代末完全清拆，因業權糾紛閒置多年）
  - [田心村](../Page/田心村_\(古洞\).md "wikilink")（已重建爲天巒）
  - [古洞坑村](../Page/古洞坑.md "wikilink")（已重建爲天巒）
  - [醫院前區村](../Page/醫院前區村.md "wikilink")（大部分重建爲天巒，僅餘少數村屋）
  - [公生農場](../Page/公生農場.md "wikilink")（已重建爲天巒）
  - [聯生農場](../Page/聯生農場.md "wikilink")（南半部大部分重建爲歐意花園，剩餘部分現稱爲「古洞聯生區」）
  - [坑頸村](../Page/坑頸村.md "wikilink")，有新界李氏原居民於一百年前由烏蛟騰村分支到現址聚居，另有大埔尾村第廿四卋李華進於戰後亦遷至此地。

### 私人建築

  - [歐意花園](../Page/歐意花園.md "wikilink")
  - [天巒](../Page/天巒.md "wikilink")
  - 何東後人申請建屋，供夾心階層居住（原址為[東英學圃](../Page/東英學圃.md "wikilink")）\[6\]

## 交通

  - [九龍巴士76K線](../Page/九龍巴士76K線.md "wikilink")
  - [公共小巴元朗至上水線](../Page/公共小巴元朗至上水線.md "wikilink")
  - [新界專線小巴51K線](../Page/新界專線小巴51K線.md "wikilink")
  - [新界專線小巴50K線](../Page/新界專線小巴50K線.md "wikilink")

### 主要道路

  - [古洞路](../Page/古洞路.md "wikilink")（由[坑頭](../Page/坑頭.md "wikilink")76K往[元朗方向](../Page/元朗.md "wikilink")[巴士站開始直至](../Page/巴士站.md "wikilink")[文天祥公園](../Page/文天祥公園.md "wikilink")[惇裕學校旁](../Page/惇裕學校.md "wikilink")）
  - [青山公路](../Page/青山公路.md "wikilink")－洲頭段及古洞段（[皇崗至落馬洲過境穿梭巴士站至](../Page/皇巴士.md "wikilink")[粉錦公路路口](../Page/粉錦公路.md "wikilink")）
  - [河上鄉路](../Page/河上鄉路.md "wikilink")（青山公路路口至古洞鳳崗區）
  - [馬草壟路](../Page/馬草壟路.md "wikilink")
  - [金錢路](../Page/金錢路.md "wikilink")（[新界喇沙中學側邊到](../Page/新界喇沙中學.md "wikilink")[牛地](../Page/牛地.md "wikilink")[雙魚河鄉村會所一帶](../Page/雙魚河.md "wikilink")）
  - [坑頭路](../Page/坑頭路.md "wikilink")（古洞[金坑路起點至坑頭](../Page/金坑路.md "wikilink")50K[小巴站一帶](../Page/小巴.md "wikilink")）
  - [粉嶺公路](../Page/粉嶺公路.md "wikilink")（上水迴旋處至白石凹交匯處）

## 重大事件

[缩略图](https://zh.wikipedia.org/wiki/File:填平後.png "fig:缩略图")

  - 2016年3月14日上午約8時，古洞村3間單層村屋突然被夷為平地，鐵皮、家具及全家幅等散落一地，兩戶居住超過60年的住客報警求助。警員接報到場，於50米外發現3部推土機，相信與事件有關，但相關的司機及工程人員則不見蹤影。警方正展開調查，暫列作刑事毀壞案處理。\[7\]住客指2015年12月已向法庭申請逆權侵佔。唯其中一間完全位於恒基地產的地皮上，並承認其土地上有建築被拆。\[8\]

## 未來發展

政府計劃將古洞北村一帶發展為[古洞北新市鎮](../Page/古洞北新市鎮.md "wikilink")，佔地498公頃，設計容量為10萬人，最快於2017年初開始發展。新市鎮南部有港鐵[東鐵綫](../Page/東鐵綫.md "wikilink")[落馬洲支線穿過](../Page/落馬洲支線.md "wikilink")，並且在新市鎮中心預留[古洞站](../Page/古洞站.md "wikilink")。

## 區議會議席分佈

由於古洞鄉村人口不足以成為一個區議會選區，所以往往跟鄰近的[河上鄉](../Page/河上鄉.md "wikilink")、[雙魚河沿線及](../Page/雙魚河.md "wikilink")[羅湖等鄉村範圍劃為同一個選區](../Page/羅湖.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>年度/範圍</p></th>
<th><p>2000-2003</p></th>
<th><p>2004-2007</p></th>
<th><p>2008-2011</p></th>
<th><p>2012-2015</p></th>
<th><p>2016-2019</p></th>
<th><p>2020-2023</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>整個古洞</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

<references />

## 外部連結

[古洞](../Category/古洞.md "wikilink")

1.  參見1939年香港地圖。
2.  參見1935年的香港地圖。
3.  在[城市規劃委員會的文件中蕉徑村的所在地被稱爲](../Page/城市規劃委員會.md "wikilink")「古洞南」。
4.  《香港地名探索》．[饒玖才著](../Page/饒玖才.md "wikilink")，香港野外學會策劃，[天地圖書](../Page/天地圖書.md "wikilink")1998年，第九頁
5.  [上水區鄉事委員會鄉村範圍圖](http://www.had.gov.hk/vre/chi/village_map/n_ssd_rc.html)

6.  <https://m.mingpao.com/pns/%E4%BD%95%E6%9D%B1%E5%BE%8C%E4%BA%BA%E7%94%B3%E5%BB%BA%E6%A8%93%20%E7%A7%9F%E5%A4%BE%E5%BF%83%E9%9A%8E%E5%B1%A4/web_tc/article/20150912/s00002/1441993706535>
7.  [古洞十多間木屋被拆　警列刑毀案追查
    on.cc東網 2016年3月14日](http://hk.on.cc/hk/bkn/cnt/news/20160314/bkn-20160314151613600-0314_00822_001.html)
8.  [強拆古洞屋涉四叔地 恒基今報警
    《蘋果日報》 2016年3月16日](http://hk.apple.nextmedia.com/news/first/20160316/19531512)