[Paléotéthys.jpg](https://zh.wikipedia.org/wiki/File:Paléotéthys.jpg "fig:Paléotéthys.jpg")\]\]
[290_global.png](https://zh.wikipedia.org/wiki/File:290_global.png "fig:290_global.png")早期，辛梅利亞大陸尚未分裂\]\]
[249_global.png](https://zh.wikipedia.org/wiki/File:249_global.png "fig:249_global.png")與[三疊紀之間](../Page/三疊紀.md "wikilink")，辛梅利亞大陸分裂自盤古大陸南部，[特提斯洋形成](../Page/特提斯洋.md "wikilink")\]\]
**古特提斯洋**（Paleo-Tethys
Ocean）是個史前海洋，存在於[古生代到](../Page/古生代.md "wikilink")[三疊紀](../Page/三疊紀.md "wikilink")，位於[匈奴地體與岡瓦那大陸之間](../Page/匈奴地體.md "wikilink")，位置相當於現今的[印度洋與](../Page/印度洋.md "wikilink")[南亞地區](../Page/南亞.md "wikilink")。

在[奧陶紀晚期](../Page/奧陶紀.md "wikilink")，匈奴地體分裂成為[歐匈地體及](../Page/歐匈地體.md "wikilink")[亞匈地體](../Page/亞匈地體.md "wikilink")，並自[岡瓦那大陸分離](../Page/岡瓦那大陸.md "wikilink")，古特提斯洋開始形成。[阿瓦隆尼亞大陸也自岡瓦那大陸分離](../Page/阿瓦隆尼亞大陸.md "wikilink")，形成[瑞亞克洋](../Page/瑞亞克洋.md "wikilink")。

在[志留紀晚期](../Page/志留紀.md "wikilink")，[華北陸塊](../Page/華北陸塊.md "wikilink")、[華南陸塊自岡瓦納大陸分離](../Page/華南陸塊.md "wikilink")，使得[原特提斯洋開始縮小](../Page/原特提斯洋.md "wikilink")，古特提斯洋更為擴張。

[泥盆紀](../Page/泥盆紀.md "wikilink")，匈奴地體南方出現[隱沒帶](../Page/隱沒帶.md "wikilink")，古特提斯洋的[海洋地殼開始隱沒](../Page/海洋地殼.md "wikilink")。岡瓦那大陸往北移動。

在[石炭紀晚期](../Page/石炭紀.md "wikilink")，華北陸塊與[西伯利亞](../Page/西伯利亞大陸.md "wikilink")-[哈薩克大陸碰撞](../Page/哈薩克大陸.md "wikilink")，原特提斯洋閉合、消失。歐美大陸與歐匈地體開始碰撞，產生[阿利根尼造山運動與](../Page/阿利根尼造山運動.md "wikilink")[華力西造山運動](../Page/華力西造山運動.md "wikilink")，瑞亞克洋逐漸消失，古特提斯洋西半部開始閉合。

在[二疊紀晚期](../Page/二疊紀.md "wikilink")，[辛梅利亞大陸](../Page/辛梅利亞大陸.md "wikilink")（[土耳其](../Page/土耳其.md "wikilink")、[伊朗](../Page/伊朗.md "wikilink")、[西藏](../Page/西藏.md "wikilink")、[印度支那](../Page/印度支那.md "wikilink")）自[盤古大陸南部分裂](../Page/盤古大陸.md "wikilink")。辛梅利亞大陸與盤古大陸東南部之間形成新的海洋，名為[特提斯洋](../Page/特提斯洋.md "wikilink")。

在[三疊紀](../Page/三疊紀.md "wikilink")，因為辛梅利亞大陸的北移，古特提斯洋成為狹窄的海道。在[侏羅紀早期](../Page/侏羅紀.md "wikilink")，古特提斯洋閉合，其海洋地殼侵入至辛梅利亞板塊下方。[黑海被認為具有古特提斯洋的海洋地殼殘餘部份](../Page/黑海.md "wikilink")。

## 參考資料

  - Stampfli, G.M.; Raumer, J.F. von; & Borel, G.D.; Feb 2002 *Paleozoic
    evolution of pre-Variscan terranes: from Gondwana to the Variscan
    collision* in Geological Society of America special paper 364, p 263

### 外部連結

  - [晚石炭紀的古特提斯洋(圖片)](http://scotese.com/late.htm)
  - [地球的歷史](https://web.archive.org/web/20060820132610/http://jan.ucc.nau.edu/~rcb7/global_history.html)
      - [原特提斯洋、古特提斯洋的圖片(泥炭紀)](https://web.archive.org/web/20160303193051/http://jan.ucc.nau.edu/~rcb7/370_1st.jpg)

[Category:古海洋](../Category/古海洋.md "wikilink")
[Category:古生代](../Category/古生代.md "wikilink")
[Category:三疊紀](../Category/三疊紀.md "wikilink")