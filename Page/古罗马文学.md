**古罗马文学**指纪元前后繁荣于[古罗马政权](../Page/古罗马.md "wikilink")（包括罗马[共和国和罗马](../Page/共和国.md "wikilink")[帝国](../Page/帝国.md "wikilink")）治下的文学。其主要语言是[拉丁语](../Page/拉丁语.md "wikilink")。尽管古罗马共和国诞生于公元前510年（摆脱[伊特鲁利亚王朝的统治](../Page/伊特鲁利亚王朝.md "wikilink")），但按照惯例，真正意义上的古罗马文学则从公元前240年算起。

## 起源与概述

[罗马城建立于公元前](../Page/罗马.md "wikilink")8世纪。在[古罗马人生活的](../Page/古罗马.md "wikilink")[伊特鲁利亚以南](../Page/伊特鲁利亚.md "wikilink")、[台伯河以西地区](../Page/台伯河.md "wikilink")，包含文艺活动的拉丁[朱庇特庆祭节的设立应该不迟于公元前](../Page/朱庇特.md "wikilink")7世纪。受[伊特鲁利亚文明的影响](../Page/伊特鲁利亚.md "wikilink")，包括罗马人在内的拉丁人逐渐形成了自己的乡土文化。

古罗马的文化主要是继承[希腊文化而逐渐发展起来的](../Page/希腊.md "wikilink")。在[希腊化时期](../Page/希腊化.md "wikilink")，[罗马就输入了许多希腊作品](../Page/罗马.md "wikilink")，加以翻译和摹仿。在公元前146年罗马灭亡希腊之后，更是将全部[希腊神话](../Page/希腊神话.md "wikilink")、诗歌和[戏剧据为己有](../Page/古希腊戏剧.md "wikilink")，找了许多从希腊俘虏来的[奴隶来做家庭教师](../Page/奴隶.md "wikilink")，让他们编剧作诗，并研究各种[科学](../Page/科学.md "wikilink")，这使得古罗马文学染上了浓厚的希腊色彩。以神话为例，同希腊文化接触后，许多[罗马的神祇便同](../Page/罗马.md "wikilink")[希腊的神祇结合起来](../Page/希腊.md "wikilink")。如罗马人信奉的主神[朱庇特便等同于希腊的](../Page/朱庇特.md "wikilink")[宙斯](../Page/宙斯.md "wikilink")，他的妻子[朱诺则等同于](../Page/朱诺.md "wikilink")[赫拉](../Page/赫拉.md "wikilink")。至于太阳神[阿波罗和文艺女神](../Page/阿波罗.md "wikilink")[缪斯等则直接进入罗马神话](../Page/缪斯.md "wikilink")，连名字都没有变。

当然，古罗马文学也并非全是古希腊文学的仿造品，因为它毕竟是罗马社会的产物，其采用的语言是[拉丁语](../Page/拉丁语.md "wikilink")。在西方学术界，古罗马文学被认为是广义的拉丁文学的一部分。与古希腊[海洋民族不同](../Page/海洋.md "wikilink")，古罗马属于内陆民族，主要以耕牧方式生存，具有上古农民和牧民粗鄙、蒙昧、淳朴的特点。建国之后的[古罗马崇尚武力](../Page/古罗马.md "wikilink")，追求社会与国家、[法律与集权的强盛与完美](../Page/法律.md "wikilink")，其文学具有更强的理性精神和集体意识，具有庄严崇高的气质，却也缺少希腊文学生动活泼的灵气和无拘无束的儿童式的天真烂漫。古罗马文学在艺术上强调均衡、严整、和谐，重视[修辞与句法](../Page/修辞.md "wikilink")，技巧上偏于雕琢与矫饰。

## 历史沿革

古罗马文学的发展大致经历了三个阶段，即[共和时代](../Page/共和.md "wikilink")、黄金时代和白银时代。应该指出的是，“共和时代”是一个[政治概念](../Page/政治.md "wikilink")，而“黄金时代”和“白银时代”则是两个主要根据[拉丁语言的发展和问题特征定性的名称](../Page/拉丁语.md "wikilink")。共和时代止于公元前30年，实际上也包含了70年的黄金时代。此外，作为一个政治概念，共和时代始于公元前510年，但作为一个与文学发展相关的名称，它的起始则从前240年算起。而尽管以[罗马为首都的](../Page/罗马.md "wikilink")[西罗马帝国覆灭于公元](../Page/西罗马帝国.md "wikilink")476年，学术界通常仍习惯于将公元2世纪中期（130－150）作为古罗马文学的终点。2世纪中期以后的拉丁语文学被定义为“后古典拉丁文学”，这一时期的拉丁语文学已经开始向[中世纪基督教文学过渡](../Page/中世纪文学.md "wikilink")，不属于传统意义上的古罗马文学了。

### 共和时代（前240年－前30年）

古罗马文学的正式形成与一位生活在公元前3世纪的叫做[利维乌斯·安德罗尼斯库的希腊人密切相关](../Page/利维乌斯·安德罗尼斯库.md "wikilink")。他是古罗马文学的奠基人，翻译了[荷马史诗](../Page/荷马史诗.md "wikilink")《[奥德赛](../Page/奥德赛.md "wikilink")》和大量古希腊抒情诗。利维乌斯的功绩主要在于把古希腊文学中的某些精品介绍给了缺少书面文学传统的罗马人，把古希腊文学的某些主要形式移栽到了缺少骨干文学类型的[古罗马](../Page/古罗马.md "wikilink")。

#### 诗歌

早期的古罗马诗人中颇多全能型作家。诗人[埃纽斯](../Page/埃纽斯.md "wikilink")（前239－前168）不仅改写和创作过[悲剧](../Page/悲剧.md "wikilink")，而且还写过戏剧和4至6卷讽刺诗。他的[史诗](../Page/史诗.md "wikilink")《编年史》追溯罗马的历史，始于[埃涅阿斯的经历](../Page/埃涅阿斯.md "wikilink")，止于作者生活年代的战争，洋洋18卷篇幅，不过已基本散佚，仅剩不到600行传世。从文学史角度看，《编年史》摈弃了古老的[神农格](../Page/神农格.md "wikilink")，采纳了荷马史诗所用的[六步音长短短格](../Page/六步音长短短格.md "wikilink")。但在风格上，有明显的模仿荷马的痕迹。埃纽斯对古罗马文学影响深远，[西塞罗](../Page/西塞罗.md "wikilink")、[卢克莱修和](../Page/卢克莱修.md "wikilink")[维吉尔都表示自己曾受其影响](../Page/维吉尔.md "wikilink")，他被尊为“古罗马文学之父”。

#### 喜剧

[普劳图斯](../Page/普劳图斯.md "wikilink")（约前254年－前184年）精通[古希腊文](../Page/古希腊文.md "wikilink")，是共和时代最著名的剧作家。相传普劳图斯著有戏剧130部，但据考证仅有21部出自他的手笔，其他均系后人伪作。他的喜剧主要以希腊新[喜剧作家](../Page/喜剧.md "wikilink")[米南德的风俗喜剧为蓝本改作](../Page/米南德.md "wikilink")，讽刺罗马社会的腐化风习。其主要作品包括《孪生兄弟》、《[俘虏](../Page/俘虏.md "wikilink")》、《[商人](../Page/商人.md "wikilink")》、《[驴](../Page/驴.md "wikilink")》、《[蝗虫](../Page/蝗虫.md "wikilink")》等。

[泰伦提乌斯](../Page/泰伦提乌斯.md "wikilink")（前190年－前159年）生于[迦太基](../Page/迦太基.md "wikilink")，本是[奴隶](../Page/奴隶.md "wikilink")，后来获释。他一生共写过六部[喜剧](../Page/喜剧.md "wikilink")，包括《婆母》、《两兄弟》等代表作品都是从古希腊新喜剧改编或翻译过来的。其[喜剧结构严谨](../Page/喜剧.md "wikilink")、语言文雅但欠生动，人物内心矛盾刻画细腻，人物形象自然。他的喜剧不如普劳图斯的滑稽有趣，在当时仅受到有教养的观众喜爱。泰伦斯对后世的喜剧产生了相当大的影响，法国的[莫里哀](../Page/莫里哀.md "wikilink")、英国的[斯梯尔和](../Page/斯梯尔.md "wikilink")[谢里丹都曾模仿过他的作品](../Page/谢里丹.md "wikilink")。

### 黄金时代（前100年－17年）

“黄金时代”即拉丁语和广义的拉丁文学（包括[修辞](../Page/修辞.md "wikilink")、[历史和](../Page/历史.md "wikilink")[哲学](../Page/哲学.md "wikilink")）发展史上的古典或辉煌时期，涵盖两位著名人物的活动年代，即“[西塞罗时期](../Page/西塞罗.md "wikilink")”（前70－前30）和“[奥古斯都时期](../Page/奥古斯都.md "wikilink")”（前31－14）。这一时期的罗马进入了大规模扩张阶段，并于公元前27年结束了[共和制政体](../Page/共和制.md "wikilink")，建立了帝国。古罗马帝国在奥古斯都（即[屋大维](../Page/屋大维.md "wikilink")）治下（前31－14）进入前所未有的繁荣时期，拉丁语文学和艺术也出现了空前的繁荣。

[屋大维统治时期采取了稳定社会秩序和促进](../Page/屋大维.md "wikilink")[经济发展的措施](../Page/经济.md "wikilink")，使得一度动荡的古罗马社会呈现出和平稳定的景象。屋大维本人十分重视文化建设，他笼络文人墨客为自己的文化政策服务，这一时期的大文豪[维吉尔](../Page/维吉尔.md "wikilink")、[贺拉斯](../Page/贺拉斯.md "wikilink")、[奥维德等都曾是他的御用作家](../Page/奥维德.md "wikilink")。正是由于这个原因，这一时期的文学作品缺乏共和时代的[哲学探索精神和](../Page/哲学.md "wikilink")[政治辩论热情](../Page/政治.md "wikilink")，而更多的肯定现存秩序所带来的和平生活和强大国力。文学风格也不及前一时期遒劲豪放，但技巧却更趋成熟，追求形式的完美。

#### 诗歌

[卢克莱修](../Page/卢克莱修.md "wikilink")（前99年－前55年）生于共和国末期，唯一的传世之作《[物性论](../Page/物性论.md "wikilink")》（一译《论自然》）共六卷，每卷千余行，是一部哲理诗。全诗着重阐述[伊壁鸠鲁的哲学思想和](../Page/伊壁鸠鲁.md "wikilink")[德谟克利特的原子论](../Page/德谟克利特.md "wikilink")，表示物质不灭、凡人不必惧怕死亡的生活观。[卢克莱修是古罗马文学史上著名的智者](../Page/卢克莱修.md "wikilink")，[维吉尔曾称羡慕他知晓事物的起因](../Page/维吉尔.md "wikilink")，是个“幸福的人”。

[卡图鲁斯](../Page/卡图鲁斯.md "wikilink")（前84年－前54年）生于意大利北部维罗那一个富有的家庭，经常出入罗马上流社会，是黄金时代成就最高的抒情诗人。他是坚定不移的共和派，曾公然反对过[恺撒](../Page/恺撒.md "wikilink")，曾创作过许多辛辣的讽刺短诗。卡图鲁斯的诗作现存116首，他善用警句式的语言表达浓郁热烈、复杂微妙的感情。他的抒情诗对后世[欧洲许多伟大诗人都产生过影响](../Page/欧洲.md "wikilink")。

[贺拉斯](../Page/贺拉斯.md "wikilink")（前65年－前8年）出生于[拍卖商家庭](../Page/拍卖.md "wikilink")，是和[卡图鲁斯齐名的抒情诗人](../Page/卡图鲁斯.md "wikilink")。他幼年受过良好的教育，通晓[拉丁语和](../Page/拉丁语.md "wikilink")[希腊语](../Page/希腊语.md "wikilink")，能诵[荷马史诗原文](../Page/荷马史诗.md "wikilink")，并到[雅典学过哲学](../Page/雅典.md "wikilink")。他的代表作品包括《长短句集》17首和《闲谈集》18首。前者表明作者反对内战，幻想黄金时代到来的思想；后者则讽刺[罗马社会的恶习](../Page/罗马.md "wikilink")。但贺拉斯最著名的作品是后期的《歌集》（一译《颂歌集》）和《[诗艺](../Page/诗艺.md "wikilink")》。贺拉斯的抒情诗改造了希腊抒情诗的格律，构思巧妙，语言优美，优雅庄重，以有意、爱情、诗艺为题，融哲理和感情于一路，不少人竞相模仿。《诗艺》则是古罗马时期文艺理论上的最高成就，被古典主义文学视为经典。

[维吉尔](../Page/维吉尔.md "wikilink")（前70年－前19年）是[古罗马最伟大的诗人](../Page/古罗马.md "wikilink")，其史诗《[埃涅阿斯纪](../Page/埃涅阿斯纪.md "wikilink")》是西方文学史上第一部[文人史诗](../Page/文人史诗.md "wikilink")。维吉尔出生于[农民家庭](../Page/农民.md "wikilink")，其抒情诗充满浪漫的田园风光。代表作品包括《牧歌》、《农事诗》和《工作与时日》，主要抒发对[爱情](../Page/爱情.md "wikilink")、时政以及乡村生活的种种感受。然而，[维吉尔成就最高的作品却是史诗](../Page/维吉尔.md "wikilink")《[埃涅阿斯纪](../Page/埃涅阿斯纪.md "wikilink")》。全诗计12卷，长达近万行，是遵照[奥古斯都的旨意创作出来的](../Page/奥古斯都.md "wikilink")。史诗讲述[特洛伊王和女神](../Page/特洛伊.md "wikilink")[维纳斯所生之子埃涅阿斯到](../Page/维纳斯.md "wikilink")[意大利建立新王朝的故事](../Page/意大利.md "wikilink")，歌颂罗马祖先建国的功绩和罗马的光荣。诗人把埃涅阿斯写成[恺撒和](../Page/恺撒.md "wikilink")[屋大维的祖先](../Page/屋大维.md "wikilink")，因而肯定了[屋大维的](../Page/屋大维.md "wikilink")“神统”。维吉尔在创作《[埃涅阿斯纪](../Page/埃涅阿斯纪.md "wikilink")》的时候虽有意摹仿[荷马史诗](../Page/荷马史诗.md "wikilink")，但全诗强调使命感、责任感，洋溢着严肃、哀婉和悲天悯人的情调，是典型的罗马风格。在语言特色和艺术手法上，《埃涅阿斯纪》辞藻华丽，行文略显呆板。维吉尔对后世的影响是巨大的。[但丁认为维吉尔最有智慧](../Page/但丁.md "wikilink")，最了解人类，因而在《[神曲](../Page/神曲.md "wikilink")》中让他作为[地狱和炼狱的向导](../Page/地狱.md "wikilink")。[斯宾塞的](../Page/斯宾塞.md "wikilink")《仙后》和[弥尔顿的](../Page/弥尔顿.md "wikilink")《[失乐园](../Page/失乐园.md "wikilink")》也有模仿《埃涅阿斯纪》的痕迹。

[Ovidius_Metamorphosis_-_George_Sandy's_1632_edition.jpg](https://zh.wikipedia.org/wiki/File:Ovidius_Metamorphosis_-_George_Sandy's_1632_edition.jpg "fig:Ovidius_Metamorphosis_-_George_Sandy's_1632_edition.jpg")

黄金时代的另一位伟大的作家是[奥维德](../Page/奥维德.md "wikilink")（前43年－18年），年轻时曾在罗马学习[修辞](../Page/修辞.md "wikilink")。不过成年以后，他却并没有按照父亲的意愿成为诉讼师而是成为一位诗人。他的代表诗作包括《爱情诗》49首以及《爱艺》和《古代名媛》。然而奥维德最著名的作品则是神话诗《[变形记](../Page/变形记_\(奥维德\).md "wikilink")》，以史诗格律写成，包括250个神话故事，集希腊、罗马神话之大成，为后世文学家提供了重要的材料和创作灵感。《[十日谈](../Page/十日谈.md "wikilink")》、《[坎特伯雷故事集](../Page/坎特伯雷故事集.md "wikilink")》等故事集都在框架上模仿《变形记》。此外，[但丁](../Page/但丁.md "wikilink")、[莎士比亚](../Page/莎士比亚.md "wikilink")、[蒙田](../Page/蒙田.md "wikilink")、[莫里哀](../Page/莫里哀.md "wikilink")、[歌德等大文豪的创作都在不同程度上受到他的影响](../Page/歌德.md "wikilink")。

黄金时代的抒情诗人还包括[普洛佩提乌斯](../Page/普洛佩提乌斯.md "wikilink")（前50－前15）和[提布鲁斯](../Page/提布鲁斯.md "wikilink")（前54－前19）。前者以抒写感情细腻的爱情诗而著称，而后者则擅长描写淳朴的田园风光。

#### 散文

古罗马的[散文发源于](../Page/散文.md "wikilink")[加图](../Page/加图.md "wikilink")（前234－前149）的演说文，繁荣于“黄金时代”，即共和国末期和[屋大维执政时期](../Page/屋大维.md "wikilink")。这一时期罗马的政治斗争、阶级矛盾异常激烈，统治阶级的法律体系已经开始形成，这就使得许多[政治家热心于](../Page/政治.md "wikilink")[雄辩术的研究](../Page/雄辩术.md "wikilink")，致使散文这一文体得以迅速发展。

[西塞罗](../Page/西塞罗.md "wikilink")（前106年－前43年）是这一时期最著名的散文家。他年少时曾学习[哲学和](../Page/哲学.md "wikilink")[法律](../Page/法律.md "wikilink")，并做过一段时间[律师](../Page/律师.md "wikilink")。43岁进入政界任执政官，后任西西里总督。内战时期他追随[庞培反对](../Page/格涅乌斯·庞贝.md "wikilink")[恺撒](../Page/恺撒.md "wikilink")，维护贵族元老派的立场，后来在政治倾轧中被刺杀。

[西塞罗的主要散文成就是演说词和书信](../Page/西塞罗.md "wikilink")。他的书信现存约900封，主要包括《致阿提库斯书》16卷、《致友人书》16卷。这些书信反映共和国末期的社会生活，描绘形形色色的政治人物，风格接近口语。其演说词传世58篇，分为法庭演说和政治演说两类。[西塞罗的散文注重材料的程式组织](../Page/西塞罗.md "wikilink")，句法考究，词汇丰富，段落对称，音调铿锵，被成为“西塞罗句法”。他的演说具有很强的鼓动力量，有时甚至不惜用侮蔑和歪曲事实的手段来感染观众的情绪。

[西塞罗对拉丁语散文的贡献非常之大](../Page/西塞罗.md "wikilink")，他确立了[拉丁语文学语言](../Page/拉丁语.md "wikilink")“准确、流畅、清新、雄浑”的原则。其散文风格对后世影响深远，成为欧洲诸民族散文的楷模。他的政敌[恺撒甚至曾公然称赞他](../Page/恺撒.md "wikilink")：“你的功绩高于军事将领，扩大知识领域比之于扩大罗马帝国的版图，在意义上更为可贵。”

[恺撒](../Page/恺撒.md "wikilink")（前102年－前44年）是[奥古斯都的养父](../Page/奥古斯都.md "wikilink")，古罗马历史上有名的军事家、政治家、独裁者，于前44年被共和派刺杀。他在散文上的贡献主要包括历史著作《[高卢战记](../Page/高卢战记.md "wikilink")》七卷和回忆他和[庞培之间战争的](../Page/格涅乌斯·庞贝.md "wikilink")《[内战记](../Page/内战记.md "wikilink")》三卷。其散文语言简洁凝练，朴实无华，体现了和[西塞罗迥异的风格](../Page/西塞罗.md "wikilink")。

此外，历史学家[萨卢斯特](../Page/萨卢斯特.md "wikilink")（生卒年不详）和[李维](../Page/蒂托·李维.md "wikilink")（前64－17）也发展了拉丁语文体。

### 白银时代（17年－130年）

[屋大维死后的一百年间](../Page/屋大维.md "wikilink")，史称[罗马文学的](../Page/罗马.md "wikilink")“白银时代”。这一时期罗马在政治上不断衰弱，内部矛盾日趋激烈，其文学发展的特点是宫廷趣味日趋浓厚，崇尚文风的花哨和滥用修辞，使得文体显得逼挤、臃肿。这一特点在2世纪前半叶达到高潮。[贵族青年以公开朗诵空洞无物的诗歌为时髦](../Page/贵族.md "wikilink")，文学更成为少数人的消遣。白银时代成就最高的[文学样式是反映](../Page/文学.md "wikilink")[奴隶主下层思想的讽刺文学和反映旧共和派不满情绪的作品](../Page/奴隶主.md "wikilink")。

#### 诗歌

[卢坎](../Page/卢坎.md "wikilink")（39年－65年）是白银时代最出色的诗人之一，创作了继《[埃涅阿斯纪](../Page/埃涅阿斯纪.md "wikilink")》之后最优秀的史诗《[法萨利亚](../Page/法萨利亚.md "wikilink")》。[马提阿利](../Page/马提阿利.md "wikilink")（40－104）则是这一时代最优秀的[碑铭体诗人](../Page/碑铭体.md "wikilink")，主要诗作是《碑铭体诗集》（一译《警句诗集》）12卷，1500余首。其风格短小精悍，含蓄突兀，富于机智和幽默。[德奇姆斯·尤尼烏斯·尤維納利斯](../Page/尤维纳利斯.md "wikilink")（60－127）则以讽刺诗著称。他长于借古喻今，诗风严峻而尖锐，其诗句“即使没有天才，愤怒出诗句”已经成为名言。19世纪欧洲资产阶级革命高涨的年代，[德奇姆斯·尤尼烏斯·尤維納利斯的作品受到人们极大的重视](../Page/尤维纳利斯.md "wikilink")，[席勒](../Page/席勒.md "wikilink")、[雨果和](../Page/維克多·雨果.md "wikilink")[别林斯基都曾给予他很高的评价](../Page/别林斯基.md "wikilink")。

在抒情诗方面，[斯泰提乌斯](../Page/斯泰提乌斯.md "wikilink")（45－95）几乎是唯一有成就的诗人，擅长描写有闲阶层生活情趣。

#### 戏剧

[塞内卡](../Page/塞内卡.md "wikilink")（4年－65年）是古罗马最重要的[悲剧作家](../Page/悲剧.md "wikilink")，他受[斯多葛哲学影响](../Page/斯多葛哲学.md "wikilink")，精于[修辞和](../Page/修辞.md "wikilink")[哲学](../Page/哲学.md "wikilink")，并曾担任过著名暴君[尼禄的老师](../Page/尼禄.md "wikilink")。他主张人们用内心的宁静来克服生活中的痛苦，宣传同情、仁爱。他一生共写过9部[悲剧和](../Page/悲剧.md "wikilink")1部讽刺剧，多半取材自希腊悲剧。其作品风格崇高严肃，夹杂大量的道德说教，使得其笔下的对话和人物都缺乏真实感。其代表作是悲剧《特洛伊妇女》。[塞内卡晚年因参加元老院贵族反对](../Page/塞内卡.md "wikilink")[尼禄暴政而被尼禄赐死](../Page/尼禄.md "wikilink")。

#### 散文

这里的“[散文](../Page/散文.md "wikilink")”并非现代文学上所讲的狭义上的散文，而是泛指拉丁语文学中的“散文体”，和诗体相对，包括散文、[小说](../Page/小说.md "wikilink")、[传记文学和](../Page/传记文学.md "wikilink")[编年史等](../Page/编年史.md "wikilink")。

从严格的意义上说，欧洲文学史上“[小说](../Page/小说.md "wikilink")”这一体裁就诞生于[古罗马时期](../Page/古罗马.md "wikilink")。[彼特隆纽斯](../Page/彼特隆纽斯.md "wikilink")（生卒不详）的《[萨蒂里卡](../Page/萨蒂里卡.md "wikilink")》是传奇式小说，现存两章残篇，广泛记录了意大利南部半[希腊化城市流行的享乐生活](../Page/希腊化.md "wikilink")。书中人物语言符合方言特点，文笔典雅，机智风趣。尽管其形式和传统意义的小说还存在差别，但学术界还是倾向于将它看作欧洲文学史上的第一部[流浪汉小说](../Page/流浪汉小说.md "wikilink")。

然而公认的“小说之父”却是[阿普列尤斯](../Page/阿普列尤斯.md "wikilink")（124年－175年），他出生于[北非的军官家庭](../Page/北非.md "wikilink")，曾漫游各地，研究过[哲学和](../Page/哲学.md "wikilink")[幻术](../Page/幻术.md "wikilink")。他最著名的作品是小说《[金驴记](../Page/金驴记.md "wikilink")》，用自叙形式写成，是西方文学史上第一部产生了深远影响的长篇小说。

在[编年史和](../Page/编年史.md "wikilink")[传记文学方面](../Page/传记文学.md "wikilink")，代表人物包括[塔西佗](../Page/塔西佗.md "wikilink")（55年－118年）、[普鲁塔克](../Page/普鲁塔克.md "wikilink")（46年－120年）和[苏维托尼乌斯](../Page/苏维托尼乌斯.md "wikilink")（69年－140年）。

[塔西佗是共和贵族派的最后一个代表](../Page/塔西佗.md "wikilink")，主要著作包括《[历史](../Page/历史.md "wikilink")》和《[编年史](../Page/编年史.md "wikilink")》。这两部著作虽然都是历史著作，却具有强烈的文学性，其历史观源于“个人创造历史”，其中对历代帝王将相的实录描写非常真实且具感染力。[普鲁塔克的代表作品是](../Page/普鲁塔克.md "wikilink")《希腊、罗马名人传》，记载从半[神话人物一直到](../Page/神话.md "wikilink")1世纪的罗马皇帝的生平，[莎士比亚](../Page/莎士比亚.md "wikilink")、[歌德等人均曾从中取材创作](../Page/歌德.md "wikilink")。但这部著作在史实上有诸多失真的地方，从19世纪之后影响力逐渐减弱。[苏维托尼乌斯则著有](../Page/苏维托尼乌斯.md "wikilink")《罗马十二帝王传》，记述了罗马社会及自[恺撒到](../Page/恺撒.md "wikilink")[图密善共](../Page/图密善.md "wikilink")12个皇帝的概况，行文朴实流畅，是古罗马文学中难得的不事辞藻的作品。此外，他还著有《名人传》，后世对古罗马文学家的生平的了解几乎都出自此书。

## 参见

  - [拉丁语](../Page/拉丁语.md "wikilink")
  - [古罗马](../Page/古罗马.md "wikilink")

[\*](../Category/古罗马文学.md "wikilink")
[Category:古典文學](../Category/古典文學.md "wikilink")
[Category:拉丁文学](../Category/拉丁文学.md "wikilink")