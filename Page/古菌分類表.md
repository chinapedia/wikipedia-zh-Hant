本表以[LPSN網站](https://web.archive.org/web/20110613224152/http://www.bacterio.cict.fr/classifphyla.html)的分類爲基礎（當前版本[2007年](../Page/2007年.md "wikilink")[3月29日](../Page/3月29日.md "wikilink")），本分類代表[原核生物分類的權威雜誌](../Page/原核生物.md "wikilink")[IJSEM](http://ijs.sgmjournals.org/)的[分類系統](../Page/分類系統.md "wikilink")，同时参考[NCBI
Taxonomy](http://www.ncbi.nlm.nih.gov/Taxonomy/)，但目前其它中文維基分類表可能依照其它標準，請注意其區別。

中文名稱採用慣例（包括網上搜索途徑）以及拉丁名直接意譯，部分參考[《細菌名稱》第二版](https://web.archive.org/web/20051221210853/http://www1.im.ac.cn/bacteria/bacteria.htm)，拉丁文意譯參考[原核生物名稱(英文)](http://www.bacterio.cict.fr/gender.html)網站。翻譯問題歡迎在原文中添加或者在討論頁中提出。

本表列到[屬級](../Page/屬_\(生物\).md "wikilink")。
後面帶\*者爲尚未被[IJSEM雜誌確認的分類](../Page/IJSEM.md "wikilink")。

參見[古細菌](../Page/古細菌.md "wikilink")。

## [初古菌門](../Page/初古菌門.md "wikilink")(Korarchaeota)\*

1.  [初古菌属](../Page/初古菌属.md "wikilink")(*Korarchaeum*)\*

## [納古菌門](../Page/納古菌門.md "wikilink")(Nanoarchaeota)\*

1.  [納古菌屬](../Page/納古菌屬.md "wikilink")(*Nanoarchaeum*)\*

## Thaumarchaeota

1.  [餐古菌目](../Page/餐古菌目.md "wikilink")(Cenarchaeales)
    1.  [餐古菌科](../Page/餐古菌科.md "wikilink")(Cenarchaeaceae)\*
        1.  [餐古菌屬](../Page/餐古菌屬.md "wikilink")(*Cenarchaeum*)\*
2.  [亞硝化暖菌目](../Page/亞硝化暖菌目.md "wikilink")(Nitrosocaldales)\*
    1.  [亞硝化暖菌科](../Page/亞硝化暖菌科.md "wikilink")(Nitrosocaldaceae)\*
        1.  [亞硝化暖菌屬](../Page/亞硝化暖菌屬.md "wikilink")(*Nitrosocaldus*)\*
3.  [亞硝化侏儒菌目](../Page/亞硝化侏儒菌目.md "wikilink")(Nitrosopumilales)\*
    1.  [亞硝化侏儒菌科](../Page/亞硝化侏儒菌科.md "wikilink")(Nitrosopumilaceae)\*
        1.  [亞硝化侏儒菌屬](../Page/亞硝化侏儒菌屬.md "wikilink")(*Nitrosopumilus*)\*

## [泉古菌門](../Page/泉古菌門.md "wikilink")(Crenarchaeota)

### [熱變形菌綱](../Page/熱變形菌綱.md "wikilink")(Thermoprotei)

1.  [暖球形菌目](../Page/暖球形菌目.md "wikilink")(Caldisphaerales)
    1.  [暖球形菌科](../Page/暖球形菌科.md "wikilink")(Caldisphaeraceae)
        1.  [暖球形菌屬](../Page/暖球形菌屬.md "wikilink")(*Caldisphaera*)
2.  [除硫球菌目](../Page/除硫球菌目.md "wikilink")(Desulfurococcales)
    1.  [除硫球菌科](../Page/除硫球菌科.md "wikilink")(Desulfurococcaceae)
        1.  [酸葉菌屬](../Page/酸葉菌屬.md "wikilink")(*Acidilobus*)
        2.  [氣火菌屬](../Page/氣火菌屬.md "wikilink")(*Aeropyrum*)
        3.  [除硫球菌屬](../Page/除硫球菌屬.md "wikilink")(*Desulfurococcus*)
        4.  [燃球菌屬](../Page/燃球菌屬.md "wikilink")(*Ignicoccus*)
        5.  [燃球形菌屬](../Page/燃球形菌屬.md "wikilink")(*Ignisphaera*)
        6.  [葡萄熱菌屬](../Page/葡萄熱菌屬.md "wikilink")(*Staphylothermus*)
        7.  [施鐵特菌屬](../Page/施鐵特菌屬.md "wikilink")(*Stetteria*)
        8.  [厭硫球菌屬](../Page/厭硫球菌屬.md "wikilink")(*Sulfophobococcus*)
        9.  [熱盤菌屬](../Page/熱盤菌屬.md "wikilink")(*Thermodiscus*)
        10. [熱球形菌屬](../Page/熱球形菌屬.md "wikilink")(*Thermosphaera*)
    2.  [熱網菌科](../Page/熱網菌科.md "wikilink")(Pyrodictiaceae)
        1.  [超熱菌屬](../Page/超熱菌屬.md "wikilink")(*Hyperthermus*)
        2.  [熱網菌屬](../Page/熱網菌屬.md "wikilink")(*Pyrodictium*)
        3.  [火葉菌屬](../Page/火葉菌屬.md "wikilink")(*Pyrolobus*)
3.  [硫化葉菌目](../Page/硫化葉菌目.md "wikilink")(Sulfolobales)
    1.  [硫化葉菌科](../Page/硫化葉菌科.md "wikilink")(Sulfolobaceae)
        1.  [喜酸菌屬](../Page/喜酸菌屬.md "wikilink")(*Acidianus*)
        2.  [除硫葉菌屬](../Page/除硫葉菌屬.md "wikilink")(*Desulfurolobus*)
        3.  [生金球形菌屬](../Page/生金球形菌屬.md "wikilink")(*Metallosphaera*)
        4.  [憎葉菌屬](../Page/憎葉菌屬.md "wikilink")(*Stygiolobus*)
        5.  [硫化葉菌屬](../Page/硫化葉菌屬.md "wikilink")(*Sulfolobus*)
        6.  [硫磺球形菌屬](../Page/硫磺球形菌屬.md "wikilink")(*Sulfurisphaera*)
        7.  [硫磺球菌屬](../Page/硫磺球菌屬.md "wikilink")(*Sulfurococcus*)
4.  [熱變形菌目](../Page/熱變形菌目.md "wikilink")(Thermoproteales)
    1.  [熱絲菌科](../Page/熱絲菌科.md "wikilink")(Thermofilaceae)
        1.  [熱絲菌屬](../Page/熱絲菌屬.md "wikilink")(*Thermofilum*)
    2.  [熱變形菌科](../Page/熱變形菌科.md "wikilink")(Thermoproteaceae)
        1.  [暖枝菌屬](../Page/暖枝菌屬.md "wikilink")(*Caldivirga*)
        2.  [火棒菌屬](../Page/火棒菌屬.md "wikilink")(*Pyrobaculum*)
        3.  [熱分支菌屬](../Page/熱分支菌屬.md "wikilink")(*Thermocladium*)
        4.  [熱變形菌屬](../Page/熱變形菌屬.md "wikilink")(*Thermoproteus*)
        5.  [火山鬃菌屬](../Page/火山鬃菌屬.md "wikilink")(*Vulcanisaeta*)

## [廣古菌門](../Page/廣古菌門.md "wikilink")(Euryarchaeota)

### [古丸菌綱](../Page/古丸菌綱.md "wikilink")(Archaeoglobi)

1.  [古丸菌目](../Page/古丸菌目.md "wikilink")(Archaeoglobales)
    1.  [古丸菌科](../Page/古丸菌科.md "wikilink")(Archaeoglobaceae)
        1.  [古丸菌屬](../Page/古丸菌屬.md "wikilink")(*Archaeoglobus*)
        2.  [鐵丸菌屬](../Page/鐵丸菌屬.md "wikilink")(*Ferroglobus*)
        3.  [地丸菌屬](../Page/地丸菌屬.md "wikilink")(*Geoglobus*)

### [鹽桿菌綱](../Page/鹽桿菌綱.md "wikilink")(Halobacteria)

1.  [鹽桿菌目](../Page/鹽桿菌目.md "wikilink")(Halobacteriales)
    1.  [鹽桿菌科](../Page/鹽桿菌科.md "wikilink")(Halobacteriaceae)
        1.  [適鹽菌屬](../Page/適鹽菌屬.md "wikilink")(*Haladaptatus*)
        2.  [鹽鹼球菌屬](../Page/鹽鹼球菌屬.md "wikilink")(*Halalkalicoccus*)
        3.  [鹽盒菌屬](../Page/鹽盒菌屬.md "wikilink")(*Haloarcula*)
        4.  [鹽桿菌屬](../Page/鹽桿菌屬.md "wikilink")(*Halobacterium*)
        5.  [鹽棒菌屬](../Page/鹽棒菌屬.md "wikilink")(*Halobaculum*)
        6.  [鹽二型菌屬](../Page/鹽二型菌屬.md "wikilink")(*Halobiforma*)
        7.  [鹽球菌屬](../Page/鹽球菌屬.md "wikilink")(*Halococcus*)
        8.  [富鹽菌屬](../Page/富鹽菌屬.md "wikilink")(*Haloferax*)
        9.  [鹽幾何菌屬](../Page/鹽幾何菌屬.md "wikilink")(*Halogeometricum*)
        10. [鹽微菌屬](../Page/鹽微菌屬.md "wikilink")(*Halomicrobium*)
        11. [鹽惰菌屬](../Page/鹽惰菌屬.md "wikilink")(*Halopiger*)
        12. [鹽盤菌屬](../Page/鹽盤菌屬.md "wikilink")(*Haloplanus*)
        13. [鹽方菌屬](../Page/鹽方菌屬.md "wikilink")(*Haloquadratum*)
        14. [鹽棍菌屬](../Page/鹽棍菌屬.md "wikilink")(*Halorhabdus*)
        15. [鹽紅菌屬](../Page/鹽紅菌屬.md "wikilink")(*Halorubrum*)
        16. [鹽簡菌屬](../Page/鹽簡菌屬.md "wikilink")(*Halosimplex*)
        17. [鹽池棲菌屬](../Page/鹽池棲菌屬.md "wikilink")(*Halostagnicola*)
        18. [鹽陸生菌屬](../Page/鹽陸生菌屬.md "wikilink")(*Haloterrigena*)
        19. [鹽長命菌屬](../Page/鹽長命菌屬.md "wikilink")(*Halovivax*)
        20. [鈉白菌屬](../Page/鈉白菌屬.md "wikilink")(*Natrialba*)
        21. [鈉線菌屬](../Page/鈉線菌屬.md "wikilink")(*Natrinema*)
        22. [鹽鹼桿菌屬](../Page/鹽鹼桿菌屬.md "wikilink")(*Natronobacterium*)
        23. [鹽鹼球菌屬](../Page/鹽鹼球菌屬.md "wikilink")(*Natronococcus*)
        24. [鹽鹼湖菌屬](../Page/鹽鹼湖菌屬.md "wikilink")(*Natronolimnobius*)
        25. [鹽鹼單胞菌屬](../Page/鹽鹼單胞菌屬.md "wikilink")(*Natronomonas*)
        26. [鹽鹼紅菌屬](../Page/鹽鹼紅菌屬.md "wikilink")(*Natronorubrum*)

### [甲烷桿菌綱](../Page/甲烷桿菌綱.md "wikilink")(Methanobacteria)

1.  [甲烷桿菌目](../Page/甲烷桿菌目.md "wikilink")(Methanobacteriales)
    1.  [甲烷桿菌科](../Page/甲烷桿菌科.md "wikilink")(Methanobacteriaceae)
        1.  [甲烷桿菌屬](../Page/甲烷桿菌屬.md "wikilink")(*Methanobacterium*)
        2.  [甲烷短桿菌屬](../Page/甲烷短桿菌屬.md "wikilink")(*Methanobrevibacter*)
        3.  [甲烷球形菌屬](../Page/甲烷球形菌屬.md "wikilink")(*Methanosphaera*)
        4.  [甲烷熱桿菌屬](../Page/甲烷熱桿菌屬.md "wikilink")(*Methanothermobacter*)
    2.  [甲烷熱菌科](../Page/甲烷熱菌科.md "wikilink")(Methanothermaceae)
        1.  [甲烷熱菌屬](../Page/甲烷熱菌屬.md "wikilink")(*Methanothermus*)

### [甲烷球菌綱](../Page/甲烷球菌綱.md "wikilink")(Methanococci)

1.  [甲烷球菌目](../Page/甲烷球菌目.md "wikilink")(Methanococcales)
    1.  [甲烷暖球菌科](../Page/甲烷暖球菌科.md "wikilink")(Methanocaldococcaceae)
        1.  [甲烷暖球菌屬](../Page/甲烷暖球菌屬.md "wikilink")(*Methanocaldococcus*)
        2.  [甲烷炎菌屬](../Page/甲烷炎菌屬.md "wikilink")(*Methanotorris*)
    2.  [甲烷球菌科](../Page/甲烷球菌科.md "wikilink")(Methanococcaceae)
        1.  [甲烷球菌屬](../Page/甲烷球菌屬.md "wikilink")(*Methanococcus*)
        2.  [甲烷熱球菌屬](../Page/甲烷熱球菌屬.md "wikilink")(*Methanothermococcus*)

### [甲烷微菌綱](../Page/甲烷微菌綱.md "wikilink")(Methanomicrobia)

1.  [甲烷微菌目](../Page/甲烷微菌目.md "wikilink")(Methanomirobiales)
    1.  [甲烷粒菌科](../Page/甲烷粒菌科.md "wikilink")(Methanocorpusculaceae)
        1.  [甲烷粒菌屬](../Page/甲烷粒菌屬.md "wikilink")(*Methanocorpusculum*)
    2.  [甲烷微菌科](../Page/甲烷微菌科.md "wikilink")(Methanomicrobiaceae)
        1.  [甲烷囊菌屬](../Page/甲烷囊菌屬.md "wikilink")(*Methanoculleus*)
        2.  [甲烷泡菌屬](../Page/甲烷泡菌屬.md "wikilink")(*Methanofollis*)
        3.  [產甲烷菌屬](../Page/產甲烷菌屬.md "wikilink")(*Methanogenium*)
        4.  [甲烷裂葉菌屬](../Page/甲烷裂葉菌屬.md "wikilink")(*Methanolacinia*)
        5.  [甲烷微菌屬](../Page/甲烷微菌屬.md "wikilink")(*Methanomicrobium*)
        6.  [甲烷盤菌屬](../Page/甲烷盤菌屬.md "wikilink")(*Methanoplanus*)
    3.  [甲烷螺菌科](../Page/甲烷螺菌科.md "wikilink")(Methanospirillaceae)
        1.  [甲烷螺菌屬](../Page/甲烷螺菌屬.md "wikilink")(*Methanospirillum*)
    4.  **科未定**
        1.  [甲烷礫菌屬](../Page/甲烷礫菌屬.md "wikilink")(*Methanocalculus*)
        2.  [甲烷繩菌屬](../Page/甲烷繩菌屬.md "wikilink")(*Methanolinea*)
2.  [甲烷八疊球菌目](../Page/甲烷八疊球菌目.md "wikilink")(Methanosarcinales)
    1.  [甲烷鬃菌科](../Page/甲烷鬃菌科.md "wikilink")(Methanosaetaceae)
        1.  [甲烷鬃菌屬](../Page/甲烷鬃菌屬.md "wikilink")(*Methanosaeta*)
        2.  [甲烷髮菌屬](../Page/甲烷髮菌屬.md "wikilink")(*Methanothrix*)
    2.  [甲烷八疊球菌科](../Page/甲烷八疊球菌科.md "wikilink")(Methanosarcinaceae)
        1.  [鹽甲烷球菌](../Page/鹽甲烷球菌.md "wikilink")(*Halomethanococcus*)
        2.  [甲烷微球菌屬](../Page/甲烷微球菌屬.md "wikilink")(*Methanimicrococcus*)
        3.  [甲烷類球菌屬](../Page/甲烷類球菌屬.md "wikilink")(*Methanococcoides*)
        4.  [甲烷鹽菌屬](../Page/甲烷鹽菌屬.md "wikilink")(*Methanohalobium*)
        5.  [甲烷嗜鹽菌屬](../Page/甲烷嗜鹽菌屬.md "wikilink")(*Methanohalophilus*)
        6.  [甲烷葉菌屬](../Page/甲烷葉菌屬.md "wikilink")(*Methanolobus*)
        7.  [甲烷食甲基菌屬](../Page/甲烷食甲基菌屬.md "wikilink")(*Methanomethylovorans*)
        8.  [甲烷鹹菌屬](../Page/甲烷鹹菌屬.md "wikilink")(*Methanosalsum*)
        9.  [甲烷八疊球菌屬](../Page/甲烷八疊球菌屬.md "wikilink")(*Methanosarcina*)
    3.  [甲熱球菌科](../Page/甲熱球菌科.md "wikilink")(Methermicoccaceae)
        1.  [甲熱球菌屬](../Page/甲熱球菌屬.md "wikilink")(*Methermicoccus*)

### [甲烷火菌綱](../Page/甲烷火菌綱.md "wikilink")(Methanopyri)

1.  [甲烷火菌目](../Page/甲烷火菌目.md "wikilink")(Methanopyrales)
    1.  [甲烷火菌科](../Page/甲烷火菌科.md "wikilink")(Methanopyraceae)
        1.  [甲烷火菌屬](../Page/甲烷火菌屬.md "wikilink")(*Methanopyrus*)

### [熱球菌綱](../Page/熱球菌綱.md "wikilink")(Thermococci)

1.  [熱球菌目](../Page/熱球菌目.md "wikilink")(Thermococcales)
    1.  [熱球菌科](../Page/熱球菌科.md "wikilink")(Thermococcaceae)
        1.  [古老球菌屬](../Page/古老球菌屬.md "wikilink")(*Palaeococcus*)
        2.  [火球菌屬](../Page/火球菌屬.md "wikilink")(*Pyrococcus*)
        3.  [熱球菌屬](../Page/熱球菌屬.md "wikilink")(*Thermococcus*)

### [熱原體綱](../Page/熱原體綱.md "wikilink")(Thermoplasmata)

1.  [熱原體目](../Page/熱原體目.md "wikilink")(Thermoplasmatales)
    1.  [鐵原體科](../Page/鐵原體科.md "wikilink")(Ferroplasmaceae)（注：正確形式應爲Ferroplasmataceae）
        1.  [鐵原體屬](../Page/鐵原體屬.md "wikilink")(*Ferroplasma*)
    2.  [嗜苦菌科](../Page/嗜苦菌科.md "wikilink")(Picrophilaceae)
        1.  [嗜苦菌屬](../Page/嗜苦菌屬.md "wikilink")(*Picrophilus*)
    3.  [熱原體科](../Page/熱原體科.md "wikilink")(Thermoplasmataceae)
        1.  [熱原體屬](../Page/熱原體屬.md "wikilink")(*Thermoplasma*)
    4.  科未定
        1.  [熱裸單胞菌屬](../Page/熱裸單胞菌屬.md "wikilink")(*Thermogymnomonas*)

[category:微生物](../Page/category:微生物.md "wikilink")
[category:古菌](../Page/category:古菌.md "wikilink")

[Category:生物學列表](../Category/生物學列表.md "wikilink")