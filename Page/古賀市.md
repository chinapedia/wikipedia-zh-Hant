**古賀市**（）是位于[福岡縣西北部的一個](../Page/福岡縣.md "wikilink")[城市](../Page/城市.md "wikilink")，日語讀音與[茨城縣的](../Page/茨城縣.md "wikilink")[古河市同樣都為](../Page/古河市.md "wikilink")「Koga-shi」。

距離[福岡市約](../Page/福岡市.md "wikilink")15公里，為[福岡都市圏的一部份](../Page/福岡都市圏.md "wikilink")，屬於福岡市的[通勤城市](../Page/通勤城市.md "wikilink")。

西側面像[玄界灘](../Page/玄界灘.md "wikilink")，主要為松原與沙丘地形，北部與南部為丘陵地，東部則為山區。

## 歷史

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：[糟屋郡席內村](../Page/糟屋郡.md "wikilink")、小野村、青柳村。
  - 1938年4月17日：席內村改制並改名為**古賀町**。
  - 1955年4月1日：古賀町、小野村、青柳村[合併為新設置的](../Page/市町村合併.md "wikilink")**古賀町**。
  - 1997年10月1日：改制為**古賀市**。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1944年</p></th>
<th><p>1945年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>席內村</p></td>
<td><p>1938年4月17日<br />
改制並改名為古賀町</p></td>
<td><p>1955年4月1日<br />
古賀町</p></td>
<td><p>1997年10月1日<br />
古賀市</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>青柳村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>小野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")
      - [鹿兒島本線](../Page/鹿兒島本線.md "wikilink")：[鹿部車站](../Page/鹿部車站_\(福岡縣\).md "wikilink")
        - [古賀車站](../Page/古賀車站.md "wikilink") -
        [千鳥車站](../Page/千鳥車站.md "wikilink")

### 廢止鐵路

  - [西日本鐵道](../Page/西日本鐵道.md "wikilink")
      - [宮地岳線](../Page/西鐵貝塚線.md "wikilink")：[花見車站](../Page/花見車站.md "wikilink")
        - [西鐵古賀車站](../Page/西鐵古賀車站.md "wikilink") -
        [古賀高爾夫球場場前車站](../Page/古賀高爾夫球場場前車站.md "wikilink")

### 道路

#### 高速道路

  - [九州自動車道](../Page/九州自動車道.md "wikilink")
      - [古賀IC](../Page/古賀IC.md "wikilink")

#### 一般國道

  - [國道3號](../Page/國道3號_\(日本\).md "wikilink")
  - [國道495號](../Page/國道495號.md "wikilink")

#### 主要地方道

  - [福岡縣道35號筑紫野古賀線](../Page/福岡縣道35號筑紫野古賀線.md "wikilink")

### 巴士

  - [西鐵巴士宗像](../Page/西鐵巴士宗像.md "wikilink")

## 教育

### 大學

  - [福岡女學院看護大學](../Page/福岡女學院看護大學.md "wikilink")

### 高等學校

  - [福岡縣立玄界高等學校](../Page/福岡縣立玄界高等學校.md "wikilink")
  - [福岡縣公立古賀竟成館高等學校](../Page/福岡縣公立古賀竟成館高等學校.md "wikilink")

## 本地出身之名人

  - [酒井志穂](../Page/酒井志穂.md "wikilink")：[游泳選手](../Page/游泳.md "wikilink")
  - [吉村裕基](../Page/吉村裕基.md "wikilink")：[職業棒球選手](../Page/職業棒球.md "wikilink")
  - [篠塚廣夢](../Page/篠塚廣夢.md "wikilink")：[漫畫家](../Page/漫畫家.md "wikilink")、[魔法咪路咪路的原作](../Page/魔法咪路咪路.md "wikilink")
  - [博多大吉](../Page/博多大吉.md "wikilink")：[漫才師](../Page/漫才師.md "wikilink")
  - [中島卓偉](../Page/中島卓偉.md "wikilink")：[創作歌手](../Page/創作歌手.md "wikilink")
  - [YUI](../Page/YUI_\(藝人\).md "wikilink")：創作歌手

## 外部連結

  - [古賀市商工會](http://www.koga-cci.org/)