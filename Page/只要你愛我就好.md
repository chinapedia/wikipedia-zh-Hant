《**只要你愛我就好**》（）是[新好男孩的一首歌曲](../Page/新好男孩.md "wikilink")，最初收錄進了[同名專輯的美國版](../Page/新好男孩_\(美國版\).md "wikilink")，隨後又收入第二張專輯[新好男孩回來了](../Page/新好男孩回來了.md "wikilink")。歌曲於1997年9月作為單曲在國際發行，同年11月在美國發行。歌曲獲得了很大的商業成功，在[紐西蘭和](../Page/紐西蘭.md "wikilink")[菲律賓市場曾排入銷售榜第一名](../Page/菲律賓.md "wikilink")；在[澳大利亞與](../Page/澳大利亞.md "wikilink")[奧地利則排名第二](../Page/奧地利.md "wikilink")；在英國排名第三；[瑞士與](../Page/瑞士.md "wikilink")[瑞典第四](../Page/瑞典.md "wikilink")。\[1\]

儘管在美國並未商業發行，單曲的錄影帶在美國流傳甚廣，而各大電台也積極播放這首曲子。單曲在[Billboard廣播百名榜上持續了](../Page/Billboard廣播百名榜.md "wikilink")56週，最高時曾達到第四位。

## 曲目

1.
2.
3.
4.
## 音樂錄影帶

[Alaylmcdcover.jpg](https://zh.wikipedia.org/wiki/File:Alaylmcdcover.jpg "fig:Alaylmcdcover.jpg")
單曲的錄影帶由[尼吉·迪克導演](../Page/尼吉·迪克.md "wikilink")，情節是男孩們參與一次試鏡，他們表演了「椅子舞」，並穿著不同的服裝。音樂達到高潮時，男孩與一開始評估他們的幾個女人互換了位置。音樂結束後，他們順次離開了屋子。

## 參考資料

[Category:新好男孩歌曲](../Category/新好男孩歌曲.md "wikilink")
[Category:1997年歌曲](../Category/1997年歌曲.md "wikilink")
[Category:马克斯·马丁制作的歌曲](../Category/马克斯·马丁制作的歌曲.md "wikilink")
[Category:马克斯·马丁创作的歌曲](../Category/马克斯·马丁创作的歌曲.md "wikilink")

1.  [1](http://charts.org.nz/showitem.asp?interpret=Backstreet+Boys&titel=As+Long+As+You+Love+Me&cat=s)