**只要我長大**是[臺灣早期風靡一時的愛國歌曲](../Page/臺灣.md "wikilink")，[白景山詞曲](../Page/白景山.md "wikilink")。1950年，[中華文藝獎金委員會舉辦徵曲比賽](../Page/中華文藝獎金委員會.md "wikilink")，白景山以《只要我長大》四段詞曲獲第一名；1952年，收於《新選歌謠》第6期，後由[中央廣播事業管理處以](../Page/中央廣播事業管理處.md "wikilink")「中華愛國歌曲」為題灌錄唱片。原版歌詞具有反共的[政治意識形態](../Page/政治意識形態.md "wikilink")。\[1\]\[2\]之後的新版歌詞，以《哥哥爸爸真偉大》兒歌面貌，傳唱各方，但大部分都只唱第一段歌詞。\[3\]\[4\]

## 歌詞

### 舊版

### 新版

## 參見

  - [反攻大陸去](../Page/反攻大陸去.md "wikilink")
  - [反共抗俄歌](../Page/反共抗俄歌.md "wikilink")

## 參考來源

## 外部連結

  -
[Category:1950年歌曲](../Category/1950年歌曲.md "wikilink")
[Category:1950年台灣](../Category/1950年台灣.md "wikilink")
[Category:中华民国政治歌曲](../Category/中华民国政治歌曲.md "wikilink")
[Category:臺灣兒歌](../Category/臺灣兒歌.md "wikilink")
[Category:剿匪题材作品](../Category/剿匪题材作品.md "wikilink")

1.
2.
3.
4.