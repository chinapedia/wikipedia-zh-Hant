**公**，**[姬](../Page/姬姓.md "wikilink")**[姓](../Page/姓.md "wikilink")，名**奭**，是[周武王的同姓](../Page/周武王.md "wikilink")[宗室](../Page/宗室.md "wikilink")，[食邑於召](../Page/食邑.md "wikilink")（今[陝西](../Page/陝西.md "wikilink")[歧山西南](../Page/歧山.md "wikilink")），谓之召公，又稱為**周召公**、**召康公**或**召伯**。又因其受封於燕國，稱**燕召公**。召公奭是後來[燕國和](../Page/燕國.md "wikilink")[召國的始祖](../Page/召國.md "wikilink")。

## 簡介

召公奭曾輔助[武王克殷](../Page/武王克殷.md "wikilink")，被封於燕（今[河北北部](../Page/河北.md "wikilink")），都城在[薊](../Page/薊.md "wikilink")（今[北京](../Page/北京.md "wikilink")），但並未前往，由[长子](../Page/長男.md "wikilink")[克前往就任](../Page/燕侯克.md "wikilink")。而他自己留在[召國](../Page/召國.md "wikilink")（今[陝西](../Page/陝西.md "wikilink")[歧山西南](../Page/歧山.md "wikilink")）。

召公奭是周初活跃时间最长的政治家，历经文武成康四世，[周成王時](../Page/周成王.md "wikilink")，位列[三公](../Page/三公.md "wikilink")，擔任[太保的職務](../Page/太保.md "wikilink")，他和[周公旦把周朝一分為二](../Page/周公旦.md "wikilink")，[陝東](../Page/陝東.md "wikilink")（今[河南省](../Page/河南省.md "wikilink")[陝縣以東](../Page/陝縣.md "wikilink")）由周公管理為東伯侯，[陝西由召公奭管理為西伯侯](../Page/陝西.md "wikilink")。他多半在[鎬京](../Page/鎬京.md "wikilink")（今[陝西省](../Page/陝西省.md "wikilink")[西安市](../Page/西安市.md "wikilink")[長安區](../Page/長安區.md "wikilink")）的[朝廷理政](../Page/朝廷.md "wikilink")，治下“自侯伯至庶人各得其所，無失職者”。

傳說召公奭曾在一棵甘棠樹下辦公聽訟，後世思其人而敬其樹。《[詩經](../Page/詩經.md "wikilink")·召南·甘棠》：“蔽甘棠，勿剪勿伐！召伯所。蔽芾甘棠，勿剪勿败！召伯所憩。蔽芾甘棠，勿剪勿拜！召伯所。”就是描述這個故事，也留下“甘棠遗爱”的[成語典故](../Page/成語.md "wikilink")。

## 参考文献

  - 《[史記](../Page/史記.md "wikilink")·燕召公世家》

{{-}}

[Category:西周人](../Category/西周人.md "wikilink")
[Category:周朝诸侯国君主](../Category/周朝诸侯国君主.md "wikilink")
[Category:燕國君主](../Category/燕國君主.md "wikilink")