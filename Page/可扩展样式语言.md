**可扩展样式语言**（），是一种为[可扩展置标语言](../Page/可扩展置标语言.md "wikilink")（XML）提供表达形式而设计的[计算机语言](../Page/计算机语言.md "wikilink")。

由于可扩展置标语言的扩展性使之没有关于显示格式的标识，可扩展样式语言可以选择和过滤可扩展置标语言中的数据，并将其转换为HTML或者PDF等其他格式文件。

可扩展样式语言可以分为三部分：[XSLT](../Page/XSLT.md "wikilink")（XSL
Transformations）、[XSL-FO](../Page/XSL-FO.md "wikilink")（XSL
Formatting Objects）和[XPath](../Page/XPath.md "wikilink")（XML Path
Language）。

[CSS可以完成与XSL类似的功能](../Page/CSS.md "wikilink")。对于[XML](../Page/XML.md "wikilink")，XSL是首选的样式语言。

## 历史

## 构成可扩展样式语言的技术

### XSL Transformations

### XML Path Language

### XSL Formatting Objects

## 参考文献

## 外部链接

  - [The Extensible Stylesheet Language Family
    (XSL)](http://www.w3.org/Style/XSL/) -
    [W3C中关于XSL的条目](../Page/World_Wide_Web_Consortium.md "wikilink")
  - [Antenna House公司中的条目](../Page/Antenna_House.md "wikilink")
      - [](http://www.antenna.co.jp/XSL-FO/data/recommend.htm)
      - [XML資料室](http://www.antenna.co.jp/XML/xmllist/)
  - [What is
    XSL-FO?](http://www.xml.com/pub/a/2002/03/20/xsl-fo.html?page=1) -
    O'REILLY XML.com
  - [XML Focus Topics : CSS, XSL,
    XSL-FO](http://www.xml.org/xml/resources_focus_cssxslfo.shtml) -
    XML.org

## 参见

  - [Extensible Markup Language（XML）](../Page/XML.md "wikilink")
  - [Style Sheet](../Page/Style_Sheet.md "wikilink")
      - XSL
          - [XSL Transformations（XSLT）](../Page/XSLT.md "wikilink")
          - [XML Path Language（XPath）](../Page/XPath.md "wikilink")
          - [XSL Formatting
            Objects（XSL-FO）](../Page/XSL-FO.md "wikilink")
      - [Document Style Semantics and Specification
        Language（DSSSL）](../Page/DSSSL.md "wikilink")
      - [Cascading Style Sheets（CSS）](../Page/CSS.md "wikilink")
  - [TeX](../Page/TeX.md "wikilink")
      - [LaTeX](../Page/LaTeX.md "wikilink")

{{-}}

[Category:W3C标准](../Category/W3C标准.md "wikilink")
[Category:标记语言](../Category/标记语言.md "wikilink")
[Category:XML](../Category/XML.md "wikilink")