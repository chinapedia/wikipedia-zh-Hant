[Korokke.jpg](https://zh.wikipedia.org/wiki/File:Korokke.jpg "fig:Korokke.jpg")
[Korokke2.jpg](https://zh.wikipedia.org/wiki/File:Korokke2.jpg "fig:Korokke2.jpg")
[Korokke_in_curry_sauce.jpg](https://zh.wikipedia.org/wiki/File:Korokke_in_curry_sauce.jpg "fig:Korokke_in_curry_sauce.jpg")
**可樂餅**（，又稱**-{zh-hans:吉乐饼;zh-hk:可樂餅;zh-tw:吉樂餅;}-**），是一種起源於十六世紀法國的油炸食物，後來流傳並流行於世界各地（尤其日本）。作法為將[馬鈴薯煮軟弄成泥狀](../Page/馬鈴薯.md "wikilink")，並將各樣配料混合後，揉成圓餅狀沾上少許[麵粉後刷上蛋汁](../Page/麵粉.md "wikilink")，再裹上麵包糠以[油炸烹調](../Page/油炸.md "wikilink")。配料可以有多種樣式，如[絞肉](../Page/絞肉.md "wikilink")、[螃蟹肉](../Page/螃蟹.md "wikilink")、[海鮮](../Page/海鮮.md "wikilink")、[洋蔥](../Page/洋蔥.md "wikilink")、[蔬菜](../Page/蔬菜.md "wikilink")、[咖哩等](../Page/咖哩.md "wikilink")。

## 起源

此種炸餅類菜式起源自[法國菜](../Page/法國菜.md "wikilink")，其原文名稱croquette是從動詞「croquer」變化而來，意思是形容咬下酥脆的食物時，齒間所發出的聲音，這一個詞貼切的符合可樂餅外皮是由麵包粉包著馬鈴薯泥，下鍋炸成金黃酥脆的口感。法國的炸肉餅是魚肉、雞肉的絞肉等混合的油炸物。然而也有看過搗碎馬鈴薯並裹上麵包粉的油炸物的。

可樂餅最早的記載是1661年法皇[路易十四御廚所手寫的食譜](../Page/路易十四.md "wikilink")，內餡以以[松露](../Page/松露.md "wikilink")、牛羊內臟及[奶油芝士搗碎混合製作](../Page/奶油芝士.md "wikilink")，再以[麵包糠及](../Page/麵包糠.md "wikilink")[馬鈴薯包起炸成](../Page/馬鈴薯.md "wikilink")。隨著[波旁王朝於](../Page/波旁王朝.md "wikilink")[法國大革命倒台](../Page/法國大革命.md "wikilink")，可樂餅與其他所有法式宮廷菜一樣飛入尋常百姓家，成為了平民百姓餐桌上的的家常小菜。隨著法國美食文化在歐洲的傳播，可樂餅也順理成章地傳到了歐陸各地，例如[荷蘭也有叫作](../Page/荷蘭.md "wikilink")「kroket」的料理，不只有用[白汁](../Page/白汁.md "wikilink")、亦有用[馬鈴薯](../Page/馬鈴薯.md "wikilink")。隨著[日本於](../Page/日本.md "wikilink")[明治維新期間大行西化](../Page/明治維新.md "wikilink")，可樂餅作為西菜的一種從歐洲傳入日本，當時傳入的可樂餅口味是奶油風味的可樂餅，到了大正時期和豬排、咖哩並稱日本三大[洋食](../Page/洋食.md "wikilink")，豐富了日式料理的內涵，也逐漸發展出各種口味。因此，當可樂餅透過日本飲食文化的擴展而傳入包括[台灣在內的其他亞洲地區時](../Page/台灣.md "wikilink")，常被誤以為是正統的日式料理而不知其發展自歐洲的起源\[1\]。

## 各國變體

### 荷蘭

在荷蘭有可以吃到可樂餅，荷蘭文的可樂餅寫成「kroket」，是法文croquette
的荷式拼法。在荷蘭的飲食文化中，荷蘭人熱愛油炸食物，除了他們常吃的薯條外，可樂餅也是他們所喜愛的食物；荷式風味的可樂餅除了在荷蘭路邊的店家可看見外，超市也有賣可樂餅。另外比較特別的是，荷蘭有路邊的自動點心販賣機「snack
automaat」裡面也有可樂餅這個選項。甚至荷蘭的麥當勞也有販賣稱作[McKroket可樂餅漢堡](../Page/McKroket.md "wikilink")，而荷蘭的可樂餅在外觀上，與日本、台灣的可樂餅形狀不同，荷蘭的可樂餅在外觀形狀上有甚多種類，自動販賣機所賣的可樂餅大多為長條圓柱型，以方便拿取\[2\]。

在烹飪方面，荷蘭對於可樂餅中的餡料是隨意加入的，包括冰箱剩下的食材，例如剩下的燉牛肉、魚片、[起司等](../Page/起司.md "wikilink")。其中最經典的牛肉可樂餅（rundervleeskroket）是由燉牛肉與奶油白醬製成的，還有很受荷蘭人歡迎的爆漿起司可樂餅（kaassouffle）。荷蘭人的可樂餅也會加入當季的食材，例如蝦可樂餅（ganaarlkroket）和蘆筍可樂餅（aspergekroket）。最特別的是在荷蘭可以吃到印尼炒飯可樂餅與炒麵可樂餅（nasikroket
&
bamikroket），酥脆的麵衣裡面包裹著貨真價實的麵與飯，這或許可能是印尼曾經為荷蘭的殖民地，荷蘭飲食文化也深受印尼料理所影響。在荷蘭，平時人們也吃炒飯炒麵料理，所以印尼炒飯可樂餅與炒麵可樂餅也許就是荷蘭人將法國和印尼料理結合，而產生的當地獨有的可樂餅風味。此外荷蘭人的可樂餅除了單獨食用外，還有搭配吐司一起吃：將壓平的可樂餅在平均對切的吐司上抹開，再抹上黃芥末醬。\[3\]

### 義大利

可樂餅在[義大利文中称为](../Page/義大利.md "wikilink")「Crocchette」。義大利人主食為[米飯](../Page/米飯.md "wikilink")，常將米飯調味入味，但是食用方式很特別，他們將米飯和可樂餅結合，製作成米飯可樂餅，外型與荷蘭很像，但義式可樂餅形狀比較短，也比較圓。\[4\]

### 日本

從[大正時代起](../Page/大正.md "wikilink")，可樂餅開始成為是常見的家庭料理，之後甚至在肉店都有販售可樂餅，最早的紀錄在昭和二年(1927年)銀座肉店Choshi首創兼賣可樂餅\[5\]，所以現在日本街上的肉店、超市、可樂餅專賣店都有販售。可樂餅在日本，可以做為配菜，咖哩可樂餅飯，可樂餅式配菜與咖哩飯一起食用，也可以像三明治一樣被夾入兩片麵包中間一起食用，甚至可搭配自製的醬汁或醬油食用。在料理方面，會用馬鈴薯泥，加入炒過的洋蔥和肉末，捏成圓柱狀再進行煎炸，在食材的選用上日本北海道的男爵馬鈴薯是很好的食材，容易煮溶，適合用來製作可樂餅。日本常見的可樂餅口味有牛肉可樂餅、咖哩可樂餅、蔬菜可樂餅、[南瓜可樂餅](../Page/南瓜.md "wikilink")。

### 中華民國（台灣）

台灣人將可樂餅發展成鹹甜客家台式和其他口味的可樂餅，不論餐廳、小吃或[夜市都可看到](../Page/夜市.md "wikilink")\[6\]。

### 西班牙

可樂餅在西班牙是一種常見的家常食物，也是常見的下酒小菜
tapas，西班牙文的可樂餅寫成「croquetas」，但是，不是用馬鈴薯做的，西班牙可樂餅是用白醬Bechamel再加入其他餡料而成，餡料可以是雞肉、火腿肉、墨魚跟墨魚汁、鱈魚
....。\[7\]

## 法式與日式可樂餅不同之處

法式可樂餅不同於日式可樂餅，日式可樂餅內餡較多為馬鈴薯，法式可樂餅則多以肉為主體，分量紮實也更有飽足感，烘調為一半煎一半炸的方式，樣式保留傳統，較少樣式上的變化。

## 書籍

日本的[小說家](../Page/小說家.md "wikilink")[村井弦齋在](../Page/村井弦齋.md "wikilink")1903年的暢銷書《食道樂》\[8\]中有記載可樂餅的[食譜](../Page/食譜.md "wikilink")\[9\]。

## 參考

<references />

[Category:馬鈴薯食品](../Category/馬鈴薯食品.md "wikilink")
[Category:日本小吃](../Category/日本小吃.md "wikilink")
[Category:混合菜](../Category/混合菜.md "wikilink")
[Category:油炸食品](../Category/油炸食品.md "wikilink")

1.

2.
3.  WEI FU(民102年4月9日)。荷蘭 好食分享
    烏特勒支。取自http://weifuweifu.blogspot.tw/2012/09/oudaen.html

4.  趙默默(民97年11月18日)。原來這是正宗的義式可樂餅啊。取自
    <http://liebemonika.pixnet.net/blog/post/22468046-so-this-is-italian-%22croche%22!-%E5%8E%9F%E4%BE%86%E9%80%99%E6%98%AF%E6%AD%A3%E5%AE%97%E7%BE%A9%E5%BC%8F%22%E5%8F%AF%E6%A8%82>

5.  藍秀朗(民104年1月19日)。朗談日本:可樂餅【新聞群組】。取自
    <http://the-sun.on.cc/cnt/lifestyle/20150119/00498_001.html>

6.  江之融、黃若珊。國民美食風靡全台魅力無窮-黃金可樂餅【新聞群組】。取自
    <http://www.ytower.com.tw/prj/prj_300/p1.asp?serno=308>

7.  <https://www.facebook.com/spain.info/posts/10151915599428962>

8.  [食道樂
    秋の巻](http://kindai.ndl.go.jp/BIImgFrame.php?JP_NUM=41008841&VOL_NUM=00003&KOMA=1&ITYPE=0)

9.  [食道樂
    秋の巻154ページ](http://kindai.ndl.go.jp/BIImgFrame.php?JP_NUM=41008841&VOL_NUM=00003&KOMA=93&ITYPE=0)