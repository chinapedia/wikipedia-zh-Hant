[Calpis_bottles.jpg](https://zh.wikipedia.org/wiki/File:Calpis_bottles.jpg "fig:Calpis_bottles.jpg")的可爾必思\]\]
**可爾必思**（）是一種[乳酸菌](../Page/乳酸菌.md "wikilink")[飲料](../Page/飲料.md "wikilink")，為[日本飲料製造商](../Page/日本.md "wikilink")[可爾必思株式會社的主要飲料產品](../Page/可爾必思株式會社.md "wikilink")。

可爾必思株式會社位在[東京都](../Page/東京都.md "wikilink")[澀谷區](../Page/澀谷區.md "wikilink")，英文為，曾在1991年時加入[味之素集團旗下](../Page/味之素.md "wikilink")，現為[朝日飲料集團成員](../Page/朝日啤酒.md "wikilink")。可爾必思的名稱同時也是該公司的[註冊商標](../Page/註冊商標.md "wikilink")。

## 企業

可爾必思企業的創業者為[僧侶出身的](../Page/僧侶.md "wikilink")[三島海雲](../Page/三島海雲.md "wikilink")。創業初期當時為[國分集團的成員](../Page/國分集團.md "wikilink")。父母是山田耕筰與渡边海旭（當時為芝學園的校長）。創業時以「初戀的味道」為宣傳，生產全世界第一瓶的[乳酸菌飲料](../Page/乳酸菌飲料.md "wikilink")「可爾必思」。同時也生產[脫脂牛奶和可爾必思奶油作為主力商品](../Page/脫脂牛奶.md "wikilink")。

在納入味之素旗下後，近年推出將可爾必思與水調和稀釋的飲料「可爾必思水語」（），並在[日本進口](../Page/日本.md "wikilink")[礦泉水](../Page/礦泉水.md "wikilink")「evian」和[葡萄酒](../Page/葡萄酒.md "wikilink")，也發售[雞尾酒](../Page/雞尾酒.md "wikilink")「可爾必思[沙瓦](../Page/沙瓦.md "wikilink")」（）進入[酒精飲料市場](../Page/酒精飲料.md "wikilink")。

此外該公司也有生產其他種類的飲料。

由於Calpis的發音，在英語中近似「Cow
piss」（牛的尿），為了避免讓顧客有不良的聯想，因此在英語國家中改用「CalPico」的名字銷售。

### 年表

  - 1917年 - 正式創業。
  - 1948年 - 成立可爾必思食品工業株式會社（）。
  - 1989年 - 停止使用「黑人標誌」。
  - 1991年 -
    進行第三人[增資](../Page/增資.md "wikilink")[釋股](../Page/釋股.md "wikilink")，味之素集團成為最大股東。
  - 1997年 - 公司名稱改為現在的可爾必思株式會社。

### 起源

1902年，年僅25歲的三島海雲造訪[中國](../Page/中國.md "wikilink")[內蒙古](../Page/內蒙古.md "wikilink")，以當地的一種類似[酸奶的飲料為基礎](../Page/酸奶.md "wikilink")，於1919年開發出了可爾必思。三島也被傳授「公司發售的第一種飲料要與公司同名」的商業技巧。以脱脂牛奶[發酵後成為的](../Page/發酵.md "wikilink")「酸乳」，在發酵過程中加入[糖](../Page/糖.md "wikilink")、[馬奶酒類似的](../Page/馬奶酒.md "wikilink")[酵母菌](../Page/酵母菌.md "wikilink")，這些都是造成可爾必思獨特風味不可或缺的要素，同時也是企業長期以來的祕密，但在1990年中公開。

公司名稱的由來是「[鈣](../Page/鈣.md "wikilink")」（、**Cal**cium）加上[梵語中的सर](../Page/梵語.md "wikilink")
पिस「、sal**pis**」（漢字翻譯為「熟酥」，第二種味道之意）組合而來。

也曾有以梵語中「sarpir-manda」（日音：，為[醍醐](../Page/醍醐.md "wikilink")、無上的味道之意）為基礎，用「」（音譯：薩爾必思）、「」（音譯：可爾必魯）當作公司名的提案，但公司中這方面的專家、同時也是音樂專家的山田耕筰認為還是「」作為名稱較佳也比較響亮，因此就成為公司和主力飲料的名字。

原本的商標是「一名戴著[巴拿馬草帽的](../Page/巴拿馬草帽.md "wikilink")[黑人男性](../Page/黑人.md "wikilink")，正在用吸管喝著玻璃瓶中的可爾必思」的圖案。這是在[第一次世界大戰後](../Page/第一次世界大戰.md "wikilink")，為了幫助困苦的[德國](../Page/德國.md "wikilink")[畫家](../Page/畫家.md "wikilink")，由社長三島海雲舉辦的「國際海報大獎」（）中選出的第三名作品。但在1989年後被批評「有[種族歧視思想](../Page/種族歧視.md "wikilink")」，因而更換成現在的商標。

## 外部連結

  - [可爾必思台灣粉絲團](https://www.facebook.com/calpis.tw/)
  - [可爾必思英文網站](http://www.calpis.net/)
  - [日本可爾必思官方網站](http://www.calpis.co.jp/)

[Category:软饮料](../Category/软饮料.md "wikilink")
[Category:日本食品公司](../Category/日本食品公司.md "wikilink")
[Category:飲料公司](../Category/飲料公司.md "wikilink")
[0](../Category/東京證券交易所上市公司.md "wikilink")