**可風中學（嗇色園主辦）**（）是[香港一所集](../Page/香港.md "wikilink")[儒](../Page/儒.md "wikilink")、[釋](../Page/釋.md "wikilink")、[道文化於一身的](../Page/道.md "wikilink")[中學](../Page/中學.md "wikilink")，辦學團體為[嗇色園](../Page/嗇色園.md "wikilink")。學校創建於1974年，位於[新界](../Page/新界.md "wikilink")[荃灣區](../Page/荃灣區.md "wikilink")[上葵涌](../Page/上葵涌.md "wikilink")[和宜合道](../Page/和宜合道.md "wikilink")448號，鄰近[梨木樹邨和](../Page/梨木樹邨.md "wikilink")[城門隧道轉車站](../Page/城門隧道.md "wikilink")，以英語授課。此中學亦有不同的名人畢業生。

## 辦學理念

可風中學的辦學慈善團體[嗇色園](../Page/嗇色園.md "wikilink")「普濟勸善」的精神，學校致力為學生提供優質教育。除了讓學生在學業上達致極高的水平，更重視學生的全面和均衡發展，令其品格、思維、能力、體魄和氣質風度均能兼善。

## 歷任校長

  - [陳昌立先生](../Page/陳昌立.md "wikilink")（1974年－2002年）
  - [凌健真先生](../Page/凌健真.md "wikilink")（2002年－2005年）
  - [伍瑞球先生](../Page/伍瑞球.md "wikilink")（2005年－2012年）
  - [蕭志新先生](../Page/蕭志新.md "wikilink")（2012年至今）

## 校園設施

可風中學是在[新界](../Page/新界.md "wikilink")[梨木樹](../Page/梨木樹.md "wikilink")，校舍位於[和宜合道及](../Page/和宜合道.md "wikilink")[城門道交界](../Page/城門道.md "wikilink")，地址和宜合道448號。校內設施如下：

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>28個普通課室</li>
<li>3個輔導教學室</li>
<li>5個實驗室
<ul>
<li>2個綜合科學實驗室</li>
<li>1個生物實驗室</li>
<li>1個物理實驗室</li>
<li>1個化學實驗室</li>
</ul></li>
<li>2個電腦室</li>
<li>1個多媒體學習中心</li>
<li>地理室</li>
</ul></td>
<td><ul>
<li>音樂室</li>
<li>視覺藝術室</li>
<li>設計與科技室</li>
<li>家政室</li>
<li>童軍房</li>
<li>中樂室</li>
<li>小食部</li>
<li>昌立堂（飯堂）</li>
<li>學生會房</li>
<li>健身室</li>
<li>樂隊練習室</li>
<li>學生活動室</li>
</ul></td>
<td><ul>
<li>禮堂</li>
<li>校務處</li>
<li>校長室</li>
<li>4個教員室</li>
<li>2個副校長室</li>
<li>教師休息室</li>
<li>操場
<ul>
<li>籃球場</li>
<li>手球場</li>
<li>羽毛球場</li>
</ul></li>
<li>有蓋操場</li>
<li>圖書館</li>
<li>家長教師會資源室</li>
<li>醫療室</li>
<li>社工室</li>
<li>空中花園</li>
<li>溫室</li>
<li>停車場</li>
</ul></td>
</tr>
</tbody>
</table>

## 開設科目

### 初中課程

  - 以中文授課：中文、中史、普通話、經訓（中三）
  - 以英文授課：英文、數學、綜合科學（中一、二）、化學（中三）、物理（中三）、生物（中三）、地理、歷史、生活與社會、視覺藝術、電腦與科技、音樂、科技與生活、體育

### 高中課程

  - 以中文授課：中文、通識教育、中史、中國文學、經訓
  - 以英文授課：英文、通識教育、數學、數學附加部份（M1、M2）、化學、物理、生物、地理、歷史、經濟、企業會計與財務概論、資訊與通訊科技
  - 常規課程以外選修科：音樂、視覺藝術、體育（文憑試）

## 課外活動

超過40個學會和校隊\[1\]，範疇涵蓋學術、藝術、體育、社會服務。

## 學生會

可風中學學生會成立於1989年，歷屆內閣如下：

  - 1997-1998年度：曉雋閣
  - 1998-1999年度：蔚風閣
  - 2001-2002年度：蔚風閣
  - 2002-2003年度：自動治閣
  - 2003-2004年度：聯合閣
  - 2004-2005年度：你想閣（Dream Cabinet）
  - 2005-2006年度：利家閣（Ricacorp Cabinet）
  - 2006-2007年度：好望閣（Good Hope Cabinet）
  - 2007-2008年度：赤臘角（Chek Lap Kok）
  - 2008-2009年度：Triangle 叄角
  - 2009-2010年度：361° 閣
  - 2010-2011年度：聯合閣（Unimate）
  - 2011-2012年度：HoFungHolic 風狂
  - 2012-2013年度：Nova
  - 2013-2014年度：Canvas
  - 2014-2015年度：Apex
  - 2015-2016年度：Colibri（風鳥）
  - 2016-2017年度：IOTA (微小)
  - 2017-2018年度：Apricity
  - 2018-2019年度：Apeiron

## 校歌

### 黃大仙頌

<div style="text-align: center;">

**<u>黃大仙頌**</u>

</div>

## 電影電視拍攝場地

  - [無綫電視八十年代校園劇](../Page/無綫電視.md "wikilink")《[中四丁班](../Page/中四丁班.md "wikilink")》，演員包括[吳啓華](../Page/吳啓華.md "wikilink")、[王書麒](../Page/王書麒.md "wikilink")、[陳敏兒](../Page/陳敏兒.md "wikilink")、[李克勤](../Page/李克勤.md "wikilink")、[張衛健](../Page/張衛健.md "wikilink")、[任達華](../Page/任達華.md "wikilink")、[莊靜而](../Page/莊靜而.md "wikilink")、[王書麒等](../Page/王書麒.md "wikilink")。
  - [香港電台劇集](../Page/香港電台.md "wikilink")《完全學生手冊》，演員包括[謝霆鋒](../Page/謝霆鋒.md "wikilink")、[黃浩然](../Page/黃浩然.md "wikilink")、[黃翠如等](../Page/黃翠如.md "wikilink")。

## 香港傑出學生選舉

直至2018年(第33屆)，可風中學（嗇色園主辦）在[香港傑出學生選舉共出產](../Page/香港傑出學生選舉.md "wikilink")2名傑出學生。

## 校友

  - 醫護界

<!-- end list -->

  - [陳燕萍](../Page/陳燕萍_\(香港\).md "wikilink")：[香港紅十字會醫護服務部主管](../Page/香港紅十字會.md "wikilink")

<!-- end list -->

  -
    教育界

<!-- end list -->

  - [游子安](../Page/游子安.md "wikilink")：[香港中文大學文化及宗教研究系助理教授](../Page/香港中文大學.md "wikilink")
  - [蕭源](../Page/蕭源.md "wikilink")：現代教育前中文補習老師\[2\] (於2018年6月被起訴而離職)
  - [林溢欣](../Page/林溢欣.md "wikilink")：[遵理學校中文補習老師](../Page/遵理學校.md "wikilink")\[3\]
  - 文靜：現代教育中文補習老師
  - [陳子瑛](../Page/陳子瑛.md "wikilink")：[遵理學校創校校長兼總校長](../Page/遵理學校.md "wikilink")（約1980年中七畢業）
  - [葉德平](../Page/葉德平.md "wikilink")：[香港中文大學專業進修學院講師](../Page/香港中文大學.md "wikilink")、[香港歷史文化研究會副會長](../Page/香港歷史文化研究會.md "wikilink")、[香港作家聯會會員](../Page/香港作家聯會.md "wikilink")。

<!-- end list -->

  -
    演藝與傳播界

<!-- end list -->

  - [鄧鈞耀](../Page/鄧鈞耀.md "wikilink")（Dawn）：香港歌手，前組合[Faith成員](../Page/Faith_\(組合\).md "wikilink")
  - [呂爵安](../Page/呂爵安.md "wikilink")（Edan）：香港[Mirror](../Page/MIRROR.md "wikilink")
    組合成員\[4\]
  - 侯肇琪：前[Now新聞台記者](../Page/Now新聞台.md "wikilink")
  - 林亦：[Now新聞台記者](../Page/Now新聞台.md "wikilink")
  - [余采霖](../Page/余采霖.md "wikilink")：[ViuTV娛樂新聞記者](../Page/ViuTV.md "wikilink")
  - [丘雨勤](../Page/丘雨勤.md "wikilink")：[Now寬頻電視體育頻道](../Page/Now寬頻電視.md "wikilink")
    [now Sports](../Page/now_Sports.md "wikilink")
    體育節目主持和及評述，曾於[香港無綫電視擔任體育記者和主持](../Page/香港無綫電視.md "wikilink")\[5\]

;

## 参考文献

## 外部連結

  - [嗇色園官方網站](http://www.siksikyuen.org.hk/)

[Category:梨木樹](../Category/梨木樹.md "wikilink")
[Category:嗇色園中學](../Category/嗇色園中學.md "wikilink")
[H](../Category/荃灣區中學.md "wikilink")
[Category:香港孔教學校](../Category/香港孔教學校.md "wikilink")
[Category:香港佛教學校](../Category/香港佛教學校.md "wikilink")
[Category:香港道教學校](../Category/香港道教學校.md "wikilink")
[Category:1974年創建的教育機構](../Category/1974年創建的教育機構.md "wikilink")
[Category:香港英文授課中學](../Category/香港英文授課中學.md "wikilink")

1.
2.  [蕭源點只凍檸茶咁簡單](http://orientaldaily.on.cc/cnt/sport/20090411/00286_007.html)《東方日報》2009年4月11日
3.  [天王溫?](http://www.singtaousa.com/550807/post-%E5%A4%A9%E7%8E%8B%E6%BA%AB/?variant=zh-hk&fs=16)
    《星島日報》2015年10月24日
4.
5.  [Edcon Yau's alma mater, print screened
    on 2018-6-7](https://drive.google.com/file/d/1mPdAETYqOn_AGv3beiNqOEtw-4aId46c/view?usp=sharing)