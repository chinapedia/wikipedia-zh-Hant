**-{台北}-地下街**位於[臺灣](../Page/臺灣.md "wikilink")[台北市](../Page/台北市.md "wikilink")[中正區與](../Page/中正區_\(臺北市\).md "wikilink")[大同區交界處](../Page/大同區_\(臺北市\).md "wikilink")，於2000年3月29日啟用\[1\]，是台北市境內眾多[地下商店街之一](../Page/地下街.md "wikilink")。

## 歷史

[Rion_from_Taipei_City_Mall_20120210.jpg](https://zh.wikipedia.org/wiki/File:Rion_from_Taipei_City_Mall_20120210.jpg "fig:Rion_from_Taipei_City_Mall_20120210.jpg")
[台北地下街的九藏喵窩海報.jpg](https://zh.wikipedia.org/wiki/File:台北地下街的九藏喵窩海報.jpg "fig:台北地下街的九藏喵窩海報.jpg")

  - 1992年10月：[台北市政府決定拆除](../Page/台北市政府.md "wikilink")[中華商場](../Page/中華商場.md "wikilink")。
  - 2000年3月29日：-{台北}-地下街開幕，安置原先中華商場810戶承租戶。
  - 2004年3月15日：[站前地下街正式對外營運](../Page/站前地下街.md "wikilink")，安置原先中華商場另外254戶承租戶。
  - 2011年10月：為慶祝台北地下街12週年，發表[美少女](../Page/美少女.md "wikilink")[虛擬角色](../Page/虛擬角色.md "wikilink")「[莉洋](../Page/莉洋.md "wikilink")」（Rion），由[櫻野露設計畫稿](../Page/櫻野露.md "wikilink")、日本[聲優](../Page/聲優.md "wikilink")[花澤香菜配音](../Page/花澤香菜.md "wikilink")。
  - 2012年7月：台北地下街發表聲明：由於櫻野露的[同人社團](../Page/同人社團.md "wikilink")「WISH
    Studio」鼓勵且不反對[二次創作](../Page/二次創作.md "wikilink")，在WISH
    Studio知情並允許的情況下導致產生莉洋形象的[色情漫畫](../Page/色情漫畫.md "wikilink")，「因莉洋形象人物被二次創作、與[限制級漫畫出現](../Page/成人漫畫.md "wikilink")，故台北地下街暫停與莉洋相關活動。」WISH
    Studio則發表聲明：「今後莉洋將由WISH Studio正式公認受理！今後的莉洋也請多多指教！」
  - 2012年8月：台北地下街正式與台灣原創動漫品牌[九藏喵窩合作](../Page/九藏喵窩.md "wikilink")，並且選擇為台北地下街吉祥物。

## 構造及位置

\-{台北}-地下街位於[市民大道一段（鄭州路）下方](../Page/市民大道.md "wikilink")，西起[台北捷運](../Page/台北捷運.md "wikilink")[松山新店線](../Page/松山新店線.md "wikilink")[北門站](../Page/北門站_\(台北市\).md "wikilink")，並與該站直接連通，東至[淡水信義線](../Page/淡水信義線.md "wikilink")；全長825公尺，面積35,738平方公尺，東端和捷運[-{台北}-車站連通](../Page/台北車站.md "wikilink")，東南端亦可到達台鐵台北車站及高鐵台北車站。地下一層為商店街，商家總計187店面，地下二層為停車場。
[臺北市台北地下街場地利用合作社.jpg](https://zh.wikipedia.org/wiki/File:臺北市台北地下街場地利用合作社.jpg "fig:臺北市台北地下街場地利用合作社.jpg")
[伯朗咖啡館市民店.jpg](https://zh.wikipedia.org/wiki/File:伯朗咖啡館市民店.jpg "fig:伯朗咖啡館市民店.jpg")市民店\]\]
[雜誌瘋北街店.jpg](https://zh.wikipedia.org/wiki/File:雜誌瘋北街店.jpg "fig:雜誌瘋北街店.jpg")北街店\]\]
[PlayStation46小舖.jpg](https://zh.wikipedia.org/wiki/File:PlayStation46小舖.jpg "fig:PlayStation46小舖.jpg")46小舖\]\]
[Maiden_School夢幻學園.jpg](https://zh.wikipedia.org/wiki/File:Maiden_School夢幻學園.jpg "fig:Maiden_School夢幻學園.jpg")\]\]
-{台北}-地下街共有二十八個出入口，南北兩側各十四個，均以英文字母**[Y](../Page/Y.md "wikilink")**為編號開頭，南側的Y2出入口和捷運-{台北}-車站的M1出入口共用樓梯往地面。東側連通口可以直接進入[中山地下街而不必先回到地面](../Page/中山地下街.md "wikilink")。

  - 市民大道北側
      - 出入口Y1（北13）　[臺北轉運站](../Page/臺北轉運站.md "wikilink")　中山地下街　中山北路；[台北市公共自行車租賃系統](../Page/台北市公共自行車租賃系統.md "wikilink")[臺北轉運站](../Page/臺北轉運站.md "wikilink")
      - 出入口Y3（北12）　臺北轉運站　[京站時尚廣場](../Page/京站時尚廣場.md "wikilink")（地下連通）
      - 出入口Y5（北11）　承德路單號　臺北轉運站　京站時尚廣場（地下連通）
      - 出入口Y7（北10）　承德路雙號
      - 出入口Y9（北9）　太原路11巷[台北市公共自行車租賃系統市民太原路口](../Page/台北市公共自行車租賃系統.md "wikilink")
      - 出入口Y11（北8）　太原路11巷
      - 出入口Y12U（北9）[桃園國際機場捷運](../Page/桃園國際機場捷運.md "wikilink")[臺北車站連通道](../Page/臺北車站_\(桃園捷運\).md "wikilink")
      - 出入口Y13（北7）　太原路　華陰街行人徒步區
      - 出入口Y15（北6）　太原路　華陰街行人徒步區
      - 出入口Y17（北5）　[重慶北路單號](../Page/重慶北路_\(臺北市\).md "wikilink")　華陰街行人徒步區
      - 出入口Y19（北4）　重慶北路雙號
      - 出入口Y21（北3）　延平北路單號
      - 出入口Y23（北2）　
      - 出入口Y25（北1乙）　塔城街　[關務署](../Page/關務署.md "wikilink")
      - 出入口Y27（北1甲）　塔城街　關務署
  - 市民大道南側
      - 出入口Y2／M1（南13）　捷運台北車站（地下連通）　台鐵／高鐵（北一門、地下連通）
      - 出入口Y4（南12）　台鐵／高鐵（北一門、地下連通）
      - 出入口Y6（南11）　台鐵／高鐵（北二門）
      - 出入口Y8（南10）　台鐵／高鐵（北三門、地下連通）　承德路
      - 出入口Y10（南9）　台鐵／高鐵西區停車場
      - 出入口Y12（南8甲）　[館前路](../Page/館前路.md "wikilink")
      - 出入口Y14（南8乙）　（2017年2月16日開放）機場捷運臺北車站[臺北雙子星C](../Page/臺北雙子星.md "wikilink")1基地
      - 出入口Y16（南7）　機場捷運臺北車站雙子星C1基地
      - 出入口Y18（南6）　機場捷運臺北車站雙子星C1基地
      - 出入口Y20（南5）　[重慶南路](../Page/重慶南路_\(臺北市\).md "wikilink")　
      - 出入口Y22（南4）　機場捷運臺北車站雙子星D1基地　[鐵路警察局](../Page/內政部警政署鐵路警察局.md "wikilink")　重慶南路　
      - 出入口Y24（南3）　[北門](../Page/臺北府城北門.md "wikilink")　[延平南路](../Page/延平南路.md "wikilink")　[博愛路](../Page/博愛路_\(台北市\).md "wikilink")　[台北北門郵局](../Page/台北北門郵局.md "wikilink")
      - 出入口Y26（南2）　台鐵舊舍
      - 出入口Y28（南1）　塔城街　[捷運北門站](../Page/捷運北門站.md "wikilink")（地下連通）；[台北市公共自行車租賃系統捷運北門站](../Page/台北市公共自行車租賃系統.md "wikilink")（3號出口）

## 利用狀況

目前營運管理單位為「保證責任台北市-{台北}-地下街場地利用合作社」。

## 相關條目

  - [台北車站](../Page/台北車站.md "wikilink")
  - [高雄地下街](../Page/高雄地下街.md "wikilink")

## 參考資料

## 外部連結

  - [-{台北}-地下街](http://www.taipeimall.com.tw/)
  - [台北地下街
    臺北旅遊網](https://www.travel.taipei/zh-tw/attraction/details/1591)

[T台](../Category/臺灣地下街.md "wikilink")
[T台](../Category/2000年完工建築物.md "wikilink")
[Category:2000年台灣建立](../Category/2000年台灣建立.md "wikilink")
[Category:台北市商場](../Category/台北市商場.md "wikilink")
[台北地下街](../Category/台北車站特定專用區.md "wikilink")
[Category:總部位於臺北市中山區的工商業機構](../Category/總部位於臺北市中山區的工商業機構.md "wikilink")
[Category:大同區 (臺灣)](../Category/大同區_\(臺灣\).md "wikilink")

1.