**臺北市建國假日花市**，簡稱**建國花市**，於1982年開始營業，設址於[臺北市](../Page/臺北市.md "wikilink")[建國南路高架橋下方](../Page/建國南路.md "wikilink")，[信義路至](../Page/信義路_\(台北市\).md "wikilink")[仁愛路之間](../Page/仁愛路_\(台北市\).md "wikilink")；[濟南路至仁愛路間另設有建國假日玉市](../Page/濟南路.md "wikilink")。高架橋下方平時做為[停車場](../Page/停車場.md "wikilink")，於[週五](../Page/週五.md "wikilink")[晚上淨空讓各地而來的園藝人士匯集設攤](../Page/晚上.md "wikilink")，[週六](../Page/週六.md "wikilink")、[週日兩日的九時至十八時營業](../Page/週日.md "wikilink")，販售植物與花藝相關用品。

## 販售項目

販售項目包括了：

  - 開花植物：例如[蝴蝶蘭](../Page/蝴蝶蘭.md "wikilink")、[吉野櫻](../Page/吉野櫻.md "wikilink")、[九重葛](../Page/九重葛.md "wikilink")、[繡球花等](../Page/繡球花.md "wikilink")。
  - 賞葉植物：例如[日本楓](../Page/日本楓.md "wikilink")、[姑婆芋等](../Page/姑婆芋.md "wikilink")。
  - 果樹：例如[桑樹](../Page/桑.md "wikilink")、[桃樹](../Page/桃.md "wikilink")、[李樹](../Page/李.md "wikilink")、[葡萄樹](../Page/葡萄.md "wikilink")、[西印度櫻桃](../Page/西印度櫻桃.md "wikilink")、[神秘果等](../Page/神秘果.md "wikilink")。
  - 民間藥用植物：例如[明日葉](../Page/明日葉.md "wikilink")、[金銀花](../Page/金銀花.md "wikilink")、[仙草](../Page/仙草.md "wikilink")、[箘桂等](../Page/箘桂.md "wikilink")。
  - 歐美與南洋香草植物：例如多種[迷迭香](../Page/迷迭香.md "wikilink")、多種[薰衣草](../Page/薰衣草.md "wikilink")、多種[薄荷](../Page/薄荷.md "wikilink")、多種[紫蘇](../Page/紫蘇.md "wikilink")、[蝦夷蔥](../Page/蝦夷蔥.md "wikilink")、[檸檬香茅等](../Page/檸檬香茅.md "wikilink")。
  - 種子：包括[農友種苗公司與其他公司出品的種子包裝](../Page/農友種苗公司.md "wikilink")，以及零賣[小麥種子](../Page/小麥.md "wikilink")(以自行耕植小麥草)或其他穀豆類種子。
  - 花藝用品：例如花盆、鏟子、吊線等。
  - 泥土與肥料：[陽明山土](../Page/陽明山.md "wikilink")、赤玉土、泥炭土、有機腐植土；化學肥與有機肥。

∗
水族活體與用品：如[金魚](../Page/金魚.md "wikilink")、[孔雀魚](../Page/孔雀魚.md "wikilink")、[角蛙](../Page/角蛙.md "wikilink")、[水龜等](../Page/紅耳龜.md "wikilink")。

## 附近地標

  - [大安森林公園](../Page/大安森林公園.md "wikilink")
  - [國立台灣師範大學附屬高級中學](../Page/國立台灣師範大學附屬高級中學.md "wikilink")（師大附中）
  - [信義聯勤俱樂部](../Page/信義聯勤俱樂部.md "wikilink")
  - [臺北市私立延平高級中學](../Page/臺北市私立延平高級中學.md "wikilink")
  - [美國在台協會台北辦事處](../Page/美國在台協會.md "wikilink")
  - [中華郵政公司台北郵局限時大樓](../Page/中華郵政公司.md "wikilink")、電腦列印封裝中心
  - [宏盛帝寶](../Page/宏盛帝寶.md "wikilink")

## 交通

### 自行開車

  - 由建國交流道→建國高架道路→信義建國路口→建國花市

### 公車

  - 信義建國路口：0東、20、204、22、38、信義幹線、88、88（區間）、1503（往西）、226（往東）
  - 信義市場：204、298、298（區間）、紅57
  - 延平中學：204、298、298（區間）、953、紅57
  - 捷運大安森林公園站：0東、20、22、38、88、88（區間）、204、226（往東）、信義幹線、1503（往西）

### 臺北捷運

  - [忠孝新生站](../Page/忠孝新生站.md "wikilink")：由新生南路步行至仁愛路後左轉，至仁愛建國路口後到達建國花市

  - [大安森林公園站](../Page/大安森林公園站.md "wikilink")：6號出口

## 參考資料

## 外部連結

  - [建國假日花市](http://www.fafa.org.tw/)
  - [建國假日玉市](http://www.tckjm.com.tw/)

[Category:台北市經濟](../Category/台北市經濟.md "wikilink") [Category:大安區
(臺北市)](../Category/大安區_\(臺北市\).md "wikilink")
[Category:花卉市場](../Category/花卉市場.md "wikilink")
[Category:台灣傳統市場](../Category/台灣傳統市場.md "wikilink")
[Category:1982年台灣建立](../Category/1982年台灣建立.md "wikilink")