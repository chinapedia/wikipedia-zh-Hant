**財團法人台北法華寺**，簡稱**台北法華寺**或**法華寺**，是一座位在臺灣臺北市[萬華區西寧南路](../Page/萬華區.md "wikilink")（創建時為[若竹町](../Page/若竹町.md "wikilink")）的[觀音寺](../Page/觀音寺.md "wikilink")。該寺原屬[日蓮宗](../Page/日蓮宗.md "wikilink")，[山號](../Page/山號.md "wikilink")「南海山」，建物興築於[1919年](../Page/1919年.md "wikilink")（[大正八年](../Page/大正.md "wikilink")）；其[本尊為](../Page/本尊.md "wikilink")[十界曼荼羅](../Page/十界曼荼羅.md "wikilink")，合祀宗祖[日蓮](../Page/日蓮.md "wikilink")、[妙見菩薩](../Page/妙見菩薩.md "wikilink")、[鬼子母神](../Page/鬼子母神.md "wikilink")、經王大菩薩、[清正公等](../Page/加藤清正.md "wikilink")。

## 歷史

[1897年](../Page/1897年.md "wikilink")（[明治三十年](../Page/明治.md "wikilink")），[日蓮宗在臺北建立其布教所](../Page/日蓮宗.md "wikilink")，並將之作為其在臺灣之布教中心。[1910年](../Page/1910年臺灣.md "wikilink")，該布教所被遷移至現今法華寺所在地點（當時屬於[若竹町](../Page/若竹町.md "wikilink")）。[1920年](../Page/1920年.md "wikilink")（[大正九年](../Page/大正.md "wikilink")），該機構之官方名稱公布為“法華寺”，該名稱於此後持續沿用至今。

## 參見

  - [台灣佛教](../Page/台灣佛教.md "wikilink")

## 參考文獻

  - 《臺北市寺廟概覽》，1994年，臺北市，[臺北市政府民政局](../Page/臺北市政府民政局.md "wikilink")

[W](../Category/台北市佛寺.md "wikilink")
[台灣石碑雕刻](../Category/台灣石碑雕刻.md "wikilink")
[臺北市文化資產](../Category/臺北市文化資產.md "wikilink")