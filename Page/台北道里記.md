**台北道里記**為噶瑪蘭（[宜蘭縣](../Page/宜蘭縣.md "wikilink")）[通判](../Page/通判.md "wikilink")[姚瑩著於](../Page/姚瑩.md "wikilink")1832年。該書詳細記載於1821年從[嘉義](../Page/嘉義市.md "wikilink")、[彰化](../Page/彰化市.md "wikilink")、[淡水](../Page/淡水區.md "wikilink")、雞籠（今[基隆](../Page/基隆市.md "wikilink")）、[三貂嶺一直到](../Page/三貂嶺.md "wikilink")[蘇澳之旅途見聞](../Page/蘇澳鎮_\(臺灣\).md "wikilink")。而該書共有一千四百多字。

## 簡介

台北道里記作者姚瑩（1785年－1853年）字石甫，一字明叔，號展和，晚號幸翁，[中國](../Page/中國.md "wikilink")[安徽](../Page/安徽.md "wikilink")[桐城人](../Page/桐城.md "wikilink")，1821年他本於[台南擔任](../Page/台南.md "wikilink")[同知](../Page/同知.md "wikilink")，後來因故貶官數百里遠的後山[噶瑪蘭擔任](../Page/噶瑪蘭.md "wikilink")[通判](../Page/通判.md "wikilink")。該書則為他從台南一路前往噶瑪蘭就任的遊記。

該書以平敘順序筆法描繪景色，多有[數據參考](../Page/數據.md "wikilink")，頗有[地理](../Page/地理.md "wikilink")[百科雛形](../Page/百科.md "wikilink")。例如描寫[雲林](../Page/雲林縣.md "wikilink")[彰化地區](../Page/彰化縣.md "wikilink")：「十里[西螺](../Page/西螺鎮.md "wikilink")，大市，有汛，駐[把總一員](../Page/把總.md "wikilink")，有溪；出柑，香美異他柑。五里，三條圳，圳即俗畝字，土人讀如浚，凡三道，水盛時，非舟不渡。十里，東螺溪。三里，寶斗，大村市，即舊東螺也，民居稠密，街市整齊，有汛。五里，茉莉莊。五里，關帝亭，廟宇甚新峻；前有僧能詩，而還俗矣。五里，大埔心，民居小村舍，多盜匪；其東北沿山，即下林仔，東南沿海，為二林，皆匪巢也。五里…
又描繪[台北地區者為](../Page/台北.md "wikilink")：五里渡大溪至[艋舺](../Page/艋舺.md "wikilink")，途中山水曲秀，風景如畫，擺接十三莊在其東南，
為北路第一勝境。艋舺民居舖戶，約四五千家。外即[八里坌口](../Page/八里區.md "wikilink")，商船聚集，闤闠最盛，[淡水](../Page/淡水區.md "wikilink")
倉在焉。[同知歲中半居此](../Page/同知.md "wikilink")，蓋民富而事繁也。駐[水師](../Page/水師.md "wikilink")[遊擊一員](../Page/游擊_\(官名\).md "wikilink")，[守備一員](../Page/守備.md "wikilink")。」

該書不但常作為台灣各地名的由來或考據，也成為平埔族存在地區的文獻，可說為台灣史裡面，相當重要的著作。

## 參見

  - [裨海紀遊](../Page/裨海紀遊.md "wikilink")（1697年）
  - [臺海使槎錄](../Page/臺海使槎錄.md "wikilink")（1722年）

## 外部連結

  - [古時的旅遊文學﹣臺北道里記
    姚瑩](http://tw.myblog.yahoo.com/freeman-588/article?mid=2089&prev=2206&next=2088&l=f&fid=44)
  - [(台北瑞芳)．淡蘭古道白蘭氏紀念碑
    (Tony的自然人文旅記第0733篇)](http://www.tonyhuang39.com/tony0733/tony0733.html)

[Category:台灣清治時期典籍](../Category/台灣清治時期典籍.md "wikilink")
[Category:台灣歷史書籍](../Category/台灣歷史書籍.md "wikilink")
[Category:平埔族群](../Category/平埔族群.md "wikilink")
[Category:1832年書籍](../Category/1832年書籍.md "wikilink")