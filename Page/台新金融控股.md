**台新金融控股股份有限公司**（英語：Taishin Financial Holding Co.,
Ltd.，，簡稱**台新金控**、**台新金**）是[台灣一家](../Page/台灣.md "wikilink")[金融控股公司](../Page/金融控股公司.md "wikilink")，旗下共有162個營業據點及2個海外[代表處或](../Page/代表處.md "wikilink")[辦事處](../Page/辦事處.md "wikilink")。

## 發展歷史

2002年2月18日，[台新銀行](../Page/台新銀行.md "wikilink")、[大安銀行以股份轉換方式共同成立](../Page/大安銀行.md "wikilink")**台新金融控股公司**。\[1\]台新金控成立後，即於5月28日將兩家銀行整併，以台新銀行為存續銀行\[2\]；同年12月，再將[台新票券及](../Page/台新票券.md "wikilink")[台証證券納為子公司](../Page/台証證券.md "wikilink")。2004年，台新銀行併購[新竹十信](../Page/新竹十信.md "wikilink")。

2005年台新金控贏得[彰化銀行](../Page/彰化銀行.md "wikilink")[特別股競標](../Page/特別股.md "wikilink")，成為彰化銀行最大法人股東，在持續收購股票合計取得超過25%的股權後，將彰化銀行納入成為金控成員，成為國內合併資產規模排名第二大的金控公司，成為[台新彰銀案的開端](../Page/台新彰銀案.md "wikilink")。\[3\]\[4\]

[Taishin_Tower_entrance_20130324.jpg](https://zh.wikipedia.org/wiki/File:Taishin_Tower_entrance_20130324.jpg "fig:Taishin_Tower_entrance_20130324.jpg")
2006年，美商[新橋投資集團](../Page/新橋投資集團.md "wikilink")（Newbridge
Capital）、日商[野村證券集團以及美國](../Page/野村證券.md "wikilink")[索羅斯基金管理公司](../Page/索羅斯基金管理公司.md "wikilink")（Soros）旗下的QE
International於台新金控私募案中先後入股投資。2006年3月23日，台新金控啟用位於台北市[仁愛敦南圓環旁的總部](../Page/仁愛敦南圓環.md "wikilink")「台新金控大樓」，係1982年歇業的[財神酒店拆除改建而成](../Page/財神酒店.md "wikilink")。

2009年，由於台新金控出現財務壓力需要資金，決定將台証證券的經紀業務、通路資產以新台幣294億元賣給[凱基證券](../Page/凱基證券.md "wikilink")，但保留自營、股務及承銷業務\[5\]\[6\]\[7\]；同年12月19日，台証證券正式併入凱基證券，同時使凱基證券成為台灣經紀[市占率第二大的證券公司](../Page/市占率.md "wikilink")。\[8\]但為彌補出售後缺少的證券業務，再以新台幣11.46億元買下東興證券，於2010年完成併購後更名為[台新證券](../Page/台新證券.md "wikilink")。\[9\]\[10\]

2010年起，台新金控陸續併入[台灣工業銀行之子公司](../Page/台灣工業銀行.md "wikilink")[台灣工銀投信](../Page/台灣工銀投信.md "wikilink")（併入台新投信）\[11\]、富蘭克林保險經紀人（併入後更名為「台新金保險經紀人」）。

2012年，台新金控曾與美商[紐約人壽完成簽約](../Page/紐約人壽.md "wikilink")，計畫將紐約人壽在台灣的子公司[國際紐約人壽納入金控](../Page/國際紐約人壽.md "wikilink")\[12\]；但因台新金[財務報表](../Page/財務報表.md "wikilink")「雙重槓桿比率」過高，此案遭[金管會駁回](../Page/金管會.md "wikilink")，最終未能完成。

2014年底，台新金控子公司[彰化銀行進行董事改選](../Page/彰化銀行.md "wikilink")，但台新金控僅獲得2席普通董事席位，依《金融控股公司法》彰化銀行不能再列為台新金控子公司。

2015年，由於無法取得對彰化銀行的控制權，台新金控認列對彰銀一次性的投資虧損新台幣148億元\[13\]。

2018年，台新金控面臨上市以來最嚴重的市場派逼宮，公司經營層用縮減董事席次的方式拉高競選門檻\[14\]，最終以[寶佳機構為首的市場派在董監改選中全軍覆沒](../Page/寶佳機構.md "wikilink")\[15\]。

## 金控成員及關係

{{ familytree/start }} {{ familytree | level0 |v| level1 |v| level2 |
level0 = **台新金融控股** | level1 =
[台新國際商業銀行](../Page/台新國際商業銀行.md "wikilink")
| level2 = 台新大安租賃 }} {{ familytree | | | |\!| | | |\!| }} {{ familytree
| | | |)| level1 |)| level2 | level1 =
[台新綜合證券](../Page/台新綜合證券.md "wikilink") | level2
= 台新建築經理 }} {{ familytree | | | |\!| | | |\!| }} {{ familytree | | | |)|
level1 |\`| level2 |-| level3 | level1 =
[台新證券投資顧問](../Page/台新證券投資顧問.md "wikilink")
| level2 = 台新保險代理人 | level3 = 台新保險經紀人 }} {{ familytree | | | |\!| }} {{
familytree | | | |)| level1 | | level1 =
[台新證券投資信託](../Page/台新證券投資信託.md "wikilink")
}} {{ familytree | | | |\!| }} {{ familytree | | | |)| level1 |v| level2
| level1 = [台新金國際創業投資](../Page/台新金國際創業投資.md "wikilink") | level2 =
台新融資租貸(中國) }} {{ familytree | | | |\!| | | |\!| }} {{
familytree | | | |)| level1 |\`| level2 | level1 =
[台新資產管理](../Page/台新資產管理.md "wikilink") | level2 =
台新融資租貸(天津) }} {{ familytree | | | |\!| }} {{ familytree | | | |\`|
level1 | level1 = [台新金保險經紀人](../Page/台新金保險經紀人.md "wikilink") }} {{
familytree/end }}

## 參考資料

## 外部链接

  - [台新金控](http://www.taishinholdings.com.tw/)

[T](../Category/臺灣的金融控股公司.md "wikilink")
[2](../Category/臺灣證券交易所上市公司.md "wikilink")
[台新金控](../Category/台新金控.md "wikilink")
[Category:2002年台灣建立](../Category/2002年台灣建立.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13. 郭芝芸，[台新金認列彰銀投資虧損148億
    稅後淨利16億](http://www.chinatimes.com/realtimenews/20150313004106-260410)，中時電子報，2015-03-13
14. 孫中英，[寶佳來插旗
    台新金縮減董事席次應戰](https://udn.com/news/story/11316/3042371)，聯合新聞網，2018-03-20
15. 中央社，[跌破眼鏡！台新金董事改選
    寶佳全軍覆沒](http://newtalk.tw/news/view/2018-06-08/127176)，新頭殼新聞網，2018-06-08