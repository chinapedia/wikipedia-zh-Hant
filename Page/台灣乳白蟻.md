**台灣乳白蟻**（*Coptotermes
formosanus*）又稱**家白蟻**或**台灣家白蟻**，是一種原產於[台灣](../Page/台灣.md "wikilink")（也是此生物的名稱來源）以及[日本](../Page/日本.md "wikilink")、[中國東部的](../Page/中國東部.md "wikilink")[白蟻](../Page/白蟻.md "wikilink")。台灣乳白蟻的破壞能力相當強，又稱為「超級白蟻」，這種白蟻會建立大群體，並快速消耗[木頭](../Page/木頭.md "wikilink")。一個台灣乳白蟻群體中可能含有數百萬隻個體，而其他白蟻則只有數十萬隻。

台灣乳白蟻大約在1600年代擴散到日本，並於1800年代後期到達夏威夷。在1950年代，南非與[斯里蘭卡皆有報告此物種的存在](../Page/斯里蘭卡.md "wikilink")。1960年代以後，則出現在[美國](../Page/美國.md "wikilink")[德州](../Page/德州.md "wikilink")、[路易斯安那](../Page/路易斯安那.md "wikilink")、[南卡羅來納](../Page/南卡羅來納.md "wikilink")。1980年，[佛羅里達也發現了台灣乳白蟻](../Page/佛羅里達.md "wikilink")，因而列入[世界百大外来入侵种](../Page/世界百大外来入侵种.md "wikilink")。

台灣乳白蟻的社會體系當中，有蟻王、蟻后、工蟻及兵蟻四種不同的角色。

台灣乳白蟻的紛飛期一般來說大約是四月到七月，通常是在相對濕度比較高的黃昏至夜間開始紛飛，生殖蟻在台灣又被俗稱「大水蟻」，身上還具有翅膀的時候牠們跟一般飛行性昆蟲一樣具有驅光性，交配、翅膀脫落之後會轉變成為負驅光性，就是找洞鑽，而鑽進去的地方只要有適當的溫度、充分的食物跟水源就會開始築巢。

一個巢穴形成之後，首先會先繁殖大量的工蟻，其次才是兵蟻。
這些工蟻及兵蟻沒有性別也不具繁殖能力，所以如果住家裡面有白蟻不斷地在產生的時候，其實從木頭被帶過來的機率微乎其微。而蟻后產生出生殖蟻的時間大約都在巢穴落成之後五年以上。

<center>

<File:Female> Coptotermes
formosanus.jpg|繁殖型蟻（[飛蟻](../Page/飛蟻.md "wikilink")）
[File:K8085-21.jpg|蟻后](File:K8085-21.jpg%7C蟻后) <File:Termite-Formosan>
subterranean-soldiers-Tamil word17.2.jpg|兵蟻 <File:Termite-Formosan>
subterranean-worker-Tamil word17.1.jpg|工蟻

</center>

## 外部連結

  - [Taxonomy of
    termites](https://web.archive.org/web/20071116100113/http://www.utoronto.ca/forest/termite/taxon.htm)
  - [Distribution in the United
    States](https://web.archive.org/web/20070929050302/http://www.termitesurvey.com/distribution/coptotermes_formosanus.shtml)
  - [Species
    profile](https://web.archive.org/web/20071130124404/http://creatures.ifas.ufl.edu/urban/termites/formosan_termite.htm)
    from the [University of
    Florida](../Page/University_of_Florida.md "wikilink") Department of
    Entomology and Nematology
  - [Earlham
    College](https://web.archive.org/web/20050530074205/http://www.earlham.edu/~biol/hawaii/termite.htm)
    Senior Seminar 2002
  - [Global Invasive Species
    Database](https://web.archive.org/web/20060210031628/http://issg.appfa.auckland.ac.nz/database/species/ecology.asp?si=61&fr=1&sts=)

[formosanus](../Category/家白蟻屬.md "wikilink")
[Category:入侵昆蟲種](../Category/入侵昆蟲種.md "wikilink")
[Category:台灣昆蟲](../Category/台灣昆蟲.md "wikilink")
[Category:1909年描述的昆蟲](../Category/1909年描述的昆蟲.md "wikilink")