[台灣約有](../Page/台灣.md "wikilink") 220 種淡水魚（含河口魚類），其中約有 30
多種為[台灣特有種](../Page/台灣動物特有種列表.md "wikilink")。其中經行政院農委會公告的瀕臨絕種保育類有[高身鏟頜魚和](../Page/高身鏟頜魚.md "wikilink")[台灣鮭魚](../Page/台灣鮭魚.md "wikilink")
2種，珍貴稀有保育類有[台東間爬岩鰍](../Page/台東間爬岩鰍.md "wikilink")、[埔里中華爬岩鰍](../Page/埔里中華爬岩鰍.md "wikilink")、[台灣鬥魚和](../Page/台灣鬥魚.md "wikilink")[鱸鰻等](../Page/鱸鰻.md "wikilink")
4種。

以下是台灣淡水（包含[溪流](../Page/溪流.md "wikilink")、[水庫](../Page/水庫.md "wikilink")、[湖泊](../Page/湖泊.md "wikilink")
和[水壩等](../Page/水壩.md "wikilink")）及[河口的魚類列表](../Page/河口.md "wikilink")：

## [骨舌魚目](../Page/骨舌魚目.md "wikilink") Osteoglossiformes

  - [骨舌魚科](../Page/骨舌魚科.md "wikilink") Osteoglossidae

<!-- end list -->

1.  [巨骨舌魚](../Page/巨骨舌魚.md "wikilink") (象魚) - *Arapaima gigas* ※外來種

<!-- end list -->

  - [駝背魚科](../Page/駝背魚科.md "wikilink") Notopteridae

<!-- end list -->

1.  [飾妝鎧甲弓背魚](../Page/七星飛刀.md "wikilink") (七星飛刀) - *Chitala ornata* ※外來種

## [海鰱目](../Page/海鰱目.md "wikilink") Elopiformes

  - [海鰱科](../Page/海鰱科.md "wikilink") Elopidae

<!-- end list -->

1.  [海鰱](../Page/海鰱.md "wikilink") - *Elops machnata* <small>(Forsskal,
    1775)</small>

<!-- end list -->

  - [大眼海鰱科](../Page/大眼海鰱科.md "wikilink") Megalopidae

<!-- end list -->

1.  [大眼海鰱](../Page/大眼海鰱.md "wikilink") - *Megalops cyprinoides*
    <small>(Broussonet, 1782)</small>

## [北梭魚目](../Page/北梭魚目.md "wikilink") Albuliformes

  - [狐鰮科](../Page/狐鰮科.md "wikilink") Albulidae

<!-- end list -->

1.  [狐鰮](../Page/狐鰮.md "wikilink") (北梭魚) - *Albula glossodonta*
    <small>(Forsskal, 1775)</small>

## [鰻鱺目](../Page/鰻鱺目.md "wikilink") Anguilliformes

  - [鰻鱺科](../Page/鰻鱺科.md "wikilink") Anguillidae

<!-- end list -->

1.  [二色鰻](../Page/二色鰻.md "wikilink") (短鰭鰻) - *Anguilla bicolor*
    <small>Schmidt, 1928</small>
2.  [日本鰻](../Page/日本鰻.md "wikilink") (白鰻) - *Anguilla japonica*
    <small>Temminck & Schlegel, 1846</small>
3.  [鱸鰻](../Page/鱸鰻.md "wikilink") - *Anguilla marmorata* <small>Quoy &
    Gaimard, 1824</small>
4.  [美洲鰻鱺](../Page/美洲鰻.md "wikilink") (美洲鰻) - *Anguilla rostrata* ※外來種

<!-- end list -->

  - [海鰻科](../Page/海鰻科.md "wikilink") Muraenesocidae

<!-- end list -->

1.  [百吉海鰻](../Page/百吉海鰻.md "wikilink") - *Muraenesox bagio*
    <small>(Hamilton, 1822)</small>

<!-- end list -->

  - [蛇鰻科](../Page/蛇鰻科.md "wikilink") Ophichthidae

<!-- end list -->

1.  [頂蛇鰻](../Page/頂蛇鰻.md "wikilink") - *Ophichthus apicalis*
    <small>Anonymous, 1830</small>
2.  [食蟹豆齒鰻](../Page/食蟹豆齒鰻.md "wikilink") - *Pisodonophis cancrivorus*
    <small>(Richardson, 1848)</small>

## [鯡形目](../Page/鯡形目.md "wikilink") Clupeiformes

  - [鯡科](../Page/鯡科.md "wikilink") Clupeidae

<!-- end list -->

1.  [環球海鰶](../Page/環球海鰶.md "wikilink") - *Nematalosa come*
    <small>(Richardson, 1846)</small>
2.  [信德小沙丁魚](../Page/信德小沙丁魚.md "wikilink") - *Sardinella sindensis*
    <small>(Day, 1878)</small>

<!-- end list -->

  - [鯷科](../Page/鯷科.md "wikilink") Engraulidae

<!-- end list -->

1.  [漢氏綾鯷](../Page/漢氏綾鯷.md "wikilink") - *Thryssa hamiltonii*
    <small>(Gray, 1835)</small>
2.  [長頜綾鯷](../Page/長頜綾鯷.md "wikilink") - *Thryssa setirostris*
    <small>(Broussonet, 1782)</small>

## [鼠鱚目](../Page/鼠鱚目.md "wikilink") Gonorhynchiformes

  - [虱目魚科](../Page/虱目魚科.md "wikilink") Chanidae

<!-- end list -->

1.  [虱目魚](../Page/虱目魚.md "wikilink") - *Chanos chanos* <small>(Forsskal,
    1775)</small>

## [鯉形目](../Page/鯉形目.md "wikilink") Cypriniformes

  - [平鰭鰍科](../Page/平鰭鰍科.md "wikilink") Balitoridae

<!-- end list -->

1.  [台灣纓口鰍](../Page/台灣纓口鰍.md "wikilink") - *Crossostoma lacustre*
    <small>Steindachner, 1908</small> ※特有種
2.  [台灣間爬岩鰍](../Page/台灣間吸鰍.md "wikilink") - *Hemimyzon formosanum*
    <small>(Boulenger, 1894)</small> ※特有種
3.  [台東間爬岩鰍](../Page/台東間爬岩鰍.md "wikilink") - *Hemimyzon taitungensis*
    <small>Tzeng & Shen, 1981</small> ※特有種
4.  [南台中華爬岩鰍](../Page/南台中華爬岩鰍.md "wikilink") - *Sinogastromyzon
    nantaiensis* <small>Chen, Han & Fang, 2003</small> ※特有種
5.  [埔里中華爬岩鰍](../Page/埔里中華爬岩鰍.md "wikilink") - *Sinogastronmyzon
    puliensis* <small>Liang, 1974</small> ※特有種

<!-- end list -->

  - [鰍科](../Page/鰍科.md "wikilink") Cobitidae

<!-- end list -->

1.  [中華花鰍](../Page/中華花鰍.md "wikilink") (花鰍) - *Cobitis sinensis*
    <small>Sauvage & Dabry de Thiersant, 1874</small>
2.  [泥鰍](../Page/泥鰍.md "wikilink") - *Misgurnus anguillicaudatus*
    <small>(Cantor, 1842)</small>
3.  [大鱗副泥鰍](../Page/大鱗副泥鰍.md "wikilink") - *Paramisgurnus debryanus*
    <small>Guichenot, 1872</small>

<!-- end list -->

  - [鯉科](../Page/鯉科.md "wikilink") Cyprinidae

<!-- end list -->

1.  [石賓光唇魚](../Page/石賓光唇魚.md "wikilink") - *Acrossocheilus formosanus*
    <small>(Gunther, 1868)</small> ※特有種
2.  [菊池氏細鯽](../Page/菊池氏細鯽.md "wikilink") - *Aphyocypris kikuchii*
    <small>(Oshima, 1919)</small> ※特有種
3.  [鱅](../Page/鱅.md "wikilink") (黑鰱) - *Aristichthys nobilis*
    <small>(Richardson, 1845)</small> ※外來種
4.  [施氏{{僻字](../Page/施氏䰾.md "wikilink")
    ([泰國鯽](../Page/泰國鯽.md "wikilink")) - *Barbodes
    schwanenfeldii* ※外來種
5.  [台灣馬口魚](../Page/台灣馬口魚.md "wikilink") - *Candidia barbata*
    <small>(Regan, 1908)</small> ※特有種
6.  [鯽](../Page/鯽.md "wikilink") - *Carassius auratus auratus*
    <small>(Linnaeus, 1758)</small>
7.  [高身鯽](../Page/高身鯽.md "wikilink") (日本鯽) - *Carassius cuvieri*
    <small>Temminck & Schlegel, 1846</small> ※外來種
8.  [鯁魚](../Page/鯁魚.md "wikilink") ([鯪魚](../Page/鯪魚.md "wikilink")) -
    *Cirrhina molitorella* <small>(Valenciennes, 1844)</small> ※外來種
9.  [草魚](../Page/草魚.md "wikilink") (鯇) - *Ctenopharyngodon idellus*
    <small>(Valenciennes, 1844)</small> ※外來種
10. [翹嘴鮊](../Page/翹嘴鮊.md "wikilink") - *Culter alburnus*
    <small>Basilewsky, 1855</small>
11. [紅鰭鮊](../Page/紅鰭鮊.md "wikilink") - *Culter erythropterus*
    <small>Basilewsky, 1855</small> ※外來種
12. [鯉](../Page/鯉.md "wikilink") - *Cyprinus carpio* <small>Linnaeus,
    1758</small> ※外來種
13. [圓吻鯝](../Page/圓吻鯝.md "wikilink") - *Distoechodon tumirostris*
    <small>Peters, 1881</small>
14. [陳氏鰍鮀](../Page/陳氏鰍鮀.md "wikilink") (台灣鰍鮀) - *Gobiobotia cheni*
    <small>Banarescu & Nalbant, 1966</small> ※特有種
15. [中間鰍鮀](../Page/中間鰍鮀.md "wikilink") - *Gobiobotia intermedia*
    <small>Banarescu & Nalbant, 1968</small>
16. [唇{{僻字](../Page/唇䱻.md "wikilink") - *Hemibarbus labeo*
    <small>(Pallas, 1776)</small>
17. [白鱎](../Page/白鱎.md "wikilink") - *Hemiculter leucisculus*
    <small>(Basilewsky, 1855)</small>
18. [白鰱](../Page/白鰱.md "wikilink") - *Hypopothalmichthys molitrix*
    <small>(Valenciennes, 1844)</small> ※外來種
19. [高體高鬚魚](../Page/高體高鬚魚.md "wikilink") (高體四鬚魮) - *Hypsibarbus pierrei*
    <small>(Sauvage, 1880)</small> ※外來種
20. [何氏細鬚{{僻字](../Page/何氏細鬚䰾.md "wikilink") (蘇丹魚) - *Leptobarbus
    hoevenii* ※外來種
21. [團頭魴](../Page/團頭魴.md "wikilink") ([武昌魚](../Page/武昌魚.md "wikilink"))
    - *Megalobrama amblycephala* <small>Yih, 1955</small> ※外來種
22. [高身小鰾鮈](../Page/高身小鰾鮈.md "wikilink") - *Microphysogobio alticorpus*
    <small>Banarescu & Nalbant, 1968</small> ※特有種
23. [短吻小鰾鮈](../Page/短吻小鰾鮈.md "wikilink") (短吻鐮柄魚) - *Microphysogobio
    brevirostris* <small>(Gunther, 1868)</small> ※特有種
24. [青魚](../Page/青魚.md "wikilink") (黑鰡、烏鰡) - *Mylopharyngodon piceus*
    <small>(Richardson, 1846)</small> ※外來種
25. [革條副鱊](../Page/革條副鱊.md "wikilink") (台灣石鮒) - *Paracheilognathus
    himantegus* <small>(Gunther, 1868)</small>
26. [台灣副細鯽](../Page/台灣副細鯽.md "wikilink") (台灣白魚) - *Pararasbora
    moltrechtii* <small>Regan, 1908</small> ※特有種
27. [羅漢魚](../Page/羅漢魚.md "wikilink") - *Pseudorasbora parva*
    <small>(Temminck & Schlegel, 1846)</small>
28. [條紋小䰾](../Page/條紋小䰾.md "wikilink") (紅目鮘) - *Puntius semifasciolatus*
    <small>(Gunther, 1868)</small>
29. [台灣細鯿](../Page/台灣細鯿.md "wikilink") (台灣黃鮰魚) - *Rasborinus formosae*
    <small>(Oshima, 1920)</small> ※特有種
30. [大鱗細鯿](../Page/大鱗細鯿.md "wikilink") (大鱗鱎) - *Hemiculter macrolepis*
    <small>(Regan, 1908)</small> ※特有種 / 滅絕
31. [高體鰟{{僻字](../Page/高體鰟鮍.md "wikilink") - *Rhodeus ocellatus*
    <small>(Kner, 1867)</small>
32. [大眼華鯿](../Page/大眼華鯿.md "wikilink") - *Sinibrama macrops*
    <small>(Gunther, 1868)</small>
33. [何氏棘魞](../Page/何氏棘魞.md "wikilink") - *Spinibarbus hollandi*
    <small>Oshima, 1919</small> ※特有種
34. [飯島氏頜鬚鮈](../Page/飯島氏頜鬚鮈.md "wikilink") - *Squalidus iijimae*
    <small>Oshima, 1919</small> ※特有種
35. [高身鏟頜魚](../Page/高身鏟頜魚.md "wikilink") (高身鯝魚) - *Varicorhinus
    alticorpus* <small>(Oshima, 1920)</small> ※特有種
36. [台灣鏟頜魚](../Page/台灣鏟頜魚.md "wikilink") (鯝魚) - *Varicorhinus
    barbatulus* <small>(Pellegrin, 1908)</small>
37. [粗首鱲](../Page/粗首鱲.md "wikilink") - *Zacco pachyephalus*
    <small>(Gunther, 1868)</small> ※特有種
38. [平頜鱲](../Page/平頜鱲.md "wikilink") - *Zacco platypus* <small>(Temminck
    & Schlegel, 1846)</small>

## [脂鯉目](../Page/脂鯉目.md "wikilink") Characiformes

  - [脂鯉科](../Page/脂鯉科.md "wikilink") Characidae

<!-- end list -->

1.  大蓋巨脂鯉 (銀板) - *Colossoma macropomum* ※外來種
2.  特氏臀點脂鯉 ([食人魚](../Page/食人魚.md "wikilink")) - *Pygocentrus nattereri*
    ※外來種

## [鯰形目](../Page/鯰形目.md "wikilink") Siluriformes

  - [鮰科](../Page/鮰科.md "wikilink") Amblycipitidae

<!-- end list -->

1.  [台灣鮰](../Page/台灣鮰.md "wikilink") - *Liobagrus formosanus*
    <small>Regan, 1908</small> ※特有種
2.  [南投鮰](../Page/南投鮰.md "wikilink")

<!-- end list -->

  - [海鯰科](../Page/海鯰科.md "wikilink") Ariidae

<!-- end list -->

1.  斑海鯰 *Arius maculatus* <small>(Thunberg, 1792)</small>

<!-- end list -->

  - [鮠科](../Page/鮠科.md "wikilink") Bagridae

<!-- end list -->

1.  [脂鮠](../Page/脂鮠.md "wikilink") - *Pseudobagrus adiposalis*
    <small>Oshima, 1919</small> ※特有種
2.  [短臀鮠](../Page/短臀鮠.md "wikilink") - *Pseudobagrus brevianalis*
    <small>Regan, 1908</small> ※特有種
    1.  短臂鮠中台灣亞種 （日月潭鮠） - *P. b. brevianalis* <small>Regan, 1908</small>
        ※特有種
    2.  短臀鮠北台灣亞種 （台灣鮠） - *P. b. taiwanensis* <small>Oshima, 1919</small>
        ※特有種

<!-- end list -->

  - [塘虱魚科](../Page/塘虱魚科.md "wikilink") Clariidae

<!-- end list -->

1.  蟾鬍鯰 （[泰國塘虱魚](../Page/泰國塘虱魚.md "wikilink")） - *Clarias batrachus*
    ※外來種
2.  [鬍子鯰](../Page/鬍子鯰.md "wikilink") （塘虱、土蝨） - *Clarias fuscus*
    <small>(Lacepede, 1803)</small>

<!-- end list -->

  - [甲鯰科](../Page/甲鯰科.md "wikilink") Loricariidae

<!-- end list -->

1.  多輻脂身鯰 （[琵琶鼠](../Page/琵琶鼠.md "wikilink")） - *Liposarcus
    multiradiatus* ※外來種
2.  下口鯰 （琵琶鼠） - *Hypostomus plecostomus* ※外來種

<!-- end list -->

  - 鯰科 Pangasiidae

<!-- end list -->

1.  低眼鯰 （[泰國鯰](../Page/泰國鯰.md "wikilink")） - *Pangasius hypophthalmus*
    ※外來種

<!-- end list -->

  - [油鯰科](../Page/油鯰科.md "wikilink") Pimelodidae

<!-- end list -->

1.  條紋鴨嘴鯰 （虎皮鴨嘴） - *Pseudoplatystoma fasciatum* ※外來種
2.  [紅尾護頭鱨](../Page/紅尾護頭鱨.md "wikilink")（[紅尾鴨嘴](../Page/紅尾鴨嘴.md "wikilink")）
    - *Phractocephalus hemioliopterus* ※外來種

<!-- end list -->

  - [鯰科](../Page/鯰科.md "wikilink") Siluridae

<!-- end list -->

1.  [花鯰](../Page/花鯰.md "wikilink") - *Parasilurus asotus*
    <small>Linnaeus, 1758</small>

## [胡瓜魚目](../Page/胡瓜魚目.md "wikilink") Osmeriformes

  - [胡瓜魚科](../Page/胡瓜魚科.md "wikilink") Osmeridae

<!-- end list -->

1.  [香魚](../Page/香魚.md "wikilink") - *Plecoglossus altivelis*
    <small>(Temminck & Schlegel, 1846)</small>
    1.  香魚迴游型 - *P. a. altivelis* (Amphidromous form) ※滅絕
    2.  香魚陸封型 - *P. a. altivelis* (Land-locked form) ※外來種

<!-- end list -->

  - [銀魚科](../Page/銀魚科.md "wikilink") Salangidae

<!-- end list -->

1.  [尖頭銀魚](../Page/尖頭銀魚.md "wikilink") (有明銀魚) - *Salanx ariakensis*
    <small>Kishinouye, 1902</small> ※滅絕

## [鮭形目](../Page/鮭形目.md "wikilink") Salmoniformes

[Oncorhynchus_masou_formosanus.jpg](https://zh.wikipedia.org/wiki/File:Oncorhynchus_masou_formosanus.jpg "fig:Oncorhynchus_masou_formosanus.jpg")

  - [鮭科](../Page/鮭科.md "wikilink") Salmonidae

<!-- end list -->

1.  [台灣鱒](../Page/台灣鱒.md "wikilink") (台灣鉤吻鮭) - *Oncorhynchus masou
    formosanum* <small>(Jordan & Oshima, 1919)</small> ※特有種
2.  [虹鱒](../Page/虹鱒.md "wikilink") (麥奇鉤吻鮭) - *Oncorhynchus mykiss*
    <small>(Walbaum, 1792)</small> ※外來種

## [仙女魚目](../Page/仙女魚目.md "wikilink") Aulopiformes

  - [狗母魚科](../Page/狗母魚科.md "wikilink") Synodontidae

<!-- end list -->

1.  雲紋蛇鯔 (狗母梭) - *Saurida nebulosa*

## [鯔形目](../Page/鯔形目.md "wikilink") Mugiliformes

  - [鯔科](../Page/鯔科.md "wikilink") Mugilidae

<!-- end list -->

1.  前鱗鮻 - *Liza affinis* <small>(Gunther, 1861)</small>
2.  [大鱗鮻](../Page/大鱗鮻.md "wikilink") (大鱗鯔) - *Liza macrolepis*
    <small>(Smith, 1846)</small>
3.  [白鮻](../Page/白鮻.md "wikilink") - *Liza subviridis*
    <small>(Valenciennes, 1836)</small>
4.  [鯔](../Page/鯔.md "wikilink") (烏魚) - *Mugil cephalus*
    <small>Linnaeus, 1758</small>
5.  長鰭凡鯔 - *Valamugil cunnesius* <small>(Valenciennes, 1836)</small>

## [銀漢魚目](../Page/銀漢魚目.md "wikilink") Atheriniformes

  - [銀漢魚科](../Page/銀漢魚科.md "wikilink") Atherinidae

<!-- end list -->

1.  凡氏下銀漢魚 - *Hypoatherina valenciennei* <small>(Bleeker, 1853)</small>

## [頜針魚目](../Page/頜針魚目.md "wikilink") Beloniformes

  - [異鱂科](../Page/異鱂科.md "wikilink") Adrianichthyidae

<!-- end list -->

1.  [青鱂](../Page/青鱂.md "wikilink") - *Oryzias latipes* <small>(Temminck
    & Schlegel, 1846)</small> ※疑滅絕

<!-- end list -->

  - [鱵科](../Page/鱵科.md "wikilink") Hemiramphidae

<!-- end list -->

1.  [台灣下鱵](../Page/台灣下鱵.md "wikilink") - *Hyporhamphus taiwanensis*
    <small>Collette & Su, 1986</small> ※滅絕
2.  董氏異鰭鱵 - *Zenarchopterus dunckeri* <small>Mohr, 1926</small>

## [鱂形目](../Page/鱂形目.md "wikilink") Cyprinodontiformes

  - [花鱂科](../Page/花鱂科.md "wikilink") Poeciliidae

<!-- end list -->

1.  [食蚊魚](../Page/食蚊魚.md "wikilink") (大肚魚) - *Gambusia affinis*
    <small>(Baird & Girard, 1853)</small> ※外來種
2.  [茉莉花鱂](../Page/茉莉花鱂.md "wikilink") (摩麗) - *Poecilia latipinna*
    <small>(Lesueur, 1821)</small> ※外來種
3.  [孔雀花鱂](../Page/孔雀花鱂.md "wikilink") (孔雀魚) - *Poecilia reticulata*
    <small>Peters, 1859</small> ※外來種
4.  [帆鰭花鱂](../Page/帆鰭花鱂.md "wikilink") (立帆摩麗) - *Poecilia velifera*
    <small>Regan, 1914</small> ※外來種
5.  [劍尾魚](../Page/劍尾魚.md "wikilink") - *Xiphophorus hellerii* ※外來種

## [刺魚目](../Page/刺魚目.md "wikilink") Gasterosteiformes

  - [海龍科](../Page/海龍科.md "wikilink") Syngnathidae

<!-- end list -->

1.  無棘海龍 - *Microphis leiaspis* <small>(Bleeker, 1853)</small>
2.  [印尼海龍](../Page/印尼海龍.md "wikilink") (瑪那海龍) - *Microphis manadensis*
    <small>(Bleeker, 1856)</small> ※疑滅絕

## [合鰓魚目](../Page/合鰓魚目.md "wikilink") Synbranchiformes

  - [棘鰍科](../Page/棘鰍科.md "wikilink") Mastacembelidae

<!-- end list -->

1.  [棘鰍](../Page/棘鰍.md "wikilink") (長吻棘鰍) - *Macrognathus aculeatum*
    <small>(Bloch, 1786)</small> ※疑滅絕
2.  [小林氏棘鰍](../Page/小林氏棘鰍.md "wikilink") - *Macrognathus kobayashi* ※滅絕

<!-- end list -->

  - [合鰓魚科](../Page/合鰓魚科.md "wikilink") Synbranchidae

<!-- end list -->

1.  [黃鱔](../Page/黃鱔.md "wikilink") (鱔魚) - *Monopterus albus*
    <small>(Zuiew, 1793)</small>

## [鮋形目](../Page/鮋形目.md "wikilink") Scorpaeniformes

  - [牛尾魚科](../Page/牛尾魚科.md "wikilink") Platycephalidae

<!-- end list -->

1.  印度牛尾魚 - *Platycephalus indicus* <small>(Linnaeus, 1758)</small>

## [鱸形目](../Page/鱸形目.md "wikilink") Perciformes

  - [玻璃魚科](../Page/玻璃魚科.md "wikilink") Ambassidae

<!-- end list -->

1.  [少棘雙邊魚](../Page/少棘雙邊魚.md "wikilink") (小雙邊魚) - *Ambassis miops*
2.  [細尾雙邊魚](../Page/細尾雙邊魚.md "wikilink") (尾紋雙邊魚) - *Ambassis urotaenia*
3.  蘭副雙邊魚 ([玻璃魚](../Page/玻璃魚.md "wikilink")) - *Parambassis ranga* ※外來種

<!-- end list -->

  - [天竺鯛科](../Page/天竺鯛科.md "wikilink") Apogonidae

<!-- end list -->

1.  扁頭天竺鯛 - *Apogon hyalosoma* <small>Bleeker, 1852</small>

<!-- end list -->

  - [鰺科](../Page/鰺科.md "wikilink") Carangidae

<!-- end list -->

1.  [大口逆鈎鰺](../Page/大口逆鈎鰺.md "wikilink") - *Scomberoides
    commersonnianus* <small>Lacepede, 1801</small>
2.  [托爾逆鈎鰺](../Page/托爾逆鈎鰺.md "wikilink") - *Scomberoides tol*
    <small>(Cuvier, 1832)</small>

<!-- end list -->

  - [棘臀魚科](../Page/棘臀魚科.md "wikilink") Centrarchidae

<!-- end list -->

1.  [大口鱸](../Page/大口鱸.md "wikilink") - *Micropterus salmoides* ※外來種

<!-- end list -->

  - [鑽嘴魚科](../Page/鑽嘴魚科.md "wikilink") Gerreidae

<!-- end list -->

1.  [短鑽嘴魚](../Page/短鑽嘴魚.md "wikilink") - ''Gerres erythrourus ''
    <small>(Bloch, 1791)</small>
2.  [曳絲鑽嘴魚](../Page/曳絲鑽嘴魚.md "wikilink") - *Gerres filamentosus*
    <small>Cuvier, 1829</small>
3.  [奧奈鑽嘴魚](../Page/奧奈鑽嘴魚.md "wikilink") - *Gerres oyena*
    <small>(Forsskal, 1775)</small>

<!-- end list -->

  - [石鱸科](../Page/石鱸科.md "wikilink") Haemulidae

<!-- end list -->

1.  [星雞魚](../Page/星雞魚.md "wikilink") - *Pomadasys kaakan*
    <small>(Cuvier, 1830)</small>
2.  [斑雞魚](../Page/斑雞魚.md "wikilink") - *Pomadasys maculatus*
    <small>(Bloch, 1793)</small>

<!-- end list -->

  - [湯鯉科](../Page/湯鯉科.md "wikilink") Kuhliidae

<!-- end list -->

1.  [湯鯉](../Page/湯鯉.md "wikilink") - *Kuhlia marginata* <small>(Cuvier,
    1829)</small>
2.  [銀湯鯉](../Page/銀湯鯉.md "wikilink") - *Kuhlia mugil* <small>(Forster,
    1801)</small>
3.  [大口湯鯉](../Page/大口湯鯉.md "wikilink") - *Kuhlia rupestris*
    <small>(Lacepede, 1802)</small>

<!-- end list -->

  - [尖嘴鱸科](../Page/尖嘴鱸科.md "wikilink") Latidae

<!-- end list -->

1.  [尖吻鱸](../Page/尖吻鱸.md "wikilink") - *Lates calcarifer* <small>(Bloch,
    1790)</small>

<!-- end list -->

  - [鰏科](../Page/鰏科.md "wikilink") Leiognathidae

<!-- end list -->

1.  [短吻鰏](../Page/短吻鰏.md "wikilink") - *Leiognathus brevirostris*
    <small>(Valenciennes, 1835)</small>
2.  [短棘鰏](../Page/短棘鰏.md "wikilink") - *Leiognathus equulus*
    <small>(Forsskal, 1775)</small>
3.  [黑邊鰏](../Page/黑邊鰏.md "wikilink") - *Leiognathus splendens*
    <small>(Cuvier, 1829)</small>
4.  [仰口鰏](../Page/仰口鰏.md "wikilink") - *Secutor ruconius*
    <small>(Hamilton, 1822)</small>

<!-- end list -->

  - [笛鯛科](../Page/笛鯛科.md "wikilink") Lutjanidae

<!-- end list -->

1.  [銀紋笛鯛](../Page/銀紋笛鯛.md "wikilink") - *Lutjanus argentimaculatus*
    <small>(Forsskal, 1775)</small>
2.  [黃足笛鯛](../Page/黃足笛鯛.md "wikilink") - *Lutjanus fulvus*
    <small>(Forster, 1801)</small>
3.  [黑斑笛鯛](../Page/黑斑笛鯛.md "wikilink") - *Lutjanus johnii*
    <small>(Bloch, 1792)</small>
4.  [黑星笛鯛](../Page/黑星笛鯛.md "wikilink") - *Lutjanus russellii*
    <small>(Bleeker, 1849)</small>

<!-- end list -->

  - [銀鱗鯧科](../Page/銀鱗鯧科.md "wikilink") Monodactylidae

<!-- end list -->

1.  [銀鱗鯧](../Page/銀鱗鯧.md "wikilink") - *Monodactylus argenteus*
    <small>(Linnaeus, 1758)</small>

<!-- end list -->

  - [夢鱸科](../Page/夢鱸科.md "wikilink") Moronidae

<!-- end list -->

1.  [日本真鱸](../Page/日本真鱸.md "wikilink") ([鱸魚](../Page/鱸魚.md "wikilink"))
    - *Lateolabrax japonicus* <small>(Cuvier, 1828)</small>

<!-- end list -->

  - [鬚鯛科](../Page/鬚鯛科.md "wikilink") Mullidae

<!-- end list -->

1.  黃帶緋鯉 - *Upeneus sulphureus* <small>Cuvier, 1829</small>

<!-- end list -->

  - [石首魚科](../Page/石首魚科.md "wikilink") Sciaenidae

<!-- end list -->

1.  道氏叫姑魚 (中華叫姑魚) - *Johnius dussumieri* <small>(Cuvier, 1830)</small>
2.  紅擬石首魚 (紅鼓魚) - *Sciaenops ocellatus* ※外來種

<!-- end list -->

  - [鮨科](../Page/鮨科.md "wikilink") Serranidae

<!-- end list -->

1.  [點帶石斑](../Page/點帶石斑.md "wikilink") - *Epinephelus coioides*
    <small>(Hamilton, 1822)</small>

<!-- end list -->

  - [沙鮻科](../Page/沙鮻科.md "wikilink") Sillaginidae

<!-- end list -->

1.  [沙鮻](../Page/沙鮻.md "wikilink") - *Sillago sihama* <small>(Forsskal,
    1775)</small>

<!-- end list -->

  - [鯛科](../Page/鯛科.md "wikilink") Sparidae

<!-- end list -->

1.  [灰鰭鯛](../Page/灰鰭鯛.md "wikilink") - *Acanthopagrus berda*
    <small>(Forsskal, 1775)</small>
2.  [黃鰭鯛](../Page/黃鰭鯛.md "wikilink") - *Acanthopagrus latus*
    <small>(Houttuyn, 1782)</small>
3.  [黑鯛](../Page/黑鯛.md "wikilink") - *Acanthopagrus schlegeli*
    <small>(Bleeker, 1854)</small>

<!-- end list -->

  - [鯻科](../Page/鯻科.md "wikilink") Terapontidae

<!-- end list -->

1.  銀島鯻 - *Mesopristes argenteus* <small>(Cuvier, 1829)</small>
2.  [格紋島鯻](../Page/格紋島鯻.md "wikilink") (橫紋雞魚) - *Mesopristes
    cancellatus* <small>(Cuvier, 1829)</small>
3.  四線列牙鯻 (四線雞魚) - *Pelates quadrilineatus* <small>(Bloch, 1790)</small>
4.  [花身鯻](../Page/花身鯻.md "wikilink")
    ([花身雞魚](../Page/花身雞魚.md "wikilink")) - *Therapon
    jarbua* <small>(Forsskal, 1775)</small>
5.  條紋鯻 (條紋雞魚) - *Terapon theraps* <small>(Cuvier, 1829)</small>

<!-- end list -->

  - [慈鯛科](../Page/慈鯛科.md "wikilink") Cichlidae

<!-- end list -->

1.  橘色雙冠麗魚 (紅魔鬼) - *Amphilophus citrinellus* ※外來種
2.  巴西珠母麗魚 (西德寶石) - *Geophagus brasiliensis* ※外來種
3.  [莫三比克口孵魚](../Page/莫三比克口孵魚.md "wikilink")
    ([南洋鯽](../Page/南洋鯽.md "wikilink")) - *Oreochromis
    mossambica* <small>(Peters, 1852)</small> ※外來種
4.  [尼羅河口孵魚](../Page/尼羅河口孵魚.md "wikilink")
    (尼羅[吳郭魚](../Page/吳郭魚.md "wikilink")) - *Oreochromis
    nilotica* <small>(Linnaeue,1758)</small> ※外來種
5.  馬拉麗體魚 (珍珠石斑) - *Parachromis managuensis* ※外來種
6.  [吉利慈鯛](../Page/吉利慈鯛.md "wikilink") (吉利吳郭魚) - *Tilapia zillii*
    <small>(Gervais, 1848)</small> ※外來種

<!-- end list -->

  - [{{僻字](../Page/䲁科.md "wikilink") Blenniidae

<!-- end list -->

1.  [斑頭肩鰓{{僻字](../Page/斑頭肩鰓䲁.md "wikilink") - *Omobranchus
    fasciolatoceps* <small>(Richardson, 1846)</small>
2.  [黑斑肩鰓{{僻字](../Page/黑斑肩鰓䲁.md "wikilink") - *Omobranchus ferox*
    <small>(Herre, 1927)</small>

<!-- end list -->

  - [塘鱧科](../Page/塘鱧科.md "wikilink") Eleotridae

<!-- end list -->

1.  中國烏塘鱧 ([中國塘鱧](../Page/中國塘鱧.md "wikilink")) - *Bostrychus sinensis*
    <small>Lacepede, 1801</small>
2.  花錐脊塘鱧 - *Butis koilomatodon* <small>(Bleeker, 1849)</small>
3.  黑斑脊塘鱧 - *Butis melanostigma* <small>(Bleeker, 1849)</small>
4.  [蓋刺塘鱧](../Page/蓋刺塘鱧.md "wikilink") ([塘鱧](../Page/塘鱧.md "wikilink"))
    - *Eleotris acanthopoma* <small>Bleeker, 1853</small>
5.  [褐塘鱧](../Page/褐塘鱧.md "wikilink") (棕塘鱧) - *Eleotris fusca*
    <small>(Schneider & Forster, 1801)</small>
6.  黑塘鱧 - *Eleotris melanosoma* <small>Bleeker, 1852</small>
7.  [尖頭塘鱧](../Page/尖頭塘鱧.md "wikilink") (銳頭塘鱧) - *Eieotris oxycephala*
    <small>Temminck & Schlegel, 1845</small>
8.  [短塘鱧](../Page/短塘鱧.md "wikilink") - *Hypseleotris bipartita*
    <small>Herre, 1927</small>
9.  擬鯉短塘鱧 - *Hypseleotris cyprinoides* <small>(Valenciennes,
    1837)</small>
10. [無孔塘鱧](../Page/無孔塘鱧.md "wikilink") - *Ophiocara aporos*
    <small>(Bleeker, 1854)</small>
11. 頭孔塘鱧 - *Ophiocara porocephala* <small>(Valenciennes, 1837)</small>
12. 斑駁尖塘鱧 (筍殼魚) - *Oxyeleotris marmorata* <small>(Bleeker, 1852)</small>
    ※外來種

<!-- end list -->

  - [鰕虎科](../Page/鰕虎科.md "wikilink") Gobiidae

<!-- end list -->

1.  綠斑細棘鰕虎 (綠斑閩鰕虎) - *Acentrogobius chlorostigmatoides* <small>(Bleeker,
    1849)</small>
2.  普氏細棘鰕虎 (普氏閩鰕虎) - *Acentrogobius pflaumii* <small>(Bleeker,
    1853)</small>
3.  雀細棘鰕虎 (雀鰕虎) - *Acentrogobius viganensis* <small>(Steindachner,
    1893)</small>
4.  青斑細棘鰕虎 - *Acentrogobius viridipunctatus* <small>(Valenciennes,
    1837)</small>
5.  短斑臥齒鯊 - *Apocryptodon punctatus* <small>Tomiyama, 1934</small>
6.  曙首厚唇鯊 - *Awaous melanocephalus* <small>(Bleeker, 1849)</small>
7.  眼斑厚唇鯊 - *Awaous ocellaris* <small>(Broussonet, 1782)</small>
8.  椰子深鰕虎 - *Bathygobius cocosensis* <small>(Bleeker, 1854)</small>
9.  [大彈塗魚](../Page/大彈塗魚.md "wikilink") - *Boleophthalmus pectinirostris*
    <small>(Linnaeus, 1758)</small>
10. 神島硬皮鰕虎 - *Callogobius tanegasimae* <small>(Snyder, 1908)</small>
11. 淺色項冠鰕虎 (那氏脊鯊) - *Cristatogobius nonatoae* <small>(Ablan
    1940)</small>
12. 谷津氏猴鯊 (亞氏猴鯊) - *Cryptocentrus yatsui* <small>Tomiyama, 1936</small>
    ※特有種
13. 小頭櫛赤鯊 (櫛赤鯊) - *Ctenotrypauchen microcephalus* <small>(Bleeker,
    1860)</small>
14. 鸚哥鯊 - *Exyrias puntang* <small>(Bleeker, 1851)</small>
15. 裸頸斑點鰕虎 - *Favonigobius gymnauchen* <small>(Bleeker, 1860)</small>
16. 金叉舌鰕虎 - *Glossogobius aureus* <small>Akihito & Meguro, 1975</small>
17. 雙鬚叉舌鰕虎 - *Glossogobius bicirrhosus* <small>(Weber, 1894)</small>
18. 雙斑叉舌鰕虎 - *Glossogobius biocellatus* <small>(Valenciennes,
    1837)</small>
19. 橫列叉舌鰕虎 - *Glossogobius circumspectus* <small>(Macleay, 1883)</small>
20. 叉舌鰕虎 (正叉舌鰕虎) - *Glossogobius giuris* <small>(Hamilton, 1822)</small>
21. 點帶叉舌鰕虎 - *Glossogobius olivaceus* <small>(Temminck & Schlegel,
    1845)</small>
22. 霍氏間鰕虎 - *Hemigobius hoevenii* <small>(Bleeker, 1851)</small>
23. 斑點竿鯊 - *Luciogobius guttatus* <small>Gill, 1859</small>
24. 阿部氏鯔鰕虎 - *Mugilogobius abei* <small>(Jordan & Snyder, 1901)</small>
25. 小鯔鰕虎 - *Mugilogobius cavifrons* <small>(Weber, 1909)</small>
26. 楊氏羽衣鯊 - *Myersina yangii* <small>(Chen, 1960)</small> ※特有種 / 滅絕
27. 尖鰭寡鱗鰕虎 (斑點狹鯊) - *Oligolepis acutipennis* <small>(Valenciennes,
    1837)</small>
28. 大口寡鱗鰕虎 - *Oligolepis stomias* <small>(Smith, 1941)</small>
29. 眼絲鴿鯊 - *Oxyurichthys ophthalmonema* <small>(Bleeker,
    1856-57)</small>
30. 維沙亞鴿鯊 (南方溝鰕虎) - *Oxyurichthys visayanus* <small>Herre, 1927</small>
31. [彈塗魚](../Page/彈塗魚.md "wikilink") - *Periophthalmus modestus*
    <small>Cantor, 1842</small>
32. 爪哇擬鰕虎 - *Pseudogobius javanicus* <small>(Bleeker, 1856)</small>
33. 小擬鰕虎 - *Pseudogobius masago* <small>(Tomiyama, 1936)</small>
34. 拜庫雷鰕虎 (巴庫寡棘鰕虎) - *Redigobius bikolanus* <small>(Herre, 1927)</small>
35. 明潭吻鰕虎 - *Rhinogobius candidianus* <small>(Regan,1908)</small> ※特有種
36. 細斑吻鰕虎 - *Rhinogobius delicatus* <small>(Regan, 1908)</small> ※特有種
37. 大吻鰕虎 - *Rhinogobius gigas* <small>Chen & Shao, 1996</small> ※特有種
38. 極樂吻鰕虎 - *Rhinogobius giurinus* <small>Aonuma & Chen, 1996</small>
39. 恆春吻鰕虎 - *Rhinogobius henchuenensis* <small>(Rutter, 1897)</small>
    ※特有種
40. 蘭嶼吻鰕虎 - *Rhinogobius lanyuensis* <small>Chen & Shao, 1996</small>
    ※特有種
41. 斑帶吻鰕虎 - *Rhinogobius maculafasciatus* <small>Chen, Miller & Fang,
    1998</small> ※特有種
42. 台灣吻鰕虎 - *Rhinogobius nagoyae formosanus* <small>Oshima, 1919</small>
    ※特有種
43. 南台吻鰕虎 - *Rhinogobius nantaiensis* <small>Aonuma & Chen, 1996</small>
    ※特有種
44. 短吻褐斑吻鰕虎 - *Rhinogobius rubromaculatus* <small>Lee & Chang,
    1996</small> ※特有種
45. [大青彈塗魚](../Page/大青彈塗魚.md "wikilink") - *Scartelaos gigas* <small>Chu
    & Wu, 1963</small>
46. [青彈塗魚](../Page/青彈塗魚.md "wikilink") - *Scartelaos histophorus*
    <small>(Valenciennes, 1837)</small>
47. 寬帶裸身鰕虎 - *Schismatogobius ampluvinculus* <small>Chen, Shao & Fang,
    1995</small>
48. 羅氏裸身鰕虎 - *Schismatogobius roxasi* <small>Herre, 1936</small>
49. 日本禿頭鯊 - *Sicyopterus japonicus* <small>(Tanaka, 1909)</small>
50. 寬頰瓢鰭鰕虎 (寬頰禿頭鯊) - *Sicyopterus macrostetholepis* <small>(Bleeker,
    1853)</small>
51. 環帶黃瓜鰕虎 - *Sicyopus zosterophorum* <small>(Bleeker, 1857)</small>
52. 紫身枝牙鰕虎 (菲律賓枝牙鰕虎) - *Stiphodon atropurpureus* <small>(Herre,
    1927)</small>
53. 黑鰭枝牙鰕虎 - *Stiphodon percnopterygionus* <small>Watson & Chen,
    1998</small>
54. 鬚鰻鰕虎 (灰盲條魚) - *Taenioides cirratus* <small>(Blyth, 1860)</small>
55. 雙帶縞鰕虎 - *Tridentiger bifasciatus* <small>Steindachner, 1881</small>
56. 裸頸縞鰕虎 - *Tridentiger nudicervicus* <small>Tomiyama, 1934</small>
57. 赤鯊 - *Trypauchen vagina* <small>(Bloch & Schneider, 1801)</small>
58. 雲紋鰕虎 (雲紋鰕虎) - *Yongeichthys nebulosus* <small>(Forsskal,
    1775)</small>

<!-- end list -->

  - [溪鱧科](../Page/溪鱧科.md "wikilink") Rhyacichthyidae

<!-- end list -->

1.  [溪鱧](../Page/溪鱧.md "wikilink") - *Rhyacichthys aspro* <small>(Kuhl &
    van Hasselth, 1837)</small>

<!-- end list -->

  - [金錢魚科](../Page/金錢魚科.md "wikilink") Scatophagidae

<!-- end list -->

1.  [金錢魚](../Page/金錢魚.md "wikilink") - *Scatophagus argus*
    <small>(Linnaeus, 1766)</small>

<!-- end list -->

  - [籃子魚科](../Page/籃子魚科.md "wikilink") Siganidae

<!-- end list -->

1.  長鰭籃子魚 - *Siganus canaliculatus* <small>(Park, 1797)</small>

<!-- end list -->

  - [金梭魚科](../Page/金梭魚科.md "wikilink") Sphyraenidae

<!-- end list -->

1.  布氏金梭魚 - *Sphyraena putnamae* <small>Jordan & Seale 1905</small>

<!-- end list -->

  - [攀鱸科](../Page/攀鱸科.md "wikilink") Anabantidae

<!-- end list -->

1.  [龜殼攀鱸](../Page/龜殼攀鱸.md "wikilink") (過山鯽) - *Anabas testudineus* ※疑滅絕

<!-- end list -->

  - [絲足鱸科](../Page/絲足鱸科.md "wikilink") Osphronemidae

<!-- end list -->

1.  五彩搏魚 ([泰國鬥魚](../Page/泰國鬥魚.md "wikilink")) - *Betta splendens* ※外來種
2.  條紋絲腹鱸 - *Colisa fasciata* ※外來種
3.  [蓋斑鬥魚](../Page/蓋斑鬥魚.md "wikilink") (台灣鬥魚) - *Macropodus opercularis*
    <small>Ahl, 1937</small>
4.  [絲鰭毛足鱸](../Page/絲鰭毛足鱸.md "wikilink") (三星攀鱸) - *Trichogaster
    trichopterus* <small>(Pallas, 1770)</small> ※外來種

<!-- end list -->

  - [鱧科](../Page/鱧科.md "wikilink") Channidae

<!-- end list -->

1.  [七星鱧](../Page/七星鱧.md "wikilink") - *Channa asiatica*
    <small>(Linnaeus, 1758)</small>
2.  寬額鱧 - *Channa gachua* ※外來種
3.  [鱧魚](../Page/鱧魚.md "wikilink") - *Channa maculata* <small>(Lacepede,
    1802)</small>
4.  小盾鱧 (紅鱧、魚虎) - *Channa micropeltes* ※外來種
5.  線鱧 ([泰國鱧](../Page/泰國鱧.md "wikilink")) - *Channa striata* ※外來種

## [鰈形目](../Page/鰈形目.md "wikilink") Pleuronectiformes

  - [舌鰨科](../Page/舌鰨科.md "wikilink") Cynoglossidae

<!-- end list -->

1.  大鱗舌鰨 - *Cynoglossus arel* <small>(Bloch & Schneider, 1801)</small>
2.  斑頭舌鰨 - *Cynoglossus puncticeps* <small>(Richardson, 1846)</small>

<!-- end list -->

  - [牙鮃科](../Page/牙鮃科.md "wikilink") Paralichthyidae

<!-- end list -->

1.  大齒斑鮃 - *Pseudorhombus arsius* <small>(Hamilton, 1822)</small>

## [魨形目](../Page/魨形目.md "wikilink") Tetraodontiformes

  - [四齒魨科](../Page/四齒魨科.md "wikilink") Tetraodontidae

<!-- end list -->

1.  [紋腹叉鼻魨](../Page/紋腹叉鼻魨.md "wikilink") - *Arothron hispidius*
    <small>(Linnaeus, 1758)</small>
2.  [鰓斑叉鼻魨](../Page/鰓斑叉鼻魨.md "wikilink") (線紋叉鼻魨) - *Arothron
    immaculatus* <small>(Bloch & Schneider, 1801)</small>
3.  [菲律賓叉鼻魨](../Page/菲律賓叉鼻魨.md "wikilink") (條紋河魨) - *Arothron
    manilensis* <small>(Marion de Proce, 1822)</small>
4.  [凹鼻魨](../Page/凹鼻魨.md "wikilink") (沖繩河魨) - *Chelonodon patoca*
    <small>(Hamilton, 1822)</small>

## 參考來源

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - 台灣物種名錄，"[Checklist of Cartilaginous fishes of
    Taiwan](http://taibnet.sinica.edu.tw/chi/taibnet_species_list.php?T2=%E8%BB%9F%E9%AA%A8%E9%AD%9A%E7%B6%B1&T2_new_value=false&fr=y)"

  - 台灣物種名錄，"[Checklist of Ray-finned fishes of
    Taiwan](http://taibnet.sinica.edu.tw/chi/taibnet_species_list.php?T2=%E6%A2%9D%E9%B0%AD%E9%AD%9A%E7%B6%B1&T2_new_value=false&fr=y)"

## 外部連結

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)
  - [世界魚庫 FishBase](http://www.fishbase.org/)
  - [FishBase: List of Freshwater Fishes reported from
    Taiwan](https://www.fishbase.de/country/CountryChecklist.php?showAll=yes&what=list&trpp=50&c_code=156A&cpresence=Reported&sortby=alpha2&ext_CL=on&ext_pic=on&vhabitat=fresh)\]

[列表](../Category/淡水魚.md "wikilink") [列表](../Category/台灣動物.md "wikilink")
[Category:鱼类列表](../Category/鱼类列表.md "wikilink")