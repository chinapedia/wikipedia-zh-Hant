**台灣農民黨**，簡稱**台農黨**，是[中華民國的一個](../Page/中華民國.md "wikilink")[政黨](../Page/台灣政黨.md "wikilink")，其前身被視為是親[王金平的基層農漁會](../Page/王金平.md "wikilink")，因不滿王未獲[中國國民黨提名](../Page/中國國民黨.md "wikilink")[2008年中華民國總統大選](../Page/2008年中華民國總統大選.md "wikilink")，而於2007年6月15日在[高雄縣](../Page/高雄縣.md "wikilink")[澄清湖風景區召開成立大會](../Page/澄清湖.md "wikilink")，並推舉[雲林縣農會理事長](../Page/雲林縣.md "wikilink")[謝永輝擔任黨主席](../Page/謝永輝.md "wikilink")。2007年7月11日，獲[內政部核備公文](../Page/中華民國內政部.md "wikilink")，成為台灣第128個[政黨](../Page/政黨.md "wikilink")。2007年10月25日，公布其黨旗、黨徽。該黨的創黨理念是「農為國之本，國以民為天，農漁民不是二等國民」，並宣稱要完成農業經濟綱領、制定農業基本法、成立農業部、改善農漁村生活、建立農漁民保險與退休福利制度等使命職責，並且派員參選[2008年中華民國立法委員選舉](../Page/2008年中華民國立法委員選舉.md "wikilink")（第七屆立法委員選舉）。該黨主張為弱勢的農工漁民發聲，於第七屆立法委員選舉中推出11位區域立委及八位不分區立委候選人，然皆未當選，立委選舉後傾向暫停黨務運作。

2007年9月17日，中國國民黨[立委](../Page/立委.md "wikilink")[柯俊雄加入台灣農民黨](../Page/柯俊雄.md "wikilink")，成為該黨在[立法院第一個席次](../Page/立法院.md "wikilink")，並排入第七屆立委不分區名單第二名。不過隨著未過政黨票5%門檻，柯連任失利後，該黨自然失去席次，目前未擁有任何公職，現況不明。

[2007Taipei101RunUp_EnterpriseGroup_Chun-hsiung_Ko.jpg](https://zh.wikipedia.org/wiki/File:2007Taipei101RunUp_EnterpriseGroup_Chun-hsiung_Ko.jpg "fig:2007Taipei101RunUp_EnterpriseGroup_Chun-hsiung_Ko.jpg")的柯俊雄，台灣農民黨黨徽印在其左胸口上\]\]

## 參照

  - [中華民國政黨列表](../Page/中華民國政黨列表.md "wikilink")

## 外部連結

  - [台灣農民黨部落格](http://tw.myblog.yahoo.com/jw!psxfAo.RGB4jUPngffZlJ7sVMDhe/profile)

[Category:臺灣政黨](../Category/臺灣政黨.md "wikilink")
[Category:2007年建立的政黨](../Category/2007年建立的政黨.md "wikilink")
[Category:農業政黨](../Category/農業政黨.md "wikilink")
[Category:2007年台灣建立](../Category/2007年台灣建立.md "wikilink")