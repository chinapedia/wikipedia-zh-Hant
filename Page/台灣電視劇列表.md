以下列出**[台灣](../Page/台灣.md "wikilink")**各家**[電視台](../Page/電視台.md "wikilink")**歷年來曾播出、或正在播出的**[戲劇節目](../Page/電視劇.md "wikilink")**。

## 現正播出中與即將接檔戲劇

以下表格列出台灣當地播出中與即將播出之台灣電視劇

<table>
<thead>
<tr class="header">
<th><p>播出檔次<br />
<small>（星期）</small></p></th>
<th><p>電視臺</p></th>
<th><p>播出中台劇</p></th>
<th><p>播出中戲劇首播日</p></th>
<th><p>接檔戲劇</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>預定首播日</p></td>
<td><p>劇名</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>每週一至週五晚間播出</p></td>
<td><p><a href="../Page/民視無線台.md" title="wikilink">民視</a></p></td>
<td><p><a href="../Page/大時代_(台灣電視劇).md" title="wikilink">大時代</a></p></td>
<td><p>2018年7月3日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p><a href="../Page/炮仔聲_(台灣電視劇).md" title="wikilink">炮仔聲</a></p></td>
<td><p>2018年12月26日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/臺灣電視台.md" title="wikilink">台視</a></p></td>
<td></td>
<td></td>
<td><p>2019年</p></td>
<td><p><a href="../Page/生生世世_(電視劇).md" title="wikilink">生生世世</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中視_(頻道).md" title="wikilink">中視</a></p></td>
<td><p><a href="../Page/鑑識英雄II_正義之戰.md" title="wikilink">鑑識英雄II 正義之戰</a></p></td>
<td><p>2019年3月27日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大愛電視.md" title="wikilink">大愛</a></p></td>
<td><p><a href="../Page/最美的相遇.md" title="wikilink">最美的相遇</a></p></td>
<td><p>2019年3月21日</p></td>
<td><p>2019年4月23日</p></td>
<td><p><a href="../Page/雨過天青.md" title="wikilink">雨過天青</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/TVBS歡樂台.md" title="wikilink">TVBS歡樂台</a></p></td>
<td><p><a href="../Page/女兵日記女力報到.md" title="wikilink">女兵日記女力報到</a></p></td>
<td><p>2018年11月15日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/客家電視台.md" title="wikilink">客家電視台</a></p></td>
<td><p><a href="../Page/日據時代的十種生存法則.md" title="wikilink">日據時代的十種生存法則</a></p></td>
<td><p>2019年4月8日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>每週三、四晚間播出</p></td>
<td><p>台視</p></td>
<td></td>
<td></td>
<td><p>2019年4月24日</p></td>
</tr>
<tr class="even">
<td><p>每週五晚間播出</p></td>
<td><p>台視</p></td>
<td><p><a href="../Page/我是顧家男.md" title="wikilink">我是顧家男</a></p></td>
<td><p>2019年3月1日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>三立都會台</p></td>
<td><p><a href="../Page/一千個晚安.md" title="wikilink">一千個晚安</a></p></td>
<td><p>2019年3月29日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>每週六晚間播出</p></td>
<td><p>台視</p></td>
<td><p><a href="../Page/愛情白皮書_(2019年電視劇).md" title="wikilink">愛情白皮書</a></p></td>
<td><p>2019年3月9日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/公視主頻.md" title="wikilink">公視</a></p></td>
<td></td>
<td></td>
<td><p>2019年5月4日</p></td>
<td><p><a href="../Page/生死接線員.md" title="wikilink">生死接線員</a></p></td>
</tr>
<tr class="even">
<td><p>每週日晚間播出</p></td>
<td><p>台視</p></td>
<td><p><a href="../Page/你有念大學嗎？.md" title="wikilink">你有念大學嗎？</a></p></td>
<td><p>2019年1月6日</p></td>
<td><p>2019年5月5日</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/華視主頻.md" title="wikilink">華視</a></p></td>
<td></td>
<td></td>
<td><p>2019年5月5日</p></td>
<td><p><a href="../Page/最佳利益.md" title="wikilink">最佳利益</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 台視

## 中視

## 華視

## 民視

## [公視](../Page/公視.md "wikilink")

## 三立電視

## 八大電視

## 大愛電視

## 東森電視

## 超視

  - [我們一家都是人](../Page/我們一家都是人.md "wikilink")
  - [秋天的童話](../Page/秋天的童話.md "wikilink")
  - [冬天的童話](../Page/冬天的童話.md "wikilink")
  - [網路美女Live秀](../Page/網路美女Live秀.md "wikilink")
  - [蝴蝶養貓](../Page/蝴蝶養貓.md "wikilink")
  - [交換愛情的女人](../Page/交換愛情的女人.md "wikilink")
  - [星願](../Page/星願.md "wikilink")
  - [33故事館](../Page/33故事館.md "wikilink")

## 東風衛視

  - [八號風球的愛戀](../Page/八號風球的愛戀.md "wikilink")

## 衛視中文台

  - [第8號當舖](../Page/第8號當舖.md "wikilink")
  - [阿薩布魯1號](../Page/阿薩布魯1號.md "wikilink")
  - [驚異傳奇](../Page/驚異傳奇.md "wikilink")
  - [天使情人](../Page/天使情人.md "wikilink")
  - [黑糖瑪奇朵](../Page/黑糖瑪奇朵.md "wikilink")
  - [愛情兩好三壞](../Page/愛情兩好三壞.md "wikilink")
  - [原來我不帥](../Page/原來我不帥.md "wikilink")
  - [黑糖群俠傳](../Page/黑糖群俠傳.md "wikilink")
  - [翻糖花園](../Page/翻糖花園.md "wikilink")
  - [K歌情人夢](../Page/K歌·情人·夢.md "wikilink")
  - [沒有名字的甜點店](../Page/沒有名字的甜點店.md "wikilink")
  - [幸福街第3號出口](../Page/幸福街第3號出口.md "wikilink")

## TVBS歡樂台

## 緯來綜合台

  - [我的兒子是老大](../Page/我的兒子是老大.md "wikilink")
  - [Q狼特勤組](../Page/Q狼特勤組.md "wikilink")

## 富士電視台

  - [來來殭屍](../Page/來來殭屍.md "wikilink")（1988年[殭屍片在日本引發話題](../Page/殭屍.md "wikilink")，日方出資請[金格影藝有限公司拍攝的](../Page/金格影藝有限公司.md "wikilink")[電視影集](../Page/電視影集.md "wikilink")；[台灣未播出](../Page/台灣.md "wikilink")）

## 改編翻拍

依時間撥映排序

  - 臺灣漫畫改編的台劇

<!-- end list -->

  - [葉宏甲漫畫](../Page/葉宏甲.md "wikilink")：[諸葛四郎](../Page/諸葛四郎.md "wikilink")→[四郎與真平](../Page/四郎與真平.md "wikilink")
  - [朱德庸漫畫](../Page/朱德庸.md "wikilink")：[澀女郎](../Page/澀女郎.md "wikilink")
  - [阮光民漫畫](../Page/阮光民.md "wikilink")：[東華春理髮廳](../Page/東華春理髮廳.md "wikilink")

<!-- end list -->

  - 日本（韓國）漫畫改編的台劇

<!-- end list -->

  - [神尾葉子漫畫](../Page/神尾葉子.md "wikilink")：[流星花園](../Page/流星花園.md "wikilink")→[流星花園
    (台灣電視劇)](../Page/流星花園_\(台灣電視劇\).md "wikilink")
  - [上田美和漫畫](../Page/上田美和.md "wikilink")：[蜜桃女孩](../Page/蜜桃女孩.md "wikilink")
  - [鈴木由美子漫畫](../Page/鈴木由美子.md "wikilink")：[醜女大翻身](../Page/醜女大翻身.md "wikilink")
  - [吉住涉漫畫](../Page/吉住涉.md "wikilink")：-{zh-hans:橘子酱男孩;zh-hk:果醬少年;zh-tw:橘子醬男孩;}-→[橘子醬男孩](../Page/橘子醬男孩.md "wikilink")
  - [森永愛漫畫](../Page/森永愛.md "wikilink")：[貧窮貴公子](../Page/貧窮貴公子.md "wikilink")→[貧窮貴公子](../Page/貧窮貴公子_\(台灣電視劇\).md "wikilink")
  - [一-{条}-由香莉漫畫](../Page/一条由香莉.md "wikilink")：[烈愛傷痕](../Page/烈愛傷痕.md "wikilink")
  - [原秀則漫畫](../Page/原秀則.md "wikilink")：[來我家吧](../Page/來我家吧.md "wikilink")
  - [石塚夢見](../Page/石塚夢見.md "wikilink")、[大石けんいち漫畫](../Page/大石けんいち.md "wikilink")：[Hi
    上班女郎](../Page/Hi_上班女郎.md "wikilink")
  - [槙村怜漫畫](../Page/槙村怜.md "wikilink")：[世紀愛情夢幻](../Page/世紀愛情夢幻.md "wikilink")、美味關係→[美味關係
    (台灣電視劇)](../Page/美味關係_\(台灣電視劇\).md "wikilink")
  - [吉村明美漫畫](../Page/吉村明美.md "wikilink")：[薔薇之戀](../Page/薔薇之戀.md "wikilink")
  - [窪之內英策漫畫](../Page/窪之內英策.md "wikilink")：[單身宿舍連環泡](../Page/單身宿舍連環泡.md "wikilink")、[流氓蛋糕店](../Page/流氓蛋糕店.md "wikilink")→[流氓蛋糕店
    (台灣電視劇)](../Page/流氓蛋糕店_\(台灣電視劇\).md "wikilink")
  - [惣領冬實漫畫](../Page/惣領冬實.md "wikilink")：[戰神
    (電視劇)](../Page/戰神_\(電視劇\).md "wikilink")
  - [高梨三葉漫畫](../Page/高梨三葉.md "wikilink")：[惡魔在身邊](../Page/惡魔在身邊.md "wikilink")
  - [多田薰漫畫](../Page/多田薰.md "wikilink")：[淘氣小親親](../Page/淘氣小親親.md "wikilink")→[惡作劇之吻](../Page/惡作劇之吻.md "wikilink")、[惡作劇2吻](../Page/惡作劇2吻.md "wikilink")、[惡作劇之吻
    (2016年)](../Page/惡作劇之吻_\(2016年電視劇\).md "wikilink")
  - [北川美幸漫畫](../Page/北川美幸.md "wikilink")：東京茱麗葉→[東方茱麗葉](../Page/東方茱麗葉.md "wikilink")
  - [中條比紗也漫畫](../Page/中條比紗也.md "wikilink")：[偷偷愛著你](../Page/偷偷愛著你.md "wikilink")→[花樣少年少女
    (2006年電視劇)](../Page/花樣少年少女_\(2006年電視劇\).md "wikilink")
  - [河成賢漫畫](../Page/河成賢.md "wikilink")：Queen's→[至尊玻璃鞋](../Page/至尊玻璃鞋.md "wikilink")
  - [谷地惠美子漫畫](../Page/谷地惠美子.md "wikilink")：美好的夏天→[熱情仲夏](../Page/熱情仲夏.md "wikilink")
  - [藤田和子漫畫](../Page/藤田和子.md "wikilink")：[公主小妹](../Page/公主小妹.md "wikilink")→[公主小妹
    (電視劇)](../Page/公主小妹_\(電視劇\).md "wikilink")
  - [羽海野千花漫畫](../Page/羽海野千花.md "wikilink")：[-{zh-tw:蜂蜜幸運草;
    zh-hk:蜂蜜與四葉草;}-](../Page/蜂蜜與四葉草.md "wikilink")→[蜂蜜幸運草
    (台灣電視劇)](../Page/蜂蜜幸運草_\(台灣電視劇\).md "wikilink")
  - [藤田和子漫畫](../Page/藤田和子.md "wikilink")：桃花小妹 (漫畫)→[桃花小妹
    (電視劇)](../Page/桃花小妹_\(電視劇\).md "wikilink")
  - [七路眺漫畫](../Page/七路眺.md "wikilink")：[-{zh-hans:爱似百汇;zh-hk:夢幻芭菲;zh-tw:愛似百匯;}-](../Page/愛似百匯.md "wikilink")→[愛似百匯
    (電視劇)](../Page/愛似百匯_\(電視劇\).md "wikilink")
  - [畑健二郎漫畫](../Page/畑健二郎.md "wikilink")：[-{zh-hans:旋风管家;zh-tw:旋風管家！;zh-hk:爆笑管家工作日誌;zh-sg:疾风守护者！;}-](../Page/旋風管家.md "wikilink")→[旋風管家
    (電視劇)](../Page/旋風管家_\(電視劇\).md "wikilink")
  - [仲村佳樹漫畫](../Page/仲村佳樹.md "wikilink")：[SKIP
    BEAT！華麗的挑戰](../Page/SKIP_BEAT！華麗的挑戰.md "wikilink")→[華麗的挑戰
    (電視劇)](../Page/華麗的挑戰_\(電視劇\).md "wikilink")
  - [渡瀨悠宇漫畫](../Page/渡瀨悠宇.md "wikilink")：[-{zh-tw:絕對達令;zh-hk:絕對彼氏。;zh-sg:绝对男友;}-](../Page/絕對達令.md "wikilink")→[絕對達令
    (台灣電視劇)](../Page/絕對達令_\(台灣電視劇\).md "wikilink")
  - [柴門文漫畫](../Page/柴門文.md "wikilink")：[愛情白皮書](../Page/愛情白皮書.md "wikilink")→[愛情白皮書
    (2019年電視劇)](../Page/愛情白皮書_\(2019年電視劇\).md "wikilink")

<!-- end list -->

  - 臺灣小說改編的台劇

<!-- end list -->

  - [瓊瑤小說](../Page/瓊瑤.md "wikilink")：[庭院深深](../Page/庭院深深.md "wikilink")、[煙雨濛濛](../Page/煙雨濛濛.md "wikilink")→[煙雨濛濛
    (台灣電視劇)](../Page/煙雨濛濛_\(台灣電視劇\).md "wikilink")、[情深深雨濛濛](../Page/情深深雨濛濛.md "wikilink")、[幾度夕陽紅](../Page/幾度夕陽紅.md "wikilink")、[六個夢](../Page/六個夢.md "wikilink")→[婉君](../Page/婉君.md "wikilink")、[啞妻](../Page/啞妻.md "wikilink")、[三朵花](../Page/三朵花.md "wikilink")、[青青河邊草](../Page/青青河邊草.md "wikilink")、[梅花三弄
    (小說)](../Page/梅花三弄_\(小說\).md "wikilink")→[梅花烙](../Page/梅花烙.md "wikilink")、[鬼丈夫](../Page/鬼丈夫.md "wikilink")、水雲間、[新月格格](../Page/新月格格.md "wikilink")、[還珠格格](../Page/還珠格格.md "wikilink")
  - [廖風德小說](../Page/廖風德.md "wikilink")：隔壁親家→[親戚不計較](../Page/親戚不計較.md "wikilink")
  - [王藍小說](../Page/王藍.md "wikilink")：[藍與黑](../Page/藍與黑.md "wikilink")
  - [李昂小說](../Page/李昂.md "wikilink")：[殺夫](../Page/殺夫.md "wikilink")
  - [杜修蘭小說](../Page/杜修蘭.md "wikilink")：[逆女](../Page/逆女.md "wikilink")
  - [汪笨湖小說](../Page/汪笨湖.md "wikilink")：[草地狀元
    (小說)](../Page/草地狀元_\(小說\).md "wikilink")→[草地狀元
    (電視劇)](../Page/草地狀元_\(電視劇\).md "wikilink")
  - [華嚴小說](../Page/華嚴_\(作家\).md "wikilink")：[花開花落](../Page/花開花落.md "wikilink")
  - [李喬小說](../Page/李喬.md "wikilink")：[寒夜三部曲](../Page/寒夜三部曲.md "wikilink")→[寒夜](../Page/寒夜.md "wikilink")、[寒夜續曲](../Page/寒夜續曲.md "wikilink")
  - [陳文玲小說](../Page/陳文玲.md "wikilink")：[多桑與紅玫瑰](../Page/多桑與紅玫瑰.md "wikilink")
  - [吳豐秋小說](../Page/吳豐秋.md "wikilink")：[後山日先照](../Page/後山日先照.md "wikilink")
  - [廖輝英小說](../Page/廖輝英.md "wikilink")：[負君千行淚](../Page/負君千行淚.md "wikilink")
  - [鄧相揚小說](../Page/鄧相揚.md "wikilink")：[風中緋櫻](../Page/風中緋櫻.md "wikilink")
  - [白先勇小說](../Page/白先勇.md "wikilink")：[孽子](../Page/孽子.md "wikilink")→[孽子
    (電視劇)](../Page/孽子_\(電視劇\).md "wikilink")
  - [汪笨湖構思](../Page/汪笨湖.md "wikilink")、[馬琇芬執筆小說](../Page/馬琇芬.md "wikilink")：台灣豪門爭霸記→[舊情綿綿
    (電視劇)](../Page/舊情綿綿_\(電視劇\).md "wikilink")
  - [東方白小說](../Page/東方白.md "wikilink")：浪淘沙→[浪淘沙
    (電視劇)](../Page/浪淘沙_\(電視劇\).md "wikilink")
  - [黃願小說](../Page/黃願.md "wikilink")：那一天，那一夜→[剪刀石頭布
    (電視影集)](../Page/剪刀石頭布_\(電視影集\).md "wikilink")
  - [侯文詠小說](../Page/侯文詠.md "wikilink")：[危險心靈](../Page/危險心靈.md "wikilink")→[危險心靈
    (電視劇)](../Page/危險心靈_\(電視劇\).md "wikilink")、[白色巨塔
    (台灣小說)](../Page/白色巨塔_\(台灣小說\).md "wikilink")→[白色巨塔
    (2006年電視劇)](../Page/白色巨塔_\(2006年電視劇\).md "wikilink")
  - [洛心小說](../Page/洛心.md "wikilink")：小雛菊→[鬥魚
    (電視劇)](../Page/鬥魚_\(電視劇\).md "wikilink")
  - [鍾肇政小說](../Page/鍾肇政.md "wikilink")：[魯冰花
    (小說)](../Page/魯冰花_\(小說\).md "wikilink")→[魯冰花
    (電視劇)](../Page/魯冰花_\(電視劇\).md "wikilink")
  - [Lowes小說](../Page/Lowes.md "wikilink")：[原來我不帥](../Page/原來我不帥.md "wikilink")
  - [席絹小說](../Page/席絹.md "wikilink")：珠玉在側→[敲敲愛上你](../Page/敲敲愛上你.md "wikilink")
  - [袁哲生小說](../Page/袁哲生.md "wikilink")：[倪亞達](../Page/倪亞達.md "wikilink")
  - [單飛雪小說](../Page/單飛雪.md "wikilink")：王的戀歌→[美樂。加油](../Page/美樂。加油.md "wikilink")
  - [李中小說](../Page/李中.md "wikilink")：[惡男日記
    (電視劇)](../Page/惡男日記_\(電視劇\).md "wikilink")
  - [九把刀小說](../Page/九把刀.md "wikilink")：[媽，親一下！](../Page/媽，親一下！.md "wikilink")
  - [鍾文音小說](../Page/鍾文音.md "wikilink")：[在河左岸](../Page/在河左岸.md "wikilink")
  - [陳玉慧小說](../Page/陳玉慧.md "wikilink")：[徵婚啟事](../Page/徵婚啟事.md "wikilink")→[徵婚啟事
    (電視劇)](../Page/徵婚啟事_\(電視劇\).md "wikilink")
  - [白先勇小說](../Page/白先勇.md "wikilink")：[臺北人](../Page/臺北人.md "wikilink")→[一把青
    (電視劇)](../Page/一把青_\(電視劇\).md "wikilink")
  - [謝里法小說](../Page/謝里法.md "wikilink")：[紫色大稻埕](../Page/紫色大稻埕.md "wikilink")

<!-- end list -->

  - 其他小說改編的台劇

<!-- end list -->

  - [深雪小說](../Page/深雪.md "wikilink")：[第8號當舖](../Page/第8號當舖.md "wikilink")
  - [明曉溪小說](../Page/明曉溪.md "wikilink")：[泡沫之夏](../Page/泡沫之夏.md "wikilink")

<!-- end list -->

  - 其他書籍改編的台劇

<!-- end list -->

  - [幾米繪本](../Page/幾米.md "wikilink")：[地下鐵
    (電視劇)](../Page/地下鐵_\(電視劇\).md "wikilink")
  - [紀寶如自傳](../Page/紀寶如.md "wikilink")：愛，逆轉勝→[珍珠人生](../Page/珍珠人生.md "wikilink")

以下以劇本為表示

  - 翻拍自其他國家電視劇的台劇

<!-- end list -->

  - [高須光聖](../Page/高須光聖.md "wikilink")：[老闆靠邊站](../Page/老闆靠邊站.md "wikilink")→[明日英雄](../Page/明日英雄.md "wikilink")
  - [李熙明](../Page/李熙明.md "wikilink")：[明朗少女成功記](../Page/明朗少女成功記.md "wikilink")→[陽光天使](../Page/陽光天使.md "wikilink")
  - [龍居由佳里](../Page/龍居由佳里.md "wikilink")：[星之金幣](../Page/星之金幣.md "wikilink")→[白色之戀
    (台灣)](../Page/白色之戀_\(台灣\).md "wikilink")
  - [洪貞恩](../Page/洪貞恩.md "wikilink")、[洪美蘭](../Page/洪美蘭.md "wikilink")：[原來是美男](../Page/原來是美男.md "wikilink")→[原來是美男
    (台灣電視劇)](../Page/原來是美男_\(台灣電視劇\).md "wikilink")
  - [吳善馨](../Page/吳善馨.md "wikilink")、[鄭道允](../Page/鄭道允.md "wikilink")：[童顏美女](../Page/童顏美女.md "wikilink")→[必勝練習生](../Page/必勝練習生.md "wikilink")

<!-- end list -->

  - 被翻拍的台劇

<!-- end list -->

  - [簡遠信](../Page/簡遠信.md "wikilink")：[長男的媳婦](../Page/長男的媳婦.md "wikilink")→[真愛之百萬新娘](../Page/真愛之百萬新娘.md "wikilink")→[百萬新娘之愛無悔](../Page/百萬新娘之愛無悔.md "wikilink")
  - 懷玉公主→[刁蠻公主](../Page/刁蠻公主.md "wikilink")
  - [中條比紗也漫畫](../Page/中條比紗也.md "wikilink")：[偷偷愛著你](../Page/偷偷愛著你.md "wikilink")→[花樣少年少女
    (2006年電視劇)](../Page/花樣少年少女_\(2006年電視劇\).md "wikilink")→[致美麗的你](../Page/花樣少年少女_\(2012年電視劇\).md "wikilink")
  - 花月正春風→[艋舺燿輝](../Page/艋舺燿輝.md "wikilink")
  - [初盈](../Page/初盈.md "wikilink")、[周家楹](../Page/周家楹.md "wikilink")：[王子變青蛙](../Page/王子變青蛙.md "wikilink")→[愛情睡醒了](../Page/愛情睡醒了.md "wikilink")
  - [林齡齡等](../Page/林齡齡.md "wikilink")：[天下父母心](../Page/天下父母心.md "wikilink")→[夏家三千金](../Page/夏家三千金.md "wikilink")
  - [李順慈](../Page/李順慈.md "wikilink")：[太陽花
    (電視劇)](../Page/太陽花_\(電視劇\).md "wikilink")→[愛可以重來](../Page/愛可以重來.md "wikilink")
  - [錢來也](../Page/錢來也.md "wikilink")
  - [林久愉等](../Page/林久愉.md "wikilink")：[家和萬事興
    (2010年電視劇)](../Page/家和萬事興_\(2010年電視劇\).md "wikilink")→[金玉滿堂
    (中國電視劇)](../Page/金玉滿堂_\(中國電視劇\).md "wikilink")
  - [瓊瑤小說](../Page/瓊瑤.md "wikilink")：[還珠格格](../Page/還珠格格.md "wikilink")→[新還珠格格](../Page/新還珠格格.md "wikilink")
  - 今夜相思雨→[陶之戀](../Page/陶之戀.md "wikilink")
  - [陳玉珊等](../Page/陳玉珊.md "wikilink")：[敗犬女王](../Page/敗犬女王.md "wikilink")→[魔女的戀愛](../Page/魔女的戀愛.md "wikilink")
  - [周平之等](../Page/周平之.md "wikilink")：[命中注定我愛你](../Page/命中注定我愛你.md "wikilink")→[命中注定我愛你
    (韓國電視劇)](../Page/命中注定我愛你_\(韓國電視劇\).md "wikilink")
  - [徐譽庭等](../Page/徐譽庭.md "wikilink")：[我可能不會愛你](../Page/我可能不會愛你.md "wikilink")→[愛你的時間](../Page/愛你的時間.md "wikilink")
  - [陳東漢等](../Page/陳東漢.md "wikilink")：[終極三國](../Page/終極三國.md "wikilink")→[終極三國
    (2017年網絡劇)](../Page/終極三國_\(2017年網絡劇\).md "wikilink")
  - [明曉溪小說](../Page/明曉溪.md "wikilink")：[泡沫之夏](../Page/泡沫之夏.md "wikilink")→[泡沫之夏](../Page/泡沫之夏_\(2018年电视剧\).md "wikilink")

<!-- end list -->

  - 電影改編的台劇

<!-- end list -->

  - [徐克等](../Page/徐克.md "wikilink")：[新龍門客棧](../Page/新龍門客棧.md "wikilink")→[新龍門客棧
    (電視劇)](../Page/新龍門客棧_\(電視劇\).md "wikilink")、[飛越·龍門客棧](../Page/飛越·龍門客棧.md "wikilink")
  - 艋舺的女人→[春子與秀緞](../Page/春子與秀緞.md "wikilink")、[艋舺的女人](../Page/艋舺的女人.md "wikilink")
  - [黃淑筠](../Page/黃淑筠.md "wikilink")、[陳怡如](../Page/陳怡如.md "wikilink")、[黃朝亮](../Page/黃朝亮.md "wikilink")：[夏天協奏曲](../Page/夏天協奏曲.md "wikilink")→[戀夏38℃](../Page/戀夏38℃.md "wikilink")
  - [飛行少年](../Page/飛行少年.md "wikilink")
  - [趙成希](../Page/趙成希.md "wikilink")：[狼少年](../Page/狼少年.md "wikilink")→[狼王子](../Page/狼王子.md "wikilink")

## 參見

  - [台灣電視劇](../Page/台灣電視劇.md "wikilink")
  - [台灣偶像劇列表](../Page/台灣偶像劇列表.md "wikilink")
  - [臺灣電視公司節目列表](../Page/臺灣電視公司節目列表.md "wikilink")
  - [中國電視公司節目列表](../Page/中國電視公司節目列表.md "wikilink")
  - [中華電視公司節目列表](../Page/中華電視公司節目列表.md "wikilink")
  - [民間全民電視公司戲劇節目列表](../Page/民間全民電視公司戲劇節目列表.md "wikilink")
  - [三立電視戲劇節目列表](../Page/三立電視戲劇節目列表.md "wikilink")
  - [八大電視戲劇節目列表](../Page/八大電視戲劇節目列表.md "wikilink")
  - [公共電視台節目列表](../Page/公共電視台節目列表.md "wikilink")

[\*](../Category/台灣電視劇列表.md "wikilink")