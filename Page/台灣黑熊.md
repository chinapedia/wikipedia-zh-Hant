**臺灣黑熊**（[学名](../Page/学名.md "wikilink")：**、[布農語](../Page/布農語.md "wikilink")：tumaz、[賽夏語](../Page/賽夏語.md "wikilink")：somay、[泰雅語](../Page/泰雅語.md "wikilink")：ngarox、[鄒語](../Page/鄒語.md "wikilink")：cmoi、[卡那卡那富語](../Page/卡那卡那富語.md "wikilink")：cumai、[太魯閣語](../Page/德路固語.md "wikilink")：kumay），是[臺灣](../Page/臺灣.md "wikilink")[特有的](../Page/特有種.md "wikilink")[亞洲黑熊](../Page/亞洲黑熊.md "wikilink")[亞種](../Page/亞種.md "wikilink")，胸前的V字型白色斑紋（太魯閣語稱做
rabang）是[亞洲黑熊共有的特徵](../Page/亞洲黑熊.md "wikilink")。台灣黑熊現存族群數量粗估為200\~600隻（2014年估計）\[1\]，出沒於[台灣](../Page/台灣.md "wikilink")[中央山脈海拔](../Page/中央山脈.md "wikilink")1000[公尺](../Page/公尺.md "wikilink")-3500公尺的山區，活動範圍可達50[平方公里](../Page/平方公里.md "wikilink")。早在1932年[日本探險家](../Page/日本.md "wikilink")所著的《台灣哺乳動物圖說》中，因土地開發導致棲息地喪失，臺灣黑熊的數量在下降中。在1989年依照中華民國《[野生動物保育法](../Page/s:野生動物保育法.md "wikilink")》，台灣黑熊被列為[瀕危](../Page/瀕危.md "wikilink")。2001年台灣黑熊被選為[台灣最具代表性的野生動物](../Page/台灣.md "wikilink")，不少台灣公司、企業、地方政府，都選用台灣黑熊作為吉祥物，是著名的台灣意象之一。

## 生物特徵

[Ursus_thibetanus_formosanus_V_character.jpg](https://zh.wikipedia.org/wiki/File:Ursus_thibetanus_formosanus_V_character.jpg "fig:Ursus_thibetanus_formosanus_V_character.jpg")
臺灣黑熊極為強壯，體長120-180[公分](../Page/公分.md "wikilink")，肩寬60-70[公分](../Page/公分.md "wikilink")，最重可達200[公斤](../Page/公斤.md "wikilink")。牠的頭呈圓形、頸部短、眼睛小、吻部很長。牠的頭約26-35公分長，頭圍約40-60公分。耳長8-12公分，牠的吻部形狀像狗，因此許多人習慣稱呼牠為「狗熊」。牠的[尾巴短](../Page/尾巴.md "wikilink")，通常不到10公分。身體覆蓋著粗，濃密而具有光澤的黑毛，在[頸部的毛可以超過](../Page/頸.md "wikilink")10公分長。牠的[頷部末端是白色的](../Page/頷.md "wikilink")，「在胸口有黃色或白色毛呈V字型」、或是弦月的形狀，因此英文中也有「月熊」（Moon
Bear）的稱呼。

## 習性

[Ursus_thibetanus_formosanus_cub_on_tree.jpg](https://zh.wikipedia.org/wiki/File:Ursus_thibetanus_formosanus_cub_on_tree.jpg "fig:Ursus_thibetanus_formosanus_cub_on_tree.jpg")
除了交配期或撫育小熊期間外，通常獨居。白天在樹洞或岩洞內休息，黃昏或夜晚時外出覓食。活動範圍大，擅爬樹，孔武有力，在受傷、受逼迫或保護幼熊時攻擊性強，速度快。冬季時並不冬眠，可能會移至較低海拔區域覓食。
屬[雜食性](../Page/雜食性.md "wikilink")，以小型動物、昆蟲、植物嫩葉、嫩芽、果實為食，台灣黑熊也喜歡吃[蜂蜜和蜂蛹](../Page/蜂蜜.md "wikilink")，也會在河邊捕魚、捕螃蟹、捕蝦等食用。

## 與人的關係

[Mascot_of_Formosan_Black_Bear_@_Yangmingshan_National_Park_in_Taipei,_TAIWAN_臺灣黑熊吉祥物布偶在臺灣臺北陽明山國家公園.jpg](https://zh.wikipedia.org/wiki/File:Mascot_of_Formosan_Black_Bear_@_Yangmingshan_National_Park_in_Taipei,_TAIWAN_臺灣黑熊吉祥物布偶在臺灣臺北陽明山國家公園.jpg "fig:Mascot_of_Formosan_Black_Bear_@_Yangmingshan_National_Park_in_Taipei,_TAIWAN_臺灣黑熊吉祥物布偶在臺灣臺北陽明山國家公園.jpg")布偶在雪季[臺灣](../Page/臺灣.md "wikilink")[臺北](../Page/臺北.md "wikilink")[陽明山國家公園](../Page/陽明山國家公園.md "wikilink")\]\]
台灣黑熊不會主動攻擊人類，若碰上，最好是靜靜、慢慢地退開，不可轉身驚慌大叫和狂奔，這樣可能會引發黑熊加速攻擊。

## 保護狀態

從1998到2000年共有15隻[玉山國家公園的臺灣黑熊被裝上無線電項圈](../Page/玉山國家公園.md "wikilink")，目擊黑熊的事件非常少，沒有證據證明尚有多少隻臺灣黑熊存在。從1989年開始臺灣黑熊就正式被列入保護動物。但盜獵可能沒有因此禁絕，對台灣黑熊的危害依然存在。當年捕捉到的15隻黑熊中，由於盜獵陷阱有8隻喪失了腳趾或是腳掌。

從1989年至2001年間，臺灣黑熊依中華民國《[文化資產保存法](../Page/文化資產保存法.md "wikilink")》列入瀕危動物或珍貴稀有動物，之後在[農委會公告的](../Page/農委會.md "wikilink")《保育類野生動物名錄》中屬於「瀕臨絕種野生動物
」\[2\]。在國際上，[亞洲黑熊屬](../Page/亞洲黑熊.md "wikilink")《[瀕臨絕種野生動植物國際貿易公約](../Page/瀕臨絕種野生動植物國際貿易公約.md "wikilink")》（CITES）的附錄I，禁止國際間對這個物種任何產品與任何形式的交易\[3\]、也是[世界自然保護聯盟的](../Page/世界自然保護聯盟.md "wikilink")《[IUCN紅色名錄](../Page/IUCN紅色名錄.md "wikilink")》易危物种\[4\]。

## 目擊事件

  - 2009年5月20日在[玉山國家公園瓦拉米山屋步道目擊黑熊並加以拍照攝影](../Page/玉山國家公園.md "wikilink")。\[5\]

<!-- end list -->

  - 2011年7月10日台20線205公里處約海拔300－400公尺高的[南橫山區目睹臺灣黑熊之後台鐵](../Page/南橫公路.md "wikilink")[自強號行經](../Page/自強號.md "wikilink")[新武呂溪橋](../Page/新武呂溪.md "wikilink")（[池上](../Page/池上車站_\(臺灣\).md "wikilink")＝[海端間](../Page/海端車站.md "wikilink")）時，有三列班車目擊。\[6\]\[7\]

<!-- end list -->

  - 2017年12月下旬，在[利嘉林道用紅外線數位攝影機拍到](../Page/利嘉林道.md "wikilink")。\[8\]

<!-- end list -->

  - 2019年3月2日，阿里山森林鐵路鐵道目睹拍下臺灣黑熊下山覓食之影片。

## 吉祥物

[YLS_8822.jpg](https://zh.wikipedia.org/wiki/File:YLS_8822.jpg "fig:YLS_8822.jpg")[吉祥物](../Page/吉祥物.md "wikilink")[熊讚](../Page/熊讚Bravo.md "wikilink")\]\]
台灣黑熊是台灣特有的黑熊亞種，所以有些機構以台灣黑熊為藍本設計[吉祥物](../Page/吉祥物.md "wikilink")。

  - [熊讚](../Page/熊讚.md "wikilink")，2017年台北世運大及台北市吉祥物「Bravo」
  - [高雄熊](../Page/高雄熊.md "wikilink")，高雄市吉祥物「Hero」
  - 喔熊，[台灣觀光局吉祥物](../Page/台灣觀光局.md "wikilink")「OhBear」
  - 威熊，[威航吉祥物](../Page/威航.md "wikilink")「V air」
  - 台鐵便當餐旅熊，[台鐵吉祥物](../Page/台鐵.md "wikilink")「鐵魯、漢娜將」
  - 寧夏熊，[寧夏夜市吉祥物](../Page/寧夏夜市.md "wikilink")

## 參考文献

## 外部链接

  - [台灣黑熊保育研究網](http://blackbear.ysnp.gov.tw/)
  - [台灣黑熊保育協會](http://www.taiwanbear.org.tw/)
  - [台灣黑熊保育](https://www.google.com/search?q=%E9%BB%83%E7%BE%8E%E7%A7%80&rlz=1C1CHZL_enTW765TW765&tbm=vid&source=lnt&tbs=dur:l&sa=X&ved=0ahUKEwjs4ZTa_Z7aAhVGlJQKHZ9tAn0QpwUIHQ&biw=1142&bih=655&dpr=2.4)


[Category:熊属](../Category/熊属.md "wikilink")
[Category:台灣哺乳動物](../Category/台灣哺乳動物.md "wikilink")
[Category:瀕危物種](../Category/瀕危物種.md "wikilink")
[Category:台灣特有亞種](../Category/台灣特有亞種.md "wikilink")
[Category:台灣之最](../Category/台灣之最.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.