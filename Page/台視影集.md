《**台視影集**》是[台灣電視公司](../Page/台灣電視公司.md "wikilink")（台視）的[西洋](../Page/西洋.md "wikilink")[電視影集時段](../Page/電視影集.md "wikilink")，始自《[天龍特攻隊](../Page/天龍特攻隊.md "wikilink")》於1984年（[民國73年](../Page/民國73年.md "wikilink")）10月27日（[週六](../Page/週六.md "wikilink")）20:00～21:00在[台視頻道播出後](../Page/台視主頻.md "wikilink")，開始固定成為台視頻道的西洋電視影集時段，逐而成為當時每週六晚間台灣民眾最主要的[娛樂](../Page/娛樂.md "wikilink")，並掀起一波「[美國影集熱](../Page/美國.md "wikilink")」。
[TTV_western_television_drama_series.jpg](https://zh.wikipedia.org/wiki/File:TTV_western_television_drama_series.jpg "fig:TTV_western_television_drama_series.jpg")\]\]

台視在1990年4月28日啟用[企業識別系統之後](../Page/企業識別.md "wikilink")，於每次播出《台視影集》之前，加入了台視自製的[3D電腦動畫作為開場](../Page/三維電腦圖形.md "wikilink")，由台視導播[陳振中為開場動畫唸](../Page/陳振中.md "wikilink")[口白](../Page/口白.md "wikilink")「**台視影集，有口皆碑**」，搭配〈[台視之聲](../Page/台視之聲.md "wikilink")〉結尾旋律作為開場動畫背景音樂，更是讓人深刻難忘。

本時段最後於1993年（[民國82年](../Page/民國82年.md "wikilink")）3月13日結束長達9年的不動地位。隔兩週後，由[張菲與](../Page/張菲.md "wikilink")[費玉清主持的大型](../Page/費玉清.md "wikilink")[綜藝節目](../Page/綜藝節目.md "wikilink")《[龍兄虎弟](../Page/龍兄虎弟_\(電視節目\).md "wikilink")》接棒，其餘影集則分散至各個其他時間播出。

## 播映影集

### 主要時段（週六晚間08:00～10:00）

  - 1984年

:\*[天龍特攻隊](../Page/天龍特攻隊.md "wikilink")（The
A-Team；1984年10月27日～1985年5月18日）

  - 1985年

:\*[勝利大決戰](../Page/勝利大決戰.md "wikilink")（V；1985年6月8日～1985年8月24日）

::數年後台視曾在深夜時段，以原音版本重播。

:\*[飛狼](../Page/飛狼.md "wikilink")（Airwolf；1985年1月1日～1988年1月23日）

  -

      -
        原為深夜時段的原音播出影集，後調動時段並改配國語。

<!-- end list -->

  - 1986年

:\*[百戰天龍](../Page/百戰天龍.md "wikilink")（MacGyver；1986年5月3日～1989年12月30日）

:\*[超人特攻隊](../Page/超人特攻隊_\(電視影集\).md "wikilink")（Misfits of
science；1986年8月16日～1986年12月6日）

:\*[邁阿密天龍](../Page/邁阿密天龍.md "wikilink")（Miami
Vice；1986年10月18日～1992年3月18日）

  -

      -
        《[邁阿密風雲](../Page/邁阿密風雲.md "wikilink")》影集的第二季

<!-- end list -->

  - 1987年

:\*[天龍小奇兵](../Page/天龍小奇兵.md "wikilink")（The Wizard；1987年3月28日～1987年8月1日）

:\*[星際爭霸戰](../Page/星際爭霸戰.md "wikilink")（Star Trek；1987年9月12日～1988年1月23日）

:\*[家有阿福](../Page/家有阿福.md "wikilink")（Alf；1987年10月17日～1989年10月7日）

  - 1988年

:\*[飛龍特攻隊](../Page/飛龍特攻隊.md "wikilink")（Captain Power and the Soldiers
of the Future；1988年2月20日～1988年7月16日）

:\*[電幻天龍](../Page/電幻天龍.md "wikilink")（Automan；1988年6月18日～1988年9月10日）

:\*[大榔頭](../Page/大榔頭.md "wikilink")（Sledge Hammer；1988年7月23日～1989年4月22日）

:\*[我的這一班](../Page/我的這一班_\(美國電視劇\).md "wikilink")（Head Of The
Class；1988年11月26日～1989年4月22日）

  - 1989年

:\*[幼虎遊龍](../Page/幼虎遊龍.md "wikilink")（My Secret
Identity；1989年4月29日～1989年10月7日）

:\*[千面飛龍](../Page/千面飛龍.md "wikilink")（Manimal；1989年10月14日～1989年12月9日）

:\*[太空金銀島](../Page/太空金銀島.md "wikilink")（Der Schatz im All (Treasure
Island in Outer Space)；1989年12月16日～1989年12月30日）

  - 1990年

:\*[外星遊龍](../Page/外星遊龍.md "wikilink")（Hard Time On Planet
Earth；1990年10月6日）

  - 1992年

:\*[百勝天龍](../Page/百勝天龍.md "wikilink")（Young Indiana Jones Chronicles）

::本影集後來重拍成電視電影，由[華視播出](../Page/華視.md "wikilink")。

:\*[銀河飛龍](../Page/銀河飛龍.md "wikilink")（Star Trek: The Next Generation）

::著名科幻影集《[星際爭霸戰](../Page/星際爭霸戰.md "wikilink")》的續篇。共七季，台視於1992年4月4日起播出一、二季，並在1995年11月26日起播出第三季。後來改到週六下午播出，並於第三季結束後停播。

:\*[虎膽妙算](../Page/虎膽妙算.md "wikilink")（Mission: Impossible）

  -

      -
        台視過去曾於1960年代以《赴湯蹈火》譯名播出第一季，1970年代播出第二季之後的內容時，才改名為《虎膽妙算》。當時僅單純採用[音譯方式來稱呼角色](../Page/音譯.md "wikilink")；此次重播乃肇於續作角色中譯名稱的形塑成功，而讓主要人物『逆向』延用相對的稱號。

<!-- end list -->

  - 1993年

:\*[異形終結者](../Page/異形終結者.md "wikilink")（War of the
worlds；1993年2月7日～1993年6月13日）

::1953年電影《[地球爭霸戰](../Page/地球爭霸戰.md "wikilink")》之續集。

:\*[轟雷龍虎鳳](../Page/轟雷龍虎鳳.md "wikilink")（Counterstrike；1993年3月20日～1996年10月16日）

:\*[忍者一條龍](../Page/忍者一條龍.md "wikilink")（Raven；1993年10月16日～1996年8月8日）

  - 1996年

:\*[新虎膽妙算](../Page/新虎膽妙算.md "wikilink")（Mission:
Impossible；1996年7月4日～1997年3月20日，每週四晚22:00～23:00，原音播出）

:\*[天才保姆](../Page/天才保姆.md "wikilink")（The
Nanny；1996年8月10日～9月28日，每週六晚21:40～22:10，原音播出）

### 深夜時段（周一至周五晚間11:30～00:30）

  - [邁阿密風雲](../Page/邁阿密風雲.md "wikilink")（Miami Vice）
  - 英雄本色（Wiseguy）
  - [十三號星期五](../Page/十三號星期五_\(電視劇\).md "wikilink")（Friday the 13th: The
    Series）
  - 警網急先鋒（1990年11月26日～1991年1月21日）
  - [雙峰](../Page/雙峰_\(電視劇\).md "wikilink")（Twin Peaks）
  - [歡樂酒店](../Page/歡樂酒店.md "wikilink")（Cheers）

<!-- end list -->

  -
    本片曾一度改配國語播出，但後來反應不佳改回原音播映。

<!-- end list -->

  - [蕾絲](../Page/蕾絲_\(1984年迷你影集\).md "wikilink")（[Lace](../Page/:en:Lace_\(miniseries\).md "wikilink")）
  - [美國狼人](../Page/美國狼人.md "wikilink")（[Werewolf](../Page/:en:Werewolf_\(TV_series\).md "wikilink")）
  - [吉星高照](../Page/吉星高照.md "wikilink")
  - [雪山三雄](../Page/雪山三雄.md "wikilink")
  - [捍衛戰艦(Supercarrier)](../Page/捍衛戰艦\(Supercarrier\).md "wikilink")

### 台視午夜場（1988年～1996年周六00:00～02:00）

<div style="-moz-column-count:3; column-count:3;">

此時段開播時主要以播映迷你影集為主，首檔播出影集為《天使之怒》。

  - 天使之怒（[Rage of
    Angels](../Page/:en:Rage_of_Angels.md "wikilink")，1988年7月9日～7月16日）
  - 克麗奧小姐（1988年7月23日～8月6日）
  - 天使之怒續集（[Rage of Angels: The Story
    Continues](../Page/:en:Rage_of_Angels.md "wikilink")）
  - 高貴之家（[Noble
    House](../Page/:en:Noble_House_\(miniseries\).md "wikilink")）
  - 沙漠之獅
  - 血濺蘭花（[Blood & Orchids](../Page/:en:Blood_&_Orchids.md "wikilink")）
  - 奧圖曼情史（[Harem](../Page/:en:Harem_\(disambiguation\).md "wikilink")）
  - 早霜（[An Early Frost](../Page/:en:An_Early_Frost.md "wikilink")）
  - 商場雙雄（[Kane & Abel](../Page/:en:Kane_&_Abel.md "wikilink")）
  - 艾曼世家（Emma Heart）
  - 如果有明天（[If Tomorrow
    Comes](../Page/:en:If_Tomorrow_Comes_\(miniseries\).md "wikilink")）
  - 鑽石鑽石（[Master of the
    Game](../Page/:en:Master_of_the_Game_\(novel\).md "wikilink")）
  - 可憐富家女
  - 無情荒漠有情天
  - 真假公主
  - 諸神的風車（[Windmills of the
    Gods](../Page/:en:Windmills_of_the_Gods_\(miniseries\).md "wikilink")）
  - 大都會的迷惘（[I'll Take
    Manhattan](../Page/:en:I'll_Take_Manhattan_\(miniseries\).md "wikilink")）
  - 蒙地卡羅（[Monte
    Carlo](../Page/:en:Monte_Carlo_\(miniseries\).md "wikilink")）
  - 斐隆夫人
  - 龐貝城最後一日（[The Last Days of
    Pompeii](../Page/:en:The_Last_Days_of_Pompeii_\(miniseries\).md "wikilink")，1989年6月10日～6月24日）
  - 牧場風雲
  - 彼得大帝（[Peter The
    Great](../Page/:en:Peter_the_Great_\(miniseries\).md "wikilink")）
  - 迴旋夢裡的男人（[Mistral's
    Daughter](../Page/:en:Mistral's_Daughter.md "wikilink")）
  - 大西部
  - 賈桂琳傳奇（[Jacqueline Bouvier
    Kennedy](../Page/:en:Jacqueline_Bouvier_Kennedy_\(film\).md "wikilink")）
  - 歐納西斯傳
  - 酒國春秋（[Champagne
    Charlie](../Page/:en:Champagne_Charlie_\(1989_film\).md "wikilink")）
  - 基度山恩仇記
  - 豪門玫瑰
  - 悲傷的葡萄
  - 祕密花園（[The Secret
    Garden](../Page/:en:The_Secret_Garden_\(1987_film\).md "wikilink")）
  - 撒哈拉寶藏
  - 心囚
  - [無名恨](../Page/神鬼認證_\(1988年電影\).md "wikilink")（[The Bourne
    Identity](../Page/:en:The_Bourne_Identity_\(1988_film\).md "wikilink")）
  - 新雙城記
  - 電影海
  - 鷹翼舞雄風
  - 朱門公子英雄淚
  - 紳士與玩家
  - 劇場魅影（[The Phantom of the
    Opera](../Page/:en:The_Phantom_of_the_Opera_\(miniseries\).md "wikilink")）
  - 諜戰香蹤
  - 情到深處無怨尤
  - 不凋的玫瑰
  - 一世情緣
  - 英雄有淚
  - 情怨深深
  - 紅伶淚
  - 叱吒風雲一豪門（[The Kenndys Of
    Massachusetts](../Page/:en:The_Kennedys_of_Massachusetts.md "wikilink")）
  - 失足恨
  - 峰迴路轉
  - 遙遠的戰篷（[The Far
    Pavilions](../Page/:en:The_Far_Pavilions.md "wikilink")）
  - 一世英豪
  - 黛西公主（[Princess
    Daisy](../Page/:en:Princess_Daisy_\(miniseries\).md "wikilink")）
  - [刺鳥](../Page/刺鳥.md "wikilink")（[The Thorn
    Birds](../Page/:en:The_Thorn_Birds_\(miniseries\).md "wikilink")，1991年2月23日～3月23日）
  - 夜影殺機
  - 法蘭西絲
  - 海明威生死戀
  - [清秀佳人](../Page/清秀佳人_\(影集\).md "wikilink")（Anne of Green
    Gables，1991年5月18日～6月8日）
  - 情網情網
  - 誰為我伴
  - 孤雛血淚
  - 倫敦世紀懸案
  - 情恨
  - 艾森豪別傳
  - 回首夢已遠（[Judith Krantz's Till We Meet
    Again](../Page/:en:Judith_Krantz's_Till_We_Meet_Again.md "wikilink")）
  - 含淚的蓓蕾
  - 沙達特傳奇（[Sadat](../Page/:en:Sadat_\(miniseries\).md "wikilink")）
  - 何處是歸程（[Which Way
    Home](../Page/:en:Which_Way_Home_\(miniseries\).md "wikilink")）
  - 澳洲驚魂夜
  - 埃及豔后
  - 諜追諜
  - 深情款款
  - 停不了的愛
  - 戰爭與和平
  - 隔離而平等
  - [凱瑟琳女皇](../Page/凱瑟琳女皇.md "wikilink")
  - 紙醉金迷
  - 戰火情緣
  - 琴韻悠揚
  - 柴里尼狂熱的一生
  - 真善美
  - 群雄競技
  - 法國大革命
  - 芳草萋萋
  - 壯志凌雲
  - 深閨情挑
  - 秋水共長天
  - 四海豪情
  - 海上驚魂
  - 梵宮傳奇
  - 雙妃怨
  - 圓舞曲之王史特勞斯
  - 大漠烽火情
  - 愛到深處無怨尤
  - 秋水共長天續集
  - 春夢了無痕－黛安娜
  - 歷盡滄桑一豪門
  - 回首來時路
  - 枕邊驚夢
  - 靈異魔咒（[It](../Page/:en:It_\(1990_film\).md "wikilink")）
  - 無情海島有情天
  - 異形大驚魂
  - 鬼屋魅影
  - 多少柔情多少淚1
  - 多少柔情多少淚2
  - 氣壯山河
  - 血染的青春
  - 緝毒大行動
  - 永不說再見（[Great
    Expectations](../Page/:en:Great_Expectations_\(1991_miniseries\).md "wikilink")）
  - 石破天驚
  - 海角天涯
  - 清秀小佳人（[Road to Avonlea](../Page/:en:Road_to_Avonlea.md "wikilink")）
  - 燃燒的海岸
  - 亂世狂燄
  - 福爾摩斯探案－飛瀑
  - 午夜狂奔
  - 終極大反擊
  - 法外情仇
  - 名流秘辛
  - 歸鄉情斷
  - 福爾摩斯探案－危機
  - 親情夢斷
  - 天涯夢、一世情
  - 柏林佳人
  - 毒梟大追擊
  - 拯救世紀劫
  - 碧血黃沙亂世情
  - 終極天牢
  - 郎心似蛇蠍
  - 海蒂小天使（[Heidi](../Page/:en:Heidi_\(miniseries\).md "wikilink")，1995年2月4日～2月11日）
  - 烈火新娘
  - 根續集（[Alex Haley's
    Queen](../Page/:en:Alex_Haley's_Queen.md "wikilink")）
  - 稚子天倫夢
  - 青春輓歌
  - 我依然戀你如昔
  - 追夢天涯
  - 還我樂土
  - 衝鋒遊俠
  - 未來戰將
  - 戲遊人間
  - 致命情謀
  - 我無罪
  - 照亮我生命
  - 荒野豪情
  - 命運枷鎖
  - 唐吉訶德
  - 追擊密令
  - 天堂來的小子
  - 幽靈船
  - 夜夜買醉的男人
  - 謎蹤三十年
  - 迷霧森林十八年（電影）
  - 暗夜哭聲（電影）
  - 陰陽界生死戀
  - 兩男一女三逃犯

</div>

  - 外籍兵團
  - 中途島
  - 戰王
  - 再死一次
  - 阿呆與阿瓜

### 其它時段

  - [糊塗情報員](../Page/糊塗情報員.md "wikilink")（Get
    Smart；1989年10月1日至1990年3月25日每週日14:20\~14:50，1990年10月13日至1991年1月5日每週六15:30\~16:00，1996年2月25日至1996年4月14日每週日17:10\~17:40）
  - [原野飆龍](../Page/原野飆龍.md "wikilink")（The Young
    Riders；1991年11月30日至1992年10月10日每週六18:00\~19:00，1995年11月22日至1996年5月1日每週三00:10\~00:40重播）
  - [寶劍遊龍](../Page/寶劍遊龍.md "wikilink")（Zorro；1991年5月25,26日19:30\~20:00、1991年10月27日至1993年11月27日每週六16:30\~17:00）

<!-- end list -->

  -
    《蒙面俠蘇洛》的1990年版電視影集，共3季。

<!-- end list -->

  - [雷霆天龍](../Page/雷霆天龍.md "wikilink")（Super Boy；1990年2月9日～1994年10月22日）

<!-- end list -->

  -
    《超人》的少年版電視影集，每週六下午播出，共四季。

<!-- end list -->

  - [龍虎少年隊](../Page/龍虎少年隊.md "wikilink")（21 Jump Street）
  - [金剛戰士](../Page/金剛戰士.md "wikilink")（Mighty Morphin Power
    Rangers；1994年5月28日）

:\*[新金剛戰士](../Page/新金剛戰士.md "wikilink")(Power Rangers
Zeo；1998年7月19日～2001年5月26日）

  -

      -
        [台視最初於每週六下午](../Page/台視.md "wikilink")2:40播出，過沒多久便移至晚間6:30時段，本作在熱潮到達巔峰之際時，台灣國內片曾引進改編TV第二季至第三季之間的劇情的[金剛戰士(電影版)](../Page/金剛戰士\(電影版\).md "wikilink")（Mighty
        Morphin Power Rangers:The
        Movie），全部只播到第二作"Zeo"結束。而後於1998年4月4日仍有在台上映，連接第二作到第三作"Turbo"間的電影《[鐵弋戰警](../Page/鐵弋戰警.md "wikilink")（Turbo:A
        Power Rangers
        Movie）》，之後[台灣就再也沒有引進相關系列的影片](../Page/台灣.md "wikilink")，但[美國目前仍還有新進度](../Page/美國.md "wikilink")。

<!-- end list -->

  - [霹靂飛車](../Page/電腦快車.md "wikilink")（Viper；1995年1月28日至1995年4月22日每週六18:00\~19:00，1999年3月28日至1999年8月21日每週日17:00\~18:00）

<!-- end list -->

  -
    [中視曾翻譯為](../Page/中視.md "wikilink")《霹靂遊龍》，共四季，台視播出第一季就停播，後來在1999年復播第二～第四季，但演員、角色有異動。

<!-- end list -->

  - [戰警遊龍](../Page/戰警遊龍.md "wikilink")（Super
    Force；1993年12月4日～1994年5月14日）

<!-- end list -->

  -
    在不久的近未來，科技越來越發達，犯罪行為也日亦擴大而於某警察局任職的男主角警官**石賽克**，在得知自己的親哥哥**石瀾克**被黑幫殺死後原本心灰意冷，過了不久，卻又得到無法證實的情報－他的哥哥還活著而且還正在為黑幫工作，並殺了某位有錢科學家並意圖奪取某項高科技產品，但石賽克在前往該科學家實驗室調查之時，卻發現科學家並沒死，並將自己的記憶和人格轉移到實驗室的超級電腦內，身邊還有一名年輕的黑人助手，在得知石賽克的哥哥本來應該已死，卻又活生生地在為黑幫工作，科學家的意識決定要與石賽克併肩打擊黑幫，於是要黑人助手幫石賽克強化一件原本是要在火星使用的太空衣，讓石賽克能穿上強化後的太空衣打擊黑幫，並且找到有關石瀾克的任何線索，因此石賽克從此就有了兩個身份，一是警察局的警官，而另一則是身穿高科技戰鬥服，騎著改裝後的重機車追殺黑幫的機器戰警。

<!-- end list -->

  - [鐵膽英雄](../Page/鐵膽英雄_\(1985年美國影集\).md "wikilink")（Spenser For Hire）

<!-- end list -->

  -
    首播初期為原音播出，後改配國語。男主角羅伯尤瑞契（Robert Urich）後罹患滑膜肉瘤，於2002年逝世。

<!-- end list -->

  - [天外雙俠](../Page/天外雙俠.md "wikilink")（Voyagers）
  - [幽靈戰士](../Page/幽靈戰士.md "wikilink")（M.A.N.T.I.S.）
  - [鐵骨遊龍](../Page/鐵骨遊龍.md "wikilink")（Mike Hammer）
  - [新百戰天龍](../Page/新百戰天龍.md "wikilink")（Legend，1997年10月15日～1998年1月21日，每週三晚上10:30\~11:30播出）
  - [都市遊俠](../Page/都市遊俠.md "wikilink")（Robin's Hoods）
  - [鐵骨遊龍](../Page/鐵骨遊龍.md "wikilink")（Mike Hammer）

<!-- end list -->

  -
    男主角Stacy Keach因涉嫌吸毒自毀前程。

<!-- end list -->

  - [都市奇俠](../Page/都市奇俠.md "wikilink")（The Equalizer）

<!-- end list -->

  -
    後翻拍為電影《[私刑教育](../Page/私刑教育.md "wikilink")》。

<!-- end list -->

  - 特警急先鋒（[Tropical
    Heat](../Page/:en:Tropical_Heat.md "wikilink")，美國播映片名為《Sweating
    Bullets》）

### 其他原音播映影集

  - [朱門恩怨](../Page/朱門恩怨.md "wikilink")（[Dallas](../Page/:en:Dallas_\(1978_TV_series\).md "wikilink")，1979年8月17日至1987年2月6日）
  - [美國淪亡錄](../Page/美國淪亡錄.md "wikilink")（[Amerika](../Page/:en:Amerika_\(miniseries\).md "wikilink")）
  - [終極警網](../Page/終極警網.md "wikilink")（[Crime
    Story](../Page/:en:Crime_Story_\(TV_series\).md "wikilink")）
  - [千面女郎](../Page/千面女郎_\(1990年美國影集\).md "wikilink")（[Carol &
    Company](../Page/:en:Carol_&_Company.md "wikilink")）
  - [雙面嬌娃](../Page/雙面嬌娃_\(1985年美國影集\).md "wikilink")（[Moonlighting](../Page/:en:Moonlighting_\(TV_series\).md "wikilink")）

<!-- end list -->

  -
    [布魯斯威利的螢光幕嶄露頭角之作](../Page/布魯斯威利.md "wikilink")。

<!-- end list -->

  - [霹靂夜](../Page/霹靂夜.md "wikilink")（[Night
    Heat](../Page/:en:Night_Heat.md "wikilink")）

<!-- end list -->

  -
    由[加拿大所製作的影集](../Page/加拿大.md "wikilink")，但台視僅播八集即告腰斬。

<!-- end list -->

  - [美國警花](../Page/美國警花.md "wikilink")（Cagney And Lacey）
  - [風雲女郎](../Page/風雲女郎.md "wikilink")（Murphy Brown）
  - [雙面諜](../Page/雙面諜.md "wikilink")（Cover Up）
  - [霹靂警探](../Page/霹靂警探.md "wikilink")（NYPD Blue）
  - [醫門英傑](../Page/醫門英傑.md "wikilink")（Chicago Hope）
  - [洛城法網](../Page/洛城法網.md "wikilink")（L.A.Law）
  - [波城杏話](../Page/波城杏話.md "wikilink")（St. Elsewhere）
  - [小城風雲](../Page/小城風雲.md "wikilink")（Picket
    Fences，1993年11月28日～1995年2月26日）
  - [正義的溫柔](../Page/正義的溫柔.md "wikilink")（Sweet Justice）
  - [頂尖高手](../Page/頂尖高手.md "wikilink")（Pointman）
  - [五號戰星](../Page/五號戰星.md "wikilink")（Babylon 5）
  - [孿生娃娃妙爸爸](../Page/孿生娃娃妙爸爸.md "wikilink")（Something Wilder）
  - [三代一家親](../Page/三代一家親.md "wikilink")（[Maybe This
    Time](../Page/:en:Maybe_This_Time.md "wikilink")）
  - [深海巡弋](../Page/深海巡弋.md "wikilink")（[seaQuest
    DSV](../Page/:en:seaQuest_DSV.md "wikilink")，1997年4月22日～？）
  - [銀河前哨](../Page/銀河前哨.md "wikilink")（Star Trek: Deep Space
    Nine，1997～1998年）
  - [新鮮醫族](../Page/新鮮醫族.md "wikilink")（[Medicine
    Ball](../Page/:en:Medicine_Ball_\(TV_series\).md "wikilink")，1998年）
  - [銀河戰士](../Page/銀河戰士_\(1996年美國影集\).md "wikilink")（[Hypernauts](../Page/:en:Hypernauts.md "wikilink")）
  - [綺情美夢](../Page/綺情美夢.md "wikilink")（[Judith Krantz's
    "Secrets"](../Page/:en:Judith_Krantz's_"Secrets".md "wikilink")，1994年5月28日～，全32集）
  - [甜蜜芳心](../Page/甜蜜芳心.md "wikilink")（[My So-Called
    Life](../Page/:en:My_So-Called_Life.md "wikilink")，1999年）
  - [反恐任務](../Page/24_\(電視劇\).md "wikilink")（24）

<!-- end list -->

  -
    台視播出第一季至第五季。

<!-- end list -->

  - [歡樂一家親](../Page/歡樂一家親.md "wikilink")（Frasier）

<!-- end list -->

  -
    台視播出第一季與第二季。

<!-- end list -->

  - [邁阿密七小龍](../Page/邁阿密七小龍.md "wikilink")（Miami 7，2000年）
  - [神秘召喚](../Page/神秘召喚.md "wikilink")（Tru Calling；2006年3月3日～2006年7月6日）

<!-- end list -->

  -
    敘述女主角楚兒‧戴維斯（Tru Davis）在停屍間工作，遇見一些靈異事件的故事。

## 相關連結

  - [臺灣電視公司](../Page/臺灣電視公司.md "wikilink")
  - [臺灣電視公司節目列表](../Page/臺灣電視公司節目列表.md "wikilink")

## 外部連結

[\*](../Category/台視外購電視劇.md "wikilink")