**史堅如**（），原名**文緯**，字**經如**，後改**堅如**，[廣東](../Page/廣東.md "wikilink")[番禺人](../Page/番禺.md "wikilink")，[史古愚之弟](../Page/史古愚.md "wikilink")。清末革命烈士。

## 生平

[史堅如墓.JPG](https://zh.wikipedia.org/wiki/File:史堅如墓.JPG "fig:史堅如墓.JPG")
出身官僚富家，1899年加入[興中會](../Page/興中會.md "wikilink")，往長江聯絡會黨。1900年中國北方爆發[義和團運動](../Page/義和團運動.md "wikilink")，導致[八國聯軍入侵](../Page/八國聯軍.md "wikilink")，[鄭士良把握良機發動](../Page/鄭士良.md "wikilink")[惠州起義](../Page/惠州起義.md "wikilink")，史堅如在[廣州響應](../Page/廣州.md "wikilink")，初購炸藥二十五箱，被安勇搜去，再購炸藥兩百磅。以朋友宋少東名義租賃[廣東巡撫](../Page/廣東巡撫.md "wikilink")[德壽後花園附近的宅院](../Page/德壽_\(人物\).md "wikilink")，掘一地道，10月26日晚引爆炸藥，但引線失靈；10月28日他獨自再炸，引燃兩百磅炸藥，因藥量不足，有數位平民被炸死，德壽只是自床上震落受驚。史最初躲在華人傳教士兼興中會會員毛文明家中，31日史堅如赴[香港時](../Page/香港.md "wikilink")，半路被清兵查獲，。1900年11月9日，於[珠江碼頭被處死](../Page/珠江碼頭.md "wikilink")\[1\]。

[孫中山稱](../Page/孫中山.md "wikilink")：「浩氣英風，實足為後死者之模範」。

[廣州私立嶺南大學](../Page/岭南大学_\(广州\).md "wikilink")（今[中山大學](../Page/中山大學.md "wikilink")）內有一座「惺亭」，由惺社同學於民國十七年（1928年）捐建，用以紀念史堅如、區勵周、許耀章三位師生。

## 墓

史坚如墓位于[广州市越秀区](../Page/广州市.md "wikilink")[先烈中路](../Page/先烈路.md "wikilink")79号[黃花崗七十二烈士墓內](../Page/黃花崗七十二烈士墓.md "wikilink")，为广州市的一个市、县级文物保护单位，类型为近现代重要史迹及代表性建筑，公布时间为1963年3月。

史堅如先生祠於民國二年（1913年）興建，原位於先烈路青菜崗，1978年因被廣州當局徵地而遷至現址。

## 参考文献

[S](../Category/广州人.md "wikilink")
[Category:清朝被處決者](../Category/清朝被處決者.md "wikilink")
[Category:中华人民共和国烈士](../Category/中华人民共和国烈士.md "wikilink")
[J堅](../Category/史姓.md "wikilink")
[Category:广州市文物保护单位](../Category/广州市文物保护单位.md "wikilink")
[Category:华南师范大学附属中学校友](../Category/华南师范大学附属中学校友.md "wikilink")

1.