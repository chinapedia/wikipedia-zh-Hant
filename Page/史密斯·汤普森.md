**史密斯·汤普森**（**Smith
Thompson**，），[美国政治家](../Page/美国.md "wikilink")，[美国民主-共和党成员](../Page/美国民主-共和党.md "wikilink")，曾任[美国海军部长](../Page/美国海军部长.md "wikilink")（1819年-1823年）和[美国最高法院大法官](../Page/美国最高法院大法官.md "wikilink")（1823年-1843年）。

## 外部链接

  - [Smith
    Thompson](http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=5664)
  - [Smith
    Thompson](http://www.history.navy.mil/danfs/s14/smith_thompson.htm)

[T](../Category/美国最高法院大法官.md "wikilink")
[T](../Category/纽约州律师.md "wikilink")
[T](../Category/普林斯頓大學校友.md "wikilink")