**史景遷**（，），是一位出生於英國[倫敦的](../Page/倫敦.md "wikilink")[中国历史](../Page/中国历史.md "wikilink")[專業研究者](../Page/历史学家.md "wikilink")\[1\]，於[1993年起担任](../Page/1993年.md "wikilink")[耶鲁大学历史学](../Page/耶鲁大学.md "wikilink")[斯特林教席](../Page/斯特林教席.md "wikilink")，其妻為歷史學者[金安平](../Page/金安平.md "wikilink")。

史景遷一名為[房兆楹所命](../Page/房兆楹.md "wikilink")，其原義為“景仰[司馬遷](../Page/司馬遷.md "wikilink")”。

## 生平

史景迁1936年出生于英国[伦敦市郊的萨瑞地区](../Page/伦敦.md "wikilink")。曾就读于[剑桥大学克莱尔学院](../Page/剑桥大学克莱尔学院.md "wikilink")，在[剑桥大学获得学士学位](../Page/剑桥大学.md "wikilink")。1959年史景迁因获[美仑奖学金](../Page/美仑奖学金.md "wikilink")，以交换学生身份到美国[耶鲁大学读硕士学位](../Page/耶鲁大学.md "wikilink")，师从于史学家[费正清的学生](../Page/费正清.md "wikilink")[芮玛丽](../Page/芮玛丽.md "wikilink")。后来芮玛丽推荐史景迁去[澳洲跟](../Page/澳洲.md "wikilink")[房兆楹教授夫妇做博士论文](../Page/房兆楹.md "wikilink")，其博士论文《[康熙与曹寅](../Page/康熙与曹寅.md "wikilink")》后获[珀特尔论文奖](../Page/珀特尔论文奖.md "wikilink")。1965年史景迁获[耶鲁大学史学博士学位](../Page/耶鲁大学.md "wikilink")，\[2\]毕业后留在耶鲁教书。史景迁是[美国历史学会](../Page/美国历史学会.md "wikilink")2004年-2005年的主席。他的研究使得他经常访问中国的大学。

## 学术思想

史景迁研究[中国历史半生](../Page/中国历史.md "wikilink")，对[中国](../Page/中国.md "wikilink")[近代史的开端时间有与中国史学界不同的看法](../Page/近代史.md "wikilink")，而且他认为应该修改中小学历史教材，他说：“我在[西方教](../Page/西方.md "wikilink")[中文](../Page/中文.md "wikilink")、中国[历史](../Page/中国历史.md "wikilink")[文化的时候](../Page/中国文化.md "wikilink")，发现[中国人编的课本有一个缺陷](../Page/中国人.md "wikilink")，就是当他们讲述中国[近代历史的时候](../Page/近代历史.md "wikilink")，总是从19世纪中国受的屈辱和侵略开始切入。40年前我在开始教授中国历史时就觉得这非常不合理，如果要更好地研究中国历史，我们应该从17、18世纪的中国开始研究。因为当时的中国在世界上表现出一种更自信的姿态。”\[3\]

## 成就

景仰[司馬遷的史景迁](../Page/司馬遷.md "wikilink")，被公认是16世纪以来的最有影响力的[汉学家之一](../Page/汉学家.md "wikilink")，他在历史塑造现代中国所扮演的角色方面有详尽的写作。他备受好评的《追寻现代中国》
已经成为近代中国史的标准教科书。他最近的著作包括[毛泽东传记和](../Page/毛泽东.md "wikilink")《[大義覺迷](../Page/大義覺迷錄.md "wikilink")》，探究18世纪历史引人注目的一段经历。

[顾思齐稱](../Page/顾思齐.md "wikilink")“史景迁以叙事和文笔见长”\[4\]，史景遷的著作，大多能深入淺出，且文筆流暢、敘事性強，美國匹茲堡大學教授[許倬雲形容](../Page/許倬雲.md "wikilink")：“給他一本電話簿，他可以從第一頁的人名開始編故事，編到最後一個人名。”\[5\]史景遷是在[美國少數能使專業](../Page/美國.md "wikilink")[史學著作成為暢銷書的作者之一](../Page/史學.md "wikilink")，對於[中國歷史知識在西方](../Page/中國歷史.md "wikilink")[英語世界的傳布造成很大影響](../Page/英語.md "wikilink")。

史家[汪榮祖說他](../Page/汪榮祖.md "wikilink")：“史景迁并不喜欢后学理论，他的书根本没有什么理论，更无艰涩的名词，但他生动的叙事，完全可以迎合史学界随后学而起“叙事再生”（Revival
of
Narrative）的呼声，使他成为史学叙事再生后的一支生力军”，“他的作品作为历史文章毕竟缺乏分析与论证，也少见他对历史问题提出独特的解释。因而虽多引人入胜的故事，却少扎实的历史知识。”\[6\]據稱[钱锺书当年访问耶鲁时私下戏称史景迁为](../Page/钱锺书.md "wikilink")“失败的小说家”。\[7\]

## 著作

  - 《康熙與曹寅：一個皇室寵臣的生涯揭秘》（*Tsʻao Yin and the Kʻang-hsi Emperor:
    bondservant and master*）（1966）
  - 《改變中國：在中國的西方顧問》（*To Change China: Western Advisers in China,
    1620–1960*）（1969）
  - 《中國皇帝：康熙自畫像》（''Emperor of China: Self-Portrait of K'ang-Hsi
    ''）（1974）
  - 《婦人王氏之死：大歷史背後的小人物命運》（*The Death of Woman Wang*）（1978）
  - 《天安門：中國的知識份子與革命》（*The Gate of Heavenly Peace: The Chinese and Their
    Revolution 1895–1980*）（1982）
  - 《利瑪竇的記憶宮殿》（*The Memory Palace of Matteo Ricci*）（1984）
  - 《胡若望的疑問》（*The Question of Hu*）（1987）
  - 《追尋現代中國》（*The Search for Modern China*）（1990）
  - 《中國縱橫：一個漢學家的學術探索之旅》（*Chinese Roundabout: Essays on History and
    Culture*）（1992）
  - 《上帝的中國之子：洪秀全的太平天國》（*God's Chinese Son: The Taiping Heavenly Kingdom
    of Hong Xiuquan*）（1996）
  - 《大汗之國：西方眼中的中國》（*The Chan's Great Continent: China in Western
    Minds*）（1998）
  - 《毛澤東》（*Mao Zedong*）（1999）
  - 《[皇帝與秀才：皇權遊戲中的文人悲劇](../Page/皇帝與秀才：皇權遊戲中的文人悲劇.md "wikilink")》/《[雍正王朝之大義覺迷](../Page/雍正王朝之大義覺迷.md "wikilink")》（*Treason
    by the Book*）（2001）
  - 《前朝夢憶：張岱的浮華與蒼涼》（*Return to Dragon Mountain: Memories of a Late Ming
    Man*）（2007）

## 注释

## 参考文献

## 外部链接

  - [Jonathan D. Spence
    生平](http://www.historians.org/info/AHA_History/spencebio.cfm)
  - 汪荣祖：\[ 史景迁论\]

{{-}}

[Category:英国历史学家](../Category/英国历史学家.md "wikilink")
[Category:英國漢學家](../Category/英國漢學家.md "wikilink")
[Category:美國漢學家](../Category/美國漢學家.md "wikilink")
[Category:美国历史学家](../Category/美国历史学家.md "wikilink")
[Category:美国历史学会会长](../Category/美国历史学会会长.md "wikilink")
[Category:耶鲁大学教授](../Category/耶鲁大学教授.md "wikilink")
[Category:剑桥大学克莱尔学院校友](../Category/剑桥大学克莱尔学院校友.md "wikilink")
[Category:在美國的英格蘭人](../Category/在美國的英格蘭人.md "wikilink")
[Category:薩里郡人](../Category/薩里郡人.md "wikilink")
[Category:耶魯大學校友](../Category/耶魯大學校友.md "wikilink")
[Category:香港科技大學榮譽博士](../Category/香港科技大學榮譽博士.md "wikilink")
[Category:香港中文大學榮譽博士](../Category/香港中文大學榮譽博士.md "wikilink")

1.

2.
3.
4.  顾思齐：《本土视野的美国“汉学三杰”》。《南方都市报》，2005年5月25日

5.  陳希林：〈一本電話簿 史景遷編故事許倬雲找史料〉，《中國時報》，2005年12月3日

6.  汪荣祖：《史景迁论》

7.  黄绍坚：《“蒙太奇史学”——美国汉学家史景迁和他的史学著作》