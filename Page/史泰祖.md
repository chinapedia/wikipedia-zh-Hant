[Dr._Shih1.jpg](https://zh.wikipedia.org/wiki/File:Dr._Shih1.jpg "fig:Dr._Shih1.jpg")
**史泰祖**，是[香港皮膚科](../Page/香港.md "wikilink")[醫生](../Page/醫生.md "wikilink")，[匯賢智庫副主席](../Page/匯賢智庫.md "wikilink")\[1\]，[香港醫學會前副會長以及](../Page/香港醫學會.md "wikilink")[新力量網絡前主席](../Page/新力量網絡.md "wikilink")。曾為[新民黨副主席](../Page/新民黨.md "wikilink")，現已退黨。\[2\]育有三名子女，分別為長子史雋維，次女史雋丹及三女史雋皓。\[3\]

## 政治生涯

[HK_2008_Lego_Vote_Banner_Regina_Ip_Lau_Suk-yee_n_Louis_Shih_Tai_Cho_a.jpg](https://zh.wikipedia.org/wiki/File:HK_2008_Lego_Vote_Banner_Regina_Ip_Lau_Suk-yee_n_Louis_Shih_Tai_Cho_a.jpg "fig:HK_2008_Lego_Vote_Banner_Regina_Ip_Lau_Suk-yee_n_Louis_Shih_Tai_Cho_a.jpg")期間，葉劉淑儀團隊的宣傳刀旗在[中西區](../Page/中西區_\(香港\).md "wikilink")。\]\]
[2007年香港立法會香港島地方選區補選中](../Page/2007年香港立法會香港島地方選區補選.md "wikilink")，由於一早答應支持獲[建制派支持的獨立候選人](../Page/建制派.md "wikilink")[葉劉淑儀](../Page/葉劉淑儀.md "wikilink")，所以後來[泛民主派的](../Page/泛民主派.md "wikilink")[陳方安生決定參選時](../Page/陳方安生.md "wikilink")，史泰祖不支持，因而受到泛民主派的抨擊。

[2008年香港立法會選舉](../Page/2008年香港立法會選舉.md "wikilink")，史泰祖獲葉劉淑儀邀請，加入其港島區參選名單，排名第二出選。競選期間，被問到北京公安對香港記者施以「叉頸」有何意見，史表示「人一定唔可以，但我知道有時動物都會咁樣箍住」（[書面語](../Page/書面語.md "wikilink")：人一定不可以，但我知道動物有時候也會被這樣纏着）。有報章認為此言論是將記者比作動物\[4\]。最後史泰祖僅取得2.82%的得票餘額，未能隨葉劉淑儀一同當選。

2014年10月29日，醫學會會長史泰祖日前否決就業界佔中立場作民調，更與支持調查的前會長[蔡堅關係撕裂](../Page/蔡堅.md "wikilink")，蔡堅昨批評史泰祖令醫學會退縮，他將不再參與會務以示抗議。\[5\]

## 簡歷

  - 1969年，[喇沙書院中學畢業](../Page/喇沙書院.md "wikilink")
  - 1977年，[香港大學醫學院](../Page/香港大學.md "wikilink")[畢業](../Page/畢業.md "wikilink")，曾任[伊利沙伯醫院](../Page/伊利沙伯醫院.md "wikilink")[內科及](../Page/內科.md "wikilink")[心臟科](../Page/心臟科.md "wikilink")[實習醫生](../Page/實習醫生.md "wikilink")。
  - 1985年，[移民](../Page/移民.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")，於[多倫多大學醫院內科部工作](../Page/多倫多大學.md "wikilink")。
  - 1991年，成為皮膚科醫生。
  - 1995年，重返香港發展。
  - 2009年，被委任為[太平紳士](../Page/太平紳士.md "wikilink")

## 職務

  - 新力量網絡前主席
  - [香港醫學會會董](../Page/香港醫學會.md "wikilink")
  - 香港[唐氏綜合症協會執行委員會秘書](../Page/唐氏綜合症.md "wikilink")

## 參考文獻

## 外部連結

  - [史泰祖醫生都有失落時](http://www.jiujik.com/jsexpertsdetails.php?lcid=HK.B5&artid=3000006100)
  - [史泰祖醫生已婚，屋企有三個女](http://health.atnext.com/index.php?fuseaction=Article.ListArticle&sec_id=6349009&iss_id=20060806&art_id=6491498)
  - [史泰祖醫生診所資料](http://www.hkdoctorguide.com/doctor/dr-shih-tai-cho-louis)

[史](../Category/香港醫學界人士.md "wikilink")
[Category:香港新民黨前成員](../Category/香港新民黨前成員.md "wikilink")
[S](../Category/喇沙書院校友.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[Category:加拿大華人](../Category/加拿大華人.md "wikilink")
[T](../Category/史姓.md "wikilink")
[Category:香港建制派人士](../Category/香港建制派人士.md "wikilink")
[Category:醫生出身的政治人物](../Category/醫生出身的政治人物.md "wikilink")

1.
2.  [政情：為選醫學會
    史泰祖退黨](http://orientaldaily.on.cc/cnt/news/20140615/00176_091.html)
    on.cc 2014-06-15
3.
4.  [2008立法會選舉：葉劉剛認衰 史泰祖又失言 評論公安叉頸
    將記者與動物相提並論](http://www1.appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20080731&sec_id=4104&subsec_id=11867&art_id=11415045)
    ，[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")，2008年7月31日
5.  [抨史泰祖退縮蔡堅決「不合作」
    醫學會兩巨頭為佔中撕裂](https://hk.news.yahoo.com/%E6%8A%A8%E5%8F%B2%E6%B3%B0%E7%A5%96%E9%80%80%E7%B8%AE%E8%94%A1%E5%A0%85%E6%B1%BA-%E4%B8%8D%E5%90%88%E4%BD%9C-%E9%86%AB%E5%AD%B8%E6%9C%83%E5%85%A9%E5%B7%A8%E9%A0%AD%E7%82%BA%E4%BD%94%E4%B8%AD%E6%92%95%E8%A3%82-215635014.html)